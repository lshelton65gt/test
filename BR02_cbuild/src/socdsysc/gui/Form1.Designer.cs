﻿namespace sys2socd
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.openFileDialogDesignFile = new System.Windows.Forms.OpenFileDialog();
      this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
      this.textBoxDesignFile = new System.Windows.Forms.TextBox();
      this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
      this.wizard1 = new Gui.Wizard.Wizard();
      this.wizardPageInit = new Gui.Wizard.WizardPage();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.header1 = new Gui.Wizard.Header();
      this.panel1 = new System.Windows.Forms.Panel();
      this.panelSourceFiles = new System.Windows.Forms.Panel();
      this.listBoxSourceFiles = new System.Windows.Forms.ListBox();
      this.label2 = new System.Windows.Forms.Label();
      this.buttonBrowseSourceFiles = new System.Windows.Forms.Button();
      this.buttonRemoveSource = new System.Windows.Forms.Button();
      this.panelIncludeFiles = new System.Windows.Forms.Panel();
      this.listBoxIncludeFiles = new System.Windows.Forms.ListBox();
      this.buttonBrowseIncludeFiles = new System.Windows.Forms.Button();
      this.buttonRemoveIncludeFile = new System.Windows.Forms.Button();
      this.label7 = new System.Windows.Forms.Label();
      this.buttonBrowseOutputDir = new System.Windows.Forms.Button();
      this.textBoxOutputDir = new System.Windows.Forms.TextBox();
      this.comboBoxTarget = new System.Windows.Forms.ComboBox();
      this.comboBoxVC = new System.Windows.Forms.ComboBox();
      this.labelTarget = new System.Windows.Forms.Label();
      this.labelVC = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.textBoxLinkerLibs = new System.Windows.Forms.TextBox();
      this.label13 = new System.Windows.Forms.Label();
      this.label10 = new System.Windows.Forms.Label();
      this.textBoxLinkerDirs = new System.Windows.Forms.TextBox();
      this.label11 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.textBoxHeaderGuard = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.textBoxDefinitions = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.buttonBrowseIncludePaths = new System.Windows.Forms.Button();
      this.textBoxIncludePaths = new System.Windows.Forms.TextBox();
      this.label14 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.buttonBrowseDesignFile = new System.Windows.Forms.Button();
      this.wizardPageGenerate = new Gui.Wizard.WizardPage();
      this.panel7 = new System.Windows.Forms.Panel();
      this.progressBar2 = new System.Windows.Forms.ProgressBar();
      this.richTextBox2 = new System.Windows.Forms.RichTextBox();
      this.header5 = new Gui.Wizard.Header();
      this.wizardPageModules = new Gui.Wizard.WizardPage();
      this.panel4 = new System.Windows.Forms.Panel();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.treeViewModules = new System.Windows.Forms.TreeView();
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.tabPage1 = new System.Windows.Forms.TabPage();
      this.dataGridView1 = new System.Windows.Forms.DataGridView();
      this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.comboBoxCtors = new System.Windows.Forms.ComboBox();
      this.label5 = new System.Windows.Forms.Label();
      this.tabPage3 = new System.Windows.Forms.TabPage();
      this.panel6 = new System.Windows.Forms.Panel();
      this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
      this.listViewParameters = new System.Windows.Forms.ListView();
      this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
      this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
      this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
      this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
      this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
      this.buttonAddParameter = new System.Windows.Forms.Button();
      this.buttonRemoveParameter = new System.Windows.Forms.Button();
      this.buttonEditParameter = new System.Windows.Forms.Button();
      this.panel5 = new System.Windows.Forms.Panel();
      this.buttonBrowseParamCallbackSource = new System.Windows.Forms.Button();
      this.textBoxParamCallbackSource = new System.Windows.Forms.TextBox();
      this.label9 = new System.Windows.Forms.Label();
      this.header4 = new Gui.Wizard.Header();
      this.wizardPageModuleSelect = new Gui.Wizard.WizardPage();
      this.panel3 = new System.Windows.Forms.Panel();
      this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
      this.checkedListBoxModules = new System.Windows.Forms.CheckedListBox();
      this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
      this.buttonCheckAll = new System.Windows.Forms.Button();
      this.buttonUncheckAll = new System.Windows.Forms.Button();
      this.header3 = new Gui.Wizard.Header();
      this.wizardPageAnalyze = new Gui.Wizard.WizardPage();
      this.panel2 = new System.Windows.Forms.Panel();
      this.richTextBox1 = new System.Windows.Forms.RichTextBox();
      this.progressBar1 = new System.Windows.Forms.ProgressBar();
      this.header2 = new Gui.Wizard.Header();
      ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
      this.wizard1.SuspendLayout();
      this.wizardPageInit.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.panel1.SuspendLayout();
      this.panelSourceFiles.SuspendLayout();
      this.panelIncludeFiles.SuspendLayout();
      this.wizardPageGenerate.SuspendLayout();
      this.panel7.SuspendLayout();
      this.wizardPageModules.SuspendLayout();
      this.panel4.SuspendLayout();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.tabControl1.SuspendLayout();
      this.tabPage1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      this.tabPage2.SuspendLayout();
      this.tabPage3.SuspendLayout();
      this.panel6.SuspendLayout();
      this.tableLayoutPanel2.SuspendLayout();
      this.tableLayoutPanel4.SuspendLayout();
      this.panel5.SuspendLayout();
      this.wizardPageModuleSelect.SuspendLayout();
      this.panel3.SuspendLayout();
      this.tableLayoutPanel3.SuspendLayout();
      this.flowLayoutPanel1.SuspendLayout();
      this.wizardPageAnalyze.SuspendLayout();
      this.panel2.SuspendLayout();
      this.SuspendLayout();
      // 
      // openFileDialogDesignFile
      // 
      this.openFileDialogDesignFile.Filter = "SystemC Files|*.cxx;*.cpp;*.h;*.cc|All files|*.*";
      // 
      // errorProvider1
      // 
      this.errorProvider1.ContainerControl = this;
      // 
      // textBoxDesignFile
      // 
      this.textBoxDesignFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.errorProvider1.SetError(this.textBoxDesignFile, "Required");
      this.textBoxDesignFile.Location = new System.Drawing.Point(158, 43);
      this.textBoxDesignFile.Name = "textBoxDesignFile";
      this.textBoxDesignFile.Size = new System.Drawing.Size(522, 21);
      this.textBoxDesignFile.TabIndex = 4;
      this.textBoxDesignFile.TextChanged += new System.EventHandler(this.textBoxDesignFile_TextChanged);
      // 
      // folderBrowserDialog1
      // 
      this.folderBrowserDialog1.ShowNewFolderButton = false;
      // 
      // wizard1
      // 
      this.wizard1.Controls.Add(this.wizardPageInit);
      this.wizard1.Controls.Add(this.wizardPageGenerate);
      this.wizard1.Controls.Add(this.wizardPageModules);
      this.wizard1.Controls.Add(this.wizardPageModuleSelect);
      this.wizard1.Controls.Add(this.wizardPageAnalyze);
      this.wizard1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.wizard1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.wizard1.Location = new System.Drawing.Point(0, 0);
      this.wizard1.Name = "wizard1";
      this.wizard1.Pages.AddRange(new Gui.Wizard.WizardPage[] {
            this.wizardPageInit,
            this.wizardPageAnalyze,
            this.wizardPageModuleSelect,
            this.wizardPageModules,
            this.wizardPageGenerate});
      this.wizard1.Size = new System.Drawing.Size(863, 593);
      this.wizard1.TabIndex = 0;
      // 
      // wizardPageInit
      // 
      this.wizardPageInit.Controls.Add(this.tableLayoutPanel1);
      this.wizardPageInit.Dock = System.Windows.Forms.DockStyle.Fill;
      this.wizardPageInit.IsFinishPage = false;
      this.wizardPageInit.Location = new System.Drawing.Point(0, 0);
      this.wizardPageInit.Name = "wizardPageInit";
      this.wizardPageInit.Size = new System.Drawing.Size(863, 545);
      this.wizardPageInit.TabIndex = 1;
      this.wizardPageInit.CloseFromNext += new Gui.Wizard.PageEventHandler(this.wizardPageInit_CloseFromNext);
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 1;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Controls.Add(this.header1, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(863, 545);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // header1
      // 
      this.header1.BackColor = System.Drawing.SystemColors.Control;
      this.header1.CausesValidation = false;
      this.header1.Description = "This wizard will create SoC Designer Components for each of your SystemC Modules " +
          "specified in the top-level design file.";
      this.header1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.header1.Image = null;
      this.header1.Location = new System.Drawing.Point(3, 3);
      this.header1.Name = "header1";
      this.header1.Size = new System.Drawing.Size(857, 64);
      this.header1.TabIndex = 0;
      this.header1.Title = "SystemC Design Parameters";
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.panelSourceFiles);
      this.panel1.Controls.Add(this.panelIncludeFiles);
      this.panel1.Controls.Add(this.buttonBrowseOutputDir);
      this.panel1.Controls.Add(this.textBoxOutputDir);
      this.panel1.Controls.Add(this.comboBoxTarget);
      this.panel1.Controls.Add(this.comboBoxVC);
      this.panel1.Controls.Add(this.labelTarget);
      this.panel1.Controls.Add(this.labelVC);
      this.panel1.Controls.Add(this.label12);
      this.panel1.Controls.Add(this.textBoxLinkerLibs);
      this.panel1.Controls.Add(this.label13);
      this.panel1.Controls.Add(this.label10);
      this.panel1.Controls.Add(this.textBoxLinkerDirs);
      this.panel1.Controls.Add(this.label11);
      this.panel1.Controls.Add(this.label8);
      this.panel1.Controls.Add(this.label6);
      this.panel1.Controls.Add(this.textBoxHeaderGuard);
      this.panel1.Controls.Add(this.label4);
      this.panel1.Controls.Add(this.textBoxDefinitions);
      this.panel1.Controls.Add(this.label3);
      this.panel1.Controls.Add(this.buttonBrowseIncludePaths);
      this.panel1.Controls.Add(this.textBoxDesignFile);
      this.panel1.Controls.Add(this.textBoxIncludePaths);
      this.panel1.Controls.Add(this.label14);
      this.panel1.Controls.Add(this.label1);
      this.panel1.Controls.Add(this.buttonBrowseDesignFile);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel1.Location = new System.Drawing.Point(3, 73);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(857, 469);
      this.panel1.TabIndex = 1;
      // 
      // panelSourceFiles
      // 
      this.panelSourceFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.panelSourceFiles.Controls.Add(this.listBoxSourceFiles);
      this.panelSourceFiles.Controls.Add(this.label2);
      this.panelSourceFiles.Controls.Add(this.buttonBrowseSourceFiles);
      this.panelSourceFiles.Controls.Add(this.buttonRemoveSource);
      this.panelSourceFiles.Location = new System.Drawing.Point(9, 89);
      this.panelSourceFiles.Name = "panelSourceFiles";
      this.panelSourceFiles.Size = new System.Drawing.Size(799, 86);
      this.panelSourceFiles.TabIndex = 33;
      // 
      // listBoxSourceFiles
      // 
      this.listBoxSourceFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.listBoxSourceFiles.FormattingEnabled = true;
      this.listBoxSourceFiles.Location = new System.Drawing.Point(148, 0);
      this.listBoxSourceFiles.Name = "listBoxSourceFiles";
      this.listBoxSourceFiles.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.listBoxSourceFiles.Size = new System.Drawing.Size(525, 82);
      this.listBoxSourceFiles.TabIndex = 1;
      this.listBoxSourceFiles.SelectedIndexChanged += new System.EventHandler(this.listBoxSourceFiles_SelectedIndexChanged);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 3);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(126, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "Additional Source File(s):";
      // 
      // buttonBrowseSourceFiles
      // 
      this.buttonBrowseSourceFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonBrowseSourceFiles.CausesValidation = false;
      this.buttonBrowseSourceFiles.Location = new System.Drawing.Point(692, 3);
      this.buttonBrowseSourceFiles.Name = "buttonBrowseSourceFiles";
      this.buttonBrowseSourceFiles.Size = new System.Drawing.Size(38, 23);
      this.buttonBrowseSourceFiles.TabIndex = 2;
      this.buttonBrowseSourceFiles.Text = "...";
      this.buttonBrowseSourceFiles.UseVisualStyleBackColor = true;
      this.buttonBrowseSourceFiles.Click += new System.EventHandler(this.buttonBrowseSourceFiles_Click);
      // 
      // buttonRemoveSource
      // 
      this.buttonRemoveSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonRemoveSource.CausesValidation = false;
      this.buttonRemoveSource.Location = new System.Drawing.Point(692, 33);
      this.buttonRemoveSource.Name = "buttonRemoveSource";
      this.buttonRemoveSource.Size = new System.Drawing.Size(75, 23);
      this.buttonRemoveSource.TabIndex = 3;
      this.buttonRemoveSource.Text = "Remove";
      this.buttonRemoveSource.UseVisualStyleBackColor = true;
      this.buttonRemoveSource.Click += new System.EventHandler(this.buttonRemoveSource_Click);
      // 
      // panelIncludeFiles
      // 
      this.panelIncludeFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.panelIncludeFiles.Controls.Add(this.listBoxIncludeFiles);
      this.panelIncludeFiles.Controls.Add(this.buttonBrowseIncludeFiles);
      this.panelIncludeFiles.Controls.Add(this.buttonRemoveIncludeFile);
      this.panelIncludeFiles.Controls.Add(this.label7);
      this.panelIncludeFiles.Location = new System.Drawing.Point(9, 186);
      this.panelIncludeFiles.Name = "panelIncludeFiles";
      this.panelIncludeFiles.Size = new System.Drawing.Size(799, 90);
      this.panelIncludeFiles.TabIndex = 32;
      // 
      // listBoxIncludeFiles
      // 
      this.listBoxIncludeFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.listBoxIncludeFiles.FormattingEnabled = true;
      this.listBoxIncludeFiles.Location = new System.Drawing.Point(147, 3);
      this.listBoxIncludeFiles.Name = "listBoxIncludeFiles";
      this.listBoxIncludeFiles.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.listBoxIncludeFiles.Size = new System.Drawing.Size(524, 82);
      this.listBoxIncludeFiles.TabIndex = 1;
      // 
      // buttonBrowseIncludeFiles
      // 
      this.buttonBrowseIncludeFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonBrowseIncludeFiles.CausesValidation = false;
      this.buttonBrowseIncludeFiles.Location = new System.Drawing.Point(696, 3);
      this.buttonBrowseIncludeFiles.Name = "buttonBrowseIncludeFiles";
      this.buttonBrowseIncludeFiles.Size = new System.Drawing.Size(38, 23);
      this.buttonBrowseIncludeFiles.TabIndex = 2;
      this.buttonBrowseIncludeFiles.Text = "...";
      this.buttonBrowseIncludeFiles.UseVisualStyleBackColor = true;
      this.buttonBrowseIncludeFiles.Click += new System.EventHandler(this.buttonBrowseIncludeFiles_Click);
      // 
      // buttonRemoveIncludeFile
      // 
      this.buttonRemoveIncludeFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonRemoveIncludeFile.CausesValidation = false;
      this.buttonRemoveIncludeFile.Location = new System.Drawing.Point(693, 32);
      this.buttonRemoveIncludeFile.Name = "buttonRemoveIncludeFile";
      this.buttonRemoveIncludeFile.Size = new System.Drawing.Size(75, 23);
      this.buttonRemoveIncludeFile.TabIndex = 3;
      this.buttonRemoveIncludeFile.Text = "Remove";
      this.buttonRemoveIncludeFile.UseVisualStyleBackColor = true;
      this.buttonRemoveIncludeFile.Click += new System.EventHandler(this.buttonRemoveIncludeFile_Click);
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(16, 3);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(122, 13);
      this.label7.TabIndex = 0;
      this.label7.Text = "Common Include File(s):";
      // 
      // buttonBrowseOutputDir
      // 
      this.buttonBrowseOutputDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonBrowseOutputDir.Location = new System.Drawing.Point(705, 6);
      this.buttonBrowseOutputDir.Name = "buttonBrowseOutputDir";
      this.buttonBrowseOutputDir.Size = new System.Drawing.Size(39, 23);
      this.buttonBrowseOutputDir.TabIndex = 2;
      this.buttonBrowseOutputDir.Text = "...";
      this.buttonBrowseOutputDir.UseVisualStyleBackColor = true;
      this.buttonBrowseOutputDir.Click += new System.EventHandler(this.buttonBrowseOutputDir_Click);
      // 
      // textBoxOutputDir
      // 
      this.textBoxOutputDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.textBoxOutputDir.Location = new System.Drawing.Point(157, 6);
      this.textBoxOutputDir.Name = "textBoxOutputDir";
      this.textBoxOutputDir.Size = new System.Drawing.Size(523, 21);
      this.textBoxOutputDir.TabIndex = 1;
      // 
      // comboBoxTarget
      // 
      this.comboBoxTarget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBoxTarget.FormattingEnabled = true;
      this.comboBoxTarget.Items.AddRange(new object[] {
            "Release",
            "Debug",
            "All"});
      this.comboBoxTarget.Location = new System.Drawing.Point(392, 438);
      this.comboBoxTarget.Name = "comboBoxTarget";
      this.comboBoxTarget.Size = new System.Drawing.Size(156, 21);
      this.comboBoxTarget.TabIndex = 22;
      // 
      // comboBoxVC
      // 
      this.comboBoxVC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBoxVC.FormattingEnabled = true;
      this.comboBoxVC.Location = new System.Drawing.Point(156, 438);
      this.comboBoxVC.Name = "comboBoxVC";
      this.comboBoxVC.Size = new System.Drawing.Size(156, 21);
      this.comboBoxVC.TabIndex = 20;
      // 
      // labelTarget
      // 
      this.labelTarget.AutoSize = true;
      this.labelTarget.Location = new System.Drawing.Point(318, 441);
      this.labelTarget.Name = "labelTarget";
      this.labelTarget.Size = new System.Drawing.Size(68, 13);
      this.labelTarget.TabIndex = 21;
      this.labelTarget.Text = "Build Target:";
      // 
      // labelVC
      // 
      this.labelVC.AutoSize = true;
      this.labelVC.Location = new System.Drawing.Point(83, 441);
      this.labelVC.Name = "labelVC";
      this.labelVC.Size = new System.Drawing.Size(64, 13);
      this.labelVC.TabIndex = 19;
      this.labelVC.Text = "Visual C++:";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(79, 354);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(68, 13);
      this.label12.TabIndex = 10;
      this.label12.Text = "Linker Lib(s):";
      // 
      // textBoxLinkerLibs
      // 
      this.textBoxLinkerLibs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.textBoxLinkerLibs.Location = new System.Drawing.Point(156, 351);
      this.textBoxLinkerLibs.Name = "textBoxLinkerLibs";
      this.textBoxLinkerLibs.Size = new System.Drawing.Size(522, 21);
      this.textBoxLinkerLibs.TabIndex = 11;
      this.textBoxLinkerLibs.Text = "maxsimcore";
      // 
      // label13
      // 
      this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point(699, 374);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(132, 13);
      this.label13.TabIndex = 15;
      this.label13.Text = "(separated by semi-colon)";
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(79, 324);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(68, 13);
      this.label10.TabIndex = 8;
      this.label10.Text = "Linker Dir(s):";
      // 
      // textBoxLinkerDirs
      // 
      this.textBoxLinkerDirs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.textBoxLinkerDirs.Location = new System.Drawing.Point(156, 321);
      this.textBoxLinkerDirs.Name = "textBoxLinkerDirs";
      this.textBoxLinkerDirs.Size = new System.Drawing.Size(522, 21);
      this.textBoxLinkerDirs.TabIndex = 9;
      this.textBoxLinkerDirs.Text = "$(MAXSIM_HOME)/lib/Linux/release";
      // 
      // label11
      // 
      this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(699, 344);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(132, 13);
      this.label11.TabIndex = 12;
      this.label11.Text = "(separated by semi-colon)";
      // 
      // label8
      // 
      this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(699, 406);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(132, 13);
      this.label8.TabIndex = 18;
      this.label8.Text = "(separated by semi-colon)";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(69, 408);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(78, 13);
      this.label6.TabIndex = 16;
      this.label6.Text = "Header Guard:";
      // 
      // textBoxHeaderGuard
      // 
      this.textBoxHeaderGuard.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.textBoxHeaderGuard.Location = new System.Drawing.Point(156, 405);
      this.textBoxHeaderGuard.Name = "textBoxHeaderGuard";
      this.textBoxHeaderGuard.Size = new System.Drawing.Size(522, 21);
      this.textBoxHeaderGuard.TabIndex = 17;
      this.textBoxHeaderGuard.Text = "DENALI_WRAPPER_HEADER";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(64, 381);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(83, 13);
      this.label4.TabIndex = 13;
      this.label4.Text = "Definitions (-D):";
      // 
      // textBoxDefinitions
      // 
      this.textBoxDefinitions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.textBoxDefinitions.Location = new System.Drawing.Point(156, 378);
      this.textBoxDefinitions.Name = "textBoxDefinitions";
      this.textBoxDefinitions.Size = new System.Drawing.Size(522, 21);
      this.textBoxDefinitions.TabIndex = 14;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(63, 288);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(84, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "Include Path(s):";
      // 
      // buttonBrowseIncludePaths
      // 
      this.buttonBrowseIncludePaths.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.buttonBrowseIncludePaths.CausesValidation = false;
      this.buttonBrowseIncludePaths.Location = new System.Drawing.Point(701, 288);
      this.buttonBrowseIncludePaths.Name = "buttonBrowseIncludePaths";
      this.buttonBrowseIncludePaths.Size = new System.Drawing.Size(38, 23);
      this.buttonBrowseIncludePaths.TabIndex = 18;
      this.buttonBrowseIncludePaths.Text = "...";
      this.buttonBrowseIncludePaths.UseVisualStyleBackColor = true;
      this.buttonBrowseIncludePaths.Click += new System.EventHandler(this.buttonBrowseIncludePaths_Click);
      // 
      // textBoxIncludePaths
      // 
      this.textBoxIncludePaths.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.textBoxIncludePaths.Location = new System.Drawing.Point(157, 285);
      this.textBoxIncludePaths.Name = "textBoxIncludePaths";
      this.textBoxIncludePaths.Size = new System.Drawing.Size(522, 21);
      this.textBoxIncludePaths.TabIndex = 7;
      this.textBoxIncludePaths.Text = "$(MAXSIM_HOME)/include";
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Location = new System.Drawing.Point(55, 9);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(92, 13);
      this.label14.TabIndex = 0;
      this.label14.Text = "Output Directory:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(36, 48);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(111, 13);
      this.label1.TabIndex = 3;
      this.label1.Text = "Top Level Design File:";
      // 
      // buttonBrowseDesignFile
      // 
      this.buttonBrowseDesignFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonBrowseDesignFile.CausesValidation = false;
      this.buttonBrowseDesignFile.Location = new System.Drawing.Point(705, 43);
      this.buttonBrowseDesignFile.Name = "buttonBrowseDesignFile";
      this.buttonBrowseDesignFile.Size = new System.Drawing.Size(38, 23);
      this.buttonBrowseDesignFile.TabIndex = 5;
      this.buttonBrowseDesignFile.Text = "...";
      this.buttonBrowseDesignFile.UseVisualStyleBackColor = true;
      this.buttonBrowseDesignFile.Click += new System.EventHandler(this.buttonBrowseDesignFile_Click);
      // 
      // wizardPageGenerate
      // 
      this.wizardPageGenerate.Controls.Add(this.panel7);
      this.wizardPageGenerate.Controls.Add(this.header5);
      this.wizardPageGenerate.Dock = System.Windows.Forms.DockStyle.Fill;
      this.wizardPageGenerate.IsFinishPage = false;
      this.wizardPageGenerate.Location = new System.Drawing.Point(0, 0);
      this.wizardPageGenerate.Name = "wizardPageGenerate";
      this.wizardPageGenerate.Size = new System.Drawing.Size(863, 545);
      this.wizardPageGenerate.TabIndex = 5;
      this.wizardPageGenerate.ShowFromNext += new System.EventHandler(this.wizardPageGenerate_ShowFromNext);
      // 
      // panel7
      // 
      this.panel7.Controls.Add(this.progressBar2);
      this.panel7.Controls.Add(this.richTextBox2);
      this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel7.Location = new System.Drawing.Point(0, 64);
      this.panel7.Name = "panel7";
      this.panel7.Size = new System.Drawing.Size(863, 481);
      this.panel7.TabIndex = 1;
      // 
      // progressBar2
      // 
      this.progressBar2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.progressBar2.Location = new System.Drawing.Point(0, 458);
      this.progressBar2.Maximum = 10;
      this.progressBar2.Name = "progressBar2";
      this.progressBar2.Size = new System.Drawing.Size(863, 23);
      this.progressBar2.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
      this.progressBar2.TabIndex = 1;
      // 
      // richTextBox2
      // 
      this.richTextBox2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.richTextBox2.Location = new System.Drawing.Point(0, 0);
      this.richTextBox2.Name = "richTextBox2";
      this.richTextBox2.ReadOnly = true;
      this.richTextBox2.Size = new System.Drawing.Size(863, 481);
      this.richTextBox2.TabIndex = 0;
      this.richTextBox2.Text = "";
      // 
      // header5
      // 
      this.header5.BackColor = System.Drawing.SystemColors.Control;
      this.header5.CausesValidation = false;
      this.header5.Description = "Description";
      this.header5.Dock = System.Windows.Forms.DockStyle.Top;
      this.header5.Image = null;
      this.header5.Location = new System.Drawing.Point(0, 0);
      this.header5.Name = "header5";
      this.header5.Size = new System.Drawing.Size(863, 64);
      this.header5.TabIndex = 0;
      this.header5.Title = "Generate Code";
      // 
      // wizardPageModules
      // 
      this.wizardPageModules.Controls.Add(this.panel4);
      this.wizardPageModules.Controls.Add(this.header4);
      this.wizardPageModules.Dock = System.Windows.Forms.DockStyle.Fill;
      this.wizardPageModules.IsFinishPage = false;
      this.wizardPageModules.Location = new System.Drawing.Point(0, 0);
      this.wizardPageModules.Name = "wizardPageModules";
      this.wizardPageModules.Size = new System.Drawing.Size(863, 580);
      this.wizardPageModules.TabIndex = 4;
      this.wizardPageModules.CloseFromNext += new Gui.Wizard.PageEventHandler(this.wizardPageModules_CloseFromNext);
      this.wizardPageModules.ShowFromNext += new System.EventHandler(this.wizardPageModules_ShowFromNext);
      // 
      // panel4
      // 
      this.panel4.Controls.Add(this.splitContainer1);
      this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel4.Location = new System.Drawing.Point(0, 64);
      this.panel4.Name = "panel4";
      this.panel4.Size = new System.Drawing.Size(863, 516);
      this.panel4.TabIndex = 1;
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.treeViewModules);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
      this.splitContainer1.Size = new System.Drawing.Size(863, 516);
      this.splitContainer1.SplitterDistance = 140;
      this.splitContainer1.TabIndex = 0;
      // 
      // treeViewModules
      // 
      this.treeViewModules.Dock = System.Windows.Forms.DockStyle.Fill;
      this.treeViewModules.FullRowSelect = true;
      this.treeViewModules.HideSelection = false;
      this.treeViewModules.Location = new System.Drawing.Point(0, 0);
      this.treeViewModules.Name = "treeViewModules";
      this.treeViewModules.ShowLines = false;
      this.treeViewModules.ShowRootLines = false;
      this.treeViewModules.Size = new System.Drawing.Size(140, 516);
      this.treeViewModules.TabIndex = 0;
      this.treeViewModules.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewModules_AfterSelect);
      this.treeViewModules.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeViewModules_BeforeSelect);
      // 
      // tabControl1
      // 
      this.tabControl1.Controls.Add(this.tabPage1);
      this.tabControl1.Controls.Add(this.tabPage2);
      this.tabControl1.Controls.Add(this.tabPage3);
      this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabControl1.Location = new System.Drawing.Point(0, 0);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size(719, 516);
      this.tabControl1.TabIndex = 0;
      // 
      // tabPage1
      // 
      this.tabPage1.Controls.Add(this.dataGridView1);
      this.tabPage1.Location = new System.Drawing.Point(4, 22);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage1.Size = new System.Drawing.Size(711, 490);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "Ports";
      this.tabPage1.UseVisualStyleBackColor = true;
      // 
      // dataGridView1
      // 
      this.dataGridView1.AllowUserToAddRows = false;
      this.dataGridView1.AllowUserToDeleteRows = false;
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
      this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dataGridView1.Location = new System.Drawing.Point(3, 3);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.RowHeadersVisible = false;
      this.dataGridView1.Size = new System.Drawing.Size(705, 484);
      this.dataGridView1.TabIndex = 0;
      // 
      // Column1
      // 
      this.Column1.DataPropertyName = "Name";
      this.Column1.Frozen = true;
      this.Column1.HeaderText = "Port Name";
      this.Column1.Name = "Column1";
      this.Column1.ReadOnly = true;
      // 
      // Column2
      // 
      this.Column2.DataPropertyName = "DataType";
      this.Column2.Frozen = true;
      this.Column2.HeaderText = "Port Type";
      this.Column2.Name = "Column2";
      this.Column2.ReadOnly = true;
      this.Column2.Width = 250;
      // 
      // Column3
      // 
      this.Column3.HeaderText = "Clock";
      this.Column3.Name = "Column3";
      this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      this.Column3.Width = 50;
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(this.comboBoxCtors);
      this.tabPage2.Controls.Add(this.label5);
      this.tabPage2.Location = new System.Drawing.Point(4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(711, 490);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "Constructors";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // comboBoxCtors
      // 
      this.comboBoxCtors.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBoxCtors.FormattingEnabled = true;
      this.comboBoxCtors.Location = new System.Drawing.Point(141, 20);
      this.comboBoxCtors.Name = "comboBoxCtors";
      this.comboBoxCtors.Size = new System.Drawing.Size(356, 21);
      this.comboBoxCtors.TabIndex = 1;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(32, 23);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(107, 13);
      this.label5.TabIndex = 0;
      this.label5.Text = "Choose Constructor:";
      // 
      // tabPage3
      // 
      this.tabPage3.Controls.Add(this.panel6);
      this.tabPage3.Controls.Add(this.panel5);
      this.tabPage3.Location = new System.Drawing.Point(4, 22);
      this.tabPage3.Name = "tabPage3";
      this.tabPage3.Size = new System.Drawing.Size(711, 490);
      this.tabPage3.TabIndex = 2;
      this.tabPage3.Text = "Parameters";
      this.tabPage3.UseVisualStyleBackColor = true;
      // 
      // panel6
      // 
      this.panel6.Controls.Add(this.tableLayoutPanel2);
      this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel6.Location = new System.Drawing.Point(0, 67);
      this.panel6.Name = "panel6";
      this.panel6.Size = new System.Drawing.Size(711, 423);
      this.panel6.TabIndex = 1;
      // 
      // tableLayoutPanel2
      // 
      this.tableLayoutPanel2.ColumnCount = 2;
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel2.Controls.Add(this.listViewParameters, 0, 0);
      this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 1, 0);
      this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 2;
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel2.Size = new System.Drawing.Size(711, 423);
      this.tableLayoutPanel2.TabIndex = 0;
      // 
      // listViewParameters
      // 
      this.listViewParameters.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4,
            this.columnHeader3});
      this.listViewParameters.Dock = System.Windows.Forms.DockStyle.Fill;
      this.listViewParameters.FullRowSelect = true;
      this.listViewParameters.Location = new System.Drawing.Point(3, 3);
      this.listViewParameters.Name = "listViewParameters";
      this.listViewParameters.Size = new System.Drawing.Size(517, 397);
      this.listViewParameters.TabIndex = 0;
      this.listViewParameters.UseCompatibleStateImageBehavior = false;
      this.listViewParameters.View = System.Windows.Forms.View.Details;
      this.listViewParameters.SelectedIndexChanged += new System.EventHandler(this.listViewParameters_SelectedIndexChanged);
      this.listViewParameters.DoubleClick += new System.EventHandler(this.listViewParameters_DoubleClick);
      // 
      // columnHeader1
      // 
      this.columnHeader1.Text = "Name";
      // 
      // columnHeader2
      // 
      this.columnHeader2.Text = "Type";
      // 
      // columnHeader4
      // 
      this.columnHeader4.Text = "Kind";
      // 
      // columnHeader3
      // 
      this.columnHeader3.Text = "Default Value";
      this.columnHeader3.Width = 352;
      // 
      // tableLayoutPanel4
      // 
      this.tableLayoutPanel4.ColumnCount = 1;
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel4.Controls.Add(this.buttonAddParameter, 0, 0);
      this.tableLayoutPanel4.Controls.Add(this.buttonRemoveParameter, 0, 1);
      this.tableLayoutPanel4.Controls.Add(this.buttonEditParameter, 0, 2);
      this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel4.Location = new System.Drawing.Point(526, 3);
      this.tableLayoutPanel4.Name = "tableLayoutPanel4";
      this.tableLayoutPanel4.RowCount = 3;
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tableLayoutPanel4.Size = new System.Drawing.Size(182, 397);
      this.tableLayoutPanel4.TabIndex = 1;
      // 
      // buttonAddParameter
      // 
      this.buttonAddParameter.Enabled = false;
      this.buttonAddParameter.Location = new System.Drawing.Point(3, 3);
      this.buttonAddParameter.Name = "buttonAddParameter";
      this.buttonAddParameter.Size = new System.Drawing.Size(75, 23);
      this.buttonAddParameter.TabIndex = 0;
      this.buttonAddParameter.Text = "Add...";
      this.buttonAddParameter.UseVisualStyleBackColor = true;
      this.buttonAddParameter.Click += new System.EventHandler(this.buttonAddParameter_Click);
      // 
      // buttonRemoveParameter
      // 
      this.buttonRemoveParameter.Enabled = false;
      this.buttonRemoveParameter.Location = new System.Drawing.Point(3, 135);
      this.buttonRemoveParameter.Name = "buttonRemoveParameter";
      this.buttonRemoveParameter.Size = new System.Drawing.Size(75, 23);
      this.buttonRemoveParameter.TabIndex = 1;
      this.buttonRemoveParameter.Text = "Remove...";
      this.buttonRemoveParameter.UseVisualStyleBackColor = true;
      this.buttonRemoveParameter.Click += new System.EventHandler(this.buttonRemoveParameter_Click);
      // 
      // buttonEditParameter
      // 
      this.buttonEditParameter.Location = new System.Drawing.Point(3, 267);
      this.buttonEditParameter.Name = "buttonEditParameter";
      this.buttonEditParameter.Size = new System.Drawing.Size(75, 23);
      this.buttonEditParameter.TabIndex = 2;
      this.buttonEditParameter.Text = "Edit...";
      this.buttonEditParameter.UseVisualStyleBackColor = true;
      this.buttonEditParameter.Click += new System.EventHandler(this.buttonEditParameter_Click);
      // 
      // panel5
      // 
      this.panel5.Controls.Add(this.buttonBrowseParamCallbackSource);
      this.panel5.Controls.Add(this.textBoxParamCallbackSource);
      this.panel5.Controls.Add(this.label9);
      this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel5.Location = new System.Drawing.Point(0, 0);
      this.panel5.Name = "panel5";
      this.panel5.Size = new System.Drawing.Size(711, 67);
      this.panel5.TabIndex = 0;
      // 
      // buttonBrowseParamCallbackSource
      // 
      this.buttonBrowseParamCallbackSource.Location = new System.Drawing.Point(361, 28);
      this.buttonBrowseParamCallbackSource.Name = "buttonBrowseParamCallbackSource";
      this.buttonBrowseParamCallbackSource.Size = new System.Drawing.Size(37, 23);
      this.buttonBrowseParamCallbackSource.TabIndex = 2;
      this.buttonBrowseParamCallbackSource.Text = "...";
      this.buttonBrowseParamCallbackSource.UseVisualStyleBackColor = true;
      this.buttonBrowseParamCallbackSource.Click += new System.EventHandler(this.buttonBrowseParamCallbackSource_Click);
      // 
      // textBoxParamCallbackSource
      // 
      this.textBoxParamCallbackSource.Location = new System.Drawing.Point(8, 30);
      this.textBoxParamCallbackSource.Name = "textBoxParamCallbackSource";
      this.textBoxParamCallbackSource.Size = new System.Drawing.Size(338, 21);
      this.textBoxParamCallbackSource.TabIndex = 1;
      this.textBoxParamCallbackSource.TextChanged += new System.EventHandler(this.textBoxParamCallbackSource_TextChanged);
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(5, 14);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(285, 13);
      this.label9.TabIndex = 0;
      this.label9.Text = "User-supplied Source File Containing Parameter Callbacks:";
      // 
      // header4
      // 
      this.header4.BackColor = System.Drawing.SystemColors.Control;
      this.header4.CausesValidation = false;
      this.header4.Description = "Choose a module from the list on the left and complete the parameter options by s" +
          "electing the appropriate tab";
      this.header4.Dock = System.Windows.Forms.DockStyle.Top;
      this.header4.Image = null;
      this.header4.Location = new System.Drawing.Point(0, 0);
      this.header4.Name = "header4";
      this.header4.Size = new System.Drawing.Size(863, 64);
      this.header4.TabIndex = 0;
      this.header4.Title = "sc_module Configuration";
      // 
      // wizardPageModuleSelect
      // 
      this.wizardPageModuleSelect.Controls.Add(this.panel3);
      this.wizardPageModuleSelect.Controls.Add(this.header3);
      this.wizardPageModuleSelect.Dock = System.Windows.Forms.DockStyle.Fill;
      this.wizardPageModuleSelect.IsFinishPage = false;
      this.wizardPageModuleSelect.Location = new System.Drawing.Point(0, 0);
      this.wizardPageModuleSelect.Name = "wizardPageModuleSelect";
      this.wizardPageModuleSelect.Size = new System.Drawing.Size(863, 580);
      this.wizardPageModuleSelect.TabIndex = 3;
      this.wizardPageModuleSelect.CloseFromNext += new Gui.Wizard.PageEventHandler(this.wizardPageModuleSelect_CloseFromNext);
      this.wizardPageModuleSelect.ShowFromNext += new System.EventHandler(this.wizardPageModuleSelect_ShowFromNext);
      // 
      // panel3
      // 
      this.panel3.Controls.Add(this.tableLayoutPanel3);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel3.Location = new System.Drawing.Point(0, 64);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(863, 516);
      this.panel3.TabIndex = 1;
      // 
      // tableLayoutPanel3
      // 
      this.tableLayoutPanel3.ColumnCount = 1;
      this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel3.Controls.Add(this.checkedListBoxModules, 0, 0);
      this.tableLayoutPanel3.Controls.Add(this.flowLayoutPanel1, 0, 1);
      this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel3.Name = "tableLayoutPanel3";
      this.tableLayoutPanel3.RowCount = 2;
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel3.Size = new System.Drawing.Size(863, 516);
      this.tableLayoutPanel3.TabIndex = 3;
      // 
      // checkedListBoxModules
      // 
      this.checkedListBoxModules.Dock = System.Windows.Forms.DockStyle.Fill;
      this.checkedListBoxModules.FormattingEnabled = true;
      this.checkedListBoxModules.Location = new System.Drawing.Point(3, 3);
      this.checkedListBoxModules.Name = "checkedListBoxModules";
      this.checkedListBoxModules.Size = new System.Drawing.Size(857, 468);
      this.checkedListBoxModules.TabIndex = 0;
      this.checkedListBoxModules.SelectedIndexChanged += new System.EventHandler(this.checkedListBoxModules_SelectedIndexChanged);
      // 
      // flowLayoutPanel1
      // 
      this.flowLayoutPanel1.AutoSize = true;
      this.flowLayoutPanel1.Controls.Add(this.buttonCheckAll);
      this.flowLayoutPanel1.Controls.Add(this.buttonUncheckAll);
      this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 484);
      this.flowLayoutPanel1.Name = "flowLayoutPanel1";
      this.flowLayoutPanel1.Size = new System.Drawing.Size(162, 29);
      this.flowLayoutPanel1.TabIndex = 1;
      // 
      // buttonCheckAll
      // 
      this.buttonCheckAll.Location = new System.Drawing.Point(3, 3);
      this.buttonCheckAll.Name = "buttonCheckAll";
      this.buttonCheckAll.Size = new System.Drawing.Size(75, 23);
      this.buttonCheckAll.TabIndex = 0;
      this.buttonCheckAll.Text = "Check All";
      this.buttonCheckAll.UseVisualStyleBackColor = true;
      this.buttonCheckAll.Click += new System.EventHandler(this.buttonCheckAll_Click);
      // 
      // buttonUncheckAll
      // 
      this.buttonUncheckAll.Location = new System.Drawing.Point(84, 3);
      this.buttonUncheckAll.Name = "buttonUncheckAll";
      this.buttonUncheckAll.Size = new System.Drawing.Size(75, 23);
      this.buttonUncheckAll.TabIndex = 1;
      this.buttonUncheckAll.Text = "Uncheck All";
      this.buttonUncheckAll.UseVisualStyleBackColor = true;
      this.buttonUncheckAll.Click += new System.EventHandler(this.buttonUncheckAll_Click);
      // 
      // header3
      // 
      this.header3.BackColor = System.Drawing.SystemColors.Control;
      this.header3.CausesValidation = false;
      this.header3.Description = "Check the sc_modules that you would like converted into a SoC Designer Component";
      this.header3.Dock = System.Windows.Forms.DockStyle.Top;
      this.header3.Image = null;
      this.header3.Location = new System.Drawing.Point(0, 0);
      this.header3.Name = "header3";
      this.header3.Size = new System.Drawing.Size(863, 64);
      this.header3.TabIndex = 0;
      this.header3.Title = "Choose Module(s) to be converted into SoC Designer Components";
      // 
      // wizardPageAnalyze
      // 
      this.wizardPageAnalyze.Controls.Add(this.panel2);
      this.wizardPageAnalyze.Controls.Add(this.header2);
      this.wizardPageAnalyze.Dock = System.Windows.Forms.DockStyle.Fill;
      this.wizardPageAnalyze.IsFinishPage = false;
      this.wizardPageAnalyze.Location = new System.Drawing.Point(0, 0);
      this.wizardPageAnalyze.Name = "wizardPageAnalyze";
      this.wizardPageAnalyze.Size = new System.Drawing.Size(863, 580);
      this.wizardPageAnalyze.TabIndex = 2;
      this.wizardPageAnalyze.CloseFromNext += new Gui.Wizard.PageEventHandler(this.wizardPageAnalyze_CloseFromNext);
      this.wizardPageAnalyze.CloseFromBack += new Gui.Wizard.PageEventHandler(this.wizardPageAnalyze_CloseFromBack);
      this.wizardPageAnalyze.ShowFromNext += new System.EventHandler(this.wizardPageAnalyze_ShowFromNext);
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.richTextBox1);
      this.panel2.Controls.Add(this.progressBar1);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(0, 64);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(863, 516);
      this.panel2.TabIndex = 1;
      // 
      // richTextBox1
      // 
      this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.richTextBox1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.richTextBox1.Location = new System.Drawing.Point(0, 0);
      this.richTextBox1.Name = "richTextBox1";
      this.richTextBox1.ReadOnly = true;
      this.richTextBox1.Size = new System.Drawing.Size(863, 493);
      this.richTextBox1.TabIndex = 1;
      this.richTextBox1.Text = "";
      // 
      // progressBar1
      // 
      this.progressBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.progressBar1.Location = new System.Drawing.Point(0, 493);
      this.progressBar1.Maximum = 10;
      this.progressBar1.Name = "progressBar1";
      this.progressBar1.Size = new System.Drawing.Size(863, 23);
      this.progressBar1.TabIndex = 0;
      // 
      // header2
      // 
      this.header2.BackColor = System.Drawing.SystemColors.Control;
      this.header2.CausesValidation = false;
      this.header2.Description = "The design is being analyzed for sc_module derived classes, please stand by.";
      this.header2.Dock = System.Windows.Forms.DockStyle.Top;
      this.header2.Image = null;
      this.header2.Location = new System.Drawing.Point(0, 0);
      this.header2.Name = "header2";
      this.header2.Size = new System.Drawing.Size(863, 64);
      this.header2.TabIndex = 0;
      this.header2.Title = "Analyzing Design";
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(863, 593);
      this.Controls.Add(this.wizard1);
      this.Name = "Form1";
      this.Text = "SystemC to SoC Designer Import Wizard";
      ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
      this.wizard1.ResumeLayout(false);
      this.wizardPageInit.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.panelSourceFiles.ResumeLayout(false);
      this.panelSourceFiles.PerformLayout();
      this.panelIncludeFiles.ResumeLayout(false);
      this.panelIncludeFiles.PerformLayout();
      this.wizardPageGenerate.ResumeLayout(false);
      this.panel7.ResumeLayout(false);
      this.wizardPageModules.ResumeLayout(false);
      this.panel4.ResumeLayout(false);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.ResumeLayout(false);
      this.tabControl1.ResumeLayout(false);
      this.tabPage1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      this.tabPage2.ResumeLayout(false);
      this.tabPage2.PerformLayout();
      this.tabPage3.ResumeLayout(false);
      this.panel6.ResumeLayout(false);
      this.tableLayoutPanel2.ResumeLayout(false);
      this.tableLayoutPanel4.ResumeLayout(false);
      this.panel5.ResumeLayout(false);
      this.panel5.PerformLayout();
      this.wizardPageModuleSelect.ResumeLayout(false);
      this.panel3.ResumeLayout(false);
      this.tableLayoutPanel3.ResumeLayout(false);
      this.tableLayoutPanel3.PerformLayout();
      this.flowLayoutPanel1.ResumeLayout(false);
      this.wizardPageAnalyze.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Gui.Wizard.Wizard wizard1;
    private Gui.Wizard.WizardPage wizardPageInit;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private Gui.Wizard.Header header1;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Button buttonBrowseIncludePaths;
    private System.Windows.Forms.Button buttonRemoveSource;
    private System.Windows.Forms.TextBox textBoxDesignFile;
    private System.Windows.Forms.TextBox textBoxIncludePaths;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button buttonBrowseDesignFile;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.ListBox listBoxSourceFiles;
    private System.Windows.Forms.Button buttonBrowseSourceFiles;
    private System.Windows.Forms.OpenFileDialog openFileDialogDesignFile;
    private System.Windows.Forms.ErrorProvider errorProvider1;
    private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    private Gui.Wizard.WizardPage wizardPageAnalyze;
    private System.Windows.Forms.Panel panel2;
    private Gui.Wizard.Header header2;
    private System.Windows.Forms.ProgressBar progressBar1;
    private System.Windows.Forms.RichTextBox richTextBox1;
    private Gui.Wizard.WizardPage wizardPageModuleSelect;
    private System.Windows.Forms.Panel panel3;
    private Gui.Wizard.Header header3;
    private System.Windows.Forms.Button buttonUncheckAll;
    private System.Windows.Forms.Button buttonCheckAll;
    private System.Windows.Forms.CheckedListBox checkedListBoxModules;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
    private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox textBoxDefinitions;
    private Gui.Wizard.WizardPage wizardPageModules;
    private Gui.Wizard.Header header4;
    private System.Windows.Forms.Panel panel4;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.TreeView treeViewModules;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.TabPage tabPage3;
    private System.Windows.Forms.DataGridView dataGridView1;
    private System.Windows.Forms.ComboBox comboBoxCtors;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox textBoxHeaderGuard;
    private System.Windows.Forms.Button buttonRemoveIncludeFile;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.ListBox listBoxIncludeFiles;
    private System.Windows.Forms.Button buttonBrowseIncludeFiles;
    private System.Windows.Forms.Label label8;
    private Gui.Wizard.WizardPage wizardPageGenerate;
    private Gui.Wizard.Header header5;
    private System.Windows.Forms.Panel panel5;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Button buttonBrowseParamCallbackSource;
    private System.Windows.Forms.TextBox textBoxParamCallbackSource;
    private System.Windows.Forms.Panel panel6;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    private System.Windows.Forms.ListView listViewParameters;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
    private System.Windows.Forms.Button buttonAddParameter;
    private System.Windows.Forms.Button buttonRemoveParameter;
    private System.Windows.Forms.ColumnHeader columnHeader1;
    private System.Windows.Forms.ColumnHeader columnHeader2;
    private System.Windows.Forms.ColumnHeader columnHeader3;
    private System.Windows.Forms.ColumnHeader columnHeader4;
    private System.Windows.Forms.Button buttonEditParameter;
    private System.Windows.Forms.Panel panel7;
    private System.Windows.Forms.RichTextBox richTextBox2;
    private System.Windows.Forms.ProgressBar progressBar2;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.TextBox textBoxLinkerDirs;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.TextBox textBoxLinkerLibs;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.ComboBox comboBoxVC;
    private System.Windows.Forms.Label labelVC;
    private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
    private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    private System.Windows.Forms.DataGridViewCheckBoxColumn Column3;
    private System.Windows.Forms.ComboBox comboBoxTarget;
    private System.Windows.Forms.Label labelTarget;
    private System.Windows.Forms.TextBox textBoxOutputDir;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.Button buttonBrowseOutputDir;
    private System.Windows.Forms.Panel panelSourceFiles;
    private System.Windows.Forms.Panel panelIncludeFiles;
  }
}

