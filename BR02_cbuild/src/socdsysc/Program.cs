﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;

using systemc;
using CPPStuff;

namespace analyze
{
  class OutputFiles
  {
    public List<string> Makefiles { get; set; }
    public List<string> ConfFiles { get; set; }

    public OutputFiles()
    {
      Makefiles = new List<string>();
      ConfFiles = new List<string>();
    }
  }

  [Serializable]
  public class Modules
  {
    [XmlArrayItemAttribute("Module", typeof(string))]
    public List<string> XmlFiles { get; set; }
    public Modules()
    {
      XmlFiles = new List<string>();
    }
  }

 
  class Program
  {
    static bool runningWithMono()
    {
      Type mt = Type.GetType("Mono.Runtime");
      return mt != null;
    }

    static int Main(string[] programArgs)
    {
      CodeGeneratorArguments cargs = new CodeGeneratorArguments();

      if (programArgs.Length == 0)
      {
        Console.WriteLine("Usage:");
        Console.WriteLine("socdsyscimport -design [tbName.cpp|.h] [DesignFile.cpp] [DesignFile.cpp...] [options]");
        Console.WriteLine("\nOPTIONS\n");
        Console.WriteLine("  -design testBenchSource.[cpp|h]");
        Console.WriteLine("    Specifies the testbench design which references all other sc_modules to be imported");
       
        Console.WriteLine();

        Console.WriteLine("  -gccxml program");
        Console.WriteLine("    Specifies a path to the gccxml program");
        Console.WriteLine();

        Console.WriteLine("  -templateDir dir");
        Console.WriteLine("    Specifies a path to the template (.stg files)");
        Console.WriteLine();
        
        Console.WriteLine("  [DesignFile.cpp]");
        Console.WriteLine("    Additional source files to be compiled into the model");
        Console.WriteLine();

        Console.WriteLine("  -I dir");
        Console.WriteLine("    Specifies an include path to be passed to the C++ compiler, any number of -I or -D");
        Console.WriteLine("    switches are allowed");
        Console.WriteLine();

        Console.WriteLine("  -D value");
        Console.WriteLine("    Specifies value to be passed the C++ compiler as a -D <value>");
        Console.WriteLine();

        Console.WriteLine("  -L dir");
        Console.WriteLine("    Specifies a linker include path to be passed to the linker, any number of -L or -l");
        Console.WriteLine("    switches are allowed");
        Console.WriteLine();

        Console.WriteLine("  -l libname");
        Console.WriteLine("    Specifies value to be passed the linkder as a -l libname value");
        Console.WriteLine();

        Console.WriteLine("  -include filename");
        Console.WriteLine("    Specifies additional include file(s) before including wrapper generated header file");
        Console.WriteLine("    Switch may be repeated for each file to be included.");
        Console.WriteLine();

        Console.WriteLine("  -headerGuard definition");
        Console.WriteLine("    Specifies value to be wrapped via #ifndef <definition> before including header files");
        Console.WriteLine();

        Console.WriteLine("  -argFile arguments.xml");
        Console.WriteLine("    Specifies argument file to be processed");
        Console.WriteLine();

        
        return 1;
      }
      string carbonBin = Path.Combine(System.Environment.GetEnvironmentVariable("CARBON_HOME"), "Win/gccxml/bin");
      if (runningWithMono())
        carbonBin = Path.Combine(System.Environment.GetEnvironmentVariable("CARBON_HOME"), "Linux/gccxml/bin");

      string gccxml_program = Path.Combine(carbonBin, "gccxml");

      //Console.WriteLine("gccxmlprog: " + gccxml_program);

      processArgs(cargs, programArgs, ref gccxml_program);

      Directory.CreateDirectory("Carbon");

      if (String.IsNullOrEmpty(cargs.ArgFile))
        return analyzeDesign(cargs, gccxml_program);
      else // Generate from argfile
      {
        string argFile = cargs.ArgFile;
        XmlSerializer serializer = new XmlSerializer(typeof(ModuleConfigurations));

        Console.WriteLine("Reading Arg File...{0}", argFile);
        ModuleConfigurations configs = null;
        using (TextReader reader = new StreamReader(argFile))
        {
          configs = (ModuleConfigurations)serializer.Deserialize(reader);
          reader.Close();
        }

        addArguments(configs, cargs);

        if (cargs.ReAnalyze)
          analyzeDesign(cargs, gccxml_program);
        processXMLFile("Carbon/out.xml", configs, cargs);
        return 0;
      }

      return -1;
    }

    static public string ExpandString(string inStr)
    {
      string temp = inStr;

      if (inStr.Contains('$'))
      {
        Regex exp1 = new Regex(@"\$\((\w+)\)");
        MatchCollection mc = exp1.Matches(temp);
        if (mc.Count > 0)
        {
          foreach (Match m in mc)
          {
            Capture c = m.Groups[1].Captures[0];
            string evar = Environment.GetEnvironmentVariable(c.Value);
            if (evar != null)
            {
              string newtemp = temp.Replace(m.Value, evar);
              temp = newtemp;
            }
          }
        }

        Regex exp2 = new Regex(@"\$\{(\w+)\}");
        mc = exp2.Matches(temp);
        if (mc.Count > 0)
        {
          foreach (Match m in mc)
          {
            Capture c = m.Groups[1].Captures[0];
            string evar = Environment.GetEnvironmentVariable(c.Value);
            if (evar != null)
            {
              string newtemp = temp.Replace(m.Value, evar);
              temp = newtemp;
            }
          }

          inStr = Environment.ExpandEnvironmentVariables(temp);
        }
      }

      if (temp.Contains(" ") && !temp.StartsWith("\""))
        temp = "\"" + temp + "\"";

      if (runningWithMono())
        return temp.Replace("\\", "/");
      else
        return temp.Replace("/", "\\");
      
    }

    private static int analyzeDesign(CodeGeneratorArguments cargs, string gccxml_program)
    {
      List<string> gccxmlArgs = new List<string>();

      gccxmlArgs.Add(cargs.DesignFile);

      foreach(string arg in cargs.Defines)
        gccxmlArgs.Add(string.Format("-D{0}", ExpandString(arg)));

      foreach(string arg in cargs.IncludePaths)
        gccxmlArgs.Add(string.Format("-I{0}", ExpandString(arg)));

      foreach(string arg in cargs.CxxFlags)
        gccxmlArgs.Add(arg);

      // SocDesigner sets this when WIN32 is defined.
      if (!runningWithMono())
        gccxmlArgs.Add("-DSC_MX_MAKE_LIB");

      gccxmlArgs.Add("-fxml=Carbon/out.xml");

      Process p = new Process();

      p.EnableRaisingEvents = true;

      p.Exited += new EventHandler(p_Exited);

      if (!runningWithMono() && !gccxml_program.StartsWith("\""))
        gccxml_program = String.Format("\"{0}\"", gccxml_program);

      p.StartInfo.FileName = gccxml_program;
      p.StartInfo.CreateNoWindow = true;
      p.StartInfo.UseShellExecute = false;

      if (!runningWithMono())
      {
        p.StartInfo.RedirectStandardError = true;
        p.StartInfo.RedirectStandardOutput = true;

        p.OutputDataReceived += new DataReceivedEventHandler(p_OutputDataReceived);
        p.ErrorDataReceived += new DataReceivedEventHandler(p_OutputDataReceived);
      }

      // If user hasn't supplied, force it
      string gccXmlCompiler = System.Environment.GetEnvironmentVariable("GCCXML_COMPILER");
      if (runningWithMono() && (gccXmlCompiler == null || gccXmlCompiler.Length == 0))
      {
        string carbonGcc = Path.Combine(System.Environment.GetEnvironmentVariable("CARBON_HOME"), "Linux/gcc/bin/g++");
        Console.WriteLine("GCCXML_COMPILER=" + carbonGcc);
        p.StartInfo.EnvironmentVariables.Add("GCCXML_COMPILER", carbonGcc);
      }

      p.StartInfo.Arguments = String.Join(" ", gccxmlArgs.ToArray());

      Console.WriteLine(String.Format("{0} {1}", p.StartInfo.FileName, p.StartInfo.Arguments));

      Console.Write("Analyzing...");
      if (p.Start())
      {
        if (!runningWithMono())
        {
          p.BeginErrorReadLine();
          p.BeginOutputReadLine();
        }

        while (!p.HasExited)
        {
          p.WaitForExit(500);
          Console.WriteLine(".");
        }
      }
      else
        Console.WriteLine("Failed to launch process");

      Console.WriteLine("");

      return p.ExitCode;
    }

    static void p_OutputDataReceived(object sender, DataReceivedEventArgs e)
    {
      Console.WriteLine(e.Data);
    }


    static void p_Exited(object sender, EventArgs e)
    {
    }

    //
    // Turn arguments from configs into cargs
    //
    private static void addArguments(ModuleConfigurations configs, CodeGeneratorArguments cargs)
    {
      char[] seps = { ';' };

      cargs.DesignFile = configs.DesignFile;
      
      // Source Files
      foreach (string arg in configs.SourceFiles)
        cargs.SourceFiles.Add(arg);

      // Common Include Files
      foreach (string arg in configs.IncludeFiles)
        cargs.IncludeFiles.Add(arg);

      // Definitions
      foreach (string arg in configs.Definitions.Split(seps))
        cargs.Defines.Add(arg);

      // Includes
      foreach (string arg in configs.IncludePaths.Split(seps))
        cargs.IncludePaths.Add(arg);

      // Linker Flags
      foreach (string arg in configs.LinkerFlags.Split(seps))
        cargs.LinkFlags.Add(arg);

      // Linker Paths
      foreach (string arg in configs.LinkerPaths.Split(seps))
        cargs.LinkPaths.Add(arg);

      // Header Guard
      cargs.HeaderGuard = configs.HeaderGuard;

    }

    private static void processXMLFile(string xmlFile, ModuleConfigurations configs, CodeGeneratorArguments cargs)
    {
      CPPInfo cppinfo;

      XmlSerializer serializer = new XmlSerializer(typeof(GCCXML.GCC_XML));

      Console.WriteLine("Begin code Generation...");
      GCCXML.GCC_XML cppStructure;
      using (TextReader reader = new StreamReader(xmlFile))
      {
        cppStructure = (GCCXML.GCC_XML)serializer.Deserialize(reader);
        reader.Close();
      }

      cppinfo = new CPPInfo(cppStructure);
      OutputFiles outFiles = new OutputFiles();
      if (cppinfo.Classes != null)
      {
        List<CPPClassOrStruct> modules = new List<CPPClassOrStruct>();
        foreach (CPPClassOrStruct c in cppinfo.ClassesAndStructs)
        {
          if (c.Name != "sc_event_queue" && c.DerivesFrom("sc_module"))
          {
            string className = fullClassName(c);

            ModuleConfiguration mc = configs.findModule(className);
            if (mc != null)
            {
              modules.Add(c);
              Console.WriteLine("Generating SystemC Wrapper for sc_module: " + c.Name);
              generateSystemC(c, cargs, outFiles, mc);
            }
          }
        }

        string templateDir = cargs.TemplateDirectory;
        if (String.IsNullOrEmpty(templateDir))
        {
          templateDir = Path.Combine(System.Environment.GetEnvironmentVariable("CARBON_HOME"), "lib");
          templateDir = Path.Combine(templateDir, "templates");
        }

        cargs.TemplateDirectory = templateDir;
        cargs.OutputDirectory = @"Carbon";

        CodeGenerator cg = new CodeGenerator(cargs);

        if (runningWithMono())
        {
          cg.generateConfFileTop("top", outFiles.ConfFiles);
          cg.generateMakeScript("Makefile", outFiles.Makefiles);
        }
        else
        {
          List<VcProject> projects = new List<VcProject>();
          // Make .vcproj for each module
          foreach (CPPClassOrStruct c in modules)
          {
            ModuleConfiguration mc = configs.findModule(c.Name);
            cargs.SystemCModule = SystemCModule.CreateFromModule(c, mc);
            cg = new CodeGenerator(cargs);
            cargs.ModConfig = mc;
            string projGuid = "";
            string vcprojFileName = cg.generateVcprojFile(out projGuid);
            Console.WriteLine("Wrote " + vcprojFileName);
            VcProject vcp = new VcProject();
            vcp.ProjectGUID = projGuid;
            vcp.ProjectFile = vcprojFileName;
            vcp.ProjectName = mc.ClassName;
            projects.Add(vcp);
          }

          cg.generateSolutionFile(projects);

        }
      }
    }

    //
    // Return fully specified class name
    //
    private static string fullClassName(CPPClassOrStruct c)
    {
      string className = null;
      if (c.Namespace.Name == "::")
        className = c.Name;
      else
        className = String.Format("{0}::{1}", c.Namespace.Name, c.Name);
      return className;
    }

    private static void generateSystemC(CPPClassOrStruct c, CodeGeneratorArguments cargs, OutputFiles outFiles, ModuleConfiguration mc)
    {
      string templateDir = cargs.TemplateDirectory;
      if (String.IsNullOrEmpty(templateDir))
      {
        templateDir = Path.Combine(System.Environment.GetEnvironmentVariable("CARBON_HOME"), "lib");
        templateDir = Path.Combine(templateDir, "templates");
      }

      cargs.TemplateDirectory = templateDir;
      cargs.OutputDirectory = @"Carbon";

      Directory.CreateDirectory("Carbon");

      SystemCModule sysc = SystemCModule.CreateFromModule(c, mc);
      cargs.SystemCModule = sysc;
      cargs.ModConfig = mc;

      CodeGenerator cg = new CodeGenerator(cargs);
      cg.generateHeaderFile(c);
      cg.generateCppFile(c);
      cg.generateParamCallbackHeader();
      outFiles.Makefiles.Add(cg.generateMakefile());
      outFiles.ConfFiles.Add(cg.generateConfFile());
    }

    private static void processArgs(CodeGeneratorArguments cargs, string[] args, ref string gccxml)
    {
      for (int i = 0; i < args.Length; i++)
      {
        string arg = args[i];

        switch (arg)
        {
          case "-L":
            if (i + 1 == args.Length) throw new Exception("-L missing argument");   
            cargs.LinkPaths.Add(args[++i]);
            continue;
           
          case "-l":
            if (i + 1 == args.Length) throw new Exception("-l missing argument");
            cargs.LinkFlags.Add(args[++i]);
            continue;
          
          case "-D":
            if (i + 1 == args.Length) throw new Exception("-D missing argument");
            cargs.Defines.Add(args[++i]);
            continue;

          case "-I":
            if (i + 1 == args.Length) throw new Exception("-I missing argument");
            string nextArg = args[++i];
            if (nextArg.Contains(" "))
              nextArg = "\"" + nextArg + "\"";
            cargs.IncludePaths.Add(nextArg);
            continue;

          case "-analyze":
            cargs.ReAnalyze = true;
            continue;

          case "-design":
            if (i + 1 == args.Length) throw new Exception("-design missing argument");
            cargs.DesignFile = args[++i];
            continue;
          
          case "-headerGuard":
            if (i + 1 == args.Length) throw new Exception("-headerGuard missing argument");
            cargs.HeaderGuard = args[++i];
            continue;
          
          case "-argFile":
            if (i + 1 == args.Length) throw new Exception("-argFile missing argument");
            cargs.ArgFile = args[++i];
            continue;

          case "-msvc":
            if (i + 1 == args.Length) throw new Exception("-msvc missing argument");
            string msvcArg = args[++i];
            switch (msvcArg.ToLower())
            {
              case "vs2005": cargs.VSVersion = VisualStudio.VisualStudioVersion.VS2005; break;
              case "vs2003": cargs.VSVersion = VisualStudio.VisualStudioVersion.VS2003; break;
            }
            continue;

          case "-include":
            if (i + 1 == args.Length) throw new Exception("-include missing argument");
            string incArg = args[++i];
            if (incArg.Contains(" "))
              incArg = "\"" + incArg + "\"";
            cargs.IncludeFiles.Add(incArg);
            continue;

          case "-templateDir":
            if (i + 1 == args.Length) throw new Exception("-templateDir missing argument");
            cargs.TemplateDirectory = args[++i];
            continue;

          case "-gccxml":
            if (i + 1 == args.Length) throw new Exception("-gccxml missing argument");
            gccxml = args[++i];
            continue;
        }

        if (arg.Length > 1)
        {
          switch (arg.Substring(0, 2))
          {
            case "-L":
              cargs.LinkPaths.Add(arg.Substring(2));
              continue;
            
            case "-l":
              cargs.LinkFlags.Add(arg.Substring(2));
              continue;
            
            case "-D":
              cargs.Defines.Add(arg.Substring(2));
              continue;
            
            case "-I":
              cargs.IncludePaths.Add(arg.Substring(2));
              continue;
          }
        }

        if (arg.StartsWith("--"))
          cargs.GccxmlArgs.Add(arg);
        else
          cargs.SourceFiles.Add(arg);
      }
    }
  }
}
