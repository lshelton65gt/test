// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "verific2nucleus/VerificVhdlDesignWalker.h"    // Visitor base class definition
#include "verific2nucleus/VerificNucleusBuilder.h"      // Nucleus builder class definition
#include "verific2nucleus/V2NDesignScope.h"         // Scope class definition
#include "verific2nucleus/Verific2NucleusUtilities.h"   // Utilities for building nucleus
#include "verific2nucleus/VerificDesignWalkerCB.h" // Included for MACRO VHDL_VISIT_CALL

#include "Array.h"              // Make dynamic array class Array available
#include "Strings.h"            // A string utility/wrapper class
#include "Message.h"            // Make message handlers available, not used in this example

#include "VhdlUnits.h"          // Definition of a libraries and design units
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations
#include "VhdlExpression.h"     // Definitions of all vhdl expressions
#include "VhdlStatement.h"      // Definitions of all vhdl statements
#include "VhdlIdDef.h"          // Definitions of all vhdl identifiers
#include "VhdlName.h"           // Definitions of all vhdl names
#include "VhdlConfiguration.h"  // Definitions of all vhdl configurations
#include "VhdlSpecification.h"  // Definitions of all vhdl specifications
#include "VhdlMisc.h"           // Definitions of miscellaneous vhdl constructrs
#include "VhdlScope.h"
#include "vhdl_tokens.h"
#include "veri_tokens.h"
#include "vhdl_file.h"
#include "veri_file.h"
#include "VhdlValue_Elab.h"

#include "nucleus/NUBase.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUSysTask.h"
#include "util/AtomicCache.h"
#include "util/CarbonAssert.h"
#include "util/UtPair.h"
#include "compiler_driver/VhdlPortTypeMap.h"

#include <iostream>

//#define DEBUG_VERIFIC_VHDL_WALKER 1

namespace verific2nucleus {

using namespace Verific;

/**
 *@VhdlIdDef
 * This needs to be reworked to use VhdlValue*
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlIdDef, node)
{
    DEBUG_TRACE_WALKER("VhdlIdDef", node);
    if (hasError()) {
        return ;
    }
    SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    if (node.IsType()) {
        return ;
    }

    if (node.Type() && (UtString("severity_level") == UtString(node.Type()->Name()))) {
        mNucleusBuilder->getMessageContext()->Verific2NUNotSynthesisableType(&source_locator, "SEVERITY_LEVEL");
    }
    if (node.Type() && node.Type()->IsStdTimeType()) {
        mNucleusBuilder->getMessageContext()->Verific2NUTimeIsUnsupported(&source_locator);
        mCurrentScope->sValue(0);
        setError(true);
        return;
    }
    if (node.Type()->IsPhysicalType()) {
        mNucleusBuilder->getMessageContext()->Verific2NUPhysicalTypeNotSupported(&source_locator, node.Name());
        mCurrentScope->sValue(0);
        setError(true);
        return;
    }
    const char* name = node.OrigName();
    StringAtom* atom = mNucleusBuilder->gCache()->intern(name);
    //create net
    NetFlags flags = sFlags(node);
    NUScope* declaration_scope = getDeclarationScope();
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(declaration_scope, source_locator, "Unable to get declaration scope");
    // Push the VhdlIdDef* onto the Id path; it will be used to register the nucleus
    // object for later lookup.
    mVerificToNucleusObject.pushPathId(&node);
    if (processScalarDef(node, atom, flags, declaration_scope, source_locator)) {
    } else if (processCompositeDef(node, atom, flags, declaration_scope, source_locator)) {
    } else if (processAccessDef(node, atom, flags, declaration_scope, source_locator)) {
    } else if (processFileDef(node, atom, flags, declaration_scope, source_locator)) {
    }
    mVerificToNucleusObject.popPathId();
//    V2N_INFO_ASSERT(false, node, "not supported VhdlIdDef");
}

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlFileOpenInfo, node)
{
    DEBUG_TRACE_WALKER("VhdlFileOpenInfo", node);
    SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    NUConst* mode = 0;
    NUExprVector expr_vector;

    VhdlStringLiteral* sLit = static_cast<VhdlStringLiteral*>(node.GetFileLogicalName());
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(sLit, src_loc, "Invalid file logical name node");
    UtString tmp(sLit->Name());
    UtString str(tmp, 1, tmp.size() - 2); // remove quotes
    UtString binary_str;
    UInt32 bit_width = composeBinStringFromString(str.c_str(), &binary_str, true);
    NUConst* file_name = NUConst::createKnownConst(binary_str, bit_width, true, src_loc);
    file_name->resize(file_name->determineBitSize());
    expr_vector.push_back(file_name);

    UInt32 mode_i=0;
    if (node.GetFileOpen()) { 
        //VHDL 93 open mode
        VhdlIdRef* idRef = static_cast<VhdlIdRef*>(node.GetFileOpen());
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(idRef, src_loc, "Invalid file open mode node");
        VhdlValue* value = idRef->Evaluate(0, mDataFlow, 0);
        mValues.push_back(value);
        UInt32 bit_width;
        UtString mode_str;
        if (0 == value->Integer()) {
            //read mode
            mode_i = 0;
            bit_width = composeBinStringFromString("r", &mode_str, false);
        } else if (1 == value->Integer()) {
            //write mode
            mode_i = 1;
            bit_width = composeBinStringFromString("w", &mode_str, false);
        } else {
            V2N_INFO_ASSERT(2 == value->Integer(), node, "Possible file open modes are: read = 0, write = 1, append = 2, in append case value should be 2");
            //append mode
            mode_i = 2;
            bit_width = composeBinStringFromString("a", &mode_str, false);
        }
        mode = NUConst::createKnownConst(mode_str, bit_width, true, src_loc);
        mode->resize(mode->determineBitSize());
        expr_vector.push_back(mode);
        delete value;
    } else if (node.GetOpenMode()) {
        //VHDL 87 file open mode
        UInt32 bit_width=0;
        UtString mode_str;
        switch ( node.GetOpenMode() )
        {
            case VHDL_in:
                mode_i = 0;
                break;
            case VHDL_out:
                mode_i = 1;
                break;
            case VHDL_inout:
            case VHDL_buffer:
            case VHDL_linkage:
            default: V2N_INFO_ASSERT(2 == node.GetOpenMode(), node, 
                             "Unexpected VHDL-87 file mode specification: read = 0, write = 1");
            break;
        }
        if (mode_i == 0)
        {
            // read mode
            bit_width = composeBinStringFromString("r", &mode_str,false);
        }
        else if (mode_i == 1)
        {
            // write mode
            bit_width = composeBinStringFromString("w", &mode_str,false);
        }
        mode = NUConst::createKnownConst(mode_str, bit_width, true, src_loc);
        mode->resize(mode->determineBitSize());
        expr_vector.push_back(mode);
       // V2N_INFO_ASSERT(false, node, "currently not supported open mode for VHDL 87");
   }
   NUModule* module = getModule();
   NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, src_loc, "Unable to get module");

   StringAtom* file_open_sys = module->gensym("file_open");
   NUNet* net = dynamic_cast<NUNet*>(mCurrentScope->gValue());
   NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(net, src_loc, "Invalid NUNet object");
   NUFOpenSysTask* systask = new NUFOpenSysTask(file_open_sys, net, 0, expr_vector,
                                                module, mNucleusBuilder->gNetRefFactory(),
                                                false, false, (0 == mode_i), src_loc);
   NUBlock* block = initBlockForFileIO(src_loc);
   NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(block, src_loc, "Failed to get inital block");
   mNucleusBuilder->aStmtToBlock(systask, block);
}

//create inital block and add to context for FILE IO
NUBlock* VerificVhdlDesignWalker::initBlockForFileIO(const SourceLocator& src_loc)
{
    // FD thinks: I'm finding below initial block (top block) get part very tricky. Why do we need this kind of getting ? (It seems this has place to be refactored or at least detailed explanation should be added in place of this function call).
    NUBlock* block = 0;
    NUScope* unit_scope = getUnitScope();
    if (NUScope::eTask == unit_scope->getScopeType()) {
        //if current scope type is TASK it means current scope owner is
        //NUTask object and we can use static_cast instead of 
        //dynamic_cast because of dynamic_cast more expensive
        NUTask* task = dynamic_cast<NUTask*>(unit_scope);
        NUCLEUS_CONSISTENCY(task, src_loc, "Invalid NUTask object");
        block = dynamic_cast<NUBlock*>(task->getStmt());
        NUCLEUS_CONSISTENCY(block, src_loc, "Invalid NUBlock object");
    } else {
        NUModule* module = dynamic_cast<NUModule*>(unit_scope);
        NUCLEUS_CONSISTENCY(module, src_loc, "Invalid NUModule object");
        NUInitialBlockList initial_list;
        module->getInitialBlocks(&initial_list);
        block = 0;
        if (initial_list.empty()) {
            block = mNucleusBuilder->cBlock(module, src_loc);
            NUScope* parent_block_scope = getBlockScope();
            NUCLEUS_CONSISTENCY(parent_block_scope, src_loc, "Unable to get block scope");
            NUBase* initial = new NUInitialBlock(parent_block_scope->gensym("initialBlock"), block, mNucleusBuilder->gNetRefFactory(), src_loc);
            module->addInitialBlock(static_cast<NUInitialBlock*>(initial));
        } else {
            NUCLEUS_CONSISTENCY(1 == initial_list.size(), src_loc, "should exists only 1 initial block");
            NUInitialBlock* initial_block = initial_list.front();
            NUCLEUS_CONSISTENCY(initial_block, src_loc, "invalid initial block");
            block = initial_block->getBlock();
        }
    }
    return block;
}


//**************** Helper methods for implementing NUNets **********************//


//RJC RC#12 The orginization of this is much improved over version I saw at Revision 532 but I am still not sure where the support for 3d arrays should be handled, would it be done here?
//ADDRESSED
//EG you right Rich it should be done here
//3d nets implemented in constrainedArray and unconstrainedArray
    //Type decls have relationship as follows
    //
    //                   +- Integer    --+
    //                   |               |- Discrete
    //                   +- Enumeration--+
    //           Scalar -|
    //                   +- Physical --+
    //                   |             |- Numeric
    //                   +- Floating --+
    //
    // 
    //                                 +-Constraint
    //                      +- Array - | 
    //           Composite -|          +-Unconstraint
    //                      +- Record
    //
    //           Access
    //
    //           File
    //

// Steve Lim 2013-11-08 Replaced calls of create2DNet and create3DNet with createMultiDimNet
bool VerificVhdlDesignWalker::constrainedArray(VhdlIdDef& node, StringAtom*& netName, NetFlags& flags, NUScope*& scope, SourceLocator& src_loc)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "constrainedArray" << std::endl;
#endif
    NUNet* net = 0;
    if (1 == node.Constraint()->ElementConstraint()->NumBitsOfRange() &&
        (!node.Constraint()->ElementConstraint()->ElementConstraint()) &&
        (!node.ElementType()->IsIntegerType()) &&
        (!node.ElementType()->IsRecordType())) {
        VectorNetFlags vflags(eNoneVectorNet);

        if (UtString("std_ulogic") == UtString(node.IndexType(0)->Name())) {
            mNucleusBuilder->getMessageContext()->Verific2NUStdLogicAsArrayIndex(&src_loc);

            //std_ulogic defined as
            // type std_ulogic is
            //('U', 'X', '0', '1', 'Z', 'W', 'L', 'H', '-')
            ConstantRange index_cr(0, 8);
            net = new NUVectorNet(netName, index_cr, flags, vflags, scope, src_loc);
        } else {
            //pre last condition for small_int
            //and last one is for array of record elements
            ConstantRange index_cr(node.Constraint()->IndexConstraint()->Left()->Integer(),
                                   node.Constraint()->IndexConstraint()->Right()->Integer());
            net = new NUVectorNet(netName, index_cr, flags, vflags, scope, src_loc);
        }
    } else {
        //VhdlConstraint* idx_constraint = node.Type()->Constraint()->IndexConstraint();
        //FD thinks : we need deep element type (in case of multidimensional array (number_of_dims > 1))
        VhdlIdDef* elem_type = node.Type()->ElementType();
        if (containsRecords(node.Type())) {
           net = createAndRegisterCompositeNet(node, netName, scope, src_loc);
        } else if (node.DeepDimension() > 1) {
           net = createMultiDimNet(node, netName, flags, scope, src_loc);
        } else if (elem_type->IsScalarType()) {
           net = createNetForScalarType(node, src_loc, netName, flags, scope);
           V2N_INFO_ASSERT(net, node, "failed to create NUBitNet");
        } else {
           V2N_INFO_ASSERT(false, node, "not supported yet");
        }
    }
    V2N_INFO_ASSERT(net, node, "failed to create NUNet");
    mCurrentScope->sValue(net);
    registerNucleusObject(net);
    return true;
}

// Steve Lim 2013-11-08
// The alleged 3D handling looks erroneous in determining which is the element.
// The IndexType() is currently wrongly returning "integer" for all dimensions.                  
NUNet* 
VerificVhdlDesignWalker::createNetForScalarType(VhdlIdDef& node, SourceLocator& src_loc, StringAtom*& netName, NetFlags& flags, NUScope*& scope)
{
  if (node.Constraint()->ElementConstraint()->ElementConstraint() &&
      node.Type()->IndexType(0) &&
      node.Type()->IndexType(1)) { // Steve Lim 2013-11-08: If this is 3D where is the third index type?
    //3D net
    VectorNetFlags vflags(eNoneVectorNet);
    ConstantRange* element_cr = new ConstantRange(node.Constraint()->ElementConstraint()->NumBitsOfRange() - 1, 0);
    UtString errMsg;
    errMsg << "Invalid element constraint for array : line" << src_loc.getLine();
    V2N_INFO_ASSERT(element_cr, node, errMsg.c_str());
    ConstantRange* index_0_cr = new ConstantRange(node.Type()->IndexType(0)->Constraint()->Left()->Integer(),
        node.Type()->IndexType(0)->Constraint()->Right()->Integer());
    errMsg = "";
    errMsg << "Invalid first index constraint for 2D array : line" << src_loc.getLine();
    V2N_INFO_ASSERT(index_0_cr, node, errMsg.c_str());
    ConstantRange* index_1_cr = new ConstantRange(node.Type()->IndexType(1)->Constraint()->Left()->Integer(),
        node.Type()->IndexType(1)->Constraint()->Right()->Integer());
    errMsg = "";
    errMsg << "Invalid second index constraint for 2D array : line" << src_loc.getLine();
    V2N_INFO_ASSERT(index_1_cr, node, errMsg.c_str());
    ConstantRangeArray dimArray;
    dimArray.push_back(element_cr);
    dimArray.push_back(index_1_cr);
    dimArray.push_back(index_0_cr); // Steve Lim 2013-11-09: This is hardcoded to a dimension limit
                                    // If this is 3D handling where is the third index constraint?
    flags = NetRedeclare(flags, eDMReg2DNet); 
    return new NUMemoryNet(netName, 1, &dimArray, flags, vflags, scope, src_loc);
  } else {
    ConstantRange* element_cr = new ConstantRange(node.Constraint()->ElementConstraint()->NumBitsOfRange() - 1, 0);
    ConstantRange* index_cr = new ConstantRange(node.Constraint()->IndexConstraint()->Left()->Integer(), node.Constraint()->IndexConstraint()->Right()->Integer());
    VectorNetFlags vflags = node.Constraint()->ElementConstraint()->IsSignedRange() ?  VectorNetFlags(eSignedSubrange) : VectorNetFlags(eUnsignedSubrange);
    ConstantRangeArray dimArray;
    dimArray.push_back(element_cr);
    dimArray.push_back(index_cr);
    flags = NetRedeclare(flags, eDMReg2DNet);
    return new NUMemoryNet(netName, 1, &dimArray, flags, vflags, scope, src_loc);
  }
}


// Steve Lim 2013-11-10
// Build a NUMemoryNet for multi-dimensional arrays.

// Extract the ranges in the index constraint of 'type' traversing in the order in which they are
// defined which is highest to lowest dimension but entering them into dimArray in the reverse order
// since the NUMemoryNet constructor requires the dimensions to be lowest to highest.
void VerificVhdlDesignWalker::extractIndexRanges(
  ConstantRangeArray& dimArray,
  VhdlIdDef*          type,
  VhdlConstraint*     constraint)
{
  int dim = type->Dimension();
  std::vector<ConstantRange*> ranges;
  VhdlConstraint* index_constraint = constraint;
  for (int i = 0; i < dim; i++)
  {
    if (!index_constraint)
      break;
    ConstantRange* range = new ConstantRange(index_constraint->Left()->Integer(), index_constraint->Right()->Integer());
    ranges.push_back(range);
    index_constraint = index_constraint->ElementConstraint();
  }
  for (int i = ranges.size()-1; i >= 0; i--)
  {
    dimArray.push_back(ranges[i]);
  }
}

void VerificVhdlDesignWalker::extractArrayDimensions(
  ConstantRangeArray& dimArray,
  NetFlags&           flags,
  VhdlIdDef*          type,       // the type we're extracting for
  VhdlConstraint*     constraint, // type's index constraint
  VhdlIdDef*          parentType) // if non-NULL, the array type of which 'type' is an element type
{
    
  if (type->IsRecordType())
    return;

  ConstantRange* range = 0;
  if (type->IsScalarType())
  {
    if (parentType) {
        VhdlFullTypeDecl* decl = dynamic_cast<VhdlFullTypeDecl*>(parentType->GetDeclaration());
        if (decl) {
            VhdlArrayTypeDef* typeDef = dynamic_cast<VhdlArrayTypeDef*>(decl->GetTypeDef());
            if (typeDef) {
                VhdlSubtypeIndication* subtypeInd = typeDef->GetSubtypeIndication();
                if (subtypeInd &&
                    ID_VHDLEXPLICITSUBTYPEINDICATION == subtypeInd->GetClassId()) {
                    VhdlExplicitSubtypeIndication* expInd =static_cast<VhdlExplicitSubtypeIndication*>(subtypeInd);
                    VhdlConstraint* constraint = expInd->GetRangeConstraint()->EvaluateConstraint(mDataFlow, 0);
                    int size = constraint->NumBitsOfRange();
                    if (size > 1)
                    {
                        // A non-bit scalar type constitutes the lowest dimension
                        range = new ConstantRange(size-1, 0);
                        dimArray.push_back(range);
                    }
                    delete constraint;
                    return;
                }
            }
        }
    }
          int size = constraint->NumBitsOfRange();
          if (size > 1)
          {
              // A non-bit scalar type constitutes the lowest dimension
              range = new ConstantRange(size-1, 0);
              dimArray.push_back(range);
              if (type->IsIntegerType())
                  flags = NetFlags (flags | eSigned);
          }
          return;
  }

  // Extract the element's constraint first
  VhdlIdDef* elem_type = type->ElementType();
  if (!elem_type)
    return;
  extractArrayDimensions(dimArray, flags, elem_type, elem_type->Constraint(), type);

  // Now extract the index constraint of 'type'
  if (type->IsUnconstrained() && parentType)
  {
    int dim = type->Dimension();
    int parentdim = parentType->Dimension();

    // Get the dim(type) ranges from the parent's deep dimensionality i.e. the list of ranges
    // from the parent array's index constraint by traversing its ElementConstraint() chain.
    // This gives us the constraint(s) on 'type' from the context of 'parentType'.
    // Example:
    //    type T is array (0 to 1, 0 to 2, 0 to 3) of bit_vector(7 downto 0);
    //  dim(T) is 3 but we need another dim(bit_vector) element constraint which is (7 downto 0).

    std::vector<VhdlConstraint*> ranges;
    VhdlConstraint* elem_constraint = parentType->Constraint();
    for (int i = 1; i <= parentdim+dim; ++i)
    {
      if (!elem_constraint)
        break;
      if (i > parentdim)
        ranges.push_back(elem_constraint);
      elem_constraint = elem_constraint->ElementConstraint();
    }
    // Get dim(type) element constraints in the reverse order from the ranges list
    for (int i = ranges.size()-1; i >= 0; i--)
    {
      elem_constraint = ranges[i];
      ConstantRange* range = new ConstantRange(elem_constraint->Left()->Integer(), elem_constraint->Right()->Integer());
      dimArray.push_back(range);
    }
    return;
  }
  extractIndexRanges(dimArray, type, constraint);
}

NUNet* VerificVhdlDesignWalker::createMultiDimNet(
  VhdlIdDef&     obj,     // multi-dimensional object we are creating a net for
  StringAtom*&   netName,
  NetFlags&      flags,
  NUScope*&      scope, 
  SourceLocator& src_loc)
{

  if (containsRecords(obj.Type())){
    return createAndRegisterCompositeNet(obj, netName, scope, src_loc);
  }

  // TODO pull this code that fills in dimArray into a separate method. (this was once done by extractArrayDimensions())
  ConstantRangeArray dimArray;

  ConstantRange* last_range = NULL; // this holds the range for the last dimension in case it is a vhdl character or integer
  VhdlIdDef* cur_elem_type = &obj;
  while ( cur_elem_type && cur_elem_type->ElementType() ){
    cur_elem_type = cur_elem_type->ElementType(); // work our way down to the deepest nested element type
  }
  if ( cur_elem_type ) {
    VhdlConstraint* elem_constraint = cur_elem_type ? cur_elem_type->Constraint() : NULL;
    if (elem_constraint && ( 1 < elem_constraint->NumBitsOfRange() ) ) {
      // if it has more than a single bit then the NU representation will need a dimension for this type
      last_range = new ConstantRange((elem_constraint->NumBitsOfRange())-1, 0);
    }
  }
  
    
  int numDims = obj.DeepDimension();
  for (int dimIndex = 0; dimIndex < numDims; ++dimIndex){
    VhdlValue* val;
    VhdlValue* delete_this_val = NULL;
    VhdlConstraint* delete_this_constraint = NULL;
    cur_elem_type = cur_elem_type ? cur_elem_type->ElementType() : NULL;

    if ( obj.GetDimensionAt(dimIndex)->GetLeftExpression() ) {
      val = delete_this_val = obj.GetDimensionAt(dimIndex)->GetLeftExpression()->Evaluate(0,0,0);
    } else {
      delete_this_constraint = obj.GetDimensionAt(dimIndex)->EvaluateConstraintInternal(0,0);
      val = delete_this_constraint->Left();
    }
    if ( ! val || ! val->IsConstant() ){
      UtString msg("non-constant range expression for left term of index dimIndex:'");
      msg << dimIndex+1 << " of " << netName;
      mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&src_loc, msg.c_str());
      setError(true);
      return NULL;
    }
    // If the items in the array are enumeration values, then use the enum position
    // (rather than Integer value) as the left and right parts of the range. Often, enum
    // Integer values are identical to Position, but for charater type enums (std_logic,
    // std_ulogic, etc) they are not.
    SInt64 left = val->IsEnumValue() ? val->Position() : val->Integer();
    delete delete_this_val;
    delete delete_this_constraint;
    delete_this_val = NULL;
    delete_this_constraint = NULL;

    if ( obj.GetDimensionAt(dimIndex)->GetRightExpression() ) {
      val = delete_this_val = obj.GetDimensionAt(dimIndex)->GetRightExpression()->Evaluate(0,0,0);
    } else {
      delete_this_constraint = obj.GetDimensionAt(dimIndex)->EvaluateConstraintInternal(0,0);
      val = delete_this_constraint->Right();
    }
    if ( ! val || ! val->IsConstant() ){
      UtString msg("non-constant range expression for right term of index dimIndex:'");
      msg << dimIndex+1 << " of " << netName;
      mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&src_loc, msg.c_str());
      setError(true);
      return NULL;
    }
    SInt64 right = val->IsEnumValue() ? val->Position() : val->Integer();
    delete delete_this_val;
    delete delete_this_constraint;

    ConstantRange* range = new ConstantRange(left, right);
    dimArray.push_back(range);
  }
  if ( last_range ){
    dimArray.push_back(last_range);
  }
  std::reverse(dimArray.begin(), dimArray.end());
  
  VectorNetFlags vflags(eNoneVectorNet);
  flags = NetRedeclare(flags, eDMReg2DNet);
  // all vhdl arrays have only one packed dimension
  return new NUMemoryNet(netName, 1, &dimArray, flags, vflags, scope, src_loc);
}

NUNet* VerificVhdlDesignWalker::
create3DNet(VhdlIdDef& node,  StringAtom*& netName, NetFlags& flags, NUScope*& scope, SourceLocator& src_loc)
{
  VhdlConstraint* idx1 = node.Constraint()->IndexConstraint();
  VhdlConstraint* elem1 = node.Constraint()->ElementConstraint();
  VhdlConstraint* elem2 = elem1->ElementConstraint();
  V2N_INFO_ASSERT(idx1, node, "invalid VhdlConstraint");
  V2N_INFO_ASSERT(elem1, node, "invalid VhdlConstraint");
  V2N_INFO_ASSERT(elem2, node, "invalid VhdlConstcraint");
  
  ConstantRange* elem2_cr = new ConstantRange(elem2->Left()->Integer(), elem2->Right()->Integer());
  ConstantRange* elem1_cr = new ConstantRange(elem1->Left()->Integer(), elem1->Right()->Integer());
  ConstantRange* idx1_cr = new ConstantRange(idx1->Left()->Integer(), idx1->Right()->Integer());
  VectorNetFlags vflags(eNoneVectorNet);
  ConstantRangeArray dimArray;
  dimArray.push_back(elem2_cr);
  dimArray.push_back(elem1_cr);
  dimArray.push_back(idx1_cr);
  flags = NetRedeclare(flags, eDMReg2DNet);
  return new NUMemoryNet(netName, 1, &dimArray, flags, vflags, scope, src_loc);
}

// This gets the element type of the deepest dimension of an array
// Examples:
//      type T1 is array(0 to 1) of E;
//      type T2 is array(0 to 1) of T1;
//      type T3 is array(0 to 1, 0 to 2) of T2;
// getDeepElementType(T1), getDeepElementType(T2), getDeepElementType(T3)
// will all return E.
VhdlIdDef* VerificVhdlDesignWalker::getDeepElementType(VhdlIdDef* type)
{
  VhdlIdDef *elementType = type->ElementType();
  if (!elementType)
    return type;
  if (elementType->IsArrayType())
    return getDeepElementType(elementType);
  else
    return elementType;
}

NUCompositeNet*
VerificVhdlDesignWalker::createAndRegisterCompositeNet(
  VhdlIdDef&     node,
  StringAtom*&   netName,
  NUScope*&      scope,
  SourceLocator& src_loc)
{
    NUCompositeNet* compositeNet = createCompositeNet(node, netName, scope, src_loc);
    return compositeNet;
}


//this method used for creating net array of records and array of array of records
NUCompositeNet* VerificVhdlDesignWalker::createCompositeNet(
  VhdlIdDef&     node,
  StringAtom*&   netName,
  NUScope*&      scope,
  SourceLocator& src_loc)
{
  VhdlIdDef* type = node.Type();
  VhdlConstraint* constraint = node.Constraint();

    VhdlIdDef* el_type = getDeepElementType(type); //node.ElementType();
    NetFlags reg_flags = sFlags(node);
    NUNamedDeclarationScope* record_scope = new NUNamedDeclarationScope(netName, scope, mNucleusBuilder->gIODB(), mNucleusBuilder->gNetRefFactory(), src_loc, eHTRecord);
   
    // no block scope, only named declaration scope
    pushDesignScope(0, record_scope);
    V2N_INFO_ASSERT(record_scope, node, "invalid decl scope");
    UInt32 size = el_type->NumOfElements();
    NUNetVector fields;
    ConstantRangeArray ranges;
    extractArrayDimensions(ranges, reg_flags,  type, constraint, 0);
    for (UInt32 i = 0; i < size; ++i)
    {
        VhdlIdDef* idDef = el_type->GetElementAt(i);
        idDef->Accept(*this);
        NUNet* net = static_cast<NUNet*>(mCurrentScope->gValue());
        V2N_INFO_ASSERT(net, node, "failed to get NUNet");
        NetFlags flags = net->getFlags();
        if (! dynamic_cast<NUMemoryNet*>(net)) {
            flags = NetFlags(flags | eDMWireNet | eBlockLocalNet);
        } else {
            flags = NetFlags(flags | eBlockLocalNet);
        }
        if (idDef->Constraint() && idDef->Constraint()->IsSignedRange()) {
            flags = NetFlags(flags | eSigned);
        }
        NetFlags inputMask(eInputNet);
        NetFlags outputMask(eOutputNet);
        if (node.IsInput() || (inputMask & reg_flags)) {
            flags = NetFlags(flags | eInputNet);
        } else if (node.IsOutput() || node.IsBuffer() || (outputMask & reg_flags)) {
            flags = NetFlags(flags | eOutputNet);
        }
        if (record_scope->inTFHier()) {
            flags = NetFlags(flags | eNonStaticNet);
        }
        net->setFlags(flags);
        fields.push_back(net);
        record_scope->addLocal(net);
    }
    popScope();
    const StringAtom* typeAtom = mNucleusBuilder->gCache()->intern(UtString(node.Type()->Name()));
    UtString prefix_name;
    prefix_name << "recnet_" << netName->str();
    StringAtom* netAtom = scope->getModule()->gensym(prefix_name.c_str());
    NUCompositeNet* net = new NUCompositeNet(typeAtom, fields, ranges,false,scope, netAtom, src_loc, reg_flags);
    //save composite net for future look-up
    //composite name is different form declaration node
    //and in lookUpNet method can't find right net by name
    //which is used in component instantiation
    //FD thinks: it seems we are keeping too many registrations of same information, there should be way to optimize this
    mCompositeNet.insert(UtPair<NUNet*, StringAtom*>(net, netName));
    return net;
}

void VerificVhdlDesignWalker::createFOpenSysTaskForLine(NUNet* net, VhdlTreeNode& node)
{
    if (! mEnableVhdlFileIO) {
        return ;
    }
    if (net->isVHDLLineNet()) {
        NUExprVector expr_vector;
        NUModule* module = getModule();
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, node, "Unable to get module");
        StringAtom* file_open_sys = module->gensym("line");
        NUFOpenSysTask* systask = new NUFOpenSysTask(file_open_sys, net, 0, expr_vector,
                                                     module, mNucleusBuilder->gNetRefFactory(),
                                                     false, false, true, net->getLoc());
        NUBlock* block = initBlockForFileIO(net->getLoc());
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(block, node, "Failed to get inital block");
        mNucleusBuilder->aStmtToBlock(systask, block);
    }
}

// This method 'declares' a non-local object, the first time the net is
// referenced. A non-local object is a signal/constant/variable declared
// in a package.
//
// The non-local object is placed in the top most module, in nested
// declaration scopes that reflect the library and package names.
NUNet* VerificVhdlDesignWalker::declareHierRefNet(VhdlIdDef& node, VhdlValue* value)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "create hier ref net" << std::endl;
#endif
    if (hasError()) {
        return 0;
    }
    // TODO: Assert that it is not already in the hierarchical registry

    //save hierRefnode for PrePopInfo and STBuilder
    mNucleusBuilder->sHierRefIdDef(&node);
    SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    //generate names for library and packages:
    // 0 - module name
    // 1 - library name
    // 2 - package name
    // 3 - net name
    AtomArray netHierName;
    createHierName(&node, netHierName);
    StringAtom* libAtom = netHierName[1];
    StringAtom* packAtom = netHierName[2];

    //we just create ones declaration scope
    //in user defined package can be located more then one declaration of identifier
    NUNamedDeclarationScope* lib_scope = 0;
    UtMap<StringAtom*, NUNamedDeclarationScope*>::iterator it_lib = mLibraries.find(libAtom);
    if (mLibraries.end() == it_lib) {
        lib_scope = new NUNamedDeclarationScope(libAtom, 
                                                mNucleusBuilder->getTopModule(),
                                                mNucleusBuilder->gIODB(), 
                                                mNucleusBuilder->gNetRefFactory(),
                                                src_loc);
        mLibraries.insert(UtPair<StringAtom*, NUNamedDeclarationScope*>(libAtom, lib_scope));
    } else {
        lib_scope = it_lib->second;
    }
    V2N_INFO_ASSERT(lib_scope, node, "Failed to create declaration scope");
    NUNamedDeclarationScope* pack_scope = 0;
    UtMap<StringAtom*, NUNamedDeclarationScope*>::iterator it_pack = mPackages.find(packAtom);
    if (mPackages.end() == it_pack) {
        pack_scope = new NUNamedDeclarationScope(packAtom, lib_scope, 
                                                 mNucleusBuilder->gIODB(), 
                                                 mNucleusBuilder->gNetRefFactory(),
                                                 src_loc);
        mPackages.insert(UtPair<StringAtom*, NUNamedDeclarationScope*>(packAtom, pack_scope));
    } else {
        pack_scope = it_pack->second;
    }
    V2N_INFO_ASSERT(pack_scope, node, "Failed to create declaration scope");
    
    // FD thinks: this seems suspicious - Is every hierarchical reference defined in package scope ?
    pushDesignScope(0, pack_scope);
    //populate net declared insight user defined package
    node.Accept(*this);
    NUBase* base = mCurrentScope->gValue();
    UtString errMsg = "";
    errMsg << "failed to populate net for HerRef, line : " << src_loc.getLine();
    V2N_INFO_ASSERT(base, node, errMsg.c_str()); 
    NUNet* mNet = dynamic_cast<NUNet*>(base);
    V2N_INFO_ASSERT(mNet, node, "Invalid NUNet object");
    errMsg = "";
    errMsg << "Invalid package scope for HierRefNet lien : " << src_loc.getLine();
    V2N_INFO_ASSERT(pack_scope, node, errMsg.c_str());
    popScope();
    pack_scope->addLocal(mNet);
    //create HierRef net from populated net which declared inside user defined package     
    // RBS thinks: TODO we don't need to put the net on the scope stack, because we return it from the function.
    mCurrentScope->sValue(mNet);
    //check line net 
    if (mEnableVhdlFileIO) {
        createFOpenSysTaskForLine(mNet, node);
    }
    //initial value
    if (value) {
        //create or get initial block in top module
        NUBlock* block = createInitialBlock(src_loc, mNucleusBuilder->getTopModule());
        if (dynamic_cast<NUVectorNet*>(mNet)) {
            NUExpr* the_const = createConst(mNet, value, src_loc);
            V2N_INFO_ASSERT(the_const, node, "Invalid NUConst object");
            NULvalue* lval = new NUIdentLvalue(mNet, src_loc);
            V2N_INFO_ASSERT(lval, node, "Failed to create NULvalue object");
            NUBlockingAssign* ba = new  NUBlockingAssign(lval, the_const, false, src_loc, false);
            V2N_INFO_ASSERT(ba, node, "invalid blocking assign");
            block->addStmt(ba);
        } else if (NUMemoryNet* memNet = dynamic_cast<NUMemoryNet*>(mNet)) {
            handleMemoryNet(memNet, node, value, mNet, block, src_loc);
            //TODO handle composite net case here
        // } else if (NUCompositeNet* compNet = dynamic_cast<NUCompositeNet*>(mNet)) {
            //handleCompositeNet(compNet, node, value, mNet, block, src_loc);
        } else {
            UtString errMsg = "";
            errMsg << "Unsupported net type line : " << src_loc.getLine();
            V2N_INFO_ASSERT(false, node, errMsg.c_str());
        }
    }
    return mNet;
}

// either return the existing initial block for the supplied module, or create an initial block and then return it
NUBlock* VerificVhdlDesignWalker::createInitialBlock(SourceLocator& src_loc, NUModule* module)
{
  NUBlock* block = 0;
  NUInitialBlockList initial_list;
  NUCLEUS_CONSISTENCY(module, src_loc, "Unable to get module");
  module->getInitialBlocks(&initial_list);
  if (initial_list.empty()) {
    block = mNucleusBuilder->cBlock(module, src_loc);
    NUScope* block_scope = getBlockScope();
    NUCLEUS_CONSISTENCY(block_scope, src_loc, "Unable to get block scope");
    NUInitialBlock* initial = new NUInitialBlock(block_scope->gensym("initialBlock"),
        block, 
        mNucleusBuilder->gNetRefFactory(), 
        src_loc);
    mNucleusBuilder->aInitialBlockToModule(initial, module);
  } else {
    NUCLEUS_CONSISTENCY(1 == initial_list.size(), src_loc, "only one initial block should exist");
    NUInitialBlock* initial_block = initial_list.front();
    NUCLEUS_CONSISTENCY(initial_block, src_loc, "invalid initial block");
    block = initial_block->getBlock();
  }
  return block;
}

void VerificVhdlDesignWalker::
handleMemoryNet(NUMemoryNet* memNet, VhdlIdDef& node, VhdlValue* value, NUNet* mNet, NUBlock* block, SourceLocator& src_loc)
{
  unsigned i = 0;
  VhdlValue* val_elem = 0;
  //if one of components not const then return not constant aggregate expression,
  //otherwise it is constant

  // RJC thinks: what is this? only handling up to 3 dimensions? why is this not:
  //   const ConstantRange* c_range = memNet->getRange((memNet->getNumDims())-1);

  const ConstantRange* c_range = (3 == memNet->getNumDims()) ? memNet->getRange(2) : memNet->getRange(1);
  bool counting_down = c_range->bigEndian(); // bigEndian is like [7:-1], !bigEndian is like [-3:7]
  SInt32 step_value = counting_down ? -1 : 1;
  SInt32 selId = c_range->getMsb();
  FOREACH_ARRAY_ITEM(value->GetValues(), i, val_elem) {
      if (!val_elem) {
          VhdlCompositeValue* cVal = dynamic_cast<VhdlCompositeValue*>(value);
          INFO_ASSERT(cVal, "invalid VhdlCompositeValue");
          val_elem = cVal->GetDefaultValue();
      }
      V2N_INFO_ASSERT(val_elem, node, "Invalid VhdlValue element");

      NUExpr* sel = NUConst::createKnownIntConst(selId, 32,  false, src_loc);
      sel->setSignedResult(true);

      selId = selId + step_value; // advance to next value for use in next iteration

      NUExprVector eVec;
      eVec.push_back(sel);
      NUMemselLvalue* memLvalue = new NUMemselLvalue(memNet, &eVec, src_loc);
      NUExpr* the_const = 0;
      if (node.ElementType()->IsStringType()) { //string type initialization misc/enumindex1.vhdl
          //initialized by string
          char* image = val_elem->Image();
          UtString str(image);
          Strings::free(image);
          //get pure string, remove first and last \" characters
          str = str.substr(1, str.size() - 2);
          UInt32 sz = str.size();
          NUExprVector expVec;
          for (UInt32 l = 0; l < sz; ++l) {
              UtString subStr = str.substr(l, 1);
              UtString binStr;
              composeBinStringFromString(subStr.c_str(), &binStr, false);
              NUExpr* charExp = NUConst::createKnownConst(binStr, 8, true, src_loc);
              V2N_INFO_ASSERT(charExp, node, "failed to create constant expression");
              expVec.push_back(charExp);
          }
          the_const = new NUConcatOp(expVec, 1, src_loc);
          V2N_INFO_ASSERT(the_const, node, "invalid concatenation object");
      } else if (val_elem->IsComposite()) { //misc/vect7.vhdl
          the_const = createCompositeConst(node.Name(), val_elem, src_loc, memNet->getRange(0));
      } else {
          the_const = createConst(mNet, val_elem, src_loc, memNet->getRange(0));
      }
      if ( ! the_const ) {
          UtString msg("a constant value for '");
          msg << node.Name() << "'";
          mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&src_loc, msg.c_str());
          setError(true);
          return;
      }
      NUBlockingAssign* ba = new NUBlockingAssign(memLvalue, the_const, false, src_loc, false);
      block->addStmt(ba);
  }
}

NUExpr* VerificVhdlDesignWalker::createConst(NUNet* net, VhdlValue* value, SourceLocator& src_loc, const ConstantRange* r)
{
    if (r) {
        UInt32 lenght = r->getMsb() > r->getLsb() ? r->getMsb() - r->getLsb() : r->getLsb() - r->getMsb();
        //       UInt32 bits = bitness(lenght + 1);
        //FD: this looks incorrect signedness should not be discoverd by value->IsSigned() api, as it is just checking for negative value, instead the sign information should be taken from type itsefl (e.g. integer always signed)
        bool is_signed = value->IsSigned() ? true : false;
        NUConst* the_const = NUConst::createKnownIntConst(value->Integer(), lenght + 1, is_signed, src_loc);
        return the_const;
    }
    UtString errMsg = "";
    errMsg << "Invalid VhdlValue object line : " << src_loc.getLine();
    INFO_ASSERT(value, errMsg.c_str());
    bool is_signed = value->IsSigned() ? true : false;
    UInt32 bits = 0;
    if (value->IsComposite()) {
        const char* node_name = net->getName()->str();
        INFO_ASSERT(value->NumElements() > 0, "Empty number of elements in composite");
        //INFO_ASSERT(mTargetConstraint->NumBitsOfRange() > 0, "Empty record constant");
        VhdlValue* elemVal = value->ValueAt(0);
        UInt32 bits = 0;
        ConstantRange* intRange = NULL;
        if (elemVal->IsIntVal()) {
            //FD TODO need to add parse-tree node/id argument to this function to be able to get constraint and then get size/sign from it
            bits = elemVal->NumBits();
            //bits = mTargetConstraint->ElementConstraintAt(0)->NumBitsOfRange();
            intRange = new ConstantRange(bits - 1, 0);
        }
        NUExpr* the_const = createCompositeConst(node_name, value, src_loc, intRange);
        delete intRange;
        return the_const;
    } else if (value->IsIntVal()) {
        if (NUVectorNet* vNet = dynamic_cast<NUVectorNet*>(net)) {
            bits = vNet->getBitSize();
        } else if (NUMemoryNet* mNet = dynamic_cast<NUMemoryNet*>(net)) {
            bits = mNet->getRangeSize(1);
        } else {
            INFO_ASSERT(0, "Not supported net type");
        }
    // RBS thinks: I would guess we need to check for IsBit() as well.
    } else if (value->IsEnumValue()) {
        UtString errMsg = "";
        errMsg << "Invalid conversion to enumeration line : " << src_loc.getLine();
        INFO_ASSERT(value->ToEnum(), errMsg.c_str());
        errMsg = "";
        errMsg << "failed to get VhdlEnumeratinId line : " << src_loc.getLine();
        INFO_ASSERT(value->ToEnum()->GetEnumId(), errMsg.c_str());
        bits = bitness(value->ToEnum()->GetEnumId()->NumOfEnums() - 1);
    } else {
        UtString errMsg = "";
        errMsg << "not supported yet line " << src_loc.getLine();
        INFO_ASSERT(0, errMsg.c_str());
    }
    NUConst* the_const = NUConst::createKnownIntConst(value->Integer(),
                                                      bits,
                                                      is_signed,
                                                      src_loc);
    errMsg = "";
    errMsg << "failed to create NUConst object line " << src_loc.getLine();
    INFO_ASSERT(the_const, errMsg.c_str());
    return the_const;
}

// Create a nucleus value for a VhdlCompositeValue. Return NULL if error.
//FD TODO need to add parse-tree node/id argument to this function to be able to get constraint and then get size/sign from it
//FD last argument seems to be used only for integer sub-element case to get size of integer sub-element, we should be able to get rid of it, after node/id argument addition.
//FD thinks: val_elem - is incorrectly named, the real name for it should be composite_val as it really represent the composite evaluated value - nucleus constant (concat) for which this function intended to return 
NUExpr* VerificVhdlDesignWalker::createCompositeConst(const char* node_name, VhdlValue* val_elem, const SourceLocator& src_loc, const ConstantRange* r)
{
  NUExpr* the_const = 0;

  VhdlCompositeValue* compVal = dynamic_cast<VhdlCompositeValue*>(val_elem);
  VhdlValue* default_composite_val = compVal ? compVal->GetDefaultValue() : NULL;

  NUExprVector expVec;
  unsigned i;
  // FD thinks: I would replace val with elem_val name as it really composites 'i'-th element value
  VhdlValue* val;
  FOREACH_ARRAY_ITEM(val_elem->GetValues(), i, val) {
    if (!val) {
      val = default_composite_val;   // no value is defined, use default value
    }
    
    char* image = NULL;
    if ( val ) {
      image = val->Image();
    } else {
      UtString msg("initial composite value for '");
      msg << node_name << "'";
      mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&src_loc, msg.c_str());
      setError(true);
      return NULL;
    }
    UtString node_str(image);
    // Remove surrounding single or double quotes from values like "'-'", or "010"
    if ( (node_str[0] == '\'' && node_str[node_str.size() - 1] == '\'') ||
         (node_str[0] == '"'  && node_str[node_str.size() - 1] == '"')     ) {
      node_str = node_str.substr(1, node_str.size() - 2); // remove first and last char
    }
    UtString node_str_uppercase(node_str); node_str_uppercase.uppercase();
    Strings::free(image);
    NUExpr* const_expr = NULL;
    bool hasNonOneOrZero = node_str.find_first_of("xXzZwWuU-") != UtString::npos;
    
    if (val->IsComposite()) {
      //FD: not sure what to put instead of node_name (node_name - used for error report message inside createCompositeConst), 
      //for now using parent name (caller name)
      const_expr = createCompositeConst(node_name, val, src_loc, r);
    } else if (UtString("FALSE") == node_str_uppercase) {
      const_expr = NUConst::createXZ(UtString("0"), false, 1, src_loc);
    } else if (UtString("TRUE") == node_str_uppercase) {
      const_expr = NUConst::createXZ(UtString("1"), false, 1, src_loc);
    } else if ( hasNonOneOrZero ){
      const_expr= NUConst::createXZ(node_str, false, 1, src_loc);
    } else {
      if (val && val->IsIntVal()) {
        UInt32 bits = r->getLength();
        const_expr = NUConst::createKnownIntConst(val->Integer(), bits,  false, src_loc);
        //FD thinks: we should try to get sign information from constraint and if that isn't available only then use implicit type sign
        const_expr->setSignedResult(true);
      } else {
          //FD thinks: are we sure in all other cases this will succeed? I don't see real value handling 
          // (so probably that is still TODO)
        INFO_ASSERT(!val->IsReal(), "Unsupported real type constant");
        const_expr = NUConst::createXZ(node_str, false, node_str.size(), src_loc);
      }
    }
    INFO_ASSERT(const_expr, "failed to create constant expression");
    expVec.push_back(const_expr);
  }
  the_const = new NUConcatOp(expVec, 1, src_loc);
  return the_const;
}

bool VerificVhdlDesignWalker::unconstrainedArray(VhdlIdDef& node, StringAtom*& netName, NetFlags& flags, NUScope*& scope, SourceLocator& src_loc)
{
    if (UtString("line") == UtString(node.Type()->Name())) {
        VectorNetFlags vflags(eVhdlLineNet);
        ConstantRange cr(31, 0);
        NUNet* net = new NUVectorNet(netName, cr, flags, vflags, scope, src_loc);
        V2N_INFO_ASSERT(net, node, "Invalid NUNet object");
        mCurrentScope->sValue(net);
        registerNucleusObject(net);
        return true;
        
    }
    NUNet* net = 0;
    //handle than element type is enumeration misc/enum6.vhdl
    // RBS thinks: this is checking for an enumeration type for which the 
    // different enumeration values can be encoded as a single bit (bit_vector,
    // std_logic_vector, etc).
    // FD thinks: separating single bit case seems artificial
    if (node.ElementType()->IsEnumerationType() && 1 == node.Constraint()->ElementConstraint()->NumBitsOfRange()) {
        VectorNetFlags vflags(eNoneVectorNet);
        // RBS thinks: is 'enum_encoding' part of the vhdl standard? I don't see it in the LRM.        
        if (node.ElementType()->GetAttributeId("enum_encoding")) { //misc/enum6.vhdl
            ConstantRange* index_cr = new ConstantRange(node.Constraint()->IndexConstraint()->Left()->Integer(),
                                                        node.Constraint()->IndexConstraint()->Right()->Integer());
            //get element range
            VhdlAttributeSpec* spec = node.ElementType()->
                GetAttributeId("enum_encoding")->FindRelevantAttributeSpec(node.ElementType());
            ConstantRange* element_cr = 0;
            if (spec && spec->GetValue()->IsStringLiteral()) {
                UtString enumStr(spec->GetValue()->Name());
                UtString::iterator f = enumStr.begin();
                UtString::iterator l = enumStr.end();
                UInt32 bitness = 0;
                for (; f != l; ++f, ++bitness) {
                    if (*f == ' ') { break; }
                }
                #ifdef DEBUG_VERIFIC_VHDL_WALKER
                    std::cerr << "bitness = " << bitness - 2 << std::endl;
                #endif
                //verific get string with \" so we have to decriment
                element_cr = new ConstantRange(bitness - 2, 0);
            } else {
                element_cr = new ConstantRange(node.Constraint()->ElementConstraint()->Left()->Integer(),
                                               node.Constraint()->ElementConstraint()->Right()->Integer());
            }
            flags = NetRedeclare(flags, eDMReg2DNet);
            ConstantRangeArray dimArray;
            dimArray.push_back(element_cr);
            dimArray.push_back(index_cr);
            net = new NUMemoryNet(netName, 1, &dimArray, flags, vflags, scope, src_loc);
        } else {
            ConstantRange index_cr(node.Constraint()->IndexConstraint()->Left()->Integer(),
                                   node.Constraint()->IndexConstraint()->Right()->Integer());
            net = new NUVectorNet(netName, index_cr, flags, vflags, scope, src_loc);
        }
    } else {
        // FD thinks: we need to get deep element type, what about multi-dimensional arrays?
        if (node.Type()->ElementType()->IsScalarType()) {
            //dynamic_var_decl.vhdl
            ConstantRange* element_cr = 0;
            VectorNetFlags vflags;
            if (node.Constraint()->ElementConstraint()->ElementConstraint()) {
                element_cr = new ConstantRange(node.Constraint()->ElementConstraint()->Left()->Integer(),
                                               node.Constraint()->ElementConstraint()->Right()->Integer());
                vflags = VectorNetFlags(eNoneVectorNet);
            } else {
                element_cr = new ConstantRange(node.Constraint()->ElementConstraint()->NumBitsOfRange() - 1, 0);
                vflags = node.Type()->IsStringType() ? VectorNetFlags(eNoneVectorNet) :
                    node.Constraint()->ElementConstraint()->IsSignedRange() ?
                    VectorNetFlags(eSignedSubrange) :
                    VectorNetFlags(eUnsignedSubrange);
            }
            ConstantRange* index_cr = new ConstantRange(node.Constraint()->IndexConstraint()->Left()->Integer(),
							node.Constraint()->IndexConstraint()->Right()->Integer());

            ConstantRangeArray dimArray;
            dimArray.push_back(element_cr);
            dimArray.push_back(index_cr);
            flags = NetRedeclare(flags, eDMReg2DNet);
            net = new NUMemoryNet(netName, 1, &dimArray, flags, vflags, scope, src_loc);
        } else if (containsRecords(node.Type())) {
            net = createAndRegisterCompositeNet(node, netName, scope, src_loc);
        } else {
            net = createMemoryNet(node, netName, flags, scope, src_loc);
        }
    }
    V2N_INFO_ASSERT(net, node, "failed to create NUNet");
    mCurrentScope->sValue(net);
    registerNucleusObject(net);
    return true;
}

NUNet* VerificVhdlDesignWalker::
createMemoryNet(VhdlIdDef& node, StringAtom*& netName, NetFlags& flags, NUScope*& scope, SourceLocator& src_loc)
{
  if (2 == node.DeepDimension()) {
    if (1 == node.Constraint()->ElementConstraint()->ElementConstraint()->NumBitsOfRange()) {
      ConstantRange* element_cr = new ConstantRange(node.Constraint()->ElementConstraint()->Left()->Integer(),
          node.Constraint()->ElementConstraint()->Right()->Integer());
      ConstantRange* index_cr = new ConstantRange(node.Constraint()->IndexConstraint()->Left()->Integer(),
          node.Constraint()->IndexConstraint()->Right()->Integer());
      VectorNetFlags vflags(eNoneVectorNet);
      ConstantRangeArray dimArray;
      dimArray.push_back(element_cr);
      dimArray.push_back(index_cr);
      flags = NetRedeclare(flags, eDMReg2DNet);
      return new NUMemoryNet(netName, 1, &dimArray, flags, vflags, scope, src_loc);
    } else if (node.Type()->ElementType()->IsArrayType()) {
      ConstantRange* element_element_cr = 0;
      if (node.Type()->ElementType()->ElementType()->IsIntegerType()) {
        element_element_cr = new ConstantRange(31, 0);
        flags = NetFlags (flags | eSigned);
      }

      UtString errMsg = "";
      errMsg << "failed to get element constraint for array of integers: line " << src_loc.getLine();
      V2N_INFO_ASSERT(element_element_cr, node, errMsg.c_str());
      ConstantRange* element_cr = new ConstantRange(node.Constraint()->ElementConstraint()->Left()->Integer(),
          node.Constraint()->ElementConstraint()->Right()->Integer());
      ConstantRange* index_cr = new ConstantRange(node.Constraint()->IndexConstraint()->Left()->Integer(),
          node.Constraint()->IndexConstraint()->Right()->Integer());

      VectorNetFlags vflags(eNoneVectorNet);
      ConstantRangeArray dimArray;
      dimArray.push_back(element_element_cr);
      dimArray.push_back(element_cr);
      dimArray.push_back(index_cr);
      flags = NetRedeclare(flags, eDMReg2DNet);
      return new NUMemoryNet(netName, 1, &dimArray, flags, vflags, scope, src_loc);
    }
  } else {
    if (node.ElementType()->IsRecord()) {
      return createAndRegisterCompositeNet(node, netName, scope, src_loc);
    } else {
      return createMultiDimNet(node, netName, flags, scope, src_loc);
    }
  }
  return 0;
}

VhdlIdDef* getBaseType(VhdlIdDef* type)
{
  VhdlIdDef *baseType = type->Type();
  if (baseType == type)
    return baseType;
  return getBaseType(baseType);
}

// Steve Lim 2013-11-23: Perform validation checks on an array identifier.
//  1. The object is not a type identifier.
//  2. The object is an array.
//  3. The object's type is either constrained or unconstrained
//     If its type is a subtype, this check is made on the subtype's base type.
//  4. The object has a type.
//  5. The object has an element type.
//  6. The object's constraint (node.Constraint()) is not NULL. A NULL
//     constraint indicates a failure to evaluate its constraint which is an
//     error condition for Nucleus population.
//     Note: An identifier's type may still be unconstrained.
//  7. The object has an index constraint
//  8. The object has an element constraint

bool VerificVhdlDesignWalker::validateArray(
  VhdlIdDef&     node,    // The array object being validated
  VhdlIdDef*     type,    // The object's type
  SourceLocator& src_loc)
{
  UtString errMsg;

  // Q: Why is an access type like "LINE" also an array type
  if (type->IsAccessType())
    return true; // not an error

  // (1)
  if (node.IsType())
  {
    errMsg << "Internal error: validateArray called on a type identifier: '" << node.Name() << "'";
    V2N_INFO_ASSERT(false, node, errMsg.c_str());
    return false;
  }

  // (2)
  if (!node.IsArray())
  {
    errMsg << "Internal error: validateArray called on a non-array " << node.Name();
    V2N_INFO_ASSERT(false, node, errMsg.c_str());
    return false;
  }

  // (3)
  if (!type->IsConstrainedArrayType() && !type->IsUnconstrained())
  {
     if (type->IsSubtype())
       return validateArray(node, type->Type(), src_loc);
     errMsg << "Array type " << type->Name() << " is neither constrained nor unconstrained.";
     setError(true);
  }

  // (4)
  if (!type)
  {
    errMsg << "Internal error: array object '" << node.Name() << "' does not have a type.";
    V2N_INFO_ASSERT(false, node, errMsg.c_str());
    return false;
  }

  // (5)
  if (!node.ElementType())
  {
    errMsg << "Internal error: array object '" << node.Name() << "' does not have an element type.";
    V2N_INFO_ASSERT(false, node, errMsg.c_str());
    return false;
  }

  // (6)
  VhdlConstraint* constraint = node.Constraint();
  if (!constraint)
  {

    mNucleusBuilder->getMessageContext()->Verific2NUFailedWidth(&src_loc, node.Name());
    mCurrentScope->sValue(NULL);
    setError(true);
    return false;
  }
  if (!hasError())
  {
    VhdlConstraint* indexConstraint = constraint->IndexConstraint();
    VhdlConstraint* elementConstraint = constraint->ElementConstraint();

    // (7)
    if (!indexConstraint)
    {
      errMsg << "Array object " << node.Name() << " has no index constraint.";
      setError(true);
    }
    // (8)
    if (!elementConstraint)
    {
      errMsg << "Array object " << node.Name() << " has no element constraint.";
      setError(true);
    }
  }
  if (hasError())
  {
    V2N_INFO_ASSERT(false, node, errMsg.c_str());
    mCurrentScope->sValue(0);
    return false;
  }
  return true;
}

// Steve Lim 2013-11-23: Perform sanity checks on a record identifier:
// 1. It has one or more elements.
// 2. The VhdlIdDef objects are present at all element positions.
bool VerificVhdlDesignWalker::validateRecord(
  VhdlIdDef&     node,
  SourceLocator& src_loc)
{
  UtString errMsg;
  VhdlIdDef* type = node.Type();
  if (!type->IsRecord())
  {
    errMsg << "Internal error: validateRecord called on a non-record " << node.Name();
    V2N_INFO_ASSERT(false, node, errMsg.c_str());
    setError(true);
    return false;
  }

  if (node.NumOfElements() <= 0)
  {
    errMsg << src_loc.getFile() << ":" << src_loc.getLine() << ": Record " << node.Name() << " has no elements";
    setError(true);
  }
  else
  {
    for (UInt32 i = 0; i < node.NumOfElements(); i++)
    {
      if (!node.GetElementAt(i))
      {
        errMsg << src_loc.getFile() << ":" << src_loc.getLine() << ": Missing element at index position " << i << " in record " << node.Name();
        setError(true);
      }
    }
  }
  if (hasError())
  {
    V2N_INFO_ASSERT(false, node, errMsg.c_str());
    mCurrentScope->sValue(0);
    return false;
  }
  return true;
}


bool VerificVhdlDesignWalker::processCompositeDef(VhdlIdDef& node, StringAtom*& netName, NetFlags& flags, NUScope*& scope, SourceLocator& src_loc)
{
    bool success = false;
    if (node.IsArray()) {
      if (validateArray(node, node.Type(), src_loc)) {
        if (node.Type()->IsConstrainedArrayType()) {
          success = constrainedArray(node, netName, flags, scope, src_loc);
        } else {
          success = unconstrainedArray(node, netName, flags, scope, src_loc);
        }
      }
    }
    else if (node.IsRecord()) {
      if (validateRecord(node, src_loc)) {
        success = true;
        //save hierRefnode for PrePopInfo and STBuilder
        NUCompositeNet* net = createAndRegisterCompositeNet(node, netName, scope, src_loc);
        //save composite net for future look-up
        //composite name is different form declaration node
        //and in lookUpNet method can't find right net by name
        //which is used in component instantiation
        //FD thinks: no need todo this set as it's alreay done in createAndRegisterCompositeNet
        //mCompositeNet.insert(UtPair<NUNet*, StringAtom*>(net, netName));
        //V2N_INFO_ASSERT(net, node, "failed to create NUBitNet");
        mCurrentScope->sValue(net);
        registerNucleusObject(net);
      }
    }
    
    return success;
}

bool VerificVhdlDesignWalker::processAccessDef(VhdlIdDef& node, StringAtom*& /* netName */, NetFlags& /*flags*/, NUScope*& /*scope*/, SourceLocator& /*src_loc*/)
{  
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "\tAccessDef" << std::endl;
#endif
    if (!node.Type() || !node.Type()->IsAccessType()) {
        return false;
    }
    V2N_INFO_ASSERT(false, node, "not supported yet");
    return true;
}

bool VerificVhdlDesignWalker::processFileDef(VhdlIdDef& node, StringAtom*& netName, NetFlags& flags, NUScope*& scope, SourceLocator& src_loc)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "\tFileDef" << std::endl;
#endif
    if (! node.IsFile()) {
        return false;
    }
    NUNet* net = 0;
    V2N_INFO_ASSERT(node.Type(), node, "Invalid type node");
    V2N_INFO_ASSERT(node.Type()->DesignatedType(), node, "Invalid deignated type node");
    if (node.Type()->DesignatedType()->IsIntegerType()) {
        VectorNetFlags vflags(eNoneVectorNet);
        NetFlags flags = sFlags(node);
        flags = NetFlags(flags | eDMIntegerNet);
        ConstantRange cr(node.Type()->DesignatedType()->Constraint()->NumBitsOfRange() - 1, 0);
        net = new NUVectorNet(netName, cr, flags, vflags, scope, src_loc);
    } else {
        VectorNetFlags vflags(eNoneVectorNet);
        flags = NetFlags(flags | eDMIntegerNet);
        ConstantRange cr(31, 0);
        net = new NUVectorNet(netName, cr, flags, vflags, scope, src_loc);
    }
    V2N_INFO_ASSERT(net, node, "failed to create NUNet");
    mCurrentScope->sValue(net);
    registerNucleusObject(net);
    return true;
}

bool VerificVhdlDesignWalker::processScalarDef(VhdlIdDef& node, StringAtom*& netName, NetFlags& flags, NUScope*& scope, SourceLocator& src_loc)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "\tScalarDef" << std::endl;
#endif
    if (!node.Type() || ! node.Type()->IsScalarType()) {
        return false;
    }
    NUNet* net = 0;
    bool isEvaluated = false;
    if (node.Type()->IsIntegerType()) {
        INFO_ASSERT(node.Type()->Constraint(), "integer type has to have constraint");
        //check for range sum : out integer range 0 to 63
        VhdlConstraint* constraint = 0;
        if (node.GetSubtypeIndication() &&
            (ID_VHDLEXPLICITSUBTYPEINDICATION == node.GetSubtypeIndication()->GetClassId())) {
            VhdlExplicitSubtypeIndication* subtypeInd = static_cast<VhdlExplicitSubtypeIndication*>(node.GetSubtypeIndication());
            V2N_INFO_ASSERT(subtypeInd, node, "invalid subtype indication node");
            if (! subtypeInd->GetRangeConstraint()) {
                mNucleusBuilder->getMessageContext()->Verific2NUInvalidRangeConstr(&src_loc);
                setError(true);
                return false;
            }
            constraint =  subtypeInd->GetRangeConstraint()->EvaluateConstraint(mDataFlow, 0);
            isEvaluated = true;
        } else {
            constraint = node.Type()->Constraint();
        }
        V2N_INFO_ASSERT(constraint, node, "invalid constraint");
        VectorNetFlags vflags = constraint->IsSignedRange() ?
            VectorNetFlags(eSignedSubrange) :
            VectorNetFlags(eUnsignedSubrange);
        ConstantRange cr(constraint->NumBitsOfRange() - 1, 0);
        net = new NUVectorNet(netName, cr, flags, vflags, scope, src_loc);
        if (isEvaluated) {
            delete constraint;
        }
    } else if (node.Type()->IsRealType()) {
        VectorNetFlags vflags(eNoneVectorNet);
        ConstantRange cr(63, 0);
        flags = NetFlags(flags | eDMRealNet);
        net = new NUVectorNet(netName, cr, flags, vflags, scope, src_loc);
    } else if (node.Type()->IsEnumerationType()) {
        if ((2 == node.Type()->NumOfEnums() || node.Type()->IsStdULogicType()) && !node.Type()->GetAttributeId("enum_encoding")) {
            net = new NUBitNet(netName, flags, scope, src_loc);
        } else {
            ConstantRange* cr = 0;
            if (node.Type()->GetAttributeId("enum_encoding")) { //misc/enum5.vhdl
                //get element range
                VhdlAttributeSpec* spec = node.Type()->GetAttributeId("enum_encoding")->FindRelevantAttributeSpec(node.Type());
                if (spec && spec->GetValue()->IsStringLiteral()) {
                    UtString enumStr(spec->GetValue()->Name());
                    UtString::iterator f = enumStr.begin();
                    UtString::iterator l = enumStr.end();
                    UInt32 bitness = 0;
                    for (; f != l; ++f, ++bitness) {
                        if (*f == ' ') {
                            break;
                        }
                    }
                    //verific get string with \" so we have to decriment
                    cr = new ConstantRange(bitness - 2, 0);
                } else {
                    cr = new ConstantRange(node.Constraint()->NumBitsOfRange() - 1, 0);
                }
            } else {
                cr = new ConstantRange(node.Constraint()->NumBitsOfRange() - 1, 0);
            }
            INFO_ASSERT(cr, "invalid range for vector net");
            VectorNetFlags vflags(eNoneVectorNet);
            net = new NUVectorNet(netName, cr, flags, vflags, scope, src_loc);
        }
    } else {
        V2N_INFO_ASSERT(false, node, "not supported yet");
    }
    V2N_INFO_ASSERT(net, node, "failed to create NUNet");
    mCurrentScope->sValue(net);
    registerNucleusObject(net);
    return true;
}

} //namespace verific2nucleus
