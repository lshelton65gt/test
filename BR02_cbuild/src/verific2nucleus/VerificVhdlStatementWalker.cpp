// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "verific2nucleus/VerificVhdlDesignWalker.h"    // Visitor base class definition
#include "verific2nucleus/VerificNucleusBuilder.h"      // Nucleus builder class definition
#include "verific2nucleus/V2NDesignScope.h"         // Scope class definition
#include "verific2nucleus/Verific2NucleusUtilities.h"   // Utilities for building nucleus
#include "verific2nucleus/VerificDesignWalkerCB.h" // Included for MACRO VHDL_VISIT_CALL

#include "Array.h"              // Make dynamic array class Array available
#include "Set.h"
#include "Strings.h"            // A string utility/wrapper class
#include "Message.h"            // Make message handlers available, not used in this example

#include "VhdlUnits.h"          // Definition of a libraries and design units
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations
#include "VhdlExpression.h"     // Definitions of all vhdl expressions
#include "VhdlStatement.h"      // Definitions of all vhdl statements
#include "VhdlIdDef.h"          // Definitions of all vhdl identifiers
#include "VhdlName.h"           // Definitions of all vhdl names
#include "VhdlConfiguration.h"  // Definitions of all vhdl configurations
#include "VhdlSpecification.h"  // Definitions of all vhdl specifications
#include "VhdlMisc.h"           // Definitions of miscellaneous vhdl constructrs
#include "VhdlScope.h"
#include "VhdlValue_Elab.h"
#include "vhdl_tokens.h"
#include "vhdl_file.h"
#include "veri_file.h"
#include "VhdlDataFlow_Elab.h"
#include "VhdlCopy.h"

#include "nucleus/NUBase.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUCompositeLvalue.h"
#include "util/AtomicCache.h"
#include "util/CarbonAssert.h"

#include <iostream>

#ifndef CARBON_UNUSED
#define CARBON_UNUSED(x) (void)x; 
#endif // CARBON_UNUSED

//#define DEBUG_VERIFIC_VHDL_WALKER 1

namespace verific2nucleus {

  using namespace Verific ;


/**
 *@brief VhdlCaseItemScheme
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlCaseItemScheme, node)
{
    DEBUG_TRACE_WALKER("VhdlCaseItemScheme", node);
    (void)node;
}

// FD I think: Non-visitor (VERI_VISIT) functions will be good to separate into utilities file
void VerificVhdlDesignWalker::unrollLoop(VhdlLoopStatement* node)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "Unroll Loop" << std::endl;
#endif
    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node->Linefile());
    NUScope* block_scope = getBlockScope();
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(block_scope, loc, "invalid block scope");
    //handle loop iter scheme
    NUBlock* body_block = mNucleusBuilder->cBlock(block_scope, loc);
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(body_block, loc, "Unable to create body block");
    pushDesignScope(body_block, 0);

    // VhdlIterScheme is a base class for iteration schemes (while/for/if)
    VhdlForScheme* fScheme = dynamic_cast<VhdlForScheme*>(node->GetIterScheme());
    // FD I think: we do support only for scheme at this point (can unrollLoop be called in other contexts ?)
    // Anyway better way to handle this will be unsupported language construct message
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(fScheme, *node, "invalid for scheme");
    VhdlDiscreteRange* range = fScheme->GetRange();
    if (!range) {
        // cloutier thinks: what vhdl FOR loop statement results in a non-existent range?
        // single range just add loop statements once
        addLoopStatements(node, body_block);
    } else {
        range->Accept(*this);
        UtPair<SInt32, SInt32> p = mCurrentScope->gRange();
        expandAndAddLoopStatements(node, body_block, fScheme, p);
    }
    // Pop the loop body block scope from the stack
    popScope();
    
    mCurrentScope->sValue(body_block);
}

class VerificUnrollLoopAnalyzer: public Verific::VhdlVisitor
{
public:
     VerificUnrollLoopAnalyzer(VhdlDataFlow* df) : mIsNeedUnroll(false), mDataFlow(df)
        {}
    ~VerificUnrollLoopAnalyzer()
        {}
public:
    bool isNeedUnroll() const
        {
            return mIsNeedUnroll;
        }

    void traverseExpr(VhdlLoopStatement& node)
        {
	  VhdlForScheme* fScheme = static_cast<VhdlForScheme*>(node.GetIterScheme());
	  INFO_ASSERT(fScheme, "invhalid for scheme");
	  VhdlDiscreteRange* range = fScheme->GetRange();
	  // We can only unroll loops with a range whose bounds are constant values.
          if (!range || !Verific2NucleusUtilities::isConstantRange(range, mDataFlow)) {
	    mIsNeedUnroll = false;
	    return;
	  }
	  else
	    node.Accept(*this);
        }

private:
    bool IsUnConstrained(VhdlIndexedName* iName)
        {
            unsigned i = 0;
            VhdlDiscreteRange* assoc = 0;
            VhdlIdDef* id = 0;
            FOREACH_ARRAY_ITEM(iName->GetAssocList(), i, assoc) {
                id = assoc->FindFullFormal() ;
                if (id) {
                    if (!id->Constraint()) {
                        return true;
                    } else {
                        if (id->Constraint()->IsUnconstrained()) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    void VHDL_VISIT(VhdlSignalAssignmentStatement, node)
        {
            //DEBUG_TRACE_WALKER("VhdlSignalAssignmentStatement", node);
            unsigned i = 0;
            VhdlExpression* elem = 0;
            FOREACH_ARRAY_ITEM(node.GetWaveform(), i, elem) {
                if (elem->IsFunctionCall()) {
                    mIsNeedUnroll = true;
                    break;
                }
                elem->Accept(*this);
            }
        }
    void VHDL_VISIT(VhdlWaveformElement, node)
        {
            //DEBUG_TRACE_WALKER("VhdlWaveformElement", node);
            node.GetValue()->Accept(*this);
        }
    void VHDL_VISIT(VhdlOperator, node)
        {
            //DEBUG_TRACE_WALKER("VhdlOperator", node);
            /*VhdlIdDef* opId = node.GetOperator();
         if (opId && opId->IsPredefinedOperator()) {
             mIsNeedUnroll = true;
             }*/
          VhdlExpression* leftExpr = node.GetLeftExpression();
          VhdlExpression* rightExpr = node.GetRightExpression();

	  //TODO clean up hardcoded
          if (leftExpr) {
              if (leftExpr->IsFunctionCall()) {
                  if (VhdlIndexedName* iName = dynamic_cast<VhdlIndexedName*>(leftExpr)) {
                      if (UtString(iName->GetPrefix()->Name()) != UtString("endfile")) {
                          mIsNeedUnroll = true;
                      }
                  }
              } else {
                  leftExpr->Accept(*this);
              }
          }
          if (rightExpr) {
              if (rightExpr->IsFunctionCall()) {
                  mIsNeedUnroll = true;
              } else {
                  rightExpr->Accept(*this);
              }
          }
        }
    void VHDL_VISIT(VhdlLoopStatement, node)
        {
            //DEBUG_TRACE_WALKER("VhdlLoopStatement", node);
            unsigned i = 0;
            VhdlTreeNode* item = 0;
	    
            FOREACH_ARRAY_ITEM(node.GetStatements(), i, item) {
                item->Accept(*this);
            }
        }
    
    void VHDL_VISIT(VhdlVariableAssignmentStatement, node)
        {
            //DEBUG_TRACE_WALKER("VhdlVariableAssignmentStatement", node);
            if (node.GetValue()->IsFunctionCall()) {
                if (VhdlIndexedName* iName = dynamic_cast<VhdlIndexedName*>(node.GetValue())) {
                    if (IsUnConstrained(iName)) {
                        mIsNeedUnroll = true;
                    }
                }               
                mIsNeedUnroll = true;
            } else {
                node.GetValue()->Accept(*this);
            }
        }
    
private:
    bool mIsNeedUnroll;
    VhdlDataFlow* mDataFlow;
};

/**
 *@brief VhdlLoopStatement
 *
 * loop_statement ::=
 *             [ loop_label : ]
 *                    [ iteration_scheme ] LOOP
 *                           sequnce of statements
 *                    END LOOP [ loop_label ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlLoopStatement, node)
{
  DEBUG_TRACE_WALKER("VhdlLoopStatement", node);

  // FD I think: This kind of code will be eliminated when we switch from INFO_ASSERT to VERIFIC_CONSISTENCY checks. Also appling same policy for module items as in Verilog. 
  if (hasError()) {
      mCurrentScope->sValue(0);
      return ;
  }

  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  //get current scope object
  NUScope* block_scope = getBlockScope();
  NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(block_scope, loc, "NULL block scope");
  //create block , which will contain loop
  NUBlock* loop_block = mNucleusBuilder->cBlock(block_scope, loc);
  // Create new V2NDesignScope object. This must be done before loop processing,
  // whether the loop is unrolled or not.
  //
  // This object will contain all extra information
  //               : parent scope
  //               : current nucleus scope objec
  //               : current nucleus scope type
  // as well this class provide all propagation methods for all Vhdl nodes inside this loop
  pushDesignScope(loop_block, 0);
  // save this loopblock
  // this infomation is needed for exit statement processing, to pop 
  // usage is isLoopBlock() method in ExptStatement
  // FD I think: we should review this approach, when we come back to implement similar stuff for SystemVerilog (exit, continue, return)
  mCurrentScope->sLoopBlock();

  // if we get here it means that the FOR loop will be populated as a FOR loop, thus it is not unrolled

  // FD: I think below section is for cases where inside for loop there is constraints/functions which requires call of ElaborateSubprogram or it's equivalent code 
  // FD: the part below seemed to be very sensitive and moving it above if (exprWalker.IsNeedUnroll()) caused memory leak and other failures in completely different places, I don't understand logic, leaving part as it is, in order not to have regressions
  // FD: So the open question remains for this part is : Why we don't need below dataflow creation/insert for loop unroll and why doing so causes failures in other places?
  Array data_flows(1);
  VhdlDataFlow* curDF = mDataFlow;
  mDataFlow = Verific2NucleusUtilities::setupLoopDataFlow(&data_flows, curDF, &node);
  
  // FD: calling VhdlForScheme visitor (which populates, for loop init, condition, increment/decrement)
  // populate loop iter scheme
  node.GetIterScheme()->Accept(*this);
  // get populated iter scheme
  NUFor* the_loop = dynamic_cast<NUFor*>(mCurrentScope->gValue());
  // we only support for scheme
  if (! the_loop) {
      //failed to create statement case15.vhdl
      popScope();
      NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(the_loop, loc, "Unable to get loop statement");
  }
  // get nucleus scope object
  NUScope* loop_block_scope = getBlockScope();
  NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(loop_block_scope, loc, "Unable to get scope");
  //create body block
  NUBlock* body_block = mNucleusBuilder->cBlock(loop_block_scope, loc);
  //add body block to loop
  the_loop->addBodyStmt(body_block);
  //create V2NDesignScope for body block and push to top of stack
  pushDesignScope(body_block, 0);
  // Adding body statements to 'body_block'
  addLoopStatements(&node, body_block);
  // restore parent dataflow
  mDataFlow = curDF;
  // merge dataflow branch with parent (invalidate id's) and cleanup branch dataflows;
  Verific2NucleusUtilities::cleanUpDataFlows(&node, &data_flows, curDF);
  //pop loop body block scope from stack
  popScope();
  loop_block->addStmt(the_loop);
  //pop loop block scope from stack
  popScope();
  //save value for caller
  mCurrentScope->sValue(loop_block);
}

/**
 *@brief VhdlNullStatement
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlNullStatement, node)
{
    DEBUG_TRACE_WALKER("VhdlNullStatement", node);
    CARBON_UNUSED(node)
    mCurrentScope->sValue(0);
}


//RJC RC#2013/08/13 (edvard)  I am not sure I understand the logic in this method, it looks like the error Verific2NUAttributeNotOnClock is called
// under the case where left or right is ID_VHDLATTRIBUTENAME and the other expression is ID_VHDLOPERATOR.
// so I am guessing that if the other expression is ID_VHDLOPERATOR, then that means that it is not a clock and the message can be printed?
//Does this mean that if the classID is ANYTHING else then it must be a clock? (what if it is a ID_VHDLEDGE?)  It seems to me that what we
//should be checking for is that one term is and ID_VHDLATTRIBUTENAME, and that the other is actually a clock, and if it is not a clock then
//the message can be reported.

/**
 *@brief VhdlBlockStatement
 *
 *block_statement ::=
 *            block_label :
 *                 BLOCK [ (guard_expression) ] [ IS ]
 *                         block_header
 *                         block_declarative_part
 *                 BEGIN
 *                         block_statement_part
 *                 END BLOCK [ block_lable ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlBlockStatement, node)
{
  DEBUG_TRACE_WALKER("VhdlBlockStatement", node);
  SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  if (node.GetGuard()) {
      mGuard = node.GetGuard();
      //check for attribute binding
      if (ID_VHDLOPERATOR == node.GetGuard()->GetClassId()) {
          VhdlOperator* op = static_cast<VhdlOperator*>(node.GetGuard());
          INFO_ASSERT(op, "invalid VhdlOperator");
          VhdlExpression* left = op->GetLeftExpression();
          VhdlExpression* right = op->GetRightExpression();
          if (left && right) {
              if (ID_VHDLOPERATOR == left->GetClassId() &&
                  ID_VHDLATTRIBUTENAME == right->GetClassId()) {
                  mNucleusBuilder->getMessageContext()->Verific2NUAttributeNotOnClock(&source_locator);
                  mCurrentScope->sValue(NULL);
                  setError(true);
                  return ;
              } else if (ID_VHDLOPERATOR == right->GetClassId() &&
                         ID_VHDLATTRIBUTENAME == left->GetClassId()) {
                  mNucleusBuilder->getMessageContext()->Verific2NUAttributeNotOnClock(&source_locator);
                  mCurrentScope->sValue(NULL);
                  setError(true);
                  return ;
              }
          }
      }
  }
  if(node.GetPorts() || node.GetGenerics()) {
      mNucleusBuilder->getMessageContext()->Verific2NUPortGenericInBlock(&source_locator);
      mCurrentScope->sValue(NULL);
      setError(true);
      return ;
  }
  NUBlock* block_scope = NULL;
  NUNamedDeclarationScope* declaration_scope = NULL;
  if (! Verific2NucleusUtilities::isElabCreatedEmpty(&node)) {
      if (blockShouldBeCreated(node.GetStatements())) {
          // create block
          NUScope* parent_block_scope = getBlockScope();
          NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(parent_block_scope, source_locator, "Unable to get parent block scope"); 
          block_scope = mNucleusBuilder->cBlock(parent_block_scope, source_locator);
          // Unrolled loops create blocks inside subprograms and process (otherwise, blocks
          // in these scopes are illegal). If this is a block from an unrolled loop, treat
          // it as any other statement, and avoid adding it to the parent scope. 
          if (!Verific2NucleusUtilities::isInProcessOrSubprogram(node.GetLabel()))
             mNucleusBuilder->aStmtToScope(block_scope, parent_block_scope, source_locator);
      }
      //if label exists , than create nameddeclaration scope 
      //RBS: I believe Vhdl blocks must always have labels.
      if (node.GetLabel()) {
          //get parent scope
          NUScope* parent_declaration_scope = getDeclarationScope();
          NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(parent_declaration_scope, source_locator, "Unable to get declaration scope");
          UtString declaration_scope_name;
          if (node.GetLabel()->Name()) { 
              declaration_scope_name = node.GetLabel()->Name();
          } else {
              //RBS: I don't think it's legal for a block to be unlabelled.
              //no explicit name specified for block but there are declarations
              // generate unique name
              UtString prefix;
              prefix << "named_decl_lb_L" << source_locator.getLine();
              StringAtom* blockNameId = mNucleusBuilder->gNextUniqueName(parent_declaration_scope, prefix.c_str());
              declaration_scope_name = blockNameId->str();
              //TODO we probably need to push other uniquifed names as well to name registry
              mNameRegistry[&node] = blockNameId;
          }
          declaration_scope = mNucleusBuilder->cNamedDeclarationScope(declaration_scope_name, parent_declaration_scope, source_locator);
      }
      if (declaration_scope || block_scope) {
          pushDesignScope(block_scope, declaration_scope);
      }
  } else {
      return; // I think no need to populate anything if it's elab created empty block
  }
  unsigned i;
  VhdlTreeNode* item;
  //populate decl part
  FOREACH_ARRAY_ITEM(node.GetDeclPart(), i, item) {
      if (! item) { 
          continue;
      }
      if (VhdlDeclaration* decl = dynamic_cast<VhdlDeclaration*>(item)) {
          //subprograms handled in VerificVhdlSubprogram.cpp file
          if (decl->IsSubprogramBody()) {
              continue;
          }
      }
      //populate declaration
      item->Accept(*this);
  }
  
  i = 0;
  VhdlStatement* stmt;
  //iterate through all statements and populate
  FOREACH_ARRAY_ITEM(node.GetStatements(), i, stmt) {
      if ( ! stmt ) continue;
      mCurrentScope->sValue(NULL);
      stmt->Accept(*this);
      NUStmt* nuStmt = dynamic_cast<NUStmt*>(mCurrentScope->gValue());
      // FD thinks: This strategy seems outdated (for all statements)
      // Each statement can add itself to parent scope (as parent scope (either declaration or block) is always available with new approach)
      // At the moment I can't imagine case where this shouldn't be possible (the same as well is applicable for 
      // verific verilog population flow), so seems like good code refactoring TODO
      if (nuStmt) mNucleusBuilder->aStmtToBlock(nuStmt, block_scope);
  }

  if (node.GetGuard()) {
      //Clck edge on guarded block statement is currently
      //unsupported
      //produce error
  }
  if (declaration_scope || block_scope) {
      //pop scope corresponding to above created nameddeclaration scope and/or block scope
      popScope();
      // Unrolled loops create blocks inside subprograms and process (otherwise, blocks
      // in these scopes are illegal). If this is a block from an unrolled loop, treat
      // it as any other statement, and put the statement in the value variable for use
      // by the parent of this statement.
      if (Verific2NucleusUtilities::isInProcessOrSubprogram(node.GetLabel()))
        mCurrentScope -> sValue(block_scope);
  }
}

/**
 *@brief VhdlWhileScheme
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlWhileScheme, node)
{
  DEBUG_TRACE_WALKER("VhdlWhileScheme", node);
  SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  //create induction variable
  NUStmtList initial_list;
  NUStmtList advance_list;
  NUStmtList body;
  //create condition for while loop
  mCurrentScope->sContext(V2NDesignScope::UNKNOWN_CONTEXT);
  //populate condition of while loop
  NUBase* base = 0;
  if (! node.GetCondition()) {
      //if condition not exists then we get out from loop using exit statement
      //eg loop
      //        statements
      //        exit condition
      //   end loop
      base = NUConst::createKnownIntConst(1, 1, false, source_locator);
  } else {
      node.GetCondition()->Accept(*this);
      //get populated condition
      base = mCurrentScope->gValue();
      V2N_INFO_ASSERT(base, node, "Failed to get while loop condition");
  }
  //while condition should be NUExpr
  NUExpr* cond = dynamic_cast<NUExpr*>(base);
  V2N_INFO_ASSERT(cond, node, "Failed to get while loop condition");
  //we populate whileScheme as a special type of forscheme
  NUFor* the_for = new NUFor(initial_list, cond, advance_list, body,
  			     mNucleusBuilder->gNetRefFactory(), false, source_locator);
  INFO_ASSERT(the_for, "invalid for loop object");
  //save for caller
  mCurrentScope->sValue(the_for);
}

/**
 *@brief VhdlForScheme
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlForScheme, node)
{
    DEBUG_TRACE_WALKER("VhdlForScheme", node);
    SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    //create induction variable
    //implementation in VerificVhdlUtils.cpp
    NUNet* induction = cInductionVar(node.GetIterId());
    INFO_ASSERT(induction, "invalid induction net");
    NUStmtList initial_list;
    NUExpr* cond;
    NUStmtList advance_list;
    NUStmtList body;
    //get start and end values for forscheme
    if (node.GetRange()) {
        //populate range for getting start and end iteration values
        node.GetRange()->Accept(*this);
        //get start and end values for ForScheme
        UtPair<SInt32, SInt32> p = mCurrentScope->gRange();
        //create expressions
        //1.first
        //2.last
        //3.condition
        forloop_first_last_step(p,
                                node,
                                induction, 
                                initial_list,
                                cond,
                                advance_list);
    }
    NUFor* the_for = new NUFor(initial_list, cond, advance_list, body,
                               mNucleusBuilder->gNetRefFactory(), false, source_locator);
    INFO_ASSERT(the_for, "invalid for loop object");
    //save for loop object for caller
    mCurrentScope->sValue(the_for);
}

/**
 *@brief VhdlCaseStatementAlternative
 *
 * case_statement_alternative ::=
 *                    WHEN choices =>
 *                           sequence of statements
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlCaseStatementAlternative, node)
{
  DEBUG_TRACE_WALKER("VhdlCaseStatementAlternative", node);
  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  //get selection value eg case control is -> return expression corresponding to control
  NUExpr* caseSel = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
  INFO_ASSERT(caseSel, "invalid selection value");
  bool select_sign = caseSel->isSignedResult(); 

  //reset current value
  mCurrentScope->sValue(0);
  unsigned i = 0;
  VhdlStatement* statement = 0;

  //make the case item
  NUCaseItem* case_item = new NUCaseItem(loc);
  INFO_ASSERT(case_item, "invalid item");
  FOREACH_ARRAY_ITEM(node.GetStatements(),i, statement) {
      //populate statements
      if (!statement) continue;
      statement -> Accept(*this);
      //get populated statement
      //in general number of statements can be more than one

      NUStmt* cur_stmt = dynamic_cast<NUStmt*>(mCurrentScope -> gValue());
      //if cur_stmt is null it means there is more than one statement,
      //which we will returned usgin gValueVecot method
      if(cur_stmt){
          //add statement to case item
          case_item -> addStmt(cur_stmt);
      } else {
          //if there is more than one statment
          UtVector<NUBase*> vec;
          //get populated statements
          mCurrentScope->gValueVector(vec);
          UInt32 s = vec.size();
          for (UInt32 j = 0; j < s; ++j) {
              if (vec[j]) {
                  //add statement to case item
                  case_item->addStmt(dynamic_cast<NUStmt*>(vec[j]));
              }
          }
      }
  }

  //get the case choices
  VhdlDiscreteRange* choice = 0;
  FOREACH_ARRAY_ITEM(node.GetChoices(), i , choice) {
    if (!choice) continue;
    if (ID_VHDLOTHERS != choice->GetClassId()) {
        //populate choices
        choice -> Accept(*this);
        //get the case choice value
        NUExpr* cur_expr = static_cast<NUExpr*>(mCurrentScope -> gValue());
        bool isRangeExpr = false;
        //if cur_expr is null it means there is more than one choice or a range
        //e.g.: when 400 downto 12
        if(! cur_expr) {
            //we suspect that range is constant
            UtVector<NUBase*> val;
            mCurrentScope->gValueVector(val);
            INFO_ASSERT(2 == val.size(), "");
            INFO_ASSERT(! val[0], "");
            INFO_ASSERT(! val[1], "");
            //get start and end values for range , eg 400 and 12
            SInt32 start = mCurrentScope->gRange().first;
            SInt32 end = mCurrentScope->gRange().second;
            bool isDownTo = (start > end);
            if ( isDownTo ){
              std::swap(start,end);              // swap values so that it looks like it was written as a 'to'
              isDownTo = false;
            }

            //create corresponding constant expressions for 400 and 12
            NUExpr* the_start = NUConst::createKnownIntConst(start, 32, false, loc);
            the_start->setSignedResult(select_sign);
            NUExpr* the_end = NUConst::createKnownIntConst(end, 32, false, loc);
            the_end->setSignedResult(select_sign);

            CopyContext copy_context(NULL,NULL);
            // TODO: the following does not appear to support enum_encoded string values

            NUExpr* selection_value1 = caseSel->copy(copy_context);
            NUExpr* selection_value2 = caseSel->copy(copy_context);

            //create two binary operators for left and right side
            //eg (12 < select_expression)   (select_expression < 400)
            INFO_ASSERT(!isDownTo, "This implementation requires that range has been setup to be of 'to' style");
            NUExpr* bin1 = new NUBinaryOp( (select_sign ? NUOp::eBiSLte  : NUOp::eBiULte), the_start,        selection_value2,  loc);
            NUExpr* bin2 = new NUBinaryOp( (select_sign ? NUOp::eBiSLte  : NUOp::eBiULte), selection_value1, the_end,           loc);
            //create final condition based on range and selective values
            //eg (12 < control) && (control < 400)
            cur_expr = new NUBinaryOp(NUOp::eBiLogAnd, bin1, bin2, loc);
            isRangeExpr = true;
        }
        NUCaseCondition* case_condition = new NUCaseCondition(loc, cur_expr, NULL/*mask*/, isRangeExpr);
        case_item -> addCondition(case_condition);
    }
  }
  mCurrentScope -> sValue(case_item); // present the resulting expression
}

/**
 *@brief VhdlExitStatement
 *
 *exit_statement ::= [ label : ] EXIT [ loop_label ] [ WHEN condition ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlExitStatement, node)
{
   DEBUG_TRACE_WALKER("VhdlExitStatement", node);
   SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

   // Steve Lim: 2013-12-17: If there has been an earlier error, we should not go on and hit asserts
   // FD:  when we switch everywhere to CONSISTENCY checks instead of assertion below pattern will be removed from code
   if (hasError()) 
     return;

   NUIf* the_if = 0;
   if (node.GetCondition()) {
       //populate exit condition
       node.GetCondition()->Accept(*this);
       //get populated condition
       NUExpr* condition = static_cast<NUExpr*>(mCurrentScope->gValue());
       INFO_ASSERT(condition, "invalid expression");
       //create if statement based on populated condition
       the_if = new NUIf(condition, mNucleusBuilder->gNetRefFactory(), false, source_locator);
       INFO_ASSERT(the_if, "invalid if statement");
       mIfStmt.insert(UtPair<NUExpr*, NUBase*>(condition, the_if));
   }
   //implement exit statement
   //if we inside the loop get V2NDesignScope corresponding the top of loop
   NUBlock* block = getLoopTopBlock();
   VERIFIC_CONSISTENCY_NO_RETURN_VALUE(block, node, "invalid block");
   // FD I think: name here isn't always empty it may contain label (loop label) which we are going to exit, so if it's not directly available in parse-tree api we should annotate it while visiting for loop, adding TODO:
   UtString name("");
   //create exit statement
   NUBreak* br = mNucleusBuilder->cBreak(block, "EXIT", name, source_locator);
   // FD I think: I'm seeing too much of this pattern of checking new result versus 0, at least standard C++ new (not placement new), if it is unable to allocate memory it will throw exception instead of returning 0. I think this is true also for Carbon memory allocator (in interra flow as far as I can see there is no checkings of new result versus 0, BTW will be good to get confirm of this from documentation) and so if this is true - all new result checkings should be removed from everywhere
   //INFO_ASSERT(br, "invalid break object");
   if (the_if) {
       //if 'condition' exists for exit
       //than add exit statement
       the_if->addThenStmt(br);
       mCurrentScope->sValue(the_if);
   } else {
       //there is no 'condition' for exit
       mCurrentScope->sValue(br);
   }
}

/**
 *@brief VhdlNextStatement
 *
 *next_statement ::= [ label : ] NEXT [ loop_label ] [ WHEN condition ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlNextStatement, node)
{
   DEBUG_TRACE_WALKER("VhdlNextStatement", node);
   SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

   // Steve Lim: 2013-12-17: If there has been an earlier error, we should not go on and hit asserts
   if (hasError()) 
     return;

   NUIf* the_if = 0;
   if (node.GetCondition()) {
       //populate next condition
       node.GetCondition()->Accept(*this);
       //get populated condition
       NUExpr* condition = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
       INFO_ASSERT(condition, "invalid expression");
       //create if statement based on populated condition
       the_if = new NUIf(condition, mNucleusBuilder->gNetRefFactory(), false, source_locator);
       INFO_ASSERT(the_if, "invalid if statement");
       mIfStmt.insert(UtPair<NUExpr*, NUBase*>(condition, the_if));
   }
   // FD thinks: Don't we need to find loop scope like in EXIT case ? Getting current scope seems unsafe ?iii well same concern about name: it's not always empty - it's loop label name which we want to 'continue'
   UtString name("");
   //get current block
   NUBlock* block = getBlock();
   NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(block, source_locator, "Unable to get parent block");
   //implement break statement
   NUBreak* br = mNucleusBuilder->cBreak(block, "NEXT", name, source_locator);
   if (the_if) {
       //if 'condition' exists for next
       //than add next statement
       the_if->addThenStmt(br);
       mCurrentScope->sValue(the_if);
   } else {
       //there is no 'condition' for next
       mCurrentScope->sValue(br);
   }
}

/**
 *@brief VhdlCaseStatement
 *
 * case_statement ::=
 *                [ case_lable : ]
 *                    CASE expression IS
 *                         case_statement_alternative
 *                         { case_statement_alternative }
 *                    END CASE [ case_label ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlCaseStatement, node)
{
    DEBUG_TRACE_WALKER("VhdlCaseStatement", node);
    //get source locator.
    SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    NUExpr* selection_value=NULL;
    unsigned i=0;
    VhdlTreeNode* item=NULL;
    VhdlExpression* case_expr = node.GetExpression();
    // Case expression should always exist
    // FD thinks, it can't not exist originally (as would have reported at parser level) or unless incorrectly updated
    // during static elaboration.
    INFO_ASSERT(case_expr, "NULL case expression encountered");
    // Check for constant condition
    bool const_case_cond = case_expr->IsConstant();
    // Set context for case expression
    mCurrentScope->sContext(V2NDesignScope::RVALUE);
    // get the selection expression result.
    case_expr -> Accept(*this);
    selection_value = dynamic_cast<NUExpr*>(mCurrentScope-> gValue());
    INFO_ASSERT(selection_value, "invalid selection value for case statement");

    NUCase* case_statement = new NUCase(selection_value,
            mNucleusBuilder->gNetRefFactory(),
            NUCase::eCtypeCase,
            true,
            source_locator);
    //check from pragmas...
    case_statement->putUserFullCase(true);
    case_statement->putFullySpecified(false);

    bool hasDefault = false;

    Array data_flows(node.GetAlternatives()->Size() + 1);
    VhdlDataFlow* curDF = mDataFlow;
    FOREACH_ARRAY_ITEM(node.GetAlternatives(),i,item){
        // We need to setup dataflow only in case of non-constant case condition expression
        if (!const_case_cond) {
            mDataFlow = Verific2NucleusUtilities::setupBranchDataFlow(&data_flows, curDF);
        }
        // FD thinks: Unusual way to pass value to child visitors. Mostly we do set value at the end of visitor to pass value to parent visitor, and for child visitors we keep contextual fields in V2NDesignScope. This seems candidate for refactoring to use consistent way of passing contextual information (possibly using separate field on V2NDesignScope)
        //sent condition expression to items for using in case for example :when 10 downto 2 
        mCurrentScope->sValue(selection_value);
        //process the alternative
        item -> Accept(*this);
        // eg.
        //     when others => assert false
        //                    report "others"
        //                    severity note;
        if (mCurrentScope->gValue()) {
            NUCaseItem* case_item = static_cast<NUCaseItem*>(mCurrentScope->gValue());
            //add case item
            case_statement -> addItem(case_item);
            if(case_item->isDefault()) {
                hasDefault = true;
            }
        }
    }
    // cleanup data_flows and invalidate id's (branch merging with parent(\a curDF) dataflow)
    Verific2NucleusUtilities::cleanUpDataFlows(&node, &data_flows, curDF);
    // restore parent dataflow
    mDataFlow = curDF;
    // set default flag
    case_statement->putHasDefault(hasDefault);
    //set the current value of the scope.
    //let parent add it to the statement block
    mCurrentScope -> sValue(case_statement);
}

NUExpr* VerificVhdlDesignWalker::convertFallingEdgeCall2EventExpression(VhdlExpression* ifcondition)
{
    //convert
    //      falling_edge(clk) call to
    //      BinOp('and', Attr(clk, event), BinOp('eq', clk, 0))
    SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,ifcondition->Linefile());
    if (ID_VHDLINDEXEDNAME != ifcondition->GetClassId()) {
        return 0;
    }


    VhdlIndexedName* idName = static_cast<VhdlIndexedName*>(ifcondition);
    INFO_ASSERT(idName, "");
    VhdlIdDef* id = idName->GetId();
    if (!Verific2NucleusUtilities::isIeeeDecl(id)) {
       return 0;
    }
    
    if (UtString("falling_edge") != UtString(idName->GetPrefix()->Name())) {
        return 0;
    }
    
    VhdlIdRef* event = 0;
    INFO_ASSERT(1 == idName->GetAssocList()->Size(), "");
    unsigned i = 0;
    VhdlTreeNode* elem = 0;
    FOREACH_ARRAY_ITEM(idName->GetAssocList(), i, elem) {
        INFO_ASSERT(ID_VHDLIDREF == elem->GetClassId(), "");
        event = static_cast<VhdlIdRef*>(elem);
    }
    INFO_ASSERT(event, "invalid VhdlTreeNode pointer");

    NUBase* base = getNucleusObject(event->GetId());
    
    INFO_ASSERT(base, "invalid registered object");
    NUNet* net = dynamic_cast<NUNet*>(base);
    INFO_ASSERT(net, "");
    NUExpr* rval_attr = mNucleusBuilder->createIdentRvalue(net, src_loc);
    NUExpr* rval_bin = mNucleusBuilder->createIdentRvalue(net, src_loc);
    NUExpr *expr = new NUAttribute(rval_attr, NUAttribute::eEvent, src_loc);
    NUExpr* con = NUConst::createKnownConst(UtString("0"), 1, false, src_loc);
    NUBinaryOp* clk_eq_1 = new NUBinaryOp(NUOp::eBiEq, con, rval_bin, src_loc);
    INFO_ASSERT(clk_eq_1, "invalid binary operator");
    NUBinaryOp* clk_and_attr_event = new NUBinaryOp(NUOp::eBiBitAnd, clk_eq_1, expr, src_loc);
    return clk_and_attr_event;
}

NUExpr* VerificVhdlDesignWalker::convertRisingEdgeCall2EventExpression(VhdlExpression* ifcondition)
{
    //convert
    //      rising_edge(clk) call to
    //      BinOp('and', Attr(clk, event), BinOp('eq', clk, 1))
    SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,ifcondition->Linefile());
    if (ID_VHDLINDEXEDNAME != ifcondition->GetClassId()) {
        return 0;
    }

    
    VhdlIndexedName* idName = static_cast<VhdlIndexedName*>(ifcondition);
    INFO_ASSERT(idName, "");
    
    VhdlIdDef* id = idName->GetId();
    if (!Verific2NucleusUtilities::isIeeeDecl(id)) {
       return 0;
    }
    
    if (UtString("rising_edge") != UtString(idName->GetPrefix()->Name())) {
        return 0;
    }
    if (mInsideConditionalSignalAssignment) {
        SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder, ifcondition->Linefile());
        mNucleusBuilder->getMessageContext()->Verific2NUClkInConcStmt(&loc);
        setError(true);
        return 0;
    }
    
    VhdlIdRef* event = 0;
    INFO_ASSERT(1 == idName->GetAssocList()->Size(), "");
    unsigned i = 0;
    VhdlTreeNode* elem = 0;
    FOREACH_ARRAY_ITEM(idName->GetAssocList(), i, elem) {
        INFO_ASSERT(ID_VHDLIDREF == elem->GetClassId(), "");
        event = static_cast<VhdlIdRef*>(elem);
    }
    INFO_ASSERT(event, "invalid VhdlTreeNode pointer");

    NUBase* base = getNucleusObject(event->GetId());
    
    INFO_ASSERT(base, "invalid registered object");
    NUNet* net = dynamic_cast<NUNet*>(base);
    INFO_ASSERT(net, "Unable to get net");
    NUExpr* rval_attr = mNucleusBuilder->createIdentRvalue(net, src_loc);
    NUExpr* rval_bin = mNucleusBuilder->createIdentRvalue(net, src_loc);
    NUExpr *expr = new NUAttribute(rval_attr, NUAttribute::eEvent, src_loc);
    NUExpr* con = NUConst::createKnownConst(UtString("1"), 1, false, src_loc);
    NUBinaryOp* clk_eq_1 = new NUBinaryOp(NUOp::eBiEq, con, rval_bin, src_loc);
    NUBinaryOp* clk_and_attr_event = new NUBinaryOp(NUOp::eBiBitAnd, clk_eq_1, expr, src_loc);
    return clk_and_attr_event;
}

bool VerificVhdlDesignWalker::populateIfStmtWhenConditionIsConstant(VhdlIfStatement& node)
{
    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    VhdlValue* cond = node.GetIfCondition()->Evaluate(0, mDataFlow, 0);

    if (!cond || (cond && !cond->IsConstant())) {
        if (cond) delete cond;
        return false;
    }

    unsigned i;
    VhdlExpression* elsif_cond = 0;
    VhdlElsif* elsif = 0;
    FOREACH_ARRAY_ITEM(node.GetElsifList(), i, elsif) {
        elsif_cond = elsif->Condition();
        if (!elsif_cond) continue;
        VhdlValue* cond_value = elsif_cond->Evaluate(0, mDataFlow, 0);
        if (!cond_value || (cond_value && !cond_value->IsConstant())) {
            delete cond_value;
            delete cond;
            return false;
        }
        delete cond_value;
    }

    NUBlock* parent_block = getBlock();
    NUCLEUS_CONSISTENCY(parent_block, loc, "Failed to get parent block");
    NUBlock* block = mNucleusBuilder->cBlock(parent_block, loc);

    VhdlStatement* stmt = 0;
    if (cond && cond->IsConstant()) {
        if (cond->IsTrue()) {
            pushDesignScope(block, 0);
            //pushScope(scope);
            FOREACH_ARRAY_ITEM(node.GetIfStatements(), i, stmt) {
                mCurrentScope->sValue(NULL); // just in case this branch turns up empty
                stmt->Accept(*this) ;
                NUStmt* curStmt = dynamic_cast<NUStmt*>(mCurrentScope->gValue());
                if (curStmt) {
                    block->addStmt(curStmt);
                } else {
                    UtVector<NUBase*> vec;
                    mCurrentScope->gValueVector(vec);
                    UInt32 s = vec.size();
                    for (UInt32 j = 0; j < s; ++j) {
                        if (vec[j]) {
                            block->addStmt(dynamic_cast<NUStmt*>(vec[j]));
                        }
                    }
                }
            }
            popScope();
            delete cond;
            mCurrentScope->sValue(static_cast<NUStmt*>(block));
            return true;
        }
        delete cond;
    }

    unsigned l = 0;
    FOREACH_ARRAY_ITEM(node.GetElsifList(), i, elsif) {
        elsif_cond = elsif->Condition();
        if (!elsif_cond) continue;
        cond = elsif_cond->Evaluate(0, mDataFlow, 0);
        if (cond && cond->IsConstant()) {
            if (cond->IsTrue()) {
                pushDesignScope(block, 0);
                FOREACH_ARRAY_ITEM(elsif->GetStatements(), l, stmt) {
                    mCurrentScope->sValue(NULL); // just in case this branch turns up empty
                    stmt->Accept(*this) ;
                    NUStmt* curStmt = dynamic_cast<NUStmt*>(mCurrentScope->gValue());
                    if (curStmt) {
                        block->addStmt(curStmt);
                    } else {
                        UtVector<NUBase*> vec;
                        mCurrentScope->gValueVector(vec);
                        UInt32 s = vec.size();
                        for (UInt32 j = 0; j < s; ++j) {
                            if (vec[j]) {
                                block->addStmt(dynamic_cast<NUStmt*>(vec[j]));
                            }
                        }
                    }
                }
                popScope();
                delete cond;
                mCurrentScope->sValue(static_cast<NUStmt*>(block));
                return true;
            }
            delete cond;
        }
    }
    
    cond = node.GetIfCondition()->Evaluate(0, mDataFlow, 0);
    if (cond && cond->IsConstant() && !cond->IsTrue()) {
        pushDesignScope(block, 0);
        FOREACH_ARRAY_ITEM(node.GetElseStatments(), i, stmt) {
            mCurrentScope->sValue(NULL); // just in case this branch turns up empty
            stmt->Accept(*this) ;
            NUStmt* curStmt = dynamic_cast<NUStmt*>(mCurrentScope->gValue());
            if (curStmt) {
                block->addStmt(curStmt);
            } else {
                UtVector<NUBase*> vec;
                mCurrentScope->gValueVector(vec);
                UInt32 s = vec.size();
                for (UInt32 j = 0; j < s; ++j) {
                    if (vec[j]) {
                        block->addStmt(dynamic_cast<NUStmt*>(vec[j]));
                    }
                }
            }
        }
        popScope();
        delete cond;
        mCurrentScope->sValue(static_cast<NUStmt*>(block));
        return true;
    }
    delete block;
    return false;   
}

/**
 *@brief VhdlIfStatement
 *
 * if_statement ::=
 *              [ if_label : ]
 *                   IF condition THEN
 *                         sequence of statements
 *                   { ELSIF condition THEN
 *                         sequence of statements }
 *                   [ ELSE
 *                         sequence of statements ]
 *                   END IF [ if_lable ]
 *
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlIfStatement, node)
{
    DEBUG_TRACE_WALKER("VhdlIfStatement", node);

    if (hasError()) {
        mCurrentScope->sValue(0);
        return ;
    }

    // We need to close this to have correct logic. So by the time we
    // -enablCSElab switch by default some tests might fail. Either need to
    // move them to TDIFF/TEXIT or do -enablCSElab by default 
    // This will be removed as soon as -disableCSElab will be removed
    // Below are some of the dependent tests all from test/rushc/vhdl/beacon_misc directory:
    // misc.elab3, misc.psel18, misc.size1, misc.loop5
    if (!mEnableCSElab) {
        if (populateIfStmtWhenConditionIsConstant(node)) return;
    }

    //get the source locator
    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    VhdlExpression* ifcond = node.GetIfCondition();
    //check condition constantness
    //check inside subprogram
    if (ifcond->FindIfEdge()) {
        if (mIsInSubprogram) {
            SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder, node.Linefile());
            mNucleusBuilder->getMessageContext()->Verific2NUEdgeExprInsideSubprogNotSupported(&loc);
            setError(true);
            return;
        }
    }

    //populate condition
    mCurrentScope->sValue(NULL); // just in case the condition is empty
    mCurrentScope->sContext(V2NDesignScope::RVALUE);
    ifcond->Accept(*this) ;
    //get populated value
    NUBase* base = mCurrentScope -> gValue();
    if (!base) {
        mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&loc, "VhdlIfStatement");
        mCurrentScope->sValue(NULL);
        setError(true);
        return ;
    }
    INFO_ASSERT(dynamic_cast<NUExpr*>(base), "should has NUExpr base");
    NUExpr* expr = static_cast<NUExpr*>(base);
    INFO_ASSERT(expr, "invalid expression for if condition");
    UtString con;
    //create if condition like : (1 != cond_from_tree)
    //eg if (cond1 and cond2) then ....
    //       we create following
    //      (1 != (cond1 and cond2))
    con += (char(0 + '0'));
    NUExpr* the_const = NUConst::createKnownConst(con, 1, false, loc);
    NUExpr* bin_cond = new NUBinaryOp(NUOp::eBiNeq, expr, the_const, loc);
    //store all if conditions in effort to handle edge expressions
    mIfCondition.insert(UtPair<VhdlExpression*, NUExpr*>(ifcond, bin_cond));
    mEdgeValidity.insert(UtPair<VhdlExpression*, NUExpr*>(ifcond, bin_cond));


    Array data_flows(2);
    // save current dataflow
    VhdlDataFlow* curDF = mDataFlow;

    bool short_circuited = false; // True if subsequent branches are guaranteed dead

    // True if any branch previous to the one being analyzed 
    // had a constant 'true' condition. Used for VhdlDataFlow
    // management.
    bool previous_visited_branch = false;

    unsigned i = 0;
    VhdlTreeNode *elem =NULL;

    // Steve Lim Oct 26, 2013:
    // IMPT: Note the difference between the different representations of the if
    // statement between Verific and Nucleus. The key is the presence of the "elsif"
    // syntax in VHDL, which is modelled by Verific VHDL but not by Nucleus.
    // The NU contruction was previously not correctly modeling this difference 
    // Testcases: beacon_sequential/if21, if23
    //
    // The Verific::VhdlIfStatement representation reflects the VHDL syntax and has the
    // "elsif" construct:
    //  + 'condition' : boolean expression
    //  + 'if'        : list of statements
    //  + 'elsif'     : list of VhdlElsif statements
    //  + 'else'      : list of statements (IMPT: Even though this is a data member of
    //                  this if statement, semantically it DOES NOT correspond to its
    //                  else branch if the 'elsif' list is non-empty. It corresponds to
    //                  the false branch of the last of the 'elsif' list (which represents
    //                  only if-then statements. See below.)
    //
    // The Verific::VhdlElsif represents only an if-then with no else part. It occurs only
    // in the elsif part of an if statement.
    //  + 'condition'  : boolean expression
    //  + 'statements' : list of statements corresponding to the then branch
    //
    //  An if-end will have empty 'elsif' and 'else' lists.
    //  An if-else-end will have an empty 'elsif' and a non-empty 'else'.
    //  An if-elsif-end will have a one 'elsif' and an empty 'else'
    //  An if-elsif-elsif-elsif-end will have 3 'elsif' statements and empty 'else'.
    //  An if-elsif-elsif-elsif-else-end will have 3 'elsif' statements and 1 'else' statement.
    //
    //  The NU construction must pay attention to which if statement the 'else' belongs to:
    //  In a deeply nested if-{elsif}-else the last (deepest nested) NUIf has the else part.
    //
    // The Nucleus representation of an IF statement has the more conventional (C style)
    // if-then-else construct:
    //   + 'condition': a boolean expression
    //   + 'then'     : list of statements 
    //   + 'else'     : list of statements
    //
    //  An if-end will have a non-empty 'then' and an empty 'else' part.
    //  An if-else-end will have non empty 'then' and 'else' part.
    //  An if-elsif-end will have an 'else' part containing only one statement
    //  which is the nested IF (modeled by Verific as an VhdlElsif). 
    //  An if-elsif-else-end will be like the above but the nested NUIf has a non-empty
    //  'else' part. If there many elsif's, the else part belongs to the last elsif.
    //

    NUStmtList if_stmts;
    NUStmtList else_stmts;

    NUBlock* if_block = 0;
    NUBlock* else_block = 0;

    // If there are any statements in the if section - not including
    // NULL statements.
    if (!Verific2NucleusUtilities::isArrayEmpty(node.GetIfStatements())) {
        NUScope* parent_block_scope = getBlockScope();
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(parent_block_scope, loc, "Unable to get parent block scope");
        if_block = mNucleusBuilder->cBlock(parent_block_scope, loc);
        pushDesignScope(if_block, 0);
        if_stmts.push_back(static_cast<NUStmt*>(if_block));
        // setup new branch data flow only if it's not a constant
        if (Verific2NucleusUtilities::branchIsLive(node.GetIfCondition(), curDF, &short_circuited)) {
            if (short_circuited && !previous_visited_branch) {
                // This is the ONLY live branch, no new dataflow required
            } else {
                mDataFlow = Verific2NucleusUtilities::setupBranchDataFlow(&data_flows, curDF);
            }
        }
        previous_visited_branch = true; // Forces new dataflows to be created
    }

    FOREACH_ARRAY_ITEM(node.GetIfStatements(), i, elem) {
        if (!elem) continue;
        mCurrentScope->sIfStmtList(&if_stmts);
        mCurrentScope->sValue(NULL); // just in case this branch is empty
        elem->Accept(*this) ;
        NUStmt* stmt =  dynamic_cast<NUStmt*>(mCurrentScope->gValue());
        if (stmt) {
            //there is only one statement
            if_block->addStmt(stmt);
        } else {
            //there are more then one statement
            UtVector<NUBase*> vec;
            mCurrentScope->gValueVector(vec);
            UInt32 s = vec.size();
            for (UInt32 j = 0; j < s; ++j) {
                if (vec[j]) {
                    if_block->addStmt(dynamic_cast<NUStmt*>(vec[j]));
                }
            }
        }
        mCurrentScope->sIfStmtList(NULL);
    }

    if (!Verific2NucleusUtilities::isArrayEmpty(node.GetIfStatements())) {
        popScope();
    }

    NUIf* elsif = 0; // Tracks the last VhdlElsIf
    VhdlElsif* elsif_elem = NULL;
    FOREACH_ARRAY_ITEM(node.GetElsifList(), i, elsif_elem) {
        if (!elsif_elem) continue;
        //setup elsif branch data flow here (and no need to data any data_flow maintencance inside VhdlElsif visitor)
        if (!Verific2NucleusUtilities::isArrayEmpty(elsif_elem->GetStatements())) {
            if (Verific2NucleusUtilities::branchIsLive(elsif_elem->Condition(), curDF, &short_circuited)) {
                if (short_circuited && !previous_visited_branch) {
                    // This is the ONLY live branch, no new dataflow required
                } else {
                    mDataFlow = Verific2NucleusUtilities::setupBranchDataFlow(&data_flows, curDF);
                }
            }
            previous_visited_branch = true;
        }
        mCurrentScope->sValue(NULL); // just in case this branch is empty
        elsif_elem->Accept(*this);
        NUIf* stmt = static_cast<NUIf*>(mCurrentScope->gValue());
        if(stmt) {
            if (elsif) {
                elsif->addElseStmt(stmt);
            } else {
                else_stmts.push_back(stmt);
            }
            elsif = stmt;
        }
    }

    // If there are any else statements (not including NULL statments)
    if (!Verific2NucleusUtilities::isArrayEmpty(node.GetElseStatments())) {
        // Push a new scope
        NUScope* parent_block_scope = getBlockScope();
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(parent_block_scope, loc, "Unable to get parent block scope");
        else_block = mNucleusBuilder->cBlock(parent_block_scope, loc);
        pushDesignScope(else_block, 0);
        if (elsif) {
            elsif->addElseStmt(static_cast<NUStmt*>(else_block));
        } else {
            else_stmts.push_back(static_cast<NUStmt*>(else_block));
        }
        // if we get here than the branch is alive and so assert !short_circuited (this can't happen unless someone updates code!)
        INFO_ASSERT(!short_circuited, "Inconsistency between isArrayEmpty and branchIsLive");
        if (!previous_visited_branch) {
            // This is the ONLY live branch, no new dataflow required
        } else {
            mDataFlow = Verific2NucleusUtilities::setupBranchDataFlow(&data_flows, curDF);
        }
    }
    
    FOREACH_ARRAY_ITEM(node.GetElseStatments(), i, elem) {
        if (!elem) continue;
        mCurrentScope->sIfStmtList(&else_stmts);
        mCurrentScope->sValue(NULL); // just in case this branch is empty
        elem->Accept(*this) ;
        NUStmt* stmt = static_cast<NUStmt*>(mCurrentScope->gValue());
        if(stmt) {
            else_block->addStmt(stmt);
        } else { //there is more then one statement
            UtVector<NUBase*> vec;
            mCurrentScope->gValueVector(vec);
            UInt32 s = vec.size();
            for (UInt32 j = 0; j < s; ++j) {
                if (vec[j]) { 
                    else_block->addStmt(static_cast<NUStmt*>(vec[j]));
                }
            }
        }
        mCurrentScope->sIfStmtList(NULL);
    }

    if (!Verific2NucleusUtilities::isArrayEmpty(node.GetElseStatments())) {
        popScope();
    }

    //create the if statement and return to parent caller via scope.
    NUBase* nu_if = new NUIf(bin_cond, if_stmts, else_stmts,mNucleusBuilder->gNetRefFactory(), false, loc);
    //for complex reset expression
    mIfStmt.insert(UtPair<NUExpr*, NUBase*>(bin_cond, nu_if));


    //return value, parent can add to block as necessary.
    mCurrentScope -> sValue(nu_if);

    // restore parent data flow
    mDataFlow = curDF;
    // cleanup data flows (cleanUpDataFlows also doing InvalidateIdValues, see details in function implementation comments)
    Verific2NucleusUtilities::cleanUpDataFlows(&node, &data_flows, curDF);
}

/**
 *@brief VhdlElsif
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlElsif, node)
{
    DEBUG_TRACE_WALKER("VhdlElsif", node);
    SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());  
    VhdlExpression* cond = node.Condition();

    mCurrentScope->sContext(V2NDesignScope::RVALUE);
    if (cond->FindIfEdge()) {
        //check for inside subprogram
        if (mIsInSubprogram) {
            SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder, node.Linefile());
            mNucleusBuilder->getMessageContext()->Verific2NUEdgeExprInsideSubprogNotSupported(&loc);
            setError(true);
            return;
        }
    }
    
    //populate elsif condition
    cond->Accept(*this);
    NUBase* base = mCurrentScope->gValue();
    INFO_ASSERT(base, "invalid base pointer");
    NUExpr* expr = dynamic_cast<NUExpr*>(base);
    INFO_ASSERT(expr, "elsif condition expr");
    //create elsif condition like : (1 != cond_from_tree)
    //eg elsif (cond1 and cond2) then ....
    //       we create following
    //      (1 != (cond1 and cond2))
    UtString con;
    con += (char(0 + '0'));
    NUExpr* the_const = NUConst::createKnownConst(con, 1, false, source_locator);
    NUExpr* bin_cond = new NUBinaryOp(NUOp::eBiNeq, expr, the_const, source_locator);
    //store all elsif conditions in effort to handle edge expressions
    mIfCondition.insert(UtPair<VhdlExpression*, NUExpr*>(cond, bin_cond));
    mEdgeValidity.insert(UtPair<VhdlExpression*, NUExpr*>(cond, bin_cond));
    
    NUStmtList if_stmts;
    NUStmtList else_stmts;
    unsigned i;
    VhdlTreeNode* item;
    NUBlock* block = 0;
    //populate elsif conditions
    if (!Verific2NucleusUtilities::isArrayEmpty(node.GetStatements())) {
      NUScope* parent_block_scope = getBlockScope();
      NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(parent_block_scope, source_locator, "Unable to get parent block scope");
      block = mNucleusBuilder->cBlock(parent_block_scope, source_locator);
      pushDesignScope(block, 0);
      if_stmts.push_back(static_cast<NUStmt*>(block));
    }
    
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, item) {
        if (!item) continue;
        mCurrentScope->sIfStmtList(&if_stmts);
        item->Accept(*this);
        NUStmt* stmt = dynamic_cast<NUStmt*>(mCurrentScope->gValue());
        if(stmt) { //there is one statement
            block->addStmt(stmt);
        } else { //there is more then one statement
            UtVector<NUBase*> vec;
            mCurrentScope->gValueVector(vec);
            UInt32 s = vec.size();
            for (UInt32 j = 0; j < s; ++j) {
                if (vec[j]) {
                    block->addStmt(dynamic_cast<NUStmt*>(vec[j]));
                }
            }
        }
        mCurrentScope->sIfStmtList(NULL);
    }

    if (!Verific2NucleusUtilities::isArrayEmpty(node.GetStatements())) {
        popScope();
    }

    NUBase* the_if = new NUIf(bin_cond, if_stmts, else_stmts, mNucleusBuilder->gNetRefFactory(), false, source_locator);
    INFO_ASSERT(the_if, "failed to create if stmt");
    mIfStmt.insert(UtPair<NUExpr*, NUBase*>(bin_cond, the_if));
    //save for caller
    mCurrentScope->sValue(the_if);
}

/*
 * @brief Create a Nucleus net for a function return given the Verific typeids and constraint on the net.
 *
 * This does almost exactly what is done when a VHDL declaration is processed. Therefore, we reuse
 * that code by creating a VhdlIdDef for the function return net, and then invoking the VerificVhdlDesignWalker
 * populator on that VhdlIdDef. This mimics what would be done for a declaration.
 *
 * There are a few differences between what we want to do here, and what the VhdlIdDef visitor does:
 *
 * 1) The VhdlIdDef visitor registers the VhdlIdDef in the nucleus object registry, for later retrieval.
 *    We don't need to do that here; but since the VhdlIdDef visitors will register them anyway, we
 *    remove them afterward. (NOT YET IMPLEMENTED) Note that this method may tend to register
 *    the same VhdlIdDef over and over (if it occupies the same address on the stack). In each
 *    case, the previous registry entry will be deleted, and the new one added.
 *
 * 2) The eNonStaticNet and eTempNet flags must be set. These flags are placed on the net AFTER the
 *    VhdlIdDef visitors are done with the population.
 *
 */
NUNet* VerificVhdlDesignWalker::createNucleusNet(VhdlName* returnType, VhdlConstraint* returnConstraint, linefile_type n)
{
    NUModule* module = getModule();
    // Create a name for this temporary return value
    StringAtom* netName = module->gensym("return");
    
    // Declare a temporary VhdlIdDef which we will use to create the id.
    // Has to be an interface id - no other id can hold the OUTPUT mode flag
    VhdlInterfaceId rtnId(Strings::save(netName->str()));
    // Source locator
    rtnId.SetLinefile(n);
    // Create a subtype indication, must be freed later
    VhdlMapForCopy old2new;
    VhdlName* copied_returnType = returnType->CopyName(old2new);
    VhdlSubtypeIndication* subtypeIndication = copied_returnType->CreateSubtypeWithConstraint(returnConstraint);
    rtnId.SetSubtypeIndication(subtypeIndication);
    // Setup the constraint and the type
    rtnId.SetConstraint(returnConstraint->Copy());
    rtnId.DeclareInterfaceId(returnType->GetId(), 
                             VHDL_signal /* VHDL_variable causes carbon internals*/ /* kind */, 
                             VHDL_out /* mode */, 
                             VHDL_register /* signal_kind */, 
                             0 /* has init value */, 
                             returnConstraint->IsUnconstrained() /* is unconstrained */, 
                             NULL /* resolution function */, 
                             0 /* locally static type */, 
                             0 /* globally static type */);
    // Indicate that this is a subprogram parameter (although this doesn't seem to make any difference) 
    rtnId.SetObjectKind(VHDL_parameter);

    // Visit the VhdlIdDef and put the net on the scope stack
    rtnId.Accept(*this);
    delete subtypeIndication;
    // Retrieve the net
    NUBase* base = mCurrentScope->gValue();
    NUNet* net = dynamic_cast<NUNet*>(base);
    if (net) {
      // Function return nets require eNonStaticNet and eTempNet, otherwise
      // we get backend compiler errors.
      // NetFlags new_flags = NetFlags(eAllocatedNet | eTempNet | eBlockLocalNet | eNonStaticNet | eOutputNet);
      NetFlags new_flags = NetFlags(eTempNet | eNonStaticNet);
      NetFlags existing_flags(net->getFlags());
      NetFlags flags = NetFlags(existing_flags | new_flags);
      net->setFlags(flags);
    }

    // TODO: Remove the id (and all record fields - if any) from the registry. 
    return net;
}

/**
 *@brief VhdlReturnStatement
 *
 * return_statement ::= [ label : ] RETURN [ expression ]
 */


// Populate the return statement.
// Carbon Static Elaboration, Constraint Elaboration and subprogram
// uniquification guarantees that the
// return constraint of a function is constrained. 
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlReturnStatement, node) {
  DEBUG_TRACE_WALKER("VhdlReturnStatement", node);
  if (hasError()) {
      mCurrentScope->sValue(0);
      return ;
  }
  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder, node.Linefile());

  if (! node.GetExpression()) {
      NUBlock* block = getSubprogramTopBlock();
      INFO_ASSERT(block, "invalid owner");
      StringAtom* rt = mNucleusBuilder -> gCache() -> intern(UtString("RETURN"));
      INFO_ASSERT(rt, "invalid keyword");
      StringAtom* tt = mNucleusBuilder -> gCache() -> intern(UtString(""));
      //implement break statement
      NUBreak* br = new NUBreak(block, rt, tt, mNucleusBuilder->gNetRefFactory(), loc);
      INFO_ASSERT(br, "invalid break object");
      mCurrentScope->sValue(br);
      return ;
  }
  
  // Populate return statement expression. 
  V2NDesignScope::ContextType parentContext = mCurrentScope->gContext();
  mCurrentScope->sContext(V2NDesignScope::RVALUE);
  node.GetExpression()->Accept(*this);
  NUExpr* retExp = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
  INFO_ASSERT(retExp, "invalid return expression");
  //restore scope type
  mCurrentScope->sContext(parentContext);
  
  // Get Scope pointer for task
  V2NDesignScope* pScope = getTaskV2NDesignScope();
  NUNet* returnNet = pScope->gReturnNet(); // get saved return net , for first call we have null pointer
                                           // since inside function can exists more than one return statement
                                           // we create return net just once for first call
                                           // and will use that net for other too.
                                           // RBS: What should we do if there are multiple return statements,
                                           // each returning a different sized object (e.g. vector)?
                                           // The Interra flow seems to go with the largest return size.

  // RBS thinks: Here's what we're trying to do here:
  // 1) Create a nucleus net object for the return value.
  // 2) To do this, we must first compute the size (constraint) of the return expression.
  //    This gets complicated for expressions with function calls, especially recursive
  //    function calls. 

  // Create a nucleus net object for the function return
  if (!returnNet) {
    VhdlConstraint* returnConstraint = node.GetOwner()->Constraint();
    // Carbon Static Elab (CSE), and in particular VhdlConstraintElaborator and VhdlSubprogUniquification 
    // guarantees that all function return constraints exist, and are constrained. If either of 
    // the following assertions are triggered, it means that CSE failed to visit a function call, and
    // therefore failed to calculate a return constraint, and uniquify the function. Look for
    // the context within the Vhdl RTL that this function call appeared, and verify
    // that CSE is visiting it.
    INFO_ASSERT(returnConstraint, "Unexpected NULL function return constraint");
    INFO_ASSERT(!returnConstraint->IsUnconstrained(), "Unexpected unconstrained function return constraint - probably means the CSE visitors missed the function call");

    // Create a nucleus net for the return value (requires return constraint)
    VhdlSpecification *funcSpec = node.GetOwner()->Spec() ;
    VhdlName *returnType = funcSpec ? funcSpec->GetReturnType() : 0 ;
    returnNet = createNucleusNet(returnType, returnConstraint, node.Linefile());
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(returnNet, loc, "invalid return net");
    // Add the return net to the task
    NUTask* task = getTask();
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(task, loc, "Unable to get parent task");
    task->addArgFirst(returnNet, NUTF::eCallByAny);
    // Remember this net in case there is another return in this subprogram
    pScope->sReturnNet(returnNet);
  }
  
  //populate return expression
  //create blocking assign for return expressin
  //eg return <EXPRESSION>
  //
  //1. create temp net
  //2. create assign operator
  //3. create break operator
  //resutl:
  //      IdnetLvalue(temp_net) := NucleusExpr(<EXPRESSION>);
  //      NUBreak;

  //create blocking assignment object
  NULvalue* lval = mNucleusBuilder->createIdentLvalue(returnNet, loc);
  NUBlockingAssign* ba = new  NUBlockingAssign(lval, retExp, false, loc, true);
  
  //create Break object  
  NUBlock* block = getSubprogramTopBlock();
  INFO_ASSERT(block, "invalid owner");
  StringAtom* rt = mNucleusBuilder -> gCache() -> intern(UtString("RETURN"));
  INFO_ASSERT(rt, "invalid keyword");
  StringAtom* tt = mNucleusBuilder -> gCache() -> intern(UtString(""));
  //implement break statement
  NUBreak* br = new NUBreak(block, rt, tt, mNucleusBuilder->gNetRefFactory(), loc);
  INFO_ASSERT(br, "invalid break object");
  UtVector<NUBase*> stmt;
  stmt.push_back(ba);
  stmt.push_back(br);
  mCurrentScope->sValueVector(stmt); 
  mCurrentScope->sValue(NULL);
}

/**
 * @brief VhdlProcedureCallStatement.
 * In Nucleus system procedures are inlined. Parametes resolved in context
 * to wires then statements added to parent.
 *
 * procedure_call_statement ::= [lable : ] procedure_call
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlProcedureCallStatement, node)
{
    DEBUG_TRACE_WALKER("VhdlProcedureCallStatement", node);
    if (node.GetCall()) {
      // once we executed this here:
      //  mCurrentScope->sContext(V2NDesignScope::PROC_CALL);
      // but there were no uses elsewhere for PROC_CALL, so all it really did was change the context from an unknown context to PROC_CALL
      mCurrentScope->sContext(V2NDesignScope::UNKNOWN_CONTEXT); // do not use V2NDesignScope::RVALUE here because it causes test/vhdl/CarbonStaticElab/dataflow10.vhd to fail
        if (ID_VHDLINDEXEDNAME == node.GetCall()->GetClassId()) {
            node.GetCall()->Accept(*this);
        } else {
            VhdlName* vName = node.GetCall();
            procCall(*vName);
        }
    }
}

/**
 *@brief VhdlCaseGenerateStatement
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlCaseGenerateStatement, node)
{
  DEBUG_TRACE_WALKER("VhdlCaseGenerateStatement", node);
  CARBON_UNUSED(node)
}

/**
 *@brief VhdlVariableAssignmentStatement
 *
 * variable_assignment_statement ::= [lable : ] target := expression
 *
 * TODO: This looks identical to VhdlSignalAssignment.
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlVariableAssignmentStatement, node)
{
    DEBUG_TRACE_WALKER("VhdlVariableAssignmentStatement", node);
    //return if before we got in error 
    if (hasError()) {
        mCurrentScope->sValue(0);
        return ;
    }
    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    //check node corectness
    if (!node.GetTarget() || !node.GetValue()) {
        mNucleusBuilder->getMessageContext()->Verific2NUInvalidNode(&loc, "variable_assignment");
        mCurrentScope->sValue(NULL);
        setError(true);
        return ;
    }

    //evaluate target constraint
    VhdlConstraint* oldTargetConstraint = mTargetConstraint;
    mTargetConstraint = getTargetConstraint(node.GetTarget(), 0, mDataFlow);
    //evaluate value expression
    VhdlValue* value = node.GetValue()->Evaluate(mTargetConstraint, mDataFlow, 0);

    //populate left side
    mCurrentScope->sContext(V2NDesignScope::LVALUE);
    node.GetTarget()->Accept(*this);
    NUBase* lBase = mCurrentScope->gValue();
    if (! lBase) {
        mNucleusBuilder->getMessageContext()->Verific2NUPopulateError(&loc);
        mCurrentScope->sValue(NULL);
        setError(true);
        return ;
    }
    INFO_ASSERT(dynamic_cast<NULvalue*>(lBase), "should has NULvalue base");
    //left side should return NULvalue base object
    NULvalue* lval =  static_cast<NULvalue*>(lBase);
    INFO_ASSERT(lval, "invalid left side expression");

     //populate right side
     mCurrentScope->sContext(V2NDesignScope::RVALUE);
     node.GetValue()->Accept(*this);
     //restore target constraint
     delete mTargetConstraint;
     mTargetConstraint = oldTargetConstraint;
     NUBase* rBase = mCurrentScope->gValue();
     if (! rBase) {
         mNucleusBuilder->getMessageContext()->Verific2NUPopulateError(&loc);
         mCurrentScope->sValue(NULL);
         setError(true);
         return ;
     }
     INFO_ASSERT(dynamic_cast<NUExpr*>(rBase), "should has NUExpr base");
     NUExpr* rval = static_cast<NUExpr*>(rBase);
     INFO_ASSERT(rval, "invlaid rvalue object");

    //create blocking assign NU object
    NUBase* ba = new  NUBlockingAssign(lval, rval, false, loc, true);
    INFO_ASSERT(ba, "failed to create blocking assign");
    mCurrentScope -> sValue(ba);

    if (mDataFlow->IsDeadCode() || node.GetTarget()->IgnoreAssignment()) {
        delete value;
    } else {
        //save right side constant value in data flow
        //for populating constant expressions in upcoming statements
        node.GetTarget()->Assign(value, mDataFlow, 0);
    }
}

/**
 *@brief VhdlSignalAssignmentStatement
 *
 * signal_assignment_statement ::= [ label :] target <= [ delay_mechanism ] waveform
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlSignalAssignmentStatement, node)
{
    DEBUG_TRACE_WALKER("VhdlSignalAssignmentStatement", node);
    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    //check node correctness
    if (!node.GetTarget() || !node.GetWaveform() || (0 == node.GetWaveform()->Size())) {
        mCurrentScope->sValue(NULL);
        return;
    } else if (1 != node.GetWaveform()->Size()) {
        //currently supported only 1 waveform
        mNucleusBuilder->getMessageContext()->Verific2NUMultipleWaveform(&loc);
        mCurrentScope->sValue(NULL);
        setError(true);
        return ;
    }
    VhdlExpression* expr = (VhdlExpression*)node.GetWaveform()->GetFirst();
    V2N_INFO_ASSERT(expr, node, "Failed to find waveform form signalassignmentstatement");

    //populate target
    mCurrentScope->sContext(V2NDesignScope::LVALUE);
    node.GetTarget()->Accept(*this);
    NULvalue* lval = dynamic_cast<NULvalue*>(mCurrentScope->gValue());
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(lval, loc, "Unable to get lvalue");  
    //populate waveform

    //set target constraint
    VhdlConstraint* oldTargetConstraint = mTargetConstraint;
    // Calcuate the constraint, required for processing RHS aggregates.
    mTargetConstraint = getTargetConstraint(node.GetTarget(), expr, mDataFlow);

    mCurrentScope->sContext(V2NDesignScope::RVALUE);
    expr->Accept(*this);
    //restore target constraint
    delete mTargetConstraint;
    mTargetConstraint = oldTargetConstraint;

    NUExpr* rval = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(rval, loc, "Unable to get expression"); 

    NUBase* nba = new NUNonBlockingAssign(lval, rval, false, loc, true);
    mCurrentScope -> sValue(nba);
}

//create edge expression from rising_edge function call
//eg rising_edge(clk) -> NUEdgeExpr(clk)
void VerificVhdlDesignWalker::risingEdgeExpr(VhdlIndexedName* idName)
{
  INFO_ASSERT(idName, "Invalid input");
  risingOrFallingEdgeExpr(idName, eClockPosedge);
}

//create edge expression from falling_edge function call
//eg falling_edge(clk) -> NUEdgeExpr(clk)
void VerificVhdlDesignWalker::fallingEdgeExpr(VhdlIndexedName* idName)
{
  INFO_ASSERT(idName, "Invalid input");
  risingOrFallingEdgeExpr(idName, eClockNegedge);
}

void VerificVhdlDesignWalker::risingOrFallingEdgeExpr(VhdlIndexedName*& idName, ClockEdge edge_type)
{
    INFO_ASSERT(1 == idName->GetAssocList()->Size(), "for rising_edge number of parameters should be 1");
    unsigned i = 0;
    VhdlTreeNode* elem = 0;
    NUExpr* rval = 0; //argument expression for rising_edge , eg, clk-expression in rising_edge(clk) case
    FOREACH_ARRAY_ITEM(idName->GetAssocList(), i, elem) {
        mCurrentScope->sContext(V2NDesignScope::RVALUE);
        //populate argument
        elem->Accept(*this);
        //get populated argument
        NUBase* base = mCurrentScope->gValue();
        INFO_ASSERT(base, "invalid NUBase object");
        //convert to NUExpr from NUBase
        rval = dynamic_cast<NUExpr*>(base); 
    }
    INFO_ASSERT(rval, "invalid NUExpr object");
    // FD thinks: Isn't it possible to have composite net here ?
    if (NUIdentRvalue* iRval = dynamic_cast<NUIdentRvalue*>(rval)) {
      //create edge expressin if it is not exitsts
      //and add to nearest always block
      if(! isEdgeExprExists(iRval->getIdent(), eClockPosedge)) {
        //RJC RC#2013/08/13 (tigran) are we sure that this is not a reset signal at this point?, the following always sets the clock flag
        //ADDRESSED I couldn't yet figure out why here is setIsClock() and not setIsReset(), needs more investigation
        (void)addEdgeExprToAlways(iRval, true, edge_type);
      }
    } else {
        INFO_ASSERT(false, "not supported");
    }
}


class VerificExpressionWalker: public Verific::VhdlVisitor
{
public:
    VerificExpressionWalker() :mEdgeExprName("")
        {}
    ~VerificExpressionWalker()
        {}

public:
    void traverseExpr(VhdlExpression* expr)
        {
            expr->Accept(*this);
        }
    
    UtString gEdgeExprName() const
        {
            return mEdgeExprName;
        }
    
private:
   void VHDL_VISIT(VhdlOperator, node)
        {
            DEBUG_TRACE_WALKER("VhdlOperator", node);
            if (node.GetLeftExpression()) {
                node.GetLeftExpression()->Accept(*this);
            }
            if (node.GetRightExpression()) {
                node.GetRightExpression()->Accept(*this);
            }
        }
    void VHDL_VISIT(VhdlAttributeName, node) {
        DEBUG_TRACE_WALKER("VhdlAttributeName", node);
        const char* event = static_cast<VhdlIdRef*>(node.GetDesignator())->Name();
        const UtString cThisAttributeName = UtString (event);
        const UtString eventStr("event");
        if (eventStr ==  cThisAttributeName) {
            VhdlName* vName = node.GetPrefix();
            INFO_ASSERT(vName, "invalid VhdlName node");
            mEdgeExprName = UtString(vName->Name());
            return ;
        }
    }
private:
    UtString mEdgeExprName;
};

//if('edgeexpr') {} if ('edgeexpr') {} not supported
bool VerificVhdlDesignWalker::validateTwoIfEdgeExpr(UtVector<VhdlIfStatement*>& ifStmts) const
{
    UInt32 count = 0;
    UInt32 s = ifStmts.size();
    UtSet<UtString> edgeExprNameSet;
    for (UInt32 i = 0; i < s; ++i) {
        if (!ifStmts[i]->GetElsifList() && ifStmts[i]->GetIfCondition()->FindIfEdge()) {
            VerificExpressionWalker exprWalker;
            exprWalker.traverseExpr(ifStmts[i]->GetIfCondition());
            UtString edgeExprName = exprWalker.gEdgeExprName();
            edgeExprNameSet.insert(edgeExprName);
            ++count;
        }
    }
    if ((1 < count) && (1 != edgeExprNameSet.size())) {
        SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,ifStmts[0]->Linefile());
        mNucleusBuilder->getMessageContext()->Verific2NU2ClocksNotAllowed(&loc);
        return false;
    }
    return true;
}

//clock expression should be single in if-elsif-else statement

bool VerificVhdlDesignWalker::validateSingleClockExpr(UtVector<VhdlIfStatement*>& ifStmts) const
{
    UInt32 s = ifStmts.size();
    for (UInt32 i = 0; i < s; ++i) {
        UtVector<VhdlExpression*> conditions;
        conditions.push_back(ifStmts[i]->GetIfCondition());
        //check other condition for edge expression
        unsigned j = 0;
        VhdlTreeNode* elem = 0;
        FOREACH_ARRAY_ITEM(ifStmts[i]->GetElsifList(), j, elem) {
            VhdlElsif* elsIf = static_cast<VhdlElsif*>(elem);
            INFO_ASSERT(elsIf, "invalid verific elsif node");
            INFO_ASSERT(ID_VHDLELSIF == elem->GetClassId(), "");
            INFO_ASSERT(elsIf->Condition(), "condition for elsif not exists");
            VhdlExpression* elsif_cond = elsIf->Condition();
            conditions.push_back(elsif_cond);
        }
        UInt32 count = 0;
        for (UInt32 l = 0; l < conditions.size(); ++l) {
            if (conditions[l]->FindIfEdge()) {
                ++count;
            }
            if (1 < count) {
                SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,conditions[l]->Linefile());
                mNucleusBuilder->getMessageContext()->Verific2NU2ClocksNotAllowed(&loc);
                return false;
            }
        }
    }
    return true;
}

//if 'if' statement condition is edge expression then it should located in top of process

 bool VerificVhdlDesignWalker::validateClock(UtVector<VhdlIfStatement*>& ifStmts) const
 {
     //should belong the same process ./misc2.vhdl
     UInt32 sz = ifStmts.size();
     UtMap<VhdlExpression*, NUExpr*>::const_iterator s = mEdgeValidity.begin();
     UtMap<VhdlExpression*, NUExpr*>::const_iterator e = mEdgeValidity.end();
     for (; s != e; ++s) {
         if (! s->first->FindIfEdge()) {
             continue;
         }
         bool isExists = false;
         for (UInt32 i = 0; i < sz; ++i) {
             if (ifStmts[i]->GetIfCondition() == s->first) {
                 isExists = true;
                 break;
             }
         }
         if (! isExists) {
             for (UInt32 i = 0; i < sz; ++i) {
                 unsigned j = 0;
                 VhdlTreeNode* elem = 0;
                 FOREACH_ARRAY_ITEM(ifStmts[i]->GetElsifList(), j, elem) {
                     if ( !elem ) continue;
                     VhdlElsif* elsIf = static_cast<VhdlElsif*>(elem);
                     INFO_ASSERT(elsIf, "invalid verific elsif node");
                     INFO_ASSERT(ID_VHDLELSIF == elem->GetClassId(), "");
                     INFO_ASSERT(elsIf->Condition(), "condition for elsif not exists");
                     if (elsIf->Condition() == s->first) {
                         isExists = true;
                         break;
                     }
                 }
                 if ( isExists ) break;
             }
         }

         if (! isExists) {
             for (UInt32 i = 0; i < sz; ++i) {
                 if (isExists ) break;
                 unsigned kk = 0;
                 VhdlStatement* stmt;
                 FOREACH_ARRAY_ITEM(ifStmts[i]->GetElseStatments(), kk, stmt) {
                     if ( !stmt ) continue;
                     VhdlIfStatement* sub_if_stmt = dynamic_cast<VhdlIfStatement*>(stmt);
                     if ( sub_if_stmt ) {
                       if (sub_if_stmt->GetIfCondition() == s->first) {
                         isExists = true;
                         break;
                       }
                     }
                 }
                 if ( isExists ) break;
             }
         }
           
         if (! isExists) {
             SourceLocator loc = s->second->getLoc();
             mNucleusBuilder->getMessageContext()->ClockingError(&loc, "Could not locate the clock in this process.");
             return false;
         }
     }
     return true;
 }

/**
 * @brief Analyze and add edge expression
 */
void VerificVhdlDesignWalker::processEdgeExpression(UtVector<VhdlExpression*>& clk_expr, UtVector<VhdlExpression*>& edge_expr)
{
    if (clk_expr.empty()) {
        return ;
    }
    //process conditions which not detected from verific as edge/stable expression,
    //but belong the same if-elsif-else statement
    UInt32 s = edge_expr.size();
    for (UInt32 j = 0; j < s; ++j) {
        VhdlExpression* expr = edge_expr[j];
        INFO_ASSERT(expr, "invalid VhdlExpression object");
        (void)createEdgeExpression(expr, expr, false);
    }
    s = clk_expr.size();
    for (UInt32 j = 0; j < s; ++j) {
        VhdlExpression* clk_condition = clk_expr[j];
        INFO_ASSERT(clk_condition, "invalid VhdlExpression node");
        //get clock event or stable expression from conditions found by verific
        if (ID_VHDLOPERATOR == clk_condition->GetClassId()) {
            VhdlOperator* op = static_cast<VhdlOperator*>(clk_condition);
            INFO_ASSERT(op, "invalid VhdlOperator node");
            VhdlExpression* left = op->GetLeftExpression();
            VhdlExpression* right = op->GetRightExpression();
            //e.g. (clock'stable and clock = '1' and sig = '1')
             if (right->FindIfEdge() ||
                right->FindIfStable()) {
                if(!right->GetRightExpression() ||
                   (ID_VHDLATTRIBUTENAME == right->GetClassId())) {
                    if (ID_VHDLINDEXEDNAME == right->GetClassId()) {
                        VhdlIndexedName* idName = static_cast<VhdlIndexedName*>(right);
                        INFO_ASSERT(idName, "");
                        if (UtString("rising_edge") == UtString(idName->GetPrefix()->Name())) {
                            risingEdgeExpr(idName);
                        } else if (UtString("falling_edge") == UtString(idName->GetPrefix()->Name())) {
                            fallingEdgeExpr(idName);
                        }
                    } else {
                        //e.g. not temp'stable
                        (void)createEdgeExpression(clk_condition, left);
                    }
                } else {
                    addEdgeExpressionToProcess(clk_condition, right);
                }
                //e.g. (sig = '1' and (clcok'stable and clock = '1'))
            } else if (left->FindIfEdge() ||
                       left->FindIfStable()) {
                //e.g. not temp'stable
                if (!left->GetRightExpression() ||
                    (ID_VHDLATTRIBUTENAME == left->GetClassId())) {
                    if (ID_VHDLINDEXEDNAME == left->GetClassId()) {
                        VhdlIndexedName* idName = static_cast<VhdlIndexedName*>(left);
                        INFO_ASSERT(idName, "");
                        if (UtString("rising_edge") == UtString(idName->GetPrefix()->Name())) {
                            risingEdgeExpr(idName);
                        } else if (UtString("falling_edge") == UtString(idName->GetPrefix()->Name())) {
                            fallingEdgeExpr(idName);
                        }
                    } else {
                        //e.g. temp = '0'
                        (void)createEdgeExpression(clk_condition, right);
                    }
                } else {
                    addEdgeExpressionToProcess(clk_condition, left);
                }
                //e.g. (clock'stable and clock = '1')
            } else {
                INFO_ASSERT(op->FindIfEdge() || op->FindIfStable(), "");
                addEdgeExpressionToProcess(clk_condition, op);
            }
        } else {
            if (ID_VHDLINDEXEDNAME == clk_condition->GetClassId()) {
                VhdlIndexedName* idName = static_cast<VhdlIndexedName*>(clk_condition);
                INFO_ASSERT(idName, "");
                if (UtString("rising_edge") == UtString(idName->GetPrefix()->Name())) {
                    risingEdgeExpr(idName);
                } else if (UtString("falling_edge") == UtString(idName->GetPrefix()->Name())) {
                    fallingEdgeExpr(idName);
                } else {
                    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,clk_condition->Linefile());
                    UtString reason("Multiple wait statements in a process are not supported.Multiple wait statements in a process are not supported.");
                     mNucleusBuilder->getMessageContext()->ClockingError(&loc, reason.c_str());
                    mCurrentScope->sValue(NULL);
                    setError(true);
                    return ;
                }
            } else {
                SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,clk_condition->Linefile());
                mNucleusBuilder->getMessageContext()->Verific2NUUnsupportedSeqProcess(&loc, "");
                mCurrentScope->sValue(NULL);
                setError(true);
                return ;
            }
        }
    }
}

// This class is used to replace the clocked net in a clock expression of type:
// vec[0]'event and vec[0] = 1
// with
// temp_vec_0'event and temp_vec_0 = 1
// where
// assign temp_vec_0 = vec[0]
// 
// If clock expression net is not a scalar bit net, but a vector bit
// like vec[0]'event, then it's replaced with a temporary net during always
// block edge expression creation, in VhdlPopulate::edgeExpr(). Replace it
// in the given expression ..typically coming from an if statement condition,
// if the expression is a clocked expression.
class ClockNetReplacer : public NuToNuFn
{
public:
    
    ClockNetReplacer(NUNet* clock_net, NUExpr* clock_expr)
        : mClockNet(clock_net),
          mClockExpr(clock_expr),
          mErrorDetected(false)
        {
        }
    
    ~ClockNetReplacer()
        {
            for (NUCExprVector::iterator itr = mMarkedForDeletion.begin();
                 itr != mMarkedForDeletion.end(); ++itr)
            {
                delete *itr;
            }
        }
    
    virtual NUExpr* operator() (NUExpr* expr, Phase phase)
        {
            // Look for binary expression like : clk'event and clk = '1'
            const SourceLocator loc = expr->getLoc();
            if (phase == ePre && expr->getType() == NUExpr::eNUAttribute)
            {
                // During ePre phase, look for clk'event and clk'stable expressions.
                NUAttribute *attrib = dynamic_cast<NUAttribute*>(expr);
                NUExpr* clk_expr = attrib->getArg(0);
                if (*clk_expr == *mClockExpr) // Is clock used in this attrib expr?
                {
                    NUAttribute::AttribT attrType = attrib->getAttrib();
                    if ((attrType == NUAttribute::eEvent) ||
                        (attrType == NUAttribute::eStable))
                    {
                        replaceClkArg(expr, 0 /*NUAttribute has only 1 arg*/, loc);
                    }
                    else
                    {
                        // Unsupported attribute
                        // const char *attribStr = attrib->getAttribChar();
                        mErrorDetected = true;
                    }
                }
            }
            else if (phase == ePost && expr->getType() == NUExpr::eNUBinaryOp)
            {
                // During the ePost phase, look for clock level expressions.
                // They are binary expressions with clock level specified like clk = '1'
                NUOp *op = dynamic_cast<NUOp*>(expr);
                if (op->getOp() == NUOp::eBiEq)
                {
                    UInt32 arg_index = 0;
                    if (op->getArg(arg_index)->isConstant()) {
                        ++arg_index;
                    }
                    NUExpr* clk_expr = op->getArg(arg_index);
                    if (*clk_expr == *mClockExpr)  // Is clock used in this level expr?
                    {
                        replaceClkArg(expr, arg_index, loc);
                    }
                }
            }
            return NULL;
        }
    
    bool getErrorDetected() const { return mErrorDetected; }
    
private:
    ClockNetReplacer();
    ClockNetReplacer( const ClockNetReplacer& );
    ClockNetReplacer &operator=( const ClockNetReplacer& );

  void replaceClkArg(NUExpr* expr, UInt32 arg_index, const SourceLocator& loc)
  {
      NUNet* clock_net = mClockNet;
      NU_ASSERT(clock_net != NULL, mClockExpr);
      NUExpr* new_arg = new NUIdentRvalue(clock_net, loc);
      NUExpr* old_expr = expr->putArg(arg_index, new_arg);
      mMarkedForDeletion.push_back(old_expr);
  }

    NUNet* mClockNet;
    NUExpr* mClockExpr;
    bool mErrorDetected;
    NUCExprVector mMarkedForDeletion;
};

// Steve Lim 2013-12-15 FIXME I still don't fully understand why we are driving a 
// clock net
NUIdentRvalue* VerificVhdlDesignWalker::replaceIfCondition(NUExpr* clock_expr, VhdlExpression* exp)
{

    //create temporary net and assign to edge expression
    //after replace edge expression with newly create net

    //create temp net
    NUModule* module = getModule();
    INFO_ASSERT(module, "Unable to get module scope");
    StringAtom* name = module->gensym("temp_clock_net");
    NUNet* clockNet = new NUBitNet(name, NetFlags(eDMWireNet | eAllocatedNet | eTempNet), module, clock_expr->getLoc());
    INFO_ASSERT(clockNet, "invalid NUBitNet");
    NUIdentLvalue* temp_lvalue = new NUIdentLvalue(clockNet, clock_expr->getLoc());
    INFO_ASSERT(temp_lvalue, "invalid Lvalue object");
    NUContAssign* assign = new NUContAssign(clockNet->getName(), temp_lvalue, clock_expr, clock_expr->getLoc());
    INFO_ASSERT(assign, "invalid NUContAssign object");
    module->addContAssign(assign);
    module->addLocal(clockNet);
    //replace edge expression 
    INFO_ASSERT(mIfCondition.end() != mIfCondition.find(exp), "NU object must exist for if condition");
    NUExpr* cond_expr = mIfCondition.find(exp)->second;
    INFO_ASSERT(cond_expr, "invalid NUExpr for if condition");
    ClockNetReplacer cnr(clockNet, clock_expr);
    cond_expr->replaceLeaves(cnr);
    clockNet->putIsEdgeTrigger(true);
    NUIdentRvalue* identRvalue = new NUIdentRvalue(clockNet, clockNet->getLoc());
    INFO_ASSERT(identRvalue, "invalid Rvalue object");
    //misc9.vhdl
    //replace complex reset
    if (!(clock_expr->getType() == NUExpr::eNUAttribute ||
          clock_expr->getType() == NUExpr::eNUBinaryOp)) {
        UtString con;
        con += (char(0 + '0'));
        NUIf* the_if = static_cast<NUIf*>(mIfStmt.find(cond_expr)->second);
        INFO_ASSERT(the_if, "invalid NUIf stmt");
        NUIdentRvalue* identRvalue = new NUIdentRvalue(clockNet, clockNet->getLoc());
        INFO_ASSERT(identRvalue, "invalid Rvalue object");
        the_if->replaceCond(identRvalue);
        delete cond_expr;
    }
    return identRvalue ;
}

//if we need to replace if conditon then will returned created NUEdgeExpression object
//which need for NuToNuFn , otherwise fill return 0
NUEdgeExpr* VerificVhdlDesignWalker::createEdgeExpression(VhdlExpression* cond, VhdlExpression* expr, bool isClock)
{
    ClockEdge edge_type;
    NUExpr* edge_expr = 0;
    NUIdentRvalue* rval_expr = 0;
    if (ID_VHDLOPERATOR == expr->GetClassId() &&
        expr->GetLeftExpression() &&
        expr->GetRightExpression()) {
        //clock = '1'
        INFO_ASSERT(expr->GetLeftExpression(), "invalid left expression");
        INFO_ASSERT(expr->GetRightExpression(), "invalid right expression");
        mCurrentScope->sContext(V2NDesignScope::RVALUE);
        //get edge expression net
        expr->GetLeftExpression()->Accept(*this);
        if (hasError()) {
            return 0;
        }
        NUBase* left = mCurrentScope->gValue();
        INFO_ASSERT(left, "invalid NUBase object");
        edge_expr = dynamic_cast<NUExpr*>(left);
        INFO_ASSERT(edge_expr, "invalid NUExpr object");
        rval_expr = dynamic_cast<NUIdentRvalue*>(edge_expr);
        //get edge expression type
        expr->GetRightExpression()->Accept(*this);
        NUBase* right = mCurrentScope->gValue();
        INFO_ASSERT(right, "invalid NUBase object");
        NUConst* edge_value = dynamic_cast<NUConst*>(right);
        INFO_ASSERT(edge_value, "invalid NUConst object");
        if (edge_value->isZero()) {
            edge_type = eClockNegedge;
        } else {
            edge_type = eClockPosedge;
        }
        //delete nucleus constant object
        delete edge_value;
    } else {
        if (ID_VHDLATTRIBUTENAME == expr->GetClassId()) {
            return 0;
        }
        edge_type = eClockPosedge;
        mCurrentScope->sContext(V2NDesignScope::RVALUE);
        expr->Accept(*this);
        NUBase* base = mCurrentScope->gValue();
        INFO_ASSERT(base, "Invalid NUBase object");
        edge_expr = dynamic_cast<NUExpr*>(base);
        INFO_ASSERT(edge_expr, "Invalid NUExpr object");
        rval_expr = dynamic_cast<NUIdentRvalue*>(edge_expr);
    }
    //create edge expression if it does not exist
    //and add to nearest always block
    if (rval_expr) {
      if(! isEdgeExprExists(rval_expr->getIdent(), edge_type)) {
        INFO_ASSERT(rval_expr->getBitSize() == 1, "");
        return addEdgeExprToAlways(rval_expr, isClock, edge_type);
      } else {
        //edge expression already exists
        delete rval_expr;
      }
    } else {
      NUIdentRvalue* identRval = replaceIfCondition(edge_expr, cond);
      INFO_ASSERT(identRval, "Invalid rvalue object");
      return addEdgeExprToAlways(identRval, isClock, edge_type);
    }
    return 0;
}

//RJC RC#2013/08/30 (edvard)
// this method was defined to return a NUEdgeExpr, but the return value was never used, I have changed it to be a void, but I don't see how error
//conditions are to be supported here, should this method return a flag indicating success?  or should it be setting hasError()?

//RJC RC#2013/08/30 (edvard)
// The use of INFO_ASSERT here to tell the user that a construct is not supported is bad programming practice.
// The assert is not an error message, it does not tell the user where in the HDL source there might be a problem so they have no recourse
// except to call customer support or spend time guessing about what the problem is.
// Using INFO_ASSERT to handle conditions that could be created by the user (such as the use of unsupported constructs) is not acceptable.  
// These kinds of asserts should only be used to protect the code from going off the deep end due to a programming error.
// please replace the unsupported construct ones with a real error message.

void VerificVhdlDesignWalker::addEdgeExpressionToProcess(VhdlExpression* cond, VhdlExpression* expr)
{
    INFO_ASSERT(expr, "invalid VhdlExpression node");

    NUEdgeExpr* edge_expr = 0;
    if (expr->FindIfStable()) {
        INFO_ASSERT(ID_VHDLOPERATOR == expr->GetClassId(), "should be VhdlOperator node");
        //clock'stable and clock = '1'
        if (ID_VHDLATTRIBUTENAME == expr->GetLeftExpression()->GetClassId()) {
            edge_expr = createEdgeExpression(cond, expr->GetRightExpression());
        } else if (ID_VHDLATTRIBUTENAME == expr->GetRightExpression()->GetClassId()) {
            edge_expr = createEdgeExpression(cond, expr->GetRightExpression());
            //(not clock'stable) and (clock = '1')
        } else if (ID_VHDLOPERATOR == expr->GetLeftExpression()->GetClassId() &&
                   !expr->GetLeftExpression()->GetRightExpression()) {
            edge_expr = createEdgeExpression(cond, expr->GetRightExpression());
        } else if (ID_VHDLOPERATOR == expr->GetRightExpression()->GetClassId() &&
                   !expr->GetRightExpression()->GetRightExpression()) {
            edge_expr = createEdgeExpression(cond, expr->GetLeftExpression());
        } else {
            INFO_ASSERT(false, "not supported");
        }
    } else if (expr->FindIfEdge()) {
        INFO_ASSERT(ID_VHDLOPERATOR == expr->GetClassId(), "should be VhdlOperator node");
        //clock'event and clock = '1'
        if (ID_VHDLATTRIBUTENAME == expr->GetLeftExpression()->GetClassId()) {
            edge_expr = createEdgeExpression(cond, expr->GetRightExpression());
        } else if (ID_VHDLATTRIBUTENAME == expr->GetRightExpression()->GetClassId()) {
            edge_expr = createEdgeExpression(cond, expr->GetRightExpression());
            //(not clock'event) and (clock = '1')
        } else if (ID_VHDLOPERATOR == expr->GetLeftExpression()->GetClassId() &&
                   !expr->GetLeftExpression()->GetRightExpression()) {
            edge_expr = createEdgeExpression(cond, expr->GetRightExpression());
        } else if (ID_VHDLOPERATOR == expr->GetRightExpression()->GetClassId() &&
                   !expr->GetLeftExpression()->GetRightExpression()) {
            edge_expr = createEdgeExpression(cond, expr->GetRightExpression());
        } else {
            INFO_ASSERT(false, "not supported");
        }
    } else {
        INFO_ASSERT(false, "not supported");
    }
    if (edge_expr)
      setEdgeExpr(edge_expr);
}

// locate clock and edge expressions within \a ifstmt, save pointers to them them in the UtVectors: \a clk_expr and \a edge_expr respectively
void VerificVhdlDesignWalker::gatherClockAndEdgeExpressions(VhdlIfStatement* ifstmt, UtVector<VhdlExpression*>* clk_expr, UtVector<VhdlExpression*>* edge_expr)
{
    fileClockOrEdgeExpression(ifstmt->GetIfCondition(), clk_expr, edge_expr);

    unsigned i = 0;
    VhdlElsif* elsif;
    FOREACH_ARRAY_ITEM(ifstmt->GetElsifList(), i, elsif) {
      if (! elsif ) continue;
      fileClockOrEdgeExpression(elsif->Condition(), clk_expr, edge_expr);
    }
    // if there is a trailing else, and it has a single statement then consider it also
    Array* elseStmts = ifstmt->GetElseStatments();
    if ( elseStmts ) {
      UInt32 count = Verific2NucleusUtilities::countNonNullArrayItems(elseStmts);
      if ( 1 == count ){
        // only consider clocks/reset if there is a single statement in the trailing else branch
        VhdlStatement* stmt;
        i=0;
        FOREACH_ARRAY_ITEM(elseStmts, i, stmt) {
          if (!stmt) continue;
          VhdlIfStatement* sub_if_stmt = dynamic_cast<VhdlIfStatement*>(stmt);
          if ( sub_if_stmt ){
            fileClockOrEdgeExpression(sub_if_stmt->GetIfCondition(), clk_expr, edge_expr);
          }
        }
      }
    }
}

// helper for gatherClockAndEdgeExpressions, puts \a cond into either \a clk_expr or \a edge_expr
void VerificVhdlDesignWalker::fileClockOrEdgeExpression(VhdlExpression* cond, UtVector<VhdlExpression*>* clk_expr, UtVector<VhdlExpression*>* edge_expr){
  if ( cond ) {
    if (cond->FindIfEdge() || cond->FindIfStable()) {
      clk_expr->push_back(cond);
    } else {
      edge_expr->push_back(cond);
    }
  }
}


// Steve 2013-12-12 A class for visiting wait statements in a process.
// Currently used to count the number of wait statements in a process.
// Put statement-level wait-related functionality here.
class VerificVhdlWaitWalker : public Verific::VhdlVisitor
{
public:
  VerificVhdlWaitWalker() : mNumWaits(0) {}
  ~VerificVhdlWaitWalker() {}

public:
  void visitAllWaitStmts(VhdlProcessStatement* process)
  {
    unsigned i;
    VhdlStatement* stmt;
    FOREACH_ARRAY_ITEM(process->GetStatementPart(), i, stmt)
    {
      if ( ! stmt ) continue;
      stmt->Accept(*this);
    }
  }

  UInt32 getNumWaits() const { return mNumWaits; }
private:
  void VHDL_VISIT(VhdlProcessStatement, node)
  {
    //DEBUG_TRACE_WALKER("VhdlProcessStatement", node);
    VhdlStatement* stmt;
    unsigned i;
    FOREACH_ARRAY_ITEM(node.GetStatementPart(), i, stmt)
    {
      if ( ! stmt ) continue;
      stmt->Accept(*this);
    }
  }
  void VHDL_VISIT(VhdlWaitStatement, node)
  {
    //DEBUG_TRACE_WALKER("VhdlWaitStatement", node);
    (void) node;
    mNumWaits++;
  }
  void VHDL_VISIT(VhdlLoopStatement, node)
  {
    //DEBUG_TRACE_WALKER("VhdlLoopStatement", node);
    VhdlStatement* stmt;
    unsigned i;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, stmt)
    {
      if ( ! stmt ) continue;
      stmt->Accept(*this);
    }
  }
  void VHDL_VISIT(VhdlIfStatement, node)
  {
    //DEBUG_TRACE_WALKER("VhdlIfStatement", node);
    VhdlStatement* stmt;
    unsigned i;
    FOREACH_ARRAY_ITEM(node.GetIfStatements(), i, stmt)
    {
      if ( ! stmt ) continue;
      stmt->Accept(*this);
    }
    FOREACH_ARRAY_ITEM(node.GetElsifList(), i, stmt)
    {
      if ( ! stmt ) continue;
      stmt->Accept(*this);
    }
    FOREACH_ARRAY_ITEM(node.GetElseStatments(), i, stmt)
    {
      if ( ! stmt ) continue;
      stmt->Accept(*this);
    }
  }
  void VHDL_VISIT(VhdlElsif, node)
  {
    //DEBUG_TRACE_WALKER("VhdlElsif", node);
    VhdlStatement* stmt;
    unsigned i;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, stmt)
    {
      if ( ! stmt ) continue;
      stmt->Accept(*this);
    }
  } 
  void VHDL_VISIT(VhdlCaseStatement, node)
  {
    //DEBUG_TRACE_WALKER("VhdlCaseStatement", node);
    VhdlStatement* stmt;
    unsigned i;
    FOREACH_ARRAY_ITEM(node.GetAlternatives(), i, stmt)
    {
      if ( ! stmt ) continue;
      stmt->Accept(*this);
    }
  } 
  void VHDL_VISIT(VhdlCaseStatementAlternative, node)
  {
    //DEBUG_TRACE_WALKER("VhdlCaseStatementAlternative", node);
    VhdlStatement* stmt;
    unsigned i;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, stmt)
    {
      if ( ! stmt ) continue;
      stmt->Accept(*this);
    }
  } 
  void VHDL_VISIT(VhdlBlockStatement, node)
  {
    //DEBUG_TRACE_WALKER("VhdlBlockStatement", node);
    VhdlStatement* stmt;
    unsigned i;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, stmt)
    {
      if ( ! stmt ) continue;
      stmt->Accept(*this);
    }
  }
  void VHDL_VISIT(VhdlSignalAssignmentStatement, node) 
  { 
      //DEBUG_TRACE_WALKER("VhdlSignalAssignmentStatement", node); 
      (void) node; 
  }
  void VHDL_VISIT(VhdlVariableAssignmentStatement, node)
  { 
      //DEBUG_TRACE_WALKER("VhdlVariableAssignmentStatement", node); 
      (void) node; 
  } 
  void VHDL_VISIT(VhdlAssertionStatement, node) 
  { 
      //DEBUG_TRACE_WALKER("VhdlAssertionStatement", node); 
      (void) node;
  }
  void VHDL_VISIT(VhdlReturnStatement, node) 
  { 
      //DEBUG_TRACE_WALKER("VhdlReturnStatement", node); 
      (void) node; 
  } 
  void VHDL_VISIT(VhdlNullStatement, node)
  { 
      //DEBUG_TRACE_WALKER("VhdlNullStatement", node); 
      (void) node;
  } 
private:
  unsigned int mNumWaits;
};


/**
 *@brief VhdlProcessStatement
 *
 * process_statement :=
 *             [ process label :]
 *                 [ POSTPONED ] PROCESS [ (sensitivity_list) ] [ IS ]
 *                       process declarative part
 *                 BEGIN
 *                       process statement part
 *                 END [ POSTPONED ] PROCESS [ process label ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlProcessStatement, node)
{
    DEBUG_TRACE_WALKER("VhdlProcessStatement", node); 
    SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    mInProcessStatement = true;
    mSensitivityList = node.GetSensitivityList();

    // Steve Lim 2013-12-12 Restructured visitAllWaitStmts
    VerificVhdlWaitWalker waitWalker;
    waitWalker.visitAllWaitStmts(&node);
    if (waitWalker.getNumWaits() > 1) {
        //We don't support multiple wait statements in a process
        UtString reason("Multiple wait statements in a process are not supported.");
        mNucleusBuilder->getMessageContext()->ClockingError(&source_locator, reason.c_str());
        setError(true);
        mCurrentScope->sValue(0);
        return;
    }

    //get parent scope
    //parent of process statement can be either NamedDeclarationScope or Module
    NUScope* parent_declaration_scope = getDeclarationScope();
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(parent_declaration_scope, source_locator, "Unable to get parent declaration scope");
    //create NamedDeclarationScope for decl part of current process
    // FD I think: this needs cleanup, hasError() checking appears too often in random places need some defined structure
    if (hasError()) {
        mCurrentScope->sValue(0);
        return ;
    }
    // FD TODO: check for label first
    UtString str;
    str << "named_decl_lb_L" << source_locator.getLine();
    NUNamedDeclarationScope* decl_scope = mNucleusBuilder->cNamedDeclarationScope(str, parent_declaration_scope, source_locator);
    NUModule* module = getModule();
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, source_locator, "Unable to get module scope");
    //create block for process statement
    //this block will be placed within a newly created always block
    NUBlock* block_scope = mNucleusBuilder->cBlock(module, source_locator);
    //create always block corresponding to process statement
    NUAlwaysBlock* always_block = mNucleusBuilder->createAlwaysBlockInScope(module, block_scope, false, NUAlwaysBlock::eAlways, source_locator);
    //we are creating V2NDesignScope for each NUScope
    //for keeping necessary extra information as well hierarchy of scopes
    //save at top level of stack
    pushDesignScope(block_scope, decl_scope);
    // as this is always top block scope, annotating always block inside it (later used to add event expressions)
    mCurrentScope->setAlwaysBlock(always_block);
    //currently we are processing inside this designscope
    unsigned i=0;
    VhdlTreeNode* elem;
    FOREACH_ARRAY_ITEM(node.GetDeclPart(), i, elem) {
        //skip subprograms decls,
        //subprograms are handled during the call
        //implementation are located in the VerificVhdlSubprograms.cpp
        if (ID_VHDLSUBPROGRAMBODY == elem->GetClassId()) {
            continue;
        }
        //propagate declaration
        elem->Accept(*this);
    }

    i = 0;
    elem = 0;
    //Process the statements in the always block and add to the block structure.
    UtVector<VhdlExpression*> clk_expr;
    UtVector<VhdlExpression*> edge_expr;
    bool waitInsideProcess = false;
    NUStmtList* stmtsFromBeforeClockWaitStmt = NULL;
    UtVector<VhdlIfStatement*> ifStmts;
    mEdgeValidity.clear();
    // Steve Lim 2013-12-12 Removed unnecessary dynamic casts
    VhdlStatement* stmt;
    FOREACH_ARRAY_ITEM(node.GetStatementPart(), i, stmt) {
        if (!stmt) continue;
        stmt->Accept(*this);
        if (ID_VHDLWAITSTATEMENT == stmt->GetClassId()) {
          // found a clock wait stmt, now take all the statements curently in the block and save them so they can be moved to after the last statement,
          // see test/rushc/vhdl/beacon_misc/misc.LOGIC33.vhdl for an example.
            INFO_ASSERT(!waitInsideProcess, "Two wait statements found in same process, this is unsupported and should have been reported as Error 3546: Unsupported VHDL clock construct: Multiple wait statements in a process are not supported.");
            waitInsideProcess = true;
            stmtsFromBeforeClockWaitStmt = block_scope->getStmts();
            NUStmtList emptyList;
            block_scope->replaceStmtList(emptyList);
        }
        if (hasError()) {
            mCurrentScope->sValue(0);
            return ;
        }
        NUBase* stmt_value = mCurrentScope -> gValue();
        if (stmt_value) {
            //FD thinks: this seems dirty. Why 'if' statement is a special case?
            if (ID_VHDLIFSTATEMENT == stmt->GetClassId()) {
                VhdlIfStatement* ifstmt = static_cast<VhdlIfStatement*>(stmt);
                gatherClockAndEdgeExpressions(ifstmt, &clk_expr, &edge_expr);
                ifStmts.push_back(ifstmt);
            }
            NUStmt* nuStmt = dynamic_cast<NUStmt*>(stmt_value);
            NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(nuStmt, source_locator, "Unable to get statement");
            block_scope->addStmt(nuStmt);
        }
    }

    //check clock location correctness e.g. misc2
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(validateClock(ifStmts), source_locator, "Unable to validate clock");

    //two_edge_same_if
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(validateSingleClockExpr(ifStmts), source_locator, "Unable to validate single clock expression");

    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(validateTwoIfEdgeExpr(ifStmts), source_locator, "Unable to validate two if edge expression");
    
    if ( waitInsideProcess ){
      // this process block uses a style where there were statements before the clock edge detection, this is supported by moving those
      // statements so they appear after the clock edge detection test/rushc/vhdl/beacon_misc/misc.LOGIC33.vhdl
      block_scope->addStmts(stmtsFromBeforeClockWaitStmt);
      delete stmtsFromBeforeClockWaitStmt;
    }

    //create edge expression
    processEdgeExpression(clk_expr, edge_expr);

    //RJC RC#2013/08/13 (edvard) you added the following comment but I don't see any checking being done, where is that check?
    //check validity for clock expression
    popScope(); //block and namedDeclarationScope
    //Process statement has no effect
    mCurrentScope -> sValue(NULL);
    mSensitivityList = 0;
    mSignalsCheckedAgainstSensList.clear();
    setEdgeExpr(0);
    mInProcessStatement = false;
}

/**
 *@brief VhdlWaitStatement
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlWaitStatement, node) 
{
    DEBUG_TRACE_WALKER("VhdlWaitStatement", node); 
    if (node.GetTimeoutClause()) {
        SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
        mNucleusBuilder->getMessageContext()->Verific2NUTimeoutClause(&loc);
    }
    if (getLoopTopBlock()) {
        SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
        mNucleusBuilder->getMessageContext()->Verific2NUWaitInsideLoopError(&loc);
        mCurrentScope->sValue(NULL);
        setError(true);
        return ;
    }
    mInWaitStatement = true;
    if (node.UntilClause() && (ID_VHDLOPERATOR == node.UntilClause()->GetClassId())) {
        VhdlExpression* op = static_cast<VhdlExpression*>(node.UntilClause());
        INFO_ASSERT(op, "invalid VhdlExpression node");
        VhdlExpression* left = op->GetLeftExpression();
        VhdlExpression* right = op->GetRightExpression();
        if (left && (ID_VHDLIDREF == left->GetClassId()) && right && (ID_VHDLCHARACTERLITERAL == right->GetClassId())) {
            (void)createEdgeExpression(0, op);
        } else if (left && right) {
            UtVector<VhdlExpression*> clk_expr;
            clk_expr.push_back(node.UntilClause());
            UtVector<VhdlExpression*> edge_expr;
            processEdgeExpression(clk_expr, edge_expr);
        }
    } else if (node.UntilClause() && node.UntilClause()->FindIfEdge()) {
        UtVector<VhdlExpression*> clk_expr;
        clk_expr.push_back(node.UntilClause());
        UtVector<VhdlExpression*> edge_expr;
        processEdgeExpression(clk_expr, edge_expr);
    }
    mCurrentScope->sValue(NULL);
    mInWaitStatement = false;
}

/**
 *@brief VhdlComponentInstantiationStatement
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlComponentInstantiationStatement, node)
{
    DEBUG_TRACE_WALKER("VhdlComponentInstantiationStatement", node); 
    if (hasError()) {
        mCurrentScope->sValue(NULL);
        return ;
    }

    SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    const char* label = node.GetLabel()->Name();
    const char* ref = node.GetInstantiatedUnit()->Name();
    UtString primUnitName(ref);
    UtString moduleName;
    //generate instantce name
    //module name has following format
    //LibName_EntityName_ArchName -> generation implemented in VerificVhdlDecl.cpp:VhdlEntityDecl
    VhdlName* name = node.GetInstantiatedUnitNameNode();
    INFO_ASSERT(name, "invalid node");
    VhdlInstantiatedUnit* insUnit = dynamic_cast<VhdlInstantiatedUnit*>(name);
    if (! insUnit) {
        mNucleusBuilder->getMessageContext()->UnsupportedInstanceType(&source_locator, "Can't get instantiated unit", "");
        return ;
    }
    VhdlIndexedName* idxName = dynamic_cast<VhdlIndexedName*>(insUnit->GetName());
    if (!idxName) {
        //verilog
        VhdlSelectedName* selName = dynamic_cast<VhdlSelectedName*>(insUnit->GetName());
        INFO_ASSERT(selName, "invalid node");
        VhdlIdRef* libId = dynamic_cast<VhdlIdRef*>(selName->GetPrefix());
        INFO_ASSERT(libId, "invalid node");
        VhdlIdRef* entId = dynamic_cast<VhdlIdRef*>(selName->GetSuffix());
        INFO_ASSERT(entId, "invalid node");
        UtString ent(entId->Name());
        moduleName = ent;
    } else {
        //VHDL
        VhdlSelectedName* selName = dynamic_cast<VhdlSelectedName*>(idxName->GetPrefix());
        //insUnit->GetName()->PrettyPrint(std::cerr, 0);
        INFO_ASSERT(selName, "invalid node");
        VhdlIdRef* libId = dynamic_cast<VhdlIdRef*>(selName->GetPrefix());
        INFO_ASSERT(libId, "invalid node");
        VhdlIdRef* entId = dynamic_cast<VhdlIdRef*>(selName->GetSuffix());
        INFO_ASSERT(entId, "invalid node");
        VhdlIdRef* archId = (VhdlIdRef*)(idxName->GetAssocList()->At(0)); //At(idx) - return void* 
        INFO_ASSERT(archId, "invalid node");
        UtString ent(entId->Name());
        if( !VerificNucleusBuilder::hasSpecialCharacter(ent) ) {
            size_t pos = ent.find_last_of("_default");
            if (UtString::npos != pos) {
                ent = ent.substr(0, ent.size() - 8);
            }
        }
        if (mNucleusBuilder->IsTopModule(ent)) {
            moduleName = ent;
        } else {
            moduleName = UtString(libId->Name()) + UtString("_") +  ent + UtString("_") + UtString(archId->Name());
        }
    }
    INFO_ASSERT(label, "empty instance name");

    UtVector<NUPortConnection*> portConnectionVector;

    Verific2NucleusUtilities::createLegalVerilogIdentifier(moduleName);
    NUBase* module = Verific2NucleusUtilities::gNUModule(moduleName, primUnitName, &node);
    NUModule* reference = dynamic_cast<NUModule*>(module);
    INFO_ASSERT(reference, "null reference module");

    //get parent declaration scope 
    NUScope* parent_declaration_scope = getDeclarationScope();
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(parent_declaration_scope, source_locator, "Unable to get parent declaration scope");
    //FD: hasError() check to be removed
    if (hasError()) {
        mCurrentScope->sValue(NULL);
        return ;
    }
    //create module instance
    NUModuleInstance *inst = mNucleusBuilder->cModuleInstance(label, parent_declaration_scope, reference, source_locator);
    // Add the instance to the parent module or NamedDeclarationScope
    mNucleusBuilder->aModuleInstanceToScope(inst, parent_declaration_scope, source_locator);
    // Annotate module instance to current scope
    // set the module instance into V2NDesignScope so that assoc code
    // knows how to wire ports up (it needs the instance
    // created for its ports. Note that the wires are in the
    // registry).
    mCurrentScope->setModuleInstance(inst);

    Array declaredFormalPorts(POINTER_HASH) ;// temp array of all ports in the declared order
    bool instantiated_is_entity = identifyFormalPorts(&declaredFormalPorts, &node);

    //resolve the hookup
    unsigned numPortMapAspect = node.GetPortMapAspect()->Size();
    if (0 != numPortMapAspect) {
      Array orderedPortMapAspect; // temp array of port association, sorted so that any slices of a formal appear in the bit declaration order of that formal

      // first copy items from the port association list (Array of VhdlDiscreteRange*) into orderedPortMapAspect Array. This copy is created
      // such that the order of items in the orderedPortMapAspect Array are aligned with the ranges of the individual Formal items.
      // Using this order simplifies the population code for port connections.
      copyAndSortPortMapAspectArray(*(node.GetPortMapAspect()), &orderedPortMapAspect);
      if (hasError()) {
        mCurrentScope->sValue(NULL);
        return ;
      }

      // At this point orderedPortMapAspect contains all port connections defined for this instantiation.
      // They are locally sorted so that if a formal port has been split up to bits then all are adjacent to each other and they are listed
      // in the declared order of the formal.
      // Note there is still a chance that there are some formal ports that are not mentioned (and are thus unconnected). They need to be
      // inserted, this is done while we create the portConnectionVector in the order of the declared formal ports.

      if ( instantiated_is_entity ) {
        unsigned fp_idx;
        VhdlInterfaceId* formal_port;
        FOREACH_ARRAY_ITEM(&declaredFormalPorts, fp_idx, formal_port) {
          bool processed_this_formal = false;
          unsigned i;
          VhdlAssocElement* item;
          // TODO: the following is inefficient, it keeps walking over the same items in orderedPortMapAspect for every 'formal_port'
          // an improvement would be to prune processed items from orderedPortMapAspect after they have been handled.
          FOREACH_ARRAY_ITEM(&orderedPortMapAspect, i, item) {
            if ( item->FormalPart()->GetFormal() == formal_port ) {
              item->Accept(*this);
              if (hasError()) {
                mCurrentScope->sValue(0);
                return ;
              }
              processed_this_formal = true;
              NUPortConnection* port_connection = dynamic_cast<NUPortConnection*>(mCurrentScope->gValue());
              INFO_ASSERT(port_connection, "missing port_connection");
              portConnectionVector.push_back(port_connection);// save all port connections for merge and addition to the instance
            } else {
              if (processed_this_formal){
                break;            // all formals were inserted in orderedPortMapAspect such that in the case where there are multiple entries
                // for the same formal (due to bit splitting) they are adjacent to each other. So if we have processed at
                // least one item for the current formal, and the current 'item' does not match that formal then we
                // have passed all possible items from orderedPortMapAspect that could contain this formal
              }
            }
          }
          if ( ! processed_this_formal ){
            // this formal was found to be disconnected, create a unconnected port connection
            NUBase* obj = getNucleusObject(formal_port);
            NUNet* formal_net = dynamic_cast<NUNet*>(obj);
            // and tell user about this unconnected port
            SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,formal_port->Linefile());
            mNucleusBuilder->getMessageContext()->UnconnectedPort(&loc,formal_net->getName()->str());

            NUPortConnection* port_connection = mNucleusBuilder->createUnconnectedPortConnection(formal_net, loc);
            portConnectionVector.push_back(port_connection);// save all port connections for merge and addition to the instance
          }
        }
      } else {
        // instantiated object is verilog,
        // TODO here we assume the items in the portconnection list are in the correct order.
        
        if ( orderedPortMapAspect.Size() != declaredFormalPorts.Size() ){
          // here we assume that if the two lists are different sizes then there is a disconnected port.  If they are not there is no resolution
          SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
          UtString msg("Unsupported construct, Vhdl instantiated a Verilog module, with an incomplete port map. Unable to proceed.");
          mNucleusBuilder->getMessageContext()->VerificConsistency(&loc,msg.c_str());
          mCurrentScope->sValue(0);
          return ;
        }
        
        unsigned ii;
        VhdlAssocElement* item;
        FOREACH_ARRAY_ITEM(&orderedPortMapAspect, ii, item) {
          if ( item ) {
            item->Accept(*this);
            if (hasError()) {
              mCurrentScope->sValue(0);
              return ;
            }
            NUPortConnection* port_connection = dynamic_cast<NUPortConnection*>(mCurrentScope->gValue());
            INFO_ASSERT(port_connection, "missing port_connection");
            portConnectionVector.push_back(port_connection);// save all port connections for merge and addition to the instance
          }
        }
      }
    }

    // TODO handle unconnected instance ports
    // test/vhdl/lang_memory/memory4a.vhd passes Interra flow, but has "CARBON INTERNAL ERROR" in verific flow

    // add the port connection to the instance after possibly merging items associated with the same formal
    // e.g. the out1 formal is merged from this:  port map(in1 => data_in, out1(0) => data_out(2), out1(2 downto 1) => data_out(0 to 1))
    //                                  to this:  port map(in1 => data_in, out1 => {data_out(0 to 1), data_out(2)})
    addPortConnectionsAfterPotentialMerge(&portConnectionVector, inst);

    //set return value to NULL, side effect statement
    mCurrentScope -> sValue(NULL);
}

// add to declaredFormalPorts, all formal ports (VhdlIdDef for VHDL, VeriIdDef for Verilog)
// returns true if instantiated object is VHDL, and false if it is Verilog
bool VerificVhdlDesignWalker::identifyFormalPorts(Array* declaredFormalPorts, VhdlComponentInstantiationStatement* nodep){

  VhdlEntityDecl* e = NULL;
  VeriModule* m = NULL;
  Verific2NucleusUtilities::getInstantiatedEntityOrModule(nodep, &e, &m);
  // INFO_ASSERT(e, "mixed language not supported");
  if ( e ) {
    Array* ports = e->GetPortClause();
    unsigned pp;
    VhdlInterfaceDecl* port;
    FOREACH_ARRAY_ITEM(ports, pp, port) {
      (void) port;
      if ( port ) {
        Array* ids = port->GetIds();
        unsigned ii;
        VhdlInterfaceId* item;
        FOREACH_ARRAY_ITEM(ids, ii, item){
          if ( item->IsPort() ){
               declaredFormalPorts->Insert(item);
          }
        }
      }
    } 
  } else if (m) {
    Array* ports = m->GetPorts();
    unsigned pp;
    VeriIdDef* port;
    FOREACH_ARRAY_ITEM(ports, pp, port) {
      if ( port ) {
        // (void)port->GetClassId();
        if ( port->IsPort() ){
          declaredFormalPorts->Insert(port);
        }
      }
    }
  } else {
    INFO_ASSERT(false, "unable to find formal ports for component instantiation");
  }
  return ( NULL != e );
}




// copy the items from \a rawPortMapAspect to \a orderedPortMapAspect, during the copy all items for an individual formal are
// first grouped together and they are sorted within this group so that they are arranged in the order of the declaration range of the
// formal. For example if the HDL contains:
//   component leaf1 port (in1 : in std_logic_vector(2 downto 0 );
//  ... port map ( in1(1) => data_in(2)), in1(0) => data_in(0), in1(2) => data_in(1)), out1 => data_out)
// then the orderedPortMapAspect array will list the portconnections in this order:
//  in1(2) => data_in(1), in1(1) => data_in(2), in1(0) => data_in(0), out1 => data_out
void VerificVhdlDesignWalker::copyAndSortPortMapAspectArray(const Array& rawPortMapAspect, Array* orderedPortMapAspect)
{
  unsigned i;
  VhdlDiscreteRange* first_item = NULL;
  VhdlDiscreteRange* item;
  Array unorderedPortMapAspect(rawPortMapAspect); // temp copy of rawPortMapAspect array
  Array portMapAspectsForOneFormal;

  // process items from unorderedPortMapAspect until it is empty, always taking the first non-null entry as the next formal to work on
  while ( unorderedPortMapAspect.Size() ){
    unorderedPortMapAspect.Item(0,&first_item);
    unorderedPortMapAspect.Remove(0);
    if (! first_item ) continue;
    VhdlName* formalPart = first_item->FormalPart();
    VhdlIdDef* formalIdDef = formalPart->GetFormal();

    // move this association item to the array used for a single formal
    portMapAspectsForOneFormal.InsertLast(first_item);
    // now add in any other association items that are for the formal identified in formalIdDef
    FOREACH_ARRAY_ITEM(&unorderedPortMapAspect, i, item) {
      VhdlName* itemFormalPart = item->FormalPart();
      VhdlIdDef* itemFormalIdDef = itemFormalPart->GetFormal();
      
      if ( itemFormalIdDef == formalIdDef ) {
        // matching formalIds, move this item to portMapAspectsForOneFormal
        portMapAspectsForOneFormal.InsertLast(item);
        unorderedPortMapAspect.Insert(i,NULL); // use null so that the above FOREACH_ARRAY_ITEM iterator will continue to work
      }
    }

    // now sort the items in portMapAspectsForOneFormal, based on the declared range of the formal
    unsigned numPortMapsForOneFormal = portMapAspectsForOneFormal.Size();
    if ( 1 == numPortMapsForOneFormal ) {
      // there was only a single entry for this formal, just put this association item at end of ordered list as there is nothing to sort
      orderedPortMapAspect->Append(&portMapAspectsForOneFormal);
      portMapAspectsForOneFormal.Reset();
    }
    else {
      // remove items from portMapAspectsForOneFormal (in the proper order), and insert them in orderedPortMapAspect

      // to know the proper order we need the declared range for the formal, we get this from the current constraint on the IdDef for the
      // formal, this constraint was created by Verific code during static elaboration from the type def(s) used to declare the type for the
      // formal.  This same constraint information could be reconstructed by working through the declaration information for the formal, but
      // it is simpler to just get it from the Constraint().
      VhdlName* whole_formal = formalPart->GetPrefix();
      VhdlIdDef* id_def = whole_formal->GetId();
      INFO_ASSERT(id_def, "invalid id_def");
      VhdlConstraint* prefix_constraint = id_def->Constraint();
      INFO_ASSERT(prefix_constraint, "invalid prefix constraint");

      // Get bounds of prefix
      VhdlValue* left = prefix_constraint->Left();
      VhdlValue* right = prefix_constraint->Right();
      INFO_ASSERT(left && right, "Unexpected NULL prefix bounds");
      INFO_ASSERT(left->IsIntVal() && right->IsIntVal(), "Unexpected non-integer found for prefix bounds");
      SInt64 prefix_left = left->Integer();
      SInt64 prefix_right = right->Integer();
      // figure out if it is TO or DOWNTO, and setup the lookingForIndex with first index
      bool isDownTo = (prefix_left > prefix_right);
      SInt64 lookingForIndex = prefix_left;


      while ( portMapAspectsForOneFormal.Size() ){
        // keep processing items from portMapAspectsForOneFormal until we have moved all to the orderedPortMapAspect
        if ( ! (portMapAspectsForOneFormal.At(0)) ) {
          // items that have been moved to orderedPortMapAspect are set to null as they are copied (see comment "done with this item" below)
          // and then recognized as NULL entries here and removed from portMapAspectsForOneFormal array
          portMapAspectsForOneFormal.Remove(0);
          continue;
        }

        bool found = false;
        FOREACH_ARRAY_ITEM(&portMapAspectsForOneFormal, i, item) {
          if (! item ) continue;
          VhdlName* itemFormalPart = item->FormalPart();
          Array* assoc_list = itemFormalPart->GetAssocList(); // array of VhdlDiscreteRange
          INFO_ASSERT(assoc_list, "must have an assocList");
          INFO_ASSERT((assoc_list->Size() == 1), "must have an assocList with one entry");
          VhdlDiscreteRange* itemRange = NULL;
          assoc_list->Item(0,&itemRange);
          SInt64 itemLeftIndex = -1;
          SInt64 itemRightIndex = -2;
          if ( itemRange->IsInteger() ) {
            VhdlInteger* vh_integer = dynamic_cast<VhdlInteger*>(itemRange);
            INFO_ASSERT(vh_integer, "unable to get Integer value from object that claims to be Integer");
            itemLeftIndex = itemRightIndex = vh_integer->GetValue();
          } else if (itemRange->IsRange() ){
            VhdlExpression* left_expr = itemRange->GetLeftExpression();
            VhdlExpression* right_expr = itemRange->GetRightExpression();
            INFO_ASSERT(left_expr->IsInteger() && right_expr->IsInteger(), "terms of assoc list must be integers");

            VhdlInteger* vh_integer = dynamic_cast<VhdlInteger*>(left_expr);
            INFO_ASSERT(vh_integer, "if IsInteger() returned true then this must be an integer");
            itemLeftIndex = vh_integer->GetValue();
            //  the above could be done with: left_expr->Evaluate(0,0,0)->Integer(); (but we also need to delete the new'd object returned by Evaluate()

            vh_integer = dynamic_cast<VhdlInteger*>(right_expr);
            INFO_ASSERT(vh_integer, "if IsInteger() returned true then this must be an integer");
            itemRightIndex = vh_integer->GetValue();
          }
          if ( lookingForIndex == itemLeftIndex ){
            found = true;
            orderedPortMapAspect->Insert(item);
            lookingForIndex = itemRightIndex  + (isDownTo ? -1 : 1);
            portMapAspectsForOneFormal.Insert(i,NULL); // done with this item
            // break;  // originally I thought to break here, but realized that if the portMapAspects are declared in the correct order
                       // then we will continue to make progress while walking through the remainder of portMapAspectsForOneFormal and
                       // thus only end up doing a single traverse of the loop
          }
        }
        if ( !found ) {
          SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,first_item->Linefile());
          UtString msg("Unable to find a specification for the actual bit(s) starting at index: ");
          msg << lookingForIndex;
          msg << " while processing the formal named: " << whole_formal->Name() << ". Unable to proceed.";
          mNucleusBuilder->getMessageContext()->VerificConsistency(&src_loc,msg.c_str());
          return;
        }
      }
    }
  }
}



// VHDL allows a component instantiation to express a port map where connections to a formal are broken into bits or slices
// Thus
//  (assume that the formal, in1 is declared as 3 downto 0, and note here that the parts of in1 are NOT listed in the order 3,2,1,0)
//   c2: leaf2 port map ( in1(2 downto 0) => data_in(21 downto 19), in1(3) => data_in(25), out1 => data_out(8 to 11));
// This is equivalent to:
//   c2: leaf2 port map (in1 => (data_in(25) & data_in(21 downto 19)), out1 => data_out(8 to 11));
//
// This method handles the merging of the actuals into a single concatenated item.
// Then this method adds the resulting port connections (merged or not) to \a inst.
//
// this method assumes that the portconnectsions in \a portConnectionVector are such that all items for a particular formal net are listed
// in the same order as the declaration range of that net.
// 
// TODO in this method
//  1. investigate what types of splitting is allowed, for example can the formal be a concatenation itself?
void VerificVhdlDesignWalker::addPortConnectionsAfterPotentialMerge(UtVector<NUPortConnection*>* portConnectionVector, NUModuleInstance* inst)
{
  // map from formal to a vector of all port connections that are connected to that formal, the order in this vector matches the declared order of the formal
  UtMap<NUNet*, UtVector<NUPortConnection*> > formalToPortConnectionMap;

  UInt32 num_unique_formals = 0;

  UtVector<NUNet*> orderedFormals; // formal nets (in declaration order seen in the HDL)
  UInt32 s = portConnectionVector->size();
  for (UInt32 j = 0; j < s; ++j) {
    NUPortConnection* current_pc = (*portConnectionVector)[j];
    NUNet* formal = current_pc->getFormal();
    INFO_ASSERT(formal, "unable to get formal part of a port connection");

    //collect all portconnections with same formal
    formalToPortConnectionMap[formal].push_back(current_pc);
    // and keep track of the order we found unique formals
    if ( num_unique_formals != formalToPortConnectionMap.size() ){
      // we must have just added an entry to formalToPortConnectionMap, so this is the first time that formal has been seen.
      orderedFormals.push_back(formal);
      num_unique_formals = formalToPortConnectionMap.size();
    }
  }

  // merge any actuals that were listed as bits/slices for the same formal by making a concatentation of the actuals
  // we process all port connections in the order that the original formals were first mentioned in the HDL.
  // Merged or not they are added to the instance here.
  UInt32 numFormals = orderedFormals.size();
  for (UInt32 oo = 0; oo < numFormals; ++oo) {
    NUNet* formal = orderedFormals[oo];

    UtMap<NUNet*, UtVector<NUPortConnection*> >::iterator iter = formalToPortConnectionMap.find(formal);
    INFO_ASSERT(!(formalToPortConnectionMap.end() == iter ), "unable to find map item for formal");
    INFO_ASSERT((iter->first == formal), "internal map corruption");

    UtVector<NUPortConnection*> pc_vector = iter->second;
    UInt32 numPortConnections = pc_vector.size();
    if (1 == numPortConnections) {
      NUPortConnection* current_pc = pc_vector[0];
      inst->addPortConnection(current_pc);
      current_pc->setModuleInstance(inst);
      continue;  // there was just a single entry for this formal, it is now in the inst so we are done with it
    }

    // merge the actuals for this formal with a concat operation
    // get the actual expressions (they are in the correct order)
    NUExprVector expression_vector;
    NULvalueVector lvalue_vector;
    NUPortConnectionInput* current_pc_input = NULL;
    NUPortConnectionOutput* current_pc_output = NULL;
    NUPortConnectionBid* current_pc_bid = NULL;
    for (UInt32 jj = 0; jj < numPortConnections; ++jj) {
      NUPortConnection* current_pc = pc_vector[jj];

      INFO_ASSERT( ( (current_pc_input = dynamic_cast<NUPortConnectionInput*>(current_pc)) ||
                     (current_pc_output = dynamic_cast<NUPortConnectionOutput*>(current_pc)) ||
                     (current_pc_bid = dynamic_cast<NUPortConnectionBid*>(current_pc)) ), "unknown port connection type");


      if ( current_pc_input ){
        expression_vector.push_back(current_pc_input->getActual());
        current_pc_input->setActual(0); // ownership of actual has been transfered to expression_vector
      } else if ( current_pc_output ){
        lvalue_vector.push_back(current_pc_output->getActual());
        current_pc_output->setActual(0); // ownership of actual has been transfered to lvalue_vector
      } else if ( current_pc_bid ){
        lvalue_vector.push_back(current_pc_bid->getActual());
        current_pc_bid->setActual(0); // ownership of actual has been transfered to lvalue_vector
      }
    }
    // create the concat, reuse the first port connection, and attach to the instance
    if ( current_pc_input ){
      NUExpr* concat = new NUConcatOp(expression_vector, 1, expression_vector[0]->getLoc());
      NUPortConnectionInput* current_pc = dynamic_cast<NUPortConnectionInput*>(pc_vector[0]);
      current_pc->setActual(concat); //reuse the first port connection by setting the actual to the concat
      inst->addPortConnection(current_pc);
      current_pc->setModuleInstance(inst);
    } else if ( current_pc_output ){
      NULvalue* concat = new NUConcatLvalue(lvalue_vector, lvalue_vector[0]->getLoc());
      NUPortConnectionOutput* current_pc = dynamic_cast<NUPortConnectionOutput*>(pc_vector[0]);
      current_pc->setActual(concat); //reuse the first port connection by setting the actual to the concat
      inst->addPortConnection(current_pc);
      current_pc->setModuleInstance(inst);
    } else if ( current_pc_bid ){
      NULvalue* concat = new NUConcatLvalue(lvalue_vector, lvalue_vector[0]->getLoc());
      NUPortConnectionBid* current_pc = dynamic_cast<NUPortConnectionBid*>(pc_vector[0]);
      current_pc->setActual(concat); //reuse the first port connection by setting the actual to the concat
      inst->addPortConnection(current_pc);
      current_pc->setModuleInstance(inst);
    }

    // now remove all but the first portconnection, they are no longer needed
    // (the first portconnection was reused above to hold the concat so we keep it (start with jj=1))
    for (UInt32 jj = 1; jj < numPortConnections; ++jj) {
      NUPortConnection* deleted_pc = pc_vector[jj];
      delete deleted_pc;
    }
  }
}

// Steve Lim 2013-12-07: Validate a selected signal assignment prior to population.
// RJC thinks: can't be interra match, so is this dead/
bool VerificVhdlDesignWalker::validateSelectedSignalAssignment(
  VhdlSelectedSignalAssignment& node,
  SourceLocator&                src_loc)
{
  VhdlExpression* target = node.GetTarget();
  VhdlExpression* expression = node.GetExpression();
  Array* waveforms = node.GetSelectedWaveforms();
  //rjc thinks: why do we need these tests here? could any of these situations get past the parser?
  if (!target)
  {
    V2N_INFO_ASSERT(false, node, "Internal Error: Missing target in selected signal assignment");
  }
  if (!expression)
  {
    V2N_INFO_ASSERT(false, node, "Internal Error: Missing expression in selected signal assignment");
  }
  if (!waveforms)
  {
    V2N_INFO_ASSERT(false, node, "Internal Error: Missing waveforms in selected signal assignment");
  }
  unsigned int i;
  VhdlTreeNode* item;
  FOREACH_ARRAY_ITEM(waveforms, i, item)
  {
    VhdlSelectedWaveform* waveform = static_cast<VhdlSelectedWaveform*>(item);
    if (!waveform)
    {
      V2N_INFO_ASSERT(false, node, "Internal Error: Invalid or missing waveform in selected signal assignment");
    }
    if (waveform->GetWaveform()->Size() > 1)
    {
      mNucleusBuilder->getMessageContext()->Verific2NUMultipleWaveform(&src_loc);
      mCurrentScope->sValue(NULL);
      setError(true);
      return false;
    }
  }
  return true;
}

/**
 *@brief VhdlSelectedSignalAssignment
 *
 * selected_signal_assignment ::=
 *                  WITH expression SELECT
 *                           target <= options selected_waveforms
 * e.g. 
 *       with cond(0 to 1)  select
 *           output1 <= 1 when "00",
 *                      2 when "01",
 *                      3 when "10",
 *                      4 when "11";
 *
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlSelectedSignalAssignment, node)
{
  DEBUG_TRACE_WALKER("VhdlSelectedSignalAssignment", node); 
  SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  if (!validateSelectedSignalAssignment(node, source_locator))
  {
    mCurrentScope->sValue(NULL);
    return;
  }
  VhdlExpression* target = node.GetTarget();
  VhdlExpression* expression = node.GetExpression();
  Array* waveforms = node.GetSelectedWaveforms();
  NULvalue* target_lval = 0;
  mCurrentScope->sContext(V2NDesignScope::UNKNOWN_CONTEXT); // RJC I think: why do we not know that the context is an assign? but I see no enum value for an assignment
  
  target->Accept(*this);  // RJC I think: we should populate the Lvalue from target, don't just create the NUnet here
  //get target net
  NUNet* tgt = dynamic_cast<NUNet*>(mCurrentScope->gValue());
  INFO_ASSERT(tgt, "invalid net");

  // Setup the target constraint. Required for RHS aggregates of the
  // form (others=>'0').
  VhdlConstraint* saved_constraint = mTargetConstraint;
  mTargetConstraint = getTargetConstraint(target, NULL, mDataFlow);
  
  unsigned i;
  VhdlTreeNode* item;
  NUIf* first_if = 0;
  NUIf* current_if = NULL;           // holds the current nucleus if stmt
  //iterate through all waveforms
  FOREACH_ARRAY_ITEM(waveforms, i, item) {
      //populate ith waveform
      //this will call VhdlSelectedWaveform
      item->Accept(*this);
      UtVector<NUBase*> val;
      //get choice and expression vector
      //val vector choices and waveform expressions will stored
      //in following form (choice1, expr1, choice2, expr2, ....)
      mCurrentScope->gValueVector(val);
      INFO_ASSERT(0 == val.size() % 2, "vector size check faild, should be pairs of choice/expr");
      //iterate throught all selected waveforms and create corresponding nucleus objects
      for (UInt32 l = 0; l < val.size(); l += 2) {
        //create target lvalue object
        //for target net
        target_lval = mNucleusBuilder->createIdentLvalue(tgt, source_locator);

        // RJC I think: this creation of nu_select_expr should be pulled up, outside of two loops, the current implementation seems to be creating multiple copies of it, when one would suffice.
        // create nucleus object for the conditional expression ("WITH condition SELECT")
        // so it can be used below to create the if condition(s)
        NUExpr* nu_select_expr = NULL;
        //trying to evaluate as constant
        VhdlValue* value = expression->Evaluate(0, mDataFlow, 0);
        if (value) {
            //expression is constant

            //create corresponding nucleus constant object
            // FD TODO: correct sign (value->IsSigned() can't be used)
            bool isSigned = value->IsSigned() ? true : false;
            nu_select_expr = NUConst::createKnownIntConst(value->Integer(), 32, isSigned, source_locator);
            delete value;
        } else {
            //expression isn't constant

            //populate as rvalue expression
            mCurrentScope->sContext(V2NDesignScope::RVALUE); // RJC I think: why not define a V2NDesignScope::EXPR since that is what we are allowing here?
            expression->Accept(*this);
            nu_select_expr = dynamic_cast<NUExpr*>(mCurrentScope->gValue());            //get populated rvalue expression
            mCurrentScope->sValue(NULL); // and clear so we don't try to use it again
        }
        INFO_ASSERT(nu_select_expr, "Unable to create expression for condition");
        //Get selected waveform
        //choice
        //if rhs == 0 - custom case for VhdlOthers
        //right side condition for selected assignment
        NUExpr* rhs = dynamic_cast<NUExpr*>(val[l]);
        //ret expression
        //correspongind nucleus object for waveform_elemnt whith rhs choice
        //here we can have 2 kind of NU objects NUExpr or NUNet
        NUExpr* rval = dynamic_cast<NUExpr*>(val[l + 1]);
        if (! rval) {
            NUNet* net = dynamic_cast<NUNet*>(val[l + 1]);
            INFO_ASSERT(net, "waveform_element neither NUExpr* nor NUNet*");
            rval = mNucleusBuilder->createIdentRvalue(net, source_locator);
        }
        INFO_ASSERT(rval, "invalid expression");
        //create the non-blocking assign statements that we will need
        // e.g. for this:
        //   with cond select
        //        output <= in1 when 0 to 100,
        //                  in2 when 101 to 200;
        //
        // we will need the following two assignments in turn
        //        output <= in1;
        //        output <= in2
        NUNonBlockingAssign* nba = new NUNonBlockingAssign(target_lval, rval, false, source_locator, false);
        INFO_ASSERT(nba, "Invalid assign object");
        //create condition expression for checking selection
        NUBinaryOp* bin_op = 0;
        if (rhs) {
            //condition expression for each choice
            bin_op = new NUBinaryOp(NUOp::eBiEq, nu_select_expr, rhs, source_locator);
            INFO_ASSERT(bin_op, "invalid binary op");
        } else {
            //corner case when last waveform contain others,
            //correspongi waveform statment can be add as "else" statment
            //on lates created if statement
            //
            //eg in6 when others
            //   we can add 'output <= in6' as 'else' statement 

            //delete the nu_select_expr object created above which was supposed to be used for creating
            //condition for if statement, so we no need this expression anymore
            //because of we added last waveform element as an 'else' statement
            delete nu_select_expr;
        }
        INFO_ASSERT(nba, "invalid object");
        //create cascaded if statement for each choice
        //eg when cond select
        //        output <= in1 when cond1
        //                  in2 when cond2
        //                  in3 when others;
        //
        //       if (cond == cond1) {
        //               output <= in1;
        //       } else {
        //           if (cond == cond2) {
        //                    output = in2;
        //           } else {
        //                 output = in3;
        //           }
        //       }
        if (NULL == first_if) {
            //root NUIf object
            current_if = new NUIf(bin_op, mNucleusBuilder->gNetRefFactory(), false, source_locator);
            current_if->addThenStmt(nba);
            //save first NUIf object as an root of this selectedsignalassignment
            first_if = current_if;
        } else {
            if (bin_op) {
                NUIf* the_if = new NUIf(bin_op, mNucleusBuilder->gNetRefFactory(), false, source_locator);
                INFO_ASSERT(the_if, "invalid if statement");
                the_if->addThenStmt(nba);
                current_if->addElseStmt(the_if);
                current_if = the_if;
            } else {
                //others case
                //add waveform_element as a else
                //else statement into latest created if statement
                current_if->addElseStmt(nba);
            }
        }
    }
  }
  delete mTargetConstraint;
  mTargetConstraint = saved_constraint;
  
  INFO_ASSERT(first_if, "invalid if object");
  //check if we inside block (begin .... end)
  //if yes then added in current block as statement
  //otherwise create always block if needed with corresponding
  //NUBlock object inside and add top-level NUIf object as statement
  //FD: This checking temporary until we get rid off scope type
  NUBlock* blockScope = dynamic_cast<NUBlock*>(mCurrentScope->gOwner());
  if (blockScope) {
    blockScope->addStmt(first_if);
  } else {
    cAlwaysBlockAndAddStatement(static_cast<NUStmt*>(first_if), source_locator);
  }
  // This does not return a statement
  mCurrentScope->sValue(NULL);
}

//
bool VerificVhdlDesignWalker::validateConditionalSignalAssignment(
    VhdlConditionalSignalAssignment& node,
    SourceLocator&                   loc)
{
  VhdlExpression* target = node.GetTarget();
  Verific::Array* waveforms = node.GetConditionalWaveforms();
  if (!target)
  {
    V2N_INFO_ASSERT(false, node, "Missing target in conditional signal assignment");
  }
  if (!waveforms)
  {
    V2N_INFO_ASSERT(false, node, "Missing waveforms in conditional signal assignment");
  }
  unsigned int i;
  VhdlConditionalWaveform* item = 0;
  FOREACH_ARRAY_ITEM(waveforms, i, item) {
      VhdlExpression* item_condition = item->Condition();
      if (item_condition) {
          if (ID_VHDLOPERATOR == item_condition->GetClassId()) {
              VhdlExpression* left = item_condition->GetLeftExpression();
              VhdlExpression* right = item_condition->GetRightExpression();
              if (left && right) {
                  if (ID_VHDLOPERATOR == left->GetClassId() &&
                      ID_VHDLATTRIBUTENAME == right->GetClassId()) {
                      VhdlAttributeName* attr = static_cast<VhdlAttributeName*>(right);
                      INFO_ASSERT(attr, "invalid vhdlattributename node");
                      const char* event = static_cast<VhdlIdRef*>(attr->GetDesignator())->Name();
                      UtString cAttributeName = UtString (event);
                      if (UtString("stable") == cAttributeName) {
                          mNucleusBuilder->getMessageContext()->Verific2NUAttributeNotOnClock(&loc);
                          mCurrentScope->sValue(NULL);
                          setError(true);
                          return false;
                      }
                  } else if (ID_VHDLOPERATOR == right->GetClassId() &&
                             ID_VHDLATTRIBUTENAME == left->GetClassId()) {
                      VhdlAttributeName* attr = static_cast<VhdlAttributeName*>(left);
                      INFO_ASSERT(attr, "invalid vhdlattributename node");
                      const char* event = static_cast<VhdlIdRef*>(attr->GetDesignator())->Name();
                      UtString cAttributeName = UtString (event);
                      if (UtString("stable") == cAttributeName) {
                          mNucleusBuilder->getMessageContext()->Verific2NUAttributeNotOnClock(&loc);
                          mCurrentScope->sValue(NULL);
                          setError(true);
                          return false;
                      }
                  }
              }
          }
      }

      if (!item)
      {
          V2N_INFO_ASSERT(false, node, "Invalid or missing waveform in conditional signal assignment");
      }
      if (item->GetWaveform()->Size() > 1)
      {
          mNucleusBuilder->getMessageContext()->Verific2NUMultipleWaveform(&loc);
          mCurrentScope->sValue(NULL);
          setError(true);
          return false;
      }
  }
  return true;
}

// Create a nested ternary operation for a vector of <condition, rhs> pairs. The 'else_expr' is required, and is the
// final 'rhs' of the nested ternary expression. For example, if the vector is size 2:
//
// condition1 ? rhs1 : (condition2 ? rhs2 : else_expr)

NUExpr* VerificVhdlDesignWalker::createTernaryOp(VhdlConditionalSignalAssignment& node, UtVector<UtPair<NUExpr*, NUExpr*> >& wVec, NUExpr* else_expr)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "Create Ternary Operator" << std::endl;
#endif
    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    INFO_ASSERT(!wVec.empty(), "There is no waveform elements");
    INFO_ASSERT(else_expr, "Invalid expr");
    UInt32 s = wVec.size();
    NUExpr* ternaryOp = new NUTernaryOp(NUOp::eTeCond, wVec[s - 1].first, wVec[s - 1].second, else_expr, loc);
    NUExpr* currOp = ternaryOp;
    for (UInt32 i = 1; i < s; ++i) {
        ternaryOp = new NUTernaryOp(NUOp::eTeCond, wVec[s - i - 1].first, wVec[s - i - 1].second, currOp, loc);
        INFO_ASSERT(ternaryOp, "failed to create ternary op");
        currOp = ternaryOp;
    }
    return ternaryOp;
}

// Create a nested if-then-else statement for a vector of <condition, rhs> pairs, where 'condition' is the if statement condition,
// and 'rhs' is the right hand side of a blocking assign, with 'lval' as the left hand side. If 'else_expr' is non-NULL, then it
// is placed in the 'else' branch of the final 'if' statement, forming the 'rhs' a blocking assign.
//
// For example, if wVec is size 3, and 'else_expr' is non-NULL:
//
// always
//   begin
//     if (cond1) then
//        lhs = rhs1;
//     else
//        begin
//         if (cond2) then
//            lhs = rhs2;
//         else
//            begin
//             if (cond3) then
//                lhs = rhs3;
//             else
//                lhs = else_expr;
//            end
//       end
//   end
//
// If 'else_expr' is NULL, the final blocking assignment is not placed in the final 'else'
//
// The nested if-then-elses are placed in a 'dummy' block.

void VerificVhdlDesignWalker::createNestedIfs(NULvalue* lval, const UtVector<UtPair<NUExpr*, NUExpr*> >& wVec, NUExpr* else_expr, const SourceLocator& loc)
{
    INFO_ASSERT(!wVec.empty(), "There is no waveform elements");

    bool lval_first_use = true;
    
    // If there is an else statement, create the blocking assign.
    NUStmt* last_stmt = NULL;
    if (else_expr) {
      last_stmt = new  NUBlockingAssign(lval, else_expr, false, loc, false);
      lval_first_use = false;
    }
    
    // Loop from last 'if' to first. Makes it easier to create the NUIf object.
    CopyContext cc(0,0);
    UInt32 s = wVec.size();
    for (SInt32 i = s-1; i >= 0; --i) {
      // We can't use the same lval over and over.
      NULvalue* cur_lval = NULL;
      if (!lval_first_use)
        cur_lval = lval->copy(cc);
      else {
        cur_lval = lval;
        lval_first_use = false;
      }
      // Create the blocking assign associated with the 'then' portion of the
      // 'if'.
      NUBlockingAssign* ba = new  NUBlockingAssign(cur_lval, wVec[i].second, false, loc, false);
      // NUStmt *last_stmt is the last if statement created. It should be hooked
      // into the 'else' portion of the next if.
      NUIf* if_stmt = new NUIf(wVec[i].first, mNucleusBuilder->gNetRefFactory(), false, loc);
      if_stmt->addThenStmt(ba);
      if (last_stmt)
        if_stmt->addElseStmt(last_stmt);
      last_stmt = if_stmt;
    }

    // Create the always block, and add the top 'if' statement to it
    cAlwaysBlockAndAddStatement(last_stmt, loc);
}

                                        
/**
 *@brief VhdlConditionalSignalAssignment
 *
 *conditional_signal_assignment ::= target <= options conditional waveforms
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlConditionalSignalAssignment, node)
{
  DEBUG_TRACE_WALKER("VhdlConditionalSignalAssignment", node); 
  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  if (!validateConditionalSignalAssignment(node, loc)) {
      return;
  }

  // Save current context (FD: although this as well seems outdated)
  V2NDesignScope::ContextType cType = mCurrentScope->gContext();

  //populate target
  VhdlExpression* target = node.GetTarget();
  mCurrentScope->sContext(V2NDesignScope::LVALUE);
  target->Accept(*this);
  NULvalue* lval = dynamic_cast<NULvalue*>(mCurrentScope->gValue());
  NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(lval, loc, "Unable to get lvalue");
  Verific::Array* waveforms = node.GetConditionalWaveforms();

  VhdlConstraint* target_constraint = getTargetConstraint(target, NULL, mDataFlow);
  INFO_ASSERT(target_constraint, "invalid target constraint");
  cType = mCurrentScope->gContext();
  mCurrentScope->sContext(V2NDesignScope::RVALUE);

  unsigned i = 0;
  VhdlConditionalWaveform* item = 0;
  bool condition_exists = false;
  UtVector<UtPair<NUExpr*, NUExpr*> > wVector;
  NUExpr* else_expr = 0;

  FOREACH_ARRAY_ITEM(waveforms, i, item) {
      VhdlExpression* item_condition = item->Condition();
      if (item_condition) {
          condition_exists = true;
          VERIFIC_CONSISTENCY_NO_RETURN_VALUE(item->GetWaveform(), *item, "invalid waveform node");
          VERIFIC_CONSISTENCY_NO_RETURN_VALUE(1 == item->GetWaveform()->Size(), *item, "not supported case");
          //populate condtion
          item_condition->Accept(*this);
          NUExpr* cond = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
          NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(cond, loc, "Unable to get condition");
          //-------------------------------------------//
          //populate waveform
          VhdlExpression* waveform = static_cast<VhdlExpression*>(item->GetWaveform()->GetLast());
          VERIFIC_CONSISTENCY_NO_RETURN_VALUE(waveform, node, "invalid VhdlExpression node");
          VhdlConstraint* oldConstraint = mTargetConstraint;
          mTargetConstraint = target_constraint;
          waveform->Accept(*this);
          NUExpr* wave = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
          NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(wave, loc, "Unable to get rvalue");
          mTargetConstraint = oldConstraint;
          wVector.push_back(UtPair<NUExpr*, NUExpr*>(cond, wave));
      } else {
          //populate else item
          VhdlConstraint* oldConstraint = mTargetConstraint;
          mTargetConstraint = target_constraint;
          item->Accept(*this);
          if (hasError()) {
            mCurrentScope->sValue(0);
            return ;
          }
          // RBS thinks: the language allows only one 'else' item without a condition.
          // RBS addresses: The language allows only be one else expression (without a conditional). 
          // It's illegal, for example, to have: lhs <= rhs when (sel = 0) else rhs2 else rhs3;
          else_expr = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
          NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(else_expr, loc, "Unable to get else expression");
          mTargetConstraint = oldConstraint;
      }
  }
  mCurrentScope->sContext(cType);
  delete target_constraint;

  NUModule* module = getModule();
  NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, loc, "Unable to get module");
  VhdlOptions* options = node.GetOptions();
  if (options && options->IsGuarded()) {
      NUBlock* block = mNucleusBuilder->cBlock(module, loc);
      (void)  mNucleusBuilder->createAlwaysBlockInScope(module, block, false, NUAlwaysBlock::eAlways, loc);

      //generate guard expressions
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(mGuard, node, "invalid guard VhdlExpression node");
      V2NDesignScope::ContextType cType = mCurrentScope->gContext();
      mCurrentScope->sContext(V2NDesignScope::RVALUE);
      mGuard->Accept(*this);
      mCurrentScope->sContext(cType);
      NUExpr* cond = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
      NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(cond, loc, "Unable to get expression");

      // FD: In verilog flow we use true for 'do_resize' argument, I wonder what impact it has on NUIf statement population ?
      // If there is some good reason why VHDL should have it false (probably resize either not needed because of VHDL language rules or because of population code organization it does happen in some other place), we can extend VerificNucleusBuilder::cIf() to have this argument optional for verilog/vhdl. 
      // otherwise it might be potential bug and we should use current VerificNucleusBuilder::cIf() approach with 'do_resize' = true; always
      // #RJC RC#2014/05/23  (cloutier) address the question about do_resize 
      bool do_resize = false;
      NUBlockingAssign* ba = new  NUBlockingAssign(lval, else_expr, false, loc, do_resize);
      NUIf* the_if = new NUIf(cond, mNucleusBuilder->gNetRefFactory(), false, loc);
      the_if->addThenStmt(ba);
      block->addStmt(the_if);
      mCurrentScope->sValue(NULL);
      return;
  }

  bool lhs_is_record = Verific2NucleusUtilities::isComposite(lval);
  bool lhs_is_memory = lval->is2DAnything();

  if (!condition_exists) {
    // No condition, must be a simple assignment
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(else_expr, loc, "Unable to get else expression");
    NUContAssign* contAssign = mNucleusBuilder->cContinuesAssign(module, lval, else_expr, loc);
    mNucleusBuilder->aContinuesAssignToModule(contAssign, module);
  } else if (else_expr && !(lhs_is_record || lhs_is_memory)) {
    // Conditions with an else expression. Can create ternary operator,
    // as long as this is not a record of memory assignment. 
    // Ternary record/memory assignments cause failures in the 'runGlobalOptimizations'
    // phase. Specifically the rewriter that converts ternary operators into if/then/else.
    NUExpr* rval = createTernaryOp(node, wVector, else_expr);
    NUContAssign* contAssign = mNucleusBuilder->cContinuesAssign(module, lval, rval, loc);
    mNucleusBuilder->aContinuesAssignToModule(contAssign, module);
  } else {
    // Create nested if's
    createNestedIfs(lval, wVector, else_expr, loc);
  }
  
  // Make it explicit there is no return value from this visitor (as well avoid any side effects);
  mCurrentScope->sValue(NULL);
}

} // namespace verific2nucleus
