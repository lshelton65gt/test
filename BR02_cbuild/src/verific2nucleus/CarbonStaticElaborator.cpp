// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// project headers
#include "verific2nucleus/CarbonStaticElaborator.h"
#include "verific2nucleus/CarbonConstraintManager.h"
#include "verific2nucleus/VhdlCSElabVisitor.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"
#include "verific2nucleus/VhdlConstraintElaborator.h"
#include "verific2nucleus/VhdlSubprogUniquifier.h"
#include "verific2nucleus/VhdlSubprogCallUniquifier.h"
#include "verific2nucleus/VhdlLoopRewriter.h"
#include "verific2nucleus/VhdlDeadBranchRewriter.h"

// verific headers
#include "VeriVisitor.h"    // Visitor base class definition
#include "VhdlVisitor.h"    // Visitor base class definition
#include "VhdlScope.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "VhdlTreeNode.h"
#include "VeriTreeNode.h"
#include "VhdlExpression.h"
#include "VhdlName.h"
#include "VhdlCopy.h"
#include "VhdlDeclaration.h"
#include "VhdlValue_Elab.h"
#include "Array.h"
#include "VhdlSpecification.h"
#include "VhdlDataFlow_Elab.h"
#include "Set.h"

// other project headers
#include "util/UtPair.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"

// standard headers
#include <sstream>

// #define DEBUG_CARBON_STATIC_ELABORATOR 1

namespace verific2nucleus {

using namespace Verific;

//===========================================================================================
// Carbon Static Elaborator class

CarbonStaticElaborator::CarbonStaticElaborator(VeriModule* module, VhdlPrimaryUnit* entity, CarbonContext* carbonContext) 
    : mTopModule(module)
    , mTopEntity(entity)
    , mCarbonContext(carbonContext)
    , mConstraintManager(new CarbonConstraintManager())
{
}

CarbonStaticElaborator::~CarbonStaticElaborator()
{
    delete mConstraintManager; 
}

bool CarbonStaticElaborator::elaborate()
{
    bool dumpVerilog = ( mCarbonContext->getArgs()->getBoolValue(CarbonContext::scDumpVerilog));
    bool traceCSE = ( mCarbonContext->getArgs()->getBoolValue(CarbonContext::scDumpVerilogCSE));
    const char* fileRoot = mCarbonContext->getFileRoot();

    bool status = true;
    if ( dumpVerilog || traceCSE ){
      // PrettPrint before carbon static elab
      Verific2NucleusUtilities::dumpVerilog(mCarbonContext, fileRoot, "CSE_PreCarbonStaticElab", mTopModule, mTopEntity);
    }

    // Current approach is - do sequential runs (to make it simpler to read)
    // Constraint map creation (no change to parse-tree)
    status &= vhdlElaborateConstraints();
    if ( !status ) return false;

    if ( traceCSE ){
      Verific2NucleusUtilities::dumpVerilog(mCarbonContext, fileRoot, "CSE_1ElaborateConstraints", mTopModule, mTopEntity);
    }

    // Create unique copies of each candidate function with correct argument and return types (doesn't overwrite calls),
    // just adds to parse-tree no deletion occurs
    status &= vhdlUniquifyFunctionDeclarations();
    if ( !status ) return false;

    if ( traceCSE ){
      Verific2NucleusUtilities::dumpVerilog(mCarbonContext, fileRoot, "CSE_2UniquifyFunctionDeclarations", mTopModule, mTopEntity);
    }

    // Unroll-rewrite loops (no branch pruning done) 
    status &= vhdlRewriteLoop();
    if ( !status ) return false;

    if ( traceCSE ){
      Verific2NucleusUtilities::dumpVerilog(mCarbonContext, fileRoot, "CSE_3LoopRewrite", mTopModule, mTopEntity);
    }
    // Apply function call replacement with actual version (here recursive functions also handled
    status &= vhdlUniquifyFunctionCalls();
    if ( !status ) return false;

    if ( traceCSE ){
      Verific2NucleusUtilities::dumpVerilog(mCarbonContext, fileRoot, "CSE_4UniquifyFunctionCalls", mTopModule, mTopEntity);
    }
    // Apply conditional statement (if/elsif/else, case) branch pruning based on constants
    status &= vhdlDeadBranchRewrite();
    if ( !status ) return false;

    if ( traceCSE ){
      Verific2NucleusUtilities::dumpVerilog(mCarbonContext, fileRoot, "CSE_5DeadBranchRewrite", mTopModule, mTopEntity);
    }
    
    // Completely separate, module/task signature substitution based on carbon directives
    // Common part is - this routine as well require parse-tree modification based on user specified signatures 
    // We may decide to separate it (at least to separate file)
    status &= vhdlSubstituteModule();
    if ( !status ) return false;

    // PrettPrint after carbon static elab
    if ( dumpVerilog || traceCSE ) {
      Verific2NucleusUtilities::dumpVerilog(mCarbonContext, fileRoot, "CSE_PostCarbonStaticElab", mTopModule, mTopEntity);
    }

    return status;
}


bool CarbonStaticElaborator::vhdlRewriteLoop()
{
    VhdlLoopRewriter vhdl_loop_rewriter(mConstraintManager, this);
    bool status = true;
    if ( mTopEntity ) {
      mTopEntity->Accept(vhdl_loop_rewriter);
      status &= vhdl_loop_rewriter.replaceLoopStatements();
      // return value, should be dependent on error situations (if any), return true - means no error
      status &= !vhdl_loop_rewriter.errorEncountered();
    }
    return status;
}

// This method calculates return constraints for all function calls, including recursive
// functions. It identifies all unique function signatures (calling argument types, and return
// sizes and types), and stores them in a table that maps the function call signature onto
// a return constraint. This table can then be used to uniquify functions.
bool CarbonStaticElaborator::vhdlElaborateConstraints()
{
  bool status = true;

  // Elaborate design constraints. Build function signature ==> constraint table.  
  if (mTopEntity) {
    VhdlConstraintElaborator ce(mConstraintManager, this);
    mTopEntity->Accept(ce);
    status &= !ce.errorEncountered();
  }
  
  return status;
}

// This method creates copies of all subprograms with unconstrained arguments
// and return values into subprograms with constrained arguments and
// return values. It creates 'unique' copies of each subprogram,
// one for each distinct call signature. 
// However, it does NOT modify any of the calls to these subprograms.
// That is done after loops are unrolled (because the calls
// might be different for each iteration of the loop).
bool CarbonStaticElaborator::vhdlUniquifyFunctionDeclarations()
{
  bool status = true;

  if (mTopEntity) {
    VhdlSubprogUniquifier subprog_uniq(mConstraintManager, this);
    mTopEntity->Accept(subprog_uniq);
    // Since subprogram id's cannot be declared in a scope
    // currently being traversed by 'walkSubrogramBody',
    // we defer their declarations until after.
    status &= subprog_uniq.ProcessDeferredDeclarations();
    status &= !subprog_uniq.errorEncountered();
    if ( status ) {
      // Perform consistency check for debugging. 
      // (Prints errors)
      bool consistent = mConstraintManager->checkConsistency();
      status &= consistent;
    }
  }
  return status;
}

bool CarbonStaticElaborator::vhdlUniquifyFunctionCalls()
{
  bool status = true;
  if (mTopEntity) {
    VhdlSubprogCallUniquifier subprog_call(mConstraintManager, this);
    mTopEntity->Accept(subprog_call);
    status &= !subprog_call.errorEncountered();
  }
  
  return status;
}

bool CarbonStaticElaborator::vhdlDeadBranchRewrite()
{
  bool status = true;
  if (mTopEntity) {
    VhdlDeadBranchRewriter vhdl_dead_branch_rewriter(mConstraintManager, this);
    mTopEntity->Accept(vhdl_dead_branch_rewriter);
    status &= vhdl_dead_branch_rewriter.finish();
    status &= !vhdl_dead_branch_rewriter.errorEncountered();
  }
  return status;
}

// Completely separate (in fact might removed from here)
bool CarbonStaticElaborator::vhdlSubstituteModule()
{
    return true;
}
void CarbonStaticElaborator::reportUnsupportedLanguageConstruct(VhdlTreeNode* node, const UtString& msg)
{
  SourceLocator loc = Verific2NucleusUtilities::getSourceLocator(mCarbonContext,node->Linefile()); 
  getCarbonContext()->getMsgContext()->UnsupportedLanguageConstruct(&loc, msg.c_str());
}

void CarbonStaticElaborator::reportFailure(VhdlTreeNode* node, const char* subPhase, const UtString& msg)
{
  SourceLocator loc = Verific2NucleusUtilities::getSourceLocator(mCarbonContext,node->Linefile());
  getCarbonContext()->getMsgContext()->FailedToStaticElab(&loc, subPhase, msg.c_str());
}

} // namespace verific2nucleus
