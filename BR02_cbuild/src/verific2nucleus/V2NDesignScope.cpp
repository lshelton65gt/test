// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "verific2nucleus/V2NDesignScope.h"
#include "verific2nucleus/VerificNucleusBuilder.h"
#include "util/AtomicCache.h"
#include <iostream>

namespace verific2nucleus {

/// static member initialization 
VerificNucleusBuilder* V2NDesignScope::mNucleusBuilder = 0;
        
void V2NDesignScope::sNucleusBuilder(VerificNucleusBuilder* nucleusBuilder)
{
    mNucleusBuilder = nucleusBuilder;
}

bool V2NDesignScope::isSubprogramBlock() const
{
    return mSubprogramBlock;
}
    
void V2NDesignScope::sSubprogramBody()
{
    mSubprogramBlock = true;
}
    
bool V2NDesignScope::isLoopBlock() const
{
  return mIsLoopBlock;
}

void V2NDesignScope::sLoopBlock()
{
  mIsLoopBlock = true;
}

bool V2NDesignScope::isModuleScope() const
{
  return mIsModuleScope;
}

void V2NDesignScope::setModuleScope()
{
  mIsModuleScope = true;
}

NUAlwaysBlock* V2NDesignScope::getAlwaysBlock() const
{
  return mAlwaysBlock;
}

void V2NDesignScope::setAlwaysBlock(NUAlwaysBlock* alwaysBlock)
{
  mAlwaysBlock = alwaysBlock;
}

NUModuleInstance* V2NDesignScope::getModuleInstance() const
{
  return mModuleInstance;
}

void V2NDesignScope::setModuleInstance(NUModuleInstance* moduleInstance)
{
  mModuleInstance = moduleInstance;
}

void V2NDesignScope::sTypeInfo(V2NDesignScope::TypeInfo t)
{
  mTypeInfo = t;
}

V2NDesignScope::TypeInfo V2NDesignScope::gTypeInfo() const 
{
  return mTypeInfo;
}

NUBase* V2NDesignScope::gValue()
{
  NUBase* retValue = mValue;
  // mValue = NULL;                //RJC RC#2013/08/18 (edvard) it seems to me that this method really should be implemented as a read-once
  // method.  Similar to the way that gValueVector is implemented as read-once.
  // But when I tried to make this change I found that there there were many-many places in the code that need to be reworked to support the
  // read-once implementation.  So for now this is a question to you edvard:
  // Is there a reason why this is not implemented using the same read-once concept that is used for gValueVector?  The reason I ask is that
  // I have found that in at least one place: ( void VerificVhdlDesignWalker::VHDL_VISIT(VhdlIfStatement, node)) that the population code
  // was incorrect unless the mValue is cleared before the call to Accept.  The simplest way to do this would have been be to clear mValue
  // upon a call to gValue, but instead I was forced to insert multiple calls to mCurrentScope->sValue(NULL) just before all the calls to
  // Accept within the VHDL_VISIT visit method for VhdlIfStatement.  I suspect that the same problem exists in other places that we have not
  // found yet.
  return retValue;
}

NUNet* V2NDesignScope::gReturnNet() const
{
  return mReturnNet;
}

void V2NDesignScope::sReturnNet(NUNet* net)
{
  mReturnNet = net;
}

void V2NDesignScope::gValueVector(UtVector<NUBase*>& vec)
{
  INFO_ASSERT(vec.empty(), "argument vector should be empty");
  UInt32 s = mValueVector.size();
  for (UInt32 i = 0; i < s; ++i) {
    vec.push_back(mValueVector[i]);
  }
  mValueVector.clear();
  // Vector doesn't release it's memory after clear, we need this trick to force it release
  UtVector<NUBase*>().swap(mValueVector);
}

void V2NDesignScope::sValueVector(UtVector<NUBase*>& vec)
{
  mValueVector.clear();
  // Vector doesn't release it's memory after clear, we need this trick to force it release
  UtVector<NUBase*>().swap(mValueVector);
  UInt32 s = vec.size();
  for (UInt32 i = 0; i < s; ++i) {
    mValueVector.push_back(vec[i]);
  }
}

void V2NDesignScope::sValue(NUBase* v) 
{
    mValue = v;
}

V2NDesignScope::ContextType V2NDesignScope::gContext() const
{
    return mContext;
}

V2NDesignScope::CaseType V2NDesignScope::gCaseType() const
{
    return mCaseType;
}

void V2NDesignScope::sBitness(unsigned n)
{
  mBitness = n;
}

unsigned V2NDesignScope::gBitness() const
{
  return mBitness;
}

void V2NDesignScope::sTypeBitness(UtString s, unsigned n)
{
  mTypeBitness[s] = n;
}

void V2NDesignScope::sOwnerBlock(NUBlock*& block)
{ 
  mBlock = block;
}

NUBlock*& V2NDesignScope::gOwnerBlock() 
{
  return mBlock;
}

NUBlock* const & V2NDesignScope::gOwnerBlock() const
{
  return mBlock;
}

unsigned V2NDesignScope::gTypeBitness(UtString& s) const
{
  UtMap<UtString, unsigned>::const_iterator it = mTypeBitness.find(s);
  if (it == mTypeBitness.end()) {
    return 0;
  }
  return it->second;
}

bool V2NDesignScope::isEnumValue(UtString& s) const
{
  UtMap<UtString, UtPair<unsigned, unsigned> >::const_iterator it = 
    mEnumValue.find(s);
  return (it != mEnumValue.end());
}

bool V2NDesignScope::isTask(UtString& s) const
{
  UtMap<UtString, NUTask*>::const_iterator it = 
    mTasks.find(s);
  return (it != mTasks.end());
}

UtPair<unsigned, unsigned> V2NDesignScope::gEnumMapValue(UtString& s) const
{
  UtMap<UtString, UtPair<unsigned, unsigned> >::const_iterator it = 
    mEnumValue.find(s);
  return it->second;
}

NUTask*& V2NDesignScope::gTaskMapValue(UtString& s)
{
  UtMap<UtString, NUTask*>::iterator it = mTasks.find(s);
  return it->second;
}

void V2NDesignScope::sTask(UtString s, NUTask*& t)
{
  mTasks[s] = t;
}

NUTask* V2NDesignScope::gTask(UtString& s)
{
  V2NDesignScope* curr = this;
  while (! curr->isTask(s)) {
    curr = curr->gParentScope();
    if (! curr) {
      return 0;
    }
  }
  return curr->gTaskMapValue(s);
}

UtPair<unsigned, unsigned> V2NDesignScope::gEnumValue(UtString& s) const
{
  const V2NDesignScope* curr = this;
  while (! curr->isEnumValue(s)) {
    curr = curr->gParentScope();
    if (! curr) {
      UtPair<unsigned, unsigned> p(0, 0);
      return p;
    }
  }
  return curr->gEnumMapValue(s);
}

void V2NDesignScope::sEnumValue(UtString s, unsigned d, unsigned b)
{
  UtPair<unsigned, unsigned> p(d, b);
  mEnumValue[s] = p;
}

void V2NDesignScope::sRange(const UtPair<int, int>& p)
{
  mRange.first = p.first;
  mRange.second = p.second;
}

const UtPair<int, int>& V2NDesignScope::gRange() const
{
  return mRange;
}

void V2NDesignScope::sContext(ContextType c)
{
    mContext = c;
}

void V2NDesignScope::sCaseType(CaseType c)
{
    mCaseType = c;
}

const NUScope* V2NDesignScope::gOwner() const
{
    return mScope;
}

NUScope* V2NDesignScope::gOwner()
{
    return mScope;
}
        
void V2NDesignScope::sDeclarationScope(NUNamedDeclarationScope* s)
{
    mDeclarationScope = s;
}

void V2NDesignScope::sElabSTScope(STBranchNode* s)
{
    mElabSTScope = s;
}

void V2NDesignScope::sUnelabSTScope(STBranchNode* s)
{
    mUnelabSTScope = s;
}

const NUNamedDeclarationScope* V2NDesignScope::gDeclarationScope() const
{
    return mDeclarationScope;
}

NUNamedDeclarationScope* V2NDesignScope::gDeclarationScope()
{
    return mDeclarationScope;
}

const STBranchNode* V2NDesignScope::gElabSTScope() const
{
    return mElabSTScope;
}

STBranchNode* V2NDesignScope::gElabSTScope()
{
    return mElabSTScope;
}

const STBranchNode* V2NDesignScope::gUnelabSTScope() const
{
    return mUnelabSTScope;
}

STBranchNode* V2NDesignScope::gUnelabSTScope()
{
    return mUnelabSTScope;
}

const V2NDesignScope* V2NDesignScope::gParentScope() const
{
    return mParentScope;
}

V2NDesignScope* V2NDesignScope::gParentScope()
{
    return mParentScope;
}

void V2NDesignScope::sParentScope(V2NDesignScope* p)
{
    mParentScope = p;
}

V2NDesignScope::V2NDesignScope(NUScope* s, V2NDesignScope* p)
    : mScope(s)
    , mDeclarationScope(0)
    , mElabSTScope(0)
    , mUnelabSTScope(0)
    , mParentScope(p)
    , mValue(NULL)
    , mContext(UNKNOWN_CONTEXT)
    , mCaseType(UNKNOWN_CASE_TYPE)
    , mRange()
    , mBitness(0)
    , mTypeBitness()
    , mEnumValue()
    , mBlock(0)
    , mTasks()
    , mTypeInfo()
    , mValueVector()
    , mIsLoopBlock(false)
    , mIsModuleScope(false)
    , mAlwaysBlock(0)
    , mModuleInstance(0)
    , mReturnNet(0)
    , mIsVar(false)
    , mIsPrefix(false)
    , mIsMemNet(false)
    , mSize(0)
    , mDir(0)
    , mAggrElemBit(0)
    , mIsChoiceExists(false)
    , mStmtList(NULL)
    , mSubprogramBlock(false)
    , mContextSizeSign(0)
    , mRecordNet(0)
    , mAggregateAssocListSize(0) // RJC thinks this variable looks dead
    , mAssocElementIndex(0)
    , mIsAggregateType(false)
    , mAggregateRange(0)
{}

V2NDesignScope::~V2NDesignScope()
{}

} // namespace verific2nucleus
