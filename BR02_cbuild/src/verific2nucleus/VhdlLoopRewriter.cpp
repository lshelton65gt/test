// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// project headers
#include "verific2nucleus/CarbonStaticElaborator.h"
#include "verific2nucleus/CarbonConstraintManager.h"
#include "verific2nucleus/VhdlCSElabVisitor.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"
#include "verific2nucleus/VhdlConstraintElaborator.h"
#include "verific2nucleus/VhdlLoopRewriter.h"

// verific headers
#include "VeriVisitor.h"    // Visitor base class definition
#include "VhdlVisitor.h"    // Visitor base class definition
#include "VhdlScope.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "VhdlTreeNode.h"
#include "VeriTreeNode.h"
#include "VhdlExpression.h"
#include "VhdlName.h"
#include "VhdlCopy.h"
#include "VhdlDeclaration.h"
#include "VhdlValue_Elab.h"
#include "Array.h"
#include "VhdlSpecification.h"
#include "VhdlDataFlow_Elab.h"
#include "Set.h"

// other project headers
#include "util/UtPair.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"

// standard headers
#include <sstream>

namespace verific2nucleus {

using namespace Verific;

//=====================================================================================
// Constructor
VhdlLoopRewriter::VhdlLoopRewriter(CarbonConstraintManager *cm, CarbonStaticElaborator* se) 
    : VhdlCSElabVisitor(cm, se)
    , mLoopParentNode(NULL)
    , mVisited()
    , mLoopReplacements()
{
}

//=====================================================================================
// Utilities


bool VhdlLoopRewriter::hasLoopStatement(Array* statementList)
{
    unsigned i = 0;
    VhdlTreeNode* stmt = 0;
    FOREACH_ARRAY_ITEM(statementList, i, stmt) {
        if ( ! stmt ) continue;
        if (ID_VHDLLOOPSTATEMENT == stmt->GetClassId()) {
            return true;
        }
    }
    return false;
}

// Determine whether supplied signature corresponds to a
// subprogram call that has already been visited.
bool VhdlLoopRewriter::alreadyVisited(UtString signature)
{
    bool visited = true;
    if (mVisited.find(signature) == mVisited.end()) {
        visited = false;
        mVisited.insert(signature);
    }
    return visited;
}

//=====================================================================================
///@name Visitors
//Annotate parent scope where loop (not generate loop) appears (TODO : add others if any)
void VhdlLoopRewriter::VHDL_VISIT(VhdlCaseStatementAlternative, node)
{
    // Save parent node
    VhdlTreeNode* saved_parent_node = mLoopParentNode;
    // Mark parent node 
    mLoopParentNode = &node;
    // Call of Base class Visit
    VhdlVisitor::VHDL_VISIT_NODE(VhdlCaseStatementAlternative, node) ;
    // Restore parent node
    mLoopParentNode = saved_parent_node;
}

void VhdlLoopRewriter::VHDL_VISIT(VhdlIfStatement, node)
{
    // Save parent node
    VhdlTreeNode* saved_parent_node = mLoopParentNode;
    // Mark parent node
    mLoopParentNode = &node;
    // Call base class visitor 
    VhdlCSElabVisitor::VHDL_VISIT_NODE(VhdlIfStatement, node);
    // Restore parent node
    mLoopParentNode = saved_parent_node;
}

void VhdlLoopRewriter::VHDL_VISIT(VhdlElsif, node)
{
    // Save parent node
    VhdlTreeNode* saved_parent_node = mLoopParentNode;
    // Mark parent node
    mLoopParentNode = &node;
    // Call base class visitor 
    VhdlCSElabVisitor::VHDL_VISIT_NODE(VhdlElsif, node);
    // Restore parent node
    mLoopParentNode = saved_parent_node;
}

void VhdlLoopRewriter::VHDL_VISIT(VhdlProcessStatement, node)
{
    // Save parent node
    VhdlTreeNode* saved_parent_node = mLoopParentNode;
    // Mark parent node
    mLoopParentNode = &node;
    // Call base class visitor 
    VhdlVisitor::VHDL_VISIT_NODE(VhdlProcessStatement, node);
    // Restore parent node
    mLoopParentNode = saved_parent_node;
}

void VhdlLoopRewriter::VHDL_VISIT(VhdlBlockStatement, node)
{
    // Save parent node
    VhdlTreeNode* saved_parent_node = mLoopParentNode;
    // Mark parent node
    mLoopParentNode = &node;
    // Call base class visitor 
    VhdlVisitor::VHDL_VISIT_NODE(VhdlBlockStatement, node);
    // Restore parent node
    mLoopParentNode = saved_parent_node;
}

void VhdlLoopRewriter::VHDL_VISIT(VhdlSubprogramBody, node)
{
    if (mEnableSubprogramBodyWalk) {
        mEnableSubprogramBodyWalk = false;
        // The parent node may be changed by the visit to the subprogram body.
        // Save parent node
        VhdlTreeNode* saved_parent_node = mLoopParentNode;
        // Mark parent node
        mLoopParentNode = &node;
        // Call base class visitor 
        VhdlVisitor::VHDL_VISIT_NODE(VhdlSubprogramBody, node);
        // Restore parent node
        mLoopParentNode = saved_parent_node;
    }
}

//TODO probably can be shared via VhdlCSElabVisitor
void VhdlLoopRewriter::VHDL_VISIT(VhdlOperator, node)
{
    // First, process left and right (if any) expressions. They may contain
    // function calls.
    if (node.GetLeftExpression()) {
        node.GetLeftExpression()->Accept(*this);
    }
    if (node.GetRightExpression()) {
        node.GetRightExpression()->Accept(*this);
    }

    // Next, check to see if this is a user supplied overloaded operator
    VhdlIdDef* opId = node.GetOperator();
    if (Verific2NucleusUtilities::isNonIeeeOverloadedOperator(opId)) {

        // Keep an eye on this - logic below needs to be consistent with VhdlSubprogCallUniquifier  and VhdlSubprogBodyUniquifier
        // Calculate signature
        UtString sig = m_cm->getOverloadedOperatorSignature(&node, mDataFlow);

        // Lookup the uniquified subprogram body.
        VhdlSubprogramBody* subprog_body = m_cm->lookupSubprogramBody(sig);
        if (!subprog_body) {
            // TODO: Replace this with a better error.
            UtString msg("ERROR: could not find a subprogram body for signature '");
            msg << sig << "' during for loop rewrite. This probably means the signature is calculated with a different value during for loop rewrite than it had during constraint elaboration and subprogram body uniquification";
            reportFailure(&node, msg);
            return;
        }

        // Now walk the subprogram body
        if (!alreadyVisited(sig)) {
            // Create an arguments array 
            Array *args = new Array();
            if (node.GetLeftExpression()) {
                args->Insert(node.GetLeftExpression());
            }
            if (node.GetRightExpression()) {
                args->Insert(node.GetRightExpression());
            }

            // Now traverse the overloaded operator declaration
            mEnableSubprogramBodyWalk = true;
            walkSubprogramBody(subprog_body, args);

            delete args;
        }

    }

}

void VhdlLoopRewriter::VHDL_VISIT(VhdlIndexedName, node) 
{
    // First, process all associations. They may contain function calls themselves.
    unsigned i = 0;
    VhdlTreeNode* assoc = 0;
    FOREACH_ARRAY_ITEM(node.GetAssocList(), i, assoc) {
        if (assoc) assoc->Accept(*this);
    }

    // We need to visit the prefix for cases like: funcall(arg1)(3 downto 0)
    if (node.GetPrefix()) node.GetPrefix()->Accept(*this);

    // If it's a function or procedure call (but not one of the builtin functions)
    if (isTraversableSubprog(&node)) {

        // Calculate signature
        UtString sig = m_cm->getSubprogramSignature(&node, mDataFlow);

        // Lookup the uniquified subprogram body.
        VhdlSubprogramBody* subprog_body = m_cm->lookupSubprogramBody(sig);
        if (!subprog_body) {
            // TODO: Replace this with a better error.
            UtString msg("ERROR: could not find a subprogram body for signature '");
            msg << sig << "' during for loop rewrite. This probably means the signature is calculated with a different value during for loop rewrite than it had during constraint elaboration and subprogram body uniquification.";
            reportFailure(&node, msg);
            return;
        }

        // Now walk the subprogram body
        if (!alreadyVisited(sig)) {
            Array *args = new Array;
            Verific2NucleusUtilities::getSubprogramArgs(args, &node, true /* use defaults */);
            mEnableSubprogramBodyWalk = true;
            walkSubprogramBody(subprog_body, args);
            delete args;
        }
    } 
}


/**
 * loop_statement ::=
 *             [ loop_lable : ]
 *                   [ iteration_scheme ] LOOP
 *                         sequence_of_statements
 *                   END LOOP [ loop_lable ] ;
 */
void VhdlLoopRewriter::VHDL_VISIT(VhdlLoopStatement, node)
{
    // Get saved parent tree node which contains for loop statement
    VhdlTreeNode* parent_node = mLoopParentNode; 
    // See if the loop is a candidate for unrolling
    bool forcedByUser = getCarbonContext()->getArgs()->getBoolValue(CarbonContext::scVhdlLoopUnroll);
    bool rewriteLoop = Verific2NucleusUtilities::loopShouldBeUnrolled(&node, mDataFlow, forcedByUser); 
    // By the time we get here, we know that any loop that must be rewritten in order to calculate subprogram argument and return
    // sizes CAN be rewritten. Otherwise, the contraint elaboration phase would have failed.
    if (rewriteLoop) {
        // TODO jump statements doesn't work properly with labels (disabling labeled statements), 
        // this is current VHDL population problem - needs to be fixed
        // Get current scope id, if no label generate name manually (StaticElaborate will take care of 
        // uniquification and checking against other local declaration names).
        VhdlScope* current_scope = node.LocalScope();
        INFO_ASSERT(current_scope, "NULL local scope");
        VhdlIdDef* current_scope_id = NULL;
        if (current_scope->GetOwner()) {
            // create new id with same name
            current_scope_id = new VhdlLabelId(Strings::save(current_scope->GetOwner()->Name())); 
            // Keep linefile info up-to-date
            current_scope_id->SetLinefile(node.Linefile());
        } else { 
            //if no id for current scope, then generate new one (Verific will take care if such id already exists)
            //TODO: the part will be entirely removed if we turn on 'vhdl_create_implicit_labels' flag in VhdlRuntimeFlags.h 
            //TODO: parent scope annotation as well might get removed if we turn on this flag 
            // (need to check whether or not other blocks - like process, get id's)
            UtString name; 
            name << "named_decl_lb_L" << node.Linefile()->GetLeftLine();
            current_scope_id = new VhdlLabelId(Strings::save(name.c_str())) ;
            // Keep linefile info up-to-date
            current_scope_id->SetLinefile(node.Linefile());
            //node.SetLabel(current_scope_id);
            //VhdlScope* parent_scope = current_scope->Upper();
            //parent_scope->Declare(current_scope_id) ;
            //VhdlIdRef* current_scope_closing_id = new VhdlIdRef(current_scope_id);
            //node.SetClosingLabel(current_scope_closing_id);
            //parent_scope->DeclareLabel(current_scope_id) ;
            ///node.SetImplicitLabel(current_scope_id);
            //current_scope->SetOwner(current_scope_id);
        }
        //TODO Commented code below can be used to get parent node if vhdl backpointer path for some scope statements work correctly
        ///// Get parent node from loop statement
        ///// Get parent scope
        ///VhdlScope* parent_scope = loop_node->LocalScope()->Upper();
        ///INFO_ASSERT(parent_scope, "NULL parent scope");
        ///// Get parent scope owner
        ///VhdlIdDef* parent_scope_id = parent_scope->GetOwner();
        ///INFO_ASSERT(parent_scope_id, "NULL parent scope id");
        ///// Get parse-tree statement
        ///VhdlTreeNode* parent_node = NULL;
        ///// TODO: check if there can ever be entity/architecture as a parent node for loop statement ?
        ///if (parent_scope_id->IsEntity()) {
        ///    parent_node = parent_scope_id->GetPrimaryUnit();
        ///} else if (parent_scope_id->IsArchitecture()) {
        ///    parent_node = parent_scope_id->GetSecondaryUnit();
        ///} else if (parent_scope_id->IsSubprogram()) {
        ///    // we need subprogram body
        ///    parent_node = parent_scope_id->Body();
        ///} else {
        ///    parent_node = parent_scope_id->GetStatement();
        ///}
        // We have choose to have a "blind copy" approach for statements inside for loop (so no constant propogation
        // or conditional statement branch pruning (if/elsif/else, case) will occur 
        // in this phase). Branch pruning and constant propagation to be handled later as a separate phase 
        // of CarbonStaticElaboration
        // If for loop doesn't have label we will generate temporary one and have constant i declared inside it 

        // TODO add support for special cases with unconstraint functions controling number of iterations 
        // (something not done by Verific VhdlForScheme::StaticElaborate() routine). 
        // As a solution constraint map should be used (CarbonConstraintElaborator supposed to be smart enough to add
        // this values to map) or even easier is loopShouldBeUnrolled can probably return constant range values
        // along with true/false and we will need to modify parse-tree (replace existing for loop condition with 
        // newly returned values) before calling VhdlForScheme::StaticElaborate() api 
        // (to make api work for this implicit constant cases).

        // Iterate over the possible values of loop index variable.
        // For each value of loop index variable, create a block
        // statement and elaborate that statement.
        // Return the created block statements.
        Map* new_blk_ids = new Map(STRING_HASH_CASE_INSENSITIVE) ;

        // use the Verific StaticElaborate method to actually unroll this loop,
        Array* new_statements = prepThenCallStaticElaborate(&node, new_blk_ids, current_scope_id);

        LoopReplacements* lr = new LoopReplacements(&node, parent_node, new_statements, new_blk_ids);
        delete current_scope_id;
        // Defer replace child stmts until we finish visit process, so saving to member
        mLoopReplacements.push_back(lr);
        // Traverse statements (as it's still can be context for other loops)
        // For unrolled version, as StaticElaborate api creates block labels, current loop can't be immediate parent for other loop statements, 
        // so don't have to mark parent node here
        TraverseArray(new_statements) ;
    } else {
        // We're not unrolling the loop, so create a sub data flow, and
        // just visit the statements once
        VhdlDataFlow *curDF = mDataFlow;
        Array data_flows(1);
        mDataFlow = Verific2NucleusUtilities::setupLoopDataFlow(&data_flows, mDataFlow, &node);
        // loop statement can contain loops itself, so marking parent scope as well here
        mLoopParentNode = &node;
        // Traverse statements (as it's still can be context for other loops)
        TraverseArray(node.GetStatements());
        // Since 'TraverseArray' may have changed parent pointer (e.g. for inserted block statements),
        // Restore parent dataflow
        Verific2NucleusUtilities::cleanUpDataFlows(&node, &data_flows, curDF);
        mDataFlow = curDF;
    }
    // Restore parent node
    mLoopParentNode = parent_node;
}

void LoopReplacements::validateMembers(CarbonContext* cc) {
  INFO_ASSERT(mLoopStatement, "NULL loop statement");
  INFO_ASSERT(mParentNode, "NULL loop statement parent node");
  if ( mLoopStatement->GetStatements() && mLoopStatement->GetStatements()->Size()) {
    // the following check is not an assert, since we can also get here if the loop iterator has a null range, 
    // there appears to be no simple way to detect the null range so we just print a message
    if ( NULL == mNewBlkStmts ) {
      SourceLocator loc = Verific2NucleusUtilities::getSourceLocator(cc,mLoopStatement->Linefile());
      cc->getMsgContext()->EmptyLoopRewrite(&loc);
    }
    INFO_ASSERT(mNewBlkIds, "NULL new block Ids list");
  }
}

/* Do the necessary preparation for calling StaticElaborate on \a pnode, then do the call to StaticElaborate, and then cleanup after the call.
 *  Currently this involves:
 *    - the save/restore of _path_name/_instance_name arrays as well as save/restore of mDataFlow.
 *    - after StaticElab but before restoration of mDataFlow clear the flag on all VHDLBlockStatments indicating they were StaticElaborated
 *      (so that Evalueate will work properly on them after this StatiElab)
*/
Array* 
VhdlLoopRewriter::prepThenCallStaticElaborate(Verific::VhdlLoopStatement* pnode, Map* new_blk_ids, VhdlIdDef* current_scope_id){

  Array* new_statements;
  VhdlForScheme* fScheme = dynamic_cast<VhdlForScheme*>(pnode->GetIterScheme());
  INFO_ASSERT(fScheme, "Incorrect for loop type");
  
  Array *start_path_name = fScheme->_path_name ;             // save
  Array *start_instance_name = fScheme->_instance_name ;     // save
  VhdlScope *start_scope = pnode->LocalScope(); // fScheme->_present_scope;a

  fScheme->_path_name = new Array(2);
  fScheme->_instance_name = new Array(2);

  primePathNames(fScheme, start_scope? start_scope->Upper() : NULL ); // insert everything above start_scope into _path_name and
                                                                      // _instance_name for fScheme


  // StaticElaborate will modify the dataflow, so save before elaborating, and restore afterward, the unroll process should not be allowed to affect the dataflow
  VhdlDataFlow *savedDF = mDataFlow;
  mDataFlow = new VhdlDataFlow(savedDF);
  mDataFlow->SetNonconstExpr(); // this marks the new dataflow as having a non-constant expression (which is true since we will be unrolling a loop in it and at least the loop counter will be non-const)

  new_statements = fScheme->StaticElaborate(mDataFlow, 0, pnode, new_blk_ids, current_scope_id);


  // before we restore the dataflow,
  // Iterate through new statement array, and reset static elaborated flag.
  // Necessary for function call Evaluate to work correctly.
  VhdlStatement* stmt;
  unsigned k;
  FOREACH_ARRAY_ITEM(new_statements, k, stmt) {
    if (!stmt) continue;
    VhdlBlockStatement* blk = dynamic_cast<VhdlBlockStatement*>(stmt);
    INFO_ASSERT(blk, "Expected all loop rewrite statements to be block statements");
    blk->ClearStaticElaborated();
  }

  // now start the restore
  delete mDataFlow;
  mDataFlow = savedDF;


  fScheme->ClearAttributeSpecificPath(); // this deletes array and elements from _path_name and _instance_name
  fScheme->_path_name = start_path_name;         // restore
  fScheme->_instance_name = start_instance_name; // restore
  return new_statements;
}


/*!
  primePathNames adds the current scope names to _path_name and _instance_name arrays.
  This is useful if you are going to call StaticElaborate on an object that is not the top of the design.
*/
bool VhdlLoopRewriter::primePathNames(VhdlNode* pnode, VhdlScope* current_scope){
  bool status = true;
  if ( NULL == current_scope ) return status;

  // recurse immediately via the Upper() pointer.
  // scope names are then saved after the recursion gests to the top of the Upper chain; so that the _path_name and _instance_name will be in the correct order.
  status = primePathNames(pnode, current_scope->Upper());
  
  VhdlIdDef *owner = current_scope ? current_scope->GetOwner(): NULL ;
  if (owner) {
    pnode->_path_name->InsertLast(Strings::save(owner->Name())) ;
    pnode->_instance_name->InsertLast(Strings::save(owner->Name())) ;
  }
  return status;
}

// Loop over saved loop replacement contexts and replace loop statement with new_statements array (as well declare all created id's)
bool VhdlLoopRewriter::replaceLoopStatements()
{
    bool status = true;
    /// Process loop replacements in reverse order (to process nested for loops first)
    for (UtVector<LoopReplacements*>::reverse_iterator it = mLoopReplacements.rbegin();
            it != mLoopReplacements.rend(); it++) {
        (*it)->validateMembers(getCarbonContext());
        replaceSingleLoopStatement((*it)->mLoopStatement, (*it)->mParentNode, (*it)->mNewBlkStmts, (*it)->mNewBlkIds);
        delete (*it);
    }
    return status;
}

// Inspired by VhdlGenerateStatement::ReplaceByBlocks at Vendors/verific/vhdl/vhdl_file_Stat.cpp
void VhdlLoopRewriter::replaceSingleLoopStatement(VhdlLoopStatement* loop_stmt, VhdlTreeNode* parent_node, Array* new_stmts, Map* new_ids)
{
    VhdlScope *loop_scope = loop_stmt->LocalScope();
    VhdlScope *parent_scope = loop_scope ? loop_scope->Upper(): 0;

    bool status = Verific2NucleusUtilities::undeclareLoopLabel(loop_stmt);
    if (!status) {
        UtString msg;
        msg << "Unable to find declaration scope for loop statement";
        reportFailure(loop_stmt, msg);
    }

    // Iterate over created identifiers and declare those in container scope:
    MapIter mi ;
    VhdlIdDef *id ;
    // Iterate over elaboration created identifiers
    FOREACH_MAP_ITEM(new_ids, mi, 0, &id) {
        if (!id) continue ;
        // Declare identifier in parent scope
        if (parent_scope) (void) parent_scope->Declare(id) ;
    }
    delete new_ids;

    // Iterate over created statements and add to parent_node (NOTE: 'loop_stmt' gets replaced with first element from 'new_stmts' array)
    bool replaced = Verific2NucleusUtilities::replaceChildStmtBy(parent_node, loop_stmt, new_stmts);
    if ( !replaced ) {
      // There may still be some cases in which the parent_node is not set correctly - e.g. nested or sequential for loops, case statements,
      // and so on. If so, this error will be triggered. We may need to create a stack of parent statements to handle this correctly.
      // Alternatively, Frunze believes that the end of June 2014 drop from Verific will provide sufficient functionality to easily
      // implement this without a stack.
      UtString msg;
      msg << "Unable to replace child statement of loop";
      reportFailure( parent_node, msg);
    }
    delete new_stmts ;
}

void VhdlLoopRewriter::reportFailure(VhdlTreeNode* node, const UtString& msg) {
  getCarbonStaticElaborator()->reportFailure(node, "VhdlLoopRewriter", msg); // typeid(*this).name()->c++filt will return the class name also
  mErrorDuringWalk = true;
}


} // namespace verific2nucleus
