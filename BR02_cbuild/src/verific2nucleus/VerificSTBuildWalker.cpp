// -*-C++-*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "verific2nucleus/VerificSTBuildWalker.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"
#include "util/UtStackPOD.h"
#include "util/UtIOStream.h"
#include "util/Zstream.h"
#include "util/AtomicCache.h"
#include "util/UtStringArray.h"
#include "util/CbuildMsgContext.h"
#include "hdl/HdlVerilogPath.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "iodb/IODBDesignData.h"
#include "VhdlIdDef.h"
#include "Array.h"
#include "Strings.h"  
#include "Message.h"  

#include "VhdlUnits.h"
#include "VhdlDeclaration.h"
#include "VhdlExpression.h" 
#include "VhdlStatement.h"  
#include "VhdlIdDef.h"      
#include "VhdlName.h"       
#include "VhdlConfiguration.h"
#include "VhdlSpecification.h" 
#include "VhdlMisc.h"          
#include "VhdlScope.h"
#include "VhdlValue_Elab.h"
#include "vhdl_tokens.h"
#include "vhdl_file.h"
#include "veri_file.h"

#include <iostream>

//#define DEBUG_VERIFIC_VHDL_WALKER 1

namespace verific2nucleus {

using namespace Verific;

//! implementation class for  VerificSTBuildWalker.
/*!
  Helps add branch/leaf nodes to elab/unelab symtabs.
*/
class VerificSTBuildWalkerImpl
{
public:

  enum UnelabScopeT {
    eModule, eTask, eNamedDecl
  };

  VerificSTBuildWalkerImpl(IODBDesignDataSymTabs* symtabs)
    : mSymTabs(symtabs), mSavedPrePopScope(NULL)
  {
  }

  ~VerificSTBuildWalkerImpl() {
  }

  bool addHierRefNet(StringAtom* netName)
  {
      if (mUnelabScopes.empty()) {
          return false;
      }
      mSymTabs->createNode(mUnelabScopes.top(), netName, 
                           IODBDesignDataSymTabs::eUnelab,
                           IODBDesignDataSymTabs::eLeaf);
      mSymTabs->createNode(mElabScopes.top(), netName,
                           IODBDesignDataSymTabs::eElab,
                           IODBDesignDataSymTabs::eLeaf);
      return true;
  }
    
  bool addNet(StringAtom* netName)
  {
    // No current scope to add net to.
    if (mUnelabScopes.empty()) {
      return false;
    }
    mSymTabs->createNode(mUnelabScopes.top(), netName, 
                         IODBDesignDataSymTabs::eUnelab,
                         IODBDesignDataSymTabs::eLeaf);
    mSymTabs->createNode(mElabScopes.top(), netName,
                         IODBDesignDataSymTabs::eElab,
                         IODBDesignDataSymTabs::eLeaf);
    return true;
  }

  STBranchNode* pushUnelabScope(StringAtom* unelab, UnelabScopeT scopeType)
  {
    STBranchNode* parent = NULL;
    if (!mUnelabScopes.empty() && (scopeType != eModule)) {
      parent = mUnelabScopes.top();
    }
    STSymbolTableNode* node =
      mSymTabs->createNode(parent, unelab, IODBDesignDataSymTabs::eUnelab,
                           IODBDesignDataSymTabs::eBranch);
    STBranchNode* newUnelabScope = node->castBranch();
    mUnelabScopes.push(newUnelabScope);

    if (mElabScopes.empty()) {
      // push the root elaborated branch.
      pushElabScope(unelab);
    }
    return newUnelabScope;
  }

  void popUnelabScope()
  {
    if (!mUnelabScopes.empty()) {
      mUnelabScopes.pop();
      if (mUnelabScopes.empty()) {
        // pop the root elaborated branch.
        popElabScope();
      }
    }
  }

  STBranchNode* pushElabScope(StringAtom* elab)
  {
    STBranchNode* parent = NULL;
    if (!mElabScopes.empty()) {
      parent = mElabScopes.top();
    }
    STSymbolTableNode* node =
      mSymTabs->createNode(parent, elab, IODBDesignDataSymTabs::eElab,
                           IODBDesignDataSymTabs::eBranch);
    STBranchNode* newElabScope = node->castBranch();
    mElabScopes.push(newElabScope);

    // The current unelaborated scope is the unelaborated scope corresponding
    // to this elaborated scope.
    IODBDesignNodeData* scopeInfo = IODBDesignDataBOM::castBOM(newElabScope);
    // Establish a link from elaborated ST branch node to unelaborated ST
    // branch node.
    STBranchNode* unelabScope = mUnelabScopes.top();
    scopeInfo->putUnelabScope(unelabScope);

    return newElabScope;
  }

  void popElabScope()
  {
    if (!mElabScopes.empty()) {
      mElabScopes.pop();
    }
  }

  void saveStacksAndBeginAtRoot(STBranchNode* prePopScope)
  {
    mUnelabScopes.saveStackAndBeginAtRoot();
    mElabScopes.saveStackAndBeginAtRoot();
    mSavedPrePopScope = prePopScope;
  }

  STBranchNode* restoreStacks()
  {
    mUnelabScopes.restoreStack();
    mElabScopes.restoreStack();
    STBranchNode* prePopScope = mSavedPrePopScope;
    mSavedPrePopScope = NULL;
    return prePopScope;
  }

  STSymbolTableNode* findNode(StringAtom* child, IODBDesignDataSymTabs::SymTabT stTyp)
  {
    STBranchNode* parent = mElabScopes.top();
    if (stTyp == IODBDesignDataSymTabs::eUnelab) {
      parent = mUnelabScopes.top();
    }
    return mSymTabs->findNode(parent, child, stTyp);
  }

  STBranchNode* getUnelabScope()
  {
    STBranchNode* unelabScope = NULL;
    if (!mUnelabScopes.empty()) {
      unelabScope = mUnelabScopes.top();
    }
    return unelabScope;
  }

private:
  IODBDesignDataSymTabs* mSymTabs;
  BranchStack mUnelabScopes;
  BranchStack mElabScopes;
  STBranchNode* mSavedPrePopScope;
};

// VerificSTBuildWalker class creates unelaborated and elaborated symbol table which
// has all scope names and the declarated net names. Associated with scopes
// and net names is the design information object which stores directives
// for now. In future it may contain user design type information.

    VerificSTBuildWalker::VerificSTBuildWalker(VerificPrePopInfoWalker* prePopInfo,
                                 VerificNucleusBuilder* nucleusBuilder,
                                 UtMap<VhdlTreeNode*, StringAtom*>* nameRegistry,
                                 IODBDesignDataSymTabs* symtabs)
        :mNucleusBuilder(nucleusBuilder)
        ,mNameRegistry(nameRegistry)
        ,mSTBuildImpl(NULL)
        ,mPrePopElabScope(NULL)
        ,mPrePopInfo(prePopInfo)
        //RJC RC#18  I am very confused about the naming used here, the member variable mPrePopInfo is
        //realy a STBuildWalkerHelper?  
        //And how does this STBuildWalkerHelper differ from the one in mSTBuildImpl?  If there are two
        //separate symbol tables being used here then that is obviously a point that needs to be documented.
        //ADDRESSED
{
    mPrePopInfo->uniquify();
    mSTBuildImpl = new VerificSTBuildWalkerImpl(symtabs);
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    //this for debug purposes will remove soon
    std::cerr << "------> PRE POP INFO <-------" << std::endl;
    mPrePopInfo->print();
#endif
}

VerificSTBuildWalker::~VerificSTBuildWalker()
{
  delete mSTBuildImpl;
}

void VerificSTBuildWalker::addHierRefNet(VhdlIdDef* node)
{
    StringAtom* netName = mNucleusBuilder->gCache()->intern(node->OrigName());
    mSTBuildImpl->addHierRefNet(netName);
}

void VerificSTBuildWalker::addNet(VhdlIdDef* node)
{
    StringAtom* name = mNucleusBuilder->gCache()->intern(node->OrigName());
    mSTBuildImpl->addNet(name);
}

void VerificSTBuildWalker::createBranchForHierRef()
{
    UtVector<VhdlIdDef*> hierRefIdDefs = mNucleusBuilder->gHierRefIdDefs();
    UInt32 s = hierRefIdDefs.size();
    for (UInt32 i = 0; i < s; ++i) {
        // The parent for package is root module. Save the current stacks and
        // begin at root. NOTE: Nested packages are not legal in VHDL.
        mSTBuildImpl->saveStacksAndBeginAtRoot(mPrePopElabScope);
        mPrePopElabScope = 
            mPrePopInfo->findElabScope(NULL, mSTBuildImpl->getUnelabScope()->strObject());
        // Push the library declaration/elaborated scope first. The library
        // name for declaration and elaborated scope are the same.
        UtString libName(hierRefIdDefs[i]->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit()->Owner()->Name());
        pushElabAndUnelabScopes(libName.c_str(), VerificSTBuildWalkerImpl::eNamedDecl);
        UtString packName(hierRefIdDefs[i]->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit()->Name());
        // Package declaration scope.
        pushElabAndUnelabScopes(packName.c_str(), VerificSTBuildWalkerImpl::eNamedDecl);


        addHierRefNet(hierRefIdDefs[i]);

        // Backup from package and library.
        popElabAndUnelabScopes();
        popElabAndUnelabScopes();
        mPrePopElabScope = mSTBuildImpl->restoreStacks();
    }
}

void VerificSTBuildWalker::VHDL_VISIT(VhdlEntityDecl, node)
{
    Verific::VhdlTreeNode* item;
    MapIter mi;
    FOREACH_VHDL_SECONDARY_UNIT(&node, mi,item) {
        VhdlSecondaryUnit* secUnit = static_cast<VhdlSecondaryUnit*>(item);
        INFO_ASSERT(secUnit, "invalid secondary unit node");
        VhdlLibrary* lib = secUnit->GetOwningLib();
        INFO_ASSERT(lib, "invalid lib node");
        UtString entityName(node.Id()->Name());

        // string::find_last_of("_default") crashes if the name is an escaped identifier e.g
        // See testcase beacon_vhdl_93/extd_identifier3 where the entity name is \\005a\\.
        // string::find_last_of("_default") returns 4 which is != string::npos and
        // therefore we tried to get entityName.substr(0, entityName.size()-8) which for
        // \\005a\\ goes into memory before the first character. We avoid this problem by
        // checking the length first and then getting the the last 8 characters and checking
        // if that substring is "_default"
 
        if (entityName.size() > 8) {
          if (VerificNucleusBuilder::hasSpecialCharacter(entityName)) {
            UtString tail = entityName.substr(entityName.size()-8, entityName.size()-1);
            if (tail == "_default") {
              entityName = entityName.substr(0, entityName.size() - 8);
            }
          } else {
            size_t pos = entityName.find_last_of("_default");
            if (UtString::npos != pos) {
              entityName = entityName.substr(0, entityName.size() - 8);
            }
          }
        }

        UtString archName(secUnit->Name());
        UtString libName(lib->Name());
        UtString moduleName;
        if (mNucleusBuilder->IsTopModule(entityName)) {
            moduleName = entityName;
        } else {
            moduleName = libName + UtString("_") +  entityName + UtString("_") + archName;
        }
        StringAtom* name = mNucleusBuilder->gCache()->intern(moduleName.c_str());
        mSTBuildImpl->pushUnelabScope(name, VerificSTBuildWalkerImpl::eModule);
        if (NULL == mPrePopElabScope) {
            // For root module, elab/unelab names are same and added by symbol tables
            // automatically. We only need to setup the elaborated scope branch node.
            mPrePopElabScope = mPrePopInfo->findElabScope(NULL, name);
            mSTBuildImpl->findNode(name, IODBDesignDataSymTabs::eElab);
        }
        unsigned i ;
        Verific::VhdlTreeNode* elem ;
        FOREACH_ARRAY_ITEM(node.GetPortClause(), i, elem) {
            elem->Accept(*this);
        }       
        i = 0;
        elem = 0;
        FOREACH_ARRAY_ITEM(node.GetDeclPart(), i, elem) {
            elem->Accept(*this);
        }
        item->Accept(*this);

        //ckeck package declaration
        MapIter mi;
        VhdlPrimaryUnit* primUnit;
        FOREACH_VHDL_PRIMARY_UNIT(lib, mi, primUnit) {
            if (ID_VHDLPACKAGEDECL == primUnit->GetClassId()) {
              //check for std library FIXME
              { //pre
                  // The parent for package is root module. Save the current stacks and
                  // begin at root. NOTE: Nested packages are not legal in VHDL.
                  mSTBuildImpl->saveStacksAndBeginAtRoot(mPrePopElabScope);
                  mPrePopElabScope = 
                      mPrePopInfo->findElabScope(NULL, mSTBuildImpl->getUnelabScope()->strObject());
                  // Push the library declaration/elaborated scope first. The library
                  // name for declaration and elaborated scope are the same.
                  pushElabAndUnelabScopes(libName.c_str(), VerificSTBuildWalkerImpl::eNamedDecl);
                  // Package declaration scope.
                  UtString packName(primUnit->Name());
                  pushElabAndUnelabScopes(packName.c_str(), VerificSTBuildWalkerImpl::eNamedDecl);
              }
              {//post
                  // Backup from package and library.
                  popElabAndUnelabScopes();
                  popElabAndUnelabScopes();
                  mPrePopElabScope = mSTBuildImpl->restoreStacks();
              }
            }
        }
        //process hierRef nets
        createBranchForHierRef();
        mSTBuildImpl->popUnelabScope();
    }
}

void VerificSTBuildWalker::VHDL_VISIT(VhdlSignalDecl, node)
{
    unsigned i;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetIds(),i,item) {
        VhdlIdDef* idDef = static_cast<VhdlIdDef*>(item);
        INFO_ASSERT(idDef, "invlaid VhdlIdDef node");
        addNet(idDef);
    }
}

void VerificSTBuildWalker::VHDL_VISIT(VhdlAliasDecl, node)
{
    addNet(node.GetDesignator());
}

void VerificSTBuildWalker::VHDL_VISIT(VhdlVariableDecl, node)
{
    unsigned i;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetIds(),i,item) {
        //node.Getids() gets array of VhdlIdDef
        VhdlIdDef* idDef = static_cast<VhdlIdDef*>(item);
        INFO_ASSERT(idDef, "invlaid VhdlIdDef node");
        addNet(idDef);
    }
}

void VerificSTBuildWalker::VHDL_VISIT(VhdlConstantDecl, node)
{
    unsigned i;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetIds(),i,item) {
        //node.Getids() gets array of VhdlIdDef
        VhdlIdDef* idDef = static_cast<VhdlIdDef*>(item);
        INFO_ASSERT(idDef, "invlaid VhdlIdDef node");
        addNet(idDef);
    }
}

void VerificSTBuildWalker::VHDL_VISIT(VhdlFileDecl, node)
{
    unsigned i;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetIds(),i,item) {
        //node.Getids() gets array of VhdlIdDef
        VhdlIdDef* idDef = static_cast<VhdlIdDef*>(item);
        INFO_ASSERT(idDef, "invlaid VhdlIdDef node");
        addNet(idDef);
    }
}

void VerificSTBuildWalker::VHDL_VISIT(VhdlInterfaceDecl, node)
{
    unsigned i;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetIds(),i,item) {
        //node.Getids() gets array of VhdlIdDef
        VhdlIdDef* idDef = static_cast<VhdlIdDef*>(item);
        INFO_ASSERT(idDef, "invlaid VhdlIdDef node");
        addNet(idDef);
    }
}

void VerificSTBuildWalker::VHDL_VISIT(VhdlArchitectureBody, node)
{
    unsigned i = 0;
    VhdlTreeNode *elem = 0 ;
    FOREACH_ARRAY_ITEM(node.GetDeclPart(), i, elem) {
        if (elem) {
            elem->Accept(*this);
        }
    }
    
    FOREACH_ARRAY_ITEM(node.GetStatementPart(),i,elem) {
        if ( ! elem ) continue;
        elem->Accept(*this);
    }
}

void VerificSTBuildWalker::VHDL_VISIT(VhdlProcessStatement, node)
{
    StringAtom* proc_name = mNucleusBuilder->gCache()->intern("named_decl_lb");
    mSTBuildImpl->pushUnelabScope(proc_name, VerificSTBuildWalkerImpl::eNamedDecl);
    pushElabScope(proc_name);
    unsigned j = 0;
    VhdlTreeNode* elem = 0;
    FOREACH_ARRAY_ITEM(node.GetDeclPart(), j, elem) {
        elem->Accept(*this);
    }
    unsigned i = 0;
    VhdlTreeNode* item = 0;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, item) {
        item->Accept(*this);
    }
    popElabScope();
    mSTBuildImpl->popUnelabScope();
}

void VerificSTBuildWalker::VHDL_VISIT(VhdlBlockStatement, node)
{
    if (node.GetLabel() && !Verific2NucleusUtilities::isElabCreatedEmpty(&node)) {
        StringAtom* blockName = NULL;
        if (node.GetLabel()->Name()) {
            blockName = mNucleusBuilder->gCache()->intern(node.GetLabel()->Name());
        } else {
            // get generated one from name registry
            blockName = findName(&node); 
        }
        pushElabScope(blockName->str());
        unsigned j = 0;
        VhdlTreeNode* elem = 0;
        FOREACH_ARRAY_ITEM(node.GetDeclPart(), j, elem) {
            if (elem) {
                elem->Accept(*this);
            }
        }
        unsigned i = 0;
        VhdlTreeNode* item = 0;
        FOREACH_ARRAY_ITEM(node.GetStatements(), i, item) {
            item->Accept(*this);
        }
        popElabScope();
    }
}

// TODO: This algorithm for finding the instantiated component seems incorrect. Consider using Verific2NucleusUtilities::getInstantiatedEntityOrModule() instead.
void VerificSTBuildWalker::VHDL_VISIT(VhdlComponentInstantiationStatement, node)
{
    const char* label = node.GetLabel()->Name();
    pushElabScope(label);
    MapIter mi;
    VhdlLibrary* lib;
    FOREACH_VHDL_LIBRARY(mi, lib) {
        UInt32 s = lib->NumOfPrimUnits();
        for (UInt32 i = 0; i < s; ++i) {
            if (UtString(lib->PrimUnitByIndex(i)->Name()) == UtString(node.GetInstantiatedUnit()->Name())) {
                lib->PrimUnitByIndex(i)->Accept(*this);
                break;
            }
        }
    }
    popElabScope();
}

void VerificSTBuildWalker::pushElabScope(const char* scope)
{
  StringAtom* scopeAtom = mNucleusBuilder->gCache()->intern(scope);
  pushElabScope(scopeAtom);
}

void VerificSTBuildWalker::pushElabScope(StringAtom* scopeAtom)
{
  mPrePopElabScope = 
      mPrePopInfo->findElabScope(mPrePopElabScope, scopeAtom);
  mSTBuildImpl->pushElabScope(mPrePopElabScope->strObject());
}

void VerificSTBuildWalker::popElabScope()
{
  mSTBuildImpl->popElabScope();
  mPrePopElabScope = mPrePopElabScope->getParent();
}

void 
VerificSTBuildWalker::pushElabAndUnelabScopes(const char* scope, SInt32 scopeType)
{
    StringAtom* scopeAtom = mNucleusBuilder->gCache()->intern(scope);
    mSTBuildImpl->pushUnelabScope(scopeAtom, static_cast<VerificSTBuildWalkerImpl::UnelabScopeT>(scopeType));
    pushElabScope(scopeAtom);
}

void VerificSTBuildWalker::popElabAndUnelabScopes()
{
  popElabScope();
  mSTBuildImpl->popUnelabScope();
}

StringAtom*
VerificSTBuildWalker::findName(VhdlTreeNode* verificObject)
{
    if (mNameRegistry->find(verificObject) != mNameRegistry->end()) {
        return mNameRegistry->at(verificObject);
    }
    return 0;
}

}
