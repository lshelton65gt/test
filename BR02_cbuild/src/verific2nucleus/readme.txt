VerificDesignWalker.hpp -- design walker header
VerificVhdlDesignWalker.cpp -- vhdl to nucleus translation (Andy and Edvard)
VerificVerilogDesignWalker.cpp -- verilog to nucleus translation (Frunze)
VerificDesignManager.cpp -- Interface to control verific elaboration

Entrance to this code is via the CarbonContext.cxx


Iteration 1: Jan 14 - Jan 25th
------------------------------
Define small test case
Walker for module, state, wiring, simple register (1 bit)
Update nucleus database pretty printer

Get Verilog/Vhdl sample to go through (sample1.v, sample1.vhd)
Generate nucleus data base for both and compare with original flow.
