// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "verific2nucleus/VerificDesignWalker.h"
#include "verific2nucleus/VerificDesignWalkerCB.h"

// #define DEBUG_PRETTY_PRINT

#ifdef DEBUG_PRETTY_PRINT
#include <iostream>

#define TRACE_DESIGNWALKER_CB(CLASSNAME,NODE,PHASE) \
    std::cerr << CLASSNAME << " CB Called" << std::endl;\
    NODE.PrettyPrint(std::cerr, 0);\
    std::cerr << std::endl; \
    return NORMAL;

#else
// empty macro
#define TRACE_DESIGNWALKER_CB(CLASSNAME,NODE,PHASE) \
    return NORMAL;
#endif

//Verilog headers
#include "VeriTreeNode.h"
#include "VeriModuleItem.h"
#include "VeriModule.h"
#include "VeriMisc.h"
#include "VeriStatement.h"
#include "VeriExpression.h"
#include "VeriConstVal.h"
#include "VeriId.h"
#include "VeriLibrary.h"

//Vhdl headers
#include "VhdlTreeNode.h"
#include "VhdlStatement.h"
#include "VhdlDeclaration.h"
#include "VhdlIdDef.h"
#include "VhdlUnits.h"
#include "VhdlConfiguration.h"
#include "VhdlName.h"
#include "VhdlMisc.h"
#include "VhdlSpecification.h"

#ifndef CARBON_UNUSED
#define CARBON_UNUSED(x) (void)x; 
#endif // CARBON_UNUSED

namespace Verific {

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriModule, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriModule",node, phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTreeNode, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTreeNode",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCommentNode, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriCommentnode",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriPrimitive, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriPrimitive",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriConfiguration, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriConfiguration",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriExpression, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriExpression",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriName, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriName",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriIdRef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriIdRef",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriIndexedId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriIndexedId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSelectedName, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriSelectedName",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriIndexedMemoryId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriIndexedMemoryId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriConcat, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriConcat",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriMultiConcat, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriMultiConcat",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriFunctionCall, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriFunctionCall",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSystemFunctionCall, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriSystemFunctionCall",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriMinTypMaxExpr, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriMinTypMaxExpr",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriUnaryOperator, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriUnaryOperator",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriBinaryOperator, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriBinaryOperator",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriQuestionColon, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriQuestionColon",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriEventExpression, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriEventExpression",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriPortConnect, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriPortConnect",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriPortOpen, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriPortOpen",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriAnsiPortDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriAnsiPortDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTimingCheckEvent, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTimingCheckEvent",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDataType, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDataType",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriNetDataType, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriNetDataType",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriPathPulseVal, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriPathPulseVal",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriIdDef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriIdDef",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriVariable, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriVariable",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriInstId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriInstId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriModuleId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriModuleId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriUdpId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriUdpId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriConfigurationId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriConfigurationId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTaskId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTaskId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriFunctionId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriFunctionId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriGenVarId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriGenVarId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriParamId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriParamId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriBlockId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriBlockId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriRange, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriRange",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriStrength, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriStrength",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriNetRegAssign, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriNetRegAssign",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDefParamAssign, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDefParamAssign",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCaseItem, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriCaseItem",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriGenerateCaseItem, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriGenerateCaseItem",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriPath, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriPath",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDelayOrEventControl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDelayOrEventControl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriConfigRule, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriConfigRule",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriInstanceConfig, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriInstanceConfig",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCellConfig, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriCellConfig",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDefaultConfig, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDefaultConfig",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriUseClause, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriUseClause",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriModuleItem, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriModuleItem",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDataDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDataDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriNetDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriNetDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriFunctionDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriFunctionDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTaskDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTaskDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDefParam, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDefParam",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriContinuousAssign, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriContinuousAssign",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriGateInstantiation, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriGateInstantiation",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriModuleInstantiation, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriModuleInstantiation",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSpecifyBlock, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriSpecifyBlock",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriPathDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriPathDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSystemTimingCheck, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriSystemTimingCheck",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriInitialConstruct, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriInitialConstruct",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriAlwaysConstruct, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriAlwaysConstruct",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriGenerateConstruct, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriGenerateConstruct",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriGenerateConditional, node, phase)
{
  CARBON_UNUSED(node) 
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriGenerateConditional",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriGenerateCase, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriGenerateCase",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriGenerateFor, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriGenerateFor",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriGenerateBlock, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriGenerateBlock",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTable, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTable",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriPulseControl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriPulseControl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriStatement",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriBlockingAssign, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriBlockingAssign",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriNonBlockingAssign, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriNonBlockingAssign",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriGenVarAssign, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriGenVarAssign",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriAssign, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriAssign",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDeAssign, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDeAssign",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriForce, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriForce",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriRelease, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriRelease",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTaskEnable, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTaskEnable",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSystemTaskEnable, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriSystemTaskEnable",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDelayControlStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDelayControlStatement",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriEventControlStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriEventControlStatement",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriConditionalStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriConditionalStatement",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCaseStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriCaseStatement",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriLoop, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriLoop",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriForever, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriForever",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriRepeat, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriRepeat",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriWhile, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriWhile",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriFor, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriFor",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriWait, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriWait",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDisable, node, phase)
{
  CARBON_UNUSED(node) 
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDisable",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriEventTrigger, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriEventTrigger",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSeqBlock, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriSeqBlock",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriParBlock, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriParBlock",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriConst, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriConst",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriConstVal, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriConstVal",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriIntVal, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriIntVal",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriRealVal, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriRealVal",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriLibraryDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriLibraryDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriInterface, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriInterface",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriProgram, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriProgram",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriClass, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriClass",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriPropertyDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriPropertyDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSequenceDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriSequenceDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriModport, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriModport",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriModportDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriModportDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriClockingDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriClockingDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriConstraintDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriConstraintDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriBindDirective, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriBindDirective",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriPackage, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriPackage",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriChecker, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriChecker",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSequentialInstantiation, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriSequentialInstantiation",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTypeRef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTypeRef",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriKeyword, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriKeyword",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriStructUnion, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriStructUnion",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriEnum, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriEnum",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDotStar, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDotStar",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriIfOperator, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriIfOperator",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSequenceConcat, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriSequenceConcat",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriClockedSequence, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriClockedSequence",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriAssignInSequence, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriAssignInSequence",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDistOperator, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDistOperator",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSolveBefore, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriSolveBefore",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCast, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriCast",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriNew, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriNew",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriConcatItem, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriConcatItem",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriAssertion, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriAssertion",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriJumpStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriJumpStatement",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDoWhile, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDoWhile",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriInterfaceId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriInterfaceId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriProgramId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriProgramId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCheckerId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriCheckerId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTypeId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTypeId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriModportId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriModportId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriNull, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriNull",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDollar, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDollar",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriAssignmentPattern, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriAssignmentPattern",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriMultiAssignmentPattern, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriMultiAssignmentPattern",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTimeLiteral, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTimeLiteral",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriOperatorId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriOperatorId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriOperatorBinding, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriOperatorBinding",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriForeachOperator, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriForeachOperator",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriStreamingConcat, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriStreamingConcat",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriNetAlias, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriNetAlias",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTimeUnit, node, phase)
{
  CARBON_UNUSED(node) 
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTimeUnit",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriForeach, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriForeach",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriWaitOrder, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriWaitOrder",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCondPredicate, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriCondPredicate",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDotName, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDotName",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTaggedUnion, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTaggedUnion",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriClockingId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriClockingId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriClockingDirection, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriClockingDirection",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriClockingSigDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriClockingSigDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriRandsequence, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriRandsequence",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCodeBlock, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriCodeBlock",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriProduction, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriProduction",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriProductionItem, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriProductionItem",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriProductionId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriProductionId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriWith, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriWith",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriWithExpr, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriWithExpr",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriInlineConstraint, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriInlineConstraint",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriWithStmt, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriWithStmt",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriArrayMethodCall, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriArrayMethodCall",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriInlineConstraintStmt, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriInlineConstraintStmt",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCovergroup, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriCovergroup",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCoverageOption, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriCoverageOption",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCoverageSpec, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriCoverageSpec",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriBinDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriBinDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriBinValue, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriBinValue",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriOpenRangeBinValue, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriOpenRangeBinValue",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTransBinValue, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTransBinValue",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDefaultBinValue, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDefaultBinValue",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSelectBinValue, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriSelectBinValue",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTransSet, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTransSet",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTransRangeList, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTransRangeList",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSelectCondition, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriSelectCondition",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriTypeOperator, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriTypeOperator",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriImportDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriImportDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriLetDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriLetDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDefaultDisableIff, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriDefaultDisableIff",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriExportDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriExportDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriScopeName, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriScopeName",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriNamedPort, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriNamedPort",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriPatternMatch, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriPatternMatch",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriConstraintSet, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VeriConstraintSet",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCovgOptionId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VeriCovgOptionId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriSeqPropertyFormal, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VeriSeqPropertyFormal",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriBinsId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VeriBinsId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriLetId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VeriLetId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCaseOperator, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VeriCaseOperator",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriCaseOperatorItem, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VeriCaseOperatorItem",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriIndexedExpr, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VeriIndexedExpr",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDPIFunctionDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VeriDPIFunctionDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriDPITaskDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VeriDPITaskDecl",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriExternForkjoinTaskId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VeriExternForkjoinTaskId",node,phase);
}

VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriPrototypeId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VeriPrototypeId",node,phase);
}
//
///* ================================================================= */
///*                         VISIT VHDL METHODS                        */
///* ================================================================= */
VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlPrimaryUnit, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlPrimaryUnit",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlTreeNode, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlTreeNode",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlCommentNode, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlCommentNode",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlDesignUnit, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlDesignUnit",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSecondaryUnit, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSecondaryUnit",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlEntityDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlEntityDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlConfigurationDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlConfigurationDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlPackageDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlPackageDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlContextDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlContextDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlPackageInstantiationDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlPackageInstantiationDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlArchitectureBody, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlArchitectureBody",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlPackageBody, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlPackageBody",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlConfigurationItem, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlConfigurationItem",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlBlockConfiguration, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlBlockConfiguration",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlComponentConfiguration, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlComponentConfiguration",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlDeclaration, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlDeclaration",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlTypeDef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlTypeDef",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlScalarTypeDef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlScalarTypeDef",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlArrayTypeDef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlArrayTypeDef",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlRecordTypeDef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlRecordTypeDef",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlProtectedTypeDef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlProtectedTypeDef",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlProtectedTypeDefBody, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlProtectedTypeDefBody",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAccessTypeDef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlAccessTypeDef",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlFileTypeDef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlFileTypeDef",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlEnumerationTypeDef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlEnumerationTypeDef",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlPhysicalTypeDef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlPhysicalTypeDef",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlElementDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlElementDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlPhysicalUnitDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlPhysicalUnitDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlUseClause, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlUseClause",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlContextReference, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlContextReference",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlLibraryClause, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlLibraryClause",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlInterfaceDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlInterfaceDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSubprogramDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSubprogramDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSubprogramBody, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSubprogramBody",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSubtypeDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSubtypeDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlFullTypeDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlFullTypeDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlIncompleteTypeDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlIncompleteTypeDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlConstantDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlConstantDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSignalDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSignalDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlVariableDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlVariableDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlFileDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlFileDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAliasDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlAliasDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlComponentDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlComponentDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAttributeDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlAttributeDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAttributeSpec, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlAttributeSpec",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlConfigurationSpec, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlConfigurationSpec",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlDisconnectionSpec, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlDisconnectionSpec",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlGroupTemplateDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlGroupTemplateDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlGroupDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlGroupDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSubprogInstantiationDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSubprogInstantiationDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlInterfaceTypeDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlInterfaceTypeDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlInterfaceSubprogDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlInterfaceSubprogDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlInterfacePackageDecl, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlInterfacePackageDecl",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlExpression, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlExpression",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlDiscreteRange, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlDiscreteRange",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSubtypeIndication, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSubtypeIndication",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlExplicitSubtypeIndication, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlExplicitSubtypeIndication",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlRange, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlRange",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlBox, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlBox",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAssocElement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlAssocElement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlInertialElement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlInertialElement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlOperator, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlOperator",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAllocator, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlAllocator",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAggregate, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlAggregate",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlQualifiedExpression, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlQualifiedExpression",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlElementAssoc, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlElementAssoc",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlRecResFunctionElement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlRecResFunctionElement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlWaveformElement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlWaveformElement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlDefault, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlDefault",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlIdDef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlIdDef",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlLibraryId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlLibraryId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlGroupId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlGroupId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlGroupTemplateId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlGroupTemplateId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAttributeId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlAttributeId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlComponentId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlComponentId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAliasId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlAliasId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlFileId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlFileId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlVariableId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlVariableId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSignalId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSignalId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlConstantId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlConstantId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlTypeId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlTypeId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlGenericTypeId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlGenericTypeId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlUniversalInteger, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlUniversalInteger",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlUniversalReal, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlUniversalReal",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAnonymousType, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlAnonymousType",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSubtypeId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSubtypeId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSubprogramId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSubprogramId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlOperatorId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlOperatorId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlInterfaceId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlInterfaceId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlEnumerationId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlEnumerationId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlElementId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlElementId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlPhysicalUnitId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlPhysicalUnitId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlEntityId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlEntityId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlArchitectureId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlArchitectureId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlConfigurationId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlConfigurationId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlPackageId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlPackageId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlPackageBodyId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlPackageBodyId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlContextId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlContextId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlLabelId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlLabelId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlBlockId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlBlockId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlProtectedTypeId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlProtectedTypeId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlProtectedTypeBodyId, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlProtectedTypeBodyId",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlPackageInstElement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlPackageInstElement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSignature, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSignature",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlFileOpenInfo, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlFileOpenInfo",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlBindingIndication, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlBindingIndication",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlIterScheme, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlIterScheme",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlWhileScheme, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlWhileScheme",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlForScheme, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlForScheme",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlIfScheme, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlIfScheme",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlElsifElseScheme, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlElsifElseScheme",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlCaseItemScheme, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlCaseItemScheme",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlDelayMechanism, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlDelayMechanism",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlTransport, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlTransport",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlInertialDelay, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlInertialDelay",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlOptions, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlOptions",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlConditionalWaveform, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlConditionalWaveform",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSelectedWaveform, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSelectedWaveform",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlConditionalExpression, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlConditionalExpression",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSelectedExpression, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSelectedExpression",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlBlockGenerics, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlBlockGenerics",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlBlockPorts, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlBlockPorts",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlCaseStatementAlternative, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlCaseStatementAlternative",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlElsif, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlElsif",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlEntityClassEntry, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlEntityClassEntry",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlName, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlName",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlDesignator, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlDesignator",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlArrayResFunction, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlArrayResFunction",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlRecordResFunction, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlRecordResFunction",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlLiteral, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlLiteral",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAll, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlAll",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlOthers, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlOthers",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlUnaffected, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlUnaffected",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlInteger, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlInteger",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlReal, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlReal",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlStringLiteral, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlStringLiteral",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlBitStringLiteral, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlBitStringLiteral",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlCharacterLiteral, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlCharacterLiteral",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlPhysicalLiteral, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlPhysicalLiteral",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlNull, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlNull",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlIdRef, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlIdRef",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSelectedName, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSelectedName",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlIndexedName, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlIndexedName",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSignaturedName, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSignaturedName",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAttributeName, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlAttributeName",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlOpen, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlOpen",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlEntityAspect, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlEntityAspect",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlInstantiatedUnit, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlInstantiatedUnit",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlExternalName, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlExternalName",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSpecification, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSpecification",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlProcedureSpec, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlProcedureSpec",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlFunctionSpec, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlFunctionSpec",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlComponentSpec, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlComponentSpec",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlGuardedSignalSpec, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlGuardedSignalSpec",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlEntitySpec, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlEntitySpec",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlNullStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlNullStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlReturnStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlReturnStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlExitStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlExitStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlNextStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlNextStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlLoopStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlLoopStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlCaseStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlCaseStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlIfStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlIfStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlVariableAssignmentStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlVariableAssignmentStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSignalAssignmentStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSignalAssignmentStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlForceAssignmentStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlForceAssignmentStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlReleaseAssignmentStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlReleaseAssignmentStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlWaitStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlWaitStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlReportStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlReportStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlAssertionStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VhdlAssertionStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlProcessStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VhdlProcessStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlBlockStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VhdlBlockStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlGenerateStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VhdlGenerateStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlProcedureCallStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlProcedureCallStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSelectedSignalAssignment, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSelectedSignalAssignment",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlConditionalSignalAssignment, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlConditionalSignalAssignment",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlConditionalForceAssignmentStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlConditionalForceAssignmentStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlSelectedVariableAssignmentStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlSelectedVariableAssignmentStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlConditionalVariableAssignmentStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)

  TRACE_DESIGNWALKER_CB("VhdlConditionalVariableAssignmentStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlComponentInstantiationStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VhdlComponentInstantiationStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlIfElsifGenerateStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VhdlIfElsifGenerateStatement",node,phase);
}

VhdlDesignWalkerCB::Status VhdlDesignWalkerCB::VHDL_WALK(VhdlCaseGenerateStatement, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VhdlCaseGenerateStatement",node,phase);
}

#ifdef VERILOG_PATHPULSE_PORTS
VerilogDesignWalkerCB::Status VerilogDesignWalkerCB::VERI_WALK(VeriPathPulseValPorts, node, phase)
{
  CARBON_UNUSED(node)
  CARBON_UNUSED(phase)
  TRACE_DESIGNWALKER_CB("VeriPathPulseValPorts",node,phase);
}
#endif

} // namespace Verific
