// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// project headers
#include "verific2nucleus/CarbonConstraintManager.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"

// verific headers
#include "VeriVisitor.h"    // Visitor base class definition
#include "VhdlVisitor.h"    // Visitor base class definition
#include "VhdlScope.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "VhdlTreeNode.h"
#include "VeriTreeNode.h"
#include "VhdlExpression.h"
#include "VhdlName.h"
#include "VhdlCopy.h"
#include "VhdlDeclaration.h"
#include "VhdlValue_Elab.h"
#include "Array.h"
#include "VhdlSpecification.h"
#include "vhdl_tokens.h"

// other project headers
#include "util/UtPair.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"

// standard headers
#include <sstream>

// Set this to '1' to emit debugging messages
#define DEBUG_CARBON_CSM 0

namespace verific2nucleus {

using namespace Verific;

CarbonConstraintManager::CarbonConstraintManager(void) 
{
}

CarbonConstraintManager::~CarbonConstraintManager()
{
  // Free all the constraints
  UtMap<UtString, VhdlConstraint*>::iterator it = mFunctionReturnConstraints.begin();
  for (; it != mFunctionReturnConstraints.end(); ++it) {
    VhdlConstraint *constraint = it->second;
    delete constraint;
  }
  mFunctionReturnConstraints.clear();
  mSubprogramBodies.clear();
}

// Given a signature, lookup the return constraint. Return NULL if none found.
VhdlConstraint* 
CarbonConstraintManager::lookupFunctionReturnConstraint(UtString sig)
{
  // Look up the constraint
  VhdlConstraint *constraint = NULL;
  UtMap<UtString, VhdlConstraint*>::iterator i = mFunctionReturnConstraints.find(sig);
  if (i != mFunctionReturnConstraints.end()) {
    constraint = i->second;
  }
  
  return constraint;
}

// Given a signature, lookup the uniquified subprogram body
VhdlSubprogramBody* 
CarbonConstraintManager::lookupSubprogramBody(UtString sig)
{
  VhdlSubprogramBody* sBody = NULL;
  UtMap<UtString, VhdlSubprogramBody*>::iterator i = mSubprogramBodies.find(sig);
  if (i != mSubprogramBodies.end()) {
    sBody = i->second;
  }
  return sBody;
}

// This method creates a constraint for a Vhdl expression, given the constraint of the target.
// 
// The target constraint is required to be unconstrained. Typically, it will be the unconstrained return
// value of a function, or the constraint on an unconstrained formal argument of a function. It
// contains data type information which is used to formulate the returned expression constraint.
//
// The expression will typically be an argument to a subprogram, or a function return statement
// expression.
//
// The kinds of Vhdl expressions that can be passed to this method are limited to those that can be 
// passed to an unconstrained formal argument (or returned from a function with an unconstrained return
// value). This eliminates a wide class of expressions - notably arithmetic type expressions,
// and relational expressions.
//
// This method assumes that the return constraints on any function calls that appear in the expression
// have already been calculated. It will look up these constraints in a map. (This includes overloaded
// operators.) If a function appears in an expression, and the function return constraint has not been 
// calculated, this function will not be able to compute the constraint for the expression, and will
// return NULL.
//
VhdlConstraint* 
CarbonConstraintManager::createConstraintFromExpression(VhdlExpression* vhdlExpr, VhdlConstraint* targetConstraint, VhdlDataFlow *df)
{
  INFO_ASSERT(targetConstraint, "Unexpected NULL targetConstraint passed to createConstraintFromExpression");
  VhdlConstraint* result = NULL;
  if (vhdlExpr->GetOperatorToken()) {
    // Check for overloaded operator (unary or binary), and lookup it's return constraint.
    // Note: IEEE overloaded operators are treated as primitives.
    if (Verific2NucleusUtilities::isNonIeeeOverloadedOperator(vhdlExpr->GetOperator())) {
      VhdlConstraint* ovloadOpConstraint = getOverloadedOperatorReturnConstraint(vhdlExpr, df);
      result = ovloadOpConstraint ? ovloadOpConstraint->Copy() : NULL;
    } else if (vhdlExpr->GetRightExpression() == NULL) {
      // NULL right expression implies unary operator. 
      // Unary operators are "not", +, -, abs
      // TODO: Are there other builtin unary operators that need to be handled here?
      unsigned op = vhdlExpr->GetOperatorToken();
      INFO_ASSERT(op == VHDL_abs || op == VHDL_not || op == VHDL_MINUS || op == VHDL_PLUS, "Unexpected unary operator");
      result = createConstraintFromExpression(vhdlExpr->GetLeftExpression(), targetConstraint, df);
    } else {
      // Binary operator, get constraint of left, right, then merge based on binary operator rules
      VhdlConstraint* left = createConstraintFromExpression(vhdlExpr->GetLeftExpression(), targetConstraint, df);
      VhdlConstraint* right = createConstraintFromExpression(vhdlExpr->GetRightExpression(), targetConstraint, df);
      // If we couldn't figure out constraints on both sides of the binary expression, then we won't
      // be able to figure out a merged constraint.
      if (left && right && !left->IsUnconstrained() && !right->IsUnconstrained()) {
        result = mergeConstraints(vhdlExpr->GetOperatorToken(), left, right, targetConstraint);
      }
      delete left;
      delete right;
    }
  } else {
    // No operator.
    result = createConstraintFromPrimary(vhdlExpr, targetConstraint, df);
    }
  
  return result;
}

// This method creates a constraint for an expression primary (i.e. no operators). The constraint
// is allocated by this method, and must be freed by the caller.
//
// The target constraint is an input to this method, and is will usually be unconstrained. If it 
// were constrained then there would be no need to call the method, since the constraint is already known.
//
// If the primary is a function call, the constraint is assumed to have already been
// calculated (during call tree processing) and will be looked up in a map. (Exception,
// if it evaluates to a constant, in which case we don't need to consult the map.)
//
// This method performs the following steps:
//
// 1) Check to see if the target constraint is already constrained. If so, return it.
// 
// 2) Evaluate the expression to see if it is a constant. If so, create a constraint based on the constant
//    value and the target constraint. 
//
// 3) Check to see if it's a function call. If so, look up the return constraint in a map.
//
// 4) Call the Verific EvaluateConstraintInternal method to create a constraint. This will
//    handle the majority of cases, e.g. if it's an id, an indexed array element, a slice of
//    an array, etc. NOTE: EvaluateConstraintInternal does not handle function calls, 
//    nor does it handle various types of expressions.
        
VhdlConstraint* 
CarbonConstraintManager::createConstraintFromPrimary(VhdlExpression* vhdlExpr, VhdlConstraint* targetConstraint, VhdlDataFlow* df)
{
  INFO_ASSERT(vhdlExpr, "Unexpected NULL expression passed to createConstraintFromPrimary");
  INFO_ASSERT(targetConstraint, "Unexpected NULL targetConstraint passed to createConstraintFromPrimary");

  VhdlConstraint* result = NULL;
  
  // If target constraint is already constrained, then we're done. Just make a copy of it.
  if (!targetConstraint->IsUnconstrained()) {
    result = targetConstraint->Copy();
  } 

  // If the expression is a constant, form a constraint from it.
  // The constant can be either an integer, a single bit, or an array.
  // There is no need to call this function for expressions that contain
  // real numbers,
  if (!result) {
    VhdlValue *ret_val = vhdlExpr->Evaluate(0, df, 0) ;
    if (ret_val) {
      if (ret_val->IsComposite()) {
        // Handle composite constant (e.g. "0" - '0' doesn't work here for some reason
        result = targetConstraint->Copy();
        result->ConstraintBy(ret_val);
      } else if (ret_val->IsBit()) {
        // Create a 'bit' constraint. 
        result = createBitConstraint(targetConstraint);
      } else if (ret_val->IsIntVal()) {
        // Create an range constraint for the integer. 
        VhdlValue *left = ret_val->Copy();
        VhdlValue *right = ret_val->Copy();
        result = new VhdlRangeConstraint(left, VHDL_to, right) ;
      }
      delete ret_val;
    }
  }
        
  // If this is a function call, retrieve a copy of it's constraint.
  // If it's a type conversion or builtin function, create a constraint for 
  // the return result directly (without looking it up).
  if (!result) {
    VhdlIndexedName* indexedName = dynamic_cast<VhdlIndexedName*>(vhdlExpr);
    if (indexedName) {
      if (indexedName->IsFunctionCall()) {
        unsigned op = Verific2NucleusUtilities::isIEEEPackageBuiltinFunction(indexedName); // Specifies algorithm for creating constraint
        if (op > 0) {
          // Builtin function call, create return constraint directly 
          result = createConstraintFromBuiltinFunction(indexedName, df, op);
        } else {
          // Regular function call, lookup return constraint
          VhdlConstraint* temp = getFunctionCallReturnConstraint(indexedName, df);
          result = temp ? temp->Copy() : NULL;
        }
      } else if (indexedName->IsTypeConversion()) {
        result = createConstraintFromTypeConversion(indexedName, df);
      }
    }
  }
        
  // If we don't have a constraint by now, then let Verific try to figure it out
  if (!result ) {
    // Note that this allocates a constraint. 
    result = vhdlExpr->EvaluateConstraintInternal(df, 0);
  }
  
  return result;
}

// Return a VHDL array constraint, given the index range (range_left to range_right),
// and left and right VhdlValue's for the elementConstraint (these are copied)
VhdlConstraint* 
CarbonConstraintManager::createArrayConstraint(SInt32 range_left, SInt32 range_right, VhdlValue* elem_left, VhdlValue* elem_right)
{
  VhdlArrayConstraint* arrConstraint = NULL;
  
  VhdlInt* left_value_index = new VhdlInt(range_left);
  VhdlInt* right_value_index = new VhdlInt(range_right);
  VhdlValue* left_value_elem = elem_left->Copy();
  VhdlValue* right_value_elem = elem_right->Copy();
  
  VhdlRangeConstraint* index_constraint = NULL;
  if (range_left > range_right)
    index_constraint = new VhdlRangeConstraint(left_value_index, VHDL_downto, right_value_index);
  else
    index_constraint = new VhdlRangeConstraint(left_value_index, VHDL_to, right_value_index);
  VhdlRangeConstraint* element_constraint = new VhdlRangeConstraint(left_value_elem, VHDL_to, right_value_elem);
  arrConstraint = new VhdlArrayConstraint(index_constraint, element_constraint);

  return arrConstraint;
}

// Create a constraint for a single bit of the targetConstraint type
// For example, if the target constraint is "std_logic_vector", this will
// return a "std_logic" constraint.
VhdlConstraint* 
CarbonConstraintManager::createBitConstraint(VhdlConstraint* targetConstraint)
{
  return targetConstraint->ElementConstraint()->Copy();
}

// Merge two constraints based on the rules of the binary operator. The operator has 
// to be a built-in (vs.overloaded) operator.
// targetConstraint will not be constrained; but we expect both left and right
// constraints to be constrained by the time we get here.
// We are handling only operators on std_logic, std_ulogic, bit, std_logic_vector,
// std_ulogic_vector, bit_vector, signed, and unsigned. We do not have to handle
// arithmetic operators, since expressions of arithmetic operators will always
// result in integers, which cannot be unconstrained.
//
// The type of constraint on left and right will depend on the operator.
// For example, 'sll' operator has an integer constraint on the right operand,
// but an array constraint on the left operand.
//
// The returned constraint is allocated by this method, and must be freed by the
// caller.
VhdlConstraint* 
CarbonConstraintManager::mergeConstraints(unsigned oper, VhdlConstraint *left, VhdlConstraint *right, VhdlConstraint* targetConstraint)
{
  VhdlConstraint* exprConstraint = NULL;
  switch(oper) {
  case VHDL_AMPERSAND:
    {
    // Valid constraints here are those on std_ulogic_vectors (or equivalent, eg bit_vector), std_ulogic (or equivalent, e.g. bit)
    INFO_ASSERT(left && !left->IsUnconstrained() && (left->IsRangeConstraint() || left->IsArrayConstraint()), "Unexpected NULL, unconstrained or unsupported constraint type on left operand of concatenation operator");
    INFO_ASSERT(right && !right->IsUnconstrained() && (right->IsRangeConstraint() || right->IsArrayConstraint()), "Unexpected NULL, unconstrained or unsupported constraint type on right operand or concatenation operator");

    // Figure out the combined length of the vector
    UInt32 length = 0;
    if (left->IsArrayConstraint())
      length += left->IndexConstraint()->Length();
    else 
      // must be range constraint (which implies a single bit object)
      length += 1;
    if (right->IsArrayConstraint())
      length += right->IndexConstraint()->Length();
    else
      length += 1;
    
    // Element constraint will be taken from targetConstraint. This defines the set of
    // values each bit could have, and will be different depending on whether this is a 'bit', 
    // std_logic, std_ulogic, etc.
    INFO_ASSERT(targetConstraint->IsArrayConstraint(), "targetConstraint for concatenation was expected to be an array constraint");
    exprConstraint = createArrayConstraint(0, length-1, targetConstraint->ElementConstraint()->Left(), targetConstraint->ElementConstraint()->Right());
    INFO_ASSERT(exprConstraint, "invalid array constraint");
    }
    break;
  case VHDL_or:
  case VHDL_nor:
  case VHDL_and:
  case VHDL_nand:
  case VHDL_xor:
  case VHDL_xnor:
    {
    INFO_ASSERT(left && !left->IsUnconstrained() && (left->IsRangeConstraint() || left->IsArrayConstraint()), "Unexpected NULL, unconstrained or unsupported constraint type on left operand of logical operation");
    // Logic operator: LRM 7.2.1 says both sides must be 1-d arrays of the same size for builtin types 'bit', 'boolean'.
    // Furthermore, the std_logic_1164 package requires both sides to be equal for the overloaded versions of these
    // operators on std_logic_vector, etc. So here, we take the constraint on the left (since it is assumed to be 
    // the same as the right).
    // TODO: Further validation is required. Interra allows different width operands, and will always use
    // the width of the left operand as the result(as we are doing here). Is this correct?
    exprConstraint = left->Copy();
    }
    break;
  case VHDL_PLUS:
  case VHDL_MINUS:
  case VHDL_STAR:
  case VHDL_SLASH:
    {
    INFO_ASSERT(left && !left->IsUnconstrained() && (left->IsRangeConstraint() || left->IsArrayConstraint()), "Unexpected NULL, unconstrained or unsupported constraint type on left operand of arithmetic operation on vector");
    INFO_ASSERT(right && !right->IsUnconstrained() && (right->IsRangeConstraint() || right->IsArrayConstraint()), "Unexpected NULL, unconstrained or unsupported constraint type on right operand of arithmetic operation on vector");
    // Must be an arithmetic operator on bit types, e.g. "vec + 1", "'1' + vec", etc.
    // The rules in syn_arit.vhd and numeric_std.vhd imply that either side can be an integer, or a single bit object.
    // At least one side has to be a vector, and the size of the result is determined by the largest vector.
    // We always return vector with range "max downto 0" (as opposed to "0 to max"). This is built-in to the operator overload definitions
    // in syn_arit.vhd and numeric_std.vhd.
    UInt32 leftLen = 0;
    UInt32 rightLen = 0;
    if (left->IsArrayConstraint())
      leftLen = left->IndexConstraint()->Length();
    if (right->IsArrayConstraint())
      rightLen = right->IndexConstraint()->Length();
    exprConstraint = createArrayConstraint(std::max(leftLen, rightLen) - 1, 0, targetConstraint->ElementConstraint()->Left(), targetConstraint->ElementConstraint()->Right());
    }
    break;
  case VHDL_sll:
  case VHDL_srl:
  case VHDL_sla:
  case VHDL_sra:
  case VHDL_rol:
  case VHDL_ror:
    INFO_ASSERT(left && !left->IsUnconstrained() && (left->IsRangeConstraint() || left->IsArrayConstraint()), "Unexpected NULL, unconstrained or unsupported constraint type on left operand of shift/rotate operator");
    // The shift operators will return a vector equal to the size of the left expression.
    // E.G. myvector sll 3
    exprConstraint = left->Copy();
    break;
  default:
    INFO_ASSERT(false, "Unexpected operator passed to mergeConstraints");
    break;
  }
  
  return exprConstraint;
}

// Add a signature/constraint pair to the map. Caller is responsible for
// freeing the supplied constraint (if it requires freeing).
// Returns true on success, false if there was a constraint conflict.
bool 
CarbonConstraintManager::addSignatureConstraint(UtString signature, VhdlConstraint* constraint)
{
  if (DEBUG_CARBON_CSM) {
    const char* msg = "NULL";
    const char* unconstrained = "";
    const char* image = NULL;
    if (constraint) {
      image = constraint->Image();
      msg = image;
      if (constraint->IsUnconstrained())
        unconstrained = ": (*** UNCONSTRAINED ***)";
    }
    UtIO::cout() << "DEBUG: Adding signature->constraint: " << signature << "-->" << msg << unconstrained;
    delete image;
  }
  
  bool success = true;
  // First, see if it is already there
  UtMap<UtString, VhdlConstraint*>::iterator i = mFunctionReturnConstraints.find(signature);
  if (i != mFunctionReturnConstraints.end()) {
    // If it's already present, verify that the return constraints match. If they don't,
    // then either (1) under certain circumstances, a function is returning two different types,
    // even though the call signatures are the same (this could happen if the return
    // type depends on an argument value), or (2) the signature is not robust enough to distinguish
    // two different sets of calling args/values/constraints. The former case represents
    // RTL that we can't support, the second is a bug in our signature construction.
    VhdlConstraint* existing_constraint = i->second;
    if (!areConstraintsEqual(constraint, existing_constraint))
      success = false;
    if (DEBUG_CARBON_CSM) {
      if (success)
        UtIO::cout() << ": (DUPLICATE) ";
      else
        UtIO::cout() << ": (*** CONFLICT ***) ";
    }
  } else {
    VhdlConstraint* copy = constraint ? constraint->Copy() : NULL;
    mFunctionReturnConstraints.insert(UtPair<UtString, VhdlConstraint*>(signature, copy));
  }
  
  if (DEBUG_CARBON_CSM) {
    UtIO::cout() << UtIO::endl;
  }
  
  return success;
}

// Add an entry to the signature->uniquified subprogram body map. 
// Since we only uniquify a subprogram once for a given signature, this
// should never be called twice for the same signature.
void 
CarbonConstraintManager::addSignatureSubprogramBody(UtString signature, VhdlSubprogramBody* sBody)
{
  // It must not already exist. Can't add a body twice.
  UtMap<UtString, VhdlSubprogramBody*>::iterator i = mSubprogramBodies.find(signature);
  if (i != mSubprogramBodies.end()) {
    INFO_ASSERT(false, "Illegal call to addSignatureSubprogramBody");
  } else {
    mSubprogramBodies.insert(UtPair<UtString, VhdlSubprogramBody*>(signature, sBody));
  }
}


// Determine whether two constraints are equal. For our purposes, they are equal if they
// require the same number of bits to represent. They must also be non-NULL, and constrained.
bool 
CarbonConstraintManager::areConstraintsEqual(VhdlConstraint* c1, VhdlConstraint* c2)
{
  bool equal = true;

  if (!c1 || !c2 || c1->IsUnconstrained() || c2->IsUnconstrained()) {
    equal = false;
  } else {
    equal = (c1->GetSizeForConstraint() == c2->GetSizeForConstraint());
  }
  
  return equal;
}


// This method computes a call signature from an expression with an overloaded operator, and uses it to look up
// the return constraint in the signature->constraint map, returning NULL if not found.
// Note that overloaded operators must be either unary or binary (no tertiary).
VhdlConstraint* 
CarbonConstraintManager::getOverloadedOperatorReturnConstraint(VhdlExpression* overloaded_op, VhdlDataFlow* df)
{
  // Get arguments to overloaded operator  
  Array *args = new Array;
  if (VhdlExpression* left = overloaded_op->GetLeftExpression()) {
    args->Insert(left);
  }
  if (VhdlExpression* right = overloaded_op->GetRightExpression()) {
    args->Insert(right);
  }

  // Get operator signature
  VhdlSubprogramId* sId = dynamic_cast<VhdlSubprogramId*>(overloaded_op->GetOperator());
  UtString sig = getSignature(sId, args, df);
  delete args;
  
  // Look up the constraint
  VhdlConstraint* constraint = lookupFunctionReturnConstraint(sig);
  
  return constraint;
}

// This method computes a call signature for a subprogram (function or procedure) given the indexed named for the
// function call. It uses the signature to look up the return constraint in the signature->constraint map, returning 
// NULL if not found.
VhdlConstraint* 
CarbonConstraintManager::getFunctionCallReturnConstraint(VhdlIndexedName* call_inst, VhdlDataFlow* df)
{
  // Get function arguments (with default values filled in)
  Array *args = new Array;
  Verific2NucleusUtilities::getSubprogramArgs(args, call_inst, true /* get defaults */);
  
  // Get subprogram call signature
  // Guaranteed to be a subprogram call
  VhdlSubprogramId* sId = dynamic_cast<VhdlSubprogramId*>(call_inst->GetId());
  UtString sig = getSignature(sId, args, df);
  delete args;
  
  // Look up the constraint
  VhdlConstraint *constraint = lookupFunctionReturnConstraint(sig);
  
  return constraint;
}

// This method is called when a subprogram is declared (and called) from within another subprogram.
// Given the containing subprogram, construct a signature that consists of the subprogram name,
// and the types and sizes of all of it's formals. The result is then used as a prefix to the
// 'contained' subprogram.
// 
// This is necessary to handle nested functions whose return size depends on the size (or value)
// of an argument to the containing subprogram.
//
// Note that we can obtain constraints and constant values directly from the formal ids, since
// we have already called 'walkSubprogramBody' which places them there.
UtString 
CarbonConstraintManager::getContainingSubprogramSignature(VhdlIdDef* containing_subprog)
{
  INFO_ASSERT(containing_subprog, "Unexpected NULL subprogram id for containing subprogram");
  
  UtString sig;
  
  // First, see if the 'containing_subprog' is itself contained in another subprogram.
  // If so, we need to capture it's signature too.
  VhdlIdDef *parent_subprogram = containing_subprog->GetOwningScope() ? containing_subprog->GetOwningScope()->GetSubprogram() : NULL;
  if (parent_subprogram) {
    sig << "_" << getContainingSubprogramSignature(parent_subprogram);
  }

  // TODO: Find a better way to deuniquify name. 
  UtString deuniquified_name = Verific2NucleusUtilities::getDeuniquifiedName(containing_subprog->Name());
  sig << deuniquified_name;

  // Get the subprogram spec associated with the body (vs that associated with
  // the 'containing_subprog'). walkSubprogramBody places constraint on the
  // subprogram spec associated with the body. They may be different, if
  // the subprogram is declared in a package. In that case, there are two
  // subprogram specs, one with the body, and another with the prototype decl.
  VhdlSpecification* subprog_spec = containing_subprog->Body()->GetSubprogramSpec();

  unsigned i, j;
  VhdlInterfaceDecl* iface = NULL;
  VhdlIdDef* formal_id;
  FOREACH_ARRAY_ITEM(subprog_spec->GetFormalParamList(), i, iface) {
    FOREACH_ARRAY_ITEM(iface->GetIds(), j, formal_id) {
    
      // Get the type name of the formal, and include that in the signature.
      // NOTE: We may want to map the type name onto something shorter.
      sig << "_" << formal_id->Type()->Name();
      
      // Get the constraint for the formal. 
      // Types like TEXT will have NULL constraints. In that case, we assume neither the value nor the
      // size influences widths of objects in the subprogram, and therefore don't influence how the
      // subprogram is populated.
       
      VhdlConstraint* constraint = formal_id->Constraint();
      if (constraint) {
        if (constraint->IsUnconstrained()) {
          sig << "_unconstrained";
        } else {
          // If supplied value is a constant, use
          // that in the signature (surrounded by parens), otherwise, use bitwidth.
          VhdlValue* value = formal_id->Value();
          if (value && value->IsConstant()) {
            const char* image = value->Image();
            sig << "(" << image << ")";
            delete image;
          } else {
            sig << "_" << (UInt64) constraint->GetSizeForConstraint();
          }
          delete value;
        }
      } else { 
        UtIO::cout() << "DEBUG: ERROR: Subprogram " << containing_subprog->Name() << " does not have a constraint for formal argument " << formal_id->Name() << UtIO::endl;
      }
    }
  }

  return sig;
}

// Create a string that uniquely identifies the subprogram and the calling argument types,
// given the subprogram id and that actual argument values/expressions.
//
// This can be used to determine whether we've populated a copy of this subprogram before.
// (If so, we might avoid populating duplicate copies.)
// It can also be used to look up the return type of the function.
//
// Here is the format of the string:
//
// <lib>_<package|entity_arch>_<line number>[_containing subprogram signature]_ (continued)
//         <subprogram name>_rtn_<return type>_<number of args>_<arg1 sig>_<arg2 sig>...
//
// For <argN sig>:
//    - Include the type name of the argument, e.g. integer, 'bit_vector'
//    - If the argument is a constant, we use the value of the
//      constant (see reasoning below). Otherwise, we use the size in bits.
//
// Note about [containing subprogram signature]: This is required for circumstances
// in which a function is declared inside another subprogram, and the function return
// size is a function of the parent subprogram arguments. Without the containing
// subprogram signature, the signature would not be distinct. E.G.
//
// function parent (arg : bit_vector) return bit_vector is
//   constant N : integer := arg'length;
//   -- Signature for 'child' is not distinct without 'parent' signature 
//   function child (pos : integer) return bit_vector is
//      variable result : bit_vector(N-1 downto 0);
//   begin
//       ... statements ...
//      return result;
//   end function child;
//   ...
// end function parent;
// 
// Note about <line number>: This is used to distinguish subprograms with the same name,
// and the same argument types, in the same package or entity arch. It differentiates
// subprograms that are declared in scopes other than packages or architecture declaration
// areas, in particular:
//
//   - block scopes
//   - process scopes
//
// So, for example, there could be two unlabelled processes, each with it's own declaration
// of 'foo', each of which has exactly the same argument and return types. Without the
// line number, the signatures of these two 'foo's would be exactly the same, even though
// they are completely different functions. And, since the process is unlabelled, we
// can't use a process label to distinguish them two different versions of 'foo'.
//
// Note about <return type>: This is used to distinguish overloaded functions whose
// arguments are exactly the same, but have different return types. 
// NOW THAT <line number> IS INCLUDED, IT MIGHT BE POSSIBLE TO ELIMINATE THIS.
//
// Note about constant args: A constant argument can determine the width of
// a returned vector. For that reason, the value of the constant needs to be included
// as part of a signature. 
UtString 
CarbonConstraintManager::getSignature(VhdlSubprogramId* sId, Array* args, VhdlDataFlow *df)
{
  INFO_ASSERT(sId, "Unexpected NULL subprogram id");
  
  if (0) // Debug
  {
    if (sId->GetOwningScope() && sId->GetOwningScope()->GetSubprogram()) 
      UtIO::cout() << "DEBUG: Subprogram " << sId->Name() << " is called from within subprogram " << sId->GetOwningScope()->GetSubprogram()->Name() << UtIO::endl;
  }
  
  UtString sig; // This will contain the signature

  // Get the containing scope. For most objects, it is retrieved by calling GetOwningScope(). But for subprogram/function declarations
  // in a package header (i.e. without a body), it's obtaining by LocalScope().
  VhdlScope* containing_scope = sId->GetOwningScope() ? sId->GetOwningScope() : sId->LocalScope();
  // INFO_ASSERT(containing_scope, "Unexpected NULL containing scope");
  UtString libName(containing_scope->GetContainingPrimaryUnit()->GetPrimaryUnit()->Owner()->Name());
  // If entity, this will be the entity name. If pkg, will be the package name
  UtString unitName(containing_scope->GetContainingPrimaryUnit()->GetPrimaryUnit()->Name());
  sig << libName << "_" << unitName;
  if (containing_scope->GetContainingPrimaryUnit()->IsEntity()) {
    // Grab name of architecture
    UtString archName(containing_scope->GetContainingDesignUnit()->Name());
    sig << "_" << archName;
  }

  // If this subprogram is declared inside another subprogram, then it's signature
  // needs to include the parent subprogram argument types/values. See comments for
  // more details.
  VhdlIdDef *containing_subprogram = sId->GetOwningScope() ? sId->GetOwningScope()->GetSubprogram() : NULL;
  if (containing_subprogram) {
    sig << "_" << getContainingSubprogramSignature(containing_subprogram);
  }
  
  
  UtString funcName(sId->Name());
  sig << "_" << funcName;

  // Include file line number
  sig << "_ln" << sId->Linefile()->GetLeftLine();
  
  VhdlSpecification* subprog_spec = sId->Spec();
  // If there is no subprogram specification, we can't create a signature.
  // In this case, do an early return. For VHDL to parse/elaborate correctly, 
  // there has to be a subprogram specification. However, 'endfile'
  // seems to violate this rule. It was removed from the language, has no
  // RTL subprogram specification, yet implementations (including Verific)
  // still seem to support it.
  if (!subprog_spec) return sig;

  // Include return type (if any) in signature. Overloaded functions can have
  // exactly the same argument/types, but different return types.
  VhdlName* return_type = subprog_spec->GetReturnType();
  if (return_type) {
    sig << "_rtn_" << Verific2NucleusUtilities::verific2String(*return_type);
  }
  
  // TODO: Compute formal array size, and assert that there are the same
  // number of actual and formal args
  unsigned nArgs = args->Size();
  sig << "_" << nArgs;

  // Compute formal constraints (these need to be freed)
  Map formal_to_constraint(POINTER_HASH);
  subprog_spec->InitializeConstraints(formal_to_constraint);
  
  // All actual arguments should have constraints determined by the time we get here,
  // unless the constraints are not determinable at elaboration time.
  // Loop through all formals, construct constraints for each, given the
  // actual argument.
  // WARNING: We have to loop through the subprog_spec formal ids, since those
  // are the ids inserted into the map. We can't loop through the sId->Arg()'s,
  // since these may be different id ptrs if subprograms are declared in packages.
  unsigned i, j;
  unsigned pos = 0;
  VhdlInterfaceDecl* iface = NULL;
  VhdlIdDef* formal_id;
  FOREACH_ARRAY_ITEM(subprog_spec->GetFormalParamList(), i, iface) {
    FOREACH_ARRAY_ITEM(iface->GetIds(), j, formal_id) {
      // Grab actual expression from next pos in supplied arg array
      VhdlExpression* actual_expr = static_cast<VhdlExpression*>(args->At(pos++));
    
      // Get the type name of the formal, and include that in the signature.
      // NOTE: We may want to map the type name onto something shorter.
      sig << "_" << formal_id->Type()->Name();
      
      // Get the constraint for the formal. 
      // Types like TEXT will have NULL constraints. In that case, we assume neither the value nor the
      // size influences widths of objects in the subprogram, and therefore don't influence how the
      // subprogram is populated.
       
      VhdlConstraint* formal_constraint = static_cast<VhdlConstraint*>(formal_to_constraint.GetValue(formal_id));

      if (formal_constraint) {
        
        // Calculate the constraint 
        VhdlConstraint* constraint = NULL;
        if (!formal_constraint->IsUnconstrained()) {
          constraint = formal_constraint->Copy();
        } else { 
          constraint = createConstraintFromExpression(actual_expr, formal_constraint, df);
        }
        if (!constraint || constraint->IsUnconstrained()) {
          sig << "_unconstrained";
        } else {
          // If supplied value is a constant, use
          // that in the signature (surrounded by parens), otherwise, use bitwidth.
          VhdlValue* value = actual_expr->Evaluate(0, df, 0);
          if (value && value->IsConstant()) {
            const char* image = value->Image();
            sig << "(" << image << ")";
            delete image;
          } else {
            sig << "_" << (UInt64) constraint->GetSizeForConstraint();
          }
          delete value;
        }
        
        delete constraint;
      }
    }
  }

  // Free formal constraints
  VhdlConstraint* constraint;
  MapIter mi ;
  FOREACH_MAP_ITEM(&formal_to_constraint, mi, 0, &constraint) delete constraint ;

  return sig;
}

// Wrapper around getSignature collects subprogram arguments, and constructs
// signature.
UtString 
CarbonConstraintManager::getSubprogramSignature(VhdlIndexedName* call_inst, VhdlDataFlow* df)
{
  // This indexed name corresponds to a function or procedure call
  // Get function arguments (with default values filled in)
  Array *args = new Array;
  Verific2NucleusUtilities::getSubprogramArgs(args, call_inst, true /* get defaults */);
  
  // Get subprogram call signature
  // Guaranteed to be a subprogram call
  VhdlSubprogramId* sId = dynamic_cast<VhdlSubprogramId*>(call_inst->GetId());

  UtString sig = getSignature(sId, args, df);
  delete args;

  return sig;
}

// Wrapper around getSignature collects overloaded operator arguments, and constructs
// signature.
UtString 
CarbonConstraintManager::getOverloadedOperatorSignature(VhdlOperator* oper, VhdlDataFlow *df)
{
  Array *args = new Array();
  if (oper->GetLeftExpression()) {
    args->Insert(oper->GetLeftExpression());
  }
  if (oper->GetRightExpression()) {
    args->Insert(oper->GetRightExpression());
  }

  VhdlSubprogramId* sId = dynamic_cast<VhdlSubprogramId*>(oper->GetOperator());
  INFO_ASSERT(sId, "Unexpected NULL subprogram id");
  
  UtString sig = getSignature(sId, args, df);
  delete args;

  return sig;
}

// Create a constraint given the VhdlIndexedName* for a type conversion. For example:
//
//       return std_logic_vector(mybitvec & "00");
//
// where 'std_logic_vector' is the type conversion.
//
// At present, this is a private function, used only to compute constraints of expressions
// in return statements, or as arguments to a function. This may prove useful during
// population of type convesions, though. If so, it needs to be made public.

VhdlConstraint*
CarbonConstraintManager::createConstraintFromTypeConversion(VhdlIndexedName* inst, VhdlDataFlow* df)
{
  INFO_ASSERT(inst->IsTypeConversion(), "Expected a type conversion");
  
  // Get the type conversion id (e.g. get's id for 'unsigned' given 'ieee.numeric_std.unsigned')
  VhdlIdDef* prefix_id = Verific2NucleusUtilities::getConvSubprogramPrefix(*inst);

  // Get typemark constraint 
  VhdlConstraint* target_constraint = NULL;
  VhdlDeclaration* decl = prefix_id->GetDeclaration();
  
  if (decl->GetClassId() == Verific::ID_VHDLSUBTYPEDECL) {
    VhdlSubtypeDecl* subtype_decl = dynamic_cast<VhdlSubtypeDecl*>(decl);
    VhdlSubtypeIndication* subtype_ind = subtype_decl->GetSubtypeIndication();
    INFO_ASSERT(subtype_ind, "Unexpected NULL subtype indication for type converson");
    target_constraint = subtype_ind->EvaluateConstraintInternal(df, 0);
  } else if (decl->GetClassId() == Verific::ID_VHDLFULLTYPEDECL) {
    VhdlFullTypeDecl* type_decl = dynamic_cast<VhdlFullTypeDecl*>(decl);
    VhdlTypeDef* type_def = type_decl->GetTypeDef();
    INFO_ASSERT(type_def, "Unexpected NULL type definition for type conversion");
    target_constraint = type_def->EvaluateConstraintInternal(df, 0);
  } else {
    INFO_ASSERT(false, "Unexpected declaration type for type conversion");
  }
  
  INFO_ASSERT(target_constraint, "Unexpected NULL type mark constraint");
  
  // If the constraint on the typemark is already constrained,
  // then we're done
  VhdlConstraint* resulting_constraint = NULL;
  if (!target_constraint->IsUnconstrained()) {
    resulting_constraint = target_constraint;
  } else {
    // Use the typemark argument/expression to figure out the resulting constraint
    INFO_ASSERT(inst->GetAssocList()->Size() == 1, "Unexpected number of args to type conversion. Expected exactly 1.");
    VhdlExpression* arg = static_cast<VhdlExpression*>(inst->GetAssocList()->At(0));
    INFO_ASSERT(arg, "Unexpected NULL type conversion arg");
    resulting_constraint = createConstraintFromExpression(arg, target_constraint, df);
    delete target_constraint;
  }
  
  return resulting_constraint;
}

// This method creates a constraint for a builtin function.
// 
// Builtin functions are functions in the IEEE library that often have no subprogram body (which
// limits our ability to analyze them), and which we populate inline (rather than generating a
// task call). We have all the knowledge about how to construct the return constraint from
// the arguments to the builtin function.
//
// The 'op' parameter specifies the algorithm for constructing the return constraint. It can take
// on the following values:
//
//   0 - Means 'unspecified', and we have to look up the algorithm from the VhdlIndexedName*
//   1 - Means the return constraint is give directly from the subprogram spec
//   2 - Means the range of the return type is given by the range of the first input
//   3 - Means the range of the return type is given by the value of the second input

VhdlConstraint*
CarbonConstraintManager::createConstraintFromBuiltinFunction(VhdlIndexedName* call_inst, VhdlDataFlow* df, unsigned op)
{
  VhdlConstraint* constraint = NULL;

  // We need to look up the algorithm
  if (0 == op) {
    op = Verific2NucleusUtilities::isIEEEPackageBuiltinFunction(call_inst);
    INFO_ASSERT(op > 0, "Expected indexed name to be a builtin function call");
  } 

  // Get function arguments
  Array *args = new Array;
  Verific2NucleusUtilities::getSubprogramArgs(args, call_inst, false /* no default values */);
  
  // Get the constraint of the return type
  VhdlConstraint* return_constraint = call_inst->GetPrefixId()->Spec()->GetReturnType()->EvaluateConstraintInternal(0,0);
  INFO_ASSERT(return_constraint, "Unexpected NULL return constraint for builtin function");
  // If the return constraint is already constrained, we're done. Otherwise, we
  // need to look at the 'op'
  if (!return_constraint->IsUnconstrained()) {
    constraint = return_constraint;
    
  } else if (2 == op) {
    // Get range of first argument
    INFO_ASSERT(args->Size() > 0, "Expected at least one argument to builtin function");
    VhdlExpression* arg = static_cast<VhdlExpression*>(args->At(0));
    INFO_ASSERT(arg, "Unexpected NULL argument to builtin function");
    VhdlConstraint* arg_constraint = createConstraintFromExpression(arg, return_constraint, df);
    // Use it to constrain return_constraint
    return_constraint->ConstraintBy(arg_constraint);
    delete arg_constraint;
    constraint = return_constraint;

  } else if (3 == op) {
    // Get constraint from value of second argument
    INFO_ASSERT(args->Size() > 1, "Expected at least two arguments to builtin function");
    VhdlExpression* arg = static_cast<VhdlExpression*>(args->At(1));
    VhdlValue* value = arg->Evaluate(0, df, 0);
    if (!value) {
      // We can't evaluate the second argument - possibly because it is not computable at elaboration
      // time. Unfortunately, the constraint will remain unconstrained.
    } else {
      // Constrain the return constraint (is there a better way to do this, without having to create array_constraint? Perhaps directly with value?)
      VhdlConstraint* array_constraint = createArrayConstraint(0, value->Integer()-1, return_constraint->ElementConstraint()->Left(), return_constraint->ElementConstraint()->Right());
      return_constraint->ConstraintBy(array_constraint);
      delete array_constraint;
    }
    constraint = return_constraint;
    delete value;

  } else {
    INFO_ASSERT(false, "Unexpected value of 'op' in in createConstraintFromBuiltinFunction");
  }
  
  delete args;
  
  return constraint;
}

// This method returns a uniquified name for a subprogram. The name will be
// used to create a uniquified copy of the subprogram.
// TODO: This could be extended to check for existence of
// the unique name in the declaration area, before returning.
// In the unlikely event that the name is already being used.
// However, the need for this may be negligable, given that
// uniquified names have '$' in them, which cannot collide with
// a user declared variable. However, if '$' is found (for some reason)
// to cause problems in the Verific parse tree or API, then we'll
// probably need to extend this to look for collisions.
//
// Note, there are related methods in Verific2NucleusUtilities:
//   getDeuniquifiedName()
// and
//   isUniquifiedName()

UtString 
CarbonConstraintManager::getUniquifiedName(VhdlScope *scope, const char* subprogram_name)
{
  // TODO: lookup uniquified name in declaration scope.
  (void)scope;
  UtString name(subprogram_name);
  UInt32 count = 1;
  UtMap<UtString, UInt32>::iterator i = mUniqCopyCount.find(name);
  if (i != mUniqCopyCount.end()) {
    // If the subprogram name already has a map entry, then increment
    // the uniquify count, and use it to construct a new uniquie name
    i->second += 1;
    count = i->second;
  } else {
    // If it doesn't yet exist in the map, then add it with an initial
    // count of 1.
    mUniqCopyCount.insert(UtPair<UtString, UInt32>(name, count));
  }
  
  UtString uniq_name(name);
  uniq_name << CarbonContext::sUniqSubprogramSuffix << count;
  return uniq_name;
}

// Verify that each function signature encountered during the constraint
// elaboration phase has had a uniquified function body created during
// the subprogram uniquification phase.
bool
CarbonConstraintManager::checkConsistency(void)
{
  bool success = true;
  
  UtMap<UtString, VhdlConstraint*>::iterator i;
  for (i = mFunctionReturnConstraints.begin(); i != mFunctionReturnConstraints.end(); ++i) {
    // Look up signature in mSubprogramBodies map, it better be there
    UtString sig = i->first;
    UtMap<UtString, VhdlSubprogramBody*>::iterator j = mSubprogramBodies.find(sig);
    if (j == mSubprogramBodies.end()) {
      success = false;
      if (1) // Debug
        UtIO::cout() << "ERROR: Signature '" << sig << "' was encountered during constraint elaboration, but "
                     << "no uniquified subprogram body was created for it during subprogram uniquification."
                     << UtIO::endl;
    }
  }
  
  return success;
}

} // namespace verific2nucleus
