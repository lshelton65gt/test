// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "verific2nucleus/VerificUserTypePopulate.h"
#include "verific2nucleus/VerificVhdlDesignWalker.h"
#include "verific2nucleus/VerificDesignWalkerCB.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"
#include "iodb/IODBDesignData.h"
#include "iodb/IODBUserTypes.h"
#include "iodb/IODBNucleus.h"
#include "nucleus/NUInstanceWalker.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUScope.h"
#include "util/AtomicCache.h"
#include "util/CbuildMsgContext.h"
#include "VhdlVisitor.h" //Visitor base class definition
#include "VhdlValue_Elab.h"
#include "VhdlSpecification.h"
#include <iostream>

//#define DEBUG_VERIFIC_VHDL_WALKER 1

namespace verific2nucleus {

static StringAtom* sGetNetName(NUNet* net)
{
  StringAtom* netAtom = NULL;
  if (net->isCompositeNet()) { // Use original name of composite net.
    NUCompositeNet* cnet = net->getCompositeNet();
    netAtom = cnet->getOriginalName();
  } else if (net->isHierRef()) {
      netAtom = net->getHierRef()->getLocallyResolvedNet()->getName();
  } else {
    netAtom = net->getName();
  }
  return netAtom;
}

class TypeInfo
{
public:
  TypeInfo() :
      mType(UserType::eScalar),
      mVhdlType(eVhdlTypeUnknown),
      mIsRange(false),
      mCompositeTypeName(NULL),
      mNet(NULL),
      mRangeRequiredInDeclaration(false),
      mLibraryName(NULL),
      mPackageName(NULL)
  {
  }
 
  TypeInfo(UserType::Type type, CarbonVhdlType vhdlType) :
      mType(type),
      mVhdlType(vhdlType),
      mIsRange(false),
      mCompositeTypeName(NULL),
      mNet(NULL),
      mRangeRequiredInDeclaration(false),
      mLibraryName(NULL),
      mPackageName(NULL)
  {
  }

  TypeInfo(UserType::Type type, CarbonVhdlType vhdlType, const char* compositeTypeName,
           UtString *libName = NULL, UtString *packName = NULL) :
      mType(type),
      mVhdlType(vhdlType),
      mIsRange(false),
      mNet(NULL),
      mRangeRequiredInDeclaration(false),
      mLibraryName(NULL),
      mPackageName(NULL)
  {
    if (strlen(compositeTypeName) > 0)
    {
      mCompositeTypeName = new UtString(compositeTypeName);
      mCompositeTypeName->lowercase();
    }
    else
    {
      mCompositeTypeName = NULL;
    }

    if((libName != NULL) && (packName != NULL) &&
       (libName->empty() == false) && (packName->empty() == false))
    {
      mLibraryName = new UtString(libName->c_str());
      mPackageName = new UtString(packName->c_str());
    }
  }

  TypeInfo(const TypeInfo& typeInfo)
  {
    mType = typeInfo.getType();
    mVhdlType = typeInfo.getVhdlType();
    mIsRange = typeInfo.isRange();
    mRange = typeInfo.getRange();
    UtString* compositeTypeName = typeInfo.getCompositeTypeName();
    if (compositeTypeName != NULL)
    {
      mCompositeTypeName = new UtString(*compositeTypeName);
      mCompositeTypeName->lowercase();
    }
    else
    {
      mCompositeTypeName = NULL;
    }

    mLibraryName = NULL;
    mPackageName = NULL;
    UtString* libName = typeInfo.getLibraryName();
    UtString* packName = typeInfo.getPackageName();
    if(libName != NULL)
      mLibraryName = new UtString(libName->c_str());

    if(packName != NULL)
      mPackageName = new UtString(packName->c_str());

    mNet = typeInfo.getNet();
    mEnumIdDefNode = typeInfo.getEnumVhdlIdDefNode();
    mEnumEncodings = typeInfo.getEnumEncodings();
    mRangeRequiredInDeclaration = typeInfo.isRangeRequiredInDeclaration();
  }


  virtual ~TypeInfo() {
    delete mCompositeTypeName;
    delete mLibraryName;
    delete mPackageName;
  }

  void setType(UserType::Type type) {
    mType = type;
  }

  UserType::Type getType() const {
    return mType;
  }

  void setVhdlType(CarbonVhdlType vhdlType) {
    mVhdlType = vhdlType;
  }

  CarbonVhdlType getVhdlType() const {
    return mVhdlType;
  }

  void setRange(ConstantRange range) {
    mRange = range;
    mIsRange = true;
  }

  bool isRange() const {return mIsRange;};

  // Array range or integer constraint.
  ConstantRange getRange() const {
    return mRange;
  }

  UtString* getCompositeTypeName() const {
    return mCompositeTypeName;
  }

  UtString* getLibraryName() const {
    return mLibraryName;
  }

  UtString* getPackageName() const {
    return mPackageName;
  }
  

  NUNet* getNet() const { return mNet; }

  void setNet(NUNet* net) { mNet = net; }

  void setEnumVhdlIdDefNode(VhdlIdDef* idDef) { mEnumIdDefNode = idDef; }
    
  VhdlIdDef* getEnumVhdlIdDefNode() const { return mEnumIdDefNode; }

  void addEnumEncoding(const UtString& enumEncoding) 
  {
    mEnumEncodings.push_back(enumEncoding); 
  }

  const UtStringArray& getEnumEncodings() const { return mEnumEncodings; }

  bool isRangeRequiredInDeclaration() const { return mRangeRequiredInDeclaration; }

  void setIsRangeRequiredInDeclaration(bool rangeRequiredInDeclaration) { mRangeRequiredInDeclaration = rangeRequiredInDeclaration; }

private:
  TypeInfo& operator=(const TypeInfo&);

  UserType::Type         mType;
  CarbonVhdlType   mVhdlType;
  
  bool          mIsRange; // does this type have a range
  ConstantRange mRange;
  UtString*     mCompositeTypeName;
  NUNet*        mNet;       // This is used only for records
  VhdlIdDef*    mEnumIdDefNode; // This is only used for enum
  UtStringArray mEnumEncodings; // Only used if enum has the ENUM_ENCODINGS attribute
  bool          mRangeRequiredInDeclaration;
  UtString*     mLibraryName;
  UtString*     mPackageName;
};

typedef  UtList<TypeInfo> TypeInfoStack;
typedef TypeInfoStack::const_reverse_iterator TypeInfoStackCRIter;
typedef TypeInfoStack::const_iterator TypeInfoStackCIter;

class TypeInfoWalker : public Verific::VhdlVisitor
{
public:

  TypeInfoWalker(UserTypeFactory* typeFact,
	     NUNet* net,
	     TypeInfoStack* tiStack,
	     MsgContext* msgContext,
	     const VerificVhdlDesignWalker::NUNetToConstraintRangeMap* constraintRangeMap)
    : mTypeFactory(typeFact),
      mNet(net),
      mMemDim(0),
      mCompDim(0),
      mTypeInfoStack(tiStack),
      mCurrentNet(net),
      mConstraintRangeMap(constraintRangeMap),
      mMsgContext(msgContext),
      mEnumAttrb(NULL),
      mNeedsRangeInDeclaration(false)
  {
  }

  virtual ~TypeInfoWalker()
  {
  }
    
    void VHDL_VISIT(VhdlExplicitSubtypeIndication ,node) {
        if (node.GetRangeConstraint()) {
            mNeedsRangeInDeclaration = true;
            VhdlConstraint* constraint = node.GetRangeConstraint()->EvaluateConstraint(0, 0);
            if (constraint) {
                mEnumRangeLhs = constraint->Left()->Integer();
                mEnumRangeRhs = constraint->Right()->Integer();
            }
            if (constraint) delete constraint;
        }
    }
    void VHDL_VISIT(VhdlIdDef, node) {
        INFO_ASSERT(node.Type(), "invalid type node");
        if (node.Type()->GetSubtypeIndication()) {
            //for recognizing rnage we have to traverse through declaration and check range existence
            node.Type()->GetSubtypeIndication()->Accept(*this);
        }
        
        bool skip = false;
        if (node.GetSubtypeIndication() || node.IsType()) {
            CarbonVhdlType stdLogicType = eVhdlTypeUnknown;
            UtString baseType;
            if (node.IsType()) {
                baseType = UtString(node.Name());
            } else {
                baseType = UtString(node.Type()->Name());
                                
            }
            if ((!node.IsType() && node.Type() && node.Type()->GetSubtypeIndication() &&
                 node.Type()->GetSubtypeIndication()->GetTypeMark() &&
                 (UtString("std_logic") == UtString(node.Type()->GetSubtypeIndication()->GetTypeMark()->Name()))) ||
                 (node.GetSubtypeIndication() && node.GetSubtypeIndication()->GetTypeMark() &&
                UtString("std_logic") == UtString(node.GetSubtypeIndication()->GetTypeMark()->Name())) ||
                (UtString("std_logic") == baseType)) {
                stdLogicType = eVhdlTypeStdLogic;
                mTypeName = UtString("std_logic");
            } else if (UtString("std_ulogic") == baseType ||
                       UtString("ux01z") == baseType ||
                       UtString("x01") == baseType ||
                       UtString("ux01") == baseType ||
                       UtString("x01z") == baseType) {
                stdLogicType = eVhdlTypeStdULogic;
                mTypeName = UtString("std_ulogic");
            } else if (UtString("bit") == baseType) {
                stdLogicType = eVhdlTypeBit;
                mTypeName = UtString("bit");
            } else if (UtString("boolean") == baseType) {
                stdLogicType = eVhdlTypeBoolean;
                mTypeName = UtString("boolean");
            }
            if (stdLogicType != eVhdlTypeUnknown)
            {
                skip = true;
                TypeInfo ti(UserType::eScalar, stdLogicType, mTypeName.c_str());
                mTypeInfoStack->push_back(ti);
            } else {
                CarbonVhdlType stdLogicVectorType = eVhdlTypeUnknown;
                if(UtString("std_logic_vector") == baseType) {
                    stdLogicVectorType = eVhdlTypeStdLogic;
                } else if(UtString("std_ulogic_vector") == baseType) { 
                    stdLogicVectorType = eVhdlTypeStdULogic;
                } else if(UtString("bit_vector") == baseType) {
                    stdLogicVectorType = eVhdlTypeBit;
                } else if(UtString("string") == baseType) {
                    stdLogicVectorType = eVhdlTypeChar;
                } else if ( UtString("text") == baseType) {
                    stdLogicVectorType = eVhdlTypeChar;
                    if (node.Type()->IsFileType() && mPackageName.empty() && mLibraryName.empty()) {
                        mPackageName = UtString(node.Type()->GetOwningScope()->
                                                GetContainingPrimaryUnit()->GetPrimaryUnit()->Name());
                        mLibraryName = UtString(node.Type()->GetOwningScope()->
                                                GetContainingPrimaryUnit()->GetPrimaryUnit()->Owner()->Name());
                    }
                }
                if (stdLogicVectorType != eVhdlTypeUnknown) {
                    skip = true;
                    TypeInfo tiAr(UserType::eArray, 
                                  stdLogicVectorType, 
                                  (mTypeName.empty()) ? baseType.c_str() : mTypeName.c_str(),
                                  &mLibraryName, &mPackageName);
                    tiAr.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
                    const ConstantRange *range = NULL;
                    if(mCurrentNet->isVectorNet()) {
                        NUVectorNet *vecNet = mCurrentNet->castVectorNet();
                        range = vecNet->getDeclaredRange();
                    } else if(mCurrentNet->isMemoryNet()) {
                        NUMemoryNet *memNet = mCurrentNet->getMemoryNet();
                        if(UtString("string") == baseType)
                            range = memNet->getRange(1);
                        else
                            range = memNet->getRange(0);
                    } else if(mCurrentNet->isBitNet()) {
                        ;
                    } else {
                        // It has to be either vector or memory net
                        NU_ASSERT(false, mNet);
                    }
                    
                    tiAr.setRange(*range);
                    mTypeInfoStack->push_back(tiAr);
                    TypeInfo ti(UserType::eScalar, stdLogicVectorType);
                    mTypeInfoStack->push_back(ti);
                } else {
                    bool intrinsic = false;
                    CarbonVhdlType subType = eVhdlTypeUnknown;
                    if(UtString("integer") == baseType) {
                        subType = eVhdlTypeInteger;
                    } else if(UtString("natural") == baseType) {
                        subType = eVhdlTypeNatural;
                    } else if(UtString("positive") == baseType) {
                        subType = eVhdlTypePositive;
                    } else if(UtString("real") == baseType) {
                        subType = eVhdlTypeReal;
                    } else if(UtString("character") == baseType) {
                        subType = eVhdlTypeChar;
                    } else if (node.Type()->IsFileType()) {
                        //file of integer
                        VhdlFullTypeDecl* decl = dynamic_cast<VhdlFullTypeDecl*>(node.Type()->GetDeclaration());
                        if (decl) {
                            VhdlFileTypeDef* tDef = dynamic_cast<VhdlFileTypeDef*>(decl->GetTypeDef());
                            if (tDef && tDef->GetFileTypeMark()) {
                                if (UtString("integer") == UtString(tDef->GetFileTypeMark()->Name())) {
                                    mTypeName = UtString(node.Type()->Name());
                                    intrinsic = true;
                                    subType = eVhdlTypeInteger;
                                } else if (UtString("boolean") == UtString(tDef->GetFileTypeMark()->Name())) {
                                    mTypeName = UtString(node.Type()->Name());
                                    intrinsic = true;
                                    subType = eVhdlTypeBoolean;
                                } else if (UtString("character") == UtString(tDef->GetFileTypeMark()->Name())) {
                                    mTypeName = UtString(node.Type()->Name());
                                    intrinsic = true;
                                    subType = eVhdlTypeChar;
                                }
                            }
                        }
                    }
                    if (subType != eVhdlTypeUnknown) {
                        skip = true;
                        TypeInfo ti(UserType::eScalar, subType, mTypeName.c_str(),  &mLibraryName, &mPackageName);
                        
                        const ConstantRange* cr= getConstraintRange();
                        if (cr != NULL && ! intrinsic) {
                            if (!node.Type()->IsRealType()) {
                                ti.setRange(*cr);
                            }
                            ti.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
                        }
                        if (UtString("integer") == baseType) {
                            VhdlSubtypeIndication* subInd = node.GetSubtypeIndication();
                            if (subInd && (ID_VHDLEXPLICITSUBTYPEINDICATION == subInd->GetClassId())) {
                                VhdlExplicitSubtypeIndication* exSubInd = static_cast<VhdlExplicitSubtypeIndication*>(subInd);
                                INFO_ASSERT(exSubInd, "invalid node");
                                if (exSubInd->GetRangeConstraint()) {
                                    VhdlConstraint* constraint = exSubInd->GetRangeConstraint()->EvaluateConstraint(0, 0);
                                    ti.setIsRangeRequiredInDeclaration(true);
                                    ConstantRange cr(constraint->Left()->Integer(),
                                                     constraint->Right()->Integer());
                                    ti.setRange(cr);
                                    if (constraint) delete constraint;
                                }
                            }
                        }
                        mTypeInfoStack->push_back(ti);
                    } else {
                        if (node.Type()->IsIntegerType()) {
                            // The integer type declared in standard.vhd.93 package is handled as a special
                            // case in vhsubtypeind, even though it is an inttype as well.
                            TypeInfo tiIntType(UserType::eScalar, eVhdlTypeIntType, baseType.c_str());
                            const ConstantRange* cr = getConstraintRange();
                            if (NULL != cr) {
                                tiIntType.setRange(*cr);
                                tiIntType.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
                            }
                            VhdlSubtypeIndication* subInd = node.Type()->GetSubtypeIndication();
                            if (subInd && (ID_VHDLEXPLICITSUBTYPEINDICATION == subInd->GetClassId())) {
                                VhdlExplicitSubtypeIndication* exSubInd = static_cast<VhdlExplicitSubtypeIndication*>(subInd);
                                INFO_ASSERT(exSubInd, "invalid node");
                                if (exSubInd->GetRangeConstraint()) {
                                    VhdlConstraint* constraint = exSubInd->GetRangeConstraint()->EvaluateConstraint(0, 0);
                                    tiIntType.setIsRangeRequiredInDeclaration(true);
                                    ConstantRange cr(constraint->Left()->Integer(),
                                                     constraint->Right()->Integer());
                                    tiIntType.setRange(cr);
                                    if (constraint) delete constraint;
                                }
                            }
                            mTypeInfoStack->push_back(tiIntType);
                        }
                    }
                }
            }
        }
        if (node.Type() && node.Type()->GetDeclaration() &&
            (ID_VHDLFULLTYPEDECL == node.Type()->GetDeclaration()->GetClassId())) { //full type decl
            if (node.Type()->GetAttributeId("enum_encoding")) {
                VhdlAttributeSpec* spec = node.Type()->GetAttributeId("enum_encoding")->FindRelevantAttributeSpec(node.Type());
                if (spec) {
                    mEnumAttrb = spec;
                }
            }
            if (mTypeName.empty()) {
                mTypeName = node.IsType() ? UtString(node.Name()) : UtString(node.Type()->Name());
                VhdlPrimaryUnit* prUnit = node.Type()->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit();
                if (ID_VHDLPACKAGEDECL == prUnit->GetClassId()) {
                    mPackageName = UtString(node.Type()->GetOwningScope()->
                                            GetContainingPrimaryUnit()->GetPrimaryUnit()->Name());
                    mLibraryName = UtString(node.Type()->GetOwningScope()->
                                            GetContainingPrimaryUnit()->GetPrimaryUnit()->Owner()->Name());
                }
            }
        }
        if (node.Type()->IsSubtype()) { //subtype definision
            VhdlPrimaryUnit* prUnit = node.Type()->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit();
            mTypeName = UtString(node.Type()->Name());
            if (ID_VHDLPACKAGEDECL == prUnit->GetClassId()) {
                mPackageName = UtString(node.Type()->GetOwningScope()->
                                     GetContainingPrimaryUnit()->GetPrimaryUnit()->Name());
                mLibraryName = UtString(node.Type()->GetOwningScope()->
                                        GetContainingPrimaryUnit()->GetPrimaryUnit()->Owner()->Name());
            }
        }
        if (node.Type()->IsUnconstrainedArrayType() && !skip) {
            UInt32 numDims = getUnconstrainedArrayDims(node);
            if (! maybeHandleNullString(numDims)) {
                pushArrayType(numDims);
                node.ElementType()->Accept(*this);
            }
        } else if (node.Type()->IsConstrainedArrayType() && !skip) {
            UInt32 numDims = getConstrainedArrayDims(node);
            if (! maybeHandleNullString(numDims)) {
                pushArrayType(numDims);
                node.ElementType()->Accept(*this);
            }
        } else if (!skip && (node.IsEnumerationType() || node.Type()->IsEnumerationType())) {
            bool isEnumType = false;
            if (node.IsType() && node.IsEnumerationType()) {
                isEnumType = true;
            } else if (node.IsType() && !node.IsEnumerationType()) {
                isEnumType = false;
            } else if (!node.IsType() && node.Type()->IsEnumerationType()) {
                isEnumType = true;
            }


            TypeInfo tiEnum(UserType::eEnum, eVhdlTypeUnknown, mTypeName.c_str(), &mLibraryName, &mPackageName);
            tiEnum.setEnumVhdlIdDefNode(&node);
            if (mEnumAttrb) {
                if (mEnumAttrb->GetValue()->IsStringLiteral()) {
                    UtString enumStr(mEnumAttrb->GetValue()->Name());
                    enumStr = enumStr.substr(1, enumStr.size() - 2);
                    UtString::iterator i = enumStr.begin();
                    UtString::iterator s = enumStr.begin();
                    while ( i != enumStr.end() ) {
                        while (*i == ' ' ) {
                            i++; s++;
                        }
                        if (i == enumStr.end() ) { 
                            break;
                        }
                        
                        while (*i != ' ' && i != enumStr.end() ) i++;
                        
                        UtString value;
                        value.assign(s, i);
                        tiEnum.addEnumEncoding(value);
                        if ( i == enumStr.end() ) {
                            break;
                        } else {
                            s = i;
                        }
                    }
                }
            }

            mTypeInfoStack->push_back(tiEnum);
            UInt32 ltDec, rtDec;
            if (getRangeOfIntegerEnum(node, ltDec, rtDec)) {
                ConstantRange cr(ltDec, rtDec);
                TypeInfo& ti = mTypeInfoStack->back();
                ti.setRange(cr);
                ti.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
                }
        } else if (node.Type()->IsRecordType()) {
            TypeInfo tiRecStart(UserType::eStruct, eVhdlTypeUnknown, mTypeName.c_str(), &mLibraryName, &mPackageName);
            mTypeName = "";
            tiRecStart.setNet(mCurrentNet);
            mTypeInfoStack->push_back(tiRecStart);
            //process record's elements
            //iterate fields of record
            UInt32 sz = node.NumOfElements();
            for (UInt32 j = 0; j < sz; ++j) {
                if (mCurrentNet->isCompositeNet()) {
                    NUCompositeNet * cnet = mCurrentNet->getCompositeNet();
                    UInt32 numFields = cnet->getNumFields();
                    for(UInt32 i = 0; i < numFields; ++i) {
                        NUNet* field = cnet->getField(i);
                        StringAtom* fName = sGetNetName(field);
                        if (UtString(fName->str()) == UtString(node.GetElementAt(j)->Name())) {
                            NUNamedDeclarationScope  *scope = dynamic_cast <NUNamedDeclarationScope*> (field->getScope());
                            if(scope ) {
                                VerificVhdlDesignWalker::NUScopeNetMap::const_iterator citr = mScopeToCompMap.find(scope);
                                if (citr == mScopeToCompMap.end()) 
                                    mScopeToCompMap[scope] = dynamic_cast <NUCompositeNet*> (mCurrentNet);
                            }
                            mCurrentNet = field;
                            mCompDim = 0;
                            mMemDim = 0;
                            break;
                        }
                    }
                } else {
                    NU_ASSERT(false, mNet);
                }
                INFO_ASSERT(node.GetElementAt(j), "invalid field of record");
                node.GetElementAt(j)->Accept(*this);
                //post
                NUNamedDeclarationScope *scope = dynamic_cast <NUNamedDeclarationScope*> (mCurrentNet->getScope());
                NUCompositeNet *cnet = mScopeToCompMap[scope];
                if(cnet) {
                    mCurrentNet = cnet;
                }
            }

            //end of record declaration
            TypeInfo tiRecEnd(UserType::eStructEnd, eVhdlTypeUnknown);
            mTypeInfoStack->push_back(tiRecEnd);
        }
    }
            

    bool getRangeOfIntegerEnum(VhdlIdDef& node, UInt32& lhs, UInt32& rhs) {
        if (node.Type()->GetSubtypeIndication()) {
            node.Type()->GetSubtypeIndication()->Accept(*this);
            if (mNeedsRangeInDeclaration) {
                lhs = mEnumRangeLhs;
                rhs = mEnumRangeRhs;
                return true;
            }
        }
        return false;
    }
    
  void pushArrayType(UInt32 numDims)
  {
      if (mCurrentNet->isVectorNet())
      {
          NUVectorNet* vnet = mCurrentNet->castVectorNet();
          NU_ASSERT(numDims == 1, vnet);
          TypeInfo tiAr(UserType::eArray, eVhdlTypeUnknown, mTypeName.c_str(), &mLibraryName, &mPackageName);
          tiAr.setRange(*(vnet->getDeclaredRange()));
          tiAr.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
          mTypeInfoStack->push_back(tiAr);
          
      }
      else if (mCurrentNet->isMemoryNet())
      {
          NUMemoryNet* mnet = mCurrentNet->getMemoryNet();
          UInt32 numMemDims = mnet->getNumDims();
          NU_ASSERT(numDims <= numMemDims && numDims > 0, mnet);
          for (UInt32 dim = 0; dim < numDims; ++dim)
          {
              const ConstantRange* cr = mnet->getRange(numMemDims - mMemDim - 1);
              
              mMemDim++; // Look for next dimension in multi-dimensional array.
              TypeInfo tiAr(UserType::eArray, eVhdlTypeUnknown, mTypeName.c_str(), &mLibraryName, &mPackageName);
              tiAr.setRange(*cr);
              tiAr.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
              mTypeInfoStack->push_back(tiAr);
          }
      }
      else if (mCurrentNet->isCompositeNet())
      {
          NUCompositeNet* cnet = mCurrentNet->getCompositeNet();
          UInt32 numArrayDims = cnet->getNumDims();
          NU_ASSERT(numDims <= numArrayDims && numDims > 0, cnet);
          for (UInt32 dim = 0; dim < numDims; ++dim)
          {
              const ConstantRange* cr = cnet->getRange(numArrayDims - mCompDim - 1);
              TypeInfo tiAr(UserType::eArray, eVhdlTypeUnknown, mTypeName.c_str(), &mLibraryName, &mPackageName);
              tiAr.setRange(*cr);
              tiAr.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
              mTypeInfoStack->push_back(tiAr);
              
              mCompDim++; // Look for next dimension in multi-dimensional array.
          }
      }
      else
      {
          NU_ASSERT(0, mNet);
      }
      
      // Make sure the array type name is reset
      clearTypeName();
  }

  void clearTypeName()
  {
      mTypeName = "";
  }
    
  const ConstantRange* getConstraintRange() const
  {
      VerificVhdlDesignWalker::NUNetToConstraintRangeMap::const_iterator p = mConstraintRangeMap->find(mCurrentNet);

      if (p == mConstraintRangeMap->end()) {
          return NULL;
      } else {
          return &(p->second);
      }
  }

  UInt32 getConstrainedArrayDims(VhdlIdDef& node) 
  {
      return node.Dimension();
  }

  UInt32 getUnconstrainedArrayDims(VhdlIdDef& node)
  {
      return node.Dimension();
  }

  bool maybeHandleNullString(UInt32 numDims)
  {
      INFO_ASSERT(mNet, "invalid net");
      if (0 < numDims && mNet->isBitNet()) {
          mTypeInfoStack->pop_back();
          return true;
      } 
      return false;
  }

  static void sGetTypeInfo(UserTypeFactory* typeFact, 
			   VhdlIdDef* node, 
			   NUNet* net,
			   const VerificVhdlDesignWalker::NUNetToConstraintRangeMap* constraintRangeMap,
                           TypeInfoStack* tiStack,
			   MsgContext* msgContext)
  {
      TypeInfoWalker typeInfoCB(typeFact, net, tiStack, msgContext, constraintRangeMap);
      node->Accept(typeInfoCB);
  }

private:

  UserTypeFactory* mTypeFactory;
  NUNet* mNet;
  UInt32 mMemDim;
  UInt32 mCompDim;
  TypeInfoStack* mTypeInfoStack;

  NUNet* mCurrentNet; // This is used only for composite nets. It points to the net
                      // that currently is browsed.
  const VerificVhdlDesignWalker::NUNetToConstraintRangeMap* mConstraintRangeMap; // This is used only for scalar nets 
                                                                                 // that have constraints.

  VerificVhdlDesignWalker::NUScopeNetMap mScopeToCompMap;
  UtString mTypeName;
  MsgContext *mMsgContext;
  VhdlAttributeSpec* mEnumAttrb;
  bool mNeedsRangeInDeclaration;
  UtString mLibraryName;
  UtString mPackageName;
  UInt32 mEnumRangeLhs; //for getting enum range left bound
  UInt32 mEnumRangeRhs; //for getting enum range right bound
};

class UserTypePopulatorCB : public NUInstanceCallback
{
public:

  UserTypePopulatorCB(STSymbolTable* symTab, 
		      const VerificVhdlDesignWalker::ScopeToNodeNetMap* netMap,
		      const VerificVhdlDesignWalker::NUNetToConstraintRangeMap* constraintRangeMap,
                      UserTypeFactory* fact, 
		      bool verbose,
		      MsgContext* msgContext)
    : NUInstanceCallback(symTab, true),
      mSymTab(symTab),
      mNetMap(netMap),
      mConstraintRangeMap(constraintRangeMap),
      mTypeFactory(fact),
      mVerbose(verbose),
      mSuccess(true),
      mMsgContext(msgContext)
  {
#ifdef DEBUG_VERIFIC_VHDL_WALKER
      std::cerr <<  " ---- print mNetMap ---- " << std::endl;
      typedef UtMap<NUScope*, VerificVhdlDesignWalker::VerificNodeNetMap>::const_iterator IT;
      typedef UtMap<VhdlIdDef*, NUNet*>::const_iterator ITT;
      for (IT it = mNetMap->begin(); it != mNetMap->end(); ++it) {
          std::cerr << "scope = " << it->first->getName()->str() << std::endl;
          for (ITT itt = it->second.begin(); itt != it->second.end(); ++itt) {
              std::cerr << "\t net = " << itt->second->getName()->str() << std::endl;
          }
      }

      std::cerr <<  " ---- print mConstraintRangeMap ---- " << std::endl;
      typedef UtMap<NUNet*, ConstantRange>::const_iterator CIT;
      for (CIT it = mConstraintRangeMap->begin(); it != mConstraintRangeMap->end(); ++it) {
          std::cerr << " net = " << it->first->getName()->str() << " range = ( " << it->second.getMsb() << " , " << it->second.getLsb() << " )" << std::endl;
      }
      std::cerr << "****************************************************" << std::endl;
#endif

  }

  ~UserTypePopulatorCB()
  {
  }

  NUDesignCallback::Status operator()(Phase phase, NUModuleInstance* inst)
  {
    NUDesignCallback::Status stat = NUDesignCallback::eNormal;
    if (phase == NUDesignCallback::ePre)
    {
      // The branch node for instances within verilog generate blocks could be missing
      // in the symbol table. Their names are constructed during population and not
      // during pre-population walk. This method can be removed once the name construction
      // for such instances moves to pre-population.
      STBranchNode* parentNode = getCurrentNode();
      STSymbolTableNode* node = mSymTab->find(parentNode, inst->getName());
      if (node != NULL) {
        stat = NUInstanceCallback::operator()(phase, inst);
      } else {
        stat = NUDesignCallback::eSkip;
      }
    }
    else
    {
      stat = NUInstanceCallback::operator()(phase, inst);
    }
    return stat;
  }

  bool getSuccess() const {
    return mSuccess;
  }

private:

  void handleModule(STBranchNode* branch, NUModule* module);
  void handleDeclScope(STBranchNode* branch, NUNamedDeclarationScope* declScope);
  void handleInstance(STBranchNode* branch, NUModuleInstance* instance);

  void populateNetTypes(STBranchNode* branch, NUScope* scope);
  const UserType* populateNetType(STBranchNode* branch, VhdlIdDef* node, NUNet* net);
  const UserType* populateVerilogNetType(STBranchNode* branch, VhdlIdDef* node, NUNet* net);
  const UserType* maybeCreateUserType(VhdlIdDef* node, NUNet* net);
  const UserType* maybeCreateVerilogNet(VhdlIdDef* node, NUNet* net);
  void  getStructFieldNames(NUNet* net, AtomVector&  fieldNames);
  void  getEnumElements(const TypeInfo& ti, ConstantRange *range, AtomVector&  enumElements);
  void  getEnumEncodingElements(const TypeInfo& ti, ConstantRange *range, AtomVector& enumEncodingElements);
  const VerificVhdlDesignWalker::VerificNodeNetMap* getNodeNetMap(NUScope* scope);
  void printVerbose(STAliasedLeafNode* net, const UserType* ut);

private:
  STSymbolTable* mSymTab;
  const VerificVhdlDesignWalker::ScopeToNodeNetMap* mNetMap;
  const VerificVhdlDesignWalker::NUNetToConstraintRangeMap* mConstraintRangeMap;
  UserTypeFactory* mTypeFactory;
  bool mVerbose;
  bool mSuccess;
  MsgContext* mMsgContext;
};

void UserTypePopulatorCB::handleModule(STBranchNode* branch, NUModule* module)
{
  populateNetTypes(branch, module);
}

void UserTypePopulatorCB::handleDeclScope(STBranchNode* branch,
                                          NUNamedDeclarationScope* declScope)
{
  populateNetTypes(branch, declScope);
}

void UserTypePopulatorCB::handleInstance(STBranchNode* branch,
                                         NUModuleInstance* instance)
{
  populateNetTypes(branch, instance->getModule());
}

void UserTypePopulatorCB::populateNetTypes(STBranchNode* branch, NUScope* scope)
{
  const VerificVhdlDesignWalker::VerificNodeNetMap* nodeNetMap = getNodeNetMap(scope);
  if (nodeNetMap != NULL)
  {
    // Convert this to net node map.
    for (VerificVhdlDesignWalker::VerificNodeNetMap::const_iterator itr = nodeNetMap->begin();
         (itr != nodeNetMap->end()); ++itr)
    {
      VhdlIdDef* node = itr->first;
      NUNet* net = itr->second;
      if (true) {
          if (net->isHierRef()) {
              continue;
          }
          populateNetType(branch, node, net);
      }
    }
  }
}

const UserType* UserTypePopulatorCB::populateNetType(STBranchNode* branch, VhdlIdDef* node, NUNet* net)
{
    STSymbolTableNode* symNode = mSymTab->find(branch, sGetNetName(net));
    STAliasedLeafNode* leafNode = NULL;
    if (symNode != NULL) {
        leafNode = symNode->castLeaf();
    }

    NU_ASSERT(leafNode != NULL, net);
    
    IODBDesignNodeData* bomData = IODBDesignDataBOM::castBOM(leafNode);
    const UserType* ut = maybeCreateUserType(node, net);
    if (ut != NULL) {
        bomData->putUserType(ut);
        if (mVerbose) {
            #ifdef DEBUG_VERIFIC_VHDL_WALKER
                printVerbose(leafNode, ut);
            #endif
        }
    }
    return ut;
}

void UserTypePopulatorCB::getStructFieldNames(NUNet* net, AtomVector&  fieldNames)
{
  AtomicCache* atomCache = mTypeFactory->getAtomicCache();
  NUCompositeNet* compNet = net->getCompositeNet();
  for(UInt32 i = 0; i < compNet->getNumFields(); i++)
  {
    NUNet* net = compNet->getField(i);
    if(net != NULL)
    {
      UtString fieldName(sGetNetName(net)->str());
      StringAtom* elemAtom = atomCache->intern(fieldName.c_str());
      fieldNames.push_back(elemAtom);
    }
  }
}

void UserTypePopulatorCB::getEnumElements(const TypeInfo& ti, ConstantRange* /*range*/, AtomVector&  enumElements)
{
    AtomicCache* atomCache = mTypeFactory->getAtomicCache();
    VhdlIdDef* idDef = ti.getEnumVhdlIdDefNode();
    INFO_ASSERT(idDef, "invalid VhdlIdDef node");
    INFO_ASSERT(idDef->Type()->IsEnumerationType(), "should be enumeration type");
    UInt32 s = idDef->Type()->NumOfEnums();
    for (UInt32 i = 0; i < s; ++i) {
        VhdlIdDef* enumEl = idDef->Type()->GetEnumAt(i);
        INFO_ASSERT(enumEl, "invalid enumeration element");
        StringAtom* elemAtom = atomCache->intern(enumEl->Name());
        enumElements.push_back(elemAtom);
    }
}

void UserTypePopulatorCB::getEnumEncodingElements(const TypeInfo& ti, 
						  ConstantRange *range, 
						  AtomVector&  enumEncodingElements)
{
  AtomicCache* atomCache = mTypeFactory->getAtomicCache();
  const UtStringArray& enumEncodings = ti.getEnumEncodings();
  if (enumEncodings.empty())
    return;

  int i = 0;
  for (UtStringArray::const_iterator iter = enumEncodings.begin();
       iter != enumEncodings.end();
       ++iter)
  {
    if (range == NULL || range->contains(i))
    {
      StringAtom* encodingAtom = atomCache->intern(*iter);
      enumEncodingElements.push_back(encodingAtom);
    }
    i++;
  }
}

// A scalar is considered a vector type if arrays of them are equivalent
// (from a cbuild memory resynthesis standpoint) to a Verilog vector.
// This is used to populate the isPacked UserArray flag.
static bool sIsVectorType(const UserType* ut) 
{
  bool vec = false;
  if (ut && (ut->getType() == UserType::eScalar)) {
    CarbonVhdlType type = ut->getVhdlType();
    if (   (eVhdlTypeStdLogic  == type)
        || (eVhdlTypeStdULogic == type)
        || (eVhdlTypeBit       == type)
        || (eVhdlTypeBoolean   == type)
      )
      vec = true;
  }
  return vec;
}

const UserType* UserTypePopulatorCB::maybeCreateUserType(VhdlIdDef* node, NUNet* net)
{
  TypeInfoStack tiStack;
  TypeInfoWalker::sGetTypeInfo(mTypeFactory, node, net, mConstraintRangeMap, 
			   &tiStack, mMsgContext);

  const UserType* ut = NULL;
  TypeInfoStackCRIter itrEnd   = tiStack.rend();
  UserTypeVector* vecUserType = NULL;
  VectorUserTypeVector vecUserTypeVec;

  for (TypeInfoStackCRIter itr = tiStack.rbegin(); itr != itrEnd; ++itr)
  {
    const TypeInfo& ti = *itr;
    ConstantRange *range = NULL, rangeObject;
    
    if(ti.isRange())
    {
      rangeObject = ti.getRange();
      range = &rangeObject;
    }
 
    if(ti.getType() == UserType::eScalar)
    {
      ut = mTypeFactory->maybeAddScalarType(eLanguageTypeVhdl,
					    eVerilogTypeUnknown, 
					    ti.getVhdlType(), 
					    range, 
					    ti.getCompositeTypeName(),
					    ti.isRangeRequiredInDeclaration(),
                                            ti.getLibraryName(),
                                            ti.getPackageName());
      if(vecUserType != NULL)
        vecUserType->push_back(ut);
    }
    else if(ti.getType() == UserType::eArray)
    {
      if(vecUserType != NULL)
        vecUserType->pop_back();

      // Borrowed directly from Interra population:
      // In vhdl, arrays of scalars (std_logic, std_ulogic, etc) are packed.
      // However, arrays of scalars (integer, real, etc, are NOT packed)
      // 'ut' is previously created UserType, to which this UserArray will point.
      bool isPacked = sIsVectorType(ut);
      ut = mTypeFactory->maybeAddArrayType(eLanguageTypeVhdl, 
					   eVerilogTypeUnknown, 
					   ti.getVhdlType(), 
                                           range,
					   ut,
                                           isPacked,
					   ti.getCompositeTypeName(),
					   ti.isRangeRequiredInDeclaration(),
                                           ti.getLibraryName(),
                                           ti.getPackageName());
      if(vecUserType != NULL)
        vecUserType->push_back(ut);
    }
    else if(ti.getType() == UserType::eStruct)
    {
      // reverse the user types for records
      UserTypeVector rVecUserType;
      for(UserTypeVectorRIter ritr = vecUserType->rbegin(); 
          ritr != vecUserType->rend(); 
          ++ritr)
      {
        const UserType* utVal = *ritr;
    #ifdef DEBUG_VERIFIC_VHDL_WALKER
        utVal->print();
    #endif
        rVecUserType.push_back(utVal);
      }
      
      AtomVector  fieldNames;
      getStructFieldNames(ti.getNet(), fieldNames);
      ut = mTypeFactory->maybeAddStructType(eLanguageTypeVhdl, 
					    eVerilogTypeUnknown, 
					    ti.getVhdlType(), 
                                            rVecUserType, 
					    fieldNames,
					    ti.getCompositeTypeName(),
                                            ti.getLibraryName(),
                                            ti.getPackageName() );
      vecUserType->clear();

      vecUserTypeVec.pop_back();
      delete vecUserType;
      if (vecUserTypeVec.size() > 0)
        vecUserType = vecUserTypeVec.back();
      else
        vecUserType = NULL;

      if(vecUserType != NULL)
        vecUserType->push_back(ut);
    }
    else if(ti.getType() == UserType::eStructEnd)
    {
      vecUserType = new UserTypeVector();
      vecUserTypeVec.push_back(vecUserType);
    }
    else if(ti.getType() == UserType::eEnum)
    {
      AtomVector  enumElems;
      getEnumElements(ti, range, enumElems);

      // An enum may have a ENUM_ENCODING attribute associated with it
      // which needs to be preserved in the database.
      AtomVector enumEncodingElems;
      getEnumEncodingElements(ti, range, enumEncodingElems);

      ut = mTypeFactory->maybeAddEnumType(eLanguageTypeVhdl, 
					  eVerilogTypeUnknown, 
					  ti.getVhdlType(),
                                          range, 
					  enumElems,
					  enumEncodingElems,
					  ti.getCompositeTypeName(),
					  ti.isRangeRequiredInDeclaration(),
                                          ti.getLibraryName(),
                                          ti.getPackageName());
      if(vecUserType != NULL)
        vecUserType->push_back(ut);
    }
  }
  return ut;
}

const VerificVhdlDesignWalker::VerificNodeNetMap* UserTypePopulatorCB::getNodeNetMap(NUScope* scope)
{
  const VerificVhdlDesignWalker::VerificNodeNetMap* nodeNetMap = NULL;
  VerificVhdlDesignWalker::ScopeToNodeNetMap::const_iterator sitr = mNetMap->find(scope);
  if (sitr != mNetMap->end()) {
    nodeNetMap = &(sitr->second);
  }
  return nodeNetMap;
}

void UserTypePopulatorCB::printVerbose(STAliasedLeafNode* net, const UserType* ut)
{
  UtIO::cout() << "Net: ";
  net->print();
  ut->print();
  UtIO::cout() << UtIO::endl;
}

UserTypePopulate::UserTypePopulate(VerificVhdlDesignWalker* vhdlDesignWalker,
                                   IODBNucleus* iodb, 
				   bool verbose,
				   MsgContext* msgContext)
  : mVhdlDesignWalker(vhdlDesignWalker),
    mIODB(iodb),
    mVerbose(verbose),
    mMsgContext(msgContext)
{
}

bool UserTypePopulate::populate(NUDesign* design)
{
  IODBDesignDataSymTabs* symTabs = mIODB->getDesignDataSymbolTables();
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << " ------------------------> symbol table before <----------------------" << std::endl;
  symTabs->getElabSymTab()->print();
  std::cerr << "---------------------------------------------------------------" << std::endl;
#endif
  UserTypePopulatorCB utp(symTabs->getElabSymTab(), 
			  mVhdlDesignWalker->getVerificNodeNetMap(),
			  mVhdlDesignWalker->getConstraintRangeMap(),
                          mIODB->userTypeFactory(), 
			  mVerbose, 
			  mMsgContext);

  mIODB->userTypeFactory()->setUserTypesAtomicCache(symTabs->getAtomicCache());

  NUDesignWalker walker(utp, false, false);
  walker.putWalkTasksFromTaskEnables(true);
  walker.design(design);
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << " ------------------------> symbol table after <----------------------" << std::endl;
  symTabs->getElabSymTab()->print();
  std::cerr << "---------------------------------------------------------------" << std::endl;
#endif
  return utp.getSuccess();
}

}
