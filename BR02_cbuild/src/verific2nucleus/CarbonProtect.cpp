// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


// Project headers
#include "verific2nucleus/CarbonProtect.h"
#include "verific2nucleus/VerificDesignManager.h"

// Other project headers
#include "util/SourceLocator.h"
#include "compiler_driver/ProtectedSource.h"
#include "compiler_driver/CarbonContext.h"


// Verific headers
#include "Strings.h"
#include "Protect.h"

// Standard headers
#include <iostream>
// #include <stdio.h>          // sprintf

namespace verific2nucleus {

//524288=65536*8 putting number bigger enough to pass the test/protected-source/bigprot
//Possible solutions for the future are
//1. provide a command line switch to set the size.  give it a default that is fairly large, and let the user set it higher if needed
//2. make the size dynamic (growing as needed)
//3. switch to stream processing
CarbonProtect::CarbonProtect(VerificDesignManager* mgr) 
    : Verific::Protect(524288)
    , mVerificDesignMgr(mgr)
    , mHaveReportedUnsupported(false) 
    , mProtectBufs()
{
}

CarbonProtect::~CarbonProtect() 
{
}

// Cheetah calls this when it sees a `protected/`endprotected block. 
const char* CarbonProtect::decryptCarbon(const char* buf, Verific::linefile_type lf)
{
  //CheetahContext* cc = reinterpret_cast<CheetahContext*>( clientData );
  const char* filename = lf->GetFileName();
  int line = lf->GetLeftLine();

  const char* ret = "";
  if (mVerificDesignMgr->getCarbonContext()->isLibraryCompileOnly()) {
    mVerificDesignMgr->getMsgContext()->CannotCompileProtectedSrcIntoLib(filename);
    return ret;
  }

  // We want the last line of protected text.  The 'line' variable points
  // to the `endprotected , which is one after the last line of text.
  //--line; 
  // TODO Somehow need to get filename,line info, Verific api doesn't provide it 
  SourceLocatorFactory f;
  const SourceLocator srcLocator = f.create(filename, line);

  // Construct a fake filename based on the file/line where the
  // `protected keyword was found.  We'll begin the filename with
  // '-' because in general it's a pain in Unix to have files that
  // start with '-'.  I hope that helps make uniqueness very likely,
  // but we will also put the exact name into a hash-table;
  UtString fnameBuf;

  // there will be a newline between `protected and `endprotected
  if (*buf == '\n')
    ++buf;

  if (buf[0] == '@')
  {
    // Make sure we know that a line was parsed from a protected block,
    // so that if the user writes a token file, we leave the output encrypted.
    ProtectedSource source;
    ++buf; // skip over leading '@'
    source << buf;  
    if (source.unprotect())
    {
      // Skip whitespace.
      const size_t offset = source.find_first_not_of(" \t\n\r");
      // Don't allow -generateCleartextSource option unless source
      // was protected with +protectHDLOnly.
      if (mVerificDesignMgr->getCarbonContext()->isGenerateCleartextSource() &&
          strncmp(source.c_str() + offset,
                  ProtectedSource::cleartextComment(),
                  strlen(ProtectedSource::cleartextComment())) != 0) // Didn't match
      {
        // "Cannot use -generateCleartextSource with `protected source."
        mVerificDesignMgr->getCarbonContext()->getMsgContext()->NoCleartextProtected(&srcLocator);
        mProtectBufs.push_back("");
      }
      else
      {
        mProtectBufs.push_back(source);
      }
    }
    else
    {
      // error unprotecting source block
      mVerificDesignMgr->getMsgContext()->InvalidEncryptionSyntax(&srcLocator);
      mProtectBufs.push_back("");
    }

    // un-comment line below to dump source encrypted via +protect
    // fprintf(stdout, "DECRYPT: %s:%d\n\t%s\n\n", filename, line, cc->mProtectBufs.back());

    ret = mProtectBufs.back();
  }
  // Check returned size against Verific limit
  unsigned block_size = GetBlockSize();
  unsigned ret_size = Verific::Strings::len(ret) ;
  if (ret_size > block_size) {
      mVerificDesignMgr->getMsgContext()->ProtectedRegionSizeLimit(&srcLocator, ret_size, block_size);
  }

  return ret;
}
  
unsigned CarbonProtect::decrypt(const char *in_buf, char *out_buf, unsigned size, Verific::linefile_type lf) 
{
    // Here we should do carbon decryption
    if(!in_buf || !out_buf) return 0 ;
    const char* decrypted_str = decryptCarbon(in_buf, lf);
    memset(out_buf, 0, size) ; // set all bytes to '\0'
    unsigned ret_size = Verific::Strings::len(decrypted_str) ;
    // ret_size is less than actual size, we do strip some characters (tokens) this is specific to CARBON encryption algorithm
    unsigned i = 0 ;
    for (i = 0 ; i < ret_size ; ++i) {
        out_buf[i] = decrypted_str[i] ;
    }
    return ret_size;
  }
// seems the routine won't be used in carbon - it is intended for the style : `pragma protect <protectblockid>
/*
  char* CarbonProtect::decrypt(void){
    if ( ! mHaveReportedUnsupported ) {
      mVerificDesignMgr->getMsgContext()->UnsupportedProtectedRegion();
      mHaveReportedUnsupported = true;
    }
    // if the user demotes the alert to warning then we ignore the contents of the protected region
    return NULL;
  }
*/

}
