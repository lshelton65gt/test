// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "verific2nucleus/VerificVerilogDesignWalker.h"     // Visitor base class definition
#include "verific2nucleus/VerificVhdlDesignWalker.h"     // Visitor base class definition
#include "verific2nucleus/VerificDesignManager.h" // Design Manager definition
#include "verific2nucleus/V2NDesignScope.h" // Design Scope definition
#include "verific2nucleus/Verific2NucleusUtilities.h" // For Source locator
#include "verific2nucleus/VerificTicProtectedNameManager.h"

#include "Strings.h"        // A string utility/wrapper class
#include "Message.h"        // Make message handlers available, not used in this example

#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriId.h"         // Definitions of all identifier definition tree nodes
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "VeriConstVal.h"   // Definitions of parse-tree nodes representing constant values in Verilog.
#include "veri_tokens.h"    // Definitions of verific enumeration
#include "VeriBaseValue_Stat.h" //Definition of VeriBaseValue class

// Vhdl include
#include "VhdlUnits.h"

#include <stdio.h>          // sprintf
#include <string.h>         // strcpy, strchr ...
#include <ctype.h>          // isalpha, etc ...
#include <cmath>            // for pow
#include <iostream>
#include <vector>
#include <string>
#include <utility>

// Nucleus headers
#include "nucleus/NUBitNet.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUCopyContext.h"
#include "nucleus/NUBitNetHierRef.h"
#include "nucleus/NUVectorNetHierRef.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUSysFunctionCall.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUElabHierHelper.h"
#include "util/AtomicCache.h"
#include "util/SetOps.h" // for set_intersection
#include "util/UtConv.h" // for CarbonValRW

//Set to enable debug
//#define DEBUG_VERIFIC_VERILOG_WALKER 1

namespace verific2nucleus {

using namespace std ;
using namespace Verific ;

//! Class which holds grabbed parameter values from Verific
/*!
  Verific is used to evaluate the parameter expressions. This holds
  the result.
 */
class VerificVerilogDesignWalker::ParamValue
{
    public:
        //! trivial constructor
        ParamValue()
        {
            clear();
        }
        // Interra  Verific value mapping
        // VE_CONST  => INTEGER
        // VE_STRING => ASCIISTR
        // VE_BASEDNUMBER => BASEDNUM
        // VE_FLOATNUMBER => REAL


        //! (Re)Initialize the object
        void clear()
        {
            mBuf.clear();
            mSize = 0;
            mType = INTEGER;
            mParamVal = 0;
            mDblVal = 0.0;
            mDoNoValOpt = false;
            mConvertBasedNum = true;
            mValidDblVal = false;
            mValidParamVal = false;
            mValidSize = false;
        }

        //! Get the parameter value string - constant
        const UtString& getStr() const 
        {
            return mBuf;
        }

        //! Get the parameter value string - modifiable
        UtString& getStr() {
            return mBuf;
        }

        //! Does the value have an X or a Z?
        bool hasXZ() const {
            bool ret = false;
            if ((mType != INTEGER) &&
                    (mType != ASCIISTR))
                ret =  (mBuf.find_first_of("xzXZ?") != UtString::npos);
            return ret;
        }

        //! Get the type of the parameter
        veri_value_type getType() const
        {
            return mType;
        }

        //! Get the effective width of the parameter
        UInt32 getSize() const {
            ASSERT(mValidSize);
            return mSize;
        }

        //! Get the integer value of the parameter 
        /*!
          This is only useful if hasValidParamVal() is true.
         */
        SInt32 getParamVal() const {
            ASSERT(hasValidParamVal());
            return mParamVal;
        }

        //! Get the double value of the parameter
        /*!
          This is only useful if hasValidDblVal() is true
         */
        double getDblValue() const {
            ASSERT(hasValidDblVal());
            return mDblVal;
        }

        //! False if the user does not want to convert based numbers
        /*!
          For parameter-based module uniquification, we want binary for
          most things to keep the uniquification consistent based on size
          and value. But for cmodels, we just want the based number as it
          was specified. 

          The default is true (unlike doNoValOpt(),
          mainly to stay away from a very confusing function name).
         */
        bool doConvertBasedNumber() const { return mConvertBasedNum; }


        //! Convert based numbers to binary?
        /*!
          See doConvertBasedNumber()
         */
        void putConvertBasedNumber(bool convertBased) { 
            mConvertBasedNum = convertBased;
        }

        //! Perform value optimization?
        /*!
          During getParamValueString, the value is analyzed to see if we
          can shorten it. If this is true, then we do not shorten it.
          Default is false.
         */
        bool doNoValOpt() const { 
            return mDoNoValOpt;
        }

        //! See doNoValOpt()
        void putDoNoValOpt(bool valOpt)
        {
            mDoNoValOpt = valOpt;
        }

        //! Setting for Verific type
        void putType(veri_value_type type) {
            mType = type;
        }

        //! Setting for integer value
        void putValue(SInt32 val) {
            ASSERT(not hasValidDblVal());
            mParamVal = val;
            mValidParamVal = true;
        }

        //! Setting for size
        void putSize(UInt32 sz) {
            mSize = sz;
            mValidSize = true;
        }

        //! Setting for double value
        void putDbl(double val)
        {
            ASSERT(not hasValidParamVal());
            mDblVal = val;
            mValidDblVal = true;
        }

        //! Return true if putValue() has been called, false otherwise.
        /*!
         * Must not call getParamVal() unless this routine returns true;
         */
        bool hasValidParamVal() const
        {
            return mValidParamVal;
        }

        //! Return true if putDbl() has been called, false otherwise.
        /*!
         * Must not call getDblVal() unless this routine returns true;
         */
        bool hasValidDblVal() const
        {
            return mValidDblVal;
        }

        //! Invalidate any dbl val that may be held.
        void invalidateDblVal()
        {
            mValidDblVal = false;
            mDblVal = 0.0;
        }

        //! Invalidate any param val that may be held.
        void invalidateParamVal()
        {
            mValidParamVal = false;
            mParamVal = 0;
        }

    private:
        UtString mBuf;
        UInt32 mSize;
        veri_value_type mType;
        SInt32 mParamVal;
        double mDblVal;
        bool mDoNoValOpt;
        bool mConvertBasedNum;

        // Note on the following booleans:  This class and its use is fairly messy, this is an attempt
        // to make sure that if someone queries a value, there is a valid value in place.
        bool mValidDblVal;    // true if putDbl was called, false otherwise.
        bool mValidParamVal;  // true if putValue was called, false otherwise.
        bool mValidSize;      // true if putSize was called, false otherwise.
};

//! Class to maintain unique parameterized module names
/*!
  This does not keep all the information for all parameterized
  modules. This is simply a map of constructed name based on parameter
  values to unique ids. 
  This does, however, keep all the parameter names for each
  parameterized module, so Verific doesn't have to supply us with the
  parameter name every time through the analyzeInstanceParameters
  call.

  As a result of not keeping all the information for a given instance
  of a parameterized module, we write out a file,
  libdesign.parameters, during the analysis process. Currently,
  keeping all the information around would be horriblely memory
  consuming. We could avoid this by implementing a more savvy hashing
  strategy based on parameter values instead of strings.
 */
class VerificVerilogDesignWalker::ParamModuleMap
{

    public:
        //! A simple class that wraps a parameter
        class Element
        {
            public:
                Element(const char* name) : mName(name)
                {}

                //! Put the interpreted value of the parameter
                void putStr(const UtString& valueStr)
                {
                    mValueString.assign(valueStr);
                }

                //! Get the value str
                const UtString& getValueStr() const {
                    return mValueString;
                }

                //! Get the parameter name
                const UtString& getName() const {
                    return mName;
                }

            private:
                UtString mValueString;
                UtString mName;

                // forbid
                Element();
                Element(const Element&);
                Element& operator=(const Element&);
        };

        //! Map of modules to unique instances
        /*!
          The 'unique instances' in this case are represented by a simple
          UInt32 which increments everytime a unique instance of a
          parameterized module is encountered. The map is really from
          \<unique string id\> to \<index\>. The unique string id is constructed
          by taking all the value strs of the parameters and concating them
          together. 
         */
        class ParamListObj
        {
            typedef UtArray<Element*> ElementVec;
            typedef UtHashMap<UtString, UInt32> StrToIntMap;

            public:
            ParamListObj() : mUniqueNum(0), mInit(false) {}

            ~ParamListObj()
            {
                for (ElementVec::iterator p = mElements.begin(), e = mElements.end(); 
                        p != e; ++p)
                    delete *p;
            }

            //! Have all the parameter names been initialized?
            bool isInitialized() const { return mInit; }

            //! Set whether or not this has been initialized
            void putInit(bool isInit) { mInit = isInit; }

            //! Number of parameters
            UInt32 size() const { return mElements.size(); }

            //! Get the parameter at specified index (0 - size-1)
            Element* getElement(UInt32 index) {
                // warning: unchecked. Caller must check.
                return mElements[index];
            }

            //! Add a parameter
            Element* addElement(const UtString& name)
            {
                Element* elem = new Element(name.c_str());
                mElements.push_back(elem);
                return elem;
            }

            //! Construct the unique id.
            /*!
              Returns true if this id has never been seen before.
             */
            bool constructSuffix(UtString* suffix)
            {
                *suffix << '_';

                bool isUnique = false;
                UtString fullAttribs;
                for (ElementVec::const_iterator p = mElements.begin(), e = mElements.end();
                        p != e; ++p)
                {
                    const Element* elem = *p;
                    const UtString& valueStr = elem->getValueStr();
                    fullAttribs.append(valueStr);
                    fullAttribs << '_';
                }
                // remove trailing '_'
                INFO_ASSERT(! fullAttribs.empty(), "inconsistent string");
                fullAttribs.erase(fullAttribs.size() - 1);

                StrToIntMap::const_iterator q = mMap.find(fullAttribs);
                if (q != mMap.end())
                {
                    UInt32 val = q->second;
                    *suffix << "#" << val;
                }
                else
                {
                    *suffix << "#" << mUniqueNum;
                    mMap[fullAttribs] = mUniqueNum;
                    ++mUniqueNum;
                    isUnique = true;
                }

                return isUnique;
            }

            private:
            ElementVec mElements;
            StrToIntMap mMap;
            UInt32 mUniqueNum;
            bool mInit;

            ParamListObj(const ParamListObj&);
            ParamListObj& operator=(const ParamListObj&);
        };

    private:
        typedef UtHashMap<UtString, ParamListObj*> StrToParamListObj;

    public:
        //! Open the libdesign.parameters file and init this object
        ParamModuleMap(const char* fileRoot, MsgContext* msgContext) 
        {
            mOutFileIsInit = false;
            UtString fileName(fileRoot);
            fileName << ".parameters";
            mOutFile = new UtOBStream(fileName.c_str());

            if (mOutFile->bad())
                msgContext->ParamMapFileError(mOutFile->getErrmsg());
        }

        ~ParamModuleMap() {
            delete mOutFile;

            for (StrToParamListObj::UnsortedLoop p = mModToParamListObj.loopUnsorted(); ! p.atEnd(); ++p)
            {
                ParamListObj* pl = p.getValue();
                delete pl;
            }
        }

        //! Get the list of unique modules for the given original name.
        ParamListObj* getParamList(UtString& origName)
        {
            ParamListObj* ret = NULL;
            StrToParamListObj::iterator p = mModToParamListObj.find(origName);
            if (p != mModToParamListObj.end())
                ret = p->second;
            else
            {
                ret = new ParamListObj;
                mModToParamListObj[origName] = ret;
            }

            return ret;
        }

        //! Write initial comments to the file.
        /*!
          This is only written once. We don't write anything to the file if
          there are no parameterized modules. So, this function gets called
          every time we encounter a parameterized module instance.
         */
        void maybeWriteInitialComments()
        {
            if (! mOutFileIsInit)
            {
                UtOBStream& out = *mOutFile;

                out << "# Map of a original module name to uniquified module name and parameter values" << UtIO::endl;
                out << "#" << UtIO::endl;
                out << "# <uniquified_name> -> <original_name>" << UtIO::endl;
                out << "#      <parameter_1> = <val>" << UtIO::endl;
                out << "#      <parameter_2> = <val>" << UtIO::endl;
                out << "#      ..." << UtIO::endl;
                out << "# ..." << UtIO::endl << UtIO::endl;

                mOutFileIsInit = true;
            }
        }

        //! Get the output file stream
        UtOBStream& getParameterMapFile()
        {
            return *mOutFile;
        }

    private:
        StrToParamListObj  mModToParamListObj;
        UtOBStream* mOutFile;
        bool mOutFileIsInit;

        ParamModuleMap();
        ParamModuleMap(const ParamModuleMap&);
        ParamModuleMap& operator=(const ParamModuleMap&);
};




void
VerificVerilogDesignWalker::addModuleItem(unsigned id, const SourceLocator & loc)
{
    NUModule* module = dynamic_cast<NUModule*>(getNUScope(NULL));
    NUContAssign* contAssign = NULL;
    UtVector<NUBase*> contAssignVector;
    UtVector<NUBase*>::iterator it;
    // TODO move specify block continousassign "add to module" into corresponding visitor
    // we can't have them here because of generate blocks (in case of generate these wouldn't be module items, so add to parent never occur);
    switch (id) {
    case ID_VERISPECIFYBLOCK :
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, loc, "Incorrect module type");
        contAssignVector.clear();
        gScope()->gValueVector(contAssignVector);
        it = contAssignVector.begin();
        while(it != contAssignVector.end()) {
          contAssign = dynamic_cast<NUContAssign*>(*it);
          NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(contAssign, loc, "Unable to get value for continuous assign");
          mNucleusBuilder->aContinuesAssignToModule(contAssign, module);
          ++it;
        }
        break;
    case ID_VERIALWAYSCONSTRUCT :
    case ID_VERICONTINUOUSASSIGN :
    case ID_VERIINITIALCONSTRUCT :
    case ID_VERIGATEINSTANTIATION :
    case ID_VERIFUNCTIONDECL :
    case ID_VERITASKDECL :
    case ID_VERINETDECL :
    case ID_VERIDATADECL :
    case ID_VERIMODULEINSTANTIATION :
    case ID_VERIDEFPARAM :
    case ID_VERITABLE :
    case ID_VERIGENERATEBLOCK :
    case ID_VERITIMEUNIT :
    case ID_VERICLASS :
    case ID_VERIIMPORTDECL :
    case ID_VERIMODULE :
    case ID_VERISEQUENCEDECL :
    case ID_VERIPROPERTYDECL :
    case ID_VERIMODPORT :
    case ID_VERIMODPORTDECL :
    case ID_VERICLOCKINGDECL :
    case ID_VERICONSTRAINTDECL :
    case ID_VERIBINDDIRECTIVE :
    case ID_VERIOPERATORBINDING :
    case ID_VERINETALIAS :
    case ID_VERICOVERGROUP :
    case ID_VERICOVERAGESPEC :
    case ID_VERILETDECL :
    case ID_VERIEXPORTDECL :
        // For these values of ID_*, the item was either:
        // 1) already added to the module in the visitor to the child (for example the ALWAYSCONSTRUCT was handled by the visitor for that object), or
        // 2) it can be ignored at this point (does not have return value like defparam);
        break;
    default:
      UtString msg;
      msg << "ID type: " << (void*)(id); // we would like a way to print the name but I don't know of a method currently available to map ID_* to a string
                                         // VeriNode::PrintToken(id);  is not correct 
      reportUnsupportedLanguageConstruct(loc, msg);
    }
}

// Method: getVerificOperatorForUnaryDoubleOperator
// Converts an operator expressed as a string into a VERI_* token value.  Verific has an equivalent method embedded in the lexer code but
// does not seem to be callable externally.  In addition here we need some specific mapping, because the function is used to produce single
// operators for handling unary double operators in nucleus  although '~' is the operation used we need to create logical not operation
// because after REDAND/REDOR/REDXOR the result is already 1 bit long.
unsigned
VerificVerilogDesignWalker::getVerificOperatorForUnaryDoubleOperator(const UtString& oper)
{
    if (UtString("~") == oper) {
        // As this is used for unary double operators case the value is always logical not
        return VERI_LOGNOT;
    } else if (UtString("&") == oper) {
        return VERI_REDAND;
    } else if (UtString("|") == oper) {
        return VERI_REDOR;
    } else if (UtString("^") == oper) {
        return VERI_REDXOR;
    } else {
        INFO_ASSERT(false, "Unsupported operation");
    }
    return 0;
}


// Method: getVerificOperatorForBinaryDoubleOperator
// Converts an operator expressed as a single character string into a VERI_* token value.
// Verific has an equivalent method embedded in the lexer code but does not seem to be callable externally.
// Note: the names returned appear to be "Reduction" operators, but remember that these names are just names for symbols, they do not imply
//       that the operator is really a reduction operator.  For example verific should have used the name VERI_AMPERSAND instead of the
//       confusing VERI_REDAND, it is just a macro name for the symbol "&".
unsigned
VerificVerilogDesignWalker::getVerificOperatorForBinaryDoubleOperator(const UtString& oper)
{
    if (UtString("~") == oper) {
        // As this is used for double operators case the value is always logical not
        return VERI_REDNOT;
    } else if (UtString("&") == oper) {
        return VERI_REDAND;
    } else if (UtString("|") == oper) {
        return VERI_REDOR;
    } else if (UtString("^") == oper) {
        return VERI_REDXOR;
    } else {
        INFO_ASSERT(false, "Unsupported operation");
    }
    return 0;
}

unsigned
VerificVerilogDesignWalker::getVerificOperatorForAssignSubOperator(unsigned oper)
{
    switch (oper) {
    case VERI_INC_OP :          return VERI_PLUS;       // "++"
    case VERI_DEC_OP :          return VERI_MIN;        // "--"
    case VERI_PLUS_ASSIGN  :    return VERI_PLUS;       // "+="
    case VERI_MIN_ASSIGN  :     return VERI_MIN;        // "-="
    case VERI_MUL_ASSIGN :      return VERI_MUL;        // "*="
    case VERI_DIV_ASSIGN :      return VERI_DIV;        // "/="
    case VERI_MOD_ASSIGN :      return VERI_MODULUS;    // "%="
    case VERI_AND_ASSIGN :      return VERI_AND;        // "&="
    case VERI_OR_ASSIGN :       return VERI_OR;         // "|="
    case VERI_XOR_ASSIGN :      return VERI_XOR;        // "^="
    case VERI_LSHIFT_ASSIGN :   return VERI_LSHIFT;     // "<<="
    case VERI_RSHIFT_ASSIGN :   return VERI_RSHIFT;     // ">>="
    case VERI_ALSHIFT_ASSIGN :  return VERI_ARITLSHIFT; // "<<<="
    case VERI_ARSHIFT_ASSIGN :  return VERI_ARITRSHIFT; // ">>>="
    default:
        INFO_ASSERT(false, "Unsupported assign operator type");
        return 0;
    }
}

bool
VerificVerilogDesignWalker::isIncDecOperator(unsigned oper)
{
    switch (oper) {
    case VERI_INC_OP:
    case VERI_DEC_OP:
        return true;
    default:
        return false;
    }
}

bool
VerificVerilogDesignWalker::isCompoundAssignOperator(unsigned oper)
{
    switch (oper) {
    case VERI_PLUS_ASSIGN  :    //"+="
    case VERI_MIN_ASSIGN  :     //"-="
    case VERI_MUL_ASSIGN :      //"*="
    case VERI_DIV_ASSIGN :      //"/="
    case VERI_MOD_ASSIGN :      //"%="
    case VERI_AND_ASSIGN :      //"&="
    case VERI_OR_ASSIGN :       //"|="
    case VERI_XOR_ASSIGN :      //"^="
    case VERI_LSHIFT_ASSIGN :   //"<<="
    case VERI_RSHIFT_ASSIGN :   //">>="
    case VERI_ALSHIFT_ASSIGN :  //"<<<="
    case VERI_ARSHIFT_ASSIGN :  //">>>="
        return true;
    default:
        return false;
    }
}


bool 
VerificVerilogDesignWalker::isDoubleOperator(unsigned oper)
{
    switch (oper) {
    case VERI_NAND:
    case VERI_NOR:
    case VERI_XNOR:
    case VERI_REDNAND :         // ~& reduction nand (unary), or nand (binary)
    case VERI_REDNOR :          // ~| reduction nor (unary), or nor (binary)
    case VERI_REDXNOR :         // ~^ reduction xnor (unary) or xNor (binary)
        return true;
    default:
        return false;
    }
}

// for the given Verific token 'oper', set up to one of the three flags to be true.
// if the oper is not one of the recognized gate types then none of the flags is set to true.
void 
VerificVerilogDesignWalker::getGateFlags(unsigned oper, bool* isMultiOutputGate, bool* isMultiInputGate, bool* isTriStateGate)
{
    switch (oper) {
    case VERI_BUF :
    case VERI_NOT :
        *isMultiOutputGate = true;
        break;
    case VERI_AND :
    case VERI_OR :
    case VERI_XOR :
    case VERI_NAND :
    case VERI_NOR :
    case VERI_XNOR :
        *isMultiInputGate = true;
        break;
    case VERI_BUFIF0 :
    case VERI_BUFIF1 :
    case VERI_NOTIF0 :
    case VERI_NOTIF1 :
    case VERI_PMOS :
    case VERI_NMOS :
    case VERI_RPMOS :
    case VERI_RNMOS :
    case VERI_CMOS :
    case VERI_RCMOS :
        *isTriStateGate = true;
        break;
    default:
        break;
    }
}

bool VerificVerilogDesignWalker::isResistiveDevice(unsigned type)
{
  // This covers all cases of resistive devices, even if we do not
  // support them
  return ((VERI_RCMOS == type) ||
          (VERI_RNMOS == type) ||
          (VERI_RPMOS == type) || 
          (VERI_RTRAN == type) ||
          (VERI_RTRANIF0 == type) ||
          (VERI_RTRANIF1 == type)
          );
}

UtPair<unsigned,unsigned>
VerificVerilogDesignWalker::getDoubleOperator(unsigned oper)
{
    switch (oper) {
    case VERI_NAND :
        return UtPair<unsigned, unsigned>(VERI_AND, VERI_NOT);
    case VERI_NOR :
        return UtPair<unsigned, unsigned>(VERI_OR, VERI_NOT);
    case VERI_XNOR :
        return UtPair<unsigned, unsigned>(VERI_XOR, VERI_NOT);
    default :
        INFO_ASSERT(false, "Unsupported double operation");
    }
    return UtPair<unsigned,unsigned>(0,0);
}

bool 
VerificVerilogDesignWalker::isSupportedSysFunction(unsigned funcType)
{
    switch(funcType) {
    case VERI_SYS_CALL_TIME:
    case VERI_SYS_CALL_STIME:
    case VERI_SYS_CALL_REALTIME:
    case VERI_SYS_CALL_BITSTOREAL:
    case VERI_SYS_CALL_ITOR:
    case VERI_SYS_CALL_REALTOBITS:
    case VERI_SYS_CALL_RTOI:
    case VERI_SYS_CALL_SIGNED:
    case VERI_SYS_CALL_UNSIGNED:
    case VERI_SYS_CALL_FOPEN:
    case VERI_SYS_CALL_DIST_CHI_SQUARE:
    case VERI_SYS_CALL_DIST_EXPONENTIAL:
    case VERI_SYS_CALL_DIST_POISSON:
    case VERI_SYS_CALL_DIST_UNIFORM:
    case VERI_SYS_CALL_DIST_ERLANG:
    case VERI_SYS_CALL_DIST_NORMAL:
    case VERI_SYS_CALL_DIST_T:
    case VERI_SYS_CALL_RANDOM:
        return true;
    default:
        return false;
    }
}

bool 
VerificVerilogDesignWalker::isRandomSysTask(unsigned funcType)
{
    switch(funcType) {
    case VERI_SYS_CALL_DIST_CHI_SQUARE:
    case VERI_SYS_CALL_DIST_EXPONENTIAL:
    case VERI_SYS_CALL_DIST_POISSON:
    case VERI_SYS_CALL_DIST_UNIFORM:
    case VERI_SYS_CALL_DIST_ERLANG:
    case VERI_SYS_CALL_DIST_NORMAL:
    case VERI_SYS_CALL_DIST_T:
    case VERI_SYS_CALL_RANDOM:
        return true;
    default:
        return false;
    }
}

bool 
VerificVerilogDesignWalker::isSupportedSysTask(unsigned funcType)
{
    switch(funcType) {
    case VERI_SYS_CALL_DISPLAY:
    case VERI_SYS_CALL_FDISPLAY:
    case VERI_SYS_CALL_WRITE:
    case VERI_SYS_CALL_FWRITE:
    case VERI_SYS_CALL_FCLOSE:
    case VERI_SYS_CALL_FFLUSH:
    case VERI_SYS_CALL_READMEMB:
    case VERI_SYS_CALL_READMEMH:
    case VERI_SYS_CALL_STOP:
    case VERI_SYS_CALL_FINISH:
        return true;
    default:
        return false;
    }
}

bool
VerificVerilogDesignWalker::isControlSysTask(unsigned funcType)
{
    switch(funcType) {
    case VERI_SYS_CALL_STOP:
    case VERI_SYS_CALL_FINISH:
        return true;
    default:
        return false;
    }
}

bool
VerificVerilogDesignWalker::isReadmemX(unsigned funcType)
{
    switch(funcType) {
    case VERI_SYS_CALL_READMEMB:
    case VERI_SYS_CALL_READMEMH:
        return true;
    default:
        return false;
    }
}

bool
VerificVerilogDesignWalker::isFFlushOrFCloseSysTask(unsigned funcType)
{
    switch(funcType) {
    case VERI_SYS_CALL_FCLOSE:
    case VERI_SYS_CALL_FFLUSH:
        return true;
    default:
        return false;
    }
}

bool
VerificVerilogDesignWalker::isOutputSysTask(unsigned funcType)
{
    switch(funcType) {
    case VERI_SYS_CALL_DISPLAY:
    case VERI_SYS_CALL_FDISPLAY:
    case VERI_SYS_CALL_WRITE:
    case VERI_SYS_CALL_FWRITE:
        return true;
    default:
        return false;
    }
}


bool 
VerificVerilogDesignWalker::isDisabledSysTask(unsigned funcType)
{
    // $display $fdisplay $write $fwrite $fclose $fflush
    switch(funcType) {
    case VERI_SYS_CALL_DISPLAY:
    case VERI_SYS_CALL_FDISPLAY:
    case VERI_SYS_CALL_WRITE:
    case VERI_SYS_CALL_FWRITE:
    case VERI_SYS_CALL_FCLOSE:
    case VERI_SYS_CALL_FFLUSH:
        return true;
    default:
        return false;
    }
}

bool 
VerificVerilogDesignWalker::isNegativeNumber(unsigned oper, VeriExpression* expr)
{
    if (expr->IsConst() && (VERI_MIN == oper)) {
        return true;
    }
    return false;
}

bool 
VerificVerilogDesignWalker::isNamedBlock(VeriStatement* stmt)
{
    return (stmt && stmt->IsSeqBlock() && static_cast<VeriSeqBlock*>(stmt)->GetLabel());
}

bool 
VerificVerilogDesignWalker::isMixedEvent(VeriStatement* stmt)
{
    unsigned i  = 0;
    VeriExpression *eventExpr;
    unsigned eventType = 0;
    FOREACH_ARRAY_ITEM(stmt->GetAt(), i, eventExpr) {
        if (eventExpr) {
            if (eventExpr->GetEdgeToken()) {
                if (2 == eventType) {
                    return true;
                }
                eventType = 1;
            } else {
                if (1 == eventType) {
                    return true;
                }
                eventType = 2; }
        }
    }
    return false;
}
    
bool 
VerificVerilogDesignWalker::isCombAlways(VeriStatement* stmt)
{
    // Degenerate case (empty always) is NULL stmt
    if (stmt) {
      unsigned i  = 0;
      VeriExpression *eventExpr;
      FOREACH_ARRAY_ITEM(stmt->GetAt(), i, eventExpr) {
          // mixed events considered to be non-combinational
          if (eventExpr && eventExpr->GetEdgeToken()) {
              return false; 
          }
      }
    }
  
    return true;
}

UtString VerificVerilogDesignWalker::composePrefixName(const UtString& moduleName)
{
    V2NDesignScope * currentScope = gScope();
    NUScope* nuScope = 0; 
    if (currentScope->gDeclarationScope()) {
        nuScope = dynamic_cast<NUScope*>(currentScope->gDeclarationScope());
    } else {
        nuScope = dynamic_cast<NUScope*>(currentScope->gOwner());
    }
    INFO_ASSERT(nuScope, "Incorrect scope type");
    UtVector<UtString> hierPath;
    UtString name(nuScope->getName()->str());
    while (name != moduleName) {
        hierPath.push_back(name);
        currentScope = currentScope->gParentScope();
        if (currentScope->gDeclarationScope()) {
            nuScope = dynamic_cast<NUScope*>(currentScope->gDeclarationScope());
        } else {
            nuScope = dynamic_cast<NUScope*>(currentScope->gOwner());
        }
        name = UtString(nuScope->getName()->str());
    }
    std::reverse(hierPath.begin(), hierPath.end());
    UtString prefixName(moduleName.c_str());
    UtVector<UtString>::iterator it = hierPath.begin();
    while (it != hierPath.end()) {
        prefixName << ".";
        prefixName << it->c_str();
        ++it;
    }
    return prefixName;
}

// Construct a UtString containing the full hierarchical reference represented by a VeriSelectedName.
UtString
VerificVerilogDesignWalker::composeSelectedName(const VeriSelectedName* selName)
{
  UtString sn;
  if (selName) {
    VeriName* pref = selName->GetPrefix();
    if (ID_VERIIDREF == pref->GetClassId()) {
      sn = UtString(gVisibleName(pref, pref->GetName())) + "." + gVisibleName(const_cast<VeriSelectedName*>(selName), selName->GetSuffix());
    }
    else
      sn = composeSelectedName(dynamic_cast<VeriSelectedName*>(pref)) + "." + gVisibleName(const_cast<VeriSelectedName*>(selName), selName->GetSuffix());
  }

  return sn;
}

// This method allows callers to determine whether a particular line of RTL source
// code has been visited before. This is useful when a particular action (e.g. an
// error message) is to be performed only once per line of RTL, versus per every
// instance (e.g. in the case of multiple module instantiations).
//
// It works by concatenating a name (for example a wire/var name) with
// the RTL file name, and the RTL line number. This string is stored in
// a container. The method returns true if the string is found, false
// otherwise. In the latter case, the string is added to the container.
bool
VerificVerilogDesignWalker::previouslyVisited(const char* name, const SourceLocator* loc)
{
  bool found = true;
  UtString unique_name = UtString(name) + "/" + loc->getFile() + "/" + loc->getLine();
  if (mPreviouslyVisited.insertWithCheck(unique_name))
    found = false;

  return found;
}

void 
VerificVerilogDesignWalker::addPragmas(UtString* netName, UtString* moduleName, Map* pragmas, Map* lineinfos, SourceLocator* loc)
{
    unsigned line = loc->getLine();
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(moduleName, *loc, "Prefix name should always exist");
    // walking attributes for current net
    MapIter it ;
    char * value = 0;
    char * name = 0;
    UtString prefixName = composePrefixName(*moduleName);
    FOREACH_MAP_ITEM(pragmas, it, &name, &value) {
        UtString stripValue(value);
        if ((stripValue.size() > 1) && ('"' == stripValue[0]) && ('"' == stripValue[stripValue.size() - 1])) {
            // remove leading and trailing quotes if they exist (TODO check if we will need this)
            stripValue = stripValue.substr(1, stripValue.size() - 2);
        }
        unsigned pragmaLine = Strings::atoul((const char*)lineinfos->GetValue(name));
        bool isDeclLine = (line == pragmaLine);
        SourceLocator pragmaLoc = *loc; 
        if (! isDeclLine) {
            // need to recreate source location
            pragmaLoc = mNucleusBuilder->gSourceLocFactory().create(loc->getFile(), pragmaLine);
        }
        if (mNucleusBuilder->isKnownDirective(UtString(name))) {
            if (mNucleusBuilder->isModuleDirective(UtString(name))) {
                mNucleusBuilder->addDirective(name, stripValue, *moduleName, 0, pragmaLoc); 
            } else if (netName) {
                // If netName is NULL, then we expect a module directive, but we can get a net directive.
                // This can happen if a directive is placed on a port within a module decl.
                // E.G. module foo (a // carbon observeSignal,
                //                  b, c, d);
                // The Interra flow silently ignores these. So we do too, though it might
                // be friendlier to issue a warning.
                mNucleusBuilder->addDirective(name, stripValue, prefixName, *netName, pragmaLoc); 
            }
        } else {
            getMessageContext()->Verific2NUUnrecognizedPragma(&pragmaLoc, name);
        }
    }
}


//RJC RC#2013/08/25 (frunze)
// what is this trying to return? the argument that has the most dimensions or the argument that has the widest declaration range?
// ADDRESSED FD: gMaxRange function returns widest range between two given, gMaxRangeDataType function returns VeriDataType* which has widest range (from the given two) 
VeriDataType* 
VerificVerilogDesignWalker::gMaxRangeDataType(VeriDataType* first, VeriDataType* second)
{
    // TODO it seems after switching to new API and depending on the decision on test/rushc/verilog/beacon_statemnt/IF16 case, we might not need this and gMaxRange methods at all, so consider removal of this methods (as well analogues from VerificVerilogSTBuildWalker)
    // TODO if we keep this method then it needs support for VERI_SHORTREAL
    if (first && second) {
        VeriRange* firstRange = NULL;
        bool firstHasTemp = false;
        // The list of types that we look for here are identified in the 1364-2005 LRM, section 3.2.1 (net declaration sytax), pg 20,
        // and section 3.2.2 (variable declaration syntax), pg 22.
        if (first->GetType() == VERI_INTEGER) {
            firstRange = new VeriRange(new VeriIntVal(31), new VeriIntVal(0));
            firstRange->SetLinefile(first->Linefile());
            firstHasTemp = true;
        } else if ((first->GetType() == VERI_TIME) || (first->GetType() == VERI_REAL) || (first->GetType() == VERI_REALTIME)) {
            firstRange = new VeriRange(new VeriIntVal(63), new VeriIntVal(0));
            firstRange->SetLinefile(first->Linefile());
            firstHasTemp = true;
        } else {
            firstRange = first->GetDimensions(); // TODO this will not work for base type typedefs, do we really need outermost packed dimension only ? I wonder whether or not there is some SV test which checks this. 
        }
        VeriRange* secondRange = NULL;
        bool secondHasTemp = false;
        // The list of types that we look for here are identified in the 1364-2005 LRM, section 3.2.1 (net declaration sytax), pg 20,
        // and section 3.2.2 (variable declaration syntax), pg 22.
        if (second->GetType() == VERI_INTEGER) {
            secondRange = new VeriRange(new VeriIntVal(31), new VeriIntVal(0));
            secondRange->SetLinefile(second->Linefile());
            secondHasTemp = true;
        } else if ((second->GetType() == VERI_TIME) || (second->GetType() == VERI_REAL) || (second->GetType() == VERI_REALTIME)) {
            secondRange = new VeriRange(new VeriIntVal(63), new VeriIntVal(0));
            secondRange->SetLinefile(second->Linefile());
            secondHasTemp = true;
        } else {
            secondRange = second->GetDimensions();
        }
        VeriDataType* maxDataType = NULL;
        if (gMaxRange(firstRange, secondRange) == firstRange) {
            maxDataType = first;
        } else {
            maxDataType = second;
        }
        if (firstHasTemp) {
            delete firstRange;
        }
        if (secondHasTemp) {
            delete secondRange;
        }
        return maxDataType;
    } else if (first && !second) {
        return first;
    } else if (!first && second) {
        return second;
    } else {
        return NULL; // both first and second were null
    }
}

VeriRange* 
VerificVerilogDesignWalker::gMaxRange(VeriRange* first, VeriRange* second)
{
    if (!first && second) {
        return second;
    } else if (first && !second) {
        return first;
    } else if (!first && !second) {
        return 0;
    } else {
        UtPair<int,int> f;
        gRange(&f, *first, second->RangeWidth(), false, "", 0, 0);
        /// gRange returns error for dynamic range don't continue if error occurred (otherwise at risk of getting range consistency assertions in nucleus)
        if (hasError()) return 0;
        UtPair<int,int> s;
        gRange(&s, *second, second->RangeWidth(), false, "", 0, 0);
        if (hasError()) return 0;
        ConstantRange cr1(f.first, f.second);
        ConstantRange cr2(s.first, s.second);
        // if equal second (latest declaration) will be returned
        if (ConstantRange::getMax(&cr2, &cr1) == &cr1) {
            return first;
        } else {
            return second;
        }
    }
}

// gRange (getRange)
// Take information from the VeriRange 'range' argument and and fill in the 'dim' argument with the two (assumed to be constant) values.
// This method assumes that both the left and right terms of range can be evaluated as constants.
// This method also includes support for verilog 2001 syntax '+:' and  '-:' which is converted to the correct range with respect to declared
// range (and seems this isn't carried over by ConstantRange::normalize function, so have to adjust it here).
// 

//RJC RC#2013/08/25 (frunze)
// from the above documentation I still don't know what width, isDeclaration and declRange are needed for or what they are.  I am just
//guessing that declRange is the declared range of the same vector that the 'range' argument refers to?  If so then why do we need the width
//argument, is it not the same as the width of declRange?
// Reading the code it seems that if isDeclaration is true then width and the declRange are used to adjust the returned dimension pair so
//that they somehow 'match' the range in declRange?  My reading suggests that the 'isDeclaration' argument means that the returned dimension
//pair is adjusted (or normalized) so that it matches the declared range defined by declRange.
//ADDRESSED FD
// gRange() - is a generic range handling (verific has visitor for it, but as far nucleus doesn't have object associated with (descent of NUBase*) we can't use ordinary visitor hierarchy. So this function is created as an alternative to visitor
// gRange() is called in 2 different contexts 1) during net/variable declaration and 2) during index access in expressions. isDeclaraton flag just points whether it is called in declaration context or in indexing context 
// width - is a declared width of the object (net).  As nucleus works correctly with normalized to [M:0] form ranges,, we have to normalize range before passing it to statemnts and expressions like varsel/memsel. For that purpose we need original declaration width. (used in indexing context not in declaration)
// declRange - is a original declared pair of MSB : LSB for current net (again this is for indexing context not in declaration) 
// 'isDeclaration' renamed to 'normalize' (as currently there are other than declaration cases where we don't need normalization
// TODO Here we use a bit custom algorithm (differs from Interra) to get normalized ranges (it comes historically), if we found that it's failing will switch to interra's approach (it will guarante as well no difference in any case)

//RJC RC#2013/08/25 (frunze)
// Note this method fails on an assignment like the following because the first index does not eval to a constant:
//  reg [7:0] i;
//  reg [31:0] vec;
//  ...
//   vec[i*4 +: 4] = expression;
// ADDRESSED FD
// Support for that case already added to nucleus builder api nad VeriIndexedId verific visitor and also handled here in "+:, -: verilog 2001 non-constant case" section
// Below is solution descripotion: 
// Expressions with 2001 range select with non-constant index like this - vec[i*4 +: 4], 
// in nucleus get populated with combination of 2 components constantRange (e.g. [3:0] normalized) and indexExpr (e.g. i*4 normalized) . Nucleus has varsel and memsel corresponding API's for that (Varsel is already added, memsel will come along with memories support)
//memsel part still needs enhancement VeriMemoryId visitor should be implemented for memories 
void 
VerificVerilogDesignWalker::gRange(UtPair<int, int>* dim, const VeriRange& range, unsigned width, bool normalize, const UtString& netName, const UtPair<int,int>* const declRange, int* adjust, const SourceLocator* const loc)
{
    bool isLeftConst = false;
    bool ok = true;
    // Get MSB of current range 
    int left = 0;
    verific_int64 lvalBig = 0;
    if (range.GetLeft()) {
        VeriBaseValue* lVal = range.GetLeft()->StaticEvaluate(0, 0, 0, 1);
        //first component might not be constant
        if (lVal) {
            isLeftConst = true;
            lvalBig = lVal->Get64bitInteger();
            if (lvalBig > std::numeric_limits<int>::max()) {
                left = 0;
                ok = false;
            } else {
                left = lVal->GetIntegerValue();
            }
            delete lVal;
        }
    }
    // Get LSB of current range
    int right = 0;
    verific_int64 rvalBig = 0;
    if (range.GetRight()) {
        VeriBaseValue* rVal = range.GetRight()->StaticEvaluate(0, 0, 0, 1);
        // second component should be always constant
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(rVal, range, "Incorrect constant range right bound");
        if (rVal) {
            rvalBig = rVal->Get64bitInteger();
            if (rvalBig > std::numeric_limits<int>::max()) {
                right = 0;
                ok = false;
            } else {
                right = rVal->GetIntegerValue();
            }
            delete rVal;
        }
    }
    // separate constant index case
    bool isConstIndex = false;
    if ((VERI_PARTSELECT_UP != range.GetPartSelectToken()) && (VERI_PARTSELECT_DOWN != range.GetPartSelectToken())) {
        isConstIndex = true;
    }


    // check for unsupported constant range (bigger than int max value)
    if (isConstIndex && (VERI_PARTSELECT_UP != range.GetPartSelectToken()) && (VERI_PARTSELECT_DOWN != range.GetPartSelectToken())
        && !ok && loc) {
        getMessageContext()->UnsupportedRange(loc, lvalBig, rvalBig);
        setError(true);
    }
    // check for queues and other dynamic arrays that we do not support
    if (range.IsDynamicRange()) {
        reportUnsupportedLanguageConstruct(*loc, "SystemVerilog queues and dynamic arrays");
        return;
    }

    ConstantRange* declaredRange = NULL;
    ConstantRange* parsedRange = NULL;
    if (!isConstIndex) { 
        declaredRange = new ConstantRange(declRange->first, declRange->second);
        if (!isLeftConst) {
            //// Handle +:, -: verilog 2001 part select non-constant case
            parsedRange = new ConstantRange (right-1, 0);
            *adjust = (declaredRange->bigEndian()) ^ (VERI_PARTSELECT_UP == range.GetPartSelectToken()) ?  parsedRange->getLength() -1 : 0;
            dim->first = parsedRange->getMsb();
            dim->second = parsedRange->getLsb();
        } else {
            //TODO merge with normalize change
            //// Handle +:, -: verilog 2001 part select constant case
            if (declaredRange->bigEndian()) {
                if (VERI_PARTSELECT_UP == range.GetPartSelectToken()) {
                    parsedRange = new ConstantRange(left + right - 1, left);
                } else {
                    parsedRange = new ConstantRange(left, left - right + 1);
                }
            } else {
                if (VERI_PARTSELECT_UP == range.GetPartSelectToken()) {
                    // TODO need to check other branches of '+:', '-:' part handling for normalize, currently it always does normalization based on Interra solution - localflow/VerilogPopulate.cxx:725 statement
                    // SV specific cases where we have memory selections with resulting part also memory shouldn't be normalized like bit references do, and so above TODO should be addressed by adding corresponding tests to test/sv/MultiDim
                    if (normalize) {
                        parsedRange = new ConstantRange(width - 1 - left, width - 1 - left - right + 1);
                    } else {
                        parsedRange = new ConstantRange(left, left + right - 1);
                    }
                } else {
                    parsedRange = new ConstantRange(width - 1 - left + right - 1, width - 1 - left);
                }
            }
            if (normalize) {
                // don't check parsedRange vs declaredRange check in here as they were normalized (changed direction from orignal) but net is populated as it is and so we will get fase error)
                dim->first = parsedRange->getMsb();
                dim->second = parsedRange->getLsb();
            } else {
                parsedRange = pruneRangeToOverlappingRange(declaredRange, parsedRange, netName, loc);
                if (checkParsedRangeVsDeclaredRange(declaredRange, parsedRange, netName, loc)) {
                    dim->first = parsedRange->getMsb();
                    dim->second = parsedRange->getLsb();
                }
            }
        }
    } else {
        // Handle constant single value range [N]
        if (range.IsSingleValueRange()) {
            dim->first = 0;
            dim->second = left - 1;
        // Handle constant index case [N:M]
        } else {
            dim->first = left;
            dim->second = right;
        }
        if (normalize) {
            declaredRange = new ConstantRange(declRange->first, declRange->second);
            parsedRange = new ConstantRange(dim->first, dim->second);
            // check for parsedRange vs declaredRange consistency here as we need todo normalization here
            parsedRange = pruneRangeToOverlappingRange(declaredRange, parsedRange, netName, loc);
            if (checkParsedRangeVsDeclaredRange(declaredRange, parsedRange, netName, loc)) {
                if ((declRange->first > declRange->second) && (left < right) ) {
                    dim->first = width - 1 - dim->first;
                    dim->second = width - 1 - dim->second;
                } 
                // normalize
                parsedRange->normalize(declaredRange, false);
                dim->first = parsedRange->getMsb();
                dim->second = parsedRange->getLsb();
            }
        }
    }
    delete declaredRange;
    delete parsedRange;
}


unsigned
VerificVerilogDesignWalker::gMaxCaseItemExprSize(const VeriCaseItem& caseItem)
{
    unsigned maxSize = 0; 
    unsigned j ;
    VeriExpression *condition = NULL;
    FOREACH_ARRAY_ITEM(caseItem.GetConditions(), j, condition) {
        maxSize = std::max(maxSize, static_cast<unsigned>(std::abs(condition->StaticSizeSign())));
    }
    return maxSize;
}


unsigned
VerificVerilogDesignWalker::gMaxCaseExprSize(const VeriCaseStatement& caseStatement)
{
    unsigned maxSize = 0;
    VeriExpression* condition = caseStatement.GetCondition();
    maxSize = std::max(maxSize, static_cast<unsigned>(std::abs(condition->StaticSizeSign())));
    unsigned i ;
    VeriCaseItem *ci ;
    FOREACH_ARRAY_ITEM(caseStatement.GetCaseItems(), i, ci) {
        maxSize = std::max(maxSize, gMaxCaseItemExprSize(*ci));
    }
    return maxSize;
}

bool
VerificVerilogDesignWalker::evalIntValue(VeriExpression* rightExpr, int* intValue)
{
    if (rightExpr && rightExpr->IsConstExpr()) {
        VeriBaseValue* value = rightExpr->StaticEvaluate(0, 0, 0, 1);
        if (value) {
            *intValue = value->GetIntegerValue(); 
            delete value;
            return true; 
        }
    }
    return false;
}

// examine rightExpr, if it is a constant then return a ptr to a VeriExpression that represents the properly sized value of that constant
// there is special handling for unsized constants such as 'h100000004 (requires at least 33 bits).  The LRM states that such constants have
// the size of an integer if they are used in a self-determined context.
VeriExpression*
VerificVerilogDesignWalker::evalConstExpr(int contextSizeSign, VeriExpression* rightExpr)
{
    // Don't evaluate constant functions to constants as they may contain side-effect statements like $display
    // which will result to simulation log diffs.
    // Also, avoid calling staticEvaluate on assignment patterns. Rather, let the VeriConcat and VeriMultiConcat
    // visitors deal with them. Otherwise, staticEvaluate returns incorrect values for multi-d assignment
    // patterns. More investigation is required - it may just be that we're not interpreting the return value
    // correctly. (TODO: Are their implications for constant propagation here?)
    if (rightExpr && rightExpr->IsConstExpr() && !rightExpr->IsFunctionCall() && !rightExpr->IsAssignPattern() && !rightExpr->IsMultiAssignPattern()) {
        VeriBaseValue* value = rightExpr->StaticEvaluate(0, 0, 0, 1);
        if (value) {
            bool isSelfDeterminedContext = (0 == contextSizeSign);
            int exprSizeSign = value->StaticSizeSign();
            unsigned context_size = GET_CONTEXT_SIZE(exprSizeSign);
            if (isSelfDeterminedContext && rightExpr->IsUnsizedConst() && (context_size > 32 ) ){
              // LRM says unsized constants are truncated to size of int if used in a self determined context, so first warn user, then set evaluation size to 32
              SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,rightExpr->Linefile());
              getMessageContext()->TruncateUnsizedConstant(&sourceLocation, "a self-determined", context_size);
              unsigned context_sign = GET_CONTEXT_SIGN(exprSizeSign);
              exprSizeSign = MAKE_CONTEXT_SIZE_SIGN(32,context_sign); // force a size of 32 bits, the size used for integer
            }
            delete value;
            unsigned contextSize = std::max(GET_CONTEXT_SIZE(contextSizeSign), GET_CONTEXT_SIZE(exprSizeSign));
            contextSizeSign = MAKE_CONTEXT_SIZE_SIGN(contextSize, GET_CONTEXT_SIGN(exprSizeSign));
            return rightExpr->StaticEvaluateToExpr(contextSizeSign, 0, 1);
        }
    }
    return NULL;
}

bool
VerificVerilogDesignWalker::gFormalName(VeriTreeNode* node, UtString* formalName)
{
    VeriPortConnect* vpc = dynamic_cast<VeriPortConnect*>(node);
    VeriIdDef* id = dynamic_cast<VeriIdDef*>(node);
    VeriExpression* expr = dynamic_cast<VeriExpression*>(node);
    if (vpc) {
        *formalName = UtString(vpc->GetNamedFormal());
    } else if (id) {
        *formalName = UtString(id->GetName());
    } else if (expr) {
        VeriIdDef* formalId = fActualRefId(expr);
        if (formalId) {
            *formalName = UtString(formalId->GetName());
        } else {
            // Unable to find formal name
            // 'formalName' argument value unchanged
            return false;
        }
    } else {
        // Unable to find formal name
        // 'formalName' argument value unchanged
        return false;
    }
    return true;
}

VeriExpression*
VerificVerilogDesignWalker::fActualRefExpr(VeriExpression* vpc)
{
    VeriExpression* portRef = vpc;
    if (portRef->GetConnection()) {
        //  if this is an port connection, then take actual expression
        portRef = portRef->GetConnection();
    }
    if (portRef->GetPrefix()) {
        // if this is an indexed expression, then take prefix
        portRef = portRef->GetPrefix();
    }
    return portRef;
}

VeriIdDef*
VerificVerilogDesignWalker::fActualRefId(VeriExpression* vpc)
{
    VeriExpression* portRef = fActualRefExpr(vpc);
    return portRef->GetId();
}

bool
VerificVerilogDesignWalker::portIsRedeclared(VeriExpression* vpc, NUNet* p, UtMap<NUBase*, bool> alreadyAddedPorts)
{
    bool isWholeIdentifier = true;
    if (vpc->GetConnection()) {
        //  if this is an port connection, then take actual expression
        isWholeIdentifier = true;
    }
    if (vpc->GetPrefix()) {
        // if this is an indexed expression, then take prefix
        isWholeIdentifier = false;
    }
    if (alreadyAddedPorts.find(p) != alreadyAddedPorts.end()) {
        bool isAlreadyAddedPortWholeIdentifier = alreadyAddedPorts[p];
        if (isWholeIdentifier && isAlreadyAddedPortWholeIdentifier) {
            return false; 
        }
    }
    return true;
}

NUBase*
VerificVerilogDesignWalker::findNet(VeriIdDef* netId)
{
    if (netId) {
        if (mDeclarationRegistry.find(netId) != mDeclarationRegistry.end()) {
            return mDeclarationRegistry[netId];
        }
    }
    return 0;
}

UtString
VerificVerilogDesignWalker::createNucleusString(const UtString& data)
{
    return mNucleusBuilder->createNucleusString(data);
}

void 
VerificVerilogDesignWalker::cOutputSysTask(VeriSystemTaskEnable* node, NUModule* module, const UtString& name, const SourceLocator& sourceLocation)
{
    bool isDisplay = ( VERI_SYS_CALL_DISPLAY  == node->GetFunctionType() );
    bool isFDisplay = ( VERI_SYS_CALL_FDISPLAY == node->GetFunctionType() );
    bool isWrite = ( VERI_SYS_CALL_WRITE == node->GetFunctionType() );
    bool isFWrite = ( VERI_SYS_CALL_FWRITE == node->GetFunctionType() );
    bool hasFileArg = ( isFDisplay || isFWrite );
    bool addNewline = ( isDisplay || isFDisplay);
    bool canHaveInstName = ( isDisplay || isFDisplay || isWrite || isFWrite );
    NUExprVector expr_vector;
    // if a file arg is required then make sure the first arg satisifies
    // the requirements that it not be null and it is 32 bits.
    bool argMustBeFD = hasFileArg; // note this variable is unconditionally set to
    // false for all but the first argument.

    // Assume there is no %m. We search for it in the expressions below.
    bool hasInstanceName = false;

    STBranchNode* instanceWhere = NULL; // module scope
    // Get expressions
    NUExprVector exprVector;
    if (node->GetArgs()) {
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(node->GetArgs(), i, expr) {
            NUExpr* rvalue = 0;
            if (!expr) {
                // check first argument
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(!argMustBeFD, *node, "The first argument of $fdisplay or $fwrite must not be null.");
                // create null expression
                rvalue = new NUNullExpr(sourceLocation);      // null expression found in list
            } else {
                gScope()->sContext(V2NDesignScope::RVALUE);
                VeriExpression* evalRvalue = evalConstExpr(0, expr); // each arg of $display is self-determined
                if (evalRvalue) {
                    evalRvalue->Accept(*this);
                    delete evalRvalue;
                } else {
                    expr->Accept(*this) ;
                }
                rvalue = dynamic_cast<NUExpr*>(gScope()->gValue());
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(rvalue, *node, "Unable to get OutputSysTask expression");

                int selfDeterminedWidth = rvalue->determineBitSize();
                if ( argMustBeFD ){
                    if ( selfDeterminedWidth != 32 ) {
                        // warn user (using argument positions starting at 1)
                        getMessageContext()->MCDShouldBe32(&sourceLocation, i+1,
                                ((selfDeterminedWidth>32)?"truncated":"extended"),
                                selfDeterminedWidth);
                    }
                    // always resize to 32 bits
                    rvalue = rvalue->makeSizeExplicit(32);
                } else {
                    rvalue->resize(selfDeterminedWidth);
                }
            }
            mNucleusBuilder->aExprToExprVector(rvalue, exprVector);

            if (canHaveInstName && !hasInstanceName) {
                NUConst* constExpr =  rvalue->castConstNoXZ ();
                if (constExpr != NULL
                        && constExpr->isDefinedByString()) {
                    UtString formatstring;
                    // the following uses composeNoProtected since we need the actual
                    // string even if it was `protected
                    constExpr->composeNoProtected(&formatstring, NULL, false, false);
                    UtString::size_type nextPercentPos=0;
                    // Check if we have a %<digits>m in a format string for display and its
                    // variants. We don't need to check for it if we already found
                    // one.
                    while((nextPercentPos = formatstring.find ("%", nextPercentPos)) != UtString::npos) {
                        // At least one format effector...
                        // skip over all the decorations...
                        UtString::size_type formatPos = formatstring.find_first_of ("%bcdefghlmostx",
                                nextPercentPos+1);
                        if (formatPos == UtString::npos)
                            break;              // no  more formats found in string
                        if (formatstring[formatPos] == 'm') {
                            hasInstanceName = true;
                            break;              // we've seen enough...
                        }

                        // Start looking after the last format char
                        nextPercentPos = formatPos+1;
                    }

                    // Find the nearest named declaration scope above us, if any
                    NUScope* declaration_scope = getDeclarationScope();
                    if (NUScope::eNamedDeclarationScope == declaration_scope->getScopeType()) {
                        // we are within is a named declaration scope, so need to
                        // save the path to current $display
                        instanceWhere = NUElabHierHelper::findUnelabBranchForScope(module, declaration_scope);
                    }
                }
            }
            argMustBeFD = false; // reset only first argument should be checked
        }
    }
    NUBase* outputSysTask = mNucleusBuilder->cOutputSysTask(name, exprVector, module, hasFileArg, addNewline, true, false, 
            hasInstanceName, instanceWhere, sourceLocation);
    if (mIsCombAlways) mHasSideEffectStatement = true; // this will cause always block to generate changeNet logic, has sense only for combinational always blocks
    gScope()->sValue(outputSysTask);
}


void
VerificVerilogDesignWalker::cControlSysTask(VeriSystemTaskEnable* node, NUModule* module, const UtString& name, const SourceLocator& sourceLocation)
{
    // Get expressions
    NUExprVector exprVector;
    if (node->GetArgs()) {
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(node->GetArgs(), i, expr) {
            if (!expr) continue ;
            gScope()->sContext(V2NDesignScope::RVALUE);
            VeriExpression* evalRvalue = evalConstExpr(0, expr);
            if (evalRvalue) {
                evalRvalue->Accept(*this);
                delete evalRvalue;
            } else {
                expr->Accept(*this) ;
            }
            NUExpr* iExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(iExpr, *node, "Unable to get OutputSysTask expression");
            mNucleusBuilder->aExprToExprVector(iExpr, exprVector);
        }
    }
    if ( exprVector.empty() ) {
        // no arg defined, create a constant 1 as the default 
        NUExpr* verboseExpr = NUConst::create(false, 1,  32, sourceLocation);
        exprVector.push_back(verboseExpr);
    }
    NUControlSysTask* controlSysTask = mNucleusBuilder->cControlSysTask(name, node->GetFunctionType(), exprVector, module, sourceLocation);
    gScope()->sValue(controlSysTask);
}

void
VerificVerilogDesignWalker::cReadmemXSysTask(VeriSystemTaskEnable* node, NUModule* module, const UtString& name, const SourceLocator& sourceLocation)
{
    bool useHexFormat = ( VERI_SYS_CALL_READMEMH == node->GetFunctionType() );
    NUExprVector dummyExprVector; // for now a placeholder
    // there is defined ordering for readmemb readmemh arguments (filename, memoryNetExpr [, startAddress, endAddress]), last 2 arguments (startAddress, endAddress) are optional
    NUExpr* filename = 0;
    NULvalue* memoryNetLvalue = 0;;
    NUExpr* startAddress = 0;
    NUExpr* endAddress = 0;
    if (node->GetArgs()) {
        unsigned i ;
        VeriExpression *expr ;
        VeriExpression *evalRvalue = 0;
        FOREACH_ARRAY_ITEM(node->GetArgs(), i, expr) {
            if (!expr) continue ;
            switch (i) {
            case 0:
                // get filename
                gScope()->sContext(V2NDesignScope::RVALUE);
                evalRvalue = evalConstExpr(0, expr);
                if (evalRvalue) {
                    evalRvalue->Accept(*this);
                    delete evalRvalue;
                    evalRvalue = 0;
                } else {
                    expr->Accept(*this) ;
                }
                filename = dynamic_cast<NUExpr*>(gScope()->gValue());
                break;
            case 1:
                // get Lvalue for memory net
                gScope()->sContext(V2NDesignScope::LVALUE);
                evalRvalue = evalConstExpr(0, expr);
                if (evalRvalue) {
                    evalRvalue->Accept(*this);
                    delete evalRvalue;
                    evalRvalue = 0;
                } else {
                    expr->Accept(*this) ;
                }
                memoryNetLvalue = dynamic_cast<NULvalue*>(gScope()->gValue()); 
                break;
            case 2:
                // get memory net start address (optional)
                gScope()->sContext(V2NDesignScope::RVALUE);
                evalRvalue = evalConstExpr(0, expr);
                if (evalRvalue) {
                    evalRvalue->Accept(*this);
                    delete evalRvalue;
                    evalRvalue = 0;
                } else {
                    expr->Accept(*this) ;
                }
                startAddress = dynamic_cast<NUExpr*>(gScope()->gValue());
                break;
            case 3:
                // get memory net end address (optional)
                gScope()->sContext(V2NDesignScope::RVALUE);
                evalRvalue = evalConstExpr(0, expr);
                if (evalRvalue) {
                    evalRvalue->Accept(*this);
                    delete evalRvalue;
                    evalRvalue = 0;
                } else {
                    expr->Accept(*this) ;
                }
                endAddress = dynamic_cast<NUExpr*>(gScope()->gValue());
                break;
            default:
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, *node, "Incorrect number of arguments");
            }
        }
    }
    // Check mandatory options existence
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(filename, *node, "Unable to get filename argument");
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(memoryNetLvalue, *node, "Unable to get memory net Lvalue expression");
    // Get memory net
    NUNet* net = memoryNetLvalue->getWholeIdentifier();
    NUMemoryNet* mem_net = net->getMemoryNet();

    // Get the optional arguments
    SInt64 startAddr;
    SInt64 endAddr;
    bool endSpecified = false;

    const UInt32 numMemDims = mem_net->getNumDims();
    if ( numMemDims != 2 ){
      // currently readmem* is only supported for 2 dim memories
      UtString errmsg;
      errmsg << name << " is only supported for memories with 1 packed and 1 unpacked dimension.";
      reportUnsupportedLanguageConstruct(sourceLocation, errmsg.c_str());
      return;                   // error condition
    }
    const ConstantRange* range = mem_net->getRange(1); // change this when support is added for arrays with more dimensions
    if (startAddress) {
        // Start address is available
        NUConst* startAddrConst = dynamic_cast<NUConst*>(startAddress);
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(startAddrConst, *node, "Bad start address");
        bool ok = startAddrConst->getLL(&startAddr);
        delete startAddrConst;
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(ok, *node, "Bad start address");
        if (endAddress) {
            // End address is available
            NUConst* endAddrConst = dynamic_cast<NUConst*>(endAddress);
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(endAddrConst, *node, "Bad end address");
            ok = endAddrConst->getLL(&endAddr);
            delete endAddrConst;
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(ok, *node, "Bad end address");
            endSpecified = true;
        } else {
            endAddr = range->getLsb();
        }
    } else {
        // Use the memories start and end address
        // TBD - range is a 32 bit constant range but I think we need 64 bits
        startAddr = range->getMsb();
        endAddr = range->getLsb();
    }

  // Get file name string
  UtString filenameBuf;
  filename->compose(&filenameBuf, 0, 0, false );
  UtString::size_type idx;
  // strip string quotes if any
  while ((idx = filenameBuf.find('"')) != UtString::npos)
    filenameBuf.erase(idx, 1);
  delete filename;
  NUBase* stmt = mNucleusBuilder->cReadmemXSysTask(dummyExprVector, module, memoryNetLvalue, filenameBuf, useHexFormat, startAddr, endAddr, endSpecified, sourceLocation);
  gScope()->sValue(stmt);
}

void
VerificVerilogDesignWalker::cFFlushOrFCloseSysTask(VeriSystemTaskEnable* node, NUModule* module, const UtString& name, const SourceLocator& sourceLocation)
{
    // Get expressions
    NUExprVector exprVector;
    if (node->GetArgs()) {
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(node->GetArgs(), i, expr) {
            if (!expr) continue ;
            gScope()->sContext(V2NDesignScope::RVALUE);
            VeriExpression* evalRvalue = evalConstExpr(0, expr);
            if (evalRvalue) {
                evalRvalue->Accept(*this);
                delete evalRvalue;
            } else {
                expr->Accept(*this) ;
            }
            NUExpr* iExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(iExpr, *node, "Unable to get OutputSysTask expression");
            mNucleusBuilder->aExprToExprVector(iExpr, exprVector);
        }
    }
    bool isClose = (VERI_SYS_CALL_FCLOSE == node->GetFunctionType());
    UtString errmsg;
    bool isvalid = false;
    NUBase* stmt = 0;
    // create sys task
    if ( isClose ) {
        NUFCloseSysTask* fclose = mNucleusBuilder->cFCloseSysTask(name, exprVector, module, sourceLocation);
        isvalid = fclose->isValid(&errmsg);
        stmt = fclose;
    } else {
        NUFFlushSysTask *fflush = mNucleusBuilder->cFFlushSysTask(name, exprVector, module, sourceLocation);
        isvalid = fflush->isValid(&errmsg);
        stmt = fflush;
    }
    // check for errors
    if ( not isvalid )
    {
        delete stmt;
        stmt = 0; 
        getMessageContext()->InvalidSysFuncArg(&sourceLocation, name.c_str(), errmsg.c_str());
        setError(true);
    }
    gScope()->sValue(stmt);
}

bool VerificVerilogDesignWalker::doingOutputSysTasks(NUModule* module)
{
    bool doingOutputSysTasks = mEnableOutputSysTasks; 
    if ( doingOutputSysTasks ){
        doingOutputSysTasks = (not mNucleusBuilder->gIODB()->isDisableOutputSysTasks(mNucleusBuilder->gOriginalModuleName(module)) );
    } else {
        doingOutputSysTasks = (mNucleusBuilder->gIODB()->isEnableOutputSysTasks(mNucleusBuilder->gOriginalModuleName(module)) );
    }
    return doingOutputSysTasks;
}

bool VerificVerilogDesignWalker::doInlineTasks()
{
    return mInlineTasks;
}

// returns true if the operand is self determined.  Based on operator _opType_ and operand _opIndex_ (1 based)
// if isUnaryOperation argument is true then use the unary interpretation of the opType (verific uses some VERI_* codes for both unary and binary operations)
bool
VerificVerilogDesignWalker::isOperandSelfDetermined(unsigned opType, bool isUnaryOperation, int opIndex){
  switch (opType) {
  case VERI_LOGNOT:
  case VERI_REDAND:
  case VERI_REDOR:
  case VERI_REDXOR:
  case VERI_REDNAND:
  case VERI_REDNOR:
  case VERI_REDXNOR:
    if ( isUnaryOperation ){
      return ( opIndex == 1 );  // only one operand possible, make sure caller is asking about it
    } else {
      // using binary equivalent
      return false;   // neither operand is self determined
    }

  case VERI_LOGAND:
  case VERI_LOGOR:
  case VERI_LOG_EQUIVALENCE:
  case VERI_RIGHTARROW:
  case VERI_LT:
  case VERI_LEQ:
  case VERI_GT:
  case VERI_GEQ:
    return ((opIndex == 1) || (opIndex == 2));   // both operands are self determined, make sure caller asked about it

  case VERI_RSHIFT:
  case VERI_LSHIFT:
  case VERI_ARITLSHIFT:
  case VERI_ARITRSHIFT:
  case VERI_POWER:
    return (opIndex == 2);      // only the second operand is self determined

//  case VERI_QUESTIONCOLON:  // this one is handled directly since there is not a single operator type
//    return (opIndex == 0);      // only the first operand is self determined
    
  default:
    return false;
  }
}

void
VerificVerilogDesignWalker::SpecifyBlockImplicitConnection(VeriExpression* lhs, VeriExpression* rhs, VeriSystemTimingCheck& node)
{
  // Get current node source location 
  SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  // We allow idrefs, and indexed idrefs, nothing else. This matches Interra flow
  if (rhs && lhs 
      && (rhs->IsIdRef() || rhs->GetIndexExpr()) 
      && (lhs->IsIdRef() || lhs->GetIndexExpr())) {
    // Sizes must match
    int rhs_size_sign = rhs->StaticSizeSign();
    int lhs_size_sign = lhs->StaticSizeSign();
    unsigned rhs_size = GET_CONTEXT_SIZE(rhs_size_sign);
    unsigned lhs_size = GET_CONTEXT_SIZE(lhs_size_sign);
    if (rhs_size != lhs_size) {
        UtString vName = gVisibleName(lhs, lhs->GetName());
        const char* name = vName.c_str();
        getMessageContext()->ImplicitConnWidthMismatch(&sourceLocation, name);
    }
    else {
      // Retrieve array of continuous assigns, and add to it
      UtVector<NUBase*> contAssignVector;
      gScope()->gValueVector(contAssignVector);
      // sizes match, use context_size_sign of lhs
      unsigned savedContextSizeSign = gScope()->updateContextSizeSign(lhs_size_sign);
      gScope()->sContext(V2NDesignScope::LVALUE);
      lhs->Accept(*this);
      NULvalue* left = dynamic_cast<NULvalue*>(gScope()->gValue());
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(left, node, "Unable to get left expression");
      (void) gScope()->updateContextSizeSign(lhs_size_sign);
      gScope()->sContext(V2NDesignScope::RVALUE);
      rhs->Accept(*this);
      NUExpr* right = dynamic_cast<NUExpr*>(gScope() -> gValue()); 
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(right, node, "Unable to get right expression");
      // Create Continuous Assign
      NUContAssign* contAssign = mNucleusBuilder->cContinuesAssign(getNUScope(&node), left, right, sourceLocation); 
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(contAssign, node, "Unable to create continuous assign");
      contAssignVector.push_back(contAssign);
      // Restore the array of continuous assigns
      gScope()->sValueVector(contAssignVector);
      (void) gScope()->updateContextSizeSign(savedContextSizeSign);//reset the context size sign back to value it had upon entry to this method
    }
  }
}

void
VerificVerilogDesignWalker::mapPort(NUScope* scope, VeriTreeNode* node, NULvalue* lvalue, const SourceLocator& sourceLocation)
{
    PortMap& nodeMap = mPortMap[scope];
    // May see this node->lvalue mapping several times for ranged
    // instantiations. (array of instances) Allow it, but make sure the
    // mapping is consistent.
    PortMap::iterator iter = nodeMap.find( node );
    if (( iter != nodeMap.end( )) and ( (*iter).second != lvalue )) {
        UtString portName("unknown");
        (void)gFormalName(node, &portName); 
        UtString vName = gVisibleName(node, portName);
        getMessageContext()->PortRedeclare(&sourceLocation, vName.c_str());
        return;
    }
    nodeMap[node] = lvalue;
}

void 
VerificVerilogDesignWalker::lookupPort(NUModule* module, VeriTreeNode* node, NUNet **the_net, bool *partial_port, ConstantRange* range, const SourceLocator& loc)
{
    ScopePortMap::const_iterator pm = mPortMap.find(module);
    UtString namebuf;

    bool found_name = false;
    UtString portName("");
    found_name = gFormalName(node, &portName);
    if ( not found_name  /*|| IsOpenPort(node)*/){
        *partial_port = false;
        *the_net = NULL;
        return;
    }


    if ( pm == mPortMap.end()) {
        if ( not found_name ){
            namebuf << "(unknown name)";
        }
        getMessageContext()->CouldNotFindPort( &loc, namebuf.c_str());
        return;
    }

    // TODO need to think about mixed language here
    //mvvLanguageType lang = mvvGetLanguageType(node);
    UtString vName = gVisibleName(node, portName);
    const char* name = vName.c_str();

    VeriTreeNode* nodeToLookup = node;

    const PortMap& nodeMap = pm->second;
    PortMap::const_iterator q = nodeMap.find( nodeToLookup );
    if ( q != nodeMap.end( )) {
        NULvalue *lvalue = q->second;
        if (lvalue->isWholeIdentifier()) {
            *the_net = lvalue->getWholeIdentifier();
        } else if ( lvalue->getType() == eNUVarselLvalue ) {
            NUVarselLvalue* varsel_lvalue = dynamic_cast<NUVarselLvalue*>(lvalue);
            *partial_port = true;
            const ConstantRange *lvalue_range = varsel_lvalue->getRange();
            range->setMsb(lvalue_range->getMsb());
            range->setLsb(lvalue_range->getLsb());
            *the_net = varsel_lvalue->getIdentNet();
        } else {
            *the_net = NULL;
        }
        NU_ASSERT((*the_net), lvalue);
    } else {
        getMessageContext()->CouldNotFindPort( &loc, found_name? name: "(null)" );
    }
}


VeriTreeNode*
VerificVerilogDesignWalker::portConnGetFormalExpr(VeriExpression* ve_portconn, Array* ve_allPortConnections, VeriModule* ve_module)
{
    VeriTreeNode* formalPortExpr = 0;
    Array* portExprs = ve_module->GetPortConnects();
    if (ID_VERIPORTCONNECT == ve_portconn->GetClassId()) {
        // verilog 95 style named connection
        VeriPortConnect* pc = static_cast<VeriPortConnect*>(ve_portconn);
        VeriPortConnect* mpc = 0;
        unsigned k = 0;
        UtString pcFormalName(gVisibleName(pc, pc->GetNamedFormal()));
        FOREACH_ARRAY_ITEM(portExprs, k, mpc) {
            if (mpc->IsAnsiPortDecl()) {
                // skip ansi ports
                continue; 
            } else {
                UtString formalName;
                bool ok = gFormalName(mpc, &formalName);
                //skip open port
                if (!ok && ((mpc == NULL) || IsOpenPort(mpc)) ) continue;
                VERIFIC_CONSISTENCY(ok, *mpc, "unable to get formal name");
                if (pcFormalName == formalName) {
                    formalPortExpr = mpc;
                    break;
                }
            }
        }
        /// should be simple id
        if (!formalPortExpr) {
            formalPortExpr = ve_module->GetPort(pc->GetNamedFormal());
        }
    } else {
        unsigned i = 0;
        // by order connection
        bool ok = getPCIndex(ve_portconn, ve_allPortConnections, &i);
        VERIFIC_CONSISTENCY(ok, *ve_portconn, "Unable to get index");
        Array* ports = ve_module->GetPorts();
        VeriIdDef* portId = static_cast<VeriIdDef*>(ports->At(i));
        if (portId->IsAnsiPort()) {
            VERIFIC_CONSISTENCY(i < ports->Size(), *ve_portconn, "index out of range");
            formalPortExpr = static_cast<VeriIdDef*>(ports->At(i));
        } else {
            VERIFIC_CONSISTENCY(i < portExprs->Size(), *ve_portconn, "index out of range");
            formalPortExpr = static_cast<VeriExpression*>(portExprs->At(i));
        }
    }
    return formalPortExpr;
}

void
VerificVerilogDesignWalker::gatherPortconnsForSinglePort(NUModule* the_module,  VeriModule* ve_module, VeriExpression **ve_portconn, unsigned *ve_portconn_iter, Array* ve_allPortConnections, UtVector<VeriExpression*> *portconns_for_this_port, NUNet** port, const SourceLocator& sourceLocation)
{
    bool partial_port = false;
    bool collapse_these_ports = false; // if true then multiple ports need to be collapsed
    ConstantRange covered_range;
    NUNet* this_port = 0;

    *port = NULL;
    //veNode ve_port = vePortConnGetParentPort( *ve_portconn );
    VeriTreeNode* ve_port = portConnGetFormalExpr(*ve_portconn, ve_allPortConnections, ve_module);

    if ( (ve_port == NULL) || IsOpenPort(ve_port) ){
        // the port was not elaborated, perhaps due to substituteModule?
        // advance the iterator so that next call will work on the next portconnection
        //*ve_portconn = veListGetNextNode(*ve_portconn_iter);
        if (!IsOpenPort(*ve_portconn)) {
            //we have case when actual connection expression is not open for formal null port, issue warning
            getMessageContext()->NullPortConnection(&sourceLocation);
        }
        arrayNextItem(ve_allPortConnections, ve_portconn_iter, ve_portconn);
        return;
    }

    lookupPort( the_module, ve_port, &this_port, &partial_port, &covered_range, sourceLocation );
    if ( this_port == NULL ){
        // there was no formal port declared for this connection, so *port
        // is null, and we advance the iterator so that next call will
        // work on the next portconnection
        arrayNextItem(ve_allPortConnections, ve_portconn_iter, ve_portconn);
    }

    collapse_these_ports = partial_port;

    portconns_for_this_port->push_back(*ve_portconn);
    *port = this_port;

    // now we look at the next port connection(s) to see if that actual
    // should be connected to the collapsed formal, if so add the
    // portconn to portconns_for_this_port
    arrayNextItem(ve_allPortConnections, ve_portconn_iter, ve_portconn);

    while ( *ve_portconn and partial_port and ( this_port == *port ) ) {
        ConstantRange port_range;
        // keep looking down the actuals until we find ourselves outside
        // of the current wacky port
        partial_port = false;
        //ve_port = vePortConnGetParentPort( *ve_portconn );
        VeriTreeNode* ve_port = portConnGetFormalExpr(*ve_portconn, ve_allPortConnections, ve_module);
        if ( (ve_port == NULL) || IsOpenPort(ve_port) ){

            if (!IsOpenPort(*ve_portconn)) {
                //we have case when actual connection expression is not open for formal null port, issue warning
                getMessageContext()->NullPortConnection(&sourceLocation);
            }
            // the port was not elaborated, perhaps due to substituteModule?
            // advance the iterator so that next call will work on the next portconnection
            break; 
        }
        lookupPort( the_module, ve_port, &this_port, &partial_port, &port_range, sourceLocation );
        if ( partial_port and (this_port == *port) ) {
            // check to see that there is no overlap of bits
            if ( covered_range.overlaps(port_range) ){
                getMessageContext()->MultiPortNet(this_port,"The same bit(s) of a net appeared more than once in the port list.");
                return;
            }
            if ( not covered_range.adjacent(port_range) ){
                getMessageContext()->MultiPortNet(this_port,"Sequential bits of a net appeared in non adjacent positions in the port list.");
                return;
            }
            covered_range = covered_range.combine(port_range);
            portconns_for_this_port->push_back(*ve_portconn);
            arrayNextItem(ve_allPortConnections, ve_portconn_iter, ve_portconn);
        }
    }
    if ( collapse_these_ports and ( covered_range.getLength() != (*port)->getBitSize () ) ){
        getMessageContext()->MultiPortNet(*port,"This net does not have all of its bits listed in the port list.");
        return;
    }
}

// old implementation, function keeped only for backword compatibility with VHDL (mixed language case)
// TODO cleanup either this needs to be reduced having just VHDL part, or which is better the one module instance needs to enhance to support VHDL verific api (probably port lookup, etc. stuff needs to be generalized and  moved into Verific2NucleusUtilities class)
void
VerificVerilogDesignWalker::createOneModuleInstanceVhdl(VeriModuleInstantiation& node, VeriInstId* inst, NUScope* parent, NUModule* masterModule, const UInt32 * /*offset*/, const SourceLocator& sourceLocation)
{
    // Creating instance and adding to module
    NUModuleInstance* moduleInstance = mNucleusBuilder->cModuleInstance(createNucleusString(inst->GetName()),
            parent, masterModule, sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(moduleInstance, node, "Unable to create instance object");
    // Creating instance connections
    unsigned j ;
    // RJC RC#15 I am concerned about situations where the module instantiation explicitly does not define an actual for a port, either
    // using named or positional connections (connection by order):
    //    , e.g. mod1 u1(val1, val2, , val4); // the third port is explicitly not connected to anything
    //    , e.g. mod1 u1(.arg1(val1), .arg2(val2), .arg3() , .arg4(val4)); // the third port is explicitly not connected to anything
    // FD: I think there are a lot of cases in beacon which test this... and I remember I specially handled this cases either by order and by name connections. If you have testcase which  can reproduce the problem please supply
    // an example testcase that has carbon_internal_error with -useVerific is where you have a module declaration:
    // module foo(clock1, clock2, clock3, in1, , out1);
    // and a module instantiation:
    //   foo u_foo(clock1, clock2, clock3, in1, , out1);
    // this passes with Interra flow
    // ADDRESSED FD: Module declarations aren't handled in this visitor. I think that particular style of not declaring port name in module declaration isn't handled currently so this is a TODO in VeriModule visitor.


    // The LRM prohibits the mixing of named and positional port ordering, so we assume here that the Verific parser will have detected
    // and reported problems if they are mixed.  In the following implementation each style is given the same priority to simplify the
    // code. There is no suggestion that an actual design could contain a mixture of port styles.
    VeriExpression *pc ;
    FOREACH_ARRAY_ITEM(inst->GetPortConnects(), j, pc) {
        NUBase* actual = NULL;
        const char* formalName = 0;
        //RJC RC#19 if pc is never NULL then why are there guards for the following two if statements?
        //ADDRESSED FD
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(pc, node, "NULL Port connection encountered");
        if (ID_VERIPORTCONNECT == pc->GetClassId()) {
            VeriPortConnect* pcc = static_cast<VeriPortConnect*>(pc);
            if (node.GetInstantiatedModule()) {
                // verilog case
                VeriPortConnect* mpc = NULL;
                unsigned k = 0;
                FOREACH_ARRAY_ITEM(node.GetInstantiatedModule()->GetPortConnects(), k, mpc) {
                    if (mpc->IsAnsiPortDecl()) {
                        formalName = pcc->GetNamedFormal();
                    } else if (mpc->GetNamedFormal()) {
                        if (UtString(mpc->GetNamedFormal()) == UtString(pcc->GetNamedFormal())) {
                            formalName = fActualRefId(mpc)->GetName();
                            break;
                        }
                    } else {
                        formalName = pcc->GetNamedFormal();
                    }
                }
            } else {
                // vhdl case
                formalName = pcc->GetNamedFormal();
            }
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(formalName, *pcc, "Unable to find formal name");
        }
        // Port connection list is always there. Even when empty
        if (ID_VERIPORTCONNECT == pc->GetClassId()) {
            // connection by name
            unsigned portDir = mNucleusBuilder->fPortDirection(masterModule, formalName);
            if ((portDir == VERI_OUTPUT) || (portDir == VERI_INOUT)) {
                gScope()->sContext(V2NDesignScope::LVALUE);
            } else {
                gScope()->sContext(V2NDesignScope::RVALUE);
            }
            VeriExpression* evalPCvalue = evalConstExpr(0, pc->GetConnection());
            if (evalPCvalue) {
                evalPCvalue->Accept(*this);
                delete evalPCvalue;
                actual = gScope()->gValue();
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(actual, node, "Unable to get actual value");
            } else if (pc->GetConnection()) {
                pc->GetConnection()->Accept(*this) ;
                actual = gScope()->gValue();
            }
        } else {
            // connection by order
            unsigned portDir = mNucleusBuilder->gPortDirection(masterModule, j, sourceLocation);
            if ((portDir == VERI_OUTPUT) || (portDir == VERI_INOUT)) {
                gScope()->sContext(V2NDesignScope::LVALUE);
                //lv = true;
            } else {
                gScope()->sContext(V2NDesignScope::RVALUE);
            }
            VeriExpression* evalPCvalue = evalConstExpr(0, pc);
            if (evalPCvalue) {
                evalPCvalue->Accept(*this);
                delete evalPCvalue;
            } else {
                pc->Accept(*this) ;
            }
            actual = gScope()->gValue();
            // NO ASSERT evaluated actual can be NULL
            // V2N_INFO_ASSERT(actual, node, "Unable to get actual value");
        }
        NUPortConnection* portConnection = NULL;
        // FD PotConnection is always present in Verific (even when empty) and in case of empty connection it will be handled by VeriPortOpen visitor.
        if (ID_VERIPORTCONNECT == pc->GetClassId()) {
            /// HERE WE LOOKUP NET BY NAME
            portConnection = mNucleusBuilder->cPortConnection(actual, formalName, masterModule, sourceLocation);
        } else {
            portConnection = mNucleusBuilder->cPortConnection(actual, j, masterModule, sourceLocation);
        }
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(portConnection, node, "Unable to get port connection");
        // Adding portConnection to moduleinstance
        mNucleusBuilder->aPortConnectionToModuleInstance(portConnection, moduleInstance);
    }
    // Adding unconnected ports if any (miss of actual is only warning), and sort connections by module port declaration order
    mNucleusBuilder->aUnconnectedPortsToModuleInstanceAndSort(moduleInstance, masterModule, sourceLocation);
    NUScope* declaration_scope = getDeclarationScope();
    mNucleusBuilder->aModuleInstanceToScope(moduleInstance, declaration_scope, sourceLocation);
}

void
VerificVerilogDesignWalker::createOneModuleInstance(VeriInstId* ve_inst, VeriModule* ve_module, NUScope* parent, NUModule* the_module, const UInt32 *offset, const SourceLocator& loc)
{
    //SourceLocator loc = locator(ve_inst);
    //ErrorCode err_code = eSuccess;
    //ErrorCode temp_err_code = eSuccess;
    //
    //NUScope* parent = mUseLegacyGenerateHierarchyNames ? context->getModule() : context->getDeclarationScope();

    bool isUdp = ve_module->GetId()->IsUdp();
    (void) isUdp;

    //NUModule* the_module = context->getLastVisitedModule();
    UtString instanceStringName(gVisibleName(ve_inst, ve_inst->GetName()));
    StringAtom* instanceName; 
    if (instanceStringName.empty()) {
        // Can't match with Interra's internal api, so just having same gensym approach like we do for other unnamed objects continousAssign, blocks, generate net names
        instanceName = mNucleusBuilder->gNextUniqueName(parent, "instance");
    } else {
        // NOTE: Here we escape the name iff the name supplied by the user requires escaping; but not if the name only requires escaping because it was
        // generated by instance array unrolling. The later should be stored in the symbol table unescaped. This is necessary for hierarchical references with
        // unrolled instance array names to work correctly, e.g. top.inst[0].b. If the name is stored escaped (e.g. top.\inst[0] .b), then the hierarchical reference
        // will not be resolved correctly. 
        if (ve_inst->IsArrayInstanceElaborationCreated())
            // Is part of an instance array
            instanceName = mNucleusBuilder->gCache()->intern(instanceStringName);
        else
            // not part of an instance array
            instanceName = mNucleusBuilder->gCache()->intern(mNucleusBuilder->createNucleusString(instanceStringName));
    }
    // register for symbol table use
    mNameRegistry[ve_inst] = instanceName;

    NUModuleInstance *the_instance =  new NUModuleInstance(instanceName, parent, the_module,
        mNucleusBuilder->gNetRefFactory(), loc);

    int num_ports = the_module->getNumPorts();
    NUPortConnectionVector portconns(num_ports);

    // Initialize to 0 because not all ports may have connections.
    std::fill(portconns.begin(), portconns.end(), (NUPortConnection*)0);

    Array* ve_allPortConnections = ve_inst->GetPortConnects();
    unsigned ve_portconn_iter = 0;
    if (ve_allPortConnections) {
        VeriExpression* ve_portconn = 0;

        arrayNextItem(ve_allPortConnections, &ve_portconn_iter, &ve_portconn);

        NUNetSet formal_ports_processed;

        while ( ve_portconn ) {
            NUNet* port = 0;

            //VeNodeList portconns_for_this_port;
            UtVector<VeriExpression*> portconns_for_this_port;

            gatherPortconnsForSinglePort(the_module, ve_module, &ve_portconn, &ve_portconn_iter, ve_allPortConnections, &portconns_for_this_port, &port, loc);

            // at this point ve_portconn and ve_portconn_iter are pointing
            // to an actual beyond the one(s) that will be connected to the
            // current formal port. 

            // Empty port if port == 0
            if (port) {
                NUPortConnection* portconn = 0;
                NU_ASSERT ((portconns_for_this_port.size()), port);

                if (formal_ports_processed.find(port) != formal_ports_processed.end()) {
                    getMessageContext()->MultiPortNet(port, "This formal port net appears in multiple and/or non-adjacent, positions in the port list.");
                    return;     // this is unrecoverable
                }
                formal_ports_processed.insert(port);

                createPortConnection(portconns_for_this_port, ve_allPortConnections, port, the_module, the_instance, ve_module, offset, loc, &portconn);

                int i = the_module->getPortIndex(port);
                if (not ((i >= 0) && (i < num_ports))) {
                    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(false, loc, "Out of bounds port index");
                }
                portconns[i] = portconn;
            }

        }    
    }

    // Create port connections for unconnected ports.
    for (int i = 0; i < num_ports; ++i) {
        if (portconns[i] == 0) {
            NUNet *port = the_module->getPortByIndex(i);
            NUPortConnection* portconn = mNucleusBuilder->createUnconnectedPortConnection(port, loc);
            portconn->setModuleInstance(the_instance);
            portconns[i] = portconn;
            getMessageContext()->UnconnectedModulePort(&loc,the_instance->getName()->str(), port->getName()->str()); // matches the interra parser message 20034
        }
    }

    the_instance->setPortConnections(portconns);

    // Add the instance to the calling module or enclosing NamedDeclarationScope
    mNucleusBuilder->aModuleInstanceToScope(the_instance, parent, loc);
}

void
VerificVerilogDesignWalker::createPortConnection(const UtVector<VeriExpression*>& ve_portconns, Array* ve_allPortConnections, NUNet* formal,
                                                 NUModule* module, NUModuleInstance *instance, VeriModule* ve_module, const UInt32 *offset,
                                                 const SourceLocator& loc, NUPortConnection **the_conn)
{
    *the_conn = 0;
    bool formal_is_input = formal->isInput();
    bool formal_is_output = formal->isOutput();
    bool formal_is_bid = formal->isBid();

    // Actual may be 0 if the port is unconnected.
    // We go by the type of the formal instead of the type of port connection which
    // the parser reports, because we may have already done port coercion.
    UInt32 formal_width = formal->getBitSize();
    SInt32 num_actuals = ve_portconns.size();
    if (formal_is_input) {
        NUExpr *actual = 0;
        if (num_actuals != 0) {
            NUExpr *temp_actual = 0;
            NUExprVector expr_vector;

            for (UtVector<VeriExpression*>::const_iterator p = ve_portconns.begin(), e = ve_portconns.end(); p != e; ++p) {
                VeriExpression* ve_portconn = *p;

                //const veNode ve_actual = vePortConnGetConnectedExpr(ve_portconn);
                NUExpr *this_actual = 0;

                VeriExpression* ve_actual = 0;
                if (ID_VERIPORTCONNECT == ve_portconn->GetClassId()) {
                    // connection by name
                    if (ve_portconn->GetConnection() ) {
                      if ( !ve_portconn->GetConnection()->IsOpen()) {
                        ve_actual = ve_portconn->GetConnection();
                      } else {
                        getMessageContext()->UnconnectedModulePort(&loc,instance->getName()->str(),formal->getName()->str()); // matches the interra parser message 20034
                      }
                    }
                } else {
                    // connection by order
                   if (ve_portconn) {
                     if (!ve_portconn->IsOpen()) {
                       ve_actual = ve_portconn;
                     } else {
                       getMessageContext()->UnconnectedModulePort(&loc,instance->getName()->str(),formal->getName()->str()); // matches the interra parser message 20034
                     }
                   }
                }
                gScope()->sContext(V2NDesignScope::RVALUE);
                // Per LRM (1364-2001, section 12.3.9.2), for purposes of 
                // size/sign calculations, port connections are treated as continuous assigns.
                int actual_size_sign = (ve_actual ? ve_actual->StaticSizeSign() : 0);
                unsigned actual_size = GET_CONTEXT_SIZE(actual_size_sign);
                unsigned actual_sign = GET_CONTEXT_SIGN(actual_size_sign);
                //get context size: max of lhs and rhs
                unsigned context_size = MAX(actual_size, formal_width);
                //context sign of rhs will be sign of rhs, (LRM says that LHS is not considered)
                int context_size_sign = MAKE_CONTEXT_SIZE_SIGN(context_size, actual_sign);
                int savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign); // save starting value
                VeriExpression* evalPCvalue = evalConstExpr(context_size_sign, ve_actual);
                if (evalPCvalue) {
                    evalPCvalue->Accept(*this);
                    delete evalPCvalue;
                    this_actual = dynamic_cast<NUExpr*>(gScope()->gValue());
                } else if (ve_actual) {
                    ve_actual->Accept(*this) ;
                    this_actual = dynamic_cast<NUExpr*>(gScope()->gValue());
                }
                (void) gScope()->updateContextSizeSign(savedContextSizeSign);                // Restore context size/sign
                if ( !ve_actual && (num_actuals > 1) ) {
                    // if there are more than 1 actuals then we will create a concat of actuals for this
                    // port, and the current bit(s) are undriven, so we put z in their place
                    // to determine the necessary size we use the size of the formal that is associated with
                    // the gap in the actuals
                    NUNet* this_port = 0;
                    bool partial_port = false;
                    ConstantRange port_range;
                    VeriTreeNode* ve_port = portConnGetFormalExpr(ve_portconn, ve_allPortConnections, ve_module);
                    lookupPort( module, ve_port, &this_port, &partial_port, &port_range, loc);
                    NU_ASSERT(this_port, module); // multiple actuals were being connected to a collapsed
                    // (wacky) port, but the formal port was
                    // undefined. something is wrong.
                    SInt32 size(port_range.getLength());
                    UtString zval(size, 'z');
                    this_actual = NUConst::createXZ(zval, false, size, loc);
                }
                expr_vector.push_back(this_actual);
            }

            if ( num_actuals == 1 ) {
                temp_actual = expr_vector.front();
            } else if ( num_actuals > 1 ) {
                temp_actual = new NUConcatOp(expr_vector, 1, loc);
            }
            if ( temp_actual ) {
                mNucleusBuilder->rvalueActualFixup(temp_actual, formal_width, offset, &actual);
                if ( actual ) {
                    actual->resize(actual->determineBitSize());
                }
            }
            if ( actual && (! actual->isSignedResult() && formal->isSigned()) ){
                UInt32 actual_width = actual->determineBitSize ();
                if ( formal_width == actual_width ) {
                    // bug 14514 only appears to be an issue if the widths match
                    UtString buf;
                    formal->compose(&buf);
                    getMessageContext()->SignedMisMatch( &loc, buf.c_str());
                }
            }
        }
        *the_conn = new NUPortConnectionInput(actual, formal, loc);
    } else if (formal_is_output or formal_is_bid) {
        NULvalue *actual = 0;
        if ( num_actuals != 0 ) {
            NULvalue *temp_actual = 0;
            NULvalueVector lvalue_vector;

            for (UtVector<VeriExpression*>::const_iterator p = ve_portconns.begin(), e = ve_portconns.end(); p != e; ++p){
                VeriExpression* ve_portconn = *p;
                //const veNode ve_actual = vePortConnGetConnectedExpr(ve_portconn);
                NULvalue *this_actual = 0;
                VeriExpression* ve_actual = 0;
                if (ID_VERIPORTCONNECT == ve_portconn->GetClassId()) {
                    // connection by name
                  if (ve_portconn->GetConnection()) {
                    if (!ve_portconn->GetConnection()->IsOpen()) { 
                      ve_actual = ve_portconn->GetConnection();
                    } else {
                      getMessageContext()->UnconnectedModulePort(&loc,instance->getName()->str(),formal->getName()->str()); // matches the interra parser message 20034
                    }
                  }
                } else {
                    // connection by order
                   if (ve_portconn) {
                     if (!ve_portconn->IsOpen()) {
                       ve_actual = ve_portconn;
                     } else {
                       getMessageContext()->UnconnectedModulePort(&loc,instance->getName()->str(),formal->getName()->str()); // matches the interra parser message 20034
                     }
                   }
                }
                gScope()->sContext(V2NDesignScope::LVALUE);

                // Per LRM (1364-2001, section 12.3.9.2), for purposes of 
                // size/sign calculations, port connections are treated as continuous assigns.
                int actual_size_sign = (ve_actual ? ve_actual->StaticSizeSign() : 0);
                unsigned actual_size = GET_CONTEXT_SIZE(actual_size_sign);
                unsigned actual_sign = GET_CONTEXT_SIGN(actual_size_sign);
                //get context size: max of lhs and rhs
                unsigned context_size = MAX(actual_size, formal_width);
                //context sign of rhs will be sign of rhs, (LRM says that LHS is not considered)
                int context_size_sign = MAKE_CONTEXT_SIZE_SIGN(context_size, actual_sign);
                int savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign); // save starting value

                // no need to call evalConstExpr because actual cannot be a constant?
                if (ve_actual) {
                    ve_actual->Accept(*this) ;
                    this_actual = dynamic_cast<NULvalue*>(gScope()->gValue());
                }
                if (!ve_actual && (num_actuals > 1) ) {
                    // I don't know how to represent the actuals of a compressed wacky port where one or
                    // more of the bits (but not all) of an output port are unconnected. So we don't support it
                    // this could be fixed by defining a new net and using it here as a placeholder
                    getMessageContext()->MultiPortNet(formal, "Some fraction of the actuals needed for this formal are unconnected.");
                    return;
                }
                lvalue_vector.push_back (this_actual);
                (void) gScope()->updateContextSizeSign(savedContextSizeSign);                // Restore context size/sign
            }
            if ( num_actuals == 1 ){
                temp_actual = lvalue_vector.front();
            } else if ( num_actuals > 1 ) {
                temp_actual = new NUConcatLvalue(lvalue_vector, loc);
            }

            if ( temp_actual ) {
                mNucleusBuilder->lvalueActualFixup(temp_actual, formal_width, offset, &actual);

                if ( actual ) {
                    actual->resize();
                }
            }
        }
        if (formal_is_output) {
            *the_conn = new NUPortConnectionOutput(actual, formal, loc);
        } else if (formal_is_bid) {
            *the_conn = new NUPortConnectionBid(actual, formal, loc);
        }
    }  else {
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(false, loc, "Unknown port type");
        return;
    }

    if (*the_conn) {
      (*the_conn)->setModuleInstance(instance);
    }
}

bool
VerificVerilogDesignWalker::getPCIndex(VeriExpression* ve_portconn, Array* ve_allPortConnections, unsigned * index)
{
    unsigned j;
    VeriExpression *pc ;
    FOREACH_ARRAY_ITEM(ve_allPortConnections, j, pc) {
        if (pc == ve_portconn) {
            *index = j;
            return true;
        }
    }
    return false; 
}


//! Get initial value NUExpr*, NULL if no initial value. This works for VeriNetDecl and
// VeriDataDecl
NUExpr*
VerificVerilogDesignWalker::gInitialValueExpr(VeriIdDef* id, VeriDataDecl& node, bool isBitType, int context_size_sign)
{
  // Get current node source location 
  SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

  // Get initial value if any
  // Checking for constant expression value, or constant net type (SUPPLY0, SUPPLY1)
  NUExpr* value = NULL;
  if (id->GetInitialValue() || (VERI_SUPPLY0 == node.GetNetType()) || (VERI_SUPPLY1 == node.GetNetType()) ) {
    gScope()->sContext(V2NDesignScope::RVALUE);
    //RJC RC#2013/09/30 (cloutier) the use of StaticSizeSign here implies that it is self-determined, it seems like the LSH should also play here
    // ADDRESSED FD: computed 'context_size_sign' value should be used for constants, as well set as a context size, for non-constant initializations (this will solve also memories case (packed dimension initializations in SystemVerilog)).
    unsigned assignBitWidth = GET_CONTEXT_SIZE(context_size_sign);
    VeriExpression* evalRvalue = evalConstExpr(context_size_sign, id->GetInitialValue());
    if (evalRvalue) {
      evalRvalue->Accept(*this);
      delete evalRvalue;
      value = dynamic_cast<NUExpr*>(gScope()->gValue());
    } else if (id->GetInitialValue()) {
      // context is already set
      id->GetInitialValue()->Accept(*this);
      value = dynamic_cast<NUExpr*>(gScope()->gValue());
    } else if (VERI_SUPPLY0 == node.GetNetType()) {
        value = mNucleusBuilder->cConst(0, assignBitWidth, false, sourceLocation);
    } else if (VERI_SUPPLY1 == node.GetNetType()) {
      if (isBitType) {
        value = mNucleusBuilder->cConst(1, 1, false, sourceLocation);
      } else {
        //RJC RC#2013/09/30 (ricks)  Create a testcase with VERY wide vectors, and verify that the
        // floating point pow works.
        value = mNucleusBuilder->cConst(std::pow(2.0, static_cast<int>(assignBitWidth)) - 1.0, assignBitWidth, false, sourceLocation);
      }
    }
  }

  return value;
}

//! Get the initializer block for a given module. If one does not exist, create it.

// Note that this will probably have to be generalized to support SystemVerilog packages
// and interfaces. Initial blocks can appear in those too.
NUBlock* VerificVerilogDesignWalker::gInitializerBlock(NUScope* scope, const SourceLocator& sourceLocation)
{
  NUBlock* block = NULL;
  NUModule* module = mNucleusBuilder->gModule(scope);
  NUCLEUS_CONSISTENCY(module, sourceLocation, "null owner module");
  if (mInitializerBlocks.find(module) != mInitializerBlocks.end()) {
    block = mInitializerBlocks[module];
  } else {
    // create begin/end block to contain blocking assigns
    block = mNucleusBuilder->cBlock(module, sourceLocation);
    // create initial block
    NUInitialBlock* initialBlock = mNucleusBuilder->cInitialBlock(module, block, sourceLocation);
    mNucleusBuilder->aInitialBlockToModule(initialBlock, module);
    // Save the initializer block for this scope. It can be used by other initializers.
    mInitializerBlocks[module] = block;
  }

  return block;
}

//! Create a continuous assign to implement the initial value on a VeriNetDecl. Add the
//  continuous assign to the module.
void
VerificVerilogDesignWalker::createInitialValueContinuousAssign(NUExpr* value, VeriNetDecl& node, NUNet* n)
{
  // Initial value guaranteed to exist
  VERIFIC_CONSISTENCY_NO_RETURN_VALUE(value, node, "unexpected null initial value");

  // Get current node source location 
  SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  // if value exists, create continous assign for initialization
  // create left expression from net
  NULvalue* lExpr = mNucleusBuilder->cIdentLvalue(n, sourceLocation);
  // Nucleus continous assign
  NUContAssign* contAssign = mNucleusBuilder->cContinuesAssign(getNUScope(&node), lExpr, value, sourceLocation);
  mNucleusBuilder->aContinuesAssignToModule(contAssign, dynamic_cast<NUModule*>(getNUScope(&node)));
}

//! Create a blocking assign for initial value on a VeriDataDecl (reg, time variable, etc). 
NUBlockingAssign*
VerificVerilogDesignWalker::createInitialValueBlockingAssign(NUExpr* value, VeriDataDecl& node, NUNet* n)
{
  // Initial value guaranteed to exist
  VERIFIC_CONSISTENCY(value, node, "unexpected null initial value");

  // Get current node source location 
  SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  // Create left expression from net
  NULvalue* lValue = mNucleusBuilder->cIdentLvalue(n, sourceLocation);
  // Create the blocking assign
  NUBlockingAssign* blockAssign = mNucleusBuilder->cBlockingAssign(lValue, value, sourceLocation);
  VERIFIC_CONSISTENCY(blockAssign, node, "Unable to create blocking assign for initializer");
  
  return blockAssign;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// Combinational UDP support /////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////


UtString 
VerificVerilogDesignWalker::seqEntryGetCurrentState(const UtString& entry)
{
    //return entry.substr(entry.find_first_of(':') + 1, entry.find_last_of(':'));
    return UtString(1, entry[entry.find_first_of(':') + 1]);
}



UtString 
VerificVerilogDesignWalker::tableEntryGetOutputSymbol(const UtString& entry)
{
    return entry.substr(entry.find_last_of(':') + 1);
}

UtString
VerificVerilogDesignWalker::tableEntryGetInputList(const UtString& entry)
{
    return entry.substr(0, entry.find_first_of(':'));
}
    
UtString 
VerificVerilogDesignWalker::levelListNextItem(const UtString& levelList, unsigned* index)
{
    UtString item;
    if (*index < levelList.size()) {
        item = UtString(1, levelList[*index]);
        ++(*index);
    } else {
        item = UtString("");
    }
    return item;
}


/*! 
  Build a continuous assign to represent a combinational UDP table.
  Only entries with output=1 and no x values are counted towards the
  sum of products representation.

  \verbatim
  Example:
   table
   // a b c : out
      0 0 1 : 1
      x b ? : 0
      1 1 0 : x
      1 1 1 : 1
   endtable
 out = !a&!b&c | a&b&c;
 \endverbatim
*/
void
VerificVerilogDesignWalker::udpContAssign(VeriTable* udpTable, NUModule* module, const SourceLocator& loc, NUContAssign** the_assign)
{
    NUExpr* sum = NULL;
    Array* tableEntries = udpTable->GetTableEntries();
    VeriModule* ve_module = udpTable->GetTheModule();
    //NUIdentLvalue* lhs;
    //udpMakeLhs(veriTable, &lhs);
    VeriTreeNode* output = static_cast<VeriTreeNode*>(ve_module->GetPorts()->GetFirst());
    NULvalue* lhs = identLvalue(output, loc);
    //gScope()->sContext(V2NDesignScope::LVALUE);
    //output->Accept(*this); // get LHS through idref
    //NUIdentLvalue* lhs = dynamic_cast<NUIdentLvalue*>(gScope()->gValue());
    //CheetahList tableEntries(veUDPGetTableEntryList( ve_udp ));
    char *entry ;
    unsigned i ;
    Array* portList = ve_module->GetPorts(); // Ports contain iddef
    FOREACH_ARRAY_ITEM(tableEntries, i, entry) {
        // Build a sum of products where the output is 1.
        // If the output is not 1, ignore this entry.
        UtString outputSymbol = tableEntryGetOutputSymbol( entry );
        //veNode outputSymbol = veTableEntryGetOutputSymbol( entry );
        if ( UtString("1") != outputSymbol )
            continue;
        //CheetahList portList(veUDPGetPortList( ve_udp ));
        // skip the output
        unsigned index = 0; // index to keep track of port access
        VeriTreeNode* output = 0;
        arrayNextItem( portList, &index, &output );

        UtString levelList = tableEntryGetInputList( entry );
        //CheetahList levelList(veCombEntryGetLevelInputList( entry ));

        NUExpr* product = NULL;
        udpLevelEntryProduct( levelList, portList, &index, loc, &product );
        if ( product ) {
            if ( sum ) {
                sum = new NUBinaryOp( NUOp::eBiBitOr, sum, product, loc );
                sum->resize( 1 );			
            } else {
                sum = product;
            }
        }
    }

    if (sum) {
        //*the_assign = helperPopulateContAssign(lhs, sum, loc, context);
        *the_assign = mNucleusBuilder->cContinuesAssign(module, lhs, sum, loc);
    } else {
        getMessageContext()->NoUDPOutputExpr( &loc );
    }
}

//! Return the product of level inputs in a UDP table.
void VerificVerilogDesignWalker::udpLevelEntryProduct( 
        const UtString& levelList,   // List of table entry levels
        Array* portList,    // List of inputs queued to be in sync with the levelList    
        unsigned* portIndex, /// current index for entry levels and inputs
        const SourceLocator& loc,
        NUExpr** product )  // Returned product expression
{
    unsigned levelIndex = 0;
    UtString level = levelListNextItem(levelList, &levelIndex);
    while (!level.empty() ) {
        VeriTreeNode* ve_port = 0; 
        arrayNextItem(portList, portIndex, &ve_port);
        // Entries with 'x' are ignored altogether.
        if ( UtString("x") == level ) {
            if ( (*product) ) {
                delete (*product);
                (*product) = NULL;
            }
            break;
        }
        // Skip don't care inputs
        if ( (UtString("b") == level) || (UtString("?") == level) || (UtString("*") == level) ) {
            level = levelListNextItem(levelList, &levelIndex);
            continue;
        }

        udpAddTermToProduct(product, ve_port, (UtString("0") == level), loc);
        level = levelListNextItem(levelList, &levelIndex);
    }
    if ((*product)) {
        (*product)->resize( 1 );
    }
}

//! Add a term to the current product.
void
VerificVerilogDesignWalker::udpAddTermToProduct( 
        NUExpr** product,   // Product of literals from previous calls
        VeriTreeNode* ve_port,        // port for the net to be used as a literal
        bool invert,      // Whether to invert this literal before anding it in.
        const SourceLocator& loc) 
{
    NUExpr* term = identRvalue(ve_port, loc);

    if (invert) 
        term = new NUUnaryOp(NUOp::eUnLogNot, term, loc );
    // Add the term to the product.
    if( (*product) )
        (*product) = new NUBinaryOp( NUOp::eBiBitAnd, (*product), term, loc );
    else
        (*product) = term;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// Sequential UDP support ////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

bool 
VerificVerilogDesignWalker::udpInputListGetEdgeIndex(const UtString& inputList,unsigned* edgeIndex)
{
    unsigned index = 0;
    UtString input = levelListNextItem(inputList, &index);
    while (!input.empty()) {
        INFO_ASSERT(1 == input.size(), "Incorrect input symbol type");
        char inputSymbol = input[0];
        if (VeriNode::GetCleanUdpEdge(inputSymbol) || VeriNode::IsUdpXEdge(inputSymbol)) {
            *edgeIndex = index - 1; // index always show beyond the one we have read, so taking -1
            return true;
        }
        input = levelListNextItem(inputList, &index);
    }
    return false;
}

char
VerificVerilogDesignWalker::udpInputListGetEdge(const UtString& inputList, unsigned edgeIndex)
{
    INFO_ASSERT(edgeIndex < inputList.size(), "Edge index out of bounds");
    return inputList[edgeIndex];
}

UtString 
VerificVerilogDesignWalker::udpEdgeInputListGetUdpLevelInputList1(const UtString& inputList, unsigned edgeIndex)
{
    INFO_ASSERT(edgeIndex < inputList.size(), "Edge index out of bounds");
    return inputList.substr(0, edgeIndex); // left list
}

UtString 
VerificVerilogDesignWalker::udpEdgeInputListGetUdpLevelInputList2(const UtString& inputList, unsigned edgeIndex)
{
    INFO_ASSERT(edgeIndex < inputList.size(), "Edge index out of bounds");
    return inputList.substr(edgeIndex + 1); // right list
}



void 
VerificVerilogDesignWalker::udpSequentialComponents(VeriTable* udpTable, NUModule* module, const SourceLocator& loc)
{
    NUExpr* levelSet = NULL;
    NUExpr* levelClear = NULL;
    NUExpr* clock = NULL;
    udpEdgeType edge = udpDontCare;
    NUExpr* edgeData = NULL;
    NUExpr* edgeEnable = NULL;
    //veNode clkNode = NULL;
    VeriTreeNode* clkNode = NULL;
    // Get UDP primitive  
    VeriModule* ve_module = udpTable->GetTheModule();
    // Process the sequential UDP table and build the expressions.
    Array* tableEntries = udpTable->GetTableEntries();
    //CheetahList tableEntries(veUDPGetTableEntryList( ve_udp ));
    char *entry ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(tableEntries, i, entry) {
        // Only process this entry if it sets or clears the state.
        UtString outputSymbol = tableEntryGetOutputSymbol( entry );
        //veNode outputSymbol = veTableEntryGetOutputSymbol( entry );
        //veOutputSymbolType outputLevel = veOutputSymbolGetSymbol( outputSymbol );
        UtString stateSymbol = seqEntryGetCurrentState( entry );
        //veNode stateSymbol = veSeqEntryGetCurrentState( entry );
        //veLevelSymbolType stateLevel = veLevelSymbolGetSymbol( stateSymbol );
        bool setEntry = (UtString("1") == outputSymbol) && (UtString("1") != stateSymbol);
        // bool setEntry = (outputLevel == OUTPUT_ONE) && (stateLevel != LEVEL_ONE);
        bool clearEntry = (UtString("0") == outputSymbol) && (UtString("0") != stateSymbol);
        //bool clearEntry = (outputLevel == OUTPUT_ZERO) && (stateLevel != LEVEL_ZERO);
        if( !setEntry && !clearEntry )
            continue;

        // the way we keep this entries this function could be in common with combinational one
        // and then in getting next item, will have todo sequential specific get 
        UtString inputList = tableEntryGetInputList(entry);
        // Get Edge index for current input list
        unsigned edgeIndex = 0;
        bool hasEdge = udpInputListGetEdgeIndex(inputList, &edgeIndex);
        //veNode inputList = veSeqEntryGetUdpInputList( entry );
        Array* portList = ve_module->GetPorts(); // Ports contain iddefs
        //CheetahList portList(veUDPGetPortList( ve_udp ));
        // Start at the the portList at the inputs.
        VeriTreeNode* ve_port = 0; 
        unsigned portIndex = 0;
        arrayNextItem(portList, &portIndex, &ve_port);

        bool isLevelEntry = false;
        NUExpr* product = NULL;
        // Determine whether we have a level entry.  This is complicated by the
        // fact that some edges (e.g. (?1) ) are effectively levels.
        //TODO Verific's clean edge detection does not do proper job need to have our own (test bug8801_01.v)
        if (!hasEdge) {
            //if( veNodeGetObjType( inputList ) == VE_UDPLEVELINPUTLIST )
            isLevelEntry = true;
            //CheetahList levelList(veUdpLevelInputListGetUdpLevelInputList( inputList ));

            udpLevelEntryProduct(inputList, portList, &portIndex, loc, &product);
        } else if (udpLevelsContainX(inputList, edgeIndex)) {
            // This entry contains an X. We just ignore these entries in UDPs
        } else {
            udpEdgeType et = udpGetEdgeType(inputList, edgeIndex);
            if( et == udpLevelHigh || et == udpLevelLow || et == udpDontCare ) {
                // This edge entry is really just a level.  Build the level list
                // including the phony edge which is really a level.
                isLevelEntry = true;

                UtString list1 = udpEdgeInputListGetUdpLevelInputList1(inputList, edgeIndex);
                //CheetahList list1(veUdpEdgeInputListGetUdpLevelInputList1( inputList ));
                udpLevelEntryProduct(list1, portList, &portIndex, loc, &product);
                arrayNextItem(portList, &portIndex, &ve_port);

                if( et == udpLevelHigh || et == udpLevelLow ) {
                    udpAddTermToProduct(&product, ve_port, (et == udpLevelLow), loc);
                }

                UtString list2 = udpEdgeInputListGetUdpLevelInputList2(inputList, edgeIndex);
                //CheetahList list2(veUdpEdgeInputListGetUdpLevelInputList2( inputList ));
                udpLevelEntryProduct(list2, portList, &portIndex, loc, &product);
            }
        }

        if( isLevelEntry ) {
            if( product ) {
                if( setEntry ) {
                    if( levelSet ) {
                        levelSet = new NUBinaryOp( NUOp::eBiBitOr, levelSet, product, loc );
                        levelSet->resize( 1 );
                    } else {
                        levelSet = product;
                    }
                } else {
                    if( levelClear ) {
                        levelClear = new NUBinaryOp( NUOp::eBiBitOr, levelClear, product, loc );
                        levelClear->resize( 1 );
                    } else {
                        levelClear = product;
                    }
                }
            }
        } else {
            NUExpr* enable = NULL;
            bool isValidEntry = true;
            udpEdgeEntry(entry, inputList, portList, portIndex, edgeIndex, module, loc, &clkNode, &clock,
                    &edge, &enable, &isValidEntry);
            if (isValidEntry) {
                if( edgeData && !edgeEnable ) {
                    // rjc we have no testcase for this condition
                    //mTicProtectedNameMgr->getVisibleName(ve_udp, &udp_name);
                    // nucleus api already has protected names, so no need to get visible name here
                    UtString udp_name(module->getName()->str());
                    getMessageContext()->UDPEdgeError( &loc, udp_name.c_str(),
                                " has an edge assignment followed by an unconditional edge assignment" ) ;
                    return;
                }

                if (enable) {
                    if (!edgeEnable) {
                        edgeEnable = enable;
                    } else {
                        edgeEnable = new NUBinaryOp(NUOp::eBiBitOr, enable, edgeEnable, loc);
                        edgeEnable->resize(1);	
                    }		
                }

                if (edgeData) {
                    if( setEntry ) {
                        CopyContext cc(0,0);
                        edgeData = new NUBinaryOp( NUOp::eBiBitOr, enable->copy(cc), edgeData, loc );
                        edgeData->resize( 1 );
                    }			
                } else {
                    if (setEntry) {
                        if (enable) {
                            CopyContext cc(0,0);
                            edgeData = enable->copy(cc);
                        }
                    } else {
                        edgeData = NUConst::create( false, 0,  1, loc );
                    }
                } 
            }
        }
    }

    // Create the always block to represent the table.
    VeriTreeNode* output = static_cast<VeriTreeNode*>(ve_module->GetPorts()->GetFirst());
    udpAlways(output, module, clock, edge, edgeEnable, edgeData, levelSet, levelClear);
}

bool VerificVerilogDesignWalker::udpLevelsContainX (const UtString& inputList, unsigned edgeIndex)
{
    //CheetahList levelList (veUdpEdgeInputListGetUdpLevelInputList1( inputList ));
    UtString levelList = udpEdgeInputListGetUdpLevelInputList1(inputList, edgeIndex); 
    //veNode level = veListGetNextNode (levelList);
    unsigned levelIndex = 0;
    UtString level = levelListNextItem(levelList, &levelIndex);
    while (!level.empty()) {
        INFO_ASSERT(1 == level.size(), "Incorrect level symbol type");
        char inputSymbol = level[0];
        if (('x' == inputSymbol)) {
            return true;
        }
        level = levelListNextItem (levelList, &levelIndex);
        //level = veListGetNextNode (levelList);
    }
    return false;
}

VerificVerilogDesignWalker::udpEdgeType VerificVerilogDesignWalker::udpGetEdgeType(const UtString& inputList, unsigned edgeIndex)
{
    udpEdgeType edge_type = udpPosedge;
    char edgeSymbol = udpInputListGetEdge(inputList, edgeIndex);
    const char* expandedEdge = VeriNode::PrintUdpEdgeChar(edgeSymbol);
    UtString edge;
    if (expandedEdge) {
        edge = UtString(expandedEdge);
        edge = edge.substr(1,edge.size()-2); // strip brackets from string like (1?)
    } else {
        edge = UtString(1, edgeSymbol);
    }
    //veNode edge = veUdpEdgeInputListGetEdge( inputList );
    if (2 == edge.size()) {
        char level1 = edge[0];
        char level2 = edge[1];
        if ('b' == level1)
            level1 = '?';
        if ('b' == level2)
            level2 = '?';
        if (('x' == level1) || ('x' == level2)) {
            edge_type = udpX;
        } else if ((('0' == level1) && ('0' == level2)) ||
                (('0' == level1) && ('?' == level2)) ||
                (('?' == level1) && ('0' == level2))) {
            edge_type = udpLevelLow;
        } else if ((('1' == level1) && ('1' == level2)) ||
                (('1' == level1) && ('?' == level2)) ||
                (('?' == level1) && ('1' == level2))) {
            edge_type = udpLevelHigh;
        } else if (('0' == level1) && ('1' == level2)) {
            edge_type = udpPosedge;
        } else if (('1' == level1) && ('0' == level2)) {
            edge_type = udpNegedge;
        } else {
            edge_type = udpDontCare;
        }
    } else {
        INFO_ASSERT(1 == edge.size(), "Edge symbol size should be 1");
        char symbol = edge[0];
        switch (symbol) {
            case 'r':
            case 'p':
                edge_type = udpPosedge;
                break;
            case 'f':
            case 'n':
                edge_type = udpNegedge;
                break;
            case '?':
                edge_type = udpDontCare;
                break;
            case '*':
                edge_type = udpX;
                break;
        }
    }
    return edge_type;
}

NUExpr* VerificVerilogDesignWalker::identRvalue(VeriTreeNode* port, const SourceLocator& loc)
{
    NUExpr* expr = NULL;
    VERIFIC_CONSISTENCY(mDeclarationRegistry.find(port) != mDeclarationRegistry.end(), *port, "Unable to get net");
    NUNet* net = mDeclarationRegistry[port];
    expr = mNucleusBuilder->cIdentRvalue(net, loc);
    return expr;
}

NULvalue* VerificVerilogDesignWalker::identLvalue(VeriTreeNode* port, const SourceLocator& loc)
{
    NULvalue* lvalue = NULL;
    VERIFIC_CONSISTENCY(mDeclarationRegistry.find(port) != mDeclarationRegistry.end(), *port, "Unable to get net");
    NUNet* net = mDeclarationRegistry[port];
    lvalue = mNucleusBuilder->cIdentLvalue(net, loc);
    return lvalue;
}

void
VerificVerilogDesignWalker::udpEdgeEntry(const UtString& entry, const UtString& inputList,
    Array* portList, unsigned portIndex, unsigned edgeIndex, NUModule* module, const SourceLocator& loc, 
    VeriTreeNode** clkNode, NUExpr** clock, udpEdgeType* edge, NUExpr** enable, bool* isValidEntry)
{
    UtString list1 = udpEdgeInputListGetUdpLevelInputList1(inputList, edgeIndex);
    UtString list2 = udpEdgeInputListGetUdpLevelInputList2(inputList, edgeIndex);
    UtString qList;
    //CheetahList list1(veUdpEdgeInputListGetUdpLevelInputList1( inputList ));
    //CheetahList list2(veUdpEdgeInputListGetUdpLevelInputList2( inputList ));
    //CheetahList qList(vewCreateNewList());
    qList.append(seqEntryGetCurrentState(entry));
    //vewAppendToList( qList, veSeqEntryGetCurrentState( entry ) );
    udpEdgeType edgeType = udpGetEdgeType(inputList, edgeIndex);

    // Ignore entries with 'x'.
    if (udpLevelListHasX(list1) || udpLevelListHasX(list2) ||
            udpLevelListHasX(qList)  || edgeType == udpX) {
        (*isValidEntry) = false;
        return;
    }
    (*isValidEntry) = true;
    udpLevelEntryProduct(list1, portList, &portIndex, loc, enable);
    VeriTreeNode* newClkNode;
    arrayNextItem(portList, &portIndex, &newClkNode);
    // Verify that we only have one input that looks like a clock.
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(newClkNode, loc, "No clock found in UDB edge list");
    if (!(*clkNode)) {
        (*clkNode) = newClkNode;
        //ec = identRvalue( vePortGetNetExpr(newClkNode), context, context->getModule(), clock );
        *clock = identRvalue(newClkNode, loc);
    }
    if ((*clkNode) != newClkNode) {
        getMessageContext()->UDPEdgeError( &loc, module->getName()->str(),
            " has multiple inputs with edge entries" );
        return;
    }
    // Verify that it is on the same edge
    if ((*edge) == udpDontCare)
        (*edge) = edgeType;
    if ((*edge) != edgeType) {
        getMessageContext()->UDPEdgeError(&loc, module->getName()->str(),
            " has edge entries for both posedge and negedge");
        return;
    }

    udpLevelEntryProduct(list2, portList, &portIndex, loc, enable);

    // Rewind the portlist back to 'Q'.  Add the current state to the enable.
    //veListRewind( portList );
    portIndex = 0;
    udpLevelEntryProduct(qList, portList, &portIndex, loc, enable);
}

void
VerificVerilogDesignWalker::udpAlways(VeriTreeNode* outputPort, NUModule* module,
    NUExpr* clock, udpEdgeType edge, NUExpr* edgeEnable, NUExpr* edgeData, NUExpr* levelSet, NUExpr* levelClear)
{
    const SourceLocator& loc = module->getLoc();

    if (!clock && !levelSet && !levelClear) {
        getMessageContext()->NoUDPOutputExpr(&loc);
        return;
    }

    // Check to see if the edge nets intersect the level nets.
    // If so, the recognition is probably not optimal.  Fix for
    // bug 5997.
    NUNetSet clockUse;
    if (clock)
        clock->getUses(&clockUse);

    NUNetSet levelUse;
    if (levelSet)
        levelSet->getUses(&levelUse);
    if (levelClear)
        levelClear->getUses(&levelUse);

    NUNetSet intersection;
    set_intersection(clockUse, levelUse, &intersection);
    if (!intersection.empty()) {
        getMessageContext()->udpClockUseWarning(&loc);
    }

    // Make the statement to insert in the always.
    NUStmt* newStmt = NULL;
    if (edgeData) {
        // Get LHS
        NULvalue* lhs = identLvalue(outputPort, loc);
        //ec = udpMakeLhs( ve_udp, context, &lhs );
        newStmt = new NUBlockingAssign(lhs, edgeData, false, loc);
        if (edgeEnable) {
            NUStmtList thenList;
            thenList.push_back(newStmt);
            NUStmtList elseList;
            newStmt = new NUIf(edgeEnable, thenList, elseList, mNucleusBuilder->gNetRefFactory(), false, loc);
        }
    }
    // Look for simple latches as a special case.
    // If we find a simple latch, then use data/enable logic instead of set/clear
    // Fix for bug 5240.
    bool simpleLatch = 0;

    if( !clock ) {
        simpleLatch = udpAddLevelLatchStatement(outputPort, module, &newStmt, levelSet, levelClear);
    }

    NUExpr* levelClearIdent = NULL;
    NUExpr* levelSetIdent = NULL;
    if( !simpleLatch ) {
        levelClearIdent = udpAddLevelStatement(outputPort, module, &newStmt, levelClear, 0, "udpClear");
        levelSetIdent = udpAddLevelStatement(outputPort, module, &newStmt, levelSet, 1, "udpSet");
    }
    NUStmtList newStmtList;
    newStmtList.push_back(newStmt);

    NUBlock* block = new NUBlock(&newStmtList, module, mNucleusBuilder->gIODB(),
        mNucleusBuilder->gNetRefFactory(), false, loc);

    StringAtom* blockName = module->newBlockName(mNucleusBuilder->gCache(), loc);
    NUAlwaysBlock* always = new NUAlwaysBlock(blockName, block, mNucleusBuilder->gNetRefFactory(), loc, false);
    module->addAlwaysBlock(always);

    // Add edge expressions for clk and async set/reset
    if (clock) {
        ClockEdge edgyEdge = (edge == udpPosedge) ? eClockPosedge : eClockNegedge;
        NUEdgeExpr* clkEdge = new NUEdgeExpr(edgyEdge, clock, loc);
        clkEdge->getNet()->putIsEdgeTrigger(true);
        always->addEdgeExpr(clkEdge);

        if (levelSetIdent) {
            CopyContext cc(0,0);
            NUEdgeExpr* asyncSet = new NUEdgeExpr(eClockPosedge, levelSetIdent->copy(cc), loc);
            asyncSet->getNet()->putIsEdgeTrigger(true);
            always->addEdgeExpr(asyncSet);
        }
        if (levelClearIdent) {
            CopyContext cc(0,0);
            NUEdgeExpr* asyncClear = new NUEdgeExpr(eClockPosedge, levelClearIdent->copy(cc), loc);
            asyncClear->getNet()->putIsEdgeTrigger(true);
            always->addEdgeExpr(asyncClear);
        }
    }
}

bool VerificVerilogDesignWalker::udpAddLevelLatchStatement( 
        VeriTreeNode* outputPort,
        NUModule* module,
        NUStmt** stmt,
        NUExpr* setExpr,
        NUExpr* clearExpr)
{
    if (!setExpr || !clearExpr)
        return 0;

    NUExpr *exprA1, *exprA2, *exprB1, *exprB2;
    NUNet *netA1, *netA2, *netB1, *netB2;

    if (!udpIsBinaryAnd(setExpr, &netA1, &exprA1, &netA2, &exprA2))
        return 0;
    if (!udpIsBinaryAnd( clearExpr, &netB1, &exprB1, &netB2, &exprB2))
        return 0;

    // If the nets are not the same, swap them.
    if (netA1 != netB1) {
        NUNet* net = netB1;
        NUExpr* expr = exprB1;
        netB1 = netB2;
        exprB1 = exprB2;
        netB2 = net;
        exprB2 = expr;
    }
    // The nets have to match.
    if ((netA1 != netB1) || (netA2 != netB2))
        return 0;

    // If the inversions match, 1 is the enable.
    // Swap to make 1 the data and 2 the enable.
    if (exprA1->getType() == exprB1->getType()) {
        NUNet* net = netA1;
        NUExpr* expr = exprA1;
        netA1 = netA2;
        exprA1 = exprA2;
        netA2 = net;
        exprA2 = expr;

        net = netB1;
        expr = exprB1;
        netB1 = netB2;
        exprB1 = exprB2;
        netB2 = net;
        exprB2 = expr;
    }

    // The data expressions must be inverses to match the template.
    if (exprA1->getType() == exprB1->getType())
        return 0;

    // The set/clear expression match the template.
    // Create the data/enable statement.
    const SourceLocator& loc = module->getLoc();

    // Get LHS
    NULvalue* lhs = identLvalue(outputPort, loc);

    CopyContext cc(0,0);
    NUStmt* thenStmt = new NUBlockingAssign(lhs, exprA1->copy(cc), false, loc);
    NUStmtList thenList;
    thenList.push_back(thenStmt);
    NUStmtList elseList;
    // For a simple latch, we should not see any current statement.
    NU_ASSERT(*stmt == NULL, *stmt);

    (*stmt) = new NUIf(exprA2->copy(cc), thenList, elseList, mNucleusBuilder->gNetRefFactory(), false, loc);

    // The set/clear exprs are replaced with data/enable.  Delete them.
    delete setExpr;
    delete clearExpr;
    return 1;
}

bool
VerificVerilogDesignWalker::udpIsBinaryAnd(NUExpr* expr, NUNet** net1, NUExpr** expr1,
    NUNet** net2, NUExpr** expr2 )
{
    if (expr->getType() != NUExpr::eNUBinaryOp)
        return 0;

    NUBinaryOp* op = dynamic_cast<NUBinaryOp*>(expr);
    if (op->getOp() != NUOp::eBiBitAnd)
        return 0;

    *expr1 = op->getArg(0);
    *expr2 = op->getArg(1);

    NUExpr* ident1 = *expr1;
    if ((*expr1)->getType() == NUExpr::eNUUnaryOp) {
        NUUnaryOp* op = dynamic_cast<NUUnaryOp*>(*expr1);
        ident1 = op->getArg(0);
    }

    NUExpr* ident2 = *expr2;
    if ((*expr2)->getType() == NUExpr::eNUUnaryOp) {
        NUUnaryOp* op = dynamic_cast<NUUnaryOp*>(*expr2);
        ident2 = op->getArg(0);
    }

    if ((ident1->getType() != NUExpr::eNUIdentRvalue) ||
            (ident2->getType() != NUExpr::eNUIdentRvalue))
        return 0;

    *net1 = ident1->getWholeIdentifier();
    *net2 = ident2->getWholeIdentifier();

    return 1;
}

NUExpr*
VerificVerilogDesignWalker::udpAddLevelStatement(VeriTreeNode* outputPort, NUModule* module, NUStmt** stmt,
    NUExpr* expr, int value, const char* lhsName)
{
    if (!expr)
        return NULL;

    const SourceLocator& loc = module->getLoc();
    NUExpr* identExpr = NULL;

    // Create a continuous assign if the expr is not an ident.
    if (expr->getType() != NUExpr::eNUIdentRvalue) {
        StringAtom* sym = module->gensym(lhsName);
        NUNet* exprNet = module->createTempNet(sym, 1, false, loc);
        NUIdentLvalue* exprLhs = new NUIdentLvalue(exprNet, loc);
        NUContAssign* newAssign = mNucleusBuilder->cContinuesAssign(module, exprLhs, expr, loc);
        //NUContAssign* newAssign = helperPopulateContAssign( exprLhs, expr, loc,
        //        context );
        module->addContAssign(newAssign);
        identExpr = new NUIdentRvalue(exprNet, loc);
    } else {
        identExpr = expr;
    }
    // Get LHS
    NULvalue* lhs = identLvalue(outputPort, loc);
    //VerilogPopulate::ErrorCode ec = udpMakeLhs( ve_udp, context, &lhs );
    NUExpr* rhs = NUConst::create(false, value, 1, loc);
    NUStmt* thenStmt = new NUBlockingAssign(lhs, rhs, false, loc);
    NUStmtList thenList;
    thenList.push_back( thenStmt );
    NUStmtList elseList;
    if ((*stmt))
        elseList.push_back((*stmt));

    (*stmt) = new NUIf(identExpr, thenList, elseList, mNucleusBuilder->gNetRefFactory(), false, loc);
    return identExpr;
}

bool VerificVerilogDesignWalker::udpLevelListHasX(const UtString& levelList)
{
    unsigned levelIndex = 0;
    UtString level = levelListNextItem(levelList, &levelIndex);
    while (!level.empty()) {
        INFO_ASSERT(1 == level.size(), "Incorrect level symbol type");
        char inputSymbol = level[0];
        if (('x' == inputSymbol)) {
            return true;
        }
        level = levelListNextItem (levelList, &levelIndex);
    }
    return false;
}



bool VerificVerilogDesignWalker::IsOpenPort(VeriTreeNode* node)
{
    VeriExpression* expr = dynamic_cast<VeriExpression*>(node);
    if (expr) {
        return expr->IsOpen();
    }
    return false;
}

NUExpr*
VerificVerilogDesignWalker::createPostIncDec(unsigned operType, VeriExpression* iter, bool isSignedResult, bool isSignedOp, const SourceLocator& loc)
{
    // we have post increment/decrement
    // i++ or i--
    // we populate the following logic
    // create 'temp' net
    // temp = i; // blocking assign
    // i = temp + 1; //blocking assign
    // result = temp; // return expression
    NUScope* parent_scope = getNUScope(iter);
    NUScope::ScopeT parent_scope_type = parent_scope->getScopeType();
    // get Lvalue for iter
    gScope()->sContext(V2NDesignScope::LVALUE);
    iter->Accept(*this);
    NULvalue* iter_lvalue = dynamic_cast<NULvalue*>(gScope()->gValue());
    // get Rvalue for iter
    gScope()->sContext(V2NDesignScope::RVALUE);
    iter->Accept(*this);
    NUExpr* iter_rvalue = dynamic_cast<NUExpr*>(gScope()->gValue());
    // Now we need to create temporary to hold current value of iter expression
    // get name prefix
    UtString prefix;
    if (VERI_INC_OP == operType) {
        prefix = "tempIncNet";
    } else if (VERI_DEC_OP == operType) {
        prefix = "tempDecNet";
    } else {
        VERIFIC_CONSISTENCY(false, *iter, "Incorrect operation type");
    }
    // get original net from expression 
    // currently support only simple nets
    // TODO may need to support complex expression (varsel, memsel, others ?)
    NUIdentRvalue* iter_ident_rvalue = dynamic_cast<NUIdentRvalue*>(iter_rvalue);
    VERIFIC_CONSISTENCY(iter_ident_rvalue, *iter, "Unsupported iter expression for inc/dec");
    NUNet* orig_net = mNucleusBuilder->gNetFromIdentRvalue(iter_ident_rvalue); 
    // create temp net  (preserving type of net and it's flags)
    // block local, temp net
    // TODO may need to adjust flags if orig_net for example is port
    NUNet* temp_net = parent_scope->createTempNetFromImage(orig_net, prefix.c_str(),
        true, false);
    // create lvalue for temp net
    NULvalue* temp_lvalue = mNucleusBuilder->cIdentLvalue(temp_net, loc);
    // create rvalue for temp net
    NUExpr* temp_rvalue = mNucleusBuilder->cIdentRvalue(temp_net, loc);
    // create constant one (may need to pass size of iter instead of 32)
    NUExpr* constantOne = mNucleusBuilder->cConst(1, 32, false, loc);
    // get sub operation (+ for ++, - for --)
    unsigned subOperator = getVerificOperatorForAssignSubOperator(operType);;
    // create binary increment or decrement by one operation
    NUExpr* temp_incdec_by_one = mNucleusBuilder->cBinaryOperation(temp_rvalue, subOperator, constantOne,
        isSignedResult, isSignedOp, loc);
    // create assignments 
    // if we are in module scope then we need to populate continues assign otherwise (TF or Block) blocking assign
    NUStmt* temp_assign_iter_old_value = NULL;
    NUStmt* iter_assign_incdec = NULL;
    if (NUScope::eModule == parent_scope_type) {
        // create continuesAssign for preserving old value of iter expression, this value will be returned as a result
        NUModule* module = parent_scope->getModule();
        VERIFIC_CONSISTENCY(module, *iter, "Incorrect scope type");
        temp_assign_iter_old_value = mNucleusBuilder->cContinuesAssign(module, temp_lvalue, iter_rvalue, loc);
        // create continuesAssign for iter new value,
        iter_assign_incdec = mNucleusBuilder->cContinuesAssign(module, iter_lvalue, temp_incdec_by_one, loc);
    } else {
        VERIFIC_CONSISTENCY((NUScope::eTask == parent_scope_type)
            || (NUScope::eBlock == parent_scope_type), *iter, "Incorrect scope type");

        // create blockingAssign for preserving old value of iter expression,
        temp_assign_iter_old_value = mNucleusBuilder->cBlockingAssign(temp_lvalue, iter_rvalue, loc);
        // create blockingAssign for iter new value,
        iter_assign_incdec = mNucleusBuilder->cBlockingAssign(iter_lvalue, temp_incdec_by_one, loc);
    }
    mNucleusBuilder->aStmtToScope(temp_assign_iter_old_value, parent_scope, loc);
    mNucleusBuilder->aStmtToScope(iter_assign_incdec, parent_scope, loc);
    // return tempNet with newly created expression (rvalue)
    NUExpr* result = mNucleusBuilder->cIdentRvalue(temp_net, loc);
    return result;
}


NUExpr*
VerificVerilogDesignWalker::createPreIncDec(unsigned operType, VeriExpression* iter, bool isSignedResult, bool isSignedOp, const SourceLocator& loc)
{
    //TODO this is a subset of createPostIncDec function, discuss if it's worth having unified function
    // we have pre increment/decrement
    // ++i or --i
    // we populate the following logic
    // i = i + 1; // blocking assign
    // result = i; // return expression
    NUScope* parent_scope = getNUScope(iter);
    NUScope::ScopeT parent_scope_type = parent_scope->getScopeType();
    // get Lvalue for iter
    gScope()->sContext(V2NDesignScope::LVALUE);
    iter->Accept(*this);
    NULvalue* iter_lvalue = dynamic_cast<NULvalue*>(gScope()->gValue());
    // get Rvalue for iter
    gScope()->sContext(V2NDesignScope::RVALUE);
    iter->Accept(*this);
    NUExpr* iter_rvalue = dynamic_cast<NUExpr*>(gScope()->gValue());
    // get original net from expression 
    // currently support only simple nets
    // TODO may need to support complex expression (varsel, memsel, others ?)
    NUIdentRvalue* iter_ident_rvalue = dynamic_cast<NUIdentRvalue*>(iter_rvalue);
    VERIFIC_CONSISTENCY(iter_ident_rvalue, *iter, "Unsupported iter expression for inc/dec");
    NUNet* orig_net = mNucleusBuilder->gNetFromIdentRvalue(iter_ident_rvalue); 
    // create constant one (may need to pass size of iter instead of 32)
    NUExpr* constantOne = mNucleusBuilder->cConst(1, 32, false, loc);
    // get sub operation (+ for ++, - for --)
    unsigned subOperator = getVerificOperatorForAssignSubOperator(operType);;
    // create binary increment or decrement by one operation
    NUExpr* iter_incdec_by_one = mNucleusBuilder->cBinaryOperation(iter_rvalue, subOperator, constantOne,
        isSignedResult, isSignedOp, loc);
    NUStmt* iter_assign_incdec = NULL;
    if (NUScope::eModule == parent_scope_type) {
        NUModule* module = dynamic_cast<NUModule*>(parent_scope);
        VERIFIC_CONSISTENCY(module, *iter, "Incorrect scope type");
        // create continuesAssign for iter new value,
        iter_assign_incdec = mNucleusBuilder->cContinuesAssign(module, iter_lvalue, iter_incdec_by_one, loc);
    } else {
        VERIFIC_CONSISTENCY((NUScope::eTask == parent_scope_type)
            || (NUScope::eBlock == parent_scope_type), *iter, "Incorrect scope type");
        // create blockingAssign for iter new value,
        iter_assign_incdec = mNucleusBuilder->cBlockingAssign(iter_lvalue, iter_incdec_by_one, loc);
    }
    mNucleusBuilder->aStmtToScope(iter_assign_incdec, parent_scope, loc);
    // return iter with newly created expression (rvalue)
    NUExpr* result = mNucleusBuilder->cIdentRvalue(orig_net, loc);
    return result;
}

void
VerificVerilogDesignWalker::HandleSVCompoundAssignOperator(unsigned oper_type, VeriExpression* ve_left, VeriExpression* ve_right, 
    int context_size_sign, unsigned lhs_sign, unsigned rhs_sign, const SourceLocator& sourceLocation, NULvalue** left, NUExpr** right)
{
    // Here we populate assign subOperation as a binary operation
    // and return computed left and right nucleus objects
    // get sub operation e.g. if node.OperType == '+=" then subOperator will be '+' 
    unsigned subOperator = getVerificOperatorForAssignSubOperator(oper_type);
    // get both rvalue and lvalue as expression
    NUExpr* op1 = 0;
    bool isPreIncrDecrOperator = false;
    if (ve_left) {
        gScope()->sContext(V2NDesignScope::RVALUE);
        ve_left->Accept(*this);
        op1 = dynamic_cast<NUExpr*>(gScope()->gValue());
    } else {
        // create constant one (TODO may need to set context size for constant)
        // constant 1 should be in the right side always
        op1 = mNucleusBuilder->cConst(1, 32, false, sourceLocation);
        isPreIncrDecrOperator = true;
        // if lval is missing (it should be pre increment/decrement case) - I don't know other operator, so asserting
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(ve_right && isIncDecOperator(oper_type),
            sourceLocation, "Expected pre increment/decrement operator only"); 
        // in that case we should take value and get Lvalue for it
        gScope()->sContext(V2NDesignScope::LVALUE);
        ve_right->Accept(*this);
        *left = dynamic_cast<NULvalue*>(gScope()->gValue());
    }
    NUExpr* op2 = 0;
    if (ve_right) {
        gScope()->sContext(V2NDesignScope::RVALUE);
        VeriExpression* evalRvalue = evalConstExpr(context_size_sign, ve_right);
        // evalRvalue will be Null if node is not a constant expression
        if (evalRvalue) {
            evalRvalue->Accept(*this);
            delete evalRvalue;
        } else { 
            ve_right->Accept(*this) ;
        }
        op2 = dynamic_cast<NUExpr*>(gScope()->gValue());
    } else {
        // if node.GetValue() is missing (it should be post increment/decrement case) - I don't know other operator, so asserting
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(ve_left && isIncDecOperator(oper_type),
                sourceLocation, "Expected post increment/decrement operator only"); 
        // create constant one (TODO may need to set context size for constant)
        op2 = mNucleusBuilder->cConst(1, 32, false, sourceLocation);
    }
    if (isPreIncrDecrOperator) {
        // swap op1/op2 operands because for ++ and -- pre increment unary operators Verific does not store a Lvalue and so we put the constant 1 there
        // but we want to populate foo = foo op 1; (not f = 1 op foo)
        NUExpr* temp = op1;
        op1 = op2;
        op2 = temp;
    }
    // Here we should already have op1 and op2 so asserting
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(op1, sourceLocation, "Unable to get op1"); 
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(op2, sourceLocation, "Unable to get op2"); 
    bool isSignedResult = (1 == GET_CONTEXT_SIGN(context_size_sign)); // 1 means signed result, 0 unsigned
    bool use_signed_binary_operator = useSignedBinaryOperator(subOperator, lhs_sign, rhs_sign);
    // when lvalueIsConstOne --i, ++i operator case we need lvalue appear in the right side of binary operation (in order not to have cases like 1 - rvalue)
    *right = mNucleusBuilder->cBinaryOperation(op1, subOperator, op2, isSignedResult, use_signed_binary_operator,
            sourceLocation);
}

void
VerificVerilogDesignWalker::HandleSVBlockingAssign(VeriBlockingAssign* node, int context_size_sign, unsigned lhs_sign, unsigned rhs_sign, const SourceLocator& sourceLocation, NULvalue** left, NUExpr** right)
{
    HandleSVCompoundAssignOperator(node->OperType(), node->GetLVal(), node->GetValue(), context_size_sign, lhs_sign, rhs_sign, sourceLocation, left, right);
}

void
VerificVerilogDesignWalker::HandleSVBinaryOperator(VeriBinaryOperator* node, int context_size_sign, unsigned lhs_sign, unsigned rhs_sign, const SourceLocator& loc, NUExpr** result)
{
    // Function handles part of (a = (b |= 4)), where (b |= 4) actually comes as a binary operation, which gets devided into 2
    // operations as follows: 
    // b_lval = b_rval | 4;
    // b_new_rval = b_lval->getNet()

    // get parent scope 
    NUScope* parent_scope = getNUScope(node);
    NUScope::ScopeT parent_scope_type = parent_scope->getScopeType();
    // Set left expression context
    gScope()->sContext(V2NDesignScope::LVALUE);
    // Get left expression 
    node->GetLeft()->Accept(*this) ;
    NULvalue* left = dynamic_cast<NULvalue*>(gScope()->gValue());
    // Although we use HandleSVCompoundAssignOperator  common function, increment/decrement operations for binary operator handled specially,  so here we can assert that lvalue always exist
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(left, *node, "Unable to get lvalue");
    NUExpr* right = NULL;
    // Populate binary operation, return left, right parts assignment ready
    HandleSVCompoundAssignOperator(node->OperType(), node->GetLeft(), node->GetRight(), context_size_sign, lhs_sign, rhs_sign, loc, &left, &right);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(right, *node, "Unable to get right expression");
    // Populate continuesAssign/blockingAssign depending on scope type
    NUStmt* bin_op_assign = NULL;
    if (NUScope::eModule == parent_scope_type) {
        NUModule* module = dynamic_cast<NUModule*>(parent_scope);
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, *node, "Incorrect scope type");
        bin_op_assign = mNucleusBuilder->cContinuesAssign(module, left, right, loc);
    } else {
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE((NUScope::eTask == parent_scope_type)
            || (NUScope::eBlock == parent_scope_type), *node, "Incorrect scope type");
        bin_op_assign = mNucleusBuilder->cBlockingAssign(left, right, loc);
    }
    mNucleusBuilder->aStmtToScope(bin_op_assign, parent_scope, loc);
    // get original net from lvalue 
    // currently support only simple nets
    // TODO may need to support complex lvalues (varsel, memsel, others ?)
    NUIdentLvalue* ident_lvalue = dynamic_cast<NUIdentLvalue*>(left);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(ident_lvalue, *node, "Unsupported left operator for binary operation");
    NUNet* orig_net = mNucleusBuilder->gNetFromIdentLvalue(ident_lvalue); 
    // Result should be rvalue 
    *result = mNucleusBuilder->cIdentRvalue(orig_net, loc);
}

bool
VerificVerilogDesignWalker::useSignedBinaryOperator(unsigned opType, unsigned lhs_sign, unsigned rhs_sign)
{
    bool isBothOpsSigned = lhs_sign && rhs_sign;
    switch(opType) {
    // the verilog LRMs state that the signedness of shift operators are determined only by the signedness of the left operand (the signedness of the right operand does not matter)
    case VERI_RSHIFT:
    case VERI_LSHIFT:
    case VERI_ARITLSHIFT:
    case VERI_ARITRSHIFT:
        return lhs_sign;    
    default:
        return isBothOpsSigned;
    }
}

//! analyze the verific range information in \a packedDims and \a unpackedDims, to prepare for use to populate nucleus 
/*! \retval dimType  the type of the dimensions, one of MEMORY, VECTOR, BIT
 *  \retval assignBitWidth  the width that can be used if an object of this type is assigned to
 *  \retval dims  an array that holds the dimensions, ordered in the 'carbon' order
 *  \retval firstUnpackedDim the index into \a dims for the first unpacked dimension, all entries in \a dims with this index (or larger) are *  unpacked dimensions
 */
void
VerificVerilogDesignWalker::parseDimensions(VeriDataType* dataType, VeriIdDef* id, const SourceLocator& sourceLocation, DimType* dimType, unsigned* assignBitWidth, UtVector<UtPair<int,int> >* dims, unsigned* firstUnpackedDim)
{
    // collect packed dimensions (if any)
    // check for non-explicit packed dimensions first
    // The list of types that we look for here are identified in the 1364-2005 LRM, section 3.2.1 (net declaration sytax), pg 20,
    // and section 3.2.2 (variable declaration syntax), pg 22.
    //defautl to 1 in case 'dataType' is NULL
    *assignBitWidth = 1;
    if (dataType) {
        if ((dataType->GetType() == VERI_INTEGER) || (dataType->GetType() == VERI_INT) /* || (dataType->GetType() == VERI_SHORTREAL)*/) {
            *assignBitWidth = 32;
        } else if (dataType->GetType() == VERI_BYTE) {
            *assignBitWidth = 8;
        } else if (dataType->GetType() == VERI_SHORTINT) {
            *assignBitWidth = 16;
        } else if (dataType->GetType() == VERI_LONGINT) {
            *assignBitWidth = 64;
        } else if ((dataType->GetType() == VERI_TIME) || (dataType->GetType() == VERI_REAL) || (dataType->GetType() == VERI_REALTIME)) {
            *assignBitWidth = 64;
        } else {
            // Must be untyped, or VERI_BIT, VERI_LOGIC, VERI_REG, VERI_WIRE, VERI_TRI, VERI_TRI0,
            // VERI_TRI1, VERI_TRIAND, VERI_TRIOR, VERI_TRIREG, VERI_WAND
            // VERI_WOR, VERI_SUPPLY1, or VERI_SUPPLY0
            *assignBitWidth = 1;
        }
    }
    UtVector<UtPair<int,int> > packedDimensions;
    // push non-explicit packed dimension first (if not 1 size)
    if (*assignBitWidth != 1) {
        UtPair<int,int> dim;
        dim.first = *assignBitWidth - 1;
        dim.second = 0;
        packedDimensions.push_back(dim);
    }
    UtVector<UtPair<int,int> > unpackedDimensions;
    // if we have id than pick number from it, otherwise use dataType, if we don't have both we don't have any dimensions
    unsigned numberOfUnpackedDims = id ? id->UnPackedDimension() : dataType ? dataType->UnPackedDimension() : 0;
    // accumulate explicit packed and unpacked dimensions (if any)
    accumulatePackedUnpackedDimensions(id, dataType, numberOfUnpackedDims, sourceLocation, assignBitWidth, &packedDimensions, &unpackedDimensions);

    // In order to handle objects that can use memory style addressing in a consistent manner we might need to add
    // an implicit dimension so that this object will be handled as a NUMemoryNet.
    if ( (packedDimensions.size() == 0) && (unpackedDimensions.size() != 0) ) {
      // in this case where we have something like:
      //   reg       netDeclaredWithNoPackedDimensions [7:0];
      // and populate it with an implicit packed dimension, as if it were declared as:
      //   reg [0:0] netDeclaredWithNoPackedDimensions [7:0];
      // this will force the creation of a NUMemoryNet and allow the proper population of references to this net.
      // It is fortunate that no other expression involviing this NUMemoryNet needs to be modified.
      packedDimensions.push_back(UtPair<int,int>(0,0));
    }

    // The order of dimensions expected by caller is the same as the order in NUMemoryNet::mRanges. (see comment for NUmemoryNet::mRanges)
    //  It is from least significant to most significant (thus the innermost range is first).
    // Here we change the order of packedDimensions and unpackedDimensions from the verilog declaration order, to be the order required for mRanges 
    std::reverse(packedDimensions.begin(), packedDimensions.end());
    std::reverse(unpackedDimensions.begin(), unpackedDimensions.end());
    
    // combine the packed and unpacked dimension vector into a single vector for use by the caller
    dims->insert(dims->end(), packedDimensions.begin(), packedDimensions.end());
    dims->insert(dims->end(), unpackedDimensions.begin(), unpackedDimensions.end());

    // firstUnpackedDim is a zero based index so the size of the packedDimension array is the index of the first unpacked dimension in dims
    *firstUnpackedDim = packedDimensions.size();

    // set dimension type
    if (dims->size() > 1) {
        *dimType = MEMORY;
    } else if (dims->size() == 1) {
        *dimType = VECTOR;
    } else {
        *dimType = BIT;
    }
}

VeriRange* VerificVerilogDesignWalker::findNthRange(VeriIdDef* id, VeriDataType* dataType, unsigned n)
{
    // in case 'n' is out of bounds GetDimensionAt(n) will return NULL
    if (id) return id->GetDimensionAt(n); // if we have id, it will contain full info
    else if (dataType) return dataType->GetDimensionAt(n); // otherwise use dataType info
    else return 0; // else we don't have any range
}

void
VerificVerilogDesignWalker::accumulatePackedUnpackedDimensions(VeriIdDef* id, VeriDataType* dataType, unsigned numberOfUnpackedDims, const SourceLocator& sourceLocation,
    unsigned * assignBitWidth, UtVector<UtPair<int,int> >* packedDims,  UtVector<UtPair<int,int> >* unpackedDims)
{
    //last 3 arguments of this function needs to be already initialized, so here we just accumulate dimensions explicitely specified in HDL to an existing containers
    // in Verific range represents joined {unpacked,packed} dimensions with 0 index having outermost dimension and last is inner most dimension
    VeriRange* range = findNthRange(id, dataType, 0); // get first range - if any
    if (range) { 
        UtPair<int,int> dim;
        unsigned n = 0;
        while (range) {
            gRange(&dim, *range, range->RangeWidth(), false, "", 0, 0, &sourceLocation);
            if (hasError()) return;
            if (n < numberOfUnpackedDims) { // if true this is still unpacked dimension
                // add to unpacked dimensions
                unpackedDims->push_back(dim);
            } else {
                // add to packed dimensions
                *assignBitWidth *= range->RangeWidth();
                packedDims->push_back(dim);
            }
            // find next range if any
            ++n; 
            range = findNthRange(id, dataType, n); 
        }
    }
}

// Adapting Interra VHDL approach
// This is partselect on a MemoryNet. Break it up into separate
// MemoryNet bit selects (or NUMemselRvalue, NUMemselLvalue bit selects in case of multi-index acces) and concat them together. 
// Handles constant and variable range access cases
// Ex.1 Constant case example
//
//      foo = mem[101][12:10];
// 
// this will be transformed to
//
//      foo = {mem[101][12],mem[101][11],mem[101][10]};
//
// Ex.2 Variable case example
//
//      foo = mem[1][start-:2] ;
//
// this will be transformed to 
//
//      foo = {mem[1][start + 0], mem[1][start + -1]}
//
// 'node' - Verific VeriIndexedId or VeriMemoryIndexedId expression (TODO: 'node' just used in VERIFIC_CONSISTENCY macro checks, so replace it with NUCLEUS_CONSISTENCY macro as 'loc' object also available in this function)
// 'index' - Verific indexing expression (constant or non-constant VeriRange) of VeriIndexedId or VeriMemoryIndexedId
// 'indexExpr' - Nucleus populated index expression, we use copies of this expression (to populate each bit-select) and at the end delete original version (which is unused) 
// 'normalizedIndices' - normalized indices (normally returned by gRange() helper method) which correspond nucleus requirement of being adjusted to normalize form (width - 1, 0), although this has many cases and not always normalization needed (so depending on arguments gRange may return unnormalized indices) (Ex1 [12,10] , Ex2 [0, -1])
// 'selObject' - can be either NUMemoryNet or selection expressions of NUMemoryNet (NUMemselRvalue or NUMemselLvalue). In the second case it represents prefix part of populated selection object (VeriIndexedMemoryId case) represents all but last indices populated expression, and so we are using it's copies to populate bit-selects and at the end we delete unused original selObject (mem[101] in the Ex.1)
// 'contextType' - either rvalue or lvalue case (in both Ex1 and Ex2 it's RVALUE, if same expression appeared in left side of assignment 'contextType' will be LVALUE
// 'inverse' - as we populate non-constant bit select expressions we need to now which direction we should go (increment, or decrement), when inverse is false we increment MSB by 1 otherwise, in case of 'inverse' true value - increment value will be -1 (which really is decrement) (Ex.2 determines -1 in popoulated binary operation [start + -1]
// 'isNonConstRangeSel' - indicates case where we have non constant range select (Ex2. it's true, for Ex1 it's false)
// 'loc' - expression location object
NUBase*
VerificVerilogDesignWalker::genMemoryPartSelect(VeriExpression* node, VeriExpression* index, NUExpr** indexExpr, const UtPair<int,int>& normalizedIndices, NUBase* selObject, unsigned contextType, bool inverse, bool isNonConstRangeSel, const SourceLocator& sourceLocation)
{
    NUBase* result = NULL;
    VeriRange* range = dynamic_cast<VeriRange*>(index);
    VERIFIC_CONSISTENCY(range, *node, "Inconsistency: index isn't VeriRange");
    // For constant range select ((2:5), (2+:3), (5-:3) etc.) we use normalized indices
    UInt32 selWidth = range->RangeWidth();
    SInt32 selMsb = normalizedIndices.first;
    SInt32 incr = -1;
    if (isNonConstRangeSel) {
        // for non constant range case ( (i+:3), (j-:4) etc.)
        // TODO: this logic needs comprehensive testing
        selMsb = 0;
        incr = inverse ? 1 : -1;
    } else {
      incr = (normalizedIndices.first < normalizedIndices.second) ? 1 : -1;
    }
    
    NUExprVector expr_vector;
    NULvalueVector lvalue_vector;
    CopyContext cc(0,0);
    NUMemoryNet* nMemoryNet = dynamic_cast<NUMemoryNet*>(selObject);
    NUMemselRvalue* memSelRvalue = dynamic_cast<NUMemselRvalue*>(selObject);
    NUMemselLvalue* memSelLvalue = dynamic_cast<NUMemselLvalue*>(selObject);
    for (UInt32 i = 0; i < selWidth ; i++)
    {
        // The index_expr plus the index address forms the index select.
        NUExpr* offset_expr = (NUExpr*)NUConst::create(true, selMsb, 32, sourceLocation);
        NUExpr* addr_expr = NULL;
        if (*indexExpr) {
            addr_expr = new NUBinaryOp(NUOp::eBiPlus, (*indexExpr)->copy(cc), offset_expr, sourceLocation); // variable range selection case
        } else {
            addr_expr = offset_expr; // constant range selection case
        }
        // here we have guarantee of single select expression (multi-cases handled by VeriIndexedMemoryId visitor)
        if (V2NDesignScope::LVALUE == contextType) {
            // either it's memory net or memory select lvalue 
            VERIFIC_CONSISTENCY(nMemoryNet || memSelLvalue, *node, "Incorrect selected object type");
            NULvalue* lv = NULL;
            if (nMemoryNet) {
                lv = mNucleusBuilder->cMemselLvalue(nMemoryNet, addr_expr, sourceLocation); 
            } else {
                // we need to copy and modify copied memsel object (at the end will delete original one)
                // the reason is that we do create concatenation of multiple single bit select objects and
                // the prefix part (memSel object) needs to be unique for each concat item.
                CopyContext ccm(0,0);
                NUMemselLvalue* copy_memSelLvalue = dynamic_cast<NUMemselLvalue*>(memSelLvalue->copy(ccm));
                copy_memSelLvalue->addIndexExpr(addr_expr); 
                lv = copy_memSelLvalue; 
            }
            lvalue_vector.push_back(lv);
        } else if (V2NDesignScope::RVALUE == contextType) {
            // either it's memory net or memory select expression 
            VERIFIC_CONSISTENCY(nMemoryNet || memSelRvalue, *node, "Incorrect selected object type");
            NUExpr* rv = NULL;
            if (nMemoryNet) {
                rv = mNucleusBuilder->cMemselRvalue(nMemoryNet, addr_expr, sourceLocation); 
            } else { 
                // we need to copy and modify copied memsel object (at the end will delete original one)
                // the reason is that we do create concatenation of multiple single bit select objects and
                // the prefix part (memSel object) needs to be unique for each concat item.
                // consider the following example
                //
                //      foo = mem[101][12:10];
                // 
                // this will be transformed to
                //
                //      foo = {mem[101][12],mem[101][11],mem[101][10]};
                // 
                // We need unique copy of each memory selection object, in order todo transform by adding
                // one more index (12, 11, 10) for each 
                //
                CopyContext ccm(0,0);
                NUMemselRvalue* copy_memSelRvalue = dynamic_cast<NUMemselRvalue*>(memSelRvalue->copy(ccm));
                copy_memSelRvalue->addIndexExpr(addr_expr); 
                rv = copy_memSelRvalue; 
            }
            expr_vector.push_back(rv);
        } else {
            VERIFIC_CONSISTENCY(false, *node, "selection context is not set");
        }
        selMsb += incr;
    }
    if (V2NDesignScope::LVALUE == contextType) {
        result = mNucleusBuilder->cConcatLvalue(lvalue_vector, sourceLocation); 
    } else if (V2NDesignScope::RVALUE == contextType) {
        result = mNucleusBuilder->cConcatRvalue(expr_vector, 1, sourceLocation);
    } else {
        VERIFIC_CONSISTENCY(false, *node, "selection context is not set");
    }
    delete *indexExpr; // We've used it's copies.
    *indexExpr = NULL;
    if (memSelLvalue || memSelRvalue) {
        delete selObject; // We've used it's copies.
    }
    return result;
}
            
bool
VerificVerilogDesignWalker::isInverseNonConstRangeSelect(VeriRange* index, const UtPair<int,int>& declRange)
{
    // function intention is to determine whether variable range should be accessed with increment or decrement order, starting from left expression (here range is asserted to be - "variable range select")
    VERIFIC_CONSISTENCY(!index->GetLeft()->IsConstExpr(), *index, "Inconsistency: only non-consant range selects allowed here");
    VERIFIC_CONSISTENCY(index->GetPartSelectToken(), *index, "Inconsistency: part select token should exist in this context");
    ConstantRange cr(declRange.first, declRange.second);
    if ((cr.bigEndian() && (index->GetPartSelectToken() == VERI_PARTSELECT_UP)) || (!cr.bigEndian() && (index->GetPartSelectToken() == VERI_PARTSELECT_DOWN))) {
        return true;
    }
    return false;
}

bool VerificVerilogDesignWalker::allModulesHaveTimeScaleSpecified()
{
    UtVector<bool>::iterator it = mTimeScaleRegistry.begin();
    for (; it != mTimeScaleRegistry.end(); ++it) {
        if (! (*it)) {
            return false;
       }
    }
    return true;
}

bool VerificVerilogDesignWalker::allModulesDoNotHaveTimeScaleSpecified()
{
    UtVector<bool>::iterator it = mTimeScaleRegistry.begin();
    for (; it != mTimeScaleRegistry.end(); ++it) {
        if (*it) {
            return false;
        }
    }
    return true;
}

// Method: createBlockAndPushScope
// This method creates and pushes a unnamed block scope. It is used on the begin portion of a begin/end.
// It's also used to create temporary block scopes to contain task enables generated by
// the VeriFunctionCall visitor (see comments there).
NUBlock*
VerificVerilogDesignWalker::createBlockAndPushScope(VeriTreeNode* node, const SourceLocator& sourceLocation, const char* name)
{
    NUBlock* block = mNucleusBuilder->cBlock(getNUScope(node), sourceLocation);
    V2NDesignScope* current_scope = new V2NDesignScope(block, gScope());
    // Push the scope
    StringAtom* scopeNameId = mNucleusBuilder->gNextUniqueName(getNUScope(node), name);
    UtString scopeName(scopeNameId->str());
    mNameRegistry[node] = scopeNameId;
    PushScope(current_scope, scopeName);

    return block;
}

// Method: deleteBlockAndPopScope
void
VerificVerilogDesignWalker::deleteBlockAndPopScope(NUBlock* block)
{
  // The block must exist
  INFO_ASSERT(block, "Unexpected NULL block");
  // And should be empty
  INFO_ASSERT(block->loopStmts().atEnd(), "Block not empty");
  PopScope();
  delete block;
}

void
VerificVerilogDesignWalker::reportUnsupportedLanguageConstruct(const SourceLocator& sourceLocation, const UtString& msg)
{
    // report message and set error state
    mNucleusBuilder->reportUnsupportedLanguageConstruct(sourceLocation, msg.c_str());
    // in each error situation we should set null as a value - in order not to populate old value (avoid side-effects)
    gScope()->sValue(NULL);
}

// Interpret Verific strength enum values, and return the '1' value
// and the '0' value. Defaults to VERI_STRONG1 and VERI_STRONG0.
static bool sIsStrength1(unsigned lval)
{
  bool result = false;
  
  if (   lval == VERI_SUPPLY1 
      || lval == VERI_STRONG1
      || lval == VERI_PULL1
      || lval == VERI_WEAK1
      || lval == VERI_HIGHZ1 ) {
    result = true;
  }

  return result;
}

static bool sIsStrength0(unsigned lval)
{
  bool result = false;
  
  if (   lval == VERI_SUPPLY0 
      || lval == VERI_STRONG0
      || lval == VERI_PULL0
      || lval == VERI_WEAK0
      || lval == VERI_HIGHZ0 ) {
    result = true;
  }

  return result;
}

unsigned
VerificVerilogDesignWalker::getStrength1(unsigned lval, unsigned rval)
{
  if (sIsStrength1(lval))
    return lval;
  else if (sIsStrength1(rval))
    return rval;
  else return VERI_STRONG1;
}
  

unsigned
VerificVerilogDesignWalker::getStrength0(unsigned lval, unsigned rval)
{
  if (sIsStrength0(lval))
    return lval;
  else if (sIsStrength0(rval))
    return rval;
  else return VERI_STRONG0;
}

void VerificVerilogDesignWalker::createParamModuleMap(const char* fileRoot, MsgContext* msgContext)
{
    mParamModuleMap = new ParamModuleMap(fileRoot, msgContext);
}

void VerificVerilogDesignWalker::destroyParamModuleMap()
{
    delete mParamModuleMap;
}

void VerificVerilogDesignWalker::getParamValueString(ParamValue* paramVal, VeriExpression* expr)
{
    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,expr->Linefile());
    VeriBaseValue* exprVal = expr->StaticEvaluate(0, 0, 0, 1);
    veri_value_type exprType = exprVal->Type();
    paramVal->putType(exprType);
    UtString& buf = paramVal->getStr();

    // Note that this size is incorrect for Verilog strings, that will be fixed up below.
    unsigned size = GET_CONTEXT_SIZE(exprVal->StaticSizeSign());
    paramVal->putSize(size);
    UtString valStr;
    // Is the string a Verilog based number?  If not, then it is just a string of values.
    bool basedNumberString = false;
    bool useDouble = false;
    switch(exprType)
    {
        case INTEGER: {
            int val = exprVal->GetIntegerValue();
            paramVal->putValue(val);
            delete exprVal;
            }
            return; 
        case ASCIISTR:
            valStr = exprVal->GetString(); 
            break;
        case BASEDNUM:
            if (paramVal->doConvertBasedNumber()) {
                unsigned unusedSign = 0;
                char* valChar = exprVal->GetBinaryValue(&unusedSign); 
                valStr = valChar;
                Strings::free(valChar);
            } else {
                // String will be of the form "<size>'<base><val>", and not just a raw value string.
                valStr = exprVal->GetString(); 
                basedNumberString = true;
            }
            break;
        case REAL: {
            double valDbl = exprVal->GetRealValue();
            paramVal->putDbl(valDbl);
            //paramVal->putSize(veExprEvaluateWidth(expr));
            useDouble = true;
            }
            break;
        default:
            getMessageContext()->NonStaticParameter(&loc);
            setError(true);
            break;
    }
    if (useDouble) {
        // we need to change the double to binary. This
        // is not difficult. Simply copy the double to a UInt64, then use
        // writeBinValToStr
        CarbonReal curValue = paramVal->getDblValue();
        UInt64 castValue = 0;
        CarbonValRW::cpSrcToDest(&castValue, &curValue, 2);
        // a 64 bit value cannot be greater than 64 bits + \0
        char convBuf[65];
        int convertStat = CarbonValRW::writeBinValToStr(convBuf, 65, &castValue, 64, false);
        // This will never fire unless someone changes the buffer size.
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(convertStat != -1, *expr, "Double value conversion to binary failed.");
        buf.assign(convBuf, 64);
    } else {
        size = valStr.size();
        paramVal->putSize(size);
        if ((valStr[0] == '"') && (valStr[size - 1] == '"')) {
            // Note that the size returned by veExprEvaluateWidth is incorrect for Verilog strings,
            // so we don't check that the strlen matches veExprEvaluateWidth.

            // Handle Verilog strings
            if (exprType != ASCIISTR) {
                getMessageContext()->VerificConsistency(&loc, "Bad string in parameter");
                return; 
            }

            // Remove the leading and trailing quotation marks.
            size -= 2;
            buf.assign(valStr[1], size);
            paramVal->putSize(size);

        } else {
            // Put the string into the parameter.
            buf.assign(valStr.c_str(), size);


            if (basedNumberString) {
                // For a based number string, cannot make the size smaller and do not try to evaluate into
                // a 32-bit integer since don't want to parse the based number for special values.
            } else {
                // For types other than Verilog strings and based numbers, the veExprEvaluateWidth size should
                // match the string length.
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(paramVal->getSize() == size, *expr, "Parameter size doesn't match with string value length.");

                bool xzFlag = exprVal->HasXZ();

                // Try to evaluate to an integer value if the size is small enough and this is not an xz binary value.
                if (not xzFlag and (size <= 32)) {
                    UInt32 val = exprVal->Get64bitUnsigned(); // do not use (veExprEvaluateValue(expr)) here, it will not return the
                    // correct value if expr is a part select of a generate FOR index
                    // variable (bug 16445)
                    paramVal->putValue(val);
                }
                if (! paramVal->doNoValOpt()) {
                    doValOpt(buf, size);
                    paramVal->putSize(size);
                }
            }
        }
    }
    delete exprVal;
}

void VerificVerilogDesignWalker::doValOpt(UtString& buf, UInt32& size)
{
    UtString::size_type pos = buf.find_first_not_of("0");
    if (pos != UtString::npos) {
        size -= pos;
        buf.erase(0, pos);
    }
}


void VerificVerilogDesignWalker::getParameterRange(VeriExpression* leftExpr, VeriExpression* rightExpr, ConstantRange* range)
{
    ParamValue leftVal;
    ParamValue rightVal;

    getParamValueString(&leftVal, leftExpr);

    getParamValueString(&rightVal, rightExpr);

    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,leftExpr->Linefile());
    if (not leftVal.hasValidParamVal() or not rightVal.hasValidParamVal()) {
        getMessageContext()->CannotEvaluateParameterRange(&loc);
    } else {
        range->setMsb(leftVal.getParamVal());
        range->setLsb(rightVal.getParamVal());
    }
}

void VerificVerilogDesignWalker::analyzeInstanceParameters(VeriModule* ve_module, UtString* modOrigName)
{
    /// need module location
    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,ve_module->Linefile());
    // No parameters on udps.
    if (ve_module->GetId()->IsUdp())
        return ;
    Array* formalParmList = ve_module->GetParameters();
    if (formalParmList)
    {
        // We definitely have a parameterized module instance

        // We only write the initial comment to the libdesign.parameters
        // file if we have parameterized modules. 
        mParamModuleMap->maybeWriteInitialComments();

        // Get the unique modules already created from this parameterized
        // module
        ParamModuleMap::ParamListObj* paramList = mParamModuleMap->getParamList(*modOrigName);
        // Used to get to each ParamListObj::Element in an initialized paramList. 
        UInt32 paramIndex = 0;

        // Used for output file indentation
        UtString indent("     ");
        // Used to express the parameter values in the output file
        UtString elemValues;

        VeriIdDef* formalParm = NULL;
        unsigned pIndex = 0;
        FOREACH_ARRAY_ITEM(formalParmList, pIndex, formalParm) {
            bool overrideToBinary = false; 
            UtString modNameAppend;
            VeriExpression* curExpr = formalParm->GetInitialValue();

            VeriBaseValue* formalParmVal = curExpr->StaticEvaluate(0, 0, 0, 1);
            if (not formalParmVal) {
                getMessageContext()->VerificConsistency(&loc, "No expression for parameter");
                setError(true);
                return;
            }
            ParamModuleMap::Element* paramElement = NULL;

            // If this parameter list was encountered before, it has been
            // initialized and we don't have to query Verific for the
            // parameter name.
            if (! paramList->isInitialized()) {
                UtString formal_name_buf(gVisibleName(formalParm, formalParm->GetName()));
                paramElement = paramList->addElement(formal_name_buf);
            } else {
                paramElement = paramList->getElement(paramIndex);
            }

            ++paramIndex;
            
            // Interra supports just single range for parameters (probably this is true only for verilog 95/2001 )
            /// TODO support multiple ranges (SV parameter types) 
            VeriRange* range = formalParm->GetDimensionAt(0); // returns the MSB dimension.
            ParamValue actualParam;

            UtPair<int,int> dim;
            if (!range) {
                // If no range is specified, parameter takes size of value applied,
                // so do not do any value optimization to make the parameter value canonical,
                // or else would lose the actual size.
                actualParam.putDoNoValOpt(true);
            }

            // WARNING: for 'actual' string parameter values that are too large to fit
            // in the parameter, 'getParamValueString' returns the parameter value AFTER it has been resized to
            // fit. As a consequence, the 'addStringComment' boolean below will not be set
            // to true, and we will not add a comment field to the libdesign.parameters file.
            // See bug 16682.
            getParamValueString(&actualParam, curExpr);

            UInt32 rangeSize = 0;
            ConstantRange formalRange;
            if (range)
            {
                VeriExpression* leftExpr = range->GetLeft();
                VeriExpression* rightExpr = range->GetRight();


                getParameterRange(leftExpr, rightExpr, &formalRange);
                rangeSize = formalRange.getLength();

                // Strings are handled independently.
                if (actualParam.getType() != ASCIISTR) {
                    // need to size the passed parameters
                    UtString paramBinary;

                    //size the binary strings
                    {
                        UtString& actualStr = actualParam.getStr();
                        UtString::size_type paramSizeStr = actualStr.size();
                        if (paramSizeStr < rangeSize) {
                            paramBinary.append(rangeSize - paramSizeStr, '0');
                            paramBinary.append(actualStr);
                        } else if (paramSizeStr > rangeSize) {
                            paramBinary.append(actualStr, paramSizeStr - rangeSize, rangeSize);

                            // reset the string to the new binary value since it has
                            // been cut.
                            actualStr.assign(paramBinary);

                            // For safety, invalidate any values.
                            actualParam.invalidateParamVal();
                            actualParam.invalidateDblVal();

                            // Whenever we cut an actual and there is a difference
                            // in parameters we write out the binary value (as
                            // opposed to decimal)
                            overrideToBinary = true;
                        } else {
                            paramBinary.assign(actualStr);
                        }
                    }

                    // Make sure the strings are consistently sized
                    UtString::size_type paramBinarySize = paramBinary.size();
                    if (paramBinarySize != rangeSize) {
                        getMessageContext()->VerificConsistency(&loc, "Bad parameter size");
                        setError(true);
                        return;
                    }
                } // if not a string

                // must reset paramSize to context size
                actualParam.putSize(rangeSize);
            }

            /* Decide whether or not to add // <orig string> to output file
               for this parameter.

               We add a trailing comment if the string has to be 0 extended
               or truncated.

               NOTE: See WARNING about getParamValueString above.
             */
            bool addStringComment = true;
            if (actualParam.getType() == ASCIISTR) {
                const UtString& paramStr = actualParam.getStr();

                if (range == NULL) {
                    // The parameter is not bound to a specific size
                    modNameAppend << "\"" << paramStr << "\"";
                    addStringComment = false;
                } else {
                    // The value is bound to a bit range.
                    UInt32 paramStrSize = paramStr.size();
                    UInt32 strBinWidth =  paramStrSize * 8;
                    UInt32 contextSize = actualParam.getSize();
                    if (strBinWidth < contextSize) {
                        // 0's will be prepended to the str value
                        modNameAppend << "{" << contextSize - strBinWidth << "'b0, \"" << paramStr << "\"}";
                    } else if (strBinWidth > contextSize) {
                        UInt32 contextMod = contextSize % 8;
                        UInt32 numCharsFit = contextSize/8;
                        if (contextMod == 0) {
                            // context size is aligned.
                            const char* s = paramStr.c_str();
                            s += paramStrSize - numCharsFit;
                            // just truncate
                            modNameAppend << "\"" << s << "\"";
                        } else {
                            // context size is not aligned
                            unsigned unusedSign = 0;
                            char* fpv = formalParmVal->GetBinaryValue(&unusedSign);
                            //UtString  strBinaryVal(Strings::save(fpv));
                            LOC_ASSERT(strlen(fpv) == strBinWidth, loc);

                            // need to use the binary value to fill out any value
                            // holes.
                            // We want to give as much information as possible to the
                            // user in libdesign.parameters, so only use as much of
                            // the binary value as needed.

                            if (numCharsFit == 0) {
                                const char* s = fpv; 
                                s += strBinWidth = contextSize;
                                modNameAppend << contextSize << "'b" << s;
                            }
                            else
                            {
                                const char* s = fpv;
                                // just get the needed binary bits
                                s += strBinWidth - contextMod;
                                modNameAppend << "{" << contextMod << "'b" << s << ", ";
                                // Get the truncated string and append
                                const char* vs = paramStr.c_str();
                                vs += paramStrSize - numCharsFit;
                                modNameAppend << "\"" << vs << "\"}";
                            } // numChars > 0
                            Strings::free(fpv);
                        } // else context size is 8 bit aligned
                    } else { // else if string > context size
                        // The size fits perfectly
                        modNameAppend << "\"" << paramStr << "\"";
                        addStringComment = false;
                    }
                } // if range
            } else { // if param is a string
                // NOT A STRING
                bool appendIt = true;
                if (! actualParam.hasValidDblVal())
                    // If this is a real value, then don't print out a based
                    // number in libdesign.parameters
                    modNameAppend << actualParam.getSize() << "'";

                if (overrideToBinary ||
                        ((actualParam.hasValidParamVal()) && ! actualParam.hasValidDblVal() &&
                         (actualParam.getStr().size() >= 4))) {
                    UtString& paramBuf = actualParam.getStr();
                    // hexify the parameter value
                    if (UtConv::BinaryStrToHex(&paramBuf))
                        modNameAppend += 'h';
                    else
                        modNameAppend += 'b';
                } else {
                    if (actualParam.hasValidParamVal()) {
                        modNameAppend += 'd';
                        // getStr() in actualParam is the binary form.  
                        // getParamVal() is the decimal form.
                        modNameAppend << actualParam.getParamVal();
                        appendIt = false;
                    } else if (actualParam.hasValidDblVal()) {
                        modNameAppend << actualParam.getDblValue();
                        appendIt = false;
                    } else {
                        modNameAppend += 'b';
                    }
                }

                if (appendIt) modNameAppend += actualParam.getStr();
            }
            bool isStringVal = (actualParam.getType() == ASCIISTR);
            // modNameAppend contains the interpreted value of the parameter
            // in string form. 
            paramElement->putStr(modNameAppend);

            elemValues << indent << paramElement->getName();
            if (range) elemValues << "[" << formalRange.getMsb() << ":" << formalRange.getLsb() << "]";
            elemValues << " = " << modNameAppend << ";";
            if (addStringComment && isStringVal) elemValues << " // \"" << actualParam.getStr() << "\"";
            elemValues << "\n";

            delete formalParmVal;
        } // while param && formal

        // This parameter list is definitely initialized at this
        // point. Future encounters of this parameterized module will be
        // more efficient.
        paramList->putInit(true);

        UtString modSuffix;
        //TODO probably need to split suffix (which already exist in verific flow, and just check if name already appeared once
        //At least need a check that Verific suffix matches with unique suffix here
        //Note name comparison can be also done in the top-level where we have an access to both strings, currently not done because algorithm don't covers
        // some cases, see comment in VeriModule visitor
        if (paramList->constructSuffix(&modSuffix))
        {
            // We created a new module name. Add the mapping to
            // libdesign.parameters.
            UtOBStream& outFile = mParamModuleMap->getParameterMapFile();
            outFile << *modOrigName << modSuffix << " -> " << *modOrigName;
            UtString tmpLoc;
            loc.compose(&tmpLoc);
            outFile << "  (" << tmpLoc << ")" << UtIO::endl;
            outFile << elemValues << UtIO::endl;
        }

        modOrigName->append(modSuffix);
        // modOrigName now contains the nucleus module name.
    } // if formalIter

} // analyzeInstanceParameters

void VerificVerilogDesignWalker::createUniquifiedModuleName(VeriModule* ve_module)
{
    UtString unusedOrigModuleName;
    UtString moduleName; 
    gVerilogModuleNames(ve_module, true, &unusedOrigModuleName, &moduleName);
    mNucleusBuilder->createUniquifiedModuleName(ve_module, moduleName);
}

// TODO move this function to Verific2NucleusUtilities
void VerificVerilogDesignWalker::gVerilogModuleNames(VeriModule* ve_module, bool escaped, UtString* origModuleName, UtString* moduleName)
{
    // if module is not a parametric module 2 names are equal, otherwise we have origname name (HDL specified name) and uniquified name (after static elaboration)
    //if (escaped) {
    //    //*moduleName = createNucleusString(ve_module->GetId()->GetName());
    //} else {
    //    *moduleName = ve_module->GetId()->GetName();
    //}
    UtString uniquifiedModuleName = mNucleusBuilder->gModuleName(ve_module);
    if (uniquifiedModuleName.empty()) {
        *moduleName = ve_module->GetId()->GetName();
    } else {
        *moduleName = uniquifiedModuleName;
    }
    if (escaped) {
        *moduleName = createNucleusString(*moduleName);
    }
    // return visible names if from protected regions
    // Below condition is a workaround to Verific api problem. By some reason in different situations either module iddef, or module node becomes protected (in fact in both cases I have monitored none of them should). This will probably require bug report to Verific and future update of the code here. Similar code can be found in other places (VerificDumpHierarchy, VerificVerilogSTBuildWalker)
    if (ve_module->IsTickProtectedNode() && ve_module->GetId()->IsTickProtectedNode()) {
        *moduleName = gVisibleName(ve_module, *moduleName); 
    }
    *origModuleName = *moduleName;
    if ( ve_module->IsCopiedModule() ){
        // This module wasn't originally instantiated (before static elaboration occured).
        // if module name has the parameter string appended to it (e.g. "foo(WIDTH=8)" ) then we need to also get the original name
        if (escaped) {
            *origModuleName = createNucleusString(ve_module->GetOriginalModuleName());
        } else {
            *origModuleName = ve_module->GetOriginalModuleName();
        }
        //*origModuleName = gVisibleName(ve_module->GetOriginalModule(), *origModuleName);
    }
}


void VerificVerilogDesignWalker::addCaseCondition(VeriCaseItem* ve_item, unsigned c, unsigned maxExprSize, NUExpr* condExpr, UtString* condValue,
    NUCaseItem* caseItem, const SourceLocator& sourceLocation)
{
    // Create Mask expression if needed
    NUConst* maskExpr = 0;
    if ((V2NDesignScope::CASE != c) && (V2NDesignScope::CASE_INSIDE != c)) {
        bool isCaseZ = (V2NDesignScope::CASE_Z == c);
        maskExpr = mNucleusBuilder->cCaseMask(condExpr, *condValue, isCaseZ, sourceLocation);
        if (condValue) delete condValue;
    }
    // Create case condition and add to caseItem
    NUCaseCondition* caseCondition = mNucleusBuilder->cCaseCondition(condExpr, maskExpr, maxExprSize, sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(caseCondition, *ve_item, "Unable to create case condition");
    mNucleusBuilder->aConditionToCaseItem(caseCondition, caseItem);
}

void VerificVerilogDesignWalker::genCaseConditions(VeriCaseItem* ve_item, unsigned c, unsigned maxExprSize, const UtPair<int,int>& dim, NUCaseItem* caseItem,
    const SourceLocator& sourceLocation)
{
    for (int i = dim.first; i < dim.second + 1; ++i) {
        UtString condValue;
        condValue << i;
        NUExpr* condExpr = mNucleusBuilder->cConst(i, 32, true, sourceLocation);
        addCaseCondition(ve_item, c, maxExprSize, condExpr, &condValue, caseItem, sourceLocation);
    }
}

bool VerificVerilogDesignWalker::IsUnsupportedTypeRef(VeriDataType* dataType)
{
    //VeriTypeRef* typeRef = dynamic_cast<VeriTypeRef*>(dataType);
    if (!dataType) return false; // we support all base types 
    VeriDataType* baseType = dataType;
    // typeRef like idRef need to use it's reloved base type to decide if we support it
    if (baseType->IsTypeRef() && baseType->GetBaseDataType()) baseType = dataType->GetBaseDataType(); // going through typedefs - to find core type
    if (baseType->IsEnumType()) {
        // TODO support enumerations
        return true;
    } else if (baseType->IsUnionType() || baseType->IsStructType() || baseType->IsTypeOperator()) {
        // Seems we will not support this ever
        return true;
    }
    return false; //otherwise it's some builtin type, where it seems we support everything
}

MsgContext* VerificVerilogDesignWalker::getMessageContext()
{
    return mNucleusBuilder->getMessageContext();
}

VerificTicProtectedNameManager* VerificVerilogDesignWalker::gTicProtectedNameMgr()
{
    return mNucleusBuilder->gTicProtectedNameMgr();
}

UtString VerificVerilogDesignWalker::gVisibleName(VeriTreeNode* ve_node, const UtString& origName)
{
    return gTicProtectedNameMgr()->getVisibleName(ve_node, origName);
}

// Return true if 'id' is declared in global/compilation scope.
bool VerificVerilogDesignWalker::isInGlobalScope(VeriIdDef* id)
{
  // Get the owning scope of this id.
  VeriScope *owningScope = id ? id->GetOwningScope() : 0;
  // Get the containing module.
  VeriIdDef *m = owningScope ? owningScope->GetContainingModule() : 0;
  // If it's the root module, then the 'id' is declared in 
  // global/compilation scope.
  return m ? m->IsRootModule() : false;
}

// Perform some range checks that compare \a declaredRange and \a parsedRange. 
// Look for a reversed range (and report an error, and return false). 
// If no problem then return true.
bool VerificVerilogDesignWalker::checkParsedRangeVsDeclaredRange(ConstantRange* declaredRange, ConstantRange* parsedRange, const UtString& netName, const SourceLocator* const loc)
{
    if ( declaredRange->isFlipped(*parsedRange) ) {
        getMessageContext()->FlippedPartRange(loc, parsedRange->getMsb(), parsedRange->getLsb(),
                netName.c_str(), declaredRange->getMsb(), declaredRange->getLsb()); // this is an error
        setError(true);
        return false;
    }
    return true;
}

// Look for ranges that only partially overlap,
//   if there is only a partial overalp then return a new range that includes only the overlapped region,
//   otherwise return the orignal \a parsedRange
ConstantRange* VerificVerilogDesignWalker::pruneRangeToOverlappingRange(ConstantRange* declaredRange, ConstantRange* parsedRange, const UtString& netName, const SourceLocator* const loc)
{
    if ( !declaredRange->contains(*parsedRange) ) {
        getMessageContext()->LFPartRangeWarning(loc, parsedRange->getMsb(), parsedRange->getLsb(),
                netName.c_str(), declaredRange->getMsb(), declaredRange->getLsb());
        if (declaredRange->overlaps(*parsedRange)) *parsedRange = declaredRange->overlap(*parsedRange);
    }
    return parsedRange;
}

} // namespace verific2nucleus


