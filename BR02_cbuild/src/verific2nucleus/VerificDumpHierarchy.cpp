// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// Project headers
#include "verific2nucleus/VerificDumpHierarchy.h"
#include "verific2nucleus/VerificDesignWalker.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"
#include "verific2nucleus/VerificTicProtectedNameManager.h"

// Other project headers
#include "iodb/IODBNucleus.h"
#include "util/UtMap.h"

// Verific headers
#include "VeriModule.h"
#include "VeriId.h"
#include "VeriLibrary.h"
#include "veri_file.h"
#include "VeriTreeNode.h"
#include "VeriExpression.h"
#include "veri_yacc.h"
#include "VeriBaseValue_Stat.h"
#include "VeriScope.h"
#include "FileSystem.h"
#include "Map.h"

// Standard headers
#include <sstream>

#define XML_ENCODING "ISO-8859-1"

namespace verific2nucleus {

///////////////////////////////////////////////////////////// constructor/destructor, public api

VerilogDumpHierarchyCB::VerilogDumpHierarchyCB(UtString fileName, IODBNucleus* iodb, VerificTicProtectedNameManager* nameManager)
: mDesignHierarchy(new DesignHierarchy(fileName))
, mIODB(iodb)
, mTicProtectedNameMgr(nameManager)
{

}


VerilogDumpHierarchyCB::~VerilogDumpHierarchyCB() 
{
    delete mDesignHierarchy;
}


void 
VerilogDumpHierarchyCB::dumpXml() 
{
    mDesignHierarchy->dumpXml();
}

///////////////////////////////////////////////////////////// utility functions

UtString VerilogDumpHierarchyCB::gVisibleName(Verific::VeriTreeNode* ve_node, const UtString& origName)
{
    return mTicProtectedNameMgr->getVisibleName(ve_node, origName);
}


Verific::VeriExpression* fActualRefExpr(Verific::VeriExpression* vpc)
{
    Verific::VeriExpression* portRef = vpc;
    if (portRef->GetConnection()) {
        //  if this is an port connection, then take actual expression
        portRef = portRef->GetConnection();
    }
    if (portRef->GetPrefix()) {
        // if this is an indexed expression, then take prefix
        portRef = portRef->GetPrefix();
    }
    return portRef;
}

Verific::VeriIdDef* fActualRefId(Verific::VeriExpression* vpc)
{
    Verific::VeriExpression* portRef = fActualRefExpr(vpc);
    return portRef->GetId();
}


UtString decompileToString(Verific::VeriTreeNode* node)
{
    std::ostringstream s;
    node->PrettyPrint(s, 0);
    UtString str(s.str().c_str());
    //TODO we probably need to apply visible name to this function always (even for values)
    return str;
}

UtString getLanguageType()
{
    UtString language("verilog");
    return language;
}

UtString getFileType(Verific::VeriTreeNode* node)
{
    File::FileType fileType = File::eUnknown;
    if (node == NULL) {
        return File::getFileType(fileType);
    }
    fileType = File::eModule;
    return File::getFileType(fileType);
}

UtString setVhdlCase(const char* name) 
{
    // function doesn't have sense for verilog, but keeping just for compatibility in order later easily support VHDL
    UtString str;
    if (name == NULL) {
        return str;
    }
    str = name;
    return str;
}

//// Returns Module/Entity Name for the passed Node
void VerilogDumpHierarchyCB::getHdlEntityName(Verific::VeriModule* module, UtString* nameBuf) 
{
    if (module->IsCopiedModule()) {
        //*nameBuf = gVisibleName(module->GetOriginalModule(), module->GetOriginalModuleName());
        *nameBuf = module->GetOriginalModuleName();
    } else {

        if (module->IsTickProtectedNode() && module->GetId()->IsTickProtectedNode()) {
            *nameBuf = gVisibleName(module, module->GetId()->GetName());
        } else {
            *nameBuf = module->GetId()->GetName();
        }
    }
}

// Returns the library name associated with design unit or module
UtString getLibraryName(Verific::VeriModule* node) 
{
    if (node == NULL) {
        return "";
    }
    if (! node->GetLibrary()) {
        return "";
    }
    return node->GetLibrary()->GetName();
}

// Returns the library path associated with design unit or module
UtString getLibraryPath(Verific::VeriModule* node) 
{
    if (node == NULL) {
        return "";
    }
    UtString libName = getLibraryName(node);
    if (libName == "") {
        return "";
    }
    return Verific::veri_file::GetLibraryPath(libName.c_str());
}

// Returns Absolute Source File Name for the passed Node
UtString getFileName(Verific::VeriTreeNode* node)
{
    if (node == NULL) {
        return "";
    }
    UtString fileName(node->Linefile()->GetAbsFileName());
    return fileName;
}

// Returns Start Line Number in the Source File for the passed Node
SInt32 getStartLineNo(Verific::VeriTreeNode* node)
{
    SInt32 lineNo = -1;
    if (node == NULL) {
        return lineNo;
    }
    lineNo = node->Linefile()->GetLeftLine();
    return lineNo;
}

// Returns End Line Number in the Source File for the passed Node
SInt32 getEndLineNo(Verific::VeriTreeNode* node) 
{
    SInt32 lineNo = -1;
    if (node == NULL) {
        return lineNo;
    }
    lineNo = node->Linefile()->GetRightLine();
    return lineNo;
}

// Returns the Type for the passed VeriTreeNode 
UtString getVeType(Verific::VeriTreeNode* node)
{
    if (node == NULL) {
        return "UNSET";
    }
    Verific::VeriIdDef* paramId = dynamic_cast<Verific::VeriIdDef*>(node);
    V2N_INFO_ASSERT(paramId, *node, "Incorrect id object");
    Verific::VeriExpression* curExpr = paramId->GetInitialValue();
    Verific::VeriBaseValue* parameterVal = curExpr->StaticEvaluate(0, 0, 0, 1);
    Verific::veri_value_type parameterValType = parameterVal->Type();
    UtString typeName;
    switch (parameterValType) {
        case Verific::INTEGER:
            typeName = "INTEGER";
            break;
        case Verific::ASCIISTR:
            typeName = "STRING";
            break;
        case Verific::BASEDNUM:
            typeName = "BASED";
            break;
        case Verific::REAL:
            typeName = "REAL";
            break;
        default:
            typeName = "EXPRESSION";
            break;
    }
    delete parameterVal;
    return typeName;
}

UtString getVeValue(Verific::VeriTreeNode* node)
{
    if (node == NULL) {
        return "";
    }
    UtString value = decompileToString(node);
    if (value.empty()) {
        value = "Unknown Value";
    }
    return value;
}

// Returns the Name for the passed Node Name based on its Object Type
UtString VerilogDumpHierarchyCB::getVerificName(Verific::VeriTreeNode* node) 
{
    if (node == NULL) {
        return "";
    }
    UtString name = gVisibleName(node, decompileToString(node));
    if (name.empty()) name = "Unknown Name";
    return name;
}

// Returns the Value for the passed Node based on its Object Type
UtString getVerificValue(Verific::VeriTreeNode* node) 
{
    if (node == NULL) {
        return "";
    }
    UtString value = decompileToString(node);
    if (value.empty()) value = "Unknown Value";
    return value;
}

//
//// Returns the Actual Port Expression for the passed Node based on its Object Type
UtString getVerificPortActual(Verific::VeriExpression* node) 
{
    if (node == NULL) {
        return "";
    }
    UtString actual = decompileToString(node);
    // The part is inherited from Interra, probably just some stripping to have desirable view, I'm not sure how much it's actual for Verific, but any generic format adjustments can be done here (like spaces in the expression etc.)
    if (! actual.empty()) {
        UtString::size_type pos;
        if((pos = actual.find("\n")) != UtString::npos)
            actual.erase(pos);
        if((pos = actual.find("/*")) != UtString::npos)
            actual.erase(pos);
        if((pos = actual.find_first_of(" ")) != UtString::npos)
            actual.erase(pos, 1);
    } else {
        actual = "Unknown Value";
    }
    return actual;
}

///////////////////////////////////////////////////////////// visitors
Verific::VerilogDesignWalkerCB::Status
VerilogDumpHierarchyCB::VERI_WALK(VeriModule, node, phase)
{
    // as we don't have special handler for library will have it populated hear, 
    // so Interra walker design + hdlEntity code will be here
    if (Verific::VerilogDesignWalkerCB::VISITPRE == phase) {

        UtString moduleName; 
        getHdlEntityName(&node, &moduleName);

        UtString libName = getLibraryName(&node); 
        // Skip nested module case which is unsupported currently (precise error will be reported by nucleus population)
        if (libName.empty()) { // nested module case
            return SKIP;
        }
        UtString libPath = getLibraryPath(&node);

        UtString language = getLanguageType();

        Instance* topLevel = new Instance(moduleName, libName, moduleName, language);

        mDesignHierarchy->addTopLevelDesignUnit(topLevel);
        mDesignHierarchy->pushInstance(topLevel);

        topLevel->isTopLevel();

        UtString fileName = getFileName(&node);

        if (!mDesignHierarchy->fileExists(fileName)) {
            addFile(fileName, getFileType(&node), language);
        }

        topLevel->setStart(fileName, getStartLineNo(&node));

        if (!mDesignHierarchy->libraryExists(libName)) {
            Library* library = new Library(libName, libPath, language);
            library->setAsDefaultLibrary();
            mDesignHierarchy->addLibrary(libName, library);
        }
        Verific::VerilogDesignWalkerCB::Status status1 = hdlEntity(phase, &node, moduleName.c_str());
        VERI_CB_STATUS_NESTED_CHK(node, status1);
        Verific::VerilogDesignWalkerCB::Status status2 = addPorts(&node);
        VERI_CB_STATUS_NESTED_CHK(node, status2);
    } else { 
        V2N_INFO_ASSERT(Verific::VerilogDesignWalkerCB::VISITPOST == phase, node, "Incorrect phase");
        mDesignHierarchy->popInstance();
    }
    return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status VerilogDumpHierarchyCB::hdlEntity(VisitPhase phase, Verific::VeriModule* node, const char* entityName) 
{
    if (Verific::VerilogDesignWalkerCB::VISITPRE == phase) {
        UtString libraryName = getLibraryName(node);
        if (libraryName.empty()) return SKIP;
        UtString entityStr   = setVhdlCase(entityName);
        UtString searchName  = libraryName + "." + entityStr;

        if (mDesignHierarchy->interfaceExists(searchName)) {
            mDesignHierarchy->setCurrentInterface(NULL);
        } else {
            UtString   libraryPath = getLibraryPath(node);
            UtString   language    = getLanguageType();

            Interface* interface   = new Interface(entityStr, libraryName, language);

            UtString   fileName    = getFileName(node);
            UInt32     start       = getStartLineNo(node);
            UInt32     end         = getEndLineNo(node);

            interface->setStart(fileName, start);
            interface->setEnd(fileName, end);

            if (!mDesignHierarchy->fileExists(fileName)) {
                addFile(fileName, getFileType(node), language);
            }

            mDesignHierarchy->addInterface(searchName, interface);
            mDesignHierarchy->setCurrentInterface(interface);

            if (!mDesignHierarchy->libraryExists(libraryName)) {
                Library* library = new Library(libraryName, libraryPath, language);
                mDesignHierarchy->addLibrary(libraryName, library);
            }

            if (! node->IsPrimitive()) {
                // Getting Parameter List from the Instance
                Verific::Array* parameterList(node->GetParameters());

                Verific::VeriIdDef* vlogNode = NULL;
                unsigned index = 0;
                FOREACH_ARRAY_ITEM(parameterList, index, vlogNode) {
                    // for now skip type parameters
                    if (vlogNode->IsType()) continue;
                    UtString masterName;
                    // Find Instance Master
                    // I have been thought of hdlEntity as module analog,
                    // so I don't understand why we have to deal with instance Master here, it seems for instances there is 
                    // separate component handler
                    Verific::VeriModule* master = node;
                    //veNode   master = veNodeGetMasterScope(vlogNode);
                    // Getting the Parameter Name from the Parameter List (Instance)
                    UtString parameterName(gVisibleName(vlogNode, vlogNode->GetName()));
                    // Getting the Parameter Value from the Parameter List (Instance)
                    if (!parameterName.empty()) {
                        UtString parameterValue(getVeValue(vlogNode->GetInitialValue()));
                        // Setting Parameter Type
                        UtString parameterType(getVeType(vlogNode));
                        // Getting Module Name form the Instance Master
                        getHdlEntityName(master, &masterName);
                        // Create and Add the Parameter to the Instance Interface
                        Parameter* parameter = new Parameter(parameterName, parameterType, parameterValue);

                        parameter->setStart(fileName, getStartLineNo(vlogNode));

                        interface->addParameter(parameter);
                    }
                }
            }
        }
    } else {
        V2N_INFO_ASSERT(Verific::VerilogDesignWalkerCB::VISITPOST == phase, *node, "Incorrect phase");
        mDesignHierarchy->setCurrentInterface(NULL);
    }
    return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status VerilogDumpHierarchyCB::VERI_WALK(VeriModuleInstantiation, node, phase)
{
    Verific::VeriModule* instMaster = node.GetInstantiatedModule();
    if (!instMaster) return SKIP; // for now skip vhdl modules, mixed language case
    Verific::Array* parameterList = instMaster->GetParameters();
    UtString entityName;
    getHdlEntityName(instMaster, &entityName);
    Verific::VerilogDesignWalkerCB::Status status1 = hdlEntity(phase, instMaster, entityName.c_str());
    VERI_CB_STATUS_NESTED_CHK(*instMaster, status1); 
    Verific::VerilogDesignWalkerCB::Status status2 = addPorts(instMaster);
    VERI_CB_STATUS_NESTED_CHK(*instMaster, status2); 
    Verific::Array* instanceArray = node.GetInstances();
    Verific::VeriInstId* inst = NULL;
    unsigned i = 0;
    FOREACH_ARRAY_ITEM(instanceArray, i, inst) {
        UtString instName(gVisibleName(inst, inst->GetName()));
        UtString instNameStr = setVhdlCase(instName.c_str());
        UtString entityNameStr = setVhdlCase(entityName.c_str());
        UtString libraryNameStr = getLibraryName(instMaster);
        if (libraryNameStr.empty()) return SKIP;
        UtString language = getLanguageType();
        if (Verific::VerilogDesignWalkerCB::VISITPRE == phase) {
            Instance* instance = new Instance(instNameStr, libraryNameStr, entityNameStr, language);

            if (mDesignHierarchy->hasSubstituteInterface()) {
                instance->setOriginalInterface(mDesignHierarchy->getSubstituteInterfaceName());
            }

            instance->setStart(getFileName(inst), getStartLineNo(inst));

            Instance* currentInstance = mDesignHierarchy->getCurrentInstance();
            currentInstance->addInstance(instance);
            mDesignHierarchy->pushInstance(instance);


            // Getting Actual Parameter List from the Instance
            //We don't have this info in Verific so had to use resolved parameter list
            // TODO consider this when adding support for mixed languages
            //if(instLang == MVV_VHDL) {
            //}
            // For Instance Masters in Verilog
            // Process instance parameters if there are any
            if (! instMaster->IsPrimitive()) {
                // Validate that we have parameters to process
                //CheetahList formalParamList(veModuleGetParamList(static_cast<veNode>(instMaster)));
                if (parameterList && (parameterList->Size() > 0)) {
                    // Walk the formals and get the values from there. The
                    // callback system elaborate function resolves parameters
                    // for us.
                    Verific::VeriIdDef*      formalParam = NULL;
                    unsigned j = 0;
                    FOREACH_ARRAY_ITEM (parameterList, j, formalParam) {
                        // not sure how to represent SV type parameters (especially it's type) so skipping for now
                        // TODO may need to support this
                        if (formalParam->IsType()) continue;

                        // Get the parameter name and value
                        UtString parameterName;
                        UtString parameterValue;
                        parameterName = gVisibleName(formalParam, formalParam->GetName()); 
                        parameterValue = getVerificValue(formalParam->GetInitialValue());

                        // Setting Parameter Type 
                        UtString parameterType(getVeType(formalParam));

                        if ( !parameterName.empty()) {
                            Parameter* parameter = new Parameter(parameterName, parameterType, parameterValue);
                            instance->addParameter(parameter);
                        }
                    }
                } // if

                // Getting Actual Port List from the Instance
                Verific::Array* allPortConnections = inst->GetPortConnects();

                // For Instance Masters in Verilog
                Verific::VeriExpression* actualPort = NULL;

                if (allPortConnections && (allPortConnections->Size() > 0)) {

                    UtSet<Verific::VeriIdDef*> alreadyAddedPorts;
                    bool isAnsiPortDecl = false;
                    // Examine first port connection of module to see if it's ansi style, remaining ports should follow same style
                    Verific::Array* formalPortConnectionArray = instMaster->GetPortConnects();
                    if (formalPortConnectionArray && formalPortConnectionArray->Size() > 0) {
                        Verific::VeriExpression* firstExpr = static_cast<Verific::VeriExpression*>(formalPortConnectionArray->GetFirst()); // cast from void*
                        isAnsiPortDecl = firstExpr->IsAnsiPortDecl();
                    }
                    // For each Actual/Formal Port Pair
                    unsigned j = 0;
                    FOREACH_ARRAY_ITEM(allPortConnections, j, actualPort) {
                        // skip open ports
                        if (!actualPort || actualPort->IsOpen()) continue;
                        UtString formalPortName;
                        // Getting Port Name from the Formal Port List (Instance Master)
                        if (actualPort->GetClassId() == Verific::ID_VERIPORTCONNECT) {
                            // named connection
                            formalPortName = gVisibleName(dynamic_cast<Verific::VeriPortConnect*>(actualPort),
                                (dynamic_cast<Verific::VeriPortConnect*>(actualPort))->GetNamedFormal());
                        } else {
                            // by order connection
                            Verific::VeriIdDef* formalPort = NULL;
                            if (isAnsiPortDecl) {
                                // ansi port decl case
                                // use port array directly to get formal
                                Verific::Array* formalPortArray = instMaster->GetPorts();
                                V2N_INFO_ASSERT(j < formalPortArray->Size(), *instMaster, "unsupported port type");
                                formalPort = static_cast<Verific::VeriIdDef*>(formalPortArray->At(j));
                            } else {
                                Verific::VeriExpression* pc = static_cast<Verific::VeriExpression*>(formalPortConnectionArray->At(j));
                                Verific::VeriExpression* portExpr = fActualRefExpr(pc);
                                // skip open ports and non-idrefs
                                if (!portExpr || portExpr->IsOpen() || !portExpr->IsIdRef()) {
                                    continue; // skip
                                }
                                formalPort = fActualRefId(pc);
                                if (alreadyAddedPorts.find(formalPort) == alreadyAddedPorts.end()) {
                                    alreadyAddedPorts.insert(formalPort);
                                } else {
                                    continue; // skip we have already added this port
                                }
                            }
                            V2N_INFO_ASSERT(formalPort, *instMaster, "unsupported port type");
                            formalPortName = gVisibleName(formalPort, getVerificName(formalPort));
                        }
                        // Getting Port Value from the Actual Port List (Instance)
                        Verific::VeriExpression* portConn = actualPort->GetConnection();
                        UtString portValue(getVerificPortActual(portConn));

                        UtString substituteFormalPortName = mDesignHierarchy->getSubstitutePortMap(formalPortName);
                        if (!substituteFormalPortName.empty()) {
                            formalPortName = substituteFormalPortName;
                        }

                        Port* port = new Port(portValue, formalPortName);
                        instance->addPort(port);
                    }
                }
            }
        } else {
            mDesignHierarchy->popInstance();
        }
    }
    return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status  VerilogDumpHierarchyCB::addPorts(Verific::VeriModule* master)
{
    Verific::VeriExpression *pc = NULL ;
    unsigned i;
    UtSet<Verific::VeriIdDef*> alreadyAddedPorts;
    FOREACH_ARRAY_ITEM(master->GetPortConnects(), i, pc) {
        if (!pc) continue; // skip
        if (pc->IsAnsiPortDecl()) {
            Verific::VeriAnsiPortDecl* ansiDecl = dynamic_cast<Verific::VeriAnsiPortDecl*>(pc);
            Verific::VeriIdDef* id = NULL;
            unsigned j;
            FOREACH_ARRAY_ITEM(ansiDecl->GetIds(), j, id) {
                Verific::VerilogDesignWalkerCB::Status status = addSinglePort(id, master, pc);
                VERI_CB_STATUS_NESTED_CHK(*pc, status);
            }
        } else {
            Verific::VeriExpression* portExpr = fActualRefExpr(pc);
            // only idref and open port supported hear no complex expressions
            if (!portExpr || portExpr->IsOpen() || !portExpr->IsIdRef()) {
                continue; // skip
            }
            Verific::VeriIdDef* portId = fActualRefId(pc);
            if (alreadyAddedPorts.find(portId) == alreadyAddedPorts.end()) {
                alreadyAddedPorts.insert(portId);
            } else {
                continue; // skip
            }
            Verific::VerilogDesignWalkerCB::Status status = addSinglePort(portId, master, pc);
            VERI_CB_STATUS_NESTED_CHK(*pc, status);
        }
    }
    return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status VerilogDumpHierarchyCB::addSinglePort(Verific::VeriIdDef* id, Verific::VeriModule* master, Verific::VeriExpression* pc)
{
    Interface* interface = mDesignHierarchy->getCurrentInterface();
    if (interface == NULL) {
        return NORMAL;
    }
    // SV interfaces aren't supported
    if (id->IsInterfacePort() || id->IsGenericInterfacePort()) {
        return NORMAL;
    }
    UtString masterName;
    getHdlEntityName(master, &masterName);
    UtString portName(gVisibleName(id, id->GetName()));
    if (!portName.empty()) {
        UtString   portDirection;
        switch(id->Dir()) {
            case VERI_INPUT:
                portDirection = "IN";
                break;
            case VERI_OUTPUT:
                portDirection = "OUT";
                break;
            case VERI_INOUT:
                portDirection = "BIDI";
                break;
            default:
                INFO_ASSERT(false, "Unset port direction");
                break;
        }
        // Get the library associated with the master
        UtString libraryName = getLibraryName(master);
        if (libraryName.empty()) return SKIP;
        // For Verilog, the port type doesn't make any sense.
        UtString portType;
        // Getting port Size from Instance
        UtString portSize;
        //portSize << vePortGetPortWidth(vlogNode);
        unsigned size = GET_CONTEXT_SIZE(id->StaticSizeSign(0));
        portSize << size; 
        // Create and Add Port to the Instance Interface
        Port* port = new Port(portName,
                portType,
                portDirection,
                portSize);
        // We need port declaration (in module declaration line) location to match up with Interra
        port->setStart(getFileName(id), getStartLineNo(pc));

        interface->addPort(port);
    }
    return NORMAL;
}

//! Generate block callback places Verilog generate labels in design hierarchy file.
// These allow modelstudio to browse instances, regs, wires underneath generates.
Verific::VerilogDesignWalkerCB::Status VerilogDumpHierarchyCB::VERI_WALK(VeriGenerateBlock, node, phase)
{
    //const char* blockName = gVisibleName(&node, node.GetBlockId()->GetName()).c_str();
    //Somehow above line doesn't produce correct result (bug in UtString? gcc problem? C++ syntacical rule?). Anyway below version with temp 'vName' variable works properly, so I have updated the code everywhere to have temp variable in case we need 'c_str' from it
    UtString vName = gVisibleName(&node, node.GetBlockId()->GetName());
    const char* blockName = vName.c_str();

   // If this is not a nameable generate block, there's no work to be done.
   if (blockName == NULL || strlen(blockName) == 0)
     return NORMAL; //skip 
   
   if (Verific::VerilogDesignWalkerCB::VISITPRE == phase) {
       // UtIO::cout() << "DEBUG: Verilog generate block PRE '" << blockName << "'" << UtIO::endl; 
       UtString instNameStr = setVhdlCase(blockName); 
       UtString language = getLanguageType();
       Instance* instance = new Instance(instNameStr, "", "", language);
       instance->setStart(getFileName(&node), getStartLineNo(&node));
       // Tell the modelstudio hierarchy browser that this is a generate label
       // (and not a module/entity instance, or a process label)
       instance->setInterfaceType("vlog_generate");
       Instance* currentInstance = mDesignHierarchy->getCurrentInstance();
       currentInstance->addInstance(instance);
       mDesignHierarchy->pushInstance(instance);
   } else {
       // UtIO::cout() << "DEBUG: Verilog generate block POST '" << blockName << "'" << UtIO::endl;
       mDesignHierarchy->popInstance();
   }
   return NORMAL;
}


void VerilogDumpHierarchyCB::addFile(UtString fileName, UtString fileType, UtString language)
{
    File* file = new File(fileName, fileType, language);

    mDesignHierarchy->addFile(fileName, file);

    if (strcasecmp(language.c_str(), "verilog") == 0) {
        //veNode vlogNode = node->castVeNode();
        //Verific::VeriModule* module = dynamic_cast<Verific::VeriModule*>(node); // can be as well udp
        //if (module->IsModule() && module->GetLibrary()) {
            //file->setFileType(File::getFileType(File::eVlogLibrary));
        //}

        // if (veNodeGetObjType(vlogNode) == VE_MODULE &&
        // veModuleGetIsLibrary(vlogNode) == VE_TRUE) {
        //   file->setFileType(File::getFileType(File::eVlogLibrary));
        // }

        //veNode vlogFile;
        //if (veNodeGetObjType(vlogNode) != VE_FILE) {
        //  vlogFile = veNodeGetFile(vlogNode);
        //}
        //else {
        //  vlogFile = vlogNode;
        //}

        // Check for `includes
        checkForIncludes();

        // Check for `define
        checkForDefines(fileName, file);
    }
}

void VerilogDumpHierarchyCB::checkForIncludes() 
{
    const Verific::Map* included_files = Verific::veri_file::GetIncludedFiles();
    Verific::MapIter mi ;
    char *name ;
    Verific::linefile_type lf;
    UtString fileName;
    FOREACH_MAP_ITEM(included_files, mi, &name, &lf) {
        char* absFileName = Verific::FileSystem::Convert2AbsolutePath(name, 1 /* elimnate /../ from path */);
        if (absFileName != NULL) fileName = absFileName;
        if (!mDesignHierarchy->fileExists(fileName)) {
            addFile(fileName, File::getFileType(File::eIncludedFile), "verilog");
        }
        Verific::Strings::free(absFileName);
    }
}

// TODO support SV defines with arguments list
// TODO check for `undefineall SV predefined macro work
void 
VerilogDumpHierarchyCB::checkForDefines(UtString fileName, File* filePtr)
{
    Verific::MapIter mi;
    char *name;
    Verific::linefile_type macro_linefile ;
    Verific::Map* macro_linefiles = Verific::veri_file::GetCarbonMacroDefLinefiles();
    FOREACH_MAP_ITEM(macro_linefiles, mi, &macro_linefile, &name) {
        UtString abs_file_name = macro_linefile->GetAbsFileName();
        if (fileName == abs_file_name) { // we need to add only defines from current file
            UtString macro_name;
            UtString macro_value;
            const char* value = Verific::veri_file::CarbonMacroValue(macro_linefile);
            if (name) macro_name = name;
            //if (mTicProtectedNameMgr->isTicProtected(macro_linefile)) {
            //    macro_name = gVisibleName(0, macro_name);
            //}
            if (value) macro_value = value;
            UtString start_line;
            start_line << macro_linefile->GetLeftLine();
            Define* define = new Define(macro_name, macro_value, start_line);
            filePtr->addDefine(define);
        }
    }
}

} // namespace verific2nucleus


