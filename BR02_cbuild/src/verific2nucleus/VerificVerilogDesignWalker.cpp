// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


// Project headers
#include "verific2nucleus/VerificVerilogDesignWalker.h"     // Visitor base class definition
#include "verific2nucleus/VerificNucleusBuilder.h" // Nucleus Builder API
#include "verific2nucleus/V2NDesignScope.h" // Scope definition
#include "verific2nucleus/VerificDesignManager.h" // Design Manager definition
#include "verific2nucleus/Verific2NucleusUtilities.h" // Common utilities 
#include "verific2nucleus/VerificDesignWalkerCB.h"
#include "verific2nucleus/VerificTicProtectedNameManager.h"


// Verific headers
#include "Array.h"          // Make dynamic array class Array available
#include "Strings.h"        // A string utility/wrapper class
#include "Message.h"        // Make message handlers available, not used in this example
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriId.h"         // Definitions of all identifier definition tree nodes
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "VeriConstVal.h"   // Definitions of parse-tree nodes representing constant values in Verilog.
#include "veri_tokens.h"
#include "VeriBaseValue_Stat.h"
#include "hdl/HdlVerilogString.h" // for verilog string to binary string converter
#include "hdl/HdlTime.h" // for verilog string to binary string converter

// Standard headers
#include <stdio.h>          // sprintf
#include <string.h>         // strcpy, strchr ...
#include <cmath>            // for pow 
#include <ctype.h>          // isalpha, etc ...
#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <sstream>

// Nucleus headers
#include "nucleus/NUBitNet.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUCopyContext.h"
#include "nucleus/NUBitNetHierRef.h"
#include "nucleus/NUVectorNetHierRef.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUSysFunctionCall.h"
#include "nucleus/NUSysRandom.h"
#include "nucleus/NuToNu.h"

#include "localflow/FindUses.h"
//Set to enable debug
//#define DEBUG_VERIFIC_VERILOG_WALKER 1

namespace verific2nucleus {


using namespace std ;
using namespace Verific ;



class ReplaceTempClocks : public NuToNuFn {
public:
  ReplaceTempClocks(VerificNucleusBuilder* nb) :
    mNucleusBuilder(nb)
  {}

  NUExpr* operator()(NUExpr* expr, Phase phase) {
    if (isPre(phase)) {
      NUNet* temp_clock = mNucleusBuilder->getEdgeNet(expr);
      if (temp_clock != NULL) {
        NUExpr* rval = new NUIdentRvalue(temp_clock, expr->getLoc());
        rval->resize(1);
        delete expr;
        return rval;
      }
    }
    return NULL;
  }

  // I thought about using replaceLeaves on the entire block, but it's
  // too aggressive.  I only want to replace uses in if-conditions
  // when those if-conditions are reached before any other statements
  // are reached, except higher level if-statements
  void walkFirstStmt(NUStmtLoop stmtLoop) {
    if (stmtLoop.atEnd()) {
      return;
    }

    // Only consider the first statement in the stmt loop
    NUIf* if_stmt = dynamic_cast<NUIf*>(*stmtLoop);
    if (if_stmt != NULL) {
      // hack the conditional
      NUExpr* cond = if_stmt->getCond();
      NUExpr* new_cond = cond->translate(*this);
      if (new_cond != NULL) {
        if_stmt->replaceCond(new_cond);
      }

      // recursively traverse the then/else branches
      walkFirstStmt(if_stmt->loopThen());
      walkFirstStmt(if_stmt->loopElse());
    }
  }

private:
  VerificNucleusBuilder* mNucleusBuilder;
};


/*-----------------------------------------------------------------*/
//                      Constructor / Destructor
/*-----------------------------------------------------------------*/

VerificVerilogDesignWalker::VerificVerilogDesignWalker(VerificNucleusBuilder* verificNucleusBuilder, const char* fileRoot, MsgContext* msgContext,
    bool allow_hierrefs, bool inlineTasks, bool doingOutputSysTasks, bool warnForSysTask, int rjcDebugValue)
    : mCurrentScope(0)
    , mScopeStack()
    , mNucleusBuilder(verificNucleusBuilder)
    , mDeclarationRegistry()
    , mTFRegistry()
    , mNameRegistry()
    , mEnableOutputSysTasks(doingOutputSysTasks)
    , mrjcDebug(rjcDebugValue)
    , mInlineTasks(inlineTasks)
    , mEventControlLegal(false)
    , mAllowHierRefs(allow_hierrefs)
    , mWarnForSysTask(warnForSysTask)
    , mHasSideEffectStatement(false)
    , mIsCombAlways(false)
    , mPortMap()
    , mPortDirectionToUseIfUndefined(0)
    , mTimeScaleRegistry()
    , mIgnoreContextSign(false)
    , mParamModuleMap(0)
{
    INFO_ASSERT(mNucleusBuilder, "Null nucleus builder");
    createParamModuleMap(fileRoot, msgContext);
}

VerificVerilogDesignWalker::~VerificVerilogDesignWalker()
{
    // delete all NULvalues from the mPortMap, they were used to track
    // partial nets in the port list but are not part of the design
    for (ScopePortMap::iterator pm = mPortMap.begin(); pm != mPortMap.end(); ++pm){
        PortMap& nodeMap = pm->second;
        for ( PortMap::iterator nlvm = nodeMap.begin(); nlvm != nodeMap.end(); ++nlvm){
            NULvalue* lvalue = nlvm->second;
            if (lvalue) delete lvalue;
        }
    }
    // delete parametric modules map
    destroyParamModuleMap();
}


/*-----------------------------------------------------------------*/
//          Set/Get/Update Metods 
/*-----------------------------------------------------------------*/

    
void VerificVerilogDesignWalker::uDesign()
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    mNucleusBuilder->pDesign();
    std::cerr << "pDesign" << std::endl;
#endif
}

UtMap<VeriTreeNode*, NUNet*>*
VerificVerilogDesignWalker::gDeclarationRegistry()
{
    return &mDeclarationRegistry;
}

UtMap<VeriTreeNode*, NUTask*>*
VerificVerilogDesignWalker::gTFRegistry()
{
    return &mTFRegistry;
}

UtMap<VeriTreeNode*, StringAtom*>*
VerificVerilogDesignWalker::gNameRegistry()
{
    return &mNameRegistry;
}


bool
VerificVerilogDesignWalker::hasError()
{
    return mNucleusBuilder->hasError();
}

void
VerificVerilogDesignWalker::setError(bool error)
{
    // TODO shouldn't we also do mCurrentScope->sValue(NULL);
    // if (mCurrentScope) mCurrentScope->sValue(NULL);
    mNucleusBuilder->setError(error);
}


/*-----------------------------------------------------------------*/
//               Push/Pop/Get methods  current scope 
/*-----------------------------------------------------------------*/

V2NDesignScope* VerificVerilogDesignWalker::gScope() 
{ 
    return mCurrentScope;
}

void VerificVerilogDesignWalker::PushScope(V2NDesignScope* new_scope, const UtString& scopeName)             
{
    (void) scopeName;
    // Pushing current scope to stack
    mScopeStack.push(mCurrentScope);
    mCurrentScope = new_scope;
}

void VerificVerilogDesignWalker::PopScope()
{
    delete mCurrentScope;
    mCurrentScope = mScopeStack.top();
    mScopeStack.pop();
}

// get declaration scope, if no named declaration scope, task, module scope found return NULL
NUScope* VerificVerilogDesignWalker::getDeclarationScope()
{
    return Verific2NucleusUtilities::getDeclarationScope(gScope());
}

// This is deprecated approach for getting scope (function is candidate for
// removal or refactor
NUScope* VerificVerilogDesignWalker::getDeclarationScope2()
{
    NUScope* declaration_scope = NULL;
    NUNamedDeclarationScope* named_declaration_scope = gScope()->gDeclarationScope(); 
    NUScope* block = gScope()->gOwner();
    if (named_declaration_scope) {
        declaration_scope = named_declaration_scope;
    } else if (block && (NUScope::eBlock == block->getScopeType())) {
        declaration_scope = block;
    } else {
        declaration_scope = mNucleusBuilder->gParentDeclUnit(getNUScope(NULL));
    }
    return declaration_scope;
}

NUScope* VerificVerilogDesignWalker::getNUScope(VeriTreeNode* node)
{
    NUScope* scope = gScope()->gOwner();
    if (node) {
        if (!scope) {
            // if not found scope then get module/function scope
            NUScope* declScope = gScope()->gDeclarationScope();
            V2N_INFO_ASSERT(declScope, *node, "Incorrect declaration scope");
            scope = mNucleusBuilder->gParentDeclUnit(declScope);
        }
        V2N_INFO_ASSERT(scope, *node, "Incorrect scope type");
    } else {
        INFO_ASSERT(scope, "Incorrect scope type");
    }
    return scope;
}
    
NUScope* VerificVerilogDesignWalker::getNUScope(V2NDesignScope* currentScope, VeriTreeNode* node)
{
    NUScope* scope = currentScope->gOwner();
    if (node) {
        if (!scope) {
            // if not found scope then get module/function scope
            NUScope* declaration_scope = currentScope->gDeclarationScope();
            VERIFIC_CONSISTENCY(declaration_scope, *node, "Incorrect declaration scope");
            scope = mNucleusBuilder->gParentDeclUnit(declaration_scope);
        }
        VERIFIC_CONSISTENCY(scope, *node, "Incorrect scope type");
    } else {
        INFO_ASSERT(scope, "Incorrect scope type");
    }
    return scope;
}

// This is recursive function to find NUScope (declaration scope) by name
// currently used to find nucleus scope object corresponding to break statement label
// it's supposed to be used in other jump statements with labels like return, continue etc.
NUScope* VerificVerilogDesignWalker::fParentNamedBlock(V2NDesignScope* cScope, const UtString& name)
{
    // if the scope owner is module 
    if (mNucleusBuilder->isModule(getNUScope(cScope, NULL))) {
        return 0;
    } else {
        if (cScope->gDeclarationScope() && (UtString(mNucleusBuilder->gNamedDeclScopeName(cScope->gDeclarationScope())->str()) == name)) {
            // if named declaration scope exist and named matches then return owner block 
            return getNUScope(cScope, NULL);
        } else if (mNucleusBuilder->isTask(getNUScope(cScope->gParentScope(), NULL))
            && (UtString(mNucleusBuilder->gTaskName(getNUScope(cScope->gParentScope(), NULL))->str()) == name)) {
            // if parent is a task and taskname matches with given name return owner block
            return getNUScope(cScope, NULL);
        } else {
            // else search upward
            return fParentNamedBlock(cScope->gParentScope(), name);
        }
    }
}

/*-----------------------------------------------------------------*/
//          NucleusBuilder Methods
/*-----------------------------------------------------------------*/

void VerificVerilogDesignWalker::VERI_VISIT(VeriModule, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "---------------------------------------------------------------" << std::endl;
    std::cerr << "NUCLEUSBUILDER VERIMODULE" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VERI_VISIT_NODE(VeriModuleItem, node) ; // Call of base class

#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cout << "TimeScale : " << node.GetTimeScale() << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // SV unsupported module types
    if (node.IsInterface() || node.IsProgram() || node.IsPackage()) {
        reportUnsupportedLanguageConstruct(sourceLocation, "SystemVerilog module type (Interface/Program/Package) ");
        return;
    }
    // check and report nested modules, we don't really support them currently
    if (!node.GetLibrary()) {
        reportUnsupportedLanguageConstruct(sourceLocation, "Nested module (only modules declared under library (verilog 95/2001) are supported)");
    }
    // Create Nucleus Module
    UtString moduleName;
    UtString origModuleName;
    gVerilogModuleNames(&node, true, &origModuleName, &moduleName);

    // check and report unsupported cModels here, 
    if (mNucleusBuilder->gIODB()->isCModel(origModuleName.c_str())) {
        //report error
        reportUnsupportedLanguageConstruct(sourceLocation, "User defined task/cModel");
        return;
    }

    // There is 2 known cases in which module gets uniquified by Verific
    // 1) Module has specified parameter value during instantiation
    // 2) where there is a hierarchical reference to net/task/function within the module 
    /// Below algorithm which is borrowed from Interra flow doesn't handle 2nd case properly (because Interra flow wasn't so much precise in module uniquification cases as Verific flow does)... 
    // TODO correct the algorithm to handle all cases in which Verific does module uniquification
    // TODO need to clarify because file is called libdesgin.parameters - does that mean that only parametric uniquified modules needs to be placed or every uniquified module ?
    // So for now trying to skip non-parameteric modules (see comment below, for the case which seems we can't easily skip) 
    // As well we don't support type parameters dumping yet (SV only) so skip it
    if (node.IsCopiedModule() && node.GetParameters() && !node.HasTypeParameters()) {
        // TODO consider moving this to Verific2NucleusUtilities and having separate visitor for it
        // We need unescaped versions of names getting them separately
        UtString unescapedOrigModuleName;
        UtString unescapedModuleName;
        gVerilogModuleNames(&node, false, &unescapedOrigModuleName, &unescapedModuleName);
        UtString uniquifiedModuleName(unescapedOrigModuleName);
        // The function is adapted from interra flow
        // The difference with Interra flow is that in Verific flow we already have all module uniquifications applied
        // and module parameter values already contain current value for that particular instantiation,
        // so here we just dump each module parameter values to libdesign.parameters file using adapted Interra flow dumper,
        // although due to some interpretion difference of constants (e.g. sizing), order differences (probably related to fact that interra flow does
        // population/elaboration and libdesgin.parameters dumping in the same phase, also there is a difference in hierarchical reference resolution ordering 
        // between Verific and Interra) and uniquification problems presented above - the output of  Verific and Interra in some cases don't match 
        // All cases of differences I have monitored so far, seem to be in Verific's favor (output of Verific seems more precise), but that should be verified
        // more thoroughly, so putting TODO
        // TODO verify differences on more cases 
        // For additional info see copied Interra flow comments in VerificVerilogDesignWalkerUtilities.cpp 
        analyzeInstanceParameters(&node, &uniquifiedModuleName);
        // Uniquified module name should match with Elaboration flow uniquified module name, otherwise inconsistency
        // Below assertion can't be opened because algorithm has a problems with Verific uniquification (test/langcov/defparam1.v reproduces the problem), 
        // some case which assumed not to be uniquified by algorithm, gets uniquified in Verific flow 
        // (which seems to be correct - Verific is known to be better in hierarchical reference resolution).
        // Due to parameters existence (with default value) seems we can't filter the case like non-parametric cases,
        // as well it seems elaborated flow doesn't contain info whether or not parameter has default value or replaced value of instantiation.
        // Here we have a way to compare with unelaborated version to find out if parameter value has been replaced... 
        // but that seems to be of lesser priority in compare with problems presented above)
        //if (uniquifiedModuleName != unescapedModuleName) getMessageContext()->VerificConsistency(&sourceLocation, "Elaboration flow uniquified module name doesn't match with population flow uniquified module name");
    }

    SInt8 timeUnit = 0;
    SInt8 timePrecision = 0;
    bool timescaleSpecified = false;
    if (node.GetTimeScale()) {
        // current module has timescale specified
        // check if one of previous modules have no timescale specified
        if (!mTimeScaleRegistry.empty() && !allModulesHaveTimeScaleSpecified()) {
            getMessageContext()->ModuleHasTimeScaleDirective(&sourceLocation, moduleName.c_str());
        }
        UtString timeUnitStr;
        UtString timePrecisionStr;
        timescaleSpecified = true;
        UtString timescale(node.GetTimeScale());
        size_t pos = timescale.find('/');
        if (pos == string::npos) {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Incorrect time scale format");
        }
        timeUnitStr = timescale.substr(0, pos);
        timePrecisionStr = timescale.substr(pos+1);

        unsigned st = 0;
        // apply a little arithmetic: 1,3 - report warning, 2,3 - report error
        timeUnit = HdlTime::parseTimeString( timeUnitStr.c_str(), &st );
        if (st % 2) {
            getMessageContext()->TimeStringRounded(&sourceLocation, timeUnitStr.c_str(), HdlTime::VerilogTimeUnits[(unsigned)timeUnit]); // this is a warning
        }
        if (st > 2) {
            getMessageContext()->UnsupportedTimeString(&sourceLocation, timeUnitStr.c_str()); // this is an alert
        }
        timePrecision = HdlTime::parseTimeString( timePrecisionStr.c_str(), &st );
        if (st % 2) {
            getMessageContext()->TimeStringRounded(&sourceLocation,  timePrecisionStr.c_str(), HdlTime::VerilogTimeUnits[(unsigned)timePrecision]); // this is a warning
        }
        if (st > 2) {
            getMessageContext()->UnsupportedTimeString(&sourceLocation, timePrecisionStr.c_str()); // this is an alert
        }
        // TODO above 2 cases are alerts (are candidates to demote to warning by user) and so we need clearly indicate what will happen if user demotes the message to warning (probably ignore setting values as if nothing specified)
        mNucleusBuilder->getDesign()->setGlobalTimePrecision( timePrecision );
        if ( timeUnit < timePrecision ){
            getMessageContext()->InvalidTimeScaleSpecification(&sourceLocation, timeUnitStr.c_str(), timePrecisionStr.c_str() );
        }
    } else {
        // current module has no timescale specified
        // check if one of previous modules have timescale specified
        if (!mTimeScaleRegistry.empty() && !allModulesDoNotHaveTimeScaleSpecified()) {
            getMessageContext()->ModuleDoesNotHaveTimeScaleDirective(&sourceLocation, moduleName.c_str());
        }
    }
    //push whether timescale specified once for each module
    mTimeScaleRegistry.push_back(timescaleSpecified);
    // $time and $display (%t) have dependancy on timeUnit and timePrecision settings
    NUModule* module = mNucleusBuilder->cModule(&node, 0, moduleName, origModuleName, true, sourceLocation, timescaleSpecified, timeUnit, timePrecision);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, node, "Unable to get module");
    //set the current scope
    V2NDesignScope* current_scope = new V2NDesignScope(module, NULL /*no parent scope*/);
    PushScope(current_scope, moduleName);
    
    // process directives if any
    Map* pragmas =  static_cast<VeriIdDef*>(node.GetId())->GetIdPragmas();
    Map* lineinfos =  static_cast<VeriIdDef*>(node.GetId())->GetPragmaLineInfos();
    if (pragmas) {
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
        printf("Got %d pragmas\n",pragmas -> Size());
#endif
        // Get net name, if multiple declarations found directive applied to the last one
        addPragmas(0, &moduleName, pragmas, lineinfos, &sourceLocation);
    }

    // Create Declaration Items
    unsigned i;
    VeriModuleItem *mid ;
    FOREACH_ARRAY_ITEM(node.GetModuleItems(), i, mid) {
        if (mid) { 
            if (mid->IsIODecl()) {
                mid->Accept(*this);
            }
        }
    }
    // Add ports to scope in declaration order (which matters)
    if (node.GetPortConnects()) {
        // default to input - LRM 1800-2005 
        mPortDirectionToUseIfUndefined = VERI_INPUT;
        VeriExpression *pc ;
        UtSet<NUBase*> alreadyAddedPorts;
        FOREACH_ARRAY_ITEM(node.GetPortConnects(), i, pc) {
            if (pc) {
                // if this is a ansi style port declaraton just call it's visitor first
                bool whole_net = false;
                if (pc->IsAnsiPortDecl()) {
                    pc->Accept(*this);
                } else { 
                    ConstantRange port_selected_range(0,0);
                    VeriExpression* portExpr = fActualRefExpr(pc);
                    // only idref and open port supported hear no complex expressions
                    if ((! portExpr->IsIdRef()) && (! portExpr->IsOpen())) {
                        getMessageContext()->UnimplPort(&sourceLocation);
                        return;
                    }
                    VeriIdDef* portId = fActualRefId(pc);
                    NUNet* p = NULL;
                    if (mDeclarationRegistry.find(portId) != mDeclarationRegistry.end()) {
                        p = mDeclarationRegistry[portId];
                        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(p, node, "Unable to get port declaration");
                        switch (pc->GetClassId()) {
                        case ID_VERIIDREF:
                            whole_net = true;
                            break;
                        case ID_VERIPORTCONNECT:
                            if (pc->GetConnection()->IsIdRef()) {
                                whole_net = true;
                            }
                            break; 
                        case ID_VERIINDEXEDID: {
                            bool is_bitselect = ! pc->GetIndexExpr()->IsRange();
                            // get ident, check to see if partselect or bitselect is for full
                            // range, if so then this is ok

                            ConstantRange declared_range(0,0);
                            NUVectorNet* vn = dynamic_cast<NUVectorNet*>(p);
                            if (vn) {
                                declared_range = *vn->getDeclaredRange ();
                            } else {
                                // a bitselect or partselect only makes sense on a vector
                                getMessageContext()->UnimplPort(&sourceLocation);
                                return;
                            }

                            bool acceptable_port_expression = false;
                            if ( is_bitselect ) {
                                int bit = 0;
                                bool ok = evalIntValue(pc->GetIndexExpr(), &bit);
                                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(ok, node, "invalid non-constant index");
                                // a bitselect is acceptable if the selected bit is within the declared range of the net
                                port_selected_range = ConstantRange(bit,bit);
                                acceptable_port_expression =  declared_range.contains(bit);
                            } else {
                                // a partselect is acceptable if it is a static range and within the declared range of the net
                                int leftRange = 0;
                                bool leftOk = evalIntValue(pc->GetIndexExpr()->GetLeft(), &leftRange);
                                int rightRange = 0;
                                bool rightOk = evalIntValue(pc->GetIndexExpr()->GetRight(), &rightRange);
                                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(leftOk && rightOk, node, "invalid non-constant index range");
                                port_selected_range = ConstantRange(leftRange,rightRange);
                                if (! pc->GetIndexExpr()->GetPartSelectToken() and ( declared_range.contains(port_selected_range)) ) {
                                    acceptable_port_expression = true;
                                }
                            }
                            if ( not acceptable_port_expression  ) {
                                getMessageContext()->UnimplPort(&sourceLocation);
                                return;
                            }
                            break;
                            }
                        default: 
                            getMessageContext()->UnimplPort(&sourceLocation);
                            return;
                        }
                        // register port in order not to add the same net twice
                        if (alreadyAddedPorts.find(p) == alreadyAddedPorts.end()) {
                            alreadyAddedPorts.insert(p);
                            // port type can be reg or wire
                            NUScope* scope = getNUScope(&node);
                            mNucleusBuilder->aPortToScope(p, scope);
                        } else if (mNucleusBuilder->IsTopModule(moduleName)) {
                            if ( whole_net ){
                                UtString vName = gVisibleName(portId, portId->GetName());
                                getMessageContext()->PortRedeclare(&sourceLocation, vName.c_str());
                            } else {
                                getMessageContext()->TopLevelMultiPortNet(p);
                            }
                        }
                        NULvalue* the_lvalue;
                        if ( whole_net ){
                            the_lvalue = new NUIdentLvalue(p, sourceLocation);
                        } else {
                            the_lvalue = new NUVarselLvalue (p, port_selected_range, sourceLocation);
                        }
                        //mapPort(dynamic_cast<NUScope*>(module), portId, the_lvalue, sourceLocation);
                        mapPort(dynamic_cast<NUScope*>(module), pc, the_lvalue, sourceLocation);
                    }
                }
            }
        }
    }

    // Create other module Items
    bool accumulatedErrorFlag = hasError();
    mNucleusBuilder->setError(false);  // temporarily clear flag so we can process the remainder of the module items
    VeriModuleItem *mi;
    FOREACH_ARRAY_ITEM(node.GetModuleItems(), i, mi) {
        if ( hasError() ) {
          accumulatedErrorFlag = true;
          mNucleusBuilder->setError(false);  // temporarily clear flag so we can process the remainder of the module items
        }
        if (mi) { 
            // skip port decl already processed
            if (!mi->IsIODecl()) {
                SourceLocator mi_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,mi->Linefile());
                mi->Accept(*this);
                addModuleItem(mi->GetClassId(), mi_loc);
            }
        }
    }
    mNucleusBuilder->setError(accumulatedErrorFlag);     // restore accumulated error flag so the caller will know how the population of this module proceeded
    uDesign();
    //restore the scope.
    PopScope();
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "---------------------------------------------------------------" << std::endl;
    std::cerr << "END NUCLEUSBUILDER VERIMODULE" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriDataDecl, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIDATADECL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VERI_VISIT_NODE(VeriModuleItem, node) ; // Call of base class
    
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();

    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // check for unsupported data types
    if (IsUnsupportedTypeRef(node.GetDataType())) {
        UtString msg("References to objects of type '");
        msg << VeriNode::PrintToken(node.GetDataType()->GetType());
        msg << "' (class,struct...) are not supported";
        reportUnsupportedLanguageConstruct(sourceLocation, msg);
        return;
    }
    // Do we claim to support 'const' property for declarations? I have seen some cases working, but we don't do anything special with this SV 'const' keyword (just ignoring so far)... 
    // Is this behavioral we want to continue or do we want to report unsupported language construct report?
    // On the other hand this seems to be some restriction on which parser can benefit and probably no difference for population
    //if (node.GetQualifier(VERI_CONST)) {
    //    UNSUPPORTED_SV_DECLARATION(node, VERI_CONST); // this is an error
    //}
    // process directives if any
    Map* pragmas = static_cast<VeriIdDef*>(node.GetIds()->GetLast())->GetIdPragmas();
    Map* lineinfos = static_cast<VeriIdDef*>(node.GetIds()->GetLast())->GetPragmaLineInfos();
    // check if not already added
    if (mDeclarationRegistry.find(static_cast<VeriIdDef*>(node.GetIds()->GetLast())) == mDeclarationRegistry.end()) {
        if (pragmas) {
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
            printf("Got %d pragmas\n",pragmas -> Size());
#endif
            // Get module name
            NUModule* module = mNucleusBuilder->gModule(getNUScope(&node));
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, node, "null owner module");
            UtString moduleName = module->getName()->str();
            // Get net name, if multiple declarations found directive applied to the last one
            UtString netName(gVisibleName(static_cast<VeriIdDef*>(node.GetIds()->GetLast()), createNucleusString(static_cast<VeriIdDef*>(node.GetIds()->GetLast())->GetName())));
            addPragmas(&netName, &moduleName, pragmas, lineinfos, &sourceLocation);
        }
    }
    // Create Ports and add to module 
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(node.GetIds(), i, id) {
        UtVector<UtPair<int,int> > dims;
        if (!id || id->IsParam()) continue ;

        //RJC RC#2013/09/17 (cloutier) See if this can be shared with similar code in VERI_VISIT(VeriAnsiPortDecl, node)
        unsigned assignBitWidth = 0; // the assignment width of ID, in verilog declaration assignments to memories (multi dimensions) is not allowed so
                                     // assignBitWidth only tracks the width that would be used for an assignment to ID

        // SV_ISSUE this will need to be revisited in SV because assignments to memories are allowed, and an assignment to a large memory will likely
        // cause node.StaticSizeSign(0) to return a value that has the MSB set to 1, which would make it look like it is signed.
        //ADDRESSED FD: I'm taking this one. 'assignBitWidth' now generalized to handle width for memories and used as context size for initializations (instead id->StaticSizeSign(0) self-determined size)


        unsigned firstUnpackedDim = 0;
        VeriDataType* dataType = gMaxRangeDataType(node.GetDataType(), id->GetDataType());
        DimType dimType;
        parseDimensions(dataType, id, sourceLocation, &dimType, &assignBitWidth, &dims, &firstUnpackedDim);
        UtString netName = gVisibleName(id, createNucleusString(id->GetName()));
        int context_size_sign = gScope()->getContextSizeSign();
        int savedContextSizeSign = context_size_sign;
        if ( 0 == context_size_sign ){
            // 0 means self determined, so do the self determined calculation now
            // we only need the sign part
            VeriDataType* node_dataType = node.GetDataType();
            // NOTE: node.GetDataType() can be NULL when a port is split into multiple declarations.
            // In this case, we will come back and set the signed on the declaration in which the datatype is specified.
            bool isSigned = ( node_dataType && (1 == node_dataType->Sign()));
            isSigned |= (1 == GET_CONTEXT_SIGN(node.StaticSizeSign(0)));
            unsigned context_sign = isSigned ? 1 : 0;
            context_size_sign = MAKE_CONTEXT_SIZE_SIGN(assignBitWidth,context_sign);
            savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign); // save starting value
        }
        bool isSigned = (1 == GET_CONTEXT_SIGN(context_size_sign)); // 1 means signed result, 0 unsigned
        ///TODO This isn't quite right for SystemVerilog default port type is 'logic', as well 'inheritance' mechanism should be used to correctly set port type for SystemVerilog (just like direction is done)
        ///As well it seems verilog 95 insists on default port type being wire, so may need todo version dependent coding here
        ///It seems current interra/carbon approach - populating VERI_NONE for functions, isn't precise, as well from LRM point of view
        int veri_type = VERI_WIRE;
        NUScope* scope = getNUScope(&node);
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(scope, node, "Unable to get parent scope");
        NUScope::ScopeT scope_type = scope->getScopeType();
        veri_type = ((dataType && dataType->GetType()) ? dataType->GetType() : veri_type);
        if ((VERI_INPUT == node.GetDir()) || (VERI_OUTPUT == node.GetDir()) || (VERI_INOUT == node.GetDir())) {
            NUNet* p = NULL;
            if (BIT == dimType) {
                p = mNucleusBuilder->cPort(scope, netName, node.GetDir(), veri_type, isSigned, sourceLocation);
            } else if (VECTOR == dimType) {
                p = mNucleusBuilder->cPort(scope, netName, node.GetDir(), dims[0], veri_type, isSigned, sourceLocation);
            } else if (MEMORY == dimType) {
                p = mNucleusBuilder->cPort(scope, netName, node.GetDir(), firstUnpackedDim, dims, veri_type, isSigned, sourceLocation); 
            } else {
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Incorrect dimension type");
            }
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(p, node, "Unable to create port");
            // aPortToScope needs to be delayed for modules - in order todo it in declaration order
            if (NUScope::eModule != scope_type) {
                mNucleusBuilder->aPortToScope(p, scope);
            }
            mDeclarationRegistry[id] = p; 
        } else {
            NUNet* r = NULL;
            if (mDeclarationRegistry.find(id) != mDeclarationRegistry.end()) {
                // this net was declared in the port list and the NUNet was created there, here we update flags that might only be defined here (and not on port)
                r = mDeclarationRegistry[id];
                if ( isSigned ) {
                    mNucleusBuilder->setNetFlag(r, eSigned);
                }
                mNucleusBuilder->sNetType(r, veri_type);
            } else {
                NUScope* declaration_scope = getDeclarationScope2();
                if (BIT == dimType) {
                    r = mNucleusBuilder->cNet(declaration_scope, netName, dataType->GetType(),
                            false, isSigned, sourceLocation);
                } else if (VECTOR == dimType) {
                    r = mNucleusBuilder->cNet(declaration_scope, netName, dataType->GetType(),
                            dims[0], false, isSigned, sourceLocation);
                } else if (MEMORY == dimType) {
                    r = mNucleusBuilder->cNet(declaration_scope, netName, dataType->GetType(),
                            firstUnpackedDim, dims, false, isSigned, sourceLocation); 
                } else {
                    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Incorrect dimension type");
                }
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(r, node, "Unable to create net");
                mNucleusBuilder->aNetToScope(r, declaration_scope);
                mDeclarationRegistry[id] = r; 
            }
            // TODO It seems this should be checked for all VeriModuleItems and process in each place corresponding to Interra
            if (id && id->IsTickProtectedNode()){
                // mark nets that are declared within `protected regions as temp so they are not dumped in waveforms
                mNucleusBuilder->setNetFlag(r, eTempNet);
            }
            // Check for initial value. If one exists, create a blocking assign, and 
            // (1) if it's a static declaration (per SystemVerilog rules) put it
            //     in an initial block at the top of this scope, or
            // (2) if it's an automatic declaration, return blocking assign for use
            //     by caller 
            NUExpr* initialValue = gInitialValueExpr(id, node, (BIT == dimType), assignBitWidth);
            if (initialValue) {
                if (node.IsNetDecl()) {
                    createInitialValueContinuousAssign(initialValue, static_cast<VeriNetDecl&>(node), r);
                } else { 
                    NUBlockingAssign* blockAssign = createInitialValueBlockingAssign(initialValue, node, r);
                    if (V2NDesignScope::FOR_SCHEME == c) {
                        // Push blocking assign onto return value vector
                        UtVector<NUBase*> blockingAssignVector;
                        gScope()->gValueVector(blockingAssignVector);
                        blockingAssignVector.push_back(blockAssign);
                        gScope()->sValueVector(blockingAssignVector);
                    } else {
                        // Find the initial block that will contain the blocking assigns. If one does not exist, create it.
                        NUBlock* block = gInitializerBlock(getNUScope(&node), sourceLocation);
                        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(block, node, "Unable to create initial block for initializer");
                        // Now add the blocking assign to the initial block
                        mNucleusBuilder->aStmtToBlock(blockAssign, block);
                    }
                }
            }
        }
        (void) gScope()->updateContextSizeSign(savedContextSizeSign);//reset the context size sign back to value it had upon entry to this block
    }

    // Restore current node context 
    gScope()->sContext(c);

#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIDATADECL" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriAnsiPortDecl, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIANSIPORTDECL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    //if (node.GetDataType().GetType()) _ofs << gStringValue(node.GetType()) << " " ;
    //if (node.GetDataType().GetSigning()) _ofs << gStringValue(node.GetSigning()) << " " ;

    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // check for unsupported data types
    if (IsUnsupportedTypeRef(node.GetDataType())) {
        UtString msg("References to objects of type '");
        msg << VeriNode::PrintToken(node.GetDataType()->GetType());
        msg << "' (class,struct...) are not supported";
        reportUnsupportedLanguageConstruct(sourceLocation, msg);
        return;
    }
    // check for hierarchical references
    if (node.GetDataType() && node.GetDataType()->IsTypeRef() && (node.GetDataType()->GetTypeName() && node.GetDataType()->GetTypeName()->IsHierName())) {
        reportUnsupportedLanguageConstruct(sourceLocation, "Hierarchical reference appearing in ansi-port declaration");
        return;
    }
    // process directives if any
    Map* pragmas = static_cast<VeriIdDef*>(node.GetIds()->GetLast())->GetIdPragmas();
    Map* lineinfos = static_cast<VeriIdDef*>(node.GetIds()->GetLast())->GetPragmaLineInfos();
    // check if not already added
    if (mDeclarationRegistry.find(static_cast<VeriIdDef*>(node.GetIds()->GetLast())) == mDeclarationRegistry.end()) {
        if (pragmas) {
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
            printf("Got %d pragmas\n",pragmas -> Size());
#endif
            // Get module name
            NUModule* module = mNucleusBuilder->gModule(getNUScope(&node));
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, node, "null owner module");
            UtString moduleName = module->getName()->str();
            // Get net name, if multiple declarations found directive applied to the last one
            UtString netName(gVisibleName(static_cast<VeriIdDef*>(node.GetIds()->GetLast()), createNucleusString(static_cast<VeriIdDef*>(node.GetIds()->GetLast())->GetName())));
            addPragmas(&netName, &moduleName, pragmas, lineinfos, &sourceLocation);
        }
    }
    // Create Ports and add to module 
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(node.GetIds(), i, id) {
        if (!id) continue ;
        UtVector<UtPair<int,int> > dims;

        //RJC RC#2013/09/17 (cloutier) See if this can be shared with similar code in VERI_VISIT(VeriDataDecl, node)
        unsigned assignBitWidth = 0; // the assignment width of ID, in verilog declaration assignments to memories (multi dimensions) is not allowed so
                                     // assignBitWidth only tracks the width that would be used for an assignment to ID

        // SV_ISSUE this will need to be revisited in SV because assignments to memories are allowed on ports, and an assignment to a large memory will likely
        // cause node.StaticSizeSign(0) to return a value that has the MSB set to 1, which would make it look like it is signed.


        VeriDataType* dataType = gMaxRangeDataType(node.GetDataType(), id->GetDataType());
        DimType dimType;
        unsigned firstUnpackedDim = 0;
        parseDimensions(dataType, id, sourceLocation, &dimType, &assignBitWidth, &dims, &firstUnpackedDim);
        UtString netName = gVisibleName(id, createNucleusString(id->GetName()));
        unsigned portDirection = node.GetDir();
        // if portDirection is undefined
        if (!portDirection) {
            // use saved direction from previous port declaration
            portDirection = mPortDirectionToUseIfUndefined;
        }
        // save current direction
        mPortDirectionToUseIfUndefined = portDirection;
        NUNet* p = NULL;
        int context_size_sign = gScope()->getContextSizeSign();
        int savedContextSizeSign = context_size_sign;
        if ( 0 == context_size_sign ){
          // 0 means self determined, so do the self determined calculation here
          // node.GetDataType() can be null for port decls split across multiple lines. In
          // which case we will set the sign properly when we visit the datatype declaration
          // for the port.
          unsigned context_sign = (dataType ? dataType->Sign() : 0);
          context_size_sign = MAKE_CONTEXT_SIZE_SIGN(assignBitWidth,context_sign);
          savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign); // save starting value
        }
        bool isSigned = (1 == GET_CONTEXT_SIGN(context_size_sign)); // 1 means signed result, 0 unsigned
        NUScope* scope = getNUScope(&node);
        ///TODO This seems to be not quite right for SystemVerilog default port type is 'logic', as well 'inheritance' mechanism should be used to correctly set port type for SystemVerilog (just like direction is done)
        ///As well it seems verilog 95 insists on default port type being wire, so may need todo version dependent coding here
        ///It seems current interra/carbon approach - populating VERI_NONE for functions, isn't precise, as well from LRM point of view
        int veri_type = VERI_WIRE;
        veri_type = ((dataType && dataType->GetType()) ? dataType->GetType() : veri_type);
        if (BIT == dimType) {
            p = mNucleusBuilder->cPort(scope, netName, portDirection, veri_type, isSigned, sourceLocation);
        } else if (VECTOR == dimType) {
            p = mNucleusBuilder->cPort(scope, netName, portDirection, dims[0], veri_type, isSigned, sourceLocation);
        } else if (MEMORY == dimType) {
            p = mNucleusBuilder->cPort(scope, netName, portDirection, firstUnpackedDim, dims, veri_type, isSigned, sourceLocation); 
        } else {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Incorrect net type");
        }
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(p, node, "Unable to create port");
        // here we already have declaration order so adding port to scope 
        mNucleusBuilder->aPortToScope(p, scope);
        mDeclarationRegistry[id] = p; 
        (void) gScope()->updateContextSizeSign(savedContextSizeSign);//reset the context size sign back to value it had upon entry to this method
        // I hope ansi ports are always whole ids
        NULvalue* the_lvalue = new NUIdentLvalue(p, sourceLocation);
        mapPort(scope, id, the_lvalue, sourceLocation);
    }
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIANSIPORTDECL" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriContinuousAssign, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERICONTINUOUSASSIGN" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VERI_VISIT_NODE(VeriModuleItem, node) ; // Call of base class
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Check strengths
    if (node.GetStrength()) node.GetStrength()->Accept(*this) ;
    // Iterate over all assignments
    unsigned i ;
    VeriNetRegAssign *assign = NULL;
    FOREACH_ARRAY_ITEM(node.GetNetAssigns(), i, assign) {
        // Set left expression context
        gScope()->sContext(V2NDesignScope::LVALUE);

        VeriExpression* veLValExpr = assign->GetLValExpr();
        VeriExpression* veRValExpr = assign->GetRValExpr();
        
        int lhs_size_sign = (veLValExpr ? veLValExpr->StaticSizeSign() : 0);
        unsigned lhs_size = GET_CONTEXT_SIZE(lhs_size_sign);
        int rhs_size_sign = (veRValExpr ? veRValExpr->StaticSizeSign() : 0);
        unsigned rhs_size = GET_CONTEXT_SIZE(rhs_size_sign);
        unsigned rhs_sign = GET_CONTEXT_SIGN(rhs_size_sign);
        //get context size: max of lhs and rhs
        unsigned context_size = MAX(rhs_size,lhs_size);
        //context sign of rhs will be sign of rhs, (LRM says that LHS is not considered)
        int context_size_sign = MAKE_CONTEXT_SIZE_SIGN(context_size,rhs_sign);

        //RJC RC#2013/09/19 (frunze) how can there ever be a VeriContinuousAssign where veLValExpr is null?
        //ADDRESSED FD: at the moment I don't know such situation for continuous assign, that check for lvalue expression (you probably can see as well for all other Verific API's) was booking up from Verific prettyprinter examples, and so in order don't get seg fault in case there is some situation (unknown to me at the moment) where veLvalExpr is NULL, we have checking and then assertion below VERIFIC_CONSISTENCY_NO_RETURN_VALUE(left, node, "Unable to get left expression"); Sometimes I have seen some case where some check like that gets assertion fail, and so it might require some implicit creation of lvalue like in case of ++i lvalue is missing bet rules tell that 'i' should become lvalue of assignment i = i + 1. I don't think that the example is applicable to our case, but I can't exclude that might be some other case (as I see this checking in PrettyPrinter example in Verific)

        /// TODO cleanup this. mIgnoreContextSign - looks more like an workaround, there should be some solution with context_size_sign, as opposite to having another variable controlling context sign.
        mIgnoreContextSign = true; // informs to use operation self determined sign
        // First process left expression 
        if (veLValExpr) {
          veLValExpr->Accept(*this);
        }
        NULvalue* left = dynamic_cast<NULvalue*>(gScope() -> gValue());
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(left, node, "Unable to get left expression");
        mIgnoreContextSign = false; // reset flag
        gScope()->sValue(NULL); // clear return location in case the RHS does not put something here (such as when it is an unsupported construct)

        // now process right expression, here the size_sign is the combination of LHS and RHS calculated above
        int savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign); // save starting value
        mIgnoreContextSign = true; // informs to use operation self determined sign
        // Set right expression context
        gScope()->sContext(V2NDesignScope::RVALUE);
        // Get right expression 
        VeriExpression* evalRvalue = evalConstExpr(context_size_sign, veRValExpr);
        if (evalRvalue) {
            evalRvalue->Accept(*this);
            delete evalRvalue;
        } else if (veRValExpr) {
          veRValExpr->Accept(*this);
        }
        NUExpr* right = dynamic_cast<NUExpr*>(gScope() -> gValue()); 
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(right, node, "Unable to get right expression");
        mIgnoreContextSign = false; // reset flag 

        // Get parent module
        NUModule* module = mNucleusBuilder->gModule(getNUScope(&node));
        NUContAssign* contAssign = mNucleusBuilder->cContinuesAssign(module, left, right, sourceLocation); 
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(contAssign, node, "Unable to create continuous assign");
        // Set evaluated value
        // as we can have array of continous assignments (for loop above) we should add to module hear (continous assign should always be added to module)
        mNucleusBuilder->aContinuesAssignToModule(contAssign, module);
        gScope()->sValue(NULL);
        (void) gScope()->updateContextSizeSign(savedContextSizeSign);    //reset the context size sign back to value it had before this Net assign
    }
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERICONTINUOUSASSIGN" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriBinaryOperator, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIBINARYOPERATOR" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    int context_size_sign = gScope()->getContextSizeSign();
    int savedContextSizeSign = context_size_sign;
    // there can't be case where both left and right missing
    // for pre increment/decrement operation left doesn't exist
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(node.GetLeft() || node.GetRight(), node, "Incorrect empty left and right binary operation");
    int lhs_size_sign = 0;
    unsigned lhs_sign = 0;
    if (node.GetLeft()) {
        lhs_size_sign = node.GetLeft()->StaticSizeSign(0);
        lhs_sign = GET_CONTEXT_SIGN(lhs_size_sign);
    }
    // for post increment/decrement operation right doesn't exist
    int rhs_size_sign = 0;
    unsigned rhs_sign = 0;
    if (node.GetRight()) {
        rhs_size_sign = node.GetRight()->StaticSizeSign(0);
        rhs_sign = GET_CONTEXT_SIGN(rhs_size_sign);
    }
    if (node.GetLeft() && !node.GetRight()) {
        // if rhs missing use lhs for size/sign
        rhs_size_sign = lhs_size_sign;
        rhs_sign = lhs_sign;
    } else if (! node.GetLeft() && node.GetRight()) {
        // if lhs missing use rhs for size/sign
        lhs_size_sign = rhs_size_sign;
        lhs_sign = rhs_sign;
    }

    if ( (0 == context_size_sign) || mIgnoreContextSign ){
        // 0 means self determined, use Verific to get size_sign
        context_size_sign = node.StaticSizeSign(0);
        (void) gScope()->updateContextSizeSign(context_size_sign); // starting value saved above
        mIgnoreContextSign = false; // reset value
    }
    bool use_signed_binary_operator = useSignedBinaryOperator(node.OperType(), lhs_sign, rhs_sign);
    bool isSignedResult = (1 == GET_CONTEXT_SIGN(context_size_sign)); // 1 means signed result, 0 unsigned
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // check for unsupported binary operator
    if ((NUOp::eInvalid == mNucleusBuilder->gNucleusBinOp(node.OperType(), use_signed_binary_operator))
        && !isIncDecOperator(node.OperType()) && !isDoubleOperator(node.OperType()) && !isCompoundAssignOperator(node.OperType())) {
        UtString msg;
        msg << "Unsupported '" << VeriNode::PrintToken(node.OperType()) << "' binary operator";
        reportUnsupportedLanguageConstruct(sourceLocation, msg); // this is an error
        return;
    }
    // Create Binary Operation
    NUExpr* result = NULL;
    if (isCompoundAssignOperator(node.OperType())) { // handle compound operators (+=, -=, *= etc. SV only)
        HandleSVBinaryOperator(&node, context_size_sign, lhs_sign, rhs_sign, sourceLocation, &result);
    } else if (isIncDecOperator(node.OperType())) { // handle increment/decrement (SV only)
        if (node.GetLeft() && !node.GetRight()) {
            // we have post increment/decrement
            // i++ or i--
            // we populate the following logic
            // temp = i;
            // i = temp + 1;
            // return temp;
            result = createPostIncDec(node.OperType(), node.GetLeft(), isSignedResult, use_signed_binary_operator, sourceLocation); 
        } else if (!node.GetLeft() && node.GetRight()) {
            // we have pre increment/decrement
            // ++i or --i
            // we populate the following logic
            // i = i + 1;
            // result = i;
            result = createPreIncDec(node.OperType(), node.GetRight(), isSignedResult, use_signed_binary_operator, sourceLocation);
        }
    } else {

        // Set left expression context
        gScope()->sContext(V2NDesignScope::RVALUE);


        // Get left expression

        bool operandIsSelfDetermined = isOperandSelfDetermined(node.OperType(), false, 1); // binary op, first operand
        int savedContextSizeSignOp1 = gScope()->updateContextSizeSign(operandIsSelfDetermined ? 0 : context_size_sign);
        VeriExpression* evalLvalue = evalConstExpr( context_size_sign, node.GetLeft());
        if (evalLvalue) {
            evalLvalue->Accept(*this);
            delete evalLvalue; // this are not part of parse tree so needs to be deleted here
        } else if (node.GetLeft()) {
          node.GetLeft()->Accept(*this) ;
        }
        NUExpr* expr1 = dynamic_cast<NUExpr*>(gScope()->gValue());
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(expr1, node, "Unable to get left expression");
        (void) gScope()->updateContextSizeSign(savedContextSizeSignOp1);

        // Set right expression context
        gScope()->sContext(V2NDesignScope::RVALUE);
        operandIsSelfDetermined = isOperandSelfDetermined(node.OperType(), false,  2); // binary op, second operand
        int savedContextSizeSignOp2 = gScope()->updateContextSizeSign(operandIsSelfDetermined ? 0 : context_size_sign);
        // Get right expression
        VeriExpression* evalRvalue = evalConstExpr( context_size_sign, node.GetRight());
        if (evalRvalue) {
            evalRvalue->Accept(*this);
            delete evalRvalue;
        } else if (node.GetRight()) {
          node.GetRight()->Accept(*this) ;
        }
        NUExpr* expr2 = dynamic_cast<NUExpr*>(gScope()->gValue());
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(expr2, node, "Unable to get right expression");
        (void) gScope()->updateContextSizeSign(savedContextSizeSignOp2);
        // need to make pair of carbon operators, first is binary, second is optional and is reduction 
        // Check for double operation
        if (isDoubleOperator(node.OperType())) {
            UtString firstOp;
            firstOp += VeriNode::PrintToken(node.OperType())[1];
            UtString secondOp;
            secondOp += VeriNode::PrintToken(node.OperType())[0];
            NUExpr* first = mNucleusBuilder->cBinaryOperation(expr1, getVerificOperatorForBinaryDoubleOperator(firstOp), expr2, isSignedResult, use_signed_binary_operator, sourceLocation);
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(first, node, "Unable to create first operation");
            result = mNucleusBuilder->cUnaryOperation(first, getVerificOperatorForBinaryDoubleOperator(secondOp), sourceLocation);
        } else {
            result = mNucleusBuilder->cBinaryOperation(expr1, node.OperType(), expr2, isSignedResult, use_signed_binary_operator, sourceLocation);
        }
    }
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(result, node, "Unable to create binary operation");
    // Set evaluated value
    gScope()->sValue(result);
    // Restore current node context 
    gScope()->sContext(c);
    (void) gScope()->updateContextSizeSign(savedContextSizeSign);//reset the context size sign back to value it had upon entry to this method
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIBINARYOPERATOR" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriBlockingAssign, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIBLOCKINGASSIGN" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();

    // Assure validity of assignment     
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(node.GetLVal() || node.GetValue(), node, "For blocking assign left and right expressions can't be empty at the same time");
    mIgnoreContextSign = true; // informs to use operation self determined sign
    // for --i, ++i case we don't have implicit lvalue, so node.GetLVal() will return NULL, 
    // but for nucleus population later we will expand --i, ++i,to i = i - 1 ,i = i + 1, so at the end will have left and right
    // for more details refer to HandleSVBlockingAssign function implementation 
    NULvalue* left = NULL;
    if (node.GetLVal()) {
        // Set left expression context
        gScope()->sContext(V2NDesignScope::LVALUE);
        // Get left expression 
        node.GetLVal()->Accept(*this) ;
        left = dynamic_cast<NULvalue*>(gScope()->gValue());
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(left, node, "Unable to get left expression");
    }
    mIgnoreContextSign = false; // reset flag

    // for assignment, the context size/sign is based on LHS and RHS
    
    // for pre increment/decrement operation lhs doesn't exist
    int lhs_size_sign = 0;
    unsigned lhs_size = 0;
    unsigned lhs_sign = 0;
    if (node.GetLVal()) {
        lhs_size_sign = node.GetLVal()->StaticSizeSign();
        lhs_size = GET_CONTEXT_SIZE(lhs_size_sign);
        lhs_sign = GET_CONTEXT_SIGN(lhs_size_sign);
    }
    // for post increment/decrement operation rhs doesn't exist
    int rhs_size_sign = 0;
    unsigned rhs_size = 0;
    unsigned rhs_sign = 0;
    if (node.GetValue()) {
        rhs_size_sign = node.GetValue() -> StaticSizeSign();
        rhs_size = GET_CONTEXT_SIZE(rhs_size_sign);
        rhs_sign = GET_CONTEXT_SIGN(rhs_size_sign);
    }
    if (node.GetLVal() && !node.GetValue()) {
        // if no rhs use lhs size sign
        rhs_size_sign = lhs_size_sign;
        rhs_size = lhs_size;
        rhs_sign = lhs_sign;
    } else if (!node.GetLVal() && node.GetValue()) {
        // if no lhs use rhs size sign
        lhs_size_sign = rhs_size_sign;
        lhs_size = rhs_size;
        lhs_sign = rhs_sign;
    }
    //get context size: max of lhs and rhs
    unsigned context_size = MAX(rhs_size,lhs_size);
    //context sign of rhs will be sign of rhs, (LRM says that LHS is not considered)
    int context_size_sign = MAKE_CONTEXT_SIZE_SIGN(context_size,rhs_sign);
    int savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign); // save starting value


    mIgnoreContextSign = true; // informs to use operation self determined sign
    // for Verilog 95/2001 OperType is always 0
    // for SV we need check node.OperType()
    // if node.OperType() is VERI_EQUAL_ASSIGN then we have simple '=' blocking assign
    // Get right expression
    NUExpr* right = 0;
    if (node.OperType() && node.OperType() != VERI_EQUAL_ASSIGN) {
        HandleSVBlockingAssign(&node, context_size_sign, lhs_sign, rhs_sign, sourceLocation, &left, &right);
    } else {
        // Set right expression context
        gScope()->sContext(V2NDesignScope::RVALUE);
        // Get right expression
        //We don't replace actual parse-tree data just get evaluated version for nucleus generation
        //It may be required to pass first argument context_size from lval object
        //TODO: overcomment this.

        VeriExpression* evalRvalue = evalConstExpr(context_size_sign, node.GetValue());
        // node.GetLVal() and node.GetValue() are Verific methods to get left and right expressions correspondingly for VeriBlockingAssign.
        // In fact I think node.GetValue() member function should probably have been named node.GetRVal()
        // evalRvalue will be Null if node was not a constant
        if (evalRvalue) {
            evalRvalue->Accept(*this);
            delete evalRvalue;
        } else if (node.GetValue()) node.GetValue()->Accept(*this) ;
        if (hasError()) return;
        // Get right expression
        right = dynamic_cast<NUExpr*>(gScope()->gValue()); 
    }
    //Here we must already have left and right sides
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(right, node, "Unable to get right expression");
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(left, node, "Unable to get left expression");
    mIgnoreContextSign = false; // reset flag 
    // Create blocking assign
    NUBase* blockingAssign = mNucleusBuilder->cBlockingAssign(left, right, sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(blockingAssign, node, "Unable to create blocking assign");
    // Set evaluated value
    gScope()->sValue(blockingAssign);
    // Restore current node context 
    gScope()->sContext(c);
    (void) gScope()->updateContextSizeSign(savedContextSizeSign);//reset the context size sign back to value it had upon entry to this method
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIBLOCKINGASSIGN" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriAlwaysConstruct, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIALWAYSCONSTRUCT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VERI_VISIT_NODE(VeriModuleItem, node) ; // Call of base class
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();

    // Check for unsupported types
    if (node.GetQualifier(VERI_FINAL)) {
        UNSUPPORTED_SV_DECLARATION(node, VERI_FINAL); // this is an error
    }
    // Check is this always block is combinational
    //TODO this seems not right in case of mixed events the block is not considered combinational and level expressions gets ignored.
    mIsCombAlways = isCombAlways(node.GetStmt());
    // Get parent module
    NUModule* module = mNucleusBuilder->gModule(getNUScope(&node));

    // If the statement within the always block is not a VeriSeqBlock,
    // then we'll need to add a begin/end
    // to capture function call residuals (see VeriFunctionCall comments).
    NUBlock* block = NULL;

    // First, get the statement within the always block. 
    VeriStatement* alwaysStatement = NULL;
    if (node.GetStmt() && node.GetStmt()->IsEventControl())
      // We have an event control. Examine the statement portion of the event control.
      alwaysStatement = node.GetStmt()->GetStmt();
    else
      // No event control.
      alwaysStatement = node.GetStmt();
    
    // If it's not a begin/end AND there's a function call (or fopen) underneath, create
    // a dummy begin/end. 
    // NOTE: As a rule we probably don't need to check for side effect statements
    //       underneath; we could just unconditionally create a dummy begin/end
    //       if there isn't already one there. But by making it conditional,
    //       there is less of an impact on tests that that have dumpVerilog
    //       gold files - because the Interra flow does not seem to have to
    //       insert the dummy begin/end.
    bool requiresDummyBlock = not alwaysStatement || (not alwaysStatement->IsSeqBlock() && Verific2NucleusUtilities::hasSideEffectStmt(alwaysStatement));
    if (requiresDummyBlock) 
      block = createBlockAndPushScope(alwaysStatement, sourceLocation, "block");

    // Now process the statements
    // Event controls are only legal inside an always construct.
    sEventControlLegal(true);
    if (alwaysStatement) alwaysStatement->Accept(*this);
    sEventControlLegal(false);
    
    // Retrieve the generated statement
    if (block) {
      // Put it in the block we created, and pop the scope
      NUStmt* stmt = dynamic_cast<NUStmt*>(gScope()->gValue()); 
      if (stmt) mNucleusBuilder->aStmtToBlock(stmt, block);
      PopScope();
    } else {
      // We didn't create a block, so create one
      block = mNucleusBuilder->blockify(dynamic_cast<NUStmt*>(gScope()->gValue()), module, sourceLocation);
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(block, node, "Unable to create block");
    }

    // At this point, we're guaranteed to have a block,
    // even for empty always (e.g. always;)
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(block, node, "Unable to get block statement");

    //RJC RC#16  The use of alwaysBlock here seems confusing to me, it looks like event expressions are added to this always block along
    //RJC RC#16  with statements, then the event exprs and statements are removed and replaced by the statements that were inside the block.
    //RJC RC#16  Can you explain the need for this complexity?
    //ADDRESSED FD: Complicated part related to changeNet detection logic, which is used to create event for Display. I think this part will need some
    // bug fixing and refactoring during system tasks carbon regression fix, but in fact the code is similar to Interra it's just that we don't want
    // to change the always block creation structure... and for changeNet we need a bit different thing, and we are taking/adjusting this info inplace. 
    // I will make sure the code is optimal as much as possible during system tasks refactoring.

    // Create Always block
    // always block should always be added to parent module

    // setup the type of the always block
    NUAlwaysBlock::NUAlwaysT always_type = ((node.GetQualifier(VERI_ALWAYS_COMB)) ? NUAlwaysBlock::eAlwaysComb :   // SV always_comb
                                            (node.GetQualifier(VERI_ALWAYS_FF)) ? NUAlwaysBlock::eAlwaysFF :       // SV always_ff
                                            (node.GetQualifier(VERI_ALWAYS_LATCH)) ? NUAlwaysBlock::eAlwaysLatch : // SV always_latch
                                            NUAlwaysBlock::eAlways);                                                // plain always block (SV, verilog or v2k

    // the following convoluted equation for isWildcard is patterned after scheme used in VeriDelayOrEventControl::PrettyPrint
    bool isWildcard = ( (NUAlwaysBlock::eAlways == always_type)  && (node.GetStmt()) && (node.GetStmt()->GetAt()) && ((node.GetStmt()->GetAt()->Size() == 0)));
    
    NUAlwaysBlock* alwaysBlock = mNucleusBuilder->createAlwaysBlockInScope(module, block, isWildcard, always_type, sourceLocation);

    // Accumulate change nets
    NUNetVector changeNets;
    NUNetRefSet senseUses(mNucleusBuilder->gNetRefFactory());
    // Handle Edge Expression
    if (node.GetStmt() && !node.GetStmt()->GetAt()) {
        // No trigger, set level expr list to true if we are doing synthesis checks, false otherwise.
        alwaysBlock->setLevelExprList(mNucleusBuilder->getSynthFlag());
    } else if (node.GetStmt() && node.GetStmt()->GetAt()->Size() != 0) {
        // With no edge expression (combinational always block) and no side effects (such as $display),
        // set level expr list to true if we are doing synthesis checks, false otherwise.
        alwaysBlock->setLevelExprList(mIsCombAlways && !mHasSideEffectStatement && mNucleusBuilder->getSynthFlag());

        unsigned i=0 ;
        VeriExpression *eventExpr=NULL;
        bool isMixed = isMixedEvent(node.GetStmt());
        if (isMixed) {
            //print error message wrt to the first event expression -- get the first one.
            FOREACH_ARRAY_ITEM(node.GetStmt()->GetAt(), i, eventExpr) {
                //use the eventExpr statement not the always block for source locator.
                sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,eventExpr->Linefile());
                getMessageContext()->UnsuppMixedEvent(&sourceLocation);
                break;
            }
        }
        FOREACH_ARRAY_ITEM(node.GetStmt()->GetAt(), i, eventExpr) {
            if (eventExpr) {
                if (isMixed && !(eventExpr->GetEdgeToken())) {
                    //Ignore level expressions in case of mixed expression
                    continue;
                }
                gScope()->sContext(V2NDesignScope::RVALUE);
                eventExpr->Accept(*this);
            }
            NUExpr* eExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(eExpr, node, "Unable to get event expression");

            if (mIsCombAlways && mHasSideEffectStatement) {
                // create temp net, accumulate
                UtString netName(mNucleusBuilder->gNextUniqueName(getNUScope(&node), "changebool")->str());
                NUNet* changeNet = mNucleusBuilder->cNet(module, netName, VERI_NONE, true, false, sourceLocation);
                changeNet->putIsClearAtEnd(true);
                mNucleusBuilder->aNetToScope(changeNet, module);
                mNucleusBuilder->aNetToNetVector(changeNet, changeNets);
                // create change detect
                NUExpr* changeDetect = mNucleusBuilder->cChangeDetect(eExpr);
                NULvalue* lhs = mNucleusBuilder->cIdentLvalue(changeNet, sourceLocation);
                // Create the assignment for the result of the change detection
                NUContAssign* assign = mNucleusBuilder->cContinuesAssign(module, lhs, changeDetect, sourceLocation);
                mNucleusBuilder->aContinuesAssignToModule(assign, module); 
                // now we need to mark all of the signals involved in the
                // sensivity list as protected observable so that they will not
                // be rescoped
                eExpr->getUses(&senseUses);
                for (NUNetRefSet::NetRefSetLoop l = senseUses.loop(); !l.atEnd(); ++l) {
                    const NUNetRefHdl netRef = *l;
                    NUNet *net = netRef->getNet();
                    // mark net as protected observable
                    net->putIsProtectedObservableNonHierref(mNucleusBuilder->gIODB());
                }
            } else {
                mNucleusBuilder->aEventExprToAlwaysBlock(eExpr, alwaysBlock);
            }
        }
    }

    if (!isWildcard && mIsCombAlways && mHasSideEffectStatement) {
        // Get the uses for this always block. We will need it for a warning message.
        NUNetRefSet alwaysUses(mNucleusBuilder->gNetRefFactory());
        LFFindUsesCallback alwaysCallback(&alwaysUses);
        NUDesignWalker walker(alwaysCallback, false, false);
        walker.alwaysBlock(alwaysBlock);
        // Print warnings about uses in the always block that are not in the sensitivity list.
        NUNetRefSet diffs(mNucleusBuilder->gNetRefFactory());
        NUNetRefSet::set_difference(alwaysUses, senseUses, diffs);
        for (NUNetRefSet::SortedLoop l = diffs.loopSorted(); !l.atEnd(); ++l) {
            // Compose a name for the net ref
            const NUNetRefHdl netRef = *l;
            UtString name;
            netRef->compose(&name, NULL);
            //const char* allreasons[] = { "Output Sys Task",
            //                             "PLI Call",
            //                             "C-Model w/ Side Effects" };
            // Currently we have only 1 reason "Output Sys Task"
            UtString reasons("Output Sys Task");
            // Print the warning
            getMessageContext()->MissingSense(&(alwaysBlock->getLoc()), name.c_str(), reasons.c_str(),
                    name.c_str());
        }
    }
                
    if (!changeNets.empty()) {
        // Gather the statements from the block in the always block. We will
        // put them in the then clause for the event detection.
        NUStmtList stmts;
        NUAlwaysBlock* nAlwaysBlock = dynamic_cast<NUAlwaysBlock*>(alwaysBlock);
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(nAlwaysBlock, node, "Unable to get alwaysBlock");
        mNucleusBuilder->gStmtsFromAlwaysBlock(&stmts, nAlwaysBlock) ;

        // Create the conditional expression for the if statement from the
        // change net bools.
        NU_ASSERT(!changeNets.empty(), alwaysBlock);
        NUExpr* cond = mNucleusBuilder->cOrConditions(changeNets);
        //cond->resize(1);

        // Create the if statement with all the original block statements
        NUStmtList elseStmts; //else stmts are empty
        NUStmt* ifStmt = mNucleusBuilder->cIf(cond, stmts, elseStmts, sourceLocation);

        // Replace the block statements with the new ifStmt
        stmts.clear();
        mNucleusBuilder->aStmtToStmtList(ifStmt, &stmts);
        mNucleusBuilder->rStmtList(nAlwaysBlock, stmts);

    }

    // Getting event expressions which are not in top block
    UtVector<NUBase*> eventExprs;
    gScope()->gValueVector(eventExprs);
    UtVector<NUBase*>::iterator it = eventExprs.begin();
    while(it != eventExprs.end()) {
        NUExpr* eExpr = dynamic_cast<NUExpr*>(*it);
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(eExpr, node, "Unable to get event expression");
        mNucleusBuilder->aEventExprToAlwaysBlock(eExpr, alwaysBlock);
        ++it;
    }


    // bug6225.  Walk the if-statement conditions, substituting any
    // temp-clocks we have created, so that reset-expressions use the
    // same temp in the edge condition as they do in the condition.
    if (! mIsCombAlways) {
        ReplaceTempClocks replace_temp_clocks(mNucleusBuilder);
        replace_temp_clocks.walkFirstStmt(block->loopStmts());
    }


    mHasSideEffectStatement = false;
    mIsCombAlways = false;
    // Set evaluated value
    gScope()->sValue(NULL);
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIALWAYSCONSTRUCT" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriSeqBlock, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERISEQBLOCK" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Create sequential block
    NUBlock* block = mNucleusBuilder->cBlock(getNUScope(&node), sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(block, node, "Unable to create block");
    // Create scope
    V2NDesignScope* current_scope = new V2NDesignScope(block, gScope());
    // Get parent declaration scope (if not defined it's module/task/function scope)
    NUScope* parent_declaration_scope = getDeclarationScope();
 
    // Create a named declaration scope iff there are declarations,
    // or if the begin block has a label.
    bool declScope = (node.GetDeclItems() != NULL) || (node.GetLabel() && node.GetLabel()->Name());
    
    // Get Block name
    UtString blockName;
    if (node.GetLabel() && node.GetLabel()->Name()) {
      blockName = gVisibleName(&node, createNucleusString(node.GetLabel()->GetName()));
    }
    else {
      StringAtom* blockNameId = mNucleusBuilder->gNextUniqueName(getNUScope(&node), "block");
      blockName = UtString(blockNameId->str());
      mNameRegistry[&node] = blockNameId; 
    }
    // Getting event expr vector
    UtVector<NUBase*> exprVector;
    gScope()->gValueVector(exprVector);
    // Push the scope
    PushScope(current_scope, blockName);
    // Create a named declaration scope, and set scope to it
    if (declScope) {
      NUNamedDeclarationScope* namedDeclarationScope = NULL;
      namedDeclarationScope = mNucleusBuilder->cNamedDeclarationScope(blockName, parent_declaration_scope, sourceLocation);
      gScope()->sDeclarationScope(namedDeclarationScope);
    }
    // Restoring event expr vector
    gScope()->sValueVector(exprVector);

    // Get declarations
    unsigned i ;
    VeriModuleItem *decl ;
    FOREACH_ARRAY_ITEM(node.GetDeclItems(), i, decl) {
        if (decl) decl->Accept(*this) ;
    }
    // Get statement list
    VeriStatement *veriStmt;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, veriStmt) {
        if (veriStmt) veriStmt->Accept(*this) ;
        // Event controls have to be the first statment within an always construct.
        // If we have just processed anything other than a begin/end, then
        // event controls are no longer legal.
        if (veriStmt) {
          if (!veriStmt->IsSeqBlock()) sEventControlLegal(false);
        }
        NUStmt* stmt = dynamic_cast<NUStmt*>(gScope()->gValue());
        if (!stmt) continue;
        mNucleusBuilder->aStmtToBlock(stmt, dynamic_cast<NUBlock*>(block));
    }
    // Getting event expr vector
    exprVector.clear();
    gScope()->gValueVector(exprVector);
    // restore the scope.
    PopScope();
    // Restoring event expr vector
    gScope()->sValueVector(exprVector);
    // Set evaluated value
    gScope()->sValue(block);
    // Restore current node context // TODO remove context restorations
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERISEQBLOCK" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriEventExpression, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIEVENTEXPRESSION" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Set expression context
    gScope()->sContext(V2NDesignScope::RVALUE);
    // the size/sign is self determined
    int context_size_sign = node.StaticSizeSign();
    int savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign);

    // check unsupported iff expression
    if (node.GetIffCondition()) {
        reportUnsupportedLanguageConstruct(sourceLocation, "iff expression"); // this is an error
        return;
    }
 
    if (!node.GetEdgeToken()) mIsCombAlways = true;
    // Get expression 
    if (node.GetExpr()) node.GetExpr()->Accept(*this);
    NUExpr* eExpr = dynamic_cast<NUExpr*>(gScope()->gValue()); 
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(eExpr, node, "Unable to get edge expression");
    eExpr->resize(eExpr->determineBitSize());       
    NUExpr* exprRef = eExpr;
    // handle vector net case
    bool isVectorNet = false;
    if (node.GetExpr() && node.GetExpr()->IsIdRef() && node.GetEdgeToken()) {
        NUIdentRvalue* e = dynamic_cast<NUIdentRvalue*>(eExpr);
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(e, node, "Incorrect event expression");
        NUNet* n = e->getIdent();
        if (n->getBitSize() != 1) {
            getMessageContext()->LFWideEdge(&sourceLocation, n->getName()->str());
            // builds VarSelRvalue[0:0] for vector net
            NUExpr* ee = e->resizeSubHelper(1);
            ee->clearBitSizeCache();
            eExpr = ee;
            isVectorNet = true;
        }
    }
    if (!mIsCombAlways && node.GetExpr() && (!node.GetExpr()->IsIdRef() || isVectorNet) && !node.GetExpr()->IsHierName()) {
        // handling not idref clock case
        NUNet* eNet = mNucleusBuilder->getEdgeNet(eExpr);
        // always create new net for constant 
        if ((NULL == eNet) || eExpr->isConstant()) {
            NUModule* module = mNucleusBuilder->gModule(getNUScope(&node));
            UtString netName(mNucleusBuilder->gNextUniqueName(module, "tempclk")->str());
            NUNet* tempClk = mNucleusBuilder->cNet(module, netName, VERI_WIRE, true, false, sourceLocation);
            mNucleusBuilder->aNetToScope(tempClk, module);
            NULvalue* tempClkLvalExpr = mNucleusBuilder->cIdentLvalue(tempClk, sourceLocation);
            NUContAssign* assign = mNucleusBuilder->cContinuesAssign(module, tempClkLvalExpr, eExpr, sourceLocation);
            mNucleusBuilder->aContinuesAssignToModule(assign, module);
            // never register constant net
            if (! eExpr->isConstant()) {
                mNucleusBuilder->putEdgeNet(eExpr, tempClk);
            }
            NUExpr* tempClkRvalExpr = mNucleusBuilder->cIdentRvalue(tempClk, sourceLocation);
            exprRef = tempClkRvalExpr;
        } else {
            delete eExpr;
            exprRef = mNucleusBuilder->cIdentRvalue(eNet, sourceLocation);
        }
    }
    NUBase* eventExpr = mNucleusBuilder->cEventExpr(node.GetEdgeToken(), exprRef, sourceLocation);
    // Create edge expression
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(eventExpr, node, "Unable to create edge expression");
    // Set evaluated value
    gScope()->sValue(eventExpr);
    // Restore current node context 
    gScope()->sContext(c);
    (void) gScope()->updateContextSizeSign(savedContextSizeSign);    //reset the context size sign back to value it had upon entry to this method
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIEVENTEXPRESSION" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriNetDecl, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERINETDECL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // No need for separate implementation, originally confused by Vendors/verific/example_apps/verilog_apps/analysis_apps/Ex5Visitor.cpp VerNetDecl node visitor implementation, but Vendors/verific/verilog/VeriVisitor.cpp VeriNetDecl visitor population shows that it differs from VeriDataDecl only by strength and delay addtion.
    // Delays are ignorable for nucleus, so here just delegating population to parent (VeriDataDecl) and checking for strength;
    // NOTE: I was looking for same kind of merging for VeriAnsiPortDecl , but that's differ a lot (starting from fact that VeriAnsiPortDecl is an expression and of course isn't direved from VeriDataDecl which is VeriModuleItem).
    VERI_VISIT_NODE(VeriDataDecl, node); // call of base class;
    if (node.GetStrength()) node.GetStrength()->Accept(*this) ;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERINETDECL" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriQuestionColon, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIQUESTIONCOLON" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Set ifExpr context
    gScope()->sContext(V2NDesignScope::RVALUE);

    // Get ifExpr, which is self determined
    int context_size_sign = node.GetIfExpr()->StaticSizeSign();
    int savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign);
    VeriExpression* evalIFValue = evalConstExpr(context_size_sign, node.GetIfExpr());
    if (evalIFValue) {
        evalIFValue->Accept(*this);
        delete evalIFValue;
    } else if (node.GetIfExpr()) {
        node.GetIfExpr()->Accept(*this) ;
    }
    NUExpr* ifExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(ifExpr, node, "Unable to get if expression");
    // RJC how does this work, to get here we asserted we must have an ifExpr, so why are we testing to see if it is an id ref below?
    if (node.GetIfExpr()->IsIdRef()) {
        // check for != 0
        unsigned constSize = mNucleusBuilder->gExprWidth(ifExpr);
        NUExpr* constantZero = mNucleusBuilder->cConst(0u, constSize, false, sourceLocation);
        ifExpr = mNucleusBuilder->cBinaryOperation(ifExpr, VERI_LOGNEQ, constantZero, false /*!Signed*/, false, sourceLocation);
    }
    (void) gScope()->updateContextSizeSign(savedContextSizeSign);    //reset the context size sign back to value it had before processing the ifCondition

    savedContextSizeSign = context_size_sign = gScope()->getContextSizeSign();
    if ( 0 == context_size_sign ) {
      // 0 means self determined, use Verific to get size_sign
      context_size_sign = node.StaticSizeSign(0);
      savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign); // save starting value
    }
    
    // Set thenExpr context
    gScope()->sContext(V2NDesignScope::RVALUE);
    // Get thenExpr
    VeriExpression* evalThenValue = evalConstExpr(context_size_sign, node.GetThenExpr());
    if (evalThenValue) {
        evalThenValue->Accept(*this);
        delete evalThenValue;
    } else if (node.GetThenExpr()) {
        node.GetThenExpr()->Accept(*this) ;
    }
    NUExpr* thenExpr = dynamic_cast<NUExpr*>(gScope()->gValue()); 
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(thenExpr, node, "Unable to get then expression");
    // Set elseExpr context
    gScope()->sContext(V2NDesignScope::RVALUE);
    // Get elseExpr
    VeriExpression* evalElseValue = evalConstExpr(context_size_sign, node.GetElseExpr());
    if (evalElseValue) {
        evalElseValue->Accept(*this);
        delete evalElseValue;
    } else if (node.GetElseExpr()) {
        node.GetElseExpr()->Accept(*this) ;
    }
    NUExpr* elseExpr = dynamic_cast<NUExpr*>(gScope()->gValue()); 
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(elseExpr, node, "Unable to get else expression");
    // Create ternary operation
    NUBase* ternaryOperation = mNucleusBuilder->cTernaryOperation(ifExpr, thenExpr, elseExpr, sourceLocation); 
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(ternaryOperation, node, "Unable to create ternary operation");
    // Set evaluated value
    gScope()->sValue(ternaryOperation);
    // Restore current node context 
    gScope()->sContext(c);
    (void) gScope()->updateContextSizeSign(savedContextSizeSign);    //reset the context size sign back to value it had upon entry to this method
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIQUESTIONCOLON" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriIdRef, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIIDREF" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // handle parameters
    if (node.GetId()->IsParam()) {
        // populate param value;
        node.GetId()->GetInitialValue()->Accept(*this);
        // we have already return value so just return 
        return;
    }
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    int context_size_sign = gScope()->getContextSizeSign();
    int savedContextSizeSign = context_size_sign; // save starting value
    if ( 0 == context_size_sign ) {
      // 0 means self determined, use Verific to get size_sign
      context_size_sign = node.StaticSizeSign(0);
      (void) gScope()->updateContextSizeSign(context_size_sign); // update to self determined, starting value was already saved
    }

    // Declared ID's always stored in global registry
    NUNet* cId = NULL;
    if (mDeclarationRegistry.find(node.GetId()) != mDeclarationRegistry.end()) {
      cId = mDeclarationRegistry[node.GetId()];
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(cId, node, "Unable to get declaration");
    } else {
        UtString idName(gVisibleName(node.GetId(), createNucleusString(node.GetId()->GetName())));
        // We don't yet support declarations in global scope
        if (isInGlobalScope(node.GetId())) {
           UtString msg;
           msg << "SystemVerilog variable '" << idName << "' declared at global scope";
           mNucleusBuilder->reportUnsupportedLanguageConstruct(sourceLocation, msg.c_str());
        }
        // Here we don't need block scope, we either need parent NamedDeclarationScope or parent unit module or task/function 
        NUScope* declaration_scope = getDeclarationScope();
        // if not found according to verilog rules creating implicit net (1 bit length wire) in declaration scope
        cId = mNucleusBuilder->cNet(declaration_scope, idName, VERI_WIRE, false, false, sourceLocation);
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(cId, node, "Unable to create net");
        // Register net;
        mDeclarationRegistry[node.GetId()] = cId;
        // add net to scope
        mNucleusBuilder->aNetToScope(cId, declaration_scope);
    }
    NUBase* exprId = NULL;
    // Create IdentRvalue/IdentLvalue depending on used context
    if (V2NDesignScope::LVALUE == c) {
        exprId = mNucleusBuilder->cIdentLvalue(cId, sourceLocation);
    } else if (V2NDesignScope::RVALUE == c) {
        exprId = mNucleusBuilder->cIdentRvalue(cId, sourceLocation);
    } else {
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Incorrect context set for VERIIDREF"); 
    }
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(exprId, node, "Unable to create expression");
    // Set found id
    gScope()->sValue(exprId);
    // Restore current node context 
    gScope()->sContext(c);
    (void) gScope()->updateContextSizeSign(savedContextSizeSign);    //reset the context size sign back to value it had upon entry to this method    
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIIDREF" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriConstVal, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERICONSTVAL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();

    std::stringstream constValStream;
    // Handling string literals
    if (node.IsString()) {
        char *str = node.Image() ;
        UtString stripValue(str);
        if ((stripValue.size() > 1) && ('"' == stripValue[0]) && ('"' == stripValue[stripValue.size() - 1])) {
            // remove leading and trailing quotes if they existed
            stripValue = stripValue.substr(1, stripValue.size() - 2);
        }
        UtString origStr(stripValue.c_str());
        UtString binaryStr;
        HdlVerilogString::convertVerilogStringToBinaryString(&origStr, &binaryStr);
        constValStream << binaryStr.c_str();
        Strings::free(str) ;
    } else {

        //if (node.IsSigned()) { constValStream << node.Size(NULL) << "'sb" ; }
        //else                { constValStream << node.Size(NULL) << "'b" ; }

        // Go MSB to LSB. MSB is highest index. 0 the lowest.
        unsigned i = node.Size(NULL) ;
        // Mask: 0 - x,z,?; 1 - 0,1
        while (i-- != 0) {
            // Determine the value of this bit
            if (GET_BIT(node.GetXValue(),i))         { constValStream << 'x' ;}
            else if (GET_BIT(node.GetZValue(),i))    { constValStream << 'z' ;}
            else if (GET_BIT(node.GetValue(),i))     { constValStream << '1' ;}
            else                                     { constValStream << '0' ;}
        }
    }
    // Create constVal
    NUBase* constVal = mNucleusBuilder->cConst(constValStream.str().c_str(), node.Size(NULL), node.HasXZ(),
            node.Sign(), node.IsString(), sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(constVal, node, "Unable to create constant value");
    // Set evaluated value
    gScope()->sValue(constVal);
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERICONSTVAL" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriGateInstantiation, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIGATEINSTANTIATION" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VERI_VISIT_NODE(VeriModuleItem, node) ; // Call of base class
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context
    
    if ( NULL != node.GetStrength() ) {
      getMessageContext()->StrengthAnnotationIgnored(&sourceLocation);      // the optional strength specification is ignored
    }

    V2NDesignScope::ContextType c = gScope()->gContext();
    // Get gate type
    bool isMultiOutput = false;
    bool isMultiInput = false; 
    bool isTriState = false; 
    getGateFlags(node.GetInstType(), &isMultiOutput, &isMultiInput, &isTriState);
    // Warn that the strength will not be reduced if this is a resistive device
    if (isTriState && isResistiveDevice(node.GetInstType()))
        getMessageContext()->NoStrengthReduceRDev(&sourceLocation);

    unsigned i ;
    VeriInstId *inst ;
    FOREACH_ARRAY_ITEM(node.GetInstances(), i, inst) {
        unsigned j ;
        VeriExpression *pc ;
        // Get actuals
        NUExprVector inputs;
        NULvalueVector outputs;
        FOREACH_ARRAY_ITEM(inst->GetPortConnects(), j, pc) {
            // Port connection list is always there. Even when empty
            if (!pc) continue ;

            // special handling for pullup/pulldown
            if ((VERI_PULLUP == node.GetInstType()) || (VERI_PULLDOWN == node.GetInstType())) {
                // Set port connection context
                gScope()->sContext(V2NDesignScope::RVALUE);
                // Get port connection
                pc->Accept(*this);
                NUExpr* pcExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(pcExpr, node, "Unable to get port expression");
                // allowed port expressions are: NUIdentRvalue, NUVarselRvalue, NUMemselRvalue 
                NUNet* net = mNucleusBuilder->gNetFromExpr(pcExpr); 
                // report if unable to get net 
                if (!net) {
                    getMessageContext()->PullNotWhole(&sourceLocation);
                    delete pcExpr;
                    return;
                }
                delete pcExpr;
                mNucleusBuilder->sNetPullUpDown(net, node.GetInstType());
                continue;
            }
            if (!isMultiOutput && !isMultiInput && !isTriState) {
                UtString name(gVisibleName(&node, VeriNode::PrintToken(node.GetInstType())));
                getMessageContext()->UnsupportedPrimitive(&sourceLocation, name.c_str());
                setError(true);
                return;
            }


            // if j==0 then we are at the first port.
            // The first port of a gate is always an output. For multioutput gates all but the last one are outputs.
            bool isOutput = (isMultiOutput && (inst->GetPortConnects()->Size() - 1 != j)) || (isMultiInput && !j) || (isTriState && !j);
            if (!isOutput) VERIFIC_CONSISTENCY_NO_RETURN_VALUE((isMultiOutput && (inst->GetPortConnects()->Size() - 1 == j)) 
                || (isMultiInput && j) || (isTriState && j), node, "incorrect port/gate type");
            if (isOutput) {
                // Set port connection context
                gScope()->sContext(V2NDesignScope::LVALUE);
                // Get port connection
                pc->Accept(*this);
                NULvalue* output = dynamic_cast<NULvalue*>(gScope()->gValue());
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(output, node, "Unable to get output port connection");
                mNucleusBuilder->aLvalueToLvalueVector(output, outputs);
            // Is input
            } else  {
                // Set port connection context
                gScope()->sContext(V2NDesignScope::RVALUE);
                // Get port connection
                VeriExpression* evalPCValue = evalConstExpr(0, pc);
                if (evalPCValue) {
                    evalPCValue->Accept(*this);
                    delete evalPCValue;
                } else {
                    pc->Accept(*this);
                }
                NUExpr* input = dynamic_cast<NUExpr*>(gScope()->gValue());
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(input, node, "Unable to get input port connection");
                // NOTE: If this is an instance array, Verific has already taken 
                // bit selects on the inputs, provided that the number of bits
                // matches the number if array instances, or is 1. Here
                // We take care of the case in which the number of bits does
                // not match the number of array instances, and is not 1.
                UInt32 inputSize = input->getBitSize();
                if (inputSize > 1) {
                  // this is not an error according to most simulators, but is an
                  // error according to the LRM.  Most simulators use only the low bit,.
                  // so warn the user that we are doing what most other simulators do.
                  // RBS: Not sure we want this warning, since Verific already gives an error message
                  // (though it is much more terse).
                  UInt32 numInstances = node.GetInstances()->Size();
                  getMessageContext()->IncorrectExprWidthOnPrimitiveGatePort(&sourceLocation, j+1, "input", inputSize, numInstances);
                  input = mNucleusBuilder->cVarselRvalue(input, UtPair<int, int>(0,0), sourceLocation);
                }
                mNucleusBuilder->aExprToExprVector(input, inputs);
            }
        }
        if ((VERI_PULLUP == node.GetInstType()) || (VERI_PULLDOWN == node.GetInstType())) {
            continue;
        }
        // Gates in nucleus handled using unary/binary/ternary operators so
        // we need to synthesize gate operations using operators 
        NULvalue* resultLval = NULL;
        NUExpr* resultExpr = NULL;
        if (isTriState) {
            if ((VERI_CMOS == node.GetInstType()) || (VERI_RCMOS == node.GetInstType())) {
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(inputs.size() == 3, node, "Incorrect number of inputs for gate");
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(outputs.size() == 1, node, "Incorrect number of outputs for gate");
                // The order of ports in tristate gates are fixed (output, input1, ncontrol, pcontrol)
                // for cmos/rcmos we need to create two transistors nmos/pmos
                NULvalue* result1Lval = outputs[0];
                NULvalue* result2Lval = mNucleusBuilder->copyLval(result1Lval);
                NUExpr* else1Expr = mNucleusBuilder->cConst("z", 1, true, VERI_UNSIGNED, false, sourceLocation);
                NUExpr* else2Expr = mNucleusBuilder->cConst("z", 1, true, VERI_UNSIGNED, false, sourceLocation);
                NUExpr* if1Expr = inputs[1];
                NUExpr* if2Expr = mNucleusBuilder->cUnaryOperation(inputs[2], VERI_LOGNOT, sourceLocation);
                NUExpr* then1Expr = inputs[0];
                NUExpr* then2Expr = mNucleusBuilder->copyExpr(then1Expr);
                NUExpr* result1Expr = mNucleusBuilder->cTernaryOperation(if1Expr, then1Expr, else1Expr, sourceLocation); 
                NUExpr* result2Expr = mNucleusBuilder->cTernaryOperation(if2Expr, then2Expr, else2Expr, sourceLocation); 
                // Creating continues assign
                NUContAssign* cont1Assign = mNucleusBuilder->cContinuesAssign(getNUScope(&node), result1Lval, result1Expr, sourceLocation);
                mNucleusBuilder->aContinuesAssignToModule(cont1Assign, dynamic_cast<NUModule*>(getNUScope(&node)));
                NUContAssign* cont2Assign = mNucleusBuilder->cContinuesAssign(getNUScope(&node), result2Lval, result2Expr, sourceLocation);
                mNucleusBuilder->aContinuesAssignToModule(cont2Assign, dynamic_cast<NUModule*>(getNUScope(&node)));
            } else {
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(inputs.size() == 2, node,"Incorrect number of inputs for gate");
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(outputs.size() == 1, node,"Incorrect number of outputs for gate");
                // The order of ports in tristate gates are fixed (output, input1, control)
                resultLval = outputs[0];
                NUExpr* elseExpr = mNucleusBuilder->cConst("z", 1, true, VERI_UNSIGNED, false, sourceLocation);
                NUExpr* ifExpr = inputs[1];
                NUExpr* thenExpr = inputs[0];
                // TODO : use switch
                if ((VERI_BUFIF0 == node.GetInstType()) || (VERI_NOTIF0 == node.GetInstType()) || (VERI_PMOS == node.GetInstType()) 
                    || (VERI_RPMOS == node.GetInstType())) {
                    ifExpr = mNucleusBuilder->cUnaryOperation(ifExpr, VERI_LOGNOT, sourceLocation);
                }
                if ((VERI_NOTIF0 == node.GetInstType()) || (VERI_NOTIF1 == node.GetInstType())) {
                    thenExpr = mNucleusBuilder->cUnaryOperation(thenExpr, VERI_LOGNOT, sourceLocation);
                }
                resultExpr = mNucleusBuilder->cTernaryOperation(ifExpr, thenExpr, elseExpr, sourceLocation); 
            }
        } else  if (isMultiOutput) { // VERI_BUF or VERI_NOT
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(inputs.size() == 1, node, "Incorrect number of inputs for gate");
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(outputs.size() > 0, node, "Incorrect number of outputs for gate");
            // Create operation
            NUExpr* result = NULL;
            if (VERI_BUF == node.GetInstType()) {
              result = inputs[0];
            } else { // VERI_NOT
              result = mNucleusBuilder->cUnaryOperation(inputs[0], node.GetInstType(), sourceLocation);
            }
            // Construct right and left hand sides of continuous assignment.
            // Single output case (no temp net)
            if (outputs.size() == 1) {
              resultExpr = result;
              resultLval = outputs[0];
            }
            // Multiple output case (create temporary net)
            else {
              NUScope* scope = getNUScope(&node);
              UtString inetName(mNucleusBuilder->gNextUniqueName(scope, "cse_inst_array")->str());
              // Create scalar temp net
              NUNet* inet = mNucleusBuilder->cNet(scope, inetName, VERI_WIRE, true, false, sourceLocation);
              // add net to scope
              mNucleusBuilder->aNetToScope(inet, scope);
              // Create lvalue
              NULvalue* lvalNet = mNucleusBuilder->cIdentLvalue(inet, sourceLocation);
              // Create rvalue
              NUExpr* rvalNet = mNucleusBuilder->cIdentRvalue(inet, sourceLocation);
              // synthesize the following replication logic for multi-output gates (buf, not) 
              // e.g.  buf bufi(out0, out1, ... , outn, in0)
              // => {out0,out1,...,outn} = {N{in0}}
              NUContAssign* tempAssign = mNucleusBuilder->cContinuesAssign(scope, lvalNet, result, sourceLocation);
              mNucleusBuilder->aContinuesAssignToModule(tempAssign, scope->getModule());
              NUExprVector iconcats;
              mNucleusBuilder->aExprToExprVector(rvalNet, iconcats);
              resultExpr = mNucleusBuilder->cConcatRvalue(iconcats, outputs.size(), sourceLocation);
              resultLval = mNucleusBuilder->cConcatLvalue(outputs, sourceLocation);
            }
        } else if (isMultiInput) {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(inputs.size() > 1, node, "Incorrect number of inputs for gate");
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(outputs.size() == 1, node, "Incorrect number of outputs for gate");
            resultLval = outputs[0];
            resultExpr = NULL;
            if (isDoubleOperator(node.GetInstType())) {
                std::pair<unsigned, unsigned> operations = getDoubleOperator(node.GetInstType());
                NUExpr* first = NULL;
                NUExpr* temp = inputs[0];
                for (unsigned i = 1; i < inputs.size(); ++i) {
                    first = temp;
                    temp = mNucleusBuilder->cBinaryOperation(first, operations.first, inputs[i], false /*always unsigned?*/, false, sourceLocation);
                }
                resultExpr = mNucleusBuilder->cUnaryOperation(temp, operations.second, sourceLocation);
            } else {
                NUExpr* first = NULL;
                resultExpr = inputs[0];
                for (unsigned i = 1; i < inputs.size(); ++i) {
                    first = resultExpr;
                    resultExpr = mNucleusBuilder->cBinaryOperation(first, node.GetInstType(), inputs[i], false /*always unsigned?*/, false, sourceLocation);
                }
            }
        } else {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Unsupported gate type");
        }
        if ((VERI_CMOS != node.GetInstType()) && (VERI_RCMOS != node.GetInstType())) {
            // Creating continues assign
            NUContAssign* contAssign = mNucleusBuilder->cContinuesAssign(getNUScope(&node), resultLval, resultExpr, sourceLocation);
            mNucleusBuilder->aContinuesAssignToModule(contAssign, getNUScope(&node)->getModule());
        }
    }
    gScope()->sValue(NULL);     //  the value was set during the Accept of the child (gate), no need to set it here
    gScope()->sContext(c);      // Restore current node context 
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIGATEINSTANTIATION" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriNonBlockingAssign, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERINONBLOCKINGASSIGN" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    //node.StaticReplaceConstantExpr(false);
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Set left expression context
    gScope()->sContext(V2NDesignScope::LVALUE);

    // the size/sign is based on LHS and RHS
    int lhs_size_sign = node.GetLVal()->StaticSizeSign();
    unsigned lhs_size = GET_CONTEXT_SIZE(lhs_size_sign);
    int rhs_size_sign = node.GetValue() -> StaticSizeSign();
    unsigned rhs_size = GET_CONTEXT_SIZE(rhs_size_sign);
    unsigned rhs_sign = GET_CONTEXT_SIGN(rhs_size_sign);
    //get context size: max of lhs and rhs
    unsigned context_size = MAX(rhs_size,lhs_size);
    //context sign of rhs will be sign of rhs (LRM says that LHS is not considered)
    int context_size_sign = MAKE_CONTEXT_SIZE_SIGN(context_size,rhs_sign);
    int savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign);

    // Get left expression
    mIgnoreContextSign = true; // informs to use operation self determined sign
    if (node.GetLVal()) node.GetLVal()->Accept(*this) ;
    NULvalue* left = dynamic_cast<NULvalue*>(gScope()->gValue());
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(left, node, "Unable to get left expression");
    mIgnoreContextSign = false; // reset flag 
    // Set right expression context
    gScope()->sContext(V2NDesignScope::RVALUE);
    // Get right expression
    mIgnoreContextSign = true; // informs to use operation self determined sign
    VeriExpression* evalRvalue = evalConstExpr(context_size_sign, node.GetValue());
    if (evalRvalue) {
        evalRvalue->Accept(*this);
        delete evalRvalue;
    } else if (node.GetValue()) node.GetValue()->Accept(*this) ;
    NUExpr* right = dynamic_cast<NUExpr*>(gScope()->gValue());
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(right, node, "Unable to get right expression");
    mIgnoreContextSign = false; // reset flag 
    // Create nonBlockingAssign
    NUBase* nonBlockingAssign = mNucleusBuilder->cNonBlockingAssign(left, right, sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(nonBlockingAssign, node, "Unable to get nonblockign assign");
    // Set evaluated value
    gScope()->sValue(nonBlockingAssign);
    // Restore current node context 
    gScope()->sContext(c);
    (void) gScope()->updateContextSizeSign(savedContextSizeSign);    //reset the context size sign back to value it had upon entry to this method
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERINONBLOCKINGASSIGN" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriUnaryOperator, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIUNARYOPERATOR" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // check for unsupported unary operator
    if ((NUOp::eInvalid == mNucleusBuilder->gNucleusUnOp(node.OperType()))
        && !isDoubleOperator(node.OperType())) {
        UtString msg;
        msg << "Unsupported '" << VeriNode::PrintToken(node.OperType()) << "' unary operator";
        reportUnsupportedLanguageConstruct(sourceLocation, msg); // this is an error
        return;
    }
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    int context_size_sign = gScope()->getContextSizeSign();
    int savedContextSizeSign = context_size_sign;
    if ( 0 == context_size_sign ){
      // 0 means self determined, use Verific to get size_sign, actually we should use node.ContextSizeSign(0) here
      context_size_sign = node.StaticSizeSign(0);
      bool opIsSelfDetermined = isOperandSelfDetermined(node.OperType(), true, 1); // unary op,  first (and only operand)
      savedContextSizeSign = gScope()->updateContextSizeSign(opIsSelfDetermined ? 0 : context_size_sign); // save starting value
    }
    mIgnoreContextSign = true; // informs to use operation self determined sign
    // Set operation context
    gScope()->sContext(V2NDesignScope::RVALUE);
    // Get expression
    if (node.GetArg()) node.GetArg()->Accept(*this) ;
    NUExpr* expr = dynamic_cast<NUExpr*>(gScope()->gValue());
    mIgnoreContextSign = false; // reset flag 
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(expr, node, "Unable to get unary argument");
    // Create unary operation
    NUBase* result = NULL;
    // handle double operators (~^ etc., which evaluate into 2 operations in nucleus)
    if (isDoubleOperator(node.OperType())) {
        UtString firstOp;
        firstOp += VeriNode::PrintToken(node.OperType())[1];
        UtString secondOp;
        secondOp += VeriNode::PrintToken(node.OperType())[0];
        NUExpr* first = mNucleusBuilder->cUnaryOperation(expr, getVerificOperatorForUnaryDoubleOperator(firstOp), sourceLocation);
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(first, node, "Unable to get first operation");
        result = mNucleusBuilder->cUnaryOperation(first, getVerificOperatorForUnaryDoubleOperator(secondOp), sourceLocation);
    } else {
        result = mNucleusBuilder->cUnaryOperation(expr, node.OperType(), sourceLocation);
    }
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(result, node, "Unable to create unary operation");
    // Set evaluated value
    gScope()->sValue(result);
    // Restore current node context 
    gScope()->sContext(c);
    (void) gScope()->updateContextSizeSign(savedContextSizeSign);    //reset the context size sign back to value it had upon entry to this method
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIUNARYOPERATOR" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriConditionalStatement, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERICONDITIONALSTATEMENT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Set ifExpr context
    gScope()->sContext(V2NDesignScope::RVALUE);

    // Check for unsupported types
    if (node.GetQualifier(VERI_UNIQUE) || node.GetQualifier(VERI_UNIQUE0) || node.GetQualifier(VERI_PRIORITY)) {
        reportUnsupportedLanguageConstruct(sourceLocation, "if qualifiers (unique, unique0, priority) not supported");
        return;
    }
    // Get ifExpr, which is self determined
    int context_size_sign = node.GetIfExpr()->StaticSizeSign();
    int savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign);
    VeriExpression * ve_if = node.GetIfExpr();
    VeriExpression* evalIfCond = evalConstExpr(context_size_sign, ve_if);
    if (evalIfCond) {
      evalIfCond->Accept(*this);
      delete evalIfCond;
    } else if (ve_if) {
      ve_if->Accept(*this);
    }
    // quit if we have encountered error already (in general this should be done after each accept) 
    if (hasError()) return;
    NUExpr* ifExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
    if ( ! ifExpr ){
      reportUnsupportedLanguageConstruct(sourceLocation, "Undefined if condition expression");
      return;
    }
    ifExpr = ifExpr->makeConditionExpr();
    ifExpr->resize(1);
    (void) gScope()->updateContextSizeSign(savedContextSizeSign);    //reset the context size sign back to value it had before processing if condition

    //UtPair<NUBase*, unsigned> declarationScopePair = mNucleusBuilder->gParentDeclUnit(gScope()->gOwner());
    // Process the then Stmts
    NUStmtList thenStmts;
    if (node.GetThenStmt()) {

        // TODO if then stmt is not VeriSeqBlock (if is empty or has one statement) we need to create one (because some statements contain function calls which in nucleus become 2 statements. (See comments for VeriFunctionCall)
        if (! node.GetThenStmt()->IsSeqBlock()) {
            //pushscope
            // Create sequential block
            NUBlock* block = mNucleusBuilder->cBlock(getNUScope(&node), sourceLocation);
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(block, node, "Unable to create block");
            // Create scope
            V2NDesignScope* current_scope = new V2NDesignScope(block, gScope());
            StringAtom* scopeNameId = mNucleusBuilder->gNextUniqueName(getNUScope(&node), "block");
            UtString scopeName(scopeNameId->str());
            mNameRegistry[node.GetThenStmt()] = scopeNameId;
            // Push the scope
            PushScope(current_scope, scopeName);
            node.GetThenStmt()->Accept(*this) ;
            NUStmt* thenStmt = dynamic_cast<NUStmt*>(gScope()->gValue()); 
            if (thenStmt) mNucleusBuilder->aStmtToBlock(thenStmt, dynamic_cast<NUBlock*>(block));
            mNucleusBuilder->aStmtToStmtList(block, &thenStmts);
            PopScope();
        } else {
            node.GetThenStmt()->Accept(*this) ;
            NUStmt* thenStmt = dynamic_cast<NUStmt*>(gScope()->gValue()); 
            if (thenStmt) mNucleusBuilder->aStmtToStmtList(thenStmt, &thenStmts);
        }
        
        //if (thenStmt && ! isNamedBlock(node.GetThenStmt())) mNucleusBuilder->gStmtsFromBlock(declarationScopePair.first, &thenStmts, thenStmt);
        //else if (thenStmt) mNucleusBuilder->aStmtToStmtList(thenStmt, &thenStmts);
        //VERIFIC_CONSISTENCY_NO_RETURN_VALUE(thenStmt, node, "Unable to get then statement");
    }
    // Set elseStmts context
    gScope()->sContext(V2NDesignScope::RVALUE);
    // Get elseStmts
    NUStmtList elseStmts;
    if (node.GetElseStmt()) {
        // if there is no seq block create one 
        if (! node.GetElseStmt()->IsSeqBlock()) {
            //pushscope
            // Create sequential block
            NUBlock* block = mNucleusBuilder->cBlock(getNUScope(&node), sourceLocation);
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(block, node, "Unable to create block");
            // Create scope
            V2NDesignScope* current_scope = new V2NDesignScope(block, gScope());
            StringAtom* scopeNameId = mNucleusBuilder->gNextUniqueName(getNUScope(&node), "block");
            UtString scopeName(scopeNameId->str());
            mNameRegistry[node.GetElseStmt()] = scopeNameId;
            // Push the scope
            PushScope(current_scope, scopeName);
            node.GetElseStmt()->Accept(*this) ;
            NUStmt* elseStmt = dynamic_cast<NUStmt*>(gScope()->gValue()); 
            if (elseStmt) mNucleusBuilder->aStmtToBlock(elseStmt, dynamic_cast<NUBlock*>(block));
            mNucleusBuilder->aStmtToStmtList(block, &elseStmts);
            PopScope();
        } else {
            node.GetElseStmt()->Accept(*this) ;
            NUStmt* elseStmt = dynamic_cast<NUStmt*>(gScope()->gValue()); 
            if (elseStmt) mNucleusBuilder->aStmtToStmtList(elseStmt, &elseStmts);
        }
        ///if (elseStmt && ! isNamedBlock(node.GetElseStmt())) mNucleusBuilder->gStmtsFromBlock(declarationScopePair.first, &elseStmts, elseStmt);
        //else if (elseStmt) mNucleusBuilder->aStmtToStmtList(elseStmt, &elseStmts);
        //VERIFIC_CONSISTENCY_NO_RETURN_VALUE(elseStmt, node, "Unable to get else statement");
    }
    // Create if construct
    NUBase* result = mNucleusBuilder->cIf(ifExpr, thenStmts, elseStmts, sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(result, node, "Unable to create if expression");
    // Set evaluated value
    gScope()->sValue(result);
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERICONDITIONALSTATEMENT" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriIntVal, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIINTVAL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Create constVal
    NUBase* constVal = mNucleusBuilder->cConst(node.GetNum(), node.Size(NULL), node.Sign(), sourceLocation); //rjc probably should use  int context_size_sign = gScope()->getContextSizeSign();
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(constVal, node, "Unable to create integer value");
    // Set evaluated value
    gScope()->sValue(constVal);
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIINTVAL" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriFunctionDecl, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIFUNCTIONDECL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VERI_VISIT_NODE(VeriModuleItem, node) ; // Call of base class
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Skip if we already resolved the function
    if (mTFRegistry.find(node.GetFunctionId()) != mTFRegistry.end()) {
        gScope()->sValue(NULL);
        return;
    }
    // Create function decl
    NUModule* module = mNucleusBuilder->gModule(getNUScope(&node));
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, node, "Unable to get parent module");
    UtString functionName(gVisibleName(node.GetFunctionId(),createNucleusString(node.GetFunctionId()->GetName())));
    NUTask* tf = mNucleusBuilder->cTF(functionName, functionName, module, sourceLocation); // FD thinks: probably for verilog name and origName are the same 
    tf->putRequestInline(doInlineTasks());
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(tf, node, "Unable to create function declaration");
    // save the function decl NU object for use in VeriFunctionCall
    mTFRegistry[node.GetFunctionId()] = tf;
    V2NDesignScope* current_scope = new V2NDesignScope(tf, gScope());
    PushScope(current_scope, functionName);
    // Check if this is a void function
    // Verific doesn't provide simple way to detect this
    bool is_void_function = Verific2NucleusUtilities::isVoidFunction(&node);
    // add implicit output port only if we don't have void function
    if (!is_void_function) {
        // Adding function output port with function name
        UtVector<UtPair<int,int> > dims;
        VeriDataType* dataType = node.GetDataType();
        unsigned assignBitWidth = 0;
        DimType dimType;
        unsigned firstUnpackedDim = 0;
        if (dataType) {
            // here we don't have have id so just getting dataType dimensions (it's possible some unpacked dimension declared through typedef, although I'm not sure it's allowed by standard, anyway if it's allowed we already support it with the following generic call
            parseDimensions(dataType, 0, sourceLocation, &dimType, &assignBitWidth, &dims, &firstUnpackedDim);
        } else {
            dimType = BIT;
        }
        NUNet* p = NULL;
        int context_size_sign = gScope()->getContextSizeSign();
        //RJC RC#11/18/13 (frunze) This looks strange, how can there ever be a valid contextSizeSign at a point where a function is being
        //declared? Do we need to investigate whether context_size_sign is ever non-zero here, if it cannot then do we need to assert dataType here?
        bool isSigned = dataType ? dataType->Sign() : (1 == GET_CONTEXT_SIGN(context_size_sign)); // 1 means signed result, 0 unsigned

        int veri_type = ((node.GetDataType() && node.GetDataType()->GetType()) ? node.GetDataType()->GetType() : VERI_NONE);
        // create the implicit net created as the 'value' of the function 
        NUScope* scope = getNUScope(&node);
        if (BIT == dimType) {
            p = mNucleusBuilder->cPort(scope, functionName, VERI_OUTPUT, veri_type, isSigned, sourceLocation);
        } else if (VECTOR == dimType) {
            p = mNucleusBuilder->cPort(scope, functionName, VERI_OUTPUT, dims[0], veri_type, isSigned, sourceLocation); 
        } else if (MEMORY == dimType) {
            p = mNucleusBuilder->cPort(scope, functionName, VERI_OUTPUT, firstUnpackedDim, dims, veri_type, isSigned, sourceLocation); 
        } else {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Incorrect net type");
        }
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(p, node, "Unable to create output port");
        mNucleusBuilder->aPortToScope(p, scope);
        mDeclarationRegistry[node.GetFunctionId()] = p;
    }
    // Get ansi port declarations
    // default to input - LRM 1800-2005 
    mPortDirectionToUseIfUndefined = VERI_INPUT;
    unsigned i ;
    VeriAnsiPortDecl * ansi_decl_item = 0 ;
    FOREACH_ARRAY_ITEM(node.GetAnsiIOList(), i, ansi_decl_item) {
        if (ansi_decl_item) ansi_decl_item->Accept(*this) ;
    }
    // Adding other declarations (input ports and local nets)
    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(node.GetDeclarations(), i, decl_item) {
        if (decl_item) decl_item->Accept(*this) ;
    }
    // push block for statements
    // Adding statements
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, stmt) {
        if (stmt) stmt->Accept(*this) ;
        NUStmt* iStmt = dynamic_cast<NUStmt*>(gScope()->gValue());
        //Consider this for future - to exactly match up with Interra
        //iStmt = mNucleusBuilder->blockify(iStmt, gNUScope(&node), sourceLocation);
        if (iStmt) mNucleusBuilder->aStmtToTF(iStmt, tf);
    }
    PopScope(); // pop function scope
    mNucleusBuilder->aTFToModule(tf, module);
    // Set evaluated value
    gScope()->sValue(NULL);
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIFUNCTIONDECL" << std::endl;
#endif
}

// A function call is populated as a task that returns the function
// value in the first argument. As a consequence, VeriFunctionCalls
// result in the population of two statements, E.G.
//
//    lval = function(args)
//
// Translates into:
//
//    function_called_as_task($temp_lval, args);
//    lval = $temp_lval;
//
// The VeriFunctionCall puts the task enable in the containing
// scope, and returns the $temp_lval as an RVALUE, which in turn
// is processed by the statement or expression in which the function
// is invoked.
//
// There are scenarios, however, in which the VeriFunctionCall
// should not populate the containing scope, for example, in the
// following case:
//
//   for (i=0; i<MAX; i++)
//      lval=function(arg)
//
// By default, VeriFunctionCall will populate the
// scope above the for loop with the task call, resulting in
// something like this:
//
//   function_called_as_task($temp_lval, args);
//   for (i=0; i<MAX; i++)
//      lval = $temp_lval 
//
// To rectify this problem, a begin/end block is created around the 
// blocking assign to contain the task eanble generated by
// this visitor. 
//
// A similar strategy is employed in 'then' and 'else' clauses
// of an 'if' statement.


void VerificVerilogDesignWalker::VERI_VISIT(VeriFunctionCall, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIFUNCTIONCALL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    NUTaskEnable * taskEnable = 0;
    if (node.GetFunctionName()->IsHierName()) {
        // Call VeriSelectedName
        node.GetFunctionName()->Accept(*this);
        // Get Task enable hierarchical reference
        taskEnable = dynamic_cast<NUTaskEnable*>(gScope()->gValue());
        if (hasError()) return;
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(taskEnable, node, "Unable to get task enable hierarchical reference");
    }
    // Getting function nucleus object from temporaries 
    NUTask* tf = NULL;
    if (mTFRegistry.find(node.GetId()) != mTFRegistry.end()) {
        tf = mTFRegistry[node.GetId()];
    } else {
        // If function declaration not found in TF registry, get function decl through parse-tree backpointer
        VeriModuleItem* mi = node.GetId()->GetModuleItem();
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(mi, node, "Unable to get module item");
        mi->Accept(*this);
        if (mTFRegistry.find(node.GetId()) != mTFRegistry.end()) {
            tf = mTFRegistry[node.GetId()];
        }
    }
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(tf, node, "Unable to get function declaration");
    NUScope* scope = getNUScope(&node);
    NUScope::ScopeT scope_type = scope->getScopeType();
    // Getting parent module
    // FD thinks: below module get is used to create function return output
    // net. Can't function called inside function ? if current scope is
    // current function scope then probably we should get it's parent scope
    // instead of module scope, anyway this needs more deep debug
    NUModule* module = mNucleusBuilder->gModule(scope); // TODO: simpler is scope->getModule()
    // Creating output net in module scope (need to review flags)
    UtString netPrefix = createNucleusString(UtString("output_") + UtString(gVisibleName(node.GetId(), node.GetId()->GetName())));
    UtString netName(mNucleusBuilder->gNextUniqueName(module, netPrefix.c_str())->str());
    NUNet* oNet = mNucleusBuilder->cFunctionOutputNet(module, netName, VERI_NONE, true, false/*unsigned*/, tf, sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(oNet, node, "Unable to create net");
    mNucleusBuilder->aNetToScope(oNet, module);
    // Creating lvalue expression
    NUBase* oActual = mNucleusBuilder->cIdentLvalue(oNet, sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(oActual, node, "Unable to create IdentLvalue");
    // Creating TFArgConnection for output
    NUTFArgConnectionVector* argConnections = new NUTFArgConnectionVector;
    NUTFArgConnection* oArgConnection = mNucleusBuilder->cTFArgConnection(oActual, 0, tf, sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(oArgConnection, node, "Unable to create output connection");
    mNucleusBuilder->aTFArgConnectionToVector(oArgConnection, argConnections);
    // Creating/Adding input TF connection
    if (node.GetArgs()) {
        unsigned i ;
        VeriExpression *expr ;
        // RJC SV note: In verilog2001, and verilog95 all ports are inputs, in systemVerilog one or more ports may be an output or inout. With SV the setting the scope to V2NDesignScope::RVALUE will be wrong.
        FOREACH_ARRAY_ITEM(node.GetArgs(), i, expr) {
            if (!expr) continue ;
            // Set expr context
            gScope()->sContext(V2NDesignScope::RVALUE);
            VeriExpression* evalRvalue = evalConstExpr(0, expr);
            if (evalRvalue) {
                evalRvalue->Accept(*this);
                delete evalRvalue;
            } else {
                expr->Accept(*this) ;
            }
            NUBase* iActual = gScope()->gValue();
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(iActual, node, "Unable to get input actual");
            // Output not included so index should be +1
            NUTFArgConnection* iArgConnection = mNucleusBuilder->cTFArgConnection(iActual, i+1, tf, sourceLocation);
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(iArgConnection, node, "Unable to create input connection");
            mNucleusBuilder->aTFArgConnectionToVector(iArgConnection, argConnections);
        }
    }
    // Getting parent module 
    if (NUScope::eModule != scope_type) { 
        if (!taskEnable) taskEnable = mNucleusBuilder->createTaskEnable(tf, module, sourceLocation);
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(taskEnable, node, "Unable to create taskEnable");
        // Adding ArgConnections to TaskEnable
        mNucleusBuilder->aTFArgConnectionsToTaskEnable(argConnections, taskEnable);
        // Adding task enable to parent block
        if (NUScope::eBlock == scope_type) {
            NUBlock* block = dynamic_cast<NUBlock*>(scope);
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(block, node, "Incorrect block type");
            mNucleusBuilder->aStmtToBlock(taskEnable, block);
        } else {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE((NUScope::eTask == scope_type), node, "Unknown scope");
            // TODO review this logic, will be good to use existing nucleus
            // api as much as possible
            NUScope* declaration_unit_scope = mNucleusBuilder->gParentDeclUnit(scope);
            NUTask* task = dynamic_cast<NUTask*>(declaration_unit_scope);
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(task, node, "Incorrect task/function type");
            mNucleusBuilder->aStmtToTF(taskEnable, task);
        }
    } else {
          // we must be in a context where statements cannot be created (continuous assign)
          // in this case we create a dummy always block to hold the assignment
        // Creating task enable
        if (!taskEnable) taskEnable = mNucleusBuilder->createTaskEnable(tf, module, sourceLocation);
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(taskEnable, node, "Unable to create taskEnable");
        // Adding ArgConnections to TaskEnable
        mNucleusBuilder->aTFArgConnectionsToTaskEnable(argConnections, taskEnable);
        // Creating block
        NUBlock* block = mNucleusBuilder->cBlock(module, sourceLocation);
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(block, node, "Unable to create block");
        // Adding task enable to block
        mNucleusBuilder->aStmtToBlock(taskEnable, block);
        // Creating always block
        (void) mNucleusBuilder->createAlwaysBlockInScope(scope, block, false, NUAlwaysBlock::eAlways, sourceLocation);
    }
    delete argConnections;
    // Output of function is rvalue anyway so creating rvalue expression here
    NUBase* oActualValue = NULL;
    if (V2NDesignScope::LVALUE == c) {
        oActualValue = mNucleusBuilder->cIdentLvalue(oNet, sourceLocation);
    } else if (V2NDesignScope::RVALUE == c) {
        oActualValue = mNucleusBuilder->cIdentRvalue(oNet, sourceLocation);
    }
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(oActualValue, node, "Unable to create output actual");
    // Set evaluated value
    gScope()->sValue(oActualValue);
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIFUNCTIONCALL" << std::endl;
#endif
}


//RJC RC#11/18/13 (frunze) this method is now getting to be overly complex and appears to have a lot of duplciated code,
// I think it is time to refactor.
// One way you might consider doing this would be to move the series of if/elseif/ tests used with the
// different calls to cVarselLvalue and cVarselRvalue into a wrapper method that accepts a NUBase argument
// and does the dynamic casts and the dispatches to the different versions of cVarsel{L,R}value

void VerificVerilogDesignWalker::VERI_VISIT(VeriIndexedId, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIINDEXEDID" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Set prefix context
    gScope()->sContext(V2NDesignScope::RVALUE);

    bool is_from_parameter = false;
    /// Get prefix net
    NUBase* prefixNet = NULL;
    VeriExpression* evalPrefix = evalConstExpr(0, node.GetPrefix());
    if (evalPrefix) {
        evalPrefix->Accept(*this);
        // Constant
        prefixNet = gScope()->gValue();
        delete evalPrefix;
        // if a constant, there is a chance it came from a parameter
        is_from_parameter = (node.GetPrefix() && node.GetPrefix()->FullId() && node.GetPrefix()->FullId()->IsParam());
    } else if (node.GetPrefix()) {
        node.GetPrefix()->Accept(*this) ;
        NUIdentRvalue* prefix = dynamic_cast<NUIdentRvalue*>(gScope()->gValue());
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(prefix, node, "Unable to get prefix");
        prefixNet = mNucleusBuilder->gNetFromIdentRvalue(prefix);
        delete prefix;
        gScope()->sValue(NULL); //  we just deleted it, so make sure that it is not used again
    }
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(prefixNet, node, "Unable to get prefix net");
    NUVectorNet* nVectorNet = dynamic_cast<NUVectorNet*>(prefixNet);
    NUMemoryNet* nMemoryNet = dynamic_cast<NUMemoryNet*>(prefixNet);
    // Although a warning will be created for trying to do a variable select on a bitnet, we will create a NU object for it anyway
    NUBitNet* nBitNet = dynamic_cast<NUBitNet*>(prefixNet);
    NULvalue* nLvalue = dynamic_cast<NULvalue*>(prefixNet);
    NUExpr* nExpr = dynamic_cast<NUExpr*>(prefixNet);
    //mDeclarationRegistry[node.GetPrefix()] = prefixNet;
    // We need net here, so may need to get it out of rvalue object
    //NUBase* net = NULL;
    //mNucleusBuilder->gNetFExpr(prefix); 
    // Getting Index expression
    bool isBitSel = false;
    UtPair<int,int> dim;
    // Set index expression context
    gScope()->sContext(V2NDesignScope::RVALUE);
    // Get index expression
    NUExpr* indexExpr = NULL;
    int adjust = 0;
    VeriExpression* veri_indexExpr = node.GetIndexExpr(); // there is just one index expression in a VeriIndexedId, if there were more then it would have been a VeriIndexedMemoryId
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(veri_indexExpr, node, "Index expression is missing in this VeriIndexedId");
    const bool isRangeSel = veri_indexExpr->IsRange();
    bool isNonConstRangeSel = false;
    UtPair<int, int> declRange(0, 0);
    if (veri_indexExpr) {       // does it ever make sense if veri_indexExpr is null?
        // index expression is self determined sign/size
        int context_size_sign = veri_indexExpr->StaticSizeSign(0);
        int savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign);
        if ( isRangeSel ) {
            bool normalize = ! nMemoryNet; // do normalization only if it's not NUMemoryNet 
            // handle variable index range-select
            bool isConstant = veri_indexExpr->GetLeft()->IsConstExpr();
            if (!isConstant) {
                // if not constant we may need something for indexExpr
                isNonConstRangeSel = true;
                veri_indexExpr->GetLeft()->Accept(*this);
                indexExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
                if (indexExpr && !mNucleusBuilder->isMemoryNet(dynamic_cast<NUNet*>(prefixNet))) {
                    isBitSel = true;
                }
            }
            VeriRange* range = dynamic_cast<VeriRange*>(veri_indexExpr);
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(range, node, "Incorrect range type");
            // Though it's illegal, prefixNet does not have to be a vector.
            // If not, compute 'dim' as if it were a single bit vector net.
            unsigned width = 1;
            if (nVectorNet) {
                width = mNucleusBuilder->gNetLength(nVectorNet);
                declRange = mNucleusBuilder->gNetDeclRange(nVectorNet);
            } else if (nMemoryNet) {
                width = mNucleusBuilder->gNetLength(nMemoryNet, nMemoryNet->getNumDims() - 1);
                declRange = mNucleusBuilder->gNetDeclRange(nMemoryNet, nMemoryNet->getNumDims() - 1);
            } else {
                // TODO support bit net ( probably warning ) and expression/lvalue cases (may need to check size from context)
            }
            NUNet* net = dynamic_cast<NUNet*>(prefixNet);
            UtString netName("");
            if (net) {
                netName = net->getName()->str();
            }
            gRange(&dim, *range, width, normalize, netName, &declRange, &adjust, &sourceLocation);
            if (hasError()) return;
        } else {
            // a single index (not a range)
            VeriExpression* evalIndex = evalConstExpr(context_size_sign, veri_indexExpr);
            if (evalIndex) {
                evalIndex->Accept(*this);
                // not in parse tree needs to be deleted manually
                delete evalIndex;
            } else {
                veri_indexExpr->Accept(*this) ;
            }
            indexExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
            if (indexExpr && !mNucleusBuilder->isMemoryNet(dynamic_cast<NUNet*>(prefixNet))) {
                isBitSel = true;
            }
        }
        (void) gScope()->updateContextSizeSign(savedContextSizeSign);    //reset the context size sign back to value it had upon entry to the index expression
    }
    if (nMemoryNet && indexExpr) {
        mNucleusBuilder->adjustSelExpr(sourceLocation, nMemoryNet, true, &indexExpr);
    }
    NUExpr* concatRvalue = NULL;
    NULvalue* concatLvalue = NULL;
    if (nMemoryNet && isRangeSel) {
        // in this case the prefixNet is a memory, and the selection is a range.  There is no NU object that can express this so create a concat of the memory elements within the range
        // function deletes indexExpr at the end
        bool inverse = false; // downto
        if (isNonConstRangeSel) {
            inverse = isInverseNonConstRangeSelect(veri_indexExpr->RangeCast(), declRange); 
        } 
        
        NUBase* resultConcat = genMemoryPartSelect(&node, veri_indexExpr, &indexExpr, dim, nMemoryNet, c, inverse, isNonConstRangeSel, sourceLocation);
        concatRvalue = dynamic_cast<NUConcatOp*>(resultConcat);
        concatLvalue = dynamic_cast<NUConcatLvalue*>(resultConcat);
    }
    if (isBitSel && ! isRangeSel) {
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(indexExpr, node, "Unable to get index expression");
        NUBase* varSel = NULL;
        if (V2NDesignScope::LVALUE == c) {
            if (nBitNet) varSel = mNucleusBuilder->cVarselLvalue(nBitNet, indexExpr, sourceLocation); 
            else if (nVectorNet) varSel = mNucleusBuilder->cVarselLvalue(nVectorNet, indexExpr, sourceLocation); 
            else if (nLvalue) varSel = mNucleusBuilder->cVarselLvalue(nLvalue, indexExpr, sourceLocation); 
            else VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Unsupported prefix net type");
        } else if (V2NDesignScope::RVALUE == c) {
            if (nBitNet) varSel = mNucleusBuilder->cVarselRvalue(nBitNet, indexExpr, sourceLocation); 
            else if (nVectorNet) varSel = mNucleusBuilder->cVarselRvalue(nVectorNet, indexExpr, sourceLocation); 
            else if (nExpr) varSel = mNucleusBuilder->cVarselRvalue(nExpr, indexExpr, sourceLocation); 
            else VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Unsupported prefix net type");
        } else {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Varsel bit VALUE context is not set");
        }
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(varSel, node, "Unable to create varsel bit object");
        // Set evaluated value
        gScope()->sValue(varSel);
    } else if (!isBitSel && isRangeSel) {
        NUBase* varSel = NULL;
        if (V2NDesignScope::LVALUE == c) {
            if (nBitNet) varSel = mNucleusBuilder->cVarselLvalue(nBitNet, dim, sourceLocation); 
            else if (nVectorNet) varSel = mNucleusBuilder->cVarselLvalue(nVectorNet, dim, sourceLocation); 
            else if (nMemoryNet) varSel = concatLvalue;
            else if (nLvalue) varSel = mNucleusBuilder->cVarselLvalue(nLvalue, dim, sourceLocation);
            else VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Unsupported prefix net type");
        } else if (V2NDesignScope::RVALUE == c) {
            if (nBitNet) varSel = mNucleusBuilder->cVarselRvalue(nBitNet, dim, sourceLocation); 
            else if (nVectorNet) varSel = mNucleusBuilder->cVarselRvalue(nVectorNet, dim, sourceLocation); 
            else if (nMemoryNet) varSel = concatRvalue;
            else if (nExpr) varSel = mNucleusBuilder->cVarselRvalue(nExpr, dim, sourceLocation); 
            else VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Unsupported prefix net type");
        } else {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Varsel range VALUE context is not set");
        }
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(varSel, node, "Unable to create varsel range objet");
        // Set evaluated value
        gScope()->sValue(varSel);
    } else if (isBitSel && isRangeSel) {
        NUBase* varSel = NULL;
        if (V2NDesignScope::LVALUE == c) {
            if (nVectorNet) varSel = mNucleusBuilder->cVarselLvalue(nVectorNet, indexExpr, adjust, dim, sourceLocation); 
            else if (nMemoryNet) varSel = concatLvalue;
            else if (nLvalue) varSel = mNucleusBuilder->cVarselLvalue(nLvalue, indexExpr, dim, sourceLocation);
            else VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Unsupported prefix net type");
        } else if (V2NDesignScope::RVALUE == c) {
            if (nVectorNet) varSel = mNucleusBuilder->cVarselRvalue(nVectorNet, indexExpr, adjust, dim, sourceLocation); 
            else if (nMemoryNet) varSel = concatRvalue;
            else if (nExpr) {
              if ( is_from_parameter ){
                SInt32 adjustS = adjust;
                //RJC RC#2014/06/02 (cloutier) the following ranges should probably come from the declared range of the parameter instead of being 31:0
                // TODO use the declared range, not 31:0 here
                indexExpr =  NUNet::sNormalizeExpr(indexExpr, ConstantRange(31,0), ConstantRange(31,0), adjustS,  false);
              }
              varSel = mNucleusBuilder->cVarselRvalue(nExpr, indexExpr, dim, sourceLocation);
            }
            else VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Unsupported prefix net type");
        } else {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Varsel range VALUE context is not set");
        }
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(varSel, node, "Unable to create varsel range objet");
        // Set evaluated value
        gScope()->sValue(varSel);
    } else {
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(indexExpr, node, "Unable to get index expression");
        NUBase* memSel = NULL; 
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(nMemoryNet->is2DAnything(), node, "Incorrect memory net");
        if (V2NDesignScope::LVALUE == c) {
            memSel = mNucleusBuilder->cMemselLvalue(nMemoryNet, indexExpr, sourceLocation); 
        } else if (V2NDesignScope::RVALUE == c) {
            memSel = mNucleusBuilder->cMemselRvalue(nMemoryNet, indexExpr, sourceLocation); 
        } else {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Memsel VALUE context is not set");
        }
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(memSel, node, "Unable to create memSel expression");
        // Set evaluated value
        gScope()->sValue(memSel);
    }
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIINDEXEDID" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriIndexedMemoryId, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIINDEXEDMEMORYID" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Set prefix context
    gScope()->sContext(V2NDesignScope::RVALUE);
    /// Get prefix net
    NUBase* prefixNet = NULL;
    VeriExpression* evalPrefix = evalConstExpr(0, node.GetPrefix());
    if (evalPrefix) {
        evalPrefix->Accept(*this);
        // Constant
        prefixNet = gScope()->gValue();
        delete evalPrefix;
    } else if (node.GetPrefix()) {
        node.GetPrefix()->Accept(*this) ;
        NUIdentRvalue* prefix = dynamic_cast<NUIdentRvalue*>(gScope()->gValue());
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(prefix, node, "Unable to get prefix");
        prefixNet = mNucleusBuilder->gNetFromIdentRvalue(prefix);
        delete prefix;
        gScope()->sValue(NULL); //  we just deleted it, so make sure that it is not used again
    }
    NUMemoryNet* nMemoryNet = dynamic_cast<NUMemoryNet*>(prefixNet);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(nMemoryNet, node, "incorrect mem net object");
    gScope()->sContext(V2NDesignScope::RVALUE);
    VeriExpression* veriRowIndexExpr = static_cast<VeriExpression*>(node.GetIndexes()->GetFirst());
    VeriExpression* veriRowEvalIndex = evalConstExpr(0, veriRowIndexExpr);
    if (veriRowEvalIndex) {
        veriRowEvalIndex->Accept(*this);
        // not in parse tree needs to be deleted manually
        delete veriRowEvalIndex;
    } else {
        veriRowIndexExpr->Accept(*this) ;
    }
    // here rowIndexExpr is the index expression for the first index on the memory (the slowest changing one).
    // For simple memories (1packed,1unpacked dim) this is exactly the rowIndexExpr, for expressions on memories with additional indices we will pack them in below
    NUExpr* rowIndexExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
    mNucleusBuilder->adjustSelExpr(sourceLocation, nMemoryNet, true, &rowIndexExpr);
    NUMemselRvalue* memSelRvalue = 0;
    NUMemselLvalue* memSelLvalue = 0;
    // RJC, the NUBase memSel should probably be named returnSelObject, since it can be a NUVarsel{L,R}value or a NUMemsel{L,R}value
    NUBase* memSel = 0; 
    if (V2NDesignScope::LVALUE == c) {
        memSelLvalue = new NUMemselLvalue(nMemoryNet, rowIndexExpr, sourceLocation); 
        memSel = memSelLvalue;
    } else if (V2NDesignScope::RVALUE == c) {
        memSelRvalue = new NUMemselRvalue(nMemoryNet, rowIndexExpr, sourceLocation); 
        memSel = memSelRvalue;
    } else {
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Memsel context is not set");
    }
    unsigned i ;
    VeriExpression *index ;
    FOREACH_ARRAY_ITEM(node.GetIndexes(), i, index) {
        if (!i) continue; // skip row index we have already processed
        // Here, we need to know what memory range we are grabbing, whether
        // this index refers to a word/bit.  If it refers to a
        // word the index needs to be jammed into the memsel.  If it
        // refers to a bit, we need to generate a varsel.


        // numMemDims - number of declared memory dimensions, both packed and unpacked dimensions number summary
        const UInt32 numMemDims = nMemoryNet->getNumDims();
        // numSelMemDims - number dimensions expressed in the VeriIndexedMemoryID (it can be less or equal to numMemDims)
        const UInt32 numSelMemDims = node.GetIndexes()->Size();
        // numSelDims - the current count of dimensions already in memSel{L,R}Value, (this FOREACH_ARRAY_ITEM loop add a selector in each iteration), at end-of-loop this number should equal to numSelMemDims
        unsigned numSelDims = 0; 
        if (V2NDesignScope::LVALUE == c) {
            numSelDims = memSelLvalue->getNumDims();
        } else if (V2NDesignScope::RVALUE == c) {
            numSelDims = memSelRvalue->getNumDims();
        }
        const bool normalize = (numMemDims == numSelDims + 1);  // if true we are at the last dimension possible for this memory, thus the bit dimension, which is always normalized

        NUExpr* indexExpr = 0;
        UtPair<int,int> dim;
        bool isNonConstRangeSel = false;
        UtPair<int, int> declRange(0, 0);
        if (index->IsRange()) {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE((numSelMemDims == (i+1)), node, "a range expression must be the last selection index expression for this memory"); // consistency check, a range is only allowed as the last index
            // handle variable index range-select
            bool isConstant = index->GetLeft()->IsConstExpr(); // somehow returns true for exprssion like $signed(idx) (although idx isn't constant) bug16744, viper8516
            if (!isConstant) {
                // if not a constant then we need indexExpr
                isNonConstRangeSel = true;
                index->GetLeft()->Accept(*this);
                indexExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
            }
            VeriRange* range = dynamic_cast<VeriRange*>(index);
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(range, node, "Incorrect range type");
            unsigned width = 1;
            NUMemoryNet* nPrefixNet = dynamic_cast<NUMemoryNet*>(prefixNet);
            if (nPrefixNet) {
              width = mNucleusBuilder->gNetLength(nPrefixNet, numMemDims - 1 - i);
              declRange = mNucleusBuilder->gNetDeclRange(nPrefixNet, numMemDims - 1 - i);
            }
            int unusedAdjust;
            NUNet* net = dynamic_cast<NUNet*>(prefixNet);
            UtString netName("");
            if (net) {
                netName = net->getName()->str();
            }
            gRange(&dim, *range, width, (numMemDims != numSelMemDims + 1), netName, &declRange, &unusedAdjust, &sourceLocation);
            if (hasError()) return;
        } else {
            VeriExpression* evalIndex = evalConstExpr(0, index);
            if (evalIndex) {
                evalIndex->Accept(*this);
                // not in parse tree needs to be deleted manually
                delete evalIndex;
            } else {
                index->Accept(*this) ;
            }
            indexExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
            mNucleusBuilder->adjustSelExpr(sourceLocation, nMemoryNet, !normalize, &indexExpr);
        }
        bool isMemoryPartSelect = false;
        NUConcatOp* concatRvalue = NULL;
        NUConcatLvalue* concatLvalue = NULL;
        if ((numSelMemDims == (i+1)) && (numMemDims != (i+1)) && index->IsRange()) {
            // we have memory range slicing case with resulting part also memory,
            // need to generate memory single bit selects with concatenation, for correct index adjustments
            // function deletes indexExpr and memSel at the end
            bool inverse = false;
            if (isNonConstRangeSel) {
                inverse = isInverseNonConstRangeSelect(index->RangeCast(), declRange);
            }
            NUBase* resultConcat = genMemoryPartSelect(&node, index, &indexExpr, dim, memSel, c, inverse, isNonConstRangeSel, sourceLocation);
            memSel = NULL; // as argument is a base class pointer (we can't pass derived** to base**), so doing nullification here
            concatRvalue = dynamic_cast<NUConcatOp*>(resultConcat);
            concatLvalue = dynamic_cast<NUConcatLvalue*>(resultConcat);
            isMemoryPartSelect = true;
        }

        if (V2NDesignScope::LVALUE == c) {
            if (index->IsRange()) {
                if (isMemoryPartSelect) {
                    memSel = concatLvalue;
                } else {
                    NULvalue* lvalueMemSel = dynamic_cast<NULvalue*>(memSel);
                    if (isNonConstRangeSel) {
                        memSel = mNucleusBuilder->cVarselLvalue(lvalueMemSel, indexExpr, dim, sourceLocation);
                    } else {
                        memSel = mNucleusBuilder->cVarselLvalue(lvalueMemSel, dim, sourceLocation);
                    }
                }
            } else {
                if (normalize) {
                    memSel = mNucleusBuilder->genVarselLvalue(nMemoryNet, memSelLvalue, indexExpr, sourceLocation, true);
                } else {
                    memSelLvalue->addIndexExpr( indexExpr );
                    memSel = memSelLvalue;
                }
            }
        } else if (V2NDesignScope::RVALUE == c) {
            if (index->IsRange()) {
                if (isMemoryPartSelect) {
                    memSel = concatRvalue;
                } else {
                    NUExpr* exprMemSel = dynamic_cast<NUExpr*>(memSel);
                    if (isNonConstRangeSel) {
                        memSel = mNucleusBuilder->cVarselRvalue(exprMemSel, indexExpr, dim, sourceLocation);
                    } else {
                        memSel = mNucleusBuilder->cVarselRvalue(exprMemSel, dim, sourceLocation);
                    }
                }
            } else {
                if (normalize) {
                    memSel = mNucleusBuilder->genVarselRvalue(nMemoryNet, memSelRvalue, indexExpr, sourceLocation, true);
                } else {
                    memSelRvalue->addIndexExpr( indexExpr );
                    memSel = memSelRvalue;
                }
            }
        }
    }
    // Set evaluated value
    gScope()->sValue(memSel);
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIINDEXEDMEMORYID" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriModuleInstantiation, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIMODULEINSTANTIATION" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VERI_VISIT_NODE(VeriModuleItem, node) ; // Call of base class
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    // V2NDesignScope::ContextType c = gScope()->gContext();
    // Get parent scope
    NUScope* parent_declaration_scope = getDeclarationScope();
    // We need to apply uniquification here, in order to be able to request uniquifed name during instantiated module resolution 
    if (node.GetInstantiatedModule()) {
        createUniquifiedModuleName(node.GetInstantiatedModule());
    }
    // Resolve master module
    NUModule* masterModule = Verific2NucleusUtilities::gNUModule(&node);


    if (masterModule == NULL ) {
        // Module does not exist due to a population problem, so do not continue with population.
        // An error will have been thrown by the population problem, so do not need another error.
        return;
    }
    unsigned i ;
    VeriInstId *inst ;
    Array *ve_instanceArray = node.GetInstances();
    bool isArrayOfInstances = ( ve_instanceArray->Size() != 1 ); // it is not clear this is the correct way to determine if this is an array of instances
    FOREACH_ARRAY_ITEM(ve_instanceArray, i, inst) {
        // Creating instance and adding to module
        if (node.GetInstantiatedModule()) {
            createOneModuleInstance(inst, node.GetInstantiatedModule(), parent_declaration_scope, masterModule, (isArrayOfInstances? &i: NULL), sourceLocation);
        } else {
            createOneModuleInstanceVhdl(node, inst, parent_declaration_scope, masterModule, (isArrayOfInstances? &i: NULL), sourceLocation);
        }
    }
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIMODULEINSTANTIATION" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriConcat, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERICONCAT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    unsigned i ;
    VeriExpression *expr = NULL;
    NULvalueVector lValueVector;
    NUExprVector exprVector;
    FOREACH_ARRAY_ITEM(node.GetExpressions(), i, expr) {
        if (!expr) continue ;
        // set expression context;
        gScope()->sContext(c);
        // each item in concat is self determined
        int context_size_sign = expr->StaticSizeSign(0);
        //Below adjustment makes problems with test/sv/Verific/Positive/5/carbon_5.2_test36_test testcase
        //and a lot of other cases do match simulation with MTI without fix correction
        //For more details on below adjustment see comment for VeriAssignmentPattern VeriExpression.h:4335
        //TODO check it (if not needed just remove)
        //if (node.IsAssignPattern()) {
        //    // each item in SV assignment pattern concat should take context size/sign if it's parent
        //    context_size_sign = gScope()->getContextSizeSign();
        //}
        int savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign); // save starting value
        VeriExpression* evalExpr = evalConstExpr(context_size_sign, expr);
        if (evalExpr) {
            evalExpr->Accept(*this);
            delete evalExpr;
        } else {
            expr->Accept(*this) ;
        }
        NUBase* iConcatExpr = gScope()->gValue();
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(iConcatExpr, node, "Unable to get concat expression");
        if (V2NDesignScope::LVALUE == c) {
            mNucleusBuilder->aLvalueToLvalueVector(dynamic_cast<NULvalue*>(iConcatExpr), lValueVector);
        } else if (V2NDesignScope::RVALUE == c) {
            mNucleusBuilder->aExprToExprVector(dynamic_cast<NUExpr*>(iConcatExpr), exprVector);
        } else {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Value context is not set for concat operation");
        }
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(iConcatExpr, node, "Unable to create concat expression");
        (void) gScope()->updateContextSizeSign(savedContextSizeSign);    //reset the context size sign back to value it had upon entry to this item in concat
    }
    NUBase* concatOp = NULL; 
    if (V2NDesignScope::LVALUE == c) {
        concatOp = mNucleusBuilder->cConcatLvalue(lValueVector, sourceLocation);
    } else if (V2NDesignScope::RVALUE == c) {
        concatOp = mNucleusBuilder->cConcatRvalue(exprVector, 1, sourceLocation);
    }
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(concatOp, node, "Unable to create concat operation");
    // Set evaluated value
    gScope()->sValue(concatOp);
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERICONCAT" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriCaseStatement, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERICASESTATEMENT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Set condition context 
    gScope()->sContext(V2NDesignScope::RVALUE);
    /*
      Some notes for future support of VERI_INSIDE
      For ranged case item conditions like [1:2], [3:5] etc.- Interra VHDL flow suggests to generate separate conditions for each ranged item,
      which is equivalent for having 1,2 and 3,4,5 correspondingly. So fastest is just address this. 
      It seems like with this handling we will mostly cover 'inside' operator behavioral, but I don't think this solution will cover all cases.
      Below are snippet from LRM (1800-2012 SV LRM, page 274) and notes/concerns.
       
      "The keyword inside can be used after the parenthesized expression to indicate a set membership (see
      11.4.13). In a case-inside statement, the case_expression shall be compared with each case_item_expression
      (open_range_list) using the set membership inside operator."

      I think we mostly cover this part if we apply Interra VHDL flow suggested approach.
       
      "The inside operator uses asymmetric wildcard matching (see 11.4.6). Accordingly, the case_expression shall be the left operand, and each
      case_item_expression shall be the right operand."

      Wildcard matching needs to be investigated more deeply, I'm not sure if with solution 1 we address all cases of wildcard matching.
      This might require nucleus db change, some flag population (inside ?) in order correctly do the matching.
       
      "The case_expression and each case_item_expression in braces shall be evaluated in the order 
      specified by a normal case, unique-case, or priority-case statement."

      I think this ordering is a new ground for us.(customer testcase doesn't have this).
       
      "A case_item shall be matched when the inside operation compares the case_expression to the
      case_item_expressions and returns 1'b1 and no match when the operation returns 1'b0 or 1'bx. If all
      comparisons do not match and the default item is given, the default item statement shall be executed."

      I'm warned about this comparison, not sure if this matches with verilog-2001 comparison which nucleus does.
       
      Another note is  I have found some interesting API in NUCaseCondition consturctor... last argument with default value rangeCond=false.
      But I wasn't able to find users of this API (which use rangeCond=true). So I'm not sure how to use it.
    */
    // We don't support set membership case statement (inside operator).

    //Leaving above comment for future reference, untill support is complete
    //TODO add support for priority, unique, unique0 cases
    //This might require also changes in design.exe generator in order correctly report runtime warnings required by 1800-2012 standard
    if (node.GetQualifier(VERI_UNIQUE) || node.GetQualifier(VERI_UNIQUE0) || node.GetQualifier(VERI_PRIORITY)) {
        reportUnsupportedLanguageConstruct(sourceLocation, "case qualifiers (unique, unique0, priority) not supported");
        return;
    }
    if (node.GetCaseStyle() == VERI_RANDCASE) {
        UNSUPPORTED_SV_DECLARATION(node, VERI_RANDCASE); // this is an error
    }
    
    // According to verilog rules all case expressions bitness should be adjusted to the value of maximum expression bitness, so we need to compute this value and pass it all caseItems
    // Using scope sbitenss/gBitness field to pass the value maxExprSize to CaseItem (in order not to create new field globally or in scope)
    unsigned maxExprSize = gMaxCaseExprSize(node);
    gScope()->sBitness(maxExprSize);
    //unsigned maxExprSize = 32; 
    // Get condition (selector) expression
    VeriExpression* evalRvalue = evalConstExpr(0, node.GetCondition());
    if (evalRvalue) {
        evalRvalue->Accept(*this);
        delete evalRvalue;
    }
    else if (node.GetCondition()) node.GetCondition()->Accept(*this) ;
    NUExpr* condExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(condExpr, node, "Unable to get condition expression");
    // Create Case statement
    NUCase* caseStmt = mNucleusBuilder->cCase(condExpr, node.GetCaseStyle(), maxExprSize, sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(caseStmt, node, "Unable to create case statement");
    NUCaseItem* defaultItem = NULL;
    // Get case items
    if (node.GetCaseItems()) {
        unsigned i ;
        VeriCaseItem *ci ;
        FOREACH_ARRAY_ITEM(node.GetCaseItems(), i, ci) {
            // Get caseItem and add to case
            if (VERI_CASE == node.GetCaseStyle()) {
                gScope()->sCaseType((VERI_INSIDE == node.GetCaseType()) ? V2NDesignScope::CASE_INSIDE : V2NDesignScope::CASE);
            } else if (VERI_CASEX == node.GetCaseStyle()) { 
                gScope()->sCaseType((VERI_INSIDE == node.GetCaseType()) ? V2NDesignScope::CASE_X_INSIDE : V2NDesignScope::CASE_X);
            } else if (VERI_CASEZ == node.GetCaseStyle()) {
                gScope()->sCaseType((VERI_INSIDE == node.GetCaseType()) ? V2NDesignScope::CASE_Z_INSIDE : V2NDesignScope::CASE_Z);
            } else {
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Incorrect case type");
            }
            ci->Accept(*this);
            NUCaseItem* caseItem = dynamic_cast<NUCaseItem*>(gScope()->gValue());
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(caseItem, node, "Unable to get case Item");
            // Store the default, so that we can put it at the end of the list
            if (caseItem->isDefault()) {
                defaultItem = caseItem;
            } else {
                mNucleusBuilder->aCaseItemToCase(caseItem, caseStmt);
            }
        }
        // Now we can add the default item
        if (defaultItem != NULL) {
            mNucleusBuilder->aCaseItemToCase(defaultItem, caseStmt);
        }
        // removed redundant cases and put isFullySpecified, isUserFullCase flags
        mNucleusBuilder->computeFullCase(&caseStmt, node.IsFullCase());
    }
    // Set evaluated value
    gScope()->sValue(caseStmt);
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERICASESTATEMENT" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriCaseItem, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERICASEITEM" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Get case type
    V2NDesignScope::CaseType caseType = gScope()->gCaseType();
    // Create Case Item
    NUCaseItem* caseItem = mNucleusBuilder->cCaseItem(sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(caseItem, node, "Unable to create case item");
    // Get max case condition element size
    //unsigned maxExprSize = gMaxCaseItemExprSize(node);
    // Getting maxExprSize, computed in caseStatement
    unsigned maxExprSize = gScope()->gBitness();
    // Get conditions and add to caseItem
    if (node.GetConditions()) {
        unsigned i ;
        VeriExpression *condition = NULL;
        FOREACH_ARRAY_ITEM(node.GetConditions(), i, condition) {
            SourceLocator conditionLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,condition->Linefile());
            // Set condition context 
            gScope()->sContext(V2NDesignScope::RVALUE);
            // Get condition expression
            VeriExpression* evalRvalue = evalConstExpr(0, condition);
            UtString* condValue = 0;
            if (condition->HasXZ() && ((V2NDesignScope::CASE_INSIDE == caseType) || (V2NDesignScope::CASE_X_INSIDE == caseType) 
                || (V2NDesignScope::CASE_Z_INSIDE == caseType))) {
                //TODO assymetric wildcard matching (see section 11.4.6 LRM 1800-2012) needs to be done (which we don't support yet)
                reportUnsupportedLanguageConstruct(sourceLocation, "xz values with 'case inside' not supported");
                return;
            }
            UtPair<int,int> dim;
            if (evalRvalue) {
                evalRvalue->Accept(*this);
                // exclude non-xz cases from mask computation
                if ((V2NDesignScope::CASE != caseType) && (V2NDesignScope::CASE_INSIDE != caseType)) {
                    VeriConstVal* constRvalue = dynamic_cast<VeriConstVal*>(evalRvalue);
                    if (constRvalue) {
                        // Go MSB to LSB. MSB is highest index. 0 the lowest.
                        unsigned i = constRvalue->Size(NULL) ;
                        // Mask: 0 - x,z,?; 1 - 0,1
                        condValue  = new UtString("");
                        while (i-- != 0) {
                            // Determine the value of this bit
                            if (GET_BIT(constRvalue->GetXValue(),i))         { *condValue << 'x' ;}
                            else if (GET_BIT(constRvalue->GetZValue(),i))    { *condValue << 'z' ;}
                            else if (GET_BIT(constRvalue->GetValue(),i))     { *condValue << '1' ;}
                            else                                     { *condValue << '0' ;}
                        }
                    }
                }
                delete evalRvalue;
            } else if (condition) {
                if (condition->IsRange()) {
                    // only constant range of integers allowed here
                    VeriRange* range = condition->RangeCast();
                    int unusedAdjust = 0;
                    gRange(&dim, *range, range->RangeWidth(), false, "", 0, &unusedAdjust, &conditionLocation);
                    if (hasError()) return;
                } else {
                    condition->Accept(*this);
                }
            }
            if (condition && condition->IsRange()) {
                // Case item conditions like [1:2], [3:5] will be populated as separate conditions for each ranged item,
                // which is equivalent for having 1,2 and 3,4,5 correspondingly in a single case item condition.
                // TODO: consider using localflow/VhPopulateStmt.xx:1342 approach (rangCond=true NUCaseCondition api) for optimization (less objects)
                genCaseConditions(&node, caseType, maxExprSize, dim, caseItem, conditionLocation);
            } else {
                NUExpr* condExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(condExpr, node, "Unable to create condition expression");
                addCaseCondition(&node, caseType, maxExprSize, condExpr, condValue, caseItem, conditionLocation);
            }
        }
    } else {
        // default case, no condition
    }
    // Get stmts and add to caseItem
    // Reseting value in order not to get old value
    gScope()->sValue(0);
    if (node.GetStmt()) node.GetStmt()->Accept(*this) ;
    NUStmt* stmt = dynamic_cast<NUStmt*>(gScope()->gValue());
    //VERIFIC_CONSISTENCY_NO_RETURN_VALUE(stmt, node, "Unable to get statement");
    //UtPair<NUBase*, unsigned> declarationScopePair = mNucleusBuilder->gParentDeclUnit(gScope()->gOwner());
    //NUStmtList* stmts = mNucleusBuilder->cStmtList();
    //if (! isNamedBlock(node.GetStmt())) {
    //    mNucleusBuilder->gStmtsFromBlock(declarationScopePair.frist, stmts, stmt);
    //    mNucleusBuilder->aStmtListToCaseItem(stmts, caseItem);
    //    delete stmts;
    //} else mNucleusBuilder->aStmtToCaseItem(stmt, caseItem);
    // There can be case Items with empty statement list
    if (stmt) mNucleusBuilder->aStmtToCaseItem(stmt, caseItem);
    // Set evaluated value
    gScope()->sValue(caseItem);
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERICASEITEM" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriParamId, node)
{
  (void) node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIPARAMID" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // parameters handled by evalExpr function
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIPARAMID" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriTaskDecl, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERITASKDECL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VERI_VISIT_NODE(VeriModuleItem, node) ; // Call of base class
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    // Skip if we already resolved the function
    if (mTFRegistry.find(node.GetTaskId()) != mTFRegistry.end()) {
        gScope()->sValue(NULL);
        return;
    }
    // Create function decl
    NUModule* module = mNucleusBuilder->gModule(getNUScope(&node));
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, node, "Unable to get parent module");
    UtString taskName(gVisibleName(node.GetTaskId(), createNucleusString(node.GetTaskId()->GetName())));
    NUTask* tf = mNucleusBuilder->cTF(taskName, taskName, module, sourceLocation);
    tf->putRequestInline(doInlineTasks());
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(tf, node, "Unable to create task declaration");
    mTFRegistry[node.GetTaskId()] = tf;
    //mDeclarationRegistry[node.GetTaskId()] = tf;
    V2NDesignScope* current_scope = new V2NDesignScope(tf, gScope());
    // We have a choice which one to put under temp, so prefereing task specific one, in order not to polute global stuff
    PushScope(current_scope, taskName);
    // Get ansi port declarations
    // default to input - LRM 1800-2005 
    mPortDirectionToUseIfUndefined = VERI_INPUT;
    unsigned i ;
    VeriAnsiPortDecl * ansi_decl_item = 0 ;
    FOREACH_ARRAY_ITEM(node.GetAnsiIOList(), i, ansi_decl_item) {
        if (ansi_decl_item) ansi_decl_item->Accept(*this) ;
    }
    // get other declarations (ports and local nets) 
    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(node.GetDeclarations(), i, decl_item) {
        decl_item->Accept(*this) ;
    }

    // Event controls are not legal within tasks. We need to set it 
    // false here in this case: always task_enable; 
    // NOTE: You can't have event controls in functions, so no need to check there.
    sEventControlLegal(false);
    // get statements
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, stmt) {
        if (stmt) stmt->Accept(*this) ;
        NUStmt* iStmt = dynamic_cast<NUStmt*>(gScope()->gValue());
        // Don't assert existence, we might have disabled or ignorable statement
        if (iStmt) mNucleusBuilder->aStmtToTF(iStmt, tf);
    }
    PopScope();
    mNucleusBuilder->aTFToModule(tf, module);
    // Set evaluated value
    gScope()->sValue(NULL);
    // Restore current node context 
    gScope()->sContext(c);

#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERITASKDECL" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriTaskEnable, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERITASKENABLE" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();
    NUTaskEnable * taskEnable = 0;
    if (node.GetTaskName()->IsHierName()) {
        // Call VeriSelectedName
        node.GetTaskName()->Accept(*this);
        // Get Task enable hierarchical reference
        taskEnable = dynamic_cast<NUTaskEnable*>(gScope()->gValue());
        if (hasError()) return;
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(taskEnable, node, "Unable to get task enable hierarchical reference");
    }

    // Getting Task nucleus object from temporaries 
    NUBase* tf = NULL;
    if (mTFRegistry.find(node.GetTaskName()->GetId()) != mTFRegistry.end()) {
        tf = mTFRegistry[node.GetTaskName()->GetId()];
    } else {
        // If task declaration not found in TF registery, get task decl through parse-tree backpointer
        VeriModuleItem* mi = node.GetTaskName()->GetId()->GetModuleItem();
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(mi, node, "Unable to get module item");
        mi->Accept(*this);
        if (mTFRegistry.find(node.GetTaskName()->GetId()) != mTFRegistry.end()) {
            tf = mTFRegistry[node.GetTaskName()->GetId()];
        }
    }
    NUTask* task = dynamic_cast<NUTask*>(tf);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(task, node, "Unable to find task declaration");
    if (!node.GetTaskName()->IsHierName()) {
        // Getting parent module from current scope
        NUModule* module = mNucleusBuilder->gModule(getNUScope(&node));
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, node, "Unable to get parent module");
        // Creating task enable, task enable creation requires parent module (not the parent scope)
        taskEnable = mNucleusBuilder->createTaskEnable(task, module, sourceLocation);
    }
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(taskEnable, node, "Unabel to create task enable");
    // Creating TFArgConnections vector
    NUTFArgConnectionVector* argConnections = new NUTFArgConnectionVector;
    // Creating ArgConnections and adding to vector
    if (node.GetArgs()) {
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(node.GetArgs(), i, expr) {
            if (!expr) continue ;
            // This handles only connection by index
            unsigned portDir = mNucleusBuilder->gPortDirection(task, i, sourceLocation); 
            if ((portDir == VERI_OUTPUT) || (portDir == VERI_INOUT)) {
                gScope()->sContext(V2NDesignScope::LVALUE);
            } else {
                gScope()->sContext(V2NDesignScope::RVALUE);
            }
            VeriExpression* evalRvalue = evalConstExpr(0, expr);
            if (evalRvalue) {
                evalRvalue->Accept(*this);
                delete evalRvalue;
            } else {
                expr->Accept(*this) ;
            }
            NUBase* pActual = gScope()->gValue();
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(pActual, node, "Unable to get port actual");
            NUTFArgConnection* pArgConnection = mNucleusBuilder->cTFArgConnection(pActual, i, task, sourceLocation);
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(pArgConnection, node, "Unable to create port connection");
            mNucleusBuilder->aTFArgConnectionToVector(pArgConnection, argConnections);
        }
    }
    // Adding ArgConnections to TaskEnable
    mNucleusBuilder->aTFArgConnectionsToTaskEnable(argConnections, taskEnable);
    delete argConnections;
    // Set evaluated value 
    gScope()->sValue(taskEnable);
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERITASKENABLE" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriSystemTaskEnable, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERISYSTEMTASKENABLE" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Get sys function name
    UtString name;
    UtString vName = gVisibleName(&node, node.GetName());
    name << "$" << (vName.c_str()); // add $ to name to match existing messages
    // get module scope from current scope
    NUModule* module = mNucleusBuilder->gModule(getNUScope(&node));
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, node, "Unable to get parent module");
    if (isDisabledSysTask(node.GetFunctionType())) {
        // we should only specify -enableOutputSysTasks for tasks: $display $fdisplay $write $fwrite $fclose $fflush, which are disabled by default
        // first get default, then correct for any module specific directives
        if (!doingOutputSysTasks(module)) {
            //Report disabled
            getMessageContext()->DisabledSysTask(&sourceLocation, name.c_str(), "-enableOutputSysTasks" );
            gScope()->sValue(NULL);
            return;
        } else {
            if (isOutputSysTask(node.GetFunctionType())) {
                // handles: $display, $fdispaly, $write, $fwrite
                cOutputSysTask(&node, module, name, sourceLocation);
            } else {
                // handles: $fflush, $fclose
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(isFFlushOrFCloseSysTask(node.GetFunctionType()), node, "incorrect system task");
                cFFlushOrFCloseSysTask(&node, module, name, sourceLocation);
            }
        }
    } else if (isControlSysTask(node.GetFunctionType())) {
        // handles: $stop, $finish
        cControlSysTask(&node, module, name, sourceLocation);
    } else if (isReadmemX(node.GetFunctionType())) {
        // handles: $readmemb, $readmemh 
        cReadmemXSysTask(&node, module, name, sourceLocation);
    } else if (mNucleusBuilder->gIODB()->isCModel(name.c_str())) {
        //report error
        reportUnsupportedLanguageConstruct(sourceLocation, "User defined task/cModel");
    } else {
        // it must be unsupported sys task encountered
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(!isSupportedSysTask(node.GetFunctionType()), node, "incorrect system task");
        if (mWarnForSysTask) { 
            // in case -warnForSysTask flag is passed (natively supported by Interra) we need to generate alert to match up with Interra parse message
            // Note: Verific is silent about unknown sys tasks/functions and there is no option to change this behavioral, so reporting alert here 
            getMessageContext()->UndefinedSysTaskFunc(&sourceLocation, name.c_str());
        } else {
            // otherwise report warning
            getMessageContext()->UnsupportedSysTask(&sourceLocation, name.c_str());
        }
        gScope()->sValue(NULL);
    }
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERISYSTEMTASKENABLE" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriMultiConcat, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIMULTICONCAT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    bool ignoreZeroRepeatCountItems = (0 != Verific::RuntimeFlags::GetVar("veri_carbon_ignore_concat_items_with_0_repeat_count"));


    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();

    //gScope()->sContext(V2NDesignScope::RVALUE);
    //node.GetRepeat()->Accept(*this) ;
    //NUBase* repeat_count = gScope()->gValue();
    VeriExpression* constVal = node.GetRepeat();
    // IsConstExpr() will evaluate to true for any constant expression, even if it is not yet reduced to a constant. isConstant() does not do this for expressions of constants.
    if ( !constVal->IsConstExpr() ){
      reportUnsupportedLanguageConstruct(sourceLocation, "Non-constant repeat expression");
      return;
    }
    VeriExpression* evalConstVal = evalConstExpr(0, constVal);
    if ( NULL == evalConstVal ){
      reportUnsupportedLanguageConstruct(sourceLocation, "Non-constant repeat expression");
      return;
    }
    VeriConst* evaluatedConstVal = dynamic_cast<VeriConst*>(evalConstVal);
    if ( NULL == evaluatedConstVal ){
      reportUnsupportedLanguageConstruct(sourceLocation, "Non-constant repeat expression");
      return;
    }
    // RJC RC#2013/10/23 (cloutier) there was once here a static cast of evalConstVal to a VeriIntVal which was incorrect.  However the
    //following call to evaluatedConstVal->Integer() is also incorrect if the constant cannot be represented in 31 bits or if there are x or z
    //involved.  This needs to be cleaned up.  see gRange() and rVal->Get64bitInteger();
//    bool supportedConstant = isVeriConstantRepresentable(evaluatedConstVal, (sizeof(int) * CHAR_BIT));
//    if ( !supportedConstant ){
//      // print error saying that constant cannot be represented in the number of bits
//    }
    NUBase* concatOp = NULL;
    int repeatCount = evaluatedConstVal->Integer();
    if (repeatCount > 0) {
        unsigned i ;
        VeriExpression *expr = NULL;
        NULvalueVector lValueVector;
        NUExprVector exprVector;
        FOREACH_ARRAY_ITEM(node.GetExpressions(), i, expr) {
            if (!expr) continue ;
            // set expression context;
            // RJC RC#15 it seems like the following call to sContext(c) sets the context of the terms of the inner concat to be the same as the context in effect outside of the repeating concat.
            //  if we were processing "if (sel) o = {3{a,b,c}};" then when this method was called the context was the then_branch_of_the_if, and
            //  when we get to this line of code the first time we should be processing 'a'.  It seems to me that the context of a is not
            //  the then_branch_of_the_if but instead it should be the first term of a concatenation.
            // ADDRESSED FD: Sorry, I think might miss something from your statemant. But hear we are taking into account the context (from parent structure) 
            // in which this expression occurs (it's not parents context but the context the parent expression has set for this particular expression)
            // if it is in righthand site (continues assign, blocking assign, non-blocking assign, binary expression, unary expression etc.)
            // then it's rvalue else if it's in left side (continues assign, blocking assign, non-blocking assign, or another concat/multiconcat)
            // then it's lvalue. So I think hear it's absolutely necessary to set context which parent structure fixed to set for it's child.
            gScope()->sContext(c);
            // each item in multi-concat is self determined
            int context_size_sign = expr->StaticSizeSign(0);
            //Below adjustment makes problems with test/sv/Verific/Positive/5/carbon_5.2_test36_test testcase
            //and a lot of other cases do match simulation with MTI without fix correction
            //For more details on below adjustment see comment for VeriMultiAssignmentPattern VeriExpression.h:4437
            //TODO check it (if not needed just remove)
            //if (node.IsMultiAssignPattern()) {
            //    // each item in SV multi-assignment pattern concat should take context size/sign if it's parent
            //    context_size_sign = gScope()->getContextSizeSign();
            //}
            int savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign); // save starting value
            VeriExpression* evalExpr = evalConstExpr(context_size_sign, expr);
            if (evalExpr) {
                evalExpr->Accept(*this);
                delete evalExpr;
            } else {
                expr->Accept(*this) ;
            }
            NUBase* iConcatExpr = gScope()->gValue();
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(iConcatExpr, node, "Unable to get concat expression");
            if (V2NDesignScope::LVALUE == c) {
                mNucleusBuilder->aLvalueToLvalueVector(dynamic_cast<NULvalue*>(iConcatExpr), lValueVector);
            } else if (V2NDesignScope::RVALUE == c) {
                mNucleusBuilder->aExprToExprVector(dynamic_cast<NUExpr*>(iConcatExpr), exprVector);
            } else {
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Value context is not set for concat operation");
            }
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(iConcatExpr, node, "Unable to create concat expression");
            (void) gScope()->updateContextSizeSign(savedContextSizeSign);    //reset the context size sign back to value it had before processing this concat element
        }
        if (V2NDesignScope::LVALUE == c) {
            if ( lValueVector.size() ){
              concatOp = mNucleusBuilder->cConcatLvalue(lValueVector, sourceLocation);
            } else {
              getMessageContext()->InvalidEmptyConcat(&sourceLocation);
              // leave concatOp unset
            }
        } else if (V2NDesignScope::RVALUE == c) {
            if ( exprVector.size() ){
              concatOp = mNucleusBuilder->cConcatRvalue(exprVector, (unsigned)repeatCount, sourceLocation);
            } else {
              getMessageContext()->InvalidEmptyConcat(&sourceLocation);
              // leave concatOp unset
            }
        }
    } else {
        // Here the repeat count is 0 or a negative value.  Some versions of Verilog allow a count of 0 in some situations.
        // No version of Verilog or SystemVerilog allow a negative count.

        // in SystemVerilog it is valid to have a multiConcat reduce to a null expression if the multiConcat is an operand of a parent concat,
        // and there is at least one other non-null term of that parent concat.
        // It appears that this visitor method is only called in the case that the VeriMultiConcat is outside of a surrounding concatenation
        // expression, for example it is called in this case: out1 = {foo{a[0]}}.
        // VeriMultiConcat nodes that are within a concat are handled during parsing or static elaboration to make sure they have a constant and
        // non-negative repetition count.  If the repetition constant is 0 then the VeriMultiConcat would be replaced by a null expression and
        // thus there will be no VeriMultiConcat that the current visitor would be called for.
      
        if ( ( 0 == repeatCount ) && ignoreZeroRepeatCountItems ) {
          // in this case, the whole of the VeriMultiConcat is null, which means that there was something like {0{a}}
          // there is no expression to create, thus we have an error 
          getMessageContext()->InvalidEmptyConcat(&sourceLocation);
          // leave concatOp unset
        } else {
          // Here we are compatible with the interra flow and we return 1'b0.  The use of this return value has also been observed in some simulators.
          concatOp = mNucleusBuilder->cConst(0, 1, false, sourceLocation);
        }
    }
    delete evalConstVal;
    // Set evaluated value
    gScope()->sValue(concatOp);
    // Restore current node context 
    gScope()->sContext(c);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIMULTICONCAT" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriFor, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIFOR" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    // Save current node context (TODO: Probably not necessary if we're pushing scope)
    V2NDesignScope::ContextType c = gScope()->gContext();

    // Determine whether this for loop has declarations, e.g. for (int i = 0; i < max; i++)
    bool hasDecls = Verific2NucleusUtilities::forLoopHasDecls(node);
    
    // If there are declarations, create a begin/end and a named declaration scope
    NUBlock* enclosing_block = NULL;
    if (hasDecls) {
      // Create sequential block
      enclosing_block = mNucleusBuilder->cBlock(getNUScope(&node), sourceLocation);
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(enclosing_block, node, "Unable to create block");
      // Create scope
      V2NDesignScope* current_scope = new V2NDesignScope(enclosing_block, gScope());
      // Get parent declaration scope (if not defined it's module/task/function scope)
      NUScope* parent_declaration_scope = getDeclarationScope();
      // Get Block name
      UtString blockName;
      if (node.GetOpeningLabel()) {
        blockName = gVisibleName(node.GetOpeningLabel(), createNucleusString(node.GetOpeningLabel()->GetName()));
      }
      else {
        StringAtom* blockNameId = mNucleusBuilder->gNextUniqueName(getNUScope(&node), "for_loop");
        blockName = UtString(blockNameId->str());
        mNameRegistry[&node] = blockNameId; 
      }
      PushScope(current_scope, blockName);
      NUNamedDeclarationScope* namedDeclarationScope = 
          mNucleusBuilder->cNamedDeclarationScope(blockName, parent_declaration_scope, sourceLocation);
      gScope()->sDeclarationScope(namedDeclarationScope);
    }
    
    // Set FOR_SCHEME context - tells VeriDataDecl to go ahead and create the blocking
    // assign for the initial value, but:
    //       (1) avoid putting blocking assign in the initial block; instead
    //       (2) return the blocking assign(s) so that they are retrievable by gValue
    // TODO: We might want to change the context to something like
    // AUTO_SCOPE to distinguish static vs. automatic variables. We'll need
    // this to handle other contexts in which variables are automatic scope.
    gScope()->sContext(V2NDesignScope::FOR_SCHEME);
    
    // Accumulate initial values in this list of blocking assignments
    NUStmtList * initStmtList = mNucleusBuilder->cStmtList();
    // Get initials
    unsigned i;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(node.GetInitials(), i, item) {
        // Null initializers (for (; i < 3; i++) will result in NULL item.
        if (!item) continue;
        item->Accept(*this);
        // SystemVerilog for loops can have declarations in the initials
        if (item->IsDataDecl()) {
          // Retrieve blocking assigns constructed by VeriDataDecl visitor
          UtVector<NUBase*> blockingAssigns;
          gScope()->gValueVector(blockingAssigns);
          UtVector<NUBase*>::iterator j;
          for (j = blockingAssigns.begin(); j != blockingAssigns.end(); ++j) {
            NUStmt* initStmt = dynamic_cast<NUStmt*>(*j);
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(initStmt, node, "Unable to retrieve blocking assign from for loop initializer declaration");
            mNucleusBuilder->aStmtToStmtList(initStmt, initStmtList);
          }
        }
        // Verilog 2001 loops can only have blocking assigns
        else {
          NUStmt* initStmt = dynamic_cast<NUStmt*>(gScope()->gValue());
          VERIFIC_CONSISTENCY_NO_RETURN_VALUE(initStmt, node, "Unable to retrieve blocking assign from for loop initializer statement");
          mNucleusBuilder->aStmtToStmtList(initStmt, initStmtList);
        }
    }

    // Restore current node context 
    gScope()->sContext(c);

    // Create a temporary block (which we use to accumulate residual task enables
    // generated by function calls in the conditional and repetition stmts).
    // See comments for 'VeriFunctionCall'.
    NUBlock* block = createBlockAndPushScope(&node, sourceLocation, "block");
    
    // Get condition, save task enables (if any) for later processing
    gScope()->sContext(V2NDesignScope::RVALUE);
    if (node.GetCondition()) node.GetCondition()->Accept(*this) ;
    NUExpr* condition = dynamic_cast<NUExpr*>(gScope()->gValue());
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(condition, node, "Unable to get condition");
    NUStmtList* extraConditionStmts = mNucleusBuilder->cStmtList();
    mNucleusBuilder->gStmtsFromBlock(extraConditionStmts, block, false /* add to back of list */);
    
    // Get repetitions
    NUStmtList * repeatStmtList = mNucleusBuilder->cStmtList();
    FOREACH_ARRAY_ITEM(node.GetRepetitions(), i, item) {
        item->Accept(*this);
        NUStmt* repeatStmt = dynamic_cast<NUStmt*>(gScope()->gValue());
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(repeatStmt, node, "Unable to get repeat statement");
        // Get residual statements (e.g. a function call) and add to repeat stmt list
        mNucleusBuilder->gStmtsFromBlock(repeatStmtList, block, false /* add to back of list */);
        mNucleusBuilder->aStmtToStmtList(repeatStmt, repeatStmtList);
    }

    // We're done capturing function call residuals. 
    deleteBlockAndPopScope(block);
    block = NULL;

    // Create a begin/end around the for body, if there
    // isn't already one there. This begin/end is
    // necessary to capture task enables that are generated
    // by function calls. See comments for VeriFunctionCall
    if (node.GetStmt() && !node.GetStmt()->IsSeqBlock()) {
      block = createBlockAndPushScope(node.GetStmt(), sourceLocation, "for");
    }

    // Get body
    NUStmtList * bodyStmtList = mNucleusBuilder->cStmtList();
    if (node.GetStmt()) node.GetStmt()->Accept(*this);
    NUStmt* forLoopBody = dynamic_cast<NUStmt*>(gScope()->gValue()); 
    // for loop body can be empty (e.g. disabled systask case, Verific has parse-tree node but we should ignore it
    //VERIFIC_CONSISTENCY_NO_RETURN_VALUE(forLoopBody, node, "Unable to get body");
    // If a block was created, then add the statement to the block,
    // and pop scope.
    NUStmt* bodyStmt = forLoopBody;
    if (block) {
      if (bodyStmt) mNucleusBuilder->aStmtToBlock(forLoopBody, block);
      PopScope();
      bodyStmt = block;
    }
    if (bodyStmt) mNucleusBuilder->aStmtToStmtList(bodyStmt, bodyStmtList);

    // before we create the NUFor, if there are any extraConditionStmts then
    // make a separate copy of them, and put one copy at the end of the
    // initial_stmts and the other copy at the end of the advance_stmts lists.
    // This allows us to handle function calls within the condition expression properly.
    if (not extraConditionStmts->empty()) {
      NUStmtList copyExtraConditionStmts;
      NUScope* parent_declaration_scope = getDeclarationScope();
      CopyContext copy_context(parent_declaration_scope, NULL) ;
      deepCopyStmtList(*extraConditionStmts, &copyExtraConditionStmts, copy_context, NULL);
      initStmtList->insert(initStmtList->end(),extraConditionStmts->begin(),extraConditionStmts->end());
      repeatStmtList->insert(repeatStmtList->end(),copyExtraConditionStmts.begin(),copyExtraConditionStmts.end());
    }

    //Create for
    NUStmt* forLoop = mNucleusBuilder->cFor(*initStmtList, condition, *repeatStmtList, *bodyStmtList, sourceLocation);
    delete initStmtList;
    delete repeatStmtList;
    delete bodyStmtList;
    delete extraConditionStmts;
    
    // Return the forLoop, unless we created an enclosing scope,
    // in which case return the enclosing begin/end block
    NUBase* stmt = forLoop;
    // If we created a named declaration scope, pop it, return enclosing scope
    if (hasDecls) {
      PopScope();
      mNucleusBuilder->aStmtToBlock(forLoop, enclosing_block);
      stmt = enclosing_block;
    }
    
    //Return for loop or enclosing begin/end
    gScope()->sValue(stmt);

#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIFOR" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriForever, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIFOREVER" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    getMessageContext()->UnsupportedStatement(&sourceLocation, VeriNode::PrintToken(VERI_FOREVER));
    setError(true);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIFOREVER" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriRepeat, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIREPEAT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Get condition expression
    gScope()->sContext(V2NDesignScope::RVALUE);
    VeriExpression* evalRvalue = evalConstExpr(0, node.GetCondition());
    if (evalRvalue) {
        evalRvalue->Accept(*this);
        delete evalRvalue;
    } else if (node.GetCondition()) node.GetCondition()->Accept(*this) ;
    NUExpr* condition = dynamic_cast<NUExpr*>(gScope()->gValue());
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(condition, node, "Unable to get condition");
    // Create temp net for keeping repeat count
    UtString net_name(mNucleusBuilder->gNextUniqueName(getNUScope(&node), "repeat_net")->str());

    //NUBase* counterTemp = mNucleusBuilder->cTempNet(gScope()->gOwner(), net_name, sourceLocation);

    UtPair<int, int> dim(31, 0);
    NUScope* scope = getNUScope(&node);
    NUNet* counterTemp = mNucleusBuilder->cNet(scope, net_name, VERI_NONE, dim, true, false, sourceLocation);
    mNucleusBuilder->aNetToScope(counterTemp, scope);

    NULvalue* counterTempLvalue = mNucleusBuilder->cIdentLvalue(counterTemp, sourceLocation);
    // Init temp net with initial condition value
    NUStmt* blockingAssign = mNucleusBuilder->cBlockingAssign(counterTempLvalue, condition, sourceLocation);
    NUStmtList* initStmts = mNucleusBuilder->cStmtList();
    mNucleusBuilder->aStmtToStmtList(blockingAssign, initStmts);
    // Test: counter > 0
    NUExpr* constantZero = mNucleusBuilder->cConst(0u, 32, false, sourceLocation);
    NUExpr* counterFetch = mNucleusBuilder->cIdentRvalue(counterTemp, sourceLocation);
    NUExpr* conditionOp = mNucleusBuilder->cBinaryOperation(counterFetch, VERI_GT, constantZero, false, true, sourceLocation);
    //conditionOp->resize(1);
    // Advance - decrement the counter.
    NUExpr* constantOne = mNucleusBuilder->cConst(1u, 32, false, sourceLocation);
    counterFetch = mNucleusBuilder->cIdentRvalue(counterTemp, sourceLocation);
    NUExpr* decrement = mNucleusBuilder->cBinaryOperation(counterFetch, VERI_MIN, constantOne, true, true, sourceLocation);
    counterTempLvalue = mNucleusBuilder->cIdentLvalue(counterTemp, sourceLocation);
    NUStmt* advanceAssign = mNucleusBuilder->cBlockingAssign(counterTempLvalue, decrement, sourceLocation);
    NUStmtList* advanceStmts = mNucleusBuilder->cStmtList();
    mNucleusBuilder->aStmtToStmtList(advanceAssign, advanceStmts); 
    // Body.
    NUStmtList* bodyStmts = mNucleusBuilder->cStmtList();
    if (node.GetStmt()) { node.GetStmt()->Accept(*this) ; } ;
    NUStmt* bodyStmt = dynamic_cast<NUStmt*>(gScope()->gValue());
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(bodyStmt, node, "Unable to get body statemnt");
    mNucleusBuilder->aStmtToStmtList(bodyStmt, bodyStmts);
    //Create for
    NUBase* repeatLoop = mNucleusBuilder->cFor(*initStmts, conditionOp, *advanceStmts, *bodyStmts, sourceLocation);
    delete initStmts;
    delete advanceStmts;
    delete bodyStmts;
    //Return for
    gScope()->sValue(repeatLoop);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIREPEAT" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriEventControlStatement, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIEVENTCONTROLSTATEMENT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // 'or' separated list of expressions
    /// TODO may need to check for mixed events here
    if (!node.GetAt() || node.GetAt()->Size() == 0) {
        // Implicit event_expression list
        // TODO support "*"
    } else {
      // Event Control statements are only allowed as the first statement in an always block, 
      // unless certain conditions hold true. gEventControlLegal is used to determine
      // whether this is the first statement or not.
      //
      // If this is not the first statement, then non-edge (i.e. combinational) event
      // controls are an error.
      //
      // If this is a combinational always block (mIsCombAlways), then we warn about
      // combinational event controls, but edge events are still illegal.
      if (!gEventControlLegal()) {
        if (!isCombAlways(&node)) {
          // There is an edge event
          mNucleusBuilder->getMessageContext()->UnsupportedEvent(&sourceLocation);
          setError(true);
        }
        else if (mIsCombAlways) {
          // No edge event, and we're in a combinational always block
          mNucleusBuilder->getMessageContext()->IgnoredEvent(&sourceLocation);
        } else {
          mNucleusBuilder->getMessageContext()->UnsupportedEvent(&sourceLocation);
          setError(true);
        }
        gScope()->sValue(NULL);
        return;
      }
      
      UtVector<NUBase*> exprVector;
        gScope()->gValueVector(exprVector);
        unsigned i ;
        VeriExpression *event_expr ;
        FOREACH_ARRAY_ITEM(node.GetAt(), i, event_expr) {
            if (event_expr) event_expr->Accept(*this) ;
            NUBase* eventExpr = gScope()->gValue();
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(eventExpr, node, "Unable to get event expression");
            exprVector.push_back(eventExpr); 
        }
        gScope()->sValueVector(exprVector);
    }

    if (node.GetStmt()) node.GetStmt()->Accept(*this) ;
    else gScope()->sValue(NULL);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIEVENTCONTROLSTATEMENT" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriDisable, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIDISABLE" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    NUBase *disable = NULL;
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Disables can only refer to blocks in the calling hierarchy. 
    // Hierarchical references are not supported.
    if (node.GetTaskBlockName() && node.GetTaskBlockName()->IsHierName()) {
      UtString hierName = composeSelectedName(dynamic_cast<VeriSelectedName*>(node.GetTaskBlockName()));
      UtString vName = gVisibleName(node.GetTaskBlockName(),hierName.c_str());
      getMessageContext()->UnsupportedBreak(&sourceLocation, VeriNode::PrintToken(VERI_DISABLE), vName.c_str());
      setError(true);
    } else {
      UtString origName;
      if (node.GetTaskBlockName()) {
          origName = node.GetTaskBlockName()->GetName();
      } else {
          // if there is no task/block name available then use the keyword 'fork' as is done in VeriDisable::PrettyPrint 
          origName = VeriNode::PrintToken(VERI_FORK);
      }
      UtString name(gVisibleName(&node, createNucleusString(origName)));
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(!name.empty(), node, "Unable to get block name");
      if (! node.GetTaskBlockName()) {
        // fork is not supported, so just report that we don't support 'break fork' either
        getMessageContext()->UnsupportedBreak(&sourceLocation, VeriNode::PrintToken(VERI_DISABLE), name.c_str());
        setError(true);
      }
      NUScope* parentNamedBlock = fParentNamedBlock(gScope(), name);
      if (! parentNamedBlock ) {
        getMessageContext()->UnsupportedBreak(&sourceLocation, VeriNode::PrintToken(VERI_DISABLE), name.c_str());
        setError(true);
      } else {
        disable = mNucleusBuilder->cBreak(dynamic_cast<NUBlock*>(parentNamedBlock), VeriNode::PrintToken(VERI_DISABLE), name, sourceLocation);
      }
    }
    
    gScope()->sValue(disable);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIDISABLE" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriWhile, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIWHILE" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // We have empty initial and advance statement list for while loop
    NUStmtList initStmtList;
    NUStmtList repeatStmtList;
 
    // Create a temporary block (which we use to accumulate residual task enables
    // generated by function calls in the conditional expression).
    // See comments for 'VeriFunctionCall'.
    NUBlock* block = createBlockAndPushScope(&node, sourceLocation, "block");

    // Get condition
    gScope()->sContext(V2NDesignScope::RVALUE);
    if (node.GetCondition()) node.GetCondition()->Accept(*this);
    NUExpr* condition = dynamic_cast<NUExpr*>(gScope()->gValue());
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(condition, node, "Unable to get condition");
    NUStmtList extraConditionStmts;
    mNucleusBuilder->gStmtsFromBlock(&extraConditionStmts, block, false /* append */);

    // We're done capturing function call residuals. 
    deleteBlockAndPopScope(block);
    block = NULL;
    
    // Create a begin/end around the while body, if there
    // isn't already one there. This begin/end is
    // necessary to capture task enables that are generated
    // by function calls. See comments for VeriFunctionCall
    if (node.GetStmt() && !node.GetStmt()->IsSeqBlock()) {
      block = createBlockAndPushScope(node.GetStmt(), sourceLocation, "while");
    }
    // Get body
    NUStmtList * bodyStmtList = mNucleusBuilder->cStmtList();
    if (node.GetStmt()) node.GetStmt()->Accept(*this);
    NUStmt* whileLoopBody = dynamic_cast<NUStmt*>(gScope()->gValue());
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(whileLoopBody, node, "Unable to get body");
    // If a block was created, then add the statement to the block,
    // and pop scope.
    NUStmt* bodyStmt = whileLoopBody;
    if (block) {
      if (whileLoopBody) mNucleusBuilder->aStmtToBlock(whileLoopBody, block);
      PopScope();
      bodyStmt = block;
    }
    if (bodyStmt) mNucleusBuilder->aStmtToStmtList(bodyStmt, bodyStmtList);

    // before we create the NUFor (to implement the while loop), if there are 
    // any extra_condition_stmts then
    // make a separate copy of them, and put one copy at the end of the
    // initial_stmts and the other copy at the end of the advance_stmts lists.
    // This allows us to handle function calls within the condition expression properly.
    if (not extraConditionStmts.empty()) {
      NUStmtList copyExtraConditionStmts;
      NUScope* parent_declaration_scope = getDeclarationScope();
      CopyContext copy_context(parent_declaration_scope, NULL) ;
      deepCopyStmtList(extraConditionStmts, &copyExtraConditionStmts, copy_context, NULL);
      initStmtList.insert(initStmtList.end(),extraConditionStmts.begin(),extraConditionStmts.end());
      repeatStmtList.insert(repeatStmtList.end(),copyExtraConditionStmts.begin(),copyExtraConditionStmts.end());
    }

    // Create while loop 
    NUBase* whileLoop = mNucleusBuilder->cFor(initStmtList, condition, repeatStmtList, *bodyStmtList, sourceLocation);
    delete bodyStmtList;
    // Return while loop 
    gScope()->sValue(whileLoop);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIWHILE" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriInitialConstruct, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIINITIALCONSTRUCT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VERI_VISIT_NODE(VeriModuleItem, node) ; // Call of base class
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    // Get parent module
    NUModule* module = mNucleusBuilder->gModule(getNUScope(&node));

    // Create dummy begin/end if required (see comments at VeriFunctionCall)
    VeriStatement* initStmt = node.GetStmt();
    NUBlock* block = NULL;
    bool requiresDummyBlock = (not initStmt->IsSeqBlock()) && Verific2NucleusUtilities::hasSideEffectStmt(initStmt);
    if (requiresDummyBlock) 
      block = createBlockAndPushScope(initStmt, sourceLocation, "init");

    // Process statement
    if (node.GetStmt()) node.GetStmt()->Accept(*this) ;

    // Retrieve generated statement
    if (block) {
      // Put it in the block we created, and pop the scope
      NUStmt* stmt = dynamic_cast<NUStmt*>(gScope()->gValue()); 
      if (stmt) mNucleusBuilder->aStmtToBlock(stmt, block);
      PopScope();
    } else {
      // We didn't create a block, so create one now, and put the statement in it
      block = mNucleusBuilder->blockify(dynamic_cast<NUStmt*>(gScope()->gValue()), module, sourceLocation);
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(block, node, "Unable to create block");
    }

    // At this point, we're guaranteed to have a block
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(block, node, "Unable to get block statement");
    
    NUInitialBlock* initialBlock = mNucleusBuilder->cInitialBlock(getNUScope(&node), block, sourceLocation);
    mNucleusBuilder->aInitialBlockToModule(initialBlock, module);
    gScope()->sValue(NULL);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIINITIALCONSTRUCT" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriDelayControlStatement, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIDELAYCONTROLSTATEMENT VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Delays are ignorable for nucleus, so here just redirecting the call to corresponding statement
    if (node.GetStmt()) {
      node.GetStmt()->Accept(*this) ;
    } else {
        /// empty delay statement (probably never happens now, with introduction of VeriNullStatement)
        gScope()->sValue(NULL);
    }
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriNullStatement, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERINULLSTATEMENT VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    (void)node;
    
    gScope()->sValue(NULL);
}

// TODO - probably this visitor not needed
void VerificVerilogDesignWalker::VERI_VISIT(VeriCommentNode, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERICOMMENTNODE VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriModuleItem, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIMODULEITEM" << std::endl;
#endif
}



//   A VeriSelectedId implements an (single dot separated) 'hierarchical' name  : "<name> . suffix"
// this is also expected to work for multi level hierarchical names such as a.b.c.d
void VerificVerilogDesignWalker::VERI_VISIT(VeriSelectedName, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERISELECTEDNAME" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    V2NDesignScope::ContextType c = gScope()->gContext();
    AtomArray hierPath; 
    UtString suffix(gVisibleName(&node, node.GetSuffix()));
    mNucleusBuilder->aNameToPath(suffix, &hierPath); 
    VeriName* pref = node.GetPrefix();
    // build up an array of names for the hier path defined in node.  The array is built in reverse order, and is reversed once complete
    // Until we get veri id ref name we will add it's suffix to the list (veriidref is the ending point, where we have simple idref
    // instead of selectedname.
    while (ID_VERIIDREF != pref->GetClassId()) {
        VeriSelectedName* selName = static_cast<VeriSelectedName*>(pref);
        // We don't want to add escapes here. If they are required, then they're already there.
        // Note that they're not allowed for references to variables in generate blocks.
        UtString name(gVisibleName(selName, selName->GetSuffix()));
        mNucleusBuilder->aNameToPath(name, &hierPath); 
        pref = pref->GetPrefix();
    }
    UtString lastName(gVisibleName(pref, pref->GetId()->GetName())); // Again, no escapes
    mNucleusBuilder->aNameToPath(lastName, &hierPath); 
    std::reverse(hierPath.begin(), hierPath.end());
    // now hierPath is an array of the names that made up the hierpath in node.

    VeriIdDef* scopeId = node.GetPrefix()->GetId();
    VeriIdDef* ref = node.GetId();
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(ref, node, "Unable to get hierarchical reference id");
    // if user has disallowed hierarchical references to tasks, reject them here
    if (ref->IsTask() && not mAllowHierRefs) {
        UtString vName = gVisibleName(ref, ref->GetName());
        const char* taskName = vName.c_str();
        // Avoid duplicating this error for multiple module instances
        if (not previouslyVisited(taskName, &sourceLocation))
            getMessageContext()->OutOfScopeTask(&sourceLocation, taskName);
    }
    VeriModule* veriModule = NULL;
    if (scopeId->IsInst()) {
        veriModule = scopeId->GetInstantiatedModule(); 
    } else if (scopeId->IsModule()) {
        // we don't support SV module types
        veriModule = scopeId->GetModule();
    } else if (scopeId->IsTask() || scopeId->IsFunction() || scopeId->IsBlock()) {
        // we don't support hierarchical references to tasks declared inside a generate block 
        // so check for it
        if (scopeId->IsBlock()) {
            VeriModuleItem* item = scopeId->GetModuleItem();
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(! (item->IsGenerateBlock() && ref->IsTask()), node, 
                "Hierarchical reference resolves into a task declared in a generate block -- This is currently unsupported.");
        }
        veriModule = scopeId->GetModuleItem()->GetScope()->GetContainingModule()->GetModule();
    } else {
        //VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Unsupported scope object id encountered during nucleus hierarchical net reference construction, the only supported scope objects are modules, instances, functions, tasks, blocks");
        //report error
        reportUnsupportedLanguageConstruct(sourceLocation, "Unsupported scope object id encountered during nucleus hierarchical net reference construction, the only supported scope objects are modules, instances, functions, tasks, blocks");
        return;
    }
    if (veriModule && (veriModule->IsInterface() || veriModule->IsPackage() || veriModule->IsProgram())) {
        reportUnsupportedLanguageConstruct(sourceLocation, "A hierarchical reference to an object defined in Interface/Package/Program");
        return;
    }
    // We need to apply uniquification here, in order to be able to request uniquifed name during instantiated module (possible resolution) 
    if (veriModule) { 
        createUniquifiedModuleName(veriModule);
    }
    // masterModule - is the module where the hierarchically referred object (net, task) is defined
    NUModule* masterModule = Verific2NucleusUtilities::gNUModule(veriModule);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(masterModule, node, "Unable to get master module");
    NUBase* hierRef = 0;
    if (ref->IsTask() || ref->IsFunction()) {
        NUModule* parentModule = mNucleusBuilder->gModule(getNUScope(&node));
        NUTask* localTask = NULL;
        if (parentModule == masterModule) {
            // local task case
            // We have to handle the case separately because local task hierarchical reference can occur before it's actual
            // declaration population so in that case we need to populate taskdecl first
            if (mTFRegistry.find(ref) == mTFRegistry.end()) {
                // populate taskDecl
                ref->GetModuleItem()->Accept(*this);
                // after populationthe taskDecl should already be in registery
                bool taskFound = (mTFRegistry.find(ref) != mTFRegistry.end());
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(taskFound, node, "Unable to find task declaration in mTFRegistry");
            }
            //Get taskDecl from mTFRegistry
            localTask = dynamic_cast<NUTask*>(mTFRegistry[ref]);
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(localTask, node, "Incorrect local task declaration object found");
        }
        if (!localTask) {
            // check if we have already populated it
            if (mTFRegistry.find(ref) == mTFRegistry.end()) {
                //set task parent module as current scope in order correctly add task to parent
                V2NDesignScope* current_scope = new V2NDesignScope(masterModule, NULL);
                PushScope(current_scope, masterModule->getName()->str());
                // populated task
                ref->GetModuleItem()->Accept(*this);
                // pop task parent module scope
                PopScope();
            }
        }
        hierRef = mNucleusBuilder->cTaskEnableHierRef(parentModule, masterModule, localTask, &hierPath, suffix, sourceLocation);
    } else if (ref->IsNet() || ref->IsReg() || ref->IsPort()) {
        // parentModule - is the module where the hierarchical reference of the object (net, task) occurs
        NUModule* parentModule = mNucleusBuilder->gModule(getNUScope(&node));
        NUNet* netHierRef = mNucleusBuilder->fNetHierRef(parentModule, &hierPath);
        if (!netHierRef) {
            NUNet* localNet = NULL;
            if (parentModule == masterModule) {
                // local net case
                // We have to handle the case separately because local net hierarchical reference can occur before it's actual
                // declaration end (where we add net to module), so will pick up from temp local registery mDeclarationRegistry
                localNet = dynamic_cast<NUNet*>(findNet(ref));
                UtString message("Unable to find local net named: ");
                message << suffix << " in module: " << parentModule->getName()->str();
                VERIFIC_CONSISTENCY_NO_RETURN_VALUE(localNet, node, message.c_str());
            }
            netHierRef = mNucleusBuilder->cNetHierRef(masterModule, parentModule, &hierPath, suffix, localNet,  sourceLocation);
            mNucleusBuilder->aNetHierRefToModule(netHierRef, parentModule);
        }
        if (mDeclarationRegistry.find(&node) == mDeclarationRegistry.end()) {
            mDeclarationRegistry[&node] = netHierRef;
        }
        // Here we should already have net hierarchical reference, otherwise inconsistency
        if (!netHierRef) {
            UtString message("Unable to find net named: ");
            message << suffix << " in module: " << parentModule->getName()->str();
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, message.c_str());
        }
        if (V2NDesignScope::LVALUE == c) {
            hierRef = mNucleusBuilder->cIdentLvalue(netHierRef, sourceLocation);
        } else if (V2NDesignScope::RVALUE == c) {
            hierRef = mNucleusBuilder->cIdentRvalue(netHierRef, sourceLocation);
        } else {
            VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Incorrect context type");
        }
    } else {
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(false, node, "Unsupported hierarchical reference");
    }
    // Set evaluated value
    gScope()->sValue(hierRef);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERISELECTEDNAME" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriPortOpen, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIPORTOPEN" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#else
    (void)node;
#endif
    // for open port connection just return NULL 
    gScope()->sValue(NULL);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIPORTOPEN" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriRealVal, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIREALVAL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Create const real value
    NUBase* constVal = mNucleusBuilder->cConst(node.GetNum(), sourceLocation);
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(constVal, node, "Unable to create real value");
    // Set evaluated value
    gScope()->sValue(constVal);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIREALVAL" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriMinTypMaxExpr, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIMINTYPMAXEXPR" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (node.GetMinExpr()) node.GetMinExpr()->Accept(*this) ;
    // Seems Cheetah using this value only
    if (node.GetTypExpr()) node.GetTypExpr()->Accept(*this) ;
//////    if (node.GetMaxExpr()) node.GetMaxExpr()->Accept(*this) ;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIMINTYPMAXEXPR" << std::endl;
#endif
}

//! handle any population issues from specify blocks
/*! at one time we would ignore all of the contents of a specify block
 * but the $setuphold timing check can imply a connection between the
 * reference event and the delayed reference, or the data_event and
 * the delayed data arguments.
 * 
 * currently this routine only handles this implicit connection
 * defined in a $setuphold timing check (LRM 15.5.1, 1364-2005)
 *
 * note it is possible(and valid) that the same connection will be
 * implied by several $setuphold specifications, we create one for
 * each specification, but the excess assignments will be removed in a
 * later step.
 */
void VerificVerilogDesignWalker::VERI_VISIT(VeriSpecifyBlock, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERISPECIFYBLOCK" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Specify blocks include statements to do the following:
    //  1) Define pin-to-pin timing delays across module paths
    //  2) Set up timing checks in the circuits
    //  3) Define specparam constants
    // The first is not supported by Nucleus/Carbon, the second
    // can imply an implicit connection which we must handle here, 
    // and third one will be taken care by verific constant evaluator.
    VERI_VISIT_NODE(VeriModuleItem, node) ; // Call of base class

    // Iterate over VeriSystemTimingCheck specify items
    // (NOTE: We're only interested in $setuphold timing check,
    // which doesn't need to create a new scope.)
    VeriModuleItem *item = NULL;
    unsigned i;
    FOREACH_ARRAY_ITEM(node.GetSpecifyItems(), i, item) {
      if (item && (item->GetClassId() == ID_VERISYSTEMTIMINGCHECK)) 
        item->Accept(*this);
    }

#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERISPECIFYBLOCK" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriSystemFunctionCall, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
  std::cerr << "NUCLEUSBUILDER VERISYSTEMFUNCTIONCALL" << std::endl;
  node.PrettyPrint(std::cerr, 0);
  std::cerr << std::endl;
#endif
  // Get current node source location 
  SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  UtString name("$");
  UtString vName = gVisibleName(&node, node.GetName());
  name << vName.c_str(); // $ is added to name to match existing messages
  // get module scope from current scope
  NUScope* scope = getNUScope(&node);
  NUModule* module = mNucleusBuilder->gModule(scope);
  VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, node, "Unable to get parent module");
  bool isFOpenSysTask = (VERI_SYS_CALL_FOPEN == node.GetFunctionType());
  if ( !isSupportedSysFunction(node.GetFunctionType())) {
    // unsupported system function, report warning and insert a constant 0 in its place
    if (mWarnForSysTask) {
      // in case -warnForSysTask flag is passed (natively supported by Interra) we need to generate alert to match up with Interra
      // Note: Verific is silent about unknown sys tasks/functions and there is no option to change this behavioral, so reporting alert here 
      getMessageContext()->UndefinedSysTaskFunc(&sourceLocation, name.c_str());
    } else {
      // otherwise report warning
      getMessageContext()->UnsupportedSysFunc(&sourceLocation, name.c_str());
    }
    NUBase* constantZero = NUConst::create(false, 0,  32, sourceLocation);
    gScope()->sValue(constantZero);
  } else {
    // Get expressions if any
    NUExprVector exprVector;
    if (!isFOpenSysTask || isFOpenSysTask && doingOutputSysTasks(module)) {
    if (node.GetArgs()) {
      unsigned i ;
      VeriExpression *expr ;
      FOREACH_ARRAY_ITEM(node.GetArgs(), i, expr) {
        if (!expr) continue ;
        gScope()->sContext(V2NDesignScope::RVALUE);
        // I think each argument is self determined
        int context_size_sign = expr->StaticSizeSign(0);
        int savedContextSizeSign = gScope()->updateContextSizeSign(context_size_sign); // save starting value
        VeriExpression* evalExpr = evalConstExpr(context_size_sign, expr);
        if (evalExpr) {
            evalExpr->Accept(*this);
            delete evalExpr;
        } else {
            expr->Accept(*this) ;
        }
        NUExpr* iExpr = dynamic_cast<NUExpr*>(gScope()->gValue());
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(iExpr, *expr, "Unable to get expression for System function");
        mNucleusBuilder->aExprToExprVector(iExpr, exprVector);
        (void) gScope()->updateContextSizeSign(savedContextSizeSign);    //reset the context size sign back to value it had before this argument
      }
    }
    }

    if (isFOpenSysTask) {
        if (doingOutputSysTasks(module)) {
            // fopen is a system function but we model it as a system task
            // because it has side effects when called, (such as opening a
            // file and creating a new file descriptor)
            StringAtom* sym = module->gensym(name.c_str());
            ConstantRange r(31, 0);   // file descriptors are always 32 bits
            NUNet* net = module->createTempVectorNet(sym, r, false, sourceLocation);
            NUFOpenSysTask* sysFOpen = mNucleusBuilder->cFOpenSysTask(sym, net, exprVector, module, sourceLocation);
            UtString errmsg;
            NUBase* expr = 0;
            if (sysFOpen->isValid(&errmsg)) {
              expr = mNucleusBuilder->cIdentRvalue(net, sourceLocation);
              // if we are in a context where stmts can't be added function will add dummy always block and add stmt to it
              mNucleusBuilder->aStmtToScope(sysFOpen, scope, sourceLocation);
            } else {
              setError(true);
              getMessageContext()->InvalidSysFuncArg(&sourceLocation, name.c_str(), errmsg.c_str());
              delete sysFOpen;
            }
            gScope()->sValue(expr);
        } else {
            //Report disabled
            getMessageContext()->DisabledSysFunc(&sourceLocation, name.c_str(), "-enableOutputSysTasks");
            // here we need to populate constant 0
            NUBase* constantZero = NUConst::create(false, 0,  32, sourceLocation);
            gScope()->sValue(constantZero);
            return;
        }
        // If this is a random function, then we model it as a system task
        // because it modifies its args, and we don't deal well with
        // expressions that mutate their args.
    } else if (isRandomSysTask(node.GetFunctionType())) {
        // add a system task to the current statement list.
        StringAtom* sym = module->gensym(name.c_str());
        ConstantRange r(31, 0);
        NUNet* net = module->createTempVectorNet(sym, r, false, sourceLocation);
        NULvalue* lvalue = mNucleusBuilder->cIdentLvalue(net, sourceLocation);
        NUSysRandom* sysRand = mNucleusBuilder->cRandomSysTask(sym, name, lvalue, exprVector, module, sourceLocation);
        UtString errmsg;
        NUExpr* expr = 0;
        if (sysRand->isValid(&errmsg)) {
            expr = mNucleusBuilder->cIdentRvalue(net, sourceLocation);
            expr->setSignedResult(true);
            // if we are in a context where stmts can't be added function will add dummy always block and add stmt to it
            mNucleusBuilder->aStmtToScope(sysRand, scope, sourceLocation);
        } else {
            getMessageContext()->InvalidSysFuncArg(&sourceLocation, name.c_str(), errmsg.c_str());
            delete sysRand;
        }
        gScope()->sValue(expr);
    } else {
        // rest of system functions $signed $unsigned $itor $rtoi etc.
        NUModule* module = mNucleusBuilder->gModule(scope);

        NUBase* sysFunction = mNucleusBuilder->cSysFunctionCall(node.GetFunctionType(), gVisibleName(&node, node.GetName()), exprVector, module, sourceLocation);
        gScope()->sValue(sysFunction);
    }
  }
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriDeAssign, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIDEASSIGN VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    getMessageContext()->UnsupportedStatement(&sourceLocation, "DEASSIGN");
    setError(true);
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriAssign, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIASSIGN VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    getMessageContext()->UnsupportedStatement(&sourceLocation, "QUASI_CONT_ASSIGN");
    setError(true);
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriStrength, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERISTRENGTH VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif

    // Make sure this isn't a non-supported strength. We only support
    // unset and VERI_STRONG0 for strength0 and VERI_STRONG1 for strength1. 
    // Note: The functions return VERI_STRONG0 and VERI_STRONG1
    // for 'unset'.
    unsigned str0 = getStrength0(node.GetLVal(), node.GetRVal());
    unsigned str1 = getStrength1(node.GetLVal(), node.GetRVal());
    if ((str0 != VERI_STRONG0) || (str1 != VERI_STRONG1)) {
        SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
        UtString strength1 = VeriNode::PrintToken(str1);
        UtString strength0 = VeriNode::PrintToken(str0);
        getMessageContext()->UnsupportedStrengths(&sourceLocation, strength0.c_str(), strength1.c_str());
        // UnsupportedStrengths is an Alert and might be demoted by user, so no setError(true) here
    }
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriParBlock, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIPARBLOCK VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    getMessageContext()->UnsupportedStatement(&sourceLocation, "PAR_BLOCK");
    setError(true);
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriPrimitive, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIPRIMITIVE" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VERI_VISIT_NODE(VeriModule, node) ; // Call of base class
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIPRIMITIVE" << std::endl;
#endif
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriTable, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERITABLE" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // local nets already created
    NUModule* module = mNucleusBuilder->gModule(getNUScope(&node));
    if (node.IsSequential()) {
        udpSequentialComponents(&node, module, sourceLocation);
    } else {
        NUContAssign* the_assign = NULL;
        udpContAssign(&node, module, sourceLocation, &the_assign);
        if (the_assign) {
            module->addContAssign(the_assign);
        }
    }
    mNucleusBuilder->verboseUdp(module, sourceLocation);
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERITABLE" << std::endl;
#endif
}

/*-----------------------------------------------------------------*/
//          Visit Methods : Class details in VeriTreeNode.h
/*-----------------------------------------------------------------*/

void VerificVerilogDesignWalker::VERI_VISIT(VeriTreeNode, node)
{
  (void)node;
//////#ifdef DEBUG_VERIFIC_VERILOG_WALKER
//////    std::cerr << "VERITREENODE VISIT CALLED" << std::endl;
//////#endif
//////    // Do nothing
}
//////
///////*-----------------------------------------------------------------*/
////////          Visit Methods : Class details in VeriModule.h
///////*-----------------------------------------------------------------*/
//////
///////*-----------------------------------------------------------------*/
////////          Visit Methods : Class details in VeriStatement.h
///////*-----------------------------------------------------------------*/
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriStatement, node)
{
  (void)node;
//////#ifdef DEBUG_VERIFIC_VERILOG_WALKER
//////    std::cerr << "VERISTATEMENT VISIT CALLED" << std::endl;
//////#endif
//////    // Do nothing
}
//////
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriGenVarAssign, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIGENVARASSIGN VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    _ofs << PrintLevel(_nLevel) ;
//////
//////    // target
//////    PrintIdentifier(_ofs, (node.GetName()) ? node.GetName() : node.GetId()->GetName()) ;
//////    _ofs << " " << gStringValue(VERI_EQUAL) << " " ;
//////
//////    // value
//////    if (node.GetValue()) node.GetValue()->Accept(*this) ;
//////    _ofs << " ;" << endl ;
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriDoWhile, node)
{
    UtString str;
    str << "'" << VeriNode::PrintToken(VERI_DO) << " " << VeriNode::PrintToken(VERI_WHILE) << "'";
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    getMessageContext()->UnsupportedStatement(&sourceLocation, str.c_str());
    setError(true);
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriRandsequence, node)
{
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    getMessageContext()->UnsupportedStatement(&sourceLocation, VeriNode::PrintToken(VERI_RANDSEQUENCE));
    setError(true);
}

//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriForce, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIFORCE VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    getMessageContext()->UnsupportedAndIgnoredStatement(&sourceLocation, "FORCE");
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    _ofs << PrintLevel(_nLevel) ;
//////    _ofs << gStringValue(VERI_FORCE) << " " ;
//////    if (node.GetAssign()) node.GetAssign()->Accept(*this) ;
//////    _ofs << " ;" << endl ;
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriRelease, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIRELEASE VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    getMessageContext()->UnsupportedAndIgnoredStatement(&sourceLocation, "RELEASE");
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    _ofs << PrintLevel(_nLevel) ;
//////    _ofs << gStringValue(VERI_RELEASE) << " " ;
//////    if (node.GetLVal()) node.GetLVal()->Accept(*this) ;
//////    _ofs << " ;" << endl ;
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriJumpStatement, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIJUMPSTATEMENT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // break, continue, return jumpers currently not supported
    // TODO support this
    // look for Interra VHDL solution for corresponding VHDL constructs: localflow/VhPopulateStmt.cxx:2299 exitStmt() function implemention
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    getMessageContext()->UnsupportedStatement(&sourceLocation, VeriNode::PrintToken(node.GetJumpToken()));
    setError(true);
    // Accessor methods
    //unsigned                    GetJumpToken() const        { return _jump_token ; } // VERI_RETURN, VERI_BREAK or VERI_CONTINUE
    //VeriExpression *            GetExpr() const             { return _expr ; }       // For 'return' statement : the returned expression
    //VeriIdDef *                 GetJumpId() const           { return _jump_id ; }    // Backpointer to id (loop label or subprogram) to which this statement jumps
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIJUMPSTATEMENT" << std::endl;
#endif
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriAssertion, node)
{
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    getMessageContext()->UnsupportedStatement(&sourceLocation, VeriNode::PrintToken(node.GetAssertCoverProperty()));
    setError(true);
}

//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriWait, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIWAIT VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    _ofs << PrintLevel(_nLevel) ;
//////
//////    _ofs << gStringValue(VERI_WAIT) << " (" ;
//////    if (node.GetCondition()) node.GetCondition()->Accept(*this) ; ;
//////    _ofs << ") " ;
//////    if (node.GetStmt()) { IncTabLevel(1) ; node.GetStmt()->Accept(*this) ; DecTabLevel(1) ; }
//////    else _ofs << ";" << endl;
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriEventTrigger, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIEVENTRIGGER VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    _ofs << PrintLevel(_nLevel) ;
//////    _ofs << gStringValue(VERI_RIGHTARROW) << " " ;
//////    if (node.GetEventName()) node.GetEventName()->Accept(*this) ;
//////    _ofs << " ;" << endl ;
}
//////
//////
///////*-----------------------------------------------------------------*/
////////        Visit Methods : Class details in VeriModuleItem.h
///////*-----------------------------------------------------------------*/
//////

//////////////////////////////////////////////////////// SV unsupported module items 

void VerificVerilogDesignWalker::VERI_VISIT(VeriImportDecl, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_IMPORT); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriClass, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_CLASS); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriPropertyDecl, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_PROPERTY); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriSequenceDecl, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_SEQUENCE); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriModport, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_MODPORT); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriModportDecl, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_MODPORT); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriClockingDecl, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_CLOCKING); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriConstraintDecl, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_CONSTRAINT); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriBindDirective, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_BIND); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriOperatorBinding, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_BIND); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriForeachOperator, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_FOREACH); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriNetAlias, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_ALIAS); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriCovergroup, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_COVERGROUP); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriCoverageSpec, node)
{
    UNSUPPORTED_SV_DECLARATION(node, node.GetType()); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriLetDecl, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_LET); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriExportDecl, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_EXPORT); // this is an error
}

/////////////////////////////////////////////////////// END SV unsupported module items 

void VerificVerilogDesignWalker::VERI_VISIT(VeriDefParam, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIDEFPARAM VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    _ofs << PrintLevel(_nLevel) ;
//////    _ofs << gStringValue(VERI_DEFPARAM) << " " ;
//////
//////    unsigned i ;
//////    VeriIdDef *id ;
//////    IncTabLevel(1) ;
//////    FOREACH_ARRAY_ITEM(node.GetIds(), i, id) {
//////        if (i) {
//////            _ofs << "," << endl ;
//////            _ofs << PrintLevel(_nLevel) ;
//////        }
//////        id->Accept(*this) ;
//////    }
//////    DecTabLevel(1) ;
//////    _ofs << " ; " << endl ;
}
//////
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriPathDecl, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIPATHDECL VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriSystemTimingCheck, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERISYSTEMTIMINGCHECK VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Save current node context 
    V2NDesignScope::ContextType c = gScope()->gContext();

    // We're only concerned with $setuphold
    if (VeriExpression::GetFuncToken(node.GetTaskName()) == VERI_SYS_CALL_SETUPHOLD) {
      // Generate two continuous assigns per $setuphold
      // $setuphold(ref, data, setup_limit, hold_limit, notifier, stamptime_cond, checktime_cond, dly_ref, dly_data);
      // dly_ref <= ref;      
      // dly_data <= data;
      // If dly_ref, dly_data are missing, no continuous assigns are generated
      if (node.GetArgs()->Size() > 7) {
        // $setuphold arguments are of class type VeriTimingcheckEvent
        VeriTimingCheckEvent* tc = static_cast<VeriTimingCheckEvent*>(node.GetArgs()->At(0));
        // Strip off the posedge, negedge
        VeriExpression* evnt_ref = tc ? tc->GetTerminalDesc() : 0;
        tc = static_cast<VeriTimingCheckEvent*>(node.GetArgs()->At(7));
        VeriExpression* dly_evnt_ref = tc ? tc->GetTerminalDesc() : 0;
        SpecifyBlockImplicitConnection(dly_evnt_ref, evnt_ref, node);
      }
      if (node.GetArgs()->Size() > 8) {
        VeriTimingCheckEvent* tc = static_cast<VeriTimingCheckEvent*>(node.GetArgs()->At(1));
        // Strip off the posedge, negedge
        VeriExpression* evnt_ref = tc ? tc->GetTerminalDesc() : 0;
        tc = static_cast<VeriTimingCheckEvent*>(node.GetArgs()->At(8));
        VeriExpression* dly_evnt_ref = tc ? tc->GetTerminalDesc() : 0;
        SpecifyBlockImplicitConnection(dly_evnt_ref, evnt_ref, node);
      }
    }

    // Restore current node context
    gScope()->sContext(c);
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriGenerateConstruct, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIGENERATECONSTRUCT VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    _ofs << PrintLevel(_nLevel) ;
//////    _ofs << gStringValue(VERI_GENERATE) << endl ;
//////    unsigned i ;
//////    VeriModuleItem *item ;
//////    FOREACH_ARRAY_ITEM(node.GetItems(), i, item) {
//////        if (item) { IncTabLevel(1) ; item->Accept(*this) ; DecTabLevel(1) ; }
//////    }
//////    _ofs << PrintLevel(_nLevel) << gStringValue(VERI_ENDGENERATE) << endl ;
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriGenerateConditional, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIGENERATECONDITIONAL VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    _ofs << PrintLevel(_nLevel) ;
//////
//////    _ofs << gStringValue(VERI_IF) << " (" ;
//////    node.GetIfExpr()->Accept(*this) ;
//////    _ofs << ") " << endl ; // statements start on a new line
//////    if (node.GetThenItem()) { IncTabLevel(1) ; node.GetThenItem()->Accept(*this) ; DecTabLevel(1) ; }
//////
//////    if (node.GetElseItem()) {
//////        _ofs << PrintLevel(_nLevel) << "else" << endl ;
//////        IncTabLevel(1) ;
//////        node.GetElseItem()->Accept(*this) ;
//////        DecTabLevel(1) ;
//////    }
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriGenerateCase, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIGENERATECASE VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    _ofs << PrintLevel(_nLevel) ;
//////
//////    _ofs << gStringValue(node.GetCaseStyle()) << " (" ;
//////    if (node.GetCondition()) node.GetCondition()->Accept(*this) ;
//////    _ofs << ")" << endl ;
//////
//////    if (node.GetCaseItems()) {
//////        IncTabLevel(1) ;
//////        unsigned i ;
//////        VeriGenerateCaseItem *ci ;
//////        FOREACH_ARRAY_ITEM(node.GetCaseItems(), i, ci) {
//////            //Visit(*ci) ;
//////            ci->Accept(*this);
//////        }
//////        DecTabLevel(1) ;
//////    }
//////
//////    _ofs << PrintLevel(_nLevel) ;
//////    _ofs << gStringValue(VERI_ENDCASE) << endl ;
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriGenerateFor, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIGENERATEFOR VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    _ofs << PrintLevel(_nLevel) ;
//////
//////    // Print 'for' items on one line : FIX ME : printing too many ';'s.
//////    _ofs << gStringValue(VERI_FOR) << " (" ;
//////    if (node.GetInitial()) node.GetInitial()->Accept(*this) ;
//////    _ofs << ";" ;
//////    if (node.GetCondition()) node.GetCondition()->Accept(*this) ;
//////    _ofs << ";" ;
//////    if (node.GetRepetition()) node.GetRepetition()->Accept(*this) ;
//////    _ofs << ")" << endl ;
//////
//////    // Print the generate block :
//////    _ofs << PrintLevel(_nLevel) ;
//////    _ofs << gStringValue(VERI_BEGIN) ;
//////
//////    if (node.GetBlockId()) {
//////        _ofs << " : " ; node.GetBlockId()->Accept(*this) ;
//////    }
//////    _ofs << endl ;
//////
//////    unsigned i ;
//////    VeriModuleItem *item ;
//////    IncTabLevel(1) ;
//////    FOREACH_ARRAY_ITEM(node.GetItems(), i, item) {
//////        item->Accept(*this) ;
//////    }
//////    DecTabLevel(1) ;
//////    _ofs << PrintLevel(_nLevel) ;
//////    _ofs << gStringValue(VERI_END) << endl ;
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriGenerateBlock, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERIGENERATEBLOCK" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get current node source location 
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // Create scope
    /// We don't have NUBlock, so creating scope with null object
    V2NDesignScope* current_scope = new V2NDesignScope(0, gScope());
    // Get parent declaration scope (if not defined it's module/task/function scope)
    NUScope* parent_declaration_scope = getDeclarationScope();
    // We have set compile flag to have always named blocks (auto-generate names for unnamed blocks)
    //TODO this seems not right we need escaping, probably the one which will not escape strings like "name[0], name[1]", but will escape other special cases,
    // at the moment I don't know testcase which will fail because of that, so keeping this until we understand what needs to be populated, or is
    // there need for special escaping in case of generate block ids.
    //UtString blockName(createNucleusString(node.GetBlockId()->GetName()));
    UtString blockName(gVisibleName(&node, node.GetBlockId()->GetName()));
    // check unnamed block and report note
    // IsUnnamedBlock, GetOrigName api's seems not working in static elaborated parse-tree...
    // this seems to be bug in Verific, they somehow get lost the info about unnamed blocks, so just doing workaround by checking genblk name
    // test/langcov/Generate/gennamedblocks5.cbld.log shows the problem with current implementation (when user chooses to have same name as Carbon/Verific
    // do generate for unnamed generate blocks
    // Recent verific releases may already fix this. Either of below comments should be uncommented in that case 
    // If Verific doesn't recognize this as bug, then the right place to populate Verific specific info message during static elaboration (after instance unification, where we still have the info about verific generated names for unnamed generate blocks).
    //if (!node.GetBlockId()->GetOrigName()) {
    //if (node.GetBlockId()->IsUnnamedBlock()) {
    //TODO update this
    UtString genPrefix("genblk");
    if (blockName.substr(0, genPrefix.size()) == genPrefix) {
        getMessageContext()->UnNamedGenBlock(&sourceLocation, blockName.c_str());
    }
    // Create named declaration scope
    NUNamedDeclarationScope* namedDeclarationScope = 
        mNucleusBuilder->cNamedDeclarationScope(blockName, parent_declaration_scope, sourceLocation);
    // Push the scope
    PushScope(current_scope, blockName);
    // Set declaration scope
    gScope()->sDeclarationScope(namedDeclarationScope);
    // Get module items 
    unsigned i ;
    VeriModuleItem* mi;
    FOREACH_ARRAY_ITEM(node.GetItems(), i, mi) {
        if ( ! mi ) continue;
        mi->Accept(*this);
    }
    // restore the scope.
    PopScope();
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END NUCLEUSBUILDER VERIGENERATEBLOCK" << std::endl;
#endif
}

//////
//////
///////*-----------------------------------------------------------------*/
////////            Visit Methods : Class details in VeriMisc.h
///////*-----------------------------------------------------------------*/
//////
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriNetRegAssign, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERINETREGASSIGN VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    if (node.GetLValExpr()) node.GetLValExpr()->Accept(*this) ;
//////    if (node.GetRValExpr()) {
//////        _ofs << " " << gStringValue(VERI_EQUAL) << " " ;
//////        node.GetRValExpr()->Accept(*this) ;
//////    }
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriInstId, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIINSTID VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    if (node.GetName()) { _ofs << node.GetName() << " " ; }
//////    if (node.GetRange()) {
//////        _ofs << "[" ;
//////        node.GetRange()->Accept(*this) ;
//////        _ofs << "] " ;
//////    }
//////    // Port connection list is always there. Even when empty
//////    _ofs << "(" ;
//////    unsigned i ;
//////    VeriExpression *pc ;
//////    FOREACH_ARRAY_ITEM(node.GetPortConnects(), i, pc) {
//////        if (i) _ofs << ", " ;
//////        if (!pc) continue ;
//////        pc->Accept(*this) ;
//////    }
//////    _ofs << ")" ;
}
//////
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriGenerateCaseItem, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIGENERATECASEITEM VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    // Each item starts at a new line
//////    _ofs << PrintLevel(_nLevel) ;
//////
//////    if (node.GetConditions()) {
//////        unsigned i ;
//////        VeriExpression *condition ;
//////        FOREACH_ARRAY_ITEM(node.GetConditions(), i, condition) {
//////            if (i) _ofs << ", " ;
//////            if (condition) condition->Accept(*this) ;
//////        }
//////    } else {
//////        // 'default' case
//////        _ofs << gStringValue(VERI_DEFAULT) ;
//////    }
//////    // Print statement on new line, one level deeper
//////    _ofs << " : " << endl ;
//////    if (node.GetItem()) {
//////        IncTabLevel(1) ; node.GetItem()->Accept(*this) ; DecTabLevel(1) ;
//////        // VeriStatement already ends with a newline
//////    } else {
//////        // Print semicolon only (null), and end with a newline (as a normal statement)
//////        _ofs << PrintLevel(_nLevel+1) << "; " << endl ;
//////    }
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriPath, node)
{
  (void) node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIPATH VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    unsigned i ;
//////    VeriExpression *expr ;
//////
//////    int bSimplePath = (node.GetDataSource()) ? 0 : 1 ;
//////    int bComplexPath = !bSimplePath ;
//////
//////    _ofs << "(" ;
//////    if (bComplexPath && node.GetEdge()) _ofs << gStringValue(node.GetEdge()) << " ";
//////    FOREACH_ARRAY_ITEM(node.GetInTerminals(), i, expr) {
//////        if (i) _ofs << "," ;
//////        if (expr) expr->Accept(*this) ;
//////    }
//////
//////    _ofs << " " ;
//////    if (bSimplePath && node.GetPolarityOperator()) _ofs << gStringValue(node.GetPolarityOperator()) ;
//////    _ofs << gStringValue(node.GetPathToken()) << " " ;
//////
//////    if (bComplexPath) _ofs << "(" ;
//////    FOREACH_ARRAY_ITEM(node.GetOutTerminals(), i, expr) {
//////        if (i) _ofs << "," ;
//////        if (expr) expr->Accept(*this) ;
//////    }
//////
//////    if (bComplexPath) {
//////        _ofs << " " ;
//////        if (node.GetPolarityOperator()) _ofs << gStringValue(node.GetPolarityOperator()) ;
//////        _ofs << ": ";
//////        node.GetDataSource()->Accept(*this) ;
//////        _ofs << ")" ;
//////    }
//////
//////    _ofs << ") " ;
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriDelayOrEventControl, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIDELAYOREVENTCONTROL VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    if (node.GetDelayControl()) {
//////        _ofs << "#" ;
//////        node.GetDelayControl()->Accept(*this) ;
//////    }
//////    if (node.GetEventControl()) {
//////        if (node.GetRepeatEvent()) {
//////            _ofs << "repeat (" ;
//////            node.GetRepeatEvent()->Accept(*this) ;
//////            _ofs << ") " ;
//////        }
//////        _ofs << "@ (" ;
//////
//////        // 'or' separated list of expressions
//////        unsigned i ;
//////        VeriExpression *event_expr ;
//////        FOREACH_ARRAY_ITEM(node.GetEventControl(), i, event_expr) {
//////            if (i) _ofs << " or " ;
//////            if (event_expr) event_expr->Accept(*this) ;
//////        }
//////        _ofs << ")" ;
//////    }
}
//////
///////*-----------------------------------------------------------------*/
////////              Visit Methods : Class details in VeriId.h
///////*-----------------------------------------------------------------*/
//////
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriIdDef, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIIDDEF VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriVariable, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIVARIABLE VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    // Print the id name with optional initial value and dimensions
//////    VERI_VISIT_CALL(VeriIdDef, static_cast<VeriIdDef&>(node)) ; // Print node.GetName()
//////    if (node.GetDimensions()) {
//////        _ofs << " " ;
//////        _ofs << "[" ;
//////        node.GetDimensions()->Accept(*this) ;
//////        _ofs << "]" ;
//////    }
//////    if (node.GetInitialValue()) {
//////        _ofs << " " << gStringValue(VERI_EQUAL) << " " ;
//////        node.GetInitialValue()->Accept(*this) ;
//////    }
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriModuleId, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIMODULEID VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriUdpId, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIUDPID VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    VERI_VISIT_CALL(VeriIdDef, static_cast<VeriIdDef&>(node)) ;
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriTaskId, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERITASKID VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    VERI_VISIT_CALL(VeriIdDef, static_cast<VeriIdDef&>(node)) ;
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriFunctionId, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIFUNCTIONID VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    VERI_VISIT_CALL(VeriIdDef, static_cast<VeriIdDef&>(node)) ;
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriGenVarId, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIGENVARID VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    VERI_VISIT_CALL(VeriIdDef, static_cast<VeriIdDef&>(node)) ;
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriBlockId, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIBLOCKID VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    VERI_VISIT_CALL(VeriIdDef, static_cast<VeriIdDef&>(node)) ;
}
//////
///////*-----------------------------------------------------------------*/
////////        Visit Methods : Class details in VeriExpression.h
///////*-----------------------------------------------------------------*/
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriExpression, node)
{
  (void)node;
//////#ifdef DEBUG_VERIFIC_VERILOG_WALKER
//////    std::cerr << "VERIEXPRESSION VISIT CALLED" << std::endl;
//////#endif
//////    // Do nothing
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriAssignmentPattern, node)
{
    // Traverse the type of the prefix
    if (node.GetTargetType()) {
        SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
        reportUnsupportedLanguageConstruct(sourceLocation, "SV assignment pattern with data_type (<data_type>'{concat}), only patterns without data_type are supported ('{concat})"); // this is an error
        return;
    } else {
        // Call of Base class
        VERI_VISIT_NODE(VeriConcat, node) ;
    }
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriMultiAssignmentPattern, node)
{
    // Traverse the type of the prefix
    if (node.GetTargetType()) {
        SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
        reportUnsupportedLanguageConstruct(sourceLocation, "SV multi-assignment pattern with data_type (<data_type>'{multi-concat}), only patterns without data_type are supported ('{multi-concat})"); // this is an error
        return;
    } else {
        // Call of Base class
        VERI_VISIT_NODE(VeriMultiConcat, node) ;
    }
}


void VerificVerilogDesignWalker::VERI_VISIT(VeriCast, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERICAST" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    reportUnsupportedLanguageConstruct(sourceLocation, "SV cast operator (<casting_type>'(expression))"); // this is an error
    return;
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriNew, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "NUCLEUSBUILDER VERINEW" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    reportUnsupportedLanguageConstruct(sourceLocation, "Operator new not supported"); // this is an error
    return;
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriPatternMatch, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_MATCHES); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriCondPredicate, node)
{
    UNSUPPORTED_SV_DECLARATION(node, VERI_EDGE_AMPERSAND); // this is an error
}

void VerificVerilogDesignWalker::VERI_VISIT(VeriPortConnect, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIPORTCONNECT VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
///    if (mIsPrettyPrint)
//        PrettyPrint(node);
#endif
}
//////
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriTimingCheckEvent, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERITIMINGCHECKEVENT VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    if (node.GetEdgeToken()) {
//////        _ofs << gStringValue(node.GetEdgeToken()) << " " ;
//////        // Print edge control specifiers
//////        if (node.GetEdgeDescStr()) {
//////           _ofs << "[" << node.GetEdgeDescStr() << "]" ;
//////        }
//////    }
//////    if (node.GetTerminalDesc()) node.GetTerminalDesc()->Accept(*this) ;
//////
//////    if (node.GetCondition()) {
//////        _ofs << " " << gStringValue(VERI_EDGE_AMPERSAND) << " " ;
//////        node.GetCondition()->Accept(*this) ;
//////    }
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriRange, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIRANGE VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////
//////    // []'s are printed in the caller of range
////////    _ofs << "[" ;
//////    if (node.GetLeft()) node.GetLeft()->Accept(*this) ;
//////    if (node.GetPartSelectToken()) {
//////        _ofs << gStringValue(node.GetPartSelectToken()) << " " ; // Verilog 2000 and later
//////    } else {
//////        _ofs << ":" ; // Verilog 95
//////    }
//////    if (node.GetRight()) node.GetRight()->Accept(*this) ;
////////    _ofs << "]" ;
//////
//////    // Print subsequent dimensions also :
//////    if (node.GetNext()) {
//////        // but first close previous range and open new one :
//////        _ofs << "][" ;
//////        node.GetNext()->Accept(*this) ;
//////    }
}
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriDataType, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "VERIDATATYPE VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
//////    if (!_bFileGood) return ; // file stream is not good
//////    if (node.GetType()) _ofs << gStringValue(node.GetType()) << " " ;
//////    if (node.GetSigning()) _ofs << gStringValue(node.GetSigning()) << " " ;
//////    if (node.GetDimensions()) {
//////        _ofs << "[" ;
//////        node.GetDimensions()->Accept(*this) ;
//////        _ofs << "] " ;
//////    }
}
//////
///////*-----------------------------------------------------------------*/
////////        Visit Methods : Class details in VeriConstVal.h
///////*-----------------------------------------------------------------*/
//////
void VerificVerilogDesignWalker::VERI_VISIT(VeriConst, node)
{
  (void)node;
//////#ifdef DEBUG_VERIFIC_VERILOG_WALKER
//////    std::cerr << "VERICONST VISIT CALLED" << std::endl;
//////#endif
//////    // Do nothing
}
//////
//////

void VerificVerilogDesignWalker::VERI_VISIT(VeriTimeUnit, node)
{
  (void)node;
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "---------------------------------------------------------------" << std::endl;
    std::cerr << "NUCLEUSBUILDER VERITIMEUNIT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    SourceLocator sourceLocation = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    NUModule* module = mNucleusBuilder->gModule(getNUScope(&node));// Get parent module

    // convert the value string into an integer
    VeriExpression* veTimeExpression = node.GetTimeLiteral();
    char *veTimeString = (veTimeExpression) ? veTimeExpression->Image() : NULL ;
    unsigned st = 0;
    SInt8 newVal = HdlTime::parseTimeString(veTimeString, &st );
    // apply a little arithmetic: 1,3 - report warning, 2,3 - report error
    if (st % 2) {
        getMessageContext()->TimeStringRounded(&sourceLocation, veTimeString, HdlTime::VerilogTimeUnits[(unsigned)newVal]); // this is a warning
    }
    if (st > 2) {
        getMessageContext()->UnsupportedTimeString(&sourceLocation, veTimeString); // this is an error
    }
    Strings::free(veTimeString);

    // set this timeunit/timprecision in the parent module
    unsigned time_unit_type = node.GetTimeUnitType();
    switch ( time_unit_type ){
    case VERI_TIMEUNIT:{
      module->setTimeUnit(newVal);
      // TODO the syntax 'timeunit 1ns / 1ps' is a SV 2012 syntax and is not part of SV 2005, currently it is not supported by the parser 
      break;
    }
    case VERI_TIMEPRECISION:{
      module->setTimePrecision(newVal);
      mNucleusBuilder->getDesign()->setGlobalTimePrecision(newVal);
      break;
    }
    }
    module->setTimescaleSpecified();
}

} // namespace verific2nucleus 
/*---------------------------------------------*/
