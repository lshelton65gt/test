
// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// project headers
#include "verific2nucleus/CarbonStaticElaborator.h"
#include "verific2nucleus/CarbonConstraintManager.h"
#include "verific2nucleus/VhdlCSElabVisitor.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"
#include "verific2nucleus/VhdlConstraintElaborator.h"
#include "verific2nucleus/VhdlDeadBranchRewriter.h"

// verific headers
#include "VeriVisitor.h"    // Visitor base class definition
#include "VhdlVisitor.h"    // Visitor base class definition
#include "VhdlScope.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "VhdlTreeNode.h"
#include "VeriTreeNode.h"
#include "VhdlExpression.h"
#include "VhdlName.h"
#include "VhdlCopy.h"
#include "VhdlDeclaration.h"
#include "VhdlValue_Elab.h"
#include "Array.h"
#include "VhdlSpecification.h"
#include "VhdlDataFlow_Elab.h"
#include "Set.h"

// other project headers
#include "util/UtPair.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"

// standard headers
#include <sstream>

namespace verific2nucleus {

using namespace Verific;

//=====================================================================================
// Constructor
VhdlDeadBranchRewriter::VhdlDeadBranchRewriter(CarbonConstraintManager *cm, CarbonStaticElaborator* se) 
    : VhdlCSElabVisitor(cm, se)
    , mVisited()
{
}

//=====================================================================================
// Utilities


// Determine whether subprogram body has already been visited.
bool VhdlDeadBranchRewriter::alreadyVisited(VhdlSubprogramBody* body)
{
    bool visited = true;
    if (mVisited.find(body) == mVisited.end()) {
        visited = false;
        mVisited.insert(body);
    }
    return visited;
}

bool VhdlDeadBranchRewriter::nullifyDeadAltStmtsAndVisitLiveAltStmts(VhdlCaseStatement* node, VhdlCaseStatementAlternative* live_alt)
{
    VhdlCaseStatementAlternative* alt = NULL;
    unsigned i = 0;
    FOREACH_ARRAY_ITEM(node->GetAlternatives(), i, alt) {
        if (!alt) continue;
        if (alt == live_alt) {
            // Traverse statements (we may still have embedded conditional statements on live branch)
            TraverseArray(alt->GetStatements()) ;
        } else {
            // Remember that we need to clear the statements.
            rememberNullifyChildStmts(alt, alt->GetStatements());
        }
    }
    return true;
}

void VhdlDeadBranchRewriter::rememberNullifyChildStmts(VhdlTreeNode* parent_node, Array* statements)
{
    mNullifyList.push_back(ParentChildPair(parent_node, statements));
}

bool VhdlDeadBranchRewriter::doNullifyChildStmts()
{
    bool status = true;
    UtList<ParentChildPair>::iterator i;
    for (i = mNullifyList.begin(); i != mNullifyList.end(); ++i) {
        ParentChildPair cur_pair = *i;
        VhdlTreeNode* parent_node = cur_pair.first;
        Array* statements = cur_pair.second;
        bool cur_status = Verific2NucleusUtilities::nullifyChildStmts(parent_node, statements);
        if (!cur_status) {
            UtString msg("Unable to nullify child statements");
            reportFailure(parent_node, msg);
        }
        status &= cur_status;
    }
    mNullifyList.clear();
    return status;
}

void VhdlDeadBranchRewriter::doWalkSubprogramBody(VhdlSubprogramBody* subprog_body, Array* args)
{
    // Perform setup items.
    mEnableSubprogramBodyWalk = true;

    walkSubprogramBody(subprog_body, args);
}

bool VhdlDeadBranchRewriter::finish()
{
    // Perform cleanup items; delete the statements.
    bool status = doNullifyChildStmts();
    return status;
}

bool
VhdlDeadBranchRewriter::isSupportedConstCaseCond(VhdlValue* case_cond)
{
    // FD TODO: revisit this as soon as composite aggreagate constants reworked in population, for now skip composites (VhdlExpression produced by CreateConstantVhdlExpression function - population flow isn't ready to handle), 
    // FD Thinks: constant string literals "UUUUUUUUU" seems to be working by magic code at VerificVhdlExpressionWalker.cpp:400 (hasxzwhere u and others get replace with '1'), oppositely when populating aggregate composite type of the form ('U', 'U', 'U' ...) this version having simulation diff with string literal version, probably because this magic replacement doesn't occur. I think this part needs serious rework/rewrite, so for now skipping replacing such constants during dead branch rewrite (to make dependent cases not to regress)
    // There are 2 sensitive cases available at test/rushc/vhdl/beacon_sequential/case{203,204}
    return case_cond && case_cond->IsConstant() && (! case_cond->IsComposite());
}


//=====================================================================================
///@name Visitors
///TODO add branch pruning for case statement (similar to if statement)

// This visitor overrides VhdlCSElabVisitor::VHDL_VISIT(VhdlIfStatement, node) (as well VhdlElsif) implementation (in fact partly it's copied) and doesn't have a call to it (so has no dependency) 
void VhdlDeadBranchRewriter::VHDL_VISIT(VhdlIfStatement, node)
{
    bool replaced = false;
    // Create a container for dataflows. Data flows are stored in an array
    // for ease of subsequent cleanup.
    Array data_flows(2);
    VhdlDataFlow* curDF = mDataFlow;

    bool short_circuited = false; // True if subsequent branches are guaranteed dead

    // True if any branch previous to the one being analyzed 
    // had a constant 'true' condition. Used for VhdlDataFlow
    // management.
    bool previous_visited_branch = false;

    // Process 'if' branch
    VhdlTreeNode *stmt =NULL;
    if (Verific2NucleusUtilities::branchIsLive(node.GetIfCondition(), curDF, &short_circuited)) {
        if (short_circuited /* if condition was constant true */ && !previous_visited_branch) {
            // This is the only live branch, no new dataflow required. 
        } else if (!Verific2NucleusUtilities::isArrayEmpty(node.GetIfStatements())) {
            // Create new branch data flow
            mDataFlow = Verific2NucleusUtilities::setupBranchDataFlow(&data_flows, curDF);
        }
        // Visit if statements
        previous_visited_branch = true; // Forces new dataflows to be created
        unsigned i = 0;
        VhdlStatement* stmt = NULL;
        FOREACH_ARRAY_ITEM(node.GetIfStatements(), i, stmt) {
            if (!stmt) continue;
            stmt->Accept(*this);
        }
        if (short_circuited) {
            // set condition for 'if' to true, 'if' statements remain unchanged
            VhdlInteger* boolean_1 = new VhdlInteger(1);
            boolean_1->SetLinefile(node.GetIfCondition()->Linefile()->Copy());
            replaced = node.ReplaceChildExpr(node.GetIfCondition(), boolean_1);
            if (!replaced) {
                UtString msg("Unable to replace child expression of if statement");
                reportFailure(&node, msg);
            }
        } else {
          // Since we're not replacing the conditional with a constant, we need to
          // visit it (in case it has function calls).
          if (node.GetIfCondition())
            node.GetIfCondition()->Accept(*this);
        }
    } else {
        // set condition for 'if' to false and clear statements (replace child statements list with NULL)
        VhdlInteger* boolean_0 = new VhdlInteger(0);
        boolean_0->SetLinefile(node.GetIfCondition()->Linefile()->Copy());
        replaced = node.ReplaceChildExpr(node.GetIfCondition(), boolean_0);
        if (!replaced) {
            UtString msg("Unable to replace child expression of if statement");
            reportFailure(&node, msg);
        }
        // Remember that we need to clear the statements.
        rememberNullifyChildStmts(&node, node.GetIfStatements());
    }

    VhdlExpression* elsif_cond = NULL;
    unsigned i;
    VhdlElsif* elsif = NULL;
    // Process 'elsif' branches
    FOREACH_ARRAY_ITEM(node.GetElsifList(), i, elsif) {
        if (!elsif) continue;
        elsif_cond = elsif->Condition();
        // Analyze call tree in 'elsif' condition
        if (Verific2NucleusUtilities::branchIsLive(elsif_cond, curDF, &short_circuited)) {
            if (short_circuited) {
                // set condition for elsif to true, elsif statements remain unchanged
                VhdlInteger* boolean_1 = new VhdlInteger(1);
                boolean_1->SetLinefile(elsif_cond->Linefile()->Copy());
                replaced = elsif->ReplaceChildExpr(elsif_cond, boolean_1);
                if (!replaced) {
                    UtString msg("Unable to replace child expression of elsif statement");
                    reportFailure(&node, msg);
                }
            } else {
              // Since we're not replacing the elsif conditional with a constant, we need to
              // visit it (in case it has function calls).
              if (elsif_cond)
                elsif_cond->Accept(*this);
            }
            if (short_circuited /* elsif condition constant true */ && !previous_visited_branch) {
                // This is the only live branch, no new dataflow required
            } else if (!Verific2NucleusUtilities::isArrayEmpty(elsif->GetStatements())) {
                // Create new branch data flow elsif (if any statement)
                mDataFlow = Verific2NucleusUtilities::setupBranchDataFlow(&data_flows, curDF);
            }
            // visit elsif statements (if any)
            previous_visited_branch = true; // Forces new dataflows to be created
            TraverseArray(elsif->GetStatements()) ;
        } else {
            // set condition for 'elsif' to false and clear statements (replace child statements list with NULL)
            VhdlInteger* boolean_0 = new VhdlInteger(0);
            boolean_0->SetLinefile(elsif_cond->Linefile()->Copy());
            replaced = elsif->ReplaceChildExpr(elsif_cond, boolean_0);
            if (!replaced) {
                UtString msg("Unable to replace child expression of elsif statement");
                reportFailure(&node, msg);
            }
            // Remember that we need to clear the statements.
            rememberNullifyChildStmts(elsif, elsif->GetStatements());
        }
    }

    // Process 'else' branch, if it hasn't been short circuited
    if (!short_circuited) {
        if (!previous_visited_branch) {
            // This is the ONLY live branch, no new dataflow required
        } else if (!Verific2NucleusUtilities::isArrayEmpty(node.GetElseStatments())) {
            // We have an 'else' with statements, so create a new branch dataflow
            mDataFlow = Verific2NucleusUtilities::setupBranchDataFlow(&data_flows, curDF);
        }

        // Visit else statements
        FOREACH_ARRAY_ITEM(node.GetElseStatments(), i, stmt) {
            if (!stmt) continue;
            stmt->Accept(*this);
        }
    } else {
        // Remember that we need to clear the statements.
        rememberNullifyChildStmts(&node, node.GetElseStatments());
    }

    // Restore top dataflow, and clean up
    mDataFlow = curDF;
    Verific2NucleusUtilities::cleanUpDataFlows(&node, &data_flows, curDF);
}

// This visitor overrides VhdlCSElabVisitor::VHDL_VISIT(VhdlCaseStatement, node) implementation (in fact partly it's copied) and doesn't have a call to it (so has no dependency) 
void VhdlDeadBranchRewriter::VHDL_VISIT(VhdlCaseStatement, node)
{
    VhdlExpression* case_expr = node.GetExpression();
    VhdlCaseStatementAlternative* live_alt = NULL;
    VhdlCaseStatementAlternative* others = NULL;
    UtList<VhdlCaseStatementAlternative*> live_alts;
    // Evaluate the case expression, to determine which VhdlCaseAlernative
    // is 'live'. If the expression is a constant, then we can figure out
    // which case alternative is 'live', and visit statements in that one only. 
    // If case expression is constant:
    // 1. All constant choices get replaced with their evaluated equivalents
    // 2. it gets replaced with it's evaluated equivalent
    // 3. Afterward all statements under dead branches gets nullified
    // NOTE: for 'others' choice (default case item) we do nullify only statements if it's dead
    VhdlValue* cond = case_expr ? case_expr->Evaluate(0, mDataFlow, 0) : NULL;
    bool replaced = true;
    if (isSupportedConstCaseCond(cond)) {
        // We can determine which case alternative is 'live'.
        VhdlCaseStatementAlternative* alt;
        unsigned i;
        FOREACH_ARRAY_ITEM(node.GetAlternatives(), i, alt) {
            if (!alt) continue;
            // Save away the 'others' choice, in case it is the only 'live' alternative
            if (alt->IsOthers()) {
                others = alt;
                continue; // nothing more needs to be done for others
            }
            // Loop through the choices
            //    when choice1 | choice2 | choice3 => statement
            unsigned j;
            VhdlDiscreteRange* choice;
            FOREACH_ARRAY_ITEM(alt->GetChoices(), j, choice) {
                if (!choice) continue;
                VhdlValue* choice_val = choice->Evaluate(0, mDataFlow, 0);
                // According to the LRM (seciton 8.8), the choices expressions must be evaluatable to a constant at
                // elaboration time. Furthermore, they must be mutually exclusive
                // so asserting here
                INFO_ASSERT(choice_val && choice_val->IsConstant(), "Unable to evaluate choice value to constant");
                // TODO: Handle ranges here, there is a testcase related test/vhdl/CarbonStaticElab/case_range.vhd
                // Figure out if this choice matches case expression.
                // If so, save it for later processing.
                if (choice_val->IsEqual(cond)) {
                    live_alts.push_back(alt);
                }
                // replace all choices with evaluated version
                VhdlExpression* const_choice = choice_val->CreateConstantVhdlExpressionInternal(choice->Linefile()->Copy(),
                        choice->GetId() ? choice->GetId()->Type() : NULL 
                        /*TODO: check with population what other types we support*/, -1/*we probably don't have dimension*/, 1/*without_other*/);
                replaced = alt->ReplaceChildDiscreteRange(choice, const_choice);
                if (!replaced) {
                    UtString msg("Unable to replace choice of case statement alternative");
                    reportFailure(&node, msg);
                }
                delete choice_val;
            }
        }
        // Make sure there was not more than one 'live' choice.
        // I don't think this can happen; but am being defensive.
        if ((live_alts.size() > 1)) {
            UtString msg("Did not expect more than one 'live' choice for case statement");
            reportFailure(&node, msg);
            return;
        } 

        // determine 'live_alt' 
        if (live_alts.size() > 0) {
            live_alt = live_alts.front();
        } else {
            // If we didn't find a 'live' choice, use the 'others'.
            INFO_ASSERT(others, "Unexpected NULL 'others' clause in case statement");
            live_alt = others;
        }
        // case statement condition replacement with evaluated constant
        VhdlExpression* const_cond = cond->CreateConstantVhdlExpressionInternal(case_expr->Linefile()->Copy(), case_expr->GetId() ? case_expr->GetId()->Type() : NULL, -1, 1/*without other, popoulation is not ready to handle other as a constant*/);
        replaced = node.ReplaceChildExpr(case_expr, const_cond);
        if (!replaced) {
            UtString msg("Unable to replace child expression of case statement");
            reportFailure(&node, msg);
        }
        // loop over alternatives and do replace all statements under non 'live_alt' branches with NULL;
        // visit live branche statements as they still can contain other conditional statements (if/elif/else , case)
        replaced = nullifyDeadAltStmtsAndVisitLiveAltStmts(&node, live_alt);
        if (!replaced) {
            UtString msg("Unable to nullify dead case statement alternative branch");
            reportFailure(&node, msg);
        }
    } else {
        // Non-constant case expression, we can't determine which case item
        // is 'live', so visit all of them, separate dataflow for each.
        // But before doing so, visit the case expression.
        if (case_expr)
          case_expr->Accept(*this);
        Array data_flows(node.GetAlternatives()->Size() + 1);
        VhdlDataFlow* curDF = mDataFlow;
        VhdlCaseStatementAlternative* alt;
        unsigned j;
        FOREACH_ARRAY_ITEM(node.GetAlternatives(), j, alt) {
            if (!alt) continue;
            mDataFlow = Verific2NucleusUtilities::setupBranchDataFlow(&data_flows, curDF);
            alt->Accept(*this);
        }

        // Invalidate id values that were set in branches with non-const conditions
        mDataFlow = curDF;
        Verific2NucleusUtilities::cleanUpDataFlows(&node, &data_flows, curDF);
    }
    delete cond;
}

void VhdlDeadBranchRewriter::VHDL_VISIT(VhdlSubprogramBody, node)
{
    if (mEnableSubprogramBodyWalk) {
        mEnableSubprogramBodyWalk = false;
        // Call base class visitor 
        VhdlVisitor::VHDL_VISIT_NODE(VhdlSubprogramBody, node);
    }
}

void VhdlDeadBranchRewriter::VHDL_VISIT(VhdlOperator, node)
{
    // First, process left and right (if any) expressions. They may contain
    // function calls.
    if (node.GetLeftExpression()) {
        node.GetLeftExpression()->Accept(*this);
    }
    if (node.GetRightExpression()) {
        node.GetRightExpression()->Accept(*this);
    }

    // Next, check to see if this is a user supplied overloaded operator
    VhdlIdDef* opId = node.GetOperator();
    if (Verific2NucleusUtilities::isNonIeeeOverloadedOperator(opId)) {

        // Get uniquified subprogram body (should be available on parse-tree after VhdlSubprogCallUniquifier phase)
        // Subprogram body (uniquified) should already exist here (don't need to lookup via signature) so just error report if some inconsistency
        //TODO may wish to add consistency check with signature=>VhdlSubprogramBody
        ///VhdlSubprogramBody* subprog_body = m_cm->lookupSubprogramBody(sig);
        VhdlSubprogramBody* subprog_body = opId->Body();
        if (!subprog_body) {
            UtString msg("Unable to get operator body");
            reportFailure(&node, msg);
            return;
        }

        // Now walk the subprogram body
        if (!alreadyVisited(subprog_body)) {
            // Create an arguments array 
            Array *args = new Array();
            if (node.GetLeftExpression()) {
                args->Insert(node.GetLeftExpression());
            }
            if (node.GetRightExpression()) {
                args->Insert(node.GetRightExpression());
            }

            // Now traverse the overloaded operator declaration
            doWalkSubprogramBody(subprog_body, args);

            delete args;
        }

    }

}

void VhdlDeadBranchRewriter::VHDL_VISIT(VhdlIndexedName, node) 
{
    // First, process all associations. They may contain function calls themselves.
    unsigned i = 0;
    VhdlTreeNode* assoc = 0;
    FOREACH_ARRAY_ITEM(node.GetAssocList(), i, assoc) {
        if (assoc) assoc->Accept(*this);
    }

    // We need to visit the prefix for cases like: funcall(arg1)(3 downto 0)
    if (node.GetPrefix()) node.GetPrefix()->Accept(*this);

    // If it's a function or procedure call (but not one of the builtin functions)
    if (isTraversableSubprog(&node)) {
        // Get uniquified subprogram body (should be available on parse-tree after VhdlSubprogCallUniquifier phase)
        //TODO may wish to add consistency check with signature=>VhdlSubprogramBody
        //VhdlSubprogramBody* subprog_body = m_cm->lookupSubprogramBody(sig);
        VhdlSubprogramBody* subprog_body = node.GetId()->Body();
        if (!subprog_body) {
            UtString msg("Unable to get subprogram body");
            reportFailure(&node, msg);
            return;
        }

        // Now walk the subprogram body
        if (!alreadyVisited(subprog_body)) {
            Array *args = new Array;
            Verific2NucleusUtilities::getSubprogramArgs(args, &node, true /* use defaults */);
            doWalkSubprogramBody(subprog_body, args);
            delete args;
        }
    } 
}

// Vhdl Loop Unrolling (by now, there shouldn't be any loops that can be unrolled.)
//
// Note that we do not create a sub VhdlDataFlow object if the loop is unrolled,
// because no conditional control flow is created by an unrolled loop. 
void VhdlDeadBranchRewriter::VHDL_VISIT(VhdlLoopStatement, node)
{
    walkLoopStatementAfterLoopRewrite(&node);
}

void VhdlDeadBranchRewriter::reportFailure(VhdlTreeNode* node, const UtString& msg) {
  getCarbonStaticElaborator()->reportFailure(node, "VhdlDeadBranchRewriter", msg); // typeid(*this).name()->c++filt will return the class name also
  mErrorDuringWalk = true;
}

} // namespace verific2nucleus
