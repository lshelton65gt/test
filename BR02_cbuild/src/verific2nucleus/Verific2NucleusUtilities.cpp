// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

//Statndard headers
#include <string.h>
#include <iostream>
#include <sstream>

// Project headers
#include "verific2nucleus/VerificNucleusBuilder.h"
#include "verific2nucleus/V2NDesignScope.h"
#include "verific2nucleus/VerificDesignManager.h"
#include "verific2nucleus/VerificVerilogDesignWalker.h"
#include "verific2nucleus/VerificVhdlDesignWalker.h"
#include "verific2nucleus/VerificTicProtectedNameManager.h"
#include "compiler_driver/CarbonContext.h"

// Verific headers
#include "LineFile.h"
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes

// Header for this class
#include "verific2nucleus/Verific2NucleusUtilities.h"

// Verific headers
#include "VeriModuleItem.h"
#include "veri_tokens.h" // VERI_SYS_CALL_FOPEN
#include "vhdl_tokens.h"
#include "VhdlValue_Elab.h"
#include "VhdlVisitor.h"
#include "VhdlMisc.h"
#include "VhdlSpecification.h"
#include "VhdlStatement.h"

// Nucleus headers
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUTF.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUCompositeNet.h"

//#define DEBUG_VERIFIC2NUCLEUS_UTILITIES

namespace verific2nucleus {


#ifdef CDB
/*-----------------------------------------------------------------*/
//                      GDB Debugging Support
/*-----------------------------------------------------------------*/

  void vhdl_pp(VhdlTreeNode *n)
  {
    if (n) {
      std::ostringstream s;
      n->PrettyPrint(s, 0);
      if (strlen(s.str().c_str()) > 0) {
        UtIO::cout() << s.str().c_str() << UtIO::endl;
      } else {
        UtIO::cout() << "No pretty print output" << UtIO::endl;
      }
    }
    else {
      UtIO::cout() << "Null VhdlTreeNode" << UtIO::endl;
    }
  }

  void verilog_pp(VeriTreeNode *n)
  {
    if (n) {
      std::ostringstream s;
      n->PrettyPrint(s, 0);
      if (strlen(s.str().c_str()) > 0) {
        UtIO::cout() << s.str().c_str() << UtIO::endl;
      } else {
        UtIO::cout() << "No pretty print output" << UtIO::endl;
      }
    }
    else {
      UtIO::cout() << "Null VeriTreeNode" << UtIO::endl;
    }
  }
  
  // the method 'verific_pp' can take pointer to either a VhdlTree node or a VeriTreeNode.
  void verific_pp(VhdlTreeNode *n)  {vhdl_pp(n);    } // pointer arg
  void verific_pp(VeriTreeNode *n)  {verilog_pp(n); } // pointer arg
#endif



  VerificNucleusBuilder* Verific2NucleusUtilities::smVerificNucleusBuilder = NULL;
  VerificDesignManager* Verific2NucleusUtilities::smVerificDesignManager = NULL;
  VerificVerilogDesignWalker* Verific2NucleusUtilities::smVerificVerilogDesignWalker = NULL;
  VerificVhdlDesignWalker* Verific2NucleusUtilities::smVerificVhdlDesignWalker = NULL;
  
  SourceLocator Verific2NucleusUtilities::gSourceLocator(VerificNucleusBuilder& nb, Verific::linefile_type lf)
  {
      int lineNumber = lf->GetLeftLine();
      UtString fileName;
      if (nb.gTicProtectedNameMgr()->isTicProtected(lf)) {
          // if coming from tick protected region apply Interra trick to file name, changing string like this 
          // e.g. fileName = protect3.vp
          //      lineNumber = 12
          // After applying trick:
          //      fileName="-protect3.vp:12"
          //      lineNumber=12
          fileName << "-" << lf->GetFileName() << ":" << lineNumber;
      } else {
          fileName = lf->GetFileName();
      }
      return nb.gSourceLocFactory().create(fileName.c_str(),lineNumber);
  }

  SourceLocator Verific2NucleusUtilities::getSourceLocator(CarbonContext* carbonContext, Verific::linefile_type lf)
  {
      int lineNumber = lf->GetLeftLine();
      UtString fileName;
      if (carbonContext->getVerificTicProtectedNameManager()->isTicProtected(lf)) {
          // if coming from tick protected region apply Interra trick to file name, changing string like this 
          // e.g. fileName = protect3.vp
          //      lineNumber = 12
          // After applying trick:
          //      fileName="-protect3.vp:12"
          //      lineNumber=12
          fileName << "-" << lf->GetFileName() << ":" << lineNumber;
      } else {
          fileName = lf->GetFileName();
      }
      return carbonContext->getSourceLocatorFactory()->create(fileName.c_str(),lineNumber);
  }



  NUNet* Verific2NucleusUtilities::gModuleNet(NUModule* cur_module, UtString net_name,
          bool case_insensitive //vhdl case insensitive
          ){
      INFO_ASSERT(cur_module, "null module pointer");
      NUNetList* net_list = new NUNetList;
      UtString net_name_local = net_name;
      cur_module -> getAllTopNets(net_list);    
      for (Loop<NUNetList> p(*net_list); !p.atEnd(); ++p) {
          NUNet* net = *p;
          UtString path;	
          net->getNameLeaf()->verilogCompose(&path);
          if (case_insensitive){
              path.lowercase();
              net_name_local.lowercase();
          }
          if (path == net_name) {
              delete net_list;
              return net;
          }
      }
      delete net_list;
      return NULL;
  }

// Given an entity instantiation, find and return a pointer to the instantiated entity or module.
//
// Note, the fact that VHDL_PRESERVE_COMPONENT_INSTANCE is not set in VhdlCompileFlags.h guarantees
// that all component instantiations are converted into entity instantiations. This means that
// the libraray in which the entity is declared is readily available on the instance, and we
// don't have to implement complex binding rules.
//
// Returns NULL for both 'e' and 'm' if no bound entity/module was found.
void 
Verific2NucleusUtilities::getInstantiatedEntityOrModule(VhdlComponentInstantiationStatement* inst, VhdlEntityDecl** e, VeriModule** m)
{
#ifdef VHDL_PRESERVE_COMPONENT_INSTANCE
  // If we decide to define VHDL_PRESERVE_COMPONENT_INSTANCE, this method will have to be extended
  // to support entity lookups using the VHDL binding/search order rules.
  INFO_ASSERT(false, "The implementation of this function assumes VHDL_PRESERVE_COMPONENT_INSTANCE is not defined.");
#endif

  // Initialize return values
  *e = NULL;
  *m = NULL;
  
  // Get library name, and find Verific library object
  const char* lib_name = inst->GetLibraryOfInstantiatedUnit();
  VhdlLibrary* lib = vhdl_file::GetLibrary(lib_name);
  INFO_ASSERT(lib, "Unexpected NULL library");
  // Get the unit name, and look it up in the library.
  const char* unit_name = inst->GetInstantiatedUnitName();
  VhdlPrimaryUnit* unit = lib->GetPrimUnit(unit_name);
  // If found figure out whether it's verilog or vhdl, and return ptr
  if (unit) {
    if (unit->IsVerilogModule()) {
      *m = vhdl_file::GetVerilogModuleFromlib(lib_name, unit_name);
    } else {
      *e = dynamic_cast<VhdlEntityDecl*>(unit);
    }
  }
}
  
NUModule* Verific2NucleusUtilities::gNUModule(const UtString& moduleName, const UtString& name, Verific::VhdlComponentInstantiationStatement* ve_stmt)
  {
      NUModule* nModule = dynamic_cast<NUModule*>(smVerificNucleusBuilder->gModule(VerificNucleusBuilder::gNameAsNucleusName(moduleName)));
      if (!nModule) {
          INFO_ASSERT(ve_stmt, "null VhdlComponentInstantiationStatement");
          VhdlEntityDecl* e = NULL;
          VeriModule* m = NULL;
          getInstantiatedEntityOrModule(ve_stmt, &e, &m);
          if (e) {
            e->Accept(*smVerificVhdlDesignWalker);
          } else if (m) {
            m->Accept(*smVerificVerilogDesignWalker);
          } else {
            UtString msg;
            msg << "Can't find module with name " << ve_stmt->GetInstantiatedUnitName() << " in Verific";
            INFO_ASSERT(false, msg.c_str());
          }
          nModule = dynamic_cast<NUModule*>(smVerificNucleusBuilder->gModule(VerificNucleusBuilder::gNameAsNucleusName(moduleName)));
          INFO_ASSERT(nModule, UtString("failed to find module with name ").append(name.c_str()).c_str());
      }
      return nModule;
  }


  NUModule* Verific2NucleusUtilities::gNUModule(Verific::VeriModuleInstantiation* mod_inst)
  {
      UtString uniqueModuleName;
      if (mod_inst->GetInstantiatedModule()) {
          uniqueModuleName = smVerificNucleusBuilder->gModuleName(mod_inst->GetInstantiatedModule());
      }
#ifdef DEBUG_VERIFIC2NUCLEUS_UTILITIES
      Verific::VeriModule *module ;
      Verific::MapIter mi ;
      std::cout << "ALL VERILOG MODULES IN VERIFIC" << std::endl;
      FOREACH_VERILOG_MODULE(mi, module) {
          std::cerr << module->Name() << std::endl;
      }
      Verific::MapIter m1, m2, m3 ;
      Verific::VhdlLibrary *lib ;
      Verific::VhdlPrimaryUnit *primunit ;
      Verific::VhdlSecondaryUnit *secunit ;
      FOREACH_VHDL_LIBRARY(m1, lib) {
          // Iterate over all (primary unit) parse-trees that were analyzed
          FOREACH_VHDL_PRIMARY_UNIT(lib, m2, primunit) {
              std::cerr << "VHDL ENTITY: " << primunit->Name() << std::endl;
              // Iterate over all (secondary unit) parse-trees analyzed under this primary unit
              FOREACH_VHDL_SECONDARY_UNIT(primunit, m3, secunit) {
                  std::cerr << "VHDL ARCH: " << secunit->Name() << std::endl;
              }
          }
      }
#endif
      UtString msg;
      // Detect correct name of the module
      Verific::VeriModule *mod = mod_inst->GetInstantiatedModule();
      Verific::VhdlPrimaryUnit *vhdl_unit = NULL;
      const char* modName = NULL;
      UtString arch_name;
      UtString lib_name;
      if (mod) {
#ifdef DEBUG_VERIFIC2NUCLEUS_UTILITIES
          std::cerr << "Instance is Verilog Module" <<  std::endl;
#endif
          if (uniqueModuleName.empty()) {
              modName = mod->Name();
          } else {
              modName = uniqueModuleName.c_str();
          }
      } else {
#ifdef DEBUG_VERIFIC2NUCLEUS_UTILITIES
          std::cerr << "Instance is not Verilog Module" <<  std::endl;
#endif
          Verific::VeriName *instantiated_unit_name = mod_inst->GetModuleNameRef();
          const char *entity_name = instantiated_unit_name ? instantiated_unit_name->GetName() : 0 ;
          Verific::VhdlLibrary *lib ;
          Verific::MapIter mi ;
          FOREACH_VHDL_LIBRARY(mi, lib) {
              // Find the vhdl unit for verilog instance
              char* verificStringArchName = 0;
              vhdl_unit = Verific::VeriNode::GetVhdlUnitForVerilogInstance(entity_name, lib->Name(), 0/*search_from_loptions*/, &verificStringArchName) ;
              if (vhdl_unit) {
                  lib_name = UtString(lib->Name());
                  if (verificStringArchName) {
                      // Verific modifies parse-tree and generates module names which include concrete arch_name like child(arch1),
                      // child(arch2), and in that case the GetVhdlUnitForVerilogInstance will return the verificStringArchName from
                      // modified module name.  Make a copy we can use here, and free the string that parser provided
                      arch_name = UtString(verificStringArchName);
                      Strings::free(verificStringArchName);
                  } else {
                      // if arch_name not returned by GetVhdlUnitForVerilogInstance function then there must be situation
                      // where there is no ambiguity around architecture name and there should be only one architecture for that entity.
                      Map *sec_units = vhdl_unit->AllSecondaryUnits();
                      INFO_ASSERT(sec_units->Size() == 1, "More than one secondary units");
                      arch_name = UtString(static_cast<char*>(sec_units->GetItemAt(0)->Key()));
                  }
                  break;
              }
          }
          if (vhdl_unit) {
#ifdef DEBUG_VERIFIC2NUCLEUS_UTILITIES
          std::cerr << "Instance is VHDL Module" <<  std::endl;
#endif
              modName = vhdl_unit->Id()->Name();
          }
      }
#ifdef DEBUG_VERIFIC2NUCLEUS_UTILITIES
      std::cerr << "Detected module Name: " << modName << std::endl;
#endif
     NUModule* nModule = NULL;
      if (modName != NULL) {
        nModule = dynamic_cast<NUModule*>(smVerificNucleusBuilder->gModule(VerificNucleusBuilder::gNameAsNucleusName(modName)));
      }
      if ((nModule == NULL) && (modName != NULL)) {
          UtString module_full_name;
          if (mod) { // This is verilog module insatantiation: need to go into that module
              mod->Accept(*smVerificVerilogDesignWalker);
              module_full_name = VerificNucleusBuilder::gNameAsNucleusName(modName);
          } else if (vhdl_unit != NULL) {
              vhdl_unit->Accept(*smVerificVhdlDesignWalker);
              // For vhdl we need to check both names (simple entity and full_name), full_name = lib_name + "_" + entity_name + "_" + arch_name
              // We need to match with population, so strip out _default suffix from entity if any
              UtString entity_name(modName);
          //TODO need to check
          if( !smVerificNucleusBuilder->hasSpecialCharacter(entity_name) ) {
              size_t pos = entity_name.find_last_of("_default");
              if (UtString::npos != pos) {
                  entity_name = entity_name.substr(0, entity_name.size() - 8);
              }
          }
          module_full_name = lib_name + "_" + entity_name + "_" + arch_name;
          }
          //check simple name (entity name)
          nModule = dynamic_cast<NUModule*>(smVerificNucleusBuilder->gModule(VerificNucleusBuilder::gNameAsNucleusName(modName)));
          if (!nModule) {
              // check full name
              nModule = dynamic_cast<NUModule*>(smVerificNucleusBuilder->gModule(VerificNucleusBuilder::gNameAsNucleusName(module_full_name)));
          }
      }
      return nModule;
  }

  NUModule* Verific2NucleusUtilities::gNUModule(Verific::VeriModule* mod)
  {
#ifdef DEBUG_VERIFIC2NUCLEUS_UTILITIES
      Verific::VeriModule *module ;
      Verific::MapIter mi ;
      std::cout << "ALL VERILOG MODULES IN VERIFIC" << std::endl;
      FOREACH_VERILOG_MODULE(mi, module) {
          std::cerr << module->Name() << std::endl;
      }
#endif
      UtString msg;
      // Detect correct name of the module
      INFO_ASSERT(mod, "Null module object");
      const char* modName = NULL; 
      UtString uniqueModuleName = smVerificNucleusBuilder->gModuleName(mod);
      if (uniqueModuleName.empty()) {
          modName = mod->Name();
      } else {
          modName = uniqueModuleName.c_str();
      }
#ifdef DEBUG_VERIFIC2NUCLEUS_UTILITIES
      std::cerr << "Detected module Name: " << modName << std::endl;
#endif
      NUModule* nModule = dynamic_cast<NUModule*>(smVerificNucleusBuilder->gModule(VerificNucleusBuilder::gNameAsNucleusName(modName)));
      if (!nModule) {
          mod->Accept(*smVerificVerilogDesignWalker);
          nModule = dynamic_cast<NUModule*>(smVerificNucleusBuilder->gModule(VerificNucleusBuilder::gNameAsNucleusName(modName)));
          INFO_ASSERT(nModule, UtString("failed to find module with name ").append(modName).c_str());
      }
      return nModule;
  }

  void Verific2NucleusUtilities::InitObjects(VerificNucleusBuilder* nb, VerificDesignManager* dm,
      VerificVerilogDesignWalker* veWalker, VerificVhdlDesignWalker* vhWalker)
  {
      INFO_ASSERT(nb, "Null nucleus builder object");
      INFO_ASSERT(dm, "Null design manager object");
      INFO_ASSERT(veWalker, "Null verilog walker object");
      INFO_ASSERT(vhWalker, "Null vhdl walker object");
      smVerificNucleusBuilder = nb;
      smVerificDesignManager = dm;
      smVerificVerilogDesignWalker = veWalker;
      smVerificVhdlDesignWalker = vhWalker;
  }
  
// To make escaped verilog names legal, the Verific2NucleusUtilities::createLegalVerilogIdentifier
// routine is called which converts these names to legal Vhdl names. The resultant
// names however, are not legal Verilog names.
// The following function takes in a name and if it is enclosed withing back-slashes
// i.e is of the form "\\string\\" then removes these back-slashes and returns a legal
// Verilog name. If the input is already a legal Verilog name then it remains same.
void Verific2NucleusUtilities::createLegalVerilogIdentifier(UtString& moduleName) {
    if((*moduleName.begin() == '\\' ) and (*moduleName.rbegin() == '\\') ) {
      moduleName.erase(moduleName.rbegin());
      moduleName.erase(moduleName.begin());
  }
}

// Determine whether a for loop will generate a named declaration scope.
// The rule is this: if the for loop has declarations in the initializers,
// a named declaration scope is created.
bool Verific2NucleusUtilities::forLoopHasDecls(VeriFor& node)
{
  bool hasDecls = false;
  unsigned i;
  VeriModuleItem *item ;
  FOREACH_ARRAY_ITEM(node.GetInitials(), i, item) {
    if (item && item->IsDataDecl()) {
      hasDecls = true;
      break;
    }
  }
  return hasDecls;
}

// This visitor class traverses the Verific parse tree
// looking for statements with side effects, i.e. statements
// that require the Verific Nucleus datastructures to be
// populated with more than 1 statement. At present, the only
// known cases are:
//
//    1) function calls (see comments for VeriFunctionCall visitor)
//    2) $fopen calls
// 
// This class is a helper for
// the hasSideEffectStatements method,
// but can be used in other contexts.
  
class VerificParseTreeSearch : public VeriVisitor
{
public:
  VerificParseTreeSearch() : mFunctionCallCount(0), mFOpenCount(0) {};
  virtual ~VerificParseTreeSearch() {};

  virtual void VERI_VISIT(VeriFunctionCall, node) 
  {
    (void) node;
    mFunctionCallCount++;
  };
  virtual void VERI_VISIT(VeriSystemFunctionCall, node) 
    {if (VERI_SYS_CALL_FOPEN == node.GetFunctionType()) mFOpenCount++;};

  UInt32 getSideEffectStmtCount(void) {return mFunctionCallCount + mFOpenCount;};
private:
  ///@name Prevent the compiler from implementing the following
  VerificParseTreeSearch(VerificParseTreeSearch &node);
  VerificParseTreeSearch& operator=(const VerificParseTreeSearch &rhs);

private:
  UInt32 mFunctionCallCount;
  UInt32 mFOpenCount;
};
  
// This function examines assignment statements, looking for
// statements with side effects in the right hand side expression.
//
// For example, if the supplied statement is
//
//     lhs = funccall(rhs);
//
// it would return true. Ditto for the following:
//
//     lhs = rhs1 * (rhs2 / funccall(rhs3));
//
bool
Verific2NucleusUtilities::hasSideEffectStmt(const VeriStatement* s)
{
  bool sideEffectStmtsFound = false;

  VeriExpression* rhs = NULL;
  if (s->GetClassId() == ID_VERIBLOCKINGASSIGN) {
    const VeriBlockingAssign* a = dynamic_cast<const VeriBlockingAssign*>(s);
    rhs = a->GetValue();
  } else if (s->GetClassId() == ID_VERINONBLOCKINGASSIGN) {
    const VeriNonBlockingAssign* a = dynamic_cast<const VeriNonBlockingAssign*>(s);
    rhs = a->GetValue();
  }

  if (rhs) {
    VerificParseTreeSearch sideEffectSearcher;
    rhs->Accept(sideEffectSearcher);
    sideEffectStmtsFound = (sideEffectSearcher.getSideEffectStmtCount() > 0);
  }

  return sideEffectStmtsFound;
}

// Function needed to identify the following situations:
// 1) Verilog function has explicit 'void' datatype - (if datatype is 'VERI_VOID' then return true to indicate void function)
// 2) verilog function has some sort of type information defined for it (size,type,signedness), in this case return false
//    to indicate non-void function.
// 3) function has no explicit 'void', and has a return value
//     (either as a 'return  n' or an assignment to variable that has name as function), return false to indicate non-void function
//    in 95/2001 mode this appears as null datatype, but for SV can be detected only by analysis
// 4) function has no explicit 'void', but has no 'return' statement and no assignment to variable
//    that has same name as function, in this case return true to indicate void function
bool 
Verific2NucleusUtilities::isVoidFunction(VeriFunctionDecl* node)
{
  // Verific has no easy way to check this, so have to go with 'hard' way
  VeriDataType* dataType = node->GetDataType();
  if (dataType) {
    // Check if explicit 'void' datatype specified
    if (dataType->GetType() == VERI_VOID) {
      return true; 
    }
    // If we have at least one of 'type', 'dimensions' or 'signing' specified then this isn't void function
    if (dataType->GetType() || dataType->GetDimensions() || dataType->GetSigning()) {
      return false;
    }
  }
  // at this point there may, or may not be any value for dataType, but the fact that it is null is insufficient to
  // establish that this is a void function.  Instead we must examine the internals of the function:

  // traverse function internals to see if there is some return statement or assignment to variable with same name as function
  // if there is none, then we will declare it 'void'
  struct CheckReturnValue : VeriVisitor {
    CheckReturnValue(const UtString& name) : mHasReturnValue(false), mFuncName(name) {}
    virtual void VERI_VISIT(VeriJumpStatement, node)
    {
      if (node.GetJumpToken() == VERI_RETURN) {
        mHasReturnValue = true;
      }
    }
    virtual void VERI_VISIT(VeriBlockingAssign, node)
    {
      if (node.GetLVal() && node.GetLVal()->GetId()) {
        // Check left value to match-up function name
        if (mFuncName == node.GetLVal()->GetId()->GetName()) {
          mHasReturnValue = true;
        }
      }
    }
    bool mHasReturnValue;
    UtString mFuncName;
  };
  VeriIdDef* func_id = node->GetFunctionId();
  CheckReturnValue crv(func_id->GetName());
  node->Accept(crv);
  return ! crv.mHasReturnValue;
}

/*------------------------------------------------------------------------------------------------------------------*/
//                                             Scope get api's
/*------------------------------------------------------------------------------------------------------------------*/
// get declaration scope, if no named declaration scope, task, module scope found return NULL
NUScope*
Verific2NucleusUtilities::getDeclarationScope(V2NDesignScope* currentScope)
{
    NUScope* declaration_scope = NULL;
    // Proceed upward until we find a named declaration scope, or until
    // we get to a module/task 
    bool found = false;
    for (V2NDesignScope* scope = currentScope; scope && !found; scope = scope->gParentScope()) {
        // If there is a named declaration scope
        if (scope->gDeclarationScope()) {
            // we are done declaration scope found
            declaration_scope = dynamic_cast<NUScope*>(scope->gDeclarationScope());
            found = true;
        } else if (scope->gOwner()) {
            // validate that scope type is task or module
            NUScope* nu_scope = scope->gOwner();
            NUScope::ScopeT nu_scope_type = nu_scope->getScopeType();
            if ((NUScope::eTask == nu_scope_type) || (NUScope::eModule == nu_scope_type)) {
                declaration_scope = nu_scope;
                found = true;
            }
        }
    }
    return declaration_scope;
}

// get block scope, if no block, task, module scope found return NULL
NUScope*
Verific2NucleusUtilities::getBlockScope(V2NDesignScope* currentScope)
{
    NUScope* block_scope = NULL;
    // Proceed upward until we find a block scope, or until
    // we get to a module/task, if not found return NULL
    bool found = false;
    for (V2NDesignScope* scope = currentScope; scope && !found; scope = scope->gParentScope()) {
        if (scope->gOwner()) {
            NUScope* nu_scope = scope->gOwner();
            // validate that scope type is one of the following - block, task, module
            NUScope::ScopeT nu_scope_type = nu_scope->getScopeType();
            if ((NUScope::eBlock == nu_scope_type) || (NUScope::eTask == nu_scope_type) || (NUScope::eModule == nu_scope_type)) {
                block_scope = nu_scope;
                found = true;
            }
        }
    }
    return block_scope;
}

// get parent unit scope (task or module scope), if no unit scope found return NULL
NUScope*
Verific2NucleusUtilities::getUnitScope(V2NDesignScope* currentScope)
{
    NUScope* unit_scope = NULL;
    // Proceed upward until we find module/task
    bool found = false;
    for (V2NDesignScope* scope = currentScope; scope && !found; scope = scope->gParentScope()) {
        if (scope->gOwner()) {
            // check if scope type is module or task
            NUScope* nu_scope = scope->gOwner();
            NUScope::ScopeT nu_scope_type = nu_scope->getScopeType(); 
            if ((NUScope::eModule == nu_scope_type) || (NUScope::eTask == nu_scope_type)) {
                unit_scope = nu_scope;
                found = true;
            }
        }
    }
    return unit_scope;
}

// get parent module scope if no module scope found return NULL
NUScope*
Verific2NucleusUtilities::getModuleScope(V2NDesignScope* currentScope)
{
    NUScope* module_scope = NULL;
    // Proceed upward until we find module
    bool found = false;
    for (V2NDesignScope* scope = currentScope; scope && !found; scope = scope->gParentScope()) {
        if (scope->gOwner()) {
            NUScope* nu_scope = scope->gOwner();
            NUScope::ScopeT nu_scope_type = nu_scope->getScopeType();
            // check if scope type is module
            if (NUScope::eModule == nu_scope_type) {
                module_scope = nu_scope;
                found = true;
            }
        }
    }
    return module_scope;
}


NUNamedDeclarationScope*
Verific2NucleusUtilities::getNamedDeclarationScope(V2NDesignScope* currentScope)
{
    NUScope* declaration_scope = getDeclarationScope(currentScope);
    if (declaration_scope && (NUScope::eNamedDeclarationScope == declaration_scope->getScopeType())) {
        return dynamic_cast<NUNamedDeclarationScope*>(declaration_scope);
    }
    return 0;
}

// This method gets the nearest scope. If it's a block scope, return it.
// Otherwise, return NULL.
NUBlock*
Verific2NucleusUtilities::getBlock(V2NDesignScope* currentScope)
{
    // Find nearest scope (block, task, module) 
    NUScope* block_scope = getBlockScope(currentScope);
    // If this is a block scope, return it, else return NULL
    if (block_scope && (NUScope::eBlock == block_scope->getScopeType())) {
        return dynamic_cast<NUBlock*>(block_scope);
    }
    return 0;
}

// This method gets the nearest scope. If it's a task scope, return it.
// Otherwise, return NULL.
NUTask*
Verific2NucleusUtilities::getTask(V2NDesignScope* currentScope)
{
    NUScope* unit_scope = getUnitScope(currentScope);
    if (unit_scope && (NUScope::eTask == unit_scope->getScopeType())) {
        return dynamic_cast<NUTask*>(unit_scope);
    }
    return 0;
}

// Get parent module scope; if none, return NULL.
// RBS thinks: why is this different from getModuleScope(currentScope)?
NUModule*
Verific2NucleusUtilities::getModule(V2NDesignScope* currentScope)
{
    NUScope* module_scope = getModuleScope(currentScope);
    if (module_scope && (NUScope::eModule == module_scope->getScopeType())) {
        return dynamic_cast<NUModule*>(module_scope);
    }
    return 0;
}

/*------------------------------------------------------------------------------------------------------------------*/

//! Brief Return true if \a id corresponds to something declared in the IEEE library
// TODO: Do we need to check for upper case IEEE too?
bool 
Verific2NucleusUtilities::isIeeeDecl(VhdlIdDef* id)
{
  if (!id)
    return false;
  UtString libName(id->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit()->Owner()->Name());
  return (libName == "ieee");
}

//! Brief Return true if \a id corresponds to something declared in the STD library
bool
Verific2NucleusUtilities::isStdDecl(VhdlIdDef* id)
{
  if (!id)
    return false;
  UtString libName(id->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit()->Owner()->Name());
  return (libName == "std");
}

//! Brief Return true if \a id is a subprogram declared in an IEEE library.
bool 
Verific2NucleusUtilities::isIeeeSubprogram(VhdlIdDef* id)
                                                                     {
  if (!isIeeeDecl(id))
    return false;
  return id->GetClassId() == ID_VHDLSUBPROGRAMID;
}

//! Brief Return true if \a opId is an overloaded operator that is NOT 
//  part of the built-in IEEE library.
bool 
Verific2NucleusUtilities::isNonIeeeOverloadedOperator(VhdlIdDef* opId)
{
  bool result = opId && !opId->IsPredefinedOperator() && !isIeeeSubprogram(opId);
  return result;
}

//! Brief Return true if \a id is a 'system' subprogram, i.e. predefined, builtin,
//  and without a subprogram body.
//
//  These are:
//
//  readline, read, oread, hread
//  writeline, write, owrite, hwrite
//
// These and only these are declared in the std library textio pkg.
//

bool
Verific2NucleusUtilities::isStdTextioSubprogram(VhdlIdDef* sId)
{
  bool result = false;
  
  if (!isStdDecl(sId))
    return false;
  else {
    // We know it's declared in the std library. Now look for textio package
    VhdlScope* containing_scope = sId->GetOwningScope() ? sId->GetOwningScope() : sId->LocalScope();
    // First, see if it's actually declared in a package
    if (containing_scope->GetContainingPrimaryUnit()->GetPrimaryUnit()->IsPackageDecl()) {
      // Get package name
      UtString pkgName(containing_scope->GetContainingPrimaryUnit()->GetPrimaryUnit()->Name());
      if (pkgName == "textio")
        result = true;
    }
  }

  return result;
}

// Return true if this is the "rising_edge" or "falling_edge" function declared
// in ieee.std_logic_1164.
bool
Verific2NucleusUtilities::isIeeeEdgeDetectFunction(VhdlIdDef* sId)
{
  bool result = false;
  
  if (isIeeeDecl(sId)) {
    // We know it's declared in the ieee library. Now look for std_logic_1164 package
    VhdlScope* containing_scope = sId->GetOwningScope() ? sId->GetOwningScope() : sId->LocalScope();
    // First, see if it's actually declared in a package
    if (containing_scope->GetContainingPrimaryUnit()->GetPrimaryUnit()->IsPackageDecl()) {
      // Get package name
      UtString pkgName(containing_scope->GetContainingPrimaryUnit()->GetPrimaryUnit()->Name());
      if (pkgName == "std_logic_1164") {
        UtString funcName(sId->Name());
        if (funcName == "rising_edge" || funcName == "falling_edge")
          result = true;
      }
    }
  }

  return result;
}


// Get subprogram argument values, with option to fill in unspecified values with default value
// expression.
//
// NOTE: For procedures, the Verific elaborator (at least with the settings we're using now) turns a named 
// association list into a positional list, with NULL values for parameters that have not been specified.
// For functions, the same, but if the unspecified values are at the end,
// there will NOT be NULL values in the positional list.

void 
Verific2NucleusUtilities::getSubprogramArgs(Array* arguments, VhdlIndexedName* call_inst, bool get_dflt)
{
  VhdlSubprogramId* sId = dynamic_cast<VhdlSubprogramId*>(call_inst->GetId());
  
  unsigned i = 0;
  VhdlTreeNode* item = 0;
  FOREACH_ARRAY_ITEM(call_inst->GetAssocList(), i, item) {
    if (item) {
      VhdlDiscreteRange* assoc = dynamic_cast<VhdlDiscreteRange*>(item);
      VhdlExpression* actual = assoc->ActualPart();
      arguments->Insert(actual);
    } else if (get_dflt) {
      arguments->Insert(getInitialAssign(i, sId));
    } else {
      arguments->Insert(NULL);
    }
  }

  // If requested, fill unspecified arguments at the end with default value expression
  if (get_dflt) {
    unsigned numFormals = sId->NumOfArgs();
    unsigned numActuals = call_inst->GetAssocList()->Size();
    if (numFormals > numActuals) {
      for (unsigned i = numActuals; i < numFormals; ++i) {
        arguments->Insert(getInitialAssign(i, sId));
      }
    }
  }
  
  // Print arguments in readable form (for debugging)
  if (0) { // Debug
    for (unsigned i = 0; i < arguments->Size(); ++i) {
      VhdlDiscreteRange *expr = static_cast<VhdlDiscreteRange*>(arguments->At(i));
      if (expr == NULL) {
        UtIO::cout() << "DEBUG: (" << i << ") null" << UtIO::endl;
      } else {
        std::ostringstream s;
        expr->PrettyPrint(s, 0);
        UtIO::cout() << "DEBUG: (" << i << ") " << s.str().c_str() << UtIO::endl;
      }
    }
  }
}

// Give the (0 based) position of a formal VHDL subprogram parameter, find the 
// initial value assignment (if any) for that parameter.
VhdlExpression* 
Verific2NucleusUtilities::getInitialAssign(unsigned idx, VhdlSubprogramId* sId)
{
  VhdlExpression* initAssign = NULL;
  
  VhdlSubprogramBody* sBody = sId->Body();
  // INFO_ASSERT(sBody, "failed to get body of subprogram");
  VhdlSpecification* spec = sBody->GetSubprogramSpec();
  // INFO_ASSERT(spec, "Invalid VhdlFunctionSpec node");
  unsigned l = 0;
  unsigned pos = 0;
  VhdlInterfaceDecl* interfaceDecl = 0;
  FOREACH_ARRAY_ITEM(spec->GetFormalParamList(), l, interfaceDecl) {
    if (pos > idx) break; // We're done
    // Each interface declaration may consist of multiple ids, all with
    // the same type, and same initial value. Count through the
    // ids for this interface declaration.
    unsigned m = 0;
    VhdlIdDef* id = 0;
    FOREACH_ARRAY_ITEM(interfaceDecl->GetIds(), m, id) {
      if (pos == idx) {
        initAssign = interfaceDecl->GetInitAssign();
      }
      pos++;
    }
  }
  
  return initAssign;
}

// Specialized helper function to retrieve integer range bounds, and put them into SInt32 variables.
// Returns 'true' if both ends of the range are constant integers, 'false' otherwise.
bool
Verific2NucleusUtilities::getConstantRangeBounds(VhdlValue* left, VhdlValue* right, SInt32* left_bound, SInt32* right_bound)
{
  bool is_const = false;
  bool left_is_const_int = left ? left->IsIntVal() : false;
  // Fill in the left bound, if requested
  if (left_is_const_int) {
    *left_bound = left->Integer();
  }
  bool right_is_const_int = right ? right->IsIntVal() : false;
  // Fill in right bound, if requested
  if (right_is_const_int) {
    *right_bound = right->Integer();
  }
  is_const = (left_is_const_int && right_is_const_int);
  return is_const;
}

// Return true if a range exists and has integer constant bounds. 
// Optionally fill in the integer bounds information.
// Useful for loop unrolling.
bool
Verific2NucleusUtilities::isConstantRange(VhdlDiscreteRange* range, VhdlDataFlow* df, SInt32* left_bound, SInt32* right_bound)
{
  bool is_const = false;
  if (range) {
    VhdlExpression* left_expr = range->GetLeftExpression();
    VhdlExpression* right_expr = range->GetRightExpression();
    if (left_expr && right_expr) {
      // This range is of the form "leftexpr to rightexpr"
      VhdlValue* left = left_expr ? left_expr->Evaluate(0, df, 0) : NULL;
      VhdlValue* right = right_expr ? right_expr->Evaluate(0, df, 0) : NULL;
      is_const = getConstantRangeBounds(left, right, left_bound, right_bound);
      delete left;
      delete right;
    } else {
      // This range is of the form f'range, where f is a variable, function, signal, constant, or some other vhdl object.
      // TODO: At present, we don't yet handle functions that return types with unconstrained ranges. To do this, we'll need
      // to look at the signature map.
      VhdlConstraint *constraint = range->EvaluateConstraintInternal(df, 0);
      if (constraint && !constraint->IsUnconstrained()) {
        VhdlRangeConstraint *range_constraint = dynamic_cast<VhdlRangeConstraint*>(constraint);
        INFO_ASSERT(range_constraint, "Expected a range constraint for for loop iteration scheme");
        VhdlValue* left = range_constraint->Left();
        VhdlValue* right = range_constraint->Right();
        is_const = getConstantRangeBounds(left, right, left_bound, right_bound);
      }
      delete constraint;
    }
  }
  return is_const;
}

// Return true if a VHDL loop will be unrolled.

// Loops are unrolled so that subprograms within them
// can be populated correctly. If a subprogram has 
// unconstrained formals with arguments whose widths
// depend on the loop index, then we have to unroll the
// loop to populate the subprogram call instances correctly.
//
// For example:
//
//    function foo(in1 : std_logic_vector) return std_logic_vector is
//       <definition of function foo here>
//    end foo;
//
//    variable vec : std_logic_vector(15 downto 0);
//    for i in 0 to 15 loop
//        count := foo(vec(i downto 0));
//    end loop;
//
// Since nucleus requires subprograms (tasks) to be populated with
// fixed size arguments, the loop above results in 16 different
// copies of 'foo', with input argument sizes equal to 1 through
// 16.
//
// The criteria for deciding to unroll a loop is as follows:
//
// + The loop must have bounds, and they must be constant. We
//   cannot unroll while loops, or other loops without iteration
//   schemes.
//
// + We only need to unroll loops that have a call
//   (somewhere in the call hierarchy) to a subprogram that has
//   unconstrained arguments and/or return value. Note that
//   to correctly determine this we have to dive deep into subprogram
//   call hierarchy. It is not sufficient to look at the top level calls
//   within the loop.
//
//   This implementation, though, is simple minded. It just 
//   examines the top level statements within a loop,
//   looking for a subprogram (or overloaded operator).
//   This will cause us to be overly agressive about unrolling
//   loops. We will be unrolling loops that we technically 
//   don't need to unroll. The justification for this is that
//   the penalty for unrolling a loop is presumed to be small.
// 
//   Loops without a function call do not need to be unrolled. 
//
// + The loop should not have too many iterations. For now, this is
//   determined by a member variable in the VhdlLoopUnrollAnalyzer class.
// 
// NOTE: If this method returns true, it does not necessarily mean
// the loop MUST be unrolled for all subprogram constraints to be
// calculated correctly. It only means that the user asked
// us to unroll the loop, and/or that it is a good candidate
// for unrolling.

bool
Verific2NucleusUtilities::loopShouldBeUnrolled(VhdlLoopStatement* loop, VhdlDataFlow* df, bool forceUnroll)
{

  class VhdlLoopUnrollAnalyzer : public VhdlVisitor
  {
  public:
    VhdlLoopUnrollAnalyzer(VhdlDataFlow* df)
    : mDataFlow(df)
    , mIsUnrollCandidate(false)
    , mIsPossibleToUnroll(true) 
    , mIterLimit(1024) // currently hardcoded, default iteration upper limit
    {
    }
    ~VhdlLoopUnrollAnalyzer(void) {};

  public:
    // Main entry point. Returns true if loop should be unrolled
    bool analyzeLoop(VhdlLoopStatement *loop, bool forceUnroll)
    {
      INFO_ASSERT(loop, "Unexpected NULL argument to VhdlLoopUnrollAnalyzer");
      mIsUnrollCandidate = false;
      mIsPossibleToUnroll = true; // default to true, unless we found some unsupported statement
      VhdlForScheme* fScheme = dynamic_cast<VhdlForScheme*>(loop->GetIterScheme());
      // We cannot unroll loops that don't have an iteration scheme
      if (fScheme) {
        //FD thinks: what about single iteration loops (it seems loop unroll old implementation (in population) unrolls as well single iteration ranges (when there is no range)
        VhdlDiscreteRange* range = fScheme->GetRange();
        // Determine whether it's a constant range, and get the bounds
        SInt32 left_bound = 0, right_bound = 0;
        bool const_range = isConstantRange(range, mDataFlow, &left_bound, &right_bound);
        // Make sure the loop constant bounds, and is not too many iterations
        if (range && const_range && (std::abs(left_bound - right_bound) <= mIterLimit)) {
          // Loop is allowed to be unrolled. Traverse statements in loop, looking for constructs
          // that would warrant unrolling. If found, set mIsUnrollCandidate to 'true'.
          // Note that we don't want to visit the iteration scheme. Function calls there
          // should not trigger unrolling.
          unsigned i = 0;
          VhdlTreeNode* item = 0;
          FOREACH_ARRAY_ITEM(loop->GetStatements(), i, item) {
            if (!item) continue;
            item->Accept(*this);
          }
        } else {
            // We never going to unroll the loop with dynamic bounds or in case it exceeds predefined limit (even if forced by user)
            mIsPossibleToUnroll = false;
        }
      } else {
          // We don't support loop unroll for other than for loop schemes
          mIsPossibleToUnroll = false;
      }

      // We'll unroll the loop if it's possible to do so, and it's a good candidate for unrolling,
      // or the user asked us to do so.
      bool willBeUnrolled = mIsPossibleToUnroll && (mIsUnrollCandidate || forceUnroll);
      return willBeUnrolled;
    }

    // Look for functions, procedures
    void VHDL_VISIT(VhdlIndexedName, node)
    {
      if (node.IsFunctionCall()) {
        // This represents a function or procedure call
        mIsUnrollCandidate = true;
      }

      if (!mIsUnrollCandidate) {
        // Look for subprogram calls in the 'arguments'
        unsigned i = 0;
        VhdlTreeNode* item = 0;
        FOREACH_ARRAY_ITEM(node.GetAssocList(), i, item) {
          if (item) {
            item->Accept(*this);
          }
        }
      }
      if (! mIsUnrollCandidate ){
        // if there is a dynamic range of select where the bound(s) is defined by a loop variable then unrolling the loop will allow the range
        // to become a static range.  The following bit of code checks for situations where a range is defined by a loop variable, if found
        // then suggest the loop be unrolled. (e.g. test/rushc/vhdl/beacon_misc/misc.size5)
        if ( node.IsArraySlice() ){
          // look to see if either index expression is a non-constant, if so then we will indicate that the loop should be unrolled with the
          // hope that the non-constant index expression(s) depend on the loop variable and will be resolved by the unroll process
          unsigned ii = 0;
          VhdlTreeNode* item = 0;
          FOREACH_ARRAY_ITEM(node.GetAssocList(), ii, item) {
            VhdlRange *range = dynamic_cast<VhdlRange*>(item);
            if ( NULL != range ){
              mIsUnrollCandidate = (! range->GetLeftExpression()->IsConstant()) || (! range->GetRightExpression()->IsConstant());
            }
          }
        }
      }
    }

    
    // Look for overloaded operators
    void VHDL_VISIT(VhdlOperator, node)
    {
      VhdlIdDef* opId = node.GetOperator();
      if (Verific2NucleusUtilities::isNonIeeeOverloadedOperator(opId)) {
        mIsUnrollCandidate = true;
      }

      if (!mIsUnrollCandidate) {
        // Look for subprograms in the left/right operator expressions
        if (node.GetLeftExpression()) node.GetLeftExpression()->Accept(*this);
        if (node.GetRightExpression()) node.GetRightExpression()->Accept(*this);
      }
    }
    // NOTE: that below cases still possible to unroll although currently unsupported in CarbonStaticElaborater phase
    // Some algorithm alredy implemented in BreakResynth phase, so actual unrolling will be done there (after nucleus population)
    void VHDL_VISIT(VhdlReturnStatement, node)
    {
    // FD: We need to examine  only statements inside loop directly, for now assuming we don't go deep into functions body 
    // (in order not to mixed with function return statement), if someday base walker or this walker is changed todo so, 
    // the case should be filtered here
        (void)node;
        mIsPossibleToUnroll = false;
    }
    
    void VHDL_VISIT(VhdlExitStatement, node)
    {
        (void)node;
        mIsPossibleToUnroll = false;
    }
    
    void VHDL_VISIT(VhdlNextStatement, node)
    {
        (void)node;
        mIsPossibleToUnroll = false;
    }
    
  private:
    VhdlDataFlow* mDataFlow;   // Used for constant calculations
    bool mIsUnrollCandidate;   // True if we find a good reason to unroll this loop (e.g. finding a subprogram call or overloaded operator)
    bool mIsPossibleToUnroll;  // True if this is a loop that could be unrolled (and false if it cannot for example, FOR loop with a non
                               // static bound, or too large a iteration count will be marked False)
    SInt32 mIterLimit;         // Upper limit on the number of iterations allowed for unrolling
  };
  
  // Invoke the walker
  VhdlLoopUnrollAnalyzer walker(df);
  return walker.analyzeLoop(loop, forceUnroll);
}

// Helper method to find all ids that are assigned within a loop, and to invalidate them
// in the parent dataflow. Called before visiting statements within a unrollable loop.
void
Verific2NucleusUtilities::findAndInvalidateAssignedIdsInLoop(VhdlDataFlow* parent_df, VhdlLoopStatement* loop)
{
  // Helper class to find ids on lhs of an assignment 
  // or connected to a subprogram output
  class AssignedIdFinder : public VhdlVisitor 
  {
  public:
    AssignedIdFinder(VhdlDataFlow* parent_df) : mParentDf(parent_df), mInLhs(false) {};

    // Variable assignments invalidate the id on the LHS. 
    // See notes in VhdlCSElabVisitor.h for VhdlVariableSignalAssignment
    // visitor (which we're not currently using to update dataflow).
    virtual void VHDL_VISIT(VhdlVariableAssignmentStatement, node)
    {
      mInLhs = true;
      TraverseNode(node.GetTarget());
      mInLhs = false;
    }

    // Id's connected to procedure output or inout must be invalidated too.
    virtual void VHDL_VISIT(VhdlIndexedName, node)
    {
      // Visit prefix
      TraverseNode(node.GetPrefix());
      
      // Note that we do not visit the associations. We don't invalidate
      // indices on the LHS, e.g.
      //   vec(i) := '0';
      // 'i' will not get invalidated.

      // Check if procedure call.
      if (node.IsFunctionCall() && node.GetId() && node.GetId()->IsProcedure()) {
        // This is a procedure call. Visit all outputs.
        VhdlIdDef* subprog_id = node.GetId();
        VhdlIdDef* id;
        VhdlDiscreteRange* assoc;
        unsigned i;
        FOREACH_ARRAY_ITEM(node.GetAssocList(), i, assoc) {
          if (!assoc) continue;
          // Get formal id associated with the 'assoc'
          if (assoc->IsAssoc()) {
            id = assoc->FindFullFormal();
            if (!id) 
              break;
          } else {
            id = subprog_id->Arg(i);
          }
          // Now that we have a formal id, see if it's an output. 
          if (id && (id->IsOutput() || id->IsInout())) {
            mInLhs = true;
            VhdlDiscreteRange* actual = assoc->ActualPart();
            if (actual) {
              if (0) // Debug
                UtIO::cout() << "DEBUG: Encountered subprogram " << subprog_id->Name() << " output " << verific2String(*actual) << UtIO::endl;
              // It's an output. Visit it.
              // NOTE, if we get this far, we cannot be on the left hand side
              // of an assignment.
              mInLhs = true;
              actual->Accept(*this);
              mInLhs = false;
            }
          }
        }
      }
    }

    // If a VhdlIdRef is on the LHS, then invalidate
    // it in the parent dataflow. Note that we
    // could be smarter here if we wanted to
    // recognize cases in which the id is
    // assigned the same value it already had.
    // If a literal is assigned, then we can compare
    // old and new values, and avoid invalidating
    // if they are the same. We have to be careful though
    // if the RHS is an expression. The value of the
    // expression may be known during this analysis, but
    // may change during execution of the loop.
    // We'd probably also have to climb up the stack
    // of dataflows, and compare against assigned values
    // to the id all the way up.
    virtual void VHDL_VISIT(VhdlIdRef, node)
    {
      VhdlIdDef* id = node.GetId();
      if (mInLhs && id) {
        // Invalidate the ID in the parent df.
        // InvalidateIdValues will propagate this up
        // through all owner dataflows.
        mParentDf->SetAssignValue(id,0);
      }
    }
 
  private:
    VhdlDataFlow* mParentDf;
    bool mInLhs;
  };

  // Body of function here
  AssignedIdFinder finder(parent_df);
  loop->Accept(finder);
}

// Setup dataflow for a loop that cannot be unrolled.
//
// Constant values of id's are inherited from parent.
// But any of these ids that appear on the lhs of
// an assignment within the loop are invalidated.
//
// This is designed to accurately handle cases like 
// this:
//
// flag := true;
// n := 7;
// for i in 0 to limit loop -- NOT UNROLLED
//    -- We should be smart enough to know that
//    -- flag is not guaranteed to always be true
//    -- here (at least not for loop iterations beyond
//    -- the first).
//    if (flag) then
//       output_val <= '1';
//    else if (input_value) then
//       flag := false;
//    else
//       for j in 0 to n loop
//         -- We should be smart enough to know
//         -- that n is 7 here.
//       end loop;
//    end if;
// end loop; 
//

VhdlDataFlow*
Verific2NucleusUtilities::setupLoopDataFlow(Array* data_flows, VhdlDataFlow* parent_df, VhdlLoopStatement* loop)
{
  // Find all ids that are assigned within the loop,
  // and invalidate them in the parent dataflow.
  findAndInvalidateAssignedIdsInLoop(parent_df, loop);

  // Now set up sub dataflow
  VhdlDataFlow* loop_df = new VhdlDataFlow(parent_df);
  loop_df->SetNonconstExpr();
  data_flows->InsertLast(loop_df);
  return loop_df;
}

VhdlDataFlow*
Verific2NucleusUtilities::setupBranchDataFlow(Array* data_flows, VhdlDataFlow* parent_df)
{
    VhdlDataFlow* branchDataFlow = new VhdlDataFlow(parent_df);
    branchDataFlow->SetNonconstExpr();
    Map* condition_id_value_map = 0; // DO WE NEED THIS?
    branchDataFlow->SetConditionIdValueMap(condition_id_value_map); // DO WE NEED THIS
    data_flows->InsertLast(branchDataFlow);
    return branchDataFlow;
}

// Cleanup dataflow object, used by for, if, case statements
void
Verific2NucleusUtilities::cleanUpDataFlows(VhdlStatement* node, Array* data_flows, VhdlDataFlow* curDF)
{
  // InvalidateIdValues sets to NULL any ids in curDF that have been assigned in a sub dataflow.
  // Have to make sure the data_flows array has items in it, otherwise dead code flag
  // will inadvertantly get set to true.
  if (data_flows && data_flows->Size() > 0)
    curDF->InvalidateIdValues(data_flows, node);
  VhdlDataFlow* sub_df = 0;
  unsigned l = 0;
  // FD I think here we do clear all dataflows (unclear about reasons behind this)
  FOREACH_ARRAY_ITEM(data_flows, l, sub_df) {
    delete sub_df;
  }
}

// get type conversion VhdlIdDef node
VhdlIdDef* 
Verific2NucleusUtilities::getConvSubprogramPrefix(const VhdlName& name)
{
  VhdlName* local_name_p = const_cast<VhdlName*>(&name);

  while (local_name_p->GetAssocList()){
      // If the VhdlName has indices/ranges, strip them off.
    local_name_p = local_name_p->GetPrefix();
  }

    if (local_name_p->Name()) {
        // simple name like "foo"
        return local_name_p->GetId();
    } else if (ID_VHDLSELECTEDNAME == local_name_p->GetPrefix()->GetClassId()) {
        // still a selected name like "a.b.c" or even "a.b(1).c", in both cases we want to return the id def for 'c'
        VhdlIdDef* return_id = local_name_p->GetId();
        return return_id;
    }
    return 0;
}

//===================================================================================   
// Used for 'if' statement pruning 
// FD thinks: this should be possible to use also for case statement pruning
//
// Returns true if the statements within an if, elsif branch are
// 'live', i.e. have a possibility of being executed at simulation time.
//
// Statements within a branch are 'live' if:
//
// 1) they have not been 'short circuited', i.e. no condition prior to
//    this has been constant and true;
//
// And at least one of the following is true:
//
// 2) the if/elsif relational expression is constant and true, or
// 3) the relational expression is not constant.
//
// If condition (2) is triggered, then this branch of the conditional
// will 'short circuit' subsequent branches, and the 'short_circuited'
// flag is set to 'true'.
//
// Basically, what we want to do is avoid visiting/populating those branches
// which we know CANNOT be executed.

bool Verific2NucleusUtilities::branchIsLive(VhdlExpression *relational_expr, VhdlDataFlow* df, bool* short_circuited) 
{
    //(void)df;
    bool branch_live = false;

    if (*short_circuited) {
        // Previous branch condition precludes execution of this one
        branch_live = false;
    } else {
        VhdlValue* cond = relational_expr ? relational_expr->Evaluate(0, df, 0) : NULL;
        //VhdlValue* cond = relational_expr ? relational_expr->Evaluate(0, 0, 0) : NULL;
        if (cond && cond->IsConstant()) {
            if (cond->IsTrue()) {
                // The condition is true, so this branch can be executed.
                // Furthermore, it will preclude execution of subsequent branches.
                branch_live = true;
                *short_circuited = true;
            } else {
                // The condition must be false, in which case, this branch can't be executed
                branch_live = false;
            }
        } else {
            // Condition is not a constant, therefore can't preclude it's execution
            branch_live = true;
        }
        delete cond;
    }

    return branch_live;
}

// get type conversion subprogram name
// for example when we have call eee.std_logic_unsigned.conv_integer
// this method will return conv_integer
UtString 
Verific2NucleusUtilities::getConvSubprogramName(const VhdlIndexedName* inst)
{
    UtString sub_prog_name;
    if (inst->GetPrefix()->Name()) {
        sub_prog_name = UtString(inst->GetPrefix()->Name());
    } else if (ID_VHDLSELECTEDNAME == inst->GetPrefix()->GetClassId()) {
        VhdlName* selName = static_cast<VhdlName*>(inst->GetPrefix());
        V2N_INFO_ASSERT(selName, *inst, "invalid selected name");
        while (ID_VHDLSELECTEDNAME == selName->GetClassId()) {
            selName = selName->GetSuffix();
        }
        sub_prog_name = UtString(selName->Name());
    }
    return sub_prog_name;
}

// Return a code indicating whether the supplied function is a IEEE package 
// builtin function (>0), and if so, how the return constraint should be generated.
// It can take on the following values:
//
//   0 - Means 'unspecified', and we have to look up the algorithm from the VhdlIndexedName*
//   1 - Means the return constraint is give directly from the subprogram spec
//   2 - Means the range of the return type is given by the range of the first input
//   3 - Means the range of the return type is given by the value of the second input
//
// See CarbonConstraintManger::CreateConstraintFromBuiltinFunction for
// more info about these codes. WARNING, if additional codes are added, or codes are
// removed, CarbonConstraintManger::CreateConstraintFromBuiltinFunction must also be
// modified.
//
// An IEEE package builtin function is one that we are populating inline (i.e. 
// without a task call). This is done (presumably) for speed.

unsigned
Verific2NucleusUtilities::isIEEEPackageBuiltinFunction(VhdlIndexedName* call_inst)
{
  unsigned result = 0;

  if (isIeeeSubprogram(call_inst->GetId())) {
    UtString function_name = getConvSubprogramName(call_inst);
    const char* name = function_name.c_str();
    // These are in numeric_std.vhd, numeric_bit.vhd. 
    if      (strcasecmp(name, "to_integer") == 0)             result = 1;
    else if (strcasecmp(name, "to_unsigned") == 0)            result = 3;
    else if (strcasecmp(name, "resize") == 0)                 result = 3;
    else if (strcasecmp(name, "shift_left") == 0)             result = 2;
    else if (strcasecmp(name, "shift_right") == 0)            result = 2;
    else if (strcasecmp(name, "rotate_left") == 0)            result = 2;
    else if (strcasecmp(name, "rotate_right") == 0)           result = 2;
    else if (strcasecmp(name, "std_match") == 0)              result = 1;

    // These are in std_1164.vhd
    else if (strcasecmp(name, "to_bit") == 0)                 result = 1;
    else if (strcasecmp(name, "to_bitvector") == 0)           result = 2;
    else if (strcasecmp(name, "to_stdulogic") == 0)           result = 1;
    else if (strcasecmp(name, "to_stdlogicvector") == 0)      result = 2;
    else if (strcasecmp(name, "to_stdulogicvector") == 0)     result = 2;
    else if (strcasecmp(name, "to_x01") == 0)                 result = 2;
    else if (strcasecmp(name, "to_x01z") == 0)                result = 2;
    else if (strcasecmp(name, "to_ux01") == 0)                result = 2;
    else if (strcasecmp(name, "is_x") == 0)                   result = 1;

    // These are in syn_arit.vhd (and some are also in syn_sign.vhd and syn_unsi.vhd)
    else if (strcasecmp(name, "shl") == 0)                    result = 2;
    else if (strcasecmp(name, "shr") == 0)                    result = 2;
    else if (strcasecmp(name, "conv_integer") == 0)           result = 1;
    else if (strcasecmp(name, "conv_unsigned") == 0)          result = 3;
    else if (strcasecmp(name, "conv_signed") == 0)            result = 3;
    else if (strcasecmp(name, "conv_std_logic_vector") == 0)  result = 3;
    else if (strcasecmp(name, "ext") == 0)                    result = 3;
    else if (strcasecmp(name, "sxt") == 0)                    result = 3;
  }
  
  return result;
}

bool Verific2NucleusUtilities::isElabCreatedEmpty(VhdlStatement* stmt)
{
    if (!stmt || stmt->IsElabCreatedEmpty()) {
        return true;
    }
    if (stmt->IsBlockStatement()) {
        // if we have declaraiton inside then it's not elab created empty (even though there are no statements)
        if (stmt && stmt->GetDeclPart()) {
            return false;
        }
        bool is_elab_created_empty = true;
        unsigned i = 0;
        VhdlStatement* child_stmt = NULL;
        //iterate through all statements recursively, 
        //return true if all statements are either IsElabCreatedEmpty or are VhdlBlockStatements which contain only elabCreatedEmpty ones.
        FOREACH_ARRAY_ITEM(stmt->GetStatements(), i, child_stmt) {
            is_elab_created_empty &= isElabCreatedEmpty(child_stmt);
        }
        return is_elab_created_empty; 
    } else {
        return false;
    }
}

// Return a UtString containing a pretty print of the node.
UtString
Verific2NucleusUtilities::verific2String(const VhdlTreeNode& n)
{
  UtString result;
  std::ostringstream s;
  n.PrettyPrint(s, 0);
  result = s.str().c_str();
  return result;
}

// Utility to replace single child statement with statements array
// Verific version of this function seems not implemented (or at least not implemented for some of the statements)
bool Verific2NucleusUtilities::replaceChildStmtBy(VhdlTreeNode* parent_node, VhdlStatement* child, const Array* new_statements)
{
    bool no_error = true;
    // FD: At the moment supported only 4 versions of parent node - VhdlStatement, VhdlDeclaration, VhdlCaseStatementAlternative, VhdlElsif
    // TODO we may decide to extend this if other type of parent nodes needed
    VhdlElsif* parent_elsif_node = dynamic_cast<VhdlElsif*>(parent_node);
    VhdlCaseStatementAlternative* parent_case_statement_alternative_node = dynamic_cast<VhdlCaseStatementAlternative*>(parent_node);
    VhdlStatement* parent_statement_node = dynamic_cast<VhdlStatement*>(parent_node);
    VhdlDeclaration* parent_declaration_node = dynamic_cast<VhdlDeclaration*>(parent_node);
    INFO_ASSERT(parent_elsif_node || parent_case_statement_alternative_node || parent_statement_node || parent_declaration_node, "Unknown loop parent node type");
    // Here we allow new_statements to be null, which is interpreted to mean that the caller wants to delete child and replace it with nothing
    // 
    // The assumption is that it makes sense to traverse empty for loop, I assume for loop condition part
    // might contain some function call (with state). I'm not sure how it will be treated by nucleus.
    VhdlStatement* current_stmt = NULL;
    if ( new_statements && new_statements->Size() ) {
      current_stmt = static_cast<VhdlStatement*>(new_statements->GetFirst());
    }
    if (parent_elsif_node) {
        no_error = parent_elsif_node->ReplaceChildStmt(child, current_stmt); 
    } else if (parent_case_statement_alternative_node) {
        no_error = parent_case_statement_alternative_node->ReplaceChildStmt(child, current_stmt); 
    } else if (parent_statement_node) {
        no_error = parent_statement_node->ReplaceChildStmt(child, current_stmt);
    } else if (parent_declaration_node) {
        no_error = parent_declaration_node->ReplaceChildStmt(child, current_stmt);
    }
    unsigned j = 0;
    VhdlStatement* new_stmt = 0;
    FOREACH_ARRAY_ITEM(new_statements, j, new_stmt) {
        if (!j) continue; // skip we have already replaced first one
        if (parent_elsif_node) {
            no_error = parent_elsif_node->InsertAfterStmt(current_stmt, new_stmt); 
        } else if (parent_case_statement_alternative_node) {
            no_error = parent_case_statement_alternative_node->InsertAfterStmt(current_stmt, new_stmt); 
        } else if (parent_statement_node) {
            no_error = parent_statement_node->InsertAfterStmt(current_stmt, new_stmt); 
        } else if (parent_declaration_node) {
            no_error = parent_declaration_node->InsertAfterStmt(current_stmt, new_stmt); 
        }
        current_stmt = new_stmt;
    }
    return no_error;
}

// Helper class to find loop statements and undeclare their labels.
// Before a loop statement is deleted, their labels need to be undeclared, because
// the labels are not owned by the loop statement, but instead by parent scope.
class VerificLoopStmtUndeclareLabels : public VhdlVisitor
{
public:
    VerificLoopStmtUndeclareLabels() : mStatus(true) {}
    virtual ~VerificLoopStmtUndeclareLabels() {}
    virtual void VHDL_VISIT(VhdlLoopStatement, node)
    {
      // Make sure nested loops are found.
      VhdlVisitor::VHDL_VISIT_NODE(VhdlLoopStatement, node);

      mStatus &= Verific2NucleusUtilities::undeclareLoopLabel(&node);
    }
    bool getStatus() { return mStatus; }

private:
    ///@name Prevent the compiler from implementing the following
    VerificLoopStmtUndeclareLabels(VerificLoopStmtUndeclareLabels &node);
    VerificLoopStmtUndeclareLabels& operator=(const VerificLoopStmtUndeclareLabels &rhs);

    bool mStatus;
};

bool
Verific2NucleusUtilities::nullifyChildStmts(VhdlTreeNode* parent_node, Array* statements)
{
        // Verific seems doesn't have api to replace all statements with NULL at once, 
        // I have checked statement destructors they where doing similar to below
        // function cleanup
        // We don't delete array pointer itself, just delete/nullify elements, array size remains same with all elements being NULL 
        // Possible optimization: we could remove node.GetIfStatements() array itself at this point
    if (!statements) return true; // no replacement for no statements
    INFO_ASSERT(parent_node, "NULL parent node");
    VhdlElsif* parent_elsif_node = dynamic_cast<VhdlElsif*>(parent_node);
    VhdlCaseStatementAlternative* parent_case_statement_alternative_node = dynamic_cast<VhdlCaseStatementAlternative*>(parent_node);
    VhdlStatement* parent_statement_node = dynamic_cast<VhdlStatement*>(parent_node);
    // TODO correct comment
    INFO_ASSERT(parent_elsif_node || parent_case_statement_alternative_node || parent_statement_node, "Unknown statement array parent node type");
    bool no_error = true;
    unsigned i = 0;
    VerificLoopStmtUndeclareLabels undecl_walker;
    VhdlStatement *elem = NULL;
    FOREACH_ARRAY_ITEM(statements,i,elem) {
        if (!elem) continue;

        // Before delete anything clean up any stray labels.
        elem->Accept(undecl_walker);
        no_error &= undecl_walker.getStatus();

        if (parent_elsif_node) {
            no_error &= (parent_elsif_node->ReplaceChildStmt(elem, NULL));
        } else if (parent_case_statement_alternative_node) {
            no_error &= (parent_case_statement_alternative_node->ReplaceChildStmt(elem, NULL));
        } else if (parent_statement_node) {
            no_error &= (parent_statement_node->ReplaceChildStmt(elem, NULL));
        }
    }
    return no_error;
}

bool Verific2NucleusUtilities::undeclareLoopLabel(VhdlLoopStatement* loop_stmt)
{
    bool status = false;

    // FD: As explained in VIPER 2597 for loop scope added as a transparent scope in order to handle exit/next statements in Verific 
    // (just internal concept) standard doesn't require any scope for "VhdlLoopStatement" as nothing can be declared there
    // (except implicit for loop iterator declaration). 
    // Below code with getting declaration scope for loop scope and consequent find of label in declaration scope
    // is done to workaround label related bug in current version of Verific (in fact Verific's 2014_05
    // drop seems already fixing the issue. And if that's true just UndeclareLabel() called on parent scope will be enough 
    // (it will skip transparent scopes and undeclare the label from first non-transparent scope). In current cbuild Verific 
    // version there are several bugs related to this (transparent isn't set properly, id get's copied instead of putting reference), so the solution isn't applicable now (and so workaround applied). 
    // My view and debugging of Verific 2014_05 drops shows that those issues get fixed already by Verific
    // TODO this is already fixed by 2014_05 Verific drop so replace it with simple parent_scope->UndeclareLabel(label_id)
    VhdlScope *loop_scope = loop_stmt->LocalScope();
    VhdlScope *parent_decl_scope = getDeclarationScopeForLoopScope(loop_scope);
    // Undeclare label of loop statement. This will allow us to declare identifier
    // with label name in container scope (may be in case of empty for loop), as well avoiding stolen poitners
    if (parent_decl_scope && loop_stmt) {
        // FD thinks : I don't understand why nested loop label is declared under declaration scope 
        // (subprogram or process scope), that seems more like a bug in Verific current implementation, and in fact 
        // even loop_stmt->GetLabel() object is not the same with one declared inside 'parent_decl_scope' (they just match 
        // by name), but somehow when we replace statement this parent_decl_scope id becomes stolen, so have to search it by
        // name and undeclare before trying to call replace statement.
        //TODO check back later, after VIPER 8776 related to implicit label fixed - this part might get updated 
        VhdlIdDef *label_id = loop_stmt->GetLabel() ;
        // if no label id then nothing to remove
        if (label_id) { 
            //TODO: This is fixed in 2014_05 Verific drop (as soon as we switch to that version - no need to get label by name)
            VhdlIdDef* l1_id = parent_decl_scope->FindSingleObject(label_id->Name());
            // Undeclare label_id
            parent_decl_scope->UndeclareLabel(l1_id) ;
            // make the owner of for loop statement's local-scope as null.
            // FD thinks : Not sure how much we need this
            VhdlScope *loop_scope = loop_stmt->LocalScope() ;
            if (loop_scope) loop_scope->SetOwner(0) ;
        }
        status = true;
    } else {
        status = false;
    }
    return status;
}

VhdlScope* Verific2NucleusUtilities::getDeclarationScopeForLoopScope(VhdlScope* loop_scope)
{
    if (!loop_scope) return 0;
    VhdlScope* decl_scope = loop_scope->GetSubprogramScope();
    if (!decl_scope) {
        decl_scope = loop_scope->GetProcessScope();
    }
    return decl_scope;
}


// Given a scope created by a process or block statement, find the corresponding block or process statement within 
// the supplied list of statements. This is necessary because there doesn't seem to be any back pointer from a 
// process/block scope to the process/block statement.
VhdlStatement*
Verific2NucleusUtilities::findScopeStatement(Array* statements, VhdlScope* scope)
{
  INFO_ASSERT(scope, "Unexpected NULL scope passed to findScopeStatement");

  VhdlStatement* scope_stmt = NULL;
  VhdlTreeNode *item;
  unsigned i;
  FOREACH_ARRAY_ITEM(statements, i, item) {
    if (!item) continue;
    VhdlStatement* stmt = dynamic_cast<VhdlStatement*>(item);
    INFO_ASSERT(stmt, "Expected all items in 'statements' array to be statements");
    if (stmt->LocalScope() == scope) {
      // If this statement owns the scope, we're done.
      scope_stmt = stmt;
    } else if (stmt->GetClassId() == ID_VHDLBLOCKSTATEMENT) {
      // Since processes and blocks can be nested inside blocks statements,
      // we need to look inside for a match.
      VhdlBlockStatement* block_stmt = dynamic_cast<VhdlBlockStatement*>(stmt);
      scope_stmt = findScopeStatement(block_stmt->GetStatements(), scope);
    } else if (stmt->GetClassId() == ID_VHDLPROCESSSTATEMENT) {
      // Also, we can now have block statements nested inside process statements,
      // due to loop rewriting.
      VhdlProcessStatement* proc_stmt = dynamic_cast<VhdlProcessStatement*>(stmt);
      scope_stmt = findScopeStatement(proc_stmt->GetStatements(), scope);
    }
    if (scope_stmt)
      break;
  }

  return scope_stmt;
}

void 
Verific2NucleusUtilities::dumpParseTree(VeriModule* topModule, VhdlPrimaryUnit* topEntity, const UtString& suffix)
{
    if (topModule || topEntity) {
        UtString fileName;
        UtString topName = topModule ? topModule->Id()->Name() : topEntity->Id()->Name();
        fileName <<  topName.c_str() << "_" << suffix.c_str();
        // TODO : add libName also
        if (topModule) {
            fileName << ".v";
            Verific::veri_file::PrettyPrint(fileName.c_str(), NULL, NULL);
        } else {
            fileName << ".vhd";
            Verific::vhdl_file::PrettyPrint(fileName.c_str(), NULL, NULL);
        }
    }
}

// This debugging function was used to debug the 'memory exhausted' error
// related to _path_name management during VhdlLoopRewrite.
// Leaving it in here to help debug that issue.
void
Verific2NucleusUtilities::dumpPathName(Array *path_name)
{
  unsigned i;
  void *p;
  if (!path_name)
    UtIO::cout() << "Path empty" << UtIO::endl;
  FOREACH_ARRAY_ITEM(path_name, i, p) {
    const char* s = NULL;
    if (p) {
      s = static_cast<const char*>(p);
    } else {
      s = "(null)";
    }
    UtIO::cout() << "Path(" << i << ")=" << s << UtIO::endl;
  }
}

// Debugging function to dump all id's in a scope
void
Verific2NucleusUtilities::dumpScope(VhdlScope *scope)
{
    if (scope) {
        UInt32 count = 0;
        VhdlIdDef *id ;
        MapIter mi;
        FOREACH_MAP_ITEM(scope->GetThisScope(), mi, 0, &id) {
            MapItem* item;
            FOREACH_SAME_KEY_MAPITEM(scope->GetThisScope(), id->Name(), item) {
                VhdlIdDef* same_key_id = (VhdlIdDef*)item->Value() ;
                count++;
                if (same_key_id) {
                    INFO_ASSERT(UtString(id->Name()) == UtString(same_key_id->Name()), "incorrect id name");
                    UtIO::cout() << "Scope item(" << count << ") " << same_key_id << " : " << verific2String(*same_key_id) << UtIO::endl;
                } else {
                    UtIO::cout() << "Scope item(" << count << ") NULL " << UtIO::endl;
                }
            }
        }
        UtIO::cout() << count << " scope items found " << UtIO::endl;
    } else {
        UtIO::cout() << "NULL Scope" << UtIO::endl;
    }
}

// Debugging function to dump ids and their values in a dataflow
void
Verific2NucleusUtilities::dumpDataFlow(VhdlDataFlow* df)
{
  dumpDataFlowInternal(df, 0);
}


void
Verific2NucleusUtilities::dumpDataFlowInternal(VhdlDataFlow* df, UInt32 indent)
{
  UtString allspaces("                                                                ");
  UtString indent_str = (indent < allspaces.size() )? allspaces.substr(0,indent) : allspaces;

  if (df) {
    // Print out info for this data flow
    UtIO::cout() << indent_str << "DataFlow (" << (void*)df << ")" << UtIO::endl;
    UtIO::cout() << indent_str << "DataFlow id value area" << UtIO::endl;
    UtIO::cout() << indent_str << "======================" << UtIO::endl;
    MapIter i;
    VhdlIdDef *id;
    SInt32 index = -1;
    FOREACH_MAP_ITEM(df, i, &id, 0) {
      index++;
      if (!id) {
        UtIO::cout() << indent_str << "[" << index << "] " << "Id: is NULL" << UtIO::endl;
        continue;
      }
      VhdlValue* value = df->IdValue(id);
      char* verific_image = value ? value->Image() : 0;
      const char* image = verific_image ? verific_image : "(null)";
      UtIO::cout() << indent_str << "[" << index << "] " << "Id(" << (void*)id << "): " << id->Name() << ", Value = " << image << UtIO::endl;
      delete verific_image;
    }
    if ( index < 0 ){
      UtIO::cout() << indent_str << "DataFlow is empty" << UtIO::endl;
    }
    const char* is_dead_code = df->IsDeadCode() ? "is" : "is not";
    UtIO::cout() << indent_str << "DataFlow " << is_dead_code << " dead code" << UtIO::endl;
    
    // Now print out info for parent data flow
    if (df->Owner()) {
      UtIO::cout() << indent_str << "DataFlow Parent:" << UtIO::endl;
      dumpDataFlowInternal(df->Owner(), indent+4);
    }
  } else {
    UtIO::cout() << indent_str << "NULL DataFlow pointer" << UtIO::endl;
  }
}

void
Verific2NucleusUtilities::dumpVerilog(CarbonContext* cc, const char* fileRoot, const char* dirExtension, VeriModule* topModule, VhdlPrimaryUnit* topEntity){
  if (topModule || topEntity) {
    UtString dirName(fileRoot);
    dirName <<  "." << dirExtension;
    if ( ! cc->createDirectory(&dirName) ) {
      UtString topName = topModule ? topModule->Id()->Name() : topEntity->Id()->Name();
      UtString pathName(dirName);
      pathName << "/" <<  topName.c_str() << ".";
      if (topModule) {
        pathName << "v";
        Verific::veri_file::PrettyPrint(pathName.c_str(), NULL, NULL);
      } else {
        pathName << "vhd";
        Verific::vhdl_file::PrettyPrint(pathName.c_str(), NULL, NULL);
      }
    }
  }
}

// Return true if this 'id' (usually a block statement label) is either directly or
// indirectly nested within a subprogram or process scope.
bool
Verific2NucleusUtilities::isInProcessOrSubprogram(VhdlIdDef* id)
{
  bool process_or_subprog = false;
  
  INFO_ASSERT(id, "Unexpected NULL argument to isInProcessOrSubprogram");
  VhdlScope* owning_scope = id->GetOwningScope();
  if (owning_scope->IsProcessScope() || owning_scope->IsSubprogramScope()) {
    process_or_subprog = true;
  } else if (owning_scope->GetOwner() && owning_scope->GetOwner()->GetClassId() == ID_VHDLBLOCKID) {
    // This is a block scope. Continue looking upward for a process or subprogram scope
    process_or_subprog = isInProcessOrSubprogram(owning_scope->GetOwner());
  } else {
    // This must be an architecture body
    process_or_subprog = false;
  }
  
  return process_or_subprog;
}

// Return true if this Verific array is empty. Null array entries do not count
bool
Verific2NucleusUtilities::isArrayEmpty(Array* a)
{
  bool empty = true;
  if (a && a->Size() > 0) {
    // Make sure there are non-NULL elements
    VhdlNode *item;
    unsigned i;
    FOREACH_ARRAY_ITEM(a, i, item) {
      if (item) {
        empty = false;
        break;
      }
    }
  }
  return empty;
}

// Return the number of non-NULL entries in \a array
UInt32
Verific2NucleusUtilities::countNonNullArrayItems(Array* array)
{
  UInt32 count=0;
  if (array && array->Size() > 0) {
    VhdlNode *item;
    unsigned i;
    FOREACH_ARRAY_ITEM(array, i, item) {
      if (!item) continue;    // only count non-NULL elements
      count++;
    }
  }
  return count;
}


// This method strips of the uniquification suffix. 
// Used during signature construction for subprogram declarations nested inside
// other subprograms.
UtString
Verific2NucleusUtilities::getDeuniquifiedName(const char* subprogram_name)
{
  // Strip of uniquifying suffix (if any)
  UtString deuniquified_name(subprogram_name);
  UtString::size_type loc = deuniquified_name.rfind(CarbonContext::sUniqSubprogramSuffix);
  if (loc != UtString::npos) {
    deuniquified_name.erase(loc);
  }

  return deuniquified_name;
}

bool
Verific2NucleusUtilities::isUniquifiedName(const UtString& subprogram_name)
{
  // see if of uniquifying suffix is present
  UtString::size_type loc = subprogram_name.rfind(CarbonContext::sUniqSubprogramSuffix);
  return (loc != UtString::npos);
}


// Return true if this lval refers to a composite object
bool
Verific2NucleusUtilities::isComposite(NULvalue* lval)
{
  bool result = false;
  if (!lval) {
    // Nothing to do, return false
  } else if (eNUIdentLvalue == lval->getType()) {
    NUIdentLvalue* l = dynamic_cast<NUIdentLvalue*>(lval);
    // Simple identifier, see if it's a composite.
    NUNet* net = l->getIdent();
    // Probably never true, since composite identifiers are populated as
    // NUCompositeIdentLvalue.
    result = net->isCompositeNet();
  } else if (eNUCompositeLvalue == lval->getType()) {
    result = true;
  } else if (eNUCompositeSelLvalue == lval->getType()) {
    // Indexed array of records are populated as NUCompositeSelLvalue
    result = true;
  } else if (eNUCompositeFieldLvalue == lval->getType()) {
    // Check to see if the record field is itself a record
    NUCompositeFieldLvalue* l = dynamic_cast<NUCompositeFieldLvalue*>(lval);
    NUNet* net = l->getNet();
    result = net->isCompositeNet();
  } else if (eNUCompositeIdentLvalue == lval->getType()) {
    result = true;
  } else {
    // What remains at this point are:
    // NUVarselLvalue, NUConcatLvalue, NUMemselLvalue
    // None of which are composites.
    NUType lvalType = lval->getType();
    INFO_ASSERT(lvalType == eNUVarselLvalue || lvalType == eNUConcatLvalue || lvalType == eNUMemselLvalue, "Unexpected lval type");
  }
  return result;
}


// Get bounds of an indexed Verific prefix expression, given it's constraint.
// This should only be called on the prefix of an indexed name.
void 
Verific2NucleusUtilities::getPrefixBounds(VhdlConstraint* prefix_constraint, SInt32* prefix_left, SInt32* prefix_right)
{
  VhdlValue* left = prefix_constraint->Left();
  VhdlValue* right = prefix_constraint->Right();
  INFO_ASSERT(left && right, "Unexpected NULL prefix bounds");
  if (left->IsEnumValue()) {
    *prefix_left = left->Position();
    *prefix_right = right->Position();
  } else {
    INFO_ASSERT(left->IsIntVal() && right->IsIntVal(), "Unexpected non-integer prefix bounds");
    *prefix_left = left->Integer();
    *prefix_right = right->Integer();
  }
}

// Find the right most association in a VhdlIndexedName
VhdlDiscreteRange*
Verific2NucleusUtilities::getLastAssoc(const VhdlIndexedName& node)
{
  VhdlDiscreteRange* assoc = NULL;
  VhdlDiscreteRange* last_assoc = NULL;
  unsigned i;
  FOREACH_ARRAY_ITEM(node.GetAssocList(), i, assoc) {
    if (!assoc) continue;
    last_assoc = assoc;
  }
  
  return last_assoc;
}

// Helper class to determine whether a Verific Vhdl
// expression has a null range embedded within it.
// If so, we will not rely on the Verific Evaluate
// method, because it seems to return a constant true
// or false for relational expressions with embedded
// null ranges. 
class VerificFindNullRanges : public VhdlVisitor
{
public:
  VerificFindNullRanges(VhdlDataFlow* df) : mFound(false), mDataFlow(df) {};
  virtual ~VerificFindNullRanges() {};
  virtual void VHDL_VISIT(VhdlIndexedName, node)
  {
    // Visit associations
    // Visit prefix
    // Check if this is an array slice
    if (node.IsArraySlice()) {
      // If so, find prefix constraint
      //   can use VerificVhdlUtil.cpp::getPrefixConstraint, or just
      //   slice->EvaluatePrefixConstraint(mDataFlow).
      VhdlConstraint* prefix_constraint = node.EvaluatePrefixConstraint(mDataFlow);
      if (prefix_constraint) {
        // Then construct prefix_range 
        SInt32 prefix_l, prefix_r;
        Verific2NucleusUtilities::getPrefixBounds(prefix_constraint, &prefix_l, &prefix_r);
        // From last association, find access range
        VhdlDiscreteRange* assoc = Verific2NucleusUtilities::getLastAssoc(node);
        SInt32 access_l = 0, access_r = 0;
        bool const_range = Verific2NucleusUtilities::isConstantRange(assoc, mDataFlow, &access_l, &access_r);
        if (const_range) {
          // if access range is in reverse direction from prefix range, set mFound
          if (access_l > access_r && prefix_l < prefix_r ||
              access_l < access_r && prefix_l > prefix_r) {
            mFound = true;
          }
        }
        delete prefix_constraint;
      }
    }
  };
  
  bool nullRangeFound(void) { return mFound; };
  
private:
  ///@name Prevent the compiler from implementing the following
  VerificFindNullRanges(VerificFindNullRanges &node);
  VerificFindNullRanges& operator=(const VerificFindNullRanges &rhs);
  
  bool mFound;
  VhdlDataFlow* mDataFlow;
};

// Return true if the supplied Verific expression has a null range in it.
bool
Verific2NucleusUtilities::hasNullRange(const VhdlDiscreteRange& expr, VhdlDataFlow* df)
{
  VerificFindNullRanges nullRangeFinder(df);
  const_cast<VhdlDiscreteRange&>(expr).Accept(nullRangeFinder);
  return nullRangeFinder.nullRangeFound();
}

} // namespace verific2nucleus
