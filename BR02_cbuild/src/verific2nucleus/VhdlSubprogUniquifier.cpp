// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// project headers
#include "verific2nucleus/CarbonConstraintManager.h"
#include "verific2nucleus/CarbonStaticElaborator.h"
#include "verific2nucleus/VhdlCSElabVisitor.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"
#include "verific2nucleus/VhdlSubprogUniquifier.h"

// verific headers
#include "VhdlScope.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "VhdlTreeNode.h"
#include "VeriTreeNode.h"
#include "VhdlExpression.h"
#include "VhdlName.h"
#include "VhdlCopy.h"
#include "VhdlDeclaration.h"
#include "VhdlValue_Elab.h"
#include "Array.h"
#include "VhdlSpecification.h"
#include "VhdlDataFlow_Elab.h"
#include "Set.h"
#include "vhdl_yacc.h" // VHDL_range

// other project headers
#include "util/UtPair.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"

// standard headers
#include <sstream>

// Set this to 1 to generate debugging output
#define ENABLE_DEBUG_MSGS 0

namespace verific2nucleus {

using namespace Verific;

    //=====================================================================================
    // Constructor

    VhdlSubprogUniquifier::VhdlSubprogUniquifier(CarbonConstraintManager* cm, CarbonStaticElaborator* ce) :
      VhdlCSElabVisitor(cm, ce),
      mEnableUniquifySubprogramBody(false),
      mUniquifiedSubprogramBody(NULL)
    {
    }

    //=====================================================================================
    // Vhdl Loop Unrolling
    //
    // Loops with subprogram calls within should be 'unrolled', so we can use the
    // value of the loop index to compute constraints on arguments to subprograms.
    // (See comments for 'loopShouldBeUnrolled' in Verific2NucleusUtilities.cpp.)
    //
    // Note that we do not create a sub VhdlDataFlow object if the loop is unrolled,
    // because no conditional control flow is created by an unrolled loop. 
    void VhdlSubprogUniquifier::VHDL_VISIT(VhdlLoopStatement, node)
    {
      // Make sure we visit the iteration scheme. There could be function
      // calls there too.
      if (node.GetIterScheme()) node.GetIterScheme()->Accept(*this);

      // Figure out whether we should/can unroll the loop. See comments
      // in Verific2NucleusUtilities.cpp
      bool forceUnroll = getCarbonContext()->getArgs()->getBoolValue(CarbonContext::scVhdlLoopUnroll);
      if (Verific2NucleusUtilities::loopShouldBeUnrolled(&node, mDataFlow, forceUnroll)) {
        // We're going to visit statements for each iteration of the loop.
  
        // Grab the range of the loop
        // Existence of iteration scheme and constant range guaranteed by 
        // loopShouldBeUnrolled()
        VhdlForScheme* fScheme = dynamic_cast<VhdlForScheme*>(node.GetIterScheme());
        if ( !fScheme ){
          UtString msg("Unsupported construct -- Loop iteration scheme is not a FOR loop.");
          reportFailure(&node, msg);
          return;
        }
        VhdlDiscreteRange* range = fScheme->GetRange();
        if ( !range ){
          UtString msg("Unsupported construct -- Loop iteration has a NULL range.");
          reportFailure(&node, msg);
          return;
        }
        SInt32 left = 0, right = 0;
        Verific2NucleusUtilities::isConstantRange(range, mDataFlow, &left, &right);

        // Figure out how many iterations through the loop, and whether we're 
        // incrementing up or down.
        SInt32 iterations = std::abs(left-right) + 1;
        SInt32 increment = (right > left) ? 1 : -1;

        // Now visit statements in the loop, with the iteration id
        // set to the value of the loop index.
        SInt32 loopIndex = left;
        for (SInt32 i = 0; i < iterations; i++, loopIndex += increment) {
          // Construct a VhdlValue equal to the VHDL loop index.
          // This will be owned by the Verific VhdlIdDef, and freed by it.
          VhdlValue *value = new VhdlInt(loopIndex);
          fScheme->GetIterId()->SetValue(value);
          unsigned i = 0;
          VhdlTreeNode* stmt = 0;
          FOREACH_ARRAY_ITEM(node.GetStatements(), i, stmt) {
            if (!stmt) continue;
            stmt->Accept(*this);
          }
        }
      } else {
        // We're not unrolling the loop, so create a sub data flow, and
        // just visit the statements once
        VhdlDataFlow *curDF = mDataFlow;
        Array data_flows(1);
        mDataFlow = Verific2NucleusUtilities::setupLoopDataFlow(&data_flows, mDataFlow, &node);
        unsigned i = 0;
        VhdlTreeNode* stmt = 0;
        FOREACH_ARRAY_ITEM(node.GetStatements(), i, stmt) {
          if (!stmt) continue;
          stmt->Accept(*this);
        }
        // Restore parent dataflow
        Verific2NucleusUtilities::cleanUpDataFlows(&node, &data_flows, curDF);
        mDataFlow = curDF;
      }
    }

    //=====================================================================================
    // This visitor should be a no-op if it is not called from the point of a 
    // subprogram invocation. It should do nothing if called from the point of declaration.
    // This is why we have the guard boolean member variables, which are only set
    // at the point of a subprogram (or operator overload) call.
    //
    // As a consequence, this only does anything meaningful when called from within 
    // 'walkSubprogramBody' which has placed constraints and values (if any) on all 
    // the formal arguments to the subprogram.
    //
    // A subprogram declaration uniquification is initiated at the point of a subprogram
    // call, and it is done in two steps:
    //
    // 1) The subprogram declaration is uniquified. This can only be done AFTER all
    //    constraints and values (if any) have been placed on the subprogram formals
    //    (which is why we do it in this visitor). Note that UniquifySubprogBody
    //    can uniquify at most one subprogram body (it is not recursive, and does
    //    not uniquify subprograms called from within). It can return NULL if
    //    the subprogram has already been uniquified.
    //
    // 2) Once the subprogram has been uniquified, it is traversed to look for nested
    //    subprogram declarations and calls.
    //
    // NOTES: 
    //
    // It is not possible to uniquify the subprogram body here, then
    // immediately traverse/visit it. The constraints and values (and VhdlDataFlow)
    // have all been setup with the id's of the original subprogram body. So
    // traversing the uniquified subprogram body here can cause data flow errors
    // and resulting seqfaults.
    //
    // It is tempting to walk the original subprogram body (rather than the 
    // uniquified one), but that will cause declarations of nested subprograms
    // to appear in the wrong place.
    //
    // The uniquify method is called BEFORE the subprogram body has been visited. This
    // enables the correct ordering of recursive subprogram declarations in architecture
    // bodies - declared before used. It also avoids issues with nested subprograms
    // (see preceeding note).

    void VhdlSubprogUniquifier::VHDL_VISIT(VhdlSubprogramBody, node)
    {
      // Should never by trying to uniquify AND visit at the same time.
      INFO_ASSERT(!(mEnableUniquifySubprogramBody && mEnableSubprogramBodyWalk), "Cannot uniquify and visit in the same call");
      if (mEnableUniquifySubprogramBody) {
        // Uniquify at most one subprogram body
        mEnableUniquifySubprogramBody = false;
        mUniquifiedSubprogramBody = UniquifySubprogBody(&node, mParentSignature);
      }
      else if (mEnableSubprogramBodyWalk) {
        // Visit the subprogram body
        mEnableSubprogramBodyWalk = false;
        VhdlVisitor::VisitVhdlSubprogramBody(node);
      }
    }
    
    //=====================================================================================
    // This visitor processes overloaded operators.
    //
    // It is almost an exact copy of the visitor used by VhdlConstraintElaborator (should
    // probably be shared).

    void VhdlSubprogUniquifier::VHDL_VISIT(VhdlOperator, node)
    {
      // First, process left and right (if any) expressions. They may contain
      // function calls.
      if (node.GetLeftExpression()) {
        node.GetLeftExpression()->Accept(*this);
      }
      if (node.GetRightExpression()) {
        node.GetRightExpression()->Accept(*this);
      }

      // Next, check to see if this is a user supplied overloaded operator
      VhdlIdDef* opId = node.GetOperator();
      if (Verific2NucleusUtilities::isNonIeeeOverloadedOperator(opId)) {

        // Calculate signature, and save for later reference from subprogram body
        UtString sig = m_cm->getOverloadedOperatorSignature(&node, mDataFlow);
        UtString old_parent_sig = mParentSignature;
        mParentSignature = sig;
        
        VhdlSubprogramBody* sBody = node.GetOperator()->Body();
        if (sBody) {
          // Create an arguments array 
          Array *args = new Array();
          if (node.GetLeftExpression()) {
            args->Insert(node.GetLeftExpression());
          }
          if (node.GetRightExpression()) {
            args->Insert(node.GetRightExpression());
          }

          // First uniquify subprogram body, then visit uniquified subprogram body
          UniquifyAndVisitSubprogramBody(sBody, args);
          delete args;
        }

        // Restore parent signature
        mParentSignature = old_parent_sig;
      }
    }
    
    //=====================================================================================
    // This visitor processes function and procedure calls. 
    // It is almost an exact copy of the visitor used by VhdlConstraintElaborator (should
    // probably be shared).

    void VhdlSubprogUniquifier::VHDL_VISIT(VhdlIndexedName, node) 
    {
      // First, process all associations. They may contain function calls themselves.
      unsigned i = 0;
      VhdlTreeNode* assoc = 0;
      FOREACH_ARRAY_ITEM(node.GetAssocList(), i, assoc) {
        if (assoc) assoc->Accept(*this);
      }

      // We need to visit the prefix for cases like: funcall(arg1)(3 downto 0)
      if (node.GetPrefix()) node.GetPrefix()->Accept(*this);

      // If it's a function or procedure call (but not one of the builtin functions)
      if (isTraversableSubprog(&node)) {
    
        // Calculate signature
        UtString sig = m_cm->getSubprogramSignature(&node, mDataFlow);

        UtString old_parent_sig = mParentSignature;
        mParentSignature = sig;
        
        // Now walk the subprogram body
        VhdlSubprogramBody* sBody = node.GetId()->Body();
        if (sBody) {
          if (ENABLE_DEBUG_MSGS) // Debug
          {
            UtIO::cout() << "DEBUG: Initiating subprogram body walk for " << sig << UtIO::endl;
          }
          Array *args = new Array;
          Verific2NucleusUtilities::getSubprogramArgs(args, &node, true /* use defaults */);
          // First uniquify subprogram body, then visit uniquified subprogram body
          UniquifyAndVisitSubprogramBody(sBody, args);
          delete args;
        }
        
        // Restore parent signature
        mParentSignature = old_parent_sig;
      } 
    }

  // Return true if subprogram body declaration is nested within
  // another.
  bool VhdlSubprogUniquifier::isNestedSubprogDecl(VhdlIdDef* sId)
  {
    VhdlIdDef* parent_subprogram = sId->GetOwningScope() ? sId->GetOwningScope()->GetSubprogram() : NULL;
    return parent_subprogram != NULL;
  }
  
  // Determine whether supplied signature corresponds to a
  // subprogram that has already been uniquified.
  bool VhdlSubprogUniquifier::AlreadyUniquified(UtString signature)
  {
    bool uniquified = true;
    if (mUniquified.find(signature) == mUniquified.end()) {
      uniquified = false;
      mUniquified.insert(signature);
    }
    return uniquified;
    
  }

  // Process deferred declarations
  // VhdlIdDef declarations cannot be added to a VhdlScope that is currently being visited by
  // 'walkSubprogramBody'. Therefore, we accumulate the required declarations in a list, and
  // process them when 'walkSubprogramBody' has been exitted.
  // Return true on success. A non-successful return implies a name collision.
  bool VhdlSubprogUniquifier::ProcessDeferredDeclarations(void)
  {
    bool success = true;

    UtList<PendingDecl>::iterator i;
    for (i = mPendingDecls.begin(); i != mPendingDecls.end(); ++i) {
      INFO_ASSERT((*i).OwningScope, "Unexpected null owning scope for pending declarations");
      INFO_ASSERT((*i).SubprogId, "Unexpected null subprogram id for pending declarations");
      if (ENABLE_DEBUG_MSGS) // Debug
        UtIO::cout() << "DEBUG: Processing deferred declarations for " << (*i).SubprogId->Name() << UtIO::endl;
      if (!(*i).OwningScope->Declare((*i).SubprogId))
        success = false;
      if ((*i).SubtypeId) {
        if (!(*i).OwningScope->Declare((*i).SubtypeId))
          success = false;
      }
    }
    mPendingDecls.clear();

    return success;
  }
  
  // This method changes the reference on recursive subprogram calls to the 'orig_id', i.e. the id of the original recursive subprogram,
  // as opposed to the copied subprogram. Consider the following recursive function, which is a copy of 'recurse'.
  //
  // function recurse_copy(v : bit_vector) return bit_vector is
  // begin
  //  ...
  //  result := recurse_copy(v(v'length-1 downto 0));
  //  ...
  // end
  //
  // This method would replace the 'id' on the call to 'recurse_copy' with the 'id' for 'recurse', as illustrated below:
  //
  // function recurse_copy(v : bit_vector) return bit_vector is
  // begin
  //  ...
  //  result := recurse(v(v'length-1 downto 0));
  //  ...
  // end
  //
  // NOTE: The name of the call does not have to be changed, because it already refers to the original
  // subprogram (in this case 'recurse'). Only the VhdlSubprogramId*, which represents the subprogram body to which the call
  // refers, needs to be changed.
  void VhdlSubprogUniquifier::ChangeRecursiveCallsBackToOriginal(VhdlSubprogramBody* sBody, VhdlIdDef* new_id, VhdlIdDef* orig_id)
  {
    // Helper visitor class
    class RevertRecursiveCalls : public VhdlVisitor
    {
    public:
      RevertRecursiveCalls(VhdlIdDef* new_id, VhdlIdDef* orig_id) : mNew(new_id), mOrig(orig_id) {}
      virtual void VHDL_VISIT(VhdlIndexedName, node) 
      {
        if (0) // Debug
          UtIO::cout() << "DEBUG: RevertRecursiveCalls Visiting VhdlIndexedName '" << Verific2NucleusUtilities::verific2String(node) << UtIO::endl;
        // If it's a function or procedure call (but not one of the builtin functions)
        // NOTE: id scope backpointer is not yet setup, so we can't call 
        // Verific2NucleusUtilities::isIEEEPackageBuiltinFunction(&node); but it's not
        // necessary, since we compare id against mNew. These can only be equal for non
        // builtin functions.
        if (node.IsFunctionCall()) {
          if (node.GetId() == mNew) {
            node.SetId(mOrig);
            if (0) // Debug
              UtIO::cout() << "DEBUG: Replaced function call id ref to " << mNew->Name() << " with call to " << mOrig->Name() << UtIO::endl;
          }
        }

        // Remember to process all associations. They may contain function calls themselves.
        unsigned i = 0;
        VhdlTreeNode* assoc = 0;
        FOREACH_ARRAY_ITEM(node.GetAssocList(), i, assoc) {
          if (assoc) assoc->Accept(*this);
        }

      }
    private:
      VhdlIdDef* mNew;
      VhdlIdDef* mOrig;
    };

    // Method body starts here
    RevertRecursiveCalls revert(new_id, orig_id);
    VhdlStatement* stmt = NULL;
    unsigned i;
    FOREACH_ARRAY_ITEM(sBody->GetStatementPart(), i, stmt) {
      if (stmt) stmt->Accept(revert);
    }
  }
  
  // Debugging code to dump all VhdlIdRef* to id's with a certain name. Used to verify integrity of copy methods.
  static void DumpIdRefsTo(const char* name, VhdlSubprogramBody* sBody)
  {
    // Helper visitor class
    class VisitIdRefs : public VhdlVisitor
    {
    public:
      VisitIdRefs(const char* name) : mName(name) {}
      virtual void VHDL_VISIT(VhdlIdRef, node) 
      {
        if (strcmp(node.Name(), mName) == 0) {
          UtIO::cout() << "DEBUG: Id Ref to '" << mName << "', VhdlIdDef* ptr = " << node.GetId() << UtIO::endl;
        }
      }
      virtual void VHDL_VISIT(VhdlConstantDecl, node)
      {
        // This assumes only a single id
        VhdlIdDef* id = static_cast<VhdlIdDef*>(node.GetIds()->At(0));
        if (strcmp(id->Name(), mName) == 0) {
          UtIO::cout() << "DEBUG: Constant Declaration for '" << mName << "', VhdlIdDef* ptr = " << id << UtIO::endl;
        }
      }
      
    private:
      const char* mName;
    };
    
    // Method body starts here
    VisitIdRefs visitor(name);
    UtIO::cout() << "DEBUG: Visiting " << sBody->GetSubprogramSpec()->GetDesignator()->Name() << UtIO::endl;
    sBody->Accept(visitor);
  }

  // First uniquify then visit a subprogram body. See comments for VhdlSubprogramBody visitor
  void VhdlSubprogUniquifier::UniquifyAndVisitSubprogramBody(VhdlSubprogramBody* subprogram_body, Array* args)
  {
    // First, uniquify the subprogram body
    mEnableUniquifySubprogramBody = true;
    walkSubprogramBody(subprogram_body, args); // This sets mUniquifiedSubprogramBody, and resets mEnableUniquifySubprogramBody
    // Second, traverse the uniquified subprogram (if any)
    if (mUniquifiedSubprogramBody) {
      mEnableSubprogramBodyWalk = true;
      VhdlSubprogramBody* uniquified_body = mUniquifiedSubprogramBody;
      mUniquifiedSubprogramBody = NULL;
      walkSubprogramBody(uniquified_body, args); // This resets mEnableSubprogramBodyWalk
    }
  }
  
  // Uniquify the subprogram body by making a copy, renamed, with constrained
  // argument and return types in the same decl area as original.
  // This is (and must be) called from within walkSubprogramBody, so constraints are on
  // formals, and return constraint is available with a lookup in the
  // signature->constraint map.
  VhdlSubprogramBody* VhdlSubprogUniquifier::UniquifySubprogBody(VhdlSubprogramBody* subprogram_body, UtString sig)
  {
    INFO_ASSERT(subprogram_body, "Unexpected NULL subprogram body passed to VhdlSubprogUniquifier::UniquifySubprogBody");
    INFO_ASSERT(!sig.empty(), "Unexpected empty signature passed to VhdlSubprogUniquifier::UniquifySubprogBody");

    // Nothing to do if we've already uniquified this
    // signature.
    if (AlreadyUniquified(sig))
      return NULL;

    // Find the owning scope
    VhdlIdDef* orig_designator = subprogram_body->GetSubprogramSpec()->GetDesignator();
    VhdlScope* owning_scope = orig_designator->GetOwningScope() ? orig_designator->GetOwningScope() : orig_designator->LocalScope()->Upper();

    // Make a copy of the original subprogram
    VhdlMapForCopy map_for_copy1;
    VhdlSubprogramBody* subprog_copy = dynamic_cast<VhdlSubprogramBody*>(subprogram_body->CopyDeclaration(map_for_copy1));
    INFO_ASSERT(subprog_copy, "Failed to copy subprogram");
    // Get the uniquified name
    VhdlSpecification* subprog_spec = subprog_copy->GetSubprogramSpec();
    VhdlIdDef* subprog_designator = subprog_spec->GetDesignator();
    UtString new_name = m_cm->getUniquifiedName(owning_scope, subprog_designator->Name());

    // Change the name of the subprogram
    subprog_designator->ChangeIdName(Strings::save(const_cast<char*>(new_name.c_str())));

    // Recursive function calls now refer to the copied subprogram, but we want them
    // to refer to the orignal. 
    ChangeRecursiveCallsBackToOriginal(subprog_copy, subprog_designator, orig_designator);
    
    // Set designator back pointers 
    subprog_designator->SetSpec(subprog_spec);
    subprog_designator->SetBody(subprog_copy);

    // Split all interface declarations into a single id. Each id
    // may have a different constraint, and thus will require a
    // different subtype specification.
    unsigned i;
    VhdlInterfaceDecl* iface_decl;
    Array* formals = new Array();
    FOREACH_ARRAY_ITEM(subprog_spec->GetFormalParamList(), i, iface_decl) {
      iface_decl->Split(formals);
    }

    // By now, the formals have been expanded to one id per interface decl.
    // Modify the subypte indication to reflect the constraint on the formal.
    // Remember, the constraint comes from the arguments.
    FOREACH_ARRAY_ITEM(formals, i, iface_decl) {
      VhdlIdDef* formal_id = static_cast<VhdlIdDef*>(iface_decl->GetIds()->At(0));
      VhdlConstraint* constraint = formal_id->Constraint();
      VhdlSubtypeIndication* subtype = iface_decl->GetSubtypeIndication();
      // If the subtype is already a constrained type, we don't need to
      // re-create the subtype with a constraint. That will avoid funny looking
      // types like "positive 1 to 2147483648"
      if (subtype->IsUnconstrained(1 /* all level */)) {
        // Note that 'new_subtype' reuses parts of 'subtype', so we cannot delete 'subtype'.
        VhdlSubtypeIndication* new_subtype = subtype->CreateSubtypeWithConstraint(constraint);
        if (new_subtype) {
          // This sets the flags on new_subtype
          (void)new_subtype->TypeInfer(0,0,VHDL_range,0) ;
          // Set the new subtype indication, don't delete the old (since the new is created from it).
          // Note, we had to create a Verific API call to do this.
          iface_decl->SetSubtypeIndication(new_subtype);
        }
      }
    }

    // I don't see a way to replace the formal parameter list array directly,
    // so delete all elements of the original array, and replace them
    // by elements of the new array.
    subprog_spec->GetFormalParamList()->Reset();
    subprog_spec->GetFormalParamList()->Append(formals);
    delete formals;
    
    // If this is a function, we have to constrain the return type (if not already constrained).
    VhdlSubtypeDecl* subtype_decl = NULL;
    if (subprog_spec->IsFunction()) {
      VhdlName* return_type = subprog_spec->GetReturnType();
      if (return_type->IsUnconstrained(1 /* all levels */)) {
        // Create a subtype declaration for the return type - will have to give it a uniq name
        //   - create a VhdlIdDef* with the new uniq name
        // TODO: Make sure this name does not conflict with another name in this scope.
        UtString subtype_name(new_name);
        subtype_name << "_return_type";
        VhdlSubtypeId* subtype_id = new VhdlSubtypeId(Strings::save(subtype_name.c_str()));
        // Keep linefile info up-to-date
        subtype_id->SetLinefile(subprog_spec->Linefile());
        //   - Look up the function return constraint, given the signature
        VhdlConstraint* function_return_constraint = m_cm->lookupFunctionReturnConstraint(mParentSignature);
        INFO_ASSERT(function_return_constraint, "Unexpected NULL function return constraint");
        // Set the constraint on the return type? 
        subtype_id->SetConstraint(function_return_constraint->Copy());
        //   - create a subtype indication for the subtype decl, using the technique above -
        //     noting that VhdlName* is a VhdlSubtypeIndication, so you can call create subtype
        //     with constraint on it.
        VhdlSubtypeIndication* new_subtype = return_type->CreateSubtypeWithConstraint(function_return_constraint);
        // This sets the flags on new_subtype
        // TODO: Do we need to set VhdlScope::_present_scope? (See comments in VhdlName.h, search for TypeInfer from top)
        (void)new_subtype->TypeInfer(0,0,VHDL_range,0) ;
        //   - construct a subtype declaration with VhdlIdDef* and new VhdlSubtypeIndication*
        subtype_decl = new VhdlSubtypeDecl(subtype_id, new_subtype);
        // Keep linefile info up-to-date
        subtype_decl->SetLinefile(subprog_spec->Linefile());
        // Create a new VhdlName* (VhdlIdRef*) with the name of the subtype declaration.
        VhdlIdRef* type_ref = new VhdlIdRef(subtype_id);
        type_ref->SetLinefile(subprog_spec->Linefile());
        // Replace the function spec return type with the newly created subtype. It'd be nice
        // if we could use VhdlFunctionSpecification::ReplaceChildName, but since the name that is
        // being replaced is now part of the subtype indication for the subtype declaration,
        // it can't be deleted. So ReplaceChildName cannot be used; instead a new api method
        // 'VhdlFunctionSpec::SetReturnTYpe was created.
        VhdlFunctionSpec* func_spec = dynamic_cast<VhdlFunctionSpec*>(subprog_spec);
        INFO_ASSERT(func_spec, "Unexpected NULL VhdlFunctionSpec");
        func_spec->SetReturnType(type_ref);
      }
    }
    
    // Add the subprogram body to a map, for use by the call uniquification phase.
    m_cm->addSignatureSubprogramBody(sig, subprog_copy);

    // Debugging messages
    if (ENABLE_DEBUG_MSGS)
    {
      UtIO::cout() << "DEBUG: Adding subprogram body for " << sig << UtIO::endl;
      if (0) // Debug
        DumpIdRefsTo("num_bits", subprog_copy);
    }
    
    // The subprogram designator (VhdlSubprogramId*) and the function return subtype
    // (if any) must be declared in the containing scope. But 
    // save these for later declaration, after we're done with the traversal. If we try
    // to do these here, it will confuse walkSubprogramBody (within which we are currently nested).
    VhdlIdDef* pending_subtype_id = subtype_decl ? subtype_decl->GetId() : NULL;
    mPendingDecls.push_back(PendingDecl(subprog_designator, pending_subtype_id, owning_scope));

    // Insert the subprogram declaration into the decl area 
    // (can be one of subprogram body, package body, architecture decl area, process decl area, block decl area)

    // Set this to 1 if uniquified version of subprogram body was successfully inserted
    unsigned successful_subprog_insertion = 0;

    VhdlDesignUnit* containing_unit = NULL;
    if (owning_scope->IsSubprogramScope()) {
      // Subprogram was declared inside another subprogram
      VhdlIdDef* owner_id = owning_scope->GetSubprogram();
      VhdlSubprogramId* subprog_id = dynamic_cast<VhdlSubprogramId*>(owner_id);
      INFO_ASSERT(subprog_id, "Unexpected NULL subprogram id during subprogram uniquification");
      // Get subprogram body
      VhdlSubprogramBody *owning_subprog_body = subprog_id->Body();
      INFO_ASSERT(owning_subprog_body, "Unexpected NULL subprogram body found during subprogram uniquification");
      // Now insert the subprogram
      successful_subprog_insertion = owning_subprog_body->InsertAfterDecl(subprogram_body, subprog_copy);
      // Insert decl for function return type (if any)
      if (subtype_decl) 
        owning_subprog_body->InsertAfterDecl(subprogram_body, subtype_decl);
    } else if (owning_scope->IsProcessScope()) {
      // Subprogram was declared inside a process
      // There doesn't seem to be any direct way to get from a process scope to the
      // process statement, so look for it in the architecture.
      VhdlDesignUnit* arch = owning_scope->GetContainingDesignUnit()->GetDesignUnit();
      VhdlStatement* stmt = Verific2NucleusUtilities::findScopeStatement(arch->GetStatementPart(), owning_scope);
      VhdlProcessStatement* proc = dynamic_cast<VhdlProcessStatement*>(stmt);
      if (!proc) {
        UtString msg("Expected to find a process statement for owning_scope");
        reportFailure( stmt  , msg);
        return NULL;
      }
      successful_subprog_insertion = proc->InsertAfterDecl(subprogram_body, subprog_copy);
      if (subtype_decl)
        proc->InsertAfterDecl(subprogram_body, subtype_decl);
    } else if (owning_scope->GetOwner() && owning_scope->GetOwner()->GetClassId() == ID_VHDLBLOCKID) {
      // Subprogram was declared inside a block 
      VhdlStatement* block = owning_scope->GetOwner()->GetStatement();
      if (!block) {
        UtString msg("Expected to find a block statement for owning_scope");
        reportFailure( owning_scope->GetOwner(), msg);
        return NULL;
      }
      successful_subprog_insertion = block->InsertAfterDecl(subprogram_body, subprog_copy);
      if (subtype_decl)
        block->InsertAfterDecl(subprogram_body, subtype_decl);
    } else {
      // This subprogram must be declared within a package, or an architecture
      VhdlIdDef* owner_id = owning_scope->GetContainingDesignUnit();
      containing_unit = owner_id->GetDesignUnit();
      successful_subprog_insertion = containing_unit->InsertAfterDecl(subprogram_body, subprog_copy);
      // If the containing unit is a package, then don't declare the subtype_decl in the package body,
      // because it will be declared later in the package header.
      if (!containing_unit->IsPackageBody() && subtype_decl)
        containing_unit->InsertAfterDecl(subprogram_body, subtype_decl);
    }
    

    // If the subprogram was declared in a package, put a VhdlSubprogramDecl in the VhdlPackageDecl
    if (containing_unit && containing_unit->IsPackageBody()) {
      // First, find the relevant VhdlPackageDecl.
      VhdlPrimaryUnit* owner = containing_unit->GetOwningUnit();
      VhdlPackageDecl*  pkg_decl = dynamic_cast<VhdlPackageDecl*>(owner);
      INFO_ASSERT(pkg_decl, "Unable to find package declaration for package body");
      
      // Create a VhdlSubprogramDecl to put in the package decl
      VhdlSpecification* subprog_spec = subprog_copy->GetSubprogramSpec();
      VhdlMapForCopy map_for_copy;
      VhdlSpecification* subprog_spec_copy = subprog_spec->CopySpecification(map_for_copy, 0 /* include scope */);
      VhdlSubprogramDecl* subprog_decl = new VhdlSubprogramDecl(subprog_spec_copy);
      subprog_decl->SetLinefile(subprog_spec->Linefile());
      // Copy pragmas onto an VhdlIdDef that is readily available to the nucleus populator.
      // Find original package header declaration. A function could be declared only
      // in the package body (for use inside the pkg), so there may be no pkg hdr declaration.
      VhdlIdDef* orig_id_in_pkg_hdr = subprogram_body->GetSubprogramSpec()->GetId()->Spec()->GetId();
      VhdlSubprogramDecl* orig_subprog_decl = FindSubprogramDecl(pkg_decl, orig_id_in_pkg_hdr);
      if (orig_subprog_decl) {
        // Copy pragmas from the package header id (from declaration) onto the subprogram copy id.
        VhdlIdDef* id_in_pkg_hdr_decl = orig_subprog_decl->GetSubprogramSpec()->GetId();
        VhdlIdDef* new_id_in_pkg_body = subprog_copy->GetSubprogramSpec()->GetId()->Spec()->GetId();
        new_id_in_pkg_body->CopyPragmas(*id_in_pkg_hdr_decl);
      }
      
      // Now insert the declaration at the end of the package. (I couldn't figure
      // out how to get the original decl, so I could insert directly after it.)
      if (subtype_decl) {
        pkg_decl->AddDeclaration(subtype_decl);
      }
      pkg_decl->AddDeclaration(subprog_decl);
    }

    // Elaborate the subprogram 
    // This doesn't really do much except to set the constraint on the return (which has been set elsewhere)
    // subprog_copy->Elaborate(0);
    
    // Make sure we successfully inserted the subprogram copy. If we didn't,
    // then it probably means one of the InsertAfterDecl calls could not find the subprogram body,
    // meaning that we probably tried to insert it in the wrong place.
    if (!successful_subprog_insertion) {
      UtString msg;
      const char* orig_subprog_name = orig_designator->Name();
      msg<< "Failed to insert uniquified copy '" << new_name << "' of subprogram '" << orig_subprog_name << "' with signature '" << sig << "'";
      reportFailure( subprogram_body, msg);
      return NULL;
    }

    return subprog_copy;
  }

  // Look up subprogram declaration in the package header, by using the VhdlIdef for the original function.
  // Using the VhdlIdDef instead of the name avoids problems with overloaded functions, all with the same name.   
  VhdlSubprogramDecl* VhdlSubprogUniquifier::FindSubprogramDecl(VhdlPackageDecl* pkg_decl, VhdlIdDef* orig_id)
  {
    VhdlSubprogramDecl* result = NULL;
    unsigned i;
    VhdlDeclaration* decl;
    FOREACH_ARRAY_ITEM(pkg_decl->GetDeclPart(), i, decl) {
      if (!decl) continue;
      if (decl->GetClassId() == ID_VHDLSUBPROGRAMDECL) {
        VhdlSubprogramDecl* subprog_decl = dynamic_cast<VhdlSubprogramDecl*>(decl);
        INFO_ASSERT(subprog_decl, "Unexpected NULL subprogram declaration");
        VhdlSpecification* spec = subprog_decl->GetSubprogramSpec();
        VhdlIdDef* id = spec->GetId();
        if (id == orig_id) {
          result = subprog_decl;
          break;
        }
      }
    }
    
    return result;
  }
  
  void VhdlSubprogUniquifier::reportFailure(VhdlTreeNode* node, const UtString& msg) {
    getCarbonStaticElaborator()->reportFailure(node, "VHDLSubprogUniquifier", msg);
    mErrorDuringWalk = true;
  }


}; // namespace Verific2Nucleus

