// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// project headers
#include "verific2nucleus/CarbonConstraintManager.h"
#include "verific2nucleus/CarbonStaticElaborator.h"
#include "verific2nucleus/VhdlCSElabVisitor.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"
#include "verific2nucleus/VhdlSubprogCallUniquifier.h"

// verific headers
#include "VhdlScope.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "VhdlTreeNode.h"
#include "VeriTreeNode.h"
#include "VhdlExpression.h"
#include "VhdlName.h"
#include "VhdlCopy.h"
#include "VhdlDeclaration.h"
#include "VhdlValue_Elab.h"
#include "Array.h"
#include "VhdlSpecification.h"
#include "VhdlDataFlow_Elab.h"
#include "Set.h"
#include "vhdl_yacc.h" // VHDL_range

// other project headers
#include "util/UtPair.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"

// standard headers
#include <sstream>

namespace verific2nucleus {

using namespace Verific;

    //=====================================================================================
    // Constructor

  VhdlSubprogCallUniquifier::VhdlSubprogCallUniquifier(CarbonConstraintManager *cm, CarbonStaticElaborator* ce)
      : VhdlCSElabVisitor(cm, ce)
    {
      mParentStatement = NULL;
    }

    //=====================================================================================
    // Vhdl Loop Unrolling (by now, there shouldn't bee any loops that can be unrolled.)
    //
    // Note that we do not create a sub VhdlDataFlow object if the loop is unrolled,
    // because no conditional control flow is created by an unrolled loop. 
    void VhdlSubprogCallUniquifier::VHDL_VISIT(VhdlLoopStatement, node)
    {
        walkLoopStatementAfterLoopRewrite(&node);
    }
    
    //=====================================================================================
    // Prevent walking subprograms from any point OTHER than a subprogram call.

    void VhdlSubprogCallUniquifier::VHDL_VISIT(VhdlSubprogramBody, node)
    {
      if (mEnableSubprogramBodyWalk) {
        mEnableSubprogramBodyWalk = false;
        VhdlVisitor::VisitVhdlSubprogramBody(node);
      }
    }

    //=====================================================================================
    // This visitor method saves the statement ptr for use by the operator overloading
    // uniquification. It is called by VhdlVisitor before visiting items within a
    // statement.
    void VhdlSubprogCallUniquifier::VHDL_VISIT(VhdlStatement, node) 
    {
      mParentStatement = &node;
      if (0) // Debug
        {
          UtIO::cout() << "DEBUG: Saving Ptr to Statement '" << Verific2NucleusUtilities::verific2String(node) << "'" << UtIO::endl;
        }
      VhdlVisitor::VisitVhdlStatement(node);
    }
  
    //=====================================================================================
    // This visitor processes overloaded operators.
    //
    // An expression that has an overloaded operator must necessarily invoke this visitor
    // method. So, for example the expression:
    //
    //     log2(16)/4 + 27 - 3
    //
    // Will invoke the VhdlOperator visitor on the entire expression. The visitor might
    // then visit the 'left' and 'right' expressions, which would cause the VhdlOperator 
    // visitor to be invoked again on the subexpressions. However, we short circuit the
    // visiting of subexpressions if the expression is found to have an overloaded operator
    // somewhere within it (except as the association of an indexed name - more about that later).
    // If an overloaded operator is found, then we perform special processing on the
    // expression. Essentially, we make a copy of the expression with overloaded operators
    // replaced by uniquified function calls. 
    //
    // If, during processing of the expression, we find an indexed name, then we invoke
    // the VhdlIndexedName visitor directly. The VhdlIndexedName visitor is responsible
    // for analyzing the associations (i.e. the arguments or indices), and replacing
    // overloaded operators with uniquified function calls. Of course, the VhdlIndexedName
    // visitor also visits the subprogram body (if it is a subprogram), ensuring that
    // subprogram calls within it are uniquified.
    //
    // When overloaded operators are found during processing of the expression, the
    // subprogram body for the overloaded operator is visited, ensuring that subprogram
    // calls within it are uniquified.

    void VhdlSubprogCallUniquifier::VHDL_VISIT(VhdlOperator, node)
    {
      // If we have overloaded operators, then call this special visitor
      // that will construct a new expresion with overloaded operators replaced
      // by function calls.
      if (HasOverloadedOperator(&node)) {
        if (0) // Debug
          UtIO::cout() << "DEBUG: Visiting overloaded operator expression: '" << Verific2NucleusUtilities::verific2String(node) << "'" << UtIO::endl;
        INFO_ASSERT(mParentStatement, "Unexpected NULL parent statement during operator overload uniquification");
        VhdlStatement* parent = mParentStatement;
        VhdlDiscreteRange* replacement_range = WalkExprAndReplaceOverloadedOperators(&node);
        // We expect 'replacement_range' to be an expression.
        VhdlExpression* replacement_expr = dynamic_cast<VhdlExpression*>(replacement_range);
        INFO_ASSERT(replacement_expr, "Unexpected NULL replacement expression");
        bool success = ReplaceChildExpr(parent, &node, replacement_expr);
        mParentStatement = parent;
        if (!success) {
          UtString msg;
          msg << "ERROR: Failed to replace overloaded operator with a uniquified function call for expr '"
              << Verific2NucleusUtilities::verific2String(node)
              << "' in statement on line " << node.Linefile()->GetLeftLine() << " of file " 
              << node.Linefile()->GetFileName();
          reportFailure(parent, msg);
          return;
        }
      } else {
        // Visit operator as normal, but we know there are no operator overloads
        if (node.GetLeftExpression()) {
          node.GetLeftExpression()->Accept(*this);
        }
        if (node.GetRightExpression()) {
          node.GetRightExpression()->Accept(*this);
        }
      }
    }

    // Debugging function 
    static UtString GetDeclNestingHierarchy(VhdlIdDef* sId)
    {
      UtString nesting_hierarchy = "";
      VhdlIdDef* parent_subprogram = sId->GetOwningScope() ? sId->GetOwningScope()->GetSubprogram() : NULL;
      while (parent_subprogram) {
        nesting_hierarchy = UtString(parent_subprogram->Name()) + "/" + nesting_hierarchy;
        parent_subprogram = parent_subprogram->GetOwningScope() ? parent_subprogram->GetOwningScope()->GetSubprogram() : NULL;
      }
      return nesting_hierarchy;
    }
  
    //=====================================================================================
    // This visitor processes function and procedure calls, and is responsible for
    // uniquifying them. It is also responsible for processing overloaded operators
    // in VhdlIndexedName associations, whether it is a subprogram call or not.

    void VhdlSubprogCallUniquifier::VHDL_VISIT(VhdlIndexedName, node) 
    {
      // We need to visit the prefix for cases like: funcall(arg1)(3 downto 0)
      if (node.GetPrefix()) node.GetPrefix()->Accept(*this);

      // If it's a function or procedure call (but not one of the builtin functions)
      if (isTraversableSubprog(&node)) {
    
        // Calculate signature
        UtString sig = m_cm->getSubprogramSignature(&node, mDataFlow);

        if (0) // Debug
        {
          UtIO::cout() << "DEBUG: Visiting '" << sig << "' during call uniquification." << UtIO::endl;
        }
        
        // Lookup the uniquified subprogram body.
        VhdlSubprogramBody* sBody = m_cm->lookupSubprogramBody(sig);
        if (!sBody) {
          UtString msg;
          msg << "ERROR: Could not find a subprogram body for signature '" << sig 
              << "' during subprogram call uniquification. This probably " 
              << "means the signature is calculated with a different value "
              << "during subprogram call uniquification than it had during "
              << "constraint elaboration and subprogram body uniquification.";
          reportFailure(&node, msg);
          return;
        }
        
        
        // Make a VhdlName (VhdlIdRef*) from the uniquified subprogram body name
        INFO_ASSERT(sBody && sBody->GetSubprogramSpec() && sBody->GetSubprogramSpec()->GetId(), 
                    "VhdlSubprogramBody pointers are not correctly set up.");
        const char* uniqified_name = sBody->GetSubprogramSpec()->GetId()->Name();
        VhdlIdRef* new_ref = new VhdlIdRef(Strings::save(const_cast<char*>(uniqified_name)));
        // Keep linefile info up to date
        new_ref->SetLinefile(sBody->GetSubprogramSpec()->GetId()->Linefile());
        // Now replace name of this node
        node.ReplaceChildName(node.GetPrefix(), new_ref);
        // Set id to uniquified subprogram
        node.SetId(sBody->GetSubprogramSpec()->GetId());
 
        if (0) // Debug
        {
          // If this subprogram declaration is nested within another, show the nesting hierarchy
          VhdlIdDef* sId = sBody->GetSubprogramSpec()->GetId();
          if (sId->GetOwningScope()->IsSubprogramScope()) {
            UtIO::cout() << "DEBUG: Declaration hierarchy for nested subprogram '" << uniqified_name << "': " << GetDeclNestingHierarchy(sId) << UtIO::endl;
          }
        }
        
        // Now walk the subprogram body, but only if we have not
        // already uniquified subprogram calls within it.
        if (!AlreadyUniquified(sig)) {
          Array *args = new Array;
          Verific2NucleusUtilities::getSubprogramArgs(args, &node, true /* use defaults */);
          mEnableSubprogramBodyWalk = true;
          walkSubprogramBody(sBody, args);
          delete args;
        }
      } 

      // Visit the associations last. Since they may be modified (if they contain overloaded operators), processing them
      // last avoids interfering with signature construction, which may be hindered if function arguments are
      // transformed to uniquified function calls.
      unsigned i = 0;
      VhdlDiscreteRange* assoc = 0;
      FOREACH_ARRAY_ITEM(node.GetAssocList(), i, assoc) {
        if (!assoc) continue;
        if (HasOverloadedOperator(assoc)) {
          // This association has overloaded operators. Transform the expression.
          if (0) // Debug
            UtIO::cout() << "DEBUG: Visiting argument with overloaded operators '" << Verific2NucleusUtilities::verific2String(*assoc) << "'  to subprogram call '" << Verific2NucleusUtilities::verific2String(node) << "'" << UtIO::endl;
          VhdlDiscreteRange* replacement_range = WalkExprAndReplaceOverloadedOperators(assoc);
          bool success = node.ReplaceChildDiscreteRange(assoc, replacement_range);
          if (!success) {
            UtString msg;
            msg << "ERROR: Failed to replace overloaded operators in association '" 
                << Verific2NucleusUtilities::verific2String(*replacement_range) 
                << "' for '" 
                << Verific2NucleusUtilities::verific2String(node) << "'";
            reportFailure(&node, msg);
            return;
          }
        } else {
          // No overloaded operators. Visit normally
          assoc->Accept(*this);
        }
      }
    }

  //======================================================================================
  // Helper functions
  //======================================================================================

  // Determine whether supplied signature corresponds to a
  // subprogram call that has already been uniquified.
  bool VhdlSubprogCallUniquifier::AlreadyUniquified(UtString signature)
  {
    bool uniquified = true;
    if (mUniquified.find(signature) == mUniquified.end()) {
      uniquified = false;
      mUniquified.insert(signature);
    }
    return uniquified;
  }

  // Replace overloaded operator expressions with a function call in a given statement.
  // Unfortunately, this algorithms depends on the type of statement.
  // Returns true on success, false otherwise.
  bool VhdlSubprogCallUniquifier::ReplaceChildExpr(VhdlStatement* stmt, VhdlExpression* orig_expr, VhdlExpression* new_expr)
  {
    bool success = false;
    
    if (stmt->GetClassId() == ID_VHDLIFSTATEMENT) {
      // If it's an if statement, examine all conditional expressions.
      VhdlIfStatement* if_stmt = dynamic_cast<VhdlIfStatement*>(stmt);
      // This handles the 'if' condition.
      if (if_stmt->ReplaceChildExpr(orig_expr, new_expr)) {
        success = true;
      } else {
        // This handles the 'elsif' conditions.
        unsigned i;
        VhdlElsif* elsif = NULL;
        FOREACH_ARRAY_ITEM(if_stmt->GetElsifList(), i, elsif) {
          if (!elsif) continue;
          if (elsif->ReplaceChildExpr(orig_expr, new_expr)) {
            success = true;
            break;
          }
        }
      } 
    } else if (stmt->GetClassId() == ID_VHDLCONDITIONALSIGNALASSIGNMENT) {
      // Conditional signal assignment: we have to walk through the waveforms
      // and replace expressions there
      VhdlConditionalSignalAssignment* csa = dynamic_cast<VhdlConditionalSignalAssignment*>(stmt);
      VhdlConditionalWaveform* wfm;
      unsigned i;
      FOREACH_ARRAY_ITEM(csa->GetConditionalWaveforms(), i, wfm) {
        if (wfm->ReplaceChildExpr(orig_expr, new_expr)) {
          success = true;
          break;
        }
      }
      
    } else {
      // Some other statement (besides if)
      if (stmt->ReplaceChildExpr(orig_expr, new_expr))
        success = true;
    }
    
    return success;
  }

  // Return true if this expression has overloaded operators. Note
  // that we don't look at indexed name associations (see comments
  // for VhdlOperator visitor).
  bool VhdlSubprogCallUniquifier::HasOverloadedOperator(VhdlDiscreteRange* expr) 
  {
    // Helper visitor class to detect overloaded operators
    class VhdlOverloadedOpAnalyzer : public VhdlVisitor
    {
    public:
      VhdlOverloadedOpAnalyzer(void) {mHasOverloadedOperator = false;}
    public:
      void VHDL_VISIT(VhdlOperator, node)
      {
        VhdlIdDef* opId = node.GetOperator();
        if (Verific2NucleusUtilities::isNonIeeeOverloadedOperator(opId)) {
          mHasOverloadedOperator = true;
        } else {
          // Look for overloaded operators in the left/right operator expressions
          if (node.GetLeftExpression()) node.GetLeftExpression()->Accept(*this);
          if (node.GetRightExpression()) node.GetRightExpression()->Accept(*this);
        }
      }

      bool containsOverloadedOperator(void) {return mHasOverloadedOperator;}

    private:
      bool mHasOverloadedOperator;
    };

    // Main body of HasOverloadedOperator method
    VhdlOverloadedOpAnalyzer ovld;
    expr->Accept(ovld);
    return ovld.containsOverloadedOperator();
  }

  // Walk an expression with overloaded operators, replacing the overloaded operators with
  // function calls. We have to take care to walk overloaded operator subprograms BEFORE 
  // transforming them to function calls, otherwise the signature construction will not work
  // correctly. Similarly, if the expression contains an indexed name, we invoke the 
  // VhdlIndexedName directly. It is responsible for processing overloaded operators in
  // it's associations. If the VhdlIndexedName is a subprogram call, it will visit the
  // subprogram body.
  //
  // This method ALWAYS allocates a new expression. It is up to the caller to
  // delete the original. This will usually be done with a VhdlStatement::ReplaceChildExpr call.
  //
  // Since this method allocates memory, we it shouldn't be called on EVERY expression. 
  // The caller should verify that there are overloaded operators in the expression
  // before calling this method.
     
  VhdlDiscreteRange* VhdlSubprogCallUniquifier::WalkExprAndReplaceOverloadedOperators(VhdlDiscreteRange *expr)
  {
    VhdlDiscreteRange* result = NULL;
    if (expr) {
      if (expr->IsVhdlOperator()) {
        // We have a unary or binary operator: visit left and right expression
        VhdlDiscreteRange* left = WalkExprAndReplaceOverloadedOperators(expr->GetLeftExpression());
        VhdlDiscreteRange* right = WalkExprAndReplaceOverloadedOperators(expr->GetRightExpression());
        if (Verific2NucleusUtilities::isNonIeeeOverloadedOperator(expr->GetOperator())) {
          // This is an overloaded operator.
          VhdlOperator* oper = dynamic_cast<VhdlOperator*>(expr);
          // Lookup the subprogram body (returns NULL if we've already visited this one)
          bool alreadyUniquified = false;
          VhdlSubprogramBody* sBody = GetOverloadedOperatorSubprogram(oper, &alreadyUniquified);
          if (sBody) {
            // Visit overloaded operator subprogram, it may contain additional function
            // calls.
            if (!alreadyUniquified)
              VisitOverloadedOperatorSubprogram(oper, sBody);
            // Create a function call to replace the operator (responsibility of caller)
            result = CreateFunctionCall(oper, sBody, left, right);
          }
        } else {
          // This is a non-overloaded operator. In this case, left and right
          // must both be VhdlExpression*
          VhdlExpression* left_expr = dynamic_cast<VhdlExpression*>(left);
          INFO_ASSERT(left_expr, "Unexpected NULL left expression");
          VhdlExpression* right_expr = dynamic_cast<VhdlExpression*>(right);
          if (right) {
            INFO_ASSERT(right_expr, "Unexpected NULL right expression");
          }
          result = new VhdlOperator(left_expr, expr->GetOperatorToken(), right_expr);
          // Keep linefile info up-to-date
          result->SetLinefile(expr->Linefile());
        }
      } else if (expr->IsRange()) {
        // This is a range of the form '(leftexpr to rightexpr)'
        // In this case, both left and right must be non-NULL VhdlExpression*
        VhdlDiscreteRange* left = WalkExprAndReplaceOverloadedOperators(expr->GetLeftExpression());
        VhdlDiscreteRange* right = WalkExprAndReplaceOverloadedOperators(expr->GetRightExpression());
        VhdlExpression* left_expr = dynamic_cast<VhdlExpression*>(left);
        INFO_ASSERT(left_expr, "Unexpected NULL left expression");
        VhdlExpression* right_expr = dynamic_cast<VhdlExpression*>(right);
        INFO_ASSERT(right_expr, "Unexpected NULL right expression");
        result = new VhdlRange(left_expr, expr->GetDir(), right_expr);
        // Keep linefile info up-to-date
        result->SetLinefile(expr->Linefile());
      } else if (expr->GetClassId() == ID_VHDLINDEXEDNAME) {
        // If this is an indexed name, invoke that visitor here. It will be reponsible for
        // transforming associations with overloaded operators. Note that those transformations
        // will be complete upon return.
        VhdlIndexedName* name = dynamic_cast<VhdlIndexedName*>(expr);
        VhdlSubprogCallUniquifier::VisitVhdlIndexedName(*name);
        VhdlMapForCopy old2new;
        result = expr->CopyDiscreteRange(old2new);
      } else {
        // This is a primary expression, not a function call. Make a copy
        VhdlMapForCopy old2new;
        result = expr->CopyDiscreteRange(old2new);
      }
    }
    return result;
  }
  
  // Given an overloaded operator expression, calculate the signature, lookup
  // and return the subprogram body. If the subprogram body
  // has already been uniquified, set the 'alreadyUniquified' argument to
  // true, otherwise false.

  VhdlSubprogramBody* VhdlSubprogCallUniquifier::GetOverloadedOperatorSubprogram(VhdlOperator* oper, bool* alreadyUniquified)
  {
    // Calculate signature
    UtString sig = m_cm->getOverloadedOperatorSignature(oper, mDataFlow);

    *alreadyUniquified = AlreadyUniquified(sig);
    
    VhdlSubprogramBody* sBody = m_cm->lookupSubprogramBody(sig);

    // Get the uniquified subprogram body for this particular signature from 
    // the constraint manager. It should always exist, unless program error.
    if (!sBody) {
      UtString msg;
      msg << "ERROR: Could not find a subprogram body for signature '" << sig 
          << "' during operator overload uniquification. This probably " 
          << "means the signature is calculated with a different value "
          << "during subprogram call uniquification than it had during "
          << "constraint elaboration and subprogram body uniquification.";
      reportFailure(oper, msg);
    }

    return sBody;
  }
  
  // Given an overloaded operator expression, calculate the signature, and walk
  // the subprogram body.
  void VhdlSubprogCallUniquifier::VisitOverloadedOperatorSubprogram(VhdlOperator* oper, VhdlSubprogramBody *sBody)
  {
    // Create an arguments array
    Array *args = new Array();
    if (oper->GetLeftExpression()) {
      args->Insert(oper->GetLeftExpression());
    }
    if (oper->GetRightExpression()) {
      args->Insert(oper->GetRightExpression());
    }
    // Now traverse the overloaded operator declaration for any
    // additional subprogram calls.
    mEnableSubprogramBodyWalk = true;
    walkSubprogramBody(sBody, args);
    delete args;
  }

  // Given an overloaded operator expression, create a uniquified function call
  // that will replace it.
  // For example, given the expression (e.g. arg1 op arg2), construct an expression
  // of the form: uniq_func_name(arg1, arg2)
  // Strategy: 
  // - Construct a VhdlName for 'uniq_func_name'
  // - Construct a VhdlIndexedName for 'uniq_func_name(arg1, arg2)
  // - Use VhdlStatement::ReplaceChildExpr(old,new) to modify the expression
  //   containing the overloaded operator.
  // - This means you'll need to save the parent statement somehow.
  // - Be sure to copy over the Linefile info, this is required by nucleus population
  //   (for error messages)

  VhdlIndexedName* VhdlSubprogCallUniquifier::CreateFunctionCall(VhdlOperator* oper, VhdlSubprogramBody* sBody, VhdlDiscreteRange* left, VhdlDiscreteRange* right)
  {
    // Create a copy of the args array to be used in the new VhdlIndexedName.
    // NOTE: left and right have already been allocated, no need to copy
    Array *args = new Array();
    if (left) {
      args->Insert(left);
    }
    if (right) {
      args->Insert(right);
    }
    // Make a VhdlName (VhdlIdRef*) from the uniquified subprogram body name
    INFO_ASSERT(sBody && sBody->GetSubprogramSpec() && sBody->GetSubprogramSpec()->GetId(), 
                "VhdlSubprogramBody pointers are not correctly set up.");
    const char* uniqified_name = sBody->GetSubprogramSpec()->GetId()->Name();
    VhdlIdRef* new_ref = new VhdlIdRef(Strings::save(const_cast<char*>(uniqified_name)));
    // Keep linefile info up-to-date
    new_ref->SetLinefile(sBody->GetSubprogramSpec()->Linefile());
    VhdlIndexedName* uniq_func_call = new VhdlIndexedName(new_ref, args);
    uniq_func_call->SetLinefile(oper->Linefile());
    uniq_func_call->SetId(sBody->GetSubprogramSpec()->GetId());
    // I couldn't figure out how to get TypeInfer to set the _is_function_call flag, so
    // I created an API call to set it directly.
    uniq_func_call->SetIsFunctionCall();

    return uniq_func_call;
  }
  
  void VhdlSubprogCallUniquifier::reportFailure(VhdlTreeNode* node, const UtString& msg) {
    getCarbonStaticElaborator()->reportFailure(node, "VhdlSubprogCallUniquifier", msg);
    mErrorDuringWalk = true;
  }
  
}; // namespace Verific2Nucleus

