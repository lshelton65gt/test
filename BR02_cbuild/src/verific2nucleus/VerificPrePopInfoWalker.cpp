// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "verific2nucleus/VerificPrePopInfoWalker.h"    // Visitor base class definition
#include "verific2nucleus/Verific2NucleusUtilities.h"

#include "Array.h"              // Make dynamic array class Array available
#include "Strings.h"            // A string utility/wrapper class
#include "Message.h"            // Make message handlers available, not used in this example

#include "VhdlUnits.h"          // Definition of a libraries and design units
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations
#include "VhdlExpression.h"     // Definitions of all vhdl expressions
#include "VhdlStatement.h"      // Definitions of all vhdl statements
#include "VhdlIdDef.h"          // Definitions of all vhdl identifiers
#include "VhdlName.h"           // Definitions of all vhdl names
#include "VhdlConfiguration.h"  // Definitions of all vhdl configurations
#include "VhdlSpecification.h"  // Definitions of all vhdl specifications
#include "VhdlMisc.h"           // Definitions of miscellaneous vhdl constructrs
#include "VhdlScope.h"
#include "vhdl_tokens.h"
#include "vhdl_file.h"
#include "veri_file.h"
#include <iostream>

namespace verific2nucleus {

using namespace Verific ;
VerificPrePopInfoWalker::VerificPrePopInfoWalker(VerificNucleusBuilder* nuBuilder, UtMap<VhdlTreeNode*, StringAtom*>* nameRegistry ):
    mNucleusBuilder(nuBuilder), mNameRegistry(nameRegistry)
{
    mVerificPrePopInfoWalkerImpl = 
        new VerificPrePopInfoWalkerImpl(mNucleusBuilder->gCache(), false);
}

VerificPrePopInfoWalker::~VerificPrePopInfoWalker()
{

     delete mVerificPrePopInfoWalkerImpl;
}


//RJC RC#18  a comment that explains what is going on here is necesary, my quick look makes me thik that all that happens is that a bunch of scopes are pushed then popped so obviously I am missing the significance of this method
//ADDRESSED

//Since HierRefNet defined in user defined package we creates two scope first for library and second for package
//package doesn't belong to current module so we save current stack which contain scope branches belong to current module
//and create scope branches for HierRefNet's library and package added to stack and after restore stack for keeping correct
//hierarchy of scopes 
void VerificPrePopInfoWalker::createBranchForHierRef()
{
    UtVector<VhdlIdDef*> hierRefIdDefs = mNucleusBuilder->gHierRefIdDefs();
    UInt32 s = hierRefIdDefs.size();
    for (UInt32 i = 0; i < s; ++i) {
        // The parent for package is root module. Save the current stacks and
        // begin at root. NOTE: Nested packages are not legal in VHDL.
        mVerificPrePopInfoWalkerImpl->saveStacksAndBeginAtRoot();
        // Push the library declaration/elaborated scope first. The library
        // name for declaration and elaborated scope are the same.
        StringAtom* libAtom =
            mNucleusBuilder->gCache()->
            intern(UtString(hierRefIdDefs[i]->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit()->Owner()->Name()));
        //push to stack for unelab scopes
        mVerificPrePopInfoWalkerImpl->pushDeclarationScope(libAtom);
        //push to stack for elab scopes
        mVerificPrePopInfoWalkerImpl->pushElabScope(libAtom, eNamedDeclScope);
        // Push the package declaration/elaborated scope first. The package
        // name for declaration and elaborated scope are the same.
        StringAtom* packAtom =
            mNucleusBuilder->gCache()->
            intern(UtString(hierRefIdDefs[i]->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit()->Name()));
        //push to stack for unelab scope
        mVerificPrePopInfoWalkerImpl->pushDeclarationScope(packAtom);
        //push to stack for elab scope
        mVerificPrePopInfoWalkerImpl->pushElabScope(packAtom, eNamedDeclScope);

        // Pop elaborated and declaration scopes for library and package.
        mVerificPrePopInfoWalkerImpl->popElabScope();
        mVerificPrePopInfoWalkerImpl->popDeclarationScope();
        mVerificPrePopInfoWalkerImpl->popElabScope();
        mVerificPrePopInfoWalkerImpl->popDeclarationScope();
        // Restore saved stacks and continue where we left off at the beginning of
        // package declaration.
        mVerificPrePopInfoWalkerImpl->restoreStacks();
    }
}

/**
 * EG: Extracting module instances and building hierarchy objects.
 * EG: Only new code !
 *
 * Visit scope create branch corresponding to scope
 * and visit child nodes which can contain scopes
 */
void VerificPrePopInfoWalker::VHDL_VISIT(VhdlEntityDecl, node)
{
  Verific::VhdlTreeNode* item;
  MapIter mi;
  UtString moduleName;
  FOREACH_VHDL_SECONDARY_UNIT(&node, mi,item) {
      VhdlSecondaryUnit* secUnit = static_cast<VhdlSecondaryUnit*>(item);
      INFO_ASSERT(secUnit, "invalid secondary unit node");
      VhdlLibrary* lib = secUnit->GetOwningLib();
      INFO_ASSERT(lib, "invalid lib node");
      UtString entityName(node.Id()->Name());

      // string::find_last_of("_default") crashes if the name is an escaped identifier e.g
      // See testcase beacon_vhdl_93/extd_identifier3 where the entity name is \\005a\\.
      // string::find_last_of("_default") returns 4 which is != string::npos and
      // therefore we tried to get entityName.substr(0, entityName.size()-8) which for
      // \\005a\\ goes into memory before the first character. We avoid this problem by
      // checking the length first and then getting the the last 8 characters and checking
      // if that substring is "_default"

      if (entityName.size() > 8) {
        if (VerificNucleusBuilder::hasSpecialCharacter(entityName)) {
          UtString tail = entityName.substr(entityName.size()-8, entityName.size()-1);
          if (tail == "_default") {
            entityName = entityName.substr(0, entityName.size() - 8);
          }
        } else {
          size_t pos = entityName.find_last_of("_default");
          if (UtString::npos != pos) {
            entityName = entityName.substr(0, entityName.size() - 8);
          }
        }
      }
      UtString archName(secUnit->Name());
      UtString libName(lib->Name());
      if (mNucleusBuilder->IsTopModule(entityName)) {
          moduleName = entityName;
      } else {
          moduleName = libName + UtString("_") +  entityName + UtString("_") + archName;
      }
      StringAtom* name = mNucleusBuilder->gCache()->intern(moduleName.c_str());
      //create branch node corresponding to visited scope
      //and push to stacks of elaborated and unelaborated scopes
      //if elab scope's stack is empty than add this scope as top level scope for elab scope's stack
      mVerificPrePopInfoWalkerImpl->pushDeclarationScope(name);
      //visit child scopes
      item->Accept(*this);

      //ckeck package declaration
      MapIter mi;
      VhdlPrimaryUnit* primUnit;
      FOREACH_VHDL_PRIMARY_UNIT(lib, mi, primUnit) {
          if (ID_VHDLPACKAGEDECL == primUnit->GetClassId()) {
              //check for std library FIXME
              { //pre
                  // The parent for package is root module. Save the current stacks and
                  // begin at root. NOTE: Nested packages are not legal in VHDL.
                  mVerificPrePopInfoWalkerImpl->saveStacksAndBeginAtRoot();
                  // Push the library declaration/elaborated scope first. The library
                  // name for declaration and elaborated scope are the same.
                  StringAtom* libAtom = mNucleusBuilder->gCache()->intern(libName);
                  mVerificPrePopInfoWalkerImpl->pushDeclarationScope(libAtom);
                  mVerificPrePopInfoWalkerImpl->pushElabScope(libAtom, eNamedDeclScope);
                  // Push the package declaration/elaborated scope first. The package
                  // name for declaration and elaborated scope are the same.
                  UtString packName(primUnit->Name());
                  StringAtom* packAtom = mNucleusBuilder->gCache()->intern(packName);
                  mVerificPrePopInfoWalkerImpl->pushDeclarationScope(packAtom);
                  mVerificPrePopInfoWalkerImpl->pushElabScope(packAtom, eNamedDeclScope);
              }
              {//post
                  // Pop elaborated and declaration scopes for library and package.
                  mVerificPrePopInfoWalkerImpl->popElabScope();
                  mVerificPrePopInfoWalkerImpl->popDeclarationScope();
                  mVerificPrePopInfoWalkerImpl->popElabScope();
                  mVerificPrePopInfoWalkerImpl->popDeclarationScope();
                  // Restore saved stacks and continue where we left off at the beginning of
                  // package declaration.
                  mVerificPrePopInfoWalkerImpl->restoreStacks();
              }
          }
      }
      //process hierRef net's named declaration scopes
      createBranchForHierRef();
      //get out of this scope
      mVerificPrePopInfoWalkerImpl->popDeclarationScope();
  }
}

/**
 * In NU population phase for labeled block which
 * create NUScope object so here we add that scope
 * as an branch node to the symbol table
 * and visit child nodes which can containt scopes
 */
void VerificPrePopInfoWalker::VHDL_VISIT(VhdlBlockStatement, node)
{
  if (node.GetLabel() && ! Verific2NucleusUtilities::isElabCreatedEmpty(&node)) {
      StringAtom* blockName = NULL;
      if (node.GetLabel()->Name()) {
          blockName = mNucleusBuilder->gCache()->intern(node.GetLabel()->Name());
      } else {
          //get it from name registry
          blockName = findName(&node);
      }
      //add branch to the elaborated symbol table and save as top of stack for elaborated scops
      mVerificPrePopInfoWalkerImpl->pushElabScope(blockName, eNamedDeclScope);
  }
  // find scope objects in residual statements in block
  unsigned i;
  VhdlTreeNode* item;
  FOREACH_ARRAY_ITEM(node.GetStatements(), i, item) {
      if ( ! item ) continue;
      item->Accept(*this);
  }
  if (node.GetLabel() && !Verific2NucleusUtilities::isElabCreatedEmpty(&node)) {
      //here we visited all child scopes and time to leave this scope
      mVerificPrePopInfoWalkerImpl->popElabScope();
  }
}

/**
 * Create branch in symbol table corresponding to VhdlProcessStatement's scope
 * Traverse any child of the VhdlProcessStatement which can contain a scope.
 */
 void VerificPrePopInfoWalker::VHDL_VISIT(VhdlProcessStatement, node)
 {
     StringAtom* proc_name = mNucleusBuilder->gCache()->intern("named_decl_lb");
     //add branch to the elaborated and unelaborated symbol table and save as top of stacks for elaborated and unelaborated scops
     mVerificPrePopInfoWalkerImpl->pushDeclarationScope(proc_name);
     mVerificPrePopInfoWalkerImpl->pushElabScope(proc_name, eNamedDeclScope);
     // currently we look in the statement and declaration parts for any scope objects, are there other places to look?
     unsigned i = 0;
     VhdlTreeNode* elem = 0;
     FOREACH_ARRAY_ITEM(node.GetDeclPart(), i, elem) {
       if ( elem )
         elem->Accept(*this);
     }
     i = 0;
     elem = 0;
     FOREACH_ARRAY_ITEM(node.GetStatementPart(), i, elem) {
       if ( elem )
         elem->Accept(*this);
     }
     //here we think we have visited all child scopes and now it is time to leave this scope
     mVerificPrePopInfoWalkerImpl->popElabScope();
     mVerificPrePopInfoWalkerImpl->popDeclarationScope();
}

/**
 * Create branch in symbol table corresponding to componenet instance block
 */
void VerificPrePopInfoWalker::VHDL_VISIT(VhdlComponentInstantiationStatement, node)
{
    const char* label = node.GetLabel()->Name();
    //add branch to the elaborated symbol table and save as top of stack for elaborated scops
    StringAtom* strAtom = mNucleusBuilder->gCache()->intern(UtString(label));
    mVerificPrePopInfoWalkerImpl->pushElabScope(strAtom, eModule);
    //EG: find scpe objects in residual statements in block
    MapIter mi;
    VhdlLibrary* lib;
    FOREACH_VHDL_LIBRARY(mi, lib) {
        UInt32 s = lib->NumOfPrimUnits();
        for (UInt32 i = 0; i < s; ++i) {
            if (UtString(lib->PrimUnitByIndex(i)->Name()) == UtString(node.GetInstantiatedUnit()->Name())) {
                lib->PrimUnitByIndex(i)->Accept(*this);
                break;
            }
        }
    }
    //here we visited all child scopes and time to leve this scope
    mVerificPrePopInfoWalkerImpl->popElabScope();
}

void VerificPrePopInfoWalker::uniquify()
{
    mVerificPrePopInfoWalkerImpl->uniquify();
}

void VerificPrePopInfoWalker::print() const
{
    mVerificPrePopInfoWalkerImpl->print();
}

STBranchNode* VerificPrePopInfoWalker::findElabScope(STBranchNode* parent, StringAtom* instAtom)
{
    return mVerificPrePopInfoWalkerImpl->findElabScope(parent, instAtom);
}

StringAtom*
VerificPrePopInfoWalker::findName(VhdlTreeNode* verificObject)
{
    if (mNameRegistry->find(verificObject) != mNameRegistry->end()) {
        return mNameRegistry->at(verificObject);
    }
    return 0;
}


}
