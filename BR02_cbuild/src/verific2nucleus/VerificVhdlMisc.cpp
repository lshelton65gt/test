// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "verific2nucleus/VerificVhdlDesignWalker.h"    // Visitor base class definition
#include "verific2nucleus/VerificNucleusBuilder.h"      // Nucleus builder class definition
#include "verific2nucleus/V2NDesignScope.h"         // Scope class definition
#include "verific2nucleus/Verific2NucleusUtilities.h"   // Utilities for building nucleus
#include "verific2nucleus/VerificDesignWalkerCB.h" // Included for MACRO VHDL_VISIT_CALL

#include "Array.h"              // Make dynamic array class Array available
#include "Strings.h"            // A string utility/wrapper class
#include "Message.h"            // Make message handlers available, not used in this example

#include "VhdlUnits.h"          // Definition of a libraries and design units
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations
#include "VhdlExpression.h"     // Definitions of all vhdl expressions
#include "VhdlStatement.h"      // Definitions of all vhdl statements
#include "VhdlIdDef.h"          // Definitions of all vhdl identifiers
#include "VhdlName.h"           // Definitions of all vhdl names
#include "VhdlConfiguration.h"  // Definitions of all vhdl configurations
#include "VhdlSpecification.h"  // Definitions of all vhdl specifications
#include "VhdlMisc.h"           // Definitions of miscellaneous vhdl constructrs
#include "VhdlScope.h"
#include "vhdl_tokens.h"
#include "veri_tokens.h"
#include "vhdl_file.h"
#include "veri_file.h"
#include "VhdlValue_Elab.h"

#include "nucleus/NUBase.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUCompositeExpr.h"
#include "util/AtomicCache.h"
#include "util/CarbonAssert.h"
#include "util/UtPair.h"


#include <iostream>             // standart IO

//#define DEBUG_VERIFIC_VHDL_WALKER 1

namespace verific2nucleus {

using namespace Verific ;


/**
 *@brief VhdlArchitectureBody
 *
 * architecture_body ::=
 *          ARCHITECTURE identifier OF entity_name IS
 *                architecture_declarative_part
 *          BEGIN
 *                architecture_statement_part
 *          END [ ARCHITECTURE ] [ architecture_simple_name ] ;
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlArchitectureBody, node)
{
  DEBUG_TRACE_WALKER("VhdlArchitectureBody", node);
  //get the source locator from verific
  SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());  
  //get module form V2NDesignScope
  //which owner was created in VhdlEntityDecl visitor
  // FD thinks: probably we have a guarantee that only module can appear here as a scope
  // FD: whenever possible (we have SourceLocator object) use NUCLEUS_CONSISTENCY* consistency check version it has one action less (source location get)
  NUModule* current_module = getModule();
  NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(current_module, source_locator, "Unable to get module");

  unsigned i = 0;
  VhdlTreeNode *elem = 0 ;
  //set the scope to the current module
  //Construct the declarations. These get put in the registry.
  // FD: Why there isn't NamedDeclarationScope here? Probably need to refactor this as well similarly will need block scope for statement part.
  FOREACH_ARRAY_ITEM(node.GetDeclPart(),i,elem) {
    if (! elem) { 
        continue;
    }
    if (VhdlDeclaration* decl = dynamic_cast<VhdlDeclaration*>(elem)) {
        //skip subporgram decl part
        //subprograms handled in VerificVhdlSubprograms.cpp file
        if (decl->IsSubprogramBody()) {
            continue;
        }
    }
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "architecturebody, GetDeclPart" << std::endl;
#endif
    if (hasError()) {
        mCurrentScope->sValue(0);
        return;
    }
    elem->Accept(*this);
  } 
  //Compile the statements
  FOREACH_ARRAY_ITEM(node.GetStatementPart(),i,elem) {
#ifdef DEBUG_VERIFIC_VHDL_WALKER
      std::cerr << "architecturebody, GetStatementPart" << std::endl;
#endif
      if ( ! elem ) continue;
      elem->Accept(*this);
  }

  //set the value to NULL,side effect stmt
  mCurrentScope -> sValue(NULL);
}

/**
 *@brief VhdlPackageBody
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlPackageBody, node)
{
  DEBUG_TRACE_WALKER("VhdlPackageBody", node);
  (void)(node);
}

/**
 *@brief VhdlUseClause
 * process information like ieee.std_logic_signed.all
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlUseClause, node)
{
    DEBUG_TRACE_WALKER("VhdlUseClause", node);
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(node.GetSelectedNameList(),i,item) {
        VhdlSelectedName* sName = static_cast<VhdlSelectedName*>(item);
        INFO_ASSERT(sName, "Invalid VhdlSelectedName node");
        sName->Name();
    }
}

/**
 *@brief VhdlInstantiateUnit
 *
 * instantiated_unit ::=
 *                [COMPONENT] component_name
 *                | ENTITY entity_name [ ( architecture_identifier ) ]
 *                | CONFIURATION configuration_name
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlInstantiatedUnit, node)
{
    DEBUG_TRACE_WALKER("VhdlInstantiateUnit", node);
    if (node.GetName()) {
        node.GetName()->Accept(*this);
    }
    //set return value to NULL, side effet statement
    mCurrentScope -> sValue(NULL);
}

/**
 *@brief VhdlConfigurationSpec
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlConfigurationSpec, node)
{
  DEBUG_TRACE_WALKER("VhdlConfigurationSpec", node);
  (void)(node);
}

} //namespace verific2nucleus
