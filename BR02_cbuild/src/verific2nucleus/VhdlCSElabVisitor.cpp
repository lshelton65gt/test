// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// project headers
#include "verific2nucleus/VhdlCSElabVisitor.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"
#include "verific2nucleus/CarbonStaticElaborator.h"

// Verific headers
#include "VhdlScope.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "VhdlTreeNode.h"
#include "VeriTreeNode.h"
#include "VhdlExpression.h"
#include "VhdlName.h"
#include "VhdlCopy.h"
#include "VhdlDeclaration.h"
#include "VhdlValue_Elab.h"
#include "Array.h"
#include "VhdlSpecification.h"
#include "VhdlDataFlow_Elab.h"
#include "Set.h"

// Carbon utilities
#include "util/UtIOStream.h"
#include "util/UtList.h"
#include "compiler_driver/CarbonContext.h"

// Other headers

namespace verific2nucleus {
using namespace Verific;

  //=====================================================================================
  // Visit an entity declaration. We get here from top level, or from component 
  // instantiation.
  //
  // Here is what this visitor does:
  //
  // 1) Avoids visiting the same entity more than once. This can happen if the same
  //    entity is instantiated multiple times.
  // 2) Creates a new VhdlDataFlow object (not derived from parent)
  // 3) Visits all architectures associated with this entity (TODO: Perhaps we
  //    should visit ONLY the instantiated architecture)
  //
  // Here's what this visitor does NOT do:
  //
  // 1) Does not visit generic or port clauses, or declarations.

  void VhdlCSElabVisitor::VHDL_VISIT(VhdlEntityDecl, node)
  {
    if (0) // Debug
    {
      UtIO::cout() << "DEBUG: Visiting VhdlCSElabVisitor::VhdlEntityDecl Visitor" << UtIO::endl;
    }
      
    // If we've already visited this entity (instantiated multiple times)
    // then we have nothing to do. Verific uniquifies entities during elaboration.
    if (mVisitedEntities.find(&node) == mVisitedEntities.end()) {
      
      // Make sure we don't visit again. If recursive instantiation is allowed,
      // we might have to be smarter here.
      mVisitedEntities.insert(&node);

      // Save dataflow from parent entity
      VhdlDataFlow* parentDataFlow = mDataFlow;
      mDataFlow = new VhdlDataFlow(0, 0, node.LocalScope());
        
      // This visits all the architectures for this entity
      VHDL_VISIT_NODE(VhdlPrimaryUnit, node);
      
      // Restore parent dataflow
      delete mDataFlow;
      mDataFlow = parentDataFlow;
    }
  }

  //=====================================================================================
  // Component instantiation statement
  //
  // Find instantiated entity, and visit it.

  void VhdlCSElabVisitor::VHDL_VISIT(VhdlComponentInstantiationStatement, node)
  {
    // The default VhdlComponentInstantiationStatement visitor visits the (potentially derived) VhdlStatement visitor.
    // Preserve that behavior here.
    // Some classes derived from VhdlCSElabVisitor depend on it:
    //     (see VhdlSubprogCallUniquifier::VHDL_VISIT(VhdlStatement, node) for one example that uses this feature)
    VHDL_VISIT_NODE(VhdlStatement, node);

    // Traverse generic map aspect
    TraverseArray(node.GetGenericMapAspect()) ;

    // Traverse port map aspect
    TraverseArray(node.GetPortMapAspect()) ;

    VhdlEntityDecl* e = NULL;
    VeriModule* m = NULL;
    Verific2NucleusUtilities::getInstantiatedEntityOrModule(&node, &e, &m);
    if (e) {
      e->Accept(*this);
    } else if (m) {
      UtString msg("The Carbon Static Elaborator does not support mixed language designs");
      reportFailure(&node, msg);
    } else {
      UtString msg("The Carbon Static Elaborator found an unbound entity which is not supported");
      reportFailure(&node, msg);
    }
  }
    
  //=====================================================================================
  // VhdlVariableAssignment assigns constant values to nodes in data flow, for populating
  // constant expressions/constraints in subsequent statements.
  //
  // NOTE: This allows constraints on function args to be calculable in the following
  // example:
  //
  //    temp := 3;
  //    result <= functioncall(slv(temp downto 0));
  //
  // TODO: We are not yet updating the dataflow for VhdlSignalAssignmentStatements,
  // because it's not clear what the semantic would be. Signal assignments don't 
  // immediately take effect, so the subsequent statement in a process (for example)
  // will not and should not see the value. There may be a way to do this with the
  // VhdlDataFlow object; more research required.

  void VhdlCSElabVisitor::VHDL_VISIT(VhdlVariableAssignmentStatement, node) 
  {
    // The default VhdlVariableAssignmentStatement visitor visits the (potentially derived) VhdlStatement visitor.
    // Preserve that behavior here.
    // Some classes derived from VhdlCSElabVisitor depend on it:
    //     (see VhdlSubprogCallUniquifier::VHDL_VISIT(VhdlStatement, node) for one example that uses this feature)
    VHDL_VISIT_NODE(VhdlStatement, node);

    // Compute the right hand side. If it's a constant, value will be non-NULL.

    // evaluate target constraint
    VhdlConstraint* target_constraint = node.GetTarget()->EvaluateConstraintInternal(mDataFlow, 0);
    // evaluate value expression
    VhdlValue* value = node.GetValue()->Evaluate(target_constraint, mDataFlow, 0);
    delete target_constraint;

    // Visit RHS
    node.GetValue()->Accept(*this);
      
    // If we're going to use the value, assign it to the data flow, otherwise
    // clean up.
    // TODO: I'm not sure we're doing enough analysis to set the 'IsDeadCode'
    // flag correctly - is the check here required? 
    if (mDataFlow->IsDeadCode() || node.GetTarget()->IgnoreAssignment()) {
      if (value) delete value;
    } else {
      //save right side constant value in data flow
      //for poplating constant expressions in upcoming statements
      node.GetTarget()->Assign(value, mDataFlow, 0);
    }
  }
    
  //=====================================================================================
  // If statement processing.
  // 
  // The VhdlIfStatement visitor method plays an important role in processing of
  // recursive functions. Without the branch pruning that goes on here, visitor
  // walks of recursive functions would recurse infinitely. This method analyzes
  // 'if' statement conditionals, looking for constant values. If an if conditional
  // is a constant 'true', the branch is guaranteed to be taken, and subsequent
  // branches are short circuited (i.e. they are not visited by this walker, nor
  // will they be populated by the nucleus populator). If an if conditional is a
  // constant 'false', then the branch is considered 'dead', and statements within
  // it will not be visited. If a conditional is not constant, then the branch is
  // considered 'live', and statements within it will be visited.
  // 
  // Examples:
  //
  // if (variable) then
  //    branch1 -- live
  // elsif (false) then
  //    branch2 -- dead
  // elsif (true) then
  //    branch3 -- live
  // elsif (true) then
  //    branch4 -- dead (short circuited by branch3)
  // else
  //    branch5 -- dead (short circuited by branch3, branch4)
  //
  // Notes about dataflows:
  //
  // A separate dataflow is created for each branch. Data flows can be modified by
  // visits to statements in the branch, and it's important that modifications
  // made by the separate branches do not interfere with each other.
  // 
  // A special optimization is made that kicks in if one and only
  // one branch of a conditional is live. If only one branch is live,
  // we use the parent dataflow - we don't create a sub dataflow. 
  // Any id assignments made in that branch will be reflected in the
  // parent data flow, and will be available in subsequent analysis.
  //
  // Dataflows are maintained in an array; this is a requirement of
  // VhdlDataFlow::InvalidateIdValues, which restores/modifies/resets values
  // in the top level dataflow (currently not using this); and makes 
  // cleanup easier.
  //
  // DataFlow TODO: We could probably become much more sophisticated
  // in our dataflow analysis. The code as implemented handles simple
  // situations where id's are assigned constant values in a branch of
  // a conditional, e.g.
  //
  // if (true) then
  //    val := 3;
  // else
  //    val := 0;
  // newval := val;
  //
  // Our analysis will correctly assign 3 to newval.
  //
  // It will also correctly handle this case, computing constraints
  // correctly on each function call:
  //
  // if (variable) then
  //    temp := 3;
  //    out := func_call(vector(temp downto 0));
  // elsif (another_variable) then
  //    temp := 4;
  //    out := func_call(vector(temp downto 0));
  // else
  //    temp := 5;
  //    out := func_call(vector(temp downto 0));
  // end if;
  // 
  //
  // WARNING: The logic implemented here is not the same logic used by the current 
  //          nucleus populator. It should change to match this. Furthermore, the 
  //          nucleus populator does not create separate data flows for each 'elsif' 
  //          - this is a bug.
  //

  void VhdlCSElabVisitor::VHDL_VISIT(VhdlIfStatement, node)
  {
    // The default VhdlIfStatement visitor visits the (potentially derived) VhdlStatement visitor.
    // Preserve that behavior here.
    // Some classes derived from VhdlCSElabVisitor depend on it:
    //     (see VhdlSubprogCallUniquifier::VHDL_VISIT(VhdlStatement, node) for one example that uses this feature)
    VHDL_VISIT_NODE(VhdlStatement, node);

    // Visit ALL the if, elsif statement conditionals first. 
    // This is required by the overloaded operator uniquification
    // algorithm in VhdlSubprogCallUniquifier, which needs to 
    // keep track of the statement in which an overloaded operator
    // reference resides. The 'if' statements 'owns' all of these
    // conditionals.
    if (node.GetIfCondition()) 
      node.GetIfCondition()->Accept(*this);
    VhdlExpression* elsif_cond = NULL;
    unsigned i;
    VhdlElsif* elsif = NULL;
    FOREACH_ARRAY_ITEM(node.GetElsifList(), i, elsif) {
      if (!elsif) continue;
      // We visit VhdlElsif twice but have a guard to visit the elsif conditionals the first time,
      // and the elsif statements the second time.
      // This trick allows derived classes to define their own implementation for VhdlElsif as well as to be able 
      // to call VhdlCSElabVisitor version to do constraint elaboration (accumulate on mDataFlow object)
      mEnableElsifCondWalk = true;
      elsif->Accept(*this);
    }
      
    // Create a container for dataflows. Data flows are stored in an array
    // for ease of subsequent cleanup.
    Array data_flows(2);
    VhdlDataFlow* curDF = mDataFlow;

    bool short_circuited = false; // True if subsequent branches are guaranteed dead

    // True if any branch previous to the one being analyzed 
    // had a constant 'true' condition. Used for VhdlDataFlow
    // management.
    bool previous_visited_branch = false;
      
    // Process 'if' branch
    VhdlTreeNode *stmt =NULL;
    if (Verific2NucleusUtilities::branchIsLive(node.GetIfCondition(), mDataFlow, &short_circuited)) {
      if (short_circuited /* if condition was constant true */ && !previous_visited_branch) {
        // This is the only live branch, no new dataflow required. 
      } else {
        // Create new branch data flow
        mDataFlow = Verific2NucleusUtilities::setupBranchDataFlow(&data_flows, curDF);
      }
      // Visit if statements
      previous_visited_branch = true; // Forces new dataflows to be created
      FOREACH_ARRAY_ITEM(node.GetIfStatements(), i, stmt) {
        if (!stmt) continue;
        stmt->Accept(*this);
      }
    }
      
    // Process 'elsif' branches
    FOREACH_ARRAY_ITEM(node.GetElsifList(), i, elsif) {
      if (!elsif) continue;
      elsif_cond = elsif->Condition();
      // Analyze call tree in 'elsif' condition
      if (Verific2NucleusUtilities::branchIsLive(elsif_cond, mDataFlow, &short_circuited)) {
          if (short_circuited /* elsif condition constant true */ && !previous_visited_branch) {
              // This is the only live branch, no new dataflow required
          } else {
              // Create new branch data flow
              mDataFlow = Verific2NucleusUtilities::setupBranchDataFlow(&data_flows, curDF);
          }
          // visit elsif statements (if any)
          previous_visited_branch = true; // Forces new dataflows to be created
          // Separeted elsif statement walk to enable derived classes to define their own versions of VhdlElsif visitor
          mEnableElsifStmtWalk = true;
          elsif->Accept(*this);
      }
    }

    // Process 'else' branch, if it hasn't been short circuited
    if (!short_circuited) {
      if (!previous_visited_branch) {
        // This is the ONLY live branch, no new dataflow required
      } else if (!Verific2NucleusUtilities::isArrayEmpty(node.GetElseStatments())) {
        // We have an 'else' with statements, so create a new dataflow
        mDataFlow = Verific2NucleusUtilities::setupBranchDataFlow(&data_flows, curDF);
      }
        
      // Visit else statements
      FOREACH_ARRAY_ITEM(node.GetElseStatments(), i, stmt) {
        if (!stmt) continue;
        stmt->Accept(*this);
      }
    }

    // Invalidate id values that were set in branches with non-const conditions
    mDataFlow = curDF;
    Verific2NucleusUtilities::cleanUpDataFlows(&node, &data_flows, curDF);
  }

  void VhdlCSElabVisitor::VHDL_VISIT(VhdlElsif, node)
  {
      // Call of Base class Visit
      //FD: We can think of having tree node visit once although this visitor not used by CarbonStaticElaboration so call is closed for now
      //VHDL_VISIT_NODE(VhdlTreeNode, node) ;

      // Traverse condition
      if (mEnableElsifCondWalk) {
          mEnableElsifCondWalk = false;
          TraverseNode(node.Condition()) ;
      }

      // Traverse statements
      if (mEnableElsifStmtWalk) {
          mEnableElsifStmtWalk = false;
          TraverseArray(node.GetStatements()) ;
      }
  }

  //=====================================================================================
  // Case statement processing.
  
  void VhdlCSElabVisitor::VHDL_VISIT(VhdlCaseStatement, node)
  {
    // The default VhdlIfStatement visitor visits the (potentially derived) VhdlStatement visitor.
    // Preserve that behavior here.
    // Some classes derived from VhdlCSElabVisitor depend on it:
    //     (see VhdlSubprogCallUniquifier::VHDL_VISIT(VhdlStatement, node) for one example that uses this feature)
    VHDL_VISIT_NODE(VhdlStatement, node);
  
    // First, visit the case expression
    VhdlExpression* case_expr = node.GetExpression();
    
    if (case_expr) 
      case_expr->Accept(*this);
    
    VhdlCaseStatementAlternative* others = NULL;
    UtList<VhdlCaseStatementAlternative*> live_alts;
    // Evaluate the case expression, to determine which VhdlCaseAlernative
    // is 'live'. If the expression is a constant, then we can figure out
    // which case alternative is 'live', and visit statements in that one only.
    VhdlValue* cond = case_expr ? case_expr->Evaluate(0, mDataFlow, 0) : NULL;
    if (cond && cond->IsConstant()) {
      // We can determine which case alternative is 'live'.
      VhdlCaseStatementAlternative* alt;
      unsigned i;
      FOREACH_ARRAY_ITEM(node.GetAlternatives(), i, alt) {
        if (!alt) continue;
        // Save away the 'others' choice, in case it is the only 'live' alternative
        if (alt->IsOthers()) {
          others = alt;
        }
        // Loop through the choices
        //    when choice1 | choice2 | choice3 => statement
        unsigned j;
        VhdlDiscreteRange* choice;
        FOREACH_ARRAY_ITEM(alt->GetChoices(), j, choice) {
          if (!choice) continue;
          VhdlValue* choice_val = choice->Evaluate(0, mDataFlow, 0);
          // TODO: Handle ranges here
          if (choice_val && choice_val->IsConstant()) {
            // Figure out if this choice matches case expression.
            // If so, save it for later processing.
            if (choice_val->IsEqual(cond)) {
              live_alts.push_back(alt);
            }
          }
          delete choice_val;
        }
      }
      // Make sure there was not more than one 'live' choice.
      // I don't think this can happen; but am being defensive.
      if (live_alts.size() > 1) {
        UtIO::cout() << "ERROR: Did not expect more than one 'live' choice "
                     << "for case statement in file "
                     << node.Linefile()->GetFileName()
                     << " on line "
                     << node.Linefile()->GetLeftLine()
                     << UtIO::endl;
      } 
      if (live_alts.size() > 0) {
        VhdlCaseStatementAlternative* alt = live_alts.front();
        alt->Accept(*this);
      } else {
        // If we didn't find a 'live' choice, use the 'others'.
        INFO_ASSERT(others, "Unexpected NULL 'others' clause in case statement");
        others->Accept(*this);
      }
      delete cond;
    } else {
      // Non-constant case expression, we can't determine which case item
      // is 'live', so visit all of them, separate dataflow for each.
      Array data_flows(node.GetAlternatives()->Size() + 1);
      VhdlDataFlow* curDF = mDataFlow;
      VhdlCaseStatementAlternative* alt;
      unsigned j;
      FOREACH_ARRAY_ITEM(node.GetAlternatives(), j, alt) {
        if (!alt) continue;
        mDataFlow = Verific2NucleusUtilities::setupBranchDataFlow(&data_flows, curDF);
        alt->Accept(*this);
      }

      // Invalidate id values that were set in branches with non-const conditions
      mDataFlow = curDF;
      Verific2NucleusUtilities::cleanUpDataFlows(&node, &data_flows, curDF);
    }
  }
  
  //===================================================================================
  // Helper Methods below
  //===================================================================================


  //===================================================================================   
  // Visit subprogram body. This method does the following:
  //
  // 1) Given the 'actual' arguments to the subprogram, calculate constraints
  //    on the 'formal' parameters, and place these on the formal ids.
  // 2) Calculate constant argments, and place constant value on the
  //    subprogram formal id. 
  // 3) Create a new dataflow for the subprogram
  // 4) Invoke the visitor on the subprogram body
  // 5) Cleanup the dataflow afterwards.
  //
  // This method places several restrictions on the visitors invoked from
  // within:
  //
  // 1) The visitor cannot add or delete ids from the scope. The scope
  //    PushStack and PopStack methods require exactly the same number
  //    of ids in the scope.
  // 2) The visitor cannot render ids within the scope 'stale'. 
  //    If the scope refers to id 'foo', then the VhdlIdDef for
  //    'foo' cannot be deleted.
  //
  // TODO: This needs more comments. 
  // This is based on VerificVhdlDesignWalker::elaborateSubprogramBody.
  void VhdlCSElabVisitor::walkSubprogramBody(VhdlSubprogramBody* sBody, Array* arguments)
  {
    VhdlSpecification* subprogram_spec = sBody->GetSubprogramSpec();
    INFO_ASSERT(subprogram_spec, "Unexpected NULL subprogram spec in walkSubprogramBody");
    VhdlIdDef *subprog = subprogram_spec->GetId() ;
    INFO_ASSERT(subprog, "Unexpected NULL subprogram id in walkSubprogramBody");

    if (0) { // Debug
      UtIO::cout() << "DEBUG: >>> enter walkSubprogramBody(" << subprog->Name() << ")" << UtIO::endl;
      Verific2NucleusUtilities::dumpPathName(sBody->_path_name);
    }
    
    Map formal_to_constraint(POINTER_HASH) ;
    Map formal_to_value(POINTER_HASH) ;
    Map formal_to_actual(POINTER_HASH) ;

    // Populate 'formal_to_constraint' with formal port constraints
    // Also populates function return constraint
    subprogram_spec->InitializeConstraints(formal_to_constraint) ;

    VhdlDataFlow *subprog_df = new VhdlDataFlow(mDataFlow, 0, subprogram_spec->LocalScope(), 0) ;
    INFO_ASSERT(subprog_df, "Invalid vhdl data flow object") ;
    subprog_df->SetInSubprogramOrProcess() ;

    VhdlDiscreteRange *assoc ;
    VhdlValue *val ;
    VhdlIdDef *id ;
    VhdlConstraint *constraint ;
    unsigned i ;

    // RBS: This container does not seem to be filled anywhere.
    Set ids_with_nonconst_actual(POINTER_HASH) ;
    // RBS thinks: 'ids' here may not match those in 'formal_to_constraints'.
    // Try a test case with functions declared in a separate package.
    FOREACH_ARRAY_ITEM(arguments, i, assoc) {
      if (!assoc) continue ;
      // Get id of formal
      if (assoc->IsAssoc() ) {
        id = assoc->FindFullFormal() ;
        if (!id) {
          break ;
        }
      } else {
        id = subprog->Arg(i) ;
      }
      if (!id) continue ;

      // Retrieve constraint of formal port (if any)
      constraint = (VhdlConstraint*)formal_to_constraint.GetValue(id) ;

      // Update formal constraint, given 'actual', and return VhdlValue for constant actuals.
      // NOTE: If 'actual' is a constant, constraint is not updated.
      val = assoc->Evaluate(constraint, mDataFlow, 0) ;
      if (!val && constraint) {
        // Formal has a constraint, and actual does not have a constant val.
        // The goal of this code is to constrain the formal constraint (if it is unconstrained).
        subprog_df->SetNonconstExpr() ;
        if (constraint && !constraint->IsUnconstrained()) {
          VhdlConstraint *actual_constraint = assoc->EvaluateConstraintInternal(mDataFlow, 0) ;
          if (actual_constraint && !actual_constraint->IsUnconstrained()) {
            // RBS I think: This just checks that the constraint on the 'actual' argument
            // is compatible with the formal. It emits an error if not. TODO: Why
            // are we doing this check. Isn't it already done at parse/elaboration time?
            (void) constraint->CheckAgainst(actual_constraint, assoc, mDataFlow) ;
          }
          delete actual_constraint ;
        } else if (constraint && constraint->IsUnconstrained()) {
          // NOTE: This will return an Unconstrained 'actual_constraint' if 'assoc' is
          // a function call. 
          VhdlConstraint *actual_constraint = assoc->EvaluateConstraintInternal(mDataFlow, 0) ;
          if (actual_constraint && !actual_constraint->IsUnconstrained()) {
            constraint->ConstraintBy(actual_constraint);
          } else {
            delete actual_constraint;
            actual_constraint = NULL;
          }
          // This code evaluates constraints for expressions that Verific cannot handle, 
          // including expressions with multiple operators, and expressions 
          // that have function calls. If the constraint is unconstrained, then that means
          // we couldn't determine the bounds of an array, probably because it is not computable
          // at elaboration time.
          if (!actual_constraint) {
            VhdlExpression *expr = dynamic_cast<VhdlExpression*>(assoc);
            INFO_ASSERT(expr, "Unexpected NULL expression during subprogram argument constraint processing");
            actual_constraint = m_cm->createConstraintFromExpression(expr, constraint, mDataFlow);
            if (!actual_constraint || actual_constraint->IsUnconstrained()) {
              // If we get here, we weren't able to figure out the size of a subprogram argument.
              // As a consequence, we won't be able populate nucleus with the correct argument
              // size for this subprogram call; and constant/constraint propagation within 
              // this subprogram invocation will
              // be less thorough. Specifically, constraints within this subprogram will
              // be unknown, or unconstrained.
              // RBS: TODO: Can we move this check out of here? Maybe to the signature generator?
              // SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,assoc->Linefile());
              // mNucleusBuilder->getMessageContext()->Verific2NUUnconstrainedActual(&src_loc, subprog->Name());
              // setError(true);
              if (0) // DEBUG
                UtIO::cout() << "DEBUG: ERROR Unable to calculate constraint for argument to formal " << id->Name() << " of subprogram " << subprog->Name() << UtIO::endl;
              UtString msg;
              msg << "Unable to calculate constraint for argument to formal '" << id->Name() << "' of subprogram '" << subprog->Name() << "'";
              reportFailure(id, msg);
              // delete actual_constraint;
              // return;
            }
            else 
              constraint->ConstraintBy(actual_constraint);
          }
          delete actual_constraint ;

        }
      }

      // RBS: The Verific copy of this code states that this is a check for LRM Section 2.1.1.2
      // RBS: TODO: Can we eliminate this?
      if (id->IsSignal() && constraint && !constraint->IsUnconstrained()) {
        VhdlConstraint *actual_constraint = assoc->EvaluateConstraintInternal(mDataFlow, 0) ;
        if (actual_constraint && !actual_constraint->IsUnconstrained()) {
          if (constraint->IsRangeConstraint() && actual_constraint->IsRangeConstraint()) {
            VhdlValue *actual_low = actual_constraint->Low() ;
            VhdlValue *actual_high = actual_constraint->High() ;
            if ((constraint->Dir() != actual_constraint->Dir()) ||
                !constraint->Contains(actual_low) ||
                !constraint->Contains(actual_high)) {
              INFO_ASSERT(false, "");
            }
          }
        }
        delete actual_constraint ;
      }
      if (!val) continue ;

      // RBS: By the time we get here, we know we have a constant value
      // for the actual argument.

      // Update constraint of formal given 'actual' constant value.
      if (constraint && constraint->IsUnconstrained()) {
        constraint->ConstraintBy(val) ;
      }

      if (id->IsOutput()) {
        if (id->IsVariable() && constraint) {
          delete val ;
          val = constraint->CreateInitialValue(id) ;
        }
      } else {
        // RBS I think: this check is not needed. Wouldn't this
        // be caught at parse/elaboration time by Verific?
        if (!val->CheckAgainst(constraint,assoc, mDataFlow)) {
          delete val ;
          continue ;
        }
      }

      (void) formal_to_value.Insert(id, val) ;

      if (id->IsSignal() && !id->IsInput()) {
        VhdlDiscreteRange *ref_target = id->GetRefTarget() ;
        (void) formal_to_actual.Insert(id, ref_target) ;

        if (!id->GetRefTarget()) {
          id->SetRefTarget(assoc) ;
          id->SetRefFormal(1) ;
        } else {
          VhdlDiscreteRange *actual = assoc->ActualPart() ;
          VhdlIdDef *full_actual_id = actual ? actual->FindFullFormal() : 0 ;
          VhdlDiscreteRange *ref_actual = full_actual_id ? (VhdlDiscreteRange*)formal_to_actual.GetValue(full_actual_id) : 0 ;
          if (!ref_actual && full_actual_id) ref_actual = full_actual_id->GetRefTarget() ;

          if (ref_actual) {
            id->SetRefTarget(ref_actual) ;
            id->SetRefFormal(1) ;
          } else {
            val = assoc->Evaluate(constraint, mDataFlow, 1) ;
            if (!val) continue ;
            subprog_df->SetAssignValue(id, val) ;
            id->SetRefFormal(0) ;
          }
        }
      }
    }

    VhdlScope *local_scope = subprogram_spec->LocalScope() ;
    // RBS thinks: this, apparently, is pushing a stack of ids, constraints on ids, or something similar. After this call,
    // function/subprogram id constraints are NULL.
    if (local_scope && !local_scope->PushStack()) {
      MapIter mi ;
      FOREACH_MAP_ITEM(&formal_to_constraint, mi, 0, &constraint) delete constraint ;
      FOREACH_MAP_ITEM(&formal_to_value, mi, 0, &val) delete val ;
        
      delete subprog_df ;
      subprog_df = 0;
      UtString msg;
      msg << "Cannot analyze subprogram '" << subprog->Name() << "'";
      reportFailure(subprog, msg);
      return;
    }

    // Set the constraint on each formal, and on the function return value (if this is a function)
    // NOTE: id->SetConstraint deletes the previous constraint, replacing it with the new.
    MapIter mi ;
    FOREACH_MAP_ITEM(&formal_to_constraint, mi, &id, &constraint) {
      id->SetConstraint(constraint) ;
    }
    // Set the constant value (if any) on each formal.
    FOREACH_MAP_ITEM(&formal_to_value, mi, &id, &val) {
      id->SetValue(val) ;
    }
    // Elaborate declaration - sets values and constraints on all declarations
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(sBody->GetDeclPart(), i, decl) {
      decl->Elaborate(subprog_df) ;
    }
    // RBS: 'ids_with_nonconst_actual's does not seem to be
    // setup anywhere. So this loop does nothing.
    SetIter si ;
    FOREACH_SET_ITEM(&ids_with_nonconst_actual, si, &id) {
      if (id) id->SetValue(0) ;
    }

    VhdlDataFlow* oldDF = mDataFlow;
    mDataFlow = subprog_df;

    // Walk the subprogram body
    sBody->Accept(*this);

    mDataFlow = oldDF;
    subprog_df->Resolve(subprog, sBody) ;

    // RBS thinks: This, apparently, is popping a stack of ids, constraints, on ids, or something similar. After this call,
    // the constraints on the subprogram/function argument formal ids is restored to the values they  had before the
    // VerificVhdlDesignWalker visitor 'Accept' call above.
    if (local_scope) {
      (void) local_scope->PopStack();
    }

    // Process assignments to external signals/variables that
    // are contained in the dataflow. See comments in Verific VhdlDeclaration_Elab.cpp
    // for more details.
    FOREACH_MAP_ITEM(subprog_df, mi, &id, &val) {
      if (!local_scope || local_scope->IsDeclaredHere(id)) continue ;
      if (id==subprog) continue ;
      if (mDataFlow) {
        if (mDataFlow && (!mDataFlow->IsInInitial() || mDataFlow->IsInSubprogramOrProcess())) {
          mDataFlow->SetAssignValue(id, val) ;
        } else if (id->Value()) {
          id->Value()->NetAssign(val,id->ResolutionFunction(),sBody) ;
        } else {
          id->SetValue(val) ;
        }
            
        (void) subprog_df->Remove(id) ;
      }
        
      if (subprog->IsProcedure()) {
        FOREACH_ARRAY_ITEM(arguments, i, assoc) {
          if (!assoc) continue ;
                
          if (assoc->IsAssoc() ) {
            id = assoc->FindFullFormal() ;
            if (!id) {
              break ;
            }
          } else {
            id = subprog->Arg(i) ;
          }
          if (!id) continue ;
          if (id->IsInput()) continue ;
                
          val = (VhdlValue*)formal_to_value.GetValue(id) ;
                
          if (id->IsRefFormal()) {
            VhdlDiscreteRange* ref_target = (VhdlDiscreteRange*)formal_to_actual.GetValue(id) ;
            id->SetRefTarget(ref_target) ;
            if (!ref_target) id->SetRefFormal(0) ; // Non Recursive.
            delete val ;
            continue ;
          }
          assoc->Assign(val, mDataFlow, 0) ;
        }
      }
    }
    delete subprog_df ;
    subprog_df = 0;

    if (0) // Debug
      UtIO::cout() << "DEBUG: <<< exit elaborateSubprogramBody(" << subprog->Name() << ")" << UtIO::endl;
  }

  // Return true if this is a function or procedure call that should be traversed. In general,
  // all user defined subprograms should be traversed. There are some builtin subprograms that
  // have no subprogram body that cannot and should not be visited (e.g. some of the file manipulation
  // subprograms). Also, there are some conversion functions in the ieee library that we are treating 
  // as builtin (i.e. the nucleus populator will 'inline' these). If a function evaluates to a constant int,
  // then we don't traverse it. The nucleus populator will just populate the constant value.
  //
  // Also, A subprogram without a subprogram body cannot be traversed.
  bool VhdlCSElabVisitor::isTraversableSubprog(VhdlIndexedName* call_inst)
  {
    INFO_ASSERT(call_inst, "Unexpected null call_inst ptr");

    bool is_traversable = false;

    if (call_inst->IsFunctionCall()) { // true if function or procedure
      is_traversable = true;
      if (Verific2NucleusUtilities::isIEEEPackageBuiltinFunction(call_inst) 
          || Verific2NucleusUtilities::isStdTextioSubprogram(call_inst->GetId())
          || Verific2NucleusUtilities::isIeeeEdgeDetectFunction(call_inst->GetId())
          || (call_inst->GetId()->Body() == NULL)) {
        is_traversable = false;
      } else if (evaluatesToConstantInt(call_inst, mDataFlow)) {
        // The nucleus populator does not visit functions that return a constant int.
        // Instead, it just populates the constant value. 
        is_traversable = false;
      }
    }
    
    return is_traversable;
  }

//=====================================================================================
  // Vhdl Loop Unrolling (this utility should only be used during phases 
  // after loop unrolling has occurred,
  // when all loops that can be unrolled have been, and the only loops
  // that remain are those that cannot be unrolled.)
  void VhdlCSElabVisitor::walkLoopStatementAfterLoopRewrite(VhdlLoopStatement* node)
  {
      // Invoke the statement visitor, we use it to keep track of
      // the parent statement for expression replacement
      VHDL_VISIT_NODE(VhdlStatement, *node);

      // Make sure we visit the iteration scheme. There could be function
      // calls there too.
      if (node->GetIterScheme()) node->GetIterScheme()->Accept(*this);

      // Figure out whether we should/can unroll the loop. See comments
      // in Verific2NucleusUtilities.cpp
      bool forceUnroll = getCarbonContext()->getArgs()->getBoolValue(CarbonContext::scVhdlLoopUnroll);
      if (Verific2NucleusUtilities::loopShouldBeUnrolled(node, mDataFlow, forceUnroll)) {
          // Error if there are loops that can be unrolled
          UtString msg;
          msg << "Unexpected unrollable loop detected. There shouldn't " <<
              "be any unrollable loops during the subprogram call uniquification phase";
          reportFailure(node, msg);
      } else {
          // We're not unrolling the loop, so create a sub data flow, and
          // just visit the statements once
          VhdlDataFlow *curDF = mDataFlow;
          Array data_flows(1);
          mDataFlow = Verific2NucleusUtilities::setupLoopDataFlow(&data_flows, mDataFlow, node);
          unsigned i = 0;
          VhdlTreeNode* stmt = 0;
          FOREACH_ARRAY_ITEM(node->GetStatements(), i, stmt) {
              if (!stmt) continue;
              stmt->Accept(*this);
          }
          // Restore parent dataflow
          Verific2NucleusUtilities::cleanUpDataFlows(node, &data_flows, curDF);
          mDataFlow = curDF;
      }
  }
  // Helper method to invalidate ids in dataflow.
  // Return true if this indexed name/function call evaluates to a constant integer
  bool VhdlCSElabVisitor::evaluatesToConstantInt(VhdlIndexedName* call_inst, VhdlDataFlow* df)
  {
    bool is_constant = false;
    VhdlValue* val = call_inst->Evaluate(0, df, 0);
    if (val && val->IsIntVal()) {
      is_constant = true;
    }
    delete val;
    return is_constant;
  }
  
  // Convenience function to return carbon context
  CarbonContext* VhdlCSElabVisitor::getCarbonContext() 
  {
    return getCarbonStaticElaborator()->getCarbonContext();
  }
}; // namespace verific2nucleus 

