// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "verific2nucleus/VerificVhdlDesignWalker.h"    // Visitor base class definition
#include "verific2nucleus/VerificNucleusBuilder.h"      // Nucleus builder class definition
#include "verific2nucleus/V2NDesignScope.h"         // Scope class definition
#include "verific2nucleus/Verific2NucleusUtilities.h"   // Utilities for building nucleus
#include "verific2nucleus/VerificDesignWalkerCB.h" // Included for MACRO VHDL_VISIT_CALL

#include "Array.h"              // Make dynamic array class Array available
#include "Strings.h"            // A string utility/wrapper class
#include "Message.h"            // Make message handlers available, not used in this example

#include "VhdlUnits.h"          // Definition of a libraries and design units
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations
#include "VhdlExpression.h"     // Definitions of all vhdl expressions
#include "VhdlStatement.h"      // Definitions of all vhdl statements
#include "VhdlIdDef.h"          // Definitions of all vhdl identifiers
#include "VhdlName.h"           // Definitions of all vhdl names
#include "VhdlConfiguration.h"  // Definitions of all vhdl configurations
#include "VhdlSpecification.h"  // Definitions of all vhdl specifications
#include "VhdlMisc.h"           // Definitions of miscellaneous vhdl constructrs
#include "VhdlScope.h"
#include "vhdl_tokens.h"
#include "vhdl_file.h"
#include "veri_file.h"
#include "VhdlValue_Elab.h"

#include "nucleus/NUBase.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUCompositeLvalue.h"
#include "util/AtomicCache.h"
#include "util/CarbonAssert.h"
#include "util/UtIOStream.h"


#include <iostream>             // standard IO

//#define DEBUG_VERIFIC_VHDL_WALKER 1
namespace verific2nucleus {

using namespace Verific ;


// Create a NUConst for a 32-bit signed integer
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlInteger, node)
{
  DEBUG_TRACE_WALKER("VhdlInteger", node);

  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  // create known int constant with 32 bit and always signed
  NUExpr* the_const = NUConst::createKnownIntConst((long)node.GetValue(),32,  false, loc);
  INFO_ASSERT(the_const, "invalid NUExpr object");
  //integer constant always 32 bit and alwas signed
  the_const->setSignedResult(true);
  //save created constant object in current scope for using by caller
  mCurrentScope->sValue(the_const);
}


/**
 *@brief VhdlSelectedWaveform
 *  
 *  selected_waveforms ::= {waveform WHEN choices, }
 *                          waveform WHEN choices
 *
 * This node occurs on the RHS of a selected signal assignment (a concurrent statement).
 *   with <expr> select
 *      <target> <= [<options>] <selected_waveforms>;
 *
 * It has a similar structure to a case statement where the choices are the cases for <expr>
 * and the waveform represents the expression to be assigned to the target under those cases.
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlSelectedWaveform, node)
{
  DEBUG_TRACE_WALKER("VhdlSelectedWaveform", node);

  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  NUBase* base_choice = 0;
  NUBase* base_waveform = 0;
  UtVector<NUBase*> choices;
  unsigned i;
  VhdlTreeNode* item;
  // collect the choices (into the array 'choices'). Each item from GetWhenChoices() can be one of: 'others', a range, an expression
  FOREACH_ARRAY_ITEM(node.GetWhenChoices(), i, item) {
      if (ID_VHDLOTHERS == item->GetClassId()) {
          // There are no choice entries for the 'others' case
          choices.push_back(0);
      } else if (ID_VHDLRANGE == item->GetClassId()) {
          //populate VhdlRange choice
          item->Accept(*this);
          UtVector<NUBase*> vec;
          //for getting more then 1 nucleus object from VhdlRange
          //VhdlRange will provide at most two nucleus or two constant values
          //correspondingly for left / right sides
          mCurrentScope->gValueVector(vec);
          if ( ! ((! vec[0]) && (! vec[1])) ) {
            // getting here meanas that the range is not constant, not currently supported
            UtString msg("Range expression with non-constant expressions");
            mNucleusBuilder->getMessageContext()->UnsupportedLanguageConstruct(&loc, msg.c_str());
            setError(true);
            mCurrentScope->sValue(0);
            return;
          }

          //get left and right constant values for VhdlRange
          UtPair<SInt32, SInt32> p = mCurrentScope->gRange();
          NUExpr* lhs = 0;
          NUExpr* rhs = 0;
          //since we have to create eBiDownTo binary object
          //it means left side should be greater than right side
          //if left > right
          if (p.first > p.second) {
              //create left constant expression
              lhs = NUConst::createKnownIntConst(p.first, 32, false, loc);
              //create right constant expression
              rhs = NUConst::createKnownIntConst(p.second, 32, false, loc);
              // right > left
          } else {
              //create right constant expression
              rhs = NUConst::createKnownIntConst(p.first, 32, false, loc);
              //create left constatn expression
              lhs = NUConst::createKnownIntConst(p.second, 32, false, loc);
          }
          
          INFO_ASSERT(lhs, "Invalid left expression for eBiDownTo"); 
          INFO_ASSERT(rhs, "Invalid right expression for eBiDownTo");
          //set signed because both are 32 bit constants
          lhs->setSignedResult(true);
          rhs->setSignedResult(true);
          //create downto object
          //e.g. (1 to 5) => (5 downto 1)
          base_choice = new NUBinaryOp(NUOp::eBiDownTo, lhs, rhs, loc);
          INFO_ASSERT(base_choice, "Invalid DownTo object for selectedwaveform choice");
          //save the nucleus binary op as the choice
          choices.push_back(base_choice);
      } else {
          //case when choice is an expression

          //populate choice expression
          item->Accept(*this);
          //get populated nucleus object for choice expression
          base_choice = mCurrentScope->gValue();
          INFO_ASSERT(base_choice, "");
          //save the nucleus object as the choice
          choices.push_back(base_choice);
      }
  }
  //In VHDL grammar waveform can contain more when one expression waveform ::= waveform_element {, waveform_element }
  //we currently support only one waveform_element
  //e.g. output <= in1 when 0 to 100

  if (1 != node.GetWaveform()->Size()) {
      mNucleusBuilder->getMessageContext()->Verific2NUUnsupportedWaveform(&loc, node.GetWaveform()->Size());
      mCurrentScope->sValue(NULL);
      return;
  }
  

  FOREACH_ARRAY_ITEM(node.GetWaveform(), i, item) {
      if (ID_VHDLUNAFFECTED == item->GetClassId()) {
          for (UInt32 l = 0; l < choices.size(); ++l) {
              delete choices[l];
          }
          choices.clear();
          return ;
      }
      //waveform expression should be rvalue expression
      //in4 when 1000 downto 600 here waveform is in4
      mCurrentScope->sContext(V2NDesignScope::RVALUE);
      item->Accept(*this);
      //get rvalue object corresponding to waveform
      //e.g. IdentRvalue
      //        VectorNet : in4
      base_waveform = mCurrentScope->gValue();
      INFO_ASSERT(base_waveform, "invalid base pointer");
  }
  //create vector for choicies and waveform elements in following form
  //<choice1, wave1, choice2, wave2, .....>
  UtVector<NUBase*> vec;
  UInt32 s = choices.size();
  for (UInt32 i = 0; i < s; ++i) {
      vec.push_back(choices[i]); 
      if (0 == i) {
          vec.push_back(base_waveform);
      } else {
          //if there is more then 1 choices for the same waveform then
          //copy waveform expression for each possible choice
          //e.g. in5 when 2000 | 3000 | 5000 split into
          //2000, in5, 3000, in5, 5000, in5
          //in this case waveform expression will copyed 2 times
          //for getting uniqe nucleus expression
          if (NUExpr* exp = dynamic_cast<NUExpr*>(base_waveform)) {
              CopyContext cc(NULL, NULL);
              NUExpr* cp_exp = exp->copy(cc);
              INFO_ASSERT(cp_exp, "");
              vec.push_back(cp_exp);
          } else {
              INFO_ASSERT(false, "never can be happen");
          }
      }
  }
  //save vector of choices and waveforms for caller (selectedsignalassignment)
  mCurrentScope->sValueVector(vec);
}


/**
 *@VhdlIndexedName
 *
 * indexed_name ::= prefix (expression {, expression })
 *
 * Note that a subprogram call is parsed as an indexed name.
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlIndexedName, node)
{
  DEBUG_TRACE_WALKER("VhdlIndexedName", node);

    if (hasError()) {
        mCurrentScope->sValue(0);
        return ;
    }

    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    //check for rising_edge and falling_edge
    //implementation for converters in VerificVhdlStatementWalker.cpp file
    //eg rising_edge(clock) - (1 == (clock'event and clk = '1'))
    if (NUExpr* expr = convertRisingEdgeCall2EventExpression(&node)) {
        //save created boolean expression for caller
        mCurrentScope->sValue(expr);
        return ;
        //eg falling_edge(clock) - (1 == (clock'event and clk = '0'))
    } else if (NUExpr* expr = convertFallingEdgeCall2EventExpression(&node)) {
        //save created boolean expression for caller
        mCurrentScope->sValue(expr);
        return ;
    }

    // Check for type conversions
    if (node.IsTypeConversion() || Verific2NucleusUtilities::isIeeeSubprogram(node.GetId())) {
      bool success = typeConversion(node);
      if ( hasError() ) {
        mCurrentScope->sValue(0);
        return;
      }
      if ( success ) {
        return;
      }
    }

    if (attemptConversion_is_x(node) ) {
      return;                   // conversion has been handled, either completed or errored out
    }

    if (attemptConversion_std_match(node) ) {
      return;                   // conversion has been handled, either completed or errored out
    }

    // check for function call
    if (node.IsFunctionCall() && node.GetId() && !node.GetId()->IsProcedure()) {
        // If this is a constant function, returning an int, 
        // don't bother to traverse the function, just populate
        // the constant value. 
        // VhdlCSElabVisitor does not visit functions that return
        // a constant int either. If the code below is extended to
        // include ALL constant return values, then 
        // VhdlCSElabVisitor::IsTraversableSubprog will also have 
        // to change.
        VhdlValue* value = node.Evaluate(0, mDataFlow, 0);
        if (value && value->IsIntVal()) {
            NUExpr* the_const= NUConst::createKnownIntConst(value -> Integer(), 32, false, loc);
            the_const->setSignedResult(true);
            mCurrentScope->sValue(the_const);
        } else {
          VhdlConstraint* oldConstraint = mTargetConstraint;
          mTargetConstraint = NULL; // the current target constraint should not propagate into the function call arguments
          functionCall(node); // in VerificVhdlSubprogramWalker.cpp
          mTargetConstraint = oldConstraint;
        }
        delete value;
    } else if (node.GetId() && node.GetId()->IsProcedure()) {
        // procedure call
        procCall(node); // in VerificVhdlSubprogramWalker.cpp
    } else if (node.IsArrayIndex()) {
        arrayIndex(node); // in VerificVhdlUtil.cpp
    } else if (node.IsArraySlice()) {
        arraySlice(node); // in VerificVhdlUtil.cpp
    }
}

/**
 * @brief VhdlStringLiteral
 *
 * string_literal ::=  " { graphic_character } "
 * e.g. "011U0000X"
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlStringLiteral, node)
{
  DEBUG_TRACE_WALKER("VhdlStringLiteral", node);

    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    UtString str(node.Name());               // create UtString from the StringLiteral
    str = str.substr(1, str.size() - 2);     // then remove first and last \" characters, to make string compatible with NU methods
    if (mCurrentScope->isMemNet()) {
        NUExprVector vec_exp;
        UInt32 s = str.size();
        for (UInt32 i = 0; i < s; ++i) {
            UtString subStr = str.substr(i, 1);
            bool hasNonOneOrZero = subStr.find_first_of("xXzZwWuU-") != UtString::npos; // check this particular bit
            NUExpr* expr = 0;
            if (hasNonOneOrZero) {
                // replace any X, Z, W, U or -  with the constant 0
                expr = NUConst::createKnownConst(UtString("0"), 0, false, loc);
            } else {
                if (UtString("Y") == subStr) { // why search for the character "Y" here? this is nonsense
                    UtString bin_str;
                    UInt32 bit_width = composeBinStringFromString(subStr.c_str(), &bin_str, false);
                    expr = NUConst::createKnownConst(bin_str, bit_width, true, loc);
                } else {
                    //create a constant that is known to have no x's or z's and was defined by a string e.g. "001110"
                    expr = NUConst::createKnownConst(subStr, 1, false, loc);
                }
            }
            INFO_ASSERT(expr, "invalied expression");
            vec_exp.push_back(expr);
        }
        NUConcatOp* conc = new NUConcatOp(vec_exp, 1, loc);
        INFO_ASSERT(conc, "invalid concatenation object");
        mCurrentScope->sValue(conc);
    } else {
        bool hasNonOneOrZero = (str.find_first_of("xXzZwWuU-") != UtString::npos);
        //create corresponding constant expression
        NUExpr* expr = 0;
        if (hasNonOneOrZero) {
            //create a constant for a string that is known to have x's and z's
            expr = NUConst::createXZ(str, false, str.size(), loc);
        } else {
            //create a constant that is known to have no x's or z's and was defined by a string e.g. "001110"
            expr = NUConst::createKnownConst(str, str.size(), false, loc);
        }
        INFO_ASSERT(expr, "invalid expression");
        mCurrentScope->sValue(expr);
    }
}

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlOpen, node)
{
  DEBUG_TRACE_WALKER("VhdlOpen", node);

    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());  
    mNucleusBuilder->getMessageContext()->Verific2NUPortSubElemIsOpen(&loc);
    mCurrentScope->sValue(0);
    setError(true); 
    return ;
}

// Helper function retrieves Nucleus net inside instantiated module for supplied
// formal port.
NUNet* VerificVhdlDesignWalker::getFormalPortNet(VhdlName* formal, bool* notNet)
{
  // Set this to true if the formal is an indexed memory
  *notNet = false;
  
  // Obtain leftmost prefix of formal, and look it up
  // in the instantiated module to obtain the NUNet
  
  VhdlName* item = formal;
  while (item->GetClassId() != ID_VHDLIDREF) {
    item = item->GetPrefix();
  }
  
  // Look up net in instantiated module
  NUModuleInstance* module_instance = mCurrentScope->getModuleInstance();
  NUModule* instantiated_module = module_instance->getModule();
  V2N_INFO_ASSERT(instantiated_module, *formal, "failed to get master module");
  UtString net_name(item->Name());
  if( Verific2NucleusUtilities::smVerificNucleusBuilder->hasSpecialCharacter(net_name) ) {
        INFO_ASSERT(*net_name.begin() == '\\', "");
        INFO_ASSERT(*net_name.rbegin() == '\\', "");
        net_name.erase(net_name.rbegin());
        net_name.erase(net_name.begin());
  }
  NUNet* val = lookupNet(instantiated_module, net_name);

  V2N_INFO_ASSERT(val, *formal, "failed to get net from instantiated module");
  //make sure the net is scoped as we would expect
  NUModule* scope_for_net = val->getScope()->getModule();
  INFO_ASSERT(scope_for_net == instantiated_module, "net scoped correctly");

  // Set notNet to true if this is a slice or index of a memory net. 
  if (val->isMemoryNet() && formal->GetAssocList()) {
    *notNet = true;
  }

  return val;
}

/**
 *@brief VhdlAssocElement
 *
 * association_element ::= [formal_part =>] | actual_part
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlAssocElement, node)
{
  DEBUG_TRACE_WALKER("VhdlAssocElement", node);

  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    bool portIsIndexedMemory = false;
    NUNet* formal = 0;
    NUPortConnection* pc = 0;
    //assoc_element ::= [formal_port => ] | actual_port;
    if (node.FormalPart()) {
        //This is the formal port on the instance: the port
        //on the definition
        //need to get port from instantiated module here
        formal = getFormalPortNet(node.FormalPart(), &portIsIndexedMemory);
    } else {
        UtString errMsg;
        errMsg << "not supported case: formal part for element_assoc does not exist : line" << node.Linefile()->GetLeftLine();
        INFO_ASSERT(formal, errMsg.c_str());
    }
    // population of the formal is complete, now populate the actual
    if (formal && formal->isInput()) {
      mCurrentScope->sContext(V2NDesignScope::RVALUE);
    } else if (formal && formal->isBid()) {
      mCurrentScope->sContext(V2NDesignScope::LVALUE);
    } else {
      // Output
      mCurrentScope->sContext(V2NDesignScope::LVALUE);
    }
    
    INFO_ASSERT(node.ActualPart(), "Invalid element_assoc, there is no specified actual part");

    // Set up the target constraint here, based on the formal. This is required
    // for port connections to array aggregate constants with 'others' in them, 
    // e.g. (others => '0'); this is necessary to know how to size the constant.
    // In particular, getMissingIndexes requires it.
    VhdlConstraint* oldConstraint = mTargetConstraint;
    mTargetConstraint = node.FormalPart()->EvaluateConstraintInternal(0,0);

    NUBase* actualPart = 0;
    if (node.ActualPart()->IsOpen()) {
      // Leave actualPart NULL
    } else {
      //populate actual part
      node.ActualPart()->Accept(*this);
      if (! mCurrentScope->gValue()) {
        mNucleusBuilder->getMessageContext()->Verific2NUPortAndPortMapDoesNotMatch(&loc);
        mCurrentScope->sValue(0);
        setError(true);
        return ;
      }
      actualPart = mCurrentScope->gValue();
    }
    // Restore constraint
    delete mTargetConstraint;
    mTargetConstraint = oldConstraint;
    
    // RBS: The following comment apparently
    // refers to the portIsIndexedMemory flag.

    // special case port map (bin(0) => tin0,
    //                        clkbug => clk,
    //                        bout(0) => tout0);
    // nucleus concat object will added at top
    
    UtString errMsg;
    errMsg << "Invalid formal net for assoc element line " << node.Linefile()->GetLeftLine();
    INFO_ASSERT(formal, errMsg.c_str());
    //check populated actual object
    if (!actualPart) {
      // Open connection (Open allowed only on outputs)
      INFO_ASSERT(formal->isOutput(), "Unexpected input or bidirectional port on open connection");
      // portIsIndexedMemory forces a concatenation to be made for indexed memories.
      // See test/vhdl/lang_memory/bug11624.vhdl
      if (portIsIndexedMemory) {
        // RBS thinks: This assert would only be triggered if
        // an indexed memory is connected to 'open', e.g. mem(1) => open.
        INFO_ASSERT(false, "Port map formal not net for open connection");
      }
      pc = new NUPortConnectionOutput(0, formal, loc);
    } else if (NUVarselRvalue* vselRval = dynamic_cast<NUVarselRvalue*>(actualPart)) {
        pc = new NUPortConnectionInput(vselRval, formal, loc);
    } else if (NULvalue* vselLval = dynamic_cast<NUVarselLvalue*>(actualPart)) {
        if (portIsIndexedMemory) {
            NULvalueVector vec_lval;
            vec_lval.push_back(vselLval);
            vselLval = new NUConcatLvalue(vec_lval, loc);
        }
        if (formal->isBid()) {
          pc = new NUPortConnectionBid(vselLval, formal, loc);
        } else {
          pc = new NUPortConnectionOutput(vselLval, formal, loc);
        }
    } else if (NUExpr* mselRval = dynamic_cast<NUMemselRvalue*>(actualPart)) {
        if (portIsIndexedMemory) {
            NUExprVector vec_exp;
            vec_exp.push_back(mselRval);
            mselRval = new NUConcatOp(vec_exp, 1, loc);
        }
        pc = new NUPortConnectionInput(mselRval, formal, loc);
    } else if (NULvalue* mselLval = dynamic_cast<NUMemselLvalue*>(actualPart)) {
        if (portIsIndexedMemory) {
            NULvalueVector vec_lval;
            vec_lval.push_back(mselLval);
            mselLval = new NUConcatLvalue(vec_lval, loc);
        }
        if (formal->isBid()) {
            pc = new NUPortConnectionBid(mselLval, formal, loc);
        } else {
            pc = new NUPortConnectionOutput(mselLval, formal, loc);
        }
    } else if (NUConst* the_const = dynamic_cast<NUConst*>(actualPart)) {
        // Constants can only be connected to inputs
        pc = new NUPortConnectionInput(the_const, formal, loc);
    } else if (NUExpr* rval = dynamic_cast<NUExpr*>(actualPart)) {
        if (portIsIndexedMemory) {
            NUExprVector vec_exp;
            vec_exp.push_back(rval);
            rval = new NUConcatOp(vec_exp, 1, loc);
        }
        pc = new NUPortConnectionInput(rval, formal, loc);
    } else if (NULvalue* lval = dynamic_cast<NULvalue*>(actualPart)) {
        if (portIsIndexedMemory) {
            NULvalueVector vec_lval;
            vec_lval.push_back(lval);
            lval = new NUConcatLvalue(vec_lval, loc);
        }
        if (formal->isBid()) {
            pc = new NUPortConnectionBid(lval, formal, loc);
        } else {
            pc = new NUPortConnectionOutput(lval, formal, loc);
        }
    } else {
        UtString msg("VhdlAssocElement visitor for 'actual' type: ");
        msg << actualPart->typeStr();
        mNucleusBuilder->getMessageContext()->UnsupportedLanguageConstruct(&loc, msg.c_str());
        setError(true);
        mCurrentScope->sValue(0);
    }
    INFO_ASSERT(pc, "invalid port connection object");
    //set return value to NULL, side effet statement
    mCurrentScope -> sValue(pc);
}

/**
 *@brief VhdlCharacterLiteral
 *
 * character_literal ::= ' graphic_character '
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlCharacterLiteral, node)
{
  DEBUG_TRACE_WALKER("VhdlCharacterLiteral", node);

    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    //convert character_literal into UtString
    UtString str = UtString(node.Name());
    //eliminate last and frist \' simbols
    str = str.substr(1, str.size() - 2);
    //AF: Ed please note
    //char bit_size == 7 in VHDL 87, 7 bits in VHDL93.
    //unsigned char_bit_size = 8;

    //get get corresponding VhdlValue for character_literal
    VhdlValue* cur_value = node.Evaluate(0, mDataFlow, 0);
    //mValues.push_back(cur_value);
    //convert to enumeration value
    VhdlEnum* cur_enum = cur_value -> ToEnum();
    //get definition of enumeration
    VhdlIdDef* enum_def = cur_enum -> GetEnumId();
    //use a binary encoding for enums
    //get number of bits needed for representing character_literal
    unsigned num_bits = bitness( (enum_def -> NumOfEnums()) -1 );
    //Get the encoding for this enum instance
    //check for sign
    bool is_signed = cur_value -> IsSigned() ? true: false;
    NUConst* the_const = 0;
    UtString node_str(node.Name());
    UtString z_str("'Z'");
    UtString little_z_str("'z'");
    UtString x_str("'X'");
    UtString little_x_str("'x'");
    UtString l_str("'L'");
    UtString little_l_str("'l'");
    UtString h_str("'H'");
    UtString little_h_str("'h'");
    UtString one_str("'1'");
    UtString two_str("'0'");
    UtString sub_str("'-'");
    UtString w_str("'W'");
    //custom case for 'Z' character
    if (z_str == node_str ||
	little_z_str == node_str
	) {
        the_const= NUConst::createXZ(UtString("z"), false, 1, loc);
        //changed based on misc/enum6
//        the_const= NUConst::createKnownIntConst(1, 1, false, loc);
        //custom case for 'X' character
    } else if (x_str == node_str || little_x_str == node_str
	       ) {
        the_const= NUConst::createXZ(UtString("x"), false, 1, loc);
    } else if (one_str == node_str || h_str == node_str) {
        the_const= NUConst::createKnownIntConst(1, 1, false, loc);
    } else if (two_str == node_str || l_str == node_str) {
        the_const= NUConst::createKnownIntConst(0, 1, false, loc);
    } else if (sub_str == node_str || w_str == node_str) {
        the_const= NUConst::createXZ(UtString("x"), false, 1, loc);
    } else {
        //is this character_literal part of aggreagete
        if (0 != mCurrentScope->gAggrElemBit()) {
            //use bitness provided by parent aggregate
            // FD: this looks wrong, third argument of createKnownIntConst is 'definedByString' why we pass is_signed ?, 
            // int is always signed
            the_const= NUConst::createKnownIntConst(cur_value -> Integer(), mCurrentScope->gAggrElemBit(), is_signed, loc);
        } else {
            if (cur_value->IsEnumValue()) {
                //create known int constant for enumeratin character literal
                the_const= NUConst::createKnownIntConst(cur_value -> Integer(), num_bits, false, loc);
            } else {
                the_const= NUConst::createKnownIntConst(cur_value -> Integer(), 1, false, loc);
            }
        }
    }
    delete cur_value;
    //save expression constant for caller
    mCurrentScope -> sValue(the_const);
}

/**
 *@brief VhdlDiscreteRange
 *
 * discrete_range ::= discreate_subtype_indication | range
 *
 * Process a range expression (such as 3 downto 0 as a pair of int's and save it in
 * the current scope for consumption by the caller.
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlDiscreteRange,node)
{
  DEBUG_TRACE_WALKER("VhdlDiscreteRange", node);

    // check for integer range
    if (ID_VHDLINTEGER == node.GetLeftExpression()->GetClassId() &&
        ID_VHDLINTEGER == node.GetRightExpression()->GetClassId()) {
        //get left value
        int start = static_cast<VhdlInteger*>(node.GetLeftExpression())->GetValue();
        //get right value
        int end = static_cast<VhdlInteger*>(node.GetRightExpression())->GetValue();
        const UtPair<int, int> p(start, end);
        //save for caller
        mCurrentScope->sRange(p);
    } else {
      SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
      UtString msg("Non-Integer range");
      mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&loc, msg.c_str());
      mCurrentScope->sRange(UtPair<SInt32, SInt32>(0, 0));
      setError(true);
    }
}

/**
 *@brief VhdlExpression
 *
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlExpression, node)
{
  DEBUG_TRACE_WALKER("VhdlExpression", node);

    if (node.IsOpen()) {
        SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
        mNucleusBuilder->getMessageContext()->Verific2NUUnsupportedOpen(&loc);
        mCurrentScope->sValue(0);
        setError(true);
        return ;
    }
}

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlReal, node)
{
  DEBUG_TRACE_WALKER("VhdlReal", node);

  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  CarbonReal dval = node.GetValue();
  NUConst* the_const = NUConst::create( dval, loc );
  V2N_INFO_ASSERT(the_const, node, "invalid real const");
  mCurrentScope->sValue(the_const);
}

/**
 *@brief VhdlQualifiedExpression
 *
 * qualified_expression ::=
 *                 type_mark ' (expression )
 *                 | type_mark ' aggregate
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlQualifiedExpression, node)
{
  DEBUG_TRACE_WALKER("VhdlQualifiedExpression", node);
  VhdlName* type_mark = node.GetPrefix();
  // RBS thinks: I think the code within this 'if' might be trying to
  // detect the case in which the type_mark is unconstrained and the expression 
  // is an aggregate, e.g.type_mark'(others=>0)
  // If it finds this it prints an error
  // 
  // But is this really an error? We should be getting the 
  // constraint of the type_mark, setting mTargetConstraint, and
  // then visiting the aggregate. Otherwise, the aggregate construction
  // cannot work correctly.
  // This code for printing error and the setup for calling Accept below needs a complete rework.
  if ( NULL != type_mark ) {
      VhdlConstraint* constraint = type_mark->EvaluateConstraintInternal(0, 0);
      if (constraint && constraint->IsUnconstrained()) {
          VhdlAggregate* aggregate = dynamic_cast<VhdlAggregate*>(node.GetAggregate());
          if (aggregate) {
              unsigned i = 0;
              VhdlTreeNode* item = 0;
              FOREACH_ARRAY_ITEM(aggregate->GetElementAssocList(), i, item) {
                  VhdlElementAssoc* eAssoc = static_cast<VhdlElementAssoc*>(item);
                  INFO_ASSERT(eAssoc, "invalid VhdlElementAssoc node");
                  if (eAssoc->GetChoices()) {
                      unsigned j = 0;
                      VhdlTreeNode* choice = 0;
                      FOREACH_ARRAY_ITEM(eAssoc->GetChoices(), j, choice) {
                          VhdlName* vName = dynamic_cast<VhdlName*>(choice);
                          if (vName && vName->IsOthers()) {
                              SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
                              mNucleusBuilder->getMessageContext()->Verific2NUOthersInUnconstrainedTarget(&loc);
                              mCurrentScope->sValue(0);
                              setError(true);
                              return ;
                          }
                      }
                  }
              }
          }
      }
      delete constraint;
  }
  if (node.GetAggregate()) {
      //populate aggregate
      node.GetAggregate()->Accept(*this);
  }

  // now check for know qualifiers like 'signed'. (and make sure it comes from the correct library)
  if ( UtString("signed") == type_mark->Name() ){
    // note the type_mark is lowercased by verific
    // RJC thinks we should be checking that this type_mark came from an appropriate library (std-logic-arith or numeric_std) at least
    NUExpr* expr = dynamic_cast<NUExpr*>( mCurrentScope->gValue());
    if ( expr ) {
      expr->setSignedResult(true);
      mCurrentScope->sValue(expr);
    }
  }
}

/**
 *@brief VhdlRange
 *
 * range ::= range attribute_name
 *         | simple_expression direction simple_expression
 *
 * For non-constant values, populate a vector of two NU values representing the range and push it in
 * the current scope for consumption by the caller. If the end point of the range is constant, put NULL
 * into the vector, and populate a UtPair for consumption by the caller.
 * 
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlRange, node)
{
  DEBUG_TRACE_WALKER("VhdlRange", node);

    INFO_ASSERT(node.GetLeftExpression(), "invalid left expr");
    INFO_ASSERT(node.GetRightExpression(), "invalid right expr");
    SInt32 start = 0;
    SInt32 end = 0;
    UtVector<NUBase*> vec;
    NUBase* nu_start = 0;
    NUBase* nu_end = 0;
    VhdlValue* value = node.GetLeftExpression()->Evaluate(0, mDataFlow, 0);
    //check if left expression is constant
    if (value) {
        mValues.push_back(value);
        //left expression is constant
        start = value->Integer();
        delete value;
    } else {
        //left expression isn't constant
        mCurrentScope->sContext(V2NDesignScope::UNKNOWN_CONTEXT);
        //populate left expression
        node.GetLeftExpression()->Accept(*this);
        //get nucleus object corresponding to left expression 
        nu_start = mCurrentScope->gValue();
        INFO_ASSERT(nu_start, "invalid base pointer");
    }
    value = node.GetRightExpression()->Evaluate(0, mDataFlow, 0);
    //check if right expression is constant
    if (value) {
        mValues.push_back(value);
        //right expression is constant
        end = value->Integer();
        delete value;
    } else {
        //right expressin isn't constant
        mCurrentScope->sContext(V2NDesignScope::UNKNOWN_CONTEXT);
        //populate right expression
        node.GetRightExpression()->Accept(*this);
        //get nucleus object corresponding to right expression
        nu_end = mCurrentScope->gValue();
        INFO_ASSERT(nu_end, "invalid base pointer");
    }
    //create UtPair for left and righ constant values when range is constant
    UtPair<SInt32, SInt32> p(start, end);
    //create vector when range is not constant if both side of range are constant then
    //vec will contain two 0s
    vec.push_back(nu_start);
    vec.push_back(nu_end);
    //save constant expressions for left and right for caller
    mCurrentScope->sValueVector(vec);
    //save constant values for left and right for caller
    mCurrentScope->sRange(p);
    //set as null , because of we propogate values using sValueVector and sRange mthods
    mCurrentScope->sValue(NULL);
}

// Get the top object from a name which may be a selected name of indexed name.
/// E.g. given A.B.C(i).D get A
//FD thinks: the way this function jumps back gives an idea how we could create prefix path
// but I don't think having returned topId will actually be able to get all the necessary information to populate 
// composite select structures
VhdlIdDef* VerificVhdlDesignWalker::getTopId(VhdlTreeNode* node)
{
    // Ordering of the checkings in this function now matters, childs should be listed above parents
  if (!node)
    return 0;
  if (VhdlSelectedName* selname = dynamic_cast<VhdlSelectedName*>(node))
  {
    VhdlName* prefix = selname->GetPrefix();
    return getTopId(prefix);
  }
  else if (VhdlIndexedName* indexedName = dynamic_cast<VhdlIndexedName*>(node))
  {
    return getTopId(indexedName->GetId());
  }
  // Old method getting type using classids fail when we have derived class id (VhdlVariableId instead of VhdlIdDef)
  else if (VhdlIdDef* idDef = dynamic_cast<VhdlIdDef*>(node))
  {
    return idDef;
  }
  else if (VhdlIdRef* idref = dynamic_cast<VhdlIdRef*>(node))
  {
    return idref->GetId();
  }
  else if (VhdlName* name = dynamic_cast<VhdlName*>(node))
  {
    return name->GetId();
  }
  return 0;
}

NUCompositeNet* disambiguateCompositeNet(UtArray<NUBase*>& cNets, NUCompositeNet* contextNet)
{
  if (!contextNet)
    return dynamic_cast<NUCompositeNet*>(cNets[0]);

  for (UInt32 i = 0; i < cNets.size(); i++)
  {
    NUBase* net = cNets[i];
    NUCompositeNet* cnet = dynamic_cast<NUCompositeNet*>(net);
    if (!cnet)
      continue;
    if (cnet == contextNet)
      return cnet;
    for (size_t k = 0; k < contextNet->getNumFields(); ++k)
    {
      NUNet* net = contextNet->getField(k);
      if (net == cnet)
        return cnet; 
    }
  }
  return dynamic_cast<NUCompositeNet*>(cNets[0]);
}

/**
 *@brief VhdlSelectedName
 *
 *selected_name ::= prefix . suffix
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlSelectedName, node)
{
  DEBUG_TRACE_WALKER("VhdlSelectedName", node);

    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    VhdlIdDef* id = node.GetId();

    V2N_INFO_ASSERT(id, node, "failed to get VhdlIdDef");

    //skip if node is package eg work.pack
    if (id && id->IsPackage())
    {
        return;
    }

    VhdlName* prefix = node.GetPrefix();
    VhdlName* suffix = node.GetSuffix();
    VhdlIdDef* suffixId = suffix->GetId();
    if (suffixId && suffixId->Type() /*Do we need this ?*/ && suffixId->IsRecordElement()) {
        
        VhdlIdDef* prefixId = prefix->GetId();
        NUBase* resultSelection = NULL;
        NUBase* nu_prefix = NULL;
        UInt32 fieldIndex = 0;
        bool is_constant_record = false;
        if (prefixId) {
            // JCT thinks: not sure why this is here.  Either you can evaluate the expression or you
            // cannot, why are we special casing constant records?  This used to use the value of the
            // prefixId if the prefix could not be evaluated, but that is just wrong:  for example, 
            // dynamic indexing of the constant.
            bool is_constant_node = isConstantNode(prefixId);
            if (is_constant_node) {
                VhdlValue* eval_val = prefix->Evaluate(0, mDataFlow, 0);
                if (eval_val) {
                    is_constant_record = isConstantRecord(eval_val);
                    delete eval_val;
                } else {
                    is_constant_record = false;
                }
            }
        }
        bool got_field_index = false;
        // clear out to avoid side-effects
        mCurrentScope->sValue(NULL);
        if (!is_constant_record) {
            prefix->Accept(*this);
            nu_prefix = mCurrentScope->gValue();
            INFO_ASSERT(nu_prefix, "failed to populate prefix part");
            got_field_index = getCompositeFieldIndex(&node, suffixId, nu_prefix, &fieldIndex);
            INFO_ASSERT(got_field_index, "Unable to get field index");
        }
        if (is_constant_record) {
            evaluateConstantRecordFieldValue(&node, loc);
            NUExpr* suffixVal = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
            resultSelection = suffixVal;
        } else if (NUCompositeIdentRvalue* identExpr=dynamic_cast<NUCompositeIdentRvalue*>(nu_prefix)) {
            NUCompositeNet* cNet = identExpr->getIdent();
            NUNet* resolvedNet = cNet->getField(fieldIndex); 
            // If prefix is a NUIdentRvalue/NUCompositeIdentRvalue (it means identifier, record directly used)
            // then we do populate just selected net wrapped with ident expression (without field select) which has declaration scope 
            // (NUNamedDeclarationScope) of prefix record
            resultSelection = mNucleusBuilder->createIdentRvalue(resolvedNet, loc);
            // We haven't used created index expression  to populate nucleus so delete it
            delete identExpr;
        } else if (NUCompositeIdentLvalue* identLvalue=dynamic_cast<NUCompositeIdentLvalue*>(nu_prefix)) {
            NUCompositeNet* cNet = identLvalue->getCompositeNet();
            NUNet* resolvedNet = cNet->getField(fieldIndex); 
            resultSelection = mNucleusBuilder->createIdentLvalue(resolvedNet, loc);
            delete identLvalue;
        } else if (NUCompositeNet* compositeNet = dynamic_cast<NUCompositeNet*>(nu_prefix)) {
            // There is an existing approach to set no context (UNKNOWN_CONTEXT) in case we want to get a net and construct
            // expression in parent visitor, so for those cases we can get composite net as a prefix net, in that case 
            // we just return net selected from prefix net
            resultSelection = compositeNet->getField(fieldIndex); 
        } else if (NUCompositeSelExpr* selExpr=dynamic_cast<NUCompositeSelExpr*>(nu_prefix)) {
            //e.g. s3(1)(j).f1 (prefix - NUCompositeSelExpr s3(1)(j), suffix - NUCompositeFieldExpr - .f1)
            resultSelection = new NUCompositeFieldExpr(selExpr, fieldIndex, loc);
        } else if (NUCompositeSelLvalue* selLvalue = dynamic_cast<NUCompositeSelLvalue*>(nu_prefix)) {
            //e.g. s3(1)(j).f1 (prefix - NUCompositeSelLvalue s3(1)(j), suffix - NUCompositeFieldLvalue - .f1)
            resultSelection = new NUCompositeFieldLvalue(selLvalue, fieldIndex, loc);
        } else if (NUCompositeFieldExpr* fieldExpr = dynamic_cast<NUCompositeFieldExpr*>(nu_prefix)) {
            //e.g. s5(i + 1).f4.f3 (prefix - NUCompositeFieldExpr - s5(i+1).f4, suffix - NUCompositeFieldExpr - .f3)
            resultSelection = new NUCompositeFieldExpr(fieldExpr, fieldIndex, loc);
        } else if (NUCompositeFieldLvalue* fieldLvalue = dynamic_cast<NUCompositeFieldLvalue*>(nu_prefix)) {
            //e.g. s5(i + 1).f4.f3 (prefix - NUCompositeFieldLvalue - s5(i+1).f4, suffix - NUCompositeFieldLvalue - .f3)
            resultSelection = new NUCompositeFieldLvalue(fieldLvalue, fieldIndex, loc);
        } else {
            // FD thinks: no other selections allowed
            INFO_ASSERT(false, "Unexpected prefix expression type");
        }
        mCurrentScope->sValue(resultSelection);
        return;
    } else if (id->IsFunction()) {

        NUBase* obj = getNucleusObject(id);
        
        if (0 == obj) {
            node.Accept(*this);
        }
    }

    NUBase* obj = getNucleusObject(id);
        
    if (!obj)
    {
        // The id was not found, so it must have been declared in a 
        // package (otherwise, the local declaration for it would have caused
        // an entry to be created in the nucleus object registry).  
        // Create the nucleus declaration for this net in 
        // a named declaration scope within the top module.
        VhdlValue* value = node.Evaluate(0, mDataFlow, 0);
        obj = declareHierRefNet(*id, value);
        if (value) delete value;
    }
    if (mNucleusBuilder->isHierRefIdDef(id)) 
    {
        // This id is declared in a package. Create a hierarchical reference to it.
        NUNet* nonLocalNet = dynamic_cast<NUNet*>(obj);
        AtomArray netHierName;
        createHierName(id, netHierName);
        obj = nonLocalNet->buildHierRef(getModule(), netHierName);
    }
    
    INFO_ASSERT(obj, "failed to get NUBase object");
    NUNet* net = dynamic_cast<NUNet*>(obj);
    V2N_INFO_ASSERT(net, node, "invalid NUNet object");

    if (V2NDesignScope::LVALUE == mCurrentScope->gContext())
    {
        NUBase* lvalue = mNucleusBuilder->createIdentLvalue(net, loc);
        mCurrentScope->sValue(lvalue);
    }
    else if (V2NDesignScope::RVALUE == mCurrentScope->gContext())
    {
        NUBase* rvalue = mNucleusBuilder->createIdentRvalue(net, loc);
        mCurrentScope->sValue(rvalue);
    }
    else
    {
        //FD thinks: shouldn't we assert UNKNOWN_CONTEXT here
        mCurrentScope->sValue(net);
    }
}

// Fills the indexes vector with index value(s) for each entry in the given \target_range that is not mentioned in \elements
// map.
void VerificVhdlDesignWalker::getMissingIndexes(const UtMap<unsigned, NUBase*>& elements, UtVector<unsigned>* indexes, const ConstantRange& target_range)
{
    for (unsigned i = 0; i < target_range.getLength(); ++i) {
        UtMap<unsigned, NUBase*>::const_iterator it = elements.find(i);
        if ( it == elements.end() ) {
          indexes->push_back(i);
        }
    }
}

/**
 *@brief VhdlAggregate
 *
 *aggregate ::= (element_association {, element_association } )
 */

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlAggregate, node)
{
  DEBUG_TRACE_WALKER("VhdlAggregate", node);

    // Message::SetMessageType("VHDL-1174", VERIFIC_IGNORE);
    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    //invalid VhdlAggregate node
    if (!node.GetElementAssocList() || !node.GetAggregateType()) {
        setError(true);
        mCurrentScope->sValue(0);
        mNucleusBuilder->getMessageContext()->Verific2NUInvalidNode(&loc, "aggregate");
        return ;
    }

    // RBS thinks: TODO: Review calculation of target_constraint here, especially in
    // the context of my comment in the VhdlQualifiedExpression visitor.
    // JCT: this needs to be cleaned up.  Use of mTargetConstraint vs. target_constraint is not
    // consistent in this function, and also I believe this type of problem is a fairly widespread
    // one throughout the code.  What is the architecture here?

    VhdlConstraint* delete_this_constraint = node.EvaluateConstraint(mDataFlow, mTargetConstraint);
    VhdlConstraint* target_constraint = delete_this_constraint;
    if (!target_constraint) {
      target_constraint = mTargetConstraint; // do not use: node.GetAggregateType()->Constraint(); it could be the full range of a std_logic_vector which is not what we want here
    }
    
    // if there is no target_constraint then we are in trouble, in some cases we can tell the user why
    if ( !target_constraint && node.IsOthersAggregate() ){
      // cannot use others in this context
      setError(true);
      mNucleusBuilder->getMessageContext()->Verific2NUOthersInNonStaticAggr(&loc);
      mCurrentScope->sValue(0);
      mCurrentScope->sValue(NULL);
      return;
    }

    V2N_INFO_ASSERT(target_constraint, node, "failed to get valid constraint for VhdlAggregate");
    

    // JCT knows:  This code is needlessly complicated and confusing.
    // This should be done in three phases.
    // Phase 1: populate the non-others elements, keep track of them in the elements Map.
    //   . For positional elements, the Map key is the current position as you iterate.
    //   . For named IsAssoc() elements, the Map key is the normalized value of the index.
    // Phase 2: When others is present, handle by populating the default into the elements Map which don't exist.
    // Phase 3: Create the final Nucleus concat by iterating through the elements Map.
    //
    ConstantRange target_range;
    bool is_downto = false;
    if (target_constraint->IsArrayConstraint() or target_constraint->IsRangeConstraint()) {
        // Is an array or range.
        // Use the Position() function to obtain the MSB and LSB, as this works for either numeric ranges or enum ranges.
        if (target_constraint->Dir() == VHDL_downto) {
            target_range = ConstantRange(target_constraint->Left()->Position(), target_constraint->Right()->Position());
            is_downto = true;
        } else {
            target_range = ConstantRange(target_constraint->Right()->Position(), target_constraint->Left()->Position());
            is_downto = false;
        }
    } else {
        // Is a record.  Verific populates the record such that element 0 of the record
        // is the leftmost, so it is a range "0 to Nelements-1"
        V2N_INFO_ASSERT(target_constraint->IsRecordConstraint(), node, "VhdlAggregate constraint is not an array, range, or record");
        target_range = ConstantRange(target_constraint->Length()-1, 0);
        is_downto = false;
    }

    bool is_assoc = false;

    // Map of elements, key == 0 means LSB item.
    UtMap<unsigned, NUBase*> elements;
    VhdlExpression* assoc = 0;
    VhdlValue* value = 0;
    unsigned i = 0;
    FOREACH_ARRAY_ITEM(node.GetElementAssocList(), i, assoc) {
        if (!assoc) continue;

        if (assoc->IsAssoc()) {
            //named element
            if (assoc->IsOthers()) {
                //populate others
                UtVector<unsigned> indexes;
                //computed missing indexes for 'others'
                // FD thinks: here we are trying to handle case where we have aggreate e.g. lval(3 downto 0) = {'1','2',others=>'0'}, 
                // then elements array will contain ('1','2') and size will be 2, mTargetConstraint->Length() will be equal to 4, 
                // and we will fill out last 2 non-explicit elements with value specified in others expression).
                getMissingIndexes(elements, &indexes, target_range);
                unsigned s = indexes.size();
                //populate 'others' element for each missing indexes
                for (unsigned j = 0; j < s; ++j) {
                    mCurrentScope->sAggregateAssocListSize(node.GetElementAssocList()->Size());
                    VhdlConstraint* oldTargetConstraint = mTargetConstraint;
                    mTargetConstraint = target_constraint->ElementConstraintAt(indexes[j]); // use the constraint associated with this index
                    if (! mTargetConstraint ) {
                      // RJC RC#2014/09/26 (cloutier)
                      // this is a hack to get around case where there is no element at named index
                      // 		out1 <= (temp = bool_array'(others => TRUE));
                      // from test/rushc/vhdl/beacon_misc/misc.logic18.vhdl
                      mTargetConstraint = oldTargetConstraint;
                    }
                    mCurrentScope->sAggregateType(true);
                    assoc->Accept(*this);
                    mCurrentScope->sAggregateType(false);
                    mTargetConstraint = oldTargetConstraint;
                    NUBase* base = mCurrentScope->gValue();
                    if (!base) {
                        setError(true);
                        mCurrentScope->sValue(0);
                        mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&loc, "aggregate");
                        delete delete_this_constraint;
                        return ;
                    }
                    elements.insert(UtPair<unsigned, NUBase*>(indexes[j], base));
                }
            } else {
                // JCT thinks: I don't think there is really any difference between this (named element) and the following
                // positional element branch.  Except, where to put the constructed Nucleus object.
                // So, this branch and the following should be merged.
                mCurrentScope->sAggregateAssocListSize(node.GetElementAssocList()->Size());
                VhdlConstraint* oldTargetConstraint = mTargetConstraint;
                mTargetConstraint = target_constraint->ElementConstraintAt(i); // use the constraint associated with this index
                mCurrentScope->sAggregateType(true);
                assoc->Accept(*this);
                mCurrentScope->sAggregateType(false);
                mTargetConstraint = oldTargetConstraint;
                NUBase* base = mCurrentScope->gValue();
                if (!base) {
                    setError(true);
                    mCurrentScope->sValue(0);
                    mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&loc, "aggregate");
                    delete delete_this_constraint;
                    return ;
                }
                if (VhdlConstraint* range = mCurrentScope->gAggregateRange()) {
                    INFO_ASSERT(range->Left(), "invalid left side of range constraint");
                    INFO_ASSERT(range->Right(), "invalid left side of range constraint");
                    ConstantRange cur_range;
                    if (range->Dir() == VHDL_downto) {
                        cur_range = ConstantRange(range->Left()->Integer(), range->Right()->Integer());
                    } else {
                        cur_range = ConstantRange(range->Right()->Integer(), range->Left()->Integer());
                    }
                    for (unsigned j = 0; j < cur_range.getLength(); ++j) {
                        CopyContext copy_context(NULL, NULL);
                        NUBase* rBase = 0;
                        if (V2NDesignScope::LVALUE == mCurrentScope->gContext()) {
                          rBase = static_cast<NULvalue*>(base)->copy(copy_context);
                        } else {
                          rBase = static_cast<NUExpr*>(base)->copy(copy_context);
                        }
                        elements.insert(UtPair<unsigned, NUBase*>(target_range.offsetBounded(cur_range.index(j)), rBase));
                    }
                    delete range; range = 0;
                    mCurrentScope->sAggregateRange(NULL);
                    delete base;
                } else {
                    signed pos = mCurrentScope->getAggregateIndex();
                    elements.insert(UtPair<unsigned, NUBase*>(target_range.offsetBounded(pos), base));
                }
                is_assoc = true;
            }
        } else {
            //positional element
            VhdlConstraint* oldTargetConstraint = mTargetConstraint;
            mTargetConstraint = target_constraint->ElementConstraintAt(i); // use the constraint associated with this index
            value = assoc->Evaluate(mTargetConstraint, mDataFlow, 0);
            if (!value && mDataFlow) mDataFlow->SetNonconstExpr();
            if (value && value->IsComposite() && node.IsAggregateTypeAssoc(i)) {
                INFO_ASSERT(false, "");
            }
            mCurrentScope->sAggregateType(true);
            assoc->Accept(*this);
            mCurrentScope->sAggregateType(false);
            mTargetConstraint = oldTargetConstraint; // Revert back to aggregate constraint.

            NUBase* base = mCurrentScope->gValue();
            if (!base) {
                setError(true);
                mCurrentScope->sValue(0);
                mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&loc, "aggregate");
                delete delete_this_constraint;
                return ;
            }
            signed pos = is_downto ? (target_range.getMsb() - i) : (target_range.getLsb() + i);
            elements.insert(UtPair<unsigned, NUBase*>(target_range.offsetBounded(pos), base));
            if (value) delete value;
        }
    }

    NULvalueVector vLval;
    NUExprVector vExp;

    if (is_downto) {
        // When range is DOWNTO, then LSB (key = 0) item needs to be rightmost (MSB downto LSB).
        for (UtMap<unsigned, NUBase*>::reverse_iterator start = elements.rbegin(), end = elements.rend(); start != end; ++start) {
            if (V2NDesignScope::LVALUE == mCurrentScope->gContext()) {
                INFO_ASSERT(dynamic_cast<NULvalue*>(start->second), "invalid NU object");
                vLval.push_back(static_cast<NULvalue*>(start->second));
            } else {
                INFO_ASSERT(dynamic_cast<NUExpr*>(start->second), "invalid NU object");
                vExp.push_back(static_cast<NUExpr*>(start->second));
            }
        }
    } else {
        // When range is TO, then LSB (key = 0) item needs to be leftmost (LSB to MSB)
        for (UtMap<unsigned, NUBase*>::iterator start = elements.begin(), end = elements.end(); start != end; ++start) {
            if (V2NDesignScope::LVALUE == mCurrentScope->gContext()) {
                INFO_ASSERT(dynamic_cast<NULvalue*>(start->second), "invalid NU object");
                vLval.push_back(static_cast<NULvalue*>(start->second));
            } else {
                INFO_ASSERT(dynamic_cast<NUExpr*>(start->second), "invalid NU object");
                vExp.push_back(static_cast<NUExpr*>(start->second));
            }
        }
    }

    if (V2NDesignScope::LVALUE == mCurrentScope->gContext()) {
        NUConcatLvalue* lConc = new NUConcatLvalue(vLval, loc);
        INFO_ASSERT(lConc, "invalid concat lvalue object");
        mCurrentScope->sValue(lConc);
    } else {
        NUConcatOp* eConc = new NUConcatOp(vExp, 1, loc);
        INFO_ASSERT(eConc, "invalid concat object");
        mCurrentScope->sValue(eConc);
    }
    delete delete_this_constraint;
}

/**
 *@brief VhdlOperator
 *
 * An operator is either a predefined operator such as +, -, * (an intrinsic in the language)
 * or a function that overloads a predefined operator (aka named function) e.g. function "+"
 * These are either unary or binary operators with a left and right expression as operands.
 * For a unary operator, the right expression is NULL.
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlOperator, node)
{
  DEBUG_TRACE_WALKER("VhdlOperator", node);

  if (hasError()) {
      mCurrentScope->sValue(0);
      return ;
  }

  VhdlIdDef* opId = node.GetOperator();

  //operator overload
  if (Verific2NucleusUtilities::isNonIeeeOverloadedOperator(opId)) {
    VhdlSubprogramId* subId = static_cast<VhdlSubprogramId*>(opId);
    UtVector<VhdlExpression*> args;
    if (VhdlExpression* left = node.GetLeftExpression()) {
      args.push_back(left);
    }
    if (VhdlExpression* right = node.GetRightExpression()) {
      args.push_back(right);
    }
    operatorOverload(args, *subId);
    return;
  }

  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  //check for constant expression

  if (evaluateConstantExpression(node)) {
      return;
  }

  //if right expression is null then this is unary expression
  if (!node.GetRightExpression()) {
    handleUnaryOperator(node, loc);
  } else {
    handleBinaryOperator(node, loc);
  }
}

void VerificVhdlDesignWalker::handleUnaryOperator(VhdlOperator& node, SourceLocator loc)
{
    if (node.GetLeftExpression()) {
        // an operator expression is always rvalue
        mCurrentScope->sContext(V2NDesignScope::RVALUE);
        //populate unary operator
        node.GetLeftExpression()->Accept(*this) ;
        //get populated unary expression
        NUExpr* op_expr = static_cast<NUExpr*>(mCurrentScope->gValue());
        if (!op_expr) {
            op_expr = NUConst::createKnownIntConst(0, 32, false, loc);
        }
        // Create unary operation
        //unaryOpKind - get unary operator kind , implemented in VerificVhdlUtil.cpp
        V2N_INFO_ASSERT(NUOp::eInvalid != unaryOpKind(node, op_expr), node, "Failed to get unary operator");
        NUBase* un = new NUUnaryOp(unaryOpKind(node, op_expr), op_expr, loc);
        INFO_ASSERT(un, "Unable to create unary operation");
        //save created unary operator for caller
        mCurrentScope -> sValue(un);
    }
}


/*
 * Binary operator
 * ---------------
 *
 */

void VerificVhdlDesignWalker::handleBinaryOperator(VhdlOperator& node, 
						   SourceLocator loc){
    NUExpr* left_expr = 0;
    NUExpr* right_expr = 0;
    SourceLocator locl; //locator for left expression
    SourceLocator locr; //locator for right expression

#ifdef DEBUG_VERIFIC_VHDL_WALKER
    static int debug = 0;
    debug ++;
    std::cerr << std::endl;
    std::cerr << " " << debug << "\tbinary op\n";
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif

    bool set_left_signed = false;
    bool set_right_signed = false;
    VhdlExpression* left = node.GetLeftExpression();
    VhdlExpression* right = node.GetRightExpression();
    VhdlIdDef* leftId = 0;
    VhdlIdDef* rightId = 0;
    if (left) {
        //get locator for left expression
        locl = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder, left->Linefile());
        // left expression of VhdlOperator is always rvalue
        mCurrentScope->sContext(V2NDesignScope::RVALUE);
        //populate left expression
        left->Accept(*this);
        leftId = left->GetId();
        //get populated left expression
        left_expr = dynamic_cast<NUExpr*>(mCurrentScope->gValue());

        // Steve Lim: 2013-12-07: FIXME: This is hardcoded. What about integer types?
	if (leftId && leftId->Type() && !strcmp(leftId->Type()->Name(), "signed")) {
	  set_left_signed = true;
	  if (NUConst* var = dynamic_cast<NUConst*>(left_expr))
	    var -> setSignedResult(true);
	}

    }
    if (right) {
        //get locator for right expression
        locr = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder, right->Linefile());
        //right expression of VhdlOperator is always rvalue
        mCurrentScope->sContext(V2NDesignScope::RVALUE);
        //populate right expression
        right->Accept(*this);
        rightId = right->GetId();
        //get populated right expression
        right_expr = dynamic_cast<NUExpr*>(mCurrentScope->gValue());

        // Steve Lim: 2013-12-07: FIXME: This is hardcoded. What about integer types?
	if (rightId && rightId->Type() && !strcmp(rightId->Type()-> Name(), "signed")){
	  set_right_signed = true;
	  if (NUConst* var = dynamic_cast<NUConst*>(right_expr))
	    var -> setSignedResult(true);
	}
    }

    //filter relational operators. LRM 7.3
    //handle null ranges and args vectors
    bool left_pattern=false;
    bool right_pattern=false;
      
    // Steve Lim 2013-12-07: Fixed this check for array type
    //if (!left_expr || ((left && dynamic_cast<VhdlIdRef*>(left) && ((dynamic_cast<VhdlIdRef*>(left)) -> GetId() -> Type()-> IsArrayType())) ))
    if (!left_expr || (leftId && leftId->Type()->IsArrayType()))
      left_pattern = true;


    // Steve Lim 2013-12-07: Fixed this check for array type
    //if (!right_expr || ( (right && dynamic_cast<VhdlIdRef*>(right) && ((dynamic_cast<VhdlIdRef*>(right)) -> GetId() -> Type()-> IsArrayType())) ))
    if (!right_expr || (rightId && rightId->Type()->IsArrayType()))
      right_pattern = true;

    if (isRelationalOp(node) &&
	//left and right must be either null range
	//or a legal array value
	 left_pattern && right_pattern
	){
      //handle empty range cases
      if (  (!left_expr ||   !right_expr)){
	(void) handleNullRangeRelationalOp(node,left_expr,right_expr,loc);
	if (left_expr)
	  delete left_expr;
	if (right_expr)
	  delete right_expr;
	return;
      }


      //check for sizes, if differen always set to 0
      // Steve Lim 2013-12-07 Do this only for the equality or inequality operator
      else if (left_expr->getBitSize() != right_expr->getBitSize() &&
               (NUOp::eBiEq == binaryOpKind(node) || NUOp::eBiNeq == binaryOpKind(node))) {
	NUExpr* bin =  0;
        if (NUOp::eBiEq == binaryOpKind(node))
	  bin =  NUConst::createKnownIntConst(0, 1, false,loc);
        else
	  bin =  NUConst::createKnownIntConst(1, 1, false,loc);
	mCurrentScope -> sValue(bin);
	if (left_expr)
	  delete left_expr;
	if (right_expr)
	  delete right_expr;
	return;
      }
    }

    if (hasError()) {
        mCurrentScope->sValue(0);
        return;
    }

    //if one of expressions not exists
    //just create constant 1 and constant 0 for
    //left and right expressions correspondingly
    //it can happen for example in null range experssions
    // eg (in1 (1 downto 4) = in2 (3 to 2))
    if (!left_expr) {
        UInt32 bits = gBits(binaryOpKind(node), right_expr);
        if (right_expr) {
            delete right_expr;
        }
        left_expr = NUConst::createKnownIntConst(1, bits, false, loc);
        right_expr = NUConst::createKnownIntConst(0, bits, false, loc);
    }
    if (! right_expr) {
        UInt32 bits = gBits(binaryOpKind(node), left_expr);
        if (left_expr) {
            delete left_expr;
        }
        left_expr = NUConst::createKnownIntConst(1, bits, false, loc);
        right_expr = NUConst::createKnownIntConst(0, bits, false, loc);
    }
    
    //Concatentation object
    if (NUOp::eNaConcat == binaryOpKind(node)) {
        //example '0' & to_std_logic(in1)
        NUExprVector vec_exp;
        vec_exp.push_back(left_expr);
        vec_exp.push_back(right_expr);
        NUConcatOp* conc = new NUConcatOp(vec_exp, 1, loc);
        INFO_ASSERT(conc, "invalid concatenation object");
        mCurrentScope->sValue(conc);
    } else {
        //special handle for nor and nand
        //eg (a nand b) -> (~(a and b))
        //   (a nor  b) -> (~(a or  b))
        if (VHDL_nor == node.GetOperatorToken() ||
            VHDL_nand == node.GetOperatorToken() ||
            VHDL_xnor == node.GetOperatorToken()) {
            NUUnaryOp* unary = handleNorNandXnor(node, left_expr, right_expr, loc);
            INFO_ASSERT(unary, "invalid unary operator");
            mCurrentScope->sValue(unary);
            return ;
        }
	
        // handle cases where operands (left_expr/right_expr) might need to be resized
        adjustOperandSizes(node, &left_expr, &right_expr);

	//TODO: See section 7 of the lrm
        //use language reference manual rules to get correct
	//sign

	if (set_left_signed)
	  left_expr -> setSignedResult(true);
	if (set_right_signed)
	  right_expr -> setSignedResult(true);
	
        bool isSigned = left_expr->isSignedResult() || right_expr->isSignedResult();
        NUBinaryOp* bin = new NUBinaryOp(binaryOpKind(node, isSigned),
                left_expr, 
                right_expr, 
                loc);

        INFO_ASSERT(bin, "invalid binary operation");
        if (NUOp::eBiEq == binaryOpKind(node, isSigned) ||
                NUOp::eBiNeq == binaryOpKind(node, isSigned)) {
            bin->setSignedResult(false);
        } else {
            bin->setSignedResult(isSigned);
        }
        //save binary operator for caller
        mCurrentScope -> sValue(bin);
    }
}




bool
VerificVhdlDesignWalker::handleNullRangeRelationalOp(VhdlOperator& node,
						     NUExpr* left_expr,
						     NUExpr* right_expr,
						     SourceLocator& loc){

  if (!left_expr && !right_expr){
    //both empty
    //case 1: Equality compares, if null range all equal
    //Note from LRM: < =  is includsive disjunction (ie inclusive or)
    //of baheviour or < , +.
    if (node.GetOperatorToken() == VHDL_EQUAL	||
	node.GetOperatorToken() == VHDL_GEQUAL ||
	node.GetOperatorToken() == VHDL_SEQUAL
	){
      NUExpr* bin =  NUConst::createKnownIntConst(1, 1, false,loc);
      mCurrentScope -> sValue(bin);
      return true;
    }
    else{
      //for non equality operators relation is ill defined
      //for all cases when both are null range
      //so return 0.
      NUExpr* bin =  NUConst::createKnownIntConst(0, 1, false,loc);
      mCurrentScope -> sValue(bin);
      return true;
    }
  }
  else{
    //one argument is a null
    if (left_expr && !right_expr){
      //null right range: greater can return true
      if (node.GetOperatorToken() ==VHDL_GEQUAL ||
	  node.GetOperatorToken() ==VHDL_GTHAN){
	NUExpr* bin =  NUConst::createKnownIntConst(1, 1, false,loc);
	mCurrentScope -> sValue(bin);
	return true;
      }
      else{
	NUExpr* bin =  NUConst::createKnownIntConst(0, 1, false,loc);
	mCurrentScope -> sValue(bin);
	return true;
      }
    }
    else if (!left_expr && right_expr){
      //null left range: less than can return true
      if (node.GetOperatorToken() ==VHDL_SEQUAL ||
	  node.GetOperatorToken() ==VHDL_STHAN){
	NUExpr* bin =  NUConst::createKnownIntConst(1, 1, false,loc);
	mCurrentScope -> sValue(bin);
	return true;
      }
      else{
	NUExpr* bin =  NUConst::createKnownIntConst(0, 1, false,loc);
	mCurrentScope -> sValue(bin);
	return true;
      }
    }
  }
  INFO_ASSERT(0, "dead code");
  return false;
}



bool VerificVhdlDesignWalker::isBinaryOpSigned(VhdlOperator& node, bool isLeftSigned, bool isRightSigned) const
{
    INFO_ASSERT(node.GetLeftExpression() && node.GetRightExpression(), "invalid binary operator");

    if (isRelationalOp(node)) {
        return false;
    }

    switch (node.GetOperatorToken()) {
        case VHDL_rem: return isLeftSigned;
        case VHDL_mod: return isRightSigned;
        case VHDL_EXPONENT: return isLeftSigned;
        default: {
            if (isLeftSigned && isRightSigned) {
                return true;
            } else {
                return false;
            }
        }
    }
    return false;
}

// This is a helper function for 'adjustOperandSizes'. It wraps VhdlExpression:TypeInfer()
// with an extra check for the case in which VhdlExpression is a VhdlAggregate. In
// this case, it uses the precomputed aggregate type, rather than trying to infer
// the type again - which can fail and returns NULL (as explained below). 
// 
// Prior to the addition of this function, the following Verific error was emitted
// by VhldAggregate::TypeInfer:
//
//      Alert 61275: type of aggregate cannot be determined without context ; 
//      0 visible types match here.
//
// This was happening when an aggregate with 'range was appearing in a
// if condition, e.g.:
//
//      if (a = (a'range => '1') then
//
// I might have thought the VhdlAggregate::TypeInfer function would handle
// this directly, but comments within that function explain why they chose
// not to do it. For our purposes, we're only interested in whether this is
// an integer type or not.

VhdlIdDef* VerificVhdlDesignWalker::typeInfer(VhdlExpression* expr)
{
  VhdlIdDef* inferred_type = NULL;
  if (expr) {
    if (expr->IsAggregate()) {
      VhdlAggregate* ag = dynamic_cast<VhdlAggregate*>(expr);
      INFO_ASSERT(ag, "Unexpected NULL VhdlAggregate expr");
      inferred_type = ag->GetAggregateType();
    } else if ( expr->IsQualifiedExpression() ){
      // consider the case where we are trying to get the type of the objects in a array type defined in a package:
      // 	type bool_array is array(4 to 5) of boolean;  // from beacon_misc/misc.logic18.vhdl
      VhdlQualifiedExpression* qualified_expr = dynamic_cast<VhdlQualifiedExpression*>(expr);
      VhdlExpression* aggregate_expression = qualified_expr->GetAggregate();
      VhdlAggregate* ag = dynamic_cast<VhdlAggregate*>(aggregate_expression);
      // aggregate_expression will have a value but ag will be null if the aggregate_expression was something like:
      // STD_LOGIC'('1'), in that kind of case the regular TypeInfer can get the type (std_ulogic)
      if ( ag ) {
        inferred_type = ag->GetAggregateType();
      }
    }
    if ( ! inferred_type ) {
      inferred_type = expr->TypeInfer(0, 0, 0, 0);
    }
  }
  
  return inferred_type;
}


// possibly adjust the size of either of the two operands, based on their type and the operator and type conversion rules
void VerificVhdlDesignWalker::adjustOperandSizes(VhdlOperator& node, NUExpr** op_left, NUExpr** op_right)
{
  bool isSigned = isBinaryOpSigned(node, (*op_left)->isSignedResult(), (*op_right)->isSignedResult()); //based on VhPopulateExpr.cxx:getVhExprSignType

  bool operands_must_be_same_size = isRequiresOpsSameSize(node);
  bool operand_is_relational = isRelationalOp(node);
  
  if ( operands_must_be_same_size ) {
    // first check for potential dirty operands, and if they are found then mask the appropriate bits
    if (!(VHDL_PLUS == node.GetOperatorToken() || VHDL_MINUS == node.GetOperatorToken())) {
      bool dirty1 = isDirty((*op_left));
      bool dirty2 = isDirty((*op_right));
      if (dirty1 || dirty2) {
        UInt32 sz1 = (*op_left)->determineBitSize();
        UInt32 sz2 = (*op_right)->determineBitSize();
        if ((sz1 < sz2 ) && dirty1) {
          (*op_left) = buildMask((*op_left), sz1, (*op_left)->isSignedResult());
          (*op_left)->setSignedResult(isSigned);
        } else if ((sz2 < sz1) && dirty2) {
          (*op_right) = buildMask((*op_right), sz1, (*op_right)->isSignedResult());
          (*op_right)->setSignedResult(isSigned);
        }
      }
    }
  }



  VhdlIdDef* id1 = typeInfer(node.GetLeftExpression());
  VhdlIdDef* id2 = typeInfer(node.GetRightExpression());

  // now what we need to know is if we have operands such that only one is an integer
  bool integer1 = false;
  integer1 |= (id1 && (UtString("integer") == UtString(id1->BaseType()->Name())));
  integer1 |= node.GetLeftExpression()->IsInteger();
  if ( !integer1 ){
    VhdlValue* val = node.GetLeftExpression()->Evaluate(0, mDataFlow, 0);
    integer1 |= (val && (val->IsIntVal()));
    delete val;
  }

  bool integer2 = false;
  integer2 |= (id2 && (UtString("integer") == UtString(id2->BaseType()->Name())));
  integer2 |= node.GetRightExpression()->IsInteger();
  if ( !integer2 ){
    VhdlValue* val = node.GetRightExpression()->Evaluate(0, mDataFlow, 0);
    integer2 |= (val && (val->IsIntVal()));
    delete val;
  }

  if ( operands_must_be_same_size ) {
    // See std_logic_arith.vhd.  When mixing vectors with integers,
    // the size of the vector wins, not the size of the max.
    // Also (Ashenden, pp247-8

    if (integer1 != integer2) {
      if (integer1) {
        (*op_left) = buildMask((*op_left), (*op_right)->determineBitSize(), (*op_left)->isSignedResult());
        (*op_left)->setSignedResult(isSigned);
      } else {
        (*op_right) = buildMask((*op_right), (*op_left)->determineBitSize(), (*op_right)->isSignedResult());
        (*op_right)->setSignedResult(isSigned);
      }
    }
  } else if ( operand_is_relational ) {
    // When mixing vectors with integers the vector sizes are preferred, but
    // if the vector is unsigned, we must WIDEN the vector and integer by one bit
    //
    // If we do widen, then we must widen the RESULT, not the actual computation
    // (bug4027):  ( unsigned(fourbit_a) - unsigned(fourbit_b) - 8) >= 8
    // wants to be computed as 4 bits, zero extended to 5 bits and then compared
    // to a 5 bit 8.

    if (integer1 != integer2) {
      if (integer1) {
        UInt32 prefLen = (*op_right)->determineBitSize();
        bool otherOK = false;
        if (!(*op_right)->isSignedResult()) {
          ++prefLen;
        } else {
          otherOK = true;
        }
        (*op_left) = buildMask((*op_left), prefLen, (*op_left)->isSignedResult());
        if (! otherOK) {
          (*op_right) = buildMask((*op_right), (*op_right)->determineBitSize(), (*op_right)->isSignedResult());
        }
      } else {
        UInt32 prefLen = (*op_left)->determineBitSize();
        bool otherOK = false;
        if (! (*op_left)->isSignedResult()) {
          ++prefLen;
        } else {
          otherOK = true;
        }
        (*op_right) = buildMask((*op_right), prefLen, (*op_right)->isSignedResult());
        if (! otherOK) {
          (*op_left) = buildMask((*op_left), (*op_left)->determineBitSize(), (*op_left)->isSignedResult());
        }
      }
    }
  }
}


/**
 *@brief VhdlWaveformElement
 *
 * waveform_element ::=
 *               value expression [ AFTER time_expression ]
 *               | NULL [ AFTER time_expression ]
 *
 * The RHS of a signal assignment. The time expression is ignored.
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlWaveformElement, node)
{
  DEBUG_TRACE_WALKER("VhdlWaveformElement", node);

    if (node.GetValue()) {
#ifdef DEBUG_VERIFIC_VHDL_WALKER
        std::cerr << "\t GetValue()" << std::endl;
#endif
        node.GetValue()->Accept(*this);
    }
    if (node.GetAfter()) {
        SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
        mNucleusBuilder->getMessageContext()->Verific2NUIgnoreAfter(&loc);
    }
}

/**
 *@brief VhdlConditionalWaveform
 *
 * conditional_waveform ::= { waveform WHEN condition ELSE }
 *                            waveform [ WHEN condition ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlConditionalWaveform, node)
{
  DEBUG_TRACE_WALKER("VhdlConditionalWaveform", node);

//   Message::SetMessageType("VHDL-4038", VERIFIC_IGNORE); // no such message id exists, what was this trying to do?
//   Message::SetMessageType("VHDL-3041", VERIFIC_IGNORE); // no such message id exists, what was this trying to do?
  if (hasError()) {
      mCurrentScope->sValue(NULL);
      return ;
  }
  //eg I0 when S = "00" else
  //   I1 when S = "01" else
  //           "ZZZ"
  //get condition part from waveform
  if (node.Condition()) {
      SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Condition()->Linefile());
      if (ID_VHDLOPERATOR == node.Condition()->GetClassId()) {
          VhdlOperator* op = static_cast<VhdlOperator*>(node.Condition());
          INFO_ASSERT(op, "invalid VhdlOperator");
          VhdlExpression* left = op->GetLeftExpression();
          VhdlExpression* right = op->GetRightExpression();
          if (left && right) {
              if (ID_VHDLOPERATOR == left->GetClassId() &&
                  ID_VHDLATTRIBUTENAME == right->GetClassId()) {
                  mNucleusBuilder->getMessageContext()->Verific2NUAttributeNotOnClock(&loc);
                  mCurrentScope->sValue(NULL);
                  setError(true);
                  return ;
              } else if (ID_VHDLOPERATOR == right->GetClassId() &&
                         ID_VHDLATTRIBUTENAME == left->GetClassId()) {
                  mNucleusBuilder->getMessageContext()->Verific2NUAttributeNotOnClock(&loc);
                  mCurrentScope->sValue(NULL);
                  setError(true);
                  return ;
              }
          }
      }
      
      //condition expression is always rvalue 
      mCurrentScope->sContext(V2NDesignScope::RVALUE);
      //populate condition
      node.Condition()->Accept(*this);
      if (hasError()) {
          mCurrentScope->sValue(NULL);
          return ;
      }
      //get populated conditional expression
      NUBase* base = mCurrentScope->gValue();
      INFO_ASSERT(base, "");
      //cast to nucleus expression
      NUExpr* cond = dynamic_cast<NUExpr*>(base);
      INFO_ASSERT(cond, "");

      //populate waveform part
      unsigned i;
      VhdlTreeNode* item;
      //we only supporting one waveform expression
      if (1 != node.GetWaveform()->Size()) {
          SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
          mNucleusBuilder->getMessageContext()->Verific2NUMultipleWaveform(&loc);
          mCurrentScope->sValue(NULL);
          setError(true);
          return ;
      }
      NUExpr* expr = 0;
      //iterate through all waveform epxressions
      FOREACH_ARRAY_ITEM(node.GetWaveform(), i, item) {
          //waveform expression is always rvalue expression
          mCurrentScope->sContext(V2NDesignScope::RVALUE);
          //populate waveform expression
          item->Accept(*this);
          //get populated waveform element
          NUBase* base = mCurrentScope->gValue();
          INFO_ASSERT(base, "");
          //cast to populated expression into nucleus expression
          expr = dynamic_cast<NUExpr*>(base);
          INFO_ASSERT(expr, "");
      }
      INFO_ASSERT(expr, "");
      UtVector<NUBase*> vec;
      //store populated conditional expression for caller eg. S = "00"
      vec.push_back(cond);
      //store populated waveform expression for caller eg. I0
      vec.push_back(expr);
      //save for caller
      mCurrentScope->sValueVector(vec);
      //we return populated values using sValueVector method ,
      //so there is no need to propogate any value for caller using sValue method
      mCurrentScope->sValue(0);
      return ;
  }

  //here we handle case then condition part is missing
  //in this case we can use sValue method because of we should
  //populate only one nucleus object
  unsigned i;
  VhdlTreeNode* item;
  FOREACH_ARRAY_ITEM(node.GetWaveform(), i, item) {
      item->Accept(*this);
      NUExpr* expr = static_cast<NUExpr*>(mCurrentScope->gValue());
      //Save for caller
      mCurrentScope->sValue(expr);
  }
  //reset content of value vector
  UtVector<NUBase*> vec;
  vec.resize(2);
  vec[0] = 0;
  vec[1] = 0;
  mCurrentScope->sValueVector(vec);
}

/**
 *@brief VhdlElementAssoc
 * element_association ::= [ choices => ] expression
 * choices ::=  choice { | choice }
 * choice ::= simple_expression | discreate_range | element_simple_name | OTHERS
 *
 * An element association occurs as an entry in an aggregate which is a composite
 * value of an array or record type. e.g. 
 *    ( val0, val1, val2, val4, val4)
 *    ( 0 => val0, 1 to 2 => val1, 3 => val2, others => val4)
 *    ( A => val0, B => val1, C => val2)
 * Choices either denote the index or indexes in a range of an array or the fields of a
 * record that the corresponding expression is to be assigned. When absent the elements
 * are ordered by position.
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlElementAssoc, node)
{
  DEBUG_TRACE_WALKER("VhdlElementAssoc", node);

    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder, node.Linefile());
    //test target constraint 
    if (!mTargetConstraint) {
        setError(true);
        mCurrentScope->sValue(0);
        mNucleusBuilder->getMessageContext()->Verific2NUInvalidTargetConstraint(&loc);
        return ;
    }
    //test correctness of node
    if (!node.GetExpression()) {
        setError(true);
        mCurrentScope->sValue(0);
        mNucleusBuilder->getMessageContext()->Verific2NUInvalidNode(&loc, "element_assoc");
        return ;
    }

    NUBase* eBase = 0;
    if ( mDataFlow ) {
      // here we may set the non-const flag on mDataFlow.
      // RJC thinks: not sure if we need this and that there must be a better way to do this but for now leaving alone

      // value is not really the value of the current constraint, but if Evaluate retunrs NULL then we know we have a non-constant expression
      VhdlValue* value = node.GetExpression()->Evaluate(mTargetConstraint, mDataFlow, 0); 
      if ( NULL == value ) {
        mDataFlow->SetNonconstExpr();
      }
      delete value;
    }
    //populate expression
    node.GetExpression()->Accept(*this);
    //get populated expression
    eBase = mCurrentScope->gValue();
    if (!eBase) {
        setError(true);
        mCurrentScope->sValue(0);
        mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&loc, "aggregate");
        return ;
    }
    mCurrentScope->sValue(eBase);

    VhdlDiscreteRange* choice = 0;
    unsigned i = 0;
    signed pos = 0;
    FOREACH_ARRAY_ITEM(node.GetChoices(), i, choice) {
        if (choice->IsOthers()) {
            // if this is 'others' then we just check for correctness of this constraint
            VhdlConstraint* range_constraint = ( (mTargetConstraint && mTargetConstraint->IsArrayConstraint()) ? mTargetConstraint->IndexConstraint() : 0);
            VhdlConstraint* tmp = range_constraint ? range_constraint : mTargetConstraint;
            if (tmp && tmp->IsUnconstrained()) {
                setError(true);
                mCurrentScope->sValue(0);
                mNucleusBuilder->getMessageContext()->Verific2NUInvalidTargetConstraint(&loc);
                return ;
            }
        }

        if (choice->IsRange()) {
            //evaluate range constraint for range choice
            VhdlConstraint* range = choice->EvaluateConstraintInternal(mDataFlow, 0);
            if (!range) {
                setError(true);
                mCurrentScope->sValue(0);
                mNucleusBuilder->getMessageContext()->Verific2NUInvalidChoiceElement(&loc, "");
                return ;
            }
            mCurrentScope->sAggregateRange(range);
        } else {
            //single choice
            VhdlValue* element_pos = choice->Evaluate(0, mDataFlow, 0);
            if (!choice->IsOthers() &&
                (!element_pos || !element_pos->IsConstant())) {
                setError(true);
                mCurrentScope->sValue(0);
                mNucleusBuilder->getMessageContext()->Verific2NUInvalidChoiceElement(&loc, "");
                return ;
            }
            if (element_pos) {
                pos = element_pos->Integer();
            }
            delete element_pos;
            //save element index
            mCurrentScope->setAggregateIndex(pos);
        }
    }
}

/**
 *@brief VhdlAssertionStatement
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlAssertionStatement, node)
{
    DEBUG_TRACE_WALKER("VhdlAssertionStatement", node);
    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder, node.Linefile());
    mNucleusBuilder->getMessageContext()->ConcAssertUnsupported(&loc);
    mCurrentScope->sValue(0);
}

/**
 *@brief VhdlOthers
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlOthers, node)
{
    DEBUG_TRACE_WALKER("VhdlOthers", node);
    (void)node;
    mCurrentScope->sValue(0);
}

/**
 *@brief VhdlAttributeName
 *
 * attribute_name ::= prefix [ signature ] ' attribute_designator [ (expression ) ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlAttributeName, node)
{
  DEBUG_TRACE_WALKER("VhdlAttributeName", node);

  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

  const char* name_str = node.GetDesignator()->Name();
  UtString this_attribute_name = UtString (name_str);

  VhdlName* v_name = node.GetPrefix();
  VhdlIdDef* v_iddef = (v_name ? v_name->GetId() : NULL);



  if ( (UtString("event")  == this_attribute_name) ||
       (UtString("stable") == this_attribute_name)   ){

    // first consider these two special attributes that create NUAttribute specific objects
    // the handling of these two attributes must come early in this method, as later code assumes they will never appear.
    NUExpr* prefix_expr = 0;
    if ( v_name ) {
      mCurrentScope->sContext(V2NDesignScope::RVALUE);      //prefix is always rvalue expression
      mCurrentScope->sValue(NULL);
      v_name->Accept(*this);
      //if we trying to get attribute from type the mCurrentScope will get null prefix value
      if (mCurrentScope->gValue()) {
        prefix_expr = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
        INFO_ASSERT(prefix_expr, "invalid NUExpr object");
      }

      NUAttribute::AttribT attribute_type = NUAttribute::eInvalid;
      if      (UtString("event")  == this_attribute_name) { attribute_type = NUAttribute::eEvent; }
      else if (UtString("stable") == this_attribute_name) { attribute_type = NUAttribute::eStable; }

      NUExpr* expr = new NUAttribute(prefix_expr, attribute_type, loc);
      mCurrentScope->sValue(expr);
      return ;
    } else {
      UtString msg;
      this_attribute_name.uppercase();
      msg << this_attribute_name << " (when applied to a prefix of type: " << node.GetClassId() << ")";
      
      mNucleusBuilder->getMessageContext()->UnsupportedAttribute(&loc, msg.c_str());
      mCurrentScope->sValue(NULL);
      return;
    }
  }


  // now handle attribute applied to a prefix that is a type specification
  if (v_iddef && v_iddef->IsType() &&
      (UtString("range")         != this_attribute_name) &&
      (UtString("reverse_range") != this_attribute_name )) {
    // range and reverse_range not considered here as they make no sense because they require two values

      //get VhdlValue node for attribute name
      VhdlValue* value = node.Evaluate(0, mDataFlow, 0);
      if (value) {
          mValues.push_back(value);
          //create NUConst object according to VhdlValue node 
          //FD correct sign
          bool isSigned = value->IsSigned() ? true : false;
          NUExpr* the_const = NUConst::createKnownIntConst(value->Integer(), 32, isSigned, loc);
          INFO_ASSERT(the_const, "Invalid NUExpr object");

          mCurrentScope -> sValue(the_const);
          delete value;
          return ;
      }
  }

  if (UtString("left")   == this_attribute_name  ||
      UtString("right")  == this_attribute_name  ||
      UtString("low")    == this_attribute_name  ||
      UtString("high")   == this_attribute_name  ||
      UtString("length") == this_attribute_name)   {
      VhdlValue* value = node.Evaluate(0, mDataFlow, 0);
      mValues.push_back(value);
      //Any attribute can be evaluated because it is compile time known value
      INFO_ASSERT(value, "Invalid VhdlValue object");
      // FD correct sign
      bool isSigned = value->IsSigned() ? true : false;
      NUExpr* the_const = NUConst::createKnownIntConst(value->Integer(), 32, isSigned, loc);
      INFO_ASSERT(the_const, "Invalid NUExpr object");
      mCurrentScope -> sValue(the_const);
      delete value;
      return ;
  }

  // now consider the attributes that will be presented with two values
  if ( (UtString("range")         == this_attribute_name) ||
       (UtString("reverse_range") == this_attribute_name)   ) {
      //evaluate constraint for getting left and right values correspondingly
      VhdlConstraint* constraint = node.EvaluateConstraintInternal(0, 0);
      INFO_ASSERT(constraint, "invalid constraint");
      UInt32 left = constraint->Left()->Integer();
      UInt32 right = constraint->Right()->Integer();
      delete constraint;
      //for *range like attributes all that needs to be done is setup the range on the mCurrnetScope

      //save constant left and right values
      mCurrentScope->sRange(UtPair<SInt32, SInt32>(left, right));
      //evaluated values are always constant
      //reset all other propogating methods
      UtVector<NUBase*> vec;
      vec.push_back(0);
      vec.push_back(0);
      mCurrentScope->sValueVector(vec);
      mCurrentScope->sValue(NULL);
      return;
  }
  // if we get here then this is a currently unsupported attribute
  this_attribute_name.uppercase();
  mNucleusBuilder->getMessageContext()->UnsupportedAttribute(&loc, this_attribute_name.c_str());
  mCurrentScope->sValue(NULL);
}

} // namespace verific2nucleus 
