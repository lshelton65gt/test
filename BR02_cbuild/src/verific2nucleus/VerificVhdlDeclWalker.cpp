// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "verific2nucleus/VerificVhdlDesignWalker.h"    // Visitor base class definition
#include "verific2nucleus/VerificNucleusBuilder.h"      // Nucleus builder class definition
#include "verific2nucleus/V2NDesignScope.h"         // Scope class definition
#include "verific2nucleus/Verific2NucleusUtilities.h"   // Utilities for building nucleus
#include "verific2nucleus/VerificDesignWalkerCB.h" // Included for MACRO VHDL_VISIT_CALL

#include "Array.h"              // Make dynamic array class Array available
#include "Strings.h"            // A string utility/wrapper class
#include "Message.h"            // Make message handlers available, not used in this example

#include "VhdlUnits.h"          // Definition of a libraries and design units
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations
#include "VhdlExpression.h"     // Definitions of all vhdl expressions
#include "VhdlStatement.h"      // Definitions of all vhdl statements
#include "VhdlIdDef.h"          // Definitions of all vhdl identifiers
#include "VhdlName.h"           // Definitions of all vhdl names
#include "VhdlConfiguration.h"  // Definitions of all vhdl configurations
#include "VhdlSpecification.h"  // Definitions of all vhdl specifications
#include "VhdlMisc.h"           // Definitions of miscellaneous vhdl constructrs
#include "VhdlScope.h"
#include "VhdlValue_Elab.h"
#include "vhdl_tokens.h"
#include "vhdl_file.h"
#include "veri_file.h"

#include "nucleus/NUBase.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUSysTask.h"
#include "util/AtomicCache.h"
#include "util/CarbonAssert.h"

#include <iostream>             // standard IO
//#define DEBUG_VERIFIC_VHDL_WALKER 1

namespace verific2nucleus {

using namespace Verific ;

/**
 *@brief VhdlInterfaceId
 *
 * VhdlIdDef base class for VhdlInterfaceId
 * which containt all necessary information
 * for populating interface identifier
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlInterfaceId, node)
{
  DEBUG_TRACE_WALKER("VhdlInterfaceId", node);
  VHDL_VISIT_CALL(VhdlIdDef, (VhdlIdDef&)node);
}

/**
 *@brief VhdlConstantId
 *
 * VhdlIdDef base class for VhdlConstantId
 * which containt all necessary information
 * for populating constant identifier
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlConstantId, node)
{
  DEBUG_TRACE_WALKER("VhdlConstantId", node);
  VHDL_VISIT_CALL(VhdlIdDef, (VhdlIdDef&)node);
}

/**
 *@brief VhdlAliasDecl
 *
 * alias_declaration ::=
 *             ALIAS alias_designator [ : subtype_indication ] IS name [ signature ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlAliasDecl ,node)
{
  DEBUG_TRACE_WALKER("VhdlAliasDecl", node);
  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  if (node.GetAliasTargetName()->GetId()->IsFunction()) {
      // Skipping aliases to functions, these are resolved at the point of function call
  } else {
    node.GetDesignator()->Accept(*this);
  }

  if (!mCurrentScope->gValue()) {
      return ;
  }
  NUNet* net = dynamic_cast<NUNet*>(mCurrentScope->gValue());
  VERIFIC_CONSISTENCY_NO_RETURN_VALUE(net, node, "designator for alias should be represented as a NUNet");
  NUScope* declaration_scope = getDeclarationScope();
  NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(declaration_scope, src_loc, "Incorrect scope object");
  mNucleusBuilder->aNetToScope(net, declaration_scope);

  if (NUScope::eTask == declaration_scope->getScopeType()) {
      return ;
  }
  
  NULvalue* lval = mNucleusBuilder->createIdentLvalue(net, src_loc);
  VERIFIC_CONSISTENCY_NO_RETURN_VALUE(node.GetAliasTargetName(), node, "Failed to get alias target name");
  mCurrentScope->sContext(V2NDesignScope::RVALUE);
  node.GetAliasTargetName()->Accept(*this) ;
  NUExpr* expr = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
  if (!expr) {
      delete lval;
      return ;
  }
  VERIFIC_CONSISTENCY_NO_RETURN_VALUE(expr, node, "Alias target should be represented in NU as an NUExpr");
  NUModule* module = getModule();
  NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, src_loc, "Unable to get module");
  NUContAssign* contAssign = mNucleusBuilder->cContinuesAssign(module, lval, expr, src_loc);
  mNucleusBuilder->aContinuesAssignToModule(contAssign, module);
}

/**
 *@brief VhdlEntityDecl
 *
 * entity_decl :=
 *         ENTITY identifier IS
 *                entity_header
 *                entity_declarative_part
 *         [ BEGIN
 *                entity_statement_part ]
 *           END [ ENTITY ] [ entity_simple_name ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlEntityDecl, node)
{
  DEBUG_TRACE_WALKER("VhdlEntityDecl", node);
  //create top-level data flow
  mDataFlow = new VhdlDataFlow(0, 0, node.LocalScope());
  INFO_ASSERT(mDataFlow, "invalid data flow");
  mDFForClean.push_back(mDataFlow);
    
//   Message::SetMessageType("VHDL-4038", VERIFIC_IGNORE); // no such message id exists, what was this trying to do?
  mEntityVisit = true;
  Verific::VhdlTreeNode* item;
  MapIter mi;
  UtString moduleName;
  bool noArch = false;
  if (!mArgArchName.empty()) {
      noArch = true;
  }
  UtString archName("");
  FOREACH_VHDL_SECONDARY_UNIT(&node, mi,item) {
      //create module for each architecture
      {
          VhdlSecondaryUnit* secUnit = static_cast<VhdlSecondaryUnit*>(item);
          INFO_ASSERT(secUnit, "invalid secondary unit node");
          archName = UtString(secUnit->Name());
          if (!mArgArchName.empty() && archName != mArgArchName) {
              continue;
          }
          if (!mArgArchName.empty()) {
              noArch = false;
          }
          VhdlLibrary* lib = secUnit->GetOwningLib();
          INFO_ASSERT(lib, "invalid lib node");
          //create top-level module

          //generate module name
          //module name = <libname>_<entityname>_<archname>
          UtString entityName(node.Id()->Name());

          //RJC RC#2013/09/29 (andy) (edvard) the following appears to be a 'fix' for the problem that verific appends _default to the entity name in some
          //cases. I think the correct solution is not to remove the "_default" but instead to use the name defined by verific as the module
          //name, but also to correctly pass the 'original' name as the second argument to cModule below.  See the verilog call to cModule
          //for how it is done there.  This issue will appear in tests that apply directives to all instances of a module/entity
          
          
          //if entity name has format like this <name>_default
          //we will strip _default for avoiding directive confilict
          if( !VerificNucleusBuilder::hasSpecialCharacter(entityName) ) {
            size_t pos = entityName.find_last_of("_default");
            if (UtString::npos != pos) {
              entityName = entityName.substr(0, entityName.size() - 8);
            }
          }
          UtString libName(lib->Name());
          if (mNucleusBuilder->IsTopModule(entityName)) {
            moduleName = entityName;
          } else {
            moduleName = libName + UtString("_") +  entityName + UtString("_") + archName;
          }
          UtString originalModuleName(moduleName);
          //RJC RC#2013/09/29, I think that we need something like the following:
          // if ( node.GetId()->GetModule()->GetOriginalModuleName() ) { originalModuleName = node.GetId()->GetModule()->GetOriginalModuleName() }
          UtString moduleNuName = VerificNucleusBuilder::gNameAsNucleusName(moduleName);
          NUModule* module = mNucleusBuilder->cModule(0, &node, moduleNuName, originalModuleName, false, gSrcLine(node.Linefile()));
          //top-level scope for tracing hierarchy of blocks
          V2NDesignScope* scope = new V2NDesignScope(module, NULL);
          //save current scope
          //make it top of stack
          pushScope(scope);
          //set current context unknown
          //for ports we will create just net
          //by default if context is unknown populator returns net
          // RBS thinks: this should be done only once - no need to 
          // declare ports for each architecture
          mCurrentScope->sContext(V2NDesignScope::UNKNOWN_CONTEXT);
          unsigned i ;
          Verific::VhdlTreeNode* elem ;
          FOREACH_ARRAY_ITEM(node.GetPortClause(), i, elem) {
              elem->Accept(*this);
          }

          i = 0;
          elem = 0;
          FOREACH_ARRAY_ITEM(node.GetDeclPart(), i, elem) {
              elem->Accept(*this);
          }
      }
      if (hasError()) {
          popScope();
          mCurrentScope->sValue(0);
          return ;
      }
      //populate architecture
      item->Accept(*this);
      //pop of V2NDesignScope corresponding for entity decl 
      popScope();
  }
  
  if (noArch || archName.empty()) {
      setError(true);
      mNucleusBuilder->getMessageContext()->Verific2NUNoArch(mArgArchName.c_str());
      return ;
  }
  // Add pragmas if any
  Map* pragmas =  node.Id()->gPragmas();
  if (pragmas) {
        #ifdef DEBUG_VERIFIC_VHDL_WALKER
            printf("Got %d pragmas\n",pragmas -> Size());
        #endif
      // Get sour location
      SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile()); 
      // TODO Need to discuss what to do with other entity/architecture pairs. Currently using last one
      // module pragmas don't have net name so passing 0
      addPragmas(0, &moduleName, 0, pragmas, false, &source_locator);
  }
  uDesign();
  //pop scope from hierarchy of scopes
  mCurrentScope -> sValue(NULL);
}

void VerificVhdlDesignWalker::cleanModifiedConstraints()
{
    //delete VhdlValues generated by Verific::Evaluate
    UInt32 sz = mDFForClean.size();
    for (UInt32 l = 0; l < sz; ++l) {
        delete mDFForClean[l];
    }
    
    sz = mValues.size();
    for (UInt32 l = 0; l < sz; ++l) {
        //delete mValues[l];
    }

//    cleanActuals();
    
    mValues.clear();
    mDFForClean.clear();
}

/**
 *@brief VhdlInterfaceDecl
 *
 * interface_declaration ::=
 *           interface_constant_declaration
 *           | interface_signal_declaration
 *           | interface_variable_declaration
 *           | interface_file_declaration
 */

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlInterfaceDecl, node)
{
  DEBUG_TRACE_WALKER("VhdlInterfaceDecl", node);
  unsigned i;
  VhdlTreeNode *item ;
  FOREACH_ARRAY_ITEM(node.GetIds(), i, item) {
  // Steve Lim 2013-11-16 Provide the target context for evaluation of the default value
  // of an object declaration.
      if (node.GetInitAssign()) {
          VhdlIdDef* id = static_cast<VhdlIdDef*>(node.GetIds()->At(i));
          if (1) { //!id->IsPort() || id->IsOutput()) {
            VhdlValue* value = node.GetInitAssign()->Evaluate(id->Constraint(), mDataFlow, 0);
            if (value) {
              id->SetValue(value);
            }
          }
      }

      //populate interface id
      item->Accept(*this);
      if (hasError()) {
          mCurrentScope->sValue(NULL);
          return ;
      }
      //interface id always populate NUNet
      NUNet* net = dynamic_cast<NUNet*>(mCurrentScope->gValue());
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(net, node, "Unable to get net");
      // FD thinks: Why not just getUnitScope() ? Unless we want to check that there is no intermediate NUNamedDeclarationScope added
      NUScope* declaration_scope = getDeclarationScope();
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(declaration_scope, node, "Unable to get declaration scope"); 
      //add to context
      if (NUScope::eTask == declaration_scope->getScopeType()) {
          NUTask* task = dynamic_cast<NUTask*>(declaration_scope);
          VERIFIC_CONSISTENCY_NO_RETURN_VALUE(task, node, "Unable to get task");
          if (static_cast<VhdlIdDef*>(item)->IsSignal()) {
              task->addArg(net, NUTF::eCallByReference);
              task->putRequestInline(true);
          } else {
              task->addArg(net, NUTF::eCallByAny);
          }
      } else {
          VERIFIC_CONSISTENCY_NO_RETURN_VALUE(NUScope::eModule == declaration_scope->getScopeType(), node, "Incorrect module type set");
          NUModule* module = dynamic_cast<NUModule*>(declaration_scope);
          VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, node, "Unable to get module");
          module->addPort(net);
      }
  }
  //no result for caller
  //we added populated net in context
  mCurrentScope -> sValue(NULL);
}

/**
 *@brief VhdlSignalId
 *
 * VhdlIdDef base class for VhdlSignalId
 * which containt all necessary information
 * for populating signal identifier
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlSignalId, node)
{
  DEBUG_TRACE_WALKER("VhdlSignalId", node);
  VHDL_VISIT_CALL(VhdlIdDef, (VhdlIdDef&)node);
}

/**
 *@brief VhdlEnumerationId
 *
 * VhdlIdDef base class for VhdlEnumerationId
 * which containt all necessary information
 * for populating enumeration identifier
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlEnumerationId, node)
{
  DEBUG_TRACE_WALKER("VhdlEnumerationId", node);
  //implemented in base class.
  VHDL_VISIT_CALL(VhdlIdDef, (VhdlIdDef&)node);
}

/**
 *@brief VhdlSignalDecl
 *
 * signal_declaration ::=
 *                 signal edentifier_list : subtype_indication [ signal_kind ] [ := expression ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlSignalDecl, node)
{
  DEBUG_TRACE_WALKER("VhdlSignalDecl", node);
  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile()); 
  //Lilit here is where you can get the pragmas
  Map* pragmas =  static_cast<VhdlIdDef*>(node.GetIds()->GetLast())->gPragmas();
  if (pragmas) {
        #ifdef DEBUG_VERIFIC_VHDL_WALKER
            printf("Got %d pragmas\n",pragmas -> Size());
        #endif
        // Get module name
      NUModule* module = getModule();
      NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, loc, "Unable to get module");
      UtString moduleName = UtString(module->getName()->str());
      // Get net name, if multiple declarations found directive applied to the last one
      UtString netName(static_cast<VhdlIdDef*>(node.GetIds()->GetLast())->Name());
      addPragmas(&netName, &moduleName, 0, pragmas, false, &loc);
  }
  unsigned i;
  VhdlTreeNode* elem;
  NUNet* net = 0;
  FOREACH_ARRAY_ITEM(node.GetIds(), i, elem) {
      elem->Accept(*this);
      //signalid always populate NUNet
      //FD I think: here should not be such code, why can't we just add net in the place net gets created  ?
      // (instead of forcing every context to add net to scope). In verific verilog population flow 
      // VeriDataDecl and VeriAnsiPortDecl are responsible to create and add net to scope, here we can choose similar approach
      net = dynamic_cast<NUNet*>(mCurrentScope->gValue());
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(net, *elem, "Unable to get net");
      //add populated NUNet to the current context
      NUScope* declaration_scope = getDeclarationScope();
      NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(declaration_scope, loc, "Unable to get declaration_scope");
      mNucleusBuilder->aNetToScope(net, declaration_scope);
  }

  //if initial expression is exists
  //populate them and add to initial block
  //if initial block doesn't exists create them
  if (node.GetInitAssign()) {
      VhdlConstraint* constraint = node.GetSubtypeIndication()->EvaluateConstraintInternal(mDataFlow, 0);
      if (!constraint || constraint->IsUnconstrained()) {
          mNucleusBuilder->getMessageContext()->Verific2NUPopulateError(&loc);
          mCurrentScope->sValue(NULL);
          setError(true);
          return ;
      }
      VhdlConstraint* oldConstraint = mTargetConstraint;
      mTargetConstraint = constraint;
      initAssign(node.GetInitAssign(), net);
      mTargetConstraint = oldConstraint;
      delete constraint;
  }

  //set return value to NULL, side effect statement
  mCurrentScope -> sValue(NULL);
}

/**
 *@brief VhdlFileDecl
 * 
 * file_declaration ::=
 *           FILE identifier_list : subtype_indication [ file_open_information ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlFileDecl, node)
{
    DEBUG_TRACE_WALKER("VhdlFileDecl", node);
    SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    // RJC RC#15 is the following static cast safe? Don't we want to be able to set the current scope to be NUTasks as well as NUModules and NUNamedDeclarationScope
    //ADDRESSED
    unsigned i;
    VhdlTreeNode* elem;
    NUNet* net = 0;
    FOREACH_ARRAY_ITEM(node.GetIds(), i, elem) {
        elem->Accept(*this);
        //for filedecl id we always populate NUNet
        net = dynamic_cast<NUNet*>(mCurrentScope->gValue());
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(net, *elem, "Unable to get net");
        // get declaraton scope
        NUScope* declaration_scope = getDeclarationScope();
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(declaration_scope, source_locator, "Unable to get declaration scope");
        // add net to scope
        mNucleusBuilder->aNetToScope(net, declaration_scope);
        if (node.GetFileOpenInfo()) {
            // now setup initial block with system call to open the file (with correct mode)
            node.GetFileOpenInfo()->Accept(*this);
        }
    }
}

/**
 *@brief VhdlFileId
 *
 * VhdlIdDef base class for VhdlFileId
 * which containt all necessary information
 * for populating file identifier
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlFileId, node)
{
    DEBUG_TRACE_WALKER("VhdlFileId", node);
    VHDL_VISIT_CALL(VhdlIdDef, (VhdlIdDef&)node);
}

/**
 * @brief VhdlConstantDecl
 *
 * constant_declaration ::=
 *               CONSTANT identifier_list : subtype_indication [ := expression ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlConstantDecl, node)
{
    DEBUG_TRACE_WALKER("VhdlConstantDecl", node);
    unsigned i;
    VhdlTreeNode* item;
    NUNet* net = 0;
    bool withinTask = getTask(); // this is the current way to determine if we are within a task (get nearest task)

    FOREACH_ARRAY_ITEM(node.GetIds(), i, item) {
        VhdlIdDef* idDef = static_cast<VhdlIdDef*>(item);
        V2N_INFO_ASSERT(idDef, node, "Variable Declaration without ID");
        item->Accept(*this);
        if (hasError()) {
            return;
        }

        //for constant decl id we always populate NUNet
        net = dynamic_cast<NUNet*>(mCurrentScope->gValue());
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(net, node, "invalid net");
        NUScope* declaration_scope = getDeclarationScope();
        if (withinTask) {
            // FD thinks: this is dirty, but there is no defined structure for settting net flags in VHDL (not using VerificNucleusBuilder::cNet routines yet)
            // all nets under task should be non-static (despite the fact that they appear inside nested named declaration scope)
            mNucleusBuilder->setNetFlag(net, eNonStaticNet);
        }
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(declaration_scope, node, "Unable to get declaration scope");
        mNucleusBuilder->aNetToScope(net, declaration_scope);
        if (node.GetInitAssign()) {
            // if we are in a module then create an assignment in the initial block for the current module.
            // if we are in a subprogram/task/function then the assignment must remain in that subprogram/task/function
            VhdlValue* val = node.GetInitAssign()->Evaluate(idDef->Constraint(), mDataFlow, 0);
            if (val) {
              char* image = val->Image();
              UtString str(image);
              if (!(2 == str.size() && str[0] == '"' && str[1] == '"')) {
                idDef->SetValue(val);
              } else {
                delete val;
                val = NULL;
              }
              Strings::free(image);
            }
            SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

            if ( NULL == val ) {
              UtString netname;
              getMessageContext()->Verific2NUUnableToDetermineConstant(&loc, net->getName()->str());
              setError(true);
            } else {

              VhdlConstraint* constraint = node.GetSubtypeIndication()->EvaluateConstraintInternal(mDataFlow, 0);
              VhdlConstraint* init_constraint = node.GetInitAssign()->EvaluateConstraintInternal(mDataFlow, 0);
              if (constraint && constraint->IsUnconstrained() && init_constraint && !init_constraint->IsUnconstrained()) {
                delete constraint;
                constraint = init_constraint;
              } else if (init_constraint) {
                delete init_constraint;
              }

              // If all else fails, use the 'value' to constrain 'constraint'
              if (constraint && constraint->IsUnconstrained()) {
                constraint->ConstraintBy(val);
              }
              
              if (!constraint || constraint->IsUnconstrained()) {
                getMessageContext()->Verific2NUPopulateError(&loc);
                mCurrentScope->sValue(NULL);
                setError(true);
                return ;
              }
              VhdlConstraint* oldConstraint = mTargetConstraint;
              mTargetConstraint = constraint;
              if ( withinTask ) {
                NULvalue* lval = mNucleusBuilder->createIdentLvalue(net, loc);
                VhdlIdRef idRef(idDef);
                NUExpr* rval = createKnownConst(idRef, val, loc);
                // FD thinks: using val->IsSigned() is incorrect
                if ( val->IsSigned() ){
                  rval->setSignedResult(true);
                }
                NUBlockingAssign* init_assign = NULL;
                init_assign = mNucleusBuilder->cBlockingAssign(lval, rval, loc);

                NUBlock* current_block = getBlock();
                NUTask* current_task = getTask();

                // If we're in a task, but not (yet) within a block, then we
                // must be in a subprogram declaration area. In this case,
                // we need to find the top level block for the task, and put
                // the initial assign there. 
                if ( ( NULL == current_block ) && current_task ){
                  SInt32 num_blocks = 0;
                  // if there is one and only one block in the task then use it as the current_block
                  for (NUBlockLoop iter = current_task->loopBlocks(); not iter.atEnd(); ++iter) {
                    current_block = *iter;
                    num_blocks ++;
                    V2N_INFO_ASSERT((1 == num_blocks), node, "Task has more than one block for statements");
                  }
                }
                V2N_INFO_ASSERT((current_block), node, "Undefined scope/block for a task");

                if ( init_assign && current_block ) {
                  // create the assignment within the task, just before the point where the statements of the task will be inserted
                  mNucleusBuilder->aStmtToBlock(init_assign, current_block);
                }

              } else {
                initAssign(node.GetInitAssign(), net);    // create an initial assignment in the inital block of the current module
              }
              mTargetConstraint = oldConstraint;
              delete constraint;
            }
        }
    }
    //set return value to NULL, side effect statement
    mCurrentScope -> sValue(NULL);
}

/**
 *@brief VhdlVariableDecl
 *
 * variable_decl :=
 *      [ SHARED ] VARIABLE identifier_list : subtype_indication [ := expression ]
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlVariableDecl, node)
{
  DEBUG_TRACE_WALKER("VhdlVariableDecl", node);

  if (hasError()) {
      return ;
  }
  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());  
  unsigned i;
  VhdlIdDef* item = 0;
  NUNet* net = 0;
  FOREACH_ARRAY_ITEM(node.GetIds(), i, item) {
      VhdlIdDef* idDef = static_cast<VhdlIdDef*>(item);
      V2N_INFO_ASSERT(idDef, node, "Variable Declaration without ID");
      //if variable declared inside subprogram with unconstrained formal arguments
      //we have to get constraint from actual arguments and populat it for variable
      {
          if (idDef->IsUnconstrained()) {
              VhdlConstraint* vConstraint = node.GetSubtypeIndication()->EvaluateConstraintInternal(mDataFlow, 0);
              if (!vConstraint) {
                  getMessageContext()->Verific2NUFailedWidth(&src_loc, idDef->Name());
                  mCurrentScope->sValue(NULL);
                  setError(true);
                  return;
              }
              V2N_INFO_ASSERT(vConstraint, node, "invalid constraint on variable subtype indication node");
              idDef->SetConstraint(vConstraint);
          }
      }
      item->Accept(*this);
      //variable decl id always populate NUNet      //RJC RC#2013/08/14 (edvard) what if there were a mistake and it was not a NUNET? I put dynamic_cast here to protect this from such a mistake 
      net = dynamic_cast<NUNet*>(mCurrentScope->gValue());

      //RJC RC#2013/08/16 (edvard) why is the following a test for (net==null AND hasError()), instead of (net==NULL || hasError())?  It would seem
      //that if anything within the call children of the call to Accept (5 lines above) were to set hasError() to true then we should be exiting
      //here.  It would seem much cleaner if after every call to Accept there was a check for hasError(), and if it was true there would be a
      //call to "mCurrentScope->sValue(NULL);" and then a return.
      //ADDRESSED FD: we should use CONSISTENCY checks to address all value get or scope get operations
      
      NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(net, src_loc, "Incorrect net type");
      //add populated NUnet to the current context
      NUScope* declaration_scope = NULL;
      if (idDef->IsSharedVariable()) { //if declared as shared at to top-level module
          declaration_scope = getModuleScope();
      } else {
          declaration_scope = getDeclarationScope();
      }
      NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(declaration_scope, src_loc, "Unable to get declaration scope");
      mNucleusBuilder->aNetToScope(net, declaration_scope);
      //if initial part not empty than
      //create initial block if it is not
      //exists and add to initial part
      createFOpenSysTaskForLine(net, node);
      if (node.GetInitAssign()) {


          if (NUMemoryNet* mNet = dynamic_cast<NUMemoryNet*>(net)) {
              mCurrentScope->sSize(mNet->getRangeSize(1));
          } else if (NUVectorNet* vNet = dynamic_cast<NUVectorNet*>(net)) {
              mCurrentScope->sSize(vNet->getBitSize());
          } else if (dynamic_cast<NUBitNet*>(net)) {
              mCurrentScope->sSize(1);
          }
       
          VhdlConstraint* constraint = node.GetSubtypeIndication()->EvaluateConstraintInternal(mDataFlow, 0);
          if (!constraint || constraint->IsUnconstrained()) {
              mNucleusBuilder->getMessageContext()->Verific2NUPopulateError(&src_loc);
              mCurrentScope->sValue(NULL);
              setError(true);
              return ;
          }
          VhdlConstraint* oldConstraint = mTargetConstraint;
          mTargetConstraint = constraint;
          VhdlValue* value = node.GetInitAssign()->Evaluate(constraint, mDataFlow, 0);
          if (value) {
              if (item->GetOwningScope() &&
                  item->GetOwningScope()->GetOwner() &&
                  item->GetOwningScope()->GetOwner()->IsFunction() &&
                  mDataFlow) {
                  mDataFlow->SetAssignValue(item, value);
              } else {
                  delete value;
              }
          }
          initAssign(node.GetInitAssign(), net);
          mTargetConstraint = oldConstraint;
          delete constraint;
      }
  }
  
  //set return value to NULL, side effect statement
  mCurrentScope->sValue(NULL);
}

/**
 *@brief VhdlVariableID
 *
 * VhdlIdDef base class for VhdlVariableId
 * which containt all necessary information
 * for populating variable identifier
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlVariableId, node)
{
    DEBUG_TRACE_WALKER("VhdlVariableId", node);
    VHDL_VISIT_CALL(VhdlIdDef, (VhdlIdDef&)node);
}

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlFullTypeDecl, node)
{
    DEBUG_TRACE_WALKER("VhdlFullTypeDecl", node);
    (void) node;
    //side effect
    return ;
}

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlComponentDecl, node)
{
    DEBUG_TRACE_WALKER("VhdlComponentDecl", node);
    (void) node;
    //side effect
    return ;
}

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlComponentId, node)
{
    DEBUG_TRACE_WALKER("VhdlComponentId", node);
    (void) node;
    //side effect
    return ;
}

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlEnumerationTypeDef, node)
{
    DEBUG_TRACE_WALKER("VhdlEnumerationTypeDef", node);
    (void) node;
    //side effect
    return ;
}

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlAttributeDecl, node)
{
    DEBUG_TRACE_WALKER("VhdlAttributeDecl", node);
    (void) node;
    //side effect
    return ;
}

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlAttributeSpec, node)
{
    DEBUG_TRACE_WALKER("VhdlAttributeSpec", node);
    (void) node;
    //side effect
    return ;
}

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlConfigurationDecl, node)
{
    DEBUG_TRACE_WALKER("VhdlConfigurationDecl", node);
     mEntityVisit = true;
    (void) node;
    //side effect
    return ;
}
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlConfigurationId, node)
{
    DEBUG_TRACE_WALKER("VhdlConfigurationId", node);
    (void) node;
    //side effect
    return ;
}

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlLibraryId, node)
{
    DEBUG_TRACE_WALKER("VhdlLibraryId", node);
    (void) node;
    //side effect
    return ;
}

void VerificVhdlDesignWalker::VHDL_VISIT(VhdlSubtypeDecl, node)
{
    DEBUG_TRACE_WALKER("VhdlSubtypeDecl", node);
    (void) node;
    //side effect
    return ;
}

} //verific2nucleus
