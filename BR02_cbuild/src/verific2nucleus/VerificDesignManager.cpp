// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// Standard headers
#include <iostream>

// Project headers
#include "verific2nucleus/VerificDesignManager.h"
#include "verific2nucleus/CarbonProtect.h"

// Carbon headers
#include "compiler_driver/CarbonContext.h"
#include "util/CarbonAssert.h"
#include "util/HdlFileCollector.h"
#include "util/SourceLocator.h"

// Verific headers
#include "Array.h"
#include "Strings.h"
#include "Message.h"

#include "VeriLibrary.h"
#include "LineFile.h"
#include "vhdl_file.h"
#include "SaveRestore.h"
#include "FileSystem.h"
//Set to enable debug
//#define DEBUG_VERIFIC_DESIGN_MANAGER 1


namespace verific2nucleus {

VerificDesignManager::VerificDesignManager()
   : mHdlFiles()
   , mVlogTop("")
   , mVhdlTop("")
   , mVerilogReader()
   , mVhdlReader()
   , mTopModule(NULL)
   , mTopEntity(NULL)
   , mParamValPairs(NULL)
   , mMsgContext(NULL)
   , mCarbonContext(NULL)
   , mArchName("")
{
}


VerificDesignManager::~VerificDesignManager()
{
    //freeAllocatedMemory();
    mVhdlExt.clear();
}

void 
VerificDesignManager::freeAllocatedMemory(bool compileLibOnly)
{
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
    std::cerr << "freeAllocatedMemory" << std::endl;
#endif
    resetParamPairs();

    if (mTopModule || mTopEntity || compileLibOnly) {
        Verific::veri_file::ResetParser();
        Verific::vhdl_file::ResetParser();
        mVhdlReader.RemoveAllUnits();
        mVerilogReader.RemoveAllModules();
        //delete Verific::VhdlAttributeName::_predef_table;
        Verific::VeriExpression::ReleaseNameToTokensData();
        Verific::SaveRestore::ResetMessageMap();         //Delete Message Map
        Verific::VeriTreeNode::ResetMessageMap();
        Verific::VhdlTreeNode::ResetMessageMap();
        Verific::vhdl_file::ResetAllLibraryPaths();
        Verific::VhdlAttributeName::VhdlAttributeName::ResetPredefAttrTable();
        Verific::VhdlNode::ResetConstNets();

        // Remove all recognized libraries
        Verific::vhdl_sort::Reset();
        Verific::VhdlNode::ResetPragmaTables();
        Verific::VhdlNode::ResetOperatorsTable();
        Verific::LineFile::DeleteAllLineFiles();
        Verific::Message::ClearAllMessageTypes();
        Verific::Message::ClearAllNewFormattedStrings();
        Verific::Message::ClearErrorCount();
        Verific::veri_file::ClearTopModuleList();
        Verific::veri_file::Reset();
        // this  is part of veri_file::Reset()  Verific::veri_file::UndefineCarbonUserDefinedMacros();
        Verific::VeriNode::ResetSystemTasksTable();
        Verific::VeriNode::ResetUdpEdgeCharMap();
        Verific::vhdl_sort::ClearRegisteredFiles();

#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
        std::cout << "done" << std::endl;
#endif
    }
}
void
VerificDesignManager::resetParamPairs()
{
  char * name;
  char * val;
  Verific::MapIter mi;
  FOREACH_MAP_ITEM(mParamValPairs, mi, &name, &val) { // Traverse declared ids
    Verific::Strings::free(name);
    Verific::Strings::free(val);
  }
  delete mParamValPairs;
  mParamValPairs = NULL;
}

Verific::VeriModule*
VerificDesignManager::GetTopModule()
{
    return mTopModule;
}

///@TODO add file multifile support later

UtString
VerificDesignManager::GetHDLFile()
{
    return mHdlFiles[0].first;
}

ArgProc* VerificDesignManager::getArgs()
{
  return getCarbonContext()->getArgs();
}

Verific::VhdlPrimaryUnit*
VerificDesignManager::GetTopEntity()
{
  return mTopEntity;
}

/**
 ** @brief setVhdlTop
 *  support specification of top level entity in Vhdl
 */
void
VerificDesignManager::setVhdlTop(const UtString& vhdl_top_name)
{
  mVhdlTop = vhdl_top_name;
  // do not change the case (upper,lower,preserve) of vhdl_top_name here, if necessary the caller should adjust the case.
}

/**
 ** @brief setVlogTop
 *  support specification of top level module in Verilog
 */
void
VerificDesignManager::setVlogTop(const UtString& vhdl_top_name)
{
  mVlogTop = vhdl_top_name;
}

UtString&
VerificDesignManager::getVhdlTop()
{
  return mVhdlTop;
}

UtString&
VerificDesignManager::getVlogTop()
{
  return mVlogTop;
}

void
VerificDesignManager::setTopLevelParameters(const UtMap<UtString, UtString>& paramValPairs)
{
    if (paramValPairs.size() > 0) {
        mParamValPairs = new Verific::Map(Verific::POINTER_HASH, paramValPairs.size());
        for (UtMap<UtString,UtString>::const_iterator it = paramValPairs.begin(); it != paramValPairs.end(); it++) {
            char* paramName = Verific::Strings::save(it->first.c_str());
            char* paramVal = Verific::Strings::save(it->second.c_str());
            mParamValPairs->Insert(paramName, paramVal); 
        }
    }
}

void
VerificDesignManager::setPragmaTriggers(const UtVector<UtString>& pragmaTriggers, bool isVerilog)
{
    
    for (UtVector<UtString>::const_iterator it = pragmaTriggers.begin(); it != pragmaTriggers.end(); it++) {
        if (isVerilog) {
            Verific::veri_file::AddPragmaTrigger(it->c_str());
        } else {
            Verific::vhdl_file::AddPragmaTrigger(it->c_str());
        }
    }
}

void 
VerificDesignManager::setIgnoreTranslatePragma(unsigned ignore)
{
    Verific::veri_file::SetIgnoreTranslateOff(ignore);
}
// map a carbon LanVer identifier to one of the verific veri_file or vhdl_file enum values, and also return a flag that indicates if the
// file is from the VHDL group
unsigned VerificDesignManager::convertToVerificLanguageVersion(LangVer langVer, bool*  isVHDLGroup)
{
  // from enum in veri_file.h: { VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG=3, VERILOG_AMS=4, VERILOG_PSL=5, UNDEFINED };
  //          and vhdl_file.h: { VHDL_93=0, VHDL_87=1, VHDL_2K=2, VHDL_2008=3, VHDL_PSL=6, UNDEFINED=7};
    switch (langVer) {
        case LangVer::VHDL87:
        case LangVer::VHDL93:
            {
                *isVHDLGroup = true;
                switch (langVer) {
                    case LangVer::VHDL87:  { return Verific::vhdl_file::VHDL_87; }
                    case LangVer::VHDL93:  { return Verific::vhdl_file::VHDL_93; }
                    default: {  
                        INFO_ASSERT(0, "Incomplete implementation of convertToVerificLanguageVersion.");
                        return Verific::vhdl_file::UNDEFINED; 
                    }      
                }
            }
        case LangVer::Verilog:
        case LangVer::Verilog2001:
        case LangVer::SystemVerilog2005:
        case LangVer::SystemVerilog2009:
        case LangVer::SystemVerilog2012: {
            *isVHDLGroup = false;

            // all verilog variants should come through here
            switch ( langVer ){
                case LangVer::Verilog:           { return Verific::veri_file::VERILOG_95; }
                case LangVer::Verilog2001:       { return Verific::veri_file::VERILOG_2K; }
                case LangVer::SystemVerilog2005: { return Verific::veri_file::SYSTEM_VERILOG_2005; }
                case LangVer::SystemVerilog2009:
                case LangVer::SystemVerilog2012: { return Verific::veri_file::SYSTEM_VERILOG; } // currently verific does not distinguish SV2012
                default: {
                    INFO_ASSERT(0, "Incomplete implementation of convertToVerificLanguageVersion.");
                    return Verific::veri_file::UNDEFINED;
                }
            }
            default: { return Verific::veri_file::UNDEFINED; }
        }
    }
}

void VerificDesignManager::printOrderedFileList(const UtString& fileRoot,
                                                const UtVector<UtPair<UtString,UtString> > &libNameLibPath,
                                                UtMap<UtString,unsigned> &fileName2langver) const
{
    UtString fileName(fileRoot);
    fileName << ".ordered";
    UtOFStream f(fileName.c_str());
    if (!f) {
        UtString msg = UtString("Could not write ordered file list to") + fileName;
        INFO_ASSERT(0,msg.c_str());
        return;
    }

    for (UtVector<UtPair<UtString,UtString> >::const_iterator it = libNameLibPath.begin();
       it != libNameLibPath.end();
       it++) {
            f << "-lib " << it->first.c_str() << ":" << it->second.c_str() << UtIO::endl;
        //unsigned langVer;
        //bool report93 = false;
        //bool report87 = false;
        //for(UtMap<UtString,unsigned>::const_iterator itr = fileName2langver.begin(), itr_end = fileName2langver.end(); 
        //    itr != itr_end; ++itr) {
        //      langVer = itr->second;
        //      if (!report87 && (Verific::vhdl_file::VHDL_87 == langVer)) {
        //          f << "-87" << UtIO::endl;
        //          report87 = true;
        //      } else if (!report93 && (Verific::vhdl_file::VHDL_93 == langVer)) {
        //          f << "-93" << UtIO::endl;
        //          report93 = true;
        //      }
        //}
    }

    //RJC RC#2013/08/14 (lilit)
    // in the following I don't see how the proper order will be printed if there is a situation where we mix -87 and -93 such that the
    //correct order is: v_93_a.vhdl, v_87_a.vhdl, v_93_b.vhdl, v_87_b.vhdl, what this implementation will print is:
    //  -87 v_87_a.vhdl, v_87_b.vhdl,
    //  -97 v_93_a.vhdl, v_93_b.vhdl, 
    // which is incorrect because  v_93_b.vhdl, must be before v_87_b.vhdl
    // I don't think it is important to have only two sections
    unsigned ignore_vhdl_mode;
    unsigned i = 0;
    const char* lib_name = "work";
    const char* file_name = NULL;
    UtVector<UtString> vhdl87files;
    UtVector<UtString> vhdl93files;
    UtMap<unsigned,UtVector<UtString> > langver2SortedFiles;
    FOREACH_SORTED_FILE(i,file_name,lib_name, ignore_vhdl_mode) {
        UtString fileName(file_name);
        unsigned langVersion = fileName2langver[fileName];
        if (langver2SortedFiles.find(langVersion) != langver2SortedFiles.end()) {
            langver2SortedFiles[langVersion].push_back(UtString(file_name));
        } else {
            langver2SortedFiles[langVersion] = UtVector<UtString>();
            langver2SortedFiles[langVersion].push_back(UtString(file_name));
        }
    }
    UtVector<unsigned> supportedLangVersions;
    supportedLangVersions.push_back(Verific::vhdl_file::VHDL_87);
    supportedLangVersions.push_back(Verific::vhdl_file::VHDL_93);
    UtVector<unsigned>::iterator it = supportedLangVersions.begin();
    for (;it != supportedLangVersions.end(); ++it) {
        if (langver2SortedFiles.find(*it) != langver2SortedFiles.end()) {
            if (Verific::vhdl_file::VHDL_87 == *it) {
                f << "-87" << UtIO::endl;
            } else if (Verific::vhdl_file::VHDL_93 == *it) {
                f << "-93" << UtIO::endl;
            } else {
                INFO_ASSERT(false, "Unsupported language version encountered");
            }
            for (UtVector<UtString>::iterator it2 = langver2SortedFiles[*it].begin(); it2 != langver2SortedFiles[*it].end(); ++it2) {
                f << *it2 << UtIO::endl;
            }
        }
    }
}

void
VerificDesignManager::RegisterFile(const UtString& f_name, const LangVer langVer, const UtString& /* l_name */ )
{
  bool thisIsAVHDLFile = false;
  unsigned f_type = convertToVerificLanguageVersion(langVer, &thisIsAVHDLFile);

  if ( thisIsAVHDLFile ) {
    f_type += mHdlFilesVHDLOffset;
  }
  mHdlFiles.push_back(UtPair<UtString, unsigned>(f_name, f_type)); 
}

/*
REWRITE THIS
Currently always this routine called, as we started to use .cmd file 
 */
int
VerificDesignManager::CompileToParseTree(const UtString& f_listname, bool assumeVerilogFilesAreSV, 
                                        const LangVer& langVer, UtMap<UtString,unsigned> &fileName2langver, 
                                        bool autoCompile, const UtString& fileRoot, 
                                        const UtVector<UtPair<UtString,UtString> > &libNameLibPath,
                                        UtMap<UtString, UtString> & fileName2LibName, bool compileLibOnly,
                                        bool hasVerilogLib, bool hasVhdlLib, bool hasLibmap, bool  hasVhdlNoWriteLib)  
{
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
    std::cout << "hasVerilogLib = "<< hasVerilogLib << std::endl;
    std::cout << "hasVhdlLib = " << hasVhdlLib << std::endl;
#endif

    // 'carbon' is the only default pragma trigger
    Verific::veri_file::AddPragmaTrigger("carbon");
    Verific::vhdl_file::AddPragmaTrigger("carbon");

    CarbonProtect carbonProtect(this);
    Verific::veri_file::SetTickProtectObject(&carbonProtect) ;
    Verific::veri_file::SetPragmaProtectObject(&carbonProtect) ;

    const char *f_file_list_name = f_listname.c_str();
    unsigned ignored_f_analysis_mode = Verific::veri_file::UNDEFINED;
    bool vhdl_only = false;
    Verific::veri_file::f_file_flags p_mode = Verific::veri_file::F_FILE_NONE;
    Verific::Array *f_names = Verific::veri_file::ProcessFFile(f_file_list_name, p_mode, ignored_f_analysis_mode);
    if ( (NULL == f_names) && mVlogTop.empty() && mVhdlTop.empty()) {
      mMsgContext->NoTopModule();
      return 0;                 // indicates failure
    }
    bool thisIsAVHDLFile = false;
    // override analysis mode with version specified on command line (note VerilogXL has no support for this);
    unsigned f_analysis_mode = convertToVerificLanguageVersion(langVer, &thisIsAVHDLFile);
    Verific::Array vhdl_files;
    Verific::Array veri_files;
    // First separate the Verilog and VHDL files:

    // We already have a container of HDL files (mHdlFiles), which was populated by
    // CarbonContext::createLangVer2FileName (the caller of compileToParseTree). This, in
    // turn, was populated by the HdlFileCollector container in CarbonContext,
    // based on the cbuild command line arguments. This container has information about the
    // language of each file. Note that command line arguments like -vhdlExt have
    // been used to figure out the language based on file extension. So we don't need
    // to perform this analysis again here.
    //
    // Here we are copying the mHdlFiles container into a pair of Verific Arrays, to
    // be used by the Verific Analyze methods.
    //
    // NOTE: It's not obvious to me why we are not using the CarbonContext HdlFileCollector
    // directly throughout this function, but that will have to wait for a later code
    // analysis.
    UtVector<UtPair<UtString, unsigned> >::const_iterator file_iter;
    for (file_iter = mHdlFiles.begin(); file_iter != mHdlFiles.end(); ++file_iter) {
      const UtString* f = &(file_iter->first);
      unsigned langType = file_iter->second;
      // Vhdl files have langtype >= the mHdlFileVHDLOffset constant.
      // Verilog files (including 1995, 2001, and SystemVerilog) have langtype 
      // less than this constant. (See the RegisterFile method)
      if (langType >= mHdlFilesVHDLOffset) {
        vhdl_files.InsertLast(f->c_str());
      } else {
        veri_files.InsertLast(f->c_str());
      }
    }

/*
   Steps for VHDL parse tree creation 

   -- data structure:
    assumption: file x library

   1) read in all saved libraries: standard, libmap libraries.

   2) sort order of files

   3) analyze all vhdl code to nominated library (file -> library map)

   --check for switch
   4) library saving stage: 
        save all analyzed code to libraries

   5) if (compile Only) {
      return with analyzed status;
   }

   6) elaborate from top level module

   */
    if ( libNameLibPath.empty() ){
      // if there are no library entries then that means that there were no recognized source hld files identified, and no user specified library
      mMsgContext->NoTopModule();
      return 0;                 // indicates failure
    } else if (mVhdlTop.empty() && mVlogTop.empty() && !libNameLibPath.empty() && (vhdl_files.Size() == 0) && (veri_files.Size() == 0)) {
      UtString reason = hasVerilogLib ? "-vlogTop" : "-vhdlTop";
      mMsgContext->VlogHasLibButNoTopModule(reason.c_str());
      return 0;
    }
    //check for load vhdl standard libraries
    checkForLoadVhdlStdLibraries();
    //read in all saved libraries: non-standard, libmap libraries.
    restoreLib(compileLibOnly, libNameLibPath);
    UtVector<UtPair<UtString,UtString> >::const_iterator it = libNameLibPath.end() - 1;
    UtString lastLib = (*it).first;
    //first check if worklib contains already top module
    bool vlogTopNotFound = false;
    if (!compileLibOnly && !mVlogTop.empty() && !mVerilogReader.GetModule(mVlogTop.c_str(), 1, lastLib.c_str())) {
        vlogTopNotFound = true;
    }
    
    bool isMixed = false;
    if ((veri_files.Size() > 0) && (vhdl_files.Size() > 0) || ( (veri_files.Size() > 0) && hasVhdlLib ) 
        || ( (vhdl_files.Size() > 0) && hasVerilogLib ) || ( hasVhdlLib && hasVerilogLib ) 
        || ( hasVhdlLib && hasVerilogLib )) {
        
        isMixed = true;
    }
    else if ( hasLibmap && (vhdl_files.Size() == 0 || veri_files.Size() == 0))
    {
        isMixed = true;
    }

    // As we copy all modules to the work library one of the side-effects is that we can incorrectly find top module 
    if (!compileLibOnly && !mVlogTop.empty() && vlogTopNotFound && mVerilogReader.GetModule(mVlogTop.c_str(), 1, lastLib.c_str())) {
        mMsgContext->VlogTopModuleNotFound(mVlogTop.c_str());
        return 0;
    }
    // 2) sort order of files
    //register and sort the vhdl files (if vhdl only)
    unsigned i = 0;
    char* f_name = NULL;
    if (vhdl_files.Size() != 0 && veri_files.Size() == 0) {
        vhdl_only = true;
	Verific::Array* vhdl_files_ptr = &vhdl_files;
        FOREACH_ARRAY_ITEM(vhdl_files_ptr, i, f_name) {
            UtString l_name(fileName2LibName[UtString(f_name)]);
            Verific::vhdl_sort::RegisterFile(f_name, l_name.c_str());
        }
        Verific::vhdl_sort::Sort();
    }
        // Next, check whether the analysis mode is set from -f file:
    if (f_analysis_mode == Verific::veri_file::UNDEFINED) {
        // Default to Verilog 95
        f_analysis_mode = Verific::veri_file::VERILOG_95;
        if (assumeVerilogFilesAreSV) {
            f_analysis_mode = Verific::veri_file::SYSTEM_VERILOG;
        }
        // But treat .sv and .sysv files as SystemVerilog:
        Verific::veri_file::AddFileExtMode(".sv", Verific::veri_file::SYSTEM_VERILOG);
        Verific::veri_file::AddFileExtMode(".sysv", Verific::veri_file::SYSTEM_VERILOG);
    }
    // Analyze all the Verilog files at-once
    // (for mixed designs we need them before VHDL designs)
    bool ve_analyzed = false;
    if (!vhdl_only) {
        UInt32 comp_unit_mode = Verific::veri_file::NO_MODE; // this tells Verific to use MFCU if in verilog mode, or SFCU if in SVmode
                                                             // (Verific is slightly wrong here because there is no default mode for SV2009 and SV2012)
                                                             // so we fix this here:
        if ( Verific::veri_file::IsAnySystemVerilogVariant(static_cast<Verific::veri_file::verilog_lang_variant>(f_analysis_mode)) ){
            // user specified a compilation unit mode
            bool cu_mode_specified = getArgs()->isParsed("-mfcu") || getArgs()->isParsed("-sfcu"); // must do arg and the bool override in order to see if either was on cmd line
            if ( cu_mode_specified ) {
              bool mfcu_specified = getArgs()->getBoolValue("-mfcu");
              comp_unit_mode = mfcu_specified ? Verific::veri_file::MFCU : Verific::veri_file::SFCU;
            } else {
              // IN systemverilog 2005 the LRM said that SFCU was the default, but this default specification was removed in later releases
              // of the LRM. Since MFCU mode is what was used for verilog95, and v2k we will also use it as the default in the cases where
              // the LRM is silent on this issue.  Thus what we SHOULD do here is set to SFCU if systemverilog2005, otherwise set to MFCU,
              // but since cbuild does not provide for the selection of systemverilog2005 we just use MFCU unconditionally
              comp_unit_mode = Verific::veri_file::MFCU;
            }
        }
        ve_analyzed = Verific::veri_file::AnalyzeMultipleFiles(&veri_files, f_analysis_mode, lastLib.c_str(), comp_unit_mode);
        if (!ve_analyzed && (veri_files.Size() != 0)) { return 0; }
        // Now analyze the VHDL files one by one:
    }
    // if still not found top module
    if (!compileLibOnly && !mVlogTop.empty() && !mVerilogReader.GetModule(mVlogTop.c_str(), 1, lastLib.c_str())) {
        mMsgContext->VlogTopModuleNotFound(mVlogTop.c_str());
        return 0;
    }
    bool vh_analyzed = true; 
    if (vhdl_only && autoCompile) {
        unsigned ignore_vhdl_mode;
        const char* lib_name = NULL;
        const char* file_name = NULL;
        printOrderedFileList(fileRoot,libNameLibPath,fileName2langver);

        // 3) analyze all vhdl code to nominated library (file -> library map)
        FOREACH_SORTED_FILE(i,file_name,lib_name, ignore_vhdl_mode) {
            if (! Verific::vhdl_file::Analyze(file_name, lib_name, fileName2langver[file_name])) {
                vh_analyzed = false;
            }
        }
    } else {
      Verific::Array* vhdl_files_ptr = &vhdl_files;
      FOREACH_ARRAY_ITEM(vhdl_files_ptr, i, f_name) {
            if (! Verific::vhdl_file::Analyze(f_name, fileName2LibName[f_name].c_str(), fileName2langver[f_name])) {
              vh_analyzed = false;
            }
        }
    }
    if (! vh_analyzed && (vhdl_files.Size() != 0)) {
      return 0;
    }
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
    std::cout << "After Analyze" << std::endl;
    debugPrettyPrint();
    debugPrettyPrintVerilog();
#endif

    ///if (hasLibMap) {
    // --check for switch
    // 4) library saving stage: 
    //save all analyzed code to libraries
    saveLib(compileLibOnly, hasLibmap, hasVhdlNoWriteLib, veri_files,vhdl_files, libNameLibPath);
    // copy all library modules to work lib (lastLib)
    if (!compileLibOnly) copyAllVlogLibraryModulesToWork(lastLib);

    if (!compileLibOnly && isMixed) copyAllVhdlLibraryToWork(lastLib);
    //5) if (compile Only) {
    //   return with analyzed status;
    //}
    if (compileLibOnly) {
        // cleanup parse-tree before return;
        // temporary
        freeAllocatedMemory(true);
        while (f_names->Size()) Verific::Strings::free((char *)f_names->RemoveLast());
        if(f_names) delete f_names;
        if (vhdl_only) {
            return vh_analyzed;
        } else if (vhdl_files.Size() > 0) {
            return vh_analyzed && ve_analyzed;
        } else {
            return ve_analyzed;
        }
    }

    // Everything is done, clean up the file names and the allocated array:
    // Caller of ProcessFFile function is responsible to delete the returned Array and its char * content.
    //while (veri_files.Size()) Verific::Strings::free((char *)veri_files.RemoveLast());
    //while (vhdl_files.Size()) Verific::Strings::free((char *)vhdl_files.RemoveLast());
    //vhdl_files.Remove(0, vhdl_files.Size());
    while (f_names->Size()) Verific::Strings::free((char *)f_names->RemoveLast());
    if(f_names) delete f_names;
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
    PrettyPrintVerilogDesign("_after_analysis");
#endif
    bool fileSize = (vhdl_files.Size() == 0 && veri_files.Size() == 0);
    if ((fileSize && !hasLibmap) && ( fileSize && !hasVhdlLib) && (fileSize && !hasVerilogLib)) {
        mMsgContext->Verific2NUNoInputFile("");
        return 0;
    }
    //6) elaborate from top level module
    // Elaborate and return the status
    bool top_found;
    bool elabStatus = ElaborateHDL(lastLib, &top_found);

    if (!top_found) {
      if (vhdl_only || isMixed) {
        //RJC RC#2013/08/20 (edvard) the following is not a useful error message, I found that this error is printed by
        // test/rushc/vhdl/beacon_sub_programs/sub_programs.func_retsize.vhdl where better information is provided by the Interra flow, 
        // please fix this so that it matches the message printed in interra flow.
        // One scenario that can cause this code to execute is this: run cbuild on an empty Verilog file, e.g. cbuild empty.v
        // There may be other scenarios as well.
        // TODO in case of VHDL (mixed language), more complex error detection needed, i.e. above mentioned testcase used to report about package not found,
        // which isn't allowed as a top level unit (message 9014), but I think there might be some other contexts.
        // Do we have some range for mixed language messages ? if yes we may wish to have below message number from that range
        // Currently just use vhdl version
        mMsgContext->Verific2NUFailedToElaborateHDL("");
      } else {
        // In case of Verilog/SV, here we can get only if alltopmodules array is 0, and so not top module found in specified Verilog/SV files
        mMsgContext->NoTopModuleFound();
      }
    }
    
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
    PrettyPrintVerilogDesign("_after_static_elab");
#endif
    return elabStatus;
}

void
VerificDesignManager::handleExt(const Verific::Array* f_names, Verific::Array& vhdl_files,Verific::Array& veri_files)
{
    // First separate the Verilog and VHDL files:
    unsigned i = 0;
    char *f_name = NULL;
    UInt32 s = mVhdlExt.size();
    FOREACH_ARRAY_ITEM(f_names, i, f_name) {
        if (s != 0) {
            for (UInt32 j = 0; j < s; ++j) {
                if (StrEndWithStr(f_name, mVhdlExt[j].c_str())) {
                    vhdl_files.InsertLast(f_name);
                }
            }
        }
        else if (StrEndWithStr(f_name, ".vhd") || StrEndWithStr(f_name, ".vhdl") ||
                StrEndWithStr(f_name, ".VHD") || StrEndWithStr(f_name, ".VHDL")) {
                vhdl_files.InsertLast(f_name);
        } else if (StrEndWithStr(f_name, ".v") || StrEndWithStr(f_name, ".sv") || StrEndWithStr(f_name, ".sysv")) {
                veri_files.InsertLast(f_name);
        }
        
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER 
            printf("WARNING: File %s has an invalid extension - ignored.\n", f_name);
#endif
    }
}

void
VerificDesignManager::checkForLoadVhdlStdLibraries()
{
        //read in the vhdl libraries.
        //get from $CARBON_HOME/lib
        char* carbon_path = getenv("CARBON_HOME");
        if (!carbon_path) {
            UtString msg = UtString("CARBON HOME is undefined. Required for vhdl libraries");
            INFO_ASSERT(false, msg.c_str());
        } else {
            // 1) read all standard precompiled libraries.
            loadVDBLibrarys();
        }
}

void 
VerificDesignManager::restoreLib(bool compileLibOnly, const UtVector<UtPair<UtString,UtString> > &libNameLibPath)
{
    // 1) read in all saved libraries: non-standard, libmap libraries (both verilog and vhdl).
    // TODO : need to check if SaveLib/RestorLib is smart enough to save and load mixed modules from verilog/vhdl
    for (UtVector<UtPair<UtString,UtString> >::const_iterator it = libNameLibPath.begin(); it != libNameLibPath.end(); it++) {
        UtString libName = (*it).first;
        UtString vhdlLibName = (*it).first;
        vhdlLibName.lowercase();
        UtString libPath = (*it).second;
        Verific::vhdl_file::AddLibraryPath(vhdlLibName.c_str(), libPath.c_str());
        Verific::veri_file::AddLibraryPath(libName.c_str(), libPath.c_str());
        UtString vhdlLibFile = libPath + UtString("/") + vhdlLibName + UtString(".vdbl");
        UtString veriLibFile = libPath + UtString("/") + libName + UtString(".sdbl");
        // Verilog libraries should be read if compileLibOnly not specified
        if (!compileLibOnly && Verific::FileSystem::PathExists(veriLibFile.c_str()) && Verific::FileSystem::IsFile(veriLibFile.c_str())) {
            Verific::veri_file::RestoreLib(libName.c_str());
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
                std::cout << "Verilog RestoreLib" << std::endl;
#endif

        }
        if (Verific::FileSystem::PathExists(vhdlLibFile.c_str()) && Verific::FileSystem::IsFile(vhdlLibFile.c_str())) {
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
            std::cout << "Vhdl RestoreLib" << std::endl;
#endif
            Verific::vhdl_file::RestoreLib(libName.c_str());
        }
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
        std::cout << "After Restore" << std::endl;
        debugPrettyPrint(libName);
        debugPrettyPrintVerilog(libName);
#endif
    }
}
void VerificDesignManager::saveLib(bool compileLibOnly, bool hasLibmap, bool hasVhdlNoWriteLib, const Verific::Array& veri_files, 
                                   const Verific::Array& vhdl_files,const UtVector<UtPair<UtString,UtString> >& libNameLibPath)
{
  for (UtVector<UtPair<UtString,UtString> >::const_iterator it = libNameLibPath.begin(); it != libNameLibPath.end(); it++) {
        UtString libName = (*it).first;
        UtString libPath = (*it).second;
        Verific::vhdl_file::AddLibraryPath(libName.c_str(), libPath.c_str());
        if (compileLibOnly || (hasLibmap && veri_files.Size() != 0)) {
            Verific::veri_file::AddLibraryPath(libName.c_str(), libPath.c_str());
        }
        if ((vhdl_files.Size() != 0) && !hasVhdlNoWriteLib) {
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
            std::cout << "Save Vhdl libraries" << std::endl;
#endif
            Verific::vhdl_file::SaveLib(libName.c_str());
        }
        // Veriflog libraries should be saved only if -compileLibOnly specified
        if ((compileLibOnly && veri_files.Size() != 0) || (hasLibmap && veri_files.Size() != 0) && !hasVhdlNoWriteLib) {
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
            std::cout << "Save Verilog libraries" << std::endl;
#endif
            Verific::veri_file::SaveLib(libName.c_str());
        }
    }
}


void
VerificDesignManager::loadVDBLibrarys()
{
  UtString f_name ;
  UtString std_lib("std");
  UtString ieee_lib("ieee");
  UtString vl_lib("vl");
  UtString synopsys_lib("synopsys");
  UtPair<UtString, UtString>f_entry;
  UtVector<UtPair<UtString,UtString> > library_files;

  char* carbon_path = getenv("CARBON_HOME");
  if (!carbon_path) {
      UtString msg = UtString("CARBON HOME is undefined. Required for vhdl libraries");
      INFO_ASSERT(false, msg.c_str());
      return;
  }
  //The standard packages
  //TODO: Case lanugage type to get corect location for vhdl libraries
  //93 or 87
  UtString lib_path = UtString(carbon_path) + "/lib/verific/vhdl_packages/ieee_1993/bin";
  mVhdlReader.SetDefaultLibraryPath(lib_path.c_str());
  f_name = UtString("standard");
  f_entry = UtPair<UtString,UtString>(std_lib,f_name);
  library_files.push_back(f_entry);

  f_name =UtString("textio");
  f_entry = UtPair<UtString,UtString>(std_lib,f_name);
  library_files.push_back(f_entry);

  //The IEEE packages
  f_name =UtString("std_logic_1164");
  f_entry = UtPair<UtString,UtString>(ieee_lib,f_name);
  library_files.push_back(f_entry);

  f_name =UtString("std_logic_arith");
  f_entry = UtPair<UtString,UtString>(ieee_lib,f_name);
  library_files.push_back(f_entry);

  f_name =UtString("std_logic_signed");
  f_entry = UtPair<UtString,UtString>(ieee_lib,f_name);
  library_files.push_back(f_entry);

  f_name =UtString("std_logic_unsigned");
  f_entry = UtPair<UtString,UtString>(ieee_lib,f_name);
  library_files.push_back(f_entry);

  //Synopsys libraries
  //put attributes in synopsys library
  f_name =UtString("attributes");
  f_entry = UtPair<UtString,UtString>(synopsys_lib,f_name);
  library_files.push_back(f_entry);

  f_name =UtString("std_logic_misc");
  f_entry = UtPair<UtString,UtString>(ieee_lib,f_name);
  library_files.push_back(f_entry);

  f_name =UtString("std_logic_textio");
  f_entry = UtPair<UtString,UtString>(ieee_lib,f_name);
  library_files.push_back(f_entry);

  // the vl_types library is required for mixed language designs
  // library_files.push_back(UtPair<UtString,UtString>(vl_lib,"vl_types"));

  f_name =UtString("numeric_std");
  f_entry = UtPair<UtString,UtString>(ieee_lib,f_name);
  library_files.push_back(f_entry);

  for (UtVector<UtPair<UtString,UtString> >::iterator it = library_files.begin();
       it != library_files.end();
       it++) {
      
    mVhdlReader.Restore(it->first.c_str(), it->second.c_str());
  }
}
  

int 
VerificDesignManager::ElaborateHDL(const UtString& lastLib, bool* topFound)
{
    int err_code = 0;
    *topFound = false;
    // Allocate and return ptr to array of top Verilog modules, NULL ptr if none.
    // Note that this does not include any VHDL entities.
    Verific::Array* all_top_modules = mVerilogReader.GetTopModules(lastLib.c_str());
    if (all_top_modules && mVhdlTop.empty()) {
        // At least one uninstantiated (top) Verilog module exists, and the
        // user did NOT specify -vhdlTop. This means we have a Vlog top,
        // and we should proceed with Verilog elaboration, though
        // additional errors may be uncovered by elaborateVerilogModule.
        *topFound = true;
        err_code = elaborateVerilogModule(all_top_modules, lastLib);
    } else if (mVhdlReader.TopUnit(lastLib.c_str())) {
        // There IS a VHDL top unit, and there are no Verilog
        // top modules. This means we have a VHDL top, and we
        // should proceed with VHDL elaboration. Note that
        // additional errors may be uncovered by elaboration.
        *topFound = true;
        err_code = elaborateVhdlModule(lastLib);
    }
    // Free top module array (if any).
    delete all_top_modules;
    return err_code;
}

int
VerificDesignManager::elaborateVerilogModule(Verific::Array* all_top_modules, const UtString& lastLib)
{
    INFO_ASSERT(all_top_modules, "Invalid input pointer");
    // Determine the name of the top module
    UtString top_design_unit_name;
    if (! mVlogTop.empty()) {
        // User supplied the name with -vlogTop, use it. 
        top_design_unit_name = mVlogTop;
    } else if (all_top_modules->Size() == 1) {
        // No user specified -vlogTop, but there is one uninstantiated Verilog module. Use it.
        Verific::VeriModule* first_top_module = (Verific::VeriModule*)all_top_modules->GetFirst();
        INFO_ASSERT(first_top_module, "There is no first module in all_top_modules");   
        top_design_unit_name = first_top_module->GetName();
    } else if (all_top_modules->Size() > 1) {
        // Multiple top modules is an error, user must specify -vlogTop
        UtString buf;
        unsigned i;
        Verific::VeriModule* m;
        FOREACH_ARRAY_ITEM(all_top_modules, i, m) {
          //RJC RC#2013/09/23 (cloutier) We don't want to expose the name of `protected modules in this message.
          // To check protected regions, we need access to the SourceLocatorFactory (which appears to
          // be in VerificNucleusBuilder, not directly available here).
          // See the analogous Interra flow code in CheetahContext.cxx, around line 1729
          if (not buf.empty()) {
            buf += ", ";
          }
          buf += m->GetName();
        }
        mMsgContext->MultipleTopLevelModules(buf.c_str());
        return 0;
    }

    if (! mVerilogReader.GetModule(top_design_unit_name.c_str(), 1, lastLib.c_str())) {
        mMsgContext->VlogTopModuleNotFound(top_design_unit_name.c_str());
        return 0;
    }
    // Statically elaborate all the Verilog modules in the "work" library. Return if any error shows up.
    if (!mVerilogReader.ElaborateStatic(top_design_unit_name.c_str(), lastLib.c_str(), mParamValPairs)) { return 0; }
    //mVerilogReader.PrettyPrint();
    //Verific::VeriLibrary* library = Verific::veri_file::GetLibrary("work", 1);
    //library->PrettyPrint(std::cout, 0);

    // Get the pointer to the top-level module
    Verific::VeriModule *top_module = mVerilogReader.GetModule(top_design_unit_name.c_str(), 1, lastLib.c_str());
    // Exit from application if there is no top module by the given name in the given Verilog designs
    if (!top_module) { return 0; }
    mTopModule = top_module;
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
    //Verific::Message::PrintLine("\n");
    Verific::Message::PrintLine("Top level module : ", top_module ? top_module->Name() : 0);
    //Verific::Message::PrintLine("\n");
#endif
    // Debug information
    TraverseVerilog(top_module); // Traverse top level module and the hierarchy under it
    return 1;
}

UtString VerificDesignManager::gArchName()
{
    return mArchName;
}

int
VerificDesignManager::elaborateVhdlModule(const UtString& lastLib)
{
    //Verific::VhdlLibrary *lib = mVhdlReader.GetWorkLib();
    Verific::VhdlLibrary* lib = mVhdlReader.GetLibrary(lastLib.c_str());
    UtString top_design_unit_name;
    if (! mVhdlTop.empty()) {
        top_design_unit_name = mVhdlTop;
    } else {
        top_design_unit_name = mVhdlReader.TopUnit(lastLib.c_str());
    }
    size_t pos = top_design_unit_name.find_last_of(":");
    if (UtString::npos != pos) {
        mArchName = top_design_unit_name.substr(pos + 1, top_design_unit_name.size() - pos - 1);
        top_design_unit_name = top_design_unit_name.substr(0,pos);
    }
    INFO_ASSERT(! top_design_unit_name.empty(), "VHDL top design name is empty");
    if (! lib->GetPrimUnitAnywhere(top_design_unit_name.c_str())) {
        UtString libName(lib->Name());
        libName.uppercase();
        mMsgContext->VhdlPrimaryUnitNotFound(top_design_unit_name.c_str(), libName.c_str());
        return 0;
    }
    // Statically elaborate all Vhdl design units. Return if any error shows up.
    if (!mVhdlReader.Elaborate(top_design_unit_name.c_str(), lastLib.c_str(), 0, mParamValPairs, 1)) return 0; // statically elaborates all vhdl modules in the lastLib library
    //because we are not passing in a generic map use the entity_default as the top level name.
    //check if this exeists, if so use it.
    Verific::VhdlPrimaryUnit *top_entity = 0;
    UtString top_level(top_design_unit_name.c_str());
    //use default configuration 
    //TODO: check if carbon have means for passing top level entity in
    //RJC RC#16  there are two command line arguments -verilogTop and -vhdlTop that define the top module/entity that should be used for this build (see CheetahContext::parseverilog for use of -vlogTop, and JaguarContext::getTopDesignUnit() for -vhdlTop )
    // ADDRESSED FD: OK, this is a TODO for command-line processing stage
    //RJC RC#18  why do we need to add the _default suffix to the top_level?, ,and what is the situation where we get here when top_level is not defined?
    // ADDRESSED FD: My understanding is that after static elaboration (for VHDL specific) in case if entity 
    // has generics verific creates at least two versions of vhdl entity : <entity_name> and <entity_name>_default
    // (there can be a lot of others based on specific generic instantiation values)
    // the first one is unelaborated version (which we have after analyze step), second one is statically elaborated version with
    // default values for generic.
    // I think it isn't quiet right to use the default one always, because we have already processed
    // command-line top level parameters and passed the map to static elaborator (see mParamValPairs).
    // So I think we need to find specific top-level entity - not the default one, in case if mParamValPairs map isn't empty.
    // For Verilog we don't have this problem, because we have just single module which is elaborated with specific parameter values or is just
    // default (no other version for libraries etc..)
    // TODO: For VHDL find correct entity name in case if mPramValPairs isn't empty.

    // Below is citation from Verific Vhdl elaborator documentation about this:
    // For each unbound component instantiation, a configuration specification may be specified either
    // in architecture or in configuration declaration. This configuration specification points to the entity
    // or configuration to be bound with the component instantiation. Binding of an instance with a
    // configuration, means bind with the entity configured by the configuration. If an entity is
    // instantiated multiple times, every instantiated entity may have different generic values and
    // different range constraint for unconstrained ports. So depending upon generic values and
    // constraints of unconstrained ports, single entity may have different flavors. Static elaboration
    // copies the original entity architecture pair for its different flavors with a unique name and
    // component instantiation is replaced by the instantiation of copied entity architecture pair. Before
    // the conversion instantiated entity/configuration is elaborated. If generics are not overridden by
    // instantiation and ports of instantiated entity are constrained, instantiated entity is copied with
    // name ‘<original_name>_default’ instead of using original entity. This is done so that original
    // entity can later be used in unmodified form from different positions.

    top_level += UtString("_default");
    if (lib && lib -> GetPrimUnit(top_level.c_str())) {
        top_entity = lib ? lib->GetPrimUnit(top_level.c_str()) : 0; // Get the top-level entity as _default
    } else {
        top_entity = lib ? lib->GetPrimUnit(top_design_unit_name.c_str()) : 0; // Get the top-level entity
    }
    // Exit from application if there is no top entity by the given name in the given Vhdl design
    if (!top_entity) { return 0; }
    mTopEntity = top_entity;
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
    //Verific::Message::PrintLine("\n");
    Verific::Message::PrintLine("Top level unit : ", top_entity ? top_entity->Name() : 0);
    //Verific::Message::PrintLine("\n");
#endif
    //Debug infomration
    TraverseVhdl(top_entity, 0 /*architecture name*/); // Traverse top level unit and the hierarchy under it
    return 1;
}

void
VerificDesignManager::TraverseVerilog(Verific::VeriModule *module)
{
    if (!module) { return; }
    // Get the scope of the module:
    Verific::VeriScope *scope = module->GetScope();
    // Find all the declared ids in this scope:
    Verific::Map *ids = scope ? scope->DeclArea() : 0;
    Verific::MapIter mi;
    Verific::VeriIdDef *id;
    FOREACH_MAP_ITEM(ids, mi, 0, &id) { // Traverse declared ids
        if (!id || !id->IsInst()) { continue; } // Consider only the instance ids
        Verific::VeriModuleInstantiation *mod_inst = id->GetModuleInstance(); // Take the moduleinstance
        Verific::VeriModule *mod = mod_inst ? mod_inst->GetInstantiatedModule() : 0; // The module instance is a module
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
        Verific::Message::PrintLine("Processing instance : ", id->Name());
#endif
        if (mod) { // This is verilog module insatantiation: need to go into that module
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
            Verific::Message::PrintLine("instantiated module : ", mod->Name());
            //Verific::Message::PrintLine("\n");
#endif
	    //just for debug for now
            TraverseVerilog(mod); // Traverse the instantiated module
        } else { // The module instance is a vhdl unit instance: need to go into that unit
            Verific::VeriName *instantiated_unit_name = mod_inst ? mod_inst->GetModuleNameRef() : 0;
            const char *entity_name = instantiated_unit_name ? instantiated_unit_name->GetName() : 0;

            // Find the lib_name from the attribute of the instance
            Verific::VeriExpression *attr = mod_inst ? mod_inst->GetAttribute(" vhdl_unit_lib_name") : 0;
            char *lib_name = attr ? attr->Image() : 0;
            // Image() routine on attr returns the string image of the VeriConsVal expression of attr
            // ie "/work/" so we take only the name of the lib "work" trancting the other portion:
            char *lib = lib_name;
            if (lib && lib[0]=='"') {
                lib++;
                lib[Verific::Strings::len(lib)-1] = 0;
            }
            // Find the vhdl unit for verilog instance
            char *arch_name = 0;
            Verific::VhdlPrimaryUnit *vhdl_unit = Verific::VeriNode::GetVhdlUnitForVerilogInstance(entity_name, lib, 0/*search_from_loptions*/, &arch_name);
            Verific::Strings::free(lib_name);
            if (!vhdl_unit) {
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
                Verific::Message::PrintLine("black-box instance");
#endif
                continue;
            }
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
            Verific::Message::PrintLine("instantiated vhdl unit : ", vhdl_unit->Name());
            if (arch_name) Verific::Message::PrintLine("architecture name : ", arch_name);
            //Verific::Message::PrintLine("\n");
#endif
	    //just for debug for now
            TraverseVhdl(vhdl_unit, arch_name); // Traverse instantiated unit
            Verific::Strings::free(arch_name);
        }
    }
}

// Traverse an entity and the hierarchy under it:
void
VerificDesignManager::TraverseVhdl(Verific::VhdlPrimaryUnit *unit, const char *arch_name)
{
    // Find the architecture of the unit:
    Verific::VhdlSecondaryUnit *arch = unit? unit->GetSecondaryUnit(arch_name) : 0;
    // Find the scope:
    Verific::VhdlScope *scope = arch ? arch->LocalScope() : 0;

    // Get the declared ids from that scope:
    Verific::Map *ids = scope ? scope->DeclArea() : 0;
    Verific::MapIter mi;
    Verific::VhdlIdDef *id;
    FOREACH_MAP_ITEM(ids, mi, 0, &id) {
        Verific::VhdlStatement *stmt = id ? id->GetStatement() : 0;
        // Get the instantiated unit id for instances only
        Verific::VhdlIdDef *inst_unit = stmt ? stmt->GetInstantiatedUnit() : 0;
        if (!inst_unit) { continue; } // Not an instance level
        
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
        Verific::Message::PrintLine("Processing instance : ", id->Name());
#endif
        
        if (inst_unit->IsComponent() || inst_unit->IsConfiguration()) {
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
            Verific::Message::PrintLine("black-box instance");
#endif
             continue; // black-box or not bind yet
        }
        if (inst_unit->IsEntity()) { // This is direct entity insatantiation
            Verific::VhdlPrimaryUnit *prim_unit = inst_unit ? inst_unit->GetPrimaryUnit() : 0; // Get the primary unit
            if (!prim_unit) { continue; }
            if (prim_unit->IsVerilogModule()) { // instance of verilog module
                // Get instantiated verilog module
                Verific::VeriModule *veri_module = Verific::vhdl_file::GetVerilogModuleFromlib(inst_unit->GetContainingLibraryName(), inst_unit->Name());
                if (!veri_module) { continue; }
                
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
                Verific::Message::PrintLine("instantiated module : ", veri_module->Name());
                //Verific::Message::PrintLine("\n");
#endif
                TraverseVerilog(veri_module); // Traverse instantiated module
            } else { // Instance of vhdl unit
                // Find the architecture name and traverse the vhdl design
                Verific::VhdlName *inst_name = stmt ? stmt->GetInstantiatedUnitNameNode() : 0;
                const char *arch_name = inst_name ? inst_name->ArchitectureNameAspect() : 0;
                
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
                Verific::Message::PrintLine("instantiated vhdl unit : ", prim_unit->Name());
                Verific::Message::PrintLine("architecture name : ", arch_name ? arch_name : 0);
                //Verific::Message::PrintLine("\n");
#endif
                TraverseVhdl(prim_unit, arch_name); // Traverse the vhdl unit
            }
        }
    }
}
/*deprecated*/
Verific::VeriModule*
VerificDesignManager::GetModule(const char* module_name)
{
    return Verific::veri_file::GetModule(module_name);
}

Verific::Map*
VerificDesignManager::GetAllModules()
{
    return Verific::veri_file::AllModules();
}

Verific::Map*
VerificDesignManager::GetAllPrimeryUnits()
{
  return Verific::vhdl_file::GetWorkLib()->AllPrimaryUnits();
}

Verific::VhdlPrimaryUnit*
VerificDesignManager::GetPrimeryUnit(const char* primery_unit_name)
{
    return Verific::vhdl_file::GetWorkLib()->GetPrimUnit(primery_unit_name);
}

bool
VerificDesignManager::StrEndWithStr(const char* str1, const char* str2)
{
    return strstr(str1, str2) && ! strcmp(strstr(str1, str2), str2);
}

void
VerificDesignManager::debugPrettyPrintVerilog(const UtString& libName)
{
    std::cerr << "DEBUG PRETTYPRINT VERILOG" << std::endl;
    Verific::VeriLibrary* lib = Verific::veri_file::GetLibrary(libName.c_str());
    if (lib) {
        std::cerr << "Lib: " << lib->GetName() << std::endl;
    }
    Verific::MapIter mi;
    Verific::VeriModule* mod;
    FOREACH_VERILOG_MODULE_IN_LIBRARY(lib, mi, mod) {
        std::cerr << "Module: " << mod->GetName() << std::endl;;
    }
}

void
VerificDesignManager::debugPrettyPrintVerilog()
{
    std::cerr << "DEBUG PRETTYPRINT VERILOG" << std::endl;
    Verific::VeriLibrary* lib;
    Verific::VeriModule *mod ;
    Verific::MapIter m1, m2 ;
    FOREACH_VERILOG_LIBRARY(m1, lib) {
        std::cerr << "Lib: " << lib->GetName() << std::endl;
        FOREACH_VERILOG_MODULE_IN_LIBRARY(lib, m2, mod) {
            std::cerr << "Module: " << mod->Name() << std::endl;
        }
    }
}

void
VerificDesignManager::debugPrettyPrint(const UtString& libName)
{
    std::cerr << "DEBUG PRETTYPRINT VHDL" << std::endl;
    Verific::MapIter m2, m3 ;
    Verific::VhdlLibrary *lib = Verific::vhdl_file::GetLibrary(libName.c_str());
    Verific::VhdlPrimaryUnit *primunit ;
    Verific::VhdlSecondaryUnit *secunit ;
    if (lib) {
        ::printf("Library %s\n",  lib->Name()) ;
    }
    // Iterate over all (primary unit) parse-trees that were analyzed
    FOREACH_VHDL_PRIMARY_UNIT(lib, m2, primunit) {
        ::printf("Entity %s\n",  primunit->Id()->Name()) ;
        // Iterate over all (secondary unit) parse-trees analyzed under this primary unit
        FOREACH_VHDL_SECONDARY_UNIT(primunit, m3, secunit) {
            //Verific::vhdl_file::PrettyPrint((UtString(primunit->Name()) + UtString(secunit->Name())).c_str(), primunit->Name(), lib->Name()) ;
            // Rule out any secondary unit that is not a Architecture :
            if (!secunit->Id()->IsArchitecture()) continue ;
            // Print name :
            ::printf("Architecture %s\n",  secunit->Id()->Name()) ;
            // Print line/file of where the architecture was declared :
            Verific::linefile_type linefile = secunit->Id()->Linefile() ;
            if (linefile) {
                ::printf(" was declared in file %s, line %d\n",
                        Verific::LineFile::GetFileName(linefile),
                        Verific::LineFile::GetLineNo(linefile)) ;
            }
        }
    }
}

void
VerificDesignManager::debugPrettyPrint()
{
    std::cerr << "DEBUG PRETTYPRINT VHDL" << std::endl;
    Verific::MapIter m1, m2, m3 ;
    Verific::VhdlLibrary* lib;
    Verific::VhdlPrimaryUnit *primunit ;
    Verific::VhdlSecondaryUnit *secunit ;
    FOREACH_VHDL_LIBRARY(m1, lib) {
        ::printf("Library %s\n",  lib->Name()) ;
        // Iterate over all (primary unit) parse-trees that were analyzed
        FOREACH_VHDL_PRIMARY_UNIT(lib, m2, primunit) {
            ::printf("Entity %s\n",  primunit->Id()->Name()) ;
            // Iterate over all (secondary unit) parse-trees analyzed under this primary unit
            FOREACH_VHDL_SECONDARY_UNIT(primunit, m3, secunit) {
                //Verific::vhdl_file::PrettyPrint((UtString(primunit->Name()) + UtString(secunit->Name())).c_str(), primunit->Name(), lib->Name()) ;
                // Rule out any secondary unit that is not a Architecture :
                if (!secunit->Id()->IsArchitecture()) continue ;
                // Print name :
                ::printf("Architecture %s\n",  secunit->Id()->Name()) ;
                // Print line/file of where the architecture was declared :
                Verific::linefile_type linefile = secunit->Id()->Linefile() ;
                if (linefile) {
                    ::printf(" was declared in file %s, line %d\n",
                            Verific::LineFile::GetFileName(linefile),
                            Verific::LineFile::GetLineNo(linefile)) ;
                }
            }
        }
    }
}

void VerificDesignManager::copyAllVlogLibraryModulesToWork(const UtString& workLibName) 
{
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
    std::cout << "copyAllVlogLibraryModulesToWork" << std::endl;
#endif

    Verific::VeriLibrary* work_lib = Verific::veri_file::GetLibrary(workLibName.c_str());
    if (!work_lib) return;
    Verific::VeriLibrary* lib;
    Verific::VeriModule *mod ;
    Verific::MapIter m1, m2 ;
    FOREACH_VERILOG_LIBRARY(m1, lib) {
        FOREACH_VERILOG_MODULE_IN_LIBRARY(lib, m2, mod) {
            if (!mod) continue ;
            // Check whether the module already exists in work library
            // Mostly existing module will be a root module, if not skip it:
            if (work_lib->GetModule(mod->Name(), 1)) continue;

            if (lib != work_lib) {
                // Detach the module from the user library:
                if (!lib->DetachModule(mod)) continue ; // Cannot remove it
                work_lib->AddModule(mod);
            }
        }
    }
}

// TODO this is not used at the moment, but it might be we will need it in future, anyway it should be created to copy secondary units (architectures) along with primary unit (entity)
void VerificDesignManager::copyAllVhdlLibraryToWork(const UtString& workLibName) 
{
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
    std::cout << "copyAllVhdlLibraryToWork" << std::endl;
#endif

    Verific::VhdlLibrary* work_lib = Verific::vhdl_file::GetLibrary(workLibName.c_str(),1);
    if (!work_lib) return;
    Verific::MapIter m1, m2, m3 ;
    Verific::VhdlLibrary* lib;
    Verific::VhdlPrimaryUnit *primunit ;
    Verific::VhdlSecondaryUnit *secunit ;
    FOREACH_VHDL_LIBRARY(m1, lib) {
        // Iterate over all (primary unit) parse-trees that were analyzed
        FOREACH_VHDL_PRIMARY_UNIT(lib, m2, primunit) {
            if (!primunit) continue ;
            // Check whether the module already exists in work library
            // Mostly existing module will be a root module, if not skip it:
            if (work_lib->GetPrimUnit(primunit->Id()->Name(), 1)) continue;
            // Check whether the module already exists in work library
            // Mostly existing module will be a root module, if not skip it:
            if (lib != work_lib) {
                //Detach the module from the user library:
                if (!DetachVhdlPrimUnit(primunit,lib)) continue ; // Cannot remove it
                work_lib->AddPrimUnit(primunit,1);
            }
            FOREACH_VHDL_SECONDARY_UNIT(primunit, m3, secunit) {
            //if(!secunit) continue;
             // Check whether the module already exists in work library
            // Mostly existing module will be a root module, if not skip it:
             //if (primunit->GetSecondaryUnit(primunit->Id()->Name())) continue;
             //if (lib !=  work_lib) {
                //Detach the module from the user library:
                 if (!DetachVhdlSecondaryUnit(secunit,primunit)) continue ; // Cannot remove it
                 primunit->AddSecondaryUnit(secunit); 
             // }
            }
        }
    }
}

unsigned VerificDesignManager::DetachVhdlPrimUnit(Verific::VhdlPrimaryUnit *unit, Verific::VhdlLibrary* lib)
{
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
    std::cout << "DetachVhdlPrimUnit" << std::endl;
#endif

 
    if (!unit) return 0 ; // no module to remove
    if (!lib->AllPrimaryUnits()) return 0 ; // module is not here.
    // remove it from the table :
    // Multiple paramset can be in _module_table, remove 'module' only
    unsigned result = 0 ;
    Verific::MapItem *item ;
    FOREACH_SAME_KEY_MAPITEM(lib->AllPrimaryUnits(), unit->Name(), item) {
        if (unit == (Verific::VhdlPrimaryUnit*)item->Value()) {
            result = lib->AllPrimaryUnits()->Remove(item) ;
            break ;
        }
    }

    //unsigned result = _module_table->Remove(module->GetName()) ;
    if (result) {
        // re-set the library pointer in the module
        unit->SetOwningLib(0) ;
    }
    return result ;
}

unsigned VerificDesignManager::DetachVhdlSecondaryUnit(Verific::VhdlSecondaryUnit *unit, Verific::VhdlPrimaryUnit* primunit)
{
#ifdef DEBUG_VERIFIC_DESIGN_MANAGER
    std::cout << "DetachVhdlSecondaryUnit" << std::endl;
#endif

    if (!unit) return 0 ; // no module to remove
    if (!primunit) return 0;
    
    if (!primunit->AllSecondaryUnits()) return 0 ; // module is not here.

    // remove it from the table :
    // Multiple paramset can be in _module_table, remove 'module' only
    unsigned result = 0 ;
    Verific::MapItem *item ;
    FOREACH_SAME_KEY_MAPITEM(primunit->AllSecondaryUnits(), unit->Name(), item) {
        if (unit == (Verific::VhdlSecondaryUnit*)item->Value()) {
            result = primunit->AllSecondaryUnits()->Remove(item) ;
            break ;
        }
    }
    //unsigned result = _module_table->Remove(module->GetName()) ;
    if (result) {
        // re-set the library pointer in the module
        unit->SetOwningUnit(0) ;
    }
    return result ;
}
void VerificDesignManager::PrettyPrintVerilogDesign(const UtString& suffix)
{
    Verific::veri_file::PrettyPrintDesign(suffix.c_str());
}

} // namespace verific2nucleus
