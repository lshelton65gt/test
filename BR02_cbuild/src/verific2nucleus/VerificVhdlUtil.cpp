// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// Steve Lim 2013-12-14 This file should be named VerificVhdlDesignWalker.cpp

#include "verific2nucleus/VerificVhdlDesignWalker.h"    // Visitor base class definition
#include "verific2nucleus/VerificNucleusBuilder.h"      // Nucleus builder class definition
#include "verific2nucleus/V2NDesignScope.h"         // Scope class definition
#include "verific2nucleus/Verific2NucleusUtilities.h"   // Utilities for building nucleus
#include "verific2nucleus/VerificDesignWalkerCB.h" // Included for MACRO VHDL_VISIT_CALL

#include "Array.h"              // Make dynamic array class Array available
#include "Strings.h"            // A string utility/wrapper class
#include "Message.h"            // Make message handlers available, not used in this example

#include "VhdlUnits.h"          // Definition of a libraries and design units
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations
#include "VhdlExpression.h"     // Definitions of all vhdl expressions
#include "VhdlStatement.h"      // Definitions of all vhdl statements
#include "VhdlIdDef.h"          // Definitions of all vhdl identifiers
#include "VhdlName.h"           // Definitions of all vhdl names
#include "VhdlConfiguration.h"  // Definitions of all vhdl configurations
#include "VhdlSpecification.h"  // Definitions of all vhdl specifications
#include "VhdlMisc.h"           // Definitions of miscellaneous vhdl constructrs
#include "VhdlScope.h"
#include "VhdlValue_Elab.h"
#include "vhdl_tokens.h"
#include "vhdl_file.h"
#include "veri_file.h"

#include "nucleus/NUBase.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUVectorNetHierRef.h"
#include "reduce/Fold.h"
#include "util/AtomicCache.h"
#include "util/CarbonAssert.h"
#include "util/UtConv.h"
#include "util/UtIOStream.h"
#include "util/CarbonTypeUtil.h"

#include <iostream>
#include <cmath>

//#define DEBUG_VERIFIC_VHDL_WALKER 1

namespace verific2nucleus {

using namespace Verific ;

VerificVhdlDesignWalker::VerificVhdlDesignWalker(VerificNucleusBuilder* verificNucleusBuilder)
  : mCurrentScope(0)
  , mScopeStack()
  , mNucleusBuilder(verificNucleusBuilder)
  , mParamType(VerificVhdlDesignWalker::eUnknownFormals)
  , mTask(0)
  , mIsFromSignedPackage(false)
  , mDataFlow(0)
  , mTargetConstraint(0)
  , mGuard(0)
  , mEntityVisit(false)
  , mIsInSubprogram(false)
  , mInsideConditionalSignalAssignment(false)
  , mIsConstParent(true)
  , mInProcessStatement(false)
  , mInWaitStatement(false)
  , mSensitivityList(0)
  , mEdgeExpr(0)
  , mArgArchName("")
  , mNameRegistry()
{
  mEnableVhdlLoopUnroll = mNucleusBuilder->getArgProc()->getBoolValue(CarbonContext::scVhdlLoopUnroll);
  mEnableVhdlFileIO = mNucleusBuilder->getArgProc()->getBoolValue(CarbonContext::scVhdlEnableFileIO);
  mEnableCSElab = mNucleusBuilder->getArgProc()->getBoolValue(CarbonContext::scEnableCSElab);
  mValues.clear();
  mDFForClean.clear();
}

VerificVhdlDesignWalker::~VerificVhdlDesignWalker()
{
}

bool VerificVhdlDesignWalker::gEntityVisit() const
{
    return mEntityVisit;
}

// Verific2NucleusMap maps VerificIds onto nucleus objects.
// See comments in VerificVhdlDesignWalker.h header file

VerificVhdlDesignWalker::Verific2NucleusMap::Verific2NucleusMap(void)
{
  // Create a top level path stack, save a pointer to it
  pushMapStack(); 
  mTopLevel = mPathMapStack.top();
}

VerificVhdlDesignWalker::Verific2NucleusMap::~Verific2NucleusMap(void)
{
  // Free up the top level stack
  popMapStack();
}

// Register nucleus object using path created with pushPathId, popPathId (records),
// or with id (if supplied and not NULL)
void VerificVhdlDesignWalker::Verific2NucleusMap::regNuObj(NUBase *obj, VhdlIdDef* id)
{
  if (id) {
    // Register nucleus object using supplied VhdlIdDef* 
    // (instead of path created with pushPathId, popPathId);
    VerificIdPath idPath;
    idPath.push_back(id);
    regNuObj(idPath, obj);
  } else {
    // Use mIdPath created by pushPathId, popPathId
    regNuObj(mIdPath, obj);
  }
}

// Private method to register nucleus object using supplied path.
// Users shouldn't need to build their own paths.
void VerificVhdlDesignWalker::Verific2NucleusMap::regNuObj(const VerificIdPath& idPath, NUBase* obj) 
{
  if (0) // Debug
  {
    UtIO::cout() << "DEBUG: registeringNucleusObject(" << obj << "): ";
    dumpIdPath(idPath);
    if (NUBase* existing_obj = getNuObj(idPath)) {
      if (existing_obj == obj) {
        UtIO::cout() << "DEBUG: Object has already been registered" << UtIO::endl;
      } else {
        UtIO::cout() << "DEBUG: ERROR: path already registered to a different object" << UtIO::endl;
      }
    }
  }
  
  // Register in the subprogram scope (if any). 
  if (mTopLevel != mPathMapStack.top()) {
    // Register a declaration local to the subprogram. If
    // it's not local to the subprogram, then it must be in
    // a package.
    regNuObj(mPathMapStack.top(),idPath, obj);
  }
  
  // Register it at the top level (even if already registered at the
  // subprogram level). This follows the semantics of the
  // previous map implementation, seems to be necessary for package declarations.
  // TODO: Rethink this. It seem like we should only register in the
  // top level scope if it's a package decl. Can we figure this out?
  regNuObj(mTopLevel, idPath, obj);
};

// Private method to register a nucleus object given the VerificIdPathMap (which
// may be a member of a stack (see push/popPathMap). This is the base level 
// registration method used by all others of the same name.
void VerificVhdlDesignWalker::Verific2NucleusMap::regNuObj(VerificIdPathMap* pm, const VerificIdPath& idPath, NUBase* obj)
{
  // To retain the same semantics as the previous map implementation,
  // make sure last inserted item overrides previous inserted item
  // with the same key.
  VerificIdPathMap::iterator i = pm->find(idPath);
  if (i != pm->end() && i->second != obj) {
    pm->erase(i);
  }
  // Insert      
  pm->insert(UtPair<VerificIdPath, NUBase*>(idPath, obj));
};

// Return field index of suffix for record prefix. Returns true on success, false otherwise
bool VerificVhdlDesignWalker::Verific2NucleusMap::getCompositeField(VhdlSelectedName* name, UInt32* fieldIndex)
{
  bool success = false;
  VhdlName* prefix = name->GetPrefix();
  // Look up NUBase object for suffix
  NUBase* field = getNuObj(name);
  // Look up NUComposite object for prefix
  if (prefix) {
    NUBase* compBase = getNuObj(prefix);
    // Now find the field index in 'comp' for 'field'
    NUCompositeNet* cNet = dynamic_cast<NUCompositeNet*>(compBase);
    if (cNet) {
      size_t i = 0;
      for (i = 0; i < cNet->getNumFields(); ++i)
      {
        if (cNet->getField(i) == field) {
          *fieldIndex = i;
          success = true;
          break;
        }
      }
    }
  }
  
  return success;
}

    // Retrieve a nucleus object, given single id or path
NUBase* VerificVhdlDesignWalker::Verific2NucleusMap::getNuObj(VhdlIdDef* id)
{
  INFO_ASSERT(id, "Unexpected NULL id passed to getNuObj");
  VerificIdPath idPath;
  idPath.push_back(id);
  NUBase* obj = getNuObj(idPath);
  return obj;
}

NUBase* VerificVhdlDesignWalker::Verific2NucleusMap::getNuObj(const VerificIdPath& path)
{
  NUBase* obj = NULL;
  // Look first in the subprogram stack
  // (or top level stack, if we're not nested in a subprogram call)
  VerificIdPathMap* pm = mPathMapStack.top();
  VerificIdPathMap::iterator i = pm->find(path);
  if (i != pm->end()) {
    obj = i->second;
  }
  // If not there, look in top level stack associated
  // with entity/architecture (if we haven't already
  // looked there)
  if (!obj && pm != mTopLevel) {
    i = mTopLevel->find(path) ;
    if (i != mTopLevel->end()) {
      obj = i->second;
    }
  }
  
  return obj;
};

    // Retrieve a nucleus object, given a VhdlName*
NUBase* VerificVhdlDesignWalker::Verific2NucleusMap::getNuObj(const VhdlName* name)
{
  VerificIdPath idPath;
  populateVerificIdPath(name, idPath);
  NUBase* base = getNuObj(idPath);
  if (0) // Debug
  {
    UtIO::cout() << "DEBUG: getting nucleus object for ";
    dumpIdPath(idPath);
  }
  return base;
}

// Helper for populateVerificIdPath.
// We don't have to use library, entity, block VhdlIdDefs in
// the path to resolve the id. Verific has already uniquified
// the id for us. So, to look up "block1.block2.variable",
// we only need the VhdlIdDef* for 'variable'
bool VerificVhdlDesignWalker::Verific2NucleusMap::requiredForResolution(VhdlIdDef* id)
{
  bool required = true;
  if (!id) {
    required = false;
  } else if (id->IsBlock() || id->IsEntity() || id->IsLibrary() || id->IsArchitecture()) {
    required = false;
  }
  return required;
}
    
// Helper function populates a VerificIdPath given a VhdlName*.
// Example: For "a.b(2).c", the following VerificIdPath vector
// will be constructed:
//    0 - "a"
//    1 - "b"
//    2 - "c"
void VerificVhdlDesignWalker::Verific2NucleusMap::populateVerificIdPath(const VhdlName* item, VerificIdPath& idPath)
{
  if (item) {
    // If the VhdlName has indices/ranges, strip them off.
    while (item->GetAssocList())
      item = item->GetPrefix();
    // Populate prefix (if any) first, then suffix
    VhdlName* prefix = item->GetPrefix();
    populateVerificIdPath(prefix, idPath);
    // Get suffix id, or simple id if this name has no suffix
    VhdlName* suffix = item->GetSuffix();
    VhdlIdDef* suffix_id = (suffix && suffix->GetId()) ? suffix->GetId() : item->GetId();
    // Id can be null for references that include block (and possibly other) names.
    // In these cases, the final suffix_id will refer to the resolved net.
    // Entity, library, and block names are ignored during the registration
    // process, and are not required to resolve the signal/variable reference.
    if (requiredForResolution(suffix_id))
      idPath.push_back(suffix_id);
    else {
      if (0) // Debug
      {
        UtIO::cout() << "DEBUG: VhdlIdDef* " << Verific2NucleusUtilities::verific2String(*suffix_id) << " is NOT required for resolution." << UtIO::endl;
      }
    }
  }
}

// Debugging function - Dump a VerificIdPath, as stored in the Verific2NucleusMap
void VerificVhdlDesignWalker::Verific2NucleusMap::dumpIdPath(const VerificIdPath& path)
{
  // left most id is stored first
  VerificIdPath::const_iterator j = path.begin();
  if (j == path.end()) {
    UtIO::cout() << "Path is empty." << UtIO::endl;
  } else {
    UtIO::cout() << Verific2NucleusUtilities::verific2String(**j) << "(id:" << *j << ")";
  }
  VerificIdPath::const_iterator i;
  for (i = j + 1; i != path.end(); ++i) {
    UtIO::cout() << "." << Verific2NucleusUtilities::verific2String(**i) << "(id:" << *i << ")";
  }
  if (j != path.end()) {
    UtIO::cout() << UtIO::endl;
  }
}

// Push/Pop a new map stack for subprogram entry/exit.
// These may become unnecessary if we eliminate duplication
// of functions during nucleus population. See comments at
// the beginning of the class header.
void VerificVhdlDesignWalker::Verific2NucleusMap::pushMapStack(void) 
{
  VerificIdPathMap* pm = new VerificIdPathMap;
  mPathMapStack.push(pm);
}
void VerificVhdlDesignWalker::Verific2NucleusMap::popMapStack(void)
{
  VerificIdPathMap* pm = mPathMapStack.top();
  mPathMapStack.pop();
  delete pm;
}

//! Compare two VerificIdPaths, using lexical ordering rules
bool VerificVhdlDesignWalker::Verific2NucleusMap::IdPathCmp::operator()(const VerificIdPath& path1, const VerificIdPath& path2) const
{
  // We should never put empty paths in the map.
  INFO_ASSERT(!path1.empty() || !path2.empty(), "Unexpected empty path");
  // path1 is less than path2 if a VhdlIdDef* in path1 is less 
  // than the corresponding VhdlIdDef* in path2, and all previous 
  // VhdlIdDef* were equal.
  VerificIdPath::const_iterator i1 = path1.begin();
  VerificIdPath::const_iterator i2 = path2.begin();
  bool less_than = false;
  bool gt_than = false;
  for (; i1 != path1.end() && i2 != path2.end(); ++i1, ++i2) {
    VhdlIdDef* id1 = *i1;
    VhdlIdDef* id2 = *i2;
    if (id1 < id2) {
      less_than = true;
      break;
    } else if (id1 != id2) {
      gt_than = true;
      break;
    }
  }
  // By the time we get here, we've either confirmed path1 < path2,
  // path1 > path2, or ran out of ids in one or both of the paths. 
  // In the latter case, if path1 is shorter than
  // path2, we'll say it's less than path2.
  if (!less_than && !gt_than && path1.size() < path2.size())
    less_than = true;
  
  return less_than;
}

// Return the lowest id in the path (required for user type info)
VhdlIdDef* VerificVhdlDesignWalker::Verific2NucleusMap::getSuffixId(void)
{
  size_t len = mIdPath.size();
  INFO_ASSERT(len > 0, "Unexpected empty VerificIdPath");
  VhdlIdDef* id = mIdPath[len-1];
  return id;
}

// Given a NUComposite, and a VhdlIdDef representing a field of the composite, find the
// field number within the composite.The VhdlIdDef* must be guaranteed to be a field within
// the composite. Returns true on success, false on failure.
bool
VerificVhdlDesignWalker::getCompositeField(NUCompositeNet* cNet, VhdlIdDef* id, UInt32* fieldIndex)
{
  bool success = false;
  
  INFO_ASSERT(cNet, "Unexpected NULL value for nucleus composite net");
  INFO_ASSERT(id, "Unexpected NULL composite field select id value");
  // Formulate the name of the field
  UtString fieldName(id->Name());
  fieldName.uppercase();
  size_t i = 0;
  for (i = 0; i < cNet->getNumFields(); ++i)
  {
    NUNet* fieldNet = cNet->getField(i);
    // Record nets are mangled during population. To get the original name, we
    // have to use NUNet* for the field to index mCompositeNet array, which 
    // will yield the original net name.
    if (NUCompositeNet* compFieldNet = dynamic_cast<NUCompositeNet*>(fieldNet)) {
      INFO_ASSERT(mCompositeNet.end() != mCompositeNet.find(compFieldNet), "Composite net not registered in mCompositeNet");
      UtString origName(mCompositeNet.find(compFieldNet)->second->str());
      origName.uppercase();
      if (origName == fieldName) {
        *fieldIndex = i;
        success = true;
        break;
      }
    } else {
      // Get the field name directly for index 'i' 
      UtString index(cNet->getField(i)->getName()->str());
      index.uppercase();
      if (fieldName == index) {
        *fieldIndex = i;
        success = true;
        break;
      }
    }
  }
  

  return success;
}

// Wrapper to register a nucleus object, and create
// user type information
void VerificVhdlDesignWalker::registerNucleusObject(NUBase* obj, VhdlIdDef* id)
{
  // Place the object in the registry (using id, if supplied, else path)
  mVerificToNucleusObject.regNuObj(obj, id);

  // Set input/output flags as required on nested fields
  correctCompositeNetFlags(obj);

  // If no id supplied, get suffix id used for registration
  if (!id)
    id = mVerificToNucleusObject.getSuffixId();

  // Construct information for UserTypeInfo.
  recordUserTypeInfo(id, obj);
}

void VerificVhdlDesignWalker::recordUserTypeInfo(VhdlIdDef* n, NUBase* o)
{
  //collect information for UserTypeInfo
  NUNet* net = dynamic_cast<NUNet*>(o);
  V2N_INFO_ASSERT(net, *n, "we registered only NUNet object");
    
  if (dynamic_cast<NUMemoryNetHierRef*>(net) ||
      dynamic_cast<NUVectorNetHierRef*>(net) ||
      n->IsRecordElement()) {
    return ;
  }
  //get net scope
  NUScope* scope = net->getScope();
  ScopeToNodeNetMap::iterator iter = mNetMap.find(scope);
  if (mNetMap.end() == iter) {
    VerificNodeNetMap vNetMap;
    vNetMap.insert(UtPair<VhdlIdDef*, NUNet*>(n, net));
    mNetMap.insert(UtPair<NUScope*, VerificNodeNetMap>(scope, vNetMap));
  } else {
    iter->second.insert(UtPair<VhdlIdDef*, NUNet*>(n, net));
  }
  if (dynamic_cast<NUVectorNet*>(net)) {
    if (n->Constraint()) {
      if (-2147483647 == n->Constraint()->Left()->Integer()) {
        ConstantRange cr(n->Constraint()->Left()->Integer() - 1, n->Constraint()->Right()->Integer());
        mConstraintRangeMap.insert(UtPair<NUNet*, ConstantRange>(net, cr));
      } else {
        ConstantRange cr(n->Constraint()->Left()->Integer(), n->Constraint()->Right()->Integer());
        mConstraintRangeMap.insert(UtPair<NUNet*, ConstantRange>(net, cr));
      }
    } else {
      //induction variable for loops
      ConstantRange cr(-2147483648LL, 2147483647);
      mConstraintRangeMap.insert(UtPair<NUNet*, ConstantRange>(net, cr));
    }
  } else if (dynamic_cast<NUMemoryNet*>(net)) {
    // Find the element constraint on the deepest element of the array. For integers,
    // this gives the range of allowable values. For other types, VerificUserTypePopulate.cpp
    // does not seem to care.
    VhdlConstraint* elem_constraint = n->Constraint();
    while (NULL != elem_constraint->ElementConstraint()) 
      elem_constraint = elem_constraint->ElementConstraint();
    ConstantRange cr(elem_constraint->Left()->Integer(), elem_constraint->Right()->Integer());
    mConstraintRangeMap.insert(UtPair<NUNet*, ConstantRange>(net, cr));
  }
}

// Retrieve the registered NUBase* object that corresponds
// to a VhdlIdDef* node. 
NUBase* VerificVhdlDesignWalker::getNucleusObject(VhdlIdDef* node)
{
  NUBase* base = mVerificToNucleusObject.getNuObj(node);
  return base;
}

//corner case embedded composite nets need flags correction ./misc/rec2.vhdl
//check correctness of flags
//correct and reassign if needed
//eg misc/rec2.vhdl
void VerificVhdlDesignWalker::correctCompositeNetFlags(NUBase* base)
{
    INFO_ASSERT(base, "invalid NUBase pointer");
    if (NUCompositeNet* compNet = dynamic_cast<NUCompositeNet*>(base)) {
        UInt32 sz = compNet->getNumFields();
        for (UInt32 i = 0; i < sz; ++i) {
            NUNet* field = compNet->getField(i);
            if (compNet->isInput() && ! field->isInput()) {
                field->putIsInput(true);
            } else if (compNet->isOutput() && ! field->isOutput()) {
                field->putIsOutput(true);
            }
        }
        for (UInt32 i = 0; i < sz; ++i) {
            NUNet* field = compNet->getField(i);
            correctCompositeNetFlags(field);
        }
    }
}


///gets Nucleus builder object
VerificNucleusBuilder* VerificVhdlDesignWalker::gNucleusBuilder() 
{
    return mNucleusBuilder;
}

///gets Nucleus builder object (read-only)
const VerificNucleusBuilder* VerificVhdlDesignWalker::gNucleusBuilder() const 
{
    return mNucleusBuilder;
}

///call nucleus builder uDesign() method
void VerificVhdlDesignWalker::uDesign()
{
    mNucleusBuilder->uDesign();
}

///create induction variable for 'forscheme'
NUNet* VerificVhdlDesignWalker::cInductionVar(VhdlIdDef* node)
{
  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node->Linefile());
  //FD: TODO rewrite this logic, the whole section is about getting scope -
  //should be simple 1-2 function call instead
  ////////////////////////////////////////////////////////////////////////////////
  //get nameddeclaration scope or task for adding induction variable
  //as local , rise up and reach NamedDeclarationScope or Task
  V2NDesignScope* vds = mCurrentScope;
  bool is_nds = true; //detector NamedDeclarationScope or Task
  while (true) {
      // FD thinks: how can we have a guarantee that there is task or named declaration scope ? What about module scope ? can't loop be in module scope ?
      //if (V2NDesignScope::NAMEDDECLARATIONSCOPE == vds->gScopeType()) {
      if (vds->gDeclarationScope()) {
          //we arrived to NamedDeclarationScope
          break;
      } else {
          NUScope* scope = vds->gOwner();
          NUCLEUS_CONSISTENCY(scope, src_loc, "NULL scope object");
          if (NUScope::eTask == scope->getScopeType()) {
              //we arrived to Task
              is_nds = false;
              break;
          }
      }
      vds = vds->gParentScope();
      if (! vds) {
          V2N_INFO_ASSERT(false, *node, "");
      }
  }
  //get NUScope corresponding to V2NDesignScope
  NUScope* scope = 0;
  if (is_nds) { //get NUScope from NamedDeclarationScope
      NUNamedDeclarationScope* nds = dynamic_cast<NUNamedDeclarationScope*>(vds->gDeclarationScope());
      INFO_ASSERT(nds, "Invalid NUNamedDeclarationScope");
      scope = dynamic_cast<NUScope*>(nds);
  } else { //get NUScope from Task
      NUTask* task = dynamic_cast<NUTask*>(vds->gOwner());
      V2N_INFO_ASSERT(task, *node, "Failed cast to NUTask");
      scope = dynamic_cast<NUScope*>(task);
  }
  V2N_INFO_ASSERT(scope, *node, "Failed to get owner NUScope object for induction variable");
  ////////////////////////////////////////////////////////////////////////////////

  //set flags for induction net
  VectorNetFlags vflags(eSignedSubrange);
  NetFlags flags = NetFlags(eAllocatedNet | eSigned);
  // FD thinks: there is inconsistency above paragraph doesn't check module case so why it is checked here ?
  if (NUScope::eModule != scope->getScopeType()) {
      flags = NetFlags(flags | eBlockLocalNet);
  }
  if (scope->inTFHier()) {
      flags = NetFlags(flags | eNonStaticNet);
  }
  // NetFlags flags = NetFlags(eAllocatedNet  | eBlockLocalNet | eSigned);
  //create induction net for 'for-loop'
  UtString str;
  //StringAtom* strAtom = mNucleusBuilder->gCache()->intern("induction_val");
  StringAtom* strAtom = getModule()->gensym("for_index");
  NUNet* induction = new NUVectorNet(strAtom,
                              ConstantRange(31, 0),
                              flags,
                              vflags,
                              scope,
                              src_loc);
  V2N_INFO_ASSERT(induction, *node, "Failed to create induction net");
  scope->addLocal(induction);
  registerNucleusObject(induction, node);
  return induction;
}



///Map Verific unary operator to Nucleus unary operator
NUOp::OpT VerificVhdlDesignWalker::unaryOpKind(VhdlOperator &node, NUExpr* exp) const
{
    INFO_ASSERT(exp, "invalid NUExpr object");
    switch (node.GetOperatorToken()) {
    case VHDL_not :   {
        if (1 == exp->determineBitSize()) {
            return NUOp::eUnLogNot;
        }
        return NUOp::eUnBitNeg;
    }
    case VHDL_MINUS : return NUOp::eUnMinus;
    case VHDL_PLUS :  return NUOp::eUnPlus;
    case VHDL_abs :   return  NUOp::eUnAbs;
    default: {
#ifdef DEBUG_VERIFIC_VHDL_WALKER
        std::cerr << VhdlNode::OperatorSimpleName(node.GetOperatorToken()) << std::endl;
#endif
    }
    }
    return NUOp::eInvalid;
}

//get bitness of expression
UInt32 VerificVhdlDesignWalker::gBits(NUOp::OpT kind, NUExpr* expr) const
{
    if (NUOp::eBiULte == kind  ||
        NUOp::eBiEq == kind    ||
        NUOp::eBiNeq == kind   ||
        NUOp::eBiUGtr == kind  ||
        NUOp::eBiUGtre == kind ||
        NUOp::eBiULt == kind) {
        return 1;
    }
    
    return expr->getBitSize();
}



//RJC RC#19 this method was copied from a static function: VhPopulateExpr.cxx:sRequiresOpsSameSize and now it has been promoted to being a method of
//VerificVhdlDesignWalker, is there a reason that the scope of this function was expanded?  I don't see that this method is needed beyond
//the file VerificVhdlExpressionWalker.cpp.  I don't understand the logic behind the expansion of this scope.
//ADDRESSED

///Return true if binary operator require same size for both rhs and lhs
///otherwise return false

bool VerificVhdlDesignWalker::isRequiresOpsSameSize(VhdlOperator& node) const 
{
  switch(node.GetOperatorToken()) {
  case VHDL_PLUS:
  case VHDL_MINUS:
  case VHDL_SLASH:
  case VHDL_mod:
  case VHDL_rem:
  case VHDL_xor:
  case VHDL_and:
  case VHDL_or:
  case VHDL_downto:
    return true;
  default:
    break;
  }
  return false;
}



//RJC RC#19 this method was copied from a static function: VhPopulateExpr.cxx:sIsDirty and now it has been promoted to being a method of
//VerificVhdlDesignWalker, is there a reason that the scope of this function was expanded?  I don't see that this method is needed beyond
//the file VerificVhdlExpressionWalker.cpp.  I don't understand the logic behind the expansion of this scope.
//ADDRESSED

//VhPopulateExpr.cxx:sIsDirty(NUexpr* expr)
// copied from interra flow, sIsDirty()

bool VerificVhdlDesignWalker::isDirty(NUExpr* expr) const
{
    NUOp* op = dynamic_cast<NUOp*>(expr);
    if (op != NULL) {
        switch (op->getOp()) {
        case NUOp::eUnMinus:
        case NUOp::eBiMinus:
        case NUOp::eBiSMult:
        case NUOp::eBiUMult:
        case NUOp::eBiPlus:
/* -- these were affected by having eBiPlus in here previously
   test/vhdl/lang_rotate
   test/cust/matrox/sunex/carbon/cwtb/dmatop
   test/vhdl/beacon7
   test/bugs/test.run.group3100
   test/cust/matrox/phoenix/carbon/cwtb/rttop
   test/vhdl/lang_miscops
*/
        case NUOp::eBiVhLshift:
        case NUOp::eBiVhLshiftArith:
        case NUOp::eBiLshift:
            return true;
        default:
            break;
        }
    }
    return false;
}

//return true only if \q node is a relational operator (like > or = )
bool VerificVhdlDesignWalker::isRelationalOp(VhdlOperator& node) const
{
       switch (node.GetOperatorToken()) {
       case VHDL_GTHAN :
       case VHDL_SEQUAL :
       case VHDL_GEQUAL:
       case VHDL_STHAN :
       case VHDL_EQUAL :
       case VHDL_NEQUAL :
           return true;
       default:
           break;
       }
       return false;
}

///Map Verific binary operator to Nucleus binary operator
NUOp::OpT VerificVhdlDesignWalker::binaryOpKind(VhdlOperator &node, bool isSigned) const
{
    switch (node.GetOperatorToken()) {
        //relations
    case VHDL_or : return NUOp::eBiBitOr;
    case VHDL_xor : return NUOp::eBiBitXor;
    case VHDL_and : return  NUOp::eBiBitAnd;
        //relational
    case VHDL_GTHAN : return isSigned || mIsFromSignedPackage ? NUOp::eBiSGtr : NUOp::eBiUGtr;
    case VHDL_SEQUAL : return isSigned || mIsFromSignedPackage ? NUOp::eBiSLte : NUOp::eBiULte;
    case VHDL_GEQUAL: return isSigned || mIsFromSignedPackage ? NUOp::eBiSGtre : NUOp::eBiUGtre;
    case VHDL_STHAN : return isSigned || mIsFromSignedPackage ? NUOp::eBiSLt :  NUOp::eBiULt;
    case VHDL_EQUAL : return NUOp::eBiEq;
    case VHDL_NEQUAL : return NUOp::eBiNeq;
        //arithmetic
    case VHDL_STAR : return isSigned || mIsFromSignedPackage ? NUOp::eBiSMult : NUOp::eBiUMult;
    case VHDL_SLASH : return isSigned || mIsFromSignedPackage ? NUOp::eBiSDiv : NUOp::eBiUDiv;
    case VHDL_rem : return isSigned || mIsFromSignedPackage ? NUOp::eBiSMod : NUOp::eBiUMod;
    case VHDL_PLUS : return NUOp::eBiPlus;
    case VHDL_MINUS : return NUOp::eBiMinus;
    case VHDL_mod : return NUOp::eBiVhdlMod;
    case VHDL_EXPONENT: return NUOp::eBiExp;
    case VHDL_AMPERSAND: return NUOp::eNaConcat;
    case VHDL_sla : return NUOp::eBiVhLshiftArith;
    case VHDL_sra : return NUOp::eBiVhRshiftArith;
    case VHDL_sll : return NUOp::eBiVhLshift;
    case VHDL_srl : return NUOp::eBiVhRshift;
    case VHDL_ror : return NUOp::eBiRoR;
    case VHDL_rol : return NUOp::eBiRoL;
    default: {
#ifdef DEBUG_VERIFIC_VHDL_WALKER
      std::cerr << VhdlNode::OperatorSimpleName(node.GetOperatorToken()) << std::endl;
#endif
    }
    }
    return NUOp::eInvalid;
}

///Gets source locator
SourceLocator VerificVhdlDesignWalker::gSrcLine(linefile_type lf)
{
  return Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder, lf);
}

///Set flags for NUNet object called in VHDL_VISIT(VhdlIdDef, node) method
NetFlags VerificVhdlDesignWalker::sFlags(VhdlIdDef& node)
{
    
  NetFlags flags(eAllocatedNet);

  if (!node.IsAlias()) {
      if (node.IsInout()) {
          flags = NetFlags(flags | eBidNet);
      }
      if (node.IsInput()) {
          flags = NetFlags(flags | eInputNet);
      }
      if (node.IsOutput() || node.IsBuffer()) {
          flags = NetFlags(flags | eOutputNet);
      }
  }

  if (node.IsSignal() && !node.IsSubprogramFormal()) {
      flags = NetFlags(flags | eDMWireNet);
  }
  if (node.IsConstant() && ! node.IsSubprogramFormal()) {
      flags = NetFlags(flags | eDMWireNet);
  }
  if (node.IsPort()) {
      flags = NetFlags(flags | eDMWireNet);
  }
  if (node.IsSubprogramFormal() && node.IsInout()) {
      flags = NetFlags(flags | eDMWireNet);
  }
  NUScope* nu_scope = getNUScope(NULL);
  VERIFIC_CONSISTENCY_WITHOUT_RETURN(nu_scope, node, "Unable to get scope");
  if (nu_scope && (NUScope::eModule != nu_scope->getScopeType())) {
      flags = NetFlags(flags | eBlockLocalNet);
  }
  if (node.ElementType() && node.ElementType()->IsIntegerType() && 
      (UtString("natural") != UtString(node.Type()->Name()) &&
       UtString("small_int") != UtString(node.Type()->Name()))) {
      if (VhdlExplicitSubtypeIndication* esInd = dynamic_cast<VhdlExplicitSubtypeIndication*>(node.ElementType()->GetSubtypeIndication())) {
          VhdlConstraint* vConstr = esInd->GetRangeConstraint()->EvaluateConstraint(mDataFlow, 0);
          if (vConstr->Left()->Integer() >= 0 &&
              vConstr->Right()->Integer() >= 0) {
              ;
          } else {
              flags = NetFlags(flags | eSigned);
          }
          delete vConstr;
      } else {
          flags = NetFlags(flags | eSigned);
      }
  }
  
  if ((node.Type() && node.Type()->IsIntegerType()) && 
      (UtString("natural") != UtString(node.Type()->Name()) &&
       UtString("small_int") != UtString(node.Type()->Name()))) {
      if (node.Constraint()) {
          if (node.Constraint()->IsSignedRange()) {
              flags = NetFlags(flags | eSigned );
          }
      } else {
          flags = NetFlags(flags | eSigned );
      }
  }
  if (node.Type() && node.Type()->IsStdTimeType()) {
    flags = NetFlags(flags | eDMTimeNet);
  }
  if (V2NDesignScope::SUB_PROG_DECL == mCurrentScope->gContext()) {
    flags = NetFlags(flags | eNonStaticNet);
  }
  if (node.IsRecordElement()) {
    flags = NetFlags(flags | eDMWireNet | eBlockLocalNet);
  }
  if (node.Type() && (UtString("signed") == UtString(node.Type()->Name()))) {
      flags = NetFlags (flags | eSigned);
  }
  
  if ((node.ElementType()) &&
      (UtString(node.ElementType()->Name()) == UtString("signed"))) {
      flags = NetFlags (flags | eSigned);
  }

  if (node.Type() && node.Type()->BaseType() &&
      (UtString(node.Type()->BaseType()->Name()) == UtString("signed"))) {
      flags = NetFlags (flags | eSigned);
  }
  if (node.BaseType() &&
      (UtString(node.BaseType()->Name()) == UtString("signed"))) {
      flags = NetFlags (flags | eSigned);
  }
  if (node.ElementType() && (UtString(node.ElementType()->Type()->Name()) == UtString("signed"))) {
      flags = NetFlags (flags | eSigned);
  }
  if (node.ElementType() && node.ElementType()->ElementType() &&
      (UtString(node.ElementType()->ElementType()->Name()) == UtString("signed"))) {
      flags = NetFlags (flags | eSigned);
  }
  return flags;
}

///Gets ConstantRange from subtype indication
ConstantRange VerificVhdlDesignWalker::gRange(VhdlSubtypeIndication* node)
{
  node->Accept(*this);
  UtPair<int, int> p = mCurrentScope->gRange();
  return ConstantRange(p.first, p.second);
}

// Given an expression, create a temporary vector of the same size.
//
// If the current context is within an always block or task, then
// create a blocking assign from the expression to the temporary
// vector, and add the assignment to the current block.
//
// If the current context is not within an always block or task,
// then create the block, create a continuous assign from the expression
// to the temporary vector, and add the assignment to
// the block. 
//
// If successful then return a NUIdentRvalue pointer to the temporary so the
// called can access this temp value.  If there was a problem return NULL

NUIdentRvalue* VerificVhdlDesignWalker::assignToTempVector(NUExpr* expr, const char* name, const SourceLocator& src_loc)
{
  NUScope* declaration_scope = getDeclarationScope();//get neareast nameddeclaration scope (if not found task/module scope will be returned)
                                         //for added return net 
  UInt32 retSize = expr->getBitSize();
  UInt32 detSize = expr->determineBitSize();
  UInt32 tempSize = retSize > detSize ? retSize : detSize;
  expr->resize(tempSize);
  // TODO consider case where size is 1 bit, should we not create a tempNet instead of TempVectorNet?
  StringAtom* tempName = declaration_scope->gensym("tempval", name, NULL, false);
  INFO_ASSERT(tempName, "Failed to create name for temporary net");
  NUNet* temp = declaration_scope->createTempVectorNet(tempName, ConstantRange(tempSize - 1, 0),
                                                       expr->isSignedResult(), src_loc);
  NUIdentLvalue* lvalue = new NUIdentLvalue(temp, src_loc);

  bool success = createAssignInCurrentContext(lvalue, expr, src_loc);
  if ( ! success ) return NULL;

  //create ident rvalue and return
  NUIdentRvalue* rvalue = new NUIdentRvalue(temp, src_loc);
  return rvalue;
}


// create NUExpr(s) for the items from \a array, placing the expressions in arg1 and arg2.
//  helper for typeConversion()
void VerificVhdlDesignWalker::populateFromArray(Array* array, NUExpr** arg1, NUExpr** arg2){
  UInt32 num_arguments = 0;
  UInt32 cur_index = 0;
  VhdlTreeNode* elem = NULL;
  UInt32 max_num_args = ( (arg2) ? 2 : (arg1 ? 1 : 0)); // max allowed is controlled by slots provided by caller
  V2NDesignScope::ContextType origContext = mCurrentScope->gContext();

  // the maximum number of arguments should be enforced by analyzer, but check here anyway
  FOREACH_ARRAY_ITEM(array, cur_index, elem) {
    if ( max_num_args < (num_arguments+1) ) {
      break;                    // caller did not request this many results so exit before finishing the array
    }

    mCurrentScope->sContext(V2NDesignScope::RVALUE);
    elem->Accept(*this);
    NUBase* base = mCurrentScope->gValue();

    switch ( cur_index ){
    case 0: {(*arg1) = dynamic_cast<NUExpr*>(base); INFO_ASSERT(*arg1, "unable to populate argument 1"); break;}
    case 1: {(*arg2) = dynamic_cast<NUExpr*>(base); INFO_ASSERT(*arg2, "unable to populate argument 2"); break;}
    default: INFO_ASSERT((cur_index < max_num_args), "unsupported number of arguments");
    }
    ++num_arguments;
  }
  mCurrentScope->sValue(NULL);  // be safe
  mCurrentScope->sContext(origContext);

  return;
}



/// Create NUExpr corresponding to a recognized standard package function call, and returns true;
// This function will return false (and print an error message) if the called VHDL function does not correspond to any standard package function.
/// only functions from IEEE package (and std.textio.endfile?) are supported here
// based on VhPopulateSubprogram.cxx:hdlPopulate::acceleratedExprForFunctionCall
// why is this method implementing stuf like rotate_left?  should it not be done in the attemptConversion_* methods?
// note this function DOES NOT REQUIRE THAT  node.IsTypeConversion() be true.
bool VerificVhdlDesignWalker::typeConversion(const VhdlIndexedName& node)
{
  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  V2N_INFO_ASSERT(node.GetPrefix(), node, "Invalid prefix node for type conversion function");
  
  UtString indexedName = Verific2NucleusUtilities::getConvSubprogramName(&node);

  UtString pkg_numeric_bit("numeric_bit");
  UtString pkg_numeric_std("numeric_std");
  UtString pkg_std_logic_1164("std_logic_1164");
  UtString pkg_std_logic_arith("std_logic_arith");
  UtString pkg_numeric_signed("numeric_signed");
  UtString pkg_numeric_unsigned("numeric_unsigned");
  UtString pkg_std_logic_signed("std_logic_signed");
  UtString pkg_std_logic_unsigned("std_logic_unsigned");


  // first check for some known function calls from known packages, plain type conversions are handled near the end of this method.

  if ( isFunctionFromSpecificIEEEPackage(node,pkg_numeric_std,   UtString("to_01")) ||
       isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_1164,UtString("to_bitvector")) ||
       isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_1164,UtString("to_stdulogicvector")) ||
       isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_1164,UtString("to_stdlogicvector")) ||
       isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_1164,UtString("to_x01")) ||
       isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_1164,UtString("to_x01z")) ||
       isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_1164,UtString("to_ux01"))                ) {
      //check isPredefined3StateLogicOrBit
      // If the argument to one of these 'pass through' type conversions is
      // an expression, then we need to create a temporary, assign the
      // expression to it, and set gValue to the NUIdentRvalue for that temporary.
      NUExpr* expr = NULL;
      (void) populateFromArray(node.GetAssocList(), &expr, NULL);
      V2N_INFO_ASSERT(expr, node, "failed to populate argument 1");

      mCurrentScope->sValue(expr);
      if (expr && !expr->isWholeIdentifier()) {
        NUIdentRvalue* rval = assignToTempVector(expr, indexedName.c_str(), src_loc);
        mCurrentScope->sValue(rval);
      }
  } else if (  isFunctionFromSpecificIEEEPackage(node,pkg_numeric_std,     UtString("to_integer"))            || // 1 arg
               isFunctionFromSpecificIEEEPackage(node,pkg_numeric_std,     UtString("to_unsigned"))           || // 2 arg
               isFunctionFromSpecificIEEEPackage(node,pkg_numeric_std,     UtString("to_signed"))             || // 2 arg
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_arith,    UtString("conv_integer"))          || // 1 arg , yes conv_integer is in: std_logic_arith, std_logic_signed, std_logic_unsigned
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_signed,   UtString("conv_integer"))          || // 1 arg
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_unsigned, UtString("conv_integer"))          || // 1 arg
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_arith, UtString("conv_unsigned"))         || // 2 arg
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_arith, UtString("conv_signed"))           || // 2 arg
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_arith, UtString("conv_std_logic_vector")) || // 2 arg
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_arith, UtString("ext"))                   || // 2 arg
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_arith, UtString("sxt"))                   || // 2 arg
               isFunctionFromSpecificIEEEPackage(node,pkg_numeric_std,     UtString("resize"))                ) {  // 2 arg

    bool needs_1_arg = ( ( UtString("to_integer")            == indexedName) ||
                         ( UtString("conv_integer")          == indexedName)   );
    
    // rjc TODO is_unsigned_result should really be is_signed_result with associated polarity change to simplify reading of code
    // rjc TODO the extending variable should also be considered for cleanup
      bool is_unsigned_result = false;
      NUExpr* arg1 = NULL;
      NUExpr* arg2 = NULL;
      (void) populateFromArray(node.GetAssocList(), &arg1, &arg2);
      V2N_INFO_ASSERT(arg1, node, "failed to populate argument 1");
      if ( not needs_1_arg ){
        V2N_INFO_ASSERT(arg2, node, "failed to populate argument 2");
      }

      bool extending = functionSignExtendsOperand(node, arg1->isSignedResult());

      // Get the size of the final expr and extend it with signed bit if the
      // function corresponds to the signed extension function.
      if (arg2) {
          NUConst* constSize = arg2->castConst();
          UInt32 sizeValue = 0;
          if (constSize) {
              constSize->getUL(&sizeValue);
              if (0 == sizeValue) {
                  mNucleusBuilder->getMessageContext()->Verific2NUWidthZero(&src_loc);
                  mCurrentScope->sValue(NULL);
                  setError(true);
                  return false;
              }
          } else {
              // Add support for ext variable length here
              // The implementation in the Interra flow is known to be incorrect because it uses the incorrect range to calculate 
              // the size used for creating what is called the masSelectRange below. 
              // test/vhdl/lang_misc/issue39a.vhd contains example where Interra flow has simulation difference with MTI
              // because of incorrect size pickup. 
              // In the Verific flow here we use the mTargetConstraint, which also is not quite correct in cases
              // where an intermediate calculation needs to be done with more bits than are defined for mTargetConstraint. 
              // An example of such an assignment might be:
              // dout1_temp <= (ext(din9, (1+LHS_WIDTH-(x1+x2))) & ext(din5, (x2)) & ext(din2, x1))(JJ downto JJ-32);
              // dout1_temp is defined 32 bits wide and so calculation for first 'ext' needs to be done on 33 bits where as target
              // constraint will tell as incorrect number. And so this is a TODO for current Verific implementation.
              // An workaround of this for customers might be using temporary net to define ext expression, in that case temp net 
              // size will be 33 bits and there will be no size issues on expression.

              // Get max select range from target constraint
              // TODO: this value isn't quiet precise, for precise value we need todo deeper analysis on right hand side expression context
              VhdlValue* target_constraint_left = mTargetConstraint->Left();
              VhdlValue* target_constraint_right = mTargetConstraint->Right();
              INFO_ASSERT(target_constraint_left && target_constraint_right, "Unexpected NULL target constraint bounds");
              INFO_ASSERT(target_constraint_left->IsIntVal() && target_constraint_right->IsIntVal(), "Unexpected non-integer target constraint bounds");
              SInt32 maxSize_left = target_constraint_left->Integer();
              SInt32 maxSize_right = target_constraint_right->Integer();
              // Bring range to normalize form
              ConstantRange maxSelectRange = (maxSize_left > maxSize_right) ? ConstantRange(maxSize_left, maxSize_right) : ConstantRange(maxSize_right, maxSize_left);
              // Populate downTo operator with varsel object
              // Resynthesis phase tranforms downTo/Varsel combination to form: 
              // (expr >> rhs) & ((maxSelectSize'h01 << lhs) - maxSelectSize'h01), 
              // which actually just trancates (if extend size is less than expression actual size) or extends from left 
              // (no value change in this case) by 0's
              // where: 
              // 'rhs' = 0 in our case
              // 'lhs' = arg2 - 1
              // 'maxSelectSize' = maxSelectRange.getLength()
              // 'expr' = arg1
              NUConst* const_zero = NUConst::createKnownIntConst(0, 32,  false, src_loc);
              NUConst* const_one = NUConst::createKnownIntConst(1, 32,  false, src_loc);
              NUExpr* msb = new NUBinaryOp(NUOp::eBiMinus, arg2, const_one, src_loc);
              NUExpr* sel_expr = new NUBinaryOp(NUOp::eBiDownTo, msb, const_zero, src_loc);
              NUExpr* varsel = new NUVarselRvalue(arg1, sel_expr, maxSelectRange, src_loc);
              mCurrentScope->sValue(varsel);
              return true;
          }
          if (0 == sizeValue) {
              sizeValue = arg1->determineBitSize();            // rjc thinks:  why is this last ditch grab of sizeValue needed?
          }
          if (0 == sizeValue) {
            mNucleusBuilder->getMessageContext()->Verific2NUErrorDuringPopulation(&src_loc, "unsupported size expression of 0 found (second operand)");
            mCurrentScope->sValue(NULL);
            setError(true);
            return false;
          }

          if (extending) {
              arg1->setSignedResult(true); // set type of arg to signed so sign extension will happen correctly below
          }

          int width1 = arg1->determineBitSize(); // keep the size of the original expression, may be needed below
          arg1 = createSizeExpr(arg1, arg2, src_loc, sizeValue, extending);
          V2N_INFO_ASSERT(arg1, node, "failed to create size expression for type convertion");
          if ( extending ) {
              if (  UtString("resize") == indexedName) {
                  arg1 = createExprForResizeFunc(arg1, width1, src_loc, sizeValue);
              } 
              if (  UtString("conv_std_logic_vector") == indexedName) {
                  arg1->setSignedResult(false);
                  is_unsigned_result = true;
              } else {
                  arg1->setSignedResult(true);
              }
          } else {
              if (  UtString("conv_signed") == indexedName ||
                    UtString("to_signed")   == indexedName   ) {
                  is_unsigned_result = false;
                  arg1->resize(sizeValue);
                  arg1->setSignedResult(true);
              } else if ( (  UtString("conv_std_logic_vector") == indexedName) ||
                          (          UtString("conv_unsigned") == indexedName) ||
                          (            UtString("to_unsigned") == indexedName)   ){
                  arg1->setSignedResult(false);
                  is_unsigned_result = true;
              } else {
                // rjc thinks?: why is this the default, this looks wrong
                  arg1->setSignedResult(true);
              }
          }
      } else {
        // there is no arg2 so this must be a call with a single operand
          if ( not extending ) {
              is_unsigned_result = true;
          }
      }

      if (  UtString("to_integer")   == indexedName ||
            UtString("conv_integer") == indexedName   ) {
          if (! is_unsigned_result) {
              arg1->setSignedResult(true);
          }
          if (32 > arg1->determineBitSize()) {
//              arg1->setSignedResult(true);
              arg1 = buildMask(arg1, 32, !is_unsigned_result);
              V2N_INFO_ASSERT(arg1, node, "Failed to build mask");
              arg1->setSignedResult(true);
          } else  {
            arg1->setSignedResult(not is_unsigned_result);
          }
      }
      if ( arg1->isConstant() ) {
        mCurrentScope->sValue(arg1);
      } else {
        NUIdentRvalue* rvalue = assignToTempVector(arg1, indexedName.c_str(), src_loc);
        mCurrentScope->sValue(rvalue);
      }
  } else if (  isFunctionFromSpecificIEEEPackage(node,pkg_numeric_std,     UtString("shift_left")) ||
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_arith, UtString("shl"))        ||
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_signed, UtString("shl"))       ||
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_unsigned, UtString("shl"))       ) {

      NUExpr* arg1 = NULL;
      NUExpr* arg2 = NULL;
      (void) populateFromArray(node.GetAssocList(), &arg1, &arg2);
      V2N_INFO_ASSERT(arg1, node, "failed to populate argument 1");
      V2N_INFO_ASSERT(arg2, node, "failed to populate argument 2");



      NUExpr* shl = new NUBinaryOp(NUOp::eBiVhLshift, arg1, arg2, src_loc);
      V2N_INFO_ASSERT(shl, node, "invalid left shift operator");
      NUScope* declaration_scope = getDeclarationScope();//get neareast nameddeclaration scope
      VERIFIC_CONSISTENCY(declaration_scope, node, "invalid owner scope for shift operator");
      UInt32 retSize = shl->getBitSize();
      UInt32 detSize = shl->determineBitSize();
      UInt32 tempSize = retSize > detSize ? retSize : detSize;
      shl->resize(tempSize);
      StringAtom* tempName = declaration_scope->gensym("retval", indexedName.c_str(), NULL, false);
      V2N_INFO_ASSERT(tempName, node, "Failed to generate unique name for temporary net");
      NUNet* temp = 0;
      if (1 == tempSize) {
          temp = declaration_scope->createTempNet(tempName, tempSize, shl->isSignedResult(), src_loc);
      } else {
          temp = declaration_scope->createTempVectorNet(tempName, ConstantRange(tempSize - 1, 0),
                                            shl->isSignedResult(), src_loc);
      }
      V2N_INFO_ASSERT(temp, node, "failed to create temporary net");
      NUIdentLvalue* lvalue = new NUIdentLvalue(temp, src_loc);
      // TODO this bit of code is very similar to assignToTempVector consider refactor
      bool success = createAssignInCurrentContext(lvalue, shl, src_loc);
      if ( ! success ) {
        mCurrentScope->sValue(NULL);
        return false;
      }

      //create ident rvalue and return
      NUIdentRvalue* rvalue = new NUIdentRvalue(temp, src_loc);
      mCurrentScope->sValue(rvalue);
  } else if (  isFunctionFromSpecificIEEEPackage(node,pkg_numeric_std,     UtString("shift_right")) ||
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_arith, UtString("shr"))         || 
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_signed, UtString("shr"))         || 
               isFunctionFromSpecificIEEEPackage(node,pkg_std_logic_unsigned, UtString("shr"))) {
      NUExpr* arg1 = NULL;
      NUExpr* arg2 = NULL;
      (void) populateFromArray(node.GetAssocList(), &arg1, &arg2);
      V2N_INFO_ASSERT(arg1, node, "failed to populate argument 1");
      V2N_INFO_ASSERT(arg2, node, "failed to populate argument 2");

      bool isSigned = arg1->isSignedResult();

      NUExpr* rsh = new NUBinaryOp((isSigned ? NUOp::eBiVhRshiftArith : NUOp::eBiVhRshift), arg1, arg2, src_loc);
      NUScope* declaration_scope = getDeclarationScope();//get nearest nameddeclaration scope
      V2N_INFO_ASSERT(declaration_scope, node, "invalid owner scope");
      UInt32 retSize = rsh->getBitSize();
      UInt32 detSize = rsh->determineBitSize();
      UInt32 tempSize = std::max(retSize, detSize);
      rsh->resize(tempSize);
      StringAtom* tempName = declaration_scope->gensym("retval", indexedName.c_str(), NULL, false);
      V2N_INFO_ASSERT(tempName, node, "Failed to generate unique name for temp net");
      NUNet* temp = 0;
      if (1 == tempSize) {
          temp = declaration_scope->createTempNet(tempName, tempSize, rsh->isSignedResult(), src_loc);
      } else {
          temp = declaration_scope->createTempVectorNet(tempName, ConstantRange(tempSize - 1, 0),
                                                        rsh->isSignedResult(), src_loc);
      }
      NUIdentLvalue* lvalue = new NUIdentLvalue(temp, src_loc);
      // TODO this bit of code is very similar to assignToTempVector consider refactor
      bool success = createAssignInCurrentContext(lvalue, rsh, src_loc);
      if ( ! success ) {
        mCurrentScope->sValue(NULL);
        return false;
      }

      //create ident rvalue and return
      NUIdentRvalue* rvalue = new NUIdentRvalue(temp, src_loc);
      mCurrentScope->sValue(rvalue);

  } else if (  isFunctionFromSpecificIEEEPackage(node,pkg_numeric_std,     UtString("rotate_left")) ||
               isFunctionFromSpecificIEEEPackage(node,pkg_numeric_std,     UtString("rotate_right"))  ) {
    // currently unsupported, but is this really a type conversion function 
    UtString msg("function: ");
    msg << indexedName;
    mNucleusBuilder->getMessageContext()->UnsupportedLanguageConstruct(&src_loc, msg.c_str());
    mCurrentScope->sValue(NULL);
    return false;
  } else if (node.IsTypeConversion()) {
    // this section is for type conversions:  ie type_mark()
    // so there is no package that is to be checked.
    // Most type conversions just map the bits of one to another type and so there is nothing special that needs to be done here -- just
    // populate the argument of the typeConversion and use it. The one special case is if the type conversion is the built-in 'signed', and
    // it does the normal bit conversion but also sets the result as signed.

    bool set_signed_result = (UtString("signed") == indexedName);

    NUExpr* arg1 = NULL;
    (void) populateFromArray(node.GetAssocList(), &arg1, NULL);
    if ( NULL ==  arg1 ) {
      setError(true);
      UtString msg("the typeconversion: ");
      msg << indexedName;
      mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&src_loc, msg.c_str());
      mCurrentScope->sValue(NULL);
      return false;
    }
    if ( set_signed_result ) {
      arg1->setSignedResult(true);
    }
    mCurrentScope->sValue(arg1);
  } else {
      mCurrentScope->sValue(NULL); // to be sure we don't leave anything in here.
      return false;
  }
  return true;
}

bool VerificVhdlDesignWalker::functionSignExtendsOperand(const VhdlIndexedName& node, bool is_signed) const
{
  // cloutier thinks: the logic in this method was adapted from
  // VhdlPopulate::functionSignExtendsOperand (const char *package, const char *func_name, vhExpr vh_arg)
  // with the assumption that it was correct in the interra flow.

    VhdlIdDef* idDef = Verific2NucleusUtilities::getConvSubprogramPrefix(node);
    UtString funcName = Verific2NucleusUtilities::getConvSubprogramName(&node);
    funcName.lowercase();
    
    if ( (UtString("sxt")        == funcName) ){
      return true;              // sign extend (sxt) always sign extends
    }


    // Complex rules here
    // Package       	 Argument         Extends
    // ===============   =============       ==========
    // STD_LOGIC_ARITH    SIGNED           yes
    //       "            UNSIGNED         no
    //       "            INTEGER          yes (but tautology)
    //       "            STD_ULOGIC       no
    // STD_LOGIC_UNSIGNED STD_LOGIC_VECTOR no
    // STD_LOGIC_SIGNED   STD_LOGIC_VECTOR yes
    //

    if (UtString("conv_integer") == funcName) {
      if ( Verific2NucleusUtilities::isIeeeDecl(idDef) ) {
        UtString pkg(idDef->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit()->Name());
        pkg.lowercase();
        if ( (UtString("std_logic_arith") == pkg) && is_signed ) {
          return true;
        } else if ( UtString("std_logic_signed") == pkg ){
          return true;
        }
      }
    }

    // some cases are sensitive to the input argument's signedness
    if (is_signed &&
        ((UtString("resize")        == funcName) ||
         (UtString("to_integer")    == funcName) ||
         (UtString("conv_signed")   == funcName))) {
        return true;
    }
    return false;   
}

// eBiVhExt is requires special consideration because the self-determined
// size is based on the *value* of its second arg.  So we need to fold
// it to do arithmetic simplification.  If we don't get a constant out,
// and size is not known from context, then we can't populate it.
// this is a copy of the interra flow version of this function, the interra flow version will be deleted 
// this uses a non-static call to fold
NUExpr* VerificVhdlDesignWalker::createSizeExpr(NUExpr* val, NUExpr* sz, const SourceLocator& loc, UInt32 lhsWidth, bool sign)
{
    sz = sz->makeSizeExplicit(32);
    
    // The size expression should always be unsigned.
    sz->setSignedResult(false);
    sz = getNucleusBuilder()->getFold()->fold(sz);
    if (lhsWidth == 0) {
        NUConst* k = sz->castConst();
        if ((k == NULL) || !k->getUL(&lhsWidth)) {
            mNucleusBuilder->getMessageContext()->VhdlNoStaticSize(&loc);
            return NULL;
        }
    }
    NUExpr* ret = new NUBinaryOp(sign ? NUOp::eBiVhExt : NUOp::eBiVhZxt, val, sz, loc);
    ret->resize(lhsWidth);
    ret->setSignedResult(sign);
    ret = getNucleusBuilder()->getFold()->fold(ret);
    return ret;
}

// Returns true if node is a slice of a slice (currently unsupported)
bool VerificVhdlDesignWalker::isSliceOfSlice(VhdlIndexedName& node)
{
  bool sliceOfSlice = false;
  if (node.IsArraySlice()) {
    if (ID_VHDLINDEXEDNAME == node.GetPrefix()->GetClassId()) {
      VhdlName* idName = static_cast<VhdlIndexedName*>(node.GetPrefix());
      V2N_INFO_ASSERT(idName, node, "invalid node");
      if (idName->IsArraySlice()) {
        sliceOfSlice = true;
      }
    }
  }
  
  return sliceOfSlice;
}

//generate nucleus object for array slicing
//this method called from VHDL_VISIT(VhdlIndexedName, node)
void VerificVhdlDesignWalker::arraySlice(VhdlIndexedName& node)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "ArraySlice" << std::endl;
#endif
    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    // Slices of slices are not supported
    if (isSliceOfSlice(node)) {
      UtString msg(Verific2NucleusUtilities::verific2String(node));
                mNucleusBuilder->getMessageContext()->Verific2NUSliceOfSliceUnsupported(&loc, msg.c_str());
                mCurrentScope->sValue(0);
                setError(true);
                return ;
            }
    
    VhdlConstraint* prefix_constraint = 0;
    VhdlConstraint* allocated_prefix_constraint = 0;

    if (node.GetPrefixId()) {
        prefix_constraint = node.GetPrefixId()->Constraint();
    } else {
        allocated_prefix_constraint = (node.GetPrefix()) ? node.GetPrefix()->EvaluateConstraintInternal(mDataFlow, 0) : 0;
        prefix_constraint = allocated_prefix_constraint;
    }
    INFO_ASSERT(prefix_constraint, "invalid prefix constraint");

    // Get bounds of prefix. We need these, because the prefix bounds are lost if it evaluates to a constant.
    SInt32 prefix_left, prefix_right;
    Verific2NucleusUtilities::getPrefixBounds(prefix_constraint, &prefix_left, &prefix_right);
    ConstantRange prefix_range(prefix_left, prefix_right);
    
    VhdlConstraint* oldTargetConstraint = mTargetConstraint;
    mTargetConstraint = prefix_constraint;
    // FD thinks: this is weird. Why do we need todo something special for prefix (if we need why just not do it in current visitor where we know that it's an prefix) ? sPrefix/isPrefix and related to it fields are candidate for removal. 
    mCurrentScope->sPrefix(true);
    node.GetPrefix()->Accept(*this);
    mCurrentScope->sPrefix(false);
    NUBase* pBase = mCurrentScope->gValue();
    if (!pBase) {
        mNucleusBuilder->getMessageContext()->Verific2NUErrorDuringPopulation(&loc, "prefix of array slice could not be created");
        mCurrentScope->sValue(NULL);
        setError(true);
        return;
    }
    mTargetConstraint = oldTargetConstraint;
    delete allocated_prefix_constraint;

    VhdlDiscreteRange* assoc = 0;
    unsigned i = 0;
    FOREACH_ARRAY_ITEM(node.GetAssocList(), i, assoc) {
        if (!assoc) break;
        
        V2NDesignScope::ContextType cType = mCurrentScope->gContext();
        mCurrentScope->sContext(V2NDesignScope::RVALUE);
        assoc->Accept(*this);
        if (hasError()) {
            mCurrentScope->sValue(0);
            return ;
        }
        mCurrentScope->sContext(cType);
        UtVector<NUBase*> val;
        mCurrentScope->gValueVector(val);
        // The 'val' vector will contain NULL entries for values that are constant.
        // Constant values are returned in 'gRange' variable.
        V2N_INFO_ASSERT(2 == val.size(), node, "slice indexes require 2 values");
        NUBase* startIdx = val[0];
        NUBase* endIdx = val[1];
        UtPair<SInt32, SInt32> p = mCurrentScope->gRange();
        // Constant ends of the range will be in 'p'
        ConstantRange access_range(p.first, p.second);
        NUExpr* lhs = 0;
        NUExpr* rhs = 0;
        if (startIdx || endIdx) {
            // At least one end of the range is non-constant.
            if (!startIdx) {
                lhs = NUConst::createKnownIntConst(access_range.getMsb(), 32,  false, loc);
                lhs->setSignedResult(true);
            } else {
                lhs = (dynamic_cast<NUNet*>(startIdx)) ? mNucleusBuilder->createIdentRvalue(static_cast<NUNet*>(startIdx), loc) :
                    static_cast<NUExpr*>(startIdx);
            }
            if (! endIdx) {
                rhs = NUConst::createKnownIntConst(access_range.getLsb(), 32,  false, loc);
                rhs->setSignedResult(true);
            } else {
                rhs = (dynamic_cast<NUNet*>(endIdx)) ? mNucleusBuilder->createIdentRvalue(static_cast<NUNet*>(endIdx), loc) :
                    static_cast<NUExpr*>(endIdx);
            }
        }

        // Populate an array slice with constant or non-constant indices
        populateArraySlice(node, pBase, prefix_range, &access_range, lhs, rhs, loc);
    }
}

bool VerificVhdlDesignWalker::isNullRange(const ConstantRange* range, const ConstantRange* bound, const SourceLocator& loc, const char* netName)
{
  bool nullRange = false;
  
  // Most cases in which the direction is reversed are flagged as errors by the Verific
  // parser, and we will never get this far. However, the case (1 downto 4) is only a Verific
  // warning, and we need this code to flag it as an error.
  if (range->getMsb() > range->getLsb() && bound->getMsb() < bound->getLsb() ||
      range->getMsb() < range->getLsb() && bound->getMsb() > bound->getLsb()) {
    //directions are different return NULL (to match interra flow)
    UtString msg;
    msg << "[" << range->getMsb() << ":" << range->getLsb() << "]";
    mNucleusBuilder->getMessageContext()->Verific2NUNullRangeDetected(&loc, netName, msg.c_str());
    nullRange = true;
  }

  return nullRange;
}

// Common error message used by populateArraySlice
void VerificVhdlDesignWalker::unsupportedVariableCompositeSlice(const VhdlIndexedName& node, const SourceLocator& loc)
{
   UtString msg("variable slice of a field in a record array: ");
   msg << Verific2NucleusUtilities::verific2String(node);
   mNucleusBuilder->getMessageContext()->UnsupportedLanguageConstruct(&loc, msg.c_str());
   setError(true);
}

// Common error message used by populateArraySlice
void VerificVhdlDesignWalker::unsupportedVariableWidthRecordSlice(const VhdlIndexedName& node, const SourceLocator& loc)
{
   UtString msg(Verific2NucleusUtilities::verific2String(node));
   getMessageContext()->VariableWidthRecordSlice(&loc, msg.c_str());
   setError(true);
}

// Populate constant or variable slice of NUIdentLvalue vector
NULvalue* VerificVhdlDesignWalker::populateIdentLvalueVectorSlice(NUIdentLvalue* identLval, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  bool isConstRange = !(range_lhs || range_rhs);
  NUNet* identNet = identLval->getIdent();
  NUVectorNet* vNet = identNet->castVectorNet();
  INFO_ASSERT(vNet, "Unexpected non-vector net passed to populateIdentLvalueVectorSlice");

  NULvalue* varsel = NULL;
  if (isConstRange) {
    const ConstantRange* bound = vNet->getDeclaredRange();
    normalizeRange(access_range, *bound, vNet->getName()->str(), loc);
    varsel = new NUVarselLvalue(identLval, *access_range, identLval->getLoc());
  } else {
    // Range has at least one variable bound
    range_lhs = vNet->normalizeSelectExpr(range_lhs, 0, true);
    range_rhs = vNet->normalizeSelectExpr(range_rhs, 0, true);
    // FD thinks : in resynthesize phase this gets tranformed to the form: (vNet >> rhs) & ((netSize'h01 << lhs) - netSize'h01) expression
    NUExpr* sel_expr = new NUBinaryOp(NUOp::eBiDownTo, range_lhs, range_rhs, identLval->getLoc());
    varsel = new NUVarselLvalue(identLval, sel_expr, *vNet->getRange(), identLval->getLoc());
  }

  return varsel;
}

// Populate constant or variable slice of a NUIdentLvalue memory
NUConcatLvalue* VerificVhdlDesignWalker::populateIdentLvalueMemorySlice(const VhdlIndexedName& node, NUIdentLvalue* identLval, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  bool isConstRange = !(range_lhs || range_rhs);
  NUNet* identNet = identLval->getIdent();
  NUMemoryNet* mNet = identNet->getMemoryNet();
  INFO_ASSERT(mNet, "Unexpected non-memory net passed to populateIdentLvalueMemorySlice");

  NUConcatLvalue* cLval = NULL;
  if (isConstRange) {
    // Slice of memory net on LHS -- create a concat of the MemselLvalues in the order defined by range
    const ConstantRange bounds = mNet->getOperationalRange(1); // using index 1 because we are in a NUMemoryNet, if there were more
    // dimensions then this would be a NUCompositeNet
    UInt32 length = access_range->getLength();
    SInt32 lhs = access_range->getMsb();
    SInt32 rhs = access_range->getLsb();
    NULvalueVector vLval;
    for (UInt32 i = 0; i < length; ++i) {
      NUExpr* idxExpr = NUConst::createKnownIntConst(lhs < rhs ? lhs + i : lhs - i, 32, false, identNet->getLoc());
      idxExpr->setSignedResult(true);
      NUMemselLvalue* memlval = new NUMemselLvalue(mNet, idxExpr, identNet->getLoc());
      vLval.push_back(memlval);
    }
    delete identLval;
    cLval = new NUConcatLvalue(vLval, identNet->getLoc());
  } else {
    // variable slices of multidimensional objects are not supported.
    UtString msg(Verific2NucleusUtilities::verific2String(node));
    getMessageContext()->VariableWidthMemory(&loc, msg.c_str());
    setError(true);
  }

  return cLval;
}

// Populate constant or variable slice of NUIdentRvalue vector
NUExpr* VerificVhdlDesignWalker::populateIdentRvalueVectorSlice(NUIdentRvalue* identRval, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  bool isConstRange = !(range_lhs || range_rhs);
  NUNet* identNet = identRval->getIdent();
  NUVectorNet* vNet = identNet->castVectorNet();
  INFO_ASSERT(vNet, "Unexpected non-vector net passed to populateIdentLvalueVectorSlice");

  NUExpr* varsel = NULL;

  if (isConstRange) {
    const ConstantRange* bound = vNet->getDeclaredRange();
    normalizeRange(access_range, *bound, vNet->getName()->str(), loc);
    varsel = new NUVarselRvalue(identRval, *access_range, identRval->getLoc());
  } else {
    // Variable range
    range_lhs = vNet->normalizeSelectExpr(range_lhs, 0, true);
    range_rhs = vNet->normalizeSelectExpr(range_rhs, 0, true);
    NUExpr* sel_expr = new NUBinaryOp(NUOp::eBiDownTo, range_lhs, range_rhs, identRval->getLoc());
    varsel = new NUVarselRvalue(identRval, sel_expr, *vNet->getRange(), identRval->getLoc());
  }
  
  bool isSigned = identRval->isSignedResult();
  if ( isSigned ){
    varsel->setSignedResult(true); // VHDL-1076-2002 LRM section 6.5, the type of the slice is the same as the type of what is being sliced. So signedness must match
  }

  return varsel;
}

// Populate constant or variable slice of a NUIdentRvalue memory
NUConcatOp* VerificVhdlDesignWalker::populateIdentRvalueMemorySlice(const VhdlIndexedName& node, NUIdentRvalue* identRval, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  bool isConstRange = !(range_lhs || range_rhs);
  NUNet* identNet = identRval->getIdent();
  NUMemoryNet* mNet = identNet->getMemoryNet();
  INFO_ASSERT(mNet, "Unexpected non-memory net passed to populateIdentRvalueMemorySlice");

  // Slice of memory net on RHS -- create a concat of the MemselRvalues in the order defined by range
  NUConcatOp* cval = NULL;
  if (isConstRange) {
    const ConstantRange bounds = mNet->getOperationalRange(1); // using index 1 because we are in a NUMemoryNet, if there were more
    // dimensions then this would be a NUCompositeNet
    UInt32 length = access_range->getLength();
    SInt32 lhs = access_range->getMsb();
    SInt32 rhs = access_range->getLsb();
    NUExprVector vRval;
    for (UInt32 i = 0; i < length; ++i) {
      NUExpr* idxExpr = NUConst::createKnownIntConst(lhs < rhs ? lhs + i : lhs - i, 32, false, identNet->getLoc());
      idxExpr->setSignedResult(true);
      NUMemselRvalue* memrval = new NUMemselRvalue(mNet, idxExpr, identNet->getLoc());
      vRval.push_back(memrval);
    }
    delete identRval;
    cval = new NUConcatOp(vRval, 1, identNet->getLoc());
  } else {
    // variable slices of multidimensional objects are not supported.
    UtString msg(Verific2NucleusUtilities::verific2String(node));
    getMessageContext()->VariableWidthMemory(&loc, msg.c_str());
    setError(true);
  }
  
  return cval;
}

// Populate constant or variable select slice of a memsel lvalue
NULvalue* VerificVhdlDesignWalker::populateMemselLvalueSlice(NUMemselLvalue* memselLval, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
    bool isConstRange = !(range_lhs || range_rhs);
    NUNet* net = memselLval->getIdent();
    NUMemoryNet* mNet = net->getMemoryNet();
    INFO_ASSERT(mNet, "Unexpected non-memory passed to populateMemselLvalueSlice");

    NULvalue* varsel = NULL;
    if (isConstRange) {
      const ConstantRange* bounds = mNet->getRange(0);
      normalizeRange(access_range, *bounds, net->getName()->str(), loc);
      varsel = new NUVarselLvalue(memselLval, *access_range, memselLval->getLoc());
      // FD thinks: possible optimization is to cast to NUCompositeInterfaceExpr and NUCompositeInterfaceLvalue. The thing which confused 
      // me a bit is the fact that both of the mentioned types have opposite type (expr or lvalue) as well listed as a possible 
      // subtype of it (so each of interface classes has 6 types).
    } else {
      // variable range
      // For now, we're supporting part selects of vectors. 
      // So this only operates on the packed range of an indexed memory.
      // Memory range selects are left TODO. Caught by INFO_ASSERTS.
      UInt32 dim = mNet->getFirstUnpackedDimensionIndex();
      INFO_ASSERT(dim > 0, "unexpected slice of unpacked array");
      UInt32 nMemDims = mNet->getNumDims();
      UInt32 nIndexDims = memselLval->getNumDims();
      INFO_ASSERT(nMemDims == (nIndexDims + 1), "Unexpected memory slice");
      range_lhs = mNet->normalizeSelectExpr(range_lhs, 0, 0, true);
      range_rhs = mNet->normalizeSelectExpr(range_rhs, 0, 0, true);
      NUExpr* sel_expr = new NUBinaryOp(NUOp::eBiDownTo, range_lhs, range_rhs, memselLval->getLoc());
      sel_expr->resize(32);
      ConstantRange netRange = *mNet->getWidthRange();
      varsel = new NUVarselLvalue(memselLval, sel_expr, netRange, memselLval->getLoc());
    }

    return varsel;
}

// Populate constant or variable select slice of a memsel rvalue
NUExpr* VerificVhdlDesignWalker::populateMemselRvalueSlice(NUMemselRvalue* memselRval, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  bool isConstRange = !(range_lhs || range_rhs);
  NUNet* net = memselRval->getIdent();
  NUMemoryNet* mNet = net->getMemoryNet();
  INFO_ASSERT(mNet, "Unexpected non-memory passed to populateMemseRLvalueSlice");

  NUExpr* varsel = NULL;
  if (isConstRange) {
    const ConstantRange* bounds = mNet->getRange(0);
    normalizeRange(access_range, *bounds, net->getName()->str(), loc);
    varsel = new NUVarselRvalue(memselRval, *access_range, memselRval->getLoc());
  } else {
    // Variable range
    // For now, we're supporting part selects of vectors. 
    // So this only operates on the packed range of an indexed memory.
    // Memory range selects are left TODO. 
    UInt32 dim = mNet->getFirstUnpackedDimensionIndex();
    INFO_ASSERT(dim > 0, "unexpected slice of unpacked array");
    UInt32 nMemDims = mNet->getNumDims();
    UInt32 nIndexDims = memselRval->getNumDims();
    INFO_ASSERT(nMemDims == (nIndexDims + 1), "unexpected memory slice");
    range_lhs = mNet->normalizeSelectExpr(range_lhs, 0, 0, true);
    range_rhs = mNet->normalizeSelectExpr(range_rhs, 0, 0, true);
    NUExpr* sel_expr = new NUBinaryOp(NUOp::eBiDownTo, range_lhs, range_rhs, memselRval->getLoc());
    sel_expr->resize(32);
    ConstantRange netRange = *mNet->getWidthRange();
    varsel = new NUVarselRvalue(memselRval, sel_expr, netRange, memselRval->getLoc());
  }

  return varsel;
}

// Populate constant or variable select slice of a NUConst
NUVarselRvalue* VerificVhdlDesignWalker::populateConstantSlice(const VhdlIndexedName& node, NUConst* constVal, const ConstantRange& prefix_range, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  NUVarselRvalue* varsel = NULL;
  bool isConstRange = !(range_lhs || range_rhs);
  if (isConstRange) {
    normalizeRange(access_range, prefix_range, Verific2NucleusUtilities::verific2String(node), loc);
    varsel = new NUVarselRvalue(constVal, *access_range, constVal->getLoc());
  } else {
    UtString msg("variable slice of a constant: ");
    msg << Verific2NucleusUtilities::verific2String(node);
    mNucleusBuilder->getMessageContext()->UnsupportedLanguageConstruct(&loc, msg.c_str());
    setError(true);
  }
  
  return varsel;
}

// Populate constant or variable select slice of a NUCompositeSelExpr
NUCompositeSelExpr* VerificVhdlDesignWalker::populateCompositeSelExprSlice(const VhdlIndexedName& node, NUCompositeSelExpr* comSelExpr, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  bool isConstRange = !(range_lhs || range_rhs);
  if (isConstRange) {
    // If it's already NUCompositeSelExpr then do extend it
    UInt32 lWidth = Log2 (access_range->getLength()) + 1;
    NUConst* the_const = NUConst::createKnownIntConst(0, lWidth,  false, comSelExpr->getLoc());
    comSelExpr->addIndexExpr(the_const);
    // TODO: check with Interra whether range normalization needed to be done (or index expression normalization)
    comSelExpr->setPartSelRange(*access_range);
    comSelExpr->setPartSelect();
  } else {
    // Interra can compile this case without error (but simulation seems to be wrong).
    // For now, we make this an error
    unsupportedVariableCompositeSlice(node, loc);
    comSelExpr = NULL;
    }

  return comSelExpr;
}

NUCompositeSelExpr* VerificVhdlDesignWalker::populateCompositeFieldExprSlice(const VhdlIndexedName& node, NUCompositeFieldExpr* comFieldExpr, const ConstantRange& prefix_range, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  bool isConstRange = !(range_lhs || range_rhs);
  NUCompositeSelExpr* comSelExpr = NULL;
  if (isConstRange) {
    // Build NUCompositeSelExpr around prefix 
    // FD thinks: is comFieldExpr->getLoc() right location for select expression? 
    // Don't we need to pass selected place location?
    // Normalize range based on prefix bounds. 
    normalizeRange(access_range, prefix_range, "which is constant", loc); // seems like we only want to normalize if this range is for a vector, and not normalize for an array index
    UInt32 lWidth = Log2 (access_range->getLength()) + 1;
    NUConst* the_const = NUConst::createKnownIntConst(0, lWidth,  false, comFieldExpr->getLoc());
    NUExprVector exprVec;
    exprVec.push_back(the_const);
    comSelExpr = new NUCompositeSelExpr(comFieldExpr, &exprVec, *access_range, isConstRange, comFieldExpr->getLoc());
  } else {
    // Interra can compile this (variable slice) case without error (but simulation seems to be wrong)
    // For now, we make this an error
    unsupportedVariableCompositeSlice(node, loc);
  }

  return comSelExpr;
}

NUCompositeSelExpr* VerificVhdlDesignWalker::populateCompositeIdentRvalueSlice(const VhdlIndexedName& node, NUCompositeIdentRvalue* comIdentRval, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  bool isConstRange = !(range_lhs || range_rhs);
  NUCompositeSelExpr* comSelExpr = NULL;
  if (isConstRange) {
    // Build NUCompositeSelExpr around it
    // Calculate the index expression to a precision sufficient to address the
    // entire object (copied from localflow/VhPopulateExpr.cxx:7623)
    UInt32 lWidth = Log2 (access_range->getLength()) + 1;
    NUConst* the_const = NUConst::createKnownIntConst(0, lWidth,  false, comIdentRval->getLoc());
    NUExprVector exprVec;
    exprVec.push_back(the_const);
    comSelExpr = new NUCompositeSelExpr(comIdentRval, &exprVec, *access_range, isConstRange, comIdentRval->getLoc());
  } else {
    // Interra gives Error 3572: Variable indexed/width slice of a record array object 'RECB((X+ 3) downto (X+ 2))' is currently unsupported.
    unsupportedVariableWidthRecordSlice(node, loc);
  }

  return comSelExpr;
}

NUCompositeSelLvalue* VerificVhdlDesignWalker::populateCompositeSelLvalueSlice(const VhdlIndexedName& node, NUCompositeSelLvalue* comSelLvalue, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  bool isConstRange = !(range_lhs || range_rhs);
  if (isConstRange) {
    UInt32 lWidth = Log2 (access_range->getLength()) + 1;
    NUConst* the_const = NUConst::createKnownIntConst(0, lWidth,  false, comSelLvalue->getLoc());
    comSelLvalue->addIndexExpr(the_const);
    comSelLvalue->setPartSelRange(*access_range);
  } else {
    // Interra fails with ERROR, but no error message 
    unsupportedVariableCompositeSlice(node, loc);
    comSelLvalue = NULL;
  }

  return comSelLvalue;
}

NUCompositeSelLvalue* VerificVhdlDesignWalker::populateCompositeFieldLvalueSlice(const VhdlIndexedName& node, NUCompositeFieldLvalue* comFieldLvalue, const ConstantRange& prefix_range, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  bool isConstRange = !(range_lhs || range_rhs);
  NUCompositeSelLvalue* comSelLvalue = NULL;
  if (isConstRange) {
    // Build NUCompositeSelLvalue around it
    //FD thinks: is comFieldLvalue->getLoc() right location for select expression ?
    // Normalize range based on prefix bounds. 
    normalizeRange(access_range, prefix_range, "which is constant", loc); // seems like we only want to normalize if this range is for a vector, and not normalize for an array index
    UInt32 lWidth = Log2 (access_range->getLength()) + 1;
    NUConst* the_const = NUConst::createKnownIntConst(0, lWidth,  false, comFieldLvalue->getLoc());
    NUExprVector exprVec;
    exprVec.push_back(the_const);
    comSelLvalue = new NUCompositeSelLvalue(comFieldLvalue, &exprVec, *access_range, isConstRange, comFieldLvalue->getLoc());
  } else {
    // Interra fails with ERROR, but no error message 
    unsupportedVariableCompositeSlice(node, loc);
  }

  return comSelLvalue;
}

NUCompositeSelLvalue* VerificVhdlDesignWalker::populateCompositeIdentLvalueSlice(const VhdlIndexedName& node, NUCompositeIdentLvalue* comIdentLval, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  bool isConstRange = !(range_lhs || range_rhs);
  NUCompositeSelLvalue* comSelLvalue = NULL;
  if (isConstRange) {
    // Build NUCompositeSelLvalue around it
    UInt32 lWidth = Log2 (access_range->getLength()) + 1;
    NUConst* the_const = NUConst::createKnownIntConst(0, lWidth, false, comIdentLval->getLoc());
    NUExprVector exprVec;
    exprVec.push_back(the_const);
    comSelLvalue = new NUCompositeSelLvalue(comIdentLval, &exprVec, *access_range, isConstRange, comIdentLval->getLoc());
  } else {
    // Interra gives Error 3572: Variable indexed/width slice of a record array object 'RECB((X+ 3) downto (X+ 2))' is currently unsupported.
    unsupportedVariableWidthRecordSlice(node, loc);
    }

  return comSelLvalue;
}

// Helper for 'populateArraySlice'; if it was not possible to create a 'value', free the prefix.
void VerificVhdlDesignWalker::setValueAndFreePrefixIfNull(NUBase* value, NUBase* prefix)
{
  mCurrentScope->sValue(value);
  if (!value)
    delete prefix;
}

// Populate constant or variable slice of an array
void VerificVhdlDesignWalker::populateArraySlice(const VhdlIndexedName& node, NUBase* prefix, const ConstantRange& prefix_range, ConstantRange* access_range, NUExpr* range_lhs, NUExpr* range_rhs, const SourceLocator& loc)
{
  if (NUIdentLvalue* identLval = dynamic_cast<NUIdentLvalue*>(prefix)) {
    NUNet* identNet = identLval->getIdent();
    if (identNet->isVectorNet()) {
      // Populate constant or variable slice vector net
      NULvalue* varsel = populateIdentLvalueVectorSlice(identLval, access_range, range_lhs, range_rhs, loc);
      setValueAndFreePrefixIfNull(varsel, prefix);
    } else if (identNet->isMemoryNet()) {
      // Populate constant or variable slice memory net
      NUConcatLvalue* cLval = populateIdentLvalueMemorySlice(node, identLval, access_range, range_lhs, range_rhs, loc);
      setValueAndFreePrefixIfNull(cLval, prefix);
    } else {
      // If it's a slice, an IdentLvalue, and there were no parse or elaboration errors, we should not get here. 
      INFO_ASSERT(false, "Unexpected NUIdentLvalue type in populateArraySlice");
    }

  } else if (NUIdentRvalue* identRval = dynamic_cast<NUIdentRvalue*>(prefix)) {
    NUNet* identNet = identRval->getIdent();
    if (identNet->isVectorNet()) {
      // Populate constant or variable slice vector rval
      NUExpr* varsel = populateIdentRvalueVectorSlice(identRval, access_range, range_lhs, range_rhs, loc);
      setValueAndFreePrefixIfNull(varsel, prefix);
    } else if (identNet->isMemoryNet()) {
      // Populate constant or variable slice memory rval
      NUConcatOp* cval = populateIdentRvalueMemorySlice(node, identRval, access_range, range_lhs, range_rhs, loc);
      setValueAndFreePrefixIfNull(cval, prefix);
    } else {
      // If it's a slice, an IdentRvalue, and there were no parse or elaboration errors, we should not get here. 
      INFO_ASSERT(false, "Unexpected NUIdentRvalue type in populateArraySlice");
    }

  } else if (NUMemselLvalue* memselLval = dynamic_cast<NUMemselLvalue*>(prefix)) {
    // Populate constant or variable slice of a memsel lvalue
    NULvalue* varsel = populateMemselLvalueSlice(memselLval, access_range, range_lhs, range_rhs, loc);
    setValueAndFreePrefixIfNull(varsel, prefix);

  } else if (NUMemselRvalue* memselRval = dynamic_cast<NUMemselRvalue*>(prefix)) {
    // Populate constant or variable slice of a memsel rvalue
    NUExpr* varsel = populateMemselRvalueSlice(memselRval, access_range, range_lhs, range_rhs, loc);
    setValueAndFreePrefixIfNull(varsel, prefix);

  } else if (NUConst* constVal = dynamic_cast<NUConst*>(prefix)) {
    NUVarselRvalue* varsel = populateConstantSlice(node, constVal, prefix_range, access_range, range_lhs, range_rhs, loc);
    setValueAndFreePrefixIfNull(varsel, prefix);

  } else if (NUCompositeSelExpr* comSelExpr = dynamic_cast<NUCompositeSelExpr*>(prefix)) {
    // Populate constant or variable slice of a NUCompositeSelExpr slice. Modifies comSelExpr
    comSelExpr = populateCompositeSelExprSlice(node, comSelExpr, access_range, range_lhs, range_rhs, loc);    
    setValueAndFreePrefixIfNull(comSelExpr, prefix);

  } else if (NUCompositeFieldExpr* comFieldExpr = dynamic_cast<NUCompositeFieldExpr*>(prefix)) {
    NUCompositeSelExpr* comSelExpr = populateCompositeFieldExprSlice(node, comFieldExpr, prefix_range, access_range, range_lhs, range_rhs, loc);
    setValueAndFreePrefixIfNull(comSelExpr, prefix);

  } else if (NUCompositeIdentRvalue* comIdentRval = dynamic_cast<NUCompositeIdentRvalue*>(prefix)) {
    NUCompositeSelExpr* comSelExpr = populateCompositeIdentRvalueSlice(node, comIdentRval, access_range, range_lhs, range_rhs, loc);
    setValueAndFreePrefixIfNull(comSelExpr, prefix);

  } else if (NUCompositeSelLvalue* comSelLvalue = dynamic_cast<NUCompositeSelLvalue*>(prefix)) {
    // Populate constant or variable slice of a NUCompositeSelLvalue slice. Modifies comSelLvalue.
    comSelLvalue = populateCompositeSelLvalueSlice(node, comSelLvalue, access_range, range_lhs, range_rhs, loc);
    setValueAndFreePrefixIfNull(comSelLvalue, prefix);

  } else if (NUCompositeFieldLvalue* comFieldLvalue = dynamic_cast<NUCompositeFieldLvalue*>(prefix)) {
    NUCompositeSelLvalue* comSelLvalue = populateCompositeFieldLvalueSlice(node, comFieldLvalue, prefix_range, access_range, range_lhs, range_rhs, loc);
    setValueAndFreePrefixIfNull(comSelLvalue, prefix);

  } else if (NUCompositeIdentLvalue* comIdentLval = dynamic_cast<NUCompositeIdentLvalue*>(prefix)) {
    NUCompositeSelLvalue* comSelLvalue = populateCompositeIdentLvalueSlice(node, comIdentLval, access_range, range_lhs, range_rhs, loc);
    setValueAndFreePrefixIfNull(comSelLvalue, prefix);

  } else {
    UtString msg("populateArraySlice for type: ");
    msg << prefix->typeStr();
    mNucleusBuilder->getMessageContext()->UnsupportedLanguageConstruct(&loc, msg.c_str());
    setError(true);
    setValueAndFreePrefixIfNull(NULL, prefix);
  } 
}

//convert character to binary value and write to string
void VerificVhdlDesignWalker::composeBinaryFromChar(UInt8 char_val, UtString *bin_str)
{
  bin_str->clear();
  UInt32 tempc = char_val;

  char result[16];

  CarbonValRW::writeBinValToStr(result, 16, &tempc, 8, false);

  *bin_str = result;
}

// This method helps in converting the char string to binary representation.
// The binary string representation is used to help store filename char*, 
// mode char* etc using NUConst() for nucleus SysTask classes.
// For ex. the character 'A' is converted to a binary string "01000001"
//         ie. 65 in binary. Since ASCII 'A' has a decimal value 65.
// NUConst() has a flag to remember that the binary string it contains was 
// actually a char string. It helps in decomposing the string when required.
// replace_null_with_zero: if true then an empty input string will
//         result in a bin_str that contains the representation of \0,
//         which will look like a null string at runtime. (default is false)
int 
VerificVhdlDesignWalker::composeBinStringFromString(const char* str, UtString* bin_str, bool replace_null_with_zero)
{
  UInt32 len =  strlen(str);
  UtString  temp;
  for (int i = 0; i <= (int)len - 1; i++) {
    composeBinaryFromChar((int)str[i], &temp);
    bin_str->append(temp.c_str());
  }
  if ( replace_null_with_zero  && ( bin_str->length() == 0 ) ) {
    // An empty input string would result in a bin_str of size zero.
    // If that string is used by  emitUtil::emitConstant, then we
    // will INFO_ASSERT (see bug7535). Here we force a zero onto the string
    // which gives us a usable constant that acts as an empty
    // string at runtime.
    bin_str->append("00000000");
  }
  return bin_str->length();
}    

bool VerificVhdlDesignWalker::arrayIndexPrefixIdentRvalue(NUIdentRvalue* irval, NUExprVector* idxExprs, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << "ArrayIndex prefix IdentRvalue" << std::endl;
#endif
  // TODO: function doesn't handle composite net type appearing inside identRvalue expression 
  NUNet* identNet = irval->getIdent();
  if (identNet->isMemoryNet()) {
      NUMemoryNet* memnet = static_cast<NUMemoryNet*>(identNet);
      INFO_ASSERT(memnet , "invalid memory net");
      NUExpr* memrval = 0;
      if (2 == memnet->getDeclaredDimensions()) {
          memrval = new NUMemselRvalue(irval, (*idxExprs)[0], loc);
      } else {
          memrval = new NUMemselRvalue(irval, idxExprs, loc);
      }
      INFO_ASSERT(memrval, "invalid lval");
      if(memnet->isIntegerSubrange() &&
         (32 > memnet->getRowSize()) &&
         (memrval->determineBitSize() == memnet->getRowSize())) {
          resizeIntegerRvalue(memrval, memnet->isSignedSubrange());
      }

      // If this is an array of vectors, then create a varsel for the
      // second index, and normalize the bit select index.
      if (1 != idxExprs->size() && (2 == memnet->getDeclaredDimensions())) {
          NUExpr* bitsel_expr = memnet->normalizeSelectExpr((*idxExprs)[1], 0, 0, true);
          NUExpr* varsel = new NUVarselRvalue(memrval, bitsel_expr, loc);
          mCurrentScope->sValue(varsel);
          return true;
      }
      mCurrentScope->sValue(memrval);
      return true;
  } else if (identNet->isVectorNet()) {
      NUExpr* expr = (*idxExprs)[0];
      INFO_ASSERT(expr, "invalid index expression");
      NUVectorNet* vNet = static_cast<NUVectorNet*>(identNet);
      INFO_ASSERT(vNet, "invalid vector net");
      const ConstantRange* bounds = vNet->getDeclaredRange();  
      if (NUConst* constant = dynamic_cast<NUConst*>(expr)) {
          SInt32 val;
          constant->getL(&val);
          ConstantRange c_range = ConstantRange(val, val);
          normalizeRange(&c_range, *bounds, vNet->getName()->str(), loc);
          NUExpr* varsel = new NUVarselRvalue(vNet, c_range, loc);
          INFO_ASSERT(varsel, "invalid expr");
          mCurrentScope->sValue(varsel);
          delete expr;
          delete irval;
      } else {
          NUExpr* norm = vNet->normalizeSelectExpr(expr, 0, true);
          if (norm) {
              expr = norm;
          }
          NUExpr* varsel = new NUVarselRvalue(irval, expr, loc);
          INFO_ASSERT(varsel, "invalid expr");
          mCurrentScope->sValue(varsel);
      }
      return true;
  } else {
      INFO_ASSERT(false, "fixme");
  }
  return false;
}

bool VerificVhdlDesignWalker::arrayIndexPrefixCompositeIdentLvalue(NUCompositeIdentLvalue* compIdentLval, NUExprVector* indexExprs, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << "ArrayIndex prefix Composite identlvalue" << std::endl;
#endif
    NUCompositeSelLvalue* selLvalue = new NUCompositeSelLvalue(compIdentLval, indexExprs, loc);
    INFO_ASSERT(selLvalue, "failed to sel lvalue");
    mCurrentScope->sValue(selLvalue);
    return true;
}

bool VerificVhdlDesignWalker::arrayIndexPrefixCompositeIdentRvalue(NUCompositeIdentRvalue* compIdentRval, NUExprVector* indexExprs, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << "ArrayIndex prefix composite ident rvalue" << std::endl;
#endif
    NUCompositeSelExpr* selExpr = new NUCompositeSelExpr(compIdentRval, indexExprs, loc);
    INFO_ASSERT(selExpr, "failed to sel lvalue");
    mCurrentScope->sValue(selExpr);
    return true;
}

bool VerificVhdlDesignWalker::arrayIndexPrefixCompositeNet(NUCompositeNet* cNet, NUExpr* rval1, const SourceLocator& source_locator)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << "ArrayIndex prefix CompositeNet" << std::endl;
#endif
  if (V2NDesignScope::LVALUE == mCurrentScope->gContext()) {
      NUCompositeInterfaceLvalue* iLvalue = new NUCompositeIdentLvalue(cNet, source_locator);
      INFO_ASSERT(iLvalue, "failed to create lvalue");
      NUExprVector eVec;
      eVec.push_back(rval1);
      NUCompositeSelLvalue* selLvalue = new NUCompositeSelLvalue(iLvalue, &eVec, source_locator);
      INFO_ASSERT(selLvalue, "failed to sel lvalue");
      mCurrentScope->sValue(selLvalue);
      return true;
  } else {
      NUCompositeIdentRvalue* iRvalue = new NUCompositeIdentRvalue(cNet, source_locator);
      INFO_ASSERT(iRvalue, "failed to create rvalue");
      NUExprVector eVec;
      eVec.push_back(rval1);
      NUCompositeInterfaceExpr* selRvalue = new NUCompositeSelExpr(iRvalue, &eVec, source_locator);
      INFO_ASSERT(selRvalue, "faioled to create sel expression");
      mCurrentScope->sValue(selRvalue);
      return true;
  }
  return false;
}

bool VerificVhdlDesignWalker::arrayIndexPrefixIdentLvalue(NUIdentLvalue* ilval, NUExprVector* idxExprs, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << "ArrayIndex prefix IdentLvalue" << std::endl;
#endif

  NUNet* identnet = ilval->getIdent();
  if (identnet->isMemoryNet()) {

      NUMemoryNet* memnet = static_cast<NUMemoryNet*>(identnet);
      INFO_ASSERT(memnet , "invalid memory net");
      NULvalue* memlval = 0;
      if (2 == memnet->getDeclaredDimensions()) {
          memlval = new NUMemselLvalue(ilval, (*idxExprs)[0], loc);
      } else {
          memlval = new NUMemselLvalue(ilval, idxExprs, loc);
      }
      INFO_ASSERT(memlval, "invalid lval");
      // If this is an array of vectors, then create a varsel for the
      // second index, and normalize the bit select index.
      if (1 != idxExprs->size() && (2 == memnet->getDeclaredDimensions())) {
          NUExpr* bitsel_expr = memnet->normalizeSelectExpr((*idxExprs)[1], 0, 0, true);
          NULvalue* varsel = new NUVarselLvalue(memlval, bitsel_expr, loc);
          mCurrentScope->sValue(varsel);
          return true;
      }
      mCurrentScope->sValue(memlval);
      
      /*NUMemoryNet* memnet = static_cast<NUMemoryNet*>(identnet);
      INFO_ASSERT(memnet , "invalid memory net");
      NUMemselLvalue* memlval = new NUMemselLvalue(ilval, idxExprs, loc);
      INFO_ASSERT(memlval, "invalid lval");
      mCurrentScope->sValue(memlval);*/
      return true;
  } else if (identnet->isVectorNet()) {
      NUExpr* expr = (*idxExprs)[0];
      INFO_ASSERT(expr , "invalid index expression");
      NUVectorNet* vNet = static_cast<NUVectorNet*>(identnet);
      INFO_ASSERT(vNet, "invalid ident net");
      if (NUConst* constant = dynamic_cast<NUConst*>(expr)) {
          const ConstantRange* bounds = vNet->getDeclaredRange();
          SInt32 val;
          constant->getL(&val);
          ConstantRange c_range = ConstantRange(val, val);
          normalizeRange(&c_range, *bounds, vNet->getName()->str(), loc);
          NULvalue* varsel = new NUVarselLvalue(vNet, c_range, loc);
          INFO_ASSERT(varsel, "invalid lvalue object");
          mCurrentScope->sValue(varsel);
          delete expr;
          delete ilval;
      } else {
          NUExpr* norm = vNet->normalizeSelectExpr(expr, 0, true);
          if (norm) {
              expr = norm;
          }
          NULvalue* varsel = new NUVarselLvalue(ilval, expr, loc);
          INFO_ASSERT(varsel, "invalid lvalue object");
          mCurrentScope->sValue(varsel);
      }
      return true;
  } else {
      INFO_ASSERT(false, "fixme");
  }
  return false;
}

bool VerificVhdlDesignWalker::arraySingleIndexPrefixMemselRvalue(NUMemselRvalue* mRvalue, NUExpr* rval1, const SourceLocator& loc)
{
  (void) loc;
  NUNet* ident = mRvalue->getIdent();
  INFO_ASSERT(ident->isMemoryNet(), "invalid memory net");
  NUMemoryNet* net = ident->getMemoryNet();
  UInt32 numMemoryDims = net->getNumDims();
  UInt32 numMemselDims = mRvalue->getNumDims();
  
  {
    // RJC RC#2014/08/14  what is this block for?  why do we normalize the range 0 every time we add an index?
    // also why is hierref check included here?

    const ConstantRange* range = net->getRange(0);
    const ConstantRange& op_range = net->getOperationalRange(0);

    if (!net->isHierRef() && (range->getMsb() != op_range.getMsb() || range->getLsb() != op_range.getLsb())) {
      NUExpr* norm = net->normalizeSelectExpr(rval1, 0, 0, true);
      if (norm) {
        rval1 = norm;
      }
      INFO_ASSERT(rval1, "failed to normalize");
    }
  }
  
  if ( (numMemselDims + 1) ==  numMemoryDims ){
    // this index we are adding here is for a bitselect of a word of the memory, so we need a NUVarselRvalue
    NUExpr* varsel = new NUVarselRvalue(mRvalue, rval1, loc);
    varsel->resize(varsel->determineBitSize());
    mCurrentScope->sValue(varsel);
  } else if ( (numMemselDims+1) < numMemoryDims ){
    mRvalue->addIndexExpr(rval1);
    mCurrentScope->sValue(mRvalue); // RJC thinks: why should this function be calling sValue?  This is out of place
  } else {
    // error condition, we have been asked to add another dimension beyond the number of indices specified for the memory
    UtString msg("Excessive number of indices specified for memory: ");
    msg << net->getName();
    mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&loc, msg.c_str());
    setError(true);
    mCurrentScope->sValue(0);
    return false;
  }
  return true;
}

bool VerificVhdlDesignWalker::arrayIndexPrefixMemselLvalue(NUMemselLvalue* lvalue, NUExprVector* idxExprs, const SourceLocator& loc)
{
    NUMemselLvalue* memsel = lvalue;
    bool success = false;
    for (UInt32 i = 0; i < idxExprs->size(); ++i) {
        mCurrentScope->sValue(NULL);
        success = arraySingleIndexPrefixMemselLvalue(memsel, idxExprs->at(i), loc);
        if (!success) return false;
        memsel = dynamic_cast<NUMemselLvalue*>(mCurrentScope->gValue());
    }
    return true;
}

bool VerificVhdlDesignWalker::arrayIndexPrefixMemselRvalue(NUMemselRvalue* rvalue, NUExprVector* idxExprs, const SourceLocator& loc)
{
    NUMemselRvalue* memsel = rvalue;
    bool success = false;
    for (UInt32 i = 0; i < idxExprs->size(); ++i) {
        mCurrentScope->sValue(NULL);
        success = arraySingleIndexPrefixMemselRvalue(memsel, idxExprs->at(i), loc);
        if (!success) return false;
        memsel = dynamic_cast<NUMemselRvalue*>(mCurrentScope->gValue());
    }
    return true;
}

bool VerificVhdlDesignWalker::arraySingleIndexPrefixMemselLvalue(NUMemselLvalue* lvalue, NUExpr* rval1, const SourceLocator& loc)
{
  NUNet* ident = lvalue->getIdent();
  INFO_ASSERT(ident->isMemoryNet(), "invalid memory net");
  NUMemoryNet* net = ident->getMemoryNet();
  UInt32 numMemoryDims = net->getNumDims();
  UInt32 numMemselDims = lvalue->getNumDims();

  {
    // RJC RC#2014/08/14  what is the following for?  why do we normalize the range 0 every time we add an index?
    const ConstantRange* range = net->getRange(0);
    const ConstantRange& op_range = net->getOperationalRange(0);

    if (range->getMsb() != op_range.getMsb() || range->getLsb() != op_range.getLsb()) {
      rval1 = net->normalizeSelectExpr(rval1, 0, 0, true);
      if (!rval1) {
        mNucleusBuilder->getMessageContext()->Verific2NUFailedToNormalize(&loc, net->getName()->str());
        setError(true);
        mCurrentScope->sValue(0);
        return false;
      }
    }
  }

  if ( (numMemselDims + 1) ==  numMemoryDims ){
    // this index we are adding here is for a bitselect of a word of the memory, so we need a NUVarselRvalue
    NUVarselLvalue* varsel = new NUVarselLvalue(lvalue, rval1, loc);
    mCurrentScope->sValue(varsel);
  } else if ( (numMemselDims+1) < numMemoryDims ){
    lvalue->addIndexExpr(rval1);
    mCurrentScope->sValue(lvalue); // RJC thinks: why should this function be calling sValue?  This is out of place
  } else {
    // error condition, we have been asked to add another dimension beyond the number of indices specified for the memory
    UtString msg("Excessive number of indices specified for memory: ");
    msg << net->getName();
    mNucleusBuilder->getMessageContext()->Verific2NUFailedToPopulate(&loc, msg.c_str());
    setError(true);
    mCurrentScope->sValue(0);
    return false;
  }

  return true;
}

bool VerificVhdlDesignWalker::arrayIndexPrefixMemoryNet(NUMemoryNet* memnet, NUExprVector* eVec, const SourceLocator& source_locator)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << "ArrayIndex prefix MemoryNet" << std::endl;
#endif

  //get info about index constraint
  //NUExprVector eVec;
  //eVec.push_back(rval1);
  
  //if (rval2) {
   //   eVec.push_back(rval2);
  //}
  if (V2NDesignScope::LVALUE == mCurrentScope->gContext()) {
      NUMemselLvalue* memlval = new NUMemselLvalue(memnet, eVec, source_locator);
      INFO_ASSERT(memlval, "invalid NUMemselLvalue object");
      mCurrentScope->sValue(memlval);
      return true;
  } else if (V2NDesignScope::RVALUE == mCurrentScope->gContext() ||
             V2NDesignScope::UNKNOWN_CONTEXT == mCurrentScope->gContext()) { // RJC RC#2014/08/19  why do we ever encounter UNKNOWN_CONTEXT here?
      NUMemselRvalue* memrval = new NUMemselRvalue(memnet, eVec, source_locator);
      INFO_ASSERT(memrval, "invalid NUMemselRvalue object");
      mCurrentScope->sValue(memrval);
      return true;
  } else {
      INFO_ASSERT(false, "not supported case");
      return false;
  }
  return false;
}

bool VerificVhdlDesignWalker::arrayIndexPrefixVectorNet(NUVectorNet* vNet, NUExpr* rval1, const SourceLocator& source_locator)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << "ArrayIndex prefix VectorNet" << std::endl;
#endif
  //constant range
  ConstantRange c_range;
  if (NUConst* constant = dynamic_cast<NUConst*>(rval1)) {
      const ConstantRange* bounds = vNet->getDeclaredRange();
      SInt32 val;
      constant->getL(&val);
      c_range = ConstantRange(val, val);
      normalizeRange(&c_range, *bounds, vNet->getName()->str(), source_locator);
      //create varsel object
      if (V2NDesignScope::LVALUE == mCurrentScope->gContext()) {
          NUVarselLvalue* varsel = new NUVarselLvalue(vNet, c_range, source_locator);
          INFO_ASSERT(varsel, "invalid lval");
          mCurrentScope->sValue(varsel);
      } else if (V2NDesignScope::RVALUE == mCurrentScope->gContext()) {
          NUExpr* varsel = new NUVarselRvalue(vNet, c_range, source_locator);
          INFO_ASSERT(varsel, "invalid expr");
          varsel->resize(varsel->determineBitSize());
          mCurrentScope->sValue(varsel);
      } else {
          INFO_ASSERT(false, "FIXME");
      }
      delete rval1;
  } else {
      NUExpr* norm = vNet->normalizeSelectExpr(rval1, 0, true);
      INFO_ASSERT(norm, "invalid expression");
      if (V2NDesignScope::LVALUE == mCurrentScope->gContext()) {
          NUVarselLvalue* varsel = new NUVarselLvalue(vNet, norm, source_locator);
          INFO_ASSERT(varsel, "invalid lval");
          mCurrentScope->sValue(varsel);
      } else if (V2NDesignScope::RVALUE == mCurrentScope->gContext() ||
                 V2NDesignScope::UNKNOWN_CONTEXT == mCurrentScope->gContext()) {
          NUExpr* varsel = new NUVarselRvalue(vNet, norm, source_locator);
          INFO_ASSERT(varsel, "invalid expr");
          mCurrentScope->sValue(varsel);
      }
  }
  return true;
}

// We're going to call this if 'prefix_expr' is a NUVarselLvalue, NUVarselRvalue, or a constant.
//
// If "prefix_expr" is a constant, we've lost the bounds of the object being indexed, so they
// have to be passed in as arguments. These are used to normalize the index.
//
// If "prefix_expr" is not constant, create a NUVarselRvalue, regardless of whether or not the
// index is constant.
bool VerificVhdlDesignWalker::arrayIndexPrefixExpr(NUExpr* prefix_expr, SInt32 prefix_left, SInt32 prefix_right, NUExpr* index_expr, const SourceLocator& source_locator)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << "ArrayIndex prefix VectorNet" << std::endl;
#endif
    NUExpr* varsel = NULL;
    if (prefix_expr->isConstant()) {
        // Normalize the index to the bounds
        NUExpr* normalized_index_expr = getNormalizedIndexExpr(index_expr, prefix_left, prefix_right);
        varsel = new NUVarselRvalue(prefix_expr, normalized_index_expr, source_locator);
    } else {
        varsel = new NUVarselRvalue(prefix_expr, index_expr, source_locator);
    }
    mCurrentScope->sValue(varsel);
    return true;
}

bool VerificVhdlDesignWalker::arrayIndexPrefixCompositeSelExpr(NUCompositeSelExpr* comSelExpr, NUExpr* rval1)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "composte sel expr" << std::endl;
#endif
    comSelExpr->addIndexExpr(rval1);
    mCurrentScope->sValue(comSelExpr);
    return true;
}

bool VerificVhdlDesignWalker::arrayIndexPrefixCompositeSelLvalue(NUCompositeSelLvalue* comSelLval, NUExpr* rval1)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "composte sel lvalue" << std::endl;
#endif
    comSelLval->addIndexExpr(rval1);
    mCurrentScope->sValue(comSelLval);
    return true;
}


bool VerificVhdlDesignWalker::arrayIndexPrefixCompositeFieldExpr(NUCompositeFieldExpr* comFieldExpr, NUExpr* rval1, const SourceLocator& source_locator)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << "composite field expr" << std::endl;
#endif
  NUExprVector eVec;
  eVec.push_back(rval1);
  NUCompositeInterfaceExpr* selRvalue = new NUCompositeSelExpr(comFieldExpr, &eVec, source_locator);
  INFO_ASSERT(selRvalue, "faioled to create sel expression");
  mCurrentScope->sValue(selRvalue);
  return true;
}

bool VerificVhdlDesignWalker::arrayIndexPrefixCompositeFieldLvalue(NUCompositeFieldLvalue* comFieldLvalue, NUExpr* rval1, const SourceLocator& source_locator)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << "composite field lvalue" << std::endl;
#endif
  NUExprVector eVec;
  eVec.push_back(rval1);
  NUCompositeInterfaceLvalue* selLvalue = new NUCompositeSelLvalue(comFieldLvalue, &eVec, source_locator);
  mCurrentScope->sValue(selLvalue);
  return true;
}


NUExpr* VerificVhdlDesignWalker::createConstantExpression(VhdlValue* val, SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "create constant expression" << std::endl;
#endif
    INFO_ASSERT(val, "invalid VhdlValue object");
    INFO_ASSERT(val->IsConstant(), "invalid constant VhdlValue object");

    NUExpr* the_const = 0;

    if (val->IsIntVal()) {
        the_const = NUConst::createKnownIntConst(val->Integer(), 32,  false, loc);
        the_const->setSignedResult(true);
    } else if (val->IsEnumValue()) {
        INFO_ASSERT(val->ToEnum(), "Failed convert to Enum");
        INFO_ASSERT(val->ToEnum()->GetEnumId(), "Failed to get Enum ID");
        UInt32 bits = bitness(val->ToEnum()->GetEnumId()->NumOfEnums() - 1);
        the_const = NUConst::createKnownIntConst(val->Integer(), bits, true, loc);
        //FD thinks: enumeration types are always unsigned by definition
        the_const->setSignedResult(val->IsSigned());
    }
    return the_const;
}

// This method checks to see if an index type is std_ulogic, and if so, adjusts
// the index. This also applies to std_logic types, since these are ultimately std_ulogic
// types. See comments below for more detail.
NUExpr* VerificVhdlDesignWalker::adjustStdUlogicIndexIfNecessary(VhdlDiscreteRange* assoc, NUExpr* index, SourceLocator& loc)
{
  NUExpr* adjustedIndex = index;
  // Get the type for this expression
  Set return_types(POINTER_HASH) ;
  (void) assoc->TypeInfer(0,&return_types,VHDL_READ,0) ;
  VhdlIdDef* typeId = (VhdlIdDef*)return_types.GetLast();
  if (typeId && typeId->IsStdULogicType()) {
    getMessageContext()->StdLogicAsArrayIndex( &loc );
    // Here we are using an object with an absolute base type of
    // STD_ULOGIC as an index in an expression.  This expression can
    // only have the values [01] in cbuild.  However, in VHDL, this
    // expression can be [UX01ZWLH-].  So, if our index expression will
    // come back either as 0 or 1, we need to or 0x2 to it to make it
    // refer to the proper index in an array using a std_[u]logic as an
    // index.
    NU_ASSERT( index->determineBitSize() == 1, index ); // sanity check
    NUConst* offset = NUConst::create(false, 1, 1, loc);
    NUExprVector concats;
    concats.push_back(offset);
    concats.push_back(index);
    adjustedIndex = new NUConcatOp(concats, 1, loc);
  };
  return adjustedIndex;
}

// Steve Lim
// This has a hard-coded two-dimension limit which we need to remove.
bool VerificVhdlDesignWalker::arrayIndex(VhdlIndexedName& node)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << "ArrayIndex" << std::endl;
  node.PrettyPrint(std::cerr, 0);
  std::cerr << std::endl;
#endif
  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

  if (hasError()) return false;

  if (! node.GetPrefix()) {
      setError(true);
      mCurrentScope->sValue(0);
      mNucleusBuilder->getMessageContext()->Verific2NUInvalidNode(&loc, "indexed_name");
      return false;
  }
  
  // RBS thinks: why is this necessary? This check keeps the mPrefix
  // flag from doing it's job - which is to prevent prefix expressions
  // from being converted to NUConst's. If this check is removed,
  // beacon/expressions.indexed_name16.vhdl does not simulate correctly.
  if (V2NDesignScope::LVALUE != mCurrentScope->gContext()) {
      VhdlValue* value = node.Evaluate(0, mDataFlow, 0);
      if (value && value->IsConstant()) {
          bool res = evaluateConstantExpression(node, value);
          INFO_ASSERT(res, "failed to create constant expression");
          return true;
      }
  }
  VhdlConstraint* prefix_constraint = 0;
  unsigned prefix_allocated = 0;

  VhdlIdDef* prefix_id = node.GetPrefixId();
  VhdlName* prefix_name = node.GetPrefix();
  Array* assoc_list = node.GetAssocList();

  VhdlName* alias_target = (prefix_id && prefix_id->IsAlias()) ? prefix_id->GetAliasTarget() : 0;
  if (alias_target && ! alias_target->FindFullFormal()) alias_target = 0;
  VhdlDiscreteRange* ref_target = (prefix_id && prefix_id->IsRefFormal()) ? prefix_id->GetRefTarget() : 0;
  if (prefix_id && prefix_name->FindFullFormal() &&
      (!prefix_id->IsAlias() || alias_target)    &&
      (!prefix_id->IsRefFormal() || ref_target)  &&
      !prefix_id->IsSubprogram()                 &&
      !prefix_id->IsRecordElement()) {
      prefix_constraint = prefix_id->Constraint();
  } else {
      //prefix is complex
      prefix_constraint = (prefix_name) ? prefix_name->EvaluateConstraintInternal(mDataFlow, 0) : 0;
      prefix_allocated = 1;

  }
  if (!prefix_constraint) {
      INFO_ASSERT(false, "");
      return false;
  }
  VhdlConstraint* oldTargetConstraint = mTargetConstraint;
  mTargetConstraint = prefix_constraint;
  mCurrentScope->sPrefix(true);
  //FD thinks: what about constant prefixes ? Shouldn't we try to evaluate prefix and populate constant instead of selection?
  prefix_name->Accept(*this);
  mCurrentScope->sPrefix(false);
  NUBase* pBase = mCurrentScope->gValue();
  if (!pBase) {
      mNucleusBuilder->getMessageContext()->Verific2NUErrorDuringPopulation(&loc, "prefix of array index could not be created");
      mCurrentScope->sValue(NULL);
      setError(true);
      return false;
  }
  mTargetConstraint = oldTargetConstraint;
  
  VhdlDiscreteRange* assoc = 0;
  
  VhdlConstraint* constraint = prefix_constraint;
  unsigned i;
  UtVector<NUExpr*> indexExprs;
  FOREACH_ARRAY_ITEM(assoc_list, i, assoc) {
      if (!assoc) break;

      if (constraint->IsNullRange()) {
          //populate error FIXME
          INFO_ASSERT(false, "");
          return false;
      }

      V2NDesignScope::ContextType cType = mCurrentScope->gContext();
      mCurrentScope->sContext(V2NDesignScope::RVALUE);
      assoc->Accept(*this);
      if (hasError()) {
          mCurrentScope->sValue(0);
          return false;
      }

      mCurrentScope->sContext(cType);
      NUExpr* indexExpr = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
      INFO_ASSERT(indexExpr, "invalid rval");
      // Adjust std_ulogic index expressions (see comments at adjustStdUlogicIndex).
      indexExpr = adjustStdUlogicIndexIfNecessary(assoc, indexExpr, loc);
      indexExprs.push_back(indexExpr);
      constraint = constraint->ElementConstraint();
  }

  // Get the bounds of the prefix. Required if the prefix evaluates to a constant, 
  // so the index can be properly normalized.
  SInt32 prefix_left = 0, prefix_right = 0;
  Verific2NucleusUtilities::getPrefixBounds(prefix_constraint, &prefix_left, &prefix_right);

  if (prefix_allocated) {
      delete prefix_constraint;
  }
  INFO_ASSERT(!indexExprs.empty(), "empty assoc expressions for array index");

  if (NUMemoryNet* memnet = dynamic_cast<NUMemoryNet*>(pBase)) {
      return arrayIndexPrefixMemoryNet(memnet, &indexExprs, loc);
  } else {
      UtString headMsg;
      headMsg << "Single index used in arrayIndex function, for prefix type ";
      UtString tailMsg;
      tailMsg << " with this many index exprs: " << indexExprs.size();
      UtString msg;
      if (NUCompositeNet* cNet = dynamic_cast<NUCompositeNet*>(pBase)) {
          msg << headMsg.c_str() << "NUCompositeNet" << tailMsg.c_str();
          INFO_ASSERT(indexExprs.size() == 1, msg.c_str());
          return arrayIndexPrefixCompositeNet(cNet, indexExprs[0], loc);
      } else if (NUIdentRvalue* iRval = dynamic_cast<NUIdentRvalue*>(pBase)) {
          return arrayIndexPrefixIdentRvalue(iRval, &indexExprs, loc);
      } else if (NUIdentLvalue* iLval = dynamic_cast<NUIdentLvalue*>(pBase)) {
          return arrayIndexPrefixIdentLvalue(iLval, &indexExprs, loc);
      } else if (NUMemselRvalue* mRvalue = dynamic_cast<NUMemselRvalue*>(pBase)) {
          return arrayIndexPrefixMemselRvalue(mRvalue, &indexExprs, loc);
      } else if (NUMemselLvalue* mLvalue = dynamic_cast<NUMemselLvalue*>(pBase)) {
          return arrayIndexPrefixMemselLvalue(mLvalue, &indexExprs, loc);
      } else if (NUVectorNet* vNet = dynamic_cast<NUVectorNet*>(pBase)) {
          msg << headMsg.c_str() << "NUVectorNet" << tailMsg.c_str();
          INFO_ASSERT(indexExprs.size() == 1, msg.c_str());
          return arrayIndexPrefixVectorNet(vNet, indexExprs[0], loc);
      } else if (NUCompositeSelExpr* comSelExpr = dynamic_cast<NUCompositeSelExpr*>(pBase)) {
          msg << headMsg.c_str() << "NUCompositeSelExpr" << tailMsg.c_str();
          INFO_ASSERT(indexExprs.size() == 1, msg.c_str());
          return arrayIndexPrefixCompositeSelExpr(comSelExpr,  indexExprs[0]);
      } else if (NUCompositeSelLvalue* comSelLval = dynamic_cast<NUCompositeSelLvalue*>(pBase)) {
          msg << headMsg.c_str() << "NUCompositeSelLvalue" << tailMsg.c_str();
          INFO_ASSERT(indexExprs.size() == 1, msg.c_str());
          return arrayIndexPrefixCompositeSelLvalue(comSelLval,  indexExprs[0]);
      } else if (NUCompositeFieldExpr* comFieldExpr = dynamic_cast<NUCompositeFieldExpr*>(pBase)) {
          msg << headMsg.c_str() << "NUCompositeFieldExpr" << tailMsg.c_str();
          INFO_ASSERT(indexExprs.size() == 1, msg.c_str());
          return arrayIndexPrefixCompositeFieldExpr(comFieldExpr, indexExprs[0], loc);
      } else if (NUCompositeFieldLvalue* comFieldLvalue = dynamic_cast<NUCompositeFieldLvalue*>(pBase)) {
          msg << headMsg.c_str() << "NUCompositeFieldLvalue" << tailMsg.c_str();
          INFO_ASSERT(indexExprs.size() == 1, msg.c_str());
          return arrayIndexPrefixCompositeFieldLvalue(comFieldLvalue, indexExprs[0], loc);
      } else if (NUCompositeIdentLvalue* compIdentLval = dynamic_cast<NUCompositeIdentLvalue*>(pBase)) {
          return arrayIndexPrefixCompositeIdentLvalue(compIdentLval, &indexExprs, loc);
      } else if (NUCompositeIdentRvalue* compIdentRval = dynamic_cast<NUCompositeIdentRvalue*>(pBase)) {
          return arrayIndexPrefixCompositeIdentRvalue(compIdentRval, &indexExprs, loc);
      } else if (NUExpr* expr = dynamic_cast<NUExpr*>(pBase)) {
          msg << headMsg.c_str() << "NUExpr" << tailMsg.c_str();
          INFO_ASSERT(indexExprs.size() == 1, msg.c_str());
          return arrayIndexPrefixExpr(expr, prefix_left, prefix_right, indexExprs[0], loc);
      } else {
          pBase->print(true, 0);
          INFO_ASSERT(false, "not supported yet");
          return false;
      }
  }

  return true;
}

///gets bitness for specified unsigned number
unsigned VerificVhdlDesignWalker::bitness(unsigned n)
{
  unsigned count = 0;
  while (n) {
    count++;
    n = n >> 1;
  }
  return count;
}

///generate initial assignment object for specified net
///create initial block if there is not one
///and add initial assignment
//ED:TODO this needs rewriting.
//Too many cases not covered.
void VerificVhdlDesignWalker::initAssign(VhdlTreeNode* node, NUNet* net)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "InitAssign" << std::endl;
    node->PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif

    if (hasError()) {
        mCurrentScope->sValue(0);
        return ;
    }
  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node->Linefile());  
  //initial block
  NUBlock* block = 0;
  NUScope* current_scope = getNUScope(NULL);
  NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(current_scope, loc, "Unable to get scope");
  if (NUScope::eTask == current_scope->getScopeType()) {
    //if current scope is task then we definatly know that 
    //current scope owner is NUTask , so we can just using
    //static_cast
    NUTask* task = dynamic_cast<NUTask*>(mCurrentScope->gOwner());
    V2N_INFO_ASSERT(task, *node, "invalid task");
    //fron statement of task is always block
    block = dynamic_cast<NUBlock*>(task->getStmt()); //block is initial block
    V2N_INFO_ASSERT(block, *node, "invalid block");
  } else {
    NUInitialBlockList initial_list;
    getModule()->getInitialBlocks(&initial_list);
    block = 0;
    if (initial_list.empty()) { //initial list is empty so there is no initial block
      //create initial block
      NUModule* module = getModule();
      NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, loc, "Unable to get parent module");
      block = mNucleusBuilder->cBlock(module, loc); // initial top block
      NUScope* parent_block_scope = getBlockScope();
      NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(parent_block_scope, loc, "Unable to get block scope");
      // FD: uses gensym and 'initialBlock' prefix, in verilog flow we just use 'newBlockName' api, not sure which one is right
      // anyway not calling nucleus builder api until clarification (I think they should be the same for verilog/vhdl)
      // NUInitialBlock* initial_block = mNucleusBuilder->cInitialBlock(parent_block_scope, block, loc);
      NUInitialBlock* initial_block = new NUInitialBlock(parent_block_scope->gensym("initialBlock"), 
              block, mNucleusBuilder->gNetRefFactory(), loc);
      mNucleusBuilder->aInitialBlockToModule(initial_block, module);
    } else { //initial list is not empty so there is initial block
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(1 == initial_list.size(), *node, "only 1 initial block should exist");
      NUInitialBlock* initial_block = initial_list.front();
      VERIFIC_CONSISTENCY_NO_RETURN_VALUE(initial_block, *node, "invalid initial block");
      block = initial_block->getBlock(); //initial block
    }
  }
  V2NDesignScope::ContextType cType = mCurrentScope->gContext();
  mCurrentScope->sContext(V2NDesignScope::RVALUE);
  node->Accept(*this);
  mCurrentScope->sContext(cType);
  NUBase* base = mCurrentScope->gValue();
  if (! base) {
      mNucleusBuilder->getMessageContext()->Verific2NUErrorDuringPopulation(&loc, "");
      mCurrentScope->sValue(NULL);
      setError(true);
      return ;
  }
  
  INFO_ASSERT(dynamic_cast<NUExpr*>(base), "should has NUExpr base");
  NULvalue* lval = mNucleusBuilder->createIdentLvalue(net, loc);
  NUExpr* expr = static_cast<NUExpr*>(base);
  INFO_ASSERT(expr, "invlaid NUExpr object");
  NUBlockingAssign* ba = new NUBlockingAssign(lval, expr, false, loc, false);
  V2N_INFO_ASSERT(ba, *node, "invalid blocking assign");
  block->addStmt(ba);

}

// Description: To create verilog style for_loop control object initializing 
//              stmt, advancing stmt and condition evaluation expression from
//              VHDL for's loop_parameter_spec (or for_index) 
//        In VHDL a 'for' loop is represented as 
//     [<loop_label> :] 
//        for <identifier> in <discrete_range> loop
//            <sequence_of_statements>
//        end loop [ <loop_label> ] 
// In the nucleus, NUFor class represents the verilog style 'for' statement, 
// where we have  1) Stmt initializing loop control variable 
//                2) A condition expression 
//            and 3) An advancing Stmt on loop control variable
// For eg.  
//    for loop_var in 4 to 9  -- vh_for_index or loop_parameter_spec 
//    loop 
//       <stmts>
//    end loop
//
// For the above VHDL stmt we create 
//   the_initial   ===> 'loop_var = 4'
//   the_condition ===> '(loop_var <= 9)'
//   the_advance   ===> ' loop_var = loop_var + 1'
// The above three objects are used to populate NUFor object.
void VerificVhdlDesignWalker::forloop_first_last_step(UtPair<SInt32, SInt32> p,
						      VhdlForScheme& node,
						      NUNet*& induction,
						      NUStmtList& initial_list, 
						      NUExpr*& cond, 
						      NUStmtList& advance_list)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
      std::cerr << "ForLoop_First_Last_Step" << std::endl;
#endif
      SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());  
      UtVector<NUBase*> val;
      mCurrentScope->gValueVector(val);
      V2N_INFO_ASSERT(2 == val.size(), node, "vector size should be two");
      SInt32 start = p.first;
      SInt32 end = p.second;
      UtString str;
      //create init expression
      NULvalue* lval = mNucleusBuilder->createIdentLvalue(induction, source_locator);
      NUExpr* exp = 0;
      if (!val[0]) {
          exp = NUConst::createKnownIntConst(start, 32,  false, source_locator);
          exp->setSignedResult(true);
      } else {
          NUNet* n = dynamic_cast<NUNet*>(val[0]);
          if (n) {
              INFO_ASSERT(n, "invalid net");
              exp = new NUIdentRvalue(n, source_locator);
          } else {
              exp = static_cast<NUExpr*>(val[0]);
          }
      }
      V2N_INFO_ASSERT(exp, node, "invalid expression");
      NUBlockingAssign* init = new NUBlockingAssign(lval, exp, false, source_locator, false);
      V2N_INFO_ASSERT(init, node, "invalid blocking assign");
      initial_list.push_back(init);
      //create condition expression
      NUIdentRvalue* rval = new NUIdentRvalue(induction, source_locator);
      V2N_INFO_ASSERT(rval, node, "invalid rval");
      if (! val[1]) {
          exp = NUConst::createKnownIntConst(end, 32,  false, source_locator);
          exp->setSignedResult(true);
      } else {
          NUNet* n = dynamic_cast<NUNet*>(val[1]);
          if (n) {
              V2N_INFO_ASSERT(n, node, "invalid net");
              exp = new NUIdentRvalue(n, source_locator);
          } else {
              exp = static_cast<NUExpr*>(val[1]);
          }
      }
      if (VHDL_to == node.GetRange()->GetDir()) {
          cond = new NUBinaryOp(NUOp::eBiSLte, rval, exp, source_locator);
      } else if (VHDL_downto == node.GetRange()->GetDir()) {
          cond = new NUBinaryOp(NUOp::eBiSGtre, rval, exp, source_locator);
      } else if (!val[0] && !val[1] && start < end) {
          cond = new NUBinaryOp(NUOp::eBiSLte, rval, exp, source_locator);
      } else {
          cond = new NUBinaryOp(NUOp::eBiSGtre, rval, exp, source_locator);
      }

      V2N_INFO_ASSERT(cond, node, "invalid binary op");
      //create step expression
      exp = NUConst::createKnownIntConst(1, 32,  false, source_locator);
      exp->setSignedResult(true);
      V2N_INFO_ASSERT(exp, node, "invalid expression");
      rval = new NUIdentRvalue(induction, source_locator);
      V2N_INFO_ASSERT(rval, node, "invalid rval");
      NUExpr* step = 0; 
      if (VHDL_to == node.GetRange()->GetDir()) {
          step = new NUBinaryOp(NUOp::eBiPlus, rval, exp, source_locator);
      } else if (VHDL_downto == node.GetRange()->GetDir()) {
          step = new NUBinaryOp(NUOp::eBiMinus, rval, exp, source_locator);
      } else if (!val[0] && !val[1] && start < end) {
          step = new NUBinaryOp(NUOp::eBiPlus, rval, exp, source_locator);
      } else {
          step = new NUBinaryOp(NUOp::eBiMinus, rval, exp, source_locator);
      }

      step->setSignedResult(rval->isSignedResult() && exp->isSignedResult());
      V2N_INFO_ASSERT(step, node, "invalid expression");
      lval = mNucleusBuilder->createIdentLvalue(induction, source_locator);
      NUBlockingAssign* advance = new NUBlockingAssign(lval, step, false, source_locator, false);
      advance_list.push_back(advance);
}

///special handle for 'nor','nand' and xnor because of there is no one operator 
///for representing in Nucleus DB
NUUnaryOp* VerificVhdlDesignWalker::handleNorNandXnor(VhdlOperator& node, 
						  NUExpr* lhs, 
						  NUExpr* rhs,
						  SourceLocator src_locator)
{
  bool is_signed = lhs->isSignedResult() && rhs->isSignedResult();
  NUBinaryOp* bin = 0;
  if(VHDL_nand == node.GetOperatorToken()) {
    bin = new NUBinaryOp(NUOp::eBiBitAnd, lhs, rhs, src_locator);
  } else if (VHDL_nor == node.GetOperatorToken()) {
    bin = new NUBinaryOp(NUOp::eBiBitOr, lhs, rhs, src_locator);
  } else if (VHDL_xnor == node.GetOperatorToken()) {
    bin = new NUBinaryOp(NUOp::eBiBitXor, lhs, rhs, src_locator);
  }
  INFO_ASSERT(bin, "invalid binary operation");
  bin->setSignedResult(is_signed);
  return new NUUnaryOp(NUOp::eUnBitNeg, bin, src_locator);
}

//if lvalue is memory net and rvalue is constant
//we have to return concat op with constant values
//otherwise just constant value
//to satisfy above mentioned condition we we mark left side memory nets
//usage VerificVhdlStatementWalker.cpp:VHDL_VISIT(VhdlVariableAssignmentStatement, node)
void VerificVhdlDesignWalker::markMemIdentLvalue(NULvalue* lval)
{
    INFO_ASSERT(lval, "invalid lvalue");
    if (dynamic_cast<NUMemselLvalue*>(lval)) {
        mCurrentScope->sMemNet(true);
    } else if (NUIdentLvalue* iLval = dynamic_cast<NUIdentLvalue*>(lval)) {
        NUNet* net = iLval->getIdent();
        INFO_ASSERT(net, "invalid net");
        if (dynamic_cast<NUMemoryNet*>(net)) {
            mCurrentScope->sMemNet(true);
        }
     }
}

//get net for specified module and net name
//get 0 if net doesn't exist
NUNet* VerificVhdlDesignWalker::lookupNet(NUModule* module, UtString& name)
{
    NUNetList* net_list = new NUNetList;
    module->getAllTopNets(net_list);
    for (Loop<NUNetList> p(*net_list); !p.atEnd(); ++p) {
        NUNet* net = *p;
        if (dynamic_cast<NUCompositeNet*>(net)) {
            INFO_ASSERT(mCompositeNet.end() != mCompositeNet.find(net), "failed to find composite net");
            UtString net_name(mCompositeNet.find(net)->second->str());
            net_name.uppercase();
            name.uppercase();
            if (net_name == name) {
                delete net_list;
                return net;
            }
        }
        UtString net_name(net->getName()->str());
        net_name.uppercase();
        name.uppercase();
        if (net_name == name) {
            delete net_list;
            return net;
        }
    }
    delete net_list;
    return 0;
}

//RJC RC#2013/08/13 (tigran) this looks like a fertile place for refactoring, this method is doing two orthogonal things, testing if there
//is an edge expression and returning an always block. However when I look at where this method is called, the caller code appears to be
//identical. Perhaps the right thing to do here is to create a method that will do the following: (check for edge expressions, if they don't
//yet exist then create a NUEdgeExpr, and add it to the containing always block). Another thing I noticed is that the setting for the flags setIsClock and
//setIsReset is not consistent at the three places where I saw isEdgeExprExists called.  perhaps a potential error?   please investigate
//that issue
//ADDRESSED didn't figure out the last comment about setIsClock()/setIsReset() yet, needs more investigation

///return true if edge expression with specified net and clock edge alredy exists
///otherwise return false
bool VerificVhdlDesignWalker::isEdgeExprExists(NUNet* net, ClockEdge edge)
{
  INFO_ASSERT(net, "");
  NUAlwaysBlock* always = getAlwaysBlock();
  NUEdgeExprList edge_list;
  always->getEdgeExprList(&edge_list);
  //check existence for specified edge expression
  //based on specified name and clockedge
  if (edge_list.empty()) { return false; }
  bool edgeNotExists = false;
  for (NUEdgeExprList::iterator it = edge_list.begin(); it != edge_list.end(); ++it) {
    NUEdgeExpr* ee = (*it);
    INFO_ASSERT(ee, "invalid edge expression");
    if (ee->getNet() == net && ee->getEdge() == edge) {
      edgeNotExists = true;
    } else if ((ee->getNet() == net) && (ee->getEdge() != edge)) {
        mNucleusBuilder->getMessageContext()->Verific2NUDiffEdge(&net->getLoc());
        setError(true);
        return false;
    }
  }
  return edgeNotExists;
}

NUEdgeExpr* VerificVhdlDesignWalker::addEdgeExprToAlways(NUIdentRvalue* iRval, bool isClock, ClockEdge edge_type)
{
  INFO_ASSERT(iRval, "Invalid input pointer");
  NUNet* net = iRval->getIdent();
  INFO_ASSERT(net, "");
  NUEdgeExpr* nu_edge_expr = new NUEdgeExpr(edge_type, iRval, iRval->getLoc());
  nu_edge_expr->resize(1);
  if (isClock) {
    nu_edge_expr->setIsClock();
  } else {
    nu_edge_expr->setIsReset();
  }
  net->putIsEdgeTrigger(true);
  NUAlwaysBlock* always = getAlwaysBlock();
  always->addEdgeExpr(nu_edge_expr); //add to nearest always block
  mEdgeExpr = nu_edge_expr;
  return nu_edge_expr;
}

void VerificVhdlDesignWalker::addPragmas(UtString* netOrFunctionName, UtString* moduleName, UtString* packageName, Map* pragmas, bool isParentPackage, SourceLocator* loc)
{
    INFO_ASSERT(moduleName, "Module name should always exist");
    // walking attributes for current net
    MapIter it ;
    char * value = 0;
    char * name = 0;
    FOREACH_MAP_ITEM(pragmas, it, &name, &value) {
        UtString stripValue(value);
        if ((stripValue.size() > 1) && ('"' == stripValue[0]) && ('"' == stripValue[stripValue.size() - 1])) {
            // remove leading and trailing quotes if they exist (TODO check if we will need this)
            stripValue = stripValue.substr(1, stripValue.size() - 2);
        }
        // name is concatenation of pragma trigger and pragma name e.g. carbon inline, so needs to be separated here
        UtString pragmaString(name);
        UtString pragmaName(pragmaString.substr(pragmaString.find_first_of(' ')+1));
        if (isParentPackage) packageName->uppercase(); // in order to match with Interra
        if (mNucleusBuilder->isKnownDirective(pragmaName)) {
            if (mNucleusBuilder->isModuleDirective(pragmaName)) {
                mNucleusBuilder->addDirective(pragmaName, stripValue, *moduleName, 0, *loc); 
            } else {
                if (mNucleusBuilder->isFunctionDirective(pragmaName) && isParentPackage) {
	                mNucleusBuilder->getMessageContext()->InlineDirPack(name, packageName->c_str() );
                } else {
                    INFO_ASSERT(netOrFunctionName, "Net/Function name should always exist for net/function directives");
                    mNucleusBuilder->addDirective(pragmaName, stripValue, *moduleName, *netOrFunctionName, *loc); 
                }
            }
        } else {
            mNucleusBuilder->getMessageContext()->Verific2NUUnrecognizedPragma(loc, name);
        }
    }
}

void VerificVhdlDesignWalker::deleteMapOfConstraints(Map* formal2constraint)
{
    VhdlIdDef* id = 0;
    VhdlConstraint* constraint = 0;
    MapIter mi;
    FOREACH_MAP_ITEM(formal2constraint, mi, &id, &constraint) {
        delete constraint;
    }
}
#define _ENABLE_SENS_CHECK

// Steve Lim 2013-12-14 Check a signal against the process' sensitivity list.
bool VerificVhdlDesignWalker::checkAgainstSensList(VhdlIdDef* sig)
{
  if ((sig->IsPort() && sig->IsOutput()) || sig->IsSubprogramFormal())
    return true;
  if (mInWaitStatement)
    return true;
#ifndef _ENABLE_SENS_CHECK
  (void) sig;
  return true;
#else
  if (isCheckedAgainstSensList(sig))
    return true;

  bool valid = false;
  if (mEdgeExpr || mEdgeValidity.size() > 0)
  {
    valid = true;
  }
  else
  {
    unsigned i;
    VhdlTreeNode* node;
    FOREACH_ARRAY_ITEM(mSensitivityList, i, node)
    {
      VhdlIdDef* id = getTopId(node);
      if (id == sig)
      {
        valid = true;
        break;
      }
    }
  }
  // Register that this signal has now been checked against the sensitivity list
  rCheckedAgainstSensList(sig);
  return valid;
#endif
}

void VerificVhdlDesignWalker::rCheckedAgainstSensList(VhdlIdDef* sig)
{
  mSignalsCheckedAgainstSensList[sig] = 1;
}

// Returns true if a signal has already been checked against the sensitivity list.
bool VerificVhdlDesignWalker::isCheckedAgainstSensList(VhdlIdDef* sig)
{
  UtMap<VhdlIdDef*,int>::iterator it = mSignalsCheckedAgainstSensList.find(sig);
  return (it != mSignalsCheckedAgainstSensList.end());
}
 
bool VerificVhdlDesignWalker::namedFunction(VhdlIdDef* func)
{
  std::string s(func->Name());
  return (s.find_first_of("\"") != std::string::npos);
}

void VerificVhdlDesignWalker::setEdgeExpr(NUExpr* expr)
{
  mEdgeExpr = expr;
}


// Attempt to create a NUConst for \a value, returns true if successful, false otherwise, the NUConst is placed in mCurrentScope->sValue()
bool VerificVhdlDesignWalker::evaluateConstantExpression(VhdlDiscreteRange& node, VhdlValue* value)
{
    mCurrentScope->sValue(NULL);

    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    if (!value) {
      // Avoid Verific constant evaluation if there are NULL ranges involved. We want to give warnings,
      // but in relational expression will embedded NULL ranges, Verific will return a 'true' or 'false'
      // value, without provided a meaningful warning. An exampl of this can be found in
      // rushc/vhdl/beacon_types_decls/types_decls.ARR_COMP_GEQ.vhdl
      if (!Verific2NucleusUtilities::hasNullRange(node, mDataFlow)) {
        value = node.Evaluate(mTargetConstraint, mDataFlow, 0);
      }
    }

    if (!value) {
        return false;
    }
    if (!value->IsConstant()) {
        delete value;
        return false;
    }

    //FD TODO check constraint, don't use value->NumBits()
    UInt32 bitsize = (value->IsIntVal() ? 32 : value->NumBits());

    if (value->HasUXZW()) {
      char* image = value->Image();
      UtString constValStr(image);
      Strings::free (image);
      // remove any "'" that are part of image, (here we would also remove any other non-value characters that are part of image if we knew what to look for)
      size_t pos = constValStr.find_first_of("'", 0, 1);
      while ( UtString::npos != pos ) {
        constValStr.erase(pos,1);
        pos = constValStr.find_first_of("'",pos,1);
      }
      // FD thinks: using value IsSigned is incorrect
      bool makeSigned = value->IsSigned() || value->IsIntVal();
      NUExpr* theConst = NUConst::createXZ(constValStr, makeSigned, bitsize, loc);
      mCurrentScope->sValue(theConst);
      
      delete value;
      return true;
    }


    // what about values larger than 64 bits?  (value->Integer() only returns a 64 bit value
    NUExpr* theConst = NUConst::createKnownIntConst(value->Integer(), bitsize, false, loc);
    // FD thinks: TODO correct sign (value->IsSigned() can't be used)
    theConst->setSignedResult(value->IsSigned() ? true : value->IsIntVal() ? true :false);
    mCurrentScope->sValue(theConst);
    delete value;
    return true;
                    
}

/*-----------------------------------------------------------------*/
//          helper methods for api name adjustments 
/*-----------------------------------------------------------------*/

bool VerificVhdlDesignWalker::hasError()
{
    return mNucleusBuilder->hasError();
}

void VerificVhdlDesignWalker::setError(bool error)
{
    mNucleusBuilder->setError(error);
}

MsgContext* VerificVhdlDesignWalker::getMessageContext()
{
    return mNucleusBuilder->getMessageContext();
}
/*-----------------------------------------------------------------*/

void VerificVhdlDesignWalker::addLoopStatements(VhdlLoopStatement* node, NUBlock* body_block)
{
    unsigned i = 0;
    VhdlTreeNode* item = 0;
    FOREACH_ARRAY_ITEM(node->GetStatements(), i, item) {
        item->Accept(*this);
        NUStmt* stmt = dynamic_cast<NUStmt*>(mCurrentScope->gValue());
        if (stmt) {
            body_block->addStmt(stmt);
        }
        mCurrentScope->sValue(NULL); // to avoid using it again
    }
}

void VerificVhdlDesignWalker::expandAndAddLoopStatements(VhdlLoopStatement* node, NUBlock* body_block, VhdlForScheme* fScheme,
    const UtPair<SInt32, SInt32>& p)
{
    // get initial value to set back later
    bool downUp = (p.first > p.second); // true - down, false - up
    // TODO haven't found an easy approach to optimize the below for loop
    for (SInt32 j = p.first; downUp ? (j >= p.second) : (j <= p.second); downUp ? --j : ++j) {
    //while (i <= last) {
        VhdlIdRef* iterId = fScheme->GetId();
        VERIFIC_CONSISTENCY_NO_RETURN_VALUE(iterId, *fScheme, "invalid iterator ID");
        // FD: I'm finding this dangerous in terms of memory leak as here it isn't obvious who is responsible for deleting Verific object. Another concern is parse-tree change (as far as I can see), I would prefer to see parse-tree unchanged and if something needed different we can make copy  and keep registry for it and later have delete it. Parse-tree change will affect all future visitors. Moreover if change somehow left in unclear state(e.g. for loop iterator value is in the end state), so if later some flow tries to read parse-tree after population I think it will end up in confusing state (Particularly this concern is related to fScheme->GetIterId()->SetValue()- unless we set back initial value, which I don't see in this case)
        VhdlValue *value = new VhdlInt(j);
        fScheme->GetIterId()->SetValue(value);
        addLoopStatements(node, body_block);
    }
}

/*--------------------------------------------------------------------------------------------------------------------------*/
//                                      helper methods for Nucleus scope api get/set
/*--------------------------------------------------------------------------------------------------------------------------*/

// Push specified 'scope' design scope object into stack
void VerificVhdlDesignWalker::pushScope(V2NDesignScope* scope)             
{
    mScopeStack.push(scope);
    mCurrentScope = scope;
}

// Pop top scope object from stack
void VerificVhdlDesignWalker::popScope()
{
    INFO_ASSERT(! mScopeStack.empty(), "empty stack");
    UtVector<NUBase*> empty;
    mCurrentScope->gValueVector(empty);
    if (! empty.empty()) {
        empty.clear();
        UtVector<NUBase*>().swap(empty);
#ifdef DEBUG_VERIFIC_VHDL_WALKER
        std::cerr << "Warning: non empty value vector found, potential population problem" << std::endl;
#endif
    }
    mScopeStack.pop();
    delete mCurrentScope;
    if (! mScopeStack.empty()) {
        mCurrentScope = mScopeStack.top();
    }
}


// Function adds block scope and named declaration scope to V2NDesignScope in parallel to maintain Nucleus approach of dividing declarations and statements
// There are 3 possible cases for this function to be called:
// 1) We have both declarations and statements in the same block - e.g. VhdlBlockStatement, VhdlProcessStatement, VhdlArchitectureBody
// 2) We have declaration scope but don't have block scope - one case of this is generate instances (instance should be added to declaration scope and for generate statements this declaration scope is NUNamedDeclarationScope and there we don't have block scope - so just named declaration scope with empty block scope field is pushed 
// 3) We have block scope but don't have declaration scope - most block scopes (like loop statement) aren't allowed to have declarations inside but have only block scope for statements in that case we populate just block scope without declaraiton scope
// There is an assertion that 'block_scope' and 'decl_scope' can't be both NULL (in nucleus population flow this is true - exception is VerificVerilogSTBuildWalker case where we use V2NDesignScope to populate symbol table and use empty fields for both 'block_scope' and 'declaration_scope' (just V2NDesignScope::ScopeType is used)
void VerificVhdlDesignWalker::pushDesignScope(NUBlock* block_scope, NUNamedDeclarationScope* decl_scope)
{
    INFO_ASSERT(block_scope || decl_scope, "NUBlock or NUNamedDeclarationScope - one of the arguments should always exist");
    V2NDesignScope* current_scope = new V2NDesignScope(block_scope, mCurrentScope);
    if (decl_scope) current_scope->sDeclarationScope(decl_scope);
    pushScope(current_scope);
}

// get nearest NUNamedDeclarationScope object, if not found return NULL
NUNamedDeclarationScope* VerificVhdlDesignWalker::getNamedDeclarationScope()
{
    return Verific2NucleusUtilities::getNamedDeclarationScope(mCurrentScope);
}

// get nearest NUBlock object, if not found return NULL
NUBlock* VerificVhdlDesignWalker::getBlock()
{
    return Verific2NucleusUtilities::getBlock(mCurrentScope);
}

// get nearest NUTask object, if not found return NULL 
NUTask* VerificVhdlDesignWalker::getTask()
{
    return Verific2NucleusUtilities::getTask(mCurrentScope);
}

// get parent module object, if not found return NULL
NUModule* VerificVhdlDesignWalker::getModule()
{
    return Verific2NucleusUtilities::getModule(mCurrentScope);
}


// get declaration scope, if no named declaration scope, task, module scope found return NULL
NUScope* VerificVhdlDesignWalker::getDeclarationScope()
{
    return Verific2NucleusUtilities::getDeclarationScope(mCurrentScope);
}

// get block scope, if no block, task, module scope found return NULL
NUScope* VerificVhdlDesignWalker::getBlockScope()
{
    return Verific2NucleusUtilities::getBlockScope(mCurrentScope);
}

// get parent unit scope (task or module scope), if no unit scope found return NULL
NUScope* VerificVhdlDesignWalker::getUnitScope()
{
    return Verific2NucleusUtilities::getUnitScope(mCurrentScope);
}

// get parent module scope if no module scope found return NULL
NUScope* VerificVhdlDesignWalker::getModuleScope()
{
    return Verific2NucleusUtilities::getModuleScope(mCurrentScope);
}

///gets NUScope object based on current V2NDesignScope type
//This is non-recursive method which just returns current NUScope whatever found in the current V2NDesignScope. Here NUNamedDeclarationScope has a priority over NUBlock, so if NUNamedDeclarationScope specified with NUBlock the first one will be returned.
//If 'currentScope' argument is NULL member mCurrentScope is used
//FD : I'm not sure how much it's useful to have such function, just reimplemented in accordance to new approach
NUScope* VerificVhdlDesignWalker::getNUScope(V2NDesignScope* currentScope)
{
    V2NDesignScope* current_scope = NULL;
    if (currentScope) {
        // if current scope specified take it
        current_scope = currentScope;
    } else {
        // take current top of scope stack
        current_scope = mCurrentScope;
    }
    NUScope* nu_scope = NULL;
    if (current_scope->gDeclarationScope()) {
        nu_scope = current_scope->gDeclarationScope();
    } else {
        nu_scope = current_scope->gOwner();
    }
    return nu_scope;
}

// get parent always block object, if no parent always block found return NULL
NUAlwaysBlock* VerificVhdlDesignWalker::getAlwaysBlock()
{
    NUAlwaysBlock* always_block = NULL;
    // Proceed upward until we find always block or return NULL
    bool found = false;
    for (V2NDesignScope* scope = mCurrentScope; scope && !found; scope = scope->gParentScope()) {
        if (scope->getAlwaysBlock()) {
            always_block = scope->getAlwaysBlock();
            found = true;
        }
    }
    return always_block;
}

// get always block top block, if not found return NULL
NUBlock* VerificVhdlDesignWalker::getAlwaysTopBlock()
{
    NUBlock* top_block = NULL;
    // Proceed upward until we find block - which is always top block 
    bool found = false;
    for (V2NDesignScope* scope = mCurrentScope; scope && !found; scope = scope->gParentScope()) {
        if (scope->getAlwaysBlock() && scope->gOwner() && (NUScope::eBlock == scope->gOwner()->getScopeType())) {
            top_block = dynamic_cast<NUBlock*>(scope->gOwner());
            found = true;
        }
    }
    return top_block;
}

// get subprogram top block, if not found return NULL
NUBlock* VerificVhdlDesignWalker::getSubprogramTopBlock()
{
    NUBlock* subprogram_block = NULL;
    // Proceed upward until we find block which is subprogram top block 
    bool found = false;
    for (V2NDesignScope* scope = mCurrentScope; scope && !found; scope = scope->gParentScope()) {
        if (scope->isSubprogramBlock() && scope->gOwner() && (NUScope::eBlock == scope->gOwner()->getScopeType())) {
            subprogram_block = dynamic_cast<NUBlock*>(scope->gOwner());
            found = true;
        }
    }
    return subprogram_block;
}

// get loop top block, if not found return NULL
NUBlock* VerificVhdlDesignWalker::getLoopTopBlock()
{
    NUBlock* loop_block = NULL;
    // Proceed upward until we find block which is loop top block 
    bool found = false;
    for (V2NDesignScope* scope = mCurrentScope; scope && !found; scope = scope->gParentScope()) {
        if (scope->isLoopBlock() && scope->gOwner() && (NUScope::eBlock == scope->gOwner()->getScopeType())) {
            loop_block = dynamic_cast<NUBlock*>(scope->gOwner());
            found = true;
        }
    }
    return loop_block;
}

// Return nearest V2NDesignScope which corresponds to task, return NULL if not found
V2NDesignScope* VerificVhdlDesignWalker::getTaskV2NDesignScope()
{
    V2NDesignScope* task_scope = NULL;
    // Proceed upward until we find task scope 
    bool found = false;
    for (V2NDesignScope* scope = mCurrentScope; scope && !found; scope = scope->gParentScope()) {
        if (scope->gOwner() && (NUScope::eTask == scope->gOwner()->getScopeType())) {
            task_scope = scope;
            found = true;
        }
    }
    return task_scope;
}

/*--------------------------------------------------------------------------------------------------------------------------*/

bool VerificVhdlDesignWalker::blockShouldBeCreated(Array* stmtList) const
{
    // Component instantiations declaration are special statements from nucleus perspective as they get added to declaration scope 
    // Conditional signal assignements are also special case as they got populated as an continues assigns
    // in nucleus instead of usual (for other statements) parent NUBlock
    // So check list of statements not to create empty block if it contains only instantiation statements and conditional signal assignments (or is NULL).
    // FD: I think list might be extended in future. If it turns out there too many constucts which need to be 
    // populated outside of block may be we should check only for those which need to be populated under block, 
    // or rearrange stuff so that we refine correct logic for populating block statement and always block
    unsigned i = 0;
    VhdlStatement* stmt = NULL;
    FOREACH_ARRAY_ITEM(stmtList, i, stmt) {
        if (!stmt) continue;
        switch(stmt->GetClassId()) {
            case ID_VHDLCOMPONENTINSTANTIATIONSTATEMENT:
            case ID_VHDLCONDITIONALSIGNALASSIGNMENT:
                continue;
            default:
                return true;
        }
    }
    return false;
}

UtMap<VhdlTreeNode*, StringAtom*>*
VerificVhdlDesignWalker::getNameRegistry()
{
    return &mNameRegistry;
}

void VerificVhdlDesignWalker::normalizeRange(ConstantRange* access_range, const ConstantRange& declared_range, const UtString& netName, const SourceLocator& loc)
{
    // Check for reversed ranges, e.g. access_range is 1 to 4 but the declared range is 4 downto 0. 
    // This constitutes a NULL range. The Interra flow just reverses the range, issues a warning, and proceeds. That's what
    // we do here.
    if (isNullRange(access_range, &declared_range, loc, netName.c_str())) {
       SInt32 swap_lsb = access_range->getLsb(), swap_msb = access_range->getMsb();
       access_range->setLsb(swap_msb);
       access_range->setMsb(swap_lsb);
    }
    
    // check for range overlap
    // copied same logic which normalize uses internally
    if (declared_range.contains(access_range->getMsb()) && declared_range.contains(access_range->getLsb())) {
        access_range->normalize(&declared_range);
    } else {
        // report warning and proceed without normalize, in any case user can still demote this to error.
        UtString range_str;
        SInt32 msb = access_range->getMsb();
        SInt32 lsb = access_range->getLsb();
        UtString index_str; 
        if (msb == lsb) {
          index_str << "(" << msb << ")";
        } else {
          index_str << "(" << msb << ((msb>lsb)? " downto ":" to ") << lsb << ")";
        }

        mNucleusBuilder->getMessageContext()->OutOfRangeIndexAccessDetected(&loc, netName.c_str(), index_str.c_str(), declared_range.format(&range_str)); 
    }
}

/**
 * @brief Helper function for assignments. Returns the allocated constraint of the prefix 
 * for VhdlIndexedNames. It is up to the caller to free the allocated constraint.
 *
 * This may need to be fleshed out for arrays of records. For now, it works for slices of
 * vectors - it will return the constraint on the vector as it was declared.
 */

VhdlConstraint* VerificVhdlDesignWalker::getPrefixConstraint(VhdlExpression* lhs, VhdlDataFlow* df)
{
  VhdlConstraint *prefix_constraint = NULL;
  if (lhs->IsArraySlice()) {
    VhdlIndexedName *slice = dynamic_cast<VhdlIndexedName*>(lhs);
    INFO_ASSERT(slice, "Expected VhdlIndexed name");
    prefix_constraint = slice->EvaluatePrefixConstraint(df);
  }
  return prefix_constraint;
}

// Compute the constraint on the left hand side of an assignment, given the rhs,
// and the dataflow. If the left hand side is a slice of an array, with a
// non-constant range, then the constraint of the prefix is computed.
// This allows aggregate expressions on the rhs to be computed correctly,
// and enables nucleus population for assignments to non-constant slices.
//
// Note: The constructed constraint is allocated by this method, and must be freed
// by the caller.
VhdlConstraint* VerificVhdlDesignWalker::getTargetConstraint(VhdlExpression* lhs, VhdlExpression* rhs, VhdlDataFlow* df)
{
  INFO_ASSERT(lhs, "Unexpected NULL lhs passed to 'getTargetConstraint'");
  
  // Obtain constraint on target of assignment
  VhdlConstraint* target_constraint = lhs->EvaluateConstraintInternal(df, 0);

  (void)rhs;
#if 0
  // RBS: I'm not sure what the purpose of this code is. It doesn't seem to make
  // any difference for the rushc_vhdl suite of tests, so I'm leaving it out for now.
  if (rhs) {
    VhdlValue* val = rhs->Evaluate(target_constraint, df, 0);
    if (!val) {
      //correct data flow and target constraint
      if (df) df->SetNonconstExpr();
      if (!target_constraint) {
        target_constraint = lhs->EvaluateConstraintInternal(df, 0);
      }
      if (target_constraint && !target_constraint->IsUnconstrained()) {
        // RBS: Do we need this checking?
        VhdlConstraint* value_constraint = rhs->EvaluateConstraintInternal(df, target_constraint);
        if (value_constraint && !value_constraint->IsUnconstrained())
          target_constraint->CheckAgainst(value_constraint, rhs, df);
        delete value_constraint;
      }
    }
  }
#endif

  // If the target is a slice with non-constant range, then target_constraint
  // can be NULL. In that case, get the constraint on the prefix, which should
  // always be known.
  if (!target_constraint) {
    target_constraint = getPrefixConstraint(lhs, df);
  }

  return target_constraint;
}

// Create a hierarchical name for an object (signal/variable/constant) declared in
// a package. The object will be declared in a nested named declaration scope
// within the top module. 
//
// The name is stored in an AtomArray:
// 0 - module name
// 1 - library name
// 2 - package name
// 3 - net name

void VerificVhdlDesignWalker::createHierName(VhdlIdDef* id, AtomArray& netHierName)
{
  const char* name = id->OrigName();
  StringAtom* netName = mNucleusBuilder->gCache()->intern(name);
  NUModule* topModule = mNucleusBuilder->getTopModule();
  StringAtom* moduleAtom = mNucleusBuilder->gCache()->intern(UtString(topModule->getName()->str()));
  StringAtom* libAtom =
    mNucleusBuilder->gCache()->intern(UtString(id->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit()->Owner()->Name()));
  StringAtom* packAtom =
    mNucleusBuilder->gCache()->intern(UtString(id->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit()->Name()));
  netHierName.push_back(moduleAtom);
  netHierName.push_back(libAtom);
  netHierName.push_back(packAtom);
  netHierName.push_back(netName);
}

bool VerificVhdlDesignWalker::isConstantRecord(VhdlValue* value)
{
    bool is_constant_record = value && value->IsConstant() && value->IsComposite();
    return is_constant_record;
}

// Normalize an index expression to [m:0] bounds, given original [lhs:rhs] bounds.
NUExpr* VerificVhdlDesignWalker::getNormalizedIndexExpr(NUExpr* index_expr, SInt32 lhs, SInt32 rhs)
{
  NUExpr* result = NULL;
  NUExpr* offset = NUConst::createKnownIntConst(rhs, 32, false, index_expr->getLoc());
  offset->setSignedResult(true);
  if (lhs > rhs) {
    result = new NUBinaryOp(NUOp::eBiMinus, index_expr, offset, index_expr->getLoc());
  } else {
    result = new NUBinaryOp(NUOp::eBiMinus, offset, index_expr, index_expr->getLoc());
  }
  result->setSignedResult(true);
  return result;
}

bool VerificVhdlDesignWalker::isImplicitIntVal(VhdlValue* val)
{
    // Try to get non-implicit integers (this looks like it's a bug setting integer flag in Verific Evaluate flow).
    // One noticed case where it's not being set is a pragma conversion function 'conv_integer'
    // Workaround: make sure string representation matches integer representation
    char* image = val->Image();
    UtString fromStr(image);
    delete image;
    UtString fromInt;
    fromInt << val->Integer(); 
    return (fromInt == fromStr);
}

// create an assigment (blocking or continuous) lval<-rval, and insert it in the current context
// context:
//   task:                              create blocking assign,
//   module with always block open:     create blocking assign, 
//   module with no always block open:  create continuous assign
// returns true on success
bool VerificVhdlDesignWalker::createAssignInCurrentContext(NULvalue* lval, NUExpr* rval, const SourceLocator& loc)
{
  // TODO: what about initial blocks?


  if (getAlwaysBlock() || getTask()) {
    // this is a blocking assign context
    NUScope* block_scope = getBlockScope();
    NUBlock* block = dynamic_cast<NUBlock*>(block_scope);
    if ( ! block ){
      mNucleusBuilder->getMessageContext()->Verific2NUErrorDuringPopulation(&loc, "failed to identify the block for assignment");
      setError(true);
      return false;
    }
    NUStmt* stmt = new NUBlockingAssign(lval, rval, false, loc, false);
    block->addStmt(stmt);
  } else {
    // create continuous assign
    StringAtom* block_name = getModule()->newBlockName(mNucleusBuilder->gCache(), loc);
    NUContAssign* contassign = new NUContAssign(block_name, lval, rval, loc);
    getModule()->addContAssign(contassign);
  }
  return true;
}

bool VerificVhdlDesignWalker::getCompositeFieldIndex(VhdlSelectedName* node, VhdlIdDef* suffixId, NUBase* prefixNet, UInt32* fieldIndex)
{
    bool got_field_index = mVerificToNucleusObject.getCompositeField(node, fieldIndex);
    if (!got_field_index) {
        INFO_ASSERT(suffixId, "NULL suffix id specified");
        INFO_ASSERT(prefixNet, "NULL prefix net specified");
        NUCompositeNet* cNet = NULL;
        if (NUCompositeIdentRvalue* identExpr=dynamic_cast<NUCompositeIdentRvalue*>(prefixNet)) {
            cNet = identExpr->getIdent();
        } else if (NUCompositeIdentLvalue* identLvalue=dynamic_cast<NUCompositeIdentLvalue*>(prefixNet)) {
            cNet = identLvalue->getCompositeNet();
        } else if (NUCompositeNet* compositeNet = dynamic_cast<NUCompositeNet*>(prefixNet)) {
            cNet = compositeNet;
        } 
        INFO_ASSERT(cNet, "Unable to get composite net from expression");
        // There is at least 1 known case where getCompositeField(&node, &fieldIndex) will be unable to get fieldIndex,
        // it's in case the prefix is function return case, but alternative version should succeed in that case.
        got_field_index  = getCompositeField(cNet, suffixId, fieldIndex);
    }
    return got_field_index;
}

bool VerificVhdlDesignWalker::isConstantNode(VhdlIdDef* id)
{
    //AF: Catch constants evaluated during static elaboration and user defined constants
    //The evaluated constants of interest are constants and generics and enums
    //Apply this to rhs or unknown but never lhs. (Verific will
    //assign lhs sides to constants, but we need the wiring info).
    //
    //The types we are looking for are:
    //
    //Constants, Generics of types:
    //scalar: integer, enumeration, physical (unsupported), floating.
    //composite, array, index, discrete range, record.
    //
    //access, incomplete.


    // Steve Lim 2013-11-05 A reference to a primary port with a default
    // value cannot be replaced with that value except when the entity's
    // architecture is being elaborated as a uniqufied instance where there
    // is no driver or sink for that port. Added the check that the VhdlIdRef's
    // VhdlIdDef node is not a port.
    // Testcases: beacon_expression/reg_op6_nat
    //            beacon_ignore/ent_port
    // rjc I think: why is this so complex to decide if something is a constant?
    bool is_constant_node = ((id->IsConstant()           || // main  constant check routine, works for most constant types
                id->IsEnumerationLiteral() || // needed for literal constants like 'true', id->IsConstant() isn't true for them
                id->IsSubprogram()            // needed for constant functions, somehow id->IsConstant() isn't true for them
                ) && !mCurrentScope->isPrefix()); // The isPrefix() guard is used here in cases where the caller (visitor)
                                                  // wants to non handle item as a constant (see ficistors for arrayIndex and arraySlice)
    return is_constant_node;
}

// This method examines \a node, if it is a function call to ieee.numeric_std.std_match then this method will return true, otherwise it
// returns false.  Note the return value does not indicate success/failure, only that the std_match function was recognized.
//
// The supported call to std match is of the form:
//   bool result = std_match(v8, "1-001--0");
// where result is true if v8 has a value where the indicated 0s and 1s match, and the positions with '-' are not compared.  so if v8 has
// the value "10001000" the result is true, if v8 is "01001000" then the result is false because v8[7] (the left most bit) does not match,
// (in both cases bit v8[6], and v8[2:1] are ignored during the comparison).
// 
// If std_match is recognized then this method attemts to convert it into equivalent logic.  This is done instead of relying on the
// population of the ieee version of the function because the behaviour depends upon the interpretion of bit values other than 0 and 1.
// Unsupported values listed in the match pattern (or non-constant match pattern) will result in a failed to populate error message.
// if population is correct then the resulting rvalue NUExpr is placed in mCurrentScope->sValue();
bool VerificVhdlDesignWalker::attemptConversion_std_match(const VhdlIndexedName& node){
  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  const UtString look_for_package("numeric_std");
  const UtString look_for_function("std_match");
  bool right_function_wrong_package = false;
  bool valid_function = isFunctionFromSpecificIEEEPackage(node, look_for_package, look_for_function, &right_function_wrong_package);

  if ( !valid_function ) {
    if ( right_function_wrong_package ) {
      // found that this is an ieee function with the right name but from wrong package, but we consider this an error
      mNucleusBuilder->getMessageContext()->UnsupportedFunction(&loc, look_for_function.c_str(), look_for_package.c_str());
      mCurrentScope->sValue(NULL);
      setError(true);
      return true;
    }
    return false;
  }

    // getting here means that this is the recognized function, from here on out we must return true indicating this is a recognized
    // function.  If there is an error during population then setError() is called.
    // 
    // find the constant argument and the non-const arg (name them: c and v) (note both might be constant)
    // generate a mask of the non dash bits of that constant
    VhdlDiscreteRange* assoc = 0;
    VhdlDiscreteRange* constant_term= NULL;
    VhdlDiscreteRange* variable_term= NULL;
    UInt32 index_of_const_arg = 0;
    unsigned ii = 0;
    FOREACH_ARRAY_ITEM(node.GetAssocList(), ii, assoc) {
      bool this_term_is_const = assoc->IsConstant();
      if ( ! this_term_is_const ){
        VhdlValue* value = assoc->Evaluate(0, mDataFlow, 0);
        if ( value && value->IsConstant() ){
          this_term_is_const = true;
        }
        delete value;
      }
      if ( this_term_is_const ){
        index_of_const_arg = ii;
        if ( constant_term ){
          variable_term = constant_term; // both terms are constants, so handle the first argument as if it were a variable
        }
        constant_term = assoc;
      } else {
        variable_term = assoc;
      }
    }

    if ( (2 != ii ) || !constant_term ){
      UtString msg ("at least one of the two arguments must be a constant");
      mNucleusBuilder->getMessageContext()->UnsupportedFunction(&loc, "std_match", msg.c_str());
      mCurrentScope->sValue(NULL);
      setError(true);
      return true;  // true because recognized as the ieee std_match function, setError(true) because it was not populated
    }

    VhdlValue* value = constant_term->Evaluate(0, mDataFlow, 0);
    if (!value || (!value->IsConstant()) ) {
      delete value;
      UtString msg ("unable to extract constant value");
      mNucleusBuilder->getMessageContext()->UnsupportedFunction(&loc, "std_match", msg.c_str());
      mCurrentScope->sValue(NULL);
      setError(true);
      return true;  // true because recognized as the ieee std_match function, setError(true) because it was not populated
    }

    //FD TODO check constraint, don't use value->NumBits()
    UInt32 bitsize = (value->IsIntVal() ? 32 : value->NumBits());
    if ( 64 < bitsize ) {
      delete value;
      UtString msg ("bitsize is greater than 64 bits: ");
      msg << bitsize;
      mNucleusBuilder->getMessageContext()->UnsupportedFunction(&loc, "std_match", msg.c_str());
      mCurrentScope->sValue(NULL);
      setError(true);
      return true;  // true because recognized as the ieee std_match function, setError(true) because it was not populated
    }
    UInt64 mask = 0;
    UInt64 const_val = 0;

    char* image = value->Image(); //     if (Strings::compare(image, "'-'")) {return 0 ;} 
    UtString constValStr(image);
    Strings::free(image);
    bool needs_mask_conversion = (UtString::npos != constValStr.find_first_of("-LlHh", 0, 5));

    if ( needs_mask_conversion ) {
      // remove quotes on image
      if ( ( constValStr[0] == '"' ) || ( constValStr[0] == '\'' ) ){
        constValStr.erase(0,1);
        UInt32 last_pos = constValStr.length()-1;
        if ( ( constValStr[last_pos] == '"' ) || ( constValStr[last_pos] == '\'' ) ){
          constValStr.erase(last_pos,1);
        }
      }
        
      if ( constValStr.length() != bitsize ){
        delete value;
        UtString msg ("constant expression is not in binary format: ");
        msg << constValStr;
        mNucleusBuilder->getMessageContext()->UnsupportedFunction(&loc, "std_match", msg.c_str());
        mCurrentScope->sValue(NULL);
        setError(true);
        return true;  // true because recognized as the ieee std_match function, setError(true) because it was not populated
      }
      UInt32 index = bitsize-1;
      for (index=0; index < bitsize; ++index ){
        switch ( constValStr[index] ){
        case '-':{
          mask = mask << 1; // appends 0
          const_val = const_val << 1; // appends 0
          break;
        }

        case '0':
        case 'L':
        case 'l':{
          // these values must match '0'
          mask = (mask << 1) | 1; // appends 1
          const_val = const_val << 1; // appends 0
          break;
        } 

        case '1':
        case 'H':
        case 'h':{
          // these values must match '1'
          mask = (mask << 1) | 1; // appends 1
          const_val = (const_val << 1) | 1; // appends 1
          break;
        }
        default:{
          delete value;
          // according to the synthesis spec, any other value (metalogical or Z) should result in a non-match, so we just return an
          // expression for false here.
          NUExpr* nu_false = NUConst::createKnownIntConst(0, 1, false, loc);
          mCurrentScope->sValue(nu_false);
          return true;  // true because recognized as the ieee std_match function
        }
        }
      }
    } else {
      // must be an integer value (no '-' so create mask for all bits, and use value directly)
      mask =  (1 << bitsize)-1;
      const_val = value->Integer();
    }
    delete value; value = NULL;


    NUExpr* nu_var = NULL;
    {
      V2NDesignScope::ContextType cType = mCurrentScope->gContext();
      mCurrentScope->sContext(V2NDesignScope::RVALUE);
      variable_term->Accept(*this);
      mCurrentScope->sContext(cType);
      nu_var = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
      if (! nu_var) {
        UtString msg("Unable to create expression for ");
        msg << ((index_of_const_arg == 1) ? "first" : "second");
        msg << " argument of std_match";
        mNucleusBuilder->getMessageContext()->Verific2NUErrorDuringPopulation(&loc, msg.c_str());
        mCurrentScope->sValue(NULL);
        setError(true);
        return true;  // true because recognized as the ieee std_match function, setError(true) because it was not populated
      }
    }


    // implement ( (v & mask) == (c & mask) )
    CopyContext cc(NULL, NULL);
    NUExpr* nu_mask_l = NUConst::createKnownIntConst(mask, bitsize, false, loc);
    NUExpr* nu_mask_r = nu_mask_l->copy(cc);
    NUExpr* nu_const_r = NUConst::createKnownIntConst(const_val, bitsize, false, loc);

    NUExpr* op_l = new NUBinaryOp(NUOp::eBiBitAnd, nu_var, nu_mask_l, loc);
    NUExpr* op_r = new NUBinaryOp(NUOp::eBiBitAnd, nu_const_r, nu_mask_r, loc);
    NUExpr* op_result = new NUBinaryOp(NUOp::eBiEq, op_l, op_r, loc);
    mCurrentScope->sValue(op_result);

    return true;
}

bool VerificVhdlDesignWalker::attemptConversion_is_x(const VhdlIndexedName& node){
  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

  const UtString look_for_package("std_logic_1164");
  const UtString look_for_function("is_x");
  bool right_function_wrong_package = false;
  bool valid_function = isFunctionFromSpecificIEEEPackage(node, look_for_package, look_for_function, &right_function_wrong_package);

  if ( !valid_function ) {
    if ( right_function_wrong_package ) {
      // found that this is an ieee function with the right name but from wrong package, but we consider this an error
      mNucleusBuilder->getMessageContext()->UnsupportedFunction(&loc, look_for_function.c_str(), look_for_package.c_str());
      mCurrentScope->sValue(NULL);
      setError(true);
      return true;
    }
    return false;
  }
  // This function almost always returns false

  // getting here means that this is the recognized function, from here on out we must return true indicating this is a recognized
  // function.  If there is an error during population then setError() is called.
  VhdlDiscreteRange* assoc = 0;
  unsigned ii = 0;
  bool has_x = false;
  FOREACH_ARRAY_ITEM(node.GetAssocList(), ii, assoc) {
    INFO_ASSERT((ii < 1), "too many arguments found for is_x()");
    VhdlValue* value = assoc->Evaluate(0, mDataFlow, 0);
    has_x = ( value && value->HasX() );
    delete value;
  }

  // create expression for the return value of this function
  NUExpr* nu_false = NUConst::createKnownIntConst((has_x ? 1 : 0), 1, false, loc);
  mCurrentScope->sValue(nu_false);
  return true;  
}

// looks at \a node to see if it is a function from ieee.check_for_this_package_name.check_for_this_function_name, if so return true
//  if it does not exactly match then return false.  If false returned then \a right_function_wrong_package is set to true if all but the
// package name matched, indicating that this is an overloaded function that might not have been handled
bool VerificVhdlDesignWalker::isFunctionFromSpecificIEEEPackage(const VhdlIndexedName& node, const UtString& check_for_this_package_name, const UtString& check_for_this_function_name, bool* right_function_wrong_package ){
  VhdlSubprogramId* sId = dynamic_cast<VhdlSubprogramId*>(node.GetId());
  if ( right_function_wrong_package ) {
    (*right_function_wrong_package) = false;
  }
  if ( ! sId ) {
    return false;               // this is not a function call
  }

  if (! Verific2NucleusUtilities::isIeeeSubprogram(sId) ){
    return false;              // this cannot be the function we are looking for
  }

  UtString function_name = sId->Name();
  if ( check_for_this_function_name != function_name ){
    return false;           // this is not the function we are looking for, note that the name should not be uniquified so it should have
                            // been identified in Verific2NucleusUtilities::isIEEEPackageBuiltinFunction(VhdlIndexedName* call_inst))
  }
  UtString packageName = sId->GetOwningScope()->GetContainingPrimaryUnit()->Name();
  if ( check_for_this_package_name != packageName ) {
    // is ieee.<something>.check_for_this_function_name where something != check_for_this_package_name,
    // so this is not the function you are looking for, but this is probably an error because the function has the right name and is from
    // an IEEE library.  Tell caller about it.
    // this an error because this is from an IEEE library with the correct function name
    if ( right_function_wrong_package ) {
      (*right_function_wrong_package) = true;
    }
    return false;               // right function name, but wrong package
  }
  return true;
}

int VerificVhdlDesignWalker::containsRecords(VhdlIdDef* typeId)
{
  if (!typeId)
    return 0;
  if (typeId->IsRecordType())     return 1;
  if (typeId->IsArrayType())
    return containsRecords(typeId->ElementType());
  return 0;
}



} // namespace verific2nucleus 

