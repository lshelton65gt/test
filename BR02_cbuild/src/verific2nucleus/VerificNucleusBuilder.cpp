// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// Context 
#include "compiler_driver/CarbonContext.h"

// Project headers
#include "verific2nucleus/VerificNucleusBuilder.h"
#include "verific2nucleus/V2NDesignScope.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"
#include "verific2nucleus/VerificTicProtectedNameManager.h"

// Nucleus API headers
#include "nucleus/NUBitNet.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUCopyContext.h"
#include "nucleus/NUBitNetHierRef.h"
#include "nucleus/NUVectorNetHierRef.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUSysFunctionCall.h"
#include "nucleus/NUSysRandom.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUCompositeLvalue.h"


// Other project api headers
#include "util/AtomicCache.h"
#include "hdl/HdlVerilogPath.h"
#include "symtab/STFieldBOM.h"
#include "symtab/STBranchNode.h"
#include "iodb/IODBDesignData.h"
#include "iodb/IODBUserTypes.h"
#include "iodb/IODBNucleus.h"
#include "util/DynBitVector.h"
#include "util/UtStringUtil.h"
#include "bdd/BDD.h"
#include "reduce/Fold.h"
#include "hdl/HdlVerilogPath.h"
#include "util/ArgProc.h"

// Standard headers
#include <iostream>
#include <utility>
#include <sstream>
#include <iomanip>
#include <cmath>

// Verific headers
#include "veri_tokens.h"
#include "vhdl_tokens.h"
#include "VeriModule.h"

//#define DEBUG_VERIFIC_NUCLEUS_BUILDER 1


namespace verific2nucleus {

// The callback invoked by MsgContext before a message is printed. 
// Used to limit messages based on user supplied value.
static eCarbonMsgCBStatus sMsgCallback(CarbonClientData clientData,
                                       CarbonMsgSeverity severity,
                                       int msgNumber, const char* text, unsigned int len)
{
  VerificNucleusBuilder* populator = (VerificNucleusBuilder*)clientData;
  return populator->gMsgCountManager()->getMsgCBStatus(severity, msgNumber, text, len);
}

VerificNucleusBuilder::VerificNucleusBuilder(NUDesign * design,
                                             AtomicCache * atomicCache,
                                             SourceLocatorFactory& sourceLocatorFactory,
                                             STSymbolTable * symbolTable,
                                             NUNetRefFactory * netRefFactory, 
                                             IODBNucleus * iODB,
                                             MsgContext* msgContext,
                                             CarbonContext* carbonContext,
                                             bool synthSpecified,
                                             const char* modulePragmas [],
                                             const char* netPragmas[],
                                             const char* functionPragmas[],
                                             IODBDesignDataSymTabs * symTabs,
                                             ArgProc* arg,
                                             VerificTicProtectedNameManager* nameManager)
    : mDesign(design)
    , mAtomicCache(atomicCache)
    , mSourceLocatorFactory(sourceLocatorFactory)
    , mSymbolTable(symbolTable)
    , mNetRefFactory(netRefFactory)
    , mIODB(iODB)
    , mUserTypeFactory(mIODB->userTypeFactory())
    , mIsTopLevel(true)
    , mTopModule(0)
    , mTopModuleName("")
    , mIsRoot(true)
    , mSynthFlagWasSpecified(synthSpecified)
    , mModulePragmas(modulePragmas)
    , mNetPragmas(netPragmas)
    , mFunctionPragmas(functionPragmas)
    , mSymTabs(symTabs)
    , mModuleRegistry()
    , mModuleNameRegistry()
    , mVeModuleRegistry()
    , mVhModuleRegistry()
    , mMessageContext(msgContext)
    , mCarbonContext(carbonContext)
    , mArg(arg)
    , mMsgCB(0)
    , mFold(0)
    , mError(false)
    , mEdgeNetMap()
    , mTicProtectedNameMgr(nameManager)
{
    // Retrieve max message repeat count command line argument, and
    // place it in the design manager.
    SInt32 maxMsgRepeatCount;
    mArg->getIntLast(CarbonContext::scMaxMsgRepeatCount, &maxMsgRepeatCount);
    mMsgCountManager.setMaxMsgRepeatCount(maxMsgRepeatCount);

    // Register message callback with MsgContext. It calls the callback before
    // issuing a message.
    mMsgCountManager.setMsgContext(mMessageContext);
    mMsgCB = new MsgCallback(sMsgCallback, this);
    mMessageContext->addMessageCallback(mMsgCB);

    mFold = new Fold(mNetRefFactory, mArg, mMessageContext, mAtomicCache, mIODB, false, eFoldPreserveReset );
}
    
VerificNucleusBuilder::~VerificNucleusBuilder()
{
    // Remove message callback that was added during construction.
    mMessageContext->removeMessageCallback(mMsgCB);
    delete mMsgCB;
    mMsgCB = NULL;

    delete mFold;
}

NUNetRefFactory* VerificNucleusBuilder::gNetRefFactory()
{
    return mNetRefFactory;
}

IODBNucleus* VerificNucleusBuilder::gIODB()
{
    return mIODB;
}

AtomicCache* VerificNucleusBuilder::gCache()
{
    return mAtomicCache;
}

MsgContext* VerificNucleusBuilder::getMessageContext()
{
    return mMessageContext;
}

VerificTicProtectedNameManager* VerificNucleusBuilder::gTicProtectedNameMgr()
{
    return mTicProtectedNameMgr;
}

NUBase* VerificNucleusBuilder::gModule(const UtString& name)
{
    if (mModuleRegistry.find(name) != mModuleRegistry.end()) {
        return mModuleRegistry[name];
    }
    return 0;
}

NUBase* VerificNucleusBuilder::gModule(VeriTreeNode* ve_module)
{
    if (mVeModuleRegistry.find(ve_module) != mVeModuleRegistry.end()) {
        return mVeModuleRegistry[ve_module];
    }
    return 0;
}


UtString VerificNucleusBuilder::gModuleName(VeriModule* name)
{
    if (mModuleNameRegistry.find(name) != mModuleNameRegistry.end()) {
        return mModuleNameRegistry[name];
    }
    return "";
}


bool
VerificNucleusBuilder::hasError()
{
    return mError;
}

void
VerificNucleusBuilder::setError(bool error)
{
    mError = error;
}


/*************************************************************************************************************/
/****************************************** UTILITIES ********************************************************/
/*************************************************************************************************************/

void VerificNucleusBuilder::reportUnsupportedLanguageConstruct(const SourceLocator& sourceLocation, const UtString& msg)
{
    // report message
    mMessageContext->UnsupportedLanguageConstruct(&sourceLocation, msg.c_str());
    // this sets error state,
    setError(true);
}

//! Convert IN, OUT or INOUT into appropriate mode for Verilog parameter
NUTF::CallByMode VerificNucleusBuilder::getParamMode (const NUNet* net)
{
  if (net->isInput ())
    return NUTF::eCallByValue;
  else if (net->isOutput ())
    return NUTF::eCallByCopyOut;
  else if (net->isBid ())
    return NUTF::eCallByCopyInOut;
  else
    NU_ASSERT (0, net);

  return NUTF::eCallByAny;
}

void
VerificNucleusBuilder::verboseUdp(NUModule* module, const SourceLocator& loc)
{
    if ( not loc.isTicProtected() ) {
        //  // never print a hint that there is a udp within a protected region
        int verbosity;
        mArg->getIntLast("-verboseUDP", &verbosity );

        if( verbosity >= 1 ) {
            // Message for UDP parsing.
            mMessageContext->ParsedUDP(&loc, module->getName()->str() );

            if( verbosity >= 2 ) {
                // Decompile parsed UDPs for debugging.
                UtString buf;
                module->compose(&buf, NULL,  0, true);
                UtIO::cout() << buf.c_str() << UtIO::endl;
            }
        }
    }
}

void 
VerificNucleusBuilder::lvalueActualFixup(NULvalue *lvalue, UInt32 formal_width, const UInt32 *offset, NULvalue **result_lvalue)
{
  *result_lvalue = lvalue;

  // there are three possibilities,
  // 1. this instance had no range declared for it (thus it is not an
  //    array of instances) so no fixup required
  // 2. this actual is the same width as the formal, and all of it is to be
  //    connected to the formal in each instance, no fixup required (bug2037)
  // 3. this actual is wider than a single bit, and only part of it is
  //    to be connected to the formal

  if (not offset) {
    return;            // case 1
  }

  UInt32 actual_width = lvalue->determineBitSize();
  if ( actual_width == formal_width ){
    return;
  }
  SourceLocator loc = lvalue->getLoc();

  // determine which bits of lvalue are to be connected to this
  // instance (defined by *offset and formal_width).
  const ConstantRange required_range(formal_width-1,0);

  // build a new lvalue 
  NUExpr *index = NUConst::create(false, ((*offset)*formal_width),  32, loc);
  *result_lvalue = new NUVarselLvalue(lvalue, index, required_range, loc);
  *result_lvalue = mFold->fold(*result_lvalue); // simplify actuals like i[3:0][1] -> i[1]
}

void
VerificNucleusBuilder::rvalueActualFixup(NUExpr *rvalue, UInt32 formal_width, const UInt32 *offset, NUExpr **result_rvalue)
{
  *result_rvalue = rvalue;

  // there are three possibilities,
  // 1. this instance had no range declared for it (thus it is not an
  //    array of instances) so no fixup required
  // 2. this actual is the same width as the formal, and all of it is to be
  //    connected to the formal in each instance, no fixup required
  // 3. this actual is wider than a single bit, and only part of it is
  //    to be connected to the formal

  if (not offset) {
    return;            // case 1
  }

  UInt32 actual_width = rvalue->determineBitSize();
  if ( actual_width == formal_width ){
    return;
  }
  SourceLocator loc = rvalue->getLoc();

  // determine which bits of lvalue are to be connected to this
  // instance (defined by *offset and formal_width).
  const ConstantRange required_range(formal_width-1,0);

  // build a new rvalue
  NUExpr *index = NUConst::create(false, ((*offset)*formal_width),  32, loc);
  *result_rvalue = new NUVarselRvalue(rvalue, index, required_range, loc);
  (*result_rvalue)->resize(formal_width);
  *result_rvalue = mFold->fold(*result_rvalue); // simplify actuals like i[3:0][1] -> i[1]
}

NUExpr* VerificNucleusBuilder::limitSelectIndexExpr(NUMemoryNet* mem, SourceLocator& loc,
                                              NUExpr* selExpr)
{
  bool was_truncated = false;
  selExpr = selExpr->limitMemselIndexExpr(&was_truncated, mMessageContext, loc);

  if ( was_truncated and mem->isDeclaredWithNegativeIndex() )
  {
    mMessageContext->NegativeIndicesWithTruncation(mem);
  }

  return selExpr;
}

void VerificNucleusBuilder::adjustSelExpr(SourceLocator& loc, NUMemoryNet* mem,
                                         bool limitIndex, NUExpr** bitSelExpr)
{
    (*bitSelExpr)->resize((*bitSelExpr)->determineBitSize()); // self-determined
    if (limitIndex) *bitSelExpr = limitSelectIndexExpr(mem, loc, *bitSelExpr);
}


NUVarselRvalue*
VerificNucleusBuilder::genVarselRvalue(NUNet* net, NUExpr* rval, NUExpr* selExpr,
                          const SourceLocator& loc,
                          bool retNullForOutOfRange, SInt32 adjust)
{
  NUExpr* sel = normalizeSelect(net, selExpr, adjust, retNullForOutOfRange);
  NUCLEUS_CONSISTENCY(sel, loc, "unable normalize select expression");
  return new NUVarselRvalue(rval, sel, loc);
}

NUVarselLvalue*
VerificNucleusBuilder::genVarselLvalue(NUNet* net, NULvalue* lval, NUExpr* selExpr,
                          const SourceLocator& loc,
                          bool retNullForOutOfRange, SInt32 adjust)
{
  NUExpr* sel = normalizeSelect(net, selExpr, adjust, retNullForOutOfRange);
  NUCLEUS_CONSISTENCY(sel, loc, "unable normalize select expression");
  return new NUVarselLvalue(lval, sel, loc);
}


NUExpr*
VerificNucleusBuilder::normalizeSelect(NUNet* net, NUExpr* selExpr, SInt32 adjust, bool retNullForOutOfRange)
{
  NUExpr* sel = selExpr;
  if (net->isMemoryNet()) {
    NUMemoryNet* mn = net->getMemoryNet();
    sel = mn->normalizeSelectExpr(selExpr, 0, adjust, retNullForOutOfRange);
  }
  return sel;
}

//// TODO probably need to put some common place for Verific/Inttera (this function is copied from localflow/Populate.cxx:1496 )
bool VerificNucleusBuilder::reduceCaseItems(const CaseItemStrings& caseItemStrings)
{
  // Go through the constant value vectors and create the appropriate
  // BDD, and check the sel expression for any constant 
  BDDContext bddContext;

  // Initially, the value of this equation is false.
  BDD resultBDD = bddContext.val0();
  BDD constantOne = bddContext.val1();
  // Go through this constant value vector and create the sum of products for all
  // conditions associated with this case statement.

  // We build a BDD with the following rule:
  //   1. iPOS is the identifier for position POS
  //   2. Negate the identifier if there is a '0' in that position.
  //   3. Ignore 'x'.
  //
  // The case statement:
  //   case(sel)
  //   01: BLAH
  //   xx: BLAH
  //   endcase
  // will create a BDD with the following equation:
  //   ((~i0) and i1) or (true)
  for (CaseItemStrings::const_iterator iter = caseItemStrings.begin(),
         e = caseItemStrings.end(); iter != e; ++iter)
  {
    const UtString& constStr = (*iter);
#ifdef DEBUG_BDD
    UtIO::cout() << "string = " << constStr << UtIO::endl;
#endif
    // Initially, the value of a single term is true.
    BDD itemBDD = constantOne;
    UInt32 index;
    UtString ident;
    UtString::const_iterator s;
    for (s = constStr.begin(), index = 0; s != constStr.end(); ++s, ++index)
    {
      const char digit = *s;
      // If the bit is 'x', it's because it's value is x/z for casex
      // or z for casez. Let '1' go into BDD to indicate that any value (0/1)
      // will match.
      if (digit != 'x')
      {
	// Create the identifier for this select bit
        ident.clear();
	ident << "i" << index;
        
	// Create the bdd for this digit
	BDD digitBDD = bddContext.bdd(ident);
	if (digit == '0') {
          // If this item is '0', invert the BDD.
	  digitBDD = bddContext.opNot(digitBDD);
        }
        
	// And it to the item BDD
        itemBDD = bddContext.opAnd(itemBDD, digitBDD);
      }
    } // for

    // The item BDD has been created, OR it into the result
    resultBDD = bddContext.opOr(resultBDD, itemBDD);

#ifdef DEBUG_BDD
    UtIO::cout() << "BDD size = " << bddContext.size(resultBDD) << UtIO::endl;
#endif
  } // for

  // Check if the BDD indicates all possibilities are covered. This
  // occurs if the bdd is 1.
  bool result (resultBDD == constantOne);
  return result;
} // bool Populate::reduceCaseItems

void VerificNucleusBuilder::computeFullCase(NUCase** the_stmt, bool hasFullCaseDirective)
{
  (*the_stmt)->eliminateUnnecessaryCaseBits(mFold);
  
  // Store the flags
  (*the_stmt)->putUserFullCase(hasFullCaseDirective);

  NUExpr* sel = (*the_stmt)->getSelect();

  // Check with bit width of the select.
  UInt32 maxExprSize = sel->getBitSize();
  UInt32 naturalSelWidth = maxExprSize;
  if (dynamic_cast<NUIdentRvalue*>(sel)   or
      dynamic_cast<NUVarselRvalue*>(sel)  or
      dynamic_cast<NUConcatOp*>(sel)) {

    // if we can determine the max number of bits sel can affect, use
    // that size instead of the one based on the branch constants.
    naturalSelWidth = sel->determineBitSize();
  }

  sel->resize(maxExprSize);

  if (naturalSelWidth > 32) {
    return;            // selector too wide for consideration
  }

  // If there are no case items, give up, it is not a full case
  NUCase::ItemLoop itemsIter = (*the_stmt)->loopItems();
  if (itemsIter.atEnd()) {
    return;
  }

  // Get binary value of select if it is constant
  NUConst* constSel = sel->castConst();
  DynBitVector selVal;
  DynBitVector selDrv;
  if (constSel != NULL)
    constSel->getValueDrive(&selVal, &selDrv);
    

  // Go through the case items and get a bit vector for each case item. We
  // convert the digits to either 0, 1, or x. 0 and 1 map to 0 and
  // 1. x, z, or ? map to x or to 0 as appropriate for casex and
  // casez. These strings are put in an ordered set for processing
  // later on.

  CaseItemStrings caseItemStr;  // set of strings that represent
                                // constant values of the case labels
                                // (Note the strings are reversed,
                                // that is the first char of each
                                // string represents the lsb of the
                                // case label)
  NUCaseItem* caseItem = NULL;
  bool fullySpecified = false;
  for (; !itemsIter.atEnd(); ++itemsIter)
  {
    // We may have a default case because even though there may be a
    // default the net may not be assigned in the default. If it isn't
    // we still need to see if we have a full case
    caseItem = *itemsIter;

    for (NUCaseItem::ConditionLoop condIter = caseItem->loopConditions();
	 !condIter.atEnd();
	 ++condIter)
    {
      NUCaseCondition* caseCond = *condIter;
      NUConst* constVal = dynamic_cast<NUConst*>(caseCond->getExpr());
      // Only process constants
      if (constVal != NULL)
      {
        UtString itemStr;
        DynBitVector value;
        DynBitVector drive;
        DynBitVector mask;
	// Get the binary vector value for the constant
	constVal->getValueDrive(&value, &drive);
        // Get the case condition mask for casez and casex. This mask
        // has bit set for 1's and 0's, and unset for x/z/?
        NUConst* constMask = dynamic_cast<NUConst*>(caseCond->getMask());
        if (constMask != NULL)
          constMask->getSignedValue(&mask);

        // If mask is anded with drive, then the result has bits set for
        // all x's (for case or casez) and z's (for case). Since there's
        // no x/z in carbon simulation, such conditions would never match
        // select and can be ignored from fully specified computation.
        size_t numBits = value.size();
        DynBitVector driveAndMask(drive);
        if (constMask != NULL)
          driveAndMask &= mask;
        else
        {
          // Prepare a mask with all 1's. This indicates no x/z's.
          mask.resize(numBits);
          mask.set();
        }

        bool ignoreCaseItem = false;
        if (driveAndMask.any())
          ignoreCaseItem = true;

        // If the case select is a constant, try matching the case item
        // constant with it while ignoring the don't cares.
        if (!ignoreCaseItem && (constSel != NULL))
        {
          size_t selBits = selVal.size();
          DynBitVector tmpVec1(selBits);
          tmpVec1 = (mask & selDrv);
          if (tmpVec1.none())
          {
            DynBitVector tmpVec2(selBits);
            tmpVec1 = (mask & selVal);
            tmpVec2 = (mask & value);
            if (tmpVec1 == tmpVec2)
              fullySpecified = true;
          }
          ignoreCaseItem = true;
        }

        for (UInt32 bit = 0; !ignoreCaseItem && (bit < numBits); ++bit)
        {
          // Check if this bit is beyond the size of the select. If
          // so, we only care about non-zeros. If there are any,
          // this condition could never match the select.
          if (bit < naturalSelWidth)
          {
            // If mask is set, then value is don't care (represented with 'x'),
            // otherwise just use value vector. 
           if (bit < mask.size()) {
                if (!mask.test(bit))
                  itemStr << 'x';
                else if (value.test(bit))
                  itemStr << '1';
                else
                  itemStr << '0';
           } else {
               itemStr << 'x';
           }
          }
          // If this value is a don't care, then ignore it. If it's
          // not, then it should be 0/1, since x/z would have resulted
          // in this case item being ignored before this.
          else if (mask.test(bit) && value.test(bit))
            ignoreCaseItem = true;
        }

        if (!ignoreCaseItem)
          caseItemStr.insertWithCheck(itemStr);
      }
    }
  }

  // now check to see if the select expression contains some bits that
  // are constant.  If so then it is not necessary to enumerate all
  // possible case tags in order to set the fully specified flag.
  // (Two possible expressions I can think of are a concat with
  // constants, and a left shift by a constant amount.
  // e.g. {1'b0,a,b} and ({1'b1,a}<<1)
  if ( not fullySpecified and (constSel == NULL) ){
    // add items to the caseItemStr string set that cover any constant
    // bits in the select expression.  e.g. If the expression is only
    // partially constant like: case ({1'b0,1'b1,a3}) then
    // we add to the caseItemStr two values: xx1 and x0x so that
    // they cover all values that cannot be selected by the selector.
    // (remember that the order of chars in each caseItemStr is
    // reversed, that is the left character represents the LSB)
    size_t numSelBits = sel->getBitSize();
    const SourceLocator &loc = sel->getLoc();
    for (UInt32 bit = 0; (bit < numSelBits); ++bit){
      // if sel[bit] is a 1/0 then gen a string that is the inversion of
      // that bit (in the appropriate position) and x's elsewhere

      CopyContext cc(NULL,NULL);
      NUExpr* sel_copy = sel->copy(cc);
      NUExpr* curBit = new NUVarselRvalue(sel_copy, ConstantRange (bit, bit), loc);
      curBit->resize(1);
      curBit = mFold->fold(curBit,eFoldAggressive);

      NUConst* constCurBit = curBit->castConst();
      if (constCurBit == NULL){
        delete curBit;
        continue;
      }
      DynBitVector curVal;
      DynBitVector curDrv;
      constCurBit->getValueDrive(&curVal, &curDrv);
      delete curBit;            // not needed anymore
      if (curDrv.any()){
        continue;  // this must be either z or x in the case condition
      }
      UtString itemStr;
      for (UInt32 pattern_bit = 0; (pattern_bit < numSelBits); ++pattern_bit)
      {
        if (bit != pattern_bit)
          itemStr << 'x';
        else {
          // curVal is a single bit, put the inversion of this bit
          // into the itemStr at this position (see comment above 
          // "add items to the caseItemStr" for why we use the inverted value)
          itemStr << (curVal.any() ? '0' : '1');
        }
      }
      caseItemStr.insertWithCheck(itemStr);
    }
  }
  
  // If all possibilities are fully specified, indicate that in the statement.
  if (fullySpecified || reduceCaseItems(caseItemStr))
  {
    (*the_stmt)->putFullySpecified(true);
    // Delete the default statements as they're never executed.
    if ((*the_stmt)->hasDefault() && (caseItem != NULL))
    {
      (*the_stmt)->putHasDefault(false);
      // The default item is at the end. 
      (*the_stmt)->removeItem(caseItem);
      if (caseItem) delete caseItem;
    }
  }
}



bool
VerificNucleusBuilder::hasSpecialCharacter(const UtString& data)
{
    for(size_t pos = 0; pos != data.size(); ++pos) {
        switch(data[pos]) {
            case '`':
            case '~':
            case '!':
            case '@':
            case '#':
            case '$':
            case '%':
            case '^':
            case '&':
            case '*':
            case '(':
            case ')':
            case '-':
            case '+':
            case '=':
            case '[':
            case '{':
            case ']':
            case '}':
            case '|':
            case '\\':
            case ':':
            case ';':
            case '"':
            case '\'':
            case '<':
            case ',':
            case '>':
            case '.':
            case '?':
            case '/':
            case ' ':
                return 1;
            default: continue; 
        }
    }
    return 0;
}

bool
VerificNucleusBuilder::startsWithNumeric(const UtString& data)
{
  if (data.size() > 0) {
    switch(data[0]) {
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      return 1;
    default: break;
    }
  }
  
  return 0;
}

UtString
VerificNucleusBuilder::createNucleusString(const UtString& data)
{
    UtString nucleusString(data);
    INFO_ASSERT(data.length() > 0,  "Incorrect empty string");
    if (hasSpecialCharacter(data) || startsWithNumeric(data)) {

        // Steve Lim: 2013-10-29
        // If the existing name is a VHDL escaped identifier, prepending
        // an escape will change the name from "\name\" to "\\name\ "
        // Avoid doing this for VHDL escaped identifiers since they are
        // already escaped.
        // Testcases: beacon_vhdl_93/
        //        extd_identifier1, extd_identifier2,
        //        extd_identifier4, extd_identifier5
        if (data[0] == '\\' && data[data.length()-1] == '\\') { // VHDL escaped identifier
          return nucleusString;
        }
        if (data[0] == '\\' && data[data.length()-1] == ' ') { // Verilog escaped identifier
            return nucleusString;
        }
        nucleusString = UtString("\\") + data + UtString(" ");
    }
    return nucleusString;
}

UtString
VerificNucleusBuilder::gNameAsNucleusName(const UtString& data)
{
    return createNucleusString(data);
}

UtString
VerificNucleusBuilder::gNameAsNucleusName(const StringAtom* data)
{
    INFO_ASSERT(data, "");
    INFO_ASSERT(data->str(), "");
    return createNucleusString(data->str());
}


bool
VerificNucleusBuilder::isKnownDirective(const UtString& keyword)
{
    return isModuleDirective(keyword) || isNetDirective(keyword) || isFunctionDirective(keyword);
}

bool
VerificNucleusBuilder::isModuleDirective(const UtString& keyword)
{
    return isFromDirectiveGroup(mModulePragmas, keyword);
}

bool
VerificNucleusBuilder::isNetDirective(const UtString& keyword)
{
    return isFromDirectiveGroup(mNetPragmas, keyword);
}

bool
VerificNucleusBuilder::isFunctionDirective(const UtString& keyword)
{
    return isFromDirectiveGroup(mFunctionPragmas, keyword);
}

bool
VerificNucleusBuilder::isFromDirectiveGroup(const char** directiveGroup, const UtString& keyword)
{
  for (const char** p = directiveGroup; *p != NULL; ++p) {
      if (UtString(*p) == keyword) {
          return true;
      }
  }
  return false;
}

void 
VerificNucleusBuilder::addDirective(const UtString& keyword, const UtString& rest, const UtString& moduleName, const UtString& netName,
    const SourceLocator& loc)
{
    // Formulate a directive as if it came from the directives file,
    // taking the pragma text and appending on the name of the
    // object.  Thus if the user wrote:
    //    module foo;
    //       reg x;        // carbon observeSignal
    //       reg clk2;     // carbon collapseClock top.clk1
    //       reg scan_ena; // carbon tieNet 1'b0
    //
    // then we want to form the strings:
    //       "observeSignal foo.x"
    //       "collapseClock top.clk1 foo.clk2"
    //       "tieNet 1'b0 foo.scan_ena"
    UtString directive(keyword);
    directive << " " << rest;
    directive << " " << moduleName;
    if (netName != NULL)
        directive << "." << netName;
    // The interface to IODB::parseDirective involves passing
    // the StrToken context, in addition to the "rest" of the
    // string, so we must now recreate the same state that
    // the directives file parser is in when it calls parseDirective
    // for a semi-parsed line.
    StrToken dirTok(directive.c_str());
    ++dirTok;
    if (loc.isTicProtected() ){
        mMessageContext->CarbonDirectiveInProtectedFile(loc.getFile(), directive.c_str());
        return;
    }
    gIODB()->parseDirective(keyword.c_str(), dirTok.curPos(), dirTok, loc);
}

NUConst*
VerificNucleusBuilder::convertRealConstantToIntConstant(NUConst *the_const, unsigned width, const SourceLocator &loc)
{
  double realval, rounded;
  bool isReal = the_const->getCarbonReal( &realval );
  NU_ASSERT (isReal, the_const);

  // Convert real into an integer part and a fraction -1.0 < frac < 1.0
  CarbonReal frac = std::modf (realval, &rounded);
  if (frac >= 0.5) {
    ++rounded;
  } else if (frac <= -0.5) {
    --rounded;
  }
  if (frac != 0.0 )              // There was a loss of precision
  {
    getMessageContext()->RealConstantRounded( &loc, (double)realval, (SInt64)rounded);
  }

  // Create a new constant the same width as the lvalue of the assignment
  // verilog lrm 4.5.1 says reals converted to integer by type coercion are signed
  NUConst *new_const = NUConst::create(true, (SInt64)rounded, width, loc);
  return new_const;
}
        
NUNet* VerificNucleusBuilder::fNetHierRef(NUModule* module, AtomArray* path)
{
    INFO_ASSERT(module, "Incorrect module type");
    NUCLEUS_CONSISTENCY(path, module->getLoc(), "Incorrect path object");
    return module->findNetHierRef(*path);
}

NUTask* VerificNucleusBuilder::fTaskByName(NUModule* module, const UtString& taskName)
{
    INFO_ASSERT(module, "Incorrect module type");
    for (NUTaskLoop q = module->loopTasks(); !q.atEnd(); ++q) {
      NUTask* task = *q;
      if (UtString(task->getName()->str()) == taskName) {
          return task; 
      }
    }
    return 0; 
}


NUModule* VerificNucleusBuilder::gModule(NUScope* scope)
{
    /// Return the module in which this scope resides.  If this scope is a module, it will return itself.
    return scope->getModule();
}

bool VerificNucleusBuilder::isModule(NUScope* scope)
{
    return (dynamic_cast<NUModule*>(scope) != NULL);
}

bool VerificNucleusBuilder::isTask(NUScope* scope)
{
    return (dynamic_cast<NUTask*>(scope) != NULL);
}


NUScope* VerificNucleusBuilder::gParentDeclUnit(NUScope* scope)
{
    NUScope* tf = scope->getTFScope();
    NUScope* declaration_unit_scope = NULL;
    /// Return if the scope resides under task/function it will return it, otherwise return the module
    /// Return the module in which this scope resides.  If this scope is a module, it will return itself.
    if (tf) {
        declaration_unit_scope = tf;
    } else {
        declaration_unit_scope = scope->getModule();
    }
    return declaration_unit_scope; 
}

void VerificNucleusBuilder::registerModule(VeriTreeNode* ve_module, VhdlTreeNode* vh_module, const UtString& name, NUModule* module)
{
    if (mModuleRegistry.find(name) != mModuleRegistry.end()) {
    #ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
        std::cerr << "Module with the same name already exists in mModuleRegistry" << std::endl;
    #endif
        return;
    }
    INFO_ASSERT(module, "null module");
    mModuleRegistry[name] = module;
    if (ve_module) mVeModuleRegistry[ve_module] = module;
    else if (vh_module) mVhModuleRegistry[vh_module] = module;
}

// TODO : probably similar function will be needed for VHDL, although not sure about Jaguar uniquification approach, need testcase to expose it and fix similarly for VHDL
void VerificNucleusBuilder::createUniquifiedModuleName(VeriModule* ve_module, const UtString& name)
{
    // check if we already have uniquified module name for this module 
    UtString moduleName;
    UtString uniquifiedModuleName = gModuleName(ve_module);
    if (uniquifiedModuleName.empty()) {
        // check if uniquification needs to be done (if we already have module with same name than it needs to be done)
        NUBase* nModule = gModule(name);
        // the 've_module' shouldn't be registered if corresponding NUModule already created
        NUBase* nModule2 = gModule(ve_module);
        if (nModule && ! nModule2) {
            // we already have module with same name so need todo uniquification of each other module with same name
            uniquifiedModuleName << name.size() << "$" << name << "$" << ve_module->Linefile()->GetFileName();
            registerModuleName(uniquifiedModuleName, ve_module);
        } else {
            // no need for uniquification, register with original 'name' 
            registerModuleName(name, ve_module);
        }
    }
}

void VerificNucleusBuilder::registerModuleName(const UtString& name, VeriModule* module)
{
    INFO_ASSERT(module, "null module");
    if (mModuleNameRegistry.find(module) != mModuleNameRegistry.end()) {
    #ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
        std::cerr << "Module already registered" << std::endl;
    #endif
        return;
    }
    mModuleNameRegistry[module] = name;
}

NUExpr* VerificNucleusBuilder::gRvalue(NUBase* lvalueOrExpr)
{ 
    NULvalue* lvalue = dynamic_cast<NULvalue*>(lvalueOrExpr);
    NUExpr* rvalue = dynamic_cast<NUExpr*>(lvalueOrExpr);
    if (lvalue) rvalue = lvalue->NURvalue(); // This function returns rvalue corresponding to lvalue
    return rvalue;
}

unsigned VerificNucleusBuilder::gPortDirection(NUScope* scope, unsigned portIndex, const SourceLocator& loc)
{
    NUCLEUS_CONSISTENCY(scope, loc, "NULL scope object");
    NUScope::ScopeT scope_type = scope->getScopeType();
    NUNet* p = NULL;
    if (NUScope::eModule == scope_type) {
        NUModule* nModule = dynamic_cast<NUModule*>(scope);
        NUCLEUS_CONSISTENCY(nModule, loc, "Incorrect module type");
        // it's not obvious why nTask->getNumArgs()/nModule->getNumPorts() returns int (really it returns size of vector which can't be negative).
        // so doing conversion to unsigned
        NUCLEUS_CONSISTENCY(portIndex < (unsigned)nModule->getNumPorts(), loc, "Out of range index for module port");
        p = nModule->getPortByIndex(portIndex);
    } else if (NUScope::eTask == scope_type) {
        NUTask* nTask = dynamic_cast<NUTask*>(scope);
        NUCLEUS_CONSISTENCY(nTask, loc, "Incorrect task type");
        NUCLEUS_CONSISTENCY(portIndex < (unsigned)nTask->getNumArgs(), loc, "Out of range index for task/function argument");
        p = nTask->getArg(portIndex);
    }
    NUCLEUS_CONSISTENCY(p, loc, "Unable to get Net");
    if (p->isInput()) {
        return VERI_INPUT;
    } else if (p->isOutput()) {
        return VERI_OUTPUT;
    } else if (p->isBid()) {
        return VERI_INOUT;
    } 
    NUCLEUS_CONSISTENCY(false, loc, "Unknown port direction");
}

unsigned VerificNucleusBuilder::fPortDirection(NUModule* module, const UtString& name)
{
    NUNet* p = NULL;
    for (NUNetVectorLoop l = module->loopPorts(); !l.atEnd(); ++l)
    {
        NUNet* net = *l;
        if (UtString(net->getName()->str()) == name) {
            p = net;
        }
    }
    INFO_ASSERT(p, "Unable to get Net");
    if (p->isInput()) {
        return VERI_INPUT;
    } else if (p->isOutput()) {
        return VERI_OUTPUT;
    } else if (p->isBid()) {
        return VERI_INOUT;
    } 
    INFO_ASSERT(false, "Unknown port direction");
    return 0;
}


unsigned VerificNucleusBuilder::gPortDirection(NUNet* p)
{
    if (p->isInput()) {
        return VERI_INPUT;
    } else if (p->isOutput()) {
        return VERI_OUTPUT;
    } else if (p->isBid()) {
        return VERI_INOUT;
    } 
    INFO_ASSERT(false, "Unknown port direction");
    return 0;
}

NUNet* VerificNucleusBuilder::gNetFromIdentRvalue(NUIdentRvalue* identRvalue)
{
    return identRvalue->getIdent();
}

NUNet* VerificNucleusBuilder::gNetFromIdentLvalue(NUIdentLvalue* identLvalue)
{
    return identLvalue->getIdent();
}


NUNet* VerificNucleusBuilder::gNetFromExpr(NUExpr* expr)
{
    NUIdentRvalue* iRvalue = dynamic_cast<NUIdentRvalue*>(expr);
    NUVarselRvalue* vRvalue = dynamic_cast<NUVarselRvalue*>(expr);
    NUMemselRvalue* mRvalue = dynamic_cast<NUMemselRvalue*>(expr);
    if (iRvalue) return iRvalue->getIdent();
    else if (vRvalue) return vRvalue->getIdent();
    else if (mRvalue) return mRvalue->getIdent();
    else return 0;
}

unsigned VerificNucleusBuilder::gNetLength(NUVectorNet* net)
{
    return net->getDeclaredRange()->getLength();
}

unsigned VerificNucleusBuilder::gNetLength(NUMemoryNet* net, unsigned i)
{
    unsigned size = net->getNumDims();
    INFO_ASSERT(i < size, "index out of range");
    const ConstantRange * nthRange = net->getRange(i);
    return nthRange->getLength();
}


unsigned VerificNucleusBuilder::gExprWidth(NUExpr* expr)
{
    return expr->getBitSize();
}

void 
VerificNucleusBuilder::dIfEmptyDeclScope(NUNamedDeclarationScope* namedDeclarationScope, NUScope* scope, bool isUserNamed)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API dIfEmptyDeclScope" << std::endl;
#endif
    // if declaration scope is empty, remove from parent scope and delete 
    if (namedDeclarationScope->isEmpty(NULL) && !isUserNamed) {
        scope->removeDeclarationScope(namedDeclarationScope);
        delete namedDeclarationScope;
    } 
}

NUBlock*
VerificNucleusBuilder::blockify(NUStmt* stmt, NUScope* scope, const SourceLocator& loc)
{
    if (stmt) {
        if (stmt->getType() == eNUBlock) {
            return dynamic_cast<NUBlock*>(stmt);
        } else {
            NUBlock* block = cBlock(scope, stmt->getLoc());
            aStmtToBlock(stmt, block); 
            return block;
        }
    } else {
        // if no stmt create empty block
        NUBlock* block = cBlock(scope, loc);
        return block;
    }
}

bool VerificNucleusBuilder::isMemoryNet(NUNet* net)
{
    return net && net->isMemoryNet();
}

// Retrieve statements from a temporary container block.
// See comments for VeriFunctionCall to see why this
// is useful.
void
VerificNucleusBuilder::gStmtsFromBlock(NUStmtList* stmts, NUStmt* stmt, bool front)
{
    if (stmt) {
        NUBlock* nBlock = dynamic_cast<NUBlock*>(stmt); 
        if (nBlock) {
            // copy stmts from block to stmts list
            for (NUStmtLoop l = nBlock->loopStmts(); !l.atEnd(); ++l) {
                NUStmt* tStmt = *l;
                if (front) {
                    aStmtToStmtListFront(tStmt, stmts);
                } else {
                    aStmtToStmtList(tStmt, stmts);
                }
            }
            NUStmtList empty;
            nBlock->replaceStmtList(empty); // clear out the stmts from original block
            NUScope* parentScope = nBlock->getParentScope();
            nBlock->moveLocals(parentScope);
        } else {
            if (front) {
                aStmtToStmtListFront(stmt, stmts);
            } else {
                aStmtToStmtList(stmt, stmts);
            }
        }
    }
}

// gStmtsFromBlock makes a list of statements 'stmts' from the statements that are in 'alwaysBlock'
void
VerificNucleusBuilder::gStmtsFromAlwaysBlock(NUStmtList* stmts, NUAlwaysBlock* alwaysBlock)
{
    NUBlock* block = alwaysBlock->getBlock();
    for (NUStmtLoop l = block->loopStmts(); !l.atEnd(); ++l) {
        NUStmt* stmt = *l;
        stmts->push_back(stmt);
    }
}

NUExpr*
VerificNucleusBuilder::cOrConditions(NUNetVector& nets)
{
    NUExpr* cond = NULL;
    for (NUNetVectorLoop l(nets); !l.atEnd();  ++l) {
        // Create a sub conditional for this net
        NUNet* net = *l;
        NUExpr* subCond = cIdentRvalue(net, net->getLoc());
        // Merge the conditionals together in a big boolean OR
        if (cond == NULL) {
            // First conditional
            cond = subCond;
        } else {
            // Or the conditionals
            cond = cBinaryOperation(cond, VERI_LOGOR, subCond, false, false, net->getLoc());
        }
    }
    return cond;
}

void
VerificNucleusBuilder::rStmtList(NUAlwaysBlock* alwaysBlock, NUStmtList& stmts)
{
    NUBlock* block = alwaysBlock->getBlock();
    block->replaceStmtList(stmts);
}

NUExpr*
VerificNucleusBuilder::copyExpr(NUExpr* expr)
{
    CopyContext cc(0, 0);
    return expr->copy(cc);
}

NULvalue*
VerificNucleusBuilder::copyLval(NULvalue* lval)
{
    CopyContext cc(0, 0);
    return lval->copy(cc); 
}


/*************************************************************************************************************/
/****************************************** NUCLEUS NAMES ****************************************************/
/*************************************************************************************************************/

// copied from PopulateExpr.cxx:1136
// Overloaded for unary operators.
void VerificNucleusBuilder::fixSign (NUUnaryOp* expr)
{
    switch (expr->getOp ()) {
        case NUOp::eUnLogNot:
        case NUOp::eUnRedAnd:
        case NUOp::eUnRedOr:
        case NUOp::eUnRedXor:
            break;
        default:
            expr->getArg (0)->setSignedResult (expr->isSignedResult ());
    }
}

// Comment copied for future reference on fixSign functions origin
// Fix the signedness of operands to binary operators.  We cannot exclusively
// depend on the value returned by veExprIsSigned(), as Cheetah treats NAMEDOBJECTUSE
// leaves as if the operand were self-determined.
//
void VerificNucleusBuilder::fixSign (NUBinaryOp* expr)
{
    NUExpr* lop = expr->getArg (0);
    NUExpr* rop = expr->getArg (1);

    switch (expr->getOp ()) {
        default:
            // Verilog rules treat the operands as being of the same signedness as the resulting
            // expression
            lop->setSignedResult (expr->isSignedResult ());
            rop->setSignedResult (expr->isSignedResult ());
            break;

        case NUOp::eBiRshift:
        case NUOp::eBiRshiftArith:
        case NUOp::eBiLshift:
        case NUOp::eBiLshiftArith:
            // RHS is self-determined (only force LHS to match)
            lop->setSignedResult (expr->isSignedResult ());
            rop->setSignedResult (false);
            break;


        case NUOp::eBiNeq:
        case NUOp::eBiEq:
        case NUOp::eBiTrieq:
        case NUOp::eBiTrineq:
            if (lop->isSignedResult () != rop->isSignedResult ()) {
                lop->setSignedResult (false);
                rop->setSignedResult (false);
            }
            break;

        case NUOp::eBiSLt:
        case NUOp::eBiSLte:
        case NUOp::eBiSGtr:
        case NUOp::eBiSGtre:
            // Wouldn't be signed unless both operands were signed already
            break;

        case NUOp::eBiULt:
        case NUOp::eBiULte:
        case NUOp::eBiUGtr:
        case NUOp::eBiUGtre:
            lop->setSignedResult (false);
            rop->setSignedResult (false);
            break;

        case NUOp::eBiLogAnd:
        case NUOp::eBiLogOr:
            break;

        case NUOp::eBiDExp:           // Leave signs of ** operands the way
        case NUOp::eBiExp:            //   we found 'em.
            break;
    }
}



//TODO Need to separate this naming functions to separate and use Verific native enumaration instead of string

CarbonControlType 
VerificNucleusBuilder::gCarbonControlType(unsigned funcType)
{
    if (VERI_SYS_CALL_STOP == funcType) {
        return eCarbonStop;
    } else if (VERI_SYS_CALL_FINISH == funcType) {
        return eCarbonFinish;
    } else {
        INFO_ASSERT(false, "Unknown control sys task type");
        return CarbonControlType(0);
    }
}

HierFlags
VerificNucleusBuilder::gNucleusLangType(bool isVerilog)
{
    if (isVerilog) {
        return eSLVerilog;
    } else {
        return eSLVHDL;
    }
}

NUCase::CaseType VerificNucleusBuilder::gNucleusCaseType(unsigned caseStyle)
{
    if (VERI_CASE == caseStyle) {
        return NUCase::eCtypeCase;                      //!< defined as case stmt (x,z,? must match exaxtly)
    } else if (VERI_CASEX == caseStyle) {
        return NUCase::eCtypeCasex;                      //!< defined as casex (x,z,? match anything)
    } else if (VERI_CASEZ == caseStyle) {
        return NUCase::eCtypeCasez;                     //!< defined as casez (z and ? match anything)
    }
    INFO_ASSERT(false, "Incorrect case type");
    return NUCase::eCtypeCase;
}

NUOp::OpT VerificNucleusBuilder::gNucleusUnOp(unsigned unOp)
{
    if ((VERI_LOGNOT == unOp) || (VERI_NOT == unOp) || (VHDL_not == unOp)) {
        return NUOp::eUnLogNot;
    } else if ((VERI_PLUS == unOp) || (VHDL_PLUS == unOp)) {
        return NUOp::eUnPlus;                //!< Unary +
    } else if ((VERI_MIN == unOp) || (VHDL_MINUS == unOp)) {
        return NUOp::eUnMinus;               //!< Unary -
    } else if ((VERI_REDNOT == unOp)) {
        return NUOp::eUnBitNeg;              //!< Unary Bitwise ~
    } else if ((VERI_REDAND == unOp) || (VHDL_REDAND == unOp)) {
        return NUOp::eUnRedAnd;              //!< Unary Reduction &
    } else if ((VERI_REDOR == unOp) || (VHDL_REDOR == unOp)) {
        return NUOp::eUnRedOr;               //!< Unary Reduction |
    } else if ((VERI_REDXOR == unOp) || (VHDL_REDXOR == unOp)) {
        return NUOp::eUnRedXor;              //!< Unary Reduction ^
    } else if (VERI_SYS_CALL_TIME == unOp) {
        return NUOp::eZSysTime;
    } else if (VERI_SYS_CALL_STIME == unOp) {
        return NUOp::eZSysStime;
    } else if (VERI_SYS_CALL_REALTIME == unOp) {
        return NUOp::eZSysRealTime;
    } else if (VERI_SYS_CALL_BITSTOREAL == unOp) {
        return NUOp::eUnBitstoReal;
    } else if (VERI_SYS_CALL_ITOR == unOp) {
        return NUOp::eUnItoR;    
    } else if (VERI_SYS_CALL_REALTOBITS == unOp) {
        return NUOp::eUnRealtoBits;
    } else if (VERI_SYS_CALL_RTOI == unOp) {
        return NUOp::eUnRtoI;     
    } else if ((VERI_SYS_CALL_SIGNED == unOp) || (VERI_SYS_CALL_UNSIGNED == unOp)) {
        return NUOp::eBiVhExt;
    } else {
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
        std::cerr << "Operation not supported" << std::endl;
#endif
        return NUOp::eInvalid;
    }
}

// converts VERI_* operator code into a NUOp::opT, with consideration of the isSignedOp
// binOp is the verific operator,  signedOp if true means that the signed verison of the operator should be used
NUOp::OpT VerificNucleusBuilder::gNucleusBinOp(unsigned binOp, bool isSignedOp)
{
  switch ( binOp ) {
  case VERI_REDAND:
  case VERI_AND :        return NUOp::eBiBitAnd;
  case VERI_REDOR:
  case VERI_OR :         return NUOp::eBiBitOr;
  case VERI_REDXOR:
  case VERI_XOR :        return NUOp::eBiBitXor;
  case VERI_PLUS :       return NUOp::eBiPlus;                //!< Binary +
  case VERI_MIN :        return NUOp::eBiMinus;               //!< Binary -
  case VERI_MUL :        return (isSignedOp ? NUOp::eBiSMult : NUOp::eBiUMult);
  case VERI_DIV :        return (isSignedOp ? NUOp::eBiSDiv  : NUOp::eBiUDiv);
  case VERI_MODULUS :    return (isSignedOp ? NUOp::eBiSMod  : NUOp::eBiUMod);
  case VERI_LOGEQ :      return NUOp::eBiEq;                  //!< Binary ==
  case VERI_LOGNEQ :     return NUOp::eBiNeq;                 //!< Binary !=
  case VERI_CASEEQ :     return NUOp::eBiTrieq;               //!< Binary ===
  case VERI_CASENEQ :    return NUOp::eBiTrineq;              //!< Binary !==
  case VERI_LOGAND :     return NUOp::eBiLogAnd;              //!< Binary Logical &&
  case VERI_LOGOR :      return NUOp::eBiLogOr;               //!< Binary Logical ||
  case VERI_LT :         return (isSignedOp ? NUOp::eBiSLt   : NUOp::eBiULt);
  case VERI_LEQ :        return (isSignedOp ? NUOp::eBiSLte  : NUOp::eBiULte);
  case VERI_GT :         return (isSignedOp ? NUOp::eBiSGtr  : NUOp::eBiUGtr);
  case VERI_GEQ :        return (isSignedOp ? NUOp::eBiSGtre : NUOp::eBiUGtre);
  case VERI_POWER :      return NUOp::eBiExp;                 //!< Binary **
  case VERI_ARITRSHIFT : return (isSignedOp ? NUOp::eBiRshiftArith : NUOp::eBiRshift) ;         //!< Binary shift right arith (Verilog) >>> 
  case VERI_ARITLSHIFT : return NUOp::eBiLshiftArith;         //!< Binary shift left arith (Verilog) <<<
  case VERI_RSHIFT :     return NUOp::eBiRshift;              //!< Binary >>
  case VERI_LSHIFT :     return NUOp::eBiLshift;              //!< Binary <<
  default:               return NUOp::eInvalid;               // unsupported
        }
}


NetFlags
VerificNucleusBuilder::gNucleusNetType(unsigned type, bool isTemp, bool is2DNet, unsigned scope_type, bool isSigned, bool isParentTF, bool isPort, const SourceLocator &loc)
{
    unsigned defFlags = eAllocatedNet;
    if (isSigned) {
        defFlags = defFlags | eSigned;
    }
    if (isParentTF) {
        defFlags = defFlags | eNonStaticNet;
    }
    if (NUScope::eNamedDeclarationScope == scope_type) {
        defFlags = defFlags | eBlockLocalNet;
    } else if (NUScope::eBlock == scope_type) {
        defFlags = defFlags | eBlockLocalNet | eTempNet | eNonStaticNet;
    }

    if (isTemp) {
        defFlags = defFlags | eTempNet;
    }
    if (VERI_WIRE == type) {
        if (is2DNet) {
            return NetFlags(defFlags | eDMWire2DNet);
        } else {
            return NetFlags(defFlags | eDMWireNet);
        }
    // If we get here, then we know it's not a wire, so must be a reg.
    // Verific will mark anything that is a wire as VERI_WIRE.
    } else if ( (VERI_REG == type)
               || (VERI_BYTE == type) 
               || (VERI_SHORTINT == type) 
               || (VERI_LONGINT == type) 
               || (VERI_LOGIC == type) 
               || (VERI_BIT == type)) {
        if (is2DNet) {
            return NetFlags(defFlags | eDMReg2DNet);
        } else {
            return NetFlags(defFlags | eDMRegNet);
        }
    } else if ((VERI_INTEGER == type) || (VERI_INT == type)) {
        if (isPort) {
            if (is2DNet) {
                return NetFlags(defFlags | eDMInteger2DNet);
            } else {
                return NetFlags(defFlags | eDMIntegerNet);
            }
        } else {
            if (is2DNet) {
                return NetFlags(defFlags | eDMInteger2DNet );
            } else {
                return NetFlags(defFlags | eDMIntegerNet );
            }
        }
    } else if (VERI_TIME == type) {
        if (isPort) {
            if (is2DNet) {
                return NetFlags(defFlags | eDMTime2DNet);
            } else {
                return NetFlags(defFlags | eDMTimeNet);
            }
        } else {
            if (is2DNet) {
                return NetFlags(defFlags | eDMTime2DNet);
            } else {
                return NetFlags(defFlags | eDMTimeNet);
            }
        }
    } else {
        // don't support 2d array of below types
        if (is2DNet) {
            UtString msg("Array of unsupported net type: ");
            const char* tokenStr = VeriNode::PrintToken(type);
            msg << tokenStr << " (" << type << ")";
            reportUnsupportedLanguageConstruct(loc, msg); // this is an error
            return NetFlags(eNoneNet);
        }
    
        if (VERI_WOR == type) {
            return NetFlags(defFlags | eDMWorNet);
        } else if (VERI_WAND == type) {
            return NetFlags(defFlags | eDMWandNet);
        } else if (VERI_TRI == type) {
            return NetFlags(defFlags | eDMTriNet);
        } else if (VERI_TRI0 == type) {
            return NetFlags(defFlags | eDMTri0Net);
        } else if (VERI_TRI1 == type) {
            return NetFlags(defFlags | eDMTri1Net);
        } else if (VERI_TRIREG == type) {
            return NetFlags(defFlags | eDMTriregNet);
        } else if (VERI_TRIOR == type) {
            return NetFlags(defFlags | eDMTriorNet);
        } else if (VERI_TRIAND == type) {
            return NetFlags(defFlags | eDMTriandNet);
        } else if (VERI_SUPPLY0 == type) {
            return NetFlags(defFlags | eDMSupply0Net);
        } else if (VERI_SUPPLY1 == type) {
            return NetFlags(defFlags | eDMSupply1Net);
        } else if ((VERI_REAL == type) || (VERI_REALTIME == type) /* || (VERI_SHORTREAL == type)*/) {
            return NetFlags(defFlags | eDMRealNet);
        } else if (VERI_NONE == type) {
            return NetFlags(defFlags);
        } else {
            UtString msg("Unsupported net type: ");
            // try to get a token and always print the decimal equivalent in the unsupported message
            const char* tokenStr = VeriNode::PrintToken(type);
            msg << tokenStr << " (" << type << ")";
            reportUnsupportedLanguageConstruct(loc, msg); // this is an error
            return NetFlags(eNoneNet);
        }
    }
}

// TODO if vhdl is going to use this gNucleus* functions, then we need to add || corresponding vhdl enum for all

// returns NetFlags for a port, based on the direction (dir from parser), and scope (from parser)
NetFlags
VerificNucleusBuilder::createPortFlags(NUScope* scope, StringAtom* name, unsigned dir, bool isSigned, const SourceLocator& loc)
{
    unsigned defFlags = 0;
    NUScope::ScopeT scope_type = scope->getScopeType();
    if ( isSigned ) defFlags |= eSigned;
    if (NUScope::eModule == scope_type) {
      defFlags |= eDMWireNet | eAllocatedNet;
    } else if (NUScope::eTask == scope_type) {
        defFlags |= eAllocatedNet | eBlockLocalNet | eNonStaticNet;
    }
    if (VERI_INPUT == dir) {
        return NetFlags(defFlags | eInputNet);
    } else if (VERI_OUTPUT == dir) {
        return NetFlags(defFlags | eOutputNet);
    } else if (VERI_INOUT == dir) {
      // We used to allow users to change inout ports
      // to input ports with the input directive, 
      // e.g. "input top.rst". Warn the user that
      // this is no longer supported.
      NUModule* mod = dynamic_cast<NUModule*>(scope);
      if ((mod != NULL) && gIODB()->isInputNet(mod->getName(), name)) {
        const char* modName = mod->getName()->str();
        const char* portName = name->str();
        mMessageContext->UnsupportedInputDirective(&loc, modName, portName);
        return NetFlags(defFlags | eBidNet);
      } 
      return NetFlags(defFlags | eBidNet);
    } else {
      UtString msg;
      msg << "Unsupported Port Direction '" << VeriNode::PrintToken(dir) << "'";
      reportUnsupportedLanguageConstruct(loc, msg.c_str());
      return NetFlags(eNoneNet);
    }
}

ClockEdge
VerificNucleusBuilder::gNucleusClockEdge(unsigned edge)
{
    if (VERI_POSEDGE == edge) {
        return eClockPosedge;
    } else if (VERI_NEGEDGE == edge) {
        return eClockNegedge;
    } else {
//#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
        std::cerr << "Unknown clock edge" << std::endl;
//#endif
        return eClockEdgeStart;
        //return eLevelHigh;
    }
}

StringAtom*
VerificNucleusBuilder::gNextUniqueName(NUScope* scope, const char* prefix)
{
    return scope->gensym(prefix);
}

StringAtom*
VerificNucleusBuilder::gNamedDeclScopeName(NUScope* scope)
{
    NUNamedDeclarationScope* nScope = dynamic_cast<NUNamedDeclarationScope*>(scope);
    INFO_ASSERT(nScope, "Unable to get named declaration scope");
    return nScope->getName();
}

StringAtom*
VerificNucleusBuilder::gTaskName(NUScope* scope)
{
    NUTF* tf = dynamic_cast<NUTF*>(scope);
    INFO_ASSERT(tf, "Unable to get task");
    return tf->getName();
}

StringAtom*
VerificNucleusBuilder::gModuleName(NUScope* scope)
{
    NUModule* module = dynamic_cast<NUModule*>(scope);
    INFO_ASSERT(module, "Unable to get module");
    return module->getName();
}

StringAtom*
VerificNucleusBuilder::gOriginalModuleName(NUScope* scope)
{
    NUModule* module = dynamic_cast<NUModule*>(scope);
    INFO_ASSERT(module, "Unable to get module");
    return module->getOriginalName();
}

/*************************************************************************************************************/
/****************************************** Nucleus DB update/set/print **************************************/
/*************************************************************************************************************/

void
VerificNucleusBuilder::uDesign()
{
  //RJC RC#2013/08/09 (frunze) by setting the IODB top level every time this method is called, we end up setting it every time the design is walked,
  //which is too often
    // ADDRESSED FD. I think I have guaranted this function to be called only once for each design, here is a fragment, mIsTopLevel is true when we create walker object, and after first run it becomes false (because first module we populate is alwyas top-level module and it's the only time we do populate top-level module)
    //if (mIsTopLevel) {
    //    mTopModuleName = name;
    //    aTopLevelModule(module);
    //    uDesign();
    //    mIsTopLevel = false;
    //} else {
    //    aModule(module);
    //}


    //set top level module in symtab
    NUModuleList topMods;


    //RJC RC#2013/08/09 (frunze) the following bit of code must be ineffective, if you run test/langerr/multi_top.v you do not hit this assert
    // ADDRESSED FD. In "normal" case, yes it's guaranted not to hit this assert because we just take first top-level module even if they are multiple ones (it's done in VerificDesignManager) (of course in case of not specifying toplevel module with -vlogTop). So that guarantes only one top level module. But sometimes we do something wrong with our code change during development (I can't recall what exactly, probably it is not just one case, because I have seen it many times in different contexts), in that case I have seen this assertion failing. I think it's best to keep this assertion, it will guard from unexpected behavoural in case we do something nasty. So in normal case where our code base is ok, we can't see this assertion even if we have multiple modules, so assertion is hear just for development.
// General thing: I think in many places where we have assertions (INFO_ASSERT, V2N_INFO_ASSERT causing CARBON INTERNAL ERROR), should be replaced with corresponding error report using MessageContext. This wasn't done in verilog, and I think if you turn on CARBON INTERNAL ERROR check for beacon verilog we will see many failures, in PEXIT tests especially which needs to be fixed. But in some places I think we shouldn't have error report and if situation occurs it's development problem and in that case it's best to have assertion which will immediately point the place of failure. So error/alert/warning reports for input file/option/directive incorrectness and ASSERTs for development checks.... if that occurs than we surely will know that we have bug in code.
    mDesign->getTopLevelModules(&topMods);
    INFO_ASSERT(topMods.size() == 1, "Design has multiple top-level modules or number of top level modules is zero");


    const NUModule* topMod = topMods.back();
    const char* topModName = topMod->getOriginalName()->str();
    mIODB->putTopLevelModuleName(topModName);
}

void 
VerificNucleusBuilder::setNetFlag(NUNet* port, NetFlags flag)
{
  if ( flag & eDeclareMask ) {
    port->setFlags(NetFlags(port->getFlags() & ~eDeclareMask)); // clear the declareMask field
  }
  if ( flag & ePortMask ) {
    port->setFlags(NetFlags(port->getFlags() & ~ePortMask)); // clear the portMask field
  }
  port->setFlags(NetFlags(port->getFlags() | flag)); // now set the flag(s)
}
    
void 
VerificNucleusBuilder::sNetType(NUNet* net, unsigned netType)
{
    // For implicit net and ports we set  eDMWireNet type by default (for implicit net always, for ports if there is no type information
    // during input/output declaration) but later this method can be called to correct the type when we encounter new declaration for this net.
    NetFlags nT = gNucleusNetType(netType, false, false, NUScope::eModule, net->isSigned(), false, false, net->getLoc());
    if (eNoneNet == nT) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for net '" << VeriNode::PrintToken(netType) << "'";
            NUCLEUS_INFO_ASSERT(false, net->getLoc(), msg.c_str());
        }
        return;
    }
    net->setFlags(NetRedeclare(net->getFlags(), nT));
}

bool
VerificNucleusBuilder::checkForPullConflicts()
{
    // default is true;
    return mArg->getBoolValue("-noCheckPullConflicts");
}

void 
VerificNucleusBuilder::sNetPullUpDown(NUNet* net, unsigned netType)
{
    if (netType == VERI_PULLUP) {
        if (checkForPullConflicts()) {
            if (net->isPullDown()) {
                mMessageContext->PullUpAndDown(net);
            }
            if (net->isTrireg()) {
                mMessageContext->PullTrireg(net);
            }
            if (net->isTri0()) {
                mMessageContext->PullUpTri0(net);
            }
        }
        net->putIsPullUp(true);
    } else {
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(netType == VERI_PULLDOWN, net->getLoc(), "Incorrect net type");
        if (checkForPullConflicts()) {
            if (net->isPullUp()) {
                mMessageContext->PullUpAndDown(net);
            }
            if (net->isTrireg()) {
                mMessageContext->PullTrireg(net);
            }
            if (net->isTri1()) {
                mMessageContext->PullDownTri1(net);
            }
        }
        net->putIsPullDown(true);
    }
}
    
//void 
//VerificNucleusBuilder::pSymTabs()
//{
//    mSymTabs->print(true, 0);
//}
    
void 
VerificNucleusBuilder::pDesign()
{
    mDesign->print(true, 0);
}

void
VerificNucleusBuilder::sUserTypesAtomicCache()
{
    mUserTypeFactory->setUserTypesAtomicCache(mSymTabs->getAtomicCache());
}

/*************************************************************************************************************/
/****************************************** Nucleus DB creation support routines *****************************/
/*************************************************************************************************************/
/*!
   \brief cleanupXZBinaryExpr: Possibly clean up a NUBinaryOp expression with respect to X and Z comparisons.
   \param lWidth is the maximum width of this expression that is used outside of the expression
   \retval  original expression or a simplified version of it (possibly a NUConst)
//RJC RC#14 this method should probably be used for both verilog and vhdl creation. May require conversion to a static method
   this method is patterned after the to-be obsolete methods: VerilogPopulate::cleanupXZBinaryExpr and VHDLPopulate::cleanupXZBinaryExpr
   // ADDRESSED FD: Vhdl flow keeps ref to NucleusBuilder object (just like verilog) and can call this function
*/
NUExpr*
VerificNucleusBuilder::cleanupXZBinaryExpr(NUBinaryOp *binary_expr, int lWidth, const SourceLocator& loc)
{
  NUCLEUS_CONSISTENCY(binary_expr, loc, "Undefined expression");

  NUExpr* out_expr = binary_expr;

  NUOp::OpT op = binary_expr->getOp();

  NUExpr *expr1 = binary_expr->getArg(0);
  NUExpr *expr2 = binary_expr->getArg(1);

  // analyze the expressions. If either is constant and contains
  // unknowns then this binary op must be resynthesized.
  NUConst* castExpr1 = expr1->castConst ();
  NUConst* castExpr2 = expr2->castConst ();
  
  // For non-comparison operators, we leave the expression alone.
  // Except for mod and divide, in that case change to an x if the
  // second arg is an x.
  switch (op) {
  case NUOp::eBiPlus:
  case NUOp::eBiMinus:
  case NUOp::eBiSMult:
  case NUOp::eBiUMult:
  case NUOp::eBiLshift:
  case NUOp::eBiRshift:
  case NUOp::eBiLshiftArith:
  case NUOp::eBiRshiftArith:
  case NUOp::eBiBitOr:
  case NUOp::eBiBitAnd:
  case NUOp::eBiBitXor:
    return out_expr;
    break;

  case NUOp::eBiSDiv:
  case NUOp::eBiUDiv:
  case NUOp::eBiSMod:
  case NUOp::eBiUMod:
    if (castExpr2 && castExpr2->castConstXZ ()) {
      break;
    } else {
      return out_expr;
    }
    break;

  default:
    // fallthrough
    break;
  }

  if ((castExpr1 && castExpr1->castConstXZ ()) || (castExpr2 && castExpr2->castConstXZ ())) {
    // oh swell. We get to resynth

    // we need the maximum size of the expression to properly
    // size the constants

    // size the expressions
    expr1->resize(expr1->determineBitSize());
    expr2->resize(expr2->determineBitSize());

    UInt32 maxSize = std::max(expr1->getBitSize(), expr2->getBitSize());
    // check against the lwidth
    maxSize = std::max(maxSize, static_cast<UInt32>(lWidth));

    UtString constValStr;
    NUConst* constVal = NULL;
    char polarity = 'm';
    UInt64 intVal = 0;

    // Next, if both are constants then take the Rvalue's
    // polarity. The polarity is used for value-creating binary ops.
    if (castExpr1 && castExpr1->drivesZ())
      polarity = 'z';
    else
      polarity = 'x';
    
    if (castExpr2 && castExpr2->drivesZ())
      polarity = 'z';
    else
      polarity = 'x';
    
    if (polarity == 'm') {
      mMessageContext->Verific2NUInfo(&loc, "Unknown constant");
      return out_expr;
    }

    switch (op)
    {
    case NUOp::eBiSDiv:
    case NUOp::eBiUDiv:
    case NUOp::eBiSMod:
    case NUOp::eBiUMod:
      constValStr.append(maxSize, polarity);
      constVal = NUConst::createXZ(constValStr, false, maxSize, loc);
      break;

      // These are always false if either expression has unknowns
    case NUOp::eBiEq:
    case NUOp::eBiNeq:
    case NUOp::eBiLogOr:
    case NUOp::eBiLogAnd:
    case NUOp::eBiSLt:
    case NUOp::eBiSLte:
    case NUOp::eBiSGtr:
    case NUOp::eBiSGtre:
    case NUOp::eBiULt:
    case NUOp::eBiULte:
    case NUOp::eBiUGtr:
    case NUOp::eBiUGtre:
      constVal = NUConst::create(false, 0ULL, 32, loc);
      break;
      
      // === and !===, result in false and true, respectively, except
      // if both operands are constants, in which case we let this
      // fall through (not setting constVal)
    case NUOp::eBiTrieq:
    case NUOp::eBiTrineq:
      if (! (castExpr1 && castExpr2))
      {
        intVal = (op == NUOp::eBiTrineq) ? 1 : 0;
        constVal = NUConst::create(false, intVal, 32, loc);
      }
      break;
      
    default:
      mMessageContext->Verific2NUInfo(&loc, "Unsupported binary expression");
      return out_expr;
      break;
    }
    
    if (constVal != NULL) {
      delete out_expr;
      out_expr = constVal;
    }
  }
  
  return out_expr;
}



/*************************************************************************************************************/
/****************************************** Nucleus DB creation **********************************************/
/*************************************************************************************************************/

NUPortConnection* VerificNucleusBuilder::createUnconnectedPortConnection(NUNet *formal, const SourceLocator& loc)
{
    NUPortConnection *the_conn = 0;
    if (formal->isInput()) {
        the_conn = new NUPortConnectionInput(0, formal, loc);
    } else if (formal->isOutput()) {
        the_conn = new NUPortConnectionOutput(0, formal, loc);
    } else if (formal->isBid()) {
        the_conn = new NUPortConnectionBid(0, formal, loc);
    } else {
        NUCLEUS_CONSISTENCY(false, loc, "Unknown port type");
    }
    return the_conn;
}

NUModule*
VerificNucleusBuilder::cModule(VeriTreeNode* ve_module, VhdlTreeNode* vh_module, const UtString& name, const UtString& origModuleName, bool isVerilog, const SourceLocator& loc, bool timescaleSpecified, SInt8 timeUnit, SInt8 timePrecision)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "Creating Nucleus module: "  << name.c_str() << std::endl;
#endif
    STSymbolTable* aliasDB = NULL;
    //local sym tab for each module
    aliasDB = new STSymbolTable(mDesign->getAliasBOM(), mAtomicCache);
    aliasDB->setHdlHier(new HdlVerilogPath);
    StringAtom* moduleNameAtom = mAtomicCache->intern(name);
    StringAtom* origModuleNameAtom = mAtomicCache->intern(origModuleName);

    NUModule* module = new NUModule(
            mDesign,
            mIsTopLevel,
            moduleNameAtom,
            aliasDB,
            gNetRefFactory(),
            mAtomicCache,
            loc,
            mIODB,
            timeUnit,
            timePrecision,
            timescaleSpecified,
            gNucleusLangType(isVerilog));
    // Register module
    registerModule(ve_module, vh_module, name, module);
    // First time this function is always called for top-level module
    if (mIsTopLevel) {
        mTopModuleName = name;
        mTopModule = module;
        aTopLevelModule(module);
        uDesign();
        mIsTopLevel = false;
    } else {
        aModule(module); 
    }
    module->putOriginalName(origModuleNameAtom);

    return module;
}

bool
VerificNucleusBuilder::IsTopModule(const UtString& name)
{
    return (((UtString("") == mTopModuleName) && (mIsTopLevel)) || (name == mTopModuleName));
}

// create a NuNet for a port (scalar port)
NUNet* 
VerificNucleusBuilder::cPort(NUScope* scope, const UtString& portName, unsigned direction, unsigned portType, bool isSigned, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cPort" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(scope, loc, "NULL scope object");
    NUScope::ScopeT scope_type = scope->getScopeType(); 
    StringAtom* name = mAtomicCache->intern(portName);
    NetFlags f =   createPortFlags(scope, name, direction, isSigned, loc);
    if (eNoneNet == f) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for port '" << portName.c_str() << "'";
            NUCLEUS_INFO_ASSERT(false, loc, msg.c_str());
        }
        return 0;
    }
    bool isParentTF = false; // non-static probably for nets only not ports, 
    NetFlags nT = gNucleusNetType(portType, false, false, scope_type, isSigned, isParentTF, true, loc);
    if (eNoneNet == nT) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for port type '" << VeriNode::PrintToken(portType) << "'";
            NUCLEUS_INFO_ASSERT(false, loc, msg.c_str());
        }
        return 0;
    }
    f = NetRedeclare(f, nT);
    return (new NUBitNet(name, f, scope, loc));
}

// create a NuNet for a port (vector port, using dimensions _dim_)
NUNet* 
VerificNucleusBuilder::cPort(NUScope* scope, const UtString& portName, unsigned direction, const UtPair<int,int>& dim, unsigned portType, bool isSigned, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cPort vector" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(scope, loc, "NULL scope object");
    NUScope::ScopeT scope_type = scope->getScopeType(); 
    StringAtom* name = mAtomicCache->intern(portName);
    NetFlags f = createPortFlags(scope, name, direction, isSigned, loc);
    if (eNoneNet == f) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for port '" << portName.c_str() << "'";
            NUCLEUS_INFO_ASSERT(false, loc, msg.c_str());
        }
        return 0;
    }
    bool isParentTF = false; // non-static probably for nets only not ports, 
    NetFlags nT = gNucleusNetType(portType, false, false, scope_type, isSigned, isParentTF, true, loc);
    if (eNoneNet == nT) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for port type '" << VeriNode::PrintToken(portType) << "'";
            NUCLEUS_INFO_ASSERT(false, loc, msg.c_str());
        }
        return 0;
    }
    f = NetRedeclare(f, nT);
    NUNet* net = (new NUVectorNet(name, ConstantRange(dim.first, dim.second), f, VectorNetFlags(0), scope, loc));
    if (net->getBitSize() == 0) {
        mMessageContext->ObjectTooLarge( &loc );
        setError(true);
    }
    return net;
}

NUNet* 
VerificNucleusBuilder::cPort(NUScope* scope, const UtString& portName, unsigned direction, const UtPair<int,int>& dim1, const UtPair<int,int>& dim2, unsigned portType, bool isSigned, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cPort 2-dim memory" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(scope, loc, "NULL scope object");
    NUScope::ScopeT scope_type = scope->getScopeType(); 
    ConstantRange* range1 = new ConstantRange(dim1.first, dim1.second);
    ConstantRange* range2 = new ConstantRange(dim2.first, dim2.second);
    StringAtom* name = mAtomicCache->intern(portName);
    NetFlags f = createPortFlags(scope, name, direction, isSigned, loc);
    if (eNoneNet == f) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for port '" << portName.c_str() << "'";
            NUCLEUS_INFO_ASSERT(false, loc, msg.c_str());
        }
        return 0;
    }
    bool isParentTF = false; // non-static probably for nets only not ports, 
    NetFlags nT = gNucleusNetType(portType, false, true, scope_type, isSigned, isParentTF, true, loc);
    if (eNoneNet == nT) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for port type '" << VeriNode::PrintToken(portType) << "'";
            NUCLEUS_INFO_ASSERT(false, loc, msg.c_str());
        }
        return 0;
    }
    f = NetRedeclare(f, nT);
    NUNet* net = (new NUMemoryNet(name, range1, range2, f, VectorNetFlags(0), scope, loc));
    if (net->getBitSize() == 0) {
        mMessageContext->ObjectTooLarge( &loc );
        setError(true);
    }
    return net;
}

NUNet* 
VerificNucleusBuilder::cPort(NUScope* scope, const UtString& portName, unsigned direction, unsigned firstUnpackedDim, const UtVector<UtPair<int,int> >& dims, unsigned portType, bool isSigned, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cPort multi-dim memory" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(scope, loc, "NULL scope object");
    NUScope::ScopeT scope_type = scope->getScopeType(); 
    ConstantRangeArray crs;
    UtVector<UtPair<int,int> >::const_iterator it = dims.begin();
    for (; it != dims.end(); ++it) {
        ConstantRange* cr = new ConstantRange(it->first, it->second);
        crs.push_back(cr);
    }
    StringAtom* name = mAtomicCache->intern(portName);
    NetFlags f = createPortFlags(scope, name, direction, isSigned, loc);
    if (eNoneNet == f) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for port '" << portName.c_str() << "'";
            NUCLEUS_INFO_ASSERT(false, loc, msg.c_str());
        }
        return 0;
    }
    bool isParentTF = false; // non-static probably for nets only not ports, 
    NetFlags nT = gNucleusNetType(portType, false, true, scope_type, isSigned, isParentTF, true, loc);
    if (eNoneNet == nT) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for port type '" << VeriNode::PrintToken(portType) << "'";
            NUCLEUS_INFO_ASSERT(false, loc, msg.c_str());
        }
        return 0;
    }
    f = NetRedeclare(f, nT);
    NUNet* net = (new NUMemoryNet(name, firstUnpackedDim, &crs, f, VectorNetFlags(0), scope, loc));
    if (net->getBitSize() == 0) {
        mMessageContext->ObjectTooLarge( &loc );
        setError(true);
    }
    return net;
}

NUNet* VerificNucleusBuilder::cFunctionOutputNet(NUScope* scope, const UtString& netName, unsigned type, bool isTemp, bool isSigned, NUTask* f, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cFunctionNet" << std::endl;
#endif
    // output
    NUCLEUS_CONSISTENCY(f, loc, "NULL object for task/function");
    NUCLEUS_CONSISTENCY(0 < (unsigned)f->getNumArgs(), loc, "Out of range index for task/function argument");
    NUNet* o = f->getArg(0);
    isTemp = true; // Function output net is always temporary
    if (o->isVectorNet()) {
        NUVectorNet* oVectorNet = static_cast<NUVectorNet*>(o);
        const ConstantRange* oRange = oVectorNet->getDeclaredRange();
        UtPair<int,int> dim(oRange->getMsb(), oRange->getLsb()); 
        return cNet(scope, netName, type, dim, isTemp, isSigned, loc);
    } else {
        return cNet(scope, netName, type, isTemp, isSigned, loc);
    }
}

NUNet*
VerificNucleusBuilder::cTempNet(NUScope* scope, const UtString& netName, const SourceLocator& loc)
{
    return (scope->createTempNet(mAtomicCache->intern(netName), 32, false /* unsigned */, loc));
}

NUNet*
VerificNucleusBuilder::cNet(NUScope* scope, const UtString& netName, unsigned type, bool isTemp, bool isSigned, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cNet" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(scope, loc, "NULL scope object");
    NUScope::ScopeT scope_type = scope->getScopeType(); 
    bool isParentTF = isScopeTF(scope);
    // it is possible to have a signed scalar net, which can hold the values 0 and -1, so we use signType here 
    NetFlags nT = gNucleusNetType(type, isTemp, false, scope_type, isSigned, isParentTF, false, loc);
    if (eNoneNet == nT) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for net type '" << VeriNode::PrintToken(type) << "'";
            NUCLEUS_INFO_ASSERT(false, loc, msg.c_str());
        }
        return 0;
    }
    return (new NUBitNet(mAtomicCache->intern(netName), nT, scope, loc));
}

NUNet*
VerificNucleusBuilder::cNet(NUScope* scope, const UtString& netName, unsigned type, const UtPair<int,int>& dim, bool isTemp, bool isSigned, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cNet vector" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(scope, loc, "NULL scope object");
    NUScope::ScopeT scope_type = scope->getScopeType(); 
    bool isParentTF = isScopeTF(scope);
    NetFlags nT = gNucleusNetType(type, isTemp, false, scope_type, isSigned, isParentTF, false, loc);
    if (eNoneNet == nT) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for net type '" << VeriNode::PrintToken(type) << "'";
            NUCLEUS_INFO_ASSERT(false, loc, msg.c_str());
        }
        return 0;
    }
    return (new NUVectorNet(mAtomicCache->intern(netName), ConstantRange(dim.first, dim.second), nT, VectorNetFlags(0), scope, loc));
}

NUNet*
VerificNucleusBuilder::cNet(NUScope* scope, const UtString& netName, unsigned type,
    const UtPair<int,int>& dim1, const UtPair<int,int>& dim2, bool isTemp, bool isSigned, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cNet memory" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(scope, loc, "NULL scope object");
    NUScope::ScopeT scope_type = scope->getScopeType(); 
    ConstantRange* range1 = new ConstantRange(dim1.first, dim1.second);
    ConstantRange* range2 = new ConstantRange(dim2.first, dim2.second);
    bool isParentTF = isScopeTF(scope);
    NetFlags nT = gNucleusNetType(type, isTemp, true, scope_type, isSigned, isParentTF, false, loc);
    if (eNoneNet == nT) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for net type '" << VeriNode::PrintToken(type) << "'";
            NUCLEUS_INFO_ASSERT(false, loc, msg.c_str());
        }
        return 0;
    }
    NUNet* net = (new NUMemoryNet(mAtomicCache->intern(netName), range1, range2, nT, VectorNetFlags(0), scope, loc));
    if (net->getBitSize() == 0) {
        mMessageContext->ObjectTooLarge( &loc );
        setError(1);
    }
    return net;
}

bool
VerificNucleusBuilder::isScopeTF(NUScope* scope)
{
    NUTF* tf = scope->getTFScope();
    bool isParentTF = false;
    if (tf) isParentTF = true;
    return isParentTF;
}

NUNet*
VerificNucleusBuilder::cNet(NUScope* scope, const UtString& netName, unsigned type, unsigned firstUnpackedDim, const UtVector<UtPair<int,int> >& dims, bool isTemp, bool isSigned, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cNet memory" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(scope, loc, "NULL scope object");
    NUScope::ScopeT scope_type = scope->getScopeType(); 
    ConstantRangeArray crs;
    UtVector<UtPair<int,int> >::const_iterator it = dims.begin();
    for (; it != dims.end(); ++it) {
        ConstantRange* cr = new ConstantRange(it->first, it->second);
        crs.push_back(cr);
    }
    bool isParentTF = isScopeTF(scope);
    NetFlags nT = gNucleusNetType(type, isTemp, true, scope_type, isSigned, isParentTF, false, loc);
    if (eNoneNet == nT) {
        // eNoneNet net type can appear only if some error occurred, so assert it and return NULL
        if (!hasError()) {
            UtString msg;
            msg << "Missing error report for net '" << VeriNode::PrintToken(type) << "'";
            NUCLEUS_INFO_ASSERT(false, loc, msg.c_str());
        }
        return 0;
    }
    NUNet* net = (new NUMemoryNet(mAtomicCache->intern(netName), firstUnpackedDim, &crs, nT, VectorNetFlags(0), scope, loc));
    if (net->getBitSize() == 0) {
        mMessageContext->ObjectTooLarge( &loc );
        setError(true);
    }
    return net;
}

// create correct identRvalue object based on net type (composite nets have special NUCompositeIdentRvalue/NUCompositeIdentLvalue 
// objects, for all other nets, NUIdentRvalue/NUIdentLvalue is available)
NUExpr* VerificNucleusBuilder::createIdentRvalue(NUNet* net, const SourceLocator& loc)
{
    NUExpr* identRvalue = NULL;
    if (net->isCompositeNet()) {
        identRvalue = new NUCompositeIdentRvalue(net->getCompositeNet(), loc);
    } else {
        identRvalue = new NUIdentRvalue(net, loc);
    }
    return identRvalue;
}

NULvalue* VerificNucleusBuilder::createIdentLvalue(NUNet* net, const SourceLocator& loc)
{
    NULvalue* identLvalue = NULL;
    if (net->isCompositeNet()) {
        identLvalue = new NUCompositeIdentLvalue(net->getCompositeNet(), loc);
    } else {
        identLvalue = new NUIdentLvalue(net, loc);
    }
    return identLvalue;
}

NULvalue*
VerificNucleusBuilder::cIdentLvalue(NUNet* net, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cIdentLvalue" << std::endl;
#endif
    return new NUIdentLvalue(net, loc);;
}

NUExpr*
VerificNucleusBuilder::cIdentRvalue(NUNet* net, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cIdentRvalue" << std::endl;
#endif
    NUExpr* identRvalue = new NUIdentRvalue(net, loc);
    identRvalue->setSignedResult(net->isSigned());
    return identRvalue;
}


NUIf*
VerificNucleusBuilder::cIf(NUExpr* cond, const NUStmtList& thenStmts, const NUStmtList& elseStmts, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cIf" << std::endl;
#endif
    bool usesCFNet = false;
    return (new NUIf(cond, thenStmts, elseStmts, gNetRefFactory(), usesCFNet, loc));
}

NUExpr*
VerificNucleusBuilder::cTernaryOperation(NUExpr* expr1, NUExpr* expr2, NUExpr* expr3, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cTernaryOperation" << std::endl;
#endif
    // RJC RC#15, I checked the interra flow for ternary operators and I see that there the signedness is set on the operands and result based on the signedness information from interra, I suspect that this is a bug just waiting to happen here with these commented out but setting them to false was also probably wrong
    // ADDRESSED FD: Probably this will come with verilog2001 tests, so just keeping the todo below for future reference
    //TODO in case this is really needed, this should look to optinos whether signed operation performed
    //expr2->setSignedResult(false);
    //expr3->setSignedResult(false);
    NUExpr* result = new NUTernaryOp(NUOp::eTeCond, expr1, expr2, expr3, loc);
    //result->setSignedResult(false);
    return result;
}

NUExpr*
VerificNucleusBuilder::cBinaryOperation(NUExpr* first, unsigned opType, NUExpr* second, bool exprIsSigned, bool isSignedOp, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cBinaryOperation" << std::endl;
#endif
    NUOp::OpT op = gNucleusBinOp(opType, isSignedOp);

    bool is_signed_result = exprIsSigned;

    // now apply rules that may change the signedness of binary operation
    switch ( op ){
    case NUOp::eBiUMult:
    case NUOp::eBiUDiv:
    case NUOp::eBiUMod:
    case NUOp::eBiEq:
    case NUOp::eBiNeq:
    case NUOp::eBiTrieq:
    case NUOp::eBiTrineq:
    case NUOp::eBiLogOr:
    case NUOp::eBiLogAnd:
    case NUOp::eBiSLt:
    case NUOp::eBiULt:
    case NUOp::eBiSGtr:
    case NUOp::eBiUGtr:
    case NUOp::eBiSLte:
    case NUOp::eBiULte:
    case NUOp::eBiSGtre:
    case NUOp::eBiUGtre:
    {
      is_signed_result = false;    // LRM says these produce unsigned results
      break;
    }
    default:{
      is_signed_result |= (first->isReal () || second->isReal ()); // LRM also says if either operand is real then result is signed
      break;
    }
    }

    if (op == NUOp::eBiExp ) {
      // this is the Verilog POW() operator "**") adjust the operator type and coerce operands as necessary 

      bool usingVerilog2005StylePowerOperator = getCarbonContext()->usingVerilog2005StylePowerOperator();
  
      if ( usingVerilog2005StylePowerOperator ) {
        // the Verilog 2005, and SystemVerilog LRMs say that the result of POW operator is real if either operand is real
        if ( first->isReal () || second->isReal () ){
          op = NUOp::eBiDExp;
        }
      } else {
        // The Verilog 2001 LRM says POW involving signed, integer or real operands produces real result, 
        // and thus the non-self determined operands are to be coerced to real before evaluation

        if (first->isReal () ||
            second->isReal () ||
            first->isSignedResult () ||
            second->isSignedResult ()  ) {
          op = NUOp::eBiDExp;
        }
      }
      // and LRM says that the non-self determined operands should be
      // coerced to the type of the expression, the second operand of power operator is self determined so we only potentially adjust the
      // first operand here.
      if ( ( NUOp::eBiDExp == op ) && ( not (first->isReal()) ) ){
        first = first->coerceOperandsToReal();
        if ( not first->isReal() ) {
          // found some invalid verilog. test/langcov/Real/coerce_02.v
          mMessageContext->CoercedRealOperandNotCompatible(first);
          delete first;
          delete second;
          return 0;
          // If this were valid verilog then we must resize before we
          // wrap with conversion to real so that no precision is lost.
          // first = first->makeSizeExplicit(64);
          // first = new NUUnaryOp (NUOp::eUnItoR, first, first->getLoc ());
        }
      }
    }

    if (first->isReal() != second->isReal()) {
        // REAL ** i , i ** REAL are okay, others must be converted to reals
        // TODO this really should coerce all non-self determined operands to real
        if ( op != NUOp::eBiDExp ){
            if ( first->isReal() ){
                NU_ASSERT(not second->isReal(), second);
                second = new NUUnaryOp (NUOp::eUnItoR, second, second->getLoc ());
            } else {
                NU_ASSERT(not first->isReal(), first);
                first = new NUUnaryOp (NUOp::eUnItoR, first, first->getLoc ());
            }
        }
    }
    NUBinaryOp* bop = new NUBinaryOp(op, first, second, loc);
    bop->setSignedResult (is_signed_result);
    fixSign(bop);
    UInt32 lWidth = bop->determineBitSize();
    NUExpr* cleanedBop = cleanupXZBinaryExpr(bop, lWidth, loc); // is lWidth correct here? or  should it be the size of the expression (perhaps determined by the lhs of an assignment or the context?)
    cleanedBop->resize(cleanedBop->determineBitSize());       
    return cleanedBop;
}

NUExpr*
VerificNucleusBuilder::cUnaryOperation(NUExpr* expr, unsigned opType, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cUnaryOperation" << std::endl;
#endif
    NUUnaryOp* uop = new NUUnaryOp(gNucleusUnOp(opType), expr, loc);
    // trying to emulate interra's veExprIsSigned api behavioral
    // uop->setSignedResult (veExprIsSigned (ve_expr) == VE_TRUE); // PopulateExpr.cxx:1131
    // make it explicit: the following unary operators results are always unsigned (!, &, |, ^)
    // for others (~, +, -, ++, --) (any other ?) sign depends on operand signedness
    switch(uop->getOp()) {
        case NUOp::eUnLogNot:
        case NUOp::eUnRedAnd:
        case NUOp::eUnRedOr:
        case NUOp::eUnRedXor:
            uop->setSignedResult(false);
            break;
        default:
            uop->setSignedResult(expr->isSignedResult());
    }
    // fix operand sign (copied Interra solution)
    fixSign(uop);
    return uop;
}

NUContAssign*
VerificNucleusBuilder::cContinuesAssign(NUScope* scope, NULvalue* lvalue, NUExpr* rvalue, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cContinuesAssign" << std::endl;
#endif
    StringAtom* name = scope->newBlockName(mAtomicCache, loc);
    if (lvalue->isReal() != rvalue->isReal()) {
        rvalue = new NUUnaryOp (NUOp::eUnRound, rvalue, loc);
    }
    return (new NUContAssign(name, lvalue, rvalue, loc));
}

NUStmtList*
VerificNucleusBuilder::cStmtList()
{
    return (new NUStmtList);
}

NUBlockingAssign*
VerificNucleusBuilder::cBlockingAssign(NULvalue* lvalue, NUExpr* rvalue, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cBlockingAssign" << std::endl;
#endif
    bool usesCFNet = false;
    bool do_resize = true; //TODO will come from verific

    if (lvalue->isReal() != rvalue->isReal()) {
        if (rvalue->isReal()) {
            // needs to be converted from real
            NUConst* nConst = rvalue->castConst();
            if (nConst != NULL) {
                NUConst* newConst = NULL;
                unsigned lWidth = lvalue->getBitSize();
                newConst = convertRealConstantToIntConstant(nConst, lWidth, loc);
                if (newConst != NULL) {
                    delete rvalue;
                    rvalue = newConst;
                }
            } else {
                rvalue = new NUUnaryOp(NUOp::eUnRound, rvalue, loc);
            }
        } else {
            // needs to be converted to real
            rvalue = new NUUnaryOp (NUOp::eUnItoR, rvalue, loc);
        }
    }
    NUBlockingAssign* ba = (new NUBlockingAssign(lvalue, rvalue, usesCFNet, loc, do_resize));
    return ba;
}


NUNonBlockingAssign*
VerificNucleusBuilder::cNonBlockingAssign(NULvalue* lvalue, NUExpr* rvalue, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cNonBlockingAssign" << std::endl;
#endif
    bool usesCFNet = false;
    bool do_resize = true; //TODO will come from verific
    // Allow real constants to be assigned to non-real nets
    if (!lvalue->isReal() && rvalue->isReal()) {
        NUConst* nConst = rvalue->castConst();
        if (nConst) {
            NUConst *newConst = NULL;
            unsigned lWidth = lvalue->getBitSize();
            newConst = convertRealConstantToIntConstant(nConst, lWidth, loc);
            if (newConst != NULL) {
                delete rvalue;
                rvalue = newConst;
            }
        } else {
            rvalue = new NUUnaryOp(NUOp::eUnRound, rvalue, loc);
        }
    }
    return (new NUNonBlockingAssign(lvalue, rvalue, usesCFNet, loc, do_resize));
}

NUTask*
VerificNucleusBuilder::cTF(const UtString& name, const UtString& origName, NUModule* parentModule, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cTF" << std::endl;
#endif
    StringAtom* tfName = mAtomicCache->intern(name);
    StringAtom* tfOrigName = mAtomicCache->intern(origName);
    NUTask* nt = new NUTask(parentModule, tfName, tfOrigName, gNetRefFactory(), loc, gIODB());
    return nt;
}

NUTaskEnable*
VerificNucleusBuilder::createTaskEnable(NUTask* tf, NUModule* module, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cTaskEnable" << std::endl;
#endif
    bool usesCFNet = false;
    NUTaskEnable* te = new NUTaskEnable(tf, module, gNetRefFactory(), usesCFNet, loc);
    return te;
}

NUTFArgConnection*
VerificNucleusBuilder::cTFArgConnection(NUBase* actual, unsigned portIndex, NUTask* tf, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cTFArgConnection" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(tf, loc, "NULL object for task/function");
    NUCLEUS_CONSISTENCY(portIndex < (unsigned)tf->getNumArgs(), loc, "Out of range index for task/function argument");
    NUNet* p = tf->getArg(portIndex);
    NUTFArgConnection* argConnect = NULL;
    if (p->isInput()) {
        NUExpr* nActual = dynamic_cast<NUExpr*>(actual);
        NUCLEUS_CONSISTENCY(nActual, loc, "Incorrect input actual");
        nActual = nActual->makeSizeExplicit(p->getBitSize());
        argConnect = new NUTFArgConnectionInput(nActual, p, tf, loc);
    } else if (p->isOutput()) {
        NULvalue* nActual = dynamic_cast<NULvalue*>(actual);
        NUCLEUS_CONSISTENCY(nActual, loc, "Incorrect output actual");
        nActual->resize();
        argConnect = new NUTFArgConnectionOutput(nActual, p, tf, loc);
    } else if (p->isBid()) {
        NULvalue* nActual = dynamic_cast<NULvalue*>(actual);
        NUCLEUS_CONSISTENCY(nActual, loc, "Incorrect inout actual");
        nActual->resize();
        argConnect = new NUTFArgConnectionBid(nActual, p, tf, loc);
    } 
    return argConnect;
}

NUPortConnection* 
VerificNucleusBuilder::cPortConnection(NUBase* actual, unsigned portIndex, NUModule* module, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cPortConnection" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(module, loc, "NULL object for module");
    NUCLEUS_CONSISTENCY(portIndex < (unsigned)module->getNumPorts(), loc, "Out of range index for module port");
    NUNet* p = module->getPortByIndex(portIndex);
    NUPortConnection* portConnect = NULL;
    if (p->isInput()) {
        NUExpr* nActual = dynamic_cast<NUExpr*>(actual);
        // nActual can be empty
        portConnect = new NUPortConnectionInput(nActual, p, loc);
    } else if (p->isOutput()) {
        NULvalue* nActual = dynamic_cast<NULvalue*>(actual);
        // nActual can be empty
        portConnect = new NUPortConnectionOutput(nActual, p, loc);
    } else if (p->isBid()) {
        NULvalue* nActual = dynamic_cast<NULvalue*>(actual);
        // nActual can be empty
        portConnect = new NUPortConnectionBid(nActual, p, loc);
    } 
    return portConnect;
}

NUPortConnection*
VerificNucleusBuilder::cPortConnection(NUBase* actual, const UtString& name, NUModule* module, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cPortConnection named" << std::endl;
#endif
    NUNet* p = NULL;
    for (NUNetVectorLoop l = module->loopPorts(); !l.atEnd(); ++l)
    {
        NUNet* net = *l;
        if (UtString(net->getName()->str()) == name) {
            p = net;
        }
    }
    //StringAtom* n = mAtomicCache->intern(name.c_str());
    //std::cerr << "N = " << n->str() << std::endl;
    //NUNet* p = nModule->lookupNet(n);
    NUCLEUS_CONSISTENCY(p, loc, "Unable to get Net");
    NUPortConnection* portConnect = NULL;
    if (p->isInput()) {
        NUExpr* nActual = dynamic_cast<NUExpr*>(actual);
        NUCLEUS_CONSISTENCY(nActual, loc, "Incorrect input actual");
        portConnect = new NUPortConnectionInput(nActual, p, loc);
    } else if (p->isOutput()) {
        NULvalue* nActual = dynamic_cast<NULvalue*>(actual);
        NUCLEUS_CONSISTENCY(nActual, loc, "Incorrect output actual");
        portConnect = new NUPortConnectionOutput(nActual, p, loc);
    } else if (p->isBid()) {
        NULvalue* nActual = dynamic_cast<NULvalue*>(actual);
        NUCLEUS_CONSISTENCY(nActual, loc, "Incorrect inout actual");
        portConnect = new NUPortConnectionBid(nActual, p, loc);
    } 
    return portConnect;
}

NUPortConnection*
VerificNucleusBuilder::cPortConnection(NUBase* actual, NUNet* formal, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cPortConnection named2" << std::endl;
#endif
    NUNet* p = static_cast<NUNet*>(formal); 
    NUCLEUS_CONSISTENCY(p, loc, "Unable to get net");
    NUPortConnection* portConnect = NULL;
    if (p->isInput()) {
        NUExpr* nActual = dynamic_cast<NUExpr*>(actual);
        // actual can be NULL
        portConnect = new NUPortConnectionInput(nActual, p, loc);
    } else if (p->isOutput()) {
        NULvalue* nActual = dynamic_cast<NULvalue*>(actual);
        // actual can be NULL
        portConnect = new NUPortConnectionOutput(nActual, p, loc);
    } else if (p->isBid()) {
        NULvalue* nActual = dynamic_cast<NULvalue*>(actual);
        // actual can be NULL
        portConnect = new NUPortConnectionBid(nActual, p, loc);
    } 
    return portConnect;
}

// create and return an always block, add the newly created always block to \a scope (which must be a NUModule)
// Note: caller must set the is_wildcard and the always_type
NUAlwaysBlock*
VerificNucleusBuilder::createAlwaysBlockInScope(NUScope* scope, NUBlock* top_block, bool is_wildcard, NUAlwaysBlock::NUAlwaysT always_type, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API createAlwaysBlockInScope" << std::endl;
#endif
    NUScope::ScopeT scope_type = scope->getScopeType();
    StringAtom* name = scope->newBlockName(mAtomicCache, loc);
    NUAlwaysBlock* always_block = new NUAlwaysBlock(name, top_block, gNetRefFactory(), loc, is_wildcard, always_type);
    switch ( scope_type ){
    case NUScope::eModule: {
      NUModule* module = dynamic_cast<NUModule*>(scope);
      addAlwaysBlockToModule(always_block, module);
      break;
    }
    case NUScope::eTask:        // is it possible to have an always block in a task? if so this needs to be changed
    case NUScope::eBlock:
    case NUScope::eNamedDeclarationScope:
    case NUScope::eUserTask:{
      UtString scope_type_str = scope->getScopeTypeStr();
      getMessageContext()->UnsupportedAlwaysBlockInContext( &loc, scope_type_str.c_str());
    }
    }
    return always_block;
}

NUBlock*
VerificNucleusBuilder::cBlock(NUScope* scope, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cBlock" << std::endl;
#endif
    bool usesCFNet = false;
    return (new NUBlock(0, scope, mIODB, gNetRefFactory(), usesCFNet, loc));
}

NUExpr*
VerificNucleusBuilder::cEventExpr(unsigned edge, NUExpr* edgeNetExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cEdgeExpr" << std::endl;
#endif
    NUIdentRvalue* e = static_cast<NUIdentRvalue*>(edgeNetExpr);
    NUNet* n = e->getIdent();
    if (edge) {
        setNetFlag(n, eEdgeTrigger);
    }
    NUExpr* eExpr = e;
    NUExpr* eventExpr = NULL;
    if (edge) {
        eventExpr = new NUEdgeExpr(gNucleusClockEdge(edge), static_cast<NUExpr*>(eExpr), loc);
    } else {
        eventExpr = eExpr; 
    }
    return eventExpr;
}

NUConst*
VerificNucleusBuilder::cConst(const UtString& val, unsigned bitWidth, bool hasXZ, bool isSigned, bool isString, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cConst string" << std::endl;
#endif
    if (hasXZ) {
        return NUConst::createXZ(val, isSigned, bitWidth, loc);
    } else if (isString) {
        return NUConst::createKnownConst(val, bitWidth, isString, loc);
    } else {
        NUConst* valExpr = NUConst::createKnownConst(val, bitWidth, loc); 
        valExpr->setSignedResult(isSigned); 
        return valExpr;
    }
}

NUConst*
VerificNucleusBuilder::cConst(unsigned val, unsigned bitWidth, bool isSigned, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cConst integer" << std::endl;
#endif
    bool definedByString = false;
    NUConst* valExpr = NUConst::createKnownIntConst(val, bitWidth, definedByString, loc); 
    valExpr->setSignedResult(isSigned);
    return valExpr;
}

NUConst*
VerificNucleusBuilder::cConst(double val, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cConst real" << std::endl;
#endif
    NUConst* valExpr = NUConst::create( CarbonReal(val), loc);
    // real is always signed
    valExpr->setSignedResult(true);
    return valExpr;
}


NUMemselRvalue*
VerificNucleusBuilder::cMemselRvalue(NUMemoryNet* net, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cMemselRvalue" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(indexExpr, loc, "null index expression");
    NUMemselRvalue* value = new NUMemselRvalue(net, indexExpr, loc);
    return value;
}

NUMemselRvalue*
VerificNucleusBuilder::cMemselRvalue(NUMemoryNet* net, NUExprVector* indexExprs, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cMemselRvalue" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(indexExprs->size() > 0, loc, "ERROR: No index expression in memory select");
    NUExprVector normExprs;
    for (NUExprVector::iterator iter = indexExprs->begin(); iter != indexExprs->end(); ++iter) {
        NUExpr* indexExpr = *iter;
        NUCLEUS_CONSISTENCY(indexExpr, loc, "Unable to get index expresion");
        NUExpr* normExpr = net->normalizeSelectExpr(indexExpr, 0, 0, 1); 
        NUCLEUS_CONSISTENCY(normExpr, loc, "Expression out of range error");
        normExprs.push_back(normExpr);
        delete indexExpr;
        NUCLEUS_CONSISTENCY(normExpr, loc, "Unable to get normalized index expression");
    }
    NUMemselRvalue* value = new NUMemselRvalue(net, &normExprs, loc);
    return value;
}

NUMemselRvalue*
VerificNucleusBuilder::cMemselRvalue(NUBitNet* net, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cMemselRvalue" << std::endl;
#endif
    return (new NUMemselRvalue(net, indexExpr, loc));
}

NUMemselRvalue*
VerificNucleusBuilder::cMemselRvalue(NUExpr* expr, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cMemselRvalue" << std::endl;
#endif
    return (new NUMemselRvalue(expr, indexExpr, loc));
}

NUMemselRvalue*
VerificNucleusBuilder::cMemselRvalue(NUBitNet* net, NUExprVector* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cMemselRvalue" << std::endl;
#endif
    return (new NUMemselRvalue(net, indexExpr, loc));
}

NUMemselRvalue*
VerificNucleusBuilder::cMemselRvalue(NUExpr* expr, NUExprVector* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cMemselRvalue" << std::endl;
#endif
    return (new NUMemselRvalue(expr, indexExpr, loc));
}



NUMemselLvalue*
VerificNucleusBuilder::cMemselLvalue(NUMemoryNet* net, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cMemselLvalue" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(indexExpr, loc, "null index expression");
    NUMemselLvalue* value = new NUMemselLvalue(net, indexExpr, loc);
    return value;
}

NUMemselLvalue*
VerificNucleusBuilder::cMemselLvalue(NUMemoryNet* net, NUExprVector* indexExprs, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cMemselLvalue" << std::endl;
#endif
    NUCLEUS_CONSISTENCY(indexExprs->size() > 0, loc, "ERROR: No index expression in memory select");
    NUExprVector normExprs;
    for (NUExprVector::iterator iter = indexExprs->begin(); iter != indexExprs->end(); ++iter) {
        NUExpr* indexExpr = *iter;
        NUCLEUS_CONSISTENCY(indexExpr, loc, "Unable to get index expresion");
        NUExpr* normExpr = net->normalizeSelectExpr(indexExpr, 0, 0, 1); 
        NUCLEUS_CONSISTENCY(normExpr, loc, "Expression out of range error");
        normExprs.push_back(normExpr);
        delete indexExpr;
        NUCLEUS_CONSISTENCY(normExpr, loc, "Unable to get normalized index expression");
    }
    NUMemselLvalue* value = new NUMemselLvalue(net, &normExprs, loc);
    return value;
}

NUMemselLvalue*
VerificNucleusBuilder::cMemselLvalue(NUBitNet* net, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cMemselLvalue" << std::endl;
#endif
    return (new NUMemselLvalue(net, indexExpr, loc));
}

NUMemselLvalue*
VerificNucleusBuilder::cMemselLvalue(NULvalue* lvalue, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cMemselLvalue" << std::endl;
#endif
    return (new NUMemselLvalue(lvalue, indexExpr, loc));
}

NUMemselLvalue*
VerificNucleusBuilder::cMemselLvalue(NUBitNet* net, NUExprVector* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cMemselLvalue" << std::endl;
#endif
    return (new NUMemselLvalue(net, indexExpr, loc));
}

NUMemselLvalue*
VerificNucleusBuilder::cMemselLvalue(NULvalue* lvalue, NUExprVector* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cMemselLvalue" << std::endl;
#endif
    return (new NUMemselLvalue(lvalue, indexExpr, loc));
}


NUVarselRvalue*
VerificNucleusBuilder::cVarselRvalue(NUVectorNet* net, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselRvalue" << std::endl;
#endif
    NUExpr* norm = net->normalizeSelectExpr(indexExpr, 0, true); 
    // if unable to normalize - use original index expression
    if (!norm) norm = indexExpr;
    NUCLEUS_CONSISTENCY(norm, loc, "Unable to get normalized index expression");
    NUVarselRvalue* value = new NUVarselRvalue(net, norm, loc);
    return value;
}

NUVarselRvalue*
VerificNucleusBuilder::cVarselRvalue(NUMemoryNet* net, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselRvalue" << std::endl;
#endif
    NUExpr* norm = net->normalizeSelectExpr(indexExpr, 0, 0, 1); 
    // if unable to normalize - use original index expression
    if (!norm) norm = indexExpr;
    NUCLEUS_CONSISTENCY(norm, loc, "Unable to get normalized index expression");
    NUVarselRvalue* value = new NUVarselRvalue(net, norm, loc);
    return value;
}


NUVarselRvalue*
VerificNucleusBuilder::cVarselRvalue(NUVectorNet* net, NUExpr* indexExpr, int adjust, const UtPair<int,int> & dim, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselRvalue" << std::endl;
#endif
    ConstantRange cr(dim.first, dim.second);
    NUExpr* norm = net->normalizeSelectExpr(indexExpr, adjust, true); 
    // if unable to normalize - use original index expression
    if (!norm) norm = indexExpr;
    NUCLEUS_CONSISTENCY(norm, loc, "Unable to get normalized index expression");
    NUVarselRvalue* value = new NUVarselRvalue(net, norm, cr, loc);
    return value;
}

NUVarselRvalue*
VerificNucleusBuilder::cVarselRvalue(NUMemoryNet* net, NUExpr* indexExpr, int adjust, const UtPair<int,int> & dim, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselRvalue" << std::endl;
#endif
    ConstantRange cr(dim.first, dim.second);
    NUExpr* norm = net->normalizeSelectExpr(indexExpr, 0, adjust, 1); 
    // if unable to normalize - use original index expression
    if (!norm) norm = indexExpr;
    NUCLEUS_CONSISTENCY(norm, loc, "Unable to get normalized index expression");
    NUVarselRvalue* value = new NUVarselRvalue(net, norm, cr, loc);
    return value;
}

NUVarselRvalue*
VerificNucleusBuilder::cVarselRvalue(NUBitNet* net, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselRvalue" << std::endl;
#endif
    return (new NUVarselRvalue(net, indexExpr, loc));
}

NUVarselRvalue*
VerificNucleusBuilder::cVarselRvalue(NUExpr* expr, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselRvalue" << std::endl;
#endif
    return (new NUVarselRvalue(expr, indexExpr, loc));
}

NUVarselRvalue*
VerificNucleusBuilder::cVarselRvalue(NUExpr* expr, NUExpr* indexExpr, const UtPair<int,int> & dim, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselRvalue" << std::endl;
#endif
    return (new NUVarselRvalue(expr, indexExpr, ConstantRange(dim.first, dim.second),  loc));
}

NUVarselLvalue*
VerificNucleusBuilder::cVarselLvalue(NUVectorNet* net, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselLvalue" << std::endl;
#endif
    NUExpr* norm = net->normalizeSelectExpr(indexExpr, 0, true); 
    // if unable to normalize - use original index expression
    if (!norm) norm = indexExpr;
    NUCLEUS_CONSISTENCY(norm, loc, "Unable to get normalized index expression");
    NUVarselLvalue* value = new NUVarselLvalue(net, norm, loc);
    return value;
}

NUVarselLvalue*
VerificNucleusBuilder::cVarselLvalue(NUVectorNet* net, NUExpr* indexExpr, int adjust, const UtPair<int,int>& dim, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselLvalue" << std::endl;
#endif
    ConstantRange cr(dim.first, dim.second);
    NUExpr* norm = net->normalizeSelectExpr(indexExpr, adjust, true); 
    // if unable to normalize - use original index expression
    if (!norm) norm = indexExpr;
    NUCLEUS_CONSISTENCY(norm, loc, "Unable to get normalized index expression");
    NUVarselLvalue* value = new NUVarselLvalue(net, norm, cr, loc);
    return value;
}

NUVarselLvalue*
VerificNucleusBuilder::cVarselLvalue(NUMemoryNet* net, NUExpr* indexExpr, int adjust, const UtPair<int,int>& dim, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselLvalue" << std::endl;
#endif
    ConstantRange cr(dim.first, dim.second);
    NUExpr* norm = net->normalizeSelectExpr(indexExpr, 0, adjust, 1); 
    // if unable to normalize - use original index expression
    if (!norm) norm = indexExpr;
    NUCLEUS_CONSISTENCY(norm, loc, "Unable to get normalized index expression");
    NUVarselLvalue* value = new NUVarselLvalue(net, norm, cr, loc);
    return value;
}

NUVarselLvalue*
VerificNucleusBuilder::cVarselLvalue(NUBitNet* net, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselLvalue" << std::endl;
#endif
    return (new NUVarselLvalue(net, indexExpr, loc));
}

NUVarselLvalue*
VerificNucleusBuilder::cVarselLvalue(NULvalue* lvalue, NUExpr* indexExpr, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselLvalue" << std::endl;
#endif
    return (new NUVarselLvalue(lvalue, indexExpr, loc));
}

NUVarselLvalue*
VerificNucleusBuilder::cVarselLvalue(NULvalue* lvalue, NUExpr* indexExpr, const UtPair<int,int>& dim, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselLvalue" << std::endl;
#endif
    return (new NUVarselLvalue(lvalue, indexExpr, ConstantRange(dim.first, dim.second), loc));
}


UtPair<int,int>
VerificNucleusBuilder::gNetDeclRange(NUVectorNet* net)
{
    return UtPair<int,int>(net->getDeclaredRange()->getMsb(), net->getDeclaredRange()->getLsb());
}

UtPair<int,int>
VerificNucleusBuilder::gNetDeclRange(NUMemoryNet* net, unsigned i)
{
    unsigned size = net->getNumDims();
    INFO_ASSERT(i < size, "index out of range");
    const ConstantRange * nthRange = net->getRange(i);
    return UtPair<int,int>(nthRange->getMsb(), nthRange->getLsb());
}

NUVarselRvalue*
VerificNucleusBuilder::cVarselRvalue(NUVectorNet* net, const UtPair<int,int> & dim, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselRvalue" << std::endl;
#endif
    ConstantRange cr(dim.first, dim.second);
    NUVarselRvalue* value = new NUVarselRvalue(net, cr, loc);
    return value;
}

NUVarselRvalue*
VerificNucleusBuilder::cVarselRvalue(NUMemoryNet* net, const UtPair<int,int> & dim, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselRvalue" << std::endl;
#endif
    ConstantRange cr(dim.first, dim.second);
    NUVarselRvalue* value = new NUVarselRvalue(net, cr, loc);
    return value;
}

NUVarselRvalue*
VerificNucleusBuilder::cVarselRvalue(NUBitNet* net, const UtPair<int,int> & dim, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselRvalue" << std::endl;
#endif
    return (new NUVarselRvalue(net, ConstantRange(dim.first, dim.second), loc));
}

NUVarselRvalue*
VerificNucleusBuilder::cVarselRvalue(NUExpr* expr, const UtPair<int,int> & dim, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselRvalue" << std::endl;
#endif
    return (new NUVarselRvalue(expr, ConstantRange(dim.first, dim.second), loc));
}

NUVarselLvalue*
VerificNucleusBuilder::cVarselLvalue(NUVectorNet* net, const UtPair<int,int> & dim , const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselLvalue" << std::endl;
#endif
    ConstantRange cr(dim.first, dim.second);
    NUVarselLvalue* value = new NUVarselLvalue(net, cr, loc);
    return value;
}

NUVarselLvalue*
VerificNucleusBuilder::cVarselLvalue(NUMemoryNet* net, const UtPair<int,int> & dim , const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselLvalue" << std::endl;
#endif
    ConstantRange cr(dim.first, dim.second);
    NUVarselLvalue* value = new NUVarselLvalue(net, cr, loc);
    return value;
}


NUVarselLvalue*
VerificNucleusBuilder::cVarselLvalue(NUBitNet* net, const UtPair<int,int> & dim , const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselLvalue" << std::endl;
#endif
    return (new NUVarselLvalue(net, ConstantRange(dim.first, dim.second), loc));
}

NUVarselLvalue*
VerificNucleusBuilder::cVarselLvalue(NULvalue* lvalue, const UtPair<int,int> & dim , const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cVarselLvalue" << std::endl;
#endif
    return (new NUVarselLvalue(lvalue, ConstantRange(dim.first, dim.second), loc));
}


NUModuleInstance*
VerificNucleusBuilder::cModuleInstance(const UtString& name, NUScope* parentScope, NUModule* masterModule, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cModuleInstance" << std::endl;
#endif
    return (new NUModuleInstance(mAtomicCache->intern(name), parentScope, masterModule, gNetRefFactory(), loc));
}


NULvalue*
VerificNucleusBuilder::cConcatLvalue(NULvalueVector& lValues, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cConcatLvalue" << std::endl;
#endif
    return (new NUConcatLvalue(lValues, loc));
}

NUExpr*
VerificNucleusBuilder::cConcatRvalue(NUExprVector& rValues, unsigned repeatCount, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cConcatRvalue" << std::endl;
#endif
    return (new NUConcatOp(rValues, repeatCount, loc));
}


NUCase*
VerificNucleusBuilder::cCase(NUExpr* sel, unsigned caseStyle, unsigned maxExprSize, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cCase" << std::endl;
#endif
    bool usesCFNet = false;
    if (maxExprSize > sel->getBitSize()) {
        sel = sel->makeSizeExplicit(maxExprSize);
    }
    return (new NUCase(sel, gNetRefFactory(), gNucleusCaseType(caseStyle), usesCFNet, loc));
}

NUCaseItem*
VerificNucleusBuilder::cCaseItem(const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cCaseItem" << std::endl;
#endif
    return (new NUCaseItem(loc));
}


//! cCaseMask:  returns a NUConst* that can be used as a bit mask to identify the specific bits of \a expr that are not x or z.
//             A 1 in the mask indicates that the bit at that position in the value is NOT a z or x.
//             if \a expr is not a constant then cCasexMask is called, which builds a mask for non-constant case conditions
//             if \a expr does not have any x or z then a null pointer is returned
NUConst*
VerificNucleusBuilder::cCaseMask(NUExpr* expr, const UtString& condValue, bool isCaseZ, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cCaseMask" << std::endl;
#endif
    // this method only returns a NUConst if the expr was itself a constant with x or z
    NUConstXZ * constExpr = dynamic_cast<NUConstXZ*>(expr);
    if (constExpr && constExpr->hasXZ()) {
        DynBitVector drive;
        DynBitVector value;
        constExpr->getValueDrive(&value, &drive);
        if (isCaseZ) { 
            // we don't need mask if casez condition doesn't contain 'z' value
            if (!constExpr->drivesZ()) return 0;
            for (UtString::size_type i = 0; i < condValue.size(); ++i) {
                // in case of casez 'x' isn't don't care (in all other cases x and z values considered don't cares)
                if (condValue[i] == 'x') {
                    drive[condValue.size() - 1 - i] = ~drive[condValue.size() - 1 - i];
                }
            }
        }
        drive.flip();
        NUConst* caseMask = NUConst::create(false, drive, constExpr->determineBitSize(), loc);
        return caseMask;
    } else {
      size_t maxCaseExprSize = expr->determineBitSize();
      return cCasexMask(expr, isCaseZ, maxCaseExprSize, loc);
    }
    
    return 0;
}

//! cCasexMask:  returns a NUConst* that can be used as a bit mask to identify the specific bits of \a expr that are not x or z.
//               This function is call when the case expression is not a constant. It returns a '1' in a bit position that is
//               NOT an 'x' or 'z'. Otherwise it returns '0'.
//               NOTE: This algorithm was adapted from PopulateExpr.cxx, casexMask, and casexMaskRecurse.
NUConst*
VerificNucleusBuilder::cCasexMask(NUExpr* expr, bool isCaseZ,  size_t maxCaseExprSize, const SourceLocator& loc)
{
  NUConst* casexMask = NULL;

  // Create a mask with '1' for bits we care about, and '0' for
  // those that are don't cares (x,z).
  DynBitVector mask(maxCaseExprSize, 0ULL);
  cCasexMaskRecurse(expr, isCaseZ, 0, maxCaseExprSize-1, &mask);
  
  // Take mask complement and test for presence of '1's.
  DynBitVector complement(mask);
  complement.flip();
  // If any 1's
  if (complement.any()) {
    casexMask = NUConst::create(false, mask, maxCaseExprSize, loc);
  }
  
  return casexMask;
}

//! cCasexMaskRecurse:  This is a helper function for casexMask. It populates positions min_bit:max_bit of a supplied DynBitVector,
//                      with bit values for the supplied NUExpr. '1' for bits that are NOT a 'z' or 'x', '0' for bits that are.
//                      It can process constants, and concatenations of expressions that contain mixes of constants and ids.
void
VerificNucleusBuilder::cCasexMaskRecurse(NUExpr* expr, bool isCaseZ, size_t min_bit, size_t max_bit, DynBitVector* mask)
{
  NUConstXZ * constExpr = dynamic_cast<NUConstXZ*>(expr);
  NUConcatOp* concat = dynamic_cast<NUConcatOp*>(expr);
  if (constExpr) {
    // This is a constant
    DynBitVector value;
    DynBitVector drive;
    constExpr->getValueDrive(&value, &drive);
    size_t index = 0;
    for (size_t cur_bit = min_bit; cur_bit <= max_bit; ++cur_bit, index++) {
      // Set the mask bit if it's a 1 or 0; also set it if isCaseZ and it's an x
      if (!drive[index] && value[index])
        mask->set(cur_bit);
      else if (!drive[index] && !value[index])
        mask->set(cur_bit);
      else if (isCaseZ && drive[index] && value[index])
        mask->set(cur_bit);
    }
  } else if (concat) {
    // This is a concat
    UInt64 repeat_count = concat->getRepeatCount();
    size_t cur_bit = max_bit;
    while (repeat_count > 0) {
      --repeat_count;
      // loop through all items of the concat
      for (NUExprLoop i = concat->loopExprs (); !i.atEnd (); ++i) {
        NUExpr* subExpr = *i;
        UInt32 bit_width = subExpr->getBitSize();
        cCasexMaskRecurse(subExpr, isCaseZ, cur_bit-bit_width+1, cur_bit, mask);
        cur_bit -= bit_width;
      }
    }
    
  // Otherwise, non-concat and not a constant; set all bits in range
  } else {
      for (size_t cur_bit = min_bit; cur_bit <= max_bit; ++cur_bit) {
	mask->set(cur_bit);
      }
  }
}

// Create a case condition expression with the proper width.
//   case expression should be set to have a width that is the the maxSize of the case labels
// This Max size is determined by static elaboration of the labels.
// This size is used to force the case condition to the right size.
//  Here the mask argument can be NULL.
//RJC RC#2013/08/09 (frunze) what does it mean to have a mask expression that is not null?
//ADDRESSED FD: I'm not an expert here, but I think it's related to casex/casez cases, and the condtion expression in that case have masks to indicate which bits are x or z and which are ordinary 0,1. I think mask can be all 0's or all 1's in the case we have no x,z data but have casex/casez case statemnt, but if we have ordinary case (not casex or casez) I think mask expression is null, in case of casex/casez this has something even if actual expression has no x's or z's inside.
NUCaseCondition*
VerificNucleusBuilder::cCaseCondition(NUExpr* nExpr, NUConst* mask, unsigned maxExprSize, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cCaseCondition" << std::endl;
#endif
    if (maxExprSize > nExpr->getBitSize()) {
        nExpr = nExpr->makeSizeExplicit(maxExprSize);
    }
    return (new NUCaseCondition(loc, nExpr, mask));
}

NUExpr* 
VerificNucleusBuilder::cSysFunctionCall(unsigned operation, const UtString& name, const NUExprVector& exprs, NUModule* module, const SourceLocator& loc)
{
    NUOp::OpT op = gNucleusUnOp(operation);
    NUExpr* funcCall = NULL;
    if ( VERI_SYS_CALL_SIGNED == operation) {
            NUExpr *e = exprs[0];
            e = new NUBinaryOp (NUOp::eBiVhExt, e,
                    NUConst::create (false, e->determineBitSize (),
                        32, loc),
                    loc);
            e->setSignedResult (true);
            funcCall = e;
    } else if ( VERI_SYS_CALL_UNSIGNED == operation) {
            NUExpr *e = exprs[0];
            //TODO may need this case $unsigned($signed(x)) expression - help it out...
            e->setSignedResult (false);
            funcCall = e;
    } else {
        NUExpr* expr1 = 0;
        if (exprs.size() > 0) {
            expr1 = exprs[0];
        }
        switch (op) {
        case NUOp::eUnItoR:
        case NUOp::eUnRtoI:
        case NUOp::eUnRealtoBits:
        case NUOp::eUnBitstoReal:
          if ( op == NUOp::eUnBitstoReal ){
            expr1->resize(expr1->determineBitSize ());
            // require that argument be 64 bits
            UInt32 bSize = expr1->getBitSize ();
            if ( bSize > 64 ){
              // it was too large to start with
              ConstantRange r(63, 0);
              expr1 = new NUVarselRvalue(expr1, r, loc);
            } else if ( bSize < 64 ) {
              expr1 = expr1->makeSizeExplicit(64);
            }
          }
          if ( ( ( op == NUOp::eUnRtoI ) || ( op ==  NUOp::eUnRealtoBits) ) && not expr1->isReal() ){
            mMessageContext->InvalidSysFuncArg(&loc, name.c_str(), "this function requires a real value argument.");
            delete expr1;
            expr1 = NULL;
            break;
          }

          expr1 = new NUUnaryOp(op, expr1, loc);
          if ( op == NUOp::eUnRtoI ){
            // the LRM says $rtoi is defined as returning a value the size of 'integer'
            // aldec does this, nc does not, 
            // in all cases the conversion is done in 64 bits,
            // then aldec and carbon truncates to 32 bits.
            ConstantRange r(31, 0);
            expr1 = new NUVarselRvalue(expr1, r, loc);
          }
          expr1->setSignedResult( (op == NUOp::eUnItoR) or (op == NUOp::eUnRtoI) or (op == NUOp::eUnBitstoReal));
          funcCall = expr1;
          break;
        case NUOp::eZSysTime:
        case NUOp::eZSysStime:
        case NUOp::eZSysRealTime:
            funcCall = new NUSysFunctionCall(op, module->getTimeUnit(), module->getTimePrecision(), exprs, loc);
            funcCall->setSignedResult(true);
            break;
        default:{
            // skip for now
            funcCall = 0; 
            break;
        }
        }
    }
    return funcCall; 
}

NUOutputSysTask* 
VerificNucleusBuilder::cOutputSysTask(const UtString& sysTaskName, NUExprVector& exprs, NUModule* module, bool hasFileSpec,
        bool appendNewLine, bool isVerilogTask, bool isWriteLineTask, bool hasInstanceName, STBranchNode* where, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cOutputSysTask" << std::endl;
#endif
    bool usesCFNet = false;
    return (new NUOutputSysTask(module->gensym(sysTaskName.c_str()), exprs, module, hasFileSpec, 
        appendNewLine, module->getTimeUnit(), module->getTimePrecision(), gNetRefFactory(), usesCFNet, isVerilogTask, isWriteLineTask, hasInstanceName, where, loc));
}

NUControlSysTask* 
VerificNucleusBuilder::cControlSysTask(const UtString& sysTaskName, unsigned funcType, NUExprVector& exprs, NUModule* module, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cControlSysTask" << std::endl;
#endif
    bool usesCFNet = false;
    CarbonControlType controlType = gCarbonControlType(funcType);
    return (new NUControlSysTask(module->gensym(sysTaskName.c_str()), controlType, exprs, module, gNetRefFactory(), usesCFNet, loc));
}

NUFCloseSysTask* 
VerificNucleusBuilder::cFCloseSysTask(const UtString& sysTaskName, NUExprVector& exprs, NUModule* module, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cFCloseSysTask" << std::endl;
#endif
    return (new NUFCloseSysTask(module->gensym(sysTaskName.c_str()), exprs, module, gNetRefFactory(), false, true /*isVerilogTask*/, false/*useInFileSystem*/, loc));
}

NUFOpenSysTask* 
VerificNucleusBuilder::cFOpenSysTask(StringAtom* name, NUNet* net, NUExprVector& exprs, NUModule* module, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cFOpenSysTask" << std::endl;
#endif
    return (new NUFOpenSysTask(name, net, NULL/*return_status*/, exprs, module, gNetRefFactory(), false, true /*isVerilogTask*/, false /*useInFileSystem*/, loc));
}

NUSysRandom* 
VerificNucleusBuilder::cRandomSysTask(StringAtom* name, const UtString& funcName, NULvalue* lvalue, NUExprVector& exprs, NUModule* module, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cRandomSysTask" << std::endl;
#endif
    NUSysRandom::Function randFunc = NUSysRandom::eInvalidRandom;
    bool isValid = NUSysRandom::lookup(funcName.c_str(), &randFunc);
    NUCLEUS_CONSISTENCY(isValid, loc, "unknown random function type"); 
    return (new NUSysRandom(randFunc, name, lvalue, NULL, exprs, module, gNetRefFactory(), false, loc));
}



NUFFlushSysTask* 
VerificNucleusBuilder::cFFlushSysTask(const UtString& sysTaskName, NUExprVector& exprs, NUModule* module, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cFFlushSysTask" << std::endl;
#endif
    return new NUFFlushSysTask(module->gensym(sysTaskName.c_str()), exprs, module, gNetRefFactory(), false, loc);
}

NUReadmemX* 
VerificNucleusBuilder::cReadmemXSysTask(NUExprVector& exprs, NUModule* module, NULvalue* memNetLvalue, const UtString& filename, bool hexFormat, SInt64 startAddr, SInt64 endAddr, bool endSpecified,  const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cReadmemXSysTask" << std::endl;
#endif
  return (new NUReadmemX(module->gensym("readmemx"), exprs, module, memNetLvalue, mAtomicCache->intern(filename), hexFormat, startAddr, endAddr, endSpecified, gNetRefFactory(), false, loc));
}

NUNamedDeclarationScope*
VerificNucleusBuilder::cNamedDeclarationScope(const UtString& name, NUScope* parentScope, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cNamedDeclarationScope" << std::endl;
#endif
    return (new NUNamedDeclarationScope(mAtomicCache->intern(name), parentScope, gIODB(), gNetRefFactory(), loc));
}

  
NUFor*
VerificNucleusBuilder::cFor(const NUStmtList& initial, NUExpr* condition, const NUStmtList& advance, const NUStmtList& body,
    const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API cFor" << std::endl;
#endif
    bool usesCFNet = false;
    return (new NUFor(initial, condition, advance, body, gNetRefFactory(), usesCFNet, loc));
}

NUBreak*
VerificNucleusBuilder::cBreak(NUBlock* block, const UtString& keyword, const UtString& name, const SourceLocator& loc)
{
    return (new NUBreak(block, mAtomicCache->intern(keyword), mAtomicCache->intern(name), gNetRefFactory(), loc));
}

NUChangeDetect*
VerificNucleusBuilder::cChangeDetect(NUExpr* expr)
{
    return (new NUChangeDetect(expr));
}

NUInitialBlock*
VerificNucleusBuilder::cInitialBlock(NUScope* scope, NUBlock* top_block, const SourceLocator& loc)
{
    StringAtom* name = scope->newBlockName(mAtomicCache, loc);
    return (new NUInitialBlock(name, top_block, mNetRefFactory, loc));
}

// Compute the net flags for hierarchical refs.
// Based on the original implementation in PopulateNet.cxx, VerilogPopulate::netHierRefResolve(void). 
// (CVS revision 1.7476)
NetFlags
VerificNucleusBuilder::gNetHierRefFlags(NUNet* net, const SourceLocator& loc)
{
  NetFlags flags(eAllocatedNet);
  if (net->isMem()) {
    flags = NetRedeclare(flags, eDMReg2DNet);
  }
  else if (net->isArrayNet()) {
    if (net->isReg()) {
      flags = NetRedeclare(flags, eDMReg2DNet);
    }
    else if (net->isWire()) {
      flags = NetRedeclare(flags, eDMWire2DNet);
    }
    else {
      // This message replicated original Interra flow message in PopulateNet.cxx (around line 1022)
      mMessageContext->VerificConsistency(&loc, "Nettype not supported by hierarchical reference consistency checking.");
    }
  }
  else if (net->isInteger()) {
    flags = NetRedeclare(flags, eDMIntegerNet|eSigned);
  }
  else if (net->isTime()) {
    flags = NetRedeclare(flags, eDMTimeNet);
  }
  // IsReal also covers realtime
  else if (net->isReal()) {
    flags = NetRedeclare(flags, eDMRealNet|eSigned);
  }
  else if (net->isReg() || net->isWire() || net->isWiredNet() || net->isDefault0() || net->isDefault1() || net->isSupply0() || net->isSupply1()) {
    // Do nothing
  }
  else {
      // This message replicated original Interra flow message in PopulateNet.cxx (around line 1053)
    mMessageContext->VerificConsistency(&loc, "Unknown net type");
  }
  
  return flags;
}

NUNet*
VerificNucleusBuilder::cNetHierRef(NUModule* masterModule, NUScope* parentScope, AtomArray* path, const UtString& netName, NUNet* localNet,  const SourceLocator& loc)
{
    NUNet* net = localNet;
    if (!localNet) net = Verific2NucleusUtilities::gModuleNet(masterModule, netName, false);
    NUCLEUS_CONSISTENCY(net, loc, "Unable to get net by name");
    NetFlags netFlags = gNetHierRefFlags(net, loc);
    VectorNetFlags vectorNetFlags = net->getVectorNetFlags();
    NUNet* netHierRef = NULL;
    if (net->isBitNet()) {
        netHierRef = new NUBitNetHierRef(*path, netFlags, parentScope, loc);
    } else if (net->isVectorNet()) {
        NUVectorNet* vectorNet = static_cast<NUVectorNet*>(net);
        ConstantRange* vectorRange = new ConstantRange(*(vectorNet->getDeclaredRange()));
        netHierRef = new NUVectorNetHierRef(*path, vectorRange, netFlags, vectorNetFlags, parentScope, loc);
    } else if (net->isMemoryNet()) {
        NUMemoryNet* memNet = static_cast<NUMemoryNet*>(net);
        ConstantRange* widthRange = new ConstantRange(*(memNet->getDeclaredWidthRange()));
        ConstantRange* depthRange = new ConstantRange(*(memNet->getDepthRange()));
        netHierRef = new NUMemoryNetHierRef(*path, widthRange, depthRange, netFlags, vectorNetFlags, parentScope, false, loc);
    } else {
        NUCLEUS_CONSISTENCY(false, loc, "Incorrect net type");
    }
    net->addHierRef(netHierRef);
    netHierRef->getHierRef()->addResolution(net);
    return netHierRef; 
}

NUTaskEnable*
VerificNucleusBuilder::cTaskEnableHierRef(NUScope* parentScope, NUModule* masterModule, NUTask* localTask, AtomArray* path, const UtString& taskName,
    const SourceLocator& loc)
{
    NUCLEUS_CONSISTENCY(masterModule, loc, "Null object as a master module passes");
    bool usesCFNet = false;
    NUTask* task = localTask;
    // if there is no local task search it by name in master module
    if (!localTask) {
        task = fTaskByName(masterModule, taskName);
    } 
    NUCLEUS_CONSISTENCY(task, loc, (UtString("Unable to find task ") + taskName + UtString(" in module ") + UtString(masterModule->getName()->str())).c_str());
    NUTaskEnable* taskEnable = new NUTaskEnableHierRef(*path, mAtomicCache->intern(taskName.c_str()), parentScope,
        gNetRefFactory(), usesCFNet, loc);
    taskEnable->getHierRef()->addResolution(task);
    task->addHierRef(taskEnable);
    return taskEnable;
}

STBranchNode* VerificNucleusBuilder::cSTBranchUnelab(const UtString& name, STBranchNode* parent)
{
    StringAtom* unelab = mAtomicCache->intern(name);
    STSymbolTableNode* node = mSymTabs->createNode(parent, unelab, IODBDesignDataSymTabs::eUnelab, IODBDesignDataSymTabs::eBranch);
    STBranchNode* newUnelabScope = node->castBranch();
    return newUnelabScope;
}

STBranchNode* VerificNucleusBuilder::cSTBranchElab(const UtString& name, STBranchNode* parent)
{
    StringAtom* elab = mAtomicCache->intern(name);
    STSymbolTableNode* node = mSymTabs->createNode(parent, elab, IODBDesignDataSymTabs::eElab, IODBDesignDataSymTabs::eBranch);
    STBranchNode* newElabScope = node->castBranch();
    return newElabScope;
}

void VerificNucleusBuilder::cSTBranch(const UtString& name, STBranchNode* parentUnelabScope, STBranchNode* parentElabScope,
    STBranchNode** newUnelabScope, STBranchNode** newElabScope)
{
    if (mIsRoot) {
        // root node can be created once, and is the only one which doesn't have parent scope
        mIsRoot = false;
        INFO_ASSERT(!parentUnelabScope, "Root module shouldn't have unelab parent branch node");
        INFO_ASSERT(!parentElabScope, "Root module shouldn't have elab parent branch node");
    } else {
        INFO_ASSERT(parentUnelabScope, "Parent node can't be null for child unelab branch");
        INFO_ASSERT(parentElabScope, "Parent node can't be null for child elab branch");
    }
    *newUnelabScope = cSTBranchUnelab(name, parentUnelabScope);
    *newElabScope = cSTBranchElab(name, parentElabScope);
    // The current unelaborated scope is the unelaborated scope corresponding
    // to this elaborated scope.
    IODBDesignNodeData* scopeInfo = IODBDesignDataBOM::castBOM(*newElabScope);
    scopeInfo->putUnelabScope(*newUnelabScope);
}


STSymbolTableNode* VerificNucleusBuilder::cSTNode(const UtString& name, STBranchNode* currentUnelabScope, STBranchNode* currentElabScope)
{
    // No current scope to add net to.
    INFO_ASSERT(currentUnelabScope, "No parent unelab scope provided");
    INFO_ASSERT(currentElabScope, "No parent elab scope provided");
    StringAtom* netName = mAtomicCache->intern(name);
    //RJC RC#2013/09/12 (frunze),
    // please explain why we create an unelab node but then never use it
    // if it really is not used then the call should be (void) mSymTabs->createNode(currentUnelabScope, netName, ...); to indicate that the
    // return value is not used
    // ADDRESSED FD: We need to populate both elab/unelab into symbol table, but will need to return just elab one to be used in population, so addressed issue per your suggestion
    (void) mSymTabs->createNode(currentUnelabScope, netName, 
                         IODBDesignDataSymTabs::eUnelab,
                         IODBDesignDataSymTabs::eLeaf);
    STSymbolTableNode* elabNode = mSymTabs->createNode(currentElabScope, netName,
                         IODBDesignDataSymTabs::eElab,
                         IODBDesignDataSymTabs::eLeaf);
    // user type info only for elab node, so returning pointer to elab node
    return elabNode;
}

void VerificNucleusBuilder::aUserTypeInfo(NUNet* net, STSymbolTableNode* symNode, unsigned netType)
{
  STAliasedLeafNode* leafNode = NULL;
  if (symNode != NULL) {
    leafNode = symNode->castLeaf();
  }
  IODBDesignNodeData* bomData = IODBDesignDataBOM::castBOM(leafNode);
  const UserType* ut = maybeCreateVerilogNet(net, netType);
  if (ut != NULL) {
    bomData->putUserType(ut);
  }
}

const UserType* VerificNucleusBuilder::maybeCreateVerilogNet(NUNet* net, unsigned netType)
{
  const UserType* ut = NULL;
  CarbonVerilogType  netTypeEnum;

  if (net->isBitNet())
  {
    netTypeEnum = getVerilogNetType(netType);
    ut = mUserTypeFactory->maybeAddScalarType(eLanguageTypeVerilog, 
					  netTypeEnum, 
					  eVhdlTypeUnknown);
  }
  else if (net->isVectorNet())
  {

    NUVectorNet *vectorNet =  net->castVectorNet();
    netTypeEnum = getVerilogNetType(netType);
    ut = mUserTypeFactory->maybeAddScalarType(eLanguageTypeVerilog, 
					  netTypeEnum, 
					  eVhdlTypeUnknown);

    // Keep integers, reals and times as a scalar
    if( (net->isInteger() == false) && (net->isReal() == false) && (net->isTime() == false))
    {
      ut = mUserTypeFactory->maybeAddArrayType(eLanguageTypeVerilog, 
					   netTypeEnum, eVhdlTypeUnknown, 
                                           const_cast <ConstantRange*> (vectorNet->getDeclaredRange()), 
                                           ut, true /* isPackedDim*/);
    }
  }
  else if (net->isMemoryNet()) // eArray
  {
    NUMemoryNet *memNet =  net->getMemoryNet();
    netTypeEnum = getVerilogNetType(netType);

    if((netTypeEnum == eVerilogTypeInteger) || (netTypeEnum == eVerilogTypeTime))
    {
      // if -2000 is not selected and the net is 
      // array of int or time
      ut = mUserTypeFactory->maybeAddScalarType(eLanguageTypeVerilog, 
					    netTypeEnum, 
					    eVhdlTypeUnknown);
      UInt32 numDim = memNet->getNumDims();
      for(UInt32 i = 1; i < numDim; i++)
      {
        ut = mUserTypeFactory->maybeAddArrayType(eLanguageTypeVerilog, 
					     netTypeEnum, 
					     eVhdlTypeUnknown, 
                                             const_cast <ConstantRange*> (memNet->getRange(i)), 
                                             ut, false /*not packed*/);
      }
    }
    else
    {                           // might be a reg, or wire type
      netTypeEnum = getVerilogNetType(netType);
      ut = mUserTypeFactory->maybeAddScalarType(eLanguageTypeVerilog, 
					    netTypeEnum, 
					    eVhdlTypeUnknown);

      UInt32 numDim = memNet->getNumDims();
      for(UInt32 i = 0; i < numDim; i++)
      {
        bool isPackedDimension = (i < (memNet->getFirstUnpackedDimensionIndex()));
        ut = mUserTypeFactory->maybeAddArrayType(eLanguageTypeVerilog, 
					     netTypeEnum, 
					     eVhdlTypeUnknown, 
                                             const_cast <ConstantRange*> (memNet->getRange(i)), 
					     ut, isPackedDimension);
      }
    }
  } // else : we don't care about type info for the rest.
  return ut;
}

CarbonVerilogType  VerificNucleusBuilder::getVerilogNetType(unsigned netType)
{
  CarbonVerilogType  netTypeEnum;
  switch (netType)
  {
    case VERI_INTEGER:
    case VERI_INT:
      netTypeEnum = eVerilogTypeInteger;
      break;
    // case VERI_SHORTREAL:
    case VERI_REAL:
      netTypeEnum = eVerilogTypeReal;
      break;
    case VERI_TIME:
      netTypeEnum = eVerilogTypeTime;
      break;
    case VERI_REALTIME:
      netTypeEnum = eVerilogTypeRealTime;
      break;
    case VERI_NONE:
      netTypeEnum = eVerilogTypeUnsetNet;      // user type is not known
      break;
    case VERI_WIRE:
      netTypeEnum = eVerilogTypeWire;
      break;
    case VERI_TRI:
      netTypeEnum = eVerilogTypeTri;
      break;
    case VERI_TRI1:
      netTypeEnum = eVerilogTypeTri1;
      break;
    case VERI_SUPPLY0:
      netTypeEnum = eVerilogTypeSupply0;
      break;
    case VERI_WAND:
      netTypeEnum = eVerilogTypeWAnd;
      break;
    case VERI_TRIAND:
      netTypeEnum = eVerilogTypeTriAnd;
      break;
    case VERI_TRI0:
      netTypeEnum = eVerilogTypeTri0;
      break;
    case VERI_SUPPLY1:
      netTypeEnum = eVerilogTypeSupply1;
      break;
    case VERI_WOR:
      netTypeEnum = eVerilogTypeWOr;
      break;
    case VERI_TRIOR:
      netTypeEnum = eVerilogTypeTriOr;
      break;
    case VERI_TRIREG:
      netTypeEnum = eVerilogTypeTriReg;
      break;
    case VERI_BIT:
    case VERI_LOGIC:
    case VERI_REG:
    case VERI_BYTE:
    case VERI_SHORTINT:
    case VERI_LONGINT:
      netTypeEnum = eVerilogTypeReg;
      break;
    default:
      netTypeEnum = eVerilogTypeUnsetNet;      // user type is not known
      break;
  }
  return netTypeEnum;
}




/*************************************************************************************************************/
/****************************************** Nucleus add to parent ********************************************/
/*************************************************************************************************************/

void
VerificNucleusBuilder::aStmtToScope(NUStmt* stmt, NUScope* scope, const SourceLocator& loc)
{
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(scope, loc, "Null scope object");
    NUScope::ScopeT scope_type = scope->getScopeType();
    if (NUScope::eModule != scope_type) { 
        // Adding stmt to parent block
        if (NUScope::eBlock == scope_type) {
            NUBlock* block = dynamic_cast<NUBlock*>(scope);
            NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(block, loc, "Incorrect block type");
            aStmtToBlock(stmt, block);
        } else {
            NUCLEUS_CONSISTENCY_NO_RETURN_VALUE((NUScope::eTask == scope_type), loc, "Incorrect task/function scope type");
            NUTask* task = dynamic_cast<NUTask*>(scope);
            NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(task, loc, "Incorrect task/function scope");
            aStmtToTF(stmt, task);
        }
    } else {
        // we must be in a context where statements cannot be created (continues assign)
        // in this case we create a dummy always block to hold the assignment
        // Creating block
        NUModule* module = dynamic_cast<NUModule*>(scope); // consider scope->getModule()
        NUBlock* block = cBlock(module, loc);
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(block, loc, "Unable to create block");
        // Adding task enable to block
        aStmtToBlock(stmt, block);
        (void) createAlwaysBlockInScope(module, block, false, NUAlwaysBlock::eAlways,  loc);
    }
}



void
VerificNucleusBuilder::aNameToPath(const UtString& name, AtomArray* path)
{
    INFO_ASSERT(path, "Null object for path");
    path->push_back(mAtomicCache->intern(name.c_str()));
}

void 
VerificNucleusBuilder::aNetToScope(NUNet* net, NUScope* scope)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aNetToScope" << std::endl;
#endif
    scope->addLocal(net);
}

void 
VerificNucleusBuilder::aNetHierRefToModule(NUNet* netHierRef, NUModule* module)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aNetHierRefToScope" << std::endl;
#endif
    // We should populate all hierarchical references in module even if parent scope is task/function
    INFO_ASSERT(module, "Incorrect module type");
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(netHierRef, module->getLoc(), "Null net hier ref");
    module->addNetHierRef(netHierRef);
}


void 
VerificNucleusBuilder::aPortToScope(NUNet* port, NUScope* scope)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aPortToScope" << std::endl;
#endif
    INFO_ASSERT(scope, "Null scope object");// TODO pass location to have CONSISTENCY check instead
    NUScope::ScopeT scope_type =  scope->getScopeType();
    if (NUScope::eModule == scope_type) {
        NUModule* module = scope->getModule();
        INFO_ASSERT(module, "Incorrect module type");
        module->addPort(port);
    } else if (NUScope::eTask == scope_type) {
        NUTF* task = scope->getTFScope();
        INFO_ASSERT(task, "Incorrect task/function type");
        task->addArg(port, getParamMode(port));
    }
}

void 
VerificNucleusBuilder::aContinuesAssignToModule(NUContAssign* contAssign, NUModule* module)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aContinuesAssignToModule" << std::endl;
#endif
    module->addContAssign(contAssign);
}

void 
VerificNucleusBuilder::aStmtToStmtList(NUStmt* stmt, NUStmtList* stmts)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aStmtToStmtList" << std::endl;
#endif
    stmts->push_back(stmt);
}

void 
VerificNucleusBuilder::aStmtToStmtListFront(NUStmt* stmt, NUStmtList* stmts)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aStmtToStmtListFront" << std::endl;
#endif
    stmts->push_front(stmt);
}


void 
VerificNucleusBuilder::aLvalueToLvalueVector(NULvalue* lvalue, NULvalueVector& lvalues)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aLvalueToLvalueVector" << std::endl;
#endif
    lvalues.push_back(lvalue);
}


void 
VerificNucleusBuilder::aExprToExprVector(NUExpr* expr, NUExprVector& exprs)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aExprToExprVector" << std::endl;
#endif
    exprs.push_back(expr);
}


void 
VerificNucleusBuilder::aStmtListToBlock(NUStmtList* stmts, NUBlock* block)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aStmtToStmtList" << std::endl;
#endif
    block->addStmts(stmts);
}

void 
VerificNucleusBuilder::addAlwaysBlockToModule(NUAlwaysBlock* alwaysBlock, NUModule* module)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API addAlwaysBlockToModule" << std::endl;
#endif
    module->addAlwaysBlock(alwaysBlock);
}

void 
VerificNucleusBuilder::aInitialBlockToModule(NUInitialBlock* initialBlock, NUModule* module)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aInitialBlockToModule" << std::endl;
#endif
    module->addInitialBlock(initialBlock);
}

void 
VerificNucleusBuilder::aEventExprToAlwaysBlock(NUExpr* eventExpr, NUAlwaysBlock* alwaysBlock)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aEventExprToAlwaysBlock" << std::endl;
#endif
    NUEdgeExpr* edgeExpr = dynamic_cast<NUEdgeExpr*>(eventExpr);
    if (edgeExpr) {
        alwaysBlock->addEdgeExpr(edgeExpr);
    } else {
        alwaysBlock->addLevelExpr(eventExpr);
    }
}


void
VerificNucleusBuilder::aTopLevelModule(NUModule* module)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aTopLevelModule" << std::endl;
#endif
    mDesign->addTopLevelModule(module);
}


void
VerificNucleusBuilder::aModule(NUModule* module)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aModule" << std::endl;
#endif
    mDesign->addModule(module);
}
    
    
void 
VerificNucleusBuilder::aTFToModule(NUTask* tf, NUModule* module)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aTFToModule" << std::endl;
#endif
    module->addTask(tf);
}
    
void 
VerificNucleusBuilder::aStmtToTF(NUStmt* stmt, NUTask* tf)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aBlockToTF" << std::endl;
#endif
    tf->addStmt(stmt);
}

void 
VerificNucleusBuilder::aTFArgConnectionToTaskEnable(NUTFArgConnection* argConnection, NUTaskEnable* taskEnable)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aTFArgConnectionToTaskEnable" << std::endl;
#endif
    taskEnable->addArgConnection(argConnection);
}

void 
VerificNucleusBuilder::aTFArgConnectionToVector(NUTFArgConnection* argConnection, NUTFArgConnectionVector* argConnections)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aTFArgConnectionToVector" << std::endl;
#endif
    argConnections->push_back(argConnection);
}

void 
VerificNucleusBuilder::aTFArgConnectionsToTaskEnable(NUTFArgConnectionVector* argConnections, NUTaskEnable* taskEnable)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aTFArgConnectionsToTaskEnable" << std::endl;
#endif
    taskEnable->setArgConnections(*argConnections);
}

void 
VerificNucleusBuilder::aStmtToBlock(NUStmt* stmt, NUBlock* block)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aStmtToBlock" << std::endl;
#endif
    block->addStmt(stmt);
}

void 
VerificNucleusBuilder::aStmtToLoop(NUStmt* stmt, NUFor* loop)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aStmtToBlock" << std::endl;
#endif
    loop->addBodyStmt(stmt);
}


//  The method finds any unconnected port and creates empty connection for it, in order to find them in both named connections and by order
//  connections we need to check each port by declaration order (declaration order matters in by order case) as well all connections need to
//  be in port declaration order.
void
VerificNucleusBuilder::aUnconnectedPortsToModuleInstanceAndSort(NUModuleInstance* moduleInstance, NUModule* masterModule, const SourceLocator& loc)
{
    NUPortConnectionMap * formal2connection = moduleInstance->getPortMap();
    NUPortConnectionVector* portConnSorted = new NUPortConnectionVector();
    // loop module ports by declaration order 
    for (NUNetVectorLoop l = masterModule->loopPorts(); !l.atEnd(); ++l)
    {
        NUNet* net = *l;
        if (formal2connection->find(net) != formal2connection->end()) {
            portConnSorted->push_back((*formal2connection)[net]);
        } else {
            // if not exist create and add empty connection
            NUPortConnection* pc = cPortConnection(NULL, net, loc);
            aPortConnectionToModuleInstance(pc, moduleInstance);
            portConnSorted->push_back(pc);
        }
    }
    delete formal2connection;
    moduleInstance->setPortConnections(*portConnSorted);
    delete portConnSorted;
}


void 
VerificNucleusBuilder::aModuleInstanceToScope(NUModuleInstance* moduleInstance, NUScope* scope, const SourceLocator& loc)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aModuleInstanceToScope" << std::endl;
#endif
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(scope, loc, "NULL scope object");
    NUScope::ScopeT scope_type = scope->getScopeType();
    // addModuleInstance is not defined under NUScope, so can't use NUScope*
    if (NUScope::eModule == scope_type) {
        NUModule* module = dynamic_cast<NUModule*>(scope);
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, loc, "Incorrect module type");
        module->addModuleInstance(moduleInstance);
    } else if (NUScope::eNamedDeclarationScope == scope_type) {
        NUNamedDeclarationScope* namedDeclarationScope = dynamic_cast<NUNamedDeclarationScope*>(scope);
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(namedDeclarationScope, loc, "Incorrect named declaration scope type");
        namedDeclarationScope->addModuleInstance(moduleInstance);
    } else {
        NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(false, loc, "Incorrect scope type");
    }
}
    
void 
VerificNucleusBuilder::aPortConnectionToModuleInstance(NUPortConnection* portConnection, NUModuleInstance* moduleInstance)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aPortConnectionToModuleInstance" << std::endl;
#endif
    moduleInstance->addPortConnection(portConnection);
    portConnection->setModuleInstance(moduleInstance);
}


void
VerificNucleusBuilder::aConditionToCaseItem(NUCaseCondition* caseCondition, NUCaseItem* caseItem)
{
    caseItem->addCondition(caseCondition);
}

void
VerificNucleusBuilder::aStmtToCaseItem(NUStmt* stmt, NUCaseItem* caseItem)
{
    caseItem->addStmt(stmt);
}

void
VerificNucleusBuilder::aStmtListToCaseItem(NUStmtList* stmts, NUCaseItem* caseItem)
{
    caseItem->addStmts(stmts);
}

void
VerificNucleusBuilder::aCaseItemToCase(NUCaseItem* caseItem, NUCase* caseStmt)
{
    caseStmt->addItem(caseItem);
    if (caseItem->isDefault()) {
      caseStmt->putHasDefault(true);
    }
}

void
VerificNucleusBuilder::aNetToNetVector(NUNet* net, NUNetVector& nets)
{
#ifdef DEBUG_VERIFIC_NUCLEUS_BUILDER
    std::cerr << "NUCLEUS API aNetToNetVector" << std::endl;
#endif
    nets.push_back(net);
}

UtVector<Verific::VhdlIdDef*>& VerificNucleusBuilder::gHierRefIdDefs()
{
    return mHierRefIdDef;
}

void VerificNucleusBuilder::sHierRefIdDef(Verific::VhdlIdDef* idDef)
{
    mHierRefIdDef.push_back(idDef);
}
bool VerificNucleusBuilder::isHierRefIdDef(Verific::VhdlIdDef* idDef)
{
  bool found = false;
  UtVector<Verific::VhdlIdDef*>::iterator iter = mHierRefIdDef.begin();
  for (iter = mHierRefIdDef.begin(); iter != mHierRefIdDef.end(); ++iter) {
    Verific::VhdlIdDef* id = *iter;
    if (id == idDef) {
      found = true;
      break;
    }
  }
  return found;
}

NUNet* VerificNucleusBuilder::getEdgeNet(NUExpr* expr)
{
    if (mEdgeNetMap.find(expr) != mEdgeNetMap.end()) {
        return mEdgeNetMap[expr];
    }
    return 0;
}

void VerificNucleusBuilder::putEdgeNet(NUExpr* expr, NUNet* net)
{
    mEdgeNetMap[expr] = net;
}

} // namespace verific2nucleus
