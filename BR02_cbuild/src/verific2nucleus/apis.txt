class VerificDesignWalkerVerilog {
};

VerificDesignWalkerVerilog.cxx

//api calls needed -- toplevel for module.
vlogModule(Verific::VeriModule*, blk_cfg, isCmodel);
-- get time units
-- process ports x
-- process declarations
-- generates handled are by Verific
-- get variables.
-- get signals
-- get continuous assigns x
-- process always blocks


VerificDesignWalkerCB::MakePort(Pre...)
VerificDesignWalkerCB::MakePort(Post...)

class VerificDesignWalkerCB -- methods invoked by design walker

Callbacks invoked from walker with pre/post methods for everything.


//Nucleus construct through call back

class NucleusVerificVerilogCB {
};


NucleusVerificVerilogCB.cxx   
---------------------------
overloaded InterraCB call backs for verific Verilog. For each InterraCB hava a verific one.


NucleusVerificVerilogCB::port(Phase phase, VeriNode node, VeriLanguageType langType)
-- straight to nucleus from here
