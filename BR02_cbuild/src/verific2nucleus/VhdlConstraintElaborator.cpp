// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// project headers
#include "verific2nucleus/CarbonStaticElaborator.h"
#include "verific2nucleus/CarbonConstraintManager.h"
#include "verific2nucleus/VhdlCSElabVisitor.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"
#include "verific2nucleus/VhdlConstraintElaborator.h"

// verific headers
#include "VeriVisitor.h"    // Visitor base class definition
#include "VhdlVisitor.h"    // Visitor base class definition
#include "VhdlScope.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "VhdlTreeNode.h"
#include "VeriTreeNode.h"
#include "VhdlExpression.h"
#include "VhdlName.h"
#include "VhdlCopy.h"
#include "VhdlDeclaration.h"
#include "VhdlValue_Elab.h"
#include "Array.h"
#include "VhdlSpecification.h"
#include "VhdlDataFlow_Elab.h"
#include "Set.h"

// other project headers
#include "util/UtPair.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"

// standard headers
#include <sstream>

namespace verific2nucleus {

using namespace Verific;

    //=====================================================================================
    // Constructor

    VhdlConstraintElaborator::VhdlConstraintElaborator(CarbonConstraintManager *cm, CarbonStaticElaborator* ce) : VhdlCSElabVisitor(cm, ce)
    {
    }

    //=====================================================================================
    // Vhdl Loop Unrolling
    //
    // Loops with subprogram calls within should be 'unrolled', so we can use the
    // value of the loop index to compute constraints on arguments to subprograms.
    // (See comments for 'loopShouldBeUnrolled' in Verific2NucleusUtilities.cpp.)
    //
    // Note that we do not create a sub VhdlDataFlow object if the loop is unrolled,
    // because no conditional control flow is created by an unrolled loop. 
    void VhdlConstraintElaborator::VHDL_VISIT(VhdlLoopStatement, node)
    {
      // Make sure we visit the iteration scheme. There could be function
      // calls there too.
      if (node.GetIterScheme()) node.GetIterScheme()->Accept(*this);

      // Figure out whether we should/can unroll the loop. See comments
      // in Verific2NucleusUtilities.cpp
      bool forceUnroll = getCarbonContext()->getArgs()->getBoolValue(CarbonContext::scVhdlLoopUnroll);
      if (Verific2NucleusUtilities::loopShouldBeUnrolled(&node, mDataFlow, forceUnroll)) {
        // We're going to visit statements for each iteration of the loop.
  
        // Grab the range of the loop
        // Existence of iteration scheme and constant range guaranteed by 
        // loopShouldBeUnrolled()
        VhdlForScheme* fScheme = dynamic_cast<VhdlForScheme*>(node.GetIterScheme());
        if ( !fScheme ){
          UtString msg("Unsupported construct -- Loop iteration scheme is not a FOR loop.");
          reportFailure(&node, msg);
          return;
        }
        VhdlDiscreteRange* range = fScheme->GetRange();
        if ( !range ){
          UtString msg("Unsupported construct -- Loop iteration has a NULL range.");
          reportFailure(&node, msg);
          return;
        }
        SInt32 left = 0, right = 0;
        Verific2NucleusUtilities::isConstantRange(range, mDataFlow, &left, &right);

        // Figure out how many iterations through the loop, and whether we're 
        // incrementing up or down.
        SInt32 iterations = std::abs(left-right) + 1;
        SInt32 increment = (right > left) ? 1 : -1;

        // Now visit statements in the loop, with the iteration id
        // set to the value of the loop index.
        SInt32 loopIndex = left;
        for (SInt32 i = 0; i < iterations; i++, loopIndex += increment) {
          // Construct a VhdlValue equal to the VHDL loop index.
          // This will be owned by the Verific VhdlIdDef, and freed by it.
          VhdlValue *value = new VhdlInt(loopIndex);
          fScheme->GetIterId()->SetValue(value);
          unsigned jj = 0;
          VhdlTreeNode* stmt = 0;
          FOREACH_ARRAY_ITEM(node.GetStatements(), jj, stmt) {
            if (!stmt) continue;
            stmt->Accept(*this);
          }
        }
      } else {
        // We're not unrolling the loop, so create a sub data flow, and
        // just visit the statements once
        VhdlDataFlow *curDF = mDataFlow;
        Array data_flows(1);
        mDataFlow = Verific2NucleusUtilities::setupLoopDataFlow(&data_flows, mDataFlow, &node);
        unsigned i = 0;
        VhdlTreeNode* stmt = 0;
        FOREACH_ARRAY_ITEM(node.GetStatements(), i, stmt) {
          if (!stmt) continue;
          stmt->Accept(*this);
        }
        // Clean up the dataflows
        Verific2NucleusUtilities::cleanUpDataFlows(&node, &data_flows, curDF);
        mDataFlow = curDF;
      }
    }
    
    //=====================================================================================
    // This visitor is here to guard against walking a subprogram body from some point
    // OTHER than a subprogram (or overloaded operator) invocation. Specifically, we
    // want to avoid walking a subprogram body from the point of declaration.

    void VhdlConstraintElaborator::VHDL_VISIT(VhdlSubprogramBody, node)
    {
      if (mEnableSubprogramBodyWalk) {
        mEnableSubprogramBodyWalk = false;
        VhdlVisitor::VisitVhdlSubprogramBody(node);
      }
    }
    
    //=====================================================================================
    // This visitor processes overloaded operators.
    //
    // It constructs a subprogram call signature, then invokes 'walkSubprogramBody'
    // to process 'return' statements within the overloaded operator function body.
    // The return statement visitor calculates constraints on the return expression,
    // and adds an entry to the signature->constraint map.
    // NOTE that overloaded operators can only be unary, or binary.

    void VhdlConstraintElaborator::VHDL_VISIT(VhdlOperator, node)
    {
      // First, process left and right (if any) expressions. They may contain
      // function calls.
      if (node.GetLeftExpression()) {
        node.GetLeftExpression()->Accept(*this);
      }
      if (node.GetRightExpression()) {
        node.GetRightExpression()->Accept(*this);
      }

      if (0) // Debug
      {
        const char* p = node.GetOperator()->IsPredefinedOperator() ? "IS" : "IS NOT";
        UtIO::cout() << "DEBUG: Operator '" << Verific2NucleusUtilities::verific2String(node) << "' " << p << " a predefined operator" << UtIO::endl;
      }
      
      // Next, check to see if this is a user supplied overloaded operator
      VhdlIdDef* opId = node.GetOperator();
      if (Verific2NucleusUtilities::isNonIeeeOverloadedOperator(opId)) {

        // Calculate signature, and save it in a visitor member variable so it
        // is readily available during return statement processing.
        UtString sig = m_cm->getOverloadedOperatorSignature(&node, mDataFlow);
        UtString parentSig = m_CallerSignature;
        m_CallerSignature = sig;
        
        if (0) // Debug
          UtIO::cout() << "DEBUG: Overloaded Operator Subprogram Signature: " << sig << UtIO::endl;

        VhdlSubprogramBody* subprog_body = node.GetOperator()->Body();
        if (subprog_body) {
          // Create an arguments array 
          Array *args = new Array();
          if (node.GetLeftExpression()) {
            args->Insert(node.GetLeftExpression());
          }
          if (node.GetRightExpression()) {
            args->Insert(node.GetRightExpression());
          }

          // Now traverse the overloaded operator declaration
          mEnableSubprogramBodyWalk = true;
          walkSubprogramBody(subprog_body, args);

          delete args;
        }

        // Restore parent signature
        m_CallerSignature = parentSig;
      }
      
    }
    
    //=====================================================================================
    // This visitor processes function and procedure calls. 
    // It constructs a subprogram call signature, then invokes 'walkSubprogramBody' to
    // process 'return' statements within the subprogram. The return statement visitor
    // calculates constraints on the return expression, and adds an entry to the
    // signature->constraint map.
    //
    // Note: Constraints are calculated directly for type conversions and builtin
    // functions. We don't need to analyze the subprogram body, or process 'return'
    // statements to figure out what their return constraints are. So we don't
    // bother to put them in the signature->constraint map.

    void VhdlConstraintElaborator::VHDL_VISIT(VhdlIndexedName, node) 
    {
      // First, process all associations. They may contain function calls themselves.
      unsigned i = 0;
      VhdlTreeNode* assoc = 0;
      FOREACH_ARRAY_ITEM(node.GetAssocList(), i, assoc) {
        if (assoc) assoc->Accept(*this);
      }

      // We need to visit the prefix for cases like: funcall(arg1)(3 downto 0)
      if (node.GetPrefix()) node.GetPrefix()->Accept(*this);

      // If it's a function or procedure call (but not one of the builtin functions)
      if (isTraversableSubprog(&node)) {

        // We are not yet handling pragma functions. Tell the user.
        // See comments with 'isUnsupportedPragmaFunction' for more detail
        if (isUnsupportedPragmaFunction(&node)) {
          UtString msg;
          msg << "Unsupported pragma function '" << node.GetId()->Name() << "'. "
              << "Pragma functions without subprogram bodies and return statements "
              << "are not yet supported.";
          reportFailure(&node, msg);
          return;
        }
        
        // Calculate signature, and save it in a visitor member variable so it
        // is readily available during return statement processing.
        UtString sig = m_cm->getSubprogramSignature(&node, mDataFlow);
        UtString parentSig = m_CallerSignature;
        m_CallerSignature = sig;
        
        if (0) // Debug
          UtIO::cout() << "DEBUG: Call Instance Subprogram Signature: " << sig << UtIO::endl;
          
        // Now walk the subprogram body
        VhdlSubprogramBody* subprog_body = node.GetId()->Body();
        if (subprog_body) {
          Array *args = new Array;
          Verific2NucleusUtilities::getSubprogramArgs(args, &node, true /* use defaults */);
          mEnableSubprogramBodyWalk = true;
          walkSubprogramBody(subprog_body, args);
          delete args;
        }
        
        // Restore parent signature
        m_CallerSignature = parentSig;
      } 
    }

    //=====================================================================================
    // Construct constraint for return expression, and add to the
    // signature->constraint map. 
    // 
    // The VHDL Return statement plays an integral role in the support for recursive functions.
    // The details of that support are documented in the following paragraphs.
    //
    // Note, this comment focusses primarily on Vhdl functions,
    // though many of the same concepts will apply to procedures.
    // 
    // VerificVhdlDesignWalker populates multiple copies of
    // a given function, one for each unique call/return argument
    // signature. This is because nucleus tasks must have 
    // fixed size arguments and return values; whereas VHDL
    // subprograms can have unconstrained arguments and, in
    // the case of functions, unconstrained return values.
    // 
    // As a consequence, the Vhdl design walker cannot populate
    // a function when it visits a function declaration; it must
    // instead populate a function at the point of invocation,
    // because only then does it know what the constraints on the
    // function arguments are. Furthermore, a function cannot
    // be fully populated until it's return statement is processed,
    // because, in general, only then do we know what the 
    // constraint on (i.e. size of) the return value is.
    // 
    // To make matters more complicated, if a function returns
    // the result of another function call, then we have to analyze
    // that second function to figure out what the return constraint
    // of the first function is. For example, suppose we have
    // two functions, myfunc1 and myfunc2, both with unconstrained
    // return values:
    // 
    //    function myfunc2 (in1 : std_logic_vector) return std_logic_vector
    //      ... defn of myfunc2 follows ...
    // 
    //    function myfunc1 (in1 : std_logic_vector) return std_logic_vector
    //       ...
    //       return myfunc2(in1(w downto 0));
    // 
    // The return constraint of myfunc1 cannot be determined until
    // the return constraint of myfunc2 has been calculated.
    // So there is no choice but to fully analyze the function 
    // call tree rooted at the call to 'myfunc2(in1(w downto 0))'.
    // Only when 
    // we've processed the myfunc2 call tree down to it's terminal
    // endpoints, and propagated the return constraints all the
    // way back up to the 'myfunc2(in1(w downto 0))' call, 
    // can we know what the size of
    // the myfunc2 return value is.
    // 
    // Unfortunately, the Verific EvaluationConstraintInternal method
    // cannot determine the constraints of expressions that contain
    // function calls. 
    // 
    // So that means we have to do it ourselves. To perform
    // the call tree analysis, it's necessary to:
    // 
    // 1) Perform call tree analysis on the return expression,
    // BEFORE attempting to compute the constraint on the function
    // return expression.
    // This will cause the call tree analysis to be done
    // before we need it.
    // 
    // 2) During call tree analysis, record the function return 
    // constraints as they are calculated,
    // i.e. when we compute the constraint on a return expression.
    // This will be done 'bottom up', starting at the leaf of the
    // call tree, then to the parent, grandparent, and so on.
    // The constraints are stored in a subprogram call signature
    // to constraint map. Each subprogram call signature is
    // constructed so that it maps onto one and only one return constraint.
    // (See comments in CarbonConstraintManager.)
    //
    // 3) Guarantee that recursive function calls terminate. We cannot
    // support a recursive function unless the depth of recursion
    // can be computed at carbon static elaboration
    // time. This means that we cannot support recursive functions
    // whose call depth is a function of design input values.
    // 
    // The process of pushing into function calls, and propagating
    // constraints from the actuals down into the formals has to
    // propagate the constraints far enough so we can evaluate
    // conditional expressions in the function body. This is
    // important to avoid endless recursion in the Vhdl design
    // walker. If statements like this appear in a recursive function:
    // 
    // function recurse (arg1 : in std_logic_vector) return std_logic_vector
    //    if (arg1'length > 2) then
    //       return recurse(arg1(arg1'length-2 downto 0))
    //    else
    //       return "00"
    // end
    // 
    // Then when we 'push into' a call to recurse with the length
    // of arg1 less than or equal to 2, we should avoid populating
    // the 'then' branch of the conditional, and by doing so, will
    // avoid 'pushing into' the call to recurse.
    // 
    // This pruning is currently done in the VhdlIfStatement visitor.
    // 

    void VhdlConstraintElaborator::VHDL_VISIT(VhdlReturnStatement, node) {

      // First, we have to do function call tree analysis on the return expression.
      // Note that procedure return statements don't have expressions. In this case,
      // there is nothing to do.
      if (node.GetExpression()) {
        node.GetExpression()->Accept(*this);

        // Now we should be able to retrieve the constraint on the return expression
      
        // This is the possibly unconstrained return constraint
        VhdlConstraint* returnConstraint = node.GetOwner()->Constraint();
        if ( ! returnConstraint ) {
          UtString msg("Unsupported construct -- function 'return' statement has a NULL constraint");
          reportFailure(&node, msg);
          return;
        }
        // This is the constrained return constraint
        VhdlConstraint* constraint = NULL;
        // Every unique function call should have an entry in the signature map.
        if (returnConstraint->IsUnconstrained()) {
          constraint = m_cm->createConstraintFromExpression(node.GetExpression(), returnConstraint, mDataFlow /* df */);
        } else {
          constraint = returnConstraint->Copy();
        }
        
        if (0) // Debug
        {
          const char* image = NULL;
          if (constraint)
            image = constraint->Image();
          const char* msg = image ? image : "NULL";
          UtIO::cout() << "DEBUG: Subprogram Return constraint image for '" << m_CallerSignature << "': " << msg << UtIO::endl;
          delete image;
        }

        // Unable to determine the width of the return value for function ???. (Would be nice to say where
        // it was called).
        if (!constraint || constraint->IsUnconstrained()) {
          UtString msg;
          msg << "Unable to determine the width or type of the return value for function '"
              << Verific2NucleusUtilities::verific2String(*node.GetOwner()) << "'";
          reportFailure(&node, msg);
        }
        
        if (0) // Debug
        {
          if (!constraint) {
            UtIO::cout() << "DEBUG: ERROR NULL Return constraint for " << m_CallerSignature << UtIO::endl;
          } else if (constraint->IsUnconstrained()) {
            UtIO::cout() << "DEBUG: ERROR Unconstrained return constraint for " << m_CallerSignature << UtIO::endl;
          }
        }
          
        // Add signature/constraint to the map, return false if error
        if (!m_cm->addSignatureConstraint(m_CallerSignature, constraint)) {
          // This represents a scenario in which
          // the same uniquified function returns two different type/bit widths. 
          UtString msg;
          msg << "Function '" << Verific2NucleusUtilities::verific2String(*node.GetOwner()) 
              << "' returns different size results for the same call signature";
          reportFailure(&node, msg);
          if (0) // Debug
            UtIO::cout() << "DEBUG: ERROR Return constraint conflict for " << m_CallerSignature << UtIO::endl;
        }
        
        delete constraint;
      }
    }

  // Report an error, set flag to terminate subsequent Carbon Static Elaboration phases
  void VhdlConstraintElaborator::reportFailure(VhdlTreeNode* node, const UtString& msg) {
    getCarbonStaticElaborator()->reportFailure(node, "VhdlConstraintElaborator", msg);
    mErrorDuringWalk = true;
  }

  // Return true if the supplied VhdlIndexedName is a call to an unsupported pragma function.
  // A pragma function is a VHDL function that has an attribute
  // whose value defines it's functionality. There are a fixed set
  // of predefined functions that the attribute may refer to.
  // Pragma functions often do not have bodies, so we cannot rely
  // on the normal mechanisms to determine the function return 
  // constraint. To handle these correctly, we will need a table
  // of pragma functions which can contain the knowledge about
  // how to determine the return constraint given the constraints
  // on the input(s). Something similar to how we handle
  // built-in conversion functions.
  //
  // But for now, we are not supporting any pragma function that
  // has an unconstrained return type, and does not have a body, 
  // or does not have a return statement in it's body. 
  // If we can figure out the return constraint, the constraint elaborator
  // can support it. Otherwise, it cannot.
  bool VhdlConstraintElaborator::isUnsupportedPragmaFunction(VhdlIndexedName* call_inst)
  {
    bool unsupported_pragma_function = false;
    
    if (call_inst->GetId()->GetPragmaFunction()) {
      // If the return value is constrained, then the 
      // constraint elaborator can support it.
      if (call_inst->GetId()->Constraint() && !call_inst->GetId()->Constraint()->IsUnconstrained()) {
        // Do nothing
      } else if (!call_inst->GetId()->Body()) {
        // If the pragma function does not have a body, we 
        // can't support it.
        unsupported_pragma_function = true;
      } else if (!hasReturnStatement(call_inst->GetId()->Body())) {
        // If the pragma function does not have a return statement,
        // then we can't support it (for now).
        unsupported_pragma_function = true;
      }
    }
    
    return unsupported_pragma_function;
  }

  // Helper method detects existence of a return statement in
  // a subprogram body.
  bool VhdlConstraintElaborator::hasReturnStatement(VhdlSubprogramBody* subprog_body)
  {
    // Visitor class to find return statement
    class RtnStmtFinder : public VhdlVisitor
    {
    public:
      RtnStmtFinder(void) : mFoundReturnStatement(false) {};

    public:
      virtual void VHDL_VISIT(VhdlReturnStatement, node)
      {
        (void)node;
        mFoundReturnStatement = true;
      }
      bool foundReturn(void) {return mFoundReturnStatement;};

    private:
      bool mFoundReturnStatement;
    }; // Class RtnStmtFinder
    
    // Main body of method
    INFO_ASSERT(subprog_body, "Unexpected NULL subprogram body");
    RtnStmtFinder finder;
    subprog_body->Accept(finder);
    return finder.foundReturn();
  }
  
  

}; // namespace Verific2Nucleus

