// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.

******************************************************************************/
#include "verific2nucleus/VerificVhdlDesignWalker.h"    // Visitor base class definition
#include "verific2nucleus/VerificNucleusBuilder.h"      // Nucleus builder class definition
#include "verific2nucleus/V2NDesignScope.h"         // Scope class definition
#include "verific2nucleus/Verific2NucleusUtilities.h"   // Utilities for building nucleus
#include "verific2nucleus/VerificDesignWalkerCB.h" // Included for MACRO VHDL_VISIT_CALL

#include "Array.h"              // Make dynamic array class Array available
#include "Strings.h"            // A string utility/wrapper class
#include "Message.h"            // Make message handlers available, not used in this example

#include "VhdlUnits.h"          // Definition of a libraries and design units
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations
#include "VhdlExpression.h"     // Definitions of all vhdl expressions
#include "VhdlStatement.h"      // Definitions of all vhdl statements
#include "VhdlIdDef.h"          // Definitions of all vhdl identifiers
#include "VhdlName.h"           // Definitions of all vhdl names
#include "VhdlConfiguration.h"  // Definitions of all vhdl configurations
#include "VhdlSpecification.h"  // Definitions of all vhdl specifications
#include "VhdlMisc.h"           // Definitions of miscellaneous vhdl constructrs
#include "VhdlScope.h"
#include "vhdl_tokens.h"
#include "veri_tokens.h"
#include "vhdl_file.h"
#include "veri_file.h"
#include "VhdlValue_Elab.h"

#include "nucleus/NUBase.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUCompositeExpr.h"
#include "util/AtomicCache.h"
#include "util/CarbonAssert.h"
#include "util/UtPair.h"

#include <iostream>

//#define DEBUG_VERIFIC_VHDL_WALKER 1

namespace verific2nucleus {

using namespace Verific;

/** *@brief VhdlIdRef */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlIdRef, node)
{
    DEBUG_TRACE_WALKER("VhdlIdRef", node);

#ifdef DEBUG_VERIFIC_VHDL_WALKER
    static int debug;
    debug++;
    std::cerr << debug << "\tidref = " << node.Name() << std::endl;
#endif

    // Early return from error
    if (hasError()) {
        mCurrentScope->sValue(0);
        return ;
    }
    SourceLocator source_locator = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    VhdlIdDef* id = node.GetId();
    if (!id) {
        mNucleusBuilder->getMessageContext()->Verific2NUFailedToGetID(&source_locator, node.Name());
        mCurrentScope->sValue(0);
        setError(true);
        return ;
    }

    // Look for a range subtype, e.g.
    // subtype my_range is integer range 3 downto 0;
    // and mimic the behavior of VhdlRange to return the range.
    // Required for range id's that appear in RTL such as 
    // vectorvar(my_range) <= "0000";
    if (node.IsRange() && id->IsSubtype() && id->IsDiscreteType()) {
      // If this is a range subtype, evaluate the range.
      // Assume (and assert) range subtypes must have constant left, right values. 
      VhdlConstraint* constraint = node.EvaluateConstraintInternal(0, 0);
      INFO_ASSERT(constraint, "Unexpected NULL constraint for range");
      INFO_ASSERT(constraint->IsRangeConstraint(), "Node is not a range constraint");
      VhdlValue* left = constraint->Left();
      INFO_ASSERT(left && left->IsConstant() && left->IsIntVal(), "Expected an integer constant for left part of range");
      VhdlValue* right = constraint->Right();
      INFO_ASSERT(right && right->IsConstant() && right->IsIntVal(), "Expected an integer constant for right part of range");
      
      // Save values the same way the VhdlRange visitor does. The VhdlArraySlice visitor helper function,
      // arraySlice expects the value vector, range, and values to be set; even though only the 
      // range is relevant.

      // VhdlRange saves 2 NULLs in the value vector when the range is constant.
      UtVector<NUBase*> vec;
      vec.push_back(NULL);
      vec.push_back(NULL);
      mCurrentScope->sValueVector(vec);
      // create UtPair for left and righ constant values when range is constant
      SInt32 start = left->Integer();
      SInt32 end = right->Integer();
      UtPair<SInt32, SInt32> p(start, end);
      mCurrentScope->sRange(p);
      // VhdlRange sets return value of NULL. 
      mCurrentScope->sValue(NULL);

      delete constraint;
      return;

    } else if (id->IsType()) {
      // If this is any other type (e.g. enumeration), then there is nothing to do here. (Why?)
        mCurrentScope->sValue(0);
        return ;
    }
    
    // Steve Lim 2013-12-14 Check against process' sensitivity list
    if (mCurrentScope->gContext() == V2NDesignScope::RVALUE && mInProcessStatement && id->IsSignal())
    {
      // rjc I think: that this is broken for a couple of reasons, first this check looks out of place here,
      // and second the checkAgainstSensList adds the signal to a checked list without regard to the context,
      // so if a signal should be in multiple sensitivity lists we will not detect this problem.
      if (!checkAgainstSensList(id))
      {
        mNucleusBuilder->getMessageContext()->Verific2NUSignalNotInSensitivityList(&source_locator, id->Name());
      }
    }
    // rjc I think: why is there this special case code for handling real values?

    //special case when real value populated using VhdlDataFlow eg. unsup_const/misc19.vhdl
    VhdlValue* eval_val = node.Evaluate(0, mDataFlow, 0);
    if (eval_val && eval_val->IsReal()) {
        CarbonReal dval = eval_val->Real();
        NUConst* the_const = NUConst::create( dval, source_locator);
        V2N_INFO_ASSERT(the_const, node, "invalid real const");
        mCurrentScope->sValue(the_const);
        delete eval_val;
        return ;
    }
    delete eval_val;
    
    bool is_constant_node = isConstantNode(id);
    if (is_constant_node && evaluateConstantValue(node, source_locator)) {
        return ;
    }

    // By the time we get here, all objects declared within the current scope have been
    // placed in the 'object registry'. If a VhdlIdDef is not found in the object
    // registry, then it must be a hierarchical reference to something declared in
    // a package.
    NUBase* obj = getNucleusObject(id);

    
    // If it's not in the registry, then call 'declareHierRefNet' to declare the hierarchical reference net, and return it.
    // 'declareHierRefNet' will also put it in the hierarchical reference map
    //FD thinks: what if we refer something declared from package, we will be here during first referal (not declaration) and obviously it isn't hierarchical reference
    if (!obj) {
      VhdlValue* value = node.Evaluate(0, mDataFlow, 0);
      obj = declareHierRefNet(*id, value);
      delete value;
    }
    
    // Consult the hierarchical reference map to determine if this id is a 
    // non-local net, and if so, create a nucleus hierarchical reference.
    if (obj && mNucleusBuilder->isHierRefIdDef(id)) {
      // Create a hier ref to the non-local net
      NUNet* nonLocalNet = dynamic_cast<NUNet*>(obj);
      INFO_ASSERT(nonLocalNet, "Unexpected NULL net");
      AtomArray netHierName;
      createHierName(id, netHierName);
      obj = nonLocalNet->buildHierRef(getModule(), netHierName);
    }

    if (id->IsAlias()) {
        // TODO: If 'id' is an alias, then we don't need the 'obj' created above,
        // though it doesn't seem to cause harm. 
        id->GetAliasTarget()->Accept(*this);
    } else if (mCurrentScope->gContext() == V2NDesignScope::LVALUE) { 
        //create ident lvalue
        lvalue(obj, source_locator);
    } else if (mCurrentScope->gContext() == V2NDesignScope::RVALUE) { 
        //create ident rvalue
        rvalue(obj, source_locator);
    } else { //if current scope type is not specified than return net
        NUNet* net = dynamic_cast<NUNet*>(obj);
        mCurrentScope->sValue(net);
    }
}

bool VerificVhdlDesignWalker::lvalue(NUBase* obj, SourceLocator& src_loc)
{
    //create lvalue object for specified obj which has NUNet static type
    NUNet* cNet = dynamic_cast<NUNet*>(obj);
    INFO_ASSERT(cNet, "incorrect net type");
    NULvalue* val = mNucleusBuilder->createIdentLvalue(cNet, src_loc);
    mCurrentScope->sValue(val);
    return true;
}

// Make sure we zero- or sign-extend from a particular bit position without modifying
// the size of the subexpression we compute.  This is used to protect a computation from
// later resizing by verilog rules.          
NUExpr* VerificVhdlDesignWalker::buildMask(NUExpr* expr, UInt32 sz, bool signExtend)
{
    const SourceLocator& src_loc = expr->getLoc();
    NUExpr* sizeExpr = NUConst::create(false, sz, 32, src_loc);
    NUOp::OpT op = signExtend ? NUOp::eBiVhExt : NUOp::eBiVhZxt;

    if (NUBinaryOp* bexpr = dynamic_cast<NUBinaryOp*>(expr)) {
        if (bexpr->getOp() == NUOp::eBiVhZxt) {
            expr->setSignedResult(false);
        }
    }
    expr = new NUBinaryOp(op, expr, sizeExpr, src_loc);
    expr->resize(sz);
    expr->setSignedResult(signExtend);
    return expr;
}

// Resizes an integerRvalue by wrapping it in a NUConcatOp so that
// the new size is 32 bits. For signed expressions it preserves the
// signed bit. For unsigned expressions, it zero extends.
// The current thinking is that this resizing is necessary
// because nucleus has no way to encode the fact that these ranged
// integers are actually integers. Creating this wrapper on the original
// expression lets optimizations (like fold) recognize the fact that these
// expressions can be treated as integers and optimized in the same way.      
void VerificVhdlDesignWalker::resizeIntegerRvalue(NUExpr*& expr, bool isSigned)
{
    NUExpr* retval = 0;
    expr->resize(expr->determineBitSize());
    if ( isSigned ) {
        retval = buildMask(expr, 32, true);
    } else {
        // Create a concat that extends the length of this, with zeros, to
        // 32 bits.  Doing it this way instead of the more straightforward
        // ZXT approach folds better and avoids codegen issues.
        expr->setSignedResult( false );
        const SourceLocator &loc = expr->getLoc();
        const UInt32 currentLength = expr->getBitSize();
        const UInt32 extendLength = 32 - currentLength;
        NUExpr* aZero = NUConst::create( false, 0, 1, loc );
        NUExprVector zeroVec;
        zeroVec.push_back( aZero );
        NUConcatOp *zeros = new NUConcatOp( zeroVec, extendLength, loc );
        NUExprVector concatVec;
        concatVec.push_back( zeros );
        concatVec.push_back(expr);
        retval = new NUConcatOp( concatVec, 1, loc );
        retval->resize( 32 );
        retval->setSignedResult( true );
    }
    expr = retval;
}

bool VerificVhdlDesignWalker::rvalue(NUBase* obj, SourceLocator& src_loc)
{
    //create rvalue object for specified obj which has NUNet static type
    NUExpr* val = 0;
    if (NUCompositeNet* cNet = dynamic_cast<NUCompositeNet*>(obj)) {
        val = new NUCompositeIdentRvalue(cNet, src_loc);
        if (cNet->isIntegerSubrange() && (32 > cNet->getBitSize()) && (!cNet->is2DAnything())) {
            resizeIntegerRvalue(val, cNet->isSignedSubrange());
        }
    } else if (NUMemoryNet* mNet = dynamic_cast<NUMemoryNet*>(obj)) {
        val = new NUIdentRvalue(mNet, src_loc);
        if (mNet->isIntegerSubrange() && (32 > mNet->getBitSize()) && (!mNet->is2DAnything())) {
            resizeIntegerRvalue(val, mNet->isSignedSubrange());
        }
    } else if (NUVectorNet* vNet = dynamic_cast<NUVectorNet*>(obj)) {
        val = new NUIdentRvalue(vNet, src_loc);
        if (vNet->isIntegerSubrange() && (32 > vNet->getBitSize()) && (!vNet->is2DAnything())) {
            resizeIntegerRvalue(val, vNet->isSignedSubrange());
        }
    } else if (NUBitNet* bNet = dynamic_cast<NUBitNet*>(obj)) {
        val = new NUIdentRvalue(bNet, src_loc);
        if (bNet->isIntegerSubrange() && (32 > bNet->getBitSize()) && (!bNet->is2DAnything())) {
            resizeIntegerRvalue(val, bNet->isSignedSubrange());
        }
    } else {
        val = 0;
    }
    mCurrentScope->sValue(val);
    return true;
}

// We need expression type for constants, to handle composite like constants which populated as concat expressions
NUExpr* VerificVhdlDesignWalker::createKnownConst(const VhdlIdRef& node, VhdlValue* val, const SourceLocator& src_loc)
{
    if (val->IsPhysicalValue()) {
        char* image = val->Image();
        mNucleusBuilder->getMessageContext()->PhysicalTypesUnsupported(&src_loc, image);
        Strings::free(image);
        INFO_ASSERT(false, "not supported yet");
    } else if (val->IsReal()) {
        INFO_ASSERT(false, "not supported yet");
    } else {
        //FD TODO: don't use val->IsSigned() sign should be detected using id constraint and if that's not available then set implicit type sign (integer- signed, enum -unsigned etc.) 
        //bool is_signed = val->IsSigned() ? true : false;
        UInt32 bits = 0;
        VhdlConstraint* cur_id_constraint = node.GetId()->Constraint();
        if (val->IsComposite()) {
            // There should be at least one element
            INFO_ASSERT(val->NumElements() > 0, "Empty record constant");
            // FD: shouldn't we apply this for each element of composite?
            //VhdlValue* elem = val->ValueAt(0);
            // if element value is integer, get number of bits needed to represent that integer
            bits = 0;
            //if (elem->IsIntVal()) {
            if (cur_id_constraint && (cur_id_constraint->NumBitsOfRange() > 0)) {
                // FD: This works for aggregates with same element type (but most probably is wrong for records with different element types)
                bits = node.GetId()->Constraint()->ElementConstraintAt(0)->NumBitsOfRange();
            }
        } else if (val->IsIntVal()) {
            INFO_ASSERT(cur_id_constraint, "");
            bits = cur_id_constraint->NumBitsOfRange();
        } else if (val->IsEnumValue()) {
            INFO_ASSERT(val->ToEnum(), "");
            INFO_ASSERT(val->ToEnum()->GetEnumId(), "");
            bits = bitness(val->ToEnum()->GetEnumId()->NumOfEnums() - 1);
        } else {
            if (isImplicitIntVal(val)) {
                if (cur_id_constraint) {
                    bits = cur_id_constraint->NumBitsOfRange();
                } else {
                    bits = 32; 
                }
            } else {
                INFO_ASSERT(false, "not supported yet");
            }
        }
        NUExpr* the_const = NULL;
        if (val->IsComposite()) {
            ConstantRange* r = new ConstantRange(bits - 1, 0);
            the_const = createCompositeConst(node.GetId()->Name(), val, src_loc, r);
            delete r;
        } else {
            bool is_signed = val->IsSigned() ? true : false;
            the_const = NUConst::createKnownIntConst(val->Integer(), bits, is_signed, src_loc);
        }
        return the_const;
    }
    return false;
}

bool VerificVhdlDesignWalker::evaluateConstantRecordFieldValue(VhdlSelectedName* node, const SourceLocator& src_loc)
{
    // In order to get constant record field value the following steps done: 
    // 1. first we evaluate value for the prefix object (which is a VhdlValue holding composite constant), 
    // 2. then we evaluate value for the suffix object which is an element of record, and evaluate routine for suffix 
    //    returns the position of this element inside prefix composite 
    //    (according to Verific: Only evaluating an element of a record type, with no context, is not
    //     very meaningful, and the position of that element is the only rational value to return), 
    // 3. next from the prefix composite value we pick up the value at position returned by suffix evaluate. 
    // 4. And at the last we call evaluateConstantValue on this picked up field VhdlValue to populate nucleus constant object 
    //    corresponding to it (evaluateConstantValue sets created nucleus object on scope using mCurrentScope->sValue())
    // 1.
    VhdlName* prefix = node->GetPrefix();
    VhdlIdDef* prefixId = prefix->GetId();
    INFO_ASSERT(prefixId, "Unable to get prefix id");
    VhdlValue* eval_prefixValue = prefix->Evaluate(0, mDataFlow, 0);
    if (eval_prefixValue == NULL) {
      // If unable to evaluate the prefix, then unable to get a constant value.
      return false;
    }
    VhdlValue* prefixValue = eval_prefixValue;
    // 2.
    VhdlName* suffix = node->GetSuffix();
    VhdlIdDef* suffixId = suffix->GetId();
    INFO_ASSERT(suffixId, "Unable to get suffix id");
    VhdlValue* eval_suffixValue = suffix->Evaluate(0, mDataFlow, 0);
    VhdlValue* suffixValue = suffixId->Value() ? suffixId->Value() : eval_suffixValue ;
    INFO_ASSERT(suffixValue, "Unable to get suffix value");
    // 3.
    VhdlValue* fieldValue = prefixValue->ValueAt(suffixValue->Integer());
    // 4.
    bool result = evaluateConstantValue(suffixId, fieldValue, src_loc);
    // delete evaluated objects as they aren't part of Verific parse-tree
    delete eval_prefixValue;
    delete eval_suffixValue;
    return result;
}

bool VerificVhdlDesignWalker::evaluateConstantValue(VhdlIdRef& node, const SourceLocator& src_loc)
{
    VhdlValue* eval_val = node.Evaluate(0, mDataFlow, 0);
    VhdlValue* cur_value = node.GetId()->Value() ? node.GetId()->Value() : eval_val;
    bool result = evaluateConstantValue(node.GetId(), cur_value, src_loc);
    delete eval_val;
    return result;
}

bool VerificVhdlDesignWalker::evaluateConstantValue(VhdlIdDef* cur_id, VhdlValue* cur_value, const SourceLocator& src_loc)
{
    //TODO: refactor/comment this function, seems too messy
    if (cur_value && cur_value->IsConstant()) {
        mValues.push_back(cur_value);
        if (cur_value->IsMetalogical()) {
            char* image = cur_value->Image();
            UtString node_str(image);
            Strings::free(image);
            UtString z_str("'Z'");
            UtString x_str("'X'");
            UtString little_z_str("'Z'");
            UtString little_x_str("'X'");
            NUConst* the_const = 0;
            if (z_str == node_str ||
		little_z_str == node_str
		) {
                the_const= NUConst::createXZ(UtString("z"), false, 1, src_loc);
                //custom case for 'X' character
            } else if (x_str == node_str ||
		       little_x_str == node_str		       
		       ) {
                the_const= NUConst::createXZ(UtString("x"), false, 1, src_loc);
            } else {
                node_str = node_str.substr(1, node_str.size() - 2);
                the_const = NUConst::createXZ(node_str, false, node_str.size(), src_loc);
            }
            NUCLEUS_CONSISTENCY(the_const, src_loc, "failed to create constant for metalogical value");
            mCurrentScope->sValue(the_const);
            return true;
        }
        if (UtString("string") == UtString(cur_id->Type()->Name())) {
            char* image = cur_value->Image();
            UtString str(image);
            str = str.substr(1, str.size() - 2);
            UInt32 s = str.size();
            NUExprVector vec_exp;
            for (UInt32 i = 0; i < s; ++i) {
                UtString tmp = str.substr(i, 1);
                UtString bin_str;
                UInt32 bit_width = composeBinStringFromString(tmp.c_str(), &bin_str, false);
                NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
                INFO_ASSERT(the_const, "invalid constant object");
                the_const->resize(the_const->determineBitSize());
                vec_exp.push_back(the_const);
            }
            NUConcatOp* conc = new NUConcatOp(vec_exp, 1, src_loc);
            INFO_ASSERT(conc, "invalid concatenation object");
            Strings::free(image);
            mCurrentScope->sValue(conc);
            return true;
        }
        if (cur_value->IsPhysicalValue()) {
            char* image = cur_value->Image();
            mNucleusBuilder->getMessageContext()->PhysicalTypesUnsupported(&src_loc, image);
            Strings::free(image);
            INFO_ASSERT(false, "not supported yet");
        } else if (cur_value->IsReal()) {
            INFO_ASSERT(false, "not supported yet");
        } else {
            bool is_signed = false;
            UInt32 bits = 0;
            // FD: somehow IsIntVal flag being set for some composite constants (probably bug in Verific),
            // so checking for composite type first
            VhdlConstraint* cur_id_constraint = cur_id->Constraint();
            // FD TODO: try to get sign from constraint as well and only if that's not set use implicit type sign
            if (cur_value->IsComposite()) {
                // There should be at least one element
                INFO_ASSERT(cur_value->NumElements() > 0, "Empty record constant");
                // if element value is integer, get number of bits needed to represent that integer
                bits = 0;
                if (cur_id_constraint && (cur_id_constraint->NumBitsOfRange() > 0)) {
                    bits = cur_id_constraint->ElementConstraintAt(0)->NumBitsOfRange();
                }
                // for now consider composite value being always unsigned 
                // (according to Verific source, there is a directive which can force composite value to be signed)
                is_signed = false; 
            } else if (cur_value->IsIntVal()) {
                is_signed = true;
                if (! cur_id_constraint) {
                    //misc22
                    NUExpr* the_const = NUConst::createKnownIntConst(cur_value->Integer(), 32,  false, src_loc);
                    the_const->setSignedResult(true);
                    mCurrentScope->sValue(the_const);
                    return true;
                } else {
                    bits = cur_id_constraint->NumBitsOfRange();
                }
            } else if (cur_value->IsBit()) {
                is_signed = false;
                bits = 1;
            } else if (cur_value->IsEnumValue()) {
                is_signed = false;
                if (cur_id->Type() && cur_id->Type()->GetAttributeId("enum_encoding")) {
                    VhdlAttributeSpec* spec = cur_id->Type()->
                        GetAttributeId("enum_encoding")->FindRelevantAttributeSpec(cur_id->Type());
                    if (spec && spec->GetValue()->IsStringLiteral()) {
                        UInt32 bitness = 0;
                        UtString enumStr(spec->GetValue()->Name());
                        UtString::iterator f = enumStr.begin();
                        UtString::iterator l = enumStr.end();
                        for (; f != l; ++f, ++bitness) {
                            if (*f == ' ') { break; }
                        }
                        bits = bitness - 1;
                    } else {
                        INFO_ASSERT(cur_value->ToEnum(), "Failed convert to Enum");
                        INFO_ASSERT(cur_value->ToEnum()->GetEnumId(), "Failed to get Enum ID");
                        bits = bitness(cur_value->ToEnum()->GetEnumId()->NumOfEnums() - 1);
                    }
                } else { 
                    INFO_ASSERT(cur_value->ToEnum(), "Failed convert to Enum");
                    INFO_ASSERT(cur_value->ToEnum()->GetEnumId(), "Failed to get Enum ID");
                    bits = bitness(cur_value->ToEnum()->GetEnumId()->NumOfEnums() - 1);
                }
            } else {
                if (isImplicitIntVal(cur_value)) {
                    is_signed = true;
                    if (cur_id_constraint) {
                        bits = cur_id_constraint->NumBitsOfRange();
                    } else {
                        bits = 32; 
                    }
                } else {
                    INFO_ASSERT(false, "not supported yet");
                }
            }
            NUExpr* the_const = 0;
            if (cur_value->IsComposite()) {
                // FD thinks: we should be able to simplify createCompositeConst interface and get range needed to represent 
                // integer constant inside createCompositeConst as it is available on cur_value
                ConstantRange* r = new ConstantRange(bits - 1, 0);
                the_const = createCompositeConst(cur_id->Name(), cur_value, src_loc, r);
                delete r;
            } else {
            char* image = cur_value->Image();
            UtString str(image);
            Strings::free(image);
            if (UtString("true") == str) {
                the_const = NUConst::createKnownIntConst(1, bits, false , src_loc);
            } else if (UtString("false") == str) {
                the_const = NUConst::createKnownIntConst(0, bits, false, src_loc);
            } else {
                the_const = NUConst::createKnownIntConst(cur_value->Integer(),
                                                         cur_id->Type()->IsStdULogicType() ? 1 :
                                                         cur_id->Type()->IsIntegerType() ? 32 : bits,
                                                         false, src_loc);
            }
            }
            INFO_ASSERT(the_const, "failed to create NUConst object");
            the_const->setSignedResult(is_signed);
            mCurrentScope -> sValue(the_const);
            return true;
        }
    }
    return false;
}

} //namespace verific2nucleus
