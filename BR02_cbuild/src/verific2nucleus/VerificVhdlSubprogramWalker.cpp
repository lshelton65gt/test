// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "verific2nucleus/VerificVhdlDesignWalker.h"    // Visitor base class definition
#include "verific2nucleus/VerificNucleusBuilder.h"      // Nucleus builder class definition
#include "verific2nucleus/V2NDesignScope.h"         // Scope class definition
#include "verific2nucleus/Verific2NucleusUtilities.h"   // Utilities for building nucleus
#include "verific2nucleus/VerificDesignWalkerCB.h" // Included for MACRO VHDL_VISIT_CALL

#include "Array.h"              // Make dynamic array class Array available
#include "Strings.h"            // A string utility/wrapper class
#include "Message.h"            // Make message handlers available, not used in this example

#include "VhdlUnits.h"          // Definition of a libraries and design units
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations
#include "VhdlExpression.h"     // Definitions of all vhdl expressions
#include "VhdlStatement.h"      // Definitions of all vhdl statements
#include "VhdlIdDef.h"          // Definitions of all vhdl identifiers
#include "VhdlName.h"           // Definitions of all vhdl names
#include "VhdlConfiguration.h"  // Definitions of all vhdl configurations
#include "VhdlSpecification.h"  // Definitions of all vhdl specifications
#include "VhdlMisc.h"           // Definitions of miscellaneous vhdl constructrs
#include "VhdlScope.h"
#include "VhdlValue_Elab.h"
#include "vhdl_tokens.h"
#include "vhdl_file.h"
#include "veri_file.h"
#include "VhdlDataFlow_Elab.h"

#include "nucleus/NUBase.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUSysFunctionCall.h"
#include "util/AtomicCache.h"
#include "util/CarbonAssert.h"

#include <iostream>             // standard IO

//#define DEBUG_VERIFIC_VHDL_WALKER 1

namespace verific2nucleus {

using namespace Verific ;

/**
 *@brief VhdlSubprogramDecl
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlSubprogramDecl, node)
{
  DEBUG_TRACE_WALKER("VhdlSubprogramDecl", node);
  (void)node;
  return ; //negative/sub_decl5.vhdl
}

/**
 *@brief VhdlSubprogInstantiationDecl
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlSubprogInstantiationDecl, node)
{
  DEBUG_TRACE_WALKER("VhdlSubprogInstantiationDecl", node);
  (void)node;
  INFO_ASSERT(false, "TODO");
}

/**
 *@brief VhdlInterfaceSubprogDecl
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlInterfaceSubprogDecl, node)
{
  DEBUG_TRACE_WALKER("VhdlInterfaceSubprogDecl", node);
  (void)node;
  INFO_ASSERT(false, "TODO");
}

/**
 * @brief VhdlSubprogramBody
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlSubprogramBody, node) {
  DEBUG_TRACE_WALKER("VhdlSubprogramBody", node);

  mIsInSubprogram = true;
  // Push the new subprogram registry stack
  mVerificToNucleusObject.pushMapStack();
  
  SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder, node.Linefile());
  NUModule* module = getModule();
  NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, loc, "Unable to get module");
  //process the definition
  // Populate the NUTask and formal parameters
  node.GetSubprogramSpec() -> Accept(*this);
  //NUTask object is already created in getsubprogramspec
  NUTask* the_procedure = getTask();
  NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(the_procedure, loc, "Unable to get task scope");
  // Create a block to hold the statements in the procedure (this needs to be done before declarations population)
  NUBlock* block = mNucleusBuilder->cBlock(the_procedure, loc);
  the_procedure->addStmt(block);
  // Populate declarations (into task scope directly)
  unsigned i = 0;
  VhdlTreeNode* item = 0;
  FOREACH_ARRAY_ITEM(node.GetDeclPart(), i, item) {
      if (dynamic_cast<VhdlSubprogramBody*>(item)) {
          continue;
      }
      item->Accept(*this);
  }
  pushDesignScope(block, 0);
  //mark scope as an subprogram body
  //need for return statement
  mCurrentScope->sSubprogramBody();
  //iterate through the statements and add to the block
  i = 0;
  VhdlStatement* cur_stmt = 0;
  FOREACH_ARRAY_ITEM(node.GetStatementPart(), i, cur_stmt){
    if (!cur_stmt) continue;
      if (ID_VHDLASSERTIONSTATEMENT == cur_stmt->GetClassId()) {
          SourceLocator stmt_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder, cur_stmt->Linefile());
          mNucleusBuilder->getMessageContext()->Verific2NUAssertNotSupported(&stmt_loc);
          continue;
      }
      if (hasError()) {
          mCurrentScope -> sValue(NULL);
          return ;
      }
      cur_stmt -> Accept(*this);
      NUBase* cur_stmt_val = mCurrentScope-> gValue();
      if (cur_stmt_val) {
          block -> addStmt(static_cast<NUStmt*>(cur_stmt_val));
      } else {
          UtVector<NUBase*> vec;
          mCurrentScope->gValueVector(vec);
          UInt32 s = vec.size();
          for (UInt32 j = 0; j < s; ++j) {
              if (vec[j]) {
                  block->addStmt(static_cast<NUStmt*>(vec[j]));
              }
          }
      }
  }

  // now check to see that at least one return statement was processed (processing that statement has the side effect of adding an implicit net to the "the_procedure")
  // if none was processed then we create and insert that return net now.

  V2NDesignScope* pScope = getTaskV2NDesignScope();
  if ( NULL == pScope->gReturnNet() ){
    NUNet* retNet = NULL;
    //misc/SUB22 has a function with no statements, so it has no return statement so cbuild needs this section of code
    VhdlFunctionSpec* fSpec = dynamic_cast<VhdlFunctionSpec*>(node.GetSubprogramSpec());
    // if node is for a procedure then fSpec is null.  A new output net is only needed when fSpec is for a function (VhdlFunctionSpec)
    if ( fSpec ) {
      if (fSpec->GetReturnType()) {
        VhdlConstraint* returnConstraint = fSpec->GetReturnType()->GetId()->Constraint();
        retNet = createNucleusNet(fSpec->GetReturnType(), returnConstraint, node.Linefile());
        the_procedure->addArgFirst(retNet, NUTF::eCallByAny);
        pScope->sReturnNet(retNet);
      } else {
        UtString msg("unable to create/add return value for function: ");
        msg << node.LocalScope()->GetOwner()->OrigName();
        mNucleusBuilder->getMessageContext()->Verific2NUErrorDuringPopulation(&loc, msg.c_str());
        mCurrentScope->sValue(0);
        setError(true);
        return;
      }
    }
  }

  //add the task to the module
  module -> addTask(the_procedure);
  //this is a side-effect
  mCurrentScope -> sValue(NULL);
  //pop task's block scope
  popScope(); // Pop for L3
  //pop task scope
  // FD: why do we need to pop task scope here ?
  popScope(); // Pop for L1, L2

  // Push the new subprogram registry stack
  mVerificToNucleusObject.popMapStack();
  mIsInSubprogram = false;
}

/**
 *@brief VhdlSubprogramId: Subprogram identifier
 *
 *       Eg: myfunction(arg1, arg2) -- 'myfunction' is the subprogram id
 *
 *       From here we can get to the body of the function 
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlSubprogramId, node) {
  DEBUG_TRACE_WALKER("VhdlSubprogramId", node);

  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  
  //check if subprogram is inside some signed ieee package
  //if yes than we have to mark as signed all operations in
  //subprogram body
  if (node.GetOwningScope()) {
      UtString packageName(node.GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit()->Name());
      if (Verific2NucleusUtilities::isIeeeDecl(&node)) {
          // Steve Lim 2013-12-30 This is not a sufficient check - what about numeric_std package
          if(UtString("std_logic_signed") == packageName) {
              mIsFromSignedPackage = true;
          }
      }
  }

  // Figure out if we're in a package.
  // RBS thinks: might be sufficient to just check for local scope (as above)
  bool in_package = false;
  if (   node.Spec() -> GetDesignator()
      && node.Spec() -> GetDesignator()->LocalScope() 
      && node.Spec() -> GetDesignator() ->LocalScope()-> GetContainingPrimaryUnit()
      && node.Spec() -> GetDesignator() ->LocalScope()-> GetContainingPrimaryUnit() -> IsPackage()) {
    in_package = true;
  }
  
  
  // check for pragmas on VhdlSpecification in package header 
  Map* package_header_pragmas = NULL;
  if (node.Spec() -> GetDesignator()){
     package_header_pragmas =  node.Spec() -> GetDesignator() -> gPragmas();
     if (package_header_pragmas){
       SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
       UtString packageName(node.Spec() -> GetDesignator() ->LocalScope()-> 
                            GetContainingPrimaryUnit() -> Name());
       NUModule* module = mNucleusBuilder->gModule(dynamic_cast<NUScope*>(mCurrentScope->gOwner()));
       V2N_INFO_ASSERT(module, node, "null owner module");
       UtString moduleName(module->getName()->str()); 
       // Get deuniquified function name
       const char* uniquified_name = node.Name();
       UtString funcName = Verific2NucleusUtilities::getDeuniquifiedName(uniquified_name);
       addPragmas(&funcName, &moduleName, &packageName, package_header_pragmas, in_package, &src_loc);
     }
  }
  
  // check for pragmas on the function body.
  // Note: CSE will cause node.gPragmas() and node.Spec()->GetDesignator()->gPragmas() to be the same.
  Map* function_body_pragmas =  node.gPragmas();
  if (function_body_pragmas && (function_body_pragmas != package_header_pragmas)) {
#ifdef DEBUG_VERIFIC_VHDL_WALKER
      printf("Got %d pragmas\n",pragmas -> Size());
#endif
      UtString packageName;
      if (in_package) {
        packageName = node.GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit()->Name();
      }
      // Get module name
      NUModule* module = getModule();
      NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, src_loc, "null owner module");
      //UtString moduleName = UtString(module->getName()->str());
      UtString moduleName(module->getName()->str()); 
      // Get deuniquified function name
      const char* uniquified_name = node.Name();
      UtString funcName = Verific2NucleusUtilities::getDeuniquifiedName(uniquified_name);
      addPragmas(&funcName, &moduleName, &packageName, function_body_pragmas, in_package, &src_loc);
  }
  node.Body()->Accept(*this);
  mIsFromSignedPackage = false;
}




/**
 *@brief VhdlFunctionSpec
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlFunctionSpec, node) {
  DEBUG_TRACE_WALKER("VhdlFunctionSpec", node);

  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  //get designator and create NUTask object
  VhdlIdDef* designator = node.GetDesignator();
  INFO_ASSERT(designator, "undefined VhdlIdDef for designator for a VhdlFunctionSpec");

  const char* uniquified_name = designator->OrigName();
  // deuniquify the name before storing in nucleus
  // UtString funcName(designator->OrigName());
  UtString funcName = Verific2NucleusUtilities::getDeuniquifiedName(uniquified_name);
  NUModule* module = getModule();
  NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, src_loc, "Unable to get parent module");
  UtString name = module->gensym(funcName.c_str())->str();
  NUTask* task = mNucleusBuilder->cTF(name, funcName, module, src_loc);
  mCurrentScope->sValue(task);
  //create task scope object
  V2NDesignScope* scope = new V2NDesignScope(task, mCurrentScope);
  //will pop in subprogram body
  //we have to do this because there
  //may be some local declarations inside subprogram
  pushScope(scope);
  unsigned i = 0; 
  VhdlTreeNode* item = 0;
  //we need this flag for creating necessary net flags
  mCurrentScope->sContext(V2NDesignScope::SUB_PROG_DECL);
  //create formal parameters
  FOREACH_ARRAY_ITEM(node.GetFormalParamList(), i, item) {
      item->Accept(*this);
  }
}

/**
 *@brief VhdlProcedureSpec
 */
void VerificVhdlDesignWalker::VHDL_VISIT(VhdlProcedureSpec,node){
  DEBUG_TRACE_WALKER("VhdlProcedureSpec", node);
  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  //get designator and create NUTask object
  VhdlIdDef* designator = static_cast<VhdlIdDef*>(node.GetDesignator());
  VERIFIC_CONSISTENCY_NO_RETURN_VALUE(designator, node, "invalid VhdlIdDef object");
  const char* uniquified_name = designator->OrigName();
  // deuniquify the name before storing in nucleus
  // UtString funcName(designator->OrigName());
  UtString funcName = Verific2NucleusUtilities::getDeuniquifiedName(uniquified_name);
  NUModule* module = getModule();
  NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, src_loc, "Unable to get parent module");
  UtString name = module->gensym(funcName.c_str())->str();
  NUTask* task = mNucleusBuilder->cTF(name, funcName, module, src_loc);
  mCurrentScope->sValue(task);
  //create task scope object
  V2NDesignScope* scope = new V2NDesignScope(task, mCurrentScope);
  //will pop in subprogram body
  //we have to do this because of there
  //may be some local declarations inside subprograms
  pushScope(scope); //L1
  unsigned i = 0; 
  VhdlTreeNode* item = 0;
  //we need this flag for creating necessary net flags
  mCurrentScope->sContext(V2NDesignScope::SUB_PROG_DECL);
  //create formal parameters
  FOREACH_ARRAY_ITEM(node.GetFormalParamList(), i, item) {
      item->Accept(*this);
  }
}

// The VHDL function RESIZE() handles the size truncation in different way.
// Here the MSB of the truncated expr is the MSB of the original
// expr. So, simple part select won't work.
// example: out1 <= resize(in1, 4), we will convert the
// rvalue as : bitSize(in1) > 4 ? (  ( (in1 >> (bitSize(in1) -1)) << (4 -1) ) |
//                                (eBiVhExt,in1,4) ) : (eBiVhExt,in1,4);
// If in1 is "11111011", RESIZE(in1,3) should return "111" . In the above case
// we will get this result as:
// 8 > 3 ? ( ( ( "11111011" >> (8 -1) ) << ( 3 -1 ) ) |
// (eBiVhExt,"11111011",3 -1) ) : (eBiVhExt,"11111011",3)
// ==> ( ( ( "11111011" >> (8 -1) ) << ( 3 -1 ) ) | (eBiVhExt,"11111011",2) )
// ==> ( ( ( "00000001" ) << 2 ) | ( "11" ) )
// ==> ( (  "100" ) | ( "11" ) )
// ==> ( "111" )
//
// Another example, consider the in1 is "01111111", here the expected result 
// is" "011" . You will get this result if you fit this into the above formula.
//
// this is a direct copy of the interra flow version, the interra version will be removed in the future
// we cannot move this to a static method in NucleusPopulateUitl because it calls createSizeExpr() 
NUExpr* VerificVhdlDesignWalker::createExprForResizeFunc( NUExpr* extdExpr, 
                                                          UInt32 lopWidth, 
                                                          const SourceLocator& loc,
                                                          UInt32 desiredResultWidth)
{
  CopyContext copy_context(NULL, NULL);

  NUBinaryOp* extendedExpr = dynamic_cast<NUBinaryOp*>(extdExpr);

  UInt32 cur_width = extdExpr->getBitSize();
  if ( ( NULL == extendedExpr ) && ( desiredResultWidth == cur_width ) ){
    return extdExpr;            // the expression is already the right size, no resize expression needed
  }
  
  // The second expression of the ternary expression is the original VhExt
  NU_ASSERT (extendedExpr, extdExpr);

  // First check if the ReSizeExpr is constant. If so, then we will not
  // create the ternary expression because we know that whether the call
  // to RESIZE() is for size extension or truncation. Accordingly we will 
  // create the truncated expression or return the extended expression.
  //
  // VHDL says 2nd arg to RESIZE is 'natural', and internally, we use the eBiLshift
  // and eBiRshift operators to do the shifting.  These operators require UNSIGNED
  // shift counts (and NU_ASSERT otherwise!).
  //
  NUExpr* resizeAmount = extendedExpr->getArg(1);
  NUConst *constResize = resizeAmount->castConst();
  bool isSizeTruncation = false;
  if (constResize) {
    UInt32 sizeValue = 0;
    constResize->getUL(&sizeValue);
    if (sizeValue >= lopWidth) {
      // The call to resize was for size extension
      return extendedExpr;
    } else {
      isSizeTruncation = true;
    }
  }
  NUExpr* sizeExpr = resizeAmount->copy(copy_context);

  // VHDL treats the size argument as a NATURAL number, so remove any
  // signedness from the result expression.  If we were doing runtime checking,
  // VHDL would require us to assert on a negative signed value.
  sizeExpr->setSignedResult (false);

  // Create the expression: final_size -1 i.e. ( 3 -1 ) in the example
  NUExpr *one = NUConst::create(false, UInt64(1),  32, loc);

  NUExpr *finalSizeMinusOne = new NUBinaryOp(NUOp::eBiMinus, sizeExpr, one, loc);
  finalSizeMinusOne->resize(finalSizeMinusOne->determineBitSize());
  NUExpr *finalSizeMinusOne_c2 = finalSizeMinusOne->copy(copy_context); // copy 2 of finalSizeMinusOne

  // Create given input_expression_bit_count - 1 i.e. ( 7 - 1 ) in the example
  NUExpr* orgBitCount = NUConst::create(false, UInt64(lopWidth), 32, loc);
  orgBitCount->resize(orgBitCount->determineBitSize());

  NUExpr *bitCountMinusOne = new NUBinaryOp(NUOp::eBiMinus, orgBitCount, one->copy(copy_context), loc);
  bitCountMinusOne->resize(bitCountMinusOne->determineBitSize());

  // Now create the right shift expression i.e. in1 >> 7 in the example
  NUExpr* lop = extendedExpr->getArg(0)->copy(copy_context);
  NUExpr* lop_c2 = lop->copy(copy_context); // copy 2 of lop expression
  NUExpr* rshiftExpr = new NUBinaryOp(NUOp::eBiRshift, lop, bitCountMinusOne, loc);
  UInt32 new_size = std::max (desiredResultWidth, rshiftExpr->determineBitSize ());
  rshiftExpr = rshiftExpr->makeSizeExplicit(new_size);
  rshiftExpr->setSignedResult(true);

  // Create the left shift expression i.e. ( (in1 >> 7) << 2 ) in the example
  NUExpr* orgMsb = new NUBinaryOp(NUOp::eBiLshift, rshiftExpr, finalSizeMinusOne, loc); 
  orgMsb->resize(orgMsb->determineBitSize());
  orgMsb->setSignedResult(true);

  NUConst* k = NUConst::create(false, desiredResultWidth, 32, loc);
  orgMsb = createSizeExpr(orgMsb, k, loc, desiredResultWidth, true); // RJC thinks: why is desiredResultWidth used here? the width of the size expression is probably 32
  if (orgMsb == NULL) {
    return NULL;
  }
    
  orgMsb->resize(desiredResultWidth);

  // Now create the temporary VHDL extension expression i.e. EXT(in1, 2) example
  // 
  NUExpr* tempExpr = createSizeExpr(lop_c2, finalSizeMinusOne_c2, loc, desiredResultWidth, false); // RJC thinks: why is desiredResultWidth used here? the width of the size expression is probably 32
  if (tempExpr == NULL) {
    return NULL;
  }
  tempExpr->setSignedResult(true);
  tempExpr->resize(desiredResultWidth);

  // Finally create the truncated expression.
  NUExpr* truncatedExpr = new NUBinaryOp(NUOp::eBiBitOr, tempExpr, orgMsb, loc);
  truncatedExpr->setSignedResult(true);
  truncatedExpr->resize(truncatedExpr->determineBitSize());
  if (isSizeTruncation)
  {
    delete extendedExpr;
    return truncatedExpr;
  }
  // Create the ternary condition i.e (8 > 4) in the given example
  NUExpr* condExpr = new NUBinaryOp( NUOp::eBiUGtr, 
                                     orgBitCount->copy(copy_context), 
                                     sizeExpr->copy(copy_context), loc );
  condExpr->resize(1);

  // The final ternary expression corresponding to the RESIZE() function
  // call with signed argument.
  NUExpr* finalExpr = new NUTernaryOp(NUOp::eTeCond, condExpr, truncatedExpr, 
                                      extendedExpr, loc);
  finalExpr->setSignedResult(true);
  finalExpr->resize(desiredResultWidth);

  return finalExpr;
}

// Rick thinks: Pragma functions seem to be functions that have synthesis attributes
// associated with them, that aid in the synthesis process. The call to VhdlIdDef::GetPragmaFunction()
// returns a code identifying the function (or_reduce, nor_reduce, or, nor, etc).
//
// However, I've noticed the code for 'or_reduce' is 904 (VHDL_REDOR, defined in vhdl_tokens.h),
// yet we don't have a case item for that here (we have VHDL_or_reduce, defined in vhdl_yacc.h).
// 
// If this function returns false, the pragma function was not recognized, and we
// proceed with normal function population.
bool VerificVhdlDesignWalker::processPragmaFunctionCall(VhdlIndexedName& node, Array*& arguments)
{
    SourceLocator loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    //gets subprogram ID
    VhdlSubprogramId* sId = dynamic_cast<VhdlSubprogramId*>(node.GetId());
    V2N_INFO_ASSERT(sId, node, "invalid subprogram identifier");

    //process arguments
    unsigned l = 0;
    VhdlTreeNode* item = 0;
    NUExpr* expr1 = 0;
    NUExpr* expr2 = 0;
    FOREACH_ARRAY_ITEM(arguments, l, item) {
        mCurrentScope->sContext(V2NDesignScope::RVALUE);
        item->Accept(*this);
        INFO_ASSERT(mCurrentScope->gValue() , "failed to populate task argument");
        if (0 == l) {
            expr1 = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
            INFO_ASSERT(expr1, "failed to populate first argument");
        } else if (1 == l) {
            expr2 = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
            INFO_ASSERT(expr2, "failed to populate second argument");
        }
    }

    //gets pragma
    unsigned pragmaFunc = sId ->GetPragmaFunction();

    NUOp::OpT op = NUOp::eInvalid;
    unsigned exprNaryCount = 2;
    bool isSigned = false;
    bool invert = false;
    bool isApplicable = true;
    
    switch (pragmaFunc) {
    case VHDL_i_to_s : exprNaryCount = 0; expr1 = createSizeExpr(expr1, expr2, loc, 0, true); break;
    case VHDL_s_to_i:
    case VHDL_u_to_i: exprNaryCount = 0; expr1->setSignedResult(true); delete expr2; break;
    case VHDL_s_to_u: exprNaryCount = 0; expr1->setSignedResult(false); delete expr2; break;
    case VHDL_buf:
    case VHDL_FEEDTHROUGH : exprNaryCount = 0; delete expr2; break; //feed_thru, feed_through
    case VHDL_u_to_u:
    case VHDL_uns_resize : {
        if (expr2) {
            NUConst* constSize = expr2->castConst();
            UInt32 sizeValue = 0;
            if (constSize) {
                constSize->getUL(&sizeValue);
                if (0 >= sizeValue) {
                    mNucleusBuilder->getMessageContext()->Verific2NUFailedToResize(&loc);
                    mCurrentScope->sValue(0);
                    setError(true);
                    return false;
                }
            }
            if (sizeValue) {
                expr1 = createSizeExpr(expr1, expr2, loc, sizeValue, false);
            }
        }
        exprNaryCount = 0;
        break;
    }
    case VHDL_s_xt : 
    case VHDL_resize : {
        VhdlDiscreteRange* arg1 = dynamic_cast<VhdlDiscreteRange*>((VhdlTreeNode*)arguments->At(0));
        INFO_ASSERT(arg1, "invalid actual argument");
        VhdlConstraint *constr = arg1->EvaluateConstraintInternal(mDataFlow, 0) ;
        if (!constr) {
            mNucleusBuilder->getMessageContext()->Verific2NUFailedToEvaluateConstraint(&loc);
            mCurrentScope->sValue(0);
            setError(true);
            return false;
        }
        unsigned len = constr->Length();
        delete constr;
        expr1->resize(len);
        UInt32 sz = mCurrentScope->gSize();
        expr1 = createSizeExpr(expr1, expr2, loc, sz, true);
        if (!expr1) {
            mNucleusBuilder->getMessageContext()->Verific2NUFailedToResize(&loc);
            mCurrentScope->sValue(0);
            setError(true);
            return false;
        }
        expr1 = createExprForResizeFunc(expr1, len, loc, sz);
        expr1->setSignedResult(true);
        exprNaryCount = 0;
        break;
    }
    case VHDL_EQUAL : op = NUOp::eBiEq; break; //eql
    case VHDL_NEQUAL : op = NUOp::eBiNeq; break; //neq
    case VHDL_and : op = NUOp::eBiBitAnd; break; //and
    case VHDL_or : op = NUOp::eBiBitOr; break; //or
    case VHDL_xor : op = NUOp::eBiBitXor; break; //xor
    case VHDL_nand : invert = true; op = NUOp::eBiBitAnd; break; //nand
    case VHDL_nor : invert = true; op = NUOp::eBiBitOr; break; //nor
    case VHDL_xnor : invert = true; op = NUOp::eBiBitXor; break; //xnor
    case VHDL_numeric_signed_mult : 
    case VHDL_signed_mult : isSigned = true; op = NUOp::eBiSMult; break; //signed_mult
    case VHDL_numeric_unsigned_mult : 
    case VHDL_unsigned_mult : op = NUOp::eBiUMult; break; //unsigned_mult
    case VHDL_numeric_sign_div : isSigned = true; op = NUOp::eBiSDiv; break; //numeric_sign_div
    case VHDL_numeric_uns_div : op = NUOp::eBiUDiv; break; //numeric_uns_div
    case VHDL_MINUS :
    case VHDL_numeric_minus : op = NUOp::eBiMinus; break; //numeric_minus
    case VHDL_PLUS :
    case VHDL_numeric_plus : op = NUOp::eBiPlus; break; //numeric_plus
    case VHDL_is_uns_less : op = NUOp::eBiULt; break; //is_uns_less
    case VHDL_is_uns_gt : op = NUOp::eBiUGtr; break; //is_uns_gt
    case VHDL_is_gt : op = NUOp::eBiSGtr; break; //is_gt
    case VHDL_is_less : op = NUOp::eBiSLt; break; //is_less
    case VHDL_is_uns_gt_or_equal : op = NUOp::eBiUGtre; break; //is_uns_gt_or_equal
    case VHDL_is_uns_less_or_equal : op = NUOp::eBiULte; break; //is_uns_less_or_equal
    case VHDL_is_gt_or_equal : op = NUOp::eBiSGtre; break; //is_gt_or_equal
    case VHDL_is_less_or_equal : op = NUOp::eBiSLte; break; //is_less_or_equal
    case VHDL_SEQUAL : op = NUOp::eBiSLte; break; //is_less_or_equal
    case VHDL_STHAN  : op = NUOp::eBiSLt; break; //is_less
    case VHDL_sll : op = NUOp::eBiVhLshift; break; //sll
    case VHDL_srl : op = NUOp::eBiVhRshift; break; //srl
    case VHDL_sla : op = NUOp::eBiVhLshiftArith; break; //sla
    case VHDL_sra : op = NUOp::eBiVhRshiftArith; break; //sra
    case VHDL_rol : op = NUOp::eBiRoL; break; //rol
    case VHDL_ror : op = NUOp::eBiRoR; break; //ror
    case VHDL_numeric_mod : op = NUOp::eBiVhdlMod; break; //numeric_mod
        //unary
    case VHDL_and_reduce : exprNaryCount = 1; op = NUOp::eUnRedAnd; break; //and_reduce
    case VHDL_or_reduce : exprNaryCount = 1; op = NUOp::eUnRedOr; break; //or_reduce
    case VHDL_xor_reduce : exprNaryCount = 1; op = NUOp::eUnRedXor; break; //xor_reduce
    case VHDL_abs : exprNaryCount = 1; op = NUOp::eUnAbs; break; //abs
    case VHDL_numeric_unary_minus : exprNaryCount = 1; op = NUOp::eUnMinus; break; //numeric_unary_minus
    case VHDL_not : exprNaryCount = 1; op = NUOp::eUnVhdlNot; break; //not

    default: {
        isApplicable = false;
    }
    }

    if (!isApplicable) {
        if (expr1) delete expr1;
        if (expr2) delete expr2;
        return false;
    }

    NUExpr* expr = 0;
    if (2 == exprNaryCount) {
        INFO_ASSERT(expr1, "invalid expression");
        INFO_ASSERT(expr2, "invalid expression");
        expr = new NUBinaryOp(op, expr1, expr2, loc);
        expr->setSignedResult(isSigned);
    } else if (1 == exprNaryCount) {
        INFO_ASSERT(expr1, "invalid expression");
        expr = new NUUnaryOp(op, expr1, loc);
        expr->setSignedResult(isSigned);
    } else {
        INFO_ASSERT(0 == exprNaryCount, "invalid value for exprNaryCount");
        expr = expr1;
    }
    if (invert) {
        expr = new NUUnaryOp((1 == expr->determineBitSize()) ? NUOp::eUnLogNot : NUOp::eUnBitNeg,
                             expr, expr->getLoc());
    }

    mCurrentScope->sValue(expr);
    return true;
}

void VerificVhdlDesignWalker::functionCall(VhdlIndexedName& node)
{
  SourceLocator call_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "functionCall -> " ;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    //check existance for function call body
    VhdlSubprogramId* sId = dynamic_cast<VhdlSubprogramId*>(node.GetId());
    V2N_INFO_ASSERT(sId, node, "invalid subprogram identifier");

    //check for procedure
    // RBS thinks: This is dead. functionCall is only called from the VhdlIndexed visitor,
    // and ONLY if sId->IsProcedure() is not true.
    if (sId->IsProcedure()) {
        procCall(node);
        return ;
    }

    // RBS I think: To answer Steve's quiestion, "endfile" is probably a builtin
    // function without a function body, and so we don't want to issue the error in
    // that case. There are probably others. Isn't the missing subprogram body
    // checked by the parser/elaborator? Answer: No, apparently not!
    VhdlSubprogramBody* sBody = sId->Body();
    if (! sBody) {
        if (UtString("endfile") != UtString(sId->Name())) { // Steve Lim 2013-12-16: Why this endfile check?
            if (namedFunction(sId))
              mNucleusBuilder->getMessageContext()->Verific2NUVhdlNamedFctsNotSupported(&call_loc, sId->Name());
            else
              mNucleusBuilder->getMessageContext()->Verific2NUNoSubprogramBody(&call_loc, sId->Name());
            mCurrentScope->sValue(0);
            return ;
        }
    }
    
    //handle system calls
    // RBS: This is duplicated in procCall
    if (checkSysCall(node)) {
        if (NUBase*  base = checkAndHandleSysCall(node)) {
            mCurrentScope->sValue(base);
        } else {
            mCurrentScope->sValue(0);
        }
        return ;
    }

    // Construct array of actuals to pass to 'elaborateSubprogramBody'
    Array* arguments = new Array;
    unsigned i = 0;
    VhdlTreeNode* item = 0;
    FOREACH_ARRAY_ITEM(node.GetAssocList(), i, item) {
        VhdlDiscreteRange* actual = static_cast<VhdlDiscreteRange*>(item);
        V2N_INFO_ASSERT(actual, node, "failed to get actual argument");
        VhdlExpression* actualPart = actual->ActualPart();
        arguments->Insert(actualPart);
    }

    // If we recognize the pragma function, then go ahead and process it inline.
    if (sId->GetPragmaFunction()) {
        if (processPragmaFunctionCall(node, arguments)) {
            delete arguments;
            return ;
        }
    }

    elaborateSubprogramBody(sBody, arguments, call_loc);
    
    delete arguments;
}

void VerificVhdlDesignWalker::operatorOverload(UtVector<VhdlExpression*> & actuals, VhdlSubprogramId& node)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << std::endl;
    std::cerr << "operator overload = " << node.Name() << std::endl;
#endif

    SourceLocator call_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

    Array* arguments = new Array;
    for (UInt32 i = 0; i < actuals.size(); ++i) {
        arguments->Insert(actuals[i]);
    }

    VhdlSubprogramBody* sBody = node.Body();
    elaborateSubprogramBody(sBody, arguments, call_loc);

    delete arguments;
}

// Create an always block, if needed, and add the supplied statement.
// This is used for function/procedure task enables, and for 'if' statements 
// constructed for conditional and selected signal assigns.
//
// As background, note that function calls always result in two nucleus
// statements being populated: (1) the nucleus task enable, and (2) the
// assignment of the task output to the LHS of the assignment containing
// the function call (or to a temporary variable). This method is used
// to populate the task enable.
//
// An always block must be created to contain the task enable whenever a
// function or procedure call appears in a concurrent statement. An always
// block must also be created for the 'if' statements generated for conditional 
// and selected signal assigns, since these are always concurrent statements, 
// and will therefore never be inside containing NUBlock.
//
void VerificVhdlDesignWalker::cAlwaysBlockAndAddStatement(NUStmt* stmt, const SourceLocator& src_loc)
{
  NUBlock* block = getBlock();
  if (block) {
      // We are already within a block (e.g. always block, or task/function), so add the task enable to it.
      mNucleusBuilder->aStmtToBlock(stmt, block);
  } else {
    // We are not within a block; so create an always block, and add task enable to it.
    NUModule* module = getModule();
    NUCLEUS_CONSISTENCY_NO_RETURN_VALUE(module, src_loc, "Unable to get module");
    block = mNucleusBuilder->cBlock(module, src_loc);
    (void)mNucleusBuilder->createAlwaysBlockInScope(module, block, false, NUAlwaysBlock::eAlways,  src_loc);
    // add task enable statement to the block (not always block)
    mNucleusBuilder->aStmtToBlock(stmt, block);
  }
}

//checking only for fileend
bool VerificVhdlDesignWalker::checkSysCall(const VhdlIndexedName& node) const
{
    SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
    UtString pcall(node.GetPrefix()->Name());
    if (ID_VHDLSELECTEDNAME == node.GetPrefix()->GetClassId()) {
        VhdlSelectedName* selName = static_cast<VhdlSelectedName*>(node.GetPrefix());
        INFO_ASSERT(selName, "");
        VhdlDesignator* designator = selName->GetSuffix();
        INFO_ASSERT(designator, "");
        pcall = UtString(designator->Name());
    }
    UtString fopen("file_open");
    UtString fclose("file_close");
    UtString read("read");
    UtString hread("hread");
    UtString oread("oread");
    UtString write("write");
    UtString owrite("owrite");
    UtString hwrite("hwrite");
    UtString writeline("writeline");
    UtString readline("readline");
    UtString endfile("endfile");
    if (fopen == pcall     ||
        fclose == pcall    ||
        read == pcall      ||
        hread == pcall     ||
        oread == pcall     ||
        write == pcall     ||
        owrite == pcall     ||
        hwrite == pcall     ||
        writeline == pcall ||
        readline == pcall  ||
        endfile == pcall) {

        if (endfile == pcall) {
            VhdlSubprogramId* sId = static_cast<VhdlSubprogramId*>(node.GetId());
            V2N_INFO_ASSERT(sId, node, "failed to get subprogram id");
            INFO_ASSERT(sId->GetOwningScope(), "");
            INFO_ASSERT(sId->GetOwningScope()->GetContainingPrimaryUnit(), "");
            INFO_ASSERT(sId->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit(), "");
            StringAtom* packAtom = mNucleusBuilder->gCache()->intern(UtString(sId->GetOwningScope()->
                                                                              GetContainingPrimaryUnit()->GetPrimaryUnit()->
                                                                              Name()));
            if (1 != sId->GetArgs()->Size()) {
                return false;
            }
            VhdlIdDef* item = 0;
            unsigned i = 0;
            FOREACH_ARRAY_ITEM(sId->GetArgs(), i, item) {
                if ((UtString("text") == UtString(item->BaseType()->Name()))
                    && (UtString("textio") != UtString(packAtom->str()))) {
                    return false;
                }
            }
        } else if (readline == pcall) {
            VhdlSubprogramId* sId = static_cast<VhdlSubprogramId*>(node.GetId());
            V2N_INFO_ASSERT(sId, node, "failed to get subprogram id");
            if (2 != sId->GetArgs()->Size()) {
                return false;
            }
            VhdlIdDef* item = 0;
            unsigned i = 0;
            FOREACH_ARRAY_ITEM(sId->GetArgs(), i, item) {
                if (0 == i) {
                    if (! item->Type()->IsFileType()) {
                        return false;
                    }
                } else if (1 == i) {
                    if (UtString("line") != UtString(item->BaseType()->Name())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    return false;
  /*
    node.PrettyPrint(std::cerr, 0);
    VhdlIdDef* sId = static_cast<VhdlIdDef*>(node.GetId());
    INFO_ASSERT(sId, "");
    INFO_ASSERT(sId->GetOwningScope(), "");
    INFO_ASSERT(sId->GetOwningScope()->GetContainingPrimaryUnit(), "");
    INFO_ASSERT(sId->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit(), "");
    StringAtom* packAtom = mNucleusBuilder->gCache()->intern(UtString(sId->GetOwningScope()->GetContainingPrimaryUnit()->GetPrimaryUnit()->Name()));
    if (UtString("textio") == UtString(packAtom->str())) {
        return true;
    }
    return false;
    */
}

NUBase* VerificVhdlDesignWalker::checkAndHandleSysCall(const VhdlIndexedName& node)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
  std::cerr << "SysCall" << std::endl;
    node.PrettyPrint(std::cerr, 0);  
#endif
  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  INFO_ASSERT(node.GetPrefix(), "");
  UtString pcall(node.GetPrefix()->Name());
  if (ID_VHDLSELECTEDNAME == node.GetPrefix()->GetClassId()) {
      VhdlSelectedName* selName = static_cast<VhdlSelectedName*>(node.GetPrefix());
      INFO_ASSERT(selName, "");
      VhdlDesignator* designator = selName->GetSuffix();
      INFO_ASSERT(designator, "");
      pcall = UtString(designator->Name());
  }
  if (!mEnableVhdlFileIO) {
    pcall.uppercase();
    mNucleusBuilder->getMessageContext()->DisabledSysTask(&src_loc, pcall.c_str(), CarbonContext::scVhdlEnableFileIO);
    return 0;
  }
  UtString fopen("file_open");
  UtString fclose("file_close");
  UtString read("read");
  UtString hread("hread");
  UtString oread("oread");
  UtString write("write");
  UtString owrite("owrite");
  UtString hwrite("hwrite");
  UtString writeline("writeline");
  UtString readline("readline");
  UtString endfile("endfile");
  if (endfile == pcall) {
    return handleEndFileSysCall(node);
  } else if (fopen == pcall) {
     return handleOpenFileSysCall(node);
  } else if (fclose == pcall) {
     return handleCloseFileSysCall(node);
  } else if (read == pcall ||
             readline == pcall ||
             hread == pcall ||
             oread == pcall) {
    return handleReadFileSysCall(node, pcall);
  } else if ((write == pcall) ||
             (owrite == pcall) ||
             (hwrite == pcall) ||
             (writeline == pcall)) {
    return handleWriteFileSysCall(node, pcall);
  }
  return 0;
}

NUSysFunctionCall*
VerificVhdlDesignWalker::handleEndFileSysCall(const VhdlIndexedName& node)
{
  unsigned i = 0;
  VhdlTreeNode* item = 0;
  NUExprVector vec_expr;
  NUExpr* expr = 0;
  FOREACH_ARRAY_ITEM(node.GetAssocList(), i, item) {
    mCurrentScope->sContext(V2NDesignScope::RVALUE);
    item->Accept(*this);
    NUBase* base = mCurrentScope->gValue();
    INFO_ASSERT(base, "");
    expr = dynamic_cast<NUExpr*>(base);
    INFO_ASSERT(expr, "");
    expr->resize(expr->determineBitSize());
    vec_expr.push_back(expr);
    INFO_ASSERT(0 == i, "");
  }
  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  NUSysFunctionCall* syscall = new NUSysFunctionCall(NUOp::eZEndFile, 0, 0, vec_expr, src_loc);
  INFO_ASSERT(syscall, "");
  return syscall;
}

NUFOpenSysTask*
VerificVhdlDesignWalker::handleOpenFileSysCall(const VhdlIndexedName& node)
{
  V2N_INFO_ASSERT((3 == node.GetAssocList()->Size()) 
                 or (4 == node.GetAssocList()->Size()), node, "invalid number of arguments for file_open");
  /**
   * procedure FILE_OPEN (file anonymous: FT;
   *                      External_Name: in STRING;
   *                      Open_Kind: in FILE_OPEN_KIND := READ_MODE);
   *
   * procedure FILE_OPEN (Status: out FILE_OPEN_STATUS;
   *                      file anonymous: FT;
   *                      External_Name: in STRING;
   *                      Open_Kind: in FILE_OPEN_KIND := READ_MODE);
   *
   */
  UInt32 f = (4 == node.GetAssocList()->Size()) ? 1 : 0;
  NUIdentLvalue* statusLvalue = 0;
  NUExprVector expr_vector;
  NUNet* net=NULL;
  UInt32 mode_i = -1;
  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  unsigned i = 0;
  VhdlTreeNode* item = 0;
  FOREACH_ARRAY_ITEM(node.GetAssocList(), i, item) {
      if (0 + f == i) {
          // size = 4, i = 1
          // size = 3, i = 0.
          // RBS thinks: the static cast is a problem. This could be a selected name, etc.
          VhdlIdRef* idRef = static_cast<VhdlIdRef*>(item);
          INFO_ASSERT(idRef, "");

          NUBase* base = getNucleusObject(idRef->GetId());
          
          INFO_ASSERT(base, "");
          net = dynamic_cast<NUNet*>(base);
          INFO_ASSERT(net, "");
      } else if (1 + f == i) {
          // size = 4, i = 2
          // size = 3, i = 1.
          UtString ext_name = "";
          if (ID_VHDLIDREF == item->GetClassId()) {
              VhdlIdRef* idRef = static_cast<VhdlIdRef*>(item);
              INFO_ASSERT(idRef, "invlaid VhdlIdRef node");
              VhdlValue* value = idRef->Evaluate(0, mDataFlow, 0);
              if (! value) {
                  mNucleusBuilder->getMessageContext()->Verific2NUFNArgDidNotResolved(&src_loc);
                  setError(true);
                  return 0;
              }
              char* image = value->Image();
              UtString tmp(image);
              ext_name = UtString(tmp, 1, tmp.size() - 2);
              Strings::free(image);
              delete value;
          } else {
              VhdlStringLiteral* sLit = static_cast<VhdlStringLiteral*>(item);
              INFO_ASSERT(sLit, "invalid VhdlStringLiteral node");
              UtString tmp(sLit->Name());
              ext_name = UtString(tmp, 1, tmp.size() - 2);
          }
          UtString binary_str;
          UInt32 bit_width = composeBinStringFromString(ext_name.c_str(), &binary_str, true);
          NUConst* file_name = NUConst::createKnownConst(binary_str, bit_width, true, src_loc);
          file_name->resize(file_name->determineBitSize());
          expr_vector.push_back(file_name);
      } else if (2 + f == i) {
          // size = 4, i = 3
          // size = 3, i = 2.
          VhdlIdRef* idRef = static_cast<VhdlIdRef*>(item);
          INFO_ASSERT(idRef, "");
          VhdlValue* value = idRef->Evaluate(0, mDataFlow, 0);
          UInt32 bit_width;
          UtString mode_str;
          mode_i = value->Integer();
          if (0 == value->Integer()) {
              //read mode
              bit_width = composeBinStringFromString("r", &mode_str, false);
          } else if (1 == value->Integer()) {
              //write mode
              bit_width = composeBinStringFromString("w", &mode_str, false);
          } else {
              INFO_ASSERT(2 == value->Integer(), "");
              //append mode
              bit_width = composeBinStringFromString("a", &mode_str, false);
          }
          delete value;
          NUConst* mode = NUConst::createKnownConst(mode_str, bit_width, true, src_loc);
          mode->resize(mode->determineBitSize());
          expr_vector.push_back(mode);
      } else if ((1 == f) && (0 == i)) {
          // s = 4 and i = 0
          mCurrentScope->sContext(V2NDesignScope::LVALUE);
          item->Accept(*this);
          INFO_ASSERT(mCurrentScope->gValue(), "failed to populate");
          statusLvalue = dynamic_cast<NUIdentLvalue*>(mCurrentScope->gValue());
          INFO_ASSERT(statusLvalue, "invlaid Lvalue object");
      } else {
//          INFO_ASSERT(false, "");
      }
  }
  INFO_ASSERT(0 == mode_i or 1 == mode_i or 2 == mode_i, "");
  NUModule* module = getModule();
  NUCLEUS_CONSISTENCY(module, src_loc, "Unable to get module");
  StringAtom* file_open_sys = module->gensym("file_open");
  NUFOpenSysTask* systask = new NUFOpenSysTask(file_open_sys, net, statusLvalue,  expr_vector, module, 
                                mNucleusBuilder->gNetRefFactory(), false, false, (0 == mode_i), src_loc);
  return systask;
}

NUFCloseSysTask* 
VerificVhdlDesignWalker::handleCloseFileSysCall(const VhdlIndexedName& node)
{
  NUExprVector expr_vector;
  unsigned i = 0;
  VhdlTreeNode* item = 0;
  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  FOREACH_ARRAY_ITEM(node.GetAssocList(), i, item) {
    // RBS thinks: the static cast is a problem. This could be a selected name, etc.
    VhdlIdRef* idRef = static_cast<VhdlIdRef*>(item);
    INFO_ASSERT(idRef, "");

    NUBase* base = getNucleusObject(idRef->GetId());
          
    INFO_ASSERT(base, "");
    NUNet* net = dynamic_cast<NUNet*>(base);
    INFO_ASSERT(net, "");
    NUExpr* rval = mNucleusBuilder->createIdentRvalue(net, src_loc);
    INFO_ASSERT(rval, "");
    rval->resize(rval->determineBitSize());
    expr_vector.push_back(rval);
  }
  NUModule* module = getModule();
  NUCLEUS_CONSISTENCY(module, src_loc, "Unable to get module");
  StringAtom* file_close_sys = module->gensym("file_close");
  NUFCloseSysTask* systask = new NUFCloseSysTask(file_close_sys, expr_vector,
      module, mNucleusBuilder->gNetRefFactory(), false, false, false, src_loc);
  return systask;
}

NUInputSysTask* 
VerificVhdlDesignWalker::handleReadFileSysCall(const VhdlIndexedName& node, const UtString& pcall)
{
  NUExprVector expr_vector;
  NULvalue* lval = 0;
  NULvalue* lval_status = 0;
  UtString readline("readline");
  UtString read("read");
  UtString hread("hread");
  UtString oread("oread");
  unsigned i = 0;
  VhdlTreeNode* item = 0;
  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  FOREACH_ARRAY_ITEM(node.GetAssocList(), i, item) {
    if (0 == i) {
      if (ID_VHDLSELECTEDNAME == item->GetClassId() && readline == pcall) {
        VhdlSelectedName* selName = static_cast<VhdlSelectedName*>(item);
        INFO_ASSERT(selName, "");
        VhdlDesignator* designator = selName->GetSuffix();
        INFO_ASSERT(designator, "");
        UtString suffix(designator->Name());
        INFO_ASSERT(selName->GetPrefix()->GetSuffix(), "");
        UtString prefix(selName->GetPrefix()->GetSuffix()->Name());
        if (UtString("input") == suffix && UtString("textio") == prefix) {
          NUExpr* this_expr = NUConst::create (false, 0, 32, src_loc);
          INFO_ASSERT(this_expr, "");
          expr_vector.push_back(this_expr);
        } else {
          INFO_ASSERT(false, "");
        }
      } else {
        VhdlIdRef* idRef = static_cast<VhdlIdRef*>(item);
        INFO_ASSERT(idRef, "");

        NUBase* base = getNucleusObject(idRef->GetId());
        
        INFO_ASSERT(base, "");
        NUNet* net = dynamic_cast<NUNet*>(base);
        INFO_ASSERT(net, "");
        NUExpr* rval = mNucleusBuilder->createIdentRvalue(net, src_loc);
        INFO_ASSERT(rval, "");
        rval->resize(rval->determineBitSize());
        expr_vector.push_back(rval);
      }
    } else if (1 == i) {
      if (readline == pcall) {
        mCurrentScope->sContext(V2NDesignScope::RVALUE);
        VhdlIdRef* idRef = static_cast<VhdlIdRef*>(item);
        INFO_ASSERT(idRef, "");
        idRef->Accept(*this);
        NUBase* base = mCurrentScope->gValue();
        INFO_ASSERT(base, "");
        NUExpr* rval = dynamic_cast<NUExpr*>(base);
        INFO_ASSERT(rval, "");
        rval->resize(rval->determineBitSize());
        expr_vector.push_back(rval);
        mCurrentScope->sContext(V2NDesignScope::UNKNOWN_CONTEXT);
      } else {
        mCurrentScope->sContext(V2NDesignScope::LVALUE);
        VhdlIdRef* idRef = static_cast<VhdlIdRef*>(item);
        INFO_ASSERT(idRef, "");
        idRef->Accept(*this);
        NUBase* base = mCurrentScope->gValue();
        INFO_ASSERT(base, "");
        lval = dynamic_cast<NULvalue*>(base);
        INFO_ASSERT(lval, "");
        mCurrentScope->sContext(V2NDesignScope::UNKNOWN_CONTEXT);
        if (hread == pcall) {
          UtString bin_str;
          UInt32 bit_width = composeBinStringFromString("%h", &bin_str, false);
          NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
          INFO_ASSERT(the_const, "");
          the_const->resize(the_const->determineBitSize());
          expr_vector.push_back(the_const);
        } else if (oread == pcall) {
          UtString bin_str;
          UInt32 bit_width = composeBinStringFromString("%o", &bin_str, false);
          NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
          INFO_ASSERT(the_const, "");
          the_const->resize(the_const->determineBitSize());
          expr_vector.push_back(the_const);
        } else {
          if (idRef->GetId()->Type()->IsArrayType()) {
	    //AF: Ed we need to be able to handle arrays of all types
	    //bit boolean array
            if (
		UtString("bit") == UtString(idRef->GetId()->Type()->ElementType()->Name()) ||
		UtString("boolean") == UtString(idRef->GetId()->Type()->ElementType()->Name())
		) {
              UtString bin_str;
              UInt32 bit_width = composeBinStringFromString("%b", &bin_str, false);
              NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
              INFO_ASSERT(the_const, "invalid NUExpr object");
              the_const->resize(the_const->determineBitSize());
              expr_vector.push_back(the_const);
            } 
	    //character array
	    else if (idRef->GetId()->Type()->ElementType()->IsStdCharacterType()) {
              UtString char_str;
              UInt32 char_width = composeBinStringFromString("%c", &char_str, false);
              NUConst* the_const = NUConst::createKnownConst(char_str, char_width, true, src_loc);
              INFO_ASSERT(the_const, "invalid NUExpr object");
              the_const->resize(the_const->determineBitSize());
              expr_vector.push_back(the_const);
        }
	    else if ( idRef->GetId()->Type()->ElementType()->IsStdULogicType() ) {
              UtString bin_str;
              UInt32 bit_width = composeBinStringFromString("%b", &bin_str, false);
              NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
              INFO_ASSERT(the_const, "invalid NUExpr object");
              the_const->resize(the_const->determineBitSize());
              expr_vector.push_back(the_const);
         }
         else {
	        //AF: Ed please note this could be an array of anything !
	        //we need to fill out residual types here.
                V2N_INFO_ASSERT(false, node, "invalid Type name");
         }
	  }
	  else if (UtString("boolean") == UtString(idRef->GetId()->Type()->Name())) {
              UtString bin_str;
              UInt32 bit_width = composeBinStringFromString("%b", &bin_str, false);
              NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
              INFO_ASSERT(the_const, "invalid NUExpr object");
              the_const->resize(the_const->determineBitSize());
              expr_vector.push_back(the_const);
          } else if (idRef->GetId()->Type()->IsStdCharacterType()) {
              UtString char_str;
              UInt32 char_width = composeBinStringFromString("%c", &char_str, false);
              NUConst* the_const = NUConst::createKnownConst(char_str, char_width, true, src_loc);
              INFO_ASSERT(the_const, "invalid NUExpr object");
              the_const->resize(the_const->determineBitSize());
              expr_vector.push_back(the_const);
          } else {
            UtString bin_str;
            UInt32 bit_width = composeBinStringFromString("%0d", &bin_str, false);
            NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
            INFO_ASSERT(the_const, "");
            the_const->resize(the_const->determineBitSize());
            expr_vector.push_back(the_const);
          }
        }
      }
    } else if (2 == i) {
      mCurrentScope->sContext(V2NDesignScope::LVALUE);
      VhdlIdRef* idRef = static_cast<VhdlIdRef*>(item);
      INFO_ASSERT(idRef, "");
      idRef->Accept(*this);
      NUBase* base = mCurrentScope->gValue();
      INFO_ASSERT(base, "");
      lval_status = dynamic_cast<NULvalue*>(base);
      INFO_ASSERT(lval_status, "");
      mCurrentScope->sContext(V2NDesignScope::UNKNOWN_CONTEXT);
    }
  }
  StringAtom* file_input_sys=NULL;
  NUModule* module = getModule();
  NUCLEUS_CONSISTENCY(module, src_loc, "Unable to get module");
  if (pcall == readline) {
    file_input_sys = module->gensym("readline");
  } else if (pcall == read) {
    file_input_sys = module->gensym("read");
  } else if (pcall == oread) {
      file_input_sys = module->gensym("oread");
  } else if (pcall == hread) {
      file_input_sys = module->gensym("hread");
  }
  bool isReadLineTask = (readline == pcall) ? true : false;
  NUInputSysTask* systask = new NUInputSysTask(file_input_sys, expr_vector, lval, lval_status, module,
      mNucleusBuilder->gNetRefFactory(),
      false, false, isReadLineTask, src_loc);
  INFO_ASSERT(systask, "");
  return systask;
}

/**
 * procedure WRITE (file anonymous: FT; Value: in SomeType);
 */
NUOutputSysTask*
VerificVhdlDesignWalker::handleWriteFileSysCall(const VhdlIndexedName& node, const UtString& pcall)
{
  NUExprVector expr_vector;
  SourceLocator src_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
  UtString write("write");
  UtString writeline("writeline");
  unsigned i = 0;
  VhdlTreeNode* item = 0;
  FOREACH_ARRAY_ITEM(node.GetAssocList(), i, item) {
      if (0 == i) {
          if (ID_VHDLIDREF == item->GetClassId()) {
              VhdlIdRef* idRef = static_cast<VhdlIdRef*>(item);
              INFO_ASSERT(idRef, "invlaid VhdlIdRef node");

              NUBase* base = getNucleusObject(idRef->GetId());
        
              if (! base && (UtString("output") == UtString(idRef->Name()))) {
                  NUExpr* the_const = NUConst::createKnownIntConst(1, 32, false, src_loc);
                  INFO_ASSERT(the_const, "invlaid constant object");
                  expr_vector.push_back(the_const);
              } else {
                  INFO_ASSERT(base, "invalid populated object");
                  NUNet* net = dynamic_cast<NUNet*>(base);
                  INFO_ASSERT(net, "");
                  NUExpr* rval = mNucleusBuilder->createIdentRvalue(net, src_loc);
                  INFO_ASSERT(rval, "");
                  rval->resize(rval->determineBitSize());
                  expr_vector.push_back(rval);
              }
          } else if (ID_VHDLSELECTEDNAME == item->GetClassId()) {
              VhdlSelectedName* selName = static_cast<VhdlSelectedName*>(item);
              INFO_ASSERT(selName, "invalid VhdlSelectedName node");
              if (UtString("output") == UtString(selName->GetSuffix()->Name())) {
                  NUExpr* the_const = NUConst::createKnownIntConst(1, 32, false, src_loc);
                  INFO_ASSERT(the_const, "invlaid constant object");
                  expr_vector.push_back(the_const);
              } else {
                  INFO_ASSERT(false, "FIXME ASAP");
              }
          } else {
              INFO_ASSERT(false, "FIXME ASAP");
          }
      } else if (1 == i) {
          mCurrentScope->sContext(V2NDesignScope::RVALUE);
          if (ID_VHDLSTRINGLITERAL == item->GetClassId()) {
              UtString bin_str;
              UInt32 bit_width = composeBinStringFromString("%s", &bin_str, false);
              NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
              INFO_ASSERT(the_const, "");
              the_const->resize(the_const->determineBitSize());
              expr_vector.push_back(the_const);
              
              VhdlStringLiteral* strLit = static_cast<VhdlStringLiteral*>(item);
              INFO_ASSERT(strLit, "");
              UtString str(strLit->Name());
              str = str.substr(1, str.size() - 2);
              UInt32 s = str.size();
              NUExprVector vec_exp;
              for (UInt32 i = 0; i < s; ++i) {
                  UtString tmp = str.substr(i, 1);
                  UtString bin_str;
                  UInt32 bit_width = composeBinStringFromString(tmp.c_str(), &bin_str, false);
                  NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
                  INFO_ASSERT(the_const, "invalid constant object");
                  the_const->resize(the_const->determineBitSize());
                  vec_exp.push_back(the_const);
              }
              NUConcatOp* conc = new NUConcatOp(vec_exp, 1, src_loc);
              INFO_ASSERT(conc, "invalid concatenation object");
              expr_vector.push_back(conc);
          } else {
              {
                  if (ID_VHDLIDREF == item->GetClassId() && (writeline != pcall)) {
                      VhdlIdRef* idRef = static_cast<VhdlIdRef*>(item);
                      INFO_ASSERT(idRef, "");
                      //TODO Need to be refactored
                      if (idRef->GetId()->Type()->IsIntegerType() || idRef->IsConstant()) {
                          UtString bin_str;
                          UInt32 bit_width = composeBinStringFromString("%0d", &bin_str, false);
                          NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
                          INFO_ASSERT(the_const, "");
                          the_const->resize(the_const->determineBitSize());
                          expr_vector.push_back(the_const);
                      } else if (UtString("bit_vector") == UtString(idRef->GetId()->Type()->Name())) {
                          UtString bin_str;
                          UInt32 bit_width = composeBinStringFromString("%b", &bin_str, false);
                          NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
                          INFO_ASSERT(the_const, "");
                          the_const->resize(the_const->determineBitSize());
                          expr_vector.push_back(the_const);
                      } else if (UtString("string") == UtString(idRef->GetId()->Type()->Name())) {
                          UtString bin_str;
                          UInt32 bit_width = composeBinStringFromString("%s", &bin_str, false);
                          NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
                          INFO_ASSERT(the_const, "");
                          the_const->resize(the_const->determineBitSize());
                          expr_vector.push_back(the_const);
                      } else if (UtString("character") == UtString(idRef->GetId()->Type()->Name())) {
                          UtString bin_str;
                          UInt32 bit_width = composeBinStringFromString("%c", &bin_str, false);
                          NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
                          INFO_ASSERT(the_const, "");
                          the_const->resize(the_const->determineBitSize());
                          expr_vector.push_back(the_const);
                      } else if (UtString("bit") == UtString(idRef->GetId()->Type()->Name()) || 
                              UtString("boolean") == UtString(idRef->GetId()->Type()->Name())) {
                          UtString bin_str;
                          UInt32 bit_width = composeBinStringFromString("%b", &bin_str, false);
                          NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
                          INFO_ASSERT(the_const, "");
                          the_const->resize(the_const->determineBitSize());
                          expr_vector.push_back(the_const);
                      } else if (idRef->GetId()->Type()->IsArrayType()) {
                          if ((UtString("bit") == UtString(idRef->GetId()->Type()->ElementType()->Name())) ||
                                  (UtString("line") == UtString(idRef->GetId()->Type()->Name()))) {
                              UtString bin_str;
                              UInt32 bit_width = composeBinStringFromString("%b", &bin_str, false);
                              NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
                              INFO_ASSERT(the_const, "");
                              the_const->resize(the_const->determineBitSize());
                              expr_vector.push_back(the_const);
                          } else if (ID_VHDLINTEGER == item->GetClassId()) {
                              UtString bin_str;
                              UInt32 bit_width = composeBinStringFromString("%0d", &bin_str, false);
                              NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
                              INFO_ASSERT(the_const, "");
                              the_const->resize(the_const->determineBitSize());
                              expr_vector.push_back(the_const);
                          }
                      }
                  } else if (ID_VHDLINTEGER == item->GetClassId() && (write == pcall)) {
                      UtString bin_str;
                      UInt32 bit_width = composeBinStringFromString("%0d", &bin_str, false);
                      NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
                      INFO_ASSERT(the_const, "");
                      the_const->resize(the_const->determineBitSize());
                      expr_vector.push_back(the_const);
                  }
              }
              item->Accept(*this);
              NUBase* base = mCurrentScope->gValue();
              INFO_ASSERT(base, "failed to populate");
              if (NUExpr* expr = dynamic_cast<NUExpr*>(base)) {
                  if (UtString("hwrite") == pcall) {
                      UtString bin_str;
                      UInt32 bit_width = composeBinStringFromString("%h", &bin_str, false);
                      NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
                      INFO_ASSERT(the_const, "");
                      the_const->resize(the_const->determineBitSize());
                      expr_vector.push_back(the_const);
                  } else if (UtString("owrite") == pcall) {
                      UtString bin_str;
                      UInt32 bit_width = composeBinStringFromString("%o", &bin_str, false);
                      NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
                      INFO_ASSERT(the_const, "");
                      the_const->resize(the_const->determineBitSize());
                      expr_vector.push_back(the_const);
                  }
                  expr->resize(expr->determineBitSize());
                  expr_vector.push_back(expr);
              } else {
                  INFO_ASSERT(false, "FIXME ASAP");
              }
          }
      }
  }
  //UtString bin_str;
  //UInt32 bit_width = composeBinStringFromString("%0d", &bin_str, false);
  //NUConst* the_const = NUConst::createKnownConst(bin_str, bit_width, true, src_loc);
  //INFO_ASSERT(the_const, "");
  //the_const->resize(the_const->determineBitSize());
  //expr_vector.push_back(the_const);
  NUModule* module = getModule();
  NUCLEUS_CONSISTENCY(module, src_loc, "Unable to get module"); 
  StringAtom* file_output_sys = module->gensym("write");
  bool isWriteLineTask = (writeline == pcall) ? true : false;
  NUOutputSysTask* systask = new NUOutputSysTask(file_output_sys, expr_vector, module,
                                                 true, false,
                                                 module->getTimeUnit(),
                                                 module->getTimePrecision(),
                                                 mNucleusBuilder->gNetRefFactory(),
                                                 false, false,
                                                 isWriteLineTask, false, NULL, src_loc);
  INFO_ASSERT(systask, "invalid NUOutputSysTask object");
  return systask;
}

void VerificVhdlDesignWalker::createTaskEnable(VhdlSubprogramId& node, NUTask* task, UtVector<NUBase*>& nuObjects, Array* arguments, const SourceLocator& loc)
{
    if (node.IsProcedure()) {
        createProcedureTaskEnable(node, task, nuObjects, arguments, loc);
    } else if (node.IsFunction()) {
        createFunctionTaskEnable(node, task, nuObjects, arguments, loc);
    } else {
      V2N_INFO_ASSERT(false, node, "Invalid subprogram id (not a procedure or task");
    }
}

void VerificVhdlDesignWalker::createProcedureTaskEnable(VhdlSubprogramId& node, NUTask* task, UtVector<NUBase*>& nuObjects, Array* arguments, const SourceLocator& loc)
{
    NUModule* module = getModule();
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, node, "Unable to get module");
    //create taskenable
    NUTaskEnable* enable = mNucleusBuilder->createTaskEnable(task, module, loc);
    //specific parameters for proc call
    //connect inputs
    for (UInt32 i = 0; i < nuObjects.size(); ++i) {
        NUNet* formal = task->getArg(i);
        INFO_ASSERT(formal, "invalid formal net");
        NUBase* base = nuObjects[i];
        INFO_ASSERT(base, "invalid base object");
        if (NUNet* net = dynamic_cast<NUNet*>(base)) {
            INFO_ASSERT(net, "Invalid NUNet object");
            if (formal->isInput()) {
                NUExpr* actual = mNucleusBuilder->createIdentRvalue(net, loc);
                NUTFArgConnection* connection_in = new NUTFArgConnectionInput(actual, formal, task, loc);
                enable->addArgConnection(connection_in);
            } else if (formal->isOutput()) {
                NULvalue* actual = mNucleusBuilder->createIdentLvalue(net, loc);
                NUTFArgConnection* connection_out = new NUTFArgConnectionOutput(actual, formal, task, loc);
                enable->addArgConnection(connection_out);
            } else {
                INFO_ASSERT(formal->isBid(), "");
                NULvalue* actual = mNucleusBuilder->createIdentLvalue(net, loc);
                NUTFArgConnection* connection_in = new NUTFArgConnectionBid(actual, formal, task, loc);
                enable->addArgConnection(connection_in);
            }
        } else if (NULvalue* lval = dynamic_cast<NULvalue*>(base)) {
            if (formal->isOutput()) {
                NUTFArgConnection* connection_out = new NUTFArgConnectionOutput(lval, formal, task, loc);
                enable->addArgConnection(connection_out);
            } else if (formal->isInput()) {
                //net is output , but formal port is input
                //so we have to delete beforly created lvalue object and recreate
                //correct one (rvalue object)
                delete lval;
                V2NDesignScope::ContextType st = mCurrentScope->gContext();
                VhdlExpression* vhExp = static_cast<VhdlExpression*>(arguments->At(i));
                V2N_INFO_ASSERT(vhExp, node, "invalid assoc element");
                mCurrentScope->sContext(V2NDesignScope::RVALUE);
                vhExp -> Accept(*this);
                V2N_INFO_ASSERT(mCurrentScope->gValue(), node, "failed to populate actual element");
                base = (mCurrentScope->gValue());
                V2N_INFO_ASSERT(dynamic_cast<NUExpr*>(mCurrentScope->gValue()), node, "failed to cast NUExpr");
                NUExpr* actual = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
                NUTFArgConnection* connection_in = new NUTFArgConnectionInput(actual, formal, task, loc);
                INFO_ASSERT(connection_in, "invalid NUTFArgConnection object");
                enable->addArgConnection(connection_in);
                mCurrentScope->sContext(st);
            } else {
                INFO_ASSERT(formal->isBid(), "should be bi direction");
                NUTFArgConnection* connection_out = new NUTFArgConnectionBid(lval, formal, task, loc);
                INFO_ASSERT(connection_out, "invalid NUTFArgConnection object");
                enable->addArgConnection(connection_out);
            }
        } else if (NUVarselRvalue* actual = dynamic_cast<NUVarselRvalue*>(base)) {
            if (formal->isInput()) {
                NUTFArgConnection* connection_in = new NUTFArgConnectionInput(actual, formal, task, loc);
                INFO_ASSERT(connection_in, "invalid NUTFArgConnection object");
                enable->addArgConnection(connection_in);
            } else {
                INFO_ASSERT(false, "");
            }
        } else if (NUConst* actual = dynamic_cast<NUConst*>(base)) {
            if (formal->isInput()) {
                NUTFArgConnection* connection_in = new NUTFArgConnectionInput(actual, formal, task, loc);
                INFO_ASSERT(connection_in, "invalid NUTFArgConnection object");
                enable->addArgConnection(connection_in);
            } else if (formal->isOutput()) {
                //misc/SUB27
                // RBS thinks: the static cast is a problem. This could be a selected name, etc.
                VhdlIdRef* idRef = static_cast<VhdlIdRef*>(arguments->At(i));
                INFO_ASSERT(idRef, "invalid VhdlIdRef node");
                delete base;

                NUBase* base = getNucleusObject(idRef->GetId());

                INFO_ASSERT(base, "invalid NUBase object");
                NUNet* net = dynamic_cast<NUNet*>(base);
                INFO_ASSERT(net, "invalid NUNet");
                NULvalue* lval = mNucleusBuilder->createIdentLvalue(net, loc);
                NUTFArgConnection* connection_out = new NUTFArgConnectionOutput(lval, formal, task, loc);
                enable->addArgConnection(connection_out);
            } else {
                INFO_ASSERT(false, "");
            }
        } else if (NUMemselRvalue* actual = dynamic_cast<NUMemselRvalue*>(base)) {
            NUTFArgConnection* connection_in = new NUTFArgConnectionInput(actual, formal, task, loc);
            INFO_ASSERT(connection_in, "invalid NUTFArgConnection object");
            enable->addArgConnection(connection_in);
        } else if (NUExpr* actual = dynamic_cast<NUExpr*>(base)) {
            NUTFArgConnection* connection_in = new NUTFArgConnectionInput(actual, formal, task, loc);
            INFO_ASSERT(connection_in, "invalid NUTFArgConnection object");
            enable->addArgConnection(connection_in);
        } else {
            INFO_ASSERT(false, "");
        }
    }

    for (SInt32 j = nuObjects.size(); j <= task->getNumArgs() - 1; ++j) {
        NUNet* formal = task->getArg(j);
        V2N_INFO_ASSERT(formal, node, "invalid NUNet object");
        NUExpr* actual_default = getDefaultValue(j, node);
        INFO_ASSERT(actual_default, "invalid NUExpr object");
        NUTFArgConnection* connection_in = new NUTFArgConnectionInput(actual_default, formal, task, loc);
        INFO_ASSERT(connection_in, "invalid NUTFArgConnection object");
        enable->addArgConnection(connection_in);
    }
    if (getAlwaysBlock()) {
        // We're in an always block; just return the task enable for the procedure
        mCurrentScope->sValue(enable);
    } else {
        // We in a concurrent statement, a block statement, or the body of a task.
        // If we're in a concurrent statement, create a containing always block. Otherwise, 
        // a containing block will already exist. Add procedure task enable to the containing
        // block.
        cAlwaysBlockAndAddStatement(enable, loc);
        mCurrentScope->sValue(NULL);
    }
}

// Give the (0 based) position of a formal subprogram parameter, find the initial value
// assignment (if any) for that parameter.
VhdlExpression* VerificVhdlDesignWalker::getInitialAssign(UInt32 idx, VhdlSubprogramId* sId)
{
  VhdlExpression* initAssign = NULL;
  
  VhdlSubprogramBody* sBody = sId->Body();
  INFO_ASSERT(sBody, "failed to get body of subprogram");
  VhdlSpecification* spec = sBody->GetSubprogramSpec();
  INFO_ASSERT(spec, "Invalid VhdlFunctionSpec node");
  unsigned l = 0;
  unsigned pos = 0;
  VhdlInterfaceDecl* interfaceDecl = 0;
  FOREACH_ARRAY_ITEM(spec->GetFormalParamList(), l, interfaceDecl) {
    if (pos > idx) break; // We're done
    // Each interface declaration may consist of multiple ids, all with
    // the same type, and same initial value. Count through the
    // ids for this interface declaration.
    unsigned m = 0;
    VhdlIdDef* id = 0;
    FOREACH_ARRAY_ITEM(interfaceDecl->GetIds(), m, id) {
      if (pos == idx) {
        initAssign = interfaceDecl->GetInitAssign();
      }
      pos++;
    }
  }
  
  return initAssign;
}

// Given the position (0 based) of a formal parameter in a function/subprogram parameter list,
// find the initial value of that parameter, and populate nucleus datastructures.
NUExpr* VerificVhdlDesignWalker::getDefaultValue(UInt32 idx, VhdlSubprogramId& sId)
{
  VhdlExpression* initAssign = getInitialAssign(idx, &sId);
  INFO_ASSERT(initAssign, "default initial values should exists");
  initAssign->Accept(*this);
  INFO_ASSERT(mCurrentScope->gValue(), "failed to populate task default argument");
  NUBase* base = mCurrentScope->gValue();
  INFO_ASSERT(base, "failed to populate task default argument");
  NUExpr* expr = dynamic_cast<NUExpr*>(base);
  INFO_ASSERT(expr, "invalid nucleus expression");
  return expr;
}


// creates a task enable statement for \a task, numObjects and arguments are the NU and verific arrays of the actual arguments to the
// function call, (neither includes the implicit net used for the return value of this function).
// TODO investigate why we even need the argument named arguments, it looks like it is only needed to repair an error in the port declaration that should not have been made in the first place
void VerificVhdlDesignWalker::createFunctionTaskEnable(VhdlSubprogramId& node, NUTask* task, UtVector<NUBase*>& nuObjects, Array* arguments, const SourceLocator& loc)
{
    NUModule* module = getModule();
    VERIFIC_CONSISTENCY_NO_RETURN_VALUE(module, node, "Unable to get module");

    VERIFIC_CONSISTENCY_NO_RETURN_VALUE( (nuObjects.size() == arguments->Size() ), node, "non-matching size arrays of NU and Verific objects passed to createFunctionTaskEnable");

    NUTaskEnable* enable = mNucleusBuilder->createTaskEnable(task, module, loc);

    UInt32 numExplicitArgs = nuObjects.size();

    //get return net FORMAL from task's argument list, it is always the first argument
    NUNet* formal_ret_net = task->getArg(0);
    if ( ! formal_ret_net || !formal_ret_net->isOutput() ) {
      UtString msg("Unable to locate the correct return net for the function: ");
      msg << task->getName();
      mNucleusBuilder->getMessageContext()->Verific2NUErrorDuringPopulation(&loc, msg.c_str());
      setError(true);
      return;
    }
      
    // Handle return value of the function:
    //  * create temporary net to hold the return value
    //  * put it into the port connection list
    //
    // This mimics what the Interra population does, which includes declaring the net at the module
    // scope.  The Interra code sights bug 4288 as reason for doing that, which is still open.
    UtString name_prefix("return_tmp_");
    name_prefix += UtString(node.Name());
    NUNet *actual_ret_net = module->createTempNetFromImage(formal_ret_net,  name_prefix.c_str(), true, false);
    INFO_ASSERT(actual_ret_net, "invalid NUNet object for function return value");
    actual_ret_net->putIsBlockLocal(false);
    NULvalue* ret_lval = mNucleusBuilder->createIdentLvalue(actual_ret_net, loc);
    NUTFArgConnection* connection_out = new NUTFArgConnectionOutput(ret_lval, formal_ret_net, task, loc);
    enable->addArgConnection(connection_out);
    if (actual_ret_net->isCompositeNet()) {
        //save composite net for future look-up
        //composite name is different from declaration node
        //and in lookUpNet method can't find right net by name
        //which is used in component instantiation
        mCompositeNet.insert(UtPair<NUNet*,StringAtom*>(actual_ret_net->getCompositeNet(), actual_ret_net->getName()));
    }

    // connect the inputs
    for (UInt32 i = 0; i < numExplicitArgs; ++i) {
        NUNet* formal = task->getArg(i+1); // first arg is the implicit output, so +1 here
        NUBase* base = nuObjects[i];
        NUExpr* actual = dynamic_cast<NUExpr*>(base);
        if (actual) {
        } else if (NUNet* net = dynamic_cast<NUNet*>(base)) {
            INFO_ASSERT(net, "Invalid NUNet object");
            actual = mNucleusBuilder->createIdentRvalue(net, loc);
        } else if (NULvalue* lval = dynamic_cast<NULvalue*>(base)) {
          // cloutier thinks: the following does not make sense, why do we know the net is an output?
            //net is output , but formal port is input
            //so we have to delete beforly created lvalue object and recreate
            //correct one (rvalue object)
            delete lval;
            V2NDesignScope::ContextType st = mCurrentScope->gContext();
            VhdlExpression* vhExp = static_cast<VhdlExpression*>(arguments->At(i));
            V2N_INFO_ASSERT(vhExp, node, "invalid assoc element");
            mCurrentScope->sContext(V2NDesignScope::RVALUE);
            vhExp -> Accept(*this);
            V2N_INFO_ASSERT(mCurrentScope->gValue(), node, "failed to populate actual element");
            base = (mCurrentScope->gValue());
            V2N_INFO_ASSERT(dynamic_cast<NUExpr*>(mCurrentScope->gValue()), node, "failed to cast NUExpr");
            actual = dynamic_cast<NUExpr*>(mCurrentScope->gValue());
            mCurrentScope->sContext(st);
        }
        INFO_ASSERT(actual, "invalid NUExpr object");
        NUTFArgConnection* connection_in = new NUTFArgConnectionInput(actual, formal, task, loc);
        INFO_ASSERT(connection_in, "invalid NUTFArgConnection object");
        enable->addArgConnection(connection_in);
    }

    //check for any remaining unspecified arguments (which have default initialization)
    //eg.function func1(inp1: integer; inp2: integer := 15) return integer is ....

    // we know that there are some if the numExplicitArgs is less than the number of formal args on the task (minus the one extra one used
    //for the implicit return value
    SInt32 numFormalInputs = task->getNumArgs()-1; // subtract one to ignore the implicit return value from the function.
    for (SInt32 j = numExplicitArgs; j < numFormalInputs; ++j) {
        NUNet* formal = task->getArg(j+1); // first arg is the implicit output, so +1 here
        V2N_INFO_ASSERT(formal, node, "invalid NUNet object");
        NUExpr* actual_default = getDefaultValue(j, node);
        INFO_ASSERT(actual_default, "invalid NUExpr object");
        NUTFArgConnection* connection_in = new NUTFArgConnectionInput(actual_default, formal, task, loc);
        enable->addArgConnection(connection_in);
    }

    NUTask* calling_task = getTask();
    NUBlock* containing_block = getBlock();
    if ( NULL == containing_block && NULL != calling_task ){
      // Currently within the declaration section of a task (the calling task).
      // The task enable is for a function call (e.g. child_func below) in 
      // an initializer of a declaration inside the calling task.
      // 
      // function calling_task(...) return ... is
      //   variable foo : integer := child_func(...);
      // begin
      //   ...
      // end;
      //
      // We haven't yet processed statements within the calling task. Append the task enable
      // to the statements in the top level block for the calling task.
      NUBlock* task_block = dynamic_cast<NUBlock*>(calling_task->getStmt()); //This should be the only block within the task (though it may have nested blocks)
      V2N_INFO_ASSERT(task_block, node, "unable to find statement block for a task/function");
      task_block->addStmt(enable);
    } else {
      // currently within the statement section of a task, or within module; 
      // put the enable in its own block in the current module, or the body of a task
      cAlwaysBlockAndAddStatement(enable, loc);     //add statement
    }

    // the following should be moved up to the section handling the implicit output, but there is some questionable code between here and
    // there that sets mCurrentScope->sValue() that might interfere.  consider moving after that questionable code is removed
    NUExpr* rval = mNucleusBuilder->createIdentRvalue(actual_ret_net, loc);
    mCurrentScope->sValue(rval);
}

// Populate nucleus for a subprogram/function call, given the arguments.
// The 'arguments' array contains expressions for each argument 'actual'.
//
// This main purpose of this method is to 'push' constraints on the
// actuals down onto the 'formals' so they are available during the
// population of the subprogram body, then to invoke VerificVhdlDesignWalker
// on the subprogram body to populate nucleus. When population is complete,
// the constraints that were 'pushed' onto the formals are poppped, thereby
// restoring constraints that might have existing in a parent call frame.

VhdlValue*
VerificVhdlDesignWalker::elaborateSubprogramBody(VhdlSubprogramBody* sBody, Array* arguments,const SourceLocator& call_loc)
{
    VhdlSpecification* subprogram_spec = sBody->GetSubprogramSpec();
    
    if (!subprogram_spec) return 0 ;
    VhdlIdDef *subprog = subprogram_spec->GetId() ;
    if (!subprog) return 0 ;

    if (0) // Debug
      UtIO::cout() << "DEBUG: >>> enter elaborateSubprogramBody(" << subprog->Name() << ")" << UtIO::endl;
    
    /********************** Arguments nucleus population *******************/

    // Populate task arguments,
    // by looping through each 'actual' argument of the function call, and
    // invoking the 'walker' on that argument.
    UtVector<NUBase*> nuObjects;
    VhdlDiscreteRange* tNode = 0;
    unsigned l = 0;
    bool revert = false; 
    // rjc thinks: can you really use the actual list to get the formals? what about open connections?
    // RBS thinks: If you don't specify all actuals in a function call, you get a parse error. So
    // it doesn't look like you can have open connections for function calls. 
    FOREACH_ARRAY_ITEM(arguments, l, tNode) {
        // RBS I think: we don't need both ct and CType
        V2NDesignScope::ContextType ct = mCurrentScope->gContext();
        V2NDesignScope::ContextType cType = mCurrentScope->gContext();
        // RBS I think: Procedures can have input and output arguments.
        // Adjust context accordingly.
        if (subprog->IsProcedure()) {
            VhdlIdDef* id = tNode->FindFullFormal() ;
            if (!id) {
                id = subprog->Arg(l);
            }
            INFO_ASSERT(id, "invalid id for formal");
            if (id->IsInput()) {
                revert = true;
                mCurrentScope->sContext(V2NDesignScope::RVALUE);
            } else if (id->IsOutput()) {
                revert = true;
                mCurrentScope->sContext(V2NDesignScope::LVALUE);
            } else if (id->IsInout()) {
                revert = true;
                mCurrentScope->sContext(V2NDesignScope::LVALUE);
            }
        }
        tNode->Accept(*this);
        mCurrentScope->sContext(cType);
        NUBase* base = mCurrentScope->gValue();
        INFO_ASSERT(base, "failed to populate task argument");
        nuObjects.push_back(base);
        if (revert) {
            revert = false;
            mCurrentScope->sContext(ct);
        }
    }

    // RBS: The following code is similar to VhdlSubprogramBody::ElaborateSubprogram
    // in the Verific file VhdlDeclaration_Elab.cpp. 
    // The differences are:
    //
    // - Generics are not 'associated'
    // - Subprograms are not 'run' in initial mode
    // - The code to support 'partial association' has been removed 
    // - The code to check for 'open' associations has been removed
    // - Some code has been added to evaluate constraints on expressions
    // - We are invoking VerificVhdlDesignWalker on the subprogram body to populate nucleus, 
    //   whereas the Verific ElaborateSubprogram calls the Verific statement elaborator. 
    /************************************************************************/

//    if (subprog->IsProcessing()) return 0 ;
    // RBS: We don't appear to reset this. It causes constant function evaluation
    // in calls to EvaluateConstraintInternal to fail.
    // subprog->SetIsProcessing(1) ; // Mark as processing to catch recursion
    Map formal_to_constraint(POINTER_HASH) ;
    Map formal_to_value(POINTER_HASH) ;
    Map formal_to_actual(POINTER_HASH) ;
    // Populate 'formal_to_constraint' with formal port constraints
    // Also populates function return constraint.
    subprogram_spec->InitializeConstraints(formal_to_constraint) ;

    VhdlDataFlow *subprog_df = new VhdlDataFlow(mDataFlow, 0, subprogram_spec->LocalScope(), 0) ;
    INFO_ASSERT(subprog_df, "Invalid vhdl data flow object") ;
    subprog_df->SetInSubprogramOrProcess() ;

    VhdlDiscreteRange *assoc ;
    VhdlValue *val ;
    VhdlIdDef *id ;
    VhdlConstraint *constraint ;
    unsigned i ;
    unsigned is_non_const_args = 0 ; 

    FOREACH_ARRAY_ITEM(arguments, i, assoc) {
        if (!assoc) continue ;
        // Get id of formal
        if (assoc->IsAssoc() ) {
            id = assoc->FindFullFormal() ;
            if (!id) {
                break ;
            }
        } else {
            id = subprog->Arg(i) ;
        }
        if (!id) continue ;

        // Retrieve constraint of formal port
        constraint = (VhdlConstraint*)formal_to_constraint.GetValue(id) ;
        // Update formal constraint, given 'actual', and return VhdlValue for constant actuals.
        // NOTE: If 'actual' is a constant, constraint is not updated.
        val = assoc->Evaluate(constraint, mDataFlow, 0) ;
        if (!val) {
            subprog_df->SetNonconstExpr() ;
            is_non_const_args = 1 ;
            if (constraint && !constraint->IsUnconstrained()) {
                VhdlConstraint *actual_constraint = assoc->EvaluateConstraintInternal(mDataFlow, 0) ;
                if (actual_constraint && !actual_constraint->IsUnconstrained()) {
                    // RBS I think: This just checks that the constraint on the 'actual' argument
                    // is compatible with the formal. It emits an error if not. TODO: Why
                    // are we doing this check. Isn't it already done at parse/elaboration time?
                    (void) constraint->CheckAgainst(actual_constraint, assoc, mDataFlow) ;
                }
                delete actual_constraint ;
            } else if (constraint && constraint->IsUnconstrained()) {
                // Carbon Static Elaboration, and in particular the constraint elaborator and
                // subprogram uniquifier guarantee that all subprogram arguments are constrained.
                INFO_ASSERT(false, "Detected an unconstrained formal on a subprogram call");
            }
        }

        // RBS: The Verific copy of this code states that this is a check for LRM Section 2.1.1.2
        if (id->IsSignal() && constraint && !constraint->IsUnconstrained()) {
            VhdlConstraint *actual_constraint = assoc->EvaluateConstraintInternal(mDataFlow, 0) ;
            if (actual_constraint && !actual_constraint->IsUnconstrained()) {
                if (constraint->IsRangeConstraint() && actual_constraint->IsRangeConstraint()) {
                    VhdlValue *actual_low = actual_constraint->Low() ;
                    VhdlValue *actual_high = actual_constraint->High() ;
                    if ((constraint->Dir() != actual_constraint->Dir()) ||
                        !constraint->Contains(actual_low) ||
                        !constraint->Contains(actual_high)) {
                        INFO_ASSERT(false, "");
                    }
                }
            }
            delete actual_constraint ;
        }
        if (!val) continue ;

        // RBS: By the time we get here, we know we have a constant value
        // for the actual argument.

        // Update constraint of formal given 'actual' constant value.
        if (constraint && constraint->IsUnconstrained()) {
            constraint->ConstraintBy(val) ;
        }

        if (id->IsOutput()) {
            if (id->IsVariable() && constraint) {
                delete val ;
                val = constraint->CreateInitialValue(id) ;
            }
        } else {
            // RBS I think: this check is not needed. Wouldn't this
            // be caught at parse/elaboration time by Verific?
            if (!val->CheckAgainst(constraint,assoc, mDataFlow)) {
                delete val ;
                continue ;
            }
        }

        (void) formal_to_value.Insert(id, val) ;

        if (id->IsSignal() && !id->IsInput()) {
            VhdlDiscreteRange *ref_target = id->GetRefTarget() ;
            (void) formal_to_actual.Insert(id, ref_target) ;

            if (!id->GetRefTarget()) {
                id->SetRefTarget(assoc) ;
                id->SetRefFormal(1) ;
            } else {
                VhdlDiscreteRange *actual = assoc->ActualPart() ;
                VhdlIdDef *full_actual_id = actual ? actual->FindFullFormal() : 0 ;
                VhdlDiscreteRange *ref_actual = full_actual_id ? (VhdlDiscreteRange*)formal_to_actual.GetValue(full_actual_id) : 0 ;
                if (!ref_actual && full_actual_id) ref_actual = full_actual_id->GetRefTarget() ;

                if (ref_actual) {
                    id->SetRefTarget(ref_actual) ;
                    id->SetRefFormal(1) ;
                } else {
                    val = assoc->Evaluate(constraint, mDataFlow, 1) ;
                    if (!val) continue ;
                    subprog_df->SetAssignValue(id, val) ;
                    id->SetRefFormal(0) ;
                }
            }
        }
    }

    VhdlScope *local_scope = subprogram_spec->LocalScope() ;
    // RBS thinks: this, apparently, is pushing a stack of ids, constraints on ids, or something similar. After this call,
    // function/subprogram id constraints are NULL.
    if (local_scope && !local_scope->PushStack()) {
        MapIter mi ;
        FOREACH_MAP_ITEM(&formal_to_constraint, mi, 0, &constraint) delete constraint ;
        FOREACH_MAP_ITEM(&formal_to_value, mi, 0, &val) delete val ;
        
        delete subprog_df ;
        subprog_df = 0;
        return 0 ;
    }

    // Set the constraint on each formal, and on the function return value (if this is a function)
    // NOTE: id->SetConstraint deletes the previous constraint, replacing it with the new.
    MapIter mi ;
    FOREACH_MAP_ITEM(&formal_to_constraint, mi, &id, &constraint) {
        id->SetConstraint(constraint) ;
    }
    // Set the constant value (if any) on each formal.
    FOREACH_MAP_ITEM(&formal_to_value, mi, &id, &val) {
        id->SetValue(val) ;
    }
    // Elaborate declaration - sets values and constraints on all declarations
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(sBody->GetDeclPart(), i, decl) {
        decl->Elaborate(subprog_df) ;
    }

    VhdlDataFlow* oldDF = mDataFlow;
    mDataFlow = subprog_df;

    /********************** Nucleus population for task body *******************/
    NUTask* task = NULL;
    NUModule* module = getModule();
    for (UtMultiMap<VhdlIdDef*,NUTask*>::iterator p = mVerificDesignatorToNUTaskMap.lower_bound(subprog); p != mVerificDesignatorToNUTaskMap.upper_bound(subprog); ++p){
      NUTask* temp_task = p->second;
      if ( temp_task->getModule() == module){
        task = temp_task;       // we have already populated this task in this module, we can reuse it
        break;
      }
    }
    if ( NULL == task) {
      // not seen yet, populate the body of the subprog/function now
      subprog->Accept(*this);
      if (hasError()) {
        return 0;
      }
      INFO_ASSERT(mCurrentScope->gValue(), "failed to populate subprogram");
      //get populated subprogram's task
      task = dynamic_cast<NUTask*>(mCurrentScope->gValue());
      VERIFIC_CONSISTENCY(task, *subprog, "Unable to get task");
      UtString subprog_name(subprog->Name());
      if ( Verific2NucleusUtilities::isUniquifiedName(subprog_name)) {
        // only save the tasks in the taskto NU object map if the task was uniquified.
        mVerificDesignatorToNUTaskMap.insert(UtMultiMap<VhdlIdDef*,NUTask*>::value_type(subprog,task));
      }
    }
    /************************************************************/

    /********************* Nucleus population for function call ****************/
    createTaskEnable(*(static_cast<VhdlSubprogramId*>(subprog)), task, nuObjects, arguments, call_loc);
    /************************************************************/
    mDataFlow = oldDF;
    subprog_df->Resolve(subprog, sBody) ;
    VhdlValue *result = 0 ;

    // RBS thinks: This, apparently, is popping a stack of ids, constraints, on ids, or something similar. After this call,
    // the constraints on the subprogram/function argument formal ids is restored to the values they  had before the
    // VerificVhdlDesignWalker visitor 'Accept' call above.
    if (local_scope) {
        (void) local_scope->PopStack();
    }

    // Process assignments to external signals/variables that
    // are contained in the dataflow. See comments in Verific VhdlDeclaration_Elab.cpp
    // for more details.
    FOREACH_MAP_ITEM(subprog_df, mi, &id, &val) {
        if (!local_scope || local_scope->IsDeclaredHere(id)) continue ;
        if (id==subprog) continue ;
        if (mDataFlow) {
            if (mDataFlow && (!mDataFlow->IsInInitial() || mDataFlow->IsInSubprogramOrProcess())) {
                mDataFlow->SetAssignValue(id, val) ;
        } else if (id->Value()) {
            id->Value()->NetAssign(val,id->ResolutionFunction(),sBody) ;
        } else {
            id->SetValue(val) ;
        }
            
            (void) subprog_df->Remove(id) ;
        }
        
        if (subprog->IsProcedure()) {
            FOREACH_ARRAY_ITEM(arguments, i, assoc) {
                if (!assoc) continue ;
                
                if (assoc->IsAssoc() ) {
                    id = assoc->FindFullFormal() ;
                    if (!id) {
                        break ;
                    }
                } else {
                    id = subprog->Arg(i) ;
                }
                if (!id) continue ;
                if (id->IsInput()) continue ;
                
                val = (VhdlValue*)formal_to_value.GetValue(id) ;
                
                if (id->IsRefFormal()) {
                    VhdlDiscreteRange* ref_target = (VhdlDiscreteRange*)formal_to_actual.GetValue(id) ;
                    id->SetRefTarget(ref_target) ;
                    if (!ref_target) id->SetRefFormal(0) ; // Non Recursive.
                    delete val ;
                    continue ;
                }
                assoc->Assign(val, mDataFlow, 0) ;
            }
        }
    }
    delete subprog_df ;
    subprog_df = 0;

    if (0) // Debug
      UtIO::cout() << "DEBUG: <<< exit elaborateSubprogramBody(" << subprog->Name() << ")" << UtIO::endl;
    
    return result ;
}

///generate Nucleus object for proceure call
void VerificVhdlDesignWalker::procCall(VhdlIndexedName& node)
{
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "ProcCall" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif

    SourceLocator call_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());

        
    //handle system calls
    // RBS I think: There's a lot of duplication between 'checkSysCall' and 'checkAndHandleSysCall'.
    // Can probably get rid of checkSysCall.
    if (checkSysCall(node)) {
        if (NUBase*  base = checkAndHandleSysCall(node)) {
            mCurrentScope->sValue(base);
        } else {
            mCurrentScope->sValue(0);
        }
        return ;
    }
    
    VhdlSubprogramId* sId = static_cast<VhdlSubprogramId*>(node.GetId());
    V2N_INFO_ASSERT(sId, node, "invalid subprogram identifier");
    VhdlSubprogramBody* sBody = sId->Body();
    INFO_ASSERT(sBody, "Failed to get procedure body");
    Array* arguments = new Array;
    unsigned i = 0;
    VhdlTreeNode* item = 0;
    FOREACH_ARRAY_ITEM(node.GetAssocList(), i, item) {
        // RBS thinks: NULL actual will be returned for procedure calls with missing arguments. Note:
        // to see this, you'll need to use named association, leaving out variable arguments that have
        // default values.  
        VhdlDiscreteRange* actual = static_cast<VhdlDiscreteRange*>(item);
        V2N_INFO_ASSERT(actual, node, "failed to get actual argument");
        VhdlExpression* actualPart = actual->ActualPart();
        arguments->Insert(actualPart);
    }

    elaborateSubprogramBody(sBody, arguments, call_loc);

    delete arguments;
}

///generate Nucleaus object for procedure call
void VerificVhdlDesignWalker::procCall(VhdlName& node)
{
    SourceLocator call_loc = Verific2NucleusUtilities::gSourceLocator(*mNucleusBuilder,node.Linefile());
#ifdef DEBUG_VERIFIC_VHDL_WALKER
    std::cerr << "ProcCall VhdlName" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    INFO_ASSERT(!node.GetAssocList(), "should not have assoc list");

    VhdlSubprogramId* sId = dynamic_cast<VhdlSubprogramId*>(node.GetId());
    V2N_INFO_ASSERT(sId, node, "invalid subprogram identifier");
    VhdlSubprogramBody* sBody = sId->Body();
    INFO_ASSERT(sBody, "Failed to get procedure body");
    //there is no arguments
    Array* arguments = new Array;
    elaborateSubprogramBody(sBody, arguments, call_loc);
    delete arguments;
}

} //namespace verific2nucleus
