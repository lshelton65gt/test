// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "verific2nucleus/VerificTicProtectedNameManager.h"
#include "VeriTreeNode.h"
#include "VeriModule.h"
#include "VeriModuleItem.h"
#include "LineFile.h"
#include "veri_yacc.h"

namespace verific2nucleus {

VerificTicProtectedNameManager::VerificTicProtectedNameManager(SourceLocatorFactory *loc_factory) :
  mSourceLocatorFactory(loc_factory)
{}

VerificTicProtectedNameManager::~VerificTicProtectedNameManager()
{
}


static void sGenUniqueProtectedName (UtString* s)
{
  char buf[32];
  static int uuid = 0;

  sprintf (buf, "$Prot%0x", uuid++);
  *s += buf;
}


bool VerificTicProtectedNameManager::isTicProtected(Verific::linefile_type lf)
{
    return Verific::LineFile::IsProtectedLinefile(lf);
}
    

UtString VerificTicProtectedNameManager::getVisibleName(Verific::VeriTreeNode* ve_node, const UtString& origName)
{
    // it's not from protected region, default original name will be used
    UtString visibleName(origName);
    // Check if we have allready registered node with a name
    VeNodeToVisibleNameMap::iterator iter = mVeNodeToVisibleNameMap.find(ve_node);
    if ( iter != mVeNodeToVisibleNameMap.end()) {
        visibleName = iter->second;
        return visibleName;
    }
    // Check if node is coming from protected region
    if ( isTicProtected(ve_node->Linefile()) ){
        UtString new_protected_name;
        sGenUniqueProtectedName(&new_protected_name);
        visibleName = new_protected_name;
        mVeNodeToVisibleNameMap[ve_node] = new_protected_name;
    }
    return visibleName;
}

} // namespace verific2nucleus

