
// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


// Project headers
#include "verific2nucleus/VerificVerilogSTBuildWalker.h"     // Visitor base class definition
#include "verific2nucleus/VerificNucleusBuilder.h" // Nucleus Builder API
#include "verific2nucleus/V2NDesignScope.h" // Scope definition
#include "verific2nucleus/VerificDesignManager.h" // Design Manager definition
#include "verific2nucleus/Verific2NucleusUtilities.h" // Common utilities 
#include "verific2nucleus/VerificDesignWalkerCB.h"
#include "verific2nucleus/VerificTicProtectedNameManager.h"


// Verific headers
#include "Array.h"          // Make dynamic array class Array available
#include "Strings.h"        // A string utility/wrapper class
#include "Message.h"        // Make message handlers available, not used in this example
#include "VeriModule.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriModuleItem.h"     // Definition of a VeriModule and VeriPrimitive
#include "VeriId.h"         // Definitions of all identifier definition tree nodes
#include "VeriExpression.h" // Definitions of all verilog expression tree nodes
#include "VeriModuleItem.h" // Definitions of all verilog module item tree nodes
#include "VeriStatement.h"  // Definitions of all verilog statement tree nodes
#include "VeriMisc.h"       // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "VeriConstVal.h"   // Definitions of parse-tree nodes representing constant values in Verilog.
#include "veri_tokens.h"
#include "VeriBaseValue_Stat.h"
#include "hdl/HdlVerilogString.h" // for verilog string to binary string converter

// Standard headers
#include <stdio.h>          // sprintf
#include <string.h>         // strcpy, strchr ...
#include <cmath>            // for pow
#include <ctype.h>          // isalpha, etc ...
#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <sstream>

// Nucleus headers
#include "nucleus/NUBitNet.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUCopyContext.h"
#include "nucleus/NUBitNetHierRef.h"
#include "nucleus/NUVectorNetHierRef.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUSysFunctionCall.h"
//Set to enable debug
//#define DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER 1

namespace verific2nucleus {


using namespace std ;
using namespace Verific ;

/*-----------------------------------------------------------------*/
//                      Constructor / Destructor
/*-----------------------------------------------------------------*/

VerificVerilogSTBuildWalker::VerificVerilogSTBuildWalker(VerificNucleusBuilder* verificNucleusBuilder, UtMap<VeriTreeNode*,StringAtom*>* nameRegistry,
    UtMap<VeriTreeNode*,NUNet*>* declarationRegistry)
    : mCurrentScope(0)
    , mScopeStack()
    , mNucleusBuilder(verificNucleusBuilder)
    , mNameRegistry(nameRegistry)
    , mDeclarationRegistry(declarationRegistry)
{
    INFO_ASSERT(mNucleusBuilder, "Null nucleus builder");
    INFO_ASSERT(mNameRegistry, "Null name registery object");
    INFO_ASSERT(mDeclarationRegistry, "Null declaration registery object");
}

VerificVerilogSTBuildWalker::~VerificVerilogSTBuildWalker()
{
}


/*-----------------------------------------------------------------*/
//               Push/Pop/Get methods  current scope 
/*-----------------------------------------------------------------*/

V2NDesignScope* VerificVerilogSTBuildWalker::gScope() 
{ 
    return mCurrentScope;
}

void VerificVerilogSTBuildWalker::PushScope(V2NDesignScope* new_scope, const UtString& scopeName)             
{
    //// Getting parent branch nodes (can be null)
    STBranchNode* unelabParentScope = NULL;
    STBranchNode* elabParentScope = NULL; 
    if (gScope()) {
        unelabParentScope = gScope()->gUnelabSTScope();
        elabParentScope = gScope()->gElabSTScope();
    }
    // Pushing current scope to stack
    mScopeStack.push(mCurrentScope);
    mCurrentScope = new_scope;
    aSTScope(scopeName, elabParentScope, unelabParentScope);
}

void VerificVerilogSTBuildWalker::PopScope()
{
    delete mCurrentScope;
    mCurrentScope = mScopeStack.top();
    mScopeStack.pop();
}

void VerificVerilogSTBuildWalker::aSTScope(const UtString& name, STBranchNode* unelabParent, STBranchNode* elabParent)
{
    STBranchNode* newUnelabParent = NULL;
    STBranchNode* newElabParent = NULL;
    mNucleusBuilder->cSTBranch(name, unelabParent, elabParent, &newUnelabParent, &newElabParent);
    gScope()->sUnelabSTScope(newUnelabParent);
    gScope()->sElabSTScope(newElabParent);
}

void VerificVerilogSTBuildWalker::aSTNode(const UtString& netName, NUNet* net, unsigned netType)
{
    STSymbolTableNode* node = mNucleusBuilder->cSTNode(netName, gScope()->gUnelabSTScope(), gScope()->gElabSTScope());
    mNucleusBuilder->aUserTypeInfo(net, node, netType);
}

/*-----------------------------------------------------------------*/
//          Utilities
/*-----------------------------------------------------------------*/


UtString VerificVerilogSTBuildWalker::gVisibleName(VeriTreeNode* ve_node, const UtString& origName)
{
    return mNucleusBuilder->gTicProtectedNameMgr()->getVisibleName(ve_node, origName);
}


UtString
VerificVerilogSTBuildWalker::createNucleusString(const UtString& data)
{
    return mNucleusBuilder->createNucleusString(data);
}

VeriIdDef*
VerificVerilogSTBuildWalker::fActualRefId(VeriExpression* vpc)
{
    VeriExpression* portRef = vpc;
    if (portRef->GetConnection()) {
        //  if this is an port connection, then take actual expression
        portRef = portRef->GetConnection();
    }
    if (portRef->GetPrefix()) {
        // if this is an indexed expression, then take prefix
        portRef = portRef->GetPrefix();
    }
    return portRef->GetId();
}

NUNet*
VerificVerilogSTBuildWalker::findNet(VeriTreeNode* verificNet)
{
    if (mDeclarationRegistry->find(verificNet) != mDeclarationRegistry->end()) {
        return mDeclarationRegistry->at(verificNet);
    }
    return 0;
}

StringAtom*
VerificVerilogSTBuildWalker::findName(VeriTreeNode* verificObject)
{
    if (mNameRegistry->find(verificObject) != mNameRegistry->end()) {
        return mNameRegistry->at(verificObject);
    }
    return 0;
}

VeriDataType* 
VerificVerilogSTBuildWalker::gMaxRangeDataType(VeriDataType* first, VeriDataType* second)
{
    if (first && second) {
        if (gMaxRange(first->GetDimensions(), second->GetDimensions()) == first->GetDimensions()) {
            return first;
        } else {
            return second;
        }
    } else if (first && !second) {
        return first;
    } else if (!first && second) {
        return second;
    } else {
        return NULL; // both first and second were null
    }
}

VeriRange* 
VerificVerilogSTBuildWalker::gMaxRange(VeriRange* first, VeriRange* second)
{
    unsigned firstDim;
    if (first) {
        firstDim = first->RangeWidth();
    } else {
        firstDim = 1;
    }
    unsigned secondDim;
    if (second) {
        secondDim = second->RangeWidth();
    } else {
        secondDim = 1;
    }
    if (firstDim >= secondDim) {
        return first;
    } else {
        return second;
    }
}

/*-----------------------------------------------------------------*/
//          VISITORS 
/*-----------------------------------------------------------------*/

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriModule, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "---------------------------------------------------------------" << std::endl;
    std::cerr << "STBUILDER VERIMODULE" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    UtString moduleName;
    if (node.IsTickProtectedNode() && node.GetId()->IsTickProtectedNode()) {
        moduleName = (gVisibleName(&node, createNucleusString(node.GetId()->GetName())));
    } else {
        moduleName = mNucleusBuilder->gModuleName(&node);
        if (moduleName.empty())  {
            moduleName = node.GetId()->GetName();
        }
        moduleName = createNucleusString(moduleName);
    }
    //set the current scope
    // only for top level module we should push the scope
    if (mNucleusBuilder->IsTopModule(moduleName)) {
        V2NDesignScope* current_scope = new V2NDesignScope(0, NULL);
        current_scope->setModuleScope();
        PushScope(current_scope, moduleName);
    }
    
    // Create Declaration Items
    unsigned i;
    VeriModuleItem *mid ;
    FOREACH_ARRAY_ITEM(node.GetModuleItems(), i, mid) {
        if (mid) { 
            if (mid->IsIODecl()) {
                mid->Accept(*this);
            }
        }
    }

    // Add ports to scope in declaration order (which matters)
    if ( node.GetPorts() ){
      VeriIdDef *portId ;
      FOREACH_ARRAY_ITEM(node.GetPorts(), i, portId) {
        NUNet* p = findNet(portId);
        if (p) {
          aSTNode(gVisibleName(portId, createNucleusString(portId->GetName())), p, (portId->GetDataType() && portId->GetDataType()->GetType()) ? portId->GetDataType()->GetType() : VERI_WIRE);
        }
      }
    }

    // Create other module Items
    VeriModuleItem *mi ;
    FOREACH_ARRAY_ITEM(node.GetModuleItems(), i, mi) {
        if (mi) { 
            if (!mi->IsIODecl()) {
                mi->Accept(*this);
            }
        }
    }
    if (mNucleusBuilder->IsTopModule(moduleName)) {
        PopScope();
    }
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER

    std::cerr << "---------------------------------------------------------------" << std::endl;
    std::cerr << "END STBUILDER VERIMODULE" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriDataDecl, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERIDATADECL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Create Ports and add to module 
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(node.GetIds(), i, id) {
        if (!id) continue ;
        // Skip parameters
        if (id->IsParam()) {
            continue;
        } else {
            NUNet* p = findNet(id); 
            if (p) {
                VeriDataType* dataType = gMaxRangeDataType(node.GetDataType(), id->GetDataType());
                UtString netName = gVisibleName(id, createNucleusString(id->GetName()));
                if ((VERI_INPUT == node.GetDir()) || (VERI_OUTPUT == node.GetDir()) || (VERI_INOUT == node.GetDir())) {
                    if (NUScope::eModule != gScope()->isModuleScope()) {
                        aSTNode(netName, p, (dataType && dataType->GetType()) ? dataType->GetType() : VERI_WIRE);
                    }
                } else {
                    aSTNode(netName, p, dataType->GetType());
                }
            }
        }
    }
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERIDATADECL" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriAnsiPortDecl, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERIANSIPORTDECL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    ///TODO ports need to support the signed datatype as well
    // Create Ports and add to module 
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(node.GetIds(), i, id) {
        if (!id) continue ;
        NUNet* p = findNet(id); 
        if (p) {
            VeriDataType* dataType = gMaxRangeDataType(node.GetDataType(), id->GetDataType());
            UtString netName = gVisibleName(id, createNucleusString(id->GetName()));
            aSTNode(netName, p, (dataType && dataType->GetType()) ? dataType->GetType() : VERI_WIRE);
        }
    }
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERIANSIPORTDECL" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriAlwaysConstruct, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERIALWAYSCONSTRUCT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VeriStatement* statement = NULL;
    if (node.GetStmt() && node.GetStmt()->IsEventControl())
      // We have an event control. Examine the statement portion of the event control.
      statement = node.GetStmt()->GetStmt();
    else
      // No event control.
      statement = node.GetStmt();
    
    if (statement) {
        if (not statement->IsSeqBlock() && Verific2NucleusUtilities::hasSideEffectStmt(statement) ) {
            StringAtom* scopeNameId = findName(statement);
            bool skip = false;
            UtString scopeName;
            if (scopeNameId) {
                scopeName = UtString(scopeNameId->str()); 
            } else {
                skip = true;
            }
            if (!skip) {
                // Create scope
                V2NDesignScope* current_scope = new V2NDesignScope(0, gScope());
                // Push the scope
                PushScope(current_scope, scopeName);
                // Get declarations
                statement->Accept(*this);
                // restore the scope.
                PopScope();
            }
        } else {
            statement->Accept(*this);
        }
    }
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERIALWAYSCONSTRUCT" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriSeqBlock, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERISEQBLOCK" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    UtString blockName;
    bool isUserNamed = false;
    if (node.GetLabel()) {
        isUserNamed = true;
        blockName = gVisibleName(&node, createNucleusString(node.GetLabel()->GetName()));
    }
    // Get scope name
    UtString scopeName = "";
    bool skip = false;
    if (isUserNamed) {
        scopeName = blockName;
    } else {
        StringAtom* scopeNameId = findName(&node);
        if (scopeNameId) {
            scopeName = UtString(scopeNameId->str()); 
        } else {
            // skip the block if nucleus has no generated id for it
            skip = true;
        }
    }
    if (!skip) {
        // Create scope
        V2NDesignScope* current_scope = new V2NDesignScope(0, gScope());
        // Push the scope
        PushScope(current_scope, scopeName);
        // Get declarations
        unsigned i ;
        VeriModuleItem *decl ;
        FOREACH_ARRAY_ITEM(node.GetDeclItems(), i, decl) {
            if (decl) decl->Accept(*this) ;
        }
        // Get statement list
        VeriStatement *veriStmt;
        FOREACH_ARRAY_ITEM(node.GetStatements(), i, veriStmt) {
            if (veriStmt) veriStmt->Accept(*this) ;
        }
        // restore the scope.
        PopScope();
    }
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERISEQBLOCK" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriGenerateBlock, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "STBUILDER VERIGENERATEBLOCK" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Create scope
    /// We don't have NUBlock, so creating scope with null object
    V2NDesignScope* current_scope = new V2NDesignScope(0, gScope());
    // We have set compile flag to always have named blocks (auto-generate names for unnamed blocks)
    //UtString blockName = createNucleusString(node.GetBlockId()->GetName());
    UtString blockName(gVisibleName(&node, node.GetBlockId()->GetName()));
    // Push the scope
    PushScope(current_scope, blockName);
    // Get module items 
    unsigned i ;
    VeriModuleItem* mi;
    FOREACH_ARRAY_ITEM(node.GetItems(), i, mi) {
        if (mi) mi->Accept(*this);
    }
    // restore the scope.
    PopScope();
#ifdef DEBUG_VERIFIC_VERILOG_WALKER
    std::cerr << "END STBUILDER VERIGENERATEBLOCK" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriNetDecl, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERINETDECL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Create net and add to current scope
    VeriIdDef *id ;
    unsigned i;
    FOREACH_ARRAY_ITEM(node.GetIds(), i, id) {
        NUNet* n = findNet(id);
        if (n) {
            UtString netName = gVisibleName(id, createNucleusString(id->GetName()));
            aSTNode(netName, n, node.GetNetType());
        }
    }
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERINETDECL" << std::endl;
#endif
}


void VerificVerilogSTBuildWalker::VERI_VISIT(VeriSelectedName, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERISELECTEDNAME" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VeriIdDef* ref = node.GetId();
    NUNet* netHierRef = 0;
    if (ref && (ref->IsNet() || ref->IsReg() || ref->IsPort())) {
        // parentModule - is the module where the hierarchical reference of the object (net, task) occurs
        netHierRef = findNet(&node);
        if (netHierRef) {
            UtString netName(netHierRef->getName()->str());
            aSTNode(netName, netHierRef, (ref->GetDataType() && ref->GetDataType()->GetType()) ? ref->GetDataType()->GetType() : VERI_WIRE);
        }
    }
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERISELECTEDNAME" << std::endl;
#endif
}


void VerificVerilogSTBuildWalker::VERI_VISIT(VeriConditionalStatement, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERICONDITIONALSTATEMENT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    if (node.GetThenStmt()) {
        if (! node.GetThenStmt()->IsSeqBlock()) {
            V2NDesignScope* current_scope = new V2NDesignScope(0, gScope());
            StringAtom* scopeNameId = findName(node.GetThenStmt());
            if (scopeNameId) {
                UtString scopeName(scopeNameId->str());
                // Push the scope
                PushScope(current_scope, scopeName);
                node.GetThenStmt()->Accept(*this) ;
                PopScope();
            }
        } else {
            node.GetThenStmt()->Accept(*this) ;
        }
    }
    if (node.GetElseStmt()) {
        // if there is no seq block create one 
        if (! node.GetElseStmt()->IsSeqBlock()) {
            //pushscope
            V2NDesignScope* current_scope = new V2NDesignScope(0, gScope());
            StringAtom* scopeNameId = findName(node.GetElseStmt());
            if (scopeNameId) {
                UtString scopeName(scopeNameId->str());
                // Push the scope
                PushScope(current_scope, scopeName);
                node.GetElseStmt()->Accept(*this) ;
                PopScope();
            }
        } else {
            node.GetElseStmt()->Accept(*this) ;
        }
    }
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERICONDITIONALSTATEMENT" << std::endl;
#endif
}


void VerificVerilogSTBuildWalker::VERI_VISIT(VeriFunctionDecl, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERIFUNCTIONDECL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif

    // TODO need to get from registery
    V2NDesignScope* current_scope = new V2NDesignScope(0, gScope());
    UtString functionName(gVisibleName(node.GetFunctionId(), createNucleusString(node.GetFunctionId()->GetName())));
    PushScope(current_scope, functionName);

    // adding function output port 
    if (! Verific2NucleusUtilities::isVoidFunction(&node)) {
        NUNet* p = findNet(node.GetFunctionId());
        // if p is null then there was no net defined for the output of this function,
        // probably because there was an error of an unsupported type for that output.
        if ( p ) {
          UtString portName(p->getName()->str());
          aSTNode(portName, p, (node.GetDataType() && node.GetDataType()->GetType()) ? node.GetDataType()->GetType() : VERI_WIRE);
        }
    }
    
    // Get ansi port declarations
    unsigned i ;
    VeriAnsiPortDecl * ansi_decl_item = 0 ;
    FOREACH_ARRAY_ITEM(node.GetAnsiIOList(), i, ansi_decl_item) {
        if (ansi_decl_item) ansi_decl_item->Accept(*this) ;
    }

    // checking declarations 
    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(node.GetDeclarations(), i, decl_item) {
        if (decl_item) decl_item->Accept(*this) ;
    }
    // Adding statements
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, stmt) {
        if (stmt) stmt->Accept(*this) ;
    }
    PopScope();
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERIFUNCTIONDECL" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriModuleInstantiation, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERIMODULEINSTANTIATION" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // TODO support mixed designs vhdl node
    VeriModule *mod = node.GetInstantiatedModule();
    unsigned i ;
    VeriInstId *inst ;
    FOREACH_ARRAY_ITEM(node.GetInstances(), i, inst) {
        if (mod) {
            StringAtom* inst_name = findName(inst);
            
            V2NDesignScope* current_scope = new V2NDesignScope(0, gScope());
            if (gScope()->isModuleScope()) {
                current_scope->setModuleScope();
            }
            PushScope(current_scope, inst_name->str());
            mod->Accept(*this);
            PopScope();
        }
    }
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERIMODULEINSTANTIATION" << std::endl;
#endif
}


void VerificVerilogSTBuildWalker::VERI_VISIT(VeriCaseStatement, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERICASESTATEMENT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // Get case items
    if (node.GetCaseItems()) {
        unsigned i ;
        VeriCaseItem *ci ;
        FOREACH_ARRAY_ITEM(node.GetCaseItems(), i, ci) {
            ci->Accept(*this);
        }
    }
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERICASESTATEMENT" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriCaseItem, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERICASEITEM" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    if (node.GetStmt()) node.GetStmt()->Accept(*this) ;
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERICASEITEM" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriTaskDecl, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERITASKDECL" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    V2NDesignScope* current_scope = new V2NDesignScope(0, gScope());
    UtString taskName(gVisibleName(node.GetTaskId(), createNucleusString(node.GetTaskId()->GetName())));
    // We have a choice which one to put under temp, so prefereing task specific one, in order not to polute global stuff
    PushScope(current_scope, taskName);
    // Get ansi port declarations
    unsigned i ;
    VeriAnsiPortDecl * ansi_decl_item = 0 ;
    FOREACH_ARRAY_ITEM(node.GetAnsiIOList(), i, ansi_decl_item) {
        if (ansi_decl_item) ansi_decl_item->Accept(*this) ;
    }
    // Now decls and statement
    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(node.GetDeclarations(), i, decl_item) {
        decl_item->Accept(*this) ;
    }
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(node.GetStatements(), i, stmt) {
        if (stmt) stmt->Accept(*this) ;
    }
    PopScope();
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERITASKDECL" << std::endl;
#endif
}


void VerificVerilogSTBuildWalker::VERI_VISIT(VeriFor, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERIFOR" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    // If the initializer has declarations, we create a named decl scope
    // around the entire loop
    bool hasDecls = Verific2NucleusUtilities::forLoopHasDecls(node);
    
    if (hasDecls) {
      V2NDesignScope* enclosing_scope = new V2NDesignScope(0, gScope());
      
      StringAtom* scopeNameId = findName(&node);
      if (scopeNameId) {
        UtString scopeName(scopeNameId->str());
        PushScope(enclosing_scope, scopeName);
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(node.GetInitials(), i, item) {
            if (item) item->Accept(*this);
        }
      }
    }

    V2NDesignScope* current_scope = new V2NDesignScope(0, gScope());
    // Push the scope
    
    StringAtom* scopeNameId = findName(&node);
    if (scopeNameId) {
        UtString scopeName(scopeNameId->str());
        PushScope(current_scope, scopeName);
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(node.GetRepetitions(), i, item) {
            item->Accept(*this);
        }
        // restore the scope.
        PopScope();
        if (node.GetStmt()) node.GetStmt()->Accept(*this);
    }

    // Pop the enclosing scope
    if (hasDecls)
      PopScope();
    
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERIFOR" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriEventControlStatement, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERIEVENTCONTROLSTATEMENT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    if (node.GetStmt()) node.GetStmt()->Accept(*this) ;
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERIEVENTCONTROLSTATEMENT" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriRepeat, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERIREPEAT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    if (node.GetStmt()) node.GetStmt()->Accept(*this) ;
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERIREPEAT" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriWhile, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERIWHILE" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    if (node.GetStmt()) node.GetStmt()->Accept(*this);
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERIWHILE" << std::endl;
#endif
}


void VerificVerilogSTBuildWalker::VERI_VISIT(VeriInitialConstruct, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "STBUILDER VERIINITIALCONSTRUCT" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    VeriStatement* initStmt = node.GetStmt();
    if (initStmt) {
        if (not initStmt->IsSeqBlock() && Verific2NucleusUtilities::hasSideEffectStmt(initStmt) ) {
            StringAtom* scopeNameId = findName(initStmt);
            bool skip = false;
            UtString scopeName;
            if (scopeNameId) {
                scopeName = UtString(scopeNameId->str()); 
            } else {
                skip = true;
            }
            if (!skip) {
                // Create scope
                V2NDesignScope* current_scope = new V2NDesignScope(0, gScope());
                // Push the scope
                PushScope(current_scope, scopeName);
                // Get declarations
                initStmt->Accept(*this);
                // restore the scope.
                PopScope();
            }
        } else {
            initStmt->Accept(*this);
        }
    }

#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERIINITIALCONSTRUCT" << std::endl;
#endif
}

void VerificVerilogSTBuildWalker::VERI_VISIT(VeriDelayControlStatement, node)
{
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "VERIDELAYCONTROLSTATEMENT VISIT CALLED" << std::endl;
    node.PrettyPrint(std::cerr, 0);
    std::cerr << std::endl;
#endif
    if (node.GetStmt()) node.GetStmt()->Accept(*this) ;
#ifdef DEBUG_VERIFIC_VERILOG_ST_BUILD_WALKER
    std::cerr << "END STBUILDER VERIDELAYCONTROLSTATEMENT" << std::endl;
#endif
}

} // namespace verific2nucleus

