// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "langcpp/LangCppStmtTree.h"
#include "langcpp/LangCppWalker.h"

#include "util/UtIOStream.h"
#include "exprsynth/Expr.h"
#include "symtab/STAliasedLeafNode.h"
#include "util/StringAtom.h"
#include "util/UtIndent.h"

static void sEmitIndent(UtOStream& out, UInt32 indent)
{
  UtString buf(indent, ' ');
  out << buf;
}

//! Class to add commas to a list of things
class CommaClosure
{
public:
  CARBONMEM_OVERRIDES

  CommaClosure(UInt32 listSize) : mNumElements(listSize),
                                  mNumSeen(0)
  {}

  //! Emit a comma if necessary
  /*! 
    Place this before you emit the thing in the list.
  */
  void emit(UtOStream& out)
  { 
    if ((mNumSeen > 0) &&
        (mNumSeen < mNumElements))
      out << ", ";
    ++mNumSeen;
  }
  
private:
  // number of elements seen so far in a list
  UInt32 mNumElements;
  UInt32 mNumSeen;
};

// LangCppStatement
LangCppStatement::LangCppStatement()
{}

LangCppStatement::~LangCppStatement()
{}

const LangCppScope* LangCppStatement::castScope() const
{
  return NULL;
}

// LangCppLineComment
LangCppLineComment::LangCppLineComment(const char* comment)
  : mComment(comment)
{}

LangCppLineComment:: ~LangCppLineComment()
{}  

void LangCppLineComment::emit(UtOStream& out, UInt32) const
{
  out << "// " << mComment << UtIO::endl;
}

void LangCppLineComment::visit(LangCppWalker* walker)
{
  walker->visitLineComment(this);
}

// LangCppScope

//! Structure to sort variables in a scope
struct LangCppScope::VariableSort
{
  CARBONMEM_OVERRIDES

  VariableSort() {}

  bool operator()(const LangCppVariable* a, const LangCppVariable* b) const
  {
    CarbonIdent* identA = a->getIdent();
    CarbonIdent* identB = b->getIdent();
    DynBitVector dummy;
    STAliasedLeafNode* nodeA = identA->getNode(&dummy);
    STAliasedLeafNode* nodeB = identB->getNode(&dummy);
    // no hierarchy in these nodes as they only represent variables
    StringAtom* strA = nodeA->strObject();
    StringAtom* strB = nodeB->strObject();
    return *strA < *strB;
  }
};

LangCppScope::LangCppScope(LangCppScope* parent)
  : mParent(parent)
{}

LangCppScope::~LangCppScope() 
{
  for (LangCppStmtList::iterator p = mStatements.begin(),
         e = mStatements.end();
       p != e; ++p)
    delete *p;

  for (LangCppExprArray::iterator p = mSavedExprs.begin(), 
         e = mSavedExprs.end(); p != e; ++p)
    delete *p;

  mVariables.clearPointerValues();

  mScopeTypes.clearPointerValues();
}

const LangCppScope* LangCppScope::castScope() const
{
  return this;
}

void LangCppScope::emitVariableDecls(UtOStream& out, UInt32 indent) const
{
  // we have to sort the variable declarations for readability
  // Create a vector of variables, and sort by name.
  typedef UtArray<const LangCppVariable*> ConstLangCppVariableArray;
  ConstLangCppVariableArray tmpArray;
  for (IdentToLangCppVarMap::UnsortedCLoop p = mVariables.loopCUnsorted();
       ! p.atEnd(); ++p)
  {
    const LangCppVariable* var = p.getValue();
    tmpArray.push_back(var);
  }
  std::sort(tmpArray.begin(), tmpArray.end(), VariableSort());

  for (ConstLangCppVariableArray::iterator p = tmpArray.begin(), 
         e = tmpArray.end();
       p != e; ++p)
  {
    const LangCppVariable* var = *p;
    // don't emit if already declared as a function parameter
    if (! var->isNoDeclare())
    {
      sEmitIndent(out, indent);
      var->emitDeclare(out);
      out << ";" << UtIO::endl;
    }
  }
  if (! tmpArray.empty())
    out << UtIO::endl;
}

void LangCppScope::emit(UtOStream& out, UInt32 indent) const
{
  out << "{" << UtIO::endl;
  indent += 2;
  emitVariableDecls(out, indent);
  for (LangCppStmtList::const_iterator p = mStatements.begin(), 
         e = mStatements.end(); p != e; ++p)
  {
    const LangCppStatement* stmt = *p;
    sEmitIndent(out, indent);
    stmt->emit(out, indent);
  }
  indent -= 2;
  sEmitIndent(out, indent);
  out << "}" << UtIO::endl;
}

void LangCppScope::visit(LangCppWalker* walker)
{
  if (!walker->preVisitScope(this))
    return;

  // visit the statements within this scope first
  for (LangCppStmtList::iterator p = mStatements.begin(), 
         e = mStatements.end(); p != e; ++p)
  {
    LangCppStatement* stmt = *p;
    walker->visitStmt(stmt);
  }

  // visit this scope
  walker->visitScope(this);
}

void LangCppScope::addLineComment(const char* comment)
{
  LangCppLineComment* stmt = new LangCppLineComment(comment);
  addStmt(stmt);
}

void LangCppScope::addUserStatement(const UtString& stmtStr)
{
  LangCppUserStatement* stmt = new LangCppUserStatement(stmtStr);
  addStmt(stmt);
}

LangCppIf* LangCppScope::addIf(LangCppExpr* condition)
{
  LangCppIf* stmt = new LangCppIf(condition, this);
  addChildScope(stmt);
  return stmt;
}

LangCppType* LangCppScope::findType(const char* typeName)
{
  LangCppType* ret = NULL;
  // first check if the type exists in an ancestor
  if (mParent)
    ret = mParent->findType(typeName);
  
  if (ret == NULL)
  {
    UtString typeStr(typeName);
    StrLangCppTypeMap::iterator p = mScopeTypes.find(typeStr);
    if (p != mScopeTypes.end())
      ret = p->second;
  }
  return ret;
}

void LangCppScope::addType(LangCppType* type)
{
  mScopeTypes[type->getTypeNameStr()] = type;
}

// assert function helpers

static void sTypePrevDeclAsNonClass(const char* className, LangCppType* type)
{
  UtString msg;
  msg << "'" << className << "' previously declared as a non-class type: ";
  type->compose(&msg);
  UtIO::cout() << msg << UtIO::endl;
}

static void sTypePrevDeclAs(const char* typeName, LangCppType* type)
{
  UtString msg;
  msg << "'" << typeName << "' previously declared as: ";
  type->compose(&msg);
  UtIO::cout() << msg << UtIO::endl;
}

static void sTypeNotDeclared(const char* typeName)
{
  UtString msg;
  msg << "'" << typeName << "' not declared.";
  UtIO::cout() << msg << UtIO::endl;
}

static void sExpectedPodType(LangCppType* type)
{ 
  UtString msg;
  msg << "Expected pod type: ";
  type->compose(&msg);
  UtIO::cout() << msg << UtIO::endl;
}

static void sExpectedClassType(LangCppType* type)
{
  UtString msg;
  msg << "Expected class type: ";
  type->compose(&msg);
  UtIO::cout() << msg << UtIO::endl;
}

LangCppClassType* LangCppScope::declareClassType(const char* className)
{
  LangCppType* basictype = findType(className);
  LangCppClassType* ret = NULL;
  if (basictype)
  {
    ret = basictype->castClass();
    if (! ret)
      FUNC_ASSERT(ret, sTypePrevDeclAsNonClass(className, ret));
  }
  else
  {
    ret = new LangCppClassType(className);
    addType(ret);
  }
  return ret;
}

LangCppClassType* LangCppScope::getClassType(const char* className)
{
  LangCppType* basicType = findType(className);
  FUNC_ASSERT(basicType, sTypeNotDeclared(className));
  LangCppClassType* ret = basicType->castClass();
  FUNC_ASSERT(ret, sTypePrevDeclAsNonClass(className, basicType));
  return ret;
}


LangCppPodType* LangCppScope::declarePodType(const char* typeName, 
                                          LangCppType* typeDef)
{
  LangCppPodType* podType = typeDef->castPod();
  FUNC_ASSERT(podType, sExpectedPodType(typeDef));
  LangCppType* foundType = findType(typeName);
  LangCppPodType* ret = NULL;
  if (foundType)
  {
    ret = foundType->castPod();
    FUNC_ASSERT(ret, sTypePrevDeclAs(typeName, ret));
  }
  else
  {
    ret = new LangCppPodType(typeName, podType->getNumBytes(),
                             podType->getSign());
    addType(ret);
  }
  return ret;
}

LangCppPodType* LangCppScope::getPodType(const char* typeName)
{
  LangCppType* ret = findType(typeName);
  FUNC_ASSERT(ret, sTypeNotDeclared(typeName));
  LangCppPodType* podType = ret->castPod();
  FUNC_ASSERT(podType, sTypePrevDeclAs(typeName, ret));
  return podType;
}

LangCppType* LangCppScope::getType(const char* typeName)
{
  LangCppType* ret = findType(typeName);
  FUNC_ASSERT(ret, sTypeNotDeclared(typeName));
  return ret;
}

LangCppVariable* LangCppScope::findVariable(CarbonIdent* ident)
{
  LangCppVariable* ret = NULL;
  // first check if the variable exists in an ancestor
  if (mParent)
    ret = mParent->findVariable(ident);
  
  if (ret == NULL)
    ret = findVariableInScope(ident);
  return ret;
}

LangCppVariable* LangCppScope::findVariableInScope(CarbonIdent* ident)
{
  LangCppVariable* ret = NULL;
  IdentToLangCppVarMap::iterator p = mVariables.find(ident);
  if (p != mVariables.end())
    ret = p->second;
  return ret;
}

void LangCppScope::addStmt(LangCppStatement* stmt)
{
  mStatements.push_back(stmt);
}

void LangCppScope::addChildScope(LangCppScope* scope)
{
  addStmt(scope);
  mChildScopes.push_back(scope);
}

LangCppAssign* LangCppScope::addAssign(LangCppExpr* lhs, LangCppExpr* rhs)
{
  LangCppAssign* assign = new LangCppAssign(lhs, rhs);
  addStmt(assign);
  return assign;
}

LangCppAssign* LangCppScope::addPrefixIncrement(LangCppVariable* var)
{
  LangCppAssign* ret = new LangCppPrefixIncrement(var);
  addStmt(ret);
  return ret;
}

LangCppAssign* LangCppScope::addOrAssign(LangCppExpr* lhs, LangCppExpr* rhs)
{
  LangCppAssign* assign = new LangCppOrAssign(lhs, rhs);
  addStmt(assign);
  return assign;
}

LangCppAssign* LangCppScope::addAndAssign(LangCppExpr* lhs, LangCppExpr* rhs)
{
  LangCppAssign* assign = new LangCppAndAssign(lhs, rhs);
  addStmt(assign);
  return assign;
}

LangCppAssign* LangCppScope::addAssignAlias(LangCppVariable* lhs, CarbonExpr* rhs,
                                            const LangCppVariableArray& referencedVars)
{
  LangCppCarbonExpr* rhsExpr = new LangCppCarbonExpr(rhs, referencedVars);
  addExpr(rhsExpr);
  return addAssignAlias(lhs, rhsExpr);
}

LangCppAssign* LangCppScope::addAssignAlias(LangCppVariable* lhs, 
                                            LangCppCarbonExpr* rhs)
{
  mCarbonExprVarAliases[rhs->getCarbonExpr()] = lhs;
  return addAssign(lhs, rhs);
}

LangCppVariable* LangCppScope::getAlias(LangCppCarbonExpr* expr)
{
  return getAlias(expr->getCarbonExpr());
}

LangCppVariable* LangCppScope::getAlias(CarbonExpr* expr)
{
  LangCppVariable* ret = NULL;
  ExprVarMap::iterator p = mCarbonExprVarAliases.find(expr);
  if (p != mCarbonExprVarAliases.end())
    ret = p->second;
  else if (mParent)
  {
    // check if the assignment was done in a parent scope
    ret = mParent->getAlias(expr);
  }
  return ret;
}

LangCppScope* LangCppScope::addScope()
{
  LangCppScope* scope = new LangCppScope(this);
  addChildScope(scope);
  return scope;
}

LangCppFor* LangCppScope::addFor()
{
  LangCppFor* forScope = new LangCppFor(this);
  addChildScope(forScope);
  return forScope;
}

void LangCppScope::addMemCpy(LangCppVariable* dest,
                             LangCppVariable* src,
                             UInt32 numBytes)
{
  LangCppMemCpy* memCpy = new LangCppMemCpy(dest, src, numBytes);
  addStmt(memCpy);
}

void LangCppScope::addMemSet(LangCppVariable* dest,
                             LangCppVariable* val,
                             UInt32 numBytes)
{
  LangCppMemSet* memSet = new LangCppMemSet(dest, val, numBytes);
  addStmt(memSet);
}

void LangCppScope::addReturn(LangCppExpr* expr)
{
  LangCppReturn* retStmt = new LangCppReturn(expr);
  addStmt(retStmt);
}

void LangCppScope::addIfDef(const char* preProcessStr)
{
  LangCppIfDef* stmt = new LangCppIfDef(preProcessStr);
  addStmt(stmt);
}

void LangCppScope::addEndIf()
{
  LangCppEndIf* stmt = new LangCppEndIf();
  addStmt(stmt);
}

LangCppDeclAssign* LangCppScope::addDeclAssign(LangCppVariable* lhs, 
                                               LangCppExpr* rhs)
{
  lhs->addAttribute(LangCppVariable::eAttribNoDeclare);
  LangCppDeclAssign* assign = new LangCppDeclAssign(lhs, rhs);
  addStmt(assign);
  return assign;
}

LangCppDeclAssign* LangCppScope::addDeclAssignAlias(LangCppVariable* lhs, CarbonExpr* rhs,
                                            const LangCppVariableArray& referencedVars)
{
  LangCppCarbonExpr* rhsExpr = new LangCppCarbonExpr(rhs, referencedVars);
  addExpr(rhsExpr);
  return addDeclAssignAlias(lhs, rhsExpr);
}

LangCppDeclAssign* LangCppScope::addDeclAssignAlias(LangCppVariable* lhs,
                                                LangCppCarbonExpr* rhs)
{
  mCarbonExprVarAliases[rhs->getCarbonExpr()] = lhs;
  return addDeclAssign(lhs, rhs);
}

LangCppFuncCallStmt* LangCppScope::addCall(LangCppFuncCall* call)
{
  LangCppFuncCallStmt* stmt = new LangCppFuncCallStmt(call);
  addStmt(stmt);
  return stmt;
}

static void sVarPrevDeclAs(CarbonIdent* ident, LangCppVariable* var)
{
  UtString msg;
  msg << "Variable '";
  ident->compose(&msg);
  msg << "' previously declared as ";
  var->compose(&msg);
  UtIO::cout() << msg << UtIO::endl;
}


LangCppVariable* LangCppScope::declareVariable(LangCppType* varType, CarbonIdent* ident)
{
  LangCppVariable* ret = findVariable(ident);
  if (ret)
    FUNC_ASSERT(varType->compare(ret->getType()) == 0, sVarPrevDeclAs(ident, ret));
  else
  {
    ret = new LangCppVariable(varType, ident);
    addVariable(ret);
  }
  return ret;
}

void LangCppScope::addVariable(LangCppVariable* variable)
{
  mVariables[variable->getIdent()] = variable;
}

LangCppStaticCast* LangCppScope::staticCast(LangCppType* type, LangCppVariable* hdl)
{
  // This is a dangling reference, so it needs to be part of statement
  // in order to be cleaned up.
  LangCppStaticCast* expr = new LangCppStaticCast(type, hdl->getType(), hdl);
  addExpr(expr);
  return expr;
}

LangCppDereference* LangCppScope::dereference(LangCppExpr* expr)
{
  LangCppDereference* deref = new LangCppDereference(expr);
  addExpr(deref);
  return deref;
}


LangCppFuncCall* 
LangCppScope::callMethod(LangCppVariable* instance,
                          const char* methodName,
                         LangCppExpr* paramList[])
{
  LangCppType* type = instance->getType();
  const LangCppClassType* chkType = type->castClass();
  if (! chkType) {
    const LangCppPointerType* pType = type->castPointerType();
    if (pType)
    {
      chkType = pType->getPointed()->castClass();
    }
  }
  FUNC_ASSERT(chkType, sExpectedClassType(type));
  
  LangCppFuncCall* expr = new LangCppMethodCall(methodName, 
                                                instance, 
                                                paramList);
  addExpr(expr);
  return expr;
}

LangCppVariable* 
LangCppScope::getMember(LangCppVariable* instance,
                         CarbonIdent* memberName,
                         LangCppType* memberType)
{
  LangCppType* type = instance->getType();
  const LangCppClassType* chkType = type->castClass();
  if (! chkType) {
    const LangCppPointerType* pType = type->castPointerType();
    if (pType)
    {
      chkType = pType->getPointed()->castClass();
    }
  }
  FUNC_ASSERT(chkType, sExpectedClassType(type));
  
  LangCppMemberVariable* var = 
    new LangCppMemberVariable(memberType, 
                              memberName,
                              instance);
  addExpr(var);
  return var;
}

LangCppFuncCall* 
LangCppScope::callFunction(const char* functionName,
                           LangCppExpr* paramList[])
{
  LangCppFuncCall* expr = new LangCppFuncCall(functionName, paramList);
  addExpr(expr);
  return expr;
}

void LangCppScope::addExpr(LangCppExpr* expr)
{
  mSavedExprs.push_back(expr);
}

LangCppUserExpr* LangCppScope::createUserExpr(const char* exprStr)
{
  LangCppUserExpr* userExpr = new LangCppUserExpr(exprStr);
  addExpr(userExpr);
  return userExpr;
}

LangCppCarbonExpr* LangCppScope::createCarbonExpr(CarbonExpr* expr, const LangCppVariableArray& refVars)
{
  LangCppCarbonExpr* cppExpr = new LangCppCarbonExpr(expr, refVars);
  addExpr(cppExpr);
  return cppExpr;
}


LangCppGlobal::LangCppGlobal() :
  LangCppScope(NULL)
{
  mVoid = new LangCppPodType("void", 0, LangCppType::eUnsigned);
  mBool = new LangCppPodType("bool", 1, LangCppType::eUnsigned);
  mUChar = new LangCppPodType("unsigned char", 1, LangCppType::eUnsigned);
  mSChar = new LangCppPodType("char", 1, LangCppType::eSigned);
}

LangCppGlobal::~LangCppGlobal()
{
  delete mVoid;
  delete mBool;
  delete mUChar;
  delete mSChar;
}

void LangCppGlobal::emit(UtOStream&, UInt32) const
{
  INFO_ASSERT(0, "Attempting to emit the global scope.");
}

void LangCppGlobal::visit(LangCppWalker*)
{
  INFO_ASSERT(0, "Attempting to visit the global scope.");
}

LangCppFileScope::LangCppFileScope(LangCppGlobal* globalScope)
  : LangCppScope(globalScope), mGlobalScope(globalScope)
{}

LangCppFileScope::~LangCppFileScope()
{}

static void sFuncAlreadyAdded(const char* funcName)
{
  UtString msg;
  msg << "Function '" << funcName << "' already added.";
  UtIO::cout() << msg << UtIO::endl;
}

LangCppFunction* 
LangCppFileScope::addFunction(const char* funcName, LangCppFunction::Linkage linkage, 
                              LangCppType* returnType)
{
  UtString funcNameStr(funcName);
  FUNC_ASSERT(mScopeNames.insertWithCheck(funcNameStr), sFuncAlreadyAdded(funcName));

  LangCppFunction* ret = new LangCppFunction(funcNameStr, linkage, returnType, this);
  addChildScope(ret);
  return ret;
}

void LangCppFileScope::visit(LangCppWalker* walker)
{
  if (! walker->preVisitFileScope(this))
    return;

  LangCppScope::visit(walker);
}

LangCppFor::LangCppFor(LangCppScope* parent)
  : LangCppScope(parent), mInitClause(NULL), 
    mCond(NULL), mPostClause(NULL)
{
  mSubScope = addScope();
}

LangCppFor::~LangCppFor()
{}

void LangCppFor::putInitClause(LangCppStatement* assign)
{
  mInitClause = assign;
}

void LangCppFor::putLoopConditional(LangCppExpr* cond)
{
  mCond = cond;
}

void LangCppFor::putPostClause(LangCppStatement* stmt)
{
  mPostClause = stmt;
}

void LangCppFor::emit(UtOStream& out, UInt32 indent) const
{
  /*
    This is kind of ugly, but will have to do. newlines are added
    after every statement. So the loop clause spans (at least) three
    lines, with the last line being a simple close parenthesis.
  */

  out << "for(";
  // Make line additions within the loop clause start within the
  // parentheses.
  indent += 4;
  if (mInitClause)
  {
    FUNC_ASSERT(mInitClause->castScope() == NULL, mInitClause->emit(UtIO::cout(), 0));
    mInitClause->emit(out, 0);
    sEmitIndent(out, indent);
  }
  else 
    out << "; ";
      
  if (mCond)
    mCond->emit(out);
  out << "; ";

  if (mPostClause)
  {
    FUNC_ASSERT(mPostClause->castScope() == NULL, mPostClause->emit(UtIO::cout(), 0));
    // This is nasty, but I don't really want to change all emit
    // functions to satisfy a loop clause exception. So, the statement
    // outputs a semicolon and a newline. We need to remove the
    // semicolon. So, we'll emit into a UtStringStream and blank the
    // semicolon before writing it out.
    UtString buf;
    UtOStringStream bufStream(&buf);
    mPostClause->emit(bufStream, indent);
    size_t pos = buf.find_last_of(';');
    if (pos != UtString::npos)
      buf[pos] = ' ';
    out << buf;
  }
  else 
    out << ";" << UtIO::endl;
  
  --indent; // Line up the ( and the )
  sEmitIndent(out, indent);
  out << ")" << UtIO::endl;
  indent -= 3;
  sEmitIndent(out, indent);
  mSubScope->emit(out, indent);
}

void LangCppFor::visit(LangCppWalker* walker)
{
  if (! walker->preVisitFor(this))
    return;

  // visit the init, cond, and post clauses
  walker->visitStmt(mInitClause);
  walker->visitExpr(mCond);
  walker->visitStmt(mPostClause);

  // now visit the subscope
  walker->visitStmt(mSubScope);
}

LangCppFunction::LangCppFunction(const UtString& name, 
                                 Linkage linkage, 
                                 LangCppType* retType,
                                 LangCppScope* parent)
  : LangCppScope(parent), mFuncName(name), mLinkage(linkage), mAttribs(eAttribNone), mReturnType(retType)
{}

LangCppFunction::~LangCppFunction()
{}

const char* LangCppFunction::getLinkageStr() const
{
  switch (mLinkage)
  {
  case eStatic: return "static "; break;
  case eGlobal: return ""; break; 
  }
  return "";
}

LangCppVariable* LangCppFunction::declareParameter(LangCppType* paramType, CarbonIdent* param)
{
  LangCppVariable* ret = findVariableInScope(param);
  FUNC_ASSERT(ret == NULL, sVarPrevDeclAs(param, ret));
  // do not call declareVariable here as that looks at the fileScope
  // as well. A function's parameters overrides filescope variables.
  ret = new LangCppVariable(paramType, param);
  ret->addAttribute(LangCppVariable::eAttribNoDeclare);
  addVariable(ret);
  ret->incrRead();
  mParameters.push_back(ret);
  return ret;
}

void LangCppFunction::addAttribute(Attrib flag)
{
  mAttribs = Attrib(mAttribs | flag);
}

void LangCppFunction::emit(UtOStream& out, UInt32 indent) const
{
  // Output the function declaration, then pass the emit duty to the
  // scope base class.
  
  // Extra newline before emitting the function
  out << UtIO::endl;
  sEmitIndent(out, indent);
  out << getLinkageStr();
  if ((mAttribs & eAttribInline) != 0)
    out << " inline ";
  mReturnType->emitDeclare(out);
  out << " " << mFuncName << "(";
  CommaClosure commaMgr(mParameters.size());
  for (LangCppVariableArray::const_iterator p = mParameters.begin(), 
         e = mParameters.end(); p != e; ++p)
  {
    commaMgr.emit(out);
    const LangCppVariable* var = *p;
    var->emitDeclare(out);
  }
  out << ")" << UtIO::endl;
  sEmitIndent(out, indent);
  LangCppScope::emit(out, indent);
  // Extra newline at the end of a function
  out << UtIO::endl;
}

void LangCppFunction::visit(LangCppWalker* walker)
{
  if (! walker->preVisitFunction(this))
    return;
  
  // visit this scope
  LangCppScope::visit(walker);
}

LangCppIf::LangCppIf(LangCppExpr* condition, LangCppScope* parent)
  : LangCppScope(parent), mCondition(condition), mElseScope(NULL)
{
  mCondition->incrRead();
  mSubScope = addScope();
}

LangCppIf::~LangCppIf()
{
}

void LangCppIf::emit(UtOStream& out, UInt32 indent) const
{
  // Output the conditional, then emit the subscope
  out << "if (";
  mCondition->emit(out);
  out << ")" << UtIO::endl;
  sEmitIndent(out, indent);
  mSubScope->emit(out, indent);
  
  // emit the else-ifs
  for (LangCppScopeArray::const_iterator p = mElseIfs.begin(),
         e = mElseIfs.end(); p != e; ++p)
  {
    const LangCppScope* scope = *p;
    sEmitIndent(out, indent);
    scope->emit(out, indent);
  }

  // emit the else
  if (mElseScope)
  {
    sEmitIndent(out, indent);
    mElseScope->emit(out, indent);
  }
}

void LangCppIf::visit(LangCppWalker* walker)
{
  if (! walker->preVisitIf(this))
    return;
  
  // visit the condition
  walker->visitExpr(mCondition);
  
  // Visit true subscope
  walker->visitStmt(mSubScope);

  // Visit the else ifs
  for (LangCppScopeArray::iterator p = mElseIfs.begin(),
         e = mElseIfs.end(); p != e; ++p)
  {
    LangCppScope* scope = *p;
    walker->visitStmt(scope);
  }

  // Visit the else scope, if it exists
  if (mElseScope)
    walker->visitStmt(mElseScope);
}

LangCppElseIf* LangCppIf::addElseIf(LangCppExpr* condition)
{
  LangCppElseIf* elseIf = new LangCppElseIf(condition, this);
  addChildScope(elseIf);
  mElseIfs.push_back(elseIf);
  return elseIf;
}

LangCppElse* LangCppIf::addElse()
{
  INFO_ASSERT(mElseScope == NULL, "else-scope already attached to if scope");
  mElseScope = new LangCppElse(this);
  addChildScope(mElseScope);
  return mElseScope;
}


LangCppElseIf::LangCppElseIf(LangCppExpr* condition, LangCppScope* parent) 
  : LangCppIf(condition, parent)
{}

LangCppElseIf::~LangCppElseIf()
{}

void LangCppElseIf::emit(UtOStream& out, UInt32 indent) const
{
  out << "else ";
  LangCppIf::emit(out, indent);
}

void LangCppElseIf::visit(LangCppWalker* walker)
{
  if (! walker->preVisitElseIf(this))
    return;
  
  //! visit the if statement
  LangCppIf::visit(walker);
}

LangCppElseIf* LangCppElseIf::addElseIf(LangCppExpr*)
{
  INFO_ASSERT(0, "Cannot add else-if to else-if scope");
  return NULL;
}

LangCppElse* LangCppElseIf::addElse()
{
  INFO_ASSERT(0, "Cannot add else to else-if scope");
  return NULL;
}

LangCppElse::LangCppElse(LangCppScope* parent) 
  : LangCppScope(parent)
{}

LangCppElse::~LangCppElse()
{}

void LangCppElse::emit(UtOStream& out, UInt32 indent) const
{
  out << "else" << UtIO::endl;
  sEmitIndent(out, indent);
  LangCppScope::emit(out, indent);
}

void LangCppElse::visit(LangCppWalker* walker)
{
  if (! walker->preVisitElse(this))
    return;
  
  //! visit this scope
  LangCppScope::visit(walker);
}

LangCppExpr::LangCppExpr() 
  : mSign(LangCppType::eUnsigned)
{}

LangCppExpr::~LangCppExpr()
{}

void LangCppExpr::incrRead() {  }
void LangCppExpr::incrWrite() {  }
void LangCppExpr::decrRead() {  }
void LangCppExpr::decrWrite() {  }

UInt32 LangCppExpr::numReads() const { return 0; }
UInt32 LangCppExpr::numWrites() const { return 0; }
UInt32 LangCppExpr::refCnt() const { return 0; }

CarbonExpr* LangCppExpr::getExpr() const { return NULL; }

LangCppVariable::LangCppVariable(LangCppType* type, CarbonIdent* identifier)
  : LangCppCarbonExpr(identifier),  
    mIdentifier(identifier), mType(type), mReadCnt(0), mWriteCnt(0),
    mAttributes(eAttribNone)
{
  addVariable(this);
}

LangCppVariable::~LangCppVariable()
{}

void LangCppVariable::incrRead()
{
  ++mReadCnt;
}
 
void LangCppVariable::incrWrite()
{
  ++mWriteCnt;
}

void LangCppVariable::decrRead()
{
  INFO_ASSERT(mReadCnt != 0, "Underflow decrementing read reference.");
  --mReadCnt;
}
 
void LangCppVariable::decrWrite()
{
  INFO_ASSERT(mWriteCnt != 0, "Underflow decrementing write reference.");
  --mWriteCnt;
}

UInt32 LangCppVariable::numReads() const
{
  return mReadCnt;
}

UInt32 LangCppVariable::numWrites() const
{
  return mWriteCnt;
}

UInt32 LangCppVariable::refCnt() const
{
  return mWriteCnt + mReadCnt;
}

void LangCppVariable::addAttribute(Attrib flag)
{
  // Do not mark a class type as register.
  CE_ASSERT(! (((flag & eAttribRegister) != 0) && (mType->castClass() != NULL)), mIdentifier);
  mAttributes = Attrib(mAttributes | flag);
}

void LangCppVariable::compose(UtString* msg) const
{
  mType->compose(msg);
  *msg << " ";
  UtIndent composer(msg);
  composer.clearLastLinePos();
  mIdentifier->composeC(&composer);
}

void LangCppVariable::emitDeclare(UtOStream& out) const
{
  if (isDeclaredRegister())
    out << "register ";
  mType->emitDeclare(out, this);
  if (! mConstructParams.empty())
  {
    out << "(";
    CommaClosure commaMgr(mConstructParams.size());
    for (LangCppExprArray::const_iterator p = mConstructParams.begin(), 
           e = mConstructParams.end(); p != e; ++p)
    {
      commaMgr.emit(out);
      LangCppExpr* expr = *p;
      expr->emit(out);
    }
    out << ")";
  }
  if (isDeclaredUnused())
    out << " UNUSED";
}

void LangCppVariable::addConstructParam(LangCppExpr* param)
{
  mConstructParams.push_back(param);
  param->incrRead();
  param->incrWrite();
}

void LangCppVariable::emit(UtOStream& out) const
{
  DynBitVector dummy;
  const STAliasedLeafNode* node = mIdentifier->getNode(&dummy);
  CE_ASSERT(node, mIdentifier);
  out << node->str();
}

void LangCppVariable::visit(LangCppWalker* walker)
{
  walker->visitVariable(this);
  // Call the walker with this as a carbon expr as well in case we are
  // looking for all carbon exprs in a walk.
  LangCppCarbonExpr::visit(walker);
}

LangCppClassType* LangCppVariable::getClassType()
{
  LangCppClassType* classType = mType->castClass();
  INFO_ASSERT(classType, "Not a class type.");
  return classType;
}

LangCppMemberVariable::LangCppMemberVariable(LangCppType* type, 
                                             CarbonIdent* ident, 
                                             LangCppVariable* classInstance)
  : LangCppVariable(type, ident), mClassInstance(classInstance)
{
  mClassInstance->incrRead();
  mClassInstance->incrWrite();
}

LangCppMemberVariable::~LangCppMemberVariable()
{}

void LangCppMemberVariable::emit(UtOStream& out) const
{
  mClassInstance->emit(out);
  LangCppType* type = mClassInstance->getType();
  type->emitRefOperator(out); // -> or .
  LangCppVariable::emit(out);
}

void LangCppMemberVariable::visit(LangCppWalker* walker)
{
  if (! walker->preVisitMemberVariable(this))
    return;
  
  // Visit the class instance
  walker->visitExpr(mClassInstance);
  // Visit this variable
  LangCppVariable::visit(walker);
}

LangCppFuncCallStmt::LangCppFuncCallStmt(LangCppFuncCall* call)
  : mCall(call)
{
  mCall->incrRead();
  mCall->incrWrite();
}

LangCppFuncCallStmt::~LangCppFuncCallStmt()
{
}

void LangCppFuncCallStmt::emit(UtOStream& out, UInt32) const
{
  mCall->emit(out);
  out << ";" << UtIO::endl;
}

void LangCppFuncCallStmt::visit(LangCppWalker* walker)
{
  if (! walker->preVisitFuncCallStmt(this))
    return;
  
  // Visit the function call
  walker->visitExpr(mCall);
  
  // Visit this statement
  walker->visitFuncCallStmt(this);
}

LangCppMemCpy::LangCppMemCpy(LangCppVariable* dest, LangCppVariable* src,
                             UInt32 numBytes)
  : mDest(dest), mSrc(src), mNumBytes(numBytes)
{
  mDest->incrWrite();
  mSrc->incrRead();
}

LangCppMemCpy::~LangCppMemCpy()
{}

void LangCppMemCpy::emit(UtOStream& out, UInt32) const
{
  out << "memcpy(";
  ConstantRange range;
  if (! mDest->getType()->castPointerType() &&
      ! mDest->getIdent()->getDeclaredRange(&range))
    out << "&";
  mDest->emit(out);
  out << ", ";
  if (! mSrc->getType()->castPointerType() &&
      ! mSrc->getIdent()->getDeclaredRange(&range))
    out << "&";
  mSrc->emit(out);
  out << ", " << mNumBytes << ");" << UtIO::endl;
}

void LangCppMemCpy::visit(LangCppWalker* walker)
{
  if (! walker->preVisitMemCpy(this))
    return;
  
  // visit the destination and then the source
  walker->visitExpr(mDest);
  walker->visitExpr(mSrc);

  // Now visit this memcpy
  walker->visitMemCpy(this);
}

LangCppMemSet::LangCppMemSet(LangCppVariable* dest, LangCppVariable* val,
                             UInt32 numBytes)
  : mDest(dest), mVal(val), mNumBytes(numBytes)
{
  mDest->incrWrite();
  mVal->incrRead();
}

LangCppMemSet::~LangCppMemSet()
{}

void LangCppMemSet::emit(UtOStream& out, UInt32) const
{
  out << "std::memset(";
  ConstantRange range;
  if (! mDest->getType()->castPointerType() &&
      ! mDest->getIdent()->getDeclaredRange(&range))
    out << "&";
  mDest->emit(out);
  out << ", ";
  if (mVal->getType()->castPointerType() ||
      mVal->getIdent()->getDeclaredRange(&range))
    out << "*";
  mVal->emit(out);
  out << ", " << mNumBytes << ");" << UtIO::endl;
}


void LangCppMemSet::visit(LangCppWalker* walker)
{
  if (! walker->preVisitMemSet(this))
    return;
  
  // visit the destination and then the source
  walker->visitExpr(mDest);
  walker->visitExpr(mVal);

  // now visit this memset
  walker->visitMemSet(this);
}

LangCppReturn::LangCppReturn(LangCppExpr* returnAction) :
  mReturnAction(returnAction)
{}
LangCppReturn::~LangCppReturn()
{}

void LangCppReturn::emit(UtOStream& out, UInt32) const
{
  out << "return";
  if (mReturnAction)
  {
    out << " ";
    mReturnAction->emit(out);
  }
  out << ";" << UtIO::endl;
}

void LangCppReturn::visit(LangCppWalker* walker)
{
  if (! walker->preVisitReturn(this))
    return;
  
  // visit the return expression, if available
  if (mReturnAction)
    walker->visitExpr(mReturnAction);
  
  // now visit this return
  walker->visitReturn(this);
}

LangCppIfDef::LangCppIfDef(const char* ifCond) 
  : mIfCond(ifCond)
{}

LangCppIfDef::~LangCppIfDef()
{}

void LangCppIfDef::emit(UtOStream& out, UInt32) const
{
  out << UtIO::endl;
  out << "#ifdef " << mIfCond << UtIO::endl;
}

void LangCppIfDef::visit(LangCppWalker* walker)
{
  walker->visitIfDef(this);
}
  
LangCppEndIf::LangCppEndIf()
{}

LangCppEndIf::~LangCppEndIf()
{}

void LangCppEndIf::emit(UtOStream& out, UInt32) const
{
  out << UtIO::endl;
  out << "#endif" << UtIO::endl;
}

void LangCppEndIf::visit(LangCppWalker* walker)
{
  walker->visitEndIf(this);
}

LangCppAssign::LangCppAssign(LangCppExpr* lhs, LangCppExpr* rhs)
{
  lhs->incrWrite();
  rhs->incrRead();
  mLhs = lhs;
  mRhs = rhs;
}

LangCppAssign::~LangCppAssign()
{
}

void LangCppAssign::emit(UtOStream& out, UInt32) const
{
  mLhs->emit(out);
  out << " = ";
  mRhs->emit(out);
  out << ";" << UtIO::endl;
}

void LangCppAssign::visit(LangCppWalker* walker)
{
  if (! walker->preVisitAssign(this))
    return;

  // visit lhs and rhs.
  walker->visitExpr(mLhs);
  walker->visitExpr(mRhs);
  
  // now visit this
  walker->visitAssign(this);
}

LangCppDeclAssign::LangCppDeclAssign(LangCppVariable* lhs, LangCppExpr* rhs) :
  LangCppAssign(lhs, rhs), mVariable(lhs)
{
}

LangCppDeclAssign::~LangCppDeclAssign()
{}

void LangCppDeclAssign::emit(UtOStream& out, UInt32) const
{
  mVariable->emitDeclare(out);
  out << " = ";
  mRhs->emit(out);
  out << ";" << UtIO::endl;
}

void LangCppDeclAssign::visit(LangCppWalker* walker)
{
  if (! walker->preVisitDeclAssign(this))
    return;

  // now visit this assign
  LangCppAssign::visit(walker);
}

LangCppOrAssign::LangCppOrAssign(LangCppExpr* lhs, LangCppExpr* rhs)
  : LangCppAssign(lhs, rhs)
{}

LangCppOrAssign::~LangCppOrAssign()
{}

void LangCppOrAssign::emit(UtOStream& out, UInt32) const
{
  mLhs->emit(out);
  out << " |= ";
  mRhs->emit(out);
  out << ";" << UtIO::endl;
}

void LangCppOrAssign::visit(LangCppWalker* walker)
{
  if (! walker->preVisitOrAssign(this))
    return;

  // now visit this assign
  LangCppAssign::visit(walker);
}

LangCppAndAssign::LangCppAndAssign(LangCppExpr* lhs, LangCppExpr* rhs)
  : LangCppAssign(lhs, rhs)
{}

LangCppAndAssign::~LangCppAndAssign()
{}

void LangCppAndAssign::emit(UtOStream& out, UInt32) const
{
  mLhs->emit(out);
  out << " &= ";
  mRhs->emit(out);
  out << ";" << UtIO::endl;
}

void LangCppAndAssign::visit(LangCppWalker* walker)
{
  if (! walker->preVisitAndAssign(this))
    return;

  // now visit this assign
  LangCppAssign::visit(walker);
}

LangCppPrefixIncrement::LangCppPrefixIncrement(LangCppVariable* var) :
  LangCppAssign(var, var), mVariable(var)
{}

LangCppPrefixIncrement::~LangCppPrefixIncrement()
{}
  
void LangCppPrefixIncrement::emit(UtOStream& out, UInt32) const
{
  out << "++";
  mVariable->emit(out);
  out << ";" << UtIO::endl;
}

void LangCppPrefixIncrement::visit(LangCppWalker* walker)
{
  if (! walker->preVisitPrefixIncrement(this))
    return;

  // Visit this assign
  LangCppAssign::visit(walker);
}

LangCppStaticCast::LangCppStaticCast(LangCppType* castTo, LangCppType* castFrom, LangCppExpr* expr)
  : mCastTo(castTo), mCastFrom(castFrom), mExpr(expr)
{
  mExpr->incrRead();
}

LangCppStaticCast::~LangCppStaticCast()
{
}

void LangCppStaticCast::emit(UtOStream& out) const
{
  // if we are casting from a pointer to a non-pointer, dereference
  // if we are casting from a non-pointer to a pointer, take addr
  LangCppVoidPointer* castToP = mCastTo->castVoidP();
  LangCppVoidPointer* castFromP = mCastFrom->castVoidP();
  bool inParen = false;
  bool castToPointerFirst = false;
  if (castToP && ! castFromP)
  {
    out << "&(";
    inParen = true;
  }
  else if (! castToP && castFromP)
  {
    out << "*(";
    inParen = true;
    castToPointerFirst = true;
  }
  
  out << "static_cast<";
  out << mCastTo->getTypeNameStr();
  if (castToPointerFirst)
    out << "*";
  out << ">(";
  mExpr->emit(out);
  out << ")";

  if (inParen)
    out << ")";
}

void LangCppStaticCast::visit(LangCppWalker* walker)
{
  if (! walker->preVisitStaticCast(this))
    return;

  // visit the expression that is being casted
  walker->visitExpr(mExpr);
  
  // Now visit this
  walker->visitStaticCast(this);
}

void LangCppStaticCast::incrRead()
{
  mExpr->incrRead();
}
void LangCppStaticCast::decrRead()
{
  mExpr->decrRead();
}
void LangCppStaticCast::incrWrite()
{
  mExpr->incrWrite();
}
void LangCppStaticCast::decrWrite()
{
  mExpr->decrWrite();
}


LangCppFuncCall::LangCppFuncCall(const char* funcName,
                                 LangCppExpr* paramList[])
  : mCall(funcName)
{
  if (paramList) {
    for (; *paramList != NULL; ++paramList)
      passParam(*paramList);
  }
}

LangCppFuncCall::~LangCppFuncCall()
{}

void LangCppFuncCall::passParam(LangCppExpr* param)
{
  mParameters.push_back(param);
  param->incrRead();
  param->incrWrite();
}

void LangCppFuncCall::emit(UtOStream& out) const
{
  out << mCall << "(";
  CommaClosure commaMgr(mParameters.size());
  for (LangCppExprArray::const_iterator p = mParameters.begin(), 
         e = mParameters.end(); p != e; ++p)
  {
    commaMgr.emit(out);
    const LangCppExpr* expr = *p;
    expr->emit(out);
  }
  out << ")";
}

void LangCppFuncCall::visit(LangCppWalker* walker)
{
  if (! walker->preVisitFuncCall(this))
    return;

  // Walk all the parameters
  for (LangCppExprArray::iterator p = mParameters.begin(), 
         e = mParameters.end(); p != e; ++p)
  {
    LangCppExpr* expr = *p;
    walker->visitExpr(expr);
  }

  // Now visit this
  walker->visitFuncCall(this);
}

LangCppMethodCall::LangCppMethodCall(const char* funcName, 
                                     LangCppVariable* instance,
                                     LangCppExpr* paramList[])
  : LangCppFuncCall(funcName, paramList), mInstance(instance)
{}

void LangCppMethodCall::emit(UtOStream& out) const
{
  mInstance->emit(out);
  LangCppType* type = mInstance->getType();
  type->emitRefOperator(out); // -> or .
  LangCppFuncCall::emit(out);
}

void LangCppMethodCall::visit(LangCppWalker* walker)
{
  if (! walker->preVisitMethodCall(this))
    return;

  // Walk this instance
  walker->visitExpr(mInstance);

  // Visit this func call
  LangCppFuncCall::visit(walker);
}

LangCppMethodCall::~LangCppMethodCall()
{}

LangCppDereference::LangCppDereference(LangCppExpr* expr)
  : mExpr(expr)
{
  mExpr->incrRead();
}

LangCppDereference::~LangCppDereference()
{
}

void LangCppDereference::emit(UtOStream& out) const
{
  out << "*(";
  mExpr->emit(out);
  out << ")";
}

void LangCppDereference::visit(LangCppWalker* walker)
{
  if (! walker->preVisitDereference(this))
    return;

  // visit the expression that is being casted
  walker->visitExpr(mExpr);
  
  // Now visit this
  walker->visitDereference(this);
}

void LangCppDereference::incrRead()
{
  mExpr->incrRead();
}
void LangCppDereference::decrRead()
{
  mExpr->decrRead();
}
void LangCppDereference::incrWrite()
{
  mExpr->incrWrite();
}
void LangCppDereference::decrWrite()
{
  mExpr->decrWrite();
}


LangCppUserExpr::LangCppUserExpr(const char* str)
  : mStr(str)
{}

LangCppUserExpr::~LangCppUserExpr()
{}

void LangCppUserExpr::emit(UtOStream& out) const
{
  out << mStr;
}

void LangCppUserExpr::visit(LangCppWalker* walker)
{
  walker->visitUserExpr(this);
}


LangCppUserStatement::LangCppUserStatement(const UtString& statementStr)
  : mStatement(statementStr)
{}

LangCppUserStatement::~LangCppUserStatement()
{}

void LangCppUserStatement::emit(UtOStream& out, UInt32) const
{
  // do not emit an indent with a user statement (might be an ifdef)
  out << UtIO::endl;
  out << mStatement << UtIO::endl;
}

void LangCppUserStatement::visit(LangCppWalker* walker)
{
  walker->visitUserStatement(this);
}

LangCppCarbonExpr::LangCppCarbonExpr(CarbonExpr* expr, 
                                     const LangCppVariableArray& referencedVars)
  : mExpr(expr), mVariables(referencedVars)
{
  incrRead();  
}

// special constructor for LangCppVariable
LangCppCarbonExpr::LangCppCarbonExpr(CarbonExpr* expr) 
  : mExpr(expr)
{}

LangCppCarbonExpr::~LangCppCarbonExpr()
{}

// LangCppVariable needs to add itself to the list
// Cannot do this using the constructor because Windows complains that
// 'this' is in the base member initialization list
void LangCppCarbonExpr::addVariable(LangCppVariable* var)
{
  mVariables.push_back(var);
}

void LangCppCarbonExpr::incrRead()
{
  for (LangCppVariableArray::iterator p = mVariables.begin(),
         e = mVariables.end(); p != e; ++p)
  {
    LangCppVariable* var = *p;
    var->incrRead();
  }
}

void LangCppCarbonExpr::incrWrite()
{
  for (LangCppVariableArray::iterator p = mVariables.begin(),
         e = mVariables.end(); p != e; ++p)
  {
    LangCppVariable* var = *p;
    var->incrWrite();
  }
}

void LangCppCarbonExpr::decrRead()
{
  for (LangCppVariableArray::iterator p = mVariables.begin(),
         e = mVariables.end(); p != e; ++p)
  {
    LangCppVariable* var = *p;
    var->decrRead();
  }
}

void LangCppCarbonExpr::decrWrite()
{
  for (LangCppVariableArray::iterator p = mVariables.begin(),
         e = mVariables.end(); p != e; ++p)
  {
    LangCppVariable* var = *p;
    var->decrWrite();
  }
}

CarbonExpr* LangCppCarbonExpr::getExpr() const 
{
  return mExpr;
}

void LangCppCarbonExpr::emit(UtOStream& out) const
{
  UtString buf;
  UtIndent composer(&buf);
  // no need to clearLastLinePos() here, since the buf is empty.
  mExpr->composeC(&composer);
  out << buf;
}

void LangCppCarbonExpr::visit(LangCppWalker* walker)
{
  walker->visitCarbonExpr(this);
}
