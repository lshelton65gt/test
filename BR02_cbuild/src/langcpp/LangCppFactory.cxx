// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "langcpp/LangCppFactory.h"
#include "exprsynth/SymTabExpr.h"
#include "util/AtomicCache.h"
#include "symtab/STSymbolTable.h"

//! Class to find LangCppVariables in CarbonExprs
class LangCppFactory::IdentFinder : public CarbonExprWalker
{
public:
  CARBONMEM_OVERRIDES

  IdentFinder(LangCppScope* scope, LangCppVariableArray* varArray) 
    : mCppScope(scope), mVarArray(varArray)
  {}
  
  virtual ~IdentFinder() {}

  virtual void visitIdent(CarbonIdent* ident) { 
    LangCppVariable* var = mCppScope->findVariable(ident);
    // We may have an internal variable that we didn't explicitly
    // declare in the LangCpp system.
    if (var)
      mVarArray->push_back(var);
  }
  
private:
  LangCppScope* mCppScope;
  LangCppVariableArray* mVarArray;
}; // IdentFinder


LangCppFactory::LangCppFactory()
{
  mAtomicCache = new AtomicCache;
  mSymTab = new STSymbolTable(&mFieldBOM, mAtomicCache);
  mSymTab->setHdlHier(&mHdlPath);
  mConstantRangeFactory = new ConstantRangeFactory;
}

LangCppFactory::~LangCppFactory()
{
  delete mSymTab;
  delete mAtomicCache;
  delete mConstantRangeFactory;
}

CarbonIdent* LangCppFactory::createIdent(const char* name, UInt32 byteSize)
{
  STAliasedLeafNode* nameNode = mSymTab->createLeaf(mAtomicCache->intern(name), NULL);
  
  SymTabIdent* nameIdent = new SymTabIdent(nameNode, byteSize * 8);

  bool added;
  CarbonIdent* ident = mExprFactory.createIdent(nameIdent, added);
  if (! added)
    delete nameIdent;
  return ident;
}

CarbonIdent* LangCppFactory::createArrayIdent(const char* name, UInt32 numElems, UInt32 elemByteSize)
{
  STAliasedLeafNode* nameNode = mSymTab->createLeaf(mAtomicCache->intern(name), NULL);

  // The number of elements can be zero if a design has no primary
  // inputs.
  if (numElems == 0)
    numElems = 1; // Don't declare a var with 0 bitsize.
  SymTabIdent* nameIdent = new SymTabIdent(nameNode, numElems * elemByteSize * 8);

  // Create the vector range
  const ConstantRange* range = mConstantRangeFactory->find(numElems - 1, 0);
  nameIdent->putDeclaredRange(range);
  
  bool added;
  CarbonIdent* ident = mExprFactory.createIdent(nameIdent, added);
  if (! added)
    delete nameIdent;
  return ident;
}


CarbonExpr* 
LangCppFactory::createIdentBitsel(CarbonIdent* ident, SInt32 bitIndex, UInt32 byteSize)
{
  CarbonConst* bitIndexExpr = mExprFactory.createConst(bitIndex, 32, false);
  return mExprFactory.createBinaryOp(CarbonExpr::eBiBitSel, ident, bitIndexExpr, byteSize, false, false);
}

CarbonExpr*
LangCppFactory::createLogicalNot(CarbonExpr* expr)
{
  // Damn it. Someone added createZeroPad and used it on all constant
  // expressions. This screws up c++ emitting. So, I have to be very
  // careful. If I make the bitsize 1, the pad won't happen. Ugh.
  return mExprFactory.createEqZero(expr, 1);
}


void LangCppFactory::gatherCppVars(CarbonExpr* singleExpr, LangCppScope* scope, LangCppVariableArray* cppVars)
{
  IdentFinder walk(scope, cppVars);
  walk.visitExpr(singleExpr);
}

LangCppCarbonExpr* LangCppFactory::createLangCppExpr(CarbonExpr* expr, LangCppScope* curScope)
{
  LangCppVariableArray refVars;
  gatherCppVars(expr, curScope, &refVars);
  return curScope->createCarbonExpr(expr, refVars);
}

LangCppCarbonExpr* 
LangCppFactory::createBinaryOp(CarbonExpr::ExprT binOp,
                               LangCppExpr* expr1, LangCppExpr* expr2, 
                               LangCppScope* scope)
{
  CarbonExpr* carbExpr = expr2->getExpr();
  INFO_ASSERT(carbExpr, "cpp expression is not a carbon expression.");
  return createBinaryOp(binOp, expr1, carbExpr, scope);
}


LangCppCarbonExpr* 
LangCppFactory::createBinaryOp(CarbonExpr::ExprT binOp,
                               LangCppExpr* cppExpr, CarbonExpr* carbExpr,
                               LangCppScope* scope)
{
  CarbonExpr* cppCarbExpr = cppExpr->getExpr();
  INFO_ASSERT(cppCarbExpr, "cpp expression is not a carbon expression.");
  CarbonExpr* currentExpr = mExprFactory.createBinaryOp(binOp, cppCarbExpr, carbExpr, 8, false, false);

  LangCppVariableArray cppVars;
  gatherCppVars(currentExpr, scope, &cppVars);
  LangCppCarbonExpr* ret = new LangCppCarbonExpr(currentExpr, cppVars);
  // Store it in the scope so it will get deleted.
  scope->addExpr(ret);
  return ret;
}

LangCppCarbonExpr*
LangCppFactory::createLogicalNot(LangCppVariable* var, LangCppScope* scope)
{
  CarbonExpr* expr = createLogicalNot(var->getIdent());
  LangCppVariableArray refVars;
  refVars.push_back(var);
  return scope->createCarbonExpr(expr, refVars);
}

LangCppCarbonExpr*
LangCppFactory::createNeqZero(LangCppCarbonExpr* expr, LangCppScope* scope)
{
  CarbonExpr* carbExpr = expr->getCarbonExpr();
  CarbonExpr* carbNeqZero = mExprFactory.createNeqZero(carbExpr, 1);

  LangCppVariableArray refVars;
  gatherCppVars(carbNeqZero, scope, &refVars);
  return scope->createCarbonExpr(carbNeqZero, refVars);
}

LangCppCarbonExpr* 
LangCppFactory::createLessThanInt(LangCppVariable* var, 
                                  SInt32 number,
                                  LangCppScope* scope)
{
  LangCppVariableArray refVars;
  refVars.push_back(var);

  CarbonExpr* numExpr = mExprFactory.createConst(number, 32, (number >= 0) ? false : true);
  CarbonExpr* expr = mExprFactory.createBinaryOp(CarbonExpr::eBiSLt, var->getIdent(), numExpr, 8, false, false);
  return scope->createCarbonExpr(expr, refVars);
}

LangCppCarbonExpr* LangCppFactory::createZeroExpr(UInt32 numBytes, LangCppScope* scope)
{
  CarbonExpr* zeroExpr = mExprFactory.createConst(0, numBytes * 8, false);
  LangCppVariableArray dummy;
  return scope->createCarbonExpr(zeroExpr, dummy);
}

LangCppCarbonExpr* LangCppFactory::createOneExpr(UInt32 numBytes, LangCppScope* scope)
{
  CarbonExpr* oneExpr = mExprFactory.createConst(1, numBytes * 8, false);
  LangCppVariableArray dummy;
  return scope->createCarbonExpr(oneExpr, dummy);
}

LangCppCarbonExpr* LangCppFactory::createIdentBitselExpr(CarbonIdent* ident, SInt32 index, UInt32 byteSize, LangCppScope* scope)
{
  CarbonExpr* expr = createIdentBitsel(ident, index, byteSize);
  return createLangCppExpr(expr, scope);
}

void LangCppFactory::addAssignAlias(LangCppVariable* var, CarbonExpr* varAlias, LangCppScope* scope)
{
  LangCppVariableArray refVars;
  gatherCppVars(varAlias, scope, &refVars); 
  scope->addAssignAlias(var, varAlias, refVars);
}

void LangCppFactory::addAssignAlias(LangCppVariable* var, LangCppCarbonExpr* varAlias, LangCppScope* scope)
{
  scope->addAssignAlias(var, varAlias);
}

void LangCppFactory::addDeclAssignAlias(LangCppVariable* var, 
                                        CarbonExpr* varAlias, 
                                        LangCppScope* scope)
{
  LangCppVariableArray refVars;
  gatherCppVars(varAlias, scope, &refVars); 
  scope->addDeclAssignAlias(var, varAlias, refVars);
}

void LangCppFactory::addDeclAssignAlias(LangCppVariable* var, 
                                        LangCppCarbonExpr* varAlias, 
                                        LangCppScope* scope)
{
  scope->addDeclAssignAlias(var, varAlias);
}

LangCppExpr* LangCppFactory::createSIntExpr(SInt32 param, LangCppScope* scope)
{
  CarbonConst* constIdent = mExprFactory.createConst(param, 32, true);
  return createLangCppExpr(constIdent, scope);
}

LangCppVariable* LangCppFactory::declareVariable(LangCppType* type, const char* varName, 
                                                 LangCppScope* scope)
{
  CarbonIdent* varIdent = createIdent(varName, type->getByteWidth());
  return scope->declareVariable(type, varIdent);
}

void LangCppFactory::addConstructUIntParam(LangCppVariable* var, UInt32 intParam, LangCppScope* scope)
{
  CarbonConst* constIdent = mExprFactory.createConst(intParam, 32, false);
  LangCppExpr* expr = createLangCppExpr(constIdent, scope);
  var->addConstructParam(expr);
}
