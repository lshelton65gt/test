// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "langcpp/LangCppType.h"
#include "langcpp/LangCppStmtTree.h"
#include "util/UtIOStream.h"
#include "util/UtOrder.h"       // For carbonPtrCompare()
#include "exprsynth/Expr.h"

LangCppType::Architecture LangCppType::smArchitecture = LangCppType::eArchSelf;

void LangCppType::sPutArchitecture(Architecture arch)
{
  smArchitecture = arch;
}

SInt32 LangCppType::sGetPointerSize()
{
  switch (smArchitecture)
  {
  case eArch32: return 4; break;
  case eArch64: return 8; break;
  case eArchSelf: return sizeof(CarbonSIntPtr); break;
  }
  INFO_ASSERT(0, "Architecture for LangCppType is invalid");
  return -1;
}

LangCppType::LangCppType(const char* typeName, SignT sign) 
{
  init(typeName, sign);
}

LangCppType::LangCppType(LangCppType* ref) 
{
  init(ref->mTypeName.c_str(), ref->mSign);
}

LangCppType::~LangCppType()
{
  delete mReference;
  delete mPointer;
}

void LangCppType::init(const char* typeName, SignT sign)
{
  mTypeName = typeName;
  mReference =NULL;
  mPointer = NULL;
  mSign = sign;
}

const char* LangCppType::getSignStr() const
{
  switch (mSign) {
  case eUnsigned: return "unsigned"; break;
  case eSigned: return "signed"; break;
  }
  return "unknown";
}

LangCppReferenceType* LangCppType::reference()
{
  if (! mReference)
    mReference = new LangCppReferenceType(this);

  return mReference;
}

LangCppPointerType* LangCppType::pointer()
{
  if (! mPointer)
    mPointer = new LangCppPointerType(this);
  
  return mPointer;
}

const LangCppClassType* LangCppType::castClass() const {
  return NULL;
}

const LangCppPodType* LangCppType::castPod() const {
  return NULL;
}

const LangCppVoidPointer* LangCppType::castVoidP() const {
  return NULL;
}

const LangCppPointerType* LangCppType::castPointerType() const
{
  return NULL;
}


const LangCppReferenceType* LangCppType::castReference() const
{
  return NULL;
}

ptrdiff_t LangCppType::compare(const LangCppType* otherType) const
{
  // first off, if the two pointers are equal then they are the same.
  if (this == otherType)
    return 0;

  // ok, they might be the same, but we need to calculate.
  ptrdiff_t ret = mTypeName.compare(otherType->mTypeName);
  if (ret == 0)
    ret = mSign - otherType->mSign;
  if (ret == 0)
    ret = compareHelper(otherType);
  return ret;
}

static void sTypeNotAClass(const UtString& msg)
{
  UtIO::cout() << msg << ": Not a class type." << UtIO::endl;
}

void LangCppType::emitRefOperator(UtOStream&) const
{
  UtString msg; 
  compose(&msg);
  FUNC_ASSERT(0, sTypeNotAClass(msg));
}

void LangCppType::emitUse(UtOStream& out) const
{
  out << mTypeName;
}

void LangCppType::emitDeclare(UtOStream& out, const LangCppVariable* var) const
{
  emitUse(out);
  out << " ";
  var->emit(out);

  const CarbonIdent* ident = var->getIdent();
  ConstantRange range;
  if (ident->getDeclaredRange(&range))
    out << "[" << range.getLength() << "]";
}

void LangCppType::emitDeclare(UtOStream& out) const
{
  emitUse(out);
}

LangCppVoidPointer::LangCppVoidPointer()
  : LangCppType("void", eSigned)
{}

LangCppVoidPointer::~LangCppVoidPointer()
{}

const LangCppVoidPointer* LangCppVoidPointer::castVoidP() const
{
  return this;
}

SInt32 LangCppVoidPointer::getByteWidth() const
{
  // Return the size of a pointer for this architecture.
  return sGetPointerSize();
}

void LangCppVoidPointer::compose(UtString* msg) const
{
  *msg << "'" << mTypeName << "*'";
}

void LangCppVoidPointer::emitUse(UtOStream& out) const
{
  out << mTypeName << "*";
}

void LangCppVoidPointer::emitTypeName(UtOStream& out) const
{
  out << mTypeName;
}


void LangCppVoidPointer::emitDeclare(UtOStream& out) const
{
  emitTypeName(out);
}

void LangCppVoidPointer::emitDeclare(UtOStream& out, const LangCppVariable* var) const
{
  emitUse(out);
  out << " ";
  var->emit(out);
}


ptrdiff_t LangCppVoidPointer::compareHelper(const LangCppType* otherType) const
{
  const LangCppVoidPointer* otherPointer = otherType->castVoidP();
  if (! otherPointer)
    return carbonPtrCompare(this, (const LangCppVoidPointer*)otherType);
  
  return 0;
}

LangCppPointerType::LangCppPointerType(LangCppType* pointed)
  : mPointed(pointed)
{}

LangCppPointerType::~LangCppPointerType()
{}

void LangCppPointerType::compose(UtString* msg) const
{
  *msg << "'" << mPointed->getTypeNameStr() << "*'";
}

void LangCppPointerType::emitUse(UtOStream& out) const
{
  mPointed->emitUse(out);
  out << "*";
}

void LangCppPointerType::emitTypeName(UtOStream& out) const
{
  mPointed->emitUse(out);
}

void LangCppPointerType::emitRefOperator(UtOStream& out) const
{
  if (mPointed->castClass())
    out << "->";
  else
    // Let the base class do the assertion
    LangCppType::emitRefOperator(out);
}

ptrdiff_t LangCppPointerType::compareHelper(const LangCppType* otherType) const
{
  const LangCppVoidPointer* otherVoid = otherType->castVoidP();
  if (! otherVoid)
    return carbonPtrCompare(this, (const LangCppPointerType*)otherType);
  
  const LangCppPointerType* otherPointer = otherVoid->castPointerType();
  if (! otherPointer)
    return carbonPtrCompare(this, (const LangCppPointerType*)otherType);
  
  
  return mPointed->compare(otherPointer->mPointed);
}

const LangCppPointerType* LangCppPointerType::castPointerType() const
{
  return this;
}

LangCppReferenceType::LangCppReferenceType(LangCppType* referenced)
  : LangCppType(referenced), mReferencedType(referenced)
{}

LangCppReferenceType::~LangCppReferenceType()
{}

SInt32 LangCppReferenceType::getByteWidth() const
{
  return mReferencedType->getByteWidth();
}

void LangCppReferenceType::compose(UtString* msg) const
{
  *msg << "'" << mReferencedType->getTypeNameStr() << "&' (" << mReferencedType->getSignStr() << ")";
}

void LangCppReferenceType::emitUse(UtOStream& out) const
{
  mReferencedType->emitUse(out);
}

void LangCppReferenceType::emitDeclare(UtOStream& out) const
{
  mReferencedType->emitUse(out);
  out << "&";
}

void LangCppReferenceType::emitDeclare(UtOStream& out, 
                                       const LangCppVariable* var) const
{
  mReferencedType->emitUse(out);
  out << "& ";
  var->emit(out);
}

void LangCppReferenceType::emitRefOperator(UtOStream& out) const
{
  mReferencedType->emitRefOperator(out);
}

const LangCppClassType* LangCppReferenceType::castClass() const {
  return mReferencedType->castClass();
}

const LangCppPodType* LangCppReferenceType::castPod() const {
  return mReferencedType->castPod();
}

ptrdiff_t LangCppReferenceType::compareHelper(const LangCppType* otherType) const
{
  const LangCppReferenceType* otherRef = otherType->castReference();
  if (! otherRef)
    return carbonPtrCompare(this, (const LangCppReferenceType*)otherType);
  
  return mReferencedType->compare(otherRef->mReferencedType);
}

LangCppPodType::LangCppPodType(const char* typeName, UInt32 byteWidth, SignT sign) 
  : LangCppType(typeName, sign), mNumBytes(byteWidth)
{}

LangCppPodType::~LangCppPodType()
{}

const LangCppPodType* LangCppPodType::castPod() const {
  return this;
}

SInt32 LangCppPodType::getByteWidth() const
{
  return getNumBytes();
}

void LangCppPodType::compose(UtString* msg) const
{
  *msg << "'" << mTypeName << "' (" << getSignStr() << ", " << mNumBytes << " bytes)";
}

ptrdiff_t LangCppPodType::compareHelper(const LangCppType* otherType) const
{
  const LangCppPodType* otherPod = otherType->castPod();
  if (! otherPod)
    return carbonPtrCompare(this, (const LangCppPodType*)otherType);
  
  return mNumBytes - otherPod->mNumBytes;
}

LangCppClassType::LangCppClassType(const char* typeName)
  : LangCppType(typeName, eUnsigned), mNumBytes(4)
{}

LangCppClassType::~LangCppClassType()
{}

const LangCppClassType* LangCppClassType::castClass() const {
  return this;
}

SInt32 LangCppClassType::getByteWidth() const
{
  return mNumBytes;
}

void LangCppClassType::putByteWidth(SInt32 numBytes)
{
  mNumBytes = numBytes;
}

void LangCppClassType::compose(UtString* msg) const
{
  *msg << "'" << mTypeName << "' (class)";
}

void LangCppClassType::emitRefOperator(UtOStream& out) const
{
  out << ".";
}

ptrdiff_t LangCppClassType::compareHelper(const LangCppType* otherType) const
{
  const LangCppClassType* otherClass = otherType->castClass();
  if (! otherClass)
    return carbonPtrCompare(this, (const LangCppClassType*)otherType);
  
  return 0;
}
