// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "langcpp/LangCppCarbonGlobal.h"

LangCppCarbonGlobal::LangCppCarbonGlobal()
  : mUInt32("UInt32", 4, LangCppType::eUnsigned),
    mSInt32("SInt32", 4, LangCppType::eSigned),
    mUInt64("UInt64", 8, LangCppType::eUnsigned),
    mSInt8("SInt8", 1, LangCppType::eSigned),
    mUInt8("UInt8", 1, LangCppType::eUnsigned)
{}


LangCppCarbonGlobal::~LangCppCarbonGlobal()
{}
