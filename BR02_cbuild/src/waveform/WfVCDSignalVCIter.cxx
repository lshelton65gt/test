// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include <algorithm>
#include "util/UtConv.h"
#include "util/DynBitVector.h"
#include "waveform/WfVCDSignal.h"
#include "waveform/WfVCDSignalVCIter.h"
#include <ctype.h>


WfVCDSignalVCIter::WfVCDSignalVCIter(const WfVCDTimeValueList* values, WfVCDSignal* signal)
  : WfAbsSignalVCIter(), mSignal(signal), mValues(values), mCurrentValue(values->begin())
{
  if (not atEnd())
  {
    mLastRequestedTime = getTime();
  }
  else
  {
    mLastRequestedTime = 0;
  }
}


WfVCDSignalVCIter::~WfVCDSignalVCIter(void)
{
}


void WfVCDSignalVCIter::getName(UtString* name) const
{
  mSignal->getName(name);
}

CarbonReal WfVCDSignalVCIter::getValueReal() const
{
  return (*mCurrentValue)->getValueReal();
}

void WfVCDSignalVCIter::getCompleteValue(const DynBitVector** value) const
{
  (*mCurrentValue)->getValue(value);
}

void WfVCDSignalVCIter::getValueDrive(const UInt32** value, const UInt32** drive) const
{
  const DynBitVector* completeVal;
  (*mCurrentValue)->getValue(&completeVal);

  const UInt32* arr = completeVal->getUIntArray();

  if (completeVal->size() > getWidth())
  {
    *value = arr;
    UInt32 index = completeVal->getUIntArraySize();
    index /= 2;
    *drive = &arr[index];
  }
  else
  {
    *drive = NULL;
    *value = arr;
  }
}

void WfVCDSignalVCIter::getDynValue(DynBitVector* value, DynBitVector* drive) const
{
  const DynBitVector* completeVal;
  (*mCurrentValue)->getValue(&completeVal);
  
  UInt32 width = getWidth();
  calcValueDrive(value, drive, completeVal, width);
}

void WfVCDSignalVCIter::calcValueDrive(DynBitVector* value, DynBitVector* drive, const DynBitVector* completeVal, UInt32 width)
{
  value->resize(width);
  drive->resize(width);
  value->reset();
  drive->reset();
  
  if (completeVal->size() > width)
  {
    const UInt32* arr = completeVal->getUIntArray();
    UInt32* destArr = value->getUIntArray();
    UInt32 numWords = completeVal->getUIntArraySize();
    numWords /= 2;
    memcpy(destArr, arr, numWords * sizeof(UInt32));
    
    destArr = drive->getUIntArray();
    const UInt32* drvArr = &arr[numWords];
    memcpy(destArr, drvArr, numWords * sizeof(UInt32));
  }
  else
    *value = *completeVal;
}

void WfVCDSignalVCIter::getValueStr(UtString* value) const
{
  const UInt32* tmpVal;
  const UInt32* tmpDrv;
  
  WfVCDBase::ISignal* internalSig = mSignal->getInternalSignal();
  bool isReal = internalSig->getSignalType() == WfVCD::eWfVCDSignalTypeReal;

  if (! isReal)
  {
    getValueDrive(&tmpVal, &tmpDrv);
    
    UInt32 width = getWidth();
    UInt32 numWords = (width + 31)/32;
    DynBitVector dynVal(width, tmpVal, numWords);
    
    if (tmpDrv == NULL)
      CarbonValRW::writeBin4ToStr(value, &dynVal, NULL);
    else
    {
      DynBitVector dynDrv(width, tmpDrv, numWords);
      CarbonValRW::writeBin4ToStr(value, &dynVal, &dynDrv);
    }
  }
  else
  {
    if (value)
    {
      CarbonReal curReal = getValueReal();
      value->clear();
      *value << curReal;
    }
  }
}

UInt32 WfVCDSignalVCIter::getWidth(void) const
{
  return mSignal->getWidth();
}

UInt64 WfVCDSignalVCIter::getTime(void) const
{
  return (*mCurrentValue)->getTime();
}

const UInt64* WfVCDSignalVCIter::getTimePtr(void) const
{
  return (*mCurrentValue)->getTimePtr();
}

UInt64 WfVCDSignalVCIter::getMinTime(void)
{
  return (*(mValues->begin()))->getTime();
}


UInt64 WfVCDSignalVCIter::getMaxTime(void)
{
  return mValues->back()->getTime();
}


UInt64 WfVCDSignalVCIter::getNextTransitionTime(void)
{
  INFO_ASSERT(not atMaxTime(), "Iterator is already at maximum time.");

  UInt64 next_time;
  WfVCDTimeValueList::const_iterator orig_pos = mCurrentValue;

  gotoNextChangeTime();
  next_time = getTime();
  mCurrentValue = orig_pos;

  return next_time;
}


UInt64 WfVCDSignalVCIter::getPrevTransitionTime(void)
{
  INFO_ASSERT(not atMinTime(), "Iterator is already at minimum time.");

  UInt64 prev_time;
  WfVCDTimeValueList::const_iterator orig_pos = mCurrentValue;

  gotoPrevChangeTime();
  prev_time = getTime();
  mCurrentValue = orig_pos;

  return prev_time;
}


void WfVCDSignalVCIter::gotoTime(UInt64 t)
{
  if (t <= getMinTime())
  {
    gotoMinTime();
    return;
  }

  if (t >= getMaxTime())
  {
    gotoMaxTime();
    return;
  }

  /*
  **  See if we are there already.
  */
  if (mLastRequestedTime == t)
  {
    return;
  }


  /*
  ** See if it is the next one.  This will often be the case
  ** in regular usage.
  */

  
  /*
  ** Enough being clever, just look for it.
  */
  mCurrentValue = std::lower_bound(mValues->begin(), mValues->end(), WfVCDTimeValue(&t), WfVCDTimeValueLess());
  if (getTime() != t)
  {
    mCurrentValue--;
  }
}

void WfVCDSignalVCIter::gotoPrevTime(void)
{
  INFO_ASSERT(not atMinTime(), "Iterator is already at minimum time.");
  mCurrentValue--; 
  mLastRequestedTime = (not atMinTime() ? getTime() : mLastRequestedTime);
}


void WfVCDSignalVCIter::gotoNextTime(void)
{
  INFO_ASSERT(not atEnd(), "Iterator is already at the end of time.");
  mCurrentValue++;
  mLastRequestedTime = (not atEnd() ? getTime() : mLastRequestedTime);
}


void WfVCDSignalVCIter::gotoNextChangeTime(void)
{
  mCurrentValue = findNextTransition();
}


void WfVCDSignalVCIter::gotoPrevChangeTime(void)
{
  const DynBitVector* curval = NULL;

  // TODO: Create a real value iterator and separate this code
  CarbonReal curReal = 0;
  WfVCDBase::ISignal* internalSig = mSignal->getInternalSignal();
  bool isReal = internalSig->getSignalType() == WfVCD::eWfVCDSignalTypeReal;
  
  if (! isReal)
    getCompleteValue(&curval);
  else
    curReal = getValueReal();

  for (gotoPrevTime(); not atStart(); gotoPrevTime())
  {
    const DynBitVector* val=0;
    CarbonReal nextReal = 0;
    if (! isReal)
      getCompleteValue(&val);
    else
      nextReal = getValueReal();

    // DynBVs are factoried, so we can compare pointers.
    if ((curval != val) ||
        (curReal != nextReal))
      break;
  }
}


void WfVCDSignalVCIter::gotoMinTime(void)
{
  mCurrentValue = mValues->begin();
  mLastRequestedTime = getTime();
}


void WfVCDSignalVCIter::gotoMaxTime(void)
{
  mCurrentValue = mValues->end() - 1;
  mLastRequestedTime = getTime();
}


bool WfVCDSignalVCIter::atMinTime(void)
{
  return mCurrentValue == mValues->begin();
}


bool WfVCDSignalVCIter::atMaxTime(void)
{
  WfVCDTimeValueList::const_iterator temp = mCurrentValue;  
  return ++temp == mValues->end();
}


bool WfVCDSignalVCIter::atStart(void)
{
  return mCurrentValue == mValues->begin();
}


bool WfVCDSignalVCIter::atEnd(void)
{
  return mCurrentValue == mValues->end();
}


bool WfVCDSignalVCIter::atLastTransition(void)
{
  return findNextTransition() == mValues->end();
}

WfAbsSignal* WfVCDSignalVCIter::getSignal()
{
  return mSignal;
}

WfVCDTimeValueList::const_iterator WfVCDSignalVCIter::findNextTransition(void)
{
  WfVCDTimeValueList::const_iterator next_pos = mValues->end();
  WfVCDTimeValueList::const_iterator orig_pos = mCurrentValue;

  const DynBitVector* curr_value = NULL;

  const DynBitVector* next_value = NULL;

  // TODO: Create a real value iterator and separate this code
  CarbonReal curReal = 0;
  CarbonReal nextReal = 0;
  WfVCDBase::ISignal* internalSig = mSignal->getInternalSignal();
  bool isReal = internalSig->getSignalType() == WfVCD::eWfVCDSignalTypeReal;
  
  if (! atEnd())
  {
    if (!isReal)
      getCompleteValue(&curr_value);
    else
      curReal = getValueReal();
    
    for (gotoNextTime(); not atEnd(); gotoNextTime())
    {    
      if (! isReal)
        getCompleteValue(&next_value);
      else
        nextReal = getValueReal();
      
      if ((curr_value != next_value) ||
          (curReal != nextReal))
      {
        next_pos = mCurrentValue;
        break;
      }
    }
    
    mCurrentValue = orig_pos;
  }
  return next_pos;
}
