// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "waveform/WfVCDFileReader.h"
#include "WfVCDFileParse.h"
#include "util/CarbonAssert.h"
#include "util/UtHashSet.h"
#include "symtab/STSymbolTable.h"

#include <stdlib.h>

template <class P> void maybeResizeVec(UtVector<P*>& vec,
                                       size_t index)
{
  size_t size = vec.size();
  if (index >= size)
  {
    size_t newSize = index + 1;
    vec.resize(newSize);
    for (size_t i = size; i < newSize; ++i)
      vec[i] = NULL;
  }
}

class WfVCDFileReader::GlyphNodeTable
{
public: CARBONMEM_OVERRIDES

  GlyphNodeTable(const WfVCDFileReader* vcdReader) : mReader(vcdReader), mSize(0)
  {}
  
  typedef UtVector<IWaveIdCode *> NodeVec;
  
  size_t glyphHash(const char* str) const
  {
    size_t hash = 0;
    int numGlyphChars = 0;
    
    const char* orig = str;
    
    while (*str != '\0') {
      INFO_ASSERT(*str >= 33, str);
      INFO_ASSERT(*str <= 126, str);
      ++numGlyphChars;
      if (numGlyphChars >= 5)
      {
        int lineNumber = mReader->getLineNo();
        const char* fileName = mReader->mFilename.c_str();
        fprintf(stderr, "%s:%d: Glyph is too large (greater than 5 characters). VCD File corrupt? Glyph is: %s\n", fileName, lineNumber, orig);
        exit(1);
      }

      // We cannot treat '!' as position 0, because "!!" does not
      // represent 0. Therefore make the starting position 32 
      // ('!' - 32 == 1).
      size_t normalizedPos = *str - (33-1);
      // Base 95 multiplication (again because '!' cannot represent
      // position 0)
      hash += (126-33+1+1)*hash + normalizedPos;
      ++str;
    }
    return hash;
  }
  
  IWaveIdCode* find(const char* glyph) {
    const GlyphNodeTable* me = const_cast<const GlyphNodeTable*>(this);
    return const_cast<IWaveIdCode*>(me->find(glyph));
  }

  const IWaveIdCode* find(const char* glyph) const
  {
    size_t index = glyphHash(glyph);
    const IWaveIdCode* node = NULL;
    if (index < mVec.size()) 
      node = mVec[index];
    return node;
  }

  void insert(const char* glyph, IWaveIdCode* node)
  {
    size_t index = glyphHash(glyph);
    maybeResizeVec(mVec, index);
    if (mVec[index] == NULL)
      ++mSize;
    mVec[index] = node;
  }
  
  typedef NodeVec::const_iterator const_iterator;
  const_iterator begin() const { return mVec.begin(); }
  const_iterator end() const { return mVec.end(); }

  typedef NodeVec::iterator iterator;
  iterator begin() { return mVec.begin(); }
  iterator end() { return mVec.end(); }

  void deleteGlyphs()
  {
    for (NodeVec::iterator p = mVec.begin(); p != mVec.end(); ++p)
      delete *p;

    mVec.clear();
  }

  UInt32 size() const {
    return mSize;
  }

private:
  const WfVCDFileReader* mReader;
  UInt32 mSize;

  NodeVec mVec;
  
  // forbid
  GlyphNodeTable();
  GlyphNodeTable(const GlyphNodeTable&);
  GlyphNodeTable& operator=(const GlyphNodeTable&);
};

WfVCDFileReader::WfVCDFileReader(const char* filename, AtomicCache* strCache, 
                                 DynBitVectorFactory* dynFactory,
                                 UtUInt64Factory* timeFactory,
                                 WfAbsControl* control) : 
  WfVCDBase(filename, strCache, dynFactory, timeFactory, control), mSkipValues(false)
{
  mParser = 0;
  mWatchedSignals = new GlyphNodeTable(this);
  mKnownSignals = new GlyphNodeTable(this);

};


WfVCDFileReader::~WfVCDFileReader(void)
{
  delete mWatchedSignals;

  mKnownSignals->deleteGlyphs();
  delete mKnownSignals;
  
  destroyParser();

};


void WfVCDFileReader::createParser(void)
{
  INFO_ASSERT(!mParser, "createParser called more than once.");
  mParser = new WfVCDFileParse(this);
}


void WfVCDFileReader::destroyParser(void)
{
  delete mParser;
  mParser = NULL;
}


/*
**
** AbsFileReader Interface
**
*/
WfAbsFileReader::Status WfVCDFileReader::loadSignals(UtString* errMsg)
{
  createParser();
  if (! mParser->openFile(mFilename.c_str(), errMsg))
    return eError;

  Status stat = eOK;
  if (! mParser->parseSignals(errMsg))
    stat = eError;
  else 
    endLoadSignals();
  return stat;
}


void WfVCDFileReader::unloadSignals(void)
{
}


WfAbsFileReader::Status WfVCDFileReader::loadValues(UtString* msg)
{
  msg->clear();
  if (mValsRead)
  {
    *msg << "VCD Values have already been loaded. They cannot be loaded again.";
    return eError;
  }
  Status stat = eOK;
  
  if (! mParser->parseValues(msg))
    stat = eError;
  else 
    mValsRead = true;

  if (stat != eError)
  {
    // collectBlastedValues(msg);
    if (! msg->empty())
      stat = eWarning;
  }

  return stat;
}


void WfVCDFileReader::unloadValues(void)
{
  destroyParser();
}




/*
**
** WfAbsVCDParserActions Interface
**
*/
void WfVCDFileReader::doVariable(WfVCD::Identifier &var_ident, WfVCD::SignalType var_type, UInt32 size, const char *glyph)
{
  STSymbolTableNode *node = createSymbolTableNode(var_ident, true);

  IWaveIdCode* glyphObj = mKnownSignals->find(glyph);
  if (glyphObj == NULL)
  {
    glyphObj = new WaveGlyph(glyph, size);
    mKnownSignals->insert(glyph, glyphObj);
  }    
  else
    ST_ASSERT(size == glyphObj->getSignalWidth(), node);
  
  setSignalOnNode(node, glyphObj, var_ident, var_type, size);
}

bool WfVCDFileReader::doTime(UInt64 sim_time)
{
  // This stays around after the file is read. mSimulationTime does
  // not.
  if (mMaxSimTime < sim_time)
    mMaxSimTime = sim_time;

  return advanceSimulationTime(sim_time);
}

void WfVCDFileReader::doValue(const char *glyph, const char *value)
{
  if (! mSkipValues)
  {
    IWaveIdCode* node = mWatchedSignals->find(glyph);
    if (node != NULL)
      addValueChange(node, value);
  }
}

void WfVCDFileReader::doValueReal(const char *glyph, CarbonReal value)
{
  if (! mSkipValues)
  {
    IWaveIdCode* node = mWatchedSignals->find(glyph);
    if (node != NULL)
      addValueChangeReal(node, value);
  }
}

void WfVCDFileReader::doEvent(const char* glyph)
{
  // just make sure that the glyph exists. we don't handle events
  if (mKnownSignals->find(glyph) == NULL)
  {
    UtString msg;
    msg << "Unrecognized glyph '" << glyph << "'. Not found in hierarchy.";
    fatalParseError(msg.c_str());
  }
}

void WfVCDFileReader::gatherWatchedIds(WaveIdSet* allWatched)
{
  for (GlyphNodeTable::iterator q = mWatchedSignals->begin(), qe = mWatchedSignals->end(); 
       q != qe; ++q)
  {
    IWaveIdCode* id = *q;
    if (id)
      allWatched->insert(id);
  }
}

void WfVCDFileReader::insertWatchedId(IWaveIdCode* absId)
{
  const char* glyph = absId->castGlyph()->symbol();
  if (mWatchedSignals->find(glyph) == NULL)
    mWatchedSignals->insert(glyph, absId);
}

void WfVCDFileReader::fatalParseError(const char* msg)
{
  fprintf(stderr, "%s\n", msg);
#ifdef CDB
  abort();
#else
  exit(1);
#endif
}

int WfVCDFileReader::getLineNo() const
{
  return mParser->getLineNo();
}

UInt32 WfVCDFileReader::getNumSignals() const
{
  return mKnownSignals->size();
}

void WfVCDFileReader::putSkipValues(bool skip)
{
  mSkipValues = skip;
}
