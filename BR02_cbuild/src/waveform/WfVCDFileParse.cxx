// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// author: Mark Seneski

#include "WfVCDFileParse.h"
#include "waveform/WfVCDPerfectHash.h"
#include "waveform/WfVCDFileReader.h"
#include "util/Zstream.h"
#include "util/OSWrapper.h"
#include "hdl/HdlId.h"

#include <cctype>

static const char* sZero = "0";
static const char* sOne = "1";

static const char* sSmallX = "x";
static const char* sBigX = "X";
static const char* sSmallZ = "z";
static const char* sBigZ = "Z";
static const char* sDash = "-";
static const char* sVhdlU = "U";
static const char* sVhdlW = "W";

static int sGetLength(const char* end, const char* begin)
{
  int length = end - begin;
  INFO_ASSERT(length >= 0, begin);
  return length;
}

WfVCDFileParse::WfVCDFileParse(WfVCDFileReader* controller) :
  mController(controller), mPerfectHash(NULL),
  mFile(NULL),
  mInputBuf(NULL), mAlignBuf(NULL),
  mErrMsg(NULL),
  mLastPosition(0), 
  mCurLineNo(1),
  mParseLineNo(1),
  mUsingAlignBuf(false),
  mLastUsedAligned(false),
  mSignalsParsed(false)
{
  unsigned char ic;
  for (unsigned int i = 0; i < 256; ++i)
  {
    ic = static_cast<unsigned char>(i);
    mDelims[i] = isspace(ic);
    mGlyphToks[i] = isgraph(ic);
  }
  
}

WfVCDFileParse::~WfVCDFileParse()
{
  delete mFile;
}

bool WfVCDFileParse::openFile(const char* fileName, UtString* errMsg)
{
  INFO_ASSERT(mFile == NULL, "openFile called more than once.");
  ZISTREAM_ALLOC(mFile, fileName);
  bool isGood = true;
  if (! mFile->fail())
  {
    if (! mFile->isCompressed())
    {
      mInputBuf = mFile->getFileBuf();
      mAlignBuf = mFile->getDataBuf();

      // Let's resize the buffer. Unfortunately, Zistream already has
      // some stuff inside the buffer for magic header processing, so,
      // we'll just seek back to the beginning.
      mFile->seek(0);
#if 0
      // A bigger buffer just slows us down due to data cache misses.
      mFile->resizeFileBuffer(2097152);
      mFile->resizeDataBuffer(2097152);
#endif
      mFile->fillFileBuf();
    }
    else
    {
      isGood = false;
      *errMsg << fileName << " is a carbon crypted file. Only unencrypted files can be processed.";
    }
  }
  else
  {
    isGood = false;
    errMsg->assign(mFile->getError());
  }
  
  return isGood;
}

void WfVCDFileParse::doErrMsg(const char* msg, const char* beginPtr, const char* endPtr)
{
  *mErrMsg << mController->getFileName() << ":" << mCurLineNo << ": " << msg;
  if (beginPtr && endPtr)
  {
    *mErrMsg << ": ";
    mErrMsg->append(beginPtr, sGetLength(endPtr, beginPtr));
  }
}

UInt32 WfVCDFileParse::getCurrentBufferPosition() const
{
  UInt32 ret = 0;
  if (! mAlignBuf->isEndBufRead())
    ret = mAlignBuf->getReadIndex();
  else if (! mInputBuf->isEndBufRead())
    ret = mInputBuf->getReadIndex();

  // If we are at the end of input in mInputBuf we want to return 0,
  // since that is technically the current position once we fill the
  // buffer.
  return ret;
}

void WfVCDFileParse::putCurrentBufferPosition(const char* endPtr)
{
  UtFileBuf* curBuf = mInputBuf;
  if (mLastUsedAligned)
    curBuf = mAlignBuf;

  UInt32 writeIndex;
  char* charBuf = curBuf->getBufferAll(&writeIndex);
  INFO_ASSERT (endPtr <= (charBuf + writeIndex), "Buffer iterator past the end of the buffer.");
  INFO_ASSERT (endPtr >= charBuf, "Buffer iterator before the beginningo of the buffer.");
  int pos = sGetLength(endPtr, charBuf);
  curBuf->putReadIndex(pos);
  mLastPosition = pos;
}

void WfVCDFileParse::rewindReadBuffer()
{
  if (mLastUsedAligned)
  {
    // the align buf was used last
    mAlignBuf->putReadIndex(mLastPosition);
    mUsingAlignBuf = true;
  }
  else
  {
    mInputBuf->putReadIndex(mLastPosition);
    // remove any seen newlines
    mParseLineNo = mCurLineNo;
  }
}

void WfVCDFileParse::invalidateAlignBuf()
{
  UInt32 wIndex = mAlignBuf->getWriteIndex();
  // This invalidates the buffer, so that isEndBufRead is true.
  mAlignBuf->putReadIndex(wIndex);
}

bool WfVCDFileParse::nextToken(char** beginPtr, char** endPtr)
{
  bool success = true;
  mCurLineNo = mParseLineNo;
  if (mUsingAlignBuf)
  {
    // The alignment buffer has exactly 1 token in it. Just retrieve
    // it and return
    UInt32 numAlignChars;
    char* alignBuf = mAlignBuf->getBufferAll(&numAlignChars);
    UInt32 alignReadIndex = mAlignBuf->getReadIndex();
    *beginPtr = &alignBuf[alignReadIndex];
    *endPtr = *beginPtr + (numAlignChars - alignReadIndex);
    invalidateAlignBuf();
    skipLeadingWS(beginPtr, *endPtr);
    mUsingAlignBuf = false;
    mLastUsedAligned = true;
    return true;
  }
  
  mLastUsedAligned = false;

  UInt32 numChars;
  char* fileBuf = mInputBuf->getBufferAll(&numChars);
  bool foundDelim = false;

  *beginPtr = mInputBuf->getCurrentPos();
  UInt32 beginIndex = mInputBuf->getReadIndex();
  UInt32 curIndex = beginIndex;
  bool pastLeadingSpaces = false;
  while (!foundDelim && (curIndex < numChars))
  {
    char p = fileBuf[curIndex];
    if (mDelims[int(p)])
    {
      if (p == '\n')
        ++mParseLineNo;

      if (pastLeadingSpaces)
      {
        foundDelim = true;
        // Order is important here
        *endPtr = mInputBuf->getCurrentPos();
        curIndex = mInputBuf->incrReadIndex();
      }
      else
      {
        // Order is important here
        curIndex = mInputBuf->incrReadIndex();
        *beginPtr = mInputBuf->getCurrentPos();
      }
    }
    else
    {
      pastLeadingSpaces = true;
      curIndex = mInputBuf->incrReadIndex();
    }
  }
  
  if (! foundDelim)
  {
    // ran out of buffer
    UInt32 numCharsLeft = numChars - beginIndex;
    
    if (numCharsLeft > 0)
    {
      // The align buffer should be the same size or greater than the
      // file buffer. For safety, mainly. Technically, we only need
      // enough space to put the unprocessed buffer.
      
      INFO_ASSERT(mAlignBuf->capacity() >= numChars, "Allocated buffer is not large enough.");
      // The align buffer should be empty!
      // If it isn't it means we have a token whose size surpasses the
      // buffer capacity.
      if (! mAlignBuf->isEndBufRead())
        mController->fatalParseError("Token too long. Buffer overrun.");
      
      // Reset the read and write indices
      mAlignBuf->reset();
      
      // Save the unprocessed buffer
      mAlignBuf->write(&fileBuf[beginIndex], numCharsLeft);
    }
    else 
      // Do this anyway. Maybe at a buffer boundary
      mAlignBuf->reset();

    // RECURSIVE CALL HERE WITHIN fillBuffer
    success = fillBuffer(pastLeadingSpaces, beginPtr, endPtr);
  }

  return success;
}

static void sMaybeNullTerminateBuf(UtFileBuf* buf)
{
  if (buf->size() > buf->getWriteIndex())
  {
    const char nullTerm = '\0';
    UInt32 curWriteIndex = buf->getWriteIndex();
    buf->write(&nullTerm, 1);
    buf->putWriteIndex(curWriteIndex);
  }
}

bool WfVCDFileParse::fillBuffer(bool pastLeadingSpaces, char** beginPtr, char** endPtr)
{
  char* subBegin;
  char* subEnd;
  UInt32 subSize; // not used except for capacity and bounds check
  char* subBuf;
  bool success = true;
  
  bool atEndOfFile = mFile->fileEof();
  if (! mFile->fileEof())
  {
    mFile->fillFileBuf();
    
    // terminate the buffer if we have unused space.
    sMaybeNullTerminateBuf(mInputBuf);

    // fence post problem. nextToken bypasses leading spaces. Not
    // good if we have already done that. The next space in the
    // buffer is the end of the current token.
    bool doNextToken = true;
    if (pastLeadingSpaces && ! mFile->fileEof())
    {
      subBuf = mInputBuf->getBufferAll(&subSize);
      if (mDelims[int(subBuf[0])])
      {
        doNextToken = false;
        mLastUsedAligned = true;
        if (subBuf[0] == '\n')
          ++mParseLineNo;

        // no need to write to the align buffer. We got everything we
        // need in it already

        // move past the delim
        mInputBuf->incrReadIndex();
      }
    }

    if (doNextToken)
    {
      // RECURSIVE CALL HERE - nextToken called this function
      if (nextToken(&subBegin, &subEnd))
      {
        // our next token is definitely coming from the align buf
        mLastUsedAligned = true;
        
        // We may be at the end of the file and the nexttoken returned
        // the rest of the alignment buffer which was filled with
        // something above.
        subBuf = mAlignBuf->getBufferAll(&subSize);
        
        if (! mAlignBuf->isEndBufRead())
        {
          
          // check if subBegin and subEnd are within mAlignBuf
          // If so, just rest return, otherwise add the rest of the
          // token to the alignment buffer and return.
          if (! ((subBegin >= subBuf) && (subEnd <= (subBuf + subSize))))
            mAlignBuf->write(subBegin, sGetLength(subEnd, subBegin));
        }
        else
          mAlignBuf->write(subBegin, sGetLength(subEnd, subBegin));
      }
      else
        // No more stuff to parse.
        success = false;
    }

    if (success)
    {
      sMaybeNullTerminateBuf(mAlignBuf);
      subBuf = mAlignBuf->getBufferAll(&subSize);
      *beginPtr = mAlignBuf->getCurrentPos();
      *endPtr = *beginPtr + (subSize - mAlignBuf->getReadIndex());
      skipLeadingWS(beginPtr, *endPtr);
      invalidateAlignBuf();
    }
  }
  else if (! mAlignBuf->isEndBufRead())
  {
    mLastUsedAligned = true;

    // If we have come to the end of the file, we should add a \0
    // terminator to the alignbuf if it is not full to avoid reading
    // uninitialized memory in the case where the entire vcd file fit
    // into the file buf.
    if (atEndOfFile)
      sMaybeNullTerminateBuf(mAlignBuf);

    // we have to give up. Just send back what is left.
    // But, first we have to bypass any leading whitespace.
    mAlignBuf->getBufferAll(&subSize);
    *beginPtr = mAlignBuf->getCurrentPos();
    *endPtr = *beginPtr + mAlignBuf->getWriteIndex();
    skipLeadingWS(beginPtr, *endPtr);
    if (*beginPtr == *endPtr)
      // we are at the end of the file and the tokens
      success = false;
    invalidateAlignBuf();
  }
  else
    // nothing left
    success = false;
  
  return success;
}

bool WfVCDFileParse::scanForEndTok(const char* beginPtr, 
                                   const char* endPtr)
{
  bool foundEnd = false;
  if (((endPtr - beginPtr) > 4) && strncmp(beginPtr, "$end", 4) == 0)
  {
    foundEnd = true;

    // now fix up the last position. 
    putCurrentBufferPosition(beginPtr + 4);
    rewindReadBuffer(); // updates appropriate buffer
  }
  return foundEnd;
}

bool WfVCDFileParse::parseSignals(UtString* errMsg)
{
  mErrMsg = errMsg;
  INFO_ASSERT(mErrMsg, "NULL error message string passed in.");

  char* beginPtr;
  char* endPtr;
  const WfVCDToken* tokenObj;
  bool isGood = true;
  while (! mSignalsParsed && isGood && nextToken(&beginPtr, &endPtr))
  {
    tokenObj = WfVCDPerfectHash::get_vcd_toktype(beginPtr, sGetLength(endPtr, beginPtr));
    if (tokenObj)
    {
      switch(tokenObj->mType)
      {
      case eWfVCDTokenDate:
        isGood = parseDate();
        break;
      case eWfVCDTokenVersion:
        isGood = parseVersion();
        break;
      case eWfVCDTokenTimescale:
        isGood = parseTimescale();
        break;
      case eWfVCDTokenScope:
        isGood = parseScope();
        break;
      case eWfVCDTokenUpscope:
        isGood = mController->doPopScope();
        if (! isGood)
          doErrMsg("Corrupt vcd file - More pop scopes than push scopes", NULL, NULL);
        break;
      case eWfVCDTokenVar:
        isGood = parseVar();
        break;
      case eWfVCDTokenComment:
        isGood = parseComment();
        break;
      case eWfVCDTokenEndDef:
      case eWfVCDTokenEnd:
        // no action for these
        break;
      case eWfVCDTokenDumpAll:
      case eWfVCDTokenDumpOff:
      case eWfVCDTokenDumpOn:
      case eWfVCDTokenDumpVars:
      case eWfVCDTokenBeginKey:
      case eWfVCDTokenForkKey:
      case eWfVCDTokenFunctionKey:
      case eWfVCDTokenModuleKey:
      case eWfVCDTokenTaskKey:
      case eWfVCDTokenEventKey:
      case eWfVCDTokenIntegerKey:
      case eWfVCDTokenParameterKey:
      case eWfVCDTokenRealKey:
      case eWfVCDTokenRegKey:
      case eWfVCDTokenSupply0Key:
      case eWfVCDTokenSupply1Key:
      case eWfVCDTokenTimeKey:
      case eWfVCDTokenTriKey:
      case eWfVCDTokenTriandKey:
      case eWfVCDTokenTriorKey:
      case eWfVCDTokenTriregKey:
      case eWfVCDTokenTri0Key:
      case eWfVCDTokenTri1Key:
      case eWfVCDTokenWandKey:
      case eWfVCDTokenWireKey:
      case eWfVCDTokenWorKey:
        mSignalsParsed = true;
        rewindReadBuffer();
        break;
      };
      mLastPosition = getCurrentBufferPosition();
    }
    else
    {
      if (beginPtr[0] == '#')
      {
        // end of signals
        mSignalsParsed = true;
        rewindReadBuffer();
      }
      else
      {
        // check if this is a $end<keyword> debacle
        if (! scanForEndTok(beginPtr, endPtr))
        {
          doErrMsg("Unrecognized vcd token", beginPtr, endPtr);
          isGood = false;
        }
      }
    }
  }
  mErrMsg = NULL;
  // maybe an empty file
  if (isGood)
    mSignalsParsed = true;
  return isGood;
}


bool WfVCDFileParse::getGlyph()
{
  bool isGood = true;
  char* beginPtr;
  char* endPtr;
  mGlyphBuf.clear();
  if (nextToken(&beginPtr, &endPtr))
    mGlyphBuf.append(beginPtr, sGetLength(endPtr, beginPtr));
  else
  {
    doErrMsg("Premature end of file. Expected glyph.", NULL, NULL);
    isGood = false;
  }
  
  return isGood;
}

bool WfVCDFileParse::parseValues(UtString* errMsg)
{
  INFO_ASSERT(mSignalsParsed, "parseValues called before hierarchy was parsed.");
  
  mErrMsg = errMsg;
  INFO_ASSERT(mErrMsg, "NULL error message string passed in.");
  
  char* beginPtr;
  char* endPtr;
  
  // for now, it is much simpler to use a null terminated string for
  // the value and for the glyph. When more performance is needed we
  // can change this.

  // mValueBuf is value
  // mGlyphBuf is glyph
  double realVal = 0;
  UInt64 time = 0;
  char* tokPtr;

  const WfVCDToken* tokenObj = NULL;
  bool isGood = true;
  bool stop = false;
  bool skipSection = false;
  while (isGood && !stop && nextToken(&beginPtr, &endPtr))
  {
    switch (beginPtr[0])
    {
    case 'b':
      ++beginPtr;
      mValueBuf.clear();
      mValueBuf.append(beginPtr, sGetLength(endPtr, beginPtr));
      isGood = getGlyph();
      if (isGood)
        mController->doValue(mGlyphBuf.c_str(), mValueBuf.c_str());
      break;
    case 'r':
      ++beginPtr;
      mGenericBuf.clear();
      realVal = OSStrToD(beginPtr, &tokPtr, &mGenericBuf);
      if (tokPtr != beginPtr)
      {
        // clear the value buf, just to have some sort of mutual
        // exclusivity between real and non-real.
        mValueBuf.clear();
        isGood = getGlyph();
      }
      else
      {
        doErrMsg(mGenericBuf.c_str(), NULL, NULL);
        isGood = false;
      }
      
      if (isGood)
        mController->doValueReal(mGlyphBuf.c_str(), realVal);
      break;
    case '0':
      ++beginPtr;
      parseGlyph(&beginPtr, endPtr);
      mController->doValue(mGlyphBuf.c_str(), sZero);
      break;
    case '1':
      ++beginPtr;
      parseGlyph(&beginPtr, endPtr);
      mController->doValue(mGlyphBuf.c_str(), sOne);
      break;
    case 'u':
    case 'U':
      ++beginPtr;
      parseGlyph(&beginPtr, endPtr);
      mController->doValue(mGlyphBuf.c_str(), sVhdlU);
      break;
    case '-':
      ++beginPtr;
      parseGlyph(&beginPtr, endPtr);
      mController->doValue(mGlyphBuf.c_str(), sDash);
      break;
    case 'w':
    case 'W':
      ++beginPtr;
      parseGlyph(&beginPtr, endPtr);
      mController->doValue(mGlyphBuf.c_str(), sVhdlW);
      break;
    case 'x':
      ++beginPtr;
      parseGlyph(&beginPtr, endPtr);
      mController->doValue(mGlyphBuf.c_str(), sSmallX);
      break;
    case 'X':
      ++beginPtr;
      parseGlyph(&beginPtr, endPtr);
      mController->doValue(mGlyphBuf.c_str(), sBigX);
      break;
    case 'z':
      ++beginPtr;
      parseGlyph(&beginPtr, endPtr);
      mController->doValue(mGlyphBuf.c_str(), sSmallZ);
      break;
    case 'Z':
      ++beginPtr;
      parseGlyph(&beginPtr, endPtr);
      mController->doValue(mGlyphBuf.c_str(), sBigZ);
      break;
    case '#':
      if (skipSection)
      {
        skipSection = false;
        mController->putSkipValues(skipSection);
      }

      ++beginPtr;
      mGenericBuf.clear();
      time = OSStrToU64(beginPtr, &tokPtr, 10, &mGenericBuf);
      if (tokPtr != beginPtr)
        stop = mController->doTime(time);
      else
      {
        doErrMsg(mGenericBuf.c_str(), NULL, NULL);
        isGood = false;
      }
      break;
    default:
      // must be a command. we just ignore those, but we need to check
      // if it is legal
      // Correction: Ignore all except dumpall. We must skip over the
      // values given in those in  order to get proper edges. dumpall
      // is just a checkpoint.
      tokenObj = WfVCDPerfectHash::get_vcd_toktype(beginPtr, sGetLength(endPtr, beginPtr));
      if (tokenObj)
      {
        switch(tokenObj->mType)
        {
        case eWfVCDTokenDumpAll:
          skipSection = true;
          mController->putSkipValues(skipSection);
          break;
        case eWfVCDTokenDumpOff:
        case eWfVCDTokenDumpOn:
        case eWfVCDTokenDumpVars:
        case eWfVCDTokenEnd:
          if (skipSection)
          {
            skipSection = false;
            mController->putSkipValues(skipSection);
          }
          // just ignore
          break;
        case eWfVCDTokenDate:
        case eWfVCDTokenVersion:
        case eWfVCDTokenTimescale:
        case eWfVCDTokenScope:
        case eWfVCDTokenUpscope:
        case eWfVCDTokenVar:
        case eWfVCDTokenComment:
        case eWfVCDTokenEndDef:
        case eWfVCDTokenBeginKey:
        case eWfVCDTokenForkKey:
        case eWfVCDTokenFunctionKey:
        case eWfVCDTokenModuleKey:
        case eWfVCDTokenTaskKey:
        case eWfVCDTokenEventKey:
        case eWfVCDTokenIntegerKey:
        case eWfVCDTokenParameterKey:
        case eWfVCDTokenRealKey:
        case eWfVCDTokenRegKey:
        case eWfVCDTokenSupply0Key:
        case eWfVCDTokenSupply1Key:
        case eWfVCDTokenTimeKey:
        case eWfVCDTokenTriKey:
        case eWfVCDTokenTriandKey:
        case eWfVCDTokenTriorKey:
        case eWfVCDTokenTriregKey:
        case eWfVCDTokenTri0Key:
        case eWfVCDTokenTri1Key:
        case eWfVCDTokenWandKey:
        case eWfVCDTokenWireKey:
        case eWfVCDTokenWorKey:
            isGood = false;
            doErrMsg("Expected simulation command but got token", beginPtr, endPtr);
            break;
        }
      }
      else
      {
        if (! scanForEndTok(beginPtr, endPtr))
        {
          // must assume an event which is just a glyph. The
          // controller must check that it is known glyph
          parseGlyph(&beginPtr, endPtr);
          mController->doEvent(mGlyphBuf.c_str());
        }
        else if (skipSection)
        {
          skipSection = false;
          mController->putSkipValues(skipSection);
        }
      }
      break;
    }
  }

  if (! stop)
    mController->endLoadValues();

  mErrMsg = NULL;
  return isGood;
}

void WfVCDFileParse::skipLeadingWS(char** beginText, char* endText)
{
  while (mDelims[int(**beginText)] && (*beginText < endText))
    ++(*beginText);
}

WfVCD::ScopeType WfVCDFileParse::parseScopeType()
{
  char* beginText;
  char* endText;

  // need to find the end of the token
  WfVCD::ScopeType scopeType = WfVCD::eWfVCDScopeTypeINVALID;

  if (nextToken(&beginText, &endText))
  {
    int length = sGetLength(endText, beginText);
    const WfVCDToken* tokenObj = WfVCDPerfectHash::get_vcd_toktype(beginText, length);
    if (tokenObj)
    {
      switch(tokenObj->mType)
      {
      case eWfVCDTokenBeginKey:
        scopeType = WfVCD::eWfVCDScopeTypeBegin;
        break;
      case eWfVCDTokenForkKey:
        scopeType = WfVCD::eWfVCDScopeTypeFork;
        break;
      case eWfVCDTokenFunctionKey:
        scopeType = WfVCD::eWfVCDScopeTypeFunction;
        break;
      case eWfVCDTokenModuleKey:
        scopeType = WfVCD::eWfVCDScopeTypeModule;
        break;
      case eWfVCDTokenTaskKey:
        scopeType = WfVCD::eWfVCDScopeTypeTask;
        break;
      case eWfVCDTokenDate:
      case eWfVCDTokenVersion:
      case eWfVCDTokenTimescale:
      case eWfVCDTokenScope:
      case eWfVCDTokenUpscope:
      case eWfVCDTokenVar:
      case eWfVCDTokenComment:
      case eWfVCDTokenEndDef:
      case eWfVCDTokenEnd:
      case eWfVCDTokenDumpAll:
      case eWfVCDTokenDumpOff:
      case eWfVCDTokenDumpOn:
      case eWfVCDTokenDumpVars:
      case eWfVCDTokenEventKey:
      case eWfVCDTokenIntegerKey:
      case eWfVCDTokenParameterKey:
      case eWfVCDTokenRealKey:
      case eWfVCDTokenRegKey:
      case eWfVCDTokenSupply0Key:
      case eWfVCDTokenSupply1Key:
      case eWfVCDTokenTimeKey:
      case eWfVCDTokenTriKey:
      case eWfVCDTokenTriandKey:
      case eWfVCDTokenTriorKey:
      case eWfVCDTokenTriregKey:
      case eWfVCDTokenTri0Key:
      case eWfVCDTokenTri1Key:
      case eWfVCDTokenWandKey:
      case eWfVCDTokenWireKey:
      case eWfVCDTokenWorKey:
        doErrMsg("Expected scope type but got token", beginText, endText);
        break;
      }
    }
    else
      doErrMsg("Unrecognized scope type", beginText, endText);

  }
  else
    doErrMsg("Premature end of file. Expected scope type", NULL, NULL);
  
  return scopeType;
}

WfVCD::SignalType WfVCDFileParse::parseVarType()
{
  // need to find the end of the token
  WfVCD::SignalType signalType = WfVCD::eWfVCDSignalTypeINVALID;

  char* beginText;
  char* endText;
  if (nextToken(&beginText, &endText))
  {
    int length = sGetLength(endText, beginText);
    const WfVCDToken* tokenObj = WfVCDPerfectHash::get_vcd_toktype(beginText, length);
    if (tokenObj)
    {
      switch(tokenObj->mType)
      {
      case eWfVCDTokenEventKey:
        signalType = WfVCD::eWfVCDSignalTypeEvent;
        break;
      case eWfVCDTokenIntegerKey:
        signalType = WfVCD::eWfVCDSignalTypeInteger;
        break;
      case eWfVCDTokenParameterKey:
        signalType = WfVCD::eWfVCDSignalTypeParameter;
        break;
      case eWfVCDTokenRealKey:
        signalType = WfVCD::eWfVCDSignalTypeReal;
        break;
      case eWfVCDTokenRegKey:
        signalType = WfVCD::eWfVCDSignalTypeReg;
        break;
      case eWfVCDTokenSupply0Key:
        signalType = WfVCD::eWfVCDSignalTypeSupply0;
        break;
      case eWfVCDTokenSupply1Key:
        signalType = WfVCD::eWfVCDSignalTypeSupply1;
        break;
      case eWfVCDTokenTimeKey:
        signalType = WfVCD::eWfVCDSignalTypeTime;
        break;
      case eWfVCDTokenTriKey:
        signalType = WfVCD::eWfVCDSignalTypeTri;
        break;
      case eWfVCDTokenTriandKey:
        signalType = WfVCD::eWfVCDSignalTypeTriAnd;
        break;
      case eWfVCDTokenTriorKey:
        signalType = WfVCD::eWfVCDSignalTypeTriOr;
        break;
      case eWfVCDTokenTriregKey:
        signalType = WfVCD::eWfVCDSignalTypeTriReg;
        break;      
      case eWfVCDTokenTri0Key:
        signalType = WfVCD::eWfVCDSignalTypeTri0;
        break;
      case eWfVCDTokenTri1Key:
        signalType = WfVCD::eWfVCDSignalTypeTri1;
        break;
      case eWfVCDTokenWandKey:
        signalType = WfVCD::eWfVCDSignalTypeWAnd;
        break;
      case eWfVCDTokenWireKey:
        signalType = WfVCD::eWfVCDSignalTypeWire;
        break;
      case eWfVCDTokenWorKey:
        signalType = WfVCD::eWfVCDSignalTypeWOr;
        break;
      case eWfVCDTokenDate:
      case eWfVCDTokenVersion:
      case eWfVCDTokenTimescale:
      case eWfVCDTokenScope:
      case eWfVCDTokenUpscope:
      case eWfVCDTokenVar:
      case eWfVCDTokenComment:
      case eWfVCDTokenEndDef:
      case eWfVCDTokenEnd:
      case eWfVCDTokenDumpAll:
      case eWfVCDTokenDumpOff:
      case eWfVCDTokenDumpOn:
      case eWfVCDTokenDumpVars:
      case eWfVCDTokenBeginKey:
      case eWfVCDTokenForkKey:
      case eWfVCDTokenFunctionKey:
      case eWfVCDTokenModuleKey:
      case eWfVCDTokenTaskKey:
        doErrMsg("Expected var type but got token", beginText, endText);
        break;
      }
    }
    else
      doErrMsg("Unrecognized var type", beginText, endText);
  }
  else
    doErrMsg("Premature end of file. Expected scope type", NULL, NULL);

  return signalType;
}

bool WfVCDFileParse::parseVerilogToken(bool keepBrackets, HdlId* info)
{
  // Escaped identifiers in the hdl source may appear without the
  // backslash in the dump. So, the best we can do is parse the token
  // and make it legal.

  // for now, we are doing this the slow way and copying the rest of
  // the buffer into a string

  const char* beginText;
  const char* endText;
  // totalLen is the number of characters to the end of the identifier
  // information
  //int totalLen = sGetLength(endText, *beginText);


  //mValueBuf.append(*beginText, totalLen);

  // I need to change how HdlVerilogPath works. Currently, it requires
  // null terminated strings. So, the next token has to go into a
  // string for now.
  mValueBuf.clear();
  
  HdlVerilogPath pather;
  bool isGood = true;
  
  /* We have come across some very strange vcd files with horrible
     verilog tokens. For example, we have seen foo[ 30:0]. That space
     after the foo[ removes the possibility of doing simple delimiter
     parsing. We can even have
     foo
     [ 30:0]
     $end
     Also, the verilog identifiers do not have to be legal. It is up
     to the parser to make them legal. So, we could have
     ** [ 30:0]
     Considering that the file buffer could end at any time, we MUST
     (after much experimentation) parse until we find $end before we
     can parse the verilog token. So, parse free text and send the
     buffer to HdlVerilogPath.
  */
  isGood = parseFreeText();
  
  beginText = mGenericBuf.c_str();
  endText = beginText + mGenericBuf.size();
  if (isGood)
  {
    if (pather.makeLegalId(&beginText, endText, &mValueBuf, info, keepBrackets) == HdlHierPath::eIllegal)
    {
      doErrMsg("Illegal Verilog Identifier encountered", beginText, endText);
      isGood = false;
    }
  }
  return isGood;
}

bool WfVCDFileParse::parseScope()
{
  // First token is type, second token is verilog identifier with optional vector information

  bool isGood = true;

  // scope type
  WfVCD::ScopeType scopeType = parseScopeType();
  isGood = scopeType != WfVCD::eWfVCDScopeTypeINVALID;
  
  // verilog identifier
  HdlId info;
  
  if (isGood)
    // ident is in mValueBuf
    isGood = parseVerilogToken(true, &info);
  
  if (isGood)
  {
    WfVCD::Identifier scopeId(mValueBuf);
    mController->doPushScope(scopeId, scopeType);
  }
  return isGood;
}

void WfVCDFileParse::parseGlyph(char** beginText, char* endText)
{
  skipLeadingWS(beginText, endText);
  
  mGlyphBuf.clear();
  char* beginPtr = *beginText;
  while(mGlyphToks[int(**beginText)] && (*beginText < endText))
    ++(*beginText);

  mGlyphBuf.append(beginPtr, sGetLength(*beginText, beginPtr));
}

bool WfVCDFileParse::parseVar()
{
  // parse until we find $end
  char* beginText;
  char* endText;

  // var type, size, glyph, identifier

  bool isGood = true;
  
  // var type
  WfVCD::SignalType sigType = parseVarType();
  isGood = sigType != WfVCD::eWfVCDSignalTypeINVALID;
  
  UInt32 size = 0;
  if (isGood)
  {
    // size
    if (nextToken(&beginText, &endText))
    {
      mGenericBuf.clear();
      char* numEnd;
      
      size = OSStrToU32(beginText, &numEnd, 10, &mGenericBuf);
      if (numEnd == beginText)
      {
        doErrMsg(mGenericBuf.c_str(), beginText, endText);
        isGood = false;
      }
      else
        beginText = numEnd;
    }
    else
    {
      doErrMsg("Premature end of file. Expected size information", NULL, NULL);
      isGood = false;
    }
    
  }
  
  if (isGood)
  {
    // glyph has to be null terminated. Will just use strings for now
    isGood = getGlyph();
    // glyph is in mGlyphBuf
    
    // verilog identifier
    HdlId info;
    if (isGood)
      isGood = parseVerilogToken(false, &info);
    
    if (isGood)
    {
      WfVCD::Identifier varId(mValueBuf, info);
      mController->doVariable(varId, sigType, size, mGlyphBuf.c_str());
    }
  }
  return isGood;
}

bool WfVCDFileParse::parseFreeText()
{
  // When parsing free text for signal tokens the free text may
  // overlap into another buffer page. We *have* to save the tokens in
  // the alignbuf or another buffer. This is only used during
  // hierarchy parsing

  char* beginPtr;
  char* endPtr;
  const WfVCDToken* tokenObj;

  mGenericBuf.clear();

  bool foundEnd = false;
  // keep looking for $end
  while (! foundEnd && nextToken(&beginPtr, &endPtr))
  {
    tokenObj = WfVCDPerfectHash::get_vcd_toktype(beginPtr, sGetLength(endPtr, beginPtr));
    if (tokenObj && (tokenObj->mType == eWfVCDTokenEnd))
      foundEnd = true;
    else if (scanForEndTok(beginPtr, endPtr))
      foundEnd = true;
    else
    {
      if (*beginPtr == '[') // beginning of range/bit specification
        // we can only get here if there is a space between the
        // identifier and the range/bit specifier. We have
        // inadvertently removed that space. Add it back in.
        mGenericBuf << " ";
      mGenericBuf.append(beginPtr, sGetLength(endPtr, beginPtr));
    }
    
    mLastPosition = getCurrentBufferPosition();    
  }

  if (! foundEnd)
    doErrMsg("Premature end of file. Expected $end", NULL, NULL);
  
  return foundEnd;
}

bool WfVCDFileParse::parseTimescale()
{
  bool isGood = true;
  char* beginText;
  char* endText;

  // Have to do some more work here. 
  // Parse the time value and the time unit

  WfVCD::TimescaleValue timeScale = WfVCD::eWfVCDTimescaleValueINVALID;
  WfVCD::TimescaleUnit timeUnit = WfVCD::eWfVCDTimescaleUnitINVALID;

  // The next token is the time value
  if (nextToken(&beginText, &endText))
  {
    mGenericBuf.clear();
    char* numEnd;

    SInt32 timeValue = OSStrToS32(beginText, &numEnd, 10, &mGenericBuf);

    if (numEnd != beginText)
    {
      beginText = numEnd;
      
      timeScale = mController->evaluateTimescaleValue(timeValue);
      if (timeScale == WfVCD::eWfVCDTimescaleValueINVALID)
      {
        UtString err;
        err << "Invalid timescale: " << timeValue;
        doErrMsg(err.c_str(), NULL, NULL);
        isGood = false;
      }
    }
    else
    {
      UtString err;
      err << "Timescale parse error: " << mGenericBuf;
      doErrMsg(err.c_str(), NULL, NULL);
      isGood = false;
    }
    
  }
  else
  {
    doErrMsg("Premature end of file. Expected timescale value", NULL, NULL);
    isGood = false;
  }
  
  if (isGood)
  {
    // time unit. The unit may already be in the current
    // string. Easily figured out by checking if the current string is
    // at its end.
    if ((beginText != endText) || nextToken(&beginText, &endText))
    {
      // next one or two characters are the timeunit
      // for clarity
      char* unit = beginText;
      timeUnit = mController->evaluateTimescaleUnit(unit);
      if (timeUnit == WfVCD::eWfVCDTimescaleUnitINVALID)
      {
        doErrMsg("Invalid time unit", beginText, endText);
        isGood = false;
      }
      
    }
    else
    {
      doErrMsg("Premature end of file. Expected timescale unit", NULL, NULL);
      isGood = false;
    }
    
  }
  
  return isGood;
}


bool WfVCDFileParse::parseVersion()
{
  bool isGood = parseFreeText();
  if (isGood)
    mController->doVersion(mGenericBuf.c_str());
  return isGood;
}

bool WfVCDFileParse::parseDate()
{
  bool isGood = parseFreeText();
  if (isGood)
    mController->doDate(mGenericBuf.c_str());
  return isGood;
}

bool WfVCDFileParse::parseComment()
{
  bool isGood = parseFreeText();
  if (isGood)
    mController->doComment(mGenericBuf.c_str());
  return isGood;
}

int WfVCDFileParse::getLineNo() const
{
  return mCurLineNo;
}
