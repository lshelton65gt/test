// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtString.h"
#include "util/UtStream.h"
#include "util/UtIOStream.h"
#include "util/AtomicCache.h"
#include "util/DynBitVector.h"
#include "util/UtConv.h"
#include "util/UtUInt64Factory.h"
#include "waveform/WfVCDSignal.h"
#include "waveform/WfVCDSignalVCIter.h"
#include "waveform/WfVCDFileReader.h"
#if pfUNIX
#include "waveform/WfFsdbFileReader.h"
#endif

static int pop_test(const char* filename,  bool isFsdb)
{
  AtomicCache strCache;
  WfVCDBase* vcd;
  
  DynBitVectorFactory dynFactory(true);
  UtUInt64Factory timeFactory;

  if (! isFsdb)
    vcd = new WfVCDFileReader(filename, &strCache, &dynFactory, &timeFactory);
  else
  {
#if pfUNIX
    vcd = WfFsdbFileReaderCreate(filename, &strCache, &dynFactory, &timeFactory);
#else
    return 0;
#endif
  }
  UtString errMsg;
  if (vcd->loadSignals(&errMsg) != WfAbsFileReader::eError)
  {
    UtIO::cerr() << "Bad dump file should have loaded with error." << UtIO::endl;
    return 1;
  }
  else
    // successfully errored. Print error message
    UtIO::cout() << errMsg << UtIO::endl;

  return 0;
}

static void wiggle_signal(WfAbsSignal *s, WfAbsFileReader* fileReader)
{
  UtString name;

  s->getName(&name);
  UtIO::cout() << "Wiggling: " << name << UtIO::endl;


  UtIO::cout() << "Printing all the times." << UtIO::endl;
  UtString buf;
  WfAbsSignalVCIter *si = fileReader->createSignalVCIter(s, &buf);
  if (! buf.empty())
    UtIO::cout() << buf << UtIO::endl;
  
  bool isReal = s->isReal();

  while (!si->atEnd())
  {
    UtString buf;
    CarbonReal realVal = 0;
    if (! isReal)
      si->getValueStr(&buf);
    else
      realVal = si->getValueReal();
    
    UtIO::cout() << si->getTime() << " ";
    if (! isReal)
      UtIO::cout() << buf;
    else
      UtIO::cout() << UtIO::Precision(16) << UtIO::general << realVal;
    UtIO::cout() << UtIO::endl;
    
    si->gotoNextTime();
  }

  UtIO::cout() << "Back up two and print them out." << UtIO::endl;
  si->gotoPrevTime();
  si->gotoPrevTime();

  while (!si->atEnd())
  {
    UtIO::cout() << si->getTime() << UtIO::endl;
    si->gotoNextTime();
  }

  UtIO::cout() << "Print out max and min times." << UtIO::endl;
  UtIO::cout() << si->getMaxTime() << UtIO::endl;
  UtIO::cout() << si->getMinTime() << UtIO::endl;

  UtIO::cout() << "Going to max time." << UtIO::endl;
  si->gotoMaxTime();
  UtIO::cout() << si->getTime() << UtIO::endl;

  UtIO::cout() << "Going to min time." << UtIO::endl;
  si->gotoMinTime();
  UtIO::cout() << si->getTime() << UtIO::endl;

  UtIO::cout() << "Going to time 0." << UtIO::endl;
  si->gotoTime(0);
  UtIO::cout() << si->getTime() << UtIO::endl;

  UtIO::cout() << "Going to time 100000." << UtIO::endl;
  si->gotoTime(100000);
  UtIO::cout() << si->getTime() << UtIO::endl;

  UtIO::cout() << "Going to time 535." << UtIO::endl;
  si->gotoTime(535);
  UtIO::cout() << si->getTime() << UtIO::endl;

  UtIO::cout() << "Going to time 536." << UtIO::endl;
  si->gotoTime(536);
  UtIO::cout() << si->getTime() << UtIO::endl;

  UtIO::cout() << "Going to time 505." << UtIO::endl;
  si->gotoTime(505);
  UtIO::cout() << si->getTime() << UtIO::endl;

  UtIO::cout() << "Going to time 2000." << UtIO::endl;
  si->gotoTime(2000);
  UtIO::cout() << si->getTime() << UtIO::endl;

  UtIO::cout() << "Going to time 1265." << UtIO::endl;
  si->gotoTime(1265);
  UtIO::cout() << si->getTime() << UtIO::endl;

  UtIO::cout() << "Going to time 1999." << UtIO::endl;
  si->gotoTime(1999);
  UtIO::cout() << si->getTime() << UtIO::endl;
  delete si;
}


static int basic_test(const char* filename,  char unlistedval, const char* m1ScopeTail, bool isFsdb)
{
  UtString x;

  AtomicCache strCache;
  WfVCDBase* vcd;
  
  DynBitVectorFactory dynFactory(true);
  UtUInt64Factory timeFactory;

  if (! isFsdb)
    vcd = new WfVCDFileReader(filename, &strCache, &dynFactory, &timeFactory);
  else
  {
#if pfUNIX
    vcd = WfFsdbFileReaderCreate(filename, &strCache, &dynFactory, &timeFactory);
#else
    return 0;
#endif
  }
  vcd->putUnlistedValueSetting(unlistedval);

  UtString errMsg;
  if (vcd->loadSignals(&errMsg) != WfAbsFileReader::eOK)
  {
    UtIO::cerr() << errMsg << UtIO::endl;
    return 1;
  }

  vcd->getSimulatorVersion(x);
  vcd->getSimulationDate(x);

  UtString net1Str;
  net1Str << "top.m1";
  if (m1ScopeTail)
    net1Str << m1ScopeTail;
  net1Str << ".net1";

  WfAbsSignal* dummy = vcd->watchSignal(net1Str);
  //  delete dummy;
  dummy = vcd->watchSignal(UtString("top.t1.index"));
  // delete dummy;
  dummy = vcd->watchSignal(UtString("top.t1.accumulator"));
  // delete dummy;
  dummy = vcd->watchSignal(UtString("top.t1.accumulator[31:0]"));
  // delete dummy;

  UtString sigMsg;
  WfAbsSignal* dummySig = NULL;
  if ((dummySig = vcd->findSignal(net1Str, &sigMsg)))
  {
    UtIO::cout() << "Found A." << UtIO::endl;
  }
  // delete dummySig;
  if ((dummySig = vcd->findSignal(UtString("top.t1.index"), &sigMsg)))
  {
    UtIO::cout() << "Found B." << UtIO::endl;
  }
  // delete dummySig;
  if ((dummySig = vcd->findSignal(UtString("top.t1.accumulator"), &sigMsg)))
  {
    UtIO::cout() << "Found C." << UtIO::endl;
  }
  // delete dummySig;
  if ((dummySig = vcd->findSignal(UtString("top.t1.accumulator[31:0]"), &sigMsg)))
  {
    UtIO::cout() << "Found D." << UtIO::endl;
  }
  // delete dummySig;

  if (! sigMsg.empty())
    UtIO::cout() << sigMsg << UtIO::endl;
  
  UtString name;
  WfAbsSignal * s1;
  WfAbsSignal * s2;
  WfAbsSignal * s3;
  s1 = vcd->watchSignal(net1Str);
  s2 = vcd->watchSignal(UtString("top.t1.index"));
  s3 = vcd->watchSignal(UtString("top.t1.accumulator[31:0]"));

  UtString msg;
  WfAbsFileReader::Status loadStat = vcd->loadValues(&msg);
  if (loadStat != WfAbsFileReader::eOK)
  {
    UtIO::cout() << msg << UtIO::endl;
    if (loadStat == WfAbsFileReader::eError)
      return 1;
  }

  wiggle_signal(s1, vcd);
  wiggle_signal(s2, vcd);
  wiggle_signal(s3, vcd);

  // delete s1;
  // delete s2;
  // delete s3;

  vcd->unloadSignals();
  vcd->unloadValues();

  delete vcd;
  return 0;
}



int main(int, char**)
{
  int stat = 0;
  bool isFsdb = false;
  const char* fileExt = "";
  
  for (int i = 0; i < 2; ++i)
  {
    // example.dump has syntax that debussy cannot read
    if (i == 0)
    {
      UtIO::cout() << "FILE: example.dump" << UtIO::endl;
      if (basic_test("example.dump", 'x', NULL, false) != 0)
      {
        ++stat;
      }
    }

    UtString file = "bug1045.dump";
    file << fileExt;
    UtIO::cout() << "FILE: " << file << UtIO::endl;
    if (basic_test(file.c_str(), '0', NULL, isFsdb) != 0)
    {
      ++stat;
    }
    
    file = "bug958.dump";
    file << fileExt;
    UtIO::cout() << "FILE: " << file << UtIO::endl;
    if (basic_test(file.c_str(), 'x', NULL, isFsdb) != 0)
    {
      ++stat;
    }
    
    file = "real-number.dump";
    file << fileExt;
    UtIO::cout() << "FILE: " << file << UtIO::endl;
    if (basic_test(file.c_str(), 'x', NULL, isFsdb) != 0)
    {
      ++stat;
    }
    
    file = "bug472.dump";
    file << fileExt;
    UtIO::cout() << "FILE: " << file << UtIO::endl;
    if (basic_test(file.c_str(), 'x', NULL, isFsdb) != 0)
    {
      ++stat;
    }

    file = "bug2043.dump";
    file << fileExt;
    UtIO::cout() << "FILE: " << file << UtIO::endl;
    if (basic_test(file.c_str(), 'x', "[0]", isFsdb) != 0)
    {
      ++stat;
    }

    file = "bug4869.dump";
    file << fileExt;
    UtIO::cout() << "FILE: " << file << UtIO::endl;
    if (pop_test(file.c_str(), isFsdb) != 0)
    {
      ++stat;
    }


    isFsdb = true;
    fileExt = ".fsdb";
  }
  
  return stat;
}
