// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "waveform/WfFsdbReaderConduit.h"
#include "ffrAPI.h"

FsdbReaderFfrObjectID* fsdbReaderFfrOpen2(str_T fname, fsdbTreeCBFunc tree_cb_func,
                                          void* tree_client_data, ffrOpenOption opt)
{
  return ffrObject::ffrOpen2(fname, tree_cb_func, tree_client_data, opt);
}

  
fsdbFileType fsdbReaderFfrGetFileType(FsdbReaderFfrObjectID* id)
{
  return id->ffrGetFileType();
}

void fsdbReaderFfrClose(FsdbReaderFfrObjectID* id)
{
  id->ffrClose();
}

str_T fsdbReaderFfrGetSimVersion(FsdbReaderFfrObjectID* id)
{
  return id->ffrGetSimVersion();
}

str_T fsdbReaderFfrGetSimDate(FsdbReaderFfrObjectID* id)
{
  return id->ffrGetSimDate();
}

str_T fsdbReaderFfrGetScaleUnit(FsdbReaderFfrObjectID* id)
{
  return id->ffrGetScaleUnit();
}


fsdbRC fsdbReaderFfrExtractScaleUnit(str_T buf, uint_T* digit, char** unit)
{
  return ffrObject::ffrExtractScaleUnit(buf, *digit, *unit);
}

fsdbRC fsdbReaderFfrReadScopeVarTree(FsdbReaderFfrObjectID* id)
{
  return id->ffrReadScopeVarTree();
}

void fsdbReaderFfrAddToSignalList(FsdbReaderFfrObjectID* id, int idCode)
{
  id->ffrAddToSignalList(idCode);
}

void fsdbReaderFfrLoadSignals(FsdbReaderFfrObjectID* id)
{
  id->ffrLoadSignals();
}

void fsdbReaderFfrUnloadSignals(FsdbReaderFfrObjectID* id)
{
  id->ffrUnloadSignals();
}

FsdbReaderFfrVCTrvsHdl* fsdbReaderFfrCreateVCTraverseHandle(FsdbReaderFfrObjectID* id, int idCode)
{
  return id->ffrCreateVCTraverseHandle(idCode);
}

bool_T fsdbReaderTrvsHdlFfrHasIncoreVC(FsdbReaderFfrVCTrvsHdl* trvsID)
{
  return trvsID->ffrHasIncoreVC();
}

void fsdbReaderTrvsHdlFfrGetMaxXTag(FsdbReaderFfrVCTrvsHdl* trvsID, fsdbTag64* timeVal)
{
  trvsID->ffrGetMaxXTag(static_cast<void*>(timeVal));
}

void fsdbReaderTrvsHdlFfrGetXTag(FsdbReaderFfrVCTrvsHdl* trvsID, fsdbTag64* timeVal)
{
  trvsID->ffrGetXTag(static_cast<void*>(timeVal));
}

void fsdbReaderTrvsHdlFfrGetVC(FsdbReaderFfrVCTrvsHdl* trvsID, byte_T** vc)
{
  trvsID->ffrGetVC(vc);
}

fsdbBytesPerBit fsdbReaderTrvsHdlFfrGetBytesPerBit(FsdbReaderFfrVCTrvsHdl* trvsID)
{
  return trvsID->ffrGetBytesPerBit();
}

uint_T fsdbReaderTrvsHdlFfrGetBitSize(FsdbReaderFfrVCTrvsHdl* trvsID)
{
  return trvsID->ffrGetBitSize();
}

fsdbRC fsdbReaderTrvsHdlFfrGotoNextVC(FsdbReaderFfrVCTrvsHdl* trvsID)
{
  return trvsID->ffrGotoNextVC();
}

void fsdbReaderTrvsHdlFfrFree(FsdbReaderFfrVCTrvsHdl* trvsID)
{
  trvsID->ffrFree();
}

fsdbRC fsdbReaderFfrCheckFSDB(const char* fileName)
{
  return ffrObject::ffrCheckFSDB(const_cast<char*>(fileName));
}
