// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "waveform/WfVCDSignal.h"
#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "hdl/HdlVerilogPath.h"
#include "symtab/STSymbolTableNode.h"
#include "symtab/STSymbolTable.h"

WfVCDSignal::WfVCDSignal(WfVCDFileReader::ISignal* signal, const WfVCDTimeValueList* values) : WfAbsSignal(), mSignal(signal), mValues(values)
{
}


WfVCDSignal::~WfVCDSignal(void)
{
}


void WfVCDSignal::getName(UtString* name) const
{
  HdlVerilogPath pather;
  pather.compPathHier(mSignal->getName(), name, NULL);
}


UInt32 WfVCDSignal::getWidth(void) const
{
  return mSignal->getWidth();
}

SInt32 WfVCDSignal::getLsb(void) const
{
  return mSignal->getLsb();
}

SInt32 WfVCDSignal::getMsb(void) const
{
  return mSignal->getMsb();
}

bool WfVCDSignal::isReal() const
{
  return mSignal->getSignalType() == WfVCD::eWfVCDSignalTypeReal;
}

WfAbsSignalVCIter * WfVCDSignal::createSignalVCIter(void)
{
  ST_ASSERT(mValues, mSignal->getName());
  return new WfVCDSignalVCIter(mValues, this);
}

const WfVCDFileReader::ISignal* WfVCDSignal::getInternalSignal() const
{
  return mSignal;
}

const STSymbolTableNode* WfVCDSignal::getSymNode() const
{
  return mSignal->getName();
}

void WfVCDSignal::print() const
{
  UtString buf;
  getName(&buf);
  UtIO::cout() << "VCD Signal: " << buf << UtIO::endl;
  UtIO::cout().flush();
}

void WfVCDSignal::getLastValue(DynBitVector* val, DynBitVector* drv) const
{
  const DynBitVector* completeVal;
  const WfVCDTimeValue* lastVal = mValues->back();
  lastVal->getValue(&completeVal);
  WfVCDSignalVCIter::calcValueDrive(val, drv, completeVal, getWidth());
}

WfVCDSignal* WfAbsSignal::castVCDSignal() {  return NULL; }
WfVCDSignal* WfVCDSignal::castVCDSignal() {  return this; }

