// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*
  Authors:  Mark Seneski

  \file
  This file is not compiled yet. It has bogus code right now that
  reminds me how fsdb hierarchy traversal works.
*/

#include "waveform/WfFsdbFileReader.h"
#include "waveform/WfFsdbReaderConduit.h"

#include "hdl/HdlVerilogPath.h"
#include "hdl/HdlId.h"
#include "util/UtConv.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtVector.h"
#include "waveform/WfVCDBase.h"
#include "symtab/STSymbolTable.h"

class WfFsdbFileReader::FsdbIdHash
{
public: 
  CARBONMEM_OVERRIDES

  FsdbIdHash()
  {}

  ~FsdbIdHash()
  {
  }

  typedef UtHashMap<int, WfVCDBase::FsdbId*> NodeMap;
  typedef NodeMap::UnsortedLoop IdIter;
  
  WfVCDBase::FsdbId* find(int idCode) {
    const FsdbIdHash* me = const_cast<const FsdbIdHash*>(this);
    return const_cast<WfVCDBase::FsdbId*>(me->find(idCode));
  }

  IdIter loopIds() {
    return mHash.loopUnsorted();
  }

  const WfVCDBase::FsdbId* find(int idCode) const
  {
    const WfVCDBase::FsdbId* ret = NULL;

    NodeMap::const_iterator p = mHash.find(idCode);
    if (p != mHash.end())
      ret = p->second;
    return ret;
  }

  void insert(int idCode, WfVCDBase::FsdbId* node)
  {
    mHash[idCode] = node;
  }

  void deleteIds()
  {
    mHash.clearPointerValues();
  }

  UInt32 size() const
  {
    return mHash.size();
  }

private:
  NodeMap mHash;
  
  // forbid
  FsdbIdHash(const FsdbIdHash&);
  FsdbIdHash& operator=(const FsdbIdHash&);
};

class WfFsdbFileReader::Buffer
{
public:
  CARBONMEM_OVERRIDES

  Buffer(char* buf) : mType(eBits)
  {
    mVal.mStrBuf = buf;
  }
  
  enum Type { eBits, eReal, eInt };
  
  void putType(Type type)
  {
    mType = type;
  }
  
  Type getType() const { return mType; }
  
  UInt32 getInt() const { return mVal.mInt; }
  CarbonReal getReal() const { return mVal.mReal; }
  
  char* getBuf() { 
    mType = eBits;
    return mVal.mStrBuf; 
  }
  void putInt(UInt32 val) { 
    mType = eInt;
    mVal.mInt = val; 
  }
  
  void putReal(CarbonReal val) { 
    mType = eReal;
    mVal.mReal = val; 
  }
  
private:
  Type mType;
  union Val {
    char* mStrBuf;
    UInt32 mInt;
    CarbonReal mReal;
  };
  Val mVal;
};

class WfFsdbFileReader::ReadObject
{
public: 
  CARBONMEM_OVERRIDES

  ReadObject(WfFsdbFileReader* waveReader) : mObj(NULL), 
                                             mWave(waveReader) 
  {}

  ~ReadObject() 
  {
    closeFile();
  }

  static ReadObject* open(const char* fileName, 
                          WfFsdbFileReader* waveReader)
  {
    ReadObject* ret = new ReadObject(waveReader);
    char* castFileName = const_cast<char*>(fileName);
    ret->mObj = fsdbReaderFfrOpen2(castFileName, __MyTreeCB, ret, FFR_OPEN_OPT_DFT);
    fsdbFileType fileType = fsdbReaderFfrGetFileType(ret->mObj);
    if ((FSDB_FT_VERILOG != fileType) &&
        (FSDB_FT_VHDL != fileType) &&
        (FSDB_FT_VERILOG_VHDL != fileType))
    {
      fprintf(stderr, 
              "%s is not a verilog or vhdl fsdb.\n", fileName);
      delete ret;
      ret = NULL;
    }
    
    return ret;
  }
  
  FsdbReaderFfrObjectID* getObject() 
  {
    return mObj;
  }

  WfFsdbFileReader* getWave()
  {
    return mWave;
  }

  void closeFile()
  {
    if (mObj)
    {
      fsdbReaderFfrClose(mObj);
      mObj = NULL;
    }
  }

  bool_T processFsdbScope(fsdbTreeCBDataScope* scope)
  {
    
    WfVCD::ScopeType scopeType = WfVCD::eWfVCDScopeTypeINVALID;
    
    bool_T ret = true;
    
    switch (scope->type) 
    {
    case FSDB_ST_VCD_MODULE:
      scopeType = WfVCD::eWfVCDScopeTypeModule;
      break;
    case FSDB_ST_VCD_TASK:
      scopeType = WfVCD::eWfVCDScopeTypeTask;
      break;
    case FSDB_ST_VCD_FUNCTION:
      scopeType = WfVCD::eWfVCDScopeTypeFunction;
      break;
    case FSDB_ST_VCD_BEGIN:
      scopeType = WfVCD::eWfVCDScopeTypeBegin;
      break;
    case FSDB_ST_VCD_FORK:
      scopeType = WfVCD::eWfVCDScopeTypeFork;
      break;
    case FSDB_ST_VHDL_ARCHITECTURE:
      scopeType = WfVCD::eWfVhdlArchitecture;
      break;
    case FSDB_ST_VHDL_PROCEDURE:
      scopeType = WfVCD::eWfVhdlProcedure;
      break;
    case FSDB_ST_VHDL_FUNCTION:
      scopeType = WfVCD::eWfVhdlFunction;
      break;
    case FSDB_ST_VHDL_RECORD:
      scopeType = WfVCD::eWfVhdlRecord;
      break;
    case FSDB_ST_VHDL_PROCESS:
      scopeType = WfVCD::eWfVhdlProcess;
      break;
    case FSDB_ST_VHDL_BLOCK:
      scopeType = WfVCD::eWfVhdlBlock;
      break;
    case FSDB_ST_VHDL_FOR_GENERATE:
      scopeType = WfVCD::eWfVhdlForGenerate;
      break;
    case FSDB_ST_VHDL_IF_GENERATE:
      scopeType = WfVCD::eWfVhdlIfGenerate;
      break;
    default:
      {
        UtString typeValue;
        typeValue << (SInt32) scope->type;
        
        UtString msg;
        msg << scope->name;
        msg << ": Unrecognized fsdb scope";
        mWave->doErrMsg(msg.c_str(), eError, typeValue.c_str());
        
        ret = false;
      }
      break;
    }

    ret = ret && pushCarbonScope(scope->name, scopeType);

    return ret;
  }

  bool pushCarbonScope(const char* scopeName, WfVCD::ScopeType scopeType)
  {
    bool ret = true;

    UtString name;
    HdlId info;
    HdlVerilogPath pather;
    const char* start = scopeName;
    const char* end = start + strlen(start);
    if (pather.makeLegalId(&start, end, &name, &info, true) == HdlHierPath::eIllegal)
    {
      mWave->doErrMsg("illegal scope name", eError, scopeName);
      ret = false;
    }
    else
    {
      WfVCD::Identifier scopeId(name);
      mWave->doPushScope(scopeId, scopeType);
    }
    return ret;
  }

  bool popCarbonScope()
  {
    bool ret = mWave->doPopScope();
    if (! ret)
      mWave->doErrMsg("Corrupt fsdb file", WfAbsFileReader::eError, "More pop scopes than push scopes");
    return ret;
  }

  bool_T processFsdbVar(fsdbTreeCBDataVar* var)
  {
    bool_T ret = true;
  
    WfVCD::SignalType sigType = WfVCD::eWfVCDSignalTypeINVALID;

    const char* unsupportedType = NULL;
    
    bool isVerilog = true;

    switch (var->type) {
    case FSDB_VT_VCD_EVENT:
      sigType = WfVCD::eWfVCDSignalTypeEvent;
      break;
    
    case FSDB_VT_VCD_INTEGER:
      sigType = WfVCD::eWfVCDSignalTypeInteger;
      break;
    
    case FSDB_VT_VCD_PARAMETER:
      sigType = WfVCD::eWfVCDSignalTypeParameter;
      break;
    
    case FSDB_VT_VCD_REAL:
      sigType = WfVCD::eWfVCDSignalTypeReal;
      break;
    
    case FSDB_VT_VCD_REG:
      sigType = WfVCD::eWfVCDSignalTypeReg;
      break;
    
    case FSDB_VT_VCD_SUPPLY0:
      sigType = WfVCD::eWfVCDSignalTypeSupply0;
      break;
    
    case FSDB_VT_VCD_SUPPLY1:
      sigType = WfVCD::eWfVCDSignalTypeSupply1;
      break;
    
    case FSDB_VT_VCD_TIME:
      sigType = WfVCD::eWfVCDSignalTypeTime;
      break;
    
    case FSDB_VT_VCD_TRI:
      sigType = WfVCD::eWfVCDSignalTypeTri;
      break;
    
    case FSDB_VT_VCD_TRIAND:
      sigType = WfVCD::eWfVCDSignalTypeTriAnd;
      break;
    
    case FSDB_VT_VCD_TRIOR:
      sigType = WfVCD::eWfVCDSignalTypeTriOr;
      break;
    
    case FSDB_VT_VCD_TRIREG:
      sigType = WfVCD::eWfVCDSignalTypeTriReg;
      break;
    
    case FSDB_VT_VCD_TRI0:
      sigType = WfVCD::eWfVCDSignalTypeTri0;
      break;
    
    case FSDB_VT_VCD_TRI1:
      sigType = WfVCD::eWfVCDSignalTypeTri1;
      break;
    
    case FSDB_VT_VCD_WAND:
      sigType = WfVCD::eWfVCDSignalTypeWAnd;
      break;
    
    case FSDB_VT_VCD_WIRE:
      sigType = WfVCD::eWfVCDSignalTypeWire;
      break;
    
    case FSDB_VT_VCD_WOR:
      sigType = WfVCD::eWfVCDSignalTypeWOr;
      break;
    
    case FSDB_VT_VHDL_SIGNAL:
      sigType = WfVCD::eWfVhdlSignal;
      isVerilog = false;
      break;
    
    case FSDB_VT_VHDL_VARIABLE:
      sigType = WfVCD::eWfVhdlVariable;
      isVerilog = false;
      break;
    
    case FSDB_VT_VHDL_CONSTANT:
      sigType = WfVCD::eWfVhdlConstant;
      isVerilog = false;
      break;
    
    case FSDB_VT_VHDL_FILE:
      sigType = WfVCD::eWfVhdlFile;
      isVerilog = false;
      break;

    case FSDB_VT_VHDL_MEMORY:
      isVerilog = false;
      unsupportedType = "FSDB_VT_VHDL_MEMORY";
      ret = false;
      break;
    case FSDB_VT_VHDL_MEMORY_DEPTH:
      isVerilog = false;
      // memory depth and range have the same case value
      unsupportedType = "FSDB_VT_VHDL_MEMORY_DEPTH/RANGE";
      ret = false;
      break;
    default:
      {
        UtString typeValue;
        typeValue << (SInt32) var->type;
        ret = false;
        UtString buf;
        buf << var->name;
        buf << ": Unknown var type";
        mWave->doErrMsg(buf.c_str(), eError, typeValue.c_str());
      }
      break;
    }

    if (unsupportedType)
      mWave->doErrMsg("Unsupported type", eError, unsupportedType);

    if (ret)
    {
      UtString name;
      HdlId info;
      HdlVerilogPath pather;
      const char* start = var->name;
      const char* end = start + strlen(start);
      UInt32 size = (std::abs(var->rbitnum - var->lbitnum) + 1);
      // Currently, we assume pods with bpb > 1
      if (var->bytes_per_bit == 2)
        size = 16;
      else if (var->bytes_per_bit == 4)
        size = 32;
      else if (var->bytes_per_bit == 8)
        size = 64;

      if (pather.makeLegalId(&start, end, &name, &info, false) == HdlHierPath::eIllegal)
      {
        mWave->doErrMsg("illegal var name", eError, var->name);
        ret = false;
      }
      else
      {
        WfVCD::Identifier varId(name, info);
        mWave->doVariable(varId, sigType, size, var->u.idcode, isVerilog);
      }
    }
    return ret;
  }
  
  void transformFsdbValue(Buffer* valbuf, 
                          FsdbReaderFfrVCTrvsHdl* vcTrvsHdl, 
                          byte_T* vc,
                          FsdbId* idObj)
  {
    uint_T i;
    uint_T numBits;
    
    char* buffer;

    switch (fsdbReaderTrvsHdlFfrGetBytesPerBit(vcTrvsHdl)) {
    case FSDB_BYTES_PER_BIT_1B:
      
      //
      // Convert each verilog bit type to corresponding
      // character.
      //
      numBits = fsdbReaderTrvsHdlFfrGetBitSize(vcTrvsHdl);
      buffer = valbuf->getBuf();
      if (idObj->isVerilog())
      {
        for (i = 0; i < numBits; i++) {
          switch(vc[i]) {
          case FSDB_BT_VCD_0:
            buffer[i] = '0';
            break;
            
          case FSDB_BT_VCD_1:
            buffer[i] = '1';
            break;
            
          case FSDB_BT_VCD_X:
            buffer[i] = 'x';
            break;
            
          case FSDB_BT_VCD_Z:
            buffer[i] = 'z';
            break;
            
          default:
            //
            // unknown verilog bit type found.
            //
            buffer[i] = 'u';
          }
        }
      }
      else 
      {
        // currently, can only be vhdl

        for (i = 0; i < numBits; i++) {
          switch(vc[i]) {
          case FSDB_BT_VHDL_STD_ULOGIC_U:
          case FSDB_BT_VHDL_STD_ULOGIC_X:
          case FSDB_BT_VHDL_STD_ULOGIC_W:
          case FSDB_BT_VHDL_STD_ULOGIC_DASH:
            // 'x'
            buffer[i] = 'x';
            break;
          case FSDB_BT_VHDL_STD_ULOGIC_0:
          case FSDB_BT_VHDL_STD_ULOGIC_L:
            // '0'
            buffer[i] = '0';
            break;
          case FSDB_BT_VHDL_STD_ULOGIC_1:
          case FSDB_BT_VHDL_STD_ULOGIC_H:
            // '1'
            buffer[i] = '1';
            break;
          case FSDB_BT_VHDL_STD_ULOGIC_Z:
            // 'z'
            buffer[i] = 'z';
            break;
          default:
            //
            // unknown vhdl bit type found.
            //
            buffer[i] = 'u';
          }
        }
      }
      buffer[i] = '\0';
      break;

    case FSDB_BYTES_PER_BIT_2B:
      {
        //
        // Not 0, 1, x, z since their bytes per bit is
        // FSDB_BYTES_PER_BIT_1B. 
        
        // Just treat a short as an integer
        UInt16* castShort = (UInt16*)vc;
        valbuf->putInt(*castShort);
        break;
      }
    case FSDB_BYTES_PER_BIT_4B:
      //
      // Not 0, 1, x, z since their bytes per bit is
      // FSDB_BYTES_PER_BIT_1B. 
      
      // integer
      valbuf->putInt(*((UInt32*)vc));
      break;
      
    case FSDB_BYTES_PER_BIT_8B:
      //
      // Not 0, 1, x, z since their bytes per bit is
      // FSDB_BYTES_PER_BIT_1B. 
      //
      // For verilog type fsdb, there is no array of 
      // real/float/double so far, so we don't have to
      // care about that kind of case.
      //
      valbuf->putReal(*((CarbonReal*)vc));
      break;
    default:
      INFO_ASSERT(0, "Fsdb Error: Control flow should not reach here.");
      break;
    }
  }

static bool_T __MyTreeCB(fsdbTreeCBType cb_type, 
                         void *client_data, 
                         void *tree_cb_data)
{
  ReadObject* me = static_cast<ReadObject*>(client_data);
  bool_T ret = true;
  WfFsdbFileReader* wave = me->getWave();
  fsdbTreeCBDataRecordBegin* record_begin = NULL;
  //fsdbTreeCBDataArrayBegin* array_begin = NULL;
  
  switch (cb_type)
  {
  case FSDB_TREE_CBT_BEGIN_TREE:
    break;
  case FSDB_TREE_CBT_SCOPE:
    ret = me->processFsdbScope(static_cast<fsdbTreeCBDataScope*>(tree_cb_data));
    break;
  case FSDB_TREE_CBT_VAR:
    {
      fsdbTreeCBDataVar* var = static_cast<fsdbTreeCBDataVar*>(tree_cb_data);
      if ((var->type != FSDB_VT_VCD_MEMORY) && 
          (var->type != FSDB_VT_VCD_MEMORY_DEPTH) &&
          (var->type != FSDB_VT_VCD_MEMORY_RANGE))
        ret = me->processFsdbVar(var);
    }
    break;
  case FSDB_TREE_CBT_UPSCOPE:
    ret = me->popCarbonScope();
    break;
  case FSDB_TREE_CBT_END_TREE:
    wave->clearScopes();
    break;
    
  case FSDB_TREE_CBT_FILE_TYPE:
    break;
    
  case FSDB_TREE_CBT_SIMULATOR_VERSION:
    break;
    
  case FSDB_TREE_CBT_SIMULATION_DATE:
    break;
    
  case FSDB_TREE_CBT_X_AXIS_SCALE:
    break;
    
  case FSDB_TREE_CBT_END_ALL_TREE:
    break;
    
  case FSDB_TREE_CBT_ARRAY_BEGIN:
    break;

  case FSDB_TREE_CBT_ARRAY_END:
    break;
    
  case FSDB_TREE_CBT_RECORD_BEGIN:
    record_begin = (fsdbTreeCBDataRecordBegin*) tree_cb_data;
    ret = me->pushCarbonScope(record_begin->name, WfVCD::eWfVhdlRecord);
    break;
    
  case FSDB_TREE_CBT_RECORD_END:
    ret = me->popCarbonScope();
    break;
    
  default:
    {
      UtString typeValue;
      typeValue << (SInt32) cb_type;
      wave->doErrMsg("Unrecognized Tree CB type", eError, typeValue.c_str());
      ret = false;
    }
    break;
    
  }
  return ret;
}
  
private:
  FsdbReaderFfrObjectID* mObj;
  WfFsdbFileReader* mWave;
};

struct WfFsdbFileReader::TrvsHdlId
{
  CARBONMEM_OVERRIDES
  TrvsHdlId(FsdbReaderFfrVCTrvsHdl* hdl, FsdbId* id) :
    mHdl(hdl), mId(id), mLink(NULL)
  {}

  FsdbReaderFfrVCTrvsHdl* mHdl;
  FsdbId* mId;
  TrvsHdlId* mLink;
};

WfFsdbFileReader::WfFsdbFileReader(const char* filename, AtomicCache* strCache, DynBitVectorFactory* dynFactory, UtUInt64Factory* timeFactory, WfAbsControl* control) :
  WfVCDBase(filename, strCache, dynFactory, timeFactory, control), mFsdbObj(NULL)
{
  mKnownIds = new FsdbIdHash;
  mWatchedSignals = new FsdbIdHash;
  mErrMsg = NULL;
  mStat = eOK;
}

WfFsdbFileReader::~WfFsdbFileReader()
{
  delete mWatchedSignals;  
  mKnownIds->deleteIds();
  delete mKnownIds;
}

bool WfFsdbFileReader::openFile(UtString* reason)
{
  char* fileName = const_cast<char*>(mFilename.c_str());
  if (FSDB_RC_SUCCESS != fsdbReaderFfrCheckFSDB(fileName))
  {
    *reason << fileName << ": is not regular, readable, or is not an fsdb file.";
    return false;
  }
  
  mFsdbObj = ReadObject::open(fileName, this);
  if (NULL == mFsdbObj) {
    *reason << "Unable to open fsdb file: " << fileName;
    return false;
  }

  return true;
}

WfAbsFileReader::Status 
WfFsdbFileReader::loadSignals(UtString* errMsg)
{
  if (! openFile(errMsg))
    return eError;

  mErrMsg = errMsg;

  INFO_ASSERT(mFsdbObj, "FSDB reader object has not been created.");

  FsdbReaderFfrObjectID* fsdbObj = mFsdbObj->getObject();
  str_T buf = fsdbReaderFfrGetSimVersion(fsdbObj);
  if (buf)
    doVersion(buf);
  buf = fsdbReaderFfrGetSimDate(fsdbObj);
  if (buf)
    doDate(buf);
  
  buf = fsdbReaderFfrGetScaleUnit(fsdbObj);
  INFO_ASSERT(buf, "ffrGetScaleUnit failed.");
  char* unit;
  uint_T digit;
  INFO_ASSERT (fsdbReaderFfrExtractScaleUnit(buf, &digit, &unit) == FSDB_RC_SUCCESS, "ffrExtractScaleUnit failed.");
  
  WfVCD::TimescaleValue timeScale = evaluateTimescaleValue(SInt32(digit));
  WfVCD::TimescaleUnit timeUnit = evaluateTimescaleUnit(unit);
  
  if (timeScale == WfVCD::eWfVCDTimescaleValueINVALID)
  {
    UtString strNum;
    strNum << digit;
    doErrMsg("Invalid timescale value", eWarning,  strNum.c_str());
  }
  
  if (timeUnit == WfVCD::eWfVCDTimescaleUnitINVALID)
    doErrMsg("Invalid timescale unit", eWarning, unit);
   

  if (mStat != eError)
  {
    if (fsdbReaderFfrReadScopeVarTree(fsdbObj) != FSDB_RC_SUCCESS)
      mStat = eError;
    else
      endLoadSignals();
  }

  return mStat;
}

static UInt64 sTransformFsdbTime(const fsdbTag64& time)
{
  UInt64 retTime = 0;
  UInt32 timeArr[2];
  timeArr[0] = time.L;
  timeArr[1] = time.H;
  CarbonValRW::cpSrcToDest(&retTime, timeArr, 2);
  return retTime;
}



void WfFsdbFileReader::insertTime(TrvsHdlId* hdlId, TimeTrvsHdlMap* timeTrvsHdls)
{
  fsdbTag64 timeVal;
  fsdbReaderTrvsHdlFfrGetXTag(hdlId->mHdl, &timeVal);
  UInt64 transTime = sTransformFsdbTime(timeVal);
  
  TimeTrvsHdlMap::value_type insertVal(transTime, hdlId);
  
  std::pair<TimeTrvsHdlMap::iterator, bool> insertStat = timeTrvsHdls->insert(insertVal);
  if (! insertStat.second)
  {
    TrvsHdlId* mappedId = (insertStat.first)->second;
    hdlId->mLink = mappedId->mLink;
    mappedId->mLink = hdlId;
  }
}

void WfFsdbFileReader::addValueChanges(TrvsHdlId* lowestId)
{
  byte_T* vc;
  static char valBuf[FSDB_MAX_BIT_SIZE+1];

  for (TrvsHdlId* hdlId = lowestId; hdlId != NULL; hdlId = hdlId->mLink)
  {
    Buffer bufObj(valBuf);
    FsdbReaderFfrVCTrvsHdl* vcTrvsHdl = hdlId->mHdl;
    FsdbId* idObj = hdlId->mId;
    fsdbReaderTrvsHdlFfrGetVC(vcTrvsHdl, &vc);
    mFsdbObj->transformFsdbValue(&bufObj, vcTrvsHdl, vc, idObj);
    switch (bufObj.getType())
    {
    case Buffer::eBits:
      addValueChange(idObj, valBuf);
      break;
    case Buffer::eReal:
      addValueChangeReal(idObj,  bufObj.getReal());
      break;
    case Buffer::eInt:
      addValueChangeInt(idObj, bufObj.getInt());
      break;
    }
  } // for
}

void WfFsdbFileReader::processWatchedSignals(TimeTrvsHdlMap& timeTrvsHdls)
{
  bool stop = false;
  while (! stop && ! timeTrvsHdls.empty())
  {
    /*
      Stage 1: Find lowest time changes
      
      The pending queue is traversed. The lowest time changes get put
      into the lowestChanges queue and removed from the pending queue.
    */
    UInt64 lowestTime;
    TrvsHdlId* lowestId;
    {
      TimeTrvsHdlMap::iterator q = timeTrvsHdls.begin();
      lowestTime = q->first;
      lowestId = q->second;
      timeTrvsHdls.erase(q);
    }
    
    /* 
       Stage 2: Grab value changes and pass them to the controller.
    */
    // first, set the sim time
    stop = advanceSimulationTime(lowestTime);
    if (! stop)
    {
      addValueChanges(lowestId);
    
      /*
        Stage 3: Iterator advance
        
        Advance time on the list of signals we know need to have
        time advanced. During this stage, every successful advance gets
        put into the pending queue.
        The list is based on the signals that had the lowest time
        value.
      */
      for (TrvsHdlId *hdlId = lowestId, *tmpId = lowestId; 
           hdlId != NULL; /* blank */)
      {
        tmpId = hdlId;
        hdlId = hdlId->mLink;
        tmpId->mLink = NULL;
        
        if (FSDB_RC_SUCCESS == fsdbReaderTrvsHdlFfrGotoNextVC(tmpId->mHdl))
          insertTime(tmpId, &timeTrvsHdls);
      }
    }
  }
  
  // unload the last of the value changes
  if (! stop)
    endLoadValues();
}

void WfFsdbFileReader::loadWatchedSignals(TimeTrvsHdlMap* timeTrvsHdls, TrvsHdlVec* allTrvsHdls)
{
  // Tell debussy which signals in which we are interested
  FsdbReaderFfrObjectID* fsdbObj = mFsdbObj->getObject();
  
  for (FsdbIdHash::IdIter p = mWatchedSignals->loopIds();
       ! p.atEnd(); ++p)
  {
    int idCode = p.getKey();
    fsdbReaderFfrAddToSignalList(fsdbObj, idCode);
  }
  
  // load them
  fsdbReaderFfrLoadSignals(fsdbObj);
  
  allTrvsHdls->reserve(mWatchedSignals->size());
  
  // Grab the value changes and monitor time
  for (FsdbIdHash::IdIter p = mWatchedSignals->loopIds();
       ! p.atEnd(); ++p)
  {
    int idCode = p.getKey();
    FsdbId* obj = p.getValue();
    FsdbReaderFfrVCTrvsHdl* vcTrvsHdl = fsdbReaderFfrCreateVCTraverseHandle(fsdbObj, idCode);
    if (vcTrvsHdl == NULL)
    {
      // very bad situation. Ran out of memory? print to stderr and
      // exit
      fprintf(stderr, "FATAL: Failed to create an FSDB traverse handle(%d)\n", 
              idCode);
      exit(FSDB_RC_OBJECT_CREATION_FAILED);
    }
    
    if (fsdbReaderTrvsHdlFfrHasIncoreVC(vcTrvsHdl))
    {
      fsdbTag64 time;
      UInt64 transTime;
      
      fsdbReaderTrvsHdlFfrGetMaxXTag(vcTrvsHdl, &time);
      transTime = sTransformFsdbTime(time);
      if (transTime > mMaxSimTime)
        mMaxSimTime = transTime;
      
      TrvsHdlId* hdlId = new TrvsHdlId(vcTrvsHdl, obj);
      allTrvsHdls->push_back(hdlId);
      insertTime(hdlId, timeTrvsHdls);
    }
    else
      allTrvsHdls->push_back(NULL);
  }
}

void WfFsdbFileReader::unloadWatchedSignals(TrvsHdlVec* allTrvsHdls)
{
  for (TrvsHdlVec::iterator q = allTrvsHdls->begin(), qe = allTrvsHdls->end();
       q != qe; ++q)
  {
    TrvsHdlId* hdlId = *q;
    if (hdlId)
    {
      fsdbReaderTrvsHdlFfrFree(hdlId->mHdl);
      delete hdlId;
    }
  }
  
  // Delete loaded signals  
  FsdbReaderFfrObjectID* fsdbObj = mFsdbObj->getObject();
  fsdbReaderFfrUnloadSignals(fsdbObj);
}
  
WfAbsFileReader::Status 
WfFsdbFileReader::loadValues(UtString* msg)
{
  msg->clear();
  if (mValsRead)
  {
    *msg << "Fsdb Values have already been loaded. They cannot be loaded again.";
    return eError;
  }

  mErrMsg = msg;
  TimeTrvsHdlMap timeTrvsHdls;
  TrvsHdlVec allTrvsHdls;
  loadWatchedSignals(&timeTrvsHdls, &allTrvsHdls);

  // The following is a pipeline. See comments at each stage
  processWatchedSignals(timeTrvsHdls);
  
  // Free up the traverse handles
  unloadWatchedSignals(&allTrvsHdls);

  return mStat;
}

void WfFsdbFileReader::doErrMsg(const char* msg, Status stat, const char* postMsg) const
{
  if (! mErrMsg->empty())
    *mErrMsg << "\n";
  *mErrMsg << mFilename << ": " << msg;
  if (postMsg)
    *mErrMsg << ": " << postMsg;
  if (mStat != eError)
    mStat = stat;
}


void WfFsdbFileReader::doVariable(WfVCD::Identifier& varId, WfVCD::SignalType sigType, UInt32 size, int idcode, bool isVerilog)
{
  STSymbolTableNode *node = createSymbolTableNode(varId, true);
  
  FsdbId* idObj = mKnownIds->find(idcode);
  if (idObj == NULL)
  {
    if (isVerilog)
      idObj = new FsdbId(idcode, size);
    else
      idObj = new FsdbVhdlId(idcode, size);
    
    mKnownIds->insert(idcode, idObj);
  }
  else
    ST_ASSERT(size == idObj->getSignalWidth(), node);
  
  setSignalOnNode(node, idObj, varId, sigType, size);
}


void WfFsdbFileReader::gatherWatchedIds(WaveIdSet* allWatched)
{
  for (FsdbIdHash::IdIter q = mWatchedSignals->loopIds(); ! q.atEnd(); ++q)
  {
    IWaveIdCode* id = q.getValue();
    allWatched->insert(id);
  }
}

void WfFsdbFileReader::insertWatchedId(IWaveIdCode* absId)
{
  FsdbId* waveId = absId->castFsdbId();
  int idcode = waveId->idCode();
  mWatchedSignals->insert(idcode, waveId);
}

void WfFsdbFileReader::unloadSignals(void)
{
}

void WfFsdbFileReader::unloadValues(void)
{
  closeFile();
}

void WfFsdbFileReader::closeFile(void)
{
  if (mFsdbObj)
  {
    delete mFsdbObj;
    mFsdbObj = NULL;
  }
}

UInt32 WfFsdbFileReader::getNumSignals() const
{
  return mKnownIds->size();
}

// Creation function
WfFsdbFileReader* WfFsdbFileReaderCreate(const char* fileName,
                                         AtomicCache* strCache,
                                         DynBitVectorFactory* dynFactory,
                                         UtUInt64Factory* timeFactory,
                                         WfAbsControl* control)
{
  return new WfFsdbFileReader(fileName, strCache, dynFactory, timeFactory, control);
}
