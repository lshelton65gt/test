// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// author: Mark Seneski

#include "waveform/WfVCD.h"

class WfVCDPerfectHash;
class WfVCDFileReader;
class UtFileBuf;
class Zistream;
class HdlId;

class WfVCDFileParse
{
public: CARBONMEM_OVERRIDES
  WfVCDFileParse(WfVCDFileReader* controller);
  ~WfVCDFileParse();

  //! Parse hierarchy. Fails if corruption is encountered
  bool parseSignals(UtString* errMsg);

  //! Parse values. Fails if corruption is encountered.
  bool parseValues(UtString* errMsg);

  //! Open the file.
  bool openFile(const char* fileName, UtString* errMsg);

  //! Current line number
  int getLineNo() const;

private:
  WfVCDFileReader* mController;
  WfVCDPerfectHash* mPerfectHash;
  Zistream* mFile;
  UtFileBuf* mInputBuf;
  UtFileBuf* mAlignBuf;
  UtString* mErrMsg; // pointer to user's error buffer
  UtString mValueBuf; // helper buffer
  UtString mGlyphBuf; // helper buffer
  UtString mGenericBuf; // helper buffer
  UInt32 mLastPosition;
  // Pipelined line number scheme
  /* The mParseLineNo is incremented as newlines are encountered by
     the token parser. At every token parse invocation mCurLineNo is
     updated to equal mParseLineNo. mCurLineNo then represents the
     line number of the token that is currently being processed.
   */
  int mCurLineNo; // line no for current token
  int mParseLineNo; // token parser line no.

  bool mDelims[256];
  bool mGlyphToks[256];
  // Needed if we rewind the buffer. This tells us if we need to look
  // at the mAlignBuf for the next token (only possible if we
  // rewound).
  bool mUsingAlignBuf;
  // Needed if we rewind the buffer. This tells us whether or not to
  // rewind the mAlignBuf or the mInputBuf
  bool mLastUsedAligned;
  bool mSignalsParsed;

  UInt32 getCurrentBufferPosition() const;
  void rewindReadBuffer();
  bool nextToken(char** beginPtr, char** endPtr);
  void skipLeadingWS(char** beginText, char* endText);
  WfVCD::ScopeType parseScopeType();
  bool parseVerilogToken(bool keepBrackets, HdlId* info);
  bool parseScope();
  void parseGlyph(char** beginText, char* endText);
  bool parseVar();
  bool parseFreeText();
  bool parseTimescale();
  bool parseVersion();
  bool parseDate();
  bool parseComment();
  WfVCD::SignalType parseVarType();
  bool getGlyph();
  void invalidateAlignBuf();
  bool scanForEndTok(const char* beginPtr, const char* endPtr);
  void putCurrentBufferPosition(const char* endPtr);
  void doErrMsg(const char* msg, const char* beginPtr, const char* endPtr);

  // Function only called by nextToken
  bool fillBuffer(bool pastLeadingSpaces, char** beginPtr, char** endPtr);

  WfVCDFileParse();
  WfVCDFileParse(const WfVCDFileParse&);
  WfVCDFileParse& operator=(const WfVCDFileParse&);
};
