// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "waveform/WfVCDBase.h"
#include "symtab/STSymbolTable.h"
#include "hdl/HdlId.h"
#include "waveform/WfVCDSignal.h"
#include "util/UtIOStream.h"
#include "util/AtomicCache.h"
#include "util/UtHashSet.h"
#include "util/DynBitVector.h"
#include "util/UtConv.h"
#include "util/UtUInt64Factory.h"
#include "util/StringAtom.h"

class WfVCDBase::DefaultWfControl : public WfAbsControl
{
public:
  virtual ~DefaultWfControl() {}

  virtual void initialValueWarning(const char* msg) const 
  {
    UtIO::cout() << msg << UtIO::endl;
  }

  virtual bool timeSlice(WfAbsSignalSet*,const UInt64*,const UInt64*)
  {
    return false;
  }
  
};

class WfVCDBase::Scope
{
public: 
  CARBONMEM_OVERRIDES

  Scope(StringAtom *name, WfVCD::ScopeType scope_type) : mName(name), mType(scope_type) { };
  const StringAtom * getName(void) const { return mName; };
  WfVCD::ScopeType getType(void) const { return mType; };
  
private:
  StringAtom *mName;
  WfVCD::ScopeType mType;
  
private:
  Scope(void);
  Scope(const Scope&);
  Scope& operator=(const Scope&);
};

static void sGenerateSigValueByDynBV(const DynBitVector& tmpVal, const DynBitVector& tmpDrv, const DynBitVector** val, DynBitVectorFactory* dynFactory)
{
  if (tmpDrv.any())
  {
    UInt32 numValWords = tmpVal.getUIntArraySize();
    
    // Add padding to array so that the value and the drive are
    // completely separate
    UInt32 bvBitSize = (numValWords * 2 * 32);
    DynBitVector fullVal(bvBitSize);
    
    // The array is setup with drv leftmost and val rightmost.
    fullVal |= tmpDrv;
    fullVal <<= (numValWords * 32);
    fullVal |= tmpVal;
    *val = dynFactory->alloc(fullVal);
  }
  else
    *val = dynFactory->alloc(tmpVal);
}

struct WfVCDBase::CreateValue
{
  CARBONMEM_OVERRIDES

  CreateValue(DynBitVector* value, DynBitVector* drv, 
              UInt32 width) : mValue(value), mDrive(drv)
  {
    mValue->resize(width);
    mDrive->resize(width);
    mValue->reset();
    mDrive->reset();
  }

  void operator()(Signal* sig)
  {
    IWaveIdCode* idCode = sig->getIdCode();
    WfVCDTimeValueList* valList = idCode->getValueList();
    
    WfVCDTimeValue* lastVal = valList->back();
    
    const DynBitVector* subFullVal;
    lastVal->getValue(&subFullVal);
    
    UInt32 width = sig->getWidth();
    (*mValue) <<= width;
    (*mDrive) <<= width;
    
    if (subFullVal->size() == width)
      (*mValue) |= *subFullVal;
    else
    {
      mValue->lpartsel(0, width) = subFullVal->partsel(0, width);
      UInt32 wordIndex = subFullVal->getUIntArraySize();
      wordIndex /= 2;
      UInt32 bitIndex = wordIndex * 32;
      mDrive->lpartsel(0, width) = subFullVal->partsel(bitIndex, width);
    }
  }
  
  DynBitVector* mValue;
  DynBitVector* mDrive;
};

#if CARBON_SORT_BLASTED_NETS
class WfVCDBase::BlastSort
{
  // sort() predicate function.  True if signal pointed to by first
  // parameter is greater than that pointed to by second parameter.
  class SigPtrGreater
  {
  public: 
    CARBONMEM_OVERRIDES
  bool operator()(const WfVCDBase::Signal* s1, const WfVCDBase::Signal* s2)
    {
      return s1->getMsb() > s2->getMsb();
    }
  };

public: 
  CARBONMEM_OVERRIDES

  void operator()(WfVCDBase::BitBlastSignal* signal)
  {
    std::sort(signal->mBlastList.begin(),
              signal->mBlastList.end(),
              SigPtrGreater());
  }
};
#endif

class WfVCDBase::WfLeafBOM 
{
public: 
  CARBONMEM_OVERRIDES

  WfLeafBOM() : mSignalIdent(NULL), mVCDSignal(NULL)
  {}

  ~WfLeafBOM() {
    delete mSignalIdent;
    delete mVCDSignal;
  }

  void print() const {
    UtIO::cout() << UtIO::hex << mSignalIdent << UtIO::endl;
    UtIO::cout() << UtIO::hex << mVCDSignal;
  }
  
  void setSignalIdent(ISignal* s) {
    mSignalIdent = s;
  }
  
  const ISignal* getSignalIdent() const {
    return mSignalIdent;
  }
  
  ISignal* getSignalIdent() {
    const WfLeafBOM* me = const_cast<const WfLeafBOM*>(this);
    return const_cast<ISignal*>(me->getSignalIdent());
  }
  
  void putVCDSignal(WfVCDSignal* vcdSignal) {
    mVCDSignal = vcdSignal;
  }

  WfVCDSignal* getVCDSignal() const {
    return mVCDSignal;
  }

private:
  ISignal* mSignalIdent;
  WfVCDSignal* mVCDSignal;
};

class WfVCDBase::WfBranchBOM 
{
public: 
  CARBONMEM_OVERRIDES

  WfBranchBOM() : mScopeIdent(NULL) 
  {}

  ~WfBranchBOM() {
    delete mScopeIdent;
  }

  void print() const {
    UtIO::cout() << UtIO::hex << mScopeIdent;
  }
  
  void setScopeIdent(Scope* s) {
    mScopeIdent = s;
  }
  
  WfVCDBase::Scope* getScopeIdent() {
    return mScopeIdent;
  }

private:
  Scope* mScopeIdent;
};

class WfVCDBase::SignalFactory
{
  typedef HashPointerValue<WfVCDBase::Signal*> CmpSig;
  typedef UtHashSet<WfVCDBase::Signal*, CmpSig> SignalSet;
  
public: 
  CARBONMEM_OVERRIDES

  SignalFactory()
  {}

  ~SignalFactory()
  {
    // The symboltable deletion deletes the signals created by this
    // class. It has to be that way because BitBlastSignals are also
    // ISignals and I really don't want to manage them separately.
  }
  
  WfVCDBase::Signal* createSignal(const WfVCD::Identifier& ident, const STSymbolTableNode* node, IWaveIdCode* glyphObj, WfVCD::SignalType signal_type, UInt32 size, bool* created)
  {
    *created = true;

    const HdlId& identInfo = ident.getHdlId();
    const HdlId* info = NULL;
    if ((size > 1) && (identInfo.getType() == HdlId::eScalar))
    {
      // identifier token does not have a range, inferred range is
      // from 0 to size - 1.
      HdlId tmp(size - 1, 0);
      info = mHdlIdFactory.getHdlId(tmp);
    }
    else
      info = mHdlIdFactory.getHdlId(identInfo);
    
    WfVCDBase::Signal* s = new WfVCDBase::Signal(node, glyphObj, signal_type, size, info);
    SignalSet::iterator p = mSignalCache.find(s);
    if (p == mSignalCache.end())
      mSignalCache.insert(s);
    else
    {
      delete s;
      s = *p;
      *created = false;
    }
    
    return s;
  }
  
private:
  SignalSet mSignalCache;
  HdlIdFactory mHdlIdFactory;
};

WfVCDBase::WfVCDBase(const char* filename, AtomicCache* strCache, DynBitVectorFactory* dynFactory, UtUInt64Factory* numFactory, WfAbsControl* control) : 
  WfAbsFileReader(filename), mStringCache(strCache), mUserControl(control), mUnlistedVal('x'), mFirstValueChanges(true), mValsRead(false),
  mRepeatedHierarchy(false)
{
  mSimulationTime = 0;
  mMaxSimTime = 0;
  mSymbolTable = NULL;
  
  mTimescaleValue = WfVCD::eWfVCDTimescaleValueINVALID;
  mTimescaleUnit = WfVCD::eWfVCDTimescaleUnitINVALID;;

  createSymbolTable();
  mSignalFactory = new SignalFactory;
  mDynFactory = dynFactory;
  mTimePtrs = numFactory;
  mDo4State = true;
  mDefaultControl = NULL;
  if (mUserControl == NULL)
  {
    mDefaultControl = new DefaultWfControl;
    mUserControl = mDefaultControl;
  }
};

WfVCDBase::~WfVCDBase()
{
  delete mSignalFactory;
  destroySymbolTable();
  if (mDefaultControl)
    delete mDefaultControl;
}

void WfVCDBase::putDo4State(bool do4State)
{
  mDo4State = do4State;
}

void WfVCDBase::createSymbolTable()
{
  INFO_ASSERT(!mSymbolTable, "Symboltable already created.");
  mSymbolTable = new STSymbolTable(this, mStringCache);
  mSymbolTable->setHdlHier(&mVerilogPath);
}

void WfVCDBase::destroySymbolTable()
{  
  delete mSymbolTable;
}


void WfVCDBase::getSimulatorVersion(UtString &v) const
{
  v = mVersion;
}


void WfVCDBase::getSimulationDate(UtString &d) const
{
  d = mDate;
}


WfVCD::TimescaleUnit WfVCDBase::getTimescaleUnit() const
{
  INFO_ASSERT(mTimescaleUnit != WfVCD::eWfVCDTimescaleUnitINVALID, "Timescale unit is invalid.");
  return mTimescaleUnit;
}


WfVCD::TimescaleValue WfVCDBase::getTimescaleValue() const
{
  INFO_ASSERT(mTimescaleValue != WfVCD::eWfVCDTimescaleValueINVALID, "Timescale value is invalid.");
  return mTimescaleValue;
}

STSymbolTableNode * WfVCDBase::lookupSignal(const UtString &signal) const
{
  HdlHierPath::Status status;
  HdlId info;
  return mSymbolTable->getNode(signal.c_str(), &status, &info);
}

void WfVCDBase::listAndWatchAll(UtVector<SigHier>* sigVec)
{
  const HdlHierPath* pather = mSymbolTable->getHdlHier();
  for (STSymbolTable::NodeLoopSorted p = mSymbolTable->getNodeLoopSorted(); ! p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf)
    {
      WfLeafBOM* bom = castLeafBOM(leaf->getBOMData());
      ISignal* s = bom->getSignalIdent();
      if (s)
      {
        UtString buf;
        pather->compPathHier(leaf, &buf);
        WfAbsSignal* absSig = watchSignal(buf);
        SigHier sh;
        sh.mLeaf = leaf;
        sh.mSignal = absSig;
        sigVec->push_back(sh);
      }
    }
  }
}

WfVCDTimeValueList* WfVCDBase::getValueList(ISignal* signal)
{
  WfVCDTimeValueList* values = NULL;
  
  Signal *sig = signal->castSignal();
  if (sig)
  {
    // insertWatchedSignal(sig);
    values = getValueListById(sig);
  }
  else
  {
    BitBlastSignal *bbSig = signal->castBitBlast();
    values = bbSig->getValList();
  }
  
  return values;
}

WfAbsSignal * WfVCDBase::watchSignal(const UtString &signal_name)
{
  WfVCDSignal* retSig = 0;
  
  STSymbolTableNode* node = lookupSignal(signal_name);
  bool inWaveForm = (node != NULL);
  WfVCDBase::ISignal* signal = NULL;
  if (inWaveForm)
  {
    WfLeafBOM* bom = getLeafBOM(node);
    retSig = bom->getVCDSignal();
    
    if (retSig == NULL)
    {
      signal = bom->getSignalIdent();

      WfVCDTimeValueList* values = getValueList(signal);
      retSig = new WfVCDSignal(signal, values);
      bom->putVCDSignal(retSig);

      Signal* sig = signal->castSignal();
      if (sig)
        insertWatchedSignal(sig, retSig);
      else 
      {
        BitBlastSignal* bbSig = signal->castBitBlast();
        ST_ASSERT(bbSig, signal->getName());
        if (! bbSig->isWatched())
        {
          for (BitBlastSignal::SigList::iterator p = bbSig->mBlastList.begin();
               p != bbSig->mBlastList.end(); ++p)
          {
            WfVCDBase::Signal* subSig = *p;
            insertWatchedSignal(subSig, retSig);
          }
          
          bbSig->putIsWatched(true);
        }
      }
    }    
  }
  return retSig;
}

void WfVCDBase::insertWatchedSignal(Signal* sig, WfVCDSignal* affectedSignal)
{
  IWaveIdCode* absId = sig->getIdCode();
  absId->addAffected(affectedSignal);
  insertWatchedId(absId);
}

bool WfVCDBase::signalExists(const UtString &signal_name) const
{
  const STSymbolTableNode* node = lookupSignal(signal_name);
  return (node != NULL);
}

bool WfVCDBase::maybeAddWatchSignal(const UtString &signal_name)
{
  return (watchSignal(signal_name) != NULL);
}


void WfVCDBase::doDate(const char *d)
{
  mDate.assign(d);
}

void WfVCDBase::doVersion(const char *v)
{
  mVersion.assign(v);
}

void WfVCDBase::doComment(const char * /* c */)
{
}

void WfVCDBase::doPushScope(const WfVCD::Identifier &scope_ident, WfVCD::ScopeType scope_type)
{
  STSymbolTableNode *node = createSymbolTableNode(scope_ident, false);
  // This will either attach the created scope onto the hierarchy
  // stack or delete the created scope if we have already seen this
  // hierarchy.
  setScopeOnNode(node, createScope(scope_ident, scope_type));
}


bool WfVCDBase::doPopScope()
{
  bool ret = ! mScopeStack.empty();
  if (ret)
    mScopeStack.pop_back();
  return ret;
}

void WfVCDBase::clearScopes()
{
  // start from the beginning. 
  mScopeStack.clear();
}

static void sPrintMsg(const char* msg, const char* postMsg)
{
  UtIO::cerr() << msg << ": " << postMsg << UtIO::endl;
}

WfVCD::TimescaleUnit WfVCDBase::evaluateTimescaleUnit(char* unit)
{
  WfVCD::TimescaleUnit timeUnit = WfVCD::eWfVCDTimescaleUnitINVALID;

  // Used to add to timeUnit in the case of a very small fractional unit.
  int startUnit = 0;
  const char* cpUnit = unit;
  
  // We could have a fractional unit (vhdl)
  if (*unit == '.')
  {
    ++unit;

    // make sure we have a valid time value
    INFO_ASSERT(mTimescaleValue != WfVCD::eWfVCDTimescaleValueINVALID, "Fractional timescale must have a numerator that is a power of 10.");
    
    int powInd = 0;
    bool found1 = false;
    while (isdigit(*unit))
    {
      // If we have found a 1, we better only see a 0
      if (found1)
      {
        FUNC_ASSERT(*unit == '0', sPrintMsg("Fractional unit value is not a factor of 10", cpUnit));
        // just keep going
      }
      else if (*unit == '1')
      {
        ++powInd;
        found1 = true;
      }
      else
      {
        FUNC_ASSERT(*unit == '0', sPrintMsg("Fractional unit value is not a factor of 10", cpUnit));
        ++powInd;
      }
      ++unit;
    }

    // powInd represents a negative power of 10. If > 3 than we can
    // move the given timeunit to the next lesser
    // thousandth. Fencepost, add 1 (we are definitely going to change
    // the calculated unit by at least 1).
    startUnit = powInd/3 + 1;  
    // The left over can be applied to the timevalue. We are adjusting
    // the timescale, so the powInd needs to be effectively divided
    // by the difference. (.1ps = 100fs, .01ps = 10fs, .001ps = 1fs)
    int remPow = powInd % 3;
    powInd = 3 - remPow;

    // This assert won't happen as long as the last two lines of code are there.
    FUNC_ASSERT((mTimescaleValue + powInd) < WfVCD::eWfVCDTimescaleValueINVALID, sPrintMsg("Fractional unit value cannot be less than .01", cpUnit));
    // Divide the timescale value by the remaining negative power of
    // 10. TimescaleValue is already a power of 10, so just add.
    mTimescaleValue = WfVCD::TimescaleValue((int)mTimescaleValue + powInd);
  }


  // next character is the time unit.
  // VCD has two characters for most, but vhdl fsdb only uses 1.
  // So, just checking 1.
  if (*unit == 'n')
    timeUnit = WfVCD::eWfVCDTimescaleUnitNS;
  else if (*unit == 'p')
    timeUnit = WfVCD::eWfVCDTimescaleUnitPS;
  else if (*unit == 'u')
    timeUnit = WfVCD::eWfVCDTimescaleUnitUS;
  else if (*unit == 'f')
    timeUnit = WfVCD::eWfVCDTimescaleUnitFS;
  else if (*unit == 'm')
    timeUnit = WfVCD::eWfVCDTimescaleUnitMS;
  else if (*unit == 's')
    timeUnit = WfVCD::eWfVCDTimescaleUnitS;
  
  // subtract the start unit from the timeUnit to deal with fractional units.
  FUNC_ASSERT((timeUnit - startUnit) >= 0, sPrintMsg("Fractional time unit is too small, less than a femtosecond", cpUnit));
  timeUnit = WfVCD::TimescaleUnit((int)timeUnit - startUnit);

  mTimescaleUnit = timeUnit;
  return timeUnit;
}

WfVCD::TimescaleValue WfVCDBase::evaluateTimescaleValue(SInt32 timeValue)
{
  WfVCD::TimescaleValue timeScale = WfVCD::eWfVCDTimescaleValueINVALID;
  switch(timeValue)
  {
  case 0:
    // special case for vhdl. Vhdl timescale can be fractions of
    // timeunits like 0.01p, for example. In this case, we will deal
    // with the fraction with the timeunit evaluation. So, for now,
    // set the timescale to be 1.
  case 1:
    timeScale = WfVCD::eWfVCDTimescaleValue1;
    break;
  case 10:
    timeScale = WfVCD::eWfVCDTimescaleValue10;
    break;
  case 100:
    timeScale = WfVCD::eWfVCDTimescaleValue100;
    break;
  default:
    break;
  }

  mTimescaleValue = timeScale;
  return timeScale;
}

UInt64 WfVCDBase::getMaxSimulationTime() const
{
  return mMaxSimTime;
}

STSymbolTableNode * WfVCDBase::createSymbolTableNode(const WfVCD::Identifier &ident, bool is_leaf)
{
  // Generate a proper identifier in case it is escaped.
  const HdlHierPath* pather = mSymbolTable->getHdlHier();

  HdlId info;
  UtString nameCp(ident.getName());
  UtString identName;
  pather->parseToken(&nameCp, &identName, &info, ! is_leaf);

  UtString finalIdentName;
  pather->compPathAppend(&finalIdentName, identName.c_str());
  
  StringAtom *name = mStringCache->intern(finalIdentName.c_str());
  STSymbolTableNode *parent_node = mScopeStack.size() ? mScopeStack.back() : 0;  
  STBranchNode* branch = NULL;
  if (parent_node)
    branch = parent_node->castBranch();
  
  UtString nameStr;
  pather->compPathHierAppend(branch, name, &nameStr);
  HdlHierPath::Status stat;
  STSymbolTableNode *node = mSymbolTable->getNode(nameStr.c_str(), &stat, &info);
  if (node == NULL)
    node = mSymbolTable->createNode(name, branch, is_leaf);
  
  return node;
}

WfVCDBase::Scope * WfVCDBase::createScope(const WfVCD::Identifier &ident, WfVCD::ScopeType scope_type)
{
  StringAtom *name = mStringCache->intern(ident.getName());
  WfVCDBase::Scope *s = new WfVCDBase::Scope(name, scope_type);
  INFO_ASSERT(s, name->str());
  return s;
}

void WfVCDBase::setScopeOnNode(STSymbolTableNode *node, WfVCDBase::Scope *s)
{
  ST_ASSERT(node->castBranch(), node);

  WfBranchBOM* bom = castBranchBOM(node->getBOMData());
  mScopeStack.push_back(node);
  // mRepeatedHierarchy allows me to track whether or not we have
  // visited this branch node previously. If we have not, and we come
  // across a node that already has an ISignal* in it then we are in a
  // strange state. So, this enables me to do a sanity check when I
  // create a Signal*
  // mRepeatedHierarchy == true ---> This branch node has been visited
  // previously
  if (bom->getScopeIdent() == NULL)
  {
    bom->setScopeIdent(s);
    mRepeatedHierarchy = false;
  }
  else
  {
    delete s;
    mRepeatedHierarchy = true;
  }
}

const WfVCDBase::WfLeafBOM* WfVCDBase::getLeafBOM(const STSymbolTableNode *node) const
{
  const STAliasedLeafNode* leaf = node->castLeaf();
  const WfLeafBOM* bom = castLeafBOM(leaf->getBOMData());
  return bom;
}

WfVCDBase::ISignal * WfVCDBase::getSignalFromNode(const STSymbolTableNode *node) const
{
  const STAliasedLeafNode* leaf = node->castLeaf();
  const WfLeafBOM* bom = castLeafBOM(leaf->getBOMData());
  ISignal* s = const_cast<ISignal*>(bom->getSignalIdent());
  ST_ASSERT(s, node);
  return(s);
}


void WfVCDBase::setSignalOnNode(STSymbolTableNode *node, IWaveIdCode* glyphObj, const WfVCD::Identifier& ident, WfVCD::SignalType signal_type, UInt32 size)
{ 
  // Create this node or return if we have visited it 
  bool sigCreated;
  WfVCDBase::Signal *s = mSignalFactory->createSignal(ident, node, glyphObj, signal_type, size, &sigCreated);
  
  if (! sigCreated)
  {
    // If this was a repeated leafnode then this might be a memory. We
    // don't support memories yet. But when we do...we'll have to deal
    // with that.
    // We have already seen this node
    return;
  }
  
  
  ST_ASSERT(node->castLeaf(), node);
  WfLeafBOM* bom = castLeafBOM(node->getBOMData());
  if (bom->getSignalIdent() == NULL)
    bom->setSignalIdent(s);
  else
  {
    // we have a bit blasted signal
    Signal* signal = bom->getSignalIdent()->castSignal();
    BitBlastSignal* bbSig = bom->getSignalIdent()->castBitBlast();
    ST_ASSERT(signal || bbSig, node);
    if (! bbSig)
    {
      // grab the current signal, replace it with a bbSig and add the
      // previous to the bbSig as well as the new signal
      bbSig = new BitBlastSignal;
      bbSig->addSignal(signal);
      bom->setSignalIdent(bbSig);
      mBlastedSigs.push_back(bbSig);
    }
    bbSig->addSignal(s);
  }
}

STFieldBOM::Data WfVCDBase::allocLeafData()
{
  return static_cast<STFieldBOM::Data>(new WfLeafBOM);
}

STFieldBOM::Data WfVCDBase::allocBranchData()
{
  return static_cast<STFieldBOM::Data>(new WfBranchBOM);
}

void WfVCDBase::freeBranchData(const STBranchNode*, Data* bomdata)
{
  WfVCDBase::WfBranchBOM* bom = WfVCDBase::castBranchBOM(*bomdata);
  *bomdata = NULL;
  delete bom;
}

void WfVCDBase::freeLeafData(const STAliasedLeafNode*, Data* bomdata)
{
  WfVCDBase::WfLeafBOM* bom = WfVCDBase::castLeafBOM(*bomdata);
  *bomdata = NULL;
  delete bom;
}

void WfVCDBase::preFieldWrite(ZostreamDB&)
{}

STFieldBOM::ReadStatus 
WfVCDBase::preFieldRead(ZistreamDB&)
{
  return eReadOK;
}

void WfVCDBase::writeLeafData(const STAliasedLeafNode* leaf, ZostreamDB&) const
{
  ST_ASSERT(0, leaf);
}


void WfVCDBase::writeBranchData(const STBranchNode* branch, ZostreamDB&,
                                AtomicCache*) const
{
  ST_ASSERT(0, branch);
}


STFieldBOM::ReadStatus WfVCDBase::readLeafData(STAliasedLeafNode* leaf, ZistreamDB&, MsgContext*)
{
  ST_ASSERT(0, leaf);
  return eReadIncompatible;
}


STFieldBOM::ReadStatus WfVCDBase::readBranchData(STBranchNode* branch, ZistreamDB&, MsgContext*)
{
  ST_ASSERT(0, branch);
  return eReadIncompatible;
}

void WfVCDBase::printBranch(const STBranchNode* branch) const
{
  const WfVCDBase::WfBranchBOM* bom = WfVCDBase::castBranchBOM(branch->getBOMData());
  bom->print();
}

void WfVCDBase::printLeaf(const STAliasedLeafNode* leaf) const
{
  const WfVCDBase::WfLeafBOM* bom = WfVCDBase::castLeafBOM(leaf->getBOMData());
  bom->print();
}

WfVCDBase::WfLeafBOM * WfVCDBase::castLeafBOM(const Data bomdata)
{
  return static_cast<WfLeafBOM*>(bomdata);
}

WfVCDBase::WfBranchBOM * WfVCDBase::castBranchBOM(const Data bomdata)
{
  return static_cast<WfBranchBOM*>(bomdata);
}

void WfVCDBase::fillHdlId(HdlId* info, const STAliasedLeafNode* net) 
  const
{
  const WfLeafBOM* bom = castLeafBOM(net->getBOMData());
  const ISignal* ident = bom->getSignalIdent();
  if (ident)
  {
    HdlId::Type type = ident->getDimensionType();
    info->setType(type);
    switch (type)
    {
    case HdlId::eScalar:
      break;
    case HdlId::eVectBit:
      info->setVectIndex(ident->getLsb());
      break;
    case HdlId::eVectBitRange:
      info->setVectMSB(ident->getMsb());
      info->setVectLSB(ident->getLsb());
      break;
    case HdlId::eArrayIndex:
      ST_ASSERT(ident->getDimensionType() != HdlId::eArrayIndex, net);
      break;
    case HdlId::eInvalid:
      info->setType(HdlId::eInvalid);
      ST_ASSERT(ident->getDimensionType() != HdlId::eInvalid, net);
      break;
    }
  }
  else
    info->setType(HdlId::eScalar);
}

void WfVCDBase::putUnlistedValueSetting(char value)
{
  if (! mValsRead)
    mUnlistedVal = value;
  switch(mUnlistedVal)
  {
  case '0': break;
  case '1': break;
  case 'x': break;
  case 'z': break;
  default:
    mUnlistedVal = 'x';
    break;
  }
}

WfAbsSignalVCIter* WfVCDBase::createSignalVCIter(WfAbsSignal* sig, UtString*)
{
  WfVCDSignal* vcdSig = sig->castVCDSignal();
  FUNC_ASSERT(vcdSig, sig->print());
  return vcdSig->createSignalVCIter();
}

void WfVCDBase::generateSigValue(const char* value, 
                                 UInt32 sigWidth,
                                 const DynBitVector** val)
{
  DynBitVector tmpVal, tmpDrv;
  tmpVal.resize(sigWidth);
  tmpDrv.resize(sigWidth);

  int truncStat;
  bool result = 
    UtConv::BinStrToValDrvFit(value,
                              &tmpVal,
                              &tmpDrv,
                              sigWidth,
                              &truncStat,
                              mDo4State);

  INFO_ASSERT(result, value);
  
  sGenerateSigValueByDynBV(tmpVal, tmpDrv, val, mDynFactory);

}

bool WfVCDBase::advanceSimulationTime(UInt64 newTime)
{
  INFO_ASSERT(newTime >= mSimulationTime, "Time went backwards.");
  const UInt64* nextTimePtr = mTimePtrs->getNumPtr(newTime);
  bool ret = reportValueChanges(nextTimePtr);
  mSimulationTime = newTime;
  return ret;
}

void WfVCDBase::endLoadValues()
{
  reportValueChanges(NULL);
}

void WfVCDBase::manualInitializeWaveId(IWaveIdCode* idCode, const UInt64* timePtr, WfAbsSignalSet* affectedSigs)
{
  WfVCDTimeValueList* valList = idCode->getValueList();
  INFO_ASSERT(valList->empty(), "Attempting to initialize wave id that already has a value.");
  
  idCode->insertAffected(affectedSigs);
    
  UInt32 width = idCode->getSignalWidth();
  UtString buf;
  buf.append(width, mUnlistedVal);
  DynBitVector val (width), drv (width);

  switch (mUnlistedVal)
  {
  case 'x':
    drv.set();
    break;
  case 'z':
    val.set();
    drv.set();
    break;
  case '1':
    val.set();
    break;
  case '0':
    break;
  default:
    INFO_ASSERT(0, "Invalid initialization character");
    break;
  }
  
  WfVCDTimeValue* initVal = new WfVCDTimeValue(timePtr);
  const DynBitVector* fVal;
  sGenerateSigValueByDynBV(val, drv, &fVal, mDynFactory);
  initVal->putValue(fVal);
  valList->push_back(initVal);
}

bool WfVCDBase::reportValueChanges(const UInt64* nextTime)
{
  const UInt64* timePtr = mTimePtrs->getNumPtr(mSimulationTime);

  WfAbsSignalSet allChangedSigs;

  if (mFirstValueChanges && ! mChangedWaveIds.empty())
  {
    mFirstValueChanges = false;
    
    // initialize any wave ids that weren't reported at the first time
    // slice.
    WaveIdSet allWatched;
    gatherWatchedIds(&allWatched);
    
    for (WaveIdVec::iterator p = mChangedWaveIds.begin(), e = mChangedWaveIds.end();
         p != e; ++p)
    {
      IWaveIdCode* id = *p;
      allWatched.erase(id);
    }
  
    if (! allWatched.empty())
    {
      WfAbsSignalSet affectedSigs;
      for (WaveIdSet::UnsortedLoop q = allWatched.loopUnsorted(); ! q.atEnd(); ++q)
      {
        IWaveIdCode* id = *q;
        manualInitializeWaveId(id, timePtr, &affectedSigs);
        id->insertAffected(&allChangedSigs);
      }

      // distill the affected sigs to the distinct ISignals
      // (bit-blasted signals have multiple ISignals)
      typedef UtHashSet<Signal*> ISigSet;
      ISigSet isigs;
      for (WfAbsSignalSet::UnsortedLoop q = affectedSigs.loopUnsorted();
           ! q.atEnd(); ++q)
      {
        WfVCDSignal* vcdSig = (*q)->castVCDSignal();
        ISignal* internalSig = vcdSig->getInternalSignal();
        Signal* simpleSig;
        BitBlastSignal* bbSig;
        if ((simpleSig = internalSig->castSignal()))
          isigs.insert(simpleSig);
        else
        {
          bbSig = internalSig->castBitBlast();
          ST_ASSERT(bbSig, internalSig->getName());
          for (BitBlastSignal::SigList::iterator p = bbSig->mBlastList.begin(),
                 e = bbSig->mBlastList.end(); p != e; ++p)
          {
            Signal* subSig = *p;
            IWaveIdCode* subId = subSig->getIdCode();
            // The entire bbsig is put into the affected list. We need
            // to find the sub parts of the bit blast that are
            // actually affected. compare to what is still left in allWatched
            if (allWatched.find(subId) != allWatched.end())
              isigs.insert(subSig);
          }
        }
      }

      // sort the isig set for stable reporting
      for (ISigSet::SortedLoop p = isigs.loopSorted(); ! p.atEnd(); ++p)
      {
        Signal* sig = *p;
        
        UtString nameBuf;
        HdlVerilogPath pather;
        const HdlId* info = sig->getHdlId();
        pather.compPathHier(sig->getName(), &nameBuf, info);

        {
          UtString msg;
          msg << "Waveform File Warning: No value information for '" << nameBuf << "' at the first time slice. Setting to all " 
              << mUnlistedVal << "'s at time " << mSimulationTime << ".";
          
          mUserControl->initialValueWarning(msg.c_str());
        }
      }
    }
  }
  
  for (WaveIdVec::iterator p = mChangedWaveIds.begin(), e = mChangedWaveIds.end();
       p != e; ++p)
  {
    IWaveIdCode* id = *p;
    id->insertAffected(&allChangedSigs);
  }

  if (! mBlastedSigs.empty())
  {
    
    for (WfAbsSignalSet::UnsortedLoop q = allChangedSigs.loopUnsorted(); 
         ! q.atEnd(); ++q)
    {
      WfVCDSignal* sig = (*q)->castVCDSignal();
      ISignal* iSig = sig->getInternalSignal();
      iSig->calcValue(timePtr, mDynFactory);
    }
  }

  bool stop = false;
  if (! allChangedSigs.empty())
    stop = mUserControl->timeSlice(&allChangedSigs, timePtr, nextTime);
  
  mChangedWaveIds.resize(0);

  return stop;
}

WfVCDTimeValue* WfVCDBase::getCurrentTimeValuePair(WfVCDTimeValueList* valList)
{
  WfVCDTimeValue* tvc = NULL;
  if (! valList->empty())
  {
    WfVCDTimeValue* tmpvc = valList->back();
    if (tmpvc->getTime() == mSimulationTime)
      tvc = tmpvc;
  }

  if (tvc == NULL)
  {
    const UInt64* timePtr = mTimePtrs->getNumPtr(mSimulationTime);
    tvc = new WfVCDTimeValue(timePtr);
    valList->push_back(tvc);
  }
  return tvc;
}

void WfVCDBase::addValueChangeInt(IWaveIdCode* id, UInt32 value)
{
  WfVCDTimeValueList* valList = id->getValueList();
  WfVCDTimeValue* tvc = getCurrentTimeValuePair(valList);
  DynBitVector tmpVal (32, UInt64 (value));

  const DynBitVector* val = mDynFactory->alloc(tmpVal);
  tvc->putValue(val);
  mChangedWaveIds.push_back(id);
}

void WfVCDBase::addValueChangeReal(IWaveIdCode* id, CarbonReal value)
{
  // doubles and UInt64s are the same size. So we can factory reals as uint64s
  WfVCDTimeValueList* valList = id->getValueList();
  WfVCDTimeValue* tvc = getCurrentTimeValuePair(valList);
  
  const CarbonReal* tmpVal = mTimePtrs->getRealPtr(value);
  tvc->putValueReal(tmpVal);
  mChangedWaveIds.push_back(id);
}

void WfVCDBase::addValueChange(IWaveIdCode* id, const char* value)
{
  UInt32 sigWidth = id->getSignalWidth();
  WfVCDTimeValueList* valList = id->getValueList();

  WfVCDTimeValue* tvc = getCurrentTimeValuePair(valList);

  const DynBitVector* val;
  generateSigValue(value, sigWidth, &val);
  
  tvc->putValue(val);
  mChangedWaveIds.push_back(id);
}

WfAbsSignal * WfVCDBase::findSignal(const UtString &signal_name, UtString* msg) const
{
  WfVCDSignal* vcdSignal = NULL;
  
  STSymbolTableNode* node = lookupSignal(signal_name);
  bool inWaveForm = (node != NULL);
  if (inWaveForm)
  {
    const WfLeafBOM* bom = getLeafBOM(node);
    vcdSignal = bom->getVCDSignal();
  }
  
  if ((vcdSignal == NULL) && msg)
    *msg << signal_name << " was not found in the watched signals list.";
  
  return vcdSignal;
}

WfVCDTimeValueList* WfVCDBase::getValueListById(Signal* sig) const
{
  IWaveIdCode* glyphObj = sig->getIdCode();
  return glyphObj->getValueList();
}

void WfVCDBase::endLoadSignals()
{
#if CARBON_SORT_BLASTED_NETS
  std::for_each(mBlastedSigs.begin(), mBlastedSigs.end(), BlastSort());
#endif
}

static void sFlushValueList(WfVCDTimeValueList* valList)
{
  if (! valList->empty())
  {
    WfVCDTimeValue* timeValue = valList->back();
    UInt32 size = valList->size();
    for (UInt32 i = 0; i < size - 1; ++i)
      delete (*valList)[i];
    valList->clear();
    valList->push_back(timeValue);
  }
}

void WfVCDBase::flushSignal(Signal* sig)
{
  IWaveIdCode* idCode = sig->getIdCode();
  WfVCDTimeValueList* valList = idCode->getValueList();
  sFlushValueList(valList);
}

void WfVCDBase::flushValues()
{
  for (STSymbolTable::NodeLoopSorted p = mSymbolTable->getNodeLoopSorted(); ! p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf)
    {
      WfLeafBOM* bom = castLeafBOM(leaf->getBOMData());
      ISignal* isig = bom->getSignalIdent();
      if (isig)
      {
        Signal* sig = isig->castSignal();
        if (sig)
          flushSignal(sig);
        else
        {
          BitBlastSignal* bbsig = isig->castBitBlast();
          ST_ASSERT(bbsig, isig->getName());
          WfVCDTimeValueList* valList = bbsig->getValList();
          sFlushValueList(valList);
          for (BitBlastSignal::SigList::iterator p = bbsig->mBlastList.begin(),
                 e = bbsig->mBlastList.end(); p != e; ++p)
          {
            Signal* sigPart = *p;
            flushSignal(sigPart);
          }
        }
      }
    }
  }
}

WfVCDBase::IWaveIdCode::~IWaveIdCode() 
{
  for (WfVCDTimeValueList::iterator p = mValueList.begin();
       p != mValueList.end();
       ++p)
    delete *p;
}

const WfVCDBase::WaveGlyph* 
WfVCDBase::IWaveIdCode::castGlyph() const 
{ 
  return NULL; 
}
    
WfVCDBase::WaveGlyph* 
WfVCDBase::IWaveIdCode::castGlyph() 
{
  const IWaveIdCode* me = const_cast<const IWaveIdCode*>(this);
  return const_cast<WaveGlyph*>(me->castGlyph());
}

const WfVCDBase::FsdbId* WfVCDBase::IWaveIdCode::castFsdbId() const
{
  return NULL;
}

WfVCDBase::FsdbId* WfVCDBase::IWaveIdCode::castFsdbId()
{
  const IWaveIdCode* me = const_cast<const IWaveIdCode*>(this);
  return const_cast<FsdbId*>(me->castFsdbId());
}

const WfVCDTimeValueList* 
WfVCDBase::IWaveIdCode::getValueList() const 
{
  return &mValueList;
}
    
WfVCDTimeValueList* 
WfVCDBase::IWaveIdCode::getValueList() 
{
  const IWaveIdCode* me = const_cast<const IWaveIdCode*>(this);
  return const_cast<WfVCDTimeValueList*>(me->getValueList());
}

void WfVCDBase::IWaveIdCode::clearAffected()
{
  mAffectedSigs.clear();
}

void WfVCDBase::IWaveIdCode::addAffected(WfVCDSignal* watchedSig)
{
  mAffectedSigs.push_back(watchedSig);
}

void WfVCDBase::IWaveIdCode::insertAffected(WfAbsSignalSet* sigSet) const
{
  for (SigVec::const_iterator p = mAffectedSigs.begin(), e = mAffectedSigs.end();
       p != e; ++p)
    sigSet->insert(*p);
}

WfVCDBase::WaveGlyph::WaveGlyph(const char* glyph, UInt32 sigWidth) : 
  IWaveIdCode(sigWidth), mGlyph(glyph)
{}

WfVCDBase::WaveGlyph::~WaveGlyph()
{}
    
const WfVCDBase::WaveGlyph* 
WfVCDBase::WaveGlyph::castGlyph() const 
{ 
  return this; 
}

const char* WfVCDBase::WaveGlyph::symbol() const 
{
  return mGlyph.c_str();
}

WfVCDBase::FsdbId::FsdbId(int idCode, UInt32 sigWidth) : 
  IWaveIdCode(sigWidth), mIdCode(idCode)
{}

WfVCDBase::FsdbId::~FsdbId()
{}

const WfVCDBase::FsdbId* 
WfVCDBase::FsdbId::castFsdbId() const
{
  return this;
}

bool WfVCDBase::FsdbId::isVerilog() const
{
  return true;
}
    
int WfVCDBase::FsdbId::idCode() const
{
  return mIdCode;
}

WfVCDBase::FsdbVhdlId::FsdbVhdlId(int idCode, UInt32 sigWidth) :
  FsdbId(idCode, sigWidth)
{}

WfVCDBase::FsdbVhdlId::~FsdbVhdlId()
{}

bool WfVCDBase::FsdbVhdlId::isVerilog() const
{
  return false;
}

WfVCDBase::ISignal::~ISignal()
{}

const WfVCDBase::Signal* 
WfVCDBase::ISignal::castSignal() const 
{ 
  return NULL; 
}

const WfVCDBase::BitBlastSignal* 
WfVCDBase::ISignal::castBitBlast() const 
{ 
  return NULL; 
}

WfVCDBase::Signal* 
WfVCDBase::ISignal::castSignal() { 
  const ISignal* me = const_cast<const ISignal*>(this);
  return const_cast<Signal*>(me->castSignal());
}

WfVCDBase::BitBlastSignal* 
WfVCDBase::ISignal::castBitBlast() 
{
  const ISignal* me = const_cast<const ISignal*>(this);
  return const_cast<BitBlastSignal*>(me->castBitBlast());
}

WfVCDBase::Signal::Signal(const STSymbolTableNode *name, IWaveIdCode* idcode, WfVCD::SignalType signal_type, UInt32 size, const HdlId* info)
  : mHdlInfo(info), mWidth(size), mName(name), mIdCode(idcode), mSignalType(signal_type)
{ 
}

WfVCDBase::Signal::~Signal() 
{
}

UInt32 WfVCDBase::Signal::getWidth(void) const 
{ 
  return mWidth; 
}

SInt32 WfVCDBase::Signal::getLsb(void) const 
{ 
  SInt32 ret = -1;
  HdlId::Type type = mHdlInfo->getType();
  switch(type)
  {
  case HdlId::eScalar:
    break;
  case HdlId::eVectBit:
    ret = mHdlInfo->getVectIndex();
    break;
  case HdlId::eVectBitRange:
    ret = mHdlInfo->getVectLSB();
    break;
  case HdlId::eArrayIndex:
    INFO_ASSERT(type != HdlId::eArrayIndex, "HdlInfo is invalid.");
  case HdlId::eInvalid:
    INFO_ASSERT(type != HdlId::eInvalid, "HdlInfo is invalid.");
    break;
  }
  return ret; 
}

SInt32 WfVCDBase::Signal::getMsb(void) const 
{ 
  SInt32 ret = -1;
  HdlId::Type type = mHdlInfo->getType();
  switch(type)
  {
  case HdlId::eScalar:
    break;
  case HdlId::eVectBit:
    ret = mHdlInfo->getVectIndex();
    break;
  case HdlId::eVectBitRange:
    ret = mHdlInfo->getVectMSB();
    break;
  case HdlId::eArrayIndex:
    INFO_ASSERT(type != HdlId::eArrayIndex, "HdlInfo is invalid.");
  case HdlId::eInvalid:
    INFO_ASSERT(type != HdlId::eInvalid, "HdlInfo is invalid.");
    break;
  }
  return ret; 
}

const STSymbolTableNode* WfVCDBase::Signal::getName(void) const 
{
  return mName; 
}

const WfVCDBase::IWaveIdCode* 
WfVCDBase::Signal::getIdCode() const 
{ 
  return mIdCode; 
}

WfVCDBase::IWaveIdCode* 
WfVCDBase::Signal::getIdCode() 
{ 
  const Signal* me = const_cast<const Signal*>(this);
  return const_cast<IWaveIdCode*>(me->getIdCode());
}

WfVCD::SignalType 
WfVCDBase::Signal::getSignalType(void) const 
{
  return mSignalType; 
}

HdlId::Type
WfVCDBase::Signal::getDimensionType(void) const 
{ 
  return mHdlInfo->getType();
}

void WfVCDBase::Signal::calcValue(const UInt64*, DynBitVectorFactory*)
{}

const WfVCDBase::Signal* 
WfVCDBase::Signal::castSignal() const 
{ 
  return this; 
}

size_t WfVCDBase::Signal::hash() const
{
  size_t h = reinterpret_cast<size_t>(mName);
  h = 3*h + reinterpret_cast<size_t>(mIdCode);
  h = 3*h + reinterpret_cast<size_t>(mHdlInfo);
  h = 3*h + mSignalType;
  h = 3*h + mWidth;
  return h;
}

bool WfVCDBase::Signal::operator==(const Signal& other) const
{
  bool isEq = mName == other.mName;
  isEq = isEq && (mIdCode == other.mIdCode);
  isEq = isEq && (mSignalType == other.mSignalType);
  isEq = isEq && (mHdlInfo == other.mHdlInfo);
  isEq = isEq && (mWidth == other.mWidth);
  return isEq;
}

bool WfVCDBase::Signal::operator<(const Signal& other) const
{
  // if other and this are the same pointer return false;
  if (this == &other)
    return false;
  
  // first compare hierarchies
  int diff = HierName::compare(mName, other.mName);
  if (diff == 0)
  {
    // the names are the same, compare HdlInfo
    diff = HdlId::compare(mHdlInfo, other.mHdlInfo);

    if (diff == 0)
    {
      // Try width
      diff = mWidth - other.mWidth;

      if (diff == 0)
      {
        // hier and hdlids are the same. These are the same Signal in
        // essence. Try signaltypes
        diff = mSignalType - other.mSignalType;
        
        if (diff == 0)
        {
          // last ditch effort. pointer compare the waveid
          diff = mIdCode - other.mIdCode;
        }
      }
    }
  }
  return (diff < 0);
}

WfVCDBase::BitBlastSignal::BitBlastSignal() 
  : mWidth(0), mIsWatched(false)
{}
    
WfVCDBase::BitBlastSignal::~BitBlastSignal()
{
  for (WfVCDTimeValueList::iterator q = mValueList.begin();
       q != mValueList.end(); ++q)
    delete *q;

  for (SigList::iterator p = mBlastList.begin(); 
       p != mBlastList.end(); ++p)
    delete *p;
}

void WfVCDBase::BitBlastSignal::addSignal(Signal* sigBit) 
{
  // either a bit or a range, not a scalar
  ST_ASSERT(sigBit->getDimensionType() != HdlId::eScalar, sigBit->getName());
  mWidth += sigBit->getWidth();
  mBlastList.push_back(sigBit);
}
    
UInt32 WfVCDBase::BitBlastSignal::getWidth(void) const 
{ 
  return mWidth;
}

SInt32 WfVCDBase::BitBlastSignal::getLsb(void) const 
{ 
  return mBlastList.back()->getLsb(); 
}

SInt32 WfVCDBase::BitBlastSignal::getMsb(void) const 
{ 
  return mBlastList[0]->getMsb(); 
}

const STSymbolTableNode* WfVCDBase::BitBlastSignal::getName(void) const 
{ 
  return mBlastList.back()->getName(); 
}

WfVCD::SignalType 
WfVCDBase::BitBlastSignal::getSignalType(void) const 
{ 
  return mBlastList.back()->getSignalType(); 
}

HdlId::Type
WfVCDBase::BitBlastSignal::getDimensionType(void) const 
{ 
  return HdlId::eVectBitRange;
}

void WfVCDBase::BitBlastSignal::calcValue(const UInt64* curTime, DynBitVectorFactory* dynFactory)
{
  DynBitVector value, drive;
  std::for_each(mBlastList.begin(), mBlastList.end(), CreateValue(&value, &drive, mWidth));
  
  WfVCDTimeValue* tvc = new WfVCDTimeValue(curTime);
  mValueList.push_back(tvc);
  
  const DynBitVector* fVal;
  sGenerateSigValueByDynBV(value, drive, &fVal, dynFactory);
  tvc->putValue(fVal);
}

const WfVCDBase::BitBlastSignal* 
WfVCDBase::BitBlastSignal::castBitBlast() const 
{ 
  return this; 
}
    
bool WfVCDBase::BitBlastSignal::isWatched() const 
{ 
  return mIsWatched; 
}

void WfVCDBase::BitBlastSignal::putIsWatched(bool isWatched) 
{ 
  mIsWatched = isWatched; 
}

const WfVCDTimeValueList* WfVCDBase::BitBlastSignal::getValList() const 
{
  return &mValueList;
}

WfVCDTimeValueList* WfVCDBase::BitBlastSignal::getValList() 
{
  const BitBlastSignal* me = const_cast<const BitBlastSignal*>(this);
  return const_cast<WfVCDTimeValueList*>(me->getValList());
}
