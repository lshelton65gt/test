#!/usr/bin/perl -w

use File::Spec::Functions;
use Getopt::Long;

sub error {
    my $errMsg = "Error: " . $_[0] . "\n";
    die $errMsg;
    exit(1);
}

sub grabFileLines {
    my ($file) = $_[0];
    if (-e $file) {
        &error ("Unable to open $file for reading: $!") unless open(IN, "<$file");
    } else {
        return;
    }
    
    my $x;
    while($x=<IN>) {
        ${$_[1]}[($#{$_[1]} + 1)] = $x;
    }
    close(IN);
}

sub writeArrayToFile {
    my($fh) = shift;
    
    my($n);
    for ($n = 0; $n <= $#{$_[0]}; $n++) {
        print $fh ${$_[0]}[$n] . "\n";
    }
}

sub main {
    my ($module);
    GetOptions("m=s", \$module);
    
    die "Must specify module with -m" unless (length($module) != 0);

    my @listOfTexFiles = ("refman.tex", "modules.tex");
    my @listOfHtmlFiles = ("modules.html", "tree.html");

    my $i;
    my @fileLines = ();
    my $curFile;
    my $j;
    my $tmpFile;

    for ($i = 0; $i <= $#listOfTexFiles; $i++) {
        $curFile = catfile(curdir(), $module . "_latex", $listOfTexFiles[$i]);
        @fileLines = ();
        grabFileLines($curFile, \@fileLines);
        chomp(@fileLines);
        
        for ($j = 0; $j <= $#fileLines; $j++) {
            $fileLines[$j] =~ s/API Main Page/API Introduction/o;
            $fileLines[$j] =~ s/API Module/API Function/o;
            $fileLines[$j] =~ s/list of all modules/list of all functions/o;
        }

        $tmpFile = $curFile . "tmp";
        &error ("Unable to open $tmpFile for writing: $!") unless open(OUT, ">$tmpFile");
        &writeArrayToFile(\*OUT, \@fileLines);
        die "Unable to rename $tmpFile to $curFile" unless rename($tmpFile, $curFile);
    }

    for ($i = 0; $i <= $#listOfHtmlFiles; $i++) {
        $curFile = catfile(curdir(), $module . "_html", $listOfHtmlFiles[$i]);
        @fileLines = ();
        &grabFileLines($curFile, \@fileLines);
        chomp(@fileLines);
        
        for ($j = 0; $j <= $#fileLines; $j++) {
            $fileLines[$j] =~ s/API Modules/API Functions/o;
            $fileLines[$j] =~ s/list of all modules/list of all functions/o;
            $fileLines[$j] =~ s/\>Main Page\</\>Introduction\</o;
            $fileLines[$j] =~ s/\>Modules\</\>Functions\</o;
            $fileLines[$j] =~ s/\>Globals\</\>Index\</o;
        }
        
        $tmpFile = $curFile . "tmp";
        &error ("Unable to open $tmpFile for writing: $!") unless open(OUT, ">$tmpFile");
        &writeArrayToFile(\*OUT, \@fileLines);
        die "Unable to rename $tmpFile to $curFile" unless rename($tmpFile, $curFile);
    }
}

&main();
