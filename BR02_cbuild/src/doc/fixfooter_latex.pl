#!/usr/bin/perl -w

use File::Spec::Functions;
use Getopt::Long;

sub error {
    my $errMsg = "Error: " . $_[0] . "\n";
    die $errMsg;
    exit(1);
}

sub grabFileLines {
    my ($file) = $_[0];
    &error ("Unable to open $file for reading: $!") unless open(IN, "<$file");

    my $x;
    while($x=<IN>) {
        ${$_[1]}[($#{$_[1]} + 1)] = $x;
    }
    close(IN);
}

sub writeArrayToFile {
    my($fh) = shift;
    
    my($n);
    for ($n = 0; $n <= $#{$_[0]}; $n++) {
        print $fh ${$_[0]}[$n] . "\n";
    }
}

sub main {
    my ($module);
    GetOptions("m=s", \$module);
    
    if (length($module) == 0) {
        $module = "vhm_api";
    }

    my $doxFile = catfile(curdir(), $module . "_latex", "doxygen.sty");
    my $msg = "Carbon Design Systems, Inc. - Confidential  (c) Copyright 2003-2010 ";
    my @doxLInes = ();
    grabFileLines($doxFile, \@doxLines);
    chomp(@doxLines);
    my $foundLFoot = 0;
    my $foundRFoot = 0;
    my $tmp;
    my @splinter;
    for (my $i = 0; ($i <= $#doxLines) && (($foundLFoot == 0) || ($foundRFoot == 0)); $i++) {
        if ($doxLines[$i] =~ /\\rfoot\[.*/) {
            $foundRFoot = 1;
            @splinter = split(/scriptsize|\}\]/, $doxLines[$i]);
            die "split incorrect" unless ($#splinter == 2);
            $splinter[1] = $msg;
            $doxLines[$i] = $splinter[0] . "scriptsize " . $splinter[1] . "}]" . $splinter[2];
        }
        if ($doxLines[$i] =~ /\\lfoot\[.*/) {
            $foundLFoot = 1;
            @splinter = split(/scriptsize|\}\}/, $doxLines[$i]);
            die "split incorrect" unless ($#splinter == 1);
            $splinter[1] = $msg;
            $doxLines[$i] = $splinter[0] . "scriptsize " . $splinter[1] . "}}";
        }
    }

    die "Did not find all required fields." unless (($foundLFoot == 1) && ($foundRFoot == 1));
    
    my $doxFileTmp = $doxFile . "tmp";
    &error ("Unable to open $doxFileTmp for writing: $!") unless open(OUT, ">$doxFileTmp");
    &writeArrayToFile(\*OUT, \@doxLines);
    die "Unable to rename $doxFileTmp to $doxFile" unless rename($doxFileTmp, $doxFile);
}

&main();
