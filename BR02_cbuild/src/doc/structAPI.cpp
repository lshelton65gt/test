#include "libdesign.h"
#include <cstdio>

int main()
{
  CarbonObject* hdl = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonWave* wave = hdl->waveCreate("mult.dump", eWaveFileTypeVCD, e1ns);
  if (! wave)
    fprintf(stderr, "VCD Init failed.\n");
  
  if (wave->dumpVars(0, "mult") != eCarbon_OK)
    fprintf(stderr, "dumpvars failed\n");

  // Instantiate the primary i/o structure
  carbon_design_interface ci;

  // initialize the structure (could use memset, here)
  ci.time = 0;
  ci.m_clk = 0;
  ci.m_reset_ = 0;

  for (i = 0; i < 5; ++i)
  {
    // toggle the clock
    ci.m_clk = !ci.m_clk;
    // run the schedule.
    carbon_design_schedule(hdl, &ci);
  }
  
  ci.m_reset_ = 1;
  ci.m_clk = !ci.m_clk;
  carbon_design_schedule(hdl, &ci);
  for (i = 0; i < 100; ++i)
  {
    ci.time = 10*i + 5;
    ci.m_clk = 1;
    carbon_design_schedule(hdl, &ci);
    ci.time = 10*i + 10;
    ci.m_clk = 0;
    carbon_design_schedule(hdl, &ci);
    fprintf(stdout, "%d\t%d\t%d\t%d\n", (int) ci.m_a, (int) ci.m_b,
            (int) ci.m_sum, (int) ci.m_product);
  }
  delete hdl;
  return 0;
}
