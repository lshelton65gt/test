#include "libdesign.h"
#include <iostream>
using namespace std;

static const int fail = 3;

int main()
{
  int stat = 0;
  CarbonObjectID* hdl = 
    carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);

  if (! hdl)
    return fail;
 
  // Dump a waveform
  CarbonWaveID* wave = carbonWaveInitVCD(hdl, 
                                      "flopregvcd.dump", 
                                      e1ns);

  if (! wave)
  {
    cerr << "wave dumpinit failed" << endl;
    stat = fail;
    carbonDestroy(&hdl);
    return stat;
  }

  // 'flopreg' is the top-level module for this design. 
  if (carbonDumpVars(wave, 0, "flopreg") != eCarbon_OK) {
    cerr << "dumpvars failed" << endl;
    stat = fail;
  }

  // Instantiate a time. This will be passed to the 
  // schedule method.
  CarbonTime t = 10;
  
  
  // Find various nets.
  CarbonNetID* dref = carbonFindNet(hdl,"flopreg.d ");
  if (! dref)
  {
    stat = fail;
    cerr << "getref d failed." << endl;
    return stat;
  }

  CarbonNetID* clkref = carbonFindNet(hdl, "flopreg.clk");
  if (! clkref)
  {
    stat = fail;
    cerr << "getref clk failed." << endl;
    return stat;
  }

  // This design contains a memory--it can be initialized with readmemh.
  CarbonMemoryID* mem = 
    carbonFindMemory(hdl, "flopreg.mem1");

  if (carbonReadmemh(mem, "mem.dat") != eCarbon_OK) {
    cerr << "Unable to readmemh mem.dat!" << endl;
    stat = fail;
  }
  
  // Make sure that the inputs are what is expected.

  CarbonDB* hdlDB = carbonGetDB(hdl);
  const CarbonDBNode* drefDB = carbonDBFindNode(hdlDB,"flopreg.d");

  if (! carbonDBIsVector(hdlDB,drefDB))
  {
    stat = fail;
    cerr << "IsVector returned false for vector" << endl;
  }

  if (carbonGetNumUInt32s(dref) != 2)
  {
    stat = fail;
    cerr << "GetNumUint32s returned incorrect size" << endl;
  }

  if (carbonDBGetLSB(hdlDB, drefDB) != 0)
  {
     stat = fail;
     cerr << "carbonDBGetLSB returned incorrect index" << endl;
  }

  if (carbonDBGetMSB(hdlDB, drefDB) != 32)
  {
     stat = fail;
     cerr << "carbonDBGetMSB returned incorrect index" << endl;
  }

  if (! (carbonIsPrimaryPort(hdl, dref) && 
         carbonIsPosedgeClock(hdl, dref) &&
         carbonIsInput(hdl, dref))) 
  {
    stat = fail;
    char dname[16];
    carbonGetNetName(hdl, dref, dname, 16);

    cerr << "Design error: " << dname 
         <<" is not a posedge primary input" << endl;
  }
  
  // Instantiate storage for each of the nets.
  CarbonUInt32 drefDep[2];
  drefDep[0] = 0;
  drefDep[1] = 0;
  CarbonUInt32 clkDep = 0;
  
  carbonDeposit(hdl, dref, drefDep, NULL);

  int i;

  // main program loop
  for (i = 0; i < 3; ++i)
  {
    // toggle clock
    clkDep = ! clkDep;
    carbonDeposit(hdl, clkref, &clkDep, NULL);
    // Call the schedule function and advance time.
    carbonSchedule(hdl, t);
    t += 10;
  }
  
  // VCD checkpoint.
  carbonDumpAll(wave);
  

  /*
    An example of a depositRange. Note however that a full deposit 
    is generally preferred over a partial deposit except on very 
    large arrays (i.e., > 10 words).
  */

  drefDep[0] = 0xffffffff;
  // This only deposits 0xffff into dref.
  if (carbonDepositRange(hdl, dref, drefDep, 16, 0, NULL) 
      != eCarbon_ERROR)
  {
    drefDep[0] = 0;
    carbonExamine(hdl, dref, drefDep, NULL);
    if (drefDep[0] != 0xffff) 
      cerr << "Unexpected value reading dref" << endl;
  }
  else {
    cerr << "Error depositing range onto dref" << endl;
    stat = fail;
  }
  
  // Turn VCD dumping off.
  carbonDumpOff(wave);
  
  // Continue running Carbon Model.
  for (i = 0; i < 10; ++i)
  {
    clkDep = ! clkDep;
    if (i == 5)
      // Turn VCD dumping back on.
      carbonDumpOn(wave);
    
    carbonDeposit(hdl, clkref, &clkDep, NULL);
    carbonSchedule(hdl, t);
    t += 10;
  }
  
  // Clean up model (flushes waveforms and, if enabled, profiling data)
  carbonDestroy(&hdl);
  return stat;
}
