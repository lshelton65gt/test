// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "flow/FLNode.h"
#include "flow/FLIter.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNetRef.h"
#include "util/CarbonAssert.h"
#include "util/UtIOStream.h"
#include "util/CbuildMsgContext.h"

/*!
  \file
  Implementation of FLNode and FLNodeBound classes.
*/

FLNode::FLNode(const NUNetRefHdl &net_ref, NUUseDefNode *node, FLTypeT type) :
  mNetRef(net_ref),
  mUseDefNode(node),
  mType(type),
  mNestingHier(false),
  mMark(false),
  mIsAllocated(true)
{}


NUNet *FLNode::getDefNet() const
{
  return (*mNetRef).getNet();
}


bool FLNode::hasNestedHier() const
{
  return ((!mNested.empty()) && mNestingHier);
}


bool FLNode::hasNestedBlock() const
{
  return ((!mNested.empty()) && not mNestingHier);
}

FLNode* FLNode::getSingleNested() const
{
  FLNodeSet::const_iterator iter = mNested.begin();
  if (iter == mNested.end())
    return NULL;
  FLNodeSet::const_iterator next = iter;
  ++next;
  if (next != mNested.end())
    return NULL;
  return (*iter);
}


UInt32 FLNode::numNestedFlows() const
{
  return mNested.size();
}

void FLNode::addNestedHier(FLNode *nested)
{
  FLN_ASSERT(nested != this, this);
  mNested.insert(nested);
  mNestingHier = true;
}


void FLNode::addNestedBlock(FLNode *nested)
{
  FLN_ASSERT(nested != this, this);
  mNested.insert(nested);
  mNestingHier = false;
}


void FLNode::clearFanin()
{
  mFanin.clear();
}


void FLNode::clearEdgeFanin()
{
  mEdgeFanin.clear();
}

void FLNode::clearNestedBlock()
{
  mNested.clear();
}

void FLNode::connectFanin(FLNode *fanin_node)
{
  mFanin.insert(fanin_node);
}


void FLNode::connectFaninFromLoop(FaninLoop & loop)
{
  for ( /*no initial*/; not loop.atEnd(); ++loop) {
    connectFanin(*loop);
  }
}


void FLNode::connectFanin(const NUNetRefHdl &net_ref, NUNetRefCompareFunction fn)
{
  for (NUNetRef::DriverLoop loop = net_ref->loopContinuousDrivers(fn);
       not loop.atEnd();
       ++loop) {
    connectFanin(*loop);
  }
}

bool
FLNode::isSameBidPortConnection(FLNode* fanin)
{
  //
  // The conditions under which the flow and fanin represent the same
  // port connection are:
  //
  // 1. *this* flow node is for a bid port connect
  //
  // 2. the fanin flow node is for a module instance
  //
  // 3. the fanin's nested flow node is for the same bid port
  //    connection.
  NUUseDefNode* useDef = getUseDefNode();
  NUUseDefNode* faninUseDef = fanin->getUseDefNode();
  if ((useDef != NULL) && (useDef->getType() == eNUPortConnectionBid) &&
      (faninUseDef != NULL) && (faninUseDef->getType() == eNUModuleInstance))
  {
    // check the ports nested under the module instance to see
    // if any has the same usedef as this port
    for (Iter<FLNode*> loop = fanin->loopNested(); !loop.atEnd(); ++loop)
    {
      FLNode* nested = *loop;
      if (nested->getUseDefNode() == useDef)
        return true;
    }
  }
  return false;
}


void
FLNode::connectFaninSkipBidCycle(const NUNetRefHdl &net_ref,
                                 NUNetRefCompareFunction fn)
{
  for (NUNetRef::DriverLoop loop = net_ref->loopContinuousDrivers(fn);
       not loop.atEnd();
       ++loop) {
    // Because we are using loopContinuousDrivers on the fanin net, we will find
    // the module instance as a driver for any bid port
    // connections. This looks like an immediate cycle. This pessimism
    // causes global constant propagation to fail. So prune that case
    FLNode* fanin = *loop;
    if (!isSameBidPortConnection(fanin)) {
      connectFanin(fanin);
    }
  }
}


void FLNode::connectEdgeFanin(FLNode *fanin_node)
{
  mEdgeFanin.insert(fanin_node);
}


void FLNode::connectEdgeFaninFromLoop(FaninLoop & loop)
{
  for ( /*no initial*/; not loop.atEnd(); ++loop) {
    connectEdgeFanin(*loop);
  }
}


void FLNode::removeFanin(FLNode* driver)
{
  FLNodeSet::iterator p;
  
  if ((p = mFanin.find(driver)) != mFanin.end()) {
    mFanin.erase(p);
  }

  if ((p = mEdgeFanin.find(driver)) != mEdgeFanin.end()) {
    mEdgeFanin.erase(p);
  }
}

void FLNode::removeLevelFanin(FLNode* driver)
{
  FLNodeSet::iterator p;
  if ((p = mFanin.find(driver)) != mFanin.end()) {
    mFanin.erase(p);
  }
}

void FLNode::removeEdgeFanin(FLNode* driver)
{
  FLNodeSet::iterator p;
  if ((p = mEdgeFanin.find(driver)) != mEdgeFanin.end()) {
    mEdgeFanin.erase(p);
  }
}

void FLNode::connectEdgeFanin(const NUNetRefHdl &net_ref, NUNetRefCompareFunction fn)
{
  for (NUNetRef::DriverLoop loop = net_ref->loopContinuousDrivers(fn);
       not loop.atEnd();
       ++loop) {
    connectEdgeFanin(*loop);
  }
}

void
FLNode::connectEdgeFaninSkipBidCycle(const NUNetRefHdl &net_ref,
                                     NUNetRefCompareFunction fn)
{
  for (NUNetRef::DriverLoop loop = net_ref->loopContinuousDrivers(fn);
       not loop.atEnd();
       ++loop) {
    // Because we are using loopContinuousDrivers on the fanin net, we will find
    // the module instance as a driver for any bid port
    // connections. This looks like an immediate cycle. This pessimism
    // causes global constant propagation to fail. So prune that case
    FLNode* fanin = *loop;
    if (!isSameBidPortConnection(fanin)) {
      connectEdgeFanin(fanin);
    }
  }
}

//! Rewrite a single fanin set based on a replacement map.
/*! 
 * We iterate over the fanin sets instead of the replacement map
 * because it is expected that the fanin sets are much smaller than
 * the replacement map. The copy at the end should be cheaper than
 * erase and insert for each individual fanin
 */
static void sRewriteOneFaninSetWithMap(FLNodeSet & fanin_set, 
                                       const FLNodeNodeSetMap &replacement_map)
{
  FLNodeSet replacements;
  bool replacement_needed = false;

  FLNodeNodeSetMapConstIter map_end = replacement_map.end();
  for (FLNodeSet::iterator iter = fanin_set.begin();
       iter != fanin_set.end();
       ++iter) {
    FLNode * old_node = *iter;
    FLNodeNodeSetMapConstIter map_location = replacement_map.find(old_node);
    if (map_location != map_end) {
      const FLNodeSet &replace_set = (*map_location).second;
      replacements.insert(replace_set.begin(), replace_set.end());
      replacement_needed = true;
    } else {
      replacements.insert(old_node);
    }
  }

  if (replacement_needed) {
    fanin_set = replacements;
  }
}

void FLNode::replaceFaninsWithMap(const FLNodeNodeSetMap &replacement_map)
{
  sRewriteOneFaninSetWithMap(mFanin,replacement_map);
  sRewriteOneFaninSetWithMap(mEdgeFanin,replacement_map);
}


void FLNode::replaceFaninsWithLoop(const FLNodeList& drivers,
				   NUNet::DriverLoop new_drivers)
{
  bool level = false, edge = false;
  for (FLNodeList::const_iterator iter = drivers.begin();
       iter != drivers.end();
       ++iter)
  {
    FLNodeSet::iterator p;
    FLNode* flow = *iter;
    if ((p = mFanin.find(flow)) != mFanin.end())
    {
      level = true;
      mFanin.erase(p);
    }
    if ((p = mEdgeFanin.find(flow)) != mEdgeFanin.end())
    {
      edge = true;
      mEdgeFanin.erase(p);
    }
  }
  FLN_ASSERT(level or edge, this);

  if (level) {
    mFanin.insert(new_drivers.begin(), new_drivers.end());
  }

  if (edge) {
    mEdgeFanin.insert(new_drivers.begin(), new_drivers.end());
  }
}


void FLNode::replaceFanin(FLNode *old_driver, FLNode *new_driver)
{
  int num = mFanin.erase(old_driver);
  FLN_ASSERT(num == 1, this);
  connectFanin(new_driver);
}


void FLNode::replaceDefNetRef(const NUNetRefHdl &old_ref, const NUNetRefHdl &new_ref)
{
  FLN_ASSERT(*old_ref == *mNetRef, this);
  mNetRef = new_ref;
}


int FLNode::compare(const FLNode * f1, const FLNode * f2) {
  if (f1==f2) return 0;

  // sort by useDef
  NUUseDefNode * ud1 = f1->getUseDefNode();
  NUUseDefNode * ud2 = f2->getUseDefNode();
  int cmp = NUUseDefNode::compare(ud1,ud2);
  if (cmp==0) {
    // If we have the same UseDef node, we know
    // that all FLNode will have nets in the
    // same module. Further, there will only be
    // one FLNode per net. This means we can 
    // compare NUNet instead of NUNetRef.
    NUNet * n1 = f1->getDefNet();
    NUNet * n2 = f2->getDefNet();
    cmp = NUNet::compare(n1,n2);

    if (cmp==0) {
      // If we have two different nets of the same name as outputs
      // from the same UseDef, fall back to ptr comparison. This can
      // occur when there are nested named blocks containing variable
      // declarations with the same name (see
      // beacon/testcases/statemnt/FOR16).
      cmp = (n1==n2 ? 0 : (n1<n2 ? -1 : 1));
    }
  }
  return cmp;
}


typedef Loop<FLNodeSet> NodeLoop;
FLNode::FaninLoop FLNode::loopFanin()
{
  return FLNode::FaninLoop::create(NodeLoop(mFanin));
}

FLNode::FaninLoop FLNode::loopEdgeFanin()
{
  return FLNode::FaninLoop::create(NodeLoop(mEdgeFanin));
}

FLNode::AllFaninLoop FLNode::loopAllFanin()
{
  return AllFaninLoop(loopFanin(), loopEdgeFanin());
}

Iter<FLNode*> FLNode::loopNested()
{
  return Iter<FLNode*>::create(NodeLoop(mNested));
}

bool FLNode::isFanin(FLNode* fanin, Sensitivity sense) const
{
  if ((sense != eSensEdge) && (mFanin.find(fanin) != mFanin.end()))
    return true;
  if ((sense != eSensLevel) && (mEdgeFanin.find(fanin) != mEdgeFanin.end()))
    return true;
  return false;
}

bool FLNode::isNested(FLNode* flow, bool recurse) const
{
  if (mNested.find(flow) != mNested.end())
    return true;
  if (recurse)
  {
    for (FLNodeSet::const_iterator iter = mNested.begin(); iter != mNested.end(); ++iter)
    {
      const FLNode* nested = *iter;
      if (nested->isNested(flow,true))
        return true;
    }
  }
  return false;
}

FLNodeSet *FLNode::getFaninForNet(const NUNetRefHdl &net_ref, NUNetRefCompareFunction fn) const
{
  FLNodeSet* new_set = new FLNodeSet();
  for (FLNodeSet::const_iterator iter = mFanin.begin();
       iter != mFanin.end(); ++iter)
  {
    FLNode* fanin = *iter;
    NUNetRefHdl fanin_ref = fanin->getDefNetRef();
    if (((*fanin_ref).*fn)(*net_ref)) {
      new_set->insert(*iter);
    }
  }
  return new_set;
}


FLNodeIter* FLNode::makeFaninIter(FLIterFlags::TraversalT visit,
				  FLIterFlags::TraversalT stop,
				  FLIterFlags::NestingT nesting,
				  bool visit_start,
				  FLIterFlags::IterationT iteration)
{
  return new FLNodeIter(this,
			visit,
			stop,
			nesting,
			visit_start,
			iteration);
}


bool FLNode::checkFaninOverlap(const FLNode * fanin, NUNetRefFactory * netRefFactory)
{
  NUUseDefNode* useDef = getUseDefNode();
  if (useDef == NULL) {
    return false; // Bound nodes have no fanin.
  }

  // Port connections can have uses in a different hierarchy -- don't check their fanin
  NUType type = useDef->getType();
  if ((type == eNUPortConnectionInput) ||
      (type == eNUPortConnectionOutput) ||
      (type == eNUPortConnectionBid))
    return true;

  // $extnet can have fanin to protected-observable nets even though they are not in its uses
  // it is also used for hierrefs in ways that are inconsistent with UD
  // $ostnet for a module instance can have fanin directly to port connections
  // (it has no nested port connection of its own)
  // $cfnet is being excluded on the basis of guilt-by-association
  if (getDefNet()->isVirtualNet())
    return true;

  // check if the fanin's def overlaps our use
  if (type==eNUAlwaysBlock) {
    NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(useDef);
    // Don't call NUAlwaysBlock::queryUses, as that includes edge uses in the search.
    useDef = always->getBlock();
  }

  if (useDef->useBlockingMethods())
  {
    NUUseDefStmtNode* stmt = dynamic_cast<NUUseDefStmtNode*>(useDef);
    return  (stmt->queryBlockingUses(getDefNetRef(),
                                     fanin->getDefNetRef(),
                                     &NUNetRef::overlapsSameNet,
                                     netRefFactory)
             or
             stmt->queryNonBlockingUses(getDefNetRef(),
                                        fanin->getDefNetRef(),
                                        &NUNetRef::overlapsSameNet,
                                        netRefFactory));
  }
  else
  {
    return (useDef->queryUses(getDefNetRef(),
                              fanin->getDefNetRef(),
                              &NUNetRef::overlapsSameNet,
                              netRefFactory));
  }
}


bool FLNode::checkEdgeFaninOverlap(const FLNode * fanin, NUNetRefFactory * netRefFactory)
{
  NUUseDefNode* useDef = getUseDefNode();
  if (useDef == NULL) {
    return false; // Bound nodes have no fanin.
  }

  // Note: In checkFaninOverlap, we blindly accept virtual nets as
  // fanin. Virtual nets cannot appear in edge expressions, so they
  // are not considered in our edge fanin check.

  // Check if the fanin's def overlaps our edge uses; only always
  // blocks have edges.
  NUType type = useDef->getType();
  if (type==eNUAlwaysBlock) {
    NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(useDef);
    return always->queryEdgeUses(fanin->getDefNetRef(),
                                 &NUNetRef::overlapsSameNet,
                                 netRefFactory);
  } else {
    return false; 
  }
}


void FLNode::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  NUBase::indent(numspaces);

  UtIO::cout() << "FLNode(" << this << ") : NUNet(" << mNetRef->getNet()
       << ")  node NUUseDefNode(" << mUseDefNode;
  if (!mNested.empty()) {
    UtIO::cout() << ")  nested FLNode(";
    bool isFirst = true;
    for (FLNodeSet::const_iterator iter = mNested.begin(); iter != mNested.end(); ++iter) {
      FLNode* nested = *iter;
      if (!isFirst)
        UtIO::cout() << ", ";        
      UtIO::cout() << nested;
      isFirst = false;
    }
  }
  UtIO::cout() << ")" << UtIO::endl;

  mNetRef->print(numspaces+2);

  if (mUseDefNode) {
    mUseDefNode->print(false, numspaces+2);
  }

  if (recurse) {
    if (not mFanin.empty())
    {
      NUBase::indent(numspaces);
      UtIO::cout() << "fanin(" << UtIO::endl;
      for (FLNodeSet::const_iterator iter = mFanin.begin(); iter != mFanin.end(); ++iter) {
        FLNode* fanin = *iter;
        fanin->print(false, numspaces+2);
      }
      NUBase::indent(numspaces);
      UtIO::cout() << ")" << UtIO::endl;
    }
    if (not mEdgeFanin.empty())
    {
      NUBase::indent(numspaces);
      UtIO::cout() << "edgefanin(" << UtIO::endl;
      for (FLNodeSet::const_iterator iter = mEdgeFanin.begin(); iter != mEdgeFanin.end(); ++iter) {
        FLNode* edgeFanin = *iter;
        edgeFanin->print(false, numspaces+2);
      }
      NUBase::indent(numspaces);
      UtIO::cout() << ")" << UtIO::endl;
    }
    if (!mNested.empty()) {
      NUBase::indent(numspaces);
      UtIO::cout() << " nested(" << UtIO::endl;
      for (FLNodeSet::const_iterator iter = mNested.begin(); iter != mNested.end(); ++iter) {
        FLNode* nested = *iter;
        nested->print(false, numspaces+2);
      }
      NUBase::indent(numspaces);
      UtIO::cout() << " )" << UtIO::endl;      
    }
  }
}


void FLNode::ppPrint(int numspaces) const
{
  NUBase::indent(numspaces);
  UtIO::cout() << "(flow " << this << " (net " << mNetRef->getNet() << ") (usedef " << mUseDefNode << ")";
  if (!mNested.empty()) {
    UtIO::cout() << " (nested ";
    bool isFirst = true;
    for (FLNodeSet::const_iterator iter = mNested.begin(); iter != mNested.end(); ++iter) {
      FLNode* nested = *iter;
      if (!isFirst)
        UtIO::cout() << ", ";        
      UtIO::cout() << nested;
      isFirst = false;
    }
    UtIO::cout() << ")";
  }
  UtIO::cout() << ")" << UtIO::endl;
}



NUUseDefNode* FLNodeBound::getUseDefNode() const
{
  return 0;
}


void FLNodeBound::clearFanin()
{
  // TBD For now, Not valid to do this.
  FLN_ASSERT(0, this);
}


void FLNodeBound::connectFanin(FLNode * /* fanin_node -- unused */)
{
  // TBD For now, Not valid to ask this -- use the iterator
  // In the future, do this??
  FLN_ASSERT(0, this);
}


void FLNodeBound::connectFanin(const NUNetRefHdl & /* net_ref -- unused */,
			       NUNetRefCompareFunction /* fn -- unused */)
{
  // TBD For now, Not valid to ask this -- use the iterator
  // In the future, do this??
  FLN_ASSERT(0, this);
}


void FLNodeBound::replaceFanin(FLNode * /* old_fanin_node -- unused */,
			       FLNode * /* new_fanin_node -- unused */)
{
  // TBD For now, Not valid to ask this -- use the iterator
  // In the future, do this??
  FLN_ASSERT(0, this);
}


void FLNodeBound::replaceDefNetRef(const NUNetRefHdl & old_ref,
				   const NUNetRefHdl & new_ref)
{
  FLN_ASSERT(*old_ref == *mNetRef, this);
  mNetRef = new_ref;
}


void FLNodeBound::print(bool /* recurse -- unused */,
			int indent_arg) const
{
  // bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  NUBase::indent(numspaces);
  UtIO::cout() << "FLNodeBound(" << this << ") : NUNet(" << mNetRef->getNet() << ")" << UtIO::endl;

  mNetRef->print(numspaces+2);
}


void FLNodeBound::ppPrint(int numspaces) const
{
  NUBase::indent(numspaces);
  UtIO::cout() << "(flowbound " << this << " (net " << mNetRef->getNet() << ")" << ")" << UtIO::endl;
}


void FLNode::printAssertInfo(const char* file, int line, const char* expStr)
  const
{
  // NOTE: This implementation is derived from FLNodeElab::printAssertInfo, but
  // the elaborated pieces have been cut out.

  // Print all the info we need about an object in order for the user to
  // find the problem.  We'd like to give human readable data if possible,
  // which is different depending on the type of object
  UtIO::cerr() << "\n\n******CARBON INTERNAL ERROR*******\n\n";
  if (this == NULL) {
    UtIO::cerr() << "(null object)\n";

  } else {
    UtString str;
    mNetRef->compose(&str, NULL, false, false);
    UtIO::cerr() << "UnelabNet: " << str << "\n";

    // Get the use def node for further processing.
    NUUseDefNode* useDef = getUseDefNode();

    // Now finally print the use def node if it is available. We use
    // the NU_ASSERT helper function which aborts
    if (useDef != NULL) {
      useDef->print(false, 0);
#ifdef CDB
      useDef->printAssertInfoHelper();
#endif
      UtIO::cout().flush();
    }
  }

  UtIO::cerr() << file << ':' << line << " FLN_ASSERT(" << expStr 
               << ") failed\n";
  UtIO::cerr().flush();
  // print stack trace ???
  abort();
}

void MsgContext::composeLocation(FLNode* flow, UtString* location)
{
  NUUseDefNode* useDef = (flow == NULL) ? NULL : flow->getUseDefNode();

  UtString buf;
  if (flow != NULL)
  {
    NUNet* defNet = flow->getDefNet();
    if (defNet != NULL)
    {
      defNet->compose(&buf, NULL);
    }
  }

  if (useDef == NULL)
  {
    if (buf.empty())
      *location << "(null NUNet)";
    else
      *location << "Net " << buf;
  }
  else
  {
    const SourceLocator& loc = useDef->getLoc();
    UtString source;
    loc.compose(&source);
    *location << source << ' ' << buf;
  }
}

void FLNode::putIsAllocated(bool flag) {
  mIsAllocated = flag;
}
