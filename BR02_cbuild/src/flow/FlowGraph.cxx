// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Contains method implementations for the FlowGraph adapter class.
*/

#include "util/Graph.h"
#include "util/GenericDigraph.h"
#include "util/GraphDot.h"
#include "flow/FlowGraph.h"
#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"
#include "nucleus/NUDesign.h"
#include "symtab/STSymbolTable.h"
#include "util/UtMap.h"
#include "util/UtIOStream.h"

#if !defined(__COVERITY__)
template class FlowGraph<FLNode>;
template class FlowGraph<FLNodeElab>;
template class FlowGraphDotWriter<FLNode>;
template class FlowGraphDotWriter<FLNodeElab>;
#endif

static void helperComposeFlowName(FLNode* flow, UtString* buf)
{
  (*buf) << flow->getName()->str();
}

static void helperComposeFlowName(FLNodeElab* flow, UtString* buf)
{
  flow->getName()->compose(buf);
}

//! label each node with its elaborated name
template<typename Flow>
void FlowGraphDotWriter<Flow>::writeLabel(Graph* g, GraphNode* node)
{
  typename FlowGraph<Flow>::GraphType* fg = dynamic_cast<typename FlowGraph<Flow>::GraphType*>(g);
  typename FlowGraph<Flow>::GraphType::Node* n = fg->castNode(node);
  Flow* flow = n->getData();
  UtString buf;
  helperComposeFlowName(flow,&buf);
  NUUseDefNode* useDef = flow->getUseDefNode();
  UtString nodeType;
  if (useDef)
    nodeType = useDef->typeStr();
  else
    nodeType = "Bound";
  *mOut << "[ label=\"" << buf << " " << nodeType << " (" << flow << ")\", ";
  switch (flow->getType())
  {
    case eFLBoundHierRef:
      *mOut << "style=dashed, color=blue";
      break;
    case eFLBoundPort:
      *mOut << "style=dashed, color=red";
      break;
    case eFLBoundProtect:
      *mOut << "style=dashed, color=green";
      break;
    case eFLBoundUndriven:
      *mOut << "style=dashed, color=yellow";
      break;
    case eFLCycle:
      *mOut << "style=solid, color=gray75";
      break;
    case eFLNormal:
    default:
      *mOut << "style=solid, color=black";
  }
  *mOut << " ]";
}

//! set the edge attributes based on the edge, level or nested type
template<typename Flow>
void FlowGraphDotWriter<Flow>::writeLabel(Graph* g, GraphEdge* edge)
{
  if (g->anyFlagSet(edge, eLevel))
  {
    *mOut << "[ style=solid, color=black ]";
  }
  else if (g->anyFlagSet(edge, eEdge))
  {
    *mOut << "[ style=dashed, color=blue ]";
  }
  else
  {
    INFO_ASSERT(g->anyFlagSet(edge, eNested), "Nesting consistency check failed.");
    *mOut << "[ style=dotted, color=gray75 ]";
  }
}

template<typename Flow>
FlowGraph<Flow>::FlowGraph(NUDesign* design, STSymbolTable* symtab)
  : mDesign(design), mSymtab(symtab), mModule(NULL), mShow(), mGraph(NULL), mStale(true)
{
}

template<typename Flow>
FlowGraph<Flow>::FlowGraph(NUModule* module)
  : mDesign(NULL), mSymtab(NULL), mModule(module), mShow(), mGraph(NULL), mStale(true)
{
}

template<typename Flow>
void FlowGraph<Flow>::addNet(typename Flow::DefNetType* net)
{
  // add the flow nodes for all drivers of this net
  for (typename Flow::DefNetType::DriverLoop l = net->loopContinuousDrivers(); !l.atEnd(); ++l)
  {
    Flow* driver = *l;
    addFlow(driver);
  }
}

template<typename Flow>
void FlowGraph<Flow>::addFlow(Flow* flow, bool addNested)
{
  // add this flow node
  if (mShow.find(flow) == mShow.end())
  {
    mShow.insert(flow);
    mStale = true;
  }

  // recurse into nested flow
  if (addNested && (flow->hasNestedBlock() || flow->hasNestedHier()))
  {
    for (Iter<Flow*> loop = flow->loopNested(); !loop.atEnd(); ++loop)
    {
      Flow* nested = *loop;
      addFlow(nested,addNested);
    }
  }
}

template<typename Flow>
void FlowGraph<Flow>::addFanin(Flow* flow, bool addNested)
{
  // Add fanin
  for (Iter<Flow*> f = flow->loopFanin(); !f.atEnd(); ++f)
    addFlow(*f, addNested);
  for (Iter<Flow*> f = flow->loopEdgeFanin(); !f.atEnd(); ++f)
    addFlow(*f, addNested);

  // recurse into nested flow to add additional fanin
  if (addNested && (flow->hasNestedBlock() || flow->hasNestedHier()))
  {
    for (Iter<Flow*> loop = flow->loopNested(); !loop.atEnd(); ++loop)
    {
      Flow* nested = *loop;
      addFanin(nested,addNested);
    }
  }
}

template<typename Flow>
void FlowGraph<Flow>::addFanout(Flow* /*flow*/)
{
}

template<typename Flow>
void FlowGraph<Flow>::clear()
{
  mShow.clear();
  delete mGraph;
  mStale = true;
}

template<typename Flow>
void FlowGraph<Flow>::write(const char* filename)
{
  update();
  if (mGraph == NULL)
    return;
  
  UtString fname(filename);
  FlowGraphDotWriter<Flow> fgdw(fname);
  fgdw.output(mGraph, "Flow");
}

template<typename Flow>
FlowGraph<Flow>* FlowGraph<Flow>::createUnelab(NUModule* module)
{
  return new FlowGraph<Flow>(module);
}

template<typename Flow>
FlowGraph<Flow>* FlowGraph<Flow>::createElab(NUDesign* design, STSymbolTable* symtab)
{
  return new FlowGraph<Flow>(design, symtab);
}

template<>
void FlowGraph<FLNodeElab>::placeHolder()
{
  // provides a place for breakpoints, and gets the debug class linked in
}

template<>
void FlowGraph<FLNode>::placeHolder()
{
  // provides a place for breakpoints, and gets the debug class linked in
}

template<typename Flow>
FlowGraph<Flow>::~FlowGraph()
{
  delete mGraph;
}

template<>
void FlowGraph<FLNodeElab>::update()
{
  if (!mDesign || !mSymtab)
    return;

  if (!mStale)
    return;
  mStale = false;

  delete mGraph;
  mGraph = new GraphType;

  NodeMap nodeMap;

  // set up graph nodes for each FLNodeElab
  FLDesignElabIter iter(mDesign, mSymtab,
                        FLIterFlags::eAllElaboratedNets,
                        FLIterFlags::eAll,
                        FLIterFlags::eNone,
                        FLIterFlags::eBranchAtAll,
                        FLIterFlags::eIterNodeOnce);
  for (;!iter.atEnd(); ++iter)
  {
    FLNodeElab* elab = *iter;

    // if mShow has been specified, use it to determine the shown subgraph
    if (!mShow.empty() && (mShow.find(elab) == mShow.end()))
      continue;
    
    FLN_ELAB_ASSERT(nodeMap.find(elab) == nodeMap.end(), elab);
    GraphType::Node* node = new GraphType::Node(elab);
    mGraph->addNode(node);
    nodeMap[elab] = node;
  }

  // add edges for each FLNodeElab
  for (iter.begin();!iter.atEnd(); ++iter)
  {
    FLNodeElab* elab = *iter;

    NodeMap::iterator n = nodeMap.find(elab);
    if (n == nodeMap.end())
      continue;

    GraphType::Node* node = n->second;

    addEdges(node,elab,nodeMap);
  }
}


template<>
void FlowGraph<FLNode>::update()
{
  if (!mModule)
    return;

  if (!mStale)
    return;
  mStale = false;

  delete mGraph;
  mGraph = new GraphType;

  NodeMap nodeMap;

  // set up graph nodes for each FLNode in this module
  NUNetList allNets;
  mModule->getAllNonTFNets(&allNets);
  FLNetIter iter(&allNets,
                 FLIterFlags::eAll,
                 FLIterFlags::eNone,
                 FLIterFlags::eBranchAtAll,
                 FLIterFlags::eIterNodeOnce);
  for (;!iter.atEnd(); ++iter)
  {
    FLNode* flow = *iter;

    // if mShow has been specified, use it to determine the shown subgraph
    if (!mShow.empty() && (mShow.find(flow) == mShow.end()))
      continue;
    
    FLN_ASSERT(nodeMap.find(flow) == nodeMap.end(), flow);
    GraphType::Node* node = new GraphType::Node(flow);
    mGraph->addNode(node);
    nodeMap[flow] = node;
  }

  // add edges for each FLNode
  for (iter.begin();!iter.atEnd(); ++iter)
  {
    FLNode* flow = *iter;

    NodeMap::iterator n = nodeMap.find(flow);
    if (n == nodeMap.end())
      continue;

    GraphType::Node* node = n->second;

    addEdges(node,flow,nodeMap);
  }
}

template<typename Flow>
void FlowGraph<Flow>::addEdges(typename GraphType::Node* node, Flow* flow, NodeMap& nodeMap)
{
    // we have to guarantee that we don't process a node more than once, since the iterator won't do it
  static const UInt32 visitedFlag = 1;
    if (mGraph->anyFlagSet(node,visitedFlag))
    return;
    mGraph->setFlags(node,visitedFlag);

  typename NodeMap::iterator n;

    // create edges for level fanin
  for (Iter<Flow*> loop = flow->loopFanin(); !loop.atEnd(); ++loop)
    {
      n = nodeMap.find(*loop);
      if (n == nodeMap.end())
        continue;
    typename GraphType::Node* to = n->second;
    typename GraphType::Edge* edge = new typename GraphType::Edge(to,NULL);
      mGraph->setFlags(edge, eLevel);
      node->addEdge(edge);
    }

    // create edges for edge fanin
  for (Iter<Flow*> loop = flow->loopEdgeFanin(); !loop.atEnd(); ++loop)
    {
      n = nodeMap.find(*loop);
      if (n == nodeMap.end())
        continue;
    typename GraphType::Node* to = n->second;
    typename GraphType::Edge* edge = new typename GraphType::Edge(to,NULL);
      mGraph->setFlags(edge, eEdge);
      node->addEdge(edge);
    }

    // create edges to show nesting
  for (Iter<Flow*> loop = flow->loopNested(); !loop.atEnd(); ++loop)
    {
      n = nodeMap.find(*loop);
      if (n == nodeMap.end())
        continue;
    typename GraphType::Node* to = n->second;
    typename GraphType::Edge* edge = new typename GraphType::Edge(to,NULL);
      mGraph->setFlags(edge, eNested);
      node->addEdge(edge);
  }
}
