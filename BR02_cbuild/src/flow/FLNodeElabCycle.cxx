// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUCycle.h"
#include "flow/FLNode.h"
#include "flow/FLNodeElabCycle.h"
#include "util/UtIOStream.h"

FLNodeElabCycle::~FLNodeElabCycle()
{
}

//! is this node an encapsuled cycle?
bool FLNodeElabCycle::isEncapsulatedCycle() const
{
  return true;
}

FLTypeT FLNodeElabCycle::getType() const
{
  return eFLCycle;
}

const char* FLNodeElabCycle::typeStr() const
{
  return "FLNodeElabCycle";
}

NUCycle* FLNodeElabCycle::getCycle()
{
  return mNode;
}

const NUCycle* FLNodeElabCycle::getCycle() const
{
  return mNode;
}

NUUseDefNode* FLNodeElabCycle::getUseDefNode() const
{
  return mNode;
}

bool FLNodeElabCycle::isBoundNode() const
{
  return false;
}

bool FLNodeElabCycle::isFanin(FLNodeElab*, Sensitivity) const
{
  FLN_ELAB_ASSERT("fanin senitivity is not supported on FLNodeElabCycle"==NULL, this);
  return false;
}
    
void FLNodeElabCycle::clearFanin()
{
  // All fanin manipulations go through NUCycle
  FLN_ELAB_ASSERT("using FLNodeElabCycle instead of NUCycle" == NULL, this);
}

void FLNodeElabCycle::clearEdgeFanin()
{
  // All fanin manipulations go through NUCycle
  FLN_ELAB_ASSERT("using FLNodeElabCycle instead of NUCycle" == NULL, this);
}

void FLNodeElabCycle::connectFanin(FLNodeElab*)
{
  // All fanin manipulations go through NUCycle
  FLN_ELAB_ASSERT("using FLNodeElabCycle instead of NUCycle" == NULL, this);
}

void FLNodeElabCycle::connectEdgeFanin(FLNodeElab*)
{
  // All fanin manipulations go through NUCycle
  FLN_ELAB_ASSERT("using FLNodeElabCycle instead of NUCycle" == NULL, this);
}

void FLNodeElabCycle::replaceFaninsWithLoop(const FLNodeElabList&,
                                            NUNetElab::DriverLoop)
{
  // All fanin manipulations go through NUCycle
  FLN_ELAB_ASSERT("using FLNodeElabCycle instead of NUCycle" == NULL, this);
} // void FLNodeElabCycle::replaceFaninsWithLoop


void FLNodeElabCycle::replaceFaninWithFanin(FLNodeElab*)
{
  // All fanin manipulations go through NUCycle
  FLN_ELAB_ASSERT("using FLNodeElabCycle instead of NUCycle" == NULL, this);
}


void FLNodeElabCycle::removeFanin(FLNodeElab*)
{
  // All fanin manipulations go through NUCycle
  FLN_ELAB_ASSERT("using FLNodeElabCycle instead of NUCycle" == NULL, this);
}

void FLNodeElabCycle::removeLevelFanin(FLNodeElab*)
{
  // All fanin manipulations go through NUCycle
  FLN_ELAB_ASSERT("using FLNodeElabCycle instead of NUCycle" == NULL, this);
}

void FLNodeElabCycle::removeEdgeFanin(FLNodeElab*)
{
  // All fanin manipulations go through NUCycle
  FLN_ELAB_ASSERT("using FLNodeElabCycle instead of NUCycle" == NULL, this);
}

bool FLNodeElabCycle::hasLevelFanin() const
{
  FLN_ELAB_ASSERT(mFanin.empty(), this);
  FLN_ELAB_ASSERT(mEdgeFanin.empty(), this);
  return (mNode->numFanins() > 0);
}

bool FLNodeElabCycle::hasEdgeFanin() const
{
  // Cycle nodes treat all fanin as level fanin
  FLN_ELAB_ASSERT(mFanin.empty(), this);
  FLN_ELAB_ASSERT(mEdgeFanin.empty(), this);
  return false;
}

FLNodeElabLoop FLNodeElabCycle::loopFanin()
{
  FLN_ELAB_ASSERT(mFanin.empty(), this);
  FLN_ELAB_ASSERT(mEdgeFanin.empty(), this);
  return mNode->loopFanin();
}

FLNodeElabLoop FLNodeElabCycle::loopEdgeFanin()
{
  FLN_ELAB_ASSERT(mFanin.empty(), this);
  FLN_ELAB_ASSERT(mEdgeFanin.empty(), this);
  return FLNodeElabLoop::create(Loop<FLNodeElabSet>(mEdgeFanin));
}

FLNodeElabSet *FLNodeElabCycle::getFaninForNet(NUNetElab*) const
{
  FLN_ELAB_ASSERT(mFanin.empty(), this);
  FLN_ELAB_ASSERT(mEdgeFanin.empty(), this);
  FLN_ELAB_ASSERT("Cannot get fanin for a net using an FLNodeElabCycle" == NULL, this);
  return NULL;
}

void FLNodeElabCycle::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  NUBase::indent(numspaces);
  UtIO::cout() << typeStr() << "(" << this << ") : NUNetElab(" << mNetElab
       << ")  BranchNode(" << mHier ;
  UtIO::cout() << ")  FLNode(" << mFLNode << ")" << UtIO::endl;

  if (mNetElab) {
    mNetElab->print(false, numspaces+2);
  }
  if (mFLNode) {
    mFLNode->print(false, numspaces+2);
  }

  if (recurse) {
    if (mNode->numFanins() > 0)
    {
      NUBase::indent(numspaces);
      UtIO::cout() << "fanin(" << UtIO::endl;
      for (FLNodeElabLoop l = mNode->loopFanin(); !l.atEnd(); ++l)
      {
        FLNodeElab* fanin = *l;
        fanin->print(false, numspaces+2);
      }
      NUBase::indent(numspaces);
      UtIO::cout() << ")" << UtIO::endl;
    }
    FLN_ELAB_ASSERT(mFanin.empty(), this);
    FLN_ELAB_ASSERT(mEdgeFanin.empty(), this);
    FLN_ELAB_ASSERT(mNested.empty(), this);
  }
}
