// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implements a cache for elaborated UD
*/

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FLElabUseCache.h"

#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"

FLElabUseCache::FLElabUseCache(NUNetRefFactory* factory, bool edge) :
  mNetRefFactory(factory), mEdge(edge), mUDCache(new UDCache),
  mEmpty(new NUNetRefSet(factory))
{}

FLElabUseCache::~FLElabUseCache()
{
  for (LoopMap<UDCache> l(*mUDCache); !l.atEnd(); ++l) {
    CacheData* data = l.getValue();
    delete data;
  }
  delete mUDCache;
  delete mEmpty;
}

const NUNetRefSet*
FLElabUseCache::getMatchingUses(FLNodeElab* fanoutElab,
                                const FLNodeElab* faninElab)
{
  // Check if we have allocated data for this before
  FLNode* flow = fanoutElab->getFLNode();
  CacheData* data = NULL;
  UDCache::iterator pos = mUDCache->find(flow);
  if (pos == mUDCache->end()) {
    // Not yet, allocate one and add it to the cache
    data = new CacheData(mNetRefFactory, flow, mEdge);
    mUDCache->insert(UDCache::value_type(flow, data));
  } else {
    data = pos->second;
  }

  // Ask the data to return a set for the given fanin. It will
  // populate the data if it wasn't already.
  STBranchNode* hier = fanoutElab->getHier();
  NUNetElab* faninNetElab = faninElab->getDefNet();
  const NUNetRefSet* netRefs = data->getUses(hier, faninNetElab, mNetRefFactory);
  if (netRefs == NULL) {
    return mEmpty;
  } else {
    return netRefs;
  }
}

FLElabUseCache::CacheData::CacheData(NUNetRefFactory* factory, FLNode* flow,
                                     bool edge)
{
  // Allocate the required sets/maps
  mUses = new NUNetRefSet(factory);
  mHierElabUses = new HierElabUses;

  // Get the use def we are going to process. It'd better not be NULL
  NUUseDefNode* useDef = flow->getUseDefNode();
  FLN_ASSERT(useDef != NULL, flow);
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  if (!edge && (useDef->getType() == eNUAlwaysBlock)) {
    // Get the block because we want the level fanin uses
    NU_ASSERT(always != NULL, useDef);
    useDef = always->getBlock();
  }

  // Get the uses for this node
  const NUNetRefHdl& netRef = flow->getDefNetRef();
  if (edge) {
    // Get the edge uses off the always block
    NU_ASSERT(always != NULL, useDef);
    always->getEdgeUses(mUses);

  } else if (useDef->useBlockingMethods()) {
    NUUseDefStmtNode* stmt = dynamic_cast<NUUseDefStmtNode*>(useDef);
    stmt->getBlockingUses(netRef, mUses);
    stmt->getNonBlockingUses(netRef, mUses);
  } else {
    useDef->getUses(netRef, mUses);
  }
} // FLElabUseCache::CacheData::CacheData

FLElabUseCache::CacheData::~CacheData()
{
  delete mUses;
  for (LoopMap<HierElabUses> hier_loop(*mHierElabUses); !hier_loop.atEnd(); ++hier_loop) {
    ElabUses* elabUses = hier_loop.getValue();
    for (LoopMap<ElabUses> use_loop(*elabUses); !use_loop.atEnd(); ++use_loop) {
      NUNetRefSet* netRefs = use_loop.getValue();
      delete netRefs;
    }
    delete elabUses;
  }
  delete mHierElabUses;
}

const NUNetRefSet*
FLElabUseCache::CacheData::getUses(STBranchNode* hier, NUNetElab* netElab,
                                   NUNetRefFactory* netRefFactory)
{
  // See if we have computed this hierarchy.  If not, compute now.
  HierElabUses::iterator hier_iter = mHierElabUses->find(hier);
  if (hier_iter == mHierElabUses->end()) {
    // Not yet done, create the elaborated data for this hierarchy
    ElabUses* elab_uses = new ElabUses;
    mHierElabUses->insertInit(hier, elab_uses);

    for (NUNetRefSet::iterator i = mUses->begin(); i != mUses->end(); ++i) {
      // Empty net refs may show up due to being driven by constants, weed them out.
      const NUNetRefHdl& ref = *i;
      if (!ref->empty()) {
        NUNet* net = ref->getNet();
        NUNetElab* useNetElab = net->lookupElab(hier);
        addElabNetRef(elab_uses, useNetElab, ref, netRefFactory);
      }
    }
  }

  // Lookup this hierarchy's use set.
  hier_iter = mHierElabUses->find(hier);
  ST_ASSERT(hier_iter != mHierElabUses->end(), hier);
  ElabUses* elabUses = hier_iter->second;

  // Return the references for the fanin net elab. This may be none
  // before aliasing.
  ElabUses::iterator pos = elabUses->find(netElab);
  if (pos != elabUses->end()) {
    return pos->second;
  } else {
    return NULL;
  }
} // FLElabUseCache::CacheData::getUses

void
FLElabUseCache::CacheData::addElabNetRef(ElabUses* elabUses,
                                         NUNetElab* netElab, 
                                         const NUNetRefHdl& netRef,
                                         NUNetRefFactory* factory)
{
  // Check if we already added this net elab
  NUNetRefSet* netRefs = NULL;
  ElabUses::iterator pos = elabUses->find(netElab);
  if (pos == elabUses->end()) {
    netRefs = new NUNetRefSet(factory);
    elabUses->insert(ElabUses::value_type(netElab, netRefs));
  } else {
    netRefs = pos->second;
  }

  // Add this net ref use
  netRefs->insert(netRef);
}

