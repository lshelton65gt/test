// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "flow/FLFactory.h"
#include "flow/FLIter.h"
#include "flow/FLNode.h"
#include "flow/FLElabUseCache.h"
#include "flow/FLNodeElab.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCycle.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUUseDefNode.h"
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STBranchNode.h"
#include "util/CarbonAssert.h"
#include "util/CbuildMsgContext.h"
#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "util/UtOrder.h"
#include <cstddef>

/*!
  \file
  Implementation of FLNodeElab and FLNodeBoundElab classes.
*/

FLNodeElab::FLNodeElab(FLNode *flnode,
		       NUNetElab *net,
		       STBranchNode *hier) :
  mFLNode(flnode),
  mNetElab(net),
  mHier(hier),
  mSignature(0),
  mScheduleDepth(0),
  mSchedulePass(0)

{
  mScheduleFlags.flags = 0;
  ASSERT(sizeof(mScheduleFlags.flags) == sizeof(mScheduleFlags));
  putIsAllocated(true);
}


NUUseDefNode* FLNodeElab::getUseDefNode() const
{
  return mFLNode->getUseDefNode();
}


const HierName* FLNodeElab::getName() const
{
  // If this node defines a net, return it's hierarchy
  if (mNetElab != NULL)
    return mNetElab->getSymNode();
  
  // Otherwise, return the hierarchy of the useDef node
  FLN_ELAB_ASSERT(mHier, this);
  return mHier;
}

bool FLNodeElab::hasNestedHier() const
{
  return ((!mNested.empty()) && mScheduleFlags.bits.mNestingHier);
}


bool FLNodeElab::hasNestedBlock() const
{
  return ((!mNested.empty()) && not mScheduleFlags.bits.mNestingHier);
}


FLNodeElab* FLNodeElab::getSingleNested() const
{
  FLNodeElabSet::const_iterator iter = mNested.begin();
  if (iter == mNested.end())
    return NULL;
  FLNodeElabSet::const_iterator next = iter;
  ++next;
  if (next != mNested.end())
    return NULL;
  return (*iter);
}

UInt32 FLNodeElab::numNestedFlows() const
{
  return mNested.size();
}

void FLNodeElab::addNestedHier(FLNodeElab *nested)
{
  FLN_ELAB_ASSERT(nested != this, this);
  mNested.insert(nested);
  mScheduleFlags.bits.mNestingHier = true;
}


void FLNodeElab::addNestedBlock(FLNodeElab *nested)
{
  FLN_ELAB_ASSERT(nested != this, this);
  mNested.insert(nested);
  mScheduleFlags.bits.mNestingHier = false;
}

void FLNodeElab::clearFanin()
{
  mFanin.clear();
}

void FLNodeElab::clearEdgeFanin()
{
  mEdgeFanin.clear();
}

void FLNodeElab::clearNestedBlock()
{
  mNested.clear();
}

#ifdef CDB
static FLNodeElab* sCheckFanout = NULL;
static FLNodeElab* sCheckFanin = NULL;
#endif

void FLNodeElab::connectFanin(FLNodeElab *fanin_node)
{
#ifdef CDB
  if ((sCheckFanout == this) && (sCheckFanin == fanin_node)) {
    UtIO::cout() << "adding fanin\n"; // stop here in debugger
  }    
#endif
  mFanin.insert(fanin_node);
}

void FLNodeElab::connectFaninFromLoop(FLNodeElabLoop & loop)
{
  for ( /*no initial*/; not loop.atEnd(); ++loop) {
    connectFanin(*loop);
  }
}


void FLNodeElab::connectEdgeFanin(FLNodeElab *fanin_node)
{
  mEdgeFanin.insert(fanin_node);
}


void FLNodeElab::connectEdgeFaninFromLoop(FLNodeElabLoop & loop)
{
  for ( /*no initial*/; not loop.atEnd(); ++loop) {
    connectEdgeFanin(*loop);
  }
}

void FLNodeElab::replaceFaninsWithList(const FLNodeElabList& drivers,
                                       const FLNodeElabList& new_drivers)
{
  bool level = false, edge = false;
  for (FLNodeElabList::const_iterator iter = drivers.begin();
       iter != drivers.end();
       ++iter)
  {
    FLNodeElabSet::iterator p;
    FLNodeElab* flow = *iter;
    if ((p = mFanin.find(flow)) != mFanin.end())
    {
      level = true;
      mFanin.erase(p);
    }
    if ((p = mEdgeFanin.find(flow)) != mEdgeFanin.end())
    {
      edge = true;
      mEdgeFanin.erase(p);
    }
  }

  // I would like to add this assert that we are doing useful work but
  // REAlias seems to call this routine multiple times on the same
  // elaborated flow. So this does not work. If we re-do REAlias,
  // maybe we can enable this assert
  // FLN_ELAB_ASSERT(level or edge, this);

  for (FLNodeElabList::const_iterator new_iter = new_drivers.begin();
       new_iter != new_drivers.end();
       ++new_iter) {
    FLNodeElab* newDriver = *new_iter;
    if (level) {
      connectFanin(newDriver);
    }
    if (edge) {
      connectEdgeFanin(newDriver);
    }
  }
} // void FLNodeElab::replaceFaninsWithLoop

void FLNodeElab::replaceFaninWithFanin(FLNodeElab* fanin)
{
  bool level = false;
  bool edge = false;
  FLNodeElabSetIter iter = mFanin.find(fanin);
  if (iter != mFanin.end())
  {
    mFanin.erase(iter);
    level = true;
  }
  iter = mEdgeFanin.find(fanin);
  if (iter != mEdgeFanin.end())
  {
    mEdgeFanin.erase(iter);
    edge = true;
  }

  for (FLNodeElabLoop l = fanin->loopFanin(); !l.atEnd(); ++l)
  {
    if (level)
      connectFanin(*l);
    if (edge)
      connectEdgeFanin(*l);
  }
}


void FLNodeElab::replaceDefNet(NUNetElab *old_net, NUNetElab *new_net)
{
  FLN_ELAB_ASSERT(mNetElab == old_net, this);
  mNetElab = new_net;
}

void FLNodeElab::replaceFLNode(FLNode * old_flow, FLNode * new_flow)
{
  FLN_ELAB_ASSERT(mFLNode == old_flow, this);
  mFLNode = new_flow;
}

bool FLNodeElab::hasLevelFanin() const
{
  return !mFanin.empty();
}

bool FLNodeElab::hasEdgeFanin() const
{
  return !mEdgeFanin.empty();
}

FLNodeElabLoop FLNodeElab::loopFanin()
{
  return FLNodeElabLoop::create(Loop<FLNodeElabSet>(mFanin));
}

FLNodeElabLoop FLNodeElab::loopEdgeFanin()
{
  return FLNodeElabLoop::create(Loop<FLNodeElabSet>(mEdgeFanin));
}

FLNodeElabLoop FLNodeElab::loopNested()
{
  return FLNodeElabLoop::create(Loop<FLNodeElabSet>(mNested));
}

FLNodeElab::AllFaninLoop FLNodeElab::loopAllFanin()
{
  return AllFaninLoop(loopFanin(), loopEdgeFanin());
}

FLNodeElabSet *FLNodeElab::getFaninForNet(NUNetElab *net) const
{
  FLNodeElabSet* new_set = new FLNodeElabSet();
  for (FLNodeElabSet::const_iterator iter = mFanin.begin();
       iter != mFanin.end(); ++iter)
  {
    FLNodeElab* fanin = *iter;
    if (fanin->getDefNet() == net) {
      new_set->insert(*iter);
    }
  }
  for (FLNodeElabSet::const_iterator iter = mEdgeFanin.begin();
       iter != mEdgeFanin.end(); ++iter)
  {
    FLNodeElab* edgeFanin = *iter;
    if (edgeFanin->getDefNet() == net) {
      new_set->insert(*iter);
    }
  }
  return new_set;
}


bool FLNodeElab::checkFaninOverlap(const FLNodeElab* fanin,
                                   NUNetRefFactory* netRefFactory,
                                   FLElabUseCache* cache)
{
  if (isEncapsulatedCycle())
    return true;  // don't try to check cycle fanin

  NUUseDefNode* useDef = getUseDefNode();
  if (!useDef)
    return false; // a bound node should not have any fanin

  // port connections can have uses in a different hierarchy -- don't
  // check their fanin
  if (useDef->isPortConn()) {
    return true;
  }

  // $extnet can have fanin to protected-observable nets even though they are not in its uses
  // it is also used for hierrefs in ways that are inconsistent with UD
  // $ostnet for a module instance can have fanin directly to port connections
  // (it has no nested port connection of its own)
  // $cfnet is being excluded on the basis of guilt-by-association
  NUNet* net = getDefNet()->getNet();
  if (net->isVirtualNet())
    return true;

  // Get the use for this flow and fanin. Note that this will get only
  // the uses that match the fanin elaborated net. If this code is run
  // before hierarchical aliasing with a port connection it will get
  // empty uses when we cross the hierarchy. That is ok, because it is
  // too hard to do the test.
  const NUNetRefSet* uses = cache->getMatchingUses(this, fanin);

  // Check for overlap
  NUNetRefHdl faninNetRef = fanin->getDefNetRef(netRefFactory);
  for (NUNetRefSet::const_iterator loop = uses->begin(); loop != uses->end(); ++loop) {
    const NUNetRefHdl& ref = *loop;
    if (ref->overlapsBits(*faninNetRef)) {
      return true;
    }
  }
  
  // if we get here we never found an overlapping use
  return false;
}


bool FLNodeElab::checkEdgeFaninOverlap(const FLNodeElab* fanin,
                                       NUNetRefFactory* netRefFactory,
                                       FLElabUseCache* cache)
{
  if (isEncapsulatedCycle())
    return true;  // don't try to check cycle fanin

  NUUseDefNode* useDef = getUseDefNode();
  if (!useDef)
    return false; // a bound node should not have any fanin

  // Note: In checkFaninOverlap, we blindly accept virtual nets as
  // fanin. Virtual nets cannot appear in edge expressions, so they
  // are not considered in our edge fanin check.

  //! Only always blocks have edge fanin
  NUType type = useDef->getType();
  if (type!=eNUAlwaysBlock) {
    return false;
  }

  // Get the use for this flow and fanin. Note that this will get only
  // the uses that match the fanin elaborated net. If this code is run
  // before hierarchical aliasing it will get empty uses when we cross
  // the hierarchy. That is ok, because it is too hard to do the test.
  const NUNetRefSet* uses = cache->getMatchingUses(this, fanin);

  // Check for overlap
  NUNetRefHdl faninNetRef = fanin->getDefNetRef(netRefFactory);
  for (NUNetRefSet::const_iterator loop = uses->begin(); loop != uses->end(); ++loop) {
    const NUNetRefHdl& ref = *loop;
    if (ref->overlapsBits(*faninNetRef)) {
      return true;
    }
  }
  
  // if we get here we never found an overlapping use
  return false;
}


FLNodeElabIter* FLNodeElab::makeFaninIter(FLIterFlags::TraversalT visit,
					  FLIterFlags::TraversalT stop,
					  FLIterFlags::NestingT nesting,
					  bool visit_start,
					  FLIterFlags::IterationT iteration)
{
  return new FLNodeElabIter(this,
			    visit,
			    stop,
			    nesting,
			    visit_start,
			    iteration);
}


bool FLNodeElab::isBoundNode() const
{
  return (mFLNode && mFLNode->isBoundNode());
}

FLTypeT FLNodeElab::getType() const
{
  return mFLNode->getType();
}

void FLNodeElab::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  NUBase::indent(numspaces);

  UtIO::cout() << typeStr() << "(" << this << ") : NUNetElab(" << mNetElab
       << ")  BranchNode(" << mHier ;
  if (!mNested.empty()) {
    UtIO::cout() << ")  nested " << typeStr() << "(";
    bool isFirst = true;
    for (FLNodeElabSet::const_iterator iter = mNested.begin();
         iter != mNested.end(); ++iter)
    {
      FLNodeElab* nested = *iter;
      if (!isFirst)
        UtIO::cout() << ", ";
      UtIO::cout() << nested;
      isFirst = false;
    }
  }
  UtIO::cout() << ")  FLNode(" << mFLNode << ")" << UtIO::endl;

  if (mNetElab) {
    mNetElab->print(false, numspaces+2);
  }

  if (recurse) {
    if (mFLNode) {
      mFLNode->print(false, numspaces+2);
    }
    if (not mFanin.empty())
    {
      NUBase::indent(numspaces);
      UtIO::cout() << "fanin(" << UtIO::endl;
      for (FLNodeElabSet::const_iterator iter = mFanin.begin(); iter != mFanin.end(); ++iter)
      {
        FLNodeElab* fanin = *iter;
        fanin->print(false, numspaces+2);
      }
      NUBase::indent(numspaces);
      UtIO::cout() << ")" << UtIO::endl;
    }
    if (not mEdgeFanin.empty())
    {
      NUBase::indent(numspaces);
      UtIO::cout() << "edgefanin(" << UtIO::endl;
      for (FLNodeElabSet::const_iterator iter = mEdgeFanin.begin(); iter != mEdgeFanin.end(); ++iter)
      {
        FLNodeElab* edgeFanin = *iter;
        edgeFanin->print(false, numspaces+2);
      }
      NUBase::indent(numspaces);
      UtIO::cout() << ")" << UtIO::endl;
    }
    if (!mNested.empty()) {
      NUBase::indent(numspaces);
      UtIO::cout() << "nested(" << UtIO::endl;
      for (FLNodeElabSet::const_iterator iter = mNested.begin(); iter != mNested.end(); ++iter)
      {
        FLNodeElab* nested = *iter;
        nested->print(false, numspaces+2);
      }
      NUBase::indent(numspaces);
      UtIO::cout() << ")" << UtIO::endl;
    }
  }
}


void FLNodeElab::ppPrint(int numspaces) const
{
  NUBase::indent(numspaces);
  UtIO::cout() << "(flowelab " << this << " (netelab " << mNetElab
       << ") (net " << mNetElab->getNet() << ") (flow " << mFLNode
       << ") (usedef " << mFLNode->getUseDefNode() << ")";
  if (!mNested.empty()) {
    UtIO::cout() << " (nested ";
    bool isFirst = true;
    for (FLNodeElabSet::const_iterator iter = mNested.begin(); iter != mNested.end(); ++iter) {
      FLNodeElab* nested = *iter;
      if (!isFirst)
        UtIO::cout() << ", ";
      UtIO::cout() << nested;
      isFirst = false;
    }
    UtIO::cout() << ")";
  }
  UtIO::cout() << ")" << UtIO::endl;
}


FLNodeBoundElab::FLNodeBoundElab(FLNode *flnode,
				 NUNetElab *net,
				 STBranchNode *hier) :
  FLNodeElab(flnode, net, hier)
{
}


NUUseDefNode* FLNodeBoundElab::getUseDefNode() const
{
  return 0;
}


void FLNodeBoundElab::clearFanin()
{
  // TBD For now, Not valid to do this.
  FLN_ELAB_ASSERT(0, this);
}


void FLNodeBoundElab::connectFanin(FLNodeElab * /* fanin_node -- unused */)
{
  // TBD For now, Not valid to ask this -- use the iterator
  // In the future, do this??
  FLN_ELAB_ASSERT(0, this);
}

void FLNodeBoundElab::connectEdgeFanin(FLNodeElab * /* fanin_node -- unused */)
{
  // TBD For now, Not valid to ask this -- use the iterator
  // In the future, do this??
  FLN_ELAB_ASSERT(0, this);
}


void FLNodeBoundElab::replaceFaninWithFanin(FLNodeElab* /* fanin -- unused */ )
{
  // TBD For now, Not valid to ask this -- use the iterator
  // In the future, do this??
  FLN_ELAB_ASSERT(0, this);
}


void FLNodeBoundElab::replaceFaninsWithLoop(const FLNodeElabList& /* drivers -- unused */,
					    NUNetElab::DriverLoop /* new_drivers -- unused */)
{
  // TBD For now, Not valid to ask this -- use the iterator
  // In the future, do this??
  FLN_ELAB_ASSERT(0, this);
}


// JDM 3/20/03 -- the replaceDefNet override below had the wrong
// signature (took NUNet*) and was not doing any overriding.  When I
// corrected the signature, langcov/undriven.v started asserting.
// 
#if 0
void FLNodeBoundElab::replaceDefNet(NUNetElab * /* old_net -- unused */,
				    NUNetElab * /* new_net -- unused */)
{
  // TBD For now, Not valid to ask this -- use the iterator
  // In the future, do this??
  FLN_ELAB_ASSERT(0, this);
}
#endif


void FLNodeBoundElab::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  NUBase::indent(numspaces);

  UtIO::cout() << typeStr() << "(" << this << ") : " << mNetElab->typeStr()
               << "(" << mNetElab << ")  BranchNode(" << mHier 
               << ")  FLNode(" << mFLNode << ")" << UtIO::endl;

  if (mNetElab) {
    mNetElab->print(false, numspaces+2);
  }
  if (recurse) {
    if (mFLNode) {
      mFLNode->print(false, numspaces+2);
    }
  }
}


void FLNodeBoundElab::ppPrint(int numspaces) const
{
  NUBase::indent(numspaces);
  UtIO::cout() << "(flowboundelab " << this << " (netelab " << mNetElab
       << ") (net " << mNetElab->getNet() << ")" << ")" << UtIO::endl;
}

const char* FLNodeElab::typeStr() const
{
  return "FLNodeElab";
}

const char* FLNodeBoundElab::typeStr() const
{
  return "FLNodeBoundElab";
}

void FLNodeElab::dump(FLNodeElabFactory* factory, bool stopAtState,
                      FILE* outfile)
  const
{
  NodeElabSet covered;
  if (outfile == NULL)
    outfile = stdout;
  UtOFileStream cout(outfile);
  dumpHelper(&covered, 0, eSensLevelOrEdge, factory, stopAtState, cout);
}

void FLNodeElab::compose(UtString* buf ,const STBranchNode* scope, int indent, bool recurse)
  const
{
  UtString tmp;
  NUUseDefNode* driver = getUseDefNode();
  
  if ( mNetElab && mNetElab->getNet()->getLoc().isTicProtected() ) {
    buf->append(indent, ' ');
    NUBase::composeProtectedNUObject(mNetElab->getNet()->getLoc(), buf);
    return;
  } else if ( driver && driver->getLoc().isTicProtected() ) {
    buf->append(indent, ' ');
    NUBase::composeProtectedNUObject(driver->getLoc(), buf);
    return;
  }
  
  if (mNetElab != NULL)
  {
    buf->append(indent, ' ');
    mNetElab->compose(&tmp, scope, true ); // include root in name
    *buf << "DefNet: " << tmp << '\n';
  }
  if (driver != NULL)
  {
    if (driver->isCycle())
    {
      buf->append(indent, ' ');
      *buf << "Cycle #" << getCycle()->getID() << '\n';
    }
    else if (driver != NULL)
    {
      tmp.clear();
      getHier()->compose(&tmp);
      buf->append(indent, ' ');
      *buf << "Scope: " << tmp << '\n';
      if (recurse)
        driver->compose(buf, scope, indent, recurse);
      else
      {
        tmp.clear();
        driver->getLoc().compose(&tmp);
        buf->append(indent, ' ');
        *buf << "Location: " << tmp << '\n';
        buf->append(indent, ' ');
        *buf << driver->typeStr() << '\n';
      }
    }
  }
} // void FLNodeElab::compose

void FLNodeElab::pname(int indent_arg, bool newline)
  const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 40, 0);
  NUBase::indent(numspaces);
  UtString buf;
  if (mNetElab != NULL)
  {
    mNetElab->compose(&buf, NULL);
    UtIO::cout() << buf;
    buf.clear();
  }
  NUUseDefNode* driver = getUseDefNode();
  if (driver != NULL)
  {
    if (driver->isCycle())
      UtIO::cout() << " Cycle #" << getCycle()->getID();
    else if (driver != NULL)
    {
      UtIO::cout() << " ";
      getHier()->compose(&buf);
      UtString locStr;
      driver->getLoc().compose(&locStr);
      UtIO::cout() << buf << ':' << driver->typeStr() << " @" << locStr;
    }
  }
  if (newline)
    UtIO::cout() << UtIO::endl;
} // void FLNodeElab::pname

void FLNodeElab::dumpHelper(NodeElabSet* covered, int depth,
                            Sensitivity sense, FLNodeElabFactory* factory,
                            bool stopAtState,
                            UtOStream& cout)
  const
{
  bool isCovered = covered->find(this) != covered->end();
  covered->insert(this);
  pname(depth, false);          // rjc_todo  why are we using a debug routine here?
  cout << " ";
  if (sense == eSensLevel)
    cout << "(level)";
  else if (sense == eSensEdge)
    cout << "(edge)";
  cout << '\t' << typeStr();
  CFanin f(factory, this, eFlowIntoCycles);
  if (isCovered && !f.atEnd())
    cout << '\t' << "***covered***"; // don't re-explore hierarchy
  cout << UtIO::endl;
  ++depth;
  if (!isCovered)
  {
    //dumpCycle(depth);
    for (; !f.atEnd(); ++f)
    {
      FLNodeElab* fanin = *f;
      NUUseDefNode* node = fanin->getUseDefNode();
      if ((depth == 1) || (node == NULL) || !stopAtState ||
          node->isSequential())
        fanin->dumpHelper(covered, depth, f.getSensitivity(), factory,
                          stopAtState, cout);
    }
  }
} // void FLNodeElab::dumpHelper

void FLNodeElab::removeFanin(FLNodeElab* driver)
{
  FLNodeElabSet::iterator p;
  bool found = false;
  
  if ((p = mFanin.find(driver)) != mFanin.end()) {
    found = true;
    mFanin.erase(p);
  }

  if ((p = mEdgeFanin.find(driver)) != mEdgeFanin.end()) {
    found = true;
    mEdgeFanin.erase(p);
  }

  if (not found) {
    FLN_ELAB_ASSERT("Tried to remove nonexistent fanin."==0, this);
  }
}

void FLNodeElab::removeLevelFanin(FLNodeElab* driver)
{
  FLNodeElabSet::iterator p;
  if ((p = mFanin.find(driver)) != mFanin.end()) {
    mFanin.erase(p);
  } else {
    FLN_ELAB_ASSERT("Tried to remove nonexistent level fanin."==0, this);
  }
}

void FLNodeElab::removeEdgeFanin(FLNodeElab* driver)
{
  FLNodeElabSet::iterator p;
  if ((p = mEdgeFanin.find(driver)) != mEdgeFanin.end()) {
    mEdgeFanin.erase(p);
  } else {
    FLN_ELAB_ASSERT("Tried to remove nonexistent edge fanin."==0, this);
  }
}

//! is this node an encapsuled cycle?
bool FLNodeElab::isEncapsulatedCycle()
  const
{
  return false;
}

int FLNodeElab::compare(const FLNodeElab* f1, const FLNodeElab* f2)
{
  // Check for simple case
  if (f1 == f2)
    return 0;

  // Sort by use def locator. Note that If one of the UD pointers is
  // NULL we should return a non-zero comparison because otherwise we
  // use the net as a tie breaker which can cause a circular
  // comparison between three flow nodes (two with real UD pointers
  // and one without).
  NUUseDefNode* ud1 = f1->getUseDefNode();
  NUUseDefNode* ud2 = f2->getUseDefNode();
  int cmp = 0;
  if ((ud1 != NULL) && (ud2 != NULL))
    cmp = SourceLocator::compare(ud1->getLoc(), ud2->getLoc());
  else if (ud1 != NULL)
    cmp = -1;
  else if (ud2 != NULL)
    cmp = 1;

  // Sort by net elab. We do this because we could have always blocks
  // that get broken up and have the same locator. This results in
  // non-canonical data. But if the net elab is different, we can sort
  // by that first.
  //
  // Compare the hierarchy of the NetElab prior to comparing
  // nets. This ensures that two nets with the same name but different
  // scope are not seen as equivalent (see
  // beacon-func/testcases/statemnt/FOR16).
  if (cmp == 0) {
    NUNetElab* ne1 = f1->getDefNet();
    NUNetElab* ne2 = f2->getDefNet();
    if (ne1 != ne2) {
      if (ne1 == NULL)
        cmp = -1;
      else if (ne2 == NULL)
        cmp = 1;
      else {
        cmp = HierName::compare(ne1->getHier(), ne2->getHier());
        if (cmp == 0) {
          NUNet* n1 = ne1->getNet();
          NUNet* n2 = ne2->getNet();
          cmp = NUNet::compare(n1, n2);
        }
      }
    }
  } // if

  // Sort by hierarchy  (perhaps this should come before we compare the NUNetElabs)
  if (cmp == 0) {
    cmp = HierName::compare(f1->getHier(), f2->getHier());
  }

  // if still identical then try to compare netRefs since the net and
  // hierarchy match.   Sort so that foo[9:0] < foo[20:10]
  if (cmp == 0) {
    FLNode* flow1 = f1->getFLNode();
    FLNode* flow2 = f2->getFLNode();
    if (flow1 && flow2) {
      NUNetRefHdl ne1 = flow1->getDefNetRef();
      NUNetRefHdl ne2 = flow2->getDefNetRef();
      cmp = NUNetRef::compare(*ne1, *ne2, true);
    }
  }

  // Sort by UD, this is somewhat redundant but it does additional
  // comparisons
  if (cmp == 0) {
    cmp = NUUseDefNode::compare(ud1, ud2);
  }

  // The three possible cases where we have the same use def and
  // hierarchy are:
  //   1. They are cycles in which case both of them have null
  //      use defs (and null nets) 
  //   2. They are the same flow block for a use def but with
  //      different nets
  //   3. They are both primary inputs

  // It would be nice to handle case #1 by getting the normal flow node
  // and doing a comparison on it, but there is currently no way to do this
  // without going through the FLNodeElabFactory...

  if (cmp == 0) { // This handles case #2.
    FLNode* flow1 = f1->getFLNode();
    FLNode* flow2 = f2->getFLNode();
    if (flow1 && flow2)
      cmp = FLNode::compare(flow1,flow2);
    else if (!flow1 && flow2)
      cmp = -1;
    else if (flow1 && !flow2)
      cmp = +1;
  }

  // two fairly identical cycle nodes may be created in CycleDetection,
  // but be sensitive to the order in which they are created, rather
  // than their pointers
  if (cmp == 0) {
    const FLNodeElabCycle* c1 = dynamic_cast<const FLNodeElabCycle*>(f1);
    const FLNodeElabCycle* c2 = dynamic_cast<const FLNodeElabCycle*>(f2);
    if (c1 != NULL) {
      if (c2 == NULL) {
        cmp = 1;
      }
      else if (c1->getCycleIndex() < c2->getCycleIndex()) {
        cmp = -1;
      }
      else if (c1->getCycleIndex() > c2->getCycleIndex()) {
        cmp = 1;
      }
    }
    else if (c2 != NULL) {
      cmp = -1;
    }
  }

  // We have run out of ideas, just compare the pointers
  if (cmp == 0) {
    cmp = carbonPtrCompare<FLNodeElab>(f1,f2);
  }

#if 0
  // This code is useful if you suspect that the comparison is inconsistent
  // (meaning that compare(f1,f2) and compare(f2,f1) might be giving the
  // same answer other than 0).  To use it, uncomment it, run the testcase
  // and grep for the FLNodeElab::compare() lines.  Pipe that through:
  //   cut -d' ' -f2- | tsort 2> tsort.errors
  // If there are cycles (meaning the order was not consistent), tsort will
  // print the pointers involved to stderr and you can use them to set
  // breakpoints in the debugger.
  if (cmp != 0)
  {
    UtIO::cout() << "FLNodeElab::compare(): ";
    if (cmp < 0)
      UtIO::cout() << f1 << " " << f2 << "\n";
    else
      UtIO::cout() << f2 << " " << f1 << "\n";
    UtIO::cout().flush();
  }
#endif

  // Two elaborated flows should never be equal if they are not the
  // same pointer. I am adding this assert here in case someone
  // changes the above and removes the pointer comparison.
  FLN_ELAB_ASSERT(cmp != 0, f1);
  return cmp;
} // static UIntPtr FLNodeElab::compare

NUCycle* FLNodeElab::getCycle() {
  return NULL;
}

const NUCycle* FLNodeElab::getCycle() const {
  return NULL;
}

//! constructor
FaninInternal::FaninInternal(FLNodeElab* flow, unsigned int additionalFlags,
                             unsigned int nestingFlags, bool sorted,
                             FLNodeElabFactory* factory)
  : mFactory(factory)
{
  FLIterFlags::TraversalT t = (FLIterFlags::TraversalT)
    (FLIterFlags::eState |
     FLIterFlags::eCombDriver |
     FLIterFlags::ePrimaryPorts |
     FLIterFlags::eBound |
     additionalFlags);

  // Make an iterator to traverse the fanin set
  FLNodeElabIter* iter;
  iter = flow->makeFaninIter(t, t,
                             nestingFlags | FLIterFlags::eIntoNestedHier);
  iter->copy();

  // Go through the list and add it to our set
  for (; !iter->atEnd(); ++(*iter))
  {
    FLFlowSensePair flowSensePair(**iter, iter->getSensitivity());
    mFaninNodes.push_back(flowSensePair);
  }

  // Sort them and get the first entry
  if (sorted)
    std::sort(mFaninNodes.begin(), mFaninNodes.end());
  mIter = mFaninNodes.begin();

  // Done with the iterator
  iter->free();
} // FaninInternal::FaninInternal

FLNodeElab* FaninInternal::operator*() const {
  FLNodeElab* flow = mIter->getFlow();
  if (mFactory != NULL)
    flow = mFactory->getAcyclicNode(flow);
  return flow;
}

bool FLNodeElab::isFanin(FLNodeElab* fanin, Sensitivity sense)
  const
{
  if ((sense != eSensEdge) && (mFanin.find(fanin) != mFanin.end()))
    return true;
  return (sense != eSensLevel) && (mEdgeFanin.find(fanin) != mEdgeFanin.end());
}
    
bool FLNodeElab::isNested(FLNodeElab* flow, bool recurse) const
{
  if (mNested.find(flow) != mNested.end())
    return true;
  if (recurse)
  {
    for (FLNodeElabSet::const_iterator iter = mNested.begin(); iter != mNested.end(); ++iter)
    {
      const FLNodeElab* nested = *iter;
      if (nested->isNested(flow,true))
        return true;
    }
  }
  return false;

}
    
NUNetRefHdl FLNodeElab::getDefNetRef(NUNetRefFactory* factory) const
{
  NUNetRefHdl localNetRef = mFLNode->getDefNetRef();
  NUNet* localNet = localNetRef->getNet();
  NUNet* elabNet = mNetElab->getNet();
  if (localNet == elabNet) {
    return localNetRef;         // nets match, so NUNetRef will match
  }
  return factory->createNetRefImage(elabNet, localNetRef);
}

void FLNodeElab::putDepth(int depth)
{
  FLN_ELAB_ASSERT(depth < (1 << ((8*sizeof(mScheduleDepth)) - 1)), this);
  mScheduleDepth = depth;
}

void FLNodeElab::putSchedulePass(UInt32 pass)
{
  FLN_ELAB_ASSERT(pass < (1 << (8*sizeof(mSchedulePass))), this);
  mSchedulePass = pass;
}

void
FLNodeElab::printAssertInfo(const char* file, int line, const char* expStr)
  const
{
  // Print all the info we need about an object in order for the user to
  // find the problem.  We'd like to give human readable data if possible,
  // which is different depending on the type of object
  UtIO::cerr() << "\n\n******CARBON INTERNAL ERROR*******\n\n";
  if (this == NULL) {
    UtIO::cerr() << "(null object)\n";

  } else {
    // Print the hierarchy for this elaborated flow
    STBranchNode* hier = getHier();
    if (hier == NULL) {
      UtIO::cerr() << "(null hierarchy)\n";
    } else {
      UtString str;
      hier->composeHelper(&str, true, true, ".", false);
      UtIO::cerr() << "Logic Hierarchy: " << str << "\n";
    }

    // Gather the elaborated and unelaborated net
    NUNetElab* netElab = getDefNet();
    NUNet* net = NULL;
    hier = NULL;
    if (netElab == NULL) {
      UtIO::cerr() << "(null elaborated net)\n";
    } else {
      // Get the net
      net = netElab->getNet();
      if (net == NULL) {
        UtIO::cerr() << "(null net)\n";
      }

      // Get the elaborated net hierarchy
      hier = netElab->getHier();
      if (hier == NULL) {
        UtIO::cerr() << "(null elaborated net hierarchy)\n";
      }
    }

    // Print the elaborated net and reference if it is available
    if ((netElab != NULL) && (net != NULL) && (hier != NULL)) {
      // Compose the elaborated net and print it
      UtString str;
      net->compose(&str, hier);
      UtIO::cerr() << "NetElab: " << str << "\n";
    }

    // Print the unelaborated net ref so we have additonal detail. It
    // would have been nice to print the above elaborated net with a
    // reference but the problem is FLNodeElab::getDefNetRef needs a
    // net ref factory which might be dangerous. Also the function has
    // asserts in it that can fire.
    FLNode* flow = getFLNode();
    NUUseDefNode* useDef = NULL;
    if (flow == NULL) {
      UtIO::cerr() << "(null unelaborated flow)\n";
    } else {
      NUNetRefHdl netRef = flow->getDefNetRef();
      UtString str;
      netRef->compose(&str, NULL, false, false);
      UtIO::cerr() << "UnelabNet: " << str << "\n";

      // Get the use def node for further processing. If the flow is
      // NULL it doesn't exist.
      useDef = flow->getUseDefNode();
    }

    // Now finally print the use def node if it is available.
    if (useDef != NULL) {
      useDef->print(false, 0);
      UtIO::cout().flush();
    }
  } // } else

  UtIO::cerr() << file << ':' << line << " FLN_ELAB_ASSERT(" << expStr 
               << ") failed\n";
  UtIO::cerr().flush();
  // print stack trace ???
  abort();
} // FLNodeElab::printAssertInfo

STBranchNode * FLNodeElab::getHierarchyHelper(NUNetElab* net)
{
  NUUseDefNode* node = getUseDefNode();
  FLN_ELAB_ASSERT(node, this);

  // Get the correct hierarchy for the use lookup below. If this is a
  // port connection the hierarchy depends on the port direction. It
  // is the flow's hierarchy otherwise.
  STBranchNode* hier = getHier();
  if (node->isPortConn()) {
    // For bid ports where the input net matches the formal we have to
    // use this elaborated flows, def net's hierarchy. For all other
    // we need to use the fanin nets hierarchy. (Not sure why)
    NUPortConnection* conn = dynamic_cast<NUPortConnection*>(node);
    FLN_ELAB_ASSERT(conn != NULL, this);
    if (conn->isPortConnBid() && (conn->getFormal() == net->getNet())) {
      // Use the def nets hierarchy
      hier = getDefNet()->getHier();

    } else {
      // Use the drivers hierarchy
      hier = net->getHier();
    }
  }
  return hier;
} // STBranchNode* FLNodeElab::getHierarchyHelper


void FLNodeElab::getUsesHelper(NUNetRefSet* uses)
{
  NUUseDefNode* node = getUseDefNode();
  FLN_ELAB_ASSERT(node, this);

  // This seems harder to use than I would have hoped, but NUUseDefStmtNode
  // asserts on getUses, but its base class NUUseDefNode has no method
  // getBlockingUses without knowing which unelaborated net we are looking
  // for.
  NUNetRefHdl def = getFLNode()->getDefNetRef();
  if (node->useBlockingMethods()) {
    NUUseDefStmtNode* udsn = dynamic_cast<NUUseDefStmtNode*>(node);
    FLN_ELAB_ASSERT(udsn, this);
    udsn->getBlockingUses(def, uses);
  }
  else {
    node->getUses(def, uses);       // works for continuous assigns
  }

} // STBranchNode* FLNodeElab::getUsesHelper

NUNetRefHdl FLNodeElab::findUsedBits(NUNetRefFactory* netRefFactory,
                                     NUNetElab* net)
{
  STBranchNode* hier = getHierarchyHelper(net);

  NUNetRefSet uses(netRefFactory);
  getUsesHelper(&uses);

  NUNetRefHdl mergedUses = netRefFactory->createEmptyNetRef();
  for (NUNetRefSet::iterator p = uses.begin(), e = uses.end(); p != e; ++p) {
    NUNetRefHdl use = *p;
    if (!use->empty()) {
      NUNet* localUse = use->getNet();
      NUNetElab* elabUse = localUse->lookupElab(hier);
      if (elabUse == net) {  
        NUNetRefHdl netUses = netRefFactory->createNetRefImage(net->getNet(),
                                                               use);
        mergedUses = netRefFactory->merge(mergedUses, netUses);
      }
    }
  }
  return mergedUses;
}

bool 
FLNodeElab::hasBitFanin(FLNodeElab* fanin, FLElabUseCache* cache)
{
  // Get the use for this flow and fanin. Note that this will get only
  // the uses that match the fanin elaborated net. If this code is run
  // before hierarchical aliasing with a port connection it will get
  // empty uses when we cross the hierarchy. That is ok, because it is
  // too hard to do the test.
  const NUNetRefSet* uses = cache->getMatchingUses(this, fanin);

  // This routine uses data produced by getElabBitFanin which does not
  // work on port connections. So make sure we aren't called on a port
  // connection.
  NUUseDefNode* useDef = getUseDefNode();
  FLN_ELAB_ASSERT((useDef != NULL) && !useDef->isPortConn(), this);

  // Check for overlap
  NUNetRefHdl faninNetRef = fanin->getFLNode()->getDefNetRef();
  for (NUNetRefSet::const_iterator loop = uses->begin(); loop != uses->end(); ++loop) {
    const NUNetRefHdl& ref = *loop;
    if (ref->overlapsBits(*faninNetRef)) {
      return true;
    }
  }
  return false;
}

void MsgContext::composeLocation(FLNodeElab* flow, UtString* location)
{
  if (flow == NULL) {
    *location << "(null)";
    return;
  }

  FLNode* localFlow = flow->getFLNode();
  NUUseDefNode* useDef = localFlow ? localFlow->getUseDefNode() : 0;
  NUNetElab* defNet = flow->getDefNet();
  NUNet* localNet = localFlow ? localFlow->getDefNet() : flow->getDefNet()->getNet();

  if (useDef != NULL) {
    const SourceLocator& loc = useDef->getLoc();
    UtString source;
    loc.compose(&source);
    *location << source;
  }
  else {
    const SourceLocator& loc = localNet->getLoc();
    UtString source;
    loc.compose(&source);
    *location << source;
  }

  UtString canonicalNetName, drivingNetName, bits;

  if (localFlow != NULL) {
    NUNetRefHdl ref = localFlow->getDefNetRef();
    bool denormalizeVectors = true;  // end-user message printing
    ref->printBits(&bits, 0, denormalizeVectors);
  }

  STAliasedLeafNode* cname = NUScope::findBestAlias(defNet->getSymNode());
  cname->compose(&canonicalNetName);
  canonicalNetName << bits;

  // If the local net is a hierarchical reference, get the resolution
  // for the reference based on its scope
  STBranchNode* hier = flow->getHier();
  NUHierRef* href = localNet->getHierRef();
  if (href != NULL) {
    const AtomArray& path = href->getPath();
    NUModule* mod = localNet->getScope()->getModule();
    NUDesign* design = mod->getDesign();
    STSymbolTable* symtab = design->getSymbolTable();
    STSymbolTableNode* resolved =
      NUHierRef::resolveHierRef(path, 0, symtab, hier);
    FLN_ASSERT(resolved, flow);
    resolved->compose(&drivingNetName);
  }

  // if heir==null that means this is a cycle, and we should probably
  // consider printing something interesting about that...
  else if (hier != NULL) {
    // Compose a local net name with the hierarchy from the flow-node,
    // and if its different from the original net name, include them
    // both (e.g.    foo.v:45 Net top.flop.q[0] (top.out[0]): ....)
    STAliasedLeafNode* name = NUScope::findBestAlias(localNet->getNameLeaf());
    hier->compose(&drivingNetName);
    drivingNetName << ".";
    name->compose(&drivingNetName);
    drivingNetName << bits;
  }

  if (drivingNetName.empty()) {
    *location << " Net " << canonicalNetName;
  }
  else {
    *location << " Net " << drivingNetName;
    if (drivingNetName != canonicalNetName) {
      *location << " (" << canonicalNetName << ")";
    }
  }

}


// Spare bit to allow FLNodeElabFactory to detect whether a flow-node
// is currently allocated or not
void FLNodeElab::putIsAllocated(bool flag) {
  mScheduleFlags.bits.mIsAllocated = flag;
  // ? FLOW_ELAB_ALLOCATED: FLOW_ELAB_FREED;
}
