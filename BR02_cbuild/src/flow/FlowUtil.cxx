// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "flow/FlowUtil.h"

NUNetNode::NUNetNode(const NUNetRefHdl& net_ref, 
                     NUUseDefNode * node, 
                     bool marker) :
  mNetRef(net_ref), 
  mNode(node), 
  mMarker(marker) 
{
}


bool NUNetNode::operator<(const NUNetNode &other) const 
{
  if (mMarker == other.mMarker) {
    // compare netrefs only if nodes are equivalent
    if ( mNode == other.mNode ) {
      return (*mNetRef) < (*other.mNetRef);
    } else {
      return mNode < other.mNode;
    }
  } else {
    return mMarker < other.mMarker;
  }
}


FlowNetHier::FlowNetHier(FLNode * flow, 
                         NUNetElab * net, 
                         STBranchNode * hier) :
  mFlow(flow), 
  mNet(net), 
  mHier(hier) 
{
}


bool FlowNetHier::operator<(const FlowNetHier &other) const 
{
  // compare precedence: flow, net, hier
  if ( mFlow == other.mFlow ) {
    if ( mNet == other.mNet ) {
      // compare nodes only if nets are equivalent
      return mHier < other.mHier;
    } else {
      return mNet < other.mNet;
    }
  } else {
    return mFlow < other.mFlow;
  }
}
