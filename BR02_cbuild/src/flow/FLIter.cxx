// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "flow/Flow.h"
#include "flow/FLIter.h"
#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "util/UtHashSet.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUUseDefNode.h"
#include "util/CarbonAssert.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUPortConnection.h"
#include "symtab/STSymbolTable.h"

/*!
  \file
  Implementation of FLIterTemplate class.
*/

void gFlowIterElementStopHere()
{
}

/*!
 * Local helper function to clear a stack.
 */
template<class FlowNode>
void helperClearStack(UtStack<FlowIterElement<FlowNode> > *s)
{
  while (not s->empty()) {
    s->pop();
  }
}

//! Get the unelaborated def net for an unelaborated flow node
NUNet *helperGetDefNet(const FLNode* node)
{
  return node->getDefNet();
}

//! Get the unelaborated def net for an elaborated flow node
NUNet *helperGetDefNet(const FLNodeElab* node)
{
  return node->getDefNet()->getNet();
}

/*!
 * Local helper function to return the traveral flags for this node.
 */
template<class FlowNode, class FlowNodeBound>
FLIterFlags::TraversalT helperNodeGetFlags(const FlowNode *node)
{
  FLIterFlags::TraversalT ret = FLIterFlags::TraversalT(0);

  if (node->isEncapsulatedCycle())
    ret = FLIterFlags::TraversalT(ret | FLIterFlags::eCombDriver);

  if (!node->hasNestedBlock() && !node->hasNestedHier())
    ret = FLIterFlags::TraversalT(ret | FLIterFlags::eLeafNode);

  if (node->getDefNet() == NULL)
    return ret;
  bool primary = node->getDefNet()->atTopLevel();

  NUNet* def_net = helperGetDefNet(node);

  if (def_net->isInput()) {
    ret = FLIterFlags::TraversalT(ret | FLIterFlags::eLocalInput);
    if (primary) {
      ret = FLIterFlags::TraversalT(ret | FLIterFlags::ePrimaryInput);
    }
  }
  if (def_net->isOutput()) {
    ret = FLIterFlags::TraversalT(ret | FLIterFlags::eLocalOutput);
    if (primary) {
      ret = FLIterFlags::TraversalT(ret | FLIterFlags::ePrimaryOutput);
    }
  }
  if (def_net->isBid()) {
    ret = FLIterFlags::TraversalT(ret | FLIterFlags::eLocalBid);
    if (primary) {
      ret = FLIterFlags::TraversalT(ret | FLIterFlags::ePrimaryBid);
    }
  }
  if (def_net->isControlFlowNet()) {
    ret = FLIterFlags::TraversalT(ret | FLIterFlags::eControlFlow);
  }

  const FlowNodeBound *test = dynamic_cast<const FlowNodeBound*>(node);
  if (test) {
    ret = FLIterFlags::TraversalT(ret | FLIterFlags::eBound);
  }
  if (node->hasNestedHier()) {
    ret = FLIterFlags::TraversalT(ret | FLIterFlags::eHasNestedHier);
  }
  if (node->hasNestedBlock()) {
    ret = FLIterFlags::TraversalT(ret | FLIterFlags::eHasNestedBlock);
  }

  NUUseDefNode* useDefNode = node->getUseDefNode();
  if (useDefNode)
  {
    if (useDefNode->isSequential())
      ret = FLIterFlags::TraversalT(ret | FLIterFlags::eState);
    else if (not (ret & FLIterFlags::eBound))
    {
      if ((dynamic_cast<NUModuleInstance*>(useDefNode) == 0) &&
          (dynamic_cast<NUPortConnection*>(useDefNode) == 0))
        ret = FLIterFlags::TraversalT(ret | FLIterFlags::eCombDriver);
    }
  }
  return ret;
}


/*!
 * Local helper function to determine if a given node matches the
 * passed-in traversal flags.
 */
template<class FlowNode, class FlowNodeBound>
bool helperNodeMatchesFlags(const FlowNode *node,
				     FLIterFlags::TraversalT flags)
{
  if (flags & FLIterFlags::eNone) {
    return false;
  }
  if (flags & FLIterFlags::eAll) {
    return true;
  }

  return ((helperNodeGetFlags<FlowNode,FlowNodeBound>(node) & flags) != 0);
}


template<class FlowNode, class FlowNodeBound>
FLIterTemplate<FlowNode,FlowNodeBound>::
FLIterTemplate(FlowNode *node,
	       FLIterFlags::TraversalT visit,
	       FLIterFlags::TraversalT stop,
	       FLIterFlags::NestingT nesting,
	       bool visit_start,
	       FLIterFlags::IterationT iteration) :
  mIteration(iteration),
  mStartNode(node),
  mVisitFlags(visit),
  mStopFlags(stop),
  mNestingFlags(nesting),
  mVisitStart(visit_start),
  mForceStop(false)
{
  begin();
}


template<class FlowNode, class FlowNodeBound>
FLIterTemplate<FlowNode,FlowNodeBound>::
FLIterTemplate(FLIterFlags::TraversalT visit,
	       FLIterFlags::TraversalT stop,
	       FLIterFlags::NestingT nesting,
	       bool visit_start,
	       FLIterFlags::IterationT iteration) :
  mIteration(iteration),
  mStartNode(0),
  mVisitFlags(visit),
  mStopFlags(stop),
  mNestingFlags(nesting),
  mVisitStart(visit_start),
  mForceStop(false)
{
}


template<class FlowNode, class FlowNodeBound>
  Sensitivity FLIterTemplate<FlowNode,FlowNodeBound>::getInitialSensitivity()
  const
{
  Sensitivity level = eSensLevelOrEdge;
  if ((mVisitFlags & FLIterFlags::eEdgesOnly) != 0)
    level = eSensEdge;
  else if ((mVisitFlags & FLIterFlags::eLevelsOnly) != 0)
    level = eSensLevel;
  return level;
}

template<class FlowNode, class FlowNodeBound>
  void FLIterTemplate<FlowNode,FlowNodeBound>::begin()
{
  helperClearStack<FlowNode>(&mStack);
  mHaveSeenSet.clear();
  mHaveSeenPairSet.clear();
  if (not mVisitStart) {
    pushNextFan(mStartNode, this->getInitialSensitivity());
  } else {
    scheduleVisit(mStartNode, NULL, this->getInitialSensitivity(), false);
  }
  advanceToVisit();
}


template<class FlowNode, class FlowNodeBound>
  void FLIterTemplate<FlowNode,FlowNodeBound>::pushNextFan(FlowNode *node,
                                                           Sensitivity sense)
{
  // Handle bound nodes, they are a hard stop on the traversal.
  if (node->isBoundNode())
    return;

  // Step into nesting, if requested
  bool step = isStepIntoNode(node);
  bool branch = isBranchAtNode(node);
  if (step or branch) {
    for (Iter<FlowNode*> loop = node->loopNested(); !loop.atEnd(); ++loop)
      scheduleVisit(*loop, node, sense, true);      
    if (not branch)
      return;
  }

  // Step into fanin, if requested
  if ((mVisitFlags & FLIterFlags::eEdgesOnly) == 0)
  {
    for (Iter<FlowNode*> loop = node->loopFanin(); !loop.atEnd(); ++loop)
      scheduleVisit(*loop, node, eSensLevel, false);
  }

  if ((mVisitFlags & FLIterFlags::eLevelsOnly) == 0)
  {
    for (Iter<FlowNode*> loop = node->loopEdgeFanin(); !loop.atEnd(); ++loop)
      scheduleVisit(*loop, node, eSensEdge, false);
  }
}

template<class FlowNode, class FlowNodeBound>
void FLIterTemplate<FlowNode,FlowNodeBound>::scheduleVisit(FlowNode* node,
                                                           FlowNode* fanout,
                                                           Sensitivity sense,
                                                           bool isNestedRelationship)
{
  if (doIterate(node, fanout, sense, isNestedRelationship)) {
    mStack.push(FlowIterElement<FlowNode>(node, fanout, sense, isNestedRelationship));
    rememberIterate(mStack.top());
  }
}

template<class FlowNode, class FlowNodeBound>
  void FLIterTemplate<FlowNode,FlowNodeBound>::operator++()
{
  ASSERT(not atEnd());

  FlowIterElement<FlowNode> e = mStack.top();
  mStack.pop();
  if (not mForceStop and not isStopNode(e.mNode)) {
    pushNextFan(e.mNode, e.mSensitivity);
  }

  mForceStop = false;
  advanceToVisit();
}


template<class FlowNode, class FlowNodeBound>
  void FLIterTemplate<FlowNode,FlowNodeBound>::advanceToVisit()
{
  while (true) {

    if (mStack.empty()) {
      break;
    }

    FlowIterElement<FlowNode> e = mStack.top();

    if (isVisitNode(e.mNode)) {
      break;
    }

    mStack.pop();

    // Not a visit node, but may want to push its fanin if it is not a stopper
    // Duplicating some code from next() here, wanna avoid recursion, in case
    // have a large fanin.
    if (not isStopNode(e.mNode)) {
      pushNextFan(e.mNode, e.mSensitivity);
    }
  }
}


template<class FlowNode, class FlowNodeBound>
  bool FLIterTemplate<FlowNode,FlowNodeBound>::atEnd() const
{
  return mStack.empty();
}


template<class FlowNode, class FlowNodeBound>
  FlowNode* FLIterTemplate<FlowNode,FlowNodeBound>::operator*() const
{
  ASSERT(not mStack.empty());
  return mStack.top().mNode;
}


template<class FlowNode, class FlowNodeBound>
  FlowNode* FLIterTemplate<FlowNode,FlowNodeBound>::getCur() const
{
  ASSERT(not mStack.empty());
  return mStack.top().mNode;
}


template<class FlowNode, class FlowNodeBound>
  FlowNode* FLIterTemplate<FlowNode,FlowNodeBound>::getCurParent() const
{
  ASSERT(not mStack.empty());
  return mStack.top().mFanout;
}

template<class FlowNode, class FlowNodeBound>
  Sensitivity FLIterTemplate<FlowNode,FlowNodeBound>::getSensitivity()
  const
{
  ASSERT(not mStack.empty());
  return mStack.top().mSensitivity;
}

template<class FlowNode, class FlowNodeBound>
bool FLIterTemplate<FlowNode,FlowNodeBound>::isNestedRelationship() const
{
  ASSERT(not mStack.empty());
  return mStack.top().mIsNestedRelationship;
}

template<class FlowNode, class FlowNodeBound>
  void FLIterTemplate<FlowNode,FlowNodeBound>::doNotExpand()
{
  mForceStop = true;
}


template<class FlowNode, class FlowNodeBound>
  FLIterFlags::TraversalT FLIterTemplate<FlowNode,FlowNodeBound>::getCurFlags() const
{
  return helperNodeGetFlags<FlowNode,FlowNodeBound>(getCur());
}


template<class FlowNode, class FlowNodeBound>
  bool FLIterTemplate<FlowNode,FlowNodeBound>::isStopNode(FlowNode* node)
{
  return helperNodeMatchesFlags<FlowNode,FlowNodeBound>(node, mStopFlags);
}


template<class FlowNode, class FlowNodeBound>
  bool FLIterTemplate<FlowNode,FlowNodeBound>::isVisitNode(FlowNode* node)
{
  return helperNodeMatchesFlags<FlowNode,FlowNodeBound>(node, mVisitFlags);
}


template<class FlowNode, class FlowNodeBound>
  bool FLIterTemplate<FlowNode,FlowNodeBound>::isStepIntoNode(FlowNode *node)
{
  bool step = false;
  if (mNestingFlags & FLIterFlags::eIntoNestedHier) {
    step |= helperNodeMatchesFlags<FlowNode,FlowNodeBound>(node,
                                                           FLIterFlags::eHasNestedHier);
  }
  if (mNestingFlags & FLIterFlags::eIntoNestedBlock) {
    step |= helperNodeMatchesFlags<FlowNode,FlowNodeBound>(node,
                                                           FLIterFlags::eHasNestedBlock);
  }  
  return step;
}


template<class FlowNode, class FlowNodeBound>
  bool FLIterTemplate<FlowNode,FlowNodeBound>::isBranchAtNode(FlowNode *node)
{
  bool branch = false;
  if (mNestingFlags & FLIterFlags::eBranchAtNestedHier) {
    branch |= helperNodeMatchesFlags<FlowNode,FlowNodeBound>(node,
                                                             FLIterFlags::eHasNestedHier);
  }
  if (mNestingFlags & FLIterFlags::eBranchAtNestedBlock) {
    branch |= helperNodeMatchesFlags<FlowNode,FlowNodeBound>(node,
                                                             FLIterFlags::eHasNestedBlock);
  }
  return branch;
}


template<class FlowNode, class FlowNodeBound>
bool FLIterTemplate<FlowNode,FlowNodeBound>::doIterate(FlowNode *node,
                                                       FlowNode *fanout,
                                                       Sensitivity sense,
                                                       bool isNested)
{
  switch (mIteration) {
  case FLIterFlags::eIterNodeOnce:
    return mHaveSeenSet.find(node) == mHaveSeenSet.end();
    break;

  case FLIterFlags::eIterFaninPairOnce:
    return (mHaveSeenPairSet.find(FlowIterElement<FlowNode>(node,fanout,sense,
                                                             isNested)) ==
            mHaveSeenPairSet.end());
    break;

  default:
    break;
  }
  return true;
}


template<class FlowNode, class FlowNodeBound>
void FLIterTemplate<FlowNode,FlowNodeBound>::rememberIterate(const FlowIterElement<FlowNode>& e)
{
  switch (mIteration) {
  case FLIterFlags::eIterNodeOnce:
    mHaveSeenSet.insert(e.mNode);
    break;

  case FLIterFlags::eIterFaninPairOnce:
    mHaveSeenPairSet.insert(e);
    break;

  default:
    break;
  }
}


template<class FlowNode, class FlowNodeBound>
  FLIterTemplate<FlowNode,FlowNodeBound>::~FLIterTemplate()
{
}


template<class FlowNode, class FlowNodeBound>
FLSetIterTemplate<FlowNode,FlowNodeBound>::
FLSetIterTemplate(FlowNodeSet *flow_set,
		  FLIterFlags::TraversalT visit,
		  FLIterFlags::TraversalT stop,
		  FLIterFlags::NestingT nesting,
		  bool visit_start,
		  FLIterFlags::IterationT iteration) :
  FLIterTemplate<FlowNode,FlowNodeBound>(visit, stop, nesting, visit_start, iteration),
  mFlowSet(*flow_set)
{
  begin();
}


template<class FlowNode, class FlowNodeBound>
FLSetIterTemplate<FlowNode,FlowNodeBound>::
FLSetIterTemplate(FLIterFlags::TraversalT visit,
		  FLIterFlags::TraversalT stop,
		  FLIterFlags::NestingT nesting,
		  bool visit_start,
		  FLIterFlags::IterationT iteration) :
  FLIterTemplate<FlowNode,FlowNodeBound>(visit, stop, nesting, visit_start, iteration)
{
}


template<class FlowNode, class FlowNodeBound>
FLSetIterTemplate<FlowNode,FlowNodeBound>::~FLSetIterTemplate()
{
}


template<class FlowNode, class FlowNodeBound>
void FLSetIterTemplate<FlowNode,FlowNodeBound>::begin()
{
  helperClearStack<FlowNode>(&this->mStack);
  this->mHaveSeenSet.clear();
  this->mHaveSeenPairSet.clear();

  for (typename FlowNodeSet::iterator iter = mFlowSet.begin();
       iter != mFlowSet.end();
       ++iter) {
    FlowNode * flow = (*iter);
    if (not this->mVisitStart) {
      pushNextFan(flow, this->getInitialSensitivity());
    } else {
      scheduleVisit(flow, NULL, this->getInitialSensitivity(), false);
    }
  }

  this->advanceToVisit();
}


template<class FlowNode, class FlowNodeBound>
void FLSetIterTemplate<FlowNode,FlowNodeBound>::
setFlowSet(FlowNodeSet *flow_set)
{
  mFlowSet.clear();
  mFlowSet.insert(flow_set->begin(),flow_set->end());
}


template<class Net, class FlowNode, class FlowNodeBound>
FLNetIterTemplate<Net,FlowNode,FlowNodeBound>::
FLNetIterTemplate(UtList<Net*> *net_list,
		  FLIterFlags::TraversalT visit,
		  FLIterFlags::TraversalT stop,
		  FLIterFlags::NestingT nesting,
		  FLIterFlags::IterationT iteration) :
  FLIterTemplate<FlowNode,FlowNodeBound>(visit, stop, nesting, true, iteration),
  mNetList(*net_list)
{
  begin();
}


template<class Net, class FlowNode, class FlowNodeBound>
FLNetIterTemplate<Net,FlowNode,FlowNodeBound>::
FLNetIterTemplate(Net* net,
		  FLIterFlags::TraversalT visit,
		  FLIterFlags::TraversalT stop,
		  FLIterFlags::NestingT nesting,
		  FLIterFlags::IterationT iteration) :
  FLIterTemplate<FlowNode,FlowNodeBound>(visit, stop, nesting, true, iteration)
{
  mNetList.push_back(net);
  begin();
}


template<class Net, class FlowNode, class FlowNodeBound>
FLNetIterTemplate<Net,FlowNode,FlowNodeBound>::
FLNetIterTemplate(FLIterFlags::TraversalT visit,
		  FLIterFlags::TraversalT stop,
		  FLIterFlags::NestingT nesting,
		  FLIterFlags::IterationT iteration) :
  FLIterTemplate<FlowNode,FlowNodeBound>(visit, stop, nesting, true, iteration)
{
}


template<class Net, class FlowNode, class FlowNodeBound>
FLNetIterTemplate<Net,FlowNode,FlowNodeBound>::~FLNetIterTemplate()
{
}


template<class Net, class FlowNode, class FlowNodeBound>
void FLNetIterTemplate<Net,FlowNode,FlowNodeBound>::begin()
{
  helperClearStack<FlowNode>(&this->mStack);
  this->mHaveSeenSet.clear();
  this->mHaveSeenPairSet.clear();

  for (typename UtList<Net*>::iterator net_iter = mNetList.begin();
       net_iter != mNetList.end();
       ++net_iter)
  {
    Net* net = *net_iter;
    for (typename Net::DriverLoop driver_iter = net->loopContinuousDrivers();
         !driver_iter.atEnd(); ++driver_iter)
    {
      FlowNode* flow = *driver_iter;
      scheduleVisit(flow, NULL, this->getInitialSensitivity(), false);
    }
  }

  this->advanceToVisit();
}


template<class Net, class FlowNode, class FlowNodeBound>
void FLNetIterTemplate<Net,FlowNode,FlowNodeBound>::
setNetList(UtList<Net*> *net_list)
{
  mNetList.clear();
  mNetList.insert(mNetList.begin(), net_list->begin(), net_list->end());
}


template <typename T> 
static void sPushNets(T iter, NUNetElabSet* net_elab_list,
                      STSymbolTable* symtab)
{
  for (; !iter.atEnd(); ++iter)
  {
    NUNet *net = *iter;
    STBranchNode *hier = net->getScope()->getModule()->lookupElab(symtab)->getHier();
    NUNetElab *net_elab = net->lookupElab(hier);
    net_elab_list->insert(net_elab);
  }
}

FLDesignElabIter::FLDesignElabIter(NUDesign *design,
				   STSymbolTable *symtab,
				   FLIterFlags::StartT start,
				   FLIterFlags::TraversalT visit,
				   FLIterFlags::TraversalT stop,
				   FLIterFlags::NestingT nesting,
				   FLIterFlags::IterationT iteration) :
  FLNetIterTemplate<NUNetElab,FLNodeElab,FLNodeBoundElab>(visit, stop, nesting, iteration)
{
  NUNetElabSet net_elab_set;
  sPushNets(design->loopOutputPorts(), &net_elab_set, symtab);
  sPushNets(design->loopBidPorts(), &net_elab_set, symtab);
  if ((start & FLIterFlags::eModuleLocals) != 0)
  {
    sPushNets(design->loopLocals(), &net_elab_set, symtab);
  }
  if ((start & FLIterFlags::eAllElaboratedNets) != 0) 
  {
    for (STSymbolTable::NodeLoop i = symtab->getNodeLoop();
         not i.atEnd(); 
         ++i) {
      STSymbolTableNode* node = *i;
      STAliasedLeafNode* leaf = node->castLeaf();
      if (leaf) {
        NUNetElab * net_elab = NUNetElab::find(leaf);
	if (net_elab) {
          net_elab_set.insertWithCheck(net_elab);
        }        
      }
    }    
  }
  for (NUDesign::NameSetLoop p = design->loopObservable(); !p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    NUNetElab *net = NUNetElab::find(node);
    if (net != NULL)
      net_elab_set.insertWithCheck(net);
  }
  if ((start & FLIterFlags::eClockMasters) != 0)
  {
    for (NUDesign::NameSetLoop p = design->loopClockMasters(); !p.atEnd(); ++p)
    {
      STSymbolTableNode* node = *p;
      NUNetElab *net = NUNetElab::find(node);
      if (net != NULL)
        net_elab_set.insert(net);
    }
  }
  mNetList.clear();
  mNetList.insert(mNetList.begin(), net_elab_set.begin(), net_elab_set.end());
  begin();
}


FLDesignElabIter::~FLDesignElabIter()
{
}

/*
 * Explicitly instantiate these classes; compiler won't do it for us since not
 * everything is implemented in the .h files.
 */
template class FLIterTemplate<FLNode, FLNodeBound>;
template class FLIterTemplate<FLNodeElab, FLNodeBoundElab>;
template class FLSetIterTemplate<FLNode, FLNodeBound>;
template class FLSetIterTemplate<FLNodeElab, FLNodeBoundElab>;
template class FLNetIterTemplate<NUNet, FLNode, FLNodeBound>;
template class FLNetIterTemplate<NUNetElab, FLNodeElab, FLNodeBoundElab>;
