// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "flow/FLFactory.h"
#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FLNodeElabCycle.h"
#include "util/CarbonAssert.h"
#include "util/MemManager.h"

/*!
  \file
  Flow factory class definitions.
*/


// Force instantiation of templatized factories.
template class FLFactoryTemplate<FLNode, FLNodeBound>;
template class FLFactoryTemplate<FLNodeElab, FLNodeBoundElab>;


template<class FlowNode, class FlowNodeBound>
FLFactoryTemplate<FlowNode,FlowNodeBound>::FLFactoryTemplate()
{
  // Make sure the 2 classes we manage are the same size, otherwise this
  // does not work.
  ASSERT(sizeof(FlowNode) == sizeof(FlowNodeBound));
}


template<class FlowNode, class FlowNodeBound>
FLFactoryTemplate<FlowNode,FlowNodeBound>::~FLFactoryTemplate()
{
}


template<class FlowNode, class FlowNodeBound>
void FLFactoryTemplate<FlowNode,FlowNodeBound>::destroy(FlowNode *flnode)
{
  mPool.free(flnode);
}

void FLNodeElabFactory::destroy(FLNodeElab *flnode)
{
  // destroy the nodes and remove them from the cycle maps
  FLNodeElabCycle* cycle = dynamic_cast<FLNodeElabCycle*>(flnode);
  if (cycle != NULL)
  {
    FLReverseCycleMap::iterator pos = mReverseCycleMap.find(cycle);
    FLN_ELAB_ASSERT(pos != mReverseCycleMap.end(), cycle);
    mReverseCycleMap.erase(pos);
    mCycleFactory.destroy(cycle);
  }
  else
  {
    FLCycleMap::iterator pos = mCycleMap.find(flnode);
    if (pos != mCycleMap.end())
      mCycleMap.erase(pos);
    mPool.free(flnode);
  }
}

FLNodeFactory::iterator FLNodeFactory::begin()
{
  return mPool.begin();
}

FLNodeFactory::iterator FLNodeFactory::end()
{
  return mPool.end();
}

template<class FlowNode, class FlowNodeBound>
void FLFactoryTemplate<FlowNode,FlowNodeBound>::getFreeSet(MemoryPoolPtrSet* free_ptrs)
{
  mPool.getFreeSet(free_ptrs);
}


template<class FlowNode, class FlowNodeBound>
void *FLFactoryTemplate<FlowNode,FlowNodeBound>::malloc()
{
  void* ptr = mPool.malloc();
  return ptr;
}


FLNodeFactory::FLNodeFactory()
{
}


FLNodeFactory::~FLNodeFactory()
{
}


FLNode *FLNodeFactory::create(const NUNetRefHdl &net_ref, NUUseDefNode *node)
{
  FLNode *flnode = new (malloc()) FLNode(net_ref, node, eFLNormal);
  return flnode;
}

FLNodeBound *FLNodeFactory::createBound(const NUNetRefHdl &net_ref, FLTypeT type)
{
  FLNodeBound *flnode = new (malloc()) FLNodeBound(net_ref, type);
  return flnode;
}

FLNodeFactory::FlowLoop FLNodeFactory::loopFlows() {
  return FlowLoop(mPool.begin(), mPool.end());
}

void FLNodeFactory::print() 
{
  for (FLNodeFactory::iterator p = this->begin(); p != this->end(); ++p) {
    FLNode *flnode = *p;
    flnode->print(true,0);
  }
}


FLNodeElabFactory::FLNodeElabFactory()
{
}


FLNodeElabFactory::~FLNodeElabFactory()
{
}


FLNodeElab *FLNodeElabFactory::create(FLNode *flnode,
                                      NUNetElab *net,
                                      STBranchNode *hier)
{
  FLNodeElab *flnodelab = new (malloc()) FLNodeElab(flnode, net, hier);
  return flnodelab;
}

/*! Create an FLNodeElabCycle for a flow node, associated with an NUCycle structure.
 *  Also maintains maps from cyclic to acyclic flow nodes and vice-versa.
 */
FLNodeElabCycle *FLNodeElabFactory::createCycle(NUCycle* node, FLNodeElab* flow,
                                                UInt32 idx)
{
  NUNetElab* net = flow->getDefNet();
  FLN_ELAB_ASSERT(net, flow);
  FLNodeElabCycle* cycleFlow =
    new (mCycleFactory.malloc()) FLNodeElabCycle(node, net, idx);

  // Add the flows to a map from the original flow node to its cycle flow node
  mCycleMap[flow] = cycleFlow;

  // Create a reverse map so we can go from the encapsulated cycle to
  // the original flow node.
  FLN_ELAB_ASSERT(!flow->isEncapsulatedCycle(), flow);
  mReverseCycleMap[cycleFlow] = flow;

  return cycleFlow;
}


FLNodeBoundElab *FLNodeElabFactory::createBound(FLNode *flnode,
                                                NUNetElab *net,
                                                STBranchNode *hier)
{
  FLNodeBoundElab *flnodelab = new (malloc()) FLNodeBoundElab(flnode, net, hier);
  return flnodelab;
}

FLNodeElabFactory::FlowLoop FLNodeElabFactory::loopFlows() {
  NodeLoop nl = NodeLoop(mPool.begin(), mPool.end());
  CycleLoop cl = CycleLoop(mCycleFactory.mPool.begin(),
                           mCycleFactory.mPool.end());
  return FlowLoop(nl, cl);
}

FLNodeElabFactory::CycleLoop FLNodeElabFactory::loopCycleFlows() {
  return CycleLoop(mCycleFactory.mPool.begin(), mCycleFactory.mPool.end());
}

// True when the given flow node is a cyclic node within a known NUCycle
bool FLNodeElabFactory::isInCycle(FLNodeElab* flow) const
{
  return mCycleMap.find(flow) != mCycleMap.end();
}

/* Given a flow node, returns the equivalent flow node whose fanin can
 * traversed without encountering a cycle.  For an FLNodeElabCycle,
 * this is itself.  For an FLNodeElab, it is the node's associated
 * FLNodeElabCycle if the flow node is in a cycle, or it is just the
 * flow node itself if it is not in a cycle.
 */
FLNodeElab* FLNodeElabFactory::getAcyclicNode(FLNodeElab* flow) const
{
  FLCycleMap::const_iterator n = mCycleMap.find(flow);
  if (n == mCycleMap.end())
    return flow;
  else
    return n->second;
}

/* Given a flow node, returns the equivalent flow node whose fanin may
 * lead to a cycle.  For an FLNodeElab, this is itself.  For and
 * FLNodeElabCycle, this is the FLNodeElab it is associated with.
 */
FLNodeElab* FLNodeElabFactory::getCyclicNode(FLNodeElab* flow) const
{
  FLNodeElabCycle* cycleNode = dynamic_cast<FLNodeElabCycle*>(flow);
  if (cycleNode == NULL)
    return flow;
  FLReverseCycleMap::const_iterator n = mReverseCycleMap.find(cycleNode);
  FLN_ELAB_ASSERT(n != mReverseCycleMap.end(), flow);
  return n->second;
}

void FLNodeElabFactory::print() 
{
  FLNodeElab *flnode = 0;
  for (FLNodeElabFactory::FlowLoop p = this->loopFlows(); p(&flnode); ) {
    flnode->print(true,0);
  }
}
