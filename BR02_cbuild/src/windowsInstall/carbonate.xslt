<?xml version="1.0" encoding="utf-8"?>
<!--************************************************************************************
  Copyright (c) 2008-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*************************************************************************************-->

<!--
This is an XSLT transform that manipulates an automatically
generated .wxs file (a WiX source file) that knows about all the files
necessary for an installation into a new .wxs file with complete
knowledge of an installation.
-->

<xsl:transform version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:wix="http://schemas.microsoft.com/wix/2006/wi"
    xmlns="http://schemas.microsoft.com/wix/2006/wi"
    exclude-result-prefixes="wix">
  <xsl:output encoding="utf-8"/>
  <!-- ProductName must be specified by caller, e.g., "Carbon Model Studio 2008_04". -->
  <xsl:param name="ProductName"></xsl:param>
  <!-- Unfortunately we need some random guids, so they need to be passed in. -->
  <xsl:param name="Guid1"></xsl:param>
  <xsl:param name="Guid2"></xsl:param>
  <xsl:param name="Guid3"></xsl:param>

  <xsl:variable name="myguid" select="wix:Wix/wix:Fragment/wix:DirectoryRef[@Id='TARGETDIR']/wix:Directory/@Id" />

  <!-- This is the identity transformation.  It passes any elements that
       aren't overridden as-is. -->
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <!-- Add lots of Product children after Package element. -->
  <xsl:template match="wix:Module">
    <Product Id="{$Guid1}" Language="1033" Manufacturer="Carbon Design Systems" Name="{$ProductName}" UpgradeCode="{$Guid2}" Version="1.0.0.0">
      <Package Compressed="yes" InstallerVersion="300" />
     
      <Property Id="WIXUI_INSTALLDIR" Value="MYDIR"/>
      <!--
    Using CustomAction with Property rather than FileKey attribute for launching 
    LicenseWizard because FileKey fails when installing on top of a previous 
    installation (in same directory), because the file doesn't get installed
    (since it already exists).  Using Return="ignore" rather than Return=
    "asyncNoWait" because the latter causes the License Wizard to appear below
    other windows where it isn't noticed, at least some times.
    Use another CustomAction to define property referencing a Dirctory value.
    -->
      <CustomAction Id="SetLicenseWizard" Property="LicenseWizard" Value="[MYDIR]Win\bin\LicenseWizard.exe"/>
      <CustomAction Id="LaunchLicenseWizard" Property="LicenseWizard" ExeCommand="" Impersonate="yes" Return="ignore"/>
      <InstallUISequence>
        <Custom Action="SetLicenseWizard" After="CostFinalize"/>
      </InstallUISequence>
      <UI>
        <Publish Dialog="ExitDialog" Control="Finish" Order="1" Event="DoAction" Value="LaunchLicenseWizard">LAUNCHLICENSEWIZARD = 1 and NOT Installed</Publish>
      </UI>
      <!--
        Refuse to install if .NET Framework 2.0 isn't installed, because License Wizard uses it.
        This requires using "-ext WixNetFxExtension" on candle and light command lines.
    -->
      <PropertyRef Id="NETFRAMEWORK35"/>
      <Condition Message="The .NET Framework 3.5 or newer must be installed.  See http://www.microsoft.com/downloads/details.aspx?FamilyId=333325FD-AE52-4E35-B531-508D977D32A6">
        Installed OR NETFRAMEWORK35
      </Condition>
      <UIRef Id="Carbon_WixUI_InstallDir"/>
      <Directory Id="TARGETDIR" Name="SourceDir">
        <Directory Id="ProgramFilesFolder">
          <Directory Id="Carbon" Name="Carbon">
            <Directory Id="MYDIR" Name="{$ProductName}">
            </Directory>
          </Directory>
        </Directory>
        <Directory Id="ProgramMenuFolder">
          <!-- Using a Carbon shortcut folder is probably convenient for -->
          <!-- users who install multiple versions.  I'm still not sure if -->
          <!-- we should do this, since it's less convenient for users -->
          <!-- without multiple versions, and it goes against recommended -->
          <!-- practice. -->
          <Directory Id="CarbonShortcutFolder" Name="Carbon">
            <!-- This component exists only for the RemoveFolder element, -->
            <!-- needed to avoid ICE64. -->
            <Component Id="CarbonShortcutComponent" Guid="{$Guid1}">
              <RemoveFolder Id="CarbonShortcutFolder" On="uninstall"/>
              <!-- Adding bogus registry value with KeyPath=yes (rather than on File) to remove ICE38 -->
              <RegistryKey Root="HKCU" Key="Software\bogus\Uninstall">
                <RegistryValue Value="0" Type="string" KeyPath="yes"/>
              </RegistryKey>
            </Component>
            <Directory Id="ShortcutFolder" Name="{$ProductName}">
              <Component Id="ShortcutsComponent" Guid="{$Guid2}">
                <Shortcut Id="ModelStudioShortcut" Directory="ShortcutFolder" Name="Carbon Model Studio" Target="[MYDIR]Win\bin\modelstudio.exe"
                          Description="Launch Carbon Model Studio"/>
                <Shortcut Id="DocShortcut" Directory="ShortcutFolder" Name="Documentation" Target="[MYDIR]userdoc\index.html"
                          Description="Browse Carbon Model Studio documentation"/>
                <Shortcut Id="ExamplesShortcut" Directory="ShortcutFolder" Name="Examples" Target="[MYDIR]examples"
                          Description="Browse Carbon Model Studio examples"/>
                <Shortcut Id="LicenseWizardShortcut" Directory="ShortcutFolder" Name="License Wizard" Target="[MYDIR]Win\bin\LicenseWizard.exe"
                          Description="Launch License Wizard"/>
                <RemoveFolder Id="ShortcutFolder" On="uninstall"/>
                <!-- Adding bogus registry value with KeyPath=yes (rather than on File) to remove ICE43 -->
                <RegistryKey Root="HKCU" Key="Software\bogus\Uninstall">
                  <RegistryValue Value="0" Type="string" KeyPath="yes"/>
                </RegistryKey>
              </Component>
            </Directory>
            <!-- Set environment variables if user checked corresponding checkbox. -->
            <Component Id="EnvVarsComponent" Guid="{$Guid3}">
              <Condition>SETENVVARS</Condition>
              <!-- Add these to the front of the PATH environment variable. -->
              <Environment Id="UpdatePath" Name="PATH" Action="set" Part="first" Value="%CARBON_HOME%\bin;%CARBON_HOME%\Win\mono\bin;%CARBON_HOME%\Win\lib;%CARBON_HOME%\Win\lib\winx\shared"/>
              <Environment Id="UpdateCarbonHome" Name="CARBON_HOME" Action="set" Value="[MYDIR]"/>
              <Environment Id="UpdateCarbonArch" Name="CARBON_ARCH" Action="set" Value="Win"/>
              <!-- Adding bogus registry value with KeyPath=yes (rather than on File) to remove ICE43 -->
              <RegistryKey Root="HKCU" Key="Software\bogus\Uninstall">
                <RegistryValue Value="0" Type="string" KeyPath="yes"/>
              </RegistryKey>
            </Component>
          </Directory>
        </Directory>
      </Directory>

      <Feature Id="ProductFeature" Level="1" Title="MainFeature">
        <ComponentRef Id="CarbonShortcutComponent" />
        <ComponentRef Id="ShortcutsComponent" />
        <ComponentRef Id="EnvVarsComponent" />
        <xsl:for-each select="wix:ComponentRef">
          <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
          </xsl:copy>
        </xsl:for-each>
      </Feature>
      <Media Id="1" Cabinet="product.cab" EmbedCab="yes" />

      <!-- Include CRT80 -->
      <DirectoryRef Id="TARGETDIR">
        <Merge Id="VCRedist" SourceFile="c:\Program Files\Common Files\Merge Modules\Microsoft_VC80_CRT_x86.msm" DiskId="1" Language="0" />
      </DirectoryRef>
      <Feature Id="VCRedist" Title="Visual C++ 8.0 Runtime" AllowAdvertise="no" Display="hidden" Level="1">
        <MergeRef Id="VCRedist" />
      </Feature>

    </Product>
  </xsl:template>
  
  <xsl:template match="wix:Fragment/wix:DirectoryRef">
    <xsl:variable name="dirchild" select="wix:Directory/@Id" />
    <xsl:variable name="compid" select="wix:Component/@Id" />
    
    <xsl:if test="wix:Directory/@Id and @Id=$myguid">
      <DirectoryRef>
        <xsl:attribute name="Id">MYDIR</xsl:attribute>
        <xsl:apply-templates select="wix:Directory"/>
      </DirectoryRef>
    </xsl:if>


  
    <!--Handle all top level files here by forcing them
    to be underneath the toplevel MYDIR-->    
    <xsl:if test="@Id=$myguid and wix:Component/@Id">
      <DirectoryRef>
        <xsl:attribute name="Id">MYDIR</xsl:attribute>
        <Component>
          <xsl:attribute name="Id">
            <xsl:value-of select="wix:Component/@Id"/>
          </xsl:attribute>
          <xsl:attribute name="Guid">
            <xsl:value-of select="wix:Component/@Guid"/>
          </xsl:attribute>
          <File>
            <xsl:attribute name="Id">
              <xsl:value-of select="wix:Component/wix:File/@Id"/>
            </xsl:attribute>
            <xsl:attribute name="KeyPath">
              <xsl:value-of select="wix:Component/wix:File/@KeyPath"/>
            </xsl:attribute>
            <xsl:attribute name="Source">
              <xsl:value-of select="wix:Component/wix:File/@Source"/>
            </xsl:attribute>
          </File>
        </Component>
      </DirectoryRef>
     
      
    </xsl:if>

    <xsl:if test="@Id!=$myguid">
      <xsl:copy>
        <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
    </xsl:if>
  </xsl:template>
  
  <!-- Remove generated fragment for Directory element for the top-level -->
  <!-- directory (because we created it in the Package template above). -->
  <xsl:template match="wix:Fragment[wix:DirectoryRef/wix:Directory/@Name='CARBON_INSTALL']">
    <!-- Do nothing - remove this element -->
  </xsl:template>


 
</xsl:transform> 
