@echo off

Rem ***************************************************************************************
Rem   Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved. 
Rem  
Rem   THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
Rem   OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
Rem   DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
Rem   OF CARBON DESIGN SYSTEMS, INC.
Rem ***************************************************************************************

Rem createWindowsInstall.bat -- Create a .msi file for installing Carbon.

Rem This batch file takes an existing self-extracting EXE containing the
Rem files we ship as input and generates a .msi file suitable for shipping
Rem to customers as output.

Rem Usage: createWindowsInstall SELF-EXTRACTING-EXE-FILE OUTPUT-MSI-FILE PRODUCT-NAME SANDBOX SILICON_TOOLS
Rem Example: createWindowsInstall carbon-runtime-Windows-C2008_04_6168.exe ^
Rem             carbon-C2008_04_6168.msi "Carbon Model Studio 2008_04" ^
Rem             \\silicon\w\lr\sandbox ^
Rem             \\silicon\o\tools

Rem This batch file leaves a few files behind in the current
Rem directory, so you may want to run this in a temporary directory.

Rem The first parameter is the self-extracting .exe file.
set self-extracting-exe=%1
Rem The second parameter is the name of the output .msi file.
set msi-file=%2
Rem The third parameter is the name of the product as displayed to the user
Rem and used in the default directory name and shortcut folder name.  Use
Rem quotes.
set product-name=%3
Rem The fourth parameter is the sandbox, used to build the LicenseWizard.
set sandbox=%4
Rem The fifth parameter is the silicon tools area
set silicon=%5

Rem This is the directory into which we dump the files we're shipping.
set install_dir=c:\CARBON_INSTALL
Rem The depends on Wix being installed.  WiX is available from
Rem http://wix.sourceforge.net/releases/.
set wix="c:\Program Files\Windows Installer XML v3\bin"

Rem Create/clean and populate install_dir.
rmdir /s /q %install_dir%
mkdir %install_dir%
pushd %install_dir%
%self-extracting-exe%

Rem -------------------------------------------------------------------------
Rem Now build LicenseWizard.  It's unfortunate that this is intermingled with
Rem building the installer, but they're both things that need to be done on
Rem Windows (because we haven't bothered to get C# builds working on Linux).
"c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.com" ^
        /rebuild Release %sandbox%\src\LicenseWizard\LicenseWizard.sln
Rem Copy files just built.
copy %sandbox%\src\LicenseWizard\WizardControl\bin\Release\WizardControl.dll Win\bin
copy %sandbox%\src\LicenseWizard\LicenseWizard\bin\Release\LicenseWizard.exe Win\bin
popd
Rem Clean up the sandbox that we polluted by building.
rmdir /S/Q %sandbox%\src\LicenseWizard\LicenseWizard\bin
rmdir /S/Q %sandbox%\src\LicenseWizard\LicenseWizard\obj
rmdir /S/Q %sandbox%\src\LicenseWizard\WizardControl\bin
rmdir /S/Q %sandbox%\src\LicenseWizard\WizardControl\obj

Rem -------------------------------------------------------------------------
Rem Now we run WiX's "heat" tool, which creates a WiX .wxs input file
Rem containing information to create all of the files in our product.
Rem -gg generates guids, -sreg supporesses registry harvesting
Rem (because it fails unable to load a DLL), -wx treats warnings as errors.

:doheat
echo %wix%\heat dir %install_dir% -template:module -out heat.wxs -gg -sreg -suid

%wix%\heat dir %install_dir% -gg -sreg  -template:module -out heat.wxs

Rem -------------------------------------------------------------------------
Rem Next we transform the automatically generated heat.wxs using a hand-written
Rem XSLT transform.  But first we need to generate a guid to pass in.

Rem We generate a guid into a file, then set a variable to the contents of
Rem the file.
%silicon%\windows\bin\Uuidgen -c -oguid.txt
set /p guid1= <guid.txt
%silicon%\windows\bin\Uuidgen -c -oguid.txt
set /p guid2= <guid.txt
%silicon%\windows\bin\Uuidgen -c -oguid.txt
set /p guid3= <guid.txt

%silicon%\windows\bin\msxsl heat.wxs ^
	%sandbox%\src\windowsInstall\carbonate.xslt ^
	ProductName=%product-name% Guid1=%guid1% Guid2=%guid2% Guid3=%guid3% > carbon.wxs

Rem -------------------------------------------------------------------------
Rem Now we're done creating the input to WiX, so we compile (with Candle) 
Rem and link (with Light).

Rem Carbon_WixUI_InstallDir.wxs is a Carbon-modified version of WiX's
Rem WixUI_InstallDir.wxs, modified to skip the EULA screen.
%wix%\candle -ext WixNetFxExtension -wx ^
    %sandbox%\src\windowsInstall\Carbon_WixUI_InstallDir.wxs ^
    %sandbox%\src\windowsInstall\CarbonInstallDirDlg.wxs ^
    carbon.wxs

Rem -sf is "suppress files" to avoid ICE60 failure.
%wix%\light -b %install_dir% -wx -ext WixUIExtension -ext WixNetFxExtension ^
    -cultures:en-us ^
    -dWixUIBannerBmp=%sandbox%\src\windowsInstall\carbonLogo.bmp ^
    -dWixUIDialogBmp=%sandbox%\src\windowsInstall\WelcomeImage.bmp ^
    -sice:ICE47 -sice:ICE60^
    carbon.wixobj Carbon_WixUI_InstallDir.wixobj CarbonInstallDirDlg.wixobj ^
    -out %msi-file%

Rem We could clean up the install directory and files we created such as 
Rem heat.wxs, guid.txt, and the .wixobj files.
