// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/ShellNetRecord.h"
#include "util/UtConv.h"
#include "symtab/STSymbolTable.h"
#include "shell/CarbonMemFile.h"
#include "shell/ShellNetMacroDefine.h"
#include "hdl/ReadMemX.h"

static CarbonStatus sCalcIndexLength(int msb, int lsb, int range_msb, int range_lsb, size_t* index, size_t* length, CarbonModel* model)
{
  return CarbonUtil::calcIndexLength(msb, lsb, range_msb, range_lsb, index, length, model);
}

//! Templated function to update mask
/*!

  Some of the code to efficiently update the masked flag and the mask
  itself is common across all nets sizes and access types.  However,
  other parts are very different, i.e. different functions are called
  with different arguments.

  This function uses templates to implement all flavors.  Because all
  the variations are determined by template parameters, the compiler
  should be able to generate fast custom versions for each one.

  This was chosen over virtual functions because I didn't want to add
  the overhead of the additional function call.
 */
template <bool _SingleWord, bool _IsRange>
inline void sUpdateMask(ShellNetReplay::Touched* touchBuffer,
                        UInt32* maskBuf,
                        UInt32 numWords,
                        UInt32 wordIndex,
                        UInt32 tailMask,
                        SInt32 rangeIndex,
                        UInt32 rangeLength)
{
  // Deal with mask.  If the net has already been masked since the
  // last schedule call, all we need to do is update the mask bits.
  // If the net has been neither masked nor touched, we also need to
  // clear the previous mask bits (for performance reasons only the
  // masked flag, not the mask itself, is cleared on each schedule
  // call) and set the masked flag.  If the net has been touched but
  // not masked, do nothing because there has already been a full
  // deposit to the net, so all the bits in the value buffer are
  // valid.
  bool isTouched = touchBuffer->isSet();
  bool isMasked = touchBuffer->isSetMasked();
  bool clearMask = !isMasked && !isTouched;
  bool updateMask = isMasked || clearMask;
  if (clearMask) {
    // Clear the mask buffer and set the masked flag
    if (_SingleWord) {
      *maskBuf = 0;
    } else {
      CarbonValRW::setToZero(maskBuf, numWords);
    }
    touchBuffer->setMasked();
  }
  if (updateMask) {
    // Record what bits were deposited
    if (_IsRange) {
      CarbonValRW::setRangeToOnes(maskBuf, rangeIndex, rangeLength);
    } else {
      maskBuf[wordIndex] = 0xffffffff;
      maskBuf[numWords - 1] &= tailMask;
    }
  }
}

// Convenience functions for calling the templated function above

//! Updates the mask for depositRange to a small (<= 32 bit) net
inline void sUpdateMaskRangeSmall(ShellNetReplay::Touched* touchBuffer,
                                  UInt32* maskBuf,
                                  SInt32 rangeIndex,
                                  UInt32 rangeLength)
{
  sUpdateMask<true, true>(touchBuffer,
                          maskBuf,
                          0 /* numWords - unused */,
                          0 /* wordIndex - unused */,
                          0 /* tailMask - unused */,
                          rangeIndex,
                          rangeLength);
}

//! Updates the mask for depositRange to a large (> 32 bit) net
inline void sUpdateMaskRangeLarge(ShellNetReplay::Touched* touchBuffer,
                                  UInt32* maskBuf,
                                  UInt32 numWords,
                                  SInt32 rangeIndex,
                                  UInt32 rangeLength)
{
  sUpdateMask<false, true>(touchBuffer,
                           maskBuf,
                           numWords,
                           0 /* wordIndex - unused */,
                           0 /* tailMask - unused */,
                           rangeIndex,
                           rangeLength);
}

//! Updates the mask for depositWord to a large (> 32 bit) net
inline void sUpdateMaskWordLarge(ShellNetReplay::Touched* touchBuffer,
                                 UInt32* maskBuf,
                                 UInt32 numWords,
                                 UInt32 wordIndex,
                                 UInt32 tailMask)
{
  sUpdateMask<false, false>(touchBuffer,
                            maskBuf,
                            numWords,
                            wordIndex,
                            tailMask,
                            0 /* rangeIndex - unused */,
                            0 /* rangeLength - unused */);
}


ShellNetRecord::ShellNetRecord(ShellNet* subNet, Touched* touchBuffer)
  : ShellNetReplay(subNet),
    mNumWords(subNet->getNumUInt32s()),
    mTailMask(0), mTouched(touchBuffer),
    mExternalIndex(-1), mInternalIndex(-1)
{
  UInt32 bitWidth = subNet->getBitWidth();
  mTailMask = CarbonValRW::getWordMask(bitWidth);
  mValueScratch = CARBON_ALLOC_VEC(UInt32, mNumWords);
}

ShellNetRecord::~ShellNetRecord()
{
  CARBON_FREE_VEC(mValueScratch, UInt32, mNumWords);
}

const ShellNetRecord* ShellNetRecord::castShellNetRecord() const
{
  return this;
}

CarbonStatus ShellNetRecord::force(const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetRecord::forceWord(UInt32, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetRecord::forceRange(const UInt32*, 
                                        int, int, 
                                        CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetRecord::release(CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetRecord::releaseWord(int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetRecord::releaseRange(int, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void ShellNetRecord::setRawToUndriven(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

bool ShellNetRecord::setToUndriven(CarbonModel*)
{
  return false;
}

void ShellNetRecord::putToZero(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

void ShellNetRecord::putToOnes(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

CarbonStatus ShellNetRecord::setRange(int, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetRecord::clearRange(int, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void ShellNetRecord::runValueChangeCB(CarbonNetValueCBData*, 
                                      UInt32*, 
                                      UInt32*,
                                      CarbonTriValShadow*,
                                      CarbonModel*) const
{
  ST_ASSERT(0, getName());
}


/* ShellNetRecordTwoStateWord */

ShellNetRecordTwoStateWord::ShellNetRecordTwoStateWord(ShellNet* subNet, UInt32* valBuf, UInt32* maskBuf, Touched* touchBuffer)
  : ShellNetRecord(subNet, touchBuffer),
    mValBuf(valBuf),
    mMaskBuf(maskBuf),
    mOwnMaskBuf(false)
{
  // If a mask buffer was not provided, allocate one and record the
  // fact that we own its storage.
  if (mMaskBuf == NULL) {
    mMaskBuf = CARBON_ALLOC_VEC(UInt32, 1);
    mOwnMaskBuf = true;
  }
}

ShellNetRecordTwoStateWord::~ShellNetRecordTwoStateWord()
{
  if (mOwnMaskBuf) {
    CARBON_FREE_VEC(mMaskBuf, UInt32, 1);
  }
}

ShellNetRecord* ShellNetRecordTwoStateWord::cloneStorage(ShellNet* primNet)
{
  // If we allocated storage for the mask buffer, force the clone to
  // do the same.
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  ShellNetRecord* ret = new ShellNetRecordTwoStateWord(primNet, mValBuf, maskBuf, mTouched);
  ret->putExternalIndex(mExternalIndex);
  ret->putInternalIndex(mInternalIndex);
  return ret;
}

UInt32* ShellNetRecordTwoStateWord::getValueBuffer()
{
  return mValBuf;
}

UInt32* ShellNetRecordTwoStateWord::getDriveBuffer()
{
  return NULL;
}

void ShellNetRecordTwoStateWord::recordValue(const UInt32* buf)
{
  *mValBuf = *buf & mTailMask;
  setTouched();
}

void ShellNetRecordTwoStateWord::recordValueCheck(const UInt32* buf)
{
  if (buf)
    recordValue(buf);
}

CarbonStatus ShellNetRecordTwoStateWord::deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  recordValueCheck(buf);
  return mNet->deposit(buf, drive, model);
}

CarbonStatus ShellNetRecordTwoStateWord::depositWord(UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  // The index is not checked when depositing to a word. We just
  // ignore it. But stat could be error if we are depositing to a
  // drive on a non-tristate. So, for this case, just ignore the index
  // and record the deposit value.
  recordValueCheck(&buf);
  CarbonStatus stat = mNet->depositWord(buf, index, drive, model);
  return stat;
}

CarbonStatus ShellNetRecordTwoStateWord::depositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonStatus stat = mNet->depositRange(buf, range_msb, range_lsb, drive, model);
  // Only update record data if we actually deposited something
  if ((stat == eCarbon_OK) && (buf != NULL))
  {
    size_t index;
    size_t length;
    sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
    
    // Only update the valbuf with the value that was deposited to.
    mNet->examine(mValueScratch, NULL, eXDrive, model);
    CarbonValRW::cpSrcRangeToDestRange(mValBuf, index, mValueScratch, index, length);
    // Update mask
    sUpdateMaskRangeSmall(mTouched, mMaskBuf, index, length);
    // Update only the touched flag, not the mask
    setTouchedOnly();
  }
  return stat;
}

void ShellNetRecordTwoStateWord::doFastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  // buf cannot be null with fastDeposit
  recordValue(buf);
  mNet->fastDeposit(buf, drive, model);
}

void ShellNetRecordTwoStateWord::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  doFastDeposit(buf, drive, model);
}

void ShellNetRecordTwoStateWord::fastDepositWord(UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  recordValue(&buf);
  mNet->fastDepositWord(buf, index, drive, model);
}

void ShellNetRecordTwoStateWord::doFastDepositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  mNet->fastDepositRange(buf, range_msb, range_lsb, drive, model);
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  // Only update record data if we actually deposited something
  if ((stat == eCarbon_OK) && (buf != NULL))
  {
    // Only update the valbuf with the value that was deposited to.
    mNet->examine(mValueScratch, NULL, eXDrive, model);
    CarbonValRW::cpSrcRangeToDestRange(mValBuf, index, mValueScratch, index, length);
    // Update mask
    sUpdateMaskRangeSmall(mTouched, mMaskBuf, index, length);
    // Update only the touched flag, not the mask
    setTouchedOnly();
  }
}

void ShellNetRecordTwoStateWord::fastDepositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  doFastDepositRange(buf, range_msb, range_lsb, drive, model);
}

void ShellNetRecordTwoStateWord::putToZero(CarbonModel* model)
{
  UInt32 zero = 0;
  doFastDeposit(&zero, 0, model);
}

void ShellNetRecordTwoStateWord::putToOnes(CarbonModel* model)
{
  UInt32 ones = 0;
  ones = ~ones;
  doFastDeposit(&ones, 0, model);
}

CarbonStatus ShellNetRecordTwoStateWord::setRange(int range_msb, int range_lsb, CarbonModel* model)
{
  UInt32 ones = 0;
  ones = ~ones;
  doFastDepositRange(&ones, range_msb, range_lsb, 0, model);
  return eCarbon_OK;
}

CarbonStatus ShellNetRecordTwoStateWord::clearRange(int range_msb, int range_lsb, CarbonModel* model)
{
  UInt32 zero = 0;
  doFastDepositRange(&zero, range_msb, range_lsb, 0, model);
  return eCarbon_OK;
}

/* ShellNetRecordTwoStateClk */
ShellNetRecordTwoStateClk::ShellNetRecordTwoStateClk(ShellNet* subNet, UInt32* valBuf, UInt32* maskBuf, UInt32* valShadow, Touched* touchBuffer)
  : ShellNetRecordTwoStateWord(subNet, valBuf, maskBuf, touchBuffer),
    mValShadow(valShadow)
{}

ShellNetRecordTwoStateClk::~ShellNetRecordTwoStateClk()
{}

ShellNetRecord* ShellNetRecordTwoStateClk::cloneStorage(ShellNet* primNet)
{
  // If we allocated storage for the mask buffer, force the clone to
  // do the same.
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  ShellNetRecord* ret = new ShellNetRecordTwoStateClk(primNet, mValBuf, maskBuf, mValShadow, mTouched);
  ret->putExternalIndex(mExternalIndex);
  ret->putInternalIndex(mInternalIndex);
  return ret;
}

void ShellNetRecordTwoStateClk::saveCurrentValue()
{
  // prepare the shadow (for db write change detection) for a fake
  // edge
  /*
    for example,
    if we have a current value of 1 and a shadow value of 1 (post
    processChanges, and then a user deposits a 0 then a 1:
    
    value:   1  ->  0  ->  1
    shadow:  1  ->  1  ->  0
  */
  *mValShadow = *mValBuf;
}

CarbonStatus ShellNetRecordTwoStateClk::deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  saveCurrentValue();
  return ShellNetRecordTwoStateWord::deposit(buf, drive, model);
}

CarbonStatus ShellNetRecordTwoStateClk::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  saveCurrentValue();
  return ShellNetRecordTwoStateWord::depositWord(buf, index, drive, model);
}

CarbonStatus ShellNetRecordTwoStateClk::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  saveCurrentValue();
  return ShellNetRecordTwoStateWord::depositRange(buf, range_msb, range_lsb, drive, model);
}

void ShellNetRecordTwoStateClk::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  saveCurrentValue();
  ShellNetRecordTwoStateWord::fastDeposit(buf, drive, model);
}

void ShellNetRecordTwoStateClk::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  saveCurrentValue();
  ShellNetRecordTwoStateWord::fastDepositWord(buf, index, drive, model);
}

void ShellNetRecordTwoStateClk::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  saveCurrentValue();
  ShellNetRecordTwoStateWord::fastDepositRange(buf, range_msb, range_lsb, drive, model);
}

void ShellNetRecordTwoStateClk::putToZero(CarbonModel* model)
{
  saveCurrentValue();
  ShellNetRecordTwoStateWord::putToZero(model);
}

void ShellNetRecordTwoStateClk::putToOnes(CarbonModel* model)
{
  saveCurrentValue();
  ShellNetRecordTwoStateWord::putToOnes(model);
}

CarbonStatus ShellNetRecordTwoStateClk::setRange(int range_msb, int range_lsb, CarbonModel* model)
{
  saveCurrentValue();
  return ShellNetRecordTwoStateWord::setRange(range_msb, range_lsb, model);
}

CarbonStatus ShellNetRecordTwoStateClk::clearRange(int range_msb, int range_lsb, CarbonModel* model)
{
  saveCurrentValue();
  return ShellNetRecordTwoStateWord::clearRange(range_msb, range_lsb, model);
}

/* ShellNetRecordTwoStateClkStateOutput */

ShellNetRecordTwoStateClkStateOutput::ShellNetRecordTwoStateClkStateOutput(ShellNet* subNet, UInt32* valBuf, UInt32* maskBuf, Touched* touchBuffer)
  : ShellNetRecordTwoStateWord(subNet, NULL, maskBuf, touchBuffer),
    mEncodedValBuf(valBuf)
{
  // We used the record buffer pointer as encoded value buffer in the
  // initializer above.  Set mValBuf to point to our scratch buffer.
  // The parent class uses mValBuf for all value operations, so we can
  // encode it afterwards.
  mRawValBuf = 0;
  mValBuf = &mRawValBuf;
}

ShellNetRecordTwoStateClkStateOutput::~ShellNetRecordTwoStateClkStateOutput()
{}

ShellNetRecord* ShellNetRecordTwoStateClkStateOutput::cloneStorage(ShellNet* primNet)
{
  // If we allocated storage for the mask buffer, force the clone to
  // do the same.
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  // Pass the real replay buffer (i.e. the encoded value), not mValBuf which points to the scratch copy!
  ShellNetRecord* ret = new ShellNetRecordTwoStateClkStateOutput(primNet, mEncodedValBuf, maskBuf, mTouched);
  ret->putExternalIndex(mExternalIndex);
  ret->putInternalIndex(mInternalIndex);
  return ret;
}

void ShellNetRecordTwoStateClkStateOutput::saveStimulus(bool touched)
{
  // State output clocks are always non-tristate scalars, which makes
  // this easy.

  // Make a local copy of the current encoded value.  I don't know how
  // well all the dereferences would be optimized.
  UInt32 val = *mEncodedValBuf;

  if (touched) {
    // The net has already been touched since the last schedule call.
    // This means the current encoded value is valid.  Shift the last
    // value/valid into the previous slots
    val = (val & (ShellNetReplay::eLastValueValid | ShellNetReplay::eLastValue)) << 1;
  } else {
    // The net has not been touched yet.  There's no reason to clear
    // the encoded buffer after every schedule call, because the net's
    // touched flag will track whether it's been updated.  Now that
    // it's being touched again, we need to clear it.
    val = 0;
  }
  // Update with the new value and valid flag.  The raw value buffer
  // is used.
  val |= ShellNetReplay::eLastValueValid | (mRawValBuf & ShellNetReplay::eLastValue);
  *mEncodedValBuf = val;
}

CarbonStatus ShellNetRecordTwoStateClkStateOutput::deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  // saveStimulus() needs to know whether the net was previously touched
  bool touched = isTouched();
  CarbonStatus stat = ShellNetRecordTwoStateWord::deposit(buf, drive, model);
  saveStimulus(touched);
  return stat;
}

CarbonStatus ShellNetRecordTwoStateClkStateOutput::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  // saveStimulus() needs to know whether the net was previously touched
  bool touched = isTouched();
  CarbonStatus stat = ShellNetRecordTwoStateWord::depositWord(buf, index, drive, model);
  saveStimulus(touched);
  return stat;
}

CarbonStatus ShellNetRecordTwoStateClkStateOutput::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  // saveStimulus() needs to know whether the net was previously touched
  bool touched = isTouched();
  CarbonStatus stat = ShellNetRecordTwoStateWord::depositRange(buf, range_msb, range_lsb, drive, model);
  saveStimulus(touched);
  return stat;
}

void ShellNetRecordTwoStateClkStateOutput::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  // saveStimulus() needs to know whether the net was previously touched
  bool touched = isTouched();
  ShellNetRecordTwoStateWord::fastDeposit(buf, drive, model);
  saveStimulus(touched);
}

void ShellNetRecordTwoStateClkStateOutput::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  // saveStimulus() needs to know whether the net was previously touched
  bool touched = isTouched();
  ShellNetRecordTwoStateWord::fastDepositWord(buf, index, drive, model);
  saveStimulus(touched);
}

void ShellNetRecordTwoStateClkStateOutput::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  // saveStimulus() needs to know whether the net was previously touched
  bool touched = isTouched();
  ShellNetRecordTwoStateWord::fastDepositRange(buf, range_msb, range_lsb, drive, model);
  saveStimulus(touched);
}

void ShellNetRecordTwoStateClkStateOutput::putToZero(CarbonModel* model)
{
  // saveStimulus() needs to know whether the net was previously touched
  bool touched = isTouched();
  ShellNetRecordTwoStateWord::putToZero(model);
  saveStimulus(touched);
}

void ShellNetRecordTwoStateClkStateOutput::putToOnes(CarbonModel* model)
{
  // saveStimulus() needs to know whether the net was previously touched
  bool touched = isTouched();
  ShellNetRecordTwoStateWord::putToOnes(model);
  saveStimulus(touched);
}

CarbonStatus ShellNetRecordTwoStateClkStateOutput::setRange(int range_msb, int range_lsb, CarbonModel* model)
{
  // saveStimulus() needs to know whether the net was previously touched
  bool touched = isTouched();
  CarbonStatus stat = ShellNetRecordTwoStateWord::setRange(range_msb, range_lsb, model);
  saveStimulus(touched);
  return stat;
}

CarbonStatus ShellNetRecordTwoStateClkStateOutput::clearRange(int range_msb, int range_lsb, CarbonModel* model)
{
  // saveStimulus() needs to know whether the net was previously touched
  bool touched = isTouched();
  CarbonStatus stat = ShellNetRecordTwoStateWord::clearRange(range_msb, range_lsb, model);
  saveStimulus(touched);
  return stat;
}

/* ShellNetRecordTwoStateA */

ShellNetRecordTwoStateA::ShellNetRecordTwoStateA(ShellNet* subNet, UInt32* valBuf, UInt32* maskBuf, Touched* touchBuffer)
  : ShellNetRecord(subNet, touchBuffer),
    mValBuf(valBuf),
    mMaskBuf(maskBuf),
    mOwnMaskBuf(false)
{
  // If a mask buffer was not provided, allocate one and record the
  // fact that we own its storage.
  if (mMaskBuf == NULL) {
    mMaskBuf = CARBON_ALLOC_VEC(UInt32, mNumWords);
    mOwnMaskBuf = true;
  }
}

ShellNetRecordTwoStateA::~ShellNetRecordTwoStateA()
{
  if (mOwnMaskBuf) {
    CARBON_FREE_VEC(mMaskBuf, UInt32, mNumWords);
  }
}

ShellNetRecord* ShellNetRecordTwoStateA::cloneStorage(ShellNet* primNet)
{
  // If we allocated storage for the mask buffer, force the clone to
  // do the same.
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  ShellNetRecord* ret = new ShellNetRecordTwoStateA(primNet, mValBuf, maskBuf, mTouched);
  ret->putExternalIndex(mExternalIndex);
  ret->putInternalIndex(mInternalIndex);
  return ret;
}

UInt32* ShellNetRecordTwoStateA::getValueBuffer()
{
  return mValBuf;
}

UInt32* ShellNetRecordTwoStateA::getDriveBuffer()
{
  return NULL;
}

CarbonStatus ShellNetRecordTwoStateA::deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  if (buf)
  {
    CarbonValRW::cpSrcToDest(mValBuf, buf, mNumWords);
    mValBuf[mNumWords - 1] &= mTailMask;
    setTouched();
  }
  
  return mNet->deposit(buf, drive, model);
}

CarbonStatus ShellNetRecordTwoStateA::depositWord(UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonStatus stat = mNet->depositWord(buf, index, drive, model);
  // For arrays of words, the index is checked before
  // depositing. However, the index could be valid, we deposit, but
  // the drive was being deposited to. In this case, we return error,
  // but we still deposit the value. So, we just need to make sure the
  // index is valid here before recording the value.
  if ((unsigned)index < mNumWords)
  {
    CarbonValRW::cpSrcWordToDest(mValBuf, buf, index);
    mValBuf[mNumWords - 1] &= mTailMask;
    // Update mask
    sUpdateMaskWordLarge(mTouched, mMaskBuf, mNumWords, index, mTailMask);
    // Update only the touched flag, not the mask
    setTouchedOnly();
  }
  return stat;
}

CarbonStatus ShellNetRecordTwoStateA::depositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonStatus stat = mNet->depositRange(buf, range_msb, range_lsb, drive, model);
  if (stat == eCarbon_OK)
  {
    size_t index;
    size_t length;
    sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
    
    // Only update the valbuf with the value that was deposited to.
    mNet->examine(mValueScratch, NULL, eXDrive, model);
    CarbonValRW::cpSrcRangeToDestRange(mValBuf, index, mValueScratch, index, length);
    // Update mask
    sUpdateMaskRangeLarge(mTouched, mMaskBuf, mNumWords, index, length);
    // Update only the touched flag, not the mask
    setTouchedOnly();
  }
  return stat;
}

void ShellNetRecordTwoStateA::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  CarbonValRW::cpSrcToDest(mValBuf, buf, mNumWords);
  mValBuf[mNumWords - 1] &= mTailMask;
  setTouched();
  mNet->fastDeposit(buf, drive, model);
}

void ShellNetRecordTwoStateA::fastDepositWord(UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonValRW::cpSrcWordToDest(mValBuf, buf, index);
  mValBuf[mNumWords - 1] &= mTailMask;
  // Update mask
  sUpdateMaskWordLarge(mTouched, mMaskBuf, mNumWords, index, mTailMask);
  // Update only the touched flag, not the mask
  setTouchedOnly();
  mNet->fastDepositWord(buf, index, drive, model);
}

void ShellNetRecordTwoStateA::fastDepositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  mNet->fastDepositRange(buf, range_msb, range_lsb, drive, model);
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    // Only update the valbuf with the value that was deposited to.
    mNet->examine(mValueScratch, NULL, eXDrive, model);
    CarbonValRW::cpSrcRangeToDestRange(mValBuf, index, mValueScratch, index, length);
    // Update mask
    sUpdateMaskRangeLarge(mTouched, mMaskBuf, mNumWords, index, length);
    // Update only the touched flag, not the mask
    setTouchedOnly();
  }
}


void ShellNetRecordTwoStateA::putToZero(CarbonModel* model)
{
  CarbonValRW::setToZero(mValBuf, mNumWords);
  mNet->putToZero(model);
  setTouched();
}

void ShellNetRecordTwoStateA::putToOnes(CarbonModel* model)
{
  CarbonValRW::setToOnes(mValBuf, mNumWords);
  mValBuf[mNumWords - 1] &= mTailMask;
  mNet->putToOnes(model);
  setTouched();
}

CarbonStatus ShellNetRecordTwoStateA::setRange(int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    CarbonValRW::setRangeToOnes(mValBuf, index, length);
    stat = mNet->setRange(range_msb, range_lsb, model);
    // Update mask
    sUpdateMaskRangeLarge(mTouched, mMaskBuf, mNumWords, index, length);
    // Update only the touched flag, not the mask
    setTouchedOnly();
  }
  return stat;
}

CarbonStatus ShellNetRecordTwoStateA::clearRange(int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    CarbonValRW::setRangeToZero(mValBuf, index, length);
    stat = mNet->clearRange(range_msb, range_lsb, model);
    // Update mask
    sUpdateMaskRangeLarge(mTouched, mMaskBuf, mNumWords, index, length);
    // Update only the touched flag, not the mask
    setTouchedOnly();
  }
  return stat;
}


/* ShellNetRecordTristate */

ShellNetRecordTristate::ShellNetRecordTristate(ShellNet* subNet, UInt32* valBuf, UInt32* drvBuf, Touched* touchBuffer)
  : ShellNetRecord(subNet, touchBuffer),
    mValBuf(valBuf), mDrvBuf(drvBuf)
{
  mDriveScratch = CARBON_ALLOC_VEC(UInt32, mNumWords);
}

ShellNetRecordTristate::~ShellNetRecordTristate()
{
  CARBON_FREE_VEC(mDriveScratch, UInt32, mNumWords);
}

ShellNetRecord* ShellNetRecordTristate::cloneStorage(ShellNet* primNet)
{
  ST_ASSERT(primNet->isTristate(), primNet->getName());
  ShellNetRecord* ret = new ShellNetRecordTristate(primNet, mValBuf, mDrvBuf, mTouched);
  ret->putExternalIndex(mExternalIndex);
  ret->putInternalIndex(mInternalIndex);
  return ret;
}

UInt32* ShellNetRecordTristate::getValueBuffer()
{
  return mValBuf;
}

UInt32* ShellNetRecordTristate::getDriveBuffer()
{
  return mDrvBuf;
}

/*
  Note: On all deposits we must examine after the deposit because the
  value passed in by the user may not stick depending on the drive
  value.
*/
CarbonStatus ShellNetRecordTristate::deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  CarbonStatus stat = mNet->deposit(buf, drive, model);
  mNet->examine(mValBuf, mDrvBuf, eXDrive, model);
  setTouched();
  return stat;
}

CarbonStatus ShellNetRecordTristate::depositWord(UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonStatus stat = mNet->depositWord(buf, index, drive, model);
  // We could just check stat here since the only reason stat would be
  // error for a tristate is if the index is out of bounds. But having
  // the same code as the non-tristate version is clearer, IMO.
  if ((unsigned)index < mNumWords)
  {
    mNet->examineValXDriveWord(&mValBuf[index], &mDrvBuf[index], index);
    setTouched();
  }
  return stat;
}

CarbonStatus ShellNetRecordTristate::depositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonStatus stat = mNet->depositRange(buf, range_msb, range_lsb, drive, model);
  if (stat == eCarbon_OK)
  {
    size_t index;
    size_t length;
    sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
    mNet->examine(mValueScratch, mDriveScratch, eXDrive, model);
    CarbonValRW::cpSrcRangeToDestRange(mValBuf, index, mValueScratch, index, length);
    CarbonValRW::cpSrcRangeToDestRange(mDrvBuf, index, mDriveScratch, index, length);
    setTouched();
  }
  return stat;
}

void ShellNetRecordTristate::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  mNet->fastDeposit(buf, drive, model);
  mNet->examine(mValBuf, mDrvBuf, eXDrive, model);
  setTouched();
}

void ShellNetRecordTristate::fastDepositWord(UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  mNet->fastDepositWord(buf, index, drive, model);
  mNet->examineValXDriveWord(&mValBuf[index], &mDrvBuf[index], index);
  setTouched();
}

void ShellNetRecordTristate::fastDepositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  mNet->fastDepositRange(buf, range_msb, range_lsb, drive, model);
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    // examine the entire value
    mNet->examine(mValueScratch, mDriveScratch, eXDrive, model);
    // Grab only the part of the value that was assigned to.
    CarbonValRW::cpSrcRangeToDestRange(mValBuf, index, mValueScratch, index, length);
    CarbonValRW::cpSrcRangeToDestRange(mDrvBuf, index, mDriveScratch, index, length);
    setTouched();
  }
}

void ShellNetRecordTristate::setRawToUndriven(CarbonModel* model)
{
  mNet->setRawToUndriven(model);
  mNet->examine(mValBuf, mDrvBuf, eXDrive, NULL);
  setTouched();
}

bool ShellNetRecordTristate::setToUndriven(CarbonModel* model)
{
  bool ret = mNet->setToUndriven(model);
  mNet->examine(mValBuf, mDrvBuf, eXDrive, NULL);
  setTouched();
  return ret;
}

bool ShellNetRecordTristate::resolveXdrive(CarbonModel* model)
{
  bool ret = mNet->resolveXdrive(model);
  mNet->examine(mValBuf, mDrvBuf, eXDrive, model);
  setTouched();
  return ret;
}

/* ShellNetRecordBidirectClk */

ShellNetRecordBidirectClk::ShellNetRecordBidirectClk(ShellNet* subNet, 
                                                     UInt32* valBuf, 
                                                     UInt32* drvBuf, 
                                                     UInt32* valShadow, 
                                                     UInt32* drvShadow,
                                                     Touched* touchBuffer)
  : ShellNetRecordTristate(subNet, valBuf, drvBuf, touchBuffer),
    mValShadow(valShadow), mDrvShadow(drvShadow)
{}


ShellNetRecordBidirectClk::~ShellNetRecordBidirectClk()
{}

ShellNetRecord* ShellNetRecordBidirectClk::cloneStorage(ShellNet* primNet)
{
  ST_ASSERT(primNet->isTristate(), primNet->getName());
  ShellNetRecord* ret = new ShellNetRecordBidirectClk(primNet, mValBuf, mDrvBuf, mValShadow, mDrvShadow, mTouched);
  ret->putExternalIndex(mExternalIndex);
  ret->putInternalIndex(mInternalIndex);
  return ret;
}

void ShellNetRecordBidirectClk::saveCurrentValue()
{
  // see ShellNetRecordTwoStateClk::saveCurrentValue for comments
  *mValShadow = *mValBuf;
  *mDrvShadow = *mDrvBuf;
}

CarbonStatus ShellNetRecordBidirectClk::deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  saveCurrentValue();
  return ShellNetRecordTristate::deposit(buf, drive, model);
}

CarbonStatus ShellNetRecordBidirectClk::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  saveCurrentValue();
  return ShellNetRecordTristate::depositWord(buf, index, drive, model);
}

CarbonStatus ShellNetRecordBidirectClk::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  saveCurrentValue();
  return ShellNetRecordTristate::depositRange(buf, range_msb, range_lsb, drive, model);
}

void ShellNetRecordBidirectClk::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  saveCurrentValue();
  ShellNetRecordTristate::fastDeposit(buf, drive, model);
}

void ShellNetRecordBidirectClk::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  saveCurrentValue();
  ShellNetRecordTristate::fastDepositWord(buf, index, drive, model);
}

void ShellNetRecordBidirectClk::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  saveCurrentValue();
  ShellNetRecordTristate::fastDepositRange(buf, range_msb, range_lsb, drive, model);
}

void ShellNetRecordBidirectClk::setRawToUndriven(CarbonModel* model)
{
  saveCurrentValue();
  ShellNetRecordTristate::setRawToUndriven(model);
}

bool ShellNetRecordBidirectClk::setToUndriven(CarbonModel* model)
{
  saveCurrentValue();
  return ShellNetRecordTristate::setToUndriven(model);
}

bool ShellNetRecordBidirectClk::resolveXdrive(CarbonModel* model)
{
  saveCurrentValue();
  return ShellNetRecordTristate::resolveXdrive(model);
}


/* ShellNetRecordMem */

ShellNetRecordMem::ShellNetRecordMem(ShellNet* mem, UInt32 externalIndex, SInt32HashSet* changedAddrs) :
  ShellNetRecord(mem, NULL), mChangedAddrs(changedAddrs)
{
  putExternalIndex(externalIndex);
}

ShellNetRecordMem::~ShellNetRecordMem()
{}

const ShellNetRecordMem* ShellNetRecordMem::castShellNetRecordMem() const
{
  return this;
}

ShellNetRecord* ShellNetRecordMem::cloneStorage(ShellNet* primNet)
{
  ST_ASSERT(primNet->castMemory() != NULL, primNet->getName());
  ShellNetRecordMem* ret = new ShellNetRecordMem(primNet, mExternalIndex, mChangedAddrs);
  ret->putInternalIndex(mInternalIndex);
  return ret;
}

UInt32* ShellNetRecordMem::getValueBuffer()
{
  return NULL;
}

UInt32* ShellNetRecordMem::getDriveBuffer()
{
  return NULL;
}

void ShellNetRecordMem::recordMemWrite(CarbonMemAddrT addr)
{
  mChangedAddrs->insert(addr);
}

CarbonStatus ShellNetRecordMem::recordReadMem(const char* filename, CarbonRadix radix)
{
  // read the memh file and gather the addresses.
  CarbonMemFile memFile(mMemNet->getCarbonModel(), filename, radix, mMemNet->getRowBitWidth(), 0);
  CarbonStatus stat = memFile.load();
  if (stat == eCarbon_OK)
  {
    SInt64 firstAddr, lastAddr;
    memFile.getFirstAndLastAddrs(&firstAddr, &lastAddr);
    UInt64 numAddrs = CBITWIDTH(firstAddr, lastAddr);
    for (UInt64 i = 0; i < numAddrs; ++i)
    {
      if (memFile.getRow(firstAddr) != NULL)
        recordMemWrite(firstAddr);
      ++firstAddr;
    }
  }
  return stat;
}

CarbonStatus ShellNetRecordMem::recordReadMem(const char* filename, SInt64 startAddr, SInt64 endAddr, CarbonRadix radix)
{
  // Unlike the other recordReadMem() above, we need to consider the
  // requested range when reading the data file, so work with the
  // HDLReadMemX object directly.
  bool isHex = (radix == eCarbonHex);
  CarbonOpaque id = mMemNet->getCarbonModel()->getHandle();
  // Don't check range of addresses in the file.  The underlying
  // memory has already been written, and the check would have been
  // done then.  All we're doing here is notifying the recording
  // mechanism about which addresses have changed.
  HDLReadMemX memFile(filename, isHex, mMemNet->getRowBitWidth(), startAddr, endAddr, false, id);
  CarbonStatus stat = eCarbon_ERROR;
  if (memFile.openFile()) {
    stat = eCarbon_OK;
    // Loop through the addresses and data from the file.  For each
    // one, if it's in the requested range, record that it was
    // written.
    //
    // I can't use the ConstantRange class to test if an address is in
    // range, because that deals with SInt32s, not SInt64s.
    SInt64 lowAddr = (startAddr < endAddr) ? startAddr : endAddr;
    SInt64 highAddr = (startAddr < endAddr) ? endAddr : startAddr;
    HDLReadMemXResult result;
    SInt64 address;
    UInt32* data = CARBON_ALLOC_VEC(UInt32, mNumWords);
    while ((result = memFile.getNextWord(&address, data)) == eRMValidData) {
      if ((address >= lowAddr) && (address <= highAddr)) {
        recordMemWrite(address);
      }
    }
    CARBON_FREE_VEC(data, UInt32, mNumWords);
    memFile.closeFile();
  }
  return stat;
}

CarbonStatus ShellNetRecordMem::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  CarbonStatus stat = mMemNet->depositMemory(address, buf);
  if (stat == eCarbon_OK)
    recordMemWrite(address);
  return stat;
}

CarbonStatus ShellNetRecordMem::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  CarbonStatus stat = mMemNet->depositMemoryWord(address, buf, index);
  if (stat == eCarbon_OK)
    recordMemWrite(address);
  return stat;
}

CarbonStatus ShellNetRecordMem::depositMemoryRange(CarbonMemAddrT address, const UInt32* buf, int range_msb, int range_lsb)
{
  CarbonStatus stat = mMemNet->depositMemoryRange(address, buf, range_msb, range_lsb);
  if (stat == eCarbon_OK)
    recordMemWrite(address);
  return stat;
}

CarbonStatus ShellNetRecordMem::readmemh(const char* filename)
{
  CarbonStatus stat = mMemNet->readmemh(filename);
  if (stat == eCarbon_OK)
    // if the actual readmemh is not ok, don't record anything. We'll
    // accept a divergence on playback.
    stat = recordReadMem(filename, eCarbonHex);
  return stat;
}

CarbonStatus ShellNetRecordMem::readmemb(const char* filename)
{
  CarbonStatus stat = mMemNet->readmemb(filename);
  if (stat == eCarbon_OK)
    // if the actual readmemh is not ok, don't record anything. We'll
    // accept a divergence on playback.
    stat = recordReadMem(filename, eCarbonBin);
  return stat;
}

CarbonStatus ShellNetRecordMem::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  CarbonStatus stat = mMemNet->readmemh(filename, startAddr, endAddr);
  if (stat == eCarbon_OK)
    // if the actual readmemh is not ok, don't record anything. We'll
    // accept a divergence on playback.
    stat = recordReadMem(filename, startAddr, endAddr, eCarbonHex);
  return stat;
}

CarbonStatus ShellNetRecordMem::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  CarbonStatus stat = mMemNet->readmemb(filename, startAddr, endAddr);
  if (stat == eCarbon_OK)
    // if the actual readmemh is not ok, don't record anything. We'll
    // accept a divergence on playback.
    stat = recordReadMem(filename, startAddr, endAddr, eCarbonBin);
  return stat;
}

const UInt32* ShellNetRecordMem::fetchValue(SInt32 address)
{
  ST_ASSERT(mMemNet->examineMemory(address, mValueScratch) == eCarbon_OK, getName());
  return mValueScratch;
}

CarbonStatus ShellNetRecordMem::deposit (const UInt32*, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetRecordMem::depositWord (UInt32, int, UInt32, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetRecordMem::depositRange (const UInt32*, int, int, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void ShellNetRecordMem::fastDeposit(const UInt32*, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
}
void ShellNetRecordMem::fastDepositWord (UInt32, int, UInt32, CarbonModel*)
{
  ST_ASSERT(0, getName());
}
void ShellNetRecordMem::fastDepositRange (const UInt32*, int, int, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
}


/*  ShellNetRecordResponse */

ShellNetRecordResponse::~ShellNetRecordResponse()
{}

ShellNetRecordResponse::ShellNetRecordResponse(ShellNet* subNet, UInt32* valBuf, Touched* touchBuffer)
  : ShellNetRecord(subNet, touchBuffer), mValBuf(valBuf)
{}

UInt32* ShellNetRecordResponse::getValueBuffer()
{
  return mValBuf;
}

UInt32* ShellNetRecordResponse::getDriveBuffer()
{
  return NULL;
}

void ShellNetRecordResponse::updateBuffer(CarbonModel* model)
{
  mNet->examine(mValBuf, NULL, eIDrive, model);
}

ShellNetRecord* ShellNetRecordResponse::cloneStorage(ShellNet* primNet)
{
  ST_ASSERT(0, primNet->getName());
  return NULL;
}

CarbonStatus ShellNetRecordResponse::deposit (const UInt32*, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetRecordResponse::depositWord (UInt32, int, UInt32, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetRecordResponse::depositRange (const UInt32*, int, int, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void ShellNetRecordResponse::fastDeposit(const UInt32*, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
}
void ShellNetRecordResponse::fastDepositWord (UInt32, int, UInt32, CarbonModel*)
{
  ST_ASSERT(0, getName());
}
void ShellNetRecordResponse::fastDepositRange (const UInt32*, int, int, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
}

ShellNetRecordResponseTristate::ShellNetRecordResponseTristate(ShellNet* subNet, UInt32* valBuf, UInt32* drvBuf, 
                                                               Touched* touchBuffer)
  : ShellNetRecordResponse(subNet, valBuf, touchBuffer), mDrvBuf(drvBuf)
{}

ShellNetRecordResponseTristate::~ShellNetRecordResponseTristate()
{}

void ShellNetRecordResponseTristate::updateBuffer(CarbonModel* model)
{
  mNet->examine(mValBuf, mDrvBuf, eIDrive, model);
  // Flip the drive. Codegen is exactly opposite in the value of
  // drive: 0 is not driving, 1 is driving.
  for (UInt32 i = 0; i < mNumWords; ++i)
    mDrvBuf[i] = ~mDrvBuf[i];
  // sanitize
  mDrvBuf[mNumWords - 1] &= mTailMask;
}

UInt32* ShellNetRecordResponseTristate::getDriveBuffer()
{
  return mDrvBuf;
}

/* ShellNetRecordNotDepositable */
ShellNetRecordNotDepositable::ShellNetRecordNotDepositable(ShellNet* subNet)
  : ShellNetRecord(subNet, NULL)  // Touched is null, but will never be referenced
{}

ShellNetRecordNotDepositable::~ShellNetRecordNotDepositable()
{}

ShellNetRecord* ShellNetRecordNotDepositable::cloneStorage(ShellNet* primNet)
{
  ShellNetRecord* ret = new ShellNetRecordNotDepositable(primNet);
  return ret;
}

UInt32* ShellNetRecordNotDepositable::getValueBuffer()
{
  // This net is not actually recorded, so there's no value buffer
  ST_ASSERT(0, getName());
  return NULL;
}

UInt32* ShellNetRecordNotDepositable::getDriveBuffer()
{
  // This net is not actually recorded, so there's no drive buffer
  ST_ASSERT(0, getName());
  return NULL;
}

CarbonStatus ShellNetRecordNotDepositable::deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  ShellGlobal::reportNotReplayDepositable(getNameAsLeaf(), model);
  CarbonStatus stat = mNet->deposit(buf, drive, model);
  return stat;
}

CarbonStatus ShellNetRecordNotDepositable::depositWord(UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  ShellGlobal::reportNotReplayDepositable(getNameAsLeaf(), model);
  CarbonStatus stat = mNet->depositWord(buf, index, drive, model);
  return stat;
}

CarbonStatus ShellNetRecordNotDepositable::depositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  ShellGlobal::reportNotReplayDepositable(getNameAsLeaf(), model);
  CarbonStatus stat = mNet->depositRange(buf, range_msb, range_lsb, drive, model);
  return stat;
}

void ShellNetRecordNotDepositable::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  ShellGlobal::reportNotReplayDepositable(getNameAsLeaf(), model);
  mNet->fastDeposit(buf, drive, model);
}

void ShellNetRecordNotDepositable::fastDepositWord(UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  ShellGlobal::reportNotReplayDepositable(getNameAsLeaf(), model);
  mNet->fastDepositWord(buf, index, drive, model);
}

void ShellNetRecordNotDepositable::fastDepositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  ShellGlobal::reportNotReplayDepositable(getNameAsLeaf(), model);
  mNet->fastDepositRange(buf, range_msb, range_lsb, drive, model);
}

void ShellNetRecordNotDepositable::putToZero(CarbonModel* model)
{
  ShellGlobal::reportNotReplayDepositable(getNameAsLeaf(), model);
  mNet->putToZero(model);
}

void ShellNetRecordNotDepositable::putToOnes(CarbonModel* model)
{
  ShellGlobal::reportNotReplayDepositable(getNameAsLeaf(), model);
  mNet->putToOnes(model);
}

CarbonStatus ShellNetRecordNotDepositable::setRange(int range_msb, int range_lsb, CarbonModel* model)
{
  ShellGlobal::reportNotReplayDepositable(getNameAsLeaf(), model);
  CarbonStatus stat = mNet->setRange(range_msb, range_lsb, model);
  return stat;
}

CarbonStatus ShellNetRecordNotDepositable::clearRange(int range_msb, int range_lsb, CarbonModel* model)
{
  ShellGlobal::reportNotReplayDepositable(getNameAsLeaf(), model);
  CarbonStatus stat = mNet->clearRange(range_msb, range_lsb, model);
  return stat;
}

CarbonStatus ShellNetRecordNotDepositable::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  STAliasedLeafNode *leaf = getNameAsLeaf();
  ST_ASSERT(mMemNet, leaf);
  CarbonModel *model = mMemNet->getCarbonModel();
  ShellGlobal::reportNotReplayDepositable(leaf, model);
  CarbonStatus stat = mMemNet->depositMemory(address, buf);
  return stat;
}

CarbonStatus ShellNetRecordNotDepositable::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  STAliasedLeafNode *leaf = getNameAsLeaf();
  ST_ASSERT(mMemNet, leaf);
  CarbonModel *model = mMemNet->getCarbonModel();
  ShellGlobal::reportNotReplayDepositable(leaf, model);
  CarbonStatus stat = mMemNet->depositMemoryWord(address, buf, index);
  return stat;
}

CarbonStatus ShellNetRecordNotDepositable::depositMemoryRange(CarbonMemAddrT address, const UInt32* buf, int range_msb, int range_lsb)
{
  STAliasedLeafNode *leaf = getNameAsLeaf();
  ST_ASSERT(mMemNet, leaf);
  CarbonModel *model = mMemNet->getCarbonModel();
  ShellGlobal::reportNotReplayDepositable(leaf, model);
  CarbonStatus stat = mMemNet->depositMemoryRange(address, buf, range_msb, range_lsb);
  return stat;
}

CarbonStatus ShellNetRecordNotDepositable::readmemh(const char* filename)
{
  STAliasedLeafNode *leaf = getNameAsLeaf();
  ST_ASSERT(mMemNet, leaf);
  CarbonModel *model = mMemNet->getCarbonModel();
  ShellGlobal::reportNotReplayDepositable(leaf, model);
  CarbonStatus stat = mMemNet->readmemh(filename);
  return stat;
}

CarbonStatus ShellNetRecordNotDepositable::readmemb(const char* filename)
{
  STAliasedLeafNode *leaf = getNameAsLeaf();
  ST_ASSERT(mMemNet, leaf);
  CarbonModel *model = mMemNet->getCarbonModel();
  ShellGlobal::reportNotReplayDepositable(leaf, model);
  CarbonStatus stat = mMemNet->readmemb(filename);
  return stat;
}

CarbonStatus ShellNetRecordNotDepositable::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  STAliasedLeafNode *leaf = getNameAsLeaf();
  ST_ASSERT(mMemNet, leaf);
  CarbonModel *model = mMemNet->getCarbonModel();
  ShellGlobal::reportNotReplayDepositable(leaf, model);
  CarbonStatus stat = mMemNet->readmemh(filename, startAddr, endAddr);
  return stat;
}

CarbonStatus ShellNetRecordNotDepositable::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  STAliasedLeafNode *leaf = getNameAsLeaf();
  ST_ASSERT(mMemNet, leaf);
  CarbonModel *model = mMemNet->getCarbonModel();
  ShellGlobal::reportNotReplayDepositable(leaf, model);
  CarbonStatus stat = mMemNet->readmemb(filename, startAddr, endAddr);
  return stat;
}

ReplayRecordBuffer::ReplayRecordBuffer(UInt32 dataSize) :
  mAllocIndex(0), mNumWords(dataSize)
{
  mValueBV.resize(dataSize * 32);
  mShadowBV.resize(dataSize * 32);
  mValues = mValueBV.getUIntArray();
  mShadowValues = mShadowBV.getUIntArray();
  
  mTouchedReference = CARBON_ALLOC_VEC(ShellNetReplay::Touched*, mNumWords);
  mStimNets = CARBON_ALLOC_VEC(ShellNetRecord*, mNumWords);
  memset(mStimNets, 0, mNumWords * sizeof(ShellNetRecord*));
}

ReplayRecordBuffer::~ReplayRecordBuffer()
{
  CARBON_FREE_VEC(mTouchedReference, ShellNetReplay::Touched*, mNumWords);
  CARBON_FREE_VEC(mStimNets, ShellNetRecord*, mNumWords);
}

void ReplayRecordBuffer::allocateValues(UInt32 netWordSize,
                                        UInt32* bufferIndex,
                                        UInt32** primaryBuffer,
                                        UInt32** secondaryBuffer,
                                        UInt32** primaryShadow,
                                        UInt32** secondaryShadow,
                                        ShellNetReplay::Touched* touched,
                                        ShellNetRecord* stimNet)
{
  // If the secondary buffer pointer is NULL, only allocate space in primary buffer
  bool doSecondary = (secondaryBuffer != NULL);
  UInt32 wordsToAlloc = netWordSize;
  if (doSecondary) {
    INFO_ASSERT(secondaryShadow != NULL, "Inconsistent secondary buffer/shadow pointers");
    wordsToAlloc = netWordSize * 2;
  }

  // Allocate the buffer space
  INFO_ASSERT((mAllocIndex + wordsToAlloc) <= mNumWords,
              "Allocation beyond the end of a replay record buffer");
  *primaryBuffer = &mValues[mAllocIndex];
  *primaryShadow = &mShadowValues[mAllocIndex];
  if (doSecondary) {
    *secondaryBuffer = &mValues[mAllocIndex + netWordSize];
    *secondaryShadow = &mShadowValues[mAllocIndex + netWordSize];
  }

  // Update the touched and stim reference maps
  for (UInt32 i = mAllocIndex; i < (mAllocIndex + wordsToAlloc); ++i) {
    mTouchedReference[i] = touched;
    mStimNets[i] = stimNet;
  }

  // Update the requested indexes
  *bufferIndex = mAllocIndex;

  // Now we can update the allocated indexes
  mAllocIndex += wordsToAlloc;
} // ReplayRecordBuffer::allocateValues

void ReplayRecordBuffer::mapOffsetsToStimNet(UInt32 offset, UInt32 words, ShellNetRecord *stimNet)
{
  // Map 'words' entries in the stimulus net array, starting at
  // 'offset', to the specified ShellNetRecord.
  while (words--) {
    mStimNets[offset++] = stimNet;
  }
}

void ReplayRecordBuffer::processChanges(bool recordAll, ChangeCallback* callback)
{
  // Walk all the buffer words, detect changes and : (1) set the
  // touched flag for the appropriate net, (2), update the shadow
  // value, (3), notify the caller about the change.
  UInt32 notifySize = 0;
  UInt32 notifyByteOffset = 0;
  for (UInt32 wordIndex = 0; wordIndex < mNumWords; ++wordIndex) {
    const UInt32 changed = mValues[wordIndex] ^ mShadowValues[wordIndex];
    // Having this if is 10% faster than not having it. Apparently,
    // the branch is less expensive than the dereference and word copy
    // in this case.
    if (changed != 0) {
      // Update the shadow and set the touched bit. We find the
      // touched bit using the word offset to touched pointer
      // reference.
      mShadowValues[wordIndex] = mValues[wordIndex];
      ShellNetReplay::Touched* touched = mTouchedReference[wordIndex];
      *touched = true;
    }

    // There is another possiblity. This is both a response net and a
    // stimulus net. If so, a deposit should get clobbered by a Carbon
    // Model update. So mark the values as changed if the response net
    // was also deposited to (touched). We want to update the entire net
    // in this case so we don't do byte change detection below. This
    // is because we don't know what parts of the net got deposited.
    const ShellNetRecord* net = mStimNets[wordIndex];
    const bool forcedChange = recordAll | (net && net->isTouched());

    // Notify the callback of the change if we detected a change
    if ((changed != 0) | forcedChange) {
      // If the word changed, check each byte to see if we can write
      // just one. This is beneficial when we have scalars (it will
      // most likely be the low byte but we check for all cases).
      //
      // Note that we don't do this if recordAll is true or if the net
      // was deposited. This is because we want to write all of it in
      // these cases.
      UInt32 byteOffset = (wordIndex * sizeof(UInt32));
      UInt32 byteChangeCount = 0;
      const char* bytes = reinterpret_cast<const char*>(&changed);
      for (UInt32 j = 0; 
           (j < sizeof(UInt32)) & (byteChangeCount < 2) & (changed != 0) & !forcedChange; 
           ++j) {
        const UInt32 byteChanged = bytes[j] != 0;
        const UInt32 byteNotChanged = ! byteChanged;
        byteOffset = 
          byteOffset * byteNotChanged + 
          byteChanged * ((wordIndex * sizeof(UInt32)) + j);
        byteChangeCount += byteChanged;
      }
      
      // If only one byte changed, notify about that
      if (byteChangeCount == 1) {
        // Notify about any previous changed words from before
        notifyChange(callback, notifySize, notifyByteOffset);
        notifySize = 0;

        // Now notify about this byte
        notifyChange(callback, 1, byteOffset);
      } else {
        // Delay this notify to accumulate more

        // If this is the first entry, update the start offset,
        // otherwise leave it alone. Don't use an if statement
        // here. The branch is expensive.
        const UInt32 firstEntry = (notifySize == 0);
        const UInt32 notFirstEntry = ! firstEntry;
        notifyByteOffset = firstEntry * (wordIndex * sizeof(UInt32)) + 
          notFirstEntry * notifyByteOffset;

        notifySize += sizeof(UInt32);
      }

    } else {
      // This word did not change. Check if any previous words change and
      // notify about that.
      notifyChange(callback, notifySize, notifyByteOffset);
      notifySize = 0;
    }
  } // for

  // We may have accumulated changes that include the last word of the
  // record buffer. If so, notify about them now.
  notifyChange(callback, notifySize, notifyByteOffset);
} // void ReplayRecordBuffer::processChanges

void
ReplayRecordBuffer::notifyChange(ChangeCallback* callback, UInt32 notifySize,
                                 UInt32 notifyByteOffset)
{
  if (notifySize > 0) {
    const UInt8* buf = reinterpret_cast<const UInt8*>(mValues);
    callback->changedData(notifyByteOffset, notifySize, &buf[notifyByteOffset]);
  }
}

void ReplayRecordBuffer::updateShadow(void)
{
  mShadowBV = mValueBV;
}

void ReplayRecordBuffer::updateValue(void)
{
  mValueBV = mShadowBV;
}

void ReplayRecordBuffer::printValues() const
{
  mValueBV.printHex();
}

void ReplayRecordBuffer::printShadows() const
{
  mShadowBV.printHex();
}
