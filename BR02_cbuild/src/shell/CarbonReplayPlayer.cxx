// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// Needed for windows compile. Without this I get:
/*
/tools/windows/msvc-2003/include/vector(377) : warning C4530: C++ exception handler used, but unwind semantics are not enabled. Specify /EHsc
*/
#include "util/CarbonPlatform.h"

#include "shell/CarbonReplayPlayer.h"
#include "iodb/IODBRuntime.h"
#include "shell/CarbonDBRead.h"
#include "shell/ShellData.h"
#include "shell/CarbonForceNet.h"


namespace CarbonReplay {

  // CModelPlayer
  CModelPlayer::~CModelPlayer()
  {}
  
  // ReplayCModelManager::CmodelPlayerPlayback
  class ReplayCModelManager::CModelPlayerPlayback : public CModelPlayer
  {
  public:
    CARBONMEM_OVERRIDES
    
    CModelPlayerPlayback(ReplayPlayer* player)
      : CModelPlayer(player)
    {}
    
    virtual ~CModelPlayerPlayback()
    {}
    
    virtual bool playCModelCall(ReplayCModelCall* cmodelCall)
    {
      EventClosure& closure = mPlayer->mClosure;

      if (closure.mWheelInterrupt == 0)
      {
        closure.mWheelInterrupt = cmodelCall->play();

        // keep this assert around but disabled in case I need to
        // check on something I know is not diverging. 
#if 0
        INFO_ASSERT(closure.mWheelInterrupt == 0, 
                    cmodelCall->getFormal()->getName());
#endif
        // If the CModel diverged, set the stim check to the last
        // cmodel event, which is this cmodel call.
        if (closure.mWheelInterrupt != 0)
        {
          mPlayer->syncStimCheckToLastCModelEvent();
          mPlayer->saveRecoverEvents();
          mPlayer->stopStream();
          UtString reason;
          reason << "Cmodel output divergence in " << cmodelCall->getFormal()->getName();
          mPlayer->reportDivergenceReason(reason.c_str());
        }
      }
      
      return closure.mWheelInterrupt != 0;
    }
  };
  
  class ReplayCModelManager::CModelPlayerRecover : public CModelPlayer
  {
  public:
    CARBONMEM_OVERRIDES
      
    CModelPlayerRecover(ReplayPlayer* player, RecoverContext* recoverContext)
      : CModelPlayer(player), mRecoverContext(recoverContext)
    {}

    virtual ~CModelPlayerRecover()
    {}
    
    virtual bool playCModelCall(ReplayCModelCall* cmodelCall)
    {
      // stop the wheel if this cmodel call was the cause of the
      // divergence

      // This cannot be called after we undo playback mode! mPlayer
      // will be garbage
      bool atDivergencePoint = mPlayer->isLastCModelEventDivergence();
      mRecoverContext->pushRecoverCModelCall(cmodelCall, atDivergencePoint);

      EventClosure& closure = mPlayer->mClosure;
      closure.mWheelInterrupt = atDivergencePoint;
      mPlayer->maybeStopStream(atDivergencePoint);
      mRecoverContext->putReachedCModelDivergenceTime(atDivergencePoint);
      return atDivergencePoint;
    }
    
  private:
    RecoverContext* mRecoverContext;
  };

  // ReplayCModelManager
  ReplayCModelManager::ReplayCModelManager(ReplayPlayer* player)
  {
    clearCall();
    mCModelPlayer = new CModelPlayerPlayback(player);
  }
    
  ReplayCModelManager::~ReplayCModelManager() 
  {
    delete mCModelPlayer;
  }

  //! Add a cmodel to the playback mechanism
  void ReplayCModelManager::addCModel(ReplayCModel* replayCModel)
  {
    UInt32 cmodelId = replayCModel->getId();
    sFitIndexIntoArray(mCModels, cmodelId);
    
    INFO_ASSERT(mCModels[cmodelId] == NULL, replayCModel->getName());
    
    mCModels[cmodelId] = replayCModel;
  }

  void ReplayCModelManager::setRecoverMode(RecoverContext& recoverContext)
  {
    clearCall();
    ReplayPlayer* player = mCModelPlayer->getReplayPlayer();
    delete mCModelPlayer;
    mCModelPlayer = new CModelPlayerRecover(player, &recoverContext);
  }  

  // CModelCallQueue
  CModelRecoverQueue::CModelRecoverQueue()
    : mIndex(0)
  {}
  
  CModelRecoverQueue::~CModelRecoverQueue()
  {
    clear();
  }
  
  void CModelRecoverQueue::clear()
  {
    for (CModelRecoverVec::iterator p = mCModelCalls.begin(),
           e = mCModelCalls.end(); p != e; ++p)
      delete *p;
    mCModelCalls.resize(0);
    mIndex = 0;
  }
  
  void CModelRecoverQueue::pushCall(ReplayCModelCall* cmodelCall, 
                                    bool notShadow)
  {
    CModelRecover* cmodRec = new CModelRecover(cmodelCall, notShadow);
    mCModelCalls.push_back(cmodRec);
  }

  CModelRecover* CModelRecoverQueue::popCall()
  {
    INFO_ASSERT(mIndex < mCModelCalls.size(), "CModelCall queue overrun.");
    return mCModelCalls[mIndex++];
  }

  // ReplayPlayer

  ReplayPlayer::ReplayPlayer(const UtString& dbName, const char* indexName, 
                             CarbonModel* model,
                             CBDataValuePairVec* netChangeCBs,
                             CarbonReplayInfo* replayInfo)
    : mStatus(eCarbon_OK), 
      mCarbonModel(model), mDBName(&dbName), mRecoverDivergencePoint(0),
      mNumEvents(0), mStimCheck(0), mLastCModelEvent(0), mEventFileListIndex(0),
      mReplayInfo(replayInfo), mNetChangeCBs(netChangeCBs),
      mInputWordToNet(NULL), mResponseWordToNet(NULL),
      mTouchedOffsetToWordOffset(NULL),
      mNoNextEventFile(false), mUpdateTouchedStatus(false),
      mDivergenceReported(false)
  {
    mStreamCB = new ReplayIStreamCB(this);
    mNetChangeValueBuffer = mCarbonModel->getNetValueChangeCBBuffer();
    readEventIndex(indexName);
    openNextEventFile();
    // Windows complains if I try to use 'this' in member
    // initialization list
    mCModelManager = new ReplayCModelManager(this);
  }

  ReplayPlayer::~ReplayPlayer()
  {
    delete mStreamCB;
    delete mCModelManager;

    for (ResponseNetVec::iterator p = mPlayResponseNets.begin(),
           e = mPlayResponseNets.end(); p != e; ++p)
      delete *p;
    
    for (ReplayMemChangeMapVec::iterator p =  mAddrChangeSets.begin(),
           e = mAddrChangeSets.end(); p != e; ++p)
      delete *p;
    
    for (SInt32UInt32MapVec::iterator p =  mWordRowMems.begin(),
           e = mWordRowMems.end(); p != e; ++p)
      delete *p;
    
    for (SInt32DynBVMapVec::iterator p =  mArrayRowMems.begin(),
           e = mArrayRowMems.end(); p != e; ++p)
      delete *p;
    
    for (DBMemVec::iterator p = mDBExternalMems.begin(),
           e = mDBExternalMems.end(); p != e; ++p)
      delete *p;
    
    for (DBMemVec::iterator p = mDBInternalMems.begin(),
           e = mDBInternalMems.end(); p != e; ++p)
      delete *p;
    
    
    if (mInputWordToNet)
      CARBON_FREE_VEC(mInputWordToNet, NetTouchedPair*, mClosure.mExpectedStimuli.getUIntArraySize());
    
    if (mResponseWordToNet)
      CARBON_FREE_VEC(mResponseWordToNet, NetTouchedPair*, mClosure.mResponse.getUIntArraySize());

    for (NetTouchedPairVec::iterator p = mAllNetTouchedPairs.begin(),
           e = mAllNetTouchedPairs.end(); p != e; ++p)
      delete *p;

    if (mTouchedOffsetToWordOffset)
      CARBON_FREE_VEC(mTouchedOffsetToWordOffset, UInt32, mStimuliTouched.getNumClkAndStateOutputNets());

  }
  
  void ReplayPlayer::mapNetToInputBuffer(ShellNetPlayback* net, UInt32 bufferIndex, UInt32 numWords, 
                                         UInt32 isTristate, bool isClock, bool isStateOutput, UInt32 touchIndex)
  {
    ShellNetReplay::Touched* shadowTouch = mClosure.mStimuliTouched.allocTouchedAt(touchIndex, isClock || isStateOutput);
    
    NetTouchedPair* ntpNoMask = new NetTouchedPair(net, shadowTouch, false);
    mAllNetTouchedPairs.push_back(ntpNoMask);
    
    UInt32 index = bufferIndex;
    // value
    for (UInt32 i = 0; i < numWords; ++i, ++index)
    {
      ST_ASSERT(mInputWordToNet[index] == NULL, net->getNameAsLeaf());
      mInputWordToNet[index] = ntpNoMask;
    }
    // mask
    if (isStateOutput) {
      // Masks are only used for state outputs.  Create a special
      // NetTouchedPair to represent that this offset corresponds to a
      // mask, not a value.
      NetTouchedPair* ntpMask = new NetTouchedPair(net, shadowTouch, true);
      mAllNetTouchedPairs.push_back(ntpMask);
      for (UInt32 i = 0; i < numWords; ++i, ++index)
      {
        ST_ASSERT(mInputWordToNet[index] == NULL, net->getNameAsLeaf());
        mInputWordToNet[index] = ntpMask;
      }
    }
    // drive
    if (isTristate) {
      for (UInt32 i = 0; i < numWords; ++i, ++index)
      {
        ST_ASSERT(mInputWordToNet[index] == NULL, net->getNameAsLeaf());
        mInputWordToNet[index] = ntpNoMask;
      }
    }

    // For clocks and state outputs, map the touched index to a
    // corresponding word offset, so we can report divergences
    // correctly.  The first one is fine.
    if (isClock || isStateOutput) {
      mTouchedOffsetToWordOffset[touchIndex] = bufferIndex;
    }
  }
  
  void ReplayPlayer::mapNetToResponseBuffer(ShellNetPlayback* net, UInt32 bufferIndex, UInt32 numWords, UInt32 isTristate)
  {
    ShellNetReplay::Touched* touchedBuffer = net->getTouchedIndicator();
    // We can get the touched index from the player's touched buffer
    // in the net.  Remember to scale by the number of bits in the
    // touched object.
    UInt32 touchIndex = touchedBuffer->getBufferIndex();
    ShellNetReplay::Touched* shadowTouch = mClosure.mTouchedResponses.allocAt(touchIndex / ShellNetReplay::Touched::eBitsPerEntry);
    NetTouchedPair* ntp = new NetTouchedPair(net, shadowTouch, false);
    mAllNetTouchedPairs.push_back(ntp);
    
    // Tristates need double the space to account for drive
    UInt32 factor = 1 + isTristate;
    for (UInt32 i = bufferIndex; i < bufferIndex + (numWords * factor); ++i)
      mResponseWordToNet[i] = ntp;
  }
  

  void ReplayPlayer::addCModel(ReplayCModel* replayCModel)
  {
    mCModelManager->addCModel(replayCModel);
  }
  
  bool ReplayPlayer::isLastCModelEventDivergence() const
  {
    return mLastCModelEvent == getDivergencePoint();
  }

  CarbonStatus ReplayPlayer::setRecoverMode(RecoverContext& recoverContext)
  {
    INFO_ASSERT(! mRecoverEventFile.empty(), "Recovery event filename is empty.");
    
    initEventStatus();
    mStatus = eCarbon_OK;
    mUpdateTouchedStatus = true;
 
    // Don't print truncated file warnings during recovery.
    mClosure.mPrintTruncatedFileWarning = false;
    
    mCModelManager->setRecoverMode(recoverContext);
    
    mPlaybackStream.reopen(mRecoverEventFile, mStreamCB);
    return getStatus();
  }

  void ReplayPlayer::fillArrWithResponseBuffers(DynBVVec* valBufs, 
                                                DynBVVec* drvBufs) const
  {
    UInt32 numResponseNets = mPlayResponseNets.size();
    valBufs->resize(numResponseNets);
    drvBufs->resize(numResponseNets);
    for (UInt32 i = 0; i < numResponseNets; ++i) {
      const ShellNetPlaybackResponse* responseNet = mPlayResponseNets[i];
      DynBitVector* value = new DynBitVector(responseNet->getNumWords() * 32);
      (*valBufs)[i] = value;
      if (responseNet->isTristate()) {
        DynBitVector* drive = new DynBitVector(responseNet->getNumWords() * 32);
        (*drvBufs)[i] = drive;
      }
    }
  }
  
  
  void ReplayPlayer::cleanArrOfResponseBuffers(DynBVVec* valBufs, 
                                               DynBVVec* drvBufs) const
  {
    for (DynBVVec::iterator p = valBufs->begin(), e = valBufs->end();
         p != e; ++p)
      delete *p;
    for (DynBVVec::iterator p = drvBufs->begin(), e = drvBufs->end();
         p != e; ++p)
      delete *p;
    valBufs->resize(0);
    drvBufs->resize(0);
  }

  void ReplayPlayer::getNetValDrv(ShellNet* net, 
                                  DynBVVec* indexToValue,
                                  DynBVVec* indexToDrive,
                                  UInt32 i,
                                  DynBitVector** value,
                                  DynBitVector** drive) const
  {
    *value = (*indexToValue)[i];
    *drive = (*indexToDrive)[i];
    ST_ASSERT(*value, net->getName());
    
    UInt32* valueBuf = (*value)->getUIntArray();
    UInt32* driveBuf = NULL;
    if (*drive)
      driveBuf = (*drive)->getUIntArray();
    net->examine(valueBuf, driveBuf, ShellNet::eIDrive, mCarbonModel);
  }

  void ReplayPlayer::printNetValDrv(ShellNet* net, 
                                    DynBitVector* value,
                                    DynBitVector* drive) const
  {
    UtString name;
    net->getName()->compose(&name);
    UtIO::cout() << name << ":\n\tValue: ";
    value->printHex();
    if (drive) {
      UtIO::cout() << "\tDrive: ";
      drive->printHex();
    }
   }

  void ReplayPlayer::printNetValDrvArr(ShellNet* net, 
                                       const UInt32* value,
                                       const UInt32* drive,
                                       UInt32 numWords) const
  {
    DynBitVector valBV(numWords * 32, value, numWords);

    if (drive)
    {
      DynBitVector drvBV(numWords * 32, drive, numWords);
      printNetValDrv(net, &valBV, &drvBV);
    }
    else
      printNetValDrv(net, &valBV, NULL);
   }

  void ReplayPlayer::getPrintNetValDrv(ShellNet* net, 
                                       DynBVVec* indexToValue,
                                       DynBVVec* indexToDrive,
                                       UInt32 i) const
  {
    DynBitVector* value = NULL;
    DynBitVector* drive = NULL;
    getNetValDrv(net, indexToValue, indexToDrive, i, &value, &drive);
    printNetValDrv(net, value, drive);
 }
  
  void ReplayPlayer::printOrigResponseVals() const
  {
    DynBVVec indexToValue;
    DynBVVec indexToDrive;
    fillArrWithResponseBuffers(&indexToValue, &indexToDrive);

    UInt32 numResponseNets = mPlayResponseNets.size();
    for (UInt32 i = 0; i < numResponseNets; ++i)
    {
      ShellNetPlaybackResponse* responseNet = mPlayResponseNets[i];
      // get the original net
      ShellNet* origNet = responseNet->getNet();
      getPrintNetValDrv(origNet, &indexToValue, &indexToDrive, i);
    }

    cleanArrOfResponseBuffers(&indexToValue, &indexToDrive);
  }

  void ReplayPlayer::fillArrWithResponseUInts(UInt32PtrVec* indexToPlayValue,
                                              UInt32PtrVec* indexToPlayDrive) const
  {
    UInt32 numResponseNets = mPlayResponseNets.size();
    indexToPlayValue->resize(numResponseNets);
    indexToPlayDrive->resize(numResponseNets);
    for (UInt32 i = 0; i < numResponseNets; ++i)
    {
      ShellNetPlaybackResponse* responseNet = mPlayResponseNets[i];
      UInt32* val;
      UInt32* drv;
      responseNet->getModelValueBuffers(&val, &drv);
      (*indexToPlayValue)[i] = val;
      (*indexToPlayDrive)[i] = drv;
    }
  }

  void ReplayPlayer::printPlayResponseVals() const
  {

    UInt32PtrVec indexToPlayValue;
    UInt32PtrVec indexToPlayDrive;
    
    fillArrWithResponseUInts(&indexToPlayValue, &indexToPlayDrive);

    UInt32 numResponseNets = mPlayResponseNets.size();    
    for (UInt32 i = 0; i < numResponseNets; ++i)
    {
      ShellNetPlaybackResponse* responseNet = mPlayResponseNets[i];
      UInt32* playValue = indexToPlayValue[i];
      UInt32* playDrive = indexToPlayDrive[i];
      
      DynBitVector tmpDrive(responseNet->getWidth());
      if (playDrive)
      {
        memcpy(tmpDrive.getUIntArray(), playDrive, tmpDrive.getUIntArraySize() * sizeof(UInt32));
        tmpDrive.flip();
        playDrive = tmpDrive.getUIntArray();
      }
      
      printNetValDrvArr(responseNet, playValue, playDrive, responseNet->getNumWords());
    }
  }

  void ReplayPlayer::checkActualResponses(RecoverContext& recoverContext)
  {
    if (runEventWheel(eResponse))
    {
      UInt32 numResponseNets = mPlayResponseNets.size();
      bool isGood = true;
      for (UInt32 i = 0; i < numResponseNets; ++i)
      {
        ShellNetPlaybackResponse* responseNet = mPlayResponseNets[i];

        UInt32* playValue = recoverContext.mIndexToPlayValue[i];
        UInt32* playDrive = recoverContext.mIndexToPlayDrive[i];

        DynBitVector tmpDrive(responseNet->getWidth());
        if (playDrive)
        {
          memcpy(tmpDrive.getUIntArray(), playDrive, tmpDrive.getUIntArraySize() * sizeof(UInt32));
          // We save the inverted value of the idrive because codegen
          // forces us to. Need to flip it in order to do a compare.
          tmpDrive.flip();
          playDrive = tmpDrive.getUIntArray();
        }

        DynBitVector* origValue;
        DynBitVector* origDrive;
        ShellNet* origNet = responseNet->getNet();
        getNetValDrv(origNet, &recoverContext.mIndexToOrigValue, 
                     &recoverContext.mIndexToOrigDrive, i,
                     &origValue, &origDrive);
        
        if ((memcmp(origValue->getUIntArray(), playValue, origValue->getUIntArraySize() * sizeof(UInt32)) != 0) ||
            // should both either be NULL or not NULL
            ((origDrive == NULL) != (playDrive == NULL)) ||
            (origDrive && (memcmp(origDrive->getUIntArray(), playDrive, origDrive->getUIntArraySize() * sizeof(UInt32)))))
        {
          isGood = false;
          UtIO::cout() << "ACTUAL(" << i << "): ";
          printNetValDrv(origNet, origValue, origDrive);
          UtIO::cout() << "EXPECT(" << i << "): ";
          printNetValDrvArr(responseNet, playValue, playDrive, origValue->getUIntArraySize());
        }
      }
      
      INFO_ASSERT(isGood, "Recorded Response does not match actual response");
    }
  }

  void ReplayPlayer::saveCurrentStimuli(RecoverContext& recoverContext)
  {
    DynBitVector::sEqualSizeDynBVCopy(&mClosure.mExpectedStimuli, mPlaybackInputBuffer);
    mClosure.mStimuliTouched.copy(mStimuliTouched);
    
    recoverContext.saveExternalMems(&mDBExternalMems);
    recoverContext.saveCModelCall(mCModelManager->getCurrentCall());
        
    // This copies the current user state for replay.
    recoverContext.mCpClosure = mClosure;

    // Now add the response check buffers
    fillArrWithResponseBuffers(&recoverContext.mIndexToOrigValue, 
                               &recoverContext.mIndexToOrigDrive);
    fillArrWithResponseUInts(&recoverContext.mIndexToPlayValue, 
                             &recoverContext.mIndexToPlayDrive);    
  }

  void ReplayPlayer::restoreCurrentStimuli(RecoverContext& recoverContext)
  {
    setInputToGiven(recoverContext.mCpClosure.mExpectedStimuli, 
                    recoverContext.mCpClosure.mStimuliTouched);
    
    recoverContext.restoreExternalMems(&mDBExternalMems);

    // clean the response check buffers
    // only the arrays of DynBVs  need special treatment
    cleanArrOfResponseBuffers(&recoverContext.mIndexToOrigValue,
                              &recoverContext.mIndexToOrigDrive);

    recoverContext.mIndexToPlayValue.resize(0);
    recoverContext.mIndexToPlayDrive.resize(0);
  }


  void ReplayPlayer::reserveInputBuffers(UInt32 bitWidth)
  {
    mPlaybackInputBuffer.resize(bitWidth);
    // The expected stimuli buffer is the same size as the input
    // buffer
    mClosure.mExpectedStimuli.resize(bitWidth);
    
    INFO_ASSERT(mInputWordToNet == NULL, "mInputWordToNet already alloced.");
    mInputWordToNet = CARBON_ALLOC_VEC(NetTouchedPair*, mClosure.mExpectedStimuli.getUIntArraySize());
    memset(mInputWordToNet, 0, mClosure.mExpectedStimuli.getUIntArraySize() * sizeof(NetTouchedPair*));

    INFO_ASSERT(mTouchedOffsetToWordOffset == NULL, "mTouchedOffsetToWordOffset already alloced.");
    mTouchedOffsetToWordOffset = CARBON_ALLOC_VEC(UInt32, mStimuliTouched.getNumClkAndStateOutputNets());
    memset(mTouchedOffsetToWordOffset, 0, mStimuliTouched.getNumClkAndStateOutputNets() * sizeof(UInt32));

    // Allocate the closure StimuliTouched. Can do this by simply
    // using operator=
    mClosure.mStimuliTouched = mStimuliTouched;
    
    // Set the clock touch buffer to all ones since the first schedule
    // call assumes all clocks have fallen.
    mStimuliTouched.setAllClksAndStateOutputs();
  }
    
  void ReplayPlayer::resizeResponseBufferWidth(UInt32 bitWidth)
  {
    mClosure.mResponse.resize(bitWidth);
    
    INFO_ASSERT(mResponseWordToNet == NULL, "mResponseWordToNet already alloced.");
    mResponseWordToNet = CARBON_ALLOC_VEC(NetTouchedPair*, mClosure.mResponse.getUIntArraySize());
    memset(mResponseWordToNet, 0, mClosure.mResponse.getUIntArraySize() * sizeof(NetTouchedPair*));
  }
  
  void ReplayPlayer::reserveResponseNets(UInt32 numNets)
  {
    mPlayResponseNets.reserve(numNets);
    mClosure.mTouchedResponses.putSize(numNets);
    // Set this to all true for now, Real change detection for
    // response nets is not yet implemented in the recorder.
    mClosure.mTouchedResponses.setAll();

  }

  ShellNetReplay::Touched* ReplayPlayer::getStimuliTouchedAt(UInt32 index, bool isClock)
  {
    return mStimuliTouched.allocTouchedAt(index, isClock);
  }

  ShellNetReplay::Touched* ReplayPlayer::getTouchedResponse(UInt32 index)
  {
    return mClosure.mTouchedResponses.allocAt(index);
  }

  void ReplayPlayer::readStimulusTouchedSize(ZistreamDB& in)
  {
    mStimuliTouched.readSize(in);
  }

  // RecoverContext
  RecoverContext::RecoverContext(CarbonReplayInfo* replayInfo,
                                 CarbonHookup* hookup)
    : mDivergingCModel(NULL), mReplayInfo(replayInfo),
      mHookup(hookup), mReachedCModelDivergenceTime(false)
  {
    init();
  }
  
  void RecoverContext::init()
  {
    mDivergingCModel = NULL;
    mReachedCModelDivergenceTime = false;
  }


  void ReplayIStreamCB::reportEOF()
  {
    mPlayer->transitionNextEventFile();
  }

  void ReplayIStreamCB::reportError(const char* errMsg)
  {
    mPlayer->reportFileError(errMsg);
  }

  static const char* sSchedStr(const char scheduleType)
  {
    switch(scheduleType)
    {
    case scNormalSchedule:
      return "normal";
      break;
    case scClkSchedule:
      return "clock";
      break;
    case scDataSchedule:
      return "data";
      break;
    case scInitialSchedule:
      return "initial";
      break;
    case scAsyncSchedule:
      return "async";
      break;
    }
    return "(unknown)";
  }

  void ReplayPlayer::preRecover(const char scheduleType)
  {
    // First, see if we diverged due to the wrong schedule type.  If
    // so, we need to call saveRecoverEvents() - see bug 7490.  All
    // other divergence reasons do this as the divergence is detected,
    // but schedule type is checked with little overhead in
    // ReplayBOM::checkForDivergence().  We don't want to add an extra
    // branch there for performance reasons, so detect it here, once
    // we already know we've diverged.
    bool scheduleDiff = (scheduleType != mClosure.mScheduleType);
    if (scheduleDiff) {
      saveRecoverEvents();
    }

    // Now, possibly print the reason for divergence.
    //
    // In most cases, the reason for divergence will be reported when
    // it's detected.  However (as mentioned above) in some
    // situations, this requires an additional branch that may impact
    // performance.  In those cases, we print the reason here, after
    // all divergence checks have been done.
    UtString reason;

    if (!mDivergenceReported) {
      if (scheduleDiff) {
        // wrong schedule type was requested
        reason << "schedule type differed: actual=" << sSchedStr(scheduleType) << ", recorded=" << sSchedStr(mClosure.mScheduleType);
      } else if (mPlaybackStream.isStopped()) {
        // Other things that can stop the playback stream (e.g. cmodel
        // divergence) are reported elsewhere.  If we get here, it's
        // due to EOF.
        reason << "End of recorded data";
      } else {
        reason << "<unknown reason>";
      }

      reportDivergenceReason(reason.c_str());
    }
    mDivergenceReported = false;

  }

  void ReplayPlayer::reportDivergenceReason(const char *reason)
  {
    if (mCarbonModel->replayIsVerboseDivergence()) {
      mCarbonModel->getMsgContext()->SHLReplayDivergenceReason(mDBName->c_str(), reason);
    }
    mDivergenceReported = true;
  }

  void ReplayPlayer::identifyDivergentNets(const UInt32HashSet &divergentOffsets)
  {
    UtHashSet<STAliasedLeafNode*> coveredSet;
    // Loop over all the stimulus buffer offsets that diverged
    for (UInt32HashSet::SortedLoop l(divergentOffsets.loopSorted()); !l.atEnd(); ++l) {
      // Get the ST leaf corresponding to this offset
      UInt32 index = *l;
      NetTouchedPair *ntp = mInputWordToNet[index];
      STAliasedLeafNode *primLeaf = ntp->mNet->getNameAsLeaf();
      // This may be part of an expression net, in which case
      // the primitive isn't interesting to the user.
      // ShellSymTabBOM::composeName() will build a
      // user-friendly name in that case.  However, force
      // subordinates are a different story.  We need to look up
      // the original net in the DB.
      STAliasedLeafNode *reportLeaf = primLeaf;
      ShellDataBOM *bom = ShellSymTabBOM::getLeafBOM(primLeaf);
      if (bom->isForceSubordinate()) {
        // Ugh.  We don't maintain a map of force subordinates
        // to their masters in IODBRuntime, only in IODBNucleus.
        // For now, we'll loop over all forcible nets, checking
        // for a value/mask match.
        IODBRuntime *iodb = mCarbonModel->getHookup()->getDB();
        bool found = false;
        for (IODB::NameSetLoop l(iodb->loopForced()); !found && !l.atEnd(); ++l) {
          // Get the leaf and its BOM
          STSymbolTableNode *forceNode = *l;
          STAliasedLeafNode *forceLeaf = forceNode->castLeaf();
          ShellDataBOM *forceBOM = ShellSymTabBOM::getLeafBOM(forceLeaf);
          // Lookup the CarbonForceNet
          ShellGlobal::lockMutex();
          UInt32 id = mCarbonModel->getHookup()->getId();
          ShellNet *net = forceBOM->getShellData()->getCarbonNet(id);
          ShellGlobal::unlockMutex();
          CarbonForceNet *forceNet = net->castForceNet();
          INFO_ASSERT(forceNet, "Not a force net");
          // Get the value/mask leaves for the force net
          STAliasedLeafNode *valueLeaf = forceNet->getValueNet()->getNameAsLeaf();
          STAliasedLeafNode *maskLeaf = forceNet->getMaskNet()->getNameAsLeaf();
          // Check for either one matching the primitive that diverged
          if ((valueLeaf == primLeaf) || (maskLeaf == primLeaf)) {
            // Use the actual forcible node for reporting
            reportLeaf = forceLeaf;
            found = true;
          }
        }
        INFO_ASSERT(found, "Force master not found");
      }

      // Multiple primitives may have the same report leaf
      // (only force value and mask for now, but eventually we
      // may be smarter about reporting expression nets).  Only
      // report each one once.
      if (coveredSet.insertWithCheck(reportLeaf)) {
        UtString name;
        ShellSymTabBOM::composeName(reportLeaf, &name, false, false);
        UtString reason;
        reason << "Stimulus difference on net " << name;
        reportDivergenceReason(reason.c_str());
      }
    }
  }

  void ReplayPlayer::identifyDivergentMemories()
  {
    if (mExternalMemChanges.size() != mClosure.mExternalMemChangeIndices.size()) {
      // Make a temporary copy of all the actual memory changes
      SInt32HashSet extraChanges = mExternalMemChanges;
      // Go through all the expected writes.
      for (UInt32 i = 0; i < mClosure.mExternalMemChangeIndices.size(); ++i) {
        SInt32 index = mClosure.mExternalMemChangeIndices[i];
        // Try to erase this from the temporary copy of actual
        // changes.  If the erase fails, report that the
        // expected write didn't occur.
        if (extraChanges.erase(index) == 0) {
          ShellNetPlaybackMem *shellMem = mExternalPlaybackMems[index];
          const STAliasedLeafNode *memLeaf = shellMem->getNameAsLeaf();
          UtString name;
          ShellSymTabBOM::composeName(memLeaf, &name, false, false);
          UtString reason;
          reason << "Missing stimulus on memory " << name;
          reportDivergenceReason(reason.c_str());
        }
      }

      // We've erased all the expected writes.  Anything left is
      // unexpected, so report it.
      for (SInt32HashSet::SortedLoop p = extraChanges.loopSorted(); !p.atEnd(); ++p) {
        SInt32 index = *p;
        ShellNetPlaybackMem *shellMem = mExternalPlaybackMems[index];
        const STAliasedLeafNode *memLeaf = shellMem->getNameAsLeaf();
        UtString name;
        ShellSymTabBOM::composeName(memLeaf, &name, false, false);
        UtString reason;
        reason << "Unrecorded stimulus on memory " << name;
        reportDivergenceReason(reason.c_str());
      }
    }
  }

}
