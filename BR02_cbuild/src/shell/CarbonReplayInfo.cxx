// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "shell/CarbonReplayInfo.h"
#include "util/OSWrapper.h"
#include "shell/CarbonModel.h"
#include "util/ShellMsgContext.h"

CarbonReplayInfo::CarbonReplayInfo(CarbonModel* model) : 
  mCarbonModel(model), mDisableAllCheckpointCBs(false), mDirAction(eCarbonDirNoOverwrite),
  mMinSaveFrequency(0), mRecoverPercent(0), 
  mIsDirActionInit(false), mIsSaveFreqInit(false)
{
}

CarbonReplayInfo::~CarbonReplayInfo()
{
  // Free allocated callback objects
  for (ModeChangeCBArray::iterator iter = mModeChangeCBs.begin(); iter != mModeChangeCBs.end(); ++iter) {
    ReplayModeChangeCB *cb = *iter;
    delete cb;
  }
  for (CheckpointCBArray::iterator iter = mCheckpointCBs.begin(); iter != mCheckpointCBs.end(); ++iter) {
    ReplayCheckpointCB *cb = *iter;
    delete cb;
  }
}

CarbonStatus CarbonReplayInfo::checkDir(const char* dirName)
{
  CarbonStatus stat = eCarbon_OK;
  if (dirName == NULL)
  {
    mCarbonModel->getMsgContext()->SHLPathnameIsNULL();
    stat = eCarbon_ERROR;
  }
  return stat;
}

CarbonStatus CarbonReplayInfo::putDB(const char* systemName, const char* instanceName)
{
  CarbonStatus stat = checkDir(systemName);
  if (checkDir(instanceName) != eCarbon_OK)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
  {
    mSystemDir.assign(systemName);
    mInstanceDir.assign(instanceName);
  }
  return stat;
}

CarbonStatus CarbonReplayInfo::putWorkArea(const char* workArea)
{
  CarbonStatus stat = checkDir(workArea);
  if (stat == eCarbon_OK)
    mWorkArea.assign(workArea);
  return stat;
}

CarbonStatus CarbonReplayInfo::putDirAction(CarbonDirAction action)
{
  CarbonStatus stat = eCarbon_OK;

  mDirAction = action;
  
  mIsDirActionInit = false;
  switch(action)
  {
    case eCarbonDirOverwrite:
  case eCarbonDirNoOverwrite:
    mIsDirActionInit = true;
    break;
  }
  
  if (! mIsDirActionInit)
  {
    mCarbonModel->getMsgContext()->SHLInvalidDirAction((int) action);
    stat = eCarbon_ERROR;
  }
  
  return stat;
}

bool CarbonReplayInfo::assembleDBName(UtString* buf)
{
  if (! isSystemDirInit() || ! isInstanceDirInit())
    return false;

  buf->clear();
  OSConstructFilePath(buf, mSystemDir.c_str(), mInstanceDir.c_str());
  return true;
}

CarbonReplayCBDataID* CarbonReplayInfo::addModeChangeCB(CarbonModeChangeCBFunc fn, void* userData)
{
  ReplayModeChangeCB* cb = new ReplayModeChangeCB(fn, userData);
  mModeChangeCBs.push_back(cb);
  return cb;
}

CarbonReplayCBDataID* CarbonReplayInfo::addCheckpointCB(CarbonCheckpointCBFunc fn, void* userData)
{
  ReplayCheckpointCB* cb = new ReplayCheckpointCB(fn, userData);
  mCheckpointCBs.push_back(cb);
  return cb;
}

void CarbonReplayInfo::callModeChangeCBs(CarbonVHMMode prev, CarbonVHMMode next)
{
  // First call the Carbon Model's mode change function. Very important to do
  // this first. The Carbon Model needs to know what the next mode is in case
  // the user changes mode in their call (starts a record session
  // after a playback->normal).
  mCarbonModel->getHookup()->runModeChangeFn(prev, next);
  // Now call the user's mode change functions.
  for (ModeChangeCBArray::iterator iter = mModeChangeCBs.begin(); iter != mModeChangeCBs.end(); ++iter) {
    ReplayModeChangeCB *cb = *iter;
    if (cb->isEnabled()) {
      CarbonObjectID* obj = mCarbonModel->getObjectID();
      cb->run(obj, prev, next);
    }
  }
}

void CarbonReplayInfo::callCheckpointCBs(CarbonUInt64 numSchedCalls, 
                                          CarbonUInt32 checkPointNum)
{
  if (!mDisableAllCheckpointCBs) {
    for (CheckpointCBArray::iterator iter = mCheckpointCBs.begin(); iter != mCheckpointCBs.end(); ++iter) {
      ReplayCheckpointCB *cb = *iter;
      if (cb->isEnabled()) {
        CarbonObjectID* obj = mCarbonModel->getObjectID();
        cb->run(obj, numSchedCalls, checkPointNum);
      }
    }
  }
}

CarbonStatus CarbonReplayInfo::putSaveFrequency(UInt32 recoverPercent, 
                                                CarbonUInt64 minNumCalls)
{
  CarbonStatus ret = eCarbon_OK;
  if (recoverPercent > 100)
  {
    mCarbonModel->getMsgContext()->SHLReplayInvalidRecoverPercentage(recoverPercent);
    ret = eCarbon_ERROR;
  }
  else
  {
    mRecoverPercent = recoverPercent;
    mMinSaveFrequency = minNumCalls;
    mIsSaveFreqInit = true;
  }
  return ret;
}


ReplayModeChangeCB::ReplayModeChangeCB(CarbonModeChangeCBFunc fn, void *userData)
  : mCBFunc(fn),
    mCBData(userData)
{
}

ReplayModeChangeCB::~ReplayModeChangeCB()
{
}

void ReplayModeChangeCB::run(CarbonObjectID* context, CarbonVHMMode from, CarbonVHMMode to)
{
  // If the callback is enabled and has a valid function pointer, call
  // the function with the saved user data.
  if (mCBFunc != NULL) {
    (*mCBFunc)(context, mCBData, from, to);
  }
}

ReplayCheckpointCB::ReplayCheckpointCB(CarbonCheckpointCBFunc fn, void *userData)
  : mCBFunc(fn),
    mCBData(userData)
{
}

ReplayCheckpointCB::~ReplayCheckpointCB()
{
}

void ReplayCheckpointCB::run(CarbonObjectID* context, CarbonUInt64 totalScheduleCalls, CarbonUInt32 checkpointNumber)
{
  // If the callback is enabled and has a valid function pointer, call
  // the function with the saved user data.
  if (mCBFunc != NULL) {
    (*mCBFunc)(context, mCBData, totalScheduleCalls, checkpointNumber);
  }
}
