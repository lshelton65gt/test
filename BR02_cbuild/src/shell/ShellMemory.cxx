// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/ShellMemory.h"
#include "shell/ShellNetMacroDefine.h"
#include "shell/ShellGlobal.h"
#include "shell/CarbonModel.h"
#include "shell/ShellNetPlayback.h"
#include "util/UtConv.h"
#include "util/UtIOStream.h"
#include "hdl/ReadMemX.h"

#include "util/HashValue.h"
#include "util/UtHashMapFastIter.h"

template <typename T, typename PrimMem> void doPrimSyncPlaybackMem(ShellNetPlaybackMem* playMem, const PrimMem* me, 
                                                                   CarbonMemAddrT rightAddr, CarbonMemAddrT leftAddr,
                                                                   SInt32 msb, SInt32 lsb)
{
  CarbonMemAddrT startAddr = rightAddr;
  CarbonMemAddrT goalAddr = leftAddr;
  if (leftAddr < rightAddr)
  {
    startAddr = leftAddr;
    goalAddr = rightAddr;
  }
  
  UInt32 rowWidth = CBITWIDTH(msb, lsb);
  for (CarbonMemAddrT i = startAddr; i <= goalAddr; ++i)
  {
    T rowVal = me->getVal(i);
    if (rowVal != 0)
    {
      DynBitVector value(rowWidth);
      value = rowVal;
      playMem->backDoorWrite(i, &value);
    }
  }
}

ShellMemory::ShellMemory(SInt32 msb, SInt32 lsb,
                         CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr)
  : mLeftAddr(leftAddr), mRightAddr(rightAddr), mMsb(msb), mLsb(lsb), mCallbacks(NULL)
{}

ShellMemoryCBManager* ShellMemory::getShellMemoryCBManager()
{
  return mCallbacks;
}

ShellMemory::~ShellMemory()
{}

int ShellMemory::getRowBitWidth () const
{
  return 1 + CABS(mMsb - mLsb);
}

int ShellMemory::getBitWidth () const
{
  return 1 + CABS(mMsb - mLsb);
}

int ShellMemory::getRowNumUInt32s () const
{
  return CNUMWORDS(mMsb, mLsb);
}

int ShellMemory::getNumUInt32s () const
{
  return CNUMWORDS(mMsb, mLsb);
}

int ShellMemory::getRowLSB () const
{
  return mLsb;
}

int ShellMemory::getRowMSB () const
{
  return mMsb;
}

CarbonMemAddrT ShellMemory::getRightAddr() const
{
  return mRightAddr;
}

CarbonMemAddrT ShellMemory::getLeftAddr() const
{
  return mLeftAddr;
}

int ShellMemory::formatString(char* valueStr, size_t len, const UInt32* data, CarbonRadix strFormat) const
{
  // Format the string
  int numBits = getRowBitWidth();
  int ret = 0;
  switch(strFormat)
  {
  case eCarbonBin:
    ret = CarbonValRW::writeBinValToStr(valueStr, len, data, numBits);
    break;
  case eCarbonHex:
    ret = CarbonValRW::writeHexValToStr(valueStr, len, data, numBits);
    break;
  case eCarbonOct:
    ret = CarbonValRW::writeOctValToStr(valueStr, len, data, numBits);
    break;
  case eCarbonDec:
    ret = CarbonValRW::writeDecValToStr(valueStr, len, data, true, numBits);
    break;
  case eCarbonUDec:
    ret = CarbonValRW::writeDecValToStr(valueStr, len, data, false, numBits);
    break;
  }
  
  return ret;
}

int ShellMemory::formatString64(char* valueStr, size_t len, UInt64 data, CarbonRadix strFormat) const
{
  // Format the string
  int numBits = getRowBitWidth();
  int ret = 0;
  switch(strFormat)
  {
  case eCarbonBin:
    ret = CarbonValRW::writeBinValToStr(valueStr, len, &data, numBits);
    break;
  case eCarbonHex:
    ret = CarbonValRW::writeHexValToStr(valueStr, len, &data, numBits);
    break;
  case eCarbonOct:
    ret = CarbonValRW::writeOctValToStr(valueStr, len, &data, numBits);
    break;
  case eCarbonDec:
    ret = CarbonValRW::writeDecValToStr(valueStr, len, &data, true, numBits);
    break;
  case eCarbonUDec:
    ret = CarbonValRW::writeDecValToStr(valueStr, len, &data, false, numBits);
    break;
  }
  
  return ret;
}

const CarbonMemory* ShellMemory::castMemory() const
{
  return this;
}

const CarbonModelMemory* ShellMemory::castModelMemory() const
{
  return this;
}

const ShellNet* ShellMemory::castMemToShellNet() const
{
  return this;
}

  
bool ShellMemory::isVector() const
{
  return false;
}
    
bool ShellMemory::isScalar() const
{
  return false;
}
  
bool ShellMemory::isReal() const
{
  return false;
}
  
bool ShellMemory::isTristate() const
{
  return false;
}
  
void ShellMemory::fastDeposit(const UInt32*, const UInt32*, CarbonModel*)
{
}  

CarbonStatus ShellMemory::deposit(const UInt32*, const UInt32*, CarbonModel*)
{
  return eCarbon_ERROR;
}
  
CarbonStatus ShellMemory::depositWord(UInt32, int, UInt32, CarbonModel*)
{
  return eCarbon_ERROR;
}
  
CarbonStatus ShellMemory::depositRange(const UInt32*, int, int, const UInt32*, CarbonModel*)
{
  return eCarbon_ERROR;
}
  
bool ShellMemory::isDataNonZero() const
{
  return false;
}
  
bool ShellMemory::setToDriven(CarbonModel*)
{
  return false;
}
  
bool ShellMemory::setToUndriven(CarbonModel*)
{
  return false;
}
  
bool ShellMemory::setWordToUndriven(int, CarbonModel*)
{
  return false;
}
  
bool ShellMemory::resolveXdrive(CarbonModel*)
{
  return false;
}
  
void ShellMemory::getExternalDrive(UInt32*) const
{
}  

bool ShellMemory::setRangeToUndriven(int, int, CarbonModel*)
{
  return false;
}
  
CarbonStatus ShellMemory::examine(UInt32*, UInt32*, ExamineMode, CarbonModel*) const
{
  return eCarbon_ERROR;
}
  
CarbonStatus ShellMemory::examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const
{
  return eCarbon_ERROR;
}
  
CarbonStatus ShellMemory::examineWord(UInt32*, int, UInt32*, ExamineMode, CarbonModel*) const
{
  return eCarbon_ERROR;
}
  
CarbonStatus ShellMemory::examineRange(UInt32*, int, int, UInt32*, CarbonModel*) const
{
  return eCarbon_ERROR;
}
  
void ShellMemory::fastDepositWord(UInt32, int, UInt32, CarbonModel*)
{
}  
  
void ShellMemory::fastDepositRange(const UInt32*, int, int, const UInt32*, CarbonModel*)
{
}  
  
CarbonStatus ShellMemory::force(const UInt32*, CarbonModel*)
{
  return eCarbon_ERROR;
}
  
CarbonStatus ShellMemory::forceWord(UInt32, int, CarbonModel*)
{
  return eCarbon_ERROR;
}
  
CarbonStatus ShellMemory::forceRange(const UInt32*, int, int, CarbonModel*)
{
  return eCarbon_ERROR;
}
  
CarbonStatus ShellMemory::release(CarbonModel*)
{
  return eCarbon_ERROR;
}
  
CarbonStatus ShellMemory::releaseWord(int, CarbonModel*)
{
  return eCarbon_ERROR;
}
  
CarbonStatus ShellMemory::releaseRange(int, int, CarbonModel*)
{
  return eCarbon_ERROR;
}
  
ShellNet::Storage ShellMemory::allocShadow() const
{
  return NULL;
}
  
void ShellMemory::freeShadow(Storage*)
{
}  
  
void ShellMemory::update(Storage*) const
{
}  
  
ShellNet::ValueState ShellMemory::compare(const Storage) const
{
  return eUnchanged;
}
  
ShellNet::ValueState ShellMemory::writeIfNotEq(char*, size_t, Storage*, NetFlags)
{
  return eUnchanged;
}

CarbonStatus ShellMemory::format(char*, size_t, CarbonRadix, NetFlags, CarbonModel*) const
{
  return eCarbon_ERROR;
}
  
bool ShellMemory::isForcible() const
{
  return false;
}
  
bool ShellMemory::isInput() const
{
  return false;
}
  
const CarbonVectorBase* ShellMemory::castVector() const
{
  return NULL;
}
  
const CarbonScalarBase* ShellMemory::castScalar() const
{
  return NULL;
}

const ShellNetConstant* ShellMemory::castConstant() const
{
  return NULL;
}
  
void ShellMemory::putToZero(CarbonModel*)
{
}  
  
void ShellMemory::putToOnes(CarbonModel*)
{
}  
  
CarbonStatus ShellMemory::setRange(int, int, CarbonModel*)
{
  return eCarbon_ERROR;
}
  
CarbonStatus ShellMemory::clearRange(int, int, CarbonModel*)
{
  return eCarbon_ERROR;
}
  
int ShellMemory::hasDriveConflict() const
{
  return 0;
} 
 
int ShellMemory::hasDriveConflictRange(SInt32, SInt32) const
{
  return 0;
}  
  
CarbonStatus ShellMemory::examineValXDriveWord(UInt32*, UInt32*, int) const
{
  return eCarbon_ERROR;
}
  
void ShellMemory::setRawToUndriven(CarbonModel*)
{
}  
  
void ShellMemory::getTraits(Traits*) const
{
}  
  
ShellNet::ValueState ShellMemory::writeIfNotEqForce(char*, size_t, Storage*, NetFlags, ShellNet*)
{
  return eUnchanged;
}
  
CarbonStatus ShellMemory::formatForce(char*, size_t, CarbonRadix, NetFlags, ShellNet*, CarbonModel*) const
{
  return eCarbon_ERROR;
}
  
void ShellMemory::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  // Pass this down to the CarbonModelMemory, where it's actually
  // stored.  This is so it can be updated in
  // CarbonModelMemory::postMemoryWrite.
  CarbonModelMemory::putChangeArrayRef(changeArrayRef);
}  
  
CarbonChangeType* ShellMemory::getChangeArrayRef()
{
  return CarbonModelMemory::getChangeArrayRef();
}

const UInt32* ShellMemory::getControlMask() const
{
  return NULL;
}

void ShellMemory::sGetLowHiAddrs(CarbonMemAddrT left,
                                 CarbonMemAddrT right,
                                 CarbonMemAddrT* low,
                                 CarbonMemAddrT* hi)
{
  CarbonMemAddrT tmpLow = left;
  CarbonMemAddrT tmpHi =  right;
  if (tmpLow <= tmpHi)
  {
    *low = tmpLow;
    *hi = tmpHi;
  }
  else
  {
    *low = tmpHi;
    *hi = tmpLow;
  }
}


ShellMemory64x8::ShellMemory64x8(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, UInt8* mem)
  : ShellMemory(msb, lsb, leftAddr, rightAddr), mMemory(mem)
{}

ShellMemory64x8::~ShellMemory64x8()
{}

bool ShellMemory64x8::isSparse() const
{
  return false;
}

void* ShellMemory64x8::getStoragePtr(CarbonMemAddrT addr) const
{
  UInt8* ptr = 0;
  if (mRightAddr <= mLeftAddr)
    ptr = &mMemory[addr - mRightAddr];
  else
    ptr = &mMemory[mRightAddr - addr];
  return ptr;
}

UInt8 ShellMemory64x8::getVal(CarbonMemAddrT address) const
{
  UInt8 buf = 0;
  if (mRightAddr <= mLeftAddr)
    buf = mMemory[address - mRightAddr];
  else
    buf = mMemory[mRightAddr - address];
  return buf;
}

void ShellMemory64x8::setVal(CarbonMemAddrT address, UInt8 val)
{
  val &= CarbonValRW::getWordMask(CBITWIDTH(mMsb, mLsb));
  if (mRightAddr <= mLeftAddr)
    mMemory[address - mRightAddr] = val;
  else
    mMemory[mRightAddr - address] = val;
}

CarbonStatus ShellMemory64x8::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
    *buf = getVal(address);

  return stat;
}

UInt32 ShellMemory64x8::examineMemoryWord(CarbonMemAddrT address, int index) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, mModel);
  UInt32 buf = 0;

  if (stat == eCarbon_OK)
    stat = examineMemory(address, &buf);

  return buf;
}

CarbonStatus ShellMemory64x8::examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, 
                                                 int range_msb, 
                                                 int range_lsb) const
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  UInt32 src = 0;
  if (examineMemory(address, &src) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  if (stat == eCarbon_OK)
  {
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
   
    CarbonValRW::cpSrcRangeToDest(dst, &src, index, length);
  }
  return stat;
}

CarbonStatus ShellMemory64x8::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName());
  if (stat == eCarbon_OK)
  {
    setVal(address, *buf);
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellMemory64x8::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, mModel);
  if (depositMemory(address, &buf) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  return stat;
}

CarbonStatus ShellMemory64x8::depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb)
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;

  if (stat == eCarbon_OK)
  {
    UInt8 dst = getVal(address);
    
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    CarbonValRW::cpSrcToDestRange(&dst, src, index, length);
    setVal(address, dst);
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellMemory64x8::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  UInt32 data[1];
  CarbonStatus stat = examineMemory(address, data);
  
  if (stat == eCarbon_OK)
  {
    int ret = formatString(valueStr, len, data, strFormat);
    if (ret == -1)
    {
      ShellGlobal::reportInsufficientBufferLength(len, mModel);
      stat = eCarbon_ERROR;
    }
  }
  
  return stat;
}


void ShellMemory64x8::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  UInt32 buf = getVal(address);
  // largest possible bufsize for a uint8, 8 bytes + \n + terminator
  const size_t bufSize = 8 + 1 + 1;
  char valueStr[bufSize];
  int actualSize = formatString(valueStr, bufSize, &buf, strFormat);
  if (actualSize >= 0) {
    valueStr[actualSize] = '\n';
    valueStr[actualSize + 1] = '\0';
    out->writeStr(valueStr, actualSize + 1);
  }
}

CarbonStatus ShellMemory64x8::readmemfile(HDLReadMemX* readmemx)
{
  if (!readmemx->openFile())
    return eCarbon_ERROR;
  
  SInt64 address;
  UInt32 data;
  HDLReadMemXResult result;
  while ((result = readmemx->getNextWord(&address, &data)) == eRMValidData)
    setVal((CarbonMemAddrT) address, data);
  
  readmemx->closeFile();
  
  CarbonStatus stat = eCarbon_OK;
  if (result != eRMEnd)
    stat = eCarbon_ERROR;
  else
    postMemoryWrite();
  
  return stat;
}

CarbonStatus ShellMemory64x8::readmemh(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);
  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellMemory64x8::readmemb(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);
  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memb);
}

CarbonStatus ShellMemory64x8::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellMemory64x8::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memb);
}

void ShellMemory64x8::syncPlaybackMem(ShellNetPlaybackMem* playMem) const
{
  doPrimSyncPlaybackMem<UInt32, ShellMemory64x8>(playMem, this, mRightAddr, mLeftAddr, mMsb, mLsb);
}

ShellMemory64x16::ShellMemory64x16(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, UInt16* mem)
  : ShellMemory(msb, lsb, leftAddr, rightAddr), mMemory(mem)
{}

ShellMemory64x16::~ShellMemory64x16()
{}

bool ShellMemory64x16::isSparse() const
{
  return false;
}

void* ShellMemory64x16::getStoragePtr(CarbonMemAddrT addr) const
{
  UInt16* ptr = 0;
  if (mRightAddr <= mLeftAddr)
    ptr = &mMemory[addr - mRightAddr];
  else
    ptr = &mMemory[mRightAddr - addr];
  return ptr;
}

UInt16 ShellMemory64x16::getVal(CarbonMemAddrT address) const
{
  UInt16 buf = 0;
  if (mRightAddr <= mLeftAddr)
    buf = mMemory[address - mRightAddr];
  else
    buf = mMemory[mRightAddr - address];
  return buf;
}

void ShellMemory64x16::setVal(CarbonMemAddrT address, UInt16 val)
{
  val &= CarbonValRW::getWordMask(CBITWIDTH(mMsb, mLsb));
  if (mRightAddr <= mLeftAddr)
    mMemory[address - mRightAddr] = val;
  else
    mMemory[mRightAddr - address] = val;
}

CarbonStatus ShellMemory64x16::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
    *buf = getVal(address);

  return stat;
}

UInt32 ShellMemory64x16::examineMemoryWord(CarbonMemAddrT address, int index) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, mModel);
  UInt32 buf = 0;

  if (stat == eCarbon_OK)
    stat = examineMemory(address, &buf);

  return buf;
}

CarbonStatus ShellMemory64x16::examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, 
                                                 int range_msb, 
                                                 int range_lsb) const
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  UInt32 src = 0;
  if (examineMemory(address, &src) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  if (stat == eCarbon_OK)
  {
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
   
    CarbonValRW::cpSrcRangeToDest(dst, &src, index, length);
  }
  return stat;
}

CarbonStatus ShellMemory64x16::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName());
  if (stat == eCarbon_OK)
  {
    setVal(address, *buf);
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellMemory64x16::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, mModel);
  if (depositMemory(address, &buf) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  return stat;
}

CarbonStatus ShellMemory64x16::depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb)
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;

  if (stat == eCarbon_OK)
  {
    UInt16 dst = getVal(address);
    
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    CarbonValRW::cpSrcToDestRange(&dst, src, index, length);
    setVal(address, dst);
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellMemory64x16::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  UInt32 data[1];
  CarbonStatus stat = examineMemory(address, data);
  
  if (stat == eCarbon_OK)
  {
    int ret = formatString(valueStr, len, data, strFormat);
    if (ret == -1)
    {
      ShellGlobal::reportInsufficientBufferLength(len, mModel);
      stat = eCarbon_ERROR;
    }
  }
  
  return stat;
}


void ShellMemory64x16::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  UInt32 buf = getVal(address);
  // largest possible bufsize for a uint8, 8 bytes + \n + terminator
  const size_t bufSize = 16 + 1 + 1;
  char valueStr[bufSize];
  int actualSize = formatString(valueStr, bufSize, &buf, strFormat);
  if (actualSize >= 0) {
    valueStr[actualSize] = '\n';
    valueStr[actualSize + 1] = '\0';
    out->writeStr(valueStr, actualSize + 1);
  }
}

CarbonStatus ShellMemory64x16::readmemfile(HDLReadMemX* readmemx)
{
  if (!readmemx->openFile())
    return eCarbon_ERROR;
  
  SInt64 address;
  UInt32 data;
  HDLReadMemXResult result;
  while ((result = readmemx->getNextWord(&address, &data)) == eRMValidData)
    setVal((CarbonMemAddrT) address, data);
  
  readmemx->closeFile();
  
  CarbonStatus stat = eCarbon_OK;
  if (result != eRMEnd)
    stat = eCarbon_ERROR;
  else
    postMemoryWrite();
  
  return stat;
}

CarbonStatus ShellMemory64x16::readmemh(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);
  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellMemory64x16::readmemb(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memb);
}

CarbonStatus ShellMemory64x16::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellMemory64x16::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memb);
}

void ShellMemory64x16::syncPlaybackMem(ShellNetPlaybackMem* playMem) const
{
  doPrimSyncPlaybackMem<UInt32, ShellMemory64x16>(playMem, this, mRightAddr, mLeftAddr, mMsb, mLsb);
}


ShellMemory64x32::ShellMemory64x32(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, UInt32* mem)
  : ShellMemory(msb, lsb, leftAddr, rightAddr), mMemory(mem)
{}

ShellMemory64x32::~ShellMemory64x32()
{}

bool ShellMemory64x32::isSparse() const
{
  return false;
}

void* ShellMemory64x32::getStoragePtr(CarbonMemAddrT addr) const
{
  UInt32* ptr = 0;
  if (mRightAddr <= mLeftAddr)
    ptr = &mMemory[addr - mRightAddr];
  else
    ptr = &mMemory[mRightAddr - addr];
  return ptr;
}

UInt32 ShellMemory64x32::getVal(CarbonMemAddrT address) const
{
  UInt32 buf = 0;
  if (mRightAddr <= mLeftAddr)
    buf = mMemory[address - mRightAddr];
  else
    buf = mMemory[mRightAddr - address];
  return buf;
}

void ShellMemory64x32::setVal(CarbonMemAddrT address, UInt32 val)
{
  val &= CarbonValRW::getWordMask(CBITWIDTH(mMsb, mLsb));
  if (mRightAddr <= mLeftAddr)
    mMemory[address - mRightAddr] = val;
  else
    mMemory[mRightAddr - address] = val;
}

CarbonStatus ShellMemory64x32::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
    *buf = getVal(address);

  return stat;
}

UInt32 ShellMemory64x32::examineMemoryWord(CarbonMemAddrT address, int index) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, mModel);
  UInt32 buf = 0;

  if (stat == eCarbon_OK)
    stat = examineMemory(address, &buf);

  return buf;
}

CarbonStatus ShellMemory64x32::examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, 
                                                 int range_msb, 
                                                 int range_lsb) const
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  UInt32 src = 0;
  if (examineMemory(address, &src) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  if (stat == eCarbon_OK)
  {
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
   
    CarbonValRW::cpSrcRangeToDest(dst, &src, index, length);
  }
  return stat;
}

CarbonStatus ShellMemory64x32::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName());
  if (stat == eCarbon_OK)
  {
    setVal(address, *buf);
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellMemory64x32::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, mModel);
  if (depositMemory(address, &buf) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  return stat;
}

CarbonStatus ShellMemory64x32::depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb)
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;

  if (stat == eCarbon_OK)
  {
    UInt32 dst = getVal(address);
    
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    CarbonValRW::cpSrcToDestRange(&dst, src, index, length);
    setVal(address, dst);
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellMemory64x32::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  UInt32 data[1];
  CarbonStatus stat = examineMemory(address, data);
  
  if (stat == eCarbon_OK)
  {
    int ret = formatString(valueStr, len, data, strFormat);
    if (ret == -1)
    {
      ShellGlobal::reportInsufficientBufferLength(len, mModel);
      stat = eCarbon_ERROR;
    }
  }
  
  return stat;
}


void ShellMemory64x32::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  UInt32 buf = getVal(address);
  // largest possible bufsize for a uint8, 8 bytes + \n + terminator
  const size_t bufSize = 32 + 1 + 1;
  char valueStr[bufSize];
  int actualSize = formatString(valueStr, bufSize, &buf, strFormat);
  if (actualSize >= 0) {
    valueStr[actualSize] = '\n';
    valueStr[actualSize + 1] = '\0';
    out->writeStr(valueStr, actualSize + 1);
  }
}

CarbonStatus ShellMemory64x32::readmemfile(HDLReadMemX* readmemx)
{
  if (!readmemx->openFile())
    return eCarbon_ERROR;
  
  SInt64 address;
  UInt32 data;
  HDLReadMemXResult result;
  while ((result = readmemx->getNextWord(&address, &data)) == eRMValidData)
    setVal((CarbonMemAddrT) address, data);
  
  readmemx->closeFile();
  
  CarbonStatus stat = eCarbon_OK;
  if (result != eRMEnd)
    stat = eCarbon_ERROR;
  else
    postMemoryWrite();
  
  return stat;
}

CarbonStatus ShellMemory64x32::readmemh(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellMemory64x32::readmemb(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memb);
}

CarbonStatus ShellMemory64x32::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellMemory64x32::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memb);
}

void ShellMemory64x32::syncPlaybackMem(ShellNetPlaybackMem* playMem) const
{
  doPrimSyncPlaybackMem<UInt32, ShellMemory64x32>(playMem, this, mRightAddr, mLeftAddr, mMsb, mLsb);
}



ShellMemory64x64::ShellMemory64x64(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, UInt64* mem)
  : ShellMemory(msb, lsb, leftAddr, rightAddr), mMemory(mem)
{}

ShellMemory64x64::~ShellMemory64x64()
{}

bool ShellMemory64x64::isSparse() const
{
  return false;
}

void* ShellMemory64x64::getStoragePtr(CarbonMemAddrT addr) const
{
  UInt64* ptr = 0;
  if (mRightAddr <= mLeftAddr)
    ptr = &mMemory[addr - mRightAddr];
  else
    ptr = &mMemory[mRightAddr - addr];
  return ptr;
}

UInt64 ShellMemory64x64::getVal(CarbonMemAddrT address) const
{
  UInt64 buf = 0;
  if (mRightAddr <= mLeftAddr)
    buf = mMemory[address - mRightAddr];
  else
    buf = mMemory[mRightAddr - address];
  return buf;
}

void ShellMemory64x64::setVal(CarbonMemAddrT address, const UInt32* val)
{
  int index;
  if (mRightAddr <= mLeftAddr)
    index = address - mRightAddr;
  else
    index = mRightAddr - address;

  UInt64 dst = mMemory[index];
  CarbonValRW::cpSrcToDest(&dst, val, CNUMWORDS(mMsb, mLsb));
  dst &= CarbonValRW::getWordMaskLL(CBITWIDTH(mMsb, mLsb));
  mMemory[index] = dst;
}

void ShellMemory64x64::setValPrim(CarbonMemAddrT address, UInt64 val)
{
  val &= CarbonValRW::getWordMaskLL(CBITWIDTH(mMsb, mLsb));
  if (mRightAddr <= mLeftAddr)
    mMemory[address - mRightAddr] = val;
  else
    mMemory[mRightAddr - address] = val;
}

CarbonStatus ShellMemory64x64::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
  {
    UInt64 src = getVal(address);
    CarbonValRW::cpSrcToDest(buf, &src, 2);
  }
  
  return stat;
}

UInt32 ShellMemory64x64::examineMemoryWord(CarbonMemAddrT address, int index) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, mModel);
  UInt32 buf[2];
  UInt32 ret = 0;

  if (stat == eCarbon_OK)
  {
    stat = examineMemory(address, buf);
    ret = buf[index];
  }
  
  return ret;
}

CarbonStatus ShellMemory64x64::examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, 
                                                  int range_msb, 
                                                  int range_lsb) const
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  UInt32 src[2];
  if (examineMemory(address, src) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  if (stat == eCarbon_OK)
  {
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    
    CarbonValRW::cpSrcRangeToDest(dst, src, index, length);
  }
  return stat;
}

CarbonStatus ShellMemory64x64::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName());
  if (stat == eCarbon_OK)
  {
    setVal(address, buf);
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellMemory64x64::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, mModel);
  if (stat == eCarbon_OK)
  {
    UInt64 dst = getVal(address);
    CarbonValRW::cpSrcWordToDest(&dst, buf, index);
    setValPrim(address, dst);
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellMemory64x64::depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb)
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;

  if (stat == eCarbon_OK)
  {
    UInt64 dst = getVal(address);
    
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    CarbonValRW::cpSrcToDestRange(&dst, src, index, length);
    setValPrim(address, dst);
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellMemory64x64::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  UInt32 data[2];
  CarbonStatus stat = examineMemory(address, data);
  
  if (stat == eCarbon_OK)
  {
    int ret = formatString(valueStr, len, data, strFormat);
    if (ret == -1)
    {
      ShellGlobal::reportInsufficientBufferLength(len, mModel);
      stat = eCarbon_ERROR;
    }
  }
  
  return stat;
}


void ShellMemory64x64::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  UInt64 buf = getVal(address);

  // largest possible bufsize for a uint8, 8 bytes + \n + terminator
  const size_t bufSize = 64 + 1 + 1;
  char valueStr[bufSize];
  int actualSize = formatString64(valueStr, bufSize, buf, strFormat);
  if (actualSize >= 0) {
    valueStr[actualSize] = '\n';
    valueStr[actualSize + 1] = '\0';
    out->writeStr(valueStr, actualSize + 1);
  }
}

CarbonStatus ShellMemory64x64::readmemfile(HDLReadMemX* readmemx)
{
  if (!readmemx->openFile())
    return eCarbon_ERROR;
  
  SInt64 address;
  UInt32 data[2];
  HDLReadMemXResult result;
  while ((result = readmemx->getNextWord(&address, data)) == eRMValidData)
    setVal((CarbonMemAddrT) address, data);
  
  readmemx->closeFile();
  
  CarbonStatus stat = eCarbon_OK;
  if (result != eRMEnd)
    stat = eCarbon_ERROR;
  else
    postMemoryWrite();
  
  return stat;
}

CarbonStatus ShellMemory64x64::readmemh(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellMemory64x64::readmemb(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memb);
}

CarbonStatus ShellMemory64x64::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellMemory64x64::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memb);
}

void ShellMemory64x64::syncPlaybackMem(ShellNetPlaybackMem* playMem) const
{
  doPrimSyncPlaybackMem<UInt64, ShellMemory64x64>(playMem, this, mRightAddr, mLeftAddr, mMsb, mLsb);
}

ShellMemory64xA::ShellMemory64xA(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, 
                                 CarbonMemReadRowFn readRow, CarbonMemWriteRowFn writeRow,
                                 CarbonMemReadRowWordFn readRowWord, CarbonMemWriteRowWordFn writeRowWord,
                                 CarbonMemReadRowRangeFn readRowRange, CarbonMemWriteRowRangeFn writeRowRange,
                                 CarbonMemRowStoragePtrFn rowStoragePtrFn,
                                 void* mem)
  : ShellMemory(msb, lsb, leftAddr, rightAddr), 
    mReadRowFn(readRow), mWriteRowFn(writeRow),
    mReadRowWordFn(readRowWord), mWriteRowWordFn(writeRowWord),
    mReadRowRangeFn(readRowRange), mWriteRowRangeFn(writeRowRange),
    mRowStoragePtrFn(rowStoragePtrFn),
    mMemory(mem)
{}

ShellMemory64xA::~ShellMemory64xA()
{}

bool ShellMemory64xA::isSparse() const
{
  // This class is used for both sparse and non-sparse memories whose
  // word width is greater than 64 bits.  To distinguish, check the
  // function pointer that will give a raw storage pointer to the
  // memory data at a particular address.  That pointer is only
  // defined for non-sparse memories.
  return (mRowStoragePtrFn == NULL);
}

void* ShellMemory64xA::getStoragePtr(CarbonMemAddrT addr) const
{
  INFO_ASSERT(mRowStoragePtrFn != NULL, "Row storage lookup function is NULL");
  void* ptr = (*mRowStoragePtrFn)(addr, mMemory);
  return ptr;
}

CarbonStatus ShellMemory64xA::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
    (*mReadRowFn)(address, buf, mMemory);
  
  return stat;
}

UInt32 ShellMemory64xA::examineMemoryWord(CarbonMemAddrT address, int index) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, CNUMWORDS(mMsb, mLsb) - 1, mModel);
  UInt32 buf = 0;
  
  if (stat == eCarbon_OK)
  {
    stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
    if (stat == eCarbon_OK)
      (*mReadRowWordFn)(address, &buf, index, mMemory);
  }
  
  return buf;
}

CarbonStatus ShellMemory64xA::examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, 
                                                 int range_msb, 
                                                 int range_lsb) const
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;

  if (stat == eCarbon_OK)
  {
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    
    (*mReadRowRangeFn)(address, dst, index, length, mMemory);
  }
  return stat;
}

CarbonStatus ShellMemory64xA::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName());
  if (stat == eCarbon_OK)
  {
    (*mWriteRowFn)(address, buf, mMemory);
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellMemory64xA::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, CNUMWORDS(mMsb, mLsb) - 1, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
  {
    (*mWriteRowWordFn)(address, &buf, index, mMemory);
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellMemory64xA::depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb)
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;

  if (stat == eCarbon_OK)
  {
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);

    (*mWriteRowRangeFn)(address, src, index, length, mMemory);
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellMemory64xA::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
  {
    UInt32 numWords = CNUMWORDS(mMsb, mLsb);
    UInt32* data = CARBON_ALLOC_VEC(UInt32, numWords);
    (*mReadRowFn)(address, data, mMemory);

    int ret = formatString(valueStr, len, data, strFormat);
    if (ret == -1)
    {
      ShellGlobal::reportInsufficientBufferLength(len, mModel);
      stat = eCarbon_ERROR;
    }
    CARBON_FREE_VEC(data, UInt32, numWords);
  }
  
  return stat;
}


void ShellMemory64xA::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  UInt32 numWords = CNUMWORDS(mMsb, mLsb);
  UInt32* buf = CARBON_ALLOC_VEC(UInt32, numWords);
  (*mReadRowFn)(address, buf, mMemory);
  
  int bufSize = getRowBitWidth() + 2;
  char* valueStr = CARBON_ALLOC_VEC(char, bufSize);
  int actualSize = formatString(valueStr, bufSize, buf, strFormat);
  if (actualSize >= 0) {
    valueStr[actualSize] = '\n';
    valueStr[actualSize + 1] = '\0';
    out->writeStr(valueStr, actualSize + 1);
  }
  CARBON_FREE_VEC(valueStr, char, bufSize);
  CARBON_FREE_VEC(buf, UInt32, numWords);
}

CarbonStatus ShellMemory64xA::readmemfile(HDLReadMemX* readmemx)
{
  if (!readmemx->openFile())
    return eCarbon_ERROR;
  
  SInt64 address;
  UInt32 numWords = getRowNumUInt32s();
  UInt32* data = CARBON_ALLOC_VEC(UInt32, numWords);
  HDLReadMemXResult result;
  while ((result = readmemx->getNextWord(&address, data)) == eRMValidData)
    (*mWriteRowFn)((CarbonMemAddrT) address, data, mMemory);
  
  CARBON_FREE_VEC(data, UInt32, numWords);

  readmemx->closeFile();
  
  CarbonStatus stat = eCarbon_OK;
  if (result != eRMEnd)
    stat = eCarbon_ERROR;
  else
    postMemoryWrite();
  
  return stat;
}

CarbonStatus ShellMemory64xA::readmemh(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellMemory64xA::readmemb(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memb);
}

CarbonStatus ShellMemory64xA::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellMemory64xA::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memb);
}

void ShellMemory64xA::syncPlaybackMem(ShellNetPlaybackMem* playMem) const
{
  CarbonMemAddrT startAddr = mRightAddr;
  CarbonMemAddrT goalAddr = mLeftAddr;
  if (mLeftAddr < mRightAddr)
  {
    startAddr = mLeftAddr;
    goalAddr = mRightAddr;
  }
  
  UInt32 rowWidth = CBITWIDTH(mMsb, mLsb);
  DynBitVector rowValue(rowWidth);
  UInt32* rowBuf = rowValue.getUIntArray();
  for (CarbonMemAddrT i = startAddr; i <= goalAddr; ++i)
  {
    (*mReadRowFn)(i, rowBuf, mMemory);
    if (! CarbonValRW::isZero(rowBuf, rowValue.numWords()))
      playMem->backDoorWrite(i, &rowValue);
  }
}

static int sZeroFormatString(char* valueStr, size_t len, 
                             CarbonRadix strFormat, int width)
{
  int adjustedWidth = width;
  int ret = -1;
  switch (strFormat)
  {
  case eCarbonBin:
    break;
  case eCarbonHex:
    adjustedWidth = (width + 3)/4;
    break;
  case eCarbonOct:
    adjustedWidth = (width + 2)/3;
    break;
  case eCarbonDec:
  case eCarbonUDec:
    adjustedWidth = 1;
    break;
  }
  
  if (len > (unsigned) width)
  {
    ret = adjustedWidth;
    for (int i = 0; i < adjustedWidth; ++i)
      valueStr[i] = '0';
    valueStr[width] = '\0';
  }
  
  return ret;
}


ShellSparseMemory32Key::ShellSparseMemory32Key(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, void* mem) 
  : ShellMemory(msb, lsb, leftAddr, rightAddr), mTable(mem)
{}

ShellSparseMemory32Key::~ShellSparseMemory32Key()
{}

bool ShellSparseMemory32Key::isSparse() const
{
  return true;
}

void* ShellSparseMemory32Key::getStoragePtr(CarbonMemAddrT) const
{
  INFO_ASSERT(0, "getStoragePtr() is invalid for sparse memories");
  return NULL;
}

template<typename DataType>
const DataType* ShellSparseMemory32Key::getRow(CarbonMemAddrT address) const {
  typedef UtHashMapFastIter<CarbonMemAddrT,DataType> Mem;
  const Mem* mem = (const Mem*) mTable;
  const typename Mem::MapEntry* mapEntry = mem->findEntry(address);
  const DataType* row = NULL;
  if (mapEntry != NULL) {
    row = &mapEntry->mKeyVal.second;
  }
  return row;
}

template<typename DataType>
DataType* ShellSparseMemory32Key::getRowOrCreate(CarbonMemAddrT address) {
  typedef UtHashMapFastIter<CarbonMemAddrT,DataType> Mem;
  typename Mem::MapEntry* mapEntry;
  Mem* mem = (Mem*) mTable;
  if (mem->insertUninitialized(address, &mapEntry)) {
    mapEntry->mKeyVal.first = address;
    mapEntry->mKeyVal.second = 0;
  }
  return &mapEntry->mKeyVal.second;
}

template<typename DataType>
void ShellSparseMemory32Key::doSyncPlaybackMem(ShellNetPlaybackMem* playMem) const
{
  typedef UtHashMapFastIter<CarbonMemAddrT,DataType> Mem;
  const Mem* mem = (const Mem*) mTable;
  const UInt32 numRowWords = CNUMWORDS(mMsb, mLsb);
  const UInt32 bitWidth = CBITWIDTH(mMsb, mLsb);
  for (typename Mem::UnsortedCLoop p = mem->loopCUnsorted(); ! p.atEnd(); ++p)
  {
    const DataType row = p.getValue();
    if (row != 0)
    {
      DynBitVector value(bitWidth);
      UInt32* valueBuf = value.getUIntArray();
      CarbonValRW::cpSrcToDest(valueBuf, &row, numRowWords);
      
      CarbonMemAddrT address = p.getKey();
      playMem->backDoorWrite(address, &value);
    }
  }
}

ShellSparseMemory32x8::ShellSparseMemory32x8(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, void* mem) 
  : ShellSparseMemory32Key(msb, lsb, leftAddr, rightAddr, mem)
{}

ShellSparseMemory32x8::~ShellSparseMemory32x8()
{}

CarbonStatus ShellSparseMemory32x8::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
  {
    const UInt8* row = getRow<UInt8>(address);
    if (row)
      *buf = *row;
    else
      *buf = 0;
  }
  return stat;
}

UInt32 ShellSparseMemory32x8::examineMemoryWord(CarbonMemAddrT address, 
                                                int index) const
{
CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, mModel);

  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  

  UInt32 buf = 0;
  if (stat == eCarbon_OK)
  {
    const UInt8* row = getRow<UInt8>(address);
    if (row)
      buf = *row;
  }
  
  return buf;
}

CarbonStatus ShellSparseMemory32x8::examineMemoryRange(CarbonMemAddrT address, 
                                                       UInt32 *dst, 
                                                       int range_msb, 
                                                       int range_lsb) const
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
  {
    const UInt8* src = getRow<UInt8>(address);
    if (src)
    {
      size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
      size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
      
      CarbonValRW::cpSrcRangeToDest(dst, src, index, length);
    }
    else
      CarbonValRW::setToZero(dst, 2);
  }
  return stat;
}

void ShellSparseMemory32x8::setVal(CarbonMemAddrT address, UInt8 val)
{
  if ( ( 0 != val ) || ( NULL != getRow<UInt8>(address) ) ) {
    // actually store the value only if it is non-zero or this address has already been written
    UInt8* row = getRowOrCreate<UInt8>(address);
    *row = val & CarbonValRW::getWordMask(CBITWIDTH(mMsb, mLsb));
  }
  postMemoryWrite();
}

CarbonStatus ShellSparseMemory32x8::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
    setVal(address, *buf);
  return stat;
}

CarbonStatus ShellSparseMemory32x8::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, mModel);
  
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
    setVal(address, buf);
  return stat;
}

CarbonStatus ShellSparseMemory32x8::depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb)
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
  {
    UInt8* row = getRowOrCreate<UInt8>(address);
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    CarbonValRW::cpSrcToDestRange(row, src, index, length);
    postMemoryWrite();
  }

  return stat;
}

CarbonStatus ShellSparseMemory32x8::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
  {
    const UInt8* data = getRow<UInt8>(address);
    int ret = -1;
    if (data)
      ret = formatString64(valueStr, len, *data, strFormat);
    else 
      ret = sZeroFormatString(valueStr, len, strFormat, getRowBitWidth());
    
    if (ret == -1)
    {
      ShellGlobal::reportInsufficientBufferLength(len, mModel);
      stat = eCarbon_ERROR;
    }
  }
  
  return stat;
}

void ShellSparseMemory32x8::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  const UInt8* buf = getRow<UInt8>(address);
  int dataWidth = getRowBitWidth();
  int bufSize = dataWidth + 2;
  char* valueStr = CARBON_ALLOC_VEC(char, bufSize);
  int actualSize = 0;
  if (buf)
    actualSize = formatString64(valueStr, bufSize, *buf, strFormat);
  else
    actualSize = sZeroFormatString(valueStr, bufSize, strFormat, dataWidth);
  if (actualSize >= 0) {
    valueStr[actualSize] = '\n';
    valueStr[actualSize + 1] = '\0';
    out->writeStr(valueStr, actualSize + 1);
  }
  CARBON_FREE_VEC(valueStr, char, bufSize);
}

CarbonStatus ShellSparseMemory32x8::readmemh(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellSparseMemory32x8::readmemb(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memb);
}

CarbonStatus ShellSparseMemory32x8::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellSparseMemory32x8::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memb);
}


void ShellSparseMemory32x8::syncPlaybackMem(ShellNetPlaybackMem* playMem) const
{
  doSyncPlaybackMem<UInt8>(playMem);
}

CarbonStatus ShellSparseMemory32x8::readmemfile(HDLReadMemX* readmemx)
{
  if (!readmemx->openFile())
    return eCarbon_ERROR;
  
  SInt64 address;
  UInt32 data;
  HDLReadMemXResult result;
  while ((result = readmemx->getNextWord(&address, &data)) == eRMValidData)
  {
    UInt8* row = getRowOrCreate<UInt8>(address);
    *row = data;
  }
  
  readmemx->closeFile();
  
  CarbonStatus stat = eCarbon_OK;
  if (result != eRMEnd)
    stat = eCarbon_ERROR;
  else
    postMemoryWrite();
  
  return stat;
}

ShellSparseMemory32x16::ShellSparseMemory32x16(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, void* mem) 
  : ShellSparseMemory32Key(msb, lsb, leftAddr, rightAddr, mem)
{}

ShellSparseMemory32x16::~ShellSparseMemory32x16()
{}

CarbonStatus ShellSparseMemory32x16::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
  {
    const UInt16* row = getRow<UInt16>(address);
    if (row)
      *buf = *row;
    else
      *buf = 0;
  }
  return stat;
}

UInt32 ShellSparseMemory32x16::examineMemoryWord(CarbonMemAddrT address, 
                                                int index) const
{
CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, mModel);

  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  

  UInt32 buf = 0;
  if (stat == eCarbon_OK)
  {
    const UInt16* row = getRow<UInt16>(address);
    if (row)
      buf = *row;
  }
  
  return buf;
}

CarbonStatus ShellSparseMemory32x16::examineMemoryRange(CarbonMemAddrT address, 
                                                       UInt32 *dst, 
                                                       int range_msb, 
                                                       int range_lsb) const
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
  {
    const UInt16* src = getRow<UInt16>(address);
    if (src)
    {
      size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
      size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
      
      CarbonValRW::cpSrcRangeToDest(dst, src, index, length);
    }
    else
      CarbonValRW::setToZero(dst, 2);
  }
  return stat;
}

void ShellSparseMemory32x16::setVal(CarbonMemAddrT address, UInt16 val)
{
  if ( ( 0 != val ) || ( NULL != getRow<UInt16>(address) ) ) {
    // actually store the value only if it is non-zero or this address has already been written
    UInt16* row = getRowOrCreate<UInt16>(address);
    *row = val & CarbonValRW::getWordMask(CBITWIDTH(mMsb, mLsb));
  }
  postMemoryWrite();
}

CarbonStatus ShellSparseMemory32x16::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
    setVal(address, *buf);
  return stat;
}

CarbonStatus ShellSparseMemory32x16::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, mModel);
  
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
    setVal(address, buf);
  return stat;
}

CarbonStatus ShellSparseMemory32x16::depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb)
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
  {
    UInt16* row = getRowOrCreate<UInt16>(address);
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    CarbonValRW::cpSrcToDestRange(row, src, index, length);
    postMemoryWrite();
  }

  return stat;
}

CarbonStatus ShellSparseMemory32x16::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
  {
    const UInt16* data = getRow<UInt16>(address);
    int ret = -1;
    if (data)
      ret = formatString64(valueStr, len, *data, strFormat);
    else 
      ret = sZeroFormatString(valueStr, len, strFormat, getRowBitWidth());
    
    if (ret == -1)
    {
      ShellGlobal::reportInsufficientBufferLength(len, mModel);
      stat = eCarbon_ERROR;
    }
  }
  
  return stat;
}

void ShellSparseMemory32x16::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  const UInt16* buf = getRow<UInt16>(address);
  int dataWidth = getRowBitWidth();
  int bufSize = dataWidth + 2;
  char* valueStr = CARBON_ALLOC_VEC(char, bufSize);
  int actualSize = 0;
  if (buf)
    actualSize = formatString64(valueStr, bufSize, *buf, strFormat);
  else
    actualSize = sZeroFormatString(valueStr, bufSize, strFormat, dataWidth);
  if (actualSize >= 0) {
    valueStr[actualSize] = '\n';
    valueStr[actualSize + 1] = '\0';
    out->writeStr(valueStr, actualSize + 1);
  }
  CARBON_FREE_VEC(valueStr, char, bufSize);
}

CarbonStatus ShellSparseMemory32x16::readmemh(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellSparseMemory32x16::readmemb(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memb);
}

CarbonStatus ShellSparseMemory32x16::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellSparseMemory32x16::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memb);
}

void ShellSparseMemory32x16::syncPlaybackMem(ShellNetPlaybackMem* playMem) const
{
  doSyncPlaybackMem<UInt16>(playMem);
}


CarbonStatus ShellSparseMemory32x16::readmemfile(HDLReadMemX* readmemx)
{
  if (!readmemx->openFile())
    return eCarbon_ERROR;
  
  SInt64 address;
  UInt32 data;
  HDLReadMemXResult result;
  while ((result = readmemx->getNextWord(&address, &data)) == eRMValidData)
  {
    UInt16* row = getRowOrCreate<UInt16>(address);
    *row = data;
  }
  
  readmemx->closeFile();
  
  CarbonStatus stat = eCarbon_OK;
  if (result != eRMEnd)
    stat = eCarbon_ERROR;
  else
    postMemoryWrite();
  
  return stat;
}

ShellSparseMemory32x32::ShellSparseMemory32x32(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, void* mem) 
  : ShellSparseMemory32Key(msb, lsb, leftAddr, rightAddr, mem)
{}

ShellSparseMemory32x32::~ShellSparseMemory32x32()
{}

CarbonStatus ShellSparseMemory32x32::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
  {
    const UInt32* row = getRow<UInt32>(address);
    if (row)
      *buf = *row;
    else
      *buf = 0;
  }
  return stat;
}

UInt32 ShellSparseMemory32x32::examineMemoryWord(CarbonMemAddrT address, 
                                                int index) const
{
CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, mModel);

  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  

  UInt32 buf = 0;
  if (stat == eCarbon_OK)
  {
    const UInt32* row = getRow<UInt32>(address);
    if (row)
      buf = *row;
  }
  
  return buf;
}

CarbonStatus ShellSparseMemory32x32::examineMemoryRange(CarbonMemAddrT address, 
                                                       UInt32 *dst, 
                                                       int range_msb, 
                                                       int range_lsb) const
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
  {
    const UInt32* src = getRow<UInt32>(address);
    if (src)
    {
      size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
      size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
      
      CarbonValRW::cpSrcRangeToDest(dst, src, index, length);
    }
    else
      CarbonValRW::setToZero(dst, 2);
  }
  return stat;
}

void ShellSparseMemory32x32::setVal(CarbonMemAddrT address, UInt32 val)
{
  if ( ( 0 != val ) || ( NULL != getRow<UInt32>(address) ) ) {
    // actually store the value only if it is non-zero or this address has already been written
    UInt32* row = getRowOrCreate<UInt32>(address);
    *row = val & CarbonValRW::getWordMask(CBITWIDTH(mMsb, mLsb));
  }
  postMemoryWrite();
}

CarbonStatus ShellSparseMemory32x32::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
    setVal(address, *buf);
  return stat;
}

CarbonStatus ShellSparseMemory32x32::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, mModel);
  
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
    setVal(address, buf);
  return stat;
}

CarbonStatus ShellSparseMemory32x32::depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb)
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
  {
    UInt32* row = getRowOrCreate<UInt32>(address);
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    CarbonValRW::cpSrcToDestRange(row, src, index, length);
    postMemoryWrite();
  }

  return stat;
}

CarbonStatus ShellSparseMemory32x32::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
  {
    const UInt32* data = getRow<UInt32>(address);
    int ret = -1;
    if (data)
      ret = formatString64(valueStr, len, *data, strFormat);
    else 
      ret = sZeroFormatString(valueStr, len, strFormat, getRowBitWidth());
    
    if (ret == -1)
    {
      ShellGlobal::reportInsufficientBufferLength(len, mModel);
      stat = eCarbon_ERROR;
    }
  }
  
  return stat;
}

void ShellSparseMemory32x32::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  const UInt32* buf = getRow<UInt32>(address);
  int dataWidth = getRowBitWidth();
  int bufSize = dataWidth + 2;
  char* valueStr = CARBON_ALLOC_VEC(char, bufSize);
  int actualSize = 0;
  if (buf)
    actualSize = formatString64(valueStr, bufSize, *buf, strFormat);
  else
    actualSize = sZeroFormatString(valueStr, bufSize, strFormat, dataWidth);
  if (actualSize >= 0) {
    valueStr[actualSize] = '\n';
    valueStr[actualSize + 1] = '\0';
    out->writeStr(valueStr, actualSize + 1);
  }
  CARBON_FREE_VEC(valueStr, char, bufSize);
}

CarbonStatus ShellSparseMemory32x32::readmemh(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellSparseMemory32x32::readmemb(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memb);
}

CarbonStatus ShellSparseMemory32x32::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellSparseMemory32x32::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memb);
}

void ShellSparseMemory32x32::syncPlaybackMem(ShellNetPlaybackMem* playMem) const
{
  doSyncPlaybackMem<UInt32>(playMem);
}

CarbonStatus ShellSparseMemory32x32::readmemfile(HDLReadMemX* readmemx)
{
  if (!readmemx->openFile())
    return eCarbon_ERROR;
  
  SInt64 address;
  UInt32 data;
  HDLReadMemXResult result;
  while ((result = readmemx->getNextWord(&address, &data)) == eRMValidData)
  {
    UInt32* row = getRowOrCreate<UInt32>(address);
    *row = data;
  }
  
  readmemx->closeFile();
  
  CarbonStatus stat = eCarbon_OK;
  if (result != eRMEnd)
    stat = eCarbon_ERROR;
  else
    postMemoryWrite();
  
  return stat;
}

ShellSparseMemory32x64::ShellSparseMemory32x64(SInt32 msb, SInt32 lsb, CarbonMemAddrT leftAddr, CarbonMemAddrT rightAddr, void* mem) 
  : ShellSparseMemory32Key(msb, lsb, leftAddr, rightAddr, mem)
{}

ShellSparseMemory32x64::~ShellSparseMemory32x64()
{}

CarbonStatus ShellSparseMemory32x64::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
  {
    const UInt64* row = getRow<UInt64>(address);
    if (row)
      CarbonValRW::cpSrcToDest(buf, row, 2);
    else
      CarbonValRW::setToZero(buf, 2);
  }
  return stat;
}

UInt32 ShellSparseMemory32x64::examineMemoryWord(CarbonMemAddrT address, 
                                                int index) const
{
CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, mModel);

  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  

  UInt32 buf = 0;
  if (stat == eCarbon_OK)
  {
    const UInt64* row = getRow<UInt64>(address);
    if (row)
      CarbonValRW::cpSrcToDestWord(&buf, row, index);
  }
  
  return buf;
}

CarbonStatus ShellSparseMemory32x64::examineMemoryRange(CarbonMemAddrT address, 
                                                       UInt32 *dst, 
                                                       int range_msb, 
                                                       int range_lsb) const
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
  {
    const UInt64* src = getRow<UInt64>(address);
    if (src)
    {
      size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
      size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
      
      CarbonValRW::cpSrcRangeToDest(dst, src, index, length);
    }
    else
      CarbonValRW::setToZero(dst, 2);
  }
  return stat;
}

CarbonStatus ShellSparseMemory32x64::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
  {
    UInt64* row = getRowOrCreate<UInt64>(address);
    CarbonValRW::cpSrcToDest(row, buf, 2);
    *row &= CarbonValRW::getWordMaskLL(CBITWIDTH(mMsb, mLsb));
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellSparseMemory32x64::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, mModel);
  
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
  {
    UInt64* row = getRowOrCreate<UInt64>(address);
    CarbonValRW::cpSrcWordToDest(row, buf, index);
    *row &= CarbonValRW::getWordMaskLL(CBITWIDTH(mMsb, mLsb));
    postMemoryWrite();
  }
  return stat;
}

CarbonStatus ShellSparseMemory32x64::depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb)
{
  CarbonStatus stat = ShellGlobal::carbonTestRange(mMsb, mLsb, range_msb, range_lsb, mModel);
  if (ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()) == eCarbon_ERROR)
    stat = eCarbon_ERROR;
  
  if (stat == eCarbon_OK)
  {
    UInt64* row = getRowOrCreate<UInt64>(address);
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mMsb, mLsb);
    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    CarbonValRW::cpSrcToDestRange(row, src, index, length);
    postMemoryWrite();
  }

  return stat;
}

CarbonStatus ShellSparseMemory32x64::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mLeftAddr, mRightAddr, mModel, getName()); 
  if (stat == eCarbon_OK)
  {
    const UInt64* data = getRow<UInt64>(address);
    int ret = -1;
    if (data)
      ret = formatString64(valueStr, len, *data, strFormat);
    else 
      ret = sZeroFormatString(valueStr, len, strFormat, getRowBitWidth());
    
    if (ret == -1)
    {
      ShellGlobal::reportInsufficientBufferLength(len, mModel);
      stat = eCarbon_ERROR;
    }
  }
  
  return stat;
}

void ShellSparseMemory32x64::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  const UInt64* buf = getRow<UInt64>(address);
  int dataWidth = getRowBitWidth();
  int bufSize = dataWidth + 2;
  char* valueStr = CARBON_ALLOC_VEC(char, bufSize);
  int actualSize = 0;
  if (buf)
    actualSize = formatString64(valueStr, bufSize, *buf, strFormat);
  else
    actualSize = sZeroFormatString(valueStr, bufSize, strFormat, dataWidth);
  if (actualSize >= 0) {
    valueStr[actualSize] = '\n';
    valueStr[actualSize + 1] = '\0';
    out->writeStr(valueStr, actualSize + 1);
  }
  CARBON_FREE_VEC(valueStr, char, bufSize);
}

CarbonStatus ShellSparseMemory32x64::readmemh(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellSparseMemory32x64::readmemb(const char* filename)
{
  CarbonMemAddrT lo, hi;
  sGetLowHiAddrs(mLeftAddr, mRightAddr, &lo, &hi);

  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, lo, hi, false, mModel->getHandle());
  return readmemfile(&memb);
}

CarbonStatus ShellSparseMemory32x64::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memh(filename, true, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memh);
}

CarbonStatus ShellSparseMemory32x64::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  HDLReadMemX memb(filename, false, CABS(mMsb - mLsb) + 1, startAddr, endAddr, true, mModel->getHandle());
  return readmemfile(&memb);
}

void ShellSparseMemory32x64::syncPlaybackMem(ShellNetPlaybackMem* playMem) const
{
  doSyncPlaybackMem<UInt64>(playMem);
}

CarbonStatus ShellSparseMemory32x64::readmemfile(HDLReadMemX* readmemx)
{
  if (!readmemx->openFile())
    return eCarbon_ERROR;
  
  SInt64 address;
  UInt32 data[2];
  HDLReadMemXResult result;
  while ((result = readmemx->getNextWord(&address, data)) == eRMValidData)
  {
    UInt64* row = getRowOrCreate<UInt64>(address);
    CarbonValRW::cpSrcToDest(row, data, 2);
  }
  
  readmemx->closeFile();
  
  CarbonStatus stat = eCarbon_OK;
  if (result != eRMEnd)
    stat = eCarbon_ERROR;
  else
    postMemoryWrite();
  
  return stat;
}
