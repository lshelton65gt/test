// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/ShellNetConstant.h"
#include "shell/ShellGlobal.h"
#include "shell/CarbonModel.h"
#include "util/UtConv.h"
#include "util/DynBitVector.h"
#include "iodb/IODBRuntime.h"
#include "iodb/IODBTypes.h"

static size_t sGetRangeArraySize(size_t index, size_t length)
{
  return ((index + length + 31)/32);
}

ShellNetConstant::ShellNetConstant()
{}

ShellNetConstant::~ShellNetConstant()
{}

void ShellNetConstant::getTraits(Traits* traits) const
{
  traits->mConstantNet = this;
}

void ShellNetConstant::issueDepositWarning(CarbonModel* model)
{
  if (model && model->issueDepositConstWarning()) 
  {
    ShellGlobal::reportDepositToConstant(getNameAsLeaf(), model);
    ShellGlobal::reportNotDepositable(getNameAsLeaf(), model);
  }
}

CarbonStatus ShellNetConstant::deposit (const UInt32*, const UInt32*, CarbonModel* model)
{
  issueDepositWarning(model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetConstant::depositWord (UInt32, int, UInt32, CarbonModel* model)
{
  issueDepositWarning(model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetConstant::depositRange (const UInt32*, int, int, const UInt32*, CarbonModel* model)
{
  issueDepositWarning(model);
  return eCarbon_ERROR;
}


void ShellNetConstant::fastDeposit (const UInt32*, const UInt32*, CarbonModel*)
{
}

void ShellNetConstant::fastDepositWord (UInt32, int, UInt32, CarbonModel*)
{
}

void ShellNetConstant::fastDepositRange (const UInt32*, int, int, const UInt32*, CarbonModel*)
{
}

ShellNet::ValueState ShellNetConstant::writeIfNotEq(char* valueStr, size_t len, Storage*, NetFlags)
{
  if (valueStr[0] == 'x')
  {
    format(valueStr, len, eCarbonBin, eNoneNet, NULL);
    return eChanged;
  }
  return eUnchanged;
}

CarbonStatus ShellNetConstant::format(char* valueStr, size_t len, CarbonRadix strFormat, NetFlags, 
                                      CarbonModel* model) const
{
  int ret = -1;
  UInt32 numStoredWords;
  const UInt32* val = getInternalValue(&numStoredWords);
  size_t numBits = unsigned(getBitWidth());
  UInt32 numStoredBits = numStoredWords * 32;
  UInt32 curIndex = 0;
  UInt32 decBits = 0;
  bool doSigned = true; // for CarbonDec
  
  switch(strFormat)
  {
  case eCarbonBin:
    while ((numBits > numStoredBits) && (len > 1))
    {
      valueStr[curIndex] = '0';
      ++curIndex;
      --len;
      --numBits;
    }
    if (len > 1)
      ret = CarbonValRW::writeBinValToStr(&valueStr[curIndex], len, val, numBits);
    break;
  case eCarbonHex:
    if ((numBits > numStoredBits) && (len > 1))
    {
      // 32 is div by 4, so just need to fill in extra nibbles and
      // truncated first nibble

      // leading truncated nibble first
      UInt32 numFirstNibbleBits = numBits % 4;
      if (numFirstNibbleBits != 0)
      {
        valueStr[curIndex] = '0';
        ++curIndex;
        --len;
        numBits -= numFirstNibbleBits;
      }
      
      while ((numBits > numStoredBits) && (len > 1))
      {
        valueStr[curIndex] = '0';
        ++curIndex;
        --len;
        numBits-=4;
      }
    }
    if (len > 1)
      ret = CarbonValRW::writeHexValToStr(&valueStr[curIndex], len, val, numBits);
    break;
  case eCarbonOct:
    if (numBits > numStoredBits)
    {
      // 32 is not div by 3, so this is a little more difficult. 
      
      // The easiest thing to do here is to figure out how many
      // characters we need, fill the string with 0's, and pass the
      // converter a modified valueStr position.
      UInt32 numCharsNeeded = (numBits + 2)/3;
      if (len > numCharsNeeded)
      {
        for (UInt32 i = 0; i < numCharsNeeded; ++i)
          valueStr[i] = '0';
        
        UInt32 numCharsCanConvert = (numStoredBits + 2)/3;
        curIndex = numCharsNeeded - numCharsCanConvert;
        len -= curIndex;
        numBits = numCharsCanConvert * 3; // makes numStoredBits div by 3
      }
      else 
        len = 0;
    }
    if (len > 1)
      ret = CarbonValRW::writeOctValToStr(&valueStr[curIndex], len, val, numBits);
    break;
  case eCarbonDec:
    if (numBits > numStoredBits)
    {
      decBits = numStoredBits;
      // can't be negative
      doSigned = false;
    }
    else 
      decBits = numBits;
    ret = CarbonValRW::writeDecValToStr(valueStr, len, val, doSigned, decBits);
    break;
  case eCarbonUDec:
    decBits = (numBits > numStoredBits) ? numStoredBits : numBits;
    ret = CarbonValRW::writeDecValToStr(valueStr, len, val, false, decBits);
    break;
  }

  CarbonStatus stat = eCarbon_OK;
  if (ret == -1)
  {
    ShellGlobal::reportInsufficientBufferLength(len, model);
    stat = eCarbon_ERROR;
  }
  return stat;
}

const ShellNetConstant* ShellNetConstant::castConstant() const
{
  return this;
}

void ShellNetConstant::getExternalDrive(UInt32* xdrv) const
{
  if (xdrv)
  {
    UInt32 numWords = getNumUInt32s();
    CarbonValRW::setToOnes(xdrv, numWords);
    UInt32 width = getBitWidth();
    xdrv[numWords - 1] &= CarbonValRW::getWordMask(width);
  }
}

bool ShellNetConstant::isReal() const
{
  return false;
}

bool ShellNetConstant::isTristate() const
{
  return false;
}

bool ShellNetConstant::setToDriven(CarbonModel*)
{
  return false;
}

bool ShellNetConstant::setToUndriven(CarbonModel*)
{
  return false;
}

bool ShellNetConstant::setWordToUndriven(int, CarbonModel*)
{
  return false;
}

bool ShellNetConstant::resolveXdrive(CarbonModel*)
{
  return false;
}

bool ShellNetConstant::setRangeToUndriven(int, int, CarbonModel*)
{
  return false;
}

const UInt32* ShellNetConstant::getControlMask() const
{
  return NULL;
}

CarbonStatus ShellNetConstant::force(const UInt32*, CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus ShellNetConstant::forceWord(UInt32, int, CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus ShellNetConstant::forceRange(const UInt32*, int, int, CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus ShellNetConstant::release(CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus ShellNetConstant::releaseWord(int, CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus ShellNetConstant::releaseRange(int, int, CarbonModel*)
{
  return eCarbon_ERROR;
}

ShellNet::Storage 
ShellNetConstant::allocShadow() const
{
  return NULL;
}

void ShellNetConstant::freeShadow(Storage*)
{
}

void ShellNetConstant::update(Storage*) const
{
}

ShellNet::ValueState ShellNetConstant::compare(const Storage) const
{
  return eUnchanged;
}

bool ShellNetConstant::isForcible() const
{
  return false;
}

bool ShellNetConstant::isInput() const
{
  return false;
}

const CarbonMemory* ShellNetConstant::castMemory() const
{
  return NULL;
}

const CarbonModelMemory* ShellNetConstant::castModelMemory() const
{
  return NULL;
}

const CarbonVectorBase* ShellNetConstant::castVector() const
{
  return NULL;
}

const CarbonScalarBase* ShellNetConstant::castScalar() const
{
  return NULL;
}


void ShellNetConstant::putToZero(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

void ShellNetConstant::putToOnes(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

CarbonStatus ShellNetConstant::setRange(int, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetConstant::clearRange(int, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

int ShellNetConstant::hasDriveConflict() const
{
  return 0;
}

int ShellNetConstant::hasDriveConflictRange(SInt32, SInt32) const
{
  return 0;
}

CarbonStatus ShellNetConstant::examineValXDriveWord(UInt32*, UInt32*, int) const
{
  return eCarbon_ERROR;
}

void ShellNetConstant::setRawToUndriven(CarbonModel*)
{
}

ShellNet::ValueState ShellNetConstant::writeIfNotEqForce(char*, size_t, Storage*, NetFlags, ShellNet*)
{
  ST_ASSERT(0, getName());
  return eUnchanged;
}

CarbonStatus ShellNetConstant::formatForce(char*, size_t, CarbonRadix, NetFlags, ShellNet*, CarbonModel*) const
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void ShellNetConstant::putChangeArrayRef(CarbonChangeType*)
{
}

void ShellNetConstant::runValueChangeCB(CarbonNetValueCBData*, UInt32*, UInt32*, CarbonTriValShadow*, CarbonModel*) const
{
}

CarbonChangeType* ShellNetConstant::getChangeArrayRef()
{
  return NULL;
}

ShellNetConstOverride::ShellNetConstOverride(const UInt32* value, const UInt32* overrideMask, const IODBIntrinsic* intrinsic)
  : mValue(value), mOverrideMask(overrideMask), mIntrinsic(intrinsic)
{}

ShellNetConstOverride::~ShellNetConstOverride()
{}

  
bool ShellNetConstOverride::isScalar() const
{
  return (mIntrinsic->getType() == IODBIntrinsic::eScalar);
}

bool ShellNetConstOverride::isVector() const
{
  return (mIntrinsic->getType() == IODBIntrinsic::eVector);
}

int ShellNetConstOverride::getLSB() const
{
  int ret = 0;
  if (mIntrinsic->getType() == IODBIntrinsic::eVector)
    ret = mIntrinsic->getLsb();
  return ret;
}

int ShellNetConstOverride::getMSB() const
{
  int ret = 0;
  if (mIntrinsic->getType() == IODBIntrinsic::eVector)
    ret = mIntrinsic->getMsb();
  return ret;
}

int ShellNetConstOverride::getBitWidth() const
{
  return (int) mIntrinsic->getWidth();
}

int ShellNetConstOverride::getNumUInt32s() const
{
  return (mIntrinsic->getWidth() + 31)/32;
}

bool ShellNetConstOverride::isDataNonZero() const
{
  return CarbonValRW::isZero(mValue, getNumUInt32s());
}

CarbonStatus ShellNetConstOverride::examine(UInt32* buf, UInt32* drive, ExamineMode, 
                                            CarbonModel*) const
{
  int numWords = getNumUInt32s();
  if (buf)
    CarbonValRW::cpSrcToDest(buf, mValue, numWords);
  if (drive)
    IODBRuntime::sSetOverride(mOverrideMask, buf, drive, numWords);
  
  return eCarbon_OK;
}

CarbonStatus ShellNetConstOverride::examine(CarbonReal* buf, UInt32* drive, ExamineMode, CarbonModel*) const
{
  int numWords = getNumUInt32s();
  if (numWords != 2)
    return eCarbon_ERROR;
  
  if (drive)
    // note that mValue already has the correct value.
    IODBRuntime::sSetOverride(mOverrideMask, NULL, drive, numWords);
  
  if (buf)
  {
    // I'm playing it conservatively and verbosely here. I'm going to
    // copy the UInt32[2] to a UInt64 then copy that to a double. I'm
    // afraid that some compiler may not like me copying a uint32 array
    // to a double and silently give me the wrong answer. a uint64 is at
    // least the correct pod size.
    UInt64 tmpVal;
    CarbonValRW::cpSrcToDest(&tmpVal, mValue, 2);
    CarbonValRW::cpSrcToDest(buf, &tmpVal, 2);
  }
  
  return eCarbon_OK;
}

CarbonStatus ShellNetConstOverride::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode, CarbonModel* model) const
{
  int numWords = getNumUInt32s();
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
  if (stat == eCarbon_OK)
  {
    if (buf)
      *buf = mValue[index];
    if (drive)
      IODBRuntime::sSetOverrideWord(mOverrideMask, buf, drive, numWords, index);
  }
  return stat;
}

CarbonStatus ShellNetConstOverride::examineRange(UInt32* buf, int range_msb, 
                                                 int range_lsb, UInt32* drive, 
                                                 CarbonModel* model) const
{
  int msb = mIntrinsic->getMsb();
  int lsb = mIntrinsic->getLsb();
  
  CarbonStatus stat = ShellGlobal::carbonTestRange(msb, lsb, range_msb, range_lsb, model);
  if (stat == eCarbon_OK)
  {

    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, msb, lsb);

    if (buf)
      CarbonValRW::cpSrcRangeToDest(buf, mValue, index, length);

    // Make temporary buffers to receive all the bits, then we will use
    // cpSrcRangeToDest to get just the desired bits into the user's arrays
    UInt32 nbits = 32 * getNumUInt32s();
    DynBitVector drvAll(nbits), bufAll(nbits);
    UInt32* drvArr = drvAll.getUIntArray();
    UInt32* bufArr = bufAll.getUIntArray();
    IODBRuntime::sSetOverride(mOverrideMask, bufArr, drvArr, getNumUInt32s());

    if (drive != NULL) {
      CarbonValRW::cpSrcRangeToDest(drive, drvArr, index, length);
    }
    if (buf != NULL) {
      CarbonValRW::cpSrcRangeToDest(buf, bufArr, index, length);
    }
  }
  return stat;
}

CarbonStatus ShellNetConstOverride::format(char* valueStr, size_t len, 
                                           CarbonRadix strFormat, NetFlags, 
                                           CarbonModel* model) const
{
  int numCharsConverted = -1;
  CarbonStatus stat = eCarbon_OK;
  size_t bitWidth = mIntrinsic->getWidth();
  switch(strFormat)
  {
  case eCarbonBin:
    numCharsConverted = CarbonValRW::writeBinXZValToStr(valueStr, len, mValue, NULL, NULL, NULL, mOverrideMask, false, bitWidth);
    break;
  case eCarbonHex:
    numCharsConverted = CarbonValRW::writeHexXZValToStr(valueStr, len, mValue, NULL, NULL, NULL, mOverrideMask, false, bitWidth);
    break;
  case eCarbonOct:
    numCharsConverted = CarbonValRW::writeOctXZValToStr(valueStr, len, mValue, NULL, NULL, NULL, mOverrideMask, false, bitWidth);
    break;
  case eCarbonDec:
    numCharsConverted = CarbonValRW::writeDecXZValToStr(valueStr, len, mValue, NULL, NULL, NULL, mOverrideMask, false, true, bitWidth);
    break;
  case eCarbonUDec:
    numCharsConverted = CarbonValRW::writeDecXZValToStr(valueStr, len, mValue, NULL, NULL, NULL, mOverrideMask, false, false, bitWidth);
    break;
  }
  
  if (numCharsConverted == -1)
  {
    ShellGlobal::reportInsufficientBufferLength(len, model);
    stat = eCarbon_ERROR;
  }
  return stat;

}

const UInt32* ShellNetConstOverride::getInternalValue(UInt32* numStoredWords) const
{
  *numStoredWords = getNumUInt32s();
  return mValue;
}


const UInt32* ShellNetConstOverride::getControlMask() const
{
  return mOverrideMask;
}

ShellNetConstScalar::ShellNetConstScalar(UInt32 value)
  : ShellNetConstant(), mValue(value)
{}

ShellNetConstScalar::~ShellNetConstScalar()
{}

bool ShellNetConstScalar::isScalar() const
{
  return true;
}

bool ShellNetConstScalar::isVector() const
{
  return false;
}

int ShellNetConstScalar::getBitWidth() const
{
  return 1;
}

int ShellNetConstScalar::getNumUInt32s() const
{
  return 1;
}

bool ShellNetConstScalar::isDataNonZero() const
{
  return (mValue != 0);
}

CarbonStatus ShellNetConstScalar::examine(UInt32* buf, UInt32* drive, ExamineMode, CarbonModel*) const
{
  if (buf)
    *buf = mValue;
  if (drive)
    *drive = 0;
  return eCarbon_OK;
}

CarbonStatus ShellNetConstScalar::examineWord(UInt32* buf, int, 
                                              UInt32* drive, 
                                              ExamineMode mode, 
                                              CarbonModel* model) const
{
  return examine(buf, drive, mode, model);
}

CarbonStatus ShellNetConstScalar::examineRange(UInt32* buf, int, 
                                               int, UInt32* drive,
                                               CarbonModel* model) const
{
  return examine(buf, drive, eIDrive, model);
}

CarbonStatus ShellNetConstScalar::examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const
{
  return eCarbon_ERROR;
}

const UInt32* ShellNetConstScalar::getInternalValue(UInt32* numStoredWords) const
{
  *numStoredWords = 1;
  return &mValue;
}


ShellNetConstVector::ShellNetConstVector(const UInt32* value, UInt32 numWords, 
                                         const IODBIntrinsic* intrinsic)
  : ShellNetConstant(), mValue(value), mIntrinsic(intrinsic),
    mNumStoredWords(numWords)
{}

ShellNetConstVector::~ShellNetConstVector() 
{}

bool ShellNetConstVector::isVector() const
{
  return true;
}

bool ShellNetConstVector::isScalar() const
{
  return false;
}

int ShellNetConstVector::getBitWidth() const
{
  return (int) mIntrinsic->getWidth();
}

int ShellNetConstVector::getNumUInt32s() const
{
  return (mIntrinsic->getWidth() + 31)/32;
}

bool ShellNetConstVector::isDataNonZero() const
{
  bool isZero = true;
  for (UInt32 i = 0; (i < mNumStoredWords) && isZero; ++i)
    isZero = (mValue[i] == 0);
  
  return !isZero;
}

int ShellNetConstVector::getLSB() const
{
  return mIntrinsic->getLsb();
}

int ShellNetConstVector::getMSB() const
{
  return mIntrinsic->getMsb();
}

CarbonStatus ShellNetConstVector::examine(UInt32* buf, UInt32* drive, ExamineMode,
                                          CarbonModel*) const
{
  int i;
  int numWords = getNumUInt32s();
  
  if (buf)
  {
    for (i = 0; i < numWords; ++i)
      buf[i] = 0;
    
    CarbonValRW::cpSrcToDest(buf, mValue, mNumStoredWords);
  }
  
  if (drive)
  {
    for (i = 0; i < numWords; ++i)
      drive[i] = 0;
  }
  return eCarbon_OK;
}

CarbonStatus ShellNetConstVector::examine(CarbonReal* buf, UInt32* drive, ExamineMode, CarbonModel*) const
{
  int numWords = getNumUInt32s();
  if (numWords != 2)
    return eCarbon_ERROR;
 
  if (drive)
  {
    for (int i = 0; i < numWords; ++i)
      drive[i] = 0;
  }
  
  if (buf)
  {
    
    // I'm playing it conservatively and verbosely here. I'm going to
    // copy the UInt32[2] to a UInt64 then copy that to a double. I'm
    // afraid that some compiler may not like me copying a uint32 array
    // to a double and silently give me the wrong answer. a uint64 is at
    // least the correct pod size.
    
    // mNumStoredWords is either == 2 or 1.

    UInt64 tmpVal = 0;

    if (mNumStoredWords == 1)
    {
      // This is nasty. But UtConv doesn't care about the number of
      // words when copying to a 64 bit pod. It expects that mValue is
      // at least 2 words long, but it isn't in this case.
      UInt32 tmpArr[2] = {0,0};
      CarbonValRW::cpSrcToDest(tmpArr, mValue, mNumStoredWords);
      CarbonValRW::cpSrcToDest(&tmpVal, tmpArr, 2);
    }
    else
      // mNumStoredWords == 2.
      CarbonValRW::cpSrcToDest(&tmpVal, mValue, 2);

    CarbonValRW::cpSrcToDest(buf, &tmpVal, 2);
    
  }
  
  return eCarbon_OK;
}

CarbonStatus ShellNetConstVector::examineWord(UInt32* buf, int index, UInt32* drive, 
                                              ExamineMode, CarbonModel* model) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, getNumUInt32s() - 1, model);
  if (stat == eCarbon_OK)
  {
    if (buf)
    {
      *buf = 0;
      if (unsigned(index) < mNumStoredWords)
        *buf = mValue[index];
    }

    if (drive)
      *drive = 0;
  }
  return stat;
}

CarbonStatus ShellNetConstVector::examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive,
                                               CarbonModel* model) const
{
  int msb = mIntrinsic->getMsb();
  int lsb = mIntrinsic->getLsb();

  CarbonStatus stat = ShellGlobal::carbonTestRange(msb, lsb, range_msb, range_lsb, model);
  if (stat == eCarbon_OK) {

    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, msb, lsb);
    size_t maxNumWords = sGetRangeArraySize(index, length);

    if (buf)
    {
      if (maxNumWords <= mNumStoredWords)
        CarbonValRW::cpSrcRangeToDest(buf, mValue, index, length);
      else {
        DynBitVector valCp(index + length, mValue, mNumStoredWords);
        
        CarbonValRW::cpSrcRangeToDest(buf, &valCp, index, length);
      }
    }
    
    if (drive)
    {
      DynBitVector drvCp(getBitWidth());
      drvCp = 0;
      CarbonValRW::cpSrcRangeToDest(drive, &drvCp, 0, length);
    }
  }
  return stat;
}

const UInt32* ShellNetConstVector::getInternalValue(UInt32* numStoredWords) const
{
  *numStoredWords = mNumStoredWords;
  return mValue;
}

ShellNetConstReal::ShellNetConstReal(const UInt32* value,
                                     const IODBIntrinsic* intrinsic)
  : ShellNetConstVector(value, 2, intrinsic)
{}

ShellNetConstReal::~ShellNetConstReal()
{}

CarbonStatus ShellNetConstReal::examine(CarbonReal* buf, UInt32* drive, ExamineMode, CarbonModel*) const
{
  if (drive)
  {
    drive[0] = 0;
    drive[1] = 0;
  }

  if (buf)
  {
    UInt32 numWords;
    const UInt32* val = getInternalValue(&numWords);
    memcpy(buf, val, 8);
  }

  return eCarbon_OK;
}

bool ShellNetConstReal::isReal() const
{
  return true;
}

ShellNetConstVectorWord::ShellNetConstVectorWord(UInt32 value, 
                                                 const IODBIntrinsic* intrinsic)
  : ShellNetConstant(), mIntrinsic(intrinsic), mValue(value)
{}

ShellNetConstVectorWord::~ShellNetConstVectorWord() 
{}

bool ShellNetConstVectorWord::isVector() const
{
  return true;
}

bool ShellNetConstVectorWord::isScalar() const
{
  return false;
}

int ShellNetConstVectorWord::getBitWidth() const
{
  return (int) mIntrinsic->getWidth();
}

int ShellNetConstVectorWord::getNumUInt32s() const
{
  return (mIntrinsic->getWidth() + 31)/32;
}

bool ShellNetConstVectorWord::isDataNonZero() const
{
  return (mValue != 0);
}

int ShellNetConstVectorWord::getLSB() const
{
  return mIntrinsic->getLsb();;
}

int ShellNetConstVectorWord::getMSB() const
{
  return mIntrinsic->getMsb();
}

CarbonStatus ShellNetConstVectorWord::examine(UInt32* buf, UInt32* drive, ExamineMode, CarbonModel*) const
{
  int numWords = getNumUInt32s();
  int i;
  if (buf)
  {
    for (i = 0; i < numWords; ++i)
      buf[i] = 0;
    
    buf[0] = mValue;
  }
  
  if (drive)
  {
    for (i = 0; i < numWords; ++i)
      drive[i] = 0;
  }

  return eCarbon_OK;
}

CarbonStatus ShellNetConstVectorWord::examineWord(UInt32* buf, int index, UInt32* drive,
                                                  ExamineMode,
                                                  CarbonModel* model) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, getNumUInt32s() - 1, model);
  if (stat == eCarbon_OK)
  {
    if (buf)
    {
      *buf = 0;
      if (index == 0)
        *buf = mValue;
    }
    if (drive)
      *drive = 0;
  }
  return stat;
}

CarbonStatus ShellNetConstVectorWord::examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive,
                                                   CarbonModel* model) const
{
  int msb = mIntrinsic->getMsb();
  int lsb = mIntrinsic->getLsb();
  
  CarbonStatus stat = ShellGlobal::carbonTestRange(msb, lsb, range_msb, range_lsb, model);
  if (stat == eCarbon_OK)
  {

    size_t length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
    size_t index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, msb, lsb);
    size_t maxNumWords = sGetRangeArraySize(index, length);
    
    if (buf) {
      
      if (maxNumWords == 1)
        CarbonValRW::cpSrcRangeToDest(buf, &mValue, index, length);
      else {
        DynBitVector valCp(index + length, &mValue, 1);
        
        CarbonValRW::cpSrcRangeToDest(buf, &valCp, index, length);
      }
      
    } 
     
    if (drive)
    {
      if (maxNumWords == 1)
      {
        UInt32 drvVal = 0;
        CarbonValRW::cpSrcRangeToDest(drive, &drvVal, 0, length);
      }
      else
      {
        DynBitVector drvCp(getBitWidth(),UInt64 (0));

        CarbonValRW::cpSrcRangeToDest(drive, &drvCp, 0, length);
      }
    }
  }
  return stat;
}

CarbonStatus ShellNetConstVectorWord::examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const
{
  return eCarbon_ERROR;
}

const UInt32* ShellNetConstVectorWord::getInternalValue(UInt32* numStoredWords) const
{
  *numStoredWords = 1;
  return &mValue;
}
