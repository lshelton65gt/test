// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/CarbonMemWordIdent.h"
#include "shell/CarbonNetIdent.h"
#include "symtab/STAliasedLeafNode.h"

CarbonMemWordIdent::CarbonMemWordIdent(CarbonMemIdent *expr1, CarbonConst *expr2, UInt32 bitSize, bool isSigned, bool computeSigned)
  : CarbonIdent(isSigned, bitSize),
    mBinOp(eBiMemSel, expr1, expr2, bitSize, isSigned, computeSigned)
{
}

CarbonMemWordIdent::~CarbonMemWordIdent()
{
}

ptrdiff_t CarbonMemWordIdent::compare(const CarbonExpr* other) const
{
  // We can assume the other expression is a CarbonIdent, but it may
  // not be a CarbonMemWordIdent.
  const CarbonIdent *otherIdent = static_cast<const CarbonIdent*>(other);  
  const CarbonMemWordIdent *otherMemWordIdent = otherIdent->castCarbonMemWordIdent();
  // If it's the wrong type, return a difference
  if (otherMemWordIdent == NULL) {
    return 1;
  }

  // All that's left is to compare the binary ops
  ptrdiff_t ret = mBinOp.compare(&otherMemWordIdent->mBinOp);
  return ret;
}

const STAliasedLeafNode* CarbonMemWordIdent::getNode(DynBitVector* usageMask) const
{
  // Get the node from the underlying CarbonMemIdent
  CarbonMemIdent *memIdent = getMemIdent();
  const STAliasedLeafNode *node = memIdent->getNode(usageMask);
  return node;
}

CarbonExpr::AssignStat CarbonMemWordIdent::assign(ExprAssignContext* context)
{
  // This is easy.   Just pass the assign along.
  AssignStat ret = mBinOp.assign(context);
  return ret;
}

CarbonExpr::AssignStat CarbonMemWordIdent::assignRange(ExprAssignContext* context, const ConstantRange& range)
{
  // Only deposits are supported
  CarbonNetIdent::AssignContext* assignContext = static_cast<CarbonNetIdent::AssignContext*>(context); 
  CE_ASSERT(assignContext->getMode() == CarbonNetIdent::AssignContext::eDeposit, this);

  // We need to do a read/modify/write here.  First examine the value
  // of the entire memory word.
  CarbonNetIdent::EvalContext tempEval(CarbonNetIdent::EvalContext::eExamine);
  mBinOp.evaluate(&tempEval);
  // Get the value as a bitvector so we can easily set the necessary
  // range to its new value.  Since bitvectors are indexed in a
  // little-endian manner with an LSB of 0, we need to normalize the
  // partsel range.
  DynBitVector &bitvec = tempEval.getValueRef();
  ConstantRange myRange;
  CE_ASSERT(getDeclaredRange(&myRange), this);
  ConstantRange bvRange = range;
  bvRange.normalize(&myRange);
  bitvec.lpartsel(bvRange.rightmost(), bvRange.getLength()) = context->getValue();
  // Create an assignment context from the modified memory word value.
  // Drive doesn't matter for memories, so just reuse the drive buffer
  // from the original context.
  CarbonNetIdent::AssignContext tempAssign(CarbonNetIdent::AssignContext::eDeposit);
  tempAssign.putAssigns(bitvec, tempEval.getDriveRef());
  // Assign the value to the memory word.
  AssignStat ret = mBinOp.assign(&tempAssign);
  return ret;
}

void CarbonMemWordIdent::print(bool recurse, int indent) const
{
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < indent; i++) {
    screen << " ";
  }
  screen << typeStr() << '(' << this << ") ";
  CarbonExpr::printSize(screen);
  screen << " : ";
  UtString name;
  compose(&name);
  screen << name << UtIO::endl;
  if (recurse) {
    mBinOp.print(recurse, indent + 2);
  }
}

CarbonExpr::SignT CarbonMemWordIdent::evaluate(ExprEvalContext* evalContext) const
{
  // Just delegate this to the memory select operator
  SignT ret = mBinOp.evaluate(evalContext);
  return ret;
}

const char* CarbonMemWordIdent::typeStr() const
{
  return "CarbonMemWordIdent";
}

void CarbonMemWordIdent::composeIdent(ComposeContext* context) const
{
  UtString* str = context->getBuffer();
  ComposeMode mode = context->getMode();
  // Use the underlying CarbonMemIdent
  CarbonMemIdent *ident = getMemIdent();
  DynBitVector unused;
  const STAliasedLeafNode* node = ident->getNode(&unused);
  switch(mode)
  {
  case eComposeNormal:
  case eComposeC:
    node->verilogCompose(str);
    break;
  case eComposeLeaf:
    node->verilogComposeLeaf(str);
    break;
  }
  // Append the index
  CarbonConst *indexExpr = getIndexExpr();
  SInt32 index;
  indexExpr->getL(&index);
  *str << '[' << index << ']';
}

const CarbonMemWordIdent* CarbonMemWordIdent::castCarbonMemWordIdent() const
{
  return this;
}

bool CarbonMemWordIdent::isWholeIdentifier() const
{
  return true;
}

CarbonMemIdent* CarbonMemWordIdent::getMemIdent() const
{
  CarbonExpr *expr = mBinOp.getArg(0);
  CarbonMemIdent *ident = expr->castCarbonMemIdent();
  CE_ASSERT(ident, this);
  return ident;
}

CarbonConst* CarbonMemWordIdent::getIndexExpr() const
{
  CarbonExpr *expr = mBinOp.getArg(1);
  CarbonConst *c = expr->castConst();
  CE_ASSERT(c, this);
  return c;
}
