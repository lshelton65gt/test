// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// author: Mark Seneski

#ifndef __CARBONWAVEVC_H_
#define __CARBONWAVEVC_H_

#ifndef __CARBON_SYS_INCLUDE_H__
#include "codegen/SysIncludes.h"
#endif
#include "util/UtArray.h"
#include "shell/ShellNet.h"
#include "shell/CarbonWaveNetAssoc.h"
#include "CarbonExamineScheduler.h"
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonHookup;
class WaveDump;
class STAliasedLeafNode;
class ScheduleStimuli;
class MiniMemPool;
class SCHEvent;
class NonPodAssoc;
class WaveScheduleGroup;
typedef UtArray<NonPodAssoc*> NonPodAssocVec;
typedef Loop<NonPodAssocVec> NonPodAssocVecLoop;

class CarbonWaveVC
{
public: CARBONMEM_OVERRIDES
  CarbonWaveVC(CarbonHookup* hookup, WaveDump* vcdFile);
  ~CarbonWaveVC();
  
  CarbonHookup* getHookup() { return mHookup; }
  WaveDump* getDataFile() { return mDataFile; }

  WaveScheduleGroup* addSchedStimGroup(ScheduleStimuli* stim);
  
  void allocateNetTrackers();
  void addNonPodCheck(CarbonWaveNetAssoc* netAssoc);
  void addPodCheck(CarbonWaveNetAssoc* netAssoc);
  void reserveSchedStimSpace(UInt32 spaceToReserve);
  void addSchedStim(ScheduleStimuli* stim);

  void setChangedNets(CarbonWaveNetAssoc::NetWriteFlags flags);

  void cleanupAssocRefs();

private:
  CarbonHookup* mHookup;
  WaveDump* mDataFile;

  typedef UtHashMap<ScheduleStimuli*, WaveScheduleGroup*> SchedStimToGroup;

  SchedStimToGroup mSchedStimToGroup;

  NonPodAssocVec mNonPodCheckHandles;

  //! Mappings of storage locations to single wave handles
  NetAssocVec mPodCheckHandles;
  //! Mappings of storage locations to groups of wave handles
  NetAssocGroupVec mPodCheckGroupHandles;

  //! Array of value change objects for individual wave handles
  VCVec mPodCheckVCs;
  //! Array of value change objects for groups of wave handles
  VCVec mPodCheckGroupVCs;
  MiniMemPool* mMemPool;
  WaveScheduleGroup** mGroups;

  void makeGroupVector();
};

class WaveScheduleGroup
{
public: CARBONMEM_OVERRIDES
  WaveScheduleGroup(ScheduleStimuli* stim);
  ~WaveScheduleGroup();
  void addHandle(CarbonWaveNetAssoc* assoc);
  void collateNets(MiniMemPool* memPool);
  void processNets(CarbonWaveNetAssoc::NetWriteFlags flags, 
                   WaveDump* dataFile);
  
  ScheduleStimuli* getStim() { return mStim; }
  
private:
  //! Mappings of storage locations to single wave handles
  NetAssocVec mHandles;
  //! Mappings of storage locations to groups of wave handles
  NetAssocGroupVec mGroupHandles;
  
  //! Array of value change objects for individual wave handles
  VCVec mVCObjs;
  //! Array of value change objects for groups of wave handles
  VCVec mGroupVCObjs;
  ScheduleStimuli* mStim;
};

#endif
