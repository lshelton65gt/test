// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// The easiest way to allow the profile and non-profile versions of
// carbon_capi.cxx to co-exist in the same directory is to give one a
// new name.  Including the .cxx files is an easy way to do this.

#define WITH_PROFILING
// Including a .cxx from here prevents debugging the module with gdb.  It
// complains that any lines greater than the end of this short file don't
// exist in this file (which is true).  If we instead #include it as a .h
// file, using a symbolic link, gdb works.
#include "shell/carbon_capi.cxx.h"    // Actually includes "carbon_capi.cxx"
