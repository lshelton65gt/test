// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "shell/ShellNetTristate.h"
#include "util/UtConv.h"

template <typename T> bool primSetToOnes(T* prim, size_t width)
{
  T ones = T(~0);
  UInt32 widthMask = CarbonValRW::getWordMask(width);
  ones &= widthMask;
  bool changed = *prim != ones;
  *prim = ones;
  return changed;
}

static bool primSetToOnesLL(UInt64* prim, size_t width)
{
  UInt64 ones = ~0;
  UInt64 widthMask = CarbonValRW::getWordMaskLL(width);
  ones &= widthMask;
  bool changed = *prim != ones;
  *prim = ones;
  return changed;
}

template <typename T> bool primSetToZero(T* prim)
{
  bool changed = *prim != 0;
  *prim = 0;
  return changed;
}

template <typename T> bool primResolveXdrive(T* xdrive, T* idrive, size_t width)
{
  T cpXdrive = *xdrive;
  UInt32 widthMask = CarbonValRW::getWordMask(width);
  *xdrive |=  (*idrive & widthMask);
  return *xdrive != cpXdrive;
}

static bool primResolveXdrive64(UInt64* xdrive, UInt64* idrive, size_t width)
{
  UInt64 cpXdrive = *xdrive;
  UInt64 widthMask = CarbonValRW::getWordMaskLL(width);
  *xdrive |=  (*idrive & widthMask);
  return *xdrive != cpXdrive;
}

// Only works for 1, 2 and 4 byte pods.
template <typename T> bool primAssign(T* idata, T* idrive, const UInt32* data, const UInt32* drive, size_t width)
{
  bool changed = false;
  if (drive)
  {
    T cpDrv = *idrive;

    UInt32 twiddleDrv = ~drive[0];
    UInt32 widthMask = CarbonValRW::getWordMask(UInt32(width));
    twiddleDrv &= widthMask;

    CarbonValRW::cpSrcToDest(idrive, &twiddleDrv, 1);
    if (*idrive != cpDrv)
      changed = true;
  }
  if (data)
  {
    T cpCur = *idata;
    *idata = data[0] & *idrive;
    if (*idata != cpCur)
      changed = true;
  }
  return changed;
}

// Only works for 1, 2 and 4 byte pods.
template <typename T> 
bool primBidiAssign(T* idata, T* xdata, T* xdrive, 
                    const UInt32* data, const UInt32* drive,
                    size_t width, bool* doIXSync)
{
  bool changed = false;
  *doIXSync = false;
  UInt32 widthMask = CarbonValRW::getWordMask(UInt32(width));

  if (drive)
  {
    UInt32 fixedDrv = drive[0] & widthMask;
    changed = (*xdrive != fixedDrv);
    CarbonValRW::cpSrcToDest(xdrive, &fixedDrv, 1);
  }
  if (data)
  {
    T cpCur = *xdata;
    *xdata = data[0] & ~(*xdrive) & widthMask;
    if (*xdata != cpCur)
    {
      *idata = *xdata;
      changed = true;
    }
  }
  else if (changed)
    *doIXSync = true;
  return changed;
}

template <typename T> T primCalcNetStrength(T* xdrive, T* idrive)
{
  return *xdrive & ~(*idrive);
}
template <typename T> bool primCalcDriveConflict(T* xdrive, T idrive, 
                                                 size_t width)
{
  // mask unused bits to 1. nor will mask them out.
  T mask = T(~0);
  int bitshift = width % (sizeof(T) * 8);
  if (bitshift > 0)
    mask <<= bitshift;
  else 
    mask = 0;

  T conflict = ~(*xdrive | T(~idrive) | mask);
  return (conflict != 0);
}

template <typename T> bool primCalcDriveConflictRange(T* xdrive, T idrive, 
                                                      size_t index, 
                                                      size_t length,
                                                      size_t)
{
  // mask unused bits to 1. nor will mask them out.
  T mask = T(~0);
  int bitshift = (index + length) % (sizeof(T) * 8);
  if (bitshift > 0)
    mask <<= bitshift;
  else 
    mask = 0;

  T startMask = 1;
  while (index != 0)
  {
    mask |= startMask;
    startMask <<= 1;
    --index;
  }
  
  // The mask now has all 1's in the unused part of the range.
  T conflict = ~(*xdrive | T(~idrive) | mask);
  return (conflict != 0);
}

template <typename T>
void primComplexDataAssign(T* idata, T* xdata, T* xdrive)
{
  *xdata = *idata = *xdata & ~(*xdrive);
}

size_t ShellNetTristate::sCalcNumWords(size_t width)
{
  return ((width - 1)/8 + sizeof(UInt32))/sizeof(UInt32);
}

ShellNetTristate::~ShellNetTristate()
{}

bool ShellNetTristate::hasDriveConflict(size_t) const
{
  return false;
}

bool ShellNetTristate::hasDriveConflictRange(size_t, size_t, size_t) const
{
  return false;
}

bool ShellNetTristate::assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t width)
{
  UInt32 val;
  UInt32 drv;
  getExamineValue(&val, &drv, width);
  CarbonValRW::cpSrcToDestRange(&val, dataRange, index, length);
  CarbonValRW::cpSrcToDestRange(&drv, driveRange, index, length);
  return assign(&val, &drv, width);
}

bool ShellNetTristate::assignWord(UInt32 dataWord, UInt32 driveWord, size_t, size_t width)
{
  return assign(&dataWord, &driveWord, width);
}

bool ShellNetTristate::deassertExternalDriveWord(int, size_t width)
{
  return deassertExternalDrive(width);
}

void ShellNetTristate::getExamineValueWord(UInt32* valWord, UInt32* drvWord, size_t, size_t width) const
{
  getExamineValue(valWord, drvWord, width);
}

bool ShellNetTristate::isBidirect() const
{
  return false;
}

void ShellNetTristate::getCalculatedValue(UInt32* val, UInt32* drv, size_t width) const
{
  // tristate's examine value is the same as the calculated value. Not
  // so for bidirects where the xdrive & ~idrive has to be done.
  getExamineValue(val, drv, width);
}

bool ShellNetTristate::resolveExternalDrive(size_t) 
{
  return false;
}

ShellNetTristate1::ShellNetTristate1(UInt8* data, UInt8* drive)
{
  mIData = data;
  mIDrive = drive;
}

ShellNetTristate1::~ShellNetTristate1() 
{}

void ShellNetTristate1::setConstantBits(const UInt32* overrideMask, size_t width)
{
  CarbonValRW::setConstantBits(mIData, mIDrive, overrideMask, width);
}

void ShellNetTristate1::getCalculatedValuePrim(UInt8* val, UInt8* drv, size_t width) const
{
  // meant only for shadow copies of data.

  // tristate's examine value is the same as the calculated value. Not
  // so for bidirects where the xdrive & ~idrive has to be done.
  *val = getIData();
  *drv = ~(*mIDrive) & CarbonValRW::getWordMask(width);
}

bool ShellNetTristate1::compareCalcValue(const UInt8* val, const UInt8* drv, size_t width) const
{
  UInt8 cpVal;
  UInt8 cpDrv;
  getCalculatedValuePrim(&cpVal, &cpDrv, width);
  bool changed = *val != cpVal;
  if (! changed)
    changed = *drv != cpDrv;
  return changed;
}


bool ShellNetTristate1::assign(const UInt32* data, const UInt32* drive, size_t width)
{
  // rarely called! Normally, tristates are only assigned to
  // when they are inputs. This just covers when they are not.
  return primAssign(mIData, mIDrive, data, drive, width);
}

void ShellNetTristate1::getExamineValue(UInt32* val, UInt32* drv, size_t width) const
{
  if (val)
  {
    UInt8 cpVal = getIData();
    CarbonValRW::cpSrcToDest(val, &cpVal, 1);
  }
  if (drv)
  {
    UInt8 cpDrv = ~(*mIDrive);
    cpDrv &= CarbonValRW::getWordMask(UInt32(width));
    CarbonValRW::cpSrcToDest(drv, &cpDrv, 1);
  }
}

void ShellNetTristate1::getExamineValueRange(UInt32* valRange, UInt32* drvRange, size_t index, size_t length, size_t) const
{
  if (valRange)
  {
    UInt8 cpVal = getIData();
    CarbonValRW::cpSrcRangeToDest(valRange, &cpVal, index, length);
  }
  if (drvRange)
  {
    UInt8 cpDrv = ~(*mIDrive);
    CarbonValRW::cpSrcRangeToDest(drvRange, &cpDrv, index, length);
  }
}

bool ShellNetTristate1::assertExternalDrive(size_t width)
{
  // not usually called! Normally, this only applies to inputs, but
  // this would be called on tristate outputs

  // mIDrive is the internal drive. Therefore, this gets set to all
  // 1's
  primSetToOnes(mIDrive, width);
  // always return true so the model can fix itself
  return true;
}

bool ShellNetTristate1::deassertExternalDrive(size_t)
{
  // not usually called! Normally, this only applies to inputs, but
  // this would be called on tristate outputs

  // mIDrive is the idrive. Therefore, this gets set to 0.
  primSetToZero(mIDrive);
  // always return true so the model can fix itself
  return true;
}

bool ShellNetTristate1::deassertExternalDriveRange(size_t bitIndex, 
                                                   size_t rangeLength,
                                                   size_t)
{
  UInt32 drv = *mIDrive;
  CarbonValRW::setRangeToZero(&drv, bitIndex, rangeLength);
  *mIDrive = drv;
  return true;
}

void ShellNetTristate1::undrivenInit(size_t width)
{
  // not usually called! Only should be called when the model is
  // constructed.
  deassertExternalDrive(width);
  *mIData = 0;
}

UInt8 ShellNetTristate1::getIData() const
{
  return *mIData;
}

UInt8 ShellNetTristate1::getIDrive() const
{
  return *mIDrive;
}

UInt8 ShellNetTristate1::getXDrive(size_t width) const
{
  UInt8 xdrive = getIDrive();
  xdrive = ~xdrive;
  xdrive &= CarbonValRW::getWordMask(width);
  return xdrive;
}

void ShellNetTristate1::getXValues(UInt8* data, UInt8* drive, size_t width) const
{
  *data = *mIData;
  *drive = ~(*mIDrive);
  *drive &= CarbonValRW::getWordMask(width);
}

void ShellNetTristate1::getExternalValues(UInt32* data, UInt32* drive, size_t width) const
{
  UInt8 tmpData, tmpDrive;
  getXValues(&tmpData, &tmpDrive, width);
  *data = tmpData;
  *drive = tmpDrive;
}

void ShellNetTristate1::copyValXDriveWord(UInt32* val, UInt32* drv, int, 
                                          size_t width) const
{
  *val = *mIData;
  *drv = ~(*mIDrive);
  *drv &= CarbonValRW::getWordMask(width);
}

void ShellNetTristate1::getValuePtrs(const UInt8** val, const UInt8** xdrv, const UInt8** idrv) const
{
  *xdrv = NULL;
  *idrv = mIDrive;
  *val = mIData;
}

ShellNetBidirect1::ShellNetBidirect1(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive) :
  ShellNetTristate1(idata, idrive)
{ 
  mXDrive = xdrive;
  mXData = xdata;
}

ShellNetBidirect1::~ShellNetBidirect1() 
{}

void ShellNetBidirect1::setConstantBits(const UInt32* overrideMask, size_t width)
{
  ShellNetTristate1::setConstantBits(overrideMask, width);
  syncXI();
}

void ShellNetBidirect1::getCalculatedValuePrim(UInt8* val, UInt8* drv, size_t) const
{
  // meant only for shadow copies of data.

  // tristate's examine value is the same as the calculated value. Not
  // so for bidirects where the xdrive & ~idrive has to be done.
  *val = getIData();
  *drv = primCalcNetStrength(mXDrive, mIDrive);
}

UInt8 ShellNetBidirect1::getXDrive(size_t) const
{
  return *mXDrive;
}

void ShellNetBidirect1::getXValues(UInt8* data, UInt8* drive, size_t) const
{
  *data = *mXData;
  *drive = *mXDrive;
}

bool ShellNetBidirect1::assign(const UInt32* data, const UInt32* drive, size_t width)
{
  syncXI();
  bool doIXSync;
  bool changed = primBidiAssign(mIData, mXData, mXDrive, data, drive, width, &doIXSync);
  if (doIXSync)
    syncIX();
  return changed;
}

bool ShellNetBidirect1::assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t width)
{
  UInt32 val;
  UInt32 drv;
  getExternalValues(&val, &drv, width);
  CarbonValRW::cpSrcToDestRange(&val, dataRange, index, length);
  CarbonValRW::cpSrcToDestRange(&drv, driveRange, index, length);
  return assign(&val, &drv, width);
}

void ShellNetBidirect1::getCalculatedValue(UInt32* val, UInt32* drv, size_t) const
{
  UInt8 cpVal = getIData();
  // need to get the idrive and invert it.
  UInt8 netStrength = primCalcNetStrength(mXDrive, mIDrive);
  CarbonValRW::cpSrcToDest(val, &cpVal, 1);
  CarbonValRW::cpSrcToDest(drv, &netStrength, 1);
}

void ShellNetBidirect1::copy(const ShellNetTristate1& src, size_t width)
{
  *mIData = src.getIData();
  *mIDrive = src.getIDrive();
  src.getXValues(mXData, mXDrive, width);
}

bool ShellNetBidirect1::assertExternalDrive(size_t)
{
  syncXI();
  bool changed = primSetToZero(mXDrive);
  syncIX();
  return changed;
}

void ShellNetBidirect1::syncIX()
{
  primComplexDataAssign(mIData, mXData, mXDrive);
}

void ShellNetBidirect1::syncXI()
{
  *mXData = *mIData;
}

bool ShellNetBidirect1::deassertExternalDrive(size_t width)
{
  syncXI();
  bool changed = primSetToOnes(mXDrive, width);
  if (changed)
    syncIX();
  return changed;
}

bool ShellNetBidirect1::deassertExternalDriveRange(size_t bitIndex, 
                                                   size_t rangeLength,
                                                   size_t)
{
  syncXI();
  UInt32 tmpDrv = *mXDrive;
  bool changed = CarbonValRW::setRangeToOnes(&tmpDrv, bitIndex, rangeLength);
  if (changed) {
    *mXDrive = tmpDrv;
    syncIX();
  }
  return changed;
}


bool ShellNetBidirect1::resolveExternalDrive(size_t width)
{
  syncXI();
  bool changed = primResolveXdrive(mXDrive, mIDrive, width);
  return changed;
}

bool ShellNetBidirect1::isBidirect() const
{
  return true;
}

void ShellNetBidirect1::copyValXDriveWord(UInt32* val, UInt32* drv, int, size_t) const
{
  *val = *mXData;
  *drv = *mXDrive;
}

void ShellNetBidirect1::getValuePtrs(const UInt8** val, const UInt8** xdrv, const UInt8** idrv) const
{
  *xdrv = mXDrive;
  *idrv = mIDrive;
  *val = mIData;
}

bool ShellNetBidirect1::hasDriveConflict(size_t bitwidth) const
{
  UInt8 idrive = getIDrive();
  return primCalcDriveConflict(mXDrive, idrive, bitwidth);
}

bool ShellNetBidirect1::hasDriveConflictRange(size_t index, size_t length, 
                                              size_t bitwidth) const
{
  UInt8 idrive = getIDrive();
  return primCalcDriveConflictRange(mXDrive, idrive, index, length, bitwidth);
}

ShellNetTristate2::ShellNetTristate2(UInt16* data, UInt16* drive)
{
  mIData = data;
  mIDrive = drive;
}

ShellNetTristate2::~ShellNetTristate2() 
{}

void ShellNetTristate2::setConstantBits(const UInt32* overrideMask, size_t width)
{
  CarbonValRW::setConstantBits(mIData, mIDrive, overrideMask, width);
}

void ShellNetTristate2::getCalculatedValuePrim(UInt16* val, UInt16* drv, size_t width) const
{
  // meant only for shadow copies of data.

  // tristate's examine value is the same as the calculated value. Not
  // so for bidirects where the xdrive & ~idrive has to be done.
  *val = getIData();
  *drv = ~(*mIDrive) & CarbonValRW::getWordMask(width);
}

bool ShellNetTristate2::compareCalcValue(const UInt16* val, const UInt16* drv, size_t width) const
{
  UInt16 cpVal;
  UInt16 cpDrv;
  getCalculatedValuePrim(&cpVal, &cpDrv, width);
  bool changed = *val != cpVal;
  if (! changed)
    changed = *drv != cpDrv;
  return changed;
}

UInt16 ShellNetTristate2::getIData() const
{
  return *mIData;
}

UInt16 ShellNetTristate2::getIDrive() const
{
  return *mIDrive;
}

UInt16 ShellNetTristate2::getXDrive(size_t width) const
{
  UInt16 xdrive = getIDrive();
  xdrive = ~xdrive;
  xdrive &= CarbonValRW::getWordMask(width);
  return xdrive;
}

void ShellNetTristate2::getXValues(UInt16* data, UInt16* drive, size_t width) const
{
  *data = *mIData;
  *drive = ~(*mIDrive);
  *drive &= CarbonValRW::getWordMask(width);
}

void ShellNetTristate2::getExternalValues(UInt32* data, UInt32* drive, size_t width) const
{
  UInt16 tmpData, tmpDrive;
  getXValues(&tmpData, &tmpDrive, width);
  *data = tmpData;
  *drive = tmpDrive;
}

void ShellNetTristate2::copyValXDriveWord(UInt32* val, UInt32* drv, int, size_t width) const
{
  *val = *mIData;
  *drv = ~(*mIDrive);
  *drv &= CarbonValRW::getWordMask(UInt32(width));
}

void ShellNetTristate2::getValuePtrs(const UInt16** val, const UInt16** xdrv, const UInt16** idrv) const
{
  *xdrv = NULL;
  *idrv = mIDrive;
  *val = mIData;
}

void ShellNetTristate2::undrivenInit(size_t width)
{
  // not usually called! Only should be called when the model is
  // constructed.
  deassertExternalDrive(width);
  *mIData = 0;
}


bool ShellNetTristate2::assertExternalDrive(size_t width)
{
  // not usually called! Normally, this only applies to inputs, but
  // this would be called on tristate outputs

  // mIDrive is the internal drive. Therefore, this gets set to all
  // 1's.
  primSetToOnes(mIDrive, width);
  // always return true so the model can fix itself
  return true;
}

bool ShellNetTristate2::deassertExternalDrive(size_t)
{
  // not usually called! Normally, this only applies to inputs, but
  // this would be called on tristate outputs

  // mIDrive is the idrive. Therefore, this gets set to 0.
  primSetToZero(mIDrive);
  // always return true so the model can fix itself
  return true;
}

bool ShellNetTristate2::deassertExternalDriveRange(size_t bitIndex, 
                                                   size_t rangeLength,
                                                   size_t)
{
  UInt32 tmpDrv = *mIDrive;
  CarbonValRW::setRangeToZero(&tmpDrv, bitIndex, rangeLength);
  *mIDrive = tmpDrv;
  return true;
}

bool ShellNetTristate2::assign(const UInt32* data, const UInt32* drive, size_t width)
{
  // rarely called! Normally, tristates are only assigned to
  // when they are inputs. This just covers when they are not.
  return primAssign(mIData, mIDrive, data, drive, width);
}

void ShellNetTristate2::getExamineValue(UInt32* val, UInt32* drv, size_t width) const
{
  if (val)
  {
    UInt16 cpVal = getIData();
    CarbonValRW::cpSrcToDest(val, &cpVal, 1);
  }
  if (drv)
  {
    UInt16 cpDrv = ~(*mIDrive);
    cpDrv &= CarbonValRW::getWordMask(UInt32(width));
    CarbonValRW::cpSrcToDest(drv, &cpDrv, 1);
  }
}

void ShellNetTristate2::getExamineValueRange(UInt32* valRange, UInt32* drvRange, size_t index, size_t length, size_t) const
{
  if (valRange)
  {
    UInt16 cpVal = getIData();
    CarbonValRW::cpSrcRangeToDest(valRange, &cpVal, index, length);
  }
  if (drvRange)
  {
    UInt16 cpDrv = ~(*mIDrive);
    CarbonValRW::cpSrcRangeToDest(drvRange, &cpDrv, index, length);
  }
}

ShellNetBidirect2::ShellNetBidirect2(UInt16* idata, UInt16* idrive, UInt16* xdata, UInt16* xdrive) :
  ShellNetTristate2(idata, idrive)
{ 
  mXDrive = xdrive;
  mXData = xdata;
}

ShellNetBidirect2::~ShellNetBidirect2() 
{}

void ShellNetBidirect2::setConstantBits(const UInt32* overrideMask, size_t width)
{
  ShellNetTristate2::setConstantBits(overrideMask, width);
  syncXI();
}

void ShellNetBidirect2::getCalculatedValuePrim(UInt16* val, UInt16* drv, size_t) const
{
  // meant only for shadow copies of data.

  // tristate's examine value is the same as the calculated value. Not
  // so for bidirects where the xdrive & ~idrive has to be done.
  *val = getIData();
  *drv = primCalcNetStrength(mXDrive, mIDrive);
}

UInt16 ShellNetBidirect2::getXDrive(size_t) const
{
  return *mXDrive;
}

void ShellNetBidirect2::getXValues(UInt16* data, UInt16* drive, size_t) const
{
  *data = *mXData;
  *drive = *mXDrive;
}

bool ShellNetBidirect2::assign(const UInt32* data, const UInt32* drive, size_t width)
{
  syncXI();
  bool doIXSync;
  bool changed = primBidiAssign(mIData, mXData, mXDrive, data, drive, width, &doIXSync);
  if (doIXSync)
    syncIX();
  return changed;
}

bool ShellNetBidirect2::assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t width)
{
  UInt32 val;
  UInt32 drv;
  getExternalValues(&val, &drv, width);
  CarbonValRW::cpSrcToDestRange(&val, dataRange, index, length);
  CarbonValRW::cpSrcToDestRange(&drv, driveRange, index, length);
  return assign(&val, &drv, width);
}

void ShellNetBidirect2::getCalculatedValue(UInt32* val, UInt32* drv, size_t) const
{
  UInt16 cpVal = getIData();
  // need to get the idrive and invert it.
  UInt16 netStrength = primCalcNetStrength(mXDrive, mIDrive);
  CarbonValRW::cpSrcToDest(val, &cpVal, 1);
  CarbonValRW::cpSrcToDest(drv, &netStrength, 1);
}

void ShellNetBidirect2::copy(const ShellNetTristate2& src, size_t width)
{
  *mIData = src.getIData();
  *mIDrive = src.getIDrive();
  src.getXValues(mXData, mXDrive, width);
}

bool ShellNetBidirect2::assertExternalDrive(size_t)
{
  syncXI();
  bool changed = primSetToZero(mXDrive);
  syncIX();
  return changed;
}

void ShellNetBidirect2::syncIX()
{
  primComplexDataAssign(mIData, mXData, mXDrive);
}

void ShellNetBidirect2::syncXI()
{
  *mXData = *mIData;
}

bool ShellNetBidirect2::deassertExternalDrive(size_t width)
{
  syncXI();
  bool changed = primSetToOnes(mXDrive, width);
  if (changed)
    syncIX();
  return changed;
}

bool ShellNetBidirect2::deassertExternalDriveRange(size_t bitIndex, 
                                                   size_t rangeLength,
                                                   size_t)
{
  syncXI();
  UInt32 tmpDrv = *mXDrive;
  bool changed = CarbonValRW::setRangeToOnes(&tmpDrv, bitIndex, rangeLength);
  if (changed) {
    *mXDrive = tmpDrv;
    syncIX();
  }
  return changed;
}

bool ShellNetBidirect2::resolveExternalDrive(size_t width)
{
  syncXI();
  bool changed = primResolveXdrive(mXDrive, mIDrive, width);
  return changed;
}

bool ShellNetBidirect2::isBidirect() const
{
  return true;
}

void ShellNetBidirect2::copyValXDriveWord(UInt32* val, UInt32* drv, int, size_t) const
{
  *val = *mXData;
  *drv = *mXDrive;
}

void ShellNetBidirect2::getValuePtrs(const UInt16** val, const UInt16** xdrv, const UInt16** idrv) const
{
  *xdrv = mXDrive;
  *idrv = mIDrive;
  *val = mIData;
}

bool ShellNetBidirect2::hasDriveConflict(size_t bitwidth) const
{
  UInt16 idrive = getIDrive();
  return primCalcDriveConflict(mXDrive, idrive, bitwidth);
}

bool ShellNetBidirect2::hasDriveConflictRange(size_t index, size_t length, 
                                              size_t bitwidth) const
{
  UInt16 idrive = getIDrive();
  return primCalcDriveConflictRange(mXDrive, idrive, index, length, bitwidth);
}

ShellNetTristate4::ShellNetTristate4(UInt32* data, UInt32* drive)
{
  mIData = data;
  mIDrive = drive;
}

ShellNetTristate4::~ShellNetTristate4() 
{}

void ShellNetTristate4::setConstantBits(const UInt32* overrideMask, size_t width)
{
  CarbonValRW::setConstantBits(mIData, mIDrive, overrideMask, width);
}

void ShellNetTristate4::getCalculatedValuePrim(UInt32* val, UInt32* drv, size_t width) const
{
  // meant only for shadow copies of data.

  // tristate's examine value is the same as the calculated value. Not
  // so for bidirects where the xdrive & ~idrive has to be done.
  *val = getIData();
  *drv = ~(*mIDrive) & CarbonValRW::getWordMask(width);
}

bool ShellNetTristate4::compareCalcValue(const UInt32* val, const UInt32* drv, size_t width) const
{
  UInt32 cpVal;
  UInt32 cpDrv;
  getCalculatedValuePrim(&cpVal, &cpDrv, width);
  bool changed = *val != cpVal;
  if (! changed)
    changed = *drv != cpDrv;
  return changed;
}

UInt32 ShellNetTristate4::getIData() const
{
  return *mIData;
}

UInt32 ShellNetTristate4::getIDrive() const
{
  return *mIDrive;
}

UInt32 ShellNetTristate4::getXDrive(size_t width) const
{
  UInt32 xdrive = getIDrive();
  xdrive = ~xdrive;
  xdrive &= CarbonValRW::getWordMask(width);
  return xdrive;
}

void ShellNetTristate4::getXValues(UInt32* data, UInt32* drive, size_t width) const
{
  *data = *mIData;
  *drive = ~(*mIDrive);
  *drive &= CarbonValRW::getWordMask(width);
}

void ShellNetTristate4::getExternalValues(UInt32* data, UInt32* drive, size_t width) const
{
  getXValues(data, drive, width);
}

void ShellNetTristate4::undrivenInit(size_t width)
{
  // not usually called! Only should be called when the model is
  // constructed.
  deassertExternalDrive(width);
  *mIData = 0;
}


bool ShellNetTristate4::assertExternalDrive(size_t width)
{
  // not usually called! Normally, this only applies to inputs, but
  // this would be called on tristate outputs

  // mIDrive is the internal drive. Therefore, this gets set to all
  // 1's.
  primSetToOnes(mIDrive, width);
  // always return true so the model can fix itself
  return true;
}

bool ShellNetTristate4::deassertExternalDrive(size_t)
{
  // not usually called! Normally, this only applies to inputs, but
  // this would be called on tristate outputs

  // mIDrive is the idrive. Therefore, this gets set to 0.
  primSetToZero(mIDrive);
  // always return true so the model can fix itself
  return true;
}

bool ShellNetTristate4::deassertExternalDriveRange(size_t bitIndex, 
                                                   size_t rangeLength,
                                                   size_t)
{
  UInt32 tmpDrv = *mIDrive;
  CarbonValRW::setRangeToZero(&tmpDrv, bitIndex, rangeLength);
  *mIDrive = tmpDrv;
  return true;
}

bool ShellNetTristate4::assign(const UInt32* data, const UInt32* drive, size_t width)
{
  // rarely called! Normally, tristates are only assigned to
  // when they are inputs. This just covers when they are not.
  return primAssign(mIData, mIDrive, data, drive, width);
}

void ShellNetTristate4::getExamineValue(UInt32* val, UInt32* drv, size_t width) const
{
  if (val)
  {
    UInt32 cpVal = getIData();
    CarbonValRW::cpSrcToDest(val, &cpVal, 1);
  }
  
  if (drv)
  {
    UInt32 cpDrv = ~(*mIDrive);
    cpDrv &= CarbonValRW::getWordMask(UInt32(width));
    CarbonValRW::cpSrcToDest(drv, &cpDrv, 1);
  }
}

void ShellNetTristate4::getExamineValueRange(UInt32* valRange, UInt32* drvRange, size_t index, size_t length, size_t) const
{
  if (valRange)
  {
    UInt32 cpVal = getIData();
    CarbonValRW::cpSrcRangeToDest(valRange, &cpVal, index, length);
  }
  if (drvRange)
  {
    UInt32 cpDrv = ~(*mIDrive);
    CarbonValRW::cpSrcRangeToDest(drvRange, &cpDrv, index, length);
  }
}

void ShellNetTristate4::copyValXDriveWord(UInt32* val, UInt32* drv, int, size_t width) const
{
  *val = *mIData;
  *drv = ~(*mIDrive);
  *drv &= CarbonValRW::getWordMask(UInt32(width));
}

void ShellNetTristate4::getValuePtrs(const UInt32** val, const UInt32** xdrv, const UInt32** idrv) const
{
  *xdrv = NULL;
  *idrv = mIDrive;
  *val = mIData;
}

ShellNetBidirect4::ShellNetBidirect4(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive) :
  ShellNetTristate4(idata, idrive)
{ 
  mXDrive = xdrive;
  mXData = xdata;
}

ShellNetBidirect4::~ShellNetBidirect4() 
{}

void ShellNetBidirect4::setConstantBits(const UInt32* overrideMask, size_t width)
{
  ShellNetTristate4::setConstantBits(overrideMask, width);
  syncXI();
}

void ShellNetBidirect4::getCalculatedValuePrim(UInt32* val, UInt32* drv, size_t) const
{
  // meant only for shadow copies of data.

  // tristate's examine value is the same as the calculated value. Not
  // so for bidirects where the xdrive & ~idrive has to be done.
  *val = getIData();
  *drv = primCalcNetStrength(mXDrive, mIDrive);
}

UInt32 ShellNetBidirect4::getXDrive(size_t) const
{
  return *mXDrive;
}

void ShellNetBidirect4::getXValues(UInt32* data, UInt32* drive, size_t) const
{
  *data = *mXData;
  *drive = *mXDrive;
}

bool ShellNetBidirect4::assign(const UInt32* data, const UInt32* drive, size_t width)
{
  syncXI();
  bool doIXSync;
  bool changed = primBidiAssign(mIData, mXData, mXDrive, data, drive, width, &doIXSync);
  if (doIXSync)
    syncIX();
  return changed;
}

bool ShellNetBidirect4::assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t width)
{
  UInt32 val;
  UInt32 drv;
  getExternalValues(&val, &drv, width);
  CarbonValRW::cpSrcToDestRange(&val, dataRange, index, length);
  CarbonValRW::cpSrcToDestRange(&drv, driveRange, index, length);
  return assign(&val, &drv, width);
}

void ShellNetBidirect4::getCalculatedValue(UInt32* val, UInt32* drv, size_t) const
{
  UInt32 cpVal = getIData();
  // need to get the idrive and invert it.
  UInt32 netStrength = primCalcNetStrength(mXDrive, mIDrive);
  CarbonValRW::cpSrcToDest(val, &cpVal, 1);
  CarbonValRW::cpSrcToDest(drv, &netStrength, 1);
}

void ShellNetBidirect4::copy(const ShellNetTristate4& src, size_t width)
{
  *mIData = src.getIData();
  *mIDrive = src.getIDrive();
  src.getXValues(mXData, mXDrive, width);
}

bool ShellNetBidirect4::assertExternalDrive(size_t)
{
  syncXI();
  bool changed = primSetToZero(mXDrive);
  syncIX();
  return changed;
}

void ShellNetBidirect4::syncIX()
{
  primComplexDataAssign(mIData, mXData, mXDrive);
}

void ShellNetBidirect4::syncXI()
{
  *mXData = *mIData;
}

bool ShellNetBidirect4::deassertExternalDrive(size_t width)
{
  syncXI();
  bool changed = primSetToOnes(mXDrive, width);
  if (changed)
    syncIX();
  return changed;
}

bool ShellNetBidirect4::deassertExternalDriveRange(size_t bitIndex, 
                                                   size_t rangeLength,
                                                   size_t)
{
  syncXI();
  UInt32 tmpDrv = *mXDrive;
  bool changed = CarbonValRW::setRangeToOnes(&tmpDrv, bitIndex, rangeLength);
  if (changed) {
    *mXDrive = tmpDrv;
    syncIX();
  }
  return changed;
}

bool ShellNetBidirect4::resolveExternalDrive(size_t width)
{
  syncXI();
  bool changed = primResolveXdrive(mXDrive, mIDrive, width);
  return changed;
}

bool ShellNetBidirect4::isBidirect() const
{
  return true;
}

void ShellNetBidirect4::copyValXDriveWord(UInt32* val, UInt32* drv, int, size_t) const
{
  *val = *mXData;
  *drv = *mXDrive;
}

void ShellNetBidirect4::getValuePtrs(const UInt32** val, const UInt32** xdrv, const UInt32** idrv) const
{
  *xdrv = mXDrive;
  *idrv = mIDrive;
  *val = mIData;
}

bool ShellNetBidirect4::hasDriveConflict(size_t bitwidth) const
{
  UInt32 idrive = getIDrive();
  return primCalcDriveConflict(mXDrive, idrive, bitwidth);
}

bool ShellNetBidirect4::hasDriveConflictRange(size_t index, size_t length, 
                                              size_t bitwidth) const
{
  UInt32 idrive = getIDrive();
  return primCalcDriveConflictRange(mXDrive, idrive, index, length, bitwidth);
}

ShellNetTristate8::ShellNetTristate8(UInt64* data, UInt64* drive)
{
  mIData = data;
  mIDrive = drive;
}

ShellNetTristate8::~ShellNetTristate8() 
{}

void ShellNetTristate8::setConstantBits(const UInt32* overrideMask, size_t width)
{
  CarbonValRW::setConstantBits(mIData, mIDrive, overrideMask, width);
}

void ShellNetTristate8::getCalculatedValuePrim(UInt64* val, UInt64* drv, size_t width) const
{
  // meant only for shadow copies of data.

  // tristate's examine value is the same as the calculated value. Not
  // so for bidirects where the xdrive & ~idrive has to be done.
  *val = getIData();
  *drv = ~(*mIDrive) & CarbonValRW::getWordMaskLL(width);
}

bool ShellNetTristate8::compareCalcValue(const UInt64* val, const UInt64* drv, size_t width) const
{
  UInt64 cpVal;
  UInt64 cpDrv;
  getCalculatedValuePrim(&cpVal, &cpDrv, width);
  bool changed = *val != cpVal;
  if (! changed)
    changed = *drv != cpDrv;
  return changed;
}

UInt64 ShellNetTristate8::getIData() const
{
  return *mIData;
}

UInt64 ShellNetTristate8::getIDrive() const
{
  return *mIDrive;
}

UInt64 ShellNetTristate8::getXDrive(size_t width) const
{
  UInt64 xdrive = getIDrive();
  xdrive = ~xdrive;
  xdrive &= CarbonValRW::getWordMaskLL(width);
  return xdrive;
}

void ShellNetTristate8::getXValues(UInt64* data, UInt64* drive, size_t width) const
{
  *data = *mIData;
  *drive = ~(*mIDrive);
  *drive &= CarbonValRW::getWordMaskLL(width);
}

void ShellNetTristate8::getExternalValues(UInt32* data, UInt32* drive, size_t width) const
{
  UInt64 tmpData, tmpDrive;
  getXValues(&tmpData, &tmpDrive, width);
  CarbonValRW::cpSrcToDest(data, &tmpData, 2);
  CarbonValRW::cpSrcToDest(drive, &tmpDrive, 2);
}

void ShellNetTristate8::copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t width) const
{
  CarbonValRW::cpSrcToDestWord(val, mIData, index);
  UInt64 tmpDrv = ~(*mIDrive);
  tmpDrv &= CarbonValRW::getWordMaskLL(UInt64(width));
  CarbonValRW::cpSrcToDestWord(drv, &tmpDrv, index);
}

void ShellNetTristate8::getValuePtrs(const UInt64** val, const UInt64** xdrv, const UInt64** idrv) const
{
  *xdrv = NULL;
  *idrv = mIDrive;
  *val = mIData;
}

void ShellNetTristate8::undrivenInit(size_t width)
{
  // not usually called! Only should be called when the model is
  // constructed.
  deassertExternalDrive(width);
  *mIData = 0;
}


bool ShellNetTristate8::assertExternalDrive(size_t width)
{
  // not usually called! Normally, this only applies to inputs, but
  // this would be called on tristate outputs

  // mIDrive is the internal drive. Therefore, this gets set to all
  // 1's.
  primSetToOnesLL(mIDrive, width);
  // always return true so the model can fix itself
  return true;
}

bool ShellNetTristate8::deassertExternalDrive(size_t)
{
  // not usually called! Normally, this only applies to inputs, but
  // this would be called on tristate outputs

  // mIDrive is the idrive. Therefore, this gets set to 0.
  primSetToZero(mIDrive);
  // always return true so the model can fix itself
  return true;
}


bool ShellNetTristate8::deassertExternalDriveWord(int index, size_t)
{
  UInt32 drive[2];
  CarbonValRW::cpSrcToDest(drive, mIDrive, 2);
  drive[index] = 0;
  CarbonValRW::cpSrcToDest(mIDrive, drive, 2);
  return true;
}

bool ShellNetTristate8::deassertExternalDriveRange(size_t bitIndex, 
                                                   size_t rangeLength,
                                                   size_t)
{
  UInt32 tmpDrv[2];
  CarbonValRW::cpSrcToDest(tmpDrv, mIDrive, 2);
  CarbonValRW::setRangeToZero(tmpDrv, bitIndex, rangeLength);
  CarbonValRW::cpSrcToDest(mIDrive, tmpDrv, 2);
  return true;
}

bool ShellNetTristate8::assign(const UInt32* data, const UInt32* drive, size_t width)
{
  // rarely called! Normally, tristates are only assigned to
  // when they are inputs. This just covers when they are not.
  bool changed = false;
  if (drive)
  {
    UInt64 cpDrv = *mIDrive;
    UInt64 widthMask = CarbonValRW::getWordMaskLL(UInt64(width));

    UInt64 driveCp;
    CarbonValRW::cpSrcToDest(&driveCp, drive, 2);
    driveCp = ~driveCp;
    driveCp &= widthMask;
    
    *mIDrive = driveCp;
    if (*mIDrive != cpDrv)
      changed = true;
  }
  if (data)
  {
    UInt64 cpCur = *mIData;
    UInt64 cpData;
    CarbonValRW::cpSrcToDest(&cpData, data, 2);
    *mIData = cpData & *mIDrive;
    if (*mIData != cpCur)
      changed = true;
  }
  return changed;
}

bool ShellNetTristate8::assignWord(UInt32 dataWord, UInt32 driveWord, size_t index, size_t width)
{
  UInt32 val[2];
  UInt32 drv[2];
  CarbonValRW::cpSrcToDest(val, mIData, 2);
  UInt64 exDrive = getXDrive(width);
  CarbonValRW::cpSrcToDest(drv, &exDrive, 2);
  val[index] = dataWord;
  drv[index] = driveWord;
  // 2 is arbitrary
  return assign(val, drv, 2);
}

bool ShellNetTristate8::assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t width)
{
  UInt32 val[2];
  UInt32 drv[2];
  getExternalValues(val, drv, width);
  CarbonValRW::cpSrcToDestRange(val, dataRange, index, length);
  CarbonValRW::cpSrcToDestRange(drv, driveRange, index, length);
  return assign(val, drv, width);
}

void ShellNetTristate8::getExamineValue(UInt32* val, UInt32* drv, size_t width) const
{
  if (val)
  {
    UInt64 cpVal = getIData();
    CarbonValRW::cpSrcToDest(val, &cpVal, 2);
  }

  if (drv)
  {
    UInt64 cpDrv = ~(*mIDrive);
    cpDrv &= CarbonValRW::getWordMaskLL(UInt64(width));
    CarbonValRW::cpSrcToDest(drv, &cpDrv, 2);
  }
}

void ShellNetTristate8::getExamineValueWord(UInt32* valWord, UInt32* drvWord, size_t index, size_t width) const
{
  UInt64 val = getIData();
  CarbonValRW::cpSrcToDestWord(valWord, &val, index);
  UInt64 drv = ~(*mIDrive);
  drv &= CarbonValRW::getWordMaskLL(UInt64(width));
  CarbonValRW::cpSrcToDestWord(drvWord, &drv, index);
}

void ShellNetTristate8::getExamineValueRange(UInt32* valRange, UInt32* drvRange, size_t index, size_t length, size_t width) const
{
  if (valRange)
  {
    UInt64 cpVal = getIData();
    CarbonValRW::cpSrcRangeToDest(valRange, &cpVal, index, length);
  }

  if (drvRange)
  {
    UInt64 cpDrv = ~(*mIDrive);
    cpDrv &= CarbonValRW::getWordMaskLL(UInt64(width));
    CarbonValRW::cpSrcRangeToDest(drvRange, &cpDrv, index, length);
  }
}

ShellNetBidirect8::ShellNetBidirect8(UInt64* idata, UInt64* idrive, UInt64* xdata, UInt64* xdrive) :
  ShellNetTristate8(idata, idrive)
{ 
  mXDrive = xdrive;
  mXData = xdata;
}

ShellNetBidirect8::~ShellNetBidirect8() 
{}

void ShellNetBidirect8::setConstantBits(const UInt32* overrideMask, size_t width)
{
  ShellNetTristate8::setConstantBits(overrideMask, width);
  syncXI();
}

void ShellNetBidirect8::getCalculatedValuePrim(UInt64* val, UInt64* drv, size_t) const
{
  // meant only for shadow copies of data.

  // tristate's examine value is the same as the calculated value. Not
  // so for bidirects where the xdrive & ~idrive has to be done.
  *val = getIData();
  *drv = primCalcNetStrength(mXDrive, mIDrive);
}

UInt64 ShellNetBidirect8::getXDrive(size_t) const
{
  return *mXDrive;
}

void ShellNetBidirect8::getXValues(UInt64* data, UInt64* drive, size_t) const
{
  *data = *mXData;
  *drive = *mXDrive;
}

bool ShellNetBidirect8::assign(const UInt32* data, const UInt32* drive, size_t width)
{
  syncXI();
  bool changed = false;
  UInt64 widthMask = CarbonValRW::getWordMaskLL(width);
  if (drive)
  {
    UInt64 driveCp; 
    CarbonValRW::cpSrcToDest(&driveCp, drive, 2);
    driveCp &= widthMask;
    changed = (*mXDrive != driveCp);
    *mXDrive = driveCp;
  }
  if (data)
  {
    UInt64 cpCur = *mXData;
    UInt64 dataCp;
    CarbonValRW::cpSrcToDest(&dataCp, data, 2);
    *mXData = dataCp & ~(*mXDrive) & widthMask;
    if (*mXData != cpCur)
    {
      // inline syncIX
      *mIData = *mXData;
      changed = true;
    }
  }
  else if (changed)
    syncIX();
  return changed;
}

void ShellNetBidirect8::getCalculatedValue(UInt32* val, UInt32* drv, size_t) const
{
  UInt64 cpVal = getIData();
  // need to get the idrive and invert it.
  UInt64 netStrength = primCalcNetStrength(mXDrive, mIDrive);
  CarbonValRW::cpSrcToDest(val, &cpVal, 2);
  CarbonValRW::cpSrcToDest(drv, &netStrength, 2);
}

void ShellNetBidirect8::copy(const ShellNetTristate8& src, size_t width)
{
  *mIData = src.getIData();
  *mIDrive = src.getIDrive();
  src.getXValues(mXData, mXDrive, width);
}

bool ShellNetBidirect8::assertExternalDrive(size_t)
{
  syncXI();
  bool changed = primSetToZero(mXDrive);
  syncIX();
  return changed;
}

void ShellNetBidirect8::syncIX()
{
  primComplexDataAssign(mIData, mXData, mXDrive);
}

void ShellNetBidirect8::syncXI()
{
  *mXData = *mIData;
}

bool ShellNetBidirect8::deassertExternalDrive(size_t width)
{
  syncXI();
  bool changed = primSetToOnesLL(mXDrive, width);
  if (changed)
    syncIX();
  return changed;
}

bool ShellNetBidirect8::deassertExternalDriveWord(int index, size_t width)
{
  syncXI();
  UInt32 drive[2];
  CarbonValRW::cpSrcToDest(drive, mXDrive, 2);
  UInt32 maxVal = UtUINT32_MAX;
  if (index == 1)
    maxVal &= CarbonValRW::getWordMask(width);
  bool changed = (drive[index] != maxVal);
  if (changed) {
    drive[index] = maxVal;
    CarbonValRW::cpSrcToDest(mXDrive, drive, 2);
    syncIX();
  }
  return changed;
}

bool ShellNetBidirect8::deassertExternalDriveRange(size_t bitIndex, 
                                                   size_t rangeLength,
                                                   size_t)
{
  syncXI();
  UInt32 tmpDrv[2];
  CarbonValRW::cpSrcToDest(tmpDrv, mXDrive, 2);
  bool changed = CarbonValRW::setRangeToOnes(tmpDrv, bitIndex, rangeLength);
  if (changed) {
    CarbonValRW::cpSrcToDest(mXDrive, tmpDrv, 2);
    syncIX();
  }
  return changed;
}

bool ShellNetBidirect8::resolveExternalDrive(size_t width)
{
  syncXI();
  bool changed = primResolveXdrive64(mXDrive, mIDrive, width);
  return changed;
}

bool ShellNetBidirect8::isBidirect() const
{
  return true;
}

void ShellNetBidirect8::copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t) const
{
  CarbonValRW::cpSrcToDestWord(val, mXData, index);
  CarbonValRW::cpSrcToDestWord(drv, mXDrive, index);
}

void ShellNetBidirect8::getValuePtrs(const UInt64** val, const UInt64** xdrv, const UInt64** idrv) const
{
  *xdrv = mXDrive;
  *idrv = mIDrive;
  *val = mIData;
}

bool ShellNetBidirect8::hasDriveConflict(size_t bitwidth) const
{
  UInt64 idrive = getIDrive();
  return primCalcDriveConflict(mXDrive, idrive, bitwidth);
}

bool ShellNetBidirect8::hasDriveConflictRange(size_t index, size_t length, 
                                              size_t bitwidth) const
{
  UInt64 idrive = getIDrive();
  return primCalcDriveConflictRange(mXDrive, idrive, index, length, bitwidth);
}

ShellNetTristateA::ShellNetTristateA(UInt32* idata, UInt32* idrive) :
  mIData(idata), mIDrive(idrive)
{
}

ShellNetTristateA::~ShellNetTristateA()
{}

void ShellNetTristateA::setConstantBits(const UInt32* overrideMask, size_t width)
{
  CarbonValRW::setConstantBits(mIData, mIDrive, overrideMask, width);
}

void ShellNetTristateA::undrivenInit(size_t width)
{
  // not usually called! Only should be called when the model is
  // constructed.
  deassertExternalDrive(width);
  CarbonValRW::setToZero(mIData, sCalcNumWords(width));
}

void ShellNetTristateA::flipIDrive(size_t width) const
{
  size_t numWords = sCalcNumWords(width);
  for (size_t i = 0; i < numWords; ++i)
    mIDrive[i] = ~mIDrive[i];
  mIDrive[numWords - 1] &= CarbonValRW::getWordMask(width);
}

bool ShellNetTristateA::assign(const UInt32* data, const UInt32* drive, 
                               size_t width)
{
  bool changed = false;
  size_t numWords = sCalcNumWords(width);
  if (data)
  {
    changed = (CarbonValRW::memCompare(data, mIData, numWords) != 0); 
    if (changed)
      CarbonValRW::cpSrcToDest(mIData, data, numWords);
  }
  if (drive)
  {
    if (! changed)
    {
      changed = ! CarbonValRW::isAllOnes(drive, width);
      if (changed)
      {
        flipIDrive(width);
        CarbonValRW::cpSrcToDest(mIDrive, drive, numWords);
        flipIDrive(width);
      }
    }
  }
  return changed;
}


bool ShellNetTristateA::assignWord(UInt32 dataWord, UInt32 driveWord, 
                                   size_t index, size_t width)
{
  bool changed = mIData[index] != dataWord; 
  mIData[index] = dataWord;

  UInt32 mask = ~0;
  size_t numWords = sCalcNumWords(width);

  if (index == numWords - 1)
    mask = CarbonValRW::getWordMask(UInt32(width));

  if (!changed)
    changed = mIDrive[index] != (~driveWord & mask);

  mIDrive[index] = ~driveWord;
  mIDrive[index] &= mask;
  return changed;
}

bool ShellNetTristateA::assignRange(const UInt32* dataRange, const UInt32* driveRange, 
                                    size_t index, size_t length, size_t width)
{
  bool changed = false;
  if (dataRange)
  {
    changed = (CarbonValRW::memCompareRange(dataRange, mIData, index, length) != 0); 
    if (changed)
      CarbonValRW::cpSrcToDestRange(mIData, dataRange, index, length);
  }
  if (driveRange)
  {
    if (! changed)
    {
      changed = ! CarbonValRW::isAllOnes(driveRange, length);
      if (changed)
      {
        flipIDrive(width);
        CarbonValRW::cpSrcToDestRange(mIDrive, driveRange, index, length);
        flipIDrive(width);
      }
    }
  }
  return changed;
}

void ShellNetTristateA::getExamineValue(UInt32* val, UInt32* drv, size_t width) const
{
  size_t numWords = sCalcNumWords(width); 
  if (val)
  {
    // don't have to worry about masking here, since BitVectors are
    // doing the assigns. BitVectors clean dirty bits
    CarbonValRW::cpSrcToDest(val, mIData, numWords);
  }
  if (drv)
  {
    flipIDrive(width);
    CarbonValRW::cpSrcToDest(drv, mIDrive, numWords);
    flipIDrive(width);
  }
}

void ShellNetTristateA::getExamineValueWord(UInt32* valWord, UInt32* drvWord, size_t index, size_t width) const
{
  *valWord = mIData[index];
  *drvWord = ~mIDrive[index];
  // dirtied the idrive if last word
  size_t numWords = sCalcNumWords(width); 
  if (index == numWords - 1)
    *drvWord &= CarbonValRW::getWordMask(width);
}

void ShellNetTristateA::getExamineValueRange(UInt32* valRange, UInt32* drvRange, size_t index, size_t length, size_t width) const
{
  if (valRange)
    CarbonValRW::cpSrcRangeToDest(valRange, mIData, index, length);
  if (drvRange)
  {
    flipIDrive(width);
    CarbonValRW::cpSrcRangeToDest(drvRange, mIDrive, index, length);
    flipIDrive(width);
  }
}

bool ShellNetTristateA::compareCalcValue(const UInt32* val, const UInt32* drv, size_t width) const
{
  size_t numWords = sCalcNumWords(width);
  bool changed = (CarbonValRW::memCompare(val, mIData, numWords) != 0); 
  if (! changed)
  {
    flipIDrive(width);
    changed = (CarbonValRW::memCompare(drv, mIDrive, numWords) != 0); 
    flipIDrive(width);
  }
  return changed;
}

bool ShellNetTristateA::assertExternalDrive(size_t width)
{
  size_t numWords = sCalcNumWords(width);
  // strongly drive in case vcd dumping is happening.
  CarbonValRW::setToOnes(mIDrive, numWords);
  // clean the bits
  mIDrive[numWords - 1] &= CarbonValRW::getWordMask(width);

  // For tristates (not bidis) always return true so the value will
  // get fixed by the generated code
  return true;
}

bool ShellNetTristateA::deassertExternalDrive(size_t width)
{
  // strongly drive in case vcd dumping is happening.
  CarbonValRW::setToZero(mIDrive, sCalcNumWords(width));

  // For tristates (not bidis) always return true so the value will
  // get fixed by the generated code
  return true;
}

bool ShellNetTristateA::deassertExternalDriveWord(int index, size_t)
{
  mIDrive[index] = 0;
  return true;
}

bool ShellNetTristateA::deassertExternalDriveRange(size_t bitIndex, 
                                                   size_t rangeLength,
                                                   size_t)
{
  CarbonValRW::setRangeToZero(mIDrive, bitIndex, rangeLength);
  return true;
}


void ShellNetTristateA::copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t width) const
{
  *val = mIData[index];
  *drv = ~mIDrive[index];
  
  size_t numWords = sCalcNumWords(width);
  if (index == signed(numWords - 1))
    *drv &= CarbonValRW::getWordMask(width);
}

void ShellNetTristateA::getValuePtrs(const UInt32** val, const UInt32** xdrv, const UInt32** idrv) const
{
  *xdrv = NULL;
  *idrv = mIDrive;
  *val = mIData;
}

void ShellNetTristateA::getIValues(UInt32* idata, UInt32* idrive, size_t width) const
{
  size_t numWords = sCalcNumWords(width);
  CarbonValRW::cpSrcToDest(idata, mIData, numWords);
  CarbonValRW::cpSrcToDest(idrive, mIDrive, numWords);
}

void ShellNetTristateA::getXValues(UInt32* xdata, UInt32* xdrive, size_t width) const
{
  size_t numWords = sCalcNumWords(width);
  CarbonValRW::cpSrcToDest(xdata, mIData, numWords);
  for (size_t i = 0; i < numWords; ++i)
    xdrive[i] = ~mIDrive[i];

  UInt32 mask = CarbonValRW::getWordMask(UInt32(width));
  xdrive[numWords - 1] &= mask;
}

void ShellNetTristateA::getExternalValues(UInt32* data, UInt32* drive, size_t width) const
{
  getXValues(data, drive, width);
}

const UInt32* ShellNetTristateA::getIData() const
{
  return mIData;
}

const UInt32* ShellNetTristateA::getIDrive() const
{
  return mIDrive;
}

UInt32 ShellNetTristateA::getXDriveWord(size_t index, size_t width) const
{
  UInt32 ret = ~mIDrive[index];
  
  size_t numWords = sCalcNumWords(width);
  if (index == numWords - 1)
    ret &= CarbonValRW::getWordMask(UInt32(width));
  return ret;
}

UInt32 ShellNetTristateA::getCalcDriveWord(UInt32 index, size_t width) const
{
  UInt32 calcDrv = ~mIDrive[index];
  size_t numWords = sCalcNumWords(width);
  if (index == numWords - 1)
    calcDrv &= CarbonValRW::getWordMask(UInt32(width));
  return calcDrv;
}

void ShellNetTristateA::signExtend(UInt32 numWords, UInt32 bitWidth)
{
  CarbonValRW::signExtendVector(mIData, numWords, bitWidth);
  CarbonValRW::signExtendVector(mIDrive, numWords, bitWidth);
}

void ShellNetTristateA::getIPtrs(UInt32** ival, UInt32** idrv)
{
  *ival = mIData;
  *idrv = mIDrive;
}

void ShellNetTristateA::getAllPtrs(UInt32** xval, UInt32** xdrv, UInt32** ival, UInt32** idrv)
{
  *xval = NULL;
  *xdrv = NULL;
  getIPtrs(ival, idrv);
}

ShellNetBidirectA::ShellNetBidirectA(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive) :
  ShellNetTristateA(idata, idrive), mXData(xdata), mXDrive(xdrive)
{}

ShellNetBidirectA::~ShellNetBidirectA()
{}

void ShellNetBidirectA::setConstantBits(const UInt32* overrideMask, size_t width)
{
  ShellNetTristateA::setConstantBits(overrideMask, width);
  size_t numWords = sCalcNumWords(width);
  syncXI(numWords);
}

void ShellNetBidirectA::syncXI(size_t numWords)
{
  CarbonValRW::cpSrcToDest(mXData, mIData, numWords);
}

void ShellNetBidirectA::syncIX(size_t numWords)
{
  for (size_t i = 0; i < numWords; ++i)
    mIData[i] = mXData[i] & ~mXDrive[i];
}

void ShellNetBidirectA::cleanXData(size_t numWords, UInt32 mask)
{
  for (size_t i = 0; i < numWords; ++i)
    mXData[i] = mXData[i] & ~mXDrive[i];
  mXData[numWords - 1] &= mask;
}

bool ShellNetBidirectA::assign(const UInt32* data, const UInt32* drive, size_t width)
{
  size_t numWords = sCalcNumWords(width);
  syncXI(numWords);
  bool changed = false;
  UInt32 mask = CarbonValRW::getWordMask(UInt32(width));
  if (drive)
  {
    if (CarbonValRW::memCompare(drive, mXDrive, numWords) != 0)
    {
      changed = true;
      CarbonValRW::cpSrcToDest(mXDrive, drive, numWords);
      mXDrive[numWords - 1] &= mask;
    }
  }
  
  if (data)
  {
    for (size_t i = 0; i < numWords; ++i)
    {
      changed = changed || ((data[i] & ~mXDrive[i]) != mXData[i]);
      mXData[i] = data[i] & ~mXDrive[i];
    }
    mXData[numWords - 1] &= mask;
  }

  syncIX(numWords);
  return changed;
}

bool ShellNetBidirectA::assignWord(UInt32 dataWord, UInt32 driveWord, size_t index, size_t width)
{
  UInt32 mask = CarbonValRW::getWordMask(UInt32(width));
  size_t numWords = sCalcNumWords(width);
  syncXI(numWords);
  if (index == numWords - 1)
  {
    dataWord &= mask;
    driveWord &= mask;
  }
  
  bool changed = driveWord != mXDrive[index];
  mXDrive[index] = driveWord;
  
  changed = changed || (dataWord != mXData[index]);  
  mXData[index] = dataWord & ~mXDrive[index];
  syncIX(numWords);
  return changed;
}

bool ShellNetBidirectA::assignRange(const UInt32* dataRange, const UInt32* driveRange, size_t index, size_t length, size_t width)
{
  bool changed = false;

  size_t numWords = sCalcNumWords(width);
  syncXI(numWords);

  if (driveRange)
  {
    if (CarbonValRW::memCompareRange(driveRange, mXDrive, index, length))
    {
      changed = true;
      CarbonValRW::cpSrcToDestRange(mXDrive, driveRange, index, length);
    }
  }
  else 
    changed = CarbonValRW::setRangeToZero(mXDrive, index, length);

  if (dataRange)
  {
    if (CarbonValRW::memCompareRange(dataRange, mXData, index, length))
    {
      CarbonValRW::cpSrcToDestRange(mXData, dataRange, index, length);
      changed = true;
      
      // clean the XData with the XDrive
      UInt32 mask = CarbonValRW::getWordMask(UInt32(width));
      cleanXData(numWords, mask);
    }
  }
  
  syncIX(numWords);
  return changed;
}

void ShellNetBidirectA::copy(const ShellNetTristateA& src, size_t width)
{
  src.getIValues(mIData, mIDrive, width);
  src.getXValues(mXData, mXDrive, width);
}

void ShellNetBidirectA::getCalculatedValue(UInt32* val, UInt32* drv, size_t width) const
{
  size_t numWords = sCalcNumWords(width);
  CarbonValRW::cpSrcToDest(val, mIData, numWords);

  for (size_t i = 0; i < numWords; ++i)
    drv[i] = mXDrive[i] & ~mIDrive[i];
}

UInt32 ShellNetBidirectA::getCalcDriveWord(UInt32 index, size_t) const
{
  return mXDrive[index] & ~mIDrive[index];
}

bool ShellNetBidirectA::isBidirect() const
{
  return true;
}

bool ShellNetBidirectA::assertExternalDrive(size_t width)
{
  size_t numWords = sCalcNumWords(width);
  syncXI(numWords);
  bool changed = ! CarbonValRW::isZero(mXDrive, numWords);
  CarbonValRW::setToZero(mXDrive, sCalcNumWords(width));
  syncIX(numWords);
  return changed;
}

bool ShellNetBidirectA::deassertExternalDrive(size_t width)
{
  size_t numWords = sCalcNumWords(width);
  syncXI(numWords);
  bool changed = ! CarbonValRW::isAllOnes(mXDrive, width);
  if (changed) {
    CarbonValRW::setToOnes(mXDrive, numWords);
    UInt32 mask = CarbonValRW::getWordMask(UInt32(width));
    mXDrive[numWords - 1] &= mask;
    syncIX(numWords);
  }
  return changed;
}

bool ShellNetBidirectA::deassertExternalDriveWord(int index, size_t width)
{
  size_t numWords = sCalcNumWords(width);
  syncXI(numWords);
  UInt32 depVal = UtUINT32_MAX;
  if (unsigned(index) == numWords - 1)
    depVal &= CarbonValRW::getWordMask(width);
  bool changed = mXDrive[index] != depVal;
  if (changed)
  {
    mXDrive[index] = depVal;
    syncIX(numWords);
  }
  return changed;
}

bool ShellNetBidirectA::deassertExternalDriveRange(size_t bitIndex, 
                                                   size_t rangeLength,
                                                   size_t width)
{
  size_t numWords = sCalcNumWords(width);
  syncXI(numWords);
  bool changed = CarbonValRW::setRangeToOnes(mXDrive, bitIndex, rangeLength);
  if (changed)
    syncIX(numWords);
  return changed;
}

bool ShellNetBidirectA::resolveExternalDrive(size_t width)
{
  size_t numWords = sCalcNumWords(width);
  syncXI(numWords);
  bool changed = false;
  for (size_t i = 0; i < numWords - 1; ++i)
  {
    changed |= primResolveXdrive(&mXDrive[i], &mIDrive[i], 32);
    width -= 32;
  }
  
  changed |= primResolveXdrive(&mXDrive[numWords - 1], &mIDrive[numWords - 1], width);
  
  return changed;
}

void ShellNetBidirectA::getXValues(UInt32* xdata, UInt32* xdrive, size_t width) const
{
  size_t numWords = sCalcNumWords(width);
  CarbonValRW::cpSrcToDest(xdata, mXData, numWords);
  CarbonValRW::cpSrcToDest(xdrive, mXDrive, numWords);
}

void ShellNetBidirectA::copyValXDriveWord(UInt32* val, UInt32* drv, int index, size_t) const
{
  *val = mXData[index];
  *drv = mXDrive[index];
}

void ShellNetBidirectA::getValuePtrs(const UInt32** val, const UInt32** xdrv, const UInt32** idrv) const
{
  *xdrv = mXDrive;
  *idrv = mIDrive;
  *val = mIData;
}

bool ShellNetBidirectA::hasDriveConflict(size_t width) const
{
  size_t divWords = (width - 1)/(sizeof(UInt32) * 8);
  // Just do up to the last word
  bool hasConflict = false;
  for (size_t i = 0; i < divWords && !hasConflict; ++i)
  {
    hasConflict = ~(mXDrive[i] | ~mIDrive[i]);
  }

  if (! hasConflict)
  {
    UInt32 mask = CarbonValRW::getWordMask(UInt32(width));
    mask = ~mask;
    hasConflict = ~(mXDrive[divWords] | ~mIDrive[divWords] | mask);
  }
  return hasConflict;
}

bool ShellNetBidirectA::hasDriveConflictRange(size_t index, size_t length, 
                                              size_t) const
{
  size_t wordIndex = (index + 31)/32;
  size_t numWords = length/32;
  
  size_t i;
  bool hasConflict = false;
  // First try the direct word indexing if there are more than 2
  // words.
  if (numWords > 2)
  {
    for (i = wordIndex + 1; i < (numWords - 1) && ! hasConflict; ++i)
      hasConflict = ~(mXDrive[i] | ~mIDrive[i]);
  }

  if (! hasConflict)
  {
    // bummer, we have to mask the last word and check that
    UInt32 lastMask = CarbonValRW::getWordMask(index + length);
    lastMask = ~lastMask;
    UInt32 lastWordIndex = numWords - 1;
    hasConflict = ~(mXDrive[lastWordIndex] | ~mIDrive[lastWordIndex] | lastMask);

    if (! hasConflict)
    {
      // double bummer, have to mask the first word and check that
      UInt32 firstMask = ~0;
      size_t normalIndex = index % 32;
      if (normalIndex > 0)
        firstMask <<= normalIndex;
      firstMask = ~firstMask;
      hasConflict = ~(mXDrive[wordIndex] | ~mIDrive[wordIndex] | firstMask);
    }
  }

  return hasConflict;
}

UInt32 ShellNetBidirectA::getXDriveWord(size_t index, size_t) const
{
  return mXDrive[index];
}

void ShellNetBidirectA::signExtend(UInt32 numWords, UInt32 bitWidth)
{
  syncXI(numWords);
  CarbonValRW::signExtendVector(mXData, numWords, bitWidth);
  CarbonValRW::signExtendVector(mXDrive, numWords, bitWidth);
  syncIX(numWords);
}

void ShellNetBidirectA::getAllPtrs(UInt32** xval, UInt32** xdrv, UInt32** ival, UInt32** idrv)
{
  *xval = mXData;
  *xdrv = mXDrive;
  getIPtrs(ival, idrv);
}

bool ShellNetBidirectA::compareCalcValue(const UInt32* val, const UInt32* drv, size_t width) const
{
  size_t numWords = sCalcNumWords(width);
  bool changed = (CarbonValRW::memCompare(val, mIData, numWords) != 0); 
  for (size_t i = 0; !changed & (i < numWords); ++i)
    changed = drv[i] != (mXDrive[i] & ~mIDrive[i]);
  return changed;
}
