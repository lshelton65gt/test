// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
// Determine candidate collapse clocks directives based on a VCD file.
// Finds all clocks using iodb.  Produces a compact canonical representation
// for the value-change information, and hashes based on that so we
// can find equivalence classes of clocks

#include "iodb/IODB.h"
#include "iodb/IODBRuntime.h"
#include "waveform/WfVCDFileReader.h"
#include "util/UtIOStream.h"
#include "util/StringAtom.h"
#include "util/ShellMsgContext.h"
#include "util/ArgProc.h"
#include "util/AtomicCache.h"
#include "iodb/ScheduleFactory.h"
#include "shell/CarbonDBRead.h"
#include "exprsynth/ExprFactory.h"
#include "util/UtHashSet.h"
#include "util/UtConv.h"
#include "util/UtUInt64Factory.h"

#if pfLINUX || pfLINUX64
#include "waveform/WfFsdbFileReader.h"
#endif

class ValueChange
{
public: CARBONMEM_OVERRIDES
  ValueChange(UInt64 deltaTime, StringAtom* val)
    : mDeltaTime(deltaTime),
      mValue(val)
  {}

  ValueChange(): mDeltaTime(0), mValue(0) {}

  ValueChange(const ValueChange& src)
    : mDeltaTime(src.mDeltaTime),
      mValue(src.mValue)
  {}

  ValueChange& operator=(const ValueChange& src)
  {
    if (&src != this)
    {
      mDeltaTime = src.mDeltaTime;
      mValue = src.mValue;
    }
    return *this;
  }
    
  // Used for associative lookups
  size_t hash() const {
    return ((size_t) mDeltaTime) + ((size_t) mValue);
  }
  
  bool operator==(const ValueChange& other)
    const
  {
    return ((mDeltaTime == other.mDeltaTime) &&
            (mValue == other.mValue));
  }
  
  bool operator!=(const ValueChange& other)
    const
  {
    return ! (*this == other);
  }

  void print()
  {
    UtIO::cout() << "(" << mDeltaTime << "," << mValue->str() << ")";
  }

private:
  UInt64 mDeltaTime;
  StringAtom* mValue;
}; // class ValueChange

class ValueChangePair
{
public: CARBONMEM_OVERRIDES
  ValueChangePair(const ValueChange& vc1, const ValueChange& vc2)
    : mFirst(vc1),
      mSecond(vc2),
      mRepeatCount(1)
  {
  }

  ValueChangePair(const ValueChangePair& src)
    : mFirst(src.mFirst),
      mSecond(src.mSecond),
      mRepeatCount(src.mRepeatCount)
  {
  }

  void bump() {++mRepeatCount;}
  size_t hash() const
  {
    return mFirst.hash() + mSecond.hash() + mRepeatCount;
  }
  bool operator==(const ValueChangePair& other)
    const
  {
    return ((mFirst == other.mFirst) &&
            (mSecond == other.mSecond) &&
            (mRepeatCount == other.mRepeatCount));
  }
  bool operator!=(const ValueChangePair& other)
    const
  {
    return ! (*this == other);
  }

  bool isSimilar(const ValueChangePair& other)
  {
    return ((mFirst == other.mFirst) &&
            (mSecond == other.mSecond));
  }

  void print()
  {
    if (mRepeatCount > 1)
      UtIO::cout() << mRepeatCount << "{";
    mFirst.print();
    mSecond.print();
    if (mRepeatCount > 1)
      UtIO::cout() << "}";
  }

  size_t numTransitions()
  {
    return 2*mRepeatCount;
  }

private:
  ValueChange mFirst;
  ValueChange mSecond;
  UInt32 mRepeatCount;
}; // class ValueChangePair

class TimeSignature
{
public: CARBONMEM_OVERRIDES
  TimeSignature(bool isPrimary)
  {
    mIsPrimary = isPrimary;
    clear();
  }

  TimeSignature(const TimeSignature& src)
    : mCurrentTime(src.mCurrentTime),
      mLastValid(src.mLastValid),
      mLast(src.mLast),
      mPairVec(src.mPairVec),
      mClocks(src.mClocks),
      mNumTransitions(src.mNumTransitions)
  {
  }

  TimeSignature& operator=(const TimeSignature& src)
  {
    if (&src != this)
    {
      mCurrentTime = src.mCurrentTime;
      mLastValid = src.mLastValid;
      mLast = src.mLast;
      mPairVec = src.mPairVec;
      mClocks = src.mClocks;
      mNumTransitions = src.mNumTransitions;
    }
    return *this;
  }

  void clear()
  {
    mCurrentTime = 0;
    mPairVec.clear();
    mLastValid = false;
    mNumTransitions = 0;
  }

  void push_back(UInt32 time, StringAtom* value)
  {
    ValueChange vc(time - mCurrentTime, value);
    mCurrentTime = time;
    // Pair up even value changes
    if (mLastValid)
    {
      ValueChangePair vcPair(mLast, vc);
      if (!mPairVec.empty() && mPairVec.back().isSimilar(vcPair))
        mPairVec.back().bump();
      else
        mPairVec.push_back(vcPair);
      mLastValid = false;
    }
    else
    {
      mLastValid = true;
      mLast = vc;
    }
    ++mNumTransitions;
  }

  size_t hash() const
  {
    size_t h = 0;
    if (mLastValid)
      h = mLast.hash();
    for (size_t i = 0; i < mPairVec.size(); ++i)
      h = 17*h + mPairVec[i].hash();
    return h;
  }

  bool operator==(const TimeSignature& other)
    const
  {
    if (mLastValid != other.mLastValid)
      return false;
    if (mLastValid && (mLast != other.mLast))
      return false;
    if (mPairVec.size() != other.mPairVec.size())
      return false;
    for (size_t i = 0; i < mPairVec.size(); ++i)
      if (mPairVec[i] != other.mPairVec[i])
        return false;
    return true;
  }

  void addClock(STSymbolTableNode* node)
  {
    mClocks.push_back(node);
  }

  void print()
  {
    for (size_t i = 0; i < mPairVec.size(); ++i)
      mPairVec[i].print();
    if (mLastValid)
      mLast.print();
  }

  void printClockSpeedDirectives()
  {
    UtString buf;
    for (size_t i = 0; i < mClocks.size(); ++i)
    {
      buf << "\nclockSpeed " << this->numTransitions() << " ";
      mClocks[i]->compose(&buf,true );     // include root in name 
    }
    UtIO::cout() << buf;
  }

  void printClocks()
  {
    UtString buf;
    for (size_t i = 0; i < mClocks.size(); ++i)
    {
      if (i != 0)
        buf << " \\\n    ";
      mClocks[i]->compose(&buf, true);     // include root in name 
    }
    UtIO::cout() << buf;
  }

  // Sort based on the name of the first clock.  We know that the first clock
  // listed will be the earliest because the data structure gets populated
  // in an algorithm driven off the order of IODB::loopClocks, which itself
  // is sorted.  We assert this when adding a new clock.
  bool operator<(const TimeSignature& other)
  {
    INFO_ASSERT(!mClocks.empty(), "Can't sort empty TimeSignature");
    INFO_ASSERT(!other.mClocks.empty(), "Can't sort empty TimeSignature");
    return *mClocks.front() < *other.mClocks.front();
  }

  size_t numClocks() {return mClocks.size();}

  // How many total clock transitions are there?
  size_t numTransitions() {
    return mNumTransitions;
  }

  // How many need to be printed, taking advantage of repeat counts
  size_t numPrintTransitions()
  {
    return 2*mPairVec.size() + (mLastValid? 1: 0);
  }

  bool isPrimary() {return mIsPrimary;}

private:
  UInt64 mCurrentTime;
  bool mLastValid;
  bool mIsPrimary;
  ValueChange mLast;
  UtVector<ValueChangePair> mPairVec;
  UtVector<STSymbolTableNode*> mClocks;
  size_t mNumTransitions;
}; // class TimeSignature

typedef UtVector<WfAbsFileReader*> VcdFileVector;

class Clock
{
public: CARBONMEM_OVERRIDES

  Clock(STSymbolTableNode* name, bool isPrimary)
  {
    mName = name;
    mWaves = NULL;
    mTimeSignature = new TimeSignature(isPrimary);
  }
  ~Clock()
  {
    delete mTimeSignature;
  }

  STSymbolTableNode* mName;
  WfAbsSignal* mWaves;
  TimeSignature* mTimeSignature;
};

static int findCollapseClocks(const VcdFileVector& vcdFiles,
                               IODB* iodb,
                               const char* basePath)
{
  int ret = 0;
  typedef UtVector<Clock*> SigVec;

  // Keep a factory of unique time signatures
  typedef UtHashSet<TimeSignature*, HashPointerValue<TimeSignature*> >
    SigFactory;
  SigFactory sigFactory;


  // compute signal vector based on clock information from IODB
  SigVec sigVec;
  for (IODB::NameSetLoop p = iodb->loopClockTree(); !p.atEnd(); ++p)
  {
    STSymbolTableNode* clock = *p;
    sigVec.push_back(new Clock(clock, true)); // do not have WfAbsSignal* yet
  }

  for (IODB::NameSetLoop p = iodb->loopClocks(); !p.atEnd(); ++p)
  {
    STSymbolTableNode* clock = *p;
    if (!iodb->isClockTree(clock))
      sigVec.push_back(new Clock(clock, false)); // do not have WfAbsSignal* yet
  }

  AtomicCache* atomicCache = iodb->getAtomicCache();
  UtString val, sigName;

  // read all the waves
  UInt64 timeOffset = 0;
  for (size_t i = 0; i < vcdFiles.size(); ++i)
  {
    UtString waveMsg;
    WfAbsFileReader::Status stat;
    WfAbsFileReader* waveFile = vcdFiles[i];
    UtString reason;
    if (waveFile->loadSignals(&reason) != WfAbsFileReader::eOK)
    {
      fprintf(stderr, "Cannot open waveform file %s\n", reason.c_str());
      return 1;
    }
    
    // Watch all the clock signal waveforms
    for (size_t j = 0; j < sigVec.size(); ++j)
    {
      Clock* clock = sigVec[j];
      sigName.clear();
      if (*basePath != '\0') {
        sigName << basePath << ".";
        clock->mName->compose(&sigName, false); // if we have a base path, do not include root in name 
      } else {
        clock->mName->compose(&sigName, true); // otherwise, include root in name 
      }
      WfAbsSignal* signal = waveFile->watchSignal(sigName.c_str());
      if (signal != NULL)
        clock->mWaves = signal;
      else
      {
        UtIO::cout() << "Error: File " << waveFile->getFileName() << ": "
                     << "# no waveforms for clock " << sigName << "\n";
        ret = 1;
      }
    }
    stat = waveFile->loadValues(&waveMsg);
    if (stat != WfAbsFileReader::eOK)
    {
      UtIO::cout() << waveMsg << UtIO::endl;
      ret = 1;
    }
    else
    {
      for (size_t j = 0; j < sigVec.size(); ++j)
      {
        Clock* clock = sigVec[j];
        TimeSignature* ts = clock->mTimeSignature;
        WfAbsSignal* sig = clock->mWaves;

        if (sig != NULL)
        {
          UtString vcIterMsg;

          WfAbsSignalVCIter *v = waveFile->createSignalVCIter(sig, &vcIterMsg);
          if (! vcIterMsg.empty())
            fprintf(stderr, "%s\n", vcIterMsg.c_str());

          bool cont = true;
          while (cont) {
            cont = !v->atMaxTime();
            val.clear();
            v->getValueStr(&val);
            StringAtom* valSym = atomicCache->intern(val.c_str(), val.size());
            ts->push_back(v->getTime() + timeOffset, valSym);
            v->gotoNextTime();
          }
          delete v;
        }
      }
    } // else

    timeOffset += waveFile->getMaxSimulationTime();
    delete waveFile;
  } // for

  // Having constructed a candidate time signatures for each signal, insert
  // them in the factory, get the canonical signature, and collate
  for (size_t j = 0; j < sigVec.size(); ++j)
  {
    Clock* clock = sigVec[j];
    TimeSignature* ts = clock->mTimeSignature;
    TimeSignature* canonicalTimeSig = NULL;
    SigFactory::iterator p = sigFactory.find(ts);
    if (p == sigFactory.end())
    {
      canonicalTimeSig = ts;
      sigFactory.insert(canonicalTimeSig);
    }
    else
      canonicalTimeSig = *p;
    canonicalTimeSig->addClock(clock->mName);
  }

  // Now walk through the canonical time signatures and print
  // out canonical clock declarations that tie all like clocks
  // together
  for (SigFactory::SortedLoop p = sigFactory.loopSorted(); !p.atEnd(); ++p)
  {
    TimeSignature* sig = *p;
    
    UtIO::cout() << "# ";
    if (sig->isPrimary())
      UtIO::cout() << " PRIMARY ";
    UtIO::cout() << sig->numTransitions() << " transitions";
    if (sig->numPrintTransitions() <= 6)
    {
      UtIO::cout() << ": ";
      sig->print();
    }
    // Print a clockSpeed directive for this clock based on the number
    // of transitions.
    sig->printClockSpeedDirectives();
   
    if (sig->numClocks() > 1)
      UtIO::cout() << "\ncollapseClock ";
    else
      UtIO::cout() << "\n# clock ";
    sig->printClocks();
    UtIO::cout() << "\n" << UtIO::endl;
  }
  return ret;
} // static int findCollapseClocks

int main(int argc, char** argv)
{
  AtomicCache atomicCache;
  DynBitVectorFactory dynFactory(true);
  UtUInt64Factory timeFactory;

  MsgStreamIO errStream(stderr, true);
  MsgContext msgContext;
  ESFactory exprFactory;
  SCHScheduleFactory scheduleFactory;

  msgContext.addReportStream(&errStream);
  ShellSymTabBOM bom;
  bom.putScheduleFactory(&scheduleFactory);
  
  STSymbolTable designSymTab(&bom, &atomicCache);
  // No source locator factory is needed.
  IODBRuntime iodb(&atomicCache, &designSymTab,
                   &msgContext, &scheduleFactory, &exprFactory, NULL);

  ArgProc args;
  args.addString("-basePath",
                 "Path to the DUT in VCD file. Parent hierarchy is ignored.",
                 "", false, false, 1);
  args.addBool("-fsdb",
               "The VCD file(s) is(are) in FSDB Format. Only works on Linux", false, 1);
  args.createSection("File Control");
  args.addToSection("File Control", "-basePath");
  args.addToSection("File Control", "-fsdb");
  args.setDescription("Clock Analysis Utility", "analyzeClocks",
                      "Examine a set of waveform files looking for clocks "
                      "that have similar behavior.  Emits directives file "
                      "that, after manual inspection, could potentially be "
                      "used with SpeedCompiler to optimize the design clocks.  "
                      "An IODB file must be supplied -- it is used by "
                      "analyzeClocks to determine the set of clock nets.");
  int numOptions;
  UtString errmsg;
  ArgProc::ParseStatusT parseStat =
    args.parseCommandLine(&argc, argv, &numOptions, &errmsg);
  if (parseStat == ArgProc::eParseError)
  {
    UtIO::cout() << errmsg;
    return 1;
  }
  
  if (argc < 3)
  {
    UtIO::cout() << "analyzeClocks [options] db_file wavefile1 wavefile2 ..." << UtIO::endl;
    UtString usage;
    args.getUsageVerbose(&usage);
    UtIO::cout() << usage << UtIO::endl;
    return 1;
  }
  
  UtString dbfile(argv[1]);
  CarbonDBType dbType = eCarbonFullDB;
  if (dbfile.rfind(IODB::scIODBExt) != UtString::npos)
    dbType = eCarbonIODB;
  else if (dbfile.rfind (IODB::scGuiDBExt) != UtString::npos)
    dbType = eCarbonGuiDB;
  
  if (!iodb.readDB(argv[1], dbType))
    return 1;
  
  VcdFileVector vcdFiles;
  bool isFsdb = args.getBoolValue("-fsdb");
  for (int i = 2; i < argc; ++i)
  {
    WfAbsFileReader* waveFile = NULL;
    if (isFsdb)
    {
#if pfLINUX || pfLINUX64
      waveFile = WfFsdbFileReaderCreate(argv[i], &atomicCache, &dynFactory, &timeFactory);
#else
      fprintf(stderr, "fsdb format is only supported on Linux.\n");
      exit(1);
#endif
    }
    else
      waveFile = new WfVCDFileReader(argv[i], &atomicCache, &dynFactory, &timeFactory);
    vcdFiles.push_back(waveFile);
  }

  const char* basePath;
  args.getStrValue("-basePath", &basePath);
  int ret = findCollapseClocks(vcdFiles, &iodb, basePath);
  if (ret != 0)
    UtIO::cout() << "***Errors were encountered.\n" << UtIO::endl;
  return ret;
} // int main
