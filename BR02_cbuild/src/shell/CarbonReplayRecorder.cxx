// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// Needed for windows compile. Without this I get:
/*
/tools/windows/msvc-2003/include/vector(377) : warning C4530: C++ exception handler used, but unwind semantics are not enabled. Specify /EHsc
*/
#include "util/CarbonPlatform.h"


#include "shell/CarbonReplayRecorder.h"
#include "util/Zstream.h"

namespace CarbonReplay {

  // ReplayRecorder definition
  ReplayRecorder::ReplayRecorder(const UtString& dbName, const char* fileName, CarbonModel* model, CarbonReplayInfo* replayInfo) : 
    mIndexStream(fileName), mCarbonModel(model), mStatus(eCarbon_OK),
    mReplayInfo(replayInfo), mNumEventFiles(0), mCurCheckpointNum(0),
    mNumSchedCallsSinceChkPt(0), mTotNumSchedCalls(0), mCheckpointFreq(0),
    mRecordInputBuffer(NULL), mRecordResponseBuffer(NULL),
    mInputValueChangeCallback(new ReplayChangeCallback(mRecordStream, eStimuli)),
    mResponseValueChangeCallback(new ReplayChangeCallback(mRecordStream, eResponse)),
    mDBName(&dbName), mRecordedScheduleCall(false)
  {
    recomputeCheckpointFreq();
    
    mIndexStream << scIndexComment << " Carbon Design Systems Replay Database" << UtIO::endl;
    openNextEventFile();
  }
  
  
  ReplayRecorder::~ReplayRecorder()
  {
    if (mCarbonModel->getRunMode() == eCarbonRunRecord)
      // This happens when we completely record a simulation (stop
      // was not called).
      mCarbonModel->getHookup()->runModeChangeFn(eCarbonRunRecord,
                                                 eCarbonRunNormal);
    
    // Close the record and index stream if it is open
    closeRecordFile();
    if (!mIndexStream.close())  {
      mCarbonModel->getMsgContext()->SHLFileProblem(mIndexStream.getErrmsg());
    }
    
    removeMemoryCallbacks();
    
    for (RecResponseVec::iterator p = mRecordResponseNets.begin(),
           e = mRecordResponseNets.end(); p != e; ++p)
      delete *p;
    
    for (SInt32HashSetVec::iterator p = mAddrChangeSets.begin(),
           e = mAddrChangeSets.end(); p != e; ++p)
      delete *p;
    
    for (MemWriteInfoVec::iterator p = mInternalMemWriteInfos.begin(),
           e = mInternalMemWriteInfos.end(); p != e; ++p)
      delete *p;
    
    delete mRecordInputBuffer;
    delete mRecordResponseBuffer;
    delete mInputValueChangeCallback;
    delete mResponseValueChangeCallback;
  }
  

  void ReplayRecorder::writeStimulusTouchedSize(ZostreamDB& out) const
  {
    mStimuliTouched.writeSize(out);
  }
  
  // ReplayCModel definition

  ReplayCModel::ReplayCModel(void* cmodelData, void* userData, const char* name, UInt32 cmodelId)
    : mCModelStorage(cmodelData), mCModelUserData(userData), mCModelId(cmodelId), mName(name)
  {}

  ReplayCModel::~ReplayCModel()
  {
    for (ReplayCModelCallVec::iterator p = mContexts.begin(),
           e = mContexts.end(); p != e; ++p)
      delete *p;
  }

  void ReplayCModel::addContext(UInt32 contextId, UInt32 numInputs,
                                UInt32 numOutputs, CModelPlaybackFn fn)
  {
    maybeResizeContextArray(contextId);
    // This might be called twice from the initial schedule if we
    // diverged during the initial schedule. So, only add it if we
    // haven't seen it.
    if (mContexts[contextId] == NULL) {
      ReplayCModelCall* replayContext = new ReplayCModelCall(contextId,
                                                             numInputs,
                                                             numOutputs,
                                                             fn,
                                                             this);
      mContexts[contextId] = replayContext;
    }
  }


  void ReplayCModel::preRecordCall(EventDBWriter& dbWriter, UInt32 contextId, UInt32* outputs)
  {
    INFO_ASSERT(contextId < mContexts.size(), mName.c_str());
    ReplayCModelCall* cmodelContext = mContexts[contextId];
    INFO_ASSERT(cmodelContext, mName.c_str());
#if REPLAY_CMODEL_VERBOSE
    UtIO::cout() << "Id: " << mCModelId << UtIO::endl;
#endif
    dbWriter << eCModelCall << mCModelId;
    cmodelContext->preRecordCall(dbWriter, outputs);
  }
  
  //! Record a call to this cmodel. Records the context as well
  void ReplayCModel::recordCall(EventDBWriter& dbWriter, UInt32 contextId, UInt32* inputs, UInt32* outputs)
  {
    INFO_ASSERT(contextId < mContexts.size(), mName.c_str());
    ReplayCModelCall* cmodelContext = mContexts[contextId];
    INFO_ASSERT(cmodelContext, mName.c_str());
    cmodelContext->recordCall(dbWriter, inputs, outputs);
  }

  void ReplayCModel::maybeResizeContextArray(UInt32 contextId)
  {
    // Put a limit on the size of the array. Assert if the contextID
    // is greater than 10000000, which is pretty huge.
    // This protects against a code-gen screw up.
    INFO_ASSERT(contextId <= 10000000, mName.c_str());
    sFitIndexIntoArray(mContexts, contextId);
  }

  void ReplayCModel::clearOutputShadowBuffers()
  {
    for (UInt32 i = 0; i < mContexts.size(); ++i) {
      mContexts[i]->clearOutputShadowBuffer();
    }
  }

  static const char* sStimuliTouchedSig = "StimuliTouched_obj";
  static UInt32 sStimuliTouchedVersion = 1;

  // StimuliTouched class
  StimuliTouched::StimuliTouched() {}
  
  bool StimuliTouched::dbWrite(ZostreamDB& out) const
  {
    out << sStimuliTouchedSig << sStimuliTouchedVersion;
    mTouchedClksAndStateOutputs.getBuffer()->dbWrite(out);
    mTouchedOther.getBuffer()->dbWrite(out);
    return ! out.fail();
  }

  bool StimuliTouched::dbRead(ZistreamDB& in)
  {
    UtString sig;
    UInt32 version = 0;
    in >> sig >> version;
    if (in.fail())
      return false;

    if (sig.compare(sStimuliTouchedSig) != 0)
    {
      in.setError("StimuliTouched Signature mismatched.");
      return false;
    }
    
    if (version != sStimuliTouchedVersion)
    {
      in.setError("The libcarbon used to read this file is from a different Carbon release than the design was compiled with.");
      return false;
    }

    mTouchedClksAndStateOutputs.getBuffer()->dbRead(in);
    mTouchedOther.getBuffer()->dbRead(in);
    return ! in.fail();
  }

  bool StimuliTouched::writeSize(ZostreamDB& out) const
  {
    out << mTouchedClksAndStateOutputs.getSize();
    out << mTouchedOther.getSize();
    return ! out.fail();
  }

  bool StimuliTouched::readSize(ZistreamDB& in)
  {
    UInt32 other = 0;
    UInt32 clksAndStateOutputs = 0;
    in >> clksAndStateOutputs;
    in >> other;
    if (in.fail())
      return false;
    
    mTouchedClksAndStateOutputs.putSize(clksAndStateOutputs);
    mTouchedOther.putSize(other);
    return true;
  }


  // ReplayCheckpointClosure 

  // 2: Added Response value/touched. Useless Touched is for future use.
  // 3: Divided touched stimuli into clk and nonclk
  // 4: Added cmodel shadow outputs
  // 5: Repartitioned StimuliTouched for state outputs, added masked
  //    flag to Touched class, added mask buffer to playback/record nets
  const UInt32 ReplayCheckpointClosure::cVersion = 5;
  
  ReplayCheckpointClosure::ReplayCheckpointClosure(DynBitVector* stim,
                                                   StimuliTouched* stimTouched,
                                                   DynBitVector* response,
                                                   DynBitVector* responseTouched,
                                                   ReplayCModelMap *cModelMap)
    : mReplayStimuli(stim),
      mReplayResponse(response), mReplayTouchedResponse(responseTouched),
      mReplayStimTouched(stimTouched)
  {
    // The cmodels are stored in a <void*, ReplayCModel> map, with the
    // void* being the address of the actual cmodel object instance.
    // This isn't convenient for us, so we'll make our own map keyed
    // by cmodel id.
    for (ReplayCModelMap::UnsortedLoop l(cModelMap->loopUnsorted()); !l.atEnd(); ++l) {
      ReplayCModel *cmodel = (*l).second;
      UInt32 id = cmodel->getId();
      mIdCModelMap[id] = cmodel;
    }
  }
  
  //! writes this object to the stream
  bool ReplayCheckpointClosure::dbWrite(ZostreamDB& out) const
  {
    out << cVersion;
    mReplayStimuli->dbWrite(out);
    mReplayStimTouched->dbWrite(out);
    mReplayResponse->dbWrite(out);
    mReplayTouchedResponse->dbWrite(out);
    writeCModelOutputs(out);
    return ! out.fail();
  }
  
  //! reads this object from the stream
  bool ReplayCheckpointClosure::dbRead(ZistreamDB& in)
  {
    UInt32 version = 0;
    in >> version;
    // This should never happen, but just in case...
    if (version != cVersion)
      in.setError("The libcarbon used to read this file is from a different Carbon release than the design was compiled with.", true);
    
    mReplayStimuli->dbRead(in);
    mReplayStimTouched->dbRead(in);
    mReplayResponse->dbRead(in);
    mReplayTouchedResponse->dbRead(in);
    readCModelOutputs(in);
    
    return ! in.fail();
  }

  void ReplayCheckpointClosure::writeCModelOutputs(ZostreamDB &out) const
  {
    // Write the shadow outputs for each cmodel
    UInt32 numCModels = mIdCModelMap.size();
    // Write the number of cmodels to the db.  If recording is started
    // before carbonInitialize() is called, the cmodels will not yet
    // have been registered, and this will be zero.
    out << numCModels;

    for (IdCModelMap::SortedLoop l(mIdCModelMap.loopSorted()); !l.atEnd(); ++l)
    {
      // Save the cmodel's ID
      UInt32 id = (*l).first;
      ReplayCModel *cmodel = (*l).second;
      out << id;
      // We don't actually need to save the number of contexts, but it's a good sanity check
      UInt32 numContexts = cmodel->numContexts();
      out << numContexts;
      for (UInt32 i = 0; i < numContexts; ++i) {
        ReplayCModelCall *call = cmodel->getContext(i);
        // Again, we don't need to save the number of output words for
        // this context, but it's nice to know if we screwed up upon recovery.
        UInt32 numWords = call->numOutputWords();
        out << numWords;
        // Save the value of the output buffers for this context
        out.write(call->getOutputShadowBuffer(), numWords * sizeof(UInt32));
      }
    }
  }
  
  void ReplayCheckpointClosure::readCModelOutputs(ZistreamDB &in)
  {
    // First, see how many cmodels were saved.
    UInt32 expectedNumCModels = mIdCModelMap.size();
    UInt32 numCModels = 0;
    in >> numCModels;

    // If this checkpoint was taken before carbonInitialize() was
    // called, there's no cmodel information because they hadn't been
    // registered yet.  That's OK.  We'll just skip reading the db,
    // and re-initialize their outputs to zero.
    if (numCModels == 0) {
      for (IdCModelMap::SortedLoop l(mIdCModelMap.loopSorted()); !l.atEnd(); ++l) {
        ReplayCModel *cmodel = (*l).second;
        cmodel->clearOutputShadowBuffers();
      }
    } else {
      // Yes, there are a lot of asserts here, but recovery bugs are
      // tough to debug as it is.  Cmodels only add to that.  This
      // function is only called once at the start of recovery, so it
      // shouldn't be a performance problem.
      INFO_ASSERT(numCModels == expectedNumCModels, "Wrong number of cmodels");
      for (UInt32 i = 0; i < numCModels; ++i) {
        // Get the cmodel's ID, and make sure it's valid.
        UInt32 id = numCModels;
        in >> id;
        INFO_ASSERT(id < numCModels, "Invalid cmodel id");
        IdCModelMap::iterator iter = mIdCModelMap.find(id);
        INFO_ASSERT(iter != mIdCModelMap.end(), "Cmodel not found");
        ReplayCModel *cmodel = iter->second;
        // Read the number of contexts saved and validate.
        UInt32 expectedNumContexts = cmodel->numContexts();
        UInt32 numContexts = 0;
        in >> numContexts;
        INFO_ASSERT(numContexts == expectedNumContexts, "Wrong number of contexts");
        for (UInt32 j = 0; j < numContexts; ++j) {
          // For each context, validate the number of output words saved
          ReplayCModelCall *call = cmodel->getContext(j);
          UInt32 expectedNumWords = call->numOutputWords();
          UInt32 numWords = 0;
          in >> numWords;
          INFO_ASSERT(numWords == expectedNumWords, "Wrong number of output words");
          // Copy the saved outputs to the cmodel call's shadow buffer
          UInt32 *buf = call->getOutputShadowBuffer();
          in.read(reinterpret_cast<char*>(buf), numWords * sizeof(UInt32));
        }
      }      
    }
  }
}

