/*******************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#include "util/CarbonPlatform.h"
#include "codegen/carbon_priv.h"
#include "iodb/IODBRuntime.h"
#include "shell/ApiId.h"
#include "shell/CarbonModel.h"
#include "shell/CarbonHookup.h"
#include "shell/Profile.h"
#include "util/OSWrapper.h"
#include "util/UtString.h"
#include <algorithm>
#include <functional>
#if pfWINDOWS
#include <process.h>            // For _beginthread()
#endif

// Static member definitions.
Profile* Profile::smTheInstance = NULL;
#if pfWINDOWS
bool Profile::smSampling = false;
#endif

Profile::Profile() : mSampleCount(0)
{
// Copied from ShellGlobals.cxx
#define MAX_MODELS 200
#if !pfWINDOWS
  memset(&mPrevAction, 0, sizeof(struct sigaction));
  memset(&mPrevItimerval, 0, sizeof(itimerval));
#endif // !pfWINDOWS
  startSampling();
}

Profile::~Profile()
{
  stopSampling();

  if (!mModelsInfo.empty()) {
    // Write buckets for each model to data file for use by profiling
    // report program.
    UtOFStream dataFile("carbon_profile.dat");
    UtString buffer;
    dataFile << "Carbon profiling data -- This file is input to the"
             << " `carbon profile' command\n"
             << OSGetTimeStr("%b %d, %Y %H:%M", &buffer) << UtIO::endl
             << mSampleCount << UtIO::endl; // Total samples
    for (ModelsInfo::iterator i = mModelsInfo.begin();
         i != mModelsInfo.end();
         ++i) {
      save(i->second, dataFile);
      delete i->second;          // Destroy ModelInfo
    }
  }

  // Prevent reusing singleton after it's deleted.
  smTheInstance = NULL;
}

void
Profile::save(ModelInfo* modelInfo, UtOFStream& stream)
{
  // First write name of design.
  // Should we verify none of these pointers is NULL?
  stream << modelInfo->getName() << UtIO::endl;
  // Or rather than name of design, how about it's UID?
  stream << modelInfo->getUID() << UtIO::endl;
  if (modelInfo->mUnprofiledHits > 0) {
    // This model was compiled without -profile, so we know only the
    // number of hits while the model was active -- we don't know the
    // number of block ,schedule, or API hits.
    stream << "unprofiled " << modelInfo->mUnprofiledHits << UtIO::endl;
    return;
  }

  // We drop down to here only if the model was compiled with -profile.

  // Write number of block and schedule buckets.
  stream << modelInfo->mModelBuckets.size() << ' '
         << modelInfo->mModelScheduleBuckets.size() << ' ';
  // Write number of API entries sampled (non-zero).
  unsigned numAPIs = std::count_if(modelInfo->mModelAPIs.begin(),
                                   modelInfo->mModelAPIs.end(),
                                   bind2nd(std::not_equal_to<unsigned>(), 0));
  stream << numAPIs << UtIO::endl;
  // Write each model bucket.
  for (ShellGlobal::ModelBuckets::iterator i
         = modelInfo->mModelBuckets.begin();
       i != modelInfo->mModelBuckets.end();
       ++i)
    stream << *i << UtIO::endl;
  // Write each model schedule bucket.
  for (ShellGlobal::ModelBuckets::iterator i
         = modelInfo->mModelScheduleBuckets.begin();
       i != modelInfo->mModelScheduleBuckets.end();
       ++i)
    stream << *i << UtIO::endl;
  // Write non-zero API buckets, with function names.
  for (unsigned i = 0; i < modelInfo->mModelAPIs.size(); i++)
    if (modelInfo->mModelAPIs[i] != 0) {
      extern const char* gApiFunctionNames[];
      stream << gApiFunctionNames[i] << ' ' << modelInfo->mModelAPIs[i]
             << UtIO::endl;
    }
}

// The signal handler that does the sampling.

// Increment element in this model's bucket array for the current
// bucket.

// Note that this function must not call any non-reentrant code that
// may have been executing when the signal is delivered.  For example,
// it must not do anything that allocates memory.
void
Profile::SIGPROFsignalHandler(int /*sig*/)
{
  //  UtIO::cerr() << "Invocation " << mSampleCount << '\n';
  Profile* profile = instance();
  ModelsInfo& modelsInfo = profile->mModelsInfo;
  const CarbonModel* model = ShellGlobal::gGetCurrentModel()->getModel ();
  if (model) {
    // The model will be set if we're in carbon_<design>_init(),
    // carbon_<design>_schedule(), or carbon_<design>_create(), or if
    // we're in an API (managed by a Profile::CurrentAPI instance).
    // We increment the API bucket only if we're not in a block,
    // because we partition the runtime into blocks and APIs that
    // are treated and displayed similarly and total to 100% of
    // runtime.
    ModelInfo* info = modelsInfo[model];
    if (info == NULL) {
      // Model must have been compiled without -profile.
      profile->addModel(model, model->getHookup()->getDB()->getIfaceTag());
      info = modelsInfo[model];
    }

    if (!model->compiledWithProfile()) {
      // This model was compiled w/o -profile, so we can't track the
      // number of hits in each block or API function, but we can
      // count the total number of hits for the unprofiled model and
      // report that.
      ++(info->mUnprofiledHits);
    }
    else {
      if (model->getBlockBucket() >= 0)
        // We're in a block
        ++(info->mModelBuckets[model->getBlockBucket()]);
      else {                      // Not in a block
        // Increment count for active API function.
        unsigned api = model->getCurrentAPI();
        // Note that we won't get here and double-count carbonSchedule
        // if we're in a block.
        ++(info->mModelAPIs[api]);
      }

      // In addition to block/API buckets, also maintain schedule
      // buckets.  We increment the appropriate schedule bucket
      // regardless of block/API buckets, because they're independent.
      if (model->getScheduleBucket() >= 0)
        ++(info->mModelScheduleBuckets[model->getScheduleBucket()]);
    }
  }

  // Keep track of the total number of samples taken.
  profile->mSampleCount++;
}

#if pfWINDOWS
void
Profile::threadFunc(void*)
{
  while(smSampling) {
    // We want to wake when this process has consumed a certain amount
    // of user+kernel CPU time.  But since Windows can't do that, we
    // make do with real time.  This will work better on an unloaded
    // system.
    // Maybe we could go back to sleep if the process hasn't used
    // enough CPU time.
    Sleep(10);                  // Milliseconds
    if (smSampling)             // May have changed while sleeping
      SIGPROFsignalHandler(0);
  }
}
#endif // pfWINDOWS

void
Profile::startSampling()
{
#if pfWINDOWS
  smSampling = true;
  _beginthread(threadFunc, 0, 0);
#else
  // Use sigaction() rather than signal() because on Solaris a handler
  // established with signal() is disabled when a signal is delivered.

  // First establish signal handler.  Remember previous value to
  // restore when we're done.
  struct sigaction new_action;
  // Set up the structure to specify the new action.
  new_action.sa_handler = SIGPROFsignalHandler;
  sigemptyset(&new_action.sa_mask);
  new_action.sa_flags = SA_RESTART; // Don't cause I/O to fail with EINTR

  int sa_result = sigaction(SIGPROF, &new_action, &mPrevAction);
  INFO_ASSERT(sa_result == 0, "sigaction() failed");

  // In case the user's application is using SIGPROF, inform him that
  // we're clobbering his handler while our model exists.
  if (mPrevAction.sa_handler != SIG_DFL)
    // "SIGPROF signal handler overwritten by Carbon profiling"
    ShellGlobal::getProgErrMsgr()->SHLSIGPROFChange();

  // Periodically raise SIGPROF signal.
  itimerval itv;
  // Linux's timer granularity is 10ms, use that interval
  itv.it_interval.tv_sec = 0;   // seconds
  itv.it_interval.tv_usec = 10000; // in microseconds, = 10ms
  itv.it_value = itv.it_interval; // Starting after this long

  // Set the profiling timer to raise a signal periodically.
  int result = setitimer(ITIMER_PROF, &itv, &mPrevItimerval);
  if (result != 0) {            // Failed
    UtString errmsg;
    OSGetLastErrmsg(&errmsg);
    UtString msg = "Carbon profiling: setitimer() failed: ";
    msg += errmsg;
    INFO_ASSERT(result == 0, msg.c_str());
  }

  // If the user already had a profiling timer set, tell him we're
  // clobbering it until we're done.
  if (mPrevItimerval.it_interval.tv_sec != 0
      || mPrevItimerval.it_interval.tv_usec != 0
      || mPrevItimerval.it_value.tv_sec != 0
      || mPrevItimerval.it_value.tv_usec != 0)
    // "Previous setitimer(ITIMER_PROF, ...) value overwritten by
    // Carbon profiling"
    ShellGlobal::getProgErrMsgr()->SHLsetitimerChange();

#endif // pfWINDOWS
}

void
Profile::stopSampling()
{
#if pfWINDOWS
  smSampling = false;           // Thread will exit next time it wakes.
#else
  itimerval otv;
  // Restore original value for setitimer.
  int result = setitimer(ITIMER_PROF, &mPrevItimerval, &otv);
  if (result != 0) {            // Failed
    UtString errmsg;
    OSGetLastErrmsg(&errmsg);
    UtString msg = "Carbon profiling: setitimer() failed: ";
    msg += errmsg;
    INFO_ASSERT(result == 0, msg.c_str());
  }

  // Restore original value for SIGPROF signal handler.
  sigaction(SIGPROF, &mPrevAction, NULL);
#endif // pfWINDOWS
}

// Return the singleton object.  Instantiate it if it doesn't exist.
Profile*
Profile::instance()
{
  if (smTheInstance == NULL)
    smTheInstance = new Profile;

  return smTheInstance;
}

void
Profile::destroy()
{
  delete smTheInstance;
}

void
Profile::addModel(const CarbonModel* model, const char* modelName)
{
  mModelsInfo.insert(std::make_pair(model, new ModelInfo(model, modelName)));
}

Profile::ModelInfo::ModelInfo(const CarbonModel* model, const char* modelName) :
  // Allocate the correctly sized mModelAPIs, mModelBuckets, and
  // mModelScheduleBuckets vectors.
  mModelAPIs(eApi_SIZE),
  mModelBuckets(model->getNumBlockBuckets()),
  mModelScheduleBuckets(model->getNumScheduleBuckets()),
  mUnprofiledHits(0),
  mUID(model->getHookup()->getUID()),
  mName(modelName)
{
}

// Copy constructor, fails if mModelAPIs, mModelBuckets, or
// mModelScheduleBuckets isn't empty
Profile::ModelInfo::ModelInfo(const ModelInfo& mi) :
  mUID(mi.mUID),
  mName(mi.mName)
{
  INFO_ASSERT(mModelAPIs.empty() and
              mModelBuckets.empty() and
              mModelScheduleBuckets.empty(),
              "Copying initialized ModelInfo");
}
