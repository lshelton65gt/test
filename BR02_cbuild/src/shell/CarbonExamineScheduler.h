// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// author: Mark Seneski

#ifndef __CARBONEXAMINESCHEDULER_H_
#define __CARBONEXAMINESCHEDULER_H_

#include "util/UtArray.h"
#include "util/UtHashMap.h"
#include "shell/CarbonModel.h"
#include "shell/CarbonNetValueCBData.h"

#include <string.h>
#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class CarbonValueChangeBase;

class SCHSignature;
class CarbonHookup;
class STAliasedLeafNode;
class ScheduleStimuli;
class MiniMemPool;
class SCHEvent;

typedef UtArray<CarbonValueChangeBase*> VCVec;
typedef Loop<VCVec> VCVecLoop;
typedef UtArray<CarbonShadowStruct*> StorageVec; // for init values
typedef Loop<StorageVec> StorageVecLoop;
typedef UtArray<CarbonNetValueCBData*> NetCBList;
typedef Loop<NetCBList> NetCBListLoop;
typedef CLoop<NetCBList> NetCBListCLoop;

class MiniMemPool
{
public: CARBONMEM_OVERRIDES
  MiniMemPool();
  ~MiniMemPool();
  void* alloc(UInt32 size);

private:
  UtArray<char*> mAllocedData;
  char* mData;
  char* mNextAlloc;
  UInt32 mFreeStorage;
};


template<class ObjType> class PodOp
{
public: CARBONMEM_OVERRIDES
  UInt32 numWords() const {return 1;}
  UInt32 numBytes() const {return sizeof(ObjType);}
  bool reportChangedBits() const { return false; }
  bool isEqual(const ObjType& a, ObjType* b) const {
    return a == *b;
  }
  void copy(ObjType* dst, const ObjType& src) const {
    *dst = src;
  }

};

/*!

  Similar to PodOp, but also saves a mask of which bits differed when
  compared.  To save the overhead of having the caller determine a
  pointer to the location where the mask should be saved, it assumes
  that the location is a fixed offset (passed to the constructor)
  after the second argument pointer.

  Since the second argument is a pointer into an array of shadow
  storage locations, it's easy for the caller to allocate twice as
  many locations, using the second half of the array as mask
  locations.
*/
template<class ObjType> class PodOpBitDiff
{
public:
  CARBONMEM_OVERRIDES

  PodOpBitDiff(UInt32 bitDiffOffset) : mBitDiffOffset(bitDiffOffset) {}
  ~PodOpBitDiff() {}

  UInt32 numWords() const {return 1;}
  UInt32 numBytes() const {return sizeof(ObjType);}
  bool reportChangedBits() const { return true; }
  bool isEqual(const ObjType& a, ObjType* b) const {
    if (a != *b) {
      // The location for the changed bits is a fixed offset from the
      // second argument.
      ObjType* changed = &b[mBitDiffOffset];
      *changed = a ^ *b;
      return false;
    }
    return true;
  }
  void copy(ObjType* dst, const ObjType& src) const {
    *dst = src;
  }

  // Ensure we have copy constructors and assignment operators.  The
  // default ones should work, but let's be safe.
  PodOpBitDiff(const PodOpBitDiff& other) : mBitDiffOffset(other.mBitDiffOffset) {}
  PodOpBitDiff& operator=(const PodOpBitDiff& other)
  {
    mBitDiffOffset = other.mBitDiffOffset;
    return *this;
  }

private:
  const UInt32 mBitDiffOffset;
};

//! Iterator over all changed bits at an index
/*!
  If needed, the specific CarbonValueChangeBase derivation should
  derive a custom version of this as well.
*/
class CarbonValueChangedBitsIter
{
public:
  CARBONMEM_OVERRIDES

  CarbonValueChangedBitsIter() {}
  virtual ~CarbonValueChangedBitsIter() {}

  //! Return the position of the next changed bit, or -1 if no more
  virtual SInt32 next() = 0;
};

class CarbonValueChangeBase
{
public: CARBONMEM_OVERRIDES
  CarbonValueChangeBase() {}
  virtual ~CarbonValueChangeBase() {}
  virtual void putModelPointer(SInt32 index, const void* modelStorage, StorageVec* initVals) = 0;
  virtual void executeCompares() = 0;
  virtual UInt32 processChanges() = 0;
  virtual void executeCBs(NetCBList* netCBs, CarbonModel* model, UInt32* val, UInt32* drv) = 0;
  virtual UInt32 numObjects() = 0;
  typedef UtArray<SInt32> IndexVec;
  typedef Loop<IndexVec> IndexVecLoop;
  typedef CLoop<IndexVec> IndexVecCLoop;

  const IndexVec& getChangedIndices() const {
    return mChangedIndices;
  }
  
  void clear() 
  {
    mChangedIndices.clear();
  }

  //! Create a bit iterator
  virtual CarbonValueChangedBitsIter* createBitIter(SInt32 index) = 0;

protected:
  IndexVec mChangedIndices;
};


// This ValueChange class is used for both pods,
// (UInt8, UInt16, UInt32, UInt64) and BitVectors of
// arbitrary width.  The differences between the 
// primitive operations on the objects are encapsulated
// in class ObjOp, which must provide methods:
//
//    bool isEqual(const ObjType&, ObjType*);
//    void copy(ObjType* dst, const ObjType& src);
//    UInt32 numWords();
//    bool reportChangedBits();
template<class ObjType, class ObjOp = PodOp<ObjType> >
class CarbonValueChange  : public CarbonValueChangeBase
{
public: CARBONMEM_OVERRIDES
  CarbonValueChange(SInt32 offset, SInt32 numObjects, ObjOp objOp, MiniMemPool* mem)
    : mObjOp(objOp),
      mOffset(offset),
      mNumObjects(numObjects),
      mProcessCallIndex(2)
  {
    UInt32 numShadowObjects = numObjects;
    // If the ObjOp supports the reporting of individual bits that
    // changed, allocate additional shadow storage to save the changed
    // bits.  The first half of the array will be used for the
    // shadows, and the second will be used for the changed bits.
    mHasChangedBits = objOp.reportChangedBits();
    if (mHasChangedBits) {
      numShadowObjects *= 2;
    }
    mShadowStorage = (ObjType*) mem->alloc(numShadowObjects * objOp.numWords() * sizeof(ObjType));
    mModelStorage = (const ObjType**) mem->alloc((numObjects + 1) * sizeof(ObjType*));
    mModelStorage[numObjects] = NULL; // for easy loop exit
  }

  ~CarbonValueChange()
  {
  }

  static CarbonValueChange* create(SInt32 offset, SInt32 numObjects, ObjOp objOp, MiniMemPool* mem)
  {
    CarbonValueChange* vc = (CarbonValueChange*) mem->alloc(sizeof(CarbonValueChange));
    return new (vc) CarbonValueChange(offset, numObjects, objOp, mem);
  }

  ObjType getValue(SInt32 absIndex) const
  {
    const ObjType* model = mModelStorage[absIndex];
    return *model;
  }

  inline SInt32 getOffset() const
  {
    return mOffset;
  }

  // The first thing we have to do is tell the CarbonValueChange
  // system where in the model the storage for each object
  // is.  This is virtual and uses void* for convenience
  // of WaveDump below.
  virtual void putModelPointer(SInt32 absIndex, const void* modelStorage, StorageVec* initVals)
  {
    SInt32 index = absIndex - mOffset;
    mModelStorage[index] = (const ObjType*) modelStorage;
    UInt32 shadowIndex = index * mObjOp.numWords();
    if (initVals == NULL)
      mObjOp.copy(&mShadowStorage[shadowIndex], *(ObjType*)modelStorage);
    else
    {
      CarbonShadowStruct* shadowObj = (*initVals)[absIndex];
      ShellNet::Storage shadowValue = shadowObj->getShadow();
      mObjOp.copy(&mShadowStorage[shadowIndex], *(ObjType*) shadowValue);
    }
  }
  
  inline SInt32 nextChangeIndex(SInt32 index)
  {
    ++index;
    const ObjType** model = &mModelStorage[index];
    UInt32 numWords = mObjOp.numWords();
    ObjType* shadow = &mShadowStorage[index * numWords];
    
    // Find the next element that does not match
    for (; (*model != NULL) && mObjOp.isEqual(**model, shadow);
         ++model, shadow += numWords)
      ;

    if (*model == NULL)
      return -1;
    
    mObjOp.copy(shadow, **model);
    //fprintf(stdout, "size=%u addr=%p\n", mObjOp.numBytes(), *model);
    return (model - mModelStorage);
  }

  virtual void executeCompares()
  {
    const ObjType** model = &mModelStorage[0];
    UInt32 numWords = mObjOp.numWords();
    ObjType* shadow = &mShadowStorage[0];
    
    // Find the next element that does not match
    for (; (*model != NULL);
         ++model, shadow += numWords)
    {
      if (! mObjOp.isEqual(**model, shadow))
        mObjOp.copy(shadow, **model);        
    }
  }

  virtual void executeCBs(NetCBList* netCBs, CarbonModel* carbonModel,  UInt32* val, UInt32* drv)
  {
    const ObjType** model = &mModelStorage[0];
    UInt32 numWords = mObjOp.numWords();
    ObjType* shadow = &mShadowStorage[0];
    
    for (; (*model != NULL);
         ++model, shadow += numWords)
    {
      CarbonNetValueCBData* cbData = (*netCBs)[(model - mModelStorage) + mOffset];
      // bug 6777 - Only update the shadow if the callback is enabled.
      if (cbData->isEnabled())
      {
        if (! mObjOp.isEqual(**model, shadow))
        {
          mObjOp.copy(shadow, **model);
          ShellNet* shlNet = cbData->getShellNet();
          shlNet->examine(val, drv, ShellNet::eIDrive, NULL);
          cbData->executeCB(carbonModel->getObjectID (), val, drv);
        }
      }
    }
  }

  virtual UInt32 numObjects()
  {
    return mNumObjects;
  }

  virtual UInt32 processChanges()
  {
    UInt32 numChanges = 0;
    SInt32 index = -1;
    // for every 1000 calls, check that mChangedIndices is a
    // reasonable size. There is a possibility that everything or
    // nearly everything changed. This will also check it on the
    // second call (we initialize mProcessCallIndex to 2).
    
    --mProcessCallIndex;
    if (mProcessCallIndex == 0)
    {
      // Let's only care if it is greater than 4 megs of space (1M
      // elements) and we are changing less than 10% most of the time.
      if ((mChangedIndices.capacity() > (1 << 20)) &&
          (mNumLargeChanges < 100))
        mChangedIndices.clear();
      
      mNumLargeChanges = 0;
      mProcessCallIndex = 1000;
    }
    
    mChangedIndices.resize(0, true);
    while ((index = nextChangeIndex(index)) != -1)
    {
      mChangedIndices.push_back(index + mOffset);
      ++numChanges;
    }
    
    if (numChanges > 0x00004000)
      ++mNumLargeChanges;
    
    return numChanges;
  }

  //! Iterator over an ObjType& that is a mask of bits that changed
  class ChangedBitsIter : public CarbonValueChangedBitsIter
  {
  public:
    CARBONMEM_OVERRIDES

    ChangedBitsIter(const ObjType& val)
      : mVal(val), mCurrBit(-1)
    {}
    virtual ~ChangedBitsIter() {}

    virtual SInt32 next()
    {
      // Look for the first bit set after the last one we saw
      bool found = false;
      while (!found && (++mCurrBit < scNumBits)) {
        if ((mVal & (static_cast<ObjType>(1) << mCurrBit)) != 0) {
          found = true;
        }
      }
      if (!found) {
        // Reset current bit if nothing found
        mCurrBit = -1;
      }
      return mCurrBit;
    }

  protected:
    static const SInt32 scNumBits = sizeof(ObjType) * 8;
    const ObjType& mVal;
    SInt32 mCurrBit;
  };
  
  CarbonValueChangedBitsIter* createBitIter(SInt32 index)
  {
    // This is only valid if we've allocated extra space in the shadow
    // storage buffer to store the changed bits.
    INFO_ASSERT(mHasChangedBits, "Changed bits buffer not allocated");
    // The changed bits for any index are in a location that's a fixed
    // offset from the corresponding shadow storage.
    const ObjType& changedBits = mShadowStorage[index + mNumObjects];
    return new ChangedBitsIter(changedBits);
  }

private:
  ObjOp mObjOp;            // help uniformly treat bitvectors & pods
  ObjType* mShadowStorage;
  const ObjType** mModelStorage;
  SInt32 mOffset;
  const SInt32 mNumObjects;
  SInt32 mProcessCallIndex;
  SInt32 mNumLargeChanges;
  bool mHasChangedBits;
};

class CarbonExamineScheduler
{
public: CARBONMEM_OVERRIDES
  CarbonExamineScheduler(CarbonHookup* hookup);
  ~CarbonExamineScheduler();
  
  //! NetInfo structure
  class NetInfo
  {
    public: CARBONMEM_OVERRIDES

    NetInfo() : mIsNonPod(false), mIsConstant(false), mHasInputSemantics(false), mPodBitsel(-1)
    {}

    inline bool isNonPOD() const { return mIsNonPod; }
    inline bool isConstant() const { return mIsConstant; }
    inline bool hasInputSemantics() const { return mHasInputSemantics; }
    inline SInt32 getPodBitsel() const { return mPodBitsel; }
    
    bool mIsNonPod;
    bool mIsConstant;
    bool mHasInputSemantics;
    /*!
      If this net represents a bitselect of a POD, this is the
      normalized bit index into it.  Otherwise, this is -1.
    */
    SInt32 mPodBitsel;
  };
  
  //! Closure for creating CarbonValueChange blocks by sorted size
  class NetTraitClosure
  {
  public: CARBONMEM_OVERRIDES

    typedef UtArray<const UInt8*> ByteVec;
    typedef Loop<ByteVec> ByteVecLoop;
    typedef UtArray<const UInt16*> ShortVec;
    typedef Loop<ShortVec> ShortVecLoop;
    typedef UtArray<const UInt32*> LongVec;
    typedef Loop<LongVec> LongVecLoop;
    typedef UtArray<const UInt64*> LLongVec;
    typedef Loop<LLongVec> LLongVecLoop;
    typedef UtHashMap<UInt32,LongVec> SizeHugeVecMap;
    
    ByteVec mBytes;
    ShortVec mShorts;
    LongVec mLongs;
    LLongVec mLLongs;
    SizeHugeVecMap mHugeVecs;

    // Similar to the above, each entry in these lists of storage
    // locations corresponds to a CarbonWaveNetAssocGroup, instead of
    // a single CarbonWaveNetAssoc.
    ByteVec mByteGroups;
    ShortVec mShortGroups;
    LongVec mLongGroups;
    LLongVec mLLongGroups;
    SizeHugeVecMap mHugeVecGroups;
    
    CarbonValueChange<UInt8>* allocBytesOnly(MiniMemPool* memPool, StorageVec* initVals);

    void allocVCs(VCVec* vec, VCVec* groupVec, MiniMemPool* memPool);

    void allocInitVCs(VCVec* vec, VCVec* groupVec, StorageVec* initVals, MiniMemPool* memPool);

    void addNet(const ShellNet* net);
    void addGroup(const void* storage, UInt32 width);
  };

  //! If you care about net information and want to try to schedule
  ScheduleStimuli* getScheduleStimuli(ShellNet* theNet, NetInfo* netInfo);
  
  //! If you only care about the net information
  void getNetInfo(ShellNet* net, NetInfo* netInfo);
  
  
  //! Basic sorting operation needed for CarbonValueChange
  static bool sSortByNoTriNumBytes(const ShellNet* a1, const ShellNet* a2);

private:
  CarbonHookup* mHookup;

  typedef UtHashMap<SCHSignature*, ScheduleStimuli*> SigStimMap;
  SigStimMap mSigToStim;

  VCVec mPodCheckVCs;
};

class ScheduleStimuli
{
  enum ClockStim {eNegEdge, ePosEdge, eBothEdges};

  class EdgeDetectInfo
  {
  public: CARBONMEM_OVERRIDES

    EdgeDetectInfo(CarbonChangeType* entry, ClockStim stim) :
      mChgArrEntry(entry), mEdgeStim(stim)
    {}
    
    CarbonChangeType* mChgArrEntry;
    ClockStim mEdgeStim;
  };

  class EdgeInfoNetPair
  {
  public: CARBONMEM_OVERRIDES

    EdgeInfoNetPair(EdgeDetectInfo* info, ShellNet* net)
      : mInfo(info), mNet(net)
    {}
    
    ~EdgeInfoNetPair() {}
    
    EdgeDetectInfo* mInfo;
    ShellNet* mNet;
  };
  
  
public: CARBONMEM_OVERRIDES
  ScheduleStimuli(SCHSignature* signature);

  ~ScheduleStimuli(); 
  
  typedef UtArray<const SCHEvent*> EventList;
  typedef Loop<EventList> EventListLoop;

  bool addEventNodes(EventList& vec, CarbonHookup* hookup);

  void setInvalid() 
  {
    mInvalid = true;
  }

  bool isInvalid() const
  {
    return mInvalid;
  }
  

  inline bool isStimulated() const { return mStimulated; }

  void cleanup(CarbonHookup* hookup);
  void transferMaps(CarbonHookup* hookup);

  void update();

  bool operator<(const ScheduleStimuli& other) const;

  inline bool doTransfer() const { return mTransferMaps; }

  //! Did a clock in this stimulus change?
  inline bool hasChange() {
    /*
      Initialize mStimulated to false, so we don't have to call
      update. If we return true, this can still be false, because
      update will be called.
    */
    mStimulated = false;
    for (CarbonChangeType *changeEntry, **chgArr = mChangeArray; 
         ((changeEntry = *chgArr) != NULL); ++chgArr)
    {
      if (*changeEntry != CARBON_CHANGE_NONE_MASK) return true;
    }
    return false;
  }

private:
  SCHSignature* mSignature;
  typedef UtArray<EdgeInfoNetPair*> EdgeInfoNetPairVec;
  typedef Loop<EdgeInfoNetPairVec> EdgeNetLoop;

  EdgeInfoNetPairVec mNonPods;
  CarbonChangeType** mChangeArray;

  typedef UtHashMap<STAliasedLeafNode*, EdgeDetectInfo*> NodeEdgeMap;
  NodeEdgeMap mNodeToEdge;
  
  EdgeInfoNetPairVec mEdgeAndNet;
  VCVec mVCObjs;

  bool mTransferMaps;
  bool mInvalid;
  bool mStimulated;
  void appendInitVal(EdgeInfoNetPair* infoNetPair, CarbonHookup* hookup);
  void destroyInitVals(CarbonHookup* hookup);
  void addNonPodClock(EdgeInfoNetPair* infoNet);
};

#endif
