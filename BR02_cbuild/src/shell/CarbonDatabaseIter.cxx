// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "CarbonDatabaseIter.h"
#include "shell/CarbonDBRead.h"
#include "util/UtWildcard.h"
#include "util/StringAtom.h"

// Returns the passed in node if the node qualifies as a non-internal
// node. NULL, otherwise.
static inline const STSymbolTableNode* sQualifyNode(const STSymbolTableNode* node)
{
  const STSymbolTableNode* ret = NULL;
  if (node)
  {
    const char* name = node->str();
    ret = *name != '$' ? node : NULL;
  }
  return ret;
}

// Returns the passed in node if the node qualifies as a non-internal
// node. NULL, otherwise.
static const STSymbolTableNode* sQualifyNodeExpr(const STSymbolTableNode* node)
{
  const STSymbolTableNode* ret = NULL;
  ret = sQualifyNode(node);
  if (ret == NULL)
  {
    // This is an internal node. Check if it has a backpointer. If so,
    // it is part of an expression. That's ok.
    if (ShellSymTabBOM::getBackPointer(node) != NULL)
      ret = node;
  }
  return ret;
}

template <typename T> const STSymbolTableNode* advanceSymNodeLooper(T& loop)
{
  const STSymbolTableNode* ret = NULL;
  if (! loop.atEnd())
  {
    do {
      ret = *loop;
      ret = sQualifyNodeExpr(ret);
      ++loop;
    } while (! loop.atEnd() && (ret == NULL));
  }
  return ret;
}

CarbonDatabaseSymtabIter::~CarbonDatabaseSymtabIter() 
{}


CarbonDatabaseSymtabSetIter::CarbonDatabaseSymtabSetIter(IODB::NameSetLoop loop) :
  mLoop(loop)
{}

const STSymbolTableNode* CarbonDatabaseSymtabSetIter::next()
{
  return advanceSymNodeLooper(mLoop);  
}


CarbonDatabaseSymtabMapIter::CarbonDatabaseSymtabMapIter(IODB::NameSetMapLoop loop) :
  mLoop(loop)
{}

const STSymbolTableNode* CarbonDatabaseSymtabMapIter::next()
{
  const STSymbolTableNode* ret = NULL;
  if (! mLoop.atEnd()) {
    do {
      ret = mLoop.getKey();
      ret = sQualifyNodeExpr(ret);
      ++mLoop;
    } while (!mLoop.atEnd() && (ret == NULL));
  }
  return ret;
}


CarbonDatabaseSymtabVecIter::CarbonDatabaseSymtabVecIter(IODB::NameVecLoop loop) :
  mLoop(loop)
{}

const STSymbolTableNode* CarbonDatabaseSymtabVecIter::next()
{
  return advanceSymNodeLooper(mLoop);  
}

CarbonDatabaseSymtabBranchIter::CarbonDatabaseSymtabBranchIter(const STBranchNode* parent) :
  mParent(parent),
  mIndex(0)
{
  // Cbuild doesn't guarantee that nodes will be added into the
  // symboltable in a consistent order. Node indices into branches are
  // not stable. So, we need to copy the branch children (1 level
  // only, no recurse) and sort them.
  doSort();
}

static bool sHierAlphaCompare(const HierName* n1, const HierName* n2)
{
  return *n1 < *n2;
}

void CarbonDatabaseSymtabBranchIter::doSort()
{
  if (mParent)
  {
    SInt32 numChildren = mParent->numChildren();
    for (SInt32 i = 0; i < numChildren; ++i)
    {
      const STSymbolTableNode* node = mParent->getChild(i);
      mChildren.push_back(node);
    }
    std::sort(mChildren.begin(), mChildren.end(), sHierAlphaCompare);
  }
}

const STSymbolTableNode* CarbonDatabaseSymtabBranchIter::next()
{
  const STSymbolTableNode* ret = NULL;
  if (mParent != NULL)
  {    
    SInt32 numChildren = mChildren.size();
    if (mIndex < numChildren) {
      do {
        ret = mChildren[mIndex];
        // Don't look at parts of expressions, so use the sQualifyNode
        // function, not the sQualifyNodeExpr.
        ret = sQualifyNode(ret);
        ++mIndex;
      } while ((mIndex < numChildren) && (ret == NULL));
    }
  }
  return ret;
}

const STSymbolTableNode* CarbonDatabaseSymtabRootIter::next() {
  return advanceSymNodeLooper(mRootIter);
}

const STSymbolTableNode* CarbonDatabaseAliasIter::next() {
  return advanceSymNodeLooper(mAliasLoop);
}

CarbonDatabaseSymtabSetSingleEdgeIter::CarbonDatabaseSymtabSetSingleEdgeIter(IODB::NameSetLoop loop, const IODB::NameSet& bothEdgeTriggers)
  : CarbonDatabaseSymtabSetIter(loop), mBothEdgeTriggers(bothEdgeTriggers)
{}

const STSymbolTableNode* CarbonDatabaseSymtabSetSingleEdgeIter::next()
{
  const STSymbolTableNode* ret = NULL;
  do {
    ret = CarbonDatabaseSymtabSetIter::next();
  } while (ret && (mBothEdgeTriggers.count(const_cast<STSymbolTableNode*>(ret)) != 0));
  return ret;
}

CarbonDatabaseSymtabMatchIter::CarbonDatabaseSymtabMatchIter(const STSymbolTable* symtab, const char* matchString)
{
  // We want escaped identifiers to be treated as such - i.e. wildcard
  // characters in them are not actually wildcards.  We also don't
  // want wildcards to match '.', because we want to emulate the
  // behavior of cbuild directives.  "top.*" should match "top.x" but
  // not "top.sub.x".
  UtWildcard wc(matchString, true, false);

  // Loop over the entire sorted symbol table, looking for matches
  for (STSymbolTable::NodeLoopSorted l = symtab->getNodeLoopSorted(); !l.atEnd(); ++l) {
    const STSymbolTableNode* node = *l;
    // Filter internal nodes
    node = sQualifyNodeExpr(node);
    if (node != NULL) {
      UtString nodeName;
      node->verilogCompose(&nodeName);
      // If the name matches the wildcard, save it.
      if (wc.isMatch(nodeName.c_str())) {
        mMatches.push_back(node);
      } else {
        // cbuild directives can be both instance-based and
        // module-based, so we want to duplicate that here.  If the
        // node is a leaf and its parent is a module (as opposed to
        // some other named scope), build the module-based name and
        // try to match that.
        const STAliasedLeafNode* leaf = node->castLeaf();
        const STSymbolTableNode* parent = node->getParent();
        const StringAtom* modName = NULL;
        if (parent != NULL) {
          // Module name is stored in the BOM.  The parent must be a
          // branch so the BOM should be valid.
          const ShellBranchDataBOM* branchBOM = ShellSymTabBOM::getBranchBOM(parent);
          ST_ASSERT(branchBOM != NULL, node);
          modName = branchBOM->getModuleName();
        }
        if ((leaf != NULL) && (modName != NULL)) {
          // Build <module_name>.<signal_name>
          UtString modBasedName, leafName;
          modBasedName << modName->str();
          leaf->verilogComposeLeaf(&leafName);
          modBasedName << "." << leafName;
          if (wc.isMatch(modBasedName.c_str())) {
            mMatches.push_back(node);
          }
        }
      }
    }
  }
  
  mIter = mMatches.begin();
}

const STSymbolTableNode* CarbonDatabaseSymtabMatchIter::next()
{
  const STSymbolTableNode* ret = NULL;
  if (mIter != mMatches.end()) {    
    ret = *mIter;
    ++mIter;
  }
  return ret;
}
