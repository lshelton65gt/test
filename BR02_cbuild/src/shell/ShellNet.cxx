// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/CarbonPlatform.h"
#include "shell/CarbonScalarInput.h"
#include "shell/CarbonTristateScalarInput.h"

#include "shell/CarbonModel.h"
#include "shell/CarbonDBRead.h"
#include "util/UtConv.h"
#include "util/UtIOStream.h"
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STSymbolTable.h"
#include "shell/ShellNetTristate.h"

static inline UInt8 sGetScalarValue(const UInt32* buf)
{ 
  return buf[0] & 0x1;
}

static bool sValueFormatScalarString(char* valueStr, size_t len, 
                                     UInt32 scalar)
{
  // scalars are always binary
  if (len < 2)
    return false;
  
  valueStr[0] = (sGetScalarValue(&scalar) != 0) ? '1' : '0';
  valueStr[1] = '\0';
  return true;
}

class TriValShadowScalar 
{
public:
  CARBONMEM_OVERRIDES

  TriValShadowScalar() {
    mValue = 0;
    mDrive = 0;
  }
  ~TriValShadowScalar() {
  }
  
  UInt8 mValue;
  UInt8 mDrive;
};

ShellNet::ShellNet() : mName(0), 
                       mRefCount(1) 
{}

ShellNet::~ShellNet() {}

void ShellNet::recomputeChangeMask()
{}

const IODBIntrinsic* ShellNet::getIntrinsic() const
{
  const ShellDataBOM* bomdata = getBOMData();
  const IODBIntrinsic* intrinsic = bomdata->getIntrinsic();
  return intrinsic;
}

int ShellNet::getBitWidth() const { 
  const IODBIntrinsic* intrinsic = getIntrinsic();
  return intrinsic->getWidth();
}

int ShellNet::getNumUInt32s() const { 
  UInt32 width = ShellNet::getBitWidth();
  return (width + 31)/32;
}


const ShellNet* ShellNet::castShellNet() const
{
  return this;
}

int ShellNet::getLSB() const
{ 
  const IODBIntrinsic* intrinsic = getIntrinsic();
  return intrinsic->getLsb();
}

int ShellNet::getMSB () const 
{ 
  const IODBIntrinsic* intrinsic = getIntrinsic();
  return intrinsic->getMsb();
}

const CarbonDatabaseNode* ShellNet::getDBNode () const 
{ 
  return NULL;
}

ShellNet::ValueState 
ShellNet::compareAndUpdate(Storage* shadow)
{
  ValueState ret = compare(*shadow);
  if (ret == eChanged)
    update(shadow);
  return ret;
}

ShellNet::ValueState 
ShellNet::compareUpdateExamine(Storage* shadow, UInt32* val, UInt32* drv)
{
  ValueState ret = compare(*shadow);
  if (ret == eChanged)
  {
    update(shadow);
    examine(val, drv, eIDrive, NULL);
  }
  return ret;
}

void ShellNet::setName(HierName* name) {
  mName = name;
}
  
void ShellNet::incrCount() {
  ++mRefCount;
}
  
bool ShellNet::decrCount() {
    
  if (mRefCount != 0)
    --mRefCount;
    
  return (mRefCount == 0);
}

bool ShellNet::isPulled(NetFlags flags)
{
  return NetIsPullUp(flags) || NetIsPullDown(flags);
}

void ShellNet::updateUnresolved(Storage* shadow) const
{
  // This function's base implementation is to use update. That is
  // because for all types but tri-states, we don't do any resolution.
  update(shadow);
}

ShellNet::ValueState
ShellNet::compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                         UInt32* drive)
{
  // This function's base implementation is to use update. That is
  // because for all types but tri-states, we don't do any resolution.
  return compareUpdateExamine(shadow, value, drive);
}

CarbonStatus ShellNet::sDoFormat(char* valueStr, size_t len, 
                                 CarbonRadix strFormat, 
                                 const UInt32* val, 
                                 const UInt32* idrive,
                                 const UInt32* overrideMask,
                                 int bitWidth, CarbonModel* model)
{
  int numCharsConverted = -1;
  CarbonStatus stat = eCarbon_OK;
  switch(strFormat)
  {
  case eCarbonBin:
    numCharsConverted = CarbonValRW::writeBinXZValToStr(valueStr, len, val, NULL, idrive, NULL, overrideMask, false, bitWidth);
    break;
  case eCarbonHex:
    numCharsConverted = CarbonValRW::writeHexXZValToStr(valueStr, len, val, NULL, idrive, NULL, overrideMask, false, bitWidth);
    break;
  case eCarbonOct:
    numCharsConverted = CarbonValRW::writeOctXZValToStr(valueStr, len, val, NULL, idrive, NULL, overrideMask, false, bitWidth);
    break;
  case eCarbonDec:
    numCharsConverted = CarbonValRW::writeDecXZValToStr(valueStr, len, val, NULL, idrive, NULL, overrideMask, false, true, bitWidth);
    break;
  case eCarbonUDec:
    numCharsConverted = CarbonValRW::writeDecXZValToStr(valueStr, len, val, NULL, idrive, NULL, overrideMask, false, false, bitWidth);
    break;
  }
  
  if (numCharsConverted == -1)
  {
    ShellGlobal::reportInsufficientBufferLength(len, model);
    stat = eCarbon_ERROR;
  }
  return stat;
}

ShellNet::Traits::Traits() : mConstantNet(NULL), 
                             mHasComplexStorage(false),
                             mWidth(0), 
                             mHasInputSemantics(false), mIsTristate(false),
                             mIsReal(false), mPodBitsel(-1)
{
  mStorage.mTriArray = NULL;
}

ShellNet::Traits::~Traits()
{}

CarbonScalarBase::CarbonScalarBase()
{}

CarbonScalarBase::~CarbonScalarBase() 
{}

void CarbonScalarBase::putToZero(CarbonModel* model)
{
  UInt32 val = 0;
  fastDeposit(&val, &val, model);
}

void CarbonScalarBase::putToOnes(CarbonModel* model)
{
  UInt32 val = 1;
  UInt32 drv = 0;
  fastDeposit(&val, &drv, model);
}

CarbonStatus CarbonScalarBase::setRange(int, int, CarbonModel* model)
{
  putToOnes(model);
  return eCarbon_OK;
}

CarbonStatus CarbonScalarBase::clearRange(int, int, CarbonModel* model)
{
  putToZero(model);
  return eCarbon_OK;
}

bool CarbonScalarBase::isScalar() const { return true; }

CarbonStatus CarbonScalarBase::examineWord(UInt32 *buf, int, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  return examine(buf, drive, mode, model);
}

CarbonStatus CarbonScalarBase::examineRange(UInt32 *buf, int, int, UInt32* drive, CarbonModel* model) const
{
  return examine(buf, drive, eIDrive, model);
}
  
CarbonStatus CarbonScalarBase::depositWord(UInt32 buf, int, UInt32 driveVal, CarbonModel* model)
{
  return deposit(&buf, &driveVal, model);
}

CarbonStatus CarbonScalarBase::depositRange(const UInt32 *buf, int, int, const UInt32* driveVal, CarbonModel* model)
{
  return deposit(buf, driveVal, model);
}

void CarbonScalarBase::fastDepositWord(UInt32 buf, int, UInt32 driveVal, CarbonModel* model)
{
  fastDeposit(&buf, &driveVal, model);
}

void CarbonScalarBase::fastDepositRange(const UInt32 *buf, int, int, const UInt32* driveVal, CarbonModel* model)
{
  fastDeposit(buf, driveVal, model);
}

void CarbonScalarBase::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
    *xdrive = 1;
}

bool CarbonScalarBase::isVector() const
{
  return false;
}

bool CarbonScalarBase::isReal() const
{
  return false;
}

bool CarbonScalarBase::isTristate() const
{
  return false;
}

bool CarbonScalarBase::setToDriven(CarbonModel*)
{
  return false;
}

bool CarbonScalarBase::setToUndriven(CarbonModel*)
{
  return false;
}

bool CarbonScalarBase::setWordToUndriven(int, CarbonModel*)
{
  return false;
}

bool CarbonScalarBase::resolveXdrive(CarbonModel*)
{
  return false;
}

bool CarbonScalarBase::setRangeToUndriven(int, int, CarbonModel*)
{
  return false;
}

CarbonStatus CarbonScalarBase::examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const
{
  return eCarbon_ERROR;
}

CarbonStatus CarbonScalarBase::force(const UInt32*, CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus CarbonScalarBase::forceWord(UInt32, int, CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus CarbonScalarBase::forceRange(const UInt32*, int, int, CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus CarbonScalarBase::release(CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus CarbonScalarBase::releaseWord(int, CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus CarbonScalarBase::releaseRange(int, int, CarbonModel*)
{
  return eCarbon_ERROR;
}

bool CarbonScalarBase::isForcible() const
{
  return false;
}

bool CarbonScalarBase::isInput() const
{
  return false;
}

const CarbonMemory* CarbonScalarBase::castMemory() const
{
  return NULL;
}

const CarbonModelMemory* CarbonScalarBase::castModelMemory() const
{
  return NULL;
}

const CarbonVectorBase* CarbonScalarBase::castVector() const
{
  return NULL;
}

const CarbonScalarBase* CarbonScalarBase::castScalar() const
{
  return this;
}

const ShellNetConstant* CarbonScalarBase::castConstant() const
{
  return NULL;
}

int CarbonScalarBase::hasDriveConflict() const
{
  return 0;
}

int CarbonScalarBase::hasDriveConflictRange(SInt32, SInt32) const
{
  return 0;
}

void CarbonScalarBase::setRawToUndriven(CarbonModel*)
{
}

void CarbonScalarBase::putChangeArrayRef(CarbonChangeType*)
{
}

CarbonChangeType* CarbonScalarBase::getChangeArrayRef()
{
  return NULL;
}

const UInt32* CarbonScalarBase::getControlMask() const
{
  return NULL;
}

void CarbonScalarBase::bypassDeposit(const UInt32*, const UInt32*)
{
  ST_ASSERT(0, getName());
}

// CarbonScalar
CarbonScalar::CarbonScalar(UInt8* addr): 
  CarbonScalarBase(),
  mScalar(addr)
{}

CarbonScalar::CarbonScalar(SInt8* addr): 
  CarbonScalarBase(),
  mScalar((UInt8*) addr)
{}

CarbonScalar::~CarbonScalar() 
{}

void CarbonScalar::examineModelDrive(UInt32* driveBuf, ExamineMode) const
{
  driveBuf[0] = 0;
}

void CarbonScalar::getTraits(Traits* traits) const
{
  traits->mWidth = 1;
  traits->putByte(mScalar);
}

bool CarbonScalar::isDataNonZero() const
{
  UInt32 val = *mScalar;
  return (val != 0);
}

CarbonStatus CarbonScalar::examineValXDriveWord(UInt32* val, UInt32* drv, int) const
{
  *val = *mScalar;
  *drv = 1;
  return eCarbon_OK;
}

ShellNet::ValueState 
CarbonScalar::internalCompare(const Storage shadow, UInt32 scalar) const 
{
  ValueState state = eUnchanged;
  UInt8 shadowVal = *(static_cast<UInt8*>(shadow));
  if (shadowVal != scalar)
    state = eChanged;
  return state;
}

ShellNet::ValueState 
CarbonScalar::compare(const Storage shadow) const 
{
  UInt32 scalar = *mScalar;
  return internalCompare(shadow, scalar);
}

ShellNet::Storage 
CarbonScalar::allocShadow() const
{
  UInt8* typedShadow = CARBON_ALLOC_VEC(UInt8, 1);
  Storage castShadow = static_cast<Storage>(typedShadow);
  update(&castShadow);
  return castShadow;
}

void CarbonScalar::freeShadow(Storage* shadow) {
  UInt8* typedShadow = static_cast<UInt8*>(*shadow);
  CARBON_FREE_VEC(typedShadow, UInt8, 1);
  *shadow = NULL;
}

ShellNet::ValueState 
CarbonScalar::writeIfNotEq(char* valueStr, size_t len, 
                               Storage* shadow, NetFlags) 
{
  UInt32 scalar = *mScalar;
  ValueState state = internalCompare(*shadow, scalar);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    UInt8* tmpBuf = static_cast<UInt8*>(*shadow);
    CarbonValRW::cpSrcToDest(tmpBuf, &scalar, 1);
    (void)sValueFormatScalarString(valueStr, len, scalar);
  }
  return state;
}

ShellNet::ValueState CarbonScalar::writeIfNotEqForce(char* valueStr, size_t len, 
                                                     ShellNet::Storage* shadow,
                                                     NetFlags flags, ShellNet*)
{
  // don't have to worry about control mask for scalars
  return writeIfNotEq(valueStr, len, shadow, flags);
}

CarbonStatus CarbonScalar::format(char* valueStr, size_t len, 
                                  CarbonRadix, NetFlags, CarbonModel* model) const 
{
  UInt32 scalar = *mScalar;
  CarbonStatus stat = eCarbon_OK;
  if (! sValueFormatScalarString(valueStr, len, scalar))
  {
    ShellGlobal::reportInsufficientBufferLength(len, model);
    stat = eCarbon_ERROR;
  }
  return stat;
}

CarbonStatus CarbonScalar::formatForce(char* valueStr, size_t len, 
                                       CarbonRadix strFormat, NetFlags flags, 
                                       ShellNet*, CarbonModel* model) const
{
  // don't have to worry about control mask for scalars
  return format(valueStr, len, strFormat, flags, model);
}

void CarbonScalar::update(Storage* shadow) const 
{
  UInt32 scalar = *mScalar;
  CarbonValRW::cpSrcToDest(static_cast<UInt8*>(*shadow), &scalar, 1);
}

void CarbonScalar::doFastDeposit(const UInt32* buf, CarbonModel* model)
{
  bool changed = assignValue(buf);
  doUpdateVHM(changed, model);
}

void CarbonScalar::doDeposit(const UInt32 *buf, CarbonModel* model)
{
  if (buf)
    doFastDeposit(buf, model);
}

CarbonStatus CarbonScalar::deposit(const UInt32 *buf, const UInt32*, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doDeposit(buf, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonScalar::depositWord (UInt32 buf, int, UInt32, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doDeposit(&buf, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonScalar::depositRange (const UInt32* buf, int, int, const UInt32*, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doDeposit(buf, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

void CarbonScalar::fastDeposit(const UInt32 *buf, const UInt32*, CarbonModel* model)
{
  doFastDeposit(buf, model);
}

void CarbonScalar::fastDepositWord (UInt32 buf, int, UInt32, CarbonModel* model)
{
  doFastDeposit(&buf, model);
}

void CarbonScalar::fastDepositRange (const UInt32* buf, int, int, const UInt32*, CarbonModel* model)
{
  doFastDeposit(buf, model);
}

CarbonStatus CarbonScalar::examine(UInt32 *buf, UInt32* drive, ExamineMode, CarbonModel*) const
{
  if (buf)
    buf[0]  = *mScalar;

  if (drive)
    *drive = 0;
  return eCarbon_OK;
}

bool CarbonScalar::assignValue(const UInt32* buf)
{
  bool changed = false;
  UInt8 newVal = sGetScalarValue(buf);
  changed = (newVal != *mScalar);
  *mScalar = newVal;
  return changed;
}


UInt32 CarbonScalar::getExamineStore() const
{ 
  return *mScalar;
}

void CarbonScalar::bypassDeposit(const UInt32 *buf, const UInt32*)
{
  assignValue(buf);
}

CarbonTristateScalar::CarbonTristateScalar(UInt8* data, UInt8* drive)
  : CarbonScalarBase()

{
  mTri  = new ShellNetTristate1(data, drive);
}

CarbonTristateScalar::CarbonTristateScalar(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive)
  : CarbonScalarBase()

{
  mTri  = new ShellNetBidirect1(idata, idrive, xdata, xdrive);
}

CarbonTristateScalar::~CarbonTristateScalar()
{
  delete mTri;
}

void CarbonTristateScalar::getTraits(Traits* traits) const
{
  traits->mIsTristate = true;
  traits->mWidth = 1;
  traits->putTriByte(mTri);
}

ShellNet::Storage 
CarbonTristateScalar::allocShadow() const 
{
  Storage shadow = static_cast<Storage>(new TriValShadowScalar);
  update(&shadow);
  return shadow;
}

void CarbonTristateScalar::freeShadow(Storage* shadow) 
{  
  TriValShadowScalar* typedShadow = static_cast<TriValShadowScalar*>(*shadow);
  delete typedShadow;
  *shadow = NULL;
}

void CarbonTristateScalar::update(Storage* shadow) const 
{
  TriValShadowScalar* dest = static_cast<TriValShadowScalar*>(*shadow);
  UInt32 val;
  UInt32 drv;
  calcValue(&val, &drv);
  drv &= 0x1;
  CarbonValRW::cpSrcToDest(&(dest->mValue), &val, 1);
  CarbonValRW::cpSrcToDest(&(dest->mDrive), &drv, 1);
}

void CarbonTristateScalar::updateUnresolved(Storage* shadow) const 
{
  TriValShadowScalar* dest = static_cast<TriValShadowScalar*>(*shadow);
  dest->mValue = mTri->getIData();
  dest->mDrive = mTri->getIDrive();
}

ShellNet::ValueState 
CarbonTristateScalar::compare(const Storage shadow) const 
{
  TriValShadowScalar* triShadow = static_cast<TriValShadowScalar*>(shadow);
  return compareValue(&(triShadow->mValue), &(triShadow->mDrive));
}

ShellNet::ValueState 
CarbonTristateScalar::compareUpdateExamineUnresolved(Storage* shadow,
                                                     UInt32* value, UInt32* drive)
{
  // Examine eIDrive
  examineValue(value, drive);

  // Compare and update
  TriValShadowScalar* triShadow = static_cast<TriValShadowScalar*>(*shadow);
  if ((triShadow->mValue != mTri->getIData()) || 
      (triShadow->mDrive != mTri->getIDrive())) {
    // Update the shadow
    triShadow->mValue = mTri->getIData();
    triShadow->mDrive = mTri->getIDrive();
    return eChanged;

  } else {
    return eUnchanged;
  }
}

ShellNet::ValueState 
CarbonTristateScalar::compareValue(UInt8* val, UInt8* drv) const 
{
  ValueState stat = eUnchanged;
  if (mTri->compareCalcValue(val, drv, 1) != 0)
    stat = eChanged;
  return stat;
}


ShellNet::ValueState 
CarbonTristateScalar::writeIfNotEq(char* valueStr, size_t len, 
                                   Storage* shadow, NetFlags flags) 
{
  ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    update(shadow);
    format(valueStr, len, eCarbonBin, flags, NULL);
  }
  return state;
}

ShellNet::ValueState CarbonTristateScalar::writeIfNotEqForce(char* valueStr, size_t len, 
                                                             ShellNet::Storage* shadow,
                                                             NetFlags flags, ShellNet*)
{
  // don't have to worry about control mask for scalars
  return writeIfNotEq(valueStr, len, shadow, flags);
}

bool CarbonTristateScalar::isDataNonZero() const
{
  UInt32 val;
  examine(&val, NULL, eIDrive, NULL);
  return (val != 0);
}

CarbonStatus CarbonTristateScalar::format(char* valueStr, size_t len, 
                                           CarbonRadix,
                                           NetFlags flags, CarbonModel* model) const
{
  UInt32 valBuf;
  UInt32 driveBuf;
  calcValue(&valBuf, &driveBuf);
  driveBuf &= 0x1;
  CarbonStatus stat = eCarbon_OK;

  if (! sValueFormatScalarString(valueStr, len, valBuf))
  {
    ShellGlobal::reportInsufficientBufferLength(len, model);
    stat = eCarbon_ERROR;
  }
  
  if (stat == eCarbon_OK) 
  {
    /*
      If both are driving, have to use the value since they both
      could be driving the same value. However, there is no way to
      detect if there is a value conflict. So, in that case, the
      value is technically an x, so whatever the value is use that.
    */

    bool pulled = isPulled(flags);
    if ((driveBuf != 0) && ! pulled)
      valueStr[0] = 'z';
  }
  return stat;
}

CarbonStatus CarbonTristateScalar::formatForce(char* valueStr, size_t len, 
                                               CarbonRadix strFormat, 
                                               NetFlags flags, 
                                               ShellNet*,
                                               CarbonModel* model) const
{
  return format(valueStr, len,  strFormat, flags, model);
}

void CarbonTristateScalar::doDeposit(const UInt32* buf, const UInt32* drive,
                                     CarbonModel* model)
{
  // must set the drive first, so we can use it
  bool changed = false;
  UInt32 fixedDrive = 0;
  const UInt32* drivePtr = drive;
  
  if (! drive)
    changed = setToDriven(NULL);
  else if (sGetScalarValue(drive) != 0)
  {
    // making sure there are no extraneous 1's
    fixedDrive = 1;
    drivePtr = &fixedDrive;
  }
  
  if (assignValue(buf, drivePtr))
    changed = true;
  doUpdateVHM(changed, model);
}

CarbonStatus CarbonTristateScalar::deposit(const UInt32* buf,
                                           const UInt32* drive, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doDeposit(buf, drive, model);
    return eCarbon_OK;  
  }
  return eCarbon_ERROR;
}

void CarbonTristateScalar::fastDeposit(const UInt32* buf,
                                       const UInt32* drive, CarbonModel* model)
{
  doDeposit(buf, drive, model);
}

CarbonStatus CarbonTristateScalar::examine(UInt32 *buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  switch (mode)
  {
  case eIDrive:
    examineValue(buf, drive);
    break;
  case eCalcDrive:
    calcValue(buf, drive);
    break;
  case eXDrive:
    examineXVals(buf, drive);
    break;
  }

  // fix the drive value, which is set to all ones if not driven
  if (drive)
    drive[0] &= 0x1;
  return eCarbon_OK;
}

void CarbonTristateScalar::examineValue(UInt32* val, UInt32* drv) const
{
  mTri->getExamineValue(val, drv, 1);
}

int CarbonTristateScalar::hasDriveConflict() const
{
  return mTri->hasDriveConflict(1) ? 1 : 0;
}

void CarbonTristateScalar::calcValue(UInt32* val, UInt32* drv) const
{
  mTri->getCalculatedValue(val, drv, 1);
}

void CarbonTristateScalar::examineXVals(UInt32* val, UInt32* drive) const
{
  mTri->getExternalValues(val, drive, 1);
}

bool CarbonTristateScalar::setToDriven(CarbonModel*) 
{
  return mTri->assertExternalDrive(1);
}

bool CarbonTristateScalar::setToUndriven(CarbonModel*) 
{
  return mTri->deassertExternalDrive(1);
}

bool CarbonTristateScalar::resolveXdrive(CarbonModel*)
{
  return mTri->resolveExternalDrive(1);
}

bool CarbonTristateScalar::setWordToUndriven(int, CarbonModel* model) 
{
  return CarbonTristateScalar::setToUndriven(model);
}

bool CarbonTristateScalar::setRangeToUndriven(int, int, CarbonModel* model) 
{
  return CarbonTristateScalar::setToUndriven(model);
}


bool CarbonTristateScalar::isTristate() const { return true; }

bool CarbonTristateScalar::assignValue(const UInt32* buf,
                                       const UInt32* drive)
{
  // mTri handles whether or not buf or drive are null
  return mTri->assign(buf, drive, 1);
}


CarbonStatus CarbonTristateScalar::examineValXDriveWord(UInt32* val, UInt32* drv, int) const
{
  copyValAndXDrive(val, drv);
  return eCarbon_OK;
}

void CarbonTristateScalar::copyValAndXDrive(UInt32* val, UInt32* drv) const
{
  mTri->copyValXDriveWord(val, drv, 0, 1);
}

void CarbonTristateScalar::setRawToUndriven(CarbonModel*)
{
  mTri->undrivenInit(1);
}


CarbonScalarInput::CarbonScalarInput(UInt8* addr) :
  CarbonScalar(addr), mChangeArrayRef(NULL)
{}

CarbonScalarInput::~CarbonScalarInput()
{}

void CarbonScalarInput::assignValueInput(const UInt32* buf, CarbonModel* model)
{
  // If changed, set the change array reference to RISE or FALL
  // appropriately, otherwise keep the same value in the change
  // array. Needed in case carbonDeposit is called twice with the same
  // value before carbonSchedule is called.
  CarbonChangeType isInit = model->getHookup ()->getInit ();
  const UInt8 newVal = sGetScalarValue(buf);
  const int isChanged = (isInit | (*mScalar ^ newVal)) << newVal;
  const int isNotChanged = !isChanged;
  *mChangeArrayRef = isChanged + isNotChanged * (*mChangeArrayRef);
  *mScalar = newVal;
  doUpdateVHM(isChanged != 0, model);
}


void CarbonScalarInput::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  mChangeArrayRef = changeArrayRef;
}

CarbonChangeType* CarbonScalarInput::getChangeArrayRef()
{
  return mChangeArrayRef;
}

void CarbonScalarInput::calcChangeMask()
{
  *mChangeArrayRef = *mScalar + 1;
}

void CarbonScalarInput::recomputeChangeMask()
{
  calcChangeMask();
}

CarbonStatus CarbonScalarInput::deposit(const UInt32* buf,
                                        const UInt32* drive, 
                                        CarbonModel* model)
{
  if (buf)
    assignValueInput(buf, model);
  CarbonStatus stat = eCarbon_OK;
  if (drive && (*drive != 0))
    stat = ShellGlobal::reportSetDriveOnNonTristate(getNameAsLeaf(), model);
  return stat;
}

CarbonStatus CarbonScalarInput::depositWord (UInt32 buf, int, UInt32 drive, CarbonModel* model)
{
  assignValueInput(&buf, model);
  CarbonStatus stat = eCarbon_OK;
  if (drive != 0)
    stat = ShellGlobal::reportSetDriveOnNonTristate(getNameAsLeaf(), model);
  return stat;
}

CarbonStatus CarbonScalarInput::depositRange (const UInt32* buf, int, int, const UInt32* drive, CarbonModel* model)
{
  if (buf)
    assignValueInput(buf, model);
  CarbonStatus stat = eCarbon_OK;
  if (drive && (*drive != 0))
    stat = ShellGlobal::reportSetDriveOnNonTristate(getNameAsLeaf(), model);
  return stat;
}

void CarbonScalarInput::fastDeposit(const UInt32* buf,
                                    const UInt32*, CarbonModel* model)
{
  assignValueInput(buf, model);
}

void CarbonScalarInput::fastDepositWord (UInt32 buf, int, UInt32, CarbonModel* model)
{
  assignValueInput(&buf, model);
}

void CarbonScalarInput::fastDepositRange (const UInt32* buf, int, int, const UInt32*, CarbonModel* model)
{
  assignValueInput(buf, model);
}

void CarbonScalarInput::getTraits(Traits* traits) const
{
  CarbonScalar::getTraits(traits);
  traits->mHasInputSemantics = true;
}

bool CarbonScalarInput::isInput() const
{
  return true;
}

CarbonStatus CarbonScalarInput::examine(UInt32 *buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  if (buf)
    buf[0] = *mScalar;

  if (drive)
  {
    switch (mode)
    {
    case eIDrive:
      drive[0] = 1;
      break;
    case eCalcDrive:
    case eXDrive:
      drive[0] = 0;
      break;
    }
  }
  return eCarbon_OK;
}

void CarbonScalarInput::examineModelDrive(UInt32* driveBuf, ExamineMode mode) const
{
  switch (mode)
  {
  case eIDrive:
    driveBuf[0] = 1;
    break;
  case eCalcDrive:
  case eXDrive:
    driveBuf[0] = 0;
    break;
  }
}

CarbonStatus CarbonScalarInput::examineValXDriveWord(UInt32* val, UInt32* drv, int) const
{
  *val = *mScalar;
  *drv = 0;
  return eCarbon_OK;
}

void CarbonScalarInput::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
    *xdrive = 0;
}

CarbonTristateScalarInput::CarbonTristateScalarInput(UInt8* data, UInt8* drive)
  : CarbonTristateScalar(data, drive), mChangeArrayRef(NULL)
{}

CarbonTristateScalarInput::CarbonTristateScalarInput(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive)
  : CarbonTristateScalar(idata, idrive, xdata, xdrive), mChangeArrayRef(NULL)
{}

CarbonTristateScalarInput::~CarbonTristateScalarInput()
{}

void CarbonTristateScalarInput::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  mChangeArrayRef = changeArrayRef;
}

CarbonChangeType* CarbonTristateScalarInput::getChangeArrayRef()
{
  return mChangeArrayRef;
}

bool CarbonTristateScalarInput::setToUndriven(CarbonModel*) 
{
  bool changed = mTri->deassertExternalDrive(1);
  if ((changed & (mChangeArrayRef != NULL)) != 0)
    // not driving from outside, so just set as changed
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  return changed;
}

void CarbonTristateScalarInput::calcChangeMask()
{
  UInt8 val, drv;
  mTri->getXValues(&val, &drv, 1);
  int fall = (drv == 0) & (val == 0);
  int rise = (drv == 0) & (val == 1);
  int noXDrv = (drv != 0);
  *mChangeArrayRef = 
    fall * CARBON_CHANGE_FALL_MASK +
    rise * CARBON_CHANGE_RISE_MASK +
    noXDrv * CARBON_CHANGE_MASK;
}

void CarbonTristateScalarInput::recomputeChangeMask()
{
  calcChangeMask();
}

bool CarbonTristateScalarInput::resolveXdrive(CarbonModel*)
{
  bool changed = mTri->resolveExternalDrive(1);
  if (changed)
    calcChangeMask();
  return changed;
}


void CarbonTristateScalarInput::doDepositInput(const UInt32* buf, 
                                               const UInt32* drive,
                                               CarbonModel* model)
{
  // must set the drive first, so we can use it
  bool changed = false;
  UInt32 fixedDrive = 0;
  const UInt32* drivePtr = drive;
  
  if (! drive)
    changed = setToDriven(NULL);
  else if (sGetScalarValue(drive) != 0)
  {
    // making sure there are no extraneous 1's
    fixedDrive = 1;
    drivePtr = &fixedDrive;
  }
  
  if (assignValue(buf, drivePtr))
    changed = true;
  doUpdateVHM(changed, model);
  if (changed) {
    calcChangeMask();
  }
}

CarbonStatus CarbonTristateScalarInput::deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  doDepositInput(buf, drive, model);
  return eCarbon_OK;
}

void CarbonTristateScalarInput::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  doDepositInput(buf, drive, model);
}

void CarbonTristateScalarInput::getTraits(Traits* traits) const
{
  CarbonTristateScalar::getTraits(traits);
  traits->mHasInputSemantics = true;
}

bool CarbonTristateScalarInput::isInput() const
{
  return true;
}

void CarbonTristateScalarInput::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
    *xdrive = mTri->getXDrive(1);
}

namespace CarbonUtil 
{
  // maybe remove this function
  size_t getRangeBitWidth(int range_msb, int range_lsb)
  {
    return std::abs(range_msb - range_lsb) + 1;
  }
  
  // remove this function
  size_t getRangeNumUInt32s(int range_msb, int range_lsb)
  {
    size_t bitWidth = getRangeBitWidth(range_msb, range_lsb);
    static const size_t wordSize = sizeof(UInt32) * 8;
    return (bitWidth + (wordSize - 1)) / wordSize;
  }

  int closeDistance(int msb, int lsb, int amount, bool* add)
  {
    if (msb == lsb)
      return lsb;

    *add = true;
    if (msb > lsb)
    {
      int bound = msb - lsb + 1;
      int adder = amount;
      if (amount > bound)
        adder = bound;
      lsb += adder;
    }
    else
    {
      *add = false;
      int bound = lsb - msb + 1;
      int subtractor = amount;
      if (amount > bound)
        subtractor = bound;
      lsb -= subtractor;
    }
    return lsb;
  }

  CarbonStatus calcIndexLength(int msb, int lsb, int range_msb, int range_lsb, 
                               size_t* index, size_t* length, CarbonModel* model)
  {
    CarbonStatus stat = ShellGlobal::carbonTestRange(msb, lsb, range_msb, range_lsb, model);
    if (stat == eCarbon_OK)
    {
      *length = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);    
      *index = CarbonValRW::calcRangeIndex(range_msb, range_lsb, msb, lsb);
    }
    return stat;
  }
  
}

CarbonStatus CarbonMemory::dumpAddressRange(CarbonMemAddrT startAddress, CarbonMemAddrT endAddress, 
                                           const char* outFile, CarbonRadix format) const
{
  const ShellNet* shellNet = castMemToShellNet();
  
  CarbonMemAddrT leftAddr = getLeftAddr();
  CarbonMemAddrT rightAddr = getRightAddr();
  CarbonModel* model = getCarbonModel();

  CarbonStatus stat = ShellGlobal::carbonTestAddress(startAddress, leftAddr, rightAddr, model, shellNet->getName());
  if ((stat == eCarbon_OK) && (ShellGlobal::carbonTestAddressRange(leftAddr, rightAddr, startAddress, endAddress, model, shellNet->getName()) == eCarbon_ERROR))
    stat = eCarbon_ERROR;
  if (stat == eCarbon_OK)
  {
    // open the file
    UtOBStream out(outFile);
    if (! out)
    {
      stat = eCarbon_ERROR;
      ShellGlobal::reportOutFileOpenError(&out);
    }
    else
    {
      // always dump the lowest value address to the highest value
      // address. That is the only way to ensure this stuff will work
      // with NC. 
      CarbonMemAddrT lowest = startAddress;
      CarbonMemAddrT highest = endAddress;
      if (startAddress > endAddress)
      {
        lowest = endAddress;
        highest = startAddress;
      }
      
#if 0
      // Don't do this, in case the file is read by a simulator with
      // start and end addresses that are reversed.
      
      // dump the first address location
      {
        out.writeStr("@", 1);
        out << UtIO::hex << lowest;
        out.writeStr("\n", 1);
      }
#endif
      // good to go
      for (CarbonMemAddrT i = lowest; i <= highest; ++i)
        dumpAddress(&out, i, format);

      if (! out.close())
      {
        ShellGlobal::reportOutFileCloseError(&out);
        stat = eCarbon_ERROR;
      }
    }
  }
  return stat;
}

const ShellNetRecordMem* CarbonModelMemory::castShellNetRecordMem() const
{
  return NULL;
}

void CarbonModelMemory::postMemoryWrite()
{
  mModel->maybeMemDepositRunCombo(this);
  // There's no point in trying to set posedge/negedge changes on a
  // memory.  Just mark it as changed.
  *mChangeArrayRef = CARBON_CHANGE_MASK;
}

void CarbonModelMemory::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  mChangeArrayRef = changeArrayRef;
}  
  
CarbonChangeType* CarbonModelMemory::getChangeArrayRef()
{
  return mChangeArrayRef;
}

bool CarbonModelMemory::isDepositable() const
{
  return mModel->isMemDepositable(this);
}

CarbonModel* CarbonModelMemory::getCarbonModel() const
{ 
  return mModel; 
}

ShellNetPrimitive::~ShellNetPrimitive()
{}

const CarbonForceNet* ShellNetPrimitive::castForceNet() const
{
  return NULL;
}

const CarbonExprNet* ShellNetPrimitive::castExprNet() const
{
  return NULL;
}

const ShellNetWrapper* ShellNetPrimitive::castShellNetWrapper() const
{
  return NULL;
}

void ShellNetPrimitive::runValueChangeCB(CarbonNetValueCBData*, 
                                         UInt32*, 
                                         UInt32*,
                                         CarbonTriValShadow*,
                                         CarbonModel*) const
{
  ST_ASSERT(0, getName());
}

CarbonNet::~CarbonNet()
{}
