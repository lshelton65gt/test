// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/ShellNetWrapper.h"
#include "shell/ShellData.h"
#include "symtab/STSymbolTable.h"

/* ShellNet Wrapper */

ShellNetWrapper::~ShellNetWrapper()
{}

const ShellNetWrapper* ShellNetWrapper::castShellNetWrapper() const
{
  return this;
}

const ShellNetWrapper1To1* ShellNetWrapper::castShellNetWrapper1To1() const
{
  return NULL;
}

const ShellNetWrapper1ToN* ShellNetWrapper::castShellNetWrapper1ToN() const
{
  return NULL;
}


/* ShellNetWrapper1To1 */

ShellNetWrapper1To1::ShellNetWrapper1To1(ShellNet* net)
{
  mNet = NULL;
  mMemNet = NULL;
  replaceNet(net);
}

ShellNetWrapper1To1::~ShellNetWrapper1To1()
{
  removeSubNet();
}

const ShellNetReplay* ShellNetWrapper1To1::castShellNetReplay() const
{
  return NULL;
}

const ShellNetRecordMem* ShellNetWrapper1To1::castShellNetRecordMem() const
{
  return NULL;
}

const ShellNetOnDemand* ShellNetWrapper1To1::castShellNetOnDemand() const
{
  return NULL;
}

const ShellVisNet* ShellNetWrapper1To1::castShellVisNet() const
{
  return NULL;
}

void ShellNetWrapper1To1::removeSubNet()
{
  if (mNet && mNet->decrCount())
    delete mNet;
  
  mNet = NULL;
}

const CarbonDatabaseNode* ShellNetWrapper1To1::getDBNode() const
{
  // Pass this to the wrapped net
  return mNet->getDBNode();
}

void ShellNetWrapper1To1::replaceNet(ShellNet* net)
{
  if (net != mNet)
  {
    removeSubNet();
    
    mNet = net;
    mMemNet = net->castMemory();
    
    setName(net->getName());
  }

  /*
    Unfortunately, there is no way to know how many of the ref
    counts are from findNet and how many are from internal
    usages. So, at worst, we'll have a net that won't get deleted
    if freeNet() is called from the api since the ref count will be 
    greater than 0. But, that's ok. The net is destroyed when
    carbonDestroy is called, regardless.
  */
  putRefCnt(net->getRefCnt());
  
  /*
    Increment the reference count on the net in case the wavedumper
    is active. If the wave dumper decides to free the net, we need
    to make sure that the net is not physically deleted.
  */
  net->incrCount();
}

void ShellNetWrapper1To1::gatherWrappedNets(NetVec* netVec) const
{
  netVec->clear();
  netVec->push_back(mNet);
}

void ShellNetWrapper1To1::replaceWrappedNets(const NetVec& netVec)
{
  ST_ASSERT(netVec.size() == 1, getName());
  replaceNet(netVec[0]);
}


bool ShellNetWrapper1To1::isVector () const
{
  return mNet->isVector();
}
  
bool ShellNetWrapper1To1::isScalar() const
{
  return mNet->isScalar();
}

bool ShellNetWrapper1To1::isTristate() const
{
  return mNet->isTristate();
}

bool ShellNetWrapper1To1::isReal() const
{
  return mNet->isReal();
}

bool ShellNetWrapper1To1::isDataNonZero() const
{
  return mNet->isDataNonZero();
}

bool ShellNetWrapper1To1::setToDriven(CarbonModel* model)
{
  return mNet->setToDriven(model);
}

bool ShellNetWrapper1To1::setToUndriven(CarbonModel* model)
{
  return mNet->setToUndriven(model);
}

bool ShellNetWrapper1To1::setWordToUndriven(int index, CarbonModel* model)
{
  return mNet->setWordToUndriven(index, model);
}

bool ShellNetWrapper1To1::resolveXdrive(CarbonModel* model)
{
  return mNet->resolveXdrive(model);
}

void ShellNetWrapper1To1::getExternalDrive(UInt32* drive) const
{
  mNet->getExternalDrive(drive);
}
  
bool ShellNetWrapper1To1::setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model)
{
  return mNet->setRangeToUndriven(range_msb, range_lsb, model);
}
  
CarbonStatus ShellNetWrapper1To1::examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  return mNet->examine(buf, drive, mode, model);
}

CarbonStatus ShellNetWrapper1To1::examine (CarbonReal* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  return mNet->examine(buf, drive, mode, model);
}

CarbonStatus ShellNetWrapper1To1::examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  return mNet->examineWord(buf, index, drive, mode, model);
}

CarbonStatus ShellNetWrapper1To1::examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  return mNet->examineRange(buf, range_msb, range_lsb, drive, model);
}

CarbonStatus ShellNetWrapper1To1::deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  return mNet->deposit(buf, drive, model);
}

CarbonStatus ShellNetWrapper1To1::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  return mNet->depositWord(buf, index, drive, model);
}

CarbonStatus ShellNetWrapper1To1::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  return mNet->depositRange(buf, range_msb, range_lsb, drive, model);
}

void ShellNetWrapper1To1::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  mNet->fastDeposit(buf, drive, model);
}

void ShellNetWrapper1To1::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  mNet->fastDepositWord(buf, index, drive, model);
}

void ShellNetWrapper1To1::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  mNet->fastDepositRange(buf, range_msb, range_lsb, drive, model);
}

CarbonStatus ShellNetWrapper1To1::force(const UInt32* buf, CarbonModel* model)
{
  return mNet->force(buf, model);
}

CarbonStatus ShellNetWrapper1To1::forceWord(UInt32 buf, int index, CarbonModel* model)
{
  return mNet->forceWord(buf, index, model);
}

CarbonStatus ShellNetWrapper1To1::forceRange(const UInt32* buf, 
                                         int range_msb, int range_lsb, 
                                         CarbonModel* model)
{
  return mNet->forceRange(buf, range_msb, range_lsb, model);
}
  
CarbonStatus ShellNetWrapper1To1::release(CarbonModel* model)
{
  return mNet->release(model);
}

CarbonStatus ShellNetWrapper1To1::releaseWord(int index, CarbonModel* model)
{
  return mNet->releaseWord(index, model);
}

CarbonStatus ShellNetWrapper1To1::releaseRange(int range_msb, int range_lsb, CarbonModel* model)
{
  return mNet->releaseRange(range_msb, range_lsb, model);
}

ShellNet::Storage 
ShellNetWrapper1To1::allocShadow() const
{
  return mNet->allocShadow();
}

void ShellNetWrapper1To1::freeShadow(Storage* shadow)
{
  mNet->freeShadow(shadow);
}

void ShellNetWrapper1To1::update(Storage* shadow) const
{
  mNet->update(shadow);
}

ShellNet::ValueState 
ShellNetWrapper1To1::compare(const Storage shadow) const
{
  return mNet->compare(shadow);
}

ShellNet::ValueState 
ShellNetWrapper1To1::writeIfNotEq(char* valueStr, size_t len, Storage* shadow,
                              NetFlags flags)
{
  return mNet->writeIfNotEq(valueStr, len, shadow, flags);
}

CarbonStatus ShellNetWrapper1To1::format(char* valueStr, size_t len, 
                                     CarbonRadix strFormat, NetFlags flags,
                                     CarbonModel* model) const
{
  return mNet->format(valueStr, len, strFormat, flags, model);
}

bool ShellNetWrapper1To1::isForcible() const
{
  return mNet->isForcible();
}

bool ShellNetWrapper1To1::isInput() const
{
  return mNet->isInput();
}

const CarbonMemory* ShellNetWrapper1To1::castMemory() const
{
  // If the subnet is a memory then this is a memory.
  return (mMemNet == NULL) ? NULL : this;
}

const CarbonModelMemory* ShellNetWrapper1To1::castModelMemory() const
{
  // FIXME: Maybe I should pass NULL back for this, since 'this' can
  // never be a model mem net. Not sure yet.
#if 0
  const CarbonModelMemory* ret = NULL;
  if (mMemNet)
    ret = mMemNet->castModelMemory();
  return ret;
#else
  return NULL;
#endif
}

const CarbonVectorBase* ShellNetWrapper1To1::castVector() const
{
  // FIXME: Maybe I should always return NULL for this?
  return mNet->castVector();
}

const CarbonScalarBase* ShellNetWrapper1To1::castScalar() const
{
  // FIXME: Maybe I should always return NULL for this?
  return mNet->castScalar();
}

const ShellNetConstant* ShellNetWrapper1To1::castConstant() const
{
  // FIXME: Eventually, constants should not be wrapped.
  return mNet->castConstant();
}

const CarbonForceNet* ShellNetWrapper1To1::castForceNet() const
{
  // FIXME: Maybe I should always return NULL for this?
  return mNet->castForceNet();
}

const CarbonExprNet* ShellNetWrapper1To1::castExprNet() const
{
  // FIXME: Maybe I should always return  NULL for this?
  return mNet->castExprNet();
}

const ShellNetWrapper1To1* ShellNetWrapper1To1::castShellNetWrapper1To1() const
{
  return this;
}

void ShellNetWrapper1To1::putToZero(CarbonModel* model)
{
  mNet->putToZero(model);
}

void ShellNetWrapper1To1::putToOnes(CarbonModel* model)
{
  mNet->putToOnes(model);
}

CarbonStatus ShellNetWrapper1To1::setRange(int range_msb, int range_lsb, CarbonModel* model)
{
  return mNet->setRange(range_msb, range_lsb, model);
}

CarbonStatus ShellNetWrapper1To1::clearRange(int range_msb, int range_lsb, CarbonModel* model)
{
  return mNet->clearRange(range_msb, range_lsb, model);
}

int ShellNetWrapper1To1::hasDriveConflict() const
{
  return mNet->hasDriveConflict();
}
  
int ShellNetWrapper1To1::hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const
{
  return mNet->hasDriveConflictRange(range_msb, range_lsb);
}
  
CarbonStatus ShellNetWrapper1To1::examineValXDriveWord(UInt32* val, UInt32* drv, int index) const
{
  return mNet->examineValXDriveWord(val, drv, index);
}

void ShellNetWrapper1To1::setRawToUndriven(CarbonModel* model)
{
  mNet->setRawToUndriven(model);
}

void ShellNetWrapper1To1::getTraits(Traits* traits) const
{
  mNet->getTraits(traits);
}

ShellNet::ValueState 
ShellNetWrapper1To1::writeIfNotEqForce(char* valueStr, size_t len, Storage* shadow, NetFlags flags, ShellNet* forceMask)
{
  return mNet->writeIfNotEqForce(valueStr, len, shadow, flags, forceMask);
}

CarbonStatus ShellNetWrapper1To1::formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel* model) const
{
  return mNet->formatForce(valueStr, len, radix, flags, forceMask, model);
}

void ShellNetWrapper1To1::putChangeArrayRef(CarbonChangeType* changeArrayIndex)
{
  mNet->putChangeArrayRef(changeArrayIndex);
}

CarbonChangeType* ShellNetWrapper1To1::getChangeArrayRef()
{
  return mNet->getChangeArrayRef();
}

const UInt32* ShellNetWrapper1To1::getControlMask() const
{
  return mNet->getControlMask();
}

const ShellNet* ShellNetWrapper1To1::castMemToShellNet() const
{
  return this;
}

CarbonMemAddrT ShellNetWrapper1To1::getRightAddr() const
{
  return mMemNet->getRightAddr();
}

CarbonMemAddrT ShellNetWrapper1To1::getLeftAddr() const
{
  return mMemNet->getLeftAddr();
}

int ShellNetWrapper1To1::getRowBitWidth () const
{
  return mMemNet->getRowBitWidth();
}
  
int ShellNetWrapper1To1::getRowNumUInt32s () const
{
  return mMemNet->getRowNumUInt32s();
}

int ShellNetWrapper1To1::getRowLSB () const
{
  return mMemNet->getRowLSB();
}
  
int ShellNetWrapper1To1::getRowMSB () const
{
  return mMemNet->getRowMSB();
}

CarbonStatus ShellNetWrapper1To1::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  return mMemNet->examineMemory(address, buf);
}

UInt32 ShellNetWrapper1To1::examineMemoryWord(CarbonMemAddrT address, int index) const
{
  return mMemNet->examineMemoryWord(address, index);
}

CarbonStatus ShellNetWrapper1To1::examineMemoryRange(CarbonMemAddrT address, UInt32 *buf, int range_msb, int range_lsb) const
{
  return mMemNet->examineMemoryRange(address, buf, range_msb, range_lsb);
}

CarbonStatus ShellNetWrapper1To1::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  return mMemNet->depositMemory(address, buf);
}


CarbonStatus ShellNetWrapper1To1::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  return mMemNet->depositMemoryWord(address, buf, index);
}

CarbonStatus ShellNetWrapper1To1::depositMemoryRange(CarbonMemAddrT address, const UInt32* buf, int range_msb, int range_lsb)
{
  return mMemNet->depositMemoryRange(address, buf, range_msb, range_lsb);
}

CarbonStatus ShellNetWrapper1To1::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  return mMemNet->formatMemory(valueStr, len, strFormat, address);
}

void ShellNetWrapper1To1::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  mMemNet->dumpAddress(out, address, strFormat);
}

CarbonStatus ShellNetWrapper1To1::readmemh(const char* filename)
{
  return mMemNet->readmemh(filename);
}

CarbonStatus ShellNetWrapper1To1::readmemb(const char* filename)
{
  return mMemNet->readmemb(filename);
}

CarbonStatus ShellNetWrapper1To1::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  return mMemNet->readmemh(filename, startAddr, endAddr);
}

CarbonStatus ShellNetWrapper1To1::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  return mMemNet->readmemb(filename, startAddr, endAddr);
}

CarbonModel* ShellNetWrapper1To1::getCarbonModel() const
{
  return mMemNet->getCarbonModel();
}

bool ShellNetWrapper1To1::isDepositable() const
{
  return mMemNet->isDepositable();
}

void ShellNetWrapper1To1::runValueChangeCB(CarbonNetValueCBData* cbData, 
                                           UInt32* newVal, 
                                           UInt32* newDrv,
                                           CarbonTriValShadow* fullShadow,
                                           CarbonModel* model) const
{
  mNet->runValueChangeCB(cbData, newVal, newDrv, fullShadow, model);
}

ShellNet::ValueState
ShellNetWrapper1To1::compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                    UInt32* drive)
{
  return mNet->compareUpdateExamineUnresolved(shadow, value, drive);
}

void ShellNetWrapper1To1::updateUnresolved(Storage* shadow) const
{
  mNet->updateUnresolved(shadow);
}


/* ShellNetWrapper1ToN */

ShellNetWrapper1ToN::~ShellNetWrapper1ToN()
{}

const ShellNetWrapper1ToN* ShellNetWrapper1ToN::castShellNetWrapper1ToN() const
{
  return this;
}

void ShellNetWrapper1ToN::runValueChangeCB(CarbonNetValueCBData*, 
                                           UInt32*, 
                                           UInt32*,
                                           CarbonTriValShadow*,
                                           CarbonModel*) const
{
  ST_ASSERT(0, getName());
}

ShellNet::ValueState
ShellNetWrapper1ToN::compareUpdateExamineUnresolved(Storage*, UInt32*, UInt32*)
{
  ST_ASSERT(0, getName());
  return ShellNet::eUnchanged;
}

void ShellNetWrapper1ToN::updateUnresolved(Storage*) const
{
  ST_ASSERT(0, getName());
}


/* ShellNetReplay */

ShellNetReplay::ShellNetReplay(ShellNet* srcNet)
  : ShellNetWrapper1To1(srcNet)
{}


ShellNetReplay::~ShellNetReplay()
{}

const ShellNetReplay* ShellNetReplay::castShellNetReplay() const
{
  return this;
}

const ShellNetPlayback* ShellNetReplay::castShellNetPlayback() const
{
  return NULL;
}

const ShellNetRecord* ShellNetReplay::castShellNetRecord() const
{
  return NULL;
}
