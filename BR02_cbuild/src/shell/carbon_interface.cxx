// -*-c++-*-
/******************************************************************************
 Copyright (c) 2008-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtCheckpointStream.h"
#include "shell/CarbonModel.h"
#include "shell/CarbonSimControl.h"
#include "shell/ShellMemoryCreateInfo.h"
#include "shell/carbon_interface.h"
#include "hdl/HdlVerilogString.h"
#include "hdl/HdlIStream.h"
#include "hdl/HdlOStream.h"
#include "hdl/HdlVerilogOFileSystem.h"
#include "hdl/ReadMemX.h"

CarbonModel* carbonInterfaceNewModel(const char* model_name, const char* dbFileRoot, CarbonDBType whichdb,
                                     const char* designVersion, carbon_model_descr* descr)
{
  CarbonModel* newModel = ShellGlobal::newModel(model_name, dbFileRoot, whichdb, designVersion, descr);
  return newModel;
}

DynBitVector* carbonInterfaceAllocDynBitVector(UInt32 size)
{
  return new DynBitVector(size);
}

void carbonInterfaceFreeDynBitVector(DynBitVector* dynBV)
{
  delete dynBV;
}

void carbonInterfaceSetDynBitVector(DynBitVector* dynBV, UInt32 bit)
{
  dynBV->set(bit);
}

char* carbonInterfaceConvertPtrToStrRep(char* buffer, UInt32 bufferSize, UInt32 numCharsToConvert, const UInt32* source)
{
  char* ret = HdlVerilogString::convertToStrRep(buffer, bufferSize, numCharsToConvert, source);
  return ret;
}

char* carbonInterfaceConvert8ToStrRep(char* buffer, UInt32 bufferSize, UInt8 source)
{
  char* ret = HdlVerilogString::convertToStrRep(buffer, bufferSize, source);
  return ret;
}

char* carbonInterfaceConvert16ToStrRep(char* buffer, UInt32 bufferSize, UInt16 source)
{
  char* ret = HdlVerilogString::convertToStrRep(buffer, bufferSize, source);
  return ret;
}

char* carbonInterfaceConvert32ToStrRep(char* buffer, UInt32 bufferSize, UInt32 source)
{
  char* ret = HdlVerilogString::convertToStrRep(buffer, bufferSize, source);
  return ret;
}

char* carbonInterfaceConvert64ToStrRep(char* buffer, UInt32 bufferSize, UInt64 source)
{
  char* ret = HdlVerilogString::convertToStrRep(buffer, bufferSize, source);
  return ret;
}

void carbonInterfaceConvertValueToRawURep(char* buffer, UInt32 bufferSize, const UInt32* source)
{
  // For the %u format specifier, the LSB words appear first
  for ( UInt32 word=1; word <= bufferSize; ++word) {
    UInt32 currentWord = *(source + (bufferSize - word));
    memcpy(buffer, reinterpret_cast<const char*>(&currentWord), 4);
    buffer += 4;
  }
}

void carbonInterfaceConvertValueToRawZRep(char* buffer, UInt32 bufferSize, const UInt32* source, const UInt32* drive)
{
  // For the %z format specifier, the LSB words appear first
  // Each source word is followed by a drive word that indicates if a particular
  // bit is "0", "1", "z" or "x", using the following encoding:
  // Word0[n] | Word1[n] | Value
  // ---------------------------
  //     0    |     0    |   0
  //     1    |     0    |   1
  //     0    |     1    |   z
  //     1    |     1    |   x
  //
  for ( UInt32 word=1; word <= bufferSize; ++word) {
    UInt32 currentWord = *(source + (bufferSize - word));
    UInt32 currentDrive = *(drive + (bufferSize - word));
    memcpy(buffer, reinterpret_cast<const char*>(&currentWord), 4);
    memcpy(buffer+4, reinterpret_cast<const char*>(&currentDrive), 4);
    buffer += 8;
  }
}

void carbonInterfaceRunControlSysTask(CarbonOpaque instance, UInt32 verbosity, CarbonControlType callbackType,
                                      const char* verilogFilename, int verilogLineNumber)
{
  ControlHelper* ctrl = ShellGlobal::gcarbonPrivateGetControlHelper(instance);
  ctrl->runControlSysTask(instance, verbosity, callbackType, verilogFilename, verilogLineNumber);
}

SInt32* carbonInterfaceGetRandomSeed(CarbonOpaque instance)
{
  return ShellGlobal::gCarbonGetRandomSeed(instance);
}


ShellMemoryCBManager* carbonInterfaceAllocMemoryCBManager()
{
  return new ShellMemoryCBManager;
}

void carbonInterfaceFreeMemoryCBManager(ShellMemoryCBManager* cb)
{
  delete cb;
}

void carbonInterfaceMemoryCBManagerReportAddress(ShellMemoryCBManager* cb, SInt32 address)
{
  cb->reportAddress(address);
}

CarbonReplayCModelData* carbonInterfaceAllocReplayCModelData(carbon_model_descr* model, void* hndl, void* cmodel,
                                                               int maxInputWords, int maxOutputWords)
{
  return new CarbonReplayCModelData(model, hndl, cmodel, maxInputWords, maxOutputWords);
}

void carbonInterfaceFreeReplayCModelData(CarbonReplayCModelData* data)
{
  delete data;
}

void* carbonInterfaceGetReplayCModelUserData(CarbonReplayCModelData* data)
{
  return data->getUserData();
}

void carbonInterfaceReplayCModelDataGetMembers(CarbonReplayCModelData* data, CarbonModel** modelPtr, void** userDataPtr,
                                               void** cModelPtr, UInt32** inputsPtr, UInt32** outputsPtr)
{
  if (modelPtr != NULL) {
    *modelPtr = data->getModel();
  }
  if (userDataPtr != NULL) {
    *userDataPtr = data->getUserData();
  }
  if (cModelPtr != NULL) {
    *cModelPtr = data->getCModel();
  }
  if (inputsPtr != NULL) {
    *inputsPtr = data->getInputs();
  }
  if (outputsPtr != NULL) {
    *outputsPtr = data->getOutputs();
  }
}

void carbonInterfacePutReplayCModelDataModel(CarbonReplayCModelData* data, carbon_model_descr* model)
{
  data->putModel(model);
}

void carbonInterfacePreRecordCModel(CarbonModel* model, void* cmodelData, UInt32 context, UInt32* outputs)
{
  model->preRecordCModel(cmodelData, context, outputs);
}

void carbonInterfaceRecordCModel(CarbonModel* model, void* cmodelData, UInt32 context, UInt32* inputs, UInt32* outputs)
{
  model->recordCModel(cmodelData, context, inputs, outputs);
}

bool carbonInterfaceGetCModelValues(CarbonModel* model, void* cmodelData, UInt32 context, UInt32** outputs)
{
  CarbonModel::CModelRecoveryStatus retEnum = model->getCModelValues(cmodelData, context, outputs);
  return (retEnum == CarbonModel::eCModelUseDB);
}

bool carbonInterfaceOnDemandDivergentCModel(CarbonReplayCModelData* data)
{
  CarbonModel* model = data->getModel();
  bool ret = model->onDemandDivergentCModel();
  return ret;
}


HDLReadMemX* carbonInterfaceAllocReadMem(const char* fileName, bool hexFormat, UInt32 bitWidth,
                                             SInt64 startAddress, SInt64 endAddress, bool checkComplete,
                                             CarbonOpaque instance)
{
  return new HDLReadMemX(fileName, hexFormat, bitWidth, startAddress, endAddress, checkComplete, instance);
}

void carbonInterfaceFreeReadMem(HDLReadMemX* readmem)
{
  delete readmem;
}

bool carbonInterfaceReadMemOpenFile(HDLReadMemX* readmem)
{
  bool ret = readmem->openFile();
  return ret;
}

void carbonInterfaceReadMemCloseFile(HDLReadMemX* readmem)
{
  readmem->closeFile();
}

bool carbonInterfaceReadMemGetNextWord(HDLReadMemX* readmem, SInt64* address, UInt32* data)
{
  HDLReadMemXResult result = readmem->getNextWord(address, data);
  return (result == eRMValidData);
}


UInt32 carbonInterfaceOpenVHDLInputFileSystem(CarbonOpaque instance, const char* filename, const char* mode)
{
  HdlIStream* istr = ShellGlobal::gCarbonGetVhdlInFileSystem(instance);
  UInt32 fd;
  // NULL mode means mode is not specified
  if (mode == NULL) {
    fd = istr->HdlIFileOpen(filename);
  } else {
    fd = istr->HdlIFileOpen(filename, mode);
  }
  return fd;
}

UInt32 carbonInterfaceOpenVHDLOutputFileSystem(CarbonOpaque instance, const char* filename, const char* mode)
{
  HdlOStream* ostr = ShellGlobal::gCarbonGetVhdlOutFileSystem(instance);
  UInt32 fd;
  // NULL mode means mode is not specified
  if (mode == NULL) {
    fd = ostr->HdlOFileOpen(filename);
  } else {
    fd = ostr->HdlOFileOpen(filename, mode);
  }
  return fd;
}

UInt32 carbonInterfaceOpenVerilogOutputFileSystem(CarbonOpaque instance, const char* filename, const char* mode)
{
  VerilogOutFileSystem* fs = ShellGlobal::gCarbonGetVerilogFileSystem(instance);
  UInt32 fd;
  // NULL mode means mode is not specified
  if (mode == NULL) {
    fd = fs->VerilogOFileOpen(filename);
  } else {
    fd = fs->VerilogOFileOpen(filename, mode);
  }
  return fd;
}

UInt32 carbonInterfaceOpenVHDLInputFileSystemStatus(CarbonOpaque instance, UInt2* status, const char* filename, const char* mode)
{
  HdlIStream* istr = ShellGlobal::gCarbonGetVhdlInFileSystem(instance);
  UInt32 fd = istr->HdlIFileOpen(status, filename, mode);
  return fd;
}

UInt32 carbonInterfaceOpenVHDLOutputFileSystemStatus(CarbonOpaque instance, UInt2* status, const char* filename, const char* mode)
{
  HdlOStream* ostr = ShellGlobal::gCarbonGetVhdlOutFileSystem(instance);
  UInt32 fd = ostr->HdlOFileOpen(status, filename, mode);
  return fd;
}

bool carbonInterfaceCloseVHDLInputFileSystem(CarbonOpaque instance, UInt32 fd)
{
  HdlIStream* istr = ShellGlobal::gCarbonGetVhdlInFileSystem(instance);
  bool stat = istr->close(fd);
  return stat;
}

bool carbonInterfaceCloseVHDLOutputFileSystem(CarbonOpaque instance, UInt32 fd)
{
  HdlOStream* ostr = ShellGlobal::gCarbonGetVhdlOutFileSystem(instance);
  bool stat = ostr->close(fd);
  return stat;
}

bool carbonInterfaceCloseVerilogOutputFileSystem(CarbonOpaque instance, UInt32 fd)
{
  VerilogOutFileSystem* fs = ShellGlobal::gCarbonGetVerilogFileSystem(instance);
  bool stat = fs->close(fd);
  return stat;
}

HdlOStream* carbonInterfacePutVHDLOutputFileDescriptor(CarbonOpaque instance, UInt32 fd)
{
  HdlOStream* ostr = ShellGlobal::gCarbonGetVhdlOutFileSystem(instance);
  ostr->putTargetFileDescriptor(fd);
  return ostr;
}

HdlIStream* carbonInterfacePutVHDLInputFileDescriptor(CarbonOpaque instance, UInt32 fd)
{
  HdlIStream* istr = ShellGlobal::gCarbonGetVhdlInFileSystem(instance);
  istr->putSourceFileDescriptor(fd);
  return istr;
}

UtIStream* carbonInterfacePutNewVHDLInputFileDescriptor(CarbonOpaque instance, UInt32 fd)
{
  HdlIStream* istr = ShellGlobal::gCarbonGetVhdlInFileSystem(instance);
  UtIStream* newStr = istr->putSourceFileDescriptor(fd);
  return newStr;
}

VerilogOutFileSystem* carbonInterfacePutVerilogOutputFileDescriptor(CarbonOpaque instance, UInt32 fd)
{
  VerilogOutFileSystem* fs = ShellGlobal::gCarbonGetVerilogFileSystem(instance);
  fs->putTargetFileDescriptor(fd);
  return fs;
}

bool carbonInterfaceVHDLInputFileSystemEOF(CarbonOpaque instance, UInt32 fd)
{
  HdlIStream* istr = ShellGlobal::gCarbonGetVhdlInFileSystem(instance);
  bool stat = istr->endOfFile(fd);
  return stat;
}

bool carbonInterfaceFlushVerilogOutputFileSystem(CarbonOpaque instance)
{
  VerilogOutFileSystem* fs = ShellGlobal::gCarbonGetVerilogFileSystem(instance);
  bool stat = fs->flush();
  return stat;
}

bool carbonInterfaceFlushVerilogOutputFileSystemFD(CarbonOpaque instance, UInt32 fd)
{
  VerilogOutFileSystem* fs = ShellGlobal::gCarbonGetVerilogFileSystem(instance);
  bool stat = fs->flush(fd);
  return stat;
}

UtIOStringStream* carbonInterfaceAllocStringStream()
{
  UtIOStringStream* sstr = new UtIOStringStream();
  return sstr;
}

void carbonInterfaceHdlIStreamReadLine(HdlIStream* hstr, UtIOStringStream* sstr)
{
  hstr->readLine(sstr);
}

void carbonInterfaceHdlOStreamWriteLine(HdlOStream* hstr, UtIOStringStream* sstr)
{
  hstr->writeLine(sstr);
}

UtIStream* carbonInterfaceCastIOStringStreamToIStream(UtIOStringStream* sstr)
{
  return sstr;
}

UtOStream* carbonInterfaceCastIOStringStreamToOStream(UtIOStringStream* sstr)
{
  // UtIOStringStream doesn't inherit multiply from UtIStringStream
  // and UtOStringStream.  Instead, it inherits from UtIStringStream
  // and has a UtOStringStream member that handles operator<<.
  //
  // This may not be intentional, but UtIOStringStream::operator<<
  // returns a reference to the UtOStringStream member, not to itself.
  // It also clears the UtIStream's EOF flag, which is good because
  // we're about to write to the shared buffer.
  //
  // Return the UtOStringStream member by doing an empty write.
  UtOStream* ostr = &(*sstr << "");
  return ostr;
}

UtOStream* carbonInterfaceCastVerilogOutFileSystemToOStream(VerilogOutFileSystem* fs)
{
  return fs;
}

UtOStream* carbonInterfaceCastHdlOStreamToOStream(HdlOStream* str)
{
  return str;
}

void carbonInterfaceWriteStringToOStream(UtOStream* ostr, const char* s)
{
  *ostr << s;
}

void carbonInterfaceWriteUInt64ToOStream(UtOStream* ostr, UInt64 num)
{
  *ostr << num;
}

void carbonInterfaceWriteSInt64ToOStream(UtOStream* ostr, SInt64 num)
{
  *ostr << num;
}

void carbonInterfaceWriteUInt32ToOStream(UtOStream* ostr, UInt32 num)
{
  *ostr << num;
}

void carbonInterfaceWriteSInt32ToOStream(UtOStream* ostr, SInt32 num)
{
  *ostr << num;
}

void carbonInterfaceWriteUInt16ToOStream(UtOStream* ostr, UInt16 num)
{
  *ostr << num;
}

void carbonInterfaceWriteSInt16ToOStream(UtOStream* ostr, SInt16 num)
{
  *ostr << num;
}

void carbonInterfaceWriteDoubleToOStream(UtOStream* ostr, double num)
{
  *ostr << num;
}

void carbonInterfaceWriteCharToOStream(UtOStream* ostr, const char ch)
{
  *ostr << ch;
}

void carbonInterfaceWriteBufToOStream(UtOStream* ostr, const char* buf, UInt32 size)
{
  ostr->write(buf, size);
}

void carbonInterfaceReadUInt64FromIStream(UtIStream* istr, UInt64& num)
{
  *istr >> num;
}

void carbonInterfaceReadSInt64FromIStream(UtIStream* istr, SInt64& num)
{
  *istr >> num;
}

void carbonInterfaceReadUInt32FromIStream(UtIStream* istr, UInt32& num)
{
  *istr >> num;
}

void carbonInterfaceReadSInt32FromIStream(UtIStream* istr, SInt32& num)
{
  *istr >> num;
}

void carbonInterfaceReadUInt16FromIStream(UtIStream* istr, UInt16& num)
{
  *istr >> num;
}

void carbonInterfaceReadSInt16FromIStream(UtIStream* istr, SInt16& num)
{
  *istr >> num;
}

void carbonInterfaceReadUInt8FromIStream(UtIStream* istr, UInt8& num)
{
  *istr >> num;
}

void carbonInterfaceReadDoubleFromIStream(UtIStream* istr, double& num)
{
  *istr >> num;
}

void carbonInterfaceReadCharFromIStream(UtIStream* istr, char& ch)
{
  *istr >> ch;
}

void carbonInterfaceReadBVRefFromIStream(UtIStream* istr, UInt32* data, UInt32 size, UInt32 offset)
{
  // This does something like UtIStream::operator>>(BVRef<false>&),
  // except that the BVRef elements have already been provided.

  // Read the stream into a DynBitVector
  DynBitVector bv(size);
  istr->readDynBitVec(&bv, false);

  // Resize to match the range of the UInt32 array referenced by the
  // BVRef, and shift to match its offset.
  bv.resize(size + offset);
  bv <<= offset;
  // Copy into the BVRef's array
  memcpy(data, bv.getUIntArray(), bv.getUIntArraySize() * sizeof(UInt32));
}


void carbonInterfaceSetOStreamRadixDec(UtOStream* ostr)
{
  *ostr << UtIO::dec;
}

void carbonInterfaceSetOStreamRadixSDec(UtOStream* ostr)
{
  *ostr << UtIO::sdec;
}

void carbonInterfaceSetOStreamRadixHex(UtOStream* ostr)
{
  *ostr << UtIO::hex;
}

void carbonInterfaceSetOStreamRadixHEX(UtOStream* ostr)
{
  *ostr << UtIO::HEX;
}

void carbonInterfaceSetOStreamRadixOct(UtOStream* ostr)
{
  *ostr << UtIO::oct;
}

void carbonInterfaceSetOStreamRadixBin(UtOStream* ostr)
{
  *ostr << UtIO::bin;
}

void carbonInterfaceSetIStreamRadixDec(UtIStream* istr)
{
  *istr >> UtIO::dec;
}

void carbonInterfaceSetIStreamRadixSDec(UtIStream* istr)
{
  *istr >> UtIO::sdec;
}

void carbonInterfaceSetIStreamRadixHex(UtIStream* istr)
{
  *istr >> UtIO::hex;
}

void carbonInterfaceSetIStreamRadixHEX(UtIStream* istr)
{
  *istr >> UtIO::HEX;
}

void carbonInterfaceSetIStreamRadixOct(UtIStream* istr)
{
  *istr >> UtIO::oct;
}

void carbonInterfaceSetIStreamRadixBin(UtIStream* istr)
{
  *istr >> UtIO::bin;
}

void carbonInterfaceSetOStreamMaxWidth(UtOStream* ostr, UInt32 width)
{
  *ostr << UtIO::setMaxw(width);
}

void carbonInterfaceSetIStreamMaxWidth(UtIStream* istr, UInt32 width)
{
  *istr >> UtIO::setMaxw(width);
}

void carbonInterfaceSetOStreamFill(UtOStream* ostr, char fill)
{
  *ostr << UtIO::setfill(fill);
}

void carbonInterfaceSetIStreamFill(UtIStream* istr, char fill)
{
  *istr >> UtIO::setfill(fill);
}

void carbonInterfaceSetOStreamWidth(UtOStream* ostr, UInt32 width)
{
  *ostr << UtIO::setw(width);
}

void carbonInterfaceSetIStreamWidth(UtIStream* istr, UInt32 width)
{
  *istr >> UtIO::setw(width);
}

void carbonInterfaceSetOStreamPrecision(UtOStream* ostr, UInt32 precision)
{
  *ostr << UtIO::setprecision(precision);
}

void carbonInterfaceSetIStreamPrecision(UtIStream* istr, UInt32 precision)
{
  *istr >> UtIO::setprecision(precision);
}

void carbonInterfaceSetOStreamFloatModeExponent(UtOStream* ostr)
{
  *ostr << UtIO::exponent;
}

void carbonInterfaceSetOStreamFloatModeFixed(UtOStream* ostr)
{
  *ostr << UtIO::fixed;
}

void carbonInterfaceSetOStreamFloatModeGeneral(UtOStream* ostr)
{
  *ostr << UtIO::general;
}

void carbonInterfaceSetIStreamFloatModeExponent(UtIStream* istr)
{
  *istr >> UtIO::exponent;
}

void carbonInterfaceSetIStreamFloatModeFixed(UtIStream* istr)
{
  *istr >> UtIO::fixed;
}

void carbonInterfaceSetIStreamFloatModeGeneral(UtIStream* istr)
{
  *istr >> UtIO::general;
}

void carbonInterfaceSetOStreamJustificationRight(UtOStream* ostr)
{
  *ostr << UtIO::right;
}

void carbonInterfaceSetOStreamJustificationLeft(UtOStream* ostr)
{
  *ostr << UtIO::left;
}

void carbonInterfaceSetOStreamJustificationInternal(UtOStream* ostr)
{
  *ostr << UtIO::internal;
}

void carbonInterfaceSetIStreamJustificationRight(UtIStream* istr)
{
  *istr >> UtIO::right;
}

void carbonInterfaceSetIStreamJustificationLeft(UtIStream* istr)
{
  *istr >> UtIO::left;
}

void carbonInterfaceSetIStreamJustificationInternal(UtIStream* istr)
{
  *istr >> UtIO::internal;
}

void carbonInterfaceSetOStreamPosDisplayShow(UtOStream* ostr)
{
  *ostr << UtIO::showpositive;
}

void carbonInterfaceSetOStreamPosDisplayNoShow(UtOStream* ostr)
{
  *ostr << UtIO::noshowpositive;
}

void carbonInterfaceOStreamEndl(UtOStream* ostr)
{
  *ostr << UtIO::endl;
}

bool carbonInterfaceIStreamFail(UtIStream* istr)
{
  bool ret = istr->fail();
  return ret;
}

UtOStream* carbonInterfaceGetCout()
{
  return &(UtIO::cout());
}

UtOStream* carbonInterfaceGetCerr()
{
  return &(UtIO::cerr());
}

UInt32 carbonInterfaceICheckpointStreamRead(UtICheckpointStream* str, void* buffer, UInt32 numBytes)
{
  return str->read(buffer, numBytes);
}

UInt32 carbonInterfaceOCheckpointStreamWrite(UtOCheckpointStream* str, const void* buffer, UInt32 numBytes)
{
  return str->write(buffer, numBytes);
}

UInt32 carbonInterfaceICheckPointStreamFailed (UtICheckpointStream *str)
{
  return str->fail ();
}

UInt32 carbonInterfaceOCheckPointStreamFailed (UtOCheckpointStream *str)
{
  return str->fail ();
}

void carbonInterfaceSaveIOStringStream(UtOCheckpointStream* str, UtIOStringStream** pp)
{
  UtIOStringStream::save(*str, pp);
}

void carbonInterfaceRestoreIOStringStream(UtICheckpointStream* str, UtIOStringStream** pp)
{
  UtIOStringStream::restore(*str, pp);
}

struct carbon_model_descr *carbonInterfaceGetCurrentModel()
{
  CarbonObjectID *model = ShellGlobal::gGetCurrentModel();
  return model;
}

void carbonInterfaceSetCurrentModel(carbon_model_descr* descr)
{
  ShellGlobal::gSetCurrentModel(descr);
}

void carbonInterfaceAddMessageContext(carbon_model_descr* descr)
{
  ShellGlobal::gCarbonAddMessageContext(descr);
}

void carbonInterfaceReportProfileGenerate(bool val)
{
  ShellGlobal::reportSHLProfileGenerate(val);
}

