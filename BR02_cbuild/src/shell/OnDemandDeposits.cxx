// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/OnDemandDeposits.h"
#include "shell/ShellNetOnDemand.h"
#include "util/UtConv.h"
#include "util/Util.h"

OnDemandDeposits::OnDemandDeposits()
{
  mNumNets = 0;
  mNumVals = 0;
  mBuf = NULL;
  mBufSize = 0;
}

OnDemandDeposits::OnDemandDeposits(const OnDemandDeposits &other)
{
  mNumNets = other.mNumNets;
  mNumVals = other.mNumVals;
  mBufSize = other.mBufSize;

  // Allocate our buffer, copy, and set pointers
  mBuf = carbonmem_alloc(mBufSize);
  memcpy(mBuf, other.mBuf, mBufSize);
}

OnDemandDeposits::~OnDemandDeposits()
{
  if (mBuf)
    carbonmem_dealloc(mBuf, mBufSize);
  for (NetMap::iterator iter = mNetMap.begin(); iter != mNetMap.end(); ++iter)
    delete iter->second;
}

OnDemandDeposits::NetMapEntry *OnDemandDeposits::addNet(ShellNetOnDemand *net, OnDemandDeposits::NetMapEntry *master)
{
  // Here's the basic idea:
  // We store the net pointers and deposit values, conceptually, in
  // separate arrays. However, we want fast memcmp operations on the
  // pointers/values, so we allocate these two arrays in one
  // continuous memory region.

  // Whenever a new net is added, we need to expand the arrays its
  // pointer and value are stored in.  At some point we might want
  // to grow the arrays in larger chunks, but for now this will work.
  // This is only called when carbonFindNet is called, anyway.

  // First, determine some info about this net
  CarbonUInt32 num_uint32 = net->getNumUInt32s();
  CarbonUInt32 last_mask = CarbonValRW::getWordMask(net->getBitWidth());

  // Allocate an entry for it
  NetMapEntry *entry = new NetMapEntry;
  entry->num_uint32 = num_uint32;
  entry->last_mask = last_mask;
  entry->subord = NULL;

  if (master) {
    // We should already have an entry (the master) in the map.  Make
    // sure it's the one we expect.
    NetMap::iterator iter = mNetMap.find(net);
    INFO_ASSERT((iter != mNetMap.end()) && (iter->second == master), "Master for subordinate net is missing or incorrect");
    // Point to the subordinate
    master->subord = entry;
  } else {
    // Save in the map.  It shouldn't already be there
    INFO_ASSERT(mNetMap.find(net) == mNetMap.end(), "Duplicate deposit net registered");
    mNetMap[net] = entry;
  }

  // Resize the buffer - how much more space do we need?
  CarbonUInt32 new_buf_size = mBufSize + sizeof(ShellNetOnDemand*) + entry->num_uint32 * sizeof(CarbonUInt32);
  // Allocate the new buffer
  void *new_buf = carbonmem_alloc(new_buf_size);

  // If we have nets allocated already, we have to copy them after we allocate more memory
  // and reorder the buffer, so save the old locations and sizes.
  UInt32 old_num_vals = getNumVals();
  UInt32 old_num_nets = getNumNets();
  UInt32 *old_val_ptr = NULL;
  ShellNetOnDemand **old_net_ptr = NULL;
  if (mBuf) {
    old_val_ptr = getValPtr(0);
    old_net_ptr = getNetPtr(0);
  }

  // Temporarily save the old buffer, and point to the new one.
  void *old_buf = mBuf;
  mBuf = new_buf;

  // Our new entry is at the end of each array
  entry->net_index = mNumNets;
  entry->val_index = mNumVals;

  // Now we can update the counters to reflect our addition
  mNumVals += num_uint32;
  ++mNumNets;

  if (old_buf) {
    // Copy the old data into its location in the new buffer.
    // The get[Val|Net]Ptr methods operate on the current value of mBuf, which
    // we've updated to the new buffer.
    CarbonUInt32 *new_val_ptr = getValPtr(0);
    ShellNetOnDemand **new_net_ptr = getNetPtr(0);
    
    // Copy the old values and free the old buffer
    memcpy(new_val_ptr, old_val_ptr, old_num_vals * sizeof(CarbonUInt32));
    memcpy(new_net_ptr, old_net_ptr, old_num_nets * sizeof(ShellNetOnDemand*));
    carbonmem_dealloc(old_buf, mBufSize);
  }
  // Finally, update the new buffer size
  mBufSize = new_buf_size;

  // Record this entry with the ShellNet so we don't need to do a map
  // lookup on a recorded deposit.
  // Don't do this on subordinate entries, because the net already has
  // an entry for the master.
  if (master == NULL) {
    net->setDepositEntry(entry);
  }

  // Clear the entries in the array
  getNet(entry->net_index) = NULL;
  memset(getValPtr(entry->net_index), 0, entry->num_uint32 * sizeof(UInt32));

  return entry;
}

void OnDemandDeposits::add(ShellNetOnDemand *net, OnDemandDeposits::NetMapEntry *entry, const CarbonUInt32 *val)
{
  INFO_ASSERT(entry, "NetMapEntry is NULL");

  // If this entry has a subordinate and the primary entry has already
  // been driven (i.e. there's a valid pointer at its net index), use
  // the subordinate entry instead
  NetMapEntry *subord = entry->subord;
  ShellNetOnDemand **net_ptr = getNetPtr(entry->net_index);
  if (subord && *net_ptr) {
    // We also need to check whether the subordinate already has a
    // deposit saved.  We only save the two most recent deposits (it's
    // all we need for an accurate change mask), so we have to copy
    // the current subordinate to the primary before continuing.
    if (getNet(subord->net_index)) {
      // The nets are the same, so just copy the deposit value
      memcpy(getValPtr(entry->val_index), getValPtr(subord->val_index), entry->num_uint32 * sizeof(UInt32));
    }

    // OK to reference the subordinate entry now
    entry = subord;
    net_ptr = getNetPtr(entry->net_index);
  }

  // Copy its pointer and value to the array.
  *net_ptr = net;
  if (entry->num_uint32 == 1) {
    getVal(entry->val_index) = *val & entry->last_mask;
  } else {
    memcpy(getValPtr(entry->val_index), val, entry->num_uint32 * sizeof(CarbonUInt32));
    // Mask out any dirty bits in the last word
    getVal(entry->val_index + entry->num_uint32 - 1) &= entry->last_mask;
  }
}

bool OnDemandDeposits::operator==(const OnDemandDeposits &other) const
{
  // Make sure the numbers of possible nets and values are the same,
  // then compare the buffer
  return ((mNumVals == other.mNumVals) &&
          (mNumNets == other.mNumNets) &&
          (carbon_duffcmp(static_cast<UInt32*>(mBuf), &static_cast<UInt32*>(mBuf)[mBufSize/sizeof(UInt32)], static_cast<UInt32*>(other.mBuf)) == 0));
}

ShellNetOnDemand *OnDemandDeposits::getDivergentNet(const OnDemandDeposits &other)
{
  // Compare two deposit sets, reporting the net that is different.
  // There may be multiple nets that are different, but we'll only
  // report the first one we see.
  // This isn't very efficient, but this is only used in debug
  // mode, in which speed is not important.

  // First, if the number of nets or the number of values differ
  // between the two sets, there must be at least one net that's
  // only in one set, so we need to find it.  We'll go through all
  // the nets in the larger set, trying to find its match in the smaller
  // one.
  if ((mNumVals != other.mNumVals) ||
      (mNumNets != other.mNumNets)) {
    const OnDemandDeposits *set1, *set2;
    if (mNumNets > other.mNumNets) {
      set1 = this;
      set2 = &other;
    } else {
      set1 = &other;
      set2 = this;
    }
    for (NetMap::const_iterator iter = set1->mNetMap.begin(); iter != set1->mNetMap.end(); ++iter) {
      ShellNetOnDemand *net = iter->first;
      if (set2->mNetMap.find(net) == set2->mNetMap.end()) {
        return net;
      }
    }
    // How did we get here?
    INFO_ASSERT(0, "couldn't find unique net in different-sized deposit sets");
  }

  // The net and value arrays are the same sizes.
  // Go through the net arrays, looking for diffs.
  // If one pointer is valid and the other is null,
  // report that net.  If both are valid but different,
  // pick one.
  for (UInt32 i = 0; i < mNumNets; ++i) {
    if (getNet(i) != other.getNet(i)) {
      // Which set's net are we going to report?
      // We'll choose this one unless it's null.
      ShellNetOnDemand *net = getNet(i) ? getNet(i) : other.getNet(i);
      INFO_ASSERT(mNetMap.find(net) != mNetMap.end(), "couldn't find divergent net pointer in known nets");
      return net;
    }
  }

  // If the net pointers matched, go through the values
  for (UInt32 i = 0; i < mNumVals; ++i) {
    if (getVal(i) != other.getVal(i)) {
      // Iterate over all the nets in the set, trying
      // to match the offset to a net.  The offset might
      // not actually be in the map, since a net's value
      // may span multiple offsets.  We'll go through the
      // whole set, and choose the largest offset that's
      // <= our index.
      ShellNetOnDemand *net = 0;
      UInt32 best_index = 0;
      for (NetMap::const_iterator iter = mNetMap.begin(); iter != mNetMap.end(); ++iter) {
         NetMapEntry *entry = iter->second;
         if (entry->val_index <= i) {
           // Save if we don't have a candidate yet, or
           // if it's better
           if ((!net) || (entry->val_index > best_index)) {
             net = iter->first;
             best_index = entry->val_index;
           }
         }
      }
      // We should have found a net
      INFO_ASSERT(net, "couldn't map deposit value offset to net");
      return net;
    }
  }

  // How did we get here?
  INFO_ASSERT(0, "couldn't find divergent deposit net");
  return NULL;
}

void OnDemandDeposits::apply(CarbonModel *obj)
{
  // Walk our map of deposited nets, depositing
  // each one's associated value.
  for (NetMap::iterator iter = mNetMap.begin(); iter != mNetMap.end(); ++iter) {
    NetMapEntry *entry = iter->second;
    ShellNetOnDemand *net;
    CarbonUInt32 *val;
    // Find the pointer to the net and its deposit value
    net = getNet(entry->net_index);
    val = getValPtr(entry->val_index);
    // If the net wasn't deposited this cycle, its pointer is null
    if (net) {
      // We want to deposit to the real net, not the onDemand wrapper one
      ShellNet *real_net = net->getNet();
      real_net->deposit(val, 0, obj);

      // If there is a subordinate entry with a valid net pointer, apply that deposit as well
      NetMapEntry *subord = entry->subord;
      if (subord && getNet(subord->net_index)) {
        // The net pointer is the same - only the values are different
        val = getValPtr(subord->val_index);
        real_net->deposit(val, 0, obj);
      }
    }
  }
}

OnDemandDepositFactory::OnDemandDepositFactory()
  : mProtected(0)
{
}

OnDemandDepositFactory::~OnDemandDepositFactory()
{
  clearAll();
}

OnDemandDeposits *OnDemandDepositFactory::create()
{
  // Each onDemand manager only creates one OnDemandDeposits object.
  // It needs to be persistent because it has all the information about
  // the depositable nets.  This object gets recorded separately
  // so it won't be cleared when idle state tracking is reset.

  INFO_ASSERT(mProtected == NULL, "Protected deposit already allocated");
  mProtected = new OnDemandDeposits;
  return mProtected;
}

OnDemandDeposits *OnDemandDepositFactory::copy(const OnDemandDeposits *other)
{
  // All the saved deposits for idle state tracking are just copies
  // of the current state of the protected deposit object.  These
  // are safe to be cleared when state tracking is reset.

  OnDemandDeposits *ptr = new OnDemandDeposits(*other);
  mAllocated.push_back(ptr);
  return ptr;
}

void OnDemandDepositFactory::freeLast()
{
  // Frees the last allocated deposit

  if (!mAllocated.empty()) {
    delete mAllocated.back();
    mAllocated.pop_back();
  }
}

void OnDemandDepositFactory::clear()
{
  // Free all the non-protected objects

  for (CarbonUInt32 i = 0; i < mAllocated.size(); ++i)
    delete mAllocated[i];
  mAllocated.clear();
}

void OnDemandDepositFactory::clearAll()
{
  // Free all allocated objects

  clear();
  delete mProtected;
  mProtected = NULL;
}

size_t OnDemandDepositSetHelper::hash(const OnDemandDeposits *var) const
{
  // Generate a checksum based on the net pointers and values
  size_t hashval = 0;
  for (CarbonUInt32 i = 0; i < var->getNumNets(); ++i)
    hashval ^= (reinterpret_cast<size_t>(var->getNet(i)) >> 2);
  for (CarbonUInt32 i = 0; i < var->getNumVals(); ++i)
    hashval ^= var->getVal(i);

  return hashval;
}

bool OnDemandDepositSetHelper::equal(const OnDemandDeposits *v1, const OnDemandDeposits *v2) const
{
  return *v1 == *v2;
}
