// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/ShellNetOnDemand.h"
#include "shell/OnDemandMgr.h"
#include "shell/OnDemandDebug.h"
#include "CarbonDatabaseNode.h"
#include "iodb/IODBRuntime.h"

ShellNetOnDemand::ShellNetOnDemand(ShellNet *net, OnDemandMgr *mgr)
  : ShellNetWrapper1To1(net),
    mMgr(mgr),
    mShadowValid(false),
    mShadowValue(NULL),
    mShadowDrive(NULL),
    mShadowDriveNull(false),
    mShadowSize(0),
    mDebug(NULL),
    mDepEntry(NULL)
{
  // Determine if this net is an idle deposit net, i.e. one whose
  // deposits are tracked when looking for an idle pattern.  Clocks
  // will always be idle deposit nets, along with user-specified nets.
  CarbonHookup *hookup = mMgr->getModel()->getHookup();
  IODBRuntime *iodb = hookup->getDB();
  STAliasedLeafNode *node = net->getNameAsLeaf();
  mIsClock = iodb->isMarkedClock(node);
  mIdleDeposit = mIsClock || iodb->isOnDemandIdleDeposit(node);

  // For non idle deposit nets, we shadow deposits.  This allows us to do a
  // local change detection, preventing onDemand from aborting idle
  // state/search on recurring deposits that don't actually change
  // value.
  //
  // Note that we can't do this for depositable state outputs, since
  // their actual values may be changed by the model.
  const ShellDataBOM *bomdata = ShellSymTabBOM::getLeafBOM(node);
  mDoShadow = !mIdleDeposit && !bomdata->isStateOutput();
  
  if (mDoShadow) {
    UInt32 num_uint32 = getNumUInt32s();
    mShadowSize = num_uint32 * sizeof(UInt32);
    mShadowValue = CARBON_ALLOC_VEC(UInt32, num_uint32);
    mShadowDrive = CARBON_ALLOC_VEC(UInt32, num_uint32);
  }

  // Create a change info if debug mode is enabled
  if (mMgr->debugModeEnabled()) {
    initDebug();
  }
}

ShellNetOnDemand::~ShellNetOnDemand()
{
  CARBON_FREE_VEC(mShadowValue, UInt32, mShadowSize / sizeof(UInt32));
  CARBON_FREE_VEC(mShadowDrive, UInt32, mShadowSize / sizeof(UInt32));
  delete mDebug;
}

const ShellNetOnDemand *ShellNetOnDemand::castShellNetOnDemand() const
{
  return this;
}

// There are three classes of API functions, as pertaining to
// onDemand idle mode:
//
// 1.  Those that operate entirely in the onDemand manager without
// touching the Carbon Model.  For now, that's just carbonDeposit() and
// carbonDepositFast(), with null drive.
//
// 2.  Those that require the Carbon Model to be restored, but allow idle mode
// to continue.  This class includes carbonExamine() and friends.
//
// 3.  Those that require idle mode to be exited.  Everything else
// (deposits other than the ones listed above, forces, etc.) fall into
// this category.

bool ShellNetOnDemand::isDataNonZero() const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::isDataNonZero();
}

bool ShellNetOnDemand::setToDriven(CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::setToDriven(model);
}

bool ShellNetOnDemand::setToUndriven(CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::setToUndriven(model);
}

bool ShellNetOnDemand::setWordToUndriven(int index, CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::setWordToUndriven(index, model);
}

bool ShellNetOnDemand::resolveXdrive(CarbonModel* model)
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::resolveXdrive(model);
}

#if 0
void ShellNetOnDemand::getExternalDrive(UInt32* drive) const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  ShellNetWrapper1To1::getExternalDrive(drive);
}
#endif

bool ShellNetOnDemand::setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::setRangeToUndriven(range_msb, range_lsb, model);
}

CarbonStatus ShellNetOnDemand::examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  // Try to get a cached examine from this idle state.
  // This will automatically restore the model state if it fails.
  CarbonStatus status;
  OnDemandCachedExamines::Examine *examine = NULL;
  if (mMgr->getCachedExamine(this, &status, buf, drive, mode, mDebug, &examine)) {
    return status;
  }
  // Do the real examine
  status = ShellNetWrapper1To1::examine(buf, drive, mode, model);
  // If the Examine pointer was set, onDemand is idle, and we should
  // cache the examine.
  if (examine)
    examine->save(status, buf, drive, mode);
  return status;
}

CarbonStatus ShellNetOnDemand::examine (CarbonReal* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::examine(buf, drive, mode, model);
}

CarbonStatus ShellNetOnDemand::examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::examineWord(buf, index, drive, mode, model);
}

CarbonStatus ShellNetOnDemand::examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::examineRange(buf, range_msb, range_lsb, drive, model);
}

CarbonStatus ShellNetOnDemand::deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  // Notwithstanding the net's status, this is a valid idle deposit if
  // the value buffer is set and the drive is null.
  bool idleDepositType = buf && !drive;
  if (!idleDepositType || !mIdleDeposit) {
    // This deposit isn't supported
    // Only report it if the value really changed, considering that some
    // nets don't do this change detection.
    if (!mDoShadow || shadowChanged(buf, drive)) {
      CarbonOnDemandDebugType type = idleDepositType ? eCarbonOnDemandDebugNonIdleNet : eCarbonOnDemandDebugNonIdleChange;
      mMgr->setNonIdleAccess(mDebug, type);
      return ShellNetWrapper1To1::deposit(buf, drive, model);
    }
    return eCarbon_OK;
  }

  CarbonStatus ret = eCarbon_OK;
  // Only do the deposit if we're actually running the model.
  CarbonOnDemandMode mode = mMgr->getMode();
  if (mode != eCarbonOnDemandIdle) {
    ret = ShellNetWrapper1To1::deposit(buf, drive, model);
  }

  // Record the deposit if we're looking or idle
  if ((mode == eCarbonOnDemandLooking) || (mode == eCarbonOnDemandIdle)) {
    mMgr->addDeposit(this, mDepEntry, buf);
  }

  return ret;
}

CarbonStatus ShellNetOnDemand::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::depositWord(buf, index, drive, model);
}

CarbonStatus ShellNetOnDemand::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::depositRange(buf, range_msb, range_lsb, drive, model);
}

void ShellNetOnDemand::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  // Notwithstanding the net's status, this is a valid idle deposit if
  // the value buffer is set and the drive is null.
  bool idleDepositType = buf && !drive;
  if (!idleDepositType || !mIdleDeposit) {
    // This deposit isn't supported
    // Only report it if the value really changed, considering that some
    // nets don't do this change detection.
    if (!mDoShadow || shadowChanged(buf, drive)) {
      CarbonOnDemandDebugType type = idleDepositType ? eCarbonOnDemandDebugNonIdleNet : eCarbonOnDemandDebugNonIdleChange;
      mMgr->setNonIdleAccess(mDebug, type);
      ShellNetWrapper1To1::fastDeposit(buf, drive, model);
    }
    return;
  }

  // Only do the deposit if we're actually running the model.
  CarbonOnDemandMode mode = mMgr->getMode();
  if (mode != eCarbonOnDemandIdle) {
    ShellNetWrapper1To1::fastDeposit(buf, drive, model);
  }

  // Record the deposit if we're looking or idle
  if ((mode == eCarbonOnDemandLooking) || (mode == eCarbonOnDemandIdle)) {
    mMgr->addDeposit(this, mDepEntry, buf);
  }
}

void ShellNetOnDemand::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  ShellNetWrapper1To1::fastDepositWord(buf, index, drive, model);
}

void ShellNetOnDemand::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  ShellNetWrapper1To1::fastDepositRange(buf, range_msb, range_lsb, drive, model);
}

CarbonStatus ShellNetOnDemand::force(const UInt32* buf, CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::force(buf, model);
}

CarbonStatus ShellNetOnDemand::forceWord(UInt32 buf, int index, CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::forceWord(buf, index, model);
}

CarbonStatus ShellNetOnDemand::forceRange(const UInt32* buf, 
                        int range_msb, int range_lsb, 
                        CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::forceRange(buf, range_msb, range_lsb, model);
}

CarbonStatus ShellNetOnDemand::release(CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::release(model);
}

CarbonStatus ShellNetOnDemand::releaseWord(int index, CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::releaseWord(index, model);
}

CarbonStatus ShellNetOnDemand::releaseRange(int range_msb, int range_lsb, CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::releaseRange(range_msb, range_lsb, model);
}

CarbonStatus ShellNetOnDemand::format(char* valueStr, size_t len, 
                    CarbonRadix strFormat, NetFlags flags,
                    CarbonModel* model) const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::format(valueStr, len, strFormat, flags, model);
}

void ShellNetOnDemand::putToZero(CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  ShellNetWrapper1To1::putToZero(model);
}

void ShellNetOnDemand::putToOnes(CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  ShellNetWrapper1To1::putToOnes(model);
}

CarbonStatus ShellNetOnDemand::setRange(int range_msb, int range_lsb, CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::setRange(range_msb, range_lsb, model);
}

CarbonStatus ShellNetOnDemand::clearRange(int range_msb, int range_lsb, CarbonModel* model)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  mShadowValid = false;
  return ShellNetWrapper1To1::clearRange(range_msb, range_lsb, model);
}

int ShellNetOnDemand::hasDriveConflict() const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::hasDriveConflict();
}

int ShellNetOnDemand::hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::hasDriveConflictRange(range_msb, range_lsb);
}

#if 0
CarbonStatus ShellNetOnDemand::examineValXDriveWord(UInt32* val, UInt32* drv, int index) const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::examineValXDriveWord(val, drv, index);
}
#endif

CarbonStatus ShellNetOnDemand::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::examineMemory(address, buf);
}

UInt32 ShellNetOnDemand::examineMemoryWord(CarbonMemAddrT address, int index) const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::examineMemoryWord(address, index);
}

CarbonStatus ShellNetOnDemand::examineMemoryRange(CarbonMemAddrT address, UInt32 *buf, int range_msb, int range_lsb) const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::examineMemoryRange(address, buf, range_msb, range_lsb);
}

CarbonStatus ShellNetOnDemand::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  return ShellNetWrapper1To1::depositMemory(address, buf);
}

CarbonStatus ShellNetOnDemand::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  return ShellNetWrapper1To1::depositMemoryWord(address, buf, index);
}

CarbonStatus ShellNetOnDemand::depositMemoryRange(CarbonMemAddrT address, const UInt32* buf, int range_msb, int range_lsb)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  return ShellNetWrapper1To1::depositMemoryRange(address, buf, range_msb, range_lsb);
}

CarbonStatus ShellNetOnDemand::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::formatMemory(valueStr, len, strFormat, address);
}

void ShellNetOnDemand::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  mMgr->restoreState(eCarbonOnDemandDebugRestore, mDebug);
  return ShellNetWrapper1To1::dumpAddress(out, address, strFormat);
}

CarbonStatus ShellNetOnDemand::readmemh(const char* filename)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  return ShellNetWrapper1To1::readmemh(filename);
}

CarbonStatus ShellNetOnDemand::readmemb(const char* filename)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  return ShellNetWrapper1To1::readmemb(filename);
}

CarbonStatus ShellNetOnDemand::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  return ShellNetWrapper1To1::readmemh(filename, startAddr, endAddr);
}

CarbonStatus ShellNetOnDemand::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  mMgr->setNonIdleAccess(mDebug, eCarbonOnDemandDebugNonIdleChange);
  return ShellNetWrapper1To1::readmemb(filename, startAddr, endAddr);
}

bool ShellNetOnDemand::shadowChanged(const UInt32 *val, const UInt32 *drive)
{
  // NOTE: The goal of doing local change detection on non idle deposit nets
  // is to prevent somewhat dumb test environments from breaking out
  // of an idle state due to unnecessary deposits, in which the new
  // value is the same as the current value.  I don't think there's a
  // need to handle exceedingly dumb test environments, in which the
  // effective value of the new deposit matches the current value, but
  // the raw val/drive differs because the unused bits don't match.
  // This is handled properly from a functional perspective, but will
  // not take advantage of the local change detection.

  // If the shadow is valid, and the value/drive match, there's no change
  if (mShadowValid && val &&
      (memcmp(val, mShadowValue, mShadowSize) == 0) &&
      ((mShadowDriveNull && (drive == NULL)) ||
       (!mShadowDriveNull && drive && (memcmp(drive, mShadowDrive, mShadowSize) == 0)))) {
    return false;
  }

  // We only save when the value is valid
  if (val == NULL) {
    mShadowValid = false;
    return true;
  }

  // Update the shadow and return that there was a change
  mShadowValid = true;
  memcpy(mShadowValue, val, mShadowSize);
  if (drive) {
    mShadowDriveNull = false;
    memcpy(mShadowDrive, drive, mShadowSize);
  } else {
    mShadowDriveNull = true;
  }
  return true;
}

void ShellNetOnDemand::initDebug()
{
  // Only need to do this once
  if (mDebug)
    return;

  // Use the CarbonDBNode to get a good name in the case of visibility nets
  UtString hier_name;
  CarbonModel* model = mMgr->getModel();
  const CarbonDatabaseNode* dbNode = model->getDBNodeFromNet(this);
  dbNode->composeFullName(&hier_name);
  mDebug = new OnDemandDebugInfo(eCarbonOnDemandDebugAPI, hier_name);
}
