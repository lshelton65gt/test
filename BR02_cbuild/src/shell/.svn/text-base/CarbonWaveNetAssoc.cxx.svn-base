// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// author: Mark Seneski

#include "util/CarbonPlatform.h"
#include "shell/CarbonWaveNetAssoc.h"
#include "shell/WaveDump.h"
#include "shell/ShellData.h"
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STSymbolTable.h"
#include "util/UtConv.h"
#include "shell/CarbonVector.h"
#include "CarbonDatabase.h"
#include "CarbonDatabaseNode.h"
#include "CarbonExamineScheduler.h"

CarbonWaveNetAssoc::CarbonWaveNetAssoc() 
{
  mHandle = NULL;
  mCarbonNet = NULL;
  mIsAliased = false;
  mHandleType = eInvalid;
  mNetFlags = eNoneNet;
}

CarbonWaveNetAssoc::~CarbonWaveNetAssoc() 
{
}

void CarbonWaveNetAssoc::setHandle(WaveHandle* handle)
{
  mHandle = handle;

  if (handle != NULL) {
    // Determine the type of net we have.  This affects how the net's
    // value is updated.
    if (mHandle->isHardwareNet() || mHandle->isCharacter() || mHandle->isEnumeration()) {
      mHandleType = eNormal;
    } else if (mHandle->isInteger()) {
      mHandleType = eInteger;
    } else if (mHandle->isReal()) {
      mHandleType = eReal;
    } else if (mHandle->isTime()) {
      mHandleType = eTime;
    }
  } else {
    mHandleType = eInvalid;
  }
}

void CarbonWaveNetAssoc::setNet(ShellNet* net)
{
  mCarbonNet = net;

  // Get the net flags as well
  const STSymbolTableNode* node = mCarbonNet->getNameAsLeaf();
  const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);      
  mNetFlags = bomdata->getNetFlags();
}

bool CarbonWaveNetAssoc::writeIfNotEq(ShellNet::Storage shadowVal)
{
  const STSymbolTableNode* node = mCarbonNet->getNameAsLeaf();
  const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);      
  NetFlags netflags = bomdata->getNetFlags();
  
  char* valStr = mHandle->getValue();
  UInt32 size = mHandle->getSize() + 1;
  bool isChanged = 
    mCarbonNet->writeIfNotEq(valStr, size, 
                             &shadowVal, netflags) == ShellNet::eChanged;
  return isChanged;
}

void CarbonWaveNetAssoc::maybeUpdateValue(NetWriteFlags flags, WaveDump* waveFile, bool isChanged, bool doFormatIfChanged)
{
  bool isForcedWrite = (flags & eValWriteVal) != 0;

  if (isForcedWrite || isChanged) {
    switch (mHandleType) {
    case eNormal:
      if (doFormatIfChanged || isForcedWrite)
        mCarbonNet->format(mHandle->getValue(), mHandle->getSize() + 1, 
                           eCarbonBin, mNetFlags, NULL);
      waveFile->addChanged(mHandle);
      break;
    case eInteger:
    {
      UInt32 val = 0;
      mCarbonNet->examine(&val, NULL, ShellNet::eIDrive, NULL);
      waveFile->addChangedWithInt32(mHandle, val);
    }
    break;
    case eReal:
    {
      CarbonReal val = 0.0;
      mCarbonNet->examine( &val, NULL, ShellNet::eIDrive, NULL );
      waveFile->addChangedWithDbl(mHandle, val );
    }
    break;
    case eTime:
    {
      UInt32 val[2] = {0, 0};
      UInt64 bVal;
      
      mCarbonNet->examine(val, NULL, ShellNet::eIDrive, NULL);
      CarbonValRW::cpSrcToDest(&bVal, val, 2);
      waveFile->addChangedWithInt64(mHandle, bVal);
    }
    break;
    case eInvalid:
      // This code used to skip handles that didn't fall into one of
      // the above types.  I thought that meant these invalid handles
      // didn't exist, but that's not true.  Continue to skip them for
      // now, but we should really think about finding out why they
      // make it this far.
      break;
    }
  }
}


CarbonWaveNetAssocGroup::CarbonWaveNetAssocGroup(const void* storage, UInt32 numAssocs)
  : mStorage(storage), mChangedBitsIter(NULL)
{
  mNetAssocs.resize(numAssocs);
}

CarbonWaveNetAssocGroup::~CarbonWaveNetAssocGroup()
{
  // We own this pointer
  delete mChangedBitsIter;
}

void CarbonWaveNetAssocGroup::addAssoc(CarbonWaveNetAssoc* assoc, UInt32 index)
{
  INFO_ASSERT(index < getNumAssocs(), "index out of range");
  INFO_ASSERT(mNetAssocs[index] == NULL, "association already exists");
  mNetAssocs[index] = assoc;
}

void CarbonWaveNetAssocGroup::maybeUpdateValue(CarbonWaveNetAssoc::NetWriteFlags flags, WaveDump* waveFile, bool isChanged, bool doFormatIfChanged)
{
  bool isForcedWrite = (flags & CarbonWaveNetAssoc::eValWriteVal) != 0;
  if (isForcedWrite) {
    // Don't look at changed bits - just update all the assocs in the group.
    for (UInt32 i = 0; i < getNumAssocs(); ++i) {
      mNetAssocs[i]->maybeUpdateValue(flags, waveFile, isChanged, doFormatIfChanged);
    }
  } else if (isChanged) {
    // For all bits that changed, update the corresponding assoc
    SInt32 changedBit;
    while ((changedBit = mChangedBitsIter->next()) != -1) {
      mNetAssocs[changedBit]->maybeUpdateValue(flags, waveFile, isChanged, doFormatIfChanged);
    }
  }
}

void CarbonWaveNetAssocGroup::putVCAndIndex(CarbonValueChangeBase* vc, UInt32 index)
{
  // We can allocate the iterator immediately.  It has a reference to
  // the changed bits location in the VC, so it will always get the
  // right values.
  mChangedBitsIter = vc->createBitIter(index);
}

ShellData* CarbonWaveNetAssoc::sGetOrCreateShellData(STSymbolTableNode* node)
{
  ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);
  ShellGlobal::lockMutex();
  ShellData* shellData = bomdata->getShellData();
  
  if (! shellData)
  {
    shellData = new ShellData;
    bomdata->setShellData(shellData);
  }
  ShellGlobal::unlockMutex();
  return shellData;
}

ShellData* CarbonWaveNetAssoc::sGetOrCreateShellData(CarbonDatabaseNode* node)
{
  ShellData* shellData = node->getShellData();
  
  if (! shellData)
  {
    shellData = new ShellData;
    node->setShellData(shellData);
  }
  return shellData;
}
