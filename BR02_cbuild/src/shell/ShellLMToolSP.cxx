// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/AtomicCache.h"
#include "util/ArgProc.h"
#include "util/UtLicense.h"
#include "util/UtLicenseMsg.h"
#include "util/ZstreamZip.h"
#include "util/ShellMsgContext.h"
#include "util/UtCustomerDB.h"
#include "util/UtIOStream.h"
#include "util/MemManager.h"
#include "util/UtSimpleCrypt.h"
#include "util/UtStringArray.h"
#include "util/OSWrapper.h"

#include "shell/CarbonDBRead.h"
#include "exprsynth/ExprFactory.h"
#include "symtab/STSymbolTable.h"
#include "iodb/ScheduleFactory.h"
#include "iodb/IODBRuntime.h"
#include <time.h>

extern const char* gCarbonVersion();

struct LicCB : public UtLicense::MsgCB
{
  CARBONMEM_OVERRIDES

  LicCB(MsgContext* msgContext) : mMsgContext(msgContext)
  {}

  virtual ~LicCB() {}

  virtual void waitingForLicense(const char*, const char*) {}
  virtual void queuedLicenseObtained(const char*, const char*) {}
  virtual void requeueLicense(const char*, const char*) {}
  virtual void relinquishLicense(const char*, const char*) {}
  virtual void exitNow(const char* reason)
  {
    mMsgContext->SHLLicenseServerFail(reason);
  }
  
  MsgContext* mMsgContext;
};

class SignatureRewriter : public ZistreamZip::EntryRewriter
{
public:
  CARBONMEM_OVERRIDES

  SignatureRewriter(const IODBRuntime* iodb) 
    : mCustDB(iodb->getCustomerDB())
  {
    (void) iodb->getEntryName(&mCustDBEntry, IODB::eEntryCustDB);
  }
  
  virtual ~SignatureRewriter() {}
  
  virtual ZistreamZip::EntryAction 
  whichAction(const UtString& entryName) const
  {
    ZistreamZip::EntryAction action = ZistreamZip::eEntryUnmodified;
    if (entryName.compare(mCustDBEntry) == 0)
      action = ZistreamZip::eEntryModified;
    return action;
  }
  
  virtual bool modifyDBEntry(ZistreamEntry* /*readEntry*/, 
                             const UtString& entryName, 
                             ZostreamDB* outDB)
  {
    bool isGood = true;
    if (entryName.compare(mCustDBEntry) == 0)
      isGood = mCustDB->dbWrite(*outDB);
    return isGood;
  }

  virtual void msgBackingupFile(const char* origFilename, 
                                const char* backupFilename) const
  {
    UtIO::cout() << "Backing up " << origFilename << " to " 
                 << backupFilename << "..." << UtIO::endl;
  }

  virtual void msgWritingTmpFile(const char* tmpFile) const
  {
    UtIO::cout() << "Writing database to " << tmpFile << "..."
                 << UtIO::endl;
  }

  virtual void msgMovingTmpToOrig(const char* tmpFile,
                                  const char* origFile) const
  {
    UtIO::cout() << "Moving " << tmpFile << " to " << origFile
                 << "..." << UtIO::endl;
  }


private:
  const UtCustomerDB* mCustDB;
  UtString mCustDBEntry;
};

static const char* sGetTypeName(const UtCustomerDB* custDB)
{
  switch(custDB->getType())
  {
  case UtCustomerDB::eSpeedCompiler:
    return "(DesignPlayer)";
    break;
  case UtCustomerDB::eVSPCompiler:
    return "(VHM)";
    break;
  case UtCustomerDB::eInvalid:
    return "(Invalid)";
    break;
  }
  return NULL;
}

static void sGetPosixTm(SInt64 curTimeBomb, struct tm* posixTm)
{
  time_t bomb = curTimeBomb;
  localtime_r(&bomb, posixTm);
}

class ParamClosure
{
public:
  CARBONMEM_OVERRIDES

  ParamClosure(const char* vhmName, MsgContext* msgContext)
    : mVhmName(vhmName), mMsgContext(msgContext),
      mNewSig(NULL), mBombTime(0)
  {}

  void putNewSig(const char* newSig)
  {
    mNewSig = newSig;
  }
  
  void putTimeBomb(SInt64 timeVal, const char* message)
  {
    mBombTime = timeVal;
    mTimeBombMessage.assign(message);
  }
  
  const char* getVhmName() const { return mVhmName; }
  MsgContext* getMsgContext() const { return mMsgContext; }
  const char* getNewSig() const { return mNewSig; }

  bool calcTime(const char* timeString)
  {
    struct tm posixTm;
    
    /* 
       Solaris and Linux's strptime do not do the same thing when you
       set tm_isdst to -1. On Linux, that means treat the given string
       as gospel and set the time from mktime to be that exact
       time. On Solaris, it means, "you don't know, so I will set it
       to 0 for you." 

       To deal with this, we set tm_isdst to -1. If strptime changes
       it to 0 and then mktime changes it to 1 we need to subtract an
       hour. We'll check the opposite case of that (strptime changes
       to 1 and mktime changes it to 0, but I'm fairly positive that
       won't happen.
    */
    
    posixTm.tm_isdst = -1;

    // Now call OSParseTime with the supplied string.  OSParseTime returns
    // false if the string couldn't be parsed.
    if (!OSParseTime(timeString, &posixTm))
    {
      UtIO::cout() << "Invalid timestring: '" << timeString << "'. Required format: yyyy-mm-dd 24h:min:sec" << UtIO::endl;
      return false;
    }
    int strpIsDst = posixTm.tm_isdst;
    
    // Now, we have a tm structure with the requested time. We now
    // need to convert that to a time_t.
    mBombTime = mktime(&posixTm);
    INFO_ASSERT(mBombTime > 0, "System call 'mktime' failed.");
    
    if (posixTm.tm_isdst != strpIsDst)
    {
      bool modified = true;
      if ((strpIsDst == 0) && (posixTm.tm_isdst == 1))
        // subtract an hour from the mBombTime
        mBombTime -= (60/* sec/min */ * 60/*min*/);
      else if ((strpIsDst == 1) && (posixTm.tm_isdst == 0))
        // I don't think this can happen, but it doesn't hurt.
        // Add an hour to mBombTime
        mBombTime += (60/* sec/min */ * 60/*min*/);
      else
        // don't do anything if strpIsDst is -1. 
        modified = false;

      if (modified)
        // Reset posixTm to the updated time.
        sGetPosixTm(mBombTime, &posixTm);
    }
    

    
    // ok now create a more specific printable string that can
    // accompany the database modification status output.
    char buf[256];
    size_t stat = strftime(buf, sizeof(buf), "%A, %B %d, %Y at %H:%M:%S", &posixTm);
    INFO_ASSERT(stat > 0, "System call 'strftime' failed");
    mTimeBombMessage.clear();
    mTimeBombMessage << "Expires on "<< buf;
    return true;
  }
  
  SInt64 getBombTime() const { return mBombTime; }

  const UtString& getTimeBombMessage() const { return mTimeBombMessage; }
  
private:
  const char* mVhmName;
  MsgContext* mMsgContext;
  const char* mNewSig;
  
  // The timebomb that the user requests in time_t format. Note that
  // time_t is a signed 32 bit type on current platforms, but I have
  // to assume that it will become a 64 bit type at some point.
  SInt64 mBombTime;
  // Time-bomb message for database modification status 
  UtString mTimeBombMessage;

};

class DBWrapper
{
public:
  CARBONMEM_OVERRIDES
  
  DBWrapper(const ParamClosure& parameters, CarbonDBType type) :
    mDBType(type),
    mMsgContext(parameters.getMsgContext()),
    mSymTab(&mBom, &mAtomicCache), 
    mIODB(&mAtomicCache, &mSymTab, mMsgContext, 
          &mScheduleFactory, &mExprFactory, NULL /* no source locator factory needed*/),
    mFileName(parameters.getVhmName()),
    mParameters(&parameters)
  {
    mBom.putScheduleFactory(&mScheduleFactory);
    
    // Check if 'lib' needs to be prepended to the filename
    if ((mFileName.size() < 3) || (strncmp(mFileName.c_str(), "lib", 3) != 0))
    {
      mFileName.clear();
      mFileName << "lib" << parameters.getVhmName();
    }
    
    switch (mDBType)
    {
    case eCarbonFullDB:
      mFileName << IODB::scFullDBExt;
      break;
    case eCarbonIODB:
      mFileName << IODB::scIODBExt;
      break;
    case eCarbonGuiDB:
      mFileName << IODB::scGuiDBExt;
      break;
    }
  }

  IODBRuntime* getDB() { return &mIODB; }

  bool open()
  {
    return mIODB.readDB(mFileName.c_str(), mDBType);
  }
  
  void printSignature() const
  {
    const UtCustomerDB* custDB = mIODB.getCustomerDB();
    const UtCustomerDB::Signature* devSig = custDB->getDeveloperSignature();
    
    UtIO::cout() << UtIO::endl << sGetTypeName(custDB) << " " << mFileName << ":" << UtIO::endl;
    UtIO::cout() << "\tSignature: " << *(devSig->getStr()) << UtIO::endl;
    SInt64 curTimeBomb = devSig->getTimeBomb();
    if (curTimeBomb > 0)
    {

      struct tm posixTm;
      sGetPosixTm(curTimeBomb, &posixTm);
      char buf[256];
      size_t stat = strftime(buf, sizeof(buf), "%A, %B %d, %Y at %H:%M:%S", &posixTm);
      INFO_ASSERT(stat > 0, "System call 'strftime' failed");
      char timeBomb[10];
      {
        int timeBombCrypt[9];
        SCRAMBLECODE9("Time Bomb", timeBombCrypt);
        timeBomb[9] = '\0';
        SCRAMBLECODE9(timeBombCrypt, timeBomb);
      }

      UtIO::cout() << "\t" << timeBomb << ": Expires on " << buf << "." << UtIO::endl;
    }
  }

  bool modify()
  {
    bool isGood = true;
    bool isModified = false;
    
    UtCustomerDB* custDB = mIODB.getCustomerDB();
    UtCustomerDB::Signature* devSig = custDB->getDeveloperSignature();

    UtStringArray messages;
    if (custDB->getType() == UtCustomerDB::eVSPCompiler)
    {
      UtString outputBuf;
      SInt64 curTimeBomb = devSig->getTimeBomb();
      SInt64 reqTimeBomb = mParameters->getBombTime();
      if (curTimeBomb != reqTimeBomb)
      {
        if (curTimeBomb != 0) 
          outputBuf << "Updated ";
        else
          outputBuf << "Installed ";

        char timeBomb[10];
        {
          int timeBombCrypt[9];
          SCRAMBLECODE9("time bomb", timeBombCrypt);
          timeBomb[9] = '\0';
          SCRAMBLECODE9(timeBombCrypt, timeBomb);
        }

        outputBuf << timeBomb <<". " << mParameters->getTimeBombMessage();
        messages.push_back(outputBuf);
        
        devSig->putTimeBomb(reqTimeBomb);
        isModified = true;
      }
      else if (curTimeBomb != 0)
      {
        char timeBomb[10];
        {
          int timeBombCrypt[9];
          SCRAMBLECODE9("Time bomb", timeBombCrypt);
          timeBomb[9] = '\0';
          SCRAMBLECODE9(timeBombCrypt, timeBomb);
        }
        
        outputBuf << timeBomb << " was not changed. " << mParameters->getTimeBombMessage();
        messages.push_back(outputBuf);
      }
    }
    else if (mParameters->getBombTime() != 0)
    {
      char timeBomb[10];
      {
        int timeBombCrypt[9];
        SCRAMBLECODE9("time-bomb", timeBombCrypt);
        timeBomb[9] = '\0';
        SCRAMBLECODE9(timeBombCrypt, timeBomb);
      }
      UtIO::cout() << sGetTypeName(custDB) << " " << mFileName << ": The " << timeBomb << " feature is not available on DesignPlayers." << UtIO::endl;
      return false;
    }
    
    const char* newSig = mParameters->getNewSig();
    if (newSig)
    {
      UtString newSigStr(newSig);
      if (custDB->getType() == UtCustomerDB::eVSPCompiler)
      {
        // Does it already have SIG= ?
        if ((newSigStr.size() < 4) 
            || (strncmp(newSigStr.c_str(), "SIG=", 4) != 0))
        {
          newSigStr.clear();
          newSigStr << "SIG=" << newSig;
        }
      }
      else
        // DesignPlayers require some UtCustomerDB initialization, which
        // can be done simply by calling putType()
        custDB->putType(UtCustomerDB::eSpeedCompiler);
      
      // This really cannot happen. I don't even know why I'm
      // asserting. I guess I'm just paranoid.
      INFO_ASSERT(devSig, "Invalid database - does not contain a signature");
      const UtString* devSigStr = devSig->getStr();
      // copy the string so we can report the change
      UtString origSig(*devSigStr);
      
      if (devSigStr->compare(newSigStr) != 0)
      {
        UtString reason;
        if (custDB->putDeveloperSignature(newSigStr.c_str(), &reason) == NULL)
        {
          isGood = false;
          mMsgContext->SHLSignatureChangeFailed(sGetTypeName(custDB), 
                                                newSigStr.c_str(), 
                                                reason.c_str());
        }
        else
        {
          isModified = true;
          UtString outputBuf;
          outputBuf << "Changed signature from '" << origSig << "' to '" << newSigStr << "'";
          messages.push_back(outputBuf);
        }
        
      }
      else
      {
        UtString outputBuf;
        outputBuf << mFileName 
                  << " already contains the signature '" << newSigStr << "'";
        messages.push_back(outputBuf);
      }
    }

    if (isGood && isModified)
    {
      // rewrite the database.
      
      // First open the zip archive. This takes no significant
      // time because we are only going to read the header
      ZISTREAMZIP(dbReadZip, mFileName.c_str());
      if (! dbReadZip)
      {
        mMsgContext->SHLDBFileOpenFail(mFileName.c_str(), dbReadZip.getFileError());
        isGood = false;
      }
      else
      {
        // It is open and all is good. Instantiate the rewriter and
        // begin.
        SignatureRewriter rewriter(&mIODB);
        isGood = dbReadZip.rewrite(&rewriter);
      }
    }

    if (! messages.empty())
    {
      // print out what we just did.
      UtIO::cout() << UtIO::endl << sGetTypeName(custDB) << " " << mFileName << ":" << UtIO::endl;
      for (UtStringArray::UnsortedCLoop p = messages.loopCUnsorted(); 
           ! p.atEnd(); ++p)
        UtIO::cout() << "\t" << *p << UtIO::endl;
    }

    return isGood;
  }
  
private:
  CarbonDBType mDBType;
  MsgContext* mMsgContext;
  AtomicCache mAtomicCache;
  ESFactory mExprFactory;
  SCHScheduleFactory mScheduleFactory;
  ShellSymTabBOM mBom;
  STSymbolTable mSymTab;
  IODBRuntime mIODB;
  UtString mFileName;
  const ParamClosure* mParameters;
};

static int sListSignature(const ParamClosure& parameters)
{
  int status = 0;
  // List the io.db sig first
  {
    DBWrapper ioWrap(parameters, eCarbonIODB);
    if (! ioWrap.open())
      status = 1;
    else
      ioWrap.printSignature();
  }

  // List the symtab.db last
  {
    DBWrapper fullWrap(parameters, eCarbonFullDB);
    if (! fullWrap.open())
      status = 1;
    else
      fullWrap.printSignature();
  }
  return status;
}

static int sModifyDatabase(const ParamClosure& parameters)
{
  int status = 0;
  // Modify the io.db sig first
  {
    DBWrapper ioWrap(parameters, eCarbonIODB);
    if (! ioWrap.open() || ! ioWrap.modify())
      status = 1;
  }

  UtIO::cout() << UtIO::endl;

  // Modify the symtab.db last
  {
    DBWrapper fullWrap(parameters, eCarbonFullDB);
    if (! fullWrap.open() || ! fullWrap.modify())
      status = 1;
  }
  return status;
}


static void sPrintUsage(ArgProc& args)
{
  UtString usage;
  args.getUsageVerbose(&usage);
  UtIO::cout() << usage << UtIO::endl;
}

static void sPrintBanner(const char* short_name, const char* long_name)
{
  UtString shortStr(short_name);
  shortStr.uppercase();
  UtIO::cout() << 
    "        " << shortStr << ", " << long_name << UtIO::endl <<
    "            " << gCarbonVersion() << UtIO::endl << UtIO::endl <<
    CarbonMem::CarbonGetRightsReservedLine() <<
    UtIO::endl <<
    UtIO::endl <<
    CarbonMem::CarbonGetCopyrightDateLine() <<
    UtIO::endl <<
    CarbonMem::CarbonGetTechSupportLine() <<
    UtIO::endl;
}

int main(int argc, char** argv)
{
  ArgProc args;

  MsgStreamIO errStream(stderr, true);
  MsgContext msgContext;
  msgContext.addReportStream(&errStream);
  LicCB licCB(&msgContext);
  UtLicense license(&licCB);

  int numOptions;
  const char* longName = "Carbon Service Provider License Manager Tool";
  UtString executable;
  UtString errmsg;
  {
    UtString path;
    OSParseFileName(argv[0], &path, &executable);
    
    args.setDescription(longName, executable.c_str(),
                        "This is used by Carbon service providers to change the developer's signature to the customer's given signature in Carbon Models. The Carbon Model name supplied by the caller is the same string as the string passed into cbuild for the -o option WITHOUT the file extension. For example, if -o libflop.a was specified on the cbuild command line, the Carbon Model name is 'libflop'. 'flop' is also permissible. The 'lib' prefix will automatically be prepended. DesignPlayer manipulation is for internal use only. This program requires that a Carbon compiler license exist and have the same vendor signature as the clmtoolsp license.");
    
    UtString buf;
    buf << executable << " -help";
    args.addSynopsis(buf);
    buf.clear();
    buf << executable << " -list <VHM | DesignPlayer>";
    args.addSynopsis(buf);
    buf.clear();
    buf << executable << " -rep <new_signature> <VHM | DesignPlayer>";
    args.addSynopsis(buf);
  }

  // public switches
  const char* cHelp = "-help";
  const char* cList = "-list";
  const char* cRep = "-rep";

  // hidden switches
  // switch: -timebomb, 9 chars
  char timeBomb[10];
  {
    int timeBombCrypt[9];
    SCRAMBLECODE9("-timebomb", timeBombCrypt);
    timeBomb[9] = '\0';
    SCRAMBLECODE9(timeBombCrypt, timeBomb);
  }

  const char* sect1 = "Modes";
  

  args.createSection(sect1);
  args.addBool(cHelp, "Print this usage message and exit.", false, 1);
  args.addToSection(sect1, cHelp);
  args.addBool(cList, "List the Developer signatures contained in the VHM or DesignPlayer.", false, 1);
  args.addToSection(sect1, cList);
  args.addString(cRep, "Replace the Developer signature in the named VHM or DesignPlayer with the specified signature. For VHMs, the signature will automatically have SIG= prepended to it, if necessary.", NULL, true, false, 1);
  args.addToSection(sect1, cRep);

  // do not add this message about the timebomb license to a section
  {
    // this bit of code is used to somewhat obscure a help string so that it is not visible using the 'strings' command
    UtString tb_help_string;
    {
      char temp_help_string[32];
      int tb_help_string_C[8][31];
      SCRAMBLECODE31("Convert to timebomb license. Fo",tb_help_string_C[0]);
      SCRAMBLECODE31("rmat yyyy-mm-dd 24h:min:sec. Al",tb_help_string_C[1]);
      SCRAMBLECODE31("l fields must be supplied. Note",tb_help_string_C[2]);
      SCRAMBLECODE31(" the time is 24 hour format (0-",tb_help_string_C[3]);
      SCRAMBLECODE31("23 hours). If you want to remov",tb_help_string_C[4]);
      SCRAMBLECODE31("e the timebomb. The keyword 're",tb_help_string_C[5]);
      SCRAMBLECODE31("move' is accepted as an argumen",tb_help_string_C[6]);
      SCRAMBLECODE2("t.",tb_help_string_C[7]);
      for (int i = 0; i <= 7; i++)
      {
        SCRAMBLECODE31(tb_help_string_C[i], temp_help_string); temp_help_string[31] = '\0';
        if (i == 7) temp_help_string[2] = '\0'; // special case for last part of string
        tb_help_string << temp_help_string;
      }
    }
    // note in the following the switch and the help string are hidden from the "strings" command
    args.addString(timeBomb, tb_help_string.c_str(), NULL, true, false, 1);
  }
  // Check if we have a timebomb license. If we do, allow it on the
  // command line
  bool hasTimeBombLicense = false;
  {
    UtString timeBombLicenseFail;
    hasTimeBombLicense = license.checkout(UtLicense::eLMToolSPTimed, &timeBombLicenseFail);
    if (hasTimeBombLicense)
      args.accessHiddenOptions();
  }
  
  if ((args.parseCommandLine(&argc, argv, &numOptions, &errmsg) ==
       ArgProc::eParseError))
  {
    fprintf(stdout, "%s\n", errmsg.c_str());
    return 1;
  }

  // if help is asked for. Print it out and exit. No need to checkout
  // a license for that.
  if ((numOptions == 0) || args.getBoolValue(cHelp))
  {
    sPrintUsage(args);
    return 0;
  }

  // If the command line is bogus exit with a bad status
  if (argc > 2)
  {
    sPrintUsage(args);
    UtIO::cout() << UtIO::endl << "Too many arguments for mode." << UtIO::endl;
    return 1;
  }
  
  if (argc != 2)
  {
    sPrintUsage(args);
    UtIO::cout() << UtIO::endl << "VHM or DesignPlayer name must be specified." << UtIO::endl;
    return 1;
  }

  // We have a valid command line. Checkout a license
  
  {
    UtString reason;
    UtStringArray vendorStrings;
    // Checking out a lmtool license is based on the crbn_vsp_comp
    // license signature.
    if (! license.findVendorStrings(UtLicense::eVSPCompiler, &vendorStrings, &reason))
    {
      reason << " CODE7";
      msgContext.SHLLicenseCheckFail(reason.c_str());
      return 1;
    }
    
    if (vendorStrings.empty())
    {
      reason << "No vendor strings found for crbn_vsp_comp.";
      msgContext.SHLLicenseCheckFail(reason.c_str());
      return 1;
    }

    if (vendorStrings.size() > 1)
    {
      reason << "More than 1 vendor string found in license file: ";
      bool isFirst = true;
      for (UtStringArray::UnsortedCLoop p = vendorStrings.loopCUnsorted(); !p.atEnd(); ++p)
      {
        if (!isFirst) {
          reason << ", ";
        }
        else {
          isFirst = false;
        }
        reason << *p;
      }
      msgContext.SHLLicenseCheckFail(reason.c_str());
      return 1;
    }
    
    const char* vendorString = vendorStrings.back();
    bool filterRejected = false;
    // We have a signature, checkout a lmtoolsp with that signature.
    if (! license.checkoutFiltered(UtLicense::eLMToolSP, vendorString, &reason, &filterRejected))
    {
      if (filterRejected)
      {
        UtString buf;
        buf << "No VENDOR_STRING match for '" << vendorString << "' - ";
        buf << reason;
        msgContext.SHLLicenseCheckFail(buf.c_str());
      }
      else {
        reason << " CODE8";
        msgContext.SHLLicenseCheckFail(reason.c_str());
      }
      
      return 1;
    }
  }
  
  // we have a license. The remaining argument on the command line is
  // the vhm name.

  // print the banner
  sPrintBanner(executable.c_str(), longName);

  ParamClosure parameters(argv[1], &msgContext);
  
  // check for basic options
  if (args.isParsed(timeBomb) == ArgProc::eParsed)
  {
    const char* timeBombStr = args.getStrValue(timeBomb);
    if (strcmp(timeBombStr, "remove") == 0)
      parameters.putTimeBomb(0, "*REMOVED*");
    else if (! parameters.calcTime(timeBombStr))
      return 1;
  }
  if (args.isParsed(cRep) == ArgProc::eParsed)
    parameters.putNewSig(args.getStrValue(cRep));
  
  
  // List operation outranks modify operations
  int status = 0;
  if (args.getBoolValue(cList))
    status = sListSignature(parameters);
  else
    status = sModifyDatabase(parameters);

  return status;
}
