// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "shell/ShellData.h"
#include "util/UtArray.h"
#include "util/CarbonAssert.h"

#include <algorithm>

class ShellData::Helper
{
public:
  CARBONMEM_OVERRIDES
  
  UtArray<ShellNet*> mNets;
  UtArray<CarbonWaveNetAssoc*> mAssocs;
  
  inline void maybeResize(UInt32 index)
  {
    // mNets and mAssocs are always the same size
    size_t size = mNets.size();
    if (index >= size)
    {
      ShellNet* dummyNet = NULL;
      CarbonWaveNetAssoc* dummyAssoc = NULL;
      size_t num = index - size + 1;
      std::fill_n(std::back_inserter(mNets), num, dummyNet);
      std::fill_n(std::back_inserter(mAssocs), num, dummyAssoc);
    }
  }
  
  bool isKnownId(UInt32 id) const
  {
    return (id < mNets.size());
  }
};

ShellData::ShellData()
{
  mHelper = new Helper;
}

ShellData::~ShellData()
{
  //  UtIO::cerr() << "Delete shelldata: " << this << UtIO::endl;
  delete mHelper;
  mHelper = NULL; // helps for mem leak debugging
}

const ShellNet* ShellData::getCarbonNet(UInt32 id) const
{
  const ShellNet* ret = NULL;
  if (mHelper->isKnownId(id))
    ret = mHelper->mNets[id];
  return ret;
}

CarbonWaveNetAssoc* 
ShellData::getNetAssoc(UInt32 id)
{
  const ShellData* me = const_cast<const ShellData*>(this);
  return const_cast<CarbonWaveNetAssoc*>(me->getNetAssoc(id));
}
  
const CarbonWaveNetAssoc* 
ShellData::getNetAssoc(UInt32 id) const
{
  const CarbonWaveNetAssoc* ret = NULL;
  if (mHelper->isKnownId(id))
    ret = mHelper->mAssocs[id];
  return ret;
}

void ShellData::putCarbonNet(ShellNet* net, UInt32 id)
{
  mHelper->maybeResize(id);
  mHelper->mNets[id] = net;
}
  
void ShellData::putNetAssoc(CarbonWaveNetAssoc* netAssoc, 
                            UInt32 id)
{
  mHelper->maybeResize(id);
  mHelper->mAssocs[id] = netAssoc;
}
