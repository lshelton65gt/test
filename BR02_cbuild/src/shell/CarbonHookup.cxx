// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/CarbonHookup.h"
#include "shell/CarbonDBRead.h"
#include "shell/ShellData.h"
#include "shell/ShellNetConstant.h"
#include "shell/CarbonModel.h"
#include "shell/CarbonVectorBase.h"
#include "shell/CarbonExprNet.h"
#include "shell/CarbonNetIdent.h"

#include "util/ShellMsgContext.h"
#include "symtab/STAliasedLeafNode.h"
#include "util/DynBitVector.h"
#include "iodb/IODBRuntime.h"
#include "iodb/IODBTypes.h"
#include "util/UtHashMap.h"
#include "util/ConstantRange.h"
#include "exprsynth/ExprFactory.h"
#include "exprsynth/ExprReduce.h"
#include "shell/CarbonWaveImp.h"
#include "CarbonExamineScheduler.h"
#include "util/UtConv.h"
#include "shell/CarbonForceNet.h"
#include "shell/carbon_model.h"
#include "shell/carbon_model_private.h"
#include "util/UtCheckpointStream.h"
#include "hdl/HdlFileSystem.h"

#ifdef INTERPRETER
#include "interp/Interp.h"
#endif

#define BIDI_VERBOSE 0

#if BIDI_VERBOSE 
#include "hdl/HdlVerilogPath.h"
#include <cstdio>
#endif

class CarbonHookup::ExprNetCreate : public CopyWalker
{

  class IdentToConstant : public CopyWalker
  {
  public: 
    CARBONMEM_OVERRIDES

    IdentToConstant(DynBitVector* val, ESFactory* factory) : 
      CopyWalker(factory), mVal(val), mNumIdents(0)
    {}

    CarbonExpr* transformIdent(CarbonIdent* ident) {
      ++mNumIdents;
      CE_ASSERT(mNumIdents < 2, ident);
      CE_ASSERT(ident->getBitSize() == mVal->size(), ident);
      return mFactory->createConst(*mVal, CarbonExpr::eUnsigned, mVal->size());
    }
    
  private:
    DynBitVector* mVal;
    UInt32 mNumIdents;
  };
  
public: 
  CARBONMEM_OVERRIDES
  ExprNetCreate(CarbonHookup* hookup, CarbonExpr* srcExpr, const STAliasedLeafNode* leaf, ESFactory* factory) :
    CopyWalker(factory),
    mHookup(hookup), mSrcExpr(srcExpr), mSrcLeaf(leaf),
    mIsInput(false), mIsForcible(false), mIsTristate(false), mHasConstantOverride(false)
  {
    mOverrideValue.resize(0);
    mOverrideXZ.resize(0);
    mOverrideControl.resize(0);
  }
  
  virtual ~ExprNetCreate() {}

  virtual void visitIdent(CarbonIdent* ident)
  {
    CarbonExpr* transExpr = ownExpr(ident);
    CE_ASSERT(transExpr, ident);
    pushResult(transExpr);
  }

  virtual void visitConst(CarbonConst* cConst) { 
    CarbonExpr* transExpr = ownExpr(cConst);
    CarbonConst* transConst = transExpr->castConst();
    CE_ASSERT(transConst, transExpr);
    pushResult(transConst);
  }

  virtual void visitConstXZ(CarbonConstXZ* cConstXZ) {
    CarbonExpr* transExpr = ownExpr(cConstXZ);
    CarbonConstXZ* transConst = transExpr->castConstXZ();
    CE_ASSERT(transConst, transExpr);
    pushResult(transConst);
  }

  bool hasInput() const {
    return mIsInput;
  }

  bool hasForcible() const {
    return mIsForcible;
  }

  bool hasTristate() const {
    return mIsTristate;
  }

  bool hasConstantOverride() const {
    return mHasConstantOverride;
  }
  
  void calcOverrideMask(DynBitVector* overrideMask)
  {
    UInt32 exprBitSize = mSrcExpr->getBitSize();
    if (mOverrideValue.size() == exprBitSize)
    {
      IODBRuntime::sComposeOverrideMask(overrideMask, 
                                        mOverrideValue.getUIntArray(),
                                        mOverrideXZ.getUIntArray(),
                                        mOverrideControl.getUIntArray(),
                                        mOverrideControl.numWords());
    }
    else
    {
      // can only deal with partsels and bitsels
      CE_ASSERT(mSrcExpr->castPartsel() || mSrcExpr->getType() == CarbonExpr::eBiBitSel, mSrcExpr);
      
      // Take the source expression and transform it replacing the
      // ident with the constant value.

      // Value transform
      IdentToConstant valXform(&mOverrideValue, mFactory);
      valXform.visitExpr(mSrcExpr);
      CarbonExpr* valOverrideExpr = valXform.getResult();
      
      // XZ transform
      IdentToConstant xzXform(&mOverrideXZ, mFactory);
      xzXform.visitExpr(mSrcExpr);
      CarbonExpr* xzOverrideExpr = xzXform.getResult();

      // Control transform
      IdentToConstant controlXform(&mOverrideControl, mFactory);
      controlXform.visitExpr(mSrcExpr);
      CarbonExpr* controlOverrideExpr = controlXform.getResult();

#define CONSTANTMASK_REDUCTION 1
#if CONSTANTMASK_REDUCTION
      // Reduce the constant expressions to simple constants
      ExprReduce reducer(mFactory, NULL);
      CarbonExpr* simpleValOver = reducer.reduce(valOverrideExpr);
      CarbonExpr* simpleXZOver = reducer.reduce(xzOverrideExpr);
      CarbonExpr* simpleControlOver = reducer.reduce(controlOverrideExpr);
      
      // assert the reduced expressions are constants, the size of the
      // source expression, and known (not xz
      CarbonConst* valConstExpr = simpleValOver->castConst();
      CarbonConst* xzConstExpr = simpleXZOver->castConst();
      CarbonConst* controlConstExpr = simpleControlOver->castConst();
      // constants
      CE_ASSERT(valConstExpr && xzConstExpr && controlConstExpr, mSrcExpr);
      // size
      CE_ASSERT(valConstExpr->getBitSize() == exprBitSize, mSrcExpr);
      CE_ASSERT(xzConstExpr->getBitSize() == exprBitSize, mSrcExpr);
      CE_ASSERT(controlConstExpr->getBitSize() == exprBitSize, mSrcExpr);
      // known
      CE_ASSERT(valConstExpr->castConstXZ() == NULL, mSrcExpr);
      CE_ASSERT(xzConstExpr->castConstXZ() == NULL, mSrcExpr);
      CE_ASSERT(controlConstExpr->castConstXZ() == NULL, mSrcExpr);


      // Get the values
      DynBitVector val;
      DynBitVector xz; 
      DynBitVector control;

      valConstExpr->getValue(&val);
      xzConstExpr->getValue(&xz);
      controlConstExpr->getValue(&control);

      // Make sure the size of the constants are correct
      CE_ASSERT(val.size() == exprBitSize, mSrcExpr);
      CE_ASSERT(xz.size() == exprBitSize, mSrcExpr);
      CE_ASSERT(control.size() == exprBitSize, mSrcExpr);
      
      IODBRuntime::sComposeOverrideMask(overrideMask, 
                                        val.getUIntArray(),
                                        xz.getUIntArray(),
                                        control.getUIntArray(),
                                        control.numWords());
#else
      // alternative way of getting the value. Less efficient, though,
      // because of the evaluates. No reduction is needed for
      // evaluation.

      ExprEvalContext valContext;
      ExprEvalContext xzContext;
      ExprEvalContext controlContext;

      CE_ASSERT(valOverrideExpr->evaluate(&valContext) != CarbonExpr::eBadSign, mSrcExpr);
      CE_ASSERT(xzOverrideExpr->evaluate(&xzContext) != CarbonExpr::eBadSign, mSrcExpr);
      CE_ASSERT(controlOverrideExpr->evaluate(&controlContext) != CarbonExpr::eBadSign, mSrcExpr);

      DynBitVector* val = valContext.getValue();
      DynBitVector* xz = xzContext.getValue();
      DynBitVector* control = controlContext.getValue();

      CE_ASSERT(val->size() == exprBitSize, mSrcExpr);
      CE_ASSERT(xz->size() == exprBitSize, mSrcExpr);
      CE_ASSERT(control->size() == exprBitSize, mSrcExpr);
      
      IODBRuntime::sComposeOverrideMask(overrideMask, 
                                        val->getUIntArray(),
                                        xz->getUIntArray(),
                                        control->getUIntArray(),
                                        control->numWords());
#endif
    }
  }
  
private:
  CarbonHookup* mHookup;
  CarbonExpr* mSrcExpr;
  const STAliasedLeafNode* mSrcLeaf;
  bool mIsInput;
  bool mIsForcible;
  bool mIsTristate;
  bool mHasConstantOverride;
  DynBitVector mOverrideValue;
  DynBitVector mOverrideXZ;
  DynBitVector mOverrideControl;

  CarbonExpr* ownExpr(CarbonExpr* exprSrc) {
    bool isSigned = exprSrc->isSigned();
    UInt32 bitSize = exprSrc->getBitSize();
    CarbonIdent* ident = exprSrc->castIdent();
    CarbonConst* aConst = exprSrc->castConst();
    CarbonConstXZ* aConstXZ = exprSrc->castConstXZ();
    CarbonExpr* ret = NULL;
    if (ident)
    {
      DynBitVector useMask;
      STSymbolTableNode* node = ident->getNode(&useMask);

      STAliasedLeafNode* nodeLeaf = node->castLeaf();
      ST_ASSERT(nodeLeaf != mSrcLeaf, node);
      
      const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(nodeLeaf);
      
      ShellNet* net = mHookup->getCarbonNet(nodeLeaf);
      ST_ASSERT(net, nodeLeaf);
      if (! bomdata->isExpression())
        ST_ASSERT(net->castExprNet() == NULL, nodeLeaf);
      
      if (net->isInput())
        mIsInput = true;
      if (net->isForcible())
        mIsForcible = true;
      if (net->isTristate())
        mIsTristate = true;
      

      /*
        Set up constant override mask for proper formatting for
        carbonFormat. This may or may not be used once the expression
        walk is complete. If mHasConstantOverride is false at the end
        of the walk, the constant override is thrown away.
        
        NOTE: I'm not sure how this will work with vectorization or
        other expression types, none of which are yet supported
        anyway. This will have to be revisited.
      */
      const DynBitVector* overrideMask = mHookup->getDB()->getConstNetBitMask(nodeLeaf);
      if (overrideMask)
      {
        mHasConstantOverride = true;
        appendOverrideMask(overrideMask, bitSize);
      }
      else
        appendNullOverride(bitSize);

      
      CarbonNetIdent* transIdent = new CarbonNetIdent(net, mHookup->getCarbonModel(),
                                                      useMask, isSigned, bitSize);
      bool added;
      ret = mFactory->createIdent(transIdent, added);
      if (! added)
        delete transIdent;
    }
    else if (aConstXZ)
    {
      DynBitVector val;
      DynBitVector drv;
      aConstXZ->getValue(&val, &drv);
      ret = mFactory->createConstXZ(val, drv, aConstXZ->getSignInfo(), bitSize);
    }
    else if (aConst)
    {
      DynBitVector val;
      aConst->getValue(&val);
      ret = mFactory->createConst(val, aConst->getSignInfo(), bitSize);
    }
    
    return ret;
  }


  void appendOverrideMask(const DynBitVector* overrideMask, UInt32 bitSize)
  {
    const UInt32* valueArr;
    const UInt32* xzMaskArr;
    const UInt32* controlArr;

    UInt32 numWords = (bitSize + 31)/32;
    IODBRuntime::sExtractOverride(overrideMask->getUIntArray(), &valueArr, &xzMaskArr, &controlArr, numWords);

    DynBitVector value(bitSize, valueArr, numWords);
    DynBitVector xzMask(bitSize, xzMaskArr, numWords);
    DynBitVector control(bitSize, controlArr, numWords);

    sAppendOverrideBV(&mOverrideValue, value, bitSize);
    sAppendOverrideBV(&mOverrideXZ, xzMask, bitSize);
    sAppendOverrideBV(&mOverrideControl, control, bitSize);
  }

  void appendNullOverride(UInt32 bitSize)
  {
    DynBitVector value, xzMask, control;
    value.resize(bitSize);
    xzMask.resize(bitSize);
    control.resize(bitSize);
    
    sAppendOverrideBV(&mOverrideValue, value, bitSize);
    sAppendOverrideBV(&mOverrideXZ, xzMask, bitSize);
    sAppendOverrideBV(&mOverrideControl, control, bitSize);
  }
  
  static void sAppendOverrideBV(DynBitVector* mask, const DynBitVector& appendage, UInt32 bitSize)
  {
    UInt32 oldBitSize = mask->size();
    mask->resize(oldBitSize + bitSize);
    *mask <<= bitSize;
    *mask |= appendage;
  }
  
};

void CarbonHookup::updateWaveform()
{
  if (mWave)
    mWave->runWaveSchedule();
}

void CarbonHookup::sRunWaveSchedule(CarbonObjectID *descr)
{
  CarbonHookup* hookup = 
    static_cast <CarbonHookup*> (descr->mPrivate->getShellCallBack ()->mClientData);
  hookup->updateWaveform ();
}

void CarbonHookup::getNodeName(UtString* name, const STSymbolTableNode* symNode) const
{
  getDB()->composeName(name, symNode);
}

#ifdef INTERPRETER
CarbonStatus CarbonHookup::interpreterSchedule(CarbonModel* model, CarbonTime)
{
  CarbonHookup* hookup = model->getHookup();
  hookup->mInterpreter->run();
  return eCarbon_OK;
}
#endif

static void sDummyModeChange(CarbonObjectID *, void*,
                             CarbonVHMMode, CarbonVHMMode)
{
}

CarbonHookup::CarbonHookup (UInt32 id, CarbonObjectID *descr, CarbonModel *model) :
  mDescr (descr),
  mPrivate (descr->mPrivate),
  mWave(NULL), mDB(NULL), 
  mModel(model),
  mIndStoreMap(NULL),
#if 0
  mDestroyFn(NULL),
  mScheduleFn(NULL),
  mClkScheduleFn(NULL),
  mDataScheduleFn(NULL),
#endif
  mModeChangeFn(sDummyModeChange),
  mId(id),
  mExamineSched(NULL),
  mDummyChangeArrayRef(eCarbonChange),
  mChangeArray (descr->mChangeArray->mChanged),
  mChangeArraySize (descr->mChangeArray->mNumElems)
{
  INFO_ASSERT (descr->mVersion <= 1, "need to set debug_clientData");
  mPrivate->getShellCallBack ()->mClientData = this;
  // Init this to true so that the first call to carbonschedule will
  // overwrite carbon_id::mInit and so that replay will write/read the
  // initial values of the input/response.
  mSeenDeposit = true;
  // Pessimistic setting.
  mSeenDepositInitValue = true;

  mExprFactory = new ESFactory;
#ifdef INTERPRETER
  mInterpreter = NULL;
  if (getenv("CARBON_INTERPRET") != NULL) {
    mInterpreter = new Interpreter;
    UtString errmsg;
    if (!mInterpreter->loadCode("libdesign.code", &errmsg)) {
      UtIO::cerr() << errmsg << "\n";
      exit(1);
    }

    // The model data is a class derived from CarbonHookup, so the
    // model is immediately below this.
    mInterpreter->putModelData((UInt8*) mInstance, mChangeArray);
  }
#endif
}

void CarbonHookup::trashDesignSymbolTable ()
{
  ShellGlobal::lockMutex();
  STSymbolTable* symTab = mDB->getDesignSymbolTable();
  for (STSymbolTable::NodeLoop symIter = symTab->getNodeLoop(); ! symIter.atEnd(); ++symIter) {
    STSymbolTableNode* node = *symIter;
    if (node->castLeaf () != NULL) {
      deleteNet(node);
    }
  }
  ShellGlobal::unlockMutex();
}

/*virtual*/ void CarbonHookup::clearDB()
{
  if (mDB) {
    trashDesignSymbolTable ();
    // If model initialization didn't complete for some reason
    // (e.g. no license), the change array index pointer will not have
    // been acquired from the central DBManager.  In that case, don't
    // decrement the change index reference count.
    bool removeChangeIndexRef = (mIndStoreMap != NULL);
    ShellGlobal::gCarbonReleaseDB(&mDB, removeChangeIndexRef);
  }
}

CarbonHookup::~CarbonHookup()
{
  closeWave();

  if (mExamineSched)
    delete mExamineSched;

  delete mExprFactory;
  
  clearDB ();

#ifdef INTERPRETER
  if (mInterpreter != NULL)
    delete mInterpreter;
#endif
}

UInt32 CarbonHookup::getId() const
{
  return mId;
}

void CarbonHookup::putDestroyFn(::CarbonCoreDestroyFn fn) 
{
  mDestroyFn = fn;
}

void CarbonHookup::putScheduleFn(::CarbonCoreScheduleFn fn) 
{
#ifdef INTERPRETER
  if (mInterpreter != NULL)
    mScheduleFn = interpreterSchedule;
  else
#endif
    mScheduleFn = fn;
}

void CarbonHookup::putClkScheduleFn(::CarbonCoreScheduleFn fn) 
{
  mClkScheduleFn = fn;
}

void CarbonHookup::putDataScheduleFn(::CarbonCoreScheduleFn fn) 
{
  mDataScheduleFn = fn;
}

void
CarbonHookup::putInitializeFn(::CarbonCoreInitializeFn fn) 
{
  mInitializeFn = fn;
}

void
CarbonHookup::putSimulationOverFn(::CarbonSimulationOverFn fn) 
{
  mSimulationOverFn = fn;
}

void CarbonHookup::putDebugScheduleFn(::CarbonSimpleScheduleFn fn)
{
  mDebugScheduleFn = fn;
}

void CarbonHookup::putDepositComboScheduleFn(::CarbonSimpleScheduleFn fn)
{
  mDepositComboScheduleFn = fn;
}

void CarbonHookup::putAsyncScheduleFn(::CarbonCoreScheduleFn fn) 
{
  mAsyncScheduleFn = fn;
}

void CarbonHookup::putModeChangeFn(CarbonModeChangeCBFunc modeChangeFn)
{
  mModeChangeFn = modeChangeFn;
}

CarbonWaveImp* CarbonHookup::createWave() 
{
  if (! mWave)
    mWave = new CarbonWaveImp(this);
  return mWave;
}

CarbonWaveImp* CarbonHookup::getWave() 
{
  return mWave;
}

void CarbonHookup::closeWave()
{
  if (mWave) {
    mWave->flushPendingData();
    delete mWave;
    mWave = NULL;
  }
}

void CarbonHookup::putDB(IODBRuntime* db) 
{
  mDB = db;
  // Now that we have the db, set the mSeenDepositInitValue
  
  // If the user has set the following environment
  // variable then override mSeendDepositInitValue to always be true
  bool schedCompressOverride = getenv("CARBON_NO_SCHEDULE_COMPRESS") != NULL;
  if (mDB) // paranoia
  {
    if (! schedCompressOverride)
    {
      // The db object handles old databases properly, so we can rely on
      // this being pessimistic.
      /*
        If the port interface is generated, we cannot skip schedule
        calls if there is no deposit. This is because the port interface
        does not use the api. It uses pointers directly. The generated
        port interface is deprecated, so this won't be an issue in the
        future, but we need to have it work now.
      */
      mSeenDepositInitValue = db->isPortInterfaceGenerated();
    }
    else 
      mSeenDepositInitValue = true;
  }
}

const IODBRuntime* CarbonHookup::getDB() const 
{
  return mDB;
}

IODBRuntime* CarbonHookup::getDB() 
{
  const CarbonHookup* me = const_cast<const CarbonHookup*>(this);
  return const_cast<IODBRuntime*>(me->getDB());
}

const IODB* CarbonHookup::getBaseDB() const
{
  return getDB();
}

IODB* CarbonHookup::getBaseDB()
{
  return getDB();
}

void CarbonHookup::activateWave() 
{
  set_debug_callback(sRunWaveSchedule);
}

ShellNet* CarbonHookup::getCarbonNet(STAliasedLeafNode* leaf) 
{
  // This delves into the database to get the storage allocated for
  // the given leaf. If there is no storage for this leaf, or a
  // constant designation then this function returns NULL.

  ShellNet* netRet = NULL;
  STAliasedLeafNode* storageLeaf = leaf->getStorage();

  // The storage bom contains the actual information we need with
  // regards to the allocated storage
  ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(storageLeaf);
  // The leaf-specific bom has intrinsic attributes such as array
  // bounds.
  ShellDataBOM* leafBomData = ShellSymTabBOM::getLeafBOM(leaf);

  // The leaf type index tells us which gen_ function to call in
  // schedule.cxx
  SInt32 leafTypeIndex = mDB->getLeafGenTypeIndex(leaf);
  ST_ASSERT(leafTypeIndex >= 0, leaf);

  int typeCode = bomdata->getTypeTag();
  
  const IODBGenTypeEntry* typeEntry = mDB->getLeafType(leaf);
  ST_ASSERT(typeEntry, leaf);

  // See if we already created a ShellData object in the leaf bom.
  // If so, see if we already created a net for this leaf.
  //
  // Since the ShellDataBOM objects are shared among all instances of
  // a model, we need to lock the mutex.
  ShellGlobal::lockMutex();
  ShellData* shellData = leafBomData->getShellData();
  if (shellData)
    netRet = shellData->getCarbonNet(mId);
  else
  {
    shellData = new ShellData;
    leafBomData->setShellData(shellData);
  }
  ShellGlobal::unlockMutex();

  // Either create a new net or increment the reference count on the
  // current one.
  if (netRet)
    netRet->incrCount();
  else
  {
    // have to use the requested node's bomdata to check if the net is
    // expressioned, not the storage
    // node's. bug 5065. Flattening doesn't copy the expression to the
    // flattened net, which usually is marked as the storage net. But,
    // the storage net has no meaning for an expressioned net.
    // We'll fix this eventually in the db writer to put the
    // expression on the storage node. However, because of
    // uncertainties with early flattening and aggressive assignment
    // aliasing, fixing the db writer may not be the correct way to
    // permanently fix the problem.
    bool isExpr = leafBomData->isExpression();

    const IODBIntrinsic* intrinsic = mDB->getLeafIntrinsic(leaf);
    if (! intrinsic)
    {
      UtString name;
      getNodeName(&name, leaf);
      // fatally exit
      mModel->getMsgContext()->SHLTypeEntryNoIntrinsic(name.c_str());
    }

    IODBIntrinsic::Type sigType = intrinsic->getType();
    if (isExpr)
    {
      // This leaf is an expression. We need to walk the expression,
      // and allocate the subnets by recursively calling this
      // function.
      CarbonExpr* descExpr = leafBomData->getExpr();
      ExprNetCreate walk(this, descExpr, leaf, getExprFactory());
      walk.visitExpr(descExpr);
      CarbonExpr* transformedExpr = walk.getResult();
      CE_ASSERT(transformedExpr->getBitSize() == intrinsic->getWidth(),
                descExpr);
      CarbonExprNet* exprNet = new CarbonExprNet(transformedExpr, intrinsic);

      // Update the expression net with unionized attributes
      exprNet->putIsInput(walk.hasInput());
      if (walk.hasInput())
        bomdata->putIsDepositable();

      exprNet->putIsForcible(walk.hasForcible());
      exprNet->putIsTristate(walk.hasTristate());
      
      // If the expression has parts that are constant, we need to put
      // the control mask into the expr net for proper formatting.
      if (walk.hasConstantOverride())
      {
        DynBitVector overrideMask;
        walk.calcOverrideMask(&overrideMask);
        DynBitVectorFactory* bvFactory = mDB->getBVFactory();
        const DynBitVector* factoryOverride = bvFactory->alloc(overrideMask);
        exprNet->putControlMask(factoryOverride->getUIntArray());
      }

      netRet = exprNet;
    }
    else
    {
      // This is a primitive net. Get the offset and call the gen_
      // function.

      int offset = int(bomdata->getStorageOffset());

      const DynBitVector* overrideMask = mDB->getConstNetBitMask(leaf);

      // First, if this net was marked completely constant, do not
      // call a gen_ function, but rather create a constant net.
      bool isCompletelyOverridden = IODBRuntime::sIsCompletelyOverridden(overrideMask, intrinsic);
      
      if (isCompletelyOverridden)
      {
        // create a constant for this net
        DynBitVector val(intrinsic->getWidth());
        UInt32* valArr = val.getUIntArray();
        CarbonValRW::setConstantBits(valArr, (UInt32*) NULL, overrideMask->getUIntArray(), intrinsic->getWidth());
        
        // create storage for the value
        DynBitVectorFactory* bvFactory = mDB->getBVFactory();
        const DynBitVector* factorVal = bvFactory->alloc(val);
        
        netRet = new ShellNetConstOverride(factorVal->getUIntArray(), overrideMask->getUIntArray(), intrinsic);
      }
      // If this net is a constant typeCode be will an appropriate value
      else if (CbuildShellDB::isConstantType(typeCode))
      {
        // The net wasn't marked constant by analysis, but it
        // was marked constant as a result of a continuous assign
        // being placed on it. 

        // We have a constant. Need to figure out size, value, etc.
        // This is signal-specific, so the master or storage nodes are
        // not necessarily the right things to look at. We must look at
        // the type of this particular leaf to get the correct msb, lsb,
        // etc.
        
        const UInt32* constVal = NULL;
        UInt32 numWordsNeeded = 1;
        UInt32 constValWord = 0;;
        if (typeCode == CbuildShellDB::eConstValWordId)
          constValWord = bomdata->getStorageValueWord();
        else if (typeCode == CbuildShellDB::eConstValId)
        {
          const DynBitVector* bv = bomdata->getStorageValue();
          constVal = bv->getUIntArray();
          numWordsNeeded = bv->getUIntArraySize();
        }
        else
          ST_ASSERT(0, leaf);
        
        ST_ASSERT((sigType == IODBIntrinsic::eScalar) || 
                  (sigType == IODBIntrinsic::eVector) 
                  // ati workaround begin
                  || typeEntry->isTristate(), leaf
                  // ati workaround end
                  );
        
        if ( sigType == IODBIntrinsic::eVector )
        {
          if (typeEntry->isReal())
            netRet = new ShellNetConstReal(constVal, 
                                           intrinsic);
          else if (numWordsNeeded == 1)
            netRet = new ShellNetConstVectorWord(constValWord, intrinsic);          
          else
            netRet = new ShellNetConstVector(constVal, numWordsNeeded, 
                                             intrinsic);
        }
        else
          netRet = new ShellNetConstScalar(constValWord);
      }
      
      // There may not be any storage for this net 
      // due to complexities of writing the symbol table db
      else if ((offset > -1) && (typeCode == CbuildShellDB::eOffsetId))
      {

        // Call the gen_ function. The net is not constant (or not
        // completely constant).

        void* dataPtr = findNetStorage (offset);

        bool isScalar = sigType == IODBIntrinsic::eScalar;    

        // Type index should be by the actual leaf (has the correct
        // parameters)
        netRet = makeShellNet (leafTypeIndex, dataPtr, isScalar);
        
        if (netRet == NULL)
        {
          // we have an incompatible library. The library is newer
          // than the carbon version.
          // I don't think this can happen as this should be caught
          // upon construction of the Carbon Model, but just in case, I'd like
          // to have a message. 
          UtString name;
          ShellSymTabBOM::composeName(leaf, &name, false, true);
          mModel->getMsgContext()->SHLIncompatLibNetCreate(name.c_str());
          // Bail out
          return NULL;
        }

        CarbonModelMemory* memType = NULL;
        if (sigType == IODBIntrinsic::eMemory)
        {
          memType = netRet->castModelMemory();
          ST_ASSERT(memType, leaf);
          memType->setCarbonModel(mModel);
        }

        // Make sure that we don't have a vector if we expect a scalar
        CarbonVectorBase* vecType = netRet->castVector();
        if (isScalar)
          ST_ASSERT(vecType == NULL, leaf);
        
        if (vecType)
        {
          // We need to set the range on the vector net.
          const ConstantRange* vecRange = intrinsic->getVecRange();
          ST_ASSERT(vecRange, leaf);
          vecType->putRange(vecRange);
          if (overrideMask)
            vecType->putControlMask(overrideMask->getUIntArray());
        }
        
        CarbonForceNet* forceType = netRet->castForceNet();
        if (forceType)
        {
          if (sigType == IODBIntrinsic::eVector)
          {
            const ConstantRange* vecRange = intrinsic->getVecRange();
            ST_ASSERT(vecRange, leaf);
            
            // A forcible vector
            forceType->putRange(vecRange);
            
            if (overrideMask)
              forceType->putControlMask(overrideMask->getUIntArray());
          }

          // For force subordinates we need to set them depositable
          // and set their HierNames as well.
          ShellNet* valueNet = forceType->getValueNet();
          ShellNet* maskNet = forceType->getMaskNet();
          ST_ASSERT(valueNet, leaf);
          ST_ASSERT(maskNet, leaf);
          
          if (mDB->allowForceSubordinates())
          {
            STSymbolTable* symTab = mDB->getDesignSymbolTable();
            AtomicCache* strCache = mDB->getAtomicCache();
            // The atomic cache is shared by all model instances, so access
            // needs to be controlled with a mutex.
            ShellGlobal::lockMutex();
            STSymbolTableNode* valueNetNode = 
              symTab->find(leaf->getParent(), IODB::sGetForceValueName(leaf, strCache));
            STSymbolTableNode* maskNetNode = 
              symTab->find(leaf->getParent(), IODB::sGetForceMaskName(leaf, strCache));
            ShellGlobal::unlockMutex();
            
            ST_ASSERT(valueNetNode, leaf);
            ST_ASSERT(maskNetNode, leaf);

            valueNet->setName(valueNetNode);
            maskNet->setName(maskNetNode);

            // In addition, mark both the value and the mask
            // depositable. They are not marked depositable by cbuild,
            // because these nets do not exist in the design. They are
            // implicitly depositable as a result of being forcible.
            ShellDataBOM* subordBom = ShellSymTabBOM::getLeafBOM(valueNet->getNameAsLeaf()->getStorage());
            subordBom->putIsDepositable();
            subordBom = ShellSymTabBOM::getLeafBOM(maskNet->getNameAsLeaf()->getStorage());
            subordBom->putIsDepositable();
          }
          else
          {
            valueNet->setName(leaf);
            maskNet->setName(leaf);
          }
        }
        
        // Vector and scalar nets that are inputs have a specialized
        // ShellNet derivative that requires a change array reference,
        // which is set when it's deposited.  Memories don't have this
        // distinction, but it's possible for a memory to be an input.
        // Since memory deposits should be infrequent, all memories
        // are forced to have a change array also.
        if (netRet->isInput() || (memType != NULL))
        {
          // An input may have a change array index.
          if (mIndStoreMap)
          {
            SInt32 index = mIndStoreMap->getIndex(SInt32(offset));
            if (index >= 0)
            {
              ST_ASSERT(index < mChangeArraySize, leaf);
              netRet->putChangeArrayRef(&mChangeArray[index]);
            }
            else
            {
              // no change array index, so assign a phony one to the
              // net to reduce branching when we deposit.
              netRet->putChangeArrayRef(&mDummyChangeArrayRef);
            }

            if (netRet->isInput()) {
              // Inputs are depositable. They are not marked depositable
              // by cbuild, though. Mark it here.
              bomdata->putIsDepositable();
              // Inputs are implicitly observable as well
              bomdata->putIsObservable();
            }
          } // if mIndStoreMap
        } // if input
      } // if valid offset
    } // if not an expression
    
    if (netRet)
    {
      // Place in table      
      ShellGlobal::lockMutex();
      shellData->putCarbonNet(netRet, mId);
      ShellGlobal::unlockMutex();
      netRet->setName(leaf);
    }
  }
  
  return netRet;
}

void CarbonHookup::freeNet(ShellNet** net)
{
  ShellNet* ref = NULL;
  if (net)
  {
    ref = *net;
    *net = NULL;
  }
  
  if (ref && ref->decrCount())
  {
    STAliasedLeafNode* node = ref->getNameAsLeaf();
    INFO_ASSERT(node, "corrupt net");
    
    // ShellData* will be in this node
    ShellGlobal::lockMutex();
    deleteNet(node);
    ShellGlobal::unlockMutex();
  }
}

void CarbonHookup::deleteNet(STSymbolTableNode* node)
{
  // Caller must have locked mutex before calling
  ST_ASSERT(ShellGlobal::isLocked(), node);
  ST_ASSERT(node->castLeaf() != NULL, node);
  ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);
  if (bomdata)
  {
    ShellData* shellData = bomdata->getShellData();
    if (shellData)
    {
      ShellNet* net = shellData->getCarbonNet(getId());
      shellData->putCarbonNet(NULL, getId());
      delete net;    
    }
  }
}

void CarbonHookup::beginChangeIndToStorageMap()
{
  INFO_ASSERT(mIndStoreMap == NULL, "Function called more than once.");
  mIndStoreMap = ShellGlobal::createChangeIndStorageMap(getDB());
}

void CarbonHookup::mapChangeIndToStorage(SInt32 chIndex, SInt32 storageOffset)
{
  mIndStoreMap->mapIndex(chIndex, storageOffset);
}

void CarbonHookup::endChangeIndToStorageMap()
{}

void CarbonHookup::putChangeArrayToStorageMap(ShellGlobal::ChangeIndexStorageMapI* indMap)
{
  mIndStoreMap = indMap;
}


CarbonModel* CarbonHookup::getCarbonModel()
{
  return mModel;
}

SInt32 CarbonHookup::getChangeArrayIndex(const STAliasedLeafNode* node) const
{
  const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node->getStorage());
  SInt32 offset = bomdata->getStorageOffset();  
  SInt32 index = -1;
  if (mIndStoreMap && (offset > -1))
    index = mIndStoreMap->getIndex(offset);
  return index;
}

CarbonChangeType* CarbonHookup::getChangeArrayEntry(SInt32 validChgArrIndex)
{
  CarbonChangeType* changeArray = getChangeArray();  
  return &changeArray[validChgArrIndex];
}

bool CarbonHookup::getIsChanged(SInt32 validChgArrIndex) const
{
  return mChangeArray[validChgArrIndex] != 0;
}

bool CarbonHookup::isConstant(const STSymbolTableNode* node) const 
{
  bool ret = false;
  const STAliasedLeafNode* lnode = node->castLeaf();
  if (lnode)
  {
    const STAliasedLeafNode* storageLeaf = lnode->getStorage();
    const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(storageLeaf);
    int typeCode = bomdata->getTypeTag();
    ret = CbuildShellDB::isConstantType(typeCode);
  }
  return ret;
}

bool CarbonHookup::hasValidCarbonNet(const STAliasedLeafNode* node) const
{
  const STAliasedLeafNode* storageLeaf = node->getStorage();
  const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(storageLeaf);
  return (bomdata->getTypeTag() != CbuildShellDB::eNoTypeId);
}

ESFactory* CarbonHookup::getExprFactory()
{
  return mExprFactory;
}

CarbonExamineScheduler* CarbonHookup::getCarbonExamineScheduler()
{
  if (! mExamineSched)
    mExamineSched = new CarbonExamineScheduler(this);
  
  return mExamineSched;
}

ShellData* CarbonHookup::getShellData(STAliasedLeafNode* node)
{
  ShellDataBOM* leafBomData = ShellSymTabBOM::getLeafBOM(node); 
  return leafBomData->getShellData();
}

void CarbonHookup::maybeRunDepositSchedule()
{
  if (mPrivate->checkAndClearRunDepositComboSchedule ()) {
    mDepositComboScheduleFn (mDescr);
  }
}

/*virtual*/ void CarbonHookup::addRunDepositComboSched (bool runit)
{
  mPrivate->addRunDepositComboSchedule (runit);
}

//! Locate storage within the model
/*virtual*/ void *CarbonHookup::findNetStorage (int offset)
{
  char *instCalc = reinterpret_cast <char*> (mDescr->mHdl);
  void *dataPtr = instCalc + sizeof (carbon_top) + offset;
  return dataPtr;
}

//! Construct a ShellNet
/*virtual*/ ShellNet *CarbonHookup::makeShellNet (UInt32 index, void *storage, bool isScalar)
{
  return (*mDescr->debugGeneratorFn) (index, storage, isScalar);
}

//! Set the debug callback 
void CarbonHookup::set_debug_callback (CarbonShellCallBackFn f)
{
  mPrivate->getShellCallBack ()->mDebugFunc = f;
}

//! Get the debug callback
CarbonShellCallBackFn CarbonHookup::get_debug_callback () const
{
  return mPrivate->getShellCallBack ()->mDebugFunc;
}

//! \return the message context
/*virtual*/ MsgContext *CarbonHookup::getMsgContext ()
{
  return mPrivate->getMsgContext ();
}

//! \return the init flags for the model
/*virtual*/ CarbonInitFlags CarbonHookup::getInitFlags () const
{
  return mDescr->mInitFlags;
}

//! Set the init flags for the model
/*virtual*/ void CarbonHookup::putInitFlags (int flags)
{
  mDescr->mInitFlags = CarbonInitFlags (flags);
}

//! \return the unique identifier for the design
/*virtual*/ const char *CarbonHookup::getUID () const
{
  return mDescr->mUID;
}

//! Call the simOverFn
/*virtual*/ void CarbonHookup::callSimOver ()
{
  if (mSimulationOverFn != NULL) {
    (*mSimulationOverFn) (mDescr);
  }
}

//! Call the destroyFn
/*virtual*/ void CarbonHookup::callDestroy ()
{
  if (mDestroyFn != NULL) {
    (*mDestroyFn) (mDescr);
  }
}

//! Call the initialisation function
/*virtual*/ void CarbonHookup::callInit (void *cmodelData, void *reserved1, void *reserved2)
{
  mInitializeFn (mDescr, cmodelData, reserved1, reserved2);
}

//! Call the debug_schedule method
/*virtual*/ void CarbonHookup::call_debug_schedule ()
{
  mDebugScheduleFn (mDescr);
}

//! \return the ControlHelper instance for the design
/*virtual*/ ControlHelper *CarbonHookup::getControlHelper ()
{
  return mDescr->getControlHelper ();
}

/*virtual*/ bool CarbonHookup::checkAndClearRunDepositComboSchedule ()
{
  return mPrivate->checkAndClearRunDepositComboSchedule ();
}

/*virtual*/ void CarbonHookup::call_deposit_combo_schedule ()
{
  mDepositComboScheduleFn (mDescr);
}

//! Get the simulation time
/*virtual*/ UInt64 CarbonHookup::getTime (void) const
{
  return mDescr->getTime ();
}

//! Set simulation time
/*virtual*/ bool CarbonHookup::setTime (UInt64 t)
{
  return mDescr->setTime (t);
}

//! save simulation time, schedule count, model state, etc. 
/*virtual*/ bool CarbonHookup::save(UtOCheckpointStream &out, CarbonModel *model)
{
  INFO_ASSERT (model->getObjectID () == mDescr, "hookup/descriptor mismatch");
  bool ok = true;

  // save local carbon_id state
  out << getUID ();

  // write sanity test marker
  out.writeToken("carbon_descr");

  // version 2
  out << mDescr->mVersion;

  // Save the init state
  // version 2
  out << mDescr->mChangeArray->mInit;

  // sim time and change array state
  out << *mDescr->mpTime;

  mDescr->mChangeArray->save (out);

  // save file i/o
  mPrivate->getHDLFileSystem ()->save(out);

  // save generated model state 
  if (!(*mDescr->saveFn) (&out, mDescr)) {
    getMsgContext ()->SHLNoSaveRestore();
    ok = false;
  }

  // as a sanity test, save UID again at the end of the model data
  out << getUID ();

  // check output stream for errors
  if (ok) {
    ok = !out.fail();
  }

  return ok;
}

//! restore simulation time, schedule count, model state, etc. 
/*virtual*/ bool CarbonHookup::restore(UtICheckpointStream &in, CarbonModel *model)
{
  INFO_ASSERT (model->getObjectID () == mDescr, "blah");
  bool ok = true;

  // read UID and check that it matches
  UtString uid(getUID ());
  UtString newUid;    
  in >> newUid;    
  if (newUid != uid) {
    getMsgContext ()->SHLWrongSaveVersion(in.getName());
    ok = false;
  }
  else {
    if (!in.checkToken("carbon_descr")) return false;

    UInt32 version;
    in >> version;

    if (version > mDescr->mVersion)
    {
      getMsgContext ()->SHLIncompatibleVHMForRestore();
      return false;
    }

    // Read the init state
    in >> mDescr->mChangeArray->mInit;

    // read simulation time and change array
    in >> (*mDescr->mpTime);
    mDescr->mChangeArray->restore (in);

    // restore file i/o
    UtIO::cout().flush();
    UtIO::cerr().flush();
    fflush(stdout);
    fflush(stderr);

    mPrivate->deleteFileSystems();   // Delete the old stuff and populate the
    mPrivate->allocateFileSystems(); // file systems from the checkpoint file.
    mPrivate->getHDLFileSystem ()->restore(in);

    // read model state
    if (!(*mDescr->restoreFn) (&in, mDescr)) {
      getMsgContext ()->SHLNoSaveRestore();
      ok = false;
    }

    // read sanity check UID marker
    UtString newUid;    
    in >> newUid;
    if (newUid != uid) {
      getMsgContext ()->SHLWrongSaveVersion(in.getName());
      ok = false;
    }
  }

  // check input stream for errors
  if (ok) {
    ok = !in.fail();
  }

  return ok;
}

CarbonHookup::ChangeArrayDescr::ChangeArrayDescr () : mSaved (NULL)
{}

/*virtual*/ CarbonHookup::ChangeArrayDescr::~ChangeArrayDescr ()
{
  // If this assertion is hit then the change array was explicitly saved but
  // the corresponding restore never happened. This is almost certainly a bug.
  // (I did consider saving in the constructor and restoring in the destructor,
  // but this way it is a little clearer that a change array excursion is being
  // protected).
  INFO_ASSERT (mSaved == NULL, "saved change array was not restored");
}

void CarbonHookup::saveChangeArray (ChangeArrayDescr &descr)
{
  // Note that CarbonHookup is a friend of ChangeArrayDescr
  INFO_ASSERT (descr.mSaved == NULL, "change array is already saved");
  descr.mSaved = CARBON_ALLOC_VEC (CarbonChangeType, mChangeArraySize);
  descr.mSavedInit = mDescr->mChangeArray->mInit;
  ::memcpy (descr.mSaved, mChangeArray, mChangeArraySize);
}

void CarbonHookup::restoreChangeArray (ChangeArrayDescr &descr)
{
  // Note that CarbonHookup is still a friend of ChangeArrayDescr
  INFO_ASSERT (descr.mSaved != NULL, "change array is not saved");
  ::memcpy (mChangeArray, descr.mSaved, mChangeArraySize);
  CARBON_FREE_VEC (descr.mSaved, CarbonChangeType, mChangeArraySize);
  descr.mSaved = NULL;
  mDescr->mChangeArray->mInit = descr.mSavedInit;
}

void CarbonHookup::setChangeArray (CarbonChangeType value)
{
  carbonPrivateInitChangeArray (mDescr->mChangeArray, value);
}

/*virtual*/ CarbonChangeType CarbonHookup::getInit () const
{
  return mDescr->mChangeArray->mInit;
}

/*virtual*/ void CarbonHookup::setInit (CarbonChangeType value) const
{
  mDescr->mChangeArray->mInit = value;
}

//! onDemand version of simSave, called from OnDemandMgr
/*virtual*/ bool CarbonHookup::simSaveOnDemand (UtOCheckpointStream &stream, CarbonModel *model)
{
  return (*mDescr->onDemandSaveFn) (&stream, model->getObjectID ());
}

//! onDemand version of simRestore, called from OnDemandMgr
/*virtual*/ bool CarbonHookup::simRestoreOnDemand (UtICheckpointStream &stream, CarbonModel *model)
{
  return (*mDescr->onDemandRestoreFn) (&stream, model->getObjectID ());
}


