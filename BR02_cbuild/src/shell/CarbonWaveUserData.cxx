// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2005-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/CarbonPlatform.h"
#include "codegen/SysIncludes.h"
#include "shell/CarbonWaveUserData.h"
#include "shell/ShellNetMacroDefine.h"
#include "shell/WaveDump.h"
#include "util/UtString.h"

CarbonWaveUserData::CarbonWaveUserData(WaveDump* fsdbFile, 
                                       int lbitnum, int rbitnum,
                                       CarbonClientData data, 
                                       const char* varName, 
                                       CarbonBytesPerBit bpb,
                                       const char* fullPathScopeName)
{
  mFsdbFile = fsdbFile;
  mUserData = data;
  size_t numBytesPerBit = (1 << bpb);
  mBufferLen = CBITWIDTH(lbitnum, rbitnum) * numBytesPerBit;
  mBuffer = CARBON_ALLOC_VEC(CarbonLogicByteType, mBufferLen);

  mVarName = new UtString(varName);
  mScopeName = new UtString(fullPathScopeName);

  // Cannot copy the buffer because the data may not yet be initialized.
  mHasChanged = false; 
  mIsInit = false; // true after first call to registerChange()
  mIsActive = true;
}

CarbonWaveUserData::~CarbonWaveUserData()
{
  CARBON_FREE_VEC(mBuffer, CarbonLogicByteType, mBufferLen);
  delete mVarName;
  delete mScopeName;
}

const char* CarbonWaveUserData::getVarName() const
{
  return mVarName->c_str();
}

const char* CarbonWaveUserData::getScopeName() const
{
  return mScopeName->c_str();
}

void CarbonWaveUserData::registerChange()
{
  /*
    Beware that logic values are entered with
    CarbonBitType. carbon_shelltypes.h copies exactly the fsdb file's
    fsdbBitType, so the enums are exactly the same. It's ugly, but I
    don't see an acceptable alternative. So, whenever we update the
    fsdb writer we need to make sure the bit type values match. It is
    more likely that a user will use this for integer, real,
    etc. value and not logic ones.
    Debussy is back-compatible, so I don't think they can change the
    values in the enum. They can only add new values.
  */
  memcpy(mBuffer, mUserData, mBufferLen);
  mHasChanged = true;
  mIsInit = true;
}

void CarbonWaveUserData::removeReferences()
{
  // Exclude this signal from updates.  Don't delete it, because its
  // underlying pointer is still associated with its signal in the
  // waveform writer.
  mIsActive = false;
}
