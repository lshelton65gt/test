// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "shell/VcdFile.h"
#include "util/CarbonAssert.h"
#include "util/OSWrapper.h"
#include "util/StringAtom.h"
#include <cmath>
#include <errno.h>
#include <cstdlib>

/*!
  \file
  Implementation of VcdFile class
*/

VcdFile::VcdFile(FileMode fileMode, const char* fileName,
                 CarbonTimescale timescale,
                 bool* isSuccess, UtString* errMsg) :
  WaveDump(timescale),
  mVcdOut(fileName, "w", 2097152),
  cIndent("    ")
{
  mBufferKb = 64;
  mVcdName = fileName;
  *isSuccess = true;
  switch (fileMode)
  {
  case eCreate:
    if (! mVcdOut)
    {
      *isSuccess = false;
      UtString errBuf;
      OSGetLastErrmsg(&errBuf);
      *errMsg << "Unable to open file " << fileName << ": "
              << errBuf << '\n';
    }      
    break;
  case eAppend:
    /*
      Currently do not support appending vcd files
    */
    INFO_ASSERT(fileMode != eAppend, "Append mode for VCD not supported.");
    *isSuccess = false;
    *errMsg << "Append mode for VCD not supported.\n";
    break;
  }

}

VcdFile::~VcdFile()
{
  if (mVcdOut.is_open()) {
    mVcdOut.flush();
    mVcdOut.close();
  }
}

VcdFile* VcdFile::open(FileMode fileMode, const char* fileName, CarbonTimescale timescale,
                       UtString* errMsg)
{
  bool isSuccess;
  VcdFile* file = new VcdFile(fileMode, fileName, timescale, &isSuccess, errMsg);
  if (! isSuccess)
  {
    delete file;
    file = NULL;
  }
  
  return file;
}

void VcdFile::setAutoFlush(UInt32 bufferSize)
{
  mBufferKb = bufferSize;
}

WaveDump::FileStatus 
VcdFile::closeHierarchy(UtString* errMsg)
{
  FileStatus stat = eOK;
  if(VERIFY(mHierOpen))
  {
    if (! (writeDate(errMsg) &&
           writeVersion(errMsg) &&
           writeTimeScale(errMsg) &&
           writeHierarchy(errMsg)))
      stat = eError;
    
    mHierOpen = false;
  }

  clearAuxHierStructs();

  return stat;
}

void VcdFile::writeTime()
{
  mVcdOut.writeStr("#", 1);
  mVcdOut << mCurTime;
  mVcdOut.writeStr("\n", 1);
}

void VcdFile::writeValueChanges()
{
  writeTime();
  if (mTimeCnt > 0)
  {
    for (HandleListCLoop p(mChanges); ! p.atEnd(); ++p)
      writeVcdHandleValue(*p);
  }
  else
  {
    mVcdOut << "$dumpvars\n";
    //have to dumpvar on first time slot
    writeAllValues();
    mVcdOut << "$end\n";    
  }
}

void VcdFile::dumpDataAll()
{
  writeTime();
  mVcdOut << "$dumpall\n";
  writeAllValues();
  mVcdOut << "$end\n";    
}

void VcdFile::dumpDataOn()
{
  writeTime();
  mVcdOut << "$dumpon\n";
  writeAllValues();
  mVcdOut << "$end\n";    
}

void VcdFile::dumpDataOff()
{
  writeTime();
  mVcdOut << "$dumpoff\n";
  writeAllValues();
  mVcdOut << "$end\n";    
}

void VcdFile::writeHandleValue(WaveHandle* handle)
{
  // call non-virtual function
  writeVcdHandleValue(handle);
}

void VcdFile::writeVcdHandleValue(WaveHandle* handle)
{
  bool isReal = handle->isReal();
  char* value = handle->getCurrentValue();
  if ((handle->getSize() == 1) && !isReal )
    mVcdOut.writeStr(value, 1);
  else if (! isReal)
  {
    mVcdOut.writeStr("b", 1);
    mVcdOut.writeStr(value, strlen(value));
    mVcdOut.writeStr(" ", 1);
  }
  else
    // VCS $dump of reals has the format of r<dbl>  id
    // 2 spaces between the number and the id. I'll do the same
    mVcdOut << 'r' << value << "  ";

  const char* glyphStr = getGlyph(handle);
  mVcdOut.writeStr(glyphStr, strlen(glyphStr));
  mVcdOut << UtIO::endl;

  handle->unregister();
}

WaveDump::FileStatus VcdFile::flush(UtString* errMsg)
{
  mVcdOut.flush();
  return checkFileState(errMsg);
}

WaveDump::FileStatus VcdFile::close(UtString* errMsg)
{
  mVcdOut.close();
  return checkFileState(errMsg);
}

WaveDump::FileStatus VcdFile::checkFileState(UtString* errMsg)
{
  FileStatus stat = eOK;
  if (mVcdOut.bad())
  {
    *errMsg << "File: " << mVcdName << " is corrupted.\n";
    stat = eError;
  }
  return stat;
}

bool VcdFile::writeDate(UtString* errMsg)
{
  UtString tmp;
  bool isOK = true;
  if (! (mVcdOut << "$date\n" << cIndent << OSGetTimeStr("%b %d, %Y  %H:%M:%S",&tmp) << "\n$end\n"))
  {
    isOK = false;
    *errMsg << "Unable to write to file: " << mVcdName << '\n';
  }
  return isOK;
}

bool VcdFile::writeVersion(UtString* errMsg)
{
  UtString tmp;
  bool isOK = true;
  if (! (mVcdOut << "$version\n" << cIndent << cShellVersion << "\n$end\n"))
  {
    isOK = false;
    *errMsg << "Unable to write to file: " << mVcdName << '\n';
  }
  return isOK;
}

bool VcdFile::writeTimeScale(UtString* errMsg)
{
  UtString tmp;
  bool isOK = true;
  if (! (mVcdOut << "$timescale\n" << cIndent << stringifyTimescale() << "\n$end\n"))
  {
    isOK = false;
    *errMsg << "Unable to write to file: " << mVcdName << '\n';
  }
  return isOK;
}

const char* VcdFile::stringifyScopeType(WaveScope::ScopeType type) const
{
  switch(type)
  {
  case WaveScope::eModule:
  // Prior to the visibility project, structures were dumped as module scopes, so continue that.
  case WaveScope::eStruct:
    return "module"; break;
  case WaveScope::eTask: return "task"; break;
  case WaveScope::eFunction: return "function"; break;
  case WaveScope::eBegin: return "begin"; break;
  case WaveScope::eFork: return "fork"; break;
  // We never dump arrays in VCD
  case WaveScope::eArray:
    INFO_ASSERT(0, "Unhandled scope type");
    return NULL;
    break;
  }
  
  //NOT REACHED
  return NULL;
}

const char* VcdFile::stringifyTimescale() const
{
  switch(mTimeScale)
  {
  case e1fs: return "1 fs"; break;
  case e10fs: return "10 fs"; break;
  case e100fs: return "100 fs"; break;
  case e1ps: return "1 ps"; break;
  case e10ps: return "10 ps"; break;
  case e100ps: return "100ps"; break;
  case e1ns: return "1 ns"; break;
  case e10ns: return "10 ns"; break;
  case e100ns: return "100 ns"; break;
  case e1us: return "1 us"; break;
  case e10us: return "10 us"; break;
  case e100us: return "100 us"; break;
  case e1ms: return "1 ms"; break;
  case e10ms: return "10 ms"; break;
  case e100ms: return "100 ms"; break;
  case e1s: return "1 s"; break;
  case e10s: return "10 s"; break;
  case e100s: return "100 s"; break;
  }

  INFO_ASSERT(0, "Unhandled timescale");
  // NOT REACHED
  return NULL;
}

const char* VcdFile::stringifySigType(CarbonVerilogType type) const
{
  switch(type)
  {
  case eVerilogTypeInteger: return "integer"; break;
  case eVerilogTypeParameter: return "parameter"; break;
  case eVerilogTypeReal: return "real"; break;
  case eVerilogTypeReg: return "reg"; break;
  case eVerilogTypeSupply0: return "supply0"; break;
  case eVerilogTypeSupply1: return "supply1"; break;
  case eVerilogTypeTime: return "time"; break;
  case eVerilogTypeTri: return "tri"; break;
  case eVerilogTypeTriAnd: return "triand"; break;
  case eVerilogTypeTriOr: return "trior"; break;
  case eVerilogTypeTriReg: return "trireg"; break;
  case eVerilogTypeTri0: return "tri0"; break;
  case eVerilogTypeTri1: return "tri1"; break;
  case eVerilogTypeWAnd: return "wand"; break;
  case eVerilogTypeWire: return "wire"; break;
  case eVerilogTypeWOr: return "wor"; break;
  case eVerilogTypeUnsetNet: return "unsetnet"; break;
  case eVerilogTypeUnknown: return "wire"; break;
  case eVerilogTypeRealTime: return "realtime"; break;
  case eVerilogTypeUWire: return "uwire"; break;
  }
  
  INFO_ASSERT(0, "Unhandled signal type");
  // NOT REACHED
  return NULL;
}

bool VcdFile::writeHierarchy(UtString* errMsg)
{
  Glyph glyph;
  sortRoots();
  WaveScopeIter p = loopRoots();
  WaveScope* scope;
  while (p(&scope))
    writeHierBranch(scope, glyph);
  
  // end definitions
  mVcdOut << "$enddefinitions $end\n";

  return (checkFileState(errMsg) == eOK);
}

void VcdFile::VcdifyName(UtString* name)
{
  INFO_ASSERT(! name->empty(), "VCDName can't be empty");
  // remove leading escapes
  if ((*name)[0] == '\\')
    name->erase(0, 1);
}

void VcdFile::writeHierBranch(WaveScope* scope, Glyph& glyph)
{
  INFO_ASSERT(scope, glyph.getStr());
  // If scope has no children and no signals, skip it.
  if (! isEmpty(scope))
  {
    UtString nameBuf;
    // write scope info
    mVcdOut << "$scope " << stringifyScopeType(scope->getType()) << ' ' << scope->getName(&nameBuf) << " $end\n";
    // Write scope's variables
    {
      scope->sortChildren();
      WaveHandleIter hPtr = scope->loopHandles();
      UtString buf; // needed by Handle to create full name
      WaveHandle* handle;
      while(hPtr(&handle))
      {
        // var, type
        mVcdOut << "$var " << stringifySigType(handle->getType());
        // size
        mVcdOut << '\t' << handle->getSize() << ' ';
        // glyph
        if (setGlyph(handle, glyph))
          ++glyph;
        mVcdOut << getGlyph(handle);
        
        // name, end
        handle->getNameWithRange(&buf);
        VcdifyName(&buf);
        mVcdOut << '\t' << buf.c_str() << " $end\n";
      }
    }
    
    // Now write out children
    {
      WaveScopeIter sPtr = scope->loopScopes();
      WaveScope* childScope;
      while(sPtr(&childScope))
        writeHierBranch(childScope, glyph);
    }
    
    // now upscope
    mVcdOut << "$upscope $end\n";
  }
}

bool VcdFile::setGlyph(WaveHandle* handle, Glyph& glyph)
{
  bool useGlyph = false;
  if (handle->getObj() == NULL)
  {
    WaveHandle* sig = handle->getTopHandle(handle);
    if (sig->getObj() == NULL)
    {
      useGlyph = true;
      mGlyphStrs.push_back(glyph.getStr());
      char* glyphStr = mGlyphStrs.back();
      sig->setObj(glyphStr);
    }
    else
      handle->setObj(sig->getObj());
  }
  
  return useGlyph;
}

const char* VcdFile::getGlyph(const WaveHandle* handle) const
{
  const WaveHandle* sig = handle->getTopHandle(handle);
  const char* glyph = static_cast<const char*>(sig->getObj());
  return glyph;
}

void VcdFile::addChangedWithDbl(WaveHandle* sig, double value)
{
  sig->updateValueDbl(value);
  addChanged(sig);
}

void VcdFile::addChangedWithInt32(WaveHandle* sig, SInt32 value)
{
  sig->updateValueInt32(value);
  addChanged(sig);
}

void VcdFile::addChangedWithInt64(WaveHandle* sig, UInt64 value)
{
  sig->updateValueInt64(value);
  addChanged(sig);
}


// Glyph definition
VcdFile::Glyph::Glyph() :
  cLexLow('!'),
  cLexHigh('~')
{
  mIdCode += cLexLow;
}

VcdFile::Glyph::~Glyph()
{
}

VcdFile::Glyph& VcdFile::Glyph::operator++()
{
  Increment();
  return *this;
}

void VcdFile::Glyph::Increment()
{
  UInt32 i;
  for (i = 0; (i < mIdCode.size()) && (mIdCode[i] == cLexHigh); ++i)
    mIdCode[i] = cLexLow;

  if (i == mIdCode.size())
    mIdCode += cLexLow;
  else {
    char p = mIdCode[i];
    mIdCode[i] = p + 1;
  }
}

const char* VcdFile::Glyph::getStr() const
{
  return mIdCode.c_str();
}

