/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// to do: 
//
// keep track of system state : running vs idle vs interruptable
//
// maxsim can tell us if it's running or not, so it's either running or
// idle.

// systemc does not tell us if it's running or not, but if we
// use threads &/or signals to interrupt it to set a boolean at some point
// when the carbon generated code wakes up.  So systemc would be interruptable.
// 

#include "util/ArgProc.h"
#include "util/CarbonAssert.h"
#include "util/OSWrapper.h"
#include "util/UtHashSet.h"
#include "util/UtIOStream.h"
#include "util/UtParamFile.h"
#include "util/subprocesses.h"
#include "shell/OnDemandTraceFileWriter.h"
#include "shell/carbon_capi.h"

#define DEFAULT_RECOVER_PERCENTAGE 10
#define DEFAULT_MIN_INTERVAL 20000
#define DEFAULT_ON_DEMAND_BACKOFF_STRATEGY eCarbonOnDemandBackoffDecay

const char* gCarbonReplayStateStrings[] = {
  "normal", "record", "playback", "recovery", NULL
};

const char* gCarbonOnDemandStateStrings[] = {
  "looking", "idle", "backoff", "disabled", NULL
};

const char *gCarbonOnDemandBackoffStrategyStrings[] = {
  "constant", "decay", NULL
};

const char* gCarbonReplaySimProcessStatusStrings[] = {
  "run", "pause", "exit", NULL
};

const char* gCarbonReplayEventTypeStrings[] = {
  "exit",
  "normal",
  "checkpoint",
  "update",
  "pause",
  "resume",
  "record",
  "playback",
  "recover",
  "odnotidle",
  "odidle",
  "oddisable",
  NULL
};

#define NO_EXTERN_REPLAY_STRINGS 1
#include "shell/ReplaySystem.h"
#undef NO_EXTERN_REPLAY_STRINGS

// version 2 - Added OnDemand state
// version 3 - Record all OnDemand parameters on a component basis, plus database ID
#define CURRENT_VERSION 3

#define DEFAULT_DB_DIR "carbon_replay.rdb"

CarbonReplaySystem::CarbonReplaySystem(const char* systemName) :
  mSystemName(systemName),
  mArgs(NULL),
  mOwnArgs(false),
  mArgsDeclared(false),
  mVerbose(false),
  mSyncGUI(false),
  mGUIWasSynched(false),
  mSimReadTime(0),
  mSimReadSize(0),
  mEventsReadTime(0),
  mEventsReadSize(0),
  mUserReadTime(0),
  mTotalScheduleCalls(0),
  mSimStatus(eReplayEventNormal),
  mCycleCountSpecified(false),
  mSimTimeSpecified(false),
  mEventFilePosition(0),
  mPrevEventSize(0),
  mSimStartTimestamp(0)
{
  clear();
#if REPLAY_USE_ENV_VAR
  const char* replayCmd = getenv("CARBON_REPLAY_CMD");
  if (replayCmd)
    mReplayCmdEnv.assign(replayCmd);
#endif
}

//! dtor
CarbonReplaySystem::~CarbonReplaySystem() {
  clear();
  if (mOwnArgs) {
    delete mArgs;
  }
}  

void CarbonReplaySystem::clear(bool retain_events_and_components) {
  mDatabaseDirectory = DEFAULT_DB_DIR;
  mVersion = CURRENT_VERSION;
  mMinInterval = DEFAULT_MIN_INTERVAL;
  mRecoverPercentage = DEFAULT_RECOVER_PERCENTAGE;
  mUserMode = eCarbonRunNormal;
  mRecordAll = false;
  mPlaybackAll = false;
  mUserReadTime = 0;
  mSimPID = 0;

  mErrmsg.clear();
  if (! retain_events_and_components) {
    clearEvents();
    mNameComponentMap.clearPointerValues();
    mComponents.clear();
  }
}

void CarbonReplaySystem::clearEvents() {
  mEvents.clearPointers();
  mEventFilePosition = 0;
  mPrevEventSize = mEvents.size();
}

//! read the system information from the systemInfoFile -- if it's changed
/*!
 *! This method will make a system call to see if the systemInfoFile has
 *! changed, so it is not appropriate for running in a simulation
 *! loop.
 */
bool CarbonReplaySystem::readSystem(bool onlyIfChanged) {
  bool ret = readSystemCSS(onlyIfChanged);
  ret |= readSystemEvents(onlyIfChanged);
  return ret;
}

bool CarbonReplaySystem::readSystemCSS(bool onlyIfChanged) {
  mErrmsg.clear();
  UtString fname(mSystemName);
  fname << CARBON_STUDIO_SIMULATION_EXT;
  bool eof_found = false;

  if (onlyIfChanged) {
    OSStatEntry statEntry;
    UtString errmsg;
    OSStatFileEntry(fname.c_str(), &statEntry, &errmsg);

    // note that with unix filetimes measured in seconds, the
    // timestamp may not change between the last checkpoint and
    // the final update, causing us to miss that final call.
    if ((statEntry.getModTime() != mSimReadTime) ||
        (statEntry.getFileSize() != mSimReadSize))
    {
      mSimReadTime = statEntry.getModTime();
      mSimReadSize = statEntry.getFileSize();
    }
    else {
      mErrmsg << "did not change";
      return false;             // did not change
    }
  }

  UtParamFile file(fname.c_str());
  if (!file.is_open()) {
    mErrmsg = file.getErrmsg();
    eof_found = true;
  }
  else {
    UtIStringStream line;

    // clear the basic flags, but not the components & events.  Those are
    // incrementally added.
    clear(true);

    while (file.getline(&line) && mErrmsg.empty()) {
      UtString kwd, db;
      UInt32 interval = 0;
      UInt32 recover = 0;
      UInt32 maxStates = 0;
      UInt32 backoffStates = 0;
      UInt32 backoffDecayPercent = 0;
      UInt32 backoffMaxDecay = 0;
      CarbonOnDemandBackoffStrategyIO backoffStrategy;

      line >> kwd;
      if (kwd == "version") {
        line >> mVersion;
      }
      else if (kwd == "database") {
        line >> db;
        mDatabaseDirectory = db;
      }
      else if (kwd == "minInterval") {
        if (line >> interval) {
          mMinInterval = interval;
        }
      }
      else if (kwd == "recoverPercent") {
        if (line >> recover) {
          mRecoverPercentage = recover;
        }
      }
      else if (kwd == "onDemandMaxStates") {
        if (line >> maxStates) {
          putOnDemandMaxStates(maxStates);
        }
      }
      else if (kwd == "onDemandBackoffStrategy") {
        if (line >> backoffStrategy) {
          putOnDemandBackoffStrategy(backoffStrategy);
        }
      }
      else if (kwd == "onDemandBackoffStates") {
        if (line >> backoffStates) {
          putOnDemandBackoffStates(backoffStates);
        }
      }
      else if (kwd == "onDemandBackoffDecayPercent") {
        if (line >> backoffDecayPercent) {
          putOnDemandBackoffDecayPercent(backoffDecayPercent);
        }
      }
      else if (kwd == "onDemandBackoffMaxDecay") {
        if (line >> backoffMaxDecay) {
          putOnDemandBackoffMaxDecay(backoffMaxDecay);
        }
      }
      else if (kwd == "component") {
        UtString name;
        if (line >> name) {
          // Note that while the GUI is running, more components can be
          // added.  Currently, there is no way to remove a component from
          // a running system without closing down the process.  There is
          // a removeComponent method in this class but it's not, as of Oct 07,
          // called in our source code, or exposed to users via a C API in
          // src/inc/shell/carbon_system.h.  If that capability gets enabled,
          // then we have to add & test code in here to make sure the event
          // structures don't get stale after a component is removed
          INFO_ASSERT(REMOVE_COMPONENT_SUPPORT == 0,
                      "component removal event coherency NYI");
          addComponent(name.c_str());
          CarbonSystemComponent* comp = findComponent(name.c_str());
          //INFO_ASSERT(comp, "component management insanity");
          if (comp) {
            // onDemand status was introduced in version 2
            bool hasOnDemandStatus = (mVersion >= 2);
            // Individual OnDemand component settings were added in version 3
            bool hasOnDemandSettings = (mVersion >= 3);
            (void) comp->read(line, hasOnDemandStatus, hasOnDemandSettings); // error recorded in 'line', checked below
            if (comp->getReplayState() == eCarbonRunRecord) {
              mRecordComponents.insert(name);
            }
            else if (comp->getReplayState() == eCarbonRunPlayback) {
              mPlaybackComponents.insert(name);
            }
          }
          else
            mErrmsg << "Component '"
                    << name << "' could not be added to the Carbon System.";
        }
      }
      else if (kwd == "event") {
        // Old .css files have events in them so parse them if they are present
        readEvent(line);
      }
      else if (kwd == "simstatus") {
        line >> mSimStatus;
      }
      else if (kwd == "simhost") {
        line >> mSimHost;
      }
      else if (kwd == "simpid") {
        line >> mSimPID;
      }
      else if (kwd == "cycleCountSpecified") {
        UInt32 spec = 0;
        line >> spec;
        mCycleCountSpecified = spec != 0;
      }
      else if (kwd == "simTimeSpecified") {
        UInt32 spec = 0;
        line >> spec;
        mSimTimeSpecified = spec != 0;
      }
      else if (kwd == "simTimeUnit") {
        line >> mSimTimeUnits;
      }
      else if (kwd == "eof") {
        eof_found = true;
      }
      else if (kwd == "simStartTimestamp") {
        UInt64 timestamp;
        if (line >> timestamp) {
          if (timestamp != mSimStartTimestamp) {
            // The simulation timestamp has changed (or is being initialized
            // from scratch).  Clear any stale events
            clearEvents();
            putSimStartTimestamp(timestamp);
          }
        }            
      }

      if (line.bad()) {
        mErrmsg << line.getErrmsg();
      }
    } // while

    // Derive the user defaults from the current state of the simulation.
    // This helps the GUI get populated correctly from a new simulation
    // that was started with command-line arguments.
    if (mRecordComponents.size() == mComponents.size()) {
      // mRecordComponents.clear();
      mRecordAll = true;
    }
    if (mPlaybackComponents.size() == mComponents.size()) {
      // mPlaybackComponents.clear();
      mPlaybackAll = true;
    }

  } // else

  if (! eof_found) {
    mErrmsg << " ... EOF not found";
  }

  return mErrmsg.empty();
} // bool CarbonReplaySystem::readSystemCSS

bool CarbonReplaySystem::readSystemEvents(bool onlyIfChanged) {
  mErrmsg.clear();
  UtString fname(mSystemName);
  fname << CARBON_STUDIO_EVENTS_EXT;

  if (onlyIfChanged) {
    OSStatEntry statEntry;
    UtString errmsg;
    OSStatFileEntry(fname.c_str(), &statEntry, &errmsg);

    // note that with unix filetimes measured in seconds, the
    // timestamp may not change between the last checkpoint and
    // the final update, causing us to miss that final call.
    if ((statEntry.getModTime() != mEventsReadTime) ||
        (statEntry.getFileSize() != mEventsReadSize))
    {
      mEventsReadTime = statEntry.getModTime();
      mEventsReadSize = statEntry.getFileSize();
    }
    else {
      mErrmsg << "did not change";
      return false;             // did not change
    }
  }

  UtParamFile file(fname.c_str());
  if (!file.is_open()) {
    mErrmsg = file.getErrmsg();
  }
  else {
    UtIStringStream line;
    file.seek(mEventFilePosition, UtIO::seek_set);

    while (file.getline(&line) && mErrmsg.empty()) {
      mEventFilePosition = file.tell(); // cheap (held in member var)

      UtString kwd;
      line >> kwd;
      if (kwd == "event") {
        readEvent(line);
      }

      if (line.bad()) {
        mErrmsg << line.getErrmsg();
      }
    }
  } // else

  return mErrmsg.empty();
} // bool CarbonReplaySystem::readSystem

void CarbonReplaySystem::readEvent(UtIStringStream& line) {
  Event* event = new Event(this, line);
  mEvents.push_back(event);
  mTotalScheduleCalls = event->mTotalSchedCalls;
  if (event->mType == eReplayEventCheckpoint) {
    CarbonSystemComponent* comp = event->mComponent;
    if (comp != NULL) {
      UInt32 ckpts = comp->getNumCheckpoints();
      comp->putNumCheckpoints(ckpts + 1);
    }
  }
}

//! poll the system information with minimal overhead
/*!
 *! This call may use a concurrent thread to monitor the system information
 *! file, or use something like SIGUSR1 on Linux, to detect
 *! if a readSystem() call is needed.  This solution may be needed
 *! for CoWare, but it's not clear how to make it work on Windows.
 *!
 *! Specifically, this call will not make any system calls (in mainline)
 *! unless the file has actually been changed.
 */
void CarbonReplaySystem::pollSystem() {
}

//! write the system information to a file
bool CarbonReplaySystem::writeSystem() {
  detectSimRestart();

  // Write to a temp file so if the GUI reads it while the simulator
  // is writing it, it won't see half a file
  UtString fname(mSystemName), tmpname;
  fname << CARBON_STUDIO_SIMULATION_EXT;
  tmpname << fname << ".tmp";
  UtOBStream file(tmpname.c_str());
  mErrmsg.clear();
  if (! file.is_open()) {
    mErrmsg = file.getErrmsg();
  }
  else {
    file << "version " << mVersion << "\n";
    file << "database " << mDatabaseDirectory << "\n";
    file << "minInterval " << mMinInterval << "\n";
    file << "recoverPercent " << mRecoverPercentage << "\n";
    file << "cycleCountSpecified " << (UInt32) mCycleCountSpecified << "\n";
    file << "simTimeSpecified " << (UInt32) mSimTimeSpecified << "\n";

    file << UtIO::slashify;
    for (UInt32 i = 0; i < numComponents(); ++i) {
      CarbonSystemComponent* comp = getComponent(i);
      comp->write(file);
    }

    file << "simstatus" << mSimStatus.getString() << '\n';
    file << "simhost" << mSimHost << '\n';
    file << "simpid" << mSimPID << '\n';
    file << "simTimeUnit" << mSimTimeUnits << '\n';
    file << "simStartTimestamp" << mSimStartTimestamp;
    file << "eof" << '\n';

    if (!file.close()) {
      mErrmsg = file.getErrmsg();
    }
  }

  if (mErrmsg.empty()) {
    (void) OSRenameFile(tmpname.c_str(), fname.c_str(), &mErrmsg);
  }
  else {
    return false;
  }

  // Now write the events to the .cse file.  Note that we can't use
  // the tmp/rename trick here, because we are trying to append.  But
  // on Windows we might not be able to open this file for append
  // if ccontrol/modelstudio is reading it, so gracefully (and silently)
  // fail.  We'll write the pending events next time there is an update.
  fname = mSystemName;
  fname << CARBON_STUDIO_EVENTS_EXT;
  UtOBStream efile(fname.c_str(), (mPrevEventSize == 0) ? "w" : "a");
  if (! efile.is_open()) {
    return true;                // ignore error.  write events next time.
  }

  efile << UtIO::slashify;
  for (UInt32 i = mPrevEventSize, n = mEvents.size(); i < n; ++i) {
    Event* event = mEvents[i];
    event->write(efile);
  }
  if (!efile.close()) {
    mErrmsg = efile.getErrmsg();
  }

  if (mErrmsg.empty()) {
    // If successful, then next time we write, pick up where we left off
    mPrevEventSize = mEvents.size();
    return true;
  }
  return false;
} // bool CarbonReplaySystem::writeSystem

void CarbonReplaySystem::putSimStartTimestamp(UInt64 timestamp) {
  if (mSimStartTimestamp != timestamp) {
    mSimStartTimestamp = timestamp;
    for (UInt32 i = 0, n = mComponents.size(); i < n; ++i) {
      CarbonSystemComponent* comp = mComponents[i];
      comp->putSimStartTimestamp(timestamp);
    }
  }
}

//! get any error message associated with last I/O operation
const char* CarbonReplaySystem::getErrmsg() {
  return mErrmsg.c_str();
}

//! Set Replay verbosity
void CarbonReplaySystem::setVerboseReplay(int verbose)
{
  mVerbose = (verbose != 0);
}

//! find a component by name or return NULL if not found
CarbonSystemComponent* CarbonReplaySystem::findComponent(const char* name)
{
  NameComponentMap::iterator p = mNameComponentMap.find(name);
  if (p == mNameComponentMap.end()) {
    return NULL;
  }
  CarbonSystemComponent* comp = p->second;
  return comp;
}

//! find a const component by name or return NULL if not found
const CarbonSystemComponent* CarbonReplaySystem::findComponent(const char* name) const
{
  NameComponentMap::const_iterator p = mNameComponentMap.find(name);
  if (p == mNameComponentMap.end()) {
    return NULL;
  }
  const CarbonSystemComponent* comp = p->second;
  return comp;
}

//! add new component
void CarbonReplaySystem::addComponent(const char* name) {
  NameComponentMap::iterator p = mNameComponentMap.find(name);
  if (p == mNameComponentMap.end()) {
    CarbonSystemComponent* component = new CarbonSystemComponent(this);
    mNameComponentMap[name] = component;
    component->putName(name);
    component->putReplayState(eCarbonRunNormal);
    component->putOnDemandState(eCarbonOnDemandStopped);
    component->putIndex(mComponents.size());
    // Trace file name is system name + component name with .cot extension
    UtString traceFile = mSystemName;
    traceFile << "." << name << CARBON_STUDIO_ONDEMAND_TRACE_EXT;
    component->putOnDemandTraceFile(traceFile.c_str());
    component->putSimStartTimestamp(mSimStartTimestamp);
    mComponents.push_back(component);
  }
}

#if REMOVE_COMPONENT_SUPPORT
//! remove a ReplayInfo* from the existing system
void CarbonReplaySystem::removeComponent(const char* name) {
  NameComponentMap::iterator p = mNameComponentMap.find(name);
  INFO_ASSERT(p != mNameComponentMap.end(), "component not found");

  CarbonSystemComponent* comp = p->second;

  // Remove the last component from the mComponentsArray, moving
  // the last one in its place
  UInt32 idx = comp->getIndex();
  UInt32 lastIdx = numComponents() - 1;
  if (idx != lastIdx) {
    CarbonSystemComponent* last = mComponents[lastIdx];
    mComponents[idx] = last;
    last->putIndex(idx);
  }

  mComponents.resize(lastIdx);
  mNameComponentMap.erase(p);   // destroys Component
  delete comp;
}
#endif

//! start a GUI in a subprocess to allow the user to manage the replay
//! state
/*!
 *! When a GUI is active, the GUI becomes the sole owner of the replay
 *! database file.  This method is expected to be run from a process
 *! that owns Carbon models.  When a GUI is started, any already-known
 *! components are transmitted to the GUI via the name of the database
 *! file.  If new componentns are added to the system after the GUI has
 *! been started, then the GUI is notified of the new components by sending
 *! it a message via the GUI's stdin.
 *!
 *! When the user changes replay parameters, the GUI state i
 */
bool CarbonReplaySystem::startGUI() {
#if 0
  killGUI();

  // Find the GUI executable
  UtString dir, path;
  OSConstructFilePath(&dir, getenv("CARBON_HOME"), "bin");
  OSConstructFilePath(&path, dir.c_str(), "runtime-gui");
  mGUISubprocess = new async_subprocess;
  arg_vector argv;
  argv += path;
  argv += mSystemInfoFile;
  mErrmsg.clear();
  if (!mGUISubprocess->spawn(path,
                             true,   // connect_stdin
                             false,  // connect_stdout
                             false)) // connect_stderr
  {
    mErrmsg = mGUISubprocess->error_text();
    delete mGUISubprocess;
    mGUISubprocess = NULL;
  }
#endif
  return mErrmsg.empty();
} // bool CarbonReplaySystem::startGUI

void CarbonReplaySystem::killGUI() {
#if 0
  // If there is an existing GUI, kill it, delete it, move on
  if (mGUISubprocess != NULL) {
    (void) mGUISubprocess->kill();
    delete mGUISubprocess;
    mGUISubprocess = NULL;
  }
#endif
}

//! is there a GUI alive?
bool CarbonReplaySystem::isGUIAlive() {
  return false;
}

//! Establish the name of the database
/*!
 *! This database is a directory name, in which we will
 *! store the databases of each system model, according to
 *! their instance name.
 */
void CarbonReplaySystem::putDatabaseDirectory(const char* dir) {
  mDatabaseDirectory = dir;
}

//! Get the current database directory
const char* CarbonReplaySystem::getDatabaseDirectory() {
  return mDatabaseDirectory.c_str();
}

CarbonVHMMode CarbonReplaySystem::getReplayState(const char* name) {
  NameComponentMap::iterator p = mNameComponentMap.find(name);
  INFO_ASSERT(p != mNameComponentMap.end(), "cannot find component");
  CarbonSystemComponent* comp = p->second;
  return comp->getReplayState();
}

void CarbonReplaySystem::putReplayState(const char* name, CarbonVHMMode state) {
  NameComponentMap::iterator p = mNameComponentMap.find(name);
  INFO_ASSERT(p != mNameComponentMap.end(), "cannot find component");
  CarbonSystemComponent* comp = p->second;
  comp->putReplayState(state);
}

void CarbonReplaySystem::putOnDemandState(const char* name, CarbonOnDemandMode state) {
  NameComponentMap::iterator p = mNameComponentMap.find(name);
  INFO_ASSERT(p != mNameComponentMap.end(), "cannot find component");
  CarbonSystemComponent* comp = p->second;
  comp->putOnDemandState(state);
}

CarbonOnDemandMode CarbonReplaySystem::getOnDemandState(const char* name) {
  NameComponentMap::iterator p = mNameComponentMap.find(name);
  INFO_ASSERT(p != mNameComponentMap.end(), "cannot find component");
  CarbonSystemComponent* comp = p->second;
  return comp->getOnDemandState();
}

bool CarbonReplaySystem::isRecordRequested(const char* name) {
  return mRecordAll ||
    (mRecordComponents.find(name) != mRecordComponents.end());
}

bool CarbonReplaySystem::isPlaybackRequested(const char* name) {
  return mPlaybackAll ||
    (mPlaybackComponents.find(name) != mPlaybackComponents.end());
}

bool CarbonReplaySystem::isEnabled(const char* name) {
  NameComponentMap::iterator p = mNameComponentMap.find(name);
  if (p != mNameComponentMap.end()) {
    CarbonSystemComponent* comp = p->second;
    return comp->isEnabled();
  }
  return false;
}

void CarbonReplaySystem::putIsEnabled(const char* name, bool ena) {
  NameComponentMap::iterator p = mNameComponentMap.find(name);
  INFO_ASSERT(p != mNameComponentMap.end(), "cannot find component");
  CarbonSystemComponent* comp = p->second;
  comp->putIsEnabled(ena);
}

void CarbonReplaySystem::putCheckpointInterval(UInt32 recoverPercentage, 
                                               UInt32 num_min_calls) {
  mMinInterval = num_min_calls;
  mRecoverPercentage = recoverPercentage;
}

void CarbonReplaySystem::putOnDemandMaxStates(UInt32 onDemandMaxStates)
{
  for (NameComponentMap::SortedLoop p(mNameComponentMap.loopSorted()); !p.atEnd(); ++p) {
    CarbonSystemComponent* comp = p.getValue();
    comp->putOnDemandMaxStates(onDemandMaxStates);
  }
}

void CarbonReplaySystem::putOnDemandBackoffStates(UInt32 onDemandBackoffStates)
{
  for (NameComponentMap::SortedLoop p(mNameComponentMap.loopSorted()); !p.atEnd(); ++p) {
    CarbonSystemComponent* comp = p.getValue();
    comp->putOnDemandBackoffStates(onDemandBackoffStates);
  }
}

void CarbonReplaySystem::putOnDemandBackoffDecayPercent(UInt32 onDemandBackoffDecayPercent)
{
  for (NameComponentMap::SortedLoop p(mNameComponentMap.loopSorted()); !p.atEnd(); ++p) {
    CarbonSystemComponent* comp = p.getValue();
    comp->putOnDemandBackoffDecayPercent(onDemandBackoffDecayPercent);
  }
}

void CarbonReplaySystem::putOnDemandBackoffMaxDecay(UInt32 onDemandBackoffMaxDecay)
{
  for (NameComponentMap::SortedLoop p(mNameComponentMap.loopSorted()); !p.atEnd(); ++p) {
    CarbonSystemComponent* comp = p.getValue();
    comp->putOnDemandBackoffMaxDecay(onDemandBackoffMaxDecay);
  }
}

void CarbonReplaySystem::putOnDemandBackoffStrategy(CarbonOnDemandBackoffStrategy onDemandBackoffStrategy)
{
  for (NameComponentMap::SortedLoop p(mNameComponentMap.loopSorted()); !p.atEnd(); ++p) {
    CarbonSystemComponent* comp = p.getValue();
    comp->putOnDemandBackoffStrategy(onDemandBackoffStrategy);
  }
}

void CarbonReplaySystem::putOnDemandMaxStates(const char* component_name, UInt32 onDemandMaxStates)
{
  CarbonSystemComponent* comp = findComponent(component_name);
  if (comp != NULL) {
    comp->putOnDemandMaxStates(onDemandMaxStates);
  }
}

UInt32 CarbonReplaySystem::getOnDemandMaxStates(const char* component_name) const
{
  const CarbonSystemComponent* comp = findComponent(component_name);
  UInt32 ret = 0;
  if (comp != NULL) {
    ret = comp->getOnDemandMaxStates();
  }
  return ret;
}

void CarbonReplaySystem::putOnDemandBackoffStates(const char* component_name, UInt32 onDemandBackoffStates)
{
  CarbonSystemComponent* comp = findComponent(component_name);
  if (comp != NULL) {
    comp->putOnDemandBackoffStates(onDemandBackoffStates);
  }
}

UInt32 CarbonReplaySystem::getOnDemandBackoffStates(const char* component_name) const
{
  const CarbonSystemComponent* comp = findComponent(component_name);
  UInt32 ret = 0;
  if (comp != NULL) {
    ret = comp->getOnDemandBackoffStates();
  }
  return ret;
}

void CarbonReplaySystem::putOnDemandBackoffDecayPercent(const char* component_name, UInt32 onDemandBackoffDecayPercent)
{
  CarbonSystemComponent* comp = findComponent(component_name);
  if (comp != NULL) {
    comp->putOnDemandBackoffDecayPercent(onDemandBackoffDecayPercent);
  }
}

UInt32 CarbonReplaySystem::getOnDemandBackoffDecayPercent(const char* component_name) const
{
  const CarbonSystemComponent* comp = findComponent(component_name);
  UInt32 ret = 0;
  if (comp != NULL) {
    ret = comp->getOnDemandBackoffDecayPercent();
  }
  return ret;
}

void CarbonReplaySystem::putOnDemandBackoffMaxDecay(const char* component_name, UInt32 onDemandBackoffMaxDecay)
{
  CarbonSystemComponent* comp = findComponent(component_name);
  if (comp != NULL) {
    comp->putOnDemandBackoffMaxDecay(onDemandBackoffMaxDecay);
  }
}

UInt32 CarbonReplaySystem::getOnDemandBackoffMaxDecay(const char* component_name) const
{
  const CarbonSystemComponent* comp = findComponent(component_name);
  UInt32 ret = 0;
  if (comp != NULL) {
    ret = comp->getOnDemandBackoffMaxDecay();
  }
  return ret;
}

void CarbonReplaySystem::putOnDemandBackoffStrategy(const char* component_name, CarbonOnDemandBackoffStrategy onDemandBackoffStrategy)
{
  CarbonSystemComponent* comp = findComponent(component_name);
  if (comp != NULL) {
    comp->putOnDemandBackoffStrategy(onDemandBackoffStrategy);
  }
}

CarbonOnDemandBackoffStrategy CarbonReplaySystem::getOnDemandBackoffStrategy(const char* component_name) const
{
  const CarbonSystemComponent* comp = findComponent(component_name);
  CarbonOnDemandBackoffStrategy ret = eCarbonOnDemandBackoffConstant;
  if (comp != NULL) {
    ret = comp->getOnDemandBackoffStrategy();
  }
  return ret;
}

const char* CarbonReplaySystem::getOnDemandBackoffStrategyName(const char* component_name) const
{
  const CarbonSystemComponent* comp = findComponent(component_name);
  const char *ret = NULL;
  if (comp != NULL) {
    ret = comp->getOnDemandBackoffStrategy().getString();
  }
  return ret;
}

void CarbonReplaySystem::addOnDemandIdleDeposit(const char* componentName, const char* signalName)
{
  CarbonSystemComponent* comp = findComponent(componentName);
  if (comp != NULL) {
    comp->addOnDemandIdleDeposit(signalName);
  }
}

void CarbonReplaySystem::clearOnDemandIdleDeposit(const char* componentName)
{
  CarbonSystemComponent* comp = findComponent(componentName);
  if (comp != NULL) {
    comp->clearOnDemandIdleDeposit();
  }
}

void CarbonReplaySystem::addOnDemandExcluded(const char* componentName, const char* signalName)
{
  CarbonSystemComponent* comp = findComponent(componentName);
  if (comp != NULL) {
    comp->addOnDemandExcluded(signalName);
  }
}

void CarbonReplaySystem::clearOnDemandExcluded(const char* componentName)
{
  CarbonSystemComponent* comp = findComponent(componentName);
  if (comp != NULL) {
    comp->clearOnDemandExcluded();
  }
}

CarbonSystemComponent::CarbonSystemComponent(const char* name, CarbonReplaySystem* system) :
  mName(name),
  mModel(NULL),
  mReplayInfo(NULL),
  mReplayState(eCarbonRunNormal),
  mOnDemandState(eCarbonOnDemandStopped),
  mEnabled(false),
  mFirstEvent(true),
  mIndex(0),
  mSchedCalls(0),
  mNumCheckpoints(0),
  mOnDemandTraceFileWriter(NULL),
  mLastEvent(NULL),
  mSystem(system)
{
  init();
}

CarbonSystemComponent::CarbonSystemComponent(CarbonReplaySystem* system) :
  mModel(NULL),
  mReplayInfo(NULL),
  mReplayState(eCarbonRunNormal),
  mOnDemandState(eCarbonOnDemandStopped),
  mEnabled(false),
  mFirstEvent(true),
  mIndex(0),
  mSchedCalls(0),
  mNumCheckpoints(0),
  mOnDemandTraceFileWriter(NULL),
  mLastEvent(NULL),
  mSystem(system)
{
  init();
}

CarbonSystemComponent::~CarbonSystemComponent() {
  delete mOnDemandTraceFileWriter;
}

void CarbonSystemComponent::init()
{
  // Ensure that these are consistent with
  // cmm/Resources/CControlModelInstanceProperties.xml
  //
  // We should grab this default value and override whatever is
  // parsed from the xml, or vice versa.  But right now they must
  // be kept consistent in the 2 source files.

  mOnDemandMaxStates = 20;  // from CarbonModel::enableOnDemand()
  mOnDemandBackoffStates = 8;  // from CarbonModel::enableOnDemand()
  mOnDemandBackoffDecayPercent = 50;
  mOnDemandBackoffMaxDecay = 100;
  mOnDemandBackoffStrategy = DEFAULT_ON_DEMAND_BACKOFF_STRATEGY;
  mOnDemandTraceEnabled = false;
}

CarbonObjectID* CarbonSystemComponent::getModel() const
{
  // It's possible that this is not a CarbonSystemSim component, so
  // the model pointer might be NULL.
  CarbonObjectID *model = NULL;
  if (mModel != NULL) {
    model = *mModel;
  }
  return model;
}

void CarbonSystemComponent::write(UtOStream& file) {
  file << "component" << mName << mReplayState.getString() << mOnDemandState.getString()
       << mOnDemandMaxStates << mOnDemandBackoffStrategy.getString() << mOnDemandBackoffStates
       << mOnDemandBackoffDecayPercent << mOnDemandBackoffMaxDecay << mOnDemandTraceEnabled
       << ((UInt32) mEnabled) << mSchedCalls << mNumCheckpoints << mIdString
       << '\n';
}

bool CarbonSystemComponent::read(UtIStream& file, bool hasOnDemandStatus, bool hasOnDemandSettings) {
  UInt32 iena = 0;
  bool success = !(file >> mReplayState).bad();
  if (hasOnDemandStatus) {
    success &= !(file >> mOnDemandState).bad();
  }
  if (hasOnDemandSettings) {
    UInt32 traceEnabled;
    success &= !(file >> mOnDemandMaxStates >> mOnDemandBackoffStrategy >> mOnDemandBackoffStates >> mOnDemandBackoffDecayPercent >> mOnDemandBackoffMaxDecay >> traceEnabled).bad();
    if (success) {
      mOnDemandTraceEnabled = traceEnabled;
    }
  }
  success &= !(file >> iena >> mSchedCalls >> mNumCheckpoints).bad();
  if (success) {
    mEnabled = iena != 0;
  }
  // The database ID was also added with OnDemand settings
  if (hasOnDemandSettings) {
    success &= !(file >> mIdString).bad();
  }
  return success;
}

void CarbonSystemComponent::putOnDemandMaxStates(UInt32 onDemandMaxStates)
{
  if (onDemandMaxStates != mOnDemandMaxStates) {
    mOnDemandMaxStates = onDemandMaxStates;
    CarbonObjectID *model = getModel();
    if (model != NULL) {
      carbonOnDemandSetMaxStates(model, mOnDemandMaxStates);
    }
  }
}

UInt32 CarbonSystemComponent::getOnDemandMaxStates() const
{
  return mOnDemandMaxStates;
}

void CarbonSystemComponent::putOnDemandBackoffStates(UInt32 onDemandBackoffStates)
{
  if (onDemandBackoffStates != mOnDemandBackoffStates) {
    mOnDemandBackoffStates = onDemandBackoffStates;
    CarbonObjectID *model = getModel();
    if (model != NULL) {
      carbonOnDemandSetBackoffCount(model, mOnDemandBackoffStates);
    }
  }
}

UInt32 CarbonSystemComponent::getOnDemandBackoffStates() const
{
  return mOnDemandBackoffStates;
}

void CarbonSystemComponent::putOnDemandBackoffDecayPercent(UInt32 onDemandBackoffDecayPercent)
{
  if (onDemandBackoffDecayPercent != mOnDemandBackoffDecayPercent) {
    mOnDemandBackoffDecayPercent = onDemandBackoffDecayPercent;
    CarbonObjectID *model = getModel();
    if (model != NULL) {
      carbonOnDemandSetBackoffDecayPercentage(model, mOnDemandBackoffDecayPercent);
    }
  }
}

UInt32 CarbonSystemComponent::getOnDemandBackoffDecayPercent() const
{
  return mOnDemandBackoffDecayPercent;
}

void CarbonSystemComponent::putOnDemandBackoffMaxDecay(UInt32 onDemandBackoffMaxDecay)
{
  if (onDemandBackoffMaxDecay != mOnDemandBackoffMaxDecay) {
    mOnDemandBackoffMaxDecay = onDemandBackoffMaxDecay;
    CarbonObjectID *model = getModel();
    if (model != NULL) {
      carbonOnDemandSetBackoffMaxDecay(model, mOnDemandBackoffMaxDecay);
    }
  }
}

UInt32 CarbonSystemComponent::getOnDemandBackoffMaxDecay() const
{
  return mOnDemandBackoffMaxDecay;
}

void CarbonSystemComponent::putOnDemandBackoffStrategy(CarbonOnDemandBackoffStrategy onDemandBackoffStrategy)
{
  if (onDemandBackoffStrategy != mOnDemandBackoffStrategy) {
    mOnDemandBackoffStrategy = onDemandBackoffStrategy;
    CarbonObjectID *model = getModel();
    if (model != NULL) {
      carbonOnDemandSetBackoffStrategy(model, mOnDemandBackoffStrategy);
    }
  }
}

const CarbonOnDemandBackoffStrategyIO& CarbonSystemComponent::getOnDemandBackoffStrategy() const
{
  return mOnDemandBackoffStrategy;
}

void CarbonSystemComponent::putOnDemandTrace(bool enable)
{
  if (enable != mOnDemandTraceEnabled) {
    mOnDemandTraceEnabled = enable;
    if (mOnDemandTraceEnabled) {
      CarbonObjectID *model = getModel();
      if (model != NULL) {
        if (mOnDemandTraceFileWriter == NULL) {
          INFO_ASSERT(!mTraceFile.empty(), "Trace filename has not been set");
          mOnDemandTraceFileWriter = new OnDemandTraceFileWriter(mTraceFile.c_str());
          mOnDemandTraceFileWriter->addModel(getName(), model);
        }
        mOnDemandTraceFileWriter->putSimStartTimestamp(mSystem->getSimStartTimestamp());
        mOnDemandTraceFileWriter->enable();
      }
    } else if (mOnDemandTraceFileWriter != NULL) {
      mOnDemandTraceFileWriter->disable();
    }
  }
}

void CarbonSystemComponent::putSimStartTimestamp(UInt64 timestamp) {
  if (mOnDemandTraceFileWriter != NULL) {
    mOnDemandTraceFileWriter->putSimStartTimestamp(timestamp);
  }
}

void CarbonSystemComponent::updateOnDemandTrace() {
  if ((mOnDemandTraceFileWriter != NULL) && mOnDemandTraceEnabled) {
    mOnDemandTraceFileWriter->flush();
  }
}

bool CarbonSystemComponent::getOnDemandTrace() const
{
  return mOnDemandTraceEnabled;
}

void CarbonSystemComponent::addOnDemandIdleDeposit(const char* signalName)
{
  if (mOnDemandIdleDepositSignals.insertWithCheck(signalName)) {
    CarbonObjectID *model = getModel();
    if (model != NULL) {
      CarbonNetID *net = carbonFindNet(model, signalName);
      if (net != NULL) {
        carbonOnDemandMakeIdleDeposit(model, net);
      }
    }
  }
}

void CarbonSystemComponent::clearOnDemandIdleDeposit()
{
  // There's no way to undo this in the model, so just update our
  // state so the .csu file is correct.
  mOnDemandIdleDepositSignals.clear();
}

void CarbonSystemComponent::addOnDemandExcluded(const char* signalName)
{
  if (mOnDemandExcludedSignals.insertWithCheck(signalName)) {
    CarbonObjectID *model = getModel();
    if (model != NULL) {
      CarbonNetID *net = carbonFindNet(model, signalName);
      if (net != NULL) {
        carbonOnDemandExclude(model, net);
      }
    }
  }
}

void CarbonSystemComponent::clearOnDemandExcluded()
{
  // There's no way to undo this in the model, so just update our
  // state so the .csu file is correct.
  mOnDemandExcludedSignals.clear();
}

CarbonSystemComponent& CarbonSystemComponent::operator=(const CarbonSystemComponent& src) {
  if (&src != this) {
    mName = src.mName;
    mModel = src.mModel;
    mReplayInfo = src.mReplayInfo;
    mReplayState = src.mReplayState;
    mOnDemandState = src.mOnDemandState;
    mEnabled = src.mEnabled;
    mSchedCalls = src.mSchedCalls;
    mNumCheckpoints = src.mNumCheckpoints;
  }
  return *this;
}

bool CarbonReplaySystem::processCmdline(int* argc, char** argv,
                                        bool reInitOptions,
                                        bool fromMain)
{
  UtString old_state, new_state;
  generateCmdline(&old_state);

  declareCmdlineOptions(reInitOptions);

  mArgvBuffer.putArgc(0);
  ArgProc::ParseStatusT status =
    mArgs->preParseCommandLine(*argc, argv, &mArgvBuffer, &mErrmsg);
  if (status == ArgProc::eParseError) {
    return false;
  }
  
#if REPLAY_USE_ENV_VAR
  if (! mReplayCmdEnv.empty())
  {
    if (mArgs->tokenizeArgString(mReplayCmdEnv.c_str(), &mArgvBuffer, &mErrmsg) != ArgProc::eParsed)
      return false;
    // clear the ReplayCmdEnv so it doesn't affect future
    // processCmdLine calls
    mReplayCmdEnv.clear();
  }
#endif

  // update the argc and the argv passed in to the post-parsed
  // version.
  // First clear out the argv passed. 
  for (int i = 0; i < *argc; ++i)
    argv[i] = NULL;
  
  // Now set it to the mArgvBuffer
  *argc = mArgvBuffer.getArgc();
  char** tmpArgv = mArgvBuffer.getArgv();
  for (int i = 0; i < *argc; ++i)
    argv[i] = tmpArgv[i];
  
  int numOptions;
  status = mArgs->parseCommandLine(argc, argv, &numOptions, &mErrmsg);
  if (status == ArgProc::eParseError) {
    return false;
  }

  // If the user specifies any command-line arguments, then this
  // overrides any .csu file that was present at the time we
  // initialized the model.  So let's establish a timestamp for that file,
  // so that subsequent calls to readCmdline(true) will ignore the file
  // unless it is updated.
  if (fromMain && (status == ArgProc::eParsed)) {
    status = ArgProc::eNotParsed;
    for (UtStringSet::iterator p = mCmdArgs.begin(), e = mCmdArgs.end();
         (p != e) && (status == ArgProc::eNotParsed); ++p)
    {
      const UtString& arg = *p;
      status = mArgs->isParsed(arg.c_str());
    }

    // We found the user specified a replay option on the command line.
    // So that means that any lingering .csu file should be ignored,
    // so grab its date if there is one.
    if (status == ArgProc::eParsed) {
      UtString fname(mSystemName), errmsg;
      fname << CARBON_STUDIO_USER_EXT;
      OSStatEntry statEntry;
      OSStatFileEntry(fname.c_str(), &statEntry, &errmsg);
      if (statEntry.exists()) {
        mUserReadTime = statEntry.getModTime();
      }
    }
  }

  mRecordAll = mArgs->getBoolValue("-record");
  mPlaybackAll = mArgs->getBoolValue("-playback");
  mVerbose = mArgs->getBoolValue("-verboseReplay");
  mSyncGUI = mArgs->getBoolValue("-replayGUI") ||
    mArgs->getBoolValue(CARBON_GUI_FLAG);
  bool carbonGUI = mArgs->getBoolValue("-carbonGUI");

  // Be compatable with older switch
  if (carbonGUI || getenv(CARBON_MODELSTUDIO_SIM) != NULL)
    mSyncGUI = true;

  mRecordComponents.clear();
  mPlaybackComponents.clear();
  mNormalComponents.clear();
  if (mRecordAll && mPlaybackAll) {
    mErrmsg << "-record and -playback are mutually exclusive";
    mRecordAll = false;
    mPlaybackAll = false;
    return false;
  }
  SInt32 interval;
  if (mArgs->getIntLast("-recordInterval", &interval) == ArgProc::eKnown) {
    mMinInterval = interval;
  }
  if (mArgs->getIntLast("-recoverPercentage", &interval) == ArgProc::eKnown) {
    mRecoverPercentage = interval;
  }
  ArgProc::StrIter strs;
  if ((mArgs->getStrIter("-recordComponent", &strs) == ArgProc::eKnown) &&
      !strs.atEnd())
  {
    if (mPlaybackAll) {
      mErrmsg << "-recordComponent and -playback are mutually exclusive";
      mPlaybackAll = false;
      return false;
    }
    for (; !strs.atEnd(); ++strs) {
      // validate components now?
      mRecordComponents.insert(*strs);
    }
  }
  if ((mArgs->getStrIter("-playbackComponent", &strs) == ArgProc::eKnown) &&
      !strs.atEnd())
  {
    if (mRecordAll) {
      mErrmsg << "-playbackComponent and -record are mutually exclusive";
      mPlaybackAll = false;
      return false;
    }
    if (! mRecordComponents.empty()) {
      mErrmsg
        << "-playbackComponent and -recordComponent are mutually exclusive";
      mRecordComponents.clear();
      return false;
    }
    for (; !strs.atEnd(); ++strs) {
      // validate components now?
      mPlaybackComponents.insert(*strs);
    }
  }
  const char* db = mArgs->getStrLast("-replayDatabase");
  if (db != NULL) {
    UInt32 sz = strlen(db);
    if ((sz < 5) || (strcmp(db + sz - 4, ".rdb") != 0)) {
      mErrmsg << "-replayDatabase directory " << db << " must end in .rdb";
      return false;
    }

    // If we are playing back, then the directory must exist
    if (mPlaybackAll || !mPlaybackComponents.empty()) {
      UtString result;
      if (OSStatFile(db, "rd", &result) != 1) {
        mErrmsg << "-replayDatabase directory " << db
                << " must exist for -playback";
        return false;
      }
    }

    // If we are recording back, then the directory must be writable
    if (mRecordAll || !mRecordComponents.empty()) {
      UtString result;
      if ((OSStatFile(db, "wd", &result) != 1) // does not exist
          && (OSMkdir(db, 0777, &result) < 0)) // fail to create it
      {
        mErrmsg << "-replayDatabase directory " << db
                << " must be writable for -record";
        return false;
      }
    }

    mDatabaseDirectory = db;
  }

  SInt32 states;
  if (mArgs->getIntLast("-onDemandMaxStates", &states) == ArgProc::eKnown) {
    putOnDemandMaxStates(states);
  }

  SInt32 count;
  if (mArgs->getIntLast("-onDemandBackoffStates", &count) == ArgProc::eKnown) {
    putOnDemandBackoffStates(count);
  }

  SInt32 percent;
  if (mArgs->getIntLast("-onDemandBackoffDecayPercent", &percent) == ArgProc::eKnown) {
    putOnDemandBackoffDecayPercent(percent);
  }

  SInt32 max;
  if (mArgs->getIntLast("-onDemandBackoffMaxDecay", &max) == ArgProc::eKnown) {
    putOnDemandBackoffMaxDecay(max);
  }

  const char *strategy = mArgs->getStrLast("-onDemandBackoffStrategy");
  if (strategy != NULL) {
    CarbonOnDemandBackoffStrategyIO strategyIO;
    if (strategyIO.parseString(strategy)) {
      putOnDemandBackoffStrategy(strategyIO);
    }
  }

  mOnDemandStartComponents.clear();
  if ((mArgs->getStrIter("-onDemandStartComponent", &strs) == ArgProc::eKnown) && !strs.atEnd()) {
    for (; !strs.atEnd(); ++strs) {
      // should validate components, remove from stop components, but
      // hopefully this code will not live long
      mOnDemandStartComponents.insert(*strs);
    }
  }

  mOnDemandStopComponents.clear();
  if ((mArgs->getStrIter("-onDemandStopComponent", &strs) == ArgProc::eKnown) && !strs.atEnd()) {
    for (; !strs.atEnd(); ++strs) {
      // should validate components, remove from start components, but
      // hopefully this code will not live long
      mOnDemandStopComponents.insert(*strs);
    }
  }

  // There is no -onDemandDisableTraceComponent, so we set the system
  // value first, and then override.
  bool onDemandTrace = mArgs->getBoolValue("-onDemandEnableTrace");
  putOnDemandTrace(onDemandTrace);

  if ((mArgs->getStrIter("-onDemandEnableTraceComponent", &strs) == ArgProc::eKnown) && !strs.atEnd()) {
    for (; !strs.atEnd(); ++strs) {
      putOnDemandTrace(*strs, true);
    }
  }

  // Handle component-specific value switches, which use a custom argument processing class
  CarbonSystemComponentSettings componentSettings;
  componentSettings.addIntSwitch("-onDemandMaxStatesComponent");
  componentSettings.addStringSwitch("-onDemandBackoffStrategyComponent");
  componentSettings.addIntSwitch("-onDemandBackoffStatesComponent");
  componentSettings.addIntSwitch("-onDemandBackoffDecayPercentComponent");
  componentSettings.addIntSwitch("-onDemandBackoffMaxDecayComponent");
  componentSettings.addStringSwitch("-onDemandIdleDeposit");
  componentSettings.addStringSwitch("-onDemandExcluded");
  if (componentSettings.parseArgs(argc, argv)) {
    // Loop over all the components with settings
    for (CarbonSystemComponentSettings::CompSwitchLoop l = componentSettings.loopCompSwitches();
         !l.atEnd(); ++l) {
      const UtString &compStr = l.getKey();
      CarbonSystemComponent *comp = findComponent(compStr.c_str());
      if (comp != NULL) {
        UInt32 val;
        const UtStringArray *valArray;
        if (CarbonSystemComponentSettings::sGetIntSwitchVal(l, "-onDemandMaxStatesComponent", &val)) {
          comp->putOnDemandMaxStates(val);
        }
        if (CarbonSystemComponentSettings::sGetStringSwitchValArray(l, "-onDemandBackoffStrategyComponent", &valArray)) {
          // Last one wins here
          const UtString &strategy = valArray->back();
          CarbonOnDemandBackoffStrategyIO strategyIO;
          if (strategyIO.parseString(strategy.c_str())) {
            comp->putOnDemandBackoffStrategy(strategyIO);
          }
        }
        if (CarbonSystemComponentSettings::sGetIntSwitchVal(l, "-onDemandBackoffStatesComponent", &val)) {
          comp->putOnDemandBackoffStates(val);
        }
        if (CarbonSystemComponentSettings::sGetIntSwitchVal(l, "-onDemandBackoffDecayPercentComponent", &val)) {
          comp->putOnDemandBackoffDecayPercent(val);
        }
        if (CarbonSystemComponentSettings::sGetIntSwitchVal(l, "-onDemandBackoffMaxDecayComponent", &val)) {
          comp->putOnDemandBackoffMaxDecay(val);
        }
        if (CarbonSystemComponentSettings::sGetStringSwitchValArray(l, "-onDemandIdleDeposit", &valArray)) {
          // Do all signals in the array
          for (UInt32 i = 0; i < valArray->size(); ++i) {
            const UtString &sig = (*valArray)[i];
            comp->addOnDemandIdleDeposit(sig.c_str());
          }
        }
        if (CarbonSystemComponentSettings::sGetStringSwitchValArray(l, "-onDemandExcluded", &valArray)) {
          // Do all signals in the array
          for (UInt32 i = 0; i < valArray->size(); ++i) {
            const UtString &sig = (*valArray)[i];
            comp->addOnDemandExcluded(sig.c_str());
          }
        }
      }
    }

#if 0
    // Print the options we found:
    for (CarbonSystemComponentSettings::CompSwitchLoop l = componentSettings.loopCompSwitches();
         !l.atEnd(); ++l) {
      const UtString &comp = l.getKey();
      UtIO::cout() << "Integer settings for component " << comp << ":" << UtIO::endl;
      for (CarbonSystemComponentSettings::IntSwitchLoop l2 = CarbonSystemComponentSettings::sLoopIntSwitches(&l);
           !l2.atEnd(); ++l2) {
        const UtString &switchName = l2.getKey();
        UInt32 switchVal = l2.getValue();
        UtIO::cout() << "  " << switchName << ": " << UtIO::dec << switchVal << UtIO::endl;
      }
      UtIO::cout() << "String settings for component " << comp << ":" << UtIO::endl;
      for (CarbonSystemComponentSettings::StringSwitchLoop l2 = CarbonSystemComponentSettings::sLoopStringSwitches(&l);
           !l2.atEnd(); ++l2) {
        const UtString &switchName = l2.getKey();
        UtStringArray *switchArray = l2.getValue();
        UtIO::cout() << "  " << switchName << ":" << UtIO::endl;
        for (UInt32 i = 0; i < switchArray->size(); ++i) {
          UtIO::cout() << "    " << (*switchArray)[i] << UtIO::endl;
        }
      }
    }

    UtIO::cout() << "Remaining arguments are:" << UtIO::endl;
    for (int i = 0; i < *argc; ++i) {
      UtIO::cout() << "  " << argv[i] << UtIO::endl;
    }
    UtIO::cout() << "done" << UtIO::endl;
#endif
  }

  // In System-C, our components don't know about each other, but each knows
  // about the command-line.  So they will all call carbonReplayProcessArgs,
  // which calls this method.  We only need to reset the simulation once,
  // however.
  generateCmdline(&new_state);
  if (new_state != old_state) {
    resetSimulation();
  }

  return true;
} // bool CarbonReplaySystem::processCmdline

bool CarbonReplaySystem::processCmdline(const char* cmd, bool fromMain) {
  UtStringArgv argv;

  // prepend a dummy token because argv[0] will be ignored during
  // option processing
  UtString buf;
  buf << "dummy " << cmd;

  declareCmdlineOptions(true);

  INFO_ASSERT(mArgs, cmd);
  ArgProc::ParseStatusT status =
    mArgs->tokenizeArgString(buf.c_str(), &argv, &mErrmsg);
  int argc = argv.getArgc();
  return (status != ArgProc::eParseError) &&
    processCmdline(&argc, argv.getArgv(), false, fromMain);
}

void CarbonReplaySystem::putArgProc(ArgProc* args) {
  if (mOwnArgs) {
    delete mArgs;
  }
  mArgs = args;
  mOwnArgs = false;
  mArgsDeclared = false;
  declareCmdlineOptions(true);
}

const char* CarbonReplaySystem::declareCmd(const char* arg) {
  mCmdArgs.insert(arg);
  return arg;
}

void CarbonReplaySystem::declareCmdlineOptions(bool reInitOptions) {

  // NOTE: If you add a section, you must add
  // a reset in the else clause below
  if (! mArgsDeclared)
  {
    mArgsDeclared = true;
    if (mArgs == NULL) {
      mArgs = new ArgProc;
      mOwnArgs = true;
    }
    
    mArgs->addBool(declareCmd("-record"), "Record all models",
                   false, ArgProc::cAllPasses);
    mArgs->addBool(declareCmd("-replayGUI"), "Pause simulation and wait for the user to confirm that Replay has been set up (same as -carbonGUI)",
                   false, ArgProc::cAllPasses);
    mArgs->addBool(declareCmd(CARBON_GUI_FLAG), "Pause simulation and wait for the user to confirm that Replay has been set up (same as -replayGUI)",
                   false, ArgProc::cAllPasses);
    mArgs->addBool(declareCmd("-playback"), "Playback all models",
                   false, ArgProc::cAllPasses);
    mArgs->addBool(declareCmd("-verboseReplay"),
                   "Log replay activity to stdout", false,
                   ArgProc::cAllPasses);
    
    mArgs->addString(declareCmd("-replayDatabase"),
                     "Replay database directory name (.rdb extension)",
                     "", true,  // no default
                     false,     // allow multiple
                     ArgProc::cAllPasses);
    mArgs->addInt(declareCmd("-recordInterval"), 
                  "Minimum number of schedule calls to run in between checkpoints",
                  0, true,        // no default
                  false,        // allow multiple
                  ArgProc::cAllPasses);
    mArgs->addInt(declareCmd("-recoverPercentage"), 
                 "Percentage number of simulation we are willing to backtrack on a divergence",
                 0, true,        // no default
                 false,        // allow multiple
                 ArgProc::cAllPasses);
    mArgs->addString(declareCmd("-recordComponent"),
                    "Record the specified component",
                    "", true,      // no default
                    true,      // allow multiple
                    ArgProc::cAllPasses);
    mArgs->addString(declareCmd("-playbackComponent"),
                    "Playback the specified component",
                    "", true,      // no default
                    true,      // allow multiple
                    ArgProc::cAllPasses);
    
    mArgs->createSection("Replay Control");
    for (UtStringSet::iterator p = mCmdArgs.begin(), e = mCmdArgs.end();
         p != e; ++p)
    {
      const UtString& arg = *p;
      mArgs->addToSection("Replay Control", arg.c_str());
    }

    mArgs->createSection("OnDemand Control");

    mArgs->addInt(declareCmd("-onDemandMaxStates"),
                  "Maximum number of states the OnDemand system will examine when looking for an idle pattern.",
                  0, true,  // no default
                  true,    // allow multiple
                  ArgProc::cAllPasses);
    mArgs->addToSection("OnDemand Control", "-onDemandMaxStates");

    mArgs->addString(declareCmd("-onDemandBackoffStrategy"),
                  "How the OnDemand system adjusts the search for idle patterns when one is not found. Valid values are [constant, decay]",
                  "", true,  // no default
                  true,    // allow multiple
                  ArgProc::cAllPasses);
    mArgs->addToSection("OnDemand Control", "-onDemandBackoffStrategy");

    mArgs->addInt(declareCmd("-onDemandBackoffStates"),
                  "The initial number of carbonSchedule calls to wait after a failed attempt to discover an idle state.",
                  0, true,  // no default
                  true,    // allow multiple
                  ArgProc::cAllPasses);
    mArgs->addToSection("OnDemand Control", "-onDemandBackoffStates");

    mArgs->addInt(declareCmd("-onDemandBackoffDecayPercent"),
                  "The percentage by which OnDemand will increase the number of states waited after a failed idle state search, before the search is resumed again.  Only used by the decay backoff strategy.",
                  0, true,  // no default
                  true,    // allow multiple
                  ArgProc::cAllPasses);
    mArgs->addToSection("OnDemand Control", "-onDemandBackoffDecayPercent");

    mArgs->addInt(declareCmd("-onDemandBackoffMaxDecay"),
                  "The maximum decay value, as a multiple of the Backoff Schedule Count (only used in decaying backoff)",
                  0, true,  // no default
                  true,    // allow multiple
                  ArgProc::cAllPasses);
    mArgs->addToSection("OnDemand Control", "-onDemandBackoffMaxDecay");

    mArgs->addString(declareCmd("-onDemandStartComponent"),
                    "Enable OnDemand for the specified component",
                    "", true,      // no default
                    true,      // allow multiple
                    ArgProc::cAllPasses);
    mArgs->addToSection("OnDemand Control", "-onDemandStartComponent");

    mArgs->addString(declareCmd("-onDemandStopComponent"),
                    "Disable OnDemand for the specified component",
                    "", true,      // no default
                    true,      // allow multiple
                    ArgProc::cAllPasses);
    mArgs->addToSection("OnDemand Control", "-onDemandStopComponent");

    mArgs->addBool(declareCmd("-onDemandEnableTrace"),
                   "Enable OnDemand event tracing for the system",
                   false, ArgProc::cAllPasses);
    mArgs->addToSection("OnDemand Control", "-onDemandEnableTrace");

    mArgs->addString(declareCmd("-onDemandEnableTraceComponent"),
                     "Enable OnDemand event tracing for the specified component",
                     "", true,      // no default
                     true,      // allow multiple
                     ArgProc::cAllPasses);
    mArgs->addToSection("OnDemand Control", "-onDemandEnableTraceComponent");

  }
  else if (reInitOptions)
  {
    // reset all the arguments
    mArgs->reInitSection("Replay Control");
    mArgs->reInitSection("OnDemand Control");
  }
} // void CarbonReplaySystem::declareCmdlineOptions

void CarbonReplaySystem::generateCmdline(UtString* cmd) {
  cmd->clear();
  if (mRecordAll) {
    *cmd << "-record\n";
  }
  else if (mPlaybackAll) {
    *cmd << "-playback\n";
  }
  else if (! mPlaybackComponents.empty()) {
    for (UtStringSet::SortedLoop p = mPlaybackComponents.loopSorted();
         !p.atEnd(); ++p)
    {
      *cmd << "-playbackComponent " << *p << "\n";
    }
  }
  else if (! mRecordComponents.empty()) {
    for (UtStringSet::SortedLoop p = mRecordComponents.loopSorted();
         !p.atEnd(); ++p)
    {
      *cmd << "-recordComponent " << *p << "\n";
    }
  }

  if (! mDatabaseDirectory.empty()) {
    *cmd << "-replayDatabase " << mDatabaseDirectory << "\n";
  }

  if (mMinInterval != 0) {
    *cmd << "-recordInterval " << mMinInterval << "\n";
  }
  if (mRecoverPercentage != 0) {
    *cmd << "-recordRecoverPercent " << mRecoverPercentage << "\n";
  }
  if (mVerbose) {
    *cmd << "-verboseReplay\n";
  }

  // TODO - only write out if not default
  for (ComponentLoop l = loopComponents(); !l.atEnd(); ++l) {
    const UtString &compName = l.getKey();
    const CarbonSystemComponent* comp = l.getValue();
    *cmd << "-onDemandMaxStatesComponent " << compName << " " << comp->getOnDemandMaxStates() << "\n";
    *cmd << "-onDemandBackoffStrategyComponent " << compName << " " << comp->getOnDemandBackoffStrategy().getString() << "\n";
    *cmd << "-onDemandBackoffStatesComponent " << compName << " " << comp->getOnDemandBackoffStates() << "\n";
    *cmd << "-onDemandBackoffDecayPercentComponent " << compName << " " << comp->getOnDemandBackoffDecayPercent() << "\n";
    *cmd << "-onDemandBackoffMaxDecayComponent " << compName << " " << comp->getOnDemandBackoffMaxDecay() << "\n";
    if (comp->getOnDemandTrace()) {
      *cmd << "-onDemandEnableTraceComponent " << compName <<  "\n";
    }
    // Write out all idle deposit and excluded signals
    for (CarbonSystemComponent::SignalLoop l = comp->loopOnDemandIdleDeposit(); !l.atEnd(); ++l) {
      const UtString &sig = *l;
      *cmd << "-onDemandIdleDeposit " << compName << " " << sig << "\n";
    }
    for (CarbonSystemComponent::SignalLoop l = comp->loopOnDemandExcluded(); !l.atEnd(); ++l) {
      const UtString &sig = *l;
      *cmd << "-onDemandExcluded " << compName << " " << sig << "\n";
    }
  }
  
  if (! mOnDemandStartComponents.empty()) {
    for (UtStringSet::SortedLoop p = mOnDemandStartComponents.loopSorted(); !p.atEnd(); ++p) {
      *cmd << "-onDemandStartComponent " << *p << "\n";
    }
  }
  if (! mOnDemandStopComponents.empty()) {
    for (UtStringSet::SortedLoop p = mOnDemandStopComponents.loopSorted(); !p.atEnd(); ++p) {
      *cmd << "-onDemandStopComponent " << *p << "\n";
    }
  }
}

bool CarbonReplaySystem::writeCmdline() {
  UtString cmd;
  generateCmdline(&cmd);
  UtString fname(mSystemName);
  fname << CARBON_STUDIO_USER_EXT;
  UtOBStream file(fname.c_str());
  file << cmd;
  return file.close();
}

CarbonSystemReadCmdlineStatus CarbonReplaySystem::readCmdline(bool onlyIfChanged) {
  UtString currentCmd, newCmd;
  generateCmdline(&currentCmd);
  UtString fname(mSystemName);
  fname << CARBON_STUDIO_USER_EXT;

  bool re_init_on_demand_event = false;

  if (mSyncGUI && !mGUIWasSynched) {
    mGUIWasSynched = true;      // prevent from running a second time

    if (! checkActiveCControl(true) && ! startCControl(60)) { // 1 minute
      return eCarbonSystemCmdError;
    }
    if (! waitForCControlSimStart()) {  // waits indefinitely
      return eCarbonSystemCmdError;
    }

    // If the user has synched with the GUI, then we can override the
    // API request to do a timestamp check
    onlyIfChanged = false;
    re_init_on_demand_event = true;
  }

  if (onlyIfChanged) {
    OSStatEntry statEntry;
    UtString errmsg;
    OSStatFileEntry(fname.c_str(), &statEntry, &errmsg);
    if (statEntry.exists() &&
        statEntry.getModTime() != mUserReadTime) {
      mUserReadTime = statEntry.getModTime();
    }
    else {
      return eCarbonSystemUnchanged;
    }
  }

  UtIBStream file(fname.c_str());
  UtString line;
  while (file.getline(&line)) {
    newCmd << line;
  }
  if (file.bad()) {
    mErrmsg = file.getErrmsg();
    return eCarbonSystemCmdError;
  }
  
  if (newCmd != currentCmd) {
    if (!processCmdline(newCmd.c_str(), false)) {
      return eCarbonSystemCmdError;
    }

    resetSimulation();
  }
  
  if (re_init_on_demand_event) {
    resetOnDemandFirstEventFlags();
  }
  
  return eCarbonSystemChanged;
}

#define CCONTROL_EXT ".ccontrol"
#define REQUEST_EXT  ".simstart_request"
#define GRANT_EXT    ".simstart_grant"
#define CARBON_PROJECT "CARBON_PROJECT"
#define CARBON_MONITOR ".monitor"
#define CARBON_WINDOWS_MONDIR "C:\\.carbon"
#define MONITOR_FILE "ms.carbon."

// Determine whether ccontrol is alive for this system.  If so,
// ccontrol will have written its PID into that file, and we should
// double-check that it's alive by running "ps" on Unix.  On Windows,
// for now, we'll assume it's active.

// ModelStudio will indicate it's identity from and env. variable
// CARBON_MODELSTUDIO_PID
bool CarbonReplaySystem::checkActiveCControl(bool noisy) {
  bool is_active = false;

  const char* msPID = getenv(CARBON_MODELSTUDIO_PID);

  if (msPID)
  {
    if (noisy)
    {
      // ModelStudio defines CARBON_PROJECT env. var
      const char* carbonProjectDir = getenv(CARBON_PROJECT);
      if (carbonProjectDir)
      {
        UtString pidFile;
        pidFile << MONITOR_FILE << msPID;

#if pfWINDOWS
        UtString monitorDir;
        OSConstructFilePath(&monitorDir, CARBON_WINDOWS_MONDIR, CARBON_MONITOR);
#else
        UtString monitorDir;
        OSConstructFilePath(&monitorDir, carbonProjectDir, CARBON_MONITOR);
#endif
        UtString monitorFile;
        OSConstructFilePath(&monitorFile, monitorDir.c_str(), pidFile.c_str());

        UtString currDir;
        OSGetCurrentDir(&currDir);

        UtOBStream ob(monitorFile.c_str());
        if (ob.is_open()) {
          ob << "<Start>" << "\n";
          ob << "  <ProjectDir>" << carbonProjectDir << "</ProjectDir>\n";
          ob << "  <SystemName>" << mSystemName << "</SystemName>\n";
          ob << "  <SimulationPID>" << OSGetPid() << "</SimulationPID>\n";
          ob << "  <ModelStudioPID>" << msPID << "</ModelStudioPID>\n";
          ob << "  <CurrentDir>" << currDir.c_str() << "</CurrentDir>\n";
          ob << "  <Css>" << mSystemName << CARBON_STUDIO_SIMULATION_EXT << "</Css>\n";
          ob << "</Start>" << "\n";
          ob.close();
        }
        else {
          UtIO::cerr() << ob.getErrmsg() << "\n";
        }
      }
    }
    return true;
  }

  UtString ccontrol_file(mSystemName);
  ccontrol_file << CCONTROL_EXT;
  UtIBStream ib(ccontrol_file.c_str());
  if (ib.is_open()) {
    // On Unix, check to see if this PID is an active process.
    // On Windows, assume it's active
    UInt32 pid;
    if (ib >> pid) {
#if pfWINDOWS
      // I don't know how to run the equiv of "ps" on Windows, and for
      // now this functionality is mainly needed for Coware which is
      // Unix-only.  In any event, the fallback position that we assume
      // the PID is valid is probably OK.
      is_active = true;
#endif
#if pfUNIX
      // Check to see if the PID in the semaphor file is valid. 
      // Don't be fooled if ccontrol crashes or is killed with the
      // kill command.
      UtString cmd, errmsg;
      cmd << "/bin/ps " << pid << " >/dev/null";
      int ret = OSSystem(cmd.c_str(), &errmsg);
      if (ret == 0) {
        is_active = true;
      }
      else {
        // remove any stray ccontrol file left behind by ccontrol crashing
        (void) OSUnlink(ccontrol_file.c_str(), &errmsg);
      }
#endif
    }
    if (is_active && noisy) {
      UtIO::cout() << "ModelStudio is running on " << mSystemName
                   << " in pid " << pid << "\n";
    }
  } // if
  return is_active;
} // bool CarbonReplaySystem::checkActiveCControl

// write the PID file from ccontrol so checkActiveCControl can find it from
// the simulation process
void CarbonReplaySystem::writeCControlActiveFile() {
  UtString ccontrol_file(mSystemName);
  ccontrol_file << CCONTROL_EXT;
  UtOBStream ob(ccontrol_file.c_str());
  if (ob.is_open()) {
    ob << OSGetPid() << "\n";
  }
  else {
    UtIO::cerr() << ob.getErrmsg() << "\n";
  }
}
  
void CarbonReplaySystem::clearCControlActiveFile() {
  UtString ccontrol_file(mSystemName), err;
  ccontrol_file << CCONTROL_EXT;
  OSUnlink(ccontrol_file.c_str(), &err);
}

void CarbonReplaySystem::writeSimGrant() {
  // Before we write the grant, we need to remove the request so
  // that we don't think it's another request.  
  UtString request_file(mSystemName), err;
  request_file << REQUEST_EXT;
  if (OSUnlink(request_file.c_str(), &err) != 0) {
    // Note that this might fail if the writer hasn't finished closing
    // it yet, so keep trying until it succeeds.
    do {
      OSSleep(1);
    } while (OSUnlink(request_file.c_str(), &err) != 0);
  }

  UtString grant_file(mSystemName);
  grant_file << GRANT_EXT;
  UtOBStream ob(grant_file.c_str());
  if (! ob.close()) {
    UtIO::cerr() << ob.getErrmsg() << "\n";
  }
}
  
bool CarbonReplaySystem::checkSimRequest() {
  UtString request_file(mSystemName), err;
  request_file << REQUEST_EXT;
  return OSStatFile(request_file.c_str(), "r", &err) == 1;
}

bool CarbonReplaySystem::startCControl(UInt32 timeout_seconds)
{
  UtString cmd, css_file, errmsg, ccontrol_file, err, ccontrol_exe;

  const char* msPID = getenv(CARBON_MODELSTUDIO_PID);
  if (msPID)
    return true;

  ccontrol_exe = "modelstudio";

  css_file << mSystemName << CARBON_STUDIO_SIMULATION_EXT;
#if pfUNIX
  cmd << "$CARBON_HOME/bin/" << ccontrol_exe << " " << css_file;
#endif
#if pfWINDOWS
  const char* chome = getenv("CARBON_HOME");
  if (chome == NULL) {
    mErrmsg = "$CARBON_HOME not defined";
    return false;
  }
  cmd << chome << "\\Win\\bin\\" << ccontrol_exe << " " << css_file;
#endif

  if (OSSystemBackground(cmd.c_str(), &mErrmsg) < 0) {
    return false;
  }

  // Wait for the .ccontrol file to show up
  ccontrol_file << mSystemName << CCONTROL_EXT;
  for (UInt32 i = 0; i < timeout_seconds; ++i) {
    if (OSStatFile(ccontrol_file.c_str(), "r", &mErrmsg) == 1) {
      return true;
    }
    OSSleep(1);
  }

  mErrmsg = "Timeout waiting for ModelStudio to start\n";
  return false;
} // bool CarbonReplaySystem::startCControl

bool CarbonReplaySystem::waitForCControlSimStart() {
  // Send a message to ccontrol indicating that a simulation is starting,
  // by writing SYSTEM.simstart
  UtString request_file(mSystemName), grant_file(mSystemName), err;
  request_file << REQUEST_EXT;
  grant_file << GRANT_EXT;

  // Before writing the request, remove any stray grant file left
  // behind for any reason
  (void) OSUnlink(grant_file.c_str(), &err);

  UtOBStream ob(request_file.c_str());
  if (!ob.close()) {
    return false;
  }

  // Write a message.  We should probably not do this directly to
  // stderr, but instead go through MsgContext so it could be redirected,
  // say, to the maxsim console.
  UtIO::cerr() << "Waiting for \"Continue Simulation\" button press in ModelStudio...\n";

  // Now we've written & closed this file, we have to wait indefinitely
  // for the user to press "Start Sim", or for the GUI to exit.  On
  // Unix the GUI check requires running /bin/ps.  Is that too expensive
  // to do every second?  I'm going to try it that way.
  //
  // We know the GUI has pressed "Sim Start" because the GUI will write
  // SYSTEM.systart_grant
  while (OSStatFile(grant_file.c_str(), "r", &err) != 1) {
    // Quietly check to see if ccontrol died
    if (! checkActiveCControl(false)) {
      (void) OSUnlink(request_file.c_str(), &err);
      mErrmsg = "ModelStudio exited before user pressed \"Continue Sim\"\n";
      return false;
    }
    OSSleep(1);
  }

  // Now that the grant has been issued, remove it so we don't think
  // it was already granted next time we do a request
  if (OSUnlink(grant_file.c_str(), &err) != 0) {
    // Note that this might fail if the writer hasn't finished closing
    // it yet, so keep trying until it succeeds.
    do {
      OSSleep(1);
    } while (OSUnlink(grant_file.c_str(), &err) != 0);
  }

  return true;
} // bool CarbonReplaySystem::waitForCControlSimStart

void CarbonReplaySystem::resetOnDemandFirstEventFlags() {
  // dummy function so the GUI can link.  Overridden by CarbonSystemSim.
}

void CarbonReplaySystem::detectSimRestart()
{
  // Before adding new events, we need to see if the simulation has
  // restarted, in which case we need to start from scratch.  When the
  // ReplaySystem is created, or when its simulation exits, the
  // timestamp is reset to 0.  If that's the case, update the
  // timestamp and clear all previous events.
  if (mSimStartTimestamp == 0) {
    putSimStartTimestamp(OSGetSecondsSince1970());
    clearEvents();
  }
}

void CarbonReplaySystem::resetSimulation() {
}

void CarbonReplaySystem::clearRequests() {
  mPlaybackComponents.clear();
  mRecordComponents.clear();
  mNormalComponents.clear();
  mRecordAll = false;
  mPlaybackAll = false;
}

void CarbonReplaySystem::recordAll() {
  clearRequests();
  mRecordAll = true;
}

void CarbonReplaySystem::playbackAll() {
  clearRequests();
  mPlaybackAll = true;
}

void CarbonReplaySystem::recordComponent(const char* name) {
  mRecordComponents.insert(name);
  mPlaybackAll = false;
  mRecordAll = mRecordComponents.size() == mComponents.size();
  mPlaybackComponents.erase(name);
  mNormalComponents.erase(name);
}

void CarbonReplaySystem::playbackComponent(const char* name) {
  mPlaybackComponents.insert(name);
  mRecordAll = false;
  mPlaybackAll = mPlaybackComponents.size() == mComponents.size();
  mRecordComponents.erase(name);
  mNormalComponents.erase(name);
}

void CarbonReplaySystem::replayStopComponent(const char* name) {
  mNormalComponents.insert(name);
  mRecordAll = false;
  mPlaybackAll = false;
  mRecordComponents.erase(name);
  mPlaybackComponents.erase(name);
}

void CarbonReplaySystem::onDemandStartComponent(const char *name) {
  mOnDemandStartComponents.insert(name);
  mOnDemandStopComponents.erase(name);
}

void CarbonReplaySystem::onDemandStopComponent(const char *name) {
  mOnDemandStopComponents.insert(name);
  mOnDemandStartComponents.erase(name);
}

bool CarbonReplaySystem::isOnDemandStartRequested(const char* name) {
  return mOnDemandStartComponents.find(name) != mOnDemandStartComponents.end();
}

bool CarbonReplaySystem::isOnDemandStopRequested(const char* name) {
  return mOnDemandStopComponents.find(name) != mOnDemandStopComponents.end();
}

void CarbonReplaySystem::putOnDemandTrace(bool enable)
{
  for (NameComponentMap::SortedLoop p(mNameComponentMap.loopSorted()); !p.atEnd(); ++p) {
    CarbonSystemComponent* comp = p.getValue();
    comp->putOnDemandTrace(enable);
  }
}

void CarbonReplaySystem::putOnDemandTrace(const char* component_name, bool enable)
{
  CarbonSystemComponent* comp = findComponent(component_name);
  if (comp != NULL) {
    comp->putOnDemandTrace(enable);
  }
}

bool CarbonReplaySystem::getOnDemandTraceRequested(const char* component_name) const
{
  const CarbonSystemComponent* comp = findComponent(component_name);
  bool ret = false;
  if (comp != NULL) {
    ret = comp->getOnDemandTrace();
  }
  return ret;
}

void CarbonReplaySystem::putOnDemandTraceFile(const char* component_name, const char* fname)
{
  CarbonSystemComponent* comp = findComponent(component_name);
  if (comp != NULL) {
    comp->putOnDemandTraceFile(fname);
  }
}

const char *CarbonReplaySystem::getOnDemandTraceFile(const char* component_name) const
{
  const CarbonSystemComponent* comp = findComponent(component_name);
  const char *ret = NULL;
  if (comp != NULL) {
    ret = comp->getOnDemandTraceFile();
  }
  return ret;
}

CarbonReplaySystem::Event::Event(CarbonReplayEventType type,
                                 CarbonSystemComponent* component,
                                 UInt64 cycles,
                                 UInt64 totalSchedCalls, UInt64 schedCalls,
                                 double real, double user, double sys,
                                 double simtime):
  mType(type),
  mComponent(component),
  mCycles(cycles),
  mTotalSchedCalls(totalSchedCalls),
  mComponentSchedCalls(schedCalls),
  mReal(real),
  mUser(user),
  mSys(sys),
  mSimTime(simtime)
{
}

CarbonSystemComponent* CarbonReplaySystem::getComponent(UInt32 idx) const {
  if (idx < numComponents()) {
    return mComponents[idx];
  }
  return NULL;
}

UInt32 CarbonReplaySystem::numComponents() const {
  return mComponents.size();
}

CarbonReplaySystem::Event::Event(CarbonReplaySystem* rs, UtIStream& in)
  : mComponent(NULL)
{
  SInt32 compIndex;
  if (in >> mType >> compIndex >> mCycles 
      >> mTotalSchedCalls >> mComponentSchedCalls
      >> mReal >> mUser >> mSys >> mSimTime)
  {
    if (compIndex != -1) {
      mComponent = rs->getComponent(compIndex);
      if (mComponent == NULL) {
        in.reportError("Invalid component index");
      }
    }
  }
}

void CarbonReplaySystem::Event::write(UtOStream& out) {
  SInt32 compIndex = -1;
  if (mComponent != NULL) {
    compIndex = mComponent->getIndex();
  }
  out << "event" << mType.getString() << compIndex << mCycles
      << mTotalSchedCalls << mComponentSchedCalls << mReal << mUser << mSys
      << mSimTime << '\n';
}

void CarbonReplaySystem::addEvent(CarbonReplayEventType type,
                                  CarbonSystemComponent* comp,
                                  UInt64 cycles,
                                  double real, double user, double sys,
                                  double simtime)
{
  Event* event = new Event(type, comp, cycles, mTotalScheduleCalls,
                           comp->getNumScheduleCalls(), real, user, sys,
                           simtime);
  comp->putLastEvent(event);
  mEvents.push_back(event);
  if (type != eReplayEventUpdate) {
    mSimStatus = type;
  }
}

void CarbonReplaySystem::addModeChange(CarbonSystemComponent* comp,
                                       CarbonReplayEventType type,
                                       UInt64 cycles,
                                       double real, double user, double sys,
                                       double simtime)
{
  Event* event = new Event(type, comp, cycles, mTotalScheduleCalls,
                           comp->getNumScheduleCalls(), real, user, sys,
                           simtime);
  if (comp != NULL) {
    comp->putLastEvent(event);
  }
  mEvents.push_back(event);
}



CarbonSystemComponentSettings::CarbonSystemComponentSettings()
{
}

CarbonSystemComponentSettings::~CarbonSystemComponentSettings()
{
  for (CompSwitchLoop l = loopCompSwitches(); !l.atEnd(); ++l) {
    Switches *p = l.getValue();
    for (StringSwitchLoop l2 = sLoopStringSwitches(&l); !l2.atEnd(); ++l2) {
      UtStringArray *p2 = l2.getValue();
      delete p2;
    }
    delete p;
  }
}

void CarbonSystemComponentSettings::addIntSwitch(const char* switchName)
{
  mIntSwitches.insert(switchName);
}

void CarbonSystemComponentSettings::addStringSwitch(const char* switchName)
{
  mStringSwitches.insert(switchName);
}

bool CarbonSystemComponentSettings::parseArgs(int* argc, char** argv)
{
  bool success = true;

  // First, go through all the arguments, looking for switches we know about
  UInt32 numArgs = *argc;
  for (UInt32 i = 0; success && (i < numArgs); ++i) {
    const char *switchName = argv[i];
    // See if it's a known arg
    bool isInt = (mIntSwitches.count(switchName) != 0);
    bool isString = (mStringSwitches.count(switchName) != 0);
    if (isInt || isString) {
      // There must be two more arguments.  The first is the component
      // name, which is always a string.  The second is the value,
      // which be a string or an integer.
      const char *component = NULL;
      const char *valueStr = NULL;
      UInt32 value = 0;
      if ((i + 2) < numArgs) {
        component = argv[++i];
        valueStr = argv[++i];
        // If needed, convert the value to an integer
        if (isInt) {
          UtString argStr(valueStr);
          success = (argStr >> value);
        }
      } else {
        success = false;
      }

      if (success) {
        // Lookup/create an entry in the component map
        SwitchesPtr &switches = mCompSwitches.insertInit(component, NULL);
        if (switches == NULL) {
          switches = new Switches;
        }
        if (isInt) {
          // Save the integer value in the integer map for this
          // component's switches
          IntSwitchMap *switchMap = switches->getIntSwitches();
          (*switchMap)[switchName] = value;
        } else {
          // Arg is a string.  Lookup/create the array of string
          // values for this switch and component
          StringSwitchMap *switchMap = switches->getStringSwitches();
          StringArrayPtr &valArray = switchMap->insertInit(switchName, NULL);
          if (valArray == NULL) {
            valArray = new UtStringArray;
          }
          // Append the new string value
          valArray->push_back(valueStr);
        }
        // NULL out all three arguments in argv.  We'll collapse it later.
        argv[i - 2] = NULL;
        argv[i - 1] = NULL;
        argv[i] = NULL;
      }
    }
  }

  // Now, collapse argv
  UInt32 nullArgs = 0;
  for (UInt32 i = 0; i < numArgs; ++i) {
    if (argv[i] == NULL) {
      ++nullArgs;
    } else if (nullArgs != 0) {
      // Move this argument towards the beginning of the array by the
      // number of null slots ahead of it.
      argv[i - nullArgs] = argv[i];
    }
  }
  // Update argc, and we're done
  *argc = numArgs - nullArgs;

  return success;
}

CarbonSystemComponentSettings::IntSwitchLoop CarbonSystemComponentSettings::sLoopIntSwitches(CompSwitchLoop* compLoop)
{
  Switches *switches = compLoop->getValue();
  IntSwitchMap *switchMap = switches->getIntSwitches();
  return switchMap->loopSorted();
}

bool CarbonSystemComponentSettings::sGetIntSwitchVal(const CompSwitchLoop& compLoop, const char *switchName, UInt32* val)
{
  // Look up the switch name in the map referenced by the loop iterator
  const Switches *switches = const_cast<CompSwitchLoop&>(compLoop).getValue();
  const IntSwitchMap *switchMap = switches->getIntSwitches();
  IntSwitchMap::const_iterator iter = switchMap->find(switchName);
  if (iter == switchMap->end()) {
    return false;
  }
  *val = iter->second;
  return true;
}

CarbonSystemComponentSettings::StringSwitchLoop CarbonSystemComponentSettings::sLoopStringSwitches(CompSwitchLoop* compLoop)
{
  Switches *switches = compLoop->getValue();
  StringSwitchMap *switchMap = switches->getStringSwitches();
  return switchMap->loopSorted();
}

bool CarbonSystemComponentSettings::sGetStringSwitchValArray(const CompSwitchLoop& compLoop, const char *switchName, const UtStringArray** arr)
{
  // Look up the switch name in the map referenced by the loop iterator
  const Switches *switches = const_cast<CompSwitchLoop&>(compLoop).getValue();
  const StringSwitchMap *switchMap = switches->getStringSwitches();
  StringSwitchMap::const_iterator iter = switchMap->find(switchName);
  if (iter == switchMap->end()) {
    return false;
  }
  *arr = iter->second;
  return true;
}
