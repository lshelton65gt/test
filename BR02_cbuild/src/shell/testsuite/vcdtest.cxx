// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "shell/VcdFile.h"
#include "util/AtomicCache.h"
#include "util/UtStream.h"
#include "util/UtVector.h"
#include "util/UtIOStream.h"
#include "hdl/HdlId.h"


int main()
{
  AtomicCache strCache;
  // make 3 signal a.b, a.c, a.d, where a.b and a.d are aliases
  // a.b and a.d are scalars, a.c is a vector of 8 bits
  StringAtom* a = strCache.intern("a");
  StringAtom* b = strCache.intern("b");  
  StringAtom* c = strCache.intern("c");  
  StringAtom* d = strCache.intern("d");
  StringAtom* e = strCache.intern("e");
  
  UtString errMsg;
  VcdFile* outFile = VcdFile::open(VcdFile::eCreate, "vcdtest.dump", e1ns, &errMsg);
  if (! outFile)
  {
    UtIO::cerr() << errMsg;
    exit(3);
  }
  
  WaveScope* scope = outFile->attachScope(a, NULL, WaveScope::eModule, NULL, eLanguageTypeUnknown);
  UtVector<WaveHandle*> handles;
  HdlId info;
  handles.push_back(scope->addSignal(b, eVerilogTypeReg, eCarbonVarDirectionImplicit, info));
  info.setType(HdlId::eVectBitRange);
  info.setVectMSB(7);
  info.setVectLSB(0);
  handles.push_back(scope->addSignal(c, eVerilogTypeWire, eCarbonVarDirectionInput, info));
  info.setType(HdlId::eScalar);
  handles.push_back(scope->addSignal(d, eVerilogTypeWire, eCarbonVarDirectionOutput, info));  
  info.setType(HdlId::eScalar);
  handles.push_back(scope->addSignal(e, eVerilogTypeReal, eCarbonVarDirectionInout, info));
  
  handles[0]->addAlias(handles[2]);
  outFile->setInitialTime(10);
  
  if (outFile->closeHierarchy(&errMsg) != WaveDump::eOK)
  {
    UtIO::cerr() << errMsg;
    exit(3);
  }
  
  // set values advance time and close
  UtVector<WaveHandle::Value> values;
  values.push_back(handles[0]->getValue());
  values.push_back(handles[1]->getValue());  

  WaveHandle::Value inVal;
  inVal = values[0];
  strcpy(inVal, "1");
  outFile->addChanged(handles[0]);
  inVal = values[1];
  strcpy(inVal, "11111111");
  outFile->addChanged(handles[1]);

  double realVal = 3.1234567890987654321;
  
  outFile->addChangedWithDbl(handles[3], realVal);

  // This should not be called, but should have no effect
  outFile->addChanged(handles[2]);
  
  outFile->advanceTime(5);
  
  inVal = values[0];
  strcpy(inVal, "0");
  outFile->addChanged(handles[0]);
  inVal = values[1];
  strcpy(inVal, "00000000");
  outFile->addChanged(handles[1]);

  realVal = 90909090;
  outFile->addChangedWithDbl(handles[3], realVal);
  outFile->advanceTime(10);
  
  delete outFile;
  return 0;
}
