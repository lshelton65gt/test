# Mostly, testsuites are not built with MSVC.
# However, cfgtest is.

if(NOT(${MSVC}))
  add_executable(carbondbdump CarbonDBDump.cxx)
  add_executable(vcdtest vcdtest.cxx)
  
  target_link_libraries(carbondbdump
                        ${libCarbonStatic}
                        ${libCarbonAux}
                        carbonmem
                        ${XTRA_LIBS}
                        ${FLEXLM_APP_LIBFLAGS}
                        ${ZLIB}
                        )
  target_link_libraries(vcdtest
                        ${libCarbonStatic}
                        carbonmem
                        ${XTRA_LIBS}
                        ${FLEXLM_APP_LIBFLAGS}
                        ${ZLIB}
                        )
  add_dependencies(carbondbdump libCarbonTarget)
  add_dependencies(vcdtest libCarbonTarget)
  
  set_target_properties(carbondbdump
                        vcdtest
                        PROPERTIES
                        RUNTIME_OUTPUT_DIRECTORY ${CARBON_OBJ}
                        )
endif()

# This is built with MSVC on Windows.  However, it's not built at all
# on Linux64, because we don't support the cfg library.
if((NOT(${WIN32}) OR (${MSVC})) AND (NOT("${ARCH}" STREQUAL "Linux64")))
  add_executable(cfgtest cfgtest.cxx)
  if(${MSVC})
    target_link_libraries(cfgtest
                          cfg_msvc
                          ${libCarbonMsvc}
                          ${libCarbonAuxImport}
                          ${CARBON_QT_LIB}
                          ${XTRA_LIBS}
                          ${XML_LIB_PATH}
                          ${ZLIB}
                          )
    # Prefix hack so the .exe ends up in the base directory instead of
    # in Release, Debug, etc.
    set_target_properties(cfgtest PROPERTIES PREFIX ../)
  else()
    target_link_libraries(cfgtest
                          cfg
                          ${libCarbonStatic}
                          ${CARBON_QT_LIB}
                          ${XTRA_LIBS}
                          ${FLEXLM_APP_LIBFLAGS}
                          ${XML_LIB_PATH}
                          ${ZLIB}
                          )
  endif()
  add_dependencies(cfgtest libCarbonTarget)

  set_target_properties(cfgtest
                        PROPERTIES
                        RUNTIME_OUTPUT_DIRECTORY ${CARBON_OBJ}
                        )
endif()

