//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/UtIOStream.h"
#include "cfg/carbon_cfg.h"
#include <cstring>
#include <cassert>

#define DESC "now is the time\nfor all good men\nto come to the aid of their country"

void test_cmp(CarbonCfgID cfg) {
  assert(carbonCfgNumRTLPorts(cfg) == 7);


  // rst port
  CarbonCfgRTLPortID rtlPort = carbonCfgGetRTLPort(cfg, 0);
  assert(strcmp(carbonCfgRTLPortGetName(rtlPort), "rst") == 0);
  assert(carbonCfgRTLPortGetWidth(rtlPort) == 1);
  assert(carbonCfgRTLPortNumConnections(rtlPort) == 1);
  CarbonCfgRTLConnectionID conn = carbonCfgRTLPortGetConnection(rtlPort, 0);
  assert(carbonCfgRTLConnectionGetType(conn) == eCarbonCfgResetGen);
  CarbonCfgResetGenID rst = carbonCfgCastResetGen(conn);
  assert(rst);
  assert(carbonCfgResetGetActiveValue(rst) == 1);
  assert(carbonCfgResetGetInactiveValue(rst) == 0);

  // make sure invalid casts don't work
  assert(carbonCfgCastClockGen(conn) == NULL);
  assert(carbonCfgCastTie(conn) == NULL);
  assert(carbonCfgCastESLPort(conn) == NULL);
  assert(carbonCfgCastXtorConn(conn) == NULL);

  // clk port
  rtlPort = carbonCfgGetRTLPort(cfg, 1);
  assert(strcmp(carbonCfgRTLPortGetName(rtlPort), "clk") == 0);
  assert(carbonCfgRTLPortGetWidth(rtlPort) == 1);
  assert(carbonCfgRTLPortNumConnections(rtlPort) == 1);
  conn = carbonCfgRTLPortGetConnection(rtlPort, 0);
  assert(carbonCfgRTLConnectionGetType(conn) == eCarbonCfgClockGen);
  CarbonCfgClockGenID clk = carbonCfgCastClockGen(conn);
  assert(clk);
  assert(carbonCfgClockGetInitialVal(clk) == 0);
  assert(carbonCfgClockGetDelay(clk) == 200);

  // make sure invalid casts don't work
  assert(carbonCfgCastResetGen(conn) == NULL);
  assert(carbonCfgCastTie(conn) == NULL);
  assert(carbonCfgCastESLPort(conn) == NULL);
  assert(carbonCfgCastXtorConn(conn) == NULL);

  // tied scanclk
  rtlPort = carbonCfgGetRTLPort(cfg, 2);
  assert(strcmp(carbonCfgRTLPortGetName(rtlPort), "scanclk") == 0);
  assert(carbonCfgRTLPortGetWidth(rtlPort) == 1);
  assert(carbonCfgRTLPortNumConnections(rtlPort) == 1);
  conn = carbonCfgRTLPortGetConnection(rtlPort, 0);
  assert(carbonCfgRTLConnectionGetType(conn) == eCarbonCfgTie);
  CarbonCfgTieID tie = carbonCfgCastTie(conn);
  assert(tie);
  assert(carbonCfgTieGetNumWords(tie) == 1);
  assert(carbonCfgTieGetWord(tie, 0) == 0);

  // make sure invalid casts don't work
  assert(carbonCfgCastResetGen(conn) == NULL);
  assert(carbonCfgCastClockGen(conn) == NULL);
  assert(carbonCfgCastESLPort(conn) == NULL);
  assert(carbonCfgCastXtorConn(conn) == NULL);

  // disconnected scan-out
  rtlPort = carbonCfgGetRTLPort(cfg, 3);
  assert(strcmp(carbonCfgRTLPortGetName(rtlPort), "scanout") == 0);
  assert(carbonCfgRTLPortGetWidth(rtlPort) == 1);
  assert(carbonCfgRTLPortNumConnections(rtlPort) == 0);

  // ESL input port
  rtlPort = carbonCfgGetRTLPort(cfg, 4);
  assert(strcmp(carbonCfgRTLPortGetName(rtlPort), "data_in") == 0);
  assert(carbonCfgRTLPortGetWidth(rtlPort) == 8);
  assert(carbonCfgRTLPortNumConnections(rtlPort) == 1);
  conn = carbonCfgRTLPortGetConnection(rtlPort, 0);
  assert(carbonCfgRTLConnectionGetType(conn) == eCarbonCfgESLPort);
  CarbonCfgESLPortID eslPort = carbonCfgCastESLPort(conn);
  assert(eslPort);
  assert(strcmp(carbonCfgESLPortGetName(eslPort), "edata_in") == 0);
  assert(carbonCfgESLPortGetType(eslPort) == eCarbonCfgESLInput);

  // make sure invalid casts don't work
  assert(carbonCfgCastResetGen(conn) == NULL);
  assert(carbonCfgCastClockGen(conn) == NULL);
  assert(carbonCfgCastTie(conn) == NULL);
  assert(carbonCfgCastXtorConn(conn) == NULL);

  // ESL output port with 2 fanouts
  rtlPort = carbonCfgGetRTLPort(cfg, 5);
  assert(strcmp(carbonCfgRTLPortGetName(rtlPort), "data_out") == 0);
  assert(carbonCfgRTLPortGetWidth(rtlPort) == 8);
  assert(carbonCfgRTLPortNumConnections(rtlPort) == 2);

  // output connection 1
  conn = carbonCfgRTLPortGetConnection(rtlPort, 0);
  assert(carbonCfgRTLConnectionGetType(conn) == eCarbonCfgESLPort);
  eslPort = carbonCfgCastESLPort(conn);
  assert(eslPort);
  assert(strcmp(carbonCfgESLPortGetName(eslPort), "edata_out") == 0);
  assert(carbonCfgESLPortGetType(eslPort) == eCarbonCfgESLOutput);

  // make sure invalid casts don't work
  assert(carbonCfgCastResetGen(conn) == NULL);
  assert(carbonCfgCastClockGen(conn) == NULL);
  assert(carbonCfgCastTie(conn) == NULL);
  assert(carbonCfgCastXtorConn(conn) == NULL);

  // output connection 2
  conn = carbonCfgRTLPortGetConnection(rtlPort, 1);
  assert(carbonCfgRTLConnectionGetType(conn) == eCarbonCfgESLPort);
  eslPort = carbonCfgCastESLPort(conn);
  assert(eslPort);
  assert(strcmp(carbonCfgESLPortGetName(eslPort), "fdata_out") == 0);
  assert(carbonCfgESLPortGetType(eslPort) == eCarbonCfgESLOutput);

  // make sure invalid casts don't work
  assert(carbonCfgCastResetGen(conn) == NULL);
  assert(carbonCfgCastClockGen(conn) == NULL);
  assert(carbonCfgCastTie(conn) == NULL);
  assert(carbonCfgCastXtorConn(conn) == NULL);

/*
  assert(strcmp(carbonCfgGetPort(cfg, 0), "input1") == 0);
  assert(strcmp(carbonCfgIOGetComment(cfg, "input1"), "input # 1") == 0);
  assert(carbonCfgIOGetWidth(cfg, "input1") == 2);
  assert(strcmp(carbonCfgGetPort(cfg, 1), "output1") == 0);
  assert(strcmp(carbonCfgGetPort(cfg, 2), "input2") == 0);
  assert(strcmp(carbonCfgGetPort(cfg, 3), "output2") == 0);
  assert(strcmp(carbonCfgGetPort(cfg, 4), "x.clk1") == 0);
  assert(strcmp(carbonCfgGetPort(cfg, 5), "x.clk2") == 0);
  assert(strcmp(carbonCfgGetPort(cfg, 6), "reset1") == 0);
  assert(strcmp(carbonCfgGetPort(cfg, 7), "ahb1") == 0);
  assert(strcmp(carbonCfgGetPort(cfg, 8), "hresetn") == 0);
  assert(strcmp(carbonCfgGetPort(cfg, 9), "hclk") == 0);
  assert(strcmp(carbonCfgGetPort(cfg, 10), "hdata") == 0);
  assert(strcmp(carbonCfgGetDescription(cfg), DESC) == 0);
  assert(strcmp(carbonCfgGetCompName(cfg), "mycomp") == 0);
  assert(strcmp(carbonCfgGetLibName(cfg), "mylib") == 0);
  assert(carbonCfgNumDebugs(cfg) == 3);
  assert(strcmp(carbonCfgDebugGetSignal(cfg, 0), "a.b") == 0);
  assert(strcmp(carbonCfgDebugGetSignal(cfg, 1), "c.d") == 0);
  assert(strcmp(carbonCfgDebugGetSignal(cfg, 2), "e.f") == 0);
  assert(strcmp(carbonCfgDebugGetName(cfg, 0), "ab_debug") == 0);
  assert(strcmp(carbonCfgDebugGetName(cfg, 1), "cd_debug") == 0);
  assert(strcmp(carbonCfgDebugGetName(cfg, 2), "ef_debug") == 0);
  assert(strcmp(carbonCfgDebugGetGroup(cfg, 0), "regs") == 0);
  assert(strcmp(carbonCfgDebugGetGroup(cfg, 1), "regs") == 0);
  assert(strcmp(carbonCfgDebugGetGroup(cfg, 2), "regs") == 0);

  assert(carbonCfgNumMemories(cfg) == 1);
  assert(strcmp(carbonCfgMemoryGetSignal(cfg, 0), "\\)(*&') .g") == 0);
  assert(strcmp(carbonCfgMemoryGetName(cfg, 0), "e_g") == 0);
  assert(strcmp(carbonCfgMemoryGetInitFile(cfg, 0),
                "c:\\Program Files\\ file . dat") == 0);
  assert(carbonCfgMemoryGetMaxAddrs(cfg, 0) == 0xffff);
  assert(carbonCfgMemoryGetNumCycles(cfg, 0) == 2);
  assert(carbonCfgMemoryGetReadWrite(cfg, 0) == 3);
  assert(carbonCfgMemoryGetIsProgram(cfg, 0) == 1);
  assert(strcmp(carbonCfgMemoryGetComment(cfg, 0),
                "this is\nquite a comment") == 0);

  assert(carbonCfgGetPortType(cfg, "input1") == eCarbonCfgInput);
  assert(carbonCfgGetPortType(cfg, "output1") == eCarbonCfgOutput);
  assert(carbonCfgGetPortType(cfg, "input2") == eCarbonCfgInput);
  assert(carbonCfgGetPortType(cfg, "output2") == eCarbonCfgOutput);
  assert(carbonCfgGetPortType(cfg, "x.clk1") == eCarbonCfgClock);
  assert(carbonCfgGetPortType(cfg, "x.clk2") == eCarbonCfgClock);
  assert(carbonCfgGetPortType(cfg, "reset1") == eCarbonCfgReset);
  assert(carbonCfgGetPortType(cfg, "ahb1") == eCarbonCfgXtor);
  assert(carbonCfgGetPortType(cfg, "hresetn") == eCarbonCfgInput);
  assert(carbonCfgGetPortType(cfg, "hclk") == eCarbonCfgInput);
  assert(carbonCfgGetPortType(cfg, "hdata") == eCarbonCfgOutput);
*/
}

void test_build() {
  CarbonCfgID cfg = carbonCfgCreate();
  if (carbonCfgReadXtorLib(cfg, eCarbonXtorsMaxsim) == eCarbonCfgFailure) {
    fprintf(stderr, "%s\n", carbonCfgGetErrmsg(cfg));
    abort();
  }
  CarbonCfgRTLPortID rtlPort;

  rtlPort = carbonCfgAddRTLPort(cfg, "rst", 1, eCarbonCfgRTLInput);
  carbonCfgResetGen(rtlPort, 1, 0, 1, 1, 0, 1, 1);

  rtlPort = carbonCfgAddRTLPort(cfg, "clk", 1, eCarbonCfgRTLInput);
  carbonCfgClockGen(rtlPort, 0, 200, 1, 1, 25);

  rtlPort = carbonCfgAddRTLPort(cfg, "scanclk", 1, eCarbonCfgRTLInput);
  unsigned val = 0;
  carbonCfgTie(rtlPort, &val, 1);

  rtlPort = carbonCfgAddRTLPort(cfg, "scanout", 1, eCarbonCfgRTLInput);

  rtlPort = carbonCfgAddRTLPort(cfg, "data_in", 8, eCarbonCfgRTLInput);
  carbonCfgAddESLPort(cfg, rtlPort, "edata_in", eCarbonCfgESLInput);

  rtlPort = carbonCfgAddRTLPort(cfg, "data_out", 8, eCarbonCfgRTLOutput);
  carbonCfgAddESLPort(cfg, rtlPort, "edata_out", eCarbonCfgESLOutput);
  carbonCfgAddESLPort(cfg, rtlPort, "fdata_out", eCarbonCfgESLOutput);

  CarbonCfgXtor* xtype = carbonCfgFindXtor(cfg, "AHB_Slave");
  assert(xtype);
  CarbonCfgXtorPort* xport = carbonCfgXtorFindPort(xtype, "hwrite");
  assert(xport);
  CarbonCfgXtorInstance* xtor = carbonCfgAddXtor(cfg, "x1", xtype);
  rtlPort = carbonCfgAddRTLPort(cfg, "hwrite", 1, eCarbonCfgRTLOutput);
  carbonCfgConnectXtor(rtlPort, xtor, 2);

/*
  carbonCfgAddIO(cfg, eCarbonCfgInput, "input1", 2, NULL, NULL, "input # 1");
  carbonCfgAddIO(cfg, eCarbonCfgOutput, "output1", 3, NULL, NULL, "output # 1");
  carbonCfgAddIO(cfg, eCarbonCfgInput, "input2", 5, NULL, NULL, "input # 2");
  carbonCfgAddIO(cfg, eCarbonCfgOutput, "output2", 8, NULL, NULL, "output # 2");
  carbonCfgAddClock(cfg, "x.clk1", 0, 100, 100, 100, "clk1", "clock # 1");
  carbonCfgAddClock(cfg, "x.clk2", 1, 50, 75, 75, "clk2", "clock # 2");
  carbonCfgAddReset(cfg, "reset1", 100, 100, 1, "reset # 1");
  carbonCfgAddXtor(cfg, "ahb1", "AHB");
  carbonCfgAddIO(cfg, eCarbonCfgInput, "hresetn", 13, "ahb1", "hresetn", "");
  carbonCfgAddIO(cfg, eCarbonCfgInput, "hclk", 21,"ahb1", "hclk", "");
  carbonCfgAddIO(cfg, eCarbonCfgOutput,"hdata", 35, "ahb1", "hdata", "");
  carbonCfgPutDescription(cfg, DESC);
  carbonCfgPutCompName(cfg, "mycomp");
  carbonCfgPutLibName(cfg, "mylib");
  carbonCfgAddDebug(cfg, "a.b", "ab_debug", "regs", 8, 16, "hello, world!");
  carbonCfgAddDebug(cfg, "c.d", "cd_debug", "regs", 1, 2, "now is the time for all good men to come to the \"aid\" of their country");
  carbonCfgAddDebug(cfg, "e.f", "ef_debug", "regs", 3, 8, "(&\001\002)");
  carbonCfgAddMemory(cfg, "\\)(*&') .g", "e_g",
                     "c:\\Program Files\\ file . dat",
                     0xffff, 32, 2, 3, 1, "this is\nquite a comment");
*/

  carbonCfgWrite(cfg, "cfgtest.ccfg");

  test_cmp(cfg);
  carbonCfgDestroy(cfg);
}

void test_read() {
  CarbonCfgID cfg = carbonCfgCreate();
  if (carbonCfgReadXtorLib(cfg, eCarbonXtorsMaxsim) == eCarbonCfgFailure) {
    fprintf(stderr, "%s\n", carbonCfgGetErrmsg(cfg));
    abort();
  }
  if (carbonCfgRead(cfg, "cfgtest.ccfg") == eCarbonCfgFailure) {
    UtIO::cout() << carbonCfgGetErrmsg(cfg) << "\n";
    exit(1);
  }
  test_cmp(cfg);
  carbonCfgDestroy(cfg);
}  

void test_errors() {
  CarbonCfgID cfg = carbonCfgCreate();
  if (carbonCfgReadXtorLib(cfg, eCarbonXtorsMaxsim) == eCarbonCfgFailure) {
    fprintf(stderr, "%s\n", carbonCfgGetErrmsg(cfg));
    abort();
  }
  if (carbonCfgRead(cfg, "errors.ccfg") == eCarbonCfgFailure) {
    UtIO::cout() << carbonCfgGetErrmsg(cfg) << "\n";
  }
  else {
    UtIO::cout() << "Expected errors reading errors.cfg, but it worked\n";
  }
}

int main() {
  test_build();
  test_read();
  test_errors();
}

