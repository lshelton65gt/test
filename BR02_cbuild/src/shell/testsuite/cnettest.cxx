// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/UtString.h"
#include "util/UtIOStream.h"
#include "shell/ShellNet.h"
#include "shell/carbon_debug.h"


/*
**
** VECTOR DEPOSIT
**
*/
void test_vec_dp_int8_msb_gt_lsb()
{
  UInt8 src[1];
  UInt32 new_val[1];

  src[0] = 0x00;
  CarbonVector<UInt8, 3, -4>  src_net(src);

  UtIO::cout() << "vec: dp: int8: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x0000000f;
  src_net.depositRange(new_val, 3, 0);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  src_net.depositRange(new_val, 3, 0);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x000000aa;
  src_net.depositRange(new_val, -1, -4);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  src_net.depositRange(new_val, -1, -4);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x000000ff;
  src_net.depositRange(new_val, 1, -2);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;
}


void test_vec_dp_int8_msb_lt_lsb()
{
  UInt8 src[1];
  UInt32 new_val[1];

  src[0] = 0x00;
  CarbonVector<UInt8, -3, 4>  src_net(src);

  UtIO::cout() << "vec: dp: int8: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x0000000f;
  src_net.depositRange(new_val, -3, 0);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  src_net.depositRange(new_val, -3, 0);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x000000aa;
  src_net.depositRange(new_val, 1, 4);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  src_net.depositRange(new_val, 1, 4);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x000000ff;
  src_net.depositRange(new_val, -1, 2);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;
}


void test_vec_dp_int32_msb_gt_lsb()
{
  UInt32 src[1];
  UInt32 new_val[1];

  src[0] = 0x00000000;
  CarbonVector<UInt32, 14, -1>  src_net(src);

  UtIO::cout() << "vec: dp: int32: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x0000000f;
  src_net.depositRange(new_val, 12, 9);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  src_net.depositRange(new_val, 12, 9);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x000000aa;
  src_net.depositRange(new_val, 6, -1);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  src_net.depositRange(new_val, 6, -1);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x000000aa;
  src_net.depositRange(new_val, 4, -1);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;
}


void test_vec_dp_int32_msb_lt_lsb()
{
  UInt32 src[1];
  UInt32 new_val[1];

  src[0] = 0x00000000;
  CarbonVector<UInt32, -12, 2>  src_net(src);

  UtIO::cout() << "vec: dp: int32: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x0000000f;
  src_net.depositRange(new_val, -12, -9);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  src_net.depositRange(new_val, -12, -9);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x000000aa;
  src_net.depositRange(new_val, -6, 1);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  src_net.depositRange(new_val, -6, 1);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x000000aa;
  src_net.depositRange(new_val, -4, 1);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;
}


void test_vec_dp_int64_msb_gt_lsb()
{
  UInt64 src[1];
  UInt32 new_val[2];

  src[0] = 0x0000000000000000;
  CarbonVector<UInt64, 29, -6>  src_net(src);

  UtIO::cout() << "vec: dp: int64: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0xffffffff;
  new_val[1] = 0x0000000f;
  src_net.depositRange(new_val, 25, -6);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  new_val[1] = 0x00000000;
  src_net.depositRange(new_val, 25, -6);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x0000000f;
  new_val[1] = 0x00000000;
  src_net.depositRange(new_val, 27, 24);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x0000000a;
  new_val[1] = 0x00000000;
  src_net.depositRange(new_val, -3, -6);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;
}


void test_vec_dp_int64_msb_lt_lsb()
{
  UInt64 src[1];
  UInt32 new_val[2];

  src[0] = 0x0000000000000000;
  CarbonVector<UInt64, -29, 6>  src_net(src);

  UtIO::cout() << "vec: dp: int64: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0xffffffff;
  new_val[1] = 0x0000000f;
  src_net.depositRange(new_val, -25, 6);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  new_val[1] = 0x00000000;
  src_net.depositRange(new_val, -25, 6);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x0000000f;
  new_val[1] = 0x00000000;
  src_net.depositRange(new_val, -27, -24);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x0000000a;
  new_val[1] = 0x00000000;
  src_net.depositRange(new_val, 3, 6);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;
}


void test_vec_dp_int65_msb_gt_lsb()
{
  UInt32 src[3];
  UInt32 new_val[3];


  src[0] = 0x00000000;
  src[1] = 0x00000000;
  src[2] = 0x00000000;
  CarbonVector<BitVector<65>, 62, -2>  src_net(src);

  UtIO::cout() << "vec: dp: int65: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0xffffffff;
  new_val[1] = 0x00000000;
  new_val[2] = 0x0000000f;
  src_net.depositRange(new_val, 62, -2);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  new_val[1] = 0x00000000;
  new_val[2] = 0x00000000;
  src_net.depositRange(new_val, 62, -2);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000a0f;
  new_val[1] = 0xf0000000;
  new_val[2] = 0x0000000f;
  src_net.depositRange(new_val, 62, 61);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;


  new_val[0] = 0x00000a0f;
  new_val[1] = 0x00000000;
  new_val[2] = 0x00000000;
  src_net.depositRange(new_val, 9, -2);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;
}


void test_vec_dp_int65_msb_lt_lsb()
{
  UInt32 src[3];
  UInt32 new_val[3];

  src[0] = 0x00000000;
  src[1] = 0x00000000;
  src[2] = 0x00000000;
  CarbonVector<BitVector<65>, -62, 2>  src_net(src);

  UtIO::cout() << "vec: dp: int65: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0xffffffff;
  new_val[1] = 0x00000000;
  new_val[2] = 0x0000000f;
  src_net.depositRange(new_val, -62, 2);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  new_val[1] = 0x00000000;
  new_val[2] = 0x00000000;
  src_net.depositRange(new_val, -62, 2);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000a0f;
  new_val[1] = 0xf0000000;
  new_val[2] = 0x0000000f;
  src_net.depositRange(new_val, -62, -61);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;


  new_val[0] = 0x00000a0f;
  new_val[1] = 0x00000000;
  new_val[2] = 0x00000000;
  src_net.depositRange(new_val, -9, 2);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;
}


/*
**
** VECTOR EXAMINE
**
*/
void test_vec_ex_int8_msb_gt_lsb()
{
  UInt8 src[1];
  UInt32 dst[1];

  src[0] = 0x47;
  CarbonVector<UInt8, 6, -1>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 2, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int8: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, 1, -1);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 4, 2);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 6, 4);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  dst[0] = 0x00000000;
  src_net.examineRange(dst, 5, 5);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int8_msb_lt_lsb()
{
  UInt8 src[1];
  UInt32 dst[1];

  src[0] = 0x47;
  CarbonVector<UInt8, -6, 1>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 2, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int8: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, -1, 1);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -4, -2);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -6, -4);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  dst[0] = 0x00000000;
  src_net.examineRange(dst, 0, 0);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int8_msb_eq_lsb()
{
  UInt8 src[1];
  UInt32 dst[1];

  src[0] = 0xff;
  CarbonVector<UInt16, 0, 0>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 0, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int8: msb == lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, 0, 0);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int16_msb_gt_lsb()
{
  UInt16 src[1];
  UInt32 dst[1];

  src[0] = 0xa0ff;
  CarbonVector<UInt16, 14, -1>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 3, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int16: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, 2, -1);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 6, 3);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 10, 7);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 14, 11);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int16_msb_lt_lsb()
{
  UInt16 src[1];
  UInt32 dst[1];

  src[0] = 0xa0ff;
  CarbonVector<UInt16, -16, -1>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 3, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int16: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, -4, -1);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -8, -5);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -12, -9);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -16, -13);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int16_msb_eq_lsb()
{
  UInt16 src[1];
  UInt32 dst[1];

  src[0] = 0xa0ff;
  CarbonVector<UInt16, -1, -1>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 0, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int16: msb == lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, -1, -1);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int32_msb_gt_lsb()
{
  UInt32 src[1];
  UInt32 dst[1];

  src[0] = 0x0000a0ff;
  CarbonVector<UInt32, 14, -1>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 3, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int32: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, 2, -1);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 6, 3);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 10, 7);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 14, 11);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int32_msb_lt_lsb()
{
  UInt32 src[1];
  UInt32 dst[1];

  src[0] = 0x0000a0ff;
  CarbonVector<UInt32, -16, -1>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 3, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int32: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, -4, -1);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -8, -5);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -12, -9);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -16, -13);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int32_msb_eq_lsb()
{
  UInt32 src[1];
  UInt32 dst[1];

  src[0] = 0x0000a0ff;
  CarbonVector<UInt32, -1, -1>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 0, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int32: msb == lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, -1, -1);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int64_msb_gt_lsb()
{
  UInt64 src[1];
  UInt32 dst[1];

  src[0] = 0x000000f0f000a0ff;
  CarbonVector<UInt64, 38, -1>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 3, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int64: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, 2, -1);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 36, 33);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 14, 11);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 32, 29);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int64_msb_lt_lsb()
{
  UInt64 src[1];
  UInt32 dst[1];

  src[0] = 0x80a000f0f000a0ff;
  CarbonVector<UInt64, -63, 0>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 3, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int64: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, -63, -60);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -11, -8);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -55, -52);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -41, -38);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int64_msb_eq_lsb()
{
  UInt64 src[1];
  UInt32 dst[1];

  src[0] = 0xffffffffffffffff;
  CarbonVector<UInt64, -61, -61>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 0, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int64: msb == lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, -61, -61);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int65_msb_gt_lsb()
{
  UInt32 src[3];
  UInt32 dst[1];

  src[0] = 0xf000a0ff;
  src[1] = 0x50ff0000;
  src[2] = 0x00000001;
  CarbonVector<BitVector<65>, 62, -2>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 3, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int65: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, 1, -2);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 5, 2);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 9, 6);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 13, 10);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 61, 58);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 62, 59);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, 31, 28);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_vec_ex_int65_msb_lt_lsb()
{
  UInt32 src[3];
  UInt32 dst[1];

  src[0] = 0xf000a0ff;
  src[1] = 0x50ff0000;
  src[2] = 0x00000001;
  CarbonVector<BitVector<65>, -164, -100>  src_net(src);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 3, 0>  dst_net(dst);

  UtIO::cout() << "vec: ex: int65: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  UtString y("");
  src_net.examineRange(dst, -103, -100);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -111, -108);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -115, -112);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -163, -160);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_net.examineRange(dst, -164, -161);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}




/*
**
** MEMORY DEPOSIT
**
*/
void test_mem_dp_int8_msb_gt_lsb()
{
  UInt32 src[3];
  UInt32 new_val[1];

  Memory<UInt8, 8, 1, -1> mem;

  src[0] = 0x00000000;
  src[1] = 0x00000000;
  src[2] = 0x00000000;
  CarbonMemory<UInt8, 7, 0, 1, -1> src_vec(static_cast<void *>(&mem));
  src_vec.depositMemory(1, &src[0]);
  src_vec.depositMemory(0, &src[1]);
  src_vec.depositMemory(-1, &src[2]);


  UtIO::cout() << "mem: dp: int8: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  for (int i = 1; i > -2; i--)
  {
    src_vec.formatMemory(&x, CarbonNet::eBin, i);
    UtIO::cout() << x << UtIO::endl;
  }

  new_val[0] = 0x00000003;
  src_vec.depositMemoryRange(1, new_val, 1, 0);
  src_vec.depositMemoryRange(0, new_val, 3, 2);
  src_vec.depositMemoryRange(-1, new_val, 7, 6);
  src_vec.depositMemoryRange(-1, new_val, 5, 4);
  for (int i = 1; i > -2; i--)
  {
    src_vec.formatMemory(&x, CarbonNet::eBin, i);
    UtIO::cout() << x << UtIO::endl;
  }
}


void test_mem_dp_int8_msb_lt_lsb()
{
  UInt32 src[3];
  UInt32 new_val[1];

  Memory<UInt8, 8, 1, -1> mem;

  src[0] = 0x00000000;
  src[1] = 0x00000000;
  src[2] = 0x00000000;
  CarbonMemory<UInt8, -7, 0, 1, -1> src_vec(static_cast<void *>(&mem));
  src_vec.depositMemory(1, &src[0]);
  src_vec.depositMemory(0, &src[1]);
  src_vec.depositMemory(-1, &src[2]);


  UtIO::cout() << "mem: dp: int8: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  for (int i = 1; i > -2; i--)
  {
    src_vec.formatMemory(&x, CarbonNet::eBin, i);
    UtIO::cout() << x << UtIO::endl;
  }

  new_val[0] = 0x00000003;
  src_vec.depositMemoryRange(1, new_val, -1, 0);
  src_vec.depositMemoryRange(0, new_val, -3, -2);
  src_vec.depositMemoryRange(-1, new_val, -7, -6);
  src_vec.depositMemoryRange(-1, new_val, -5, -4);
  for (int i = 1; i > -2; i--)
  {
    src_vec.formatMemory(&x, CarbonNet::eBin, i);
    UtIO::cout() << x << UtIO::endl;
  }
}


void test_mem_dp_int65_msb_gt_lsb()
{
  UInt32 src[9];
  UInt32 new_val[2];

  Memory<BitVector<65>, 65, 1, -1> mem;

  src[0] = 0x00000000;
  src[1] = 0x00000000;
  src[2] = 0x00000000;

  src[3] = 0x00000000;
  src[4] = 0x00000000;
  src[5] = 0x00000000;

  src[6] = 0x00000000;
  src[7] = 0x00000000;
  src[8] = 0x00000000;

  CarbonMemory<BitVector<65>, 65, 1, 1, -1> src_vec(static_cast<void *>(&mem));
  src_vec.depositMemory(1, &src[0]);
  src_vec.depositMemory(0, &src[3]);
  src_vec.depositMemory(-1, &src[6]);


  UtIO::cout() << "mem: dp: int65: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  for (int i = 1; i > -2; i--)
  {
    src_vec.formatMemory(&x, CarbonNet::eBin, i);
    UtIO::cout() << x << UtIO::endl;
  }

  new_val[0] = 0x0fffffff;
  new_val[1] = 0xfffffff0;
  src_vec.depositMemoryRange(1, new_val, 65, 2);
  src_vec.depositMemoryRange(0, new_val, 64, 1);
  src_vec.depositMemoryRange(-1, new_val, 65, 62);
  src_vec.depositMemoryRange(-1, new_val, 57, 54);
  for (int i = 1; i > -2; i--)
  {
    src_vec.formatMemory(&x, CarbonNet::eBin, i);
    UtIO::cout() << x << UtIO::endl;
  }
}


void test_mem_dp_int65_msb_lt_lsb()
{
  UInt32 src[9];
  UInt32 new_val[2];

  Memory<BitVector<65>, 65, 1, -1> mem;

  src[0] = 0x00000000;
  src[1] = 0x00000000;
  src[2] = 0x00000000;

  src[3] = 0x00000000;
  src[4] = 0x00000000;
  src[5] = 0x00000000;

  src[6] = 0x00000000;
  src[7] = 0x00000000;
  src[8] = 0x00000000;

  CarbonMemory<BitVector<65>, -65, -1, 1, -1> src_vec(static_cast<void *>(&mem));
  src_vec.depositMemory(1, &src[0]);
  src_vec.depositMemory(0, &src[3]);
  src_vec.depositMemory(-1, &src[6]);


  UtIO::cout() << "mem: dp: int65: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  for (int i = 1; i > -2; i--)
  {
    src_vec.formatMemory(&x, CarbonNet::eBin, i);
    UtIO::cout() << x << UtIO::endl;
  }

  new_val[0] = 0x0fffffff;
  new_val[1] = 0xfffffff0;
  src_vec.depositMemoryRange(1, new_val, -65, -2);
  src_vec.depositMemoryRange(0, new_val, -64, -1);
  src_vec.depositMemoryRange(-1, new_val, -65, -62);
  src_vec.depositMemoryRange(-1, new_val, -57, -54);
  for (int i = 1; i > -2; i--)
  {
    src_vec.formatMemory(&x, CarbonNet::eBin, i);
    UtIO::cout() << x << UtIO::endl;
  }
}


/*
**
** MEMORY EXAMINE
**
*/
void test_mem_ex_int8_msb_gt_lsb()
{
  UInt32 src[3];
  UInt32 dst[1];

  Memory<UInt8, 8, 1, -1> mem;

  src[0] = 0x000000f0;
  src[1] = 0x0000003c;
  src[2] = 0x00000005;
  CarbonMemory<UInt8, 7, 0, 1, -1> src_vec(static_cast<void *>(&mem));
  src_vec.depositMemory(1, &src[0]);
  src_vec.depositMemory(0, &src[1]);
  src_vec.depositMemory(-1, &src[2]);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 3, 0>  dst_net(dst);

  UtIO::cout() << "mem: ex: int8: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  for (int i = 1; i > -2; i--)
  {
    src_vec.formatMemory(&x, CarbonNet::eBin, i);
    UtIO::cout() << x << UtIO::endl;
  }

  UtString y("");
  src_vec.examineMemoryRange(1, dst, 5, 2);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_vec.examineMemoryRange(0, dst, 5, 2);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_vec.examineMemoryRange(-1, dst, 3, 0);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_mem_ex_int8_msb_lt_lsb()
{
  UInt32 src[3];
  UInt32 dst[1];

  Memory<UInt8, 8, 1, -1> mem;

  src[0] = 0x000000f0;
  src[1] = 0x0000003c;
  src[2] = 0x00000005;
  CarbonMemory<UInt8, -7, 0, 1, -1> src_vec(static_cast<void *>(&mem));
  src_vec.depositMemory(1, &src[0]);
  src_vec.depositMemory(0, &src[1]);
  src_vec.depositMemory(-1, &src[2]);

  dst[0] = 0x00000000;
  CarbonVector<UInt32, 3, 0>  dst_net(dst);

  UtIO::cout() << "mem: ex: int8: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  for (int i = 1; i > -2; i--)
  {
    src_vec.formatMemory(&x, CarbonNet::eBin, i);
    UtIO::cout() << x << UtIO::endl;
  }

  UtString y("");
  src_vec.examineMemoryRange(1, dst, -5, -2);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_vec.examineMemoryRange(0, dst, -5, -2);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_vec.examineMemoryRange(-1, dst, -3, -0);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_mem_ex_int65_msb_gt_lsb()
{
  UInt32 src[9];
  UInt32 dst[2];

  Memory<BitVector<65>, 65, 1, -1> mem;

  src[0] = 0xffffffff;
  src[1] = 0x00000003;
  src[2] = 0x00000000;

  src[3] = 0xffffffff;
  src[4] = 0x00fffffd;
  src[5] = 0xffffffff;

  src[6] = 0x00000000;
  src[7] = 0x00000000;
  src[8] = 0x00000000;

  CarbonMemory<BitVector<65>, 164, 100, 1, -1> src_vec(static_cast<void *>(&mem));
  src_vec.depositMemory(1, &src[0]);
  src_vec.depositMemory(0, &src[3]);
  src_vec.depositMemory(-1, &src[6]);

  dst[0] = 0x00000000;
  dst[1] = 0x00000000;
  CarbonVector<UInt32, 34, 0>  dst_net(dst);

  UtIO::cout() << "mem: ex: int65: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  for (int i = 1; i > -2; i--)
  {
    src_vec.formatMemory(&x, CarbonNet::eBin, i);
    UtIO::cout() << x << UtIO::endl;
  }

  UtString y("");
  src_vec.examineMemoryRange(1, dst, 134, 100);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_vec.examineMemoryRange(0, dst, 134, 100);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_vec.examineMemoryRange(-1, dst, 134, 100);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_vec.examineMemoryRange(0, dst, 164, 130);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}


void test_mem_ex_int65_msb_lt_lsb()
{
  UInt32 src[9];
  UInt32 dst[2];

  Memory<BitVector<65>, 65, 1, -1> mem;

  src[0] = 0xffffffff;
  src[1] = 0x00000003;
  src[2] = 0x00000000;

  src[3] = 0xffffffff;
  src[4] = 0x00fffffd;
  src[5] = 0xffffffff;

  src[6] = 0x00000000;
  src[7] = 0x00000000;
  src[8] = 0x00000000;

  CarbonMemory<BitVector<65>, -164, -100, 1, -1> src_vec(static_cast<void *>(&mem));
  src_vec.depositMemory(1, &src[0]);
  src_vec.depositMemory(0, &src[3]);
  src_vec.depositMemory(-1, &src[6]);

  dst[0] = 0x00000000;
  dst[1] = 0x00000000;
  CarbonVector<UInt32, 34, 0>  dst_net(dst);

  UtIO::cout() << "mem: ex: int65: msb < lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  for (int i = 1; i > -2; i--)
  {
    src_vec.formatMemory(&x, CarbonNet::eBin, i);
    UtIO::cout() << x << UtIO::endl;
  }
  
  UtString y("");
  src_vec.examineMemoryRange(1, dst, -134, -100);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_vec.examineMemoryRange(0, dst, -134, -100);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_vec.examineMemoryRange(-1, dst, -134, -100);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;

  src_vec.examineMemoryRange(0, dst, -164, -130);
  dst_net.format(&y, CarbonNet::eBin);
  UtIO::cout() << y << UtIO::endl;
}



/*
**
** C TESTS
**
*/
void test_c_scalar()
{
  UInt8 src[1];
  //UInt32 new_val[1];

  src[0] = 0x00;
  CarbonVector<UInt8, 3, -4> src_net(src);
  CarbonHandle ch = static_cast<CarbonHandle>(&src_net);

  if (carbonGetRangeNumUint32s(ch, 1, -1) != 1)
  {
    UtIO::cout() << "OOPS" << UtIO::endl;
  }
  else
  {
    UtIO::cout() << "YAY" << UtIO::endl;
  }


#if 0
  UtIO::cout() << "vec: dp: int8: msb > lsb" << UtIO::endl;
  UtIO::cout() << "--------------------------------" << UtIO::endl;
  UtString x("");
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x0000000f;
  src_net.depositRange(new_val, 3, 0);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  src_net.depositRange(new_val, 3, 0);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x000000aa;
  src_net.depositRange(new_val, -1, -4);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x00000000;
  src_net.depositRange(new_val, -1, -4);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;

  new_val[0] = 0x000000ff;
  src_net.depositRange(new_val, 1, -2);
  src_net.format(&x, CarbonNet::eBin);
  UtIO::cout() << x << UtIO::endl;
#endif
}



int main()
{
  // Test the C++ interface.
#if 0
  test_vec_ex_int8_msb_gt_lsb();
  test_vec_ex_int8_msb_lt_lsb();
  test_vec_ex_int8_msb_eq_lsb();
  test_vec_ex_int16_msb_gt_lsb();
  test_vec_ex_int16_msb_lt_lsb();
  test_vec_ex_int16_msb_eq_lsb();
  test_vec_ex_int32_msb_gt_lsb();
  test_vec_ex_int32_msb_lt_lsb();
  test_vec_ex_int32_msb_eq_lsb();
  test_vec_ex_int64_msb_gt_lsb();
  test_vec_ex_int64_msb_lt_lsb();
  test_vec_ex_int64_msb_eq_lsb();
  test_vec_ex_int65_msb_gt_lsb();
  test_vec_ex_int65_msb_lt_lsb();
  test_vec_dp_int32_msb_gt_lsb();
  test_vec_dp_int32_msb_lt_lsb();
  test_vec_dp_int8_msb_gt_lsb();
  test_vec_dp_int8_msb_lt_lsb();
  test_vec_dp_int64_msb_gt_lsb();
  test_vec_dp_int64_msb_lt_lsb();
  test_vec_dp_int65_msb_gt_lsb();
  test_vec_dp_int65_msb_lt_lsb();
  test_mem_ex_int8_msb_gt_lsb();
  test_mem_ex_int8_msb_lt_lsb();
  test_mem_ex_int65_msb_gt_lsb();
  test_mem_ex_int65_msb_lt_lsb();
  test_mem_dp_int8_msb_gt_lsb();
  test_mem_dp_int8_msb_lt_lsb();
  test_mem_dp_int65_msb_gt_lsb();
  test_mem_dp_int65_msb_lt_lsb();
#endif

  // Test the C interface.
  test_c_scalar();

  return 0;
}
