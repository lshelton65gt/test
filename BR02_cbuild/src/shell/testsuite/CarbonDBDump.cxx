// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*
  This is used to build carbondbdump. This is currently just used to
  debug problems with the carbon database api. It doesn't fully test
  the api. The python wrapper, dbdump.py, fully tests the api. But, if
  the wrapper crashes for any reason, I'd rather debug it in gdb using
  this.
*/

#include "shell/carbon_dbapi.h"
#include "shell/carbon_misc.h"
#include "util/UtIOStream.h"
#include "util/UtIndent.h"
#include "util/UtString.h"
#include "util/ArgProc.h"
#include "util/RandomValGen.h"

static eCarbonMsgCBStatus myCB(CarbonClientData clientData, 
                               CarbonMsgSeverity severity, 
                               int,
                               const char*,
                               unsigned int)
{
  if ((severity == eCarbonMsgError) || (severity == eCarbonMsgAlert))
  {
    UInt32* errors = (UInt32*) clientData;
    ++(*errors);
  }
  UtIO::cout() << "CarbonDBDump::Message handler invoked." << UtIO::endl;
  return eCarbonMsgContinue;
}

static void dumpModule(CarbonDB* db, const CarbonDBNode* node, int spaces) {
  UtString buf;
  UtIndent indent(&buf);
  indent.tab(spaces);
  buf << carbonDBNodeGetLeafName(db, node);
  indent.tabToLocColumn();

  if (carbonDBIsForcible(db, node))     {buf << " Forcible";}
  if (carbonDBIsDepositable(db, node))  {buf << " Depositable";}
  if (carbonDBIsObservable(db, node))   {buf << " Observable";}
  if (carbonDBIsVisible(db, node))      {buf << " Visible";}
  if (carbonDBIsLiveInput(db, node))      {buf << " LiveIn";}
  if (carbonDBIsLiveOutput(db, node))      {buf << " LiveOut";}
  if (carbonDBIsTriggerUsedAsData(db, node))      {buf << " Trigger/Data";}
  if (carbonDBIsBothEdgeTrigger(db, node))      
  {
    buf << " 2Trigger";
  }
  else
  {
    if (carbonDBIsPosedgeTrigger(db, node))      {buf << " PTrigger";}
    if (carbonDBIsNegedgeTrigger(db, node))      {buf << " NTrigger";}
  }

  UtIO::cout() << buf << "\n";

  CarbonDBNodeIter* iter = carbonDBLoopChildren(db, node);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    dumpModule(db, node, spaces + 2);
  }
  carbonDBFreeNodeIter(iter);
}

static void sPrintFullNames(CarbonDBNodeIter* dbNodeIter, CarbonDB* dbContext)
{
  const CarbonDBNode* node = NULL;
  while ((node = carbonDBNodeIterNext(dbNodeIter)))
  {
    const char* name =  carbonDBNodeGetFullName(dbContext, node);
    UtIO::cout() << "\t" << name << UtIO::endl;
  }
}

static void sPrintNodes(CarbonDBNodeIter* dbNodeIter, CarbonDB* dbContext)
{
  const CarbonDBNode* node = NULL;
  while ((node = carbonDBNodeIterNext(dbNodeIter)))
  {
    const char* name =  carbonDBNodeGetFullName(dbContext, node);
    UtIO::cout() << "\t" << name << UtIO::endl;
    const char* leafname =  carbonDBNodeGetLeafName(dbContext, node);
    UtIO::cout() << "\t\t" << leafname << UtIO::endl;
    
  }
}

int main(int argc, char* argv[])
{
  ArgProc argProc;
  argProc.setDescription("Carbon C API Database Dumper", "carbondbdump",
                         "This is used for internal testing only. This tests the database C api.");

  {
    UtString synopsis("carbondbdump [-socvsp] <filename> [<filename> ...]");
    argProc.addSynopsis(synopsis);
  }

  argProc.createSection("General");
  
  argProc.addBool("-socvsp", 
                  "Open the database using the soc vsp open routine.",
                  false, 1);
  argProc.addToSection("General", "-socvsp");

  if (argc < 2)
  {
    UtString buf;
    argProc.getUsageVerbose(&buf);
    UtIO::cout() << buf << UtIO::endl;
    return 1;
  }
  

  int numOptions;
  UtString errMsg;
  if (argProc.parseCommandLine(&argc, argv, &numOptions, &errMsg) != ArgProc::eParsed)
  {
    UtIO::cout() << errMsg << UtIO::endl;
    return 1;
  }
  
  UInt32 errors = 0;

  CarbonMsgCBDataID* cbID = carbonDBAddMsgCB(myCB, &errors);

  for (int i = 1;  i < argc; ++i)
  {
    CarbonDB* dbContext = NULL;
    if (argProc.getBoolValue("-socvsp"))
    {
      SInt32 key;
      RANDOM_SEEDGEN_1(key, "VSP", 3);
      dbContext= carbonDBSocVspInit(key, argv[i], true);
    }
    else
      dbContext = carbonDBCreate(argv[i], true);
    
    if (dbContext)
    {
      UtIO::cout() << "Inputs:" << UtIO::endl;
      CarbonDBNodeIter* dbNodeIter = carbonDBLoopPrimaryInputs(dbContext);
      sPrintNodes(dbNodeIter, dbContext);
      carbonDBFreeNodeIter(dbNodeIter);

      UtIO::cout() << "Primary Clks:" << UtIO::endl;
      dbNodeIter = carbonDBLoopPrimaryClks(dbContext);
      sPrintNodes(dbNodeIter, dbContext);
      carbonDBFreeNodeIter(dbNodeIter);

      UtIO::cout() << "Posedge Triggers: " <<  UtIO::endl;
      dbNodeIter = carbonDBLoopPosedgeTriggers(dbContext);
      sPrintFullNames(dbNodeIter, dbContext);
      UtIO::cout() << "Negedge Triggers: " << UtIO::endl;
      dbNodeIter = carbonDBLoopNegedgeTriggers(dbContext);
      sPrintFullNames(dbNodeIter, dbContext);
      UtIO::cout() << "BothEdge Triggers: " << UtIO::endl;
      dbNodeIter = carbonDBLoopBothEdgeTriggers(dbContext);
      sPrintFullNames(dbNodeIter, dbContext);
      UtIO::cout() << "Posedge Only Triggers: " << UtIO::endl;
      dbNodeIter = carbonDBLoopPosedgeOnlyTriggers(dbContext);
      sPrintFullNames(dbNodeIter, dbContext);
      UtIO::cout() << "Negedge Only Triggers: " << UtIO::endl;
      dbNodeIter = carbonDBLoopNegedgeOnlyTriggers(dbContext);
      sPrintFullNames(dbNodeIter, dbContext);
      UtIO::cout() << "Triggers Used As Data: " << UtIO::endl;
      dbNodeIter = carbonDBLoopTriggersUsedAsData(dbContext);
      sPrintFullNames(dbNodeIter, dbContext);

      UtIO::cout() << UtIO::endl << UtIO::endl;

      dbNodeIter = carbonDBLoopDesignRoots(dbContext);
      const CarbonDBNode* node;
      while ((node = carbonDBNodeIterNext(dbNodeIter)) != NULL) {
        dumpModule(dbContext, node, 0);
      }
      carbonDBFreeNodeIter(dbNodeIter);

      carbonDBFree(dbContext);
    }
  }

  carbonDBRemoveMsgCB(&cbID);

  if (errors != 0)
    return 1;
  return 0;
}
