// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// author: Mark Seneski

#include "util/CarbonPlatform.h"
#include "shell/CarbonWaveImp.h"
#include "shell/CarbonHookup.h"
#include "shell/CarbonDBRead.h"
#include "shell/CarbonModel.h"
#include "shell/ShellNet.h"
#include "shell/ShellData.h"
#include "util/ShellMsgContext.h"
#include "util/CarbonAssert.h"
#include "iodb/IODBRuntime.h"
#include "iodb/IODBTypes.h"
#include "iodb/IODBUserTypes.h"
#include "hdl/HdlId.h"
#include "symtab/STSymbolTable.h"
#include "util/UtString.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Signature.h"
#include "schedule/Event.h"
#include "shell/WaveDump.h"
#include "util/UtConv.h"
#include "CarbonWaveRegistrar.h"
#include "CarbonWaveVC.h"
#include "util/AtomicCache.h"
#include "CarbonExamineScheduler.h"
#include "CarbonDatabase.h"
#include "CarbonDatabaseNode.h"
#include "CarbonDatabaseIter.h"
#include "util/UtStackPOD.h"
#include "shell/ShellSymNodeIdent.h"
#include "util/StringAtom.h"
#include "shell/CarbonScalarBase.h"
#include "shell/CarbonVectorBase.h"
#include "shell/CarbonExprNet.h"

#if SORT_BY_OFFSET
#include <algorithm>
#endif

#include <string.h>


typedef UtArray<STAliasedLeafNode*> LeafNodeVec;
// Stupid class so I don't have to do a vector of a vector

//! AliasData class
/*!
  This simplifies template notation, so I don't have to notate a
  vector of a vector. Plus, I may need to add some more functionality
  to this class.
*/

class CarbonWaveRegistrar::AliasData
{

public: 
  CARBONMEM_OVERRIDES

  //! constructor
  AliasData() 
  {
  }
  
  //! destructor
  ~AliasData()
  {
  }

  //! All aliases for a particular leaf node
  /*!
    The STAliasedLeafNode at position [0] is what we consider the
    master alias, although this does not necessarily mean that the
    node is the master alias in the symbol table.
  */
  typedef UtArray<STAliasedLeafNode*> AliasList;
  AliasList mAliases;
};


class CarbonWaveRegistrar::SplitNetWalker : public CarbonExprWalker
{
public:
  CARBONMEM_OVERRIDES

  SplitNetWalker(CarbonWaveRegistrar* registrar, WaveScope* brScope) : mRegistrar(registrar),
                                                                       mBranchScope(brScope)
  {}
  
  virtual ~SplitNetWalker() 
  {}


  virtual void visitIdent(CarbonIdent* ident) { 
    SymTabIdent* symTabIdent = ident->castSymTabIdent();
    CE_ASSERT(symTabIdent, ident);
    STAliasedLeafNode* node = symTabIdent->getNode();
    mRegistrar->addLeafToWave(node, mBranchScope);
  }
  
private:
  CarbonWaveRegistrar* mRegistrar;
  WaveScope* mBranchScope;
};

//! Check if a given expression is complete
/*!
  This is a safety mechanism. If separation reconstructs the original
  names, we could end up with malformed expressions or expressions
  that are only partly understood. A net may get optimized away or
  lowered or rescoped in such a way that the expression that was
  created from Separation is no longer fully valid. We will know this
  if an ident within the expression doesn't have an expression in its
  node's BOM. 

  We could potentially write out what we know, but that may be
  confusing to a user. For now, this is used simply to skip an
  expression that is not complete.
*/
class CarbonWaveRegistrar::CheckExprCompleteWalk : public CarbonExprWalker
{
public:
  CARBONMEM_OVERRIDES

  CheckExprCompleteWalk() : mIsCompleteExpr(true) {}

  virtual ~CheckExprCompleteWalk() {}

  /*!
    This checks if the ident that is passed in is actually referenced
    by the leaf that it contains. Separation will create slices that
    may never actually get NUNet's, and this causes idents to be in
    expressions that have no real context to them. In this case we
    just want to skip the whole expression, even if parts of it may be
    ok. We could print out portions of the net that are valid, but
    that can be done later. To do that, you'll need to return from
    addNetToScope() if the bomdata is NULL (as below) or there is no
    backpointer ident expression for the the leaf of the ident (the
    ident needs to describe itself in terms its backpointer).
  */
  virtual bool preVisitIdent(CarbonIdent* ident)
  {
    SymTabIdent* symTabIdent = ident->castSymTabIdent();
    CE_ASSERT(symTabIdent, ident);
    STAliasedLeafNode* leaf = symTabIdent->getNode();
    ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(leaf);
    mIsCompleteExpr = bomdata != NULL;
    if (bomdata)
    {
      CarbonExpr* identExpr = bomdata->getExpr();
      mIsCompleteExpr = identExpr != NULL;
      
      if (identExpr) {
        SymTabIdent* symTabIdent = identExpr->castSymTabIdent();
        mIsCompleteExpr = symTabIdent != NULL;
      }
    }
    return mIsCompleteExpr;
  }

  /*! 
    Returns true if all the idents within the expression have leaves
    that refer to them.
  */
  bool isCompleteExpr() const { return mIsCompleteExpr; }

private:
  bool mIsCompleteExpr;
};

// This is used to figure out if we should allow access to a requested
// node (from carbonFindNet). It may be forbidden if it is in a hidden
// module (directive: hideModule)
class CarbonWaveRegistrar::NodeContext
{
public:
  CARBONMEM_OVERRIDES

  NodeContext()
  {
    clear();
  }

  void clear() { 
    mLevel = 0;
    mSignalsAdded = false; 
  }

  bool empty() const { return ! mSignalsAdded;}

  inline bool isInHidden() const { return mLevel > 0; }

  inline void add() { 
    if (isInHidden()) 
      mSignalsAdded = true; 
  }
  
  void pushHidden() {
    ++mLevel;
  }
  
  void popHidden() {
    INFO_ASSERT(mLevel != 0, "NodeContext stack empty");
    --mLevel;
  }
  
private:
  UInt32 mLevel;
  bool mSignalsAdded;
};


// Returns true if the user type needs to be expanded to be added to a
// waveform.  Basically, anything that isn't a scalar, vector, or enum
// needs to be expanded.
static bool sMustExpandUserType(const UserType* ut)
{
  if(ut != NULL) 
  {
    bool isScalar = ut->getType() == UserType::eScalar;
    bool isEnum = ut->getType() == UserType::eEnum;
    // It's a vector if either of the following is true:
    //    there are two or more packed dimensions
    //    it's a 1-D array of scalars whose width is 1

    const UserArray* arrayType = ut->castArray();
    bool isVector = ( (arrayType != NULL) && (arrayType->isPackedArray()));           // a packed array item is handled like a vector

    if (!isVector && (arrayType != NULL) && (arrayType->getNumDims() == 1)) {
      const UserType* elemType = arrayType->getElementType();
      const UserScalar* scalarType = elemType->castScalar();
      if ((scalarType != NULL) && (scalarType->getSize() == 1)) {
        // There is no vector of booleans 
        if(scalarType->getVhdlType() != eVhdlTypeBoolean)
          isVector = true;
      }
    }

    if (!(isScalar || isEnum || isVector)) {
      return true;
    }
  }
  return false;
}

static CarbonLanguageType sGetBranchLanguage(const STBranchNode* node) {
  const ShellBranchDataBOM* branchBOM = ShellSymTabBOM::getBranchBOM(node);
  HierFlags langFlags = branchBOM->getSourceLanguage();
  CarbonLanguageType lang = eLanguageTypeUnknown;
  if (langFlags == eSLVerilog) {
    lang = eLanguageTypeVerilog;
  } else if (langFlags == eSLVHDL) {
    lang = eLanguageTypeVhdl;
  }
  else {
    const STBranchNode* parent = node->getParent();
    if(parent != NULL)
      return sGetBranchLanguage(parent);
  }
    
  return lang;
}

//! Abstract base class for building the stack of WaveScopes for a node.
class CarbonWaveRegistrar::ScopeWalker
{
public:
  CARBONMEM_OVERRIDES

  ScopeWalker()
    : mStartScope(NULL)
  {}
  virtual ~ScopeWalker() {}

  WaveScope* getStartScope() { return mStartScope; }

  //! Builds the scope stack
  virtual void walk() = 0;
  //! Is the scope stack empty?
  virtual bool empty() = 0;
  //! Pop the top node from the stack
  virtual void popNode() = 0;
  //! Map the top node in the stack to a WaveScope
  virtual void mapNodeToScope(WaveScope* scope) = 0;
  //! Get the UserType for the top of the stack
  virtual const UserType* getNodeType() = 0;
  //! Get the StringAtom for the top of the stack
  virtual const StringAtom* getNodeName() = 0;
  //! Get the language for the top of the stack
  virtual CarbonLanguageType getNodeLanguage() = 0;

protected:
  //! The starting scope for this sub-tree, if it already exists
  WaveScope* mStartScope;
};

//! Specialized ScopeWalker for STSymbolTableNodes
class CarbonWaveRegistrar::STScopeWalker : public CarbonWaveRegistrar::ScopeWalker
{
public:
  CARBONMEM_OVERRIDES

  STScopeWalker(STBranchNode* node, BranchScopeMap* scopeMap)
    : mStartNode(node),
      mScopeMap(scopeMap)
  {}
  virtual ~STScopeWalker() {}

  virtual void walk()
  {
    // make a stack of nodes that do not have scopes (the last one added
    // has either a Scope or has a NULL parent).
    bool foundScope = false;
    STBranchNode* node = mStartNode;

    BranchScopeMap::iterator p;
    while(! foundScope && node)
    {      
      p = mScopeMap->find(node);

      if (p == mScopeMap->end()) {
        mNodeStack.push(node);
      } else {
        mStartScope = p->second;
        foundScope = true;
      }

      node = node->getParent();
    }
  }

  virtual bool empty()
  {
    return mNodeStack.empty();
  }

  virtual void popNode()
  {
    mNodeStack.pop();
  }

  virtual void mapNodeToScope(WaveScope* scope)
  {
    STBranchNode* node = mNodeStack.top();
    (*mScopeMap)[node] = scope;
  }

  virtual const UserType* getNodeType()
  {
    // Although UserType may exist for the STSymbolTableNode, it's
    // only interesting for waveforms when dumping composites.
    return NULL;
  }

  virtual const StringAtom* getNodeName()
  {
    STBranchNode* node = mNodeStack.top();
    const StringAtom* strAtom = node->strObject();
    return strAtom;
  }

  virtual CarbonLanguageType getNodeLanguage()
  {
    STBranchNode* node = mNodeStack.top();
    CarbonLanguageType lang = sGetBranchLanguage(node);
    return lang;
  }

protected:
  //! The starting (i.e. bottom) node for the stack
  STBranchNode* mStartNode;
  //! The map of nodes to scopes
  BranchScopeMap* mScopeMap;
  //! The current stack of nodes
  UtStackPOD<STBranchNode*> mNodeStack;
};

//! Specialized ScopeWalker for STSymbolTableNodes
class CarbonWaveRegistrar::DBScopeWalker : public CarbonWaveRegistrar::ScopeWalker
{
public:
  CARBONMEM_OVERRIDES

  DBScopeWalker(CarbonDatabaseNode* node, DBNodeScopeMap* dbScopeMap, BranchScopeMap* branchScopeMap)
    : mStartNode(node),
      mDBScopeMap(dbScopeMap),
      mBranchScopeMap(branchScopeMap)
  {}
  virtual ~DBScopeWalker() {}

  virtual void walk()
  {
    // make a stack of nodes that do not have scopes (the last one added
    // has either a Scope or has a NULL parent).
    WaveScope* foundScope = NULL;
    CarbonDatabaseNode* node = mStartNode;

    while((foundScope == NULL) && node) {
      // Look for a scope for the branch node first.  It may have been
      // created by the branch node walker.
      const STSymbolTableNode* stNode = node->getSymTabNode();
      const STBranchNode* branch = stNode->castBranch();
      if (branch != NULL) {
        BranchScopeMap::iterator branchIter = mBranchScopeMap->find(branch);
        if (branchIter != mBranchScopeMap->end()) {
          foundScope = branchIter->second;
        }
      }
      if (foundScope == NULL) {
        DBNodeScopeMap::iterator dbIter = mDBScopeMap->find(node);
        if (dbIter != mDBScopeMap->end()) {
          foundScope = dbIter->second;
        }
      }
      
      if (foundScope == NULL) {
        mNodeStack.push(node);
      } else {
        mStartScope = foundScope;
      }

      node = node->getParent();
    }
  }

  virtual bool empty()
  {
    return mNodeStack.empty();
  }

  virtual void popNode()
  {
    mNodeStack.pop();
  }

  virtual void mapNodeToScope(WaveScope* scope)
  {
    CarbonDatabaseNode* node = mNodeStack.top();
    // Only use the DBNode when required, i.e. for a composite.  Use
    // the branch node if there is no user type (meaning it's a module
    // or other named scope), so that non-composites can share it.
    const UserType* ut = node->getUserType();
    if (ut == NULL) {
      const STSymbolTableNode* stNode = node->getSymTabNode();
      const STBranchNode* branch = stNode->castBranch();
      ST_ASSERT(branch != NULL, stNode);
      (*mBranchScopeMap)[branch] = scope;
    } else {
      (*mDBScopeMap)[node] = scope;
    }
  }

  virtual const UserType* getNodeType()
  {
    CarbonDatabaseNode* node = mNodeStack.top();
    const UserType* ut = node->getUserType();
    // As a sanity check, make sure it's something that must be expanded if the
    // type is known.  Scalars/vectors/enums should never need a DBScopeWalker,
    // because they're either not in a composite (in which case
    // they'll have a STScopeWalker), or they're within a composite
    // (in which case they'll be added through an explicit walk of the
    // composite).
    if (ut != NULL) {
      ST_ASSERT(sMustExpandUserType(ut), node->getSymTabNode()); 
    }
    return ut;
  }

  virtual const StringAtom* getNodeName()
  {
    CarbonDatabaseNode* node = mNodeStack.top();
    const StringAtom* strAtom = node->getName();
    return strAtom;
  }

  virtual CarbonLanguageType getNodeLanguage()
  {
    CarbonLanguageType lang = eLanguageTypeUnknown;
    const UserType* ut = getNodeType();
    if (ut != NULL) {
      lang = ut->getLanguageType();
    }
    return lang;
  }

protected:
  //! The starting (i.e. bottom) node for the stack
  CarbonDatabaseNode* mStartNode;
  //! The map of DB nodes to scopes
  DBNodeScopeMap* mDBScopeMap;
  //! The map of ST branch nodes to scopes
  BranchScopeMap* mBranchScopeMap;
  //! The current stack of nodes
  UtStackPOD<CarbonDatabaseNode*> mNodeStack;
};


static CarbonExpr* sGetExpression(const STAliasedLeafNode* leaf)
{
  const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(leaf);
  return const_cast<CarbonExpr*>(bomdata->getExpr());
}

static CarbonExpr* sGetConcatExpression(STAliasedLeafNode* leaf)
{
  CarbonExpr* expr = sGetExpression(leaf);
  if (expr)
    // This is a simple-minded split net detection. 
    expr = expr->castConcat();
  
  return expr;
}

CarbonWaveRegistrar::CarbonWaveRegistrar(CarbonWaveVC* vcObj,
                                         CarbonHookup* hookup,
                                         WaveDump* dataFile, 
                                         const UtStringArray& prefixHier,
                                         bool replaceDesignRoot) :
  mCarbonVC(vcObj),
  mHookup(hookup),
  mDataFile(dataFile),
  mPrefixHier(NULL),
  mNodeType(eAnyNode),
  mRunDebugSchedule(false),
  mReplaceDesignRoot(replaceDesignRoot)
{
  IODBRuntime* iodb = mHookup->getDB();
  AtomicCache* strCache = iodb->getDesignSymbolTable()->getAtomicCache();
  WaveScope* curScope = NULL;

  // If we're adding additional prefix hierarchy, use the language of
  // the root module.
  STSymbolTable* symtab = iodb->getDesignSymbolTable();
  STSymbolTable::RootIter rootIter = symtab->getRootIter();
  STSymbolTableNode* rootNode = *rootIter;
  STBranchNode* rootBranch = rootNode->castBranch();
  ST_ASSERT(rootBranch != NULL, rootNode);
  CarbonLanguageType rootLang = sGetBranchLanguage(rootBranch);

  for (UtStringArray::UnsortedCLoop p = prefixHier.loopCUnsorted();
       ! p.atEnd(); ++p)
  {
    const char* str = *p;
    // The atomic cache is shared by all model instances, so access
    // needs to be controlled with a mutex.
    ShellGlobal::lockMutex();
    StringAtom* object = strCache->intern(str);
    ShellGlobal::unlockMutex();
    curScope = mDataFile->attachScope(object, curScope, WaveScope::eModule, NULL, rootLang);
    mPrefixHier = curScope;
  }
  // If true, the wave dumping becomes VERY slow, but can workaround
  // model bugs where a signal isn't attached to a schedule or a
  // combinational node gets updated but the clock path is not.
  mAlwaysUpdate = (getenv("CARBON_NOSCHED_WAVEFORM") != NULL);
  mConcatExprsOnly = (getenv("CARBON_NOEXPR_WAVEFORM") != NULL);
  mWalkingExpr = 0;
  mNodeContext = new NodeContext;

  // Dump Size Limit: Default Value
  mDumpSizeLimit = scDefaultDumpSizeLimit;
  // If one was specified on the cbuild command line, use that
  // instead.  It's stored in the generic attribute map.
  UInt32 commandLineDumpSizeLimit;
  // This should only update the int value if the attribute is
  // present, so we could probably store it directly, but this is
  // safer in case the implemenation changes.
  if (iodb->getIntAttribute("waveformDumpSizeLimit", &commandLineDumpSizeLimit)) {
    mDumpSizeLimit = commandLineDumpSizeLimit;
  }
}

CarbonWaveRegistrar::~CarbonWaveRegistrar() 
{
  mCarbonVC->cleanupAssocRefs();

  for (NetAssocVecLoop q(mAllAssocs);
       ! q.atEnd(); ++q)
  {
    CarbonWaveNetAssoc* assoc = *q;
    delete assoc;
  }
  delete mNodeContext;
}


bool CarbonWaveRegistrar::runDebugSchedule() const 
{
  return mRunDebugSchedule;
}

CarbonStatus CarbonWaveRegistrar::doNets(UtStringArray& nodeNames, 
                                         unsigned int depth,
                                         NodeType nodeType)
{
  CarbonStatus stat = eCarbon_OK;
  
  mNodeType = nodeType;
  
  HdlHierPath::Status parseStatus;
  STSymbolTableNode* symNode;
  HdlId info;
  
  IODBRuntime* db = mHookup->getDB();
  STSymbolTable* symTab = db->getDesignSymbolTable();
  const char* netName;
  bool setError = false;
  for(UtStringArray::UnsortedCLoop p = nodeNames.loopCUnsorted(); 
      ! p.atEnd(); ++p)
  {
    netName = *p;
    // This uses the atomic cache that's shared by all model
    // instances, so access needs to be controlled with a mutex.
    ShellGlobal::lockMutex();
    symNode = symTab->getNode(netName, &parseStatus, &info);
    ShellGlobal::unlockMutex();
    if (parseStatus != HdlHierPath::eIllegal)
    {
      mNodeContext->clear();
      if (symNode)
      {
        // check for hidden module
        const STAliasedLeafNode* leaf = symNode->castLeaf();
        if (leaf && db->isNetHidden(leaf))
          symNode = NULL;
        else if (db->isModuleHidden(symNode))
          // need to process the module in case there are exposed
          // signals. This push does not get popped. It gets cleared
          // above or after the for loop 
          mNodeContext->pushHidden();
      }
      
      if (symNode)
      {
        stat = processNode(symNode, depth);
        if (mNodeContext->isInHidden() && 
            (stat == eCarbon_OK) &&
            mNodeContext->empty())
          getMsgContext()->SHLPathNotFound(netName);
        
      }
      else
      {
        getMsgContext()->SHLPathNotFound(netName);
        setError = true;
      }
    }
    else
    {
      getMsgContext()->SHLInvalidPath(netName);
      setError = true;
    }
  }

  // clear in case we ended in a hidden node
  mNodeContext->clear();

  if (setError)
    stat = eCarbon_ERROR;
  
  return stat;
}

void CarbonWaveRegistrar::cleanupAssocs()
{
  CarbonWaveNetAssoc* assoc;
  {
    WaveHandle* handle = NULL;
    STAliasedLeafNode* lnode = NULL;
    if (! mAllAliases.empty())
    {
      // Resolve Aliases
      for (AliasDataVecLoop p(mAllAliases);
           ! p.atEnd(); ++p)
      {
        AliasData* aData = *p;
        
        // remove dead assocs
        {
          UInt32 numAliases = aData->mAliases.size();
          for (UInt32 i = 0; i < numAliases; ++i)
          {
            lnode = aData->mAliases[i];
            assoc = getAssoc(lnode);
            if (! assoc->getHandle())
              aData->mAliases[i] = NULL;
          }
        }
        
        AliasData::AliasList::iterator ap = aData->mAliases.begin();
        // safety measure
        handle = NULL;
        
        // the first alias is definitely registered and we should
        // consider it the master alias.
        if (ap != aData->mAliases.end())
        {
          lnode = *ap;
          assoc = getAssoc(lnode);
          handle = assoc->getHandle();
          ++ap;
        }
        INFO_ASSERT(handle, "Invalid alias ring");
        while(ap != aData->mAliases.end())
        {
          lnode = *ap;
          if (lnode) // if ! dead assoc
          {
            assoc = getAssoc(lnode);
            if (VERIFY(assoc->getHandle()))
              handle->addAlias(assoc->getHandle());
          }
          ++ap;
        }
      } // for
    } // if
  }
  
  {
    AliasData* aData;
    STAliasedLeafNode* lnode;
    for (AliasDataVecLoop p(mAllAliases);
         ! p.atEnd(); ++p)
    {
      aData = *p;
      AliasData::AliasList::iterator adp = aData->mAliases.begin();
      AliasData::AliasList::iterator adpe = aData->mAliases.end();
      if (adp != adpe)
        ++adp; // skip first since that is the 'master alias'
      while (adp != adpe)
      {
        lnode = *adp;
        if (lnode)
        {
          assoc = getAssoc(lnode);
          // remove the unneeded aliased handles
          assoc->setHandle(NULL);
        }
        ++adp;
      }
      
      // Do not need AliasDatas anymore
      delete aData;
    }
    
    mAllAliases.clear();
  }
  
  NetAssocVec constants;
  NetAssocVec doomedAssoc;
  {
    // place space efficient swapAssoc on stack
    NetAssocVec swapAssoc;
    
    // Can't reserve, because capacity gets swapped as well
    //    swapAssoc.reserve(mAllAssocs.size());
    
    for (NetAssocVecLoop q(mAllAssocs);
         ! q.atEnd(); ++q)
    {
      assoc = *q;
      bool isConstant = (assoc->getNet() 
                         && (assoc->getNet()->castConstant() != NULL));
      bool delAssoc = ! assoc->getHandle();
      
      if (isConstant)
      {
        constants.push_back(assoc);
        doomedAssoc.push_back(assoc);
      }
      else if (delAssoc)
        doomedAssoc.push_back(assoc);
      else
        swapAssoc.push_back(assoc);
    }
    
    // sort the assocs by offset then allocate the shadows.
#if SORT_BY_OFFSET
    std::sort(swapAssoc.begin(), swapAssoc.end(), sSortByOffset);
#endif
#define OLD_SHADOW_ALLOC 0
#if OLD_SHADOW_ALLOC
    for (NetAssocVecLoop swapAssocP(swapAssoc);
         ! swapAssocP.atEnd();
         ++swapAssocP)
    {
      assoc = *swapAssocP;
      assoc->allocShadow();
    }
#endif
    
    // now keep only needed assocs
    // The contents of allassocs can be used to update the data file.
    mAllAssocs.swap(swapAssoc);
  }
  
  // close the hierarchy
  UtString errMsg;
  if (mDataFile->closeHierarchy(&errMsg) != WaveDump::eOK)
    getMsgContext()->SHLFileProblem(errMsg.c_str());

  // Now set the constants
  for (NetAssocVecLoop r(constants);
       ! r.atEnd(); ++r)
  {
    CarbonWaveNetAssoc* constAssoc = *r;
    WaveHandle* handle = constAssoc->getHandle();
    ShellNet* net = constAssoc->getNet();
    if (handle)
    {
      
      if (handle->isInteger())
      {
        UInt32 intval;
        ST_ASSERT(net->getNumUInt32s() == 1,
                  net->getName());
        net->examine(&intval, NULL, ShellNet::eIDrive, NULL);
        mDataFile->addChangedWithInt32(handle, SInt32(intval));
      }
      else if (handle->isReal())
      {
        CarbonReal val = 0.0;
        net->examine(&val, NULL, ShellNet::eIDrive, NULL);
        mDataFile->addChangedWithDbl(handle, val);
      }
      else if (handle->isTime())
      {
        // 64 bit time, even if it isn't
        UInt32 timeVal[2];
        timeVal[0] = 0;
        timeVal[1] = 0;
        ST_ASSERT(net->getNumUInt32s() <= 2,
                  net->getName());
        net->examine(timeVal, NULL, ShellNet::eIDrive, NULL);
        UInt64 bVal;
        CarbonValRW::cpSrcToDest(&bVal, timeVal, 2);
        mDataFile->addChangedWithInt64(handle, bVal);
      }
      else
      {
        net->format(handle->getValue(), handle->getSize() + 1, eCarbonBin, eNoneNet, NULL);
        mDataFile->addChanged(handle);
      }
      
    }
  }
  
  // Now remove all the assocs from the symbol table. No need for
  // them now, and we are going to delete the unneeded ones soon
  STSymbolTable* symTab = mHookup->getDB()->getDesignSymbolTable();
  ShellGlobal::lockMutex();
  for (STSymbolTable::NodeLoop iter = symTab->getNodeLoop(); ! iter.atEnd(); ++iter)
  {
    ShellData* shellData = CarbonWaveNetAssoc::sGetShellData(*iter);
    if (shellData)
      shellData->putNetAssoc(NULL, mHookup->getId());
  }
  ShellGlobal::unlockMutex();
  
  constants.clear(); 
  for (NetAssocVecLoop dna(doomedAssoc);
       ! dna.atEnd(); ++dna)
  {
    assoc = *dna;
    ShellNet* doomedNet = assoc->getNet();
    delete assoc;
    mHookup->freeNet(&doomedNet);
  }
  
  mCarbonVC->allocateNetTrackers();
}

CarbonStatus CarbonWaveRegistrar::processNode(STSymbolTableNode* symNode, unsigned int depth)
{
  STBranchNode* bnode;
  STAliasedLeafNode* lnode;
  WaveScope* scope = NULL;
  
  CarbonStatus status = eCarbon_OK;

  // Don't need to check for temporaries here since a user
  // wouldn't know it.
  if ((bnode = symNode->castBranch()))
  {
    scope = generateScopes(bnode);
    addDownNets(bnode, scope, depth, (depth == 0));
  }
  else if ((lnode = symNode->castLeaf()))
  {
    bool isWaveable = isWaveAbleNet(lnode);
    bool isValid = true;
      
    switch (mNodeType)
    {
    case eAnyNode:
      break;
    case eStateIONode:
      {
        if (! isStateIONode(lnode))
        {
          isValid = false;
          UtString name;
          mHookup->getNodeName(&name, symNode);
          MsgContextBase::Severity sev = getMsgContext()->SHLNonStateIO(name.c_str());
          status = ShellGlobal::severityToStatus(sev);
        }
      }
      break;
    }
      
    if (! isWaveable)
    {
      UtString name;
      mHookup->getNodeName(&name, symNode);
      MsgContextBase::Severity sev = getMsgContext()->SHLOptimizedWaveNet(name.c_str());
      status = ShellGlobal::severityToStatus(sev);
    }
    else if (isValid)
    {
      bool createdAssoc = false;
      CarbonWaveNetAssoc* waveAssoc = maybeCreateAssoc(symNode, &createdAssoc);
      if (isValid && createdAssoc && ! waveAssoc->getHandle())
      {
        scope = generateScopes(lnode->getParent());
        addNetToScope(scope, waveAssoc, lnode);
      } // else already added
    } // else if waveable net
  } // else if

  return status;
} // processNode

// Speeds up the process of waveform dumping by limiting the process only to 
// the signals for which waveform dumping has been enabled and their bit size 
// does not exceed the passed limit size.  
// Sets the size limit for waveform dumping in mDumpSizeLimit. 
void CarbonWaveRegistrar::setDumpSizeLimit(UInt32 size)
{
  mDumpSizeLimit = size;
}

bool CarbonWaveRegistrar::checkExprCompatibility(const STAliasedLeafNode* node) const
{
  bool isCompatible = true;
  CarbonExpr* expr = sGetExpression(node);
  if (expr)
  {
    if (mConcatExprsOnly)
      // Only allow concats and constants
      isCompatible = (expr->castConcat() != NULL) || (expr->castConst() != NULL);
    else
      // for clarity
      isCompatible = true;
  }
  return isCompatible;
}

bool CarbonWaveRegistrar::isArrayNet(const STAliasedLeafNode* node) const
{
  const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);

  NetFlags flags = bomdata->getNetFlags();

  return ( NetIsDeclaredAs(flags, eDMWire2DNet) ||
           NetIsDeclaredAs(flags, eDMReg2DNet) ||
           NetIsDeclaredAs(flags, eDMInteger2DNet) ||
           NetIsDeclaredAs(flags, eDMTime2DNet) ||
           NetIsDeclaredAs(flags, eDMRTime2DNet) );
}

bool CarbonWaveRegistrar::isWaveAbleNet(const STAliasedLeafNode* node) const
{
  IODBRuntime* db = mHookup->getDB();

  if (mNodeContext->isInHidden() && ! db->isNetExposed(node))
    return false;
  
  // Composites (arrays and structs) are not waveable on their own.
  // Array elements and struct fields could be waveable.
  const UserType* ut = db->getUserType(node);
  if (sMustExpandUserType(ut)) {
    return false;
  }

  const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);
  
  NetFlags flags = bomdata->getNetFlags();

  NetFlags portFlags = NetFlags(flags & ePortMask);

  bool isDead = bomdata->isDeadNet();
  if (isDead)
  {
    const STBranchNode* parent = node->getParent();
    bool isTopNode = false;
    if (parent)
      isTopNode = parent->getParent() == NULL;
    
    if (isTopNode &&
        ((portFlags == eInputNet) ||
         (portFlags == eBidNet)))
      isDead = false;
  }

  bool isArrayOfSomething = isArrayNet(node);

  bool isConstant = db->isFullyConstant(node);
  bool isWaveable = (! isArrayOfSomething &&
                     // portsplits are temps. Allow those
                     (! NetIsTemp(flags) || mWalkingExpr) &&
                     (! isDead || isConstant));
  isWaveable = isWaveable && (mHookup->hasValidCarbonNet(node) ||
                              isConstant);
  
  isWaveable = isWaveable && checkExprCompatibility(node);
  isWaveable &= ! bomdata->isForceSubordinate();

  // Dump Size Limit 
  // mDumpSizeLimit = 0: No Limitation
  if((ut != NULL) && (mDumpSizeLimit != 0)) 
    isWaveable &= (ut->getSize() <= mDumpSizeLimit);
 
  if (isWaveable)
  {
    switch (mNodeType)
    {
    case eAnyNode:
      break;
    case eStateIONode:
      if (! isStateIONode(node))
        isWaveable = false;
      break;
    }
    isWaveable = isWaveable && ! db->isInvalidWaveNet(node);
  }
  
  return isWaveable;
}


MsgContext* CarbonWaveRegistrar::getMsgContext()
{
  return mHookup->getMsgContext();
}

CarbonWaveNetAssoc* 
CarbonWaveRegistrar::getAssoc(STSymbolTableNode* node)
{
  CarbonWaveNetAssoc* assoc = NULL;
  if (node)
  {
    ShellGlobal::lockMutex();
    ShellData* shellData = CarbonWaveNetAssoc::sGetShellData(node);
    if (shellData)
      assoc = shellData->getNetAssoc(mHookup->getId());
    ShellGlobal::unlockMutex();
  }
  return assoc;
}

NetFlags CarbonWaveRegistrar::sGetNetFlags(const STSymbolTableNode* node)
{
  const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);
  return bomdata->getNetFlags();
}

ShellNet* CarbonWaveRegistrar::getShellNet(STAliasedLeafNode* leaf) {
  return mHookup->getCarbonNet(leaf);
}

bool CarbonWaveRegistrar::requiresDebugSchedule(const STAliasedLeafNode* leaf) const
{
  const ShellDataBOM* bomdata = ShellSymTabBOM::getStorageDataBOM(leaf);
  return bomdata->needsDebugSched();
}

bool CarbonWaveRegistrar::isStateIONode(const STAliasedLeafNode* leaf) const
{
  NetFlags flags = sGetNetFlags(leaf);
  bool isStateIO = NetIsFlop(flags) || NetIsLatch(flags);
  const IODB* db = mHookup->getDB();
  // also check for clockness at this level
  isStateIO = isStateIO || db->isClock(leaf);
  isStateIO = isStateIO || db->isPrimary(leaf);
  return isStateIO;
}

static CarbonVarDirection sGetDirection(NetFlags flags)
{
  NetFlags maskedFlags = NetFlags(flags & ePortMask);
  CarbonVarDirection ret = eCarbonVarDirectionImplicit;
  if (maskedFlags != 0)
  {
    if (maskedFlags == eInputNet)
      ret = eCarbonVarDirectionInput;
    else if (maskedFlags == eOutputNet)
      ret = eCarbonVarDirectionOutput;
    else if (maskedFlags == eBidNet)
      ret = eCarbonVarDirectionInout;
  }
  return ret;
}

static const StringAtom* sGetNameAtom(CarbonExpr* arg, WaveScope*)
{
  CarbonIdent* ident = arg->castIdent();
  CE_ASSERT(ident, arg);
  const SymTabIdent* symTabIdent = ident->castSymTabIdent();
  const STAliasedLeafNode* identNode = symTabIdent->getNode();
  return identNode->strObject();
}

static const STSymbolTableNode* sGetNodeForIdent(CarbonExpr* arg, WaveScope*)
{
  CarbonIdent* ident = arg->castIdent();
  CE_ASSERT(ident, arg);
  const SymTabIdent* symTabIdent = ident->castSymTabIdent();
  const STAliasedLeafNode* identNode = symTabIdent->getNode();
  return identNode;
}

static CarbonVerilogType sGetVerilogType(NetFlags flags)
{
  CarbonVerilogType verType= eVerilogTypeWire;
  switch(flags & eDeclareMask)
  {
    case eDMTimeNet:
      verType = eVerilogTypeTime;
      break;
    case eDMWireNet:
      verType = eVerilogTypeWire;
      break;
    case eDMTriNet:
      verType = eVerilogTypeTri;
      break;
    case eDMTri1Net:
      verType = eVerilogTypeTri1;
      break;
    case eDMSupply0Net:
      verType = eVerilogTypeSupply0;
      break;
    case eDMWandNet:
      verType = eVerilogTypeWAnd;
      break;
    case eDMTriandNet:
      verType = eVerilogTypeTriAnd;
      break;
    case eDMTri0Net:
      verType = eVerilogTypeTri0;
      break;
    case eDMSupply1Net:
      verType = eVerilogTypeSupply1;
      break;
    case eDMWorNet:
      verType = eVerilogTypeWOr;
      break;
    case eDMTriorNet:
      verType = eVerilogTypeTriOr;
      break;
    case eDMTriregNet:
      verType = eVerilogTypeTriReg;
      break;
    case eDMRegNet:
      verType = eVerilogTypeReg;
      break;
    case eDMRealNet:
      verType = eVerilogTypeReal;
      break;
    case eDMIntegerNet:
      verType = eVerilogTypeInteger;
      break;
  }
  return verType;
}

static const UserType* sGetBestUserType(IODBRuntime* db, STAliasedLeafNode* leaf)
{
  // Start with the actual user type
  const UserType* ut = db->getUserType(leaf);
  if (ut == NULL) {
    // See if we can get this from somewhere else.  Currently, this
    // only works for pieces of split nets.  In that case, we have
    // an expression net whose expression is a simple SymTabIdent.
    ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(leaf);
    CarbonExpr* identExpr = bomdata->getExpr();
    if (identExpr != NULL) {
      SymTabIdent* symTabIdent = identExpr->castSymTabIdent();
      // If the expression isn't a SymTabIdent, this is some other
      // case in which we can't yet determine the correct user type.
      // An example of this is bug 12466.
      //
      // At some point, we should really be able to determine the user
      // type for all nodes we dump.  Until then, punt and return no
      // user type.  That just means that we'll use other intrinsic
      // properties of the node to determine what type of waveform
      // signal to create for it.  This is basically what was done
      // before the visibility project, when there were no user types.
      if (symTabIdent != NULL) {
      // Get the backpointer expression of the original node.
        ShellSymNodeIdentBP* shellNodeIdentBP = symTabIdent->castShellSymNodeIdentBP();
        ST_ASSERT(shellNodeIdentBP, leaf);
        CarbonExpr* backPointer = shellNodeIdentBP->getBackPointer();
        // The first argument, if it exists, is the ident of the original node.
        CarbonExpr* origIdent = backPointer->getArg(0);
        if (origIdent != NULL) {
          const STSymbolTableNode* origNode = sGetNodeForIdent(origIdent, NULL /* unused */);
          ut = db->getUserType(origNode);
        }
      }
    }
  }
  return ut;
}

// Get the language that will be used for dumping this user type
static CarbonLanguageType sGetLanguageForDump(const UserType* ut)
{
  // If there's no user type, we'll dump it as Verilog.  Otherwise,
  // use the language stored in the user type.
  CarbonLanguageType lang = eLanguageTypeVerilog;
  if (ut != NULL) {
    lang = ut->getLanguageType();
  }
  return lang;
}

void CarbonWaveRegistrar::addNetToScope(WaveScope* scope, 
                                        CarbonWaveNetAssoc* waveAssoc,
                                        STAliasedLeafNode* leaf)
{
  CarbonVerilogType verType = eVerilogTypeUnknown;
  CarbonVhdlType vhdlType = eVhdlTypeUnknown;
  HdlId nameInfo;
  CarbonVarDirection direction = eCarbonVarDirectionImplicit;
  IODBRuntime* db = mHookup->getDB();
  // If the leaf is part of a port-split net, it won't have an
  // intrinsic user type.  Use the helper function to extract it from
  // the expression back pointers.
  const UserType* ut = sGetBestUserType(db, leaf);
  NetFlags flags = sGetNetFlags(leaf);

  const IODBGenTypeEntry* typeEntry = db->getLeafType(leaf);

  {
#ifdef CDB
    // We are about to crash in the assert below, might as well give
    // some info beforehand if we are going to debug it.
    if (typeEntry == NULL)
      leaf->printAliasStorage();
#endif
    if (! typeEntry)
    {
      UtString name;
      mHookup->getNodeName(&name, leaf);
      getMsgContext()->SHLNoTypeEntry(name.c_str());
    }
    const IODBIntrinsic *intrinsic = db->getLeafIntrinsic( leaf );
    if (! intrinsic)
    {
      UtString name;
      mHookup->getNodeName(&name, leaf);
      getMsgContext()->SHLTypeEntryNoIntrinsic(name.c_str());
    }
  
    const IODBIntrinsic::Type sigType = intrinsic->getType();
  
    if ((sigType == IODBIntrinsic::eScalar) || NetIsDeclaredAs(flags, eDMRealNet))
      ; // nameInfo defaults to scalar
    else if (sigType ==  IODBIntrinsic::eVector)
    {
      const ConstantRange* vecRange = intrinsic->getVecRange();
      nameInfo.setVectorParams(*vecRange);
    }

    mRunDebugSchedule |= requiresDebugSchedule(leaf);
    direction = sGetDirection(flags);
  }
  
  const StringAtom* signalName = leaf->strObject();
  if (mWalkingExpr)
  {
    // we are adding the component parts of a split net to the
    // waveform, we need to know if this is a bit select or a
    // partselect.
    // We also need the actual name (backpointer).
    ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(leaf);
    CarbonExpr* identExpr = bomdata->getExpr();
    ST_ASSERT(identExpr, leaf);
    SymTabIdent* symTabIdent = identExpr->castSymTabIdent();
    ST_ASSERT(symTabIdent, leaf);
    ShellSymNodeIdentBP* shellNodeIdentBP = symTabIdent->castShellSymNodeIdentBP();
    ST_ASSERT(shellNodeIdentBP, leaf);
    CarbonExpr* backPointer = shellNodeIdentBP->getBackPointer();
  
    CarbonBinaryOp* binaryOp = backPointer->castBinary();
    if (binaryOp)
    {
      CE_ASSERT(binaryOp->getType() == CarbonExpr::eBiBitSel, binaryOp);
      CarbonExpr* arg0 = binaryOp->getArg(0);
      ConstantRange declaredRange;
      arg0->getDeclaredRange(&declaredRange);

      CarbonExpr* arg1 = binaryOp->getArg(1);

      signalName = sGetNameAtom(arg0, scope);

      // Since this is a split port we need to get the user type
      // for the original node.
      const STSymbolTableNode *nodeSub = sGetNodeForIdent(arg0, scope);
      ut = db->getUserType(nodeSub);
      if((ut != NULL) && (ut->getType() != UserType::eArray))
        CE_ASSERT(false, arg1);
      
      CarbonConst* bitIndex = arg1->castConst();
      CE_ASSERT(bitIndex, arg1);
      DynBitVector bv;
      bitIndex->getValue(&bv);
      UInt32 valUnsigned = bv.to_ulong();
      valUnsigned = declaredRange.index(valUnsigned);
      SInt32 val = (SInt32) valUnsigned;

      nameInfo.setType(HdlId::eVectBit);
      nameInfo.setVectIndex(val);
    }
    else
    {
      CarbonPartsel* partSel = backPointer->castPartsel();
      CE_ASSERT(partSel,  backPointer);
      CarbonExpr* arg = partSel->getArg(0);
      ConstantRange declaredRange;
      arg->getDeclaredRange(&declaredRange);
      
      signalName = sGetNameAtom(arg, scope);
      
      ConstantRange range = partSel->getRange();
      range.denormalize(&declaredRange);
      nameInfo.setVectorParams(range);
    }
  }

  if(ut != NULL)
  {
    verType = ut->getVerilogType();
    if(ut->getLanguageType() == eLanguageTypeVhdl)
    {
      vhdlType = ut->getVhdlType();
      switch(ut->getType())
      {
        case UserType::eScalar:
        case UserType::eEnum:
          vhdlType = ut->getVhdlType();
          break;

        case UserType::eArray:
        {
          const UserArray*  ua = ut->castArray();
          const UserType*   elem = ua->getElementType();
          if(elem)
          {
            vhdlType = elem->getVhdlType();
            ut = elem;
          }
         
          break;
        }
        case UserType::eStruct:
        case UserType::eStructEnd:
          break;
      }
    }
  }
  else
    verType = sGetVerilogType(flags);

  WaveHandle* handle = scope->addSignal(signalName, verType, direction, nameInfo,
                                        ut, vhdlType);
  waveAssoc->setHandle(handle);
  mNodeContext->add();

  ShellNet* theNet = NULL;


  // We are checking whether all the aliases have the same language.
  // If they are, we assotiate all the aliases with master handle,
  // otherwise all of them handled separatly.
  // This could be improved by creating two handles only for each 
  // language.
  bool isSameLanguage = true;
  CarbonLanguageType leafLang = sGetLanguageForDump(ut);
  // Bug 13632 showed that it's really inefficient to walk the ring
  // every time, especially when there are nodes with thousands of
  // aliases.  After we check a ring the first time, we store the
  // result in a map keyed by the master node.
  STAliasedLeafNode* master = leaf->getMaster();
  LeafBoolMap::iterator iter = mSameLanguageMap.find(master);
  if (iter != mSameLanguageMap.end()) {
    isSameLanguage = iter->second;
  } else if (leaf->hasAliases())
  {
    STAliasedLeafNode* curLeaf = leaf;
    do
    {
       curLeaf = curLeaf->getAlias();

       // If the current leaf is a temporary (e.g. a piece of an
       // expression net), it doesn't have user type information.  Try
       // to get some reasonable user type.
       const UserType *curLeafUT = sGetBestUserType(db, curLeaf);
       CarbonLanguageType curLeafLang = sGetLanguageForDump(curLeafUT);
       if (leafLang != curLeafLang) {
         isSameLanguage = false;
       }
    } while((isSameLanguage == true) && (leaf != curLeaf));

    mSameLanguageMap[master] = isSameLanguage;
  }

  bool aliasSignals = leaf->hasAliases() && isSameLanguage;

  // do alias thang
  if (aliasSignals)
  {
    if (! waveAssoc->mIsAliased)
    {
      AliasData* aData = new AliasData();
      mAllAliases.push_back(aData);
        
      // Make this one the master. It doesn't matter which one actually
      // is the master except with noInputFlow
      STAliasedLeafNode* aLeaf = leaf;
      theNet = getShellNet(leaf);
      scheduleNet(theNet, waveAssoc);

      // mark all aliases even if they are not waveable
      bool created;
      do {
        waveAssoc->mIsAliased = true;
        aData->mAliases.push_back(aLeaf);
        aLeaf = aLeaf->getAlias();
        // This must get created so we don't fall into this method again
        // for an aliased signal
        waveAssoc = maybeCreateAssoc(aLeaf, &created);
      } while (aLeaf != leaf);
    }
  }
  else {
    theNet = getShellNet(leaf);
    scheduleNet(theNet, waveAssoc);
  }
}

void CarbonWaveRegistrar::addNetToScope(WaveScope* scope, 
                                        CarbonWaveNetAssoc* waveAssoc,
                                        CarbonDatabaseNode* node)
{
  // When User Type Population is disabled there is no info
  // for sig type, so we initialize it with eWire. When User Type
  // Population is enabled, it should be reassigned.
  CarbonVerilogType verType = eVerilogTypeWire;
  HdlId nameInfo;
  CarbonVarDirection direction = eCarbonVarDirectionImplicit;

  // Get the symtab node and user type.  We should only reach here
  // with scalars, vectors, and enums, i.e. types that don't need to
  // be expanded before being added to the waveform.
  const STSymbolTableNode* stNode = node->getSymTabNode();
  const UserType *ut = node->getUserType();
  ST_ASSERT(!sMustExpandUserType(ut), stNode);
  CarbonVhdlType vtype = ut->getVhdlType();
  verType = ut->getVerilogType();

  // Get the ShellNet for this node
  CarbonModel* model = mHookup->getCarbonModel();
  CarbonDatabaseRuntime* cdr = model->getDBAPI();
  // This may be an expression net, so go through the CarbonDatabase.
  // It should not be a memory, and it's for wave dumping.
  ShellNet* net = cdr->getCarbonNet(node, false, true);
  if (net != NULL)
  {
    if (net->isScalar())
    {
      // Nothing to do. By default HdlId is setup as a scalar.
    }
    else if (net->isVector())
    {
      // Set the HdlId's msb/lsb.
      nameInfo.setVectLSB(net->getLSB());
      nameInfo.setVectMSB(net->getMSB());
      nameInfo.setType(HdlId::eVectBitRange);
    }
    else
    {
      ST_ASSERT(0, stNode); // Unsupported composite base type.
    }
  } else {
    return; // Couldn't find the shell net for this composite leaf. This net
    // will not appear in the waveform.
  }
  
  // Add this to the waveform, using the name of the DB node.
  const StringAtom* signalName = node->getName();
  ST_ASSERT(signalName != NULL, stNode);
  WaveHandle* handle = scope->addSignal(signalName, verType, direction, nameInfo,
                                        ut, vtype);
  waveAssoc->setHandle(handle);
  mNodeContext->add();
  scheduleNet(net, waveAssoc);
}

void CarbonWaveRegistrar::putInNonPodCheckList(CarbonWaveNetAssoc* assoc)
{
  mCarbonVC->addNonPodCheck(assoc);
}

void CarbonWaveRegistrar::putInPodCheckList(CarbonWaveNetAssoc* assoc)
{
  mCarbonVC->addPodCheck(assoc);
}

void CarbonWaveRegistrar::scheduleNet(ShellNet* theNet, CarbonWaveNetAssoc* assoc)
{
  assoc->setNet(theNet);
    
  if (! theNet)
    return;
 
  CarbonExamineScheduler* examineSched = mHookup->getCarbonExamineScheduler();
  CarbonExamineScheduler::NetInfo netInfo;
  ScheduleStimuli* schedStim = examineSched->getScheduleStimuli(theNet, &netInfo);

  if (netInfo.isConstant())
    return;

  if (netInfo.isNonPOD())
    putInNonPodCheckList(assoc);
  else if ( (mAlwaysUpdate) || (schedStim == NULL) )
    putInPodCheckList(assoc);
  else
  {
    WaveScheduleGroup* group = mCarbonVC->addSchedStimGroup(schedStim);
    group->addHandle(assoc);
  }
}

CarbonWaveNetAssoc* 
CarbonWaveRegistrar::maybeCreateAssoc(STSymbolTableNode* node, bool* isCreated)
{
  INFO_ASSERT(node, "NULL node");

  ShellData* shellData = CarbonWaveNetAssoc::sGetOrCreateShellData(node);
  CarbonWaveNetAssoc* waveAssoc = maybeCreateAssoc(shellData, isCreated);
  return waveAssoc;
}

CarbonWaveNetAssoc* CarbonWaveRegistrar::maybeCreateAssoc(CarbonDatabaseNode* node, bool* isCreated)
{
  INFO_ASSERT(node, "NULL node");

  ShellData* shellData = CarbonWaveNetAssoc::sGetOrCreateShellData(node);
  CarbonWaveNetAssoc* waveAssoc = maybeCreateAssoc(shellData, isCreated);
  return waveAssoc;
}

CarbonWaveNetAssoc* CarbonWaveRegistrar::maybeCreateAssoc(ShellData* shellData, bool* isCreated)
{
  *isCreated = false;

  ShellGlobal::lockMutex();
  CarbonWaveNetAssoc* waveAssoc = shellData->getNetAssoc(mHookup->getId());;
  ShellGlobal::unlockMutex();
  
  if (! waveAssoc)
  {
    waveAssoc = new CarbonWaveNetAssoc;
    ShellGlobal::lockMutex();
    shellData->putNetAssoc(waveAssoc, mHookup->getId());
    ShellGlobal::unlockMutex();
    mAllAssocs.push_back(waveAssoc);
    *isCreated = true;
  }
  
  return waveAssoc;
}

void CarbonWaveRegistrar::addCompositeToWave(CarbonDatabaseNode* node,
                                             const UserType* ut)
{
  if (ut != NULL) // VHDL composites have user type info.
  {
    // If this is an array, we can only add this if the waveform type
    // supports array scopes.  FSDB does, but VCD doesn't.  If it's a
    // structure, it doesn't matter.  We've been dumping records to
    // VCD for a long time, treating them the same as any other named
    // scope.  Customers may be counting on that, so we'll persist it.
    UserType::Type utType = ut->getType();
    switch (utType)
    {
    case UserType::eArray:
      if (!mDataFile->supportsArrayScopes()) {
        return;
      }
      // Fall through
    case UserType::eStruct:
    {
      // Dump Size Limit
      if((mDumpSizeLimit == 0) ||             // No Limitation on Size
         (ut->getSize() <= mDumpSizeLimit)) { // Satisfies the Limit

        CarbonModel* model = mHookup->getCarbonModel();
        CarbonDatabaseRuntime* cdr = model->getDBAPI();
        // Ensure that all children of the composite (array elements and
        // record fields) have nodes in the visibility database.
        cdr->elabDBNode(node);
        WaveScope* childScope = generateScopes(node);
        addDownNets(node, childScope);
      }
      break;
    }
    default:
      ST_ASSERT(0, node->getSymTabNode());
      break;
    }
  }
}

void CarbonWaveRegistrar::addScopeToWave(STBranchNode* chBr, unsigned int depth,
                                         bool doAll)
{
  CarbonModel* model = mHookup->getCarbonModel();
  CarbonDatabaseRuntime* cdr = model->getDBAPI();
  IODBRuntime* db = mHookup->getDB();
  const UserType* ut = db->getUserType(chBr);

  if (sMustExpandUserType(ut))
  {
    // This is an array or struct.  We should be able to create a DB
    // node to represent it.
    CarbonDatabaseNode *dbNode = cdr->translateToDB(chBr);
    ST_ASSERT(dbNode, chBr);
    // Elaborate the composite children (array elements and record fields)
    // and add them to visibility database. Iterate through them and
    // add WaveScopes and WaveHandles for all the children and schedule
    // waveform dumping for them.
    addCompositeToWave(dbNode, ut);
  }
  else
  {
    WaveScope* childScope = generateScopes(chBr);
    addDownNets(chBr, childScope, depth, doAll);
  }
}

void CarbonWaveRegistrar::addLeafToWave(STAliasedLeafNode* chLf, WaveScope* brScope)
{
  if (mWalkingExpr || isWaveAbleNet(chLf))
  {
    // check if this node is described by an expression.
    CarbonExpr* concatExpr = NULL;

    concatExpr = sGetConcatExpression(chLf);

    if (concatExpr != NULL)
    {
      CheckExprCompleteWalk check;
      check.visitExpr(concatExpr);
      if (check.isCompleteExpr())
      {
        // it is a split net. Walk it and add the blasted parts.
        ++mWalkingExpr;
        SplitNetWalker walk(this, brScope);
        walk.visitExpr(concatExpr);
        --mWalkingExpr;
        ST_ASSERT(mWalkingExpr >= 0, chLf);
      }
      // else just skip it.
    }
    else
    {
      // Currently, the only non-concat expression that exists is a
      // vectorized expression (a bit or partselect of another
      // internal net). We don't need to do anything special for
      // that. scheduleNet() will just put that in the non pod list
      // and it will be evaluated on every waveSchedule call.

      CarbonWaveNetAssoc* chassoc;
      bool created;
      
      chassoc = maybeCreateAssoc(chLf, &created);
      if (chassoc->getHandle() == NULL)
        addNetToScope(brScope, chassoc, chLf);
    }
  }
  else
  {
    // This might be a composite leaf (i.e. array of scalars)
    IODBRuntime* db = mHookup->getDB();
    const UserType* ut = db->getUserType(chLf);
    // Create a DB node for the leaf so we can figure out what to do with it
      CarbonModel* model = mHookup->getCarbonModel();
      CarbonDatabaseRuntime* cdr = model->getDBAPI();
      CarbonDatabaseNode *dbNode = cdr->translateToDB(chLf);
      ST_ASSERT(dbNode, chLf);

    if (sMustExpandUserType(ut)) {
      addCompositeToWave(dbNode, ut);       // the usertype info says this must be expanded, so do so
    } else {
      UInt32 numPacked = 0;
      UInt32 numUnpacked = 0 ;
      CarbonDatabase::sGetPackedUnpackedDimensionCount(dbNode, &numPacked, &numUnpacked, NULL);
      if ( ( 1 < numPacked ) && ( 0 == numUnpacked ) ) {
        // this is represented in the db as a 2 D object (because it had more than 1 dimensions) but
        // because it only has packed dimensions we can add it to the wavedump as a single net
        addNodeToWave(dbNode, brScope);
    }
  }
  }
}

void CarbonWaveRegistrar::addNodeToWave(CarbonDatabaseNode* node, WaveScope* brScope)
{
  // This is similar to the simple branch of the STAliasedLeafNode
  // version of addNodeToWave().
  CarbonWaveNetAssoc* chassoc;
  bool created;
      
  chassoc = maybeCreateAssoc(node, &created);
  if (chassoc->getHandle() == NULL) {
    addNetToScope(brScope, chassoc, node);
  }
}

void CarbonWaveRegistrar::addDownNets(STBranchNode* bnode, WaveScope* bScope, 
                                      unsigned int depth, 
                                      bool doAll)
{
  UtStackPOD<STBranchNode*> branches;
  STSymbolTableNode* symNode;
  STBranchNode* chBr;
  STAliasedLeafNode* chLf;
  SInt32 index;
  STBranchNodeIter bIter(bnode);
  IODB* iodb = mHookup->getDB();

  // We are entering a hidden node if we are a child of a hidden node
  // or if the node is explicitly hidden
  bool currentScopeHidden = mNodeContext->isInHidden() ||
    iodb->isModuleHidden(bnode);

  if (currentScopeHidden)
    mNodeContext->pushHidden();
  
  {
    typedef UtHashSet<STSymbolTableNode*> SymtabNodeSet;
    
    SymtabNodeSet primaries;
    switch (mNodeType)
    {
    case eAnyNode:
      break;
    case eStateIONode:
      // Run through all the i/os and add them if bnode is a top level
      // module, carefully check for actual primaries
      if (bnode->getParent() == NULL)
      {
        for (IODB::NameSetLoop p = iodb->loopInputs(); ! p.atEnd(); ++p)
        {
          STSymbolTableNode* cur = *p;
          if (cur->getParent() == bnode)
            primaries.insert(*p);
        }

        for (IODB::NameSetLoop p = iodb->loopBidis(); ! p.atEnd(); ++p)
        {
          STSymbolTableNode* cur = *p;
          if (cur->getParent() == bnode)
            primaries.insert(*p);
        }

        for (IODB::NameSetLoop p = iodb->loopOutputs(); ! p.atEnd(); ++p)
        {
          STSymbolTableNode* cur = *p;
          if (cur->getParent() == bnode)
            primaries.insert(*p);
        }
          
        
        for (SymtabNodeSet::UnsortedLoop q = primaries.loopUnsorted();
             ! q.atEnd(); ++q)
        {
          STSymbolTableNode* cur = *q;
          chLf = cur->castLeaf();
          ST_ASSERT(chLf, cur);
          addLeafToWave(chLf, bScope);
        }
      }
      break;
    }
  }
    
  if(doAll || (depth > 0))
  {
    while ((symNode = bIter.next(&index)))
    {
      if ((chBr = symNode->castBranch()))
        branches.push(chBr);
      else if ((chLf = symNode->castLeaf()))
        addLeafToWave(chLf, bScope);
    } // while
      
    if (! doAll)
      --depth;
    while (! branches.empty())
    {
      chBr = branches.top();
      branches.pop();
        
      addScopeToWave(chBr, depth, doAll);
    } // while
  } // if depth

  if (currentScopeHidden)
    mNodeContext->popHidden();
}
  
void CarbonWaveRegistrar::addDownNets(CarbonDatabaseNode* node, WaveScope* bScope)
{
  // This is similar to (and partially copied from) the
  // STAliasedLeafNode version of addDownNets, but it's simpler
  // because:
  //
  // - No need to worry about hidden nets and the NodeContext
  //   object. That only needs to be managed when entering a hidden
  //   module.  CarbonDatabaseNodes are only created for composites,
  //   not modules, so the NodeContext is already correct.
  //
  // - Again, since the node is a composite, it can't be the top node
  //   of the design.  There's no need to worry about adding I/Os in
  //   state/IO mode.
  //
  // - Depth limiting applies only to modules and other named scopes.
  //   Once we hit a composite we dump the whole thing.
  //
  // - To maintain ordering of fields within structures, scopes and
  // - nets are added as they're seen, instead of pushing scopes onto
  // - a stack to be processed later.

  const CarbonDatabaseNode* child;
  CarbonDatabaseNodeChildIter iter(node);
  iter.populate(NULL, NULL);

  while ((child = iter.next()) != NULL)
  {
    const UserType* ut = child->getUserType();
    // If we don't need to expand this user type, we can add it
    // directly to the waveform.  Otherwise, add it to the list of
    // branches, so a scope can be created for it.
    if (!sMustExpandUserType(ut)) {
      addNodeToWave(const_cast<CarbonDatabaseNode*>(child), bScope);
    } else {
      const UserType* ut = child->getUserType();
      addCompositeToWave(const_cast<CarbonDatabaseNode*>(child), ut);
    }
  } // while
}
  
WaveScope* CarbonWaveRegistrar::generateScopes(STBranchNode* bnode)
{
  STScopeWalker walker(bnode, &mBranchToScope);
  walker.walk();
  WaveScope* scope = walkScopes(&walker);
  return scope;
}

WaveScope* CarbonWaveRegistrar::generateScopes(CarbonDatabaseNode* node)
{
  DBScopeWalker walker(node, &mDBNodeToScope, &mBranchToScope);
  walker.walk();
  WaveScope* scope = walkScopes(&walker);
  return scope;
}

WaveScope* CarbonWaveRegistrar::walkScopes(ScopeWalker* walker)
{
  WaveScope* scope = walker->getStartScope();
  while(!walker->empty())
  {
    bool replaced = false;
    //assoc = getAssoc(hier);
      
    // If this is the first entry (top) and we have a prefix, then add that.
    if ((scope == NULL) && (mPrefixHier != NULL)) {
      scope = mPrefixHier;
      if (mReplaceDesignRoot) {
        walker->mapNodeToScope(scope);
        replaced = true;
      }
    }

    // Add the new scope unless we just replaced the top scope
    if (!replaced) {
      // Differentiate between scopes of array, struct and module kinds.
      WaveScope::ScopeType st = WaveScope::eModule;
      // Look up user type
      const UserType* ut = walker->getNodeType();
      if (ut != NULL)
      {
        switch (ut->getType())
        {
          case UserType::eArray:
            st = WaveScope::eArray;
            break;
          case UserType::eStruct:
            st = WaveScope::eStruct;
            break;
          default:
            // Can't reach, because we validate this in getNodeType()
            break;
        }
      }

      const StringAtom* strAtom = walker->getNodeName();
      CarbonLanguageType lang = walker->getNodeLanguage();
      scope = mDataFile->attachScope(strAtom, scope, st, ut, lang);
      walker->mapNodeToScope(scope);
    }

    walker->popNode();
  }
    
  return scope;
}

