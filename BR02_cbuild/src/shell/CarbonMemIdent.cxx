// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/CarbonMemIdent.h"
#include "shell/CarbonNetIdent.h"       // for assign context stuff
#include "shell/ShellNet.h"
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STSymbolTable.h"

CarbonMemIdent::CarbonMemIdent(ShellNet* net, CarbonModel* model, bool isSigned, UInt32 size)
  : CarbonIdent(isSigned, size),
    mCarbonModel(model)
{
  putShellNet(net);
}

CarbonMemIdent::~CarbonMemIdent()
{
}

ptrdiff_t CarbonMemIdent::compare(const CarbonExpr* other) const
{
  // We can assume the other expression is a CarbonIdent, but it may
  // not be a CarbonMemIdent.
  const CarbonIdent *otherIdent = static_cast<const CarbonIdent*>(other);
  const CarbonMemIdent *otherMemIdent = otherIdent->castCarbonMemIdent();
  // If it's the wrong type, return a difference
  if (otherMemIdent == NULL) {
    return 1;
  }

  const HierName* myName = mShellNet->getName();
  const HierName* otherName = otherMemIdent->mShellNet->getName();
  
  ptrdiff_t ret = HierName::compare(myName, otherName);
  if (ret == 0)
  {
    if (mCarbonModel != otherMemIdent->mCarbonModel)
      ret = 1;
  }

  return ret;
}

const STAliasedLeafNode* CarbonMemIdent::getNode(DynBitVector* usageMask) const
{
  int bitWidth = mShellNet->getBitWidth();
  usageMask->resize(bitWidth);
  usageMask->reset();
  usageMask->setRange(0, bitWidth, 1);
  return mShellNet->getNameAsLeaf();
}

CarbonExpr::AssignStat CarbonMemIdent::assign(ExprAssignContext*)
{
  // Can't assign to an entire memory
  CE_ASSERT(0, this);
  return eFull;
}

CarbonExpr::AssignStat CarbonMemIdent::assignRange(ExprAssignContext* context, const ConstantRange& range)
{
  int bitWidth = getBitSize();
  size_t shiftAmount = bitWidth;

  ST_ASSERT(bitWidth == mShellNet->getBitWidth(), mShellNet->getNameAsLeaf());
  // The range here is actually the memory index, and it can't span multiple indices
  CE_ASSERT(range.getLength() == 1, this);
  // Only deposits are supported
  CarbonNetIdent::AssignContext* assignContext = static_cast<CarbonNetIdent::AssignContext*>(context); 
  CE_ASSERT(assignContext->getMode() == CarbonNetIdent::AssignContext::eDeposit, this);
  // Get the value and index, and deposit to that memory word
  SInt32 index = range.getMsb();
  UInt32 *val = context->getValueArr();
  mMemoryNet->depositMemory(index, val);
  // Shift out the bits we used
  context->rshift(shiftAmount);
  
  return eFull;
}

void CarbonMemIdent::print(bool, int indent) const
{
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < indent; i++) {
    screen << " ";
  }
  screen << typeStr() << '(' << this << ") ";
  CarbonExpr::printSize(screen);
  screen << " : ";
  UtString name;
  compose(&name);
  screen << name << UtIO::endl;
}

CarbonExpr::SignT CarbonMemIdent::evaluate(ExprEvalContext*) const
{
  // Can't evaluate an entire memory.  However, don't assert.  This
  // needs to be a no-op because CarbonBinaryOp::evaluate() will
  // blindly call this before it realizes it actually needs to do
  // evaluateRange().
  return eUnsigned;
}

const char* CarbonMemIdent::typeStr() const
{
  return "CarbonMemIdent";
}

void CarbonMemIdent::composeIdent(ComposeContext* context) const
{
  UtString* str = context->getBuffer();
  ComposeMode mode = context->getMode();
  const STAliasedLeafNode* node = mShellNet->getNameAsLeaf();
  switch(mode)
  {
  case eComposeNormal:
  case eComposeC:
    node->verilogCompose(str);
    break;
  case eComposeLeaf:
    node->verilogComposeLeaf(str);
    break;
  }
}

const CarbonMemIdent* CarbonMemIdent::castCarbonMemIdent() const
{
  return this;
}

bool CarbonMemIdent::isWholeIdentifier() const
{
  return true;
}

CarbonExpr::SignT CarbonMemIdent::evaluateRange(ExprEvalContext* context, const ConstantRange& range)
{
  int bitWidth = getBitSize();

  ST_ASSERT(bitWidth == mShellNet->getBitWidth(), mShellNet->getNameAsLeaf());
  // The range here is actually the memory index, and it can't span multiple indices
  CE_ASSERT(range.getLength() == 1, this);
  // Get the value and index, and examine that memory word
  SInt32 index = range.getMsb();
  UInt32 *val = context->getValue()->getUIntArray();
  mMemoryNet->examineMemory(index, val);
  // Memories can't be tristates, so the drive is always 0
  DynBitVector *driveBV = context->getDrive();
  driveBV->reset();

  return eUnsigned;
}

const ShellNet* CarbonMemIdent::getShellNet() const
{
  return mShellNet;
}

void CarbonMemIdent::putShellNet(ShellNet* replacement)
{
  mShellNet = replacement;
  mMemoryNet = mShellNet->castMemory();
  INFO_ASSERT(mMemoryNet, "Not a memory net");
}
