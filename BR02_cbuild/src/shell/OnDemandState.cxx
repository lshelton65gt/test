// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/OnDemandState.h"
#include "util/Util.h"

OnDemandState::OnDemandState(CarbonUInt32 size)
{
  mSize = size;
  allocateBuffer();
  mCurrIndex = 0;
  mHashVal = 0;
  mBufferedDataStart = NULL;
  mBufferedDataNext = NULL;
  mBufferedDataSize = 0;
}

OnDemandState::OnDemandState()
{
  mSize = 0;
  mNumUInt32 = 0;
  mData = NULL;
  mCurrIndex = 0;
  mHashVal = 0;
  mBufferedDataStart = NULL;
  mBufferedDataNext = NULL;
  mBufferedDataSize = 0;
}

OnDemandState::~OnDemandState()
{
  if (mData)
    CARBON_FREE_VEC(mData, CarbonUInt8, mNumUInt32 * sizeof(UInt32));
}

void OnDemandState::add(CarbonUInt32 num_bytes, const CarbonUInt8 *data)
{
  if (mSize) {
    // We've already allocated the buffer
    INFO_ASSERT(mCurrIndex + num_bytes <= mSize, "onDemand state data is incorrect size");

    // The checkpoint write functions pass everything by reference,
    // so we end up seeing the address of the actual storage for
    // each net.  Codegen declares all the onDemand state variables
    // together, and the layout order matches the order in which the
    // variables are saved/restored for onDemand.
    // The result of this is that we can track the address/size
    // of requests, and accumulate one large region that we can
    // memcpy.

    if (mBufferedDataSize) {
      // We already have some write data pending.
      // See if this continues the same memory region.
      if (mBufferedDataNext == data) {
        // Add to our buffer
        mBufferedDataSize += num_bytes;
        mBufferedDataNext = &mBufferedDataStart[mBufferedDataSize];
      } else {
        // Oops, need to write out the current buffer
        // and start a new one
        writeBufferedData();
      }
    }

    // Either we haven't done anything yet, or we've just cleared
    // an old buffer.  Initialize a new buffer pointer
    if (!mBufferedDataSize) {
      mBufferedDataStart = data;
      mBufferedDataSize = num_bytes;
      mBufferedDataNext = &mBufferedDataStart[mBufferedDataSize];
    }

  } else {
    // We haven't allocated a buffer yet because we don't know
    // how big the state data is yet.  We store it in a temp
    // array and allocate the storage later.
    for (CarbonUInt32 i = 0; i < num_bytes; ++i)
      mTempArray.push_back(data[i]);
  }

  // Calculate a running "checksum" of the data as it's added.
  // This will be used as the hash value for this state.
  //
  // Note that we're only using one byte per word of data.  This is a
  // compromise between speed and the probability of generating a
  // meaningful hash value.
  for (UInt32 i = 0; i < num_bytes; i += 4) {
    mHashVal ^= data[i];
  }
}

void OnDemandState::read(CarbonUInt32 num_bytes, CarbonUInt8 *data)
{
  INFO_ASSERT(mCurrIndex + num_bytes <= mSize, "Out-of-bounds onDemand state read");
  memcpy(data, &mData[mCurrIndex], num_bytes * sizeof(CarbonUInt8));
  mCurrIndex += num_bytes;
}

void OnDemandState::close()
{
  // Allocate our data buffer, and copy everything from the
  // temp array.
  // This can only be called once.
  INFO_ASSERT(mSize == 0, "Failed close of onDemand state data");
  mSize = mTempArray.size();
  allocateBuffer();
  reset();       // reset for future read
  for (CarbonUInt32 i = 0; i < mSize; ++i)
    mData[i] = mTempArray[i];
}

void OnDemandState::flush()
{
  // Flush any data that was buffered but not
  // actually copied
  if (mBufferedDataSize)
    writeBufferedData();
  reset();
}

void OnDemandState::reset()
{
  // reset our pointer so we're ready to restore
  mCurrIndex = 0;
}

void OnDemandState::recalculateHashValue()
{
  // WARNING!
  //
  // Normally, the hash value is calculated using every fourth byte as
  // data is added to the state buffer, which can happen several
  // times, since the state is saved in chunks of contiguous memory.
  // In this routine, we use every fourth byte in a single pass of the
  // entire state buffer.  This means that two identical state buffers
  // will have different hash values if one was recalculated and the
  // other wasn't!
  //
  // Care must be taken to ensure that all states stored in the
  // OnDemandStateSet have their hash values calculated in the same
  // way.
  mHashVal = 0;
  for (UInt32 i = 0; i < mSize; i += 4) {
    mHashVal ^= getValue(i);
  }
}

bool OnDemandState::operator==(const OnDemandState &other) const
{
  // We need to compare the full state data here
  INFO_ASSERT(mSize == other.mSize, "Comparison of state dumps of inequal sizes");
  // We padded our buffer to a UInt32 boundary, so we can safely treat
  // the buffer that way.
  return (carbon_duffcmp(reinterpret_cast<UInt32*>(mData), &reinterpret_cast<UInt32*>(mData)[mNumUInt32], reinterpret_cast<UInt32*>(other.mData)) == 0);
}

void OnDemandState::writeBufferedData()
{
  // Copy whatever memory region we've been accumulating
  // into the actual storage.
  if (mBufferedDataSize == 1)
    mData[mCurrIndex] = *mBufferedDataStart;
  else
    memcpy(&mData[mCurrIndex], mBufferedDataStart, mBufferedDataSize * sizeof(CarbonUInt8));

  mCurrIndex += mBufferedDataSize;
  mBufferedDataSize = 0;
}

void OnDemandState::allocateBuffer()
{
  // Determine how many UInt32s we need to store these bytes, and
  // allocate UInt8 storage padded to that UInt32 boundary.
  mNumUInt32 = (mSize + sizeof(UInt32) - 1) / sizeof(UInt32);
  mData = CARBON_ALLOC_VEC(CarbonUInt8, mNumUInt32 * sizeof(UInt32));
  // Init to 0, since the padding will never be written yet needs to
  // compare.
  memset(mData, 0, mNumUInt32 * sizeof(UInt32));
}

OnDemandStateFactory::OnDemandStateFactory()
{
}

OnDemandStateFactory::~OnDemandStateFactory()
{
  clear();
}

OnDemandState *OnDemandStateFactory::create()
{
  OnDemandState *ptr = new OnDemandState;
  mAllocated.push_back(ptr);
  return ptr;
}

OnDemandState *OnDemandStateFactory::create(CarbonUInt32 size)
{
  OnDemandState *ptr = new OnDemandState(size);
  mAllocated.push_back(ptr);
  return ptr;
}

void OnDemandStateFactory::freeLast()
{
  if (!mAllocated.empty()) {
    delete mAllocated.back();
    mAllocated.pop_back();
  }
}

void OnDemandStateFactory::clear()
{
  for (CarbonUInt32 i = 0; i < mAllocated.size(); ++i)
    delete mAllocated[i];
  mAllocated.clear();
}

size_t OnDemandStateSetHelper::hash(const OnDemandState *var) const
{
  // We accumulated the hash value as data was added
  return var->mHashVal;
}

bool OnDemandStateSetHelper::equal(const OnDemandState *v1, const OnDemandState *v2) const
{
  return *v1 == *v2;
}
