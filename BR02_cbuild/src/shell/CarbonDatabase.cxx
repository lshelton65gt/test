// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "CarbonDatabase.h"
#include "CarbonDatabaseNode.h"
#include "CarbonDatabaseIter.h"
#include "iodb/IODBRuntime.h"
#include "iodb/ScheduleFactory.h"
#include "iodb/IODBUserTypes.h"
#include "shell/ShellGlobal.h"
#include "shell/CarbonDBRead.h"
#include "shell/CarbonModel.h"
#include "shell/ShellNet.h"
#include "shell/VisNetFactory.h"
#include "shell/carbon_capi.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtStackPOD.h"
#include "util/AtomicCache.h"
#include "util/StringAtom.h"
#include "util/ShellMsgContext.h"
#include "util/RandomValGen.h"
#include "exprsynth/ExprFactory.h"
#include "hdl/HdlHierPath.h"
#include "hdl/HdlId.h"

struct CarbonDatabase::Aux
{
  CARBONMEM_OVERRIDES

  IODB::NameSet mBothEdgeTriggers;
};

CarbonDatabase::CarbonDatabase()
{
  mAux = new Aux;

  mDBNodeFactory = NULL;
}

CarbonDatabase::~CarbonDatabase()
{
  delete mDBNodeFactory;
  delete mAux;
}

IODBRuntime* CarbonDatabase::getDB()
{ 
  return mDB;
}

const IODBRuntime* CarbonDatabase::getDB() const
{ 
  return mDB;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopPrimaryPorts()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabVecIter iter(mDB->loopPrimaryPorts());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopPrimaryInputs()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopInputs());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopPrimaryOutputs()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopOutputs());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopPrimaryBidis()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopBidis());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopAsyncs()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopAsyncs());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopAsyncOutputs()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabMapIter iter(mDB->loopAsyncOutputs());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopAsyncFanin(const CarbonDatabaseNode* node)
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    CarbonDatabaseSymtabSetIter iter(mDB->loopAsyncFanin(stNode));
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopAsyncDeposits()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopAsyncDeposits());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopAsyncPosResets()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopAsyncPosResets());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopAsyncNegResets()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopAsyncNegResets());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopPrimaryClks()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopPrimaryClocks());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopClkTree()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopClockTree());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopObservable()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopObserved());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopScObservable()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopScObserved());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopMvObservable()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopMvObserved());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopDepositable()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopDeposit());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopScDepositable()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopScDeposit());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopMvDepositable()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopMvDeposit());
    ret = createLoop(&iter);
  }
  return ret;
}

int CarbonDatabase::isWrapperPort2State(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isWrapperPort2State(stNode);
  }
  return ret;
}

int CarbonDatabase::isWrapperPort4State(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isWrapperPort4State(stNode);
  }
  return ret;
}

int CarbonDatabase::isPrimaryInput(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isPrimaryInput(stNode);
  }
  return ret;
}

int CarbonDatabase::isPrimaryOutput(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isPrimaryOutput(stNode);
  }
  return ret;
}

int CarbonDatabase::isPrimaryBidi(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isPrimaryBidirect(stNode);
  }
  return ret;
}

int CarbonDatabase::isInput(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isInput(stNode);
  }
  return ret;
}

int CarbonDatabase::isOutput(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isOutput(stNode);
  }
  return ret;
}

int CarbonDatabase::isBidi(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isBidirect(stNode);
  }
  return ret;
}

int CarbonDatabase::is2DArray(const CarbonDatabaseNode* node) const
{
  // If there is user type information, use that.  Otherwise revert to the DB.
  int ret = 0;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    const UserArray *arrayType = type->castArray();
    if (arrayType != NULL) {
      ret = arrayType->isMemory();
    }
  } else if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->is2DArray(stNode);
  }
  return ret;
}

// Since it's possible for a NUMemoryNet to consist only of packed 
// dimensions (SystemVerilog), e.g.
//
//    reg [15:0][3:0] mem;
//
// We need to distinguish these from arrays that have unpacked
// dimensions, e.g.
//
//    reg [3:0] mem2 [15:0];
//
// CarbonDatabaseNode* node will point to a 'reference' to one
// of these variables (for example). The value returned by
// 'isUnpackedArray' will depend on how many indices are supplied 
// to the reference. For example:
//
// mem[3]   - will return false (it's a 1-d packed array)
// mem      - will return false (it's a 2-d packed array)
// mem2     - will return true
// mem2[15] - will return false (it's a packed array)

int CarbonDatabase::isUnpackedArray(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    const UserArray *arrayType = type->castArray();
    if (arrayType != NULL) {
      // arrayType points to one dimension of a possible multi-d
      // array. The dimension is either packed or unpacked. If
      // packed, then there can be no 'contained' unpacked dimensions.
      // Therefore it's strictly a packed array. If it's not packed, it
      // must be unpacked. (In this case, there can be 'contained'
      // packed dimensions.)
      ret = !arrayType->isPackedArray();
    }
  } else if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    // is it a memory?
    ret = mDB->is2DArray(stNode);
  }
  return ret;
}

int CarbonDatabase::isVector(const CarbonDatabaseNode* node) const
{
  // If there is user type information, use that.  Otherwise revert to the DB.
  int ret = 0;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    // It's a vector if it's a 1-D array whose element type is scalar
    const UserArray *arrayType = type->castArray();
    if (arrayType != NULL) {
      ret = arrayType->isVector();
    }
  } else if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isVector(stNode);
  }
  return ret;
}

int CarbonDatabase::isScalar(const CarbonDatabaseNode* node) const
{
  // If there is user type information, use that.  Otherwise revert to the DB.
  int ret = 0;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    ret = (type->getType() == UserType::eScalar);
  } else if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isScalar(stNode);
  }
  return ret;
}

int CarbonDatabase::isTristate(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isTristate(stNode);
  }
  return ret;
}

int CarbonDatabase::isConstant(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isConstant(stNode);
  }
  return ret;
}

int CarbonDatabase::isClk(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isMarkedClock(stNode);
  }
  return ret;
}

int CarbonDatabase::isClkTree(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isClockTree(stNode);
  }
  return ret;
}

int CarbonDatabase::isAsync(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isRuntimeAsync(stNode);
  }
  return ret;
}

int CarbonDatabase::isAsyncOutput(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isRuntimeAsyncOutput(stNode);
  }
  return ret;
}

int CarbonDatabase::isAsyncDeposit(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isRuntimeAsyncDeposit(stNode);
  }
  return ret;
}

int CarbonDatabase::isAsyncPosReset(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isPosedgeReset(stNode);
  }
  return ret;
}

int CarbonDatabase::isAsyncNegReset(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isNegedgeReset(stNode);
  }
  return ret;
}

int CarbonDatabase::getWidth(const CarbonDatabaseNode* node) const
{
  // If there is user type information, use that.  Otherwise revert to the DB.
  int ret = 0;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    const UserScalar *scalar = type->castScalar();
    const UserEnum *e = type->castEnum();
    const UserArray *array = type->castArray();
    if ((scalar != NULL) || (e != NULL)) {
      // For these, the width is the same as their size
      ret = type->getSize();
    } else if (array != NULL) {
      const ConstantRange *range = array->getVectorRange();
      if (range != NULL) {
        ret = range->getLength();
      }
    }
  } else if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->getWidth(stNode);
  }
  return ret;
}

int CarbonDatabase::getMSB(const CarbonDatabaseNode* node) const
{
  // If there is user type information, use that.  Otherwise revert to the DB.
  int ret = 0;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    const UserArray *array = type->castArray();
    if (array != NULL) {
      const ConstantRange *range = array->getVectorRange();
      if (range != NULL) {
        ret = range->getMsb();
      }
    }
  } else if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->getMsb(stNode);
  }
  return ret;
}

int CarbonDatabase::getLSB(const CarbonDatabaseNode* node) const
{
  // If there is user type information, use that.  Otherwise revert to the DB.
  int ret = 0;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    const UserArray *array = type->castArray();
    if (array != NULL) {
      const ConstantRange *range = array->getVectorRange();
      if (range != NULL) {
        ret = range->getLsb();
      }
    }
  } else if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->getLsb(stNode);
  }
  return ret;
}

int CarbonDatabase::get2DArrayLeftAddr(const CarbonDatabaseNode* node) const
{
  // If there is user type information, use that.  Otherwise revert to the DB.
  int ret = 0;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    const UserArray *array = type->castArray();
    if (array != NULL) {
      const ConstantRange *range = array->getMemoryRange();
      if (range != NULL) {
        ret = range->getMsb();
      }
    }
  } else if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->get2DArrayLeftAddr(stNode);
  }
  return ret;
}

int CarbonDatabase::get2DArrayRightAddr(const CarbonDatabaseNode* node) const
{
  // If there is user type information, use that.  Otherwise revert to the DB.
  int ret = 0;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    const UserArray *array = type->castArray();
    if (array != NULL) {
      const ConstantRange *range = array->getMemoryRange();
      if (range != NULL) {
        ret = range->getLsb();
      }
    }
  } else if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->get2DArrayRightAddr(stNode);
  }
  return ret;
}

const char* CarbonDatabase::getInterfaceName() const
{
  const char* ret = "";
  if (mDB)
    ret = mDB->getIfaceTag();
  return ret;
}

const char* CarbonDatabase::getTopLevelModuleName() const
{
  const char* ret = "";
  if (mDB)
    ret = mDB->getTopLevelModuleName();
  return ret;
}

const char* CarbonDatabase::getSystemCModuleName() const
{
  const char* ret = "";
  if (mDB)
    ret = mDB->getSystemCModuleName();
  return ret;
}

const char* CarbonDatabase::getFullName(const CarbonDatabaseNode* node)
{
  mReturnBuffer.clear();
  if (node != NULL) {
    node->composeFullName(&mReturnBuffer);
  }
  return mReturnBuffer.c_str();
}

const char* CarbonDatabase::getLeafName(const CarbonDatabaseNode* node)
{
  // We state that the pointer returned by this function (unlike
  // getFullName()) is internally managed, so the user doesn't need to
  // make a copy of it.  Each node must manage its own name.
  return node->getLeafName();
}


// Looping over the symbol table roots
CarbonDatabaseNodeIter* CarbonDatabase::loopDesignRoots() {
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabRootIter iter(mDesignSymTab->getRootIter());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopChildren(const CarbonDatabaseNode* node)
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    populateChildren(const_cast<CarbonDatabaseNode*>(node));
    ret = new CarbonDatabaseNodeChildIter(node);
    // Populate the iterator's internal node list.
    ret->populate(NULL, NULL);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopAliases(const CarbonDatabaseNode* node)
{
  CarbonDatabaseNodeIter* ret = NULL;
  const STSymbolTableNode *stNode = node->getSymTabNode();
  const STAliasedLeafNode* leaf = stNode->castLeaf();
  if (mDB && leaf) {
    STAliasedLeafNode::AliasLoop loop(const_cast<STAliasedLeafNode*>(leaf));
    CarbonDatabaseAliasIter iter(loop);
    ret = createLoop(&iter);
  }
  return ret;
}

int CarbonDatabase::isLeaf(const CarbonDatabaseNode* node) const {
  const STSymbolTableNode *stNode = node->getSymTabNode();
  return stNode->castLeaf() != NULL;
}

int CarbonDatabase::isBranch(const CarbonDatabaseNode* node) const {
  const STSymbolTableNode *stNode = node->getSymTabNode();
  return stNode->castBranch() != NULL;
}

int CarbonDatabase::isForcible(const CarbonDatabaseNode* node) const {
  int ret = 0;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isForcible(stNode);
  }
  return ret;
}

int CarbonDatabase::isDepositable(const CarbonDatabaseNode* node) const {
  int ret = 0;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isDepositable(stNode);
  }
  return ret;
}

int CarbonDatabase::isScDepositable(const CarbonDatabaseNode* node) const {
  int ret = 0;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isScDepositable(stNode);
  }
  return ret;
}

int CarbonDatabase::isMvDepositable(const CarbonDatabaseNode* node) const {
  int ret = 0;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isMvDepositable(stNode);
  }
  return ret;
}

int CarbonDatabase::isObservable(const CarbonDatabaseNode* node) const {
  int ret = 0;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isObservable(stNode);
  }
  return ret;
}

int CarbonDatabase::isScObservable(const CarbonDatabaseNode* node) const {
  int ret = 0;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isScObservable(stNode);
  }
  return ret;
}

int CarbonDatabase::isMvObservable(const CarbonDatabaseNode* node) const {
  int ret = 0;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isMvObservable(stNode);
  }
  return ret;
}

int CarbonDatabase::isSampleScheduled(const CarbonDatabaseNode* node) const {
  int ret = 0;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isSampleScheduled(stNode);
  }
  return ret;
}

int CarbonDatabase::isTied(const CarbonDatabaseNode* node) const {
  int ret = 0;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isTied(stNode);
  }
  return ret;
}

int CarbonDatabase::isVisible(const CarbonDatabaseNode* node) const {
  int ret = 0;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = (mDB->getSignature(stNode) != NULL);
  }
  return ret;
}

const CarbonDatabaseNode* CarbonDatabase::findChild(const CarbonDatabaseNode* parent,
                                                   const char* childName)
{
  const CarbonDatabaseNode *dbNode = NULL;
  if (parent != NULL) {
    CarbonDatabaseNode *parentNonConst = const_cast<CarbonDatabaseNode*>(parent);
    // This is potentially expensive, if (for example) this is an
    // array with a very large dimension.  However, findChild() is
    // implemented on arrays only for backwards compatibility.
    // getArrayElement() is the preferred way of looking up an array
    // element.
    populateChildren(parentNonConst);
    if (isArray(parentNonConst)) {
      // If the parent is an array, it has indexed children, not named
      // children.  Convert the name to an integer and look up the
      // child that way.
      SInt32 index;
      UtString indexStr(childName);
      if (indexStr >> index) {
        dbNode = parentNonConst->findChild(index);
      }
    } else {
      // Named children are keyed by StringAtoms, so look up this
      // string.  If the StringAtom doesn't exist, that's OK.  It
      // can't be a valid child anyway.
      //
      // The atomic cache is shared by all model instances, so access
      // needs to be controlled with a mutex.
      ShellGlobal::lockMutex();
      const StringAtom *strAtom = mAtomicCache->getIntern(childName);
      ShellGlobal::unlockMutex();
      dbNode = parentNonConst->findChild(strAtom);
    }
  }
  return dbNode;
}

const CarbonDatabaseNode* CarbonDatabase::getParent(const CarbonDatabaseNode* node) const
{
  const CarbonDatabaseNode *dbNode = NULL;
  if (node != NULL) {
    dbNode = node->getParent();
  }
  return dbNode;
}

const CarbonDatabaseNode* CarbonDatabase::getMaster(const CarbonDatabaseNode* node)
{
  const CarbonDatabaseNode* ret = NULL;
  if (node != NULL) {
    const STSymbolTableNode* stNode = node->getSymTabNode();
    const STAliasedLeafNode* stLeaf = stNode->castLeaf();
    if (stLeaf != NULL) {
      const STSymbolTableNode* stMaster = stLeaf->getMaster();
      // If the master is the same, return the node that was passed in,
      // otherwise create a CarbonDatabaseNode for the master.
      if (stMaster == stLeaf) {
        ret = node;
      } else {
        ret = translateToDB(stMaster);
      }
    }
  }
  return ret;
}

const CarbonDatabaseNode* CarbonDatabase::getStorage(const CarbonDatabaseNode* node)
{
  const CarbonDatabaseNode* ret = NULL;
  if (node != NULL) {
    const STSymbolTableNode* stNode = node->getSymTabNode();
    const STAliasedLeafNode* stLeaf = stNode->castLeaf();
    if (stLeaf != NULL) {
      const STSymbolTableNode* stStorage = stLeaf->getStorage();
      // If the storage is the same, return the node that was passed in,
      // otherwise create a CarbonDatabaseNode for the storage.
      if (stStorage == stNode) {
        ret = node;
      } else {
        ret = translateToDB(stStorage);
      }
    }
  }
  return ret;
}

const char* CarbonDatabase::intrinsicType(const CarbonDatabaseNode* node)
  const
{
  const char *ret = NULL;
  if (node != NULL) {
    // If the node has user type information, use the type name stored
    // there.  Otherwise, fall back on the declaration type stored for
    // the symtab node.
    const UserType *userType = node->getUserType();
    if (userType != NULL) {
      // This may need to access the atomic cache that is shared
      // across all models, so the ShellGlobal mutex needs to be
      // locked.  We can't lock it inside the function call, because
      // it's also used during DB creation, when the mutex is already
      // locked at a higher level.
      ShellGlobal::lockMutex();
      StringAtom *strAtom = userType->getIntrinsicTypeName();
      ShellGlobal::unlockMutex();
      ret = strAtom->str();
    } else { 
      const STSymbolTableNode *stNode = node->getSymTabNode();
      ret = mDB->declarationType(stNode);
    }

  }
  return ret;
}

const char* CarbonDatabase::declarationType(const CarbonDatabaseNode* node)
  const
{
  const char *ret = NULL;
  if (node != NULL) {
    // If the node has user type information, use the type name stored
    // there.  Otherwise, fall back on the declaration type stored for
    // the symtab node.
    const UserType *userType = node->getUserType();
    if (userType != NULL) {
      StringAtom *strAtom = userType->getTypeName();
      ret = strAtom->str();
    } else { 
      const STSymbolTableNode *stNode = node->getSymTabNode();
      ret = mDB->declarationType(stNode);
    }

  }
  return ret;
}


const char* CarbonDatabase::typeLibraryName(const CarbonDatabaseNode* node)
  const
{
  const char *ret = NULL;
  if (node != NULL) {
    const UserType *userType = node->getUserType();
    if (userType != NULL) {
      StringAtom *strAtom = userType->getLibraryName();
      if(strAtom != NULL)
        ret = strAtom->str();
    } 
  }
  return ret;
}

const char* CarbonDatabase::typePackageName(const CarbonDatabaseNode* node)
  const
{
  const char *ret = NULL;
  if (node != NULL) {
    const UserType *userType = node->getUserType();
    if (userType != NULL) {
      StringAtom *strAtom = userType->getPackageName();
      if(strAtom != NULL)
        ret = strAtom->str();
    } 
  }
  return ret;
}


const char* CarbonDatabase::componentName(const CarbonDatabaseNode* node) const
{
  const char *ret = NULL;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->componentName(stNode);
  }
  return ret;
}

const char* CarbonDatabase::sourceLanguage(const CarbonDatabaseNode* node) const
{
  const char *ret = NULL;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->sourceLanguage(stNode);
  }
  return ret;
}

const char* CarbonDatabase::getPullMode(const CarbonDatabaseNode* node) const
{
  const char* pullMode = "none";
  CarbonPullMode mode = eCarbonNoPull;
  if (node != NULL) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    mode = mDB->getPullMode(stNode);
  }
  switch (mode) {
    case eCarbonPullUp:
      pullMode = "pullup";
      break;

    case eCarbonPullDown:
      pullMode = "pulldown";
      break;

    case eCarbonNoPull:
      pullMode = "none";
      break;
  }
  return pullMode;
}

const char* CarbonDatabase::getType() const
{
  const char* dbType = "UnknownDB";
  switch (mDB->getDBType()) {
  case eCarbonFullDB:
    dbType = "FullDB";
    break;
  case eCarbonIODB:
    dbType = "IODB";
    break;
  case eCarbonGuiDB:
    dbType = "GuiDB";
    break; 
  case eCarbonAutoDB:
    // This should never happen.  Either a full or IO DB should be
    // selected when the DB is created.
    INFO_ASSERT(0, "Unexpected DB type");
    break;
  }
  return dbType;
}

int CarbonDatabase::isTemp(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isTemp(stNode);
  }
  return ret;
}


int CarbonDatabase::isReplayable() const
{
  return ((mDB->getVHMTypeFlags() & eVHMReplay) != 0);
}

int CarbonDatabase::supportsOnDemand() const
{
  return ((mDB->getVHMTypeFlags() & eVHMOnDemand) != 0);
}

CarbonDatabaseNodeIter* CarbonDatabase::loopPosedgeOnlyTriggers()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetSingleEdgeIter iter(mDB->loopPosedgeScheduleTriggers(), mAux->mBothEdgeTriggers);
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopNegedgeOnlyTriggers()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetSingleEdgeIter iter(mDB->loopNegedgeScheduleTriggers(), mAux->mBothEdgeTriggers);
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopPosedgeTriggers()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopPosedgeScheduleTriggers());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopNegedgeTriggers()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopNegedgeScheduleTriggers());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopBothEdgeTriggers()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(IODB::NameSetLoop::create(mAux->mBothEdgeTriggers.loopSorted()));
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopTriggersUsedAsData()
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    CarbonDatabaseSymtabSetIter iter(mDB->loopScheduleTriggersUsedAsData());
    ret = createLoop(&iter);
  }
  return ret;
}

CarbonDatabaseNodeIter* CarbonDatabase::loopMatching(const char* matchString)
{
  CarbonDatabaseNodeIter* ret = NULL;
  if (mDB) {
    const STSymbolTable* symtab = mDB->getDesignSymbolTable();
    CarbonDatabaseSymtabMatchIter iter(symtab, matchString);
    ret = createLoop(&iter);
  }
  return ret;
}

int CarbonDatabase::isPosedgeTrigger(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isPosedgeScheduleTrigger(stNode);
  }
  return ret;
}

int CarbonDatabase::isNegedgeTrigger(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isNegedgeScheduleTrigger(stNode);
  }
  return ret;
}

int CarbonDatabase::isBothEdgeTrigger(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mAux->mBothEdgeTriggers.count(const_cast<STSymbolTableNode*>(stNode)) != 0;
  }
  return ret;
}

int CarbonDatabase::isTriggerUsedAsData(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isScheduleTriggerUsedAsData(stNode);
  }
  return ret;
}

int CarbonDatabase::isLiveInput(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isUsedAsLiveInput(stNode) || 
      mDB->isPosedgeScheduleTrigger(stNode) ||
      mDB->isNegedgeScheduleTrigger(stNode);
  }
  return ret;
}

int CarbonDatabase::isLiveOutput(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isUsedAsLiveOutput(stNode);
  }
  return ret;
}

int CarbonDatabase::isOnDemandIdleDeposit(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isOnDemandIdleDeposit(stNode);
  }
  return ret;
}

int CarbonDatabase::isOnDemandExcluded(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if ((mDB != NULL) && (node != NULL)) {
    const STSymbolTableNode *stNode = node->getSymTabNode();
    ret = mDB->isOnDemandExcluded(stNode);
  }
  return ret;
}

const char* CarbonDatabase::getSoftwareVersion() const
{
  return mDB->getSoftwareVersion();
}

const char* CarbonDatabase::getIdString() const
{
  return mDB->getDesignId();
}

int CarbonDatabase::isStruct(const CarbonDatabaseNode* node) const
{
  const UserType *type = getTrueType(node);
  int ret = (type != NULL) && (type->getType() == UserType::eStruct);
  return ret;
}

int CarbonDatabase::isArray(const CarbonDatabaseNode* node) const
{
  const UserType *type = getTrueType(node);
  int ret = (type != NULL) && (type->getType() == UserType::eArray);
  return ret;
}

int CarbonDatabase::isEnum(const CarbonDatabaseNode* node) const
{
  const UserType *type = getTrueType(node);
  int ret = (type != NULL) && (type->getType() == UserType::eEnum);
  return ret;
}

int CarbonDatabase::getNumberElemInEnum(const CarbonDatabaseNode* node) const
{
  const UserType *type = getTrueType(node);
  int ret = 0;
  const UserEnum* user_enum = type->castEnum();
  if((type != NULL) && (user_enum != NULL))
  {
    ret = user_enum->getNumberElems();
  }
  return ret;
}

const char* CarbonDatabase::getEnumElem(const CarbonDatabaseNode* node, int index) const
{
  const UserType *type = getTrueType(node);
  const char* ret = NULL;
  if((type != NULL) && (type->getType() == UserType::eEnum))
  {
    const UserEnum* user_enum = type->castEnum();
    if(index >= 0 && user_enum != NULL && index < ((int) user_enum->getNumberElems()))
      ret = user_enum->getElem(index)->str();
  }
  return ret;
}

int CarbonDatabase::getNumStructFields(const CarbonDatabaseNode* node) const
{
  int ret = -1;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    const UserStruct *structType = type->castStruct();
    if (structType != NULL) {
      ret = structType->getNumFields();
    }
  }
  return ret;
}

const CarbonDatabaseNode* CarbonDatabase::getStructFieldByName(const CarbonDatabaseNode* node, const char* name)
{
  // Must be a struct, and a valid string
  if (!isStruct(node) || (name == NULL)) {
    return NULL;
  }

  // Look up the symtab branch corresponding to the db node
  const STSymbolTableNode* stNode = node->getSymTabNode();
  const STBranchNode* stParent = stNode->castBranch();
  ST_ASSERT(stParent, stNode);

  // Find the child of the symtab branch.
  const STSymbolTableNode* stField = findChild(stParent, name);

  CarbonDatabaseNode* dbField = NULL;
  if (stField != NULL) {
    // If the symtab node exists, we can add the db node
    dbField = mDBNodeFactory->addToDB(const_cast<CarbonDatabaseNode*>(node), stField, false);
  }
  return dbField;
}

int CarbonDatabase::getArrayLeftBound(const CarbonDatabaseNode* node) const
{
  int ret = -1;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    const UserArray *arrayType = type->castArray();
    if (arrayType != NULL) {
      ret = arrayType->getRange()->getMsb();
    }
  }
  return ret;
}

int CarbonDatabase::getArrayRightBound(const CarbonDatabaseNode* node) const
{
  int ret = -1;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    const UserArray *arrayType = type->castArray();
    if (arrayType != NULL) {
      ret = arrayType->getRange()->getLsb();
    }
  }
  return ret;
}

int CarbonDatabase::getArrayDims(const CarbonDatabaseNode* node) const
{
  int ret = -1;
  const UserType *type = getTrueType(node);
  if (type != NULL) {
    const UserArray *arrayType = type->castArray();
    if (arrayType != NULL) {
      ret = arrayType->getNumDims();
    }
  }
  return ret;
}

const CarbonDatabaseNode* CarbonDatabase::getArrayElement(const CarbonDatabaseNode* node, const int* indices, int dims)
{
  // Must be an array
  if (!isArray(node)) {
    return NULL;
  }

  // Since multiple dimensions can be specified, we may need to add
  // multiple children to this starting node.  We also need to do some
  // non-const operations on it.
  CarbonDatabaseNode* dbParent = const_cast<CarbonDatabaseNode*>(node);
  // Find/add a node for each dimension.  If zero dimensions were
  // requested (hey, you never know), just return the original node
  CarbonDatabaseNode* dbElem = dbParent;

  // For each dimension specified, get the array element
  bool good = true;
  for (UInt32 i = 0; good && (i < static_cast<UInt32>(dims)); ++i) {
    const STSymbolTableNode *stParent = NULL;
    UInt32 dim = 0;
    SInt32 index = 0;
    // The base array type of the symtab node
    const UserArray* baseArrayType = NULL;
    if (good) {
      // Array dimensions aren't represented in the design symbol table,
      // so use the parent's design node for the new db node
      // we're about to create.  Note that the design node might
      // actually be a leaf.
      stParent = dbParent->getSymTabNode();
      const UserType* ut = mDB->getUserType(stParent);
      baseArrayType = ut->castArray();
      ST_ASSERT(baseArrayType != NULL, stParent);

      // Determine which dimension of the array this represents.  Make
      // sure it's valid.
      if (dbParent->isArrayDim()) {
        // If its parent is an array dimension, this is the next one
        dim = dbParent->getDimension() + 1;
        // Needs to be less than the number of dimensions of the base
        // array.  Check the user type of the symtab node to get this.
        UInt32 numDims = baseArrayType->getNumDims();
        if (dim >= numDims) {
          good = false;
          dbElem = NULL;
        }
      }

      // Make sure the index is within the range for this dimension
      index = indices[i];
      SInt32 upperBound, lowerBound;
      getUpperLowerBounds(dbParent, &upperBound, &lowerBound);
      if ((index > upperBound) || (index < lowerBound)) {
        good = false;
        dbElem = NULL;
      }
    } // if

    if (good) {
      // Add the node, indicating that it represents an array dimension
      dbElem = mDBNodeFactory->addToDB(dbParent, stParent, true, index);

      // Update the parent pointer to the new node and continue
      dbParent = dbElem;
    }
  }

  return dbElem;

}

int CarbonDatabase::canBeCarbonNet(const CarbonDatabaseNode* node) const
{
  int ret = 0;
  if (node != NULL) {
    // If we have user type information for this node, this is easy
    const UserType* type = node->getUserType();
    if (type != NULL) {
      ret = sIsLegalVectorOrScalar(node);
    } else {
      // If the corresponding symbol table node is a leaf, this could
      // be an old model without user type information.  In that case,
      // it can be a CarbonNet as long as it's not a memory.
      const STSymbolTableNode* stNode = node->getSymTabNode();
      const STAliasedLeafNode* stLeaf = stNode->castLeaf();
      if (stLeaf != NULL) {
        const IODBIntrinsic* intr = mDB->getLeafIntrinsic(stLeaf);
        ret = (intr->getType() != IODBIntrinsic::eMemory);
      }
    }
  }
  return ret;
}

int CarbonDatabase::getBitSize(const CarbonDatabaseNode* node) const
{
  int size = 0;
  if (node != NULL) {
    const UserType *type = node->getUserType();
    if (type != NULL) {
      // The UserType object already knows how to calculate its bit size
      size = type->getSize();
    } else {
      // There are two possibilities here.  The node could represent
      // something like a module instance, which doesn't have user
      // type information.  It could also be the case that we're
      // dealing with an old model that doesn't have user type
      // information.
      //
      // In the second case, the corresponding symtab node might be a
      // leaf, in which case we can query the IODB to determine its
      // size.
      const STSymbolTableNode* stNode = node->getSymTabNode();
      const STAliasedLeafNode* stLeaf = stNode->castLeaf();
      if (stLeaf != NULL) {
        const IODBIntrinsic* intr = mDB->getLeafIntrinsic(stLeaf);
        // The size is always at least 1.  If it's a vector, this gets
        // multiplied by the width.  If it's a memory, it gets
        // multiplied by the width and depth.
        size = 1;
        const ConstantRange* vecRange = intr->getVecRange();
        if (vecRange != NULL) {
          size *= vecRange->getLength();
        }
        const ConstantRange* memRange = intr->getMemAddrRange();
        if (memRange != NULL) {
          size *= memRange->getLength();
        }
      }
    }
  }
  return size;
}

const char* CarbonDatabase::getSourceFile(const CarbonDatabaseNode* node) const
{
  const char* file = NULL;
  if (node != NULL) {
    const STSymbolTableNode* stNode = node->getSymTabNode();
    SourceLocator loc;
    if (mDB->findLoc(stNode, &loc)) {
      // If multiple models exist, a single SourceLocatorFactory is
      // shared by all of them.  We need to lock the central mutex
      // before accessing it.
      ShellGlobal::lockMutex();
      file = loc.getFile();
      ShellGlobal::unlockMutex();
    }
  }
  return file;
}

int CarbonDatabase::getSourceLine(const CarbonDatabaseNode* node) const
{
  int line = 0;
  if (node != NULL) {
    const STSymbolTableNode* stNode = node->getSymTabNode();
    SourceLocator loc;
    if (mDB->findLoc(stNode, &loc)) {
      // If multiple models exist, a single SourceLocatorFactory is
      // shared by all of them.  We need to lock the central mutex
      // before accessing it.
      ShellGlobal::lockMutex();
      line = loc.getLine();
      ShellGlobal::unlockMutex();
    }
  }
  return line;
}

const char* CarbonDatabase::getStringAttribute(const char* attributeName) const
{
  // The IODB stores these attributes as UtStrings, but users don't
  // have access to them.  Instead, we need to update a local UtString
  // and return it as a char*.
  //
  // Note that this means the user needs to copy the return value
  // before looking up another attribute, since the storage is shared.
  // This is noted in the documentation for
  // carbonDBGetStringAttribute.
  const char* val = NULL;
  if (mDB->getStringAttribute(attributeName, &mStringAttributeValue)) {
    val = mStringAttributeValue.c_str();
  }
  return val;
}

CarbonStatus CarbonDatabase::getIntAttribute(const char* attributeName, UInt32* attributeValue) const
{
  CarbonStatus stat = eCarbon_ERROR;
  if (mDB->getIntAttribute(attributeName, attributeValue)) {
    stat = eCarbon_OK;
  }
  return stat;
}


CarbonDatabaseNode* CarbonDatabase::translateToDB(const STSymbolTableNode* node)
{
  /*

  The goal here is to take the input node (from the design symbol
  table) and create the appropriate nodes in the visibility database.
  
  This is complex because the symbol table and the visibility database
  don't have identical structures in all cases.  Consider the
  following:
  
    architecture arch of top is
      type subrec is record
        a: std_logic;
        b: std_logic;
      endrecord;
      type myrec is record
        x: subrec;
        y: subrec;
      endrecord;
      type myarray is array (1 downto 0) of myrec;
      signal arr: myarray
  
  In the design symbol table, this is represented as
    top
    +-arr
      +-x
      | +-a
      | +-b
      +-y
        +-a
        +-b
  
  Note that the array dimensions are not present.  In the visibility
  database, we need to add a node for each array dimension.
  This is done as needed as the user looks up nodes.  For example:
  
    top
    +-arr
      +-[0]
        +-x
          +-a
  
  Node top.arr.x from the design symbol table doesn't actually exist
  in the visibility database, so we can't allow it to be
  translated.  While "a" is a child of that node in the design symbol
  table, we can't return it as a struct field in the visibility database
  without knowing which index of top.arr it corresponds to.
  
  So...
  
  We will add each node in the input node's hierarchy to the
  visibility database, giving up if we detect the above condition.
  If any of these nodes are already in the visibility database, we
  validate them and continue.

  */

  // First, push the input node and all its parents onto a stack.
  UtArray<const STSymbolTableNode*> nodeStack;
  const STSymbolTableNode *currNode = node;
  while (currNode != NULL) {
    nodeStack.push_back(currNode);
    currNode = currNode->getParent();
  }

  // Starting at the top of the stack (root), either ensure the node
  // is already in the visibility database or add it.
  const STSymbolTableNode *inputNode;
  CarbonDatabaseNode *dbParent = NULL;
  CarbonDatabaseNode *dbNode = NULL;
  bool fail = false;
  while (!nodeStack.empty() && !fail) {
    inputNode = nodeStack.back();
    nodeStack.pop_back();
    // A symbol table node can't be added as a direct child of a DB
    // node that represents an array, because the index isn't
    // specified.
    if (isArray(dbParent)) {
      dbNode = false;
      fail = true;
    } else {
      // Ask the factory to add this node.  The node may or may not
      // exist, but the factory handles the complexity related to that.
      dbNode = mDBNodeFactory->addToDB(dbParent, inputNode, false);
      // Save this node as the parent for the next iteration
      dbParent = dbNode;
    }
  }
  return dbNode;
}

void CarbonDatabase::populateAllInstances(const STSymbolTableNode* stNode, CarbonDatabaseNodeIter::NodeArray* nodeArray)
{
  // Some STSymbolTableNodes can map to many CarbonDatabaseNodes.
  // Consider an array of records.  The design symbol table has one
  // node for each field of the record.  However, because of the
  // array, there are many instances of that record from the user's
  // point of view.  This function finds and populates all those
  // possible CarbonDatabaseNodes.

  // First, walk up the tree from the symtab node until we find a node
  // with a one-to-one mapping to a CarbonDatabaseNode.  For each node
  // that failed, make a map from its parent to itself.  Later, when
  // we need to populate the children of CarbonDatabaseNodes, we'll
  // use the map to populate only the path to the desired end node,
  // instead of doing all children.
  CarbonDatabaseNode* dbNode = NULL;
  const STSymbolTableNode* currStNode = stNode;
  UtHashMap<const STSymbolTableNode*, const STSymbolTableNode*> parentToChild;

  while (dbNode == NULL) {
    dbNode = translateToDB(currStNode);
    if (dbNode == NULL) {
      const STSymbolTableNode* parent = currStNode->getParent();
      parentToChild[parent] = currStNode;
      currStNode = parent;
    }
  }

  // If the starting node was successfully converted to a DB node,
  // we're done.  Update the array and return.
  if (parentToChild.empty()) {
    if (nodeArray != NULL) {
      nodeArray->push_back(dbNode);
    }
    return;
  }

  // Now, build a stack of all the CarbonDatabaseNodes we need to
  // traverse.  Initially, it's just the one we found.  As we process
  // each node, we may push new nodes onto the stack to replace it.
  UtStackPOD<CarbonDatabaseNode*> dbNodeStack;
  dbNodeStack.push(dbNode);

  while (!dbNodeStack.empty()) {
    // Pop the top node from the stack
    dbNode = dbNodeStack.top();
    dbNodeStack.pop();

    // Get its user type
    const UserType* ut = dbNode->getUserType();
    switch(ut->getType()) {
    case UserType::eArray:
    {
      // For an array node, loop over all its children
      // (i.e. elements in its outermost dimension) and add them to
      // the stack of nodes we need to process.  Use
      // CarbonDatabaseNodeChildIter, since it sorts by index.
      CarbonDatabaseNodeIter* l = loopChildren(dbNode);
      const CarbonDatabaseNode* dbElem;
      while ((dbElem = l->next()) != NULL) {
        dbNodeStack.push(const_cast<CarbonDatabaseNode*>(dbElem));
      }
      // The iterator allocated by loopChildren isn't managed
      // anywhere, so we need to delete it.
      delete l;
    }
    break;
    case UserType::eStruct:
    {
      // For a structure, get the symbol table node corresponding to
      // the DB node and look up its child in tha map we populated
      // earlier.  It's guaranteed to exist.  Add it as a child of
      // the DB node.
      currStNode = dbNode->getSymTabNode();
      const STSymbolTableNode* stChild = parentToChild[currStNode];
      CarbonDatabaseNode* dbChild = mDBNodeFactory->addToDB(dbNode, stChild, false);
      if (stChild == stNode) {
        // If this symtab node is the one that was passed into the
        // function, this DB node is one of its elaborated instances.
        // Add it to the array.  We're done with this DB node.
        if (nodeArray != NULL) {
          nodeArray->push_back(dbChild);
        }
      } else {
        // The node we're looking for is nested somewhere below this
        // record.  Push the node onto the stack and continue.
        dbNodeStack.push(dbChild);
      }
    }
    break;
    case UserType::eScalar:
      // This should never occur.  If there wasn't a one-to-one
      // mapping from the original symtab node to a DB node, it's
      // because one of its parents was an array.  Furthermore, the
      // original node's parent must be a structure, because an array
      // of scalars doesn't have any children in the design symtab.
      // Both those types are handled above and never actually get to
      // the scalar leaf.
      ST_ASSERT(0, dbNode->getSymTabNode());
      break;

    case UserType::eEnum:
      ST_ASSERT(0, dbNode->getSymTabNode());
      break;
    case UserType::eStructEnd:
      // There is no user type, it's just used during browsing process
      // for creation of user types.
      break;
    }
  }

  return;
}

bool CarbonDatabase::canBeDBNode(const STSymbolTableNode* node) const
{
  /*

  Some nodes, though they have valid type information, can't be
  treated as visibility nodes.  For example, consider an array
  "top.arr" whose elements are records with fields "a" and "b".
  "top.arr.a" is a valid symbol table node, but it can't be treated
  as a visibility node because we don't know which index of "arr" it
  corresponds to.  See translateToDB() for more on this.

  You could argue that the type information is still correct, and
  therefore should be returned.  However, this creates a sticky
  situation for the end user.  Let's say "a" in the above example is
  itself a record, with fields "x" and "y".  The user would be told
  that "top.arr.a" is a structure, but he would be unable to get
  handles for its fields, because the array index is missing.

  Basically, none of the node's parents can be arrays, because the
  indices aren't represented in the design symbol table.

  */

  bool good = (node != NULL);

  if (good) {
    // Ensure none of this node's parents are arrays.
    const STSymbolTableNode *currNode = node->getParent();
    while ((currNode != NULL) && good) {
      const UserType *type = mDB->getUserType(currNode);
      if ((type != NULL) && (type->getType() == UserType::eArray)) {
        good = false;
      } else {
        currNode = currNode->getParent();
      }
    }
  }

  return good;
}

const STSymbolTableNode* CarbonDatabase::findChild(const STSymbolTableNode* parent, const char* childName) const
{
  // Get/create the StringAtom for the child name, and look it
  // up in the symbol table.  This can't use getIntern(), because
  // find() will crash if the StringAtom is NULL.
  //
  // The atomic cache is shared by all model instances, so access
  // needs to be controlled with a mutex.
  ShellGlobal::lockMutex();
  StringAtom *strAtom = mAtomicCache->intern(childName);
  ShellGlobal::unlockMutex();
  const STSymbolTableNode *node = mDesignSymTab->find(parent, strAtom);
  return node;
}

void CarbonDatabase::init()
{
  // Regardless of whether this is standalone or runtime, we have our
  // IODBRuntime and AtomicCache now, so we can create the node
  // factory.
  mDBNodeFactory = new CarbonDatabaseNodeFactory(mDB, mAtomicCache);

  // Setup the both edge trigger set
  mDB->computeBothEdgeScheduleTriggers(&mAux->mBothEdgeTriggers);
}

void CarbonDatabase::populateChildren(CarbonDatabaseNode* node)
{
  /*

  Since the visibility database is populated as needed based on
  user actions, nodes generally don't have all their possible
  children present.  Here, we create new children for each possible
  array element (for arrays), or for each child of the corresponding
  symbol table node (for others).

  */

  if (isArray(node)) {
    // Just look up all the elements in this dimension
    SInt32 upperBound, lowerBound;
    getUpperLowerBounds(node, &upperBound, &lowerBound);
    for (SInt32 i = lowerBound; i <= upperBound; ++i) {
      // Getting the element adds it as a child.  Ignore the returned
      // node.
      (void) getArrayElement(node, &i, 1);
    }
  } else {
    // Add a node for each child of the corresponding design node
    const STSymbolTableNode *designNode = node->getSymTabNode();
    const STBranchNode *designBranch = designNode->castBranch();
    // The design node might be a leaf, if it's a scalar
    if (designBranch != NULL) {
      // Use the branch node iterator, instead of directly accessing
      // the children.  It automatically filters temp names.
      CarbonDatabaseSymtabBranchIter iter(designBranch);
      const STSymbolTableNode *designChild;
      while ((designChild = iter.next()) != NULL) {
        // Ignore the returned node.  It's still added as a child.
        (void) mDBNodeFactory->addToDB(node, designChild, false);
      }
    }
  }
}

const CarbonDatabaseNode* CarbonDatabase::findNode(const char* path, bool warnNotFound)
{
  // Split the path into its component tokens
  const HdlHierPath *hdl = mDesignSymTab->getHdlHier();
  UtStringArray tokenArray;
  HdlHierPath::Status stat = HdlHierPath::eIllegal;
  HdlId info;
  // If the last token ends with an index, we need to keep it.
  // "top.myarray" is very different than "top.myarray[1]".
  info.setAllowIndices(true);
  hdl->decompPath(path, &tokenArray, &info);

  // Now, try to look up each token in the design symbol table,
  // starting at the root
  STBranchNode *designParent = NULL;
  CarbonDatabaseNode *dbParent = NULL;
  CarbonDatabaseNode *dbNode = NULL;
  // If the decomposition failed, we're already done
  bool fail = (info.getType() == HdlId::eInvalid);
  for (UtStringArray::iterator iter = tokenArray.begin(); !fail && (iter != tokenArray.end()); ++iter) {
    dbNode = NULL;
    STSymbolTableNode* designNode = NULL;
    // We should never see an array node as the DB parent here.  If we
    // do, it's because the user has requested a child of an array
    // without specifying all the array's dimensions.  For example, if
    // top.arr is a 1D array of records, and the record has a field x,
    // a request for "top.arr.x" is invalid.
    //
    // If that happens, we'll reach this point with dbParent ==
    // top.arr, and so we need to bail out.  Note that if the valid
    // path of "top.arr[0].x" is requested, we'll reach this point
    // with dbParent == top.arr[0], which is OK because that node is a
    // struct.
    if (! isArray(dbParent)) {
      const char *token = *iter;
      // This uses the atomic cache that's shared by all model
      // instances, so access needs to be controlled with a mutex.
      ShellGlobal::lockMutex();
      designNode = mDesignSymTab->getNode(token, &stat, &info, designParent);
      ShellGlobal::unlockMutex();
      if (designNode != NULL) {
        // We found a design node matching this token.  Add it to the
        // visibility database and continue.  If this isn't valid,
        // it will return NULL.
        dbNode = mDBNodeFactory->addToDB(dbParent, designNode, false);
      } else {
        // This token may contain array indices.  We need to extract
        // these and try to create the corresponding visibility table
        // nodes.
        UtArray<SInt32> indices;
        // Make a copy of the arrayed token.  extractIndices() will strip
        // indices from this.
        UtString modToken = token;
        // Attempt to parse the token into a base name and array of
        // indices
        if (extractIndices(&modToken, &indices)) {
          // Look up the stripped name.  Hopefully it exists.
          //
          // This uses the atomic cache that's shared by all model
          // instances, so access needs to be controlled with a mutex.
          ShellGlobal::lockMutex();
          designNode = mDesignSymTab->getNode(modToken.c_str(), &stat, &info, designParent);
          ShellGlobal::unlockMutex();
          if (designNode != NULL) {
            // Add it to the vis database.  This represents the base array
            // with no dimensions specified.
            CarbonDatabaseNode *dbArrayNode = mDBNodeFactory->addToDB(dbParent, designNode, false);

            UInt32 numDims = indices.size();
            // Allocate a temporary array to pass to getArrayElement(),
            // and fill it with all the extracted indices.
            SInt32 *indexArray = CARBON_ALLOC_VEC(SInt32, numDims);
            for (UInt32 i = 0; i < numDims; ++i) {
              indexArray[i] = indices[i];
            }

            // Now that we have the base array node and an array of
            // indices, we can leverage other code to retrieve the
            // desired element.
            dbNode = const_cast<CarbonDatabaseNode*>(getArrayElement(dbArrayNode, indexArray, numDims));
            CARBON_FREE_VEC(indexArray, SInt32, numDims);
          }
        }
      }
    }

    if (dbNode == NULL) {
      // If we failed to create a database node at any point, abort.
      fail = true;
    } else {
      // Update parent pointers and continue
      designParent = designNode->castBranch();
      dbParent = dbNode;
    }
  }
  
  // If we failed, print why
  if (fail && warnNotFound) {
    MsgContext *msg = getMsgContext();
    if (stat == HdlHierPath::eIllegal) {
      msg->SHLInvalidPath(path);
    } else {
      msg->SHLPathNotFound(path);
    }
  }

  return dbNode;
}

// If the node is a composite, this method drills down to the array elements and
// struct fields and adds each one to the visibility database.
void CarbonDatabase::elabDBNode(CarbonDatabaseNode* node)
{
  if (node != NULL)
  {
    // If the user type of the node is array or record, add all elements/fields
    // to the visibility symbol table.
    if (isStruct(node))
    {
      populateChildren(const_cast<CarbonDatabaseNode*> (node));
    }
    else if (isArray(node))
    {
      int numDims = getArrayDims(node);
      int lbound = getArrayLeftBound(node);
      int rbound = getArrayRightBound(node);
      ConstantRange cr(lbound, rbound);
      int incr = (lbound > rbound ? -1 : 1);
      int bound = lbound;

      while (cr.contains(bound))
      {
        const CarbonDatabaseNode* elemNode = getArrayElement(node, &bound, 1);
        if (numDims > 1) {
          elabDBNode(const_cast<CarbonDatabaseNode*>(elemNode));
        }
        bound += incr;
      }
    }
  }
}

const UserType* CarbonDatabase::getTrueType(const CarbonDatabaseNode* node) 
{
  /*

  Previously, when the visibility symbol table existed in addition to
  the design symbol table, determining the true UserType for a node
  was complex enough to require a utility function.  Now it's much
  easier, but the function remains to minimize code changes elsewhere.

  */

  const UserType *trueType = NULL;

  if (node != NULL) {
    trueType = node->getUserType();
  }

  return trueType;
}

/*! \brief sGetPackedUnpackedDimensionCount. examine the UserType information associated with \a node, and return three values concerning its declared structure.

  Optionally returns up to 3 values:
  nPacked:      the number of Packed dimensions
  nUnpacked:    the number of unpacked dimensions
  npackedWidth: the combined width of all packed dimensions,
                e.g. for reg [7:6][2:0] mem1 [0:10], the combined width of packed dimensions is 6 == (2*3).

  These values are determined by walking up to the base usertype of \a node and then counting the sizes for packed and unpacked dimensions.
 */

void CarbonDatabase::sGetPackedUnpackedDimensionCount(const CarbonDatabaseNode* node,
                                                      UInt32* nPacked,
                                                      UInt32* nUnpacked,
                                                      UInt32* nPackedWidth)
{
  UInt32 packedArrayDims = 0;
  UInt32 unpackedArrayDims = 0;
  UInt32 packedWidth = 0;
  

  // Traverse upward until we reach the root usertype of this
  // variable/wire. For example, if 'node' is mem[3][2], follow
  // 'getParent' until we get to 'mem'.
  const CarbonDatabaseNode *curr = node; 
  for (; curr && curr->getParent() && curr->getParent()->getUserType(); curr = curr->getParent());

  // 'curr' now points to the root CarbonDatabaseNode that has usertype information associated with it.
  // Now traverse the user array element types, counting packed, unpacked dimensions, in the userType (thus this count does not include any
  // implicit unpacked dimension added during population)

  // If it's not an array type, there's nothing to do.
  const UserType *type = curr->getUserType();
  while (type) {
    const UserArray *arrayType = type->castArray();
    if (arrayType) {
      type = arrayType->getElementType();
      if (arrayType->isPackedArray()) {
        packedArrayDims++;
        UInt32 width = arrayType->getRange()->getLength();
        INFO_ASSERT(width, "An usertype object with a width of 0 has been detected"); // the following code assumes that width is never 0
        if ( 0 == packedWidth ){
          // if packedWidth is 0 then this is the first packed dimension we have encountered
          packedWidth = width;
      } else {
          packedWidth *= width;
        }
      } else {
        unpackedArrayDims++;
      }
    }
    else {
      type = NULL;
  }
  }

  // since each argument is optional only return values if the caller provided a place to put the value
  if ( nPacked){ *nPacked = packedArrayDims; }
  if ( nUnpacked ) {*nUnpacked = unpackedArrayDims; }
  if ( nPackedWidth ) { *nPackedWidth = packedWidth; }
}

// This method returns true if the the database node
// is a vector or scalar.
// It takes into account the fact that
// SystemVerilog packed arrays can have multiple packed
// dimensions, and still be a vector.
// NOTE: At present, the indexing code does not yet
// handle the following cases, so we catch them here:
//
// 1) A part select of a multi-dimensional packed array, e.g.
//    
//    reg [3:0][3:0] foo;
//
//    carbonFindNet("foo[3]") is not yet supported.
//
// 2) A bit select of a multi-dimensional packed array,
//    e.g.
//
//    reg [3:0][3:0] foo;
//
//    carbonFindNet("foo[3][3]") is not yet supported.

bool CarbonDatabase::sIsLegalVectorOrScalar(const CarbonDatabaseNode *node)
{
  const UserType *type = getTrueType(node);
  
  // It must either be a scalar, or a 1-D array of 1-bit scalars
  // Note that it can be a packed array, as long as it isn't
  // contained in another packed array.
  const UserScalar *scalarType = type->castScalar();
  // If it's a scalar, make sure it isn't a bitselect
  // of a multi-dimensional packed array. That is not
  // yet supported.
  if (scalarType) {
    UInt32 nPackedDims = 0;
    sGetPackedUnpackedDimensionCount(node, &nPackedDims, NULL, NULL);
    if (nPackedDims > 1) {
      scalarType = NULL;
    }
  }
  
  // Enums are OK, too, since they're represented as vectors
  const UserEnum  *enumType = type->castEnum();
  // If it's an array, then the dimensions must ALL be
  // packed, and the array cannot be contained within
  // another packed array (i.e. it must be an entire memory
  // word, not a part-select of the word).
  bool arrayGood = false;
  const UserArray *arrayType = type->castArray();
  if ((arrayType != NULL) && (arrayType->isPackedArray())) {
    const CarbonDatabaseNode* parent = node->getParent();
    const UserType *parentType = getTrueType(parent);
    const UserArray *parentArray = parentType ? parentType->castArray() : NULL;
    if (!parentArray || !parentArray->isPackedArray())
    {
      arrayGood = true;
    }
  }

  return ((scalarType != NULL) || (enumType != NULL) || arrayGood);
}

bool CarbonDatabase::isInvalidNetNode(const CarbonDatabaseNode* node, bool forCarbonMemory) const
{
  /*

  Returns whether a node is known to be an invalid net node.  That is,
  that a CarbonNetID/CarbonMemoryID cannot be created to represent it.  Note that if
  this function returns false, it doesn't necessariliy mean it's
  valid!  It just means we can't be sure.

  */

  bool invalid = true;
  // First, the node must be valid
  if (node != NULL) {
    // Get the type for it (or whatever design node it represents).
    const UserType *type = getTrueType(node);
    // Now, we have two situations.  If the type info exists, we can check it
    if (type != NULL) {
      // This code path is shared by carbonFindNet() and
      // carbonFindMemory().  They're both ShellNet derivatives, and
      // if the wrong type is returned getCarbonNet() throws it away.
      if (forCarbonMemory) {
        // This can be a memory as long as its design node is a leaf,
        // and it's not contained in an array.
        //
        // The visibility project doesn't change the behavior of
        // carbonFindMemory(), so we'll continue to let users get a
        // CarbonMemoryID for multidimensional arrays, arrays within
        // records, etc.  We just need to make sure that a particular
        // index of an array hasn't been specified.  We can't
        // represent that subarray as a CarbonMemoryID.
        //
        // Also, it doesn't matter if this ends up being a vector or
        // scalar instead of a memory.  It will be thrown away later.
        const STSymbolTableNode* stNode = node->getSymTabNode();
        bool isLeaf = stNode->castLeaf() != NULL;
        bool containedInArray = false;
        const CarbonDatabaseNode* parent = node->getParent();
        while ((parent != NULL) && !containedInArray) {
          const UserType* parentType = parent->getUserType();
          containedInArray = (parentType != NULL) && (parentType->getType() == UserType::eArray);
          parent = parent->getParent();
        }
        invalid = !isLeaf || containedInArray;
      } else {
        // For a CarbonNetID, the only requirement is that it be a
        // vector or scalar.
        invalid = !sIsLegalVectorOrScalar(node);
      }
    } else {
      // If there's no type info, we can't assume it's invalid.  This
      // may be an older model that doesn't have type info, or a new
      // model without it (we don't write out user type info for
      // verilog yet).
      //
      // While assuming it's not invalid may be wrong, it's
      // essentially the same behavior as old models.
      invalid = false;
    }
  }
  return invalid;
}

bool CarbonDatabase::extractIndices(UtString *token, UtArray<SInt32> *indices) const
{
  // TODO - use HdlVerilogPath functions

  bool done = false;
  while (!done) {
    // Look for '[' and ']' in the token
    size_t leftBracket = token->find('[');
    size_t rightBracket = token->find(']', leftBracket + 1);
    // Detect bad conditions
    UInt32 foundChars = 0;
    if (leftBracket != UtString::npos) {
      ++foundChars;
    }
    if (rightBracket != UtString::npos) {
      ++foundChars;
    }
    if ((foundChars == 1) || // We can find both chars or neither, but not just one
        (rightBracket < leftBracket)) { // '[' must come before ']'
      return false;
    }
    if (foundChars == 2) {
      // Try to extract the index.  Create a string containing
      // everything between the brackets.
      size_t start = leftBracket + 1;
      size_t length = rightBracket - start;
      UtString indexStr(*token, start, length);
      SInt32 index;
      // Extraction should succeed and leave an empty string
      if ((indexStr >> index) && indexStr.empty()) {
        // Save the index, and erase from the token
        indices->push_back(index);
        token->erase(leftBracket, length + 2);
      } else {
        // badly formed
        return false;
      }
    }
    if (foundChars == 0) {
      // No more indices found
      done = true;
    }
  }

  return true;
}

void CarbonDatabase::getUpperLowerBounds(const CarbonDatabaseNode* node, SInt32* upper, SInt32* lower) const
{
  // Determine the upper (i.e. max(msb, lsb)) and lower
  // (i.e. min(msb,lsb)) bounds of an array
  SInt32 leftBound = getArrayLeftBound(node);
  SInt32 rightBound = getArrayRightBound(node);
  *upper = (leftBound > rightBound) ? leftBound : rightBound;
  *lower = (leftBound < rightBound) ? leftBound : rightBound;
}

CarbonDatabaseNodeIter* CarbonDatabase::createLoop(CarbonDatabaseSymtabIter* symtabIter)
{
  CarbonDatabaseNodeIter* ret = new CarbonDatabaseNodeIter;
  // Populate the iterator's node list from the symtab nodes in the
  // input iterator.
  ret->populate(symtabIter, this);
  return ret;
}


CarbonDatabaseStandalone::CarbonDatabaseStandalone()
{ 
  mDB = NULL;
  mAtomicCache = new AtomicCache;
  mShellSymTabBOM = new ShellSymTabBOM;
  mScheduleFactory = new SCHScheduleFactory;
  mExprFactory = new ESFactory;
  mSourceLocatorFactory = NULL;   // This will be allocated if needed

  mShellSymTabBOM->putScheduleFactory(mScheduleFactory);

  mDesignSymTab = new STSymbolTable(mShellSymTabBOM,
                                    mAtomicCache);
}

CarbonDatabaseStandalone::~CarbonDatabaseStandalone()
{
  delete mDB;
  
  delete mExprFactory;
  delete mDesignSymTab;
  delete mShellSymTabBOM;
  delete mScheduleFactory;
  delete mAtomicCache;
  delete mSourceLocatorFactory;
}

bool CarbonDatabaseStandalone::openDBFile(const char* fileName, SourceLocatorFactory* factory)
{
  // If the caller specified a factory, use it.  Otherwise allocate
  // one for ourselves and use it.
  if (factory == NULL) {
    if (mSourceLocatorFactory == NULL) {
      mSourceLocatorFactory = new SourceLocatorFactory;
    }
    factory = mSourceLocatorFactory;
  }

  CarbonDBType whichDB = eCarbonIODB;
  UtString name(fileName);
  MsgContext *msg = getMsgContext();
  if (name.rfind(IODB::scFullDBExt) != UtString::npos)
    whichDB = eCarbonFullDB;
  else if (name.rfind(IODB::scIODBExt) != UtString::npos)
    whichDB = eCarbonIODB;
  else if (name.rfind(IODB::scGuiDBExt) != UtString::npos)
    whichDB = eCarbonGuiDB;
  else
  {
    msg->SHLDBUnknownDBType(fileName);
    return false;
  }

  mDB = new IODBRuntime(mAtomicCache, mDesignSymTab, msg, mScheduleFactory, mExprFactory, factory);

  bool ret = mDB->readDB(fileName, whichDB);
  if (ret)
  {
    init();
  }
  
  return ret;
}

void CarbonDatabaseStandalone::setupLicensing(bool waitForLicense)
{
  bool block = waitForLicense ? true : false;
    // initializes licensing
  ShellGlobal::setLicenseQueuing(block);
}

bool CarbonDatabaseStandalone::open(const char* fileName, int waitForLicense)
{
  bool isGood = false;
  if (openDBFile(fileName))
  {
    setupLicensing(waitForLicense);
    if (ShellGlobal::checkoutVSPLicenses(mDB, fileName))
      isGood = true;
  }
  
  return isGood;
}

bool CarbonDatabaseStandalone::openSocVsp(SInt32 key, const char* fileName, int waitForLicense)
{
  // Make sure the key is valid.
  SInt32 calcKey;
  RANDOM_SEEDGEN_1(calcKey, "VSP", 3);

  // just to be more confusing in case someone decides to disassemble
  // this. Not rocket-science here, but several more lines of
  // assembly if gcc doesn't optimize this away.
  calcKey += waitForLicense;
  SInt32 result = calcKey - key;
  // result should be the same value as waitForLicense now
  
  bool isGood = false;
  if ((result == waitForLicense) && openDBFile(fileName))
  {
    setupLicensing(waitForLicense);
    // Model kit users with only an AD compile license need to be able
    // to run carmgr, so check for that license as well.
    isGood = ShellGlobal::initSocVsp() || ShellGlobal::initADCompiler();
  }
  return isGood;
}

bool CarbonDatabaseStandalone::openPlatArch(SInt32 key, const char* fileName, int waitForLicense)
{
  // Make sure the key is valid.
  SInt32 calcKey;
  RANDOM_SEEDGEN_1(calcKey, "PLAT", 4);

  // just to be more confusing in case someone decides to disassemble
  // this. Not rocket-science here, but several more lines of
  // assembly if gcc doesn't optimize this away.
  calcKey += waitForLicense;
  SInt32 result = calcKey - key;
  // result should be the same value as waitForLicense now
  
  bool isGood = false;
  if ((result == waitForLicense) && openDBFile(fileName))
  {
    setupLicensing(waitForLicense);
    isGood = ShellGlobal::initPlatArch();
  }
  return isGood;
}

MsgContext* CarbonDatabaseStandalone::getMsgContext() const
{
  // Get the global message context
  return ShellGlobal::getProgErrMsgr();
}

CarbonDatabaseRuntime* CarbonDatabaseStandalone::castRuntime()
{
  return NULL;
}




CarbonDatabaseRuntime::CarbonDatabaseRuntime(CarbonModel* model)
  : mModel(model)
{
  // Nearly everything we need already exists in the model
  CarbonHookup *hookup = mModel->getHookup();
  mDB = hookup->getDB();
  mDesignSymTab = mDB->getDesignSymbolTable();
  mAtomicCache = mDB->getAtomicCache();

  // Create the net factory, using the model's hookup and IODB
  mNetFactory = new VisNetFactory(hookup, mDB);

  init();
}

CarbonDatabaseRuntime::~CarbonDatabaseRuntime()
{
  // Destroy the net factory
  delete mNetFactory;

  // Model frees everything else
}

MsgContext* CarbonDatabaseRuntime::getMsgContext() const
{
  // Use the model's message context
  return mModel->getMsgContext();
}

CarbonDatabaseRuntime* CarbonDatabaseRuntime::castRuntime()
{
  return this;
}

ShellNet* CarbonDatabaseRuntime::getCarbonNet(const CarbonDatabaseNode* node,
                                              bool forCarbonMemory,
                                              bool forWaveDump)
{
  if (node == NULL) {
    return NULL;
  }

  // Start with the corresponding design node
  const STSymbolTableNode *designNode = node->getSymTabNode();

  MsgContext *msg = getMsgContext();
  UtString netName;
  node->composeFullName(&netName);

  ShellNet *net = NULL;
  const STAliasedLeafNode *leaf = designNode->castLeaf();
  // Just because it's a leaf doesn't mean we can get a net/memory for it.
  bool invalidNetNode = isInvalidNetNode(node, forCarbonMemory);

  if (leaf && ! mDB->isNetHidden(leaf) && !invalidNetNode) {
    // Ask the net factory to create a net.  Any required expressions will be applied.
    net = mNetFactory->createNet(node);

    if (! net) {
      // The net was optimized away. Warn only if the flag is set. During
      // waveform dumping, we don't want to issue these warnings bug display
      // 'x' in the waveform for this signal.
      if (!forWaveDump) {
        msg->SHLOptimizedNet(netName.c_str());
      }
    }
  } else if ((! leaf || invalidNetNode) && ! mDB->isModuleHidden(designNode)) {
    // The design path isn't a net
    msg->SHLPathNotANet(netName.c_str());
  } else {
    // The design path doesn't exist or is hidden
    msg->SHLPathNotFound(netName.c_str());
  }

  // Apply any wrappers required for Replay/OnDemand
  if (net != NULL) {
    // Ensure we got a CarbonNet or a CarbonMemory, as requested
    // NOTE: It's OK to get a multi-d packed array (which is also
    // considered a memory net).
    bool isMemNet = (isUnpackedArray(node) == 1);
    if (isMemNet != forCarbonMemory) {
      if (isMemNet) {
        // Asked for a net, got a memory
        msg->SHLNetAMemory(netName.c_str());
      } else {
        // Asked for a memory, got a net
        msg->SHLNotAMemory(netName.c_str());
      }
      // Either way, free it and return NULL
      CarbonNet* doomedNet = net;
      mModel->freeNet(&doomedNet);
      return NULL;
    }

    ShellNet *wrappedNet = mModel->applyNetWrappers(net, true);
    return wrappedNet;
  }
  return NULL;
}

CarbonStatus CarbonDatabaseRuntime::examineArrayToFile(const CarbonDatabaseNode* node, CarbonRadix format, const char* filename)
{
  // Determine the array ranges, element type, etc.
  ArrayFileInfo info(this, node);
  if (!info.examine()) {
    return eCarbon_ERROR;
  }

  // Allocate a buffer to store the value.  Its size depends on the format we're using.
  UInt32 width = info.getRowWidth();
  UInt32 numChars = width;      // assume binary
  switch (format) {
  case eCarbonHex:
    numChars = (width + 3) / 4;
    break;
  case eCarbonDec:      // pessimism
  case eCarbonOct:
    numChars = (width + 2) / 3;
    break;
  default:
    break;
  }
  ++numChars;   // for '\0'
  char *valBuf = CARBON_ALLOC_VEC(char, numChars);

  UtOBStream out(filename);

  // We can't instrument temporary nets for OnDemand, because they'll
  // still be referenced by the OnDemand wrappers after they're freed.
  // Force a state restore so we're guaranteed to get the right values
  // when we examine them.  Use this node's name for OnDemand debug
  // callback reporting.
  UtString nodeName;
  node->composeFullName(&nodeName);
  mModel->onDemandForceStateRestore(nodeName);

  // Now, for each element in the array, get a temporary net and
  // examine it.
  const RangeArray &arrayRanges = info.getArrayRanges();
  ArrayWalker arrayWalk(arrayRanges);
  const UserType *elemType = info.getElemType();
  NetFlags netFlags = info.getNetFlags();
  while (arrayWalk.next()) {
    // Scope this here so the temp net will automatically be cleaned up on each iteration
    VisNetFactory::TempNetManager mgr;
    ShellNet *tempNet = mNetFactory->createTempNet(const_cast<CarbonDatabaseNode*>(node), elemType, &mgr, arrayWalk.getArray(), arrayWalk.getSize(), arrayRanges);
    tempNet->format(valBuf, numChars, format, netFlags, mModel);
    out << valBuf << '\n';
  }

  CARBON_FREE_VEC(valBuf, char, numChars);
  return eCarbon_OK;
}

CarbonStatus CarbonDatabaseRuntime::depositArrayFromFile(const CarbonDatabaseNode* node, CarbonRadix format, const char* filename)
{
  ArrayFileInfo info(this, node);
  if (!info.examine()) {
    return eCarbon_ERROR;
  }

  UInt32 width = info.getRowWidth();
  CarbonMemFile *memFile = carbonReadMemFile(mModel->getObjectID (), filename, format, width, 0);
  // Check the bounds.  Not all addresses need to be specified, but
  // they need to start at 0 and go up
  SInt64 lowAddr, highAddr;
  carbonMemFileGetFirstAndLastAddrs(memFile, &lowAddr, &highAddr);
  if ((lowAddr != 0) || (highAddr < lowAddr)) {
    // TODO - error message
    return eCarbon_ERROR;
  }

  // We can't instrument temporary nets for OnDemand, because they'll
  // still be referenced by the OnDemand wrappers after they're freed.
  // Force a non-idle access to ensure we exit the idle/looking state,
  // so the deposit doesn't need to be saved.  Use this node's name
  // for OnDemand debug callback reporting.
  UtString nodeName;
  node->composeFullName(&nodeName);
  mModel->onDemandForceNonIdleAccess(nodeName);

  // Now, for each element in the array, get a temporary net and
  // deposit the data from the appropriate memory row.
  const RangeArray &arrayRanges = info.getArrayRanges();
  ArrayWalker arrayWalk(arrayRanges);
  SInt64 addr = 0;
  const UserType *elemType = info.getElemType();
  while (arrayWalk.next() && (addr <= highAddr)) {
    const UInt32 *data = carbonMemFileGetRow(memFile, addr);
    if (data != NULL) {
      // Scope this here so the temp net will automatically be cleaned up on each iteration
      VisNetFactory::TempNetManager mgr;
      ShellNet *tempNet = mNetFactory->createTempNet(const_cast<CarbonDatabaseNode*>(node), elemType, &mgr, arrayWalk.getArray(), arrayWalk.getSize(), arrayRanges);
      tempNet->deposit(data, NULL, mModel);
    }
    ++addr;
  }
  carbonFreeMemFileID(&memFile);

  return eCarbon_OK;
}

CarbonDatabaseRuntime::ArrayWalker::ArrayWalker(const UtArray<const ConstantRange*>& ranges)
  : mInitialized(false),
    mSize(ranges.size()),
    mRanges(ranges)
{
  mArray = CARBON_ALLOC_VEC(SInt32, mSize);
}

CarbonDatabaseRuntime::ArrayWalker::~ArrayWalker()
{
  CARBON_FREE_VEC(mArray, SInt32, mSize);
}

bool CarbonDatabaseRuntime::ArrayWalker::next()
{
  if (!mInitialized) {
    // This is the first call to next().  Initialize each dimension to
    // the rightmost (i.e. lowest) index.
    for (UInt32 i = 0; i < mSize; ++i) {
      mArray[i] = mRanges[i]->rightmost();
    }
    mInitialized = true;
    return true;
  }

  bool success = false;
  // See if we can advance a dimension.  Start at the innermost one,
  // and try to move to the next index.  If it doesn't wrap, succeed.
  // Otherwise, wrap the value and move to the next dimension.
  for (SInt32 dim = mSize - 1; !success && (dim >= 0); --dim) {
    SInt32 currIndex = mArray[dim];
    SInt32 nextIndex;
    const ConstantRange *range = mRanges[dim];
    if (currIndex == range->leftmost()) {
      // wrap
      nextIndex = range->rightmost();
    } else {
      // increment, depending on endianness
      nextIndex = currIndex + 1;
      success = true;
    }
    mArray[dim] = nextIndex;
  }
  return success;
}


CarbonDatabaseRuntime::ArrayFileInfo::ArrayFileInfo(CarbonDatabase *db, const CarbonDatabaseNode* dbNode)
  : mDB(db),
    mDBNode(dbNode),
    mValid(false),
    mElemType(NULL),
    mRowWidth(0),
    mNetFlags(eNoneNet)
{
}

CarbonDatabaseRuntime::ArrayFileInfo::~ArrayFileInfo()
{
}

bool CarbonDatabaseRuntime::ArrayFileInfo::examine()
{
  // The database node must be valid, and an array
  if ((mDBNode == NULL) || !mDB->isArray(mDBNode)) {
    // TODO - error message
    return false;
  }

  // We now know that this has an array type
  const UserType *userType = mDBNode->getUserType();
  const UserArray *arrayType = userType->castArray();

  // Determine the ranges of all the dimensions, and the final element type
  while (arrayType != NULL) {
    const ConstantRange *range = arrayType->getRange();
    mArrayRanges.push_back(range);
    mElemType = arrayType->getElementType();
    // If this is a multi-dimensional array, continue
    arrayType = mElemType->castArray();
  }

  // The final element type needs to be a scalar
  if (mElemType->getType() != UserType::eScalar) {
    // TODO - error message
    return false;
  }

  // Determine the row width
  if (mArrayRanges.size() == 1) {
    // If this is a 1-D array, each bit gets a row in the file
    mRowWidth = 1;
  } else {
    // The last dimension determines the width of the row in the file
    const ConstantRange *range = mArrayRanges.back();
    mRowWidth = range->getLength();
    mArrayRanges.pop_back();
    // Update the elemType to the type of the next-to-last dimension
    arrayType = userType->castArray();
    UInt32 dim = mArrayRanges.size() - 1;
    mElemType = arrayType->getDimElementType(dim);
  }

  // Determine the net flags
  const STSymbolTableNode *designLeaf = mDBNode->getSymTabNode()->castLeaf();
  const ShellDataBOM* shellBOMData = ShellSymTabBOM::getLeafBOM(designLeaf);
  mNetFlags = shellBOMData->getNetFlags();

  // success
  mValid = true;

  return true;
}

int CarbonDatabase::getArrayDimLeftBound(const CarbonDatabaseNode* node, int dim) const
{
  int ret = -1;
  
  const UserType *type = getTrueType(node);
  const UserType *elem_type = NULL;
  if (type != NULL) 
  {
    const UserArray *arrayType = type->castArray();
    if (arrayType != NULL)
    {
      int num_dims = arrayType->getNumDims();
      if(dim < num_dims)
      {
        for(int i = 0; i <= dim; i++)
        {
          if(i == dim)
            ret = arrayType->getRange()->getMsb();
          else
          {
            elem_type = arrayType->getElementType();
            arrayType = elem_type->castArray();
          }
        }
      }
    }
  }
  return ret;
}

int CarbonDatabase::getArrayDimRightBound(const CarbonDatabaseNode* node, int dim) const
{
  int ret = -1;
  
  const UserType *type = getTrueType(node);
  const UserType *elem_type = NULL;
  if (type != NULL) 
  {
    const UserArray *arrayType = type->castArray();
    if (arrayType != NULL)
    {
      int num_dims = arrayType->getNumDims();
      if(dim < num_dims)
      {
        for(int i = 0; i <= dim; i++)
        {
          if(i == dim)
            ret = arrayType->getRange()->getLsb();
          else
          {
            elem_type = arrayType->getElementType();
            arrayType = elem_type->castArray();
          }
        }
      }
    }
  }
  return ret;
}

int CarbonDatabase::getArrayNumDeclaredDims(const CarbonDatabaseNode* node) const
{
  int ret = -1;
  
  const UserType *type = getTrueType(node);
  const UserType *elem_type = NULL;
  if (type != NULL) 
  {
    const UserArray *arrayType = type->castArray();
    if (arrayType != NULL)
    {
      int num_dims = arrayType->getNumDims();
      ret = 0;
      StringAtom* type_name = arrayType->getTypeName();
      for(int i = 0; i < num_dims; i++)
      {
        ret++;
        elem_type = arrayType->getElementType();
        StringAtom* elem_type_name = elem_type->getTypeName();
        bool isTheSame = (elem_type_name == type_name);
        // If the two types are the same, then
        // it is multirange array, like array (range1, range2),
        // so keep counting
        // Otherwise, it's array of arrays, stop counting.
        if(isTheSame)
          arrayType = elem_type->castArray();
        else
          break;
      }
    }
  }
  return ret;
}

int CarbonDatabase::getRangeConstraintLeftBound(const CarbonDatabaseNode* node) const
{
  int ret = -1;
  const UserType *type = getTrueType(node);
  if (type != NULL) 
  {
    const UserScalar *scalarType = type->castScalar();
    if (scalarType != NULL)
    {
      ret = scalarType->getRangeConstraint()->getLsb();
    }
    const UserEnum *enumType = type->castEnum();
    if (enumType != NULL)
    {
      if(enumType->getRange() != NULL)
        ret = enumType->getRange()->getLsb();
    }
  }
  return ret;
}

int CarbonDatabase::getRangeConstraintRightBound(const CarbonDatabaseNode* node) const
{
  int ret = -1;
  const UserType *type = getTrueType(node);
  if (type != NULL) 
  {
    const UserScalar *scalarType = type->castScalar();
    if (scalarType != NULL)
    {
      ret = scalarType->getRangeConstraint()->getMsb();
    }
    const UserEnum *enumType = type->castEnum();
    if (enumType != NULL)
    {
      if(enumType->getRange() != NULL)
        ret = enumType->getRange()->getMsb();
    }
  }
  return ret;
}

int CarbonDatabase::isRangeRequiredInDeclaration(const CarbonDatabaseNode* node) const
{
  const UserType *type = getTrueType(node);

  int ret = type->isRangeRequiredInDeclaration();
  return ret;
}
