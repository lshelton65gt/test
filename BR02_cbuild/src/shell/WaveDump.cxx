// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonAssert.h"
#include "shell/WaveDump.h"
#include "util/Loop.h"
#include "util/StringAtom.h"
#include "util/UtString.h"
#include "util/UtArray.h"
#include "util/UtHashSet.h"
#include "util/UtHashMap.h"
#include "util/UtConv.h"
#include "iodb/IODBUserTypes.h"

static bool sScopeNameCmpAsc(const WaveScope* p, const WaveScope* q);

const char* WaveDump::cShellVersion = "Carbon Shell: 0.1";

//! container class for scope
class WaveScope::Children
{
public: CARBONMEM_OVERRIDES
  //! constructor
  Children() {}
  //! destructor
  ~Children() {
    for (HandleListLoop p(mSignals); 
         ! p.atEnd(); ++p)
      delete *p;
  }

  //! Loop all children (handles and scopes)
  WaveChildIter loopAll() {
    return WaveChildIter::create(ChildListLoop(mAllChildren));
  }

  //! Loop handles
  WaveHandleIter loopHandles() {
    return WaveHandleIter::create(HandleListLoop(mSignals));
  }
  
  //! Loop scopes
  WaveScopeIter loopScopes() {
    return WaveScopeIter::create(ScopeListLoop(mScopes));
  }

  //! add a handle 
  void addHandle(WaveHandle* sig)
  {
    mSignals.push_back(sig);  
    mAllChildren.push_back(sig);
  }

  //! add a scope
  void addScope(WaveScope* scope)
  {
    mScopes.push_back(scope);
    mAllChildren.push_back(scope);
  }

  void sortAll(const UserType* ut)
  {
    // Sorting is only required if there's no user type.  Arrays and
    // structures are sorted as they're added.
    if (ut == NULL)
    {
      // For consistent waveforms from run to run, sort scopes by their
      // names in ascending order.
      std::sort(mScopes.begin(), mScopes.end(), sScopeNameCmpAsc);
      // Also, if this isn't an array/struct, dump signals followed by
      // scopes.  This may not be the order in which things were
      // added, so recreate the list of all children.
      mAllChildren.clear();
      mAllChildren.insert(mAllChildren.end(), mSignals.begin(), mSignals.end());
      mAllChildren.insert(mAllChildren.end(), mScopes.begin(), mScopes.end());
    }
  }

  bool isEmpty() const
  {
    return (! hasChildScopes() && ! hasSignals());
  }
  
  bool hasChildScopes() const
  {
    return ! mScopes.empty();
  }

  bool hasSignals() const
  {
    return ! mSignals.empty();
  }

  UInt32 numChildScopes() const 
  {
    return mScopes.size();
  }

  UInt32 numSignals() const
  {
    return mSignals.size();
  }
  
private:

  typedef UtArray<WaveChild*> ChildList;
  typedef UtArray<WaveHandle*> HandleList;
  typedef UtArray<WaveScope*> ScopeList;
  typedef Loop<ChildList> ChildListLoop;
  typedef Loop<HandleList> HandleListLoop;
  typedef Loop<ScopeList> ScopeListLoop;

  ScopeList mScopes;
  HandleList mSignals;
  ChildList mAllChildren;
};

//! container class for WaveDump
class WaveDump::ScopeNodes
{
public: CARBONMEM_OVERRIDES
  //! constructor
  ScopeNodes() {}

  //! destructor
  ~ScopeNodes() {
    for (ScopeListLoop p(mScopes); 
         ! p.atEnd(); ++p)
      delete *p;
  }

  //! Add a scope
  void addScope(WaveScope* scope)
  {
    mScopes.push_back(scope);
  }

  //! Add a root scope
  void addRoot(WaveScope* scope)
  {
    mRoots.push_back(scope);
  }

  //! Loop all scopes
  WaveScopeIter loopScopes() {
    return WaveScopeIter::create(ScopeListLoop(mScopes));
  }

  //! Loop all roots
  WaveScopeIter loopRoots() {
    return WaveScopeIter::create(ScopeListLoop(mRoots));
  }
  
  void sortRoots()
  {
    std::sort(mRoots.begin(), mRoots.end(), sScopeNameCmpAsc);
    //mRoots.sort(sScopeNameCmp);
  }
  
  void clearFullScopeSet()
  {
    mFullScopes.freeMemory();
  }

  bool hasChildSignals(WaveScope* scope)
  {
    // did we fill the scope hash?
    if (mFullScopes.empty())
      // Nope (or the entire design has no signals)
      fillScopeHash();
    
    return (mFullScopes.find(scope) == mFullScopes.end());
  }
  
private:
  typedef UtArray<WaveScope*> ScopeList;
  typedef Loop<ScopeList> ScopeListLoop;
  typedef UtHashSet<WaveScope*> ScopeSet;

  ScopeList mScopes;
  ScopeList mRoots;
  ScopeSet mFullScopes;

  void fillScopeHash()
  {
    for (WaveScopeIter p = loopRoots(); ! p.atEnd(); ++p)
    {
      WaveScope* scope = *p;
      if (addNonEmptyScopes(scope) || scope->hasSignals())
        mFullScopes.insert(scope);
    }
  }
  
  bool addNonEmptyScopes(WaveScope* scope)
  {
    bool isNonEmpty = false;
    if (scope->hasChildScopes())
    {
      for (WaveScopeIter p = scope->loopScopes(); ! p.atEnd(); ++p)
      {
        WaveScope* lowerScope = *p;
        if (addNonEmptyScopes(lowerScope) || lowerScope->hasSignals())
        {
          mFullScopes.insert(lowerScope);
          isNonEmpty = true;
        }
      }
    }
    
    if (! isNonEmpty && scope->hasSignals())
    {
      // mFullScopes.insert(scope);
      isNonEmpty = true;
    }
    
    return isNonEmpty;
  }
  
};

static bool sScopeNameCmpAsc(const WaveScope* p, const WaveScope* q)
{
  UtString nameBuf1;
  UtString nameBuf2;
  return (strcmp(p->getName(&nameBuf1), q->getName(&nameBuf2)) < 0);
}

const WaveHandle* WaveChild::castWaveHandle() const
{
  return NULL;
}

const WaveScope* WaveChild::castWaveScope() const
{
  return NULL;
}

WaveHandle::WaveHandle(const StringAtom* sigName, 
                       CarbonVerilogType type, CarbonVhdlType vtype,
                       CarbonVarDirection direction,
                       const HdlId& nameInfo, const UserType* ut) :
   mNameInfo(nameInfo)
{
  mName = sigName;
  mType = type;
  mVhdlType = vtype;
  mMasterHandle = NULL;
  mDirection = direction;
  mUserType = ut;
  
  size_t size = getSize();
  
  if (type == eVerilogTypeInteger)
    size = 32;
  if (isReal())
    size = 64;
  mValue = CARBON_ALLOC_VEC(char, size + 1);
  memset(mValue, 'x', size);
  mValue[size] = '\0';
  mIsRegistered = false;
  mRep = NULL;
}

WaveHandle::~WaveHandle()
{
  if (mValue)
    deleteValue();
}

void WaveHandle::setValueX()
{
  UInt32 size = getSize();
  memset(mValue, 'x', size);
  mValue[size] = '\0';
}

CarbonVerilogType WaveHandle::getType() const
{
  return mType;
}

const char* WaveHandle::getName() const
{
  return mName->str();
}

CarbonVarDirection WaveHandle::getDirection() const
{
  return mDirection;
}

const char* WaveHandle::getNameWithRange(UtString* buf, bool addVecSpace,
                                         const char* prefix) const
{
  buf->clear();
  if (prefix) {
    *buf << prefix;
  }
  *buf += getName();

  if (mNameInfo.hasVectorSemantics() && ( !isInteger()) && ( !isTime()) &&
      (!isEnumeration()) && !isCharacter() )
  {
    // CompareScan bug
    if (addVecSpace)
      *buf << ' ';
    
    // now the range or bitsel
    *buf << '[';

    if (mNameInfo.getType() == HdlId::eVectBitRange)
      *buf << mNameInfo.getVectMSB() << ':' << mNameInfo.getVectLSB();
    else
      *buf << mNameInfo.getVectIndex();
    
    *buf << ']';
  }
  return buf->c_str();
}

UInt32 WaveHandle::getSize() const
{
  if (isReal())
    return 64;
  else
    return mNameInfo.getWidth();
  
  // safety 
  return 0;
}

SInt32 WaveHandle::getMSB() const
{
  SInt32 ret = -1;
  if (mNameInfo.getType() == HdlId::eVectBitRange)
    ret = mNameInfo.getVectMSB();
  return ret;
}

SInt32 WaveHandle::getLSB() const
{
  SInt32 ret = -1;
  if (mNameInfo.getType() == HdlId::eVectBitRange)
    ret = mNameInfo.getVectLSB();
  return ret;
}

const UserType* WaveHandle::getUserType() const
{
  return mUserType;
}

bool WaveHandle::isHardwareNet() const
{
  // If there is no user type or it's verilog net,
  // return true
  if((mUserType == NULL) || (mUserType->getLanguageType() == eLanguageTypeVerilog))
    return (mType > eVerilogTypeTime);
  else if(mUserType->getLanguageType() == eLanguageTypeVhdl)
  {
    if(mUserType->getType() == UserType::eArray)
    {
      const UserArray* ua = mUserType->castArray();
      const UserType* utElem = ua->getElementType();
      if(utElem->getVhdlType() < eVhdlTypeInteger)
        return true;
    }
    else
    {
      if(mUserType->getVhdlType() < eVhdlTypeInteger)
        return true;
    }
  }

  return false;
}



void WaveHandle::updateValue(const Value value)
{
  WaveHandle* sig = getTopHandle(this);
  sig->setValue(value);
}

void WaveHandle::setValue(const Value value)
{
  WaveHandle* sig = getTopHandle(this);
  
  // numbers
  unsigned int the_size = sig->getSize();
  if (!isReal() && !isTime())
    INFO_ASSERT(strlen(value) == the_size, value);
  else
    the_size = 64;

  strncpy(sig->mValue, value, the_size + 1);
}

void WaveHandle::updateValueDbl(double dblValue)
{
  setValueDbl(dblValue);
}

void WaveHandle::updateValueInt32(SInt32 intValue)
{
  setValueInt32((UInt32)intValue);
}

void WaveHandle::updateValueInt64(UInt64 intValue)
{
  setValueInt64(intValue);
}

void WaveHandle::setValueDbl(double dblValue)
{
  if (VERIFY(isReal()))
  {
    char val[100];
    sprintf(val, "%.15g", dblValue);
    setValue(val);
  }
}

void WaveHandle::setValueInt32(UInt32 intValue)
{
  if (VERIFY(isInteger()))
  {
    char val[100];
    // Just because it's an integer doesn't mean it's 32 bits wide.
    // It could have a constrained range.
    UInt32 numBits = getSize();
    CarbonValRW::writeBinValToStr(val, 100, &intValue, numBits, false);
    setValue(val);
  }
}

void WaveHandle::setValueInt64(UInt64 intValue)
{
  if (VERIFY(isTime())) // currently time is the only 64 bit integer
                        // we deal with.
  {
    char val[100];
    CarbonValRW::writeBinValToStr(val, 100, &intValue, 64, false);
    setValue(val);
  }
}

void WaveHandle::setIsChanged()
{
  WaveHandle* sig = getTopHandle(this);
  sig->mIsRegistered = true;
}

void WaveHandle::deleteValue()
{
  size_t size = getSize();
  CARBON_FREE_VEC(mValue, char, size + 1);
  mValue = NULL;
}

void WaveHandle::addAlias(WaveHandle* sig)
{
  WaveHandle* link = getTopHandle(sig);
  WaveHandle* master = getTopHandle(this);
  if (link != master)
  {
    link->mMasterHandle = master;
    link->deleteValue();
    //    link->mValue = master->mValue;
  }
}

bool WaveHandle::isSlaveAlias() const
{
  const WaveHandle* top = getTopHandle(this);
  return (top != this);
}

bool WaveHandle::isReal() const
{
  if(mType == eVerilogTypeUnknown)
  {
    // this is vhdl net
    return (mVhdlType == eVhdlTypeReal);
  }
  else 
    return (mType == eVerilogTypeReal) || (mType == eVerilogTypeRealTime);
}

bool WaveHandle::isInteger() const
{
  if(mType == eVerilogTypeUnknown)
  {
    // this is vhdl net
    if((mVhdlType == eVhdlTypeInteger) || 
       (mVhdlType == eVhdlTypeNatural) ||
       (mVhdlType == eVhdlTypePositive) ||
       (mVhdlType == eVhdlTypeIntType)) 
      return true;
    else
      return false;
  }
  else 
    return (mType == eVerilogTypeInteger);
}

bool WaveHandle::isTime() const
{
  return (mType == eVerilogTypeTime);
}

// is this vhdl character
bool WaveHandle::isCharacter() const
{
  if(mType == eVerilogTypeUnknown)
    return (mVhdlType == eVhdlTypeChar);
  else
    return false;
}

// is this vhdl enumeration
bool WaveHandle::isEnumeration() const
{
  if(mType == eVerilogTypeUnknown)
  {
    if(mUserType->getType() == UserType::eEnum)
      return true;
    else 
      return false;
  }
  else
    return false;
}


bool WaveHandle::isVhdlSig() const
{
  return (mType == eVerilogTypeUnknown);
}

CarbonVhdlType WaveHandle::getVhdlType() const
{
  return mVhdlType;
}

WaveHandle::Obj WaveHandle::getObj()
{
  return mRep;
}

WaveHandle::CObj WaveHandle::getObj() const
{
  return mRep;
}

void WaveHandle::setObj(Obj data)
{
  mRep = data;
}

const WaveHandle* WaveHandle::castWaveHandle() const
{
  return this;
}


WaveScope::WaveScope(const StringAtom* name, WaveScope* parentScope,
                     ScopeType type, const UserType* ut, CarbonLanguageType language)
{
  mScopeName = name;
  mParentScope = parentScope;
  mType = type;
  mUserType = ut;
  mChildren = new Children;
  mScopeLanguage = language;
}

WaveScope::~WaveScope()
{
  delete mChildren;
}

WaveHandle* 
WaveScope::addSignal(const StringAtom* sigName, CarbonVerilogType type,
                     CarbonVarDirection direction,
                     const HdlId& nameInfo, const UserType* ut, 
                     CarbonVhdlType vtype)
{
  WaveHandle* sig = new WaveHandle(sigName, type, vtype, direction,
                                   nameInfo, ut);
  mChildren->addHandle(sig);
  return sig;
}

const char* WaveScope::getName(UtString* nameStr) const
{
  nameStr->clear();
  (*nameStr) << mScopeName->str();
  return nameStr->c_str();
}

const char* WaveScope::getNameWithRange(UtString* nameStr) const
{
  nameStr->clear();
  getName(nameStr);
  if (mType == WaveScope::eArray) {
    // Array names should look like arr[msb:lsb] instead of just arr.
    const UserArray* ua = static_cast<const UserArray*>(mUserType);
    const ConstantRange* range = ua->getRange();
    (*nameStr) << "[" << range->getMsb() << ":" << range->getLsb() << "]";
  }
  return nameStr->c_str();
}

const StringAtom* WaveScope::getNameAtom() const
{
  return mScopeName;
}

const WaveScope* WaveScope::getParentScope() const
{
  return mParentScope;
}

void WaveScope::addChild(WaveScope* scope)
{
  mChildren->addScope(scope);
}

WaveScope::ScopeType WaveScope::getType() const
{
  return mType;
}

WaveChildIter WaveScope::loopChildren()
{
  return mChildren->loopAll();
}

WaveHandleIter WaveScope::loopHandles() 
{
  return mChildren->loopHandles();
}

WaveScopeIter WaveScope::loopScopes()
{
  return mChildren->loopScopes();
}

void WaveScope::sortChildren()
{
  mChildren->sortAll(mUserType);
}

bool WaveScope::isEmpty() const
{
  return mChildren->isEmpty();
}

bool WaveScope::hasSignals() const
{
  return mChildren->hasSignals();
}

bool WaveScope::hasChildScopes() const
{
  return mChildren->hasChildScopes();
}

UInt32 WaveScope::numChildren() const
{
  return mChildren->numSignals() + mChildren->numChildScopes();
}

const WaveScope* WaveScope::castWaveScope() const
{
  return this;
}


WaveDump::WaveDump(CarbonTimescale timescale)
{
  mTimeScale = timescale;
  mCurTime = 0;
  mHierOpen = true;
  mDumpOn = true;
  mInitTimeSet = false;
  mTimeCnt = 0;

  mScopeNodes = new ScopeNodes;
}

WaveDump::~WaveDump()
{
  delete mScopeNodes;
}

WaveScope* WaveDump::attachScope(const StringAtom* scopeName, 
                                 WaveScope* parentScope, 
                                 WaveScope::ScopeType type,
                                 const UserType* ut,
                                 CarbonLanguageType language)
{
  WaveScope* scope = new WaveScope(scopeName, parentScope, type, ut, language);
  mScopeNodes->addScope(scope);
  if (parentScope)
    parentScope->addChild(scope);
  else
    mScopeNodes->addRoot(scope);
  
  return scope;
}

void WaveDump::setTimeScale(CarbonTimescale units)
{
  mTimeScale = units;
}

CarbonTimescale WaveDump::getTimeScale()
{
  return mTimeScale;
}

void WaveDump::setInitialTime(const SimTime absTime)
{
  INFO_ASSERT(mInitTimeSet == false, "Wave dump initial time has already been set.");
  // But 7994 identified a case in which mTimeCnt can be incremented
  // before carbonSchedule() is called:
  // - Open an FSDB file
  // - carbonDumpVars()
  // - Manually switch the FSDB file (increments mTimeCnt)
  // - carbonSchedule() (calls setInitialTime())
  //
  // This used to be a debug-only assert:
  // if (VERIFY(mTimeCnt == 0))
  // However, that was also incorrect, because the above sequence
  // would not update the current time.  See bug 8010.
  //
  // Instead, if we've made it this far, always update the time.
  mCurTime = absTime;
  mInitTimeSet = true;
}

SInt32 WaveDump::numAdvanceTimes() const
{
  return mTimeCnt;
}

bool WaveDump::isDumpOn() const
{
  return mDumpOn;
}

WaveDump::SimTime WaveDump::getCurrentTime() const
{
  return mCurTime;
}

void WaveDump::advanceTime(const SimTime curTime)
{
  if (curTime > mCurTime)
  {
    flushPendingData();
    mCurTime = curTime;
  }
}

void WaveDump::addChanged(WaveHandle* modifiedSig)
{
  if (! modifiedSig->isRegistered())
  {
    modifiedSig->setIsChanged();
    mChanges.push_back(modifiedSig);
  }
}

void WaveDump::addChangedWithVal(WaveHandle* sig, const WaveHandle::Value value)
{
  sig->updateValue(value);
  if (! sig->isRegistered())
  {
    sig->setIsChanged();
    mChanges.push_back(sig);
  }
}

WaveScopeIter WaveDump::loopScopes()
{
  return mScopeNodes->loopScopes();
}

WaveScopeIter WaveDump::loopRoots()
{
  return mScopeNodes->loopRoots();
}

void WaveDump::sortRoots()
{
  return mScopeNodes->sortRoots();
}

void WaveDump::writeAllValues()
{
  INFO_ASSERT(! mHierOpen, "Hierarchy has been closed");
  WaveScopeIter scopeP = loopScopes();
  WaveScope* scope;
  while (scopeP(&scope))
  {
    WaveHandleIter sigP = scope->loopHandles();
    WaveHandle* handle;
    while(sigP(&handle))
    {
      if (! handle->isSlaveAlias())
        writeHandleValue(handle);
    }
  }
  // man, this is ugly, but I need a central place where all values
  // including user data values get written. So, I have to add yet
  // another virtual call here. At some point, I should
  // replicate all the logic here and just put it in each file writer.
  // Writing all the values is very rare. That only happens for a
  // dumpAll(), dumpOff(), dumpOn(), and the first
  // timestamp. Normally, updates just write changed values.
  writeAllUserData();
}

void WaveDump::xAllHandles()
{
  WaveScopeIter scopeP = loopScopes();
  WaveScope* scope;
  while (scopeP(&scope))
  {
    WaveHandleIter sigP = scope->loopHandles();
    WaveHandle* handle;
    while(sigP(&handle))
    {
      if (! handle->isSlaveAlias())
        handle->setValueX();
    }
  }
}

bool WaveDump::isEmpty(WaveScope* scope)
{
  bool isEmpty = scope->isEmpty();
  if (! isEmpty)
    isEmpty = mScopeNodes->hasChildSignals(scope);
  return isEmpty;
}

void WaveDump::clearAuxHierStructs()
{
  mScopeNodes->clearFullScopeSet();
}

void WaveDump::flushPendingData()
{
  // This could be called from the CarbonWave destructor without
  // having closed the hierarchy.
  if (mHierOpen) return;

  if (mDumpOn && (! mChanges.empty() || mTimeCnt == 0))
  {
    writeValueChanges();
    ++mTimeCnt;
    mChanges.clear();
  }
}

void WaveDump::dumpAll(const SimTime curTime)
{
  if (mDumpOn)
  {
    if (mTimeCnt > 0) 
    {
      mCurTime = curTime;
      dumpDataAll();
      mChanges.clear();
    }
  }
}

void WaveDump::dumpOn(const SimTime curTime)
{
  if (!mDumpOn)
  {
    mCurTime = curTime;
    dumpDataOn();
    mChanges.clear();
    mDumpOn = true;
  }
}

void WaveDump::dumpOff(const SimTime curTime)
{
  if (mDumpOn && (!mChanges.empty() || mTimeCnt > 0))
  {
    flushPendingData();
    // mChanges is now empty.

    mCurTime = curTime;
    xAllHandles();
    dumpDataOff();
    mDumpOn = false;
  }
}

bool WaveDump::supportsArrayScopes() const
{
  return false;
}

bool WaveDump::isFsdb() const
{
  return false;
}

CarbonWaveUserData* WaveDump::addUserVar(CarbonVarType, 
                                         CarbonVarDirection,
                                         CarbonDataType,
                                         int, int,
                                         CarbonClientData, 
                                         const char*, 
                                         CarbonBytesPerBit,
                                         const char* fullPathScopeName,
                                         const char*,
                                         UtString*)
{
  INFO_ASSERT(isFsdb(), fullPathScopeName);
  // safety, just in case there is a major bug in FsdbFile
  INFO_ASSERT(0, fullPathScopeName);
  return NULL;
}

void WaveDump::eraseUserData(CarbonWaveUserData*)
{
}

void WaveDump::writeAllUserData()
{}

bool WaveDump::switchFSDBFile(const char* )
{
  INFO_ASSERT(isFsdb(), "switchFSDBFile called on a non-FSDB wave file");
  // safety, just in case there is a major bug in FsdbFile
  INFO_ASSERT(false, "switchFSDBFile called on a non-FSDB wave file");
  return false;
}
