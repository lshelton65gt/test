// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CARBONDATABASENODE_H_
#define CARBONDATABASENODE_H_

#include "util/UtHashSet.h"
#include "util/UtHashMap.h"

class STSymbolTableNode;
class AtomicCache;
class IODBRuntime;
class StringAtom;
class UserType;
class ShellData;
class CarbonDatabaseSymtabIter;

//! User-visible node representing a design element
/*!
  For simple design nodes, this just wraps the STSymbolTableNode.
  However, for composites (especially arrays), this has additional
  information about the dimension and index of the array that is
  represented by this node.  Parent/child relationships are also
  maintained.

  Each node stores a name in the form of a StringAtom.  For most
  nodes, this is the same as the corresponding STSymbolTableNode's
  name.  However, for array elements, a new name is composed,
  including the necessary array indices.

  A node's children are stored differently depending on the parent's
  type.  For normal nodes, children are mapped using their StringAtom
  name.  However, to allow easier lookup and ordered looping, children
  of array nodes are mapped using their element index.
 */
class CarbonDatabaseNode
{
public:
  CARBONMEM_OVERRIDES

  //! Construct a node representing an indexed child
  CarbonDatabaseNode(const CarbonDatabaseNode* dbParent, const STSymbolTableNode* stNode,
                     const StringAtom* name, const UserType* ut, UInt32 dim, SInt32 index);
  //! Construct a node representing a named child
  CarbonDatabaseNode(const CarbonDatabaseNode* dbParent, const STSymbolTableNode* stNode,
                     const StringAtom* name, const UserType* ut, SInt32 index);

  ~CarbonDatabaseNode();

  //! Print my contents to UtIO::cout()
  void print() const;

  //! Does this node represent an array dimension?
  bool isArrayDim() const { return mIsArrayDim; }
  //! What index does this node represent?
  /*!
    For array elements, this is the index of the dimension in the
    array.  For structure fields, this is the index of the field
    within the structure.
   */
  SInt32 getIndex() const { return mIndex; }
  //! What array dimension is represented?
  UInt32 getDimension() const { return mDimension; }
  //! Get the node's internal name
  const StringAtom* getName() const { return mName; }
  //! What is the corresponding node in the design symbol table?
  const STSymbolTableNode* getSymTabNode() const { return mSymTabNode; }
  //! What is the type of this node?
  const UserType* getUserType() const { return mUserType; }
  //! Get the shell data
  ShellData* getShellData() { return mShellData; }

  //! Put the shell data 
  void setShellData(ShellData* sd) { mShellData = sd; }


  //! Composes the full node name into a UtString
  void composeFullName(UtString* buf) const;
  //! Returns the internally-stored leaf name
  const char* getLeafName() const;

  //! Does this node have indexed (vs. named) children?
  bool isIndexed() const { return !mIndexedChildren.empty(); }

  //! Look up a child by name - only valid for nodes with named children
  CarbonDatabaseNode* findChild(const StringAtom* name) const;
  //! Look up a child by index - only valid for nodes with indexed children
  CarbonDatabaseNode* findChild(SInt32 index) const;

  //! Add a child to this node
  void addChild(CarbonDatabaseNode* child);

  const CarbonDatabaseNode* getParent() const { return mParent; }
  CarbonDatabaseNode* getParent() { return const_cast<CarbonDatabaseNode*>(mParent); }

  typedef UtHashMap<const StringAtom*, CarbonDatabaseNode*> NamedNodeMap;
  typedef UtHashMap<SInt32, CarbonDatabaseNode*> IndexedNodeMap;

  friend class NamedChildLoop;

  //! Unsorted loop over a node's named children
  class NamedChildLoop
  {
  public:
    CARBONMEM_OVERRIDES

    NamedChildLoop(const CarbonDatabaseNode *node)
      : mMap(node->mNamedChildren)
    {
      mIter = mMap.begin();
    }
    ~NamedChildLoop() {}

    const CarbonDatabaseNode* next()
    {
      const CarbonDatabaseNode* node = NULL;
      if (mIter != mMap.end()) {
        node = mIter->second;
        ++mIter;
      }
      return node;
    }

  protected:
    NamedNodeMap::const_iterator mIter;
    const NamedNodeMap& mMap;

  private:
    // Forbid
    NamedChildLoop(const NamedChildLoop&);
    NamedChildLoop& operator=(const NamedChildLoop&);
  };

  friend class IndexedChildLoop;

  //! Unsorted loop over a node's indexed children
  class IndexedChildLoop
  {
  public:
    CARBONMEM_OVERRIDES

    IndexedChildLoop(const CarbonDatabaseNode *node)
      : mMap(node->mIndexedChildren)
    {
      mIter = mMap.begin();
    }
    ~IndexedChildLoop() {}

    const CarbonDatabaseNode* next()
    {
      const CarbonDatabaseNode* node = NULL;
      if (mIter != mMap.end()) {
        node = mIter->second;
        ++mIter;
      }
      return node;
    }

  protected:
    IndexedNodeMap::const_iterator mIter;
    const IndexedNodeMap& mMap;

  private:
    // Forbid
    IndexedChildLoop(const IndexedChildLoop&);
    IndexedChildLoop& operator=(const IndexedChildLoop&);
  };


protected:
  //! Does this node represent an array dimension?
  const bool mIsArrayDim;
  //! This node's index
  /*!
    For nodes representing array dimensions, this index is the array
    index of the element in that dimension.  For nodes representing
    structure fields, this is used to represent the index within the
    structure.
   */
  const SInt32 mIndex;
  //! This node's array dimension
  const UInt32 mDimension;
  //! Name for this node
  const StringAtom * const mName;
  //! Corresponding symtab node
  const STSymbolTableNode * const mSymTabNode;
  //! Node's user type
  const UserType * const mUserType;
  //! Node's ShellData
  ShellData* mShellData;

  //! Parent of the node
  const CarbonDatabaseNode * const mParent;

  //! Map of named children
  NamedNodeMap mNamedChildren;
  //! Map of indexed children
  IndexedNodeMap mIndexedChildren;

  //! Prepends node name to a string, optionally including a delimiter
  void prependName(UtString* buf, bool includeDelim) const;

private:
  // Forbid
  CarbonDatabaseNode(const CarbonDatabaseNode&);
  CarbonDatabaseNode& operator=(const CarbonDatabaseNode&);
};


//! Factory class for CarbonDatabaseNodes
/*!
  This is a very low-level factory class, basically doing only memory
  management.  Parent/child relationships need to be maintained
  explicitly outside the factory.  Also, no validation is made that
  the STSymbolTableNode can be represented as a CarbonDatabaseNode.
 */
class CarbonDatabaseNodeFactory
{
public:
  CARBONMEM_OVERRIDES

  CarbonDatabaseNodeFactory(IODBRuntime* iodb, AtomicCache* atomicCache);
  ~CarbonDatabaseNodeFactory();

  //! Adds a symbol table node as a child of a database node, if it doesn't already exist
  CarbonDatabaseNode* addToDB(CarbonDatabaseNode* dbParent, const STSymbolTableNode* stNode,
                              bool isArrayDim, SInt32 arrayIndex = 0);


protected:

  //! Class to represent unique attributes of a CarbonDatabaseNode
  /*!
    This is used when allocating nodes to ensure duplicates are never created.
   */
  class NodeAttr
  {
  public:
    CARBONMEM_OVERRIDES

    NodeAttr(const CarbonDatabaseNode* dbParent, const STSymbolTableNode* stNode, SInt32 index);
    ~NodeAttr();

    size_t hash() const;
    bool equal(const NodeAttr* other) const;

  private:
    const CarbonDatabaseNode* mDBParent;
    const STSymbolTableNode* mSTNode;
    const SInt32 mIndex;

    // Forbid
    NodeAttr(const NodeAttr&);
    NodeAttr& operator=(const NodeAttr&);
  };

  //! Helper class to compare/hash NodeAttr objects in a UtHashMap
  class MapHelper
  {
  public:
    CARBONMEM_OVERRIDES

    size_t hash(const NodeAttr* n) const;
    bool equal(const NodeAttr* n1, const NodeAttr* n2) const;
  };

  //! Allocate a new node, which must not yet exist
  CarbonDatabaseNode* allocateNode(CarbonDatabaseNode* dbParent, const STSymbolTableNode* stNode,
                                   const StringAtom* name, bool isArrayDim, SInt32 arrayIndex);

  IODBRuntime* mIODB;
  AtomicCache* mAtomicCache;
  typedef UtHashMap<NodeAttr*, CarbonDatabaseNode*, MapHelper> NodeMap;
  NodeMap mNodeMap;
  CarbonDatabaseNode *mRootNode;

private:
  // Forbid
  CarbonDatabaseNodeFactory(const CarbonDatabaseNodeFactory&);
  CarbonDatabaseNodeFactory& operator=(const CarbonDatabaseNodeFactory&);
};

//! Base iterator for CarbonDatabaseNodes
/*!
  For most loops (e.g. loopDeposits), the corresponding loop over the
  STSymbolTableNodes needs to be used, and each STSymbolTableNode
  needs to be elaborated into all its possible CarbonDatabaseNodes.
  This allows the iterator to be fairly generic.

  Note that the iterator is not populated upon construction.  The
  populate method must be called explicitly.  This is because derived
  classes are populated differently than the base class, and the
  virtual populate() function can't be called from the constructor.

  The alternative would have been to call explicitly a non-virtual
  populate() function from each class's constructor (or inline the
  custom population code), but that would require the additional
  complexity of ensuring the derived classes are only populated once.
 */
class CarbonDatabaseNodeIter
{
public:
  CARBONMEM_OVERRIDES
  
  CarbonDatabaseNodeIter();
  virtual ~CarbonDatabaseNodeIter();

  //! Return the next stored node of this iterator
  const CarbonDatabaseNode* next();
  //! Elaborate/save all CarbonDatabaseNodes corresponding to the nodes in a symtab loop
  virtual void populate(CarbonDatabaseSymtabIter* iter, CarbonDatabase* db);

  typedef UtArray<const CarbonDatabaseNode*> NodeArray;

protected:
  //! Current index into the array of nodes
  SInt32 mIndex;
  //! Saved nodes for the iterator
  NodeArray mNodes;

private:
  // Forbid
  CarbonDatabaseNodeIter(const CarbonDatabaseNodeIter&);
  CarbonDatabaseNodeIter& operator=(const CarbonDatabaseNodeIter&);
};

/*!
  Looping the children of a CarbonDBNode is a special case.  No symtab
  loop is required.  Instead, the existing children of the node are
  looped.
 */
class CarbonDatabaseNodeChildIter : public CarbonDatabaseNodeIter
{
public:
  CARBONMEM_OVERRIDES

  CarbonDatabaseNodeChildIter(const CarbonDatabaseNode* parent);
  virtual ~CarbonDatabaseNodeChildIter();
  
  virtual void populate(CarbonDatabaseSymtabIter*, CarbonDatabase*);

private:
  const CarbonDatabaseNode* mParent;
};

#endif
