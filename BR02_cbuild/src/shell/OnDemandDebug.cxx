// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/OnDemandMgr.h"
#include "shell/OnDemandDebug.h"
#include "iodb/IODBRuntime.h"
#include "symtab/STSymbolTable.h"
#include "util/ShellMsgContext.h"

//! Default debug mode callback function
static void sDefaultCB(CarbonObjectID *obj,
                       void*,
                       CarbonTime simTime,
                       CarbonOnDemandDebugType type,
                       CarbonOnDemandDebugAction action,
                       const char *path,
                       CarbonUInt32 length)
{
  CarbonModel *model = obj->getModel ();
  MsgContext *context = model->getMsgContext();
  UtString msg;
  msg << "OnDemand debug event: time=" << simTime << " type=";
  switch (type) {
  case eCarbonOnDemandDebugNonIdleNet: msg << "NonIdleNet"; break;
  case eCarbonOnDemandDebugNonIdleChange: msg << "NonIdleChange"; break;
  case eCarbonOnDemandDebugRestore: msg << "StateRestore"; break;
  case eCarbonOnDemandDebugDiverge: msg << "Divergence"; break;
  case eCarbonOnDemandDebugFail: msg << "FailedRepeat"; break;
  }
  msg << " action=";
  switch (action) {
  case eCarbonOnDemandDebugAPI: msg << "api"; break;
  case eCarbonOnDemandDebugInternal: msg << "internal"; break;
  case eCarbonOnDemandDebugCModel: msg << "cmodel"; break;
  }
  msg << " name=" << path;
  if (type == eCarbonOnDemandDebugFail) {
    msg << " length=" << length;
  }

  context->SHLOnDemandMsg(msg.c_str());
}

OnDemandDebug::OnDemandDebug(OnDemandMgr *mgr)
  : mMgr(mgr),
    mInfo(NULL),
    mLevel(eCarbonOnDemandDebugNone),
    mDepInfo(eCarbonOnDemandDebugAPI, "<deposits>"),
    mSchedInfo(eCarbonOnDemandDebugAPI, "<schedule types>")
{
  mapDebugOffsets();
}

OnDemandDebug::~OnDemandDebug()
{
  for (UInt32 i = 0; i < mAllocatedNodes.size(); ++i) {
    delete mAllocatedNodes[i];
  }
}

void OnDemandDebug::mapDebugOffsets()
{
  // We save a map of symbol table nodes to state offsets in the IODB.
  // We also save the total state size.  This makes it easy to
  // construct an array (indexed by offset) of debug info objects.
  IODBRuntime* db = mMgr->getModel()->getHookup()->getDB();

  UInt32 size = db->getOnDemandStateSize();
  mDebugMap.resize(size);
  // Also keep a temporary array of offsets and the symtab leaves they
  // represent
  UtArray<STAliasedLeafNode*> leafArray(size);

  // Loop over all the node offsets
  for (IODB::NameIntMapLoop l(db->loopOnDemandStateOffsets()); !l.atEnd(); ++l) {
    STSymbolTableNode *node = l.getKey();
    STAliasedLeafNode *leaf = node->castLeaf();
    ST_ASSERT(leaf, node);

    UInt32 offset = l.getValue();
    INFO_ASSERT(offset < size, "Out-of-bounds OnDemand debug state offset");

    UtString name;
    makeFriendlyName(leaf, &name);

    // Create a debug info object and store it at the offset into the
    // debug map
    OnDemandDebugInfo *info = new OnDemandDebugInfo(eCarbonOnDemandDebugInternal, name);
    mAllocatedNodes.push_back(info);
    mDebugMap[offset] = info;

    // Save the symtab leaf as well.  We'll use it later to build the
    // leaf->offsets map.  Everything will be done using the master of
    // the alias ring.
    leafArray[offset] = leaf->getMaster();
  }

  // If the IODB was used instead of the full DB, no offsets are
  // stored.  Print a warning, and create a dummy info at offset 0.
  // The next bit of code will copy it over the rest of the map.
  if (db->getDBType() == eCarbonIODB) {
    MsgContext *msg = mMgr->getModel()->getMsgContext();
    msg->SHLOnDemandDebugIODB();

    OnDemandDebugInfo *info = new OnDemandDebugInfo(eCarbonOnDemandDebugInternal, "<unknown>");
    mAllocatedNodes.push_back(info);
    mDebugMap[0] = info;
  }

  // If a node spans multiple offsets, we only store the starting one.
  // Go through the array and expand the valid object pointers upwards
  // until the next valid one is seen.
  // Also, map each symtab leaf to its size/offset in the state buffer.
  OnDemandDebugInfo *last_info = mDebugMap[0];
  INFO_ASSERT(last_info, "Invalid entry at offset 0 of onDemand debug state");
  OffsetSizePair *last_pair = NULL;
  for (UInt32 i = 0; i < size; ++i) {
    if (mDebugMap[i]) {
      // Found the starting offset of a signal.  Save it so we can
      // fill in any following null entries.
      last_info = mDebugMap[i];
      // Create the entry in the leaf->offset map.  Note that if the
      // IODB was used, the entry won't be valid.
      STAliasedLeafNode *leaf = leafArray[i];
      if (leaf) {
        // Intially, this leaf maps to one byte in the buffer.  We'll
        // adjust that if necessary by saving a pointer to the
        // inserted OffsetSizePair.
        last_pair = &mLeafOffsets.insertInit(leaf, OffsetSizePair(i, 1));
      }
    } else {
      // Fill in this empty offset with the saved one
      mDebugMap[i] = last_info;
      // Similarly, if we have a valid pointer, increment the number
      // of bytes covered by this leaf.
      if (last_pair) {
        ++(last_pair->second);
      }
    }
  }
}

void OnDemandDebug::checkSize(UInt32 size)
{
  INFO_ASSERT(mDebugMap.size() == size, "OnDemand calculated debug state size does not match actual state size");
}

//! Determines the longest repeating pattern of values in an array
template <class _ArrayType, class _ValType>
static void sFindPattern(const _ArrayType &array, UInt32 num_states, UInt32 *length, UInt32 *states)
{
  // Here's the process for identifying a repeating pattern:
  //
  // 1. Search backwards through the array, looking for an element
  // matching the last element.  Call the distance between the two
  // instances D.
  //
  // 2. Continue moving backwards, comparing the element at the
  // current index I with the element at index I + D.
  //
  // 3. If the elements don't match, wait for the next element that
  // matches the last element.
  //
  // 4. Remember the front-most element that was part of a matching
  // sequence, as well as that sequence length.

  UInt32 last_index = num_states - 1;
  _ValType last_val = array[last_index];
  UInt32 curr_index = last_index;

  // The best repeat interval, and earliest index that was part of the
  // pattern.
  UInt32 pattern_length = 0;
  UInt32 earliest_index = last_index;
  // Are we looking for a pattern, or checking one?
  bool looking = true;

  while (curr_index > 0) {
    --curr_index;
    _ValType curr_val = array[curr_index];
    if (!looking) {
      // Make sure this matches the expected pattern
      _ValType match_val = array[curr_index + pattern_length];
      if (curr_val == match_val) {
        // Record the index
        earliest_index = curr_index;
      } else {
        // Go back to looking.  There may be a larger pattern
        looking = true;

        // Reset the current index to just before the index where we
        // identified the current pattern.  If we just resume looking
        // from where we are, we might have missed the ideal larger
        // pattern.  This is required for sequences like this one:
        //
        // 11100011100
        curr_index = last_index - pattern_length - 1;
      }
    }

    if (looking) {
      if (curr_val == last_val) {
        // Found a pattern, so record it
        earliest_index = curr_index;
        pattern_length = last_index - curr_index;
        looking = false;
      }
    }
  }

  // Copy results
  *length = pattern_length;
  *states = num_states - earliest_index;
}

UInt32 OnDemandDebug::sFindLCM(const UInt32Array &patternLengths)
{
  // To prevent against overflow on multiplication, bail out if either
  // term exceeds 16 bits.  Pessimistic, but we shouldn't be dealing
  // with patterns that long anyway.
  const UInt32 cMaxTerm = 0xffff;
  // The maximum value we can return.  Used when a muliplication term
  // exceed its maximum.
  const UInt32 cMaxVal = 0xffffffff;

  // Build a set of all the lengths
  UtHashSet<UInt32> lengthSet;
  for (UInt32 i = 0; i < patternLengths.size(); ++i) {
    UInt32 length = patternLengths[i];
    if (length > 1) {
      lengthSet.insert(length);
    }
  }

  // Three points:
  // 1.  LCM is associative -> LCM(A, B, C) == LCM(A, LCM(B, C));
  // 2.  LCM(A, B) = (A * B)/GCD(A, B);
  // 3.  GCD found through Euclid's algorithm
  UInt32 lcm = 1;
  for (UtHashSet<UInt32>::SortedLoop l(lengthSet.loopSorted());
       (lcm != cMaxVal) && !l.atEnd(); ++l) {
    UInt32 x = *l;
    if ((x > cMaxTerm) || (lcm > cMaxTerm)) {
      lcm = cMaxVal;
    } else {
      UInt32 gcd = lcm;
      UInt32 rem = x;
      while (rem != 0) {
        UInt32 temp = rem;
        rem = gcd % rem;
        gcd = temp;
      }
      if (gcd != x) {
        lcm *= (x / gcd);
      }
    }
  }

  return lcm;
}

void OnDemandDebug::sFilterMultiples(UInt32Array *patternLengths, UInt32 maxLength)
{
  // We want to reduce the number of failures we report.  For example,
  // if there were individual signals with repeating periods of 2, 3,
  // 4, 5, 6, and 8, we should report only 5, 6, and 8.  The values
  // that have multiples that are also in the list are filtered.

  // To start, we create an array with an index for each possible
  // period length.  Each element is an array of offsets into the
  // pattern array that match this length.
  UtArray<UInt32Array*> lengthsToOffsets(maxLength + 1);
  for (UInt32 i = 0; i < patternLengths->size(); ++i) {
    UInt32 length = (*patternLengths)[i];
    if (length > 1) {
      UInt32Array *arr = lengthsToOffsets[length];
      if (arr == NULL) {
        // First instance of this length.  Allocate the array
        arr = new UInt32Array;
        lengthsToOffsets[length] = arr;
      }
      arr->push_back(i);
    }
  }

  // Now, starting at the lowest length, see if there are any larger
  // lengths that are a multiple of it.  If so, clear the matching
  // entries in the length array and move to the next length
  //
  // Note that this is O(N^2), but that shouldn't matter because the
  // algorithm to determine individual signals' pattern lengths in
  // findNonRepeatingNets() is O(N*M), where M is the number of state
  // points in the design, and M >> N.
  for (UInt32 curr = 2; curr <= maxLength; ++curr) {
    UInt32Array *arr = lengthsToOffsets[curr];
    if (arr != NULL) {
      // This length is represented in the array.  See if it has any
      // multiples.
      bool found = false;
      for (UInt32 i = curr + 1; !found && (i <= maxLength); ++i) {
        if ((lengthsToOffsets[i] != NULL) && ((i % curr) == 0)) {
          // This offset represents a length and is a multiple of the
          // current length.  Set all its entries to 1 so they won't
          // be reported as non-repeating.
          found = true;
          for (UInt32 j = 0; j < arr->size(); ++j) {
            UInt32 offset = (*arr)[j];
            (*patternLengths)[offset] = 1;
          }
        }
      }
      // Clear out the entry
      delete arr;
      lengthsToOffsets[curr] = NULL;
    }
  }
}

void OnDemandDebug::analyzeFailedPattern(const OnDemandMgr::DepositArray &dep_array,
                                         const OnDemandMgr::StateArray &state_array,
                                         const OnDemandMgr::SchedTypeArray &sched_array,
                                         UInt32 num_states)
{
  // An array to store the length of each offset's individual
  // repeating pattern, plus two entries at the end (for deposits and
  // schedule types)
  UInt32 debug_map_size = mDebugMap.size();
  UInt32Array pattern_lengths(debug_map_size + eOffsetNum);

  // First see if any nets didn't repeat.  If that's the case, we're
  // done, unless we're reporting all events
  if (findNonRepeatingNets(dep_array, state_array, sched_array, num_states, &pattern_lengths) &&
      !reportAllEvents()) {
    return;
  }

  // For each length, see if the addition of it pushes the LCM over
  // the number of states tracked.  If so, it needs to be reported.

  // Now it gets more complicated.  We need to look at the repeat
  // periods for each of the individual signals.  If their least
  // common multiple exceeds the number of states tracked, they also
  // contributed to the failed pattern detection.
  if (sFindLCM(pattern_lengths) > num_states) {

    // Filter smaller lengths if there are longer lengths that are
    // integer multiples of them.  For example, if the max states is 10,
    // and there are individual lengths of 2, 4, and 5, we would only
    // report 4 and 5.
    sFilterMultiples(&pattern_lengths, num_states);

    for (UInt32 i = 0; i < pattern_lengths.size(); ++i) {
      if (pattern_lengths[i] > 1) {
        OnDemandDebugInfo *info = NULL;
        if (i >= debug_map_size) {
          // There are special entries at the end of the debug map.
          // Select the right one.
          UInt32 offset = i - debug_map_size;
          switch (offset) {
          case eOffsetDeposits:
            info = &mDepInfo;
            break;
          case eOffsetSchedule:
            info = &mSchedInfo;
            break;
          default:
            INFO_ASSERT(0, "Unmapped debug map offset");
          }
        } else {
          info = mDebugMap[i];
        }
        report(eCarbonOnDemandDebugFail, info, pattern_lengths[i]);
      }
    }
  }
}

bool OnDemandDebug::findNonRepeatingNets(const OnDemandMgr::DepositArray &dep_array,
                                         const OnDemandMgr::StateArray &state_array,
                                         const OnDemandMgr::SchedTypeArray &sched_array,
                                         UInt32 num_states,
                                         UInt32Array *pattern_lengths)
{
  // Report any deposits and internal nets that didn't repeat for the
  // duration of the saved sequence.  Return whether we found any.

  // Look for a repeating pattern in deposits first.  Note that at
  // least initially, we won't identify specific deposit nets/values
  // that caused non-cyclic behavior.  We'll just identify that
  // deposits in general were to blame.
  //
  // Also, since we guarantee that two equivalent OnDemandDeposits in
  // the array are actually pointers to the same object, we can just
  // compare pointers.

  UInt32 length, states;
  bool found = false;

  sFindPattern<OnDemandMgr::DepositArray, OnDemandDeposits*>(dep_array, num_states, &length, &states);

  // Only print deposits that didn't repeat for the entire state space
  if (states != num_states) {
    report(eCarbonOnDemandDebugFail, &mDepInfo, 0);
    found = true;
  } else {
    // Record the length.  Deposits are after the other signals in the array.
    (*pattern_lengths)[mDebugMap.size() + eOffsetDeposits] = length;
  }

  // The schedule type is checked in a similar manner.

  sFindPattern<OnDemandMgr::SchedTypeArray, ScheduleFnContainer::ScheduleType>(sched_array, num_states, &length, &states);

  // Only print schedule types that didn't repeat for the entire state space
  if (states != num_states) {
    report(eCarbonOnDemandDebugFail, &mSchedInfo, 0);
    found = true;
  } else {
    // Record the length.  Schedule types are after the other signals in the array.
    (*pattern_lengths)[mDebugMap.size() + eOffsetSchedule] = length;
  }

  // Now for the state.  Extract all the state values at a particular
  // index in the various OnDemandState objects and store them in an
  // array so we can pass them to the templatized pattern-finding
  // function.

  UInt8 *val_array = CARBON_ALLOC_VEC(CarbonUInt8, num_states);
  OnDemandDebugInfo *last_info = 0;

  for (UInt32 debug_index = 0; debug_index < mDebugMap.size(); ++debug_index) {
    OnDemandDebugInfo *info = mDebugMap[debug_index];
    // If this offset corresponds to a node that we already reported
    // as not repeating, there's no need to check this one.
    if (info != last_info) {

      // Extract the values of this index
      for (UInt32 i = 0; i < num_states; ++i) {
        val_array[i] = state_array[i]->getValue(debug_index);
      }
      // Look for patterns
      sFindPattern<UInt8*, UInt8>(val_array, num_states, &length, &states);

      // Only print signals that didn't repeat for the entire state space
      if (states != num_states) {
        report(eCarbonOnDemandDebugFail, info, 0);
        last_info = info;
        found = true;
      } else {
        // Record the length at this offset.
        (*pattern_lengths)[debug_index] = length;
      }
    }
  }

  CARBON_FREE_VEC(val_array, CarbonUInt8, mDebugMap.size());

  return found;
}

void OnDemandDebug::report(CarbonOnDemandDebugType reason, OnDemandDebugInfo *debug_info, UInt32 length)
{
  // Check the event level to see if we should mask this.  We should
  // if the level is "none", regardless of the current mode, or if the
  // level is "idle", provided we're in backoff or stopped mode.
  CarbonOnDemandMode mode = mMgr->getMode();
  if ((mLevel == eCarbonOnDemandDebugNone) ||
      ((mLevel == eCarbonOnDemandDebugIdle) &&
       ((mode == eCarbonOnDemandBackoff) || (mode == eCarbonOnDemandStopped)))) {
    return;
  }

  // If no debug info was passed, use our recorded one
  OnDemandDebugInfo *report_info = debug_info ? debug_info : mInfo;

  INFO_ASSERT(report_info, "No OnDemand debug information available for event reporting");

  // Run the debug callbacks, if they exist
  UInt32 numCBs = mDebugCBs.size();
  if (numCBs != 0) {
    for (UInt32 i = 0; i < numCBs; ++i) {
      OnDemandDebugCB *cb = mDebugCBs[i];
      cb->run(mMgr->getModel()->getObjectID (),
              mMgr->getTime(),
              reason,
              report_info->mAction,
              report_info->mName.c_str(),
              length);
    }
  } else {
    // Run the default one if nothing is registered
    sDefaultCB(mMgr->getModel()->getObjectID (),
               NULL,
               mMgr->getTime(),
               reason,
               report_info->mAction,
               report_info->mName.c_str(),
               length);
  }
}

CarbonOnDemandCBDataID *OnDemandDebug::addCB(CarbonOnDemandDebugCBFunc fn, void *userData)
{
  // OnDemandMgr manages and frees these
  OnDemandDebugCB *cb = new OnDemandDebugCB(fn, userData);
  mDebugCBs.push_back(cb);
  return cb;
}

CarbonStatus OnDemandDebug::exclude(CarbonNetID *net)
{
  ShellNet *shellNet = net->castShellNet();
  STAliasedLeafNode *master = shellNet->getNameAsLeaf()->getMaster();
  CarbonStatus stat = exclude(master);
  return stat;
}

CarbonStatus OnDemandDebug::exclude(STAliasedLeafNode *leaf)
{
  // See if we have an entry in the map.  If not, issue a warning that
  // this net can't be excluded.
  LeafOffsetMap::iterator iter = mLeafOffsets.find(leaf);
  if (iter == mLeafOffsets.end()) {
    MsgContext *msg = mMgr->getModel()->getMsgContext();
    UtString name;
    leaf->compose(&name);
    msg->SHLOnDemandNoExclude(name.c_str());
    return eCarbon_ERROR;
  }

  // Add this offset/size to our exclusion list
  OffsetSizePair &p = iter->second;
  mExclusions.push_back(&p);
  return eCarbon_OK;
}

void OnDemandDebug::maskExcluded(OnDemandState *stateBuffer)
{
  // For each saved exclusion, mask the corresponding bytes in the
  // state buffer
  if (!mExclusions.empty()) {
    for (ExclusionList::iterator iter = mExclusions.begin(); iter != mExclusions.end(); ++iter) {
      OffsetSizePair *p = *iter;
      UInt32 offset = p->first;
      UInt32 size = p->second;
      UInt8 *buf = stateBuffer->getOffset(offset);
      memset(buf, 0, size);
    }
    // We changed the state, so we need to recalculate its hash value
    // before it gets inserted into the UtHashSet.
    stateBuffer->recalculateHashValue();
  }
}

void OnDemandDebug::makeFriendlyName(const STAliasedLeafNode *leaf, UtString *name)
{
  // The offset is stored with the storage node, which might not be
  // the best alias to use for reporting debug events.  Get a
  // suitable name for this leaf, using a non-generated name if
  // possible.
  STAliasedLeafNode::ConstAliasLoop l(leaf);
  bool found = false;
  const STAliasedLeafNode *alias;
  while (!found && !l.atEnd()) {
    alias = *l;
    name->clear();
    alias->verilogCompose(name);
    // If there's no '$', use this name
    if (name->find('$') == UtString::npos) {
      found = true;
    } else {
      ++l;
    }
  }
}

OnDemandDebugCB::OnDemandDebugCB(CarbonOnDemandDebugCBFunc fn, void *userData)
  : mCBFunc(fn),
    mCBData(userData)
{
}

OnDemandDebugCB::~OnDemandDebugCB()
{
}

void OnDemandDebugCB::run(CarbonObjectID *obj,
                          CarbonTime simTime,
                          CarbonOnDemandDebugType type,
                          CarbonOnDemandDebugAction action,
                          const char *path,
                          CarbonUInt32 length)
{
  // If the callback is enabled and has a valid function pointer, call
  // the function with the saved user data.
  if (isEnabled() && mCBFunc) {
    (*mCBFunc)(obj, mCBData, simTime, type, action, path, length);
  }
}

