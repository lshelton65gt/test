// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CARBONDATABASE_H_
#define CARBONDATABASE_H_

#include "util/CarbonPlatform.h"
#include "util/c_memmanager.h"
#include "util/UtString.h"
#include "util/UtHashSet.h"
#include "symtab/STFieldBOM.h"
#include "CarbonDatabaseNode.h"

class IODBRuntime;
class CarbonDatabaseNode;
class CarbonDatabaseNodeFactory;
class CarbonDatabaseNodeIter;
class CarbonDatabaseSymtabIter;
class AtomicCache;
class ShellSymTabBOM;
class SCHScheduleFactory;
class STSymbolTable;
class ESFactory;
class STSymbolTableNode;
class UserType;
class ShellNet;
class VisNetFactory;
class CarbonDatabaseRuntime;
class CarbonDatabaseVisBOM;
class ShellData;
class SourceLocatorFactory;
class CarbonModel;

class CarbonDatabase
{
public:
  CARBONMEM_OVERRIDES
  
  CarbonDatabase();

  virtual ~CarbonDatabase();
  
  CarbonDatabaseNodeIter* loopPrimaryPorts();
  CarbonDatabaseNodeIter* loopPrimaryInputs();
  CarbonDatabaseNodeIter* loopPrimaryOutputs();
  CarbonDatabaseNodeIter* loopPrimaryBidis();
  CarbonDatabaseNodeIter* loopPrimaryClks();
  CarbonDatabaseNodeIter* loopClkTree();
  CarbonDatabaseNodeIter* loopObservable();
  CarbonDatabaseNodeIter* loopDepositable();
  CarbonDatabaseNodeIter* loopScDepositable();
  CarbonDatabaseNodeIter* loopScObservable();
  CarbonDatabaseNodeIter* loopMvDepositable();
  CarbonDatabaseNodeIter* loopMvObservable();
  CarbonDatabaseNodeIter* loopAsyncs();
  CarbonDatabaseNodeIter* loopAsyncOutputs();
  CarbonDatabaseNodeIter* loopAsyncFanin(const CarbonDatabaseNode* node);
  CarbonDatabaseNodeIter* loopAsyncDeposits();
  CarbonDatabaseNodeIter* loopAsyncPosResets();
  CarbonDatabaseNodeIter* loopAsyncNegResets();
  CarbonDatabaseNodeIter* loopDesignRoots();
  CarbonDatabaseNodeIter* loopChildren(const CarbonDatabaseNode* node);
  CarbonDatabaseNodeIter* loopAliases(const CarbonDatabaseNode* node);
  CarbonDatabaseNodeIter* loopPosedgeOnlyTriggers();
  CarbonDatabaseNodeIter* loopNegedgeOnlyTriggers();
  CarbonDatabaseNodeIter* loopPosedgeTriggers();
  CarbonDatabaseNodeIter* loopNegedgeTriggers();
  CarbonDatabaseNodeIter* loopBothEdgeTriggers();
  CarbonDatabaseNodeIter* loopTriggersUsedAsData();
  CarbonDatabaseNodeIter* loopMatching(const char* matchString);

  int isWrapperPort2State(const CarbonDatabaseNode* node) const;
  int isWrapperPort4State(const CarbonDatabaseNode* node) const;
  int isPrimaryInput(const CarbonDatabaseNode* node) const;
  int isPrimaryOutput(const CarbonDatabaseNode* node) const;
  int isPrimaryBidi(const CarbonDatabaseNode* node) const;
  int isInput(const CarbonDatabaseNode* node) const;
  int isOutput(const CarbonDatabaseNode* node) const;
  int isBidi(const CarbonDatabaseNode* node) const;
  int is2DArray(const CarbonDatabaseNode* node) const;
  int isUnpackedArray(const CarbonDatabaseNode* node) const;
  int isVector(const CarbonDatabaseNode* node) const;
  int isScalar(const CarbonDatabaseNode* node) const;
  int isTristate(const CarbonDatabaseNode* node) const;
  int isConstant(const CarbonDatabaseNode* node) const;
  int isClk(const CarbonDatabaseNode* node) const;
  int isClkTree(const CarbonDatabaseNode* node) const;
  int isAsync(const CarbonDatabaseNode* node) const;
  int isAsyncOutput(const CarbonDatabaseNode* node) const;
  int isAsyncDeposit(const CarbonDatabaseNode* node) const;
  int isAsyncPosReset(const CarbonDatabaseNode* node) const;
  int isAsyncNegReset(const CarbonDatabaseNode* node) const;
  int isBranch(const CarbonDatabaseNode* node) const;
  int isLeaf(const CarbonDatabaseNode* node) const;
  int isForcible(const CarbonDatabaseNode* node) const;
  int isDepositable(const CarbonDatabaseNode*) const;
  int isScDepositable(const CarbonDatabaseNode*) const;
  int isMvDepositable(const CarbonDatabaseNode*) const;
  int isObservable(const CarbonDatabaseNode*) const;
  int isScObservable(const CarbonDatabaseNode*) const;
  int isMvObservable(const CarbonDatabaseNode*) const;
  int isVisible(const CarbonDatabaseNode*) const;
  int isSampleScheduled(const CarbonDatabaseNode*) const;
  int isTied(const CarbonDatabaseNode*) const;
  const char* intrinsicType(const CarbonDatabaseNode*) const;
  const char* declarationType(const CarbonDatabaseNode*) const;
  const char* typeLibraryName(const CarbonDatabaseNode*) const;
  const char* typePackageName(const CarbonDatabaseNode*) const;
  const char* componentName(const CarbonDatabaseNode* node) const;
  const char* sourceLanguage(const CarbonDatabaseNode* node) const;
  const char* getPullMode(const CarbonDatabaseNode* node) const;
  const char* getType() const;
  int getRangeConstraintLeftBound(const CarbonDatabaseNode* node) const;
  int getRangeConstraintRightBound(const CarbonDatabaseNode* node) const;
  int isRangeRequiredInDeclaration(const CarbonDatabaseNode* node) const;
  int isTemp(const CarbonDatabaseNode* node) const;

  int getWidth(const CarbonDatabaseNode* node) const;
  int getMSB(const CarbonDatabaseNode* node) const;
  int getLSB(const CarbonDatabaseNode* node) const;
  int get2DArrayLeftAddr(const CarbonDatabaseNode* node) const;
  int get2DArrayRightAddr(const CarbonDatabaseNode* node) const;
  int isPosedgeTrigger(const CarbonDatabaseNode* node) const;
  int isNegedgeTrigger(const CarbonDatabaseNode* node) const;
  int isBothEdgeTrigger(const CarbonDatabaseNode* node) const;
  int isTriggerUsedAsData(const CarbonDatabaseNode* node) const;
  int isLiveInput(const CarbonDatabaseNode* node) const;
  int isLiveOutput(const CarbonDatabaseNode* node) const;
  int isOnDemandIdleDeposit(const CarbonDatabaseNode* node) const;
  int isOnDemandExcluded(const CarbonDatabaseNode* node) const;

  const char* getInterfaceName() const;
  const char* getTopLevelModuleName() const;
  const char* getSystemCModuleName() const;
  const char* getSoftwareVersion() const;
  const char* getIdString() const;
  int isReplayable() const;
  int supportsOnDemand() const;
  
  IODBRuntime* getDB();
  const IODBRuntime* getDB() const;

  const char* getFullName(const CarbonDatabaseNode* node);
  const char* getLeafName(const CarbonDatabaseNode* node);

  const CarbonDatabaseNode* findNode(const char* pathName, bool warnNotFound);
  const CarbonDatabaseNode* findChild(const CarbonDatabaseNode*,
                                     const char* childName);
  const CarbonDatabaseNode* getParent(const CarbonDatabaseNode*) const;
  const CarbonDatabaseNode* getMaster(const CarbonDatabaseNode*);
  const CarbonDatabaseNode* getStorage(const CarbonDatabaseNode*);

  //! Is this node a structure?
  int isStruct(const CarbonDatabaseNode* node) const;
  //! Is this node an array?
  int isArray(const CarbonDatabaseNode* node) const;
  //! Is this node an enum?
  int isEnum(const CarbonDatabaseNode* node) const;
  //! Returns the number of elements in the enumeration
  int getNumberElemInEnum(const CarbonDatabaseNode* node) const;
  //! Returns element of enumeration, selected by index
  const char* getEnumElem(const CarbonDatabaseNode* node, int index) const;
  //! Returns the number of fields in the structure
  int getNumStructFields(const CarbonDatabaseNode* node) const;
  //! Get a structure field by name
  const CarbonDatabaseNode* getStructFieldByName(const CarbonDatabaseNode* node, const char* name);

  //! Returns the left bound of an array
  int getArrayLeftBound(const CarbonDatabaseNode* node) const;
  //! Returns the right bound of an array
  int getArrayRightBound(const CarbonDatabaseNode* node) const;
  //! Returns the number of dimensions in an array
  int getArrayDims(const CarbonDatabaseNode* node) const;

  //! Returns the left bound of an array for dimension dim
  int getArrayDimLeftBound(const CarbonDatabaseNode* node, int dim) const;
  //! Returns the right bound of an array for dimension dim
  int getArrayDimRightBound(const CarbonDatabaseNode* node, int dim) const;
  //! Returns the number of dimensions in an array as it's declared
  int getArrayNumDeclaredDims(const CarbonDatabaseNode* node) const;


  //! Get an element of an array
  const CarbonDatabaseNode* getArrayElement(const CarbonDatabaseNode* node, const int* indices, int dims);

  //! Can this node be treated as a CarbonNet?
  int canBeCarbonNet(const CarbonDatabaseNode* node) const;
  //! Get the aggregate bit size of this node
  int getBitSize(const CarbonDatabaseNode* node) const;

  const char* getSourceFile(const CarbonDatabaseNode* node) const;
  int getSourceLine(const CarbonDatabaseNode* node) const;

  //! Get the value of a string attribute, if it exists
  const char* getStringAttribute(const char* attributeName) const;
  //! Get the value of an integer attribute, returning success
  CarbonStatus getIntAttribute(const char* attributeName, UInt32* attributeValue) const;


  AtomicCache* getAtomicCache() const {return mAtomicCache;}

  //! Elaborate all the node's children in the visibility database
  /*!
    For example: Let top.s1 be array(1 downto 0) of record with one field f1.

    Then this method enrolls the following nodes in the visibility database:
    
    top.s1[0].f1
    top.s1[1].f1
  */
  void elabDBNode(CarbonDatabaseNode* node);

  //! Gets the true type of a node, considering array dimensions when necessary
  //  Static because it's called by sIsVectorOrScalar
  static const UserType* getTrueType(const CarbonDatabaseNode* node);

  // Create/return the corresponding node in the visibility database, if possible
  CarbonDatabaseNode* translateToDB(const STSymbolTableNode* node);

  // Populate all possible instance of a symtab node, optionally adding them to an array
  void populateAllInstances(const STSymbolTableNode* stNode, CarbonDatabaseNodeIter::NodeArray* nodeArray);

  //! Get the message context
  virtual MsgContext* getMsgContext() const = 0;

  //! Is this a runtime database?
  virtual CarbonDatabaseRuntime* castRuntime() = 0;
  //! count the number of packed and unpacked dimensions, and the number of bits in the packed range(s), any of the three return args can be null indicating they are not needed
  static void sGetPackedUnpackedDimensionCount(const CarbonDatabaseNode* node, UInt32* nPacked, UInt32* nUnpacked, UInt32* packedWidth);

protected:
  UtString mReturnBuffer;                               //! Buffer used for composing node names
  AtomicCache* mAtomicCache;                            //! The IODB's atomic cache
  STSymbolTable* mDesignSymTab;                         //! The design symbol table from the IODB
  IODBRuntime* mDB;                                     //! The IODB for the design
  CarbonDatabaseNodeFactory* mDBNodeFactory;            //! Factory object for database nodes

  // helper class - a place to hold other data structures needed by
  // the database object.
  struct Aux;
  Aux* mAux;

  //! Local string to be used as storage for looking up string attributes
  /*!
    Looking up a string attribute is conceptually a const operation,
    but requires this temp storage to be modified.
  */
  mutable UtString mStringAttributeValue;

  //! Returns whether the node can be converted to a database node
  bool canBeDBNode(const STSymbolTableNode* node) const;
  //! Finds a child of a node by name
  /*!
    This populates all possible children first, so it's potentially expensive!
   */
  const STSymbolTableNode* findChild(const STSymbolTableNode* node, const char* childName) const;

  //! Common initialization functions
  void init();
  //! Populates all the children of a database node
  void populateChildren(CarbonDatabaseNode* node);
  //! Returns whether a user type represents a scalar or a supported vector type
  static bool sIsLegalVectorOrScalar(const CarbonDatabaseNode* node);
  //! Returns whether a node cannot have a representative ShellNet
  bool isInvalidNetNode(const CarbonDatabaseNode* node, bool forCarbonMemory) const;

  //! Extracts array indices from a token
  bool extractIndices(UtString* token, UtArray<SInt32>* indices) const;
  //! Determines the upper and lower bounds of an array
  void getUpperLowerBounds(const CarbonDatabaseNode* node, SInt32* upper, SInt32* lower) const;

  //! Create a loop over all DB nodes corresponding to a symtab node loop
  CarbonDatabaseNodeIter* createLoop(CarbonDatabaseSymtabIter* symtabIter);
};

//! Database opened from a file
/*!
  A database opened from a file has its own IODB, AtomicCache,
  ExprFactory, etc.  The design hierarchy can be traversed and queried
  through the API, but CarbonNetIDs cannot be created for design nodes
  because there is no Carbon model.
 */
class CarbonDatabaseStandalone : public CarbonDatabase
{
public:
  CARBONMEM_OVERRIDES

  CarbonDatabaseStandalone();
  virtual ~CarbonDatabaseStandalone();

  //! Open the database, checkout vhm licenses
  bool open(const char* fileName, int waitForLicense);
  //! Same as open, but checkout a soc vsp license instead
  bool openSocVsp(SInt32 key, const char* fileName, int waitForLicense);
  //! Same as open, but checkout a platarch license instead
  bool openPlatArch(SInt32 key, const char* fileName, int waitForLicense);
  // ! open a database w/o a license
  bool openDBFile(const char* fileName, SourceLocatorFactory* factory=NULL);

  //! Get the message context
  virtual MsgContext* getMsgContext() const;

  //! Returns NULL
  virtual CarbonDatabaseRuntime* castRuntime();

private:
  ShellSymTabBOM* mShellSymTabBOM;
  SCHScheduleFactory* mScheduleFactory;
  SourceLocatorFactory* mSourceLocatorFactory;
  ESFactory* mExprFactory;

  // Inline this to remove the function from the link, so licensing
  // cannot be bypassed.
  inline void setupLicensing(bool waitForLicense);
};

//! Database extracted from a Carbon model
/*!
  A database extracted from a Carbon model uses the IODB, AtomicCache,
  etc. from the model.  In addition to traversing and querying the
  design hierarchy through the API, CarbonNetIDs can be created for
  design nodes representing the corresponding net instance in the
  model.
 */
class CarbonDatabaseRuntime : public CarbonDatabase
{
public:
  CARBONMEM_OVERRIDES

  CarbonDatabaseRuntime(CarbonModel* model);
  virtual ~CarbonDatabaseRuntime();

  //! Get the message context
  virtual MsgContext* getMsgContext() const;

  //! Returns this
  virtual CarbonDatabaseRuntime* castRuntime();

  //! Lookup a ShellNet for a symtab node
  ShellNet* getCarbonNet(const CarbonDatabaseNode* node, bool forCarbonMemory, bool forWaveDump);

  //! Examines an array node to a file
  CarbonStatus examineArrayToFile(const CarbonDatabaseNode* node, CarbonRadix format, const char* filename);

  //! Deposits an array node from a file
  CarbonStatus depositArrayFromFile(const CarbonDatabaseNode* node, CarbonRadix format, const char* filename);

private:
  CarbonModel *mModel;                  //! The model corresponding to this database
  VisNetFactory *mNetFactory;           //! The factory for creating visibility nets

  typedef UtArray<const ConstantRange*> RangeArray;

  //! Class to iterate over all the possible array indices of a multi-dimensional array
  class ArrayWalker
  {
  public:
    CARBONMEM_OVERRIDES

    ArrayWalker(const RangeArray& ranges);
    ~ArrayWalker();

    bool next();
    SInt32* getArray() const { return mArray; }
    UInt32 getSize() const { return mSize; }

  protected:
    bool mInitialized;
    SInt32 *mArray;
    UInt32 mSize;
    const RangeArray &mRanges;
  };

  //! Class to determine parameters related to examining/depositing an array to/from a file
  class ArrayFileInfo
  {
  public:
    CARBONMEM_OVERRIDES

    ArrayFileInfo(CarbonDatabase *db, const CarbonDatabaseNode* dbNode);
    ~ArrayFileInfo();

    //! Examine the node and populate the members
    /*!
      This is done as a separate function, instead of in the
      constructor, so it's easier to set a breakpoint.
     */
    bool examine();

    const CarbonDatabaseNode* getDBNode() const { return mDBNode; }
    bool isValid() const { return mValid; }
    const UserType* getElemType() const { return mElemType; }
    const RangeArray& getArrayRanges() const { return mArrayRanges; }
    UInt32 getRowWidth() const { return mRowWidth; }
    NetFlags getNetFlags() const { return mNetFlags; }

  private:
    CarbonDatabase * const mDB;
    const CarbonDatabaseNode * const mDBNode;
    bool mValid;
    const UserType *mElemType;
    RangeArray mArrayRanges;
    UInt32 mRowWidth;
    NetFlags mNetFlags;
  };
};

#endif
