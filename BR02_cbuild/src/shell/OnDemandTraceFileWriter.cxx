// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/UtIStream.h"
#include "shell/carbon_capi.h"
#include "util/UtIOStream.h"
#include "util/CarbonAssert.h"
#include "shell/OnDemandTraceFileWriter.h"

class ModelData {
public:
  CARBONMEM_OVERRIDES

  ModelData(const char *name, CarbonObjectID *model,
            OnDemandTraceFileWriter* writer)
    : mName(name), mModel(model), mEnabled(false), mDebugCB(NULL),
      mModeChangeCB(NULL), mWriter(writer)
  {
  }

  void enable() {
    if (!mEnabled) {
      mEnabled = true;
      carbonOnDemandSetDebugLevel(mModel, eCarbonOnDemandDebugAll);
      carbonOnDemandEnableStats(mModel);
      if (mDebugCB == NULL) {
        mDebugCB = carbonOnDemandAddDebugCB(mModel, debug_callback, this);
      } else {
        carbonOnDemandEnableCB(mModel, mDebugCB);
      }
      if (mModeChangeCB == NULL) {
        mModeChangeCB = carbonOnDemandAddModeChangeCB(mModel, mode_change_callback, this);
      } else {
        carbonOnDemandEnableCB(mModel, mModeChangeCB);
      }
    }
  }

  void disable() {
    carbonOnDemandSetDebugLevel(mModel, eCarbonOnDemandDebugNone);
    if (mDebugCB != NULL) {
      carbonOnDemandDisableCB(mModel, mDebugCB);
    }
    if (mModeChangeCB != NULL) {
      carbonOnDemandDisableCB(mModel, mModeChangeCB);
    }
    // do not disable stats, as there is no API for that
  }

  CarbonObjectID* getModel() const { return mModel; }

private:
  static void debug_callback(CarbonObjectID* context,
                             void* userContext,
                             CarbonTime simTime,
                             CarbonOnDemandDebugType type,
                             CarbonOnDemandDebugAction action,
                             const char *path,
                             CarbonUInt32 length) {

    (void) context;
    
    // Restore events are not particularly interesting, since they
    // don't cause an idle state to be exited
    if (type != eCarbonOnDemandDebugRestore) {
      ModelData *data = (ModelData *) userContext;
      // The schedule count is also needed
      UInt64 schedCalls = carbonGetTotalNumberOfScheduleCalls(data->mModel);
      UtString buf;
      UtOStringStream os(&buf);
      os << UtIO::slashify << OnDemandTraceFileWriter::scDebugEvent << ' '
         << schedCalls << ((UInt64) simTime) << type << action << length << path << '\n';
      data->mWriter->append(buf.c_str());
    }
  }

  static void mode_change_callback(CarbonObjectID* context,
                                   void* userContext,
                                   CarbonOnDemandMode from,
                                   CarbonOnDemandMode to,
                                   CarbonTime simTime)
  {
    (void) context;

    // See where we're going
    char eventCode = OnDemandTraceFileWriter::scNoEvent;
    switch (to) {
    case eCarbonOnDemandLooking:
      eventCode = OnDemandTraceFileWriter::scLooking;
      break;
    case eCarbonOnDemandIdle:
      eventCode = OnDemandTraceFileWriter::scIdle;
      break;
    case eCarbonOnDemandBackoff:
    case eCarbonOnDemandStopped:
      // Only care if we came here from looking/idle
      if ((from == eCarbonOnDemandLooking) || (from == eCarbonOnDemandIdle)) {
        eventCode = OnDemandTraceFileWriter::scNotLooking;
      }
    }
    if (eventCode != OnDemandTraceFileWriter::scNoEvent) {
      ModelData *data = (ModelData *) userContext;
      UInt64 schedCalls = carbonGetTotalNumberOfScheduleCalls(data->mModel);
      UtString buf;
      UtOStringStream os(&buf);
      os << UtIO::slashify << eventCode << ' ' << schedCalls
         << ((UInt64) simTime) << '\n';
      data->mWriter->append(buf.c_str());
    }
  }

  const char     *mName;
  CarbonObjectID *mModel;
  bool            mEnabled;
  CarbonOnDemandCBDataID *mDebugCB;
  CarbonOnDemandCBDataID *mModeChangeCB;
  OnDemandTraceFileWriter* mWriter;
};


OnDemandTraceFileWriter::OnDemandTraceFileWriter(const char *fname)
  :mFileName(fname), mEnabled(false), mModelData(NULL), mSimStartTimestamp(0)
{
  // Empty out the file.  We will not leave it open, but re-open it
  // for append each time we wish to flush.
  UtOBStream f(fname);
  // I guess we are ignoring errors for now.

  // Constructing this object creates an empty file, and letting the
  // object fall out of scope closes it, leaving it empty, which is
  // what we want.
}


OnDemandTraceFileWriter::~OnDemandTraceFileWriter()
{
  if (mModelData != NULL) {
    mModelData->disable();
  }

  writeEndOfData();

  delete mModelData;
}

void OnDemandTraceFileWriter::addModel(const char *name, CarbonObjectID *model)
{
  // Only one model allowed
  INFO_ASSERT(mModelData == NULL, "Trace file writer already has a model registered");
  mModelData = new ModelData(name, model, this);
  if (mEnabled) {
    mModelData->enable();
  }
}

bool OnDemandTraceFileWriter::enable()
{
  bool success = true;

  if (!mEnabled) {
    if (mModelData != NULL) {
      mModelData->enable();
    }
    mEnabled = true;
  }
  mEnabled = success;
  return success;
}

void OnDemandTraceFileWriter::disable()
{
  if (mEnabled) {
    writeEndOfData(); // causes graph to be re-zoomed to 100% on read
    mEnabled = false;

    // disable the model
    if (mModelData != NULL) {
      mModelData->disable();
    }
  }
}

void OnDemandTraceFileWriter::flush() {
  if (!mBuffer.empty()) {
    UtOBStream file(mFileName.c_str(), "a");
    if (file.is_open()) {
      if (mSimStartTimestamp != 0) {
        file << scTimestamp << ' ' << mSimStartTimestamp << '\n';
      }
      file << mBuffer;
      mBuffer.clear();
    }
    // i guess we will ignore errors on close
  }
}

void OnDemandTraceFileWriter::append(const char* buf) {
  mBuffer << buf;

  // Flush every once in a while
  if (mBuffer.size() > 10000) {
    flush();
  }
}

void OnDemandTraceFileWriter::putSimStartTimestamp(UInt64 ts) {
  if (ts != mSimStartTimestamp) {
    mSimStartTimestamp = ts;
    if (ts == 0) {
      writeEndOfData();
    }
    else {
      mBuffer << scTimestamp << ' ' << ts << '\n';
    }
  }
}

void OnDemandTraceFileWriter::writeEndOfData()
{
  // Determine the current simulation time and schedule call count.
  // This is recorded because the model might be idle, in which case
  // the last event was a long time ago, and we want the timeline on
  // the graph to be correct.
  CarbonObjectID *obj = mModelData->getModel();
  UInt64 schedCalls = carbonGetTotalNumberOfScheduleCalls(obj);
  UInt64 simTime = carbonGetSimulationTime(obj);

  UtOStringStream os(&mBuffer);
  os << UtIO::slashify << scEndOfData << ' ' << schedCalls << simTime << '\n';
  flush();
}
