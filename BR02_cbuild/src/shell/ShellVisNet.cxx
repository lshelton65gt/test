// -*-c++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/ShellVisNet.h"
#include "shell/CarbonNetValueCBData.h"
#include "CarbonDatabaseNode.h"
#include "symtab/STSymbolTable.h"
#include "util/UtConv.h"

class ShellVisNet::StorageOp
{
public:
  CARBONMEM_OVERRIDES

  StorageOp() {}
  virtual ~StorageOp() {}

  //! Allocate shadow storage for the net's value
  virtual Storage allocShadow(UInt32 numWords) const = 0;
  //! Free allocated shadow storage
  virtual void freeShadow(Storage* shadow) = 0;
  //! Update the shadow storage with the net's resolved value
  virtual void update(Storage* shadow) const = 0;
  //! Update the shadow storage with the net's unresolved (i.e. idrive only) value
  virtual void updateUnresolved(Storage* shadow) const = 0;
  //! Compare shadow storage with the net's current value
  virtual ValueState compare(const Storage shadow, UInt32* valBuf, UInt32* driveBuf, UInt32 numWords) const = 0;
  //! Compare shadow storage with the net's current value (value only, not drive)
  virtual ValueState compareNoDrive(const Storage shadow, UInt32* valBuf, UInt32 numWords) const = 0;
  //! Compare and updates the shadow value to the current value/drive unresolved
  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* valBuf, UInt32* driveBuf) const = 0;
  //! Get the net's current value and run the value change callback
  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, UInt32* valBuf, UInt32* driveBuf,
                                CarbonTriValShadow* fullShadow, CarbonModel* model) const = 0;
  //! Get the POD pointer to the net's storage location, if valid
  virtual const void* getStoragePtr() const = 0;
};

//! Storage operator for non-PODs
/*!
  Non-POD nets don't have a simple mapping to their storage location
  in the model.  This could be because they implement a bitselect, are
  part of a sparse memory, or are wider than 64 bits.

  For these, all storage operations use the generic CarbonTriValShadow
  class, which allocates an array of UInt32s for value and drive
  buffers.
 */
class ShellVisNet::StorageOpNonPOD : public ShellVisNet::StorageOp
{
public:
  CARBONMEM_OVERRIDES

  StorageOpNonPOD(const ShellVisNet* net) : mNet(net) {}
  virtual ~StorageOpNonPOD() {}

  virtual Storage allocShadow(UInt32 numWords) const
  {
    Storage shadow = static_cast<Storage>(new CarbonTriValShadow(numWords));
    update(&shadow);
    return shadow;
  }

  virtual void freeShadow(Storage* shadow)
  {
    CarbonTriValShadow* typedShadow = static_cast<CarbonTriValShadow*>(*shadow);
    delete typedShadow;
    *shadow = NULL;
  }

  virtual void update(Storage* shadow) const
  {
    CarbonTriValShadow* typedShadow = static_cast<CarbonTriValShadow*>(*shadow);
    mNet->examine(typedShadow->mValue, typedShadow->mDrive, eCalcDrive, NULL);
  }

  virtual void updateUnresolved(Storage* shadow) const
  {
    CarbonTriValShadow* typedShadow = static_cast<CarbonTriValShadow*>(*shadow);
    mNet->examine(typedShadow->mValue, typedShadow->mDrive, eIDrive, NULL);
  }

  virtual ValueState compare(const Storage shadow, UInt32* valBuf, UInt32* driveBuf, UInt32 numWords) const
  {
    CarbonTriValShadow* typedShadow = static_cast<CarbonTriValShadow*>(shadow);
    mNet->examine(valBuf, driveBuf, eCalcDrive, NULL);
    ShellNet::ValueState stat = eUnchanged;
    if (CarbonValRW::memCompare(typedShadow->mValue, valBuf, numWords) != 0) {
      stat = eChanged;
    }
    if (CarbonValRW::memCompare(typedShadow->mDrive, driveBuf, numWords) != 0) {
      stat = eChanged;
    }
    return stat;
  }

  virtual ValueState compareNoDrive(const Storage shadow, UInt32* valBuf, UInt32 numWords) const
  {
    CarbonTriValShadow* typedShadow = static_cast<CarbonTriValShadow*>(shadow);
    mNet->examine(valBuf, NULL, eIDrive, NULL);
    ShellNet::ValueState stat = eUnchanged;
    if (CarbonValRW::memCompare(typedShadow->mValue, valBuf, numWords) != 0) {
      stat = eChanged;
    }
    return stat;
  }

  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* valBuf, UInt32* driveBuf) const
  {
    // This is only ever used by ShellVisNetBitsel, so the value is always a single bit
    CarbonTriValShadow* typedShadow = static_cast<CarbonTriValShadow*>(*shadow);
    UInt32 currVal, currDrive;
    mNet->examine(&currVal, &currDrive, eIDrive, NULL);
    ShellNet::ValueState stat = eUnchanged;
    if ((currVal != *typedShadow->mValue) || (currDrive != *typedShadow->mDrive)) {
      stat = eChanged;
      *typedShadow->mValue = currVal;
      *typedShadow->mDrive = currDrive;
      *valBuf = currVal;
      *driveBuf = currDrive;
    }
    return stat;
  }

  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, UInt32* valBuf, UInt32* driveBuf,
                                CarbonTriValShadow* fullShadow, CarbonModel* model) const
  {
    // Just like CarbonExprNet, do a compare/update using the calculated
    // drive, then run the callback with the idrive.
    //
    // compare(), update(), and examine() are all const, but
    // compareUpdateExamine() isn't?  Ugh.
    if (mNet->compare(fullShadow) == eChanged) {
      ShellNet::Storage storage = static_cast<ShellNet::Storage>(fullShadow);
      update(&storage);
      mNet->examine(valBuf, driveBuf, eIDrive, model);
      cbData->executeCB(model->getObjectID (), valBuf, driveBuf);
    }
  }

  virtual const void* getStoragePtr() const
  {
    // We don't have a simple storage pointer
    return NULL;
  }

private:
  const ShellVisNet* mNet;
};


//! Storage operator for PODs
/*!
  POD nets have a simple POD pointer to their model storage.  For
  these, all storage operations use a single allocated element of that
  POD.
 */
template<typename POD>
class ShellVisNet::StorageOpPOD : public ShellVisNet::StorageOp
{
public:
  CARBONMEM_OVERRIDES

  StorageOpPOD(const void* storage) : mStorage(static_cast<const POD*>(storage)) {}
  virtual ~StorageOpPOD() {}

  virtual Storage allocShadow(UInt32) const
  {
    Storage shadow = static_cast<Storage>(CARBON_ALLOC_VEC(POD, 1));
    update(&shadow);
    return shadow;
  }

  virtual void freeShadow(Storage* shadow)
  {
    POD* typedShadow = static_cast<POD*>(*shadow);
    CARBON_FREE_VEC(typedShadow, POD, 1);
    *shadow = NULL;
  }

  virtual void update(Storage* shadow) const
  {
    POD* typedShadow = static_cast<POD*>(*shadow);
    *typedShadow = *mStorage;
  }

  virtual void updateUnresolved(Storage* shadow) const
  {
    // PODs are never tristates, so this is the same as update()
    POD* typedShadow = static_cast<POD*>(*shadow);
    *typedShadow = *mStorage;
  }

  virtual ValueState compare(const Storage shadow, UInt32* valBuf, UInt32*, UInt32 numWords) const
  {
    // PODs are never tristates, so this is the same as compareNoDrive
    return compareNoDrive(shadow, valBuf, numWords);
  }

  virtual ValueState compareNoDrive(const Storage shadow, UInt32*, UInt32) const
  {
    POD* typedShadow = static_cast<POD*>(shadow);
    ShellNet::ValueState stat = eUnchanged;
    if (*typedShadow != *mStorage) {
      stat = eChanged;
    }
    return stat;
  }

  virtual ValueState compareUpdateExamineUnresolved(Storage*, UInt32*, UInt32*) const
  {
    // This is only ever used by ShellVisNetBitsel, which can't have this type of storage op
    INFO_ASSERT(0, "compareUpdateExamineUnresolved invalid for StorageOpPOD");
    return eChanged;
  }

  virtual void runValueChangeCB(CarbonNetValueCBData*, UInt32*, UInt32*, CarbonTriValShadow*, CarbonModel*) const
  {
    // This should never be called, as it's only used for non-PODs
    INFO_ASSERT(0, "runValueChangeCB called for non-POD");
  }

  virtual const void* getStoragePtr() const
  {
    const void* storage = static_cast<const void*>(mStorage);
    return storage;
  }

private:
  const POD* mStorage;
};

//! Storage operator for POD arrays
/*!
  This is used for bitvectors, which store their values in an array of
  UInt32s.
 */
class ShellVisNet::StorageOpPODArray : public ShellVisNet::StorageOp
{
public:
  CARBONMEM_OVERRIDES

  StorageOpPODArray(const void* storage, UInt32 numWords) : mStorage(static_cast<const UInt32*>(storage)), mNumWords(numWords) {}
  virtual ~StorageOpPODArray() {}

  virtual Storage allocShadow(UInt32) const
  {
    Storage shadow = static_cast<Storage>(CARBON_ALLOC_VEC(UInt32, mNumWords));
    update(&shadow);
    return shadow;
  }

  virtual void freeShadow(Storage* shadow)
  {
    UInt32* typedShadow = static_cast<UInt32*>(*shadow);
    CARBON_FREE_VEC(typedShadow, UInt32, mNumWords);
    *shadow = NULL;
  }

  virtual void update(Storage* shadow) const
  {
    UInt32* typedShadow = static_cast<UInt32*>(*shadow);
    for (UInt32 i = 0; i < mNumWords; ++i) {
      typedShadow[i] = mStorage[i];
    }
  }

  virtual void updateUnresolved(Storage* shadow) const
  {
    // PODs are never tristates, so this is the same as update()
    UInt32* typedShadow = static_cast<UInt32*>(*shadow);
    for (UInt32 i = 0; i < mNumWords; ++i) {
      typedShadow[i] = mStorage[i];
    }
  }

  virtual ValueState compare(const Storage shadow, UInt32* valBuf, UInt32*, UInt32 numWords) const
  {
    // PODs are never tristates, so this is the same as compareNoDrive
    return compareNoDrive(shadow, valBuf, numWords);
  }

  virtual ValueState compareNoDrive(const Storage shadow, UInt32*, UInt32) const
  {
    UInt32* typedShadow = static_cast<UInt32*>(shadow);
    ShellNet::ValueState stat = eUnchanged;
    for (UInt32 i = 0; (stat == eUnchanged) && (i < mNumWords); ++i) {
      if (typedShadow[i] != mStorage[i]) {
        stat = eChanged;
      }
    }
    return stat;
  }

  virtual ValueState compareUpdateExamineUnresolved(Storage*, UInt32*, UInt32*) const
  {
    // This is only ever used by ShellVisNetBitsel, which can't have this type of storage op
    INFO_ASSERT(0, "compareUpdateExamineUnresolved invalid for StorageOpPODArray");
    return eChanged;
  }

  virtual void runValueChangeCB(CarbonNetValueCBData*, UInt32*, UInt32*, CarbonTriValShadow*, CarbonModel*) const
  {
    // This should never be called, as it's only used for non-PODs
    INFO_ASSERT(0, "runValueChangeCB called for non-POD");
  }

  virtual const void* getStoragePtr() const
  {
    const void* storage = static_cast<const void*>(mStorage);
    return storage;
  }

private:
  const UInt32* mStorage;
  const UInt32 mNumWords;
};

//! Storage operator for bitsel of PODs
/*!
  The perceived value here is only a bit, so the storage is a UInt8,
  but the model value is a bitselect of a POD.
 */
template<typename POD>
class ShellVisNet::StorageOpPODBitsel : public ShellVisNet::StorageOp
{
public:
  CARBONMEM_OVERRIDES

  StorageOpPODBitsel(const ShellVisNet* net, const void* storage, UInt32 bitsel)
    : mNet(net), mStorage(static_cast<const POD*>(storage)), mBitsel(bitsel) {}
  virtual ~StorageOpPODBitsel() {}

  virtual Storage allocShadow(UInt32) const
  {
    Storage shadow = static_cast<Storage>(CARBON_ALLOC_VEC(UInt8, 1));
    update(&shadow);
    return shadow;
  }

  virtual void freeShadow(Storage* shadow)
  {
    UInt8* typedShadow = static_cast<UInt8*>(*shadow);
    CARBON_FREE_VEC(typedShadow, UInt8, 1);
    *shadow = NULL;
  }

  virtual void update(Storage* shadow) const
  {
    UInt8* typedShadow = static_cast<UInt8*>(*shadow);
    // Set the shadow to 1 if our bit in the storage location is set
    *typedShadow = (static_cast<UInt8>(*mStorage >> mBitsel) & 0x1) != 0;
  }

  virtual void updateUnresolved(Storage* shadow) const
  {
    // PODs are never tristates, so this is the same as update()
    UInt8* typedShadow = static_cast<UInt8*>(*shadow);
    // Set the shadow to 1 if our bit in the storage location is set
    *typedShadow = (static_cast<UInt8>(*mStorage >> mBitsel) & 0x1) != 0;
  }

  virtual ValueState compare(const Storage shadow, UInt32* valBuf, UInt32*, UInt32 numWords) const
  {
    // PODs are never tristates, so this is the same as compareNoDrive
    return compareNoDrive(shadow, valBuf, numWords);
  }

  virtual ValueState compareNoDrive(const Storage shadow, UInt32*, UInt32) const
  {
    UInt8* typedShadow = static_cast<UInt8*>(shadow);
    ShellNet::ValueState stat = eUnchanged;
    // Compare with our bit in the storage location
    if (*typedShadow != (static_cast<UInt8>(*mStorage >> mBitsel) & 0x1)) {
      stat = eChanged;
    }
    return stat;
  }

  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* valBuf, UInt32* driveBuf) const
  {
    // This can't be a tristate, so there's no need to consider the drive
    ValueState stat = compareNoDrive(*shadow, valBuf, 0);
    if (stat == eChanged) {
      updateUnresolved(shadow);
      // Update the value buffer
      if (valBuf) {
        UInt8* typedShadow = static_cast<UInt8*>(*shadow);
        *valBuf = *typedShadow;
      }
      // Drive is always 0 for non-tristates
      if (driveBuf) {
        *driveBuf = 0;
      }
    }
    return stat;
  }

  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, UInt32* valBuf, UInt32* driveBuf,
                                CarbonTriValShadow* fullShadow, CarbonModel* model) const
  {
    // This is only ever called in playback mode, so we can't go
    // directly to the storage pointer.  We have to go through the
    // ShellNet.
    UInt32 val;
    mNet->examine(&val, NULL, eIDrive, model);
    // We only use the value portion of the shadow.
    if (val != *(fullShadow->mValue)) {
      *(fullShadow->mValue) = val;
      *valBuf = val;
      *driveBuf = 0;
      cbData->executeCB(model->getObjectID (), valBuf, driveBuf);
    }
  }

  virtual const void* getStoragePtr() const
  {
    const void* storage = static_cast<const void*>(mStorage);
    return storage;
  }

private:
  const ShellVisNet* mNet;      //!< Needed for running callbacks
  const POD* mStorage;          //!< Pointer to the whole POD storage location
  const UInt32 mBitsel;         //!< Bitselect of the POD corresponding to this net
};

//! Storage operator for bitsel of POD arrays
/*!
  The perceived value here is only a bit, so the storage is a UInt8,
  but the model value is a bitselect of an array of UInt32s
  representing a bitvector.
 */
class ShellVisNet::StorageOpPODArrayBitsel : public ShellVisNet::StorageOp
{
public:
  CARBONMEM_OVERRIDES

  StorageOpPODArrayBitsel(const ShellVisNet* net, const void* storage, UInt32 numWords, UInt32 bitsel)
    : mNet(net), mStorage(static_cast<const UInt32*>(storage)), mNumWords(numWords),
      mWord(bitsel / 32), mBit(bitsel % 32)
  {
    INFO_ASSERT(mWord < mNumWords, "StorageOpPODArrayBitsel word out of range");
  }
  virtual ~StorageOpPODArrayBitsel() {}

  virtual Storage allocShadow(UInt32) const
  {
    Storage shadow = static_cast<Storage>(CARBON_ALLOC_VEC(UInt8, 1));
    update(&shadow);
    return shadow;
  }

  virtual void freeShadow(Storage* shadow)
  {
    UInt8* typedShadow = static_cast<UInt8*>(*shadow);
    CARBON_FREE_VEC(typedShadow, UInt8, 1);
  }

  virtual void update(Storage* shadow) const
  {
    UInt8* typedShadow = static_cast<UInt8*>(*shadow);
    // Set the shadow to 1 if our bit in the storage location is set
    *typedShadow = (static_cast<UInt8>(mStorage[mWord] >> mBit) & 0x1) != 0;
  }

  virtual void updateUnresolved(Storage* shadow) const
  {
    // PODs are never tristates, so this is the same as update()
    UInt8* typedShadow = static_cast<UInt8*>(*shadow);
    // Set the shadow to 1 if our bit in the storage location is set
    *typedShadow = (static_cast<UInt8>(mStorage[mWord] >> mBit) & 0x1) != 0;
  }

  virtual ValueState compare(const Storage shadow, UInt32* valBuf, UInt32*, UInt32 numWords) const
  {
    // PODs are never tristates, so this is the same as compareNoDrive
    return compareNoDrive(shadow, valBuf, numWords);
  }

  virtual ValueState compareNoDrive(const Storage shadow, UInt32*, UInt32) const
  {
    UInt8* typedShadow = static_cast<UInt8*>(shadow);
    ShellNet::ValueState stat = eUnchanged;
    // Compare with our bit in the storage location
    if (*typedShadow != (static_cast<UInt8>(mStorage[mWord] >> mBit) & 0x1)) {
      stat = eChanged;
    }
    return stat;
  }

  virtual ValueState compareUpdateExamineUnresolved(Storage* shadow, UInt32* valBuf, UInt32* driveBuf) const
  {
    // This can't be a tristate, so there's no need to consider the drive
    ValueState stat = compareNoDrive(*shadow, valBuf, 0);
    if (stat == eChanged) {
      updateUnresolved(shadow);
      // Update the value buffer
      if (valBuf) {
        UInt8* typedShadow = static_cast<UInt8*>(*shadow);
        *valBuf = *typedShadow;
      }
      // Drive is always 0 for non-tristates
      if (driveBuf) {
        *driveBuf = 0;
      }
    }
    return stat;
  }

  virtual void runValueChangeCB(CarbonNetValueCBData* cbData, UInt32* valBuf, UInt32* driveBuf,
                                CarbonTriValShadow* fullShadow, CarbonModel* model) const
  {
    // This is only ever called in playback mode, so we can't go
    // directly to the storage pointer.  We have to go through the
    // ShellNet.
    UInt32 val;
    mNet->examine(&val, NULL, eIDrive, model);
    // We only use the value portion of the shadow.
    if (val != *(fullShadow->mValue)) {
      *(fullShadow->mValue) = val;
      *valBuf = val;
      *driveBuf = 0;
      cbData->executeCB(model->getObjectID (), valBuf, driveBuf);
    }
  }

  virtual const void* getStoragePtr() const
  {
    const void* storage = static_cast<const void*>(mStorage);
    return storage;
  }

private:
  const ShellVisNet* mNet;      // Needed for running callbacks
  const UInt32* mStorage;       //!< Pointer to the whole UInt32 array for the bitvector
  const UInt32 mNumWords;       //!< Number of UInt32s in the bitvector
  const UInt32 mWord;           //!< Word of the bitvector corresponding to this net
  const UInt32 mBit;            //!< Bit of the word corresponding to this net
};


ShellVisNet::ShellVisNet(ShellNet* wrappedNet, const CarbonDatabaseNode* dbNode, const IODBIntrinsic* intrinsic)
  : ShellNetWrapper1To1(wrappedNet),
    mDBNode(dbNode),
    mIntrinsic(intrinsic),
    mStorageOp(NULL)
{
  mNumWords = wrappedNet->getNumUInt32s();
  mValBuf = CARBON_ALLOC_VEC(UInt32, mNumWords);
  mDriveBuf = CARBON_ALLOC_VEC(UInt32, mNumWords);
  mBitWidth = mIntrinsic->getWidth();
}

ShellVisNet::~ShellVisNet()
{
  CARBON_FREE_VEC(mValBuf, UInt32, mNumWords);
  CARBON_FREE_VEC(mDriveBuf, UInt32, mNumWords);
  delete mStorageOp;
}

const ShellVisNet *ShellVisNet::castShellVisNet() const
{
  return this;
}

const CarbonDatabaseNode* ShellVisNet::getDBNode() const
{
  return mDBNode;
}

void ShellVisNet::getTraits(Traits* traits) const
{
  // The wrapped net has most of the correct traits, except for the
  // width, and the fact that this might have complex storage
  mNet->getTraits(traits);
  traits->mWidth = mBitWidth;

  // Determine if the net has complex storage, that is, whether we
  // can't create a simple POD pointer to its value.  We need the
  // custom storage operator object for that.
  if (mStorageOp == NULL) {
    mStorageOp = createStorageOp();
  }

  const void* storagePtr = mStorageOp->getStoragePtr();
  if (storagePtr == NULL) {
    traits->mHasComplexStorage = true;
  } else {
    // Set the traits storage pointer.  A pointer is a pointer, so we
    // could probably just call Traits::putByte() for all of these,
    // but let's be safe.
    if (mBitWidth <= 8) {
      traits->putByte(static_cast<const UInt8*>(storagePtr));
    } else if (mBitWidth <= 16) {
      traits->putShort(static_cast<const UInt16*>(storagePtr));
    } else if ((mBitWidth > 32) && (mBitWidth <= 64)) {
      traits->putLLong(static_cast<const UInt64*>(storagePtr));
    } else {
      // This is either a single UInt32 (17-32 bits) or an array of
      // UInt32s from a BitVector (65+ bits)
      traits->putLongPtr(static_cast<const UInt32*>(storagePtr));
    }

    // If this is a bitselect, we need to set the normalized index
    // into the POD.
    UInt32 bitsel = 0;
    if (getBitsel(&bitsel)) {
      traits->mPodBitsel = bitsel;
    }
  }

}

const IODBIntrinsic* ShellVisNet::getIntrinsic() const
{
  return mIntrinsic;
}

bool ShellVisNet::isVector() const
{
  return mIntrinsic->getType() == IODBIntrinsic::eVector;
}

bool ShellVisNet::isScalar() const
{
  return mIntrinsic->getType() == IODBIntrinsic::eScalar;
}

ShellNet::Storage ShellVisNet::allocShadow() const
{
  // allocShadow() will be called before any other Storage functions,
  // so maybe create our StorageOp object here.
  if (mStorageOp == NULL) {
    mStorageOp = createStorageOp();
  }
  return mStorageOp->allocShadow(mNumWords);
}

void ShellVisNet::freeShadow(Storage* shadow)
{
  mStorageOp->freeShadow(shadow);
}

ShellNet::ValueState ShellVisNet::writeIfNotEq(char* valueStr, size_t len, Storage* shadow,
                                           NetFlags flags)
{
  ShellNet::ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x')) {
    state = eChanged;
    update(shadow);
    format(valueStr, len, eCarbonBin, flags, NULL);
  }
  return state;
}

CarbonStatus ShellVisNet::format(char* valueStr, size_t len,
                                 CarbonRadix strFormat, NetFlags,
                                 CarbonModel* model) const
{
  examine(mValBuf, mDriveBuf, eCalcDrive, NULL);

  // Just like CarbonExprNet::format(), we need to convert the
  // calculated drive to an equivalent idrive by inverting the bits.
  // Don't worry about dirty bits, because they'll be ignored by
  // sDoFormat().
  if (mNumWords == 1) {
    *mDriveBuf = ~(*mDriveBuf);
  } else {
    for (UInt32 i = 0; i < mNumWords; ++i) {
      mDriveBuf[i] = ~mDriveBuf[i];
    }
  }

  CarbonStatus stat = ShellNet::sDoFormat(valueStr, len, strFormat, mValBuf, mDriveBuf, NULL, mBitWidth, model);
  return stat;
}

void ShellVisNet::update(Storage* shadow) const
{
  mStorageOp->update(shadow);
}

void ShellVisNet::updateUnresolved(Storage* shadow) const
{
  mStorageOp->updateUnresolved(shadow);
}

void ShellVisNet::runValueChangeCB(CarbonNetValueCBData* cbData, 
                                   UInt32*, 
                                   UInt32*,
                                   CarbonTriValShadow* fullShadow,
                                   CarbonModel* model) const
{
  mStorageOp->runValueChangeCB(cbData, mValBuf, mDriveBuf, fullShadow, model);
}

const CarbonMemory* ShellVisNet::castMemory() const
{
  return NULL;
}

const CarbonModelMemory* ShellVisNet::castModelMemory() const
{
  return NULL;
}


void ShellVisNet::putToZero(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

void ShellVisNet::putToOnes(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

CarbonStatus ShellVisNet::setRange(int, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellVisNet::clearRange(int, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

int ShellVisNet::hasDriveConflictRange(SInt32, SInt32) const
{
  ST_ASSERT(0, getName());
  return -1;
}

CarbonStatus ShellVisNet::examineValXDriveWord(UInt32*, UInt32*, int) const
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void ShellVisNet::setRawToUndriven(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

ShellNet::ValueState ShellVisNet::writeIfNotEqForce(char*, size_t, Storage*, NetFlags, ShellNet*)
{
  ST_ASSERT(0, getName());
  return eUnchanged;
}

CarbonStatus ShellVisNet::formatForce(char*, size_t, CarbonRadix, NetFlags, ShellNet*, CarbonModel*) const
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void ShellVisNet::putChangeArrayRef(CarbonChangeType*)
{
  ST_ASSERT(0, getName());
}

CarbonChangeType* ShellVisNet::getChangeArrayRef()
{
  ST_ASSERT(0, getName());
  return NULL;
}

CarbonStatus ShellVisNet::examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const
{
  return eCarbon_ERROR;
}



ShellVisNetBitsel::ShellVisNetBitsel(ShellNet* wrappedNet, const CarbonDatabaseNode* dbNode,
                                     const IODBIntrinsic* intrinsic, UInt32 bit, SInt32 hdlBit)
  : ShellVisNet(wrappedNet, dbNode, intrinsic),
    mBit(bit),
    mWord(bit / 32),
    mWordBit(bit % 32),
    mBitMask(1 << (bit % 32)),
    mHdlBit(hdlBit)

{
  ST_ASSERT(mWord < mNumWords, mDBNode->getSymTabNode());
}

ShellVisNetBitsel::~ShellVisNetBitsel()
{
}

bool ShellVisNetBitsel::getBitsel(UInt32* bitsel) const
{
  *bitsel = mBit;
  return true;
}

ShellVisNet::StorageOp* ShellVisNetBitsel::createStorageOp() const
{
  // If the net we're wrapping is a POD, we can use a storage op that
  // takes a bitselect of its storage location.
  Traits traits;
  mNet->getTraits(&traits);

  StorageOp* storageOp = NULL;
  if (traits.isNonPod()) {
    storageOp =  new StorageOpNonPOD(this);
  } else {
    UInt32 width = mNet->getBitWidth();
    // Get the pointer and create the POD storage operator of the
    // right type, specifying the correct bit of the POD to use.
    if (width <= 8) {
      const UInt8* storagePtr = traits.getByte();
      storageOp = new StorageOpPODBitsel<UInt8>(this, storagePtr, mBit);
    } else if (width <= 16) {
      const UInt16* storagePtr = traits.getShort();
      storageOp = new StorageOpPODBitsel<UInt16>(this, storagePtr, mBit);
    } else if (width <= 32) {
      const UInt32* storagePtr = traits.getLongPtr();
      storageOp = new StorageOpPODBitsel<UInt32>(this, storagePtr, mBit);
    } else if (width <= 64) {
      const UInt64* storagePtr = traits.getLLong();
      storageOp = new StorageOpPODBitsel<UInt64>(this, storagePtr, mBit);
    } else {
      // An array of UInt32s in a BitVector.  We need to determine the
      // number of words
      UInt32 words = mNet->getNumUInt32s();
      const UInt32* storagePtr = traits.getLongPtr();
      storageOp = new StorageOpPODArrayBitsel(this, storagePtr, words, mBit);
    }
  }
  return storageOp;
}

bool ShellVisNetBitsel::isDataNonZero() const
{
  // Examine the wrapped net, and see if our bit is non-zero
  mNet->examine(mValBuf, NULL, eIDrive, NULL);
  return (mValBuf[mWord] & mBitMask) != 0;
}

bool ShellVisNetBitsel::setToDriven(CarbonModel* model)
{
  // We need to do a depositRange here, because we don't want to
  // affect the other bits.
  UInt32 drv = 0;
  mNet->depositRange(NULL, mHdlBit, mHdlBit, &drv, model);
  // CarbonExprNet always returns false here.  Is that correct?
  return false;
}

bool ShellVisNetBitsel::setToUndriven(CarbonModel* model)
{
  mNet->setRangeToUndriven(mHdlBit, mHdlBit, model);
  return false;
}

bool ShellVisNetBitsel::setWordToUndriven(int index, CarbonModel* model)
{
  // The net is a bit, so there's only ever one word
  if (index == 0) {
    setToUndriven(model);
  }
  return false;
}

bool ShellVisNetBitsel::setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model)
{
  // The net is a bit, so MSB/LSB are defined as zero
  if ((range_msb == 0) && (range_lsb == 0)) {
    setToUndriven(model);
  }
  return false;
}

CarbonStatus ShellVisNetBitsel::examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  // Examine the wrapped net, and return our bit
  CarbonStatus stat = mNet->examine(mValBuf, mDriveBuf, mode, NULL);
  if (buf != NULL) {
    *buf = (mValBuf[mWord] >> mWordBit) & 0x1;
  }
  if (drive != NULL) {
    *drive = (mDriveBuf[mWord] >> mWordBit) & 0x1;
  }
  return stat;
}

CarbonStatus ShellVisNetBitsel::examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  CarbonStatus stat = eCarbon_ERROR;
  // The net is a bit, so there's only ever one word
  if (index == 0) {
    stat = examine(buf, drive, mode, NULL);
  }
  return stat;
}

CarbonStatus ShellVisNetBitsel::examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const
{
  CarbonStatus stat = eCarbon_ERROR;
  // The net is a bit, so MSB/LSB are defined as zero
  if ((range_msb == 0) && (range_lsb == 0)) {
    stat = examine(buf, drive, eIDrive, NULL);
  }
  return stat;
}

CarbonStatus ShellVisNetBitsel::deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  // Work around bug8506 - a NULL drive pointer should be interpreted
  // as fully driven by depositRange(), but it isn't.
  const UInt32 zero = 0;
  if (drive == NULL) {
    drive = &zero;
  }
  CarbonStatus stat = mNet->depositRange(buf, mHdlBit, mHdlBit, drive, model);
  return stat;
}

CarbonStatus ShellVisNetBitsel::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonStatus stat = eCarbon_ERROR;
  // The net is a bit, so there's only ever one word
  if (index == 0) {
    stat = deposit(&buf, &drive, model);
  }
  return stat;
}

CarbonStatus ShellVisNetBitsel::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonStatus stat = eCarbon_ERROR;
  // The net is a bit, so MSB/LSB are defined as zero
  if ((range_msb == 0) && (range_lsb == 0)) {
    stat = deposit(buf, drive, model);
  }
  return stat;
}

void ShellVisNetBitsel::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  // Work around bug8506 - a NULL drive pointer should be interpreted
  // as fully driven by depositRange(), but it isn't.
  const UInt32 zero = 0;
  if (drive == NULL) {
    drive = &zero;
  }
  mNet->fastDepositRange(buf, mHdlBit, mHdlBit, drive, model);
}

void ShellVisNetBitsel::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  // The net is a bit, so there's only ever one word
  if (index == 0) {
    fastDeposit(&buf, &drive, model);
  }
}

void ShellVisNetBitsel::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  // The net is a bit, so MSB/LSB are defined as zero
  if ((range_msb == 0) && (range_lsb == 0)) {
    fastDeposit(buf, drive, model);
  }
}

CarbonStatus ShellVisNetBitsel::force(const UInt32* buf, CarbonModel* model)
{
  CarbonStatus stat = mNet->forceRange(buf, mHdlBit, mHdlBit, model);
  return stat;
}

CarbonStatus ShellVisNetBitsel::forceWord(UInt32 buf, int index, CarbonModel* model)
{
  CarbonStatus stat = eCarbon_ERROR;
  // The net is a bit, so there's only ever one word
  if (index == 0) {
    stat = force(&buf, model);
  }
  return stat;
}

CarbonStatus ShellVisNetBitsel::forceRange(const UInt32* buf,
                                           int range_msb, int range_lsb,
                                           CarbonModel* model)
{
  CarbonStatus stat = eCarbon_ERROR;
  // The net is a bit, so MSB/LSB are defined as zero
  if ((range_msb == 0) && (range_lsb == 0)) {
    stat = force(buf, model);
  }
  return stat;
}

CarbonStatus ShellVisNetBitsel::release(CarbonModel* model)
{
  CarbonStatus stat = mNet->releaseRange(mHdlBit, mHdlBit, model);
  return stat;
}

CarbonStatus ShellVisNetBitsel::releaseWord(int index, CarbonModel* model)
{
  CarbonStatus stat = eCarbon_ERROR;
  // The net is a bit, so there's only ever one word
  if (index == 0) {
    stat = release(model);
  }
  return stat;
}

CarbonStatus ShellVisNetBitsel::releaseRange(int range_msb, int range_lsb, CarbonModel* model)
{
  CarbonStatus stat = eCarbon_ERROR;
  // The net is a bit, so MSB/LSB are defined as zero
  if ((range_msb == 0) && (range_lsb == 0)) {
    stat = release(model);
  }
  return stat;
}

ShellNet::ValueState ShellVisNetBitsel::compare(const Storage shadow) const
{
  ShellNet::ValueState stat = mStorageOp->compare(shadow, mValBuf, mDriveBuf, mNumWords);
  return stat;
}

int ShellVisNetBitsel::hasDriveConflict() const
{
  int ret = mNet->hasDriveConflictRange(mHdlBit, mHdlBit);
  return ret;
}

void ShellVisNetBitsel::getExternalDrive(UInt32* xdrive) const
{
  examine(NULL, xdrive, eXDrive, NULL);
}

ShellNet::ValueState ShellVisNetBitsel::compareUpdateExamineUnresolved(Storage* shadow, UInt32* value,
                                                             UInt32* drive)
{
  ShellNet::ValueState stat = mStorageOp->compareUpdateExamineUnresolved(shadow, value, drive);
  return stat;
}





ShellVisNetMemsel::ShellVisNetMemsel(ShellNet* wrappedNet, const CarbonDatabaseNode* dbNode,
                                     const IODBIntrinsic* intrinsic, SInt32 address)
  : ShellVisNet(wrappedNet, dbNode, intrinsic),
    mAddress(address)
{
  updateMem();
  // Address must be in range
  ConstantRange memRange(mMem->getLeftAddr(), mMem->getRightAddr());
  ST_ASSERT(memRange.contains(mAddress), mDBNode->getSymTabNode());
}

ShellVisNetMemsel::~ShellVisNetMemsel()
{
}

bool ShellVisNetMemsel::getBitsel(UInt32*) const
{
  return false;
}

ShellVisNet::StorageOp* ShellVisNetMemsel::createStorageOp() const
{
  // We're selecting a full word of a memory.  If that memory is
  // non-sparse we can use a POD storage
  // operator.  Otherwise, the storage is complex.

  CarbonModelMemory* modelMem = mNet->castModelMemory();
  bool isSparse = false;
  // We're not guaranteed to wrap a model memory.  With Replay, we
  // might be wrapping a record or playback memory.
  if (modelMem != NULL) {
    isSparse = modelMem->isSparse();
  }
  UInt32 width = mNet->getBitWidth();

  StorageOp* storageOp = NULL;
  if ((modelMem == NULL) || isSparse) {
    storageOp = new StorageOpNonPOD(this);
  } else {
    // Get the pointer and create the POD storage operator of the
    // right type.
    void* storagePtr = modelMem->getStoragePtr(mAddress);
    if (width <= 8) {
      storageOp = new StorageOpPOD<UInt8>(storagePtr);
    } else if (width <= 16) {
      storageOp = new StorageOpPOD<UInt16>(storagePtr);
    } else if (width <= 32) {
      storageOp = new StorageOpPOD<UInt32>(storagePtr);
    } else if (width <= 64) {
      storageOp = new StorageOpPOD<UInt64>(storagePtr);
    } else {
      // An array of UInt32s in a BitVector.  We need to determine the
      // number of words
      UInt32 words = mNet->getNumUInt32s();
      storageOp = new StorageOpPODArray(storagePtr, words);
    }
  }

  return storageOp;
}

bool ShellVisNetMemsel::isDataNonZero() const
{
  // Examine the wrapped memory, and see if our word is non-zero
  mMem->examineMemory(mAddress, mValBuf);
  bool isZero = CarbonValRW::isZero(mValBuf, mNumWords);
  return !isZero;
}

CarbonStatus ShellVisNetMemsel::examine (UInt32* buf, UInt32* drive, ExamineMode, CarbonModel*) const
{
  // Examine the wrapped memory, and return our word
  CarbonStatus stat = mMem->examineMemory(mAddress, buf);
  // Memories can't be tristates.
  if (drive != NULL) {
    CarbonValRW::setToZero(drive, mNumWords);
  }
  return stat;
}

CarbonStatus ShellVisNetMemsel::examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode, CarbonModel*) const
{
  // Examine the whole memory word, and copy the UInt32 of interest
  CarbonStatus stat = eCarbon_ERROR;
  if (index < static_cast<SInt32>(mNumWords)) {
    stat = mMem->examineMemory(mAddress, mValBuf);
    if (buf != NULL) {
      *buf = mValBuf[index];
    }
    if (drive != NULL) {
      // Memories can't be tristates.
      *drive = 0;
    }
  }
  return stat;
}

CarbonStatus ShellVisNetMemsel::examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  // Make sure the range is OK
  size_t index = 0;
  size_t length = 0;
  CarbonStatus stat = CarbonUtil::calcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK) {
    // Examine the whole memory word, and copy the range of interest
    stat = mMem->examineMemory(mAddress, mValBuf);
    if (buf != NULL) {
      CarbonValRW::cpSrcRangeToDest(buf, mValBuf, index, length);
    }
    if (drive != NULL) {
      // Memories can't be tristates.
      UInt32 words = (length + 31) / 32;
      CarbonValRW::setToZero(drive, words);
    }
  }
  return stat;
}

CarbonStatus ShellVisNetMemsel::deposit (const UInt32* buf, const UInt32*, CarbonModel*)
{
  // Deposit the correct word in the memory
  CarbonStatus stat = mMem->depositMemory(mAddress, buf);
  return stat;
}

CarbonStatus ShellVisNetMemsel::depositWord (UInt32 buf, int index, UInt32, CarbonModel*)
{
  // Deposit the correct word in the memory
  CarbonStatus stat = mMem->depositMemoryWord(mAddress, buf, index);
  return stat;
}

CarbonStatus ShellVisNetMemsel::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel*)
{
  // Deposit the correct word in the memory
  fixRange(&range_msb, &range_lsb);
  CarbonStatus stat = mMem->depositMemoryRange(mAddress, buf, range_msb, range_lsb);
  return stat;
}

void ShellVisNetMemsel::fastDeposit(const UInt32* buf, const UInt32*, CarbonModel*)
{
  // Deposit the correct word in the memory
  mMem->depositMemory(mAddress, buf);
}

void ShellVisNetMemsel::fastDepositWord (UInt32 buf, int index, UInt32, CarbonModel*)
{
  // Deposit the correct word in the memory
  mMem->depositMemoryWord(mAddress, buf, index);
}

void ShellVisNetMemsel::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel*)
{
  // Deposit the correct word in the memory
  fixRange(&range_msb, &range_lsb);
  mMem->depositMemoryRange(mAddress, buf, range_msb, range_lsb);
}

ShellNet::ValueState ShellVisNetMemsel::compare(const Storage shadow) const
{
  // This might be a POD or complex storage.  Let the storage operator
  // handle it.  Either way, memories can't be tristates, so the drive
  // doesn't need to be compared.
  ShellNet::ValueState stat = mStorageOp->compareNoDrive(shadow, mValBuf, mNumWords);
  return stat;
}

void ShellVisNetMemsel::getExternalDrive(UInt32* xdrive) const
{
  // Memories are always driven
  if (xdrive != NULL) {
    CarbonValRW::setToZero(xdrive, mNumWords);
  }
}

void ShellVisNetMemsel::updateMem()
{
  // Update our memory pointer for the new wrapped net.  It must be a
  // memory.
  mMem = mNet->castMemory();
  ST_ASSERT(mMem != NULL, mDBNode->getSymTabNode());
}

void ShellVisNetMemsel::replaceNet(ShellNet* net)
{
  // Call the base class's replaceNet(), then update our memory
  // pointers.  This is needed because replay installs record/playback
  // memories, which need to be accessed.
  ShellNetWrapper1To1::replaceNet(net);
  updateMem();
}

void ShellVisNetMemsel::fixRange(SInt32* msb, SInt32* lsb) const
{
  // This can be ugly.  Thanks to memory resynthesis, the vector range
  // of the net may not be the same as the vector range of the
  // memory.
  //
  // For example, the net is declared as [10:3], but thanks to
  // resynthesis the memory thinks it's [7:0].  This means a requested
  // range of [8:5] needs to be converted to [5:2].
  const ConstantRange* netRange = mIntrinsic->getVecRange();
  const IODBIntrinsic* memIntrinsic = mNet->getIntrinsic();
  const ConstantRange* memRange = memIntrinsic->getVecRange();
  if (netRange != memRange) {
    // normalize the requested range with respect to the net's range,
    // then denormalize it with respect to the memory's range.
    ConstantRange requestedRange(*msb, *lsb);
    requestedRange.normalize(netRange);
    requestedRange.denormalize(memRange);
    *msb = requestedRange.getMsb();
    *lsb = requestedRange.getLsb();
  }
}



ShellVisNetMemBitsel::ShellVisNetMemBitsel(ShellNet* wrappedNet, const CarbonDatabaseNode* dbNode,
                                           const IODBIntrinsic* intrinsic, SInt32 address, UInt32 bit, SInt32 hdlBit)
  : ShellVisNetMemsel(wrappedNet, dbNode, intrinsic, address),
    mBit(bit),
    mWord(bit / 32),
    mWordBit(bit % 32),
    mBitMask(1 << (bit % 32)),
    mHdlBit(hdlBit)
{
  ST_ASSERT(mWord < mNumWords, mDBNode->getSymTabNode());
}

ShellVisNetMemBitsel::~ShellVisNetMemBitsel()
{
}

bool ShellVisNetMemBitsel::getBitsel(UInt32* bitsel) const
{
  *bitsel = mBit;
  return true;
}

ShellVisNet::StorageOp* ShellVisNetMemBitsel::createStorageOp() const
{
  // We're taking a bitselect of a memory word.  If the memory we're
  // wrapping is non-sparse, we can use a storage op that takes a
  // bitselect of its storage location.

  CarbonModelMemory* modelMem = mNet->castModelMemory();
  bool isSparse = false;
  // We're not guaranteed to wrap a model memory.  With Replay, we
  // might be wrapping a record or playback memory.
  if (modelMem != NULL) {
    isSparse = modelMem->isSparse();
  }
  UInt32 width = mNet->getBitWidth();

  StorageOp* storageOp = NULL;
  if ((modelMem == NULL) || isSparse) {
    storageOp = new StorageOpNonPOD(this);
  } else {
    // Get the pointer and create the POD storage operator of the
    // right type, specifying the correct bit of the POD to use.
    void* storagePtr = modelMem->getStoragePtr(mAddress);
    if (width <= 8) {
      storageOp = new StorageOpPODBitsel<UInt8>(this, storagePtr, mBit);
    } else if (width <= 16) {
      storageOp = new StorageOpPODBitsel<UInt16>(this, storagePtr, mBit);
    } else if (width <= 32) {
      storageOp = new StorageOpPODBitsel<UInt32>(this, storagePtr, mBit);
    } else if (width <= 64) {
      storageOp = new StorageOpPODBitsel<UInt64>(this, storagePtr, mBit);
    } else {
      // An array of UInt32s in a BitVector.  We need to determine the
      // number of words
      UInt32 words = mNet->getNumUInt32s();
      storageOp = new StorageOpPODArrayBitsel(this, storagePtr, words, mBit);
    }
  }

  return storageOp;
}

bool ShellVisNetMemBitsel::isDataNonZero() const
{
  // Examine the wrapped memory, and see if our bit in the word is non-zero
  mMem->examineMemory(mAddress, mValBuf);
  return (mValBuf[mWord] & mBitMask) != 0;
}

CarbonStatus ShellVisNetMemBitsel::examine (UInt32* buf, UInt32* drive, ExamineMode, CarbonModel*) const
{
  // Examine the wrapped memory, and return our bit
  CarbonStatus stat = mMem->examineMemory(mAddress, mValBuf);
  if (buf != NULL) {
    *buf = (mValBuf[mWord] >> mWordBit) & 0x1;
  }
  // Memories can't be tristates.
  if (drive != NULL) {
    *drive = 0;
  }
  return stat;
}

CarbonStatus ShellVisNetMemBitsel::examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  CarbonStatus stat = eCarbon_ERROR;
  // The net is a bit, so there's only ever one word
  if (index == 0) {
    stat = examine(buf, drive, mode, NULL);
  }
  return stat;
}

CarbonStatus ShellVisNetMemBitsel::examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel*) const
{
  CarbonStatus stat = eCarbon_ERROR;
  // The net is a bit, so MSB/LSB are defined as zero
  if ((range_msb == 0) && (range_lsb == 0)) {
    stat = examine(buf, drive, eIDrive, NULL);
  }
  return stat;
}

CarbonStatus ShellVisNetMemBitsel::deposit (const UInt32* buf, const UInt32*, CarbonModel*)
{
  // Deposit the correct range of our word in the memory
  CarbonStatus stat = mMem->depositMemoryRange(mAddress, buf, mHdlBit, mHdlBit);
  return stat;
}

CarbonStatus ShellVisNetMemBitsel::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonStatus stat = eCarbon_ERROR;
  // The net is a bit, so there's only ever one word
  if (index == 0) {
    stat = deposit(&buf, &drive, model);
  }
  return stat;
}

CarbonStatus ShellVisNetMemBitsel::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonStatus stat = eCarbon_ERROR;
  // The net is a bit, so MSB/LSB are defined as zero
  if ((range_msb == 0) && (range_lsb == 0)) {
    stat = deposit(buf, drive, model);
  }
  return stat;
}

void ShellVisNetMemBitsel::fastDeposit(const UInt32* buf, const UInt32*, CarbonModel*)
{
  // Deposit the correct range of our word in the memory
  mMem->depositMemoryRange(mAddress, buf, mHdlBit, mHdlBit);
}

void ShellVisNetMemBitsel::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  // The net is a bit, so there's only ever one word
  if (index == 0) {
    fastDeposit(&buf, &drive, model);
  }
}

void ShellVisNetMemBitsel::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  // The net is a bit, so MSB/LSB are defined as zero
  if ((range_msb == 0) && (range_lsb == 0)) {
    fastDeposit(buf, drive, model);
  }
}

void ShellVisNetMemBitsel::getExternalDrive(UInt32* xdrive) const
{
  // Memories are always driven
  if (xdrive != NULL) {
    *xdrive = 0;
  }
}
