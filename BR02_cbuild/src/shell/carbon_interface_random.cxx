// -*-c++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/carbon_interface.h"
#include "hdl/HdlVerilogDist.h"

// These functions need to be in a separate file because most of the
// HdlVerilogDist functions use log(), which is in libm.a and not
// included in $CARBON_LIB_LIST.  Otherwise, any use of the other
// functions in carbon_interface.o would cause unresolved references,
// even if the carbonInterfaceVerilogDist* functions weren't used.

SInt32 carbonInterfaceVerilogDistChiSquare(SInt32 *seed, SInt32 df)
{
  return HdlVerilogDist::ChiSquare(seed, df);
}

SInt32 carbonInterfaceVerilogDistErlang(SInt32 *seed, SInt32 k, SInt32 mean)
{
  return HdlVerilogDist::Erlang(seed, k, mean);
}

SInt32 carbonInterfaceVerilogDistExponential(SInt32 *seed, SInt32 mean)
{
  return HdlVerilogDist::Exponential(seed, mean);
}

SInt32 carbonInterfaceVerilogDistNormal(SInt32 *seed, SInt32 mean, SInt32 sd)
{
  return HdlVerilogDist::Normal(seed, mean, sd);
}

SInt32 carbonInterfaceVerilogDistPoisson(SInt32 *seed, SInt32 mean)
{
  return HdlVerilogDist::Poisson(seed, mean);
}

SInt32 carbonInterfaceVerilogDistT(SInt32 *seed, SInt32 df)
{
  return HdlVerilogDist::T(seed, df);
}

SInt32 carbonInterfaceVerilogDistUniform(SInt32 *seed, SInt32 start, SInt32 end)
{
  return HdlVerilogDist::Uniform(seed, start, end);
}

SInt32 carbonInterfaceVerilogDistRandom(SInt32 *seed)
{
  return HdlVerilogDist::Random(seed);
}

