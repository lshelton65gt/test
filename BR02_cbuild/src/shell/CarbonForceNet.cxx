// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/CarbonForceNet.h"
#include "shell/CarbonVectorBase.h"
#include "util/DynBitVector.h"
#include "util/UtConv.h"
#include "symtab/STSymbolTable.h"

CarbonForceNet::CarbonForceNet(ShellNet* storage, ShellNet* forceMask) :
  mStorage(storage), mForceMask(forceMask)
{
}

CarbonForceNet::~CarbonForceNet()
{
  // Unconditionally delete these without checking their reference
  // counts.  Replay is pessimistic when instrumenting nets,
  // incrementing reference counts when it might not be necessary.
  // This means some nets persist until model destruction when they
  // could have been freed earlier.
  //
  // For most nets, that's not a problem, because
  // CarbonHookup::clearDB() does a symbol table walk and forcibly
  // frees all allocated nets.  Force subordinates pose a problem,
  // though, because they're not covered in that walk; they rely on
  // their master to free them.  If the reference count gets out of
  // sync, they won't be deleted.
  //
  // It is safe to ignore reference count here, because force
  // subordinates are only ever referenced by their master, and
  // there's a one-to-one relationship there.

  delete mStorage;
  delete mForceMask;
}

void CarbonForceNet::putChangeArrayRef(CarbonChangeType* changeArrayIndex)
{
  mStorage->putChangeArrayRef(changeArrayIndex);
}

CarbonChangeType* CarbonForceNet::getChangeArrayRef()
{
  return mStorage->getChangeArrayRef();
}

void CarbonForceNet::getTraits(Traits* traits) const
{
  mStorage->getTraits(traits);
}

bool CarbonForceNet::isVector () const
{
  return mStorage->isVector();
}

bool CarbonForceNet::isScalar() const
{
  return mStorage->isScalar();
}

bool CarbonForceNet::isTristate() const
{
  return mStorage->isTristate();
}

bool CarbonForceNet::isReal() const
{
  return mStorage->isReal();
}

bool CarbonForceNet::isDataNonZero() const
{
  return mStorage->isDataNonZero();
}

bool CarbonForceNet::setToDriven(CarbonModel* model)
{
  return mStorage->setToDriven(model);
}

bool CarbonForceNet::setToUndriven(CarbonModel* model)
{
  return mStorage->setToUndriven(model);
}

bool CarbonForceNet::resolveXdrive(CarbonModel* model)
{
  return mStorage->resolveXdrive(model);
}

void CarbonForceNet::getExternalDrive(UInt32* xdrive) const
{
  mStorage->getExternalDrive(xdrive);
}

bool CarbonForceNet::setWordToUndriven(int index, CarbonModel* model)
{
  return mStorage->setWordToUndriven(index, model);
}

bool CarbonForceNet::setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model)
{
  return mStorage->setRangeToUndriven(range_msb, range_lsb, model);
}

CarbonStatus CarbonForceNet::examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = mStorage->examine(buf, drive, mode, model);
  if ((stat == eCarbon_OK) && drive)
  {
    /*
      For each bit that we are forcing, set the drive to 0. This makes
      wavedump formatting much easier.
    */
    UInt32 forceMaskWord = 0;
    int numWords = mStorage->getNumUInt32s();
    for (int i = 0; i < numWords; ++i)
    {
      mForceMask->examineWord(&forceMaskWord, i, NULL, eIDrive, model);
      drive[i] &= ~forceMaskWord;
    }
  }
  return stat;
}

CarbonStatus CarbonForceNet::examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = mStorage->examineWord(buf, index, drive, mode, model);
  if ((stat == eCarbon_OK) && drive)
  {
    // see CarbonForceNet::examine()
    UInt32 forceMaskWord = 0;
    mForceMask->examineWord(&forceMaskWord, index, NULL, eIDrive, model);
    *drive &= ~forceMaskWord;
  }
  return stat;
}

CarbonStatus CarbonForceNet::examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  CarbonStatus stat = mStorage->examineRange(buf, range_msb, range_lsb, drive, model);
  if ((stat == eCarbon_OK) && drive)
  {
    // see CarbonForceNet::examine()

    // Ranges are known not to be MT-Safe right now, so for expediency
    // using DynBitVectors
    int numBits = getBitWidth();
    DynBitVector forceMask(numBits);
    UInt32* maskArr = forceMask.getUIntArray();
    mForceMask->examineRange(maskArr, range_msb, range_lsb, NULL, model);
    size_t numWords = CarbonUtil::getRangeNumUInt32s(range_msb, range_lsb);
    for (size_t i = 0; i < numWords; ++i)
      drive[i] &= ~maskArr[i];
  }
  return stat;
}

CarbonStatus CarbonForceNet::deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  CarbonStatus status = eCarbon_OK;

  if (mForceMask->isDataNonZero())
  {
    int numWords = mStorage->getNumUInt32s();
    for (int i = 0; (i < numWords) && (status == eCarbon_OK); ++i)
    {
      UInt32 driveWord = 0;
      if (drive)
        driveWord = drive[i];
      if (buf)
        status = depositWord(buf[i], i, driveWord, model);
    }
  }
  else
    status = mStorage->deposit(buf, drive, model);
  return status;
}

CarbonStatus CarbonForceNet::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonStatus status = eCarbon_OK;

  UInt32 dataTmp = 0;
  UInt32 driveTmp = 0;
  UInt32 forceTmp = 0;
  
  // Use CarbonForceNet's examineWord method to get a clean idrive
  // based on the force mask
  if (mStorage->examineWord(&dataTmp, index, &driveTmp, eXDrive, model) != eCarbon_OK)
    status = eCarbon_ERROR;
  if (mForceMask->examineWord(&forceTmp, index, NULL, eIDrive, model) != eCarbon_OK)
    status = eCarbon_ERROR;
  
  if (status == eCarbon_OK)  
  {
    dataTmp &= forceTmp;
    dataTmp |= (buf & ~forceTmp);

    // If the model is driving the force, we cannot.
    // If the model is not driving the force, we must,
    // since we do not force z's.

    // first, and the xdrive with the forcemask
    // This gives us the external drive for the force
    UInt32 driveTmp2 = driveTmp & forceTmp;
    
    // Zero the forced bits on the given drive
    drive &= ~forceTmp;
    // Combine the 2 to get the correct external drive
    driveTmp = driveTmp2 | drive;
    status = mStorage->depositWord(dataTmp, index, driveTmp, model);
  }
  
  return status;
}

CarbonStatus CarbonForceNet::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonStatus status = eCarbon_OK;
  const size_t wordBitSize = (sizeof(UInt32) *8);
  bool hasForceOn = mForceMask->isDataNonZero();
  if (mStorage->isVector() && hasForceOn)
  {
    size_t numRangeWords = 
      CarbonUtil::getRangeNumUInt32s(range_msb, range_lsb);
  
    const size_t rangeLength = 
      CarbonUtil::getRangeBitWidth(range_msb, range_lsb);

    const size_t vectorBitIndex = CarbonValRW::calcRangeIndex(range_msb, range_lsb, mStorage->getMSB(), mStorage->getLSB());

    size_t vectorWordIndex = vectorBitIndex/wordBitSize;
    const size_t lastWordIndex = (vectorBitIndex + rangeLength - 1)/wordBitSize;
    const size_t numIndices = lastWordIndex - vectorWordIndex + 1;
    const size_t shiftOffset = vectorBitIndex % wordBitSize;
    const size_t backOffset = wordBitSize - shiftOffset;

    // Is the range within 1 word of the value?
    bool isInclusiveWord = false;
    {
      size_t tmpChk = (shiftOffset + rangeLength) % wordBitSize;
      if (tmpChk == 0)
        tmpChk = wordBitSize;
      isInclusiveWord = (tmpChk == (rangeLength + shiftOffset));
    }
    

    // calculate the first value word mask
    UInt32 bValMask = 0;
    if (shiftOffset != 0)
    {
      bValMask = ~bValMask;
      bValMask <<= shiftOffset;
      bValMask = ~bValMask;
    }


    // calculate the last value word mask
    const size_t lastShiftOffset = 
      (vectorBitIndex + rangeLength) % wordBitSize;
    
    UInt32 eValMask = 0;
    if (lastShiftOffset != 0)
    {
      eValMask = ~eValMask;
      eValMask <<= lastShiftOffset;
    }
    
    // get the value for the first word
    UInt32 exWordVal = 0; 
    UInt32 exWordDrv = 0;
    status = mStorage->examineValXDriveWord(&exWordVal, &exWordDrv, vectorWordIndex);

    UInt32 wordBuf = 0;
    UInt32 depWordVal = 0;
    UInt32 depWordDrv = 0;
    
    if (status == eCarbon_OK)
    {
      depWordVal = exWordVal & bValMask;
      depWordDrv = exWordDrv & bValMask;
      if (buf)
      {
        wordBuf = buf[0];
        wordBuf <<= shiftOffset;
        depWordVal |= wordBuf;
      }
      if (drive)
      {
        wordBuf = drive[0];
        wordBuf <<= shiftOffset;
        depWordDrv |= wordBuf;
      }
      
      if (isInclusiveWord)
      {
        ST_ASSERT(numRangeWords == 1, getName());
        // Fill back in the original value in the head of the word
        depWordVal &= ~eValMask; // clean dirty bits
        depWordDrv &= ~eValMask;
        depWordVal |= (exWordVal & eValMask);
        depWordDrv |= (exWordDrv & eValMask);
      }
      
      status = depositWord(depWordVal, vectorWordIndex, 
                           depWordDrv, model);
      ++vectorWordIndex;
    }
    

    // get the last word that will be deposited to.
    UInt32 lastWordVal = 0;
    UInt32 lastWordDrv = 0;
    if (! isInclusiveWord)
    {
      status = mStorage->examineValXDriveWord(&lastWordVal, &lastWordDrv, lastWordIndex);
    } 
    
    
    UInt32 carryOverMaskVal = 0;    
    UInt32 carryOverMaskDrv = 0;    
    for (size_t i = 1; 
         (i < numIndices) && (status == eCarbon_OK); 
         ++i, ++vectorWordIndex)
    {
      bool isLastWord = (vectorWordIndex == lastWordIndex);
      
      // avoid portability issue with shift >= 32 bits | 64 bits
      carryOverMaskVal = 0;
      if (buf)
      {
        if (backOffset < wordBitSize)
          carryOverMaskVal = buf[i - 1] >> backOffset;
        
        depWordVal = (buf[i] << shiftOffset) | carryOverMaskVal;
        
        if (isLastWord)
        {
          depWordVal &= ~eValMask; // clean dirty bits
          depWordVal |= (lastWordVal & eValMask);
        }
      }
      
      carryOverMaskDrv = 0;
      depWordDrv = 0;
      if (drive)
      {
        carryOverMaskDrv = 0;
        if (backOffset < wordBitSize)
          carryOverMaskDrv = drive[i - 1] >> backOffset;
        
        depWordDrv = (drive[i] << shiftOffset) | carryOverMaskDrv;
        if (isLastWord)
        {
          depWordDrv &= ~eValMask;
          depWordDrv |= (lastWordDrv & eValMask);
        }
      }
      status = depositWord(depWordVal, vectorWordIndex, depWordDrv, model);
    }
  }
  // handles scalar w/ and w/o force and vector w/o force
  else if (! hasForceOn) 
    status = mStorage->depositRange(buf, range_msb, range_lsb, drive, model);
  return status;
}

void CarbonForceNet::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  CarbonForceNet::deposit(buf, drive, model);
}

void CarbonForceNet::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonForceNet::depositWord(buf, index, drive, model);
}

void CarbonForceNet::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonForceNet::depositRange(buf, range_msb, range_lsb, drive, model);
}

ShellNet::Storage CarbonForceNet::allocShadow() const
{
  return mStorage->allocShadow();
}

void CarbonForceNet::freeShadow(Storage* shadow)
{
  mStorage->freeShadow(shadow);
}

void CarbonForceNet::update(Storage* shadow) const
{
  mStorage->update(shadow);
}

ShellNet::ValueState CarbonForceNet::compare(const Storage shadow) const
{
  return mStorage->compare(shadow);
}

ShellNet::ValueState CarbonForceNet::writeIfNotEq(char* valueStr, size_t len, Storage* shadow,
                                                  NetFlags flags)
{
  return mStorage->writeIfNotEqForce(valueStr, len, shadow, flags, mForceMask);
}

CarbonStatus CarbonForceNet::format(char* valueStr, size_t len, 
                                    CarbonRadix strFormat, NetFlags flags, 
                                    CarbonModel* model) const
{
  return mStorage->formatForce(valueStr, len, strFormat, flags, mForceMask, model);
}

bool CarbonForceNet::isInput() const
{
  return mStorage->isInput();
}

int CarbonForceNet::hasDriveConflict() const
{
  return mStorage->hasDriveConflict();
}

int CarbonForceNet::hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const
{
  return mStorage->hasDriveConflictRange(range_msb, range_lsb);
}


bool CarbonForceNet::isForcible() const
{
  return true;
}

const CarbonForceNet* CarbonForceNet::castForceNet() const
{
  return this;
}

void CarbonForceNet::putControlMask(const UInt32* controlMask)
{
  CarbonVectorBase* vec = mStorage->castVector();
  ST_ASSERT(vec, getName());
  vec->putControlMask(controlMask);
}

CarbonStatus CarbonForceNet::force(const UInt32* buf, CarbonModel* model)
{
  release(model);
  mStorage->fastDeposit(buf, NULL, model);

  mForceMask->putToOnes(model);
  return eCarbon_OK;
}


CarbonStatus CarbonForceNet::forceWord(UInt32 buf, int index, CarbonModel* model)
{
  releaseWord(index, model);
  mStorage->fastDepositWord(buf, index, 0, model);
  
  UInt32 tmp = 0;
  tmp = ~tmp;
  if (index == mForceMask->getNumUInt32s() - 1)
  {
    UInt32 offset = mForceMask->getBitWidth() % (sizeof(UInt32) * 8);
    if (offset != 0)
    {
      tmp <<= offset;
      tmp = ~tmp;
    }
  }

  mForceMask->fastDepositWord(tmp, index, 0, model);

  return eCarbon_OK;
}

CarbonStatus CarbonForceNet::forceRange(const UInt32* buf, 
                                        int range_msb, int range_lsb, 
                                        CarbonModel* model)
{
  releaseRange(range_msb, range_lsb, model);
  mStorage->fastDepositRange(buf, range_msb, range_lsb, NULL, model);

  CarbonStatus stat = mForceMask->setRange(range_msb, range_lsb, model);

  return stat;
}

CarbonStatus CarbonForceNet::release(CarbonModel* model)
{
  mForceMask->putToZero(model);
  return eCarbon_OK;
}

CarbonStatus CarbonForceNet::releaseWord(int index, CarbonModel* model)
{
  UInt32 tmp = 0;
  mForceMask->fastDepositWord(tmp, index, 0, model);
  return eCarbon_OK;
}

CarbonStatus CarbonForceNet::releaseRange(int range_msb, int range_lsb, CarbonModel* model)
{
  return mForceMask->clearRange(range_msb, range_lsb, model);
}

void CarbonForceNet::setRawToUndriven(CarbonModel* model)
{
  mStorage->setRawToUndriven(model);
}

void CarbonForceNet::putRange(const ConstantRange* range)
{
  ST_ASSERT(mStorage->isVector(), getName());
  CarbonVectorBase* vec = mStorage->castVector();
  vec->putRange(range);

  ST_ASSERT(mForceMask->isVector(), getName());
  vec = mForceMask->castVector();
  vec->putRange(range);
}

CarbonStatus CarbonForceNet::examineValXDriveWord(UInt32* val, UInt32* drv, int index) const
{
  return mStorage->examineValXDriveWord(val, drv, index);
}

CarbonStatus CarbonForceNet::examine(CarbonReal* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  return mStorage->examine(buf, drive, mode, model);
}

const CarbonMemory* CarbonForceNet::castMemory() const
{
  return NULL;
}

const CarbonModelMemory* CarbonForceNet::castModelMemory() const
{
  return NULL;
}

const CarbonVectorBase* CarbonForceNet::castVector() const
{
  return NULL;
}

const CarbonScalarBase* CarbonForceNet::castScalar() const
{
  return NULL;
}

const ShellNetConstant* CarbonForceNet::castConstant() const
{
  return NULL;
}

const CarbonExprNet* CarbonForceNet::castExprNet() const
{
  return NULL;
}

const UInt32* CarbonForceNet::getControlMask() const
{
  return mStorage->getControlMask();
}

void CarbonForceNet::putToZero(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

void CarbonForceNet::putToOnes(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

CarbonStatus CarbonForceNet::setRange(int, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus CarbonForceNet::clearRange(int, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

ShellNet::ValueState 
CarbonForceNet::writeIfNotEqForce(char*, size_t, Storage*, NetFlags, ShellNet*)
{
  ST_ASSERT(0, getName());
  return eUnchanged;
}

CarbonStatus CarbonForceNet::formatForce(char*, size_t, CarbonRadix, NetFlags, ShellNet*, CarbonModel*) const
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void CarbonForceNet::gatherWrappedNets(NetVec* netVec) const
{
  netVec->clear();
  netVec->reserve(2);
  netVec->push_back(mStorage);
  netVec->push_back(mForceMask);
}

void CarbonForceNet::replaceWrappedNets(const NetVec& netVec)
{
  ST_ASSERT(netVec.size() == 2, getName());

  mStorage = netVec[0];
  mForceMask = netVec[1];
}

void CarbonForceNet::runValueChangeCB(CarbonNetValueCBData* cbData, 
                                      UInt32* newVal, 
                                      UInt32* newDrv,
                                      CarbonTriValShadow* fullShadow,
                                      CarbonModel* model) const
{
  mStorage->runValueChangeCB(cbData, newVal, newDrv, fullShadow, model);
}

ShellNet::ValueState
CarbonForceNet::compareUpdateExamineUnresolved(Storage* shadow,
                                               UInt32* value, UInt32* drive)
{
  return mStorage->compareUpdateExamineUnresolved(shadow, value, drive);
}

void CarbonForceNet::updateUnresolved(Storage* shadow) const
{
  mStorage->updateUnresolved(shadow);
}
