// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/CarbonTristateVector.h"
#include "shell/CarbonTristateVectorInput.h"
#include "shell/ShellNetTristate.h"

#include "shell/CarbonVector.h"
#include "util/ConstantRange.h"

#include "shell/ShellNetMacroDefine.h"

static inline size_t sGetNumWords(int bitWidth)
{
  const size_t wordBitSize = sizeof(UInt32) * 8;
  return (bitWidth + wordBitSize - 1)/wordBitSize;
}

static CarbonStatus sCalcIndexLength(int msb, int lsb, int range_msb, int range_lsb, size_t* index, size_t* length, CarbonModel* model)
{
  return CarbonUtil::calcIndexLength(msb, lsb, range_msb, range_lsb, index, length, model);
}

template <typename T> class CarbonTriShadowPrim
{
public: CARBONMEM_OVERRIDES

  CarbonTriShadowPrim(T) 
  {
  }

  ~CarbonTriShadowPrim() 
  {
  }

  T mValue;
  T mDrive;
};

CarbonTristateVector::CarbonTristateVector()
{}

CarbonTristateVector::~CarbonTristateVector()
{}

void CarbonTristateVector::getTraits(Traits* traits) const
{
  traits->mWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  traits->mIsTristate = true;
}

bool CarbonTristateVector::isTristate() const
{
  return true;
}

CarbonTristateVector1::CarbonTristateVector1(UInt8* idata, UInt8* idrive, 
                                             UInt8* xdata, UInt8* xdrive)
{
  mTri  = new ShellNetBidirect1(idata, idrive, xdata, xdrive);
}

void CarbonTristateVector1::setConstantBits()
{
  mTri->setConstantBits(mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}


//! constructor for tristate vectors <= 8 bits
CarbonTristateVector1::CarbonTristateVector1(UInt8* idata, UInt8* idrive)
{
  mTri  = new ShellNetTristate1(idata, idrive);
}

CarbonTristateVector1::~CarbonTristateVector1()
{
  delete mTri;
}

void CarbonTristateVector1::getTraits(Traits* traits) const
{
  CarbonTristateVector::getTraits(traits);
  traits->putTriByte(mTri);
}

bool CarbonTristateVector1::setToDriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->assertExternalDrive(bitWidth);
}

bool CarbonTristateVector1::setToUndriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->deassertExternalDrive(bitWidth);
}

bool CarbonTristateVector1::resolveXdrive(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->resolveExternalDrive(bitWidth);
}

bool CarbonTristateVector1::setWordToUndriven(int index, CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->deassertExternalDriveWord(index, bitWidth);
}

bool CarbonTristateVector1::setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  bool changed = false;
  if (stat == eCarbon_OK)
  {
    model->getHookup()->setSeenDeposit();
    int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());   
    changed = mTri->deassertExternalDriveRange(index, length, bitWidth);
  }
  return changed;
}

void CarbonTristateVector1::setRawToUndriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->undrivenInit(bitWidth);
}

void CarbonTristateVector1::doFastDeposit(const UInt32* buf, const UInt32* drive,
                                          CarbonModel* model)
{
  bool changed = false; 
  if (! drive)
    changed = setToDriven(model);

  changed |= assignValue(buf, drive);
  doUpdateVHM(changed, model);
}

void CarbonTristateVector1::doFastDepositWord(UInt32 buf, int index, UInt32 drive,
                                              CarbonModel* model)
{
  bool changed = assignValueWord(buf, drive, index);
  doUpdateVHM(changed, model);
}

CarbonStatus CarbonTristateVector1::doDepositWord(UInt32 buf, int index,
                                                  UInt32 drive, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
    doFastDepositWord(buf, index, drive, model);
  return stat;
}

CarbonStatus CarbonTristateVector1::doFastDepositRange(const UInt32* buf,
                                                       int range_msb,
                                                       int range_lsb,
                                                       const UInt32* drive,
                                                       CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK) {
    bool changed = assignValueRange(buf, drive, index, length);
    doUpdateVHM(changed, model);
  }
  return stat;
}

CarbonStatus CarbonTristateVector1::deposit(const UInt32* buf,
                                            const UInt32* drive, CarbonModel* model)
{
  // must set drive first so we can use it
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doFastDeposit(buf, drive, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonTristateVector1::depositWord(UInt32 buf, int index, 
                                                UInt32 drive, CarbonModel* model) 
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    CarbonStatus stat = doDepositWord(buf, index, drive, model);
    return stat;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonTristateVector1::depositRange(const UInt32 *buf, 
                                                 int range_msb, 
                                                 int range_lsb, 
                                                 const UInt32* drive, CarbonModel* model) 
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    return doFastDepositRange(buf, range_msb, range_lsb, drive,model);
  }
  return eCarbon_ERROR;
}

void CarbonTristateVector1::fastDeposit(const UInt32* buf,
                                        const UInt32* drive, CarbonModel* model)
{
  doFastDeposit(buf, drive, model);
}

void CarbonTristateVector1::fastDepositWord(UInt32 buf, int index, 
                                            UInt32 drive, CarbonModel* model) 
{
  doFastDepositWord(buf, index, drive, model);
}

void CarbonTristateVector1::fastDepositRange(const UInt32 *buf, 
                                             int range_msb, 
                                             int range_lsb, 
                                             const UInt32* drive,
                                             CarbonModel* model)
{
  doFastDepositRange(buf, range_msb, range_lsb, drive, model);
}

CarbonStatus CarbonTristateVector1::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  switch(mode)
  {
  case eIDrive:
    examineValue(buf, drive);
    break;
  case eCalcDrive:
    calcValue(buf, drive);
    break;
  case eXDrive:
    examineXVals(buf, drive);
    break;
  }
  return eCarbon_OK;
}
  
CarbonStatus CarbonTristateVector1::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
  {
    switch(mode)
    {
    case eIDrive:
      examineValueWord(buf, drive, index);
      break;
    case eCalcDrive:
      calcValueWord(buf, drive, index);
      break;
    case eXDrive:
      examineValXDriveWord(buf, drive, index);
      break;
    }
  }
  return stat;
}

CarbonStatus CarbonTristateVector1::examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
    examineValueRange(buf, drive, index, length);
  
  return stat;
}

void CarbonTristateVector1::update(ShellNet::Storage* shadow) const {
  CarbonTriShadowPrim<UInt8>* dest = static_cast<CarbonTriShadowPrim<UInt8>*>(*shadow);
  calcValuePrim(&dest->mValue, &dest->mDrive);
}
  
void CarbonTristateVector1::updateUnresolved(ShellNet::Storage* shadow) const {
  CarbonTriShadowPrim<UInt8>* dest = static_cast<CarbonTriShadowPrim<UInt8>*>(*shadow);
  dest->mValue = mTri->getIData();
  dest->mDrive = mTri->getIDrive();
}

//! Compare the shadow and the storage values
ShellNet::ValueState 
CarbonTristateVector1::compare(const ShellNet::Storage shadow) const 
{
  CarbonTriShadowPrim<UInt8>* triShadow = static_cast<CarbonTriShadowPrim<UInt8>*>(shadow);
  return compareValue(&triShadow->mValue, &triShadow->mDrive);    
}
  
ShellNet::ValueState 
CarbonTristateVector1::compareUpdateExamineUnresolved(Storage* shadow,
                                                      UInt32* value, UInt32* drive)
{
  // Examine eIDrive
  examineValue(value, drive);

  // Compare and update
  CarbonTriShadowPrim<UInt8>* triShadow = static_cast<CarbonTriShadowPrim<UInt8>*>(*shadow);
  if ((triShadow->mValue != mTri->getIData()) || 
      (triShadow->mDrive != mTri->getIDrive())) {
    // Update the shadow
    triShadow->mValue = mTri->getIData();
    triShadow->mDrive = mTri->getIDrive();
    return eChanged;

  } else {
    return eUnchanged;
  }
}

//! Write value to valueStr if shadow and storage differ
ShellNet::ValueState 
CarbonTristateVector1::writeIfNotEq(char* valueStr, size_t len, 
                                    ShellNet::Storage* shadow, 
                                    NetFlags flags) 
{
  ShellNet::ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    update(shadow);
    format(valueStr, len, eCarbonBin, flags, NULL);
  }
  return state;
}

ShellNet::ValueState 
CarbonTristateVector1::writeIfNotEqForce(char* valueStr, size_t len, 
                                         ShellNet::Storage* shadow, 
                                         NetFlags flags, ShellNet* forceMask) 
{
  ShellNet::ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    update(shadow);
    formatForce(valueStr, len, eCarbonBin, flags, forceMask, NULL);
  }
  return state;
}
  

CarbonStatus CarbonTristateVector1::examineValXDriveWord(UInt32* val, 
                                                         UInt32* drv, 
                                                         int index) const
{
  CarbonStatus stat = 
    ShellGlobal::carbonTestIndex(index, 0, 0, NULL);
  if (stat == eCarbon_OK)
  {
    copyValAndXDriveWord(val, drv, index);
  }
  return stat;
}

int CarbonTristateVector1::hasDriveConflict() const
{
  return mTri->hasDriveConflict(CBITWIDTH(mRange->getMsb(), mRange->getLsb())) ? 1 : 0;
}

int CarbonTristateVector1::hasDriveConflictRange(SInt32 range_msb, 
                                                 SInt32 range_lsb) const
{
  size_t index, length;
  int ret = 0;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, NULL);
  if (stat == eCarbon_OK)
  {
    if (mTri->hasDriveConflictRange(index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb())))
      ret = 1;
  }
  else
    ret = -1;
  return ret;
}

bool CarbonTristateVector1::isDataNonZero() const
{
  UInt32 val;
  examine(&val, NULL, eIDrive, NULL);
  return (val != 0);
}

void CarbonTristateVector1::copyValAndXDriveWord(UInt32* val, UInt32* drv, 
                                                 int index) const
{
  mTri->copyValXDriveWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector1::examineValue(UInt32* val, UInt32* drv) const
{
  mTri->getExamineValue(val, drv, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector1::examineValueWord(UInt32* val, UInt32* drv, 
                                             int index) const
{
  mTri->getExamineValueWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector1::examineValueRange(UInt32* val, UInt32* drv, 
                                              size_t index, 
                                              size_t length) const
{
  mTri->getExamineValueRange(val, drv, index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

CarbonStatus CarbonTristateVector1::format(char* valueStr, size_t len, 
                                           CarbonRadix strFormat, 
                                           NetFlags flags, CarbonModel* model) const
{
  const UInt8* val;
  const UInt8* xdrive;
  const UInt8* idrive;
  getValuePtrs(&val, &xdrive, &idrive);
  
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  bool pulled = isPulled(flags);
  
  return doTristateFormatPrim(valueStr, len, strFormat, 
                              val, xdrive, idrive, (UInt8*) NULL, mControlMask, bitWidth, pulled, model);
}

CarbonStatus CarbonTristateVector1::formatForce(char* valueStr, size_t len, 
                                                CarbonRadix strFormat, 
                                                NetFlags flags,
                                                ShellNet* forceMask, 
                                                CarbonModel* model) const
{
  CarbonVector1* castForce = (CarbonVector1*) forceMask;
  const UInt8* forceMaskVec = castForce->getExamineStore();
  const UInt8* val;
  const UInt8* xdrive;
  const UInt8* idrive;
  getValuePtrs(&val, &xdrive, &idrive);
  
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  bool pulled = isPulled(flags);
  
  return doTristateFormatPrim(valueStr, len, strFormat, 
                              val, xdrive, idrive, forceMaskVec, mControlMask, bitWidth, pulled, model);
}

void CarbonTristateVector1::calcValue(UInt32* val, UInt32* drv) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getCalculatedValue(val, drv, bitWidth);
}

void CarbonTristateVector1::calcValueWord(UInt32* val, UInt32* drv, int) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getCalculatedValue(val, drv, bitWidth);
}

void CarbonTristateVector1::calcValuePrim(UInt8* val, UInt8* drv) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getCalculatedValuePrim(val, drv, bitWidth);
}

void CarbonTristateVector1::getValuePtrs(const UInt8** val, const UInt8** xdrv, const UInt8** idrv) const
{
  mTri->getValuePtrs(val, xdrv, idrv);
}

void CarbonTristateVector1::examineXVals(UInt32* val, UInt32* drive) const
{
  mTri->getExternalValues(val, drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

bool CarbonTristateVector1::assignValue(const UInt32* val, const UInt32* drv)
{
  return mTri->assign(val, drv, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}


bool CarbonTristateVector1::assignValueWord(UInt32 val, UInt32 drv, int index)
{
  return mTri->assignWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

bool CarbonTristateVector1::assignValueRange(const UInt32* val, 
                                             const UInt32* drv, 
                                             size_t index, 
                                             size_t length)
{
  return mTri->assignRange(val, drv, index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

ShellNet::ValueState 
CarbonTristateVector1::compareValue(const UInt8* val, const UInt8* drv) const
{
  ShellNet::ValueState stat = eUnchanged;
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  if (mTri->compareCalcValue(val, drv, bitWidth) != 0)
    stat = eChanged;
  return stat;
}

ShellNet::Storage CarbonTristateVector1::allocShadow() const
{
  UInt8 dummy = 0;
  CarbonTriShadowPrim<UInt8>* shadow = new CarbonTriShadowPrim<UInt8>(dummy);
  ShellNet::Storage s = static_cast<ShellNet::Storage>(shadow);
  update(&s);
  return s;
}
  
  //! Free space allocated with allocShadow()
void CarbonTristateVector1::freeShadow(ShellNet::Storage* shadow) {
  CarbonTriShadowPrim<UInt8>* typedShadow = static_cast<CarbonTriShadowPrim<UInt8>*>(*shadow);
  delete typedShadow;
  *shadow = NULL;
}

CarbonTristateVector2::CarbonTristateVector2(UInt16* idata, UInt16* idrive, 
                                             UInt16* xdata, UInt16* xdrive)
{
  mTri  = new ShellNetBidirect2(idata, idrive, xdata, xdrive);
}

//! constructor for tristate vectors <= 8 bits
CarbonTristateVector2::CarbonTristateVector2(UInt16* idata, UInt16* idrive)
{
  mTri  = new ShellNetTristate2(idata, idrive);
}

CarbonTristateVector2::~CarbonTristateVector2()
{
  delete mTri;
}

void CarbonTristateVector2::setConstantBits()
{
  mTri->setConstantBits(mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector2::getTraits(Traits* traits) const
{
  CarbonTristateVector::getTraits(traits);
  traits->putTriShort(mTri);
}

bool CarbonTristateVector2::setToDriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->assertExternalDrive(bitWidth);
}

bool CarbonTristateVector2::setToUndriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->deassertExternalDrive(bitWidth);
}

bool CarbonTristateVector2::resolveXdrive(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->resolveExternalDrive(bitWidth);
}

bool CarbonTristateVector2::setWordToUndriven(int index, CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->deassertExternalDriveWord(index, bitWidth);
}

bool CarbonTristateVector2::setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  bool changed = false;
  if (stat == eCarbon_OK)
  {
    model->getHookup()->setSeenDeposit();
    int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
    changed = mTri->deassertExternalDriveRange(index, length, bitWidth);
  }
  return changed;
}

void CarbonTristateVector2::setRawToUndriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->undrivenInit(bitWidth);
}

void CarbonTristateVector2::doFastDeposit(const UInt32* buf, const UInt32* drive,
                                          CarbonModel* model)
{
  bool changed = false;
  if (! drive)
    changed = setToDriven(model);
  
  changed |= assignValue(buf, drive);
  doUpdateVHM(changed, model);
}

void CarbonTristateVector2::doFastDepositWord(UInt32 buf, int index, UInt32 drive,
                                              CarbonModel* model)
{
  bool changed = assignValueWord(buf, drive, index);
  doUpdateVHM(changed, model);
}

CarbonStatus CarbonTristateVector2::doDepositWord(UInt32 buf, int index,
                                                  UInt32 drive, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
    doFastDepositWord(buf, index, drive, model);
  return stat;
}

CarbonStatus CarbonTristateVector2::doFastDepositRange(const UInt32* buf,
                                                       int range_msb,
                                                       int range_lsb,
                                                       const UInt32* drive,
                                                       CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK) {
    bool changed = assignValueRange(buf, drive, index, length);
    doUpdateVHM(changed, model);
  }
  return stat;
}

CarbonStatus CarbonTristateVector2::deposit(const UInt32* buf,
                                            const UInt32* drive, CarbonModel* model)
{
  // must set drive first so we can use it
  const ShellDataBOM* bom;  
  if (isDepositAllowed(model, &bom))
  {
    doFastDeposit(buf, drive, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonTristateVector2::depositWord(UInt32 buf, int index, 
                                                UInt32 drive, CarbonModel* model) 
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doDepositWord(buf, index, drive, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonTristateVector2::depositRange(const UInt32 *buf, 
                                                 int range_msb, 
                                                 int range_lsb, 
                                                 const UInt32* drive, CarbonModel* model) 
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    return doFastDepositRange(buf, range_msb, range_lsb, drive, model);
  }
  return eCarbon_ERROR;
}

void CarbonTristateVector2::fastDeposit(const UInt32* buf,
                                        const UInt32* drive, CarbonModel* model)
{
  doFastDeposit(buf, drive, model);
}

void CarbonTristateVector2::fastDepositWord(UInt32 buf, int index, 
                                            UInt32 drive, CarbonModel* model) 
{
  doFastDepositWord(buf, index, drive, model);
}

void CarbonTristateVector2::fastDepositRange(const UInt32 *buf, 
                                             int range_msb, 
                                             int range_lsb, 
                                             const UInt32* drive, CarbonModel* model) 
{
  doFastDepositRange(buf, range_msb, range_lsb, drive, model);
}

CarbonStatus CarbonTristateVector2::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  switch(mode)
  {
  case eIDrive:
    examineValue(buf, drive);
    break;
  case eCalcDrive:
    calcValue(buf, drive);
    break;
  case eXDrive:
    examineXVals(buf, drive);
    break;
  }

  return eCarbon_OK;
}
  
CarbonStatus CarbonTristateVector2::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
  {
    switch(mode)
    {
    case eIDrive:
      examineValueWord(buf, drive, index);
      break;
    case eCalcDrive:
      calcValueWord(buf, drive, index);
      break;
    case eXDrive:
      examineValXDriveWord(buf, drive, index);
      break;
    }
  }
  return stat;
}

CarbonStatus CarbonTristateVector2::examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
    examineValueRange(buf, drive, index, length);
  
  return stat;
}

void CarbonTristateVector2::update(ShellNet::Storage* shadow) const {
  CarbonTriShadowPrim<UInt16>* dest = static_cast<CarbonTriShadowPrim<UInt16>*>(*shadow);
  calcValuePrim(&dest->mValue, &dest->mDrive);
}
  
void CarbonTristateVector2::updateUnresolved(ShellNet::Storage* shadow) const {
  CarbonTriShadowPrim<UInt16>* dest = static_cast<CarbonTriShadowPrim<UInt16>*>(*shadow);
  dest->mValue = mTri->getIData();
  dest->mDrive = mTri->getIDrive();
}

//! Compare the shadow and the storage values
ShellNet::ValueState 
CarbonTristateVector2::compare(const ShellNet::Storage shadow) const 
{
  CarbonTriShadowPrim<UInt16>* triShadow = static_cast<CarbonTriShadowPrim<UInt16>*>(shadow);
  return compareValue(&triShadow->mValue, &triShadow->mDrive);    
}
  
ShellNet::ValueState 
CarbonTristateVector2::compareUpdateExamineUnresolved(Storage* shadow,
                                                      UInt32* value, UInt32* drive)
{
  // Examine eIDrive
  examineValue(value, drive);

  // Compare and update
  CarbonTriShadowPrim<UInt16>* triShadow = static_cast<CarbonTriShadowPrim<UInt16>*>(*shadow);
  if ((triShadow->mValue != mTri->getIData()) || 
      (triShadow->mDrive != mTri->getIDrive())) {
    // Update the shadow
    triShadow->mValue = mTri->getIData();
    triShadow->mDrive = mTri->getIDrive();
    return eChanged;

  } else {
    return eUnchanged;
  }
}

//! Write value to valueStr if shadow and storage differ
ShellNet::ValueState 
CarbonTristateVector2::writeIfNotEq(char* valueStr, size_t len, 
                                    ShellNet::Storage* shadow, 
                                    NetFlags flags) 
{
  ShellNet::ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    update(shadow);
    format(valueStr, len, eCarbonBin, flags, NULL);
  }
  return state;
}

ShellNet::ValueState 
CarbonTristateVector2::writeIfNotEqForce(char* valueStr, size_t len, 
                                         ShellNet::Storage* shadow, 
                                         NetFlags flags, ShellNet* forceMask) 
{
  ShellNet::ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    update(shadow);
    formatForce(valueStr, len, eCarbonBin, flags, forceMask, NULL);
  }
  return state;
}

CarbonStatus CarbonTristateVector2::examineValXDriveWord(UInt32* val, 
                                                         UInt32* drv, 
                                                         int index) const
{
  CarbonStatus stat = 
    ShellGlobal::carbonTestIndex(index, 0, 0, NULL);
  if (stat == eCarbon_OK)
  {
    copyValAndXDriveWord(val, drv, index);
  }
  return stat;
}

int CarbonTristateVector2::hasDriveConflict() const
{
  return mTri->hasDriveConflict(CBITWIDTH(mRange->getMsb(), mRange->getLsb())) ? 1 : 0;
}

int CarbonTristateVector2::hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const
{
  size_t index, length;
  int ret = 0;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, NULL);
  if (stat == eCarbon_OK)
  {
    if (mTri->hasDriveConflictRange(index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb())))
      ret = 1;
  }
  else
    ret = -1;
  return ret;
}

bool CarbonTristateVector2::isDataNonZero() const
{
  UInt32 val;
  examine(&val, NULL, eIDrive, NULL);
  return (val != 0);
}

void CarbonTristateVector2::copyValAndXDriveWord(UInt32* val, UInt32* drv, 
                                                 int index) const
{
  mTri->copyValXDriveWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector2::examineValue(UInt32* val, UInt32* drv) const
{
  mTri->getExamineValue(val, drv, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector2::examineValueWord(UInt32* val, UInt32* drv, 
                                             int index) const
{
  mTri->getExamineValueWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector2::examineValueRange(UInt32* val, UInt32* drv, 
                                              size_t index, 
                                              size_t length) const
{
  mTri->getExamineValueRange(val, drv, index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

CarbonStatus CarbonTristateVector2::format(char* valueStr, size_t len, 
                                           CarbonRadix strFormat, 
                                           NetFlags flags, 
                                           CarbonModel* model) const
{
  const UInt16* val;
  const UInt16* xdrive;
  const UInt16* idrive;
  getValuePtrs(&val, &xdrive, &idrive);
  
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  bool pulled = isPulled(flags);
  
  return doTristateFormatPrim(valueStr, len, strFormat, 
                              val, xdrive, idrive, (UInt16*) NULL, mControlMask, bitWidth, pulled, model);
}

CarbonStatus CarbonTristateVector2::formatForce(char* valueStr, size_t len, 
                                                CarbonRadix strFormat, 
                                                NetFlags flags,
                                                ShellNet* forceMask, 
                                                CarbonModel* model) const
{
  CarbonVector2* castForce = (CarbonVector2*) forceMask;
  const UInt16* forceMaskVec = castForce->getExamineStore();
  const UInt16* val;
  const UInt16* xdrive;
  const UInt16* idrive;
  getValuePtrs(&val, &xdrive, &idrive);
  
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  bool pulled = isPulled(flags);
  
  return doTristateFormatPrim(valueStr, len, strFormat, 
                              val, xdrive, idrive, forceMaskVec, mControlMask, bitWidth, pulled, model);
}

void CarbonTristateVector2::calcValue(UInt32* val, UInt32* drv) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getCalculatedValue(val, drv, bitWidth);
}

void CarbonTristateVector2::calcValueWord(UInt32* val, UInt32* drv, int) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getCalculatedValue(val, drv, bitWidth);
}

void CarbonTristateVector2::calcValuePrim(UInt16* val, UInt16* drv) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getCalculatedValuePrim(val, drv, bitWidth);
}

void CarbonTristateVector2::getValuePtrs(const UInt16** val, const UInt16** xdrv, const UInt16** idrv) const
{
  mTri->getValuePtrs(val, xdrv, idrv);
}

void CarbonTristateVector2::examineXVals(UInt32* val, UInt32* drive) const
{
  mTri->getExternalValues(val, drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

bool CarbonTristateVector2::assignValue(const UInt32* val, const UInt32* drv)
{
  return mTri->assign(val, drv, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}


bool CarbonTristateVector2::assignValueWord(UInt32 val, UInt32 drv, int index)
{
  return mTri->assignWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

bool CarbonTristateVector2::assignValueRange(const UInt32* val, 
                                             const UInt32* drv, 
                                             size_t index, 
                                             size_t length)
{
  return mTri->assignRange(val, drv, index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

ShellNet::ValueState 
CarbonTristateVector2::compareValue(const UInt16* val, const UInt16* drv) const
{
  ShellNet::ValueState stat = eUnchanged;
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  if (mTri->compareCalcValue(val, drv, bitWidth) != 0)
    stat = eChanged;
  return stat;
}

ShellNet::Storage CarbonTristateVector2::allocShadow() const
{
  UInt16 dummy = 0;
  CarbonTriShadowPrim<UInt16>* shadow = new CarbonTriShadowPrim<UInt16>(dummy);
  ShellNet::Storage s = static_cast<ShellNet::Storage>(shadow);
  update(&s);
  return s;
}
  
  //! Free space allocated with allocShadow()
void CarbonTristateVector2::freeShadow(ShellNet::Storage* shadow) {
  CarbonTriShadowPrim<UInt16>* typedShadow = static_cast<CarbonTriShadowPrim<UInt16>*>(*shadow);
  delete typedShadow;
  *shadow = NULL;
}

CarbonTristateVector4::CarbonTristateVector4(UInt32* idata, UInt32* idrive, 
                                             UInt32* xdata, UInt32* xdrive)
{
  mTri  = new ShellNetBidirect4(idata, idrive, xdata, xdrive);
}

//! constructor for tristate vectors <= 8 bits
CarbonTristateVector4::CarbonTristateVector4(UInt32* idata, UInt32* idrive)
{
  mTri  = new ShellNetTristate4(idata, idrive);
}

CarbonTristateVector4::~CarbonTristateVector4()
{
  delete mTri;
}

void CarbonTristateVector4::setConstantBits()
{
  mTri->setConstantBits(mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector4::getTraits(Traits* traits) const
{
  CarbonTristateVector::getTraits(traits);
  traits->putTriLong(mTri);
}

bool CarbonTristateVector4::setToDriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->assertExternalDrive(bitWidth);
}

bool CarbonTristateVector4::setToUndriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->deassertExternalDrive(bitWidth);
}

bool CarbonTristateVector4::resolveXdrive(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->resolveExternalDrive(bitWidth);
}

bool CarbonTristateVector4::setWordToUndriven(int index, CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->deassertExternalDriveWord(index, bitWidth);
}

bool CarbonTristateVector4::setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  bool changed = false;
  if (stat == eCarbon_OK)
  {
    model->getHookup()->setSeenDeposit();
    int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
    changed = mTri->deassertExternalDriveRange(index, length, bitWidth);
  }
  return changed;
}

void CarbonTristateVector4::setRawToUndriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->undrivenInit(bitWidth);
}


void CarbonTristateVector4::doFastDeposit(const UInt32* buf, const UInt32* drive,
                                          CarbonModel* model)
{
  bool changed = false;
  if (! drive)
    changed = setToDriven(model);
  
  changed |= assignValue(buf, drive);
  doUpdateVHM(changed, model);
}

void CarbonTristateVector4::doFastDepositWord(UInt32 buf, int index,
                                              UInt32 drive,
                                              CarbonModel* model)
{
  bool changed = assignValueWord(buf, drive, index);
  doUpdateVHM(changed, model);
}

CarbonStatus CarbonTristateVector4::doDepositWord(UInt32 buf, int index,
                                                  UInt32 drive,
                                                  CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK) {
    doFastDepositWord(buf, index, drive, model);
  }
  return stat;
}

CarbonStatus CarbonTristateVector4::doFastDepositRange(const UInt32* buf,
                                                       int range_msb,
                                                       int range_lsb,
                                                       const UInt32* drive,
                                                       CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK) {
    bool changed = assignValueRange(buf, drive, index, length);
    doUpdateVHM(changed, model);
  }
  return stat;
}

CarbonStatus CarbonTristateVector4::deposit(const UInt32* buf,
                                            const UInt32* drive,
                                            CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doFastDeposit(buf, drive, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonTristateVector4::depositWord(UInt32 buf, int index, 
                                                UInt32 drive, CarbonModel* model) 
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    return doDepositWord(buf, index, drive, model);
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonTristateVector4::depositRange(const UInt32 *buf, 
                                                 int range_msb, 
                                                 int range_lsb, 
                                                 const UInt32* drive, CarbonModel* model) 
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    return doFastDepositRange(buf, range_msb, range_lsb, drive, model);
  }
  return eCarbon_ERROR;
}


void CarbonTristateVector4::fastDeposit(const UInt32* buf,
                                        const UInt32* drive, CarbonModel* model)
{
  doFastDeposit(buf, drive, model);
}

void CarbonTristateVector4::fastDepositWord(UInt32 buf, int index, 
                                            UInt32 drive, CarbonModel* model) 
{
  doFastDepositWord(buf, index, drive, model);
}

void CarbonTristateVector4::fastDepositRange(const UInt32 *buf, 
                                             int range_msb, 
                                             int range_lsb, 
                                             const UInt32* drive, CarbonModel* model) 
{
  doFastDepositRange(buf, range_msb, range_lsb, drive, model);
}

CarbonStatus CarbonTristateVector4::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  switch(mode)
  {
  case eIDrive:
    examineValue(buf, drive);
    break;
  case eCalcDrive:
    calcValue(buf, drive);
    break;
  case eXDrive:
    examineXVals(buf, drive);
    break;
  }
  return eCarbon_OK;
}

CarbonStatus CarbonTristateVector4::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
  {
    switch(mode)
    {
    case eIDrive:
      examineValueWord(buf, drive, index);
      break;
    case eCalcDrive:
      calcValueWord(buf, drive, index);
      break;
    case eXDrive:
      examineValXDriveWord(buf, drive, index);
      break;
    }
  }
  return stat;
}

CarbonStatus CarbonTristateVector4::examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
    examineValueRange(buf, drive, index, length);
  
  return stat;
}

void CarbonTristateVector4::update(ShellNet::Storage* shadow) const {
  CarbonTriShadowPrim<UInt32>* dest = static_cast<CarbonTriShadowPrim<UInt32>*>(*shadow);
  calcValuePrim(&dest->mValue, &dest->mDrive);
}
  
void CarbonTristateVector4::updateUnresolved(ShellNet::Storage* shadow) const {
  CarbonTriShadowPrim<UInt32>* dest = static_cast<CarbonTriShadowPrim<UInt32>*>(*shadow);
  dest->mValue = mTri->getIData();
  dest->mDrive = mTri->getIDrive();
}

//! Compare the shadow and the storage values
ShellNet::ValueState 
CarbonTristateVector4::compare(const ShellNet::Storage shadow) const 
{
  CarbonTriShadowPrim<UInt32>* triShadow = static_cast<CarbonTriShadowPrim<UInt32>*>(shadow);
  return compareValue(&triShadow->mValue, &triShadow->mDrive);    
}
  
ShellNet::ValueState 
CarbonTristateVector4::compareUpdateExamineUnresolved(Storage* shadow,
                                                      UInt32* value, UInt32* drive)
{
  // Examine eIDrive
  examineValue(value, drive);

  // Compare
  CarbonTriShadowPrim<UInt32>* triShadow = static_cast<CarbonTriShadowPrim<UInt32>*>(*shadow);
  if ((triShadow->mValue != mTri->getIData()) || 
      (triShadow->mDrive != mTri->getIDrive())) {
    // Update the shadow
    triShadow->mValue = mTri->getIData();
    triShadow->mDrive = mTri->getIDrive();
    return eChanged;

  } else {
    return eUnchanged;
  }
}
  
//! Write value to valueStr if shadow and storage differ
ShellNet::ValueState 
CarbonTristateVector4::writeIfNotEq(char* valueStr, size_t len, 
                                    ShellNet::Storage* shadow, 
                                    NetFlags flags) 
{
  ShellNet::ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    update(shadow);
    format(valueStr, len, eCarbonBin, flags, NULL);
  }
  return state;
}

ShellNet::ValueState 
CarbonTristateVector4::writeIfNotEqForce(char* valueStr, size_t len, 
                                         ShellNet::Storage* shadow, 
                                         NetFlags flags, ShellNet* forceMask) 
{
  ShellNet::ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    update(shadow);
    formatForce(valueStr, len, eCarbonBin, flags, forceMask, NULL);
  }
  return state;
}

CarbonStatus CarbonTristateVector4::examineValXDriveWord(UInt32* val, 
                                                         UInt32* drv, 
                                                         int index) const
{
  CarbonStatus stat = 
    ShellGlobal::carbonTestIndex(index, 0, 0, NULL);
  if (stat == eCarbon_OK)
  {
    copyValAndXDriveWord(val, drv, index);
  }
  return stat;
}

int CarbonTristateVector4::hasDriveConflict() const
{
  return mTri->hasDriveConflict(CBITWIDTH(mRange->getMsb(), mRange->getLsb())) ? 1 : 0;
}

int CarbonTristateVector4::hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const
{
  size_t index, length;
  int ret = 0;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, NULL);
  if (stat == eCarbon_OK)
  {
    if (mTri->hasDriveConflictRange(index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb())))
      ret = 1;
  }
  else
    ret = -1;
  return ret;
}

bool CarbonTristateVector4::isDataNonZero() const
{
  UInt32 val;
  examine(&val, NULL, eIDrive, NULL);
  return (val != 0);
}

void CarbonTristateVector4::copyValAndXDriveWord(UInt32* val, UInt32* drv, 
                                                 int index) const
{
  mTri->copyValXDriveWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector4::examineValue(UInt32* val, UInt32* drv) const
{
  mTri->getExamineValue(val, drv, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector4::examineValueWord(UInt32* val, UInt32* drv, 
                                             int index) const
{
  mTri->getExamineValueWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector4::examineValueRange(UInt32* val, UInt32* drv, 
                                              size_t index, 
                                              size_t length) const
{
  mTri->getExamineValueRange(val, drv, index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

CarbonStatus CarbonTristateVector4::format(char* valueStr, size_t len, 
                                           CarbonRadix strFormat, 
                                           NetFlags flags, 
                                           CarbonModel* model) const
{
  const UInt32* val;
  const UInt32* xdrive;
  const UInt32* idrive;
  getValuePtrs(&val, &xdrive, &idrive);
  
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  bool pulled = isPulled(flags);
  
  return doTristateFormatPrim(valueStr, len, strFormat, 
                              val, xdrive, idrive, (UInt32*) NULL, mControlMask, bitWidth, pulled, model);
}

CarbonStatus CarbonTristateVector4::formatForce(char* valueStr, size_t len, 
                                                CarbonRadix strFormat, 
                                                NetFlags flags,
                                                ShellNet* forceMask, 
                                                CarbonModel* model) const
{
  CarbonVector4* castForce = (CarbonVector4*) forceMask;
  const UInt32* forceMaskVec = castForce->getExamineStore();
  const UInt32* val;
  const UInt32* xdrive;
  const UInt32* idrive;
  getValuePtrs(&val, &xdrive, &idrive);
  
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  bool pulled = isPulled(flags);
  
  return doTristateFormatPrim(valueStr, len, strFormat, 
                              val, xdrive, idrive, forceMaskVec, mControlMask, bitWidth, pulled, model);
}

void CarbonTristateVector4::calcValue(UInt32* val, UInt32* drv) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getCalculatedValue(val, drv, bitWidth);
}

void CarbonTristateVector4::calcValueWord(UInt32* val, UInt32* drv, int) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getCalculatedValue(val, drv, bitWidth);
}

void CarbonTristateVector4::calcValuePrim(UInt32* val, UInt32* drv) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getCalculatedValuePrim(val, drv, bitWidth);
}

void CarbonTristateVector4::getValuePtrs(const UInt32** val, const UInt32** xdrv, const UInt32** idrv) const
{
  mTri->getValuePtrs(val, xdrv, idrv);
}

void CarbonTristateVector4::examineXVals(UInt32* val, UInt32* drive) const
{
  mTri->getExternalValues(val, drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}


bool CarbonTristateVector4::assignValue(const UInt32* val, const UInt32* drv)
{
  return mTri->assign(val, drv, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}


bool CarbonTristateVector4::assignValueWord(UInt32 val, UInt32 drv, int index)
{
  return mTri->assignWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

bool CarbonTristateVector4::assignValueRange(const UInt32* val, 
                                             const UInt32* drv, 
                                             size_t index, 
                                             size_t length)
{
  return mTri->assignRange(val, drv, index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

ShellNet::ValueState 
CarbonTristateVector4::compareValue(const UInt32* val, const UInt32* drv) const
{
  ShellNet::ValueState stat = eUnchanged;
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  if (mTri->compareCalcValue(val, drv, bitWidth) != 0)
    stat = eChanged;
  return stat;
}

ShellNet::Storage CarbonTristateVector4::allocShadow() const
{
  UInt32 dummy = 0;
  CarbonTriShadowPrim<UInt32>* shadow = new CarbonTriShadowPrim<UInt32>(dummy);
  ShellNet::Storage s = static_cast<ShellNet::Storage>(shadow);
  update(&s);
  return s;
}
  
  //! Free space allocated with allocShadow()
void CarbonTristateVector4::freeShadow(ShellNet::Storage* shadow) {
  CarbonTriShadowPrim<UInt32>* typedShadow = static_cast<CarbonTriShadowPrim<UInt32>*>(*shadow);
  delete typedShadow;
  *shadow = NULL;
}

CarbonTristateVector8::CarbonTristateVector8(UInt64* idata, UInt64* idrive, 
                                             UInt64* xdata, UInt64* xdrive)
{
  mTri  = new ShellNetBidirect8(idata, idrive, xdata, xdrive);
}

//! constructor for tristate vectors <= 8 bits
CarbonTristateVector8::CarbonTristateVector8(UInt64* idata, UInt64* idrive)
{
  mTri  = new ShellNetTristate8(idata, idrive);
}

CarbonTristateVector8::~CarbonTristateVector8()
{
  delete mTri;
}

void CarbonTristateVector8::setConstantBits()
{
  mTri->setConstantBits(mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector8::getTraits(Traits* traits) const
{
  CarbonTristateVector::getTraits(traits);
  traits->putTriLLong(mTri);
}

bool CarbonTristateVector8::setToDriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->assertExternalDrive(bitWidth);
}

bool CarbonTristateVector8::setToUndriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->deassertExternalDrive(bitWidth);
}

bool CarbonTristateVector8::resolveXdrive(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->resolveExternalDrive(bitWidth);
}

bool CarbonTristateVector8::setWordToUndriven(int index, CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->deassertExternalDriveWord(index, bitWidth);
}

bool CarbonTristateVector8::setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  bool changed = false;
  if (stat == eCarbon_OK)
  {
    model->getHookup()->setSeenDeposit();
    int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
    changed = mTri->deassertExternalDriveRange(index, length, bitWidth);
  }
  return changed;
}

void CarbonTristateVector8::setRawToUndriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->undrivenInit(bitWidth);
}


void CarbonTristateVector8::doFastDeposit(const UInt32* buf, const UInt32* drive,
                                          CarbonModel* model)
{
  bool changed = false;
  if (! drive)
    changed = setToDriven(model);
  
  changed |= assignValue(buf, drive);
  doUpdateVHM(changed, model);
}

void CarbonTristateVector8::doFastDepositWord(UInt32 buf, int index,
                                              UInt32 drive,
                                              CarbonModel* model)
{
  bool changed = assignValueWord(buf, drive, index);
  doUpdateVHM(changed, model);
}

CarbonStatus CarbonTristateVector8::doDepositWord(UInt32 buf, int index,
                                                  UInt32 drive,
                                                  CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, model);
  if (stat == eCarbon_OK) {
    doFastDepositWord(buf, index, drive, model);
  }
  return stat;
}

CarbonStatus CarbonTristateVector8::doFastDepositRange(const UInt32* buf,
                                                       int range_msb,
                                                       int range_lsb,
                                                       const UInt32* drive,
                                                       CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK) {
    bool changed = assignValueRange(buf, drive, index, length);
    doUpdateVHM(changed, model);
  }
  return stat;
}

CarbonStatus CarbonTristateVector8::deposit(const UInt32* buf,
                                            const UInt32* drive, CarbonModel* model)
{
  // must set drive first so we can use it
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doFastDeposit(buf, drive, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonTristateVector8::depositWord(UInt32 buf, int index, 
                                                UInt32 drive, CarbonModel* model) 
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {  
    return doDepositWord(buf, index, drive,model);
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonTristateVector8::depositRange(const UInt32 *buf, 
                                                 int range_msb, 
                                                 int range_lsb, 
                                                 const UInt32* drive, CarbonModel* model) 
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    return doFastDepositRange(buf, range_msb, range_lsb, drive, model);
  }
  return eCarbon_ERROR;
}


void CarbonTristateVector8::fastDeposit(const UInt32* buf,
                                        const UInt32* drive, CarbonModel* model)
{
  doFastDeposit(buf, drive, model);
}

void CarbonTristateVector8::fastDepositWord(UInt32 buf, int index, 
                                            UInt32 drive, CarbonModel* model) 
{
  doFastDepositWord(buf, index, drive, model);
}

void CarbonTristateVector8::fastDepositRange(const UInt32 *buf, 
                                             int range_msb, 
                                             int range_lsb, 
                                             const UInt32* drive, CarbonModel* model) 
{
  doFastDepositRange(buf, range_msb, range_lsb, drive, model);
}

CarbonStatus CarbonTristateVector8::examine(UInt32* buf, UInt32* drive, 
                                            ExamineMode mode, 
                                            CarbonModel*) const
{
  switch(mode)
  {
  case eIDrive:
    examineValue(buf, drive);
    break;
  case eCalcDrive:
    calcValue(buf, drive);
    break;
  case eXDrive:
    examineXVals(buf, drive);
    break;
  }
  return eCarbon_OK;
}
  
CarbonStatus CarbonTristateVector8::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, model);
  if (stat == eCarbon_OK)
  {
    switch(mode)
    {
    case eIDrive:
      examineValueWord(buf, drive, index);
      break;
    case eCalcDrive:
      calcValueWord(buf, drive, index);
      break;
    case eXDrive:
      examineValXDriveWord(buf, drive, index);
      break;
    }
  }
  return stat;
}

CarbonStatus CarbonTristateVector8::examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
    examineValueRange(buf, drive, index, length);
  
  return stat;
}

void CarbonTristateVector8::update(ShellNet::Storage* shadow) const {
  CarbonTriShadowPrim<UInt64>* dest = static_cast<CarbonTriShadowPrim<UInt64>*>(*shadow);
  calcValuePrim(&dest->mValue, &dest->mDrive);
}
  
void CarbonTristateVector8::updateUnresolved(ShellNet::Storage* shadow) const {
  CarbonTriShadowPrim<UInt64>* dest = static_cast<CarbonTriShadowPrim<UInt64>*>(*shadow);
  dest->mValue = mTri->getIData();
  dest->mDrive = mTri->getIDrive();
}

//! Compare the shadow and the storage values
ShellNet::ValueState 
CarbonTristateVector8::compare(const ShellNet::Storage shadow) const 
{
  CarbonTriShadowPrim<UInt64>* triShadow = static_cast<CarbonTriShadowPrim<UInt64>*>(shadow);
  return compareValue(&triShadow->mValue, &triShadow->mDrive);    
}
  
ShellNet::ValueState 
CarbonTristateVector8::compareUpdateExamineUnresolved(Storage* shadow,
                                                      UInt32* value, UInt32* drive)
{
  // Examine eIDrive
  examineValue(value, drive);

  // Compare and uapte
  CarbonTriShadowPrim<UInt64>* triShadow = static_cast<CarbonTriShadowPrim<UInt64>*>(*shadow);
  if ((triShadow->mValue != mTri->getIData()) || 
      (triShadow->mDrive != mTri->getIDrive())) {
    // Update the shadow
    triShadow->mValue = mTri->getIData();
    triShadow->mDrive = mTri->getIDrive();
    return eChanged;

  } else {
    return eUnchanged;
  }
}

//! Write value to valueStr if shadow and storage differ
ShellNet::ValueState 
CarbonTristateVector8::writeIfNotEq(char* valueStr, size_t len, 
                                    ShellNet::Storage* shadow, 
                                    NetFlags flags) 
{
  ShellNet::ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    update(shadow);
    format(valueStr, len, eCarbonBin, flags, NULL);
  }
  return state;
}

ShellNet::ValueState 
CarbonTristateVector8::writeIfNotEqForce(char* valueStr, size_t len, 
                                         ShellNet::Storage* shadow, 
                                         NetFlags flags, ShellNet* forceMask) 
{
  ShellNet::ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    update(shadow);
    formatForce(valueStr, len, eCarbonBin, flags, forceMask, NULL);
  }
  return state;
}

CarbonStatus CarbonTristateVector8::examineValXDriveWord(UInt32* val, 
                                                         UInt32* drv, 
                                                         int index) const
{
  CarbonStatus stat = 
    ShellGlobal::carbonTestIndex(index, 0, 1, NULL);
  if (stat == eCarbon_OK)
  {
    copyValAndXDriveWord(val, drv, index);
  }
  return stat;
}

int CarbonTristateVector8::hasDriveConflict() const
{
  return mTri->hasDriveConflict(CBITWIDTH(mRange->getMsb(), mRange->getLsb())) ? 1 : 0;
}

int CarbonTristateVector8::hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const
{
  size_t index, length;
  int ret = 0;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, NULL);
  if (stat == eCarbon_OK)
  {
    if (mTri->hasDriveConflictRange(index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb())))
      ret = 1;
  }
  else
    ret = -1;
  return ret;
}

bool CarbonTristateVector8::isDataNonZero() const
{
  UInt32 val[2];
  examine(val, NULL, eIDrive, NULL);
  return (CarbonValRW::isZero(val, 2));
}

void CarbonTristateVector8::copyValAndXDriveWord(UInt32* val, UInt32* drv, 
                                                 int index) const
{
  mTri->copyValXDriveWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector8::examineValue(UInt32* val, UInt32* drv) const
{
  mTri->getExamineValue(val, drv, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector8::examineValueWord(UInt32* val, UInt32* drv, 
                                             int index) const
{
  mTri->getExamineValueWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVector8::examineValueRange(UInt32* val, UInt32* drv, 
                                              size_t index, 
                                              size_t length) const
{
  mTri->getExamineValueRange(val, drv, index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

CarbonStatus CarbonTristateVector8::format(char* valueStr, size_t len, 
                                           CarbonRadix strFormat, 
                                           NetFlags flags, 
                                           CarbonModel* model) const
{
  const UInt64* val;
  const UInt64* xdrive;
  const UInt64* idrive;
  getValuePtrs(&val, &xdrive, &idrive);
  
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  bool pulled = isPulled(flags);
  
  return doTristateFormatPrim(valueStr, len, strFormat, 
                              val, xdrive, idrive, (UInt64*) NULL, mControlMask, bitWidth, pulled, model);
}

CarbonStatus CarbonTristateVector8::formatForce(char* valueStr, size_t len, 
                                                CarbonRadix strFormat, 
                                                NetFlags flags,
                                                ShellNet* forceMask, 
                                                CarbonModel* model) const
{
  CarbonVector8* castForce = (CarbonVector8*) forceMask;
  const UInt64* forceMaskVec = castForce->getExamineStore();
  const UInt64* val;
  const UInt64* xdrive;
  const UInt64* idrive;
  getValuePtrs(&val, &xdrive, &idrive);
  
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  bool pulled = isPulled(flags);
  
  return doTristateFormatPrim(valueStr, len, strFormat, 
                              val, xdrive, idrive, forceMaskVec, mControlMask, bitWidth, pulled, model);
}

void CarbonTristateVector8::calcValue(UInt32* val, UInt32* drv) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getCalculatedValue(val, drv, bitWidth);
}

void CarbonTristateVector8::calcValueWord(UInt32* val, UInt32* drv, int index) const
{
  UInt64 val64, drv64;
  calcValuePrim(&val64, &drv64);
  CarbonValRW::cpSrcToDestWord(val, &val64, index);
  CarbonValRW::cpSrcToDestWord(drv, &drv64, index);
}

void CarbonTristateVector8::calcValuePrim(UInt64* val, UInt64* drv) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getCalculatedValuePrim(val, drv, bitWidth);
}

void CarbonTristateVector8::getValuePtrs(const UInt64** val, const UInt64** xdrv, const UInt64** idrv) const
{
  mTri->getValuePtrs(val, xdrv, idrv);
}

void CarbonTristateVector8::examineXVals(UInt32* val, UInt32* drive) const
{
  mTri->getExternalValues(val, drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

bool CarbonTristateVector8::assignValue(const UInt32* val, const UInt32* drv)
{
  return mTri->assign(val, drv, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}


bool CarbonTristateVector8::assignValueWord(UInt32 val, UInt32 drv, int index)
{
  return mTri->assignWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

bool CarbonTristateVector8::assignValueRange(const UInt32* val, 
                                             const UInt32* drv, 
                                             size_t index, 
                                             size_t length)
{
  return mTri->assignRange(val, drv, index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

ShellNet::ValueState 
CarbonTristateVector8::compareValue(const UInt64* val, const UInt64* drv) const
{
  ShellNet::ValueState stat = eUnchanged;
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  if (mTri->compareCalcValue(val, drv, bitWidth) != 0)
    stat = eChanged;
  return stat;
}

ShellNet::Storage CarbonTristateVector8::allocShadow() const
{
  UInt64 dummy = 0;
  CarbonTriShadowPrim<UInt64>* shadow = new CarbonTriShadowPrim<UInt64>(dummy);
  ShellNet::Storage s = static_cast<ShellNet::Storage>(shadow);
  update(&s);
  return s;
}
  
  //! Free space allocated with allocShadow()
void CarbonTristateVector8::freeShadow(ShellNet::Storage* shadow) {
  CarbonTriShadowPrim<UInt64>* typedShadow = static_cast<CarbonTriShadowPrim<UInt64>*>(*shadow);
  delete typedShadow;
  *shadow = NULL;
}

CarbonTristateVectorA::CarbonTristateVectorA(UInt32* idata, UInt32* idrive, 
                                             UInt32* xdata, UInt32* xdrive)
{
  mTri  = new ShellNetBidirectA(idata, idrive, xdata, xdrive);
}

//! constructor for tristate vectors <= 8 bits
CarbonTristateVectorA::CarbonTristateVectorA(UInt32* idata, UInt32* idrive)
{
  mTri  = new ShellNetTristateA(idata, idrive);
}

CarbonTristateVectorA::~CarbonTristateVectorA()
{
  delete mTri;
}

void CarbonTristateVectorA::setConstantBits()
{
  mTri->setConstantBits(mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVectorA::getTraits(Traits* traits) const
{
  CarbonTristateVector::getTraits(traits);
  traits->putTriArray(mTri);
}

bool CarbonTristateVectorA::setToDriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->assertExternalDrive(bitWidth);
}

bool CarbonTristateVectorA::setToUndriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->deassertExternalDrive(bitWidth);
}

bool CarbonTristateVectorA::resolveXdrive(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->resolveExternalDrive(bitWidth);
}

bool CarbonTristateVectorA::setWordToUndriven(int index, CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->deassertExternalDriveWord(index, bitWidth);
}

bool CarbonTristateVectorA::setRangeToUndriven(int range_msb, int range_lsb,
                                               CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  bool changed = false;
  if (stat == eCarbon_OK)
  {
    model->getHookup()->setSeenDeposit();
    int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
    changed = mTri->deassertExternalDriveRange(index, length, bitWidth);
  }
  return changed;
}

void CarbonTristateVectorA::setRawToUndriven(CarbonModel* model)
{
  model->getHookup()->setSeenDeposit();
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->undrivenInit(bitWidth);
}


void CarbonTristateVectorA::doFastDeposit(const UInt32* buf, const UInt32* drive,
                                          CarbonModel* model)
{
  bool changed = false;
  if (! drive)
    changed = setToDriven(model);
  
  changed |= assignValue(buf, drive);
  doUpdateVHM(changed, model);
}

void CarbonTristateVectorA::doFastDepositWord(UInt32 buf, int index, 
                                              UInt32 drive,
                                              CarbonModel* model)
{
  bool changed = assignValueWord(buf, drive, index);
  doUpdateVHM(changed, model);
}

CarbonStatus CarbonTristateVectorA::doDepositWord(UInt32 buf, int index, 
                                                  UInt32 drive,
                                                  CarbonModel* model)
{
  int numWords = getNumUInt32s();
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
  if (stat == eCarbon_OK) {
    doFastDepositWord(buf, index, drive, model);
  }
  return stat;
}

CarbonStatus CarbonTristateVectorA::doFastDepositRange(const UInt32* buf,
                                                       int range_msb,
                                                       int range_lsb,
                                                       const UInt32* drive,
                                                       CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK) {
    bool changed = assignValueRange(buf, drive, index, length);
    doUpdateVHM(changed, model);
  }
  return stat;
}

CarbonStatus CarbonTristateVectorA::deposit(const UInt32* buf,
                                            const UInt32* drive, CarbonModel* model)
{
  // must set drive first so we can use it
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doFastDeposit(buf, drive, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonTristateVectorA::depositWord(UInt32 buf, int index, 
                                                UInt32 drive, CarbonModel* model) 
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    return doDepositWord(buf, index, drive, model);
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonTristateVectorA::depositRange(const UInt32 *buf, 
                                                 int range_msb, 
                                                 int range_lsb, 
                                                 const UInt32* drive, CarbonModel* model) 
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    return doFastDepositRange(buf, range_msb, range_lsb, drive, model);
  }
  return eCarbon_ERROR;
}

void CarbonTristateVectorA::fastDeposit(const UInt32* buf,
                                        const UInt32* drive, CarbonModel* model)
{
  doFastDeposit(buf, drive, model);
}

void CarbonTristateVectorA::fastDepositWord(UInt32 buf, int index, 
                                            UInt32 drive, CarbonModel* model) 
{
  doFastDepositWord(buf, index, drive, model);
}

void CarbonTristateVectorA::fastDepositRange(const UInt32 *buf, 
                                             int range_msb, 
                                             int range_lsb, 
                                             const UInt32* drive, CarbonModel* model) 
{
  doFastDepositRange(buf, range_msb, range_lsb, drive, model);
}

CarbonStatus CarbonTristateVectorA::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  switch(mode)
  {
  case eIDrive:
    examineValue(buf, drive);
    break;
  case eCalcDrive:
    calcValue(buf, drive);
    break;
  case eXDrive:
    examineXVals(buf, drive);
    break;
  }
  return eCarbon_OK;
}
  
CarbonStatus CarbonTristateVectorA::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  int numWords = getNumUInt32s();
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
  if (stat == eCarbon_OK)
  {
    switch(mode)
    {
    case eIDrive:
      examineValueWord(buf, drive, index);
      break;
    case eCalcDrive:
      calcValueWord(buf, drive, index);
      break;
    case eXDrive:
      examineValXDriveWord(buf, drive, index);
      break;
    }
  }
  return stat;
}

CarbonStatus CarbonTristateVectorA::examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
    examineValueRange(buf, drive, index, length);
  
  return stat;
}

void CarbonTristateVectorA::update(ShellNet::Storage* shadow) const {
  CarbonTriValShadow* dest = static_cast<CarbonTriValShadow*>(*shadow);
  calcValue(dest->mValue, dest->mDrive);
}
  
void CarbonTristateVectorA::updateUnresolved(ShellNet::Storage* shadow) const {
  CarbonTriValShadow* dest = static_cast<CarbonTriValShadow*>(*shadow);
  size_t numWords = getNumUInt32s();
  const UInt32* value = mTri->getIData();
  const UInt32* drive = mTri->getIDrive();
  for (size_t i = 0; (i < numWords); ++i) {
    dest->mValue[i] = value[i];
    dest->mDrive[i] = drive[i];
  }
}

//! Compare the shadow and the storage values
ShellNet::ValueState 
CarbonTristateVectorA::compare(const ShellNet::Storage shadow) const 
{
  CarbonTriValShadow* triShadow = static_cast<CarbonTriValShadow*>(shadow);
  return compareValue(triShadow->mValue, triShadow->mDrive);    
}
  
ShellNet::ValueState 
CarbonTristateVectorA::compareUpdateExamineUnresolved(Storage* shadow,
                                                      UInt32* value, UInt32* drive)
{
  // Examine eIDrive
  examineValue(value, drive);

  // Compare and update. Note that we don't stop when we get a diff
  // because we have to update and we only update the words that
  // changed.
  CarbonTriValShadow* triShadow = static_cast<CarbonTriValShadow*>(*shadow);
  ValueState state = eUnchanged;
  size_t numWords = getNumUInt32s();
  const UInt32* actualValue = mTri->getIData();
  const UInt32* actualDrive = mTri->getIDrive();
  for (size_t i = 0; i < numWords; ++i) {
    if ((triShadow->mValue[i] != actualValue[i]) || 
        (triShadow->mDrive[i] != actualDrive[i])) {
      // Update the shadow
      triShadow->mValue[i] = actualValue[i];
      triShadow->mDrive[i] = actualDrive[i];
      state = eChanged;
    }
  }
  return state;
}
  
//! Write value to valueStr if shadow and storage differ
ShellNet::ValueState 
CarbonTristateVectorA::writeIfNotEq(char* valueStr, size_t len, 
                                    ShellNet::Storage* shadow, 
                                    NetFlags flags) 
{
  ShellNet::ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    update(shadow);
    format(valueStr, len, eCarbonBin, flags, NULL);
  }
  return state;
}

ShellNet::ValueState 
CarbonTristateVectorA::writeIfNotEqForce(char* valueStr, size_t len, 
                                         ShellNet::Storage* shadow, 
                                         NetFlags flags, ShellNet* forceMask) 
{
  ShellNet::ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    update(shadow);
    formatForce(valueStr, len, eCarbonBin, flags, forceMask, NULL);
  }
  return state;
}

CarbonStatus CarbonTristateVectorA::examineValXDriveWord(UInt32* val, 
                                                         UInt32* drv, 
                                                         int index) const
{
  int numWords = getNumUInt32s();
  CarbonStatus stat = 
    ShellGlobal::carbonTestIndex(index, 0, numWords - 1, NULL);
  if (stat == eCarbon_OK)
  {
    copyValAndXDriveWord(val, drv, index);
  }
  return stat;
}

int CarbonTristateVectorA::hasDriveConflict() const
{
  return mTri->hasDriveConflict(CBITWIDTH(mRange->getMsb(), mRange->getLsb())) ? 1 : 0;
}

int CarbonTristateVectorA::hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const
{
  size_t index, length;
  int ret = 0;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, NULL);
  if (stat == eCarbon_OK)
  {
    if (mTri->hasDriveConflictRange(index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb())))
      ret = 1;
  }
  else
    ret = -1;
  return ret;
}

bool CarbonTristateVectorA::isDataNonZero() const
{
  const UInt32* value = getExamineStore();
  return (! CarbonValRW::isZero(value, getNumUInt32s()));
}

void CarbonTristateVectorA::copyValAndXDriveWord(UInt32* val, UInt32* drv, 
                                                 int index) const
{
  mTri->copyValXDriveWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVectorA::examineValue(UInt32* val, UInt32* drv) const
{
  mTri->getExamineValue(val, drv, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVectorA::examineValueWord(UInt32* val, UInt32* drv, 
                                             int index) const
{
  mTri->getExamineValueWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonTristateVectorA::examineValueRange(UInt32* val, UInt32* drv, 
                                              size_t index, 
                                              size_t length) const
{
  mTri->getExamineValueRange(val, drv, index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

CarbonStatus CarbonTristateVectorA::format(char* valueStr, size_t len, 
                                           CarbonRadix strFormat, 
                                           NetFlags flags, 
                                           CarbonModel* model) const
{

  const UInt32* val;
  const UInt32* xdrive;
  const UInt32* idrive;
  getValuePtrs(&val, &xdrive, &idrive);
  
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  bool pulled = isPulled(flags);
  
  return doTristateFormatPrim(valueStr, len, strFormat, 
                              val, xdrive, idrive, (UInt32*) NULL, mControlMask, bitWidth, pulled, model);
}

CarbonStatus CarbonTristateVectorA::formatForce(char* valueStr, size_t len, 
                                                CarbonRadix strFormat, 
                                                NetFlags flags,
                                                ShellNet* forceMask, 
                                                CarbonModel* model) const
{
  CarbonVectorA* castForce = (CarbonVectorA*) forceMask;
  const UInt32* forceMaskVec = castForce->getExamineStore();
  const UInt32* val;
  const UInt32* xdrive;
  const UInt32* idrive;
  getValuePtrs(&val, &xdrive, &idrive);
  
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  bool pulled = isPulled(flags);
  
  return doTristateFormatPrim(valueStr, len, strFormat, 
                              val, xdrive, idrive, forceMaskVec, mControlMask, bitWidth, pulled, model);
}

void CarbonTristateVectorA::calcValue(UInt32* val, UInt32* drv) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getCalculatedValue(val, drv, bitWidth);
  // don't need to mask because bitvectors did that for us.
}

void CarbonTristateVectorA::calcValueWord(UInt32* val, UInt32* drv, int index) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  mTri->getExamineValueWord(val, drv, index, bitWidth);
  *drv = mTri->getCalcDriveWord(index, bitWidth);
}

void CarbonTristateVectorA::getValuePtrs(const UInt32** val, const UInt32** xdrv, const UInt32** idrv) const
{
  mTri->getValuePtrs(val, xdrv, idrv);
}

void CarbonTristateVectorA::examineXVals(UInt32* val, UInt32* drive) const
{
  mTri->getExternalValues(val, drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

UInt32 CarbonTristateVectorA::getCalcDriveWord(UInt32 index) const
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return mTri->getCalcDriveWord(index, bitWidth);
}

bool CarbonTristateVectorA::assignValue(const UInt32* val, const UInt32* drv)
{
  return mTri->assign(val, drv, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}


bool CarbonTristateVectorA::assignValueWord(UInt32 val, UInt32 drv, int index)
{
  return mTri->assignWord(val, drv, index, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

bool CarbonTristateVectorA::assignValueRange(const UInt32* val, 
                                             const UInt32* drv, 
                                             size_t index, 
                                             size_t length)
{
  return mTri->assignRange(val, drv, index, length, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

ShellNet::ValueState 
CarbonTristateVectorA::compareValue(const UInt32* val, const UInt32* drv) const
{
  ShellNet::ValueState stat = eUnchanged;
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  if (mTri->compareCalcValue(val, drv, bitWidth) != 0)
    stat = eChanged;
  return stat;
}

ShellNet::Storage CarbonTristateVectorA::allocShadow() const
{
  int numWords = getNumUInt32s();
  CarbonTriValShadow* shadow = new CarbonTriValShadow(numWords);
  ShellNet::Storage s = static_cast<ShellNet::Storage>(shadow);
  
  update(&s);
  return s;
}
  
  //! Free space allocated with allocShadow()
void CarbonTristateVectorA::freeShadow(ShellNet::Storage* shadow) {
  CarbonTriValShadow* typedShadow = static_cast<CarbonTriValShadow*>(*shadow);
  delete typedShadow;
  *shadow = NULL;
}

const UInt32* CarbonTristateVectorA::getExamineStore() const
{
  return mTri->getIData();
}

CarbonTristateVector1Input::CarbonTristateVector1Input(UInt8* idata, 
                                                       UInt8* idrive, 
                                                       UInt8* xdata, 
                                                       UInt8* xdrive) :
  CarbonTristateVector1(idata, idrive, xdata, xdrive), mChangeArrayRef(NULL)
{}
  
CarbonTristateVector1Input::CarbonTristateVector1Input(UInt8* idata, 
                                                       UInt8* idrive) :
  CarbonTristateVector1(idata, idrive), mChangeArrayRef(NULL)
{}
  
CarbonTristateVector1Input::~CarbonTristateVector1Input()
{}

void CarbonTristateVector1Input::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  mChangeArrayRef = changeArrayRef;
}

CarbonChangeType* CarbonTristateVector1Input::getChangeArrayRef()
{
  return mChangeArrayRef;
}

void CarbonTristateVector1Input::calcChangeMask()
{
  if (CBITWIDTH(mRange->getMsb(), mRange->getLsb()) == 1)
  {
    UInt8 val, drv;
    mTri->getXValues(&val, &drv, 1);
    int fall = (drv == 0) & (val == 0);
    int rise = (drv == 0) & (val == 1);
    int noXDrv = (drv != 0);
    *mChangeArrayRef = 
      fall * CARBON_CHANGE_FALL_MASK +
      rise * CARBON_CHANGE_RISE_MASK +
      noXDrv * CARBON_CHANGE_MASK;
  }
  else
    *mChangeArrayRef = CARBON_CHANGE_MASK;
}

void CarbonTristateVector1Input::recomputeChangeMask()
{
  calcChangeMask();
}

void CarbonTristateVector1Input::doFastDepositInput(const UInt32* buf,
                                                    const UInt32* drive,
                                                    CarbonModel* model)
{
  bool changed = false; 
  if (! drive)
    changed = setToDriven(model);

  changed |= assignValue(buf, drive);
  doUpdateVHM(changed, model);
  if (changed) {
    calcChangeMask();
  }
}

void CarbonTristateVector1Input::doFastDepositWordInput(UInt32 buf, int index,
                                                        UInt32 drive,
                                                        CarbonModel* model)
{
  bool changed = assignValueWord(buf, drive, index);
  doUpdateVHM(changed, model);
  if (changed) {
    calcChangeMask();
  }
}

CarbonStatus CarbonTristateVector1Input::doDepositWordInput(UInt32 buf, int index,
                                                            UInt32 drive,
                                                            CarbonModel* model)
{
  int numWords = getNumUInt32s();
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
  if (stat == eCarbon_OK) {
    doFastDepositWordInput(buf, index, drive, model);
  }
  return stat;
}

CarbonStatus CarbonTristateVector1Input::doFastDepositRangeInput(const UInt32* buf,
                                                                 int range_msb,
                                                                 int range_lsb,
                                                                 const UInt32* drive,
                                                                 CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK) {
    bool changed = assignValueRange(buf, drive, index, length);
    doUpdateVHM(changed, model);
    if (changed) {
      calcChangeMask();
    }
  }
  return stat;
}



CarbonStatus CarbonTristateVector1Input::deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  doFastDepositInput(buf, drive, model);
  return eCarbon_OK;
}

CarbonStatus CarbonTristateVector1Input::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  return doDepositWordInput(buf, index, drive, model);
}

CarbonStatus CarbonTristateVector1Input::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  return doFastDepositRangeInput(buf, range_msb, range_lsb, drive, model);
}

void CarbonTristateVector1Input::fastDeposit(const UInt32* buf,
                                             const UInt32* drive,
                                             CarbonModel* model)
{
  doFastDepositInput(buf, drive, model);
}

void CarbonTristateVector1Input::fastDepositWord(UInt32 buf, int index, 
                                                 UInt32 drive, CarbonModel* model) 
{
  doFastDepositWordInput(buf, index, drive, model);
}

void CarbonTristateVector1Input::fastDepositRange(const UInt32 *buf, 
                                                  int range_msb, 
                                                  int range_lsb, 
                                                  const UInt32* drive, CarbonModel* model)  
{
  doFastDepositRangeInput(buf, range_msb, range_lsb, drive, model);
}

bool CarbonTristateVector1Input::setToUndriven(CarbonModel* model)
{
  bool changed = CarbonTristateVector1::setToUndriven(model);
  if ((changed & (mChangeArrayRef != NULL)) != 0)
    // not driving from outside, so just set as changed
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  return changed;
}

bool CarbonTristateVector1Input::resolveXdrive(CarbonModel* model)
{
  bool changed = CarbonTristateVector1::resolveXdrive(model);
  if ((changed & (mChangeArrayRef != NULL)) != 0)
    calcChangeMask();
  return changed;
}

void CarbonTristateVector1Input::getTraits(Traits* traits) const
{
  CarbonTristateVector1::getTraits(traits);
  traits->mHasInputSemantics = true;
}

bool CarbonTristateVector1Input::isInput() const
{
  return true;
}

void CarbonTristateVector1Input::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
    *xdrive = mTri->getXDrive(CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

CarbonTristateVector2Input::CarbonTristateVector2Input(UInt16* idata, 
                                                       UInt16* idrive, 
                                                       UInt16* xdata, 
                                                       UInt16* xdrive) :
  CarbonTristateVector2(idata, idrive, xdata, xdrive), mChangeArrayRef(NULL)
{}
  
CarbonTristateVector2Input::CarbonTristateVector2Input(UInt16* idata, 
                                                       UInt16* idrive) :
  CarbonTristateVector2(idata, idrive), mChangeArrayRef(NULL)
{}
  
CarbonTristateVector2Input::~CarbonTristateVector2Input()
{}

void CarbonTristateVector2Input::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  mChangeArrayRef = changeArrayRef;
}

CarbonChangeType* CarbonTristateVector2Input::getChangeArrayRef()
{
  return mChangeArrayRef;
}

void CarbonTristateVector2Input::doFastDepositInput(const UInt32* buf,
                                                    const UInt32* drive,
                                                    CarbonModel* model)
{
  bool changed = false; 
  if (! drive)
    changed = setToDriven(model);

  changed |= assignValue(buf, drive);
  doUpdateVHM(changed, model);
  if (changed) {
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  }
}

void CarbonTristateVector2Input::doFastDepositWordInput(UInt32 buf, int index,
                                                        UInt32 drive,
                                                        CarbonModel* model)
{
  bool changed = assignValueWord(buf, drive, index);
  doUpdateVHM(changed, model);
  if (changed) {
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  }
}

CarbonStatus CarbonTristateVector2Input::doDepositWordInput(UInt32 buf, int index,
                                                            UInt32 drive,
                                                            CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
    doFastDepositWordInput(buf, index, drive, model);
  return stat;
}

CarbonStatus CarbonTristateVector2Input::doFastDepositRangeInput(const UInt32* buf,
                                                                 int range_msb,
                                                                 int range_lsb,
                                                                 const UInt32* drive,
                                                                 CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK) {
    bool changed = assignValueRange(buf, drive, index, length);
    doUpdateVHM(changed, model);
    if (changed) {
      *mChangeArrayRef = CARBON_CHANGE_MASK;
    }
  }
  return stat;
}

CarbonStatus CarbonTristateVector2Input::deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  doFastDepositInput(buf, drive, model);
  return eCarbon_OK;
}

CarbonStatus CarbonTristateVector2Input::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  return doDepositWordInput(buf, index, drive, model);
}

CarbonStatus CarbonTristateVector2Input::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  return doFastDepositRangeInput(buf, range_msb, range_lsb, drive, model);
}


void CarbonTristateVector2Input::fastDeposit(const UInt32* buf, 
                                             const UInt32* drive,
                                             CarbonModel* model)
{
  doFastDepositInput(buf, drive, model);
}

void CarbonTristateVector2Input::fastDepositWord(UInt32 buf, int index, 
                                                 UInt32 drive, CarbonModel* model) 
{
  doFastDepositWordInput(buf, index, drive, model);
}

void CarbonTristateVector2Input::fastDepositRange(const UInt32 *buf, 
                                                  int range_msb, 
                                                  int range_lsb, 
                                                  const UInt32* drive, CarbonModel* model) 
{
  doFastDepositRange(buf, range_msb, range_lsb, drive, model);
}

bool CarbonTristateVector2Input::setToUndriven(CarbonModel* model)
{
  bool changed = CarbonTristateVector2::setToUndriven(model);
  if ((changed & (mChangeArrayRef != NULL)) != 0)
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  return changed;
}

bool CarbonTristateVector2Input::resolveXdrive(CarbonModel* model)
{
  bool changed = CarbonTristateVector2::resolveXdrive(model);
  if ((changed & (mChangeArrayRef != NULL)) != 0)
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  return changed;
}

void CarbonTristateVector2Input::getTraits(Traits* traits) const
{
  CarbonTristateVector2::getTraits(traits);
  traits->mHasInputSemantics = true;
}

bool CarbonTristateVector2Input::isInput() const
{
  return true;
}

void CarbonTristateVector2Input::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
    *xdrive = mTri->getXDrive(CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

CarbonTristateVector4Input::CarbonTristateVector4Input(UInt32* idata, 
                                                       UInt32* idrive, 
                                                       UInt32* xdata, 
                                                       UInt32* xdrive) :
  CarbonTristateVector4(idata, idrive, xdata, xdrive), mChangeArrayRef(NULL)
{}
  
CarbonTristateVector4Input::CarbonTristateVector4Input(UInt32* idata, 
                                                       UInt32* idrive) :
  CarbonTristateVector4(idata, idrive), mChangeArrayRef(NULL)
{}
  
CarbonTristateVector4Input::~CarbonTristateVector4Input()
{}

void CarbonTristateVector4Input::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  mChangeArrayRef = changeArrayRef;
}

CarbonChangeType* CarbonTristateVector4Input::getChangeArrayRef()
{
  return mChangeArrayRef;
}

void CarbonTristateVector4Input::doFastDepositInput(const UInt32* buf,
                                                    const UInt32* drive,
                                                    CarbonModel* model)
{
  bool changed = false;
  if (! drive)
    changed = setToDriven(model);
  
  changed |= assignValue(buf, drive);
  doUpdateVHM(changed, model);
  if (changed) {
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  }
}

void CarbonTristateVector4Input::doFastDepositWordInput(UInt32 buf,
                                                        int index,
                                                        UInt32 drive,
                                                        CarbonModel* model)
{
  bool changed = assignValueWord(buf, drive, index);
  doUpdateVHM(changed, model);
  if (changed) {
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  }
}

CarbonStatus CarbonTristateVector4Input::doDepositWordInput(UInt32 buf,
                                                            int index,
                                                            UInt32 drive,
                                                            CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK) {
    doFastDepositWordInput(buf, index, drive, model);
  }
  return stat;
}

CarbonStatus CarbonTristateVector4Input::doFastDepositRangeInput(const UInt32* buf,
                                                                 int range_msb,
                                                                 int range_lsb,
                                                                 const UInt32* drive,
                                                                 CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK) {
    bool changed = assignValueRange(buf, drive, index, length);
    doUpdateVHM(changed, model);
    if (changed) {
      *mChangeArrayRef = CARBON_CHANGE_MASK;
    }
  }
  return stat;
}

CarbonStatus CarbonTristateVector4Input::deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  doFastDepositInput(buf, drive, model);
  return eCarbon_OK;
}

CarbonStatus CarbonTristateVector4Input::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  return doDepositWordInput(buf, index, drive, model);
}

CarbonStatus CarbonTristateVector4Input::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  return doFastDepositRangeInput(buf, range_msb, range_lsb, drive, model);
}


void CarbonTristateVector4Input::fastDeposit(const UInt32* buf,
                                             const UInt32* drive,
                                             CarbonModel* model)
{
  doFastDepositInput(buf, drive, model);
}

void CarbonTristateVector4Input::fastDepositWord(UInt32 buf, int index, 
                                                 UInt32 drive, CarbonModel* model)
{
  doFastDepositWordInput(buf, index, drive, model);
}

void CarbonTristateVector4Input::fastDepositRange(const UInt32 *buf, 
                                                  int range_msb, 
                                                  int range_lsb, 
                                                  const UInt32* drive, CarbonModel* model) 
{
  doFastDepositRangeInput(buf, range_msb, range_lsb, drive, model);
}

bool CarbonTristateVector4Input::setToUndriven(CarbonModel* model)
{
  bool changed = CarbonTristateVector4::setToUndriven(model);
  if ((changed & (mChangeArrayRef != NULL)) != 0)
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  return changed;
}

bool CarbonTristateVector4Input::resolveXdrive(CarbonModel* model)
{
  bool changed = CarbonTristateVector4::resolveXdrive(model);
  if ((changed & (mChangeArrayRef != NULL)) != 0)
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  return changed;
}

void CarbonTristateVector4Input::getTraits(Traits* traits) const
{
  CarbonTristateVector4::getTraits(traits);
  traits->mHasInputSemantics = true;
}

bool CarbonTristateVector4Input::isInput() const
{
  return true;
}

void CarbonTristateVector4Input::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
    *xdrive = mTri->getXDrive(CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

CarbonTristateVector8Input::CarbonTristateVector8Input(UInt64* idata, 
                                                       UInt64* idrive, 
                                                       UInt64* xdata, 
                                                       UInt64* xdrive) :
  CarbonTristateVector8(idata, idrive, xdata, xdrive), mChangeArrayRef(NULL)
{}
  
CarbonTristateVector8Input::CarbonTristateVector8Input(UInt64* idata, 
                                                       UInt64* idrive) :
  CarbonTristateVector8(idata, idrive), mChangeArrayRef(NULL)
{}
  
CarbonTristateVector8Input::~CarbonTristateVector8Input()
{}

void CarbonTristateVector8Input::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  mChangeArrayRef = changeArrayRef;
}

CarbonChangeType* CarbonTristateVector8Input::getChangeArrayRef()
{
  return mChangeArrayRef;
}

void CarbonTristateVector8Input::doFastDepositInput(const UInt32* buf,
                                                    const UInt32* drive,
                                                    CarbonModel* model)
{
  bool changed = false;
  if (! drive)
    changed = setToDriven(model);
  
  changed |= assignValue(buf, drive);
  doUpdateVHM(changed, model);
  if (changed) {
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  }
}

void CarbonTristateVector8Input::doFastDepositWordInput(UInt32 buf, int index,
                                                        UInt32 drive,
                                                        CarbonModel* model)
{
  bool changed = assignValueWord(buf, drive, index);
  doUpdateVHM(changed, model);
  if (changed) {
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  }
}

CarbonStatus CarbonTristateVector8Input::doDepositWordInput(UInt32 buf,
                                                            int index,
                                                            UInt32 drive,
                                                            CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, model);
  if (stat == eCarbon_OK) {
    doFastDepositWordInput(buf, index, drive, model);
  }
  return stat;
}

CarbonStatus CarbonTristateVector8Input::doFastDepositRangeInput(const UInt32* buf,
                                                                 int range_msb,
                                                                 int range_lsb,
                                                                 const UInt32* drive,
                                                                 CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK) {
    bool changed = assignValueRange(buf, drive, index, length);
    doUpdateVHM(changed, model);
    if (changed) {
      *mChangeArrayRef = CARBON_CHANGE_MASK;
    }
  }
  return stat;
}

CarbonStatus CarbonTristateVector8Input::deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  doFastDepositInput(buf, drive, model);
  return eCarbon_OK;
}

CarbonStatus CarbonTristateVector8Input::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  return doDepositWordInput(buf, index, drive, model);
}

CarbonStatus CarbonTristateVector8Input::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  return doFastDepositRangeInput(buf, range_msb, range_lsb, drive, model);
}


void CarbonTristateVector8Input::fastDeposit(const UInt32* buf,
                                             const UInt32* drive, 
                                             CarbonModel* model)
{
  doFastDepositInput(buf, drive, model);
}

void CarbonTristateVector8Input::fastDepositWord(UInt32 buf, int index, 
                                                 UInt32 drive, CarbonModel* model) 
{
  doFastDepositWordInput(buf, index, drive, model);
}

void CarbonTristateVector8Input::fastDepositRange(const UInt32 *buf, 
                                                  int range_msb, 
                                                  int range_lsb, 
                                                  const UInt32* drive, 
                                                  CarbonModel* model) 
{
  doFastDepositRangeInput(buf, range_msb, range_lsb, drive, model);
}

bool CarbonTristateVector8Input::setToUndriven(CarbonModel* model)
{
  bool changed = CarbonTristateVector8::setToUndriven(model);
  if ((changed & (mChangeArrayRef != NULL)) != 0)
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  return changed;
}

bool CarbonTristateVector8Input::resolveXdrive(CarbonModel* model)
{
  bool changed = CarbonTristateVector8::resolveXdrive(model);
  if ((changed & (mChangeArrayRef != NULL)) != 0)
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  return changed;
}

void CarbonTristateVector8Input::getTraits(Traits* traits) const
{
  CarbonTristateVector8::getTraits(traits);
  traits->mHasInputSemantics = true;
}

bool CarbonTristateVector8Input::isInput() const
{
  return true;
}

void CarbonTristateVector8Input::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
  {
    UInt32 val[2];
    mTri->getExternalValues(val, xdrive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
  }
}

CarbonTristateVectorAInput::CarbonTristateVectorAInput(UInt32* idata, 
                                                       UInt32* idrive, 
                                                       UInt32* xdata, 
                                                       UInt32* xdrive) :
  CarbonTristateVectorA(idata, idrive, xdata, xdrive), mChangeArrayRef(NULL)
{}
  
CarbonTristateVectorAInput::CarbonTristateVectorAInput(UInt32* idata, 
                                                       UInt32* idrive) :
  CarbonTristateVectorA(idata, idrive), mChangeArrayRef(NULL)
{}
  
CarbonTristateVectorAInput::~CarbonTristateVectorAInput()
{}

void CarbonTristateVectorAInput::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  mChangeArrayRef = changeArrayRef;
}

CarbonChangeType* CarbonTristateVectorAInput::getChangeArrayRef()
{
  return mChangeArrayRef;
}

void CarbonTristateVectorAInput::doFastDepositInput(const UInt32* buf,
                                                    const UInt32* drive,
                                                    CarbonModel* model)
{
  bool changed = false;
  if (! drive)
    changed = setToDriven(model);
  
  changed |= assignValue(buf, drive);
  doUpdateVHM(changed, model);
  if (changed) {
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  }    
}

void CarbonTristateVectorAInput::doFastDepositWordInput(UInt32 buf,
                                                        int index, 
                                                        UInt32 drive,
                                                        CarbonModel* model)
{
  bool changed = assignValueWord(buf, drive, index);
  doUpdateVHM(changed, model);
  if (changed) {
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  }
}

CarbonStatus CarbonTristateVectorAInput::doDepositWordInput(UInt32 buf, int index, 
                                                            UInt32 drive,
                                                            CarbonModel* model)
{
  int numWords = getNumUInt32s();
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
  if (stat == eCarbon_OK) {
    doFastDepositWordInput(buf, index, drive, model);
  }
  return stat;
}

CarbonStatus CarbonTristateVectorAInput::doFastDepositRangeInput(const UInt32* buf,
                                                                 int range_msb,
                                                                 int range_lsb,
                                                                 const UInt32* drive,
                                                                 CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK) {
    bool changed = assignValueRange(buf, drive, index, length);
    doUpdateVHM(changed, model);
    if (changed) {
      *mChangeArrayRef = CARBON_CHANGE_MASK;
    }
  }
  return stat;
}

CarbonStatus CarbonTristateVectorAInput::deposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  doFastDepositInput(buf, drive, model);
  return eCarbon_OK;
}

CarbonStatus CarbonTristateVectorAInput::depositWord (UInt32 buf, int index, 
                                                      UInt32 drive, CarbonModel* model)
{
  return doDepositWordInput(buf, index, drive, model);
}

CarbonStatus CarbonTristateVectorAInput::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  return doFastDepositRangeInput(buf, range_msb, range_lsb, drive, model);
}

void CarbonTristateVectorAInput::fastDeposit(const UInt32* buf,
                                             const UInt32* drive,
                                             CarbonModel* model)
{
  doFastDepositInput(buf, drive, model);
}

void CarbonTristateVectorAInput::fastDepositWord(UInt32 buf, int index, 
                                                 UInt32 drive, CarbonModel* model)
{
  doFastDepositWordInput(buf, index, drive, model);
}

void CarbonTristateVectorAInput::fastDepositRange(const UInt32 *buf, 
                                                  int range_msb, 
                                                  int range_lsb, 
                                                  const UInt32* drive, CarbonModel* model) 
{
  doFastDepositRangeInput(buf, range_msb, range_lsb, drive, model);
}

bool CarbonTristateVectorAInput::setToUndriven(CarbonModel* model)
{
  bool changed = CarbonTristateVectorA::setToUndriven(model);
  if ((changed & (mChangeArrayRef != NULL)) != 0)
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  return changed;
}

bool CarbonTristateVectorAInput::resolveXdrive(CarbonModel* model)
{
  bool changed = CarbonTristateVectorA::resolveXdrive(model);
  if ((changed & (mChangeArrayRef != NULL)) != 0)
    *mChangeArrayRef = CARBON_CHANGE_MASK;
  return changed;
}

void CarbonTristateVectorAInput::getTraits(Traits* traits) const
{
  CarbonTristateVectorA::getTraits(traits);
  traits->mHasInputSemantics = true;
}

bool CarbonTristateVectorAInput::isInput() const
{
  return true;
}

void CarbonTristateVectorAInput::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
  {
    // not the most efficient way to do this, but I don't expect this
    // to be called often and I'm not too keen on adding yet another
    // function to tristates. If this becomes an issue we can fix it
    // in the future.
    UInt32 numWords = mRange->numWordsNeeded();
    UInt32 numBits = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
    for (UInt32 i = 0; i < numWords; ++i)
      xdrive[i] = mTri->getXDriveWord(i, numBits);
  }
}

/* signed tristate arrays */

void CarbonTristateVectorA::signExtend()
{
  const UInt32 numWords = CNUMWORDS(mRange->getMsb(), mRange->getLsb());
  const UInt32 bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  
  mTri->signExtend(numWords, bitWidth);  
}

void CarbonTristateVectorA::sanitize(UInt32* buf, UInt32* drive) const
{
  const UInt32 numWords = CNUMWORDS(mRange->getMsb(), mRange->getLsb());
  const UInt32 bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  const UInt32 mask = CarbonValRW::getWordMask(bitWidth);
  if (buf)
    buf[numWords - 1] &= mask;
  if (drive)
    drive[numWords - 1] &= mask;
}

void CarbonTristateVectorA::sanitizeWord(UInt32* buf, UInt32* drive, UInt32 index) const
{
  const UInt32 numWords = CNUMWORDS(mRange->getMsb(), mRange->getLsb());
  const UInt32 bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  const UInt32 mask = CarbonValRW::getWordMask(bitWidth);
  if (index == numWords - 1)
  {
    if (buf)
      buf[index] &= mask;
    if (drive)
      drive[index] &= mask;
  }
}

CarbonTristateVectorAS::CarbonTristateVectorAS(UInt32* idata, UInt32* idrive, 
                                               UInt32* xdata, UInt32* xdrive)
  : CarbonTristateVectorA(idata, idrive, xdata, xdrive)
{}

CarbonTristateVectorAS::CarbonTristateVectorAS(UInt32* idata, UInt32* idrive)
  : CarbonTristateVectorA(idata, idrive)
{}

CarbonTristateVectorAS::~CarbonTristateVectorAS()
{}

CarbonStatus CarbonTristateVectorAS::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = CarbonTristateVectorA::examine(buf, drive, mode, model);
  if (stat == eCarbon_OK)
    sanitize(buf, drive);
  return stat;
  
}
  
CarbonStatus CarbonTristateVectorAS::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = CarbonTristateVectorA::examineWord(buf, index, drive, mode, model);
  if (stat == eCarbon_OK)
    // don't need to sanitize drive on a non-tristate
    sanitizeWord(buf, drive, index);
  return stat;
}

CarbonStatus CarbonTristateVectorAS::deposit(const UInt32* buf, const UInt32* drv, CarbonModel* model)
{
  CarbonStatus stat = CarbonTristateVectorA::deposit(buf, drv, model);
  if (stat == eCarbon_OK)
    signExtend();
  return stat;
}
  
CarbonStatus CarbonTristateVectorAS::depositWord(UInt32 buf, int index, UInt32 drv, CarbonModel* model)
{
  CarbonStatus stat = CarbonTristateVectorA::depositWord(buf, index, drv, model);  
  if (stat == eCarbon_OK)
    signExtend();
  return stat;
}

CarbonStatus CarbonTristateVectorAS::depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32* drv, CarbonModel* model)
{
  CarbonStatus stat = CarbonTristateVectorA::depositRange(buf, range_msb, range_lsb, drv, model);  
  if (stat == eCarbon_OK)
    signExtend();
  return stat;
}

void CarbonTristateVectorAS::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  CarbonTristateVectorA::fastDeposit(buf, drive, model);
  signExtend();
}

void CarbonTristateVectorAS::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonTristateVectorA::fastDepositWord(buf, index, drive, model);
  signExtend();
}

void CarbonTristateVectorAS::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonTristateVectorA::fastDepositRange(buf, range_msb, range_lsb, drive, model);
  signExtend();
}

CarbonTristateVectorASInput::CarbonTristateVectorASInput(UInt32* idata, UInt32* idrive, 
                                                         UInt32* xdata, UInt32* xdrive)
  : CarbonTristateVectorAInput(idata, idrive, xdata, xdrive)
{}

  
CarbonTristateVectorASInput::CarbonTristateVectorASInput(UInt32* idata, UInt32* idrive)
  : CarbonTristateVectorAInput(idata, idrive)
{}

CarbonTristateVectorASInput::~CarbonTristateVectorASInput()
{}

CarbonStatus CarbonTristateVectorASInput::deposit(const UInt32* buf, const UInt32* drv, CarbonModel* model)
{
  CarbonStatus stat = CarbonTristateVectorAInput::deposit(buf, drv, model);
  if (stat == eCarbon_OK)
    signExtend();
  return stat;
}
  
CarbonStatus CarbonTristateVectorASInput::depositWord(UInt32 buf, int index, UInt32 drv, CarbonModel* model)
{
  CarbonStatus stat = CarbonTristateVectorAInput::depositWord(buf, index, drv, model);  
  if (stat == eCarbon_OK)
    signExtend();
  return stat;
}

CarbonStatus CarbonTristateVectorASInput::depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32* drv, CarbonModel* model)
{
  CarbonStatus stat = CarbonTristateVectorAInput::depositRange(buf, range_msb, range_lsb, drv, model);  
  if (stat == eCarbon_OK)
    signExtend();
  return stat;
}

void CarbonTristateVectorASInput::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  CarbonTristateVectorAInput::fastDeposit(buf, drive, model);
  signExtend();
}

void CarbonTristateVectorASInput::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonTristateVectorAInput::fastDepositWord(buf, index, drive, model);
  signExtend();
}

void CarbonTristateVectorASInput::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonTristateVectorAInput::fastDepositRange(buf, range_msb, range_lsb, drive, model);
  signExtend();
}
