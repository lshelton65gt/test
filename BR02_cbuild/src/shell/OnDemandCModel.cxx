// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/OnDemandCModel.h"
#include "shell/OnDemandMgr.h"
#include "shell/OnDemandDebug.h"

OnDemandCModelContext::~OnDemandCModelContext()
{
  CARBON_FREE_VEC(mOutputBuffer, CarbonUInt32, mOutputWords);
  delete mDebugInfo;
}

bool OnDemandCModelContext::run(CarbonUInt32 *inputs, CarbonUInt32 *outputs, CarbonUInt32 *prev_outputs)
{
  // Call the cmodel, letting it write to the temp buffer, which
  // has been initialized with the previous output values
  memcpy(mOutputBuffer, prev_outputs, mOutputWords * sizeof(CarbonUInt32));
  (*mPlayFn)(mUserData, inputs, mOutputBuffer);
  // Return success if the outputs matched
  if (memcmp(outputs, mOutputBuffer, mOutputWords * sizeof(CarbonUInt32)) == 0)
    return true;

  // Save the new outputs on a divergence
  memcpy(outputs, mOutputBuffer, mOutputWords * sizeof(CarbonUInt32));
  return false;
}

void OnDemandCModelContext::initDebug()
{
  // This should only be called once
  INFO_ASSERT(mDebugInfo == NULL, "Debug mode already enabled in cmodel context");
  mDebugInfo = new OnDemandDebugInfo(eCarbonOnDemandDebugCModel, mName);
}

OnDemandCModelContext::OnDemandCModelContext(void *userdata, CarbonUInt32 context, const char *name,
                                             CarbonUInt32 input_words, CarbonUInt32 output_words,
                                             CarbonUInt32 input_offset, CarbonUInt32 output_offset,
                                             CModelPlaybackFn fn)
  : mUserData(userdata),
    mContext(context),
    mName(name),
    mInputWords(input_words),
    mOutputWords(output_words),
    mInputOffset(input_offset),
    mOutputOffset(output_offset),
    mPlayFn(fn),
    mDebugInfo(NULL)
{
  mOutputBuffer = CARBON_ALLOC_VEC(CarbonUInt32, mOutputWords);
}

OnDemandCModelCall::~OnDemandCModelCall()
{
}

bool OnDemandCModelCall::run()
{
  return mContext->run(mInputs, mOutputs, mPrevOutputs);
}

OnDemandCModelCall::OnDemandCModelCall(OnDemandCModelContext *context, CarbonUInt32 *inputs, CarbonUInt32 *outputs, CarbonUInt32 *prev_outputs)
  : mContext(context),
    mInputs(inputs),
    mOutputs(outputs),
    mPrevOutputs(prev_outputs)
{
}

OnDemandCModelCallCollection::~OnDemandCModelCallCollection()
{
  CARBON_FREE_VEC(mInputs, CarbonUInt32, mMaxInputWords);
  CARBON_FREE_VEC(mOutputs, CarbonUInt32, mMaxOutputWords);
  CARBON_FREE_VEC(mPrevOutputs, CarbonUInt32, mMaxOutputWords);
}

void OnDemandCModelCallCollection::addCall(OnDemandCModelCall *call)
{
  mCalls.push_back(call);
  ++mNumCalls;
}

bool OnDemandCModelCallCollection::run()
{
  for (mLastCModelRun = 0; mLastCModelRun < mNumCalls; ++mLastCModelRun) {
    // The calls are saved in the order they occurred during the schedule
    // call.  It's possible that some have data dependencies on the outputs
    // of earlier calls, so we need to abort as soon as there's a divergence.
    // We keep track of the last model run so we can run the remaining ones
    // after recovery.
    if (!(mCalls[mLastCModelRun]->run()))
      return false;
  }

  return true;
}

CarbonModel::CModelRecoveryStatus OnDemandCModelCallCollection::getCallStatus(OnDemandCModelContext *context, CarbonUInt32 **outputs)
{
  // When there is a divergence on a cmodel call during idle, the outputs of all
  // the cmodel calls need to be copied into the Carbon Model as part of the state restore.
  // We need to distinguish between those cmodels that have already been run
  // (and only copy their outputs) and those that have not yet run (and actually
  // run them as usual).
  //
  // When we run the calls in this collection, the index of the last one run is
  // recorded, so we just need to find the call corresponding to the requested context
  // in our array.  We need to walk the array to do this, but this only occurs
  // on a divergence so it shouldn't be a big deal.

  for (CarbonUInt32 i = 0; i < mNumCalls; ++i) {
    if (mCalls[i]->getContext() == context) {
      if (i <= mLastCModelRun) {
        // point to the actual outputs the call wrote
        *outputs = mCalls[i]->getOutputBuffer();
        return CarbonModel::eCModelUseDB;
      }
      return CarbonModel::eCModelRunFn;
    }
  }

  // It's an error if we didn't find the context
  INFO_ASSERT(0, "cmodel context not found in divergent collection");
  return CarbonModel::eCModelRunFn;
}

OnDemandCModelContext *OnDemandCModelCallCollection::getLastRunContext()
{
  // If there was no divergence, mLastCModelRun will
  // point beyond the end of the array.  This function
  // should be never be called then, but let's be safe.
  INFO_ASSERT(mLastCModelRun < mNumCalls, "no cmodel call diverged");
  return mCalls[mLastCModelRun]->getContext();
}

OnDemandCModelCallCollection::OnDemandCModelCallCollection(CarbonUInt32 max_input_words, CarbonUInt32 max_output_words)
  : mNumCalls(0),
    mInputWords(0),
    mOutputWords(0),
    mMaxInputWords(max_input_words),
    mMaxOutputWords(max_output_words),
    mLastCModelRun(0)
{
  // Allocate the maximum space we might need, even though it
  // might not all be needed.  If we discover that this is too
  // wasteful, it can be changed, but this is easier.
  mInputs = CARBON_ALLOC_VEC(CarbonUInt32, mMaxInputWords);
  mOutputs = CARBON_ALLOC_VEC(CarbonUInt32, mMaxOutputWords);
  mPrevOutputs = CARBON_ALLOC_VEC(CarbonUInt32, mMaxOutputWords);
}


OnDemandCModelFactory::OnDemandCModelFactory()
  : mMaxInputWords(0),
    mMaxOutputWords(0)
{
}

OnDemandCModelFactory::~OnDemandCModelFactory()
{
  clearAll();
}

void OnDemandCModelFactory::addContext(void *cmodel, void *userdata,
                                       CarbonUInt32 context_id, const char *name,
                                       CarbonUInt32 input_words, CarbonUInt32 output_words,
                                       CModelPlaybackFn fn)
{
  // Have we seen any contexts for this cmodel yet?
  ContextArray *array;
  ContextMap::iterator iter = mContextMap.find(cmodel);
  if (iter == mContextMap.end()) {
    // Create a new array of contexts for this cmodel
    array = new ContextArray;
    mContextMap[cmodel] = array;
  } else {
    array = iter->second;
  }

  // We might need to resize the array to accomodate this context.
  if (context_id >= array->size()) {
    // We can't guarantee that a registration of context N implies
    // contexts [0..N-1] will also be registered, so we set the
    // newly added context pointers to null.  We can skip the
    // index for our new context because we'll set it below.
    CarbonUInt32 old_size = array->size();
    array->resize(context_id + 1);
    for (CarbonUInt32 i = old_size; i < context_id; ++i)
      (*array)[i] = NULL;
  }

  // Create a new context and save it in the array
  OnDemandCModelContext *context = new OnDemandCModelContext(userdata, context_id, name, input_words, output_words,
                                                             mMaxInputWords, mMaxOutputWords, fn);
  (*array)[context_id] = context;

  // Update the total input/output words required
  mMaxInputWords += input_words;
  mMaxOutputWords += output_words;
}

void OnDemandCModelFactory::addCallToCollection(void *cmodel, CarbonUInt32 context_id,
                                                CarbonUInt32 *outputs,
                                                OnDemandCModelCallCollection *collection)
{
  // Find the context in our map
  OnDemandCModelContext *context = findContext(cmodel, context_id);
  // Copy the previous output data
  CarbonUInt32 *input_buf = collection->getInputBufPtr(context->getInputOffset());
  CarbonUInt32 *output_buf = collection->getOutputBufPtr(context->getOutputOffset());
  CarbonUInt32 *prev_output_buf = collection->getPrevOutputBufPtr(context->getOutputOffset());
  memcpy(prev_output_buf, outputs, context->getOutputWords() * sizeof(CarbonUInt32));
  // Create the call and add it to the collection
  OnDemandCModelCall *call = new OnDemandCModelCall(context, input_buf, output_buf, prev_output_buf);
  mCalls.push_back(call);
  collection->addCall(call);
}

void OnDemandCModelFactory::saveCallResultToCollection(void *cmodel, CarbonUInt32 context_id,
                                                       CarbonUInt32 *inputs, CarbonUInt32 *outputs,
                                                       OnDemandCModelCallCollection *collection)
{
  // Find the context in our map
  OnDemandCModelContext *context = findContext(cmodel, context_id);
  // Copy the input/output data
  CarbonUInt32 *input_buf = collection->getInputBufPtr(context->getInputOffset());
  CarbonUInt32 *output_buf = collection->getOutputBufPtr(context->getOutputOffset());
  memcpy(input_buf, inputs, context->getInputWords() * sizeof(CarbonUInt32));
  memcpy(output_buf, outputs, context->getOutputWords() * sizeof(CarbonUInt32));
}

void OnDemandCModelFactory::initDebug()
{
  // Init all the contexts we know about
  for (ContextMap::iterator iter = mContextMap.begin(); iter != mContextMap.end(); ++iter) {
    ContextArray *array = iter->second;
    for (CarbonUInt32 i = 0; i < array->size(); ++i) {
      OnDemandCModelContext *context = (*array)[i];
      // It's possible that some contexts might not be registered,
      // if that particular context is never called.
      if (context) {
        context->initDebug();
      }
    }
  }
}

OnDemandCModelCallCollection *OnDemandCModelFactory::createCollection()
{
  OnDemandCModelCallCollection *collection = new OnDemandCModelCallCollection(mMaxInputWords, mMaxOutputWords);
  mCollections.push_back(collection);
  return collection;
}

OnDemandCModelContext *OnDemandCModelFactory::findContext(void *cmodel, CarbonUInt32 context_id)
{
  // Look up the model in the map.  This must succeed
  ContextMap::iterator iter = mContextMap.find(cmodel);
  INFO_ASSERT(iter != mContextMap.end(), "cmodel not found");
  ContextArray &array = *iter->second;
  // The context must exist as well
  INFO_ASSERT(context_id < array.size(), "cmodel context not found");
  return array[context_id];
}

void OnDemandCModelFactory::clear()
{
  // Only clear the data pertaining to cmodel calls, not their contexts
  for (CarbonUInt32 i = 0; i < mCalls.size(); ++i)
    delete mCalls[i];
  mCalls.clear();
  for (CarbonUInt32 i = 0; i < mCollections.size(); ++i)
    delete mCollections[i];
  mCollections.clear();
}


void OnDemandCModelFactory::clearAll()
{
  clear();
  // Also clear saved context information
  for (ContextMap::iterator iter = mContextMap.begin(); iter != mContextMap.end(); ++iter) {
    ContextArray *array = iter->second;
    for (CarbonUInt32 i = 0; i < array->size(); ++i) {
      OnDemandCModelContext *context = (*array)[i];
      delete context;
    }
    delete array;
  }
  mContextMap.clear();
}
