// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file 
  Implements support for callback functions to be associated with
  $stop, $finish, and save/restore.
*/

#include "util/CarbonPlatform.h"
#include "codegen/SysIncludes.h"
#include "hdl/HdlVerilogOFileSystem.h"
#include "shell/CarbonHookup.h"
#include "shell/CarbonModel.h"
#include "shell/CarbonSimControl.h"
#include "util/OSWrapper.h"
#include "util/ShellMsgContext.h"
#include "util/UtArray.h"

// constructor
ControlHelper::ControlHelper(CarbonOpaque handle)
{
  mIsFinished = false;
  mControlCBs = new ControlCBList();
  // during construction we install the default handlers, we cannot
  // specify a carbonModel because it does not exist yet, instead we
  // use the carbon_id.
  mDefaultStop = new RegisteredControlCBData(controlDefaultCBFunction, (CarbonModel*)NULL, (CarbonClientData) handle, eCarbonStop);
  mControlCBs->push_back(mDefaultStop);
  mDefaultFinish = new RegisteredControlCBData(controlDefaultCBFunction, NULL, (CarbonClientData) handle, eCarbonFinish);
  mControlCBs->push_back(mDefaultFinish);
}
  // destructor
ControlHelper::~ControlHelper()
{
  for (ControlCBListLoop p(*mControlCBs); ! p.atEnd(); ++p) {
    if ( ( mDefaultStop == *p ) or ( mDefaultFinish == *p ) ) {
      continue; // default will be explicitly deleted below
    }
    delete *p;
  }
  delete mControlCBs;
  delete mDefaultStop;
  delete mDefaultFinish;
  mDefaultStop = NULL;
  mDefaultFinish = NULL;
  mControlCBs = NULL;
}


void ControlHelper::updateCarbonStatusDueToStop()
{
  if ( mCarbonStatus == eCarbon_OK )
    mCarbonStatus = eCarbon_STOP;
}

void ControlHelper::updateCarbonStatusDueToFinish()
{
  if ( ( mCarbonStatus == eCarbon_OK ) or
       ( mCarbonStatus == eCarbon_STOP ) )
    mCarbonStatus = eCarbon_FINISH;
}

void ControlHelper::updateCarbonStatusDueToError()
{
  mCarbonStatus = eCarbon_ERROR;
}


// this is the method that gets called by the emited code for $stop and $finish
// it logs the fact that a stop/finish was called, then prints the
// verbosity message, then calls the registered callbacks for \a callbackType
void ControlHelper::runControlSysTask( CarbonOpaque const subModule,
                                       UInt32 verbosity,
                                       CarbonControlType callbackType,
                                       const char* verilogFilename,
                                       int verilogLineNumber)
{
  switch ( callbackType ) {
  case eCarbonStop: {
    updateCarbonStatusDueToStop();
    break;
  }
  case eCarbonFinish: {
    updateCarbonStatusDueToFinish();
    putIsFinished();
    break;
  }
  case eCarbonSave:
  case eCarbonRestore: {
    // Currently this happens only as a result of calls to carbonSave,
    // carbonRestore.  Until we handle $save, $incsave, $restart, do
    // not update CarbonStatus.
    break;
  }
    // this can only happen if memory has been corrupted
  default : INFO_ASSERT(0, verilogFilename);
  }


  if ( verbosity != 0 ) {
    // we use the verilog file system here so that if the user has
    // somehow overridden it the output will go through the users modifications.
    VerilogOutFileSystem* vfs = ShellGlobal::gCarbonGetVerilogFileSystem(subModule);
    vfs->putTargetFileDescriptor(1U); // only send output to stdout
    (*vfs) << (( callbackType == eCarbonStop ) ? "$stop" : "$finish" );
    (*vfs) << " called from file " << verilogFilename << ", line " << verilogLineNumber << ", in scope: _, at time: ";
    (*vfs) << *(ShellGlobal::gCarbonGetTimevarAddr(subModule));
    (*vfs) << UtIO::endl;
    if ( verbosity == 2 ) {
      (*vfs) << "Cpu time: ";
      (*vfs) << OSGetRusageTime(true, true);
      (*vfs) << ", Memory used: " << (UInt32) CarbonMem::getBytesAllocated() << " bytes" ;
      (*vfs) << UtIO::endl;
    }
  }
  

  for (ControlCBListLoop p(*mControlCBs); ! p.atEnd(); ++p) {
    RegisteredControlCBData* data = *p;
    if ( data->getControlCallbackType() == callbackType ) {
      CarbonModel *model = data->getCarbonModel ();
      CarbonObjectID *descr = model != NULL ? model->getObjectID () : NULL;
      (*(data->mUserFn))(descr, callbackType,
                         data->mUserData, verilogFilename, verilogLineNumber);
    }
  }
}

//! here is a default callback function for control tasks, it is static
void ControlHelper::controlDefaultCBFunction (CarbonObjectID* /* carbonObject is null for the default function */,
                                              CarbonControlType callbackType,
                                              CarbonClientData data,
                                              const char* verilogFilename,
                                              int verilogLineNumber)
{
  // since this is a static function, we need to lookup the message
  // context when it is called
  MsgContext* msgContext = ShellGlobal::gCarbonGetMessageContext (data);

  fflush(stdout);               // flush stdout so that this error message appears in proper place

  msgContext->SHLNoControlCallbackDefined((callbackType==eCarbonStop ? "$stop" : "$finish" ),
                                          verilogFilename, verilogLineNumber);
}




//! this function is static
RegisteredControlCBData* ControlHelper::adminAddControlCB(CarbonControlCBFunction fn,
                                                          CarbonModel* carbonModel,
                                                          CarbonClientData userData,
                                                          CarbonControlType controlType)
{
  if ( carbonModel == NULL ) {
    ShellGlobal::getProgErrMsgr()->SHLInvalidAddControlCallbackArg(1, "  It was NULL, it must point to a CarbonObjectID");
    return NULL;
  }
  CarbonHookup* hookup = carbonModel->getHookup();
  if ( hookup == NULL ){
    ShellGlobal::getProgErrMsgr()->SHLInvalidAddControlCallbackArg(1,"  It had an invalid value.");
    return NULL;
  }
  ControlHelper* controlHelper = hookup->getControlHelper();
  if ( controlHelper == NULL ) {
    ShellGlobal::getProgErrMsgr()->SHLInvalidAddControlCallbackArg(1,"  It had an invalid value.");
    return NULL;
  }
  
  // remove any default function for controlType before adding a user function, we
  // remove it directly (instead of using adminRemoveControlCB since
  // the default function did not have a CarbonModel
  switch (controlType)
  {
  case eCarbonStop:
  {
    if ( controlHelper->mDefaultStop ) {
      controlHelper->mControlCBs->remove(controlHelper->mDefaultStop);
    }
    break;
  }
  case eCarbonFinish: {
    if ( controlHelper->mDefaultFinish) {
      controlHelper->mControlCBs->remove(controlHelper->mDefaultFinish);
    }
    break;
  }
  case eCarbonSave:
  case eCarbonRestore:
    // there is currently no default handler for save or restore callbacks.
    break;
  }

  // and add the user's function
  RegisteredControlCBData* handle = new RegisteredControlCBData(fn, carbonModel, userData, controlType);
  controlHelper->mControlCBs->push_back(handle);

  return handle;
}

//! this function is static
void ControlHelper::adminRemoveControlCB(RegisteredControlCBData** pcallbackHandle)
{
  if (pcallbackHandle && *pcallbackHandle)
  {
    RegisteredControlCBData* handle = *pcallbackHandle;
    *pcallbackHandle = NULL;

    if ( handle  == NULL ) {
      ShellGlobal::getProgErrMsgr()->SHLInvalidRemoveControlCallbackArg("  Argument 2 was NULL");
      return;
    }
    
    CarbonModel* carbonObject = handle->getCarbonModel();
    if ( carbonObject  == NULL ) {
      ShellGlobal::getProgErrMsgr()->SHLInvalidRemoveControlCallbackArg("  Argument 2 was invalid.");
      return;
    }
    
    CarbonHookup* hookup = carbonObject->getHookup();
    if ( hookup  == NULL ) {
      ShellGlobal::getProgErrMsgr()->SHLInvalidRemoveControlCallbackArg("  Argument 2 was invalid.");
      return;
    }

    ControlHelper* controlHelper = hookup->getControlHelper();
    if ( controlHelper  == NULL ) {
      ShellGlobal::getProgErrMsgr()->SHLInvalidRemoveControlCallbackArg("  Argument 2 was invalid.");
      return;
    }

    // this can only happen if memory is corrupted
    INFO_ASSERT(controlHelper, "Design not fully initialized");

    controlHelper->mControlCBs->remove(handle);
    delete handle;
  }
}

