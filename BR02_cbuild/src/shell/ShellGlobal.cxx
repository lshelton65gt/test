// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


// We only build a thread-safe libcarbon now
#define PTHREADS

#include "util/MutexWrapper.h"
#include "util/CarbonPlatform.h"
#include "codegen/SysIncludes.h"
#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/ShellMsgContext.h"
#include "util/Zstream.h"
#include "util/CarbonAssert.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/UtIOStream.h"
#include "shell/ShellGlobal.h"
#include "shell/CarbonDBRead.h"
#include "shell/ShellData.h"
#include "shell/CarbonModel.h"
#include "shell/CarbonHookup.h"
#include "shell/Profile.h"
#include "iodb/IODBRuntime.h"
#include "iodb/ScheduleFactory.h"
#include "exprsynth/ExprFactory.h"
#include "util/Util.h"
#include "util/UtLicense.h"
#include "util/UtLicenseMsg.h"
#include "util/UtStringArray.h"
#include "util/UtSimpleCrypt.h"
#include "hdl/HdlVerilogPath.h"
#include "util/LicenseBypass.h"
#include "shell/carbon_model.h"
#include "shell/carbon_model_private.h"
#include "util/UtDLList.h"
#include "util/UtShellTok.h"

#if pfMSVC
# include <io.h>
#else
# include <unistd.h>
#endif

#if pfUNIX
#include <cerrno>
#endif

#include <stdio.h>

extern const char* gCarbonVersion();
/*!
  \file 
  Trash heap of global functions for shell-side use.
*/


// A note on thread safety
//
// Much of the static data in the ShellGlobal namespace is accessed
// only during model creation/destruction, and during API calls such
// as carbonFindNet() and carbonDumpVars().  Since these happen
// infrequently, the solution is to acquire short-lived locks whenever
// the static data is accessed.  ShellGlobal has lockMutex() and
// unlockMutex() methods to facilitate this.
//
// In some cases, such as IODB creation/destruction, it's easier to
// acquire a lock for the duration of the procedure.  This has the
// benefit of making accesses to static data in other classes
// (e.g. AtomicCache, SourceLocatorFactory) thread-safe, provided
// they're only made during model creation/deletion.  This also
// protects symbol table BOM data that is shared across multiple
// instances of the same model.
//
// For static data that can be accessed outside model
// creation/deletion as well, we can use the same ShellGlobal mutex.
// For example, the static data related to DB search path is accessed
// via C API functions as well as during model creation.  In this
// case, the ShellGlobal mutex is locked during the API functions.
//
// Additionally, we need to manage the allocation/freeing of memory
// for these shared data structures.  It's entirely possible that
// memory will be allocated in one thread but freed by another.  Our
// memory manager normally assigns a pool of memory to each thread, so
// this can cause a problem if one thread is freeing memory allocated
// by another thread while that other thread is also
// allocating/freeing.
//
// This problem is solved by switching to a custom memory pool when
// the mutex is locked.  The normal pools for each thread will not be
// affected, and since the mutex must be locked for the custom pool to
// be active, it can't be accessed by two threads simultaneously.
//
// Aside from the common data guarded by the model creation mutex,
// there is other common data whose usage is well-bounded and can be
// controlled by other mutexes.  This includes the model finder and
// the message contexts.

//! Mutex for model creation
MUTEX_WRAPPER_DECLARE(sCreateMutex);
//! Instance of the mutex wrapper
static volatile MutexWrapperMemPool* sCreateMutexWrapper = NULL;
//! Mutex for message context
MUTEX_WRAPPER_DECLARE(sMessageMutex);
//! Mutex for model finder
MUTEX_WRAPPER_DECLARE(sFinderMutex);


//! Instance of the database mutex wrapper
static volatile MutexWrapperMemPool* sDatabaseMutexWrapper = NULL;
//! Mutex for caching carbon databases
MUTEX_WRAPPER_DECLARE(sDatabaseMutex);

namespace ShellGlobal {
  static char sDirSearchList[cDirListLength + 1];
  static bool sOverridingDirSearch = false;
  static CarbonObjectID* sCurrentModel = NULL;

  //! Failsafe message context stream for programmer error
  class ProgErrStream : public MsgStream
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    ProgErrStream() : mDie(false) {}
    
    //! virtual destructor
    virtual ~ProgErrStream() {}
    
    //! Report message using low-level write
    /*!
      This does not guard against message intermingling, it just ensures
      that the user error will be reported as long as there is no fatal
      error caused by a signal such as SIGSEGV or SIGKILL, etc.
    */
    virtual void report(MsgContextBase::MsgObject* mo) {
      UtString rStr;
      mo->format(&rStr, false);
      size_t numBytesW = rStr.size();
      const char* data = rStr.data();
      fflush(stdout);
      size_t written;
      do {
        written = write(fileno(stdout), data, numBytesW);
        if (((int) written) == -1) {
#if pfUNIX
          if (errno != EINTR)
#endif
            // give up
            numBytesW = 0;

        }
        else {
          numBytesW -= written;
          data += written;
        }
      } while (numBytesW > 0);
      
      if (!rStr.empty())        // Will be empty if suppressed
        mo->reportMessageBox(rStr.c_str());

      if (mDie || (mo->getSeverity() == MsgContextBase::eFatal))
        Die();
    }

    //! setup so that next call to Report will exit
    void setDieOnNextCall() {
      mDie = true;
    }
    
  private:
    UtString mStr;
    bool mDie;
    
    //! Exit the program
    /*!
      This flushes the standard i/o descriptors and terminal console and
      exits with a non-zero status. We must do it this way instead of
      using exit, because exit could have atexit or onexit
      registration. Without messaging working properly, this can be
      fatal or have undefined behaviour.
      
      This will not return.
    */
    void Die() {
      fflush(stderr);
      fflush(stdout);
      fclose(stderr);
      fclose(stdout);
      _exit(1);
    }
  };

  //! Programmer error messaging stream
  static ShellGlobal::ProgErrStream* sProgErrStream = NULL;


  //! Model instance-specific associations for: MsgContext, random, VerilogFilesystem...
  /*!
    This structure allows us to find and return information that is associated
    with an instance of the carbon model based on any address that
    falls between the low and high address of the carbon_id.
    For example a MsgContext owned by an instance of the
    carbon model wil be returned based on an address that falls within
    the lowAddr (beginning address of the carbon_id) and the high_addr
    (the lowAddr + the size of the carbon_id).
  */
  class CarbonModelFinder 
  {
  public:
    CARBONMEM_OVERRIDES

    CarbonModelFinder(): mRandomSeed(0), mSimTime(0LL) {}
    virtual ~CarbonModelFinder() {}

    //! Beginning address of the carbon_id
    char* lowAddr;
    //! Ending address of the carbon_id
    char* highAddr;
    //! Random seed, for $random, etc
    SInt32 mRandomSeed;
    //! Global simulation time; needed for $time, $stime
    CarbonTime mSimTime;

    virtual MsgContext *getMsgContext () = 0;
    virtual VerilogOutFileSystem *getVerilogOutFileSystem () = 0;
    virtual HdlOStream *getVhdlOutFileSystem () = 0;
    virtual HdlIStream *getVhdlInFileSystem () = 0;
    virtual ControlHelper *getControlHelper () = 0;
  };

  class CarbonModelDescrFinder : public CarbonModelFinder 
  {
  public:

    CARBONMEM_OVERRIDES
    CarbonModelDescrFinder (carbon_model_descr *model) : 
      CarbonModelFinder (), mModel (model), mPrivate (model->mPrivate) {}
    virtual ~CarbonModelDescrFinder() {}
    
    virtual MsgContext *getMsgContext () 
    { return mPrivate->getMsgContext (); }

    virtual VerilogOutFileSystem *getVerilogOutFileSystem ()
    { return mPrivate->getVerilogOutFileSystem (); }

    virtual HdlOStream *getVhdlOutFileSystem ()
    { return mPrivate->getVhdlOutFileSystem (); }

    virtual HdlIStream *getVhdlInFileSystem ()
    { return mPrivate->getVhdlInFileSystem (); }

    virtual ControlHelper *getControlHelper ()
    { return mPrivate->getControlHelper (); }

  private:

    //! The carbon_id object for this design
    carbon_model_descr *mModel;
    carbon_model_private *mPrivate;

  };

  //! class to hide STL usage
  class ChangeIndexStorageMap : public ChangeIndexStorageMapI
  {
  public: CARBONMEM_OVERRIDES
    //! constructor
    ChangeIndexStorageMap() : mMaxIndex(0) 
    {}

    //! virtual destructor
    virtual ~ChangeIndexStorageMap() {}

    //! Map change array index
    virtual void mapIndex(SInt32 chIndex, SInt32 storageOffset)
    {
      mMap[storageOffset] = chIndex;
      mMaxIndex = std::max(chIndex, mMaxIndex);
    }
    
    virtual SInt32 getMaxIndex() const
    {
      return mMaxIndex;
    }

    //! Get index from storage offset
    virtual SInt32 getIndex(SInt32 storageOffset) const
    {
      NumToNum::const_iterator p = mMap.find(storageOffset);
      SInt32 ret = -1;
      if (p != mMap.end())
        ret = p->second;
      return ret;
    }
    
  private:
    typedef UtHashMap<SInt32, SInt32> NumToNum;
    NumToNum mMap;
    SInt32 mMaxIndex;
  };

  static const char scDefaultName[] = "design";
  static AtomicCache* sAtomicCache = NULL;
  static int sNumCacheRefs = 0;
  
  bool severityIsError(MsgContextBase::Severity sev)
  {
    bool ret = false;
    switch(sev)
    {
    case MsgContextBase::eSuppress:
    case MsgContextBase::eStatus:
    case MsgContextBase::eNote:
    case MsgContextBase::eWarning:
    case MsgContextBase::eContinue:
      break;
    case MsgContextBase::eFatal: // can't be here if it is fatal
    case MsgContextBase::eAlert:
    case MsgContextBase::eError:
      ret = true;
      break;
    }
    return ret;
  }

  CarbonStatus severityToStatus(MsgContextBase::Severity sev)
  {
    CarbonStatus ret = eCarbon_OK;
    switch(sev)
    {
    case MsgContextBase::eSuppress:
    case MsgContextBase::eStatus:
    case MsgContextBase::eNote:
    case MsgContextBase::eWarning:
    case MsgContextBase::eContinue:
      break;
    case MsgContextBase::eFatal: // can't be here if it is fatal
    case MsgContextBase::eAlert:
    case MsgContextBase::eError:
      ret = eCarbon_ERROR;
      break;
    }
    return ret;
  }

  //! \class DBManager
  /*! A single instance of this class is instantiated in gCarbonGetDB to open
   *  and close DB files.
   */
  class DBManager
  {
  private:
    class DBAttribs
    {
    public: CARBONMEM_OVERRIDES
      DBAttribs(CarbonDBType type) :
        mDBType(type)
      {
        if (! sAtomicCache) 
          sAtomicCache = new AtomicCache;
        
        ++sNumCacheRefs;

        mScheduleFactory = new SCHScheduleFactory;
        mShellSymTabBOM.putScheduleFactory(mScheduleFactory);
        
        mSymTab = new STSymbolTable(&mShellSymTabBOM,
                                    sAtomicCache);
        
        mSourceLocatorFactory = new SourceLocatorFactory;
        mIODB = new IODBRuntime(sAtomicCache, mSymTab, getProgErrMsgr(), mScheduleFactory, &mExprFactory, mSourceLocatorFactory);
        mNeedsLicense = true;
      }
     
      ~DBAttribs()
      {
        // before deleting the symtab, clean up any shelldata objects
        for (STSymbolTable::NodeLoop symIter = mSymTab->getNodeLoop(); 
             ! symIter.atEnd(); ++symIter)
        {
          STSymbolTableNode* node = *symIter;
          STAliasedLeafNode* leaf = node->castLeaf();
          if (leaf != NULL) {
            ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);
            if (bomdata)
            {
              ShellData* shellData = bomdata->getShellData();
              if (shellData)
                delete shellData;
            }
          }
        }
        delete mIODB;
        delete mSymTab;
        delete mScheduleFactory;
        delete mSourceLocatorFactory;
        --sNumCacheRefs;
        if(sNumCacheRefs == 0)
        {
          delete sAtomicCache;
          sAtomicCache = NULL;
        }
      }

      MsgContext* getMsgContext() {
        return mIODB->getMsgContext();
      }

      STSymbolTable* mSymTab;
      SCHScheduleFactory* mScheduleFactory;
      SourceLocatorFactory* mSourceLocatorFactory;
      IODBRuntime* mIODB;
      CarbonDBType mDBType;
      ShellSymTabBOM mShellSymTabBOM;
      ESFactory mExprFactory;

      bool needsLicense() const {
        return mNeedsLicense;
      }

      void putNeedsLicense(bool needsLicense) {
        mNeedsLicense = needsLicense;
      }

      const UtStringArray& getValidLicenseFeatures() {
        return mValidLicenseFeatures;
      }

      void putValidLicenseFeatures(const UtStringArray& features)
      {
        mValidLicenseFeatures = features;
      }
      
    private:
      bool mNeedsLicense;
      //! Ordered list of valid license features for this model
      UtStringArray mValidLicenseFeatures;
    };
  
    typedef UtHashMap<const IODBRuntime*, DBAttribs*> DBAttribMap;
    typedef UtHashMap<IODB*, ChangeIndexStorageMap*> DBChgIndexMap;

  public: CARBONMEM_OVERRIDES
    DBManager() {}
    ~DBManager() 
    {
      for(DBAttribMap::iterator iter = mDBAttrib.begin(); iter != mDBAttrib.end(); ++iter) {
        DBAttribs* attribs = iter->second;
        delete attribs;
      }
    }

    IODBRuntime* createIODB(CarbonObjectID *descr, 
                            const char* dbFilenameOrBuffer,
                            int bufferLen,
                            CarbonDBType whichDB, 
                            UInt32* id)
    {
      IODBRuntime* db = NULL;
      DBAttribs* dbAtt = loadDB(descr, dbFilenameOrBuffer,
                                bufferLen, whichDB);
      if (dbAtt) {
        db = link(dbAtt, id);
      }
      
      return db;
    }

    bool needsLicense(const IODBRuntime* db)
    {
      bool ret = true;
      DBAttribMap::iterator p = mDBAttrib.find(db);
      if (p != mDBAttrib.end())
      {
        DBAttribs* attrib = p->second;
        ret = attrib->needsLicense();
      }
      return ret;
    }

    void putNeedsLicense(const IODBRuntime* db, bool needsLicense)
    {
      DBAttribMap::iterator p = mDBAttrib.find(db);
      if (VERIFY(p != mDBAttrib.end()))
      {
        DBAttribs* attrib = p->second;
        attrib->putNeedsLicense(needsLicense);
      }
    }

    typedef UtArray<const UtStringArray*> LicenseFeatureArray;
    /*!
      Return an array of valid license features (which are themselves
      an array of strings) for all the models that exist.  Each
      element corresponds to a model instance in the simulation.

      The UtStringArray pointers returned are persistent, because
      they're stored in the DBAttribs objects.  The caller should not
      free them.
    */
    void getAllModelsValidLicenseFeatures(LicenseFeatureArray* featureArray)
    {
      for (DBAttribMap::UnsortedLoop l = mDBAttrib.loopUnsorted(); !l.atEnd(); ++l) {
        DBAttribs* attribs = l.getValue();
        const UtStringArray& validFeatures = attribs->getValidLicenseFeatures();
        featureArray->push_back(&validFeatures);
      }
    }

    /*!
      Set the valid license features for a model corresponding to an IODB.
    */
    void putValidLicenseFeatures(const IODBRuntime* db, const UtStringArray& features)
    {
      DBAttribMap::iterator p = mDBAttrib.find(db);
      INFO_ASSERT(p != mDBAttrib.end(), "DBAttrib not found");
      DBAttribs* attrib = p->second;
      attrib->putValidLicenseFeatures(features);
    }

    void removeRef(IODBRuntime* db, bool removeChangeIndexRef)
    {
      DBAttribMap::iterator p = mDBAttrib.find(db);
      if (VERIFY(p != mDBAttrib.end()))
      {
        DBAttribs* attrib = p->second;
        mDBAttrib.erase(p);
        delete attrib;
      }
      DBChgIndexMap::iterator d = mDBChgIndexMap.find(db);
      if (removeChangeIndexRef && (d != mDBChgIndexMap.end()))
      {
        ChangeIndexStorageMap* mapRef = d->second;
        mDBChgIndexMap.erase(d);
        delete mapRef;
      }
    }

    ChangeIndexStorageMap* createChgIndStoreMap(IODB* db)
    {
      DBChgIndexMap::iterator p = mDBChgIndexMap.find(db); 
      ChangeIndexStorageMap* mapRef = new ChangeIndexStorageMap;
      mDBChgIndexMap[db] = mapRef;
      return mapRef;
    }

    ChangeIndexStorageMap* getChgIndStoreMap(IODB* db)
    {
      DBChgIndexMap::iterator p = mDBChgIndexMap.find(db); 
      ChangeIndexStorageMap* ret = NULL;
      if (p != mDBChgIndexMap.end())
      {
        ret = p->second;
      }
      
      return ret;
    }

  private:
    // Also unique. Needed for reference removal 
    DBAttribMap mDBAttrib;
    
    // This map is a slave to dbAttrib
    DBChgIndexMap mDBChgIndexMap;

    DBAttribs* loadDB(CarbonObjectID *descr, 
                      const char* dbFilenameOrBuffer,
                      int bufferLen, // if non-zero, dbFilenameOrBuffer is whole buffer
                      CarbonDBType whichDB)
    {
      DBAttribs* dbAtt = new DBAttribs(whichDB);
      return loadDB (dbAtt, descr->mUID, dbFilenameOrBuffer, bufferLen, whichDB);
    }

    DBAttribs* loadDB(DBAttribs *dbAtt, 
                      const char *uid,
                      const char* dbFilenameOrBuffer,
                      int bufferLen, // if non-zero, dbFilenameOrBuffer is whole buffer
                      CarbonDBType whichDB)
    {
      bool invalidateDB = false;
      bool dbIsRead = false;
      IODBRuntime* db = dbAtt->mIODB;

      if (bufferLen != 0) {
        // The IODB bits were compiled into the model, so we don't need
        // to search -- the bits are passed in directly as the filename

        dbIsRead = db->readDBFromBuffer(dbFilenameOrBuffer, bufferLen,
                                        whichDB);
      }
      else {
        UtString buf(dbFilenameOrBuffer);

        // We have two possibilities here.  If a specific DB was
        // requested, the filename is complete and we can just check
        // for it.  If automatic DB selection was requested, we just
        // have the root name of the DB.
        if (whichDB == eCarbonAutoDB) {
          // Search for the full, then IO DBs
          UtString full;
          full << buf << IODB::scFullDBExt;
          UtString io;
          io << buf << IODB::scIODBExt;
          if (searchFilePath(&full)) {
            whichDB = eCarbonFullDB;
            buf = full;
          } else if (searchFilePath(&io)) {
            whichDB = eCarbonIODB;
            buf = io;
          } else {
            // We didn't find either one.
            invalidateDB = true;
            // Add the "auto" extension so the error message printed
            // below makes more sense to the user.
            buf << IODB::scAutoDBExt;
          }
          // Update the DBAttribs object with the actual DB type
          dbAtt->mDBType = whichDB;
        } else {
          // Full or IO DB requested explicitly
          if (! searchFilePath(&buf))
            invalidateDB = true;
        }
        if (invalidateDB)
        {
          {
            // Mutex must be locally scoped, because it may be
            // acquired again in getProgErrMsgr() below.
            MutexWrapper mutex(&sMessageMutex);
            if (sProgErrStream)
              sProgErrStream->setDieOnNextCall();
          }

          UtString cwd;
          OSGetCurrentDir(&cwd);
          UtString searchPath;
          if (sOverridingDirSearch)
            searchPath.assign(sDirSearchList);
          else
            searchPath.assign("./");
          
          UtString msg("No such file or directory or path is not readable. ");
          msg << "Current dir: " << cwd << " -- ";
          msg << "Search path: " << searchPath;
          getProgErrMsgr()->SHLDBFileOpenFail(buf.c_str(), msg.c_str());
        }
        else
          dbIsRead = db->readDB(buf.c_str(), whichDB);
      }
      
      if (dbIsRead)
      {
        if (strcmp(db->getDesignId(), uid) != 0)
        {
          if (bufferLen != 0) {
            // We should not be able to get here -- we are loading
            // the database from the model itself.  What could go
            // wrong?  But don't assert, since we are just trying to
            // issue an error message.  This is cryptic but at least
            // we should be able to find it.
            dbFilenameOrBuffer = "<model data>";
          }
          getProgErrMsgr()->SHLNonMatchingDB(dbFilenameOrBuffer,
                                             db->getDesignId(),
                                             uid);
          invalidateDB = true;
        }
      }
      else
        invalidateDB = true;

      if (invalidateDB)
      {
        delete dbAtt;
        dbAtt = NULL;
      }

      return dbAtt;
    } // loadDB
    
    const char* getTargetCompilerStr(TargetCompiler tcomp)
    {
      switch(tcomp)
      {
      case eGCC4: return "gcc4.x"; break;
      case eGCC3: return "gcc3.x"; break;
      case eICC: return "Intel icc"; break;
      case eSPW: return "Sparcworks Forte"; break;
      case eWINX: return "gcc3.x-mingw"; break;
      case eWIN: case eNoCC: return "Unknown compiler"; break;
      }
      return "Unknown compiler";
    }
    
    IODBRuntime* link(DBAttribs* dbAtt, UInt32* id)
    {
      IODBRuntime* db = dbAtt->mIODB;
      // Now that we don't share DBs across multiple models, every
      // model has an ID of 0.  This should be cleaned up at some
      // point.
      *id = 0;
      mDBAttrib[db] = dbAtt;
      return db;
    } // link

  }; // class DBManager

  struct LicMsgCB : public UtLicense::MsgCB
  {
  public:
    CARBONMEM_OVERRIDES

    virtual ~LicMsgCB() {}

    LicMsgCB() {}

    virtual void waitingForLicense(const char* feature);
    virtual void queuedLicenseObtained(const char* feature);

    virtual void exitNow(const char* reason);
    virtual void requeueLicense(const char* featureName);
    virtual void relinquishLicense(const char* featureName);
  };
  
} // namespace ShellGlobal

//! Message context for the programmer error stream.
static MsgContext* sProgErrMsgContext = NULL;
//! Number of references to sProgEerrMsgContext
static int sNumPERefs = 0;
static bool sHasDiags = false;




namespace ShellGlobal {
  //! Initializes and returns the programmer error message context.
  /*!
    Not thread-safe!  Call only from a function that has already locked sMessageMutex.
  */
  static MsgContext* sGetProgErrMsgrNoLock()
  {
    if (sProgErrMsgContext == NULL) {
      sProgErrStream = new ShellGlobal::ProgErrStream;
      sProgErrMsgContext = new MsgContext;
      sProgErrMsgContext->addReportStream(sProgErrStream);
    }
  
    return sProgErrMsgContext;
  }

  // Already doc'ed in ShellGlobal.h
  MsgContext* getProgErrMsgr()
  {
    // This may allocate memory, so install a mem pool along with the mutex.
    MutexWrapperMemPool mutex(&sMessageMutex);
    return sGetProgErrMsgrNoLock();
  }

  //! The maximum number of carbon models allowed
#define MAX_MODELS 2000
  //! An array of CarbonModelFinders for MsgContext association.
  static CarbonModelFinder* sCarbonModelFinder[MAX_MODELS];
  //! Index into array of sCarbonModelFinder array.
  static unsigned sNumModelFinders = 0;

  static DBManager* sDBManager = NULL;
  static int sNumDBManagerRefs = 0;
  static LicMsgCB* sLicMsgCB = NULL;
  static UtLicense* sLicense = NULL;
  static bool sLicenseQuiet = false;
  
  CarbonModelFinder* findModelInfo(CarbonOpaque instance)
  {
    // No memory allocation is done here, so the basic mutex will do.
    MutexWrapper mutex(&sFinderMutex);
    char* instanceCast = reinterpret_cast<char*>(instance);
    for (unsigned i = 0; i < sNumModelFinders; ++i)
    {
      CarbonModelFinder* finder = sCarbonModelFinder[i];
      if ((finder->lowAddr <= instanceCast) &&
          (finder->highAddr >= instanceCast))
        return finder;
    }
    return NULL;
  }

  // Already doc'ed in ShellGlobal.h
  MsgContext* gCarbonGetMessageContext(CarbonOpaque instance)
  {
    MsgContext* ret = NULL;
    if (instance != NULL)
    {
      CarbonModelFinder* finder = findModelInfo(instance);
      INFO_ASSERT(finder, "Instance not found.");
      ret = finder->getMsgContext();
    }
    else
    {
      ret = getProgErrMsgr();
    }
    return ret;
  }

  CarbonMsgCBDataID* gCarbonAddMsgCB(CarbonModel *context,
                                     CarbonMsgCB cbFun, 
                                     CarbonClientData userData)
  {
    MsgContext *shellMsgContext, *modelMsgContext;
    
    MsgCallback *msgCallback = new MsgCallback(cbFun, userData);
    
    shellMsgContext = getProgErrMsgr();
    shellMsgContext->addMessageCallback(msgCallback);

    if (context != NULL) {
      modelMsgContext = gCarbonGetMessageContext(context->getHandle ());
      modelMsgContext->addMessageCallback(msgCallback);
    }
    
    return (CarbonMsgCBDataID *) msgCallback;
  }

  void gCarbonRemoveMsgCB(CarbonModel *context, CarbonMsgCBDataID **ppCbData)
  {
    if (ppCbData != NULL && *ppCbData != NULL)
    {
      MsgCallback *msgCallback = (MsgCallback *) *ppCbData;
      
      MsgContext *shellMsgContext = getProgErrMsgr();
      shellMsgContext->removeMessageCallback(msgCallback);

      if (context != NULL) {
        MsgContext *modelMsgContext
          = gCarbonGetMessageContext(context->getHandle ());
        modelMsgContext->removeMessageCallback(msgCallback);
      }
      
      delete msgCallback;
      *ppCbData = NULL;
    }
  }

  SInt32* gCarbonGetRandomSeed(CarbonOpaque instance)
  {
    CarbonModelFinder* finder = findModelInfo(instance);
    INFO_ASSERT(finder, "Instance not found.");
    return &finder->mRandomSeed;
  }

  // finds and returns the verilog output file system defined for the
  // model that contains instance.
  VerilogOutFileSystem* gCarbonGetVerilogFileSystem(CarbonOpaque instance)
  {
    CarbonModelFinder* finder = findModelInfo(instance);
    INFO_ASSERT(finder, "Instance not found.");
    return finder->getVerilogOutFileSystem();
  }

  // finds and returns the vhdl output file system defined for the
  // model that contains instance.
  HdlOStream* gCarbonGetVhdlOutFileSystem(CarbonOpaque instance)
  {
    CarbonModelFinder* finder = findModelInfo(instance);
    INFO_ASSERT(finder, "Instance not found.");
    return finder->getVhdlOutFileSystem();
  }

  // finds and returns the vhdl input file system defined for the
  // model that contains instance.
  HdlIStream* gCarbonGetVhdlInFileSystem(CarbonOpaque instance)
  {
    CarbonModelFinder* finder = findModelInfo(instance);
    INFO_ASSERT(finder, "Instance not found.");
    return finder->getVhdlInFileSystem();
  }

  // finds and returns the ControlHelper defined for model that contains \a instance.
  ControlHelper* gcarbonPrivateGetControlHelper(CarbonOpaque instance)
  {
    CarbonModelFinder* finder = findModelInfo(instance);
    INFO_ASSERT(finder, "Instance not found.");
    return finder->getControlHelper();
  }

  void gCarbonAddMessageContext (CarbonObjectID *model)
  {
    // We may allocate and modify the static message context data, so lock the mutex.
    MutexWrapperMemPool messageMutex(&sMessageMutex);

    // assuming carbon_id in version 0
    if (sNumModelFinders >= MAX_MODELS) {
      model->mPrivate->getMsgContext()->SHLMaxModelsExceeded(MAX_MODELS);
    }
    
    // Lock the finder mutex, and install a custom memory pool for memory allocation
    MutexWrapperMemPool* finderMutex = new MutexWrapperMemPool(&sFinderMutex);

    CarbonModelFinder* finder = new CarbonModelDescrFinder (model);
    finder->lowAddr = reinterpret_cast <char *> (model->mHdl);
    finder->highAddr = finder->lowAddr + model->mHdlSize;
    sCarbonModelFinder[sNumModelFinders] = finder;
    // Incrementing in separate statement from indexing, to ensure
    // sequence point so signal handler won't see entry before it's
    // been assigned.
    sNumModelFinders++;

    // Done with the static finder data, so release the lock
    delete finderMutex;

    // Here we can also set up the Programmer Error msgContext.  Call
    // the non-locked version since we already acquired the lock
    // above.
    sGetProgErrMsgrNoLock();
    ++sNumPERefs;
  }

  //! Remove reference to Programmer Error Message Context
  /*!
    This should be called as a result of the destroy() function of each
    carbon model.
    Once the reference reaches 0, the Programmer Error Message Context
    is destroyed.
  */
  void sCarbonRemoveProgErrRef()
  {
    // We may free the static message context data, so lock the mutex.
    MutexWrapperMemPool mutex(&sMessageMutex);
    // Could potentially call this without an initialization
    if (sNumPERefs != 0)
    {
      --sNumPERefs;
      if (sNumPERefs == 0)
      {
        if (sProgErrMsgContext)
          delete sProgErrMsgContext;
        sProgErrMsgContext = NULL;
        if (sProgErrStream)
          delete sProgErrStream;
        sProgErrStream = NULL;
      }
    }
  }
  
  // Remove the model from the findModelInfo() data structure.
  // Remove reference to Programmer Error Message Context
  void gCarbonRemoveModel(carbon_model_descr* model)
  {
    // Lock the finder mutex, and install a custom memory pool for memory allocation
    MutexWrapperMemPool* finderMutex = new MutexWrapperMemPool(&sFinderMutex);

    // Tell Profile that model has been removed.
    // Not gonna do it.  We want profiling information to persist
    // after model goes away.  It gets written when simulation ends.
    //    Profile::instance()->removeModel(model);

    char* modelCast = reinterpret_cast<char*> (model->mHdl);
    unsigned i;
    for (i = 0;
         i < sNumModelFinders && sCarbonModelFinder[i]->lowAddr != modelCast;
         ++i)
      ;
    INFO_ASSERT(sCarbonModelFinder[i]->lowAddr == modelCast, "Unregistered Carbon Model");
    // Rather than delete here, causing reentrancy problem for signal
    // handler, save pointer and delete after overwriting array entry.
    CarbonModelFinder* victim = sCarbonModelFinder[i];
    --sNumModelFinders;         // There's one fewer now
    // If there's at least one left...
    if (sNumModelFinders > 0) {
      // If we didn't delete the last slot...
      if (i != sNumModelFinders) {
        // Move last one up into deleted slot
        sCarbonModelFinder[i] = sCarbonModelFinder[sNumModelFinders];
      }
      sCarbonModelFinder[sNumModelFinders] = NULL;
    }
    else
      // Last model has been removed.  Destroy Profile singleton.
      // This is a problem because though we may have just deleted the
      // last model, we may later create and delete more models.
      Profile::destroy();

    // Either array element has been overwritten (if there were >1),
    // or it's now beyond sNumModelFinders-1, so it can no longer be
    // accessed and is therefore safe to delete.
    delete victim;

    // To avoid deadlock, unlock the finder mutex now.
    // sCarbonRemoveProgErrRef() will lock the message mutex, and
    // otherwise we would still have the finder mutex locked when that
    // happens.  This is a problem because gCarbonAddMessageContext()
    // locks the message mutex before it locks the finder mutex.  We
    // need to abide by that ordering restriction here.
    delete finderMutex;

    sCarbonRemoveProgErrRef();
  }

  IODBRuntime* gCarbonCreateDB(CarbonObjectID *descr, 
                               const char* dbFilenameOrBuffer, int bufferSize,
                               CarbonDBType whichDB, UInt32* id)
  {
    lockMutex();
    // Need to initialize id, in case there is an error
    *id = 0;
    if (! sDBManager)
      sDBManager = new DBManager;
    ++sNumDBManagerRefs;
    IODBRuntime* iodb = sDBManager->createIODB(descr, dbFilenameOrBuffer,
                                            bufferSize, whichDB, id);
    unlockMutex();
    return iodb;
  }

  void gCarbonReleaseDB(IODBRuntime** iodb, bool removeChangeIndexRef)
  {
    lockMutex();
    if (VERIFY(sDBManager) && VERIFY(sLicense))
    {
      sDBManager->removeRef(*iodb, removeChangeIndexRef);
      --sNumDBManagerRefs;

      if (sNumDBManagerRefs == 0) {
        if (sHasDiags)
        {
          sLicense->release(UtLicense::eDIAGNOSTICS);
          sHasDiags = false;
        }
        
        delete sDBManager;
        sDBManager = NULL;
        delete sLicense;
        sLicense = NULL;
        delete sLicMsgCB;
        sLicMsgCB = NULL;
      }
    }
    unlockMutex();
    *iodb = NULL;
  }

  void doHeartbeat()
  {
    if (sLicense)
      sLicense->heartbeat();
  }

  // begin LicMsgCB definition
  void LicMsgCB::waitingForLicense(const char* feature)
  {
    ShellGlobal::getProgErrMsgr()->SHLWaitingForGenericLicense(feature);
  }

  void LicMsgCB::queuedLicenseObtained(const char* feature)
  {
    ShellGlobal::getProgErrMsgr()->SHLObtainedGenericLicense(feature);
  }

  void LicMsgCB::exitNow(const char* reason)
  {
    MsgContext* msg = ShellGlobal::getProgErrMsgr();
    msg->SHLLicenseServerFail(reason);
  }

  void LicMsgCB::requeueLicense(const char* feature)
  {
    ShellGlobal::getProgErrMsgr()->SHLRequeuingGenericLicense(feature);
  }

  void LicMsgCB::relinquishLicense(const char* feature)
  {
    ShellGlobal::getProgErrMsgr()->SHLRelinquishingGenericLicense(feature);
  }

  // end LicMsgCB definition


  void sMaybeCreateLicense()
  {
    if (!sLicense)
    {
      sLicMsgCB = new LicMsgCB;
      bool useHeartBeats = true;
#ifdef PTHREADS
      useHeartBeats = false;
#endif
      sLicense = new UtLicense(sLicMsgCB, useHeartBeats);
      sLicenseQuiet = (getenv("CARBON_LICENSE_QUIET") != NULL);
      sLicense->setMode(UtLicense::eMultiple);
      sLicense->putServerTimeout(UtLicense::cServerTimeoutMax);

      UtString dummy;
      sHasDiags = sLicense->checkout(UtLicense::eDIAGNOSTICS, &dummy);
#ifdef CARBON_IGNORE_LIC
      sHasDiags = true;
#endif
    }
  }
  
  void sGetStrfTime(UtString* msg, SInt64 timeBomb)
  {
    time_t bomb = timeBomb;
    struct tm* posixTmPtr = NULL;
#if ! pfWINDOWS
    struct tm posixTm;
    localtime_r(&bomb, &posixTm);
    posixTmPtr = &posixTm;
#else
    struct tm* winTime = localtime(&bomb);
    posixTmPtr = winTime;
#endif

    char buf[256];
    size_t stat = strftime(buf, sizeof(buf), "%A, %B %d, %Y at %H:%M:%S", posixTmPtr);
    INFO_ASSERT(stat > 0, "System call 'strftime' failed");
    (*msg) << buf;
  }

  static inline bool doVSPCheckout(UtLicense::Type licType,
                                   UtString* licReason)
  {
    bool isGood = true;

    /*
      We can't use the reference counting mode of UtLicense because
      we have to share the license object with the old
      speedcompiler licensing which uses the multiple mode. So,
      just check if the license is checked out before trying to
      check it out.
    */
    if (! sLicense->isCheckedOut(licType) && 
        ! sLicense->checkout(licType, licReason) && 
        ! sHasDiags)
    {
      isGood = false;
    }
    return isGood;
  }

  static inline bool doVSPCheckoutPrint(const UtCustomerDB::Signature* custSig,
                                        const IODBRuntime* iodb)
  {
    UtLicense::Type defaultType = UtLicense::eVSPRuntime;
    UtString licReason;
    bool isGood = false;

    // To later clean up the set of checked out licenses (see
    // cleanupCheckedOutLicenses()), we need to build a list of valid
    // feature names for each model and associate it with the IODB.
    //
    // Note that we have to do this even when a model uses the default
    // runtime license.  Consider model 1 that can check out either an
    // A license or a default license, and model 2 that has default
    // runtime licensing.  When model 1 is instantiated followed by
    // model 2, the A license that was checked out by model 1 should
    // be returned because model 2's default license is sufficient.
    UtStringArray featureList;
    // Get the base runtime feature name
    UtString baseFeature;
    sLicense->getFeatureName(&baseFeature, UtLicense::eVSPRuntime);

    // check for runtime customer license
    const char* custStr = custSig->c_str();
    if(strlen(custStr) == 0)
    {
      // no customer string, use default runtime license
      isGood = doVSPCheckout(defaultType, &licReason);
      if ( !isGood ) licReason << " CODE1";
      featureList.push_back(baseFeature);
    }
    else
    {
      bool licenseDiags = getenv("CARBON_LICENSE_DIAG") != NULL ? true : false;

      // The customer string mechanism that has existed in the IODB
      // for some time has evolved into a generic mechanism for
      // checking out a runtime license other than the default one.
      // See the documentation for UtCustomerDB.
      
      // Decompose the list of features from the signature and try to
      // check one out.
      UtString featureName;
      for (UtShellTok tok(custStr, false, ","); !tok.atEnd(); ++tok) {
        const char* suffix = *tok;
        // Append this to the base feature, unless it's the special
        // keyword DEFAULT.
        featureName = baseFeature;
        if (strcmp(suffix, "DEFAULT") != 0) {
          featureName << "_" << suffix;
        }
        featureList.push_back(featureName);

        if (licenseDiags)
        {
          fprintf(stderr, "CRBN-I-LICRUNTIME: Requesting Runtime License: %s\n", featureName.c_str());
        }
      }

      // If the feature starts with crbn_vsp_tokens_, skip the
      // cache check, we want a checkout for every instance,
      // not per-process.
      // set isGood = false

      // If one of the list of license features is already checked
      // out, that's sufficient.  Otherwise check one of them out.
      isGood = sLicense->isFeatureNameFromListCheckedOut(featureList);
      if (!isGood) {
        isGood = sLicense->checkoutFeatureNameFromList(featureList, &licReason);
        if ( !isGood ) licReason << " CODE2";
      }
    }

    // Store the feature list
    sDBManager->putValidLicenseFeatures(iodb, featureList);

    if (! isGood)
      getProgErrMsgr()->SHLLicenseCheckFail(licReason.c_str());

    return isGood;
  }

  bool checkoutSimulationRuntime(const char* custStr, UtString* licReason)
  {
    bool isGood = false;
    UtStringArray features;
    features.push_back(custStr);

       // If one of the list of license features is already checked
      // out, that's sufficient.  Otherwise check one of them out.
    isGood = sLicense->isFeatureNameFromListCheckedOut(features);
    if (!isGood)
      isGood = sLicense->checkoutFeatureNameFromList(features, licReason);
    
    return isGood;
  }

  bool checkoutRuntimeCustomer(const char* custStr, UtString* licReason)
  {
    lockMutex();

    sMaybeCreateLicense();

    UtString featureName;
    bool isGood = false;
    sLicense->getFeatureName(&featureName, UtLicense::eVSPRuntime);
    featureName.append("_");
    featureName.append(custStr);

    if ((sLicense->doesFeatureNameExist(featureName.c_str(), licReason) == false) ||
        (sLicense->checkoutFeatureName(featureName.c_str(), licReason) == false))
    {
      UtString tmp;
      tmp << "Checkout of " << featureName.c_str() << " failed: No feature match for " << custStr;
      tmp << " - " << licReason;
      licReason->assign(tmp.c_str());
    }
    else
      isGood = true;

    unlockMutex();
    return isGood;
  }

  bool doVSPLicensing(const IODBRuntime* iodb, 
                      const char* fileName,
                      const UtCustomerDB::Signature* custSig,
                      bool checkoutReplayLic,
                      bool* isReplayLicCheckedOut,
                      bool checkoutOnDemandLic,
                      bool* isOnDemandLicCheckedOut)
  {
    bool isGood = true;

    bool timeBombOk = false;

    *isReplayLicCheckedOut = false;
    *isOnDemandLicCheckedOut = false;
    
    // First check if there is a timebombed license. If so, use
    // that. If there isn't, or if the timebomb has expired checkout
    // flexlm licenses
    SInt64 timeBomb = custSig->getTimeBomb();
    if (timeBomb > 0)
    {
      UtString err;
      time_t curTime;
      time(&curTime);
      if (curTime < 0)
      {
        err << "Current-time system error";
        getProgErrMsgr()->SHLTimedLicenseCheckFail(err.c_str());
      }
      else if (curTime > timeBomb)
      {
        err << "Carbon Model " << iodb->getIfaceTag() << " <" << fileName << ">: ";
        /* for added security, scramble the message a little. Having
           the string right next to time check in the assembly code
           makes it easier to defeat.
        */
        char LicExpire[19];
        LicExpire[18] = '\0';
        int LicExpireCrypt[18];
        SCRAMBLECODE18("License expired on", LicExpireCrypt);
        SCRAMBLECODE18(LicExpireCrypt, LicExpire);

        err << LicExpire << " ";
        UtString buf;
        sGetStrfTime(&err, timeBomb);
        getProgErrMsgr()->SHLTimedLicenseCheckFail(err.c_str());
      }
      else
      {
        timeBombOk = true;

        // Timebombed models that requested Replay or OnDemand support
        // at compilation time had an existence check done on the
        // corresponding license features.  If the checks failed, the
        // model wouldn't have been created.  That means they can use
        // these technologies without a license check if the model
        // supports them.
        *isReplayLicCheckedOut = true;
        *isOnDemandLicCheckedOut = true;
        
        // now check if the timebomb will expire within the next 2
        // weeks. I'm going to be very verbose about the calculation
        // here, so it makes sense to me months from now.
        const SInt32 numSecondsInMin = 60;
        const SInt32 numSecondsInHour = numSecondsInMin * 60;
        const SInt32 numSecondsInDay = numSecondsInHour * 24;
        const SInt32 numSecondsInWeek = numSecondsInDay * 7;
        if ((curTime + (2 * numSecondsInWeek)) > timeBomb)
        {
          UtString buf;
          sGetStrfTime(&buf, timeBomb);

          UtString vhmId;
          vhmId << iodb->getIfaceTag() << " <" << fileName << ">";
          getProgErrMsgr()->SHLLicenseWillExpire(vhmId.c_str(), buf.c_str());
        }
      }
    }

    // If we are supposed to checkout a replay license, make sure that
    // that the iodb supports it
    if (checkoutReplayLic)
    {
      if ((iodb->getVHMTypeFlags() & eVHMReplay) == 0)
      {
        checkoutReplayLic = false;
        MsgContext::Severity severity = getProgErrMsgr()->SHLReplayDisabled();
        isGood = ! MsgContext::isErrorSeverity(severity);
      }
    }
    if (checkoutOnDemandLic)
    {
      if ((iodb->getVHMTypeFlags() & eVHMOnDemand) == 0)
      {
        checkoutOnDemandLic = false;
        MsgContext::Severity severity = getProgErrMsgr()->SHLOnDemandDisabled();
        isGood = ! MsgContext::isErrorSeverity(severity);
      }
    }

    // If the timebomb check succeeded do not checkout flexlm
    // licenses.
    if (! timeBombOk)
    {
      lockMutex();
      isGood &= doVSPCheckoutPrint(custSig, iodb);
      
      // Checkout a replay license
      if (checkoutReplayLic)
      {
        UtString licReason;
        *isReplayLicCheckedOut = doVSPCheckout(UtLicense::eVSPReplay, &licReason);
        if (! *isReplayLicCheckedOut)
        {
          MsgContext::Severity severity = getProgErrMsgr()->SHLNoReplayLicFound();
          if (MsgContext::isErrorSeverity(severity))
          {
            isGood = false;
            if ( !isGood ) licReason << " CODE3";
            getProgErrMsgr()->SHLLicenseCheckFail(licReason.c_str());            
          }
        }
      }
      // Checkout an onDemand license
      if (checkoutOnDemandLic) {
        UtString licReason;
        *isOnDemandLicCheckedOut = doVSPCheckout(UtLicense::eOnDemand, &licReason);
        if (! *isOnDemandLicCheckedOut)
        {
          MsgContext::Severity severity = getProgErrMsgr()->SHLNoOnDemandLicFound();
          if (MsgContext::isErrorSeverity(severity))
          {
            isGood = false;
            if ( !isGood ) licReason << " CODE4";
            getProgErrMsgr()->SHLLicenseCheckFail(licReason.c_str());            
          }
        }
      }
      
      for (UtCustomerDB::StrIter q = custSig->loopIPFeatures(); ! q.atEnd(); ++q)
      {
        UtString licReason;
        
        const char* featureName = *q;
        // see comment above about UtLicense object. Only checkout a
        // feature once.
        if (! sLicense->isFeatureNameCheckedOut(featureName) && 
            ! sLicense->checkoutFeatureName(featureName, &licReason) && 
            ! sHasDiags)
        {
          isGood = false;
          if ( !isGood ) licReason << " CODE5";
          getProgErrMsgr()->SHLLicenseCheckFail(licReason.c_str());
        }
      } // for
      unlockMutex();
    } // if timeBombOk
    return isGood;
  }

  bool checkoutVSPLicenses(const IODBRuntime* iodb, const char* dbFilename)
  {
    const UtCustomerDB* custDB = iodb->getCustomerDB();
    const UtCustomerDB::Signature* custSig = custDB->getCustomerSignature();
    if(custSig == NULL)
    {
      getProgErrMsgr()->SHLInvalidDB(dbFilename);
      return false;
    }
    
    bool isGood = false;
    bool dummyBool = false;
    isGood = doVSPLicensing(iodb, dbFilename, custSig, false, &dummyBool, false, &dummyBool);
    return isGood;
  }

  bool initByLicType(UtLicense::Type licType)
  {
    // Model creation/destruction also accesses static license data, so acquire the same lock.
    lockMutex();
    sMaybeCreateLicense();

    UtString licReason;
    bool isGood = true, bypass = false;

    LICENSE_ENV_CHECK(bypass);
    if (! bypass &&
        ! sLicense->isCheckedOut(licType) && 
        ! sHasDiags &&
        ! sLicense->doesFeatureExist(licType, &licReason)
        )
    {
      isGood = false;
    }

    if (getenv("CARBON_LICENSE_DIAG"))
    {
      UtString buf;
      sLicense->getFeatureName(&buf, licType);
      fprintf(stderr, "CRBN-I-LICEXISTCHECK: %s, Feature Exists: %s\n", buf.c_str(), isGood ? "true" : "FALSE");
    }

    unlockMutex();

    return isGood;
  }
 
  bool initByLicFeatureString(const char* feature)
  {
    // Model creation/destruction also accesses static license data, so acquire the same lock.
    lockMutex();
    sMaybeCreateLicense();

    UtString licReason;
    bool isGood = true, bypass = false;

    LICENSE_ENV_CHECK(bypass);
    if (! bypass &&
        ! sLicense->isFeatureNameCheckedOut(feature) && 
        ! sHasDiags &&
        ! sLicense->doesFeatureNameExist(feature, &licReason)
        )
    {
      isGood = false;
    }

    if (getenv("CARBON_LICENSE_DIAG"))
    {
      fprintf(stderr, "CRBN-I-LICEXISTCHECK: %s, Feature Exists: %s\n", feature, isGood ? "true" : "FALSE");
    }

    unlockMutex();

    return isGood;
  }
 
  bool initModelKits()
  {
    return initByLicType(UtLicense::eDIAGNOSTICS);
  }

  bool initModelValidation()
  {
    return initByLicType(UtLicense::eModelValidation);
  }
  bool initSystemC()
  {
    return initByLicType(UtLicense::eSystemCComp);
  }
  bool initSocVsp()
  {
    return initByLicType(UtLicense::eIntfRealViewSOC);
  }
  bool initPlatArch()
  {
    return initByLicType(UtLicense::eVSPExecPlatArch);
  }
  bool initPackage()
  {
    return initByLicType(UtLicense::eIPCreator);
  }
  bool initCompiler()
  {
    return initByLicType(UtLicense::eVSPCompiler);
  }
  bool initADCompiler()
  {
    // The AMBA Designer compiler license is the base license with an
    // _ad suffix.
    UtString featureName;
    
    // Need to create the license, so go through all the mutex stuff.
    // This is ugly but should be going away soon.
    lockMutex();
    sMaybeCreateLicense();
    sLicense->getFeatureName(&featureName, UtLicense::eVSPCompiler);
    unlockMutex();

    featureName << "_ad";

    return initByLicFeatureString(featureName.c_str());
  }

  bool initSoCDSystemCExport()
  {
    return initByLicType(UtLicense::eSoCDSystemCExport);
  }


  void cleanupCheckedOutLicenses()
  {
    // Since we're accessing all the models and the static DBManager,
    // acquire a lock.  The mutex installs its own custom memory pool,
    // so any allocations done below must be freed before the mutex is
    // unlocked.  That means we can't have any function-scoped
    // automatically-allocated objects.
    lockMutex();

    // Bug 12911 describes a scenario in which too many runtime
    // licenses might be checked out when multiple models are present.
    // Consider model 1, which can check out either an A or a B
    // license, and model 2, which can check out only a B license.
    //
    // If model 2 is created first, it will check out a B license, and
    // when model 1 is created it will use that already-checked-out
    // license.  However, if model 1 is created first, it will check
    // out an A license, while model 2 will check out a B license.
    //
    // The fix is to look at all the models and their list of valid
    // licenses, along with the currently checked-out licenses, and
    // relinquish those licenses that aren't needed.  In the above
    // example, the A license would be checked in because the
    // checked-out B license is sufficient for model 1.
    //
    // Note that this doesn't address the case in which model 1 can
    // check out an A or a C license, while model 2 can check out a B
    // or a C license.  Regardless of the order of creation, a
    // simulation containing instances of both models will consume an
    // A and a B license.  To do otherwise would require that we apply
    // costing to each license feature.  While this scenario could
    // check out a single C license instead of an A and a B license,
    // that's probably not desirable if the customer paid $10K for his
    // C license and only $1K apiece for the A and B.

    // Based on the criteria above, we won't check out any additional
    // licenses, but we might check in some that we don't need
    // anymore.  Keep a set of the necessary features, plus a list of
    // all the features currently in use.  We can't just query the
    // license object for all its checked-out licenses, because that
    // may include other non-runtime licenses (e.g. transactors).
    UtStringSet* necessaryFeatures = new UtStringSet;
    UtStringSet* allFeatures = new UtStringSet;
    // Now, for each model, compare its list of valid features against
    // those already checked out.  The DBManager class doesn't expose
    // its DBAttribs objects, and I don't want to change that, so have
    // it populate an array of feature lists.
    DBManager::LicenseFeatureArray* featureArray = new DBManager::LicenseFeatureArray;
    sDBManager->getAllModelsValidLicenseFeatures(featureArray);
    for (Loop<DBManager::LicenseFeatureArray> l(*featureArray); !l.atEnd(); ++l) {
      const UtStringArray* validFeatures = *l;
      // This list of features is created when the license checkout
      // for the model is done.  If the model is timebombed, the list
      // will be empty, so skip it.
      if (!(validFeatures->empty())) {
        // We want the "greatest" available license to be checked out,
        // so start from the end of the valid feature list.
        bool found = false;
        for (UtStringArray::const_reverse_iterator iter = validFeatures->rbegin();
             iter != validFeatures->rend(); ++iter) {
          const char* feature = *iter;
          if (sLicense->isFeatureNameCheckedOut(feature)) {
            UtString featureStr(feature);
            allFeatures->insert(featureStr);
            if (!found) {
              // Save this as a necessary feature.
              necessaryFeatures->insert(featureStr);
              found = true;
            }
          }
        }
        INFO_ASSERT(found, "Model has no license checked out");
      }
    }

    // Now determine the features we don't need, and check them in.
    allFeatures->eraseSet(*necessaryFeatures);
    for (UtStringSet::UnsortedLoop l = allFeatures->loopUnsorted(); !l.atEnd(); ++l) {
      const UtString& feature = *l;
      sLicense->releaseFeatureName(feature.c_str());
    }

    // Clean up
    delete featureArray;
    delete necessaryFeatures;
    delete allFeatures;
    unlockMutex();
  }

  // We used to need the model name, but now that we're not sharing
  // the IODB among multiple instances of the same model, we don't.
  // However, we can't change the C interface between the model and
  // libcarbon, so we need to ignore it at some point.
  CarbonModel* newModel(const char* /* model_name */,
                        const char* dbFileRoot,
                        CarbonDBType whichdb,
                        const char* designVersion,
                        CarbonObjectID *descr)
  {
    // Check version compatibility.
    if (descr->mVersion > CARBON_MODEL_VERSION) {
      getProgErrMsgr()->SHLDBCarbonDesignDBMismatch(gCarbonVersion(),
                                                    designVersion);
      return NULL;
    }

    lockMutex();
    sMaybeCreateLicense();
    unlockMutex();

    bool dbOpened;
    CarbonModel* model = NULL;
    
    UtString dbName(dbFileRoot);

    int bufferSize = 0;
    const char* buffer = NULL;
    switch (whichdb) {
    case eCarbonFullDB:
      if (descr->getFullDBFn) {
        (*descr->getFullDBFn)(&buffer, &bufferSize);
      } else {
        dbName << IODB::scFullDBExt;
      }
      break;
    case eCarbonIODB:
      if (descr->getIODBFn) {
        (*descr->getIODBFn)(&buffer, &bufferSize);
      } else {
        dbName << IODB::scIODBExt;
      }
      break;
    case eCarbonAutoDB:
      // We have a bit of a dilemma here.  If there are embedded DBs,
      // we can easily determine which one to use.  However, if there
      // are none, we don't what files exist until model creation
      // occurs.  We'll have to use a hybrid strategy.
      //
      // If we find an embedded DB, load it into the buffer as above,
      // and update the DB type.  Otherwise, do nothing.  When the
      // model actually tries to read the DB from disk, it will handle
      // the case that the DB type is still set to auto.  See
      // ShellGlobal::DBManager::loadDB().
      if (descr->getFullDBFn) {
        (*descr->getFullDBFn)(&buffer, &bufferSize);
        whichdb = eCarbonFullDB;
      } else if (descr->getIODBFn) {
        (*descr->getIODBFn)(&buffer, &bufferSize);
        whichdb = eCarbonIODB;
      }      
      break;
    case eCarbonGuiDB:
      // The GUI DB cannot be used in createModel. This method is only called
      // from compiled code and the database must be consistent with the
      // generated model. So, the full database must be used.
      ASSERT (whichdb != eCarbonGuiDB); /* this_assert_OK */
      break;
    default:
      // Might as well check for something too weird.
      ASSERT (whichdb == eCarbonFullDB || whichdb == eCarbonIODB); /* this_assert_OK */
      break;
    }

    // Construct the CarbonModel
    if (bufferSize == 0) {
      // Reads the DB from a DB file.
      model = new CarbonModel(descr, dbName.c_str(), 0, whichdb, &dbOpened);
    } else {
      // The DB file is compiled into the model.
      model = new CarbonModel(descr, buffer, bufferSize, whichdb, &dbOpened);
    }

    // Immediately install the model into the descr object. Otherwise, the
    // putChangeArrayToStorageMap call will assert.
    descr->mModel = model;

    CarbonHookup* hookup = model->getHookup();
    hookup->putDestroyFn(descr->destroyFn);
    hookup->putScheduleFn(descr->schedFn);
    hookup->putInitializeFn(descr->initFn);
    hookup->putSimulationOverFn(descr->simOverFn);
    
    hookup->putClkScheduleFn(descr->clkSchedFn);
    hookup->putDataScheduleFn(descr->dataSchedFn);

    hookup->putModeChangeFn(descr->modeChangeFn);

    hookup->putAsyncScheduleFn(descr->asyncSchedFn);
    hookup->putDebugScheduleFn(descr->debugSchedFn);
    hookup->putDepositComboScheduleFn(descr->depositComboSchedFn);

    OSStdio::mstdout = descr->mStdout;
    OSStdio::mstderr = descr->mStderr;
    
    OSStdio::pfwrite = descr->pfwrite;
    OSStdio::pfflush = descr->pfflush;

    if (! dbOpened) {
      delete model;
      model = NULL;
    }
    else
    {
      INFO_ASSERT(sDBManager, "Model not initialized");
      
      IODBRuntime* iodb = hookup->getDB();
      const UtCustomerDB* custDB = iodb->getCustomerDB();
      const UtCustomerDB::Signature* custSig = custDB->getCustomerSignature();
      if(custSig == NULL)
      {
        getProgErrMsgr()->SHLInvalidDB(dbName.c_str());
      }
      
      bool isGood = false;
      bool isReplayCheckedOut = false;
      bool isOnDemandCheckedOut = false;

      isGood = doVSPLicensing(iodb, dbName.c_str(), custSig, descr->isReplayEnabled(), &isReplayCheckedOut, 
                              descr->isOnDemandEnabled(), &isOnDemandCheckedOut);

      if (! isGood)
      {
#ifndef CARBON_IGNORE_LIC
        delete model;
        model = NULL;
#endif
      }

      if (model)
      {
        // Clean up the set of checked-out licenses for all models
        cleanupCheckedOutLicenses();

        if (descr->chgIndFn)
        {
          // if indToStoreFn is null we are using the struct api and the
          // change array indices are not needed
          lockMutex();
          ChangeIndexStorageMap* indMap = 
            sDBManager->getChgIndStoreMap(iodb);
          if (indMap)
            hookup->putChangeArrayToStorageMap(indMap);
          else
            (*descr->chgIndFn)(descr);
          unlockMutex();
          
        } // if indToStoreFn

        // Enable onDemand, if applicable.  This needs to be done before replay is
        // enabled, so that replay saves the onDemand disabled schedule functions as its
        // normal mode schedules.
        if (descr->isOnDemandEnabled() && (iodb->getVHMTypeFlags() & eVHMOnDemand) && isOnDemandCheckedOut)
          model->enableOnDemand();

        // if replay is enabled and this is a replayable Carbon Model, enable
        // replay in the CarbonModel object.
        if (descr->isReplayEnabled() && (iodb->getVHMTypeFlags() & eVHMReplay) && isReplayCheckedOut)
          model->enableReplay();

        // Now install the OnDemand enabled schedule functions
        model->onDemandInstallSchedulePointers();

        // Print a warning if clock glitch detection was requested but
        // the model doesn't support it.
        if (descr->checkClockGlitches() && !iodb->isClockGlitchGenerated()) {
          getProgErrMsgr()->SHLNoClockGlitchDetection();
        }
      } // if model
    } // else
    return model;
  }

  // This might be faster as an inline function.
  void gSetCurrentModel(CarbonObjectID* descr)
  {
    sCurrentModel = descr;
  }

  // This might be faster as an inline function.
  CarbonObjectID* gGetCurrentModel()
  {
    return sCurrentModel;
  }


  template <typename T> 
  RangeTestStatus carbonTestRangeType(T msb, T lsb, T range_msb, T range_lsb, T* badVal)
  {
    *badVal = 0; 
    if (! (((msb >= lsb) && (range_msb >= range_lsb)) ||
             ((msb <= lsb) && (range_msb <= range_lsb))))
      return eRangeIllegalOrder;
    
    if (! (((msb >= lsb) && (msb >= range_msb)) ||
             ((msb <  lsb) && (msb <= range_msb))))
    {
      *badVal = range_msb;
      return eRangeIndexOutOfBounds;
    }
    
    if (! (((msb >= lsb) && (lsb <= range_lsb)) ||
             ((msb <  lsb) && (lsb >= range_lsb))))
    {
      *badVal = range_lsb;
      return eRangeIndexOutOfBounds;
    }

    return eRangeOK;
  }

  static MsgContext* sGetMsgContext(CarbonModel* model)
  {
    return model ? model->getMsgContext() : getProgErrMsgr();
  }

  CarbonStatus carbonTestRange(int msb, int lsb, int range_msb, int range_lsb, CarbonModel* model)
  {
    return carbonTestRangeContext(msb, lsb, range_msb, range_lsb, 
                                  sGetMsgContext(model));
  }
  
  CarbonStatus carbonTestRangeContext(int msb, int lsb, int range_msb, int range_lsb, 
                                      MsgContext* context)
  {
    
    int badVal;
    RangeTestStatus rangeStat = carbonTestRangeType(msb, lsb, range_msb, range_lsb, &badVal);
    
    CarbonStatus stat = eCarbon_OK;
    
    switch(rangeStat)
    {
    case eRangeOK:
      break;
    case eRangeIllegalOrder:
      context->SHLValueIllegalRangeOrder(range_msb, range_lsb);
      stat = eCarbon_ERROR;
      break;
    case eRangeIndexOutOfBounds:
      context->SHLValueIndexOutOfBounds(badVal);
      stat = eCarbon_ERROR;
      break;
    }
    return stat;
  }

  static const char* sGetPathname(const HierName* memName, 
                                  UtString* buf)
  {
    buf->clear();
    // be careful. If the memName happens to be null, return <unknown>
    if (memName)
    {
      HdlVerilogPath pather;
      pather.compPathHier(memName, buf);
    }
    else
      buf->assign("<unknown>");
    return buf->c_str();
  }

  CarbonStatus carbonTestAddressRange(SInt64 msa, 
                                      SInt64 lsa, 
                                      SInt64 range_msa, 
                                      SInt64 range_lsa,
                                      CarbonModel* model,
                                      const HierName* memName)
  {

    SInt64 badVal;
    RangeTestStatus rangeStat = carbonTestRangeType(msa, lsa, range_msa, range_lsa, &badVal);
    
    CarbonStatus stat = eCarbon_OK;
    UtString buf;

    switch(rangeStat)
    {
    case eRangeOK:
      break;
    case eRangeIllegalOrder:

      if (model)
        sGetMsgContext(model)->SHLValueIllegalAddressRangeOrderAtTime(sGetPathname(memName, &buf), model->getSimulationTime(), range_msa, range_lsa);
      else
        sGetMsgContext(model)->SHLValueIllegalAddressRangeOrder(sGetPathname(memName, &buf), range_msa, range_lsa);
      
      stat = eCarbon_ERROR;
      break;
    case eRangeIndexOutOfBounds:
      if (model)
        sGetMsgContext(model)->SHLAddressOutOfBoundsAtTime(sGetPathname(memName,&buf), model->getSimulationTime(), badVal, msa, lsa);
      else
        sGetMsgContext(model)->SHLAddressOutOfBounds(sGetPathname(memName, &buf), badVal, msa, lsa);
      stat = eCarbon_ERROR;
      break;
    }
    return stat;
  }

  CarbonStatus carbonTestIndex(int index, int lowIndex, int highIndex, CarbonModel* model)
  {
    CarbonStatus stat = eCarbon_OK;
    if ((index < lowIndex) || (index > highIndex))
    {
      sGetMsgContext(model)->SHLValueIndexOutOfBounds(index);
      stat = eCarbon_ERROR;
    }
    return stat;
  }
  
  CarbonStatus carbonTestAddress(SInt64 address, SInt64 high_addr, SInt64 low_addr, CarbonModel* model, const HierName* memName)
  {
    if (not (((high_addr >= low_addr) && (high_addr >= address) && (address >= low_addr)) ||
             ((high_addr <= low_addr) && (high_addr <= address) && (address <= low_addr))))
    {
      UtString buf;

      if (model)
        sGetMsgContext(model)->SHLAddressOutOfBoundsAtTime(sGetPathname(memName, &buf), model->getSimulationTime(), address, high_addr, low_addr);
      else
        sGetMsgContext(model)->SHLAddressOutOfBounds(sGetPathname(memName, &buf), address, high_addr, low_addr);
      return eCarbon_ERROR;
    }
    
    return eCarbon_OK;
  }

  void reportOutFileOpenError(UtOStream* out)
  {
    getProgErrMsgr()->SHLFailedToOpenForOutput(out->getFilename(), out->getErrmsg());
  }
  
  void reportOutFileCloseError(UtOStream* out)
  {
    getProgErrMsgr()->SHLFailedToClose(out->getFilename(), out->getErrmsg());
  }
  
  void reportInsufficientBufferLength(size_t len, CarbonModel* model)
  {
    sGetMsgContext(model)->SHLInsufficientBufLen(len);
  }

  CarbonStatus reportSetDriveOnNonTristate(const STSymbolTableNode* name, CarbonModel* model)
  {
    UtString nameBuf;
    ShellSymTabBOM::composeName(name, &nameBuf, false, true);
    MsgContextBase::Severity sev = 
      model->getMsgContext()->SHLSetDriveOnNonTristate(nameBuf.c_str());
    return severityToStatus(sev);
  }

  void reportDepositToConstant(const STSymbolTableNode* name, CarbonModel* model)
  {
    UtString nameBuf;
    ShellSymTabBOM::composeName(name, &nameBuf, false, true);
    MsgContextBase::Severity sev =
      model->getMsgContext()->SHLDepositToConstant(nameBuf.c_str());
    if (sev == MsgContextBase::eSuppress) {
      // save time next time on warning
      model->putIssueDepositConstWarning(false);
    }
  }

  bool reportNotDepositable(const STSymbolTableNode* name, CarbonModel* model)
  {
    bool isDepositable = false;
    UtString nameBuf;
    ShellSymTabBOM::composeName(name, &nameBuf, false, true);
    MsgContextBase::Severity sev =
      model->getMsgContext()->SHLNetNotDepositable(nameBuf.c_str());
    switch (sev)
    {
    case MsgContextBase::eSuppress:
      // save time next time on warning
      model->putIssueDepositComboWarning(false);
      // fall through here
    case MsgContextBase::eStatus:
    case MsgContextBase::eNote:
    case MsgContextBase::eWarning:
    case MsgContextBase::eContinue:
      // Allow the value to be written in this case.
      isDepositable = true;
      break;
    case MsgContextBase::eFatal: // can't be here if it is fatal
    case MsgContextBase::eAlert:
    case MsgContextBase::eError:
      isDepositable = false; // explicit fail
      break;
    } 
    return isDepositable;
  }
  
  void reportNotForcible(const STSymbolTableNode* name, CarbonModel* model)
  {
    UtString nameBuf;
    ShellSymTabBOM::composeName(name, &nameBuf, false, true);
    model->getMsgContext()->SHLNetNotForcible(nameBuf.c_str());
  }

  void reportNotReplayObservable(const STSymbolTableNode* name, CarbonModel* model)
  {
    UtString nameBuf;
    ShellSymTabBOM::composeName(name, &nameBuf, false, true);
    model->getMsgContext()->SHLReplayNotObservable(nameBuf.c_str());
  }
  
  void reportNotReplayDepositable(const STSymbolTableNode* name, CarbonModel* model)
  {
    UtString nameBuf;
    ShellSymTabBOM::composeName(name, &nameBuf, false, true);
    model->getMsgContext()->SHLReplayNotDepositable(nameBuf.c_str());
  }
  
  void reportSHLProfileGenerate(bool doit)
  {
    if (doit)
      getProgErrMsgr()->SHLProfileGenerate();
  }

  ChangeIndexStorageMapI* createChangeIndStorageMap(IODB* db)
  {
    // No mutex required for sDBManager, since this is only called
    // from newModel(), where it's already been locked.
    INFO_ASSERT(sDBManager, "Model not initialized");
    return sDBManager->createChgIndStoreMap(db);
  }

  CarbonStatus setFilePath(const char* dirList)
  {
    CarbonStatus stat = eCarbon_OK;
    size_t dirListLen = strlen(dirList);
    if (dirListLen == 0)
    {
      MsgContextBase::Severity sev = getProgErrMsgr()->SHLDirListEmpty();
      stat = severityToStatus(sev);
    }
    else if (dirListLen > cDirListLength)
    {
      MsgContextBase::Severity sev = getProgErrMsgr()->SHLDirListTooLong(dirListLen, cDirListLength);
      stat = severityToStatus(sev);
    }
    else
    {
      // This static data is used during model creation, so acquire its lock
      lockMutex();
      strcpy(sDirSearchList, dirList);
      sOverridingDirSearch = true;
      unlockMutex();
    }
    return stat;
  }

  bool searchFilePath(UtString* fileName)
  {
    bool foundFile = false;
    UtString statResult;

    // This is only called from within newModel(), so the mutex has
    // already been locked.
    if (sOverridingDirSearch)
    {
      StrToken tok(sDirSearchList, ENVLIST_SEPARATOR);
      const char* dir;
      UtString fullFile;
      while (! foundFile && tok(&dir))
      {
        fullFile.erase();
        OSConstructFilePath(&fullFile, dir, fileName->c_str());
        if (OSStatFile(fullFile.c_str(), "f",  &statResult) == 1)
        {
          foundFile = true;
          *fileName = fullFile;
        }
      }
    }
    else
      foundFile = (OSStatFile(fileName->c_str(), "f",  &statResult) == 1);
    
    return foundFile;
  }
  
#undef MAX_MODELS

  void gCarbonSetSimTime( CarbonOpaque instance, const CarbonTime newTime )
  {
    CarbonModelFinder* finder = findModelInfo(instance);
    INFO_ASSERT(finder, "Instance not found.");
    finder->mSimTime = newTime;
  }

  CarbonTime* gCarbonGetTimevarAddr( CarbonOpaque instance )
  {
    CarbonModelFinder* finder = findModelInfo(instance);
    INFO_ASSERT(finder, "Instance not found.");
    return &finder->mSimTime;
  }

  void setLicenseQueuing(bool block)
  {
    // Model creation/destruction also accesses static license data, so acquire the same lock.
    lockMutex();
    sMaybeCreateLicense();
    sLicense->putBlocking(block);
    unlockMutex();
  }

  void lockMutex()
  {
    // I don't think the compiler will do something bad here by
    // setting the mutex before the lock is actually acquired, but
    // let's be safe.
    MutexWrapperMemPool* mutex = new MutexWrapperMemPool(&sCreateMutex);
    sCreateMutexWrapper = mutex;
  }

  void unlockMutex()
  {
    volatile MutexWrapperMemPool* mutex = sCreateMutexWrapper;
    sCreateMutexWrapper = NULL;
    delete mutex;
  }

  void lockDatabaseMutex()
  {
    // I don't think the compiler will do something bad here by
    // setting the mutex before the lock is actually acquired, but
    // let's be safe.
    MutexWrapperMemPool* mutex = new MutexWrapperMemPool(&sDatabaseMutex);
    sDatabaseMutexWrapper = mutex;
  }

  void unlockDatabaseMutex()
  {
    volatile MutexWrapperMemPool* mutex = sDatabaseMutexWrapper;
    sDatabaseMutexWrapper = NULL;
    delete mutex;
  }

  bool isLocked()
  {
    return (sCreateMutexWrapper != NULL);
  }

}
