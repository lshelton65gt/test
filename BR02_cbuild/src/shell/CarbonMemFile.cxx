// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/CarbonModel.h"
#include "shell/CarbonMemFile.h"
#include "hdl/ReadMemX.h"
#include "util/UtArray.h"
#include "util/UtHashMap.h"
#include "util/ShellMsgContext.h"
#include "util/UtConv.h"
#include "util/Loop.h"
#include "util/HierStringName.h"
#include "util/AtomicCache.h"
#include "shell/ShellGlobal.h"

#include <string.h>

/*!
  \file 
  This implements a wrapper for a generic verilog memory .dat file. We
  can read any dat file as long as we know how wide the rows are.
*/

//! Helper class that hides all the data structures.
class CarbonMemFile::Helper
{
  struct HashUInt64: public HashValue<UInt64> {
    size_t hash(UInt64 n) const {
      return (((size_t) n) & 0xffffffff) ^ ((size_t) (n >> 32));
    }
  };
  
  typedef UtHashMap<SInt64, UInt32*, HashUInt64> AddrRowMap;
  
public: CARBONMEM_OVERRIDES
  //! Constructor
  Helper(CarbonModel* context, const char* fileName, CarbonRadix format, 
         UInt32 rowWidth, int decreasingAddresses) : mFilename(fileName)
  {
    mCarbonModel = context;

    CarbonOpaque id = NULL;
    if (context)
      id = context->getHandle();
    
    INFO_ASSERT(format == eCarbonBin || format == eCarbonHex, "Only Bin and Hex formats currently supported");
    mReadMemX = new HDLReadMemX(fileName, format == eCarbonHex, rowWidth, (decreasingAddresses != 0), id);
    mHiAddr = UtSINT64_MIN;
    mLoAddr = UtSINT64_MAX;
    mDecreasingAddrs = decreasingAddresses;
    mRowWidth = rowWidth;
    mHole = NULL;
    mNumWords = 0;
  }

  //! Destructor
  ~Helper()
  {
    if (mReadMemX)
      delete mReadMemX;

    cleanMem();
    
    if (mHole)
      CARBON_FREE_VEC(mHole, UInt32, mNumWords);
  }
  
  //! Load the memory dat file
  CarbonStatus load()
  {
    if (! mReadMemX->openFile())
      return eCarbon_ERROR;
    
    SInt64 address;
    
    mNumWords = (mReadMemX->getRowWidth() + 31)/32;
    HDLReadMemXResult result;
    
    mHole = CARBON_ALLOC_VEC(UInt32, mNumWords);
    memset(mHole, 0, (sizeof(UInt32)) * mNumWords);
    
    UInt32* data = CARBON_ALLOC_VEC(UInt32, mNumWords);    
    while ((result = mReadMemX->getNextRowNoDepth(&address, data)) == eRMValidData)
    {
      if (address < mLoAddr)
        mLoAddr = address;
      if (address > mHiAddr)
        mHiAddr = address;

      AddrRowMap::iterator p = mSparseMem.find(address);
      if (p != mSparseMem.end())
      {
        UInt32* doomedData = p->second;
        CARBON_FREE_VEC(doomedData, UInt32, mNumWords);
      }
      mSparseMem[address] = data;
      data = CARBON_ALLOC_VEC(UInt32, mNumWords);
    }
    
    // We always allocate 1 too many, but I don't see a good alternative.
    CARBON_FREE_VEC(data, UInt32, mNumWords);
    
    mReadMemX->closeFile();
    
    CarbonStatus retStat = eCarbon_OK;
    if (result != eRMEnd)
    {
      retStat = eCarbon_ERROR;
      // clear out the bogus data
      cleanMem();
    }
    else
    {
      // all is good
      
      // don't delete this until the very end. It's nullness tells
      // me if the file has been loaded or not.
      delete mReadMemX;
      mReadMemX = NULL;
    }
    return retStat;
  }
  
  //! Get the first and last addr based on mDecreasingAddrs
  CarbonStatus getFirstAndLastAddrs(SInt64* firstAddress, SInt64* lastAddress) const
  {
    if (mReadMemX)
    {
      ShellGlobal::getProgErrMsgr()->SHLMemFileNotLoaded(mReadMemX->getFileName());
      return eCarbon_ERROR;
    }

    if (mDecreasingAddrs)
    {
      *firstAddress = mHiAddr;
      *lastAddress = mLoAddr;
    }
    else
    {
      *firstAddress = mLoAddr;
      *lastAddress = mHiAddr;
    }
    return eCarbon_OK;
  }

  //! Get the row of data
  const UInt32* getRow(SInt64 address) const
  {
    if (mReadMemX)
    {
      ShellGlobal::getProgErrMsgr()->SHLMemFileNotLoaded(mReadMemX->getFileName());
      return NULL;
    }

    // If it is a hole then it is null. 
    const UInt32* ret = NULL;
    
    // test the address
    AtomicCache strcache;
    HierStringName fileName(strcache.intern(mFilename.c_str()));
    CarbonStatus stat = ShellGlobal::carbonTestAddress(address, mHiAddr, mLoAddr, mCarbonModel, &fileName);
    
    if (stat == eCarbon_OK)
    {
      // get the row
      AddrRowMap::const_iterator p = mSparseMem.find(address);
      if (p != mSparseMem.end())
        ret = p->second;
    }
    return ret;
  }

  CarbonStatus populateArray(UInt32* theArray, UInt32 numWords, UInt32 rowBitIndex, UInt32 numBits)
  {
    if (mReadMemX)
    {
      ShellGlobal::getProgErrMsgr()->SHLMemFileNotLoaded(mReadMemX->getFileName());
      return eCarbon_ERROR;
    }
   
    // check if the rowBitIndex and numBits go beyond the row width
    if (rowBitIndex + numBits > mRowWidth)
    {
      ShellGlobal::getProgErrMsgr()->SHLMemRowBitAndLenOutOfBounds(rowBitIndex, numBits, mRowWidth);
      return eCarbon_ERROR;
    }
    
    // calculate num words per row
    UInt32 numWordsPerRow = (numBits + 31)/32;
    UInt32 numRows = calcMemSize();
    
    {
      // calculate total numWords needed
      UInt32 numWordsNeeded = numWordsPerRow * numRows;
      if (numWords < numWordsNeeded)
      {
        
        ShellGlobal::getProgErrMsgr()->SHLInsufficientArrayLen(numWords, numBits, numWordsNeeded);
        return eCarbon_ERROR;
      }
    }
    
    // memset the entire array to 0
    memset(theArray, 0, numWords * sizeof(UInt32));
    
    // for each row, grab the requested data and populate the given
    // array
    for (AddrRowMap::UnsortedCLoop p = mSparseMem.loopCUnsorted(); ! p.atEnd(); ++p)
    {
      SInt64 addr = p.getKey();
      const UInt32* row = p.getValue();
      SInt64 arrayAddr = calcDistance(addr);
      
      UInt32* arrayRow = &theArray[numWordsPerRow * arrayAddr];
      CarbonValRW::cpSrcRangeToDest(arrayRow, row, rowBitIndex, numBits);
    }
    
    return eCarbon_OK;
  }

private:

  //! Decreasing addrs?
  /*!
    kept this an int for padding purposes. Will make it a bool if I
    need more bools
  */
  int mDecreasingAddrs;
  //! Highest value address
  /*!
    Not necessarily the last address
  */
  SInt64 mHiAddr;
  //! Lowest value address
  /*!
    Not necessarily the first address
  */
  SInt64 mLoAddr;

  //! A place holder for all zeros, used for holes
  UInt32* mHole;

  //! The bit width of a row
  UInt32 mRowWidth;

  //! How many words allocated?
  UInt32 mNumWords;

  //! The parser
  HDLReadMemX* mReadMemX;

  //! Possibly NULL. Used for message context
  CarbonModel* mCarbonModel;

  //! Filename of memfile
  UtString mFilename;

  //! Typedef for the memory
  typedef UtArray<UInt32*> Rows;
  typedef Loop<Rows> RowsLoop;

  //! The memory
  AddrRowMap mSparseMem;

  void cleanMem()
  {
    for (AddrRowMap::UnsortedLoop p = mSparseMem.loopUnsorted(); 
         !p.atEnd(); ++p)
    {
      UInt32* doomed = p.getValue();
      CARBON_FREE_VEC(doomed, UInt32, mNumWords);
    }
  }

  SInt64 calcDistance(SInt64 address) const
  {
    SInt64 ret = 0;
    if (mDecreasingAddrs)
      ret = mHiAddr - address;
    else
      ret = address - mLoAddr;
    INFO_ASSERT(ret >= 0, "Calculated address < 0");
    
    return ret;
  }

  UInt32 calcMemSize() const
  {
    return mHiAddr - mLoAddr + 1;
  }

};

CarbonMemFile::CarbonMemFile(CarbonModel* context, const char* fileName, CarbonRadix format, UInt32 rowWidth, int decreasingAddresses)
{
  mHelper = new Helper(context, fileName, format, rowWidth, decreasingAddresses);
}


CarbonMemFile::~CarbonMemFile()
{
  delete mHelper;
}

CarbonStatus CarbonMemFile::load()
{
  return mHelper->load();
}

CarbonStatus CarbonMemFile::getFirstAndLastAddrs(SInt64* firstAddress, SInt64* lastAddress) const
{
  return mHelper->getFirstAndLastAddrs(firstAddress, lastAddress);
}


const UInt32* CarbonMemFile::getRow(SInt64 address) const
{
  return mHelper->getRow(address);
}

CarbonStatus CarbonMemFile::populateArray(UInt32* theArray, UInt32 numWords, UInt32 rowBitIndex, UInt32 numBits)
{
  return mHelper->populateArray(theArray, numWords, rowBitIndex, numBits);
}
