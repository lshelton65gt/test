// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


// We only build a thread-safe libcarbon now
#define PTHREADS

#include "util/CarbonPlatform.h"
#include "shell/CarbonModel.h"
#include "iodb/IODBRuntime.h"
#include "shell/CarbonSimControl.h"
#include "shell/CarbonDBRead.h"
#include "shell/CarbonHookup.h"
#include "shell/ShellNetWrapper.h"
#include "shell/ShellData.h"
#include "shell/WaveDump.h"
#include "hdl/HdlId.h"
#include "util/ShellMsgContext.h"
#include "util/UtHashSet.h"
#include "util/UtArray.h"
#include "util/Zstream.h"
#include "util/UtCheckpointStream.h"
#include "util/DynBitVector.h"
#include "shell/CarbonWaveImp.h"
#include "CarbonExamineScheduler.h"
#include "CarbonDatabase.h"
#include "shell/ShellNetTristate.h"
#include "shell/CarbonNetValueCBData.h"
#include "shell/Profile.h"
#include "shell/ShellNetRecord.h"
#include "shell/ShellNetPlayback.h"
#include "shell/CarbonReplayInfo.h"
#include "shell/ShellMemoryCreateInfo.h"
#include "shell/OnDemandMgr.h"
#include "shell/ShellNetOnDemand.h"
#include "shell/OnDemandDebug.h"
#include "shell/ApiId.h"
#include "shell/ShellVisNet.h"
#include "util/UtIStream.h"
#include "util/UtStringUtil.h"
#include "util/UtConv.h"
#include "util/UtStackPOD.h"

#include "shell/CarbonReplay.h"

using namespace CarbonReplay;

const volatile int CarbonModel::scNoBucket = -2;

CarbonNetCBData::CarbonNetCBData(CarbonNetCB fn, 
                                 CarbonClientData data)
  : mUserFn(fn), mNewLayer(NULL), mUserData(data)
{}

CarbonNetCBData::~CarbonNetCBData()
{}

CarbonNetValueCBData::CarbonNetValueCBData(CarbonNetValueCB fn, 
                                           CarbonClientData data,
                                           ShellNet* net,
                                           ShellNet::ExamineMode examineMode)
  : mUserFn(fn), mUserData(data), mNet(net), mExamineMode(examineMode),
    mEnabled(true), mIsSampleSched(false)
{}

CarbonNetValueCBData::~CarbonNetValueCBData()
{}

static void sOldNetCB(CarbonObjectID* model, CarbonNetID*, CarbonClientData data, UInt32*, UInt32*)
{
  CarbonNetCBData* cbData = (CarbonNetCBData*) data;
  cbData->executeCB(model->getModel ());
}

typedef UtHashSet<STAliasedLeafNode*> LeafSet;
typedef UtArray<STAliasedLeafNode*> LeafVector;

class CarbonModel::DataHelper
{
public: CARBONMEM_OVERRIDES

  typedef UtArray<ScheduleStimuli*> StimVec;
    
  class CBScheduleGroup
  {
  public: CARBONMEM_OVERRIDES

    CBScheduleGroup(ScheduleStimuli* stim) : mInitVals(NULL), 
                                             mScheduleStimuli(stim),
                                             mVCObjsNum(0)
    {}
    ~CBScheduleGroup() {
      cleanVCs();

      for (NetCBListLoop p(mCBData);
           ! p.atEnd(); ++p)
        delete *p;
    }

    ScheduleStimuli* getScheduleStimuli() {
      return mScheduleStimuli;
    }

    UInt32 numNets() const { return mCBData.size(); }

    void addCallBack(CarbonNetValueCBData* cbData, CarbonModel* model)
    {
      
      if (mInitVals == NULL)
        regenInitVals(model);

      mCBData.push_back(cbData);
      appendInitVal(cbData, model);
    }
    
    void collateNets(MiniMemPool* memPool, CarbonModel* model)
    {
      if (! mInitVals)
        regenInitVals(model);

      // I HATE not having the initial values with the callback data
      // structure, but it is wasted space if I do that. 
      std::sort(mCBData.begin(), mCBData.end(), sSortByNoTriNumBytes);
      std::sort(mInitVals->begin(), mInitVals->end(), sSortInitValsByNoTriNumBytes);
      
      CarbonExamineScheduler::NetTraitClosure traitClosure;
      for (NetCBListCLoop p(mCBData); ! p.atEnd(); ++p)
      {
        const CarbonNetValueCBData* cbData = *p;
        const ShellNet* net = cbData->getShellNet();
        traitClosure.addNet(net);
      }
      
      traitClosure.allocInitVCs(&mVCObjs, NULL, mInitVals, memPool);
      mVCObjsNum = mVCObjs.size();
      destroyInitVals(model);
    }
    
    void cleanVCs()
    {
      for (VCVecLoop p(mVCObjs);
           ! p.atEnd(); ++p)
      {
        CarbonValueChangeBase* base = *p;
        base->clear();
      }
      mVCObjs.clear();
    }
    
    void updateShadows()
    {
      for (UInt32 i = 0; i < mVCObjsNum; ++i)
      {
        CarbonValueChangeBase* vc = mVCObjs[i];
        vc->executeCompares();
      }
    }

    void processCBs(CarbonModel* model, DynBitVectorPair* valueBuf)
    {
      for (UInt32 i = 0; i < mVCObjsNum; ++i)
      {
        CarbonValueChangeBase* vc = mVCObjs[i];
        vc->executeCBs(&mCBData, model, valueBuf->mVal.getUIntArray(),
                       valueBuf->mDrv.getUIntArray());
      }
    }
    
    void destroyInitVals(CarbonModel* model)
    {
      if (mInitVals)
      {
        for (StorageVecLoop q(*mInitVals); 
             ! q.atEnd(); ++q)
        {
          CarbonShadowStruct* shadowStruct = *q;
          model->freeShadow(&shadowStruct);
        }
        delete mInitVals;
        mInitVals = NULL;
      }
    }
    
  private:
    NetCBList mCBData;
    VCVec mVCObjs;
    StorageVec* mInitVals;
    ScheduleStimuli* mScheduleStimuli; // needed to recreate stim->group map
    UInt32 mVCObjsNum;

    static bool sSortByNoTriNumBytes(const CarbonNetValueCBData* a1, const CarbonNetValueCBData* a2)
    {
      return CarbonExamineScheduler::sSortByNoTriNumBytes(a1->getShellNet(), a2->getShellNet());
    }

    static bool sSortInitValsByNoTriNumBytes(const CarbonShadowStruct* a1, const CarbonShadowStruct* a2)
    {
      return CarbonExamineScheduler::sSortByNoTriNumBytes(a1->getNet()->castShellNet(), a2->getNet()->castShellNet());
    }
    
    void appendInitVal(CarbonNetValueCBData* cbData, CarbonModel* model)
    {
      ShellNet* shlNet = cbData->getShellNet();
      CarbonShadowStruct* initVal = model->allocShadow(shlNet);
      mInitVals->push_back(initVal);
    }


    void regenInitVals(CarbonModel* model)
    {
      mInitVals = new StorageVec;
      // We may be adding a new callback to an existing list. Need
      // to get all the initial values of all the current nets
      // listed for callback
      for (NetCBListLoop p(mCBData);
           ! p.atEnd(); ++p)
        appendInitVal(*p, model);
    }

  };
    
  class NonPodCB
  {
  public: CARBONMEM_OVERRIDES

    NonPodCB(CarbonNetValueCBData* cbData)
    {
      mCBData = cbData;
      ShellNet* net = mCBData->getShellNet();
      mShadowValue = net->allocShadow();
    }

    ~NonPodCB()
    {
      if (mCBData)
      {
        ShellNet* net = mCBData->getShellNet();
        net->freeShadow(&mShadowValue);
      }
      delete mCBData;
    }
    
    ShellNet::Storage mShadowValue;
    CarbonNetValueCBData* mCBData;
  };

  typedef UtArray<NonPodCB*> NonPodCBList;
  typedef Loop<NonPodCBList> NonPodCBListLoop;
  
  friend class CBScheduleGroup;

  DataHelper() :
    mNoSchedPodGroup(NULL),
    mCollateCBs(true)
  {
    mMemPool = new MiniMemPool;
    mNonPodNum = 0;
    mValueDriveNonPodNum = 0;
  }

  ~DataHelper()
  {
    for (NetValueCBVec::iterator p = mDeadCBs.begin(),
           e = mDeadCBs.end(); p != e; ++p)
      delete *p;

    if (mNoSchedPodGroup)
      delete mNoSchedPodGroup;

    for (NonPodCBListLoop r(mNonPods); ! r.atEnd(); ++r)
      delete *r;

    for (NonPodCBListLoop r(mValueDriveNonPods); ! r.atEnd(); ++r)
      delete *r;

    for (OldCBListLoop s(mOldCBs); ! s.atEnd(); ++s)
      delete *s;
    
    delete mMemPool;
  }

  void addOldCB(CarbonNetCBData* oldCB)
  {
    mOldCBs.push_back(oldCB);
  }

  void clearInitVals(CarbonModel* model)
  {
    if (mNoSchedPodGroup)
      mNoSchedPodGroup->destroyInitVals(model);

  }

  void runNetCBs(CarbonModel* model)
  {
    UInt32* val = mValueBuffer.mVal.getUIntArray();
    UInt32* drv = mValueBuffer.mDrv.getUIntArray();
    for (UInt32 i = 0; i < mNonPodNum; ++i)
    {
      
      NonPodCB* nonPod = mNonPods[i];
      
      CarbonNetValueCBData* data = nonPod->mCBData;
      if (data->isEnabled())
      {
        ShellNet* net = data->getShellNet();
        if (net->compareUpdateExamine(&(nonPod->mShadowValue), val, drv) == ShellNet::eChanged)
          data->executeCB(model->getObjectID (), val, drv);
      }
    }

    for (UInt32 i = 0; i < mValueDriveNonPodNum; ++i) {
      NonPodCB* nonPod = mValueDriveNonPods[i];
      CarbonNetValueCBData* data = nonPod->mCBData;
      if (data->isEnabled()) {
        ShellNet* net = data->getShellNet();
        ShellNet::Storage* shadow = &(nonPod->mShadowValue);
        ShellNet::ValueState state;
        state = net->compareUpdateExamineUnresolved(shadow, val, drv);
        if (state == ShellNet::eChanged) {
          data->executeCB(model->getObjectID (), val, drv);
        }
      }
    }
    
    if (mNoSchedPodGroup)
      mNoSchedPodGroup->processCBs(model, &mValueBuffer);
  }

  void updateCBShadows()
  {
    for (UInt32 i = 0; i < mNonPodNum; ++i)
    {
      NonPodCB* nonPod = mNonPods[i];
      CarbonNetValueCBData* data = nonPod->mCBData;
      ShellNet* net = data->getShellNet();
      net->update(&(nonPod->mShadowValue));
    }

    for (UInt32 i = 0; i < mValueDriveNonPodNum; ++i)
    {
      NonPodCB* nonPod = mNonPods[i];
      CarbonNetValueCBData* data = nonPod->mCBData;
      ShellNet* net = data->getShellNet();
      net->updateUnresolved(&(nonPod->mShadowValue));
    }

    if (mNoSchedPodGroup)
      mNoSchedPodGroup->updateShadows();
  }

  CarbonNetValueCBData* addCB(CarbonModel* model, CarbonNetValueCB fn,
                              CarbonClientData userData,
                              ShellNet* net)
  {
    CarbonHookup* hookup = model->getHookup();
    CarbonExamineScheduler* examineSched = hookup->getCarbonExamineScheduler();

    CarbonExamineScheduler::NetInfo netInfo;
    examineSched->getNetInfo(net, &netInfo);

    mCollateCBs = true;

    maybeResizeValueBuffer(net);
    CarbonNetValueCBData* cbData = new CarbonNetValueCBData(fn, userData, net,
                                                            ShellNet::eCalcDrive);
    // The net will not be freed when carbonFreeHandle is called.
    net->incrCount();
    
    // Nets which are bitselects of PODs aren't yet handled for net
    // change callbacks.  Treat them as non-PODs.
    bool isNonPOD = netInfo.isNonPOD() || (netInfo.getPodBitsel() != -1);

    if (netInfo.isConstant())
      mDeadCBs.push_back(cbData);
    else if (isNonPOD)
    {
      NonPodCB* nonPod = new NonPodCB(cbData);
      mNonPods.push_back(nonPod);
    }
    else
    {
      if (mNoSchedPodGroup == NULL)
        mNoSchedPodGroup = new CBScheduleGroup(NULL);
      mNoSchedPodGroup->addCallBack(cbData, model);
    }

    return cbData;
  }
  

  CarbonNetValueCBData* addValueDriveCB(CarbonModel* model, CarbonNetValueCB fn,
                                        CarbonClientData userData,
                                        ShellNet* net)
  {
    CarbonHookup* hookup = model->getHookup();
    CarbonExamineScheduler* examineSched = hookup->getCarbonExamineScheduler();

    CarbonExamineScheduler::NetInfo netInfo;
    examineSched->getNetInfo(net, &netInfo);

    if (netInfo.isConstant())
      return NULL;

    // Only handle tri-states here
    if (net->isTristate()) {
      mCollateCBs = true;
      maybeResizeValueBuffer(net);
      CarbonNetValueCBData* cbData = new CarbonNetValueCBData(fn, userData, net,
                                                              ShellNet::eIDrive);
      // The net will not be freed when carbonFreeHandle is called.
      net->incrCount();

      INFO_ASSERT(netInfo.isNonPOD(),
                  "Registering value/change callback on non-tristate.");
      NonPodCB* nonPod = new NonPodCB(cbData);
      mValueDriveNonPods.push_back(nonPod);

      // Update the shadow value so that we see a change at
      // time-0. This overwrites the update done with
      // ShellNet::update() in the ShellNet::allocShadow() call. At
      // some point it would be better to reoriganize the shell to
      // make this simpler and easier to do.
      net->updateUnresolved(&(nonPod->mShadowValue));
    
      return cbData;
    } else {
      return addCB(model, fn, userData, net);
    }
  }
  

  void maybeCollateCBs(CarbonModel* model)
  {
    if (mCollateCBs)
    {
      if (mNoSchedPodGroup)
        mNoSchedPodGroup->cleanVCs();
      
      delete mMemPool;
      mMemPool = new MiniMemPool;
      
      mNonPodNum = mNonPods.size();
      mValueDriveNonPodNum = mValueDriveNonPods.size();
      
      if (mNoSchedPodGroup)
        mNoSchedPodGroup->collateNets(mMemPool, model);
    }
    mCollateCBs = false;
  }

  DynBitVectorPair* getValueBuffer() { return &mValueBuffer; }

private:
  typedef UtArray<CarbonNetCBData*> OldCBList;
  typedef Loop<OldCBList> OldCBListLoop;

  typedef UtArray<CarbonNetValueCBData*> NetValueCBVec;

  MiniMemPool* mMemPool;
  CBScheduleGroup* mNoSchedPodGroup;
  OldCBList mOldCBs;
  NetValueCBVec mDeadCBs;
  NonPodCBList mNonPods;
  NonPodCBList mValueDriveNonPods; // For tri-state when we detect drive changes
  UInt32 mNonPodNum;
  UInt32 mValueDriveNonPodNum;
  bool mCollateCBs;
  
  DynBitVectorPair mValueBuffer;
  
  void cleanVCs()
  {
    if (mNoSchedPodGroup)
      mNoSchedPodGroup->cleanVCs();
  }

  void maybeResizeValueBuffer(ShellNet* net)
  {
    size_t size = (size_t) net->getBitWidth();
    if (size > mValueBuffer.size())
      mValueBuffer.resize(size);
  }
};

class CarbonModel::ShadowList
{
public: CARBONMEM_OVERRIDES
  typedef UtHashSet<CarbonShadowStruct*, HashPointer<CarbonShadowStruct*> > ListType;
  
  ListType mHash;

  ~ShadowList() {
    destroy();
  }

  void add(CarbonShadowStruct* shadow) {
    mHash.insert(shadow);
  }
  bool remove(CarbonShadowStruct* shadow) {
    ListType::iterator p = mHash.find(shadow);
    bool ret = (p != mHash.end());
    if (ret) 
    {
      mHash.erase(p);

      CarbonNet* net = shadow->mNet;
      ShellNet::Storage s = 
        static_cast<ShellNet::Storage>(shadow->mShadow);
      ShellNet* shlNet = net->castShellNet();
      shlNet->freeShadow(&s);
      delete shadow;
    }
    return ret;
  }
  
  void destroy() {
    typedef UtArray<CarbonShadowStruct*> ShadowStructVec;

    ShadowStructVec v(mHash.size());
    UInt32 i = 0;
    for (ListType::iterator p = mHash.begin(), e = mHash.end(); p != e; ++p) {
      v[i++] = *p;
    }
    mHash.clear();
    for (ShadowStructVec::iterator p = v.begin(), e = v.end();
         p != e; ++p)
    {
      CarbonShadowStruct* shadow = *p;

      CarbonNet* cNet = shadow->getNet();
      ShellNet* net = cNet->castShellNet();
      ShellNet::Storage s = 
        static_cast<ShellNet::Storage>(shadow->mShadow);
      net->freeShadow(&s);
      delete shadow;
    }
  }
};

static STAliasedLeafNode* sFindReplayLeafInSymTab(STSymbolTable* dbSymtab, STAliasedLeafNode* replayLeafNode, 
                                                  MsgContext* msgContext, const char* dbName)
{
  STAliasedLeafNode* designLeaf = NULL;
  STSymbolTableNode* designNode = dbSymtab->lookup(replayLeafNode);
  if (designNode == NULL)
  {
    // possible scenario. Record with the symtab db and then
    // playback with the io db. Technically, any observables
    // should be in the iodb, but that may not be true if we
    // allow for point access on flop outputs (save the value
    // when it was examined).
    // Another possible scenario. The user pointed us to the
    // wrong replay directory?
    UtString name;
    replayLeafNode->compose(&name);
    msgContext->SHLReplayDBNetNotFoundInDesign(dbName, name.c_str());
  }
  else
  {
    designLeaf = designNode->castLeaf();
    if (designLeaf == NULL)
    {
      UtString name;
      designNode->compose(&name);
      msgContext->SHLPathNotANet(name.c_str());
    }
  }
  return designLeaf;
}

static ShellNet* sFindReplayNetInSymTab(STSymbolTable* dbSymtab, STAliasedLeafNode* replayLeafNode, 
                                        CarbonHookup* hookup, MsgContext* msgContext, 
                                        const char* dbName)
{
  ShellNet* ret = NULL;
  STAliasedLeafNode* designLeaf = sFindReplayLeafInSymTab(dbSymtab, replayLeafNode, msgContext, dbName);
  if (designLeaf)
    ret = hookup->getCarbonNet(designLeaf);
  return ret;
}

static void sFindDesignMemories(IODBRuntime *db, LeafVector *vec)
{
  STSymbolTable* dbSymtab = db->getDesignSymbolTable();

  LeafSet covered;
  for (STSymbolTable::NodeLoop q = dbSymtab->getNodeLoop();
       ! q.atEnd(); ++q)
  {
    STSymbolTableNode* node = *q;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf)
    {
      STAliasedLeafNode* leafStorage = leaf->getStorage();
      if (covered.insertWithCheck(leafStorage))
      {
        const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(leaf);
        ST_ASSERT(bomdata, leaf);
        // A net that has been optimized away may not have an
        // intrinsic. This only happens to internal nets
        // (portsplits, for example). Design nets always have
        // intrinsics, but we are working at a lower level here.
        const IODBIntrinsic* intrinsic = db->getLeafIntrinsic(leaf);
        if (intrinsic && (intrinsic->getMemAddrRange() != NULL)) {
          // We have found a memory
          vec->push_back(leafStorage);
        }
      }
    }
  }
}

struct CarbonModel::ReplayReadLeafClosure
{
  CARBONMEM_OVERRIDES
  
  ReplayReadLeafClosure()
    : mHookup(NULL), mDBName(NULL),
        mNet(NULL), mNumWords(0),
      mExternalIndex(-1),
      mInternalIndex(-1),
      mIsTristate(false),
      mIsMemory(false)
  {}                
  
  CarbonHookup* mHookup;
  const char* mDBName;
  ShellNet* mNet;
  UInt32 mNumWords;
  SInt32 mExternalIndex;
  SInt32 mInternalIndex;
  bool mIsTristate;
  bool mIsMemory;
};


class CarbonModel::ReplayData
{
public:
  CARBONMEM_OVERRIDES

  ReplayData() : mNetWrap(NULL), mRecordNet(NULL), mPlaybackNet(NULL),
                 mOriginalNet(NULL),
                 mInputBufferIndex(-1),
                 mRealRecord(false)
  {}

  ~ReplayData()
  {
    delete mNetWrap;
    clearRecordData();
    clearPlaybackData();
  }

  static const SInt32 scDataVersion = 1;
  
  ShellNetWrapper1To1* getNetWrapper()
  {
    return mNetWrap;
  }

  void revertToOriginalNet()
  {
    mNetWrap->replaceNet(mOriginalNet);
  }
  
  void putNetWrapper(ShellNetWrapper1To1* netWrap)
  {
    mNetWrap = netWrap;
    // Increment the reference count, since we need to clean the
    // wrapper up on deletion.
    mNetWrap->incrCount();
    maybeSetOriginalNet(netWrap);
  }

  //! Saves the record mode net for this node
  /*!
    Record nets are normally used to save stimulus to the record
    buffer before passing the stimulus to the underlying primitive
    net.  However, for nets that aren't depositable, a special record
    net is created.  It does not record its stimulus, but rather
    issues an error that it will not be recorded.

    The ReplayData class needs to distinguish between the two types
    when saving itself to the replay database.

    \param recordNet The record mode net created for this node
    \param realRecord True if the record net actually records its stimulus
   */
  void putRecordNet(ShellNetRecord* recordNet, bool realRecord)
  {
    mRecordNet = recordNet;
    mRealRecord = realRecord;
    maybeSetOriginalNet(recordNet);
  }

  ShellNetRecord* getRecordNet()
  {
    return mRecordNet;
  }

  void putPlaybackNet(ShellNetPlayback* playNet)
  {
    mPlaybackNet = playNet;
    maybeSetOriginalNet(playNet);
  }
  
  ShellNetPlayback* getPlaybackNet()
  {
    return mPlaybackNet;
  }

  void putInputBufferIndex(SInt32 index)
  {
    mInputBufferIndex = index;
  }

  SInt32 getInputBufferIndex() const { return mInputBufferIndex; }
  
  void writeDB(ZostreamDB& out, const STAliasedLeafNode* leaf) const
  {
    out << mInputBufferIndex;
    if (mInputBufferIndex >= 0)
    {
      // stimulus net must have a record net
      ST_ASSERT(mRecordNet, leaf);
      // Only storage nodes have stimulus
      ST_ASSERT(leaf->getStorage() == leaf, leaf);
    }
    
    UInt32 numWords = 0;
    UInt32 isTristate = 0;
    UInt32 isMemory = 0;
    SInt32 internalIndex = -1;
    SInt32 externalIndex = -1;
    // Only real recorded nets (not those that exist only to warn
    // about unrecorded deposits) get saved.
    if (mRecordNet && mRealRecord)
    {
      numWords = mRecordNet->getRecordNumWords();
      isTristate = mRecordNet->isTristate() ? 1 : 0;
      isMemory = mRecordNet->castMemory() != NULL ? 1 : 0;
      externalIndex = mRecordNet->getExternalIndex();
      internalIndex = mRecordNet->getInternalIndex();
    }
    
    out << numWords;
    out << isTristate;
    out << isMemory;
    out << externalIndex;
    out << internalIndex;
  }

  STFieldBOM::ReadStatus readDB(ZistreamDB& in,
                                STAliasedLeafNode* leaf,
                                MsgContext* msgContext,
                                ReplayReadLeafClosure& readClosure)
  {
    in >> mInputBufferIndex;
    
    UInt32 tmpIsTristate;
    in >> readClosure.mNumWords;
    in >> tmpIsTristate;
    
    UInt32 tmpIsMemory;
    in >> tmpIsMemory;
    
    in >> readClosure.mExternalIndex;
    in >> readClosure.mInternalIndex;

    // Return now if the file is bad; otherwise, we may be reading
    // uninitialized memory.
    if (in.fail())
      return STFieldBOM::eReadFileError;
    
    readClosure.mIsMemory = tmpIsMemory > 0;
    readClosure.mIsTristate = tmpIsTristate > 0;
    
    // process this node if it is in the input buffer or if it is a
    // storage memory
    if ((mInputBufferIndex >= 0) || (tmpIsMemory && (leaf->getStorage() == leaf)))
    {

      // This node is a stimulus node. It gets part of the input
      // buffer assigned to it.
      
      IODBRuntime* db = readClosure.mHookup->getDB();
      STSymbolTable* dbSymtab = db->getDesignSymbolTable();
      
      STAliasedLeafNode* designLeaf = sFindReplayLeafInSymTab(dbSymtab, leaf, msgContext, readClosure.mDBName);
      const ShellDataBOM* designData = NULL;
      if (designLeaf)
        designData = ShellSymTabBOM::getStorageDataBOM(designLeaf);
      else
        return STFieldBOM::eReadIncompatible;
      
      // force subordinates should already have their net. A
      // pre-read force loop was done to put the original ShellNets
      // into force subordinate slots.
      if (designData->isForceSubordinate())
      {
        ST_ASSERT(mOriginalNet, leaf);
        readClosure.mNet = mOriginalNet;
      }
      else
      {
        readClosure.mNet = sFindReplayNetInSymTab(dbSymtab, leaf, readClosure.mHookup, msgContext, readClosure.mDBName);
        if (readClosure.mNet == NULL)
          return STFieldBOM::eReadIncompatible;
      }

      if (readClosure.mNumWords != (unsigned) readClosure.mNet->getNumUInt32s())
      {
        UtString name;
        leaf->compose(&name);
        msgContext->SHLReplayNodeNumWordMismatch(readClosure.mDBName, name.c_str(), readClosure.mNumWords, readClosure.mNet->getNumUInt32s());
        return STFieldBOM::eReadIncompatible;
      }

      if (readClosure.mIsTristate != readClosure.mNet->isTristate())
      {
        UtString name;
        leaf->compose(&name);
        const char* thisDB = "Tristate";
        if (! readClosure.mIsTristate)
          thisDB = "Non-tristate";

        const char* thatDB = "Tristate";
        if (! readClosure.mNet->isTristate())
          thatDB = "Non-tristate";
        
        msgContext->SHLReplayNodeTristateMismatch(readClosure.mDBName, name.c_str(), thisDB, thatDB);
        return STFieldBOM::eReadIncompatible;
      }
    }
    return STFieldBOM::eReadOK;
  }
  
  void print() const
  {
    UtIO::cout() << "NetWrap = " << mNetWrap << UtIO::endl;
    UtIO::cout() << "RecordNet = " << mRecordNet << UtIO::endl;
    UtIO::cout() << "PlaybackNet = " << mPlaybackNet << UtIO::endl;
    UtIO::cout() << "InputBufferIndex = " << mInputBufferIndex << UtIO::endl;
  }

  ShellNet* getOriginalNet() { return mOriginalNet; }

  void putOriginalNet(ShellNet* net)
  {
    mOriginalNet = net;
  }
  
  void clearRecordData()
  {
    delete mRecordNet;
    mRecordNet = NULL;
  }

  void clearPlaybackData()
  {
    delete mPlaybackNet;
    mPlaybackNet = NULL;
  }

  void maybeSetOriginalNet(ShellNetWrapper1To1* netWrap)
  {
    if (mOriginalNet == NULL)
      putOriginalNet(netWrap->getNet());
  }

private:
  ShellNetWrapper1To1* mNetWrap;
  ShellNetRecord* mRecordNet;
  ShellNetPlayback* mPlaybackNet;
  ShellNet* mOriginalNet;

  SInt32 mInputBufferIndex;
  //! Is the record net a real record net?
  /*!
    Non-depositable nets are not actually recorded, but they have a
    valid record net so that errors can be reported if they're
    deposited while in record mode.
   */
  bool mRealRecord;
};

//! Replay symboltable BOM manager
/*!
  This handles all the data needed for replay (playback, record, and
  divergence detection).

  The event file uses a simple character tag for each event. The
  character tags are as follows (this needs to be kept up to date)
  
  # - Comment (ignored)
  @ - Checkpoint file
  t - Time update
  s - stimuli buffer
  r - response buffer
*/
class CarbonModel::ReplayBOM : public STFieldBOM
{
public:
  CARBONMEM_OVERRIDES


  ReplayBOM(CarbonHookup* hookup) 
    : mHookup(hookup), mReplayInfo(mHookup->getCarbonModel()), mRecorder(NULL), mPlayer(NULL), mRecoverContext(&mReplayInfo, hookup), mDynFactory(true)
  {
    mReplayTable = NULL;
    reAllocReplaySymTab();
    mPlaybackDiverged = false;
    mVerboseDivergence = false;
    // mHookup has all the schedule pointers, unless we create the
    // ReplayBOM in CarbonModel's constructor. We don't do that
    // currently, so let's assert that mDesignSchedules has valid
    // pointers.
    mDesignSchedules.init(mHookup);
    INFO_ASSERT(mDesignSchedules.isInit(), "At least one schedule pointer is NULL.");

    mPlaybackSchedules.init(sSchedulePlayback,
                            sClkSchedulePlayback,
                            sDataSchedulePlayback,
                            sAsyncSchedulePlayback);

    mRecordSchedules.init(sScheduleRecord,
                          sClkScheduleRecord,
                          sDataScheduleRecord,
                          sAsyncScheduleRecord);
  }
  
  virtual ~ReplayBOM() {
    unwrap1toNNets();
    delete mRecorder;
    delete mPlayer;

    delete mReplayTable;
    
    for (CBDataValuePairVec::iterator p = mNetValueChangeCBs.begin(),
           e = mNetValueChangeCBs.end(); p != e; ++p)
      delete *p;
    
    mNonPlaybackNets.clearPointerValues();

    mReplayCModels.clearPointerValues();
  }

  // 4: Removed time from the database
  // 5: Added clocks to preFieldWrite/Read.
  // 6: Added state outputs to touched nets
  static const SInt32 scManagerVersion = 6;
  static const SInt32 scSymTabVersion = 1;
  
  static const char* scVersionFile;
  static const char* scVhmSignatureFile;
  static const char* scIndexFile;
  static const char* scSymtabFile;
  static const char* scVersionReplayDBSignature;
  static const char* scVhmSigFileSignature;
  static const char* scVersionToken;
  static const char* scIdToken;
  static const char* scInputBufferWidthStr;
  static const char* scResponseBufferWidthStr;
  static const char* scResponseNetsNumStr;
  static const char* scReplaySymTabSig;
  static const char* scStimuliNetsNumStr;
  static const char* scStimuliClkNetsNumStr;

  //! Not yet implemented
  void writeBOMSignature(ZostreamDB& out) const 
  {
    // string signature
    out << scReplaySymTabSig;
    // symtab writing version
    out << scSymTabVersion;
    // ReplayData version
    out << ReplayData::scDataVersion;
  }
  
  //! Not yet implemented
  ReadStatus readBOMSignature(ZistreamDB& in, UtString* errMsg)
  {
    // string signature    
    UtString signature;
    in >> signature;

    if (signature.compare(scReplaySymTabSig) != 0)
    {
      *errMsg << "Incorrect Replay Symboltable Bill Of Materials Tag: " << scReplaySymTabSig;
      return eReadIncompatible;
    }
    
    // symtab writing version
    SInt32 symtabVersion = 0;
    in >> symtabVersion;
    if (in.fail())
      return eReadFileError;

    // ReplayData version
    SInt32 dataVersion = 0;
    in >> dataVersion;
    if (in.fail())
      return eReadFileError;

    if (symtabVersion > scSymTabVersion)
    {
      *errMsg << "Replay Symboltable version is from a newer version of the carbon runtime.";
      return eReadIncompatible;
    }

    if (dataVersion > ReplayData::scDataVersion)
    {
      *errMsg << "Replay Symboltable Field version is from a newer version of the carbon runtime.";
      return eReadIncompatible;
    }
    
    return eReadOK;
  }
  
  
  Data allocLeafData() {
    return new ReplayData;
  }
  
  Data allocBranchData() {
    return NULL;
  }

  void freeLeafData(const STAliasedLeafNode*, Data* bomdata) {
    if (bomdata)
    {
      ReplayData* data = static_cast<ReplayData*>(*bomdata);
      delete data;
    }
  }
  
  void freeBranchData(const STBranchNode* branch, Data* bomdata) 
  {
    if (bomdata)
      ST_ASSERT(*bomdata == NULL, branch);
  }
  
  void preFieldWrite(ZostreamDB& out)
  {
    // Write out the sizes of all the buffers
    out << scInputBufferWidthStr << mRecorder->getInputBufferWidth();
    out << scResponseBufferWidthStr << mRecorder->getResponseBufferWidth();

    // Write out the number of non-clock input nets (aka the number of
    // Touched buffers for the non-clock stimuli)
    out << scStimuliNetsNumStr;
    mRecorder->writeStimulusTouchedSize(out);
    
    // Write out the size of the response net array.
    out << scResponseNetsNumStr << mRecorder->getNumResponseNets();

    // Write out the response nets
    for (ReplayRecorder::ResponseIter p = mRecorder->loopResponseNets();
         ! p.atEnd(); ++p)
    {
      ShellNetRecordResponse* responseNet = *p;
      STAliasedLeafNode* name = responseNet->getNameAsLeaf();
      // Write out THIS table's leaf not the design table's.
      STAliasedLeafNode* thisTablesLeaf = findReplayLeaf(name);
      ST_ASSERT(thisTablesLeaf, name);
      out.writePointer(thisTablesLeaf);
      
      // Just in case we ever want to change the container for the
      // response nets write out the internal index to the Touched
      // buffer.
      out << responseNet->getInternalIndex();
    }

    // Write out the clock nodes
    out << mRecorder->numRecordClocks();
    for (ReplayRecorder::RecordNetVecIter p = mRecorder->loopRecordClocks();
         ! p.atEnd(); ++p)
    {
      ShellNetRecord* recordNet = *p;
      STAliasedLeafNode* name = recordNet->getNameAsLeaf();
      // Write out THIS table's leaf not the design table's.
      STAliasedLeafNode* thisTablesLeaf = findReplayLeaf(name);
      ST_ASSERT(thisTablesLeaf, name);
      out.writePointer(thisTablesLeaf);
    }

    // Write out the state output nodes
    out << mRecorder->numRecordStateOutputs();
    for (ReplayRecorder::RecordNetVecIter p = mRecorder->loopRecordStateOutputs();
         ! p.atEnd(); ++p)
    {
      ShellNetRecord* recordNet = *p;
      STAliasedLeafNode* name = recordNet->getNameAsLeaf();
      // Write out THIS table's leaf not the design table's.
      STAliasedLeafNode* thisTablesLeaf = findReplayLeaf(name);
      ST_ASSERT(thisTablesLeaf, name);
      out.writePointer(thisTablesLeaf);
    }
  }
  
  //! write leaf information to the database
  void writeLeafData(const STAliasedLeafNode* leaf, ZostreamDB& out) const
  {
    const ReplayData* data = castBOM(leaf);
    data->writeDB(out, leaf);
  }

  void reportUnrecognizedToken(ZistreamDB& in, const char* token)
  {
    UtString filename;
    in.getFilename(&filename);
    mHookup->getCarbonModel()->getMsgContext()->SHLDBUnrecognizedToken(filename.c_str(), token);
  }

  void writeBranchData(const STBranchNode*, ZostreamDB&, AtomicCache*) const
  {}

  //! Reads in buffer sizes and the response nets
  ReadStatus preFieldRead(ZistreamDB& in)
  {
    UtString inputBufWidthStr;
    UtString responseBufWidthStr;
    UtString numResponseNetsStr;
    UtString numStimuliNetsStr;
    UtString numStimuliClkNetsStr;

    UInt32 inputBufWidth = 0;
    in >> inputBufWidthStr;
    // Compare the token
    if (inputBufWidthStr.compare(scInputBufferWidthStr) != 0)
    {
      reportUnrecognizedToken(in, inputBufWidthStr.c_str());
      return eReadIncompatible;
    }
    in >> inputBufWidth;
    // Check if the conversion failed.
    if (in.fail())
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(in.getErrmsg());
      return eReadFileError;
    }
    
    UInt32 responseBufWidth = 0;
    in >> responseBufWidthStr;
    // Compare the token
    if (responseBufWidthStr.compare(scResponseBufferWidthStr) != 0)
    {
      reportUnrecognizedToken(in, responseBufWidthStr.c_str());
      return eReadIncompatible;
    }
    in >> responseBufWidth;
    // Check if the conversion failed.
    if (in.fail())
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(in.getErrmsg());
      return eReadFileError;
    }

    in >> numStimuliNetsStr;
    // Compare the token
    if (numStimuliNetsStr.compare(scStimuliNetsNumStr) != 0)
    {
      reportUnrecognizedToken(in, numStimuliNetsStr.c_str());
      return eReadIncompatible;
    }

    mPlayer->readStimulusTouchedSize(in);
    if (in.fail())
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(in.getErrmsg());
      return eReadFileError;
    }

    
    UInt32 numResponseNets = 0;
    in >> numResponseNetsStr;
    // Compare the token
    if (numResponseNetsStr.compare(scResponseNetsNumStr) != 0)
    {
      reportUnrecognizedToken(in, numResponseNetsStr.c_str());
      return eReadIncompatible;
    }
    in >> numResponseNets;
    // Check if the conversion failed.
    if (in.fail())
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(in.getErrmsg());
      return eReadFileError;
    }
    
    ReadStatus stat = eReadOK;

    // Resize the response buffer
    mPlayer->resizeResponseBufferWidth(responseBufWidth);


    // Reserve room in the response net array
    mPlayer->reserveResponseNets(numResponseNets);


    // Need the iodb to look up the design nets
    IODBRuntime* db = mHookup->getDB();
    STSymbolTable* dbSymtab = db->getDesignSymbolTable();
    MsgContext* msgContext = mHookup->getCarbonModel()->getMsgContext();
    for (UInt32 i = 0; i < numResponseNets; ++i)
    {
      STAliasedLeafNode* leafNode;
      if (in.readPointer(&leafNode))
      {
        SInt32 internalTouchIndex = -1;
        in >> internalTouchIndex;

        // Need to get the net for the Carbon Model.
        ReplayData* data = castBOM(leafNode);
        ShellNet* net = data->getOriginalNet();
        if (net == NULL)
          net = sFindReplayNetInSymTab(dbSymtab, leafNode, mHookup, msgContext, mDBInstanceDir.c_str());
        
        if (net == NULL)
          stat = eReadIncompatible;
        else
        {
          ST_ASSERT(internalTouchIndex > -1, leafNode);
          const ShellNetReplay::Touched* touched = mPlayer->getTouchedResponse(internalTouchIndex);
          ShellNetPlaybackResponse* responseNet = new ShellNetPlaybackResponse(net, *touched);
          mPlayer->addResponseNet(responseNet);
        }
      } // readPointer
    } // for


    // Resize the input buffers
    mPlayer->reserveInputBuffers(inputBufWidth);

    UInt32 numClkNets = 0;
    in >> numClkNets;
    UInt32 totalClkWords = 0;
    for (UInt32 i = 0; i < numClkNets; ++i)
    {
      STAliasedLeafNode* leafNode;
      if (in.readPointer(&leafNode))
      {
        STAliasedLeafNode* designLeaf = sFindReplayLeafInSymTab(dbSymtab,
                                                                leafNode,
                                                                msgContext,
                                                                mDBInstanceDir.c_str());
        if (designLeaf == NULL)
          stat = eReadIncompatible;
        else if (db->isTristate(designLeaf))
          // drive shadow
          ++totalClkWords;
        // value shadow
        ++totalClkWords;
        // non-state output clocks don't use a mask buffer, so there's
        // no need to add an additional word.
        mPlayer->addRecordedClock(leafNode);
      }
    }
    mPlayer->reserveClockShadow(totalClkWords);
    
    // Read in the state output nets
    UInt32 numStateOutputNets = 0;
    in >> numStateOutputNets;
    for (UInt32 i = 0; i < numStateOutputNets; ++i)
    {
      STAliasedLeafNode* leafNode;
      if (in.readPointer(&leafNode))
      {
        STAliasedLeafNode* designLeaf = sFindReplayLeafInSymTab(dbSymtab,
                                                                leafNode,
                                                                msgContext,
                                                                mDBInstanceDir.c_str());
        if (designLeaf == NULL) {
          stat = eReadIncompatible;
        }
        mPlayer->addRecordedStateOutput(leafNode);
      }
    }
    
    return stat;
  }
  
  //! Reads in a replay leaf and assigns a piece of the input buffer
  /*!
    Input buffer assignment is only done on storage nodes.
  */
  ReadStatus readLeafData(STAliasedLeafNode* leaf, ZistreamDB& in, 
                          MsgContext* msgContext) 
  {
    ReplayData* data = castBOM(leaf);

    ReplayReadLeafClosure closure;
    closure.mHookup = mHookup;
    closure.mDBName = mDBInstanceDir.c_str();
    ReadStatus stat = data->readDB(in, leaf, msgContext, closure);

    SInt32 inputBufferIndex = data->getInputBufferIndex();
    if ((stat == eReadOK) && closure.mNet)
    {
      ST_ASSERT(data->getPlaybackNet() == NULL, leaf);
      if (inputBufferIndex >= 0)
      {
        // This is a storage node that is also an input stimulus
        // node. Storage because net is non-null, and input because it
        // has a non-negative inputBufferIndex.

        ST_ASSERT(! closure.mIsMemory, leaf);

        ST_ASSERT(closure.mExternalIndex > -1, leaf);

        bool isClock = mPlayer->isRecordedClock(leaf);
        bool isStateOutput = mPlayer->isRecordedStateOutput(leaf);
        // The closure's external index is the touched buffer index.
        const ShellNetReplay::Touched* touchIndicator = 
          mPlayer->getStimuliTouchedAt(closure.mExternalIndex, isClock || isStateOutput);
        
        // Assign an input buffer chunk to this playback net.
        UInt32* valueBuf;
        UInt32* maskBuf;
        UInt32* driveBuf;
        UInt32* inputWords = mPlayer->getInputBuffer();
        // For the stimulus buffer, we want to allocate mask space if
        // the net is a state output.
        (void) assignReplayBufferChunk(&valueBuf, &maskBuf, &driveBuf, 
                                       closure.mNumWords,
                                       isStateOutput,
                                       closure.mIsTristate,
                                       inputBufferIndex, 
                                       inputWords);
        
        // We have a storage node that needs to have a playback net
        // created
        ShellNetPlayback* net = allocateBufferedPlaybackNet(closure.mNet, data, valueBuf, maskBuf, driveBuf, closure.mNumWords, 
                                                            closure.mIsTristate, true, touchIndicator, isClock, isStateOutput);

        mPlayer->mapNetToInputBuffer(net, inputBufferIndex, closure.mNumWords, closure.mIsTristate, isClock, isStateOutput, closure.mExternalIndex);
      }
      else if (closure.mIsMemory)
      {
        // Currently I'm requiring that all api-accessible memories
        // have external indices. This may change.
        ST_ASSERT(closure.mExternalIndex > -1, closure.mNet->getName());
        ShellNetPlaybackMem* memPlay = mPlayer->addExternalPlaybackMem(closure.mNet, closure.mNumWords, closure.mExternalIndex);
        data->putPlaybackNet(memPlay);
        if (closure.mInternalIndex > -1)
          mPlayer->addInternalPlaybackMem(memPlay, closure.mNumWords, closure.mInternalIndex);
      }
      else
        ST_ASSERT(0, leaf);
    }
    return stat;
  }
  
  ReadStatus readBranchData(STBranchNode*, ZistreamDB&, 
                            MsgContext*) {
    return eReadOK;
  }
  
  void printLeaf(const STAliasedLeafNode* leaf) const 
  {
    const ReplayData* data = castBOM(leaf);
    data->print();
  }
  
  void printBranch(const STBranchNode*) const 
  {
    UtIO::cout() << "0x0" << UtIO::endl;
  }

  //! Return BOM class name
  virtual const char* getClassName() const
  {
    return "CarbonModel::ReplayBOM";
  }

  //! Write the BOMData for a branch node
  virtual void xmlWriteBranchData(const STBranchNode* /*branch*/,UtXmlWriter* /*writer*/) const
  {
  }

  //! Write the BOMData for a leaf node
  virtual void xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/,UtXmlWriter* /*writer*/) const
  {
  }


  static ReplayData* castBOM(STAliasedLeafNode* replayLeaf)
  {
    return static_cast<ReplayData*>(replayLeaf->getBOMData());
  }

  static const ReplayData* castBOM(const STAliasedLeafNode* replayLeaf)
  {
    return static_cast<const ReplayData*>(replayLeaf->getBOMData());
  }

  // For record, replaces the generated schedule function
  static CarbonStatus sScheduleRecord(CarbonObjectID *descr, UInt64 simTime)
  {
    CarbonModel *hdl = reinterpret_cast <CarbonModel *> (descr->mModel);
    ReplayBOM* me = hdl->getReplayBOM();
    return me->recordScheduleCall(scNormalSchedule, simTime);
  }

  static CarbonStatus sClkScheduleRecord(CarbonObjectID *descr, UInt64 simTime)
  {
    CarbonModel *hdl = reinterpret_cast <CarbonModel *> (descr->mModel);
    ReplayBOM* me = hdl->getReplayBOM();
    return me->recordScheduleCall(scClkSchedule, simTime);
  }

  static CarbonStatus sDataScheduleRecord(CarbonObjectID *descr, UInt64 simTime)
  {
    CarbonModel *hdl = reinterpret_cast <CarbonModel *> (descr->mModel);
    ReplayBOM* me = hdl->getReplayBOM();
    return me->recordScheduleCall(scDataSchedule, simTime);
  }
  
  static CarbonStatus sAsyncScheduleRecord(CarbonObjectID *descr, UInt64 simTime)
  {
    CarbonModel *hdl = reinterpret_cast <CarbonModel *> (descr->mModel);
    ReplayBOM* me = hdl->getReplayBOM();
    return me->recordScheduleCall(scAsyncSchedule, simTime);
  }
  
  // For playback, replaces the generated schedule function
  static CarbonStatus sSchedulePlayback(CarbonObjectID *descr, UInt64 simTime)
  {
    CarbonModel *hdl = reinterpret_cast <CarbonModel *> (descr->mModel);
    hdl->putSimulationTime(simTime);
    ReplayBOM* me = hdl->getReplayBOM();
    return me->playScheduleCall(scNormalSchedule, simTime);
  }

  static CarbonStatus sClkSchedulePlayback(CarbonObjectID *descr, UInt64 simTime)
  {
    CarbonModel *hdl = reinterpret_cast <CarbonModel *> (descr->mModel);
    hdl->putSimulationTime(simTime);
    ReplayBOM* me = hdl->getReplayBOM();
    return me->playScheduleCall(scClkSchedule, simTime);
  }

  static CarbonStatus sDataSchedulePlayback(CarbonObjectID *descr, UInt64 simTime)
  {
    CarbonModel *hdl = reinterpret_cast <CarbonModel *> (descr->mModel);
    hdl->putSimulationTime(simTime);
    ReplayBOM* me = hdl->getReplayBOM();
    return me->playScheduleCall(scDataSchedule, simTime);
  }
  
  static CarbonStatus sAsyncSchedulePlayback(CarbonObjectID *descr, UInt64 simTime)
  {
    CarbonModel *hdl = reinterpret_cast <CarbonModel *> (descr->mModel);
    hdl->putSimulationTime(simTime);
    ReplayBOM* me = hdl->getReplayBOM();
    return me->playScheduleCall(scAsyncSchedule, simTime);
  }
  
  //! Prepares the shell for recording net stimuli
  /*!
    This calculates the
    replay input-stimulus buffer size. It does this by flattening all
    the input and deposit nets into their primitives and incrementally
    adding the number of words of each net. Then a buffer is allocated
    (dynbitvector). All the nets that can be stimulated are wrapped in
    a ShellNetRecord wrapper, and a part of the allocated buffer is
    given to each wrapper.

    On top of that, some basic directory deletion/creation and file
    opening is done to start the recording process. 
  */
  CarbonStatus recordStart(CarbonVHMMode prevMode)
  {
    if (mRecorder != NULL)
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLReplayRecordAlreadyRunning(mDBInstanceDir.c_str());
      return eCarbon_ERROR;
    }

    // cannot record if we are playing
    if (mPlayer != NULL)
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLReplayPlayerAlreadyRunning(mDBInstanceDir.c_str());
      return eCarbon_ERROR;
    }

    CarbonStatus stat = checkDBInstanceSetting();
    if (stat != eCarbon_OK)
      return stat;
    
    // check the checkpoint frequency initialization
    if (! mReplayInfo.isSaveFrequencyInit())
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLReplaySaveFreqNotInit(mDBInstanceDir.c_str());
      return eCarbon_ERROR;
    }

    // if the system directory does not exist, create it
    stat = maybeCreateSystemDir();
    if (stat != eCarbon_OK)
      // we haven't done anything yet, so we can just return here.
      return stat;

    
    // Make sure we are entering record from normal mode
    INFO_ASSERT(prevMode == eCarbonRunNormal, "Record mode can only originate from normal mode");
    mHookup->getCarbonModel()->putRunMode(eCarbonRunRecord);    
    // onDemand is disabled while recording
    mHookup->getCarbonModel()->maybeDisableOnDemand();
    
    // create a new database
    stat = openDBForRecord();
    if (stat != eCarbon_OK)
    {
      undoRecordMode();
      return stat;
    }

    
    IODBRuntime* db = mHookup->getDB();
    computeStimuliBufferFromDesign(db);
    computeResponseBufferFromDesign(db);
    computeMemoriesFromDesign(db);

    /*
      Run through the replay table, re-instrumenting nets that are
      already allocated by the api
      We must do this because nets may have already been allocated via
      carbonFindNet in normal mode and we are now switching to record
      mode.
      E.g.,
      in = carbonFindNet(hdl, "top.in");
      carbonReplayRecordStart();

      in was allocated as a wrapped net, but it is in normal mode. It
      has to be put into record mode.
    */
    reInstrumentAllocatedNets();
    
    // write the symboltable to disk
    stat = writeReplaySymTab();
    if (stat != eCarbon_OK)
    {
      undoRecordMode();
      return stat;
    }
    
    // Save the first checkpoint
    stat = mRecorder->recordCheckpoint();
    if (stat != eCarbon_OK)
    {
      undoRecordMode();
      return stat;
    }
    
    // Now check the record stream in case something happened while
    // writing to the file that was not yet detected.
    stat = mRecorder->getStatus();
    if (stat != eCarbon_OK)
    {
      undoRecordMode();
      return stat;
    }

    // Everything is ok, set the schedule function to record
    // Don't want to do this to early because returning an error means
    // that replay doesn't work, but normal operation should.
    mRecordSchedules.resetHookupSchedules(mHookup);

    // notify the user that the mode has changed.
    mReplayInfo.callModeChangeCBs(eCarbonRunNormal, eCarbonRunRecord);

    // Make sure that the next schedule call is recorded.
    mHookup->setSeenDeposit();

    return eCarbon_OK;
  }
  
  int compareCheckPoints(const UtString& chk1, const UtString& chk2);

  CarbonStatus playbackStart(CarbonVHMMode prevMode)
  {
    if (mPlayer != NULL)
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLReplayPlayerAlreadyRunning(mDBInstanceDir.c_str());
      return eCarbon_ERROR;
    }

    // Cannot playback if we are recording
    if (mRecorder != NULL)
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLReplayRecordAlreadyRunning(mDBInstanceDir.c_str());
      return eCarbon_ERROR;
    }

    // check that the replayInfo has everything we need for playback
    CarbonStatus stat = eCarbon_OK;
    if (! mReplayInfo.isDirActionInit()) {
      mHookup->getCarbonModel()->getMsgContext()->SHLReplayDirActionNotSet();
      stat = eCarbon_ERROR;
    }

    // Check the db instance directory
    if (checkDBInstanceSetting() != eCarbon_OK)
      stat = eCarbon_ERROR;

    // Return here if there has been a problem.
    if (stat != eCarbon_OK)
      return stat;
    
    stat = createWorkingArea();
    // Return if we cannot create the working directory.
    if (stat != eCarbon_OK)
      return stat;
    
    // Ok. After this point we will need to undo playback mode if an
    // error occurs.
    
    // Make sure we are entering playback from normal mode.
    INFO_ASSERT(prevMode == eCarbonRunNormal, "Playback mode can only originate from normal mode");
    
    // open the database
    stat = openDBForPlayback();
    if (stat != eCarbon_OK)
    {
      undoPlaybackMode();
      return stat;
    }

    // Read the symboltable
    stat = readReplaySymTab();
    if (stat != eCarbon_OK)
    {
      undoPlaybackMode();
      return stat;
    }

    assignPlaybackResponseBuffers();

    // before we reinstrument the nets, update any callback shadows
    // that we may have. Can do this after re-instrumentation, but
    // that could be slower
    mPlayer->syncNetChangeCBShadows();

    // Take a snapshot of the current Carbon Model in case we need to
    // recover to this point in time.
    const char* prePlaybackChkptFile = "preplay";
    // create a systemName_instanceDir_preplay filename
    /* The name has to be unique to the Carbon Model instance because several
       Carbon Models may be sharing the same work area.
    */
    UtString firstChkPt(mDBInstanceDir);
    OSReplaceDirectoryDelim(&firstChkPt, '_');
    // remove leading underscores
    while (! firstChkPt.empty() && firstChkPt[0] == '_')
      firstChkPt.erase(0, 1);
    firstChkPt << '_' << prePlaybackChkptFile << '.' << OSGetPid();
    
    UtString prePlayVHMStateFile;
    OSConstructFilePath(&prePlayVHMStateFile, mReplayInfo.getWorkArea(),
                        firstChkPt.c_str());
    ReplayCheckpointClosure chkPointClosure(mPlayer->getInputBufferBV(),
                                            mPlayer->getStimuliTouched(),
                                            mPlayer->getResponseBufferBV(),
                                            mPlayer->getTouchedResponseBuffer(),
                                            getCModelMap());
    stat = mHookup->getCarbonModel()->saveReplayCheckpoint(prePlayVHMStateFile.c_str(), chkPointClosure);
    if (stat != eCarbon_OK)
      return stat;

    // Now that the initial state is saved we can place the model in
    // playback mode
    mHookup->getCarbonModel()->putRunMode(eCarbonRunPlayback);
    /*
      Run through the replay table, re-instrumenting nets that are
      already allocated by the api
      We must do this because nets may have already been allocated via
      carbonFindNet in normal mode and we are now switching to playback
      mode.
      E.g.,
      in = carbonFindNet(hdl, "top.in");
      carbonReplayPlaybackStart();

      in was allocated as a wrapped net, but it is in normal mode. It
      has to be put into playback mode.
    */
    reInstrumentAllocatedNets();

    // onDemand is disabled while in playback.  While the onDemand
    // schedule wrapper functions won't be called, we need to consider
    // the API functions.
    mHookup->getCarbonModel()->maybeDisableOnDemand();

    bool isGood = mPlayer->runEventWheel(eCheckpoint);
    // At this point give up if there are any issues.
    if ((mPlayer->getStatus() != eCarbon_OK) ||
        ! isGood)
    {
      undoPlaybackMode();
      return eCarbon_ERROR;
    }
    
    // change the schedule to be in play mode
    mPlaybackSchedules.resetHookupSchedules(mHookup);

    // Get the current time
    CarbonModel* model = mHookup->getCarbonModel();


    // Construct the name of the initial checkpoint
    UtString chkPointPath;
    const UtString& chkpointName = mPlayer->getCheckpointFile();
    OSConstructFilePath(&chkPointPath, mDBInstanceDir.c_str(),
                        chkpointName.c_str());
    
    // Compare the initial checkpoint against our current state (which
    // was saved above). If there is a difference, warn the user that
    // the initial state may not be correct.
    int cmp = compareCheckPoints(chkPointPath, prePlayVHMStateFile);
    if (cmp != 0) {
      MsgContext* msgContext = model->getMsgContext();
      MsgContext::Severity severity = msgContext->SHLPlaybackStateDiff();
      isGood &= !MsgContext::isErrorSeverity(severity);
    }

    // At this point bail, if there is a db problem
    if ((mPlayer->getStatus() != eCarbon_OK) || ! isGood)
    {
      undoPlaybackMode();
      return eCarbon_ERROR;
    }

    // Since the checkpoint compares equally to the database's
    // checkpoint we can just use the one in the database on a
    // divergence.
    mPlayer->saveRecoverEvents();
    
    // The response buffer is already initialized to the current
    // values in the design. Update the player's expected stimuli so
    // we can do divergence detection on the next schedule call.
    mPlayer->updateStimuli();
    if (mPlaybackDiverged)
    {
      // If this is the case we have diverged before we even began. No
      // reason to even go into playback mode
      model->getMsgContext()->SHLReplayPlayerDivergedAtStart(mDBInstanceDir.c_str());
      undoPlaybackMode();
      return eCarbon_ERROR;
    }

    // Add all the registered cmodels to the player
    for (ReplayCModelMap::UnsortedLoop p = mReplayCModels.loopUnsorted();
         ! p.atEnd(); ++p)
    {
      ReplayCModel* cmodel = p.getValue();
      mPlayer->addCModel(cmodel);
    }

    // notify the user that the mode has changed.
    mReplayInfo.callModeChangeCBs(eCarbonRunNormal, eCarbonRunPlayback);

    // Make sure that the next schedule call is played.
    mHookup->setSeenDeposit();

    return eCarbon_OK;
  }
  
  //! Net instrumentation for replay
  /*!
    Based on the Carbon Model type, this does whatever is needed to make the
    net available to the caller.
    for a normal Carbon Model, this does nothing. For a replay Carbon Model, this wraps
    a net with 1to1 wrapper, if needed. Forcibles and Expr nets are
    already wrappers, so they don't get instrumented.
  */
  ShellNet* instrumentNet(ShellNet* srcNet)
  {
    const IODBRuntime* db = mHookup->getDB();

    if (db->isConstant(srcNet->getNameAsLeaf()))
      return srcNet;


    ShellNet* retNet = srcNet;
    STAliasedLeafNode* srcName = srcNet->getNameAsLeaf();
    INFO_ASSERT(srcName, "Net without a name");
    if (db->getVHMTypeFlags() & eVHMReplay)
    {
        CarbonVHMMode runMode = mHookup->getCarbonModel()->getRunMode();
        ShellNetWrapper* srcNetCastWrapper = srcNet->castShellNetWrapper();
        ShellNetWrapper1To1*  srcNetCastWrapperOne = NULL;
        if (srcNetCastWrapper)
          srcNetCastWrapperOne = srcNetCastWrapper->castShellNetWrapper1To1();
        
        ShellNetReplay* srcNetCastReplay = NULL;
        if (srcNetCastWrapperOne)
          srcNetCastReplay = srcNetCastWrapperOne->castShellNetReplay();

        // instrument the net if this is a primitive net or a
        // primitive net that is wrapped by a replay net. A replay net
        // only exists if it wraps a primitive net. A primitive net by
        // itself cannot be a wrapper.
        if ((srcNetCastWrapper == NULL) /* primitive net */
            || (srcNetCastReplay != NULL) /* replay net */)
        {
          INFO_ASSERT(mReplayTable != NULL, "Replay symboltable not allocated.");
          
          // Create/Get the same node from the replay table
          STAliasedLeafNode* replayLeaf = fetchReplayLeaf(srcName);
          ReplayData* replayData = castBOM(replayLeaf);
          
          ShellNetWrapper1To1* netWrap = replayData->getNetWrapper();
          if (netWrap) {
            /*
              Already have an allocated wrapper. Increment the
              reference count. We have to increment the reference
              count, because the user incremented the underlying
              net's reference count
              
              Note: if we are replacing the underlying net due to a
              mode change, the reference count gets overwritten, but
              accounted for.
            */
            netWrap->incrCount();
          }
          
          switch(runMode)
          {
            // We do not reinstrument nets during recovery
          case eCarbonRunRecover:
            INFO_ASSERT(0, "Reinstrumenting net in recovery mode.");
            break;
          case eCarbonRunNormal:
            if (netWrap == NULL)
              netWrap = allocateOneToOneWrap(srcNet, replayData);
            else
              replayData->revertToOriginalNet();
            break;
            
          case eCarbonRunRecord:
            {
              const ShellDataBOM* designData = ShellSymTabBOM::getStorageDataBOM(srcName);
              bool isDepositable = designData->isDepositable();
              if (srcNet->castMemory())
              {
                // we have a memory. If it is observable or
                // depositable it is depositable (for now).
                isDepositable |= designData->isObservable();
              }
              
              ShellNet *recordWrap;
              if (isDepositable) {
                // If this net is depositable, bind a record net (which
                // should have been created when record mode was
                // entered) to the primitive net.
                recordWrap = bindRecordNet(srcNet);
              } else {
                // If the net is not depositable, it's not being
                // recorded.  When record mode is entered, only
                // depositable nets (explicit and implicit) get
                // ShellNetRecord objects, stimulus buffer space, etc.
                //
                // However, It's nice to warn the user that deposits
                // to this net won't be recorded.  We'll wrap a
                // special ShellNetRecord to do this.  Note that this
                // only happens if the user has acquired a handle for
                // this net.
                recordWrap = bindRecordNetNotDepositable(srcNet);
              }
              // And then wrap the record net into a 1 to 1 wrapper
              // so we can change modes post-record.
              if (netWrap == NULL) {
                netWrap = allocateOneToOneWrap(recordWrap, replayData);
              } else {
                // The net is already wrapped, so just update with the
                // record net.
                netWrap->replaceNet(recordWrap);
              }
            }
            break;
          case eCarbonRunPlayback:
            {
              if (netWrap == NULL)
                netWrap = allocateOneToOneWrap(srcNet, replayData);
              
              ShellNetPlayback* playbackNet = bindPlaybackNet(srcNet);
              if (playbackNet == NULL)
              {
                // We never saw this net during record. 
                // We should warn but do not diverge
                UtString nameBuf;
                ShellSymTabBOM::composeName(srcName, &nameBuf, false, true);
                mHookup->getCarbonModel()->getMsgContext()->SHLUnrecordedReplayNet(nameBuf.c_str());

                // create a dummy net and add it to the non-replayable
                // list. Would like to add it to the symboltable, but
                // we would miss warning about any aliases of a
                // non-replayable net.
                playbackNet = allocPlaybackNonReplayable(srcNet);
              }
              
              ST_ASSERT(playbackNet, srcNet->getName());
              netWrap->replaceNet(playbackNet);
              break;
            }
          } // switch

          // Return the net wrapper
          retNet = netWrap;
        } // if primitive or replay net
        else if (ShellNetWrapper1ToN* oneToN = srcNetCastWrapper->castShellNetWrapper1ToN())
        {
          // this is a non-primitive, non-replay net. Recurse until we
          // find one or the other.
          
          /*
            We never return an internal net from here. We always
            return an api-level net. At this point, we know we have a
            non-primitive, non-replay net, which can only mean that
            we have a forcible or an expression net
            (currently). Return that, but instrument its internals
          */
          retNet = srcNetCastWrapper;
          
          
          ShellNetWrapper::NetVec replacedNets;
          ShellNetWrapper::NetVec subNets;
          oneToN->gatherWrappedNets(&subNets);
          for (ShellNetWrapper::NetVec::iterator p = subNets.begin(), 
                 e = subNets.end(); p != e; ++p)
          {
            // RECURSE until we get a primitive net
            ShellNet* modeNet = instrumentNet(*p);
            replacedNets.push_back(modeNet);
          }
          
          // Now replace the subnets with the instrumented version of
          // the nets.
          oneToN->replaceWrappedNets(replacedNets);
        } // else if
        else 
        {
          ST_ASSERT(srcNetCastWrapperOne, srcNet->getName());
          // It is a wrapper but not a replayNet, Get the subNet and
          // recurse further.
          ShellNet* subNet = srcNetCastWrapperOne->getNet();
          // If instrumentNet is being called multiple times on the
          // same net in the same mode, retNet will be the same as
          // srcNetCastWrapperOne after this recursion, or it will be
          // a different net based on the different mode that we are
          // switching to.
          //
          // Actually, that's only true if this is a simple wrapper
          // net, in which case it's OK to throw it away and replace
          // it with the instrumented version.  If this is a
          // visibility net, the wrapper is not just a pass-through
          // and needs to be preserved.
          ShellNet* instrNet = instrumentNet(subNet);
          ShellVisNet* visNet = srcNetCastWrapperOne->castShellVisNet();
          if (visNet != NULL) {
            visNet->replaceNet(instrNet);
          } else {
            retNet = instrNet;
          }
        }
    } // if
    
    return retNet;
  }

  // schedule() calls this during playback.
  CarbonStatus playScheduleCall(const char scheduleType, UInt64 simTime)
  {
    CarbonStatus stat = checkForDivergence(scheduleType);
    if (! mPlaybackDiverged)
    {
      mPlayer->runNetChangeCBs();
      mPlayer->clearTouchedStimuli();
    }
    else
    {
      mPlayer->preRecover(scheduleType);
      return recover(mRecoverContext, scheduleType, simTime);
    }
    
    return stat;
  }

  // Record the current stimuli and responses to disk
  CarbonStatus recordScheduleCall(const char scheduleType, UInt64 simTime)
  {
    // Record the stimuli before calling schedule. This is needed in
    // order to share stim and response storage for memories.
    //
    mRecorder->recordStimuli();
    bool isGood = mRecorder->getStatus() == eCarbon_OK;
    
    // Run the schedule regardless of the recording session status.
    CarbonModel* model = mHookup->getCarbonModel();
    CarbonStatus ret;
    switch(scheduleType) {
      case scNormalSchedule:
        ret = mDesignSchedules.callSchedule(model, simTime);
        break;

      case scClkSchedule:
        ret = mDesignSchedules.callClkSchedule(model, simTime);
        break;

      case scDataSchedule:
        ret = mDesignSchedules.callDataSchedule(model, simTime);
        break;

      case scAsyncSchedule:
        ret = mDesignSchedules.callAsyncSchedule(model, simTime);
        break;

      default:
        INFO_ASSERT(0, "Unrecognized schedule event");
        ret = eCarbon_ERROR;
        break;
    } // switch
    
    if ((ret != eCarbon_ERROR) & isGood)
    {
      // everything is ok. Update and record the response.
      mRecorder->recordScheduleCall(scheduleType, simTime, ret);
      // This adds to the schedule call total and takes a checkpoint if
      // we have reached our call limit.
      isGood = mRecorder->updateCheckpointStatus();
    }
    else 
      isGood = false;
    
    if (! isGood)
      recordStop();
    
    // Always return the status of the schedule.
    return ret;
  }
  
  //! Get a pointer to the replay info structure
  CarbonReplayInfo* getReplayInfo()
  {
    return &mReplayInfo;
  }

  CarbonStatus scheduleRecordCheckpoint()
  {
    if (mRecorder == NULL)
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLReplayRecordNotRunning();
      return eCarbon_ERROR;
    }
    
    mRecorder->scheduleCheckpoint();
    return eCarbon_OK;
  }

  //! Stop the recording process
  CarbonStatus recordStop()
  {
    if (mRecorder == NULL)
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLReplayRecordNotRunning();
      return eCarbon_ERROR;
    }

    CarbonStatus stat = eCarbon_OK;
    UtString errMsg;
    if (! mRecorder->close(&errMsg))
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(errMsg.c_str());
      stat = eCarbon_ERROR;
    }

    undoRecordMode();

    mReplayInfo.callModeChangeCBs(eCarbonRunRecord, eCarbonRunNormal);

    // If this is being called by the user. We need to make sure that
    // the next schedule call does not get optimized away
    mHookup->setSeenDeposit();

    return stat;
  }

  //! Stop the playback process
  CarbonStatus playbackStop()
  {
    CarbonModel* model = mHookup->getCarbonModel();
    if (mPlayer == NULL)
    {
      model->getMsgContext()->SHLReplayPlayerNotRunning();
      return eCarbon_ERROR;
    }

    // We are in-between schedule calls. The database is set to do
    // divergence detection for the next schedule, but the recovery
    // isn't ready until hasDiverged() has been called. hasDiverged()
    // sets the mStimCheck var to mNumEvents, which allows the
    // recovery wheel to spin to the current time.
    // Instead of calling hasDiverged() here, I'll directly sync the
    // mStimCheck variable.
    mPlayer->syncStimCheck();
    mPlayer->saveRecoverEvents();
    mPlayer->reportDivergenceReason("carbonReplayPlaybackStop() requested");

    CarbonStatus ret = recoverNoSchedule(mRecoverContext, model->getSimulationTime());
    // If this is being called by the user. We need to make sure that
    // the next schedule call does not get optimized away
    mHookup->setSeenDeposit();
    return ret;
  }
  
  //! Adds a net value change callback.
  /*!
    Currently, these are only used by the playback mechanism to call
    user net value change callbacks. But, this could also be used by
    the recorder at some point if we have a mode where we know which
    nets have callbacks on them in playback mode so we can simply
    record when to call a callback on a net. Currently, we record
    which nets have changed so any playback net can have a net value
    change callback on it, even if there wasn't one during record.
  */
  void addNetValueChangeCB(CarbonNetValueCBData* cbData)
  {
    CBDataValuePair* cbDataValue = new CBDataValuePair(cbData);
    mNetValueChangeCBs.push_back(cbDataValue);
  }

  //! Called before recordInitialSchedule to record pre-init stimuli
  void recordInitialScheduleStimuli()
  {
    if (mRecorder)
      // Should record all zeros for the stimuli, but we need to do
      // this so we can do incremental value updates on the buffer.
      mRecorder->recordStimuli();
  }

  void recordInitialSchedule(void* cmodelData, 
                             void* reserved1, 
                             void* reserved2)
  {
    // If we are recording, record the event
    if (mRecorder)
      mRecorder->recordInitialSchedule();
    // if we are in playback but not recover save the passed in data
    else if (mPlayer && 
             mHookup->getCarbonModel()->getRunMode() == eCarbonRunPlayback)
    {
      mDesignSchedules.saveInitSchedData(cmodelData, reserved1, reserved2);
      (void) playScheduleCall(scInitialSchedule, 0);
    }
  }

  void registerCModel(void* cmodelData, void* userData, UInt32 context,
                      const char* name,
                      UInt32 numInputs, UInt32 numOutputs,
                      CModelPlaybackFn fn)
  {
    ReplayCModel* replayCModel = NULL;
    ReplayCModelMap::iterator p = mReplayCModels.find(cmodelData);
    if (p == mReplayCModels.end())
    {
      replayCModel = new ReplayCModel(cmodelData, userData, name, mReplayCModels.size());
      mReplayCModels[cmodelData] = replayCModel;

      // For playback, this can be called after the player has been
      // started in between a design_create(noInit) and a
      // carbonInitialize. We can't assume that the player has all
      // the information that it needs at this point. So, we have to
      // register the model with the player as well.
      if (mPlayer)
        mPlayer->addCModel(replayCModel);
    }
    else
      replayCModel = p->second;

    replayCModel->addContext(context, numInputs, numOutputs, fn);
  }

  void preRecordCModel(void* cmodelData, UInt32 context, UInt32* outputs)
  {
    if (mRecorder)
    {
      ReplayCModelMap::iterator p = mReplayCModels.find(cmodelData);
      INFO_ASSERT(p != mReplayCModels.end(), "Unknown cmodel passed into preRecordCModel");
      ReplayCModel* replayCModel = p->second;
#if REPLAY_CMODEL_VERBOSE
      UtIO::cout() << "Time: " << mHookup->getCarbonId()->getTime() << UtIO::endl;
#endif
      replayCModel->preRecordCall(mRecorder->getRecordStream(), context, outputs);
    }
  }

  void recordCModel(void* cmodelData, UInt32 context,
                    UInt32* inputs, UInt32* outputs)
  {
    if (mRecorder)
    {
      ReplayCModelMap::iterator p = mReplayCModels.find(cmodelData);
      INFO_ASSERT(p != mReplayCModels.end(), "Unknown cmodel passed into recordCModel");
      ReplayCModel* replayCModel = p->second;
      replayCModel->recordCall(mRecorder->getRecordStream(), context, inputs, outputs);
    }
  }
  
  CarbonModel::CModelRecoveryStatus 
  getCModelCallOutput(void* cmodelData, UInt32 context, UInt32** outputs)
  {
    ReplayCModelMap::iterator p = mReplayCModels.find(cmodelData);
    INFO_ASSERT(p != mReplayCModels.end(), "Unknown cmodel passed into getCModelValues");
    ReplayCModel* replayCModel = p->second;

    return mRecoverContext.getCModelCallOutput(replayCModel, context, outputs);
  }

  CarbonStatus setVerboseDivergence(bool verbose)
  {
    mVerboseDivergence = verbose;
    return eCarbon_OK;
  }

  bool isVerboseDivergence() const { return mVerboseDivergence; }

  ReplayCModelMap *getCModelMap() { return &mReplayCModels; }

  //! Records that a net has been found with carbonFindNet()
  /*!
    For a given user-visible net, save all the wrapped primitive nets.
    This is later used to determine which primitive nets need special
    record nets created, in order to warn that deposits to those nets
    will not be recorded.

    \param net ShellNet returned by carbonFindNet()
   */
  void addFoundNet(ShellNet *net)
  {
    // This is the high-level net seen by the user.  It may wrap
    // several primitive nets, and those are what we care about.
    ShellNetWrapper::NetVec primitiveNets;
    LeafSet coveredSet;
    flattenNet(net, &primitiveNets, &coveredSet);
    // Add all the primitive nets to our set
    for (ShellNetWrapper::NetVec::iterator iter = primitiveNets.begin(); iter != primitiveNets.end(); ++iter) {
      ShellNet *primNet = *iter;
      mFoundNets.insert(primNet);
    }
  }

private:
  CarbonHookup* mHookup;
  CarbonReplayInfo mReplayInfo;
  
  // Replay symbol table
  STSymbolTable* mReplayTable;
  
  ReplayRecorder* mRecorder;
  ReplayPlayer* mPlayer;
  // The recover context if we go into recover mode
  /*!
    This has to be live for the entire recovery process possibly even
    after the playback mode has been undone (deleted mPlayer). This is
    because a cmodel that caused a divergence still needs the output
    that was found to be different during the 'normal' schedule call
    after playback is undone. During that last schedule call by the
    recovery process, the cmodel calls getCModelCallOutput, we see
    that it is the divergence point and we call the mode change at
    that point.
  */
  RecoverContext mRecoverContext;

  ScheduleFnContainer mDesignSchedules;
  ScheduleFnContainer mPlaybackSchedules;
  ScheduleFnContainer mRecordSchedules;
  
  UtString mDBInstanceDir;
  
  DynBitVectorFactory mDynFactory;

  bool mPlaybackDiverged;
  bool mVerboseDivergence;

  CBDataValuePairVec mNetValueChangeCBs;
  
  typedef UtArray<ReplayData*> ReplayDataVec;

  typedef UtHashMap<ShellNet*, ShellNetPlaybackNotReplayable*> PrimToNonPlayable;
  PrimToNonPlayable mNonPlaybackNets;

  ReplayCModelMap mReplayCModels;

  typedef UtHashSet<ShellNet*> ShellNetSet;
  ShellNetSet mFoundNets;

  ShellNetPlaybackNotReplayable* allocPlaybackNonReplayable(ShellNet* srcNet)
  {
    ShellNetPlaybackNotReplayable* nonReplayNet = NULL;
    PrimToNonPlayable::iterator p = mNonPlaybackNets.find(srcNet);
    if (p != mNonPlaybackNets.end())
    {
      nonReplayNet = p->second;
      nonReplayNet->incrCount();
    }
    else
    {
      nonReplayNet = new ShellNetPlaybackNotReplayable(srcNet, mHookup->getCarbonModel());
      mNonPlaybackNets[srcNet] = nonReplayNet;
    }
    return nonReplayNet;
  }

  static void sRecoverNetCB(CarbonObjectID* closure, ShellNetPlayback* playNet, ReplayData* replayData, CarbonUInt32* val, CarbonUInt32* mask, CarbonUInt32* xdrv)
  {
    CarbonModel *model = closure->getModel ();
    ShellNet* origNet = replayData->getOriginalNet();
    if (mask == NULL) {
      origNet->fastDeposit(val, xdrv, model);
    } else {
      // This is a state output net with a masked deposit.  Examine
      // the current value, mask in the new bits, and redeposit the
      // whole thing.
      UInt32 numWords = playNet->getNumWords();
      UInt32* tempBuf = CARBON_ALLOC_VEC(UInt32, numWords);
      origNet->examine(tempBuf, NULL, ShellNet::eXDrive, model);
      for (UInt32 i = 0; i < numWords; ++i) {
        tempBuf[i] &= ~mask[i];
        tempBuf[i] |= (val[i] & mask[i]);
      }
      origNet->fastDeposit(tempBuf, xdrv, model);
      CARBON_FREE_VEC(tempBuf, UInt32, numWords);
    }
    // Clocks may have been deposited to more than once. We recorded
    // that by monitoring the change array. So, here, we are just
    // recomputing the change mask based on the current value.

    // Also do this if this is a state output that's been
    // deposited twice, i.e. it has an encoded value change.
    if (playNet->isSimpleClock() || playNet->isEncodedValueChanged())
      origNet->recomputeChangeMask();
  }

  // Returns the original memory net given a player memory net
  CarbonMemory* getOriginalMem(ShellNetPlaybackMem* playerMem)
  {
    STAliasedLeafNode* thisTablesLeaf = findReplayLeaf(playerMem->getNameAsLeaf());
    ST_ASSERT(thisTablesLeaf, playerMem->getName());
    ReplayData* replayData = castBOM(thisTablesLeaf);
    ST_ASSERT(replayData, thisTablesLeaf);
    ShellNet* origNet = replayData->getOriginalNet();
    ST_ASSERT(origNet, thisTablesLeaf);
    CarbonMemory* origMem = origNet->castMemory();
    ST_ASSERT(origMem, origNet->getName());
    return origMem;
  }

  void recoverDeposits(ReplayDataVec& inputNets, CarbonModel* model)
  {
    for (ReplayDataVec::iterator p = inputNets.begin(), 
           e = inputNets.end(); p != e; ++p)
    {
      ReplayData* replayData = *p;
      ShellNetPlayback* playNet = replayData->getPlaybackNet();
      // This can be useful in debugging. This will print out the
      // name of the net and the value that is being deposited.
#if 0
      {
        UInt32* val;
        UInt32* drv;
        playNet->getEncodedDepositBuffers(&val, &drv);
        UtString name;
        playNet->getName()->compose(&name);
        UtIO::cout() << name << ":\n\tValue: ";
        UInt32 numWords = playNet->getNumWords();
        DynBitVector valBV(numWords * 32, val, numWords);
        valBV.printHex();
        if (drv)
        {
          UtIO::cout() << "\tDrive: ";
          DynBitVector drvBV(numWords * 32, drv, numWords);
          drvBV.printHex();
        }
      }
#endif
      if (playNet->isTouched()) {
        UInt32* val;
        UInt32* drv;
        UInt32* mask = NULL;
        // We need to use the encoded deposit buffers, if they exist.
        playNet->getEncodedDepositBuffers(&val, &drv);
        if (playNet->isMasked()) {
          // If this net's access was masked, the recovery function
          // needs to account for that.
          mask = playNet->getMaskBuffer();
        }
        sRecoverNetCB(model->getObjectID (), playNet, replayData, val, mask, drv);
      }
    }
  }
  
  // This gets called during recovery. This sets the input buffer to
  // the 'expected value' read from the db, runs net callbacks on
  // those and updates the original nets. In addition, external memory
  // writes are applied to the original memory nets.
  void applyStimuliToOrigNets(ReplayDataVec& inputReplayData, 
                              CarbonModel* model)
  {
    // place the expected stimuli into the input buffer
    mPlayer->setInputToExpected();
    // now run the deposits
    recoverDeposits(inputReplayData, model);

    // and now for the memories.
    for (SInt32Vec::iterator p = mPlayer->mClosure.mExternalMemChangeIndices.begin(),
           e = mPlayer->mClosure.mExternalMemChangeIndices.end(); 
         p != e; ++p)
    {
      SInt32 index = *p;

      // Get the original memory net. We do this by cross-referencing
      // the name of the playback memory.
      ShellNetPlaybackMem* playerMem = mPlayer->getPlaybackMem(index);
      CarbonMemory* origMem = getOriginalMem(playerMem);
      
      mPlayer->transferExpectedStimToMem(origMem, index);
    }
    // clear the memory changes
    mPlayer->mClosure.clearExternalMemChanges();
  }

  CarbonStatus callInitialSchedule()
  {
    void* cmodelData;
    void* reserved1;
    void* reserved2;
    mDesignSchedules.getInitSchedData(&cmodelData, &reserved1,
                                      &reserved2);
    return mHookup->getCarbonModel()->initialize(cmodelData, reserved1, reserved2);
  }
  
  /* This saves the current state of the external stimuli, then loads
     the state from the last checkpoint, applying stimuli from that
     checkpoint to the current state, at which point, the saved
     current state is applied. Once the vhm state is in sync with the
     simulation, we currently go back into normal mode. Eventually, we
     would like to go back into record mode (TBD). This does not run
     the current schedule call, if we are indeed inside a schedule
     call. recover() does that.
  */
  CarbonStatus recoverNoSchedule(RecoverContext& recoverContext,
                                 CarbonUInt64 simTime)
  {
    // initialize the recover context
    recoverContext.init();

    // Cannot call the checkpoint callback function while rerunning
    // the event file. Temporarily disable it.
    mReplayInfo.setAllCheckpointCBsDisabled(true);

    /* Save off the current stimuli. We need to apply that at the
       point of divergence to the Carbon Model.
       This takes what the user deposited and puts it into the
       expected stimuli buffer, which gets copied into the
       recoverContext.

       Note: the Player's mExternalMemChanges and the corresponding
       ShellNetPlaybackMems will not be modified during recovery, so
       we can just keep those values there and use them once we get
       to the divergence point.
    */
    mPlayer->saveCurrentStimuli(recoverContext);

    // Get the number of events we need to go through to get to the
    // divergence point.
    UInt64 divergencePoint = mPlayer->getDivergencePoint();
    
    // Call the mode change now. We need to do this in case the user
    // deposits or examines values. Any deposits will be lost at this
    // point, but examines are definitely valid. 
    mReplayInfo.callModeChangeCBs(eCarbonRunPlayback, eCarbonRunRecover);

    // open the event file that begins recovery
    CarbonStatus stat = mPlayer->setRecoverMode(recoverContext);
    if (stat != eCarbon_OK)
    {
      // we are in a very bad state. We cannot allow simulation
      // anymore and we cannot crash. So, set the status to
      // eCarbon_FINISH
      return eCarbon_FINISH;
    }

    // go to the checkpoint. Impossible for there not to be one.
    INFO_ASSERT(mPlayer->runEventWheel(eCheckpoint), "Missing Checkpoint in recovery.");
    
    // Restore the Carbon Model to the current checkpoint
    CarbonModel* model = mHookup->getCarbonModel();
    UtString chkPointFilePath;
    const UtString& checkpointName = mPlayer->getCheckpointFile();
    OSConstructFilePath(&chkPointFilePath, mDBInstanceDir.c_str(), checkpointName.c_str());
    
    // Temporarily put this in normal mode so we can reinstrument
    // the nets. We need to do this so the net value change
    // callbacks get updated with respect to the actual Carbon Model.
    // Also, the simRestore does a shadow sync, so we want the
    // original nets in place
    model->putRunMode(eCarbonRunNormal);
    reInstrumentAllocatedNets();
    // Now that we have normal nets, place in recover mode.
    model->putRunMode(eCarbonRunRecover);    
    
    // Now do the restore from the checkpoint.
    ReplayCheckpointClosure chkPointClosure(&(mPlayer->mClosure.mExpectedStimuli),
                                            &(mPlayer->mClosure.mStimuliTouched),
                                            mPlayer->getResponseBufferBV(),
                                            mPlayer->getTouchedResponseBuffer(),
                                            getCModelMap());
    stat = model->restoreReplayCheckpoint(chkPointFilePath.c_str(), &chkPointClosure);
    if (stat != eCarbon_OK)
    {
      // we are in a very bad state. We cannot allow simulation
      // anymore and we cannot crash. So, set the status to
      // eCarbon_FINISH
      return eCarbon_FINISH;
    }

    // gather playback input nets. Gathering the replaydata objects
    // for each net allows us to have both the playback net and the
    // original together. As we re-read the database, the stimuli
    // Touched indicators will be updated and we can use that to
    // know when to deposit to the original net.
    ReplayDataVec inputReplayData;
    for (STSymbolTable::NodeLoop q = mReplayTable->getNodeLoop();
         ! q.atEnd(); ++q)
    {
      STSymbolTableNode* node = *q;
      STAliasedLeafNode* leaf = node->castLeaf();
      if (leaf)
      {
        ReplayData* replayData = castBOM(leaf);
        if (replayData->getInputBufferIndex() >= 0)
        {
          // storage playback node
          ShellNetPlayback* playNet = replayData->getPlaybackNet();
          ST_ASSERT(playNet, leaf);
          inputReplayData.push_back(replayData);
        }
      } // if leaf
    } // for

    if (stat == eCarbon_OK)
    {
      // turn off model components that may affect the environment,
      // while we recover.
      CarbonInitFlags origInit = mHookup->getInitFlags();
      int modFlags = origInit & ~eCarbon_ClockGlitchDetect;
      mHookup->putInitFlags(modFlags);

      // put the normal schedule back into the hookup
      mDesignSchedules.resetHookupSchedules(mHookup);
      
      /* now run through the event wheel looking for stimuli until we
         get to where we ended. Each time we find stimuli, run the
         callbacks. Note that the number of events is 1 based. There
         is no 0th event. So, we need to stop if we are equal to or
         greater than the player's number of events AFTER we run the
         event wheel.
      */
      while (mPlayer->runEventWheel(eSchedule) && 
             mPlayer->numEvents() < divergencePoint)
      {
#if 0
        // When debugging recovery problems, it's nice to set a
        // conditional breakpoint on a particular event number.  I was
        // having problems getting GDB to do this by diving into the
        // mPlayer object.  This is much easier.
        UInt64 numEvents = mPlayer->numEvents();
        (void) numEvents;
#endif
        applyStimuliToOrigNets(inputReplayData, model);

#ifndef PTHREADS
        model->licCheck();
#endif
        
        /* Finally, run the low-level schedule.
           NOTE: the low-level schedule is the schedule generated by
           cbuild. This is not the CarbonModel schedule which
           runs the callbacks and waveform update.

           We don't want those to be run until we reach the divergence
           point.
           Also, we do not move time forward here. Just use time 0. At
           the divergence point we will set time to what the caller
           set it to be.
        */
        switch(mPlayer->mClosure.mScheduleType)
        {
        case scNormalSchedule:
          mHookup->runSchedule(0);
          break;
        case scClkSchedule:
          mHookup->runClkSchedule(0);
          break;
        case scDataSchedule:
          mHookup->runDataSchedule(0);
          break;
        case scAsyncSchedule:
          mHookup->runAsyncSchedule(0);
          break;
        case scInitialSchedule:
          callInitialSchedule();
          break;
        default:
          INFO_ASSERT(0, "Unrecognized schedule event");
          break;
        }

        // Do whatever needs to be done inside the recoverContext and
        // player at the end of a schedule during recovery.
        mPlayer->recoverPostSchedule(recoverContext);
      } // while

      // update any net value change callbacks. We only want to call
      // change callbacks for the nets that have changed at the
      // divergence point.
      model->updateNetValueChangeCBShadows(false);
      
      // we are now at the divergence point
      // The copied closure's expected stimuli was set to the user's
      // actual input.
      mPlayer->restoreCurrentStimuli(recoverContext);
      
      // now run the deposit callbacks
      recoverDeposits(inputReplayData, model);

      // Same for external memories
      SInt32HashSet* externalMemChanges = mPlayer->getExternalChangedMemSet();
      for (SInt32HashSet::UnsortedLoop p = externalMemChanges->loopUnsorted();
           !p.atEnd(); ++p)
      {
        SInt32 index = *p;
        ShellNetPlaybackMem* playMem = mPlayer->getPlaybackMem(index);
        CarbonMemory* origMem = getOriginalMem(playMem);

        ReplayMemChangeMap* writtenAddrs = playMem->getWrittenAddrs();
        SInt32DynBVMap& addrs = writtenAddrs->mMem;
        for (SInt32DynBVMap::UnsortedLoop q = addrs.loopUnsorted();
             ! q.atEnd(); ++q)
        {
          SInt32 addr = q.getKey();
          const DynBitVector& value = q.getValue();
          origMem->depositMemory(addr, value.getUIntArray());
        }
      }

      bool wheelInterrupted = mPlayer->mClosure.mWheelInterrupt != 0;

      // at this point we no longer need the player. We can undo the
      // playback mode, which reinstruments the nets into normal mode
      undoPlaybackMode();

      // Temporarily reset the time to the user's time before calling
      // the callback, so the user doesn't think time went backwards
      model->putSimulationTime(simTime);

      // reset the Carbon Model's init flags
      mHookup->putInitFlags(origInit);

      // Restore the checkpoint callbacks
      mReplayInfo.setAllCheckpointCBsDisabled(false);

      // Do not call the mode change cb if a cmodel caused the
      // divergence (ie, the wheel was interrupted). The cmodel
      // recover queue will call it.
      if (! wheelInterrupted)
        // Call the change mode function right before the schedule
        // function is to be called.
        mReplayInfo.callModeChangeCBs(eCarbonRunRecover, eCarbonRunNormal);
    }
    else
    {
      // we are in a very bad state. We cannot allow simulation
      // anymore and we cannot crash. So, set the status to
      // eCarbon_FINISH
      stat = eCarbon_FINISH;
    }

    return stat;
  }

  // This calls recoverNoSchedule and then runs the schedule at the
  // current time.
  CarbonStatus recover(RecoverContext& recoverContext,
                       const char scheduleType, CarbonUInt64 simTime)
  {
    CarbonStatus stat = recoverNoSchedule(recoverContext, simTime);
    if (stat == eCarbon_OK)
    {
      CarbonModel* model = mHookup->getCarbonModel();
      // Reset the model to time 0, so the schedule
      // sees a time change.
      model->putSimulationTime(0);
      
      // Run the schedule to the current time, but use the CarbonModel
      // schedule function this time, to update waves if started in
      // the mode change fn, and call callbacks. Use the simTime
      // passed in, since that is the actual current time.
      switch (scheduleType)
      {
      case scNormalSchedule:
        // We are about to call the same schedule twice. Decrement it
        // by one.
        model->decrTotNumSchedCalls();
        stat = model->schedule(simTime);
        break;
      case scClkSchedule:
        // We are about to call the same schedule twice. Decrement it
        // by one.
        model->decrTotNumSchedCalls();
        stat = model->clkSchedule(simTime);
        break;
      case scDataSchedule:
        // Note: the data schedule is usually called on the same time
        // slice as the clock schedule, but the data schedule doesn't
        // care about time changes (at least from what I can see).
        stat = model->dataSchedule(simTime);
        break;
      case scAsyncSchedule:
        // Note: the async schedule is usually called on the same time
        // slice as the clock schedule. Or the clock schedule may not
        // be called if no clocks change.
        stat = model->asyncSchedule(simTime);
        break;
      case scInitialSchedule:
        callInitialSchedule();
        break;
      default:
        INFO_ASSERT(0, "Unrecognized schedule event");
        break;
      }
    }
    return stat;
  }

  CarbonStatus createWorkingArea()
  {
    if (! mReplayInfo.isWorkAreaInit())
    {
      // default to systemName_instanceDir
      UtString tmp(mDBInstanceDir);
      OSReplaceDirectoryDelim(&tmp, '_');
      // remove leading underscores
      while (! tmp.empty() && tmp[0] == '_')
        tmp.erase(0, 1);
      mReplayInfo.putWorkArea(tmp.c_str());
    }

    CarbonStatus stat = eCarbon_OK;
    
    // Check if the directory is there and is writable. If so,
    // return. Else, create the directory
    UtString errMsg;
    OSStatEntry dirEntry;
    if (OSStatFileEntry(mReplayInfo.getWorkArea(), &dirEntry, &errMsg) != 0)
    {
      // the directory does not exist. Create it
      errMsg.clear();
      if (OSMkdir(mReplayInfo.getWorkArea(), 0777, &errMsg) != 0)
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(errMsg.c_str());
        stat = eCarbon_ERROR;
      }
    }
    else
    {
      // check that is it is a writable and readable directory
      if (! dirEntry.isDirectory())
      {
        errMsg.clear();
        errMsg << mReplayInfo.getWorkArea() << ": Path is not a directory.";
        mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(errMsg.c_str());
        // return here
        return eCarbon_ERROR;
      }

      if (! dirEntry.isWritable())
      {
        errMsg.clear();
        errMsg << mReplayInfo.getWorkArea() << ": directory does not have write access.";
        mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(errMsg.c_str());
        stat = eCarbon_ERROR;
      }

      if (! dirEntry.isReadable())
      {
        errMsg.clear();
        errMsg << mReplayInfo.getWorkArea() << ": directory does not have read access.";
        mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(errMsg.c_str());
        stat = eCarbon_ERROR;
      }
    }
    return stat;
  }
  
  CarbonStatus checkDBInstanceSetting()
  {
    CarbonStatus ret = eCarbon_OK;
    if (! mReplayInfo.isSystemDirInit())
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLReplaySystemDirNotSet();
      ret = eCarbon_ERROR;
    }
    
    if (! mReplayInfo.isInstanceDirInit())
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLReplayInstanceDirNotSet();
      ret = eCarbon_ERROR;
    }

    mReplayInfo.assembleDBName(&mDBInstanceDir);
    return ret;
  }

  void undoRecordMode()
  {
    // Always go into normal mode when leaving record mode
    mHookup->getCarbonModel()->putRunMode(eCarbonRunNormal);
    reInstrumentAllocatedNets();

    // Loop through the replay table and null out the record
    // information
    for (STSymbolTable::NodeLoop q = mReplayTable->getNodeLoop();
         ! q.atEnd(); ++q)
    {
      STSymbolTableNode* node = *q;
      STAliasedLeafNode* leaf = node->castLeaf();
      if (leaf)
      {
        ReplayData* replayData = castBOM(leaf);
        replayData->clearRecordData();
      } // if leaf
    } // for
    
    delete mRecorder;
    mRecorder = NULL;
    mDesignSchedules.resetHookupSchedules(mHookup);
    // onDemand can resume now that playback is in normal mode.
    mHookup->getCarbonModel()->maybeReEnableOnDemand();
  }

  void undoPlaybackMode()
  {
    // Always go into normal mode when leaving play mode
    mHookup->getCarbonModel()->putRunMode(eCarbonRunNormal);
    reInstrumentAllocatedNets();

    // Loop through the replay table and null out the playback
    // information
    for (STSymbolTable::NodeLoop q = mReplayTable->getNodeLoop();
         ! q.atEnd(); ++q)
    {
      STSymbolTableNode* node = *q;
      STAliasedLeafNode* leaf = node->castLeaf();
      if (leaf)
      {
        ReplayData* replayData = castBOM(leaf);
        replayData->clearPlaybackData();
      } // if leaf
    } // for
    
    delete mPlayer;
    mPlayer = NULL;
    mPlaybackDiverged = false;
    
    mDesignSchedules.resetHookupSchedules(mHookup);
    // onDemand can resume now that playback is in normal mode.
    mHookup->getCarbonModel()->maybeReEnableOnDemand();
  }
  
  inline CarbonStatus checkForDivergence(const char scheduleType)
  {
    mPlaybackDiverged |= mPlayer->hasDiverged();
    
    // We have to do separate event wheel spins to get the response
    // and stimuli, in order to avoid the typical fencepost error.
    // We may have the response, but the next stimuli may not be
    // there.
    // grab the response
    mPlaybackDiverged |= mPlayer->updateResponse();

    // must check the schedule type after updating the response
    // since the schedule type will be changed to the current schedule
    // once we run the event wheel. However, this must be done before
    // the stimuli update because we can only hit the eof of an event
    // file AFTER a response buffer and before a stimuli buffer.
    mPlaybackDiverged |= scheduleType != mPlayer->mClosure.mScheduleType;
    
    // The status will be overwritten when we update the stimuli
    CarbonStatus stat = mPlayer->mClosure.mScheduleStatus;

    // grab the next stimuli
    /* don't care if the stimuli are there or not. Either we will
       diverge when we check the stimuli or we will diverge when we go
       to get the response.
    */
    mPlayer->updateStimuli();
    
    return stat;
  }
  
  // Assign playback response buffers to playback nets
  /*!
    When storage playback nets are created during the symboltable
    read, the response buffer is NOT yet allocatable. So, we leave
    those NULL. The playback nets only have the deposit buffers
    currently.
    After all the storage playback nets are allocated (the result of
    the symtab read) we may have depositable nets that have no
    responses. A simple input for example, would only have itself as a
    response. We did not record a response for such a net. These can
    be identified by querying the list of response nets when coming
    across a node that has a playback net in its data bom.

    We may also have observable nets that are not depositable. In this
    case, no deposits were recorded for these nets. These can be
    identified by checking if there is a playback net in the data bom
    while running through the response nets.
  */
  void assignPlaybackResponseBuffers()
  {
    // First assign the response buffer parts to the response nets.
    UInt32 bufferIndex = 0;
    UInt32* inputWords = mPlayer->mClosure.getResponseBuffer();

    typedef UtHashMap<STAliasedLeafNode*, ShellNetPlaybackResponse*> LeafResponseMap;
    LeafResponseMap responseNodes;
    for (ReplayPlayer::ResponseNetVecLoop p = mPlayer->loopResponseNets();
         ! p.atEnd(); ++p)
    {
      ShellNetPlaybackResponse* response = *p;
      ShellNet* subNet = response->getNet();
      UInt32* modelValue;
      UInt32* modelMask;
      UInt32* modelDrive;
      UInt32 numWords = subNet->getNumUInt32s();
      bool isTristate = subNet->isTristate();
      UInt32 beginIndex = bufferIndex;
      // State outputs only need masks for stimulus, not response, so
      // pass false for doMask parameter.
      bufferIndex = assignReplayBufferChunk(&modelValue, &modelMask, &modelDrive, 
                                            numWords, 
                                            false,
                                            isTristate, 
                                            bufferIndex,
                                            inputWords);
      
      subNet->examine(modelValue, modelDrive, ShellNet::eIDrive, mHookup->getCarbonModel());
      if (modelDrive)
      {
        // the idrive is inverted to make code-generation easier,
        // although it makes my life a living hell.
        // invert it, since we are giving the idrive to a
        // ShellNetTristate underneath.
        for (UInt32 i = 0; i < numWords; ++i)
          modelDrive[i] = ~modelDrive[i];
        modelDrive[numWords - 1] &= CarbonValRW::getWordMask(response->getWidth());
      }
      response->putModelValueBuffers(modelValue, modelDrive);
      mPlayer->mapNetToResponseBuffer(response, beginIndex, numWords, isTristate);

      // add the response net's aliased leaf node to the set of all
      // response nets, so I can assign response buffers to those that
      // have been recorded.
      STAliasedLeafNode* designLeaf = response->getNameAsLeaf();
      STAliasedLeafNode* replayLeaf = findReplayLeaf(designLeaf);
      ST_ASSERT(replayLeaf, designLeaf);
      responseNodes[replayLeaf] = response;
    }

    
    // now run through the symboltable updating any allocated playback
    // nets with response buffers
    for (STSymbolTable::NodeLoop q = mReplayTable->getNodeLoop();
         ! q.atEnd(); ++q)
    {
      STSymbolTableNode* node = *q;
      STAliasedLeafNode* leaf = node->castLeaf();
      if (leaf)
      {
        ReplayData* replayData = castBOM(leaf);
        ShellNetPlayback* playNet = replayData->getPlaybackNet();

        // Skip memories. Those are handled separately. They are not
        // assigned a piece of the response buffer.
        if (playNet && playNet->castMemory() != NULL)
          continue;


        UInt32* modelVal = NULL;
        UInt32* modelDrv = NULL;
        UInt32* modelMask = NULL;
        LeafResponseMap::iterator r = responseNodes.find(leaf);
        ShellNetPlaybackResponse* responseNet = NULL;
        if (r != responseNodes.end())
        {
          responseNet = r->second;
          responseNet->getModelValueBuffers(&modelVal, &modelDrv);
        }

        // We need to check the ReplayData for this node to make sure it
        // has a playback net if it is a response net. This response
        // net may be an observable net that has no stimulus. Here, we
        // need to set the deposit buffers to be the same pointers as
        // the model buffers. Note that the touched buffer from the
        // response is given to the play net. This is because playNet
        // is not depositable, but in case a deposit goes through we
        // do not want to crash. The touched buffer gets overwritten
        // by the database read during response update in such a case.
        if ((playNet == NULL) && (responseNet != NULL))
          playNet = allocateBufferedPlaybackNet(responseNet->getNet(), 
                                                replayData,
                                                modelVal,
                                                modelMask,
                                                modelDrv,
                                                responseNet->getNumUInt32s(),
                                                responseNet->isTristate(),
                                                false,
                                                responseNet->getTouchedIndicator(),
                                                false, false);
        
        if (playNet)
        {
          // This needs model values
          if (responseNet)
          {
            //  and is a response node, so give
            // the model value buffers from the response net to the
            // playback net
            playNet->putModelValueBuffers(modelVal, modelDrv);
          }
          else
          {
            // we need to make the model buffers be the deposit
            // buffers. It is not a response net, but could be a
            // simple input, which will always be the same value as
            // its stimulus.
            UInt32* depositVal;
            UInt32* depositDrv;
            playNet->getDepositBuffers(&depositVal, &depositDrv);
            playNet->putModelValueBuffers(depositVal, depositDrv);
          }
        } // if playNet
      } // if leaf
    } // loop
  }
  
  // Create the replay system directory if it doesn't already exist
  CarbonStatus maybeCreateSystemDir()
  {
    UtString reason;
    OSStatEntry sysDirEntry;
    if (OSStatFileEntry(mReplayInfo.getSystemDir(), &sysDirEntry, &reason) == 0)
    {
      if (! sysDirEntry.isDirectory())
      {
        reason.clear();
        reason << mReplayInfo.getSystemDir() << ": Not a directory.";
        mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(reason.c_str());
        return eCarbon_ERROR;
      }
    }
    else
    {
      // Ok it doesn't exist. Create it
      if (OSMkdir(mReplayInfo.getSystemDir(), 0777, &reason) < 0)
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(reason.c_str());
        return eCarbon_ERROR;
      }
    }
    return eCarbon_OK;
  }
  
  // Write the replay symboltable to disk.
  CarbonStatus writeReplaySymTab()
  {
    {
      UtString replaySymTabName;
      OSConstructFilePath(&replaySymTabName, mDBInstanceDir.c_str(), scSymtabFile);
      
      ZOSTREAMDB(replaySymTab, replaySymTabName.c_str());
      if (! replaySymTab)
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLDBFileOpenFail(replaySymTabName.c_str(), replaySymTab.getError());
        return eCarbon_ERROR;
      }
      
      mReplayTable->writeDB(replaySymTab, NULL, true);
      if (replaySymTab.fail())
        return eCarbon_ERROR;
    }
    return eCarbon_OK;
  }
  
  // read the replay symboltable from disk.
  CarbonStatus readReplaySymTab()
  {

    // First, we have to run through the forcibles, allocate actual
    // shellnets, so we can annotate the symboltable with force
    // subordinate information

    LeafSet inCovered;
    ShellNetWrapper::NetVec forceNets;

    IODBRuntime* db = mHookup->getDB();
    // The counting function also gives us the primitive nets.
    (void) loopCountNumPrimitiveWords(db->loopForced(), false, &inCovered, &forceNets);

    // loop through the forceNets vector, create replay nodes, and set
    // the original net.
    for (ShellNetWrapper::NetVec::iterator p = forceNets.begin(), 
           e = forceNets.end(); p != e; ++p)
    {
      ShellNet* primNet = *p;
      STAliasedLeafNode* designLeaf = primNet->getNameAsLeaf();
      STAliasedLeafNode* replayLeaf = fetchReplayLeaf(designLeaf);
      ReplayData* replayData = castBOM(replayLeaf);
      replayData->putOriginalNet(primNet);
    }
    

    CarbonStatus stat = eCarbon_ERROR;
    {
      UtString replaySymTabName;
      OSConstructFilePath(&replaySymTabName, mDBInstanceDir.c_str(), scSymtabFile);
      
      ZISTREAMDB(replaySymTab, replaySymTabName.c_str());
      if (! replaySymTab)
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLDBFileOpenFail(replaySymTabName.c_str(), replaySymTab.getError());
        return eCarbon_ERROR;
      }

      switch(mReplayTable->readDB(replaySymTab, mHookup->getCarbonModel()->getMsgContext()))
      {
      case STFieldBOM::eReadOK:
        stat = eCarbon_OK;
        break;
      case STFieldBOM::eReadIncompatible:
        mHookup->getCarbonModel()->getMsgContext()->SHLReplayDBReadIncompatible(mDBInstanceDir.c_str());
        break;
      case STFieldBOM::eReadFileError:
        mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(replaySymTab.getErrmsg());
        break;
      case STFieldBOM::eReadCorruptFile:
        mHookup->getCarbonModel()->getMsgContext()->SHLCorruptDBRead(mDBInstanceDir.c_str());
        break;
      }    
    }
    
    // If stat is not ok, the symtab may be corrupt, but there is
    // little we can do. We have to undo playback mode, which
    // happens when we return error. We can't reallocate the
    // symtab because it will delete a net wrapper that the
    // testbench may already have.
    return stat;
  }
  
  /*
    Run through the replay symtab and change any allocated nets to
    be whatever the mode dictates. If we were in normal mode and we
    are going to record, for example, the sub nets of the wrapper nets
    need to be changed to record nets.
  */
  void reInstrumentAllocatedNets()
  {
    for (STSymbolTable::NodeLoop q = mReplayTable->getNodeLoop();
         ! q.atEnd(); ++q)
    {
      STSymbolTableNode* node = *q;
      STAliasedLeafNode* leaf = node->castLeaf();
      if (leaf)
      {
        ReplayData* replayData = castBOM(leaf);
        ShellNetWrapper* wrap = replayData->getNetWrapper();
        if (wrap)
        {
          ShellNetWrapper::NetVec netVec;
          wrap->gatherWrappedNets(&netVec);
          for (ShellNetWrapper::NetVec::iterator r = netVec.begin(), 
                 re = netVec.end(); r != re; ++r)
          {
            ShellNet* subNet = *r;
            instrumentNet(subNet);
          }
        } // if wrap
      } // if leaf
    } // for
  } // reInstrumentAllocatedNets
  
  void reAllocReplaySymTab()
  {
    delete mReplayTable;
    
    IODBRuntime* iodb = mHookup->getDB();
    mReplayTable = new STSymbolTable(this, iodb->getAtomicCache());
    mReplayTable->setHdlHier(iodb->getHdlHier());
  }
  
  /* 
     This runs through the entire design symboltable, looking for any
     api-accessible memories. Any api-accessible memories are
     considered stimuli, even if they are not marked depositable. If
     any of these memories are marked observable they are considered
     response and are added to the internal mem write list, the values
     of which get updated when we update the response.

     We may decide to only record depositable memories, but we may
     miss a divergence in such a case, ie., we may not know that
     non-depositable memory has been deposited to during playback. So,
     until we understand this better, all api-accessible memories are
     considered stimuli. 
  */
  void computeMemoriesFromDesign(IODBRuntime* db)
  {
    // Loop through the design, find the api-accessible memories and
    // create wrappers for them.
    LeafVector vec;
    sFindDesignMemories(db, &vec);
    for (UInt32 i = 0; i < vec.size(); ++i) {
      STAliasedLeafNode *leafStorage = vec[i];
      // Loop through the aliases and see if any alias has been
      // marked depositable or observable. Run through the
      // entire alias ring unless both isDepositable and
      // isObservable are true.
      bool isDepositable = false;
      bool isObservable = false;
      for (STAliasedLeafNode::AliasLoop aliases(leafStorage);
           ! aliases.atEnd() && !(isDepositable && isObservable);
           ++aliases)
      {
        STAliasedLeafNode* aliasLeaf = *aliases;
        isDepositable |= db->isDepositable(aliasLeaf);
        isObservable |= db->isObservable(aliasLeaf);
      }

      if (isDepositable || isObservable)
      {
        // we have an api-accessible memory
        // Reference ONLY the storage node, so we can alias the
        // hashtable the shell net will hold
        const IODBIntrinsic* intrinsic = db->getLeafIntrinsic(leafStorage);
        // At this point we should only be dealing with nets
        // that have intrinsics.
        ST_ASSERT(intrinsic, leafStorage);
        ShellNet* primMem = mHookup->getCarbonNet(leafStorage);
        ST_ASSERT(primMem, leafStorage);
        
        ShellNetRecordMem* recordMem = NULL;
        
        STAliasedLeafNode* replayLeaf = fetchReplayLeaf(leafStorage);
        ReplayData* data = castBOM(replayLeaf);
        if (data->getRecordNet() == NULL)
        {
          recordMem = mRecorder->addExternalRecordMem(primMem);
          data->putRecordNet(recordMem, true);
        } // if
      
        // observables are a subset of depositables (for now)
        if (isObservable)
        {
          CarbonModelMemory* modelMem = primMem->castModelMemory();
          ST_ASSERT(modelMem, primMem->getName());
          ShellMemoryCBManager* cbManager = modelMem->getShellMemoryCBManager();
          // All observable mems must have managers. If this is
          // an old Carbon Model without memory callbacks, we should have
          // caught that during the create call.
          ST_ASSERT(cbManager, primMem->getName());
          
          // Currently, observable mems are a subset of
          // api-accessible deposit mems, so we better have a
          // recordMem already
          ST_ASSERT(recordMem, primMem->getName());
          mRecorder->addInternalRecordMem(recordMem, cbManager);
        } // if observable
      } // if depositable or observable
    } // for
  }
  
  // Compute the response buffer size and net allocation.
  void computeResponseBufferFromDesign(IODBRuntime* db)
  {
    // Allocate a buffer for the outputs and observables
    
    // Total number of words needed to represent all the outs/observes
    UInt32 totNumObserveWords = 0;
    // List of all the observable nets
    ShellNetWrapper::NetVec examineNets;
    {
      // Set of covered alias rings
      LeafSet outCovered;
      // Response buffer doesn't need masks, so don't count them
      totNumObserveWords += loopCountNumPrimitiveWords(db->loopOutputs(), false, &outCovered, &examineNets);
      totNumObserveWords += loopCountNumPrimitiveWords(db->loopBidis(), false, &outCovered, &examineNets);
      totNumObserveWords += loopCountNumPrimitiveWords(db->loopObserved(), false, &outCovered, &examineNets);
      totNumObserveWords += loopCountNumPrimitiveWords(db->loopForced(), false, &outCovered, &examineNets);

      // Create the proper sized response buffer
      mRecorder->allocateResponseBuffer(totNumObserveWords, examineNets.size());
    }
    
    {
      ReplayRecordBuffer* responseBuffer = mRecorder->getResponseBuffer();
      for (ShellNetWrapper::NetVec::iterator p =  examineNets.begin(),
             e = examineNets.end(); p != e; ++p)
      {
        ShellNet* primNet = *p;
        
        // create a leaf in the symtab for the response net. If this
        // net is also stimulus, we need to tell the response net
        // about it so that it can computed changed information
        // correctly.
        STAliasedLeafNode* primLeaf = primNet->getNameAsLeaf();
        STAliasedLeafNode* replayLeaf = fetchReplayLeaf(primLeaf);
        ReplayData* replayData = castBOM(replayLeaf);
        ShellNetRecord* stimNet = replayData->getRecordNet();
        
        ShellNetRecordResponse* responseNet;
        responseNet = allocateRecordResponseNet(primNet, stimNet, responseBuffer);
        replayData->maybeSetOriginalNet(responseNet);
        mRecorder->addResponseNet(responseNet);
      }

      // Update the shadow based on the response net
      responseBuffer->updateShadow();
    }
  } // void computeResponseBufferFromDesign

  // This sorts the input nets so that clocks are first in the list,
  // followed by state outputs.  It also counts the number of clocks
  // and state output nets.
  void processClocksAndStateOutputs(ShellNetWrapper::NetVec* sortedInputNets,
                                    ShellNetWrapper::NetVec* inputNets,
                                    LeafSet* clkNodes,
                                    LeafSet* stateOutputNodes,
                                    const IODBRuntime* db,
                                    UInt32* numNonStateOutputClocks,
                                    UInt32* numStateOutputs)
  {
    ShellNetWrapper::NetVec stateOutputClocks, stateOutputNonClocks;
    UInt32 numInputs = inputNets->size();
    for (UInt32 i = 0; i < numInputs; ++i)
    {
      ShellNet* primNet = (*inputNets)[i];
      STAliasedLeafNode* primLeaf = primNet->getNameAsLeaf();
      const ShellDataBOM *bomdata = ShellSymTabBOM::getLeafBOM(primLeaf);
      bool isClock = db->isMarkedClock(primLeaf) && (primNet->getChangeArrayRef() != NULL) && (primNet->getBitWidth() == 1);
      bool isStateOutput = bomdata->isStateOutput();
      if (isClock)
      {
        if (!isStateOutput) {
          // Add all non-stateoutput clocks to the front of the list
          sortedInputNets->push_back(primNet);
          (*inputNets)[i] = NULL;
        }
        clkNodes->insert(primLeaf);
      }
      if (isStateOutput) {
        stateOutputNodes->insert(primLeaf);
        if (isClock) {
          stateOutputClocks.push_back(primNet);
        } else {
          stateOutputNonClocks.push_back(primNet);
        }
        (*inputNets)[i] = NULL;
      }
    }
    
    *numNonStateOutputClocks = sortedInputNets->size();
    *numStateOutputs = stateOutputNodes->size();

    // Add all state outputs next, so that everything with a touched
    // buffer that's checked for divergence is at the front.  This
    // makes reporting divergent nets easier because the value buffer
    // and touched buffer are at the same offset.  Within this group,
    // state output clocks come first.
    sortedInputNets->insert(sortedInputNets->end(), stateOutputClocks.begin(), stateOutputClocks.end());
    sortedInputNets->insert(sortedInputNets->end(), stateOutputNonClocks.begin(), stateOutputNonClocks.end());
    
    // now for the rest of them
    for (UInt32 i = 0; i < numInputs; ++i)
    {
      ShellNet* primNet = (*inputNets)[i];
      if (primNet)
        sortedInputNets->push_back(primNet);
    }
    
  }

  // Compute the stimuli buffer size and net allocation.
  void computeStimuliBufferFromDesign(IODBRuntime* db)
  {
    // Allocate a buffer for the inputs and depositables
    UInt32 totNumInputWords = 0;
    // The input primitive nets
    ShellNetWrapper::NetVec inputNets;
    {
      // Set of covered alias rings
      LeafSet inCovered;
      
      // The total number of words needed to represent all the
      // input/deposits/forcibles.  We need to include space for the
      // mask in the case of state outputs.
      totNumInputWords += loopCountNumPrimitiveWords(db->loopInputs(), true, &inCovered, &inputNets);
      totNumInputWords += loopCountNumPrimitiveWords(db->loopBidis(), true, &inCovered, &inputNets);
      totNumInputWords += loopCountNumPrimitiveWords(db->loopDeposit(), true, &inCovered, &inputNets);
      totNumInputWords += loopCountNumPrimitiveWords(db->loopForced(), true, &inCovered, &inputNets);
      
      // Allocate the buffers
      mRecorder->allocateInputBuffer(totNumInputWords);
    }

    // Move the clocks to the front of the vector. I need this to loop
    // through the clocks after I've allocated the record nets. It
    // could potentially have a positive performance
    // effect since clocks tend to change more often than non-clocks.
    // Also count the number of non-stateoutput clocks, and all state output
    // nets (clocks included).  Both groups need special treatment.
    ShellNetWrapper::NetVec sortedInputNets;
    UInt32 numNonStateOutputClocks, numStateOutputs;
    LeafSet clkNodes, stateOutputNodes;
    processClocksAndStateOutputs(&sortedInputNets, &inputNets, &clkNodes, &stateOutputNodes, db, &numNonStateOutputClocks, &numStateOutputs);
    
    // clocks and state outputs need to have their touched buffers recorded.
    UInt32 touchedBufRequired = numNonStateOutputClocks + numStateOutputs;
    mRecorder->reserveTouched(touchedBufRequired, sortedInputNets.size());

    // now run through the inputs again, allocating buffers to the
    // replayData for each node
    {
      ReplayRecordBuffer* recordBuffer = mRecorder->getInputBuffer();
      for (ShellNetWrapper::NetVec::iterator p =  sortedInputNets.begin(),
             e = sortedInputNets.end(); p != e; ++p)
      {
        ShellNet* primNet = *p;
        STAliasedLeafNode* primLeaf = primNet->getNameAsLeaf();
        
        // this node is in the replay table, not the iodb design table.
        STAliasedLeafNode* replayLeaf = fetchReplayLeaf(primLeaf->getStorage());
        replayLeaf->setThisStorage();
        
        ReplayData* replayData = castBOM(replayLeaf);
        // Make sure we don't have a record net already allocated.
        ST_ASSERT(replayData->getRecordNet() == NULL, primLeaf);
        bool isClock = clkNodes.count(primLeaf) > 0;
        bool isStateOutput = stateOutputNodes.count(primLeaf) > 0;
        allocateRecordStorage(primNet, replayData, recordBuffer, isClock, isStateOutput);
      }
    }
    
    for (UInt32 i = 0; i < numNonStateOutputClocks; ++i)
    {
      // Monitor this net as a clock.
      ShellNet* primVHMClk = sortedInputNets[i];
      STAliasedLeafNode* clkLeaf = primVHMClk->getNameAsLeaf();
      STAliasedLeafNode* replayClk = findReplayLeaf(clkLeaf->getStorage());
      ST_ASSERT(replayClk, clkLeaf);
      ReplayData* replayData = castBOM(replayClk);
      ShellNetRecord* recordNet = replayData->getRecordNet();
      ST_ASSERT(recordNet, replayClk);
      ST_ASSERT(replayData->getInputBufferIndex() != -1, replayClk);
      ST_ASSERT(primVHMClk->getBitWidth() == 1, clkLeaf);
      mRecorder->addRecordClock(recordNet);
    }

    // Some clocks are also state outputs
    UInt32 numStateOutputClocks = clkNodes.size() - numNonStateOutputClocks;

    for (UInt32 i = 0; i < numStateOutputs; ++i)
    {
      // Monitor this net as a state output.
      // State outputs come immediately after clocks in the array.
      ShellNet* primVHMNet = sortedInputNets[i + numNonStateOutputClocks];
      STAliasedLeafNode* netLeaf = primVHMNet->getNameAsLeaf();
      STAliasedLeafNode* replayNet = findReplayLeaf(netLeaf->getStorage());
      ST_ASSERT(replayNet, netLeaf);
      ReplayData* replayData = castBOM(replayNet);
      ShellNetRecord* recordNet = replayData->getRecordNet();
      ST_ASSERT(recordNet, replayNet);
      ST_ASSERT(replayData->getInputBufferIndex() != -1, replayNet);
      mRecorder->addRecordStateOutput(recordNet);
      // State output clocks, if they exist are at the beginning of
      // the array.  Add them as clocks, too.
      if (i < numStateOutputClocks) {
        mRecorder->addRecordClock(recordNet);
      }
    }
  }
  
  /*
    This creates a version file for the database, and opens the data
    file (event file).
    The system directory has to be already created. The instance
    directory is removed and re-created if it exists.
  */
  CarbonStatus openDBForRecord()
  {
    UtString reason;
    switch(mReplayInfo.getDirAction())
    {
    case eCarbonDirOverwrite:
      {
        OSDeleteRecursive(mDBInstanceDir.c_str(), &reason);
        reason.clear();
      }
      break;
    case eCarbonDirNoOverwrite:
      {
        OSStatEntry dirEntry;
        if (OSStatFileEntry(mDBInstanceDir.c_str(), &dirEntry, &reason) != 0)
        {
          mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(reason.c_str());
          return eCarbon_ERROR;
        }
      }
      break;
    }
    
    if (OSMkdir(mDBInstanceDir.c_str(), 0777, &reason) < 0)
    {
      mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(reason.c_str());
      return eCarbon_ERROR;
    }
    
    // create a version file
    {
      UtString versionName;
      OSConstructFilePath(&versionName, mDBInstanceDir.c_str(), scVersionFile);
      UtOBStream versionFile(versionName.c_str());
      versionFile << scVersionReplayDBSignature << UtIO::endl;
      versionFile << scVersionToken << " " << scManagerVersion << UtIO::endl;
      if (versionFile.bad())
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(versionFile.getErrmsg());
        return eCarbon_ERROR;
      }
    }

    // create a Carbon Model signature file
    {
      UtString vhmSigName;
      OSConstructFilePath(&vhmSigName, mDBInstanceDir.c_str(), scVhmSignatureFile);
      UtOBStream vhmSigFile(vhmSigName.c_str());
      vhmSigFile << scVhmSigFileSignature << UtIO::endl;
      vhmSigFile << scIdToken << " ";
      vhmSigFile << mHookup->getUID() << UtIO::endl;
      if (vhmSigFile.bad())
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(vhmSigFile.getErrmsg());
        return eCarbon_ERROR;
      }
    }
    
    // Open up the data file for write
    UtString recStreamName;
    OSConstructFilePath(&recStreamName, mDBInstanceDir.c_str(), scIndexFile);
    mRecorder = new ReplayRecorder(mDBInstanceDir, recStreamName.c_str(), mHookup->getCarbonModel(), getReplayInfo());
    CarbonStatus curStat = mRecorder->getStatus();
    if (curStat != eCarbon_OK)
    {
      delete mRecorder;
      mRecorder = NULL;
      return curStat;
    }
    return eCarbon_OK;
  }

  /*
    This checks the version of the database, and opens the data
    file (event file).
  */
  CarbonStatus openDBForPlayback()
  {
    UtString reason;
    // open the version file
    {
      UtString versionName;
      OSConstructFilePath(&versionName, mDBInstanceDir.c_str(), scVersionFile);
      UtIBStream versionFile(versionName.c_str());
      if (versionFile.bad())
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(versionFile.getErrmsg());
        return eCarbon_ERROR;
      }
      
      UtString sig;
      UtString versionTok;
      versionFile >> sig;
      versionFile >> versionTok; 
      if (sig.compare(scVersionReplayDBSignature) != 0)
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLDBInvalidSignature(sig.c_str());
        return eCarbon_ERROR;
      }

      if (versionTok.compare(scVersionToken) != 0)
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLDBUnrecognizedToken(versionFile.getFilename(), versionTok.c_str());
        return eCarbon_ERROR;
      }

      // check the version number for compatibility
      SInt32 versionNum;
      versionFile >> versionNum;
      if (versionFile.bad())
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(versionFile.getErrmsg());
        return eCarbon_ERROR;
      }

      // We do not support back-compatibility with replay. The speed
      // of the database is too crucial
      if (versionNum != scManagerVersion)
      {
        // unsupported version
        mHookup->getCarbonModel()->getMsgContext()->SHLReplayDBUnsupportedVersion(mDBInstanceDir.c_str(), versionNum);
        return eCarbon_ERROR;
      }
    }
    
    // open and check the Carbon Model signature file
    {
      UtString vhmSigName;
      OSConstructFilePath(&vhmSigName, mDBInstanceDir.c_str(), scVhmSignatureFile);
      UtIBStream vhmSigFile(vhmSigName.c_str());
      if (vhmSigFile.bad())
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLFileProblem(vhmSigFile.getErrmsg());
        return eCarbon_ERROR;
      }

      UtString sig;
      UtString idTok;
      vhmSigFile >> sig;

      // Carbon Model signature file signature
      if (sig.compare(scVhmSigFileSignature) != 0)
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLDBInvalidSignature(sig.c_str());
        return eCarbon_ERROR;
      }

      // Note that versioning is handled solely through the version
      // file. Any changes to the database have to be represented in
      // the version. So, no version check on this file.
      
      // Id:
      vhmSigFile >> idTok; 
      if (idTok.compare(scIdToken) != 0)
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLDBUnrecognizedToken(vhmSigFile.getFilename(), idTok.c_str());
        return eCarbon_ERROR;
      }

      // Carbon Model Signature
      // grab the Carbon Model signature and compare with the carbon_id that we
      // have loaded now
      sig.clear();
      vhmSigFile.getline(&sig);
      // strip off the delimiter
      StringUtil::strip(&sig);
      
      const char* vhmID = mHookup->getUID();
      if (sig.compare(vhmID) != 0)
      {
        mHookup->getCarbonModel()->getMsgContext()->SHLReplayDBNotMatchDesign(mDBInstanceDir.c_str(), sig.c_str(), vhmID);
        return eCarbon_ERROR;
      }
    }
    
    // Open up the data file for read
    UtString playStreamName;
    OSConstructFilePath(&playStreamName, mDBInstanceDir.c_str(), scIndexFile);
    mPlayer = new ReplayPlayer(mDBInstanceDir, playStreamName.c_str(), mHookup->getCarbonModel(), &mNetValueChangeCBs, getReplayInfo());
    CarbonStatus curStat = mPlayer->getStatus();
    if (curStat != eCarbon_OK)
      return curStat;
    return eCarbon_OK;
  }
  
  // Find or create a leaf in the replay symbol table
  STAliasedLeafNode* fetchReplayLeaf(STAliasedLeafNode* srcLeaf)
  {
    // This used to simply call translateLeaf(), but that's not
    // thread-safe.  translateLeaf() is a general function that allows
    // the source and destination symbol tables to use different
    // atomic caches, which causes an addition/lookup to the cache.
    //
    // The atomic cache in this case is shared across all model
    // instances, so we'd need a mutex.  We can't just lock the same
    // ShellGlobal mutex that's used for other atomic cache accesses,
    // because it also needs to install a custom memory pool.  By
    // locking the mutex before calling translateLeaf(), all the
    // memory allocation for translating the leaf (not just the cache
    // lookup) would use that pool.  That would mean that any future
    // access to the symbol table that could allocate/free memory
    // would also need to lock the mutex.  That's a big can of worms.
    //
    // Instead, take advantage of the fact that the source and
    // destination symbol tables share the same atomic cache.  Walk
    // the hierarchy of the design node and manually add the necessary
    // nodes to the replay symbol table.

    UtStackPOD<STSymbolTableNode*> nodeStack;
    for (STSymbolTableNode* node = srcLeaf; node != NULL; node = node->getParent()) {
      nodeStack.push(node);
    }

    STBranchNode* replayParent = NULL;
    STAliasedLeafNode* replayLeaf = NULL;
    while (!nodeStack.empty()) {
      STSymbolTableNode* designNode = nodeStack.top();
      // See if we already have it
      STSymbolTableNode* replayNode = mReplayTable->lookup(designNode);
      if (replayNode != NULL) {
        // Save the node pointers and continue
        replayParent = replayNode->castBranch();
        replayLeaf = replayNode->castLeaf();
      } else {
        // Add the node as a child of the last node
        StringAtom* name = designNode->strObject();
        STBranchNode* designBranch = designNode->castBranch();
        if (designBranch != NULL) {
          replayParent = mReplayTable->createBranch(name, replayParent);
        } else {
          replayLeaf = mReplayTable->createLeaf(name, replayParent);
        }
      }
      nodeStack.pop();
    }

    ST_ASSERT(replayLeaf != NULL, srcLeaf);
    return replayLeaf;
  }

  // Find a leaf in the replay table. Return NULL if not found.
  STAliasedLeafNode* findReplayLeaf(STAliasedLeafNode* srcLeaf)
  {
    STSymbolTableNode* replayNode = mReplayTable->lookup(srcLeaf);
    STAliasedLeafNode* replayLeaf = NULL;
    if (replayNode)
      replayLeaf = replayNode->castLeaf();
    return replayLeaf;
  }

  // Create a one to one wrapper
  ShellNetWrapper1To1* allocateOneToOneWrap(ShellNet* netToWrap,
                                            ReplayData* replayData)
  {
    ShellNetWrapper1To1* netWrap = new ShellNetWrapper1To1(netToWrap);
    replayData->putNetWrapper(netWrap);
    return netWrap;
  }
  
  // Create a record net assigning a chunk of the replay buffer
  void allocateRecordStorage(ShellNet* primNet,
                             ReplayData* replayData,
                             ReplayRecordBuffer* recordBuffer,
                             bool isClock, 
                             bool isStateOutput)
  { 
    // A memory should never come through here
    ST_ASSERT(primNet->castMemory() == NULL, primNet->getName());

    // Allocate the space for this record net
    UInt32 numNetWords = primNet->getNumUInt32s();
    bool isTristate = primNet->isTristate();
    UInt32* valueBuf;
    UInt32* maskBuf = NULL;
    UInt32* driveBuf = NULL;
    UInt32* valueShadow;
    UInt32* maskShadow = NULL;
    UInt32* driveShadow = NULL;

    UInt32 touchedIndex = 0;
    ShellNetReplay::Touched* touchedBuffer = mRecorder->allocStimuliTouched(&touchedIndex, isClock || isStateOutput);
    
    UInt32 valueOffset;
    if (isTristate) {
      // Tristates have drive, but no mask
      recordBuffer->allocateValues(numNetWords, &valueOffset, &valueBuf, &driveBuf,
                                   &valueShadow, &driveShadow, touchedBuffer, NULL);
    } else if (isStateOutput) {
      // Non-tristate state outputs have mask, but no drive.  The mask
      // shadow buffer entry is not actually used, but needs be
      // allocated so the value and shadow buffers are the same size.
      recordBuffer->allocateValues(numNetWords, &valueOffset, &valueBuf, &maskBuf,
                                   &valueShadow, &maskShadow, touchedBuffer, NULL);
    } else {
      // Non-tristate non-state outputs have neither mask nor drive,
      // so pass NULL to prevent allocation of buffer space
      recordBuffer->allocateValues(numNetWords, &valueOffset, &valueBuf, NULL,
                                   &valueShadow, NULL, touchedBuffer, NULL);
    }
    
    // The current input buffer index will be assigned to this net
    replayData->putInputBufferIndex(valueOffset);
    
    // Create the Record wrapper for the net
    allocateBufferedRecordNet(primNet, replayData, valueBuf, maskBuf, driveBuf, numNetWords,
                              isTristate, touchedBuffer, touchedIndex, valueShadow, driveShadow, 
                              isClock, isStateOutput);

    // State outputs will register their record net as a stimulus net
    // with the record buffer.  This functionality was created for the
    // response buffer, so that response nets that are also stimulus
    // are always seen as changed when they're touched, since a
    // comparison to the previously-seen value doesn't work.  We have
    // essentially the same problem here.
    if (isStateOutput) {
      ST_ASSERT(!isTristate, primNet->getName());
      ShellNetRecord *recNet = replayData->getRecordNet();
      // State outputs have entries in the buffer for both value and mask
      recordBuffer->mapOffsetsToStimNet(valueOffset, numNetWords * 2, recNet);
    }

  }
  
  ShellNetRecord* allocateBufferedRecordNet(ShellNet* primNet, 
                                            ReplayData* replayData,
                                            UInt32* valueBuf,
                                            UInt32* maskBuf,
                                            UInt32* driveBuf,
                                            UInt32 numNetWords,
                                            bool isTristate,
                                            ShellNetReplay::Touched* netTouchBuffer,
                                            UInt32 touchedIndex,
                                            UInt32* valueShadow,
                                            UInt32* driveShadow,
                                            bool isClock,
                                            bool isStateOutput)
  {
    ST_ASSERT(primNet->castMemory() == NULL, primNet->getName());
    
    // Initialize the value and drive buffers.
    primNet->examine(valueBuf, driveBuf, ShellNet::eXDrive, mHookup->getCarbonModel());
    
    // Create the Record wrapper for the net
    ShellNetRecord* recNet = NULL;
    if (isTristate)
    {
      ST_ASSERT(!isStateOutput, primNet->getName());
      if (isClock)
        // a tristate that is a clock is a bidirect clk
        recNet = new ShellNetRecordBidirectClk(primNet, valueBuf, driveBuf, valueShadow, driveShadow, netTouchBuffer);
      else
        recNet = new ShellNetRecordTristate(primNet, valueBuf, driveBuf, netTouchBuffer);
    }
    else
    {
      if (numNetWords == 1)
      {
        if (isClock) {
          if (isStateOutput) {
            recNet = new ShellNetRecordTwoStateClkStateOutput(primNet, valueBuf, maskBuf, netTouchBuffer);
          } else {
            recNet = new ShellNetRecordTwoStateClk(primNet, valueBuf, maskBuf, valueShadow, netTouchBuffer);
          }
        } else
          recNet = new ShellNetRecordTwoStateWord(primNet, valueBuf, maskBuf, netTouchBuffer);
      }
      else
        recNet = new ShellNetRecordTwoStateA(primNet, valueBuf, maskBuf, netTouchBuffer);
    }
    
    ST_ASSERT(recNet, primNet->getName());

    // Set the external index of the net to its touched buffer index.
    // Note: these are not unique since there are multiple buffers.
    recNet->putExternalIndex(touchedIndex);
    
    // Update the replay table with the record net.
    replayData->putRecordNet(recNet, true);
    return recNet;
  }

  // Bind a non-storage net to a storage (primitive) replay net
  ShellNetRecord* bindRecordNet(ShellNet* primNet)
  {
    STAliasedLeafNode* primLeaf = primNet->getNameAsLeaf();

    STAliasedLeafNode* replayLeaf =  fetchReplayLeaf(primLeaf);
    ReplayData* replayData = castBOM(replayLeaf);
    ShellNetRecord* recordNet = replayData->getRecordNet();
    if (recordNet == NULL)
    {
      // We should only be calling this if the primNet is not the
      // storage net that has already been assigned a buffer.
      ST_ASSERT(primLeaf->getStorage() != primLeaf, primLeaf);
      
      // Get the storage net
      STAliasedLeafNode* storageLeaf = primLeaf->getStorage();
      STAliasedLeafNode* replayStoreLeaf =  fetchReplayLeaf(storageLeaf);
      // link the storage net with the requested net
      replayStoreLeaf->linkAlias(replayLeaf);

      ReplayData* replayStoreData = castBOM(replayStoreLeaf);
      
      ShellNetRecord* storeRecordNet = replayStoreData->getRecordNet();
      ST_ASSERT(storeRecordNet, storageLeaf);

      // Create the record net for this alias via the storage net.
      recordNet = storeRecordNet->cloneStorage(primNet);
      replayData->putRecordNet(recordNet, true);
    }
    else
      recordNet->incrCount();
    
    return recordNet;
  }

  // Possibly bind a non-depositable record net to a primitive
  ShellNet* bindRecordNetNotDepositable(ShellNet* primNet)
  {
    // Assume we won't actually do anything to the primitive
    ShellNet *returnNet = primNet;
    // Find the leaf in the replay symtab and get its replay data
    STAliasedLeafNode* primLeaf = primNet->getNameAsLeaf();
    STAliasedLeafNode* replayLeaf =  fetchReplayLeaf(primLeaf);
    ReplayData* replayData = castBOM(replayLeaf);
    ShellNetRecord* recordNet = replayData->getRecordNet();
    if (recordNet == NULL) {
      // We haven't created the ShellNetRecordNotDepositable for this
      // net yet.  Recordable deposits have their ShellNetRecords
      // created when record mode starts, but that's because we know
      // about all of them.  We can't do that for nets that aren't
      // explicitly depositable (i.e. recordable) without creating a
      // ShellNetRecordNotDepositable for every node in the design,
      // which would be very inefficient.
      if (mFoundNets.count(primNet) > 0) {
        // Only create the record net if the user has found this net.
        recordNet = new ShellNetRecordNotDepositable(primNet);
        replayData->putRecordNet(recordNet, false);
        returnNet = recordNet;
      }
    } else {
      // Just increment the underlying net's reference count.
      recordNet->incrCount();
      returnNet = recordNet;
    }
    return returnNet;
  }

  ShellNetPlayback* allocateBufferedPlaybackNet(ShellNet* primNet, 
                                                ReplayData* replayData,
                                                UInt32* valueBuf,
                                                UInt32* maskBuf,
                                                UInt32* driveBuf,
                                                UInt32 numNetWords,
                                                bool isTristate,
                                                bool doBufInit,
                                                const ShellNetReplay::Touched* touchBuffer,
                                                bool isClock,
                                                bool isStateOutput)
  {
    // initialize the deposit buffers
    /*
      Note the record side did exactly the same thing. So even if
      this is a bidi, where the value buf may have actually changed
      from the original stimulus to be a resultant value, it should
      match the initial value in the event file.

      We shouldn't update the buffers if this is a read-only
      net. doBufInit will be false in such a case.
    */
    if (doBufInit)
      primNet->examine(valueBuf, driveBuf, ShellNet::eXDrive, mHookup->getCarbonModel());

    // Need the bom from the Carbon Model to decide which playback net to
    // create. For a read-only net or a write-only net a special
    // wrapper has to be used so that the restricted functionality
    // causes an error message.
    const ShellDataBOM* vhmNodeData = ShellSymTabBOM::getStorageDataBOM(primNet->getNameAsLeaf());
    bool depositable = vhmNodeData->isDepositable();
    bool observable = vhmNodeData->isObservable();
    ST_ASSERT(depositable || observable, primNet->getName());
    
    ShellNetPlayback* playNet = NULL;
    if (isTristate)
    {
      if (depositable && observable)
      {
        if (isClock)
        {
          ST_ASSERT(primNet->getBitWidth() == 1, primNet->getName());
          // We must also allocate a shadow for the clock so we can
          // detect fake edges and set the touched buffer
          // appropriately.
          UInt32* shadow = mPlayer->allocClkShadow(2);
          playNet = new ShellNetPlaybackBidirectClk(primNet, valueBuf, driveBuf, &shadow[0], &shadow[1], *touchBuffer);
        }
        else
          playNet = new ShellNetPlaybackBidirect(primNet, valueBuf, driveBuf, *touchBuffer);
      }
      else
      {
        // Tristates are NOT implicitly depositable and observable
        ST_ASSERT(!isClock, primNet->getName());
        if (depositable) {
          // I don't think this is possible, but could be if I didn't
          // make forcibles both depositable and observable
          playNet = new ShellNetPlaybackTristateWriteOnly(primNet, valueBuf, driveBuf, *touchBuffer);
        }
        else if (observable)
          playNet = new ShellNetPlaybackTristateReadOnly(primNet, valueBuf, driveBuf, *touchBuffer);
      }
    }
    else
    {
      ST_ASSERT(driveBuf == NULL, primNet->getName());

      // create the drive values for the different examine types for
      // a pod. idrive is always 0 unless the primitive net is an
      // input, then it is all ones. xdrive is always all ones
      // unless it is an input, then it is all zeros.
      
      DynBitVector tmpZero(numNetWords * 32);
      const DynBitVector* allzeros = mDynFactory.alloc(tmpZero);
      
      UInt32 bitWidth = primNet->getBitWidth();
      DynBitVector tmpOnes(bitWidth);
      tmpOnes.set();
      const DynBitVector* allones = mDynFactory.alloc(tmpOnes);
      
      const DynBitVector* xdriveBV = allones;
      const DynBitVector* idriveBV = allzeros;
      
      if (primNet->isInput())
      {
        idriveBV = allones;
        xdriveBV = allzeros;
      }
      
      if (numNetWords == 1)
      {
        if (depositable && observable)
        {
          if (isClock)
          {
            ST_ASSERT(bitWidth == 1, primNet->getName());
            if (isStateOutput) {
              playNet = new ShellNetPlaybackTwoStateClkStateOutput(primNet, valueBuf, maskBuf, idriveBV->value(), xdriveBV->value(), *touchBuffer);
            } else {
              // We must also allocate a shadow for the clock so we can
              // detect fake edges and set the touched buffer
              // appropriately.
              UInt32* shadow = mPlayer->allocClkShadow(1);
              playNet = new ShellNetPlaybackTwoStateClk(primNet, valueBuf, maskBuf, idriveBV->value(), xdriveBV->value(), shadow, *touchBuffer);
            }
          }
          else
            playNet = new ShellNetPlaybackTwoStateWord(primNet, valueBuf, maskBuf, idriveBV->value(), xdriveBV->value(), *touchBuffer);
        }
        else if (depositable)
        {
          if (isClock)
          {
            // I don't think this is currently possible. See
            // test/replay/depob, part 2. The second part of that test
            // recompiles that design with top.ren2 as a infrequent
            // depositable. As a result, it gets removed from the
            // clock tree. When it is a frequent depositable, it
            // becomes an input, which is observable.
            ST_ASSERT(bitWidth == 1, primNet->getName());
            // Not implemented for state output clocks
            ST_ASSERT(!isStateOutput, primNet->getName());
            UInt32* shadow = mPlayer->allocClkShadow(1);
            playNet = new ShellNetPlaybackTwoStateClkWriteOnly(primNet, valueBuf, maskBuf, idriveBV->value(), xdriveBV->value(), shadow, *touchBuffer);
          }
          else
            playNet = new ShellNetPlaybackTwoStateWordWriteOnly(primNet, valueBuf, maskBuf, idriveBV->value(), xdriveBV->value(), *touchBuffer);
        }
        else if (observable)
          playNet = new ShellNetPlaybackTwoStateWordReadOnly(primNet, valueBuf, maskBuf, idriveBV->value(), xdriveBV->value(), *touchBuffer);
      }
      else
      {
        ST_ASSERT(! isClock, primNet->getName());
        if (depositable && observable)
          playNet = new ShellNetPlaybackTwoStateA(primNet, valueBuf, maskBuf, idriveBV->getUIntArray(), xdriveBV->getUIntArray(), *touchBuffer);
        else if (depositable)
          playNet = new ShellNetPlaybackTwoStateAWriteOnly(primNet, valueBuf, maskBuf, idriveBV->getUIntArray(), xdriveBV->getUIntArray(), *touchBuffer);
        else if (observable)
          playNet = new ShellNetPlaybackTwoStateAReadOnly(primNet, valueBuf, maskBuf, idriveBV->getUIntArray(), xdriveBV->getUIntArray(), *touchBuffer);
      }
    }
    
    ST_ASSERT(playNet, primNet->getName());
    
    // Update the replay table with the playback net.
    replayData->putPlaybackNet(playNet);
    return playNet;
  }

  // Binds a primitive net to its playback net.
  ShellNetPlayback* bindPlaybackNet(ShellNet* primNet)
  {
    STAliasedLeafNode* primLeaf = primNet->getNameAsLeaf();
    
    STAliasedLeafNode* replayLeaf =  fetchReplayLeaf(primLeaf);
    ReplayData* replayData = castBOM(replayLeaf);
    ShellNetPlayback* playbackNet = replayData->getPlaybackNet();
    if (playbackNet == NULL)
    {
      // We should only be calling this if the primNet is not the
      // storage net that has already been assigned a buffer.
      if (primLeaf->getStorage() == primLeaf)
        // This is not a replayable net
        return NULL;

      STAliasedLeafNode* storageLeaf = primLeaf->getStorage();
      STAliasedLeafNode* replayStoreLeaf =  fetchReplayLeaf(storageLeaf);
      ReplayData* replayStoreData = castBOM(replayStoreLeaf);
      
      ShellNetPlayback* storePlaybackNet = replayStoreData->getPlaybackNet();
      if (! storePlaybackNet)
        return NULL;
      
      playbackNet = storePlaybackNet->cloneStorage(primNet);
      replayData->putPlaybackNet(playbackNet);
    }
    else
      playbackNet->incrCount();
    
    return playbackNet;
  }

  // Assigns buffer space for a net, returns updated inputBufferIndex
  UInt32 assignReplayBufferChunk(UInt32** valueBuf, UInt32** maskBuf, 
                                 UInt32** driveBuf,
                                 UInt32 numNetWords,
                                 bool doMask,
                                 bool isTristate, 
                                 UInt32 inputBufferIndex, 
                                 UInt32* inputWords)
  {
    *valueBuf = &inputWords[inputBufferIndex];
    inputBufferIndex += numNetWords;

    *maskBuf = NULL;
    if (doMask) {
      *maskBuf = &inputWords[inputBufferIndex];
      inputBufferIndex += numNetWords;
    }
    
    *driveBuf = NULL;
    if (isTristate)
    {
      // Only tristates have interesting drives
      *driveBuf = &inputWords[inputBufferIndex];
      inputBufferIndex += numNetWords;
    }
    return inputBufferIndex;
  }

  UInt32 loopCountNumPrimitiveWords(IODB::NameSetLoop loop, bool withMask,
                                    LeafSet* covered, ShellNetWrapper::NetVec* primitiveNets)
  {
    IODBRuntime* db = mHookup->getDB();
    UInt32 totNumWords = 0;
    STSymbolTableNode* node;
    while(loop(&node))
    {
      STAliasedLeafNode* leaf = node->castLeaf();
      STAliasedLeafNode* storage = leaf->getStorage();
      // Only work with storage nodes, keeping the reference counts
      // manageable.
      // We cannot use insertWithCheck here because we need to
      // populate the primitive vector before we add it to the covered
      // set. See the comment below.
      if (leaf && (covered->count(storage) == 0))
      {
        // The looped nets may have been flattened and thus bypassed
        // the covered check. So, we just need to run through the locally
        // added primitive nets, add them to the covered set as well as
        // the primitiveNet set if they weren't covered before.
        if (! db->isConstant(leaf))
          totNumWords += countNumPrimitiveWords(storage, withMask, primitiveNets, covered);
        covered->insert(storage);
      }
    }

    return totNumWords;
  }
  
  UInt32 countNumPrimitiveWords(STAliasedLeafNode* leaf, bool withMask,
                                ShellNetWrapper::NetVec* allPrimNets, LeafSet* covered)
  {
    // Do not count memories
    const IODBIntrinsic* intrinsic = mHookup->getDB()->getLeafIntrinsic(leaf);
    if (intrinsic->getType() == IODBIntrinsic::eMemory)
      return 0;

    UInt32 totNumWords = 0;
    
    ShellNet* net = mHookup->getCarbonNet(leaf);
    ST_ASSERT(net, leaf);

    ShellNetWrapper::NetVec primitiveNets;
    flattenNet(net, &primitiveNets, covered);
    
    allPrimNets->insert(allPrimNets->end(), primitiveNets.begin(), primitiveNets.end());
    
    // Grab the numWords needed to represent the value
    for (ShellNetWrapper::NetVec::const_iterator p = primitiveNets.begin(),
           e = primitiveNets.end(); p != e; ++p)
    {
      ShellNet* chk = *p;
      
      // Sanity check of the nets. They should not be
      // wrapped nets before we set up for recording, 
      ST_ASSERT(chk->castShellNetWrapper() == NULL, chk->getName());
      UInt32 numWords = chk->getNumUInt32s();

      // If this is a tristate, or if it's a state output and the mask
      // space was also requested, double the words required.  Note
      // that tristates don't use masks, so there's no condition that
      // requires the size to be tripled.
      bool stateOutputWithMask = false;
      if (withMask) {
        STAliasedLeafNode *primLeaf = chk->getNameAsLeaf();
        const ShellDataBOM *bomdata = ShellSymTabBOM::getLeafBOM(primLeaf);
        stateOutputWithMask = bomdata->isStateOutput();
      }

      if (chk->isTristate() || stateOutputWithMask) {
        numWords *= 2;
      }
      
      totNumWords += numWords;
    }
    
    return totNumWords;
  }

  ShellNetRecordResponse*
  allocateRecordResponseNet(ShellNet* primNet, ShellNetRecord* stimNet,
                            ReplayRecordBuffer* responseBuffer)
  { 
    UInt32 numNetWords = primNet->getNumUInt32s();
    bool isTristate = primNet->isTristate();
    
    // Buffer allocation to the wrapper net
    UInt32* valueBuf;
    UInt32* driveBuf;
    ShellNetReplay::Touched* touchedBuffer;
    UInt32 valueOffset;
    UInt32 touchedOffset;
    UInt32* dummyValueShadow;
    UInt32* dummyDriveShadow;
    touchedBuffer = mRecorder->allocResponseTouched(&touchedOffset);
    if (isTristate) {
      responseBuffer->allocateValues(numNetWords, &valueOffset, &valueBuf,
                                     &driveBuf, &dummyValueShadow,
                                     &dummyDriveShadow, touchedBuffer,
                                     stimNet);
    } else {
      // Non-tristates don't need drive, so pass NULL to prevent
      // allocation.  Masks are only allocated for state output
      // stimulus nets.
      responseBuffer->allocateValues(numNetWords, &valueOffset, &valueBuf,
                                     NULL, &dummyValueShadow, NULL,
                                     touchedBuffer, stimNet);
    }
    
    // Create the Record wrapper for the net
    ShellNetRecordResponse* recNet = NULL;
    if (isTristate)
      recNet = new ShellNetRecordResponseTristate(primNet, valueBuf, driveBuf,
                                                  touchedBuffer);
    else
      recNet = new ShellNetRecordResponse(primNet, valueBuf, touchedBuffer);
    
    // Set the internal index to the Touch buffer
    recNet->putInternalIndex(touchedOffset);

    // initialize the response buffers
    recNet->updateBuffer(mHookup->getCarbonModel());
    return recNet;
  } // allocateRecordResponseNet
  
  void flattenNet(ShellNet* net, ShellNetWrapper::NetVec* netVec, LeafSet* covered)
  {
    // See if the underlying net is a force or an expr net
    ShellNetWrapper* wrapper = net->castShellNetWrapper();
    if (wrapper)
    {
      // gather the wrapped nets
      ShellNetWrapper::NetVec tmpNetVec;
      wrapper->gatherWrappedNets(&tmpNetVec);

      // the wrapped nets may also be wrapper nets (split nets of
      // forcibles)
      for (ShellNetWrapper::NetVec::iterator p =  tmpNetVec.begin(),
             e = tmpNetVec.end(); p != e; ++p)
      {
        ShellNet* innerNet = *p;
        // RECURSE 
        flattenNet(innerNet, netVec, covered);
      }
    }
    else
    {
      // Ah, finally a primitive
      // Only add it if it hasn't been seen already and it is not a
      // constant.
      STAliasedLeafNode* leaf = net->getNameAsLeaf();
      IODBRuntime* db = mHookup->getDB();
      if (covered->insertWithCheck(leaf->getStorage()) && ! db->isConstant(leaf))
        netVec->push_back(net);
    }
  }

  void unwrap1toNNets()
  {
    // When deleting the ReplayBOM any forcibles and expression nets
    // have netwrapped subnets. Those subnets have to be replaced with
    // their original nets, so we can delete the net wraps.

    // Loop through the full design and find any
    // shellnetwrappers. Any that are 1toN must have their subnets
    // replaced with their original nets.

    // This will modify shared ShellDataBOM objects in the symbol
    // table, so acquire the lock.
    ShellGlobal::lockMutex();

    IODBRuntime* db = mHookup->getDB();
    STSymbolTable* symTab = db->getDesignSymbolTable();
    for (STSymbolTable::NodeLoop q = symTab->getNodeLoop();
         ! q.atEnd(); ++q)
    {
      STSymbolTableNode* node = *q;
      STAliasedLeafNode* leaf = node->castLeaf();
      if (leaf)
      {
        ShellDataBOM* designData = ShellSymTabBOM::getLeafBOM(leaf);
        ShellData* shellData = designData->getShellData();
        if (shellData)
        {
          ShellNet* leafNet = shellData->getCarbonNet(mHookup->getId());
          if (leafNet)
          {
            // Ok we have a net for this node. See if it is a
            // ShellNetWrapper
            ShellNetWrapper* wrapper = leafNet->castShellNetWrapper();
            if (wrapper)
            {
              // See if it is a 1 to N wrapper
              ShellNetWrapper1ToN* wrap1ToN = wrapper->castShellNetWrapper1ToN();
              if (wrap1ToN)
              {
                // Ok, this is a forcible or expr net that has subnets
                // that could be wrapped.
                ShellNetWrapper::NetVec netVec;
                wrap1ToN->gatherWrappedNets(&netVec);
                // run through each net and unwrap it. This will be
                // done very conservatively, skipping any nets that do
                // not show up in the replay table.
                ShellNetWrapper::NetVec replaceNets;
                for (ShellNetWrapper::NetVec::iterator p =  netVec.begin(),
                       e = netVec.end(); p != e; ++p)
                {
                  ShellNet* subNet = *p;
                  STAliasedLeafNode* replayLeaf = findReplayLeaf(subNet->getNameAsLeaf());
                  if (! replayLeaf)
                  {
                    // we didn't wrap this net, just add the subnet to
                    // the replaced vector
                    replaceNets.push_back(subNet);
                  }
                  else
                  {
                    ReplayData* replayData = castBOM(replayLeaf);
                    ShellNet* origNet = replayData->getOriginalNet();
                    if (origNet)
                      replaceNets.push_back(origNet);
                    else
                      // In case we needed to do a fetch in the Replay
                      // table and we didn't actually wrap the sub
                      // nets, just add the original.
                      replaceNets.push_back(subNet);
                  }
                } // for
                
                // now replace the nets
                wrap1ToN->replaceWrappedNets(replaceNets);
              } // if wrap1ToN
            } // if wrapper
          } // if leafNet
        } // if shellData
      } // if leaf
    } // for 

    ShellGlobal::unlockMutex();
  }
};

int
CarbonModel::ReplayBOM::compareCheckPoints(const UtString& chk1,
                                           const UtString& chk2)
{
  MsgContext* msgContext = mHookup->getCarbonModel()->getMsgContext();

  // Open the checkpoints
  ZISTREAMDB(zin1, chk1.c_str());
  if (zin1 == NULL) {
    msgContext->SHLDBFileOpenFail(chk1.c_str(), zin1.getError());
    return -1;
  }
  ZISTREAMDB(zin2, chk2.c_str());
  if (zin2 == NULL) {
    msgContext->SHLDBFileOpenFail(chk2.c_str(), zin2.getError());
    return 1;
  }

  // In a loop compare the data
  int cmp = 0;
  while ((cmp == 0) && !zin1.fileEof() && !zin2.fileEof()) {
    char c1, c2;
    zin1 >> c1;
    zin2 >> c2;
    cmp = c1 - c2;
  }
  return cmp;
} // CarbonModel::ReplayBOM::compareCheckPoints

const char* CarbonModel::ReplayBOM::scVersionFile = "version";
const char* CarbonModel::ReplayBOM::scVhmSignatureFile = "vhm_signature";
const char* CarbonModel::ReplayBOM::scIndexFile = "index";
const char* CarbonModel::ReplayBOM::scSymtabFile = "symtab";
const char* CarbonModel::ReplayBOM::scVersionReplayDBSignature = "Carbon_Replay_Database";
const char* CarbonModel::ReplayBOM::scVhmSigFileSignature = "Carbon_VHM_Signature";
const char* CarbonModel::ReplayBOM::scVersionToken = "Version:";
const char* CarbonModel::ReplayBOM::scIdToken = "Id:";
const char* CarbonModel::ReplayBOM::scInputBufferWidthStr = "InputBufferWidth";
const char* CarbonModel::ReplayBOM::scResponseBufferWidthStr = "ResponseBufferWidth";
const char* CarbonModel::ReplayBOM::scResponseNetsNumStr = "ResponseNetsNum";
const char* CarbonModel::ReplayBOM::scReplaySymTabSig = "ReplaySymTab";
const char* CarbonModel::ReplayBOM::scStimuliNetsNumStr = "StimuliNetsNum";
const char* CarbonModel::ReplayBOM::scStimuliClkNetsNumStr = "StimuliClkNetsNum";


CarbonModel::CarbonModel(CarbonObjectID *descr, 
                         const char* dbFilenameOrBuffer,
                         int bufferSize,
                         CarbonDBType whichDB, bool* dbOpened) : 
  /* 
     For a server timeout of 15300 seconds, we can tolerate designs
     of at least 1 Hz, (2 schedule calls per second). 
     max_schedcalls_btwn_heartbeats = 
        max_heartbeat_interval * slowest_schedcalls_per_second

     15300s * 2calls/s = 30600 sched calls.

     This means that a design can go no slower than 1 Hz for 4 hours
     and 15 minutes. We haven't seen less than 100 Hz, maybe 20 with
     waveforms turned on. So, this is pretty darn safe. We are not
     going to sell if we are running at 1 Hz, anyway.
  */
  cSchedCallLimit(UtLicense::cServerTimeoutMax * 2),
  mTotNumSchedCalls(0),
  mCurrentAPI(eApi_NONE),
  mNumBlockBuckets(0),
  mNumScheduleBuckets(0)
{
  mNumSchedCallsUntilHeartbeat = cSchedCallLimit;
  *dbOpened = true;
  mShadowList = NULL;
  mDataHelper = new DataHelper;
  mIsCheckpointEvent = false;
  mIsInitialized = false;
  mIssueDepositComboWarning = true;
  mIssueDepositConstWarning = true;
  mIssueNotObservableWarning = true;
  mIssueStaleNetWarning = true;
  mSampleSchedHasRun = false;
  mCheckpointReadStream = NULL;
  mCheckpointWriteStream = NULL;
  mPostSchedInaccurateNets = 0;
  mCurrentBlockBucket = &scNoBucket;
  mCurrentScheduleBucket = &scNoBucket;
  mRunMode = eCarbonRunNormal;
  mReplayBOM = NULL;
  mOnDemandMgr = NULL;
  mDBAPI = NULL;
  
  UInt32 id;
  IODBRuntime* iodb = ShellGlobal::gCarbonCreateDB(descr,
                                                   dbFilenameOrBuffer, bufferSize,
                                                   whichDB, &id);
  mHookup = new CarbonHookup (id, descr, this);
  if (!iodb)
    *dbOpened = false;
  else {
    mHookup->putDB(iodb);
    mDBAPI = new CarbonDatabaseRuntime(this);
  }
}

CarbonModel::~CarbonModel()
{  
  // simulation must be over now... call the modeal and let it know.
  mHookup->callSimOver ();

  // We could be in a state where we have init vals, but we didn't
  // do collation yet.
  mDataHelper->clearInitVals(this);
  delete mDataHelper;

  if (mShadowList)
    delete mShadowList;
  mShadowList = NULL;

  CarbonObjectID *descr = getObjectID ();
  ::CarbonCoreDestroyFn destroyFn = mHookup->getDestroyFn();

  delete mReplayBOM;

  delete mOnDemandMgr;

  delete mHookup;

  delete mDBAPI;

  // This must be last so that the error context is still around
  // The error context is reference counted based on the number of
  // carbon objects.
  if (destroyFn)
    (*destroyFn) (descr);

}

// Same thing as sGetSymNode without the cast
static inline const STSymbolTableNode* sGetShellNetSymNode(const ShellNet* shNet)
{
  return shNet->getNameAsLeaf();
}

static inline const STSymbolTableNode* sGetSymNode(const CarbonNet* net)
{
  const ShellNet* shNet = net->castShellNet();
  return sGetShellNetSymNode(shNet);
}

static inline bool sIsShellNetDepositable(const ShellNet* net, 
                                          bool* runCombo)
{
  const STSymbolTableNode* node = sGetShellNetSymNode(net);
  const ShellDataBOM* data = ShellSymTabBOM::getStorageDataBOM(node->castLeaf());
  *runCombo = data->isComboRun();
  return data->isDepositable();
}

bool CarbonModel::isInput(const CarbonNet* net) const
{
  const STSymbolTableNode* node = sGetSymNode(net);
  return mHookup->getDB()->isInput(node);
}

bool CarbonModel::isOutput(const CarbonNet* net) const
{
  const STSymbolTableNode* node = sGetSymNode(net);
  return mHookup->getDB()->isOutput(node);
}

bool CarbonModel::isBidirect(const CarbonNet* net) const
{
  const STSymbolTableNode* node = sGetSymNode(net);
  return mHookup->getDB()->isBidirect(node);
}

bool CarbonModel::isClock(const CarbonNet* net) const
{
  const STSymbolTableNode* node = sGetSymNode(net);
  return mHookup->getDB()->isClock(node);
}

bool CarbonModel::isPosedgeClock(const CarbonNet* net) const
{
  const STSymbolTableNode* node = sGetSymNode(net);
  const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
  return mHookup->getDB()->hasPosedgeEvent(data->getSCHSignature());
}

bool CarbonModel::isNegedgeClock(const CarbonNet* net) const
{
  const STSymbolTableNode* node = sGetSymNode(net);
  const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
  return mHookup->getDB()->hasNegedgeEvent(data->getSCHSignature());
}

bool CarbonModel::isPosedgeReset(const CarbonNet* net) const
{
  const STSymbolTableNode* node = sGetSymNode(net);
  return mHookup->getDB()->isPosedgeReset(node);
}

bool CarbonModel::isNegedgeReset(const CarbonNet* net) const
{
  const STSymbolTableNode* node = sGetSymNode(net);
  return mHookup->getDB()->isNegedgeReset(node);
}

bool CarbonModel::isPrimaryPort(const CarbonNet* net) const
{
  const STSymbolTableNode* node = sGetSymNode(net);
  return (mHookup->getDB()->isPrimaryInput(node) || 
          mHookup->getDB()->isPrimaryOutput(node) ||
          mHookup->getDB()->isPrimaryBidirect(node));
}

bool CarbonModel::isInternal(const CarbonNet* net) const
{
  return ! isPrimaryPort(net);
}

bool CarbonModel::isPrimaryInput(const CarbonNet* net) const
{
  const ShellNet* shlNet = net->castShellNet();
  return shlNet->isInput();
}

bool CarbonModel::isSync(const CarbonNet* net) const
{
  const STSymbolTableNode* node = sGetSymNode(net);
  const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
  return mHookup->getDB()->isSync(data->getSCHSignature());
}

bool CarbonModel::isAsync(const CarbonNet* net) const
{
  const STSymbolTableNode* node = sGetSymNode(net);
  const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
  return mHookup->getDB()->isAsync(data->getSCHSignature());
}

bool CarbonModel::isReset(const CarbonNet* net) const
{
  return (isPosedgeReset(net) || isNegedgeReset(net));
}

bool CarbonModel::isDepositable(const CarbonNet* net) const
{
  const ShellNet* shNet = net->castShellNet();
  bool dummy;
  return sIsShellNetDepositable(shNet, &dummy);
}

bool CarbonModel::isMemDepositable(const CarbonMemory* mem) const
{
  const ShellNet* shNet = mem->castMemToShellNet();
  bool depositable = false;
  if (shNet->getName())
  {
    bool runCombo;
    depositable = sIsShellNetDepositable(shNet, &runCombo);
  }
  return depositable;
}

void CarbonModel::maybeMemDepositRunCombo(const CarbonMemory* mem)
{
  const ShellNet* shNet = mem->castMemToShellNet();
  // The HierName on the ShellNet may be null if this is a testdriver
  // memory used for stimulus.
  if (shNet->getName())
  {
    mHookup->setSeenDeposit();

    bool runCombo;
    if (sIsShellNetDepositable(shNet, &runCombo))
    {
      mHookup->addRunDepositComboSched(runCombo);
    }
  }
}

bool CarbonModel::isForcible(const CarbonNet* net) const
{
  const ShellNet* shlNet = net->castShellNet();
  return shlNet->isForcible();
}

CarbonWave* CarbonModel::waveCreate(WaveDump* dumpFile,
                                    const UtString* errMsg) 
{
  CarbonWaveImp* wave = NULL;
  if (! dumpFile)
    mHookup->getMsgContext()->SHLFileProblem(errMsg->c_str());
  else if (mHookup->getWave() != NULL)
  {
    delete dumpFile;
    mHookup->getMsgContext()->SHLMultipleWavesNotSupported();
  }
  else
  {
    wave = mHookup->createWave();
    wave->putInit(dumpFile);
  }
  return wave;
}

CarbonWave* CarbonModel::getCarbonWave()
{
  return mHookup->getWave();
}

void CarbonModel::waveClose()
{
  mHookup->closeWave();
}

CarbonNet* CarbonModel::findNet(const char* netName)
{
  ShellNet* net = NULL;
  if (strlen(netName) > 0)
  {
    // When looking up nets, we want to warn about invalid nodes
    const CarbonDatabaseNode* node = mDBAPI->findNode(netName, true);
    net = findShellNet(node, false);
  }
  return net;
}

CarbonShadowStruct* 
CarbonModel::findNetAllocShadow(const char* netName)
{
  CarbonShadowStruct* shadow = NULL;
  CarbonNet* net = findNet(netName);
  if (net) 
    shadow = allocShadow(net);
  return shadow;
}

ShellNet* CarbonModel::findShellNet(const CarbonDatabaseNode* node, bool forCarbonMemory)
{
  ShellNet* net = NULL;
    
  if (node != NULL) {
    // Go through the visibility database.  All the error reporting,
    // replay/onDemand instrumentation, etc. has been consolidated
    // there.
    net = mDBAPI->getCarbonNet(node, forCarbonMemory, false);
  }
  
  return net;
}

CarbonVHMTypeFlags CarbonModel::determineVHMTypeFlags() const
{
  CarbonVHMTypeFlags ret = eVHMNormal;
  // mReplayBOM gets created when enableReplay() is called from
  // ShellGlobal
  if (mReplayBOM || mOnDemandMgr)
  {
    const IODBRuntime* db = mHookup->getDB();
    ret = db->getVHMTypeFlags();
  }
  return ret;
}

CarbonMemory* CarbonModel::findMemory(const char* memName)
{
  // When looking up nets, we want to warn about invalid nodes
  const CarbonDatabaseNode* node = mDBAPI->findNode(memName, true);
  ShellNet* net = findShellNet(node, true);

  CarbonMemory* mem = NULL;
  if (net)
  {
    mem = net->castMemory();
    // findShellNet guarantees that we get a memory
    INFO_ASSERT(mem != NULL, "Expecting a memory");
  }
  return mem;
}

void CarbonModel::freeMemory(CarbonMemory** mem)
{
  if (mem && *mem)
  {
    ShellNet* shlNet = (*mem)->castMemToShellNet();
    mHookup->freeNet(&shlNet);
    *mem = NULL;
  }
}

void CarbonModel::freeNet(CarbonNet** net)
{
  if (net && *net)
  {
    ShellNet* shlNet = (*net)->castShellNet();
    mHookup->freeNet(&shlNet);
    *net = NULL;
  }
}

CarbonStatus CarbonModel::initialize(void* cmodelData, void* reserved1, void* reserved2)
{
  CarbonStatus status = eCarbon_OK;
  if (!isInitialized())
  {
    if (mReplayBOM)
      mReplayBOM->recordInitialScheduleStimuli();
    
    getHookup ()->callInit (cmodelData, reserved1, reserved2);
    putInitialized();

    if (mReplayBOM)
      mReplayBOM->recordInitialSchedule(cmodelData, reserved1, reserved2);

    if (mOnDemandMgr) {
      mOnDemandMgr->finalizeInit();
    }
  }
  else
  {
    MsgContextBase::Severity sev = getMsgContext()->SHLInitializeAlreadyCalled();
    status = ShellGlobal::severityToStatus(sev);
  }
  return status;
}

void CarbonModel::runSampleSchedule() const
{
  if (! mSampleSchedHasRun)
  {
    mHookup->call_debug_schedule ();
    mSampleSchedHasRun = true;
  }
}

void CarbonModel::postSchedule()
{
  // update any inaccurate nets
  if (mPostSchedInaccurateNets > 0)
    runSampleSchedule();
  
  // run value change callbacks
  mDataHelper->runNetCBs(this);
}

CarbonStatus CarbonModel::schedule(CarbonTime simTime)
{
  CarbonStatus status = eCarbon_OK;

  ++mTotNumSchedCalls;

#ifndef PTHREADS
  licCheck();
#endif
  // Make sure the model has been initialized
  if (isInitialized())
  {
    if (! mHookup->getControlHelper()->isFinished() )
    {
      mDataHelper->maybeCollateCBs(this);

      if (mHookup->resetSeenDeposit())
      {
        mSampleSchedHasRun = false;
        status = mHookup->runSchedule(simTime);
        if ((status != eCarbon_ERROR) & (mRunMode != eCarbonRunPlayback))
          postSchedule();
      }
    }
    else
    {
      // we do not allow user to run schedule after a $finish 
      getMsgContext()->SHLTriedToRunScheduleAfterFinish();
      status = eCarbon_ERROR;
    }
  }
  else {
    getMsgContext()->SHLNotInitialized();
    status = eCarbon_ERROR;
  }
  
  return status;
}

CarbonStatus CarbonModel::clkSchedule(CarbonTime simTime)
{
  CarbonStatus status = eCarbon_OK;

  ++mTotNumSchedCalls;

  // Make sure the model has been initialized
  if (isInitialized())
  {
    if (! mHookup->getControlHelper()->isFinished() )
    {
      mDataHelper->maybeCollateCBs(this);
      
      mSampleSchedHasRun = false;
      status = mHookup->runClkSchedule(simTime);
    }
    else
    {
      // we do not allow user to run schedule after a $finish 
      getMsgContext()->SHLTriedToRunScheduleAfterFinish();
      status = eCarbon_ERROR;
    }
  }
  else {
    getMsgContext()->SHLNotInitialized();
    status = eCarbon_ERROR;
  }
  
  return status;
}

CarbonStatus CarbonModel::dataSchedule(CarbonTime simTime)
{
  CarbonStatus status = eCarbon_OK;

#ifndef PTHREADS
  licCheck();
#endif
  
  // Make sure the model has been initialized
  if (isInitialized())
  {
    if (! mHookup->getControlHelper()->isFinished() )
    {
      mDataHelper->maybeCollateCBs(this);
      
      mSampleSchedHasRun = false;
      status = mHookup->runDataSchedule(simTime);
      if ((status != eCarbon_ERROR) & (mRunMode != eCarbonRunPlayback))
        postSchedule();
    }
    else
    {
      // we do not allow user to run schedule after a $finish 
      getMsgContext()->SHLTriedToRunScheduleAfterFinish();
      status = eCarbon_ERROR;
    }
  }
  else {
    getMsgContext()->SHLNotInitialized();
    status = eCarbon_ERROR;
  }
  
  return status;
}

CarbonStatus CarbonModel::asyncSchedule(CarbonTime simTime)
{
  CarbonStatus status = eCarbon_OK;

#ifndef PTHREADS
  licCheck();
#endif
  
  // Make sure the model has been initialized
  if (isInitialized())
  {
    if (! mHookup->getControlHelper()->isFinished() )
    {
      mDataHelper->maybeCollateCBs(this);
      
      mSampleSchedHasRun = false;
      status = mHookup->runAsyncSchedule(simTime);
      if ((status != eCarbon_ERROR) & (mRunMode != eCarbonRunPlayback))
        postSchedule();
    }
    else
    {
      // we do not allow user to run schedule after a $finish 
      getMsgContext()->SHLTriedToRunScheduleAfterFinish();
      status = eCarbon_ERROR;
    }
  }
  else {
    getMsgContext()->SHLNotInitialized();
    status = eCarbon_ERROR;
  }
  
  return status;
}

void CarbonModel::depComboSchedule()
{
  if (mHookup->checkAndClearRunDepositComboSchedule()) {
    mHookup->call_deposit_combo_schedule();
  }
}

CarbonTime CarbonModel::getSimulationTime() const
{
  // For performance reasons, OnDemand keeps a local copy of the time
  // when in idle mode, since it's expensive to update the model's
  // copy.  Use that copy if necessary.
  if (mOnDemandMgr && mOnDemandMgr->isIdle()) {
    return mOnDemandMgr->getTime();
  }

  return mHookup->getTime();
}

void CarbonModel::putSimulationTime(CarbonTime simTime)
{
  (void) mHookup->setTime(simTime);
}


CarbonHookup* CarbonModel::getHookup()
{
  return mHookup;
}

const CarbonHookup* CarbonModel::getHookup() const
{
  return const_cast<CarbonModel*>(this)->getHookup();
}

void CarbonModel::setToUndriven(CarbonNet* net)
{
  ShellNet* shlNet = net->castShellNet();
  shlNet->setToUndriven(this);
}

void CarbonModel::resolveXdrive(CarbonNet* net)
{
  ShellNet* shlNet = net->castShellNet();
  shlNet->resolveXdrive(this);
}

void CarbonModel::getExternalDrive(CarbonNet* net, UInt32* xdrv)
{
  ShellNet* shlNet = net->castShellNet();
  shlNet->getExternalDrive(xdrv);
}

bool CarbonModel::checkObservable(const ShellNet* shlNet) const
{
  bool isObservable = true;
  if (mIssueNotObservableWarning || mIssueStaleNetWarning)
  {
    const STAliasedLeafNode* netNode = shlNet->getNameAsLeaf();
    const ShellDataBOM* netBOM = ShellSymTabBOM::getStorageDataBOM(netNode);

    // if the waveform is active, then the debug schedule was run and
    // the value would be correct.

    // Always report an inaccurate net even if it is accurate. We want
    // the user to recompile the design with that net marked observable.
    bool needsDebugSched = netBOM->needsDebugSched();

    // An invalid wave net is never observable, and should be checked
    // ahead of debug schedule. This is possible with -noDebugSchedule.
    if (mIssueNotObservableWarning && netBOM->isInvalidWaveNet())
    {
      UtString nameBuf;
      ShellSymTabBOM::composeName(netNode, &nameBuf, false, false);
      MsgContextBase::Severity sev =
        getMsgContext()->SHLNetNotObservable(nameBuf.c_str());
      switch (sev)
      {
      case MsgContextBase::eSuppress:
        // save time next time on warning
        mIssueNotObservableWarning = false;
        // fall through here
      case MsgContextBase::eStatus:
      case MsgContextBase::eNote:
      case MsgContextBase::eWarning:
      case MsgContextBase::eContinue:
        isObservable = true;
        break;
      case MsgContextBase::eFatal: // can't be here if it is fatal
      case MsgContextBase::eAlert:
      case MsgContextBase::eError:
        isObservable = false;
        break;
      } 
    }
    else if (mIssueStaleNetWarning && needsDebugSched)
    {
      UtString nameBuf;
      ShellSymTabBOM::composeName(netNode, &nameBuf, false, false);
      MsgContextBase::Severity sev =
        getMsgContext()->SHLNetStaleValue(nameBuf.c_str(), nameBuf.c_str());
      switch (sev)
      {
      case MsgContextBase::eSuppress:
        // save time next time on warning
        mIssueStaleNetWarning = false;
        // fall through here
      case MsgContextBase::eStatus:
      case MsgContextBase::eNote:
      case MsgContextBase::eWarning:
      case MsgContextBase::eContinue:
        isObservable = true;
        break;
      case MsgContextBase::eFatal: // can't be here if it is fatal
      case MsgContextBase::eAlert:
      case MsgContextBase::eError:
        isObservable = false;
        break;
      } 

      // if isObservable is true here then we know we need to run the
      // debug schedule. If false, we error anyway, so no need to run
      // it
      if (isObservable)
        runSampleSchedule();
    }
  }
  return isObservable;
}

CarbonStatus CarbonModel::examine(const CarbonNet* net, UInt32* value, UInt32* drive) 
{
  const ShellNet* shlNet = net->castShellNet();
  bool isObservable = checkObservable(shlNet);
  CarbonStatus stat = shlNet->examine(value, drive, ShellNet::eIDrive, this);
  if ((stat == eCarbon_OK) && ! isObservable)
    stat = eCarbon_ERROR;
  return stat;
}

CarbonStatus CarbonModel::examineWord(const CarbonNet* net, UInt32* value, 
                                       int index, UInt32* drive)
{
  const ShellNet* shlNet = net->castShellNet();
  bool isObservable = checkObservable(shlNet);
  CarbonStatus stat = shlNet->examineWord(value, index, drive, ShellNet::eIDrive, this);
  if ((stat == eCarbon_OK) && ! isObservable)
    stat = eCarbon_ERROR;
  return stat;
}
  
CarbonStatus CarbonModel::examineRange(const CarbonNet* net, UInt32 *buf, int range_msb, int range_lsb, UInt32* drive)
{
  const ShellNet* shlNet = net->castShellNet();
  bool isObservable = checkObservable(shlNet);
  CarbonStatus stat = net->castShellNet()->examineRange(buf, range_msb, range_lsb, drive, this);
  if ((stat == eCarbon_OK) && ! isObservable)
    stat = eCarbon_ERROR;
  return stat;
}

void CarbonModel::putIssueDepositComboWarning(bool flag) {
  mIssueDepositComboWarning = flag;
}

void CarbonModel::putIssueDepositConstWarning(bool flag) {
  mIssueDepositConstWarning = flag;
}

void CarbonModel::putIssueNotObservableWarning(bool flag) {
  mIssueNotObservableWarning = flag;
}

void CarbonModel::putIssueStaleNetWarning(bool flag) {
  mIssueStaleNetWarning = flag;
}

bool CarbonModel::checkForcible(ShellNet* shlNet, 
                                bool* runCombo) const
{
  *runCombo = false;
  bool canForce = isForcible(shlNet);
  if (! canForce)
  {
    char nameBuf[10000];
    getName(shlNet, nameBuf, 10000);
    getMsgContext()->SHLNetNotForcible(nameBuf);
  }
  else
  {
    const STSymbolTableNode* node = sGetShellNetSymNode(shlNet);
    const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
    *runCombo = data->isComboRun();
  }
  return canForce;
}

#if 1
CarbonStatus CarbonModel::deposit(CarbonNet* net, const UInt32* buf, const UInt32* drive)
{
  return net->deposit(buf, drive, this);
}

CarbonStatus CarbonModel::depositWord(CarbonNet* net, UInt32 buf, int index, UInt32 drive)
{
  return net->depositWord(buf, index, drive, this);
}

CarbonStatus CarbonModel::depositRange(CarbonNet* net, const UInt32* buf, 
                                       int range_msb, int range_lsb, 
                                       const UInt32* drive)
{
  return net->depositRange(buf, range_msb, range_lsb, drive, this);
}
#endif

CarbonStatus CarbonModel::force(CarbonNet* net, const UInt32* buf)
{
  ShellNet* shlNet = net->castShellNet();
  CarbonStatus stat = eCarbon_ERROR;
  bool runCombo = false;
  if (checkForcible(shlNet, &runCombo))
  {
    if (buf)
      stat = shlNet->force(buf, this);
    else
    {
      stat = eCarbon_ERROR;
      
      char nameBuf[10000];
      getName(shlNet, nameBuf, 10000, false);
      getMsgContext()->SHLNullForceValue(nameBuf);
    }
  }
  
  return stat;
}

CarbonStatus CarbonModel::forceWord(CarbonNet* net, UInt32 buf, int index)
{
  ShellNet* shlNet = net->castShellNet();

  CarbonStatus stat = eCarbon_ERROR;
  bool runCombo = false;
  if (checkForcible(shlNet, &runCombo))
  {
    stat = shlNet->forceWord(buf, index, this);
  }
  return stat;
}

CarbonStatus CarbonModel::forceRange(CarbonNet* net, const UInt32* buf, 
                                     int range_msb, int range_lsb)
{
  ShellNet* shlNet = net->castShellNet();
  CarbonStatus stat = eCarbon_ERROR;
  bool runCombo = false;
  if (checkForcible(shlNet, &runCombo))
  {
    if (buf)
      stat = shlNet->forceRange(buf, range_msb, range_lsb, this);
    else
    {
      stat = eCarbon_ERROR;

      char nameBuf[10000];
      getName(shlNet, nameBuf, 10000, false);
      getMsgContext()->SHLNullForceValue(nameBuf);
    }
  }
  return stat;
}

CarbonStatus CarbonModel::release(CarbonNet* net)
{
  ShellNet* shlNet = net->castShellNet();
  CarbonStatus stat = eCarbon_ERROR;
  // don't care about the result of runCombo because release doesn't
  // change the value. If this is a combo node, the value that is
  // driving the combo node will propagate the next time that schedule
  // is called.
  bool runCombo; 
  if (checkForcible(shlNet, &runCombo))
  {
    shlNet->release(this);
    if (shlNet->castExprNet() == NULL)
    {
      // Don't set any changed flags. We don't want input schedules to
      // run on a release
      shlNet->setToUndriven(this);
    }
 
    // Update the drivers (the design may be driving)
    mHookup->addRunDepositComboSched(true);
    stat = eCarbon_OK;
  }
  
  return stat;
}

CarbonStatus CarbonModel::releaseWord(CarbonNet* net, int index)
{
  ShellNet* shlNet = net->castShellNet();
  CarbonStatus stat = eCarbon_ERROR;
  bool runCombo; 
  if (checkForcible(shlNet, &runCombo))
  {
    stat = shlNet->releaseWord(index, this);
    if (stat == eCarbon_OK)
    {
      if (shlNet->castExprNet() == NULL)
        shlNet->setWordToUndriven(index, this);
      
      // see release()
      mHookup->addRunDepositComboSched(true);
    }
  }
  return stat;
}

CarbonStatus CarbonModel::releaseRange(CarbonNet* net, 
                                       int range_msb, int range_lsb)
{
  ShellNet* shlNet = net->castShellNet();
  CarbonStatus stat = eCarbon_ERROR;
  bool runCombo; 
  if (checkForcible(shlNet, &runCombo))
  {
    stat = shlNet->releaseRange(range_msb, range_lsb, this);
    if (stat == eCarbon_OK)
    {
      if (shlNet->castExprNet() == NULL)
        shlNet->setRangeToUndriven(range_msb, range_lsb, this);
      
      // see release()
      mHookup->addRunDepositComboSched(true);
    }
  }
  return stat;
}

CarbonStatus CarbonModel::format(const CarbonNet* net, char* valueStr, size_t len, CarbonRadix strFormat)
{
  const ShellNet* shlNet = net->castShellNet();
  const STSymbolTableNode* node = sGetSymNode(net);
  const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
  bool isObservable = checkObservable(shlNet);
  CarbonStatus stat = shlNet->format(valueStr, len, strFormat, data->getNetFlags(), this);
  if ((stat == eCarbon_OK) && ! isObservable)
    stat = eCarbon_ERROR;
  return stat;
}

CarbonStatus CarbonModel::getName(const CarbonNet* net, char* buffer, size_t len, bool includeVector) const
{
  CarbonStatus stat = eCarbon_OK;
  if (!net)
  {
    getMsgContext()->SHLInvalidHandle();
    return eCarbon_ERROR;
  }

  UtString nameBuf;
  composeNetName(net, &nameBuf, includeVector);

  size_t bufSize = nameBuf.size();
  if (bufSize > (len - 1)) 
    stat = eCarbon_ERROR;
  else 
    strncpy(buffer, nameBuf.c_str(), bufSize + 1);
  return stat;
}

void CarbonModel::composeNetName(const CarbonNet* net, UtString* nameBuf, bool includeVector) const
{
  const ShellNet* shlNet = net->castShellNet();
  // If we have a CarbonDatabaseNode, use that
  const CarbonDatabaseNode* dbNode = shlNet->getDBNode();
  if (dbNode != NULL) {
    *nameBuf = mDBAPI->getFullName(dbNode);
  } else {
    ShellSymTabBOM::composeName(shlNet->getNameAsLeaf(), nameBuf, false, includeVector);
  }
}

CarbonShadowStruct*
CarbonModel::allocShadow(CarbonNet* net)
{
  ShellNet* shlNet = net->castShellNet(); 
  CarbonShadowStruct::Shadow netShadow = 
    static_cast<CarbonShadowStruct::Shadow>(shlNet->allocShadow());
  CarbonShadowStruct* shadow = new CarbonShadowStruct(netShadow, net);
  if (! mShadowList)
    mShadowList = new ShadowList;
  mShadowList->add(shadow);
  return shadow;
}

void CarbonModel::freeShadow(CarbonShadowStruct** shadow)
{
  if (shadow && *shadow)
  {
    ASSERT(mShadowList->remove(*shadow));
    *shadow = NULL;
  }
}

bool CarbonModel::compareUpdate(CarbonShadowStruct* shadow)
{
  bool changed = false;
  CarbonNet* net = shadow->mNet;
  ShellNet* shlNet = net->castShellNet();
  ShellNet::Storage val = static_cast<ShellNet::Storage>(shadow->mShadow);
  changed = (shlNet->compareAndUpdate(&val) == ShellNet::eChanged);
  return changed;
}

CarbonNetValueCBData*
CarbonModel::changeCallbackFinish(CarbonNetValueCBData* cbData, ShellNet* shlNet)
{
  const STAliasedLeafNode* netNode = shlNet->getNameAsLeaf();
  const ShellDataBOM* netBOM = ShellSymTabBOM::getStorageDataBOM(netNode);
  if (netBOM->needsDebugSched())
  {
    cbData->putIsSampleScheduled(true);
    ++mPostSchedInaccurateNets;
  }
  
  if (mReplayBOM)
    mReplayBOM->addNetValueChangeCB(cbData);
  if (mOnDemandMgr)
    mOnDemandMgr->addNetValueChangeCB(cbData);

  return cbData;
} // CarbonModel::changeCallbackFinish

CarbonNetValueCBData* CarbonModel::addNetChangeCB(CarbonNetValueCB fn,
                                                  CarbonClientData userData,
                                                  CarbonNet* net)
{
  INFO_ASSERT(mDataHelper, "Auxiliary class, DataHelper, not initialized.");
  ShellNet* shlNet = net->castShellNet();

  
  if (! checkObservable(shlNet))
    return NULL;

  CarbonNetValueCBData* cbData = mDataHelper->addCB(this, fn, userData, shlNet);
  return changeCallbackFinish(cbData, shlNet);
}

CarbonNetValueCBData* CarbonModel::addNetValueDriveCB(CarbonNetValueCB fn,
                                                      CarbonClientData userData,
                                                      CarbonNet* net)
{
  INFO_ASSERT(mDataHelper, "Auxiliary class, DataHelper, not initialized.");
  ShellNet* shlNet = net->castShellNet();

  
  if (! checkObservable(shlNet))
    return NULL;

  CarbonNetValueCBData* cbData = mDataHelper->addValueDriveCB(this, fn, userData,
                                                              shlNet);
  return changeCallbackFinish(cbData, shlNet);
}

CarbonNetCBData* CarbonModel::addNetChangeCB_old(CarbonNetCB fn,
                                                 CarbonClientData userData,
                                                 CarbonNet* net)
{
  CarbonNetCBData* cbData = new CarbonNetCBData(fn, userData);
  CarbonNetValueCBData* newCB = addNetChangeCB(sOldNetCB, cbData, net);
  cbData->mNewLayer = newCB;
  mDataHelper->addOldCB(cbData);
  return cbData;
}

bool CarbonModel::areAliased(const CarbonNet* n1, const CarbonNet* n2) const
{
  bool ret = n1 && n2;
  if (ret)
  {
    const ShellNet* shl1 = n1->castShellNet();
    const ShellNet* shl2 = n2->castShellNet();
    const STAliasedLeafNode* shlNode1 = shl1->getNameAsLeaf();
    const STAliasedLeafNode* shlNode2 = shl2->getNameAsLeaf();
    INFO_ASSERT(shlNode1, "Corrupt net");
    INFO_ASSERT(shlNode2, "Corrupt net");
    ret = (shlNode1->getStorage() == shlNode2->getStorage());
  }
  return ret;
}

CarbonPullMode CarbonModel::getPullMode(const CarbonNet* net) const
{
  const STSymbolTableNode* node = sGetSymNode(net);
  return mHookup->getDB()->getPullMode(node);
}

int CarbonModel::hasDriveConflict(CarbonNet* net) const
{
  ShellNet* shlNet = net->castShellNet();
  return shlNet->hasDriveConflict();
}

void CarbonModel::updateNetValueChangeCBShadows(bool collate)
{
  if (collate) {
    mDataHelper->maybeCollateCBs(this);
  }
  mDataHelper->updateCBShadows();
}

void CarbonModel::maybeDisableOnDemand()
{
  if (mOnDemandMgr)
    mOnDemandMgr->disable(true);
}

void CarbonModel::maybeReEnableOnDemand()
{
  if (mOnDemandMgr)
    mOnDemandMgr->reEnable(true);
}

void CarbonModel::onDemandPreSave()
{
  if (mOnDemandMgr) {
    // carbonSave(), much like carbonExamine(), requires a restore but
    // not a break from idle.  Force an OnDemand state restore of the
    // model so that the normal save state is correct.
    //
    // For debug mode, we need to create a temporary debug info object
    // referring to the model as a whole.
    OnDemandDebugInfo info(eCarbonOnDemandDebugAPI, mHookup->getDB()->getTopLevelModuleName());
    mOnDemandMgr->restoreState(eCarbonOnDemandDebugRestore, &info);
  }
}

void CarbonModel::onDemandPreRestore()
{
  if (mOnDemandMgr) {
    // carbonRestore() is an non idle access, forcing a break from
    // any idle state or idle pattern search, since the entire model
    // state is about to change.
    //
    // For debug mode, we need to create a temporary debug info object
    // referring to the model as a whole.
    OnDemandDebugInfo info(eCarbonOnDemandDebugAPI, mHookup->getDB()->getTopLevelModuleName());
    mOnDemandMgr->setNonIdleAccess(&info, eCarbonOnDemandDebugNonIdleChange);
  }
}

ShellNet* CarbonModel::applyNetWrappers(ShellNet* origNet, bool doOnDemand)
{
  // Ensure Replay and OnDemand wrappers are in place
  ShellNet *wrappedNet = origNet;
  if (origNet != NULL) {
    if (mReplayBOM) {
      // Note that this net was looked up, and instrument it for the
      // current Replay mode.
      mReplayBOM->addFoundNet(origNet);
      wrappedNet = mReplayBOM->instrumentNet(origNet);
    }
    // The OnDemand net will wrap the Replay net, if necessary
    if (doOnDemand && (mOnDemandMgr != NULL)) {
      wrappedNet = mOnDemandMgr->wrapNet(wrappedNet);
    }
  }
  return wrappedNet;
}

static CarbonUInt32 sZStreamRead(void *context, void *data, CarbonUInt32 numBytes)
{
  Zistream *in = (Zistream *) context;
  return in->read((char *) data, numBytes);  
}

static CarbonUInt32 sZStreamWrite(void *context, const void *data, CarbonUInt32 numBytes)
{
  Zostream *out = (Zostream *) context;
  if (out->write(data, numBytes)) {
    return numBytes;
  }
  else {
    return 0; // error: assume zero bytes written
  }
}

static void sCheckpointReadError(void *context, UtString &message)
{
  (void) context; // not used
  ShellGlobal::getProgErrMsgr()->SHLCheckpointRestoreError(message.c_str());
}

static void sCheckpointWriteError(void *context, UtString &message)
{
  (void) context; // not used
  ShellGlobal::getProgErrMsgr()->SHLCheckpointSaveError(message.c_str());
}

CarbonStatus CarbonModel::simCommonSave(ZostreamDB& zout)
{
  MsgContext *msgContext = getMsgContext();

  CarbonStatus stat = eCarbon_OK;
  if (! zout)
  {
    UtString filename;
    zout.getFilename(&filename);
    msgContext->SHLDBFileOpenFail(filename.c_str(), zout.getError());
    stat = eCarbon_ERROR;
  }
  else
  {
    UtString filename;
    zout.getFilename(&filename);
    stat = simStreamSave(sZStreamWrite, &zout, filename.c_str());
  }
  
  if ((stat == eCarbon_OK) && zout.fail())
  {
    msgContext->SHLFileProblem(zout.getError());
    stat = eCarbon_ERROR;
  }
  return stat;
}

CarbonStatus CarbonModel::simSave(const char *filename)
{
  // open the file and call the model's save
  
  ZOSTREAMDB(zout, filename);
  return simCommonSave(zout);
}

CarbonStatus CarbonModel::simCommonRestore(ZistreamDB& zin)
{
  MsgContext *msgContext = getMsgContext();

  // We can't restore Carbon Models prior to versioning the carbon_id
  if (mHookup->getDB()->getIODBVersion() < 5)
  {
    msgContext->SHLIncompatibleVHMForRestore();
    return eCarbon_ERROR;
  }
  
  CarbonStatus stat = eCarbon_OK;
  if (! zin)
  {
    UtString filename;
    zin.getFilename(&filename);
    msgContext->SHLDBFileOpenFail(filename.c_str(), zin.getError());
    stat = eCarbon_ERROR;
  }
  else
  {
    UtString filename;
    zin.getFilename(&filename);
    stat = simStreamRestore(sZStreamRead, &zin, filename.c_str());
  }
  
  if ((stat == eCarbon_OK) && zin.fail())
  {
    msgContext->SHLFileProblem(zin.getError());
    stat = eCarbon_ERROR;
  }
  return stat;
}

CarbonStatus CarbonModel::simRestore(const char *filename)
{
  ZISTREAMDB(zin, filename);
  return simCommonRestore(zin);
}

CarbonStatus
CarbonModel::simStreamSave(CarbonStreamWriteFunc func, void *userData, const char *streamName)
{
  UtOCheckpointStream out(func, userData, streamName, sCheckpointWriteError);
  return save(out);
}

CarbonStatus
CarbonModel::simStreamRestore(CarbonStreamReadFunc func, void *userData, const char *streamName)
{
  UtICheckpointStream in(func, userData, streamName, sCheckpointReadError);
  return restore(in);
}

CarbonStatus
CarbonModel::save(UtOCheckpointStream &out)
{
  if (getRunMode() == eCarbonRunPlayback)
  {
    getMsgContext()->SHLNoPlaybackSaveRestore();
    return eCarbon_ERROR;
  }

  // Tell OnDemand we're about to do a save, if necessary
  onDemandPreSave();
  
  CarbonStatus status = eCarbon_OK;

  // The stream pointer is set so that callbacks can use checkpointWrite to 
  // save their own data to our file.
  mCheckpointWriteStream = &out;

  // save random seed
  out.writeToken("shell");
  SInt32 *pRandomSeed = ShellGlobal::gCarbonGetRandomSeed(mHookup->getHandle ());
  out << *pRandomSeed;

  // save CarbonModel state
  out.writeToken("CarbonModel");
  out << mIsInitialized;

  // save the carbon_id and model state
  if (!mHookup->save(out, this)) {
    status = eCarbon_ERROR;
  }

  // Execute any registered save callbacks.
  out.writeToken("callbacks");
  ShellGlobal::gcarbonPrivateGetControlHelper(mHookup->getHandle ())
    ->runControlSysTask(mHookup->getHandle (), 0 /* verbosity */, eCarbonSave, "file", 0 /* line number */);
  mCheckpointWriteStream = NULL;

  // santiy test end of data marker
  out.writeToken("end");

  return status;
}

CarbonStatus
CarbonModel::restore(UtICheckpointStream &in)
{
  // Don't allow restore during playback unless we are recovering. The
  // restore is actually done before the mode change to recover is
  // executed, so we need to check if we are in a checkpoint event and
  // we are in a playback. If we are in a checkpoint event, then we
  // are restoring for recovery.
  // We cannot restore during record, either.
  if ((getRunMode() == eCarbonRunPlayback) && ! isCheckpointEvent())
  {
    getMsgContext()->SHLNoPlaybackSaveRestore();
    return eCarbon_ERROR;
  }
  else if (getRunMode() == eCarbonRunRecord)
  {
    getMsgContext()->SHLNoRecordRestore();
    return eCarbon_ERROR;
  }
  
  if (mHookup->getDB()->getIODBVersion() < 5)
  {
    getMsgContext()->SHLIncompatibleVHMForRestore();
    return eCarbon_ERROR;
  }
  
  // Tell OnDemand we're about to do a restore, if necessary
  onDemandPreRestore();
  
  CarbonStatus status = eCarbon_OK;
  CarbonOpaque handle = mHookup->getHandle ();
  CarbonWaveImp *wave = mHookup->getWave();

  // Flush any pending wave data we have before changing the model state.
  // (but only if the hierarchy has been closed).
  if (wave && !wave->isHierarchyOpen()) {
    wave->flushPendingData();
  }

  // The stream pointer is set so that callbacks can use checkpointRead to 
  // read their data from our file.
  mCheckpointReadStream = &in;
    
  // restore random seed
  if (!in.checkToken("shell")) return eCarbon_ERROR;
  SInt32 *pRandomSeed = ShellGlobal::gCarbonGetRandomSeed(mHookup->getHandle ());
  in >> *pRandomSeed;

  // restore CarbonModel state
  if (!in.checkToken("CarbonModel")) return eCarbon_ERROR;
  in >> mIsInitialized;

  // restore carbon_id and generated model state
  if (!mHookup->restore(in, this)) {
    status = eCarbon_ERROR;
  }

  // Execute any registered restore callbacks.
  if (!in.checkToken("callbacks")) return eCarbon_ERROR;
  ShellGlobal::gcarbonPrivateGetControlHelper(handle)
    ->runControlSysTask(handle, 0 /* verbosity */, eCarbonRestore, "file", 0 /* line number */);
  mCheckpointReadStream = NULL;

  // santiy test end of data marker
  if (!in.checkToken("end")) return eCarbon_ERROR;

  // Force current model state into the waveform.
  // (only if the hierarchy has been closed)
  if (wave && !wave->isHierarchyOpen()) {
#if 0
    // save a copy of the change array data
    CarbonChangeType saveInit = hdl->mInit;
    CarbonChangeType *saveArray = CARBON_ALLOC_VEC(CarbonChangeType, hdl->getChangeArraySize());
    ::memcpy(saveArray, hdl->getChangeArray(), hdl->getChangeArraySize());

    // temporarily hack the change array to indicate that everything
    // has changed
    hdl->mInit = CARBON_CHANGE_FALL_MASK;
    hdl->reInitChangeArray();

    // write the waveform with our artificial changes
    wave->manualUpdateWaveform();

    // restore the change array and mInit
    ::memcpy(hdl->getChangeArray(), saveArray, hdl->getChangeArraySize() * sizeof(CarbonChangeType));
    hdl->mInit = saveInit;
    CARBON_FREE_VEC(saveArray, CarbonChangeType, hdl->getChangeArraySize());
#else
    // save a copy of the change array data
    CarbonHookup::ChangeArrayDescr saveArray;
    mHookup->saveChangeArray (saveArray);

    // temporarily hack the change array to indicate that everything
    // has changed
    mHookup->setChangeArray (CARBON_CHANGE_FALL_MASK);

    // write the waveform with our artificial changes
    wave->manualUpdateWaveform();

    mHookup->restoreChangeArray (saveArray);
#endif
  }

  // Update shadow values to match the restored model.
  updateNetValueChangeCBShadows(false);
  return status;
}

CarbonStatus
CarbonModel::checkpointWrite(const void *data, UInt32 numBytes)
{
  CarbonStatus status = eCarbon_OK;

  if (mCheckpointWriteStream == NULL) {
    // The write stream must be set by the save call before the user can
    // write any data to the checkpoint file.
    getMsgContext()->SHLCheckpointWriteNoStream();
    status = eCarbon_ERROR;
  }
  else if (numBytes > 0 && data == NULL) {
    // if writing more than 0 bytes, the data pointer cannot be NULL
    getMsgContext()->SHLCheckpointWriteNullData(numBytes);
    status = eCarbon_ERROR;
  }
  else {
    // write the byte count
    *mCheckpointWriteStream << numBytes;
    if (mCheckpointWriteStream->fail()) {
      status = eCarbon_ERROR;
    }
    else {
      // write the actual bytes
      if(numBytes > 0 && !mCheckpointWriteStream->write(data, numBytes))
      {
        status = eCarbon_ERROR;
      }
    }
  }
  return status;
}
  
CarbonStatus
CarbonModel::checkpointRead(void *data, UInt32 numBytes)
{
  CarbonStatus status = eCarbon_OK;
  UInt32 bytesInCheckpoint;

  if (mCheckpointReadStream == NULL) {
    // The read stream must be set by the restore call before the user can
    // read any data from the checkpoint file.
    getMsgContext()->SHLCheckpointReadNoStream();
    status = eCarbon_ERROR;
  }
  else if (numBytes > 0 && data == NULL) {
    // if reading more than 0 bytes, the data pointer cannot be NULL
    getMsgContext()->SHLCheckpointReadNullData(numBytes);
    status = eCarbon_ERROR;
  }
  else {
    // read the byte count
    *mCheckpointReadStream >> bytesInCheckpoint;
    if (mCheckpointReadStream->fail()) {
      status = eCarbon_ERROR;
    }
    else if (bytesInCheckpoint != numBytes) {
      getMsgContext()->SHLCheckpointReadBadCount(numBytes, bytesInCheckpoint);
      status = eCarbon_ERROR;
    }
    else {
      // read the actual bytes
      if(numBytes > 0 && mCheckpointReadStream->read((char *) data, numBytes) != numBytes) {
        status = eCarbon_ERROR;
      }
    }
  }
  return status;
}

void CarbonModel::disableNetValueCB(CarbonNetValueCBData* cbData)
{
  // OnDemand always needs the raw callback enabled, and has its own
  // enable/disable mechanism.
  if (mOnDemandMgr) {
    mOnDemandMgr->setNetCBEnable(cbData, false);
    return;
  }

  if (cbData->isEnabled())
  {
    if (cbData->isSampleScheduled() && (mPostSchedInaccurateNets > 0))
      --mPostSchedInaccurateNets;
    cbData->disable();
  }
}

void CarbonModel::enableNetValueCB(CarbonNetValueCBData* cbData)
{
  // OnDemand always needs the raw callback enabled, and has its own
  // enable/disable mechanism.
  if (mOnDemandMgr) {
    mOnDemandMgr->setNetCBEnable(cbData, true);
    return;
  }

  if (! cbData->isEnabled())
  {
    if (cbData->isSampleScheduled())
      ++mPostSchedInaccurateNets;
    cbData->enable();
    // Make sure we run net callbacks on the next schedule call.
    mHookup->setSeenDeposit();
  }
}

void CarbonModel::setupProfiling(CarbonProfileInfo* prof_info)
{
  mNumBlockBuckets = prof_info->numBlockBuckets;
  mNumScheduleBuckets = prof_info->numScheduleBuckets;
  mCurrentBlockBucket = prof_info->currentBlockBucket;
  mCurrentScheduleBucket = prof_info->currentScheduleBucket;
  Profile::instance()->addModel(this, prof_info->modelName);
}

MsgContext* CarbonModel::getMsgContext() const
{
  return mHookup->getMsgContext();
}

CarbonReplayInfo* CarbonModel::getReplayInfo()
{
  CarbonReplayInfo* info = NULL;
  if (mReplayBOM)
    info = mReplayBOM->getReplayInfo();
  return info;
}


CarbonStatus CarbonModel::replayRecordStart()
{
  CarbonStatus ret = eCarbon_ERROR;
  if (mReplayBOM)
  {
    if (mHookup->getDB()->getIODBVersion() < 5)
    {
      getMsgContext()->SHLReplayCannotRecordOldVHM();
      return eCarbon_ERROR;
    }
    
    ret = mReplayBOM->recordStart(mRunMode);
    if (ret == eCarbon_OK)
      INFO_ASSERT(mRunMode == eCarbonRunRecord, "Record mode not set.");
  }
  else
    getMsgContext()->SHLReplayDisabled();
  
  return ret;
}

CarbonStatus CarbonModel::replayRecordStop()
{
  CarbonStatus ret = eCarbon_ERROR;
  if (mReplayBOM)
    ret = mReplayBOM->recordStop();
  else
    getMsgContext()->SHLReplayDisabled();

  return ret;
}


CarbonStatus CarbonModel::replayPlaybackStart()
{
  CarbonStatus ret = eCarbon_ERROR;
  if (mReplayBOM)
  {   
    ret = mReplayBOM->playbackStart(mRunMode);
    if (ret == eCarbon_OK)
      INFO_ASSERT(mRunMode == eCarbonRunPlayback, "Playback mode not set.");
  }
  else
    getMsgContext()->SHLReplayDisabled();
  return ret;
}

CarbonStatus CarbonModel::replayPlaybackStop()
{
  CarbonStatus ret = eCarbon_ERROR;
  if (mReplayBOM)
    ret = mReplayBOM->playbackStop();
  else
    getMsgContext()->SHLReplayDisabled();
  return ret;
}

CarbonStatus CarbonModel::replaySetVerboseDivergence(bool verbose)
{
  CarbonStatus ret = eCarbon_ERROR;
  if (mReplayBOM)
    ret = mReplayBOM->setVerboseDivergence(verbose);
  else
    getMsgContext()->SHLReplayDisabled();
  return ret;
}

bool CarbonModel::replayIsVerboseDivergence()
{
  if (mReplayBOM) {
    return mReplayBOM->isVerboseDivergence();
  }
  return false;
}

void CarbonModel::enableReplay()
{
  INFO_ASSERT(mReplayBOM == NULL, "Double initialization of replay.");
  mReplayBOM = new ReplayBOM(mHookup);  
}

void CarbonModel::enableOnDemand()
{
  // These defaults can be overridden by onDemandSetMaxStates()
  // and onDemandConfigureBackoff()
  CarbonUInt32 max_states = 20;
  CarbonUInt32 backoff_states = 1000;

  mOnDemandMgr = new OnDemandMgr(this, max_states, backoff_states);

  // Find the memories in the design and store their
  // memory callback objects.
  LeafVector vec;
  IODBRuntime* db = mHookup->getDB();
  sFindDesignMemories(db, &vec);
  for (UInt32 i = 0; i < vec.size(); ++i) {
    STAliasedLeafNode *leafStorage = vec[i];
    ShellNet* primMem = mHookup->getCarbonNet(leafStorage);
    // If there's no net, there's nothing to detect changes on.
    // This can happen, for instance, if a constant memory is optimized away.
    if (primMem) {
      CarbonModelMemory* modelMem = primMem->castModelMemory();
      ST_ASSERT(modelMem, primMem->getName());
      ShellMemoryCBManager* cbManager = modelMem->getShellMemoryCBManager();
      // All observable mems must have managers. If this is
      // an old Carbon Model without memory callbacks, we should have
      // caught that during the create call.
      ST_ASSERT(cbManager, primMem->getName());

      mOnDemandMgr->addMemoryCB(cbManager, primMem);
    }
  }
}

int CarbonModel::onDemandIsEnabled()
{
  return (mOnDemandMgr != NULL);
}

CarbonStatus CarbonModel::onDemandSetMaxStates(CarbonUInt32 max_states)
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return eCarbon_ERROR;
  }

  mOnDemandMgr->setMaxStates(max_states);
  return eCarbon_OK;
}

CarbonStatus CarbonModel::onDemandSetBackoffStrategy(CarbonOnDemandBackoffStrategy strategy)
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return eCarbon_ERROR;
  }

  mOnDemandMgr->setBackoffStrategy(strategy);
  return eCarbon_OK;
}

CarbonStatus CarbonModel::onDemandSetBackoffCount(CarbonUInt32 count)
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return eCarbon_ERROR;
  }

  mOnDemandMgr->setBackoffCount(count);
  return eCarbon_OK;
}

CarbonStatus CarbonModel::onDemandSetBackoffDecayPercentage(CarbonUInt32 percentage)
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return eCarbon_ERROR;
  }

  mOnDemandMgr->setBackoffDecayPercentage(percentage);
  return eCarbon_OK;
}

CarbonStatus CarbonModel::onDemandSetBackoffMaxDecay(CarbonUInt32 max)
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return eCarbon_ERROR;
  }

  mOnDemandMgr->setBackoffMaxDecay(max);
  return eCarbon_OK;
}

CarbonOnDemandCBDataID* CarbonModel::onDemandAddModeChangeCB(CarbonOnDemandModeChangeCBFunc fn, void *userData)
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return NULL;
  }

  CarbonOnDemandCBDataID *cb = mOnDemandMgr->addModeChangeCB(fn, userData);
  return cb;
}

CarbonStatus CarbonModel::onDemandEnableStats()
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return eCarbon_ERROR;
  }

  mOnDemandMgr->enableStats();
  return eCarbon_OK;
}

CarbonStatus CarbonModel::onDemandStop()
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return eCarbon_ERROR;
  }

  mOnDemandMgr->disable(false);
  return eCarbon_OK;
}

CarbonStatus CarbonModel::onDemandStart()
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return eCarbon_ERROR;
  }

  mOnDemandMgr->reEnable(false);
  return eCarbon_OK;
}

CarbonStatus CarbonModel::onDemandSetDebugLevel(CarbonOnDemandDebugLevel level)
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return eCarbon_ERROR;
  }

  mOnDemandMgr->setDebugLevel(level);
  return eCarbon_OK;
}

CarbonOnDemandCBDataID* CarbonModel::onDemandAddDebugCB(CarbonOnDemandDebugCBFunc fn, void* userData)
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return NULL;
  }

  // This might fail if debug mode hasn't been configured yet.
  CarbonOnDemandCBDataID *cb = mOnDemandMgr->addDebugCB(fn, userData);
  return cb;
}

CarbonStatus CarbonModel::onDemandSetCBEnable(CarbonOnDemandCBDataID* cb, bool enable)
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return eCarbon_ERROR;
  }

  // This might fail if the CBData is invalid
  CarbonStatus stat = mOnDemandMgr->setUserCBEnable(cb, enable);
  return stat;
}

CarbonStatus CarbonModel::onDemandMakeIdleDeposit(CarbonNetID *net)
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return eCarbon_ERROR;
  }

  CarbonStatus stat = mOnDemandMgr->makeIdleDeposit(net);
  return stat;
}

CarbonStatus CarbonModel::onDemandExclude(CarbonNetID *net)
{
  // The onDemand manager must be active
  if (mOnDemandMgr == 0) {
    getMsgContext()->SHLOnDemandDisabled();
    return eCarbon_ERROR;
  }

  CarbonStatus stat = mOnDemandMgr->exclude(net);
  return stat;
}

void CarbonModel::onDemandForceNonIdleAccess(const UtString& nodeName)
{
  OnDemandMgr* onDemandMgr = getOnDemandMgr();
  if (onDemandMgr != NULL) {
    OnDemandDebugInfo debugInfo(eCarbonOnDemandDebugAPI, nodeName);
    onDemandMgr->setNonIdleAccess(&debugInfo, eCarbonOnDemandDebugNonIdleChange);
  }
}

void CarbonModel::onDemandForceStateRestore(const UtString& nodeName)
{
  OnDemandMgr* onDemandMgr = getOnDemandMgr();
  if (onDemandMgr != NULL) {
    OnDemandDebugInfo debugInfo(eCarbonOnDemandDebugAPI, nodeName);
    onDemandMgr->restoreState(eCarbonOnDemandDebugRestore, &debugInfo);
  }
}

const CarbonDatabaseNode* CarbonModel::getDBNodeFromNet(CarbonNetID* net)
{
  const ShellNet* shellNet = net->castShellNet();
  // If we have a CarbonDatabaseNode, use that
  const CarbonDatabaseNode* node = shellNet->getDBNode();
  // If not, create a DB node from the symtab node
  if (node == NULL) {
    const STSymbolTableNode* stNode = shellNet->getNameAsLeaf();
    node = mDBAPI->translateToDB(stNode);
  }
  return node;
}

CarbonStatus CarbonModel::examineArrayToFile(const CarbonDatabaseNode* node, CarbonRadix format, const char* filename)
{
  // Delegate to CarbonDatabaseRuntime
  CarbonStatus stat = mDBAPI->examineArrayToFile(node, format, filename);
  return stat;
}

CarbonStatus CarbonModel::depositArrayFromFile(const CarbonDatabaseNode* node, CarbonRadix format, const char* filename)
{
  // Delegate to CarbonDatabaseRuntime
  CarbonStatus stat = mDBAPI->depositArrayFromFile(node, format, filename);
  return stat;
}

bool CarbonModel::onDemandDivergentCModel()
{
  return (mOnDemandMgr && mOnDemandMgr->anyCModelDivergence());
}

void CarbonModel::onDemandInstallSchedulePointers()
{
  if (mOnDemandMgr) {
    // OnDemand has two sets of schedule pointers: One set for when
    // it's enabled, and another for when it's disabled.  In order for
    // the disabled functions to be run when OnDemand is disabled by
    // replay, the disabled functions need to be installed before
    // replay is created.  This causes replay to save the disabled
    // functions as its normal mode functions.  After replay is
    // created, though, we want OnDemand to use its enabled schedule
    // functions.
    mOnDemandMgr->installEnabledSchedulePointers();
  }
}

DynBitVectorPair* CarbonModel::getNetValueChangeCBBuffer()
{
  return mDataHelper->getValueBuffer();
}

CarbonStatus CarbonModel::replayRecordScheduleCheckpoint()
{
  CarbonStatus ret = eCarbon_ERROR;
  if (mReplayBOM)
    ret = mReplayBOM->scheduleRecordCheckpoint();
  else
    getMsgContext()->SHLReplayDisabled();
  
  return ret;
}

CarbonStatus CarbonModel::saveReplayCheckpoint(const char* fileName, const ReplayCheckpointClosure& closure)
{
  putIsCheckpointEvent(true);
  
  ZOSTREAMDB(zout, fileName);
  CarbonStatus stat = simCommonSave(zout);
  
  if (stat == eCarbon_OK)
  {
    if (! closure.dbWrite(zout))
    {
      getMsgContext()->SHLFileProblem(zout.getError());
      stat = eCarbon_ERROR;
    }
  }
  
  putIsCheckpointEvent(false);
  return stat;
}

CarbonStatus CarbonModel::restoreReplayCheckpoint(const char* fileName, ReplayCheckpointClosure* closure)
{
  putIsCheckpointEvent(true);

  ZISTREAMDB(zin, fileName);
  CarbonStatus stat = simCommonRestore(zin);
  if (stat == eCarbon_OK)
  {
    if (! closure->dbRead(zin))
    {
      getMsgContext()->SHLFileProblem(zin.getError());
      stat = eCarbon_ERROR;
    }
  }
  
  putIsCheckpointEvent(false);
  return stat;
}
  
ReplayCModelMap *CarbonModel::getReplayCModelMap()
{
  if (mReplayBOM) {
    return mReplayBOM->getCModelMap();
  }
  return NULL;
}

void
CarbonModel::registerCModel(void* cmodelData, void* userData, UInt32 context,
                            const char* name,
                            UInt32 numInputs, UInt32 numOutputs,
                            CModelPlaybackFn fn)
{
  //  UtIO::cout() << "Registering Cmodel: " << name << ", " << cmodelData << ", " << context << UtIO::endl;
  if (mReplayBOM)
  {
    // If we are in recovery, ignore this call. It is being called
    // from the initial schedule.
    if (getRunMode() == eCarbonRunRecover)
      return;
    
    mReplayBOM->registerCModel(cmodelData, userData, context, name, 
                               numInputs, numOutputs, fn);
  }

  if (mOnDemandMgr)
    mOnDemandMgr->registerCModel(cmodelData, userData, context, name,
                                 numInputs, numOutputs, fn);
}

void 
CarbonModel::preRecordCModel(void* cmodelData, UInt32 context, UInt32* outputs)
{
  //UtIO::cout() << "PreRecord Cmodel: " << cmodelData << ", " << context << UtIO::endl;
  if (mReplayBOM)
    mReplayBOM->preRecordCModel(cmodelData, context, outputs);
  if (mOnDemandMgr)
    mOnDemandMgr->preRecordCModel(cmodelData, context, outputs);
}

void
CarbonModel::recordCModel(void* cmodelData, UInt32 context,
                          UInt32* inputs, UInt32* outputs)
{
  //UtIO::cout() << "Recording Cmodel: " << cmodelData << ", " << context << UtIO::endl;
  if (mReplayBOM)
    mReplayBOM->recordCModel(cmodelData, context, 
                             inputs, outputs);
  if (mOnDemandMgr)
    mOnDemandMgr->recordCModel(cmodelData, context, 
                               inputs, outputs);
}


CarbonModel::CModelRecoveryStatus
CarbonModel::getCModelValues(void* cmodelData, UInt32 context,
                             UInt32** outputs)
{
  INFO_ASSERT(mReplayBOM || mOnDemandMgr, "getCModelValues called while not in replay/onDemand mode.");
  // onDemand knows when it needs to propagate cmodel outputs back
  // into the Carbon Model on a divergence, so give it precedence.
  if (mOnDemandMgr) {
    CarbonModel::CModelRecoveryStatus status;
    if (mOnDemandMgr->maybeGetCModelCallOutput(&status, cmodelData, context, outputs))
      return status;
  }

  return mReplayBOM->getCModelCallOutput(cmodelData, context, outputs);
}

CarbonReplayCModelData::CarbonReplayCModelData(CarbonObjectID* descr, void* hndl,
                                               void* cmodel, int maxInputWords,
                                               int maxOutputWords) :
  mModel(descr ? descr->getModel () : NULL), mHndl(hndl), mCmodel(cmodel),
  mMaxInputWords(maxInputWords), mMaxOutputWords(maxOutputWords)
{
  mInputs = CARBON_ALLOC_VEC(UInt32, maxInputWords);
  mOutputs = CARBON_ALLOC_VEC(UInt32, maxOutputWords);
}

CarbonReplayCModelData::~CarbonReplayCModelData()
{
  CARBON_FREE_VEC(mInputs, UInt32, mMaxInputWords);
  CARBON_FREE_VEC(mOutputs, UInt32, mMaxOutputWords);
}

IODBRuntime* CarbonModel::getIODB() const {
  return mHookup->getDB();
}

void carbonPrivateBeginChangeIndToStorageMap (CarbonObjectID *descr)
{
  CarbonModel *model = descr->getModel ();
  model->getHookup ()->beginChangeIndToStorageMap ();
}

void carbonPrivateMapChangeIndToStorage (CarbonObjectID *descr, SInt32 change, SInt32 offset)
{
  CarbonModel *model = descr->getModel ();
  model->getHookup ()->mapChangeIndToStorage (change, offset);
}

void carbonPrivateEndChangeIndToStorageMap (CarbonObjectID *descr)
{
  CarbonModel *model = descr->getModel ();
  model->getHookup ()->endChangeIndToStorageMap ();
}

void carbonPrivateRegisterCModel (CarbonObjectID *descr, void* cmodelData, void *userData, 
  UInt32 context, const char* name, UInt32 numInputWords, 
  UInt32 numOutputWords, CModelPlaybackFn fn)
{
  CarbonModel *model = descr->getModel ();
  model->registerCModel (cmodelData, userData, context, name, numInputWords, 
    numOutputWords, fn);
}

int carbonPrivateIsCheckpointEvent (CarbonObjectID *descr)
{
  return descr->getModel ()->isCheckpointEvent();
}

#if 0
CarbonStatus ScheduleFnContainer::callSchedule(CarbonModel* model, CarbonTime simTime)
{
  return model->getHookup ()->runSchedule (simTime);
}

CarbonStatus ScheduleFnContainer::callClkSchedule(CarbonModel* model, CarbonTime simTime)
{
  return model->getHookup ()->runClkSchedule (simTime);
}

CarbonStatus ScheduleFnContainer::callDataSchedule(CarbonModel* model, CarbonTime simTime)
{
  return model->getHookup ()->runDataSchedule (simTime);
}

CarbonStatus ScheduleFnContainer::callAsyncSchedule(CarbonModel* model, CarbonTime simTime)
{
  return model->getHookup ()->runAsyncSchedule (simTime);
}
#endif
