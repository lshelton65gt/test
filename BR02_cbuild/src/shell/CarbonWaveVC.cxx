// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// author: Mark Seneski

#include "util/CarbonPlatform.h"
#include "CarbonWaveVC.h"
#include "schedule/Event.h"
#include "shell/CarbonHookup.h"
#include "shell/ShellData.h"
#include "shell/ShellNetWrapper.h"
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STSymbolTable.h"
#include <algorithm>
#include <string.h>

static bool sSortByNoTriNumBytes(const CarbonWaveNetAssoc* a1, const CarbonWaveNetAssoc* a2);
static bool sSortGroupByNoTriNumBytes(const CarbonWaveNetAssocGroup* g1, const CarbonWaveNetAssocGroup* g2);
static void sCollateNets(const NetAssocVec* vec, const NetAssocGroupVec* groupVec, CarbonExamineScheduler::NetTraitClosure* traitClosure);
template <class Assoc>
static void sVCUpdatePod(VCVec* vcObjs, UtArray<Assoc*>* assocs, CarbonWaveNetAssoc::NetWriteFlags flags, WaveDump* dataFile);
static void sExtractBitselAssocs(NetAssocVec* handles, NetAssocGroupVec* groupHandles);
static void sSyncGroupAssocsAndVCs(const NetAssocGroupVec* groupAssocs, const VCVec* groupVCs);

class NonPodAssoc
{
public:
  CARBONMEM_OVERRIDES

  NonPodAssoc(CarbonWaveNetAssoc* assoc) 
  {
    mShadowValue = assoc->getNet()->allocShadow();
    mAssoc = assoc;
  }

  ~NonPodAssoc() 
  {
    mAssoc->getNet()->freeShadow(&mShadowValue);
  }
  
  ShellNet::Storage mShadowValue;
  CarbonWaveNetAssoc* mAssoc;
};

class ProcessHandleNonPod
{
private:
  CarbonWaveNetAssoc::NetWriteFlags mFlags;
  WaveDump* mWaveFile;
  
public: CARBONMEM_OVERRIDES
  ProcessHandleNonPod(CarbonWaveNetAssoc::NetWriteFlags flags, WaveDump* waveFile) 
    : mFlags(flags), mWaveFile(waveFile)
  {}
  
  void operator()(NonPodAssoc* nonPodAssoc) {
    CarbonWaveNetAssoc* assoc = nonPodAssoc->mAssoc;
    bool isChanged = assoc->writeIfNotEq(nonPodAssoc->mShadowValue);
    assoc->maybeUpdateValue(mFlags, mWaveFile, isChanged, false);
  }
  
};

template <class Assoc>
class ProcessHandlePod
{
private:
  CarbonWaveNetAssoc::NetWriteFlags mFlags;
  WaveDump* mWaveFile;
  bool mIsChanged;
  
public: CARBONMEM_OVERRIDES
  ProcessHandlePod(CarbonWaveNetAssoc::NetWriteFlags flags, WaveDump* waveFile, bool isChanged) 
    : mFlags(flags), mWaveFile(waveFile), mIsChanged(isChanged)
  {}
  
  void operator()(Assoc* assoc) {
    assoc->maybeUpdateValue(mFlags, mWaveFile, mIsChanged, true);
  }
};

template <class Assoc>
class ProcessVC
{
private:
  UtArray<Assoc*>* mHandles;
  CarbonWaveNetAssoc::NetWriteFlags mFlags;
  WaveDump* mWaveFile;

public: CARBONMEM_OVERRIDES
  ProcessVC(UtArray<Assoc*>* handles, CarbonWaveNetAssoc::NetWriteFlags flags, WaveDump* waveFile) 
    : mHandles(handles), mFlags(flags), mWaveFile(waveFile)
  {}
  
  void operator()(CarbonValueChangeBase* vc) {
    // always update the shadows
    SInt32 numChanges = vc->processChanges();
    if (numChanges > 0)
    {
      const CarbonValueChangeBase::IndexVec& changedIndices = vc->getChangedIndices();
      ProcessHandlePod<Assoc> helper(mFlags, mWaveFile, true);
      
      for (CarbonValueChangeBase::IndexVecCLoop p(changedIndices);
           ! p.atEnd(); ++p)
      {
        Assoc* assoc = (*mHandles)[*p];
        helper(assoc);
      }
    }
  }
};


CarbonWaveVC::CarbonWaveVC(CarbonHookup* hookup, WaveDump* vcdFile) 
  : mHookup(hookup), mDataFile(vcdFile), mGroups(NULL)
{
  mMemPool = new MiniMemPool;
}

CarbonWaveVC::~CarbonWaveVC()
{
  for (VCVecLoop q(mPodCheckVCs);
       ! q.atEnd();
       ++q)
  {
    CarbonValueChangeBase* base = *q;
    base->clear();
  }
  
  for (VCVecLoop q(mPodCheckGroupVCs);
       ! q.atEnd();
       ++q)
  {
    CarbonValueChangeBase* base = *q;
    base->clear();
  }
  
  for (NetAssocGroupVecLoop p(mPodCheckGroupHandles); ! p.atEnd(); ++p) {
    CarbonWaveNetAssocGroup* group = *p;
    delete group;
  }

  for (SchedStimToGroup::UnsortedLoop p = mSchedStimToGroup.loopUnsorted(); 
       ! p.atEnd(); ++p)
  {
    WaveScheduleGroup* g = p.getValue();
    delete g;
  }

  delete mMemPool;

  if (mGroups) {
    UInt32 numGroups = mSchedStimToGroup.size() + 1; // +1 for trailing NULL
    CARBON_FREE_VEC(mGroups, WaveScheduleGroup*, numGroups);
  }
}

WaveScheduleGroup* CarbonWaveVC::addSchedStimGroup(ScheduleStimuli* stim)
{
  WaveScheduleGroup* group = NULL;
  SchedStimToGroup::iterator p = mSchedStimToGroup.find(stim);
  if (p == mSchedStimToGroup.end())
  {
    group = new WaveScheduleGroup(stim);
    mSchedStimToGroup[stim] = group;
  }
  else
    group = p->second;
  return group;
}

void CarbonWaveVC::cleanupAssocRefs()
{
  for (NonPodAssocVecLoop p(mNonPodCheckHandles);
       ! p.atEnd(); ++p)
    delete *p;
  mNonPodCheckHandles.clear();
}

void CarbonWaveVC::addNonPodCheck(CarbonWaveNetAssoc* netAssoc)
{
  NonPodAssoc* nonPodAssoc = new NonPodAssoc(netAssoc);
  mNonPodCheckHandles.push_back(nonPodAssoc);
}

void CarbonWaveVC::addPodCheck(CarbonWaveNetAssoc* netAssoc)
{
  mPodCheckHandles.push_back(netAssoc);
}

void CarbonWaveVC::allocateNetTrackers()
{
  for (SchedStimToGroup::iterator p = mSchedStimToGroup.begin(); 
       p != mSchedStimToGroup.end(); ++p)
  {
    WaveScheduleGroup* group = p->second;
    group->collateNets(mMemPool);
  }

  sExtractBitselAssocs(&mPodCheckHandles, &mPodCheckGroupHandles);
  // sort the PODCheckHandles
  std::sort(mPodCheckHandles.begin(), mPodCheckHandles.end(), sSortByNoTriNumBytes);
  // Sort the group handles, too, using the size of the wrapped net
  std::sort(mPodCheckGroupHandles.begin(), mPodCheckGroupHandles.end(), sSortGroupByNoTriNumBytes);
  
  CarbonExamineScheduler::NetTraitClosure netClosure;
  sCollateNets(&mPodCheckHandles, &mPodCheckGroupHandles, &netClosure);
  netClosure.allocVCs(&mPodCheckVCs, &mPodCheckGroupVCs, mMemPool);
  sSyncGroupAssocsAndVCs(&mPodCheckGroupHandles, &mPodCheckGroupVCs);
}


#if SORT_BY_OFFSET
static bool sSortByOffset(const CarbonWaveNetAssoc* a1, const CarbonWaveNetAssoc* a2)
{
  const STAliasedLeafNode* leaf1 = a1->getNet()->getNameAsLeaf();
  const STAliasedLeafNode* leaf2 = a2->getNet()->getNameAsLeaf();
  const ShellDataBOM* bomdata1 = ShellSymTabBOM::getLeafBOM(leaf1);      
  const ShellDataBOM* bomdata2 = ShellSymTabBOM::getLeafBOM(leaf2);      
  SInt32 offset1 = bomdata1->getStorageOffset();
  SInt32 offset2 = bomdata2->getStorageOffset();
  return offset1 > offset2;
}
#endif

static bool sSortByNoTriNumBytes(const CarbonWaveNetAssoc* a1, const CarbonWaveNetAssoc* a2)
{
  return CarbonExamineScheduler::sSortByNoTriNumBytes(a1->getNet(), a2->getNet());
}

static bool sSortGroupByNoTriNumBytes(const CarbonWaveNetAssocGroup* g1, const CarbonWaveNetAssocGroup* g2)
{
  UInt32 size1 = g1->getNumAssocs();
  UInt32 size2 = g2->getNumAssocs();
  return size1 < size2;
}

static void sCollateNets(const NetAssocVec* vec, const NetAssocGroupVec* groupVec, CarbonExamineScheduler::NetTraitClosure* traitClosure)
{
  for (NetAssocVecCLoop p(*vec); ! p.atEnd(); ++p)
  {
    const CarbonWaveNetAssoc* assoc = *p;
    const ShellNet* net = assoc->getNet();
    INFO_ASSERT(net, "Corrupt net entry.");
    
    traitClosure->addNet(net);
  }
  for (NetAssocGroupVecCLoop p(*groupVec); ! p.atEnd(); ++p)
  {
    const CarbonWaveNetAssocGroup* group = *p;
    const void* storage = group->getStorage();
    UInt32 width = group->getNumAssocs();
    traitClosure->addGroup(storage, width);
  }
}

void CarbonWaveVC::makeGroupVector()
{
  INFO_ASSERT(mGroups == NULL, "Function called more than once.");
  
  UInt32 numGroups = mSchedStimToGroup.size() + 1; // +1 for trailing NULL
  mGroups = CARBON_ALLOC_VEC(WaveScheduleGroup*, numGroups);
  // null out the array
  memset(mGroups, 0, sizeof(WaveScheduleGroup*) * numGroups);

  UInt32 index = 0;
  for (SchedStimToGroup::UnsortedLoop p = mSchedStimToGroup.loopUnsorted(); 
       ! p.atEnd(); ++p)
  {
    mGroups[index] = p.getValue();
    ++index;
  }
  
}

void CarbonWaveVC::setChangedNets(CarbonWaveNetAssoc::NetWriteFlags flags)
{
  std::for_each(mNonPodCheckHandles.begin(), mNonPodCheckHandles.end(), ProcessHandleNonPod(flags, mDataFile));

  sVCUpdatePod(&mPodCheckVCs, &mPodCheckHandles, flags, mDataFile);
  sVCUpdatePod(&mPodCheckGroupVCs, &mPodCheckGroupHandles, flags, mDataFile);

  if (! mGroups)
    makeGroupVector();

  bool forceWrite = ((flags & CarbonWaveNetAssoc::eValWriteVal) != 0);
  
  for (WaveScheduleGroup** allGroups = mGroups; 
       *allGroups != NULL; ++allGroups)
  {
    WaveScheduleGroup* group = *allGroups;
    ScheduleStimuli* stim = group->getStim();

    if (stim->doTransfer())
      stim->transferMaps(mHookup);
    
    // CURRENTLY THIS IS THE ONLY PLACE THE UPDATE HAPPENS
    // If somewhere else tries to update, the information will be
    // wrong. I didn't add a dirty bit to this, because the resetting
    // of that showed up to be a bit of a performance hit. So, when
    // the need arises for multiple things using the same
    // schedulestimuli, I need to implement a fast dirty bit cleaning
    // mechanism.
    if (stim->hasChange())
      stim->update();
    
    if (forceWrite || stim->isStimulated())
      group->processNets(flags, mDataFile);
  }
  
  //  std::for_each(mSchedStims.begin(), mSchedStims.end(), ProcessSchedule(mHookup, flags, mDataFile));
}

template <class Assoc>
static void sVCUpdatePod(VCVec* vcObjs, UtArray<Assoc*>* assocs, CarbonWaveNetAssoc::NetWriteFlags flags, WaveDump* dataFile)
{
  if (! CarbonWaveNetAssoc::sDoWriteVal(flags))
    std::for_each(vcObjs->begin(), vcObjs->end(), ProcessVC<Assoc>(assocs, flags, dataFile));
  else
  {
    // update the shadows
    for (VCVecLoop p(*vcObjs); ! p.atEnd(); ++p)
    {
      CarbonValueChangeBase* vc = *p;
      vc->executeCompares();
    }
    // Write all handles
    std::for_each(assocs->begin(), assocs->end(), ProcessHandlePod<Assoc>(flags, dataFile, false));    
  }
}

//! Key class for saving storage location/net pairs in a map
class StorageNetPair
{
public:
  CARBONMEM_OVERRIDES

  StorageNetPair(const void* storage, ShellNet* net) : mStorage(storage), mNet(net) {}
  ~StorageNetPair() {}

  size_t hash() const
  {
    return reinterpret_cast<size_t>(mStorage) * 17 + reinterpret_cast<size_t>(mNet);
  }

  bool operator==(const StorageNetPair& other) const
  {
    return ((mStorage == other.mStorage) && (mNet == other.mNet));
  }

private:
  const void* mStorage;
  ShellNet* mNet;
};

/*!
  We need to go through all the POD CarbonWaveNetAssoc objects and
  extract those that represent a bitselect of a storage location, as
  opposed to the whole thing.  Those bitselect objects get grouped
  according to their storage location.  This allows a location to be
  mapped to a group of WaveHandles instead of a single one.
 */
static void sExtractBitselAssocs(NetAssocVec* handles, NetAssocGroupVec* groupHandles)
{
  // We need a way to determine that we've already created a
  // CarbonWaveNetAssocGroup for a particular bitselect.  Save them in
  // a map.
  //
  // We're not smart about determining that visibility nets are
  // actually aliases of each other.  This means that the same storage
  // location may actually be used by multiple
  // CarbonWaveNetAssocGroups.  Also, the underlying primitive net
  // shared by the group may be shared, too.  For example, if it's a
  // memory, each word in the memory corresponds to a different group.
  //
  // The (storage, net) pair should be unique, though, so use that to
  // look up groups that already exist.
  typedef UtHashMap<StorageNetPair, CarbonWaveNetAssocGroup*> GroupMapType;
  GroupMapType groupMap;
  NetAssocVec nonBitselHandles;

  for (NetAssocVecLoop l(*handles); !l.atEnd(); ++l) {
    CarbonWaveNetAssoc* assoc = *l;
    ShellNet* net = assoc->getNet();
    ShellNet::Traits traits;
    net->getTraits(&traits);
    SInt32 bitsel = traits.getPodBitsel();
    if (bitsel == -1) {
      // This is not a bitselect, so it will stay in the original
      // list.
      nonBitselHandles.push_back(assoc);
    } else {
      // Add it to the group handle list.  We may already have an
      // entry for the storage location.  To get the right pointer
      // type, we need the width of the wrapped net.
      ShellNetWrapper* netWrapper = net->castShellNetWrapper();
      ST_ASSERT(netWrapper != NULL, net->getNameAsLeaf());
      ShellNetWrapper1To1* netWrapper1To1 = netWrapper->castShellNetWrapper1To1();
      ST_ASSERT(netWrapper1To1 != NULL, net->getNameAsLeaf());
      ShellNet* wrappedNet = netWrapper1To1->getNet();
      UInt32 width = wrappedNet->getBitWidth();

      const void* storage;
      if (width <= 8) {
        storage = traits.getByte();
      } else if (width <= 16) {
        storage = traits.getShort();
      } else if ((width > 32) && (width <= 64)) {
        storage = traits.getLLong();
      } else {
        storage = traits.getLongPtr();
      }

      // See if the group already exists.  Otherwise, create a new
      // one.
      StorageNetPair key(storage, wrappedNet);
      GroupMapType::iterator iter = groupMap.find(key);
      CarbonWaveNetAssocGroup* group;
      if (iter != groupMap.end()) {
        group = iter->second;
        ST_ASSERT(group->getNumAssocs() == width, net->getNameAsLeaf());
      } else {
        group = new CarbonWaveNetAssocGroup(storage, width);
        // Save this in the map
        groupMap[key] = group;
        groupHandles->push_back(group);
      }
      group->addAssoc(assoc, bitsel);
    }
  }

  // Update the list of non-bitsel handles.  This removes the bitsel
  // ones that were added to the new list from the original.
  handles->swap(nonBitselHandles);
}

static void sSyncGroupAssocsAndVCs(const NetAssocGroupVec* groupAssocs, const VCVec* groupVCs)
{
  // Once everything is set up, each group association needs to know
  // its corresponding VC object and its index within the VC's arrays.
  // This is because the association needs to access the VC's changed
  // bits to know which specific net handles have changed.
  //
  // Go through all the VCs, and then through all the objects in the
  // VC.  They'll be seen in the same sorted order as the group
  // assocs.  This is because the assocs are sorted by size before the
  // VCs are created.

  NetAssocGroupVec::const_iterator aIter = groupAssocs->begin();

  for (VCVec::const_iterator vcIter = groupVCs->begin(); vcIter != groupVCs->end(); ++vcIter) {
    CarbonValueChangeBase* vc = *vcIter;
    // For each object in the VC, map the assoc to it and its index.
    UInt32 numObjects = vc->numObjects();
    for (UInt32 i = 0; i < numObjects; ++i) {
      INFO_ASSERT(aIter != groupAssocs->end(), "Group assoc iterator is invalid");
      CarbonWaveNetAssocGroup* assoc = *aIter;
      assoc->putVCAndIndex(vc, i);
      ++aIter;
    }
  }
}

WaveScheduleGroup::WaveScheduleGroup(ScheduleStimuli* stim)
  : mStim(stim)
{}

WaveScheduleGroup::~WaveScheduleGroup()
{
  for (VCVecLoop p(mVCObjs);
       ! p.atEnd(); ++p)
  {
    CarbonValueChangeBase* base = *p;
    base->clear();
  }

  for (VCVecLoop p(mGroupVCObjs);
       ! p.atEnd(); ++p)
  {
    CarbonValueChangeBase* base = *p;
    base->clear();
  }

  for (NetAssocGroupVecLoop p(mGroupHandles); ! p.atEnd(); ++p) {
    CarbonWaveNetAssocGroup* group = *p;
    delete group;
  }

}

void WaveScheduleGroup::addHandle(CarbonWaveNetAssoc* assoc)
{
  mHandles.push_back(assoc);
  
#if 0
  // invalid at this point? Already checked for tristates
  {
    NonPodAssoc* nonPodAssoc = new NonPodAssoc(assoc);
    mTriHandles.push_back(nonPodAssoc);
  }
#endif
}

void WaveScheduleGroup::collateNets(MiniMemPool* memPool)
{
  // First, we need to extract the handles that are bitselects of PODs
  sExtractBitselAssocs(&mHandles, &mGroupHandles);

#if SORT_BY_OFFSET
  std::sort(mHandles.begin(), mHandles.end(), sSortByOffset);
#else
  std::sort(mHandles.begin(), mHandles.end(), sSortByNoTriNumBytes);
#endif

  // Sort the group handles, too, using the size of the wrapped net
  std::sort(mGroupHandles.begin(), mGroupHandles.end(), sSortGroupByNoTriNumBytes);

  CarbonExamineScheduler::NetTraitClosure traitClosure;
  sCollateNets(&mHandles, &mGroupHandles, &traitClosure);
  traitClosure.allocVCs(&mVCObjs, &mGroupVCObjs, memPool);
  sSyncGroupAssocsAndVCs(&mGroupHandles, &mGroupVCObjs);
}

void WaveScheduleGroup::processNets(CarbonWaveNetAssoc::NetWriteFlags flags, 
                                    WaveDump* waveFile)
{
  sVCUpdatePod(&mVCObjs, &mHandles, flags, waveFile);
  sVCUpdatePod(&mGroupVCObjs, &mGroupHandles, flags, waveFile);
}
