// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/OnDemandCallback.h"
#include "shell/CarbonNetValueCBData.h"
#include "util/UtArray.h"

OnDemandCallback::OnDemandCallback(CarbonObject *obj, CarbonNetValueCBData *cb, const bool *enabled)
  : mCB(cb),
    mCBFunc(cb->mUserFn),
    mCBData(cb->mUserData),
    mObj(obj),
    mNet(cb->getShellNet()),
    mCalled(false),
    mEnabled(enabled)
{
  // Allocate storage for value/drive
  mNumUInt32 = mNet->getNumUInt32s();
  mSize = mNumUInt32 * sizeof(CarbonUInt32);
  mVal = CARBON_ALLOC_VEC(CarbonUInt32, mNumUInt32);
  mDrive = CARBON_ALLOC_VEC(CarbonUInt32, mNumUInt32);
  memset(mVal, 0, mSize);
  memset(mDrive, 0, mSize);

  // Change the callback function and data to be our own
  cb->mUserFn = sInterceptCB;
  cb->mUserData = this;
}

OnDemandCallback::OnDemandCallback(const OnDemandCallback &other)
  : mCB(other.mCB),
    mCBFunc(other.mCBFunc),
    mCBData(other.mCBData),
    mObj(other.mObj),
    mNet(other.mNet),
    mCalled(other.mCalled),
    mEnabled(other.mEnabled),
    mNumUInt32(other.mNumUInt32),
    mSize(other.mSize)
{
  // Copy other's value/drive
  mVal = CARBON_ALLOC_VEC(CarbonUInt32, mNumUInt32);
  mDrive = CARBON_ALLOC_VEC(CarbonUInt32, mNumUInt32);
  memcpy(mVal, other.mVal, mSize);
  memcpy(mDrive, other.mDrive, mSize);
}

OnDemandCallback::~OnDemandCallback()
{
  CARBON_FREE_VEC(mVal, CarbonUInt32, mNumUInt32);
  CARBON_FREE_VEC(mDrive, CarbonUInt32, mNumUInt32);
}

void OnDemandCallback::sInterceptCB(CarbonObject *, CarbonNet *, CarbonClientData userdata, CarbonUInt32 *val, CarbonUInt32 *drive)
{
  OnDemandCallback *cb = reinterpret_cast<OnDemandCallback*>(userdata);

  // record that this callback was called this schedule call
  cb->setCalled(val, drive);

  // run the original callback with the current data
  cb->run();
}

void OnDemandCallback::reset()
{
  mCalled = false;
}

void OnDemandCallback::run()
{
  // Run the original callback, if it was called this cycle and is enabled
  if (mCalled && *mEnabled)
    (*mCBFunc)(mObj, mNet, mCBData, mVal, mDrive);
}

void OnDemandCallback::setCalled(const CarbonUInt32 *val, const CarbonUInt32 *drive)
{
  mCalled = true;
  memcpy(mVal, val, mSize);
  memcpy(mDrive, drive, mSize);
}

OnDemandCallbackCollection::OnDemandCallbackCollection()
  : mNumCallbacks(0)
{
}

OnDemandCallbackCollection::~OnDemandCallbackCollection()
{
}

void OnDemandCallbackCollection::add(OnDemandCallback *cb)
{
  ++mNumCallbacks;
  mCallbacks.push_back(cb);
}

void OnDemandCallbackCollection::reset()
{
  for (CarbonUInt32 i = 0; i < mNumCallbacks; ++i)
    mCallbacks[i]->reset();
}

void OnDemandCallbackCollection::run()
{
  for (CarbonUInt32 i = 0; i < mNumCallbacks; ++i)
    mCallbacks[i]->run();
}

bool OnDemandCallbackCollection::anyCalled()
{
  for (CarbonUInt32 i = 0; i < mNumCallbacks; ++i)
    if (mCallbacks[i]->wasCalled())
      return true;
  return false;
}

OnDemandCallbackFactory::OnDemandCallbackFactory()
  : mProtectedCollection(0),
    mEnablesUsed(0)
{
}

OnDemandCallbackFactory::~OnDemandCallbackFactory()
{
  clearAll();
}

OnDemandCallback *OnDemandCallbackFactory::createCallback(CarbonObject *obj, CarbonNetValueCBData *cb)
{
  // OnDemand always records callbacks, regardless of their current
  // enabled/disabled status.  This is so that disabled callbacks can
  // be enabled when idle/looking without forcing backoff to be
  // entered.  This means OnDemand must maintain its own enabled flag
  // for each callback, and check it before running the user's
  // registered function.  There needs to be a single copy of this
  // flag for each callback, referenced by each recorded instance.
  bool *enabled = getNewEnablePointer();
  // Callbacks should be enabled by default when created, but don't
  // count on that.
  *enabled = cb->isEnabled();
  cb->enable();
  // Save the flag pointer so we can handle user enable/disable requests
  mEnabledMap[cb] = enabled;

  // The protected callback collection (see below) contains an OnDemandCallback
  // for each callback that was registered.  These callbacks are persistent,
  // as opposed to the OnDemandCallback objects that represent a particular
  // instance of a callback (and which are just copies of the persistent
  // version).  These protected callbacks need to be stored separately.

  OnDemandCallback *ptr = new OnDemandCallback(obj, cb, enabled);
  mProtectedCallbacks.push_back(ptr);
  return ptr;
}

OnDemandCallbackCollection *OnDemandCallbackFactory::createCollection()
{
  // Each onDemand manager only creates one OnDemandCallbackCollection object.
  // It needs to be persistent because it has all the information about
  // the registered callbacks.  This object gets recorded separately
  // so it won't be cleared when idle state tracking is reset.

  INFO_ASSERT(mProtectedCollection == NULL, "Protected callback collection already allocated");
  mProtectedCollection = new OnDemandCallbackCollection;
  return mProtectedCollection;
}

OnDemandCallbackCollection *OnDemandCallbackFactory::copyCollection(const OnDemandCallbackCollection *other)
{
  OnDemandCallbackCollection *ptr = new OnDemandCallbackCollection;
  mCollections.push_back(ptr);

  for (CarbonUInt32 i = 0; i < other->mNumCallbacks; ++i) {
    OnDemandCallback *cb = copyCallback(other->mCallbacks[i]);
    ptr->add(cb);
  }

  return ptr;
}

void OnDemandCallbackFactory::clear()
{
  // Free all the non-protected objects

  for (CarbonUInt32 i = 0; i < mCallbacks.size(); ++i)
    delete mCallbacks[i];
  mCallbacks.clear();
  for (CarbonUInt32 i = 0; i < mCollections.size(); ++i)
    delete mCollections[i];
  mCollections.clear();
}

void OnDemandCallbackFactory::clearAll()
{
  // Free all allocated objects

  clear();
  delete mProtectedCollection;
  mProtectedCollection = NULL;
  for (CarbonUInt32 i = 0; i < mProtectedCallbacks.size(); ++i)
    delete mProtectedCallbacks[i];
  mProtectedCallbacks.clear();
  mEnabledMap.clear();

  // Free the enabled pointer pool
  for (UInt32 i = 0; i < mEnablePool.size(); ++i) {
    bool *pool = mEnablePool[i];
    CARBON_FREE_VEC(pool, bool, scEnablePoolSize);
  }
  mEnablePool.clear();
  mEnablesUsed = 0;
}

void OnDemandCallbackFactory::setEnable(CarbonNetValueCBData *cb, bool enabled)
{
  // Lookup the pointer and set it to the requested value.  All
  // recorded callbacks store a pointer to the enable, so they will
  // see the new value automatically.
  CBEnabledMap::iterator iter = mEnabledMap.find(cb);
  INFO_ASSERT(iter != mEnabledMap.end(), "Callback not found in enable map");
  bool *enPtr = iter->second;
  *enPtr = enabled;
}

OnDemandCallback *OnDemandCallbackFactory::copyCallback(const OnDemandCallback *other)
{
  OnDemandCallback *ptr = new OnDemandCallback(*other);
  mCallbacks.push_back(ptr);
  return ptr;
}

bool *OnDemandCallbackFactory::getNewEnablePointer()
{
  // OnDemand needs core model callbacks to be enabled all the time,
  // so their calls can be recorded.  To allow the user to
  // enable/disable his actual callback function, a separate enabling
  // layer is managed.  Since all recorded instances of a callback
  // need to reference the same enable flag, a single copy is kept in
  // the callback factory.
  //
  // To avoid having to allocate a single bool* every time a callback
  // is registered, we allocate them in pools, adding more as needed.
  if (mEnablePool.empty() || (mEnablesUsed == scEnablePoolSize)) {
    // Allocate a new pool and add it to the array
    bool *pool = CARBON_ALLOC_VEC(bool, scEnablePoolSize);
    mEnablePool.push_back(pool);
    mEnablesUsed = 0;
  }

  // Fetch a pointer from the last-allocated pool, updating the used
  // count.
  bool *pool = mEnablePool.back();
  bool *newPtr = &pool[mEnablesUsed];
  ++mEnablesUsed;
  return newPtr;
}
