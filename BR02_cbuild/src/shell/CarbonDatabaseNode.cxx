// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/CarbonPlatform.h"
#include "CarbonDatabaseNode.h"
#include "CarbonDatabaseIter.h"
#include "CarbonDatabase.h"
#include "symtab/STSymbolTable.h"
#include "shell/CarbonDBRead.h"
#include "shell/ShellData.h"
#include "shell/ShellGlobal.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"
#include "iodb/IODBRuntime.h"
#include "iodb/IODBUserTypes.h"

// Create indexed child
CarbonDatabaseNode::CarbonDatabaseNode(const CarbonDatabaseNode* dbParent, const STSymbolTableNode* stNode,
                                       const StringAtom* name, const UserType* ut, UInt32 dim, SInt32 index)
  : mIsArrayDim(true),
    mIndex(index),
    mDimension(dim),
    mName(name),
    mSymTabNode(stNode),
    mUserType(ut),
    mShellData(NULL),
    mParent(dbParent)
{
}

// Create named child
CarbonDatabaseNode::CarbonDatabaseNode(const CarbonDatabaseNode* dbParent, const STSymbolTableNode* stNode,
                                       const StringAtom* name, const UserType* ut, SInt32 index)
  : mIsArrayDim(false),
    mIndex(index),
    mDimension(0),
    mName(name),
    mSymTabNode(stNode),
    mUserType(ut),
    mShellData(NULL),
    mParent(dbParent)
{
}

CarbonDatabaseNode::~CarbonDatabaseNode()
{
  // The ShellData associated with a CarbonDatabaseNode is allocated
  // during wavedumping.  See
  // CarbonWaveNetAssoc::sGetOrCreateShellData().  There's no factory,
  // so we need to delete it here.
  //
  // This isn't a problem with the ShellData objects associated with
  // STSymbolTableNodes.  When the model is destroyed, the symbol
  // table is walked and all ShellData objects are freed.  See
  // ShellGlobal::DBManager::DBAttribs::~DBAttribs().
  delete mShellData;
}

void CarbonDatabaseNode::print() const
{
  // Print the design symbol table node and, if valid, the StringAtom
  UtString name;
  composeFullName(&name);
  fprintf(stdout, "%s\n", name.c_str());
  mSymTabNode->print();
  fprintf(stdout, "%#x, %#x, %#x, %p, %p, %p\n",
          mIsArrayDim,
          mIndex,
          mDimension, 
          mName,
          mSymTabNode,
          mUserType);
}

void CarbonDatabaseNode::composeFullName(UtString* buf) const
{
  const CarbonDatabaseNode* curr = this;

  // Compose all names to root
  while (curr != NULL) {
    const CarbonDatabaseNode* parent = curr->getParent();
    // Don't add delimiter in front of top node
    curr->prependName(buf, parent != NULL);
    curr = parent;
  }
}

const char* CarbonDatabaseNode::getLeafName() const
{
  const char* name = NULL;
  if (mName != NULL) {
    name = mName->str();
  }
  return name;
}

CarbonDatabaseNode* CarbonDatabaseNode::findChild(const StringAtom* name) const
{
  CarbonDatabaseNode *child = NULL;
  NamedNodeMap::const_iterator iter = mNamedChildren.find(name);
  if (iter != mNamedChildren.end()) {
    child = iter->second;
  }
  return child;
}

CarbonDatabaseNode* CarbonDatabaseNode::findChild(SInt32 index) const
{
  CarbonDatabaseNode *child = NULL;
  IndexedNodeMap::const_iterator iter = mIndexedChildren.find(index);
  if (iter != mIndexedChildren.end()) {
    child = iter->second;
  }
  return child;
}

void CarbonDatabaseNode::addChild(CarbonDatabaseNode* child)
{
  // We can only have one type of children - named or indexed.  Check
  // before we add.
  if (child->isArrayDim()) {
    // Array dimensions are added as indexed children
    ST_ASSERT(mNamedChildren.empty(), mSymTabNode);
    SInt32 key = child->getIndex();
    mIndexedChildren[key] = child;
  } else {
    // Others are added as named children
    ST_ASSERT(mIndexedChildren.empty(), mSymTabNode);
    const StringAtom *key = child->getName();
    mNamedChildren[key] = child;
  }
}

void CarbonDatabaseNode::prependName(UtString* buf, bool includeDelim) const
{
  UtString myName;
  if (mIsArrayDim) {
    // Use our array index with brackets.  Never add delimiter - it's
    // senseless for array dimensions
    myName << '[' << mIndex << ']';
  } else {
    if (includeDelim) {
      myName << '.';
    }
    myName << mName->str();
  }
  // Prepend to the current buffer
  buf->insert(0, myName);
}


CarbonDatabaseNodeFactory::CarbonDatabaseNodeFactory(IODBRuntime* iodb, AtomicCache* atomicCache)
  : mIODB(iodb), mAtomicCache(atomicCache), mRootNode(NULL)
{
}

CarbonDatabaseNodeFactory::~CarbonDatabaseNodeFactory()
{
  for (NodeMap::UnsortedLoop l = mNodeMap.loopUnsorted(); !l.atEnd(); ++l) {
    NodeAttr *attr = l.getKey();
    delete attr;
    CarbonDatabaseNode *node = l.getValue();
    delete node;
  }
}

CarbonDatabaseNode* CarbonDatabaseNodeFactory::addToDB(CarbonDatabaseNode* dbParent, const STSymbolTableNode* stNode, bool isArrayDim, SInt32 arrayIndex)
{
  CarbonDatabaseNode* newNode = NULL;

  // Non-indexed children are keyed using their symtab node's
  // StringAtom.
  const StringAtom* strAtom = stNode->strObject();

  // If this node is a temp, create a user-friendly name for it.
  if (mIODB->isTemp(stNode)) {
    UtString newName;
    ShellSymTabBOM::composeLeafName(stNode, &newName, false);
    // The atomic cache is shared by all model instances, so access
    // needs to be controlled with a mutex.
    ShellGlobal::lockMutex();
    strAtom = mAtomicCache->intern(newName.c_str());
    ShellGlobal::unlockMutex();
  }

  // First, see if the node already exists
  if (dbParent == NULL) {
    // This is a request for the root node, which can't be an array
    // dimension.
    ST_ASSERT(!isArrayDim, stNode);
    // If we have a root node, make sure it's the right one.
    if (mRootNode != NULL) {
      ST_ASSERT(mRootNode->getSymTabNode() == stNode, stNode);
      newNode = mRootNode;
    }
  } else {
    // The parent is valid, so query it.
    if (isArrayDim) {
      // Use the index for array dimensions
      newNode = dbParent->findChild(arrayIndex);
    } else {
      // Use the StringAtom for named children
      newNode = dbParent->findChild(strAtom);
    }
  }

  if (newNode == NULL) {
    // The node doesn't already exist, so we need to add it.
    newNode = allocateNode(dbParent, stNode, strAtom, isArrayDim, arrayIndex);
  }

  return newNode;
}

CarbonDatabaseNode* CarbonDatabaseNodeFactory::allocateNode(CarbonDatabaseNode* dbParent, const STSymbolTableNode* stNode,
                                                            const StringAtom* name, bool isArrayDim, SInt32 arrayIndex)
{
  CarbonDatabaseNode* newNode = NULL;

  // First, build a unique attribute signature for this node.  This is
  // used as a sanity check against allocating duplicates by using it
  // to key the map of allocated nodes.
  NodeAttr* attr = new NodeAttr(dbParent, stNode, arrayIndex);
  NodeMap::iterator iter = mNodeMap.find(attr);
  ST_ASSERT(iter == mNodeMap.end(), stNode);

  // We need a name for this node.  For named children, use the
  // StringAtom from the symtab node.  For indexed children, we
  // need to build a new name by appending our index to the parent
  // node's name.
  if (isArrayDim) {
    UtString newName;
    newName << dbParent->getLeafName() << "[" << arrayIndex << "]";
    // The atomic cache is shared by all model instances, so access
    // needs to be controlled with a mutex.
    ShellGlobal::lockMutex();
    name = mAtomicCache->intern(newName.c_str());
    ShellGlobal::unlockMutex();
  }

  // Determine the user type for the node and allocate it.  Note
  // that for indexed children, we need to choose the user type from
  // the appropriate dimension of the array.  We also need to
  // determine which dimension in the array is represented.
  const UserType* userType;
  if (isArrayDim) {
    // If the parent node represents an array dimension, we're the
    // next one.
    UInt32 arrayDim = 0;
    if (dbParent->isArrayDim()) {
      arrayDim = dbParent->getDimension() + 1;
    }
      
    // Get the user type for this dimension in the array.  Get the
    // first dimension of the parent, which must be an array.
    const UserType* parentType = dbParent->getUserType();
    const UserArray* parentArray = parentType->castArray();
    ST_ASSERT(parentArray != NULL, stNode);
    userType = parentArray->getDimElementType(0);

    // Create the node
    newNode = new CarbonDatabaseNode(dbParent, stNode, name, userType, arrayDim, arrayIndex);
  } else {
    // The user type is always the same as the symtab node
    userType = mIODB->getUserType(stNode);

    // If this is the field of a structure, we need to determine its
    // index within the structure.  This is required when sorting
    // nodes for wave dumping, etc.  For nodes other than structure
    // fields, it doesn't really matter.
    SInt32 index = -1;
    if (dbParent != NULL) {
      const UserType* parentType = dbParent->getUserType();
      if (parentType != NULL) {
        const UserStruct* parentStruct = parentType->castStruct();
        if (parentStruct != NULL) {
          // UserStruct has an array that maps index->name, but not
          // the reverse.  Just walk through all of them.  This only
          // happens when a node is allocated, so it shouldn't be a
          // big deal.
          UInt32 numFields = parentStruct->getNumFields();
          bool found = false;
          for (UInt32 i = 0; !found && (i < numFields); ++i) {
            StringAtom* fieldName = parentStruct->getFieldName(i);
            if (fieldName == name) {
              index = i;
              found = true;
            }
          }
          ST_ASSERT(found, stNode);
        }
      }
    }

    // Create the node
    newNode = new CarbonDatabaseNode(dbParent, stNode, name, userType, index);
  }

  // Now that we have a new node, cache it with the factory.  We've
  // already checked for duplicates.
  mNodeMap[attr] = newNode;

  // Finally, add it as a child of the parent, considering that this
  // might be the root.
  if (dbParent == NULL) {
    ST_ASSERT(mRootNode == NULL, stNode);
    mRootNode = newNode;
  } else {
    dbParent->addChild(newNode);
  }

  return newNode;
}

CarbonDatabaseNodeFactory::NodeAttr::NodeAttr(const CarbonDatabaseNode* dbParent, const STSymbolTableNode* stNode, SInt32 index)
  : mDBParent(dbParent), mSTNode(stNode), mIndex(index)
{
}

CarbonDatabaseNodeFactory::NodeAttr::~NodeAttr()
{
}

size_t CarbonDatabaseNodeFactory::NodeAttr::hash() const
{
  size_t val = reinterpret_cast<size_t>(mDBParent) ^ reinterpret_cast<size_t>(mSTNode) ^ mIndex;
  return val;
}

bool CarbonDatabaseNodeFactory::NodeAttr::equal(const NodeAttr* other) const
{
  bool eq = (mDBParent == other->mDBParent) &&
    (mSTNode == other->mSTNode) &&
    (mIndex == other->mIndex);
  return eq;
}

size_t CarbonDatabaseNodeFactory::MapHelper::hash(const NodeAttr* n) const
{
  return n->hash();
}

bool CarbonDatabaseNodeFactory::MapHelper::equal(const NodeAttr* n1, const NodeAttr* n2) const
{
  return n1->equal(n2);
}


CarbonDatabaseNodeIter::CarbonDatabaseNodeIter()
  : mIndex(0)
{
}

CarbonDatabaseNodeIter::~CarbonDatabaseNodeIter()
{
}

const CarbonDatabaseNode* CarbonDatabaseNodeIter::next()
{
  // If the current index isn't at the end of our node array, return
  // that array element and advance the index.
  const CarbonDatabaseNode* ret = NULL;
  SInt32 numNodes = mNodes.size();
  if (mIndex < numNodes) {
    ret = mNodes[mIndex];
    ++mIndex;
  }
  return ret;
}

void CarbonDatabaseNodeIter::populate(CarbonDatabaseSymtabIter* iter, CarbonDatabase* db)
{
  const STSymbolTableNode* stNode;
  NodeArray tempNodes;
  while ((stNode = iter->next()) != NULL) {
    // Ask the DB to populate all possible DB nodes corresponding to
    // this symtab node, and save them in a temp array.
    tempNodes.clear();
    db->populateAllInstances(stNode, &tempNodes);
    // Since the array elements were looped in a sorted manner, and
    // since the nodes were processed using a stack, the resulting
    // array of nodes is actually sorted in reverse.  We need to undo
    // that before appending to the main array so the user sees them
    // in the correct order.
    std::reverse(tempNodes.begin(), tempNodes.end());
    mNodes.insert(mNodes.end(), tempNodes.begin(), tempNodes.end());
  }
}

CarbonDatabaseNodeChildIter::CarbonDatabaseNodeChildIter(const CarbonDatabaseNode* parent) :
  mParent(parent)
{
}

CarbonDatabaseNodeChildIter::~CarbonDatabaseNodeChildIter()
{
}

static bool sIndexCompareAscending(const CarbonDatabaseNode* n1, const CarbonDatabaseNode* n2)
{
  // This is used to sort three types of nodes:
  //
  // - array elements (in which case the index is the array dimension
  //   index)
  //
  // - structure fields (in which case the index is the order of
  //   declaration within the structure).
  //
  // - others (e.g. nodes within a module scope, in which case the
  //   index of each node is -1)
  //
  // To handle the third case, do a lexical compare if the indices
  // match.
  
  // Return true if n2 > n1, i.e. cmp > 0
  SInt32 cmp = n2->getIndex() - n1->getIndex();
  if (cmp == 0) {
    const StringAtom& s1 = *(n1->getName());
    const StringAtom& s2 = *(n2->getName());
    // We only care if s2 > s1.  If s1 > s2, or they're equal, cmp is
    // already 0 so we'll return false.
    if (s2 > s1) {
      cmp = 1;
    }
  }

  return cmp > 0;
}

static bool sIndexCompareDescending(const CarbonDatabaseNode* n1, const CarbonDatabaseNode* n2)
{
  return n1->getIndex() > n2->getIndex();
}

void CarbonDatabaseNodeChildIter::populate(CarbonDatabaseSymtabIter*, CarbonDatabase*)
{
  if (mParent)
  {
    // Loop over all named or indexed children, as needed
    const CarbonDatabaseNode* child = NULL;
    CarbonDatabaseNode::NamedChildLoop nl(mParent);
    CarbonDatabaseNode::IndexedChildLoop il(mParent);
    // Are we dealing with named or indexed nodes?
    bool indexed = mParent->isIndexed();
    // Loop over the proper set of children
    while ((child = (indexed ? il.next() : nl.next())) != NULL) {
      mNodes.push_back(child);
    }
    // Now sort the list by index.  Sort ascending unless these are
    // array elements and the MSB of the dimension is greater than the
    // LSB.  This ensures array dimensions are iterated from
    // left->right, and structures are iterated in their fields'
    // declaration order.
    bool ascending = true;
    if (indexed) {
      // Only arrays have indexed children
      const UserType* ut = mParent->getUserType();
      const UserArray* ua = ut->castArray();
      ST_ASSERT(ua != NULL, mParent->getSymTabNode());
      const ConstantRange* range = ua->getRange();
      ascending = range->getMsb() < range->getLsb();
    }
    std::sort(mNodes.begin(), mNodes.end(), ascending ? sIndexCompareAscending : sIndexCompareDescending);
  }
}
