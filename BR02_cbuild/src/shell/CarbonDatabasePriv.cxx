// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/CarbonDatabasePriv.h"
#include "CarbonDatabase.h"
#include "CarbonDatabaseNode.h"

const IODBRuntime* CarbonDatabasePriv::getIODB(const CarbonDatabase* db)
{
  return db->getDB();
}

const STSymbolTableNode* CarbonDatabasePriv::getSymTabNode(const CarbonDatabaseNode* dbNode)
{
  return dbNode->getSymTabNode();
}

int CarbonDatabasePriv::getNodeIndex(const CarbonDatabaseNode* dbNode)
{
  return dbNode->getIndex();
}

