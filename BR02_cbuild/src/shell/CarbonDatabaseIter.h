// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef CARBONDATABASEITER_H_
#define CARBONDATABASEITER_H_

#include "iodb/IODB.h"

class CarbonDatabaseSymtabIter
{
public:
  CARBONMEM_OVERRIDES
  
  virtual ~CarbonDatabaseSymtabIter();

  virtual const STSymbolTableNode* next() = 0;
};

class CarbonDatabaseSymtabSetIter : public CarbonDatabaseSymtabIter
{
public:
  CARBONMEM_OVERRIDES

  CarbonDatabaseSymtabSetIter(IODB::NameSetLoop loop);

  virtual const STSymbolTableNode* next();
  
private:
  IODB::NameSetLoop mLoop;
};

class CarbonDatabaseSymtabMapIter : public CarbonDatabaseSymtabIter
{
public:
  CARBONMEM_OVERRIDES

  CarbonDatabaseSymtabMapIter(IODB::NameSetMapLoop loop);

  virtual const STSymbolTableNode* next();
  
private:
  IODB::NameSetMapLoop mLoop;
};

//! Weeds out dual-edged triggers from a loop.
class CarbonDatabaseSymtabSetSingleEdgeIter : public CarbonDatabaseSymtabSetIter
{
public:
  CarbonDatabaseSymtabSetSingleEdgeIter(IODB::NameSetLoop loop, 
                                      const IODB::NameSet& bothEdgeTriggers);
  
  virtual const STSymbolTableNode* next();
  
private:
  const IODB::NameSet& mBothEdgeTriggers;
};

class CarbonDatabaseSymtabVecIter : public CarbonDatabaseSymtabIter
{
public:
  CARBONMEM_OVERRIDES

  CarbonDatabaseSymtabVecIter(IODB::NameVecLoop loop);

  virtual const STSymbolTableNode* next();
  
private:
  IODB::NameVecLoop mLoop;
};

class CarbonDatabaseSymtabBranchIter : public CarbonDatabaseSymtabIter
{
public:
  CARBONMEM_OVERRIDES

  CarbonDatabaseSymtabBranchIter(const STBranchNode* branchNode);

  virtual const STSymbolTableNode* next();
  
private:
  const STBranchNode* mParent;
  SInt32 mIndex;
  typedef UtArray<const STSymbolTableNode*> NodeArray;
  NodeArray mChildren;

  void doSort();
};

class CarbonDatabaseSymtabRootIter : public CarbonDatabaseSymtabIter
{
public:
  CARBONMEM_OVERRIDES

  CarbonDatabaseSymtabRootIter(STSymbolTable::RootIter rootIter)
    : mRootIter(rootIter)
  {
  }

  virtual const STSymbolTableNode* next();
  
private:
  STSymbolTable::RootIter mRootIter;
};

class CarbonDatabaseAliasIter : public CarbonDatabaseSymtabIter
{
public:
  CARBONMEM_OVERRIDES

  CarbonDatabaseAliasIter(STAliasedLeafNode::AliasLoop aliasLoop)
    : mAliasLoop(aliasLoop)
  {
  }

  virtual const STSymbolTableNode* next();
  
private:
  STAliasedLeafNode::AliasLoop mAliasLoop;
};

//! Iterates over nodes matching a string, possibly with wildcards
class CarbonDatabaseSymtabMatchIter : public CarbonDatabaseSymtabIter
{
public:
  CARBONMEM_OVERRIDES

  CarbonDatabaseSymtabMatchIter(const STSymbolTable* symtab, const char* matchString);

  virtual const STSymbolTableNode* next();
  
private:
  typedef UtArray<const STSymbolTableNode*> NodeArray;
  NodeArray mMatches;
  typedef UtArray<const STSymbolTableNode*>::iterator NodeIter;
  NodeIter mIter;
};


#endif
