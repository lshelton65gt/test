// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "util/UtIStream.h"
#include "shell/OnDemandTraceFileReader.h"
#include "shell/OnDemandTraceFileWriter.h"      // for event codes

OnDemandTraceFileReader::OnDemandTraceFileReader(const char *fname,
                                                 ReadTraceDebugCallbackFunc debug_func,
                                                 ReadTraceModeChangeCallbackFunc mode_change_func,
                                                 ReadTraceStaleDataCallbackFunc stale_data_fn,
                                                 void *client_data)
  :mFileName(fname),
   mDebugFunc(debug_func),
   mModeChangeFunc(mode_change_func),
   mStaleDataFunc(stale_data_fn),
   mClientData(client_data),
   mEndOfData(false),
   mReadPosition(0),
   mSimStartTimestamp(0),
   mReadTime(0),
   mReadSize(0)
{
}

OnDemandTraceFileReader::~OnDemandTraceFileReader()
{
}

bool OnDemandTraceFileReader::read()
{
  bool success = true;
  UtString line_buf, signal;

  UtIBStream file(mFileName.c_str());
  if (!file.is_open()) {
    mErrmsg = file.getErrmsg();
    return false;
  }

  file.seek(mReadPosition, UtIO::seek_set);
  UtIStringStream line;

  while (success && file.getline(&line_buf)) {
    // If this line is not complete then ignore it.  We will get it
    // the next time we are called because we will seek back to the
    // appropriate location at a line boundary due to the 'tell'
    // call below.
    UInt32 sz = line_buf.size();
    if ((sz == 0) || (line_buf[sz - 1] != '\n')) {
      return false;
    }

    mReadPosition = file.tell(); // this is cheap cause it's a member var

    // Consider this scenario.  The user starts a simulation, and a 1000-byte
    // COT file is recorded.  Then the user restarts the simulation, and
    // in-between calls to read(), 1200 bytes is written.  We now have 1000 bytes
    // of stale data cached, and no way of telling that it's stale, except that
    // a timestamp will be written to the file which is inconsistent with the
    // one we have cached as mSimStartTimestamp.  When we see the new timestamp
    // we need to clear the client's data (via StaleData callback) and read the
    // original 1000 bytes again.  I don't think it's possible for the writer to
    // put two different timestamps into the file but let's guard against it anyway
    // because that would cause an infinite loop.
    bool seen_timestamp_change = false;


    // strip the newline
    line_buf.resize(sz - 1);
    line.str(line_buf.c_str());

    // Parse the line and call the callback function.
    // See OnDemandTraceFileWriter.cxx, in ModelData::callback 
    // for the code that writes this line.
    char eventType;
    UInt64 schedCalls, simTime, timestamp;
    UInt32 type, action, length;

    success = line >> UtIO::slashify >> eventType;

    bool enterIdle = false;
    if (success) {
      // See if this is a mode change or a debug event
      switch (eventType) {
      case OnDemandTraceFileWriter::scDebugEvent:
        success = line >> schedCalls >> simTime >> type >> action >> length >> signal;
        if (success) {
          mDebugFunc(mClientData, schedCalls, simTime,
                     CarbonOnDemandDebugType(type),
                     CarbonOnDemandDebugAction(action),
                     length,
                     signal.c_str());
        }
        mEndOfData = false;
        break;
      case OnDemandTraceFileWriter::scIdle:
        enterIdle = true;
        // fall through
      case OnDemandTraceFileWriter::scLooking:
      case OnDemandTraceFileWriter::scNotLooking:
        success = line >> schedCalls >> simTime;
        if (success) {
          mModeChangeFunc(mClientData, schedCalls, simTime, enterIdle);
        }
        mEndOfData = false;
      break;
      case OnDemandTraceFileWriter::scEndOfData:
        success = line >> schedCalls >> simTime;
        if (success) {
          // Reuse the mode change function to create a new quantum at
          // the end of time.  This ensures the full timeline of the
          // simulation is displayed.
          mModeChangeFunc(mClientData, schedCalls, simTime, false);
        }
        // The user might leave a trace file up, and re-run the
        // simulation.  So as soon as we see another record we
        // clear the mEndOfData flag.
        mEndOfData = true;
        break;
      case OnDemandTraceFileWriter::scTimestamp:
        success = (line >> timestamp);
        if (success) {
          // first time through, we should see a timestamp
          if (mSimStartTimestamp == 0) {
            mSimStartTimestamp = timestamp;
          }
          else if (mSimStartTimestamp != timestamp) {
            if (seen_timestamp_change) {
              // something is corrupt.
              file.reportError("Inconsinstent COT timestamps in file");
              success = false;
            }
            else {
              seen_timestamp_change = true;
              // If the simulation timestamp has been initialized, and this
              // changes it, then we were previously reading stale trace data.
              // Tell the caller about it via a callback.  We don't need to
              // rewind the file.  All the data before this timestamp is
              // probably bogus anyway.
              staleDataReset();
              mSimStartTimestamp = timestamp;
              file.seek(0, UtIO::seek_set);
            }
          }
        }
        break;
      default:
        file.reportError("unexpected event type");
        success = false;
      }
    }
    if (!success) {
      mErrmsg = line.getErrmsg();
    }
  } // while
  return success;
} // bool OnDemandTraceFileReader::read

bool OnDemandTraceFileReader::checkFileForUpdate() {
  OSStatEntry statEntry;
  UtString errmsg;
  OSStatFileEntry(mFileName.c_str(), &statEntry, &errmsg);

  // note that with unix filetimes measured in seconds, the
  // timestamp may not change between the last checkpoint and
  // the final update, causing us to miss that final call, unless
  // we also check size.  We must also check date because the file
  // might have changed content but not size when it's rewritten
  // completely.  Hopefully that doesn't happen in less than a second.
  if ((statEntry.getModTime() == mReadTime) &&
      (statEntry.getFileSize() == mReadSize))
  {
    return false;
  }

  // If the file size has shrunk, then we must be restarting the simulation,
  // so clear the quanta.
  //
  // This is not 100% robust because the old stale data may have been
  // in a smaller file than what we are looking at now, which is timer-driven.
  //
  // This is why we also have timestamps in the file, so that if we
  // read one and decide that the cached data is invalid we can call
  // staleDataReset() from read() as well.  But it's better to do it here
  // so there's a smaller probability that we will display garbled data,
  // and have to "rewind" from read().
  if (statEntry.getFileSize() < mReadSize) {
    staleDataReset();
    mSimStartTimestamp = 0;
  }

  mReadTime = statEntry.getModTime();
  mReadSize = statEntry.getFileSize();

  return true;
} // bool OnDemandTraceFileReader::checkFileForUpdate

void OnDemandTraceFileReader::staleDataReset() {
  mEndOfData = false;
  mReadPosition = 0;
  mStaleDataFunc(mClientData);
}
