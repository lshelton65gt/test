// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/carbon_dbapi.h"
#include "shell/carbon_misc.h"
#include "shell/ShellGlobal.h"
#include "shell/ShellNet.h"
#include "util/ShellMsgContext.h"
#include "util/UtHashMap.h"
#include "util/OSWrapper.h"
#include "CarbonDatabase.h"
#include "CarbonDatabaseNode.h"
#include "CarbonDatabaseIter.h"

extern const char* gCarbonVersion();

// Keep track of open databases, and share them amongst the clients.
// No point opening potentially huge databases more than once.
// Ref bug15944 and 15907.
// Note: Perhaps these should be consolidated into a single
// map.

struct DBCachedObject
{
  CARBONMEM_OVERRIDES

  CarbonDatabase* mDatabase;
  UInt64 mLastModTimeStamp;
  UInt32 mRefCount;
};

typedef UtHashMap<UtString, DBCachedObject*> CarbonCachedDBMap;

static CarbonCachedDBMap DBCacheMap;

const char* carbonDBGetVersion()
{
  return gCarbonVersion();
}

CarbonMsgCBDataID* carbonDBAddMsgCB(CarbonMsgCB cbFun, 
                                    CarbonClientData userData)
{
  return ShellGlobal::gCarbonAddMsgCB(NULL, cbFun, userData);
}

void carbonDBRemoveMsgCB(CarbonMsgCBDataID **cbDataPtr)
{
  ShellGlobal::gCarbonRemoveMsgCB(NULL, cbDataPtr);
}

// No longer requires a crbn_vsp license, no license checks
// are needed as per Matt Grasse.
CarbonDatabase* carbonDBCreate(const char* fileName, int)
{
  return carbonDBOpenFile(fileName);
}

CarbonDatabase* carbonDBSocVspInit(SInt32 key, const char* fileName, int waitForLicense)
{
  CarbonDatabaseStandalone* dbObj = new CarbonDatabaseStandalone;
  if (! dbObj->openSocVsp(key, fileName, waitForLicense))
  {
    delete dbObj;
    dbObj = NULL;
  }
  return dbObj;
}

CarbonDatabase* carbonDBPlatArchInit(SInt32 key, const char* fileName, int waitForLicense)
{
  CarbonDatabaseStandalone* dbObj = new CarbonDatabaseStandalone;
  if (! dbObj->openPlatArch(key, fileName, waitForLicense))
  {
    delete dbObj;
    dbObj = NULL;
  }
  return dbObj;
}

CarbonDatabase* carbonDBOpenFile(const char* inputFileName)
{
  // The maps are shared by all model instances, so access needs to be
  // controlled with a mutex.
  ShellGlobal::lockDatabaseMutex();

  UtString errmsg;
  OSStatEntry statEntry;
  OSStatFileEntry(inputFileName, &statEntry, &errmsg);
  UtString absoluteFileName;
  OSGetRealPath(inputFileName, &absoluteFileName);
  const char* fileName = absoluteFileName.c_str();
  CarbonDatabase* loadedDb = NULL;
  DBCachedObject* cachedDB = NULL;

  bool mapHit = false;
  
  if (statEntry.exists())
  {     
    cachedDB = DBCacheMap[fileName];
    if (cachedDB)
    {
      UInt64 lastModTime = cachedDB->mLastModTimeStamp;
      if (lastModTime ==  statEntry.getModTime())
      {
        loadedDb = cachedDB->mDatabase;
        mapHit = true;
        cachedDB->mRefCount++;      
      }
    }  
  }

  if (mapHit)
  {
    ShellGlobal::unlockDatabaseMutex();
    return loadedDb;
  }

  CarbonDatabaseStandalone* dbObj = new CarbonDatabaseStandalone;
  if (! dbObj->openDBFile(fileName))
  {
    delete dbObj;
    dbObj = NULL;
  }

  // Insert the cached entry here, assuming its valid
  if (dbObj)
  {
    cachedDB = new DBCachedObject();  
    cachedDB->mRefCount = 1;
    cachedDB->mLastModTimeStamp = statEntry.getModTime();
    cachedDB->mDatabase = dbObj;
    DBCacheMap[fileName] = cachedDB; 
  }

  ShellGlobal::unlockDatabaseMutex();

  return dbObj;
}

CarbonDatabaseNodeIter* carbonDBLoopPrimaryPorts(CarbonDatabase* dbContext)
{
  return dbContext->loopPrimaryPorts();
}

CarbonDatabaseNodeIter* carbonDBLoopPrimaryInputs(CarbonDatabase* dbContext)
{
  return dbContext->loopPrimaryInputs();
}

CarbonDatabaseNodeIter* carbonDBLoopPrimaryOutputs(CarbonDatabase* dbContext)
{
  return dbContext->loopPrimaryOutputs();
}

CarbonDatabaseNodeIter* carbonDBLoopPrimaryBidis(CarbonDatabase* dbContext)
{
  return dbContext->loopPrimaryBidis();
}

CarbonDatabaseNodeIter* carbonDBLoopPrimaryClks(CarbonDatabase* dbContext)
{
  return dbContext->loopPrimaryClks();
}

CarbonDatabaseNodeIter* carbonDBLoopClkTree(CarbonDatabase* dbContext)
{
  return dbContext->loopClkTree();
}

CarbonDatabaseNodeIter* carbonDBLoopAsyncs(CarbonDatabase* dbContext)
{
  return dbContext->loopAsyncs();
}

CarbonDatabaseNodeIter* carbonDBLoopAsyncOutputs(CarbonDatabase* dbContext)
{
  return dbContext->loopAsyncOutputs();
}

CarbonDatabaseNodeIter* carbonDBLoopAsyncFanin(CarbonDatabase* dbContext, const CarbonDatabaseNode* output)
{
  return dbContext->loopAsyncFanin(output);
}

CarbonDatabaseNodeIter* carbonDBLoopAsyncDeposits(CarbonDatabase* dbContext)
{
  return dbContext->loopAsyncDeposits();
}

CarbonDatabaseNodeIter* carbonDBLoopAsyncPosResets(CarbonDatabase* dbContext)
{
  return dbContext->loopAsyncPosResets();
}

CarbonDatabaseNodeIter* carbonDBLoopAsyncNegResets(CarbonDatabase* dbContext)
{
  return dbContext->loopAsyncNegResets();
}

CarbonDatabaseNodeIter* carbonDBLoopDepositable(CarbonDatabase* dbContext)
{
  return dbContext->loopDepositable();
}

CarbonDatabaseNodeIter* carbonDBLoopScDepositable(CarbonDatabase* dbContext)
{
  return dbContext->loopScDepositable();
}

CarbonDatabaseNodeIter* carbonDBLoopObservable(CarbonDatabase* dbContext)
{
  return dbContext->loopObservable();
}

CarbonDatabaseNodeIter* carbonDBLoopScObservable(CarbonDatabase* dbContext)
{
  return dbContext->loopScObservable();
}

const CarbonDBNode* carbonDBNodeIterNext(CarbonDatabaseNodeIter* nodeIterObj)
{
  return nodeIterObj->next();
}

void carbonDBFreeNodeIter(CarbonDatabaseNodeIter* dbNodeIter)
{
  delete dbNodeIter;
}

void carbonDBFree(CarbonDatabase* dbObj)
{
  ShellGlobal::lockDatabaseMutex();
  for (LoopMap<CarbonCachedDBMap> p(DBCacheMap); !p.atEnd(); ++p) 
  {
    DBCachedObject* cachedDB = p.getValue();
    UtString dbFileName = p.getKey();
    CarbonDatabase* db = cachedDB->mDatabase;
    // If no more references to this DB, remove it from the maps
    // and free it.
    if (db == dbObj)
    {
      cachedDB->mRefCount--;     
      if (cachedDB->mRefCount < 1)
      {
        DBCacheMap.erase(dbFileName);       
        // If this is a runtime database, the model owns it, so don't free it.
        if (dbObj && !dbObj->castRuntime()) {
          delete dbObj;
        }
      }
      break;
    }
  }
  ShellGlobal::unlockDatabaseMutex();
}


const char* carbonDBGetInterfaceName(CarbonDB* dbContext)
{
  return dbContext->getInterfaceName();
}

const char* carbonDBGetTopLevelModuleName(CarbonDB* dbContext)
{
  return dbContext->getTopLevelModuleName();
}

const char* carbonDBGetSystemCModuleName(CarbonDB* dbContext)
{
  return dbContext->getSystemCModuleName();
}

const char* carbonDBNodeGetFullName(CarbonDatabase* dbContext, const CarbonDatabaseNode* node)
{
  return dbContext->getFullName(node);
}

const char* carbonDBNodeGetLeafName(CarbonDatabase* dbContext, const CarbonDatabaseNode* node)
{
  return dbContext->getLeafName(node);
}

int carbonDBIsPrimaryInput(const CarbonDatabase* dbContext, 
                           const CarbonDatabaseNode* node)
{
  return dbContext->isPrimaryInput(node);
}

int carbonDBIsPrimaryOutput(const CarbonDatabase* dbContext, 
                            const CarbonDatabaseNode* node)
{
  return dbContext->isPrimaryOutput(node);
}

int carbonDBIsPrimaryBidi(const CarbonDatabase* dbContext, 
                          const CarbonDatabaseNode* node)
{
  return dbContext->isPrimaryBidi(node);
}

int carbonDBIsInput(const CarbonDatabase* dbContext, 
                           const CarbonDatabaseNode* node)
{
  return dbContext->isInput(node);
}

int carbonDBIsOutput(const CarbonDatabase* dbContext, 
                            const CarbonDatabaseNode* node)
{
  return dbContext->isOutput(node);
}

int carbonDBIsBidi(const CarbonDatabase* dbContext, 
                   const CarbonDatabaseNode* node)
{
  return dbContext->isBidi(node);
}

int carbonDBIs2DArray(const CarbonDatabase* dbContext, 
                     const CarbonDatabaseNode* node)
{
  return dbContext->is2DArray(node);
}

int carbonDBIsVector(const CarbonDatabase* dbContext, 
                     const CarbonDatabaseNode* node)
{
  return dbContext->isVector(node);
}

int carbonDBIsScalar(const CarbonDatabase* dbContext, 
                     const CarbonDatabaseNode* node)
{
  return dbContext->isScalar(node);
}

int carbonDBIsTristate(const CarbonDatabase* dbContext, 
                       const CarbonDatabaseNode* node)
{
  return dbContext->isTristate(node);
}

int carbonDBIsConstant(const CarbonDatabase* dbContext, 
                       const CarbonDatabaseNode* node)
{
  return dbContext->isConstant(node);
}

int carbonDBIsClk(const CarbonDatabase* dbContext, 
                    const CarbonDatabaseNode* node)
{
  return dbContext->isClk(node);
}

int carbonDBIsClkTree(const CarbonDatabase* dbContext, 
                      const CarbonDatabaseNode* node)
{
  return dbContext->isClkTree(node);
}

int carbonDBIsAsync(const CarbonDatabase* dbContext, 
                    const CarbonDatabaseNode* node)
{
  return dbContext->isAsync(node);
}

int carbonDBIsAsyncOutput(const CarbonDatabase* dbContext, 
                          const CarbonDatabaseNode* node)
{
  return dbContext->isAsyncOutput(node);
}

int carbonDBIsAsyncDeposit(const CarbonDatabase* dbContext, 
                          const CarbonDatabaseNode* node)
{
  return dbContext->isAsyncDeposit(node);
}

int carbonDBIsAsyncPosReset(const CarbonDatabase* dbContext, 
                            const CarbonDatabaseNode* node)
{
  return dbContext->isAsyncPosReset(node);
}

int carbonDBIsAsyncNegReset(const CarbonDatabase* dbContext, 
                            const CarbonDatabaseNode* node)
{
  return dbContext->isAsyncNegReset(node);
}

int carbonDBGetWidth(const CarbonDatabase* dbContext, 
                     const CarbonDatabaseNode* node)
{
  return dbContext->getWidth(node);
}
  
int carbonDBGetMSB(const CarbonDatabase* dbContext, 
                   const CarbonDatabaseNode* node)
{
  return dbContext->getMSB(node);
}

int carbonDBGetLSB(const CarbonDatabase* dbContext, 
                   const CarbonDatabaseNode* node)
{
  return dbContext->getLSB(node);
}

SInt32 carbonDBGet2DArrayLeftAddr(const CarbonDatabase* dbContext, 
                                 const CarbonDatabaseNode* node)
{
  return dbContext->get2DArrayLeftAddr(node);
}
  
SInt32 carbonDBGet2DArrayRightAddr(const CarbonDatabase* dbContext, 
                                   const CarbonDatabaseNode* node)
{
  return dbContext->get2DArrayRightAddr(node);
}

const CarbonDBNode* carbonDBFindNode(CarbonDatabase* dbContext, 
                                     const char* pathname)
{
  // We never warned before if a node wasn't found
  return dbContext->findNode(pathname, false);
}

/*!
  \brief Find a DB node's child, return NULL if on failure
*/
const CarbonDBNode* carbonDBFindChild(CarbonDB* dbContext,
                                      const CarbonDBNode* parent,
                                      const char* childName)
{
  return dbContext->findChild(parent, childName);
}

const CarbonDBNode* carbonDBNodeGetParent(const CarbonDatabase* dbContext, 
                                          const CarbonDatabaseNode* node)
{
  return dbContext->getParent(node);
}

CarbonDBNodeIter* carbonDBLoopDesignRoots(CarbonDB* dbContext) {
  return dbContext->loopDesignRoots();
}

CarbonDBNodeIter* carbonDBLoopAliases(CarbonDB* dbContext, 
                                      const CarbonDatabaseNode* node)
{
  return dbContext->loopAliases(node);
}

CarbonDBNodeIter* carbonDBLoopChildren(CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->loopChildren(node);
}

CarbonDBNodeIter* carbonDBLoopMatching(CarbonDB* dbContext, const char* matchString)
{
  return dbContext->loopMatching(matchString);
}

int carbonDBIsForcible(const CarbonDB* dbContext,
                       const CarbonDatabaseNode* node)
{
  return dbContext->isForcible(node);
}

int carbonDBIsDepositable(const CarbonDB* dbContext,
                          const CarbonDatabaseNode* node)
{
  return dbContext->isDepositable(node);
}

int carbonDBIsScDepositable(const CarbonDB* dbContext,
                          const CarbonDatabaseNode* node)
{
  return dbContext->isScDepositable(node);
}

int carbonDBIsObservable(const CarbonDB* dbContext,
                         const CarbonDatabaseNode* node)
{
  return dbContext->isObservable(node);
}

int carbonDBIsScObservable(const CarbonDB* dbContext,
                         const CarbonDatabaseNode* node)
{
  return dbContext->isScObservable(node);
}

int carbonDBIsVisible(const CarbonDB* dbContext,
                      const CarbonDatabaseNode* node)
{
  return dbContext->isVisible(node);
}

int carbonDBIsTied(const CarbonDB* dbContext,
                   const CarbonDatabaseNode* node)
{
  return dbContext->isTied(node);
}

const char* carbonDBIntrinsicType(const CarbonDB* dbContext,
                  const CarbonDatabaseNode* node)
{
  return dbContext->intrinsicType(node);
}

const char* carbonDBdeclarationType(const CarbonDB* dbContext,
                                    const CarbonDatabaseNode* node)
{
  return dbContext->declarationType(node);
}


const char* carbonDBtypeLibraryName(const CarbonDB* dbContext,
                                    const CarbonDatabaseNode* node)
{
  return dbContext->typeLibraryName(node);
}

const char* carbonDBtypePackageName(const CarbonDB* dbContext,
                                    const CarbonDatabaseNode* node)
{
  return dbContext->typePackageName(node);
}


const char* carbonDBComponentName(const CarbonDB* dbContext,
                                  const CarbonDatabaseNode* node)
{
  return dbContext->componentName(node);
}

const char* carbonDBSourceLanguage(const CarbonDB* dbContext,
                                   const CarbonDatabaseNode* node)
{
  return dbContext->sourceLanguage(node);
}

const char* carbonDBGetPullMode(const CarbonDB* dbContext,
                                const CarbonDatabaseNode* node)
{
  return dbContext->getPullMode(node);
}

const char* carbonDBGetType(const CarbonDB* dbContext)
{
  return dbContext->getType();
}

int carbonDBIsTemp(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->isTemp(node);
}

int carbonDBIsReplayable(const CarbonDB* dbContext)
{
  return dbContext->isReplayable();
}

int carbonDBSupportsOnDemand(const CarbonDB* dbContext)
{
  return dbContext->supportsOnDemand();
}

int carbonDBIsPosedgeTrigger(const CarbonDB* dbContext,
                             const CarbonDBNode* node)
{
  return dbContext->isPosedgeTrigger(node);
}

int carbonDBIsNegedgeTrigger(const CarbonDB* dbContext,
                             const CarbonDBNode* node)
{
  return dbContext->isNegedgeTrigger(node);
}

int carbonDBIsBothEdgeTrigger(const CarbonDB* dbContext,
                              const CarbonDBNode* node)
{
  return dbContext->isBothEdgeTrigger(node);
}

int carbonDBIsTriggerUsedAsData(const CarbonDB* dbContext,
                                const CarbonDBNode* node)
{
  return dbContext->isTriggerUsedAsData(node);
}

int carbonDBIsLiveInput(const CarbonDB* dbContext,
                        const CarbonDBNode* node)
{
  return dbContext->isLiveInput(node);
}

int carbonDBIsLiveOutput(const CarbonDB* dbContext,
                         const CarbonDBNode* node)
{
  return dbContext->isLiveOutput(node);
}

int carbonDBIsOnDemandIdleDeposit(const CarbonDB* dbContext,
                               const CarbonDBNode* node)
{
  return dbContext->isOnDemandIdleDeposit(node);
}

int carbonDBIsOnDemandExcluded(const CarbonDB* dbContext,
                               const CarbonDBNode* node)
{
  return dbContext->isOnDemandExcluded(node);
}

CarbonDBNodeIter* carbonDBLoopPosedgeOnlyTriggers(CarbonDB* dbContext)
{
  return dbContext->loopPosedgeOnlyTriggers();
}

CarbonDBNodeIter* carbonDBLoopNegedgeOnlyTriggers(CarbonDB* dbContext)
{
  return dbContext->loopNegedgeOnlyTriggers();
}

CarbonDBNodeIter* carbonDBLoopPosedgeTriggers(CarbonDB* dbContext)
{
  return dbContext->loopPosedgeTriggers();
}

CarbonDBNodeIter* carbonDBLoopNegedgeTriggers(CarbonDB* dbContext)
{
  return dbContext->loopNegedgeTriggers();
}

CarbonDBNodeIter* carbonDBLoopBothEdgeTriggers(CarbonDB* dbContext)
{
  return dbContext->loopBothEdgeTriggers();
}

CarbonDBNodeIter* carbonDBLoopTriggersUsedAsData(CarbonDB* dbContext)
{
  return dbContext->loopTriggersUsedAsData();
}

const char* carbonDBGetSoftwareVersion(CarbonDB* dbContext)
{
  return dbContext->getSoftwareVersion();
}

const char* carbonDBGetIdString(CarbonDB* dbContext)
{
  return dbContext->getIdString();
}

int carbonDBIsStruct(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->isStruct(node);
}

int carbonDBIsArray(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->isArray(node);
}

int carbonDBIsEnum(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->isEnum(node);
}

int carbonDBGetNumberElemInEnum(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->getNumberElemInEnum(node);
}

const char* carbonDBGetEnumElem(const CarbonDB* dbContext, const CarbonDBNode* node, int index)
{
  return dbContext->getEnumElem(node, index);
}

int carbonDBGetNumStructFields(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->getNumStructFields(node);
}

const CarbonDBNode* carbonDBGetStructFieldByName(CarbonDB* dbContext, const CarbonDBNode* node, const char* name)
{
  return dbContext->getStructFieldByName(node, name);
}

int carbonDBGetArrayLeftBound(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->getArrayLeftBound(node);
}

int carbonDBGetArrayDimLeftBound(const CarbonDB* dbContext, const CarbonDBNode* node, int dim)
{
  return dbContext->getArrayDimLeftBound(node, dim);
}
int carbonDBGetArrayDimRightBound(const CarbonDB* dbContext, const CarbonDBNode* node, int dim)
{
  return dbContext->getArrayDimRightBound(node, dim);
}

int carbonDBGetArrayNumDeclaredDims(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->getArrayNumDeclaredDims(node);
}

int carbonDBGetArrayRightBound(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->getArrayRightBound(node);
}

int carbonDBGetArrayDims(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->getArrayDims(node);
}

const CarbonDBNode* carbonDBGetArrayElement(CarbonDB* dbContext, const CarbonDBNode* node, const int* indices, int dims)
{
  return dbContext->getArrayElement(node, indices, dims);
}

CarbonNetID* carbonDBGetCarbonNet(CarbonDB* dbContext, const CarbonDBNode* node)
{
  // This is only valid with a runtime database
  CarbonDatabaseRuntime *runtimeDB = dbContext->castRuntime();
  if (runtimeDB == NULL) {
    ShellGlobal::gCarbonGetMessageContext(NULL)->SHLDBRuntimeOnly("getCarbonNet");
    return NULL;
  }

  return runtimeDB->getCarbonNet(node, false, false);
}

int carbonDBCanBeCarbonNet(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->canBeCarbonNet(node);
}

int carbonDBGetBitSize(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->getBitSize(node);
}

int carbonDBGetRangeConstraintLeftBound(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->getRangeConstraintLeftBound(node);
}

int carbonDBGetRangeConstraintRightBound(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->getRangeConstraintRightBound(node);
}

int carbonDBIsRangeRequiredInDeclaration(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->isRangeRequiredInDeclaration(node);
}



const char* carbonDBGetSourceFile(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->getSourceFile(node);
}

int carbonDBGetSourceLine(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  return dbContext->getSourceLine(node);
}

int carbonDBIsContainedByComposite(const CarbonDB* dbContext, const CarbonDBNode* node)
{
  // Composites (arrays and structs) can only contain other arrays,
  // structs, or scalars - no module instances, named scopes, etc.
  // That means it's sufficient to check whether the immediate parent
  // is a composite.

  int ret = 0;
  const CarbonDBNode* parent = dbContext->getParent(node);
  if ((dbContext->isArray(parent)) || (dbContext->isStruct(parent))) {
    ret = 1;
  }
  return ret;
}

const char* carbonDBGetStringAttribute(const CarbonDB* dbContext, const char* attributeName)
{
  return dbContext->getStringAttribute(attributeName);
}

int carbonDBGetIntAttribute(const CarbonDB* dbContext, const char* attributeName, CarbonUInt32* attributeValue)
{
  return dbContext->getIntAttribute(attributeName, attributeValue);
}
