// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/CarbonExprNet.h"
#include "shell/CarbonNetIdent.h"
#include "shell/CarbonMemIdent.h"
#include "shell/CarbonMemWordIdent.h"
#include "shell/ShellGlobal.h"
#include "shell/CarbonVectorBase.h"
#include "iodb/IODBTypes.h"
#include "util/UtConv.h"
#include "symtab/STSymbolTable.h"
#include "shell/CarbonNetValueCBData.h"

//! Expression walk for conflict detection
class CarbonExprNet::ConflictDetectWalk : public CarbonExprWalker
{
  //! enum for determining how to visit idents
  enum IdentContext {
    eFull, //!< visiting full ident
    ePartial //!< visiting partial ident
  };

public: CARBONMEM_OVERRIDES
  //! Constructor
  ConflictDetectWalk() : mHasConflict(false), mIdentContext(eFull)
  {}
  
  //! virtual destructor
  virtual ~ConflictDetectWalk() {}

  //! Stop traversal if a conflict has been detected
  virtual bool preVisitPartsel(CarbonPartsel*) { 
    bool keepTraversing = ! hasConflict(); 
    if (keepTraversing)
      mIdentContext = ePartial;
    return keepTraversing;
  }

  //! Stop traversal if a conflict has been detected
  virtual bool preVisitUnaryOp(CarbonUnaryOp*) { return ! hasConflict(); }
  //! Stop traversal if a conflict has been detected
  virtual bool preVisitBinaryOp(CarbonBinaryOp* binOp) { 
    bool keepTraversing = ! hasConflict(); 
    if (keepTraversing && 
        binOp->getType() == CarbonExpr::eBiBitSel)
    {
      CarbonExpr* bitSelIndex = binOp->getArg(1);
      CE_ASSERT(bitSelIndex->castConst() != NULL, bitSelIndex);
      CE_ASSERT(bitSelIndex->castConstXZ() == NULL, bitSelIndex);
      
      mIdentContext = ePartial;
    }
    return keepTraversing;
  }
  //! Stop traversal if a conflict has been detected
  virtual bool preVisitTernaryOp(CarbonTernaryOp*) { return ! hasConflict(); }
  //! Stop traversal if a conflict has been detected
  virtual bool preVisitNaryOp(CarbonNaryOp*) { return ! hasConflict(); }
  //! Stop traversal if a conflict has been detected
  virtual bool preVisitConcatOp(CarbonConcatOp*) { return ! hasConflict(); }

  //! This will look at partial idents
  virtual void visitPartsel(CarbonPartsel* partsel)
  {
    CarbonExpr* partExpr = partsel->getArg(0);
    CarbonIdent* partIdent = partExpr->castIdent();
    CE_ASSERT(partIdent, partExpr);
    ConstantRange partRange = partsel->getRange();
    ConstantRange declaredRange;
    CE_ASSERT(partIdent->getDeclaredRange(&declaredRange), partIdent);
    
    if (partRange.isFlipped(declaredRange))
      partRange.switchSBs();
    partRange.denormalize(&declaredRange);
    
    const CarbonNetIdent* netIdent = (const CarbonNetIdent*)(partIdent);

    const ShellNet* shlNet = netIdent->getShellNet();
    mHasConflict = shlNet->hasDriveConflictRange(partRange.getMsb(), partRange.getLsb());
    mIdentContext = eFull;
  }

  //! This will look at partial idents
  virtual void visitBinaryOp(CarbonBinaryOp* binaryOp)
  {
    if (binaryOp->getType() == CarbonExpr::eBiBitSel)
    {
      CE_ASSERT(mIdentContext == ePartial, binaryOp);
      CarbonConst* bitSelIndex = binaryOp->getArg(1)->castConst();
      SInt32 bitIndex;
      bitSelIndex->getL(&bitIndex);
      CE_ASSERT(bitIndex >= 0, binaryOp);
      

      CarbonExpr* identExpr = binaryOp->getArg(0);
      CarbonIdent* ident = identExpr->castIdent();
      CE_ASSERT(ident, identExpr);
      ConstantRange declaredRange;
      CE_ASSERT(ident->getDeclaredRange(&declaredRange), ident);
      bitIndex = declaredRange.index(bitIndex);

      const CarbonNetIdent* netIdent = (const CarbonNetIdent*)(ident);

      const ShellNet* shlNet = netIdent->getShellNet();
      mHasConflict = shlNet->hasDriveConflictRange(bitIndex, bitIndex);
      mIdentContext = eFull;
    }
  }

  inline bool hasConflict() const {
    return mHasConflict;
  }

private:
  bool mHasConflict;
  IdentContext mIdentContext;
};

//! Class for calling resolveXDrive on expression identifiers
class CarbonExprNet::ResolveXDrive : public CarbonExprWalker
{
public: 
  CARBONMEM_OVERRIDES

  //! Constructor
  ResolveXDrive(CarbonModel* model) 
    : mModel(model), mChanged(false)
  {}
  
  //! virtual destructor
  virtual ~ResolveXDrive() {}

  //! Run resolveXDrive on the shellent.
  virtual void visitIdent(CarbonIdent* ident) 
  { 
    CarbonNetIdent* me = ident->castCarbonNetIdent();
    if (me != NULL) {
      ShellNet* net = me->getShellNet();
      mChanged |= net->resolveXdrive(mModel);
    } else {
      // If this isn't a net ident, it's a memory or memory word
      // ident.  Memories can't be tristates, so no resolution is
      // needed.
    }
  }
  
  bool isChanged() const { return mChanged; }

private:
  CarbonModel* mModel;
  bool mChanged;
};

class CarbonExprNet::SubNetGatherWalk : public CarbonExprWalker
{
public:
  CARBONMEM_OVERRIDES

  SubNetGatherWalk(ShellNetWrapper1ToN::NetVec* netVec) 
    : mNetVec(netVec)
  {
    mNetVec->clear();
  }

  virtual ~SubNetGatherWalk()
  {}

  virtual void visitIdent(CarbonIdent* ident) 
  { 
    if (mCovered.insertWithCheck(ident))
    {
      // This needs to be either a net or mem word ident
      CarbonNetIdent *netIdent = ident->castCarbonNetIdent();
      CarbonMemWordIdent *memWordIdent = ident->castCarbonMemWordIdent();
      CE_ASSERT(netIdent || memWordIdent, ident);
      ShellNet *net;
      if (netIdent != NULL) {
        net = netIdent->getShellNet();
      } else {
        // Use the net for the underlying mem ident
        CarbonMemIdent *memIdent = memWordIdent->getMemIdent();
        net = memIdent->getShellNet();
      }
      mNetVec->push_back(net);
    }
  }
  
private:
  ShellNetWrapper1ToN::NetVec* mNetVec;
  typedef UtHashSet<CarbonIdent*> IdentSet;
  IdentSet mCovered;
};


class CarbonExprNet::SubNetReplaceWalk : public CarbonExprWalker
{
public:
  CARBONMEM_OVERRIDES

  SubNetReplaceWalk(const ShellNetWrapper1ToN::NetVec& netVec) 
    : mNetVec(&netVec), mVecIndex(0)
  {}
  
  virtual ~SubNetReplaceWalk()
  {}

  virtual void visitIdent(CarbonIdent* ident) 
  { 
    if (mCovered.insertWithCheck(ident))
    {
      // This needs to be either a net or mem word ident
      CarbonNetIdent *netIdent = ident->castCarbonNetIdent();
      CarbonMemWordIdent *memWordIdent = ident->castCarbonMemWordIdent();
      CE_ASSERT(netIdent || memWordIdent, ident);
      ShellNet* replacement = (*mNetVec)[mVecIndex];
      ++mVecIndex;
      if (netIdent != NULL) {
        netIdent->putShellNet(replacement);
      } else {
        // Use the net for the underlying mem ident
        CarbonMemIdent *memIdent = memWordIdent->getMemIdent();
        memIdent->putShellNet(replacement);
      }
    }
  }
  
private:
  const ShellNetWrapper1ToN::NetVec* mNetVec;
  typedef UtHashSet<CarbonIdent*> IdentSet;
  IdentSet mCovered;
  UInt32 mVecIndex;
};

CarbonExprNet::CarbonExprNet(CarbonExpr* expr, const IODBIntrinsic* range) :
  mExpr(expr), mIntrinsic(range), mControlMask(NULL), mAttribFlags(eNone)
{
  // Don't handle memories as expressions yet
  CE_ASSERT(mIntrinsic->getType() != IODBIntrinsic::eMemory, expr);
}

CarbonExprNet::~CarbonExprNet()
{}

void CarbonExprNet::putIsTristate(bool isTristate)
{
  if (isTristate)
    mAttribFlags = NetAttributes(mAttribFlags | eTristate);
  else
    mAttribFlags = NetAttributes(mAttribFlags & ~eTristate);
}

void CarbonExprNet::putIsForcible(bool isForcible)
{
  if (isForcible)
    mAttribFlags = NetAttributes(mAttribFlags | eForcible);
  else
    mAttribFlags = NetAttributes(mAttribFlags & ~eForcible);
}

void CarbonExprNet::putIsInput(bool isInput)
{
  if (isInput)
    mAttribFlags = NetAttributes(mAttribFlags | eInput);
  else
    mAttribFlags = NetAttributes(mAttribFlags & ~eInput);
}

void CarbonExprNet::getTraits(Traits* traits) const
{
  if (isInput())
    traits->mHasInputSemantics = true;
  if (isTristate())
    traits->mIsTristate = true;
  // We can't produce a simple pointer to a storage location
  // representing this net's value.
  traits->mHasComplexStorage = true;
  traits->mWidth = mIntrinsic->getWidth();
}

bool CarbonExprNet::isForcible() const
{
  return ((mAttribFlags & eForcible) != 0);
}

bool CarbonExprNet::isVector() const
{
  return mIntrinsic->getType() == IODBIntrinsic::eVector;
}

bool CarbonExprNet::isScalar() const
{
  return mIntrinsic->getType() == IODBIntrinsic::eScalar;
}

bool CarbonExprNet::isTristate() const
{
  return ((mAttribFlags & eTristate) != 0);
}

bool CarbonExprNet::isInput() const
{
  return ((mAttribFlags & eInput) != 0);
}

const IODBIntrinsic* CarbonExprNet::getIntrinsic() const
{
  // Use our stored intrinsic, not the one associated with the storage node
  return mIntrinsic;
}

CarbonStatus CarbonExprNet::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  if (buf || drive)
  {
    int numWords = getNumUInt32s();
    int bitWidth = getBitWidth();
    CarbonNetIdent::EvalContext 
      evalContext(CarbonNetIdent::EvalContext::eExamine);
    
    switch (mode)
    {
    case eIDrive:
      break;
    case eCalcDrive:
      evalContext.setMode(CarbonNetIdent::EvalContext::eDriveCalc);
      break;
    case eXDrive:
      evalContext.setMode(CarbonNetIdent::EvalContext::eXDrive);
      break;
    }

    evalContext.setBitWidth(bitWidth);
    DynBitVector& valBuf = evalContext.getValueRef();
    DynBitVector& drvBuf = evalContext.getDriveRef();
    
    CarbonExpr::SignT evalStat = mExpr->evaluate(&evalContext);
    CE_ASSERT(evalStat != CarbonExpr::eBadSign, mExpr);

    if (buf)
    {
      const UInt32* valArr = valBuf.getUIntArray();
      CarbonValRW::cpSrcToDest(buf, valArr, numWords);
    }
    if (drive)
    {
      const UInt32* drvArr = drvBuf.getUIntArray();
      CarbonValRW::cpSrcToDest(drive, drvArr, numWords);
    }
  }

  return eCarbon_OK;
}

CarbonStatus CarbonExprNet::examineWord (UInt32* buf, int index, 
                                         UInt32* drive,
                                         ExamineMode mode, 
                                         CarbonModel* model) const
{
  size_t numWords = getNumUInt32s();
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
  if (stat == eCarbon_OK)
  {
    if (buf || drive)
    {
      int bitWidth = getBitWidth();
      DynBitVector valBuf(bitWidth);
      DynBitVector drvBuf(bitWidth);
      UInt32* valArr = valBuf.getUIntArray();
      UInt32* drvArr = drvBuf.getUIntArray();
      examine(valArr, drvArr, mode, model);
      if (buf)
        *buf = valArr[index];
      if (drive)
        *drive = drvArr[index];
    } // if buf or drive
  } // if stat
  return stat;
}

CarbonStatus CarbonExprNet::examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index = 0;
  size_t length = 0;
  CarbonStatus stat = CarbonUtil::calcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    if (buf || drive)
    {
      int bitWidth = getBitWidth();
      DynBitVector valBuf(bitWidth);
      DynBitVector drvBuf(bitWidth);
      UInt32* valArr = valBuf.getUIntArray();
      UInt32* drvArr = drvBuf.getUIntArray();
      examine(valArr, drvArr, eIDrive, model);
      if (buf)
        CarbonValRW::cpSrcRangeToDest(buf, valArr, index, length);
      if (drive)
        CarbonValRW::cpSrcRangeToDest(drive, drvArr, index, length);
    } // if buf or drive
  } // if stat
  return stat;
}

void CarbonExprNet::prepareDeposit(const UInt32* buf, const UInt32* drive,
                                   DynBitVector* valBuf, DynBitVector* drvBuf,
                                   int numWords, int bitWidth)
{
  UInt32 lastMask = CarbonValRW::getWordMask(bitWidth);
  UInt32* valArr = valBuf->getUIntArray();
  if (buf)
  {
    CarbonValRW::cpSrcToDest(valArr, buf, numWords);
    valArr[numWords - 1] &= lastMask;
  }
  else
    examine(valArr, NULL, eXDrive, NULL);
  
  if (drive)
  {
    UInt32* drvArr = drvBuf->getUIntArray();
    CarbonValRW::cpSrcToDest(drvArr, drive, numWords);
    drvArr[numWords - 1] &= lastMask;
  }
}

void CarbonExprNet::prepareDepositWord(UInt32 buf, UInt32 drive,
                                       DynBitVector* valBuf, DynBitVector* drvBuf,
                                       int index)
{
  UInt32* valArr = valBuf->getUIntArray();
  UInt32* drvArr = drvBuf->getUIntArray();

  examine(valArr, drvArr, eXDrive, NULL);
  
  valArr[index] = buf;
  drvArr[index] = drive;
}

void CarbonExprNet::prepareDepositRange(const UInt32* buf, const UInt32* drive,
                                        DynBitVector* valBuf, DynBitVector* drvBuf,
                                        size_t index, size_t length)
{
  UInt32* valArr = valBuf->getUIntArray();
  UInt32* drvArr = drvBuf->getUIntArray();

  examine(valArr, drvArr, eXDrive, NULL);
  
  if (buf)
    CarbonValRW::cpSrcToDestRange(valArr, buf, index, length);
  
  if (drive)
    CarbonValRW::cpSrcToDestRange(drvArr, drive, index, length);
}


CarbonStatus CarbonExprNet::deposit(const UInt32* buf, 
                                    const UInt32* drive, CarbonModel* model)
{
  // CarbonExprNet's never change, only their sub expressions do.
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    CarbonStatus stat = eCarbon_OK;
    if (buf || drive)
    {
      int bitWidth = getBitWidth();
      int numWords = getNumUInt32s();
      DynBitVector valBuf(bitWidth);
      DynBitVector drvBuf(bitWidth);
      
      prepareDeposit(buf, drive, &valBuf, &drvBuf, numWords, bitWidth);
      
      CarbonNetIdent::AssignContext 
        assignContext(CarbonNetIdent::AssignContext::eDeposit);
      assignContext.putAssigns(valBuf, drvBuf);
      mExpr->assign(&assignContext);
      stat = assignContext.getStatus();
    }
    return stat;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonExprNet::depositWord(UInt32 buf, int index,
                                        UInt32 drive, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    size_t numWords = getNumUInt32s();
    CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
    if (stat == eCarbon_OK)
    {
      int bitWidth = getBitWidth();
      DynBitVector valBuf(bitWidth);
      DynBitVector drvBuf(bitWidth);
      
      prepareDepositWord(buf, drive, &valBuf, &drvBuf, index);
      stat = deposit(valBuf.getUIntArray(), drvBuf.getUIntArray(), model);
    }
    
    return stat;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonExprNet::depositRange(const UInt32* buf, int range_msb, 
                                         int range_lsb,
                                         const UInt32* drive, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    size_t index = 0;
    size_t length = 0;
    CarbonStatus stat = CarbonUtil::calcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
    if (stat == eCarbon_OK)
    {
      if (buf || drive)
      {
        int bitWidth = getBitWidth();
        DynBitVector valBuf(bitWidth);
        DynBitVector drvBuf(bitWidth);
        prepareDepositRange(buf, drive, &valBuf, &drvBuf, index, length);
        
        const UInt32* valArr = valBuf.getUIntArray();
        const UInt32* drvArr = drvBuf.getUIntArray();
        stat = deposit(valArr, drvArr, model);
      } // if buf or drive
    } // if stat
    return stat;
  }
  return eCarbon_ERROR;
}

void CarbonExprNet::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  CarbonExprNet::deposit(buf, drive, model);
}

void CarbonExprNet::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonExprNet::depositWord(buf, index, drive, model);
}

void CarbonExprNet::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonExprNet::depositRange(buf, range_msb, range_lsb, drive, model);
}

bool CarbonExprNet::isDataNonZero() const
{
  DynBitVector val;
  val.resize(getBitWidth());
  UInt32* valArr = val.getUIntArray();
  examine(valArr, NULL, eIDrive, NULL);
  return val.any();
}

bool CarbonExprNet::setToDriven(CarbonModel* model)
{
  DynBitVector drv;
  drv.resize(getBitWidth());
  UInt32* valArr = drv.getUIntArray();
  deposit(NULL, valArr, model);
  return false;
}

bool CarbonExprNet::setToUndriven(CarbonModel* model)
{
  DynBitVector drv;
  drv.resize(getBitWidth());
  drv.set();
  UInt32* valArr = drv.getUIntArray();
  deposit(NULL, valArr, model);
  return false;
}

bool CarbonExprNet::setWordToUndriven(int index, CarbonModel* model)
{
  DynBitVector drv;
  size_t bitWidth = getBitWidth();
  drv.resize(bitWidth);
  UInt32* valArr = drv.getUIntArray();
  examine((UInt32*)NULL, valArr, eXDrive, model);
  valArr[index] = ~0u;
  if (index == getNumUInt32s() - 1)
    valArr[index] &= CarbonValRW::getWordMask(bitWidth);
  deposit(NULL, valArr, model);
  return false;
}

bool CarbonExprNet::setRangeToUndriven(int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index = 0;
  size_t length = 0;
  CarbonStatus stat = CarbonUtil::calcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    DynBitVector drv;
    drv.resize(getBitWidth());
    UInt32* valArr = drv.getUIntArray();
    examine((UInt32*)NULL, valArr, eXDrive, model);
    
    DynBitVector tmp(length);
    tmp.set();
    const UInt32* tmpArr = tmp.getUIntArray();
    CarbonValRW::cpSrcToDestRange(valArr, tmpArr, index, length);
    deposit(NULL, valArr, model);
  }
  return false;
}

ShellNet::Storage CarbonExprNet::allocShadow() const
{
  int numWords = getNumUInt32s();
  CarbonTriValShadow* shadow = new CarbonTriValShadow(numWords);
  ShellNet::Storage s = static_cast<ShellNet::Storage>(shadow);
  
  update(&s);
  return s;
}

void CarbonExprNet::freeShadow(Storage* shadow)
{
  CarbonTriValShadow* typedShadow = static_cast<CarbonTriValShadow*>(*shadow);
  delete typedShadow;
  *shadow = NULL;
}


ShellNet::ValueState 
CarbonExprNet::writeIfNotEq(char* valueStr, size_t len, 
                            ShellNet::Storage* shadow, 
                            NetFlags flags) 
{
  ShellNet::ValueState state = compare(*shadow);
  if ((state == eChanged) || (valueStr[0] == 'x'))
  {
    state = eChanged;
    update(shadow);
    format(valueStr, len, eCarbonBin, flags, NULL);
  }
  return state;
}

CarbonStatus CarbonExprNet::force(const UInt32* buf, CarbonModel*)
{
  int bitWidth = getBitWidth();
  int numWords = getNumUInt32s();
  DynBitVector valBuf(bitWidth);  
  DynBitVector drvBuf(bitWidth);
  prepareDeposit(buf, NULL, &valBuf, &drvBuf, numWords, bitWidth);
  CarbonNetIdent::AssignContext 
    assignContext(CarbonNetIdent::AssignContext::eForce);
  assignContext.putAssigns(valBuf, drvBuf);
  mExpr->assign(&assignContext);
  return eCarbon_OK;
}

CarbonStatus CarbonExprNet::forceWord(UInt32 buf, int index, CarbonModel* model)
{
  size_t numWords = getNumUInt32s();
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
  if (stat == eCarbon_OK)
  {
    int bitWidth = getBitWidth();
    DynBitVector valBuf(bitWidth);
    DynBitVector drvBuf(bitWidth);

    prepareDepositWord(buf, 0, &valBuf, &drvBuf, index);
    
    CarbonNetIdent::AssignContext 
      assignContext(CarbonNetIdent::AssignContext::eForceRange);
    
    assignContext.putAssigns(valBuf, drvBuf);
    size_t bitIndex, length;
    setupWordRange(&bitIndex, &length, index);
    assignContext.putIndexLength(bitIndex, length);

    mExpr->assign(&assignContext);
  }
  return stat;
}

CarbonStatus CarbonExprNet::forceRange(const UInt32* buf, 
                                       int range_msb, int range_lsb, 
                                       CarbonModel* model)
{
  size_t length, index;
  CarbonStatus stat = CarbonUtil::calcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    int bitWidth = getBitWidth();
    DynBitVector valBuf(bitWidth);
    DynBitVector drvBuf(bitWidth);
    prepareDepositRange(buf, NULL, &valBuf, &drvBuf, index, length);

    CarbonNetIdent::AssignContext 
      assignContext(CarbonNetIdent::AssignContext::eForceRange);
    assignContext.putAssigns(valBuf, drvBuf);
    
    assignContext.putIndexLength(index, length);
    mExpr->assign(&assignContext);
  } // if stat
  return stat;
}


CarbonStatus CarbonExprNet::release(CarbonModel*)
{
  int bitWidth = getBitWidth();
  CarbonNetIdent::AssignContext 
    assignContext(CarbonNetIdent::AssignContext::eRelease);
  // just need these to be the correct size. No assignment takes place
  // underneath
  assignContext.resize(bitWidth);
  mExpr->assign(&assignContext);
  return eCarbon_OK;
}

CarbonStatus CarbonExprNet::releaseWord(int index, CarbonModel* model)
{
  size_t numWords = getNumUInt32s();
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
  if (stat == eCarbon_OK)
  {
    CarbonNetIdent::AssignContext 
      assignContext(CarbonNetIdent::AssignContext::eReleaseRange);
    size_t bitIndex, length;
    setupWordRange(&bitIndex, &length, index);
    assignContext.putIndexLength(bitIndex, length);
    assignContext.resize(getBitWidth());
    
    mExpr->assign(&assignContext);
  }
  return stat;
}

void CarbonExprNet::setupWordRange(size_t* bitIndex, size_t* length, int index)
{
  // for clarity, if this is a scalar the range is 0,0
  *bitIndex = 0;
  *length = 1;
  if (mIntrinsic->getType() == IODBIntrinsic::eVector)
  {
    const ConstantRange* vecRange = mIntrinsic->getVecRange();
    *bitIndex = index * 32;
    *length = 32;
    if (vecRange->getLength() < 32)
      *length = vecRange->getLength();
  }
}

CarbonStatus CarbonExprNet::releaseRange(int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = CarbonUtil::calcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    CarbonNetIdent::AssignContext 
      assignContext(CarbonNetIdent::AssignContext::eReleaseRange);
    
    assignContext.putIndexLength(index, length);
    assignContext.resize(getBitWidth());
    mExpr->assign(&assignContext);
  } // if stat
  return stat;
}

ShellNet::ValueState 
CarbonExprNet::compare(const Storage shadow) const
{
  CarbonNetIdent::EvalContext 
    evalContext(CarbonNetIdent::EvalContext::eDriveCalc);
  CarbonExpr::SignT evalStat = mExpr->evaluate(&evalContext);
  CE_ASSERT(evalStat != CarbonExpr::eBadSign, mExpr);
  
  CarbonTriValShadow* triShadow = static_cast<CarbonTriValShadow*>(shadow);
  int numWords = getNumUInt32s();
  DynBitVector* val = evalContext.getValue();
  bool changed = 
    CarbonValRW::memCompare(triShadow->mValue, val->getUIntArray(), numWords);
  if (! changed)
  {
    DynBitVector* drv = evalContext.getDrive();
    changed =     
      CarbonValRW::memCompare(triShadow->mDrive, drv->getUIntArray(), numWords);
  }

  ValueState stat = eUnchanged;
  if (changed)
    stat = eChanged;
  return stat;
}

void CarbonExprNet::update(Storage* shadow) const
{
  CarbonNetIdent::EvalContext 
    evalContext(CarbonNetIdent::EvalContext::eDriveCalc);
  CarbonExpr::SignT evalStat = mExpr->evaluate(&evalContext);
  CE_ASSERT(evalStat != CarbonExpr::eBadSign, mExpr);
  
  CarbonTriValShadow* triShadow = static_cast<CarbonTriValShadow*>(*shadow);
  int numWords = getNumUInt32s();
  DynBitVector* val = evalContext.getValue();
  DynBitVector* drv = evalContext.getDrive();
  CarbonValRW::cpSrcToDest(triShadow->mValue, val->getUIntArray(), numWords);
  CarbonValRW::cpSrcToDest(triShadow->mDrive, drv->getUIntArray(), numWords);
}

CarbonStatus CarbonExprNet::format(char* valueStr, size_t len, 
                                   CarbonRadix strFormat, 
                                   NetFlags, CarbonModel* model) const
{
  CarbonStatus stat = eCarbon_OK;

  CarbonNetIdent::EvalContext 
    evalContext(CarbonNetIdent::EvalContext::eFormat);
  CarbonExpr::SignT evalStat = mExpr->evaluate(&evalContext);
  CE_ASSERT(evalStat != CarbonExpr::eBadSign, mExpr);

  const DynBitVector& value = evalContext.getValueRef();
  const UInt32* valArr = value.getUIntArray();
  DynBitVector& drive = evalContext.getDriveRef();
  
  /*
    have to pass drive in as idrive. The expression context can't
    split the drive into xdrive and idrive. It can only give a
    calculated drive. That's fine. The calculated drive allows us to
    simply treat an expressioned bidirect as a non-bidirect tristate
    for formatting purposes, because the drive is resolved. We have to
    flip the value however.
  */
  drive.flip();
  size_t bitWidth = getBitWidth();
  if (ShellNet::sDoFormat(valueStr, len, strFormat, valArr, drive.getUIntArray(), mControlMask, bitWidth, model) != eCarbon_OK)
    stat = eCarbon_ERROR;
  return stat;
}

int CarbonExprNet::hasDriveConflict() const
{
  int ret = 0;
  if (isTristate())
  {
    ConflictDetectWalk walker;
    walker.visitExpr(const_cast<CarbonExpr*>(mExpr));
    if (walker.hasConflict())
      ret = 1;
  }
  return ret;
}

const CarbonExprNet* CarbonExprNet::castExprNet() const
{
  return this;
}

bool CarbonExprNet::resolveXdrive(CarbonModel* model)
{
  ResolveXDrive xdriveResolver(model);
  xdriveResolver.visitExpr(mExpr);
  return xdriveResolver.isChanged();
}

void CarbonExprNet::getExternalDrive(UInt32* xdrive) const
{
  (void) examine((UInt32*)NULL, xdrive, eXDrive, NULL);
}

void CarbonExprNet::putControlMask(const UInt32* controlMask)
{
  mControlMask = controlMask;
}

const UInt32* CarbonExprNet::getControlMask() const
{
  return mControlMask;
}

bool CarbonExprNet::isReal() const
{
  return false;
}

CarbonStatus CarbonExprNet::examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const
{
  return eCarbon_ERROR;
}

const CarbonMemory* CarbonExprNet::castMemory() const
{
  return NULL;
}

const CarbonModelMemory* CarbonExprNet::castModelMemory() const
{
  return NULL;
}

const CarbonVectorBase* CarbonExprNet::castVector() const
{
  return NULL;
}

const CarbonScalarBase* CarbonExprNet::castScalar() const
{
  return NULL;
}

const ShellNetConstant* CarbonExprNet::castConstant() const
{
  return NULL;
}

const CarbonForceNet* CarbonExprNet::castForceNet() const
{
  return NULL;
}

void CarbonExprNet::putToZero(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

void CarbonExprNet::putToOnes(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

CarbonStatus CarbonExprNet::setRange(int, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus CarbonExprNet::clearRange(int, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

int CarbonExprNet::hasDriveConflictRange(SInt32, SInt32) const
{
  // not allowed on a carbonexpr net
  ST_ASSERT(0, getName());
  return 0;
}

CarbonStatus CarbonExprNet::examineValXDriveWord(UInt32*, UInt32*, int) const
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void CarbonExprNet::setRawToUndriven(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

ShellNet::ValueState CarbonExprNet::writeIfNotEqForce(char*, size_t, Storage*, NetFlags, ShellNet*)
{
  ST_ASSERT(0, getName());
  return eUnchanged;
}

CarbonStatus CarbonExprNet::formatForce(char*, size_t, CarbonRadix, NetFlags, ShellNet*, CarbonModel*) const
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void CarbonExprNet::putChangeArrayRef(CarbonChangeType*)
{
  ST_ASSERT(0, getName());
}

CarbonChangeType* CarbonExprNet::getChangeArrayRef()
{
  ST_ASSERT(0, getName());
  return NULL;
}

void CarbonExprNet::gatherWrappedNets(NetVec* netVec) const
{
  SubNetGatherWalk walk(netVec);
  walk.visitExpr(mExpr);
}

void CarbonExprNet::replaceWrappedNets(const NetVec& netVec)
{
  NetVec check;
  SubNetGatherWalk gatherWalk(&check);
  gatherWalk.visitExpr(mExpr);
  
  ST_ASSERT(netVec.size() == check.size(), getName());
  
  SubNetReplaceWalk walk(netVec);
  walk.visitExpr(mExpr);
}

static inline ShellNet::ValueState
sValueChangeHelper(CarbonExpr* expr,
                   CarbonTriValShadow* fullShadow, 
                   CarbonNetIdent::EvalContext::ModeT mode,
                   const UInt32 numWords)
{
  const UInt32 numBytes = numWords * sizeof(UInt32);
  
  // Examine (mode specified by caller)
  CarbonNetIdent::EvalContext calcEvalContext(mode);
  CarbonExpr::SignT evalStat = expr->evaluate(&calcEvalContext);
  CE_ASSERT(evalStat != CarbonExpr::eBadSign, expr);
  DynBitVector* val = calcEvalContext.getValue();
  DynBitVector* drv = calcEvalContext.getDrive();

  // Compare and update
  if ((memcmp(fullShadow->mValue, val->getUIntArray(), numBytes) != 0) ||
      (memcmp(fullShadow->mDrive, drv->getUIntArray(), numBytes) != 0))
  {
    CarbonValRW::cpSrcToDest(fullShadow->mValue, val->getUIntArray(), numWords);
    CarbonValRW::cpSrcToDest(fullShadow->mDrive, drv->getUIntArray(), numWords);
    return ShellNet::eChanged;
  } else {
    return ShellNet::eUnchanged;
  }
} // sValueChangeHelper

void CarbonExprNet::runValueChangeCB(CarbonNetValueCBData* cbData, 
                                     UInt32*, 
                                     UInt32*,
                                     CarbonTriValShadow* fullShadow,
                                     CarbonModel* model) const
{
  // It must be calc mode because no calc mode goes through
  // compareUpdateExamineUnresolved.
  ST_ASSERT(cbData->getExamineMode() == ShellNet::eCalcDrive, getName());
  CarbonNetIdent::EvalContext::ModeT mode = 
    CarbonNetIdent::EvalContext::eDriveCalc;

  // Check for changes using the resolved value (eDriveCalc)
  const UInt32 numWords = getNumUInt32s();
  if (sValueChangeHelper(mExpr, fullShadow, mode, numWords) == eChanged) {
    // examine again. Need the idrive (Carbon contribution)
    CarbonNetIdent::EvalContext 
      evalContext(CarbonNetIdent::EvalContext::eExamine);
    CarbonExpr::SignT evalStat = mExpr->evaluate(&evalContext);
    CE_ASSERT(evalStat != CarbonExpr::eBadSign, mExpr);

    // Call the callback
    DynBitVector* val = evalContext.getValue();
    DynBitVector* drv = evalContext.getDrive();
    cbData->executeCB(model->getObjectID (), val->getUIntArray(), drv->getUIntArray());
  }
}

ShellNet::ValueState
CarbonExprNet::compareUpdateExamineUnresolved(Storage* shadow,
                                              UInt32* value, UInt32* drive)
{
  // Examine and compare using the Carbon contribution only (eExamine)
  CarbonTriValShadow* fullShadow = static_cast<CarbonTriValShadow*>(*shadow);
  const UInt32 numWords = getNumUInt32s();
  ValueState state = sValueChangeHelper(mExpr, fullShadow, 
                                        CarbonNetIdent::EvalContext::eExamine,
                                        numWords);

  // If it changed, update the shadow and the examine buffers
  if (state == eChanged) {
    // It changed, update the examine buffers
    CarbonValRW::cpSrcToDest(value, fullShadow->mValue, numWords);
    CarbonValRW::cpSrcToDest(drive, fullShadow->mDrive, numWords);
  }
  return state;
}

void CarbonExprNet::updateUnresolved(Storage* shadow) const
{
  CarbonNetIdent::EvalContext evalContext(CarbonNetIdent::EvalContext::eExamine);
  CarbonExpr::SignT evalStat = mExpr->evaluate(&evalContext);
  CE_ASSERT(evalStat != CarbonExpr::eBadSign, mExpr);
  
  CarbonTriValShadow* triShadow = static_cast<CarbonTriValShadow*>(*shadow);
  int numWords = getNumUInt32s();
  DynBitVector* val = evalContext.getValue();
  DynBitVector* drv = evalContext.getDrive();
  CarbonValRW::cpSrcToDest(triShadow->mValue, val->getUIntArray(), numWords);
  CarbonValRW::cpSrcToDest(triShadow->mDrive, drv->getUIntArray(), numWords);
}
