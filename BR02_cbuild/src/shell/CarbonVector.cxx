// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/CarbonVector.h"
#include "shell/CarbonVectorInput.h"
#include "util/ConstantRange.h"
#include "shell/ShellNetMacroDefine.h"

static inline size_t sGetNumWords(int bitWidth)
{
  const size_t wordBitSize = sizeof(UInt32) * 8;
  return (bitWidth + wordBitSize - 1)/wordBitSize;
}

static UInt32 sGetPrimValue(const UInt32* buf, int bitWidth)
{
  UInt32 maskVal = CarbonValRW::getWordMask(UInt32(bitWidth));
  return *buf & maskVal;
}

static CarbonStatus sCalcIndexLength(int msb, int lsb, int range_msb, int range_lsb, size_t* index, size_t* length, CarbonModel* model)
{
  return CarbonUtil::calcIndexLength(msb, lsb, range_msb, range_lsb, index, length, model);
}

static void sDoAllOnes(UInt32* drive, int bitWidth)
{
  size_t numWords = sGetNumWords(bitWidth);
  CarbonValRW::setToOnes(drive, numWords);
  drive[numWords - 1] &= CarbonValRW::getWordMask(UInt32(bitWidth));
}

static void sDoAllZeros(UInt32* drive, int bitWidth)
{
  size_t numWords = sGetNumWords(bitWidth);
  CarbonValRW::setToZero(drive, numWords);
  drive[numWords - 1] &= CarbonValRW::getWordMask(UInt32(bitWidth));
}

template <typename T> ShellNet::ValueState doCompare(const void* shadow, const T* val, int numWords)
{
  ShellNet::ValueState state = ShellNet::eUnchanged;
  if (CarbonValRW::memCompare(shadow, val, numWords))
    state = ShellNet::eChanged;
  return state;
}

template <typename T> ShellNet::ValueState doComparePrim(const void* shadow, const T* val)
{
  ShellNet::ValueState state = ShellNet::eUnchanged;
  const T* typedShadow = static_cast<const T*>(shadow);
  if (*typedShadow != *val)
    state = ShellNet::eChanged;
  return state;
}

template <typename T> void doUpdate(ShellNet::Storage* shadow, const T* val, int numWords)
{
  T* dest = static_cast<T*>(*shadow);
  CarbonValRW::cpSrcToDest(dest, val, numWords);
}

static ShellNet::ValueState 
sDoWriteIfNotEq(char* valueStr, 
                size_t len, 
                ShellNet::Storage* shadow,
                const UInt32* val, const UInt32* forceMask, 
                const UInt32* controlMask, int bitWidth)
{
  size_t numWords = sGetNumWords(bitWidth);
  ShellNet::ValueState state = doCompare(*shadow, val, numWords);
  if ((state == ShellNet::eChanged) || (valueStr[0] == 'x'))
  {
    state = ShellNet::eChanged;

    CarbonValRW::cpSrcToDest(static_cast<UInt32*>(*shadow), val, numWords); 
    (void)valueFormatString(valueStr, len, eCarbonBin, val, forceMask, controlMask, bitWidth, NULL);
  }
  return state;
}

template <typename T> ShellNet::ValueState 
doWriteIfNotEqPrim(char* valueStr, 
                   size_t len, 
                   ShellNet::Storage* shadow,
                   const T* val, const T* forceMask, 
                   const UInt32* controlMask,
                   int bitWidth)
{
  ShellNet::ValueState state = doComparePrim(*shadow, val);
  if ((state == ShellNet::eChanged) || (valueStr[0] == 'x'))
  {
    state = ShellNet::eChanged;
    T* typedShadow = static_cast<T*>(*shadow);
    size_t numWords = sizeof(T)/4;
    CarbonValRW::cpSrcToDest(typedShadow, val, numWords); 
    (void)valueFormatString(valueStr, len, eCarbonBin, val, forceMask, controlMask, bitWidth, NULL);
  }
  return state;
}


// For word-sized or less primitives. Will not work for UInt64 and arrays
template <typename T> CarbonStatus doExamineValXDriveWordDefault(UInt32* val, UInt32* drv, int index, const T* vec, int bitWidth)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, NULL);
  if (stat == eCarbon_OK)
  {
    *val = *vec;
    *drv = CarbonValRW::getWordMask(UInt32(bitWidth));
  }
  return stat;
}

template <typename T> CarbonStatus doExamineValXDriveWordDefaultArr(UInt32* val, UInt32* drv, int index, const T* vec, int bitWidth)
{
  size_t numWords = sGetNumWords(bitWidth);
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, NULL);
  if (stat == eCarbon_OK)
  {
    CarbonValRW::cpSrcToDestWord(val, vec, index);
    if (index == signed(numWords - 1))
      *drv = CarbonValRW::getWordMask(UInt32(bitWidth));
    else
      CarbonValRW::setToOnes(drv, 1);
  }
  return stat;
}


template <typename T> bool doAssignInputPrim(const UInt32* buf, T* vec, int bitWidth)
{
  UInt32 val = sGetPrimValue(buf, bitWidth);
  bool changed = false;
  if (*vec != val)
  {
    changed = true;
    *vec = val;
  }
  return changed;
}

template <typename T> bool doAssignInputRange(const UInt32* buf, 
                                              T* vec, 
                                              size_t index, 
                                              size_t length)
{
  bool changed = false;
  if (CarbonValRW::memCompareRange(buf, vec, index, length) != 0)
  {
    changed = true;
    CarbonValRW::cpSrcToDestRange(vec, buf, index, length);
  }
  return changed;
}

  
static bool sDoAssignInputLL(const UInt32* buf, 
                             UInt64* vec, int bitWidth)
{
  UInt64 cpVal;
  CarbonValRW::cpSrcToDest(&cpVal, buf, 2);
  cpVal &= CarbonValRW::getWordMaskLL(UInt64(bitWidth));
  bool changed = false;
  if (*vec != cpVal)
  {
    changed = true;
    *vec = cpVal;
  }
  return changed;
}

static bool sDoAssignInputWordLL(UInt32 buf, UInt64* vec, int index, int bitWidth)
{
  UInt32 myVal[2];
  CarbonValRW::cpSrcToDest(myVal, vec, 2);
  if (index == 1)
    buf &= CarbonValRW::getWordMask(UInt32(bitWidth));
  
  bool changed = false;
  if (myVal[index] != buf)
  {
    changed = true;
    CarbonValRW::cpSrcWordToDest(vec, buf, index);
  }
  return changed;  
}

static bool sDoAssignInputA(const UInt32* buf, UInt32* vec, int bitWidth)
{
  UInt32 lastMask = CarbonValRW::getWordMask(UInt32(bitWidth));

  // Can't use memCompare because I need to look at the words up to
  // the last word. Big-endian v. little-endian could get different
  // results.
  size_t numWords = sGetNumWords(bitWidth);
  bool changed = false;
  for (size_t i = 0; ! changed && (i < numWords - 1); ++i)
    changed = buf[i] != vec[i];

  if (! changed)
    changed = vec[numWords - 1] != (buf[numWords - 1] & lastMask);
  
  if (changed)
  {
    CarbonValRW::cpSrcToDest(vec, buf, numWords);
    vec[numWords - 1] &= lastMask;
  }
  return changed;
}

static bool sDoAssignInputWordA(UInt32 buf, UInt32* vec, int index, int bitWidth)
{
  int numWords = sGetNumWords(bitWidth);
  if (index == numWords - 1)
    buf &= CarbonValRW::getWordMask(UInt32(bitWidth));
  
  bool changed = false;
  if (vec[index] != buf)
  {
    changed = true;
    vec[index] = buf;
  }
  return changed;  
}

// A very efficient way to compute the mask for the last bits of an
// array
static inline UInt32 sGetMask(int numBits)
{
  register UInt32 mask = UtUINT32_MAX;
  const int bitShift = numBits % (sizeof(UInt32) * 8);
  const UInt32 doShift = bitShift != 0;
  const UInt32 noShift = !doShift;
  mask <<= bitShift;
  mask = doShift * ~mask + noShift * mask;
  return mask;
}

// CarbonVector
CarbonVector::CarbonVector()
{}

CarbonVector::~CarbonVector()
{}

// CarbonVector1
CarbonVector1::CarbonVector1(UInt8* vec) :
  mVector(vec)
{}

CarbonVector1::CarbonVector1(SInt8* vec) :
  mVector(reinterpret_cast<UInt8*>(vec))
{}


CarbonVector1::~CarbonVector1()
{}

void CarbonVector1::setConstantBits()
{
  CarbonValRW::setConstantBits(mVector, NULL, mControlMask, mRange->getLength());
}

void CarbonVector1::getTraits(Traits* traits) const
{
  traits->mWidth = mRange->getLength();
  traits->putByte(mVector);
}

void CarbonVector1::doFastDeposit(const UInt32* buf, CarbonModel* model)
{
  bool changed = assignValue(buf);
  doUpdateVHM(changed, model);
}

void CarbonVector1::doDeposit(const UInt32* buf, CarbonModel* model)
{
  if (buf)
    doFastDeposit(buf, model);
}

CarbonStatus CarbonVector1::doDepositWord(UInt32 buf, int index, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
    doDeposit(&buf, model);
  return stat;
}

CarbonStatus CarbonVector1::deposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doDeposit(buf, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}
  
CarbonStatus CarbonVector1::depositWord(UInt32 buf, int index, UInt32, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    return doDepositWord(buf, index, model);
  return eCarbon_ERROR;
}

CarbonStatus CarbonVector1::doDepositRange(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;

  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    bool changed = assignValueRange(buf, index, length);
    doUpdateVHM(changed, model);
  }
  return stat;
}


CarbonStatus CarbonVector1::depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    return doDepositRange(buf, range_msb, range_lsb, model);
  return eCarbon_ERROR;
}

void CarbonVector1::fastDeposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  doFastDeposit(buf, model);
}

void CarbonVector1::fastDepositWord(UInt32 buf, int, UInt32, CarbonModel* model)
{
  doFastDeposit(&buf, model);
}

void CarbonVector1::fastDepositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRange(buf, range_msb, range_lsb, model);
}


CarbonStatus CarbonVector1::examine(UInt32* buf, UInt32* drive, ExamineMode, CarbonModel*) const
{
  if (buf)
    *buf = *mVector;
  if (drive)
    *drive = 0;
  return eCarbon_OK;
}

CarbonStatus CarbonVector1::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);  
  if (stat == eCarbon_OK)
    examine(buf, drive, mode, model);
  return stat;
}

CarbonStatus CarbonVector1::examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    const UInt8* vec = mVector;
    if (buf)
      CarbonValRW::cpSrcRangeToDest(buf, vec, index, length);
    if (drive)
      examineModelDriveRange(drive, index, length);
  }
  return stat;
}

CarbonStatus CarbonVector1::examineValXDriveWord(UInt32* val, UInt32* drv, int index) const
{
  return doExamineValXDriveWordDefault(val, drv, index, mVector, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonVector1::update(ShellNet::Storage* shadow) const
{
  const UInt8* vec = mVector;
  doUpdate(shadow, vec, 1);
}
  
ShellNet::ValueState CarbonVector1::compare(const ShellNet::Storage shadow) const
{
  const UInt8* vec = mVector;
  return doComparePrim(shadow, vec);
}

ShellNet::ValueState CarbonVector1::writeIfNotEq(char* valueStr, size_t len, 
                                                 ShellNet::Storage* shadow,
                                                 NetFlags)
{
  const UInt8* vec = mVector;
  return doWriteIfNotEqPrim(valueStr, len, shadow, vec, (UInt8*)NULL, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

ShellNet::ValueState CarbonVector1::writeIfNotEqForce(char* valueStr, size_t len, 
                                                      ShellNet::Storage* shadow,
                                                      NetFlags, ShellNet* forceMask)
{
  const UInt8* vec = mVector;
  CarbonVector1* castForce = (CarbonVector1*) forceMask;
  const UInt8* forceMaskVec = castForce->getExamineStore();
  return doWriteIfNotEqPrim(valueStr, len, shadow, vec, forceMaskVec, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}


CarbonStatus CarbonVector1::format(char* valueStr, size_t len, 
                                   CarbonRadix strFormat,
                                   NetFlags, CarbonModel* model) const
{
  const UInt8* vec = mVector;
  return valueFormatString(valueStr, len, strFormat, vec, (UInt8*) NULL, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()), model);
}

CarbonStatus CarbonVector1::formatForce(char* valueStr, size_t len, 
                                        CarbonRadix strFormat, NetFlags, ShellNet* forceMask, CarbonModel* model) const
{
  CarbonVector1* castForce = (CarbonVector1*) forceMask;
  const UInt8* forceMaskVec = castForce->getExamineStore();
  const UInt8* vec = mVector;
  return valueFormatString(valueStr, len, strFormat, vec, forceMaskVec, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()), model);
}

bool CarbonVector1::isDataNonZero() const
{
  const UInt8* vec = mVector;
  return (*vec != 0);
}

bool CarbonVector1::assignValue(const UInt32* buf)
{
  //  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  int bitWidth = mRange->getLength();
  return doAssignInputPrim(buf, mVector, bitWidth);
}

bool CarbonVector1::assignValueRange(const UInt32* buf, size_t index, size_t length)
{
  return doAssignInputRange(buf, mVector, index, length);
}

const UInt8* CarbonVector1::getExamineStore() const
{
  return mVector;
}

void CarbonVector1::examineModelDrive(UInt32* drive, ExamineMode) const
{
  *drive = 0;
}

void CarbonVector1::examineModelDriveRange(UInt32* drive, size_t, size_t) const
{
  *drive = 0;
}

ShellNet::Storage CarbonVector1::allocShadow() const
{
  UInt8* typedShadow = CARBON_ALLOC_VEC(UInt8, 1);
  ShellNet::Storage castShadow = static_cast<ShellNet::Storage>(typedShadow);
  update(&castShadow);
  return castShadow;
}
  
void CarbonVector1::freeShadow(ShellNet::Storage* shadow)
{
  UInt8* typedShadow = static_cast<UInt8*>(*shadow);
  CARBON_FREE_VEC(typedShadow, UInt8, 1);
  *shadow = NULL;
}

// CarbonVector2
CarbonVector2::CarbonVector2(UInt16* vec) :
  mVector(vec)
{}

CarbonVector2::CarbonVector2(SInt16* vec) :
  mVector(reinterpret_cast<UInt16*>(vec))
{}


CarbonVector2::~CarbonVector2()
{}

void CarbonVector2::setConstantBits()
{
  CarbonValRW::setConstantBits(mVector, NULL, mControlMask, mRange->getLength());
}

void CarbonVector2::getTraits(Traits* traits) const
{
  traits->mWidth = mRange->getLength();
  traits->putShort(mVector);
}

void CarbonVector2::doFastDeposit(const UInt32* buf, CarbonModel* model)
{
  bool changed = assignValue(buf);
  doUpdateVHM(changed, model);
}

void CarbonVector2::doDeposit(const UInt32* buf, CarbonModel* model)
{
  if (buf)
    doFastDeposit(buf, model);
}

CarbonStatus CarbonVector2::deposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doDeposit(buf, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonVector2::doDepositWord(UInt32 buf, int index, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
    doDeposit(&buf, model);
  return stat;
}

CarbonStatus CarbonVector2::depositWord(UInt32 buf, int index, UInt32, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    return doDepositWord(buf, index, model);
  return eCarbon_ERROR;
}

CarbonStatus CarbonVector2::doDepositRange(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;

  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    bool changed = assignValueRange(buf, index, length);
    doUpdateVHM(changed, model);
  }
  return stat;
}

CarbonStatus CarbonVector2::depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    return doDepositRange(buf, range_msb, range_lsb, model);
  return eCarbon_ERROR;
}


void CarbonVector2::fastDeposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  doFastDeposit(buf, model);
}

void CarbonVector2::fastDepositWord(UInt32 buf, int, UInt32, CarbonModel* model)
{
  doFastDeposit(&buf, model);
}

void CarbonVector2::fastDepositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRange(buf, range_msb, range_lsb, model);
}

CarbonStatus CarbonVector2::examine(UInt32* buf, UInt32* drive, ExamineMode, CarbonModel*) const
{
  if (buf)
    *buf = *mVector;
  if (drive)
    *drive = 0;
  return eCarbon_OK;
}
  
CarbonStatus CarbonVector2::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);  
  if (stat == eCarbon_OK)
    examine(buf, drive, mode, model);
  return stat;
}

CarbonStatus CarbonVector2::examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    const UInt16* vec = mVector;
    if (buf)
      CarbonValRW::cpSrcRangeToDest(buf, vec, index, length);
    if (drive)
      examineModelDriveRange(drive, index, length);
  }
  return stat;
}

CarbonStatus CarbonVector2::examineValXDriveWord(UInt32* val, UInt32* drv, int index) const
{
  return doExamineValXDriveWordDefault(val, drv, index, mVector, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonVector2::update(ShellNet::Storage* shadow) const
{
  const UInt16* vec = mVector;
  doUpdate(shadow, vec, 1);
}
  
ShellNet::ValueState CarbonVector2::compare(const ShellNet::Storage shadow) const
{
  const UInt16* vec = mVector;
  return doComparePrim(shadow, vec);
}

ShellNet::ValueState CarbonVector2::writeIfNotEq(char* valueStr, size_t len, 
                                                 ShellNet::Storage* shadow,
                                                 NetFlags)
{
  const UInt16* vec = mVector;
  return doWriteIfNotEqPrim(valueStr, len, shadow, vec, (UInt16*) NULL,  mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}


ShellNet::ValueState CarbonVector2::writeIfNotEqForce(char* valueStr, size_t len, 
                                                      ShellNet::Storage* shadow,
                                                      NetFlags, ShellNet* forceMask)
{
  const UInt16* vec = mVector;
  CarbonVector2* castForce = (CarbonVector2*) forceMask;
  const UInt16* forceMaskVec = castForce->getExamineStore();
  return doWriteIfNotEqPrim(valueStr, len, shadow, vec, forceMaskVec, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

CarbonStatus CarbonVector2::format(char* valueStr, size_t len, 
                                   CarbonRadix strFormat,
                                   NetFlags, CarbonModel* model) const
{
  const UInt16* vec = mVector;
  return valueFormatString(valueStr, len, strFormat, vec, (UInt16*) NULL, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()), model);
}

CarbonStatus CarbonVector2::formatForce(char* valueStr, size_t len, 
                                        CarbonRadix strFormat, NetFlags, ShellNet* forceMask, CarbonModel* model) const
{
  CarbonVector2* castForce = (CarbonVector2*) forceMask;
  const UInt16* forceMaskVec = castForce->getExamineStore();
  const UInt16* vec = mVector;
  return valueFormatString(valueStr, len, strFormat, vec, forceMaskVec, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()), model);
}

bool CarbonVector2::isDataNonZero() const
{
  const UInt16* vec = mVector;
  return (*vec != 0);
}

bool CarbonVector2::assignValue(const UInt32* buf)
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return doAssignInputPrim(buf, mVector, bitWidth);
}

bool CarbonVector2::assignValueRange(const UInt32* buf, size_t index, size_t length)
{
  return doAssignInputRange(buf, mVector, index, length);
}

const UInt16* CarbonVector2::getExamineStore() const
{
  return mVector;
}

void CarbonVector2::examineModelDrive(UInt32* drive, ExamineMode) const
{
  *drive = 0;
}

void CarbonVector2::examineModelDriveRange(UInt32* drive, size_t, size_t) const
{
  *drive = 0;
}


ShellNet::Storage CarbonVector2::allocShadow() const
{
  UInt16* typedShadow = CARBON_ALLOC_VEC(UInt16, 1);
  ShellNet::Storage castShadow = static_cast<ShellNet::Storage>(typedShadow);
  update(&castShadow);
  return castShadow;
}
  
void CarbonVector2::freeShadow(ShellNet::Storage* shadow)
{
  UInt16* typedShadow = static_cast<UInt16*>(*shadow);
  CARBON_FREE_VEC(typedShadow, UInt16, 1);
  *shadow = NULL;
}

// CarbonVector4
CarbonVector4::CarbonVector4(UInt32* vec) :
  mVector(vec)
{}

CarbonVector4::CarbonVector4(SInt32* vec) :
  mVector(reinterpret_cast<UInt32*>(vec))
{}

CarbonVector4::~CarbonVector4()
{}

void CarbonVector4::setConstantBits()
{
  CarbonValRW::setConstantBits(mVector, NULL, mControlMask, mRange->getLength());
}

void CarbonVector4::getTraits(Traits* traits) const
{
  traits->mWidth = mRange->getLength();
  traits->putLongPtr(mVector);
}

void CarbonVector4::doFastDeposit(const UInt32* buf, CarbonModel* model)
{
  bool changed = assignValue(buf);
  doUpdateVHM(changed, model);
}

void CarbonVector4::doDeposit(const UInt32* buf, CarbonModel* model)
{
  if (buf)
    doFastDeposit(buf, model);
}

CarbonStatus CarbonVector4::deposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doDeposit(buf, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}
  
CarbonStatus CarbonVector4::doDepositWord(UInt32 buf, int index, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
    doDeposit(&buf, model);
  return stat;
}

CarbonStatus CarbonVector4::depositWord(UInt32 buf, int index, UInt32, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    return doDepositWord(buf, index, model);
  return eCarbon_ERROR;
}

CarbonStatus CarbonVector4::doDepositRange(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;

  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    bool changed = assignValueRange(buf, index, length);
    doUpdateVHM(changed, model);
  }
  return stat;
}

CarbonStatus CarbonVector4::depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    return doDepositRange(buf, range_msb, range_lsb, model);
  return eCarbon_ERROR;
}

void CarbonVector4::fastDeposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  doFastDeposit(buf, model);
}

void CarbonVector4::fastDepositWord(UInt32 buf, int, UInt32, CarbonModel* model)
{
  doFastDeposit(&buf, model);
}

void CarbonVector4::fastDepositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRange(buf, range_msb, range_lsb,model);
}

CarbonStatus CarbonVector4::examine(UInt32* buf, UInt32* drive, ExamineMode, CarbonModel*) const
{
  if (buf)
    *buf = *mVector;
  if (drive)
    *drive = 0;
  return eCarbon_OK;
}
  
CarbonStatus CarbonVector4::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);  
  if (stat == eCarbon_OK)
    examine(buf, drive, mode, model);
  return stat;
}

CarbonStatus CarbonVector4::examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    const UInt32* vec = mVector;
    if (buf)
      CarbonValRW::cpSrcRangeToDest(buf, vec, index, length);
    if (drive)
      examineModelDriveRange(drive, index, length);
  }
  return stat;
}

CarbonStatus CarbonVector4::examineValXDriveWord(UInt32* val, UInt32* drv, int index) const
{
  return doExamineValXDriveWordDefault(val, drv, index, mVector, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonVector4::update(ShellNet::Storage* shadow) const
{
  const UInt32* vec = mVector;
  doUpdate(shadow, vec, 1);
}
  
ShellNet::ValueState CarbonVector4::compare(const ShellNet::Storage shadow) const
{
  const UInt32* vec = mVector;
  return doComparePrim(shadow, vec);
}

ShellNet::ValueState CarbonVector4::writeIfNotEq(char* valueStr, size_t len, 
                                                 ShellNet::Storage* shadow,
                                                 NetFlags)
{
  const UInt32* vec = mVector;
  return doWriteIfNotEqPrim(valueStr, len, shadow, vec, (UInt32*) NULL,  mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

ShellNet::ValueState CarbonVector4::writeIfNotEqForce(char* valueStr, size_t len, 
                                                      ShellNet::Storage* shadow,
                                                      NetFlags, ShellNet* forceMask)
{
  const UInt32* vec = mVector;
  CarbonVector4* castForce = (CarbonVector4*) forceMask;
  const UInt32* forceMaskVec = castForce->getExamineStore();
  return doWriteIfNotEqPrim(valueStr, len, shadow, vec, forceMaskVec, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

CarbonStatus CarbonVector4::format(char* valueStr, size_t len, 
                                   CarbonRadix strFormat,
                                   NetFlags, CarbonModel* model) const
{
  const UInt32* vec = mVector;
  return valueFormatString(valueStr, len, strFormat, vec, (UInt32*) NULL, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()), model);
}

CarbonStatus CarbonVector4::formatForce(char* valueStr, size_t len, 
                                        CarbonRadix strFormat, NetFlags, ShellNet* forceMask, CarbonModel* model) const
{
  CarbonVector4* castForce = (CarbonVector4*) forceMask;
  const UInt32* forceMaskVec = castForce->getExamineStore();
  const UInt32* vec = mVector;
  return valueFormatString(valueStr, len, strFormat, vec, forceMaskVec, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()), model);
}

bool CarbonVector4::isDataNonZero() const
{
  const UInt32* vec = mVector;
  return (*vec != 0);
}

bool CarbonVector4::assignValue(const UInt32* buf)
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return doAssignInputPrim(buf, mVector, bitWidth);
}

bool CarbonVector4::assignValueRange(const UInt32* buf, size_t index, size_t length)
{
  return doAssignInputRange(buf, mVector, index, length);
}

const UInt32* CarbonVector4::getExamineStore() const
{
  return mVector;
}

void CarbonVector4::examineModelDrive(UInt32* drive, ExamineMode) const
{
  *drive = 0;
}

void CarbonVector4::examineModelDriveRange(UInt32* drive, size_t, size_t) const
{
  *drive = 0;
}

ShellNet::Storage CarbonVector4::allocShadow() const
{
  UInt32* typedShadow = CARBON_ALLOC_VEC(UInt32, 1);
  ShellNet::Storage castShadow = static_cast<ShellNet::Storage>(typedShadow);
  update(&castShadow);
  return castShadow;
}
  
void CarbonVector4::freeShadow(ShellNet::Storage* shadow)
{
  UInt32* typedShadow = static_cast<UInt32*>(*shadow);
  CARBON_FREE_VEC(typedShadow, UInt32, 1);
  *shadow = NULL;
}

// CarbonVector8
CarbonVector8::CarbonVector8(UInt64* vec) :
  mVector(vec)
{}

CarbonVector8::CarbonVector8(SInt64* vec) :
  mVector(reinterpret_cast<UInt64*>(vec))
{}

CarbonVector8::CarbonVector8(CarbonReal* vec) :
  mVector(reinterpret_cast<UInt64*>(vec))
{}

void CarbonVector8::setConstantBits()
{
  CarbonValRW::setConstantBits(mVector, NULL, mControlMask, mRange->getLength());
}

CarbonVector8::~CarbonVector8()
{}

void CarbonVector8::getTraits(Traits* traits) const
{
  traits->mWidth = mRange->getLength();
  traits->putLLong(mVector);
}

void CarbonVector8::doDeposit(const UInt32* buf, CarbonModel* model)
{
  if (buf)
  {
    bool changed = assignValue(buf);
    doUpdateVHM(changed, model);
  }
}

CarbonStatus CarbonVector8::deposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doDeposit(buf, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonVector8::doDepositWord(UInt32 buf, int index, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, model);
  if (stat == eCarbon_OK)
  {
    bool changed = assignValueWord(buf, index);
    doUpdateVHM(changed, model);
  }
  return stat;
}

CarbonStatus CarbonVector8::depositWord(UInt32 buf, int index, UInt32, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    return doDepositWord(buf, index, model);
  return eCarbon_ERROR;
}

CarbonStatus CarbonVector8::doDepositRange(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    bool changed = assignValueRange(buf, index, length);
    doUpdateVHM(changed, model);
  }
  return stat;
}

CarbonStatus CarbonVector8::depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    return doDepositRange(buf, range_msb, range_lsb, model);
  return eCarbon_ERROR;
}


void CarbonVector8::fastDeposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  bool changed = assignValue(buf);
  doUpdateVHM(changed, model);
}

void CarbonVector8::fastDepositWord(UInt32 buf, int index, UInt32, CarbonModel* model)
{
  bool changed = assignValueWord(buf, index);
  doUpdateVHM(changed, model);
}

void CarbonVector8::fastDepositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRange(buf, range_msb, range_lsb, model);
}

CarbonStatus CarbonVector8::examine(UInt32* buf, UInt32* drive, ExamineMode, CarbonModel*) const
{

  if (buf)
  {
    const UInt64* vec = mVector;  
    CarbonValRW::cpSrcToDest(buf, vec, 2);
  }

  if (drive)
  {
    drive[0] = 0;
    drive[1] = 0;
  }
  return eCarbon_OK;
}

CarbonStatus
CarbonVector8::examine(CarbonReal* buf, UInt32*, ExamineMode, CarbonModel*) const
{
  if (buf)
  {
    const UInt64* vec = mVector;
    CarbonValRW::cpSrcToDest( buf, vec, 2 );
  }
  return eCarbon_OK;
}

CarbonStatus CarbonVector8::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode, CarbonModel* model) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, model);  
  if (stat == eCarbon_OK)
  {
    if (buf)
    {
      const UInt64* vec = mVector;
      CarbonValRW::cpSrcToDestWord(buf, vec, index);
    }
    if (drive)
      examineModelDriveWord(drive, index);
  }
  return stat;
}

CarbonStatus CarbonVector8::examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    const UInt64* vec = mVector;
    if (buf)
      CarbonValRW::cpSrcRangeToDest(buf, vec, index, length);
    if (drive)
      examineModelDriveRange(drive, index, length);
  }
  return stat;
}

CarbonStatus CarbonVector8::examineValXDriveWord(UInt32* val, UInt32* drv, int index) const
{
  return doExamineValXDriveWordDefaultArr(val, drv, index, mVector, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonVector8::update(ShellNet::Storage* shadow) const
{
  const UInt64* vec = mVector;
  doUpdate(shadow, vec, 2);
}
  
ShellNet::ValueState CarbonVector8::compare(const ShellNet::Storage shadow) const
{
  const UInt64* vec = mVector;
  return doComparePrim(shadow, vec);
}

ShellNet::ValueState CarbonVector8::writeIfNotEq(char* valueStr, size_t len, ShellNet::Storage* shadow,
                                                 NetFlags)
{
  const UInt64* vec = mVector;
  return doWriteIfNotEqPrim(valueStr, len, shadow, vec, (UInt64*) NULL, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

ShellNet::ValueState CarbonVector8::writeIfNotEqForce(char* valueStr, size_t len, 
                                                      ShellNet::Storage* shadow,
                                                      NetFlags, ShellNet* forceMask)
{
  const UInt64* vec = mVector;
  CarbonVector8* castForce = (CarbonVector8*) forceMask;
  const UInt64* forceMaskVec = castForce->getExamineStore();
  return doWriteIfNotEqPrim(valueStr, len, shadow, vec, forceMaskVec, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

CarbonStatus CarbonVector8::format(char* valueStr, size_t len, 
                                   CarbonRadix strFormat,
                                   NetFlags, CarbonModel* model) const
{
  const UInt64* vec = mVector;
  return valueFormatString(valueStr, len, strFormat, vec, (UInt64*) NULL, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()), model);
}

CarbonStatus CarbonVector8::formatForce(char* valueStr, size_t len, 
                                        CarbonRadix strFormat, NetFlags, ShellNet* forceMask, CarbonModel* model) const
{
  CarbonVector8* castForce = (CarbonVector8*) forceMask;
  const UInt64* forceMaskVec = castForce->getExamineStore();
  const UInt64* vec = mVector;
  return valueFormatString(valueStr, len, strFormat, vec, forceMaskVec, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()), model);
}

bool CarbonVector8::isDataNonZero() const
{
  const UInt64* vec = mVector;
  return (*vec != 0);
}

bool CarbonVector8::assignValue(const UInt32* buf)
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return sDoAssignInputLL(buf, mVector, bitWidth);
}

bool CarbonVector8::assignValueWord(UInt32 buf, int index)
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return sDoAssignInputWordLL(buf, mVector, index, bitWidth);
}

bool CarbonVector8::assignValueRange(const UInt32* buf, size_t index, size_t length)
{
  return doAssignInputRange(buf, mVector, index, length);
}

const UInt64* CarbonVector8::getExamineStore() const
{
  return mVector;
}

void CarbonVector8::examineModelDrive(UInt32* drive, ExamineMode) const
{
  UInt64 tmpDrive = 0;
  CarbonValRW::cpSrcToDest(drive, &tmpDrive, 2);
}

void CarbonVector8::examineModelDriveRange(UInt32* drive, size_t, size_t length) const
{
  size_t numLengthWords = sGetNumWords(length);
  CarbonValRW::setToZero(drive, numLengthWords);
}

void CarbonVector8::examineModelDriveWord(UInt32* drive, int) const
{
  *drive = 0;
}

ShellNet::Storage CarbonVector8::allocShadow() const
{
  UInt64* typedShadow = CARBON_ALLOC_VEC(UInt64, 1);
  ShellNet::Storage castShadow = static_cast<ShellNet::Storage>(typedShadow);
  update(&castShadow);
  return castShadow;
}
  
void CarbonVector8::freeShadow(ShellNet::Storage* shadow)
{
  UInt64* typedShadow = static_cast<UInt64*>(*shadow);
  CARBON_FREE_VEC(typedShadow, UInt64, 1);
  *shadow = NULL;
}

// CarbonVectorA
CarbonVectorA::CarbonVectorA(UInt32* vec) :
  mVector(vec)
{}


CarbonVectorA::~CarbonVectorA()
{}

void CarbonVectorA::setConstantBits()
{
  CarbonValRW::setConstantBits(mVector, NULL, mControlMask, mRange->getLength());
}

void CarbonVectorA::getTraits(Traits* traits) const
{
  traits->mWidth = mRange->getLength();
  traits->putLongPtr(mVector);
}

void CarbonVectorA::doDeposit(const UInt32* buf, CarbonModel* model)
{
  if (buf)
  {
    bool changed = assignValue(buf);
    doUpdateVHM(changed, model);
  }
}

CarbonStatus CarbonVectorA::deposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doDeposit(buf, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

CarbonStatus CarbonVectorA::doDepositWord(UInt32 buf, int index, CarbonModel* model)
{
  int numWords = getNumUInt32s();
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
  if (stat == eCarbon_OK)
  {
    bool changed = assignValueWord(buf, index);
    doUpdateVHM(changed, model);
  }
  return stat;
}

CarbonStatus CarbonVectorA::depositWord(UInt32 buf, int index, UInt32, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    return doDepositWord(buf, index, model);
  return eCarbon_ERROR;
}

CarbonStatus CarbonVectorA::doDepositRange(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;

  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    bool changed = assignValueRange(buf, index, length);
    doUpdateVHM(changed, model);
  }
  return stat;
}

CarbonStatus CarbonVectorA::depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    return doDepositRange(buf, range_msb, range_lsb, model);
  return eCarbon_ERROR;
}


void CarbonVectorA::fastDeposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  bool changed = assignValue(buf);
  doUpdateVHM(changed, model);
}

void CarbonVectorA::fastDepositWord(UInt32 buf, int index, UInt32, CarbonModel* model)
{
  bool changed = assignValueWord(buf, index);
  doUpdateVHM(changed, model);
}

void CarbonVectorA::fastDepositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRange(buf, range_msb, range_lsb, model);
}

CarbonStatus CarbonVectorA::examine(UInt32* buf, UInt32* drive, ExamineMode, CarbonModel*) const
{
  
  if (buf)
  {
    const UInt32* vec = mVector;
    CarbonValRW::cpSrcToDest(buf, vec, CNUMWORDS(mRange->getMsb(), mRange->getLsb()));
  }
  if (drive)
    CarbonValRW::setToZero(drive, CNUMWORDS(mRange->getMsb(), mRange->getLsb()));
  return eCarbon_OK;
}

CarbonStatus CarbonVectorA::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode, CarbonModel* model) const
{
  int numWords = getNumUInt32s();
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);  
  if (stat == eCarbon_OK)
  {
    if (buf)
    {
      const UInt32* vec = getExamineStore();
      CarbonValRW::cpSrcToDestWord(buf, vec, index);
    }
    if (drive)
      examineModelDriveWord(drive, index);
  }
  return stat;
}

CarbonStatus CarbonVectorA::examineRange(UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    const UInt32* vec = mVector;
    if (buf)
      CarbonValRW::cpSrcRangeToDest(buf, vec, index, length);
    if (drive)
      examineModelDriveRange(drive, index, length);
  }
  return stat;
}

CarbonStatus CarbonVectorA::examineValXDriveWord(UInt32* val, UInt32* drv, int index) const
{
  return doExamineValXDriveWordDefaultArr(val, drv, index, mVector, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

void CarbonVectorA::update(ShellNet::Storage* shadow) const
{
  const UInt32* vec = mVector;
  doUpdate(shadow, vec, getNumUInt32s());
}
  
ShellNet::ValueState CarbonVectorA::compare(const ShellNet::Storage shadow) const
{
  const UInt32* vec = mVector;
  return doCompare(shadow, vec, getNumUInt32s());
}

ShellNet::ValueState CarbonVectorA::writeIfNotEq(char* valueStr, size_t len, ShellNet::Storage* shadow,
                                                 NetFlags)
{
  const UInt32* vec = mVector;
  return sDoWriteIfNotEq(valueStr, len, shadow, vec, NULL, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}


ShellNet::ValueState CarbonVectorA::writeIfNotEqForce(char* valueStr, size_t len, 
                                                      ShellNet::Storage* shadow,
                                                      NetFlags, ShellNet* forceMask)
{
  const UInt32* vec = mVector;
  CarbonVectorA* castForce = (CarbonVectorA*) forceMask;
  const UInt32* forceMaskVec = castForce->getExamineStore();
  return sDoWriteIfNotEq(valueStr, len, shadow, vec, forceMaskVec, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
}

CarbonStatus CarbonVectorA::format(char* valueStr, size_t len, 
                                   CarbonRadix strFormat,
                                   NetFlags, CarbonModel* model) const
{
  const UInt32* vec = mVector;
  return valueFormatString(valueStr, len, strFormat, vec, (UInt32*) NULL, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()), model);
}

CarbonStatus CarbonVectorA::formatForce(char* valueStr, size_t len, 
                                        CarbonRadix strFormat, NetFlags, ShellNet* forceMask, CarbonModel* model) const
{
  CarbonVectorA* castForce = (CarbonVectorA*) forceMask;
  const UInt32* forceMaskVec = castForce->getExamineStore();
  const UInt32* vec = mVector;
  return valueFormatString(valueStr, len, strFormat, vec, forceMaskVec, mControlMask, CBITWIDTH(mRange->getMsb(), mRange->getLsb()), model);
}

bool CarbonVectorA::isDataNonZero() const
{
  const UInt32* vec = mVector;
  return ! CarbonValRW::isZero(vec, getNumUInt32s());
}

bool CarbonVectorA::assignValue(const UInt32* buf)
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return sDoAssignInputA(buf, mVector, bitWidth);
}

bool CarbonVectorA::assignValueWord(UInt32 buf, int index)
{
  int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  return sDoAssignInputWordA(buf, mVector, index, bitWidth);
}

bool CarbonVectorA::assignValueRange(const UInt32* buf, size_t index, 
                                          size_t length)
{
  return doAssignInputRange(buf, mVector, index, length);
}

const UInt32* CarbonVectorA::getExamineStore() const
{
  return mVector;
}

void CarbonVectorA::examineModelDrive(UInt32* drive, ExamineMode) const
{
  CarbonValRW::setToZero(drive, getNumUInt32s());
}

void CarbonVectorA::examineModelDriveRange(UInt32* drive, 
                                           size_t, 
                                           size_t length) const
{
  size_t numLengthWords = sGetNumWords(length);
  CarbonValRW::setToZero(drive, numLengthWords);
}

void CarbonVectorA::examineModelDriveWord(UInt32* drive, int) const
{
  *drive = 0;
}

ShellNet::Storage CarbonVectorA::allocShadow() const
{
  int numWords = getNumUInt32s();
  UInt32* typedShadow = CARBON_ALLOC_VEC(UInt32, numWords);
  ShellNet::Storage castShadow = static_cast<ShellNet::Storage>(typedShadow);
  update(&castShadow);
  return castShadow;
}
  
void CarbonVectorA::freeShadow(ShellNet::Storage* shadow)
{
  int numWords = getNumUInt32s();
  UInt32* typedShadow = static_cast<UInt32*>(*shadow);
  CARBON_FREE_VEC(typedShadow, UInt32, numWords);
  *shadow = NULL;
}

// CarbonVector1Input
CarbonVector1Input::CarbonVector1Input(UInt8* vec) :
  CarbonVector1(vec), mChangeArrayRef(NULL)
{}

CarbonVector1Input::CarbonVector1Input(SInt8* vec) :
  CarbonVector1(vec), mChangeArrayRef(NULL)
{}

CarbonVector1Input::~CarbonVector1Input()
{}

void CarbonVector1Input::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
   mChangeArrayRef = changeArrayRef;
}

CarbonChangeType* CarbonVector1Input::getChangeArrayRef()
{
  return mChangeArrayRef;
}

void CarbonVector1Input::calcChangeMask()
{
  if (mRange->getLength() == 1)
    *mChangeArrayRef = *mVector + 1;
  else
    *mChangeArrayRef = CARBON_CHANGE_MASK;
}

void CarbonVector1Input::recomputeChangeMask()
{
  calcChangeMask();
}

void CarbonVector1Input::doDepositInput(const UInt32* buf, CarbonModel* model)
{
  // CarbonVector1 can be 1 - 8 bits long. If it is 1 bit, treat the
  // change array like a scalar and set the value to RISE or FALL as
  // appropriate. Otherwise, set it to CHANGE, if changed. If not
  // changed, keep the same value as before.
  // Note,  too, that any extraneous 1's in the passed-in value are
  // zeroed.
  CarbonChangeType isInit = model->getHookup ()->getInit();
  const UInt32 bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  const UInt32 lengthOne = bitWidth == 1;
  const UInt32 lengthNotOne = bitWidth != 1;
  // This is either 3 or 1
  const UInt32 widthMode = (lengthOne + lengthNotOne * 0x3);

  // Zero the value.
  const UInt32 val = buf[0] & sGetMask(bitWidth);
  // If the length is one, then we calculate the shift needed for the
  // change reference if the val is 1. For a rising edge
  // we will need a b10 for the change reference so we would need to
  // shift the widthmode by 1 to get that.
  const UInt32 shift = lengthOne * val;
  const UInt32 isChanged = isInit | ((*mVector ^ val) != 0);
  const UInt32 isNotChanged = !isChanged;
  // Calculate the change array reference for this deposit only. 
  const CarbonChangeType localChange = (isChanged * widthMode) << shift;
  // The localChange may be 0 (unchanged) but this may be a double
  // deposit on the same net, and the first deposit did change.
  *mChangeArrayRef = (isChanged * localChange) + (isNotChanged * *mChangeArrayRef);
  *mVector = val;

  doUpdateVHM(isChanged != 0, model);
}

CarbonStatus CarbonVector1Input::doDepositWordInput(UInt32 buf, int index, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
    doDepositInput(&buf, model);
  return stat;
}

CarbonStatus CarbonVector1Input::deposit(const UInt32* buf,
                                         const UInt32* drive, 
                                         CarbonModel* model)
{
  if (buf)
    doDepositInput(buf, model);
  if (drive && (checkIfDriveSet(drive, model) != eCarbon_OK))
    return eCarbon_ERROR;
  return eCarbon_OK;
}

CarbonStatus CarbonVector1Input::depositWord (UInt32 buf, int index, 
                                              UInt32 drive, CarbonModel* model)
{
  CarbonStatus stat = doDepositWordInput(buf, index, model);
  if (drive != 0)
  {
    ShellGlobal::reportSetDriveOnNonTristate(getNameAsLeaf(), model);
    stat = eCarbon_ERROR;
  }
  return stat;
}

CarbonStatus CarbonVector1Input::doDepositRangeInput(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;

  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    bool changed = assignValueRange(buf, index, length);
    doUpdateVHM(changed, model);
    if (changed)
      calcChangeMask();
  }
  return stat;
}


CarbonStatus CarbonVector1Input::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{

  CarbonStatus stat = doDepositRangeInput(buf, range_msb, range_lsb, model);
  
  if (drive && (checkIfDriveSetRange(drive, range_msb, range_lsb, model) != eCarbon_OK))
    stat = eCarbon_ERROR;
  return stat;
}


void CarbonVector1Input::fastDeposit(const UInt32* buf,
                                     const UInt32*, 
                                     CarbonModel* model)
{
  doDepositInput(buf, model);
}

void CarbonVector1Input::fastDepositWord(UInt32 buf, int,
                                         UInt32, CarbonModel* model)
{
  doDepositInput(&buf, model);
}

void CarbonVector1Input::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRangeInput(buf, range_msb, range_lsb, model);
}


void CarbonVector1Input::getTraits(Traits* traits) const
{
  CarbonVector1::getTraits(traits);
  traits->mHasInputSemantics = true;
}

bool CarbonVector1Input::isInput() const
{
  return true;
}
  
CarbonStatus CarbonVector1Input::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  if (buf)
    *buf = *mVector;
  if (drive)
  {
    switch (mode)
    {
    case eIDrive:
      sDoAllOnes(drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
      break;
    case eCalcDrive:
    case eXDrive:
      *drive = 0;
      break;
    }
  }
  return eCarbon_OK;
}

void CarbonVector1Input::examineModelDrive(UInt32* drive, ExamineMode mode) const
{
  switch (mode)
  {
  case eIDrive:
    sDoAllOnes(drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
    break;
  case eCalcDrive:
  case eXDrive:
    *drive = 0;
    break;
  }
}

void CarbonVector1Input::examineModelDriveRange(UInt32* drive, size_t, size_t length) const
{
  sDoAllOnes(drive, length);
}


void CarbonVector1Input::getExternalDrive(UInt32* drive) const
{
  if (drive)
    sDoAllZeros(drive, mRange->getLength());
}

// CarbonVector2Input
CarbonVector2Input::CarbonVector2Input(UInt16* vec) :
  CarbonVector2(vec), mChangeArrayRef(NULL)
{}

CarbonVector2Input::CarbonVector2Input(SInt16* vec) :
  CarbonVector2(vec), mChangeArrayRef(NULL)
{}

CarbonVector2Input::~CarbonVector2Input()
{}

void CarbonVector2Input::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  mChangeArrayRef = changeArrayRef;
}

CarbonChangeType* CarbonVector2Input::getChangeArrayRef()
{
  return mChangeArrayRef;
}


void CarbonVector2Input::doDepositInput(const UInt32* buf, CarbonModel* model)
{
  // If changed, set the change array reference to CHANGE, otherwise
  // keep the same value in the change 
  // array. Needed in case carbonDeposit is called twice with the same
  // value before carbonSchedule is called.
  // Note, too, that any extraneous 1's in the passed-in value are
  // set to zero.
  CarbonChangeType isInit = model->getHookup ()->getInit ();
  UInt32 val = buf[0] & sGetMask(CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
  const UInt32 isChanged = (isInit | ((*mVector ^ val) != 0)) * CARBON_CHANGE_MASK;
  const UInt32 isNotChanged = !isChanged;
  *mChangeArrayRef = isChanged + isNotChanged * (*mChangeArrayRef);
  *mVector = val;

  doUpdateVHM(isChanged != 0, model);
}

CarbonStatus CarbonVector2Input::doDepositWordInput(UInt32 buf, int index, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
    doDepositInput(&buf, model);
  return stat;
}


CarbonStatus CarbonVector2Input::deposit(const UInt32* buf,
                                         const UInt32* drive, 
                                         CarbonModel* model)
{
  if (buf)
    doDepositInput(buf, model);
  if (drive && (checkIfDriveSet(drive, model) != eCarbon_OK))
    return eCarbon_ERROR;
  return eCarbon_OK;
}

CarbonStatus CarbonVector2Input::depositWord (UInt32 buf, int index, 
                                              UInt32 drive, 
                                              CarbonModel* model)
{
  CarbonStatus stat = doDepositWordInput(buf, index, model);
  
  if (drive != 0)
  {
    ShellGlobal::reportSetDriveOnNonTristate(getNameAsLeaf(), model);
    stat = eCarbon_ERROR;
  }
  return stat;
}

CarbonStatus CarbonVector2Input::doDepositRangeInput(const UInt32* buf, int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    bool changed = assignValueRange(buf, index, length);
    doUpdateVHM(changed, model);
    if (changed)
      *mChangeArrayRef = CARBON_CHANGE_MASK;
  }
  return stat;
}

CarbonStatus CarbonVector2Input::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonStatus stat = doDepositRangeInput(buf, range_msb, range_lsb, model);
  if (drive && (checkIfDriveSetRange(drive, range_msb, range_lsb, model) != eCarbon_OK))
    stat = eCarbon_ERROR;
  return stat;
}


void CarbonVector2Input::fastDeposit(const UInt32* buf,
                                     const UInt32*, 
                                     CarbonModel* model)
{
  doDepositInput(buf, model);
}

void CarbonVector2Input::fastDepositWord(UInt32 buf, int,
                                         UInt32, CarbonModel* model)
{
  doDepositInput(&buf, model);
}

void CarbonVector2Input::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRangeInput(buf, range_msb, range_lsb, model);
}

void CarbonVector2Input::getTraits(Traits* traits) const
{
  CarbonVector2::getTraits(traits);
  traits->mHasInputSemantics = true;
}

bool CarbonVector2Input::isInput() const
{
  return true;
}

CarbonStatus CarbonVector2Input::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{

  if (buf)
    *buf = *mVector;

  if (drive)
  {
    switch (mode)
    {
    case eIDrive:
      sDoAllOnes(drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
      break;
    case eCalcDrive:
    case eXDrive:
      *drive = 0;
      break;
    }
  }
  return eCarbon_OK;
}

void CarbonVector2Input::examineModelDrive(UInt32* drive, 
                                           ExamineMode mode) const
{
  switch (mode)
  {
  case eIDrive:
    sDoAllOnes(drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
    break;
  case eCalcDrive:
  case eXDrive:
    *drive = 0;
    break;
  }
}

void CarbonVector2Input::examineModelDriveRange(UInt32* drive, size_t, size_t length) const
{
  sDoAllOnes(drive, length);
}

void CarbonVector2Input::getExternalDrive(UInt32* drive) const
{
  if (drive)
    sDoAllZeros(drive, mRange->getLength());
}

// CarbonVector4Input
CarbonVector4Input::CarbonVector4Input(UInt32* vec) :
  CarbonVector4(vec), mChangeArrayRef(NULL)
{}

CarbonVector4Input::CarbonVector4Input(SInt32* vec) :
  CarbonVector4(vec), mChangeArrayRef(NULL)
{}

CarbonVector4Input::~CarbonVector4Input()
{}

void CarbonVector4Input::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  mChangeArrayRef = changeArrayRef;
}

CarbonChangeType* CarbonVector4Input::getChangeArrayRef()
{
  return mChangeArrayRef;
}

void CarbonVector4Input::doDepositInput(const UInt32* buf, CarbonModel* model)
{
  // If changed, set the change array reference to CHANGE, otherwise
  // keep the same value in the change 
  // array. Needed in case carbonDeposit is called twice with the same
  // value before carbonSchedule is called.
  // Note, too, that any extraneous 1's in the passed-in value are
  // set to zero.
  CarbonChangeType isInit = model->getHookup ()->getInit ();
  UInt32 val = buf[0] & sGetMask(CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
  const UInt32 isChanged = (isInit | ((*mVector ^ val) != 0)) * CARBON_CHANGE_MASK;
  const UInt32 isNotChanged = !isChanged;
  *mChangeArrayRef = isChanged + isNotChanged * (*mChangeArrayRef);
  *mVector = val;

  doUpdateVHM(isChanged != 0, model);
}

CarbonStatus CarbonVector4Input::doDepositWordInput(UInt32 buf, int index, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
    doDepositInput(&buf, model);
  return stat;
}

CarbonStatus CarbonVector4Input::deposit(const UInt32* buf,
                                         const UInt32* drive, 
                                         CarbonModel* model)
{
  if (buf)
    doDepositInput(buf, model);
  if (drive && (checkIfDriveSet(drive, model) != eCarbon_OK))
    return eCarbon_ERROR;
  return eCarbon_OK;
}

CarbonStatus CarbonVector4Input::depositWord (UInt32 buf, int index,
                                              UInt32 drive, CarbonModel* model)
{
  CarbonStatus stat = doDepositWordInput(buf, index, model);
  if (drive != 0)
  {
    ShellGlobal::reportSetDriveOnNonTristate(getNameAsLeaf(), model);
    stat = eCarbon_ERROR;
  }
  return stat;
}

CarbonStatus CarbonVector4Input::doDepositRangeInput(const UInt32* buf, int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    bool changed = assignValueRange(buf, index, length);
    doUpdateVHM(changed, model);
    if (changed)
      *mChangeArrayRef = CARBON_CHANGE_MASK;
  }
  return stat;
}

CarbonStatus CarbonVector4Input::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonStatus stat = doDepositRangeInput(buf, range_msb, range_lsb, model);
  if (drive && (checkIfDriveSetRange(drive, range_msb, range_lsb, model) != eCarbon_OK))
    stat = eCarbon_ERROR;
  return stat;
}


void CarbonVector4Input::fastDeposit(const UInt32* buf,
                                     const UInt32*, 
                                     CarbonModel* model)
{
  doDepositInput(buf, model);
}

void CarbonVector4Input::fastDepositWord(UInt32 buf, int,
                                         UInt32, CarbonModel* model)
{
  doDepositInput(&buf, model);
}

void CarbonVector4Input::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRangeInput(buf, range_msb, range_lsb, model);
}

void CarbonVector4Input::getTraits(Traits* traits) const
{
  CarbonVector4::getTraits(traits);
  traits->mHasInputSemantics = true;
}

bool CarbonVector4Input::isInput() const
{
  return true;
}
  
CarbonStatus CarbonVector4Input::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{

  if (buf)
    *buf = *mVector;

  if (drive)
  {
    switch (mode)
    {
    case eIDrive:
      sDoAllOnes(drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
      break;
    case eCalcDrive:
    case eXDrive:
      *drive = 0;
      break;
    }
  }
  return eCarbon_OK;
}

void CarbonVector4Input::examineModelDrive(UInt32* drive, 
                                           ExamineMode mode) const
{
  switch (mode)
  {
  case eIDrive:
    sDoAllOnes(drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
    break;
  case eCalcDrive:
  case eXDrive:
    *drive = 0;
    break;
  }
}

void CarbonVector4Input::examineModelDriveRange(UInt32* drive, size_t, size_t length) const
{
  sDoAllOnes(drive, length);
}

void CarbonVector4Input::getExternalDrive(UInt32* drive) const
{
  if (drive)
    sDoAllZeros(drive, mRange->getLength());
}

// CarbonVector8Input
CarbonVector8Input::CarbonVector8Input(UInt64* vec) :
  CarbonVector8(vec), mChangeArrayRef(NULL)
{}

CarbonVector8Input::CarbonVector8Input(SInt64* vec) :
  CarbonVector8(vec), mChangeArrayRef(NULL)
{}

CarbonVector8Input::CarbonVector8Input(CarbonReal* vec) :
  CarbonVector8(vec), mChangeArrayRef(NULL)
{}

CarbonVector8Input::~CarbonVector8Input()
{}

void CarbonVector8Input::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  mChangeArrayRef = changeArrayRef;
}

CarbonChangeType* CarbonVector8Input::getChangeArrayRef()
{
  return mChangeArrayRef;
}

void CarbonVector8Input::doDepositInput(const UInt32* buf, CarbonModel* model)
{
  // If changed, set the change array reference to CHANGE, otherwise
  // keep the same value in the change 
  // array. Needed in case carbonDeposit is called twice with the same
  // value before carbonSchedule is called.
  // Note, too, that any extraneous 1's in the passed-in value are
  // set to zero.

  CarbonChangeType isInit = model->getHookup ()->getInit ();
  UInt32 highWord = buf[1];
  highWord &= sGetMask(CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
  UInt64 val = highWord;
  val <<= 32;
  val += buf[0];

  const UInt32 isChanged = (isInit | ((*mVector ^ val) != 0)) * CARBON_CHANGE_MASK;
  const UInt32 isNotChanged = !isChanged;
  *mChangeArrayRef = isChanged + isNotChanged * (*mChangeArrayRef);
  *mVector = val;

  doUpdateVHM(isChanged != 0, model);
}

CarbonStatus CarbonVector8Input::doDepositWordInput(UInt32 buf, int index, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, model);
  if (stat == eCarbon_OK)
  {
    UInt32 localBuf[2];
    localBuf[1] = (*mVector) >> 32;
    localBuf[0] = *mVector;
    localBuf[index] = buf;
    doDepositInput(localBuf, model);
  }
  return stat;
}

CarbonStatus CarbonVector8Input::deposit(const UInt32* buf,
                                         const UInt32* drive, 
                                         CarbonModel* model)
{
  if (buf)
    doDepositInput(buf, model);
  if (drive && (checkIfDriveSet(drive, model) != eCarbon_OK))
    return eCarbon_ERROR;
  return eCarbon_OK;
}

CarbonStatus CarbonVector8Input::depositWord (UInt32 buf, int index,
                                              UInt32 drive, CarbonModel* model)
{
  CarbonStatus stat = doDepositWordInput(buf, index, model);
  if (drive != 0)
  {
    ShellGlobal::reportSetDriveOnNonTristate(getNameAsLeaf(), model);
    stat = eCarbon_ERROR;
  }
  return stat;
}


CarbonStatus CarbonVector8Input::doDepositRangeInput(const UInt32* buf, int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    bool changed = assignValueRange(buf, index, length);
    doUpdateVHM(changed, model);
    if (changed)
      *mChangeArrayRef = CARBON_CHANGE_MASK;
  }
  return stat;
}

CarbonStatus CarbonVector8Input::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonStatus stat = doDepositRangeInput(buf, range_msb, range_lsb, model);
  if (drive && (checkIfDriveSetRange(drive, range_msb, range_lsb, model) != eCarbon_OK))
    stat = eCarbon_ERROR;
  return stat;
}


void CarbonVector8Input::fastDeposit(const UInt32* buf,
                                     const UInt32*, 
                                     CarbonModel* model)
{
  doDepositInput(buf, model);
}

void CarbonVector8Input::fastDepositWord(UInt32 buf, int index,
                                         UInt32, CarbonModel* model)
{
  doDepositWordInput(buf, index, model);
}

void CarbonVector8Input::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRangeInput(buf, range_msb, range_lsb, model);
}

void CarbonVector8Input::getTraits(Traits* traits) const
{
  CarbonVector8::getTraits(traits);
  traits->mHasInputSemantics = true;
}

bool CarbonVector8Input::isInput() const
{
  return true;
}

CarbonStatus CarbonVector8Input::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  if (buf)
  {
    const UInt64* vec = mVector;
    CarbonValRW::cpSrcToDest(buf, vec, 2);
  }

  if (drive)
  {
    switch (mode)
    {
    case eIDrive:
      sDoAllOnes(drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
      break;
    case eCalcDrive:
    case eXDrive:
      drive[0] = 0;
      drive[1] = 0;
      break;
    }
  }
  return eCarbon_OK;
}

CarbonStatus CarbonVector8Input::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 1, model);  
  if (stat == eCarbon_OK)
  {
    if (buf)
      CarbonValRW::cpSrcToDestWord(buf, mVector, index);

    if (drive)
    {
      switch (mode)
      {
      case eIDrive:
        *drive = 0;
        *drive = ~*drive;
        if (index == 1)
          *drive &= CarbonValRW::getWordMask(CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
        break;
      case eCalcDrive:
      case eXDrive:
        *drive = 0;
        break;
      }
    }
  }
  return eCarbon_OK;
}

void CarbonVector8Input::examineModelDrive(UInt32* drive, 
                                           ExamineMode mode) const
{
  switch (mode)
  {
  case eIDrive:
    sDoAllOnes(drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
    break;
  case eCalcDrive:
  case eXDrive:
    CarbonValRW::setToZero(drive, 2);
    break;
  }
}

void CarbonVector8Input::examineModelDriveWord(UInt32* drive, int index) const
{
  CarbonValRW::setToOnes(drive, 1);  
  if (index == 1)
  {
    int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
    *drive &= CarbonValRW::getWordMask(UInt32(bitWidth));
  }    
}

void CarbonVector8Input::examineModelDriveRange(UInt32* drive, size_t, size_t length) const
{
  sDoAllOnes(drive, length);
}

void CarbonVector8Input::getExternalDrive(UInt32* drive) const
{
  if (drive)
    sDoAllZeros(drive, mRange->getLength());
}

// CarbonVectorAInput
CarbonVectorAInput::CarbonVectorAInput(UInt32* vec) :
  CarbonVectorA(vec), mChangeArrayRef(NULL)
{}

CarbonVectorAInput::~CarbonVectorAInput()
{}

void CarbonVectorAInput::putChangeArrayRef(CarbonChangeType* changeArrayRef)
{
  mChangeArrayRef = changeArrayRef;
}

CarbonChangeType* CarbonVectorAInput::getChangeArrayRef()
{
  return mChangeArrayRef;
}

void CarbonVectorAInput::doDepositInput(const UInt32* buf, CarbonModel* model)
{
  // If changed, set the change array reference to CHANGE, otherwise
  // keep the same value in the change 
  // array. Needed in case carbonDeposit is called twice with the same
  // value before carbonSchedule is called.
  // Note, too, that any extraneous 1's in the passed-in value are
  // set to zero.

  CarbonChangeType isInit = model->getHookup ()->getInit ();
  UInt32 numWords = CNUMWORDS(mRange->getMsb(), mRange->getLsb());
  CarbonChangeType localChange = CARBON_CHANGE_NONE_MASK | isInit;
  for (UInt32 i = 0; i < numWords - 1; ++i)
  {
    UInt32 val = buf[i];
    localChange |= ((mVector[i] ^ val) != 0) * CARBON_CHANGE_MASK;
    mVector[i] = val;
  }
  UInt32 highWord = buf[numWords - 1];
  highWord &= sGetMask(CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
  localChange |= ((mVector[numWords - 1] ^ highWord) != 0) * CARBON_CHANGE_MASK;
  mVector[numWords - 1] = highWord;
  const UInt32 notChanged = !localChange;
  *mChangeArrayRef = localChange + notChanged * (*mChangeArrayRef);

  doUpdateVHM(localChange != CARBON_CHANGE_NONE_MASK, model);
}

CarbonStatus CarbonVectorAInput::doDepositWordInput(UInt32 buf, int index, CarbonModel* model)
{
  UInt32 numWords = CNUMWORDS(mRange->getMsb(), mRange->getLsb());
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
  if (stat == eCarbon_OK)
  {
    CarbonChangeType isInit = model->getHookup ()->getInit ();
    const int lastWord = ((unsigned)index == (numWords - 1));
    const int notLastWord = ! lastWord;
    buf = lastWord * (buf & sGetMask(CBITWIDTH(mRange->getMsb(), mRange->getLsb()))) + notLastWord * buf;
    
    const UInt32 isChanged = (isInit | ((mVector[index] ^ buf) != 0)) * CARBON_CHANGE_MASK;
    const UInt32 isNotChanged = !isChanged;
    *mChangeArrayRef = isChanged + isNotChanged * (*mChangeArrayRef);
    mVector[index] = buf;

    doUpdateVHM(isChanged != 0, model);
  }
  return stat;
}

CarbonStatus CarbonVectorAInput::deposit(const UInt32* buf,
                                         const UInt32* drive, 
                                         CarbonModel* model)
{
  if (buf)
    doDepositInput(buf, model);
  if (drive && (checkIfDriveSet(drive, model) != eCarbon_OK))
    return eCarbon_ERROR;
  return eCarbon_OK;
}

CarbonStatus CarbonVectorAInput::depositWord (UInt32 buf, int index,
                                              UInt32 drive, CarbonModel* model)
{
  CarbonStatus stat = doDepositWordInput(buf, index, model);
  
  if (drive != 0)
  {
    ShellGlobal::reportSetDriveOnNonTristate(getNameAsLeaf(), model);
    stat = eCarbon_ERROR;
  }
  return stat;
}

CarbonStatus CarbonVectorAInput::doDepositRangeInput(const UInt32 *buf, int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    bool changed = assignValueRange(buf, index, length);
    doUpdateVHM(changed, model);
    if (changed)
      *mChangeArrayRef = CARBON_CHANGE_MASK;
  }
  return stat;
}

CarbonStatus CarbonVectorAInput::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonStatus stat = doDepositRangeInput(buf, range_msb, range_lsb, model);
  if (drive && (checkIfDriveSetRange(drive, range_msb, range_lsb, model) != eCarbon_OK))
    stat = eCarbon_ERROR;
  return stat;
}

void CarbonVectorAInput::fastDeposit(const UInt32* buf,
                                     const UInt32*, 
                                     CarbonModel* model)
{
  doDepositInput(buf, model);
}

void CarbonVectorAInput::fastDepositWord(UInt32 buf, int index,
                                         UInt32, CarbonModel* model)
{
  doDepositWordInput(buf, index, model);
}

void CarbonVectorAInput::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRangeInput(buf, range_msb, range_lsb, model);
}

void CarbonVectorAInput::getTraits(Traits* traits) const
{
  CarbonVectorA::getTraits(traits);
  traits->mHasInputSemantics = true;
}

bool CarbonVectorAInput::isInput() const
{
  return true;
}

CarbonStatus CarbonVectorAInput::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  if (buf)
  {
    CarbonValRW::cpSrcToDest(buf, mVector, CNUMWORDS(mRange->getMsb(), mRange->getLsb()));
  }
  
  if (drive)
  {
    switch (mode)
    {
    case eIDrive:
      sDoAllOnes(drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
      break;
    case eCalcDrive:
    case eXDrive:
      CarbonValRW::setToZero(drive, CNUMWORDS(mRange->getMsb(), mRange->getLsb()));
      break;
    }
  }
  return eCarbon_OK;
}


CarbonStatus CarbonVectorAInput::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  int numWords = getNumUInt32s();
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);  
  if (stat == eCarbon_OK)
  {
    if (buf)
    {
      const UInt32* vec = getExamineStore();
      CarbonValRW::cpSrcToDestWord(buf, vec, index);
    }
    if (drive)
    {
      switch (mode)
      {
      case eIDrive:
        *drive = 0;
        *drive = ~*drive;
        if (index == numWords - 1)
          *drive &= CarbonValRW::getWordMask(CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
        break;
      case eCalcDrive:
      case eXDrive:
        *drive = 0;
        break;
      }
    }
  }
  return stat;
}

void CarbonVectorAInput::examineModelDrive(UInt32* drive, 
                                           ExamineMode mode) const
{
  switch (mode)
  {
  case eIDrive:
    sDoAllOnes(drive, CBITWIDTH(mRange->getMsb(), mRange->getLsb()));
    break;
  case eCalcDrive:
  case eXDrive:
    CarbonValRW::setToZero(drive, getNumUInt32s());
    break;
  }
}

void CarbonVectorAInput::examineModelDriveWord(UInt32* drive, int index) const
{
  CarbonValRW::setToOnes(drive, 1);  

  int numWords = getNumUInt32s();
  if (index == numWords - 1)
  {
    int bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
    *drive &= CarbonValRW::getWordMask(UInt32(bitWidth));
  }    
}

void CarbonVectorAInput::examineModelDriveRange(UInt32* drive, size_t, size_t length) const
{
  sDoAllOnes(drive, length);
}

void CarbonVectorAInput::getExternalDrive(UInt32* drive) const
{
  if (drive)
    sDoAllZeros(drive, mRange->getLength());
}

// very efficient sign-extend that does no branching.
void CarbonVectorA::signExtend()
{
  const UInt32 numWords = CNUMWORDS(mRange->getMsb(), mRange->getLsb());
  const UInt32 bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());

  CarbonValRW::signExtendVector(mVector, numWords, bitWidth);
}

void CarbonVectorA::sanitize(UInt32* buf) const
{
  const UInt32 numWords = CNUMWORDS(mRange->getMsb(), mRange->getLsb());
  const UInt32 bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
  buf[numWords - 1] &= CarbonValRW::getWordMask(bitWidth);
}

void CarbonVectorA::sanitizeWord(UInt32* buf, UInt32 index) const
{
  const UInt32 numWords = CNUMWORDS(mRange->getMsb(), mRange->getLsb());
  if (index == numWords - 1)
  {
    const UInt32 bitWidth = CBITWIDTH(mRange->getMsb(), mRange->getLsb());
    *buf &= CarbonValRW::getWordMask(bitWidth);
  }
}

CarbonVectorAS::CarbonVectorAS(UInt32* vec)
  : CarbonVectorA(vec)
{}

CarbonVectorAS::~CarbonVectorAS()
{}

CarbonStatus CarbonVectorAS::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = CarbonVectorA::examine(buf, drive, mode, model);
  if (stat == eCarbon_OK && buf)
    // don't need to sanitize drive on a non-tristate
    sanitize(buf);
  return stat;
}

CarbonStatus CarbonVectorAS::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = CarbonVectorA::examineWord(buf, index, drive, mode, model);
  if (stat == eCarbon_OK && buf)
    // don't need to sanitize drive on a non-tristate
    sanitizeWord(buf, index);
  return stat;
}

CarbonStatus CarbonVectorAS::deposit(const UInt32* buf, const UInt32* drv, CarbonModel* model)
{
  CarbonStatus stat = CarbonVectorA::deposit(buf, drv, model);
  if (stat == eCarbon_OK)
    signExtend();
  return stat;
}
  
CarbonStatus CarbonVectorAS::depositWord(UInt32 buf, int index, UInt32 drv, CarbonModel* model)
{
  CarbonStatus stat = CarbonVectorA::depositWord(buf, index, drv, model);  
  if (stat == eCarbon_OK)
    signExtend();
  return stat;
}

CarbonStatus CarbonVectorAS::depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32* drv, CarbonModel* model)
{
  CarbonStatus stat = CarbonVectorA::depositRange(buf, range_msb, range_lsb, drv, model);  
  if (stat == eCarbon_OK)
    signExtend();
  return stat;
}

void CarbonVectorAS::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  CarbonVectorA::fastDeposit(buf, drive, model);
  signExtend();
}

void CarbonVectorAS::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonVectorA::fastDepositWord(buf, index, drive, model);
  signExtend();
}

void CarbonVectorAS::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonVectorA::fastDepositRange(buf, range_msb, range_lsb, drive, model);
  signExtend();
}

CarbonVectorASInput::CarbonVectorASInput(UInt32* vec)
  : CarbonVectorAInput(vec)
{}

CarbonVectorASInput::~CarbonVectorASInput()
{}

CarbonStatus CarbonVectorASInput::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = CarbonVectorAInput::examine(buf, drive, mode, model);
  if (stat == eCarbon_OK && buf)
    sanitize(buf);
  return stat;
}

CarbonStatus CarbonVectorASInput::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  CarbonStatus stat = CarbonVectorAInput::examineWord(buf, index, drive, mode, model);
  if (stat == eCarbon_OK && buf)
    sanitizeWord(buf, index);
  return stat;
}

CarbonStatus CarbonVectorASInput::deposit(const UInt32* buf, const UInt32* drv, CarbonModel* model)
{
  CarbonStatus stat = CarbonVectorAInput::deposit(buf, drv, model);
  if (stat == eCarbon_OK)
    signExtend();
  return stat;
}
  
CarbonStatus CarbonVectorASInput::depositWord(UInt32 buf, int index, UInt32 drv, CarbonModel* model)
{
  CarbonStatus stat = CarbonVectorAInput::depositWord(buf, index, drv, model);  
  if (stat == eCarbon_OK)
    signExtend();
  return stat;
}

CarbonStatus CarbonVectorASInput::depositRange(const UInt32 *buf, int range_msb, int range_lsb, const UInt32* drv, CarbonModel* model)
{
  CarbonStatus stat = CarbonVectorAInput::depositRange(buf, range_msb, range_lsb, drv, model);  
  if (stat == eCarbon_OK)
    signExtend();
  return stat;
}

void CarbonVectorASInput::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  CarbonVectorAInput::fastDeposit(buf, drive, model);
  signExtend();
}

void CarbonVectorASInput::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonVectorAInput::fastDepositWord(buf, index, drive, model);
  signExtend();
}

void CarbonVectorASInput::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  CarbonVectorAInput::fastDepositRange(buf, range_msb, range_lsb, drive, model);
  signExtend();
}


