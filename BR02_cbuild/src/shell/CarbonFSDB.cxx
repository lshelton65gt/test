// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/CarbonWaveImp.h"
#include "shell/carbon_capi.h"
#include "shell/FsdbFile.h"

#include "shell/CarbonModel.h"
#include "carbon_capi_priv.h"

CarbonWave* carbonWaveInitFSDB(CarbonObjectID* closure, 
                               const char* fileName, 
                               CarbonTimescale timescale)
{
  CarbonModel *context = closure->getModel ();
  CarbonStatus stat = gCarbonCheckModelInit(closure);
  CarbonWave* wave = NULL;
  if (stat == eCarbon_OK)
  {
    UtString errMsg;
    MsgContext* msgContext = context->getMsgContext();
    FsdbFile* dataFile = FsdbFile::open(fileName, timescale, &errMsg, msgContext);
    wave = context->waveCreate(dataFile, &errMsg);
  }
  return wave;
}

CarbonWave* carbonWaveInitFSDBAutoSwitch(CarbonObjectID *closure,
                                         const char* fileNamePrefix, 
                                         CarbonTimescale timescale,
                                         unsigned int limitMegs,
                                         unsigned int maxFiles)
{
  CarbonModel *context = closure != NULL ? closure->getModel () : NULL;
  CarbonStatus stat = gCarbonCheckModelInit(closure);
  CarbonWave* wave = NULL;
  if (stat == eCarbon_OK)
  {
    UtString errMsg;
    MsgContext* msgContext = context->getMsgContext();
    FsdbFile* dataFile = FsdbFile::openAutoSwitch(fileNamePrefix, timescale, limitMegs, maxFiles, msgContext, &errMsg);
    wave = context->waveCreate(dataFile, &errMsg);
  }
  return wave;
}


CarbonStatus carbonWaveSwitchFSDBFile(CarbonWave* wave, const char* fileName)
{
// Can't get this to work yet and don't want to put this in
// carbon_capi.cxx because it causes us to include FsdbFile.h. Not
// sure if there was a reason to split out the above two functions,
// but I'm trying to respect it.
//
//  REGISTER_API(carbonWaveSwitchFSDBFile, GET_MODEL(wave));

  CarbonStatus ret = eCarbon_ERROR;
  if (wave != NULL) {
    ret = wave->switchFSDBFile(fileName);
  }
  return ret;
}

