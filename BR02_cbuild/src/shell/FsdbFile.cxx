// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "shell/FsdbFile.h"
#include "shell/CarbonWaveUserData.h"
#include "util/ShellMsgContext.h"

#include "util/UtConv.h"
#include "util/OSWrapper.h"
#include "util/UtHashMap.h"
#include "util/Loop.h"
#include "util/StringAtom.h"

#include "iodb/IODBUserTypes.h"

static void sMakeAllX(char*, SInt32);

class FsdbFile::AllVals
{
  typedef UtArray<double*> DblList;
  typedef Loop<DblList> DblListLoop;
  typedef UtArray<char*> CharList;
  typedef Loop<CharList> CharListLoop;
  typedef UtArray<SInt32*> IntList;
  typedef Loop<IntList> IntListLoop;
  typedef UtArray<SInt64*> Int64List;
  typedef Loop<Int64List> Int64ListLoop;

public: CARBONMEM_OVERRIDES
  AllVals() {}
  ~AllVals() {
    for (DblListLoop d(mDbls); 
         ! d.atEnd(); ++d)
      carbonmem_dealloc(*d, sizeof(double));

    for (IntListLoop i(mInts);
         ! i.atEnd(); ++i)
      carbonmem_dealloc(*i, sizeof(SInt32));

    for (Int64ListLoop i(mInt64s);
         ! i.atEnd(); ++i)
      carbonmem_dealloc(*i, sizeof(SInt64));


    for (CharListLoop c(mArrs);
         ! c.atEnd(); ++c)
      carbonmem_free(*c);
  }

  double* getNewDbl() {
    double* dbl = (double*) carbonmem_alloc(sizeof(double));
    mDbls.push_back(dbl);
    *dbl = 0.0;
    return dbl;
  }

  SInt32* getNewInt() {
    SInt32* word = (SInt32*) carbonmem_alloc(sizeof(SInt32));
    mInts.push_back(word);
    *word = 0;
    return word;
  }

  SInt64* getNewInt64() {
    SInt64* word = (SInt64*) carbonmem_alloc(sizeof(SInt64));
    mInt64s.push_back(word);
    *word = 0;
    return word;
  }

  char* getNewByteArr(SInt32 size)
  {
    char* arr = (char*) carbonmem_malloc(size);
    mArrs.push_back(arr);
    // make all fsdb x
    sMakeAllX(arr, size);
    return arr;
  }

  DblList mDbls;
  CharList mArrs;
  IntList mInts;
  Int64List mInt64s;
};

class FsdbFile::UserDataMap
{
public:
  CARBONMEM_OVERRIDES

  typedef UtHashMap<CarbonClientData, CarbonWaveUserData*> DataToID;
  DataToID mHash;
};

static void sMakeAllX(char* arr, SInt32 size)
{
  for (SInt32 i = 0; i < size; ++i)
    arr[i] = FSDB_BT_VCD_X;
}

static fsdbScopeType sGetFsdbScopeType(WaveScope* scope)
{
  fsdbScopeType ftype = FSDB_ST_VCD_MODULE;
  CarbonLanguageType lang = scope->getScopeLanguage();
  switch (scope->getType())
  {
  case WaveScope::eModule:
  {
    if(lang == eLanguageTypeVhdl)
      ftype = FSDB_ST_VHDL_ARCHITECTURE;
    else
      ftype = FSDB_ST_VCD_MODULE;
    break;
  }
  case WaveScope::eTask:
    ftype = FSDB_ST_VCD_TASK;
    break;
  case WaveScope::eFunction:
    ftype = FSDB_ST_VCD_FUNCTION;
    break;
  case WaveScope::eBegin:
    ftype = FSDB_ST_VCD_BEGIN;
    break;
  case WaveScope::eFork:
    ftype = FSDB_ST_VCD_FORK;
    break;
  case WaveScope::eStruct:
    ftype = FSDB_ST_VHDL_RECORD;
    break;
  case WaveScope::eArray:
    // It's okay to let the scope type for array to be invalid.
    // Arrays become groups and not scopes in fsdb file.
    break;
  }
  return ftype;
}

static fsdbVarType sGetFsdbVarType(CarbonVerilogType type)
{
  // incorrect initialization, again to see if we miss something
  fsdbVarType fType = FSDB_VT_VCD_EVENT;
  switch (type)
  {
  case eVerilogTypeInteger:
    fType = FSDB_VT_VCD_INTEGER;
    break;
  case eVerilogTypeParameter:
    fType = FSDB_VT_VCD_PARAMETER;
    break;
  case eVerilogTypeReal:
    fType = FSDB_VT_VCD_REAL;
    break;
  case eVerilogTypeReg:
    fType = FSDB_VT_VCD_REG;
    break;
  case eVerilogTypeSupply0:
    fType = FSDB_VT_VCD_SUPPLY0;
    break;
  case eVerilogTypeSupply1:
    fType = FSDB_VT_VCD_SUPPLY1;
    break;
  case eVerilogTypeTime:
    fType = FSDB_VT_VCD_TIME;
    break;
  case eVerilogTypeTri:
    fType = FSDB_VT_VCD_TRI;
    break;
  case eVerilogTypeTriAnd:
    fType = FSDB_VT_VCD_TRIAND;
    break;
  case eVerilogTypeTriOr:
    fType = FSDB_VT_VCD_TRIOR;
    break;
  case eVerilogTypeTriReg:
    fType = FSDB_VT_VCD_TRIREG;
    break;
  case eVerilogTypeTri0:
    fType = FSDB_VT_VCD_TRI0;
    break;
  case eVerilogTypeTri1:
    fType = FSDB_VT_VCD_TRI1;
    break;
  case eVerilogTypeWAnd:
    fType = FSDB_VT_VCD_WAND;
    break;
  case eVerilogTypeWire:
    fType = FSDB_VT_VCD_WIRE;
    break;
  case eVerilogTypeWOr:
    fType = FSDB_VT_VCD_WOR;
    break;
  case eVerilogTypeRealTime:
    fType = FSDB_VT_VCD_REAL;
    break;
  case eVerilogTypeUWire:
    fType = FSDB_VT_VCD_WIRE;
    break;
  case eVerilogTypeUnsetNet:
    // eUnsetNet type is Jaguar type only, there is no such type in Novas.
    // Let's put wire type for it.
    fType = FSDB_VT_VCD_WIRE;
    break;
  case eVerilogTypeUnknown:
    fType = FSDB_VT_VHDL_SIGNAL;
    break;
  }
  return fType;
}

static fsdbVarType sTransCarbonToFsdbVarType(CarbonVarType carbonVarType)
{
  fsdbVarType varType = FSDB_VT_VCD_EVENT;
  
  switch(carbonVarType)
  {
  case eCARBON_VT_VCD_EVENT: /*!< VCD EVENT */
    varType = FSDB_VT_VCD_EVENT;
    break;
  case eCARBON_VT_VCD_INTEGER: /*!< VCD INTEGER */
    varType = FSDB_VT_VCD_INTEGER;
    break;
  case eCARBON_VT_VCD_PARAMETER: /*!< VCD PARAMETER */
    varType = FSDB_VT_VCD_PARAMETER;
    break;
  case eCARBON_VT_VCD_REAL: /*!< VCD REAL */
    varType = FSDB_VT_VCD_REAL;
    break;
  case eCARBON_VT_VCD_REG: /*!< VCD REG */
    varType = FSDB_VT_VCD_REG;
    break;
  case eCARBON_VT_VCD_SUPPLY0: /*!< VCD SUPPLY0 */
    varType = FSDB_VT_VCD_SUPPLY0;
    break;
  case eCARBON_VT_VCD_SUPPLY1: /*!< VCD SUPPLY1 */
    varType = FSDB_VT_VCD_SUPPLY1;
    break;
  case eCARBON_VT_VCD_TIME: /*!< VCD TIME */
    varType = FSDB_VT_VCD_TIME;
    break;
  case eCARBON_VT_VCD_TRI: /*!< VCD TRI */
    varType = FSDB_VT_VCD_TRI;
    break;
  case eCARBON_VT_VCD_TRIAND: /*!< VCD TRIAND */
    varType = FSDB_VT_VCD_TRIAND;
    break;
  case eCARBON_VT_VCD_TRIOR: /*!< VCD TRIOR */
    varType = FSDB_VT_VCD_TRIOR;
    break;
  case eCARBON_VT_VCD_TRIREG: /*!< VCD TRIREG */
    varType = FSDB_VT_VCD_TRIREG;
    break;
  case eCARBON_VT_VCD_TRI0: /*!< VCD TRI0 */
    varType = FSDB_VT_VCD_TRI0;
    break;
  case eCARBON_VT_VCD_TRI1: /*!< VCD TRI1 */
    varType = FSDB_VT_VCD_TRI1;
    break;
  case eCARBON_VT_VCD_WAND: /*!< VCD WAND */
    varType = FSDB_VT_VCD_WAND;
    break;
  case eCARBON_VT_VCD_WIRE: /*!< VCD WIRE */
    varType = FSDB_VT_VCD_WIRE;
    break;
  case eCARBON_VT_VCD_WOR: /*!< VCD WOR */
    varType = FSDB_VT_VCD_WOR;
    break;
  case eCARBON_VT_VCD_MEMORY: /*!< VCD MEMORY */
    varType = FSDB_VT_VCD_MEMORY;
    break;
  case eCARBON_VT_VCD_MEMORY_DEPTH: /*!< VCD MEMORY DEPTH */
    varType = FSDB_VT_VCD_MEMORY_DEPTH;
    break;
  case eCARBON_VT_VCD_MEMORY_RANGE: /*!< VCD MEMORY RANGE */
    varType = FSDB_VT_VCD_MEMORY_RANGE;
    break;
  case eCARBON_VT_VCD_PORT: /*!< VCD PORT */
    varType = FSDB_VT_VCD_PORT;
    break;

  case eCARBON_VT_VHDL_SIGNAL: /*!< VHDL SIGNAL */
    varType = FSDB_VT_VHDL_SIGNAL;
    break;
  case eCARBON_VT_VHDL_VARIABLE: /*!< VHDL VARIABLE */
    varType = FSDB_VT_VHDL_VARIABLE;
    break;
  case eCARBON_VT_VHDL_CONSTANT: /*!< VHDL CONSTANT */
    varType = FSDB_VT_VHDL_CONSTANT;
    break;
  case eCARBON_VT_VHDL_FILE: /*!< VHDL FILE */
    varType = FSDB_VT_VHDL_FILE;
    break;
  case eCARBON_VT_VHDL_MEMORY: /*!< VHDL MEMORY */
    varType = FSDB_VT_VHDL_MEMORY;
    break;
  case eCARBON_VT_VHDL_MEMORY_DEPTH: /*!< VHDL MEMORY DEPTH */
    varType = FSDB_VT_VHDL_MEMORY_DEPTH;
    break;
  case eCARBON_VT_VHDL_MEMORY_RANGE: /*!< VHDL MEMORY RANGE */
    varType = FSDB_VT_VHDL_MEMORY_RANGE;
    break;

  case eCARBON_VT_STREAM: /*!< STREAM */
    varType = FSDB_VT_STREAM;
    break;
  case eCARBON_VT_LOOP_MARKER: /*!< LOOP MARKER */
    varType = FSDB_VT_LOOP_MARKER;
    break;

  /* Property var types */
  case eCARBON_VT_PROP_MIN: /*!< PROP MIN */
    varType = FSDB_VT_PROP_MIN;
    break;
  case eCARBON_VT_PROP_COVER: /*!< PROP COVER */
    varType = FSDB_VT_PROP_COVER;
    break;
  case eCARBON_VT_PROP_LOCAL_MEMORY: /*!< PROP LOCAL MEMORY */
    varType = FSDB_VT_PROP_LOCAL_MEMORY;
    break;
  case eCARBON_VT_PROP_LOCAL: /*!< PROP LOCAL */
    varType = FSDB_VT_PROP_LOCAL;
    break;
  case eCARBON_VT_PROP_EVENT: /*!< PROP EVENT */
    varType = FSDB_VT_PROP_EVENT;
    break;
  case eCARBON_VT_PROP_BOOL: /*!< PROP BOOL */
    varType = FSDB_VT_PROP_BOOL;
    break;
  case eCARBON_VT_PROP_ASSERT_FORBID: /*!< PROP ASSERT FORBID */
    varType = FSDB_VT_PROP_ASSERT_FORBID;
    break;
  case eCARBON_VT_PROP_ASSERT_CHECK: /*!< PROP ASSERT CHECK */
    varType = FSDB_VT_PROP_ASSERT_CHECK;
    break;
  case eCARBON_VT_PROP_ASSERT: /*!< PROP ASSERT */
    varType = FSDB_VT_PROP_ASSERT;
    break;
  case eCARBON_VT_PROP_MAX: /*!< PROP MAX */
    varType = FSDB_VT_PROP_MAX;
    break;

  case eCARBON_VT_STRING: /*!< STRING */
    varType = FSDB_VT_STRING;
    break;

  /* epic var types */
  case eCARBON_VT_EPIC_LOGIC: /*!< EPIC LOGIC */
    varType = FSDB_VT_EPIC_LOGIC;
    break;
  case eCARBON_VT_EPIC_VOLTAGE: /*!< EPIC VOLTAGE */
    varType = FSDB_VT_EPIC_VOLTAGE;
    break;
  case eCARBON_VT_EPIC_INSTANTANEOUS_CURRENT: /*!< EPIC INSTANTANEOUS
                                                CURRENT */
    varType = FSDB_VT_EPIC_INSTANTANEOUS_CURRENT;
    break;
  case eCARBON_VT_EPIC_AVERAGE_RMS_CURRENT: /*!< EPIC AVERAGE RMS
                                              CURRENT */
    varType = FSDB_VT_EPIC_AVERAGE_RMS_CURRENT;
    break;
  case eCARBON_VT_EPIC_DI_DT: /*!< EPIC DI DT */
    varType = FSDB_VT_EPIC_DI_DT;
    break;
  case eCARBON_VT_EPIC_MATHEMATICS: /*!< EPIC MATHEMATICS */
    varType = FSDB_VT_EPIC_MATHEMATICS;
    break;
  case eCARBON_VT_EPIC_POWER: /*!< EPIC POWER */
    varType = FSDB_VT_EPIC_POWER;
    break;

  /* Nanosim var types */
  case eCARBON_VT_NANOSIM_LOGIC: /*!< NANOSIM LOGIC */
    varType = FSDB_VT_NANOSIM_LOGIC;
    break;
  case eCARBON_VT_NANOSIM_VOLTAGE: /*!< NANOSIM VOLTAGE */
    varType = FSDB_VT_NANOSIM_VOLTAGE;
    break;
  case eCARBON_VT_NANOSIM_INSTANTANEOUS_CURRENT: /*!< NANOSIM
                                                   INSTANTANEOUS
  CURRENT */
    varType = FSDB_VT_NANOSIM_INSTANTANEOUS_CURRENT;
    break;
  case eCARBON_VT_NANOSIM_AVERAGE_RMS_CURRENT: /*!< NANOSIM
                                                 AVERAGE_RMS CURRENT
  */
    varType = FSDB_VT_NANOSIM_AVERAGE_RMS_CURRENT;
    break;
  case eCARBON_VT_NANOSIM_DI_DT: /*!< NANOSIM DI DT */
    varType = FSDB_VT_NANOSIM_DI_DT;
    break;
  case eCARBON_VT_NANOSIM_MATHEMATICS: /*!< NANOSIM MATHEMATICS */
    varType = FSDB_VT_NANOSIM_MATHEMATICS;
    break;
  case eCARBON_VT_NANOSIM_POWER: /*!< NANOSIM POWER */
    varType = FSDB_VT_NANOSIM_POWER;
    break;
  
  /* HSPICE var types */
  case eCARBON_VT_HSPICE_VOLTAGE: /*!< HSPICE VOLTAGE */
    varType = FSDB_VT_HSPICE_VOLTAGE;
    break;
  case eCARBON_VT_HSPICE_VOLTS_MAG: /*!< HSPICE VOLTS MAG */
    varType = FSDB_VT_HSPICE_VOLTS_MAG;
    break;
  case eCARBON_VT_HSPICE_VOLTS_REAL: /*!< HSPICE VOLTS REAL */
    varType = FSDB_VT_HSPICE_VOLTS_REAL;
    break;
  case eCARBON_VT_HSPICE_VOLTS_IMAG: /*!< HSPICE VOLTS IMAG */
    varType = FSDB_VT_HSPICE_VOLTS_IMAG;
    break;
  case eCARBON_VT_HSPICE_PHASE: /*!< HSPICE PHASE */
    varType = FSDB_VT_HSPICE_PHASE;
    break;
  case eCARBON_VT_HSPICE_VOLTS_DB: /*!< HSPICE VOLTS DB */
    varType = FSDB_VT_HSPICE_VOLTS_DB;
    break;
  case eCARBON_VT_HSPICE_VOLTS_TDLY: /*!< HSPICE_VOLTS TDLY */
    varType = FSDB_VT_HSPICE_VOLTS_TDLY;
    break;
  case eCARBON_VT_HSPICE_CURRENT: /*!< HSPICE CURRENT */
    varType = FSDB_VT_HSPICE_CURRENT;
    break;
  case eCARBON_VT_HSPICE_I_MAG: /*!< HSPICE I MAG */
    varType = FSDB_VT_HSPICE_I_MAG;
    break;
  case eCARBON_VT_HSPICE_I_REAL: /*!< HSPICE I REAL */
    varType = FSDB_VT_HSPICE_I_REAL;
    break;
  case eCARBON_VT_HSPICE_I_IMAG: /*!< HSPICE I IMAG */
    varType = FSDB_VT_HSPICE_I_IMAG;
    break;
  case eCARBON_VT_HSPICE_I_PHASE: /*!< HSPICE I PHASE */
    varType = FSDB_VT_HSPICE_I_PHASE;
    break;
  case eCARBON_VT_HSPICE_I_DECIBEL: /*!< HSPICE I DECIBEL */
    varType = FSDB_VT_HSPICE_I_DECIBEL;
    break;
  case eCARBON_VT_HSPICE_I_TDLY: /*!< HSPICE I TDLY */
    varType = FSDB_VT_HSPICE_I_TDLY;
    break;
  case eCARBON_VT_HSPICE_CURRENT_1: /*!< HSPICE_CURRENT 1 */
    varType = FSDB_VT_HSPICE_CURRENT_1;
    break;
  case eCARBON_VT_HSPICE_I_MAG_1: /*!< HSPICE I MAG 1 */
    varType = FSDB_VT_HSPICE_I_MAG_1;
    break;
  case eCARBON_VT_HSPICE_I_REAL_1: /*!< HSPICE I REAL 1 */
    varType = FSDB_VT_HSPICE_I_REAL_1;
    break;
  case eCARBON_VT_HSPICE_I_IMAG_1: /*!< HSPICE I IMAG 1 */
    varType = FSDB_VT_HSPICE_I_IMAG_1;
    break;
  case eCARBON_VT_HSPICE_I_PHASE_1: /*!< HSPICE I PHASE 1 */
    varType = FSDB_VT_HSPICE_I_PHASE_1;
    break;
  case eCARBON_VT_HSPICE_I_DECIBEL_1: /*!< HSPICE I DECIBEL 1 */
    varType = FSDB_VT_HSPICE_I_DECIBEL_1;
    break;
  case eCARBON_VT_HSPICE_I_TDLY_1: /*!< HSPICE I TDLY 1 */
    varType = FSDB_VT_HSPICE_I_TDLY_1;
    break;
  case eCARBON_VT_HSPICE_CURRENT_2: /*!< HSPICE CURRENT 2 */
    varType = FSDB_VT_HSPICE_CURRENT_2;
    break;
  case eCARBON_VT_HSPICE_I_MAG_2: /*!< HSPICE I MAG 2 */
    varType = FSDB_VT_HSPICE_I_MAG_2;
    break;
  case eCARBON_VT_HSPICE_I_REAL_2: /*!< HSPICE I REAL 2 */
    varType = FSDB_VT_HSPICE_I_REAL_2;
    break;
  case eCARBON_VT_HSPICE_I_IMAG_2: /*!< HSPICE I IMAG 2 */
    varType = FSDB_VT_HSPICE_I_IMAG_2;
    break;
  case eCARBON_VT_HSPICE_I_PHASE_2: /*!< HSPICE I PHASE 2 */
    varType = FSDB_VT_HSPICE_I_PHASE_2;
    break;
  case eCARBON_VT_HSPICE_I_DECIBEL_2: /*!< HSPICE I DECIBEL 2 */
    varType = FSDB_VT_HSPICE_I_DECIBEL_2;
    break;
  case eCARBON_VT_HSPICE_I_TDLY_2: /*!< HSPICE I TDLY 2 */
    varType = FSDB_VT_HSPICE_I_TDLY_2;
    break;
  case eCARBON_VT_HSPICE_CURRENT_3: /*!< HSPICE CURRENT 3 */
    varType = FSDB_VT_HSPICE_CURRENT_3;
    break;
  case eCARBON_VT_HSPICE_I_MAG_3: /*!< HSPICE I MAG 3 */
    varType = FSDB_VT_HSPICE_I_MAG_3;
    break;
  case eCARBON_VT_HSPICE_I_REAL_3: /*!< HSPICE I REAL 3 */
    varType = FSDB_VT_HSPICE_I_REAL_3;
    break;
  case eCARBON_VT_HSPICE_I_IMAG_3: /*!< HSPICE I IMAG 3 */
    varType = FSDB_VT_HSPICE_I_IMAG_3;
    break;
  case eCARBON_VT_HSPICE_I_PHASE_3: /*!< HSPICE I PHASE 3 */
    varType = FSDB_VT_HSPICE_I_PHASE_3;
    break;
  case eCARBON_VT_HSPICE_I_DECIBEL_3: /*!< HSPICE I DECIBEL 3 */
    varType = FSDB_VT_HSPICE_I_DECIBEL_3;
    break;
  case eCARBON_VT_HSPICE_I_TDLY_3: /*!< HSPICE I TDLY 3 */
    varType = FSDB_VT_HSPICE_I_TDLY_3;
    break;
  case eCARBON_VT_HSPICE_CURRENT_4: /*!< HSPICE CURRENT 4 */
    varType = FSDB_VT_HSPICE_CURRENT_4;
    break;
  case eCARBON_VT_HSPICE_I_MAG_4: /*!< HSPICE I MAG 4 */
    varType = FSDB_VT_HSPICE_I_MAG_4;
    break;
  case eCARBON_VT_HSPICE_I_REAL_4: /*!< HSPICE I REAL 4 */
    varType = FSDB_VT_HSPICE_I_REAL_4;
    break;
  case eCARBON_VT_HSPICE_I_IMAG_4: /*!< HSPICE I IMAG 4 */
    varType = FSDB_VT_HSPICE_I_IMAG_4;
    break;
  case eCARBON_VT_HSPICE_I_PHASE_4: /*!< HSPICE I PHASE 4 */
    varType = FSDB_VT_HSPICE_I_PHASE_4;
    break;
  case eCARBON_VT_HSPICE_I_DECIBEL_4: /*!< HSPICE I DECIBEL 4 */
    varType = FSDB_VT_HSPICE_I_DECIBEL_4;
    break;
  case eCARBON_VT_HSPICE_I_TDLY_4: /*!< HSPICE I TDLY 4 */
    varType = FSDB_VT_HSPICE_I_TDLY_4;
    break;
  case eCARBON_VT_HSPICE_POWER: /*!< HSPICE POWER */
    varType = FSDB_VT_HSPICE_POWER;
    break;
  case eCARBON_VT_HSPICE_P_M: /*!< HSPICE P M */
    varType = FSDB_VT_HSPICE_P_M;
    break;
  case eCARBON_VT_HSPICE_P_REAL: /*!< HSPICE P REAL */
    varType = FSDB_VT_HSPICE_P_REAL;
    break;
  case eCARBON_VT_HSPICE_P_IMAG: /*!< HSPICE P IMAG */
    varType = FSDB_VT_HSPICE_P_IMAG;
    break;
  case eCARBON_VT_HSPICE_P_PHASE: /*!< HSPICE P PHASE */
    varType = FSDB_VT_HSPICE_P_PHASE;
    break;
  case eCARBON_VT_HSPICE_P_DB: /*!< HSPICE P DB */
    varType = FSDB_VT_HSPICE_P_DB;
    break;
  case eCARBON_VT_HSPICE_P_TDLY: /*!< HSPICE P TDLY */
    varType = FSDB_VT_HSPICE_P_TDLY;
    break;
  case eCARBON_VT_HSPICE_TPOWRD: /*!< HSPICE TPOWRD */
    varType = FSDB_VT_HSPICE_TPOWRD;
    break;
  case eCARBON_VT_HSPICE_NOI_V_SQ_HZ: /*!< HSPICE NOI V SQ HZ */
    varType = FSDB_VT_HSPICE_NOI_V_SQ_HZ;
    break;
  case eCARBON_VT_HSPICE_INOISE: /*!< HSPICE INOISE */
    varType = FSDB_VT_HSPICE_INOISE;
    break;
  case eCARBON_VT_HSPICE_HD2: /*!< HSPICE HD2 */
    varType = FSDB_VT_HSPICE_HD2;
    break;
  case eCARBON_VT_HSPICE_HD3: /*!< HSPICE HD3 */
    varType = FSDB_VT_HSPICE_HD3;
    break;
  case eCARBON_VT_HSPICE_DIM2: /*!< HSPICE DIM2 */
    varType = FSDB_VT_HSPICE_DIM2;
    break;
  case eCARBON_VT_HSPICE_SIM2: /*!< HSPICE SIM2 */
    varType = FSDB_VT_HSPICE_SIM2;
    break;
  case eCARBON_VT_HSPICE_DIM3: /*!< HSPICE DIM3 */
    varType = FSDB_VT_HSPICE_DIM3;
    break;
  case eCARBON_VT_HSPICE_Z11: /*!< HSPICE Z11 */
    varType = FSDB_VT_HSPICE_Z11;
    break;
  case eCARBON_VT_HSPICE_Z21: /*!< HSPICE Z21 */
    varType = FSDB_VT_HSPICE_Z21;
    break;
  case eCARBON_VT_HSPICE_Z12: /*!< HSPICE Z12 */
    varType = FSDB_VT_HSPICE_Z12;
    break;
  case eCARBON_VT_HSPICE_Z22: /*!< HSPICE Z22 */
    varType = FSDB_VT_HSPICE_Z22;
    break;
  case eCARBON_VT_HSPICE_ZIN: /*!< HSPICE ZIN */
    varType = FSDB_VT_HSPICE_ZIN;
    break;
  case eCARBON_VT_HSPICE_ZOUT: /*!< HSPICE ZOUT */
    varType = FSDB_VT_HSPICE_ZOUT;
    break;
  case eCARBON_VT_HSPICE_Y11: /*!< HSPICE Y11 */
    varType = FSDB_VT_HSPICE_Y11;
    break;
  case eCARBON_VT_HSPICE_Y21: /*!< HSPICE Y21 */
    varType = FSDB_VT_HSPICE_Y21;
    break;
  case eCARBON_VT_HSPICE_Y12: /*!< HSPICE Y12 */
    varType = FSDB_VT_HSPICE_Y12;
    break;
  case eCARBON_VT_HSPICE_Y22: /*!< HSPICE Y22 */
    varType = FSDB_VT_HSPICE_Y22;
    break;
  case eCARBON_VT_HSPICE_YIN: /*!< HSPICE YIN */
    varType = FSDB_VT_HSPICE_YIN;
    break;
  case eCARBON_VT_HSPICE_YOUT: /*!< HSPICE YOUT */
    varType = FSDB_VT_HSPICE_YOUT;
    break;
  case eCARBON_VT_HSPICE_H11: /*!< HSPICE H11 */
    varType = FSDB_VT_HSPICE_H11;
    break;
  case eCARBON_VT_HSPICE_H21: /*!< HSPICE H21 */
    varType = FSDB_VT_HSPICE_H21;
    break;
  case eCARBON_VT_HSPICE_H12: /*!< HSPICE H12 */
    varType = FSDB_VT_HSPICE_H12;
    break;
  case eCARBON_VT_HSPICE_H22: /*!< HSPICE H22 */
    varType = FSDB_VT_HSPICE_H22;
    break;
  case eCARBON_VT_HSPICE_S11: /*!< HSPICE S11 */
    varType = FSDB_VT_HSPICE_S11;
    break;
  case eCARBON_VT_HSPICE_S21: /*!< HSPICE S21 */
    varType = FSDB_VT_HSPICE_S21;
    break;
  case eCARBON_VT_HSPICE_S12: /*!< HSPICE S12 */
    varType = FSDB_VT_HSPICE_S12;
    break;
  case eCARBON_VT_HSPICE_S22: /*!< HSPICE S22 */
    varType = FSDB_VT_HSPICE_S22;
    break;
  case eCARBON_VT_HSPICE_PARAM: /*!< HSPICE PARAM */
    varType = FSDB_VT_HSPICE_PARAM;
    break;
  case eCARBON_VT_HSPICE_LX: /*!< HSPICE LX */
    varType = FSDB_VT_HSPICE_LX;
    break;
  case eCARBON_VT_HSPICE_LV: /*!< HSPICE LV */
    varType = FSDB_VT_HSPICE_LV;
    break;
  }

  return varType;
}

static fsdbDataType sTranslateCarbonDTToFsdb(CarbonDataType carbonDT)
{
  fsdbDataType dt = FSDB_DT_BYTE;
  
  switch (carbonDT)
  {
  case eCARBON_DT_BYTE: /*!< 1 byte integer */
    dt = FSDB_DT_BYTE;
    break;
  case eCARBON_DT_SHORT: /*!< 2 byte integer */
    dt = FSDB_DT_SHORT;
    break;
  case eCARBON_DT_INT: /*!< integer type */
    dt = FSDB_DT_INT;
    break;
  case eCARBON_DT_FLOAT: /*!< float type */
    dt = FSDB_DT_FLOAT;
    break;
  case eCARBON_DT_HL_INT: /*!< High/Low integer */
    dt = FSDB_DT_HL_INT;
    break;
  case eCARBON_DT_LONG: /*!< long type */
    dt = FSDB_DT_LONG;
    break;
  case eCARBON_DT_DOUBLE: /*!< double type */
    dt = FSDB_DT_DOUBLE;
    break;

  case eCARBON_DT_VERILOG_STANDARD: /*!< logic type */
    dt = FSDB_DT_VERILOG_STANDARD;
    break;
  case eCARBON_DT_VERILOG_REAL: /*!< double type */
    dt = FSDB_DT_VERILOG_REAL;
    break;
  case eCARBON_DT_VERILOG_INTEGER: /*!< 32 bit integer type */
    dt = FSDB_DT_VERILOG_INTEGER;
    break;

  case eCARBON_DT_EPIC_DIGITAL: /*!< Epic digital type */
    dt = FSDB_DT_EPIC_DIGITAL;
    break;
  case eCARBON_DT_EPIC_ANALOG: /*!< Epic analog type */
    dt = FSDB_DT_EPIC_ANALOG;
    break;

  case eCARBON_DT_HSPICE: /*!< hspice type */
    dt = FSDB_DT_HSPICE;
    break;
  case eCARBON_DT_XP: /*!< XP type */
    dt = FSDB_DT_XP;
    break;
  case eCARBON_DT_RAWFILE: /*!< rawfile */
    dt = FSDB_DT_RAWFILE;
    break;
  case eCARBON_DT_WFM: /*!< wfm */
    dt = FSDB_DT_WFM;
    break;

  case eCARBON_DT_VHDL_BOOLEAN: /*!< VHDL BOOLEAN */
    dt = FSDB_DT_VHDL_BOOLEAN;
    break;
  case eCARBON_DT_VHDL_BIT: /*!< VHDL BIT */
    dt = FSDB_DT_VHDL_BIT;
    break;
  case eCARBON_DT_VHDL_BIT_VECTOR: /*!< VHDL BIT VECTOR */
    dt = FSDB_DT_VHDL_BIT_VECTOR;
    break;
  case eCARBON_DT_VHDL_STD_ULOGIC: /*!< VHDL STD_ULOGIC scalar */
    dt = FSDB_DT_VHDL_STD_ULOGIC;
    break;
  case eCARBON_DT_VHDL_STD_ULOGIC_VECTOR: /*!< VHDL STD ULOGIC VECTOR
                                            */
    dt = FSDB_DT_VHDL_STD_ULOGIC_VECTOR;
    break;
  case eCARBON_DT_VHDL_STD_LOGIC: /*!< VHDL STD LOGIC scalar */
    dt = FSDB_DT_VHDL_STD_LOGIC;
    break;
  case eCARBON_DT_VHDL_STD_LOGIC_VECTOR: /*!< VHDL STD LOGIC VECTOR */
    dt = FSDB_DT_VHDL_STD_LOGIC_VECTOR;
    break;
  case eCARBON_DT_VHDL_UNSIGNED: /*!< VHDL STD UNSIGNED */
    dt = FSDB_DT_VHDL_UNSIGNED;
    break;
  case eCARBON_DT_VHDL_SIGNED: /*!< VHDL STD SIGNED */
    dt = FSDB_DT_VHDL_SIGNED;
    break;
  case eCARBON_DT_VHDL_INTEGER: /*!< VHDL STD INTEGER */
    dt = FSDB_DT_VHDL_INTEGER;
    break;
  case eCARBON_DT_VHDL_REAL: /*!< VHDL REAL */
    dt = FSDB_DT_VHDL_REAL;
    break;
  case eCARBON_DT_VHDL_NATURAL: /*!< VHDL NATURAL */
    dt = FSDB_DT_VHDL_NATURAL;
    break;
  case eCARBON_DT_VHDL_POSITIVE: /*!< VHDL POSITIVE */
    dt = FSDB_DT_VHDL_POSITIVE;
    break;
  case eCARBON_DT_VHDL_TIME: /*!< VHDL TIME */
    dt = FSDB_DT_VHDL_TIME;
    break;
  case eCARBON_DT_VHDL_CHARACTER: /*!< VHDL CHARACTER */
    dt = FSDB_DT_VHDL_CHARACTER;
    break;
  case eCARBON_DT_VHDL_STRING: /*!< VHDL_STRING */
    dt = FSDB_DT_VHDL_STRING;
    break;
  case eCARBON_DT_VHDL_STDLOGIC_1D: /*!< VHDL STD LOGIC 1D */
    dt = FSDB_DT_VHDL_STDLOGIC_1D;
    break;
  case eCARBON_DT_VHDL_STDLOGIC_TABLE: /*!< VHDL STD LOGIC TABLE */
    dt = FSDB_DT_VHDL_STDLOGIC_TABLE;
    break;
  case eCARBON_DT_VHDL_LOGIC_X01_TABLE: /*!< VHDL X01 LOGIC TABLE */
    dt = FSDB_DT_VHDL_LOGIC_X01_TABLE;
    break;
  case eCARBON_DT_VHDL_LOGIC_X01Z_TABLE: /*!< VHDL X01Z LOGIC TABLE */
    dt = FSDB_DT_VHDL_LOGIC_X01Z_TABLE;
    break;
  case eCARBON_DT_VHDL_LOGIC_UX01_TABLE: /*!< VHDL UX01 LOGIC TABLE */
    dt = FSDB_DT_VHDL_LOGIC_UX01_TABLE;
    break;
  case eCARBON_DT_VHDL_X01: /*!< VHDL X01 */
    dt = FSDB_DT_VHDL_X01;
    break;
  case eCARBON_DT_VHDL_X01Z: /*!< VHDL X01Z */
    dt = FSDB_DT_VHDL_X01Z;
    break;
  case eCARBON_DT_VHDL_UX01: /*!< VHDL UX01 */
    dt = FSDB_DT_VHDL_UX01;
    break;
  case eCARBON_DT_VHDL_UX01Z: /*!< VHDL UX01Z */
    dt = FSDB_DT_VHDL_UX01Z;
    break;
  case eCARBON_DT_VHDL_SEVERITY_LEVEL: /*!< VHDL SEVERITY LEVEL */
    dt = FSDB_DT_VHDL_SEVERITY_LEVEL;
    break;
  case eCARBON_DT_VHDL_DELAY_LENGTH: /*!< VHDL DELAY LENGTH */
    dt = FSDB_DT_VHDL_DELAY_LENGTH;
    break;
  case eCARBON_DT_VHDL_LINE: /*!< VHDL LINE */
    dt = FSDB_DT_VHDL_LINE;
    break;
  case eCARBON_DT_VHDL_TEXT: /*!< VHDL TEXT */
    dt = FSDB_DT_VHDL_TEXT;
    break;
  case eCARBON_DT_VHDL_SIDE: /*!< VHDL SIDE */
    dt = FSDB_DT_VHDL_SIDE;
    break;
  case eCARBON_DT_VHDL_WIDTH: /*!< VHDL WIDTH */
    dt = FSDB_DT_VHDL_WIDTH;
    break;
  case eCARBON_DT_VHDL_FILE_OPEN_KIND: /*!< VHDL FILE OPEN KIND */
    dt = FSDB_DT_VHDL_FILE_OPEN_KIND;
    break;
  case eCARBON_DT_VHDL_FILE_OPEN_STATUS: /*!< VHDL FILE OPEN STATUS */
    dt = FSDB_DT_VHDL_FILE_OPEN_STATUS;
    break;
  }
  
  return dt;
}
static fsdbBytesPerBit sTranslateCarbonBPBToFsdb(CarbonBytesPerBit carbonBpb)
{
  fsdbBytesPerBit bpb = FSDB_BYTES_PER_BIT_1B;

  switch(carbonBpb)
  {
  case eCARBON_BYTES_PER_BIT_1B:
    bpb = FSDB_BYTES_PER_BIT_1B;
    break;
  case eCARBON_BYTES_PER_BIT_2B:
    bpb = FSDB_BYTES_PER_BIT_2B;
    break;
  case eCARBON_BYTES_PER_BIT_4B:
    bpb = FSDB_BYTES_PER_BIT_4B;
    break;
  case eCARBON_BYTES_PER_BIT_8B:
    bpb = FSDB_BYTES_PER_BIT_8B;
    break;
  }
  return bpb;
}

static void* sGetFsdbDataType(const UserType* ut)
{
  // This is the types defined in FSDB writer
  fsdbDataType  fsdb_type = FSDB_DT_VERILOG_STANDARD;
  if(ut->getType() == UserType::eScalar)
  {
    CarbonVhdlType vhdlType = ut->getVhdlType();
    switch(vhdlType)
    {
    case eVhdlTypeBit:
      fsdb_type = FSDB_DT_VHDL_BIT;
      break;

    case eVhdlTypeStdLogic:
      fsdb_type = FSDB_DT_VHDL_STD_LOGIC;
      break;

    case eVhdlTypeStdULogic:
      fsdb_type = FSDB_DT_VHDL_STD_ULOGIC;
      break;

    case eVhdlTypeBoolean:
      fsdb_type = FSDB_DT_VHDL_BOOLEAN;
      break;

    case eVhdlTypeInteger:
    case eVhdlTypeIntType:
      fsdb_type = FSDB_DT_VHDL_INTEGER;
      break;

    case eVhdlTypeNatural:
      fsdb_type = FSDB_DT_VHDL_NATURAL;
      break;

    case eVhdlTypePositive:
      fsdb_type = FSDB_DT_VHDL_POSITIVE;
      break;

    case eVhdlTypeReal:
      fsdb_type = FSDB_DT_VHDL_REAL;
      break;

    case eVhdlTypeChar:
      fsdb_type = FSDB_DT_VHDL_CHARACTER;
      break;

    case eVhdlTypeUnknown:
      INFO_ASSERT(0, "Unknown vhdl type.");
      break;
    }
  }
  else if(ut->getType() == UserType::eArray)
  {
    const UserArray* ua = ut->castArray();
    const UserType* ut = ua->getElementType();
    if(ut->getType() == UserType::eScalar)
    {
      // This is element type
      CarbonVhdlType vhdlType = ut->getVhdlType();
      switch(vhdlType)
      {
        case eVhdlTypeStdLogic:
          fsdb_type = FSDB_DT_VHDL_STD_LOGIC_VECTOR;
          break;

        case eVhdlTypeStdULogic:
          fsdb_type = FSDB_DT_VHDL_STD_ULOGIC_VECTOR;
          break;

        case eVhdlTypeBit:
          fsdb_type = FSDB_DT_VHDL_BIT_VECTOR;
          break;

        case eVhdlTypeBoolean:
          fsdb_type = FSDB_DT_VHDL_BOOLEAN;
          break;

        case eVhdlTypeInteger:
        case eVhdlTypeIntType:
          fsdb_type = FSDB_DT_VHDL_INTEGER;
          break;

        case eVhdlTypeNatural:
          fsdb_type = FSDB_DT_VHDL_NATURAL;
          break;

        case eVhdlTypePositive:
          fsdb_type = FSDB_DT_VHDL_POSITIVE;
          break;

        case eVhdlTypeReal:
          fsdb_type = FSDB_DT_VHDL_REAL;
          break;

        case eVhdlTypeChar:
          fsdb_type = FSDB_DT_VHDL_CHARACTER;
          break;

        case eVhdlTypeUnknown:
          INFO_ASSERT(0, "Unknown vhdl type.");
          break;
      }
    }
  }
  else
  {
     INFO_ASSERT(0, "Unknown vhdl type.");
  }
  
  return (void*) fsdb_type;
}

FsdbFile::FsdbFile(CarbonTimescale timescale, MsgContext* msgContext)
  : WaveDump(timescale), mMsgContext(msgContext)
{
  init();
}



FsdbFile::FsdbFile(const char* fileName, CarbonTimescale timescale,
                   bool* isSuccess, UtString* errMsg,
                   MsgContext* msgContext) :
  WaveDump(timescale), mFsdbEnvSyncControl (true), mMsgContext(msgContext)
{
  init();

  const char* env = getenv ("FSDB_ENV_SYNC_CONTROL");
  if (env && strcmp (env, "off")==0)
    mFsdbEnvSyncControl = false;

  *isSuccess = true;
  mFfwObj = ffw_Open(const_cast<char*>(fileName), FSDB_FT_VERILOG_VHDL);
  if (mFfwObj == NULL)
  {
    *errMsg << "Unable to open fsdb file '" << fileName << "' for writing.";
    *isSuccess = false;
  }
  else
    writeFsdbHeader();
}

void FsdbFile::init()
{
  mUserDataMap = NULL;
  mIsOpen = false;
  mAllVals = new AllVals;
  mFsdbType = FSDB_DT_MAX_RESERVED_IDCODE;  // This is max existing FSDB data type 
}

void FsdbFile::writeFsdbHeader()
{
  mIsOpen = true;
  ffw_CreateTreeByHandleScheme(mFfwObj);
  setDate();
  setVersion();
  setTimescale();
}

FsdbFile::~FsdbFile()
{
  closeFfwObj();

  if (mUserDataMap)
  {
    for(UserDataMap::DataToID::UnsortedLoop p = mUserDataMap->mHash.loopUnsorted();
        ! p.atEnd(); ++p)
    {
      CarbonWaveUserData* data = p.getValue();
      delete data;
    }
    
    delete mUserDataMap;
  }

  delete mAllVals;
}


void FsdbFile::closeFfwObj()
{
  if (mIsOpen)
  {
    ffw_Close(mFfwObj);
    mIsOpen = false;
  }
}

FsdbFile*
FsdbFile::open(const char* fileName, CarbonTimescale timescale, UtString* errMsg,
               MsgContext* msgContext)
{
  bool isSuccess;
  FsdbFile* file = new FsdbFile(fileName, timescale, &isSuccess, errMsg, 
                                msgContext);
  if (! isSuccess)
  {
    delete file;
    file = NULL;
  }
  return file;
}


WaveDump::FileStatus 
FsdbFile::flush(UtString*)
{
  ffw_Sync(mFfwObj);
  return eOK;
}

WaveDump::FileStatus 
FsdbFile::close(UtString*)
{
  closeFfwObj();
  return eOK;
}

WaveDump::FileStatus 
FsdbFile::closeHierarchy(UtString* errMsg)
{
  FileStatus stat = eOK;
  if(VERIFY(mHierOpen))
  {
    stat = writeHierarchy(errMsg);

    mHierOpen = false;
  }

  clearAuxHierStructs();

  return stat;
}

void FsdbFile::setDate()
{
  UtString date;
  OSGetTimeStr("%b %d, %Y  %H:%M:%S", &date);
  const char* dval = date.c_str();
  ffw_SetSimulationDate(mFfwObj, const_cast<char*>(dval));
}

void FsdbFile::setVersion()
{
  ffw_SetSimulatorVersion(mFfwObj, const_cast<char*>(cShellVersion));
}

void FsdbFile::setTimescale()
{
  ffw_SetScaleUnit(mFfwObj, const_cast<char *>(stringifyTimescale()));
  ffw_GetDataTypeCreationReady(mFfwObj);
}

const char* FsdbFile::stringifyTimescale() const
{
  switch(mTimeScale)
  {
  case e1fs: return "1fs"; break;
  case e10fs: return "10fs"; break;
  case e100fs: return "100fs"; break;
  case e1ps: return "1ps"; break;
  case e10ps: return "10ps"; break;
  case e100ps: return "100ps"; break;
  case e1ns: return "1ns"; break;
  case e10ns: return "10ns"; break;
  case e100ns: return "100ns"; break;
  case e1us: return "1us"; break;
  case e10us: return "10us"; break;
  case e100us: return "100us"; break;
  case e1ms: return "1ms"; break;
  case e10ms: return "10ms"; break;
  case e100ms: return "100ms"; break;
  case e1s: return "1s"; break;
  case e10s: return "10s"; break;
  case e100s: return "100s"; break;
  }

  INFO_ASSERT(0, "Unhandled timescale");
  // NOT REACHED
  return NULL;
}

WaveDump::FileStatus 
FsdbFile::writeHierarchy(UtString* errMsg)
{
  ffw_BeginTree(mFfwObj);
  WaveScopeIter p = loopRoots();
  WaveScope* scope;
  while (p(&scope))
    writeHierBranch(scope, errMsg);
  ffw_EndTree(mFfwObj);

  if (not mFsdbEnvSyncControl)
    ffw_SyncControlTurnOff(mFfwObj);
  else
    ffw_SyncControlTurnOn(mFfwObj);

  return errMsg->empty() ? eOK : eError;
}

static fsdbVarDir sTranslateCarbonVarDirToFsdb(CarbonVarDirection dir)
{
  fsdbVarDir fsdbDir = FSDB_VD_IMPLICIT;
  switch (dir)
  {
  case eCarbonVarDirectionImplicit:
    break;
  case eCarbonVarDirectionInput:
    fsdbDir = FSDB_VD_INPUT;
    break;
  case eCarbonVarDirectionOutput:
    fsdbDir = FSDB_VD_OUTPUT;
    break;
  case eCarbonVarDirectionInout:
    fsdbDir = FSDB_VD_INOUT;
    break;
  case eCarbonVarDirectionBuffer:
    fsdbDir = FSDB_VD_BUFFER;
    break;
  case eCarbonVarDirectionLinkage:
    fsdbDir = FSDB_VD_LINKAGE;
    break;
  }
  return fsdbDir;
}

static fsdbVarDir sGetFsdbDirection(const WaveHandle* handle)
{
  CarbonVarDirection dir = handle->getDirection();
  return sTranslateCarbonVarDirToFsdb(dir);
}

void* FsdbFile::createEnumFSDBtype(const UserType* ut, WaveHandle* handle, UtString* errMsg)
{
  const UserEnum* uEnum = ut->castEnum();
  INFO_ASSERT(uEnum, "Not enum type.");
  UInt32 numElems = uEnum->getNumberElems();

  UIntPtr fsdb_type = 0;

  // First check whether enumeration for this handle was created before
  UserTypeToIntMap::const_iterator citr = mUserTypeToEnumTypeMap.find(ut);
  if (citr == mUserTypeToEnumTypeMap.end())
  {
    // There is no enum for this handle - create it
    mFsdbType++;  // this should be incremented to make it different from previous one 
    mUserTypeToEnumTypeMap[ut] = mFsdbType;

    // Creating array of pointers to strings 
    char** strArr = static_cast<char**> (CarbonMem::malloc(numElems*sizeof(char*)) );
    for(UInt32 i = 0; i < numElems; i++)
    {
      StringAtom* enumElem = uEnum->getElem(i);
      strArr[i] = const_cast <char*> (enumElem->str());
    }

    fsdb_type = mFsdbType;
    fsdbDataTypeIdcode  idcode = ffw_DTCreateEnum(mFfwObj,(void*) fsdb_type, numElems, 
                                                strArr);
    if(idcode == FSDB_INVALID_DATA_TYPE_IDCODE)
    {
      UtString nameBuf;
      *errMsg << "\nFailed to create enumeration type for " << handle->getNameAtom()->str() << ".";
    }

    CarbonMem::free(strArr);
  }
  else
  {
    fsdb_type = mUserTypeToEnumTypeMap[ut]; 
  }
    
  return (void*) fsdb_type;
}


void* FsdbFile::createEnumCharFSDBtype(const UserType *ut, WaveHandle* handle, UtString* errMsg)
{
  UIntPtr fsdb_type = 0;

  // First check whether enumeration for this handle was created before
  UserTypeToIntMap::const_iterator citr = mUserTypeToEnumTypeMap.find(ut);
  if (citr == mUserTypeToEnumTypeMap.end())
  {
    // There is no enum for this handle - create it
    mFsdbType++;  // this should be incremented to make it different from previous one 
    mUserTypeToEnumTypeMap[ut] = mFsdbType;

    const char *char_ar[] = {
                       "nul",   "soh",   "stx",   "etx",    "eot",   "enq",   "ack",   "bel",   "bs",    "ht", 
                       "lf",    "vt",    "ff",    "cr",     "so",    "si",    "dle",   "dc1",   "dc2",   "dc3",
                       "dc4",   "nak",   "syn",   "etb",    "can",   "em",    "sub",   "esc",   "fsp",   "gsp",
                       "rsp",   "usp",   "'",     "'!'",    "'\"'",  "'#'",   "'$'",   "'%'",   "'&'",   "'''", 
                       "'('",   "')'",   "'*'",   "'+'",    "'",     "'-'",   "'.'",   "'/'",   "'0'",   "'1'",
                       "'2'",   "'3'",   "'4'",   "'5'",    "'6'",   "'7'",   "'8'",   "'9'",   "':'",   "';'",
                       "'<'",   "'='",   "'>'",   "'?'",    "'@'",   "'A'",   "'B'",   "'C'",   "'D'",   "'E'", 
                       "'F'",   "'G'",   "'H'",   "'I'",    "'J'",   "'K'",   "'L'",   "'M'",   "'N'",   "'O'",
                       "'P'",   "'Q'",   "'R'",   "'S'",    "'T'",   "'U'",   "'V'",   "'W'",   "'X'",   "'Y'", 
                       "'Z'",   "'['",   "'\'",   "']'",    "'^'",   "'_'",   "'`'",   "'a'",   "'b'",   "'c'",
                       "'d'",   "'e'",   "'f'",   "'g'",    "'h'",   "'i'",   "'j'",   "'k'",   "'l'",   "'m'",
                       "'n'",   "'o'",   "'p'",   "'q'",    "'r'",   "'s'",   "'t'",   "'u'",   "'v'",   "'w'",
                       "'x'",   "'y'",   "'z'",   "'{'",    "'|'",   "'}'",   "'~'",   "del",   "c128",  "c129",
                       "c130",  "c131",  "c132",  "c133",   "c134",  "c135",  "c136",  "c137",  "c138",  "c139",
                       "c140",  "c141",  "c142",  "c143",   "c144",  "c145",  "c146",  "c147",  "c148",  "c149",
                       "c150",  "c151",  "c152",  "c153",   "c154",  "c155",  "c156",  "c157",  "c158",  "c159",
                       "'�'","'\241'","'\242'","'\243'","'\244'","'\245'","'\246'","'\247'","'\250'","'\251'",
                       "'�'","'\253'","'\254'","'\255'","'\256'","'\257'","'\260'","'\261'","'\262'","'\263'",
                       "'\264'","'\265'","'\266'","'\267'","'\270'","'\271'","'\272'","'\273'","'\274'","'\275'",
                       "'\276'","'\277'","'\300'","'\301'","'\302'","'\303'","'\304'","'\305'","'\305'","'\307'",
                       "'\310'","'\311'","'\312'","'\313'","'\314'","'\315'","'\316'","'\317'","'\320'","'\321'",
                       "'\322'","'\323'","'\324'","'\325'","'\326'","'\327'","'\330'","'\331'","'\332'","'\333'",
                       "'\334'","'\335'","'\336'","'\337'","'\340'","'\341'","'\342'","'\343'","'\344'","'\345'",
                       "'\346'","'\347'","'\350'","'\351'","'\352'","'\353'","'\354'","'\355'","'\356'","'\357'",
                       "'\360'","'\361'","'\362'","'\363'","'\364'","'\365'","'\366'","'\367'","'\370'","'\371'",
                       "'\372'","'\373'","'\374'","'\375'","'\376'","'\377'"};

    UInt32 numElems = sizeof(char_ar)/sizeof(char_ar[0]);
    fsdb_type = mFsdbType;
    fsdbDataTypeIdcode  idcode = ffw_DTCreateEnum(mFfwObj,(void*) fsdb_type, numElems, 
                                                  const_cast<char**> (char_ar));
    if(idcode == FSDB_INVALID_DATA_TYPE_IDCODE)
    {
      UtString nameBuf;
      *errMsg << "\nFailed to create character enumeration type for " << handle->getNameAtom()->str() << ".";
    }
  }
  else
  {
    fsdb_type = mUserTypeToEnumTypeMap[ut]; 
  }
    
  return (void*) fsdb_type;
}


void FsdbFile::writeHierBranch(WaveScope* scope, UtString* errMsg)
{
  if (! isEmpty(scope)) {
    fsdbScopeType scopeType = sGetFsdbScopeType(scope);
    
    UtString nameStr;
    scope->getNameWithRange(&nameStr);

    UtString prefix;
    if (scope->getType() == WaveScope::eStruct)
    {
      // Group fields of a record.
      ffw_CreateRecordBegin(mFfwObj, nameStr.getBuffer(), scope->numChildren());
    }
    else if (scope->getType() == WaveScope::eArray)
    {
      // Group elements of an array.
      ffw_CreateArrayBegin(mFfwObj, nameStr.getBuffer(), scope->numChildren());
    }
    else
    {
      // Begin new module scope.
      ffw_CreateScope(mFfwObj, scopeType, nameStr.getBuffer());
    }
    
    // Sort children.  If this scope is an array or struct, this does
    // nothing as those children are sorted before they're added.
    scope->sortChildren();
    // Now loop over all children, in order, processing handles and
    // scopes.  This ensures that scopes with both handles and scopes
    // as children get dumped in the right order.
    WaveChildIter childIter = scope->loopChildren();
    WaveChild* waveChild;
    while (childIter(&waveChild)) {
      WaveHandle* handle = waveChild->castWaveHandle();
      WaveScope* childScope = waveChild->castWaveScope();
      UtString buf; // needed by Handle to create full name
      WaveHandle* masterHandle = NULL;
      if (handle != NULL) {
        masterHandle = handle->getTopHandle(handle);
        fsdbVarType varType = sGetFsdbVarType(handle->getType());
        void* dType = FSDB_DT_HANDLE_VERILOG_STANDARD;
        
        bool isChar = false, isEnum = false;
        int numEnumElem = 0;
        // If we have vhdl net, varType should be FSDB_VT_VHDL_SIGNAL
        if(varType == FSDB_VT_VHDL_SIGNAL)
        {
          // If the waveform handle has user type info, use it to 
          // get the fsdb data type.
          const UserType* ut = handle->getUserType();
          if (ut != NULL) 
          {
            if(ut->getType() == UserType::eEnum) 
            {
              dType = createEnumFSDBtype(ut, handle, errMsg); 
              isEnum = true;
              const UserEnum* ue = ut->castEnum();
              numEnumElem = ue->getNumberElems();
            }
            else
            {
              dType = sGetFsdbDataType(ut);
              if((ut->getType() == UserType::eScalar)  && (ut->getVhdlType() == eVhdlTypeChar))
              {
                isChar = true;
                dType = createEnumCharFSDBtype(ut, handle, errMsg);
              }
            }
          }
        }

        fsdbBytesPerBit bpb = FSDB_BYTES_PER_BIT_1B;
        SInt32 msb = handle->getMSB();
        SInt32 lsb = handle->getLSB();

        if (handle->isReal())
        {
          if(varType != FSDB_VT_VHDL_SIGNAL)
            dType = FSDB_DT_HANDLE_VERILOG_REAL;

          bpb = FSDB_BYTES_PER_BIT_8B;
          double* dblVal = mAllVals->getNewDbl();
          handle->setObj(dblVal);
          msb = 0;
          lsb = 0;
        }
        else if (handle->isInteger())
        {
          if(varType != FSDB_VT_VHDL_SIGNAL)
            dType = FSDB_DT_HANDLE_VERILOG_INTEGER;

          bpb = FSDB_BYTES_PER_BIT_4B;
          SInt32* intVal = mAllVals->getNewInt();
          handle->setObj(intVal);
          msb = 0;
          lsb = 0;
        }
        else if (handle->isTime())
        {
          dType = FSDB_DT_HANDLE_VERILOG_INTEGER;
          bpb = FSDB_BYTES_PER_BIT_8B;
          SInt64*  int64Val = mAllVals->getNewInt64();
          handle->setObj(int64Val);
          msb = 0;
          lsb = 0;
        }
        else if(isChar)
        {
          // We get here only for VHDL design
          // The type is character
          bpb = FSDB_BYTES_PER_BIT_1B;
          char* byteArr = mAllVals->getNewByteArr(1);
          handle->setObj(byteArr);
          msb = 0;
          lsb = 0;
        }
        else if(isEnum)
        {
          if(numEnumElem < 256)
            bpb = FSDB_BYTES_PER_BIT_1B;
          else if(numEnumElem < 65535)
            bpb = FSDB_BYTES_PER_BIT_2B;
          else
            bpb = FSDB_BYTES_PER_BIT_4B;

          char* byteArr = mAllVals->getNewByteArr(1);
          handle->setObj(byteArr);
          msb = 0;
          lsb = 0;
        }
        else if (masterHandle == handle)
        {
          SInt32 numBytes = std::abs(handle->getMSB() - handle->getLSB()) + 1;
          char* byteArr = mAllVals->getNewByteArr(numBytes);
          handle->setObj(byteArr);
        }


        handle->getNameWithRange(&buf, false, prefix.c_str());
        
        fsdbVarDir fsdbDir = sGetFsdbDirection(handle);

        if (ffw_CreateVarByHandle(mFfwObj, varType, fsdbDir, (void*)dType,
                                  msb, lsb, masterHandle,
                                  const_cast<char*>(buf.c_str()), bpb) == NULL) {
          UtString nameBuf;
          *errMsg << "\nFailed to create fsdb var(" << buf << ") in scope("
                  << scope->getName(&nameBuf) << ")";
        }
      } else if (childScope != NULL) {
        writeHierBranch(childScope, errMsg);
      }
    }
    
    if (scope->getType() == WaveScope::eStruct) {
      ffw_CreateRecordEnd(mFfwObj); // Close record group.
    } else if (scope->getType() == WaveScope::eArray) {
      ffw_CreateArrayEnd(mFfwObj); // Close array group.
    } else {
      // now upscope
      ffw_CreateUpscope(mFfwObj);
    }
  }  
}

void FsdbFile::writeFsdbHandleValue(WaveHandle* handle)
{
  updateFsdbValue(handle);
  ffw_CreateVarValueByHandle(mFfwObj, handle, 
                             static_cast<byte_T*>(handle->getObj()));
  handle->unregister();
}

SInt32 FsdbFile::actualValue(WaveHandle* sig, SInt32 value)
{
  if (! sig->isInteger())
    return value;

  const UserType* ut = sig->getUserType();

  if (ut == NULL)
    return value;

  const UserScalar* st =  ut->castScalar();

  if (st == NULL)
    return value;

  const ConstantRange* range = st->getRangeConstraint();

  if (range == NULL)
    return value;

  SInt32 lsb  = range->getLsb();
  SInt32 msb  = range->getMsb();

  // If the lsb and msb are positive then there is no problem.
  if (lsb >= 0 && msb >= 0)
    return value;

  UInt32 size = sig->getSize();

  // Try to determine if the sign bit is set
  if (value >> (size-1) != 1)
    return value;

  // Create a mask 
  SInt32 mask = -1;
  mask <<= size;

  // Apply the mask to sign extend the value
  return value | mask;
}

void FsdbFile::writeValueChanges()
{
  writeTime();
  
  if (mTimeCnt > 0)
  {
    for (HandleListCLoop p(mChanges); ! p.atEnd(); ++p)
      writeFsdbHandleValue(*p);
    
    if (mUserDataMap)
      updateUserData();
  }
  else
    writeAllValues();
}


void FsdbFile::writeTime()
{
  UInt32 curTime[2];
  getCurrentFsdbTime(curTime);
  ffw_CreateXCoorByHnL(mFfwObj, curTime[1], curTime[0]);
}

void FsdbFile::getCurrentFsdbTime(UInt32 curTime[2]) const
{
  CarbonValRW::cpSrcToDest(curTime, &mCurTime, 2);
}

void FsdbFile::writeHandleValue(WaveHandle* handle)
{
  // call non-virtual function
  writeFsdbHandleValue(handle);
}


void FsdbFile::updateFsdbValue(WaveHandle* handle)
{
  // integers, time and reals get updated when changed
  if (handle->isVhdlSig())
  {
    updateVhdlFsdbValue(handle);
  }
  else if (! (handle->isReal() || handle->isInteger()
              || handle->isTime()) )
  {
    UInt32 numBytes = handle->getSize();
    char* fval = static_cast<char*>(handle->getObj());
    char* val = handle->getValue();
    for (UInt32 i = 0; i < numBytes; ++i)
    {
      switch(val[i])
      {
      case 'x':
      case 'X':
      case '?':
        fval[i] = FSDB_BT_VCD_X;
        break;
      case 'z':
      case 'Z':
        fval[i] = FSDB_BT_VCD_Z;
        break;
      case '0':
        fval[i] = FSDB_BT_VCD_0;
        break;
      case '1':
        fval[i] = FSDB_BT_VCD_1;
        break;
      }
    }
  }
}

void FsdbFile::updateVhdlFsdbValue(WaveHandle* handle)
{
  CarbonVhdlType vhdlType;
  if(handle->getUserType()->getType() == UserType::eArray)
  {
    const UserArray* ua = handle->getUserType()->castArray();
    vhdlType = ua->getElementType()->getVhdlType();
  }
  else if(handle->getUserType()->getType() == UserType::eEnum)
  {
    updateVhdlEnum(handle);
    return;
  }
  else
    vhdlType = handle->getVhdlType();

  switch (vhdlType)
  {
  case eVhdlTypeBoolean:
    updateVhdlBoolean(handle);
    break;
  case eVhdlTypeBit:
    updateVhdlBit(handle);
    break;
  case eVhdlTypeStdLogic:
    updateVhdlStdLogic(handle);
    break;
  case eVhdlTypeStdULogic:
    updateVhdlStdULogic(handle);
    break;
  case eVhdlTypeChar:
    updateVhdlCharacter(handle);
    break;
  case eVhdlTypeInteger:
  case eVhdlTypeNatural:
  case eVhdlTypePositive:
    break;
  case eVhdlTypeReal:
    break;
  default:
    INFO_ASSERT(0, handle->getNameAtom()->str());
    break;
  }
}

void FsdbFile::updateVhdlCharacter(WaveHandle* handle)
{
  UInt32 numBytes = handle->getSize();
  char* fval = static_cast<char*>(handle->getObj());
  char* val = static_cast<char*> (handle->getValue());
  UInt8 valInt = 0;
  UInt8 exp = 1;

  for(UInt32 i = 0; i < numBytes; ++i)
  {
    if(i != 0)
      exp *= 2;
      
    valInt += (val[numBytes-1 - i] == '1')*exp;
  }
  *fval = valInt;
  fval[1] = 0;
}


void FsdbFile::updateVhdlEnum(WaveHandle* handle)
{
  UInt32 numBytes = handle->getSize();
  char* fval = static_cast<char*>(handle->getObj());
  char* val = static_cast<char*> (handle->getValue());
  UInt32 valInt = 0;
  UInt32 exp = 1;

  for(UInt32 i = 0; i < numBytes; ++i)
  {
    if(i != 0)
      exp *= 2;
      
    valInt += (val[numBytes-1 - i] == '1')*exp;
  }

  const UserEnum* ue = handle->getUserType()->castEnum();
  const ConstantRange* range = ue->getRange();
  if(range != NULL)
    valInt -= range->getMsb();

  *fval = valInt;
  fval[1] = 0;
}


void FsdbFile::updateVhdlBoolean(WaveHandle* handle)
{
  UInt32 numBytes = handle->getSize();
  char* fval = static_cast<char*>(handle->getObj());
  char* val = handle->getValue();
  for (UInt32 i = 0; i < numBytes; ++i)
  {
    switch(val[i])
    {
    case 'x':
    case 'X':
    case '?':
    case 'z':
    case 'Z':
    case '0':
      fval[i] = FSDB_BT_VHDL_BOOLEAN_FALSE;
      break;
    case '1':
      fval[i] = FSDB_BT_VHDL_BOOLEAN_TRUE;
      break;
    }
  }
}

void FsdbFile::updateVhdlBit(WaveHandle* handle)
{
  UInt32 numBytes = handle->getSize();
  char* fval = static_cast<char*>(handle->getObj());
  char* val = handle->getValue();
  for (UInt32 i = 0; i < numBytes; ++i)
  {
    switch(val[i])
    {
    case 'x':
    case 'X':
    case '?':
    case 'z':
    case 'Z':
    case '0':
      fval[i] = FSDB_BT_VHDL_BIT_0;
      break;
    case '1':
      fval[i] = FSDB_BT_VHDL_BIT_1;
      break;
    }
  }
}

void FsdbFile::updateVhdlStdLogic(WaveHandle* handle)
{
  UInt32 numBytes = handle->getSize();
  char* fval = static_cast<char*>(handle->getObj());
  char* val = handle->getValue();
  for (UInt32 i = 0; i < numBytes; ++i)
  {
    switch(val[i])
    {
    case '?':
      fval[i] = FSDB_BT_VHDL_STD_LOGIC_U;
      break;
    case 'x':
    case 'X':
      fval[i] = FSDB_BT_VHDL_STD_LOGIC_X;
      break;
    case 'z':
    case 'Z':
      fval[i] = FSDB_BT_VHDL_STD_LOGIC_Z;
      break;
    case '0':
      fval[i] = FSDB_BT_VHDL_STD_LOGIC_0;
      break;
    case '1':
      fval[i] = FSDB_BT_VHDL_STD_LOGIC_1;
      break;
    }
  }
}

void FsdbFile::updateVhdlStdULogic(WaveHandle* handle)
{
  UInt32 numBytes = handle->getSize();
  char* fval = static_cast<char*>(handle->getObj());
  char* val = handle->getValue();
  for (UInt32 i = 0; i < numBytes; ++i)
  {
    switch(val[i])
    {
    case '?':
      fval[i] = FSDB_BT_VHDL_STD_ULOGIC_U;
      break;
    case 'x':
    case 'X':
      fval[i] = FSDB_BT_VHDL_STD_ULOGIC_X;
      break;
    case 'z':
    case 'Z':
      fval[i] = FSDB_BT_VHDL_STD_ULOGIC_Z;
      break;
    case '0':
      fval[i] = FSDB_BT_VHDL_STD_ULOGIC_0;
      break;
    case '1':
      fval[i] = FSDB_BT_VHDL_STD_ULOGIC_1;
      break;
    }
  }
}

void FsdbFile::addChangedWithDbl(WaveHandle* sig, double value)
{
  setValDbl(sig, value);
  addChanged(sig);
}

//! Change the value of a real signal and add to change table
/*!
  Different waveform dumpers have different ways of dealing with
  32 bit integers. So, pass this off to the implementation.
*/
void FsdbFile::addChangedWithInt32(WaveHandle* sig, SInt32 value)
{
  setValInt32(sig, value);
  addChanged(sig);
}
  
//! Change the value of a real signal and add to change table
/*!
  Different waveform dumpers have different ways of dealing with
  64 bit integers. So, pass this off to the implementation.
*/
void FsdbFile::addChangedWithInt64(WaveHandle* sig, UInt64 value)
{
  setValInt64(sig, value);
  addChanged(sig);
}

//! Sets the value of the real number handle
/*!
  This does not add the handle to the change table. This is used
  generally to initialize handles.
*/
void FsdbFile::setValDbl(WaveHandle* sig, double value)
{
  WaveHandle* top = sig->getTopHandle(sig);
  INFO_ASSERT(top->isReal(), sig->getName());
  double* hVal = static_cast<double*>(top->getObj());
  *hVal = value;
}

//! Sets the value of the integer handle
/*!
  This does not add the handle to the change table. This is used
  generally to initialize handles.
*/
void FsdbFile::setValInt32(WaveHandle* sig, SInt32 value)
{
  WaveHandle* top = sig->getTopHandle(sig);
  INFO_ASSERT(top->isInteger(), sig->getName());
  SInt32* hVal = static_cast<SInt32*>(top->getObj());

  // The value could be negative; figure it out based on the sig.
  *hVal = actualValue(sig, value);
}
  
//! Set the value of the 64 bit integer handle
/*!
  This does not add the handle to the change table. This is used
  generally to initialize handles.
*/
void FsdbFile::setValInt64(WaveHandle* sig, UInt64 value)
{
  WaveHandle* top = sig->getTopHandle(sig);
  INFO_ASSERT(top->isTime(), sig->getName());
  UInt64* hVal = static_cast<UInt64*>(top->getObj());
  *hVal = value;
}

// fsdb doesn't do dumpall
void FsdbFile::dumpDataAll()
{
}

void FsdbFile::dumpDataOff()
{
  writeTime();
  writeAllValues();
}
  
void FsdbFile::dumpDataOn()
{
  writeTime();
  writeAllValues();
}

bool FsdbFile::supportsArrayScopes() const
{
  return true;
}

bool FsdbFile::isFsdb() const
{
  return true;
}

CarbonWaveUserData* FsdbFile::addUserVar(CarbonVarType netType, 
                                         CarbonVarDirection direction,
                                         CarbonDataType dataType,
                                         int lbitnum, int rbitnum,
                                         CarbonClientData data, 
                                         const char* varName, 
                                         CarbonBytesPerBit bpb,
                                         const char* fullPathScopeName,
                                         const char* scopeSeparator,
                                         UtString* errMsg)
{
  
  if (! mIsOpen)
  {
    *errMsg << "\nFile Not opened: Failed to create fsdb var(" << varName << ") in scope("
            << fullPathScopeName << ")";
    return NULL;
  }

  // Check if data is already being used by another
  // CarbonWaveUserData. If so just return that but update the
  // datafile so it will be aliased.
  if (! mUserDataMap)
    mUserDataMap = new UserDataMap;
  
  CarbonWaveUserData* userData = NULL;
  
  bool isAliased = false;
  UserDataMap::DataToID::iterator p = mUserDataMap->mHash.find(data);
  if (p != mUserDataMap->mHash.end())
  {
    userData = p->second;
    isAliased = true;
  }
  else
  {
    userData = new CarbonWaveUserData(this, lbitnum, rbitnum, data, varName, bpb, fullPathScopeName);
    mUserDataMap->mHash[data] = userData;
  }
  

  // This gets passed to the ffw_Create method 
  unsigned char* buffer = userData->getBuffer();
  
  fsdbVarType fsdbVarType = sTransCarbonToFsdbVarType(netType);
  fsdbVarDir fsdbDir = sTranslateCarbonVarDirToFsdb(direction);
  fsdbDataType fsdbDataType = sTranslateCarbonDTToFsdb(dataType);
  fsdbBytesPerBit fsdbBpb = sTranslateCarbonBPBToFsdb(bpb);

  ffw_BeginTree(mFfwObj);  
  
  if (ffw_CreateVarByHandleAndPath(mFfwObj, fsdbVarType,
                                   fsdbDir, (void*)fsdbDataType,
                                   lbitnum, rbitnum, buffer,
                                   const_cast<char*>(varName), 
                                   fsdbBpb,
                                   const_cast<char*>(fullPathScopeName),  
                                   const_cast<char*>(scopeSeparator)) == NULL)
  {
    *errMsg << "\nFailed to create fsdb var(" << varName << ") in scope("
            << fullPathScopeName << ")";
    
    if (! isAliased)
    {
      mUserDataMap->mHash.erase(data);
      delete userData;
      userData = NULL;
    }
    
  }
  ffw_EndTree(mFfwObj);

  return userData;
}

void FsdbFile::writeAllUserData()
{
  if (mUserDataMap)
  {
    for(UserDataMap::DataToID::UnsortedLoop p = mUserDataMap->mHash.loopUnsorted();
        ! p.atEnd(); ++p)
    {
      CarbonWaveUserData* data = p.getValue();
      // The user may have added the data to the waveform, but hasn't
      // called carbonWaveUserDataChange yet, which could mean that
      // the data is uninitialized. EMC may add a variable to the
      // waveform, but not actually initialize it until the first
      // assignment.
      if (data->isInit() && data->isActive())
      {
        unsigned char* buf = data->getBuffer();
        ffw_CreateVarValueByHandle(mFfwObj, buf, buf);
        data->clearChange();
      }
    }
  }
}

void FsdbFile::updateUserData()
{
  for(UserDataMap::DataToID::UnsortedLoop p = mUserDataMap->mHash.loopUnsorted();
      ! p.atEnd(); ++p)
  {
    CarbonWaveUserData* data = p.getValue();
    if (data->hasChanged() && data->isActive())
    {
      unsigned char* buf = data->getBuffer();
      
      ffw_CreateVarValueByHandle(mFfwObj, buf, buf);
      data->clearChange();
    }
  }
}

void FsdbFile::switchFileHelper(const char* fileName, ffwObject* oldObj,
                                UInt32 high, UInt32 low)
{
  MsgContext* msgContext = getMsgContext();
  msgContext->SHLFsdbFileSwitch(fileName, high, low);

  // Open the new switch file if possible
  ffwObject* obj = ffw_SwitchFile(oldObj, const_cast<char*>(fileName));
  if (obj == NULL)
  {
    UtString errMsg;
    errMsg << "Unable to switch to fsdb file '" << fileName << "' for writing.";
    msgContext->SHLFatalFileProblem(errMsg.c_str());
  }
  
  // Update the current state
  putFfwObj(obj);

  // If carbonSchedule() hasn't been called yet to set the initial
  // time, don't dump anything.  We don't want to assume that the time
  // will be zero.
  if (isInitTimeSet()) {
    // This writes out the current time and all the current values
    dumpDataOn();
  }
}

static void sHitLimitedFileSizeCBFunc(ffwHitLimitedFileSizeCBRec *cb_rec, 
                                      void *client_data)
{
  FsdbAutoSwitchFile* fsdbFile = (FsdbAutoSwitchFile*) client_data;

  UtString newFileName;
  newFileName.assign(fsdbFile->getFilePrefix());

  fsdbFile->incrFileIndex();
  fsdbFile->calcFilename();

  const char* fileName = fsdbFile->getFilename();
  fsdbFile->switchFileHelper(fileName, cb_rec->obj, cb_rec->xtag.hltag.H,
                             cb_rec->xtag.hltag.L);

  UInt32 time[2];
  fsdbFile->getCurrentFsdbTime(time);
  
  INFO_ASSERT(time[0] == cb_rec->xtag.hltag.L, "Waveform and model not synched");
  INFO_ASSERT(time[1] == cb_rec->xtag.hltag.H, "Waveform and model not synched");
}

FsdbFile* FsdbFile::openAutoSwitch(const char* fileNamePrefix, 
                                   CarbonTimescale timescale,
                                   unsigned int limitMegs,
                                   unsigned int maxFiles,
                                   MsgContext* msgContext,
                                   UtString* errMsg)
{
  bool isSuccess;
  FsdbAutoSwitchFile* file = new FsdbAutoSwitchFile(fileNamePrefix, timescale, limitMegs, maxFiles, msgContext, &isSuccess, errMsg);
  if (! isSuccess)
  {
    delete file;
    file = NULL;
  }
  return file;
}

FsdbAutoSwitchFile::FsdbAutoSwitchFile(const char* fileNamePrefix, 
                                       CarbonTimescale timescale, 
                                       UInt32 limitMegs, 
                                       UInt32 maxFiles,
                                       MsgContext* msgContext,
                                       bool* isSuccess,
                                       UtString* errMsg) : 
  FsdbFile(timescale, msgContext),
  mMaxFiles(maxFiles),
  mFileIndex(0)
{
  mFilePrefix = new UtString(fileNamePrefix);
  mFilename = new UtString;

  calcFilename();

  *isSuccess = true;

  if (limitMegs < 2)
  {
    msgContext->SHLFsdbLimitSizeTooSmall(limitMegs);
    limitMegs = 2;
  }
  
  if (maxFiles == 1)
  {
    *isSuccess = false;
    *errMsg << "Maximum file specification cannot be 1. Either 0 or a number greater than 1 is allowed.";
    return;
  }

  ffwAutoSwitchFileRec fileRec;
  fileRec.file_name = const_cast<char*>(mFilename->c_str());
  fileRec.file_type = FSDB_FT_VERILOG_VHDL;
  fileRec.limited_file_size = limitMegs; 
  fileRec.cb_func   = sHitLimitedFileSizeCBFunc;
  
  mFfwObj = ffw_OpenAutoSwitchFile(&fileRec);

  if (mFfwObj == NULL)
  {
    *errMsg << "Unable fsdb file '" << *mFilename << "' for writing.";
    *isSuccess = false;
  }
  else
  {
    ffw_RegisterAutoSwitchFileClientData(mFfwObj, (void*)this);
    writeFsdbHeader();
  }
}

bool FsdbFile::switchFSDBFile(const char* fileName)
{
  UInt32 time[2];
  getCurrentFsdbTime(time);
  switchFileHelper(fileName, mFfwObj, time[1], time[0]);
  return mFfwObj != NULL;
}

void FsdbFile::putFfwObj(ffwObject* obj)
{
  mFfwObj = obj;
}

FsdbAutoSwitchFile::~FsdbAutoSwitchFile()
{
  delete mFilePrefix;
  delete mFilename;
}

void FsdbAutoSwitchFile::calcFilename()
{
  mFilename->clear();
  *mFilename << *mFilePrefix << "_" << mFileIndex << ".fsdb";
}

void FsdbAutoSwitchFile::incrFileIndex() 
{ 
  ++mFileIndex;
  if (mMaxFiles > 0)
    mFileIndex %= mMaxFiles; 
}

const char* FsdbAutoSwitchFile::getFilename() const
{
  return mFilename->c_str();
}

const char* FsdbAutoSwitchFile::getFilePrefix() const
{
  return mFilePrefix->c_str(); 
}

bool FsdbAutoSwitchFile::switchFSDBFile(const char* fileName)
{
  // Update the file prefix
  *mFilePrefix = fileName;
  mFileIndex = 0;

  // Recompute the new file name
  UtString newFileName;
  newFileName.assign(getFilePrefix());
  calcFilename();

  // Switch to the new file
  UInt32 time[2];
  getCurrentFsdbTime(time);
  switchFileHelper(getFilename(), mFfwObj, time[1], time[0]);
  return mFfwObj != NULL;
}
