// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/ShellNetPlayback.h"
#include "util/UtConv.h"
#include "util/UtIOStream.h"
#include "symtab/STSymbolTable.h"
#include "shell/ShellNetTristate.h"
#include "shell/CarbonTristateVector.h"
#include "hdl/ReadMemX.h"
#include "shell/CarbonNetValueCBData.h"
#include "shell/ShellMemory.h"

static CarbonStatus sCalcIndexLength(int msb, int lsb, int range_msb, int range_lsb, size_t* index, size_t* length, CarbonModel* model)
{
  return CarbonUtil::calcIndexLength(msb, lsb, range_msb, range_lsb, index, length, model);
}

//! Templated function to update mask
/*!

  Some of the code to efficiently update the masked flag and the mask
  itself is common across all nets sizes and access types.  However,
  other parts are very different, i.e. different functions are called
  with different arguments.

  This function uses templates to implement all flavors.  Because all
  the variations are determined by template parameters, the compiler
  should be able to generate fast custom versions for each one.

  This was chosen over virtual functions because I didn't want to add
  the overhead of the additional function call.
 */
template <bool _SingleWord, bool _IsRange>
inline void sUpdateMask(ShellNetReplay::Touched* touchBuffer,
                        UInt32* maskBuf,
                        UInt32 numWords,
                        UInt32 wordIndex,
                        UInt32 tailMask,
                        SInt32 rangeIndex,
                        UInt32 rangeLength)
{
  // Deal with mask.  If the net has already been masked since the
  // last schedule call, all we need to do is update the mask bits.
  // If the net has been neither masked nor touched, we also need to
  // clear the previous mask bits (for performance reasons only the
  // masked flag, not the mask itself, is cleared on each schedule
  // call) and set the masked flag.  If the net has been touched but
  // not masked, do nothing because there has already been a full
  // deposit to the net, so all the bits in the value buffer are
  // valid.
  bool isTouched = touchBuffer->isSet();
  bool isMasked = touchBuffer->isSetMasked();
  bool clearMask = !isMasked && !isTouched;
  bool updateMask = isMasked || clearMask;
  if (clearMask) {
    // Clear the mask buffer and set the masked flag
    if (_SingleWord) {
      *maskBuf = 0;
    } else {
      CarbonValRW::setToZero(maskBuf, numWords);
    }
    touchBuffer->setMasked();
  }
  if (updateMask) {
    // Record what bits were deposited
    if (_IsRange) {
      CarbonValRW::setRangeToOnes(maskBuf, rangeIndex, rangeLength);
    } else {
      maskBuf[wordIndex] = 0xffffffff;
      maskBuf[numWords - 1] &= tailMask;
    }
  }
}

// Convenience functions for calling the templated function above

//! Updates the mask for depositRange to a small (<= 32 bit) net
inline void sUpdateMaskRangeSmall(ShellNetReplay::Touched* touchBuffer,
                                  UInt32* maskBuf,
                                  SInt32 rangeIndex,
                                  UInt32 rangeLength)
{
  sUpdateMask<true, true>(touchBuffer,
                          maskBuf,
                          0 /* numWords - unused */,
                          0 /* wordIndex - unused */,
                          0 /* tailMask - unused */,
                          rangeIndex,
                          rangeLength);
}

//! Updates the mask for depositRange to a large (> 32 bit) net
inline void sUpdateMaskRangeLarge(ShellNetReplay::Touched* touchBuffer,
                                  UInt32* maskBuf,
                                  UInt32 numWords,
                                  SInt32 rangeIndex,
                                  UInt32 rangeLength)
{
  sUpdateMask<false, true>(touchBuffer,
                           maskBuf,
                           numWords,
                           0 /* wordIndex - unused */,
                           0 /* tailMask - unused */,
                           rangeIndex,
                           rangeLength);
}

//! Updates the mask for depositWord to a large (> 32 bit) net
inline void sUpdateMaskWordLarge(ShellNetReplay::Touched* touchBuffer,
                                 UInt32* maskBuf,
                                 UInt32 numWords,
                                 UInt32 wordIndex,
                                 UInt32 tailMask)
{
  sUpdateMask<false, false>(touchBuffer,
                            maskBuf,
                            numWords,
                            wordIndex,
                            tailMask,
                            0 /* rangeIndex - unused */,
                            0 /* rangeLength - unused */);
}


/* ShellNetPlayback */

ShellNetPlayback::ShellNetPlayback(ShellNet* subNet, const Touched& touched)
  : ShellNetReplay(subNet),
    mTouched(touched)
{
  init(subNet);
}

ShellNetPlayback::ShellNetPlayback(ShellNet* subNet)
  : ShellNetReplay(subNet)
{
  init(subNet);
}

void ShellNetPlayback::init(ShellNet* subNet)
{
  mBitWidth = subNet->getBitWidth();
  mTailMask = CarbonValRW::getWordMask(mBitWidth);
}

ShellNetPlayback::~ShellNetPlayback()
{}

const ShellNetPlayback* ShellNetPlayback::castShellNetPlayback() const
{
  return this;
}

void ShellNetPlayback::genericGetModelValueBuffers(ShellNet* wrapperNet, UInt32** value, UInt32** drive)
{
  // 5 virtual calls here. As a first cut, I'd rather do that than
  // create default functions for all the derivations of ShellNet
  // or ShellNetWrapper.
  // 1)
  const ShellNetWrapper* wrapper = wrapperNet->castShellNetWrapper();
  ST_ASSERT(wrapper, wrapperNet->getName());
  // 2)
  const ShellNetWrapper1To1* wrapperOne = wrapper->castShellNetWrapper1To1();
  ST_ASSERT(wrapperOne, wrapperNet->getName());
  // 3) 
  const ShellNetReplay* replayNet = wrapperOne->castShellNetReplay();
  ST_ASSERT(replayNet, wrapperNet->getName());
  // 4)
  const ShellNetPlayback* playNet = replayNet->castShellNetPlayback();
  ST_ASSERT(playNet, wrapperNet->getName());
  // 5)
  playNet->getModelValueBuffers(value, drive);
}

CarbonStatus ShellNetPlayback::twoStateFormatForceHelper(char* valueStr, size_t len, CarbonRadix radix, 
                                                         NetFlags /*flags*/, const UInt32* modelValue, 
                                                         ShellNet* forceMask, UInt32 bitWidth, 
                                                         CarbonModel* model) const
{
  UInt32* forceMaskValue;
  UInt32* forceMaskDrive /*always NULL*/;
  genericGetModelValueBuffers(forceMask, &forceMaskValue, &forceMaskDrive);
  
  const UInt32* controlMask = mNet->getControlMask();
  return valueFormatString(valueStr, len, radix, modelValue, forceMaskValue, controlMask, bitWidth, model);  
}

CarbonStatus ShellNetPlayback::force(const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlayback::forceWord(UInt32, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlayback::forceRange(const UInt32*, 
                                          int, int, 
                                          CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlayback::release(CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlayback::releaseWord(int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlayback::releaseRange(int, int, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void ShellNetPlayback::getEncodedDepositBuffers(UInt32** value, UInt32** drive) const
{
  getDepositBuffers(value, drive);
}

UInt32* ShellNetPlayback::getMaskBuffer() const
{
  return NULL;
}

void ShellNetPlayback::putToZero(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

void ShellNetPlayback::putToOnes(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

CarbonStatus ShellNetPlayback::setRange(int , int , CarbonModel* )
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlayback::clearRange(int , int , CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

bool ShellNetPlayback::isDataNonZero() const
{
  ST_ASSERT(0, getName());
  return false;
}

void ShellNetPlayback::setRawToUndriven(CarbonModel*)
{
  ST_ASSERT(0, getName());
}

bool ShellNetPlayback::setToUndriven(CarbonModel*)
{
  return false;
}

bool ShellNetPlayback::resolveXdrive(CarbonModel*)
{
  return false;
}

void ShellNetPlayback::runValueChangeCB(CarbonNetValueCBData*, 
                                        UInt32*, 
                                        UInt32*,
                                        CarbonTriValShadow*,
                                        CarbonModel*) const
{
  ST_ASSERT(0, getName());
}

bool ShellNetPlayback::isSimpleClock() const
{
  return false;
}

bool ShellNetPlayback::isEncodedValueChanged() const
{
  return false;
}

void ShellNetPlayback::doReportNotDepositable(CarbonModel* model)
{
  // This should only be called if this is not depositable
  // (bom->isDepositable() is false) but the record side may have
  // recorded a propagation. We 
  // can find this out by calling isDepositAllowed() which will warn
  // about the non-depositability, but also tell us if the deposit was
  // accepted anyway. In that case, we need to make sure
  // carbonSchedule is called.
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    model->getHookup()->setSeenDeposit();
  ST_ASSERT(! bom->isDepositable(), getName());
}

/* ShellNetPlaybackNotReplayable */

ShellNetPlaybackNotReplayable::ShellNetPlaybackNotReplayable(ShellNet* primNet, CarbonModel* model)
  : ShellNetPlayback(primNet), mCarbonModel(model)
{}

ShellNetPlaybackNotReplayable::~ShellNetPlaybackNotReplayable()
{}

CarbonStatus ShellNetPlaybackNotReplayable::examine (UInt32*, UInt32*, ExamineMode, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackNotReplayable::examineWord (UInt32*, int, UInt32*, ExamineMode, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackNotReplayable::examineRange (UInt32*, int, int, UInt32*, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackNotReplayable::format(char*, size_t, 
                                                   CarbonRadix, NetFlags,
                                                   CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackNotReplayable::deposit (const UInt32*, const UInt32*, CarbonModel* model)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackNotReplayable::depositWord (UInt32 , int , UInt32, CarbonModel* model)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), model);
  return eCarbon_ERROR;

}

CarbonStatus ShellNetPlaybackNotReplayable::depositRange (const UInt32* , int, int, const UInt32*, CarbonModel* model)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

void ShellNetPlaybackNotReplayable::fastDeposit(const UInt32*, const UInt32* , CarbonModel* model)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), model);
}

void ShellNetPlaybackNotReplayable::fastDepositWord (UInt32, int, UInt32 , CarbonModel* model)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), model);
}

void ShellNetPlaybackNotReplayable::fastDepositRange (const UInt32* , int, int, const UInt32* , CarbonModel* model)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), model);
}

ShellNetPlayback* ShellNetPlaybackNotReplayable::cloneStorage(ShellNet*)
{
  ST_ASSERT(0, getName());
  return NULL;
}

void ShellNetPlaybackNotReplayable::putModelValueBuffers(UInt32*, UInt32*)
{
  ST_ASSERT(0, getName());
}

void ShellNetPlaybackNotReplayable::getModelValueBuffers(UInt32** , UInt32**) const
{
  ST_ASSERT(0, getName());
}

void ShellNetPlaybackNotReplayable::getDepositBuffers(UInt32**, UInt32**) const
{
  ST_ASSERT(0, getName());
}


CarbonStatus ShellNetPlaybackNotReplayable::depositMemory(CarbonMemAddrT, const UInt32*)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), mCarbonModel);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackNotReplayable::depositMemoryWord(CarbonMemAddrT, UInt32, int)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), mCarbonModel);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackNotReplayable::depositMemoryRange(CarbonMemAddrT, const UInt32*, int, int)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), mCarbonModel);
  return eCarbon_ERROR;
}
  
CarbonStatus ShellNetPlaybackNotReplayable::examineMemory(CarbonMemAddrT, UInt32*) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mCarbonModel);
  return eCarbon_ERROR;
}

UInt32 ShellNetPlaybackNotReplayable::examineMemoryWord(CarbonMemAddrT, int) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mCarbonModel);
  return 0;
}

CarbonStatus ShellNetPlaybackNotReplayable::examineMemoryRange(CarbonMemAddrT, UInt32*, int, int) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mCarbonModel);
  return eCarbon_ERROR;
}
  
CarbonStatus ShellNetPlaybackNotReplayable::formatMemory(char*, size_t, CarbonRadix, CarbonMemAddrT) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mCarbonModel);
  return eCarbon_ERROR;
}


CarbonStatus ShellNetPlaybackNotReplayable::readmemh(const char*)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), mCarbonModel);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackNotReplayable::readmemb(const char*)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), mCarbonModel);
  return eCarbon_ERROR;
}
  
CarbonStatus ShellNetPlaybackNotReplayable::readmemh(const char*, SInt64, SInt64)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), mCarbonModel);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackNotReplayable::readmemb(const char*, SInt64, SInt64)
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), mCarbonModel);
  return eCarbon_ERROR;
}
  
void ShellNetPlaybackNotReplayable::dumpAddress(UtOBStream*, CarbonMemAddrT , CarbonRadix) const
{
  ShellGlobal::reportNotDepositable(getNameAsLeaf(), mCarbonModel);
}


/* ShellNetPlaybackTwoStateWord */

ShellNetPlaybackTwoStateWord::ShellNetPlaybackTwoStateWord(ShellNet* subNet, UInt32* depositBuf, UInt32* maskBuf, UInt32 idriveVal, UInt32 xdriveVal, const Touched& touched)
  : ShellNetPlayback(subNet, touched),
    mDepositBuf(depositBuf),
    mModelValue(NULL),
    mMaskBuf(maskBuf),
    mOwnMaskBuf(false),
    mIDriveValue(idriveVal),
    mXDriveValue(xdriveVal)
{
  if (mMaskBuf == NULL) {
    mMaskBuf = CARBON_ALLOC_VEC(UInt32, 1);
    mOwnMaskBuf = true;
  }
}

ShellNetPlaybackTwoStateWord::~ShellNetPlaybackTwoStateWord()
{
  if (mOwnMaskBuf) {
    CARBON_FREE_VEC(mMaskBuf, UInt32, 1);
  }
}


void ShellNetPlaybackTwoStateWord::doExamine(UInt32* buf, UInt32* drive,
                                             ExamineMode mode) const
{
  if (buf)
    *buf = *mModelValue;
  
  if (drive)
  {
    switch (mode)
    {
    case eIDrive:
      *drive = mIDriveValue;
      break;
    case eCalcDrive:
      // Two state is always driven either externally or internally.
      *drive = 0;
      break;
    case eXDrive:
      *drive = mXDriveValue;
      break;
    }
  }
}

CarbonStatus ShellNetPlaybackTwoStateWord::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  doExamine(buf, drive, mode);
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackTwoStateWord::examine (CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const
{
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTwoStateWord::examineWord(UInt32* buf, int, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  doExamine(buf, drive, mode);
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackTwoStateWord::examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index;
  size_t length;
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  
  if (stat == eCarbon_OK)
  {
    if (buf)
      CarbonValRW::cpSrcRangeToDest(buf, mModelValue, index, length);
    
    if (drive)
      CarbonValRW::cpSrcRangeToDest(drive, &mIDriveValue, index, length);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackTwoStateWord::examineValXDriveWord(UInt32* val, UInt32* drv, int) const
{
  doExamine(val, drv, eXDrive);
  return eCarbon_OK;
}

void ShellNetPlaybackTwoStateWord::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
    *xdrive = mXDriveValue;
}

CarbonStatus 
ShellNetPlaybackTwoStateWord::format(char* valueStr, size_t len, 
                                     CarbonRadix strFormat, NetFlags,
                                     CarbonModel* model) const
{
  const UInt32* controlMask = mNet->getControlMask();
  return valueFormatString(valueStr, len, strFormat, mModelValue, (UInt32*) NULL, controlMask, mBitWidth, model);
}

CarbonStatus 
ShellNetPlaybackTwoStateWord::formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel* model) const
{
  return twoStateFormatForceHelper(valueStr, len, radix, flags, mModelValue, forceMask, mBitWidth, model);
}

void ShellNetPlaybackTwoStateWord::doDeposit(const UInt32* buf, CarbonModel* model)
{
  setTouched(model);
  doDepositNoTouch(buf);
}

CarbonStatus ShellNetPlaybackTwoStateWord::deposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  if (buf)
    doDeposit(buf, model);
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackTwoStateWord::depositWord(UInt32 buf, int index, UInt32, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK)
    doDeposit(&buf, model);
  return stat;
}

CarbonStatus 
ShellNetPlaybackTwoStateWord::doDepositRange(const UInt32* buf, 
                                             int range_msb, int range_lsb,
                                             CarbonModel* model)
{
  size_t index;
  size_t length;
  
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    CarbonValRW::cpSrcToDestRange(mDepositBuf, buf, index, length);
    CarbonValRW::cpSrcToDestRange(mModelValue, buf, index, length);
    // Update mask
    sUpdateMaskRangeSmall(&mTouched, mMaskBuf, index, length);
    // Update only the touched flag, not the mask
    setTouchedOnly(model);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackTwoStateWord::depositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  return doDepositRange(buf, range_msb, range_lsb, model);
}

void ShellNetPlaybackTwoStateWord::fastDeposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  doDeposit(buf, model);
}

void ShellNetPlaybackTwoStateWord::fastDepositWord(UInt32 buf, int, UInt32, CarbonModel* model)
{
  doDeposit(&buf, model);
}

void ShellNetPlaybackTwoStateWord::fastDepositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRange(buf, range_msb, range_lsb, model);
}

ShellNetPlayback* ShellNetPlaybackTwoStateWord::cloneStorage(ShellNet* primNet)
{
  ShellNetPlayback* ret = instanceMe(primNet);
  ret->putModelValueBuffers(mModelValue, NULL);
  return ret;
}

ShellNetPlaybackTwoStateWord* ShellNetPlaybackTwoStateWord::instanceMe(ShellNet* subNet)
{
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  return new ShellNetPlaybackTwoStateWord(subNet, mDepositBuf, maskBuf, mIDriveValue, mXDriveValue, mTouched);
}

void ShellNetPlaybackTwoStateWord::putModelValueBuffers(UInt32* value, 
                                                        UInt32* drive)
{
  ST_ASSERT(drive == NULL, getName());
  ST_ASSERT(value != NULL, getName());
  mModelValue = value;
}

void ShellNetPlaybackTwoStateWord::getModelValueBuffers(UInt32** value, 
                                                        UInt32** drive) const
{
  *value = mModelValue;
  *drive = NULL;
}

void ShellNetPlaybackTwoStateWord::getDepositBuffers(UInt32** value, 
                                                     UInt32** drive) const
{
  *value = mDepositBuf;
  *drive = NULL;
}

UInt32* ShellNetPlaybackTwoStateWord::getMaskBuffer() const
{
  return mMaskBuf;
}

void ShellNetPlaybackTwoStateWord::putToZero(CarbonModel* model)
{
  UInt32 zero = 0;
  doDeposit(&zero, model);
}

void ShellNetPlaybackTwoStateWord::putToOnes(CarbonModel* model)
{
  UInt32 ones = 0;
  ones = ~ones;
  doDeposit(&ones, model);
}

CarbonStatus ShellNetPlaybackTwoStateWord::setRange(int range_msb, int range_lsb, CarbonModel* model)
{
  UInt32 ones = 0;
  ones = ~ones;
  return doDepositRange(&ones, range_msb, range_lsb, model);
}

CarbonStatus ShellNetPlaybackTwoStateWord::clearRange(int range_msb, int range_lsb, CarbonModel* model)
{
  UInt32 zero = 0;
  return doDepositRange(&zero, range_msb, range_lsb, model);
}

bool ShellNetPlaybackTwoStateWord::isDataNonZero() const
{
  return *mModelValue != 0;
}

void ShellNetPlaybackTwoStateWord::runValueChangeCB(CarbonNetValueCBData* cbData, 
                                                    UInt32* newVal, 
                                                    UInt32* newDrv,
                                                    CarbonTriValShadow* fullShadow,
                                                    CarbonModel* model) const
{
  // Do an idrive examine here, since we do not have a drive to compare
  doExamine(newVal, newDrv, eIDrive);
  
  if (*(fullShadow->mValue) != *newVal)
  {
    cbData->executeCB(model->getObjectID (), newVal, newDrv);
    *(fullShadow->mValue) = *newVal;  
  }
}


/* ShellNetPlaybackTwoStateClk */
ShellNetPlaybackTwoStateClk::ShellNetPlaybackTwoStateClk(ShellNet* subNet, 
                                                         UInt32* depositBuf,
                                                         UInt32* maskBuf,
                                                         UInt32 idriveVal, 
                                                         UInt32 xdriveVal,
                                                         UInt32* valShadow,
                                                         const Touched& touchBuffer)
  : ShellNetPlaybackTwoStateWord(subNet, depositBuf, maskBuf, idriveVal, xdriveVal, touchBuffer),
    mValShadow(valShadow)
{
  ST_ASSERT(mBitWidth == 1, getName());
}

ShellNetPlaybackTwoStateClk::~ShellNetPlaybackTwoStateClk()
{
}

void ShellNetPlaybackTwoStateClk::doClkDeposit(const UInt32* buf, CarbonModel* model)
{
  *mValShadow = *mDepositBuf;
  doDepositNoTouch(buf);
  const UInt32 changed = *mValShadow != *mDepositBuf;
  addTouched(changed, model);
}

CarbonStatus ShellNetPlaybackTwoStateClk::deposit (const UInt32* buf, const UInt32*, CarbonModel* model)
{
  // check if it changed prior to doing the actual deposit
  
  if (buf)
    doClkDeposit(buf, model);
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackTwoStateClk::depositWord (UInt32 buf, int index, UInt32, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK) {
    doClkDeposit(&buf, model);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackTwoStateClk::doDepositRange(const UInt32* buf, 
                                                         int range_msb, int range_lsb,
                                                         CarbonModel* model)
{
  size_t index;
  size_t length;
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    // width is 1 because this is a clock, so don't worry about the
    // range.
    doClkDeposit(buf, model);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackTwoStateClk::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  return doDepositRange(buf, range_msb, range_lsb, model);
}

void ShellNetPlaybackTwoStateClk::fastDeposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  doClkDeposit(buf, model);
}

void ShellNetPlaybackTwoStateClk::fastDepositWord (UInt32 buf, int, UInt32, CarbonModel* model)
{
  doClkDeposit(&buf, model);
}

void ShellNetPlaybackTwoStateClk::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRange(buf, range_msb, range_lsb, model);
}

void ShellNetPlaybackTwoStateClk::putToZero(CarbonModel* model)
{
  UInt32 zero = 0;
  doClkDeposit(&zero, model);
}

void ShellNetPlaybackTwoStateClk::putToOnes(CarbonModel* model)
{
  UInt32 one = 1;
  doClkDeposit(&one, model);
}

CarbonStatus ShellNetPlaybackTwoStateClk::setRange(int range_msb, int range_lsb, CarbonModel* model)
{
  UInt32 one = 1;
  return doDepositRange(&one, range_msb, range_lsb, model);
}

CarbonStatus ShellNetPlaybackTwoStateClk::clearRange(int range_msb, int range_lsb, CarbonModel* model)
{
  UInt32 zero = 0;
  return doDepositRange(&zero, range_msb, range_lsb, model);
}

ShellNetPlaybackTwoStateWord* ShellNetPlaybackTwoStateClk::instanceMe(ShellNet* subNet)
{
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  return new ShellNetPlaybackTwoStateClk(subNet, mDepositBuf, maskBuf, mIDriveValue, mXDriveValue, mValShadow, mTouched);
}


bool ShellNetPlaybackTwoStateClk::isSimpleClock() const
{
  return true;
}

/* ShellNetPlaybackTwoStateClkWriteOnly */
ShellNetPlaybackTwoStateClkWriteOnly::ShellNetPlaybackTwoStateClkWriteOnly(ShellNet* subNet, UInt32* depositBuf,
                                                                           UInt32* maskBuf, 
                                                                           UInt32 idriveVal, 
                                                                           UInt32 xdriveVal,
                                                                           UInt32* valShadow,
                                                                           const Touched& touchBuffer)
  : ShellNetPlaybackTwoStateClk(subNet, depositBuf, maskBuf, idriveVal, xdriveVal, valShadow, touchBuffer)
{}

ShellNetPlaybackTwoStateClkWriteOnly::~ShellNetPlaybackTwoStateClkWriteOnly()
{}

CarbonStatus ShellNetPlaybackTwoStateClkWriteOnly::examine(UInt32*, UInt32*, ExamineMode, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}
  
CarbonStatus ShellNetPlaybackTwoStateClkWriteOnly::examineWord (UInt32*, int, UInt32*, ExamineMode, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTwoStateClkWriteOnly::examineRange (UInt32*, int, int, UInt32*, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTwoStateClkWriteOnly::format(char*, size_t, 
                                                          CarbonRadix, NetFlags,
                                                          CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

ShellNetPlaybackTwoStateWord* ShellNetPlaybackTwoStateClkWriteOnly::instanceMe(ShellNet* subNet)
{
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  return new ShellNetPlaybackTwoStateClkWriteOnly(subNet, mDepositBuf, maskBuf, mIDriveValue, mXDriveValue, mValShadow, mTouched);
}


/* ShellNetPlaybackTwoStateClkStateOutput */
ShellNetPlaybackTwoStateClkStateOutput::ShellNetPlaybackTwoStateClkStateOutput(ShellNet* subNet, 
                                                                               UInt32* depositBuf,
                                                                               UInt32* maskBuf,
                                                                               UInt32 idriveVal, 
                                                                               UInt32 xdriveVal,
                                                                               const Touched& touchBuffer)
  : ShellNetPlaybackTwoStateWord(subNet, NULL, maskBuf, idriveVal, xdriveVal, touchBuffer),
    mEncodedValBuf(depositBuf)
{
  ST_ASSERT(mBitWidth == 1, getName());
  // Make the parent class store values in our scratch buffer
  mRawValBuf = 0;
  mDepositBuf = &mRawValBuf;
}

ShellNetPlaybackTwoStateClkStateOutput::~ShellNetPlaybackTwoStateClkStateOutput()
{
}

void ShellNetPlaybackTwoStateClkStateOutput::doClkDeposit(const UInt32* buf, CarbonModel* model)
{
  doDepositNoTouch(buf);
  saveStimulus();
  // We don't actually know if the underlying model value changed, so be pessimistic.
  addTouched(1, model);
}

CarbonStatus ShellNetPlaybackTwoStateClkStateOutput::deposit (const UInt32* buf, const UInt32*, CarbonModel* model)
{
  // check if it changed prior to doing the actual deposit
  
  if (buf)
    doClkDeposit(buf, model);
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackTwoStateClkStateOutput::depositWord (UInt32 buf, int index, UInt32, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
  if (stat == eCarbon_OK) {
    doClkDeposit(&buf, model);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackTwoStateClkStateOutput::doDepositRange(const UInt32* buf, 
                                                         int range_msb, int range_lsb,
                                                         CarbonModel* model)
{
  size_t index;
  size_t length;
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    // width is 1 because this is a clock, so don't worry about the
    // range.
    doClkDeposit(buf, model);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackTwoStateClkStateOutput::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  return doDepositRange(buf, range_msb, range_lsb, model);
}

void ShellNetPlaybackTwoStateClkStateOutput::fastDeposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  doClkDeposit(buf, model);
}

void ShellNetPlaybackTwoStateClkStateOutput::fastDepositWord (UInt32 buf, int, UInt32, CarbonModel* model)
{
  doClkDeposit(&buf, model);
}

void ShellNetPlaybackTwoStateClkStateOutput::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRange(buf, range_msb, range_lsb, model);
}

void ShellNetPlaybackTwoStateClkStateOutput::getEncodedDepositBuffers(UInt32** value, UInt32** drive) const
{
  // Return the encoded buffer here
  *value = mEncodedValBuf;
  *drive = NULL;
}

void ShellNetPlaybackTwoStateClkStateOutput::putToZero(CarbonModel* model)
{
  UInt32 zero = 0;
  doClkDeposit(&zero, model);
}

void ShellNetPlaybackTwoStateClkStateOutput::putToOnes(CarbonModel* model)
{
  UInt32 one = 1;
  doClkDeposit(&one, model);
}

CarbonStatus ShellNetPlaybackTwoStateClkStateOutput::setRange(int range_msb, int range_lsb, CarbonModel* model)
{
  UInt32 one = 1;
  return doDepositRange(&one, range_msb, range_lsb, model);
}

CarbonStatus ShellNetPlaybackTwoStateClkStateOutput::clearRange(int range_msb, int range_lsb, CarbonModel* model)
{
  UInt32 zero = 0;
  return doDepositRange(&zero, range_msb, range_lsb, model);
}

ShellNetPlaybackTwoStateWord* ShellNetPlaybackTwoStateClkStateOutput::instanceMe(ShellNet* subNet)
{
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  // Pass the real buffer, not mDepositBuf which points to the scratch copy!
  return new ShellNetPlaybackTwoStateClkStateOutput(subNet, mEncodedValBuf, maskBuf, mIDriveValue, mXDriveValue, mTouched);
}

bool ShellNetPlaybackTwoStateClkStateOutput::isSimpleClock() const
{
  // This is used to determine whether the change array reference
  // should be recalculated on all deposits.  This is handled
  // elsewhere for state output clocks, so return false here.
  return false;
}

bool ShellNetPlaybackTwoStateClkStateOutput::isEncodedValueChanged() const
{
  // We've changed if both touched flags are set, and the values are
  // different.
  UInt32 val = *mEncodedValBuf;
  bool changed = (val == (ePrevValueValid | eLastValueValid | ePrevValue)) ||
    (val == (ePrevValueValid | eLastValueValid | eLastValue));
  return changed;
}

void ShellNetPlaybackTwoStateClkStateOutput::saveStimulus()
{
  // State output clocks are always non-tristate scalars, which makes
  // this easy.

  // Make a local copy of the encoded value.  I don't know how well
  // all the dereferences would be optimized.
  UInt32 val = *mEncodedValBuf;

  if (isTouched()) {
    // Shift the last value/valid into the previous slots
    val = (val & (ShellNetReplay::eLastValueValid | ShellNetReplay::eLastValue)) << 1;
  } else {
    // This is the first access, so overwrite the buffer
    val = 0;
  }
  val |= ShellNetReplay::eLastValueValid | (*mDepositBuf & ShellNetReplay::eLastValue);
  *mEncodedValBuf = val;
}

/* ShellNetPlaybackTwoStateWordWriteOnly */
ShellNetPlaybackTwoStateWordWriteOnly::ShellNetPlaybackTwoStateWordWriteOnly(ShellNet* subNet, UInt32* depositBuf,
                                                                             UInt32* maskBuf,
                                                                             UInt32 idriveVal, 
                                                                             UInt32 xdriveVal,
                                                                             const Touched& touchBuffer)
  : ShellNetPlaybackTwoStateWord(subNet, depositBuf, maskBuf, idriveVal, xdriveVal, touchBuffer)
{}

ShellNetPlaybackTwoStateWordWriteOnly::~ShellNetPlaybackTwoStateWordWriteOnly()
{}

CarbonStatus ShellNetPlaybackTwoStateWordWriteOnly::examine(UInt32*, UInt32*, ExamineMode, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}
  
CarbonStatus ShellNetPlaybackTwoStateWordWriteOnly::examineWord (UInt32*, int, UInt32*, ExamineMode, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTwoStateWordWriteOnly::examineRange (UInt32*, int, int, UInt32*, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTwoStateWordWriteOnly::format(char*, size_t, 
                                                           CarbonRadix, NetFlags,
                                                           CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

ShellNetPlaybackTwoStateWord* ShellNetPlaybackTwoStateWordWriteOnly::instanceMe(ShellNet* subNet)
{
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  return new ShellNetPlaybackTwoStateWordWriteOnly(subNet, mDepositBuf, maskBuf, mIDriveValue, mXDriveValue, mTouched);
}

/* ShellNetPlaybackTwoStateWordReadOnly */
ShellNetPlaybackTwoStateWordReadOnly::ShellNetPlaybackTwoStateWordReadOnly(ShellNet* subNet, UInt32* depositBuf,
                                                                           UInt32* maskBuf,
                                                                           UInt32 idriveVal, 
                                                                           UInt32 xdriveVal,
                                                                           const Touched& touchBuffer)
  : ShellNetPlaybackTwoStateWord(subNet, depositBuf, maskBuf, idriveVal, xdriveVal, touchBuffer)
{}
  
ShellNetPlaybackTwoStateWordReadOnly::~ShellNetPlaybackTwoStateWordReadOnly()
{}

CarbonStatus ShellNetPlaybackTwoStateWordReadOnly::deposit (const UInt32*, const UInt32*, CarbonModel* model)
{
  doReportNotDepositable(model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTwoStateWordReadOnly::depositWord (UInt32, int, UInt32, CarbonModel* model)
{
  doReportNotDepositable(model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTwoStateWordReadOnly::depositRange (const UInt32*, int, int, const UInt32*, CarbonModel* model)
{
  doReportNotDepositable(model);
  return eCarbon_ERROR;
}

void ShellNetPlaybackTwoStateWordReadOnly::fastDeposit(const UInt32*, const UInt32*, CarbonModel* model)
{
  doReportNotDepositable(model);
}

void ShellNetPlaybackTwoStateWordReadOnly::fastDepositWord (UInt32, int, UInt32, CarbonModel* model)
{
  doReportNotDepositable(model);
}

void ShellNetPlaybackTwoStateWordReadOnly::fastDepositRange (const UInt32*, int, int, const UInt32*, CarbonModel* model)
{
  doReportNotDepositable(model);
}

ShellNetPlaybackTwoStateWord* ShellNetPlaybackTwoStateWordReadOnly::instanceMe(ShellNet* subNet)
{
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  return new ShellNetPlaybackTwoStateWordReadOnly(subNet, mDepositBuf, maskBuf, mIDriveValue, mXDriveValue, mTouched);
}


/* ShellNetPlaybackTwoStateA */

ShellNetPlaybackTwoStateA::ShellNetPlaybackTwoStateA(ShellNet* subNet, UInt32* depositBuf, UInt32* maskBuf, const UInt32* idriveVal, const UInt32* xdriveVal, const Touched& touched)
  : ShellNetPlayback(subNet, touched),
    mDepositBuf(depositBuf),
    mModelValue(NULL),
    mMaskBuf(maskBuf),
    mOwnMaskBuf(false),
    mIDriveValue(idriveVal),
    mXDriveValue(xdriveVal)
{
  if (mMaskBuf == NULL) {
    mMaskBuf = CARBON_ALLOC_VEC(UInt32, getNumWords());
    mOwnMaskBuf = true;
  }
}

ShellNetPlaybackTwoStateA::~ShellNetPlaybackTwoStateA()
{
  if (mOwnMaskBuf) {
    CARBON_FREE_VEC(mMaskBuf, UInt32, getNumWords());
  }
}

void ShellNetPlaybackTwoStateA::doExamine(UInt32* buf, UInt32* drive,
                                          ExamineMode mode) const
{
  const UInt32 numWords = getNumWords();
  if (buf)
    CarbonValRW::cpSrcToDest(buf, mModelValue, numWords);
  
  if (drive)
  {
    switch (mode)
    {
    case eIDrive:
      CarbonValRW::cpSrcToDest(drive, mIDriveValue, numWords);
      break;
    case eCalcDrive:
      // Two state is always driven either externally or internally
      CarbonValRW::setToZero(drive, numWords);
      break;
    case eXDrive:
      CarbonValRW::cpSrcToDest(drive, mXDriveValue, numWords);
      break;
    }
  }
}

CarbonStatus ShellNetPlaybackTwoStateA::examine(UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  doExamine(buf, drive, mode);
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackTwoStateA::examine (CarbonReal* buf, UInt32*, ExamineMode, CarbonModel*) const
{
  if (mBitWidth != 64)
    return eCarbon_ERROR;
  
  if (buf)
  {
    UInt64 tmpVal;
    CarbonValRW::cpSrcToDest(&tmpVal, mModelValue, 2);
    CarbonValRW::cpSrcToDest(buf, &tmpVal, 2);
  }
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackTwoStateA::examineWord(UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  if (buf)
    *buf = mModelValue[index];
  if (drive)
  {
    switch (mode)
    {
    case eIDrive:
      *drive = mIDriveValue[index];
      break;
    case eCalcDrive:
      *drive = 0;
      break;
    case eXDrive:
      *drive = mXDriveValue[index];
      break;
    }
  }
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackTwoStateA::examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index;
  size_t length;
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  
  if (stat == eCarbon_OK)
  {
    if (buf)
      CarbonValRW::cpSrcRangeToDest(buf, mModelValue, index, length);
    
    if (drive)
      CarbonValRW::cpSrcRangeToDest(drive, mIDriveValue, index, length);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackTwoStateA::examineValXDriveWord(UInt32* val, UInt32* drv, int index) const
{
  if (val)
    *val = mModelValue[index];
  if (drv)
    *drv = mXDriveValue[index];
  return eCarbon_OK;
}

void ShellNetPlaybackTwoStateA::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
    CarbonValRW::cpSrcToDest(xdrive, mXDriveValue, getNumWords());
}

CarbonStatus 
ShellNetPlaybackTwoStateA::format(char* valueStr, size_t len, 
                                  CarbonRadix strFormat, NetFlags,
                                  CarbonModel* model) const
{
  const UInt32* controlMask = mNet->getControlMask();
  return valueFormatString(valueStr, len, strFormat, mModelValue, (UInt32*) NULL, controlMask, mBitWidth, model);
}

CarbonStatus 
ShellNetPlaybackTwoStateA::formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel* model) const
{
  return twoStateFormatForceHelper(valueStr, len, radix, flags, mModelValue, forceMask, mBitWidth, model);
}

void ShellNetPlaybackTwoStateA::wordSanitize(UInt32 numWords)
{
  mDepositBuf[numWords - 1] &= mTailMask;
  mModelValue[numWords - 1] &= mTailMask;
}

void ShellNetPlaybackTwoStateA::widthSanitize()
{
  const UInt32 numWords = getNumWords();
  wordSanitize(numWords);
}

void ShellNetPlaybackTwoStateA::doDeposit(const UInt32* buf, CarbonModel* model)
{
  const UInt32 numWords = getNumWords();
  CarbonValRW::cpSrcToDest(mDepositBuf, buf, numWords);
  CarbonValRW::cpSrcToDest(mModelValue, buf, numWords);
  wordSanitize(numWords);
  setTouched(model);
}

CarbonStatus ShellNetPlaybackTwoStateA::deposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  if (buf)
    doDeposit(buf, model);
  return eCarbon_OK;
}

void ShellNetPlaybackTwoStateA::doDepositWord(UInt32 buf, int index, CarbonModel* model)
{
  mDepositBuf[index] = buf;
  mModelValue[index] = buf;
  widthSanitize();
  // Update mask
  UInt32 numWords = getNumWords();
  sUpdateMaskWordLarge(&mTouched, mMaskBuf, numWords, index, mTailMask);
  // Update only the touched flag, not the mask
  setTouchedOnly(model);
}

CarbonStatus ShellNetPlaybackTwoStateA::depositWord(UInt32 buf, int index, UInt32, CarbonModel* model)
{
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, 
                                                   getNumUInt32s() - 1, 
                                                   model);
  if (stat == eCarbon_OK)
    doDepositWord(buf, index, model);
  return eCarbon_OK;
}

CarbonStatus 
ShellNetPlaybackTwoStateA::doDepositRange(const UInt32* buf, 
                                          int range_msb, int range_lsb,
                                          CarbonModel* model)
{
  size_t index;
  size_t length;
  
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (((stat == eCarbon_OK) & (buf != NULL)))
  {
    CarbonValRW::cpSrcToDestRange(mDepositBuf, buf, index, length);
    CarbonValRW::cpSrcToDestRange(mModelValue, buf, index, length);
    // Update mask
    UInt32 numWords = getNumWords();
    sUpdateMaskRangeLarge(&mTouched, mMaskBuf, numWords, index, length);
    // Update only the touched flag, not the mask
    setTouchedOnly(model);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackTwoStateA::depositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  return doDepositRange(buf, range_msb, range_lsb, model);
}

void ShellNetPlaybackTwoStateA::fastDeposit(const UInt32* buf, const UInt32*, CarbonModel* model)
{
  doDeposit(buf, model);
}

void ShellNetPlaybackTwoStateA::fastDepositWord(UInt32 buf, int index, UInt32, CarbonModel* model)
{
  doDepositWord(buf, index, model);
}

void ShellNetPlaybackTwoStateA::fastDepositRange(const UInt32* buf, int range_msb, int range_lsb, const UInt32*, CarbonModel* model)
{
  doDepositRange(buf, range_msb, range_lsb, model);
}

ShellNetPlayback* ShellNetPlaybackTwoStateA::cloneStorage(ShellNet* primNet)
{
  ShellNetPlayback* ret = instanceMe(primNet);
  ret->putModelValueBuffers(mModelValue, NULL);
  return ret;
}

ShellNetPlaybackTwoStateA* ShellNetPlaybackTwoStateA::instanceMe(ShellNet* primNet)
{
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  return new ShellNetPlaybackTwoStateA(primNet, mDepositBuf, maskBuf, mIDriveValue, mXDriveValue, mTouched);
}

void ShellNetPlaybackTwoStateA::putModelValueBuffers(UInt32* value, 
                                                     UInt32* drive)
{
  ST_ASSERT(drive == NULL, getName());
  ST_ASSERT(value != NULL, getName());
  mModelValue = value;
}

void ShellNetPlaybackTwoStateA::getModelValueBuffers(UInt32** value, 
                                                     UInt32** drive) const
{
  *value = mModelValue;
  *drive = NULL;
}

void ShellNetPlaybackTwoStateA::getDepositBuffers(UInt32** value, 
                                                  UInt32** drive) const
{
  *value = mDepositBuf;
  *drive = NULL;
}

UInt32* ShellNetPlaybackTwoStateA::getMaskBuffer() const
{
  return mMaskBuf;
}

void ShellNetPlaybackTwoStateA::putToZero(CarbonModel* model)
{
  const UInt32 numWords = getNumWords();
  CarbonValRW::setToZero(mDepositBuf, numWords);
  CarbonValRW::setToZero(mModelValue, numWords);
  setTouched(model);
}

void ShellNetPlaybackTwoStateA::putToOnes(CarbonModel* model)
{
  const UInt32 numWords = getNumWords();
  CarbonValRW::setToOnes(mDepositBuf, numWords);
  CarbonValRW::setToOnes(mModelValue, numWords);
  wordSanitize(numWords);
  setTouched(model);
}

CarbonStatus ShellNetPlaybackTwoStateA::setRange(int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    CarbonValRW::setRangeToOnes(mDepositBuf, index, length);
    CarbonValRW::setRangeToOnes(mModelValue, index, length);
    // Update mask
    UInt32 numWords = getNumWords();
    sUpdateMaskRangeLarge(&mTouched, mMaskBuf, numWords, index, length);
    // Update only the touched flag, not the mask
    setTouchedOnly(model);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackTwoStateA::clearRange(int range_msb, int range_lsb, CarbonModel* model)
{
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    CarbonValRW::setRangeToZero(mDepositBuf, index, length);
    CarbonValRW::setRangeToZero(mModelValue, index, length);
    // Update mask
    UInt32 numWords = getNumWords();
    sUpdateMaskRangeLarge(&mTouched, mMaskBuf, numWords, index, length);
    // Update only the touched flag, not the mask
    setTouchedOnly(model);
  }
  return stat;
}

bool ShellNetPlaybackTwoStateA::isDataNonZero() const
{
  return ! CarbonValRW::isZero(mModelValue, getNumWords());
}

void ShellNetPlaybackTwoStateA::runValueChangeCB(CarbonNetValueCBData* cbData, 
                                                 UInt32* newVal, 
                                                 UInt32* newDrv,
                                                 CarbonTriValShadow* fullShadow,
                                                 CarbonModel* model) const
{
  // Do an idrive examine here, since we do not have a drive to compare
  doExamine(newVal, newDrv, eIDrive);
  
  UInt32 numWords = getNumWords();
  if (memcmp(fullShadow->mValue, newVal, numWords * sizeof(UInt32)) != 0)
  {
    cbData->executeCB(model->getObjectID (), newVal, newDrv);
    CarbonValRW::cpSrcToDest(fullShadow->mValue, newVal, numWords);
  }
}

/* ShellNetPlaybackTwoStateAWriteOnly */

ShellNetPlaybackTwoStateAWriteOnly::ShellNetPlaybackTwoStateAWriteOnly(ShellNet* subNet, UInt32* depositBuf, UInt32* maskBuf, const UInt32* idriveVal, 
                                                                       const UInt32* xdriveVal, const Touched& touchBuffer)
  : ShellNetPlaybackTwoStateA(subNet, depositBuf, maskBuf, idriveVal, xdriveVal, touchBuffer)
{}

ShellNetPlaybackTwoStateAWriteOnly::~ShellNetPlaybackTwoStateAWriteOnly()
{}

CarbonStatus ShellNetPlaybackTwoStateAWriteOnly::examine(UInt32*, UInt32*, ExamineMode, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}
  
CarbonStatus ShellNetPlaybackTwoStateAWriteOnly::examineWord (UInt32*, int, UInt32*, ExamineMode, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTwoStateAWriteOnly::examineRange (UInt32*, int, int, UInt32*, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTwoStateAWriteOnly::format(char*, size_t, 
                                                        CarbonRadix, NetFlags,
                                                        CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

ShellNetPlaybackTwoStateA* ShellNetPlaybackTwoStateAWriteOnly::instanceMe(ShellNet* primNet)
{
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  return new ShellNetPlaybackTwoStateAWriteOnly(primNet, mDepositBuf, maskBuf, mIDriveValue, mXDriveValue, mTouched);
}

/* ShellNetPlaybackTwoStateAReadOnly */

ShellNetPlaybackTwoStateAReadOnly::ShellNetPlaybackTwoStateAReadOnly(ShellNet* subNet, UInt32* depositBuf, UInt32* maskBuf, const UInt32* idriveVal, 
                                                                     const UInt32* xdriveVal, const Touched& touchBuffer)
  : ShellNetPlaybackTwoStateA(subNet, depositBuf, maskBuf, idriveVal, xdriveVal, touchBuffer)
{}

ShellNetPlaybackTwoStateAReadOnly::~ShellNetPlaybackTwoStateAReadOnly()
{}

CarbonStatus ShellNetPlaybackTwoStateAReadOnly::deposit (const UInt32*, const UInt32*, CarbonModel* model)
{
  doReportNotDepositable(model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTwoStateAReadOnly::depositWord (UInt32, int, UInt32, CarbonModel* model)
{
  doReportNotDepositable(model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTwoStateAReadOnly::depositRange (const UInt32*, int, int, const UInt32*, CarbonModel* model)
{
  doReportNotDepositable(model);
  return eCarbon_ERROR;
}

void ShellNetPlaybackTwoStateAReadOnly::fastDeposit(const UInt32*, const UInt32*, CarbonModel* model)
{
  doReportNotDepositable(model);
}

void ShellNetPlaybackTwoStateAReadOnly::fastDepositWord (UInt32, int, UInt32, CarbonModel* model)
{
  doReportNotDepositable(model);
}

void ShellNetPlaybackTwoStateAReadOnly::fastDepositRange (const UInt32*, int, int, const UInt32*, CarbonModel* model)
{
  doReportNotDepositable(model);
}

ShellNetPlaybackTwoStateA* ShellNetPlaybackTwoStateAReadOnly::instanceMe(ShellNet* primNet)
{
  UInt32* maskBuf = mOwnMaskBuf ? NULL : mMaskBuf;
  return new ShellNetPlaybackTwoStateAReadOnly(primNet, mDepositBuf, maskBuf, mIDriveValue, mXDriveValue, mTouched);
}

/* ShellNetPlaybackTristate */

ShellNetPlaybackTristate::ShellNetPlaybackTristate(ShellNet* subNet, const Touched& touched)
  : ShellNetPlayback(subNet, touched)
{
}

ShellNetPlaybackTristate::ShellNetPlaybackTristate(ShellNet* subNet, 
                                                   UInt32* depositValBuf, 
                                                   UInt32* depositDrvBuf, const Touched& touched)
  : ShellNetPlayback(subNet, touched), mTri(NULL)
{
  mTri = new ShellNetTristateA(depositValBuf, depositDrvBuf);
}

ShellNetPlaybackTristate::~ShellNetPlaybackTristate()
{
  delete mTri;
}

void ShellNetPlaybackTristate::doExamine(UInt32* buf, UInt32* drive, ExamineMode mode) const
{
  switch(mode)
  {
  case eIDrive:
    mTri->getExamineValue(buf, drive, mBitWidth);
    break;
  case eCalcDrive:
    mTri->getCalculatedValue(buf, drive, mBitWidth);
    break;
  case eXDrive:
    mTri->getExternalValues(buf, drive, mBitWidth);
    break;
  }
}

CarbonStatus ShellNetPlaybackTristate::examine (UInt32* buf, UInt32* drive, ExamineMode mode, CarbonModel*) const
{
  doExamine(buf, drive, mode);
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackTristate::examine (CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const
{
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTristate::examineWord (UInt32* buf, int index, UInt32* drive, ExamineMode mode, CarbonModel* model) const
{
  const UInt32 numWords = getNumWords();
  CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
  if (stat == eCarbon_OK)
  {
    switch (mode)
    {
    case eIDrive:
      mTri->getExamineValueWord(buf, drive, index, mBitWidth);
      break;
    case eCalcDrive:
      mTri->getExamineValueWord(buf, drive, index, mBitWidth);
      *drive = mTri->getCalcDriveWord(index, mBitWidth);
      break;
    case eXDrive:
      mTri->copyValXDriveWord(buf, drive, index, mBitWidth);
      break;
    }
  }
  return stat;
}

CarbonStatus ShellNetPlaybackTristate::examineRange (UInt32* buf, int range_msb, int range_lsb, UInt32* drive, CarbonModel* model) const
{
  size_t index;
  size_t length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
    mTri->getExamineValueRange(buf, drive, index, length, mBitWidth);
  return stat;
}

CarbonStatus ShellNetPlaybackTristate::examineValXDriveWord(UInt32* val, UInt32* drv, int index) const
{
  mTri->copyValXDriveWord(val, drv, index, mBitWidth);
  return eCarbon_OK;
}

void ShellNetPlaybackTristate::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
  {
    UInt32 numWords = getNumWords();
    CarbonValRW::setToOnes(xdrive, numWords);
    xdrive[numWords - 1] &= mTailMask;
  }
}

CarbonStatus 
ShellNetPlaybackTristate::format(char* valueStr, size_t len, 
                                  CarbonRadix strFormat, NetFlags flags,
                                  CarbonModel* model) const
{
  UInt32* dummyVal;
  UInt32* xdrive;
  UInt32* modelValue;
  UInt32* modelDrive;
  mTri->getAllPtrs(&dummyVal, &xdrive, &modelValue, &modelDrive);

  bool pulled = isPulled(flags);
  
  const UInt32* controlMask = mNet->getControlMask();
  return doTristateFormatPrim(valueStr, len, strFormat, 
                              modelValue, xdrive, modelDrive, (UInt32*) NULL, controlMask, mBitWidth, pulled, model);
}

CarbonStatus 
ShellNetPlaybackTristate::formatForce(char* valueStr, size_t len, CarbonRadix radix, NetFlags flags, ShellNet* forceMask, CarbonModel* model) const
{
  UInt32* forceMaskValue;
  UInt32* forceMaskDrive /*always NULL*/;
  genericGetModelValueBuffers(forceMask, &forceMaskValue, &forceMaskDrive);
  
  bool pulled = isPulled(flags);
  
  UInt32* dummyVal;
  UInt32* xdrive;
  UInt32* modelValue;
  UInt32* modelDrive;
  mTri->getAllPtrs(&dummyVal, &xdrive, &modelValue, &modelDrive);
  const UInt32* controlMask = mNet->getControlMask();
  return doTristateFormatPrim(valueStr, len, radix, 
                              modelValue, xdrive, modelDrive, forceMaskValue, controlMask, mBitWidth, pulled, model);
}

void ShellNetPlaybackTristate::doFastDeposit(const UInt32* buf, 
                                             const UInt32* drv,
                                             CarbonModel* model)
{
  if (drv == NULL)
    mTri->assertExternalDrive(mBitWidth);

  mTri->assign(buf, drv, mBitWidth);
  setTouched(model);
}

void ShellNetPlaybackTristate::doFastDepositWord(UInt32 buf, int index,
                                                 UInt32 drv,
                                                 CarbonModel* model)
{
  mTri->assignWord(buf, drv, index, mBitWidth);
  setTouched(model);
}

CarbonStatus 
ShellNetPlaybackTristate::doFastDepositRange(const UInt32* val, 
                                             int range_msb, 
                                             int range_lsb, 
                                             const UInt32* drv,
                                             CarbonModel* model)
{
  size_t index, length;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK) {
    mTri->assignRange(val, drv, index, length, mBitWidth);
    setTouched(model);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackTristate::deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    doFastDeposit(buf, drive, model);
    return eCarbon_OK;
  }
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTristate::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    const UInt32 numWords = getNumWords();
    CarbonStatus stat = ShellGlobal::carbonTestIndex(index, 0, numWords - 1, model);
    if (stat == eCarbon_OK)
      doFastDepositWord(buf, index, drive, model);
    return stat;
  }
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTristate::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    return doFastDepositRange(buf, range_msb, range_lsb, drive, model);
  }
  return eCarbon_ERROR;
}

void ShellNetPlaybackTristate::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  doFastDeposit(buf, drive, model);
}

void ShellNetPlaybackTristate::fastDepositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  doFastDepositWord(buf, index, drive, model);
}

void ShellNetPlaybackTristate::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  doFastDepositRange(buf, range_msb, range_lsb, drive, model);
}

ShellNetPlayback* ShellNetPlaybackTristate::cloneStorage(ShellNet* primNet)
{
  UInt32* modelValBuf;
  UInt32* modelDrvBuf;
  UInt32* depositValBuf;
  UInt32* depositDrvBuf;
  
  mTri->getAllPtrs(&depositValBuf, &depositDrvBuf, &modelValBuf, &modelDrvBuf);
  ShellNetPlayback* ret = instanceMe(primNet, depositValBuf, depositDrvBuf);
  ret->putModelValueBuffers(modelValBuf, modelDrvBuf);
  return ret;
}

ShellNetPlaybackTristate* ShellNetPlaybackTristate::instanceMe(ShellNet* primNet, UInt32* depositValBuf, UInt32* depositDrvBuf)
{
  return new ShellNetPlaybackTristate(primNet, depositValBuf, depositDrvBuf, mTouched);
}

void ShellNetPlaybackTristate::putModelValueBuffers(UInt32* value, 
                                                    UInt32* drive)
{
  ST_ASSERT(drive != NULL, getName());
  ST_ASSERT(value != NULL, getName());
  
  // Check that the model value buffers passed in our the same as the
  // deposit buffers. A non-bidi tristate should not have deposit
  // buffers.
  UInt32* depositValBuf;
  UInt32* depositDrvBuf;
  mTri->getIPtrs(&depositValBuf, &depositDrvBuf);
  
  ST_ASSERT(value == depositValBuf, getName());
  ST_ASSERT(drive == depositDrvBuf, getName());
}

void ShellNetPlaybackTristate::getModelValueBuffers(UInt32** value, 
                                                    UInt32** drive) const
{
  mTri->getIPtrs(value, drive);
}

void ShellNetPlaybackTristate::getDepositBuffers(UInt32** value, 
                                                 UInt32** drive) const
{
  mTri->getIPtrs(value, drive);
}

void ShellNetPlaybackTristate::setRawToUndriven(CarbonModel* model)
{
  mTri->undrivenInit(mBitWidth);
  setTouched(model);
}

bool ShellNetPlaybackTristate::setToUndriven(CarbonModel* model)
{
  setTouched(model);
  return mTri->deassertExternalDrive(mBitWidth);
}

bool ShellNetPlaybackTristate::setToDriven(CarbonModel* model)
{
  setTouched(model);
  return mTri->assertExternalDrive(mBitWidth);
}

bool ShellNetPlaybackTristate::resolveXdrive(CarbonModel* model)
{
  setTouched(model);
  return mTri->resolveExternalDrive(mBitWidth);
}

void ShellNetPlaybackTristate::runValueChangeCB(CarbonNetValueCBData* cbData, 
                                                UInt32* newVal, 
                                                UInt32* newDrv,
                                                CarbonTriValShadow* fullShadow,
                                                CarbonModel* model) const
{
  // Do an examine using the mode for the specific callback. For some
  // callbacks it must do a calc drive examine here, to compare the
  // drive as well. For others it may just look at the Carbon value
  // and drive.
  doExamine(newVal, newDrv, cbData->getExamineMode());
  
  UInt32 numWords = getNumWords();
  if ((memcmp(fullShadow->mValue, newVal, numWords * sizeof(UInt32)) != 0) ||
      (memcmp(fullShadow->mDrive, newDrv, numWords * sizeof(UInt32)) != 0))
  {
    // update the shadow
    CarbonValRW::cpSrcToDest(fullShadow->mValue, newVal, numWords);
    CarbonValRW::cpSrcToDest(fullShadow->mDrive, newDrv, numWords);

    // examine with an idrive now
    doExamine(newVal, newDrv, eIDrive);
    cbData->executeCB(model->getObjectID (), newVal, newDrv);
  }
}

/* ShellNetPlaybackTristateWriteOnly */
ShellNetPlaybackTristateWriteOnly::ShellNetPlaybackTristateWriteOnly(ShellNet* subNet, 
                                                                     UInt32* depositValBuf, 
                                                                     UInt32* depositDrvBuf, 
                                                                     const Touched& touchBuffer)
  : ShellNetPlaybackTristate(subNet, depositValBuf, depositDrvBuf, touchBuffer)
{}

ShellNetPlaybackTristateWriteOnly::~ShellNetPlaybackTristateWriteOnly()
{}

CarbonStatus ShellNetPlaybackTristateWriteOnly::examine(UInt32*, UInt32*, ExamineMode, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}
  
CarbonStatus ShellNetPlaybackTristateWriteOnly::examineWord (UInt32*, int, UInt32*, ExamineMode, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTristateWriteOnly::examineRange (UInt32*, int, int, UInt32*, CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTristateWriteOnly::format(char*, size_t, 
                                                       CarbonRadix, NetFlags,
                                                       CarbonModel* model) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), model);
  return eCarbon_ERROR;
}

ShellNetPlaybackTristate* ShellNetPlaybackTristateWriteOnly::instanceMe(ShellNet* primNet, UInt32* depositValBuf, UInt32* depositDrvBuf)
{
  return new ShellNetPlaybackTristateWriteOnly(primNet, depositValBuf, depositDrvBuf, mTouched);
}

/* ShellNetPlaybackTristateReadOnly */
ShellNetPlaybackTristateReadOnly::ShellNetPlaybackTristateReadOnly(ShellNet* subNet, 
                                                                   UInt32* depositValBuf, 
                                                                   UInt32* depositDrvBuf, 
                                                                   const Touched& touchBuffer)
  : ShellNetPlaybackTristate(subNet, depositValBuf, depositDrvBuf, touchBuffer)
{}

ShellNetPlaybackTristateReadOnly::~ShellNetPlaybackTristateReadOnly()
{}

CarbonStatus ShellNetPlaybackTristateReadOnly::deposit (const UInt32*, const UInt32*, CarbonModel* model)
{
  doReportNotDepositable(model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTristateReadOnly::depositWord (UInt32, int, UInt32, CarbonModel* model)
{
  doReportNotDepositable(model);
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackTristateReadOnly::depositRange (const UInt32*, int, int, const UInt32*, CarbonModel* model)
{
  doReportNotDepositable(model);
  return eCarbon_ERROR;
}

void ShellNetPlaybackTristateReadOnly::fastDeposit(const UInt32*, const UInt32*, CarbonModel* model)
{
  doReportNotDepositable(model);
}

void ShellNetPlaybackTristateReadOnly::fastDepositWord (UInt32, int, UInt32, CarbonModel* model)
{
  doReportNotDepositable(model);
}

void ShellNetPlaybackTristateReadOnly::fastDepositRange (const UInt32*, int, int, const UInt32*, CarbonModel* model)
{
  doReportNotDepositable(model);
}

ShellNetPlaybackTristate* ShellNetPlaybackTristateReadOnly::instanceMe(ShellNet* primNet, UInt32* depositValBuf, UInt32* depositDrvBuf)
{
  return new ShellNetPlaybackTristateReadOnly(primNet, depositValBuf, depositDrvBuf, mTouched);
}

/* ShellNetPlaybackBidirect */

ShellNetPlaybackBidirect::ShellNetPlaybackBidirect(ShellNet* subNet, 
                                                   UInt32* depositValBuf, 
                                                   UInt32* depositDrvBuf, const Touched& touched)
  : ShellNetPlaybackTristate(subNet, touched)
{
  /* 
     Put the external data in a ShellNetBidirectA by themselves. This
     is wrong if mTri were to be used now. But it isn't. mTri will
     be reallocated during putModelValueBuffers. Otherwise, there is
     no place to hold the external values at this time, and this is
     the only time we can get the values from CarbonModel. It is a
     requirement that putModelValueBuffers() gets called anyway at
     some point after the construction and before use.
  */
  mTri = new ShellNetBidirectA(NULL, NULL, depositValBuf, depositDrvBuf);
}

ShellNetPlaybackBidirect::~ShellNetPlaybackBidirect()
{}

void ShellNetPlaybackBidirect::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
  {
    UInt32 numWords = getNumWords();
    for (UInt32 i = 0; i < numWords; ++i)
      xdrive[i] = mTri->getXDriveWord(i, mBitWidth);
  }
}

ShellNetPlayback* ShellNetPlaybackBidirect::cloneStorage(ShellNet* primNet)
{
  UInt32* modelValBuf;
  UInt32* modelDrvBuf;
  UInt32* depositValBuf;
  UInt32* depositDrvBuf;
  
  mTri->getAllPtrs(&depositValBuf, &depositDrvBuf, &modelValBuf, &modelDrvBuf);
  ShellNetPlayback* ret = new ShellNetPlaybackBidirect(primNet, depositValBuf, depositDrvBuf, mTouched);
  ret->putModelValueBuffers(modelValBuf, modelDrvBuf);
  return ret;
}

void ShellNetPlaybackBidirect::putModelValueBuffers(UInt32* value, 
                                                    UInt32* drive)
{
  ST_ASSERT(drive != NULL, getName());
  ST_ASSERT(value != NULL, getName());
  
  // grab the external value component saved away at construction
  UInt32* depositValBuf;
  UInt32* depositDrvBuf;
  UInt32* dummyIVal;
  UInt32* dummyIDrv;
  mTri->getAllPtrs(&depositValBuf, &depositDrvBuf, &dummyIVal, &dummyIDrv);
  
  // Reallocate the ShellNetBidirectA with proper pointer association
  delete mTri;
  
  mTri = new ShellNetBidirectA(value, drive, depositValBuf, depositDrvBuf);
}

void ShellNetPlaybackBidirect::getDepositBuffers(UInt32** value, 
                                                 UInt32** drive) const
{
  UInt32* dummyIVal;
  UInt32* dummyIDrv;
  mTri->getAllPtrs(value, drive, &dummyIVal, &dummyIDrv);
}

int ShellNetPlaybackBidirect::hasDriveConflict() const
{
  return mTri->hasDriveConflict(mBitWidth);
}

int ShellNetPlaybackBidirect::hasDriveConflictRange(SInt32 range_msb, SInt32 range_lsb) const
{
  size_t index, length;
  int ret = 0;
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, NULL);
  if (stat == eCarbon_OK)
  {
    if (mTri->hasDriveConflictRange(index, length, mBitWidth))
      ret = 1;
  }
  else
    ret = -1;
  return ret;

}

ShellNetPlaybackTristate* ShellNetPlaybackBidirect::instanceMe(ShellNet* subNet, UInt32* depositValBuf, UInt32* depositDrvBuf)
{
  return new ShellNetPlaybackBidirect(subNet, depositValBuf, depositDrvBuf, mTouched);
}

ShellNetPlaybackBidirectClk::ShellNetPlaybackBidirectClk(ShellNet* subNet, 
                                                         UInt32* depositValBuf, 
                                                         UInt32* depositDrvBuf,
                                                         UInt32* valShadow,
                                                         UInt32* drvShadow,
                                                         const Touched& touchBuffer)
  : ShellNetPlaybackBidirect(subNet, depositValBuf, depositDrvBuf, touchBuffer),
    mValBuf(depositValBuf), mDrvBuf(depositDrvBuf), mValShadow(valShadow), mDrvShadow(drvShadow)
{}

ShellNetPlaybackBidirectClk::~ShellNetPlaybackBidirectClk()
{
}

void ShellNetPlaybackBidirectClk::saveCurrentValue()
{
  *mValShadow = *mValBuf;
  *mDrvShadow = *mDrvBuf;
}

void ShellNetPlaybackBidirectClk::calcTouched(CarbonModel* model)
{
  const UInt32 changed = (*mValShadow != *mValBuf) | (*mDrvShadow != *mDrvBuf);
  addTouched(changed, model);
}

void ShellNetPlaybackBidirectClk::doClkDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  saveCurrentValue();

  if (!drive)
    setToDriven(model);
  mTri->assign(buf, drive, mBitWidth);

  calcTouched(model);
}

CarbonStatus ShellNetPlaybackBidirectClk::deposit (const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    doClkDeposit(buf, drive, model);
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackBidirectClk::depositWord (UInt32 buf, int index, UInt32 drive, CarbonModel* model)
{
  CarbonStatus stat = eCarbon_ERROR;
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
  {
    stat = ShellGlobal::carbonTestIndex(index, 0, 0, model);
    if (stat == eCarbon_OK) {
      doClkDeposit(&buf, &drive, model);
    }
  }
  return stat;
}

CarbonStatus ShellNetPlaybackBidirectClk::doDepositRange(const UInt32* buf, 
                                                         int range_msb, int range_lsb,
                                                         const UInt32* drive,
                                                         CarbonModel* model)
{
  size_t index;
  size_t length;
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, model);
  if (stat == eCarbon_OK)
  {
    // width is 1 because this is a clock, so don't worry about the
    // range.
    doClkDeposit(buf, drive, model);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackBidirectClk::depositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  const ShellDataBOM* bom;
  if (isDepositAllowed(model, &bom))
    return doDepositRange(buf, range_msb, range_lsb, drive, model);
  return eCarbon_ERROR;
}

void ShellNetPlaybackBidirectClk::fastDeposit(const UInt32* buf, const UInt32* drive, CarbonModel* model)
{
  doClkDeposit(buf, drive, model);
}

void ShellNetPlaybackBidirectClk::fastDepositWord (UInt32 buf, int, UInt32 drive, CarbonModel* model)
{
  doClkDeposit(&buf, &drive, model);
}

void ShellNetPlaybackBidirectClk::fastDepositRange (const UInt32* buf, int range_msb, int range_lsb, const UInt32* drive, CarbonModel* model)
{
  doDepositRange(buf, range_msb, range_lsb, drive, model);
}


bool ShellNetPlaybackBidirectClk::setToUndriven(CarbonModel* model)
{
  saveCurrentValue();
  bool ret = mTri->deassertExternalDrive(mBitWidth);
  calcTouched(model);
  return ret;
}

bool ShellNetPlaybackBidirectClk::setToDriven(CarbonModel* model)
{
  saveCurrentValue();
  bool ret = mTri->assertExternalDrive(mBitWidth);
  calcTouched(model);
  return ret;
}

bool ShellNetPlaybackBidirectClk::resolveXdrive(CarbonModel* model)
{
  saveCurrentValue();
  bool ret = mTri->resolveExternalDrive(mBitWidth);
  calcTouched(model);
  return ret;
}

ShellNetPlaybackTristate* ShellNetPlaybackBidirectClk::instanceMe(ShellNet* subNet, UInt32* depositValBuf, UInt32* depositDrvBuf)
{
  return new ShellNetPlaybackBidirectClk(subNet, depositValBuf, depositDrvBuf, mValShadow, mDrvShadow, mTouched);
}

bool ShellNetPlaybackBidirectClk::isSimpleClock() const
{
  return true;
}

/* ShellNetPlaybackMem */

ShellNetPlaybackMem::ShellNetPlaybackMem(ShellNet* mem, UInt32 externalIndex, ReplayMemChangeMap* addrChanges, SInt32HashSet* memChanges)
  : ShellNetPlayback(mem), mAddrChanges(addrChanges), 
    mMemChanges(memChanges), mExternalIndex(externalIndex)
{
  CarbonModelMemory* modelMem = mem->castModelMemory();
  ST_ASSERT(modelMem, mem->getName());
  mCarbonModel = modelMem->getCarbonModel();
}

ShellNetPlaybackMem::~ShellNetPlaybackMem()
{}

void ShellNetPlaybackMem::notifyWriteAddress(CarbonMemAddrT address, const UInt32* value)
{
  mMemChanges->insert(mExternalIndex);
  UInt32* valBuf = mAddrChanges->appendAddr(address);
  CarbonValRW::cpSrcToDest(valBuf, value, getNumWords());
  mCarbonModel->getHookup()->setSeenDeposit();
}

int ShellNetPlaybackMem::formatRow(char* valueStr, size_t len, CarbonRadix strFormat, const UInt32* data) const
{
  int ret = 0;
  switch(strFormat)
  {
  case eCarbonBin:
    ret = CarbonValRW::writeBinValToStr(valueStr, len, data, mBitWidth);
    break;
  case eCarbonHex:
    ret = CarbonValRW::writeHexValToStr(valueStr, len, data, mBitWidth);
    break;
  case eCarbonOct:
    ret = CarbonValRW::writeOctValToStr(valueStr, len, data, mBitWidth);
    break;
  case eCarbonDec:
    ret = CarbonValRW::writeDecValToStr(valueStr, len, data, true, mBitWidth);
    break;
  case eCarbonUDec:
    ret = CarbonValRW::writeDecValToStr(valueStr, len, data, false, mBitWidth);
    break;
  }
  
  return ret;
}

void ShellNetPlaybackMem::putModelValueBuffers(UInt32*, UInt32*)
{
  ST_ASSERT(0, getName());
}

void ShellNetPlaybackMem::getModelValueBuffers(UInt32**, UInt32**) const
{
  ST_ASSERT(0, getName());
}

void ShellNetPlaybackMem::getDepositBuffers(UInt32**, UInt32**) const
{
  ST_ASSERT(0, getName());
}

CarbonStatus ShellNetPlaybackMem::deposit(const UInt32*, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackMem::depositWord(UInt32, int, UInt32, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackMem::depositRange(const UInt32*, int, int, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void ShellNetPlaybackMem::fastDeposit(const UInt32*, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
}

void ShellNetPlaybackMem::fastDepositWord (UInt32, int, UInt32, CarbonModel*)
{
  ST_ASSERT(0, getName());
}

void ShellNetPlaybackMem::fastDepositRange(const UInt32*, int, int, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
}

/* ShellNetPlaybackMem1 */

ShellNetPlaybackMem1::ShellNetPlaybackMem1(ShellNet* memNet, UInt32 externalIndex, ReplayMemChangeMap* addrChanges, SInt32HashSet* memChanges, SInt32UInt32Map* memStorage)
  : ShellNetPlaybackMem(memNet, externalIndex, addrChanges, memChanges), mMem(memStorage)
{}

ShellNetPlaybackMem1::~ShellNetPlaybackMem1()
{}

CarbonStatus ShellNetPlaybackMem1::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  UInt32 cleanVal = *buf & mTailMask;
  (*mMem)[address] = cleanVal;
  notifyWriteAddress(address, &cleanVal);
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackMem1::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int)
{
  UInt32 cleanVal = buf & mTailMask;
  (*mMem)[address] = cleanVal;
  notifyWriteAddress(address, &cleanVal);
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackMem1::depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb)
{
  size_t index;
  size_t length;

  CarbonStatus stat = sCalcIndexLength(getRowMSB(), getRowLSB(), range_msb, range_lsb, &index, &length, mMemNet->getCarbonModel());
  if (((stat == eCarbon_OK) & (src != NULL)))
  {
    UInt32 buf = (*mMem)[address];
    CarbonValRW::cpSrcToDestRange(&buf, src, index, length);
    (*mMem)[address] = buf;
    notifyWriteAddress(address, &buf);
  }
  return stat;
}

UInt32 ShellNetPlaybackMem1::getRowValue(CarbonMemAddrT address) const
{
  UInt32 data = 0;
  SInt32UInt32Map::MapEntry* entry = mMem->findEntry(address);
  if (entry)
    data = entry->mKeyVal.second;
  return data;
}

CarbonStatus ShellNetPlaybackMem1::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  *buf = getRowValue(address);
  return eCarbon_OK;
}

UInt32 ShellNetPlaybackMem1::examineMemoryWord(CarbonMemAddrT address, int) const
{
  return getRowValue(address);
}

CarbonStatus ShellNetPlaybackMem1::examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, 
                                                      int range_msb, int range_lsb) const
{
  size_t index;
  size_t length;
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, mMemNet->getCarbonModel());
  
  if (stat == eCarbon_OK)
  {
    UInt32 buf = getRowValue(address);
    CarbonValRW::cpSrcRangeToDest(dst, &buf, index, length);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackMem1::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  UInt32 data = getRowValue(address);
  CarbonStatus stat = eCarbon_OK;
  if (formatRow(valueStr, len, strFormat, &data) == -1)
  {
    ShellGlobal::reportInsufficientBufferLength(len, mMemNet->getCarbonModel());
    stat = eCarbon_ERROR;
  }
  
  return stat;
}

void ShellNetPlaybackMem1::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  int bufSize = mBitWidth + 2;
  char* valueStr = CARBON_ALLOC_VEC(char, bufSize);
  
  UInt32 data = getRowValue(address);
  
  int actualSize = formatRow(valueStr, bufSize, strFormat, &data);
  if (actualSize >= 0) {
    valueStr[actualSize] = '\n';
    valueStr[actualSize + 1] = '\0';
    out->writeStr(valueStr, actualSize + 1);
  }
  
  CARBON_FREE_VEC(valueStr, char, bufSize);
}

CarbonStatus ShellNetPlaybackMem1::notifyReadMem(const char* filename, CarbonRadix radix)
{
  CarbonMemAddrT lo;
  CarbonMemAddrT hi;
  ShellMemory::sGetLowHiAddrs(mMemNet->getLeftAddr(), mMemNet->getRightAddr(), &lo, &hi);
  // Readmem without specified start/end addresses doesn't check range of file
  CarbonStatus stat = notifyReadMem(filename, lo, hi, radix, false);
  return stat;
}

CarbonStatus ShellNetPlaybackMem1::notifyReadMem(const char* filename, SInt64 startAddr, SInt64 endAddr, CarbonRadix radix, bool checkRange)
{
  CarbonOpaque id = mMemNet->getCarbonModel()->getHandle ();
  HDLReadMemX memFile(filename, radix == eCarbonHex, mMemNet->getRowBitWidth(),
                      startAddr, endAddr, checkRange, id);
  if (! memFile.openFile())
    return eCarbon_ERROR;

  HDLReadMemXResult result;
  SInt64 address;
  UInt32 row;
  while ((result = memFile.getNextWord(&address, &row)) == eRMValidData)
  {
    (*mMem)[address] = row;
    notifyWriteAddress(address, &row);
  }
  
  memFile.closeFile();
 
  CarbonStatus stat = eCarbon_OK;
  if (result != eRMEnd)
    stat = eCarbon_ERROR;
  
  return stat;
}

CarbonStatus ShellNetPlaybackMem1::readmemh(const char* filename)
{
  return notifyReadMem(filename, eCarbonHex);
}

CarbonStatus ShellNetPlaybackMem1::readmemb(const char* filename)
{
  return notifyReadMem(filename, eCarbonBin);
}

CarbonStatus ShellNetPlaybackMem1::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  return notifyReadMem(filename, startAddr, endAddr, eCarbonHex, true);
}

CarbonStatus ShellNetPlaybackMem1::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  return notifyReadMem(filename, startAddr, endAddr, eCarbonBin, true);
}


ShellNetPlayback* ShellNetPlaybackMem1::cloneStorage(ShellNet* primNet)
{
  ST_ASSERT(primNet->castMemory() != NULL, primNet->getName());
  ShellNetPlaybackMem1* mem = new ShellNetPlaybackMem1(primNet, mExternalIndex, mAddrChanges, mMemChanges, mMem);
  return mem;
}

void ShellNetPlaybackMem1::backDoorWrite(SInt32 address, const DynBitVector* valBuf)
{
  (*mMem)[address] = valBuf->value();
}

/* ShellNetPlaybackMem1WriteOnly */
ShellNetPlaybackMem1WriteOnly::ShellNetPlaybackMem1WriteOnly(ShellNet* memNet, UInt32 externalIndex, ReplayMemChangeMap* addrChanges, 
                                                             SInt32HashSet* memChanges, SInt32UInt32Map* memStorage)
  : ShellNetPlaybackMem1(memNet, externalIndex, addrChanges, memChanges, memStorage)
{}

ShellNetPlaybackMem1WriteOnly::~ShellNetPlaybackMem1WriteOnly()
{}

CarbonStatus ShellNetPlaybackMem1WriteOnly::examineMemory(CarbonMemAddrT, UInt32*) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mMemNet->getCarbonModel());
  return eCarbon_ERROR;
}

UInt32 ShellNetPlaybackMem1WriteOnly::examineMemoryWord(CarbonMemAddrT, int) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mMemNet->getCarbonModel());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackMem1WriteOnly::examineMemoryRange(CarbonMemAddrT , UInt32*, int, int) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mMemNet->getCarbonModel());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackMem1WriteOnly::formatMemory(char*, size_t, CarbonRadix , CarbonMemAddrT) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mMemNet->getCarbonModel());
  return eCarbon_ERROR;
}

void ShellNetPlaybackMem1WriteOnly::dumpAddress(UtOBStream*, CarbonMemAddrT, CarbonRadix) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mMemNet->getCarbonModel());
}


ShellNetPlayback* ShellNetPlaybackMem1WriteOnly::cloneStorage(ShellNet* primNet)
{
  return new ShellNetPlaybackMem1WriteOnly(primNet, mExternalIndex, mAddrChanges, mMemChanges, mMem);
}

/* ShellNetPlaybackMemA */
ShellNetPlaybackMemA::ShellNetPlaybackMemA(ShellNet* memNet, UInt32 externalIndex, 
                                           ReplayMemChangeMap* addrChanges, 
                                           SInt32HashSet* memChanges, SInt32DynBVMap* memStorage)
  : ShellNetPlaybackMem(memNet, externalIndex, addrChanges, memChanges), mMem(memStorage)
{
  mZeroValue.resize(mBitWidth);
}

ShellNetPlaybackMemA::~ShellNetPlaybackMemA()
{}

DynBitVector* ShellNetPlaybackMemA::getRowOrCreate(CarbonMemAddrT address)
{
  DynBitVector& row = mMem->insertInit(address, mZeroValue);
  return &row;
}

CarbonStatus ShellNetPlaybackMemA::depositMemory(CarbonMemAddrT address, const UInt32* buf)
{
  DynBitVector* row = getRowOrCreate(address);
  UInt32* arr = row->getUIntArray();
  UInt32 numWords = getNumWords();
  CarbonValRW::cpSrcToDest(arr, buf, numWords);
  arr[numWords - 1] &= mTailMask;
  
  notifyWriteAddress(address, arr);
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackMemA::depositMemoryWord(CarbonMemAddrT address, UInt32 buf, int index)
{
  DynBitVector* row = getRowOrCreate(address);
  UInt32* arr = row->getUIntArray();
  arr[index] = buf;
  UInt32 numWords = getNumWords();
  arr[numWords - 1] &= mTailMask;

  notifyWriteAddress(address, arr);
  return eCarbon_OK;
}

CarbonStatus ShellNetPlaybackMemA::depositMemoryRange(CarbonMemAddrT address, const UInt32 *src, int range_msb, int range_lsb)
{
  size_t index;
  size_t length;

  CarbonStatus stat = sCalcIndexLength(getRowMSB(), getRowLSB(), range_msb, range_lsb, &index, &length, mMemNet->getCarbonModel());
  if (((stat == eCarbon_OK) & (src != NULL)))
  {
    DynBitVector* row = getRowOrCreate(address);
    UInt32* arr = row->getUIntArray();
    CarbonValRW::cpSrcToDestRange(arr, src, index, length);
    notifyWriteAddress(address, arr);
  }
  return stat;
}

const DynBitVector* ShellNetPlaybackMemA::getRowValue(CarbonMemAddrT address) const
{
  const DynBitVector* ret = &mZeroValue;
  SInt32DynBVMap::MapEntry* entry = mMem->findEntry(address);
  if (entry)
    ret = &(entry->mKeyVal.second);
  return ret;
}

CarbonStatus ShellNetPlaybackMemA::examineMemory(CarbonMemAddrT address, UInt32* buf) const
{
  const DynBitVector* row = getRowValue(address);
  CarbonValRW::cpSrcToDest(buf, row->getUIntArray(), getNumWords());
  return eCarbon_OK;
}

UInt32 ShellNetPlaybackMemA::examineMemoryWord(CarbonMemAddrT address, int index) const
{
  const DynBitVector* row = getRowValue(address);
  const UInt32* arr = row->getUIntArray();
  return arr[index];
}

CarbonStatus ShellNetPlaybackMemA::examineMemoryRange(CarbonMemAddrT address, UInt32 *dst, 
                                                      int range_msb, int range_lsb) const
{
  size_t index;
  size_t length;
  
  CarbonStatus stat = sCalcIndexLength(getMSB(), getLSB(), range_msb, range_lsb, &index, &length, mMemNet->getCarbonModel());
  
  if (stat == eCarbon_OK)
  {
    const DynBitVector* row = getRowValue(address);
    const UInt32* arr = row->getUIntArray();
    CarbonValRW::cpSrcRangeToDest(dst, arr, index, length);
  }
  return stat;
}

CarbonStatus ShellNetPlaybackMemA::formatMemory(char* valueStr, size_t len, CarbonRadix strFormat, CarbonMemAddrT address) const
{
  const DynBitVector* row = getRowValue(address);
  const UInt32* data = row->getUIntArray();

  CarbonStatus stat = eCarbon_OK;
  if (formatRow(valueStr, len, strFormat, data) == -1)
  {
    ShellGlobal::reportInsufficientBufferLength(len, mMemNet->getCarbonModel());
    stat = eCarbon_ERROR;
  }
  
  return stat;
}

void ShellNetPlaybackMemA::dumpAddress(UtOBStream* out, CarbonMemAddrT address, CarbonRadix strFormat) const
{
  int bufSize = mBitWidth + 2;
  char* valueStr = CARBON_ALLOC_VEC(char, bufSize);

  const DynBitVector* row = getRowValue(address);
  const UInt32* data = row->getUIntArray();
  
  int actualSize = formatRow(valueStr, bufSize, strFormat, data);
  if (actualSize >= 0) {
    valueStr[actualSize] = '\n';
    valueStr[actualSize + 1] = '\0';
    out->writeStr(valueStr, actualSize + 1);
  }
  
  CARBON_FREE_VEC(valueStr, char, bufSize);
}

CarbonStatus ShellNetPlaybackMemA::notifyReadMem(const char* filename, CarbonRadix radix)
{
  CarbonMemAddrT lo;
  CarbonMemAddrT hi;
  ShellMemory::sGetLowHiAddrs(mMemNet->getLeftAddr(), mMemNet->getRightAddr(), &lo, &hi);
  // Readmem without specified start/end addresses doesn't check range of file
  CarbonStatus stat = notifyReadMem(filename, lo, hi, radix, false);
  return stat;
}

CarbonStatus ShellNetPlaybackMemA::notifyReadMem(const char* filename, SInt64 startAddr, SInt64 endAddr, CarbonRadix radix, bool checkRange)
{
  CarbonOpaque id = mMemNet->getCarbonModel()->getHandle ();
  HDLReadMemX memFile(filename, radix == eCarbonHex, mMemNet->getRowBitWidth(),
                      startAddr, endAddr, checkRange, id);
  
  if (! memFile.openFile())
    return eCarbon_ERROR;

  HDLReadMemXResult result;
  SInt64 address;
  UInt32 numWords = getNumWords();
  UInt32* row = CARBON_ALLOC_VEC(UInt32, numWords);
  
  while ((result = memFile.getNextWord(&address, row)) == eRMValidData)
  {
    DynBitVector* memRow = getRowOrCreate(address);
    UInt32* arr = memRow->getUIntArray();
    CarbonValRW::cpSrcToDest(arr, row, numWords);
    notifyWriteAddress(address, row);
  }
  
  CARBON_FREE_VEC(row, UInt32, numWords);  
  memFile.closeFile();
  
  CarbonStatus stat = eCarbon_OK;
  if (result != eRMEnd)
    stat = eCarbon_ERROR;
  
  return stat;
}

CarbonStatus ShellNetPlaybackMemA::readmemh(const char* filename)
{
  return notifyReadMem(filename, eCarbonHex);
}

CarbonStatus ShellNetPlaybackMemA::readmemb(const char* filename)
{
  return notifyReadMem(filename, eCarbonBin);
}

CarbonStatus ShellNetPlaybackMemA::readmemh(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  return notifyReadMem(filename, startAddr, endAddr, eCarbonHex, true);
}

CarbonStatus ShellNetPlaybackMemA::readmemb(const char* filename, SInt64 startAddr, SInt64 endAddr)
{
  // Range checking is done when start/end addresses are specified.
  return notifyReadMem(filename, startAddr, endAddr, eCarbonBin, true);
}


ShellNetPlayback* ShellNetPlaybackMemA::cloneStorage(ShellNet* primNet)
{
  ST_ASSERT(primNet->castMemory() != NULL, primNet->getName());
  ShellNetPlaybackMemA* mem = new ShellNetPlaybackMemA(primNet, mExternalIndex, mAddrChanges, mMemChanges, mMem);
  return mem;
}

void ShellNetPlaybackMemA::backDoorWrite(SInt32 address, const DynBitVector* valBuf)
{
  DynBitVector* row = getRowOrCreate(address);
  CarbonValRW::cpSrcToDest(row->getUIntArray(), valBuf->getUIntArray(), getNumWords());
}


/* ShellNetPlaybackMemAWriteOnly */
ShellNetPlaybackMemAWriteOnly::ShellNetPlaybackMemAWriteOnly(ShellNet* memNet, UInt32 externalIndex, 
                                                             ReplayMemChangeMap* addrChanges, 
                                                             SInt32HashSet* memChanges, SInt32DynBVMap* memStorage)
  : ShellNetPlaybackMemA(memNet, externalIndex, addrChanges, memChanges, memStorage)
{}

ShellNetPlaybackMemAWriteOnly::~ShellNetPlaybackMemAWriteOnly()
{}

CarbonStatus ShellNetPlaybackMemAWriteOnly::examineMemory(CarbonMemAddrT, UInt32*) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mMemNet->getCarbonModel());
  return eCarbon_ERROR;
}

UInt32 ShellNetPlaybackMemAWriteOnly::examineMemoryWord(CarbonMemAddrT, int) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mMemNet->getCarbonModel());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackMemAWriteOnly::examineMemoryRange(CarbonMemAddrT, UInt32*, int, int) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mMemNet->getCarbonModel());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackMemAWriteOnly::formatMemory(char*, size_t, CarbonRadix, CarbonMemAddrT) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mMemNet->getCarbonModel());
  return eCarbon_ERROR;
}

void ShellNetPlaybackMemAWriteOnly::dumpAddress(UtOBStream*, CarbonMemAddrT, CarbonRadix) const
{
  ShellGlobal::reportNotReplayObservable(getNameAsLeaf(), mMemNet->getCarbonModel());
}

ShellNetPlayback* ShellNetPlaybackMemAWriteOnly::cloneStorage(ShellNet* primNet)
{
  return new ShellNetPlaybackMemAWriteOnly(primNet, mExternalIndex, mAddrChanges, mMemChanges, mMem);
}


/* ShellNetPlaybackResponse */

ShellNetPlaybackResponse::ShellNetPlaybackResponse(ShellNet* subNet, const Touched& touched)
  : ShellNetPlayback(subNet, touched), mValBuf(NULL), mDrvBuf(NULL)
{
}

ShellNetPlaybackResponse::~ShellNetPlaybackResponse()
{}

ShellNetPlayback* ShellNetPlaybackResponse::cloneStorage(ShellNet* primNet)
{
  ST_ASSERT(0, primNet->getName());
  return NULL;
}

void ShellNetPlaybackResponse::putModelValueBuffers(UInt32* value, 
                                                    UInt32* drive)
{
  mValBuf = value;
  mDrvBuf = drive;
}

void ShellNetPlaybackResponse::getModelValueBuffers(UInt32** value, 
                                                    UInt32** drive) const
{
  *value = mValBuf;
  *drive = mDrvBuf;
}

void ShellNetPlaybackResponse::getDepositBuffers(UInt32** value, UInt32** drive) const
{
  *value = NULL;
  *drive = NULL;
}

CarbonStatus ShellNetPlaybackResponse::deposit (const UInt32*, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackResponse::depositWord (UInt32, int, UInt32, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

CarbonStatus ShellNetPlaybackResponse::depositRange (const UInt32*, int, int, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
  return eCarbon_ERROR;
}

void ShellNetPlaybackResponse::fastDeposit(const UInt32*, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
}

void ShellNetPlaybackResponse::fastDepositWord (UInt32, int, UInt32, CarbonModel*)
{
  ST_ASSERT(0, getName());
}

void ShellNetPlaybackResponse::fastDepositRange (const UInt32*, int, int, const UInt32*, CarbonModel*)
{
  ST_ASSERT(0, getName());
}
