// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "shell/ShellSymNodeIdent.h"
#include "shell/CarbonDBRead.h"
#include "util/DynBitVector.h"
#include "util/UtOrder.h"       // For carbonPtrCompare()
#include "symtab/STAliasedLeafNode.h"
#include "iodb/IODBTypes.h"
#include "iodb/IODBRuntime.h"

// ShellSymNodeIdent implementation
ShellSymNodeIdent::ShellSymNodeIdent(STAliasedLeafNode* node,
				     UInt32 bitSize,
                                     const DynBitVector* usageMask,
                                     const IODBRuntime* db) :
  SymTabIdent(node, bitSize), mDB(db), mUseMask(*usageMask)
{}

ShellSymNodeIdent::~ShellSymNodeIdent()
{}

ptrdiff_t ShellSymNodeIdent::compare(const CarbonExpr* other) const
{

  const ShellSymNodeIdent* otherIdent = (const ShellSymNodeIdent*)(other);  

  ptrdiff_t ret = HierName::compare(mNode, otherIdent->mNode);
  if (ret == 0)
    ret = mUseMask.compare(otherIdent->mUseMask);
  
  return ret;
}

size_t ShellSymNodeIdent::hash() const
{
  size_t ret = SymTabIdent::hash();
  UInt32 val = mUseMask.to_ulong();
  ret += val;
  ret ^= (size_t) mDB;
  return ret;
}

const STAliasedLeafNode* 
ShellSymNodeIdent::getNode(DynBitVector* usageMask) const
{
  *usageMask = mUseMask;
  return mNode;
}

const char* ShellSymNodeIdent::typeStr() const
{
  return "ShellSymNodeIdent";
}

UInt32 ShellSymNodeIdent::determineBitSize() const
{
  INFO_ASSERT(0, "No sizing information available at runtime.");
  return 0;
}

const ShellSymNodeIdent* ShellSymNodeIdent::castShellSymNodeIdent() const
{
  return this;
}

ShellSymNodeIdentBP::ShellSymNodeIdentBP(STAliasedLeafNode* node,
                                         UInt32 bitSize,
                                         const DynBitVector* usageMask,
                                         const IODBRuntime* db, 
                                         CarbonExpr* backPointer) :
  ShellSymNodeIdent(node, bitSize, usageMask, db), mBackPointer(backPointer)
{}

ShellSymNodeIdentBP::~ShellSymNodeIdentBP()
{}

size_t ShellSymNodeIdentBP::hash() const
{
  size_t ret = ShellSymNodeIdent::hash();
  ret += ((size_t) mBackPointer >> 2);
  return ret;
}
  
ptrdiff_t ShellSymNodeIdentBP::compare(const CarbonExpr* other) const
{
  ptrdiff_t cmp = ShellSymNodeIdent::compare(other);
  if (cmp == 0)
  {
    const ShellSymNodeIdentBP* otherCast = (const ShellSymNodeIdentBP*) other;
    cmp = carbonPtrCompare(mBackPointer, otherCast->mBackPointer);
  }
  return cmp;
}

void ShellSymNodeIdentBP::print(bool recurse, int indent) const
{
  ShellSymNodeIdent::print(recurse, indent);
  
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < indent; i++) {
    screen << " ";
  }
  
  screen << "BACKPOINTER:";
  mBackPointer->print(recurse, indent);
}

void ShellSymNodeIdentBP::composeIdent(ComposeContext* context) const
{
  mBackPointer->composeHelper(context);
}

const char* ShellSymNodeIdentBP::typeStr() const
{
  return "ShellSymNodeIdentBP";
}

const ShellSymNodeIdentBP* ShellSymNodeIdentBP::castShellSymNodeIdentBP() const
{
  return this;
}

// SymNodeDBExprContext Implementation
SymNodeDBExprContext::SymNodeDBExprContext(const IODBRuntime* caller, STSymbolTable* symTab,
                                           DynBitVectorFactory* factory) :
  ExprDBContext(symTab, factory), mDB(caller)
{}

SymNodeDBExprContext::~SymNodeDBExprContext()
{}

CarbonIdent* SymNodeDBExprContext::createIdent(STAliasedLeafNode* node, 
					       UInt32 bitSize,
                                               const DynBitVector* usageMask,
                                               CbuildShellDB::CarbonIdentType type,
                                               const CarbonExprVector* nestedExprs)
{
  CarbonIdent* ret = NULL;
  switch (type)
  {
  case CbuildShellDB::eSymTabIdent:
    ret = new ShellSymNodeIdent(node, bitSize, usageMask, mDB);
    break;
  case CbuildShellDB::eSymTabIdentBP:
    ST_ASSERT(nestedExprs, node);
    ret = new ShellSymNodeIdentBP(node, bitSize, usageMask, mDB, nestedExprs->back());
  }
  return ret;
}

void SymNodeDBExprContext::destroyIdent(CarbonIdent* doomedIdent) 
{
  delete doomedIdent;
}
