// -*-c++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/ShellMsgContext.h"
#include "shell/carbon_interface_xtor.h"

MsgStreamIO* carbonInterfaceXtorAllocMsgStream(FILE* stream, bool includeStatusMsgs)
{
  return new MsgStreamIO(stream, includeStatusMsgs);
}


void carbonInterfaceXtorFreeMsgStream(MsgStreamIO* str)
{
  delete str;
}

MsgContext* carbonInterfaceXtorAllocMsgContext(MsgStreamIO* msg)
{
  MsgContext* msgContext = new MsgContext;
  msgContext->addReportStream(msg);
  return msgContext;
}

void carbonInterfaceXtorFreeMsgContext(MsgContext* msg)
{
  delete msg;
}

void carbonInterfaceXtorPCIEInvLinkWidth(MsgContext* msgContext, const char* name, const UInt32 linkwidth)
{
  msgContext->PCIEInvLinkWidth(name, linkwidth);
}

void carbonInterfaceXtorPCIENotConfiged(MsgContext* msgContext, const char* name)
{
  msgContext->PCIENotConfiged(name);
}

void carbonInterfaceXtorPCIEInvTransaction(MsgContext* msgContext, const char* name)
{
  msgContext->PCIEInvTransaction(name);
}

void carbonInterfaceXtorPCIEInvTransactionInternal(MsgContext* msgContext, const char* name, const char* cmdstr)
{
  msgContext->PCIEInvTransactionInternal(name, cmdstr);
}

void carbonInterfaceXtorPCIEUnknownFlowCtrlType(MsgContext* msgContext, const char* name, const char* transaction)
{
  msgContext->PCIEUnknownFlowCtrlType(name, transaction);
}

void carbonInterfaceXtorPCIESplitFailed(MsgContext* msgContext, const char* name, const char* transaction)
{
  msgContext->PCIESplitFailed(name, transaction);
}

void carbonInterfaceXtorPCIECompletionWithNoMatch(MsgContext* msgContext, const char* name, const UInt32 trans_id)
{
  msgContext->PCIECompletionWithNoMatch(name, trans_id);
}

void carbonInterfaceXtorPCIEInvalidAckSequenceNum(MsgContext* msgContext, const char* name, const UInt32 seq_num, const UInt32 last_seq_num)
{
  msgContext->PCIEInvalidAckSequenceNum(name, seq_num, last_seq_num);
}

void carbonInterfaceXtorPCIEAckWithNoMatch(MsgContext* msgContext, const char* name, const UInt32 seq_num, const UInt32 last_seq_num)
{
  msgContext->PCIEAckWithNoMatch(name, seq_num, last_seq_num);
}

void carbonInterfaceXtorPCIEInvalidNakSequenceNum(MsgContext* msgContext, const char* name, const UInt32 seq_num, const UInt32 last_seq_num)
{
  msgContext->PCIEInvalidNakSequenceNum(name, seq_num, last_seq_num);
}

void carbonInterfaceXtorPCIENakForAckTransaction(MsgContext* msgContext, const char* name, const UInt32 last_seq_num, const UInt32 seq_num)
{
  msgContext->PCIENakForAckTransaction(name, last_seq_num, seq_num);
}

void carbonInterfaceXtorPCIECorruptDLLP(MsgContext* msgContext, const char* name)
{
  msgContext->PCIECorruptDLLP(name);
}

void carbonInterfaceXtorPCIEBadCRCOnTLPDoingRetry(MsgContext* msgContext, const char* name, const UInt32 seq_num)
{
  msgContext->PCIEBadCRCOnTLPDoingRetry(name, seq_num);
}

void carbonInterfaceXtorPCIEGetReturnDataCmdIsNull(MsgContext* msgContext, const char* name)
{
  msgContext->PCIEGetReturnDataCmdIsNull(name);
}

void carbonInterfaceXtorPCIEGetReceivedTransCmdIsNull(MsgContext* msgContext, const char* name)
{
  msgContext->PCIEGetReceivedTransCmdIsNull(name);
}

void carbonInterfaceXtorPCIEInvalidMaxPayloadSize(MsgContext* msgContext, const char* name, const UInt32 payload_size)
{
  msgContext->PCIEInvalidMaxPayloadSize(name, payload_size);
}

void carbonInterfaceXtorPCIEInvalidRCB(MsgContext* msgContext, const char* name, const UInt32 rcb)
{
  msgContext->PCIEInvalidRCB(name, rcb);
}

void carbonInterfaceXtorPCIEUnsupportedSpeed(MsgContext* msgContext, const char* name)
{
  msgContext->PCIEUnsupportedSpeed(name);
}

void carbonInterfaceXtorPCIEUnknownTransactionCategory(MsgContext* msgContext, const char* name, const char* type_string, const UInt32 type)
{
  msgContext->PCIEUnknownTransactionCategory(name, type_string, type);
}

void carbonInterfaceXtorPCIEIllegalInterfaceType(MsgContext* msgContext, const char* name)
{
  msgContext->PCIEIllegalInterfaceType(name);
}

void carbonInterfaceXtorPCIEInvalidLinkIndex(MsgContext* msgContext, const char* name, const UInt32 index, const UInt32 link_width)
{
  msgContext->PCIEInvalidLinkIndex(name, index, link_width);
}

void carbonInterfaceXtorPCIEInvalidClockNet(MsgContext* msgContext, const char* name, const char* clock_net)
{
  msgContext->PCIEInvalidClockNet(name, clock_net);
}

void carbonInterfaceXtorPCIEInvalid10BitSignal(MsgContext* msgContext, const char* name, const char* signal_name)
{
  msgContext->PCIEInvalid10BitSignal(name, signal_name);
}

void carbonInterfaceXtorPCIEInvalid10BitSymbol(MsgContext* msgContext, const char* name, const UInt32 symbol, const UInt32 disparity, const UInt32 lane, const char* time, const UInt32 idle)
{
  msgContext->PCIEInvalid10BitSymbol(name, symbol, disparity, lane, time, idle);
}

void carbonInterfaceXtorPCIEInvalidIndex(MsgContext* msgContext, const char* name, const char* signal, const UInt32 index)
{
  msgContext->PCIEInvalidIndex(name, signal, index);
}

void carbonInterfaceXtorPCIEInvalidPipeSignal(MsgContext* msgContext, const char* name, const char* signal)
{
  msgContext->PCIEInvalidPipeSignal(name, signal);
}

void carbonInterfaceXtorPCIEInvalidSignalLinkIndex(MsgContext* msgContext, const char* name, const char* signal, const UInt32 index, const UInt32 link_width)
{
  msgContext->PCIEInvalidSignalLinkIndex(name, signal, index, link_width);
}

void carbonInterfaceXtorPCIESecondDLLPInCycle(MsgContext* msgContext, const char* name, const char* time)
{
  msgContext->PCIESecondDLLPInCycle(name, time);
}

void carbonInterfaceXtorPCIEInvalidTSNumber(MsgContext* msgContext, const char* name)
{
  msgContext->PCIEInvalidTSNumber(name);
}

void carbonInterfaceXtorPCIEUnableToExpandReceivedPackets(MsgContext* msgContext, const char* name, const UInt32 total_pkts, const UInt32 length, const UInt32 start)
{
  msgContext->PCIEUnableToExpandReceivedPackets(name, total_pkts, length, start);
}

void carbonInterfaceXtorPCIEInvalidTLPType(MsgContext* msgContext, const UInt32 type)
{
  msgContext->PCIEInvalidTLPType(type);
}
