// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/OnDemandMgr.h"
#include "shell/OnDemandDeposits.h"
#include "shell/OnDemandState.h"
#include "shell/OnDemandCallback.h"
#include "shell/OnDemandCachedExamines.h"
#include "shell/OnDemandDebug.h"
#include "shell/ShellNetOnDemand.h"
#include "shell/CarbonNetValueCBData.h"
#include "shell/CarbonScalarBase.h"
#include "util/UtCheckpointStream.h"
#include "util/ShellMsgContext.h"
#include "util/AtomicCache.h"
#include "iodb/IODBRuntime.h"

OnDemandMgr::OnDemandMgr(CarbonModel *obj, CarbonUInt32 max_states, CarbonUInt32 backoff_states)
  : mObj(obj),
    mMaxStates(0),
    mBackoffStates(0),
    mTime(0),
    mMode(eCarbonOnDemandLooking),
    mNumStates(0),
    mStartState(0),
    mLastState(0),
    mCurrState(0),
    mRepeatCount(0),
    mNumStateBytes(0),
    mBackoffTimer(0),
    mInitialized(false),
    mBackoffRestart(false),
    mNonIdleAccess(false),
    mRestored(false),
    mMemCBTriggered(false),
    mInternallyDisabled(false),
    mReEnableAfterSchedule(false),
    mBackoffEntered(false),
    mAnyCModels(false),
    mNextSetIdlePointers(false),
    mStatsEnabled(false),
    mSchedulesRequested(0),
    mSchedulesRun(0),
    mLongestPattern(0),
    mDebug(NULL),
    mAPIDebugInfo(NULL)
{
  mCurrDeposits = mDepositFactory.create();
  mCurrCB = mCallbackFactory.createCollection();
  // We can't create our cmodel collection yet, because the cmodels
  // haven't been registered yet, so we don't know the input/output sizes.
  mCurrCModel = NULL;
  // This is only set on a divergence
  mRestoreCModel = NULL;
  
  // Set the initial parameters.  Query the IODB in case any of these
  // were set via attributes at compile time.
  CarbonOnDemandBackoffStrategy backoffStrategy = eCarbonOnDemandBackoffDecay;
  UInt32 decayPercentage = 10;
  UInt32 maxDecay = 10;
  UtString strategyStr;
  CarbonHookup *hookup = getHookup();
  IODBRuntime *iodb = hookup->getDB();

  iodb->getIntAttribute("OnDemandMaxStates", &max_states);
  iodb->getIntAttribute("OnDemandBackoffCount", &backoff_states);
  iodb->getIntAttribute("OnDemandBackoffDecayPercentage", &decayPercentage);
  iodb->getIntAttribute("OnDemandBackoffMaxDecay", &maxDecay);

  if (iodb->getStringAttribute("OnDemandBackoffStrategy", &strategyStr)) {
    if (strategyStr == "constant") {
      backoffStrategy = eCarbonOnDemandBackoffConstant;
    } else if (strategyStr == "decay") {
      backoffStrategy = eCarbonOnDemandBackoffDecay;
    }
  }

  setMaxStates(max_states);
  setBackoffStrategy(backoffStrategy);
  setBackoffCount(backoff_states);
  setBackoffDecayPercentage(decayPercentage);
  setBackoffMaxDecay(maxDecay);

  // Save the original design schedule pointers, and replace
  // them with our onDemand versions.
  mScheduleFnOrig.init(hookup);
  mScheduleFnEnabled.init(sScheduleEnabled,
                          sClockScheduleEnabled,
                          sDataScheduleEnabled,
                          sAsyncScheduleEnabled);
  mScheduleFnDisabled.init(sScheduleDisabled,
                           sClockScheduleDisabled,
                           sDataScheduleDisabled,
                           sAsyncScheduleDisabled);
  mScheduleFnIdle.init(sScheduleIdle,
                       sClockScheduleIdle,
                       sDataScheduleIdle,
                       sAsyncScheduleIdle);

  // Install the disabled schedule pointers.  Replay needs to think
  // these are the normal mode pointers.  We'll install the enabled
  // pointers after replay is initialized.
  installDisabledSchedulePointers();

  // Lookup the idle net.  We need to access the symtab/hookup
  // directly because we don't want this net wrapped.
  STSymbolTable *symtab = iodb->getDesignSymbolTable();
  STSymbolTable::RootIter iter = symtab->getRootIter();
  STSymbolTableNode *root = *iter;
  // The atomic cache is shared by all model instances, so access
  // needs to be controlled with a mutex.
  ShellGlobal::lockMutex();
  StringAtom *strAtom = symtab->getAtomicCache()->getIntern(sIdleSignalName());
  ShellGlobal::unlockMutex();
  STSymbolTableNode *node = symtab->find(root, strAtom);
  INFO_ASSERT(node, "OnDemand idle signal not found");
  STAliasedLeafNode *leaf = node->castLeaf();
  ShellNet *shellNet = hookup->getCarbonNet(leaf);
  // This should be a scalar
  mIdleNet = shellNet->castScalar();
  INFO_ASSERT(mIdleNet, "OnDemand idle signal is not a scalar");
}

OnDemandMgr::~OnDemandMgr()
{
  if (mStatsEnabled) {
    MsgContext *msg = getModel()->getMsgContext();
    UtString str;
    str << mSchedulesRequested << " carbonSchedule() calls requested.";
    msg->SHLOnDemandMsg(str.c_str());
    str.clear();
    str << mSchedulesRun << " carbonSchedule() calls made.";
    msg->SHLOnDemandMsg(str.c_str());
    str.clear();
    str << "Longest pattern was " << mLongestPattern << " states.";
    msg->SHLOnDemandMsg(str.c_str());
  }
  mStateFactory.clear();
  mDepositFactory.clearAll();
  mCallbackFactory.clearAll();
  mCModelFactory.clearAll();
  for (UInt32 i = 0; i < mMemCBs.size(); ++i) {
    delete static_cast<OnDemandMemWriteCB*>(mMemCBs[i]->mUserData);
    delete mMemCBs[i];
  }
  delete mDebug;
  delete mAPIDebugInfo;
  // The nets we wrapped will automatically be freed upon destruction,
  // but we need to free the wrappers themselves.
  for (WrapNetMap::UnsortedLoop l = mWrappedNets.loopUnsorted(); !l.atEnd(); ++l) {
    ShellNetOnDemand *net = (*l).second;
    delete net;
  }
  // Free all allocated callback objects
  for (UserCBSet::UnsortedLoop l = mUserCBs.loopUnsorted(); !l.atEnd(); ++l) {
    CarbonOnDemandCBData *cb = *l;
    delete cb;
  }
}

void OnDemandMgr::finalizeInit()
{
  // If there were any state points that couldn't be excluded at
  // compile time, exclude them now.  This can't be done as part of
  // the constructor, because the memory write change callbacks are
  // added afterwards.  When debug mode is enabled, all the callbacks
  // need to be in place.
  CarbonHookup *hookup = mObj->getHookup();
  IODBRuntime *iodb = hookup->getDB();

  IODB::NameSetLoop l = iodb->loopOnDemandRuntimeExcluded();
  if (!l.atEnd()) {
    // There's at least one runtime exclusion.  Debug mode must be
    // enabled to do that.
    setDebugLevel(eCarbonOnDemandDebugNone);    
  }
  for (; !l.atEnd(); ++l) {
    STSymbolTableNode *node = *l;
    STAliasedLeafNode *leaf = node->castLeaf();
    INFO_ASSERT(leaf, "Runtime exclusion node is not a leaf");
    mDebug->exclude(leaf);
  }

  mInitialized = true;
}

void OnDemandMgr::setMaxStates(CarbonUInt32 max_states)
{
  mMaxStates = max_states;
  // Allow an extra entry so debug mode can analyze the state/deposits
  // that pushed us over the edge
  mStateArray.resize(mMaxStates + 1);
  mDepositArray.resize(mMaxStates + 1);
  mCBArray.resize(mMaxStates + 1);
  mCModelArray.resize(mMaxStates + 1);
  mExamineArray.resize(mMaxStates + 1);
  mSchedArray.resize(mMaxStates + 1);

  reset(false);
  setMode(eCarbonOnDemandLooking);
}

void OnDemandMgr::setBackoffStrategy(CarbonOnDemandBackoffStrategy strategy)
{
  reset(false);
  setMode(eCarbonOnDemandLooking);

  mBackoffStrategy = static_cast<CarbonOnDemandBackoffStrategy>(strategy & eCarbonOnDemandBackoffModeMask);
  mBackoffRestart = strategy & eCarbonOnDemandBackoffNonIdleRestart;
}

void OnDemandMgr::setBackoffCount(CarbonUInt32 backoff_count)
{
  reset(false);
  setMode(eCarbonOnDemandLooking);

  mBackoffStatesOrig = backoff_count;
  updateBackoffVars();
}

void OnDemandMgr::setBackoffDecayPercentage(CarbonUInt32 decay_percentage)
{
  reset(false);
  setMode(eCarbonOnDemandLooking);

  mBackoffDecayPct = decay_percentage;
  updateBackoffVars();
}

void OnDemandMgr::setBackoffMaxDecay(CarbonUInt32 max_decay)
{
  reset(false);
  setMode(eCarbonOnDemandLooking);

  mBackoffDecayMaxMult = max_decay;
  updateBackoffVars();
}

void OnDemandMgr::updateBackoffVars()
{
  // Starting backoff counter gets the original user value
  mBackoffStates = mBackoffStatesOrig;
  // Next decaying backoff is also the original value, because we've reset
  mBackoffDecayNext = mBackoffStatesOrig;
  // Recalculate maximum decay
  mBackoffDecayMax = mBackoffStatesOrig * mBackoffDecayMaxMult;
}

CarbonOnDemandCBDataID *OnDemandMgr::addModeChangeCB(CarbonOnDemandModeChangeCBFunc fn, void *userData)
{
  OnDemandModeChangeCB *cb = new OnDemandModeChangeCB(fn, userData);
  // Register this so it can be looked up/freed
  registerUserCB(cb);
  mModeChangeCBs.push_back(cb);
  return cb;
}

ShellNetOnDemand *OnDemandMgr::wrapNet(ShellNet *net)
{
  ShellNetOnDemand *wrap_net;
  // See if we've already wrapped this net.
  // If so, return the existing wrapper, but increment
  // its reference count.
  WrapNetMap::iterator iter = mWrappedNets.find(net);
  if (iter != mWrappedNets.end()) {
    wrap_net = iter->second;
    wrap_net->incrCount();
    return wrap_net;
  }

  wrap_net = new ShellNetOnDemand(net, this);
  // We'll take care of deleting these nets, so increment the
  // reference count.
  wrap_net->incrCount();

  // If this is an idle deposit net, save it
  // for fast access later
  if (wrap_net->isIdleDeposit()) {
    OnDemandDeposits::NetMapEntry *master = mCurrDeposits->addNet(wrap_net, NULL);
    // If this is a clock net, we need to add a subordinate net entry
    // to the master one.  It will be used to track fake clock edges
    // caused by multiple deposits with no carbonSchedule() call
    // separating them.
    if (wrap_net->isClock()) {
      mCurrDeposits->addNet(wrap_net, master);
    }
  }

  // Store this in our map
  mWrappedNets[net] = wrap_net;
  return wrap_net;
}

void OnDemandMgr::setNonIdleAccess(OnDemandDebugInfo *debug_info, CarbonOnDemandDebugType type)
{
  // Print debug info, but only on the first instance of a non idle
  // access.  However, if all events are being reported, do all
  // instances.
  if (debugModeEnabled() && 
      (!mNonIdleAccess || (mDebug->reportAllEvents()))) {
    mDebug->report(type, debug_info, 0);
  }
  // Abort, if we were idle
  if (isIdle()) {
    restoreState(type, debug_info);
  }
  enterBackoff();
  mNonIdleAccess = true;
}

void OnDemandMgr::restoreState(CarbonOnDemandDebugType reason, OnDemandDebugInfo *debug_info)
{
  if (isIdle()) {
    if (!mRestored) {
      // Restore the last state
      UtICheckpointStream in(streamReadFunc, mStateArray[mCurrState], 0, 0);
      getHookup()->simRestoreOnDemand(in, getModel ());
      // Reset in case we need to restore this state in the future
      mStateArray[mCurrState]->reset();
    }

    // If in debug mode, print restore reason.
    // Skip not idle nets/changes, since they're printed explicitly
    // in setNonIdleAccess().  Also, only print examines if
    // we actually did a restore.
    if (debugModeEnabled() &&
        (((reason == eCarbonOnDemandDebugRestore) && !mRestored) ||
         (reason == eCarbonOnDemandDebugDiverge))) {
      mDebug->report(reason, debug_info, 0);
    }

    if ((reason == eCarbonOnDemandDebugNonIdleNet) || 
        (reason == eCarbonOnDemandDebugNonIdleChange) || 
        (reason == eCarbonOnDemandDebugDiverge))
      // If we received an unsupported API call, or our saved stimulus
      // diverged from what we expected, we need to actually apply
      // the deposits we saved.
      mCurrDeposits->apply(mObj);

    // We also need to update the shadow values of any
    // signals that have callbacks registered, so they don't
    // incorrectly trigger.
    //
    // There's one situation in which we don't want to update the
    // shadow values.  When a callback is triggered, the net's value
    // is examined, which ends up calling this function.  If we reset
    // the shadow values, any additional callbacks for this schedule
    // call won't be called.  This can only happen on the schedule
    // call in which idle mode is entered, at which point the model is
    // already in a restored state, so just check that flag.
    if (!mRestored) {
      getModel ()->updateNetValueChangeCBShadows(true);
    }

    if (reason == eCarbonOnDemandDebugDiverge) {
      // Run the schedule to make sure all signals are correct.
      ++mSchedulesRun;
      mScheduleFnOrig.callScheduleType(getModel (), mTime, mCurrSched);
    }
  }
  // Prevent restoring again before the next schedule
  mRestored = true;
  // If we needed to restore cmodel outputs, we're done with that now
  mRestoreCModel = false;
}

void OnDemandMgr::addNetValueChangeCB(CarbonNetValueCBData *cb)
{
  OnDemandCallback *od_cb = mCallbackFactory.createCallback(mObj->getObjectID (), cb);
  mCurrCB->add(od_cb);

  // This forces us out of idle mode, since we didn't intercept this
  // callback when tracking the repeating state.  That is, it's a
  // non idle access.  But first, reset the model's shadow values
  // for all callbacks.  We have to force collation, or else the
  // callback we just registered (and any others since the last
  // schedule call) won't be updated.
  getModel ()->updateNetValueChangeCBShadows(true);

  // If in debug mode, record this net's access, otherwise simply
  // set the non idle access flag.
  if (debugModeEnabled()) {
    ShellNet *net = cb->getShellNet();
    UtString hier_name;
    net->getName()->compose(&hier_name);
    OnDemandDebugInfo debug_info(eCarbonOnDemandDebugAPI, hier_name);
    setNonIdleAccess(&debug_info, eCarbonOnDemandDebugNonIdleChange);
  } else {
    setNonIdleAccess(0, eCarbonOnDemandDebugNonIdleChange);
  }
}

void OnDemandMgr::registerCModel(void *cmodel, void *userdata,
                                 CarbonUInt32 context_id, const char *name,
                                 CarbonUInt32 input_words, CarbonUInt32 output_words,
                                 CModelPlaybackFn fn)
{
  mAnyCModels = true;
  mCModelFactory.addContext(cmodel, userdata, context_id, name, input_words, output_words, fn);
}

void OnDemandMgr::preRecordCModel(void *cmodel, CarbonUInt32 context_id, CarbonUInt32 *outputs)
{
  // Only record if we're actually looking for idle state
  if (mMode == eCarbonOnDemandLooking) {
    // If a cmodel is run as part of the initial schedule, we won't
    // have allocated the current cmodel collection yet.
    if (!mCurrCModel) {
      mCurrCModel = mCModelFactory.createCollection();
    }
    mCModelFactory.addCallToCollection(cmodel, context_id, outputs, mCurrCModel);
  }
}

void OnDemandMgr::recordCModel(void *cmodel, CarbonUInt32 context_id, CarbonUInt32 *inputs, CarbonUInt32 *outputs)
{
  // Only record if we're actually looking for idle state
  if (mMode == eCarbonOnDemandLooking)
    mCModelFactory.saveCallResultToCollection(cmodel, context_id, inputs, outputs, mCurrCModel);
}

bool OnDemandMgr::maybeGetCModelCallOutput(CarbonModel::CModelRecoveryStatus *status, void* cmodelData, CarbonUInt32 context_id, CarbonUInt32** outputs)
{
  // If we didn't record an index of a diverging cmodel call collection, we have
  // nothing to do.
  if (!anyCModelDivergence())
    return false;

  // Look up the context
  OnDemandCModelContext *context = mCModelFactory.findContext(cmodelData, context_id);
  // Match this context to a call in the collection, copying the divergent outputs
  *status = mRestoreCModel->getCallStatus(context, outputs);
  return true;
}

static void sSetMemCBTriggered(CarbonMemoryID*, void *userData, CarbonSInt64)
{
  OnDemandMemWriteCB *cb_data = static_cast<OnDemandMemWriteCB*>(userData);
  // If a memory changed during the initial schedule, the callback
  // will be triggered.  However, since OnDemand hasn't started
  // tracking stimulus/state yet, it doesn't matter.  In fact, if we
  // record that the memory has been written, an assertion can occur
  // if debug mode is enabled before the first schedule call, because
  // it expects the debug information for the memory change to be
  // valid.  It won't be valid, though, because debug mode hasn't been
  // enabled yet.
  if (cb_data->isInitialized()) {
    cb_data->setTriggered();
  }
}

void OnDemandMgr::addMemoryCB(ShellMemoryCBManager *cb_mgr, const ShellNet *net)
{
  // Only add a callback if it's needed
  if (cb_mgr->needsOnDemandCB()) {
    ShellGlobal::MemoryCallback *cb = cb_mgr->allocCB();
    // Allocate our user data.
    // The OnDemandDebugInfo object is a placeholder in case
    // debug mode is enabled later.
    OnDemandDebugInfo *debug_info = 0;
    // We don't care about the address/mem - we just
    // care whether it was hit.
    cb->mUserData = new OnDemandMemWriteCB(&mMemCBTriggered, &mInitialized, &mDebug, debug_info, net);
    cb->mMemWriteCB = sSetMemCBTriggered;
    mMemCBs.push_back(cb);
  }
}

void OnDemandMgr::disable(bool internal)
{
  reset(false);
  setMode(eCarbonOnDemandStopped);
  // Use the original design schedule pointers, but only if we're not
  // internally disabled.  The disabler (e.g. Replay) may have
  // installed its own schedule pointers.
  if (!mInternallyDisabled) {
    installDisabledSchedulePointers();
  }
  // Internal disables stick until an internal reenable
  if (internal)
    mInternallyDisabled = true;
}

void OnDemandMgr::reEnable(bool internal)
{
  if (!internal && mInternallyDisabled) {
    // Internal disables can only be undone by an internal reenable
    MsgContext *msg = getModel()->getMsgContext();
    msg->SHLOnDemandDebugStopped();
    return;
  }
  mInternallyDisabled = false;

  // Don't do anything if we're already looking or idle
  if ((mMode == eCarbonOnDemandLooking) || (mMode == eCarbonOnDemandIdle))
    return;

  // We can't immediately re-enable idle searching here, because there
  // may have been some deposits that have already been applied, and
  // we will have missed them.  Instead, just re-enable our schedule
  // functions.  The next time the schedule function is called, we'll
  // detect that we're no longer disabled, and re-enable idle checking.
  installEnabledSchedulePointers();
  mReEnableAfterSchedule = true;
}

bool OnDemandMgr::getCachedExamine(const ShellNetOnDemand *net, CarbonStatus *status, UInt32 *val, UInt32 *drive,
                                   ShellNet::ExamineMode mode, OnDemandDebugInfo *debug_info, OnDemandCachedExamines::Examine **examine_ptr)
{
  // Fail if we're not idle
  if (!isIdle()) {
    return false;
  }

  if (mExamineArray[mCurrState] == NULL) {
    // We don't have any examines cached for this state.  We'll need to add this
    // one, so create an object
    mExamineArray[mCurrState] = new OnDemandCachedExamines;
  }

  if (mExamineArray[mCurrState]->lookup(net, status, val, drive, mode, examine_ptr)) {
    // success!
    return true;
  }

  // We're idle, but the examine wasn't cached, so we have to restore state
  restoreState(eCarbonOnDemandDebugRestore, debug_info);
  return false;
}

void OnDemandMgr::setDebugLevel(CarbonOnDemandDebugLevel level)
{
  // Do we need to create the debug object and instrument?
  if (!debugModeEnabled()) {
    initDebug();
  }

  mDebug->setLevel(level);
}


void OnDemandMgr::initDebug()
{
  // Any nets found before debug was enabled aren't instrumented
  // for debug.
  for (WrapNetMap::iterator iter = mWrappedNets.begin(); iter != mWrappedNets.end(); ++iter) {
    ShellNetOnDemand *net = iter->second;
    net->initDebug();
  }

  // Instrument all the memory write callbacks that were
  // created at model creation, allowing them to trigger
  // debug mode updates
  for (UInt32 i = 0; i < mMemCBs.size(); ++i) {
    ShellGlobal::MemoryCallback *cb = mMemCBs[i];
    OnDemandMemWriteCB *user_data = static_cast<OnDemandMemWriteCB*>(cb->mUserData);
    // Compose the memory's name
    const ShellNet *net = user_data->mNet;
    const STAliasedLeafNode *leaf = net->getNameAsLeaf();
    UtString hier_name;
    OnDemandDebug::makeFriendlyName(leaf, &hier_name);
    // Replace the null debug info with a real one
    user_data->mMyDebugInfo = new OnDemandDebugInfo(eCarbonOnDemandDebugInternal, hier_name);
  }

  // Cmodels need to be instrumented as well
  mCModelFactory.initDebug();

  // Finally, create our debug object.  This also maps the offsets in
  // the state array to net names.
  mDebug = new OnDemandDebug(this);

  // If we know the size of the actual onDemand state, sanity check
  // it now.  Otherwise we'll do this when we learn the actual size.
  if (mNumStateBytes) {
    mDebug->checkSize(mNumStateBytes);
  }

  // Create a debug object to be used for general API calls that don't
  // otherwise have a debug object.
  IODBRuntime *db = getHookup()->getDB();
  mAPIDebugInfo = new OnDemandDebugInfo(eCarbonOnDemandDebugAPI, db->getTopLevelModuleName());
}

CarbonOnDemandCBDataID *OnDemandMgr::addDebugCB(CarbonOnDemandDebugCBFunc fn, void *userData)
{
  // If debug mode has not been configured yet, fail
  if (!debugModeEnabled()) {
    MsgContext *msg = getModel()->getMsgContext();
    msg->SHLOnDemandDebugNotEnabled();
    return NULL;
  }

  CarbonOnDemandCBDataID *cb = mDebug->addCB(fn, userData);
  if (cb != NULL) {
    // Register this so it can be looked up/freed
    registerUserCB(cb);
  }
  return cb;
}

CarbonStatus OnDemandMgr::makeIdleDeposit(CarbonNetID *net)
{
  // This should be castable to a ShellNetOnDemand
  ShellNet *shellNet = net->castShellNet();
  INFO_ASSERT(shellNet, "cast failed");
  ShellNetWrapper *shellNetWrapper = shellNet->castShellNetWrapper();
  INFO_ASSERT(shellNetWrapper, "cast failed");
  ShellNetWrapper1To1 *wrap1to1 = shellNetWrapper->castShellNetWrapper1To1();
  INFO_ASSERT(wrap1to1, "cast failed");
  ShellNetOnDemand *netOnDemand = wrap1to1->castShellNetOnDemand();
  INFO_ASSERT(netOnDemand, "cast failed");

  // One more sanity check - the wrapped net should already be in the map
  ShellNet *wrappedNet = netOnDemand->getNet();
  WrapNetMap::iterator iter = mWrappedNets.find(wrappedNet);
  INFO_ASSERT((iter != mWrappedNets.end()) && (iter->second == netOnDemand), "OnDemand net not found");

  // Only process the net if it's not already an idle deposit
  if (!netOnDemand->isIdleDeposit()) {
    netOnDemand->makeIdleDeposit();
    mCurrDeposits->addNet(netOnDemand, NULL);
  }

  return eCarbon_OK;
}

CarbonStatus OnDemandMgr::exclude(CarbonNetID *net)
{
  // We need debug mode to do this, since the OnDemandDebug object has
  // all the state offset mappings.  Enable it with reporting disabled
  // if necessary.
  if (!debugModeEnabled()) {
    setDebugLevel(eCarbonOnDemandDebugNone);
  }

  CarbonStatus stat = mDebug->exclude(net);

  // If we have any states saved, they're invalid now, since they
  // don't have the just-excluded net masked.  We can't just go back
  // and mask the state in place, because that changes the hash value.
  // If we did that, two identical states would not be seen as
  // identical.
  if (mNumStates != 0) {
    reset(true);
  }

  return stat;
}

void OnDemandMgr::setNetCBEnable(CarbonNetValueCBData *cb, bool enabled)
{
  // The callback factory manages the enables for all the callbacks.
  mCallbackFactory.setEnable(cb, enabled);
}

CarbonStatus OnDemandMgr::setUserCBEnable(CarbonOnDemandCBData* cb, bool enable)
{
  // Make sure the callback data is valid
  if (mUserCBs.count(cb) == 0) {
    // Invalid callback data
    MsgContext *msg = getModel()->getMsgContext();
    msg->SHLOnDemandBadCBData(enable ? "enabled" : "disabled");
    return eCarbon_ERROR;
  }
  if (enable) {
    cb->enable();
  } else {
    cb->disable();
  }
  return eCarbon_OK;
}

CarbonUInt32 OnDemandMgr::streamWriteFunc(void *stream, const void *data, const CarbonUInt32 numBytes)
{
  OnDemandState *state = reinterpret_cast<OnDemandState *>(stream);
  state->add(numBytes, reinterpret_cast<const CarbonUInt8 *>(data));
  return numBytes;
}

CarbonUInt32 OnDemandMgr::streamReadFunc(void *stream, void *data, const CarbonUInt32 numBytes)
{
  OnDemandState *state = reinterpret_cast<OnDemandState *>(stream);
  state->read(numBytes, reinterpret_cast<CarbonUInt8 *>(data));
  return numBytes;
}

void OnDemandMgr::checkpointLooking()
{
  // Looking for a repeating state
  if (!mNonIdleAccess) {
    // Try to match this deposit/state set
    match();
  } else {
    // non idle access - give up
    reset(true);
  }
}

void OnDemandMgr::checkpointIdle()
{
  // Determine the next state in our saved sequence
  CarbonUInt32 next_state;
  if (mCurrState == mLastState) {
    next_state = mStartState;
  } else {
    next_state = mCurrState + 1;
  }

  bool deposits_matched = (*mCurrDeposits == *(mDepositArray[next_state]));
  bool sched_matched = (mCurrSched == mSchedArray[next_state]);
  if (EXPECTED_VALUE((deposits_matched && sched_matched &&
                      (!mAnyCModels || runCModels(next_state))), 1)) {
    // Deposits and cmodels matched - stay in idle mode
    mCurrState = next_state;
    // Run the original callbacks from this state
    OnDemandCallbackCollection *cb = mCBArray[mCurrState];
    if (cb)
      cb->run();
    // Keep track of how many times we've completed this pattern
    if (mCurrState == mStartState) {
      ++mRepeatCount;
      if (mStatsEnabled && (mRepeatCount == 2)) {
        // Only consider patterns that repeated completely when
        // tracking the longest patterns seen.
        CarbonUInt32 pattern_length = mNumStates - mStartState;
        if (pattern_length > mLongestPattern)
          mLongestPattern = pattern_length;
      }
    }
  } else {
    // Deposits didn't match, but that might be OK.

    // If we're in debug mode and the divergence was due to
    // deposits/schedule type, update our info here.  Divergence due to cmodels
    // is handled elsewhere.
    if (debugModeEnabled()) {
      if (!deposits_matched) {
        ShellNetOnDemand *net = mCurrDeposits->getDivergentNet(*(mDepositArray[next_state]));
        mDebug->record(net->getDebugInfo());
      } else if (!sched_matched) {
        mDebug->record(mAPIDebugInfo);
      }
    }

    // See if we can expand our previous repeating state into a larger one
    bool diverged = !extendRepeatingState();
    if (diverged) {
      // Oops, we diverged!
      // Restore the last matching state.
      // This also reapplies the divergent inputs, since the restore will have
      // clobbered them, and runs the VHM with the new inputs
      restoreState(eCarbonOnDemandDebugDiverge, 0);
      // Reset all our state tracking
      reset(true);
    }
  }
}

void OnDemandMgr::match()
{
  // Trying to find a pattern.  See if we've seen both the input
  // pattern and resulting state before.  Both need to match.
  //
  // Note that we could have stored both pieces of data in the same
  // set, but that would have performance problems later on.  When
  // we're trying to find a pattern, we need to compare both the
  // deposited nets and values (relatively small) and the resulting
  // state (relatively large).  However, once we've identified a
  // repeating pattern, we only need to compare the deposits.
  // 
  // This can actually be done in a single set with some additional
  // work, but for now I don't think it's a problem.  The only real
  // drawback is that when we think we've found a repeating pattern
  // (i.e. a deposit set and state that we've seen before), we need
  // to walk our array of saved states to verify that they line up.

  // Create a data structure to hold the state of the design.
  // If this is the first time through, we don't yet know
  // how many bytes are required.
  OnDemandState *statedata;
  if (mNumStateBytes)
    statedata = mStateFactory.create(mNumStateBytes);
  else
    statedata = mStateFactory.create();
    
  // Have the model dump its onDemand state
  UtOCheckpointStream out(streamWriteFunc, statedata, 0, 0);
  getHookup()->simSaveOnDemand(out, getModel ());

  // Now, if we don't already know the data size, we can update it.
  if (mNumStateBytes == 0) {
    // If the state object didn't know how much space it needed
    // upon initialization, it stored the data in a less efficient
    // manner.  We need to tell it to update itself for faster
    // comparisons.
    statedata->close();
    mNumStateBytes = statedata->mSize;
    // If in debug mode, make sure its state size matches
    if (debugModeEnabled()) {
      mDebug->checkSize(mNumStateBytes);
    }
  } else {
    // Flush/reset the state object.
    // This needs to be done to update all the data members
    // that were saved, and to reset the internal pointers
    // to allow a restore.
    statedata->flush();
  }

  // If debug mode is enabled, there may be some state points that
  // need to be masked
  if (debugModeEnabled()) {
    mDebug->maskExcluded(statedata);
  }

  // See if we've seen this set of deposits/state before.
  OnDemandDeposits *depositdata = mDepositFactory.copy(mCurrDeposits);
  OnDemandDepositSet::IterBoolPair dpair = mDepositSet.insert(depositdata);
  OnDemandStateSet::IterBoolPair spair = mStateSet.insert(statedata);

  // If found, point to the data already in the set and free the
  // duplicate copy, so that we only have a single copy of any unique
  // deposit or state.  This allows us just to do pointer comparisons
  // to match an index in our arrays.
  bool found_deposit = !dpair.second;
  bool found_state = !spair.second;

  if (found_deposit) {
    depositdata = *dpair.first;
    mDepositFactory.freeLast();
  }
  if (found_state) {
    statedata = *spair.first;
    mStateFactory.freeLast();
  }

  if (found_deposit && found_state) {
    // Make sure they match each other in our sequence
    for (CarbonUInt32 i = 0; i < mNumStates; ++i)
      if ((mDepositArray[i] == depositdata) &&
          (mStateArray[i] == statedata) &&
          (mSchedArray[i] == mCurrSched)) {
        // Both were found at the same index - success!
        // Save the starting and ending indices, plus other stuff.
        mStartState = i;
        mLastState = mNumStates - 1;
        mCurrState = i;
        mRepeatCount = 1;
        setMode(eCarbonOnDemandIdle);
        break;
      }
  }

  // If the found set entries didn't line up, or we only found one
  // (or none), try to append to the sequence we're tracking
  if (!isIdle()) {
    if (mNumStates == mMaxStates) {
      // We hit the maximum number of states we want to track.
      // Give up and reset everything.
      if (debugModeEnabled()) {
        // Store the last deposit and state in the array.  Otherwise,
        // we won't be able to identify individual signals that would
        // have just repeated.
        mDepositArray[mNumStates] = depositdata;
        mStateArray[mNumStates] = statedata;
        mSchedArray[mNumStates] = mCurrSched;
        mDebug->analyzeFailedPattern(mDepositArray, mStateArray, mSchedArray, mNumStates + 1);
      }
      reset(true);
    } else {
      // Record in our sequence array
      // Callbacks are recorded elsewhere - see processCBs().
      mDepositArray[mNumStates] = depositdata;
      mStateArray[mNumStates] = statedata;
      mSchedArray[mNumStates] = mCurrSched;
      // Only record cmodel calls if any actually occurred.
      if (!mCurrCModel->isEmpty()) {
        mCModelArray[mNumStates] = mCurrCModel;
        mCurrCModel = NULL;
      }
      ++mNumStates;
    }
  }
}

void OnDemandMgr::clear()
{
  // Clear/deallocate all our data structures
  // All the allocated objects are freed by the appropriate
  // factory class.
  for (CarbonUInt32 i = 0; i <= mMaxStates; ++i) {
    mStateArray[i] = NULL;
    mDepositArray[i] = NULL;
    mCBArray[i] = NULL;
    mCModelArray[i] = NULL;
    delete mExamineArray[i];
    mExamineArray[i] = NULL;
  }
  mStateSet.clear();
  mDepositSet.clear();

  mStateFactory.clear();
  mDepositFactory.clear();
  mCallbackFactory.clear();
  mCModelFactory.clear();
  mCurrCModel = NULL;
}

void OnDemandMgr::reset(bool enter_backoff)
{
  // Give up on any idle tracking, and possibly backoff mode.
  clear();
  mNumStates = 0;
  if (enter_backoff) {
    enterBackoff();
  } else {
    mBackoffTimer = 0;
  }
}

bool OnDemandMgr::extendRepeatingState()
{
  // Sometimes the shortest (i.e. first found) repeating state isn't the
  // best one.  If we're in a repeating state and see a divergence, try to
  // make a new repeating state from the smaller one plus the new stimulus.

  // First, find out how many states we would need.
  // We need to consider all the states we've saved, even those that came
  // before the start of the current repeating pattern.  They might be part
  // of the larger pattern.  We start with this count.
  CarbonUInt64 new_pattern_length = mStartState;
  // To this, we need to add the length of the current pattern, times the
  // number of times we've seen it.
  new_pattern_length += ((mNumStates - mStartState) * mRepeatCount);
  // Finally, add the number of states we've seen in this partial pass,
  // plus the new deposit/state
  new_pattern_length += (mCurrState - mStartState) + 1;

  // Abort if this new pattern would be too long
  if (new_pattern_length > static_cast<CarbonUInt64>(mMaxStates))
    return false;

  // Rebuild the state/deposit arrays as if we had been tracking this
  // larger pattern all along, by unrolling the smaller pattern.
  // First, the complete passes
  UInt32 length = mLastState - mStartState + 1;
  for (CarbonUInt32 i = 1; i < mRepeatCount; ++i) {
    duplicateState(length);
  }
  // Then, the current partial pass
  length = mCurrState - mStartState + 1;
  duplicateState(length);

  // Restore the VHM's state as if we diverged, and resume looking
  restoreState(eCarbonOnDemandDebugDiverge, 0);
  setMode(eCarbonOnDemandLooking);
  // Attempt to match this larger sequence
  match();

  // Regardless of whether we matched or not, consider this a success.
  // We only want this function to report failure if the larger pattern
  // would have been too large to track.
  return true;
}

void OnDemandMgr::duplicateState(UInt32 length)
{
  // Duplicate the requested number of saved state points from the
  // beginning of the repeating pattern, adding to the end.
  memcpy(&mDepositArray[mNumStates], &mDepositArray[mStartState], length * sizeof(OnDemandDeposits*));
  memcpy(&mStateArray[mNumStates], &mStateArray[mStartState], length * sizeof(OnDemandState*));
  memcpy(&mCBArray[mNumStates], &mCBArray[mStartState], length * sizeof(OnDemandCallbackCollection*));
  memcpy(&mCModelArray[mNumStates], &mCModelArray[mStartState], length * sizeof(OnDemandCModelCallCollection*));
  memcpy(&mSchedArray[mNumStates], &mSchedArray[mStartState], length * sizeof(ScheduleFnContainer::ScheduleType));
  mNumStates += length;
}

void OnDemandMgr::setMode(CarbonOnDemandMode newmode)
{
  if (mMode != newmode) {
    // Run all mode change callbacks
    UInt32 numCBs = mModeChangeCBs.size();
    if (numCBs) {
      for (UInt32 i = 0; i < numCBs; ++i) {
        OnDemandModeChangeCB *cb = mModeChangeCBs[i];
        cb->run(mObj->getObjectID (), mMode, newmode, mTime);
      }
    }

    // Install the correct schedule pointer, if necessary.  Pointer
    // manipulation for entering/leaving stopped mode are handled
    // elsewhere.
    if ((mMode != eCarbonOnDemandStopped) && (newmode != eCarbonOnDemandStopped)) {
      if (newmode == eCarbonOnDemandIdle) {
        // We actually have to wait until the next schedule call to
        // install the idle pointers, since callbacks are run in the
        // postSchedule() function.  See processCBs() for more
        // details.
        mNextSetIdlePointers = true;
      } else if (mMode == eCarbonOnDemandIdle) {
        // transition to looking/backoff
        installEnabledSchedulePointers();
      }
    }

    mMode = newmode;
    // Update idle waveform signal
    UInt32 idle = isIdle();
    // We can't do a true deposit, because that will set the changed
    // flag on the model, etc.  Instead, we use a bypass function that
    // just updates the storage.
    mIdleNet->bypassDeposit(&idle, NULL);
    // Update waveform if we're idle, since we won't actually run the
    // low-level schedule function again until we leave the idle
    // state!
    if (idle) {
      getHookup()->updateWaveform();
    }
  }
}

void OnDemandMgr::processCBs()
{
  // Callbacks are run in the post-schedule function, so we can't
  // trap them in our schedule wrapper function.  Instead, we process
  // any callbacks that fired on the previous schedule call before we
  // start a new one.
  bool save = false;
  UInt32 index = 0;
  if ((mMode == eCarbonOnDemandLooking) && (mNumStates != 0)) {
    // Looking, so save at the end of the current sequence.
    save = true;
    index = mNumStates - 1;
  } else if (mMode == eCarbonOnDemandIdle) {
    // There are situations in which callbacks may be different after
    // schedule call N (and 2N, 3N, ...) of a pattern of length N than
    // they were on schedule call 0 of the sequence.  For example, a
    // register may load its input value on schedule 0, causing a
    // change.  However, if the input value doesn't change, although
    // the register loads the input on schedules N, 2N, 3N, ..., the
    // output value won't actually change.
    // 
    // To fix this, processCBs() is called on the first schedule after
    // idle mode is entered, in order to handle the callbacks that ran
    // during the post-schedule of the entrance to idle.  In this
    // situation, we overwrite the callbacks for the first entry in
    // the sequence with whatever happened on the last schedule call.
    //
    // The factory class handles memory deallocation, so we don't have
    // to worry about clobbering the old pointer.
    save = true;
    index = mStartState;
  }
  if (save) {
    OnDemandCallbackCollection *coll = NULL;
    // Only make a copy if at least one of the callbacks was called
    if (mCurrCB->anyCalled())
      coll = mCallbackFactory.copyCollection(mCurrCB);
    mCBArray[index] = coll;
  }
  mCurrCB->reset();
}

bool OnDemandMgr::runCModels(CarbonUInt32 state)
{
  if (!mCModelArray[state] || mCModelArray[state]->run())
    // No cmodels need to be called, or they all matched
    return true;

  // Divergence on a cmodel output.  Save the collection
  // pointer so we can copy the new outputs into the model
  mRestoreCModel = mCModelArray[state];

  // Save info about the divergent model if debug
  // mode is enabled
  if (debugModeEnabled()) {
    mDebug->record(mRestoreCModel->getLastRunContext()->getDebugInfo());
  }

  return false;
}

void OnDemandMgr::checkMemCBs()
{
  // If any memories changed during the previous schedule
  // call (other than those that we're saving as part of
  // the state, which don't have callbacks attached), we
  // need to abort idle searching.
  if (mMemCBTriggered)
    setNonIdleAccess(0, eCarbonOnDemandDebugNonIdleChange);

  // Reset for next pass
  mMemCBTriggered = false;
}

void OnDemandMgr::enterBackoff()
{
  // If we're stopped, don't do anything
  if (mMode == eCarbonOnDemandStopped) {
    return;
  }

  // If we're already in backoff, restart the timer (if requested) and
  // then return
  if (mMode == eCarbonOnDemandBackoff) {
    if (mBackoffRestart) {
      mBackoffTimer = mBackoffStates;
    }
    return;
  }

  // Recalculate backoff timer when in decaying mode
  if (mBackoffStrategy == eCarbonOnDemandBackoffDecay) {
    if (mMode == eCarbonOnDemandIdle) {
      // If leaving idle state, reset timer to original value
      mBackoffStates = mBackoffStatesOrig;
    } else {
      // Move to the previously-calculated value
      mBackoffStates = mBackoffDecayNext;
    }
    // Calculate next value, rounding down to nearest integer
    mBackoffDecayNext = (mBackoffStates * (100 + mBackoffDecayPct)) / 100;
    if (mBackoffDecayNext > mBackoffDecayMax) {
      mBackoffDecayNext = mBackoffDecayMax;
    }
  }

  // Only enter backoff if the number of states is non-zero.
  // Otherwise go to looking.
  if (mBackoffStates == 0) {
    setMode(eCarbonOnDemandLooking);
  } else {
    setMode(eCarbonOnDemandBackoff);
  }
  mBackoffTimer = mBackoffStates;

  // Set a flag to reset everything on the next schedule call.
  // We want to reuse some code there for restoring state, etc.,
  // so we don't just reset it here.
  mBackoffEntered = true;
}

void OnDemandMgr::registerUserCB(CarbonOnDemandCBDataID *cb)
{
  // Add this callback to our saved set, so it can be looked up and
  // freed on destruction.
  mUserCBs.insert(cb);
}

CarbonStatus OnDemandMgr::sScheduleEnabled(CarbonObjectID *descr, CarbonTime t)
{
  CarbonModel *model = reinterpret_cast <CarbonModel *> (descr->mModel);
  OnDemandMgr *ondemand = model->getOnDemandMgr();
  return ondemand->runScheduleAndCheckpoint(ScheduleFnContainer::eDesign, model, t);
}

CarbonStatus OnDemandMgr::sClockScheduleEnabled(CarbonObjectID *descr, CarbonTime t)
{
  CarbonModel *model = reinterpret_cast <CarbonModel *> (descr->mModel);
  OnDemandMgr *ondemand = model->getOnDemandMgr();
  return ondemand->runScheduleAndCheckpoint(ScheduleFnContainer::eClk, model, t);
}

CarbonStatus OnDemandMgr::sDataScheduleEnabled(CarbonObjectID *descr, CarbonTime t)
{
  CarbonModel *model = reinterpret_cast <CarbonModel *> (descr->mModel);
  OnDemandMgr *ondemand = model->getOnDemandMgr();
  return ondemand->runScheduleAndCheckpoint(ScheduleFnContainer::eData, model, t);
}

CarbonStatus OnDemandMgr::sAsyncScheduleEnabled(CarbonObjectID *descr, CarbonTime t)
{
  CarbonModel *model = reinterpret_cast <CarbonModel *> (descr->mModel);
  OnDemandMgr *ondemand = model->getOnDemandMgr();
  return ondemand->runScheduleAndCheckpoint(ScheduleFnContainer::eAsync, model, t);
}

CarbonStatus OnDemandMgr::sScheduleDisabled(CarbonObjectID *descr, CarbonTime t)
{
  CarbonModel *model = reinterpret_cast <CarbonModel *> (descr->mModel);
  OnDemandMgr *ondemand = model->getOnDemandMgr();
  return ondemand->runOriginalVHMSchedule(ScheduleFnContainer::eDesign, model, t);
}

CarbonStatus OnDemandMgr::sClockScheduleDisabled(CarbonObjectID *descr, CarbonTime t)
{
  CarbonModel *model = reinterpret_cast <CarbonModel *> (descr->mModel);
  OnDemandMgr *ondemand = model->getOnDemandMgr();
  return ondemand->runOriginalVHMSchedule(ScheduleFnContainer::eClk, model, t);
}

CarbonStatus OnDemandMgr::sDataScheduleDisabled(CarbonObjectID *descr, CarbonTime t)
{
  CarbonModel *model = reinterpret_cast <CarbonModel *> (descr->mModel);
  OnDemandMgr *ondemand = model->getOnDemandMgr();
  return ondemand->runOriginalVHMSchedule(ScheduleFnContainer::eData, model, t);
}

CarbonStatus OnDemandMgr::sAsyncScheduleDisabled(CarbonObjectID *descr, CarbonTime t)
{
  CarbonModel *model = reinterpret_cast <CarbonModel *> (descr->mModel);
  OnDemandMgr *ondemand = model->getOnDemandMgr();
  return ondemand->runOriginalVHMSchedule(ScheduleFnContainer::eAsync, model, t);
}

CarbonStatus OnDemandMgr::sScheduleIdle(CarbonObjectID *descr, CarbonTime t)
{
  CarbonModel *model = reinterpret_cast <CarbonModel *> (descr->mModel);
  OnDemandMgr *ondemand = model->getOnDemandMgr();
  return ondemand->runIdleCheckpoint(ScheduleFnContainer::eDesign, t);
}

CarbonStatus OnDemandMgr::sClockScheduleIdle(CarbonObjectID *descr, CarbonTime t)
{
  CarbonModel *model = reinterpret_cast <CarbonModel *> (descr->mModel);
  OnDemandMgr *ondemand = model->getOnDemandMgr();
  return ondemand->runIdleCheckpoint(ScheduleFnContainer::eClk, t);
}

CarbonStatus OnDemandMgr::sDataScheduleIdle(CarbonObjectID *descr, CarbonTime t)
{
  CarbonModel *model = reinterpret_cast <CarbonModel *> (descr->mModel);
  OnDemandMgr *ondemand = model->getOnDemandMgr();
  return ondemand->runIdleCheckpoint(ScheduleFnContainer::eData, t);
}

CarbonStatus OnDemandMgr::sAsyncScheduleIdle(CarbonObjectID *descr, CarbonTime t)
{
  CarbonModel *model = reinterpret_cast <CarbonModel *> (descr->mModel);
  OnDemandMgr *ondemand = model->getOnDemandMgr();
  return ondemand->runIdleCheckpoint(ScheduleFnContainer::eAsync, t);
}

CarbonStatus OnDemandMgr::runScheduleAndCheckpoint(ScheduleFnContainer::ScheduleType type, CarbonModel *model, CarbonTime t)
{
  // Save the current time and schedule type
  mTime = t;
  mCurrSched = type;

  // Process any callbacks received after the last call
  processCBs();

  // Create an object to track cmodel calls, if we don't already have one
  if (mCurrCModel == NULL)
    mCurrCModel = mCModelFactory.createCollection();

  CarbonStatus status = eCarbon_OK;
  ++mSchedulesRequested;
  if (mMode != eCarbonOnDemandIdle) {
    ++mSchedulesRun;
    status = mScheduleFnOrig.callScheduleType(model, t, type);
    // See if any non-state-dumped memories changed as
    // a result of the schedule call.
    checkMemCBs();
  }

  // Should we do a checkpoint?
  bool do_checkpoint = true;

  // If we're in a backoff state, update the
  // counter but don't do a checkpoint.
  // Exception - if we've just entered a backoff
  // state due to a non idle access.
  if ((mMode == eCarbonOnDemandBackoff) && !mBackoffEntered) {
    --mBackoffTimer;
    do_checkpoint = false;
    // Go back to looking when the timer expires
    if (mBackoffTimer == 0)
      setMode(eCarbonOnDemandLooking);
  }

  // When onDemand is temporarily disabled, the original design
  // schedules are called.  If we're here and the flag is set, it
  // means this is the first schedule call after being reenabled.
  // It's now safe to go back into looking mode.  Don't actually do
  // anything on this call, though, because we might not have all the
  // data we need.
  if (mReEnableAfterSchedule) {
    mBackoffTimer = 0;  // clear backoff if set
    setMode(eCarbonOnDemandLooking);
    do_checkpoint = false;
    mReEnableAfterSchedule = false;
  }

  // If we should install the idle schedule pointers, do so.  Also,
  // run the idle mode version of the checkpoint function.  Only do
  // this if we haven't already left the idle state.
  bool do_idle = mNextSetIdlePointers && (mMode == eCarbonOnDemandIdle);
  // Clear the flag before we do a checkpoint.  If we extend the
  // repeating state as part of the checkpoint, we may temporarily
  // enter the looking state and return to idle, in which case the
  // flag will be set again for the next pass.
  mNextSetIdlePointers = false;
  if (do_idle) {
    installIdleSchedulePointers();
    do_checkpoint = false;
    checkpointIdle();
  }

  CarbonOnDemandMode prev_mode = mMode;
  if (do_checkpoint)
    checkpointLooking();

  // clear for next pass
  mCurrDeposits->clear();
  mNonIdleAccess = false;
  mBackoffEntered = false;
  if (mDebug) {
    mDebug->clear();
  }

  // If we just entered idle mode, the model state is consistent.
  mRestored = ((prev_mode != eCarbonOnDemandIdle) && (mMode == eCarbonOnDemandIdle));

  return status;
}

CarbonStatus OnDemandMgr::runIdleCheckpoint(ScheduleFnContainer::ScheduleType type, CarbonTime t)
{
  // Save the current time
  mTime = t;
  mCurrSched = type;
  ++mSchedulesRequested;
  checkpointIdle();

  // clear for next pass
  mCurrDeposits->clear();
  mBackoffEntered = false;
  if (mDebug) {
    mDebug->clear();
  }
  mRestored = false;

  return eCarbon_OK;
}

CarbonStatus OnDemandMgr::runOriginalVHMSchedule(ScheduleFnContainer::ScheduleType type, CarbonModel *model, CarbonTime t)
{
  mTime = t;
  mCurrSched = type;
  ++mSchedulesRequested;
  ++mSchedulesRun;
  return mScheduleFnOrig.callScheduleType(model, t, type);
}

OnDemandMemWriteCB::~OnDemandMemWriteCB()
{
  delete mMyDebugInfo;
}

void OnDemandMemWriteCB::setTriggered()
{
  // Set the triggered flag
  *mTriggerPtr = true;
  // Update the debug info with this memory
  OnDemandDebug *dbg = *mDebugPtr;
  if (dbg)
    dbg->record(mMyDebugInfo);
}

OnDemandModeChangeCB::OnDemandModeChangeCB(CarbonOnDemandModeChangeCBFunc fn, void *userData)
  : mCBFunc(fn),
    mCBData(userData)
{
}

OnDemandModeChangeCB::~OnDemandModeChangeCB()
{
}

void OnDemandModeChangeCB::run(CarbonObjectID* context,
                               CarbonOnDemandMode from,
                               CarbonOnDemandMode to,
                               CarbonTime simTime)
{
  // If the callback is enabled and has a valid function pointer, call
  // the function with the saved user data.
  if (isEnabled() && mCBFunc) {
    (*mCBFunc)(context, mCBData, from, to, simTime);
  }
}

