/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/MutexWrapper.h"
#include "shell/ReplaySystemSim.h"
#include "shell/carbon_capi.h"
#include "shell/ShellGlobal.h"
#include "shell/CarbonModel.h"
#include "util/subprocesses.h"
#include "util/OSWrapper.h"
#include "util/ShellMsgContext.h"
#include "iodb/IODBRuntime.h"

//! Mutex for allocation of single system instance
MUTEX_WRAPPER_DECLARE(sSystemMutex);

CarbonSystemSim::CarbonSystemSim() :
  mStats(),
  mNumReplayableComponents(0),
  mCycles(0),
  mSimTime(0),
  mCycleCountFn(0),
  mCycleCountClientData(NULL),
  mSimTimeFn(0),
  mSimTimeClientData(0)
{
  // Suppress warnings about the Carbon Model not supporting replay and onDemand
  carbonChangeMsgSeverity(NULL, 5151, eCarbonMsgSuppress);
  carbonChangeMsgSeverity(NULL, 5181, eCarbonMsgSuppress);
  mSimPID = OSGetPid();
  OSGetHostname(&mSimHost);
}

CarbonSystemSim::~CarbonSystemSim() {
  updateSystem(eReplayEventExit);
}

//! add new component
/*!
 *! When called from inside the simulation process, the model should
 *! be non-null.  When called inside the GUI, the model can be NULL.
 */
CarbonSystemComponent* CarbonSystemSim::addComponent(const char* name,
                                                     CarbonObjectID** model,
                                                     void* userData)
{
  CarbonReplaySystem::addComponent(name);
  CarbonSystemComponent* component = mNameComponentMap[name];
  mModelComponentMap[*model] = component;
  component->putModel(model);
  // Get the IODB's ID string and save it with the component
  CarbonHookup *hookup = (*model)->getModel ()->getHookup ();
  INFO_ASSERT(hookup, "Could not find hookup function for model");
  IODBRuntime *db = hookup->getDB();
  component->putID(db->getDesignId());
  component->putUserData(userData);
  CarbonReplayInfoID* replayInfo = carbonGetReplayInfo(*model);

  // Check if this is a new replayable component so we can increment
  // the count
  if ((component->getReplayInfo() == NULL) && (replayInfo != NULL)) {
    ++mNumReplayableComponents;
  }
  component->putReplayInfo(replayInfo);

  if (replayInfo != NULL) {
    bool isGood = true;
    if (carbonReplayInfoAddModeChangeCB(replayInfo, component_changed, this) == NULL)
      isGood = false;
    if (carbonReplayInfoAddCheckpointCB(replayInfo, checkpoint_cb, this) == NULL)
      isGood = false;

    if (! isGood)
    {
      ShellGlobal::getProgErrMsgr()->SHLCompReplayUnableToAddComponent(name);
      return NULL;
    }

    carbonReplayInfoPutDB(replayInfo, mDatabaseDirectory.c_str(), name);
    carbonReplayInfoPutDirAction(replayInfo, eCarbonDirOverwrite);

    if (! mDatabaseDirectory.empty()) {
      carbonReplayInfoPutDB(replayInfo, mDatabaseDirectory.c_str(),
                            component->getName());
    }
    if ((mMinInterval != 0) || (mRecoverPercentage != 0)) {
      carbonReplayInfoPutSaveFrequency(replayInfo, mRecoverPercentage, mMinInterval);
    }
    if (isRecordRequested(name)) {
      carbonReplayRecordStart(*model);
    }      
    else if (isPlaybackRequested(name)) {
      carbonReplayPlaybackStart(*model);
    }      
  }
  else if (isRecordRequested(name)) {
    ShellGlobal::getProgErrMsgr()->SHLCompReplayDisabled(name, "record");
  }
  else if (isPlaybackRequested(name)) {
    ShellGlobal::getProgErrMsgr()->SHLCompReplayDisabled(name, "playback");
  }

  // Setup onDemand, if enabled
  if (carbonOnDemandIsEnabled(*model)) {
    if (carbonOnDemandAddModeChangeCB(*model, ondemand_mode_changed, this) == NULL) {
      // TODO - error message
      return NULL;
    }
  }

  // Return a pointer to the component to get information
  return component;
} // CarbonSystemComponent* CarbonSystemSim::addComponent

void CarbonSystemSim::composeCycle(UInt64 compScheduleCalls, UtString* str)
{
  // Use a string stream so that we print doubles the same way as if
  // we sent it to a stream.
  UtOStringStream ss(str);

  if (mCycleCountFn != 0) {
    ss << "cycle #" << getCycles();
  }
  else if (mSimTimeFn != 0) {
    ss << "simtime " << getSimTime() << mSimTimeUnits;
  }
  else {
    ss << "schedule call #" << compScheduleCalls;
  }
}

void CarbonSystemSim::printCycle(UInt64 compScheduleCalls)
{
  UtString cycleMsg;
  composeCycle(compScheduleCalls, &cycleMsg);
  UtIO::cout() << cycleMsg << UtIO::endl;
}

void CarbonSystemSim::updateStats(CarbonSystemComponent *comp, CarbonReplayEventType type, bool calc_schedule_calls)
{
  detectSimRestart();

  // Collect user+sys time statistics.  Using peek* rather than get*
  // means that we will be recording the total elapsed times in all
  // categories.  CPS data can be calculated by taking the delta
  // between events.
  Stats::StatsData stats;
  mStats.peekIntervalStatistics(&stats);

  if (calc_schedule_calls) {
    determineCompScheduleCalls(comp);
  }
  updateTotalSchedCalls();
  addModeChange(comp, type, getCycles(),
                stats.getRealTime(),
                stats.getUserTime(), stats.getSysTime(),
                getSimTime());

  // let the GUI know
  (void) writeSystem();
}

void CarbonSystemSim::component_changed(CarbonObjectID *descr,
                                        void* userContext,
                                        CarbonVHMMode changingFrom,
                                        CarbonVHMMode changingTo)
{
  CarbonModel *model = descr->getModel ();
  CarbonSystemSim* replaySystem = (CarbonSystemSim*) userContext;
  CarbonSystemComponent* comp = replaySystem->mModelComponentMap[descr];
  INFO_ASSERT(comp, "Could not find model");
  CarbonReplayEventType type = eReplayEventNormal;
  switch (changingTo) {
  case eCarbonRunNormal:
    comp->putReplayState(eCarbonRunNormal);
    break;
  case eCarbonRunRecord:
    comp->putReplayState(eCarbonRunRecord);
    type = eReplayEventRecord;
    break;
  case eCarbonRunPlayback:
    comp->putReplayState(eCarbonRunPlayback);
    type = eReplayEventPlayback;
    break;
  case eCarbonRunRecover:
    comp->putReplayState(eCarbonRunRecover);
    type = eReplayEventRecover;
    break;
  }

  replaySystem->updateStats(comp, type, false);

  // If verbose print transition. This has to run after
  // updateStats.
  if (replaySystem->mVerbose) {
    UtString msg;
    msg << "Replay: component " << comp->getName()
        << " changed status from "
        << carbonGetVHMModeString(changingFrom) << " to "
        << carbonGetVHMModeString(changingTo) << " at ";
    replaySystem->composeCycle(replaySystem->mTotalScheduleCalls, &msg);
    model->getMsgContext()->SHLReplayMsg(msg.c_str());
  }

#if 0
  // If there is a GUI active, then notify it of the change, so that
  // it can change the status screen
  if (replaySystem->mGUISubprocess != NULL) {
    UtString buf;
    buf << comp->mState.getString() << " " << comp->mName << "\n";
    replaySystem->mGUISubprocess->write_stdin(buf);
  }
#endif
}

void CarbonSystemSim::checkpoint_cb(CarbonObjectID* descr,
                                    void* userContext,
                                    CarbonUInt64 compScheduleCalls,
                                    CarbonUInt32 checkpointNumber)
{
  CarbonSystemSim* replaySystem = (CarbonSystemSim*) userContext;
  CarbonSystemComponent* comp = replaySystem->mModelComponentMap[descr];
  INFO_ASSERT(comp, "Could not find model");
  
  if (replaySystem->mVerbose) {
    UtString msg;
    msg << "Replay: component " << comp->getName()
        << " checkpoint #" << checkpointNumber << " at ";
    replaySystem->composeCycle(compScheduleCalls, &msg);
    CarbonModel *model = descr->getModel ();
    model->getMsgContext()->SHLReplayMsg(msg.c_str());
  }

  replaySystem->updateStats(comp, eReplayEventCheckpoint, true);
} // void CarbonSystemSim::checkpoint_cb

void CarbonSystemSim::resetOnDemandFirstEventFlags() {
  // Depending on the order of construction and establishment of
  // callbacks, we may not properly record the initial transition
  // into "looking" or "backoff" mode, resulting in the perf-plot
  // not being drawn in cyan until the first real change to idle and
  // back.
  for (UInt32 i = 0; i < numComponents(); ++i) {
    CarbonSystemComponent* comp = getComponent(i);
    if (isOnDemandStartRequested(comp->getName())) {
      comp->putIsFirstEvent(true);
    }
  }
}

void CarbonSystemSim::ondemand_mode_changed(CarbonObjectID* context,
                                            void* userContext,
                                            CarbonOnDemandMode from,
                                            CarbonOnDemandMode to,
                                            CarbonTime /* unused */)
{
  CarbonSystemSim* replaySystem = (CarbonSystemSim*) userContext;
  CarbonSystemComponent* comp = replaySystem->mModelComponentMap[context];
  INFO_ASSERT(comp, "Could not find model");
  // If there are a lot of search failures, there will be a lot
  // of looking<->backoff transitions, which will swamp the event file.
  // We'll just lump those two together into "not idle".
  CarbonReplayEventType type = eOnDemandEventNotIdle;
  // We also only want to update the system if the onDemand state actually
  // changed
  bool changed = true;
  switch (to) {
  case eCarbonOnDemandLooking:
    comp->putOnDemandState(eCarbonOnDemandLooking);
    // Don't record backoff->looking
    if (from == eCarbonOnDemandBackoff) {
      changed = false;
    }
    break;
  case eCarbonOnDemandIdle:
    comp->putOnDemandState(eCarbonOnDemandIdle);
    type = eOnDemandEventIdle;
    break;
  case eCarbonOnDemandBackoff:
    comp->putOnDemandState(eCarbonOnDemandBackoff);
    // Don't record looking->backoff
    if (from == eCarbonOnDemandLooking) {
      changed = false;
    }
    break;
  case eCarbonOnDemandStopped:
    comp->putOnDemandState(eCarbonOnDemandStopped);
    type = eOnDemandEventDisabled;
    break;
  }

  if (changed || comp->isFirstEvent()) {
    comp->putIsFirstEvent(false);
    replaySystem->updateStats(comp, type, false);
  }
}

#if REMOVE_COMPONENT_SUPPORT
//! remove a ReplayInfo* from the existing system
void CarbonSystemSim::removeComponent(const char* name) {
  NameComponentMap::iterator p = mNameComponentMap.find(name);
  INFO_ASSERT(p != mNameComponentMap.end(), "component not found");
  CarbonSystemComponent* comp = p->second;
  if (comp->getModel() != NULL) {
    UInt32 erased = mModelComponentMap.erase(comp->getModel());
    INFO_ASSERT(erased == 1, "model not found");
  }

  // remove it from the base class
  CarbonReplaySystem::removeComponent(name);
}
#endif

//! Look up component name from object, return NULL if none found
const char* CarbonSystemSim::findComponentName(CarbonObjectID* model) {
  ModelComponentMap::iterator p = mModelComponentMap.find(model);
  if (p == mModelComponentMap.end()) {
    return NULL;
  }
  CarbonSystemComponent* comp = p->second;
  return comp->getName();
}

//! Look up Carbon Model from component name, return NULL if none found
CarbonObjectID* CarbonSystemSim::findModel(const char* name) {
  NameComponentMap::iterator p = mNameComponentMap.find(name);
  if (p == mNameComponentMap.end()) {
    return NULL;
  }
  CarbonSystemComponent* comp = p->second;
  return comp->getModel();
}

//! Get a singleton instance of a CarbonReplaySystem
/*!
 *! I normally don't like these sorts of APIs, but this is the only
 *! way I can think of to associate all components of a system
 *! simulation together.  E.g. maxsim components do not know anything
 *! about each other.
 */
CarbonSystemSim* CarbonSystemSim::singleton(bool* firstCall) {
  static CarbonSystemSim* the_system = NULL;
  MutexWrapper mutex(&sSystemMutex);
  if (the_system == NULL) {
    *firstCall = true;
    the_system = new CarbonSystemSim;
  } else {
    *firstCall = false;
  }
  return the_system;
}

//! Establish the name of the database
/*!
 *! This database is a directory name, in which we will
 *! store the databases of each system model, according to
 *! their instance name.
 */
void CarbonSystemSim::putDatabaseDirectory(const char* dir) {
  if (mDatabaseDirectory != dir) {
    mDatabaseDirectory = dir;

    for (NameComponentMap::SortedLoop p(mNameComponentMap.loopSorted());
         !p.atEnd(); ++p)
    {
      CarbonSystemComponent* comp = p.getValue();
      carbonReplayInfoPutDB(comp->getReplayInfo(), dir, comp->getName());
      carbonReplayInfoPutDirAction(comp->getReplayInfo(), eCarbonDirOverwrite);
    }
  }
}

void CarbonSystemSim::putCheckpointInterval(UInt32 recoverPercentage, 
                                                  UInt32 num_min_calls)
{
  if ((mMinInterval != num_min_calls) || 
      (mRecoverPercentage != recoverPercentage))
  {
    mMinInterval = num_min_calls;
    mRecoverPercentage = recoverPercentage;
    for (NameComponentMap::SortedLoop p(mNameComponentMap.loopSorted());
         !p.atEnd(); ++p)
    {
      CarbonSystemComponent* comp = p.getValue();
      carbonReplayInfoPutSaveFrequency(comp->getReplayInfo(), 
                                       mRecoverPercentage,
                                       mMinInterval);
    }
  }
}

void CarbonSystemSim::resetSimulation() {
  for (UInt32 i = 0; i < numComponents(); ++i) {
    CarbonSystemComponent* comp = getComponent(i);
    CarbonReplayInfo* replayInfo = comp->getReplayInfo();
    const char* name = comp->getName();

    if (replayInfo != NULL) {
      carbonReplayInfoPutDB(replayInfo, mDatabaseDirectory.c_str(), name);
      carbonReplayInfoPutDirAction(replayInfo, eCarbonDirOverwrite);
      if (isRecordRequested(name)) {
        if (comp->getReplayState() == eCarbonRunPlayback) {
          carbonReplayPlaybackStop(comp->getModel());
        }
        carbonReplayInfoPutSaveFrequency(replayInfo, mRecoverPercentage,
                                         mMinInterval);
        if (comp->getReplayState() != eCarbonRunRecord) {
          carbonReplayRecordStart(comp->getModel());
        }
      }
      else if (isPlaybackRequested(name)) {
        if (comp->getReplayState() == eCarbonRunRecord) {
          carbonReplayRecordStop(comp->getModel());
        }
        if (comp->getReplayState() != eCarbonRunPlayback) {
          carbonReplayPlaybackStart(comp->getModel());
        }
      }
      else {
        switch (comp->getReplayState()) {
        case eCarbonRunRecord:   carbonReplayRecordStop(comp->getModel()); break;
        case eCarbonRunPlayback: carbonReplayPlaybackStop(comp->getModel());   break;
        default:
          break;
        }
      }        
    } // if

    // set onDemand state
    CarbonObjectID *model = comp->getModel();
    if (carbonOnDemandIsEnabled(model)) {
      if (isOnDemandStartRequested(name)) {
        if (comp->getOnDemandState() == eCarbonOnDemandStopped) {
          carbonOnDemandStart(model);
        }
      }
      if (isOnDemandStopRequested(name)) {
        carbonOnDemandStop(model);
      }
    }
  } // for

} // void CarbonSystemSim::resetSimulation

void CarbonSystemSim::updateTotalSchedCalls() {
  UInt64 totalCalls = 0;
  for (UInt32 i = 0, n = numComponents(); i < n; ++i) {
    CarbonSystemComponent* comp = getComponent(i);
    determineCompScheduleCalls(comp);
    totalCalls += comp->getNumScheduleCalls();
  }
  mTotalScheduleCalls = totalCalls;
}

void CarbonSystemSim::updateSystem(CarbonReplayEventType type) {
  detectSimRestart();

  Stats::StatsData stats;
  mStats.peekIntervalStatistics(&stats);

  updateTotalSchedCalls();

  for (UInt32 i = 0, n = numComponents(); i < n; ++i) {
    CarbonSystemComponent* comp = getComponent(i);
    addEvent(type, comp, getCycles(), stats.getRealTime(),
             stats.getUserTime(), stats.getSysTime(),
             getSimTime());

    comp->updateOnDemandTrace();
  }

  // let the GUI know
  (void) writeSystem();

  // No more events should be written at this point.  Clear the timestamp
  if (type == eReplayEventExit) {
    putSimStartTimestamp(0);
  }
}

void CarbonSystemSim::updateComponent(CarbonSystemComponent* comp,
                                            CarbonReplayEventType type)
{
  detectSimRestart();

  Stats::StatsData stats;
  mStats.peekIntervalStatistics(&stats);

  determineCompScheduleCalls(comp);
  addEvent(type, comp, getCycles(), stats.getRealTime(), stats.getUserTime(),
           stats.getSysTime(), getSimTime());

  // let the GUI know
  (void) writeSystem();
}

void
CarbonSystemSim::determineCompScheduleCalls(CarbonSystemComponent* comp)
{
  CarbonObjectID* model = comp->getModel();
  if (model != NULL) {        // NULLed out on CarbonDestroy via **
    UInt64 calls = carbonGetTotalNumberOfScheduleCalls(model);
    comp->putScheduleCalls(calls);
  }
}

UInt64 CarbonSystemSim::getCycles() {
  if (mCycleCountFn != 0) {
    mCycles = (*mCycleCountFn)(mCycleCountClientData);
  }
  return mCycles;
}

void CarbonSystemSim::putCycleCountCB(CarbonSystemCycleCountFn fn,
                                            void *clientData)
{
  mCycleCountFn = fn;
  mCycleCountClientData = clientData;
  mCycleCountSpecified = true;
}

CarbonSystemComponent* CarbonSystemSim::findComponent(const char* name)
{
  NameComponentMap::iterator pos = mNameComponentMap.find(name);
  if (pos != mNameComponentMap.end()) {
    return pos->second;
  } else {
    return NULL;
  }
}

CarbonSystemComponent* CarbonSystemComponentIter::getNext()
{
  if (mLoop.atEnd()) {
    return NULL;
  } else {
    CarbonSystemComponent* comp = mLoop.getValue();
    ++mLoop;
    return comp;
  }
}

double CarbonSystemSim::getSimTime() {
  if (mSimTimeFn != 0) {
    mSimTime = (*mSimTimeFn)(mSimTimeClientData);
  }
  return mSimTime;
}

void CarbonSystemSim::putSimTimeCB(CarbonSystemSimTimeFn fn, void *clientData)
{
  mSimTimeFn = fn;
  mSimTimeClientData = clientData;
  mSimTimeSpecified = true;
}
