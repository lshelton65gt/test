// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdio.h>
#include "util/UtCheckpointStream.h"
#include "util/Stats.h"
#include "util/ShellMsgContext.h"
#include "shell/carbon_model.h"
#include "shell/carbon_model_private.h"
#include "shell/CarbonModel.h"
#include "shell/CarbonSimControl.h"
#include "hdl/HdlFileSystem.h"
#include "hdl/HdlOStream.h"
#include "hdl/HdlIStream.h"
#include "hdl/HdlVerilogOFileSystem.h"
#include "iodb/IODBRuntime.h"

#define CAST_TO_MODEL(_descr_) reinterpret_cast <CarbonModel *> ((_descr_)->mModel)

CarbonModel *carbonPrivateGetModel (struct carbon_model_descr *descr) 
{ 
  return descr ? CAST_TO_MODEL (descr) : NULL;
}

//! Find a memory based on its name
CarbonMemory *carbonPrivateFindMemory (carbon_model_descr *descr, const char *name)
{
  return CAST_TO_MODEL (descr)->findMemory (name);
}

//! Find a net based on its name
CarbonNet* carbonPrivateFindNet (carbon_model_descr *descr, const char* name)
{ 
  return CAST_TO_MODEL (descr)->findNet (name);
}

//! Reads the net's value and puts it into buf
CarbonStatus carbonPrivateExamine (carbon_model_descr *descr, const CarbonNet* net, 
  CarbonUInt32* value, CarbonUInt32* drive)
{
  return CAST_TO_MODEL (descr)->examine (net, value, drive);
}

//! Examine a word of the net's value
CarbonStatus carbonPrivateExamineWord(carbon_model_descr *descr, const CarbonNet* net, 
  CarbonUInt32* value, int index, CarbonUInt32* drive)
{
  return CAST_TO_MODEL (descr)->examineWord (net, value, index, drive);
}

//! Return the already created CarbonWave
CarbonWave *carbonPrivateGetCarbonWave (carbon_model_descr *descr)
{
  return CAST_TO_MODEL (descr)->getCarbonWave ();
}

//! Called by generated model to initialize profiling.
void carbonPrivateSetupProfiling (carbon_model_descr *descr, CarbonProfileInfo* prof_info)
{
  CAST_TO_MODEL (descr)->setupProfiling (prof_info);
}

//! Run the schedule and update time. MT-Safe
CarbonStatus carbonPrivateSchedule (struct carbon_model_descr *descr, 
  CarbonTime currentSimulationTime)
{
  return CAST_TO_MODEL (descr)->schedule (currentSimulationTime);
}

//! Run the clock schedule and update time. MT-Safe
CarbonStatus carbonPrivateClkSchedule (struct carbon_model_descr *descr, 
  CarbonTime currentSimulationTime)
{
  return CAST_TO_MODEL (descr)->clkSchedule (currentSimulationTime);
}
 
//! Run the data schedule and update time. MT-Safe
CarbonStatus carbonPrivateDataSchedule (struct carbon_model_descr *descr, 
  CarbonTime currentSimulationTime)
{
  return CAST_TO_MODEL (descr)->dataSchedule (currentSimulationTime);
}
 
//! Run the async schedule and update time. MT-Safe
CarbonStatus carbonPrivateAsyncSchedule(struct carbon_model_descr *descr, 
  CarbonTime currentSimulationTime)
{
  return CAST_TO_MODEL (descr)->asyncSchedule (currentSimulationTime);
}

//! Run the dep-combo schedule, if needed.
void carbonPrivateDepComboSchedule (struct carbon_model_descr *descr)
{
  CAST_TO_MODEL (descr)->depComboSchedule ();
}
 
//! Get the current simulation time. MT-Safe
CarbonTime carbonPrivateGetSimulationTime (const struct carbon_model_descr *descr)
{
  return descr->getModel ()->getSimulationTime ();
}

//! Put the current simulation time. MT-Safe
void carbonPrivatePutSimulationTime (struct carbon_model_descr *descr, CarbonTime simTime)
{
  CAST_TO_MODEL (descr)->putSimulationTime (simTime);
}

//! Returns the current run mode
CarbonVHMMode carbonPrivateGetRunMode (struct carbon_model_descr *descr)
{
  return CAST_TO_MODEL (descr)->getRunMode ();
}

//! Run the initialization function
CarbonStatus carbonPrivateRunInitialize (struct carbon_model_descr *descr,
  void* cmodelData, void* reserved1, void* reserved2)
{
  return CAST_TO_MODEL (descr)->initialize (cmodelData, 
    reserved1, reserved2);
}

//! This is here to prevent calling delete on these guys...
void carbon_model_descr::operator delete (void *)
{
  INFO_ASSERT (false, "called delete on a struct carbon_model instance");
}

//! ctor for struct carbon_change_array
/*! The change array should be created from the generated code with
 *  CarbonMem::malloc so define the constructor to just assert out.
 */
carbon_change_array::carbon_change_array ()
{
  INFO_ASSERT (false, "the change array must be created with CarbonMem::malloc");
}

//! Create a change array and initialise the elements to CARBON_CHANGE_FALL_MASK
CarbonChangeArray *carbonPrivateCreateChangeArray (CarbonUInt32 n_elements)
{
  CarbonChangeArray *change_array = 
    (CarbonChangeArray *) CarbonMem::malloc (sizeof (CarbonChangeArray));
  INFO_ASSERT (change_array != NULL, "change array allocation failed");
  change_array->mChanged = 
    (CarbonChangeType *) CarbonMem::malloc (sizeof (CarbonChangeType) * n_elements);
  INFO_ASSERT (change_array->mChanged != NULL, "change array element allocation failed");
  change_array->mNumElems = n_elements;
  carbonPrivateInitChangeArray (change_array, CARBON_CHANGE_FALL_MASK);
  return change_array;
}

//! Destroy a change array
void carbonPrivateDestroyChangeArray (CarbonChangeArray *change_array)
{
  CarbonMem::free (change_array->mChanged);
  CarbonMem::free (change_array);
}

//! Set all the elements of a change array to a value
void carbonPrivateSetChangeArray (CarbonChangeArray *change_array, CarbonChangeType value)
{
  // memset only works for this if the CarbonChangeType is a single byte
  INFO_ASSERT (sizeof (CarbonChangeType) == 1, "ChangeArrayType is bogus");
  ::memset (change_array->mChanged, value, change_array->mNumElems * sizeof (CarbonChangeType));
}

//! Initialise the elements of the change array
void carbonPrivateInitChangeArray (CarbonChangeArray *change_array, CarbonChangeType value)
{
  change_array->mInit = value;
  carbonPrivateSetChangeArray (change_array, value);
}

//! Reset the elements of the change array to the initial value
void carbonPrivateReInitChangeArray (CarbonChangeArray *change_array)
{
  carbonPrivateSetChangeArray (change_array, change_array->mInit);
}

//! Save a change array to a checkpoint stream
bool carbon_change_array::save (UtOCheckpointStream &out)
{
  out << mNumElems;
  for (UInt32 i = 0; i < mNumElems; i++) {
    out << mChanged[i];
  }
  return !out.fail();
}

//! Restore a change array from a checkpoint stream
bool carbon_change_array::restore (UtICheckpointStream &in)
{
  UInt32 numElements;
  in >> numElements;
  INFO_ASSERT (numElements == mNumElems, 
    "Checkpoint restore: change array size mismatch.");
  for (UInt32 i = 0; i < mNumElems; i++) {
    in >> mChanged[i];
  }
  return in.fail() ? 0 : 1;
}

void carbonPrivateCallDebugCallback (struct carbon_model_descr *descr)
{
  if (descr->mPrivate->getShellCallBack ()->mDebugFunc != NULL) {
    (*descr->mPrivate->getShellCallBack ()->mDebugFunc) (descr);
  }
}

void carbonPrivateCallDestroyCallback (struct carbon_model_descr *descr)
{
  if (descr->mPrivate->getShellCallBack ()->mDestroyFunc != NULL) {
    (*descr->mPrivate->getShellCallBack ()->mDestroyFunc) (descr);
  }
}

carbon_model_private::carbon_model_private (struct carbon_model_descr *descr, 
  void *handle)
{
  mDescr = descr;
  mMsgContext = new MsgContext;
  mErrStream = new MsgStreamIO (descr->mStdout, true);
  mMsgContext->addReportStream (mErrStream);
  mStats = new Stats;
  mScheduleCallCount = 0;
  mShellCallBack = new CarbonShellCBStruct;
  mShellCallBack->mVersion = CARBON_MODEL_VERSION;
  mShellCallBack->mDebugFunc = NULL;
  mShellCallBack->mDestroyFunc = NULL;
  mShellCallBack->mClientData = NULL;
  mRunDepositComboSched = 0;
  mChangedPrimaryClocks = NULL;
  mPrimaryClocks = NULL;
  mControlHelper = new ControlHelper (handle);
  allocateFileSystems ();
}

carbon_model_private::~carbon_model_private ()
{
  delete mStats;
  if (mChangedPrimaryClocks != NULL) {
    delete mChangedPrimaryClocks;
  }
  if (mPrimaryClocks != NULL) {
    CarbonMem::free (mPrimaryClocks);
  }
  delete mMsgContext;
  delete mErrStream;
  delete mControlHelper;
  delete mShellCallBack;
  deleteFileSystems ();
}

void carbon_model_private::allocateFileSystems ()
{
  mHDLFileSystem = new HDLFileSystem (mMsgContext);
  mVerilogOutFileSystem = new VerilogOutFileSystem (mHDLFileSystem, mMsgContext);
  mVhdlOutFileSystem = new HdlOStream (mHDLFileSystem, mMsgContext);
  mVhdlInFileSystem = new HdlIStream (mHDLFileSystem, mMsgContext);
}

void carbon_model_private::deleteFileSystems ()
{
  delete mVerilogOutFileSystem;
  delete mVhdlOutFileSystem;
  delete mVhdlInFileSystem;
  delete mHDLFileSystem;
}


struct carbon_model_private *carbonPrivateCreatePrivateModelData (
  struct carbon_model_descr *descr, CarbonOpaque handle)
{
  carbon_model_private *data = new carbon_model_private (descr, handle);
  return data;
}

CarbonUInt32 carbon_model_private::checkAndClearRunDepositComboSchedule ()
{
  int runit = mRunDepositComboSched;
  mRunDepositComboSched = false;
  return runit;
}

int carbonPrivateCheckAndClearRunDepositComboSchedule (struct carbon_model_descr *descr)
{
  return descr->mPrivate->checkAndClearRunDepositComboSchedule ();
}

void carbonPrivateFinalizeScheduleCall (struct carbon_model_descr *descr)
{
  carbonPrivateCallDebugCallback (descr);
  if (descr->printStatistics()) {
    descr->incrScheduleCallCount();
  }
  descr->mChangeArray->mInit = CARBON_CHANGE_NONE_MASK;
}

CarbonUInt32 *carbonPrivateGetRunDepositComboSchedPointer (struct carbon_model_descr *descr)
{
  return descr->mPrivate->getRunDepositComboSchedPointer ();
}

void carbonPrivateIncrScheduleCallCount (struct carbon_model_descr *descr)
{
  descr->mPrivate->incrScheduleCallCount ();
}

//! Reset the schedule call count
void carbonPrivateResetScheduleCallCount (struct carbon_model_descr *descr)
{
  descr->mPrivate->resetScheduleCallCount  ();
}

//! Use the Stats obj to print the interval stats
void carbonPrivatePrintIntervalStatistics (struct carbon_model_descr *descr, const char *label)
{
  descr->mPrivate->printIntervalStatistics (label);
}

//! If statistics are to be printed, print them out
void carbonPrivateMaybePrintStatsStdout (struct carbon_model_descr *descr, const char* name, size_t objSize)
{
  // If stats are enabled, print them now
  if (descr->printStatistics())
  {
    fprintf(stdout, "%10ld Calls made to carbon_%s_schedule\n",
      (long) descr->mPrivate->getScheduleCallCount (), name);
    fprintf(stdout, "%10ld Bytes data memory for design\n", (long) objSize);
    descr->printIntervalStatistics("Total");
  }
}

void carbonPrivateFreePrivateModelData (struct carbon_model_descr *descr)
{
  delete descr->mPrivate;
}

void carbon_model_private::allocatePrimaryClockChangeData (size_t numClocks)
{
  mChangedPrimaryClocks = new DynBitVector(numClocks);
  mPrimaryClocks = (const char **) CarbonMem::malloc (numClocks * sizeof (const char *));
}

//! Allocate space for primary clock changed array and clock names
// This routine only be called if glitch detection is turned on
void carbonPrivateAllocatePrimaryClockChangeData (struct carbon_model_descr *descr, size_t numClocks)
{
  descr->mPrivate->allocatePrimaryClockChangeData (numClocks);
}

void carbon_model_private::setClockName (size_t index, const char* name)
{
  INFO_ASSERT(index < mChangedPrimaryClocks->size(), 
    "Inconsistency found when testing for clock race conditions");
  mPrimaryClocks [index] = name;
}

//! Set the name for a given primary clock
// This routine only be called if glitch detection is turned on
void carbonPrivateSetClockName(struct carbon_model_descr *descr, size_t index, const char* name)
{
  descr->mPrivate->setClockName (index, name);
}

//! Clear the primary clock changed array
// This routine only be called if glitch detection is turned on
void carbonPrivateClearPrimaryClockChanges (struct carbon_model_descr *descr)
{
  descr->mPrivate->clearPrimaryClockChanges ();
}

void carbon_model_private::clearPrimaryClockChanges ()
{ mChangedPrimaryClocks->reset (); }

//! Test and potential print a race
// This routine only be called if glitch detection is turned on
void carbon_model_private::checkPrimaryClockRace (const DynBitVector& curChanges)
{
  // Check if any changes happened with a previous call using the same
  // time stamp (we assume that curChanges has some changes in it)
  if (mChangedPrimaryClocks->any() && curChanges.any()) {
    // Print messages about the potential clock glitch
    mMsgContext->SHLPotentialFlopRace(*mDescr->mpTime);
    for (size_t i = 0; i < mChangedPrimaryClocks->size(); ++i) {
      if (mChangedPrimaryClocks->test(i)) {
        mMsgContext->SHLPrevCallClockChange (mPrimaryClocks [i]);
      }
    }
    for (size_t i = 0; i < curChanges.size(); ++i) {
      if (curChanges.test(i)) {
        mMsgContext->SHLCurCallClockChange (mPrimaryClocks [i]);
      }
    }
  }

  // Update the current clock changes for the next call
  *mChangedPrimaryClocks |= curChanges;
}

void carbonPrivateCheckPrimaryClockRace (struct carbon_model_descr *descr, const DynBitVector& curChanges)
{
  if (descr->checkClockGlitches()) {
    descr->mPrivate->checkPrimaryClockRace (curChanges);
  }
}

//! No longer used, but must remain for backwards compatibilty
void carbonPrivateSHLClockGlitch (struct carbon_model_descr *descr, const char *name, 
  unsigned int oldVal, unsigned int newVal)
{
  CAST_TO_MODEL (descr)->getMsgContext ()->SHLClockGlitch (name, oldVal, newVal, *descr->mpTime);
}

void carbonPrivateSHLClockGlitchByIndex (struct carbon_model_descr *descr, UInt32 index, 
  unsigned int oldVal, unsigned int newVal)
{
  CarbonModel* model = CAST_TO_MODEL(descr);
  IODBRuntime* iodb = model->getIODB();
  UtString* clkName = iodb->getClockGlitchName(index);
  INFO_ASSERT(clkName != NULL, "Clock glitch name not found");
  model->getMsgContext ()->SHLClockGlitch (clkName->c_str(), oldVal, newVal, *descr->mpTime);
}

//! \return the control helper object for the model
ControlHelper *carbonPrivateGetControlHelper (struct carbon_model_descr *descr)
{
  return descr->mPrivate->getControlHelper ();
}

//! returns the current carbon status for the current run of the schedule
CarbonStatus carbonPrivateGetCarbonStatus  (struct carbon_model_descr *descr)
{
  return descr->mPrivate->getControlHelper ()->getCarbonStatus ();
}

//! reset the carbonStatus for the current run of the schedule
void carbonPrivateResetCarbonStatus (struct carbon_model_descr *descr)
{
  descr->mPrivate->getControlHelper ()->resetCarbonStatus ();
}


//! use this to update the carbonStatus when an error has been processed in the schedule
void carbonPrivateUpdateCarbonStatusDueToError (struct carbon_model_descr *descr)
{
  descr->mPrivate->getControlHelper ()->updateCarbonStatusDueToError ();
}


//! use this to update the carbonStatus when a stop has been processed in the schedule
void carbonPrivateUpdateCarbonStatusDueToStop (struct carbon_model_descr *descr)
{
  descr->mPrivate->getControlHelper ()->updateCarbonStatusDueToStop ();
}


//! use this to update the carbonStatus when a finish has been processed in the schedule
void carbonPrivateUpdateCarbonStatusDueToFinish (struct carbon_model_descr *descr)
{
  descr->mPrivate->getControlHelper ()->updateCarbonStatusDueToFinish ();
}

//! returns the VerilogOutFileSystem for this model.
VerilogOutFileSystem* carbonPrivateGetVerilogOutFileSystem (struct carbon_model_descr *descr)
{ return descr->mPrivate->getVerilogOutFileSystem (); }

//! returns the VhdlOutFileSystem for this model.
HdlOStream* carbonPrivateGetVhdlOutFileSystem (struct carbon_model_descr *descr)  
{ return descr->mPrivate->getVhdlOutFileSystem (); }

//! returns the VhdlInFileSystem for this model.
HdlIStream* carbonPrivateGetVhdlInFileSystem (struct carbon_model_descr *descr)
{ return descr->mPrivate->getVhdlInFileSystem (); }

//! Remove a model from the shell model finder
void carbonPrivateRemoveModel (struct carbon_model_descr *descr)
{
  ShellGlobal::gCarbonRemoveModel (descr);
}

CarbonChangeType *carbonPrivateGetChanged (struct carbon_model_descr *descr)
{
  return descr->mChangeArray->mChanged;
}

SInt8 carbonPrivateGetChangeInit (struct carbon_model_descr *descr)
{
  return descr->mChangeArray->mInit;
}

CarbonUInt64 *carbonPrivateGetTimeVarAddr (CarbonOpaque handle)
{
  return ShellGlobal::gCarbonGetTimevarAddr (handle);
}
