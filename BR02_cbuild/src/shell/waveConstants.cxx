// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Determine the signals with constant value across an entire FSDB file.
*/

//#include "waveform/WfVCDFileReader.h"
#include "waveform/WfFsdbFileReader.h"
#include "waveform/WfVCDSignal.h"

#include "symtab/STSymbolTable.h"

#include "util/AtomicCache.h"
#include "util/DynBitVector.h"
#include "util/UtConv.h"
#include "util/UtIOStream.h"
#include "util/UtUInt64Factory.h"

typedef UtVector<WfAbsFileReader*> WfFileReaderVector;
typedef UtVector<WfAbsFileReader::SigHier> WfSignalVector;
int main(int argc, char* argv[])
{
  AtomicCache atomicCache;
  DynBitVectorFactory dynFactory(true);
  UtUInt64Factory timeFactory;

  // For now, assume that we have FSDB files.
  WfFileReaderVector wfFiles;
  for (int i = 1; i < argc; ++i) {
    WfAbsFileReader* waveFile = WfFsdbFileReaderCreate(argv[i], &atomicCache, &dynFactory, &timeFactory);
    wfFiles.push_back(waveFile);
  }

  int error = 0;
  for (size_t i = 0; i < wfFiles.size(); ++i) {
    WfAbsFileReader * wf = wfFiles[i];

    UtString errorMsg;
    WfAbsFileReader::Status status;

    status = wf->loadSignals(&errorMsg);
    if (status != WfAbsFileReader::eOK) {
      fprintf(stderr, "%s\n", errorMsg.c_str());
      error = 1;
      continue;
    }

    WfSignalVector signals;
    wf->listAndWatchAll(&signals);

    status = wf->loadValues(&errorMsg);
    if (status != WfAbsFileReader::eOK) {
      fprintf(stderr, "%s\n", errorMsg.c_str());
      error = 1;
      continue;
    }

    size_t signal_count = signals.size();
    for (size_t id = 0; id < signal_count; ++id) {
      WfAbsSignal * signal = signals[id].mSignal;
      bool isReal = signal->isReal();
      if (isReal) {
        continue; // skip reals because they need complicated comparisons.
      }

      UtString msg;
      WfAbsSignalVCIter * vc_iter = wf->createSignalVCIter(signal, &msg);
      vc_iter->gotoMinTime();

      if (vc_iter->atEnd()) {
        continue;               // No data for this signal. 
      }

      // Determine the initial value for this signal. This complete
      // value contains both value and drive packaged as one vector.
      const DynBitVector * initial_complete_value;
      vc_iter->getCompleteValue(&initial_complete_value);

      UInt64 vc_time = vc_iter->getTime();
      UInt32 value_width = vc_iter->getWidth();

      vc_iter->gotoNextChangeTime();
      if (vc_iter->atEnd()) {
        // Only one value-change for this signal.
        UtString name;
        wf->getSymbolTable()->getHdlHier()->compPathHier(signals[id].mLeaf, &name);

        // We don't have a valid vc_iter anymore and want to avoid pre-computing strings.
        DynBitVector value(value_width);
        DynBitVector drive(value_width);
        WfVCDSignalVCIter::calcValueDrive(&value,&drive,initial_complete_value,value_width);

        // Write out the value as a string, including x/z.
        UtString value_str;
        CarbonValRW::writeBin4ToStr(&value_str, &value, &drive);

        UtIO::cout() << "Signal: " << name << UtIO::endl;
        UtIO::cout() << "Time:   " << vc_time << UtIO::endl;
        UtIO::cout() << "Value:  " << value_width << "'b" << value_str << UtIO::endl;
      } else {
        // Multiple value-changes for this signal.
        const DynBitVector * complete_value;
        vc_iter->getCompleteValue(&complete_value);

        // FSDB should not contain nop VCs.
        if (initial_complete_value != complete_value) {
          printf("Signal value consistency error.\n");
        }
      }

      delete vc_iter;
    }
  }

  for (size_t i = 0; i < wfFiles.size(); ++i) {
    WfAbsFileReader * wf = wfFiles[i];
    delete wf;
  }

  return error;
}
