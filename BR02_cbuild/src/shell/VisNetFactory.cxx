// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/VisNetFactory.h"
#include "shell/ShellVisNet.h"
#include "CarbonDatabase.h"
#include "CarbonDatabaseNode.h"
#include "iodb/IODBRuntime.h"
#include "iodb/IODBUserTypes.h"

VisNetFactory::VisNetFactory(CarbonHookup* hookup, IODBRuntime* db)
  : mHookup(hookup),
    mDB(db),
    mTypeDictionary(db->typeDictionary())
{
}

VisNetFactory::~VisNetFactory()
{
  // Free all allocated nets.  Normally, when a net is found, the
  // corresponding leaf's shell data is updated with the net, allowing
  // it to be freed automatically upon model destruction.  However,
  // visibility nets don't have a one-to-one mapping with symtab
  // leaves, so we'll manage them here.
  for (AttrNetMap::UnsortedLoop l = mAllocatedNets.loopUnsorted(); !l.atEnd(); ++l) {
    const NetAttr *attr = (*l).first;
    ShellNet *net = (*l).second;
    delete attr;
    delete net;
  }
}
// fill in the indices array (and matching sizes) arrays with the index(s) that should be used to access a UserArray
void VisNetFactory::buildIndices(const CarbonDatabaseNode* node, UIntArray* indices, UIntArray* sizes)
{
  // We have a special case to consider here. If the user is referencing a node
  // that is a multi-dimensional packed array, with no unpacked dimensions, then
  // memory resynthesis changes it into a memory with one unpacked dimension [0:0].
  // For example, the SystemVerilog reg:
  //
  //   reg [3:0][2:1] a;
  //
  // is changed into:
  //
  //   reg [3:0][2:1] a [0:0];
  //
  // So a user reference to 'a' needs to be transformed to a[0].
  // indices and sizes arrays are filled in with the slowest changing dimension listed first, this normally means unpacked dimensions(s) first then packed dimension(s)
  {
  UInt32 nPackedDims = 0;
  UInt32 nUnpackedDims = 0;
    CarbonDatabase::sGetPackedUnpackedDimensionCount(node, &nPackedDims, &nUnpackedDims, NULL);
  if ((nPackedDims > 1) && (nUnpackedDims == 0)) {
      INFO_ASSERT((indices->size() == 0), "Adding implicit dimension into wrong slot.");
      indices->push_back(0);      // first add the implicit unpacked dimension (for the [0:0])
    sizes->push_back(1);
  }
  }
  
  // Walk this node and its parents until we reach the root
  for (const CarbonDatabaseNode *curr = node; curr != NULL; curr = curr->getParent()) {
    // See if this is an array index
    if (curr->isArrayDim()) {
      // Get the index for this array
      SInt32 dimIndex = curr->getIndex();

      // Determine the range of the dimension of this element.  Note
      // that we actually want the UserType for the parent here, since
      // that represents the dimension of which this node is an element.
      const CarbonDatabaseNode *parent = curr->getParent();
      const UserType *type = parent->getUserType();
      INFO_ASSERT(type != NULL, "Null user type");
      const UserArray *arrayType = type->castArray();
      INFO_ASSERT(arrayType != NULL, "Not an array");
      const ConstantRange *range = arrayType->getRange();
      UInt32 dimSize = range->getLength();

      // Normalize as an offset from the LSB
      dimIndex = range->offsetBounded(dimIndex);
      // Save the index and size
      indices->push_back(dimIndex);
      sizes->push_back(dimSize);
    }
  }
}

ShellNet* VisNetFactory::createNet(const CarbonDatabaseNode* node)
{
  // Create empty index/size arrays for calculating the expression
  // data.  Everything we need is contained in the node.
  UIntArray containingArrayIndices;
  UIntArray containingArraySizes;
  ExprData exprData;
  // Don't pass in a user type.  It will be determined automatically.
  getExpressionData(node, NULL, &containingArrayIndices, &containingArraySizes, &exprData);
  // Don't use a temp net manager.  We want the net and all its
  // expressions to persist.
  ShellNet *retNet = maybeCreateExpressionNet(exprData, node, NULL);

  return retNet;
}

void VisNetFactory::calcExpr(UIntArray* containingArrayIndices,  UIntArray* containingArraySizes,
                             ExprData* exprData, bool isMemory, UInt32 width)
{
  // We may need a partselect into either the net or the selected
  // memory word, due to memory transformations.  The need for
  // partselects depends on three things:
  // 1. The number of dimensions to identify this node
  // 2. The width (in bits) of Carbon's implementation of the node
  // 3. Whether the ShellNet is a memory or a scalar/vector

  // Start with the number of dimensions
  UInt32 numDims = containingArrayIndices->size();
  UInt32 dimsForPartsel = 0;
  if (numDims == 1) {
    // If the ShellNet isn't a memory, we need to transform this
    // dimension into a partselect.
    if (!isMemory) {
      dimsForPartsel = 1;
    }
  } else if (numDims >= 2) {
    if (!isMemory) {
      // All the dimensions need to be converted to a partsel
      dimsForPartsel = numDims;
    } else if (width == 1) {
      // We have a scalar that has 2 or more containing arrays, which
      // means the base ShellNet is a memory.  There are two options
      // for the mapping:
      //
      // 1. The innermost dimension has been extracted and mapped to
      // the width of the memory.  The remaining dimensions are
      // converted to a single dimension representing the depth of the
      // memory.  In that case, we need to convert that last index to
      // a partsel.
      //
      // 2.  All the dimensions have been converted to a single
      // dimension representing the depth of the memory, which is only
      // one bit wide.  In that case, no partsel is required.
      //
      // To determine which case we're dealing with, check the
      // intrinsics of the leaf.  If it's more than 1 bit wide, we
      // know we have case #1 and need the partsel.
      STAliasedLeafNode* leaf = exprData->getDesignLeaf();
      const ShellDataBOM* bom = ShellSymTabBOM::getLeafBOM(leaf);
      const IODBIntrinsic* intr = bom->getIntrinsic();
      if (intr->getWidth() != 1) {
        dimsForPartsel = 1;
      }
    }
  }

  if (dimsForPartsel != 0) {
  // Now, calculate the LSB of the partselect.  It's 0 to start.
  // We're indexing into normalized vectors, so this is unsigned.
    UInt32 partselLsb = 0;
    // If we're using N dimensions, we want to start at element N-1
    // and work backwards until element 0.
    for (SInt32 i = dimsForPartsel - 1; i >= 0; --i) {
      // First, scale the current LSB by the size of the last
      // (i.e. outermost) dimension
      UInt32 scale = (*containingArraySizes)[i];
      partselLsb *= scale;
      // Then, add the outermost index
      UInt32 index = (*containingArrayIndices)[i];
      partselLsb += index;
    }

    // There's no easy way to erase from the front of a UtArray, so
    // just update the number of meaningful dimensions remaining.
    numDims -= dimsForPartsel;

    // Although we could eventually support partselects, we should
    // only ever need bitselects.  If the width of a net is greater
    // than 1, it should be mapped to a word in a memory.
    if (0) // TODO: suppress this for now
    {
      ST_ASSERT(width == 1, exprData->getDesignLeaf());
    }
    exprData->setBitsel(partselLsb);
  }

  // In a similar manner, we need to take the remaining indices (if
  // any) and compute a memory index.
  UInt32 dimsForMemIndex = numDims;
  UInt32 memIndex = 0;
  if (dimsForMemIndex != 0) {
    // We should have a memory net if we get here
    INFO_ASSERT(isMemory, "Expected a memory net");
    for (UInt32 i = 0; i < dimsForMemIndex; ++i) {
      // First, scale the current index by the size of the last
      // (i.e. outermost) dimension
      UInt32 scale = containingArraySizes->back();
      memIndex *= scale;
      // Then, add the outermost index
      UInt32 index = containingArrayIndices->back();
      memIndex += index;

      containingArraySizes->pop_back();
      containingArrayIndices->pop_back();
    }

    // Record the index
    exprData->setMemIndex(memIndex);
  }
}


void VisNetFactory::getExpressionData(const CarbonDatabaseNode* node, const UserType* userType,
                                      UIntArray* containingArrayIndices, UIntArray* containingArraySizes,
                                      ExprData* exprData)
{
  // Determine the indices/sizes for any containing arrays of this
  // node.  Note that this appends to what may already be there.
  // in addition note that it only gets the array indices, (not any partselect range), but will include index for implicit [0:0] range added to multi packed dimension objects
  buildIndices(node, containingArrayIndices, containingArraySizes);
  // Get storage leaf from design node
  const STSymbolTableNode *designNode = node->getSymTabNode();
  // This needs to be non-const, because CarbonHookup::getCarbonNet
  // needs to modify the leaf's BOM data.
  STAliasedLeafNode *designLeaf = const_cast<STAliasedLeafNode*>(designNode->castLeaf());
  INFO_ASSERT(designLeaf, "design leaf not found");
  exprData->putDesignLeaf(designLeaf);

  // Get the shell net for this leaf.  It may be a memory.
  ShellNet *shellNet = mHookup->getCarbonNet(designLeaf);
  exprData->putShellNet(shellNet);
  if (shellNet == NULL) {
    // Shell net not found. Return with exprData empty.
    return;
  }
  CarbonMemory *memNet = shellNet->castMemory();

  // If no user type was passed in, get it from the node.
  if (userType == NULL) {
    userType = node->getUserType();
  }

  // If we still don't have a user type, the model must not support
  // it, so there are no expressions to calculate
  if (userType == NULL) {
    return;
  }

  // Determine the width of the node, i.e. the number of bits needed
  // to represent it in the model's storage.  getSize() calculates this correctly regardless of the specific user type.
  UInt32 width = userType->getSize();
  exprData->putNumBits(width);


  // Determine the partsel/memindex that are required, if any
  bool isMemory = (memNet != NULL);
  calcExpr(containingArrayIndices, containingArraySizes, exprData, isMemory, width);

  // If this is an array, save its range
  const UserArray *arrayType = userType->castArray();

  // We only want to attach the vector range if this is a 1-d packed array.
  // If it's a multi-d packed array, we cannot do multi-bit selects on the
  // packed portion of the array (yet).
  UInt32 nPacked = 0;
  UInt32 packedWidth = 0;
  CarbonDatabase::sGetPackedUnpackedDimensionCount(node, &nPacked, NULL, &packedWidth);
  if ( arrayType ) {
    if ( nPacked <= 1) {
      // save user declared range for a word of memory. that is the [10:8] of reg [10:8] mem [2:3][4:7];
      exprData->putArrayRange(arrayType->getRange(), false); // second arg false says exprData does not own the range
    } else {
      // here there is more than a single packed dimension, so we know that it must have been resynthesized and thus it will be normalized so we create a range that will hold it
      // save user declared range for a word of memory. that is the [10:8] of reg [10:8] mem [2:3][4:7];
      const ConstantRange *arrayRange = new ConstantRange(packedWidth-1,0);
      exprData->putArrayRange(arrayRange, true); // save with flag to indicate that the ownership to exprData is transfered to exprData
    }
  }
}

ShellNet* VisNetFactory::maybeCreateExpressionNet(const ExprData& exprData, const CarbonDatabaseNode* node,
                                                  TempNetManager* netMgr)
{
  ShellNet *retNet = NULL;

  // Extract some info from the ExprData
  ShellNet *shellNet = exprData.getShellNet();
  if (shellNet == NULL) {
    // ExprData has no shell net.
    return NULL;
  }
  const STAliasedLeafNode *designLeaf = exprData.getDesignLeaf();
  const ConstantRange *arrayRange = exprData.getArrayRange(); // if a memory is being read then holds word range, so if declared as reg [7:5] mem [2:3]; then arrayRange holds [7:5]

  // We need to determine what type of net we need to return.  If this
  // is simply an element of a structure (nested or simple), with no
  // containing indices and no partsel, we can just use the shell net
  // we found earlier.
  UInt32 bitsel = 0;
  UInt32 memIndex = 0;       // this is a normalized index into the memory, and if the memory was resynthesized it is the normalized index into the resynthesized memory
  SInt32 signedMemIndex = 0; // signedMemIndex holds the exprData->mMemIndex converted to range of intrinsic memory, (see mMemIndex to get the normalized memory word index)
  SInt32 hdlBit = 0;
  bool needsBitsel = exprData.getBitsel(&bitsel);
  bool needsMemIndex = exprData.getMemIndex(&memIndex);

  if (!needsBitsel && !needsMemIndex) {
    retNet = shellNet;
  } else {
    // This will be an expression of some sort.
    // Get some more info about the net.
    //
    // First, we need to get the intrinsic for the net.  This allows
    // us to express our normalized bitselect and memory index in
    // terms of the net's implemented range.  Determining the correct
    // intrinsic is tricky.  We need to pick the one that matches the
    // primitive net.
    //
    // For vector nets, use the intrinsic associated with the actual
    // leaf node from the symbol table.  This is because primitive
    // vector nets don't have a range upon construction.  Instead,
    // their range is set after creation based on the intrinsic of the
    // corresponding symtab node.  Multiple aliased nodes can
    // therefore have vector nets with different ranges.
    //
    // For memory nets, use the intrinsic associated with the leaf
    // node's storage.  This is because primitive memories have a
    // range upon creation.  The debug generator function emitted by
    // codegen forces the memory to have the same range as its storage
    // node's intrinsic.
    const STAliasedLeafNode* whichLeaf = designLeaf;
    if (needsMemIndex) {
      whichLeaf = designLeaf->getStorage();
    }
    const ShellDataBOM *shellNetBOM = ShellSymTabBOM::getLeafBOM(whichLeaf);
    const IODBIntrinsic *shellNetIntr = shellNetBOM->getIntrinsic();

    // If this is a memory net, determine the signed memory index.
    // The one in the ExprData is an offset from the LSB.
    if (needsMemIndex) {
      const ConstantRange* memRange = shellNetIntr->getMemAddrRange();
      INFO_ASSERT(memRange != NULL, "Intrinsic memory range is NULL");
      signedMemIndex = memRange->index(memIndex); // signedMemIndex holds the memory word index in terms of range of intrinsic memory, so if it was resynthesized then in terms of resynthesized address range of memory
    }

    // Do a similar thing for the bitsel
    if (needsBitsel) {
      const ConstantRange* vecRange = shellNetIntr->getVecRange();
      INFO_ASSERT(vecRange != NULL, "Intrinsic vector range is NULL");
      hdlBit = vecRange->index(bitsel);
    }

    // Create a new intrinsic for the visibility net.  Use the range
    // of the user type for arrays.  Again, use either the factory or
    // temp net manager as appropriate.
    const IODBIntrinsic *netIntr;
    if (arrayRange != NULL) {
      if (netMgr != NULL) {
        netIntr = new IODBIntrinsic(arrayRange);
        netMgr->mIntrinsic = netIntr;
      } else {
        netIntr = mTypeDictionary->addVectorIntrinsic(arrayRange->getMsb(), arrayRange->getLsb());
      }
    } else {
      // The user type is scalar, but that doesn't mean its
      // IODBIntrinsic should be scalar.  IODBIntrinsic scalar means a
      // single bit, but things like integers, chars, etc. are scalars
      // as far as user type is concerned.  In that case, create a
      // vector instrinsic to match the actual storage in the model.
      UInt32 numBits = exprData.getNumBits();
      if (numBits == 1) {
        netIntr = mTypeDictionary->addScalarIntrinsic();
      } else {
        netIntr = mTypeDictionary->addVectorIntrinsic(numBits - 1, 0);
      }
    }

    // Attempt to use an existing visibility net matching these
    // attributes.  If it doesn't exist, create one.
    NetAttr attr(shellNet, netIntr, needsBitsel, needsMemIndex, bitsel, signedMemIndex);
    ShellVisNet *visNet = lookupNet(attr);
    if (visNet == NULL) {
      // Create the right kind
      if (needsBitsel && needsMemIndex) {
        visNet = new ShellVisNetMemBitsel(shellNet, node, netIntr, signedMemIndex, bitsel, hdlBit);
      } else if (needsMemIndex) {
        visNet = new ShellVisNetMemsel(shellNet, node, netIntr, signedMemIndex);
      } else if (needsBitsel) {
        visNet = new ShellVisNetBitsel(shellNet, node, netIntr, bitsel, hdlBit);
      } else {
        ST_ASSERT(0, node->getSymTabNode());
      }
      // Use the storage net here, so net->ShellDataBOM mappings work
      visNet->setName(shellNet->getName());
      if (netMgr != NULL) {
        // This is a temporary net, so record it with the temp net
        // manager.
        netMgr->mNet = visNet;
      } else {
        // Cache it here along with its attributes.  This allows the
        // same net to be returned if looked up again.
        cacheNet(attr, visNet);
      }
    }

    retNet = visNet;
  }

  return retNet;
}

ShellNet* VisNetFactory::createTempNet(CarbonDatabaseNode* node, const UserType *userType,
                                       TempNetManager* netMgr, const SInt32* indices,
                                       UInt32 numDims, const RangeArray& ranges)
{
  // Build a pair of arrays holding the size and index for each
  // dimension specified by the arguments, starting with the innermost
  // (i.e. last).
  UIntArray containingArrayIndices;
  UIntArray containingArraySizes;
  for (SInt32 i = numDims - 1; i >= 0; --i) {
    const ConstantRange *range = ranges[i];
    containingArraySizes.push_back(range->getLength());
    SInt32 index = indices[i];
    // Normalize as an offset from the LSB
    index = range->offsetBounded(index);
    containingArrayIndices.push_back(index);
  }

  // It was thought that there is a possible error situation here by calling getExpressionData below, If containingArrayIndices and
  // containingArraySizes are not empty and the memory we are examining had no declared unpacked dimension, then getExpressionData()calls
  // buildIndicies() which will add the implicit [0:0] dimension.  buildIndicies has an assert making sure that we never add the implicit
  // dimension in the wrong place.  The current thinking is that we never get here (createTempNet() if there the node had no unpacked
  // dimensions).
  //
  // it may add the implicit unpacked dimension to containingArrayIndices, and if this array was not empty then the [0:0] will be inserted in the wrong place.
  // Now, add whatever expressions may be needed by the node itself.
  // Pass in the user type for this dimension.
  ExprData exprData;
  getExpressionData(node, userType, &containingArrayIndices, &containingArraySizes, &exprData);
  // Create the net, using the TempNetManager so the factories are
  // bypassed and the net is not cached.
  ShellNet *retNet = maybeCreateExpressionNet(exprData, node, netMgr);

  // Replay may be active, so wrap the net.  Don't apply OnDemand
  // wrappers, though, because that's done above the level of this
  // net, and this net only exists temporarily.  We've already forced
  // a break from idle or state restore, as necessary.
  //
  // This isn't an issue for Replay because its instrumentation is
  // done at the primitive net level, and those nets persist, even if
  // our expression nets don't.
  CarbonModel *model = mHookup->getCarbonModel();
  retNet = model->applyNetWrappers(retNet, false);

  return retNet;
}

VisNetFactory::NetAttr::NetAttr(const CarbonNet* wrappedNet, const IODBIntrinsic* intrinsic,
                                bool needsBitsel, bool needsMemsel, UInt32 bitsel, SInt32 memsel)
  : mWrappedNet(wrappedNet),
    mIntrinsic(intrinsic),
    mNeedsBitsel(needsBitsel),
    mNeedsMemsel(needsMemsel),
    mBitsel(bitsel),
    mMemsel(memsel)
{
}

VisNetFactory::NetAttr::~NetAttr()
{
}

size_t VisNetFactory::NetAttr::hash() const
{
  // Don't worry about flags.  They shouldn't cause collisions that
  // frequently.
  size_t val = reinterpret_cast<size_t>(mWrappedNet) + reinterpret_cast<size_t>(mIntrinsic) + mBitsel + mMemsel;
  return val;
}

bool VisNetFactory::NetAttr::equal(const NetAttr* other) const
{
  return ((mWrappedNet == other->mWrappedNet) &&
          (mIntrinsic == other->mIntrinsic) &&
          (mNeedsBitsel == other->mNeedsBitsel) &&
          (mNeedsMemsel == other->mNeedsMemsel) &&
          (mBitsel == other->mBitsel) &&
          (mMemsel == other->mMemsel));
}

VisNetFactory::NetAttr* VisNetFactory::NetAttr::allocateACopy() const
{
  return new NetAttr(mWrappedNet, mIntrinsic, mNeedsBitsel, mNeedsMemsel, mBitsel, mMemsel);
}

size_t VisNetFactory::MapHelper::hash(const NetAttr *var) const
{
  return var->hash();
}

bool VisNetFactory::MapHelper::equal(const NetAttr *v1, const NetAttr *v2) const
{
  return v1->equal(v2);
}

ShellVisNet* VisNetFactory::lookupNet(const NetAttr& attr)
{
  ShellVisNet *net = NULL;
  AttrNetMap::iterator iter = mAllocatedNets.find(&attr);
  if (iter != mAllocatedNets.end()) {
    net = iter->second;
  }
  return net;
}

void VisNetFactory::cacheNet(const NetAttr& attr, ShellVisNet* net)
{
  // We need a copy, because the attribute passed in is probably
  // allocated on the stack.
  NetAttr *copyAttr = attr.allocateACopy();
  mAllocatedNets[copyAttr] = net;
}

VisNetFactory::ExprData::ExprData()
  : mNeedsBitsel(false),
    mNeedsMemIndex(false),
    mBitsel(0),
    mMemIndex(0),
    mNumBits(0),
    mShellNet(NULL),
    mDesignLeaf(NULL),
    mExprDataOwnsArrayRange(false),
    mArrayRange(NULL)
{
}

VisNetFactory::ExprData::~ExprData()
{
  if ( mExprDataOwnsArrayRange ) {
    delete mArrayRange;
  }
}

void VisNetFactory::ExprData::setBitsel(UInt32 bitsel)
{
  // Only allow this once
  INFO_ASSERT(mNeedsBitsel == false, "Bitsel is already set");
  mNeedsBitsel = true;
  mBitsel = bitsel;
}

void VisNetFactory::ExprData::setMemIndex(UInt32 index)
{
  // Only allow this once
  INFO_ASSERT(mNeedsMemIndex == false, "Mem index is already set");
  mNeedsMemIndex = true;
  mMemIndex = index;
}

bool VisNetFactory::ExprData::getBitsel(UInt32* bitsel) const
{
  if (!mNeedsBitsel) {
    return false;
  }

  *bitsel = mBitsel;
  return true;
}

bool VisNetFactory::ExprData::getMemIndex(UInt32 *index) const
{
  if (!mNeedsMemIndex) {
    return false;
  }

  *index = mMemIndex;
  return true;
}

VisNetFactory::TempNetManager::TempNetManager()
  : mNet(NULL),
    mIntrinsic(NULL)
{
}

VisNetFactory::TempNetManager::~TempNetManager()
{
  delete mNet;
  delete mIntrinsic;
}
