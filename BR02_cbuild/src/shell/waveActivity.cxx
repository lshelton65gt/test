// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
// Print the switching activity (value change counts) for an fsdb file.
#include "util/CarbonPlatform.h"

#include "ffrAPI.h"
#include "util/UtMap.h"
#include "util/UtStringUtil.h"
#include "util/OSWrapper.h"
#include "shell/carbon_shelltypes.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// We reserve 1 bucket for signals with 1 change and one bucket for
// signals with the max activity.
#define LOW_RESERVED_BUCKETS 1
#define HIGH_RESERVED_BUCKETS 1
#define RESERVED_BUCKETS ((LOW_RESERVED_BUCKETS)+(HIGH_RESERVED_BUCKETS))
#define UNRESERVED_BUCKETS 20
#define TOTAL_BUCKETS ((RESERVED_BUCKETS)+(UNRESERVED_BUCKETS))
#define THRESHOLD 1
#define LOAD_BUNDLE_SIZE 1000

typedef UtMap<int, UInt64> ActivityMap;
typedef UtMap<fsdbVarIdcode, char*> SymbolTable;

static SymbolTable symbolTable;


static bool_T __MyTreeCB(fsdbTreeCBType cb_type, 
                         void * /*client_data */, 
                         void *tree_cb_data)
{
  if( cb_type == FSDB_TREE_CBT_VAR ) {
    fsdbTreeCBDataVar* var = static_cast<fsdbTreeCBDataVar*>(tree_cb_data);

    char * signal_name = StringUtil::dup(var->name);
    symbolTable[var->u.idcode] = signal_name;
  }

  return true;
}

int main(int, char** argv)
{
  if( !argv[1] ) {
    printf("Dude, I need an fsdb file.\n");
    exit(1);
  }

  UInt64 maxActivity = 0;

  ActivityMap activityMap;      // Number of changes for each signal.
  ffrObject * wf = ffrObject::ffrOpen( argv[1], __MyTreeCB, 0 ); 
  if (wf==NULL) {
    printf("Could not process FSDB file: %s\n", argv[1]);
    exit(1);
  }

  printf("waveActivity for %s\n", argv[1]);
  printf("------------------------------------------------------------\n");

  fsdbVarIdcode maxId = wf->ffrGetMaxVarIdcode();
  wf->ffrReadScopeVarTree();

  SInt64 signals = 0;           // Total signals.
  SInt64 signals2 = 0;          // Signals with >= THRESHOLD toggles.

  UInt64 totalActivity = 0;     // Activity for all signals.
  UInt64 totalActivity2 = 0;    // Activity for signals with >= THRESHOLD toggles.

  fsdbVarIdcode old_i = 1;
  for(fsdbVarIdcode i=1; i<=maxId; i++) {
    wf->ffrAddToSignalList( i );

    // Process signals in groups of LOAD_BUNDLE_SIZE. Not sure about
    // the speed/memory consequences of this vs. per-signal
    // processing.
    if(i == maxId || ((i % LOAD_BUNDLE_SIZE) == 0)) {
      wf->ffrLoadSignals();
      for(fsdbVarIdcode j = old_i; j <=maxId && j<=i; ++j) {
        UInt64 count = wf->ffrGetVCCount(j);
        activityMap[j] = count;
        if( count > maxActivity ) {
          maxActivity = count;
        }

        // printf("ID:%d ACTIVITY:" Format64(d) " NAME:%s\n",j,count,symbolTable[ j ]);

        ++signals;
        totalActivity += count;

        if( count > THRESHOLD ) {
          signals2++;
          totalActivity2 += count;
        }
      }
      wf->ffrUnloadSignals();
      wf->ffrResetSignalList();
      old_i = i+1;
    }
  }

  UInt64 histogram[ TOTAL_BUCKETS ];             // Histogram of the number of signals in each bucket.
  fsdbVarIdcode bucket_signals[ TOTAL_BUCKETS ]; // Representative FSDB id (signal name) for each bucket.
  UInt64 bucket_minActivity[ TOTAL_BUCKETS ];    // Minimum activity for a particular bucket.
  UInt64 bucket_maxActivity[ TOTAL_BUCKETS ];    // Maximum activity for a particular bucket.
  for(int i=0; i<TOTAL_BUCKETS; i++) {
    histogram[i] = 0;
    bucket_signals[i] = 0;
    bucket_maxActivity[i] = 0;
    bucket_minActivity[i] = maxActivity;
  }
  // Initialize activity levels for reserved buckets.
  bucket_maxActivity[0] = 1;
  bucket_minActivity[0] = 1;
  bucket_maxActivity[TOTAL_BUCKETS-1] = maxActivity;
  bucket_minActivity[TOTAL_BUCKETS-1] = maxActivity;

  for(fsdbVarIdcode i=1; i<=maxId; i++) {
    
    UInt64 activity = activityMap[i];

    int index = 0;
    // Reserved buckets for activity==1,maxActivity.
    if (activity==1) {
      index = 0;
    } else if (activity==maxActivity) {
      index = TOTAL_BUCKETS-1;
    } else {
      // Not a reserved activity level; compute which bucket this falls into.
      index = ((UNRESERVED_BUCKETS * activity) / (maxActivity+1)) + (LOW_RESERVED_BUCKETS);
    }

    histogram[index]++;

    // The representative signal name for this particular bucket is
    // the first encountered, highest activity signal.
    if( activity > bucket_maxActivity[index] or bucket_signals[index]==0 ) {
      bucket_signals[index] = i;
      bucket_maxActivity[index] = activity;
    }

    if( activity < bucket_minActivity[index] ) {
      bucket_minActivity[index] = activity;
    }
  }

  printf("%23s : %10s : %s\n",
         "Value Change Range", "# Signals", "Fastest Signal Name");
  printf("------------------------------------------------------------\n");

  SInt64 totalSignalsChk = 0;
  for(int i = 0; i<TOTAL_BUCKETS; ++i) {
    UInt64 minBucketActivity = bucket_minActivity[i];
    UInt64 maxBucketActivity = bucket_maxActivity[i];
    UInt64 signalsInBucket = histogram[i];

    // Only output an unreserved bucket if there's data.
    if (i<LOW_RESERVED_BUCKETS or i==(TOTAL_BUCKETS-1) or signalsInBucket>0) {
      printf(Formatw64 (10,d) " - " Formatw64 (10,d) " : " Formatw64 (10,d),
             minBucketActivity, maxBucketActivity, signalsInBucket );

      fsdbVarIdcode bucket_signal = bucket_signals[i];
      const char * bucket_signal_name = symbolTable[ bucket_signal ];
      if( bucket_signal_name ) {
        printf(" : %s", bucket_signal_name);
      }
      printf("\n");
    }
    totalSignalsChk += signalsInBucket;
  }

  double b = (double)totalActivity * 100 / ((double)maxActivity * (double)signals);
  double b2 = (double)totalActivity2 * 100 / ((double)maxActivity * (double)signals2);

  printf("------------------------------------------------------------\n");

  printf( "                  All Changes :  Changes>%d\n", THRESHOLD);
  printf( "Signals:          " Formatw64 (10,d) "  : " Formatw64(10,d) "\n", signals, signals2);
  printf( "Changes:          " Formatw64 (10,d) "  : " Formatw64(10,d) "\n", totalActivity, totalActivity2 );
  printf("Activity wrt Max: %10f%% : %10f%%\n", b, b2);

  // Check that we processed all the signals contained in the FSDB;
  // just make an informational message for now.
  if (maxId!=signals or totalSignalsChk!=maxId) {
    printf("Signal-count consistency error.\n");
    printf("MaxID: %d, Processed: " Format64(d) " Histogram: " Format64(d) "\n", maxId, signals, totalSignalsChk);
  }


  // Free our string memory.
  for(fsdbVarIdcode i=1; i<=maxId; i++) {
    char * signal_name = symbolTable[ i ];
    StringUtil::free(signal_name);
  }
} 


