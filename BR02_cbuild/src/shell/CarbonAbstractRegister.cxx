/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/carbon_shelltypes.h"
#include "shell/CarbonAbstractRegister.h"
#include "util/UtLicense.h"
#include "util/UtLicenseMsg.h"
#include "util/UtIOStream.h"
#include "util/DynBitVector.h"
#include "util/CarbonAssert.h"
#include "shell/carbon_shelltypes.h"
#include "shell/ShellGlobal.h"

struct CarbonAbstractRegister
{
  CARBONMEM_OVERRIDES

  CarbonAbstractRegister(CarbonUInt32 srcStart, CarbonUInt32 numBits, CarbonUInt32 dstStart)
    : mSrcStart(srcStart)
      , mNumBits(numBits)
      , mDstStart(dstStart)
  {
  }

  ~CarbonAbstractRegister()
  {
  }

  void xfer(const CarbonUInt32* src, CarbonUInt32* dst)
  {
    DynBitVector::anytoany(src, dst, mSrcStart, mNumBits, mDstStart);
  }
  
  CarbonUInt32 mSrcStart;
  CarbonUInt32 mNumBits;
  CarbonUInt32 mDstStart;
};


CarbonAbstractRegister* carbonAbstractRegisterCreate(CarbonUInt32 srcStart, CarbonUInt32 numBits, CarbonUInt32 dstStart)
{
  return new CarbonAbstractRegister(srcStart, numBits, dstStart);
}

void carbonAbstractRegisterDestroy(CarbonAbstractRegister* ctx)
{
  delete ctx;
}

void CarbonAbstractRegisterXfer(CarbonAbstractRegisterID ctx, const CarbonUInt32* src, CarbonUInt32* dst)
{
  ctx->xfer(src, dst);
}

