// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "shell/CarbonDBRead.h"
#include "util/ShellMsgContext.h"
#include "util/Zstream.h"
#include "util/UtArray.h"
#include "util/DynBitVector.h"
#include "util/StringAtom.h"
#include "util/UtIndent.h"
#include "util/UtIO.h"
#include "util/CarbonTypeUtil.h"
#include "iodb/ScheduleFactory.h"
#include "iodb/IODBRuntime.h"
#include "iodb/IODBUserTypes.h"
#include "symtab/STSymbolTable.h"
#include "shell/ShellSymNodeIdent.h"
#include "hdl/HdlVerilogPath.h"
#include "hdl/HdlId.h"

class ShellDataBOM::DeadCheckWalk : public CarbonExprWalker
{
public: 
  CARBONMEM_OVERRIDES
  // Assume dead at first. If any part of the expression is live the
  // expression is not dead
  DeadCheckWalk() : mIsDead(true), mHasIdents(false)
  {}
  
  virtual ~DeadCheckWalk() {}
  
  virtual bool preVisitIdent(CarbonIdent* ident) {
    mHasIdents = true;

    DynBitVector useMask;
    const STAliasedLeafNode* srcLeaf = ident->getNode(&useMask);
    const ShellDataBOM* bom = ShellSymTabBOM::getLeafBOM(srcLeaf);
    CE_ASSERT(bom, ident);

    mIsDead = bom->isDeadNet();

    return ! mIsDead;
  }

  bool isDead() const {
    // Only dead if actually visited an identifier, otherwise
    // constant!
    return mIsDead && mHasIdents;
  }
  
private:
  bool mIsDead;
  bool mHasIdents;
};

ShellDataBOM::ShellDataBOM()
{
  init();
}

ShellDataBOM::~ShellDataBOM()
{
}

void ShellDataBOM::carbondbPrint() const
{
  fprintf(stdout, "  NetFlags: ");
  carbonNetPrintFlags(mNetFlags);
  CbuildShellDB::printTagTypeIndex(2, mTagTypeIndex);
  printStorage(2);
  // signature is already printed by carbondb, so just print the
  // pointer
  fprintf(stdout, "  Signature Pointer: " FormatPtr "\n", mSignature);
  // mShellData has no interesting data for carbondb. 
  // It contains a net and net assoc for wave dumping.
  // It is only useful during an actual simulation. So, skipping it
  // here
  // mExpr will be printed out by carbondb when -printexprs is
  // specified, so just print the pointer here.
  fprintf(stdout, "  Expression Pointer: " FormatPtr "\n", mExpr);
  fprintf(stdout, "  NodeFlags: %#x", mNodeFlags);
  printNodeFlags();
}

void ShellDataBOM::print() const
{
  fprintf(stdout, "%#x, %#x, %#x, %p, %p, %p",
          mNetFlags,
          mTagTypeIndex,
          mStorageValue.mImmediateStorage,
          mSignature,
          mShellData,
          mExpr);

  printNodeFlags();
}

void ShellDataBOM::printStorage(UInt32 indent) const
{
  UtString indentStr;
  UtIndent indenter(&indentStr);
  indenter.tab(indent);

  UtIO::cout() << indentStr << "Storage: ";

  // This will be a little redundant. We already printed out the tag
  // type, but I want to be as clear as possible. So, here the value
  // or offset will be printed plus the tag type in parens.
  int typeTag = CbuildShellDB::getTypeTag(mTagTypeIndex);
  if (typeTag == CbuildShellDB::eConstValWordId)
    UtIO::cout() << mStorageValue.mImmediateStorage << " (constant word)";
  else if (typeTag == CbuildShellDB::eConstValId)
  {
    // paranoia. This shouldn't happen, but no reason to crash.
    if (mStorageValue.mLargeStorage) {
      mStorageValue.mLargeStorage->printHex();
      UtIO::cout() << " (constant DynBV)";
    }
    else
      UtIO::cout() << "NULL";
  }
  else
    UtIO::cout() << mStorageValue.mOffsetToStorage << " (offset)";
  UtIO::cout() << UtIO::endl;
}

// This is a little ugly due to the prefixed comma.
void ShellDataBOM::printNodeFlags() const
{
  if (isDepositable()) {
    fprintf(stdout, ", depositable");
  }
  if (needsDebugSched()) {
    fprintf(stdout, ", debugsched");
  }
  if (isInvalidWaveNet()) {
    fprintf(stdout, ", invalidwave");
  }
  if (isComboRun()) {
    fprintf(stdout, ", comborun");
  }
  if (isForceSubordinate()) {
    fprintf(stdout, ", forcesubord");
  }
  if (isClock()) {
    fprintf(stdout, ", clock");
  }
  if (isObservable()) {
    fprintf(stdout, ", observable");
  }
  if (isStateOutput()) {
    fprintf(stdout, ", stateout");
  }
  
  fprintf(stdout, "\n");
}

bool ShellDataBOM::isExpression() const
{
  return (getTypeTag() == CbuildShellDB::eExprId) && (mExpr != NULL);
}

UInt32 ShellDataBOM::getTypeIndex() const
{
  return CbuildShellDB::getTypeIndex(mTagTypeIndex);
}

int ShellDataBOM::getTypeTag() const
{
  return CbuildShellDB::getTypeTag(mTagTypeIndex);
}

const IODBIntrinsic*
ShellDataBOM::getIntrinsic() const
{
  return mIntrinsic;
}

CbuildShellDB::NodeFlags ShellDataBOM::getNodeFlags() const
{
  return mNodeFlags;
}

SInt32 ShellDataBOM::getStorageOffset() const
{
  return mStorageValue.mOffsetToStorage;
}

UInt32 ShellDataBOM::getStorageValueWord() const
{
  return mStorageValue.mImmediateStorage;
}

const DynBitVector* ShellDataBOM::getStorageValue() const
{
  return mStorageValue.mLargeStorage;
}

SCHSignature* ShellDataBOM::getSCHSignature()
{
  const ShellDataBOM* me = const_cast<const ShellDataBOM*>(this);
  return const_cast<SCHSignature*>(me->getSCHSignature());
}

const SCHSignature* ShellDataBOM::getSCHSignature() const
{
  return mSignature;
}

/*
  This function is potentially n^2 where n is the number of
  expressions in the expression graph. This only gets called when
  setting up a waveform hierarchy. So, the impact is only felt once
  and that is only if expressions are nested. We currently flatten all
  expressions in cbuild so this is not yet an issue.
*/
bool ShellDataBOM::isDeadNet() const
{
  bool isExpr = isExpression();
  bool isDead = (mSignature == NULL) && ! isExpr;
  if (! isDead && isExpr)
  {
    // Check if the expression is dead
    DeadCheckWalk walk;
    walk.visitExpr(mExpr);
    isDead = walk.isDead();
  }
  return isDead;
}

void ShellDataBOM::setNetFlags(NetFlags netFlags)
{
  mNetFlags = netFlags;
}
  
void ShellDataBOM::setTagTypeIndex(UInt32 tagTypeIndex)
{
  mTagTypeIndex = tagTypeIndex;
}
  
void
ShellDataBOM::setIntrinsic( const IODBIntrinsic *intrinsic )
{
  mIntrinsic = intrinsic;
}
  
void ShellDataBOM::setNodeFlags(CbuildShellDB::NodeFlags nodeFlags)
{
  mNodeFlags = nodeFlags;
}

void ShellDataBOM::setStorageValue(const CbuildShellDB::StorageValue& storageInfo)
{
  mStorageValue = storageInfo;
}

void ShellDataBOM::setSCHSignature(SCHSignature* signature)
{
  mSignature = signature;
}

ShellData* ShellDataBOM::getShellData()
{
  const ShellDataBOM* me = const_cast<const ShellDataBOM*>(this);
  return const_cast<ShellData*>(me->getShellData());
}

const ShellData* ShellDataBOM::getShellData() const
{
  return mShellData;
}

void ShellDataBOM::setShellData(ShellData* shlData)
{
  mShellData = shlData;
}

void ShellDataBOM::setExpr(CarbonExpr* expr)
{
  mExpr = expr;
}

const CarbonExpr* ShellDataBOM::getExpr() const
{
  return mExpr;
}

CarbonExpr* ShellDataBOM::getExpr()
{
  const ShellDataBOM* me = const_cast<const ShellDataBOM*>(this);
  return const_cast<CarbonExpr*>(me->getExpr());
}

void ShellDataBOM::setUserType(const UserType* ut)
{
  mUserType = ut;
}

const UserType* ShellDataBOM::getUserType() const
{
  return mUserType;
}

void ShellDataBOM::init()
{
  mNetFlags = NetFlags(0);
  mTagTypeIndex = 0;
  mIntrinsic = NULL;
  mStorageValue.mOffsetToStorage = -1;
  mSignature = NULL;
  mShellData = NULL;
  mExpr = NULL;
  mUserType = NULL;
  mNodeFlags = CbuildShellDB::eNoNodeFlags;
}

// ShellSymTabBOM implementation

ShellSymTabBOM::ShellSymTabBOM()
{
  mScheduleFactory = NULL;
  mUserTypeFactory = NULL;
  mExprDBContext = NULL;
  mExprFactory = new ESFactory;
  mCapabilities = Capabilities(0);
}

ShellSymTabBOM::~ShellSymTabBOM()
{
  delete mExprDBContext;
  delete mExprFactory;
}

STFieldBOM::Data ShellSymTabBOM::allocLeafData()
{
  return new ShellDataBOM;
}

STFieldBOM::Data ShellSymTabBOM::allocBranchData()
{
  return new ShellBranchDataBOM;
}

void ShellSymTabBOM::freeLeafData(const STAliasedLeafNode* leaf, Data* bomdata)
{
  ShellDataBOM* data = getLeafBOM(leaf);
  *bomdata = NULL;
  delete data;
}

void ShellSymTabBOM::freeBranchData(const STBranchNode* branch, Data* bomdata)
{
  ShellBranchDataBOM* data = getBranchBOM(branch);
  *bomdata = NULL;
  delete data;
}

void ShellSymTabBOM::writeLeafData(const STAliasedLeafNode* leaf, 
                                   ZostreamDB& /*out*/) const
{
  ST_ASSERT(0, leaf);
}

void ShellSymTabBOM::writeBranchData(const STBranchNode* branch, 
                                     ZostreamDB& /*out*/,
                                     AtomicCache* /*atomicCache*/) const
{
  ST_ASSERT(0, branch);
}
  
void ShellSymTabBOM::printLeaf(const STAliasedLeafNode* leaf) const
{
  const ShellDataBOM* data = getLeafBOM(leaf);
  data->print();
}

void ShellSymTabBOM::printBranch(const STBranchNode*) const
{
  fprintf(stdout, "0x0\n");
}

STFieldBOM::ReadStatus 
ShellSymTabBOM::readLeafData(STAliasedLeafNode* leaf, ZistreamDB& in, 
                             MsgContext* msg)
{
  UInt32 netFlags;
  UInt32 tagtypeIndex;
  UInt32 sigIndex;
  IODBIntrinsic *intrinsic;
  UInt32 exprExist;
  UInt32 exprFactoryIndex;
  CbuildShellDB::NodeFlags nodeFlags;
  CbuildShellDB::StorageValue stVal;
  UserType* ut = NULL;

  // Test for the user type capability. If it isn't there, pass in a NULL
  // pointer to read so that it isn't read in.
  ReadStatus stat = eReadOK;
  CbuildShellDB::read(in, &netFlags, &sigIndex, &tagtypeIndex, 
                      &intrinsic, &stVal, &exprExist, 
                      &exprFactoryIndex, &nodeFlags,
                      (hasUserTypeV1Capability() ? &ut : NULL));

  if (! in) 
  {
    stat = eReadFileError;
    msg->STFileError(in.getError());
  }
  else {
    ShellDataBOM* data = getLeafBOM(leaf);
    data->setNetFlags(NetFlags(netFlags));
    data->setTagTypeIndex(tagtypeIndex);
    data->setIntrinsic(intrinsic);
    data->setStorageValue(stVal);
    data->setNodeFlags(nodeFlags);
    CarbonExpr* descExpr = NULL;
    if (exprExist)
      descExpr = mExprDBContext->getExpr(exprFactoryIndex);
    data->setExpr(descExpr);
    data->setUserType(ut);
  }
  return stat;
}

STFieldBOM::ReadStatus 
ShellSymTabBOM::readBranchData(STBranchNode* branch, ZistreamDB& in, 
                               MsgContext* msg)
{
  // Test for the capability, if it isn't there we leave it alone
  // which has empty flags and module name.
  if (!hasBranchBOMV1Capability()) {
    return eReadOK;
  }

  // Read the hierarchical flags and module name (or NULL)
  UInt32 hierFlags;
  ReadStatus stat = eReadOK;
  StringAtom* moduleName = NULL;
  UserType* ut = NULL;
  // Test for the user type capability. If it isn't there, pass in a NULL
  // pointer to readBranch so that it isn't read in.
  CbuildShellDB::readBranch(in, &hierFlags, &moduleName,
                            (hasUserTypeV1Capability() ? &ut : NULL));

  if (!in) {
    stat = eReadFileError;
    msg->STFileError(in.getError());
  }

  // Add the data to the bom
  ShellBranchDataBOM* data = getBranchBOM(branch);
  data->setHierFlags(HierFlags(hierFlags));
  data->setModuleName(moduleName);
  data->setUserType(ut);
  return eReadOK;
} // ShellSymTabBOM::readBranchData

void ShellSymTabBOM::putScheduleFactory(SCHScheduleFactory* sf)
{
  mScheduleFactory = sf;
}

void ShellSymTabBOM::putUserTypeFactory(UserTypeFactory* utf)
{
  mUserTypeFactory = utf;
}

bool ShellSymTabBOM::readData(ZistreamDB& in, IODBRuntime* iodb, MsgContext* mc)
{
  STSymbolTable* symTab = iodb->getDesignSymbolTable();
  INFO_ASSERT(mExprDBContext == NULL, "Function called more than once");
  mExprDBContext = new SymNodeDBExprContext(iodb, symTab, iodb->getBVFactory());
  
  ReadStatus symReadStat = symTab->readDB(in, mc);
  if (symReadStat != eReadOK) {
    mc->SHLCorruptDBRead(in.getError());
    return false;
  }
  
  INFO_ASSERT(mScheduleFactory, "Schedule factory not set");
  if (!mScheduleFactory->readDatabase(in)) {
    mc->SHLCorruptDBRead(in.getError());
    return false;
  }
  
  if (! in.expect("netSignatures"))
  {
    mc->SHLCorruptDBRead("Expected netSignatures.");
    return false;
  }

  // We expect that all live nodes that were written to have signature
  // fields, including aliases.
  for (STAliasedLeafNode* leaf; in.readPointer(&leaf) && (leaf != NULL);)
  {
    SCHSignature* sig;
    if (in.readPointer(&sig))
    {
      ShellDataBOM* data = getLeafBOM(leaf);
      data->setSCHSignature(sig);
    }
  }
  
  return ! in.fail();
}

void ShellSymTabBOM::writeBOMSignature(ZostreamDB& /*out*/) const
{
  INFO_ASSERT(0, "Runtime symtab write not implemented");
}

STFieldBOM::ReadStatus 
ShellSymTabBOM::readBOMSignature(ZistreamDB& in, UtString* errMsg)
{
  bool compatible = CbuildShellDB::readSignature(in, errMsg);
  if (! compatible) {
    if (! in)
      return eReadFileError;
    else
      return eReadIncompatible;
  }
  return eReadOK;
}

void ShellSymTabBOM::preFieldWrite(ZostreamDB&)
{}

STFieldBOM::ReadStatus 
ShellSymTabBOM::preFieldRead(ZistreamDB& in)
{
  ReadStatus stat = eReadOK;
  mExprDBContext->readExprs(in, mExprFactory);
  if (in.fail())
    stat = eReadFileError;
  else if (hasUserTypeV1Capability())
  {
    mUserTypeFactory->restore(in);
    if (in.fail())
      stat = eReadFileError;
  }
  return stat;
}

static void sAppendVectorRange(UtString* buf,
                               const ShellDataBOM* sdBOM)
{
  const ConstantRange* range = NULL;
  if (sdBOM)
  {
    const IODBIntrinsic* intrinsic = sdBOM->getIntrinsic();
    // this could potentially be called from getCarbonNet which
    // asserts that the intrinsic is already there. In any case, be
    // safe here - do not assert, in case we need to print a name in
    // an error message
    if (intrinsic && (intrinsic->getType() == IODBIntrinsic::eVector))
    {
      range = intrinsic->getVecRange();
      HdlVerilogPath pather;
      HdlId id(range->getMsb(), range->getLsb());
      pather.appendVectorInfo(buf, &id);
    }
  }
}

static void sComposeHierPath(UtString* buf, 
                             const STSymbolTableNode* node, 
                             const ShellDataBOM* sdBOM,
                             bool includeVectorRange)
{
  if (node)
  {
    node->verilogCompose(buf);
    if (includeVectorRange)
      sAppendVectorRange(buf, sdBOM);
  }
}

static void sComposeHierPathLeaf(UtString* buf,
                                 const STSymbolTableNode* node, 
                                 const ShellDataBOM* sdBOM,
                                 bool includeVectorRange)
{
  if (node)
  {
    node->verilogComposeLeaf(buf);
    if (includeVectorRange)
      sAppendVectorRange(buf, sdBOM);
  }
}

void ShellSymTabBOM::composeName(const STSymbolTableNode* node, UtString* buf, 
                                 bool noBackPointer, 
                                 bool includeVectorRange)
{
  bool useHierPath = true;
  const ShellDataBOM* sdBOM = NULL;
  if (! noBackPointer && node && node->castLeaf())
  {
    sdBOM = getLeafBOM(node);
    
    buf->clear();
    if (sdBOM)
    {
      const CarbonExpr* expr = sdBOM->getExpr();
      if (expr)
      {
        const CarbonIdent* exprIdent = expr->castIdent();
        if (exprIdent)
        {
          exprIdent->compose(buf);
          useHierPath = false;
        }
      }
    }
  }

  if (useHierPath)
    sComposeHierPath(buf, node, sdBOM, includeVectorRange);
}

void ShellSymTabBOM::composeLeafName(const STSymbolTableNode* node, 
                                     UtString* buf, 
                                     bool includeVectorRange)
{
  bool useHierPath = true;
  const ShellDataBOM* sdBOM = NULL;
  if (node && node->castLeaf())
  {
    sdBOM = getLeafBOM(node);
    
    buf->clear();
    if (sdBOM)
    {
      const CarbonExpr* expr = sdBOM->getExpr();
      if (expr)
      {
        const CarbonIdent* exprIdent = expr->castIdent();
        if (exprIdent)
        {
          exprIdent->composeLeaf(buf);
          useHierPath = false;
        }
      }
    }
  }

  if (useHierPath)
    sComposeHierPathLeaf(buf, node, sdBOM, includeVectorRange);
}


const CarbonExpr* ShellSymTabBOM::getBackPointer(const STSymbolTableNode* node)
{
  const CarbonExpr* backPointer = NULL;
  if (node->castLeaf())
  {
    const ShellDataBOM* sdBOM = getLeafBOM(node);
    if (sdBOM)
    {
      const CarbonExpr* expr = sdBOM->getExpr();
      if (expr)
      {
        const CarbonIdent* exprIdent = expr->castIdent();
        if (exprIdent)
        {
          const SymTabIdent* symTabIdent = exprIdent->castSymTabIdent();
          if (symTabIdent)
          {
            const ShellSymNodeIdentBP* shellIdent = symTabIdent->castShellSymNodeIdentBP();
            if (shellIdent)
              backPointer = shellIdent->getBackPointer();
          }
        }
      }
    }
  }
  return backPointer;
}

ShellBranchDataBOM::ShellBranchDataBOM() :
  mHierFlags(HierFlags(0)), mModuleName(NULL),
  mUserType(NULL)
{
}

ShellBranchDataBOM::~ShellBranchDataBOM()
{
}

void ShellBranchDataBOM::print() const
{
  fprintf(stdout, "%x", mHierFlags);
  if (mModuleName != NULL) {
    fprintf(stdout, ", name=%s\n", mModuleName->str());
  }
  fprintf(stdout, "\n");
}

