// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/carbon_system.h"
#include "shell/ReplaySystemSim.h"

CarbonSys* carbonGetSystem(int* firstCall)
{
  bool firstCallBool;
  CarbonSys* sys = CarbonSystemSim::singleton(&firstCallBool);
  if (firstCall != NULL) {
    if (firstCallBool) {
      *firstCall = 1;
    } else {
      *firstCall = 0;
    }
  }
  return sys;
}

void carbonSystemPutName(CarbonSys* sys, const char* name)
{
  sys->putSystemName(name);
}

CarbonSC* carbonSystemAddComponent(CarbonSys* sys, const char* componentName,
                                   CarbonObjectID** model)
{
  return sys->addComponent(componentName, model, NULL);
}

void carbonSystemUpdateGUI(CarbonSys* sys)
{
  sys->updateSystem(eReplayEventUpdate);
}

int carbonSystemNumComponents(CarbonSys* sys)
{
  return sys->numComponents();
}

int carbonSystemNumReplayableComponents(CarbonSys* sys)
{
  return sys->numReplayableComponents();
}

int carbonSystemComponentReplayable(CarbonSC* comp)
{
  if (comp->getReplayInfo() != NULL) {
    return 1;
  } else {
    return 0;
  }
}

CarbonSystemReadCmdlineStatus
carbonSystemReadFromGUI(CarbonSys* sys, int onlyIfChanged)
{
  return sys->readCmdline(onlyIfChanged == 1);
}

const char* carbonSystemGetErrmsg(CarbonSys* sys)
{
  return sys->getErrmsg();
}

void carbonSystemSetVerboseReplay(CarbonSys* sys, int verbose)
{
  sys->setVerboseReplay(verbose);
}

void carbonSystemShutdown(CarbonSys* sys)
{
  sys->updateSystem(eReplayEventExit);
}

void carbonSystemPutCycleCountCB(CarbonSys* sys,
                                 CarbonSystemCycleCountFn fn,
                                 void *clientData)
{
  sys->putCycleCountCB(fn, clientData);
}

void carbonSystemClearCycleCountCB(CarbonSys* sys) {
  sys->clearCycleCountCB();
}

CarbonSC* carbonSystemFindComponent(CarbonSys* sys, const char* componentName)
{
  return sys->findComponent(componentName);
}

CarbonObjectID* carbonSystemComponentGetModel(CarbonSC* comp)
{
  return comp->getModel();
}

CarbonSCIter* carbonSystemLoopComponents(CarbonSys* sys)
{
  return new CarbonSystemComponentIter(sys->loopComponents());
}

CarbonSystemComponent* carbonSystemComponentNext(CarbonSCIter* iter)
{
  return iter->getNext();
}

void carbonSystemFreeComponentIter(CarbonSCIter* iter)
{
  delete iter;
}

const char* carbonSystemComponentGetName(CarbonSC* comp)
{
  return comp->getName();
}

void carbonSystemPutSimTimeCB(CarbonSys* sys,
                                 CarbonSystemSimTimeFn fn,
                                 void *clientData)
{
  sys->putSimTimeCB(fn, clientData);
}

void carbonSystemClearSimTimeCB(CarbonSys* sys) {
  sys->clearSimTimeCB();
}

void carbonSystemPutSimTimeUnits(CarbonSys* rss, const char* units) {
  rss->putSimTimeUnits(units);
}

void* carbonSystemComponentGetUserData(CarbonSC* comp)
{
  return comp->getUserData();
}

void carbonSystemComponentPutUserData(CarbonSC* comp, void* userData)
{
  comp->putUserData(userData);
}

int carbonSystemIsShutdown(CarbonSys* sys)
{
  /*
    Checking whether the starting timestamp is 0 is essentially the
    same thing, and has the side effect that a newly-created system
    that hasn't actually seen any updates is considered to be shut
    down.
  */
  int shutdown = (sys->getSimStartTimestamp() == 0);
  return shutdown;
}
