﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace Carbon.CarbonDB
{
	public class CarbonDB : IDisposable
	{
		private SWIGTYPE_p_CarbonDB db { get; set; }

		public CarbonDB (string dbPath)
		{
			FileInfo fi = new FileInfo (dbPath);
			if (fi.Exists)
				db = carbondbapics.carbonDBCreate (dbPath, 0);
			else
				throw new Exception (string.Format ("Error: CarbonDB: Unable to open file: {0}", fi.FullName));
		}

		public void Dispose ()
		{
			if (db != null) {
				carbondbapics.carbonDBFree (db);
				db = null;
			}
		}

		public CarbonDBNode FindNode (string hierPath)
		{
			var node = carbondbapics.carbonDBFindNode (db, hierPath);
			if (node != null) 
				return new CarbonDBNode (db, node);
			else 
				return null;
		}

		public CarbonDBNode FindChild (CarbonDBNode parent, string childName)
		{
			var node = carbondbapics.carbonDBFindChild (db, parent, childName);
			if (node != null) 
				
				return new CarbonDBNode (db, node);
			else 
				return null;
		}

		public string IdString {
			get {
				return carbondbapics.carbonDBGetIdString (db);
			}
		}

		public string InterfaceName {
			get {
				return carbondbapics.carbonDBGetInterfaceName (db);
			}
		}

		public string GetSoftwareVersion {
			get {
				return carbondbapics.carbonDBGetSoftwareVersion (db);
			}
		}

		public string GetStringAttribute (string attributeName)
		{
			return carbondbapics.carbonDBGetStringAttribute (db, attributeName);
		}

		public string SystemCModuleName {
			get {
				return carbondbapics.carbonDBGetSystemCModuleName (db);
			}
		}

		public string TopLevelModuleName {
			get {
				return carbondbapics.carbonDBGetTopLevelModuleName (db);
			}
		}

		public string Type {
			get {
				return carbondbapics.carbonDBGetType (db);
			}
		}

		public string Version {
			get {
				return carbondbapics.carbonDBGetVersion ();
			}
		}

		public bool IsReplayable {
			get {
				return carbondbapics.carbonDBIsReplayable (db) != 0;
			}
		}

		public bool SupportsOnDemand {
			get {
				return carbondbapics.carbonDBSupportsOnDemand (db) != 0;
			}
		}
 
		public IEnumerable<CarbonDBNode> AsyncDeposits ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopAsyncDeposits (db);

			HandleRef h = SWIGTYPE_p_CarbonDBNodeIter.getCPtr (iter);

			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> AsyncNegResets ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopAsyncNegResets (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> AsyncOutputs ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopAsyncOutputs (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> AsyncPosResets ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopAsyncPosResets (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> Asyncs ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopAsyncs (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> BothEdgeTriggers ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopBothEdgeTriggers (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> ClkTree ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopClkTree (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> Depositable ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopDepositable (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> DesignRoots ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopDesignRoots (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> Matching (string matchString)
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopMatching (db, matchString);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> NegedgeonlyTriggers ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopNegedgeOnlyTriggers (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> NegedgeTriggers ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopNegedgeTriggers (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> Observable ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopObservable (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> PosedgeOnly ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopPosedgeOnlyTriggers (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> PosedgeTriggers ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopPosedgeTriggers (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> PrimaryBidis ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopPrimaryBidis (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> PrimaryClks ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopPrimaryClks (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> PrimaryInputs ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopPrimaryInputs (db);   
			SWIGTYPE_p_CarbonDBNode pnode = null;

			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> PrimaryOutputs ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopPrimaryOutputs (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> PrimaryPorts ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopPrimaryPorts (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> ScDepositable ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopScDepositable (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> ScObservable ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopScObservable (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> TriggersUsedAsData ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopTriggersUsedAsData (db);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

#if false
    public SWIGTYPE_p_CarbonMsgCBDataID carbonDBAddMsgCB(SWIGTYPE_p_CarbonMsgCB cbFun, SWIGTYPE_p_CarbonClientData userData);      
    public int carbonDBGetIntAttribute( string attributeName, SWIGTYPE_p_CarbonUInt32 attributeValue);   
    public void carbonDBRemoveMsgCB(SWIGTYPE_p_p_CarbonMsgCBDataID cbDataPtr);
#endif
	}

	public class CarbonDBNode
	{
		private SWIGTYPE_p_CarbonDBNode node { get; set; }

		private SWIGTYPE_p_CarbonDB db { get; set; }

		public CarbonDBNode (SWIGTYPE_p_CarbonDB d, SWIGTYPE_p_CarbonDBNode n)
		{
			db = d;
			node = n;
		}

		public static implicit operator CarbonDBNode (SWIGTYPE_p_CarbonDBNode n)
		{
			CarbonDBNode dbn = new CarbonDBNode (null, n);
			return dbn;
		}

		public static implicit operator SWIGTYPE_p_CarbonDBNode (CarbonDBNode n)
		{
			return n.node;
		}

		public bool CanBeCarbonNet {
			get {
				return carbondbapics.carbonDBCanBeCarbonNet (db, node) != 0;
			}
		}

		public string ComponentName {
			get {
				return carbondbapics.carbonDBComponentName (db, node);
			}
		}

		public string DeclarationType {
			get {
				return carbondbapics.carbonDBdeclarationType (db, node);
			}
		}

		public int Array2DLeftAddr {
			get {
				return carbondbapics.carbonDBGet2DArrayLeftAddr (db, node);
			}
		}

		public int Array2DRightAddr {
			get {
				return carbondbapics.carbonDBGet2DArrayRightAddr (db, node);
			}
		}

		public int GetArrayDimLeftBound (int dim)
		{
			return carbondbapics.carbonDBGetArrayDimLeftBound (db, node, dim);
		}

		public int GetArrayDimRightBound (int dim)
		{
			return carbondbapics.carbonDBGetArrayDimRightBound (db, node, dim);
		}

		public int ArrayDims {
			get {
				return carbondbapics.carbonDBGetArrayDims (db, node);
			}
		}

		public int ArrayLeftBound {
			get {
				return carbondbapics.carbonDBGetArrayLeftBound (db, node);
			}
		}

		public int ArrayRightBound {
			get {
				return carbondbapics.carbonDBGetArrayRightBound (db, node);
			}
		}

		public int ArrayNumDeclaredDims {
			get {
				return carbondbapics.carbonDBGetArrayNumDeclaredDims (db, node);
			}
		}

		public int BitSize {
			get {
				return carbondbapics.carbonDBGetBitSize (db, node);
			}
		}

		public int Width {
			get {
				return carbondbapics.carbonDBGetWidth (db, node);
			}
		}

		public string IntrinsicType {
			get {
				return carbondbapics.carbonDBIntrinsicType (db, node);
			}
		}

		public bool Is2DArray {
			get {
				return carbondbapics.carbonDBIs2DArray (db, node) != 0;
			}
		}

		public bool IsArray {
			get {
				return carbondbapics.carbonDBIsArray (db, node) != 0;
			}
		}

		public bool IsAsync {
			get {
				return carbondbapics.carbonDBIsAsync (db, node) != 0;
			}
		}

		public bool IsAsyncDeposit {
			get {
				return carbondbapics.carbonDBIsAsyncDeposit (db, node) != 0;
			}
		}

		public bool IsAsyncNegReset {
			get {
				return carbondbapics.carbonDBIsAsyncNegReset (db, node) != 0;
			}
		}

		public bool IsAsyncOutput {
			get {
				return carbondbapics.carbonDBIsAsyncOutput (db, node) != 0;
			}
		}

		public bool IsAsyncPosReset {
			get {
				return carbondbapics.carbonDBIsAsyncPosReset (db, node) != 0;
			}
		}

		public bool IsBidi {
			get {
				return carbondbapics.carbonDBIsBidi (db, node) != 0;
			}
		}

		public bool IsBothEdgeTrigger {
			get {
				return carbondbapics.carbonDBIsBothEdgeTrigger (db, node) != 0;
			}
		}

		public bool IsClk {
			get {
				return carbondbapics.carbonDBIsClk (db, node) != 0;
			}
		}

		public bool IsClkTree {
			get {
				return carbondbapics.carbonDBIsClkTree (db, node) != 0;
			}
		}

		public bool IsConstant {
			get {
				return carbondbapics.carbonDBIsConstant (db, node) != 0;
			}
		}

		public bool IsContainedByComposite {
			get {
				return carbondbapics.carbonDBIsContainedByComposite (db, node) != 0;
			}
		}

		public bool IsDepositable {
			get {
				return carbondbapics.carbonDBIsDepositable (db, node) != 0;
			}
		}

		public bool IsEnum {
			get {
				return carbondbapics.carbonDBIsEnum (db, node) != 0;
			}
		}

		public bool IsForcible {
			get {
				return carbondbapics.carbonDBIsForcible (db, node) != 0;
			}
		}

		public bool IsInput {
			get {
				return carbondbapics.carbonDBIsInput (db, node) != 0;
			}
		}

		public bool IsLiveInput {
			get {
				return carbondbapics.carbonDBIsLiveInput (db, node) != 0;
			}
		}

		public bool IsLiveOutput {
			get {
				return carbondbapics.carbonDBIsLiveOutput (db, node) != 0;
			}
		}

		public bool IsNegedgeTrigger {
			get {
				return carbondbapics.carbonDBIsNegedgeTrigger (db, node) != 0;
			}
		}

		public bool IsObservable {
			get {
				return carbondbapics.carbonDBIsObservable (db, node) != 0;
			}
		}

		public bool IsOnDemandExcluded {
			get {
				return carbondbapics.carbonDBIsOnDemandExcluded (db, node) != 0;
			}
		}

		public bool IsOnDemandIdleDeposit {
			get {
				return carbondbapics.carbonDBIsOnDemandIdleDeposit (db, node) != 0;
			}
		}

		public bool IsOutput {
			get {
				return carbondbapics.carbonDBIsOutput (db, node) != 0;
			}
		}

		public bool IsPosedgeTrigger {
			get {
				return carbondbapics.carbonDBIsPosedgeTrigger (db, node) != 0;
			}
		}

		public bool IsPrimaryBidi {
			get {
				return carbondbapics.carbonDBIsPrimaryBidi (db, node) != 0;
			}
		}

		public bool IsPrimaryInput {
			get {
				return carbondbapics.carbonDBIsPrimaryInput (db, node) != 0;
			}
		}

		public bool IsPrimaryOutput {
			get {
				return carbondbapics.carbonDBIsPrimaryOutput (db, node) != 0;
			}
		}

		public bool IsRangeRequiredInDeclaration {
			get {
				return carbondbapics.carbonDBIsRangeRequiredInDeclaration (db, node) != 0;
			}
		}

		public int LSB {
			get {
				return carbondbapics.carbonDBGetLSB (db, node);
			}
		}

		public int MSB {
			get {
				return carbondbapics.carbonDBGetMSB (db, node);
			}
		}

		public int NumberElemInEnum {
			get {
				return carbondbapics.carbonDBGetNumberElemInEnum (db, node);
			}
		}

		public int NumStructFields {
			get {
				return carbondbapics.carbonDBGetNumStructFields (db, node);
			}
		}

		public string PullMode {
			get {
				return carbondbapics.carbonDBGetPullMode (db, node);
			}
		}

		public int RangeConstraintLeftBound {
			get {
				return carbondbapics.carbonDBGetRangeConstraintLeftBound (db, node);
			}
		}

		public int RangeConstraintRightBound {
			get {
				return carbondbapics.carbonDBGetRangeConstraintRightBound (db, node);
			}
		}

		public bool IsScalar {
			get {
				return carbondbapics.carbonDBIsScalar (db, node) != 0;
			}
		}

		public bool IsScDepositable {
			get {
				return carbondbapics.carbonDBIsScDepositable (db, node) != 0;
			}
		}

		public bool IsScObservable {
			get {
				return carbondbapics.carbonDBIsScObservable (db, node) != 0;
			}
		}

		public bool IsStruct {
			get {
				return carbondbapics.carbonDBIsStruct (db, node) != 0;
			}
		}

		public bool IsTemp {
			get {
				return carbondbapics.carbonDBIsTemp (db, node) != 0;
			}
		}

		public bool IsTied {
			get {
				return carbondbapics.carbonDBIsTied (db, node) != 0;
			}
		}

		public bool IsTriggerUsedAsData {
			get {
				return carbondbapics.carbonDBIsTriggerUsedAsData (db, node) != 0;
			}
		}

		public bool IsTristate {
			get {
				return carbondbapics.carbonDBIsTristate (db, node) != 0;
			}
		}

		public bool IsVector {
			get {
				return carbondbapics.carbonDBIsVector (db, node) != 0;
			}
		}

		public bool IsVisible {
			get {
				return carbondbapics.carbonDBIsVisible (db, node) != 0;
			}
		}

		public string SourceFile {
			get {
				return carbondbapics.carbonDBGetSourceFile (db, node);
			}
		}

		public int SourceLine {
			get {
				return carbondbapics.carbonDBGetSourceLine (db, node);
			}
		}

		public string FullName {
			get {
				return carbondbapics.carbonDBNodeGetFullName (db, node);
			}
		}

		public string LeafName {
			get {
				return carbondbapics.carbonDBNodeGetLeafName (db, node);
			}
		}

		public CarbonDBNode Parent {
			get {
				var n = carbondbapics.carbonDBNodeGetParent (db, node);
				if (n != null) 
					return new CarbonDBNode (db, n);
				else
					return null;
			}
		}

		public string SourceLanguage {
			get {
				return carbondbapics.carbonDBSourceLanguage (db, node);
			}
		}

		public string TypeLibraryName {
			get {
				return carbondbapics.carbonDBtypeLibraryName (db, node);
			}
		}

		public string TypePackageName {
			get {
				return carbondbapics.carbonDBtypePackageName (db, node);
			}
		}

		public CarbonDBNode GetStructFieldByName (string name)
		{
			var n = carbondbapics.carbonDBGetStructFieldByName (db, node, name);
			if (n != null)
				return new CarbonDBNode (db, n);
			else
				return null;
		}
		// public SWIGTYPE_p_CarbonNetID carbonDBGetCarbonNet(CarbonDBNode node);
		// public CarbonDBNode carbonDBGetArrayElement(CarbonDBNode node, SWIGTYPE_p_int indices, int dims);
		public string GetEnumElem (int index)
		{
			return carbondbapics.carbonDBGetEnumElem (db, node, index);
		}

		public IEnumerable<CarbonDBNode> Aliases ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopAliases (db, node);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> AsyncFanin ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopAsyncFanin (db, node);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}

		public IEnumerable<CarbonDBNode> Children ()
		{
			SWIGTYPE_p_CarbonDBNodeIter iter = carbondbapics.carbonDBLoopChildren (db, node);
			SWIGTYPE_p_CarbonDBNode pnode = null;
			while ((pnode = carbondbapics.carbonDBNodeIterNext(iter)) != null) {
				yield return new CarbonDBNode(db, pnode);
			}
			carbondbapics.carbonDBFreeNodeIter (iter);
		}
	}
}

