// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/OnDemandCachedExamines.h"
#include "shell/ShellNetOnDemand.h"

OnDemandCachedExamines::OnDemandCachedExamines()
{
}

OnDemandCachedExamines::~OnDemandCachedExamines()
{
  for (ExamineMap::iterator iter = mExamineMap.begin(); iter != mExamineMap.end(); ++iter) {
    delete iter->second;
  }
}

bool OnDemandCachedExamines::lookup(const ShellNetOnDemand *net, CarbonStatus *status, UInt32 *val, UInt32 *drive,
                                    ShellNet::ExamineMode mode, Examine **examine_ptr)
{
  // See if the net is in the map
  ExamineMap::iterator iter = mExamineMap.find(net);
  bool success = false;
  Examine *examine;

  // Create a new examine if there was none for this net
  if (iter == mExamineMap.end()) {
    examine = new Examine(net->getNumUInt32s());
    mExamineMap[net] = examine;
  } else {
    examine = iter->second;
    success = examine->tryCopy(status, val, drive, mode);
  }

  *examine_ptr = examine;
  return success;
}

OnDemandCachedExamines::Examine::Examine(UInt32 num_uint32)
  : mSize(num_uint32 * sizeof(UInt32)),
    mValueValid(false),
    mDriveValid(false)
{
  mValue = CARBON_ALLOC_VEC(UInt32, num_uint32);
  mDrive = CARBON_ALLOC_VEC(UInt32, num_uint32);
}

OnDemandCachedExamines::Examine::~Examine()
{
  CARBON_FREE_VEC(mValue, UInt32, mSize / sizeof(UInt32));
  CARBON_FREE_VEC(mDrive, UInt32, mSize / sizeof(UInt32));
}

bool OnDemandCachedExamines::Examine::tryCopy(CarbonStatus *status, UInt32 *val, UInt32 *drive, ShellNet::ExamineMode mode)
{
  // Mode needs to match
  if (mode != mMode)
    return false;

  // If value was requested, and we have one, copy it, else fail
  if (val) {
    if (mValueValid) {
      memcpy(val, mValue, mSize);
    } else {
      return false;
    }
  }

  // If drive was requested, and we have one, copy it, else fail
  if (drive) {
    if (mDriveValid) {
      memcpy(drive, mDrive, mSize);
    } else {
      return false;
    }
  }

  // If we got here, we succeeded.  Copy the status
  *status = mStatus;
  return true;
}

void OnDemandCachedExamines::Examine::save(CarbonStatus status, UInt32 *val, UInt32 *drive, ShellNet::ExamineMode mode)
{
  mStatus = status;
  mMode = mode;

  // Record val/drive, and whether they're valid
  if (val) {
    mValueValid = true;
    memcpy(mValue, val, mSize);
  } else {
    mValueValid = false;
  }
  if (drive) {
    mDriveValid = true;
    memcpy(mDrive, drive, mSize);
  } else {
    mDriveValid = false;
  }
}
