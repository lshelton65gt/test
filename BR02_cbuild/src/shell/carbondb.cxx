// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "iodb/IODBRuntime.h"
#include "iodb/IODBUserTypes.h"
#include "util/ArgProc.h"
#include "hdl/HdlVerilogPath.h"
#include "hdl/HdlId.h"
#include "util/AtomicCache.h"
#include "symtab/STSymbolTableNode.h"
#include "util/SourceLocator.h"
#include "util/ArgProc.h"
#include "util/ShellMsgContext.h"
#include "iodb/ScheduleFactory.h"
#include "shell/CarbonDBRead.h"
#include "iodb/IODBTypes.h"
#include "util/UtIOStream.h"
#include "exprsynth/ExprFactory.h"
#include "schedule/Event.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Signature.h"
#include "symtab/STAliasedLeafNode.h"
#include "util/CarbonTypes.h"
#include "util/CarbonTypeUtil.h"
#include "util/UtLicense.h"
#include "util/UtLicenseMsg.h"
#include "iodb/CGraph.h"
#include "CarbonDatabase.h"

struct LicCB : public UtLicense::MsgCB
{
  CARBONMEM_OVERRIDES

  LicCB(MsgContext* msgContext) : mMsgContext(msgContext)
  {}

  virtual ~LicCB() {}

  virtual void waitingForLicense(const char*) {}
  virtual void queuedLicenseObtained(const char*) {}
  virtual void requeueLicense(const char*) {}
  virtual void relinquishLicense(const char*) {}
  virtual void exitNow(const char* reason)
  {
    mMsgContext->SHLLicenseServerFail(reason);
  }
  
  MsgContext* mMsgContext;
};

static void printAliases(STSymbolTableNode* node, UtString& emptyPrefix)
{
  UtString buf;
  STAliasedLeafNode* a = node->castLeaf();
  if (a != NULL) {
    for (a = a->getAlias(); a != node; a = a->getAlias()) {
      ShellSymTabBOM::composeName(a, &buf, false, false);
      fprintf(stdout, "%s%s\n", emptyPrefix.c_str(), buf.c_str());
    }
  }
}


static bool printNode(STSymbolTableNode* node, bool filterLive, IODBRuntime* db)
{
  return (!filterLive || !db->isUsedAsLiveInput(node) || !db->isUsedAsLiveOutput(node));
}

static void printSet(const char* prefix, IODB::NameSetLoop loop,
                     bool dumpAliases, bool filterLive, IODBRuntime* db)
{
  STSymbolTableNode* node;
  UtString buf, emptyPrefix;
  if (dumpAliases)
    emptyPrefix.append(strlen(prefix) + 2, ' ');
  
  while (loop(&node)) {
    if (printNode(node, filterLive, db)) {
      ShellSymTabBOM::composeName(node, &buf, false, false);
      
      fprintf(stdout, "%s: %s\n", prefix, buf.c_str());
      if (dumpAliases) {
        printAliases(node, emptyPrefix);
      }
    }
  }
}

static void printMap(const char* prefix1, const char* prefix2, IODB::NameSetMapLoop loop,
                     bool dumpAliases, bool filterLive, IODBRuntime* db)
{
  UtString buf, emptyPrefix1, emptyPrefix2;
  if (dumpAliases) {
    emptyPrefix1.append(strlen(prefix1) + 2, ' ');
    emptyPrefix2.append(strlen(prefix2) + 2, ' ');
  }
  
  for (; !loop.atEnd(); ++loop) {
    // Print the node
    STSymbolTableNode* node = loop.getKey();
    if (printNode(node, filterLive, db)) {
      ShellSymTabBOM::composeName(node, &buf, false, false);
      
      fprintf(stdout, "%s: %s\n", prefix1, buf.c_str());
      if (dumpAliases) {
        printAliases(node, emptyPrefix1);
      }
    }

    // Print the fanin
    IODB::NameSet* ns = loop.getValue();
    for (IODB::NameSet::SortedLoop l = ns->loopSorted(); !l.atEnd(); ++l) {
      STSymbolTableNode* subNode = *l;
      if (printNode(subNode, filterLive, db)) {
        ShellSymTabBOM::composeName(subNode, &buf, false, false);
      
        fprintf(stdout, "%s: %s\n", prefix2, buf.c_str());
        if (dumpAliases) {
          printAliases(subNode, emptyPrefix2);
        }
      }
    }
  }
}

static void dump_symtab_node(const STSymbolTableNode* node, IODBRuntime* iodb, 
                             bool dumpAliases, bool dumpBOM, bool printExprs,
                             bool textual)
{
  UtOStream& ut_cout = UtIO::cout();
  UtString buf;
  ShellSymTabBOM::composeName(node, &buf, true, true);
  ut_cout << buf;
  SourceLocator loc;
  if (textual && iodb->findLoc (node, &loc)) {
    loc.compose (&buf);
    ut_cout << " (" << buf << ")";
  }
  ut_cout << "\n";
  if (node->castLeaf())
  {
    const SCHSignature* sig = iodb->getSignature(node);

    if (sig != NULL)
    {
      ut_cout << "    ";
      sig->print();
    }
    const IODBIntrinsic* intrinsic = iodb->getLeafIntrinsic( node->castLeaf() );
    const IODBGenTypeEntry* type = iodb->getType(node);
    if ( intrinsic != NULL )
    {
      ut_cout << "    ";
      intrinsic->print();
    }
    if (type != NULL)
    {
      type->print();
    }
    
    const ShellDataBOM* sdBOM = ShellSymTabBOM::getLeafBOM(node);
    if (sdBOM)
    {
      if (dumpBOM)
      {
        if (textual)
        {
          sdBOM->carbondbPrint();
          // The declaration type code needs to be refactored out of
          // IODBRead and put into CarbonTypeUtil.cxx
          const char* declType = iodb->declarationType(node);
          if (declType)
            ut_cout << "  " << declType << UtIO::endl;
        }
        else
          sdBOM->print();
      }
      
      if (printExprs)
      {
        const CarbonExpr* expr = sdBOM->getExpr();
        if (expr != NULL)
        {
          ut_cout << "EXPRESSION:" << UtIO::endl;
          expr->print();
        }
      }
      if (dumpAliases)
      {
        const STAliasedLeafNode* leaf = node->castLeaf();
        if (leaf)
        {
          const STAliasedLeafNode* next = leaf->getAlias();
          while (next != leaf)
          {
            dump_symtab_node(next, iodb, false, dumpBOM, printExprs, textual);
            next = next->getAlias();
          }
        }
      }
    }
  }
  const UserType* ut = iodb->getUserType(node);
  if (ut != NULL && textual)
  {
    ut_cout << "User Type: ";
    ut->print(&ut_cout);
    UtIO::cout() << UtIO::endl;
  }
}

static void dump_human_readable(IODBRuntime * iodb, bool dumpAliases, bool doBOM, bool printExprs, bool filterLive, bool textual)
{
  UtOStream& ut_cout = UtIO::cout();
  UtString buf;
  STSymbolTable* st = iodb->getDesignSymbolTable();

  for (STSymbolTable::NodeLoopSorted loop = st->getNodeLoopSorted();
       not loop.atEnd(); 
       ++loop)
  {
    const STSymbolTableNode* node = *loop;
    dump_symtab_node(node, iodb, dumpAliases, doBOM, printExprs, textual);
  } // for
  ut_cout << UtIO::endl;
  
  printSet("             Clocks", iodb->loopClocks(), dumpAliases, filterLive, iodb);
  printSet("           Clk Tree", iodb->loopClockTree(), dumpAliases, filterLive, iodb);
  printSet("        Primary Clk", iodb->loopPrimaryClocks(), dumpAliases, filterLive, iodb);
  printSet("          Fast Clks", iodb->loopFastClocks(), dumpAliases, filterLive, iodb);
  printSet("           Observed", iodb->loopObserved(), dumpAliases, filterLive, iodb);
  printSet("            Deposit", iodb->loopDeposit(), dumpAliases, filterLive, iodb);
  printSet("              Force", iodb->loopForced(), dumpAliases, filterLive, iodb);
  printSet("              Input", iodb->loopInputs(), dumpAliases, filterLive, iodb);
  printSet("             Output", iodb->loopOutputs(), dumpAliases, filterLive, iodb);
  printSet("               Bidi", iodb->loopBidis(), dumpAliases, filterLive, iodb);
  printSet("              Async", iodb->loopAsyncs(), dumpAliases, filterLive, iodb);
  printMap("       Async Output",
           "        Async Fanin", iodb->loopAsyncOutputs(), dumpAliases, filterLive, iodb);
  printSet("Async Posedge Reset", iodb->loopAsyncPosResets(), dumpAliases, filterLive, iodb);
  printSet("Async Negedge Reset", iodb->loopAsyncNegResets(), dumpAliases, filterLive, iodb);
  printSet("Posedge Trigger", iodb->loopPosedgeScheduleTriggers(), dumpAliases, filterLive, iodb);
  printSet("Negedge Trigger", iodb->loopNegedgeScheduleTriggers(), dumpAliases, filterLive, iodb);

  // Print all the collapsed clocks
  for (IODB::NameSetMapLoop p = iodb->loopCollapseClocks();
       !p.atEnd(); ++p)
  {
    const STSymbolTableNode* node = p.getKey();
    buf.clear();
    ShellSymTabBOM::composeName(node, &buf, false, true);

    ut_cout << "Collapse(" << buf << "):";
    IODB::NameSet* ns = p.getValue();
    for (IODB::NameSet::SortedLoop q = ns->loopSorted(); !q.atEnd(); ++q)
    {
      node = *q;
      buf.clear();
      ShellSymTabBOM::composeName(node, &buf, false, true);
      ut_cout << " " << buf;
    }
    ut_cout << UtIO::endl;
  }

  // Print all the clocks speeds
  for (IODB::NameIntMapLoop p = iodb->loopClockSpeeds();
       !p.atEnd(); ++p)
  {
    const STSymbolTableNode* node = p.getKey();
    UInt32 speed = p.getValue();
    buf.clear();
    ShellSymTabBOM::composeName(node, &buf, false, true);
    ut_cout << "Speed(" << buf << ") = " << speed << UtIO::endl;
  }

  // Print all the tie nets
  for (IODB::NameValueLoop l = iodb->loopTieNets(); !l.atEnd(); ++l)
  {
    const STSymbolTableNode* node = l.getKey();
    buf.clear();
    ShellSymTabBOM::composeName(node, &buf, false, true);

    ut_cout << "TieNet(" << buf << ") = 0x";

    const DynBitVector* value = l.getValue();
    buf.clear();
    value->format(&buf, eCarbonHex);
    ut_cout << buf << UtIO::endl;
  }

  // Print all the constant nets
  for (IODB::NameValueLoop l = iodb->loopConstNets(); !l.atEnd(); ++l)
  {
    const STSymbolTableNode* node = l.getKey();
    buf.clear();
    ShellSymTabBOM::composeName(node, &buf, false, true);
    ut_cout << "ConstNet(" << buf << ") = 0x";

    const DynBitVector* value = l.getValue();
    buf.clear();
    value->format(&buf, eCarbonHex);
    ut_cout << buf << UtIO::endl;
  }

  // print out enable expressions
  for (IODB::NameExprMapLoop en = iodb->loopEnableExprs();
       ! en.atEnd(); ++en)
  {
    const STSymbolTableNode* node = en.getKey();
    buf.clear();
    ShellSymTabBOM::composeName(node, &buf, false, true);
    ut_cout << "Enable(" << buf << "):" << UtIO::endl << " ";
    const CarbonExpr* expr = en.getValue();
    expr->print(true, 1);
    ut_cout << UtIO::endl;
  }
  

  // The following should be printed out regardless of the textual
  // boolean, but I didn't want to perturb the tests the day before I
  // leave. I will leave this for someone else to do. 
  // - Mark Seneski 7/10/2007
  if (textual)
    // dump the Design ID
    ut_cout << "Design ID: " << iodb->getDesignId() << UtIO::endl;
 
}



static void dump_xml_module(IODBRuntime * iodb, UtOStream * ut_cout)
{
  UtString buf;
  STSymbolTable* st = iodb->getDesignSymbolTable();
  STSymbolTable::NodeLoopSorted loop = st->getNodeLoopSorted();
  const STSymbolTableNode* node = *loop;

  buf.clear();
  ShellSymTabBOM::composeName(node, &buf, false, true);

  *ut_cout << "  <module>" << UtIO::endl;
  *ut_cout << "    " <<  buf << UtIO::endl;
  *ut_cout << "  </module>" << UtIO::endl;
}



static void dump_xml_signal_attributes(IODBRuntime * iodb, UtOStream * ut_cout, const STSymbolTableNode * node, const IODBGenTypeEntry* type)
{
  UtString buf;

  /*
  ** name
  */
  buf.clear();
  ShellSymTabBOM::composeName(node, &buf, false, true);
  *ut_cout << "name=\"" << buf << "\" ";

  /*
  ** clock
  */
  if (iodb->isClock(node))
  {
    *ut_cout << "clock=\"true\" ";
  }

  /*
  ** reset
  */
  if ((iodb->isPosedgeReset(node)) or (iodb->isNegedgeReset(node)))
  {
    *ut_cout << "reset=\"true\" ";
  }

  /*
  ** primary
  */
  if (iodb->isPrimary(node))
  {
    *ut_cout << "primary=\"true\" ";
  }

  /*
  ** input
  */
  if (iodb->isPrimaryInput(node))
  {
    *ut_cout << "input=\"true\" ";
  }

  /*
  ** output
  */
  if (iodb->isPrimaryOutput(node))
  {
    *ut_cout << "output=\"true\" ";
  }

  /*
  ** bidi
  */
  if (iodb->isPrimaryBidirect(node))
  {
    *ut_cout << "bidirect=\"true\" ";
  }

  /*
  ** tristate
  */
  if (type->isTristate())
  {
    *ut_cout << "tristate=\"true\" ";
  }


  /*
  ** sync
  */
#if THIS_LOOKS_LIKE_A_BUG
  /*
  ** This seems to get into an infinite loop.
  */
  if (not iodb->isSync(sig))
  {
    *ut_cout << "sync=\"true\" ";
  }
#endif
}



static void dump_xml_event(UtOStream * ut_cout, const SCHEvent* event)
{
  if (event->isPrimaryInput())
  {
    *ut_cout << "          <event_input>" << UtIO::endl;
    *ut_cout << "          </event_input>" << UtIO::endl;
  }
  else if (event->isPrimaryOutput())
  {
    *ut_cout << "          <event_output>" << UtIO::endl;
    *ut_cout << "          </event_output>" << UtIO::endl;
  }
  else if (event->isConstant())
  {
    *ut_cout << "          <event_constant>" << UtIO::endl;
    *ut_cout << "          </event_constant>" << UtIO::endl;
  }
  else
  {
    *ut_cout << "          <event_signal ";

    /*
    ** edge
    */
    FUNC_ASSERT(event->isClockEvent(), event->print());
    *ut_cout << "edge=\"" << ClockEdgeString( event->getClockEdge() ) << "\" ";

    /*
    ** priority
    */
    if (event->getPriority() > 0)
    {
      *ut_cout << "priority=\"" << event->getPriority() << "\" ";
    }

    // Add the name

    /*
    ** name
    */
    UtString buf;
    event->getClock()->compose(&buf, true);    // include root in name 
    *ut_cout << "name=\"" << buf << "\">"  << UtIO::endl;

    *ut_cout << "          </event_signal>" << UtIO::endl;
  }
}



static void dump_xml_mask(UtOStream * ut_cout, const SCHScheduleMask* mask, const char *type)
{
  *ut_cout << "        <mask type=\"" << type << "\">" << UtIO::endl;
  for (SCHScheduleMask::SortedEvents l = mask->loopEventsSorted(); not l.atEnd(); ++l)
  {
    const SCHEvent * event = *l;
    dump_xml_event(ut_cout, event);
  }
  *ut_cout << "        </mask>" << UtIO::endl;
}



static void dump_xml_schedule(UtOStream * ut_cout, const SCHSignature* sig)
{
  if (sig == NULL)
  {
    return;
  }

  *ut_cout << "      <schedule>" << UtIO::endl;
  dump_xml_mask(ut_cout, sig->getSampleMask(), "sample");
  dump_xml_mask(ut_cout, sig->getTransitionMask(), "transition");
  *ut_cout << "      </schedule>" << UtIO::endl;
}



static void dump_xml_scalar(IODBRuntime * iodb, UtOStream * ut_cout,
                            const STSymbolTableNode * node, const SCHSignature* sig,
                            const IODBGenTypeEntry* type)
{
  ST_ASSERT(type, node);

  *ut_cout << "    <scalar ";
  dump_xml_signal_attributes(iodb, ut_cout, node, type);
  *ut_cout << ">" << UtIO::endl;

  dump_xml_schedule(ut_cout, sig);

  *ut_cout << "    </scalar>" << UtIO::endl;
}



static void dump_xml_vector(IODBRuntime * iodb, UtOStream * ut_cout,
                            const STSymbolTableNode * node, const SCHSignature* sig,
                            const IODBGenTypeEntry* type,
                            const IODBIntrinsic* intrinsic )
{
  ST_ASSERT(type, node);
  ST_ASSERT( intrinsic, node );

  *ut_cout << "    <vector ";
  dump_xml_signal_attributes(iodb, ut_cout, node, type);

  /*
  ** msb & lsb
  */
  *ut_cout << "msb=\"" << intrinsic->getMsb() << "\" ";
  *ut_cout << "lsb=\"" << intrinsic->getLsb() << "\" ";

  *ut_cout << ">" << UtIO::endl;

  dump_xml_schedule(ut_cout, sig);

  *ut_cout << "    </vector>" << UtIO::endl;
}



static void dump_xml_memory(IODBRuntime * iodb, UtOStream * ut_cout,
                            const STSymbolTableNode * node, const SCHSignature* sig,
                            const IODBGenTypeEntry* type,
                            const IODBIntrinsic *intrinsic )
{
  ST_ASSERT(type, node);
  ST_ASSERT( intrinsic, node );

  *ut_cout << "    <memory ";
  dump_xml_signal_attributes(iodb, ut_cout, node, type);

  /*
  ** msb, lsb, high address, low address
  */
  *ut_cout << "msb=\"" << intrinsic->getMsb() << "\" ";
  *ut_cout << "lsb=\"" << intrinsic->getLsb() << "\" ";
  *ut_cout << "high_address=\"" << intrinsic->getHighAddr() << "\" ";
  *ut_cout << "low_address=\"" << intrinsic->getLowAddr() << "\" ";

  *ut_cout << ">" << UtIO::endl;

  dump_xml_schedule(ut_cout, sig);

  *ut_cout << "    </memory>" << UtIO::endl;
}



static void dump_xml_signals(IODBRuntime * iodb, UtOStream * ut_cout)
{
  STSymbolTable* st = iodb->getDesignSymbolTable();

  *ut_cout << "  <signals>" << UtIO::endl;

  for (STSymbolTable::NodeLoopSorted loop = st->getNodeLoopSorted();
       not loop.atEnd(); 
       ++loop)
  {
    const STSymbolTableNode* node = *loop;

    const SCHSignature* sig = iodb->getSignature(node);
    const IODBGenTypeEntry* type = iodb->getType(node);

    if ((sig == NULL) and (type == NULL))
    {
      continue;
    }

    const IODBIntrinsic* intrinsic = iodb->getLeafIntrinsic( node->castLeaf() );
    ST_ASSERT( intrinsic, node );
    
    switch (intrinsic->getType())
    {
    case IODBIntrinsic::eScalar:
      dump_xml_scalar(iodb, ut_cout, node, sig, type);
      break;
    case IODBIntrinsic::eVector:
      dump_xml_vector(iodb, ut_cout, node, sig, type, intrinsic);
      break;
    case IODBIntrinsic::eMemory:
      dump_xml_memory(iodb, ut_cout, node, sig, type, intrinsic);
      break;
    }
  }

  *ut_cout << "  </signals>" << UtIO::endl;
}



static void dump_xml(IODBRuntime * iodb)
{
  UtOStream& ut_cout = UtIO::cout();
  UtString buf;

  ut_cout << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" << UtIO::endl;
  ut_cout << "<!DOCTYPE carbon_db SYSTEM \"carbon_db.dtd\">" << UtIO::endl;

  ut_cout << "<carbon_db version=\"1.0\">" << UtIO::endl;


  /**
  *** Print out the module name here.
  **/
  dump_xml_module(iodb, &ut_cout);


  /**
  *** Signals
  **/
  dump_xml_signals(iodb, &ut_cout);

#if 0
  for (STSymbolTable::NodeLoopSorted loop = st->getNodeLoopSorted();
       not loop.atEnd(); 
       ++loop)
  {
    const STSymbolTableNode* node = (*loop);
    const SCHSignature* sig = iodb->getSignature(node);
    const IODBTypeEntry* type = iodb->getType(node);

    ut_cout << "\n";
    if (sig != NULL)
    {
      ut_cout << "    ";
      sig->print();
    }
    if (type != NULL)
    {
      ut_cout << "    ";
      type->print();
    }
  }
#endif

  ut_cout << "</carbon_db>" << UtIO::endl;
}


static int sDumpMemory(const char* memdebug,
                       bool listPointers,
                       int detail)
{
  if (!CarbonMem::replay(memdebug, NULL, detail, listPointers))
    return 1;
  return 0;
} // static int sDumpMemory

static void sDoAnalysis(IODB& iodb, AtomicCache& atomicCache,
                        const STSymbolTable& designSymTab)
{
  UtIO::cout() << "AtomicCache size: " << atomicCache.size() << UtIO::endl;
  //  UtIO::cout() << "AtomicCache Entries BEGIN:" << UtIO::endl;
  UInt32 numUsedStrBytes = 0;
  for (AtomicCache::CacheIter aci = atomicCache.getCacheIter(); ! aci.atEnd(); ++aci)
  {
    const char* str = aci.getKey();
    numUsedStrBytes += strlen(str) + 1; // add \0
    // UtIO::cout() << aci.getKey() << UtIO::endl;
  }
  UtIO::cout() << "Num Used String Bytes: " << numUsedStrBytes << UtIO::endl;
  
  DynBitVectorFactory* usedBVPool = iodb.getBVFactory();
  UtIO::cout() << "Used BVPool: ";
  usedBVPool->printStats();
  //  UtIO::cout() << "AtomicCache Entries END:" << UtIO::endl;
  
  IODB::NameSet unneededNodes;
  for (IODB::NameValueLoop l = iodb.loopConstNets(); !l.atEnd(); ++l)
  {
    const STSymbolTableNode* node = l.getKey();
    const STAliasedLeafNode* leaf = node->castLeaf();
    // if the node is a module then assume that is needed
    // if the node is a leaf check it.
    if (leaf && ! iodb.isPrimary(leaf) && ! iodb.isNetExposed(leaf))
      unneededNodes.insert(const_cast<STSymbolTableNode*>(node));
  }

  AtomicCache tmpCache;
  DynBitVectorFactory tmpBVPool(true);
  for (STSymbolTable::CNodeLoop st = designSymTab.getCNodeLoop(); ! st.atEnd(); ++st)
  {
    const STSymbolTableNode* node = *st;
    if (unneededNodes.find(const_cast<STSymbolTableNode*>(node)) == unneededNodes.end())
    {
      tmpCache.intern(node->str());
      const STAliasedLeafNode* leaf = node->castLeaf();
      if (leaf)
      {
        const DynBitVector* bv = iodb.getConstNetBitMask(leaf);
        if (bv)
          tmpBVPool.alloc(*bv);
        const ShellDataBOM* sdBOM = ShellSymTabBOM::getLeafBOM(leaf);
        int tagType = sdBOM->getTypeTag();
        if (tagType == CbuildShellDB::eConstValId)
        {
          bv = sdBOM->getStorageValue();
          tmpBVPool.alloc(*bv);
        }
      }
    }
  }
  UtIO::cout() << "AtomicCache needed size: " << tmpCache.size() << UtIO::endl;
  UInt32 numNeededStrBytes = 0;
  for (AtomicCache::CacheIter aci = tmpCache.getCacheIter(); ! aci.atEnd(); ++aci)
  {
    const char* str = aci.getKey();
    numNeededStrBytes += strlen(str) + 1; // add \0
    // UtIO::cout() << aci.getKey() << UtIO::endl;
  }
  UtIO::cout() << "Num Needed String Bytes: " << numNeededStrBytes << UtIO::endl;

  UtIO::cout() << "Needed BVPool: ";
  tmpBVPool.printStats();
}

static int dump_cgraph(const char* cgraph_file, const char* dbfile, SourceLocatorFactory &slc) {
  CarbonDatabaseStandalone db;
  if (!db.open(dbfile, 0)) {
    return 1;
  }
  CGraph cgraph(&slc, db.getAtomicCache());
  UtString errmsg;
  if (!cgraph.read(cgraph_file, &errmsg, &db)) {
    UtIO::cerr() << errmsg << "\n";
    return 1;
  }

  cgraph.buildSequentialFanin();
  cgraph.sort();
  cgraph.dump();
  return 0;
}

int main(int argc, char** argv)
{
  ArgProc args;
  AtomicCache atomicCache;
  ESFactory exprFactory;
  
  MsgStreamIO errStream(stderr, true);
  MsgContext msgContext;
  SCHScheduleFactory scheduleFactory;
  msgContext.addReportStream(&errStream);
  ShellSymTabBOM bom;
  bom.putScheduleFactory(&scheduleFactory);

  LicCB licCB(&msgContext);
  UtLicense license(&licCB);
  
  {
    UtString licReason;
    // carbondb is needed with cwavetestbench to generate rules files
    if (! license.checkout(UtLicense::eCwaveTestbench, &licReason))
    {
      fprintf(stderr, "%s\n", licReason.c_str());
      return 1;
    }
  }
  
  STSymbolTable designSymTab(&bom, &atomicCache);
  SourceLocatorFactory locations;
  IODBRuntime iodb(&atomicCache, &designSymTab,
                   &msgContext, &scheduleFactory, &exprFactory, &locations);
  iodb.userTypeFactory()->setUserTypesAtomicCache(&atomicCache);

  const SInt32 cGenArgPass = 1;

  int numOptions;
  UtString errmsg;
  {
    UtString buf;
    buf << "carbondb [options] db_file";
    args.addSynopsis(buf);
  }
  const char* sect1 = "Output Control";
  args.createSection(sect1);
  args.addBool("-analyze", "Do some informal analysis of the io database. This currently only works well with an io database, not a symtab. And, the analysis is rather primitive.", false, cGenArgPass);
  args.addToSection(sect1, "-analyze");
  args.addBool("-xml", "Dump using xml format", false, cGenArgPass);
  args.addToSection(sect1, "-xml");
  args.addBool("-aliases", "Dump alias ring", false, cGenArgPass);
  args.addToSection(sect1, "-aliases");
  args.addBool("-bom", "Dump Symboltable BOM contents", false, cGenArgPass);
  args.addToSection(sect1, "-bom");
  args.addString("-node", "Just look for this node and print its information. Works with -aliases, too, in which case this and all its aliases are printed.", NULL, true, false, cGenArgPass);
  args.addToSection(sect1, "-node");
  args.addBool("-storage", "Only valid with -node. Prints out the storage node given by -node", false, cGenArgPass);
  args.addToSection(sect1, "-storage");

  args.addBool("-checkbidis", "Run through the design and look for bidi types that are not declared as bidis in the primary bidi list.", false, cGenArgPass);
  args.addToSection(sect1, "-checkbidis");
  args.addBool("-printexprs", "If a symtab node has an expression associated with it, dump it out", false, cGenArgPass);
  args.addToSection(sect1, "-printexprs");
  args.addBool("-filterLive", "When printing the different sets only print out the primary i/os and depobs that are live. Internal nodes will not be printed.", false, cGenArgPass);
  args.addToSection(sect1, "-filterLive");
  args.addBool("-text", "Print out as much of a BOM as text as possible. This will print out netflags, node flags, etc. as words instead of a hex digits.", false, cGenArgPass);
  args.addToSection(sect1, "-text");
  args.addInputFile("-memDebug", "Read in the specified memory dump file and decode it, dumping the 4 levels of detail to <file>.0, <file>.1, <file>.2, and <file>.4", NULL, true, false, cGenArgPass);
  args.addToSection(sect1, "-memDebug");
  args.addInputFile("-cgraph", "Read in the specified cgraph file and dump it", NULL, true, false, cGenArgPass);
  args.addToSection(sect1, "-cgraph");
  args.addBool("-memPointers", "Dump the allocated pointers values (can get big)", false, cGenArgPass);
  args.addToSection(sect1, "-memPointers");
  args.addInt("-memDetail", "Level of detail (0-3)\n\
<verbatim>\n\
         0 --> brief summary of each allocation\n\
         1 --> correlated, filtered, simplified stack trace of each allocation\n\
         2 --> correlated, simplified stack trace of each allocation\n\
         3 --> correlated stack trace of each allocation\n\
</verbatim>", 0, false, false, cGenArgPass);
  args.addToSection(sect1, "-memDetail");

  args.setDescription("Carbon Database Dumper", "carbondb",
                      "Read's in a full database (libdesign.symtab.db) or an I/O database (libdesign.io.db), and prints it out textually.");

  if ((args.parseCommandLine(&argc, argv, &numOptions, &errmsg) ==
       ArgProc::eParseError))
  {
    fprintf(stdout, "%s\n", errmsg.c_str());
    return 1;
  }
  
  const char* memdebug = args.getStrLast("-memDebug");
  const char* cgraph_file = args.getStrLast("-cgraph");
  if (memdebug != NULL)
  {
    SInt32 detail = 0;
    (void) args.getIntLast("-memDetail", &detail);
    return sDumpMemory(memdebug,
                       args.getBoolValue("-memPointers"),
                       detail);
  }

  if (argc != 2)
  {
    UtString usage;
    args.getUsageVerbose(&usage);
    fprintf(stderr, "%s\n", usage.c_str());
    return 1;
  }

  UtString dbfile(argv[1]);
  CarbonDBType dbType = eCarbonFullDB;
  if (dbfile.rfind(IODB::scIODBExt) != UtString::npos)
    dbType = eCarbonIODB;
  else if (dbfile.rfind (IODB::scGuiDBExt) != UtString::npos)
    dbType = eCarbonGuiDB;
  
  if (!iodb.readDB(argv[1], dbType))
    return 1;  

  if (args.getBoolValue("-analyze"))
    sDoAnalysis(iodb, atomicCache, designSymTab);
  
  if (args.getBoolValue("-checkbidis"))
  {
    UtString bidicheck;
    iodb.doBidiCheck(&bidicheck);
    if (! bidicheck.empty())
      fprintf(stdout, "%s\n", bidicheck.c_str());
  }

  bool doAliases = args.getBoolValue("-aliases");
  bool doBOM = args.getBoolValue("-bom");
  bool printExprs = args.getBoolValue("-printexprs");
  bool filterLive = args.getBoolValue("-filterLive");

  if (args.isParsed("-node") == ArgProc::eParsed)
  {
    const char* nodeName = args.getStrValue("-node");
    if (nodeName)
    {
      STSymbolTable* st = iodb.getDesignSymbolTable();
      HdlHierPath::Status stat;
      HdlId info;
      STSymbolTableNode* node = st->getNode(nodeName, &stat, &info);
      if (! node)
      {
        if (stat == HdlHierPath::eLegal)
          fprintf(stderr, "%s not found.\n", nodeName);
        else if (stat == HdlHierPath::eIllegal)
          fprintf(stderr, "%s not a legal hdl name.\n", nodeName);
        else
          fprintf(stderr, "Internal error parsing %s\n", nodeName);
        return 1;
      }
      else
      {
        STSymbolTableNode* dumpNode = node;
        if (args.getBoolValue("-storage"))
        {
          STAliasedLeafNode* leaf = node->castLeaf();
          if (leaf)
            dumpNode = leaf->getStorage();
        }
        dump_symtab_node(dumpNode, &iodb, doAliases, doBOM, printExprs,
                         args.getBoolValue("-text"));
      }
    }
  }
  else if (args.getBoolValue("-xml"))
  {
    dump_xml(&iodb);
  }
  else if (cgraph_file != NULL) {
    return dump_cgraph(cgraph_file, dbfile.c_str(), locations);
  }
  else
  {
    dump_human_readable(&iodb, doAliases, doBOM, printExprs, filterLive, 
                        args.getBoolValue("-text"));
  }
  
  return 0;
} // int main
