// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/CarbonWaveImp.h"
#include "shell/CarbonHookup.h"
#include "shell/CarbonDBRead.h"
#include "shell/ShellNet.h"
#include "shell/ShellData.h"
#include "util/ShellMsgContext.h"
#include "iodb/IODBRuntime.h"
#include "iodb/IODBTypes.h"
#include "hdl/HdlId.h"
#include "symtab/STSymbolTable.h"
#include "util/UtString.h"
#include "shell/WaveDump.h"
#include "CarbonWaveRegistrar.h"
#include "CarbonWaveVC.h"

/*!
  \file
  CarbonWave implementation and helper objects
*/

static const char scComma = ',';

CarbonWaveImp::CarbonWaveImp(CarbonHookup* hookup) : 
  mHookup(hookup), mDataFile(NULL), mWaveState(eDataNone),
  mVCEngine(NULL),
  mNetRegistrar(NULL),
  mReplaceDesignRoot(false)
{
}

CarbonWaveImp::~CarbonWaveImp()
{
  // order matters here
  delete mNetRegistrar;
  delete mVCEngine;
  delete mDataFile;
}

void CarbonWaveImp::putInit(WaveDump* dataFile)
{
  INFO_ASSERT(mDataFile == NULL, "Can't use more than one data file");
  mDataFile = dataFile;
}

CarbonStatus CarbonWaveImp::dumpVar(CarbonNet* net)
{
  CarbonStatus stat = maybeCreateRegistrar(); 
  if (stat == eCarbon_OK) {
    ShellNet* snet = net->castShellNet();
    IODB* iodb = mHookup->getDB();
    STSymbolTable* symTab = iodb->getDesignSymbolTable();
    STSymbolTableNode* symNode = symTab->lookup(snet->getName());
    ST_ASSERT(symNode, snet->getName());
    mNetRegistrar->processNode(symNode, 0);
    mWaveState = eDataUpdate;
  }
  return stat;
}

CarbonStatus CarbonWaveImp::maybeCreateRegistrar()
{
  CarbonStatus stat = eCarbon_OK;
  if (mDataFile && ! mDataFile->isHierarchyOpen()) {
    MsgContext* msgContext = mHookup->getMsgContext();
    MsgContextBase::Severity sev = msgContext->SHLCannotAddNetsToWave();
    stat = ShellGlobal::severityToStatus(sev);
  }
  else if (! mDataFile) {
    MsgContext* msgContext = mHookup->getMsgContext();
    msgContext->SHLWaveNotInitialized();
    stat = eCarbon_ERROR;
  }
  else if (! mNetRegistrar)
  {
    mVCEngine = new CarbonWaveVC(mHookup, mDataFile);
    mNetRegistrar = new CarbonWaveRegistrar(mVCEngine, mHookup, mDataFile, mHierPrefix, mReplaceDesignRoot);
    mHookup->activateWave();
  }
  return stat;
}


CarbonStatus CarbonWaveImp::extractPaths(const char* specifier,
                                         UtStringArray* argNames)
{
  const IODB* db = mHookup->getDB();
  const HdlHierPath* pather = db->getHdlHier();
  MsgContext* msgContext = mHookup->getMsgContext();
  CarbonStatus stat = maybeCreateRegistrar();
  
  if (stat == eCarbon_ERROR)
    return stat;

  UtStringArray nameElems;
  HdlId info;
  while(specifier && *specifier != '\0')
  {
    if (pather->parseName(&specifier, &nameElems, &info) != HdlHierPath::eEndPath)
    {
      msgContext->SHLBadHdlPathList(specifier);
      // report badness, DO NOT REGISTER ANYTHING!
      return eCarbon_ERROR;
    }
    else
    {
      UtString buf;
      pather->compPath(nameElems, &buf, &info);
      argNames->push_back(buf);
    }
    nameElems.clear();
    
    if (*specifier == scComma)
      ++specifier;
    
    while(isspace(*specifier))
      ++specifier;
  }
  
  if (argNames->empty())
  {
    msgContext->SHLDumpVarsBadUsage();
    stat = eCarbon_ERROR;
  }
  
  return stat;
}
                                          
CarbonStatus CarbonWaveImp::dumpStateIO(unsigned int levels, 
                                         const char* specifier)
{
  UtStringArray argNames;
  CarbonStatus stat = extractPaths(specifier, &argNames);

  if (stat == eCarbon_OK)
  {
    stat = mNetRegistrar->doNets(argNames, levels, CarbonWaveRegistrar::eStateIONode);
    if (stat == eCarbon_OK)
      mWaveState = eDataUpdate;
  }
  return stat;
}


CarbonStatus CarbonWaveImp::dumpVars(unsigned int levels, 
                                      const char* specifier)
{
  UtStringArray argNames;
  CarbonStatus stat = extractPaths(specifier, &argNames);

  if (stat == eCarbon_OK)
  {
    stat = mNetRegistrar->doNets(argNames, levels);
    if (stat == eCarbon_OK)
      mWaveState = eDataUpdate;
  }

  return stat;
}

void CarbonWaveImp::dumpOn()
{
  mWaveState = eDataOn;
  // Tell the model to call the waveCycle
  mHookup->activateWave();
}

void CarbonWaveImp::dumpOff()
{
  // Can change this even if we haven't written a value out yet. A
  // user may not want to dump at the beginning of time.
  mWaveState = eDataOff;
}

void CarbonWaveImp::dumpAll()
{
  mWaveState = eDataDumpAll;
}

CarbonStatus CarbonWaveImp::dumpFlush()
{
  CarbonStatus stat = eCarbon_OK;
  if (mDataFile)
  {
    UtString errMsg;
    if (mDataFile->flush(&errMsg) == WaveDump::eError)
    {
      stat = eCarbon_ERROR;
      MsgContext* msgContext = mHookup->getMsgContext();
      msgContext->SHLFileProblem(errMsg.c_str());
    }
  }
  return stat;
}

// Speeds up the process of waveform dumping by limiting the process only to 
// the signals for which waveform dumping has been enabled and their bit size 
// does not exceed the passed limit size.
// Calls CarbonWaveRegistrar::setDumpSizeLimit
CarbonStatus CarbonWaveImp::dumpSizeLimit(UInt32 size)
{
  CarbonStatus stat;

  stat = maybeCreateRegistrar();
  if(stat == eCarbon_OK)  
    mNetRegistrar->setDumpSizeLimit(size);
  return stat;
}
 
void CarbonWaveImp::setChangedNets(CarbonWaveNetAssoc::NetWriteFlags flags)
{
  mVCEngine->setChangedNets(flags);
}

bool CarbonWaveImp::doForceWrite()
{
  return mHookup->getInit () || (mDataFile->numAdvanceTimes() == 0);
}

void CarbonWaveImp::waveOn()
{
  if (mDataFile)
  {
    bool fileIsOff = ! mDataFile->isDumpOn();
    bool doUpdate = (fileIsOff || doForceWrite());
    if (doUpdate)
    {
      setChangedNets(CarbonWaveNetAssoc::NetWriteFlags(CarbonWaveNetAssoc::eValNoHandle | CarbonWaveNetAssoc::eValWriteVal | CarbonWaveNetAssoc::eValUpdate));
    }

    if (fileIsOff)
      mDataFile->dumpOn(mHookup->getTime());
    else if (doUpdate)
      mDataFile->dumpAll(mHookup->getTime());
  }
}

void CarbonWaveImp::waveOff()
{
  if (mDataFile && mDataFile->isDumpOn()) {
    mDataFile->dumpOff(mHookup->getTime());    
    mHookup->deactivateWave();
  }
}

void CarbonWaveImp::waveAll()
{
  if (mDataFile)
  {
    bool fileIsOn = mDataFile->isDumpOn();
    CarbonWaveNetAssoc::NetWriteFlags flags = CarbonWaveNetAssoc::NetWriteFlags(CarbonWaveNetAssoc::eValNoHandle | CarbonWaveNetAssoc::eValUpdate);
    if (fileIsOn && doForceWrite())
    {
      flags = CarbonWaveNetAssoc::NetWriteFlags(flags | CarbonWaveNetAssoc::eValWriteVal);
    }

    if (fileIsOn)
    {
      mDataFile->flushPendingData();

      setChangedNets(flags);
      mDataFile->dumpAll(mHookup->getTime());    
    }
  }
}

void CarbonWaveImp::waveUpdate()
{
  if (mDataFile && mDataFile->isDumpOn())
  {
    UInt64 currentTime = mHookup->getTime();
    mDataFile->advanceTime(currentTime);
    
    CarbonWaveNetAssoc::NetWriteFlags flags = CarbonWaveNetAssoc::eValUpdate;
    if (doForceWrite())
      flags = CarbonWaveNetAssoc::NetWriteFlags(flags | CarbonWaveNetAssoc::eValWriteVal | CarbonWaveNetAssoc::eValNoHandle);
    setChangedNets(flags);
  }
}

void CarbonWaveImp::closeHierarchy()
{
  if (! mDataFile || ! mNetRegistrar)
    return;
  
  // check if we need to close the wave hierarchy
  if (mDataFile->isHierarchyOpen())
  {
    mNetRegistrar->cleanupAssocs();
  }
}

bool CarbonWaveImp::isHierarchyOpen()
{
  bool open = true;
  if (mDataFile) {
    open = mDataFile->isHierarchyOpen();
  }
  return open;
}

void CarbonWaveImp::runWaveSchedule()
{
  if (! mDataFile || ! mNetRegistrar)
    return;
  
  closeHierarchy();

  if ((mWaveState != eDataOff) && ! mDataFile->isInitTimeSet())
  {
    mDataFile->setInitialTime(mHookup->getTime());
  }

  switch(mWaveState)
  {
  case eDataUpdate:
    if (mNetRegistrar->runDebugSchedule())
      mHookup->getCarbonModel()->runSampleSchedule();
    waveUpdate();
    break;
  case eDataDumpAll:
    if (mNetRegistrar->runDebugSchedule())
      mHookup->getCarbonModel()->runSampleSchedule();
    waveAll();
    mWaveState = eDataUpdate;
    break;
  case eDataOff:
    waveOff();
    mWaveState = eDataNone;
    break;
  case eDataOn:
    if (mNetRegistrar->runDebugSchedule())
      mHookup->getCarbonModel()->runSampleSchedule();
    waveOn();
    mWaveState = eDataUpdate;      
    break;
  case eDataNone:
    break;
  }
}

CarbonTimescale CarbonWaveImp::getTimeScale() const
{
  CarbonTimescale scale = e1ns;
  if (mDataFile)
    scale = mDataFile->getTimeScale();
  return scale;
}

CarbonTime CarbonWaveImp::getTime() const
{
  CarbonTime time = 0;
  if (mDataFile)
    time = mDataFile->getCurrentTime();
  return time;
}

CarbonStatus CarbonWaveImp::putPrefixHierarchy(const char* prefix, bool replaceDesignRoot)
{
  const STSymbolTable* symTab = mHookup->getDB()->getDesignSymbolTable();
  const HdlHierPath* pather = symTab->getHdlHier();
  bool isGood = true;
  HdlId info;
  if (pather->decompPath(prefix, &mHierPrefix, &info) != HdlHierPath::eLegal)
    isGood = false;
  else 
    mReplaceDesignRoot = replaceDesignRoot;
  return isGood ? eCarbon_OK : eCarbon_ERROR;
}

void CarbonWaveImp::flushPendingData()
{
  closeHierarchy();
  if (mDataFile)
    mDataFile->flushPendingData();
}

CarbonWaveUserData* 
CarbonWaveImp::addUserData(CarbonVarType netType, 
                           CarbonVarDirection direction,
                           CarbonDataType dataType,
                           int lbitnum, int rbitnum,
                           CarbonClientData data, 
                           const char* varName, 
                           CarbonBytesPerBit bpb,
                           const char* fullPathScopeName,
                           const char* scopeSeparator)
{
  if (! mDataFile->isFsdb())
  {
    MsgContext* msgContext = mHookup->getMsgContext();
    msgContext->SHLUserDataUnsupportedFormat();
    return NULL;
  }

  UtString errMsg;
  CarbonWaveUserData* userData = 
    mDataFile->addUserVar(netType, direction, dataType, lbitnum, rbitnum, 
                          data, varName, bpb, fullPathScopeName, scopeSeparator,
                          &errMsg);
  
  if (! userData)
  {
    MsgContext* msgContext = mHookup->getMsgContext();
    msgContext->SHLUserDataAddFail(errMsg.c_str());
  }
  return userData;
}

void CarbonWaveImp::manualUpdateWaveform()
{
  mHookup->maybeRunDepositSchedule();
  runWaveSchedule();
}

CarbonModel* CarbonWaveImp::getCarbonModel()
{
  return mHookup->getCarbonModel();
}

bool CarbonWaveImp::debugScheduleActive() const
{
  return (mNetRegistrar && mNetRegistrar->runDebugSchedule());
}

// interface destructor
CarbonWave::~CarbonWave()
{}

CarbonStatus CarbonWaveImp::switchFSDBFile(const char* fileName)
{
  // Only supported on FSDB wave files
  if (!mDataFile->isFsdb()) {
    MsgContext* msgContext = mHookup->getMsgContext();
    msgContext->SHLSwitchUnsupportedFormat();
    return eCarbon_ERROR;
  }

  // Switch to the new dump file
  if (mDataFile->switchFSDBFile(fileName)) {
    return eCarbon_OK;
  } else {
    return eCarbon_ERROR;
  }
}
