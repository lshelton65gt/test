// -*-c++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/carbon_interface.h"
#include "util/UtConv.h"
#include "util/UtIOStream.h"
#include "util/DynBitVector.h"

// This in a separate file so it can easily be linked into the BV unit test

UInt32 carbonInterfaceGetWordMask(UInt32 numBits)
{
  return CarbonValRW::getWordMask(numBits);
}

UInt64 carbonInterfaceGetWordMaskLL(UInt64 numBits)
{
  return CarbonValRW::getWordMaskLL(numBits);
}

void carbonInterfaceCpSrc32ToDest64Range(UInt64* dest, const UInt32 *src, size_t index, size_t length)
{
  CarbonValRW::cpSrcToDestRange(dest, src, index, length);
}

void carbonInterfaceCpSrc32ToDest32Range(UInt32* dest, const UInt32 *src, size_t index, size_t length)
{
  CarbonValRW::cpSrcToDestRange(dest, src, index, length);
}

void carbonInterfaceCpSrc32ToDest16Range(UInt16* dest, const UInt32 *src, size_t index, size_t length)
{
  CarbonValRW::cpSrcToDestRange(dest, src, index, length);
}

void carbonInterfaceCpSrc32ToDest8Range(UInt8* dest, const UInt32 *src, size_t index, size_t length)
{
  CarbonValRW::cpSrcToDestRange(dest, src, index, length);
}

void carbonInterfaceCpSrc64ToDest32Word(UInt32* dest, const UInt64* src, size_t wordIndex)
{
  CarbonValRW::cpSrcToDestWord(dest, src, wordIndex);
}

void carbonInterfaceCpSrc32ToDest32Word(UInt32* dest, const UInt32* src, size_t wordIndex)
{
  CarbonValRW::cpSrcToDestWord(dest, src, wordIndex);
}

void carbonInterfaceCpSrc16ToDest32Word(UInt32* dest, const UInt16* src, size_t wordIndex)
{
  CarbonValRW::cpSrcToDestWord(dest, src, wordIndex);
}

void carbonInterfaceCpSrc8ToDest32Word(UInt32* dest, const UInt8* src, size_t wordIndex)
{
  CarbonValRW::cpSrcToDestWord(dest, src, wordIndex);
}

void carbonInterfaceCpSrc32WordToDest64(UInt64* dest, const UInt32 src, size_t wordIndex)
{
  CarbonValRW::cpSrcWordToDest(dest, src, wordIndex);
}

void carbonInterfaceCpSrc32WordToDest32(UInt32* dest, const UInt32 src, size_t wordIndex)
{
  CarbonValRW::cpSrcWordToDest(dest, src, wordIndex);
}

void carbonInterfaceCpSrc32WordToDest16(UInt16* dest, const UInt32 src, size_t wordIndex)
{
  CarbonValRW::cpSrcWordToDest(dest, src, wordIndex);
}

void carbonInterfaceCpSrc32WordToDest8(UInt8* dest, const UInt32 src, size_t wordIndex)
{
  CarbonValRW::cpSrcWordToDest(dest, src, wordIndex);
}

void carbonInterfaceCpSrc64WordToDest32(UInt32* dest, const UInt64 src, size_t wordIndex)
{
  CarbonValRW::cpSrcWordToDest(dest, src, wordIndex);
}

void carbonInterfaceCpSrc16WordToDest32(UInt32* dest, const UInt16 src, size_t wordIndex)
{
  CarbonValRW::cpSrcWordToDest(dest, src, wordIndex);
}

void carbonInterfaceCpSrc8WordToDest32(UInt32* dest, const UInt8 src, size_t wordIndex)
{
  CarbonValRW::cpSrcWordToDest(dest, src, wordIndex);
}

void carbonInterfaceCpSrcRangeToDestRange(UInt32* dest, size_t dstIndex,
                                          const UInt32* src, size_t srcindex,
                                          size_t length)
{
  CarbonValRW::cpSrcRangeToDestRange(dest, dstIndex, src, srcindex, length);
}

bool carbonInterfaceUtOStreamFormatBignum(UtOStream* str, const UInt32* data, UInt32 numBits)
{
  bool ret = str->formatBignum(data, numBits);
  return ret;
}

bool carbonInterfaceUtOStreamFormatBignumRange(UtOStream* str, const UInt32* data, UInt32 size, UInt32 offset)
{
  // This does something like sFormatBitvecRef() in
  // src/util/BitVectorIO.cxx, except that the BVRef elements have already been provided.

  // Create a DynBitVector from the necessary range of the data array, including the offset
  UInt32 numWords = (size + offset + 31) / 32;
  DynBitVector tempCopy(size + offset, data, numWords);
  // Shift out the unneeded offset bits, leaving just the desired range.
  tempCopy >>= offset;
  bool ret = str->formatBignum(tempCopy.getUIntArray(), size);
  return ret;
}
