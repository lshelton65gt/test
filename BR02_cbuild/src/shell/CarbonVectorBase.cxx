// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/CarbonVectorBase.h"
#include <cassert>
#include "util/ConstantRange.h"

CarbonVectorBase::CarbonVectorBase() :
  mRange(NULL), mControlMask(NULL)
{}

void CarbonVectorBase::putRange(const ConstantRange* range)
{
  mRange = range;
}

void CarbonVectorBase::putControlMask(const UInt32* controlMask)
{
  mControlMask = controlMask;
  setConstantBits();
}

CarbonVectorBase::~CarbonVectorBase()
{}

int CarbonVectorBase::getBitWidth() const
{
  return int(mRange->getLength());
}

int CarbonVectorBase::getNumUInt32s() const
{
  return int(mRange->numWordsNeeded());
}

bool CarbonVectorBase::isVector() const
{
  return true;
}

void CarbonVectorBase::maybeSetupShadow()
{}

int CarbonVectorBase::getLSB() const
{
  return int(mRange->getLsb());
}

int CarbonVectorBase::getMSB() const
{
  return int(mRange->getMsb());
}

const CarbonVectorBase* CarbonVectorBase::castVector() const
{
  return this;
}


void CarbonVectorBase::putToZero(CarbonModel* model)
{
  UInt32 val = 0;
  int numWords = getNumUInt32s();
  for (int i = 0; i < numWords; ++i)
    fastDepositWord(val, i, val, model);
}

void CarbonVectorBase::putToOnes(CarbonModel* model)
{
  UInt32 val = 0;
  val = ~val;
  UInt32 drv = 0;
  int numWords = getNumUInt32s();
  int i;
  for (i = 0; i < numWords - 1; ++i)
    fastDepositWord(val, i, drv, model);

  UInt32 offset = getBitWidth() % (sizeof(UInt32) * 8);
  if (offset != 0)
  {
    val <<= offset;
    val = ~val;
  }
  fastDepositWord(val, i, drv, model);
}

CarbonStatus CarbonVectorBase::setRange(int range_msb, int range_lsb, CarbonModel* model)
{
  UInt32 val[2];
  val[0] = UtUINT32_MAX;
  val[1] = UtUINT32_MAX;
  
  UInt32 drv[2];
  drv[0] = 0;
  drv[1] = 0;
  return noAllocDepositValRange(val, drv, range_msb, range_lsb, model);
}

//! ShellNet::clearRange()
CarbonStatus CarbonVectorBase::clearRange(int range_msb, int range_lsb, CarbonModel* model)
{
  UInt32 val[2];
  val[0] = 0;
  val[1] = 0;
  
  return noAllocDepositValRange(val, val, range_msb, range_lsb, model);
}

CarbonStatus CarbonVectorBase::noAllocDepositValRange(const UInt32* val, 
                                                      const UInt32* drv, 
                                                      int range_msb, 
                                                      int range_lsb, 
                                                      CarbonModel* model)
{
  CarbonStatus stat = eCarbon_OK;
  size_t rangeLength = CarbonUtil::getRangeBitWidth(range_msb, range_lsb);
  if (rangeLength <= 64)
    fastDepositRange(val, range_msb, range_lsb, drv, model);
  else
  {
    // An ugly way to do this
    while ((range_lsb < range_msb) && (stat == eCarbon_OK))
    {
      bool dummy;
      int nextlsb = CarbonUtil::closeDistance(range_msb, range_lsb, 64, &dummy);
      int curmsb = nextlsb;
      if (dummy)
        curmsb -= 1;
      else
        curmsb += 1;
      
      fastDepositRange(val, curmsb, range_lsb, drv, model);
      range_lsb = nextlsb;
    }
  }
  return stat;
}

void CarbonVectorBase::getExternalDrive(UInt32* xdrive) const
{
  if (xdrive)
  {
    UInt32 width = mRange->getLength();
    UInt32 numWordsNeeded = mRange->numWordsNeeded();
    UInt32 lastWordMask = CarbonValRW::getWordMask(width);
    CarbonValRW::setToOnes(xdrive, numWordsNeeded);
    // mask off the bits beyond the range
    xdrive[numWordsNeeded - 1] &= lastWordMask;
  }
}

CarbonStatus CarbonVectorBase::checkIfDriveSet(const UInt32* drive, CarbonModel* model)
{
  CarbonStatus stat = eCarbon_OK;
  bool driving = true;
  UInt32 numWords = mRange->numWordsNeeded();
  for (UInt32 i = 0; ((i < numWords) & driving) != 0; ++i)
  {
    if (drive[i] != 0)
    {
      stat = ShellGlobal::reportSetDriveOnNonTristate(getNameAsLeaf(), model);
      driving = false;
    }
  }
  return stat;
}

CarbonStatus CarbonVectorBase::checkIfDriveSetRange(const UInt32* drive, int range_msb, int range_lsb, CarbonModel* model)
{
  CarbonStatus stat = eCarbon_OK;
  bool driving = true;
  UInt32 numWords = CarbonUtil::getRangeNumUInt32s(range_msb, range_lsb);
  for (UInt32 i = 0; ((i < numWords) & driving) != 0; ++i)
  {
    if (drive[i] != 0)
    {
      ShellGlobal::reportSetDriveOnNonTristate(getNameAsLeaf(), model);
      stat = eCarbon_ERROR;
      driving = false;
    }
  }
  return stat;
}

bool CarbonVectorBase::isScalar() const
{
  return false;
}

bool CarbonVectorBase::isReal() const
{
  return false;
}

bool CarbonVectorBase::isTristate() const
{
  return false;
}

bool CarbonVectorBase::setToDriven(CarbonModel*)
{
  return false;
}

bool CarbonVectorBase::setToUndriven(CarbonModel*)
{
  return false;
}

bool CarbonVectorBase::setWordToUndriven(int, CarbonModel*)
{
  return false;
}

bool CarbonVectorBase::resolveXdrive(CarbonModel*)
{
  return false;
}

bool CarbonVectorBase::setRangeToUndriven(int, int, CarbonModel*)
{
  return false;
}

CarbonStatus CarbonVectorBase::examine(CarbonReal*, UInt32*, ExamineMode, CarbonModel*) const
{
  return eCarbon_ERROR;
}

CarbonStatus CarbonVectorBase::force(const UInt32*, CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus CarbonVectorBase::forceWord(UInt32, int, CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus CarbonVectorBase::forceRange(const UInt32*, int, int, CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus CarbonVectorBase::release(CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus CarbonVectorBase::releaseWord(int, CarbonModel*)
{
  return eCarbon_ERROR;
}

CarbonStatus CarbonVectorBase::releaseRange(int, int, CarbonModel*)
{
  return eCarbon_ERROR;
}

bool CarbonVectorBase::isForcible() const
{
  return false;
}

bool CarbonVectorBase::isInput() const
{
  return false;
}

const CarbonMemory* CarbonVectorBase::castMemory() const
{
  return NULL;
}

const CarbonModelMemory* CarbonVectorBase::castModelMemory() const
{
  return NULL;
}

const CarbonScalarBase* CarbonVectorBase::castScalar() const
{
  return NULL;
}

const ShellNetConstant* CarbonVectorBase::castConstant() const
{
  return NULL;
}

int CarbonVectorBase::hasDriveConflict() const
{
  return 0;
}

int CarbonVectorBase::hasDriveConflictRange(SInt32, SInt32) const
{
  return 0;
}

void CarbonVectorBase::setRawToUndriven(CarbonModel*)
{
}

void CarbonVectorBase::putChangeArrayRef(CarbonChangeType*)
{
}

CarbonChangeType* CarbonVectorBase::getChangeArrayRef()
{
  return NULL;
}

const UInt32* CarbonVectorBase::getControlMask() const
{
  return mControlMask;
}
