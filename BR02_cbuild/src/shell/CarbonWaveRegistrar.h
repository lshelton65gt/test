// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// author: Mark Seneski

#ifndef __CARBONWAVEREGISTRAR_H_
#define __CARBONWAVEREGISTRAR_H_

#include "shell/CarbonWaveImp.h"
#include "util/UtArray.h"
#include "util/UtHashMap.h"

#ifndef _C_MEMMANAGER_H_
#include "carbon/c_memmanager.h"
#endif

class SCHSignature;
class CarbonWaveNetAssoc;
class ShellNet;
class CarbonWaveVC;
class ScheduleStimuli;

//! Registers scopes and nets to be dumped to a waveform
/*!
  This class processes waveform dump requests, walking the design
  symbol table to find and process all scopes/nets.  Originally,
  STBranchNodes mapped to waveform scopes and STAliasedLeafNodes
  mapped to waveform signals.  However, the addition of composite
  visibility has changed that.

  Additionally, composite visibility requires the creation of
  CarbonDatabaseNodes, which are thin wrappers over
  STSymbolTableNodes.  Only CarbonDatabaseNodes, not
  STSymbolTableNodes, are exposed to the user through the DB API.
  However, for internal use during waveform dumping, the raw
  STSymbolTableNode is sufficient in most cases.  CarbonDatabaseNodes
  are constructed and used only when required for composites.

  Much of the dumping process is independent of the node type.  As a
  result, there are several related functions pairs - one for
  STSymbolTableNodes and one for CarbonDatabaseNodes.  Where possible,
  the common functionality is extracted to a node-independent function
  or helper class.
 */
class CarbonWaveRegistrar
{
public: 
  CARBONMEM_OVERRIDES

  CarbonWaveRegistrar(CarbonWaveVC* vcEngine,
                      CarbonHookup* hookup,
                      WaveDump* dataFile, 
                      const UtStringArray& prefixHier,
                      bool replaceDesignRoot);
  
  ~CarbonWaveRegistrar();
  
  //! Enum for node selections
  enum NodeType {
    eAnyNode, //!< Any node
    eStateIONode //!< Nodes that are either state or primary i/os
  };
  
  bool runDebugSchedule() const;

  CarbonStatus doNets(UtStringArray& nodeNames, 
                      unsigned int depth,
                      NodeType nodeType = eAnyNode);
  
  void cleanupAssocs();
  
  CarbonStatus processNode(STSymbolTableNode* symNode, unsigned int depth);

  /* 
   * Speeds up the process of waveform dumping by limiting the process only to 
   * the signals for which waveform dumping has been enabled and their bit size
   * does not exceed the passed limit size. 
   */
  //! Sets the size limit for waveform dumping 
  void setDumpSizeLimit(UInt32 size);

private:
  class AliasData;
  class SplitNetWalker;
  friend class SplitNetWalker;

  class NodeContext;
  class CheckExprCompleteWalk;

  class ScopeWalker;
  class STScopeWalker;
  class DBScopeWalker;

  CarbonWaveVC* mCarbonVC;
  CarbonHookup* mHookup;
  WaveDump* mDataFile;
  NodeContext* mNodeContext;

  NetAssocVec mAllAssocs;
  typedef UtArray<AliasData*> AliasDataVec;
  typedef Loop<AliasDataVec> AliasDataVecLoop;
  AliasDataVec mAllAliases;

  WaveScope* mPrefixHier;
  typedef UtHashMap<const STBranchNode*, WaveScope*> BranchScopeMap;
  BranchScopeMap mBranchToScope;
  typedef UtHashMap<CarbonDatabaseNode*, WaveScope*> DBNodeScopeMap;
  DBNodeScopeMap mDBNodeToScope;

  NodeType mNodeType;

  // A marker to know whether we are walking an expression or
  // not. This gets incremented for each nesting of expression we walk
  // into. It should never fall below 0.
  int mWalkingExpr;

  bool mRunDebugSchedule;
  bool mReplaceDesignRoot;
  bool mAlwaysUpdate;  

  /*
    True if we are not supposed to dump expressions that aren't
    concats. Port vectorization will create many bitsels that need to
    be evaluated on every call. So, setting this to avoid those nets
    will give better dumping performance, but no visibility to those
    nets.
  */
  bool mConcatExprsOnly;

  // Dump Size Limit: Stores the signal size limit for dumping waveforms
  //! Default value for limiting waveform dumping (1 KBit)         
  static const UInt32 scDefaultDumpSizeLimit = 1024;
  //! Cuurent value for limiting waveform dumping
  UInt32 mDumpSizeLimit;

  typedef UtHashMap<STAliasedLeafNode*, bool> LeafBoolMap;
  //! Map (keyed by master node) of whether all aliases are the same language (Veriog/VHDL)
  LeafBoolMap mSameLanguageMap;

private:

  /*
    Called during isWaveableNet to check if a node's expression, if
    it exists is compatible with our wave dumping
    scheme. mConcatExprsOnly affects the return value.
    
    Returns true if the expression is compatible or if there is no
    expression.
    
    Returns false if the expression exists and it is not compatible.
  */
  bool checkExprCompatibility(const STAliasedLeafNode* node) const;
  
  // Called when the hierarchy is closed. The SigToStim map is no
  // longer needed. So, just put the stimuli in the Stim vector and
  // clear the map
  void transferMaps();

  // Does the node correspond to an array net?
  bool isArrayNet(const STAliasedLeafNode* node) const;

  bool isWaveAbleNet(const STAliasedLeafNode* node) const;
  
  MsgContext* getMsgContext();

  CarbonWaveNetAssoc* getAssoc(STSymbolTableNode* node);
  
  static NetFlags sGetNetFlags(const STSymbolTableNode* node);

  ShellNet* getShellNet(STAliasedLeafNode* leaf);

  bool isStateIONode(const STAliasedLeafNode* leaf) const;

  bool requiresDebugSchedule(const STAliasedLeafNode* leaf) const;

  void addNetToScope(WaveScope* scope, 
                     CarbonWaveNetAssoc* waveAssoc,
                     STAliasedLeafNode* leaf);

  void addNetToScope(WaveScope* scope, 
                     CarbonWaveNetAssoc* waveAssoc,
                     CarbonDatabaseNode* node);

  ScheduleStimuli* getSchedStim(SCHSignature* sig);
  
  void putInNonPodCheckList(CarbonWaveNetAssoc* assoc);
  
  void putInPodCheckList(CarbonWaveNetAssoc* assoc);
  
  void scheduleNet(ShellNet* theNet, CarbonWaveNetAssoc* assoc);
  
  CarbonWaveNetAssoc* maybeCreateAssoc(STSymbolTableNode* node, bool* isCreated);
  CarbonWaveNetAssoc* maybeCreateAssoc(CarbonDatabaseNode* node, bool* isCreated);
  CarbonWaveNetAssoc* maybeCreateAssoc(ShellData* shellData, bool* isCreated);
  
  //! Add the composite net represented by node to the waveform.
  void addCompositeToWave(CarbonDatabaseNode* node, const UserType* ut);

  //! Add the scope associated with module or composite node to waveform.
  void addScopeToWave(STBranchNode* chBr, unsigned int depth, bool doAll);

  void addLeafToWave(STAliasedLeafNode* chLf, WaveScope* brScope);

  //! Adds the DB node as a signal (not a scope) to the waveform
  void addNodeToWave(CarbonDatabaseNode* node, WaveScope* brScope);
  
  void addDownNets(STBranchNode* bnode, WaveScope* bScope, 
                   unsigned int depth, 
                   bool doAll);
  
  void addDownNets(CarbonDatabaseNode* node, WaveScope* bScope);
  
  WaveScope* generateScopes(STBranchNode* bnode);

  WaveScope* generateScopes(CarbonDatabaseNode* node);

  WaveScope* walkScopes(ScopeWalker* walker);

};

#endif
