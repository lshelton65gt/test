// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// author: Mark Seneski

#include "util/CarbonPlatform.h"
#include "shell/CarbonHookup.h"
#include "schedule/Event.h"
#include "shell/ShellData.h"
#include "shell/CarbonModel.h"
#include "shell/CarbonDBRead.h"
#include "iodb/IODBRuntime.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Signature.h"
#include "symtab/STAliasedLeafNode.h"
#include "CarbonExamineScheduler.h"
#include "iodb/ScheduleFactory.h"
#include "CarbonDatabaseNode.h"

class BVOp
{
public: CARBONMEM_OVERRIDES
  BVOp(UInt32 numWords): mNumWords(numWords),
                         mNumBytes(4*numWords) {}
  UInt32 numWords() const {return mNumWords;}
  UInt32 numBytes() const {return mNumBytes;}
  bool reportChangedBits() const { return false; }
  bool isEqual(const UInt32& a, UInt32* b) const {
    return memcmp(&a, b, mNumBytes) == 0;
  }
  void copy(UInt32* dst, const UInt32& src) const {
    memcpy(dst, &src, mNumBytes);
  }

private:
  UInt32 mNumWords;
  UInt32 mNumBytes;
};

/*!
  Just like PodOpBitDiff (in CarbonExamineScheduler.h) is a version of
  PodOp that updates a mask of changed bits, this does the same thing
  for bitvectors.
*/
class BVOpBitDiff
{
public: CARBONMEM_OVERRIDES
  BVOpBitDiff(UInt32 numWords, UInt32 bitDiffOffset)
    : mNumWords(numWords), mNumBytes(4*numWords), mBitDiffOffset(bitDiffOffset) {}
  UInt32 numWords() const {return mNumWords;}
  UInt32 numBytes() const {return mNumBytes;}
  bool reportChangedBits() const { return true; }
  bool isEqual(const UInt32& a, UInt32* b) const {
    bool equal = (memcmp(&a, b, mNumBytes) == 0);
    if (!equal) {
      // Update the changed bits
      UInt32* changed = &b[mBitDiffOffset];
      const UInt32* a_ptr = &a;
      for (UInt32 i = 0; i < mNumWords; ++i) {
        changed[i] = a_ptr[i] ^ b[i];
      }
      return false;
    }
    return equal;
  }
  void copy(UInt32* dst, const UInt32& src) const {
    memcpy(dst, &src, mNumBytes);
  }

  // Ensure we have a copy constructor.  The default one should work,
  // but let's be safe.
  BVOpBitDiff(const BVOpBitDiff& other)
  : mNumWords(other.mNumWords), mNumBytes(other.mNumBytes), mBitDiffOffset(other.mBitDiffOffset) {}

private:
  UInt32 mNumWords;
  UInt32 mNumBytes;
  const UInt32 mBitDiffOffset;

  // We have a const member, so we can't assign
  BVOpBitDiff& operator=(const BVOpBitDiff& other);
};


static size_t sNumWords(UInt32 width)
{
  return ((width + 31)/32);
}

static size_t sNumBytes(UInt32 width)
{
  size_t ret = 1;
  if ((width > 8) && (width <= 16))
    ret = 2;
  else if ((width > 16) && (width <= 32))
    ret = 4;
  else if ((width > 32) && (width <= 64))
    ret = 8;
  else
    ret = (width + 7)/8;
  return ret;
}


ScheduleStimuli::ScheduleStimuli(SCHSignature* signature) : 
  mSignature(signature), mChangeArray(NULL), mTransferMaps(true), mInvalid(false), mStimulated(false) 
{}

ScheduleStimuli::~ScheduleStimuli() 
{
  if (mChangeArray) {
    UInt32 numEdges = mEdgeAndNet.size() + mNonPods.size() + 1;
    CARBON_FREE_VEC(mChangeArray, CarbonChangeType*, numEdges);
  }

  // I could decrement the reference count on the nets here, but most
  // likely we are destroying the entire model at this point and it
  // would be a waste of time to do so.

  for (EdgeNetLoop p(mNonPods); 
       ! p.atEnd(); ++p)
    delete *p;
  mNonPods.clear();

  for (EdgeNetLoop p(mEdgeAndNet);
       ! p.atEnd(); ++p)
    delete *p;
  mEdgeAndNet.clear();
  
  
  for (NodeEdgeMap::UnsortedLoop p = mNodeToEdge.loopUnsorted();
       ! p.atEnd(); ++p)
  {
    EdgeDetectInfo* info = p.getValue();
    delete info;
  }
  
  for (VCVecLoop p(mVCObjs);
       ! p.atEnd(); ++p)
  {
    CarbonValueChangeBase* base = *p;
    base->clear();
  }
}

void ScheduleStimuli::addNonPodClock(EdgeInfoNetPair* infoNet)
{
  mNonPods.push_back(infoNet);
}

bool ScheduleStimuli::addEventNodes(EventList& vec, CarbonHookup* hookup) {
  EdgeInfoNetPairVec added;
  for(EventListLoop p(vec); ! p.atEnd(); ++p)
  {
    const SCHEvent* event = *p;
    ClockStim evStim = (event->getClockEdge() == eClockPosedge) ? ePosEdge : eNegEdge;
    STAliasedLeafNode* leafNode = event->getClockModify()->castLeaf();
    FUNC_ASSERT(leafNode, event->print());
    STAliasedLeafNode* node = leafNode->getStorage();
    
    NodeEdgeMap::iterator q = mNodeToEdge.find(node);
    EdgeDetectInfo* info = NULL;
    if (q != mNodeToEdge.end())
    {
      info = q->second;
      if (info->mEdgeStim != evStim)
        info->mEdgeStim = eBothEdges;
    }
    else
    {
      SInt32 chgIndex = hookup->getChangeArrayIndex(node);
      if (chgIndex != -1)
      {
        CarbonChangeType* chgEntry = hookup->getChangeArrayEntry(chgIndex);
        info = new EdgeDetectInfo(chgEntry, evStim);
        mNodeToEdge[node] = info;

        // Up the reference count for the net
        ShellNet* net = hookup->getCarbonNet(node);
        ST_ASSERT(net, node);

        ShellNet::Traits traits;
        net->getTraits(&traits);

        if (traits.getConstant() == NULL)
        {
          EdgeInfoNetPair* infoNetPair = new EdgeInfoNetPair(info, net);
          
          if (traits.isTristate())
            addNonPodClock(infoNetPair);
          else
            added.push_back(infoNetPair);
        }
        else
          // decr the ref count
          hookup->freeNet(&net);
      }
      else
      {
        // derived clocks don't have entries in the change array,

        setInvalid();
        if (! added.empty())
        {
          for (EdgeNetLoop q(added);
               ! q.atEnd(); ++q)
          {
            EdgeInfoNetPair* infoNetPair = *q;
            ShellNet* shlNet = infoNetPair->mNet;
            // decrement the reference count - may free up some memory.
            hookup->freeNet(&shlNet);
            delete *q;
          }
        }
        return false;
      }
    }
  }

  if (! added.empty())
  {
    for (EdgeNetLoop q(added); ! q.atEnd(); ++q) {      
      // append to the array now so we can maintain consistent state
      mEdgeAndNet.push_back(*q);
    }
  }
  return true;
}

// SHOULD ONLY BE CALLED ONCE PER SCHEDULE CALL
void ScheduleStimuli::update()
{
  mStimulated = false;
  if (! mInvalid)
  {
    // Walk the set of pod changes looking for edges
    for (EdgeNetLoop l(mEdgeAndNet); !l.atEnd() && !mStimulated; ++l) {
      // Get the change array entry for this node
      EdgeInfoNetPair* edgeNetPair = *l;
      EdgeDetectInfo* info = edgeNetPair->mInfo;
      const CarbonChangeType& changeEntry = *(info->mChgArrEntry);

      // Check which edge we need for this clock. If it matches the
      // change array entry, then we need to dump waves for this
      // group.
      switch(info->mEdgeStim) {
        case eBothEdges:
          mStimulated = (changeEntry != CARBON_CHANGE_NONE_MASK);
          break;

        case ePosEdge:
          mStimulated = ((changeEntry & CARBON_CHANGE_RISE_MASK) != 0);
          break;

        case eNegEdge:
          mStimulated = ((changeEntry & CARBON_CHANGE_FALL_MASK) != 0);
          break;
      }
    }

    // tristate clocks can change more than once during 1 time
    // slice. once if the enable changes and once if the external
    // world changes its value.
    if (! mStimulated && ! mNonPods.empty())
    {
      for (EdgeNetLoop p(mNonPods);
           ! mStimulated && (! p.atEnd()); ++p)
      {
        EdgeInfoNetPair* edgeNetPair = *p;
        EdgeDetectInfo* info = edgeNetPair->mInfo;
        UInt32 clkValWord;
        
        if (*(info->mChgArrEntry))
        {
          ShellNet* shlNet = edgeNetPair->mNet;
          
          switch(info->mEdgeStim)
          {
          case eBothEdges:
            mStimulated = true;
            break;
          case ePosEdge:
            shlNet->examine(&clkValWord, NULL, ShellNet::eIDrive, NULL);
            if (clkValWord == 1)
              mStimulated = true;
            break;
          case eNegEdge:
            shlNet->examine(&clkValWord, NULL, ShellNet::eIDrive, NULL);
            if (clkValWord == 0)
              mStimulated = true;
            break;
          } // switch
        } // if chg
      } // for
    } // if mNonPods
  } // if valid
}


void ScheduleStimuli::transferMaps(CarbonHookup*)
{
  UInt32 numEdges = mEdgeAndNet.size() + mNonPods.size() + 1;
  mChangeArray = CARBON_ALLOC_VEC(CarbonChangeType*, numEdges);
  memset(mChangeArray, 0, sizeof(CarbonChangeType*) * numEdges);
  UInt32 index = 0;
  
  if (! mEdgeAndNet.empty())
  {    
    for (EdgeNetLoop p(mEdgeAndNet);
         ! p.atEnd(); ++p)
    {
      EdgeInfoNetPair* netPair = *p;
      EdgeDetectInfo* info = netPair->mInfo;
      mChangeArray[index] = info->mChgArrEntry;
      ++index;
    }
  }

  for (EdgeNetLoop p(mNonPods);
       ! p.atEnd(); ++p)
  {
    EdgeInfoNetPair* netPair = *p;
    EdgeDetectInfo* info = netPair->mInfo;
    mChangeArray[index] = info->mChgArrEntry;
    ++index;
  }

  mTransferMaps = false;
}

bool ScheduleStimuli::operator<(const ScheduleStimuli& other) const
{
  return (*mSignature) < *(other.mSignature);
}

typedef CarbonValueChange<UInt32, BVOp> BitVectorVC;
typedef CarbonValueChange<UInt32, BVOpBitDiff> BitVectorVCBitDiff;

void CarbonExamineScheduler::NetTraitClosure::allocVCs(VCVec* vec, VCVec* groupVec, MiniMemPool* memPool)
{
  allocInitVCs(vec, groupVec, NULL, memPool);
}

CarbonValueChange<UInt8>* 
CarbonExamineScheduler::NetTraitClosure::allocBytesOnly(MiniMemPool* memPool, StorageVec* initVals)
{
  CarbonValueChange<UInt8>* vc = NULL;
  SInt32 index = 0;
  if (! mBytes.empty())
  {
    vc = CarbonValueChange<UInt8>::create(index, mBytes.size(),
                                          PodOp<UInt8>(), memPool);
    
    for (ByteVecLoop p(mBytes);
         ! p.atEnd(); ++p)
      vc->putModelPointer(index++, *p, initVals);
    
  }
  return vc;
}

void CarbonExamineScheduler::NetTraitClosure::allocInitVCs(VCVec* vec, VCVec* groupVec, StorageVec* initVals, MiniMemPool* memPool)
{
  CarbonValueChangeBase* vc = NULL;
  SInt32 index = 0;
  if (! mBytes.empty())
  {
    vc = CarbonValueChange<UInt8>::create(index, mBytes.size(),
                                          PodOp<UInt8>(), memPool);
    
    for (ByteVecLoop p(mBytes);
         ! p.atEnd(); ++p)
      vc->putModelPointer(index++, *p, initVals);
    
    vec->push_back(vc);
  }
  
  if (! mShorts.empty())
  {
    vc = CarbonValueChange<UInt16>::create(index, mShorts.size(),
                                           PodOp<UInt16>(), memPool);
    
    for (ShortVecLoop p(mShorts);
         ! p.atEnd(); ++p)
      vc->putModelPointer(index++, *p, initVals);

    vec->push_back(vc);
  }
  
  if (! mLongs.empty())
  {
    vc = CarbonValueChange<UInt32>::create(index, mLongs.size(),
                                           PodOp<UInt32>(), memPool);
    for (LongVecLoop p(mLongs);
         ! p.atEnd(); ++p)
      vc->putModelPointer(index++, *p, initVals);

    vec->push_back(vc);
  }
  
  if (! mLLongs.empty())
  {
    vc = CarbonValueChange<UInt64>::create(index, mLLongs.size(),
                                           PodOp<UInt64>(), memPool);
    
    for (LLongVecLoop p(mLLongs);
         ! p.atEnd(); ++p)
      vc->putModelPointer(index++, *p, initVals);
    
    vec->push_back(vc);
  }
  
  if (! mHugeVecs.empty())
  {
    for (SizeHugeVecMap::SortedLoop p = mHugeVecs.loopSorted();
         ! p.atEnd(); ++p)
    {
      UInt32 numWords = p.getKey();
      LongVec& ptrArr = p.getValue();
      BitVectorVC* bvc = BitVectorVC::create(index, ptrArr.size(), 
                                             BVOp(numWords),
                                             memPool);
      vc = bvc;
      
      for (LongVecLoop q(ptrArr); ! q.atEnd(); ++q)
        vc->putModelPointer(index++, *q, initVals);
      
      vec->push_back(vc);
    }
  }

  // Now do the same thing, but for the group storage pointers.  The
  // ObjOp template argument for the value change object is slightly
  // different here, because it needs to report which bits of the
  // storage have changed.  Using this ObjOp causes the
  // CarbonValueChange class to allocate double the required storage
  // for shadow values.  The second half of that buffer is used for
  // saving a mask of the bits that differed during comparison of the
  // model and shadow values.
  //
  // This means that the mask location is always a fixed offset from
  // the corresponding shadow location - the number of offsets tracked
  // by the CarbonValueChange object.  That offset is passed to the
  // ObjOp constructor so it can update the mask on comparisons.
  //
  // First, a sanity check.  Group storage pointers are only supported
  // for waveform dumping, not net change callbacks.  If we have any
  // of them, the group vector pointer must be valid, and the initial
  // value pointer must be NULL.
  UInt32 numGroupEntries = mByteGroups.size() + mShortGroups.size() + mLongGroups.size() + mLLongGroups.size() + mHugeVecGroups.size();
  INFO_ASSERT((numGroupEntries == 0) || ((groupVec != NULL) && (initVals == NULL)), "group sanity check failed");

  // Allocate, resetting the index, because we're working with the group vector now.
  index = 0;
  if (! mByteGroups.empty())
  {
    UInt32 size = mByteGroups.size();
    vc = CarbonValueChange<UInt8, PodOpBitDiff<UInt8> >::create(index, size,
                                                                PodOpBitDiff<UInt8>(size), memPool);
    
    for (ByteVecLoop p(mByteGroups);
         ! p.atEnd(); ++p)
      vc->putModelPointer(index++, *p, NULL);
    
    groupVec->push_back(vc);
  }
  
  if (! mShortGroups.empty())
  {
    UInt32 size = mShortGroups.size();
    vc = CarbonValueChange<UInt16, PodOpBitDiff<UInt16> >::create(index, size,
                                                                  PodOpBitDiff<UInt16>(size), memPool);
    
    for (ShortVecLoop p(mShortGroups);
         ! p.atEnd(); ++p)
      vc->putModelPointer(index++, *p, NULL);

    groupVec->push_back(vc);
  }
  
  if (! mLongGroups.empty())
  {
    UInt32 size = mLongGroups.size();
    vc = CarbonValueChange<UInt32, PodOpBitDiff<UInt32> >::create(index, size,
                                                                  PodOpBitDiff<UInt32>(size), memPool);
    for (LongVecLoop p(mLongGroups);
         ! p.atEnd(); ++p)
      vc->putModelPointer(index++, *p, NULL);

    groupVec->push_back(vc);
  }
  
  if (! mLLongGroups.empty())
  {
    UInt32 size = mLLongGroups.size();
    vc = CarbonValueChange<UInt64, PodOpBitDiff<UInt64> >::create(index, size,
                                                                  PodOpBitDiff<UInt64>(size), memPool);
    
    for (LLongVecLoop p(mLLongGroups);
         ! p.atEnd(); ++p)
      vc->putModelPointer(index++, *p, NULL);
    
    groupVec->push_back(vc);
  }
  
  if (! mHugeVecGroups.empty())
  {
    for (SizeHugeVecMap::SortedLoop p = mHugeVecGroups.loopSorted();
         ! p.atEnd(); ++p)
    {
      UInt32 numWords = p.getKey();
      LongVec& ptrArr = p.getValue();

      UInt32 size = ptrArr.size();
      // For the PodOpBitDiff objects above, we're dealing with arrays
      // of single PODs.  For bitvectors, each entry may span multiple
      // UInt32s, so scale the size before passing it to the
      // BVOpBitDiff, because it interprets it as a UInt32 array
      // offset.
      UInt32 offset = size * numWords;
      BitVectorVCBitDiff* bvc = BitVectorVCBitDiff::create(index, size,
                                                           BVOpBitDiff(numWords, offset), memPool);
      vc = bvc;
      
      for (LongVecLoop q(ptrArr); ! q.atEnd(); ++q)
        vc->putModelPointer(index++, *q, NULL);
      
      groupVec->push_back(vc);
    }
  }
}

void CarbonExamineScheduler::NetTraitClosure::addNet(const ShellNet* net)
{
  ShellNet::Traits traits;
  net->getTraits(&traits);
  size_t numBytes = sNumBytes(traits.getWidth());
  size_t numWords = 0;
  ST_ASSERT(numBytes > 0, net->getNameAsLeaf());
  ST_ASSERT(! traits.isTristate(), net->getNameAsLeaf());
  // We're in big trouble if we got here with a net that represents a
  // bitselect of a POD.  Those should be handled by addGroup().
  ST_ASSERT(traits.getPodBitsel() == -1, net->getNameAsLeaf());
  
  switch(numBytes)
  {
  case 1:
    mBytes.push_back(traits.getByte());
    break;
  case 2:
    mShorts.push_back(traits.getShort());
    break;
  case 4:
    mLongs.push_back(traits.getLongPtr());
    break;
  case 8:
    mLLongs.push_back(traits.getLLong());
    break;
  default:
    ST_ASSERT(numBytes > 8, net->getNameAsLeaf());
    numWords = sNumWords(traits.getWidth());
    mHugeVecs[numWords].push_back(traits.getLongPtr());
    break;
  }
}

void CarbonExamineScheduler::NetTraitClosure::addGroup(const void* storage, UInt32 width)
{
  size_t numBytes = sNumBytes(width);
  size_t numWords = 0;
  INFO_ASSERT(numBytes > 0, "Storage group with 0 bytes");
  
  switch(numBytes)
  {
  case 1:
    mByteGroups.push_back(static_cast<const UInt8*>(storage));
    break;
  case 2:
    mShortGroups.push_back(static_cast<const UInt16*>(storage));
    break;
  case 4:
    mLongGroups.push_back(static_cast<const UInt32*>(storage));
    break;
  case 8:
    mLLongGroups.push_back(static_cast<const UInt64*>(storage));
    break;
  default:
    INFO_ASSERT(numBytes > 8, "Expecting more than 8 bytes");
    numWords = sNumWords(width);
    mHugeVecGroups[numWords].push_back(static_cast<const UInt32*>(storage));
    break;
  }
}


CarbonExamineScheduler::CarbonExamineScheduler(CarbonHookup* hookup) 
  : mHookup(hookup)
{}

CarbonExamineScheduler::~CarbonExamineScheduler()
{
  SigStimMap::iterator e = mSigToStim.end();
  for (SigStimMap::iterator p = mSigToStim.begin(); p != e; 
       ++p)
  {
    ScheduleStimuli* stim = p->second;
    delete stim;
  }

}


void CarbonExamineScheduler::getNetInfo(ShellNet* theNet, NetInfo* netInfo)
{
  ShellNet::Traits netTraits;
  theNet->getTraits(&netTraits);
  
  netInfo->mIsNonPod = true;
  netInfo->mIsConstant = false;
  {
    /*
      gcc 2.95 doesn't like assoc->mCarbonNet->castConstant(). It
      returns non-null when it shouldn't. I think this only happens
      with shared libraries, though. Anyway, doing the following
      seems to fix it.
    */
    const ShellNetConstant* theConstant = netTraits.getConstant();
    if (theConstant != NULL)
      netInfo->mIsConstant = true;
  }
  
  // copy POD flags
  netInfo->mIsNonPod = netTraits.isNonPod();
  netInfo->mPodBitsel = netTraits.getPodBitsel();
  netInfo->mHasInputSemantics = netTraits.hasInputSemantics();
}

ScheduleStimuli*  CarbonExamineScheduler::getScheduleStimuli(ShellNet* theNet, 
                                                             NetInfo* netInfo)
{

  getNetInfo(theNet, netInfo);

  if (netInfo->isConstant())
    return NULL;

  ScheduleStimuli* schedStim = NULL;
  const STSymbolTableNode* tableNode = 
    theNet->getNameAsLeaf();
  const IODB* iodb = mHookup->getDB();
  const CarbonModel* model = mHookup->getCarbonModel();


  if (! netInfo->isNonPOD())
  {
    bool isInput = netInfo->hasInputSemantics();
    if (! isInput)
      /*    
            THIS IS ONLY TEMPORARY! For the structure api, we cannot
            rely on a cast to an input. Once the structure api is no
            longer supported the following is more correct, especially
            in considering force and deposit
            
            if (assoc->mCarbonNet->castInput() != NULL)
            putInCheckList(assoc);
      */
      // The structure api forces me to check the iodb
      isInput = iodb->isPrimaryInput(tableNode) || iodb->isPrimaryBidirect(tableNode);

    ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(tableNode);
    SCHSignature* signature = bomdata->getSCHSignature();
    
    bool isNotSchedulable = (signature == NULL) || isInput ||
      model->isDepositable(theNet) ||
      model->isForcible(theNet);
    
    if (! isNotSchedulable)
    {
      ST_ASSERT(signature, tableNode);
      const SCHScheduleMask* transMask = signature->getTransitionMask();
      schedStim = NULL;

      {
        SigStimMap::iterator p = mSigToStim.find(signature);
        if (p != mSigToStim.end())
          schedStim = p->second;
      }
      
      if (schedStim)
      {
        if (schedStim->isInvalid())
          schedStim = NULL;
      }
      else
      {
        schedStim = new ScheduleStimuli(signature);
        mSigToStim[signature] = schedStim;
        
        bool hasInputSched = false;
        SCHScheduleMask::UnsortedEvents p;
        const SCHEvent* event;
        ScheduleStimuli::EventList clkEvents;
        for (p = transMask->loopEvents();
             ! p.atEnd() && ! hasInputSched; ++p)
        {
          event = *p;
          hasInputSched = event->isPrimaryInput();
          if (! hasInputSched && event->isClockEvent())
            clkEvents.push_back(event);
        }
        
        if (hasInputSched || clkEvents.empty())
        {
          schedStim->setInvalid();
          schedStim = NULL;
        }
        else if (!clkEvents.empty())
        {
          if (! schedStim->addEventNodes(clkEvents, mHookup))
            schedStim = NULL;
        }
      }
    }
  }
  return schedStim;
}

bool CarbonExamineScheduler::sSortByNoTriNumBytes(const ShellNet* a1, const ShellNet* a2)
{
  int tristateNess = int(a1->isTristate()) - int(a2->isTristate());
  int a1Width = a1->getBitWidth();
  int a2Width = a2->getBitWidth();
  SInt32 bitDifference = a1Width - a2Width;
  
  bool lessThan = false;
  if (tristateNess == 0)
    // both have the same tristate attribute
    lessThan = bitDifference < 0;
  else if (tristateNess < 0)
    // a1 is not a tri, a2 is
    lessThan = true;
  else /* > 0 */
    // a1 is a tri, a2 is not
    lessThan = false;
  
  return lessThan;
}

MiniMemPool::MiniMemPool()
{
  mData = NULL;
  mFreeStorage = 0;
  mNextAlloc = NULL;
}

#define MINI_MEM_POOL_BLOCK_SIZE 65536

MiniMemPool::~MiniMemPool() {
  for (UInt32 i = 0; i < mAllocedData.size(); ++i)
    carbonmem_free(mAllocedData[i]);
}

void* MiniMemPool::alloc(UInt32 size)
{
  if (size <= MINI_MEM_POOL_BLOCK_SIZE)
  {
    size += 3;
    size &= ~0x3;
    
    if (size > mFreeStorage)
    {
      mData = (char*) carbonmem_malloc(MINI_MEM_POOL_BLOCK_SIZE);
      mFreeStorage = MINI_MEM_POOL_BLOCK_SIZE;
      mNextAlloc = mData;
      mAllocedData.push_back(mData);
    }
    mFreeStorage -= size;
    void* ret = (void*) mNextAlloc;
    mNextAlloc += size;
    return ret;
  }
  
  char* ret = (char*) carbonmem_malloc(size);
  mAllocedData.push_back(ret);
  return (void*) ret;
}
