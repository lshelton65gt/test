// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "shell/carbon_type_create.h"

#include "shell/CarbonScalarInput.h"
#include "shell/CarbonTristateScalar.h"
#include "shell/CarbonTristateScalarInput.h"

#include "shell/CarbonVector.h"
#include "shell/CarbonVectorInput.h"

#include "shell/CarbonTristateVector.h"
#include "shell/CarbonTristateVectorInput.h"

#include "shell/ShellMemory.h"
#include "shell/CarbonForceNet.h"
#include "shell/ShellMemoryCreateInfo.h"

ShellNet* CarbonScalarCreate(UInt8* addr)
{
  return new CarbonScalar(addr);
}

ShellNet* CarbonScalarInputCreate(UInt8* addr)
{
  return new CarbonScalarInput(addr);
}

ShellNet* CarbonTristateScalarCreate(UInt8* idata, UInt8* idrive)
{
  return new CarbonTristateScalar(idata, idrive);
}

ShellNet* CarbonTristateScalarSCreate(SInt8* idata, SInt8* idrive)
{
  return new CarbonTristateScalar((UInt8*) idata, (UInt8*) idrive);
}

ShellNet* CarbonTristateScalarInputCreate(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive)
{
   return new CarbonTristateScalarInput(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateScalarInputSCreate(SInt8* idata, SInt8* idrive, SInt8* xdata, SInt8* xdrive)
{
   return new CarbonTristateScalarInput((UInt8*) idata, (UInt8*) idrive, (UInt8*) xdata, (UInt8*) xdrive);
}

ShellNet* CarbonTristateScalarBidiCreate(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive)
{
   return new CarbonTristateScalar(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateScalarBidiSCreate(SInt8* idata, SInt8* idrive, SInt8* xdata, SInt8* xdrive)
{
   return new CarbonTristateScalar((UInt8*) idata, (UInt8*) idrive, (UInt8*) xdata, (UInt8*) xdrive);
}

ShellNet* CarbonTristateScalarDepInputCreate(UInt8* idata, UInt8* idrive)
{
  return new CarbonTristateScalarInput(idata, idrive);
}

ShellNet* CarbonTristateScalarDepInputSCreate(SInt8* idata, SInt8* idrive)
{
  return new CarbonTristateScalarInput((UInt8*) idata, (UInt8*) idrive);
}

ShellNet* CarbonVector1Create(UInt8* addr)
{
  return new CarbonVector1(addr);
}

ShellNet* CarbonVector1InputCreate(UInt8* addr)
{
  return new CarbonVector1Input(addr);
}

ShellNet* CarbonTristateVector1Create(UInt8* idata, UInt8* idrive)
{
  return new CarbonTristateVector1(idata, idrive);
}

ShellNet* CarbonTristateVector1SCreate(SInt8* idata, SInt8* idrive)
{
  return new CarbonTristateVector1((UInt8*) idata, (UInt8*) idrive);
}

ShellNet* CarbonTristateVector1InputCreate(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive)
{
  return new CarbonTristateVector1Input(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateVector1InputSCreate(SInt8* idata, SInt8* idrive, SInt8* xdata, SInt8* xdrive)
{
  return new CarbonTristateVector1Input((UInt8*) idata, (UInt8*) idrive, (UInt8*) xdata, (UInt8*) xdrive);
}

ShellNet* CarbonTristateVector1BidiCreate(UInt8* idata, UInt8* idrive, UInt8* xdata, UInt8* xdrive)
{
  return new CarbonTristateVector1(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateVector1BidiSCreate(SInt8* idata, SInt8* idrive, SInt8* xdata, SInt8* xdrive)
{
  return new CarbonTristateVector1((UInt8*) idata, (UInt8*) idrive, (UInt8*) xdata, (UInt8*) xdrive);
}

ShellNet* CarbonTristateVector1DepInputCreate(UInt8* idata, UInt8* idrive)
{
  return new CarbonTristateVector1Input(idata, idrive);
}

ShellNet* CarbonTristateVector1DepInputSCreate(SInt8* idata, SInt8* idrive)
{
  return new CarbonTristateVector1Input((UInt8*) idata, (UInt8*) idrive);
}

ShellNet* CarbonVector2Create(UInt16* addr)
{
  return new CarbonVector2(addr);
}

ShellNet* CarbonVector2InputCreate(UInt16* addr)
{
  return new CarbonVector2Input(addr);
}

ShellNet* CarbonTristateVector2Create(UInt16* idata, UInt16* idrive)
{
  return new CarbonTristateVector2(idata, idrive);
}

ShellNet* CarbonTristateVector2SCreate(SInt16* idata, SInt16* idrive)
{
  return new CarbonTristateVector2((UInt16*) idata, (UInt16*) idrive);
}

ShellNet* CarbonTristateVector2InputCreate(UInt16* idata, UInt16* idrive, UInt16* xdata, UInt16* xdrive)
{
  return new CarbonTristateVector2Input(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateVector2InputSCreate(SInt16* idata, SInt16* idrive, SInt16* xdata, SInt16* xdrive)
{
  return new CarbonTristateVector2Input((UInt16*) idata, (UInt16*) idrive, (UInt16*) xdata, (UInt16*) xdrive);
}

ShellNet* CarbonTristateVector2BidiCreate(UInt16* idata, UInt16* idrive, UInt16* xdata, UInt16* xdrive)
{
  return new CarbonTristateVector2(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateVector2BidiSCreate(SInt16* idata, SInt16* idrive, SInt16* xdata, SInt16* xdrive)
{
  return new CarbonTristateVector2((UInt16*) idata, (UInt16*) idrive, (UInt16*) xdata, (UInt16*) xdrive);
}

ShellNet* CarbonTristateVector2DepInputCreate(UInt16* idata, UInt16* idrive)
{
  return new CarbonTristateVector2Input(idata, idrive);
}

ShellNet* CarbonTristateVector2DepInputSCreate(SInt16* idata, SInt16* idrive)
{
  return new CarbonTristateVector2Input((UInt16*) idata, (UInt16*) idrive);
}

ShellNet* CarbonVector4Create(UInt32* addr)
{
  return new CarbonVector4(addr);
}

ShellNet* CarbonVector4InputCreate(UInt32* addr)
{
  return new CarbonVector4Input(addr);
}

ShellNet* CarbonTristateVector4Create(UInt32* idata, UInt32* idrive)
{
  return new CarbonTristateVector4(idata, idrive);
}

ShellNet* CarbonTristateVector4SCreate(SInt32* idata, SInt32* idrive)
{
  return new CarbonTristateVector4((UInt32*) idata, (UInt32*) idrive);
}

ShellNet* CarbonTristateVector4InputCreate(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive)
{
  return new CarbonTristateVector4Input(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateVector4InputSCreate(SInt32* idata, SInt32* idrive, SInt32* xdata, SInt32* xdrive)
{
  return new CarbonTristateVector4Input((UInt32*) idata, (UInt32*) idrive, (UInt32*) xdata, (UInt32*) xdrive);
}

ShellNet* CarbonTristateVector4BidiCreate(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive)
{
  return new CarbonTristateVector4(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateVector4BidiSCreate(SInt32* idata, SInt32* idrive, SInt32* xdata, SInt32* xdrive)
{
  return new CarbonTristateVector4((UInt32*) idata, (UInt32*) idrive, (UInt32*) xdata, (UInt32*) xdrive);
}

ShellNet* CarbonTristateVector4DepInputCreate(UInt32* idata, UInt32* idrive)
{
  return new CarbonTristateVector4Input(idata, idrive);
}

ShellNet* CarbonTristateVector4DepInputSCreate(SInt32* idata, SInt32* idrive)
{
  return new CarbonTristateVector4Input((UInt32*) idata, (UInt32*) idrive);
}

ShellNet* CarbonVector8Create(UInt64* addr)
{
  return new CarbonVector8(addr);
}

ShellNet* CarbonVector8InputCreate(UInt64* addr)
{
  return new CarbonVector8Input(addr);
}

ShellNet* CarbonTristateVector8Create(UInt64* idata, UInt64* idrive)
{
  return new CarbonTristateVector8(idata, idrive);
}

ShellNet* CarbonTristateVector8SCreate(SInt64* idata, SInt64* idrive)
{
  return new CarbonTristateVector8((UInt64*) idata, (UInt64*) idrive);
}

ShellNet* CarbonTristateVector8InputCreate(UInt64* idata, UInt64* idrive, UInt64* xdata, UInt64* xdrive)
{
  return new CarbonTristateVector8Input(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateVector8InputSCreate(SInt64* idata, SInt64* idrive, SInt64* xdata, SInt64* xdrive)
{
  return new CarbonTristateVector8Input((UInt64*) idata, (UInt64*) idrive, (UInt64*) xdata, (UInt64*) xdrive);
}

ShellNet* CarbonTristateVector8BidiCreate(UInt64* idata, UInt64* idrive, UInt64* xdata, UInt64* xdrive)
{
  return new CarbonTristateVector8(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateVector8BidiSCreate(SInt64* idata, SInt64* idrive, SInt64* xdata, SInt64* xdrive)
{
  return new CarbonTristateVector8((UInt64*) idata, (UInt64*) idrive, (UInt64*) xdata, (UInt64*) xdrive);
}

ShellNet* CarbonTristateVector8DepInputCreate(UInt64* idata, UInt64* idrive)
{
  return new CarbonTristateVector8Input(idata, idrive);
}

ShellNet* CarbonTristateVector8DepInputSCreate(SInt64* idata, SInt64* idrive)
{
  return new CarbonTristateVector8Input((UInt64*) idata, (UInt64*) idrive);
}

ShellNet* CarbonVectorACreate(UInt32* addr)
{
  return new CarbonVectorA(addr);
}

ShellNet* CarbonVectorASCreate(UInt32* addr)
{
  return new CarbonVectorAS(addr);
}

ShellNet* CarbonVectorAInputCreate(UInt32* addr)
{
  return new CarbonVectorAInput(addr);
}

ShellNet* CarbonVectorASInputCreate(UInt32* addr)
{
  return new CarbonVectorASInput(addr);
}

ShellNet* CarbonTristateVectorACreate(UInt32* idata, UInt32* idrive)
{
  return new CarbonTristateVectorA(idata, idrive);
}

ShellNet* CarbonTristateVectorASCreate(UInt32* idata, UInt32* idrive)
{
  return new CarbonTristateVectorAS(idata, idrive);
}

ShellNet* CarbonTristateVectorAInputCreate(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive)
{
  return new CarbonTristateVectorAInput(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateVectorASInputCreate(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive)
{
  return new CarbonTristateVectorASInput(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateVectorABidiCreate(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive)
{
  return new CarbonTristateVectorA(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateVectorASBidiCreate(UInt32* idata, UInt32* idrive, UInt32* xdata, UInt32* xdrive)
{
  return new CarbonTristateVectorAS(idata, idrive, xdata, xdrive);
}

ShellNet* CarbonTristateVectorADepInputCreate(UInt32* idata, UInt32* idrive)
{
  return new CarbonTristateVectorAInput(idata, idrive);
}

ShellNet* CarbonTristateVectorASDepInputCreate(UInt32* idata, UInt32* idrive)
{
  return new CarbonTristateVectorASInput(idata, idrive);
}

ShellNet* CarbonForceNetCreate(ShellNet* storage, ShellNet* forceMask)
{
  return new CarbonForceNet(storage, forceMask);
}

ShellNet* CarbonShellMemory64x8Create2(ShellMemoryCreateInfo* memClosure, UInt8* mem)
{
  if (memClosure->getVersion() > eShellMemoryCreateInfoLatest)
    // No message context here.
    // The shell will spew out an appropriate 'incompatible' message. 
    return NULL;
  
  ShellMemory64x8* ret = new ShellMemory64x8(memClosure->getMsb(), memClosure->getLsb(), 
                                             memClosure->getLeftAddr(), memClosure->getRightAddr(), mem);
  ret->putCallbackArray(memClosure->getCallbackArray());
  return ret;
}

ShellNet* CarbonShellMemory64x16Create2(ShellMemoryCreateInfo* memClosure, UInt16* mem)
{
  if (memClosure->getVersion() > eShellMemoryCreateInfoLatest)
    // No message context here.
    // The shell will spew out an appropriate 'incompatible' message. 
    return NULL;

  ShellMemory64x16* ret = new ShellMemory64x16(memClosure->getMsb(), memClosure->getLsb(), 
                                              memClosure->getLeftAddr(), memClosure->getRightAddr(), mem);
  ret->putCallbackArray(memClosure->getCallbackArray());
  return ret;
}

ShellNet* CarbonShellMemory64x32Create2(ShellMemoryCreateInfo* memClosure, UInt32* mem)
{
  if (memClosure->getVersion() > eShellMemoryCreateInfoLatest)
    // No message context here.
    // The shell will spew out an appropriate 'incompatible' message. 
    return NULL;

  ShellMemory64x32* ret = new ShellMemory64x32(memClosure->getMsb(), memClosure->getLsb(), 
                                               memClosure->getLeftAddr(), memClosure->getRightAddr(), mem);
  ret->putCallbackArray(memClosure->getCallbackArray());
  return ret;
}

ShellNet* CarbonShellMemory64x64Create2(ShellMemoryCreateInfo* memClosure, UInt64* mem)
{
  if (memClosure->getVersion() > eShellMemoryCreateInfoLatest)
    // No message context here.
    // The shell will spew out an appropriate 'incompatible' message. 
    return NULL;
  
  ShellMemory64x64* ret = new ShellMemory64x64(memClosure->getMsb(), memClosure->getLsb(), 
                                               memClosure->getLeftAddr(), memClosure->getRightAddr(), mem);
  ret->putCallbackArray(memClosure->getCallbackArray());
  return ret;
}

ShellNet* CarbonShellMemory64xACreate2(ShellMemoryCreateInfo* memClosure, void* mem)
{
  if (memClosure->getVersion() > eShellMemoryCreateInfoLatest)
    // No message context here.
    // The shell will spew out an appropriate 'incompatible' message. 
    return NULL;

  // The row storage pointer function was introduced in version 2 of
  // ShellMemoryCreateInfo.  If we have an older one, we have to be
  // careful.  The instance of the ShellMemoryCreateInfo is actually
  // created by the model's generated code.  Since that generated code
  // didn't know about the new function, it didn't set it.
  //
  // The current constructor for ShellMemoryCreateInfo does initialize
  // all pointers to NULL, but we can't take advantage of that here.
  // Since the constructor is inlined in the header, the generated
  // code is actually calling the old version of the constructor that
  // didn't know to initialize the new pointer!
  //
  // The only safe thing to do is to explicitly use NULL for the
  // function pointer unless the version is new enough.
  CarbonMemRowStoragePtrFn rowStoragePtrFn = NULL;
  if (memClosure->getVersion() >= eShellMemoryCreateInfoVersion2) {
    rowStoragePtrFn = memClosure->getRowStoragePtrFn();
  }

  ShellMemory64xA* ret = new ShellMemory64xA(memClosure->getMsb(), memClosure->getLsb(), 
                                             memClosure->getLeftAddr(), memClosure->getRightAddr(), 
                                             memClosure->getReadRowFn(), memClosure->getWriteRowFn(),
                                             memClosure->getReadRowWordFn(), memClosure->getWriteRowWordFn(),
                                             memClosure->getReadRowRangeFn(), memClosure->getWriteRowRangeFn(),
                                             rowStoragePtrFn,
                                             mem);
  ret->putCallbackArray(memClosure->getCallbackArray());
  return ret;
}

ShellNet* CarbonShellSparseMemory32x8Create2(ShellMemoryCreateInfo* memClosure, void* mem)
{
  if (memClosure->getVersion() > eShellMemoryCreateInfoLatest)
    // No message context here.
    // The shell will spew out an appropriate 'incompatible' message. 
    return NULL;

  ShellSparseMemory32x8* ret = new ShellSparseMemory32x8(memClosure->getMsb(), memClosure->getLsb(), 
                                             memClosure->getLeftAddr(), memClosure->getRightAddr(), mem);
  ret->putCallbackArray(memClosure->getCallbackArray());
  return ret;
}

ShellNet* CarbonShellSparseMemory32x16Create2(ShellMemoryCreateInfo* memClosure, void* mem)
{
  if (memClosure->getVersion() > eShellMemoryCreateInfoLatest)
    // No message context here.
    // The shell will spew out an appropriate 'incompatible' message. 
    return NULL;

  ShellSparseMemory32x16* ret = new ShellSparseMemory32x16(memClosure->getMsb(), memClosure->getLsb(), 
                                               memClosure->getLeftAddr(), memClosure->getRightAddr(), mem);
  ret->putCallbackArray(memClosure->getCallbackArray());
  return ret;
}

ShellNet* CarbonShellSparseMemory32x32Create2(ShellMemoryCreateInfo* memClosure, void* mem)
{
  if (memClosure->getVersion() > eShellMemoryCreateInfoLatest)
    // No message context here.
    // The shell will spew out an appropriate 'incompatible' message. 
    return NULL;

  ShellSparseMemory32x32* ret = new ShellSparseMemory32x32(memClosure->getMsb(), memClosure->getLsb(), 
                                               memClosure->getLeftAddr(), memClosure->getRightAddr(), mem);
  ret->putCallbackArray(memClosure->getCallbackArray());
  return ret;
}

ShellNet* CarbonShellSparseMemory32x64Create2(ShellMemoryCreateInfo* memClosure, void* mem)
{
  if (memClosure->getVersion() > eShellMemoryCreateInfoLatest)
    // No message context here.
    // The shell will spew out an appropriate 'incompatible' message. 
    return NULL;

  ShellSparseMemory32x64* ret = new ShellSparseMemory32x64(memClosure->getMsb(), memClosure->getLsb(), 
                                               memClosure->getLeftAddr(), memClosure->getRightAddr(), mem);
  ret->putCallbackArray(memClosure->getCallbackArray());
  return ret;
}


/* obsolete memory functions - here for compatibility. */
ShellNet* CarbonShellMemory64x8Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, UInt8* mem)
{
  return new ShellMemory64x8(msb, lsb, hiAddr, loAddr, mem);
}

ShellNet* CarbonShellMemory64x16Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, UInt16* mem)
{
  return new ShellMemory64x16(msb, lsb, hiAddr, loAddr, mem);
}

ShellNet* CarbonShellMemory64x32Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, UInt32* mem)
{
  return new ShellMemory64x32(msb, lsb, hiAddr, loAddr, mem);
}

ShellNet* CarbonShellMemory64x64Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, UInt64* mem)
{
  return new ShellMemory64x64(msb, lsb, hiAddr, loAddr, mem);
}

ShellNet* CarbonShellMemory64xACreate(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, 
                                      CarbonMemReadRowFn readRow, CarbonMemWriteRowFn writeRow,
                                      CarbonMemReadRowWordFn readRowWord, CarbonMemWriteRowWordFn writeRowWord,
                                      CarbonMemReadRowRangeFn readRowRange, CarbonMemWriteRowRangeFn writeRowRange,
                                      void* mem)
{
  // This create call is used by older Carbon models.  Their memories
  // don't support returning a pointer to row storage for a particular
  // address, so pass NULL for that function pointer.  It simply means
  // that the shell can't be bypassed for wave dumping of memories,
  // but that's only useful with newer models that support visibility
  // anyway.
  return new ShellMemory64xA(msb, lsb, hiAddr, loAddr, readRow, writeRow, readRowWord, writeRowWord, readRowRange, writeRowRange, NULL, mem);
}


/* Sparse Memory types */
ShellNet* CarbonShellSparseMemory32x8Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, void* mem)
{
  return new ShellSparseMemory32x8(msb, lsb, hiAddr, loAddr, mem);
}

ShellNet* CarbonShellSparseMemory32x16Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, void* mem)
{
  return new ShellSparseMemory32x16(msb, lsb, hiAddr, loAddr, mem);
}

ShellNet* CarbonShellSparseMemory32x32Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, void* mem)
{
  return new ShellSparseMemory32x32(msb, lsb, hiAddr, loAddr, mem);
}

ShellNet* CarbonShellSparseMemory32x64Create(SInt32 msb, SInt32 lsb, SInt64 hiAddr, SInt64 loAddr, void* mem)
{
  return new ShellSparseMemory32x64(msb, lsb, hiAddr, loAddr, mem);
}
