// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file 
  Implements minimal runtime functions for error reporting from inside the design.
  This code can only be linked into the design, and should not be linked with cbuild
  or any of the other utilities.
*/

#include "util/CarbonPlatform.h"
#include "util/ShellMsgContext.h"
#include "shell/carbon_shelltypes.h"
#include "shell/ShellGlobal.h"


#include "util/CarbonRunTime.h"


// Report an out-of-bounds error to OBJECT[INDEX].  The declared bounds and contextMsg
// provide additional information to assist in identifying the access.
void
carbon_oob_access (const char*contextMsg, const char* object, const ConstantRange& bounds, SInt32 index)
{
  MsgContext* msg = ShellGlobal::getProgErrMsgr ();

  ConstantRange r (index,index);
  r.denormalize (&bounds, false); // Translate back into declared bounds access..

  msg->SHLOutOfBoundsAccess (contextMsg, r.getLsb (), object, bounds.getMsb (), bounds.getLsb ());
}

// Report an out-of-bounds error to a memory at [INDEX].  The declared bounds and contextMsg
// provide additional information to assist in identifying the access.
void
carbon_oob_memory_access (const char*contextMsg, const char* object, const ConstantRange& bounds, SInt32 index)
{
  MsgContext* msg = ShellGlobal::getProgErrMsgr ();

  // Memories are never normalized, so no denorm is required.

  msg->SHLOutOfBoundsAccess (contextMsg, index, object, bounds.getMsb (), bounds.getLsb ());
}

