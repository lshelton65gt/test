// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/CarbonNetIdent.h"
#include "shell/ShellNet.h"
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STSymbolTable.h"
#include "shell/CarbonDBRead.h"
#include "shell/CarbonModel.h"
#include "util/UtConv.h"
#include "util/ShellMsgContext.h"


CarbonNetIdent::AssignContext::AssignContext(ModeT mode) : 
  ExprAssignContext(),
  mMode(mode),
  mBitIndex(0),
  mRangeLength(0),
  mStatus(eCarbon_OK)
{}

//! normal destructor
CarbonNetIdent::AssignContext::~AssignContext() 
{}

CarbonNetIdent::AssignContext::ModeT 
CarbonNetIdent::AssignContext::getMode() const {
  return mMode;
}

void CarbonNetIdent::AssignContext::updateStatus(CarbonStatus status)
{
  if (mStatus != eCarbon_ERROR)
    mStatus = status;
}

CarbonStatus CarbonNetIdent::AssignContext::getStatus() const
{
  return mStatus;
}

ExprAssignContext* CarbonNetIdent::AssignContext::copy() const
{
  AssignContext* copyMe = new AssignContext(mMode);
  internalCopy(copyMe);
  return copyMe;
}

void CarbonNetIdent::AssignContext::putIndexLength(size_t index, size_t length)
{
  mBitIndex = index;
  mRangeLength = length;
}

void CarbonNetIdent::AssignContext::getIndexLength(size_t* index, size_t* length)
{
  *index = mBitIndex;
  *length = mRangeLength;
}

bool CarbonNetIdent::AssignContext::isActiveRange() const
{
  return mRangeLength != 0;
}

void CarbonNetIdent::AssignContext::chopIndexLength(size_t numBits)
{
  size_t subtractor = std::min(numBits, mRangeLength);
  mRangeLength -= subtractor;
  mBitIndex += subtractor;
}

CarbonNetIdent::EvalContext::EvalContext(ModeT mode) : 
  ExprEvalContext(), mMode(mode)
{}

CarbonNetIdent::EvalContext::~EvalContext() 
{}

void CarbonNetIdent::EvalContext::setMode(ModeT mode) {
  mMode = mode;
}

CarbonNetIdent::EvalContext::ModeT 
CarbonNetIdent::EvalContext::getMode() const {
  return mMode;
}

void CarbonNetIdent::EvalContext::setBitWidth(int bitWidth)
{
  getValueRef().resize(bitWidth);
  getDriveRef().resize(bitWidth);
}

ExprEvalContext* CarbonNetIdent::EvalContext::copy() const
{
  EvalContext* copyMe = new EvalContext(mMode);
  internalCopy(copyMe);
  return copyMe;
}

CarbonNetIdent::CarbonNetIdent(ShellNet* net, CarbonModel* model, const DynBitVector&, bool isSigned, UInt32 size) :
  CarbonIdent(isSigned, size), mShellNet(net), mCarbonModel(model)
{
  UInt32 bitWidth = mShellNet->getBitWidth();
  ST_ASSERT(size == bitWidth, net->getNameAsLeaf());
}

CarbonNetIdent::~CarbonNetIdent()
{}

const ShellNet* CarbonNetIdent::getShellNet() const
{
  return mShellNet;
}

const char* CarbonNetIdent::typeStr() const
{
  return "CarbonNetIdent";
}

void CarbonNetIdent::composeIdent(ComposeContext* context) const
{
  UtString* str = context->getBuffer();
  ComposeMode mode = context->getMode();
  const STAliasedLeafNode* node = mShellNet->getNameAsLeaf();
  switch(mode)
  {
  case eComposeNormal:
  case eComposeC:
    node->verilogCompose(str);
    break;
  case eComposeLeaf:
    node->verilogComposeLeaf(str);
    break;
  }
}

const CarbonNetIdent* CarbonNetIdent::castCarbonNetIdent() const
{
  return this;
}

const STAliasedLeafNode* CarbonNetIdent::getNode(DynBitVector* usageMask) const
{
  int bitWidth = mShellNet->getBitWidth();
  usageMask->resize(bitWidth);
  usageMask->reset();
  usageMask->setRange(0, bitWidth, 1);
  return mShellNet->getNameAsLeaf();
}

void CarbonNetIdent::print(bool, int indent) const
{
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < indent; i++) {
    screen << " ";
  }
  screen << typeStr() << '(' << this << ") ";
  CarbonExpr::printSize(screen);
  screen << " : ";
  UtString name;
  compose(&name);
  screen << name << UtIO::endl;
}

CarbonExpr::SignT 
CarbonNetIdent::evaluate(ExprEvalContext* evalContext) const
{
  DynBitVector* value = evalContext->getValue();
  DynBitVector* drive = evalContext->getDrive();
  int bitWidth = mShellNet->getBitWidth();
  value->resize(bitWidth);
  drive->resize(bitWidth);
 
  UInt32* valArr = value->getUIntArray();
  UInt32* drvArr = drive->getUIntArray();
  
  EvalContext* identEvalContext = static_cast<EvalContext*>(evalContext);
  EvalContext::ModeT mode = identEvalContext->getMode();
  switch (mode)
  {
  case EvalContext::eExamine:
    mShellNet->examine(valArr, drvArr, ShellNet::eIDrive, mCarbonModel);
    break;
  case EvalContext::eDriveCalc:
    mShellNet->examine(valArr, drvArr, ShellNet::eCalcDrive, mCarbonModel);
    break;
  case EvalContext::eXDrive:
    mShellNet->examine(valArr, drvArr, ShellNet::eXDrive, mCarbonModel);
    break;
  case EvalContext::eFormat:
    mShellNet->examine(valArr, drvArr, ShellNet::eCalcDrive, mCarbonModel);
    prepareFormat(drive);
    break;
  }
  
  return eUnsigned;
}

void CarbonNetIdent::prepareFormat(DynBitVector* drive) const
{
  const STAliasedLeafNode* node = mShellNet->getNameAsLeaf();
  const ShellDataBOM* bom = ShellSymTabBOM::getLeafBOM(node);
  NetFlags flags = bom->getNetFlags();

  bool isPulled = ShellNet::isPulled(flags);
 
  // Currently CarbonNetIdents can only be primitive shellnets. 
  // If this isn't true then we can't reset drive, because some of the
  // value may be holes in the expression (x's).
  ST_ASSERT(mShellNet->castExprNet() == NULL, node);

  if (isPulled)
    drive->reset();
}

CarbonExpr::AssignStat 
CarbonNetIdent::assign(ExprAssignContext* context)
{
  int bitWidth = getBitSize();
  size_t shiftAmount = bitWidth;

  ST_ASSERT(bitWidth == mShellNet->getBitWidth(), mShellNet->getNameAsLeaf());
  AssignContext* assignContext = static_cast<AssignContext*>(context); 

  AssignContext::ModeT mode = assignContext->getMode();
  switch (mode)
  {
  case AssignContext::eDeposit:
    if (allowDeposit(context))
      doDeposit(assignContext);
    else
      mCarbonModel->getMsgContext()->SHLNetNotDepositable(mShellNet->getName()->str());
    break;
  case AssignContext::eForce:
    doForce(assignContext);
    break;
  case AssignContext::eRelease:
    doRelease(assignContext);
    break;
  case AssignContext::eForceRange:
  case AssignContext::eReleaseRange:
    doCalcRange(assignContext, &shiftAmount, mShellNet->getLSB(),
                mShellNet->getMSB());
    break;
  }

  context->rshift(shiftAmount);
  
  return eFull;
}

CarbonExpr::AssignStat 
CarbonNetIdent::assignRange(ExprAssignContext* context,
                            const ConstantRange& range)
{
  int bitWidth = getBitSize();
  ST_ASSERT(bitWidth == mShellNet->getBitWidth(), mShellNet->getNameAsLeaf());
  AssignContext* assignContext = static_cast<AssignContext*>(context); 
  
  AssignContext::ModeT mode = assignContext->getMode();
  size_t shiftAmount = range.getLength();
  switch (mode)
  {
  case AssignContext::eDeposit:
    if (allowDeposit(context))
      doDepositRange(assignContext, range);
    else
      mCarbonModel->getMsgContext()->SHLNetNotDepositable(mShellNet->getName()->str());
    break;
  case AssignContext::eForce:
    doForceRange(assignContext, range);
    break;
  case AssignContext::eRelease:
    doReleaseRange(assignContext, range);
    break;
  case AssignContext::eForceRange:
  case AssignContext::eReleaseRange:
    doCalcRange(assignContext, &shiftAmount, range.getLsb(),
                range.getMsb());
    break;
  }
  
  context->rshift(shiftAmount);
  return eFull;
}

void CarbonNetIdent::doDeposit(AssignContext* context)
{
  CarbonStatus stat = mShellNet->deposit(context->getValueArr(), context->getDriveArr(), mCarbonModel);
  context->updateStatus(stat);
}

void CarbonNetIdent::doDepositRange(AssignContext* context, 
                                    const ConstantRange& range)
{
  CarbonStatus stat = mShellNet->depositRange(context->getValueArr(), range.getMsb(), range.getLsb(), context->getDriveArr(), mCarbonModel);
  context->updateStatus(stat);
}

void CarbonNetIdent::doForce(AssignContext* context)
{
  CarbonStatus stat = mCarbonModel->force(mShellNet, context->getValueArr());
  context->updateStatus(stat);
}

void CarbonNetIdent::doRelease(AssignContext* context)
{
  CarbonStatus stat = mCarbonModel->release(mShellNet);
  context->updateStatus(stat);
}

void CarbonNetIdent::doForceRange(AssignContext* context, 
                                  const ConstantRange& range)
{
  CarbonStatus stat = mCarbonModel->forceRange(mShellNet, context->getValueArr(), range.getMsb(), range.getLsb());
  context->updateStatus(stat);
}

void CarbonNetIdent::doReleaseRange(AssignContext* context, 
                                    const ConstantRange& range)
{
  CarbonStatus stat = mCarbonModel->releaseRange(mShellNet, range.getMsb(), range.getLsb());
  context->updateStatus(stat);
}

ptrdiff_t CarbonNetIdent::compare(const CarbonExpr* other) const
{
  // We can assume the other expression is a CarbonIdent, but it may
  // not be a CarbonNetIdent.
  const CarbonIdent *otherIdent = static_cast<const CarbonIdent*>(other);
  const CarbonNetIdent *otherNetIdent = otherIdent->castCarbonNetIdent();
  // If it's the wrong type, return a difference
  if (otherNetIdent == NULL) {
    return 1;
  }

  const HierName* myName = mShellNet->getName();
  const HierName* otherName = otherNetIdent->mShellNet->getName();
  
  ptrdiff_t ret = HierName::compare(myName, otherName);
  if (ret == 0)
  {
    if (mCarbonModel != otherNetIdent->mCarbonModel)
      ret = 1;
  }

  return ret;

}

void CarbonNetIdent::applyRange(AssignContext* context, 
                                int msb, int lsb, size_t chopAmount)
{
  AssignContext::ModeT mode = context->getMode();
  switch(mode)
  {
  case AssignContext::eDeposit:
  case AssignContext::eForce:
  case AssignContext::eRelease:
    ST_ASSERT((mode == AssignContext::eForceRange) || 
              (mode == AssignContext::eReleaseRange),
              mShellNet->getNameAsLeaf());
    break;
  case AssignContext::eForceRange:
    mCarbonModel->forceRange(mShellNet, context->getValueArr(), 
                             msb, lsb);
    break;
  case AssignContext::eReleaseRange:
    mCarbonModel->releaseRange(mShellNet, msb, lsb);
    break;
  }
  context->chopIndexLength(chopAmount);
}

void CarbonNetIdent::doCalcRange(AssignContext* context, 
                                 size_t* limitLength,
                                 int lsb,
                                 int msb)
{
  size_t bitIndex, length;
  context->getIndexLength(&bitIndex, &length);

  // Are we still looking to force or release at this point?
  // if not, just ignore and move on
  if (context->isActiveRange())
  {
    UInt32 curBitIndex = context->curBitIndex();
    if (curBitIndex == bitIndex)
    {
      size_t adder = std::min(*limitLength, length);
      ST_ASSERT(adder > 0, mShellNet->getNameAsLeaf());
      size_t chopAmount = adder;
      --adder;
      if (lsb > msb)
        msb = lsb - adder;
      else
        msb = lsb + adder;

      applyRange(context, msb, lsb, chopAmount);
#if 0      
      AssignContext::ModeT mode = context->getMode();
      switch(mode)
      {
      case AssignContext::eDeposit:
      case AssignContext::eForce:
      case AssignContext::eRelease:
        ST_ASSERT((mode == AssignContext::eForceRange) || 
                  (mode == AssignContext::eReleaseRange),
                  mShellNet->getNameAsLeaf());
        break;
      case AssignContext::eForceRange:
        mShellNet->forceRange(context->getValueArr(), 
                              msb, lsb, mCarbonModel);
        break;
      case AssignContext::eReleaseRange:
        mShellNet->releaseRange(msb, lsb, mCarbonModel);
        break;
      }
      context->chopIndexLength(chopAmount);
#endif
    }
    else if (curBitIndex < bitIndex)
    {
      // need to figure out if the beginning of the range is within this
      // assign
      if ((curBitIndex + *limitLength) > bitIndex)
      {
        UInt32 tmpShift = bitIndex - curBitIndex;
        context->rshift(tmpShift);
        *limitLength -= tmpShift;
        if (lsb > msb)
          msb += tmpShift;
        else
          lsb += tmpShift;

        applyRange(context, msb, lsb, *limitLength);
      }
    }
    else
    {
      // curBitIndex is greater than bitIndex.
      // Chop the bitIndex and length
      context->chopIndexLength(curBitIndex - bitIndex);
      doCalcRange(context, limitLength, lsb, msb);
    }
  }
}

bool CarbonNetIdent::allowDeposit(ExprAssignContext* context)
{
  bool allow = true;
  if (! mCarbonModel->isDepositable(mShellNet))
  {
    // Can only catch things if the shellnet is NOT a tristate (an
    // output for example should not be assigned)
    if (! mShellNet->isTristate())
    {
      // if the drive is all ones for the shell then skip, otherwise let
      // it go through to generate a warning
      bool isDriving = false;
      int bitWidth = getBitSize();      
      int numWords = (bitWidth + 31)/32;
      UInt32* drvArr = context->getDriveArr();
      for (int i = 0; (i < (numWords - 1)) && ! isDriving; ++i)
        isDriving = (drvArr[i] != UtUINT32_MAX);
      
      if (! isDriving)
      {
        UInt32 mask = CarbonValRW::getWordMask(bitWidth);
        isDriving = ((drvArr[numWords - 1] & mask) == 0);
      }
      
      if (! isDriving)
        allow = false;
    }
  }
  return allow;
}

bool CarbonNetIdent::isWholeIdentifier() const
{
  return true;
}

void CarbonNetIdent::putShellNet(ShellNet* replacement)
{
  mShellNet = replacement;
}
