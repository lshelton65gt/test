// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef REPLAY_GUI_H
#define REPLAY_GUI_H

#include "carbon/carbon_dbapi.h"
#include "carbon/carbon_shelltypes.h"
#include "cfg/carbon_cfg.h"
#include "gui/CFilenameEdit.h"
#include "gui/CQt.h"
#include "util/SourceLocator.h"
#include "util/UtHashSet.h"
#include <QMainWindow>
#include <QObject>
#include <QtGui>

#include "ReplayPerfPlot.h"
#include "gui/CMainWindow.h"

class QAction;
class QMenu;
class QTextEdit;
class CarbonClkPanel;
class CarbonDbgTree;
class CarbonBuild;
class DriverDatabase;
class CarbonHBrowser;
class CarbonMemEdit;
class CarbonPortEdit;
class CarbonGenProp;
class CarbonProfileEdit;
class TextEdit;
class CarbonReplaySystem;

//! class for the Replay GUI main window
class ReplayGUI : public CMainWindow
{
  Q_OBJECT
    
public:
  //! ctor
  ReplayGUI(CQtContext* cqt);
  
  //! dtor
  ~ReplayGUI();

  //! load a replay system
  bool loadFile(const char* filename);

  //! pop the window to the forefront
  void showWindow();

  //! Get the Qt Context
  CQtContext* getCQtContext() { return mCQt; }

  //! override notifyModified because we want to do a save
  virtual void notifyModified();

protected:
  void closeEvent(QCloseEvent *event);

private slots:
  void newFile();
  void open();
  bool saveAs();
  
public slots:
  bool save();
  void changeDatabase(const char* filename);
  void changeCheckpoint(int interval);
  void changeRecoverPercentage(int recoverPercentage);
  void repaint();
  void changeCompSel();
  void pollSimFile();
  void changeXAxis(int);
  void widgetModified();
  void continueSim();

private:
  void clearComponents();
  void buildWidgets();
  void populateWidgets();
  void buildLayout();
  QWidget* buildTab();
  void createStatusBar();
  void readSettings();
  void writeSettings();
  bool maybeSave();
  bool saveFile();
  void setCurrentFile(const char* filename);
  QString strippedName(const QString &fullFileName);
  void setupHeaderLabels();
  void paintSimContButton();

  void populateLoop(const char* label, CarbonDBNodeIter* iter,
                    QString* str);
  
  static eCarbonMsgCBStatus sMsgCallback(CarbonClientData, CarbonMsgSeverity,
                                         int number, const char* text,
                                         unsigned int len);

  //! enum for component status columns
  enum {
    eName,
    eReplayState,
    eOnDemandState,
    eCheckpoints,
    eSchedCalls,
    eEna,
    eRequest,
    eNumColumns
  } Column;

  CarbonReplaySystem* mReplaySystem;
  CarbonReplaySystem* mPollSimState;
  CFilenameEdit* mDatabase;

  QSpinBox* mCheckpointInterval;
  QSpinBox* mRecoverPercentage;
  CDragDropTreeWidget* mComponents;

  QRadioButton* mReplayAll;
  QRadioButton* mReplayNone;
  QRadioButton* mReplaySome;
  QRadioButton* mRecord;
  QRadioButton* mPlayback;
  QRadioButton* mStop;
  QAction *mExitAct;
  QComboBox* mXAxis;
  UtArray<ReplayPerfPlot::XAxis> mXAxisValues;
  QCheckBox* mVerbose;
  QPushButton* mContinueSim;

  CQtContext* mCQt;
  CQtAbortOverride mAssertOverride;

  UtStringSet mSelectedComponents;
  QTimer* mPollTimer;
  ReplayPerfPlot* mPlot;
  bool mPopulating;
  bool mCyclesSpecified;
  bool mSimRequestActive;
  QColor mSaveSimContForeground;
  QColor mSaveSimContBackground;
};

#endif
