/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "RegisterTableWidget.h"
#include "gui/CQt.h"
#include <QLineEdit>
#include <QComboBox>
#include <QtGui>

RegisterTableWidget::RegisterTableWidget(QWidget *parent)
  : CTableWidget(parent)
{
  ui.setupUi(this);

  clearAll();
  mCtx = 0;

  CQT_CONNECT(this, itemSelectionChanged(), this, itemSelectionChanged());
  //CQT_CONNECT(this, itemClicked(QTableWidgetItem*), this, itemClicked(QTableWidgetItem*));
}

void RegisterTableWidget::putContext(ApplicationContext *ctx)
{
  mCtx = ctx;
  setItemDelegate(new RegisterEditDelegate(this, this, mCtx->getCfg()));
  
  if (mCtx->castCoware()) {
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colOFFSET"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colREGNAME"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colWIDTH"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colENDIAN"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colDESCRIPTION"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colFIELDS"), QHeaderView::Stretch);
  }

  if (mCtx->castMaxSim()) {
    int nCols = mCtx->getRegisterTableColumnEnum("colEND");
    setColumnCount(nCols);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colOFFSET"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colGROUP"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colREGNAME"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colWIDTH"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colENDIAN"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colDESCRIPTION"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colFIELDS"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colRADIX"), QHeaderView::Interactive);
    horizontalHeader()->setResizeMode(mCtx->getRegisterTableColumnEnum("colPORT"), QHeaderView::Stretch);
  }

  setEditTriggers(QAbstractItemView::AllEditTriggers);
}

RegisterTableWidget::~RegisterTableWidget()
{

}

void RegisterTableWidget::itemClicked(QTableWidgetItem* item)
{
  if (item && item->flags() & Qt::ItemIsEditable)
    editItem(item);
}

void RegisterTableWidget::itemSelectionChanged()
{
  QList<QTableWidgetItem*> itemList;
  buildSelectionList(itemList);

  foreach (QTableWidgetItem *item, itemList)
  {
    QVariant qv = item->data(Qt::UserRole);
    CarbonCfgRegister* reg = (CarbonCfgRegister*)qv.value<void*>();
  
    emit regSelectionChanged(reg);
  }

  // Show nothing is selected
  if (itemList.count() == 0)
    emit regSelectionChanged(NULL);
}



void RegisterTableWidget::buildSelectionList(QList<QTableWidgetItem*>& list)
{
 // The list gives items for every column and row, we just want it row
  // oriented.

  list.clear();

  foreach (QTableWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonCfgRegisterField* field = (CarbonCfgRegisterField*)qv.value<void*>();
      if (field != NULL)
      {
        list.append(item);
      }
    }
  }  
}

void RegisterTableWidget::DeleteRegister()
{
  QList<QTableWidgetItem*> selectedItemList;
  buildSelectionList(selectedItemList);
  CarbonCfgRegister* reg = NULL;

  foreach (QTableWidgetItem *item, selectedItemList)
  {
    QVariant qv = item->data(Qt::UserRole);
    reg = (CarbonCfgRegister*)qv.value<void*>();

    // We have the field to work with;
    if (reg != NULL)
    {
      removeRow(item->row());
      mCfg->removeRegister(reg);
      mCtx->setDocumentModified(true);
    }
  }
}

void RegisterTableWidget::AddRegister()
{
  int row = rowCount();

  // Insert a new row
  setRowCount(row+1);

  if (mGroup == NULL)
  {
    if (mCfg->numRegisters() == 0)
    {
      UtString groupName;
      mCfg->getUniqueGroupName(&groupName, "Registers");
      mGroup = mCfg->addGroup(groupName.c_str()); 
    }
    else // Use 1st registers group
    {
      CarbonCfgRegister* r = mCfg->getRegister(0);
      mGroup = r->getGroup();
    }
  }

  UtString regName;
  mCfg->getUniqueRegName(&regName, "<New Register>");

// Find highest index
  UInt32 newOffset=0;
  for (UInt32 i=0; i<mCfg->numRegisters(); ++i)
  {
    CarbonCfgRegister* r = mCfg->getRegister(i);
    if (r->getOffset() >= newOffset)
      // offset always increment by 1, no alignment needed after bug 8933 fix
      newOffset = r->getOffset() + 1;
  }

  UInt32 width = 32; // Default Width

  CarbonCfgRegister* reg = mCfg->addRegister(regName.c_str(), mGroup, 
    width, false, eCarbonCfgRadixHex, "Register Description", false);

  CarbonCfgRegisterField *field = new CarbonCfgRegisterField(width - 1, 0, eCfgRegAccessRW);
  field->putName("Reserved");
  field->putLoc(new CarbonCfgRegisterLocReg("", 0, 0));
  reg->addField(field);
  reg->putOffset(newOffset);

  updateRow(row, reg, true);

  mCtx->setDocumentModified(true);
}

// Create a table row for this register
void RegisterTableWidget::updateRow(int row, CarbonCfgRegister *reg, bool clearSelect)
{
  BlockTableUpdates block(this);

  if (clearSelect)
    clearSelection();

  QTableWidgetItem* regOffset = new QTableWidgetItem;
  
  UtString hexOffset;
  UtOStringStream ss(&hexOffset);
  ss << "0x" << UtIO::hex << reg->getOffset();
  regOffset->setText(hexOffset.c_str());

  QVariant qv = qVariantFromValue((void*)reg);
  regOffset->setData(Qt::UserRole, qv);
  setItem(row, mCtx->getRegisterTableColumnEnum("colOFFSET"), regOffset);

  if (mCtx->castMaxSim()) {
    QTableWidgetItem* regGroup = new QTableWidgetItem;
    regGroup->setText(reg->getGroupName());
    setItem(row, mCtx->getRegisterTableColumnEnum("colGROUP"), regGroup);
  }

  QTableWidgetItem *regName = new QTableWidgetItem;
  regName->setText(reg->getName());

  setItem(row, mCtx->getRegisterTableColumnEnum("colREGNAME"), regName);

  QTableWidgetItem *regWidth = new QTableWidgetItem;
  UtString width;
  width << reg->getWidth();

  regWidth->setText(width.c_str());
  setItem(row, mCtx->getRegisterTableColumnEnum("colWIDTH"), regWidth);

  QTableWidgetItem* regDescr = new QTableWidgetItem;
  regDescr->setText(reg->getComment());

  setItem(row, mCtx->getRegisterTableColumnEnum("colDESCRIPTION"), regDescr);

  QTableWidgetItem* regFields = new QTableWidgetItem;
  UtString fields;

  UtString currEndian;
  if (reg->getBigEndian())
    currEndian = "Big";
  else
    currEndian = "Little";
  QTableWidgetItem* regEndian = new QTableWidgetItem;
  regEndian->setText(currEndian.c_str());
  setItem(row, mCtx->getRegisterTableColumnEnum("colENDIAN"), regEndian);


  fields << reg->numFields();
  if (reg->numFields() > 1)
    fields << " fields";
  else
    fields << " field";

  regFields->setText(fields.c_str());
  setItem(row, mCtx->getRegisterTableColumnEnum("colFIELDS"), regFields);

  if (mCtx->castMaxSim()) {
    QTableWidgetItem* regRadix = new QTableWidgetItem;
    regRadix->setText(reg->getRadix().getString());
    setItem(row, mCtx->getRegisterTableColumnEnum("colRADIX"), regRadix);

    QTableWidgetItem *port = new QTableWidgetItem;
    port->setText(reg->getPort());
    setItem(row, mCtx->getRegisterTableColumnEnum("colPORT"), port);
  }

  setCurrentItem(regName);

  horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);  

  //editItem(regName);
}

void RegisterTableWidget::Populate()
{
  mCfg = mCtx->getCfg();
  CQtRecordBlocker blocker(mCtx->getCQt());

  int row = rowCount();

  for (UInt32 i=0, nRegs = mCfg->numRegisters(); i<nRegs; ++i)
  {
    CarbonCfgRegister* reg = mCfg->getRegister(i);
    // Insert a new row
    setRowCount(row+1);

    updateRow(row, reg, false);

    row++;
  }

  scrollToTop();

  mCtx->setDocumentModified(false);
}

void RegisterTableWidget::dragMoveEvent(QDragMoveEvent* event)
{
  bool ignore = true;
  const QMimeData* mimeData = event->mimeData();
  if (mimeData->hasText())
  {
    event->acceptProposedAction();
    ignore = false;
  }
  if (ignore)
    event->ignore();
}

void RegisterTableWidget::dragEnterEvent(QDragEnterEvent *event)
{
  if (event->mimeData()->hasFormat("text/plain"))
    event->acceptProposedAction();
}

void RegisterTableWidget::dropEvent(QDropEvent* ev)
{
  QString nets = ev->mimeData()->text();
  QStringList netList = nets.split("\n");

  foreach(QString net, netList)
  {
    UtString netName;
    netName << net;
    const CarbonDBNode* node = carbonDBFindNode(mCtx->getDB(), netName.c_str());

    bool isMemory = carbonDBIs2DArray(mCtx->getDB(), node) && !carbonDBIsContainedByComposite(mCtx->getDB(), node);
    bool canBeNet = carbonDBCanBeCarbonNet(mCtx->getDB(), node);
    if (! (isMemory || canBeNet))
    {
      QMessageBox::warning(this, "Carbon Model Studio", "Composites are not allowed as register fields.");
      ev->setAccepted(false);
      return;
    }

    if (node)
    {
       int rtlMSB = carbonDBGetMSB(mCtx->getDB(), node);
       int rtlLSB = carbonDBGetLSB(mCtx->getDB(), node);
       int rtlNodeWidth = carbonDBGetWidth(mCtx->getDB(), node);


       // If we are CoWare, then it must be 32 or 64 bits
       if (mCtx->castCoware()) 
       {
         if (rtlNodeWidth != 32 && rtlNodeWidth != 64) 
         {
            UtString msg;
            msg << "CoWare registers can only be 32 or 64 bits in width\n";
            msg << "The Net " << carbonDBNodeGetFullName(mCtx->getDB(), node) << " is " << rtlNodeWidth << " bit(s)";

            QMessageBox::warning(this, "Model Studio", msg.c_str());

            ev->setAccepted(false);
            return;
         }
         if   (!carbonDBIsObservable(mCtx->getDB(), node))
         {
            UtString msg;
            msg << "CoWare register contents must be observable.\n";
            msg << "The Net " << carbonDBNodeGetFullName(mCtx->getDB(), node) << " is  not observable.\n";
            msg << "Please make these signal(s) observable and recompile.\n";

            QMessageBox::warning(this, "Model Studio", msg.c_str());

            ev->setAccepted(false);
            return;
         }
       }

       const char* leafName = carbonDBNodeGetLeafName(mCtx->getDB(), node);
       const char* fullName = carbonDBNodeGetFullName(mCtx->getDB(), node);

       int row = rowCount();

        if (mGroup == NULL)
        {
          if (mCfg->numRegisters() == 0)
          {
            UtString groupName;
            mCfg->getUniqueGroupName(&groupName, "Registers");
            mGroup = mCfg->addGroup(groupName.c_str()); 
          }
          else // Use 1st registers group
          {
            CarbonCfgRegister* r = mCfg->getRegister(0);
            mGroup = r->getGroup();
          }
        }

        UtString regName;
        regName << leafName;

      // Find highest index
        UInt32 newOffset=0;
        for (UInt32 i=0; i<mCfg->numRegisters(); ++i)
        {
          CarbonCfgRegister* r = mCfg->getRegister(i);
          if (r->getOffset() >= newOffset)
	    // offset always increment by 1, no alignment needed after bug 8933 fix
            newOffset = r->getOffset() + 1;
        }

        UInt32 width = rtlNodeWidth; // Default Width

        bool addReg = true;
        for (UInt32 ri=0; ri<mCfg->numRegisters(); ri++)
        {
          CarbonCfgRegister* reg = mCfg->getRegister(ri);
          if (0 == strcmp(reg->getName(), regName.c_str()))
          {
            addReg = false;
            break;
          }
        }

        if (addReg)
        {
          // Insert a new row
          setRowCount(row+1);

          CarbonCfgRegister* reg = mCfg->addRegister(regName.c_str(), mGroup, 
            width, false, eCarbonCfgRadixHex, "Register Description", false);

          CarbonCfgRegisterField *field = new CarbonCfgRegisterField(rtlNodeWidth - 1, 0, eCfgRegAccessRW);
          field->putName("Value");
          field->putLoc(new CarbonCfgRegisterLocReg(fullName, rtlMSB, rtlLSB));
        

          reg->addField(field);
          reg->putOffset(newOffset);

          updateRow(row, reg, true);

          mCtx->setDocumentModified(true);
        }
        else
        {
          UtString msg;
          msg << "Register named: " << regName << " aready exists, ignoring";
          QMessageBox::warning(this, "Carbon Model Studio", msg.c_str());
        }
    }
  }
  ev->acceptProposedAction();
  setFocus();
}

void RegisterTableWidget::clearAll()
{
  mCfg = 0;
  mGroup = 0;
  clearContents();
  setRowCount(0);
}

void RegisterTableWidget::setModelData(const UtString &data, QAbstractItemModel *model,
                                       const QModelIndex &index)
{
  QTableWidgetItem* i = item(index.row(), 0);

  if (i == 0 || mCfg == NULL)
    return;

  QVariant qv = i->data(Qt::UserRole);
  CarbonCfgRegister* reg = (CarbonCfgRegister*)qv.value<void*>();
  UtString newVal;
  CarbonSInt32 col = index.column();

  if ((col == mCtx->getRegisterTableColumnEnum("colOFFSET")))
    {
      newVal << data;
      UtIO::Radix radix = UtIO::hex;

      if (0 == strncasecmp(newVal.c_str(), "0x", 2))
      {
        newVal.erase(0, 2);
        radix = UtIO::hex;
      }
      else
      {
        radix = UtIO::dec;
      }
      UInt32 newOffset;

      UtIStringStream sNewVal(newVal);
      if (sNewVal >> radix >> newOffset)
      {
          mCtx->setDocumentModified(true);
          reg->putOffset(newOffset);

          UtString hexOffset;
          UtOStringStream ss(&hexOffset);
          ss << "0x" << UtIO::hex << reg->getOffset();
 
          model->setData(index, hexOffset.c_str());
      }
    }
  else if (mCtx->castMaxSim() && (col == mCtx->getRegisterTableColumnEnum("colGROUP")))
    {
      mCtx->setDocumentModified(true);
      newVal << data;
      mCfg->changeRegGroup(reg, newVal.c_str());
      // Save this group name to be used for new registers
      mGroup = reg->getGroup();
      model->setData(index, newVal.c_str());
    }
  else if (col == mCtx->getRegisterTableColumnEnum("colREGNAME"))
    {
      mCtx->setDocumentModified(true);
      newVal << data;
      mCfg->changeRegName(reg, newVal.c_str());
      // Cfg automatically uniquifies the name if necessary,
      // so read it back
      newVal = reg->getName();
      model->setData(index, newVal.c_str());
    }
  else if (col == mCtx->getRegisterTableColumnEnum("colDESCRIPTION"))
    {
      mCtx->setDocumentModified(true);
      newVal << data;
      reg->putComment(newVal.c_str());
      model->setData(index, newVal.c_str());
    }
  else if (col == mCtx->getRegisterTableColumnEnum("colENDIAN"))
    {
      mCtx->setDocumentModified(true);
      newVal << data;
      newVal.lowercase();
      if (newVal == "big") {
        reg->putBigEndian(true);
      } else {
        reg->putBigEndian(false);
      }
      model->setData(index, newVal.c_str());
    }
  else if (col == mCtx->getRegisterTableColumnEnum("colWIDTH"))
    {
      newVal << data;

      UtIStringStream sNewVal(newVal);
      
      UInt32 newWidth;
      if (sNewVal >> UtIO::dec >> newWidth)
      {
        mCtx->setDocumentModified(true);
        reg->putWidth(newWidth);
        model->setData(index, newVal.c_str());
      }
    }
  else if (mCtx->castMaxSim() && (col == mCtx->getRegisterTableColumnEnum("colRADIX")))
    {
      mCtx->setDocumentModified(true);
      newVal << data;
      CarbonCfgRadixIO radix;
      radix.parseString(newVal.c_str());
      reg->putRadix(radix);
      model->setData(index, newVal.c_str());
    }
  else if (mCtx->castMaxSim() && (col == mCtx->getRegisterTableColumnEnum("colPORT")))
    {
      mCtx->setDocumentModified(true);
      newVal << data;
	  if(newVal != "<None>") {
        reg->putPort(newVal.c_str());
        model->setData(index, newVal.c_str());
	  }
	  else {
	    reg->putPort("");
		model->setData(index, "");
	  }
    }

  CTableWidget::setModelData(data, model, index);
}

// Edit Delegate

// Fields Edit Delegate

RegisterEditDelegate::RegisterEditDelegate(QObject *parent, RegisterTableWidget* t, CarbonCfgID cfg)
: QItemDelegate(parent), table(t), mCfg(cfg)
{
}

//    enum Column {colREGNAME = 0, colWIDTH, colENDIAN, colDESCRIPTION, colFIELDS};

QWidget *RegisterEditDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                            const QModelIndex &index) const
{
  QTableWidgetItem* item = table->item(index.row(), 0);
  QVariant qv = item->data(Qt::UserRole);
  CarbonCfgRegister* reg = (CarbonCfgRegister*)qv.value<void*>();
  CarbonSInt32 col = index.column();

  if ((col == table->mCtx->getRegisterTableColumnEnum("colOFFSET")) ||
      (table->mCtx->castMaxSim() && (col == table->mCtx->getRegisterTableColumnEnum("colGROUP"))) ||
      (col == table->mCtx->getRegisterTableColumnEnum("colREGNAME")) ||
      (col == table->mCtx->getRegisterTableColumnEnum("colDESCRIPTION")))
    {
      QLineEdit *editor = new QLineEdit(parent);
      connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
      return editor;
    }
  else if (col == table->mCtx->getRegisterTableColumnEnum("colWIDTH"))
    {
      UtString currWidth;
      currWidth << reg->getWidth();

      QComboBox *cbox = new QComboBox(parent);
      cbox->addItem("32");
      cbox->addItem("64");

      int currIndex = cbox->findText(currWidth.c_str());
      cbox->setCurrentIndex(currIndex);

      CQT_CONNECT(cbox, currentIndexChanged(int), this, commitAndCloseComboBox(int));

      return cbox;
    }
  else if (col == table->mCtx->getRegisterTableColumnEnum("colENDIAN"))
    {
      UtString currEndian;
      if (reg->getBigEndian())
        currEndian = "Big";
      else
        currEndian = "Little";

      QComboBox *cbox = new QComboBox(parent);
      cbox->addItem("Big");
      cbox->addItem("Little");

      int currIndex = cbox->findText(currEndian.c_str());
      cbox->setCurrentIndex(currIndex);

      CQT_CONNECT(cbox, currentIndexChanged(int), this, commitAndCloseComboBox(int));

      return cbox;
    }
  else if (table->mCtx->castMaxSim() && (col == table->mCtx->getRegisterTableColumnEnum("colRADIX")))
    {
      QComboBox *cbox = new QComboBox(parent);
      cbox->addItem("Hex");
      cbox->addItem("SignedInt");
      cbox->addItem("UnsignedInt");

      int currIndex = cbox->findText(reg->getRadix().getString());
      cbox->setCurrentIndex(currIndex);

      CQT_CONNECT(cbox, currentIndexChanged(int), this, commitAndCloseComboBox(int));
      return cbox;
    }
  else if (table->mCtx->castMaxSim() && (col == table->mCtx->getRegisterTableColumnEnum("colPORT")))
    {
      QComboBox *cbox = new QComboBox(parent);
      cbox->addItem("<None>");
	  if(mCfg != 0) {
        for (UInt32 i = 0, num = mCfg->numXtorInstances(); i < num; ++i)
        {
          CarbonCfgXtorInstance* xtorInst = mCfg->getXtorInstance(i);
          if (xtorInst->getType()->hasWriteDebug())
            cbox->addItem(xtorInst->getName());
          }
      }
      int currIndex = cbox->findText(reg->getPort());
      cbox->setCurrentIndex(currIndex);

      CQT_CONNECT(cbox, currentIndexChanged(int), this, commitAndCloseComboBox(int));
      return cbox;
    }

  return NULL;
}

void RegisterEditDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  emit commitData(editor);
  emit closeEditor(editor);
}

void RegisterEditDelegate::commitAndCloseComboBox(int i)
{
  if (i != -1)
  {
    QComboBox *editor = qobject_cast<QComboBox *>(sender());
    emit commitData(editor);
    emit closeEditor(editor);
  }
}

void RegisterEditDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit) {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}
void RegisterEditDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                        const QModelIndex &index) const
{
  UtString editText;
  CarbonSInt32 col = index.column();

  if ((col == table->mCtx->getRegisterTableColumnEnum("colOFFSET")) ||
      (table->mCtx->castMaxSim() && (col == table->mCtx->getRegisterTableColumnEnum("colGROUP"))) ||
      (col == table->mCtx->getRegisterTableColumnEnum("colREGNAME")) ||
      (col == table->mCtx->getRegisterTableColumnEnum("colDESCRIPTION")))
    {
      QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
      if (edit) {
        editText << edit->text();
      }
    }
  else if ((col == table->mCtx->getRegisterTableColumnEnum("colENDIAN")) ||
           (col == table->mCtx->getRegisterTableColumnEnum("colWIDTH")) ||
           (table->mCtx->castMaxSim() && (col == table->mCtx->getRegisterTableColumnEnum("colRADIX"))) || 
           (table->mCtx->castMaxSim() && (col == table->mCtx->getRegisterTableColumnEnum("colPORT"))))
    {
      QComboBox* cbox = qobject_cast<QComboBox *>(editor);
      if (cbox != NULL)
      {
        editText << cbox->currentText();
      }
    }

  table->setModelData(editText, model, index);
}

