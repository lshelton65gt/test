/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "DialogAdvancedBinding.h"
#include <QMessageBox>
#include "gui/CQt.h"

DialogAdvancedBinding::DialogAdvancedBinding(QWidget *parent)
: CDialog(parent)
{
  ui.setupUi(this);
}

DialogAdvancedBinding::~DialogAdvancedBinding()
{

}

void DialogAdvancedBinding::setData(const UtString &data)
{
  UtIStringStream is(data.c_str());

  is >> UtIO::slashify >> m_prefix >> m_suffix;

  ui.lineEditPrefix->setText(m_prefix.c_str());
  ui.lineEditSuffix->setText(m_suffix.c_str());

  accept();
  CDialog::setData(data);
}
