#ifndef XTORPORTS_H
#define XTORPORTS_H

#include "gui/CDragDropTreeWidget.h"

#include <QTreeWidget>
#include "ui_XtorPorts.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "cfg/CarbonCfg.h"
#include "util/UtWildcard.h"

#include "ApplicationContext.h"

class XtorPorts : public CDragDropTreeWidget
{
  Q_OBJECT
  
  enum Column {colPORT=0, colWIDTH, colXTORPORT};

public:
  XtorPorts(QWidget *parent = 0);
  ~XtorPorts();
  void putContext(ApplicationContext* ctx);
  void Populate(ApplicationContext* ctx, bool initPorts);
  void Refresh();

private:
  void populateLoop(CarbonDB* carbonDB, CarbonCfgRTLPortType type, CarbonDBNodeIter* iter);
  void buildSelectionList(QList<QTreeWidgetItem*>& list);

private slots:
  void itemSelectionChanged();

private:
  Ui::XtorPortsClass ui;
  ApplicationContext* mCtx;

signals:
  void portSelectionChanged(CarbonCfgRTLPortID port);

};

#endif // XTORPORTS_H
