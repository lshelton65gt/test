// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "CQtWizardContext.h"
#include "util/ArgProc.h"
#include "DlgSplashScreen.h"
#include <QTimer>
#include <QDebug>

DlgSplashScreen* dm = NULL;

CQtWizardContext::CQtWizardContext(int& argc, char** argv)
  : CQtContext(argc, argv)
{
  //QTimer* tempTimer = new QTimer(this);
  //tempTimer->setSingleShot(true);
  //tempTimer->setInterval(3000);     // 3 seconds
  //connect(tempTimer, SIGNAL(timeout()), this, SLOT(killSplash()));

  //dm = new DlgSplashScreen();
  //dm->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::SplashScreen);
  //dm->show();
  //tempTimer->start();
}

void CQtWizardContext::killSplash()
{
  qDebug() << "Kill splash now";
  //dm->close();
}


CQtWizardContext::~CQtWizardContext()
{
}

CQtWizardContext::WizardType CQtWizardContext::parseTypeArgs()
{
  static const char *type_names[] = {"-maxsim", "-coware"};

  mType = eNone;
  for (int i = 0; i != static_cast<int>(eNone); ++i) {
    if (mArgs->getBoolValue(type_names[i])) {
      if (mType == eNone)
        mType = static_cast<WizardType>(i);
      else
        mType = eMult;
    }
  }

  return mType;
}

void CQtWizardContext::addCustomArgs()
{
  mArgs->addBool("-maxsim", "invoke MaxSim wizard", false, 1);
  mArgs->addBool("-coware", "invoke Coware wizard", false, 1);

  const char *sect = "Wizard Mode Selection";
  mArgs->createSection(sect);
  mArgs->addToSection(sect, "-maxsim");
  mArgs->addToSection(sect, "-coware");
}
