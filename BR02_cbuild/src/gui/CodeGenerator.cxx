/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/


#include "util/CarbonPlatform.h"

#include <QProcess>
#include <QTextEdit>

#include "car2cow.h"
#include "CodeGenerator.h"
#include "util/LicenseBypass.h"

CodeGenerator::~CodeGenerator()
{
}


CodeGenerator::CodeGenerator(Car2Cow* pApp)
{
  mCodec = QTextCodec::codecForLocale(); // could this fail?
  mApp = pApp;
  mProcess = NULL;
  mText = NULL;
}

void CodeGenerator::Generate(QTextEdit* txt)
{
  mText = txt;
  mProcess = new QProcess(this);

  LICENSE_ENV_SETUP(mProcess);

  CQT_CONNECT(mProcess, error(QProcess::ProcessError),
              this, processError(QProcess::ProcessError));
  CQT_CONNECT(mProcess, readyReadStandardError(), this, readStderr());
  CQT_CONNECT(mProcess, readyReadStandardOutput(), this, readStdout());
  CQT_CONNECT(mProcess, started(), this, started());
  CQT_CONNECT(mProcess, finished(int), this, finished(int));

  char* path = getenv("CARBON_HOME");
  if( path == NULL )
  {
    mText->append("ERROR: Environment variable CARBON_HOME not set.\n");
    return;
  }

  QString carbon_entry( path );

#ifdef Q_OS_WIN
  carbon_entry += "\\bin\\carbon.bat"; 
#else
  carbon_entry += "/bin/carbon"; 
#endif
  UtString ccfg(mApp->getCcfgFilename());

  QStringList command;
  UtString ccfgArg;

  ccfgArg << "-ccfg " << ccfg.c_str();

  if (ccfg.length() > 0)
    command << "systemCWrapper" << "-gencowarebuild" << "-ccfg" << ccfg.c_str();
  else
    command << "systemCWrapper" << "-gencowarebuild" << mApp->getCcfgID()->getIODBFileName();
  
  launch(carbon_entry, command);
}

void CodeGenerator::processError(QProcess::ProcessError err) 
{
  const char* emsg = "unknown error";
  switch (err) {
  case QProcess::FailedToStart: emsg = "FailedToStart"; break;
  case QProcess::Crashed: emsg = "Crashed"; break;
  case QProcess::Timedout: emsg = "Timedout"; break;
  case QProcess::WriteError: emsg = "WriteError"; break;
  case QProcess::ReadError: emsg = "ReadError"; break;
  case QProcess::UnknownError: emsg = "UnknownError"; break;
  }
  mText->append(emsg);
  mText->append("\n");

}
void CodeGenerator::launch(QString& cmd, QStringList& args)
{
  QString launch_msg("Invoking...\n    ");
  launch_msg += cmd;
  for( QStringList::iterator i = args.begin(); i != args.end(); i++) {
    launch_msg += " ";
    launch_msg += *i;
  }
  mText->append(launch_msg);
  mProcess->start( cmd, args );
}

void CodeGenerator::readStderr()
{
  appendBytes(mProcess->readAllStandardError());
}

void CodeGenerator::readStdout() 
{
  appendBytes(mProcess->readAllStandardOutput());
}

void CodeGenerator::started()
{
  mText->append("Started\n");
}

void CodeGenerator::appendBytes(const QByteArray& bytes)
{
  if (mCodec != NULL) {
    QString str = mCodec->toUnicode(bytes);
    mText->append(str);
  }
  else {
    // I'm not sure if we might fail to get the CODEC, but let's
    // let the failure-mode for that be that we will append Euro-centric
    // text rather than crashing.
    mText->append(bytes);
  }
}

void CodeGenerator::finished(int exit_code)
{
  char buf[100];
  sprintf(buf, "\nFinished: exit code %d", exit_code);
  mText->append(buf);
}
