// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CARBON_PORT_EDIT_H_
#define _CARBON_PORT_EDIT_H_

#include "gui/CQt.h"
#include <QHBoxLayout>
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "RegTabWidget.h"

class CDragDropTreeWidget;
class QTreeWidgetItem;
class QLabel;
class CarbonCfgXtorParam;
class CarbonCfgXtorParamInst;
class CExprValidator;
class DialogAdvancedBinding;
class CHexDecValidator;

class CarbonPortEdit : public QWidget {
  Q_OBJECT

public:

  //! ctor
  CarbonPortEdit(CQtContext* cqt, CarbonCfgID cfg,
                 AtomicCache* atomicCache,
                 QAction* deleteAction, RegTabWidget::WizardMode mode=RegTabWidget::Standalone);

  //! dtor
  ~CarbonPortEdit();

  //! lay out the port tree page
  QWidget* layout();

  //! clear the tree
  void clear();

  //! clear and read the tree from a carbon config object
  void readConfig();
  void updateWidgets();

  //! write the tree from a carbon config object
  void write();

  //! speculatively populate the tree based on the ports acquired
  //! from the Carbon database.  At some point this should merge
  //! in with the information already in the tree, but for now it
  //! will clear it first
  void populate(CarbonDB*, bool initPorts);

  void deleteCurrent();
  bool isDeleteActive() const;
  QWidget* layoutGeneralProperties();
  void addTransactorsToComboBox();
  void switchParticularRTLPortType(CarbonCfgRTLPort *rtlPort, int index);

public slots:
  void changeRTLTreeSel();
  void changeESLTreeSel();
  void changePRMTreeSel();
  void switchRTLType(int index);
  void switchXtorType(int index);
  void changeClkInit(int index);
  void changeClkDelay(int val);
  void changeClkDutyCycle(int val);
  void changeClkCompCycles(int val);
  void changeClkClockCycles(int val);
  void changeResetActiveValue(const QString& val);
  void changeResetInactiveValue(const QString& val);
  void changeResetClockCycles(int index);
  void changeResetCompCycles(int index);
  void changeResetCyclesBefore(int index);
  void changeResetCyclesAsserted(int index);
  void changeResetCyclesAfter(int index);
  void changeXtorClockMaster(int index);
  void addPort();
  void addXtor();
  void assignByName(const char* prefix=NULL, const char* suffix=NULL);
  void updateESLName(const QString&);
  void updateESLPortExpr(const QString&);
  void updateESLParamEdit(const QString&);
  void dropRTLPort(const char*,QTreeWidgetItem*);
  void dropXtorConn(const char*,QTreeWidgetItem*);
  void dropPRM(const char*,QTreeWidgetItem*);
  void advancedBinding();
  void newParameter();
  void delParameter();

private:
  void build();
  void updateParamInstance(CarbonCfgXtorParamInst* paramInst, QTreeWidgetItem* paramItem=NULL);
  void selectTieParam(CarbonCfgTieParam* tiep);
  void removeTieParam(CarbonCfgTieParam* tiep);
  void createGlobalParam(CarbonCfgXtorParamInst* paramInst);
  QTreeWidgetItem* findChild(QTreeWidgetItem* parent, int col, const char* text);
  int findChildIndex(QTreeWidgetItem* parent, int col, const char* text);

  CarbonCfgESLPortID addConnection(CarbonCfgRTLPortID rtlPort);
  CarbonCfgESLPortID addESLConnection(CarbonCfgRTLPortID rtlPort,
                                      CarbonCfgESLPortType type);
  void updateClk(QTreeWidgetItem* item, const char* portName);
  void updateClkWidgets(CarbonCfgClockGen*);
  void updateResetWidgets(CarbonCfgResetGen*);
  void updateESLWidgets(CarbonCfgESLPort*);
  void updateXtorConn(CarbonCfgXtorConn*);
  void updateTieWidgets(CarbonCfgTie*);
  bool getSelectedPort(QTreeWidgetItem** pitem, UtString* portName) const;
  void updateIO(QTreeWidgetItem* item, const char* portName);
  void updateTieParamWidgets(CarbonCfgTieParam* tiep);

  void showParamEditor(CarbonCfgXtorParamInst* paramInst);
  void showParamValue(QTreeWidgetItem* item, CarbonCfgXtorParamInst* paramInst);
  QTreeWidgetItem* showParam(QTreeWidgetItem* parent, CarbonCfgXtorParamInst* paramInst);
  QTreeWidgetItem* showConnection(CarbonCfgRTLConnectionID conn);
  QTreeWidgetItem* showESLPort(CarbonCfgESLPort* eslPort,
                               QTreeWidgetItem* conItem);

  void disconnectRTLPort(CarbonCfgRTLPortID, bool leaveXtorConns);
  QTreeWidgetItem* drawRTLPortItem(CarbonCfgRTLPort* port);
  void connectXtor(CarbonCfgRTLPort* rtlPort,
                   CarbonCfgXtorInstance* xinst,
                   UInt32 portIndex);
  QTreeWidgetItem* showXtorConn(CarbonCfgXtorConn* xcon,
                                QTreeWidgetItem* conItem);
  void showPortExpr(QTreeWidgetItem* conItem,
                    const char* default_text,
                    const char* var_name,
                    const char* expr);

  void hideConnectionWidgets();
  void updateXtorWidgets(const char* portName);

  void updateResetWidgets(const char* portName);

  void updateReset(QTreeWidgetItem* item, const char* portName);
  void updateDisconnect(QTreeWidgetItem* item, const char* portName);
  void updateTie(QTreeWidgetItem* item, const char* portName);
  void updateXtor(QTreeWidgetItem* item, const char* portName);

  void populateLoop(CarbonDB*, CarbonCfgRTLPortType type,
                    CarbonDBNodeIter* iter);
  void addClockButtons();
  void selectRTL(CarbonCfgRTLPortID rtlPort);
  void selectConnection(CarbonCfgRTLConnectionID conn);
  void selectXtorInst(CarbonCfgXtorInstance* xinst);
  void selectItem(QTreeWidgetItem* item);
  void buildGeneralProperties();
  void buildPortMap();
  void rebuildXtorNameChooser();
  void reassignPorts(QTreeWidgetItem* item, const char* xtorName);
  bool assignByNameHelper(bool dry_run, const char* prefix=NULL, const char* suffix=NULL);
  void mapXtor(const char* xtorName, const char* xtorType,
               QTreeWidgetItem* item);
  void deleteXtor(QTreeWidgetItem* item, const char* portName);
  void clearSelections();

  QTreeWidgetItem* showXtorInstance(CarbonCfgXtorInstance* xtorInstance);
  void addXtor(CarbonCfgXtor* xtor);
  void removeConnection(CarbonCfgRTLConnection* conn, bool redraw,
                        int idx = -1);
  void removeXtorInstance(CarbonCfgXtorInstance* xinst);
  void enableWidgets();
  bool deleteHelper(bool dry_run);
  void addXtorHelper(CarbonCfgXtor*);

  QGroupBox *frequencyGroup(const char *objName, QSpinBox *clockCycleEditor, QSpinBox *compCycleEditor);
  QGroupBox *simpleGroup(const char *name, QWidget *widget);

  CDragDropTreeWidget* mRTLTree;
  CDragDropTreeWidget* mESLTree;
  CDragDropTreeWidget* mPRMTree;

  typedef UtHashMap<QTreeWidgetItem*,CarbonCfgRTLPortID> ItemPortMap;
  typedef UtHashMap<CarbonCfgRTLPortID,QTreeWidgetItem*> PortItemMap;
  ItemPortMap mItemPortMap;
  PortItemMap mPortItemMap;

  // RTLConnections show up in both the RTL tree and the ESL tree
  typedef UtHashMap<QTreeWidgetItem*,CarbonCfgRTLConnectionID> ItemConnMap;
  typedef UtHashMap<CarbonCfgRTLConnectionID,QTreeWidgetItem*> ConnItemMap;
  ItemConnMap mItemConnMap;
  ConnItemMap mConnItemMap;

  typedef UtHashMap<QTreeWidgetItem*,CarbonCfgXtorInstanceID> ItemXtorMap;
  typedef UtHashMap<CarbonCfgXtorInstanceID,QTreeWidgetItem*> XtorItemMap;
  ItemXtorMap mItemXtorMap;
  XtorItemMap mXtorItemMap;

  CQtContext* mCQt;
  CarbonCfgID mCfg;
  QComboBox* mSwitchRTLType;
  QComboBox* mSwitchXtorType;
  QHBoxLayout* mPortButtons;
  QComboBox* mClockInitialValue;
  QComboBox* mXtorClockMaster;
  QSpinBox* mClockDelay;
  QSpinBox* mCompCycles;
  QSpinBox* mClockCycles;
  QSpinBox* mDutyCycle;
  QLineEdit* mResetActiveValue;
  QLineEdit* mResetInactiveValue;
  CHexDecValidator* mResetValidator;
  QSpinBox* mResetCompCycles;
  QSpinBox* mResetClockCycles;
  QSpinBox* mResetCyclesBefore;
  QSpinBox* mResetCyclesAsserted;
  QSpinBox* mResetCyclesAfter;

  //QComboBox* mPortName;

  QPushButton* mAddPort;
  QPushButton* mAddXtor;
  QPushButton* mAssignByName;
  QPushButton* mAdvancedBind;

  QPushButton* mNewParam;

  QRadioButton* mRadioMaxsim;
  QRadioButton* mRadioRealview;
  QTextEdit* mComponentDescription;
  QLineEdit* mLibFile;
  QLineEdit* mCompName;
  QLineEdit* mESLName;
  CExprValidator* mESLPortExpr;
  QLineEdit* mParamEditor;
  QWidget* mClockGenFrame;
  QWidget* mResetGenFrame;
  QWidget* mXtorClockFrame;
  QWidget* mPortExprFrame;
  QGroupBox* mParamEditFrame;

  CarbonCfgRTLConnectionType mCurrentType;
  UtArray<CarbonCfgESLPort*> mCurrentESLPorts;
  UtArray<CarbonCfgXtorConn*> mCurrentXtorConns;
  UtArray<CarbonCfgClockGen*> mCurrentClockGens;
  UtArray<CarbonCfgResetGen*> mCurrentResetGens;
  UtArray<CarbonCfgTie*> mCurrentTies;
  UtArray<CarbonCfgRTLPortID> mCurrentRTLPorts;
  UtArray<CarbonCfgXtorInstance*> mCurrentXtorInstances;
  int mCurrentXtorSwitchIndex;
  int mCurrentRTLSwitchIndex;
  enum {eNone, eClock, eReset, eXtor} mClockWidgetMode;
  typedef UtHashSet<CarbonCfgRTLConnection*> ConSet;
  ConSet mCurrentConnections;

  typedef UtHashMap<UtString,int> XtorIndexMap;
  XtorIndexMap mXtorIndexMap;

  UtStringArray mXtorNameChooserMap;

  // Keep track of which transactor ports are accounted for
  // so far.  The map is from name-of-xtor to names-of-xtor-ports
  typedef UtHashMap<UtString,UtStringSet> XtorPortsMap;
  XtorPortsMap mXtorPortsMap;

  QTreeWidgetItem* mDelayXtorItem;
  QTreeWidgetItem* mDelaySigItem;

  QCheckBox* mEnaDebugMsg;

  AtomicCache* mAtomicCache;

  UtArray<CarbonCfgXtorPort*> mPortVector;
  QAction* mDeleteAction;

  CarbonDB* mDB;

  UInt32 mSelectingRTL;
  UInt32 mSelectingESL;
  UInt32 mSelectingPRM;

  RegTabWidget::WizardMode mMode;

  DialogAdvancedBinding* mDialog;
  CarbonCfgXtorParamInst* mParamInstance;

  UtString mParamInst;
  UtString mGlobParam;

};

#endif
