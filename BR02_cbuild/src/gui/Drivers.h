// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _DRIVERS__H
#define _DRIVERS__H

#include "util/SourceLocator.h"
#include "util/UtArray.h"
#include "shell/carbon_dbapi.h"
#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/StringAtom.h"
#include "util/Loop.h"

// Keep track of all the drivers in the entire design.  

class Driver;
typedef UtArray<Driver*> DriverVec;

//! Loop over all the vector of drivers
typedef Loop<DriverVec> DriverLoop;

typedef UtArray<const CarbonDBNode*> DBNodeVec;

//! Loop over vector of DBNodes
typedef Loop<DBNodeVec> DBNodeLoop;


//! Driver class
class Driver {
public:
  CARBONMEM_OVERRIDES

  //! ctor
  Driver(const CarbonDBNode* driver_path, const CarbonDBNode* net,
         const SourceLocator& loc, StringAtom* bits, StringAtom* type) :
    mDriverPath(driver_path), mDefNet(net), mLoc(loc), mBits(bits), mType(type)
  {}

  //! dtor
  ~Driver();

  //! add a nested driver
  void addNested(Driver* driver);

  //! add a fanin node
  void addFanin(const CarbonDBNode* fanin);

  //! compare two drivers
  bool operator<(const Driver& other) const;

  //! get the hierarchical path to the driver
  const CarbonDBNode* getDriverPath() const {return mDriverPath;}

  //! Get the net that's written by this node
  const CarbonDBNode* getDefNet() const {return mDefNet;}

  //! Get the source location of the driver
  const SourceLocator& getLoc() const {return mLoc;}

  //! Get a string representing the net bits driven by this driver
  const char* getBits() {return mBits->str();}

  //! Get the type of driver (e.g. ContAssign, AlwaysBlock)
  const char* getType() {return mType->str();}

  //! Loop over all the nested drivers
  DriverLoop loopNested() {return DriverLoop(mNested);}

  //! Loop over all fanin
  DBNodeLoop loopFanin() {return DBNodeLoop(mFanin);}

private:
  const CarbonDBNode* mDriverPath;
  const CarbonDBNode* mDefNet;
  DriverVec mNested;
  DBNodeVec mFanin;
  SourceLocator mLoc;
  StringAtom* mBits;
  StringAtom* mType;
};

//! Database of drivers
class DriverDatabase {
  typedef UtHashMap<const CarbonDBNode*,DriverVec> NodeDriverMap;

public:
  CARBONMEM_OVERRIDES

  //! ctor
  DriverDatabase(SourceLocatorFactory*, AtomicCache*, CarbonDB*);

  //! dtor
  ~DriverDatabase();

  //! Load the drivers database from the .drivers file
  bool load(const char* filename, UtString* errmsg);

  //! Loop over all the drivers associated with a net
  DriverLoop loopDrivers(const CarbonDBNode*);

  //! does this net have any drivers?
  bool hasDrivers(const CarbonDBNode*) const;

  //! Count the drivers
  UInt32 numDrivers(const CarbonDBNode*) const;

  //! Loop over all the drivers associated with a net
  DriverLoop loopReaders(const CarbonDBNode*);

  //! does this net have any readers?
  bool hasReaders(const CarbonDBNode*) const;

  //! Count the readers
  UInt32 numReaders(const CarbonDBNode*) const;

  //! get the source locator factory
  SourceLocatorFactory* getSourceLocatorFactory() const {
    return mSourceLocatorFactory;
  }

private:
  void readString(ZistreamDB&);
  void readPath(ZistreamDB&);
  void readDriver(ZistreamDB&);

  NodeDriverMap mNodeDriverMap;
  NodeDriverMap mNodeReaderMap;
  SourceLocatorFactory* mSourceLocatorFactory;
  AtomicCache* mAtomicCache;
  CarbonDB* mDB;
  DriverVec mEmptyDriverVec;
  UtString mBuffer;
  UInt32 mNumRecords;
};

#endif
