//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "gui/CZoomPlot.h"
#include "gui/CScrollZoomer.h"
#include <qwt_plot_grid.h>
#include <qwt_plot_layout.h>
#include <qwt_scale_widget.h>
#include <qwt_scale_draw.h>

const unsigned int c_rangeMax = 1000;

class CPlotZoomer: public CScrollZoomer
{
public:
  CPlotZoomer(CZoomPlot* zoom_plot) :
    CScrollZoomer(zoom_plot->canvas()),
    mZoomPlot(zoom_plot)
  {
  }
  
  virtual void rescale()
  {
    QwtScaleWidget *scaleWidget = plot()->axisWidget(yAxis());
    QwtScaleDraw *sd = scaleWidget->scaleDraw();
    
    int minExtent = 0;
    if ( zoomRectIndex() > 0 )
    {
      // When scrolling in vertical direction
      // the plot is jumping in horizontal direction
      // because of the different widths of the labels
      // So we better use a fixed extent.
      
      minExtent = sd->spacing() + sd->majTickLength() + 1;
      double xsize, ysize;
      mZoomPlot->getExtents(&xsize, &ysize);
      minExtent += sd->labelSize(scaleWidget->font(), xsize).width();
    }
    
    sd->setMinimumExtent(minExtent);
    
    CScrollZoomer::rescale();
    //mZoomPlot->update();
  }

private:
  CZoomPlot* mZoomPlot;
};


CZoomPlot::CZoomPlot(QWidget* parent) : QwtPlot(parent) {
  setFrameStyle(QFrame::NoFrame);
  setLineWidth(0);
  setCanvasLineWidth(2);
  
  plotLayout()->setAlignCanvasToScales(true);
  
  QwtPlotGrid *grid = new QwtPlotGrid;
  grid->setMajPen(QPen(Qt::gray, 0, Qt::DotLine));
  grid->attach(this);
  
  setCanvasBackground(QColor(29, 100, 141)); // nice blue
  
//  setAxisScale(xBottom, 0, c_rangeMax);
//  setAxisScale(yLeft, 0, c_rangeMax);
  
//  replot();
  
  // enable zooming
  
  CPlotZoomer *zoomer = new CPlotZoomer(this);
  zoomer->setRubberBandPen(QPen(Qt::red, 2, Qt::DotLine));
  zoomer->setTrackerPen(QPen(Qt::red));
} // CZoomPlot::CZoomPlot


CZoomPlot::~CZoomPlot() {
}


