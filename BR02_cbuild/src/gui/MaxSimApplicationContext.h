// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef __MAXSIM_APPLICATION_CONTEXT_H__
#define __MAXSIM_APPLICATION_CONTEXT_H__

#include "ApplicationContext.h"

class QMainWindow;

class MaxSimApplicationContext : public ApplicationContext
{
public:
  MaxSimApplicationContext(CarbonDB *db, CarbonCfg* cfg, CQtContext *cqt);
  virtual ~MaxSimApplicationContext();

  // reimplemented functions from base class
  virtual void setDocumentModified(bool value);
  virtual MaxSimApplicationContext *castMaxSim();

  void putMainWin(QMainWindow *win) {mMainWin = win;}
  void putModelStudio(QWidget* wiz) {mWizard = wiz; }

private:
  QMainWindow *mMainWin;
  QWidget* mWizard;
  enum RegisterTableColumn {colOFFSET = 0, colGROUP, colREGNAME, colWIDTH, colENDIAN, colDESCRIPTION, colFIELDS, colRADIX, colPORT, colEND};
};

#endif
