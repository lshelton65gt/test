// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CQTWIZARDCONTEXT_H__
#define __CQTWIZARDCONTEXT_H__

#include "gui/CQt.h"

class CQtWizardContext : public CQtContext
{
  Q_OBJECT
public:
  CQtWizardContext(int& argc, char** argv);
  ~CQtWizardContext();

  enum WizardType {eMaxSim, eCoware, eNone, eMult};

  WizardType parseTypeArgs();
  WizardType getType() const {return mType;}
  void putType(WizardType newVal) { mType=newVal; }

protected:
  virtual void addCustomArgs();

private slots:
 void killSplash();

private:
  WizardType mType;
};

#endif
