/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CQt.h"
#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/AtomicCache.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/ArgProc.h"
#include <QEvent>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QComboBox>
#include <QGroupBox>
#include <QTimer>
#include <QtGui>
#include "gui/CHexSpinBox.h"
#include "gui/CDragDropTreeWidget.h"
#include "gui/CFilenameEdit.h"
#include "gui/CQtAction.h"
#include "gui/CQtProcessControl.h"
#include "gui/CQtPushButtonControl.h"
#include "gui/CQtRadioButtonControl.h"
#include "gui/CGroupBoxHelp.h"
#include "gui/CMainWindow.h"
#include "assistant/Assistant.h"

#if defined(Q_OS_WIN32)
#include <windows.h>
#endif

#define APP_NAME "Carbon Component Wizard"

CQtContext::CQtContext(int& argc, char** argv):
  QApplication(argc, argv)
{
  mIgnoreRegistration = false;
  mAtomicCache = new AtomicCache;
  mNameToKeyMap = new NameToKeyMap;
  mKeyToNameMap = new KeyToNameMap;
  mNameControlMap = new NameControlMap;
  mWidgetNameMap = new WidgetNameMap;
  mActionNameMap = new ActionNameMap;
  mHelpPageMap = new TagToUrlMap;
  mDisableRecordStack = 0;
  mPlaybackBusy = false;
  mHelpWindow = NULL;
  mMainWindow = NULL;
  mExitLogged = false;

  initializeHelpPages();

  mNextEventTimer = NULL;
  mArgs = 0;
}

void CQtContext::init(int& argc, char** argv) {
  mArgs = new ArgProc;
  mArgs->addOutputFile("-recordFile",
                       "File to record events into",
                       NULL,
                       true,    // no default
                       ArgProc::eRegFile,
                       false,   // allow multiple
                       1);      // pass
  mArgs->addInputFile("-playFile",
                      "File to play events from",
                      NULL,
                      true,    // no default
                      false,   // allow multiple
                      1);      // pass
  const char* sect = "Regression Testing";
  mArgs->createSection(sect);
  mArgs->addToSection(sect, "-recordFile");
  mArgs->addToSection(sect, "-playFile");

  const char* xtorSect = "Transactors";
  mArgs->createSection(xtorSect);
  mArgs->addInputFile("-xtorDef",
                      "File containing transactor definitions.",
                      NULL,
                      true,  // no default
                      false, // allow multiple (maybe later)
                      1);
  mArgs->addToSection(xtorSect, "-xtorDef");

  addCustomArgs();

  int numOptions;
  UtString errmsg;
  if (mArgs->parseCommandLine(&argc, argv, &numOptions, &errmsg)
      != ArgProc::eParsed)
  {
    UtIO::cout() << errmsg << "\n";
    abort();
  }

  // look for a transactor definition file
  mXtorDefFile = mArgs->getStrLast("-xtorDef");

  mRecordFile = NULL;
  mPlaybackFile = NULL;

  // If we are recording, open the file
  const char* filename = mArgs->getStrLast("-recordFile");
  if (filename != NULL) {
    mRecordFile = new UtOBStream(filename);
    *mRecordFile << UtIO::slashify;
    if (!mRecordFile->is_open()) {
      UtIO::cout() << mRecordFile->getErrmsg();
      abort();
    }
    setupKeymap();
  }

  filename = mArgs->getStrLast("-playFile");
  if (filename != NULL) {
    if (mRecordFile != NULL) {
      UtIO::cout() << "Cannot record and play at the same time\n";
      abort();
    }

    mPlaybackFile = new UtIBStream(filename);
    *mPlaybackFile >> UtIO::slashify;
    if (!mPlaybackFile->is_open()) {
      UtIO::cout() << mPlaybackFile->getErrmsg();
      abort();
    }
    setupKeymap();

    // Set up a timer to read an event from the input file.
    // After we have read that event, we will queue up to read another
    // one.
    mNextEventTimer = new QTimer(this);
    mNextEventTimer->setInterval(10);     // milliseconds
    CQT_CONNECT(mNextEventTimer, timeout(), this, readNextEvent());
    mNextEventTimer->start();
  }
} // CQtContext::CQtContext

void CQtContext::stopPlayback() {
  if (mPlaybackFile != NULL) {
    delete mPlaybackFile;
    mPlaybackFile = NULL;
  }
}

void CQtContext::readNextEvent() {
  if (mPlaybackBusy) {
    // If we got into a blocking dialog during playback then we
    // might start reading events too soon.  So far the only case
    // I know of is when we assert.  continuing to read & play
    // events after an assert is very confusing to debug.
    return;
  }
  mPlaybackBusy = true;

  UtString buf;
  if (mPlaybackFile == NULL) {
    // We are out of events.  Time to die.
    UtIO::cout() << "GUI playback file terminated without quitting.\n";
    UtIO::cout().flush();
    abort();
  }
  else if (mPlaybackFile->getline(&buf)) {
    playEvent(buf.c_str());

    if (! mProcessWait.empty()) {
      mNextEventTimer->stop();  // restart when processFinished is called
    }
  }
  else {
    delete mPlaybackFile;
    mPlaybackFile = NULL;

    // We are out of events.  If we are connected to a terminal, then
    // continue indefinitely.  But if our output is redirected then
    // we are probably in regressions and we should just exit, which
    // we would do the next time the timer is called
    bool interactive = false;
#if pfUNIX
    if (isatty(fileno(stdin)) && isatty(fileno(stdout))) {
      interactive = true;
    }
#endif
    if (interactive) {
      mNextEventTimer->stop();
    }
    else {
      // Give the system 10 seconds to settle out and exit on its
      // own, in case there is some kind of window-system delay
      // before the termination.
      mNextEventTimer->setInterval(10*1000);
    }
  }
  mPlaybackBusy = false;
} // void CQtContext::readNextEvent

void CQtContext::processFinished(QProcess* process) {
  /*int removed = */mProcessWait.erase(process);
  //INFO_ASSERT(removed == 1, "Process finished that was not registered");
  if (mProcessWait.empty() && isPlaybackActive()) {
    mNextEventTimer->start();
  }
}

void CQtContext::waitForProcess(QProcess* process) {
  mProcessWait.insert(process);
}

void CQtContext::dumpStateHierarchy()
{
  if ((mRecordFile != NULL) || (mPlaybackFile != NULL))
  {
    UtOStream& stdout_stream = UtIO::cout();
    for (NameControlMap::SortedLoop q = mNameControlMap->loopSorted(); !q.atEnd(); ++q)
      {
        UtString key = q.getKey();
	CQtControl* control = q.getValue();
        UtString value, state;
        control->getValue(&value);
        UtOStringStream state_stream(&state);
        control->dumpWidgetProperties(state_stream);
        control->dumpState(state_stream);

        // Only dump a widget state if it's got something interesting
        // in it
        if (!value.empty() || (!state.empty() && (state != "\n"))) {
          stdout_stream << "Begin Widget " << key << "\n";
          stdout_stream << "Value (" << value << ")\n";
          stdout_stream << state;
          stdout_stream << "End Widget " << key << "\n\n";
        }
      }
  }
}

CQtContext::~CQtContext() {
  INFO_ASSERT(mDisableRecordStack == 0, "stack indiscipline");
  delete mNameControlMap;
  delete mWidgetNameMap;
  delete mAtomicCache;
  delete mNameToKeyMap;
  delete mKeyToNameMap;
  delete mActionNameMap;
  delete mHelpPageMap;
  delete mArgs;
  if (mRecordFile != NULL) {
    if (! mRecordFile->close()) {
      UtIO::cout() << mRecordFile->getErrmsg() << "\n";
    }
    delete mRecordFile;
  }
  if (mPlaybackFile != NULL) {
    delete mPlaybackFile;
  }
}

// Flush any recording file that might be open, but do not close it.
// This can be called from the debugger to save an event log after
// a crash or assert
void CQtContext::flush() {
  if (mRecordFile != NULL) {
    if (! mRecordFile->flush()) {
      UtIO::cout() << mRecordFile->getErrmsg() << "\n";
    }
  }
}

void CQtContext::registerWidget(CQtControl* control,
                                const char* name, bool recordEvents)
{
  if (mIgnoreRegistration)
    return;

  StringAtom* sym = mAtomicCache->intern(name);
  UtString buf;
  buf << "Cannot re-use widget name " << sym->str();

  INFO_ASSERT(mNameControlMap->find(name) == mNameControlMap->end(),
              buf.c_str());
  (*mNameControlMap)[name] = control;

  QWidget* widget = control->getWidget();
  (*mWidgetNameMap)[widget] = sym;

  // Don't bother installing the filter unless we are recording
  if (widget != NULL) {
    if ((mRecordFile != NULL) && recordEvents) {
      widget->installEventFilter(this);
    }
  }
}

void CQtContext::unregisterWidget(QWidget* widget) {
  if (mIgnoreRegistration)
    return;

  StringAtom* sym = (*mWidgetNameMap)[widget];
  INFO_ASSERT(sym, "Cannot find name for widget");
  mWidgetNameMap->erase(widget);
  CQtControl* control = (*mNameControlMap)[sym->str()];
  INFO_ASSERT(sym, "Cannot find control for widget");
  mNameControlMap->erase(sym->str());
  delete control;
}

void CQtContext::unregisterAction(QAction* action) {
  StringAtom* sym = (*mActionNameMap)[action];
  INFO_ASSERT(sym, "Cannot find name for widget");
  mActionNameMap->erase(action);
  CQtControl* control = (*mNameControlMap)[sym->str()];
  INFO_ASSERT(sym, "Cannot find control for action");
  mNameControlMap->erase(sym->str());
  delete control;
}

void CQtContext::registerAction(QAction* action, const char* name) {
  StringAtom* sym = mAtomicCache->intern(name);
  CQtAction* qta = new CQtAction(sym, action);
  (*mActionNameMap)[action] = sym;

  registerWidget(qta, name, false);
  if (mRecordFile != NULL) {
    CQT_CONNECT(action, triggered(bool), qta, triggered(bool));
    CQT_CONNECT(qta, notify(StringAtom*,bool),
                this, recordTrigger(StringAtom*,bool));
  }
}

void CQtContext::recordTrigger(StringAtom* name, bool val) {
  if (isRecording()) {
    if (strcmp(name->str(), "exit") == 0) {
      if (mExitLogged) {
        return;
      }
      mExitLogged = true;
    }
    *mRecordFile << name->str() << "trigger" << (UInt32) val << '\n';
  }
}

void CQtContext::recordText(StringAtom* name, const QString& str) {
  if (isRecording()) {
    UtString buf;
    buf << str;
    *mRecordFile << name->str() << "putValue" << buf << '\n';
  }
}

void CQtContext::recordIndex(StringAtom* name, int index) {
  if ((isRecording()) && (index >= 0)) { // -1 shows up when changing types
    *mRecordFile << name->str() << "index" << index << '\n';
  }
}

void CQtContext::recordFilename(const char* file) {
  if (isRecording()) {
    *mRecordFile << "." << "filename" << file << '\n';
  }
}

bool CQtContext::getPlaybackFile(UtString* filename) {
  readNextEvent();
  *filename = mPlaybackFilenameBuf;
  mPlaybackFilenameBuf.clear();
  return true;
}

void CQtContext::setupKeymap() {
  // Map between Qt integer keycodes and strings, so that
  // the event files are legible
  defkey("Key_Escape", Qt::Key_Escape);
  defkey("Key_Tab", Qt::Key_Tab);
  defkey("Key_Backtab", Qt::Key_Backtab);
#if defined(QT3_SUPPORT) && !defined(Q_MOC_RUN)
  defkey("Key_BackTab", Qt::Key_BackTab);
#endif
  defkey("Key_Backspace", Qt::Key_Backspace);
#if defined(QT3_SUPPORT) && !defined(Q_MOC_RUN)
  defkey("Key_BackSpace", Qt::Key_BackSpace);
#endif
  defkey("Key_Return", Qt::Key_Return);
  defkey("Key_Enter", Qt::Key_Enter);
  defkey("Key_Insert", Qt::Key_Insert);
  defkey("Key_Delete", Qt::Key_Delete);
  defkey("Key_Pause", Qt::Key_Pause);
  defkey("Key_Print", Qt::Key_Print);
  defkey("Key_SysReq", Qt::Key_SysReq);
  defkey("Key_Clear", Qt::Key_Clear);
  defkey("Key_Home", Qt::Key_Home);
  defkey("Key_End", Qt::Key_End);
  defkey("Key_Left", Qt::Key_Left);
  defkey("Key_Up", Qt::Key_Up);
  defkey("Key_Right", Qt::Key_Right);
  defkey("Key_Down", Qt::Key_Down);
  defkey("Key_PageUp", Qt::Key_PageUp);
#if defined(QT3_SUPPORT) && !defined(Q_MOC_RUN)
  defkey("Key_Prior", Qt::Key_Prior);
#endif
  defkey("Key_PageDown", Qt::Key_PageDown);
#if defined(QT3_SUPPORT) && !defined(Q_MOC_RUN)
  defkey("Key_Next", Qt::Key_Next);
#endif
  defkey("Key_Shift", Qt::Key_Shift);
  defkey("Key_Control", Qt::Key_Control);
  defkey("Key_Meta", Qt::Key_Meta);
  defkey("Key_Alt", Qt::Key_Alt);
  defkey("Key_CapsLock", Qt::Key_CapsLock);
  defkey("Key_NumLock", Qt::Key_NumLock);
  defkey("Key_ScrollLock", Qt::Key_ScrollLock);
  defkey("Key_F1", Qt::Key_F1);
  defkey("Key_F2", Qt::Key_F2);
  defkey("Key_F3", Qt::Key_F3);
  defkey("Key_F4", Qt::Key_F4);
  defkey("Key_F5", Qt::Key_F5);
  defkey("Key_F6", Qt::Key_F6);
  defkey("Key_F7", Qt::Key_F7);
  defkey("Key_F8", Qt::Key_F8);
  defkey("Key_F9", Qt::Key_F9);
  defkey("Key_F10", Qt::Key_F10);
  defkey("Key_F11", Qt::Key_F11);
  defkey("Key_F12", Qt::Key_F12);
  defkey("Key_F13", Qt::Key_F13);
  defkey("Key_F14", Qt::Key_F14);
  defkey("Key_F15", Qt::Key_F15);
  defkey("Key_F16", Qt::Key_F16);
  defkey("Key_F17", Qt::Key_F17);
  defkey("Key_F18", Qt::Key_F18);
  defkey("Key_F19", Qt::Key_F19);
  defkey("Key_F20", Qt::Key_F20);
  defkey("Key_F21", Qt::Key_F21);
  defkey("Key_F22", Qt::Key_F22);
  defkey("Key_F23", Qt::Key_F23);
  defkey("Key_F24", Qt::Key_F24);
  defkey("Key_F25", Qt::Key_F25);
  defkey("Key_F26", Qt::Key_F26);
  defkey("Key_F27", Qt::Key_F27);
  defkey("Key_F28", Qt::Key_F28);
  defkey("Key_F29", Qt::Key_F29);
  defkey("Key_F30", Qt::Key_F30);
  defkey("Key_F31", Qt::Key_F31);
  defkey("Key_F32", Qt::Key_F32);
  defkey("Key_F33", Qt::Key_F33);
  defkey("Key_F34", Qt::Key_F34);
  defkey("Key_F35", Qt::Key_F35);
  defkey("Key_Super_L", Qt::Key_Super_L);
  defkey("Key_Super_R", Qt::Key_Super_R);
  defkey("Key_Menu", Qt::Key_Menu);
  defkey("Key_Hyper_L", Qt::Key_Hyper_L);
  defkey("Key_Hyper_R", Qt::Key_Hyper_R);
  defkey("Key_Help", Qt::Key_Help);
  defkey("Key_Direction_L", Qt::Key_Direction_L);
  defkey("Key_Direction_R", Qt::Key_Direction_R);
  defkey("Key_Space", Qt::Key_Space);
  defkey("Key_Any", Qt::Key_Any);
  defkey("Key_Exclam", Qt::Key_Exclam);
  defkey("Key_QuoteDbl", Qt::Key_QuoteDbl);
  defkey("Key_NumberSign", Qt::Key_NumberSign);
  defkey("Key_Dollar", Qt::Key_Dollar);
  defkey("Key_Percent", Qt::Key_Percent);
  defkey("Key_Ampersand", Qt::Key_Ampersand);
  defkey("Key_Apostrophe", Qt::Key_Apostrophe);
  defkey("Key_ParenLeft", Qt::Key_ParenLeft);
  defkey("Key_ParenRight", Qt::Key_ParenRight);
  defkey("Key_Asterisk", Qt::Key_Asterisk);
  defkey("Key_Plus", Qt::Key_Plus);
  defkey("Key_Comma", Qt::Key_Comma);
  defkey("Key_Minus", Qt::Key_Minus);
  defkey("Key_Period", Qt::Key_Period);
  defkey("Key_Slash", Qt::Key_Slash);
  defkey("Key_0", Qt::Key_0);
  defkey("Key_1", Qt::Key_1);
  defkey("Key_2", Qt::Key_2);
  defkey("Key_3", Qt::Key_3);
  defkey("Key_4", Qt::Key_4);
  defkey("Key_5", Qt::Key_5);
  defkey("Key_6", Qt::Key_6);
  defkey("Key_7", Qt::Key_7);
  defkey("Key_8", Qt::Key_8);
  defkey("Key_9", Qt::Key_9);
  defkey("Key_Colon", Qt::Key_Colon);
  defkey("Key_Semicolon", Qt::Key_Semicolon);
  defkey("Key_Less", Qt::Key_Less);
  defkey("Key_Equal", Qt::Key_Equal);
  defkey("Key_Greater", Qt::Key_Greater);
  defkey("Key_Question", Qt::Key_Question);
  defkey("Key_At", Qt::Key_At);
  defkey("Key_A", Qt::Key_A);
  defkey("Key_B", Qt::Key_B);
  defkey("Key_C", Qt::Key_C);
  defkey("Key_D", Qt::Key_D);
  defkey("Key_E", Qt::Key_E);
  defkey("Key_F", Qt::Key_F);
  defkey("Key_G", Qt::Key_G);
  defkey("Key_H", Qt::Key_H);
  defkey("Key_I", Qt::Key_I);
  defkey("Key_J", Qt::Key_J);
  defkey("Key_K", Qt::Key_K);
  defkey("Key_L", Qt::Key_L);
  defkey("Key_M", Qt::Key_M);
  defkey("Key_N", Qt::Key_N);
  defkey("Key_O", Qt::Key_O);
  defkey("Key_P", Qt::Key_P);
  defkey("Key_Q", Qt::Key_Q);
  defkey("Key_R", Qt::Key_R);
  defkey("Key_S", Qt::Key_S);
  defkey("Key_T", Qt::Key_T);
  defkey("Key_U", Qt::Key_U);
  defkey("Key_V", Qt::Key_V);
  defkey("Key_W", Qt::Key_W);
  defkey("Key_X", Qt::Key_X);
  defkey("Key_Y", Qt::Key_Y);
  defkey("Key_Z", Qt::Key_Z);
  defkey("Key_BracketLeft", Qt::Key_BracketLeft);
  defkey("Key_Backslash", Qt::Key_Backslash);
  defkey("Key_BracketRight", Qt::Key_BracketRight);
  defkey("Key_AsciiCircum", Qt::Key_AsciiCircum);
  defkey("Key_Underscore", Qt::Key_Underscore);
  defkey("Key_QuoteLeft", Qt::Key_QuoteLeft);
  defkey("Key_BraceLeft", Qt::Key_BraceLeft);
  defkey("Key_Bar", Qt::Key_Bar);
  defkey("Key_BraceRight", Qt::Key_BraceRight);
  defkey("Key_AsciiTilde", Qt::Key_AsciiTilde);

  defkey("Key_nobreakspace", Qt::Key_nobreakspace);
  defkey("Key_exclamdown", Qt::Key_exclamdown);
  defkey("Key_cent", Qt::Key_cent);
  defkey("Key_sterling", Qt::Key_sterling);
  defkey("Key_currency", Qt::Key_currency);
  defkey("Key_yen", Qt::Key_yen);
  defkey("Key_brokenbar", Qt::Key_brokenbar);
  defkey("Key_section", Qt::Key_section);
  defkey("Key_diaeresis", Qt::Key_diaeresis);
  defkey("Key_copyright", Qt::Key_copyright);
  defkey("Key_ordfeminine", Qt::Key_ordfeminine);
  defkey("Key_guillemotleft", Qt::Key_guillemotleft);
  defkey("Key_notsign", Qt::Key_notsign);
  defkey("Key_hyphen", Qt::Key_hyphen);
  defkey("Key_registered", Qt::Key_registered);
  defkey("Key_macron", Qt::Key_macron);
  defkey("Key_degree", Qt::Key_degree);
  defkey("Key_plusminus", Qt::Key_plusminus);
  defkey("Key_twosuperior", Qt::Key_twosuperior);
  defkey("Key_threesuperior", Qt::Key_threesuperior);
  defkey("Key_acute", Qt::Key_acute);
  defkey("Key_mu", Qt::Key_mu);
  defkey("Key_paragraph", Qt::Key_paragraph);
  defkey("Key_periodcentered", Qt::Key_periodcentered);
  defkey("Key_cedilla", Qt::Key_cedilla);
  defkey("Key_onesuperior", Qt::Key_onesuperior);
  defkey("Key_masculine", Qt::Key_masculine);
  defkey("Key_guillemotright", Qt::Key_guillemotright);
  defkey("Key_onequarter", Qt::Key_onequarter);
  defkey("Key_onehalf", Qt::Key_onehalf);
  defkey("Key_threequarters", Qt::Key_threequarters);
  defkey("Key_questiondown", Qt::Key_questiondown);
  defkey("Key_Agrave", Qt::Key_Agrave);
  defkey("Key_Aacute", Qt::Key_Aacute);
  defkey("Key_Acircumflex", Qt::Key_Acircumflex);
  defkey("Key_Atilde", Qt::Key_Atilde);
  defkey("Key_Adiaeresis", Qt::Key_Adiaeresis);
  defkey("Key_Aring", Qt::Key_Aring);
  defkey("Key_AE", Qt::Key_AE);
  defkey("Key_Ccedilla", Qt::Key_Ccedilla);
  defkey("Key_Egrave", Qt::Key_Egrave);
  defkey("Key_Eacute", Qt::Key_Eacute);
  defkey("Key_Ecircumflex", Qt::Key_Ecircumflex);
  defkey("Key_Ediaeresis", Qt::Key_Ediaeresis);
  defkey("Key_Igrave", Qt::Key_Igrave);
  defkey("Key_Iacute", Qt::Key_Iacute);
  defkey("Key_Icircumflex", Qt::Key_Icircumflex);
  defkey("Key_Idiaeresis", Qt::Key_Idiaeresis);
  defkey("Key_ETH", Qt::Key_ETH);
  defkey("Key_Ntilde", Qt::Key_Ntilde);
  defkey("Key_Ograve", Qt::Key_Ograve);
  defkey("Key_Oacute", Qt::Key_Oacute);
  defkey("Key_Ocircumflex", Qt::Key_Ocircumflex);
  defkey("Key_Otilde", Qt::Key_Otilde);
  defkey("Key_Odiaeresis", Qt::Key_Odiaeresis);
  defkey("Key_multiply", Qt::Key_multiply);
  defkey("Key_Ooblique", Qt::Key_Ooblique);
  defkey("Key_Ugrave", Qt::Key_Ugrave);
  defkey("Key_Uacute", Qt::Key_Uacute);
  defkey("Key_Ucircumflex", Qt::Key_Ucircumflex);
  defkey("Key_Udiaeresis", Qt::Key_Udiaeresis);
  defkey("Key_Yacute", Qt::Key_Yacute);
  defkey("Key_THORN", Qt::Key_THORN);
  defkey("Key_ssharp", Qt::Key_ssharp);
#if defined(QT3_SUPPORT) && !defined(Q_MOC_RUN)
  defkey("Key_agrave", Qt::Key_agrave);
  defkey("Key_aacute", Qt::Key_aacute);
  defkey("Key_acircumflex", Qt::Key_acircumflex);
  defkey("Key_atilde", Qt::Key_atilde);
  defkey("Key_adiaeresis", Qt::Key_adiaeresis);
  defkey("Key_aring", Qt::Key_aring);
  defkey("Key_ae", Qt::Key_ae);
  defkey("Key_ccedilla", Qt::Key_ccedilla);
  defkey("Key_egrave", Qt::Key_egrave);
  defkey("Key_eacute", Qt::Key_eacute);
  defkey("Key_ecircumflex", Qt::Key_ecircumflex);
  defkey("Key_ediaeresis", Qt::Key_ediaeresis);
  defkey("Key_igrave", Qt::Key_igrave);
  defkey("Key_iacute", Qt::Key_iacute);
  defkey("Key_icircumflex", Qt::Key_icircumflex);
  defkey("Key_idiaeresis", Qt::Key_idiaeresis);
  defkey("Key_eth", Qt::Key_eth);
  defkey("Key_ntilde", Qt::Key_ntilde);
  defkey("Key_ograve", Qt::Key_ograve);
  defkey("Key_oacute", Qt::Key_oacute);
  defkey("Key_ocircumflex", Qt::Key_ocircumflex);
  defkey("Key_otilde", Qt::Key_otilde);
  defkey("Key_odiaeresis", Qt::Key_odiaeresis);
#endif
  defkey("Key_division", Qt::Key_division);
#if defined(QT3_SUPPORT) && !defined(Q_MOC_RUN)
  defkey("Key_oslash", Qt::Key_oslash);
  defkey("Key_ugrave", Qt::Key_ugrave);
  defkey("Key_uacute", Qt::Key_uacute);
  defkey("Key_ucircumflex", Qt::Key_ucircumflex);
  defkey("Key_udiaeresis", Qt::Key_udiaeresis);
  defkey("Key_yacute", Qt::Key_yacute);
  defkey("Key_thorn", Qt::Key_thorn);
#endif
  defkey("Key_ydiaeresis", Qt::Key_ydiaeresis);
}

void CQtContext::defkey(const char* keyname, Qt::Key key) {
  StringAtom* sym = mAtomicCache->intern(keyname);
  (*mKeyToNameMap)[(int) key] = sym;
  (*mNameToKeyMap)[sym] = (int) key;
}

bool CQtContext::eventFilter(QObject* obj, QEvent* event) {
  QWidget* widget = qobject_cast<QWidget*>(obj);
  QEvent::Type eventType = event->type();
  if ((widget != NULL) && isRecording()) {
    WidgetNameMap::iterator p = mWidgetNameMap->find(widget);
    if (p != mWidgetNameMap->end()) {
      StringAtom* sym = p->second;
      CQtControl* control = (*mNameControlMap)[sym->str()];
      INFO_ASSERT(control, "Control map sanity");
      const char* widgetName = sym->str();
      bool empty = false;

      switch (eventType) {
#if 0
      case QEvent::Enter:
        *mRecordFile << widgetName << "enter";
        break;
      case QEvent::Leave:
        *mRecordFile << widgetName << "leave";
        break;
      case QEvent::FocusIn:
        *mRecordFile << widgetName << "focusIn";
        break;
      case QEvent::FocusOut:
        *mRecordFile << widgetName << "focusOut";
        break;
      case QEvent::WindowActivate:
        *mRecordFile << widgetName << "windowActivate";
        break;
      case QEvent::WindowDeactivate:
        *mRecordFile << widgetName << "windowDeactivate";
        break;
#endif
      case QEvent::KeyPress:
        recordKey(widgetName, "keyPress", (QKeyEvent*)event);
        break;
      case QEvent::KeyRelease:
        recordKey(widgetName, "keyRelease", (QKeyEvent*)event);
        break;
      case QEvent::MouseButtonPress:
        recordMouse(control, widgetName, "mousePress", (QMouseEvent*)event);
        break;
      case QEvent::MouseButtonRelease:
        recordMouse(control, widgetName, "mouseRelease", (QMouseEvent*)event);
        break;
      case QEvent::MouseButtonDblClick:
        recordMouse(control, widgetName, "mouseDoubleClick", (QMouseEvent*)event);
        break;
      default:
        empty = true;
        break;
      } // switch
      if (!empty) {
        *mRecordFile << '\n';
      }
    } // if
  } // if
  return false;
} // bool CQtContext::eventFilter

void CQtContext::addCustomArgs()
{
  // Derived class can reimplement this
}

void CQtContext::recordPutValue(QWidget* widget, const char* text) {
  if (isRecording()) {
    StringAtom* sym = (*mWidgetNameMap)[widget];
    INFO_ASSERT(sym, "Could not find widget for update event");
    *mRecordFile << sym->str() << "putValue" << text << '\n';
  }
}

void CQtContext::recordKey(const char* widgetName, const char* eventName,
                    QKeyEvent* keyEvent)
{
  if (isRecording()) {
    *mRecordFile << widgetName << eventName;
    Qt::Key key = (Qt::Key) keyEvent->key();
    KeyToNameMap::iterator p = mKeyToNameMap->find(key);

    // If we can't find the key in the map, just use the number
    if (p == mKeyToNameMap->end()) {
      *mRecordFile << UtIO::raw << "0x" << UtIO::hex << ((UInt32) key)
                   << UtIO::slashify;
    }
    else {
      StringAtom* keyName = p->second;
      *mRecordFile << keyName->str();
    }
    *mRecordFile << keyEvent->modifiers();
    *mRecordFile << keyEvent->text();
    // ??? use keyEvent->text()????
  }
}

QEvent* CQtContext::parseKeyEvent(QEvent::Type type, UtIStringStream& line) {
  UtString keystring, text;
  UInt32 modifiers;
  if ((line >> keystring) &&
      (line >> UtIO::hex >> modifiers) &&
      (line >> text))
  {
    int key = -1;
    if (strncmp(keystring.c_str(), "0x", 2) == 0) {
      StrToken tok(keystring.c_str() + 2);
      UInt32 ukey;
      if (tok.parseNumber(&ukey, 16)) {
        key = ukey;
      }
    }
    else {
      StringAtom* sym = mAtomicCache->intern(keystring);
      NameToKeyMap::iterator p = mNameToKeyMap->find(sym);
      if (p != mNameToKeyMap->end()) {
        key = p->second;
      }
    }
    return new QKeyEvent(type, key,
                         (Qt::KeyboardModifiers) modifiers,
                         text.c_str());
  } // if
  return NULL;
} // QEvent* CQtContext::parseKeyEvent

void CQtContext::recordMouse(CQtControl* control, const char* widgetName,
                             const char* eventName, QMouseEvent* mouseEvent)
{
  if (isRecording()) {
    UtString location, errmsg;
    if (control->XYToLocation(mouseEvent->x(), mouseEvent->y(), &location,
                              &errmsg))
    {
      *mRecordFile << widgetName << eventName
                   << UtIO::hex << mouseEvent->modifiers()
                   << ((UInt32) mouseEvent->button())
                   << ((UInt32) mouseEvent->buttons())
                   << location;
    }
  }
}

QEvent* CQtContext::parseMouseEvent(CQtControl* control,
                                    QEvent::Type type, UtIStringStream& line)
{
  UInt32 modifiers, button, buttons;
  UtString location, errmsg;
  int x, y;
  if ((line >> UtIO::hex >> modifiers) &&
      (line >> UtIO::hex >> button) &&
      (line >> UtIO::hex >> buttons) &&
      (line >> location) &&
      control->locationToXY(location.c_str(), &x, &y, &errmsg))
  {
    return new QMouseEvent(type, QPoint(x, y),
                           (Qt::MouseButton) buttons,
                           (Qt::MouseButtons) buttons,
                           (Qt::KeyboardModifiers) modifiers);
  }
  return NULL;
}

void CQtContext::playEvent(const char* buf) {
  UtIStringStream line(buf);
  UtString widgetName, eventType;
  line >> UtIO::slashify >> widgetName >> eventType;
  StringAtom* sym = NULL;
  CQtControl* control = NULL;
  QWidget* widget = NULL;

  QEvent* event = NULL;
  if (widgetName != ".") {
    sym = mAtomicCache->intern(widgetName);
    control = (*mNameControlMap)[sym->str()];
    if (control == NULL) {
      UtIO::cout() << "Cannot find control " << widgetName << "\n";
      abort();
    }
    widget = control->getWidget();
    if (eventType == "enter") {
      event = new QEvent(QEvent::Enter);
    }
    else if (eventType == "leave") {
      event = new QEvent(QEvent::Leave);
    }
    else if (eventType == "windowActivate") {
      event = new QEvent(QEvent::WindowActivate);
    }
    else if (eventType == "windowDeactivate") {
      event = new QEvent(QEvent::WindowDeactivate);
    }
    else if (eventType == "focusIn") {
      event = new QFocusEvent(QEvent::FocusIn, Qt::OtherFocusReason);
    }
    else if (eventType == "focusOut") {
      event = new QFocusEvent(QEvent::FocusOut, Qt::OtherFocusReason);
    }
    else if (eventType == "keyPress") {
      event = parseKeyEvent(QEvent::KeyPress, line);
    }
    else if (eventType == "keyRelease") {
      event = parseKeyEvent(QEvent::KeyRelease, line);
    }
    else if (eventType == "mousePress") {
      event = parseMouseEvent(control, QEvent::MouseButtonPress, line);
    }
    else if (eventType == "mouseRelease") {
      event = parseMouseEvent(control, QEvent::MouseButtonRelease, line);
    }
    else if (eventType == "mouseDoubleClick") {
      event = parseMouseEvent(control, QEvent::MouseButtonDblClick, line);
    }
  } // if
  if (event != NULL) {
    if (!sendEvent(widget, event)) {
      UtIO::cout() << widgetName
                   << ": Qt did not like sending event: " << buf << "\n";
      abort();
    }
    delete event;
  }
  else {
    if (eventType == "putValue") {
      UtString buf;
      if (! (line >> buf)) {
        UtIO::cout() << "error : " << widgetName << " : "
                     << line.getErrmsg() << "\n";
      }
      else {
        // parse the update event and send it to the control
        UtString errmsg;
        if (!control->putValue(buf.c_str(), &errmsg)) {
          UtIO::cout() << "error : " << widgetName << " : "
                       << errmsg.c_str() << "\n";
        }
      }
      return;
    }
    else if (eventType == "index") {
      int idx;
      if (! (line >> UtIO::hex >> idx)) {
        UtIO::cout() << "error : " << widgetName << " : "
                     << line.getErrmsg() << "\n";
      }
      else {
        // parse the update event and send it to the control
        UtString errmsg;
        if (!control->putIndex(idx, &errmsg)) {
          UtIO::cout() << "error : " << widgetName << " : "
                       << errmsg.c_str() << "\n";
        }
      }
      return;
    }
    else if (eventType == "trigger") {
      UtString errmsg;
      (void) control->putValue("trigger!", &errmsg);
      return;
    }
    else if (eventType == "filename") {
      if (! (line >> mPlaybackFilenameBuf)) {
        UtIO::cout() << "error : " << widgetName << " : "
                     << line.getErrmsg() << "\n";
      }
      return;
    }
    else {
      UtIO::cout() << widgetName
                   << " : Cannot find event type: " << eventType << "\n";
    }
  } // else
} // void CQtContext::playEvent

class CQtGenericControl : public CQtControl {
public:
  CQtGenericControl(QWidget* widget) : mWidget(widget) {}

  virtual void dumpState(UtOStream&) {
  }

  virtual void getValue(UtString* str) const {
    str->clear();
  }

  virtual bool putValue(const char* /*str*/, UtString* errmsg) {
    errmsg->clear();
    return true;
  }
  
  virtual QWidget* getWidget() const {return mWidget;}

private:
  QWidget* mWidget;
};

class CQtTabBarControl : public CQtGenericControl {
public:
  CQtTabBarControl(QTabBar* widget):
    CQtGenericControl(widget),
    mTabBar(widget)
  {}

  virtual bool XYToLocation(int x, int y, UtString* location,
                            UtString* errmsg)
  {
    for (UInt32 i = 0, n = mTabBar->count(); i < n; ++i) {
      QRect rect = mTabBar->tabRect(i);
      if (rect.contains(x, y)) {
        *location << i;
        return true;
      }
    }
    *errmsg << "NotFound";
    return false;
  }

  virtual bool locationToXY(const char* location, int* x, int* y,
                            UtString* errmsg)
  {
    UInt32 index;
    UtIStringStream is(location);
    if (is >> index) {
      if (index < (UInt32) mTabBar->count()) {
        QRect rect = mTabBar->tabRect(index);
        QPoint center = rect.center();
        *x = center.x();
        *y = center.y();
        return true;
      }
      else {
        *errmsg << "index " << location << " out of bounds";
      }
    }
    else {
      *errmsg << "could not parse index from " << location;
    }
    return false;
  } // virtual bool locationToXY

private:
  QTabBar* mTabBar;
};


class CQtFilenameControl : public CQtControl {
public:
  CQtFilenameControl(CFilenameEdit* fe):
    mFilenameEdit(fe)
  {}
    
  virtual void dumpState(UtOStream& stream) {
    UtString str = mFilenameEdit->getFilename();
    stream << str << UtIO::endl;
  }

  virtual void getValue(UtString* str) const {
    *str = mFilenameEdit->getFilename();
  }
    
  virtual bool putValue(const char* str, UtString* /* errmsg */) {
    mFilenameEdit->putFilename(str);
    return true;
  }

  virtual QWidget* getWidget() const {return mFilenameEdit;}

private:
  CFilenameEdit* mFilenameEdit;
};

//! construct a recordable, replayable QTextEdit
CTextEdit* CQtContext::textEdit(const char* widgetName, CTextEdit* w) {
  CTextEdit* widget = w;
  if (widget == NULL)
    widget = new CTextEdit;

  registerWidget(new CQtTextEditControl(widget, this), widgetName,
                 false);
  CQT_CONNECT(widget, textChanged(), this, setModified());
  return widget;
}

//! construct a recordable, replayable QLineEdit
QLineEdit* CQtContext::lineEdit(const char* widgetName, QLineEdit* w) {
  QLineEdit* widget = w;
  if (widget == NULL)
    widget = new QLineEdit;
  registerWidget(new CQtLineEditControl(widget, this), widgetName,
                 false);
  CQT_CONNECT(widget, textChanged(const QString&), this, setModified());
  return widget;
}

//! construct a recordable, replayable QSpinBox
QSpinBox* CQtContext::spinBox(const char* widgetName, QSpinBox* w) {
  QSpinBox* widget = w;
  if (widget == NULL)
    widget = new QSpinBox;
  registerWidget(new CQtSpinBoxControl(widget, this), widgetName,
                 false);
  CQT_CONNECT(widget, valueChanged(int), this, setModified());
  return widget;
}

//! construct a recordable, replayable CHexSpinBox
CHexSpinBox* CQtContext::hexSpinBox(const char* widgetName, CHexSpinBox* w) {
  CHexSpinBox* widget = w;
  if (widget == NULL)
    widget = new CHexSpinBox;
  registerWidget(new CQtHexSpinBoxControl(widget, this), widgetName,
                 false);
  CQT_CONNECT(widget, valueChanged(const QString&), this, setModified());
  return widget;
}

//! construct a recordable, replayable QTreeWidget
QTreeWidget* CQtContext::treeWidget(const char* widgetName, QTreeWidget* w) {
  QTreeWidget* widget = w;
  if (widget == NULL)
    widget = new QTreeWidget;
  registerWidget(new CQtGenericControl(widget), widgetName, true);

  // actions on the tree widget don't cause the document to be modified

  return widget;
}

//! construct a recordable, replayable QTreeWidget
CDragDropTreeWidget* CQtContext::dragDropTreeWidget(const char* widgetName,
                                                    bool enaDrop, CDragDropTreeWidget* w)
{
  CDragDropTreeWidget* widget = w;
  if (widget == NULL)
    widget = new CDragDropTreeWidget(enaDrop);
  
  // This is highly annoying, but event filtering does not appear
  // to work on QTreeWidgets.  I will need to get help from Trolltech.
  // It looks like we are going to have to capture the interaction with
  // signals, which is a much thicker interface.
  // 
  //   registerWidget(new CQtGenericControl(widget), widget, widgetName);
  //   registerChildren(widget, widgetName);

  CQtDragDropWidgetControl* control = new CQtDragDropWidgetControl(widget,
                                                                   this);
  registerWidget(control, widgetName, false);  
  return widget;
}

void CQtContext::registerChildren(QWidget* widget, const char* widgetName) {
  UtString buf;
  UInt32 index = 0;
  QObjectList children = widget->children();
  for (QObjectList::iterator p = children.begin(), e = children.end();
       p != e; ++p)
  {
    QObject* obj = *p;
    if (obj->isWidgetType()) {
      QWidget* child = (QWidget*) obj;
      buf.clear();
      buf << widgetName << "_child" << index;
      ++index;
      registerWidget(new CQtGenericControl(child), buf.c_str(), true);
      registerChildren(child, buf.c_str());
    }
  }
}

//! construct a recordable, replayable QComboBox
QComboBox* CQtContext::comboBox(const char* widgetName, QComboBox* w) {
  QComboBox* widget = w;
  if (widget == NULL)
    widget = new QComboBox;

  registerWidget(new CQtComboBoxControl(widget),
                 widgetName, false);

  StringAtom* sym = mAtomicCache->intern(widgetName);
  CQtAction* qta = new CQtAction(sym, NULL);
  CQT_CONNECT(widget, editTextChanged(const QString&),
              qta, textChanged(const QString&));
  CQT_CONNECT(widget, currentIndexChanged(int),
              qta, indexChanged(int)); // Qt 4.1 dependency
  CQT_CONNECT(qta, notifyText(StringAtom*,const QString&),
              this, recordText(StringAtom*,const QString&));
  CQT_CONNECT(qta, notifyIndex(StringAtom*,int),
              this, recordIndex(StringAtom*,int));
  CQT_CONNECT(widget, currentIndexChanged(int), this, setModified());
  CQT_CONNECT(widget, editTextChanged(const QString&), this, setModified());

  return widget;
}

//! construct a recordable, replayable QTabWidget
CTabWidget* CQtContext::tabWidget(const char* widgetName, CTabWidget* w, QWidget* parent) {
  CTabWidget* widget = w;
  if (widget == NULL)
  {
    if (parent)
      widget = new CTabWidget(parent);
    else
      widget = new CTabWidget;
  }

  registerWidget(new CQtGenericControl(widget), widgetName, true);
  QTabBar* tabBar = widget->getTabBar();
  UtString bar(widgetName);
  bar << "_tabbar";
  registerWidget(new CQtTabBarControl(tabBar), bar.c_str(), true);
  return widget;
}

//! construct a recordable, replayable QPushButton
QPushButton* CQtContext::pushButton(const char* widgetName, const char* label, QPushButton* w) {
  QPushButton* widget = w;
  if (widget == NULL)
    widget = new QPushButton(label);
  registerWidget(new CQtPushButtonControl(widget, this),
                 widgetName,
                 false);

  // this is pessimistic.  Not every button change results in the
  // document state being modified.
  CQT_CONNECT(widget, clicked(), this, setModified());
  return widget;
}

//! construct a recordable, replayable QProcess
QProcess* CQtContext::process(const char* widgetName, QProcess* p) {
  QProcess* proc = p;
  if (proc == NULL)
    proc = new QProcess;
  registerWidget(new CQtProcessControl(proc, this),
                 widgetName, false);
  return proc;
}

//! construct a recordable, replayable QRadioButton
QRadioButton* CQtContext::radioButton(const char* widgetName, const char* label, QRadioButton* w) {
  QRadioButton* widget = w;
  if (widget == NULL)
    widget = new QRadioButton(label);
  registerWidget(new CQtRadioButtonControl(widget, this), widgetName, false);
  CQT_CONNECT(widget, toggled(bool), this, setModified());
  return widget;
}

//! construct a recordable, replayable QCheckBox
QCheckBox* CQtContext::checkBox(const char* widgetName, const char* label, QCheckBox* w) {
  QCheckBox* widget = w;
  if (widget == NULL)
    widget = new QCheckBox(label);
  registerWidget(new CQtCheckBoxControl(widget, this), widgetName, false);
  CQT_CONNECT(widget, stateChanged(int), this, setModified());
  return widget;
}

//! construct a filename field with a browse button
CFilenameEdit* CQtContext::filenameEdit(const char* widgetName,
                                        bool readFile, const char* prompt,
                                        const char* filter)
{
  CFilenameEdit* widget = new CFilenameEdit(widgetName, readFile,
                                            prompt, filter, this);
  registerWidget(new CQtFilenameControl(widget), widgetName, true);
  CQT_CONNECT(widget, filenameChanged(const char*), this, setModified());
  return widget;
}

bool CQtContext::popupFileDialog(QWidget* widget,
                                 bool readFile,
                                 const char* prompt,
                                 const char* filter,
                                 UtString* result)
{
  bool ret = false;
  if (isPlaybackActive()) {
    ret = getPlaybackFile(result);
  }
  else {
    QString qfile;
    if (readFile) {
      qfile = QFileDialog::getOpenFileName(widget, prompt, result->c_str(),
                                           filter);
    }
    else {
      qfile = QFileDialog::getSaveFileName(widget, prompt, result->c_str(),
                                           filter);
    }
    result->clear();
    *result << qfile;
    ret = !result->empty();
    if (isRecordActive()) {
      recordFilename(result->c_str());
    }
  }
  return ret;
} // bool CQtContext::popupFileDialog

void CQtContext::warning(QWidget* widget, const QString& msg) {
  bool do_message_box = (mPlaybackFile == NULL) && (mRecordFile == NULL);
  UtIO::cerr() << msg << UtIO::endl;
  UtIO::cerr().flush();
  if (do_message_box) {
    QMessageBox::warning(widget, tr(APP_NAME), msg);
  }
}

CQtAbortOverride::CQtAbortOverride(const char* appName, QWidget* widget,
                                   CQtContext* cqt)
  : mAppName(appName), mWidget(widget), mCQt(cqt), mDoFlush(true)
{
}

CQtAbortOverride::~CQtAbortOverride() {
}

void CQtAbortOverride::printHandler(const char* msg) {
  mCQt->warning(mWidget, msg);
}


void CQtAbortOverride::abortHandler() {
  if (mDoFlush) {
    mDoFlush = false;           // avoid potential infinite recursion
    mCQt->flush();              // finish recording events
  }
  ::abort();
}

QWidget* CQt::hSplit(QWidget* w1, QWidget* w2, QWidget* w3) {
  QSplitter* split = new QSplitter;
  split->addWidget(w1);
  split->addWidget(w2);
  if (w3 != NULL) {
    split->addWidget(w3);
  }
  return split;
}

QWidget* CQt::vSplit(QWidget* w1, QWidget* w2, QWidget* w3) {
  QSplitter* split = new QSplitter(Qt::Vertical);
  split->addWidget(w1);
  split->addWidget(w2);
  if (w3 != NULL) {
    split->addWidget(w3);
  }
  return split;
}

QGroupBox* CQt::makeGroup(const char* name, QLayout* layout,
                        bool fixedVertical)
{
  QGroupBox* box  = new QGroupBox(name);
  if (fixedVertical) {
    box->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  }
  box->setLayout(layout);
  return box;
}

QGroupBox* CQtContext::makeGroup(const char* name, const char* tag,
                               QLayout* layout, bool fixedVertical)
{
  QGroupBox* box  = new CGroupBoxHelp(this, name, tag);
  if (fixedVertical) {
    box->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  }
  box->setLayout(layout);
  return box;
}

QGroupBox* CQt::makeGroup(const char* name, QWidget* widget, bool fixedVertical)
{
  return makeGroup(name, vbox(widget), fixedVertical);
}

QGroupBox* CQtContext::makeGroup(const char* name, const char* tag,
                               QWidget* widget, bool fixedVertical)
{
  return makeGroup(name, tag, CQt::vbox(widget), fixedVertical);
}

QVBoxLayout* CQt::vbox(QWidget* w1, QWidget* w2, QWidget* w3, QWidget* w4,
                       QWidget* w5)
{
  QVBoxLayout *vbox = new QVBoxLayout;
  if (w1 != NULL) {
    vbox->addWidget(w1);
  }
  if (w2 != NULL) {
    vbox->addWidget(w2);
  }
  if (w3 != NULL) {
    vbox->addWidget(w3);
  }
  if (w4 != NULL) {
    vbox->addWidget(w4);
  }
  if (w5 != NULL) {
    vbox->addWidget(w5);
  }
  return vbox;
}

QLayout* CQt::hbox(QWidget* w1, QWidget* w2, QWidget* w3, QWidget* w4,
                   QWidget* w5, QWidget* w6, QWidget* w7, QWidget* w8,
                   QWidget* w9, QWidget* w10, QWidget* w11)
{
  QHBoxLayout *hbox = new QHBoxLayout;
  if (w1 != NULL) {
    hbox->addWidget(w1);
  }
  if (w2 != NULL) {
    hbox->addWidget(w2);
  }
  if (w3 != NULL) {
    hbox->addWidget(w3);
  }    
  if (w4 != NULL) {
    hbox->addWidget(w4);
  }    
  if (w5 != NULL) {
    hbox->addWidget(w5);
  }    
  if (w6 != NULL) {
    hbox->addWidget(w6);
  }    
  if (w7 != NULL) {
    hbox->addWidget(w7);
  }    
  if (w8 != NULL) {
    hbox->addWidget(w8);
  }    
  if (w9 != NULL) {
    hbox->addWidget(w9);
  }    
  if (w10 != NULL) {
    hbox->addWidget(w10);
  }    
  if (w11 != NULL) {
    hbox->addWidget(w11);
  }    
  return hbox;
}
QWidget* CQt::makeWidget(QLayout* layout) {
  QWidget* w = new QWidget;
  w->setLayout(layout);
  return w;
}

QColor CQt::getDefaultTextColor()
{
  // Get the default text color of a tree widget item
  QTreeWidget tmpTree;
  tmpTree.setColumnCount(1);
  QTreeWidgetItem tmpItem(&tmpTree);
  return tmpItem.textColor(0);
}

QLayout& operator<<(QLayout& mgr, QLayout* sub) {
  QWidget* w = CQt::makeWidget(sub);
  //w->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  mgr.addWidget(w);
  return mgr;
}

QLayout& operator<<(QLayout& mgr, QWidget* widget) {
  //widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  mgr.addWidget(widget);
  return mgr;
}    

void CQtContext::enableRecord() {
  INFO_ASSERT(mDisableRecordStack > 0, "stack indiscipline");
  --mDisableRecordStack;
}

void CQtContext::disableRecord() {
  ++mDisableRecordStack;
}

void CQtContext::launchHelp(const char* helpTag) {
  const char* carbon_home = getenv("CARBON_HOME");

  // find the help page URL
  const char *helpPage = (*mHelpPageMap)[helpTag];
  if (helpPage == NULL) {
    helpPage = (*mHelpPageMap)["default"];
  }

  // display the requested page in the help viewer
  if (helpPage != NULL && carbon_home != NULL) {
#if defined(Q_OS_WIN32)
    const char sep = '\\';
#else
    const char sep = '/';
#endif

    // all help URLS are relative to CARBON_HOME/userdoc/soc_html/output
    UtString fname("file://");
    //fname << carbon_home << sep << "userdoc" << sep << "soc_html" << sep
    //      << "output" << sep << helpPage;
    fname << carbon_home << sep << "userdoc" << sep << "soc_html" << sep
          << sep << helpPage;

    launchHelpUrl(fname.c_str());
  }
}

void CQtContext::launchHelpUrl(const char* url) {
#if defined(Q_OS_WIN32)
#define BROWSER_NAME_BUF_LEN 1000
  // On Windows, look in the registry for the default web browser,
  // and open the help page with it.  We do not use the QtAssistant
  // on windows because it does not have GIF support built in, and I
  // have not yet figured out how to add it.
  char browserName[BROWSER_NAME_BUF_LEN];
  long browserNameBufLen = BROWSER_NAME_BUF_LEN;
  if (RegQueryValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Clients\\StartMenuInternet\\",
                    browserName, &browserNameBufLen) == ERROR_SUCCESS) {
    
    ShellExecute(NULL,           // window handle.  NULL means go get your own window.
                 "open",         // verb: open, edit, explore, find, print
                 browserName,    // program to execute 
                 url,            // program arguments
                 NULL,           // default directory
                 SW_SHOWNORMAL); // show the window (restore if minimized, etc.)
  }
#else
  const char* carbon_home = getenv("CARBON_HOME");

  // create a help viewer if necessary
  if (mHelpWindow == NULL) {
    mHelpWindow = new Assistant(carbon_home);
  }
  
  // display the page
  mHelpWindow->show(url);
#endif
}

void CQtContext::setModified() {
  // We don't want to set the modified flag if we are making a programmatic
  // change to a widget value, so look at mDisableRecordStack.
  if ((mMainWindow != NULL) && (mDisableRecordStack == 0)) {
    mMainWindow->setWindowModified(true);
  }
}

QColor CQtContext::getForegroundColor() {
  const QPalette& my_palette = palette();
  QColor color = my_palette.color(QPalette::WindowText);
  return color;
}

void CQtContext::initializeHelpPages()
{
#define HELP_PAGE(TAG, URL) (*mHelpPageMap)[TAG] = URL;
#include "HelpPages.h"
#undef HELP_PAGE
}

void CQtContext::recordExit() {
  if (isRecording() && !mExitLogged) {
    *mRecordFile << "exit" << "trigger" << "0" << '\n';
    mExitLogged = true;
  }
}

void CQt::addGridRow(QGridLayout* grid, const char* prompt, QWidget* widget) {
  UInt32 row = grid->rowCount();
  QLabel* label = new QLabel(prompt);
  grid->addWidget(label, row, 0);
  grid->addWidget(widget, row, 1);
}  

void CQt::addGridRow(QGridLayout* grid, const char* prompt, QLayout* layout) {
  UInt32 row = grid->rowCount();
  QLabel* label = new QLabel(prompt);
  grid->addWidget(label, row, 0);
  grid->addLayout(layout, row, 1);
}  
