//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ast_common.h>
#include <agraph.h>

//#include <libgraph.h>

typedef struct mydata_s {int x,y,z;} mydata;

void print_attr(void* node, char* attr) {
  printf("%s = %s\n", attr, agget(node, attr));
}

// gcc4 does not allow conversion of literal strings to char*, so cast
#define AGGET(obj, name) agget(obj, (char*) name)

int main(int /* argc */, char ** /*argv */) {
  Agraph_t *g;
  Agnode_t *v;
  Agedge_t *e;
  //Agsym_t *attr;
  //Dict_t *d;
  int edge_cnt;
  int node_cnt;
  //mydata *p;
  //Agsym_t* sym = 0;

  if ((g = agread(stdin, 0)) != NULL) {
#if 0
    /* dtsize() is a Libdict primitive */
    /*fprintf(stderr,"%s has %d node attributes", "??", dtsize(agdictof(g)));*/
    attr = agattr(g,AGNODE,"color","blue");

    /* create a newgraph */
    h=agopen("tmp",g->desc);
#endif

/*
    while (sym = agnxtattr(g, AGNODE, sym)) {
      printf("Attribute %s = %s (default)\n", sym->name, sym->defval);
    }
*/

    /* print bounding box */
    fprintf(stdout, "b %s\n", AGGET(g, "bb"));

    /* this is a way of counting all the edges of the graph */
    edge_cnt = 0;
    node_cnt = 0;
    for (v = agfstnode(g); v; v = agnxtnode(v)) {
      /*fprintf(stdout, "Node %d\n", node_cnt);
        ++node_cnt;*/

      fprintf(stdout, "n %s %s\n", agnameof(v) + 1, AGGET(v, "pos"));
      for (e = agfstout(v); e; e = agnxtout(e)) {
        edge_cnt++;
        fprintf(stdout, "e %s %s %s\n", agnameof(v) + 1, agnameof(e->node) + 1,
                AGGET(e, "pos"));
      }

      /* using inline functions or macros, attach records to edges */
      /* agflatten(g); */
#if 0
      for (v = agfstn(g); v; v = agnxtn(v)) {
        for (e = agfout(v); e; e; = agnxte(e)) {
          p=(mydata*) agnewrec(g,e,"mydata",sizeof(mydata));
          p->x = 27; /* meaningless example */
        }
      }
#endif
    }
  }
  return 0;
}
