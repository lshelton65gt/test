/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "cbrowser.h"
#include "hbrowser.h"
#include <QTreeWidgetItem>
#include <QVBoxLayout>
#include <QStringList>
#include <QTimer>
#include <cassert>
#include "util/SourceLocator.h"
#include "util/UtIStream.h"
#include "util/AtomicCache.h"
#include "util/UtStringUtil.h"
#include "util/Zstream.h"
#include "textedit.h"
#include "gui/CDragDropTreeWidget.h"
#include <QtGui>
#define APP_NAME "Carbon Component Wizard"

class CGraph;

CarbonCBrowser::CarbonCBrowser(CQtContext* cqt,
                               const char* name,
                               DriverDatabase* drivers, CarbonDB* db,
                               const CarbonDBNode* node)
  : mDB(db), mDrivers(drivers), mNode(node), mCQt(cqt)
{
  setAttribute(Qt::WA_DeleteOnClose);
  QStringList headers;
  UtString widgetName;

  UtString namebuf;

  namebuf << name << "_drivers";
  mDriverTree = cqt->dragDropTreeWidget(namebuf.c_str(), true);
  mDriverTree->setSelectionMode(QAbstractItemView::ExtendedSelection);
  mDriverTree->setAlternatingRowColors(true);
  mDriverTree->setColumnCount(2);
  headers.clear();
  headers << "Path";
  headers << "Source Line";
  mDriverTree->setHeaderLabels(headers);
  
  namebuf.clear();
  namebuf << name << "_loads";
  mLoadTree = cqt->dragDropTreeWidget(namebuf.c_str(), true);
  mLoadTree->setSelectionMode(QAbstractItemView::ExtendedSelection);
  mLoadTree->setAlternatingRowColors(true);
  mLoadTree->setColumnCount(2);
  headers.clear();
  headers << "Path";
  headers << "Source Line";
  mLoadTree->setHeaderLabels(headers);
  
  namebuf.clear();
  namebuf << name << "_fanin";
  mFaninTree = cqt->dragDropTreeWidget(namebuf.c_str(), true);
  mFaninTree->setSelectionMode(QAbstractItemView::ExtendedSelection);
  mFaninTree->setAlternatingRowColors(true);
  mFaninTree->setColumnCount(2);
  headers.clear();
  headers << "Name";
  headers << "Properties";
  mFaninTree->setHeaderLabels(headers);
  
  CQT_CONNECT(mDriverTree, selectionChanged(),
              this, changeDriverSel());
  CQT_CONNECT(mLoadTree, selectionChanged(),
              this, changeLoadSel());
  CQT_CONNECT(mFaninTree, selectionChanged(),
              this, changeFaninTreeSel());
  CQT_CONNECT(mDriverTree, itemDoubleClicked(QTreeWidgetItem*,int),
              this, popupSource(QTreeWidgetItem*,int));
  CQT_CONNECT(mLoadTree, itemDoubleClicked(QTreeWidgetItem*,int),
              this, popupSource(QTreeWidgetItem*,int));
  CQT_CONNECT(mDriverTree, dropReceived(const char*,QTreeWidgetItem*),
              this, dropNet(const char*,QTreeWidgetItem*));
  CQT_CONNECT(mFaninTree, dropReceived(const char*,QTreeWidgetItem*),
              this, dropNet(const char*,QTreeWidgetItem*));
  CQT_CONNECT(mLoadTree, dropReceived(const char*,QTreeWidgetItem*),
              this, dropNet(const char*,QTreeWidgetItem*));

  setCentralWidget(buildLayout());
  
  setFocus(node);
} // CarbonCBrowser::CarbonCBrowser

void CarbonCBrowser::setFocus(const CarbonDBNode* node) {
  showDrivers(node, mDrivers->loopReaders(node), mLoadTree);
  showDrivers(node, mDrivers->loopDrivers(node), mDriverTree);

  UtString title;
  title << "Carbon Connectivity" << " - "
        << carbonDBNodeGetFullName(mDB, node);
  setWindowTitle(title.c_str());
}

void CarbonCBrowser::clear() {
  mDriverTree->clear();
  mLoadTree->clear();
  mFaninTree->clear();
  mItemDriverMap.clear();
  mItemNetMap.clear();
  mItemFaninMap.clear();
}

CarbonCBrowser::~CarbonCBrowser() {
}

QWidget* CarbonCBrowser::buildLayout() {
  QWidget* drivers = CQt::makeGroup("Drivers", CQt::vbox(mDriverTree));
  QWidget* loads = CQt::makeGroup("Loads", CQt::vbox(mLoadTree));
#if 0
  QWidget* aliases = CQt::makeGroup("Aliases", CQt::vbox(mAliasTree));
#endif
  QWidget* fanin = CQt::makeGroup("Fanin", CQt::vbox(mFaninTree));
    
  QWidget* layout = CQt::hSplit(drivers, loads, fanin);
  return layout;
}

#if 0
void CarbonCBrowser::showAliases(const CarbonDBNode* node) {
  mAliasTree->clear();

  // See whether this node has any aliases or drivers.  If so,
  // add placeholders so the user can get to those by expanding,
  // but don't fill them in yet.
  CarbonDBNodeIter* iter = carbonDBLoopAliases(mDB, node);
  if (iter != NULL) {
    const CarbonDBNode* alias = NULL;
    UtStringArray aliases;

    while ((alias = carbonDBNodeIterNext(iter)) != NULL) {
      const char* leafName = carbonDBNodeGetLeafName(mDB, alias);
      if (*leafName != '$') {
        const char* fullName = carbonDBNodeGetFullName(mDB, alias);
        aliases.push_back(fullName);
      }
    }
    carbonDBFreeNodeIter(iter);

    std::sort(aliases.begin(), aliases.end());
    for (UInt32 i = 0, n = aliases.size(); i < n; ++i) {
      QTreeWidgetItem* aliasItem = new QTreeWidgetItem(mAliasTree);
      aliasItem->setText(0, aliases[i]);
    }
  }
}
#endif

void CarbonCBrowser::showDriver(const CarbonDBNode* node,
                                Driver* driver,
                                CDragDropTreeWidget* tree,
                                QTreeWidgetItem* parent)
{
  // If the driver in the drivers database did not exist in the DB
  // symbol table, we need to create the Driver node to read in the
  // databse properly.  But we shouldn't do anything with that node.
  if (driver->getDriverPath() != NULL) {
    QTreeWidgetItem* driverItem = NULL;
    if (parent == NULL) {
      driverItem = new QTreeWidgetItem(tree);
    }
    else {
      driverItem = new QTreeWidgetItem(parent);
    }
    const char* path = carbonDBNodeGetFullName(mDB, driver->getDriverPath());
    driverItem->setText(0, path);
    UtString buf, loc;
    driver->getLoc().compose(&loc);
    const char* type = driver->getType();
    buf << type << " " << driver->getBits() << " " << loc;
    driverItem->setText(1, buf.c_str());
    mItemDriverMap[driverItem] = driver;
    tree->expandItem(driverItem);

    for (DriverLoop p = driver->loopNested(); !p.atEnd(); ++p) {
      Driver* nested = *p;
      showDriver(node, nested, tree, driverItem);
    }

    // For port connections, show the fanin with nesting
    if (strncmp(type, "PortConn", 8) == 0) {

      // Note that there should really be only 1 fanin for
      // a port connection, but we will go through the motions
      // because we can't guarantee what will be in the drivers
      // file.
      UtArray<const CarbonDBNode*> nodes;

      // For the drivers tree, explore through the hieararchy by
      // traversing the port connection's fanin
      if (tree == mDriverTree) {
        for (DBNodeLoop d = driver->loopFanin(); !d.atEnd(); ++d) {
          const CarbonDBNode* node = *d;
          if (node != NULL) {
            nodes.push_back(node);
          }
        }
        std::sort(nodes.begin(), nodes.end(), CarbonHBrowserNodeSorter(mDB));
      }
      else {
        nodes.push_back(driver->getDefNet());
      }

      for (UInt32 i = 0, n = nodes.size(); i < n; ++i) {
        const CarbonDBNode* hnode = nodes[i];

        // Note -- we are putting a non-driver into the drivers tree,
        // so no map entry will be created
        QTreeWidgetItem* hitem = new QTreeWidgetItem(driverItem);
        hitem->setText(0, carbonDBNodeGetFullName(mDB, hnode));
        mItemNetMap[hitem] = hnode;

        DriverLoop d = (tree == mDriverTree)
          ? mDrivers->loopDrivers(hnode)
          : mDrivers->loopReaders(hnode);
        for (; !d.atEnd(); ++d) {
          Driver* hdriver = *d;
          showDriver(hnode, hdriver, tree, hitem);
        }
      }
    }
  }
} // void CarbonCBrowser::showDriver

void CarbonCBrowser::showDrivers(const CarbonDBNode* node,
                                 DriverLoop drivers,
                                 CDragDropTreeWidget* tree)
{
  
  for (; !drivers.atEnd(); ++drivers) {
    Driver* driver = *drivers;
    showDriver(node, driver, tree, NULL);
  }
} // void CarbonCBrowser::showDrivers

typedef QList<QTreeWidgetItem*> QTreeItemList;

void CarbonCBrowser::changeDriverSel() {
  changeDriverTreeSel(mDriverTree);
}

void CarbonCBrowser::changeLoadSel() {
  changeDriverTreeSel(mLoadTree);
}

void CarbonCBrowser::changeDriverTreeSel(CDragDropTreeWidget* tree) {
  QTreeItemList sel = tree->selectedItems();

  UtString dragPath;

  // Find the currently selected debug group/name and populate the editing
  // widgets
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;
    ItemDriverMap::iterator q = mItemDriverMap.find(item);

    // The selected item may not be in the driver map because
    // we put hierarchically connected nets right there in the
    // map, and they don't necessarily have a single driver associated
    // with them -- they may have multiple drivers, which will be
    // underneath.
    if (q != mItemDriverMap.end()) {
      Driver* driver = q->second;
      mFaninTree->clear();
      mItemFaninMap.clear();
      UtArray<const CarbonDBNode*> fanin;
      for (DBNodeLoop d = driver->loopFanin(); !d.atEnd(); ++d) {
        const CarbonDBNode* node = *d;
        if (node != NULL) {
          fanin.push_back(node);
        }
      }
      std::sort(fanin.begin(), fanin.end());
      for (UInt32 i = 0, n = fanin.size(); i < n; ++i) {
        QTreeWidgetItem* faninItem = new QTreeWidgetItem(mFaninTree);
        const CarbonDBNode* node = fanin[i];
        const char* fullName = carbonDBNodeGetFullName(mDB, node);
        faninItem->setText(0, fullName);
        mItemFaninMap[faninItem] = node;
      }
    } // if
    else {
      // It must be a net.  We might want to drag & drop that net
      // somewhere else
      const CarbonDBNode* net = mItemNetMap[item];
      INFO_ASSERT(net, "Could not find net or driver for item");
      dragPath << carbonDBNodeGetFullName(mDB, net) << "\n";
    }
  } // for
  tree->putDragData(dragPath.c_str());
} // void CarbonCBrowser::changeDriverTreeSel

void CarbonCBrowser::textEditDestroyed(QObject* obj) {
  // TextEdit* textEdit = qobject_cast<TextEdit *>(obj);  yields NULL
  TextEdit* textEdit = (TextEdit*)(obj); // works fine
  INFO_ASSERT(textEdit, "only care about text editors -- errant signal?");
  const char* filename = mFileEditorReverseMap[textEdit];
  INFO_ASSERT(filename, "could not find text editor file on destruction");
  mFileEditorMap.erase(filename);
  mFileEditorReverseMap.erase(textEdit);
}

void CarbonCBrowser::changeFaninTreeSel() {
  QTreeItemList sel = mFaninTree->selectedItems();
  const CarbonDBNode* net = NULL;
  UtString buf;
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;
    net = mItemFaninMap[item];
    INFO_ASSERT(net, "could not find fanin net");
    buf << carbonDBNodeGetFullName(mDB, net) << "\n";
  }
  mFaninTree->putDragData(buf.c_str());
}

// we should keep the master text editor map in the main window
// so that text windows can be shared

void CarbonCBrowser::popupSource(QTreeWidgetItem* item, int /*column */)
{
  ItemDriverMap::iterator p = mItemDriverMap.find(item);

  // The selected item may not be in the driver map because
  // we put hierarchically connected nets right there in the
  // map, and they don't necessarily have a single driver associated
  // with them -- they may have multiple drivers, which will be
  // underneath.
  if (p != mItemDriverMap.end()) {
    Driver* driver = p->second;

    const SourceLocator& loc = driver->getLoc(); 
    const char* filename = loc.getFile();
    TextEdit* textEdit = mFileEditorMap[filename];
    if (textEdit == NULL) {
      textEdit = new TextEdit(filename, mCQt);
      mFileEditorMap[filename] = textEdit;
      mFileEditorReverseMap[textEdit] = filename;
      CQT_CONNECT(textEdit, destroyed(QObject*), this,
                  textEditDestroyed(QObject*));
    }
    textEdit->gotoLine(loc.getLine());
  }
} // void CarbonCBrowser::popupSource

void CarbonCBrowser::dropNet(const char* path, QTreeWidgetItem*) {
  UtString buf(path);
  StringUtil::strip(&buf);
  path = buf.c_str();

  if (mDB == NULL) {
    mCQt->warning(this, tr("Design Database not loaded"));
    return;
  }

  const CarbonDBNode* node = carbonDBFindNode(mDB, path);
  if (node == NULL) {
    mCQt->warning(this, tr("No such net in the design: %1")
                  .arg(path));
  }
  else if (! carbonDBIsLeaf(mDB, node)) {
    mCQt->warning(this, tr("Node %1 is a module, debug registers must be nets")
                  .arg(path));
  }
  else {
    clear();
    setFocus(node);
  }
}
