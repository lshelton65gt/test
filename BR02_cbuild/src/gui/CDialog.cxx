/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CDialog.h"

CDialog::CDialog(QWidget *parent)
  : QDialog(parent)
{
}

void CDialog::putRecordData(const UtString &data)
{
  // Store this dialog's new data, but don't update
  // the model yet.  The event to open the dialog
  // may not have occurred yet, and so the derived
  // class's state may not be valid.
  mStringValue = data;
}

void CDialog::updateData()
{
  // Actually update the derived class's data model.
  setData(mStringValue);
}

void CDialog::setData(const UtString &data)
{
  // *** Always call base class at the end of
  // *** your reimplemented function!

  mStringValue = data;
  emit dataEntered(data);
}
