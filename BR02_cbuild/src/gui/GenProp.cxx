/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

// Second implementation of general properties page
//  - remove useless first selection of Realview/Maxsim.  It makes
//    no difference to us.  Some day we may want to support CASI/CADI/CAPI
//    but there is no compelling reason for us to do this now, because
//    MXSI/MXDI/MXPI will work forever view adaptation headers.  The only
//    reason for us to migrate to the C* interface is to interface to
//    other front ends like Coware.

#include "GenProp.h"
#include <QTreeWidgetItem>
#include <QVBoxLayout>
#include <QTimer>
#include <QLabel>
#include <cassert>
#include "gui/CFilenameEdit.h"
#include "gui/CDragDropTreeWidget.h"
#include "cfg/carbon_cfg.h"

CarbonGenProp::CarbonGenProp(CQtContext* cqt, CarbonCfgID cfg)
  : mCQt(cqt), mCfg(cfg)
{
  build();
}

void CarbonGenProp::build() {
  CQtRecordBlocker blocker(mCQt);

#if MX_CA_SWITCH
  // Currently you can play with these buttons in the GUI but they
  // do not affect the config
  mRadioMaxsim = mCQt->radioButton("radio_maxsim", "Maxsim Realview");
  mRadioRealview = mCQt->radioButton("radio_realview", "Realview ESL");
  mRadioRealview->setEnabled(false);
  mRadioMaxsim->setChecked(true);
#endif
  
  mComponentDescription = mCQt->textEdit("comp_desc");
  mCxxFlags = mCQt->lineEdit("cxx_flags");
  mLinkFlags = mCQt->lineEdit("link_flags");
  mIODBFile = mCQt->filenameEdit("iodb_file",
                                 true, // readFile
                                 "Choose a Carbon database filename",
                                 "DB File (*.symtab.db *.io.db)");
  mLibFile = mCQt->filenameEdit("lib_file",
                                true, // readFile
                                "Choose a carbon library file",
                                "Library file (*.dll *.so *.lib *.a)");
  mCompName = mCQt->lineEdit("comp_name");
  mWaveFile = mCQt->filenameEdit("wave_file",
                                 false, // readFile
                                 "Choose a waveform filename for writing",
                                 "Wave File (*.dump *.vcd *.fsdb)");
  mWaveType = mCQt->comboBox("wave_type");
  mWaveType->addItem("No waveforms");
  mWaveType->addItem("VCD");
  mWaveType->addItem("FSDB");
  mWaveFile->setEnabled(false);

#define RESET_IN_ZERO_TIME 0
#if RESET_IN_ZERO_TIME
  mResetInZeroTime = mCQt->checkBox("zerotime_reset", "Reset in Zero Time");
#endif

  CQT_CONNECT(mWaveType, currentIndexChanged(int), this, changeWaveType(int));

#if ENA_DEBUG_MESSAGES
  mEnaDebugMsg = mCQt->checkBox("ena_debug", "Enable Debug Messages");
#endif
} // void CarbonGenProp::build

QWidget* CarbonGenProp::layout() {
#if MX_CA_SWITCH
  QWidget* selTargetCode =
    CQt::makeGroup("Select Target Code",
                   CQt::vbox(mRadioMaxsim, mRadioRealview));
#endif
  QLayout* waveWidgets = CQt::hbox(mWaveType, mWaveFile);
#if ENA_DEBUG_MESSAGES
  QLayout* dbg = CQt::vbox(mEnaDebugMsg);
  *dbg << waveWidgets;
  QWidget* debug = CQt::makeGroup("Debug Control", dbg);
#endif                   

  QGridLayout* grid = new QGridLayout;
  CQt::addGridRow(grid, "IODB File", mIODBFile);
  CQt::addGridRow(grid, "Library File", mLibFile);
  CQt::addGridRow(grid, "Component Name", mCompName);
  CQt::addGridRow(grid, "Debug Control", waveWidgets);
  CQt::addGridRow(grid, "C++ Compile Flags", mCxxFlags);
  CQt::addGridRow(grid, "Link Flags", mLinkFlags);

  QWidget* compDescription =
    CQt::makeGroup("Component Description", mComponentDescription);

  QLayout* layout = CQt::vbox();
  *layout << grid << compDescription;
  return CQt::makeWidget(layout);
} // QWidget* CarbonGenProp::layout

CarbonGenProp::~CarbonGenProp() {
}
  
void CarbonGenProp::updateCfg() {
  UtString buf;
  buf << mComponentDescription->toPlainText();
  carbonCfgPutDescription(mCfg, buf.c_str());
  buf.clear();
  buf << mCompName->text();
  carbonCfgPutCompName(mCfg, buf.c_str());
  buf.clear();
  buf << mCxxFlags->text();
  carbonCfgPutCxxFlags(mCfg, buf.c_str());
  buf.clear();
  buf << mLinkFlags->text();
  carbonCfgPutLinkFlags(mCfg, buf.c_str());
  carbonCfgPutLibName(mCfg, mLibFile->getFilename());
  carbonCfgPutIODBFile(mCfg, mIODBFile->getFilename());
  carbonCfgPutWaveFilename(mCfg, mWaveFile->getFilename());
  switch (mWaveType->currentIndex()) {
  case 0:
    carbonCfgPutWaveType(mCfg, eCarbonCfgNone);
    break;
  case 1:
    carbonCfgPutWaveType(mCfg, eCarbonCfgVCD);
    break;
  case 2:
    carbonCfgPutWaveType(mCfg, eCarbonCfgFSDB);
    break;
  }
} // void CarbonGenProp::updateCfg

void CarbonGenProp::readConfig() {
  CQtRecordBlocker blocker(mCQt);

  mIODBFile->putFilename(carbonCfgGetIODBFile(mCfg));
  mComponentDescription->clear();
  mComponentDescription->append(carbonCfgGetDescription(mCfg));
  mCompName->setText(carbonCfgGetCompName(mCfg));
  mCxxFlags->setText(carbonCfgGetCxxFlags(mCfg));
  mLinkFlags->setText(carbonCfgGetLinkFlags(mCfg));
  mLibFile->putFilename(carbonCfgGetLibName(mCfg));
  mWaveFile->putFilename(carbonCfgGetWaveFilename(mCfg));
  switch (carbonCfgGetWaveType(mCfg)) {
  case eCarbonCfgNone:
    mWaveFile->setEnabled(false);
    mWaveType->setCurrentIndex(0);
    break;
  case eCarbonCfgVCD:
    mWaveFile->setEnabled(true);
    mWaveType->setCurrentIndex(1);
    break;
  case eCarbonCfgFSDB:
    mWaveFile->setEnabled(true);
    mWaveType->setCurrentIndex(2);
    break;
  }
} // void CarbonGenProp::readConfig

void CarbonGenProp::changeWaveType(int waveType) {
  CQtRecordBlocker blocker(mCQt);
  mWaveFile->setEnabled(waveType != 0);
}
