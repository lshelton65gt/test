// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CARBON_MEM_EDIT_H_
#define _CARBON_MEM_EDIT_H_

#include "gui/CQt.h"
#include <QHBoxLayout>
#include "util/UtBiMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "util/SourceLocator.h"

class QTreeWidget;
class QTreeWidgetItem;
class TextEdit;
class QLabel;
class DynBitVector;
class ApplicationContext;

class CarbonMemEdit : public QWidget {
  Q_OBJECT
public:
  CARBONMEM_OVERRIDES

  //! ctor
  CarbonMemEdit(CQtContext*, CarbonCfgID, AtomicCache* atomicCache,
                QAction* deleteAction, ApplicationContext* ctx=NULL);

  //! dtor
  ~CarbonMemEdit();

  //! clear the tree
  void clear();
  
  //! after reading the database, populate the debug tree
  void populate(CarbonDB* carbonDB);
  void read();
  QWidget* buildLayout();

  void deleteCurrent();
  bool isDeleteActive() const;

  QTreeWidgetItem* addMem(CarbonCfgMemory* mem);
  QTreeWidgetItem* addGroup(const UtString& groupName);

public slots:
  void changeName(const QString&);
  void changeComment(const QString&);
  void changeMemTreeSel();
  void dropMemory(const char*);
  void changeMaxAddr(int);
  void changeBaseAddr(const DynBitVector&);
  void changeInitFile(const char*);
  void addAllMemories();
  void changeReadmemType(int);
  void changeEslPort(const QString&);
  void noLoadToggle(bool);
  void loadByProgramToggle(bool);
  void loadByDatFileToggle(bool);
  void changeDisassemblyName(const QString&);
  void changeDisassemblerName(const QString&);

private:
  void applyChanges(const char* name, const char* comment, int radix);
  void setTreeText(CarbonCfgMemory* mem);
  void setMemWidgets(UInt32 numSelected, CarbonCfgMemory* mem);
  UInt32 findMemories(const CarbonDBNode* node, bool dry_run);
  UInt32 addAllMemoriesHelper(bool dry_run);
  QTreeWidgetItem* addMemoryNode(const CarbonDBNode* node);
  void enableWidgets();

  CDragDropTreeWidget* mMemTree;

  QLineEdit* mComment;
  QLineEdit* mName;

  CHexSpinBox* mMemMaxAddr;
  QPushButton* mAddAllMemories;
  QLineEdit* mDisassemblyName;
  QLineEdit* mDisassemblerName;

  typedef UtBiMap<QTreeWidgetItem*,CarbonCfgMemory*> ItemMemoryMap;
  ItemMemoryMap mItemMemoryMap;

  typedef UtHashMap<UtString,CarbonCfgMemory*> MemoryNameMap;
  MemoryNameMap mMemoryNameMap;

  CarbonCfgID mCfg;
  bool mDisable;
  bool mDeleteActive;
  CarbonDB* mDB;
  CarbonCfgMemory* mSelectedMem;

  SourceLocatorFactory mLocatorFactory;
  AtomicCache* mAtomicCache;

  QAction* mDeleteAction;
  CQtContext* mCQt;
  SInt32 mTotalMemories;        // initialized to -1

  // helper class for memory initialization
  struct MemInitHelper;
  MemInitHelper* mMemInitHelper;

  ApplicationContext* mCtx;

}; // class CarbonMemEdit : public QWidget

#endif
