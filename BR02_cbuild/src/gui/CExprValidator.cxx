/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
 
#include "gui/CExprValidator.h"
#include "util/CExprParse.h"
#include "util/UtHashSet.h"
#include <QPalette>
#include <QLabel>

CExprValidator::CExprValidator(CQtContext* cqt, const char* name,
                               bool allow_empty):
  mCQt(cqt),
  mAllowEmpty(allow_empty)
{
  mLineEdit = cqt->lineEdit(name);
  UtString buf;
  buf << name << "_errors";
  mErrorWidget = cqt->lineEdit(buf.c_str());
  mErrorWidget->setReadOnly(true);

  mSplitter = CQt::hSplit(mLineEdit, mErrorWidget);

  CQT_CONNECT(mLineEdit, textChanged(const QString&),
              this, updateText(const QString&));

  mColor = mCQt->getForegroundColor();
  mValidIdentifiers = new UtStringSet;
  mValidationDirty = true;
}

CExprValidator::~CExprValidator() {
  delete mValidIdentifiers;
}

void CExprValidator::updateText(const QString& str) {
  UtString buf;
  buf << str;
  if (buf != mValue) {
    mValue = buf;
    mValidationDirty = true;
    (void) validate();
    emit textChanged(QString(str));
  }
}

void CExprValidator::setText(const char* str) {
  if (strcmp(str, mValue.c_str()) != 0) {
    mValidationDirty = true;
    mValue = str;
    (void) validate();
    mLineEdit->setText(str);    // will not recurse cause we cache mValue
    // do not emit textChanged from a programmatic update
  } // if
} // void CExprValidator::setText

void CExprValidator::setEnabled(bool val) {
  mLineEdit->setEnabled(val);
  mErrorWidget->setEnabled(val);
  if (!val) {
    mErrorWidget->setText("");
  }
}

//! clear the set of valid identifiers
void CExprValidator::clearIdents() {
  mValidIdentifiers->clear();
  mValidationDirty = true;
}

//! add a valid identifier
void CExprValidator::addIdent(const char* ident) {
  mValidIdentifiers->insert(ident);
  mValidationDirty = true;
}

bool CExprValidator::validate() {
  if (mValidationDirty) {
    mValidationDirty = false;
    UtStringSet idents;
    mErrorText.clear();
    if ((mAllowEmpty && mValue.empty()) ||
        carbonCExprValidate(mValue.c_str(), &idents)) {
      for (UtStringSet::SortedLoop p = idents.loopSorted(); !p.atEnd(); ++p) {
        const UtString& ident = *p;
        if (mValidIdentifiers->find(ident) == mValidIdentifiers->end()) {
          mColor = QColor(Qt::darkMagenta);
          if (mErrorText.empty()) {
            mErrorText << "Invalid signal(s): ";
          }
          mErrorText << " " << ident;
        }
      }
      if (mErrorText.empty()) {
        mColor = mCQt->getForegroundColor();
      }
    }
    else {
      mColor = QColor(Qt::darkRed);
      mErrorText << "Syntax error";
    }
    QPalette palette = mLineEdit->palette();
    palette.setColor(QPalette::Text, mColor);
    mLineEdit->setPalette(palette);

    mErrorWidget->setPalette(palette);
    mErrorWidget->setText(mErrorText.c_str());
  } // if

  return mErrorText.empty();
} // bool CExprValidator::validate
