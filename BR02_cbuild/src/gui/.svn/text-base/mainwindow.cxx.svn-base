/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

/*

3. show separate clock panel
5. add tienets
6. add disconnects
7. add xtors
8. show separate reset panel
9. check it still works on Windowsn
10. clock panel graphics
*/

#include "util/CarbonPlatform.h"
#include "cfg/carbon_cfg.h"
#include "shell/carbon_misc.h"
#include <QtGui>
#include <QGridLayout>
#include "mainwindow.h"
#include "build.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/RandomValGen.h"
#include "shell/carbon_capi.h"
#include "hbrowser.h"
#include "RegTabWidget.h"
#include "MemEdit.h"
#include "ProfileEdit.h"
#include "gui/CDragDropTreeWidget.h"
#include "GenProp.h"
#include "PortEdit.h"
#include "ProfileEdit.h"
#include "textedit.h"
#include "cfg/CarbonCfg.h"
#include "util/CarbonVersion.h"
#include "iodb/CGraph.h"

#define APP_NAME "Carbon Component Wizard"

MainWindow::MainWindow(CQtContext* cqt)
  : mCQt(cqt),
    mAssertOverride(APP_NAME, this, cqt)
{
  mMsgCallback = carbonAddMsgCB(NULL, sMsgCallback, this);

  mNumBrowsers = 0;
  mAtomicCache = new AtomicCache;
  mDrivers = NULL;
  mDB = NULL;
  mCGraph = NULL;
  mCurrentTabIndex = 0;
  createActions();


  mErrorTextEditor = NULL;

  // Build the leaf widgets that hold the content for the configuration
  mCfg = carbonCfgCreate();
  
  if (carbonCfgReadXtorLib(mCfg, eCarbonXtorsMaxsim) == eCarbonCfgFailure) {
    mCQt->warning(this, carbonCfgGetErrmsg(mCfg));
  }
  
  // read any user transactor defs
  if (mCQt->getXtorDefFile() != NULL) {
    if (carbonCfgReadXtorDefinitions(mCfg, mCQt->getXtorDefFile()) == eCarbonCfgFailure) {
      mCQt->warning(this, carbonCfgGetErrmsg(mCfg));
    }
  }

  mGenProp = new CarbonGenProp(cqt, mCfg);
  mPortEdit = new CarbonPortEdit(cqt, mCfg, mAtomicCache, mDeleteAct);

  // Registers window
  mRegTabWidget = new RegTabWidget(this, cqt, mCfg);
  mBuild = new CarbonBuild(mCfg, this, this);
  mMemEdit = new CarbonMemEdit(cqt, mCfg, mAtomicCache, mDeleteAct);
  mProfileEdit = new CarbonProfileEdit(cqt, mCfg, mAtomicCache, mDeleteAct,
                                       statusBar());

  buildLayout();
  createMenus();
  createToolBars();
  createStatusBar();
  
  readSettings();
  
  setWindowIcon(QIcon(":/images/carbon-logo.png"));

  setCurrentFile("");
} // MainWindow::MainWindow

bool MainWindow::loadFile(const char* filename, ParseResult parseResult) {
  bool ret = false;
  if ((parseResult == eFullDB) || (parseResult == eIODB)) {
    ret = loadDatabase(filename, true);
  }
  if (parseResult == eCcfg) {
    ret = loadConfig(filename);
  }

  
  return ret;
}

MainWindow::~MainWindow() {

  if (mDB != NULL) {
    carbonDBFree(mDB);
    mDB = NULL;
  }
  if (mCGraph != NULL) {
    delete mCGraph;
  }

  carbonRemoveMsgCB(NULL, &mMsgCallback);
}

eCarbonMsgCBStatus MainWindow::sMsgCallback(CarbonClientData clientData,
                                             CarbonMsgSeverity severity,
                                             int number, const char* text,
                                             unsigned int)
{
  MainWindow* mw = (MainWindow*) clientData;
  const char* severityStr = "";
  switch (severity) {
  case eCarbonMsgStatus:   severityStr = "Status"; break;
  case eCarbonMsgNote:     severityStr = "Note"; break;
  case eCarbonMsgWarning:  severityStr = "Warning"; break;
  case eCarbonMsgError:    severityStr = "Error"; break;
  case eCarbonMsgFatal:    severityStr = "Fatal"; break;
  case eCarbonMsgSuppress: severityStr = "Suppress"; break;
  case eCarbonMsgAlert:    severityStr = "Alert"; break;
  }

  UtString buf;
  buf << severityStr << " " << number << ": " << text;

  mw->mCQt->warning(mw, buf.c_str());
  return eCarbonMsgContinue;
}


void MainWindow::buildLayout() {
  CTabWidget* tab = mCQt->tabWidget("main_tab");
  tab->setTabShape(QTabWidget::Triangular);
#if pfWINDOWS
  QFont f("Helvetica", 10, QFont::Normal);
#else
  QFont f("Helvetica", 14, QFont::Normal);
#endif
  tab->getTabBar()->setFont(f);

  tab->addTab(mGenProp->layout(), "General Properties");
  tab->addTab(mPortEdit->layout(), "Port Map");
  tab->addTab(mRegTabWidget, "Registers");
  tab->addTab(mMemEdit->buildLayout(), "Memories");
  tab->addTab(mProfileEdit->buildLayout(), "Profile");
  tab->addTab(mBuild->layout(), "Build");

  CQT_CONNECT(tab, currentChanged(int), this, tabSwitch(int));

  QLayout* layout = CQt::vbox();
  layout->addItem(new QSpacerItem(1, 10));
  *layout << tab;
  setCentralWidget(CQt::makeWidget(layout));
} // void MainWindow::buildLayout


void MainWindow::showWindow()
{
  show();
  if (mErrorTextEditor != NULL) {
    mErrorTextEditor->raise();
  }
}
void MainWindow::closeEvent(QCloseEvent *event)
{
  mCQt->recordExit();
  if (maybeSave()) {
    if (mErrorTextEditor != NULL && (mCQt->isPlaybackActive() || mCQt->isRecordActive())) {
      mErrorTextEditor->save();
      bool closed = mErrorTextEditor->close();
      INFO_ASSERT(closed == true, "Unable to save and close Error Text");
    }
    mCQt->dumpStateHierarchy();
    writeSettings();
    event->accept();
  } else {
    event->ignore();
  }
}

void MainWindow::newFile()
{
  if (maybeSave()) {
    //mTextEdit->clear();
    setCurrentFile("");
  }
}

void MainWindow::open()
{
  if (maybeSave()) {
    UtString filename, basename;

    if (mCQt->isPlaybackActive()) {
      mCQt->getPlaybackFile(&filename);
    }
    else {
      QApplication::setOverrideCursor(Qt::WaitCursor);
      QString qfile =
        QFileDialog::getOpenFileName(this,
                                     "Choose a Carbon Configuration or Database",
                                     "", 
                                     "Carbon Configuration or DB (*.ccfg *.db)");
      QApplication::restoreOverrideCursor();
      filename << qfile;
      if (mCQt->isRecordActive()) {
        mCQt->recordFilename(filename.c_str());
      }
    }
    ParseResult parseResult = parseFileName(filename.c_str(), &basename);

    mRegTabWidget->clear();
    mMemEdit->clear();
    mProfileEdit->clear();
    mPortEdit->clear();

    carbonCfgClear(mCfg);
    setCurrentFile("");
    
    (void) loadFile(filename.c_str(), parseResult);
  }
}

bool MainWindow::save()
{
  if (mCurFile.empty()) {
    return saveAs();
  } else {
    return saveFile(mCurFile.c_str());
  }
}

bool MainWindow::saveAs() {
  UtString filename, basename;

  bool ret = mCQt->popupFileDialog(this, false, // readFile
                                   "Create a Carbon Configuration",
                                   "Carbon Configuration (*.ccfg)",
                                   &filename)
    && saveFile(filename.c_str());
  return ret;
}

void MainWindow::about()
{
  QMessageBox::about(this, tr("About Carbon Component Wizard"),
                     tr("The <b>Carbon Component Wizard</b> creates "
                        "an ARM/Maxsim component from a precompiled "
                        "Carbon model.  The user controls transactor "
                        "port mapping and debug visibility, and can "
                        "set up abstract registers and Maxsim profiling streams. "
                        "<p>Version " CARBON_RELEASE_ID));
}

void MainWindow::help() {
  mCQt->launchHelp("help-menu");
}

void MainWindow::documentWasModified()
{
  setWindowModified(true);
}


void MainWindow::createActions()
{
/*
  mNewAct = new QAction(QIcon(":/images/new.png"), tr("&New"), this);
  mNewAct->setShortcut(tr("Ctrl+N"));
  mNewAct->setStatusTip(tr("Create a new file"));
  //connect(mNewAct, SIGNAL(triggered()), this, SLOT(newFile()));
  */
  
  mOpenAct = new QAction(QIcon(":/images/open.png"), tr("&Open..."), this);
  mOpenAct->setShortcut(tr("Ctrl+O"));
  mOpenAct->setStatusTip(tr("Open an existing file"));
  CQT_CONNECT(mOpenAct, triggered(), this, open());
  mCQt->registerAction(mOpenAct, "open");
  
  mSaveAct = new QAction(QIcon(":/images/save.png"), tr("&Save"), this);
  mSaveAct->setShortcut(tr("Ctrl+S"));
  mSaveAct->setStatusTip(tr("Save the document to disk"));
  CQT_CONNECT(mSaveAct, triggered(), this, save());
  mCQt->registerAction(mSaveAct, "save");
  
  mSaveAsAct = new QAction(tr("Save &As..."), this);
  mSaveAsAct->setStatusTip(tr("Save the document under a new name"));
  CQT_CONNECT(mSaveAsAct, triggered(), this, saveAs());
  mCQt->registerAction(mSaveAsAct, "saveAs");
  
  mExitAct = new QAction(tr("E&xit"), this);
  mExitAct->setShortcut(tr("Ctrl+Q"));
  mExitAct->setStatusTip(tr("Exit the application"));
  CQT_CONNECT(mExitAct, triggered(), this, close());
  mCQt->registerAction(mExitAct, "exit");
  
  
  mCutAct = new QAction(QIcon(":/images/cut.png"), tr("Cu&t"), this);
  mCutAct->setShortcut(tr("Ctrl+X"));
  mCutAct->setStatusTip(tr("Cut the current selection's contents to the "
                          "clipboard"));
  //connect(mCutAct, SIGNAL(triggered()), mTextEdit, SLOT(cut()));
  //registerAction
  
  mCopyAct = new QAction(QIcon(":/images/copy.png"), tr("&Copy"), this);
  mCopyAct->setShortcut(tr("Ctrl+C"));
  mCopyAct->setStatusTip(tr("Copy the current selection's contents to the "
                           "clipboard"));
  //connect(mCopyAct, SIGNAL(triggered()), mTextEdit, SLOT(copy()));
  //registerAction  
  mPasteAct = new QAction(QIcon(":/images/paste.png"), tr("&Paste"), this);
  mPasteAct->setShortcut(tr("Ctrl+V"));
  mPasteAct->setStatusTip(tr("Paste the clipboard's contents into the current "
                            "selection"));
  //connect(mPasteAct, SIGNAL(triggered()), mTextEdit, SLOT(paste()));
  //registerAction  

  mHelpAct = new QAction(tr("&Help"), this);
  mHelpAct->setStatusTip(tr("Show detailed help"));
  CQT_CONNECT(mHelpAct, triggered(), this, help());
  mCQt->registerAction(mHelpAct, "help");
  
  mAboutAct = new QAction(tr("&About"), this);
  mAboutAct->setStatusTip(tr("Show the application's About box"));
  CQT_CONNECT(mAboutAct, triggered(), this, about());
  mCQt->registerAction(mAboutAct, "about");
  
  mCutAct->setEnabled(false);
  mCopyAct->setEnabled(false);

  // The delete button takes on different meanings depending on which
  // tab is currently active.  Every time we switch tabs we must check
  // whether the current tab has something deletable selected.
  mDeleteAct = new QAction(QIcon(":/images/cut.png"), tr("&Delete"), this);
  //mCutAct->setShortcut(tr("Ctrl+X"));
  mDeleteAct->setStatusTip(tr("Delete the currently selected item"));
  CQT_CONNECT(mDeleteAct, triggered(), this, deleteCurrent());
  mDeleteAct->setEnabled(false);
  mCQt->registerAction(mDeleteAct, "delete");

  mBrowseAct = new QAction(QIcon(":/images/tree.png"), tr("&HBrowse"), this);
  connect(mBrowseAct, SIGNAL(triggered()), this, SLOT(createBrowser()));
  mBrowseAct->setEnabled(false);
  mCQt->registerAction(mBrowseAct, "browse");

  /*
    connect(mTextEdit, SIGNAL(copyAvailable(bool)),
          mCutAct, SLOT(setEnabled(bool)));
  connect(mTextEdit, SIGNAL(copyAvailable(bool)),
          mCopyAct, SLOT(setEnabled(bool)));
  */
}

void MainWindow::createMenus()
{
  mFileMenu = menuBar()->addMenu(tr("&File"));
  //mFileMenu->addAction(mNewAct);
  mFileMenu->addAction(mOpenAct);
  mFileMenu->addAction(mSaveAct);
  mFileMenu->addAction(mSaveAsAct);
  mFileMenu->addSeparator();
  mFileMenu->addAction(mExitAct);
  
  mEditMenu = menuBar()->addMenu(tr("&Edit"));
  mEditMenu->addAction(mCutAct);
  mEditMenu->addAction(mCopyAct);
  mEditMenu->addAction(mPasteAct);
  mEditMenu->addAction(mDeleteAct);
  mEditMenu->addAction(mBrowseAct);
  
#if pfWINDOWS                   // seems broken on Linux.
  menuBar()->addSeparator();
#endif
  
  mHelpMenu = menuBar()->addMenu(tr("&Help"));
  mHelpMenu->addAction(mHelpAct);
  mHelpMenu->addAction(mAboutAct);
}

void MainWindow::createToolBars()
{
  mFileToolBar = addToolBar(tr("File"));
  //mFileToolBar->addAction(mNewAct);
  mFileToolBar->addAction(mOpenAct);
  mFileToolBar->addAction(mSaveAct);
  mFileToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
  
  mEditToolBar = addToolBar(tr("Edit"));
  mEditToolBar->addAction(mCutAct);
  mEditToolBar->addAction(mCopyAct);
  mEditToolBar->addAction(mPasteAct);
  mEditToolBar->addAction(mDeleteAct);
  mEditToolBar->addAction(mBrowseAct);
  mEditToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
}

void MainWindow::createStatusBar()
{
  statusBar()->showMessage(tr("Ready"));
}

void MainWindow::readSettings()
{
  QSettings settings("Carbon", "Component Wizard");
  QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
  QSize size = settings.value("size", QSize(400, 400)).toSize();
  resize(size);
  move(pos);
}

void MainWindow::writeSettings()
{
  QSettings settings("Carbon", "Component Wizard");
  settings.setValue("pos", pos());
  settings.setValue("size", size());
}

bool MainWindow::maybeSave()
{
  bool status = mBuild->checkExitOK();
  if (status && isWindowModified()) {
    int ret = QMessageBox::Yes;
    if (!mCQt->isPlaybackActive() && !mCQt->isRecordActive()) {
      ret = QMessageBox::warning(this, tr(APP_NAME),
                                 tr("The configuration has been modified.\n"
                                    "Do you want to save your changes?"),
                                 QMessageBox::Yes | QMessageBox::Default,
                                 QMessageBox::No,
                                 QMessageBox::Cancel | QMessageBox::Escape);
    }
    if (ret == QMessageBox::Yes) {
      status = save();
    }
    else if (ret == QMessageBox::Cancel) {
      status = false;
    }
  }
  return status;
} // bool MainWindow::maybeSave

MainWindow::ParseResult MainWindow::parseFileName(const char* filename,
                                                  UtString* baseName)
{
  *baseName = filename;
  UInt32 sz = strlen(filename);

  ParseResult parseResult = eNoMatch;
  baseName->clear();

  if ((sz > 6) && (strcmp(filename + sz - 6, ".io.db") == 0)) {
    parseResult = eIODB;
    baseName->append(filename, sz - 6);
  }
  else if ((sz > 10) && (strcmp(filename + sz - 10, ".symtab.db") == 0)) {
    parseResult = eFullDB;
    baseName->append(filename, sz - 10);
  }
  else if ((sz > 5) && (strcmp(filename + sz - 5, ".ccfg") == 0)) {
    parseResult = eCcfg;
    baseName->append(filename, sz - 5);
  }
  return parseResult;
} // bool MainWindow::parseFileName

bool MainWindow::loadDatabase(const char* iodbName, bool initPorts)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);
  SInt32 key;
  RANDOM_SEEDGEN_1(key, "VSP", 3);

  // clear old DB
  mDB = 0;
  CarbonDB *oldDB = mCfg->getDB();
  if (oldDB) {
    mCfg->putDB(0);
    carbonDBFree(oldDB);
  }

  // load new DB
  CarbonDB* newDB = carbonDBSocVspInit(key, iodbName, false);

  if (newDB == NULL) {
    QApplication::restoreOverrideCursor();
    return false;
  }
  mDB = newDB;
  mCfg->putDB(mDB);  // Ownership transferred to CarbonCfg

  mBrowseAct->setEnabled(true);
  
  // Only initialize data from the database if we are starting
  // from one.  If we came from a .ccfg file then the ports are already
  // initialized, and we should probably validate that they are
  // consistent

  UtString baseFile, errmsg;
  parseFileName(iodbName, &baseFile);

  if (mCGraph != NULL) {
    delete mCGraph;
  }

#ifdef NEW_CBROWSE
  mCGraph = new CGraph(&mLocatorFactory, mAtomicCache);
  UtString cgFile(baseFile);
  cgFile << ".cgraph";
  if (mCGraph->read(cgFile.c_str(), &errmsg, mDB)) {
    mCGraph->buildNetNodeMap();
  }
  else {
    mCQt->warning(this, errmsg.c_str());
  }
#endif

  errmsg.clear();
  if (initPorts) {
    UtString libFile(baseFile);
#ifdef Q_OS_WIN
    libFile += ".lib";
#else
    libFile += ".a";
#endif
    carbonCfgPutLibName(mCfg, libFile.c_str());
    setWindowModified(true);
  }

  mPortEdit->populate(mDB, initPorts);

  if (initPorts) {
    mRegTabWidget->clear();
    mMemEdit->clear();
    mProfileEdit->clear();
  }
  mRegTabWidget->populate(mDB);
  mMemEdit->populate(mDB);
  mProfileEdit->populate(mDB);
  mBuild->populate(mDB);

  carbonCfgPutIODBFile(mCfg, iodbName);
  if (initPorts) {
    carbonCfgPutCompName(mCfg, carbonDBGetTopLevelModuleName(mDB));
    carbonCfgPutTopModuleName(mCfg, carbonDBGetTopLevelModuleName(mDB));
    mGenProp->readConfig();
  }

  statusBar()->showMessage(tr("Database loaded"), 2000);
  QApplication::restoreOverrideCursor();
  return true;
} // bool MainWindow::loadDatabase

bool MainWindow::loadConfig(const QString &fileName) {
  QApplication::setOverrideCursor(Qt::WaitCursor);
  carbonCfgClear(mCfg);
  UtString fname;
  fname << fileName;
  CarbonCfgStatus cfgStatus = carbonCfgRead(mCfg, fname.c_str());
  bool success = cfgStatus != eCarbonCfgFailure;
  QApplication::restoreOverrideCursor();
  
  if (success) {
    // Read in the IODB file to populate the debug tree
    const char* iodbFile = carbonCfgGetIODBFile(mCfg);
    success = loadDatabase(iodbFile, false);
    if (success) {
      setCurrentFile(fileName);

      mGenProp->readConfig();
      mPortEdit->updateWidgets();
      mMemEdit->read();
      mProfileEdit->read();
    }
  }
  else {
    mCQt->warning(this, tr("Cannot read file %1:\n%2.")
                  .arg(fileName)
                  .arg(carbonCfgGetErrmsg(mCfg)));
  }
  
  // Database is out of synch, warn the user
  if (cfgStatus == eCarbonCfgInconsistent) {
    UtString curDir;
    UtString filePath;
    OSGetCurrentDir(&curDir);
    OSConstructFilePath(&filePath, curDir.c_str(), "DatabaseErrors.txt");
    const char* lastError = carbonCfgGetErrmsg(mCfg);
    if (mErrorTextEditor == NULL) {
      TextEdit* te = new TextEdit(mCQt);
      te->setCurrentFile(filePath.c_str());
      te->getWidget()->setDocumentTitle(filePath.c_str());
      te->show();
      CQT_CONNECT(te, destroyed(QObject*), this, textEditDestroyed(QObject*));
      mErrorTextEditor = te;
    } 

    INFO_ASSERT(mErrorTextEditor != NULL, "Unable to create text editor for some reason");

    if (mErrorTextEditor != NULL) {
      mErrorTextEditor->getWidget()->append(lastError);
      mErrorTextEditor->raise();
    }
  }

  return success;
} // bool MainWindow::loadConfig

void MainWindow::textEditDestroyed(QObject*) {
  mErrorTextEditor = NULL;
}

bool MainWindow::saveFile(const char* fileName)
{
  bool ret = false;
  QApplication::setOverrideCursor(Qt::WaitCursor);
  mGenProp->updateCfg();

  // Add a .ccfg extension if one does't exist
  UtString fname(fileName);
  if ((fname.size() < 5) ||
      (strcmp(fname.c_str() + fname.size() - 5, ".ccfg") != 0))
  {
    fname << ".ccfg";
  }

  if (carbonCfgWrite(mCfg, fname.c_str()) == eCarbonCfgFailure) {
    mCQt->warning(this, tr("Cannot write file %1:\n%2.")
                  .arg(fname.c_str())
                  .arg(carbonCfgGetErrmsg(mCfg)));
  }
  else {
    setCurrentFile(fname.c_str());
    statusBar()->showMessage(tr("File saved"), 2000);
    ret = true;
  }
  
  QApplication::restoreOverrideCursor();
  return ret;
} // bool MainWindow::saveFile

void MainWindow::setCurrentFile(const QString &fileName)
{
  mCurFile.clear();
  mCurFile << fileName;
  setWindowModified(false);
  
  QString shownName;
  if (mCurFile.empty())
    shownName = "untitled.ccfg";
  else
    shownName = strippedName(QString(mCurFile.c_str()));
  
  setWindowTitle(tr("%1[*] - %2").arg(shownName).arg(tr(APP_NAME)));
  
  mMakefile = "Makefile.";
  QFileInfo fileInfo(mCurFile.c_str());
  QString baseName = fileInfo.completeBaseName();
  mMakefile << baseName << ".mx";
#ifdef Q_OS_WIN
  mMakefile << ".windows";
#endif
}

const char* MainWindow::getCcfgFilename() {
  return mCurFile.c_str();
}

const char* MainWindow::getMakefileName() {
  return mMakefile.c_str();
}

QString MainWindow::strippedName(const QString &fullFileName)
{
  return QFileInfo(fullFileName).fileName();
}

void MainWindow::deleteCurrent() {
  setWindowModified(true);
  switch (mCurrentTabIndex) {
  case 1: // port setup
    mPortEdit->deleteCurrent();
    break;
  case 2: // registers
    mRegTabWidget->deleteRegister();
    break;
  case 3: // memories
    mMemEdit->deleteCurrent();
    break;
  case 4: // profile
    mProfileEdit->deleteCurrent();
    break;
  default:
    break;
  }
}

void MainWindow::tabSwitch(int index) {
  mCurrentTabIndex = index;
  mDeleteAct->setEnabled(false);

  // probably there is a better way to do this in Qt
  switch (index) {
  case 1: // port setup
    if (mPortEdit->isDeleteActive()) {
      mDeleteAct->setEnabled(true);
    }
    break;
  case 2: // registers
//     if (mDbgTree->isDeleteActive()) {
//       mDeleteAct->setEnabled(true);
//     }
    break;
  case 3:
    if (mMemEdit->isDeleteActive()) {
      mDeleteAct->setEnabled(true);
    }
    break;
  case 4:
    if (mProfileEdit->isDeleteActive()) {
      mDeleteAct->setEnabled(true);
    }
    break;
  default:
    break;
  }
}

void MainWindow::createBrowser() {
  ++mNumBrowsers;
  CarbonHBrowser* browser = new CarbonHBrowser(mCQt, mNumBrowsers,
                                               mCGraph);
  browser->populate(mDB);
  browser->show();

  // Keep track of all browsers so they can be closed if the 
  // database is closed, or alternatively we could reference-count
  // the database objects so that browsers could outlive configs.
}
