//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "car2cow.h"
#include "XtorPortTreeWidget.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtStringArray.h"
#include "util/UtStringUtil.h"
#include "DialogAdvancedBinding.h"

XtorPortTreeWidget::XtorPortTreeWidget(QWidget *parent)
: CDragDropTreeWidget(true, parent)
{
  ui.setupUi(this);
  mInst = NULL;
  mDialog = new DialogAdvancedBinding(this);

  CQT_CONNECT(this, dropReceived(const char*,QTreeWidgetItem*), this, dropPort(const char*,QTreeWidgetItem*));
  CQT_CONNECT(this, itemSelectionChanged(), this, itemSelectionChanged());
}

XtorPortTreeWidget::~XtorPortTreeWidget()
{

}

void XtorPortTreeWidget::putContext(ApplicationContext* ctx)
{
  mCtx = ctx;

  header()->setResizeMode(colPORTNAME, QHeaderView::ResizeToContents);
  header()->setResizeMode(colWIDTH, QHeaderView::ResizeToContents);
  header()->setResizeMode(colRTL, QHeaderView::Stretch);
}

// Get and/or Create item
QTreeWidgetItem* XtorPortTreeWidget::getItem(int row, Column ) 
{
  QTreeWidgetItem* itm = topLevelItem(row);
  if (itm)
    return itm;
  else
  {
    itm = new QTreeWidgetItem(this);
    return itm;
  }
}

QTreeWidgetItem* XtorPortTreeWidget::findXtorConnItem(CarbonCfgXtorConn* xconn)
{
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    QVariant qv = item->data(colPORTNAME, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonCfgXtorConn* conn = (CarbonCfgXtorConn*)qv.value<void*>();
      if (conn != NULL)
      {
        const char* port1 = conn->getPort()->getName();
        const char* port2 = xconn->getPort()->getName();

        if (strcasecmp(port1,port2) == 0)
        {
          return item;
        }
      }
    }
  }
  return NULL;
}

void XtorPortTreeWidget::buildSelectionList(QList<QTreeWidgetItem*>& list)
{
  // The list gives items for every column and row, we just want it row
  // oriented.

  list.clear();

  foreach (QTreeWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(colPORTNAME, Qt::UserRole);
    if (!qv.isNull())
      list.append(item);
  }  
}

void XtorPortTreeWidget::itemSelectionChanged()
{
  QList<QTreeWidgetItem*> itemList;
  buildSelectionList(itemList);
  int nItems = 0;
 // Now, disconnect each item
  // Now walk the list of items to be deleted
  foreach (QTreeWidgetItem *item, itemList)
  {
    QVariant qv = item->data(0, Qt::UserRole);
    CarbonCfgXtorConn* xconn = (CarbonCfgXtorConn*)qv.value<void*>();

    if (xconn->getRTLPort() != NULL)
    {
      emit xtorBindingSelectionChanged(xconn);
      nItems++;
    }
  }

  // Show nothing is selected
  if (nItems == 0)
    emit xtorBindingSelectionChanged(NULL);
}

void XtorPortTreeWidget::updateRow(int row, CarbonCfgXtorConn* xconn, bool clearSelect)
{ 
  if (clearSelect)
    clearSelection();

  QTreeWidgetItem* portName = getItem(row, colPORTNAME);
  QVariant qv = qVariantFromValue((void*)xconn);
  portName->setData(colPORTNAME, Qt::UserRole, qv);
  portName->setText(colPORTNAME, xconn->getPort()->getName());

  UtString width;
  width << "";

  CarbonCfgRTLPort* rtlPort = xconn->getRTLPort();

  if (rtlPort)
    width << rtlPort->getWidth();

  portName->setText(colWIDTH, width.c_str());


  UtString rtlName;
  rtlName << "";

  if (rtlPort)
    rtlName << rtlPort->getName();

  portName->setText(colRTL, rtlName.c_str());
}

void XtorPortTreeWidget::Select(CarbonCfgXtorInstance* xinst)
{
  mInst = xinst;

  clear();

  if (mInst)
  {
    for (UInt32 i = 0, n=mInst->getType()->numPorts(); i<n; ++i)
    {
      CarbonCfgXtorConn* xcon = mInst->getConnection(i);
      updateRow((int)i, xcon, false);
    }
  }
}
CDialog *XtorPortTreeWidget::getAdvancedDialog()
{
  return mDialog;
}

void XtorPortTreeWidget::DeleteBinding(CarbonCfgXtorInstance* xinst)
{
  if (xinst != NULL)
  {
    QList<QTreeWidgetItem*> itemList;
    buildSelectionList(itemList);

    foreach (QTreeWidgetItem *item, itemList)
    {
      QVariant qv = item->data(0, Qt::UserRole);
      CarbonCfgXtorConn* xconn = (CarbonCfgXtorConn*)qv.value<void*>();
      CarbonCfgXtorInstance* xinst = xconn->getInstance();
      UInt32 portIndex = xconn->getPortIndex();
      mCtx->getCfg()->disconnect(xconn);
      // When we delete an xtor connection, it is replaced with
      // a new empty one, which needs to be re-associated with the same
      // item.
      xconn = xinst->getConnection(portIndex);
      updateRow(indexOfTopLevelItem(item), xconn, false);

      mCtx->setDocumentModified(true);
    }

    deselectAll();

    emit xtorBindingsChanged();
  }
}

int XtorPortTreeWidget::AdvancedBind(CarbonCfgXtorInstance* xinst)
{
  int numAssigned = 0;
  
  mDialog->initialize();
  bool playback_active = mCtx->getCQt()->isPlaybackActive();
  if (playback_active)
    mDialog->updateData();

  if (playback_active || (CDialog::Accepted == mDialog->exec()))
  {
    UtString buf;
    UtOStringStream os(&buf);

    os << UtIO::slashify << mDialog->getPrefix() << mDialog->getSuffix();
    
    mDialog->setData(buf);
    numAssigned = BindByName(xinst, mDialog->hasPrefix() ? mDialog->getPrefix() : NULL, mDialog->hasSuffix() ? mDialog->getSuffix() : NULL);
  }

  return numAssigned;
}

int XtorPortTreeWidget::BindByName(CarbonCfgXtorInstance* xinst, const char* prefix, const char* suffix)
{
  CQtRecordBlocker blocker(mCtx->getCQt());

  CarbonCfgXtor* xtor = xinst->getType();

  int numAssigned = 0;
  bool hasPrefix = (prefix != NULL) ? true : false;
  bool hasSuffix = (suffix != NULL) ? true : false;

  // First look for exact matches, then substrings

  // Collect up substring matches, because the ahb-slave at
  // the moment has "hsel" where the model has vic.HSELBLAH.
  // but it also has partial + exact matches for ready/readyin
  // so prefer exact matches.
  UtArray<CarbonCfgRTLPort*> partialActuals;
  UtArray<UInt32> partialFormals;

  for (UInt32 portIndex = 0; portIndex < xtor->numPorts(); ++portIndex)
  {
    CarbonCfgXtorPort* xport = xtor->getPort(portIndex);
    CarbonCfgXtorConn* xconn = xinst->getConnection(portIndex);

    // Can the XTOR port be connected already?  No.
    if (xconn->getRTLPort() != NULL) 
      continue;

    for (UInt32 i = 0, n = mCtx->getCfg()->numRTLPorts(); i < n; ++i) 
    {
      CarbonCfgRTLPort* rtlPort = mCtx->getCfg()->getRTLPort(i);

      // Can the RTL port be assigned already?  Or should it be
      // disconnected?  I'm going to, for the moment, allow it
      // to be auto-assigned even if it's connected, as long as
      // it's not connected to a generator.
      bool exclusive = false;
      for (UInt32 c = 0, nc = rtlPort->numConnections(); c < nc; ++c) {
        if (rtlPort->getConnection(c)->isExclusive()) {
          exclusive = true;
          break;
        }
      }
      if (exclusive) {
        continue;
      }

      // rtlPort->getName() will give me top.portname.  I just want
      // portname.
      const CarbonDBNode* node = carbonDBFindNode(mCtx->getDB(), rtlPort->getName());
      if (node == NULL) {
        continue;
      }
      const char* rtlPortName = carbonDBNodeGetLeafName(mCtx->getDB(), node);
      const char* xtorPortName = xport->getName();

      UtString adjustedPortName;
      if (hasPrefix)
        adjustedPortName << prefix;
      adjustedPortName << xtorPortName;
      if (hasSuffix)
        adjustedPortName << suffix;

//      printf("comparing: %s with %s\n", rtlPortName, adjustedPortName.c_str());
      if (strcasecmp(rtlPortName, adjustedPortName.c_str()) == 0)
      {
        ++numAssigned;
        connectXtor(rtlPort, xinst, portIndex);
      }

      else if (strncasecmp(rtlPortName, adjustedPortName.c_str(),
        std::min(strlen(rtlPortName),
        strlen(adjustedPortName.c_str()))) == 0)
      {
        partialActuals.push_back(rtlPort);
        partialFormals.push_back(portIndex);
      }
    } // for
  } // for

  // If we didn't assign them all, match by substring
  for (UInt32 i = 0, n = partialActuals.size(); i < n; ++i)
  {
    UInt32 portIndex = partialFormals[i];
    CarbonCfgXtorConn* xconn = xinst->getConnection(portIndex);

    if (xconn->getRTLPort() == NULL)
    {
      ++numAssigned;
      connectXtor(partialActuals[i], xinst, portIndex);
    }
  }

  if (numAssigned > 0)
    emit xtorBindingsChanged();

  return numAssigned;
}


void XtorPortTreeWidget::connectXtor(CarbonCfgRTLPort* rtlPort, CarbonCfgXtorInstance* xinst, UInt32 portIndex)
{
  CarbonCfgXtor* xtor = xinst->getType();
  if (portIndex < xtor->numPorts())
  {
    // we already have the connection item in the xtor, and
    // it will be cleaned up in the CarbonCfg.  But we need
    // to fix the item.  We don't want to remove the item
    // from the tree widget -- we just want to redraw it
    CarbonCfgXtorConnID xconn = xinst->getConnection(portIndex);
    //QTreeWidgetItem* conItem = mConnItemMap[xconn];
    //INFO_ASSERT(conItem, "cannot find xtor con item");

    // Now we can connect the old one and show it.
    xconn = carbonCfgConnectXtor(rtlPort, xinst, portIndex);

    QTreeWidgetItem* conItem = findXtorConnItem(xconn);

    INFO_ASSERT(conItem != NULL, "Unable to locate connection");

    if (conItem)
      updateRow(indexOfTopLevelItem(conItem), xconn, false);

    mCtx->setDocumentModified(true);
  }
}

bool XtorPortTreeWidget::isPort(const CarbonDBNode* node)
{
  if (node != NULL)
    return (carbonDBIsPrimaryInput(mCtx->getDB(), node) || 
    carbonDBIsPrimaryOutput(mCtx->getDB(), node) || 
    carbonDBIsPrimaryBidi(mCtx->getDB(), node));
  else
    return false;
}

void XtorPortTreeWidget::dropPort(const char* path, QTreeWidgetItem* item) 
{
  CQtRecordBlocker blocker(mCtx->getCQt());

  UtString warning;
  UtIStringStream is(path);
  UtString buf;
  UtString errmsg;
  while (is.getline(&buf)) 
  {
    StringUtil::strip(&buf, "\n");
    path = buf.c_str();
    const CarbonDBNode* node = carbonDBFindNode(mCtx->getDB(), path);
    if (node == NULL) 
    {
      errmsg << "No such net in the design: " << path << "\n";
    }
    else if (isPort(node))
    {
      CarbonCfgRTLPort* rtlPort = mCtx->getCfg()->findRTLPort(path);
   
      QVariant qv = item->data(colPORTNAME, Qt::UserRole);
      if (!qv.isNull())
      {
        CarbonCfgXtorConn* conn = (CarbonCfgXtorConn*)qv.value<void*>();
        connectXtor(rtlPort, mInst, conn->getPortIndex());
      }
    }
  }

  if (!errmsg.empty()) 
  {
    mCtx->getCQt()->warning(this, errmsg.c_str());
  }

  emit xtorBindingsChanged();
}
