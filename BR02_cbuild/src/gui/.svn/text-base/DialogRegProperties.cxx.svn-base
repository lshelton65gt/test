/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "DialogRegProperties.h"
#include <QMessageBox>
#include "gui/CQt.h"
#include "util/DynBitVector.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"

DialogRegProperties::DialogRegProperties(QWidget *parent)
  : CDialog(parent)
{
  ui.setupUi(this);
}

void DialogRegProperties::setValues(CarbonRadix radix,
                                    CarbonBreakpointType bt,
                                    const char* breakpoint_val,
                                    const char* current_val)
{
  mRadix = radix;
  mBreakpointType = bt;
  mBreakpointValue = breakpoint_val;
  mCurrentValue = current_val;

  switch (radix) {
  case eCarbonBin:  ui.binary->setChecked(true); break;
  case eCarbonOct:  ui.octal->setChecked(true); break;
  case eCarbonHex:  ui.hexadecimal->setChecked(true); break;
  case eCarbonDec:  ui.decimal->setChecked(true); break;
  case eCarbonUDec: ui.udecimal->setChecked(true); break;
  }

  switch (bt) {
  case eCarbonBreakpointOff:        ui.bkpt_off->setChecked(true); break;
  case eCarbonBreakpointOnChange:   ui.bkpt_on_change->setChecked(true); break;
  case eCarbonBreakpointOnEqual:    ui.bkpt_on_eq->setChecked(true); break;
  case eCarbonBreakpointOnNotEqual: ui.bkpt_on_not_eq->setChecked(true); break;
  }

  ui.bkpt_value->setText(breakpoint_val);
} // void DialogRegProperties::setValues

DialogRegProperties::~DialogRegProperties()
{

}

void DialogRegProperties::on_binary_toggled(bool on) {
  if (on) {
    changeRadix(eCarbonBin);
  }
}

void DialogRegProperties::on_octal_toggled(bool on) {
  if (on) {
    changeRadix(eCarbonOct);
  }
}

void DialogRegProperties::on_decimal_toggled(bool on) {
  if (on) {
    changeRadix(eCarbonDec);
  }
}
void DialogRegProperties::on_udecimal_toggled(bool on) {
  if (on) {
    changeRadix(eCarbonUDec);
  }
}

void DialogRegProperties::on_hexadecimal_toggled(bool on) {
  if (on) {
    changeRadix(eCarbonHex);
  }
}    

void DialogRegProperties::on_bkpt_off_toggled(bool on) {
  if (on) {
    mBreakpointType = eCarbonBreakpointOff;
    ui.bkpt_value->setEnabled(false);
  }
}

void DialogRegProperties::setupValueField() {
  ui.bkpt_value->setEnabled(true);
  if (mBreakpointValue.empty()) {
    if (mCurrentValue.empty()) {
      ui.bkpt_value->setText("0"); // some default better than none
    }
    else {
      // default to the current simulation value if there is one
      ui.bkpt_value->setText(mCurrentValue.c_str());
    }
  }
}

void DialogRegProperties::changeRadix(CarbonRadix new_radix) {
  if (! mBreakpointValue.empty() && (new_radix != mRadix)) {
    // Parse the value based on the old radix
    UtIStringStream is(mBreakpointValue.c_str());

    // size the bit-vector to accomodate the max number of bits
    // based on what the user previously typed.  It would be better
    // to get actual bit size of the net, otherwise converting between
    // signed-decimal and other radixes won't work.
    UInt32 num_bits = 32;
    switch (mRadix) {
    case eCarbonHex:    num_bits = 4 * mBreakpointValue.size(); break;
    case eCarbonOct:    num_bits = 3 * mBreakpointValue.size(); break;
    case eCarbonUDec:   // be pessimistic
    case eCarbonDec:    num_bits = 4 * mBreakpointValue.size(); break;
    case eCarbonBin:    num_bits = 1 * mBreakpointValue.size(); break;
    }
    DynBitVector bv(num_bits);

    if (is >> mRadix >> bv) {
      mBreakpointValue.clear();
      UtOStringStream os(&mBreakpointValue);
      os << new_radix << bv;

      // skip leading 0s?

      ui.bkpt_value->setText(mBreakpointValue.c_str());
    }
  }
  mRadix = new_radix;
}

void DialogRegProperties::on_bkpt_on_not_eq_toggled(bool on) {
  if (on) {
    mBreakpointType = eCarbonBreakpointOnNotEqual;
    setupValueField();
  }
}

void DialogRegProperties::on_bkpt_on_eq_toggled(bool on) {
  if (on) {
    mBreakpointType = eCarbonBreakpointOnEqual;
    setupValueField();
  }
}

void DialogRegProperties::on_bkpt_on_change_toggled(bool on) {
  if (on) {
    mBreakpointType = eCarbonBreakpointOnChange;
    ui.bkpt_value->setEnabled(false);
  }
}

void DialogRegProperties::on_bkpt_value_textChanged(const QString& str) {
  mBreakpointValue.clear();
  mBreakpointValue << str;
}
