// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef CBROWSE_BUTTON_H
#define CBROWSE_BUTTON_H

#define BUTTON_WIDTH 8 // pixels
#define BUTTON_HEIGHT 12 // pixels

#include "gui/CQt.h"
#include "iodb/CGraph.h"
#include <QPainter>

class CBrowse;

//! class to represent a CBrowseButton
/*!
 *! This class draws a triangler 3-state button as part of the QGraphicsView
 *! paint() method for CBrowseNode.  It is positioned relative to the
 *! CBrowesrNode.  It also keeps track of which CGraph::Nodes it's connected
 *! to, and provides access to those so that the CBrowse can determine
 *! which nodes should be included in the graph by traversing through the
 *! the nodes belonging to buttons that have state CBrowseButton::eOn.
 */
class CBrowseButton {
public:
  //! button orientation
  enum Orientation {eDown, eLeft, eRight};

  //! button state
  enum State {eOn, eOff, eDisabled};

  //! ctor
  CBrowseButton(double x, double y, Orientation orientation, CBrowse*);

  //! dtor
  ~CBrowseButton();

  //! draw the button graphics (a red, blue, or gray triangle)
  void draw(QPainter* painter);

  //! is the given x/y position inside the triangle?
  /*!
   *! Actually this returns true if it's inside the bounding box enclosing
   *! the triangle.  There's no reason to be quite so precise to exclude
   *! button hits in the bounding box but outside the triange.
   */
  bool inside(double x, double y) const;

  //! Add a node to the button
  void addNode(CGraph::Node*);

  //! How many nodes in the button?
  UInt32 numNodes() const {return mNodes.size();}

  //! get the node at index i
  CGraph::Node* getNode(UInt32 i) const {return mNodes[i];}

  //! get the width of the button
  double getWidth() const;

  //! get the height of the button
  double getHeight() const;

  //! get the current button state
  State getState() const {return mState;}

  //! programmatically set the current state
  void putState(State state) {mState = state;}

  //! get the current button orientation
  Orientation getOrientation() const {return mOrientation;}

  //! toggle the button state (e.g. cause the user clicked and inside()==true
  void toggle();

  //! return the position of the button relative to the CBrowseNode
  QPointF pos() const {return QPointF(mX, mY);}

private:
  double mX;
  double mY;
  Orientation mOrientation;
  State mState;
  UtArray<CGraph::Node*> mNodes;
  CBrowse* mBrowser;
};



#endif
