/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "FieldsTableWidget.h"
#include "DialogBindConstant.h"
#include "ApplicationContext.h"
#include "CarbonProjectWidget.h"
#include "DirectivesEditor.h"

#include "shell/ShellNetMacroDefine.h"
#include "util/ConstantRange.h"
#include "hdl/HdlVerilogPath.h"
#include "hdl/HdlId.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtStringArray.h"
#include "util/UtStringUtil.h"

#include <QDialog>
#include "gui/CQt.h"
#include "gui/CQtDialogControl.h"
#include "CarbonProjectWidget.h"

FieldsTableWidget::FieldsTableWidget(QWidget *parent)
: CTableWidget(parent)
{
  ui.setupUi(this);
  mProjectWidget = NULL;
  mDelegate = new FieldsEditDelegate(this,this);
  mConstantDialog = new DialogBindConstant(this);
  setItemDelegate(mDelegate);
  clearAll();
  mCtx = 0;

  CQT_CONNECT(this, itemSelectionChanged(), this, itemSelectionChanged());

  setEditTriggers(QAbstractItemView::AllEditTriggers);
}

FieldsTableWidget::~FieldsTableWidget()
{
}


void FieldsTableWidget::dropEvent(QDropEvent* ev)
{
  UtString rtlPath;
  rtlPath << ev->mimeData()->text();

  QTableWidgetItem* droppedItem = itemAt(ev->pos());

  if (droppedItem != NULL)
  {
    setCurrentItem(droppedItem);
    UtIStringStream is(rtlPath);
    UtString buf;

    const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPath.c_str());
    bool isMemory = carbonDBIs2DArray(mDB, node) && !carbonDBIsContainedByComposite(mDB, node);
    bool canBeNet = carbonDBCanBeCarbonNet(mDB, node);
    ev->acceptProposedAction();

    if (canBeNet || isMemory)
    {
      if (node != NULL)
        BindField((CarbonDBNode*)node);

      setFocus();
    }
    else
    {
      QMessageBox::warning(this, MODELSTUDIO_TITLE, "Composites are not allowed as register fields.");
    }
  }
  else if (mReg)
  {
    qDebug() << "TODO: Create a field on the fly";
  }
}

void FieldsTableWidget::dragMoveEvent(QDragMoveEvent *ev)
{
  QTableWidgetItem* droppedItem = itemAt(ev->pos());
  if (droppedItem || (mReg && numUsedBits(mReg) < mReg->getWidth()))
  {
    ev->setDropAction(Qt::MoveAction);
    ev->accept();
  }
}

UInt32 FieldsTableWidget::numUsedBits(CarbonCfgRegister* reg)
{
  int usedBits = 0;
  for (UInt32 i=0; i<reg->numFields(); i++)
  {
    CarbonCfgRegisterField* field = reg->getField(i);
    CarbonCfgRegisterLocRTL* rtlReg = field->getLoc()->castRTL();
    if (rtlReg && rtlReg->hasPath())
      usedBits += field->getWidth();
  }

  return usedBits;
}

void FieldsTableWidget::dragEnterEvent(QDragEnterEvent* ev)
{
  ev->acceptProposedAction();
}

void FieldsTableWidget::putContext(ApplicationContext *ctx, CarbonProjectWidget* pw)
{
  mProjectWidget = pw;
  mCtx = ctx;
}

void FieldsTableWidget::Initialize(CarbonCfgRegister *reg)
{
  BlockTableUpdates block(this);

  mDB = mCtx->getDB();
  mReg = reg;

  clearContents();
  setRowCount(0);

  if (mReg)
  {
    for (UInt32 i = 0, n=mReg->numFields(); i<n; ++i)
    {
      CarbonCfgRegisterField* field = mReg->getField(i);
      createRow(field);
    }
  }

 // enum Columns {colNAME=0, colDSTRANGE, colACCESS, colCONTENT, colSRCRANGE, colMEMINDEX, colSTATUS};

  horizontalHeader()->setResizeMode(colNAME, QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(colDSTRANGE, QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(colACCESS, QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(colCONTENT, QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(colSRCRANGE, QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(colMEMINDEX, QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(colSTATUS, QHeaderView::Stretch);

  clearSelection();
}


void FieldsTableWidget::updateRow(int row, CarbonCfgRegisterField* field)
{
  BlockTableUpdates block(this);

  // Name
  QTableWidgetItem *iName = item(row, colNAME);
  iName->setText(field->getName());

  // Dest Range
  UtString dstRange;

  if (mReg->getBigEndian())
  {
    dstRange << "[";
    dstRange << field->getLow(); 
    if (field->getLow() != field->getHigh())
    {
      dstRange << ":";
      dstRange << field->getHigh();
    }
    dstRange << "]";
  }
  else
  {
    dstRange << "[";
    dstRange << field->getHigh(); 
    if (field->getLow() != field->getHigh())
    {
      dstRange << ":";
      dstRange << field->getLow();
    }
    dstRange << "]";
  }


  QTableWidgetItem *iDestRange = item(row, colDSTRANGE);
  iDestRange->setText(dstRange.c_str());

  // Access
  QTableWidgetItem *iAccess = item(row, colACCESS);
  CarbonCfgRegAccessTypeIO accType = field->getAccess();
  
  iAccess->setText(gCarbonCfgRegAccessShortTypes[accType]);

  // Content
  QTableWidgetItem *iContent = item(row, colCONTENT);
  CarbonCfgRegisterLoc* loc = field->getLoc();
  CarbonCfgRegisterLocRTL* rtlLoc = loc->castRTL();
  CarbonCfgRegisterLocArray* arrayLoc = loc->castArray();

  bool validLoc = hasValidLocation(field);

  if (loc->getType() == eCfgRegLocConstant)
  {
    UtString tieValue;
    UtOStringStream ss(&tieValue);
    ss << "0x" << UtIO::hex << loc->castConstant()->getValue();
    iContent->setText(tieValue.c_str());
  }
  else if (loc->getType() == eCfgRegLocRegister)
    iContent->setText(rtlLoc->getPath());
  else if (loc->getType() == eCfgRegLocArray)
    iContent->setText(arrayLoc->getPath());
  else if (iContent)
    iContent->setText("");

  // Source Range
  QTableWidgetItem *iSrcRange = item(row, colSRCRANGE);
  if (validLoc && (loc->getType() == eCfgRegLocRegister || loc->getType() == eCfgRegLocArray) )
  {
    UtString srcRange;

    bool isScalar = false;
    if (rtlLoc && (rtlLoc->getLeft() == rtlLoc->getRight()))
      isScalar = true;

    if (!isScalar)
    {
      srcRange << "[";
      srcRange << rtlLoc->getLeft();
      if (rtlLoc->getLeft() != rtlLoc->getRight())
        srcRange << ":" << rtlLoc->getRight();
      srcRange << "]";
    }
 
    iSrcRange->setText(srcRange.c_str());
  }
  else if (iSrcRange)
    iSrcRange->setText("");

  // Memory Index
  QTableWidgetItem *iMemIndex = item(row, colMEMINDEX);
  if (validLoc && arrayLoc != NULL)
  {
    UInt32 index = arrayLoc->getIndex(); 

    UtString hexIndex;
    UtOStringStream ss(&hexIndex);
    ss << "0x" << UtIO::hex << index;

    iMemIndex->setText(hexIndex.c_str());
  }
  else if (iMemIndex)
    iMemIndex->setText("");

  // validate the fields
  checkRegisterFields();
}

bool FieldsTableWidget::colorField(QTableWidgetItem* rowItem, bool signalError, const char* msg)
{
  QColor color(getDefaultTextColor());
  QColor red(Qt::red);

  if (signalError)
    color = red;   

  QTableWidgetItem* statusItem = item(rowItem->row(), colSTATUS);
  if (msg && statusItem == NULL)
  {
    statusItem = new QTableWidgetItem;
    setItem(rowItem->row(), colSTATUS, statusItem);
  }

  if (statusItem != NULL)
  {
    if (signalError && msg != NULL)
      statusItem->setText(msg);
    else
      statusItem->setText("");
  }

  for (int col = 0; col < columnCount() ; ++col)
  {
    QTableWidgetItem* itm = item(rowItem->row(), col);
    if (itm != NULL)
      itm->setTextColor(color);
  }

  return signalError;
}

QColor FieldsTableWidget::getDefaultTextColor()
{
  QTreeWidget tmpTree;
  tmpTree.setColumnCount(1);
  QTreeWidgetItem tmpItem(&tmpTree);
  return tmpItem.textColor(0);
}

void FieldsTableWidget::FinishEdit()
{
  //if (mEditItem)
  //{
  //  mDelegate->finishEdit();
  //  mEditItem = 0;
  //}
}

bool FieldsTableWidget::checkField(QTableWidgetItem* item, CarbonCfgRegisterField* field)
{
  UtString errMsg;
  if (mReg == NULL)
    return true;

  // Check bit ranges
  int lsb = field->getLow();
  int msb = field->getHigh();

  if (mCtx->castCoware()) {
    int fieldWidth = std::abs((int)field->getHigh()-(int)field->getLow());
    if (fieldWidth > 32)
    {
      errMsg << "Any single field must not exceed 32 bits in size, field is (" << fieldWidth << ") bits";
      return colorField(item, true, errMsg.c_str());
    }
  }

  int width = mReg->getWidth();

  if (lsb < 0 || lsb >= width)
  {
    return colorField(item, true, errMsg.c_str());
  }

  if (msb < 0 || msb >= width)
  {
    return colorField(item, true, errMsg.c_str());
  }

  CarbonCfgRegisterLoc* loc = field->getLoc();
  CarbonCfgRegisterLocReg* locReg = loc->castReg();
//  CarbonCfgRegisterLocArray* locArray= loc->castArray();

  // Scalar or Vector
  if (locReg != NULL)
  {
    const CarbonDBNode* node = carbonDBFindNode(mDB, locReg->getPath());
    if (node == NULL)
    {
      if (strlen(locReg->getPath()) > 0)
        errMsg << "Unable to locate Carbon DB Node for: " << locReg->getPath();
      else
        errMsg << "Unbound field";

      return colorField(item, true, errMsg.c_str());
    }
    else // Found a node, now verify against it
    {
//      int rtlMSB = carbonDBGetMSB(mDB, node);
//      int rtlLSB = carbonDBGetLSB(mDB, node);
//      int rtlNodeWidth = carbonDBGetWidth(mDB, node);

      int partSelectWidth = CBITWIDTH((int)locReg->getLeft(), (int)locReg->getRight());
      int fieldWidth = field->getWidth();

      if (partSelectWidth != fieldWidth)
      {
        if (partSelectWidth == 1)
          errMsg << "Scalar does not match field width (" << fieldWidth << ")";      
        else
          errMsg << "Part select width (" << partSelectWidth << ") does not match field width (" << fieldWidth << ")";
        return colorField(item, true, errMsg.c_str());
      }
    }
  }

  return colorField(item, false, NULL);
}

// Walk the gui objects and validate each one for holes and overlaps
void FieldsTableWidget::checkRegisterFields()
{
//  bool someErrors = !mReg->checkFields();

//  int numBits = mReg->getWidth();

  for (int row=0, nRows=rowCount(); row<nRows; ++row)
  {
    QTableWidgetItem* itm = item(row, 0);
    if (itm)
    {
      QVariant qv = itm->data(Qt::UserRole);
      CarbonCfgRegisterField* field = (CarbonCfgRegisterField*)qv.value<void*>();

      checkField(itm, field);
    }
  }

//  clearSelection();
}

void FieldsTableWidget::createRow(CarbonCfgRegisterField* field)
{
  int row = rowCount();
  setRowCount(row+1);

  QTableWidgetItem *itemName = new QTableWidgetItem;
  QVariant qv = qVariantFromValue((void*)field);
  itemName->setData(Qt::UserRole, qv);

  setItem(row, colNAME, itemName);
  setItem(row, colDSTRANGE, new QTableWidgetItem()); 
  setItem(row, colACCESS, new QTableWidgetItem());
  setItem(row, colCONTENT, new QTableWidgetItem());
  setItem(row, colSRCRANGE, new QTableWidgetItem());
  setItem(row, colMEMINDEX, new QTableWidgetItem());

  updateRow(row, field);
}

void FieldsTableWidget::buildSelectionList(QList<QTableWidgetItem*>& list)
{
  // The list gives items for every column and row, we just want it row
  // oriented.

  list.clear();

  foreach (QTableWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonCfgRegisterField* field = (CarbonCfgRegisterField*)qv.value<void*>();
      if (field != NULL)
      {
        list.append(item);
      }
    }
  }  
}

CarbonCfgRegAccessTypeIO FieldsTableWidget::computeAccess(CarbonDBNode* node)
{
  bool readAccess = false;
  bool writeAccess = false;

  if (carbonDBIsObservable(mDB, node) || carbonDBIsVisible(mDB, node))
    readAccess = true;

  if (carbonDBIsDepositable(mDB, node))
    writeAccess = true;

  if (readAccess && !writeAccess)
    return eCfgRegAccessRO;
  
  if (!readAccess && writeAccess)
    return eCfgRegAccessWO;

  return eCfgRegAccessRW;
}


// Assign the Carbon Object to the Field
void FieldsTableWidget::bind(QTableWidgetItem* item, CarbonCfgRegisterField* field, CarbonDBNode* node)
{
  const char* fullName = carbonDBNodeGetFullName(mDB, node);

  // Add an observe directive for this node 
  if (carbonDBIsScalar(mDB, node) || carbonDBIsVector(mDB, node))
  {
    CarbonCfgRegisterLocReg* loc = new CarbonCfgRegisterLocReg(fullName);

    loc->putLeft(carbonDBGetMSB(mDB, node));
    loc->putRight(carbonDBGetLSB(mDB, node));
    loc->putHasRange(true);

    field->putAccess(computeAccess(node));
    field->putLoc(loc);
    updateRow(item->row(), field);
  }
  else if (carbonDBIs2DArray(mDB, node))
  {
    CarbonCfgRegisterLocArray* loc = new CarbonCfgRegisterLocArray(fullName, 0);

    loc->putLeft(carbonDBGetMSB(mDB, node));
    loc->putRight(carbonDBGetLSB(mDB, node));
    loc->putIndex(0);
    loc->putHasRange(true);

    field->putAccess(computeAccess(node));
    field->putLoc(loc);
    updateRow(item->row(), field);
  }
  else
  {
    QMessageBox::warning(this, tr(APP_NAME), "Unknown type");
  }

  mCtx->setDocumentModified(true);

  clearSelection();

  DirectivesEditor* dirEditor = mProjectWidget->directivesEditor();
  if (dirEditor)
  {
    TabNetDirectives* netDirectives = dirEditor->getNetDirectivesTab();
    QStringList nets;
    nets << fullName;
    netDirectives->setDirective(nets, "observeSignal", true);
  }
}

// Deletes the selected field(s)
void FieldsTableWidget::DeleteField()
{
  QList<QTableWidgetItem*> selectedItemList;
  buildSelectionList(selectedItemList);
  CarbonCfgRegisterField* field = NULL;
  foreach (QTableWidgetItem *item, selectedItemList)
  {
    QVariant qv = item->data(Qt::UserRole);
    field = (CarbonCfgRegisterField*)qv.value<void*>();

    // We have the field to work with;
    if (field != NULL)
    {
      blockSignals(true);
      int row = item->row();
      removeRow(row);
      mReg->removeField(field);
      mCtx->setDocumentModified(true);
      blockSignals(false);
    }
  }
}


// Bind the Node to the current selection
void FieldsTableWidget::BindConstant()
{
  QList<QTableWidgetItem*> selectedItemList;
  buildSelectionList(selectedItemList);
  CarbonCfgRegisterField* field = NULL;

  foreach (QTableWidgetItem *item, selectedItemList)
  {
    QVariant qv = item->data(Qt::UserRole);
    field = (CarbonCfgRegisterField*)qv.value<void*>();

    // We have the field to work with;
    if (field != NULL)
    {
      mConstantDialog->setup(mDB, mReg, field);
      bool playback_active = mCtx->getCQt()->isPlaybackActive();
      if (playback_active) {
        // Update model with value set during playback, now that
        // the rest of the dialog's data is valid.
        mConstantDialog->updateData();
      }
      if (playback_active ||
          (QDialog::Accepted == mConstantDialog->exec()))
      {
        CarbonCfgRegisterLoc* currLoc = field->getLoc();
        CarbonCfgRegisterLocConstant* c = new CarbonCfgRegisterLocConstant(mConstantDialog->getValue());
        field->putLoc(c);
        field->putAccess(eCfgRegAccessRO);
        delete currLoc;
        updateRow(item->row(), field);
        mCtx->setDocumentModified(true);
      }
    }
    break;
  }
}

// Bind the Node to the current selection
void FieldsTableWidget::BindField(CarbonDBNode* node)
{
  QList<QTableWidgetItem*> selectedItemList;
  buildSelectionList(selectedItemList);
  CarbonCfgRegisterField* field = NULL;

  foreach (QTableWidgetItem *item, selectedItemList)
  {
    QVariant qv = item->data(Qt::UserRole);
    field = (CarbonCfgRegisterField*)qv.value<void*>();

    // We have the field to work with;
    if (field != NULL)
    {
      bool assignValue = true;
      if (hasValidLocation(field))
      {
        if (QMessageBox::warning(this, tr(APP_NAME), "Replace value?", QMessageBox::Ok|QMessageBox::Cancel) == QMessageBox::Cancel)
          assignValue = false;
      }

      if (assignValue)
        bind(item, field, node);
    }
    break; // do only the first one
  }
}

void FieldsTableWidget::clearAll()
{
  mReg = 0;
  mDB = 0;
  mEditItem = 0;
  clearContents();
  setRowCount(0);
}

CDialog *FieldsTableWidget::getConstantDialog()
{
  return mConstantDialog;
}

void FieldsTableWidget::setModelData(const UtString &data, QAbstractItemModel *model,
                                     const QModelIndex &index)
{
  QTableWidgetItem* i = item(index.row(), 0);
  QVariant qv = i->data(Qt::UserRole);
  CarbonCfgRegisterField* field = (CarbonCfgRegisterField*)qv.value<void*>();
  CarbonCfgRegisterLoc* loc = field->getLoc();
  CarbonCfgRegisterLocRTL* rtlLoc = loc->castRTL();
  CarbonCfgRegisterLocArray* arrayLoc = loc->castArray();
  
  switch (index.column())
  {
  case FieldsTableWidget::colMEMINDEX:
    {
      UtIO::Radix radix = UtIO::hex;
      UtString newVal;
      newVal << data;

      if (0 == strncasecmp(newVal.c_str(), "0x", 2))
      {
        newVal.erase(0, 2);
        radix = UtIO::hex;
      }
      else
      {
        radix = UtIO::dec;
      }
      UInt32 newIndex;

      UtIStringStream sNewVal(newVal);
      if (arrayLoc != NULL && sNewVal >> radix >> newIndex)
      {
        UtString hexIndex;
        UtOStringStream ss(&hexIndex);
        ss << "0x" << UtIO::hex << newIndex;

        model->setData(index, hexIndex.c_str());
        mCtx->setDocumentModified(true);
        arrayLoc->putIndex(newIndex);
      }
    }
    break;

  case FieldsTableWidget::colACCESS:
    {
      mCtx->setDocumentModified(true);
      UtString newVal;
      newVal << data;
      model->setData(index, newVal.c_str());
      
      for (int i=0; i<100; i++) 
      {
        const char* accName = gCarbonCfgRegAccessShortTypes[i];

        if (accName == NULL) {
          break;
        }
        
        if (0 == strcmp(accName, newVal.c_str()))
        {
          field->putAccess((CarbonCfgRegAccessType)i);
          break;
        }
      }
    }
    break;
  case FieldsTableWidget::colNAME:
    {
      mCtx->setDocumentModified(true);
      field->putName(data.c_str());
      model->setData(index, data.c_str());
    }
    break;
  case FieldsTableWidget::colDSTRANGE:
    {
      HdlVerilogPath hpath;
      mCtx->setDocumentModified(true);
      UtString newVal;
      
      if (data.empty() || (data[0] != '[')) {
        newVal << "[" << data << "]";
      } else {
        newVal << data;
      }

      const char* vector = newVal.c_str();
      HdlId hdlid;
      if (HdlHierPath::eLegal == hpath.parseVectorInfo(&vector, NULL, &hdlid))
      {
        model->setData(index, newVal.c_str()); 
        switch (hdlid.getType())
        {
        case HdlId::eVectBitRange:
          field->putLow(hdlid.getVectLSB());
          field->putHigh(hdlid.getVectMSB());
          break;

        case HdlId::eVectBit:
          field->putLow(hdlid.getVectIndex());
          field->putHigh(hdlid.getVectIndex());
          break;
        
        default:
        case HdlId::eScalar:
          field->putLow(hdlid.getVectLSB());
          field->putHigh(hdlid.getVectLSB());
         break;
        }
      }
    }
    break;
  
  case FieldsTableWidget::colSRCRANGE:
    {
      HdlVerilogPath hpath;
      mCtx->setDocumentModified(true);

      if (rtlLoc)
      {
        UtString newVal;
        
        if (data.empty() || (data[0] != '[')) {
          newVal << "[" << data << "]";
        } else {
          newVal << data;
        }

        const char* vector = newVal.c_str();
        HdlId hdlid;
        if (HdlHierPath::eLegal == hpath.parseVectorInfo(&vector, NULL, &hdlid))
        {
          model->setData(index, newVal.c_str()); 
          switch (hdlid.getType())
          {
          case HdlId::eVectBitRange:
            rtlLoc->putRight(hdlid.getVectLSB());
            rtlLoc->putLeft(hdlid.getVectMSB());
            break;

          case HdlId::eVectBit:
            rtlLoc->putRight(hdlid.getVectIndex());
            rtlLoc->putLeft(hdlid.getVectIndex());
            break;
          
          default:
          case HdlId::eScalar:
            rtlLoc->putRight(hdlid.getVectLSB());
            rtlLoc->putLeft(hdlid.getVectLSB());
           break;
          }
        }
      }
    }
    break;
  default:
    break;
  }

  CTableWidget::setModelData(data, model, index);
}

bool FieldsTableWidget::hasValidLocation(CarbonCfgRegisterField* field)
{
  CarbonCfgRegisterLoc* loc = field->getLoc();
  CarbonCfgRegisterLocRTL* locRtl = loc->castRTL();
  CarbonCfgRegisterLocReg* locReg = loc->castReg();
  CarbonCfgRegisterLocArray* locArray = loc->castArray();
  CarbonCfgRegisterLocConstant* locConst = loc->castConstant();

  if (locRtl != NULL && locRtl->hasPath())
    return true;

  if (locReg != NULL && locReg->hasPath())
    return true;

  if (locArray != NULL && locArray->hasPath())
    return true;

  if (locConst != NULL)
    return true;

  return false;
}

// Add a new field
void FieldsTableWidget::AddField()
{
  // Determine if there are any unused bits
  // Basically, the Register has a Width like 32
  // So total up the fields and compute how many bits are left.
  int regWidth = mReg->getWidth();
  int usedBits = 0;

  for (UInt32 i = 0, n=mReg->numFields(); i<n; ++i)
  {
    CarbonCfgRegisterField* field = mReg->getField(i);
    usedBits += field->getWidth();
  }

  if (usedBits < regWidth) // Room for a new Field
  {
    int unusedBits = regWidth-usedBits;
    CarbonCfgRegisterField *field = new CarbonCfgRegisterField(unusedBits, 0, eCfgRegAccessRW);
    
    UtString fieldName;

    if (mReg->numFields() > 0)
      fieldName << "Field" << mReg->numFields();
    else
      fieldName << "Field";

    field->putName(fieldName.c_str());

    field->putLow(regWidth-unusedBits);
    field->putHigh(regWidth-1);

    CarbonCfgRegisterLocReg* loc = new CarbonCfgRegisterLocReg("");
    field->putLoc(loc);
    mReg->addField(field);

    createRow(field);
  }
  else
  {
    UtString msg;
    msg << "The register is fully allocated to its maximum size of " << regWidth ;
    QMessageBox::warning(this, tr(APP_NAME), msg.c_str());
  }  
}

void FieldsTableWidget::itemClicked(QTableWidgetItem* item)
{
  if (item && item->flags() & Qt::ItemIsEditable)
    editItem(item);
}

void FieldsTableWidget::itemSelectionChanged()
{
  QList<QTableWidgetItem*> itemList;
  buildSelectionList(itemList);

  foreach (QTableWidgetItem *item, itemList)
  {
    QVariant qv = item->data(Qt::UserRole);
    CarbonCfgRegisterField* field = (CarbonCfgRegisterField*)qv.value<void*>();
  
    emit selectionChanged(field);
  }

  // Show nothing is selected
  if (itemList.count() == 0)
    emit selectionChanged(NULL);
}

// Fields Edit Delegate

FieldsEditDelegate::FieldsEditDelegate(QObject *parent, FieldsTableWidget* t)
: QItemDelegate(parent), table(t)
{
}

void FieldsEditDelegate::currentIndexChanged(int i)
{
  if (i != -1)
  {
    QComboBox *editor = qobject_cast<QComboBox *>(sender());
    emit commitData(editor);
    emit closeEditor(editor);
  }
}

QWidget *FieldsEditDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                          const QModelIndex &index) const
{
  QTableWidgetItem* item = table->item(index.row(), 0);
  QVariant qv = item->data(Qt::UserRole);
  CarbonCfgRegisterField* field = (CarbonCfgRegisterField*)qv.value<void*>();

  switch (index.column())
  {
  case FieldsTableWidget::colACCESS:
    {
      CarbonCfgRegAccessTypeIO accType = field->getAccess();

      QComboBox *cbox = new QComboBox(parent);
      cbox->addItem("ro");
      cbox->addItem("wo");
      cbox->addItem("rw");
  
      const char* currAccess = gCarbonCfgRegAccessShortTypes[accType];

      int currIndex = cbox->findText(currAccess);
      cbox->setCurrentIndex(currIndex);

      table->mEditItem = cbox;

      CQT_CONNECT(cbox, currentIndexChanged(int), this, currentIndexChanged(int));
      return cbox;
    }
    break;
  case FieldsTableWidget::colNAME:
  case FieldsTableWidget::colSRCRANGE:
  case FieldsTableWidget::colDSTRANGE:
  case FieldsTableWidget::colMEMINDEX:
    {
      QLineEdit *editor = new QLineEdit(parent);
      connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
      return editor;
    }
    break;
  default:
    break;
  }

  table->mEditItem = 0;

  return NULL;
}

void FieldsEditDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  if (editor)
  {
    emit commitData(editor);
    emit closeEditor(editor);
  }
}

void FieldsEditDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}
void FieldsEditDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                      const QModelIndex &index) const
{
  UtString editText;

  switch (index.column())
  {
  case FieldsTableWidget::colACCESS:
    {
      QComboBox* cbox = qobject_cast<QComboBox *>(editor);
      if (cbox != NULL) {
        editText << cbox->currentText();
      }
    }
    break;
  case FieldsTableWidget::colMEMINDEX:
  case FieldsTableWidget::colNAME:
  case FieldsTableWidget::colDSTRANGE:
  case FieldsTableWidget::colSRCRANGE:
    {
      QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
      if (edit) {
        editText << edit->text();
      }
    }
    break;
  default:
    break;
  }

  table->setModelData(editText, model, index);

  table->mEditItem = 0;
  table->checkRegisterFields();
}
