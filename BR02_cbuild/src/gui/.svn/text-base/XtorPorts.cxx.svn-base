//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "car2cow.h"
#include "XtorPorts.h"

#include "shell/carbon_capi.h"
#include "cfg/CarbonCfg.h"

XtorPorts::XtorPorts(QWidget *parent)
: CDragDropTreeWidget(false, parent)
{
  ui.setupUi(this);
  CQT_CONNECT(this, itemSelectionChanged(), this, itemSelectionChanged());
}

XtorPorts::~XtorPorts()
{

}
void XtorPorts::buildSelectionList(QList<QTreeWidgetItem*>& list)
{
  list.clear();

  foreach (QTreeWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(colPORT, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonCfgRTLPortID node = (CarbonCfgRTLPortID)qv.value<void*>();
      if (node != NULL)
      {
        list.append(item);
      }
    }
  }  
}
void XtorPorts::itemSelectionChanged()
{
  QList<QTreeWidgetItem*> itemList;
  buildSelectionList(itemList);

  foreach (QTreeWidgetItem *item, itemList)
  {
    QVariant qv = item->data(colPORT, Qt::UserRole);
    CarbonCfgRTLPortID port = (CarbonCfgRTLPortID)qv.value<void*>();
    emit portSelectionChanged(port);
    putDragData(port->getName());
  }

  if (itemList.count() == 0)
  {
    putDragData(NULL);
    emit portSelectionChanged(NULL);
  }
}
void XtorPorts::Populate(ApplicationContext* ctx, bool /*initPorts*/)
{
  mCtx = ctx;
  CQtRecordBlocker blocker(mCtx->getCQt());

  populateLoop(mCtx->getDB(), eCarbonCfgRTLInput,
    carbonDBLoopPrimaryInputs(mCtx->getDB()));
  populateLoop(mCtx->getDB(), eCarbonCfgRTLInout,
    carbonDBLoopPrimaryBidis(mCtx->getDB()));
  populateLoop(mCtx->getDB(), eCarbonCfgRTLOutput,
    carbonDBLoopPrimaryOutputs(mCtx->getDB()));

  // Not really sure if we should show these, but I think so.
  populateLoop(mCtx->getDB(), eCarbonCfgRTLInput,
    carbonDBLoopScDepositable(mCtx->getDB()));
  populateLoop(mCtx->getDB(), eCarbonCfgRTLOutput,
    carbonDBLoopScObservable(mCtx->getDB()));
}

void XtorPorts::putContext(ApplicationContext* ctx)
{
  mCtx = ctx;
  header()->setResizeMode(colPORT, QHeaderView::ResizeToContents);
  header()->setResizeMode(colWIDTH, QHeaderView::ResizeToContents);
  header()->setResizeMode(colXTORPORT, QHeaderView::Stretch);
}


void XtorPorts::Refresh()
{
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    QVariant qv = item->data(colPORT, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonCfgRTLPortID port = (CarbonCfgRTLPortID)qv.value<void*>();
      if (port != NULL)
      {
        const CarbonDBNode* node = carbonDBFindNode(mCtx->getDB(), port->getName());
        const char* leafName = carbonDBNodeGetLeafName(mCtx->getDB(), node);
        item->setText(colPORT, leafName);
        UtString w;
        w << port->getWidth();
        item->setText(colWIDTH, w.c_str());
        item->setText(colXTORPORT, "");

       // Go through the connections
        for (UInt32 i=0; i<port->numConnections(); i++)
        {
          CarbonCfgRTLConnection* conn = port->getConnection(i);
          CarbonCfgXtorConn* xconn = conn->castXtorConn();
          if (xconn)
          {
            UtString text;
            UtString currText;
            currText << item->text(colXTORPORT);
            
            if (currText.length() > 0)
              text << currText << "," << xconn->getInstance()->getName() << ":" << xconn->getPort()->getName();
            else
              text << xconn->getInstance()->getName() << ":" << xconn->getPort()->getName();

            item->setText(colXTORPORT, text.c_str());
          }
        }
      }
    }
  }
}

void XtorPorts::populateLoop(CarbonDB* carbonDB, CarbonCfgRTLPortType /*type*/, CarbonDBNodeIter* iter)
{
  const CarbonDBNode* node;

  while ((node = carbonDBNodeIterNext(iter)) != NULL)
  {
    int width = carbonDBGetWidth(carbonDB, node);
    const char* name = carbonDBNodeGetFullName(carbonDB, node);
    const char* leafName = carbonDBNodeGetLeafName(carbonDB, node);

    CarbonCfgRTLPortID port = mCtx->getCfg()->findRTLPort(name);

    if (port != NULL) 
    {
      QTreeWidgetItem* item = new QTreeWidgetItem(this);
      item->setText(colPORT, leafName);
      UtString w;
      w << width;
      item->setText(colWIDTH, w.c_str());
      QVariant qv = qVariantFromValue((void*)port);
      item->setData(colPORT, Qt::UserRole, qv);

      // Go through the connections
      for (UInt32 i=0; i<port->numConnections(); i++)
      {
        CarbonCfgRTLConnection* conn = port->getConnection(i);
        CarbonCfgXtorConn* xconn = conn->castXtorConn();
        if (xconn)
        {
          UtString text;
          UtString currText;
          currText << item->text(colXTORPORT);
          
          if (currText.length() > 0)
            text << currText << "," << xconn->getInstance()->getName() << ":" << xconn->getPort()->getName();
          else
            text << xconn->getInstance()->getName() << ":" << xconn->getPort()->getName();

          item->setText(colXTORPORT, text.c_str());
        }
      }
    }
  }
  carbonDBFreeNodeIter(iter);
}
