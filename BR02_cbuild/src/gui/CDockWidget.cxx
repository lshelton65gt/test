//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "gui/CDockWidget.h"

CDockWidget::CDockWidget(const char* name, QWidget *parent)
  : QDockWidget(name, parent)
{}

void CDockWidget::closeEvent(QCloseEvent *e) {
  QDockWidget::closeEvent(e);
  emit closed(this);
}
