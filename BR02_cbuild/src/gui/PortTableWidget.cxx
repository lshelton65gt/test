/******************************************************************************
 Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "car2cow.h"
#include "PortTableWidget.h"
#include "TieOffTableWidget.h"
#include "CarbonProjectWidget.h"
#include "DisconnectTableWidget.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "shell/carbon_capi.h"
#include "cfg/CarbonCfg.h"
#include "gui/CQt.h"

PortTableWidget::PortTableWidget(QWidget* parent) : QTableWidget(parent)
{
  mDB = NULL;
  mCfg = NULL;

  initialize();

  createActions();

  connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(QPoint)));

  setEditTriggers(QAbstractItemView::AllEditTriggers);
}
PortTableWidget::~PortTableWidget()
{
}

void PortTableWidget::initialize()
{
  QStringList labels;
  labels << "ESL Name" << "Width" << "Direction" << "Type" << "RTL Mode" << "RTL Port";

  setColumnCount(labels.count());
  setHorizontalHeaderLabels(labels);

  horizontalHeader()->setResizeMode(colESLPORT, QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(colWIDTH, QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(colDIRECTION, QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(colTYPE, QHeaderView::Interactive);
  horizontalHeader()->setResizeMode(colRTLMODE, QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(colNAME, QHeaderView::Stretch);

  setItemDelegate(new PortTableEditDelegate(0, this));
}
void PortTableWidget::createActions()
{
  actionTieToOne = new QAction(tr("Tie to 0x1"), this);
  connect(actionTieToOne, SIGNAL(triggered()), this, SLOT(menuTieToOne()));

  actionTieToZero = new QAction(tr("Tie to 0x0"), this);
  connect(actionTieToZero, SIGNAL(triggered()), this, SLOT(menuTieToZero()));

  actionDisconnect = new QAction(tr("Disconnect"), this);
  connect(actionDisconnect, SIGNAL(triggered()), this, SLOT(menuDisconnect()));
}

void PortTableWidget::showContextMenu(const QPoint &pos)
{
//  QTableWidgetItem *item = itemAt(pos);

  QMenu menu(this);
  menu.addAction(actionTieToOne);
  menu.addAction(actionTieToZero);
  menu.addAction(actionDisconnect);
  menu.exec(viewport()->mapToGlobal(pos));
}

void PortTableWidget::removeESLPort(CarbonCfgRTLPortID rtlPort)
{
  // First gather the disconnects, we can remove while iterating
  UInt32 numConnections = rtlPort->numConnections();
  UtArray<CarbonCfgESLPort*> disconnects;
  for (UInt32 i = 0; i < numConnections; ++i) {
    CarbonCfgRTLConnection* con = rtlPort->getConnection(i);
    CarbonCfgESLPort* eslPort = con->castESLPort();
    if (eslPort != NULL) {
      disconnects.push_back(eslPort);
    }
  }

  // Now disconnect any of them
  for (UtArray<CarbonCfgESLPort*>::iterator i = disconnects.begin(); i != disconnects.end(); ++i) {
    CarbonCfgESLPort* eslPort = *i;
    mCfg->disconnect(eslPort);
  }

}

void PortTableWidget::mousePressEvent(QMouseEvent* ev)
{
  if (ev->button() == Qt::LeftButton)
  {
    mPressPos = ev->pos();
  
    UtString selectedPorts;
    QList<QTableWidgetItem*> selectedItemList;
    buildSelectionList(selectedItemList);

   // Now walk the list of items to be deleted
    foreach (QTableWidgetItem *item, selectedItemList)
    {
      QVariant qv = item->data(Qt::UserRole);
      CarbonCfgRTLPortID rtlPort = (CarbonCfgRTLPortID)qv.value<void*>();
//      qDebug() << "rtl port: " << rtlPort->getName() << " has " << rtlPort->numConnections() << " connections";
      selectedPorts << rtlPort->getName() << "\n";
    }

    // Any items to handle?
    if (selectedItemList.count() > 0)
    {
      QMimeData *mimeData = new QMimeData;
      mimeData->setText(selectedPorts.c_str());

      QDrag *drag = new QDrag(this);
      drag->setMimeData(mimeData);
      drag->setHotSpot(ev->pos() - rect().topLeft());

      Qt::DropAction dropAction = drag->start(Qt::MoveAction);
       
//      qDebug() << "Drop action: " << dropAction;

      switch (dropAction)
      {
      case Qt::CopyAction:
//        qDebug() << "Action accepted: Deleting item(s) now";
       // Now walk the list of items to be deleted

        foreach (QTableWidgetItem *item, selectedItemList)
        {
          QVariant qv = item->data(Qt::UserRole);
          CarbonCfgRTLPortID rtlPort = (CarbonCfgRTLPortID)qv.value<void*>();
          removeESLPort(rtlPort);
        }

        foreach (QTableWidgetItem *item, selectedItemList)
        {
          removeRow(item->row());
        }
        clearSelection();
        break;
      default:
      case Qt::IgnoreAction:
        break;
      }
      ev->accept();
    }
  }

  QTableWidget::mousePressEvent(ev);
}

void PortTableWidget::mouseMoveEvent(QMouseEvent* ev)
{
  QTableWidget::mouseMoveEvent(ev);
}


void PortTableWidget::tieOffSelected(UInt32 value)
{
  QList<QTableWidgetItem*> itemList;
  buildSelectionList(itemList);
  QList<int> rowList;

  // Now walk the list of items to be deleted
  foreach (QTableWidgetItem *item, itemList)
  {
    QVariant qv = item->data(Qt::UserRole);
    CarbonCfgRTLPortID rtlPort = (CarbonCfgRTLPortID)qv.value<void*>();

     // Disconnect it
    removeESLPort(rtlPort);

    carbonCfgTie(rtlPort, &value, 1);

    // Remove it from the Table
    //removeRow(item->row());
    rowList.append(item->row());
    // Get the Tie we just created
    CarbonCfgTie* tie = getTie(rtlPort);
    const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());

    // Add it to the tie table
    m_pTieOffTable->AddTie(rtlPort, tie, carbonDBIsTied(mDB, node));   
  }

  setCurrentItem(NULL);

  foreach(int row, rowList)
  {
    removeRow(row);
  }

  mCtx->setDocumentModified(true);
}

void PortTableWidget::menuTieToOne()
{
  tieOffSelected(0xffffffff);
}

void PortTableWidget::menuTieToZero()
{
  tieOffSelected(0);
}

void PortTableWidget::buildSelectionList(QList<QTableWidgetItem*>& list)
{
  list.clear();

  foreach (QTableWidgetItem *item, selectedItems())
  {
    if (item->column() == colESLPORT)
    {
      QVariant qv = item->data(Qt::UserRole);
      if (!qv.isNull())
      {
        CarbonCfgRTLPortID rtlPort = (CarbonCfgRTLPortID)qv.value<void*>();
        if (rtlPort != NULL)
        {
          list.append(item);
        }
      }
    }
  }  
}


void PortTableWidget::menuDisconnect()
{
  QList<QTableWidgetItem*> itemList;
  buildSelectionList(itemList);
  QList<int> rowList;

  // Now, disconnect each item
  // Now walk the list of items to be deleted
  foreach (QTableWidgetItem *item, itemList)
  {
    QVariant qv = item->data(Qt::UserRole);
    CarbonCfgRTLPortID rtlPort = (CarbonCfgRTLPortID)qv.value<void*>();

    bool disconnectIt = true;
    const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
    if (node && carbonDBIsTied(mDB, node))
    {
        UtString msg;
        msg << "The port: " << rtlPort->getName() << " is tied off by a compiler directive.\n";
        msg << "Are you sure you want to remove it?";
        
        if (QMessageBox::question(this, MODELSTUDIO_TITLE, msg.c_str(), 
          QMessageBox::No|QMessageBox::Yes, QMessageBox::No) == QMessageBox::No)
          disconnectIt = false;
    }

    if (disconnectIt)
    {
      // Disconnect it
      removeESLPort(rtlPort);
      rowList.append(item->row());
      // Add it to the tie table
      m_pDisconnectTable->AddDisconnect(rtlPort);   
    }
  }

  setCurrentItem(NULL);
  foreach(int row, rowList)
  {
    removeRow(row);
  }

  mCtx->setDocumentModified(true);
}

void PortTableWidget::populateLoop(CarbonDB* carbonDB,
                                   CarbonCfgRTLPortType type,
                                   CarbonDBNodeIter* iter)
{
  const CarbonDBNode* node;

  while ((node = carbonDBNodeIterNext(iter)) != NULL)
  {
    int width = carbonDBGetWidth(carbonDB, node);
    const char* name = carbonDBNodeGetFullName(carbonDB, node);
    CarbonCfgRTLPortID port = carbonCfgAddRTLPort(mCfg, name, width, type);

    if (carbonDBIsTied(carbonDB, node))
    {
      carbonCfgTie(port, NULL, 0);
      // It's tied off, so add it to the tie off list
      CarbonCfgTie* tie = getTie(port);
      // Add it to the tie table
      m_pTieOffTable->AddTie(port, tie, true);   
    }
    else
    {
      if (port != NULL) 
      {
        (void) addConnection(port);
      }
    }
  }
  carbonDBFreeNodeIter(iter);
}

// Add the RTL port to the Datamodel and GUI
void PortTableWidget::CreateESLPort(CarbonCfgRTLPortID rtlPort)
{
  addConnection(rtlPort);
  addESLPort(rtlPort);
  mCtx->setDocumentModified(true);
}


CarbonCfgESLPortID PortTableWidget::addConnection(CarbonCfgRTLPortID rtlPort) {
  CarbonCfgESLPortType eslType = CarbonCfg::convertRTLPortType(rtlPort);
  return addESLConnection(rtlPort, eslType);
}


CarbonCfgESLPortID PortTableWidget::addESLConnection(CarbonCfgRTLPortID rtlPort,
                                                     CarbonCfgESLPortType type)
{
  // Get a leaf name for the RTL port, use that as a starting point for
  // the ESL name
  const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
  UtString buf;
  if (node == NULL) {
    mCfg->getUniqueESLName(&buf, rtlPort->getName());
  }
  else {
    mCfg->getUniqueESLName(&buf, carbonDBNodeGetLeafName(mDB, node));
  }

  // Remove any exclusive connections (tie, reset, or clock)
  for (SInt32 i = rtlPort->numConnections() - 1; i >= 0; --i) {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    if (conn->isExclusive()) {
      removeConnection(conn, false, i);
    }
  }

  CarbonCfgESLPortID eslPort = carbonCfgAddESLPort(mCfg, rtlPort,  buf.c_str(), type);

  computeMode(mDB, node, eslPort);

  INFO_ASSERT(eslPort, "Failed to create ESL port");
  return eslPort;
}

void PortTableWidget::computeMode(CarbonDB* db, const CarbonDBNode* node, CarbonCfgESLPort* eslPort)
{
  if (carbonDBIsAsyncPosReset(db, node) || carbonDBIsAsyncNegReset(db, node))
  {
    eslPort->putMode(eCarbonCfgESLPortReset);
  }
  else if (carbonDBIsClk(db, node) || carbonDBIsClkTree(db, node))
  {
    eslPort->putMode(eCarbonCfgESLPortClock);
  }
  else
  {
    eslPort->putMode(eCarbonCfgESLPortControl);
  }
}

void PortTableWidget::Populate(ApplicationContext* ctx, bool initPorts)
{
  mCtx = ctx;
  mDB = ctx->getDB();
  mCfg = ctx->getCfg();

  // Ensure that the Tieoff widget has the context
  m_pTieOffTable->Populate(mCtx, initPorts);

  if (initPorts)
  {
    populateLoop(mDB, eCarbonCfgRTLInput,
      carbonDBLoopPrimaryInputs(mDB));
    populateLoop(mDB, eCarbonCfgRTLInout,
      carbonDBLoopPrimaryBidis(mDB));
    populateLoop(mDB, eCarbonCfgRTLOutput,
      carbonDBLoopPrimaryOutputs(mDB));

    // Not really sure if we should show these, but I think so.
    populateLoop(mDB, eCarbonCfgRTLInput,
      carbonDBLoopScDepositable(mDB));
    populateLoop(mDB, eCarbonCfgRTLOutput,
      carbonDBLoopScObservable(mDB));
  }
}
void PortTableWidget::removeConnection(CarbonCfgRTLConnection* /*conn*/, bool /*redraw*/, int /*idx*/)
{
} 

// If the port is input,output,or bi, it's a real port
// otherwise, it's an scObserv or scDeposit

bool PortTableWidget::isRealRTLPort(CarbonCfgRTLPort* rtlPort)
{
  const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());

  if (node != NULL)
    return (carbonDBIsPrimaryInput(mDB, node) || 
    carbonDBIsPrimaryOutput(mDB, node) || 
    carbonDBIsPrimaryBidi(mDB, node));
  else
    return false;
}

CarbonCfgTie* PortTableWidget::getTie(CarbonCfgRTLPort* port)
{
  UInt32 nConnections = port->numConnections();
  if (nConnections == 1)
    return port->getConnection(0)->castTie();

  return NULL;
}

// Get the ESL Port 
const char* PortTableWidget::getSystemCType(CarbonCfgRTLPort* port)
{
  for (UInt32 i=0; i<port->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = port->getConnection(i);
    // Skip the Xtor Connections
    if (conn->castXtorConn() != NULL)
      continue;

    CarbonCfgESLPort* eslPort = conn->castESLPort();
    if (eslPort != NULL)
      return eslPort->getTypeDef();
  }
  return NULL;
}


// Populate the Grid(s) with the ports now
void PortTableWidget::Update(ApplicationContext* ctx, TieOffTableWidget* tieOffTable, DisconnectTableWidget* disconnectTable)
{
  if (ctx)
    mCfg = ctx->getCfg();

  m_pTieOffTable = tieOffTable;
  m_pDisconnectTable = disconnectTable;

  m_pTieOffTable->SetPortWidget(this);
  m_pDisconnectTable->SetPortWidget(this);

  // nothing more to do here for now.
  if (mCfg == NULL || mDB == NULL)
    return;

  setRowCount(0);
  setUpdatesEnabled(false);
  setSortingEnabled(false);

  m_pTieOffTable->BeginUpdate();

  for (unsigned int i = 0, n = mCfg->numRTLPorts(); i < n; ++i)
  {
    CarbonCfgRTLPort* port = mCfg->getRTLPort(i);

    QTableWidgetItem *itemName = new QTableWidgetItem;
    itemName->setText(port->getName());

    int nConnections = port->numConnections();
    if (nConnections == 0) // Disconnect
    {
      m_pDisconnectTable->AddDisconnect(port);
      continue;
    }

 /*   UtString errMsg;
    errMsg << "port " << port->getName() << " should have only 1 connection, found: " << nConnections << " instead";
    INFO_ASSERT(nConnections <= 2, errMsg.c_str());
 */

    for (int i=0; i<nConnections; i++)
    {
      // Get the first connection
      CarbonCfgRTLConnection* conn = port->getConnection(i);

      // It *MUST* be one of these
      CarbonCfgESLPort* eslPort = conn->castESLPort();
      CarbonCfgTie* tiePort = conn->castTie();
      CarbonCfgClockGen* clkPort = conn->castClockGen();
      CarbonCfgResetGen* resetPort = conn->castResetGen();

      // Add the ESL Port
      if (eslPort != NULL)
        addESLPort(port);

      // It's a TIE off
      if (tiePort != NULL)
      {
         const CarbonDBNode* tieNode = carbonDBFindNode(mDB, port->getName());
         m_pTieOffTable->AddTie(port, tiePort, carbonDBIsTied(mDB, tieNode));
      }

      if (clkPort != NULL)
      {
        qDebug() << "CoWare does not support ClkGen ports: " << port->getName() << " ignored";
      }

      if (resetPort != NULL)
      {
        qDebug() << "CoWare does not support Reset ports: " << port->getName() << " ignored";
      }
    }
  }

//  horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
//  horizontalHeader()->setResizeMode(columnCount()-1, QHeaderView::Stretch);

  setUpdatesEnabled(true);
  setSortingEnabled(true);

  m_pTieOffTable->EndUpdate();
}


void PortTableWidget::addESLPort(CarbonCfgRTLPort* port)
{
  bool updates = updatesEnabled();
  bool sorting = isSortingEnabled();

  setUpdatesEnabled(false);
  setSortingEnabled(false);

  QTableWidgetItem *itemName = new QTableWidgetItem;
  itemName->setText(port->getName());


  UtString buf;
  buf << port->getWidth();

  int row = rowCount();

  QTableWidgetItem *itemWidth = new QTableWidgetItem;
  itemWidth->setText(buf.c_str());

  QTableWidgetItem *itemDirection = new QTableWidgetItem;
  switch (port->getType())
  {
  case eCarbonCfgRTLInput:  itemDirection->setText("input");  break;
  case eCarbonCfgRTLOutput: itemDirection->setText("output"); break;
  case eCarbonCfgRTLInout:  itemDirection->setText("inout");  break;
  }

  CarbonCfgRTLConnection* conn = port->getConnection(0);
  CarbonCfgESLPort* eslPort = conn->castESLPort();

  CarbonCfgESLPortMode mode = eslPort->getMode();
  if (mode == eCarbonCfgESLPortUndefined)
  {
    const CarbonDBNode* node = carbonDBFindNode(mDB, port->getName());
    computeMode(mDB, node, eslPort);
  }

  QTableWidgetItem *itemType = new QTableWidgetItem;
  itemType->setText(getSystemCType(port));

  setRowCount(row+1);

  QTableWidgetItem *itemESLPort = new QTableWidgetItem;
  itemESLPort->setText(eslPort->getName());
  QVariant qv = qVariantFromValue((void*)port);
  itemESLPort->setData(Qt::UserRole, qv);

  QTableWidgetItem *itemMode = new QTableWidgetItem;
  switch (eslPort->getMode())
  { 
    case eCarbonCfgESLPortUndefined: itemMode->setText("undefined"); break;
    case eCarbonCfgESLPortControl: itemMode->setText("control"); break; 
    case eCarbonCfgESLPortClock: itemMode->setText("clock"); break; 
    case eCarbonCfgESLPortReset: itemMode->setText("reset"); break; 
  }
  
  // Save the ESL port here
  QVariant qv1 = qVariantFromValue((void*)eslPort);
  itemMode->setData(Qt::UserRole, qv1);


  setItem(row, colESLPORT, itemESLPort);
  setItem(row, colWIDTH, itemWidth);
  setItem(row, colDIRECTION, itemDirection);
  setItem(row, colTYPE, itemType);
  setItem(row, colNAME, itemName);
  setItem(row, colRTLMODE, itemMode);

  setUpdatesEnabled(updates);
  setSortingEnabled(sorting);
}

PortTableEditDelegate::PortTableEditDelegate(QObject *parent, PortTableWidget* table)
: QItemDelegate(parent), portTable(table)
{
}

QWidget *PortTableEditDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                             const QModelIndex &index) const
{
  qDebug() << "Create Editor";

  switch (index.column())
  {
  case PortTableWidget::colESLPORT:
    break;
  case PortTableWidget::colTYPE:
    {
      QTableWidgetItem* item = portTable->item(index.row(), PortTableWidget::colESLPORT);
      QVariant qv = item->data(Qt::UserRole);
      CarbonCfgRTLPortID rtlPort = (CarbonCfgRTLPortID)qv.value<void*>();
      const char* currType = portTable->getSystemCType(rtlPort);

      QComboBox *cbox = new QComboBox(parent);
      cbox->addItem("bool");
      cbox->addItem("sc_uint");
      cbox->addItem("sc_biguint");
      cbox->addItem("sc_logic");
      cbox->addItem("sc_lv");
      cbox->addItem("sc_bv");
      cbox->addItem("unsigned int");
      cbox->addItem("unsigned char");
      
      int currIndex = cbox->findText(currType);
      cbox->setCurrentIndex(currIndex);

      connect(cbox, SIGNAL(currentIndexChanged(int)), this, SLOT(commitAndCloseComboBox(int)));

      return cbox;
    }
    break;
  case PortTableWidget::colRTLMODE:
    {
      QTableWidgetItem* item = portTable->item(index.row(), PortTableWidget::colRTLMODE);
//      QVariant qv = item->data(Qt::UserRole);
//      CarbonCfgESLPort* eslPort = (CarbonCfgESLPort*)qv.value<void*>();

      UtString currType;
      currType << item->text();

      QComboBox *cbox = new QComboBox(parent);
      cbox->addItem("undefined");
      cbox->addItem("control");
      cbox->addItem("clock");
      cbox->addItem("reset");

      int currIndex = cbox->findText(currType.c_str());
      cbox->setCurrentIndex(currIndex);

      connect(cbox, SIGNAL(currentIndexChanged(int)), this, SLOT(commitAndCloseComboBox(int)));
      return cbox;
    }
  case PortTableWidget::colWIDTH:
  case PortTableWidget::colNAME:
  case PortTableWidget::colDIRECTION:
  default:
    return NULL; // Read Only
    break;
  }

  QLineEdit *editor = new QLineEdit(parent);
  connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));

  return editor;
}

void PortTableEditDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  emit commitData(editor);
  emit closeEditor(editor);
}

void PortTableEditDelegate::commitAndCloseComboBox(int i)
{
  if (i != -1)
  {
    QComboBox *editor = qobject_cast<QComboBox *>(sender());
    emit commitData(editor);
    emit closeEditor(editor);
  }
}


void PortTableEditDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit) {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
  else {
    QDateTimeEdit *dateEditor = qobject_cast<QDateTimeEdit *>(editor);
    if (dateEditor) {
      dateEditor->setDate(QDate::fromString(index.model()->data(index, Qt::EditRole).toString(), "d/M/yyyy"));
    }
  }
}

void PortTableEditDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                         const QModelIndex &index) const
{
  QTableWidgetItem* item = portTable->item(index.row(), PortTableWidget::colESLPORT);
  QVariant qv = item->data(Qt::UserRole);
  CarbonCfgRTLPortID rtlPort = (CarbonCfgRTLPortID)qv.value<void*>();

  if (rtlPort->numConnections() == 0) // we've already deleted it
    return;

  CarbonCfgRTLConnection* conn = rtlPort->getConnection(0);
  CarbonCfgESLPort* eslPort = conn->castESLPort();
  UtString editText;
  if (eslPort == NULL)
    return;


  switch (index.column())
  {
  case PortTableWidget::colESLPORT:
    {
      QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
      if (edit)
      {
        const char* eslPortName = eslPort->getName();
        editText << edit->text();
        model->setData(index, edit->text());
        portTable->mCfg->changeESLName(eslPort, editText.c_str());
        if (0 != strcmp(editText.c_str(), eslPortName))
          portTable->mCtx->setDocumentModified(true);
      }
    }
    break;
  case PortTableWidget::colTYPE:
    {
      QComboBox* cbox = qobject_cast<QComboBox *>(editor);
      if (cbox != NULL)
      {
        model->setData(index, cbox->currentText());
        editText << cbox->currentText();
        eslPort->putTypeDef(editText.c_str());
        portTable->mCtx->setDocumentModified(true);
      }
    }
    break;
  case PortTableWidget::colRTLMODE:
    {
      QComboBox* cbox = qobject_cast<QComboBox *>(editor);
      if (cbox != NULL)
      {
        model->setData(index, cbox->currentText());
        editText << cbox->currentText();
      
        if (editText == "control")
          eslPort->putMode(eCarbonCfgESLPortControl);
        else if (editText == "clock")
          eslPort->putMode(eCarbonCfgESLPortClock);
        else if (editText == "reset")
          eslPort->putMode(eCarbonCfgESLPortReset);
        else if (editText == "undefined")
          eslPort->putMode(eCarbonCfgESLPortUndefined);

        portTable->mCtx->setDocumentModified(true);
      }
    }
    break;

  case PortTableWidget::colWIDTH:
  case PortTableWidget::colNAME:
  case PortTableWidget::colDIRECTION:
  default:
    break;
  }
}
