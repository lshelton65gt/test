/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CTableWidget.h"

CTableWidget::CTableWidget(QWidget *parent)
  : QTableWidget(parent)
{
}

void CTableWidget::putRecordData(const UtString &data)
{
  QTableWidgetItem *i = currentItem();
  if (!i) {
    return;
  }
  setModelData(data, model(), indexFromItem(i));
}

void CTableWidget::setModelData(const UtString &, QAbstractItemModel *,
                                const QModelIndex &index)
{
  // *** Always call base class at the end of
  // *** your reimplemented function!

  emit cellEdited(itemFromIndex(index));
}
