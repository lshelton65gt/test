// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef REGTABWIDGET_H
#define REGTABWIDGET_H

#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "util/c_memmanager.h"
#include <QObject>
#include <QWidget>

namespace Ui {
class RegTab;
}
class QMainWindow;
class MaxSimApplicationContext;
class CQtContext;
class CarbonProjectWidget;

class RegTabWidget : public QWidget
{
  Q_OBJECT

public:
  enum WizardMode {Standalone, ModelMaker };

  CARBONMEM_OVERRIDES

  RegTabWidget(QMainWindow *main_win,
               CQtContext *cqt,
               CarbonCfgID cfg, WizardMode mode=Standalone, CarbonProjectWidget* pw=NULL);
  ~RegTabWidget();

  void populate(CarbonDB *db);
  void clear();
  void deleteRegister();
  void populateBaseAddressComboBox();
  MaxSimApplicationContext* getContext() { return mCtx; }

private:
  Ui::RegTab *mUI;
  CarbonDBNode* mSelectedNode;
  MaxSimApplicationContext *mCtx;
  CarbonCfgRegister* mReg;
  QMainWindow *mMainWin;
  CQtContext *mCQt;
  CarbonCfgID mCfg;
  CarbonDB *mDB;
  WizardMode mMode;

private slots:
  void on_btnAdd_clicked();
  void on_btnDelete_clicked();
  void documentWasModified();
  void selectionChanged(CarbonDBNode* node);
  void fieldSelectionChanged(CarbonCfgRegisterField* field);
  void registerSelectionChanged(CarbonCfgRegister* reg);
  void on_btnDeleteField_clicked();
  void on_btnBindConstant_clicked();
  void on_btnAddField_clicked();
  void on_cbRegBaseAddr_currentIndexChanged(const QString & text);
  void on_cbRegBaseAddr_editTextChanged(const QString & text);
};


#endif
