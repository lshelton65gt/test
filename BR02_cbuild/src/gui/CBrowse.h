// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CBROWSE_H_
#define _CBROWSE_H_

#include "gui/CQt.h"
#include <QHBoxLayout>
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "carbon/carbon_dbapi.h"
#include "util/SourceLocator.h"
#include "util/UtWildcard.h"
#include <QMainWindow>
#include <QProcess>
#include <QGraphicsItem>
#include "iodb/CGraph.h"
#include "util/UtSet.h"
#include "CBrowseNode.h"

class TextEdit;
class CBrowseButton;
class CBrowseView;
class CarbonHBrowser;
class QMessageBox;

class CBrowse : public QMainWindow {
  Q_OBJECT
public:
  typedef UtHashSet<const CarbonDBNode*> DBNodeSet;

  //! ctor
  CBrowse(CQtContext* cqt, const char* name,
          CarbonDB*, const CarbonDBNode*,
          CGraph* cgraph,
          CarbonHBrowser* hbrowser,
          const char* unique_prefix);

  //! dtor
  ~CBrowse();

  CarbonDB* getDB() const {return mDB;}
  QFontMetrics* getFontMetrics() const {return mFontMetrics;}
  QFont* getFont() const {return mFont;}

  void drawText(QPainter* painter,
                const char* str, double x, double y,
                double* width, double* height);
  void layout();
  void redraw(const QRectF& area);
  bool isVisible(CGraph::Node*) const;
  CBrowseNode* layoutNode(CGraph::Node*);
  void addEdge(CBrowseNode*, CBrowseButton*, CBrowseNode*, CBrowseButton*);
  void popupSource(const SourceLocator& loc);
  void stabilize(CBrowseNode* cnode);

  void enterNode(CBrowseNode*);
  void leaveNode(CBrowseNode*);
  void dropText(const char* text);

  void createToolBars();

  //! node positions & button-states for every visibile button
  class State {
  public:
    CARBONMEM_OVERRIDES

    ~State() {clear();}
    void clear();

    DBNodeSet mFocusNodes;
    UtArray<CBrowseNode::State*> mNodeStates;
    QRectF mBoundingBox;
    UtArray<QLineF*> mLines;
    UtArray<QPainterPath*> mSplines;
    int mXScroll;
    int mYScroll;
  };

  void saveState();
  void restoreState(const State&);
  void enableButtons();

  //! return a reasonable initial size for the connectivity browser
  QSize sizeHint() const;


public slots:
  void dropNet(const char* path, QTreeWidgetItem*);
  void clearNodes();
  void nextView();
  void prevView();
  void textEditClosed(QDockWidget*);
  void dotFinished(int code, QProcess::ExitStatus exit_status);
  void dotOutput();
  void dotErrors();
  void dotError(QProcess::ProcessError err);

  CGraph* getCGraph() const {return mCGraph;}

private:
  void setupDotProcess();
  QWidget* buildLayout();
  void clear();
  void setFocus(const CarbonDBNode* node);
  void removeDeadNodes();
  void clearLayoutStructures();
  void clearEdgeObjects();
  bool writeDotFile(const char* dot_filename);
  bool runDot(const char* dot_filename);
  typedef UtHashMap<QGraphicsItem*,QPointF> ItemPosMap;
  bool readDotFile(ItemPosMap* item_pos_map, bool *is_bb_set,
                   QRectF* bb);
  void doStabilization(const ItemPosMap& item_pos_map,
                       bool is_bb_set, QRectF* bb);
  void placeEdges();

#define USE_GRAPHICS_SCENE 1
#if USE_GRAPHICS_SCENE
  CBrowseView* mGraphicsView;
  QGraphicsScene* mGraphicsScene;
#else
  QWidget* mCanvas;
#endif

  CarbonDB* mDB;

  const CarbonDBNode* mNode;
  CQtContext* mCQt;

  QFont* mFont;
  QFontMetrics* mFontMetrics;
  DBNodeSet mFocusNodes;
  UtString mEdgeStrs;
  UtString mNodeStrs;

  typedef UtHashMap<CGraph::Node*,CBrowseNode*> NodeMap;
  NodeMap mNodeMap;
  typedef UtHashSet<CBrowseNode*> NodeSet;
  NodeSet mLiveNodes;
  NodeSet mNodesInScene;
  typedef UtArray<CBrowseNode*> NodeArray;
  NodeArray mNodeArray;
  typedef UtArray<QGraphicsItem*> ItemArray;
  ItemArray mEdgeItems;
  CGraph* mCGraph;

  typedef std::pair<CBrowseNode*,CBrowseNode*> NodePair;

  struct Edge {
    size_t hash() const {
      return (size_t) b1 + (size_t) b2;
    }
    bool operator==(const Edge& other) const {
      return (((b1 == other.b1) && (b2 == other.b2)) ||
              ((b1 == other.b2) && (b2 == other.b1)));
    }
    CBrowseNode* n1;
    CBrowseButton* b1;
    CBrowseNode* n2;
    CBrowseButton* b2;
  };
  typedef UtHashSet<Edge> EdgeSet;
  EdgeSet mEdges;    
  CBrowseNode* mStabilize;      // node to be stabilized
  QPointF mStabilizePos;
  QRectF mBoundingBox;
  SInt32 mNodeEnterLeaveDepth;
  CarbonHBrowser* mHBrowser;

  QToolBar* mMainToolBar;
  QAction* mNextAct;
  QAction* mPrevAct;
  QAction* mClearAct;

  UtString mUniqueSuffix;

  UtArray<State*> mNavigationStack;
  UInt32 mNavigationStackIndex;

  typedef UtHashMap<const char*,TextEdit*> FileEditorMap;
  FileEditorMap mFileEditorMap;

  typedef UtHashMap<TextEdit*,const char*> FileEditorReverseMap;
  FileEditorReverseMap mFileEditorReverseMap;

  QProcess* mDotProcess;
  QMessageBox* mDotTimeoutMessageBox;
  bool mDotTimeoutMessageBoxActive;
  bool mDotProcessSucceeded;
  bool mDotStarted;
  UtString mDotOutput;
  UtString mDotErrors;
}; // class CBrowse : public QWidget

#endif
