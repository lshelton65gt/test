// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef __TieOffTableWidget_H__
#define __TieOffTableWidget_H__


#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"

#include "ApplicationContext.h"

#include <QObject>
#include <QTableWidget>
#include <QtGui>

#define UNKNOWN_TIE_VALUE "<Set by Compiler>"

class PortTableWidget;

class TieOffTableWidget : public QTableWidget
{
   Q_OBJECT

public:
  TieOffTableWidget(QWidget* parent);
  virtual ~TieOffTableWidget();

  enum Column {colHDLNAME = 0, colWIDTH, colTIED};

  void Populate(ApplicationContext* ctx, bool initPorts);
  QTableWidgetItem* AddTie(CarbonCfgRTLPort* rtlPort, CarbonCfgTie* tie, bool isDirectiveTied=false);
  void BeginUpdate();
  void EndUpdate();
  void SetPortWidget(PortTableWidget* portWidget) { m_pPortTable = portWidget; }
  void ClearSelection();

private:
  void initialize();
  void createActions();
  void removeTie(CarbonCfgTie* tie);
  CarbonCfgTie* getTie(CarbonCfgRTLPort* port);

private:
  virtual void dragEnterEvent(QDragEnterEvent* ev);
  virtual void dropEvent(QDropEvent* ev);
  virtual void dragMoveEvent(QDragMoveEvent *ev);
  virtual QMimeData* mimeData(const QList<QTableWidgetItem*>) const;

private slots:
  void showContextMenu(const QPoint &pos);
  void remove();

  friend class TieOffTableEditDelegate;
private:
  CarbonDB* mDB;
  CarbonCfgID mCfg;
  ApplicationContext* mCtx;

  PortTableWidget* m_pPortTable;
  QAction *removeAction;
};

class TieOffTableEditDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    TieOffTableEditDelegate(QObject *parent = 0, TieOffTableWidget* table=0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
        const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const;
private slots:
    void commitAndCloseEditor();

private:
  TieOffTableWidget* tieOffTable;
};
#endif
