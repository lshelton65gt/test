/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "ReplayGUI.h"
#include "gui/CDragDropTreeWidget.h"
#include "shell/ReplaySystem.h"
#include "shell/carbon_misc.h"
#include "util/AtomicCache.h"
#include "util/CarbonPlatform.h"
#include "util/OSWrapper.h"
#include "util/RandomValGen.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QtGui>
#include <QTimer>
#include <QPushButton>

#define APP_NAME "Carbon Control"

#ifndef Q_OS_WIN
#include <QtPlugin>
Q_IMPORT_PLUGIN(qgif)
#endif

ReplayGUI::ReplayGUI(CQtContext* cqt)
  : mCQt(cqt),
    mAssertOverride(APP_NAME, this, cqt),
    mPopulating(false),
    mCyclesSpecified(false),
    mSimRequestActive(false)
{
  mReplaySystem = new CarbonReplaySystem;
  mPollSimState = NULL;
  buildWidgets();

  mExitAct = new QAction(tr("E&xit"), this);
  mExitAct->setShortcut(tr("Ctrl+Q"));
  mExitAct->setStatusTip(tr("Exit the application"));
  CQT_CONNECT(mExitAct, triggered(), this, close());
  mCQt->registerAction(mExitAct, "exit");
  
  buildLayout();
  createStatusBar();
  
  readSettings();
  
  setWindowIcon(QIcon(":/images/carbon-logo.png"));

  setCurrentFile("");
  mPollTimer = new QTimer(this);

  CQT_CONNECT(mPollTimer, timeout(), this, pollSimFile());
  mPollTimer->setInterval(2000); // milliseconds
} // ReplayGUI::ReplayGUI

ReplayGUI::~ReplayGUI() {
  delete mReplaySystem;
  if (mPollSimState != NULL) {
    delete mPollSimState;
  }
}

// Load a file into the replay database.  If successful, repopulate all
// the widgets based on the new database.  If not successful, then leave
// the GUI as it was.
bool ReplayGUI::loadFile(const char* filename) {
  UInt32 fname_len = strlen(filename);
  if (fname_len < 5) {
    mCQt->warning(this, "loadFile called on filename with invalid extension");
    return false;
  }

  // Remove the extension provided, and then add the extensions we
  // are going to work with
  UtString base(filename, fname_len - 4), guiFile, simFile;
  simFile << base << CARBON_STUDIO_SIMULATION_EXT;
  guiFile << base << CARBON_STUDIO_USER_EXT;

  bool ret = false;
  CarbonReplaySystem* old_system = mReplaySystem;
  mReplaySystem = new CarbonReplaySystem(base.c_str());
  mPlot->putReplaySystem(mReplaySystem);
  ret = mReplaySystem->readSystem(false);
  
  // Now, read the GUI file to get the user's preferences, in case
  // they have not yet been reflected in the system.
  UtString result;
  if (ret) {
    // If there is a .csu file, read it.  It overrides the .css file
    if (OSStatFile(guiFile.c_str(), "r", &result) == 1) {
      if (mReplaySystem->readCmdline(false) == eCarbonSystemCmdError) {
        ret = false;            // warning issued below
      }
    }
    else {
      // Otherwise, we should write a .csu file reflecting the last state
      // of the simulation
      ret = saveFile();
    }
  }

  if (ret) {
    if (old_system != NULL) {
      old_system->clearCControlActiveFile();
      delete old_system;
    }
    mReplaySystem->writeCControlActiveFile();

    setupHeaderLabels();
    mPollSimState = new CarbonReplaySystem(base.c_str());
    mPlot->putReplaySystem(mPollSimState);
    populateWidgets();
    setCurrentFile(guiFile.c_str());
    mPollTimer->start();
    pollSimFile();              // look immediately
  }
  else {
    mCQt->warning(this, mReplaySystem->getErrmsg());
    delete mReplaySystem;
    mReplaySystem = old_system;
    mPlot->putReplaySystem(mReplaySystem);
  }

  return ret;
}

void ReplayGUI::buildWidgets() {

  mCQt->disableRecord();

  // These widgets correspond to the fields in CarbonReplaySystem.
  mDatabase = mCQt->filenameEdit("database", false, "Replay Database",
                                 "*.rdb");
  CQT_CONNECT(mDatabase, filenameChanged(const char*),
              this, changeDatabase(const char*));

  mCheckpointInterval = mCQt->spinBox("checkpoint_interval");
  mCheckpointInterval->setMinimum(0);
  mCheckpointInterval->setMaximum(2000000000);
  CQT_CONNECT(mCheckpointInterval, valueChanged(int),
              this, changeCheckpoint(int));
  mRecoverPercentage = mCQt->spinBox("recoverPercentage");
  mRecoverPercentage->setMinimum(0);
  mRecoverPercentage->setMaximum(100);
  CQT_CONNECT(mRecoverPercentage, valueChanged(int),
              this, changeRecoverPercentage(int));
  mComponents = mCQt->dragDropTreeWidget("components", false);
  mComponents->setColumnCount(eNumColumns);
  mComponents->setAlternatingRowColors(true);
  mComponents->setRootIsDecorated(false);
  mComponents->setSortingEnabled(true);
  CQT_CONNECT(mComponents, selectionChanged(), this, changeCompSel());

  mReplayAll = mCQt->radioButton("replay_all", "All Components");
  mReplayNone = mCQt->radioButton("replay_none", "No Components");
  mReplaySome = mCQt->radioButton("replay_some", "Select components");
  mReplayAll->setChecked(true);

  mVerbose = mCQt->checkBox("verbose_replay", "Verbose Replay");
  mVerbose->setEnabled(false);
  mRecord = mCQt->radioButton("record", "Record");
  mPlayback = mCQt->radioButton("playback", "Play");
  mStop = mCQt->radioButton("stop", "Stop");

  mContinueSim = mCQt->pushButton("cont", "Continue Sim");
  mContinueSim->setEnabled(false);
  QPalette palette;
  mSaveSimContForeground = palette.color(QPalette::Active,
                                         mContinueSim->foregroundRole());
  mSaveSimContBackground = palette.color(QPalette::Active,
                                         mContinueSim->backgroundRole());

  // Try to emulate the font used in the Y axis.  I hope this
  // looks good on all platforms, but it looks good on Linux and VNC.
  // But using a combo-box rather than the default label allows us
  // to directly change what time-units are on the X axis
  mXAxis = mCQt->comboBox("x_axis");
  QFont font("Helvetica", 12, QFont::Bold);
  mXAxis->setFont(font);
  mXAxis->setFrame(false);

  setupHeaderLabels();

  // Note that we figure everything out in repaint, so we use one slot
  // to sort out what to do for every widget...
  CQT_CONNECT(mPlayback, clicked(), this, widgetModified());
  CQT_CONNECT(mReplayAll, toggled(bool), this, widgetModified());
  CQT_CONNECT(mReplayNone, toggled(bool), this, widgetModified());
  CQT_CONNECT(mReplaySome, toggled(bool), this, widgetModified());
  CQT_CONNECT(mRecord, toggled(bool), this, widgetModified());
  CQT_CONNECT(mPlayback, toggled(bool), this, widgetModified());
  CQT_CONNECT(mStop, toggled(bool), this, widgetModified());
  CQT_CONNECT(mXAxis, currentIndexChanged(int),
              this, changeXAxis(int));
  CQT_CONNECT(mVerbose, stateChanged(int), this, widgetModified());
  CQT_CONNECT(mContinueSim, clicked(), this, continueSim());

  mPlot = new ReplayPerfPlot(mReplaySystem);
  mCQt->enableRecord();
}

void ReplayGUI::setupHeaderLabels() {
  QStringList headers;
  headers << "Name";
  headers << "Replay State";
  headers << "OnDemand State";
  headers << "Checkpoints";
  headers << "Sched Calls";
  headers << "Ena";
  headers << "Requested Mode";
  mComponents->setHeaderLabels(headers);

  mXAxis->clear();
  mXAxis->setFrame(false);
  mXAxis->addItem("CPU Seconds (User + Sys)");
  mXAxisValues.clear();
  mXAxisValues.push_back(ReplayPerfPlot::eCPUTime);
  mXAxis->addItem("Real Time (seconds)");
  mXAxisValues.push_back(ReplayPerfPlot::eRealTime);

  CarbonReplaySystem* rss = mPollSimState ? mPollSimState : mReplaySystem;
  if (rss->isCycleCountSpecified()) {
    mXAxis->addItem("Cycles");
    mXAxisValues.push_back(ReplayPerfPlot::eCycles);
  }
  if (rss->isSimTimeSpecified()) {
    UtString buf;
    buf << "Sim Time (" << rss->getSimTimeUnits() << ")";
    mXAxis->addItem(buf.c_str());
    mXAxisValues.push_back(ReplayPerfPlot::eSimTime);
  }
  mXAxis->addItem("Aggregate Schedule Calls");
  mXAxisValues.push_back(ReplayPerfPlot::eSchedCalls);
} // void ReplayGUI::setupHeaderLabels

void ReplayGUI::buildLayout() {
  QGridLayout* grid = new QGridLayout;
  CQt::addGridRow(grid, "Replay Database", mDatabase);
  CQt::addGridRow(grid, "Minimum Checkpoint Interval", mCheckpointInterval);
  CQt::addGridRow(grid, "Recover Percentage", mRecoverPercentage);

  QWidget* genProp = mCQt->makeGroup("Properties", "replay_prop", grid);

  QLabel* logoIdle = new QLabel();
  logoIdle->setObjectName(QString::fromUtf8("logo"));
  logoIdle->setGeometry(QRect(10, 0, 80, 82));
  logoIdle->setPixmap(QPixmap(QString::fromUtf8(":/carbon/images/carbon-logo.png")));

  QVBoxLayout* participation = CQt::vbox(mReplayAll, mReplayNone, mReplaySome);
  QVBoxLayout* control = CQt::vbox(mRecord, mPlayback, mStop);
  QLayout* scopeLayout = CQt::hbox();
  *scopeLayout << participation << control;
  QLayout* groupLayout = CQt::vbox();
  QLayout* verbose_continue_layout = CQt::hbox();
  *verbose_continue_layout << mVerbose << mContinueSim;
  *groupLayout << scopeLayout << verbose_continue_layout;
  QWidget* scopeGroup = mCQt->makeGroup("Replay Control", "replay_control",
                                        groupLayout);
  QWidget* compListGroup = mCQt->makeGroup("Carbon Components", "replay_comps",
                                           mComponents);
  //QVBoxLayout* selection = CQt::vbox();
  //*selection << compListGroup << mReplayEnable;
    

  QLayout* top = CQt::hbox();
  *top << logoIdle << genProp << scopeGroup;
  QVBoxLayout* layout = CQt::vbox();
  *layout << top << compListGroup;

  mXAxis->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  QVBoxLayout* plotLayout = CQt::vbox(mPlot, mXAxis);
  plotLayout->setAlignment(mXAxis, Qt::AlignHCenter);
  
  setCentralWidget(CQt::vSplit(CQt::makeWidget(layout),
                               CQt::makeWidget(plotLayout)));
} // void ReplayGUI::buildLayout

void ReplayGUI::changeXAxis(int i) {
  mPlot->putXAxis(mXAxisValues[i]);
}

void ReplayGUI::clearComponents() {
  // Be sure to unregister all the checkboxes in the lines of the tree
  for (UInt32 i = 0, n = mComponents->topLevelItemCount(); i < n; ++i) {
    QTreeWidgetItem* item = mComponents->topLevelItem(i);
    QCheckBox* checkBox = (QCheckBox*) mComponents->itemWidget(item, eEna);
    mCQt->unregisterWidget(checkBox);
  }
  mComponents->clear();
}

void ReplayGUI::populateWidgets() {
  mCQt->disableRecord();
  mPopulating = true;

  mDatabase->putFilename(mReplaySystem->getDatabaseDirectory());
  mCheckpointInterval->setValue(mReplaySystem->getCheckpointInterval());
  mRecoverPercentage->setValue(mReplaySystem->getRecoverPercentage());
  clearComponents();

  UInt32 i = 0;
  bool record_any = false;
  bool playback_any = false;

  for (CarbonReplaySystem::ComponentLoop p(mReplaySystem->loopComponents());
       !p.atEnd(); ++p, ++i)
  {
    CarbonSystemComponent* comp = p.getValue();
    QTreeWidgetItem* item = new QTreeWidgetItem(mComponents);
    const char* name = comp->getName();
    item->setText(eName, name);
    record_any |= mReplaySystem->isRecordRequested(name);
    playback_any |= mReplaySystem->isPlaybackRequested(name);
  }

  if (mReplaySystem->isRecordAll() || mReplaySystem->isPlaybackAll()) {
    mReplayAll->setChecked(true);
  }
  else if (record_any || playback_any) {
    mReplaySome->setChecked(true);
  }
  else {
    mReplayNone->setChecked(true);
  }

  if (record_any) {
    mRecord->setChecked(true);
  }
  else if (playback_any) {
    mPlayback->setChecked(true);
  }
  else {
    mStop->setChecked(true);
  }

  mComponents->resizeColumnToContents(eReplayState); // get 'state' to fit
  mComponents->resizeColumnToContents(eOnDemandState); // get 'state' to fit
  mComponents->resizeColumnToContents(eEna); // enable column should be small

  mRecoverPercentage->setValue(mReplaySystem->getRecoverPercentage());
  mCheckpointInterval->setValue(mReplaySystem->getCheckpointInterval());
  mDatabase->putFilename(mReplaySystem->getDatabaseDirectory());

  mVerbose->setCheckState(mReplaySystem->isVerbose() ?
                          Qt::Checked : Qt::Unchecked);

  mCQt->enableRecord();
  mPopulating = false;
} // void ReplayGUI::populateWidgets

void ReplayGUI::showWindow() {
  show();
}

void ReplayGUI::closeEvent(QCloseEvent *event) {
  mCQt->recordExit();
  if (maybeSave()) {
    writeSettings();
    mReplaySystem->clearCControlActiveFile();
    event->accept();
  } else {
    event->ignore();
  }
}

void ReplayGUI::newFile() {
  if (maybeSave()) {
    //mTextEdit->clear();
    setCurrentFile("");
  }
}

void ReplayGUI::open() {
  if (maybeSave()) {
    UtString filename;

    if (mCQt->isPlaybackActive()) {
      mCQt->getPlaybackFile(&filename);
    }
    else {
      QApplication::setOverrideCursor(Qt::WaitCursor);
      QString qfile =
        QFileDialog::getOpenFileName(this,
                                     "Choose a Replay Configuration",
                                     "", 
                                     "Carbon Replay System (*.css)");
      QApplication::restoreOverrideCursor();
      filename << qfile;
      if (mCQt->isRecordActive()) {
        mCQt->recordFilename(filename.c_str());
      }
    }
    (void) loadFile(filename.c_str());
  }
} // void ReplayGUI::open

bool ReplayGUI::save()
{
  if (mReplaySystem != NULL) {
    return saveFile();
  } else {
    return saveAs();
  }
}

bool ReplayGUI::saveAs() {
  UtString filename;

  bool ret = mCQt->popupFileDialog(this, false, // readFile
                                   "Save a Carbon Replay System",
                                   "Carbon Replay Configuration (*.css)",
                                   &filename);
  if (ret) {
    setCurrentFile(filename.c_str());
    // remove the extension
    filename.resize(filename.size() - 4);
    mReplaySystem->putSystemName(filename.c_str());
    ret = saveFile();
  }
  return ret;
}

void ReplayGUI::notifyModified() {
  // We do a save at the end of repaint() so it is not needed here
}

void ReplayGUI::createStatusBar()
{
  statusBar()->showMessage(tr("Ready"));
}

void ReplayGUI::readSettings()
{
  QSettings settings("Carbon", "Control");
  QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
  QSize size = settings.value("size", QSize(400, 400)).toSize();
  resize(size);
  move(pos);
}

void ReplayGUI::writeSettings()
{
  QSettings settings("Carbon", "Control");
  settings.setValue("pos", pos());
  settings.setValue("size", size());
}

bool ReplayGUI::maybeSave()
{
  bool status = true;
  if (isWindowModified()) {
    int ret = QMessageBox::warning(this, tr(APP_NAME),
                                   tr("The configuration has been modified.\n"
                                      "Do you want to save your changes?"),
                                   QMessageBox::Yes | QMessageBox::Default,
                                   QMessageBox::No,
                                   QMessageBox::Cancel | QMessageBox::Escape);
    if (ret == QMessageBox::Yes) {
      status = save();
    }
    else if (ret == QMessageBox::Cancel) {
      status = false;
    }
  }
  return status;
}

bool ReplayGUI::saveFile()
{
  bool ret = false;

  if (! mReplaySystem->writeCmdline()) {
    mCQt->warning(this, tr("Cannot write command-file file:\n%1.")
                  .arg(mReplaySystem->getErrmsg()));
  }
  else {
    setWindowModified(false);
    statusBar()->showMessage(tr("File saved"), 2000);
    ret = true;
  }
  
  return ret;
} // bool ReplayGUI::saveFile

void ReplayGUI::setCurrentFile(const char* filename) {
  setWindowModified(false);
  
  QString shownName;
  shownName = strippedName(QString(filename));
  
  setWindowTitle(tr("%1[*] - %2").arg(shownName).arg(tr(APP_NAME)));
}

QString ReplayGUI::strippedName(const QString &fullFileName)
{
  return QFileInfo(fullFileName).fileName();
}

static bool sCompareExt(const char* base, const char* ext) {
  size_t base_len = strlen(base);
  size_t ext_len = strlen(ext);
  if (base_len <= ext_len) {
    return false;
  }
  return strcmp(base + base_len - ext_len, ext) == 0;
}

int main(int argc, char **argv)
{
  int ret = 0;

  // explicit test for bug 7586:
  if ((argc == 2) && (strcmp(argv[1], "-testCpuArray") == 0)) {
    return ReplayPerfPlot::testCpuArray();
  }

  // At the moment there some carbon-controlled memory leaks which
  // need to be fixed.  So we can't use this.  In addition, Qt is
  // not written in a manner which works well with our memory-leak
  // detector.  For example, it uses static constructors.  So we
  // would need to do some work to use this with Qt and our
  // global operator new override.  But I like our global opertor new
  // override for debugging purposes (deadbeef, CarbonMem::watch), so
  // I'm leaving it on for now (link in libcarbonmem.a in Makefile.am)
  // and turning off memory-leak detection.
  //
  // To do:
  //   1. do not link with libcarbonmem, but just rely on the explicit
  //      operator new overrides in our class definitions
  //      (CARBONMEM_OVERRIDES).
  //   2. Fix the Carbon memory leaks.
  //   3. Link in libcarbonmem only in debug mode.
  //   4. Figure out a way to use the global operator new overrride with
  //      Qt (e.g. account for Qt's static ctors and anything else that
  //      comes up).
  // 
  // CarbonMem memContext(&argc, argv, getenv("CARBON_WIZARD_DUMPFILE"));

  {
    Q_INIT_RESOURCE(application);

    // Allow the user to specify the db filename from the command-line to avoid
    // the heinous slow Qt open-file dialog
    UtString filename;

    for (int i = 1; i < argc; ++i) {
      if (sCompareExt(argv[i], CARBON_STUDIO_USER_EXT) ||
          sCompareExt(argv[i], CARBON_STUDIO_SIMULATION_EXT))
      {
        filename = argv[i];
        memmove(&argv[i], &argv[i + 1], ((argc - i) * (sizeof(char*))));
        --argc;
        --i;
      }
    }

    CQtContext app(argc, argv);
    app.init(argc, argv);
    ReplayGUI mainWin(&app);
    app.putMainWindow(&mainWin);

    // If the user didn't specified a .replay config file, prompt for one
    if (filename.empty()) {
      QString qfileName =
        QFileDialog::getOpenFileName(&mainWin,
                                     "Choose a Carbon Control Database",
                                     "", 
                                     "Carbon Control Database (*.css *.csu)");
      filename << qfileName;
      if (filename.empty()) {
        return 1;               // user canceled
      }
    }

    if (!mainWin.loadFile(filename.c_str())) {
      return 1;
    }
    mainWin.adjustSize();       // account for min-size tweaks
    mainWin.showWindow();
    ret = app.exec();
  }
  return ret;
} // int main

// slots

void ReplayGUI::changeDatabase(const char* filename) {
  if (mReplaySystem != NULL) {
    if (strcmp(filename, mReplaySystem->getDatabaseDirectory()) != 0) {
      mReplaySystem->putDatabaseDirectory(filename);
    }
  }
}  

void ReplayGUI::changeCheckpoint(int interval) {
  if (mReplaySystem != NULL) {
    UInt32 ival = interval;
    if (ival != mReplaySystem->getCheckpointInterval()) {
      mReplaySystem->putCheckpointInterval(mReplaySystem->getRecoverPercentage(), ival);
    }
  }
}

void ReplayGUI::changeRecoverPercentage(int recoverPercentage) {
  if (mReplaySystem != NULL) {
    UInt32 iRecoverPercentage = recoverPercentage;
    if (iRecoverPercentage != mReplaySystem->getRecoverPercentage()) {
      mReplaySystem->putCheckpointInterval(iRecoverPercentage,
                                           mReplaySystem->getCheckpointInterval());
    }
  }
}

void ReplayGUI::changeCompSel() {
/*
  QTreeItemList sel = mComponents->selectedItems();
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;
    QString qname = item->text(0);
    UtString name = qname;
    bool checked = (mSelectedComponents.find(name) != mSelectedComponents.end());
    //mReplayEnable->setCheckState(checked ? Qt::Checked : Qt::Unchecked);
    //QWidget* cb = mComponents->itemWidget(item, 4);
    cb->setCheckState(checked ? Qt::Checked : Qt::Unchecked);
  }
*/
}

void ReplayGUI::widgetModified() {
  if (mPopulating) {
    return;
  }
  repaint();
  (void) save();
}

void ReplayGUI::paintSimContButton() {
  bool ena = mSimRequestActive &&
    (mReplayAll->isChecked() || mReplaySome->isChecked());

  QPalette palette;
  if (ena && mRecord->isChecked()) {
    palette.setColor(mContinueSim->backgroundRole(), QColor(Qt::red));
    palette.setColor(mContinueSim->foregroundRole(), QColor(Qt::black));
  }
  else if (ena && mPlayback->isChecked()) {
    palette.setColor(mContinueSim->backgroundRole(), QColor(Qt::green));
    palette.setColor(mContinueSim->foregroundRole(), QColor(Qt::black));
  }
  else if (mSimRequestActive) {
    palette.setColor(mContinueSim->backgroundRole(), QColor(Qt::cyan));
    palette.setColor(mContinueSim->foregroundRole(), QColor(Qt::black));
  }
  else {
    palette.setColor(mContinueSim->backgroundRole(), mSaveSimContBackground);
    palette.setColor(mContinueSim->foregroundRole(), mSaveSimContForeground);
  }
  mContinueSim->setPalette(palette);
} // void ReplayGUI::paintSimContButton

void ReplayGUI::repaint() {
  mReplaySystem->clearRequests();
  bool resize_ena_col = false;
  for (UInt32 i = 0, n = mComponents->topLevelItemCount(); i < n; ++i) {
    QTreeWidgetItem* item = mComponents->topLevelItem(i);
    QCheckBox* checkBox = (QCheckBox*) mComponents->itemWidget(item, eEna);
    UtString buf("enable_replay_");
    buf << i;
    if (mReplaySome->isChecked()) {
      if (checkBox == NULL) {
        checkBox = mCQt->checkBox(buf.c_str(), "");
        mComponents->setItemWidget(item, eEna, checkBox);
        resize_ena_col = true;
        CQT_CONNECT(checkBox, toggled(bool), this, widgetModified());
      }
    }
    else if (checkBox != NULL) {
      mCQt->unregisterWidget(checkBox);
      mComponents->setItemWidget(item, eEna, NULL);
      resize_ena_col = true;
    }
  }
  if (resize_ena_col) {
    mComponents->resizeColumnToContents(eEna); // enable column should be small
  }

  bool ena_control = false;

  if (mReplayAll->isChecked()) {
    ena_control = true;
    if (mRecord->isChecked()) {
      mReplaySystem->recordAll();
    }
    else if (mPlayback->isChecked()) {
      mReplaySystem->playbackAll();
    }
  }

  mReplaySystem->putVerbose(mVerbose->checkState() == Qt::Checked);

  for (UInt32 i = 0, n = mComponents->topLevelItemCount(); i < n; ++i) {
    QTreeWidgetItem* item = mComponents->topLevelItem(i);
    UtString name;
    name << item->text(0);
    
    CarbonSystemComponent* comp = mReplaySystem->findComponent(name.c_str());
    if (comp == NULL) {
      item->setText(eReplayState, "(removed)");
      item->setText(eOnDemandState, "(removed)");
      item->setText(eCheckpoints, "");
      item->setText(eSchedCalls, "");
      item->setText(eRequest, "");
    }
    else {
      QTreeWidgetItem* item = mComponents->topLevelItem(i); // not right
      QCheckBox* checkBox = (QCheckBox*) mComponents->itemWidget(item, eEna);

      const char* name = comp->getName();
      if (mReplaySome->isChecked()) {
        if (checkBox->checkState() == Qt::Checked) {
          ena_control = true;
          if (mRecord->isChecked()) {
            mReplaySystem->recordComponent(name);
          }
          else if (mPlayback->isChecked()) {
            mReplaySystem->playbackComponent(name);
          }
        }
      }

      item->setText(eReplayState, comp->getReplayState().getString());
      item->setText(eOnDemandState, comp->getOnDemandState().getString());
      UtString buf;
      buf << comp->getNumCheckpoints();
      item->setText(eCheckpoints, buf.c_str());
      buf.clear();
      buf << comp->getNumScheduleCalls();
      item->setText(eSchedCalls, buf.c_str());

      if (mReplaySystem->isRecordRequested(name)) {
        item->setText(eRequest, "record");
      }
      else if (mReplaySystem->isPlaybackRequested(name)) {
        item->setText(eRequest, "playback");
      }
      else {
        item->setText(eRequest, "");
      }
    } // if
  } // for

  mRecord->setEnabled(ena_control);
  mPlayback->setEnabled(ena_control);
  mStop->setEnabled(ena_control);
  mVerbose->setEnabled(ena_control);
  if (mSimRequestActive) {
    paintSimContButton();
  }
} // void ReplayGUI::repaint

// See if the .css file has changed.  If so, re-read it
void ReplayGUI::pollSimFile() {
  if ((mPollSimState != NULL) && mPollSimState->readSystem(true)) {
    UtString buf, time;
    OSTimeToString(mPollSimState->getSimReadTime(), &time);
    CarbonReplayEventType status = mPollSimState->getSimStatus();
    if (status == eReplayEventExit) {
      buf << "Simulation exited";

      if (mRecord->isChecked()) {
        // auto-switch into 'stop' mode
        mStop->setChecked(true);
      }
    }
    else {
      buf << "Simhost " << mPollSimState->getSimHost()
          << ";   pid " << mPollSimState->getSimPID();

    }
    buf << ";   last update " << time;

    UInt32 numEvents = mPollSimState->numEvents();
    if (numEvents > 0) {
      CarbonReplaySystem::Event* event;
      event = mPollSimState->getEvent(numEvents - 1);

      if (mCyclesSpecified) {
        UInt32 cyclesPerSecond
          = UInt32(0.5 + (event->mCycles / event->mUser));
        buf << ";   speed " << cyclesPerSecond << "cps";
      }
      else {
        UInt32 cyclesPerSecond
          = UInt32(0.5 + (event->mTotalSchedCalls / event->mUser));
        buf << ";   accumulated aggregate speed " << cyclesPerSecond << "cps";
      }
    }

    statusBar()->showMessage(buf.c_str());

    for (CarbonReplaySystem::ComponentLoop p(mPollSimState->loopComponents());
         !p.atEnd(); ++p)
    {
      CarbonSystemComponent* simComp = p.getValue();
      const char* name = simComp->getName();
      CarbonSystemComponent* guiComp =
        mReplaySystem->findComponent(name);
      if (guiComp != NULL) {
        *guiComp = *simComp;
      }
    }
    repaint();

    bool cycles = mPollSimState->isCycleCountSpecified();
    if (cycles != mCyclesSpecified) {
      mCyclesSpecified = cycles;
      setupHeaderLabels();
    }
    mPlot->update();
  } // if

  if (!mSimRequestActive && mPollSimState->checkSimRequest()) {
    mSimRequestActive = true;
    showWindow();
    raise();
    mContinueSim->setEnabled(true);
    paintSimContButton();
  }
} // void ReplayGUI::pollSimFile

void ReplayGUI::continueSim() {
  mContinueSim->setEnabled(false);
  mPollSimState->writeSimGrant();
  mSimRequestActive = false;
  paintSimContButton();
}
