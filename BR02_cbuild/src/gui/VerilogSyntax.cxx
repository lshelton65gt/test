// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "VerilogSyntax.h"
#include <QtGui>
#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "gui/CQt.h"            // for operator<<(UtString&,QString)


VerilogSyntax::VerilogSyntax(QTextDocument *parent)
  : QSyntaxHighlighter(parent)
{
  mKeywordFormat.setFontWeight(QFont::Bold);
  UtString keywords, gates, types, v2001;

  keywords
    << "always assign attribute begin case casex casez deassign default " 
    << "defparam disable edge else end endattribute endcase endfunction "
    << "endmodule endprimitive endspecify endtable endtask event for force "
    << "forever fork function highz0 highz1 if initial join large macromodule "
    << "medium module negedge parameter posedge primitive pull0 pull1 release "
    << "repeat rtranif1 scalared small specify specparam strength strong0 "
    << "strong1 table task trior use vectored wait weak0 weak1 while";
  for (StrToken tok(keywords.c_str()); !tok.atEnd(); ++tok) {
    mColorMap[*tok] = Qt::darkBlue;
  }

  gates
    << "and nand or nor xor xnor buf bufif0 bufif1 not notif0 notif1 "
    << "pulldown pullup nmos rnmos pmos rpmos cmos rcmos tran rtran "
    << "tranif0 rtranif0 tranif1 rtranif1";
  for (StrToken tok(gates.c_str()); !tok.atEnd(); ++tok) {
    mColorMap[*tok] = Qt::darkGreen;
  }

  types
    << "input output inout wire tri tri1 supply0 wand triand tri0 "
    << "supply1 wor time trireg trior reg integer real realtime genvar";
  for (StrToken tok(types.c_str()); !tok.atEnd(); ++tok) {
    mColorMap[*tok] = Qt::darkMagenta;
  }

  v2001
    << "config endconfig design instance liblist use library cell "
    << " generate endgenerate automatic";
  for (StrToken tok(v2001.c_str()); !tok.atEnd(); ++tok) {
    mColorMap[*tok] = Qt::darkYellow;
  }

  mCommentFormat.setForeground(Qt::red);
  mCommentFormat.setFontItalic(true);
  
  mQuoteFormat.setForeground(Qt::darkGray);
  
  setCurrentBlockState((int) eStart);

  // I don't think any special formatting is required for functions
  /*
   *  QTextCharFormat functionFormat;
   *  functionFormat.setFontItalic(true);
   *  functionFormat.setForeground(Qt::blue);
   *  mMappings["\\b[A-Za-z0-9_]+\\(.*\\)"] = functionFormat;
   */
} // VerilogSyntax::VerilogSyntax

VerilogSyntax::~VerilogSyntax() {
}

// re-entrant lexer.  It will return every time it finds a token,
// and put that token into 'tok', and put the current state into
// retstate
VerilogSyntax::State VerilogSyntax::lex(const char* ascii, int* start,
                                        int* len, State* retstate)
{
  State state = *retstate;
  State nextState = state;
  const char* p;
  ascii += *start;

  for (p = ascii; (state == nextState) && (*p != '\0'); ++p) {
    char c = *p;

    if (state == eBlockComment) {
      if (strncmp(p, "*/", 2) == 0) {
        ++p;
        nextState = eStart;
      }
    }
    else if (state == eLineComment) {
      if (c == '\n') {
        nextState = eStart;
      }
    }
    else if (state == eDQuote) {
      if (c == '\\') {
        c = *++p;
      }
      if (c == '"') {
        nextState = eStart;
        ++p;
      }
    }
    else if (c == '"') {
      nextState = eDQuote;
    }
    else if (isalnum(c) || (c == '$') || (c == '_') || (c == '\'')) {
      nextState = eName;        // or number.  Don't care which
    }
    else if (isspace(c)) {
      nextState = eWhitespace;
    }
    else if (strncmp(p, "/*", 2) == 0) {
      nextState = eBlockComment;
    }
    else if (strncmp(p, "//", 2) == 0) {
      nextState = eLineComment;
    }      
    else {
      nextState = ePunctuation;
    }

    if (state == eStart) {
      state = nextState;
    }
  } // for

  *len = p - ascii;
  if (state != nextState) {
    *len -= 1;
  }
  
  // It appears that the 'blocks' that Qt sends us are lines, and
  // they do not include the trailing newlines, so forcibly end
  // line-comments
  if (nextState == eLineComment) {
    nextState = eStart;
  }

  *retstate = nextState;
  return state;
} // VerilogSyntax::State VerilogSyntax::lex

void VerilogSyntax::highlightBlock(const QString &text)
{
  UtString cpy;
  cpy << text;
  const char* ascii = cpy.c_str();

  int stateIndex = previousBlockState();
  State state = (stateIndex == -1) ? eStart : ((State) stateIndex);
  int start = 0, len = 0;
  while (ascii[start] != '\0') {
    State tokType = lex(ascii, &start, &len, &state);
    switch (tokType) {
    case eName: {
      UtString tok(ascii + start, len);
      ColorMap::iterator p = mColorMap.find(tok);
      if (p != mColorMap.end()) {
        QColor color = p->second;
        mKeywordFormat.setForeground(color);
        setFormat(start, len, mKeywordFormat);
      }
      break;
    }
    case eBlockComment:
    case eLineComment:
      setFormat(start, len, mCommentFormat);
      break;
    case eDQuote:
      setFormat(start, len, mQuoteFormat);
      break;
    default:
      break;
    }
    start += len;
  } // while
  setCurrentBlockState((int) state);
} // void VerilogSyntax::highlightBlock
