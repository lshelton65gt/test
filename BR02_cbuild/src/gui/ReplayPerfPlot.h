/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _REPLAY_PERF_PLOT_H_
#define _REPLAY_PERF_PLOT_H_

#include "shell/ReplaySystem.h"
#include <QObject>


// at the moment, CZoomPlot doesn't work.  It zooms, it scrolls, but it
// shows no data.  I think this is because the transform-stack is not
// working.  Or perhaps this mechanism does not interact correctly with
// the setRawData mechanism we use for creating curves.  This has to be
// debugged.  For now, turn it off.

#define USE_ZOOM_PLOT 0
#if USE_ZOOM_PLOT
#include "gui/CZoomPlot.h"
#define REPLAY_PERF_PLOT_SUPERCLASS CZoomPlot
#else
#include <qwt_plot.h>
#include <qwt_symbol.h>
#define REPLAY_PERF_PLOT_SUPERCLASS QwtPlot
#endif

class QwtPlotCurve;
class Background;
class CpuCurve;

class ReplayPerfPlot : public REPLAY_PERF_PLOT_SUPERCLASS {
  Q_OBJECT
public:
  ReplayPerfPlot(CarbonReplaySystem* rs, QWidget *parent = NULL);
  ~ReplayPerfPlot();

  //! update the plot based on the current state of the replay system
  virtual void update();

  //! reset the ReplaySystem ptr
  void putReplaySystem(CarbonReplaySystem* rs);
  
  //! override the size hint so the user can squish the graph
  virtual QSize sizeHint() const;

  enum XAxis {eCycles, eRealTime, eCPUTime, eSchedCalls, eSimTime};
  enum SimUnit {eUCycles, eUSchedCalls, eUSimTime};

  XAxis getXAxis() const {return mXAxis;}

  double eventX(CarbonReplaySystem::Event* event);
  double eventCpu(CarbonReplaySystem::Event* event);
  void putXAxis(XAxis axis);

  //! unit regression test for bug7586
  static int testCpuArray();

  //! override method for benefit of CZoomPlot
  void getExtents(double* xsize, double* ysize);

private:
  CpuCurve* addLegend(const char* name, QwtSymbol::Style style,
                      int x, int y, const QColor&);
  void resetYAxisTitle();

  CpuCurve *mPerformance;

  double mXAxisMax;
  double mYAxisMax;
  
  Background* mBackground;

  XAxis mXAxis;
  CarbonReplaySystem* mReplaySystem;
  SimUnit mSimUnit;
  UtString mTimeUnits;
};

#endif
