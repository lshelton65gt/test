// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include <qvalidator.h>

// This is a validator class that uses the Qt Builtin QRegExpValidator
// to create a validator that allows of for either a hexadecimal string
// started 0x followed by the [0-9,a-f,A-F], or a decimal string containing only the digits [0-9]
// It can also check if the resulting value is within range

class CHexDecValidator : public QValidator {
 public:
  //! Constructor
  /*
    \param parent Parent Widget
    \param min Minimum value allowed, -1 means no minimum.
    \param min Maximum value allowed, -1 means no maximum.
  */
  CHexDecValidator(QObject* parent, unsigned long long min = 0, unsigned long long max = 0xffffffffffffffffLL)
    : QValidator(parent),
      mRegExp("[0-9]+|0x[0-9,a-f,A-F]+"),
      mRegExpValidator(mRegExp, parent),
      mMin(min),
      mMax(max)
  {
  }
  
  //! Set Valid Range
  /*
    \param min Minimum value allowed, -1 means no minimum.
    \param min Maximum value allowed, -1 means no maximum.
  */
  void setRange(unsigned long long min, unsigned long long max)
  {
    mMin = min;
    mMax = max;
  }

  //! Perform string validation
  QValidator::State validate(QString & input, int & pos) const
  {
    // First use the RegExp validator to check it it's legal characters
    QValidator::State state = mRegExpValidator.validate(input, pos);

    // If its a legal value, check the range
    if(state == QValidator::Acceptable) {
      // First check if value is decimal
      bool ok = false;
      unsigned long long val = input.toULongLong(&ok);
      
      // If ok is true, the value must have been decimal, else it must be hexadecimal
      // It already passed the Dec or Hex validator, so there are no other posibilities
      if(!ok) {
        val = input.right(input.length()-2).toULongLong(&ok, 16);
      }
      
      // now check the values
      if(val < mMin || val > mMax)
        state = QValidator::Invalid;
    }

    return state;
  }

private:
  QRegExp            mRegExp;
  QRegExpValidator   mRegExpValidator;
  unsigned long long mMin;
  unsigned long long mMax;
};


  
