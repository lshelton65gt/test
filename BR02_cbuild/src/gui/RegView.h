// -*-C++-*-

#ifndef _REG_VIEW_H_
#define _REG_VIEW_H_

#include "carbon/carbon_dbapi.h"
#include "gui/CQt.h"
#include "shell/ReplaySystem.h"
#include "util/SourceLocator.h"
#include "util/UtBiMap.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtIOEnum.h"
#include "util/UtString.h"
#include <QHBoxLayout>

class QTreeWidget;
class QTreeWidgetItem;
class TextEdit;
class CarbonReg;
class ReplayGUI;                // should use signals/slots.  trouble for now
class DialogRegProperties;

struct CarbonGroup {
public:
  CARBONMEM_OVERRIDES

  CarbonGroup(const char* name): mName(name) {}
  const char* getName() const {return mName.c_str();}

  UInt32 numRegs() const {return mRegs.size();}
  CarbonReg* getReg(UInt32 i) {return mRegs[i];}
  void putName(const char* name) {mName = name;}
  void addReg(CarbonReg* reg) {mRegs.push_back(reg);}
  void removeReg(CarbonReg* reg);

private:
  
  UtString mName;
  UtArray<CarbonReg*> mRegs;
};

struct CarbonReg {
public:
  CARBONMEM_OVERRIDES

  CarbonReg(const char* signal,
            //const char* name,
            CarbonGroup* group, 
            CarbonRadix radix,
            UInt32 idx);
  CarbonReg(UtIStream& f, UtString* groupName);
  ~CarbonReg();
  void write(UtOStream& f);

  //const char* getName() const {return mName.c_str();}
  const char* getPath() const {return mSignal.c_str();}
  CarbonRadixIO getRadix() const {return mRadix;}
  const char* getGroupName() const {return mGroup->getName();}
  CarbonGroup* getGroup() const {return mGroup;}

  void putRadix(CarbonRadix radix) {mRadix = radix;}
  void putGroup(CarbonGroup* group) {mGroup = group;}
  //void putName(const char* name) {mName = name;}

  void localize(const char* comp, const char* design_path);
  bool isLocalized() {return mIsLocalized;}
  const char* getComponent() const {return mComponent.c_str();}
  const char* getDesignPath() const {return mDesignPath.c_str();}

  CarbonBreakpointType getBreakpointType() const { return mBreakpointType; }
  const char* getBreakpointValue() const { return mBreakpointValue.c_str(); }

  void putBreakpointType(CarbonBreakpointType type) { mBreakpointType = type; }
  void putBreakpointValue(const char* val) { mBreakpointValue = val; }

  const char* getCurrentValue() { return mCurrentValue.c_str(); }
  void putCurrentValue(const char* val) { mCurrentValue = val; }

  UInt32 getIndex() const { return mIndex; }

private:

  UtString mSignal;
  bool mIsLocalized;
  UtString mComponent;
  UtString mDesignPath;
  CarbonGroup* mGroup;
  CarbonRadixIO mRadix;
  UtString mCurrentValue;
  
  CarbonBreakpointType mBreakpointType;
  UtString mBreakpointValue;
  UInt32 mIndex;
}; // struct CarbonReg


class RegView : public QWidget {
  Q_OBJECT

public:

  //! enum for register columns
  enum {
    eRTLPath,
    eRadix,
    eCurrentValue,
    eBreakpoint
  } Column;

  //! ctor
  RegView(CQtContext*, AtomicCache* atomicCache, QAction* deleteAction,
          ReplayGUI* replay_gui = NULL);

  //! dtor
  ~RegView();

  //! clear the tree
  void clear();
  
  void read();
  QWidget* buildLayout();

  bool isDeleteActive() const;

  QTreeWidgetItem* addReg(QTreeWidgetItem*, CarbonReg*);
  QTreeWidgetItem* addGroup(CarbonGroup* group, bool expand_item);

  UInt32 numGroups() const {return mGroups.size();}
  CarbonGroup* getGroup(UInt32 i) const {return mGroups[i];}

  void putRegValue(CarbonReg* reg, const char* val, bool is_fired,
                   bool sim_advanced);

  void buildWindow(QWidget* parent = NULL);

  void resizeColumns();

  void putNextIndex(UInt32 next_idx) { mNextIndex = next_idx; }

  void showContinueButton();

public slots:
  void changeName(const QString&);
  // void changeRegRadix(int);
  // void changeBreakpointType(int);
  void changeRegTreeSel();
  void dropRegister(const char*, QTreeWidgetItem*);
  void createGroup();
  void deleteObj();
  void mapDropItem(QTreeWidgetItem** item);
  void doubleClick(QTreeWidgetItem* item, int col);
  void editObj();
  void continueSim();

private:
  typedef UtArray<QTreeWidgetItem*> ItemVec;

  void applyChanges(CarbonRadix* radix, CarbonBreakpointType* bt);
  void setRegisterTreeText(QTreeWidgetItem* item, CarbonReg* reg);
  void setRegisterWidgets(int numSelected, CarbonReg* nd);
  void deleteNetItem(QTreeWidgetItem* item, int childIndex, bool removeReg);
  void deleteGroup(QTreeWidgetItem* groupItem, int idx);
  void launchRegPropDialog(const ItemVec& v);

  CDragDropTreeWidget* mRegTree;

  QPushButton* mContinueSim;
  QPushButton* mCreateGroup;
  QPushButton* mDeleteObj;
  QPushButton* mEditObj;

  QComboBox* mRegGroup;

  UtBiMap<QTreeWidgetItem*,CarbonGroup*> mItemGroupMap;
  UtBiMap<QTreeWidgetItem*,CarbonReg*> mItemRegMap;

  typedef UtHashMap<UtString,CarbonReg*> NetRegisterNameMap;
  NetRegisterNameMap mNetRegisterNameMap;

  bool mDisable;
  
  SourceLocatorFactory mLocatorFactory;
  AtomicCache* mAtomicCache;

  QAction* mDeleteAction;
  
  CarbonReg* mEditRegister;
  CarbonGroup* mEditGroup;
  CQtContext* mCQt;

  QColor mDefaultTextColor;
  UtArray<CarbonGroup*> mGroups;
  UInt32 mGroupIndex;
  QMainWindow* mMainWindow;

  QComboBox* mRadix;
  QComboBox* mBreakpointType;
  QLineEdit* mBreakpointValue;
  ReplayGUI* mReplayGUI;
  DialogRegProperties* mDialogRegProperties;
  UInt32 mNextIndex;
  QColor mSaveSimContForeground;
  QColor mSaveSimContBackground;
}; // class RegView : public QWidget

#endif
