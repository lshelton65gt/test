/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "car2cow.h"
#include "MemoryTreeWidget.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtStringArray.h"
#include "util/UtStringUtil.h"

#include "gui/CQt.h"


MemoryTreeWidget::MemoryTreeWidget(QWidget* parent) : CDragDropTreeWidget(true, parent)
{
  mDB = NULL;
  mCfg = NULL;
  mCtx = NULL;

  CQT_CONNECT(this, dropReceived(const char*,QTreeWidgetItem*), this, dropMemory(const char*));

  setItemDelegate(new MemoryEditDelegate(this,this));

  setSortingEnabled(true);

 // header()->setResizeMode(0, QHeaderView::ResizeToContents);
 //// header()->setResizeMode(2, QHeaderView::Stretch);
}

MemoryTreeWidget::~MemoryTreeWidget()
{

}

void MemoryTreeWidget::dropMemory(const char* path) 
{
  UtString warning;
  UtIStringStream is(path);
  UtString buf;
  UtString errmsg;
  QTreeWidgetItem* item = NULL;
  while (is.getline(&buf)) 
  {
    StringUtil::strip(&buf, "\n");
    path = buf.c_str();
    const CarbonDBNode* node = carbonDBFindNode(mDB, path);

    if (node == NULL) 
    {
      errmsg << "No such net in the design: " << path << "\n";
    }
    else if (node && ! ((carbonDBIs2DArray(mDB, node) && !carbonDBIsContainedByComposite(mDB, node))))
    {
      errmsg << "Node " << path << " is not a memory\n";
    }
    else if (! carbonDBIsVisible(mDB, node)) 
    {
      errmsg << "Net " << path << " is not visible -- declare it observable\n";
    }
    else if (findMemory(path) != NULL) 
    {
      errmsg << "Duplicate entry for memory " << path << "\n";
    }
    else 
    {
      item = addMemoryNode(node);
      INFO_ASSERT(item, "Failed to drop memory");
      mCtx->setDocumentModified(true);
    }
  }
  
  if (!errmsg.empty()) 
  {
    mCtx->getCQt()->warning(this, errmsg.c_str());
  }

}

CarbonCfgMemory* MemoryTreeWidget::findMemory(const char* path)
{
  for (UInt32 i=0, nMems = mCfg->numMemories(); i<nMems; ++i)
  {
    CarbonCfgMemory* mem = mCfg->getMemory(i);

    if (0 == strcmp(path, mem->getPath()))
      return mem;
  }

  return NULL;
}

QTreeWidgetItem* MemoryTreeWidget::addMemoryNode(const CarbonDBNode* node)
{
  UtString path(carbonDBNodeGetFullName(mDB, node));

  UtString name;
  mCfg->getUniqueMemName(&name, carbonDBNodeGetLeafName(mDB, node));
  SInt32 left = carbonDBGet2DArrayLeftAddr(mDB, node);
  SInt32 right = carbonDBGet2DArrayRightAddr(mDB, node);
  UInt32 maxAddr = std::abs(right - left); // this is not 'num addrs'
  UInt32 width = carbonDBGetWidth(mDB, node);

  CarbonCfgMemory* mem = mCfg->addMemory(path.c_str(), name.c_str(),
                                "", // init file
                                maxAddr, width,
                                eCarbonCfgReadmemh,
                                ""); // comment
  return addMem(mem);
}

QTreeWidgetItem* MemoryTreeWidget::addMem(CarbonCfgMemory* mem) 
{
  QTreeWidgetItem* item = new QTreeWidgetItem(this);
  item->setText(colNAME, mem->getName());
  QVariant qv = qVariantFromValue((void*)mem);
  item->setData(0, Qt::UserRole, qv);
  item->setFlags(item->flags() | Qt::ItemIsEditable);

  UtString maxAddr = "0x";
  UtOStringStream os(&maxAddr);
  os << UtIO::hex << mem->getMaxAddrs();
 
  item->setText(colPATH, mem->getPath());
  item->setText(colMAXADDR, maxAddr.c_str());

  UtString width;
  UtOStringStream os1(&width);
  os1 << UtIO::dec << mem->getWidth();

  item->setText(colWIDTH, width.c_str());

  return item;
}

void MemoryTreeWidget::buildSelectionList(QList<QTreeWidgetItem*>& list)
{
  // The list gives items for every column and row, we just want it row
  // oriented.

  list.clear();

  foreach (QTreeWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(0, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonCfgMemory* node = (CarbonCfgMemory*)qv.value<void*>();
      if (node != NULL)
      {
        list.append(item);
      }
    }
  }  
}

void MemoryTreeWidget::DeleteMemory()
{
  QList<QTreeWidgetItem*> selectedItemList;
  buildSelectionList(selectedItemList);
  CarbonCfgMemory* mem = NULL;

  foreach (QTreeWidgetItem *item, selectedItemList)
  {
    QVariant qv = item->data(0, Qt::UserRole);
    mem = (CarbonCfgMemory*)qv.value<void*>();

    // We have the field to work with;
    if (mem != NULL)
    {
      mCfg->removeMemory(mem);

      int childIndex = indexOfTopLevelItem(item);
      INFO_ASSERT(childIndex >= 0, "Cannot find item index of item");
      takeTopLevelItem(childIndex);

      mCtx->setDocumentModified(true);
    }
  }

}

void MemoryTreeWidget::Populate(ApplicationContext* ctx, bool /*initPorts*/)
{
  mCtx = ctx;
  mCfg = ctx->getCfg();

  for (UInt32 i=0, nMems = mCfg->numMemories(); i<nMems; ++i)
  {
    CarbonCfgMemory* mem = mCfg->getMemory(i);
    addMem(mem);
  }

  header()->setResizeMode(colNAME, QHeaderView::ResizeToContents);
  header()->setResizeMode(colPATH, QHeaderView::Stretch);
  header()->setResizeMode(colMAXADDR, QHeaderView::ResizeToContents);
  header()->setResizeMode(colWIDTH, QHeaderView::ResizeToContents);
  header()->setResizeMode(colCOMMENT, QHeaderView::Stretch);
}


void MemoryTreeWidget::Initialize(ApplicationContext* ctx)
{
  mCtx = ctx;
  mDB = ctx->getDB();
  mCfg = ctx->getCfg();
}

// Editor Delegate
MemoryEditDelegate::MemoryEditDelegate(QObject *parent, MemoryTreeWidget* t)
: QItemDelegate(parent), tree(t)
{
}

void MemoryEditDelegate::currentIndexChanged(int)
{
  //QWidget* widget = tree->mEditItem;
  //if (widget)
  //{
  //  commitData(widget);
  //  closeEditor(widget);
  //}
}

QWidget *MemoryEditDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                          const QModelIndex &index) const
{
  QTreeWidgetItem* item = tree->currentItem();
  QVariant qv = item->data(0, Qt::UserRole);
 // CarbonCfgMemory* mem = (CarbonCfgMemory*)qv.value<void*>();

  switch (index.column())
  {
  case MemoryTreeWidget::colNAME:
  case MemoryTreeWidget::colCOMMENT:
   {
      QLineEdit *editor = new QLineEdit(parent);
      connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
      return editor;
    }
    break;
  default:
    break;
  }

  return NULL;
}

void MemoryEditDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  if (editor)
  {
    emit commitData(editor);
    emit closeEditor(editor);
  }
}

void MemoryEditDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}
void MemoryEditDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                      const QModelIndex &index) const
{
  QTreeWidgetItem* item = tree->currentItem();
  QVariant qv = item->data(0, Qt::UserRole);
  CarbonCfgMemory* mem = (CarbonCfgMemory*)qv.value<void*>();

  UtString editText;
  switch (index.column())
  {
  case MemoryTreeWidget::colNAME:
    {
      QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
      if (edit) 
      {
        tree->mCtx->setDocumentModified(true);
        editText << edit->text();
        tree->mCfg->changeMemoryName(mem, editText.c_str());
        model->setData(index, editText.c_str());
      }
    }
    break;
  case MemoryTreeWidget::colCOMMENT:
    {
      QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
      if (edit) 
      {
        tree->mCtx->setDocumentModified(true);
        editText << edit->text();
        mem->putComment(editText.c_str());
        model->setData(index, editText.c_str());
      }
    }
    break;

  default:
    break;
  }

}
