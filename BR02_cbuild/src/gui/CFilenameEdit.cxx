/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "util/CarbonAssert.h"
#include "gui/CFilenameEdit.h"
#include "gui/CQt.h"
#include <QtGui>

CFilenameEdit::CFilenameEdit(const char* widgetName,
                             bool readFile, const char* prompt,
                             const char* filter, CQtContext* cqt):
  mReadFile(readFile),
  mPrompt(prompt),
  mFilter(filter),
  mCQt(cqt)
{
  UtString buf;
  buf << widgetName << "_lineEdit";
  mLineEdit = cqt->lineEdit(buf.c_str());
  buf.clear();
  buf << widgetName << "_pushButton";
  mPushButton = cqt->pushButton(buf.c_str(), "Browse");
  CQT_CONNECT(mPushButton, clicked(), this, openFileDialog());
  CQT_CONNECT(mLineEdit, textChanged(const QString&),
              this, lineEditChanged(const QString&));
  setLayout(CQt::hbox(mLineEdit, mPushButton));
}

CFilenameEdit::~CFilenameEdit() {
}

void CFilenameEdit::lineEditChanged(const QString& str) {
  UtString buf;
  buf << str;
  emit filenameChanged(buf.c_str());
}

void CFilenameEdit::openFileDialog() {
  UtString filename;
  filename << mLineEdit->text();
  if (mCQt->popupFileDialog(this, mReadFile, mPrompt.c_str(),
                            mFilter.c_str(), &filename))
  {
    mLineEdit->setText(filename.c_str());
    emit filenameChanged(filename.c_str());
  }
}

const char* CFilenameEdit::getFilename() const {
  mFileBuffer.clear();
  mFileBuffer << mLineEdit->text();
  return mFileBuffer.c_str();
}
