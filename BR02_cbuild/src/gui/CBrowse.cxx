/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

/*
to do

-  distinguish primary-input nodes from 'unbound'
-  add widgets to control max width
-  encapsulate non-continuous drivers in visible hierarchy, to be
   expanded by user when more detail is needed (soha)
-  add 'print' button
-  use QProcess for dotty & dotread rather than OSSystem, also eliminating
   foo.dot tempfile
-  drag & drop nodes
-  display more detail about node while hovering over it

*/

#include "CBrowse.h"
#include "CBrowseButton.h"
#include "CBrowseNode.h"
#include "gui/CDockWidget.h"
#include "gui/CDragDropTreeWidget.h"
#include "hbrowser.h"
#include "iodb/CGraph.h"
#include "textedit.h"
#include "util/AtomicCache.h"
#include "util/SourceLocator.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtStringUtil.h"
#include "util/Zstream.h"
#include <QMessageBox>
#include <QStringList>
#include <QTimer>
#include <QTreeWidgetItem>
#include <QVBoxLayout>
#include <QtGui>
#include <cassert>

#define APP_NAME "Carbon Component Wizard"

#define PIXELS_PER_INCH 100.0
#define POINTS_PER_INCH 72.0
#define PIXELS_PER_POINT (PIXELS_PER_INCH / POINTS_PER_INCH)

CBrowse::CBrowse(CQtContext* cqt,
                 const char* /*name*/,
                 CarbonDB* db,
                 const CarbonDBNode* node,
                 CGraph* cgraph,
                 CarbonHBrowser* hbrowser,
                 const char* unique_suffix)
  : mDB(db), mNode(node), mCQt(cqt), mCGraph(cgraph),
    mHBrowser(hbrowser),
    mUniqueSuffix(unique_suffix),
    mDotTimeoutMessageBox(NULL),
    mDotTimeoutMessageBoxActive(false),
    mDotProcessSucceeded(false),
    mDotStarted(false)
  
{
  CQtRecordBlocker blocker(cqt);

  mDotProcess = NULL;
  setupDotProcess();

  setAttribute(Qt::WA_DeleteOnClose);
  setCentralWidget(buildLayout());
  
  mFont = new QFont("Helvetica", 8, QFont::Normal);
  mFontMetrics = new QFontMetrics(*mFont);
  mNodeEnterLeaveDepth = 0;

  mStabilize = NULL;
  mNavigationStackIndex = 0;
  setFocus(node);
  enableButtons();
} // CBrowse::CBrowse

void CBrowse::setupDotProcess() {
  if (mDotProcess != NULL) {
    disconnect(mDotProcess, SIGNAL(finished(int, QProcess::ExitStatus)),
               this, SLOT(dotFinished(int, QProcess::ExitStatus)));
    disconnect(mDotProcess, SIGNAL(readyReadStandardOutput()),
               this, SLOT(dotOutput()));
    disconnect(mDotProcess, SIGNAL(readyReadStandardError()),
               this, SLOT(dotErrors()));
    disconnect(mDotProcess, SIGNAL(error(QProcess::ProcessError err)),
               this, SLOT(dotError(QProcess::ProcessError err)));
    // leak the old zombie process variable
  }
  mDotProcess = new QProcess(this);
  CQT_CONNECT(mDotProcess, finished(int, QProcess::ExitStatus),
              this, dotFinished(int, QProcess::ExitStatus));
  CQT_CONNECT(mDotProcess, readyReadStandardOutput(), this, dotOutput());
  CQT_CONNECT(mDotProcess, readyReadStandardError(), this, dotErrors());
  CQT_CONNECT(mDotProcess, error(QProcess::ProcessError),
              this, dotError(QProcess::ProcessError));
}

void CBrowse::createToolBars() {
  UtString actionName;

  // no exit action because this is a dockable window, and
  // the dockable decorations have their own mechanism for
  // closing

  actionName.clear();
  actionName << "clear_cbrowser" << mUniqueSuffix;
  mClearAct = new QAction(tr("C&lear"), this);
  mClearAct->setShortcut(tr("Ctrl+C"));
  mClearAct->setStatusTip(tr("Clear the browser"));
  CQT_CONNECT(mClearAct, triggered(), this, clearNodes());
  mCQt->registerAction(mClearAct, actionName.c_str());

  actionName.clear();
  actionName << "next_cbrowser" << mUniqueSuffix;
  mNextAct = new QAction(tr("N&ext"), this);
  mNextAct->setShortcut(tr("Ctrl+N"));
  mNextAct->setStatusTip(tr("Next View"));
  CQT_CONNECT(mNextAct, triggered(), this, nextView());
  mCQt->registerAction(mNextAct, actionName.c_str());

  actionName.clear();
  actionName << "prev_cbrowser" << mUniqueSuffix;
  mPrevAct = new QAction(tr("P&rev"), this);
  mPrevAct->setShortcut(tr("Ctrl+P"));
  mPrevAct->setStatusTip(tr("Previous View"));
  CQT_CONNECT(mPrevAct, triggered(), this, prevView());
  mCQt->registerAction(mPrevAct, actionName.c_str());

  mMainToolBar = addToolBar(tr("CBrowser"));
  mMainToolBar->addAction(mClearAct);
  mMainToolBar->addAction(mNextAct);
  mMainToolBar->addAction(mPrevAct);
  mMainToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
} // void CBrowse::createToolBars

void CBrowse::setFocus(const CarbonDBNode* node) {
  UtString title;
  title << "Carbon Connectivity" << " - "
        << carbonDBNodeGetFullName(mDB, node);
  setWindowTitle(title.c_str());

  mFocusNodes.insert(node);

  // Create an initial layout that's (say) 640 x 480.  I think that will
  // show up as the initial pixel size of the graphics view.  Then we'll
  // try to stabilize the first item we draw in the middle of that rectangle.
  // When we let 'dot' lay things out, we'll try to keep the rectangle stable
  // because I think it's hard to control the scrolling of the QGraphicsView
  // when we expand the bounding box.
  mBoundingBox = QRectF(-320, -240, 640, 480);
  //mGraphicsScene->setSceneRect(mBoundingBox);
  mStabilizePos = QPointF(0, 0);
  layout();
}

void CBrowse::clear() {
}

CBrowse::~CBrowse() {
  delete mFontMetrics;
  delete mFont;
  for (UInt32 i = 0; i < mNavigationStack.size(); ++i) {
    State* state = mNavigationStack[i];
    delete state;
  }
}

class CBrowseView : public QGraphicsView {
public:
  CARBONMEM_OVERRIDES

  CBrowseView(QGraphicsScene* scene, CBrowse* browse) :
    QGraphicsView(scene),
    mBrowse(browse)
  {}

  void dropEvent(QDropEvent* event) {
    if (event->mimeData()->hasText()) {
      event->acceptProposedAction();
      QString str = event->mimeData()->text();
      UtString buf;
      buf << str;
      mBrowse->dropText(buf.c_str());
    }
    else {
      event->ignore();
    }
  }

  void dragLeaveEvent(QDragLeaveEvent*) {
  }

  void dragMoveEvent(QDragMoveEvent* event) {
    const QMimeData* mimeData = event->mimeData();
    if (mimeData->hasText()) {
      event->acceptProposedAction();
    }
    else {
      event->ignore();
    }
  }

  void dragEnterEvent(QDragEnterEvent* event) {
    const QMimeData* mimeData = event->mimeData();
    if (mimeData->hasText()) {
      event->acceptProposedAction();
    }
    else {
      event->ignore();
    }
  }

private:
  CBrowse* mBrowse;
};

QWidget* CBrowse::buildLayout() {
  createToolBars();

  mGraphicsScene = new QGraphicsScene;
  //mGraphicsScene->setSceneRect(-200, -200, 200, 200);
  mGraphicsScene->setItemIndexMethod(QGraphicsScene::NoIndex);

  mGraphicsView = new CBrowseView(mGraphicsScene, this);
  mGraphicsView->setRenderHint(QPainter::Antialiasing);
  mGraphicsView->setCacheMode(QGraphicsView::CacheBackground);
  mGraphicsView->setAcceptDrops(true);

  // I like the hand-drag functionality.  If I could figure out how
  // to have it do that only when over whitespace I'd turn it on.  This
  // is much better than scrolling for navigation.  I guess a toggle-button
  // would do the trick.
  //mGraphicsView->setDragMode(QGraphicsView::ScrollHandDrag);

  return mGraphicsView;
}

void CBrowse::dropNet(const char* path, QTreeWidgetItem*) {
  UtString buf(path);
  StringUtil::strip(&buf);
  path = buf.c_str();

  if (mDB == NULL) {
    mCQt->warning(this, tr("Design Database not loaded"));
    return;
  }

  const CarbonDBNode* node = carbonDBFindNode(mDB, path);
  if (node == NULL) {
    mCQt->warning(this, tr("No such net in the design: %1")
                  .arg(path));
  }
  else if (! carbonDBIsLeaf(mDB, node)) {
    mCQt->warning(this, tr("Node %1 is a module, debug registers must be nets")
                  .arg(path));
  }
  else {
    clear();
    setFocus(node);
  }
}

void CBrowse::drawText(QPainter* painter,
                              const char* str, double x, double y,
                              double* width, double* height)
{
  QRectF r = mFontMetrics->boundingRect(str);
  *width = r.width();
  *height = r.height();
  r.moveTo(x, y);

  // for some reason I don't understand, Qt clips the text unless I
  // do this hack
  r.setWidth(2 * *width);

  QTextOption align(Qt::AlignLeft | Qt::AlignTop);
  painter->drawText(r, str, align);
}

void CBrowse::clearLayoutStructures() {
  mNodeStrs.clear();
  mEdgeStrs.clear();
  mLiveNodes.clear();
  mEdges.clear();
  clearEdgeObjects();
}

void CBrowse::clearEdgeObjects() {
  for (UInt32 i = 0; i < mEdgeItems.size(); ++i) {
    QGraphicsItem* item = mEdgeItems[i];
    mGraphicsScene->removeItem(item);
    delete item;
  }
  mEdgeItems.clear();
}

bool CBrowse::writeDotFile(const char* dot_filename) {
  // Write out the 'dot' file for every visible node, starting at
  // the focus node and expanding in all interesting directions
  UtOBStream dotfile(dot_filename);
  if (!dotfile.is_open()) {
    mCQt->warning(this, dotfile.getErrmsg());
    return false;
  }
  dotfile << "digraph L0 {\n";
  dotfile << "  graph [rankdir=LR nodesep=0]\n";
  dotfile << "  ordering=out;\n";
  dotfile << "  node [shape = box];\n\n";

  for (UtHashSet<const CarbonDBNode*>::SortedLoop p = mFocusNodes.loopSorted();
       !p.atEnd(); ++p)
  {
    const CarbonDBNode* focus_node = *p;
    for (CGraph::NodeSetLoop p(mCGraph->findDrivers(focus_node)); !p.atEnd();
         ++p)
    {
      CGraph::Node* cgNode = *p;

      CBrowseNode* cnode = layoutNode(cgNode);
      if (mStabilize == NULL) {
        mStabilize = cnode;
      }
    }
  }
  dotfile << mNodeStrs << mEdgeStrs << "}\n";
  if (!dotfile.close()) {
    mCQt->warning(this, dotfile.getErrmsg());
    return false;
  }
  return true;
} // bool CBrowse::writeDotFile

bool CBrowse::runDot(const char* dot_filename) {
  UtString cmd, dir, errmsg;

  mDotProcessSucceeded = false;
  mDotOutput.clear();
  mDotErrors.clear();
  const char* chome = getenv("CARBON_HOME");
  if (chome != NULL) {
#if pfUNIX
    cmd << chome << "/bin/dot-run " << dot_filename;
#endif
#if pfWINDOWS
    cmd << "dot " << dot_filename " | "
        << chome << "/bin/dotread.bat";
#endif

    mDotStarted = true;
    mDotProcess->start(cmd.c_str());
  }

  // Typically, 'dot' will only take a few seconds.  However it could
  // go compute-bound.  Let's give it 5 seconds to do its work.  After
  // that we'll put up a modal dialog that will allow the user to kill
  // the dot process.  Note that the UI will freeze while doing this
  // wait, until either the process exits or 5 seconds expires.
   if (! mDotProcess->waitForFinished(5000 /* 5 seconds */)) {
    if (!mDotStarted) {
      UtString buf("Failed to run dot:\n");
      buf << mDotErrors;
      mCQt->warning(this, buf.c_str());
    }
    else {
      // put up a dialog box
      if (mDotTimeoutMessageBox == NULL) {
        mDotTimeoutMessageBox = new QMessageBox(this);
        mDotTimeoutMessageBox->addButton(QMessageBox::Cancel);
        mDotTimeoutMessageBox->setText("The graph layout is taking a long time.");
      }

      // Note that while we are in exec(), the Dot process may finally
      // finish.  In that case, the dialog is programmatically "accepted"
      // and will disappear.
      mDotTimeoutMessageBoxActive = true;
      bool cancel = ! mDotTimeoutMessageBox->exec();
      mDotTimeoutMessageBoxActive = false;
      if (cancel) {
        mDotProcess->terminate();
        if (!mDotProcess->waitForFinished(2000)) { // escalate after 2 seconds
          mDotProcess->kill();
          if (!mDotProcess->waitForFinished(10000)) {

            // if after another 10 seconds if the process still hasn't died, we
            // have serious problems.  Let's just disconnect the signals
            // for this QProcess, leak it, and re-initialize
            setupDotProcess();
          }
        }
      }
    } // else
  } // if

  return mDotProcessSucceeded;
} // bool CBrowse::runDot

void CBrowse::dotFinished(int code, QProcess::ExitStatus exit_status) {
  mDotProcessSucceeded = (code == 0) && (exit_status == QProcess::NormalExit);
}

void CBrowse::dotError(QProcess::ProcessError err) {
  mDotStarted = false;
  const char* emsg = "?";
  switch (err) {
  case QProcess::FailedToStart: emsg = "FailedToStart"; break;
  case QProcess::Crashed: emsg = "Crashed"; break;
  case QProcess::Timedout: emsg = "Timedout"; break;
  case QProcess::WriteError: emsg = "WriteError"; break;
  case QProcess::ReadError: emsg = "ReadError"; break;
  case QProcess::UnknownError: emsg = "UnknownError"; break;
  }
  mDotErrors << emsg << "\n";
}

void CBrowse::dotOutput() {
  QByteArray bytes = mDotProcess->readAllStandardOutput();
  mDotOutput.append(bytes.data(), bytes.size());
}

void CBrowse::dotErrors() {
  QByteArray bytes = mDotProcess->readAllStandardError();
  mDotErrors.append(bytes.data(), bytes.size());
}


bool CBrowse::readDotFile(ItemPosMap* item_pos_map, bool *is_bb_set,
                          QRectF* bb)
{
  *is_bb_set = false;
  UtIStringStream is(mDotOutput.c_str());
  if (!is.is_open()) {
    mCQt->warning(this, is.getErrmsg());
    return false;
  }

  UInt32 line_number = 0;
  for (UtString buf; is.getline(&buf); ++line_number) {
    UtIStringStream line(buf);
    char comma;

    char token = '\0';
    if (! (line >> token)) {
      mCQt->warning(this, is.getErrmsg());
      return false;
    }

    // bounding box
    if (token == 'b') {
      UInt32 x1, y1, x2, y2;
      if (! (line
             >> x1 >> comma >> y1 >> comma
             >> x2 >> comma >> y2 >> comma))
      {
        mCQt->warning(this, is.getErrmsg());
        return false;
      }
      *bb = QRectF(x1 * PIXELS_PER_POINT,
                   y1 * PIXELS_PER_POINT,
                   x2 * PIXELS_PER_POINT,
                   y2 * PIXELS_PER_POINT);
      *is_bb_set = true;
    }

    // node
    else if (token == 'n') {
      UInt32 node_index, x, y;
      if (!(line >> node_index >> x >> comma >> y)) {
        mCQt->warning(this, is.getErrmsg());
        return false;
      }
      else if (comma != ',') {
        mCQt->warning(this, "expected comma");
      }
      else if (node_index >= mNodeArray.size()) {
        mCQt->warning(this, "invalid node");
        return false;
      }
      QGraphicsItem* item = mNodeArray[node_index];
      QRectF r = item->boundingRect();
      (*item_pos_map)[item] = QPointF(x * PIXELS_PER_POINT - r.width()/2,
                                      y * PIXELS_PER_POINT - r.height()/2);
    }

    else if (token == 'e') {
/*
  Ignore edges from 'dot' for now, just draw lines between the
  arrows.  To be able to use the 'dot' edges, we must tell it
  about the sub-structures indicating where on the box to draw
  the lines.
      UInt32 from, to, x1, y1, x2, y2;
      char space, e;
      if (!(line >> from >> to >> space >> e >> comma
            >> x1 >> comma >> y1 >> comma
            >> x2 >> comma >> y2 >> comma))
      {
        mCQt->warning(this, is.getErrmsg());
        return false;
      }
      
      QLineF line(x1 * PIXELS_PER_POINT,
                  y1 * PIXELS_PER_POINT,
                  x2 * PIXELS_PER_POINT,
                  y2 * PIXELS_PER_POINT);
      mEdgeItems.push_back(mGraphicsScene->addLine(line));
*/
    }
    else if (token == '\n')
      continue;
    // the remaining lines are edges
    else {
      UtString errmsg;
      errmsg << "unexpected token: " << token;
      mCQt->warning(this, errmsg.c_str());
      return false;
    }
  } // for
  return true;
} // bool CBrowse::readDotFile

void CBrowse::doStabilization(const ItemPosMap& item_pos_map,
                              bool is_bb_set, QRectF* bb)
{
  QPointF delta(0, 0);
  if (mStabilize != NULL) {
    ItemPosMap::const_iterator p = item_pos_map.find(mStabilize);
    if (p != item_pos_map.end()) {
      // if there is a visible stabilization node, then compute a delta
      // from where 'dot' placed it and move it back to where it
      // started.
      const QPointF& item_pos = p->second;
      delta = mStabilizePos - item_pos;
    }
  }
  mStabilize = NULL;

  for (UInt32 i = 0, n = mNodeArray.size(); i < n; ++i) {
    QGraphicsItem* cnode = mNodeArray[i];
    ItemPosMap::const_iterator p = item_pos_map.find(cnode);
    if (p != item_pos_map.end()) {
      QPointF pos = p->second;
      pos += delta;
      cnode->setPos(pos);
    }
  }

  if (is_bb_set) {
    bb->translate(delta);
  }
}

void CBrowse::placeEdges() {
  // Draw al the edges based on where the nodes were placed
  for (EdgeSet::iterator p = mEdges.begin(), e = mEdges.end(); p != e; ++p) {
    Edge& edge = *p;

    QPointF pos1 = edge.n1->pos();
    QPointF pos2 = edge.n2->pos();
    pos1 += edge.b1->pos();
    pos2 += edge.b2->pos();

    // We always express edges from n1 to n2, where n2 should be to
    // the right of n1.  If n2 is to the left, or is horizontally
    // aligned, then we have some kind of cyclic situation, and what
    // I'm going to do is draw a cubic spline instead, so the user knows it's
    // different.  A nicer solution would be to take the spline path
    // suggested by dot, but for now I don't understand how to interpret
    // that path with Qt graphics objects, and more importantly, dot
    // doesn't know the precise end-points, because it doesn't know about
    // the buttons.  It will connect the edges with any convenient point
    // on the periphery of the bounding box, which is not what I want.
    QGraphicsItem* item = NULL;
    if (pos1.x() >= pos2.x()) {
      QPainterPath path(pos1);

      // control-point 1 is at the midpoint between pos1 and pos2
      double mid_x = (pos1.x() + pos2.x()) / 2;
      double mid_y = (pos1.y() + pos2.y()) / 2;
      QPointF c1(mid_x, mid_y);
                 
      // control-point 2 is at the same x-position as the mid-point,
      // but is below it by the width/2 of the line's bounding box.  That
      // makes an spline that is proportional in vertical coverage
      // to its width.  Based on my limited experiments this seems to
      // look OK.  Note that this (as well as the straight edges) can
      // intersect nodes.
      double c2_y = mid_y + (pos1.x() - pos2.x())/2;
      QPointF c2(mid_x, c2_y);

      path.cubicTo(c1, c2, pos2);
      item = mGraphicsScene->addPath(path);

      // the spline covers the lowest real estate (greatest y-value) on the
      // display.  The boundingRect function on the PathItem seems
      // to be pessimistic (but fast) -- I think it's calling
      // QPainterPath::controlPointsRect.  I think I want a more accurate view
      // of what points are hit by the spline, which is available with
      // QPainterPath::boundingRect, which is slower.  If there are speed issues
      // then we may want to estimate the bounding rect a little better.
      QRectF spline_bounds = path.boundingRect();
      if (!mBoundingBox.contains(spline_bounds)) {
        mBoundingBox |= spline_bounds;
      }
    }
    else {
      QLineF line(pos1, pos2);
      item = mGraphicsScene->addLine(line);
    }
    item->setZValue(1.0);       // draw edges over nodes
    mEdgeItems.push_back(item);
  }
} // void CBrowse::placeEdges

void CBrowse::layout() {
  clearLayoutStructures();
  UtString dot_filename, errmsg;
  OSGetTempFileName(&dot_filename);
  dot_filename << ".dot";
  if (!writeDotFile(dot_filename.c_str())) {
    return;
  }
  removeDeadNodes();
  bool dot_process_succeeded = runDot(dot_filename.c_str());
  (void) OSUnlink(dot_filename.c_str(), &errmsg);
  if (!dot_process_succeeded) {
    return;
  }
  ItemPosMap item_pos_map;
  bool is_bb_set = false;
  QRectF bb;
  if (!readDotFile(&item_pos_map, &is_bb_set, &bb)) {
    return;
  }
  doStabilization(item_pos_map, is_bb_set, &bb);
  if (is_bb_set) {
    mBoundingBox = bb;
  }
  placeEdges();
  mGraphicsScene->setSceneRect(mBoundingBox);

  // bugs in QGraphicsView make it easier to be pessimistic.
  mGraphicsScene->update(mBoundingBox);
} // void CBrowse::layout

void CBrowse::removeDeadNodes() {
  for (UInt32 i = 0; i < mNodeArray.size(); ++i) {
    CBrowseNode* cnode = mNodeArray[i];
    if (mLiveNodes.find(cnode) == mLiveNodes.end()) {
      NodeSet::iterator p = mNodesInScene.find(cnode);
      if (p != mNodesInScene.end()) {
        mNodesInScene.erase(p);
        mGraphicsScene->removeItem(cnode);
      }
    }
  }
}

bool CBrowse::isVisible(CGraph::Node* node) const {
  NodeMap::const_iterator p = mNodeMap.find(node);
  if (p == mNodeMap.end()) {
    return false;
  }
  CBrowseNode* cnode = p->second;
  return mLiveNodes.find(cnode) != mLiveNodes.end();
}

CBrowseNode* CBrowse::layoutNode(CGraph::Node* node) {
  CBrowseNode* cnode = mNodeMap[node];
  if (cnode == NULL) {
    cnode = new CBrowseNode(this, node);
    cnode->layout();
    cnode->putIndex(mNodeArray.size());
    mNodeArray.push_back(cnode);
    mNodeMap[node] = cnode;
  }
  else if (mLiveNodes.find(cnode) != mLiveNodes.end()) {
    return cnode;
  }
  mLiveNodes.insert(cnode);

  if (mNodesInScene.find(cnode) == mNodesInScene.end()) {
    mGraphicsScene->addItem(cnode);
    mNodesInScene.insert(cnode);
  }

  mNodeStrs << "  n" << cnode->getIndex() << " [label=\"\" width="
            << cnode->getWidth()/PIXELS_PER_INCH << " height="
            << cnode->getHeight()/PIXELS_PER_INCH << "];\n";
  cnode->mark();
  return cnode;
} // CBrowseNode* CBrowse::layoutNode

void CBrowse::addEdge(CBrowseNode* n1, CBrowseButton* b1,
                      CBrowseNode* n2, CBrowseButton* b2)
{
  Edge edge;
  edge.n1 = n1;
  edge.b1 = b1;
  edge.n2 = n2;
  edge.b2 = b2;

  // don't make n1->n2 if n2->n1 exists
  if (mEdges.find(edge) != mEdges.end()) {
    return;
  }
  mEdges.insert(edge);

  mEdgeStrs << "  n" << n1->getIndex() << " -> n" << n2->getIndex() << ";\n";
#if 0
  if (cnode->isShowingFanin()) {
    for (CGraph::NodeSetLoop p(node->loopFanin()); !p.atEnd(); ++p) {
      CGraph::Node* fanin = *p;
      CBrowseNode* cfanin = layoutNode(fanin);
    }
    for (UInt32 i = 0; i < node->numSubNodes(); ++i) {
      CGraph::Node* subnode = node->getSubNode(i);
      CBrowseNode* csubnode = layoutNode(subnode);
      mEdgeStrs << "  n" << csubnode->getIndex() << " -> n"
                << cnode->getIndex() << ";\n";
    }
  }
        
  if (cnode->isShowingFanout()) {
    for (CGraph::NodeSetLoop p(node->loopFanout()); !p.atEnd(); ++p) {
      CGraph::Node* fanout = *p;
      CBrowseNode* cfanout = layoutNode(fanout);
      mEdgeStrs << "  n" << cnode->getIndex() << " -> n"
                << cfanout->getIndex() << ";\n";
    }
  }
  return cnode;
#endif
} // void CBrowse::addEdge

void CBrowse::redraw(const QRectF& /*area*/) {
  //mGraphicsScene->update(area);
}

void CBrowse::stabilize(CBrowseNode* cnode) {
  mStabilize = cnode;
  mStabilizePos = cnode->pos();
}

void CBrowse::enterNode(CBrowseNode*) {
  ++mNodeEnterLeaveDepth;
  mGraphicsView->setDragMode(QGraphicsView::NoDrag);
}

void CBrowse::leaveNode(CBrowseNode*) {
  --mNodeEnterLeaveDepth;
  mGraphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
}

void CBrowse::dropText(const char* text) {
  // if 'text' comes from a hierarchy browser, it may have multiple
  // nodes seperated by newlines
  bool do_layout = false;
  UtString errmsg;
  UInt32 num_not_found = 0;
  for (StrToken tok(text, "\n"); !tok.atEnd(); ++tok) {
    const CarbonDBNode* node = carbonDBFindNode(mDB, *tok);
    if (node != NULL) {
      if (! do_layout) {
        saveState();
        do_layout = true;
      }
      mFocusNodes.insert(node);
    }
    else {
      errmsg << *tok << "\n";
      ++num_not_found;
    }
  }
  if (do_layout) {
    layout();
  }
  else {
    UtString buf("Cannot find node");
    if (num_not_found > 1) {
      buf << "s";
    }
    buf << ":\n\n" << errmsg;
    mCQt->warning(mGraphicsView, buf.c_str());
  }
} // void CBrowse::dropText

void CBrowse::clearNodes() {
  saveState();
  mFocusNodes.clear();
  layout();
}

void CBrowse::nextView() {
  if ((mNavigationStackIndex + 1) < mNavigationStack.size()) {
    ++mNavigationStackIndex;
    restoreState(*mNavigationStack[mNavigationStackIndex]);
    enableButtons();
  }
}

void CBrowse::prevView() {
  if (mNavigationStackIndex > 0) {
    if (mNavigationStackIndex == mNavigationStack.size()) {
      // special case the first time we call 'prev' after doing
      // manual navigation, so that 'next' becomes valid to call
      saveState();
      --mNavigationStackIndex;
    }
    --mNavigationStackIndex;
    restoreState(*mNavigationStack[mNavigationStackIndex]);
    enableButtons();
  }
}

void CBrowse::State::clear() {
  for (UInt32 i = 0; i < mNodeStates.size(); ++i) {
    CBrowseNode::State* nstate = mNodeStates[i];
    delete nstate;
  }
  mNodeStates.clear();
  mFocusNodes.clear();
  for (UInt32 i = 0; i < mLines.size(); ++i) {
    QLineF* line = mLines[i];
    delete line;
  }
  mLines.clear();
  for (UInt32 i = 0; i < mSplines.size(); ++i) {
    QPainterPath* spline = mSplines[i];
    delete spline;
  }
  mSplines.clear();
}

void CBrowse::enableButtons() {
  mNextAct->setEnabled((mNavigationStackIndex + 1) < mNavigationStack.size());
  mPrevAct->setEnabled(mNavigationStackIndex > 0);
  mClearAct->setEnabled(! mFocusNodes.empty());
}

#define MAX_STATES 30

void CBrowse::saveState() {
  // If we are currently in the middle of the navigation stack,
  // then a saveState() call will eliminate the rest of the stack
  for (UInt32 i = mNavigationStackIndex; i < mNavigationStack.size(); ++i) {
    State* state = mNavigationStack[i];
    delete state;
  }
  mNavigationStack.resize(mNavigationStackIndex);

  // If we are trying to save off the max-size of the stack, then
  // eliminate the first element
  if (mNavigationStackIndex == MAX_STATES) {
    State* state = mNavigationStack[0];
    mNavigationStack.remove(state); // order N.  Won't be an issue as N<=30
    delete state;
    mNavigationStackIndex = MAX_STATES - 1;
  }
  State* state = new State;
  mNavigationStack.push_back(state);
  ++mNavigationStackIndex;
  enableButtons();

  state->mFocusNodes = mFocusNodes;

  for (NodeSet::iterator p = mLiveNodes.begin(), e = mLiveNodes.end();
       p != e; ++p)
  {
    CBrowseNode* cnode = *p;
    CBrowseNode::State* nstate = new CBrowseNode::State;
    cnode->saveState(nstate);
    state->mNodeStates.push_back(nstate);
  }

  for (UInt32 i = 0; i < mEdgeItems.size(); ++i) {
    QGraphicsItem* item = mEdgeItems[i];
    QGraphicsLineItem* lineitem = qgraphicsitem_cast<QGraphicsLineItem*>(item);
    QGraphicsPathItem* pathitem = qgraphicsitem_cast<QGraphicsPathItem*>(item);
    if (lineitem != NULL) {
      QLineF line = lineitem->line();
      state->mLines.push_back(new QLineF(line));
    }
    else if (pathitem != NULL) {
      QPainterPath path = pathitem->path();
      state->mSplines.push_back(new QPainterPath(path));
    }
  }

  state->mBoundingBox = mBoundingBox;
  state->mXScroll = mGraphicsView->horizontalScrollBar()->value();
  state->mYScroll = mGraphicsView->verticalScrollBar()->value();
} // void CBrowse::saveState

void CBrowse::restoreState(const State& state) {
  clearLayoutStructures();
  for (UInt32 i = 0; i < state.mNodeStates.size(); ++i) {
    CBrowseNode::State* nstate = state.mNodeStates[i];
    nstate->restore();
    CBrowseNode* cnode = nstate->getNode();
    mLiveNodes.insert(cnode);
    if (mNodesInScene.find(cnode) == mNodesInScene.end()) {
      mGraphicsScene->addItem(cnode);
      mNodesInScene.insert(cnode);
    }
  }
  removeDeadNodes();

  mFocusNodes = state.mFocusNodes;
  mBoundingBox = state.mBoundingBox;

  for (UInt32 i = 0; i < state.mLines.size(); ++i) {
    QLineF* line = state.mLines[i];
    QGraphicsItem* item = mGraphicsScene->addLine(*line);
    item->setZValue(1.0);       // draw edges over nodes
    mEdgeItems.push_back(item);
  }
  for (UInt32 i = 0; i < state.mSplines.size(); ++i) {
    QPainterPath* spline = state.mSplines[i];
    QGraphicsItem* item = mGraphicsScene->addPath(*spline);
    item->setZValue(1.0);       // draw edges over nodes
    mEdgeItems.push_back(item);
  }

  mGraphicsView->horizontalScrollBar()->setValue(state.mXScroll);
  mGraphicsView->verticalScrollBar()->setValue(state.mYScroll);

  // bugs in QGraphicsView make it easier to be pessimistic.
  mGraphicsScene->setSceneRect(mBoundingBox);
  mGraphicsScene->update(mBoundingBox);
} // void CBrowse::restoreState


QSize CBrowse::sizeHint() const {
  return QSize(640, 400);
}

void CBrowse::textEditClosed(QDockWidget* dock) {
  TextEdit* textEdit = (TextEdit*)(dock->widget());
  INFO_ASSERT(textEdit, "only care about text editors -- errant signal?");
  const char* filename = mFileEditorReverseMap[textEdit];
  INFO_ASSERT(filename, "could not find text editor file on destruction");
  mFileEditorMap.erase(filename);
  mFileEditorReverseMap.erase(textEdit);
}

// we should keep the master text editor map in the main window
// so that text windows can be shared

void CBrowse::popupSource(const SourceLocator& loc) {
#if 0
  if (0 /* currently docked? */) {
    mHBrowser->popupSource(loc);
  }
  else
#endif
  {
    const char* filename = loc.getFile();
    TextEdit* textEdit = mFileEditorMap[filename];
    if (textEdit == NULL) {
      textEdit = new TextEdit(filename, mCQt);
      mFileEditorMap[filename] = textEdit;
      mFileEditorReverseMap[textEdit] = filename;
      CDockWidget *dock = new CDockWidget(filename, this);
      dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea |
        Qt::BottomDockWidgetArea);
      dock->setFeatures(QDockWidget::AllDockWidgetFeatures);
      dock->setWidget(textEdit);
      addDockWidget(Qt::BottomDockWidgetArea, dock);
      CQT_CONNECT(dock, closed(QDockWidget*),
                  this, textEditClosed(QDockWidget*));
    }

    QWidget* dock = textEdit->parentWidget();
    dock->show();
    textEdit->gotoLine(loc.getLine());
  }
}
