/******************************************************************************
 Copyright (c) 2006-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

// select prog/debug/both


#include "MemEdit.h"
#include <QTreeWidgetItem>
#include <QVBoxLayout>
#include <QStringList>
#include <QTimer>
#include <cassert>
#include "util/SourceLocator.h"
#include "util/UtIStream.h"
#include "util/AtomicCache.h"
#include "util/UtStringArray.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtStringUtil.h"
#include "util/Zstream.h"
#include "gui/CHexSpinBox.h"
#include "gui/CFilenameEdit.h"
#include "textedit.h"
#include "gui/CDragDropTreeWidget.h"
#include "cfg/CarbonCfg.h"
#include <QtGui>
#include <QLabel>
#include "ApplicationContext.h"
#include "MaxSimApplicationContext.h"

#define APP_NAME "Carbon Component Wizard"

struct CarbonMemEdit::MemInitHelper
{
  CARBONMEM_OVERRIDES
  
  MemInitHelper(CQtContext* cqt, CarbonMemEdit* memEdit) : 
    mCQt(cqt), mMemEdit(memEdit)
  {
    mInitFile = mCQt->filenameEdit("mem_init_file", true,
                                   "Memory Initialization File ($readmmemh format)",
                                   "*.*");
    mInitFile->setEnabled(false);
    mReadmemType = mCQt->enumBox<CarbonCfgReadmemTypeIO>("readmem_type");
    mReadmemType->setEnabled(false);

    mBaseAddr = mCQt->hexSpinBox("base_addr");
    mBaseAddr->setSingleStep(0x10000);
    DynBitVector baseAddrMax(64);
    baseAddrMax.set();
    mBaseAddr->setMaximum(baseAddrMax);
    mBaseAddr->setValue(0x0);
    mBaseAddr->setEnabled(false);
    
    mBaseAddrLabel = new QLabel("Address Offset:");
    mBaseAddrLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    mEslPorts = mCQt->comboBox("Preload_esl_port");
    mEslPorts->setEnabled(false);
    mEslPortsLabel = new QLabel("ESLPort:");
    mEslPortsLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

    mLoadByProgram = mCQt->radioButton("program_preload", "Program Preload");
    mLoadByProgram->setEnabled(false);
    
    mLoadByDatFile = mCQt->radioButton("datfile_init", "Initialization File");
    mLoadByDatFile->setEnabled(false);
    
    mNoLoad = mCQt->radioButton("no_meminit", "No Initialization");
    mNoLoad->setChecked(true);
    mNoLoad->setEnabled(false);
    
    CQT_CONNECT(mInitFile, filenameChanged(const char*),
                mMemEdit, changeInitFile(const char*));
    CQT_CONNECT(mReadmemType, currentIndexChanged(int),
                mMemEdit, changeReadmemType(int));
    CQT_CONNECT(mBaseAddr, valueChanged(const DynBitVector&),
                mMemEdit, changeBaseAddr(const DynBitVector&));
    CQT_CONNECT(mEslPorts, highlighted(const QString&), 
                mMemEdit, changeEslPort(const QString&));
    CQT_CONNECT(mNoLoad, toggled(bool), mMemEdit, noLoadToggle(bool));
    CQT_CONNECT(mLoadByProgram, toggled(bool), mMemEdit, loadByProgramToggle(bool));
    CQT_CONNECT(mLoadByDatFile, toggled(bool), mMemEdit, loadByDatFileToggle(bool));
  }
  

  QWidget* buildLayout()
  {
    QLayout* rbuttons = CQt::vbox(mLoadByProgram, mLoadByDatFile, mNoLoad);
    //    QWidget* initType = CQt::makeGroup("Type", rbuttons, true);
    
    // Memory readmemh load attributes
    QLayout* ifile = CQt::hbox(mInitFile, mReadmemType);  
    
    // Program Preload attributes
    QLayout* baseaddr_box = CQt::hbox(mEslPortsLabel, mEslPorts, mBaseAddrLabel, mBaseAddr);
    //    QWidget* baseaddr = CQt::makeGroup("Base Address", baseaddr_box, true);
    
    QLayout* initAttribBox = CQt::vbox(CQt::makeWidget(baseaddr_box), 
                                       CQt::makeWidget(ifile));
    
    //    QWidget* initAttribs = CQt::makeGroup("Attributes", initAttribBox, true);
    QLayout* progControls = CQt::hbox(CQt::makeWidget(rbuttons), 
                                      CQt::makeWidget(initAttribBox));
    
    return CQt::makeGroup("Memory Initialization", progControls, true);
  }

  void setMemInitWidgets(bool ena, CarbonCfg* cfg, CarbonCfgMemory* mem)
  {
    // repopulate the eslports if enabling
    mEslPorts->clear();
    if (ena && mem)
    {
      for (UInt32 i = 0, num = cfg->numXtorInstances(); i < num; ++i)
      {
        CarbonCfgXtorInstance* xtorInst = cfg->getXtorInstance(i);
        if (xtorInst->getType()->hasWriteDebug())
          mEslPorts->addItem(xtorInst->getName());
      }

      /*
        Set the current index of the eslport to the current esl port
        name saved in the mem, if it can be found. Makes it look like
        the wizard remembers the text, even if it isn't active.
      */
      (void) findAndSetEslPort(mem, false);
    }
    
    /*
      gray out the current filename in the init file box, even if the
      init file is not used. I'm not married to this, but I do like
      the look of the gui when disabling previously active text. It
      gives the user the notion that the gui is remembering what you
      entered.
      For example, if you click on Initialization file, enter a
      filename and choose readmemb, then you click on Program
      Initialization, keeping the inactive (grayed out) text in the
      init file box looks good, imho, especially when you click a
      different memory and then click back on the current memory.
    */    
    if (mem)
    {
      mInitFile->putFilename(mem->getInitFile());
      // same for the readmem type
      mReadmemType->setCurrentIndex((int) mem->getReadmemType());
      
      // Set the base addr to either 0 or the last value entered for
      // this mem
      // The Base Addr is really a "minimum access unit address" so convert that to bytes before display
      DynBitVector bv(64, (mem->getBaseAddr() * mem->getMAU()));
      mBaseAddr->setValue(bv);
    }
    else
    {
      mInitFile->putFilename("");
      UtString errMsg;
      INFO_ASSERT(mBaseAddr->setValue("", &errMsg), "Unable to clear base addr spinbox.");

    }
    
    // Now disable the init file. It may be re-enabled when we switch
    // on the mem init type.
    mInitFile->setEnabled(false);
    mReadmemType->setEnabled(false);
    mBaseAddr->setEnabled(false);
    
    // Enable/Disable the radio buttons
    mLoadByProgram->setEnabled(ena);
    mLoadByDatFile->setEnabled(ena);
    mNoLoad->setEnabled(ena);

    if (ena && (mem != NULL)) {
      switch(mem->getMemInitType())
      {
      case  eCarbonCfgMemInitNone:
        activateNoInitButton();
        break;
      case eCarbonCfgMemInitProgPreload:
        if (! activateProgLoadButton(mem))
        {
          // The port went away! Set to no init
          activateNoInitButton();
        }
        break;
      case eCarbonCfgMemInitReadmem:
        activateDatFileButton(mem);
        break;
      }
    }
  }

  void activateNoInitButton()
  {
    enableNoInitButton();
    mNoLoad->setChecked(true);
  }

  void activateDatFileButton(CarbonCfgMemory* mem)
  {
    enableDatFileButton(mem);
    mLoadByDatFile->setChecked(true);
  }

  bool activateProgLoadButton(CarbonCfgMemory* mem)
  {
    bool ret = enableProgLoadButton(mem);
    if (ret)
      mLoadByProgram->setChecked(true);
    return ret;
  }

  void enableNoInitButton()
  {
    putEnableDatFileWidgets(false);
    putEnableProgLoadWidgets(false);
  }

  void enableDatFileButton(CarbonCfgMemory* mem)
  {
    putEnableDatFileWidgets(true);
    putEnableProgLoadWidgets(false);

    if (mem)
    {
      const char* initFile = mem->getInitFile();
      mInitFile->setEnabled(true);
      mInitFile->putFilename(initFile);
      bool enaMemType = *initFile != '\0';
      if (enaMemType)
      {
        mReadmemType->setEnabled(true);        
        mReadmemType->setCurrentIndex((int) mem->getReadmemType());
      }
    }
    else
    {
      // to combat a potential hysteresis. set the init file and
      // readmemtype to defaults. As far as I can tell, we will never
      // get here, unless this is called out-of-context.
      mInitFile->putFilename("");
      mReadmemType->setCurrentIndex(0);
    }
  }
  
  bool enableProgLoadButton(CarbonCfgMemory* mem)
  {
    if (mem && (mEslPorts->count() == 0))
    {
      mCQt->warning(mMemEdit, tr("No ESL Slave Transactors with debug capability instantiated."));
      return false;
    }
    
    putEnableDatFileWidgets(false);
    putEnableProgLoadWidgets(true);

    if (mem)
    {
      // The Base Addr is really a "minimum access unit address" so convert that to bytes before display
      DynBitVector curVal(64, (mem->getBaseAddr() * mem->getMAU()));
      mBaseAddr->setValue(curVal);
      // Note that index will not be -1
      int index = findAndSetEslPort(mem, true);
      INFO_ASSERT(index >= 0, "EslPort Combo box out of sync with ccfg.");
      
      // update the CarbonCfgMem in case the index changed
      UtString eslPort;
      eslPort << mEslPorts->itemText(index);
      mem->putSystemAddressESLPortName(eslPort.c_str());
    }
    else
    {
      // combat potential hysteresis
      mBaseAddr->setValue(0);
      // Note that we can't get here unless mEslPorts->count() > 0
      mEslPorts->setCurrentIndex(0);
    }
    return true;
  }
  

  int findAndSetEslPort(CarbonCfgMemory* mem, bool warnAndDefaultIfNotFound)
  {
    int index = 0;
    UtString savedEslPort(mem->getSystemAddressESLPortName());
    if (! savedEslPort.empty())
    {
      index = mEslPorts->findText(savedEslPort.c_str());
      if ((index == -1) && warnAndDefaultIfNotFound)
      {
        UtString msg;
        msg << "The ESL Slave Transactor '" << savedEslPort << "' associated with this program memory is no longer instantiated.";
        mCQt->warning(mMemEdit, tr(msg.c_str()));

        // set the index to 0
        index = 0;
      }
    }

    // index could be -1 here.
    if (index >= 0)
      mEslPorts->setCurrentIndex(index);
    return index;
  }

  void putEnableProgLoadWidgets(bool ena)
  {
    mBaseAddr->setEnabled(ena);
    mEslPorts->setEnabled(ena);
  }

  void putEnableDatFileWidgets(bool ena)
  {
    mInitFile->setEnabled(ena);
    mReadmemType->setEnabled(ena);
  }


  void changeInitFileSlotCalled(const char* str)
  {
    mReadmemType->setEnabled(*str != '\0');
  }


  CQtContext* mCQt;
  CarbonMemEdit* mMemEdit;

  CHexSpinBox* mBaseAddr;
  QComboBox* mEslPorts;
  
  QLabel* mBaseAddrLabel;
  QLabel* mEslPortsLabel;

  CFilenameEdit* mInitFile;
  QComboBox* mReadmemType;

  QRadioButton* mLoadByProgram;
  QRadioButton* mLoadByDatFile;
  QRadioButton* mNoLoad;
};

CarbonMemEdit::CarbonMemEdit(CQtContext* cqt,
                             CarbonCfgID cfg,
                             AtomicCache* atomicCache,
                             QAction* deleteAction, ApplicationContext* ctx)
  : mCfg(cfg),
    mAtomicCache(atomicCache),
    mDeleteAction(deleteAction),
    mCQt(cqt),
    mCtx(ctx)
{
  CQtRecordBlocker blocker(cqt);

  QStringList headers;
  mMemTree = cqt->dragDropTreeWidget("memTree", true);
  mMemTree->setSelectionMode(QAbstractItemView::ExtendedSelection);
  mMemTree->setAlternatingRowColors(true);
  mMemTree->setColumnCount(7);
  headers.clear();
  headers << "Name";
  headers << "RTL Path";
  headers << "Max Addr";
  headers << "Init File";
  headers << "Disassembler Name";
  headers << "Disassembly Name";
  headers << "Comment";
  mMemTree->setHeaderLabels(headers);
  CQT_CONNECT(mMemTree, dropReceived(const char*,QTreeWidgetItem*),
              this, dropMemory(const char*));
  
  mName = cqt->lineEdit("mem_name");
  mName->setEnabled(false);

  mComment = cqt->lineEdit("mem_comment");
  mComment->setEnabled(false);


  mDisassemblyName = cqt->lineEdit("mem_diss_name");
  mDisassemblyName->setEnabled(false);

  mDisassemblerName = cqt->lineEdit("mem_disler_name");
  mDisassemblerName->setEnabled(false);



  mMemMaxAddr = cqt->hexSpinBox("max_addr");
  mMemMaxAddr->setSingleStep(0x10000);
  mMemMaxAddr->setMaximum(0x7fffffff);


  mAddAllMemories = cqt->pushButton("add_all_mem", "Add All Memories");

  // unsigned int bitsPerMau
  mMemMaxAddr->setEnabled(false);
    
  CQT_CONNECT(mMemTree, selectionChanged(), this, changeMemTreeSel());
  CQT_CONNECT(mName, textChanged(const QString&),
              this, changeName(const QString&));
  CQT_CONNECT(mDisassemblyName, textChanged(const QString&),
              this, changeDisassemblyName(const QString&));
  CQT_CONNECT(mDisassemblerName, textChanged(const QString&),
              this, changeDisassemblerName(const QString&));
  CQT_CONNECT(mComment, textChanged(const QString&),
              this, changeComment(const QString&));
  CQT_CONNECT(mMemMaxAddr, valueChanged(int), this, changeMaxAddr(int));
  CQT_CONNECT(mAddAllMemories, clicked(), this, addAllMemories());

  mMemInitHelper = new MemInitHelper(cqt, this);
  mDB = NULL;
  mDisable = false;
  mDeleteActive = false;
  mSelectedMem = NULL;
  mTotalMemories = -1;
} // CarbonMemEdit::CarbonMemEdit

CarbonMemEdit::~CarbonMemEdit() {
  clear();
  delete mMemInitHelper;
}

void CarbonMemEdit::populate(CarbonDB* carbonDB) {
  mDB = carbonDB;
}

void CarbonMemEdit::clear() {
  mItemMemoryMap.clear();
  mMemoryNameMap.clear();
  mMemTree->clear();
}

void CarbonMemEdit::read() {
  clear();
  // First get memories from top level component
  for (unsigned int i = 0, n = carbonCfgNumMemories(mCfg); i < n; ++i) {
    CarbonCfgMemoryID mem = carbonCfgGetMemory(mCfg, i);
    addMem(mem);
  }

  // Then loop through the sub components
  for(unsigned int sc = 0; sc < mCfg->numSubComponents(); ++sc) {
    CarbonCfg* sub = mCfg->getSubComponent(sc);
    for (unsigned int i = 0; i < sub->numMemories(); ++i) {
      CarbonCfgMemoryID mem = sub->getMemory(i);
      addMem(mem);
    }
  }
}

#if 0
void CarbonMemEdit::changeRegRadix(int index) {
  switch (index) {
  case 0: applyChanges(NULL, NULL, 16); break;
  case 1: applyChanges(NULL, NULL, 10); break;
  case 2: applyChanges(NULL, NULL, 8); break;
  case 3: applyChanges(NULL, NULL, 2); break;
  }    
}
#endif

void CarbonMemEdit::changeMemTreeSel() {
  QTreeItemList sel = mMemTree->selectedItems();
  int numSelected = 0;
  QTreeWidgetItem* item = NULL;
  CarbonCfgMemory* mem = NULL;

  // Find the currently selected debug group/name and populate the editing
  // widgets
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    item = *p;
    mem = mItemMemoryMap.find(item);
    ++numSelected;
  }

  setMemWidgets(numSelected, mem);
}

void CarbonMemEdit::setMemWidgets(UInt32 numSelected, CarbonCfgMemory* mem) {
  CQtRecordBlocker blocker(mCQt);
  mSelectedMem = NULL;

  // For the moment, we let's only allow the user to change parameters
  // for a one mem at a time
  bool ena = numSelected > 0;
  mName->setEnabled(ena);
  mComment->setEnabled(ena);
  mMemMaxAddr->setEnabled(ena);
  mDisassemblerName->setEnabled(ena);
  mDisassemblyName->setEnabled(ena);


  if (ena && (mem != NULL)) {
    mMemMaxAddr->setValue(mem->getMaxAddrs());
    mName->setText(mem->getName());
    mComment->setText(mem->getComment());
    mDisassemblerName->setText(mem->getParent()->getCadi()->getDisassemblerName());
    if(mem->getParent()->isTopLevel())
      mDisassemblyName->setText("");
    else
      mDisassemblyName->setText(mem->getParent()->getCompName());
  }
  else
  {
    UtString errMsg;
    INFO_ASSERT(mMemMaxAddr->setValue("", &errMsg), "Unable to clear max addr spinbox.");
    mName->setText("");
    mComment->setText("");
  }
  
  if (mDeleteAction)
    mDeleteAction->setEnabled(ena);

  mDeleteActive = ena;
  mDisable = false;
  mSelectedMem = mem;

  // This has to be called after setting up mSelectedMem to deal with
  // radio button toggles.
  mMemInitHelper->setMemInitWidgets(ena, mCfg, mem);
} // void CarbonMemEdit::setMemWidgets

QWidget* CarbonMemEdit::buildLayout() {
  // Master trees of registers and memories
  QWidget* memories = mCQt->makeGroup("Memories (drag and drop from Design Hierarchy)",
                                      "mem-tree", mMemTree , false);

  // Memory attributes, user comment
  QWidget* name = CQt::makeGroup("Name", mName, true);
  QWidget* maxAddr = CQt::makeGroup("Maximum Address", mMemMaxAddr, true);
  QLayout* name_maxAddr = CQt::hbox(name, maxAddr);
  QWidget* comment = CQt::makeGroup("Comment", mComment, true);
  QLayout* comment_addAll = CQt::hbox(comment, mAddAllMemories);
  QWidget* top_ctrls = CQt::hSplit(CQt::makeWidget(name_maxAddr), CQt::makeWidget(comment_addAll));

  QWidget* dis1 = CQt::makeGroup("Disassembly", mDisassemblyName);
  QWidget* dis2 = CQt::makeGroup("Disassembler", mDisassemblerName);

  QLayout* assemblies = CQt::hbox(dis1, dis2);

  
  // Memory initialization options
  QWidget* bot_ctrls = mMemInitHelper->buildLayout();

  QLayout* layout = CQt::vbox();
  *layout << memories << top_ctrls << assemblies << bot_ctrls;
  return CQt::makeWidget(layout);
} // QWidget* CarbonMemEdit::buildLayout


bool CarbonMemEdit::isDeleteActive() const {
  return mDeleteActive;
}

void CarbonMemEdit::deleteCurrent() {
  // First, delete all the item group items and their children
  QTreeItemList sel = mMemTree->selectedItems();
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;
    CarbonCfgMemory* mem = mItemMemoryMap.find(item);
    int idx = mMemTree->indexOfTopLevelItem(item);
    INFO_ASSERT(idx != -1, "cannot find item in tree");
    mMemTree->takeTopLevelItem(idx);
    mItemMemoryMap.unmap(item);
    delete item;
    size_t erased = mMemoryNameMap.erase(mem->getPath());
    INFO_ASSERT(erased, "Could not find memory in name map");
    mCfg->removeMemory(mem);
    if (mCtx)
      mCtx->setDocumentModified(true);
  }

  // There should be no more selected items
  sel = mMemTree->selectedItems();
  INFO_ASSERT(sel.empty(), "Should be no more selected items");

  setMemWidgets(0, NULL);
  enableWidgets();
} // void CarbonMemEdit::deleteCurrent

void CarbonMemEdit::dropMemory(const char* path) {
  mMemTree->deselectAll();
  if (mDB == NULL) {
    mCQt->warning(this, tr("Design Database not loaded"));
    return;
  }

  UtString warning;
  UtIStringStream is(path);
  UtString buf;
  UtString errmsg;
  QTreeWidgetItem* item = NULL;
  while (is.getline(&buf)) {
    StringUtil::strip(&buf, "\n");
    path = buf.c_str();

    const CarbonDBNode* node = carbonDBFindNode(mDB, path);
    if (node == NULL) {
      errmsg << "No such net in the design: " << path << "\n";
    }
    else if (! carbonDBIs2DArray(mDB, node)) {
      errmsg << "Net " << path << " is not a memory\n";
    }
    else if (! carbonDBIsVisible(mDB, node)) {
      errmsg << "Net " << path << " is not visible -- declare it observable\n";
    }
    else if (mMemoryNameMap.find(path) != mMemoryNameMap.end()) {
      errmsg << "Duplicate entry for memory " << path << "\n";
    }
    else {
      item = addMemoryNode(node);
      INFO_ASSERT(item, "Failed to drop memory");
      if (mCtx)
        mCtx->setDocumentModified(true);
    }
  } // while
  if (!errmsg.empty()) {
    mCQt->warning(this, errmsg.c_str());
  }
  if (item != NULL) {
    mMemTree->setItemSelected(item, true);
    setMemWidgets(1, mItemMemoryMap.find(item));
    mMemTree->scrollToItem(item);
  }
  enableWidgets();
} // void CarbonMemEdit::dropMemory

QTreeWidgetItem* CarbonMemEdit::addMemoryNode(const CarbonDBNode* node) {
  UtString path(carbonDBNodeGetFullName(mDB, node));
  if (mMemoryNameMap.find(path) != mMemoryNameMap.end()) {
    return NULL;
  }

  UtString name;
  mCfg->getUniqueMemName(&name, carbonDBNodeGetLeafName(mDB, node));
  SInt32 left = carbonDBGet2DArrayLeftAddr(mDB, node);
  SInt32 right = carbonDBGet2DArrayRightAddr(mDB, node);
  UInt32 maxAddr = std::abs(right - left); // this is not 'num addrs'
  UInt32 width = carbonDBGetWidth(mDB, node);

  CarbonCfgMemory* mem = mCfg->addMemory(path.c_str(), name.c_str(),
                                         "", // init file
                                         maxAddr, width,
                                         eCarbonCfgReadmemh,
                                         ""); // comment
  QTreeWidgetItem* item = addMem(mem);
  return item;
}

void CarbonMemEdit::setTreeText(CarbonCfgMemory* mem) {
  QTreeWidgetItem* item = mItemMemoryMap.find(mem);
  INFO_ASSERT(item, "Cannot find item for memory");
  item->setText(0, mem->getName());
  item->setText(1, mem->getPath());
  UtString buf;
  UtOStringStream os(&buf);
  os << UtIO::hex << mem->getMaxAddrs();
  item->setText(2, buf.c_str());
  buf.clear();


  switch(mem->getMemInitType())
  {
  case  eCarbonCfgMemInitNone:
    break;
  case eCarbonCfgMemInitProgPreload:
    buf << "<program>";
    break;
  case eCarbonCfgMemInitReadmem:
    {
      const char* initFile = mem->getInitFile();
      if (*initFile != '\0') {
        buf << mem->getInitFile() << " " << mem->getReadmemTypeStr();
      }
    }
    break;
  }
  item->setText(3, buf.c_str());

  item->setText(4, mem->getParent()->getCadi()->getDisassemblerName());
  if(mem->getParent()->isTopLevel())
    item->setText(5, "");
  else
    item->setText(5, mem->getParent()->getCompName());

  item->setText(6, mem->getComment());
}

QTreeWidgetItem* CarbonMemEdit::addMem(CarbonCfgMemory* mem) {
  QTreeWidgetItem* item = new QTreeWidgetItem(mMemTree);
  mItemMemoryMap.map(item, mem);
  mMemoryNameMap[mem->getPath()] = mem;
  setTreeText(mem);
  return item;
}

void CarbonMemEdit::changeName(const QString& str) {
  if ((mSelectedMem != NULL) && (str != mSelectedMem->getName())) {
    UtString buf;
    buf << str;
    mCfg->changeMemoryName(mSelectedMem, buf.c_str());
    setTreeText(mSelectedMem);
  }    
}

void CarbonMemEdit::changeDisassemblyName(const QString& str)
{
  if (mSelectedMem && mDisassemblyName != NULL)
  {
    // Get Source Component
    CarbonCfg* srcComp = mSelectedMem->getParent();

    // If the name hasn't changed, don't bother.
    if (str != srcComp->getCompName() && !(str == "" && srcComp->isTopLevel())) {
      CarbonCfg* dstComp = NULL;

      // If disassembly name changed to an empty string, move the memory to the top level component
      if (str == "") dstComp = mCfg;

      // If not empty string, it is a sub component
      else {
        UtString disassemblyName;
        disassemblyName << str;

        // See if there already is a sub component with this name
        dstComp = mCfg->findSubComponent(disassemblyName.c_str());
        
        // If not found create a new component
        if (dstComp == NULL) { 
          dstComp = mCfg->addSubComp(disassemblyName.c_str());
          INFO_ASSERT(dstComp, "Adding sub component failed.");
          // We also need to update the disassembler name for the new component
          dstComp->getCadi()->putDisassemblerName(srcComp->getCadi()->getDisassemblerName());
        }
      }
      // Move memory to new component
      dstComp->moveMemory(mSelectedMem);

      // If moving the memory resulted in the source component now being empty, we need to remove it
      // unless it is the top level component, then we want to keep it
      if (srcComp != mCfg && srcComp->numMemories() == 0 && srcComp->numRegisters() == 0)
        mCfg->removeSubCompRecursive(srcComp);
    }
    
    // Update GUI widgets
    setTreeText(mSelectedMem);
    } 
}

void CarbonMemEdit::changeDisassemblerName(const QString& str) {
  if (mSelectedMem && mDisassemblerName != NULL)
  {
    UtString buf;
    buf << str;
    // This will actually update the disassembler name for all memories
    // in the current sub component since disassembler name is now
    // set per sub component.
    CarbonCfg* comp = mSelectedMem->getParent();
    comp->getCadi()->putDisassemblerName(buf.c_str());

    // Update GUI for all memories in this sub component
    for(unsigned int i = 0; i < comp->numMemories(); ++i)
      setTreeText(comp->getMemory(i));
  }    
}

void CarbonMemEdit::changeComment(const QString& str) {
  if (mSelectedMem != NULL) {
    UtString buf;
    buf << str;
    mSelectedMem->putComment(buf.c_str());
    setTreeText(mSelectedMem);
  }    
}

void CarbonMemEdit::changeBaseAddr(const DynBitVector& value) {
  if (mSelectedMem != NULL) {
    // The Base Addr is really a "minimum access unit address" so convert that from bytes before save
    mSelectedMem->putBaseAddr((value.llvalue()/mSelectedMem->getMAU()));
    setTreeText(mSelectedMem);
  }    
}

void CarbonMemEdit::changeInitFile(const char* str) {
  if (mSelectedMem != NULL) {
    mSelectedMem->putInitFile(str);
    setTreeText(mSelectedMem);
    mMemInitHelper->changeInitFileSlotCalled(str);
  }    
}

void CarbonMemEdit::changeReadmemType(int index) {
  if (mSelectedMem != NULL) {
    mSelectedMem->putReadmemType((CarbonCfgReadmemType) index);
    setTreeText(mSelectedMem);
  }    
}


void CarbonMemEdit::changeEslPort(const QString& portName) {
  if (mSelectedMem != NULL) {
    UtString name;
    name << portName;
    mSelectedMem->putSystemAddressESLPortName(name.c_str());
    setTreeText(mSelectedMem);
  }    
}

void CarbonMemEdit::changeMaxAddr(int val) {
  if (mSelectedMem != NULL) {
    mSelectedMem->putMaxAddrs(val);
    setTreeText(mSelectedMem);
  }    
}

void CarbonMemEdit::addAllMemories() {
  CQtRecordBlocker blocker(mCQt);
  mMemTree->deselectAll();
  addAllMemoriesHelper(false);
  enableWidgets();
}
  
void CarbonMemEdit::enableWidgets() {
  mAddAllMemories->setEnabled(addAllMemoriesHelper(true) != 0);
}

UInt32 CarbonMemEdit::addAllMemoriesHelper(bool dry_run) {
  // Walk through the design and add memory entries for every 2-D
  // array that is found, unless it's already in.
  UInt32 total = 0;
  if (mDB != NULL) {
    CarbonDBNodeIter* iter = carbonDBLoopDesignRoots(mDB);
    const CarbonDBNode* node = NULL;
    while ((node = carbonDBNodeIterNext(iter)) != NULL) {
      total += findMemories(node, dry_run);
    }
    carbonDBFreeNodeIter(iter);
  }
  return total;
}

UInt32 CarbonMemEdit::findMemories(const CarbonDBNode* node, bool dry_run) {
  UInt32 total = 0;
  if (carbonDBIs2DArray(mDB, node)) {
    const char* leafName = carbonDBNodeGetLeafName(mDB, node);
    // exclude resynthesized memories
    if (*leafName != '$') {
      if (dry_run) {
        UtString path(carbonDBNodeGetFullName(mDB, node));
        if (mMemoryNameMap.find(path) == mMemoryNameMap.end()) {
          ++total;
        }
      }
      else {
        (void) addMemoryNode(node);
      }
    }
  }
  else {
    CarbonDBNodeIter* iter = carbonDBLoopChildren(mDB, node);
    while ((node = carbonDBNodeIterNext(iter)) != NULL) {
      total += findMemories(node, dry_run);
    }
  }
  return total;
} // UInt32 CarbonMemEdit::findMemories

void CarbonMemEdit::noLoadToggle(bool checked)
{
  if (checked)
  {
    mMemInitHelper->enableNoInitButton();
    if (mSelectedMem)
    {
      mSelectedMem->putMemInitType(eCarbonCfgMemInitNone);
      setTreeText(mSelectedMem);
    }
  }
}

void CarbonMemEdit::loadByProgramToggle(bool checked)
{
  if (mSelectedMem)
  {
    if (checked)
    {
      if (mMemInitHelper->enableProgLoadButton(mSelectedMem))
        mSelectedMem->putMemInitType(eCarbonCfgMemInitProgPreload);
      else
        // restore the state from before the button was pressed.
        mMemInitHelper->setMemInitWidgets(true, mCfg, mSelectedMem);
      setTreeText(mSelectedMem);
    }
  }
}

void CarbonMemEdit::loadByDatFileToggle(bool checked)
{
  if (checked && mSelectedMem)
  {
    mMemInitHelper->enableDatFileButton(mSelectedMem);

    mSelectedMem->putMemInitType(eCarbonCfgMemInitReadmem);
    setTreeText(mSelectedMem);
  }
}
