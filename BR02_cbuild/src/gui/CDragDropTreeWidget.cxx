/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CDragDropTreeWidget.h"
#include "gui/CQt.h"
#include "util/CarbonAssert.h"
#include "util/UtStringUtil.h"
#include "util/UtHashSet.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include <QMouseEvent>
#include <QtGui>




CDragDropTreeWidget::CDragDropTreeWidget(bool enaDrop, QWidget *parent)
  : QTreeWidget(parent),
    mDragState(eDragIdle),
    mDropState(eDropIdle),
    mSaveMousePress(NULL),
    mSaveItem(NULL),
    mEnableDrop(enaDrop),
    mEnableRecord(true),
    mEnableSelectSignal(true),
    mDropSiteItem(NULL),
    mDropColumnCount(0),
    mDropItemSelected(false),
    mDropEnableSelectSignal(false),
    mSortingEnabled(false)
{
  mDragImmediate = false;

  if (enaDrop) {
    setAcceptDrops(true);
    setDropIndicatorShown(true);
  }
  dumpObjectTree();
  CQT_CONNECT(this, itemSelectionChanged(), this, qtSelectionChangedHandler());
  CQT_CONNECT(header(), sectionClicked(int), this, onHeaderClicked(int));
  QTreeWidget::setSortingEnabled(false);
}

void CDragDropTreeWidget::mousePressEvent(QMouseEvent* e) {
  bool forwardEventToParent = true;

  int button = e->button();
  if (mDragImmediate || (!mDragData.empty() &&
      (mDragState == eDragIdle) &&
      ((button == Qt::LeftButton) || (button == Qt::MidButton))))
  {
    // QTreeItemList sel = selectedItems();

    // you have to have clicked over an item
    QTreeWidgetItem* item = itemAt(e->x(), e->y());

    if (item != NULL) {

      if (mDragImmediate) // Drag immediately?
      {
        if (!item->treeWidget()->isItemSelected(item))
          item->treeWidget()->setCurrentItem(item);
      }

      // If the button is either Left (Windows std) or Middle (Motif std),
      // start a drag, so that the app behaves the same way on Windows
      // and Linux, and isn't surprising on either.  If the middle
      // button is used, then we can begin the drag immediately on
      // the press.  For a left-button we have to wait till the
      // mouse has been moved outside of the widget, otherwise we
      // will disturb group-selects
      if (button == Qt::MidButton) {
        startDrag();
        forwardEventToParent = false;
      }


      // Left button starts a drag only if the item is selected already
      else if (item->treeWidget()->isItemSelected(item)) {
        mDragState = eLookForMove;
        //mSaveMousePress = new QMouseEvent(*e);
        mSaveItem = item;
      }
    }
  }
  
  if (forwardEventToParent) {
    QTreeWidget::mousePressEvent(e);
  }
} // void CDragDropTreeWidget::mousePressEvent

void CDragDropTreeWidget::startDrag() {
  mDrag = new QDrag(this);
  QMimeData* mimeData = new QMimeData;
  mimeData->setText(mDragData.c_str());
  mDrag->setMimeData(mimeData);
  mDragState = eDragStarted;
  (void) mDrag->start(Qt::CopyAction); // for now, don't care what result is
  mDragState = eDragIdle;
}

void CDragDropTreeWidget::mouseReleaseEvent(QMouseEvent* e) {
  // If a mouse-release event occurred, and we were thinking we
  // might have been doing a drag&drop but we didn't escape the
  // current item, we have to play the mouse press event first.
  if (mDragState == eLookForMove) {
    mSaveItem = NULL;
    //QTreeWidget::mousePressEvent(mSaveMousePress);
    //delete mSaveMousePress;
  }

  mDragState = eDragIdle;
  QTreeWidget::mouseReleaseEvent(e);
}

void CDragDropTreeWidget::mouseMoveEvent(QMouseEvent* e) {
  if (mDragState == eLookForMove) {
    // See if we've moved away from our selected item
    QTreeWidgetItem* item = itemAt(e->x(), e->y());
    if (item != mSaveItem) {
      // OK, do the D&D
      //delete mSaveMousePress;
      //mSaveMousePress = NULL;
      mSaveItem = NULL;
      startDrag();
    }
  }
  else {
    QTreeWidget::mouseMoveEvent(e);
  }
} // void CDragDropTreeWidget::mouseMoveEvent

void CDragDropTreeWidget::dragEnterEvent(QDragEnterEvent* event) {
  bool ignore = true;
  if (mEnableDrop) {
    const QMimeData* mimeData = event->mimeData();
    //UtIO::cerr() << "enter: trying drop\n";
    if (mimeData->hasText()) {
      //UtIO::cerr() << "enter: accepting drop\n";
      event->acceptProposedAction();
      animateDropSite(event->pos());
      ignore = false;
    }
    else {
      //UtIO::cerr() << "enter: mime has no text\n";
    }
  }
  if (ignore) {
    event->ignore();
  }
}

void CDragDropTreeWidget::dragMoveEvent(QDragMoveEvent* event) {
  bool ignore = true;
  if (mEnableDrop) {
    const QMimeData* mimeData = event->mimeData();
    //UtIO::cerr() << "move: trying drop\n";
    if (mimeData->hasText()) {
      //UtIO::cerr() << "move: accepting drop\n";
      event->acceptProposedAction();
      ignore = false;
      animateDropSite(event->pos());
    }
    else {
      //UtIO::cerr() << "move: mime has no text\n";
    }
  }
  if (ignore) {
    event->ignore();
  }
}

void CDragDropTreeWidget::dragLeaveEvent(QDragLeaveEvent*) {
  clearAnimation();
}

void CDragDropTreeWidget::animateDropSite(const QPoint& pos) {
  QTreeWidgetItem* item = itemAt(pos.x(), pos.y());
  emit mapDropItem(&item);
  if (mDropSiteItem != item) {
    clearAnimation();
    mDropSiteItem = item;
    if (item != NULL) {
      mDropItemSelected = isItemSelected(item);
      mDropColumnCount = std::min(DD_MAX_COLUMNS, columnCount());
      for (UInt32 i = 0; i < mDropColumnCount; ++i) {
        mDropSaveBrush[i] = item->background(i);
      }
      QColor dropHighlightColor = QColor(Qt::green);
      if (mDropItemSelected) {
        mDropEnableSelectSignal = mEnableRecord;
        mEnableSelectSignal = false;
        setItemSelected(item, false);
        dropHighlightColor = QColor(Qt::darkGreen);
      }
      for (UInt32 i = 0; i < mDropColumnCount; ++i) {
        item->setBackgroundColor(i, dropHighlightColor);
      }
    }
  }
}

void CDragDropTreeWidget::clearAnimation() {
  if (mDropSiteItem != NULL) {
    for (UInt32 i = 0; i < mDropColumnCount; ++i) {
      mDropSiteItem->setBackground(i, mDropSaveBrush[i]);
    }
    if (mDropItemSelected) {
      setItemSelected(mDropSiteItem, true);
      mEnableSelectSignal = mDropEnableSelectSignal;
    }
    mDropSiteItem = NULL;
  }
}

void CDragDropTreeWidget::dropEvent(QDropEvent* event) {
  clearAnimation();
  if (mEnableDrop && event->mimeData()->hasText()) {
    event->acceptProposedAction();
    QString str = event->mimeData()->text();
    UtString buf;
    buf << str;
    QPoint pos = event->pos();

    QTreeWidgetItem* item = itemAt(pos.x(), pos.y());
    emit dropReceived(buf.c_str(), item);
  }
  else {
    event->ignore();
  }
}

void CDragDropTreeWidget::replayDrop(const char* str, QTreeWidgetItem* item) {
  emit dropReceived(str, item);
}

void CDragDropTreeWidget::putDragData(const char* str) {
  if (str != NULL) {
    mDragData = str;
  }
  else {
    mDragData.clear();
  }
}

void CDragDropTreeWidget::deselectAll() {
  mEnableSelectSignal = false;
  QTreeItemList sel = selectedItems();
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;
    setItemSelected(item, false);
  }
  mEnableSelectSignal = true;
#if 0
  ItemArray nothing;
  emit selectionChanged(nothing);
#else
  emit selectionChanged();
#endif
}

// Intercept Qt's selection signals, and relay them only when the
void CDragDropTreeWidget::qtSelectionChangedHandler() {
  if (mEnableSelectSignal) {
    emitSelectionChanged();
  }
}

#if 0
static void sFindSelections(QTreeWidget* tree,
                            CDragDropTreeWidget::ItemArray* sel,
                            QTreeWidgetItem* item)
{
  if (tree->isItemSelected(item)) {
    sel->push_back(item);
  }
  for (int j = 0, nj = item->childCount(); j < nj; ++j) {
    QTreeWidgetItem* child = item->child(j);
    sFindSelections(tree, sel, child);
  }
}
#endif

void CDragDropTreeWidget::emitSelectionChanged() {
  emit selectionChanged();
}

void CDragDropTreeWidget::setItemSelected(QTreeWidgetItem* item, bool sel) {
  bool saveEna = mEnableRecord;
  mEnableRecord = false;
  QTreeWidget::setItemSelected(item, sel);
  mEnableRecord = saveEna;
}

void CDragDropTreeWidget::expandItem(QTreeWidgetItem* item) {
  bool saveEna = mEnableRecord;
  mEnableRecord = false;
  QTreeWidget::expandItem(item);
  mEnableRecord = saveEna;
}

void CDragDropTreeWidget::takeTopLevelItem(int idx) {
  bool saveEna = mEnableRecord;
  mEnableRecord = false;
  QTreeWidget::takeTopLevelItem(idx);
  mEnableRecord = saveEna;
}

void CDragDropTreeWidget::deleteItem(QTreeWidgetItem* item) {
  bool saveEna = mEnableRecord;
  mEnableRecord = false;
  deselectAll();
  QTreeWidgetItem* parent = item->parent();
  if (parent != NULL) {
    int childIndex = parent->indexOfChild(item);
    INFO_ASSERT(childIndex >= 0, "Cannot find item index of item");
    parent->takeChild(childIndex);
  }
  else {
    int childIndex = indexOfTopLevelItem(item);
    INFO_ASSERT(childIndex >= 0, "Cannot find item index of item");
    takeTopLevelItem(childIndex);
  }
  delete item;
  mEnableRecord = saveEna;
  deselectAll();
}

void CDragDropTreeWidget::setSortingEnabled(bool ena) {
  if (mSortingEnabled != ena) {
    mSortingEnabled = ena;
    header()->setClickable(ena);
    header()->setSortIndicatorShown(ena);
  }
}

void CDragDropTreeWidget::onHeaderClicked(int section) {
  if (mSortingEnabled) {
    model()->sort(section, qobject_cast<QHeaderView*>(sender())->sortIndicatorOrder());
  }
}

CQtDragDropWidgetControl::CQtDragDropWidgetControl(CDragDropTreeWidget* widget,
                         CQtContext* cqt)
  : mWidget(widget),
    mCQt(cqt)
{
  CQT_CONNECT(widget, itemSelectionChanged(), this, itemSelectionChanged());
  CQT_CONNECT(widget, itemExpanded(QTreeWidgetItem*),
              this, itemExpanded(QTreeWidgetItem*));
  CQT_CONNECT(widget, itemCollapsed(QTreeWidgetItem*),
              this, itemCollapsed(QTreeWidgetItem*));
  CQT_CONNECT(widget, dropReceived(const char*,QTreeWidgetItem*),
              this, dropReceived(const char*,QTreeWidgetItem*));
  CQT_CONNECT(widget, itemChanged(QTreeWidgetItem*,int),
              this, itemChanged(QTreeWidgetItem*,int));
}

QWidget* CQtDragDropWidgetControl::getWidget() const {
  return mWidget;
}


void CQtDragDropWidgetControl::dumpItem(UInt32 depth, UtOStream& stream, QTreeWidgetItem* item)
{
  for (UInt32 sp=0; sp<2*depth; ++sp)
    stream << " ";

  int columns = mWidget->columnCount();

  if (mWidget->isItemSelected(item))
      stream << "selected " ;

  for (int col=0; col < columns; col++) {
    stream << item->text(col);
    if (col < columns-1)
      stream << "\\";
  }

  stream << "\n";
  for (int ci=0; ci<item->childCount(); ci++)
    dumpItem(1+depth, stream, item->child(ci));
}

void CQtDragDropWidgetControl::dumpState(UtOStream& stream)
{
  QTreeWidgetItemIterator it(mWidget);

  for (UInt32 i = 0, n = mWidget->topLevelItemCount(); i < n; ++i) {
    QTreeWidgetItem* item = mWidget->topLevelItem(i);
    dumpItem(1, stream, item);
  }
}

void CQtDragDropWidgetControl::getValue(UtString* str) const {
  str->clear();
}

// Parse an item-list expressed as a sequence like "1 4 5.3.2"
// This indicates:
//    the 1st root item (counting from 0)
//    the fourth root item
//    the second child of the third child of the 5th root item
// This is how we encode widget items in an event log
bool CQtDragDropWidgetControl::parseItemList(const char* str, ItemSet* items) {
  for (StrToken itemsTok(str); !itemsTok.atEnd(); ++itemsTok) {
    const char* itemStr = *itemsTok;

    QTreeWidgetItem* item = NULL;
    for (StrToken itemTok(itemStr, "."); !itemTok.atEnd(); ++itemTok) {
      UInt32 idx;
      if (!itemTok.parseNumber(&idx)) {
        return false;
      }
      if (item == NULL) {
        if (idx < ((UInt32) mWidget->topLevelItemCount())) {
          item = mWidget->topLevelItem(idx);
        }
      }
      else if (idx < ((UInt32) item->childCount())) {
        item = item->child(idx);
      }
      if (item == NULL) {
        return false;
      }
    }
    if (item == NULL) {
      return false;
    }
    items->insert(item);
  }
  return true;
} // bool parseItemList

void CQtDragDropWidgetControl::encodeItemList(UtString* buf, const ItemSet& items) {
  // Find the string encoding for an item as described in the
  // comment for parseItemList.  We are given items in an order
  // that is not obviously guaranteed by the Qt docss, so the
  // best thing to do is walk the entire tree with our set in
  // hand, rather than risk an n^2 search
  for (UInt32 i = 0, n = mWidget->topLevelItemCount(); i < n; ++i) {
    QTreeWidgetItem* item = mWidget->topLevelItem(i);
    if (items.find(item) != items.end()) {
      *buf << i << ' ';               // top level item
    }
    else {
      UtString path;
      path << i << '.';
      encodeNestedItems(buf, path, item, items);
    }
  }
}

void CQtDragDropWidgetControl::encodeNestedItems(UtString* buf, const UtString& path,
                                                 QTreeWidgetItem* item,
                                                 const ItemSet& items)
{
  for (UInt32 i = 0, n = item->childCount(); i < n; ++i) {
    QTreeWidgetItem* child = item->child(i);
    if (items.find(child) != items.end()) {
      *buf << path << i << ' ';               // add it to master buf
    }
    else {
      UtString subpath(path);
      subpath << i << '.';
      encodeNestedItems(buf, subpath, child, items);
    }
  }
}

// Walk thru every friggin item and determine if it's selection
// status is right or wrong, and if so, change it
bool CQtDragDropWidgetControl::selectItems(const ItemSet& items) {
  bool changed = false;
  for (UInt32 i = 0, n = mWidget->topLevelItemCount(); i < n; ++i) {
    QTreeWidgetItem* item = mWidget->topLevelItem(i);
    bool wantSelect = (items.find(item) != items.end());
    if (wantSelect != mWidget->isItemSelected(item)) {
      mWidget->setItemSelected(item, wantSelect);
      changed = true;
    }
    changed |= selectItems(items, item);
  }
  if (changed) {
    mWidget->emitSelectionChanged();
  }
  return changed;
}

bool CQtDragDropWidgetControl::selectItems(const ItemSet& items,
                                           QTreeWidgetItem* parent)
{
  bool changed = false;
  for (UInt32 i = 0, n = parent->childCount(); i < n; ++i) {
    QTreeWidgetItem* item = parent->child(i);
    bool wantSelect = (items.find(item) != items.end());
    if (wantSelect != mWidget->isItemSelected(item)) {
      mWidget->setItemSelected(item, wantSelect);
      changed = true;
    }
    changed |= selectItems(items, item);
  }
  return changed;
}

void CQtDragDropWidgetControl::expandItems(const ItemSet& items) {
  for (ItemSet::const_iterator p = items.begin(), e = items.end();
       p != e; ++p)
  {
    QTreeWidgetItem* item = *p;
    mWidget->setItemExpanded(item, true);
  }
}


void CQtDragDropWidgetControl::collapseItems(const ItemSet& items) {
  for (ItemSet::const_iterator p = items.begin(), e = items.end();
       p != e; ++p)
  {
    QTreeWidgetItem* item = *p;
    mWidget->setItemExpanded(item, false);
  }
}

// putValue is used here as a hook to control the tree widget
// by changing the
bool CQtDragDropWidgetControl::putValue(const char* str, UtString* errmsg) {
  char cmd = *str;
  if (cmd == '\0') {
    *errmsg << "Empty tree-item string: " << str;
    return false;
  }
  ++str;
  ItemSet items;
  switch (cmd) {
  case 'S':
  case 'E':
  case 'C': {
    if (!parseItemList(str, &items)) {
      *errmsg << "Cannot parse tree-item string: " << str;
      return false;
    }
    else {
      switch (cmd) {
      case 'S':  selectItems(items);       break;
      case 'E':  expandItems(items);       break;
      case 'C':  collapseItems(items);     break;
      }
    }
    break;
  }
  case 'G': {
    UtIStringStream is(str);
    UtString itemStr, itemText;
    int col, row;
    if ((is >> UtIO::slashify >> itemStr >> row >> col >> itemText) &&
        parseItemList(itemStr.c_str(), &items))
    {
      QTreeWidgetItem* item = NULL;
      if (! items.empty()) {
        item = *items.begin();
      }

      QAbstractItemDelegate* delegate = mWidget->itemDelegate();
      if (delegate)
      {
        mWidget->setItemSelected(item, true);
        QModelIndex index = mWidget->model()->index(row, col);
        QStyleOptionViewItem ovi;
        item->setText(col, itemText.c_str());
        QWidget* editor = delegate->createEditor(mWidget, ovi, index);
        delegate->setEditorData(editor, index);
        delegate->setModelData(editor, mWidget->model(), index);
      }
      item->setText(col, itemText.c_str());
    }
    else {
      *errmsg << "Cannot parse drop string: " << str;
      return false;
    }
  }
  case 'D': {
    UtIStringStream is(str);
    UtString buf, itemStr;
    if ((is >> UtIO::slashify >> itemStr >> buf) &&
        parseItemList(itemStr.c_str(), &items))
    {
      QTreeWidgetItem* item = NULL;
      if (! items.empty()) {
        item = *items.begin();
      }
      mWidget->replayDrop(buf.c_str(), item);
    }
    else {
      *errmsg << "Cannot parse drop string: " << str;
      return false;
    }
  }
  }     // switch
  return true;
} // bool CQtDragDropWidgetControl::putValue

void CQtDragDropWidgetControl::itemSelectionChanged() {
  if (mCQt->isRecordActive()) {
    QTreeItemList itemList = mWidget->selectedItems();
    ItemSet items;
    for (QTreeItemList::iterator p = itemList.begin(), e = itemList.end();
         p != e; ++p)
    {
      QTreeWidgetItem* item = *p;
      items.insert(item);
    }
    UtString buf("S");
    encodeItemList(&buf, items);
    mCQt->recordPutValue(mWidget, buf.c_str());
  }
}

void CQtDragDropWidgetControl::itemChanged(QTreeWidgetItem* item, int col) 
{
  if (mCQt->isRecordActive()) {
    ItemSet items;
    items.insert(item);

    UtString buf, itemStr;
    encodeItemList(&itemStr, items);

    UtOStringStream os(&buf);
    int row = mWidget->indexOfTopLevelItem(item);
    os << UtIO::slashify << 'G' << itemStr << row << col << item->text(col);

    mCQt->recordPutValue(mWidget, buf.c_str());
  }
}

void CQtDragDropWidgetControl::itemExpanded(QTreeWidgetItem* item) {
  if (mCQt->isRecordActive()) {
    ItemSet items;
    items.insert(item);
    UtString buf("E");
    encodeItemList(&buf, items);
    mCQt->recordPutValue(mWidget, buf.c_str());
  }
}

void CQtDragDropWidgetControl::itemCollapsed(QTreeWidgetItem* item) {
  if (mCQt->isRecordActive()) {
    ItemSet items;
    items.insert(item);
    UtString buf("C");
    encodeItemList(&buf, items);
    mCQt->recordPutValue(mWidget, buf.c_str());
  }
}

void CQtDragDropWidgetControl::dropReceived(const char* str,
                                            QTreeWidgetItem* item)
{
  UtString buf, itemStr;
  ItemSet items;
  if (item != NULL) {
    items.insert(item);
  }
  encodeItemList(&itemStr, items);
  UtOStringStream os(&buf);
  os << UtIO::slashify << 'D' << itemStr << str;
  mCQt->recordPutValue(mWidget, buf.c_str());
}
