//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "car2cow.h"
#include "XtorTreeWidget.h"
#include "gui/CQt.h"
#include <QLineEdit>
#include <QComboBox>

#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtStringArray.h"
#include "util/UtStringUtil.h"

XtorTreeWidget::XtorTreeWidget(QWidget *parent)
: CDragDropTreeWidget(true, parent)
{
  ui.setupUi(this);

  CQT_CONNECT(this, itemSelectionChanged(), this, itemSelectionChanged());

  setItemDelegate(new XtorTreeEditDelegate(this, this));
}

XtorTreeWidget::~XtorTreeWidget()
{

}


void XtorTreeWidget::itemSelectionChanged()
{
  QList<QTreeWidgetItem*> itemList;
  buildSelectionList(itemList);

  foreach (QTreeWidgetItem *item, itemList)
  {
    QVariant qv = item->data(colINSTNAME, Qt::UserRole);
    CarbonCfgXtorInstance* xinst = (CarbonCfgXtorInstance*)qv.value<void*>();
  
    emit xtorSelectionChanged(xinst);
  }

  // Show nothing is selected
  if (itemList.count() == 0)
    emit xtorSelectionChanged(NULL);
}



void XtorTreeWidget::buildSelectionList(QList<QTreeWidgetItem*>& list)
{
  list.clear();

  foreach (QTreeWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(colINSTNAME, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonCfgXtorInstance* xinst = (CarbonCfgXtorInstance*)qv.value<void*>();
      if (xinst != NULL)
        list.append(item);
    }
  }  
}


// Get and/or Create item
QTreeWidgetItem* XtorTreeWidget::getItem(int row, Column) 
{
  QTreeWidgetItem* itm = topLevelItem(row);
  if (itm)
    return itm;
  else
  {
    itm = new QTreeWidgetItem(this);
    return itm;
  }
}

QTreeWidgetItem* XtorTreeWidget::updateRow(int row, CarbonCfgXtorInstance* xinst, bool clearSelect)
{
  if (clearSelect)
    clearSelection();

  QTreeWidgetItem* instName = getItem(row, colINSTNAME);
  QVariant qv = qVariantFromValue((void*)xinst);
  instName->setData(colINSTNAME, Qt::UserRole, qv);
  instName->setText(colINSTNAME, xinst->getName());
  instName->setFlags(instName->flags() | Qt::ItemIsEditable);

  instName->setText(colLIBRARY, xinst->getType()->getLibraryName());
  instName->setText(colXTORNAME, xinst->getType()->getName());

  return instName;
}

// User clicked the "Add Transactor" button
QTreeWidgetItem* XtorTreeWidget::AddXtor(CarbonCfgXtor* xtor)
{  
  CQtRecordBlocker blocker(mCtx->getCQt());

  UtString buf;
  mCtx->getCfg()->getUniqueESLName(&buf, xtor->getName());
  CarbonCfgXtorInstance* xinst = carbonCfgAddXtor(mCtx->getCfg(), buf.c_str(), xtor);

  int row = topLevelItemCount();
  
  QTreeWidgetItem* item = updateRow(row, xinst, false);
  setCurrentItem(item);
  
  mCtx->setDocumentModified(true);
  
  return item;
}

void XtorTreeWidget::DeleteXtor()
{
  CQtRecordBlocker blocker(mCtx->getCQt());

  QList<QTreeWidgetItem*> itemList;
  buildSelectionList(itemList);

  foreach (QTreeWidgetItem *item, itemList)
  {
    QVariant qv = item->data(colINSTNAME, Qt::UserRole);
    CarbonCfgXtorInstance* xinst = (CarbonCfgXtorInstance*)qv.value<void*>();
    if (xinst)
      deleteXtorInst(item, xinst);
  }
}

void XtorTreeWidget::deleteXtorInst(QTreeWidgetItem* item, CarbonCfgXtorInstance* xinst)
{
  for (int i = xinst->getType()->numPorts() - 1; i >= 0; --i)
  {
    CarbonCfgXtorConn* conn = xinst->getConnection(i);
    mCtx->getCfg()->disconnect(conn);
  }

  mCtx->getCfg()->removeXtorInstance(xinst);

  deleteItem(item);

  emit xtorSelectionChanged(NULL);

  mCtx->setDocumentModified(true);
}

void XtorTreeWidget::Populate(ApplicationContext* ctx, bool /*initPorts*/)
{
  mCtx = ctx;

  CQtRecordBlocker blocker(mCtx->getCQt());

  for (UInt32 i=0, nRegs = mCtx->getCfg()->numXtorInstances(); i<nRegs; ++i)
  {
    CarbonCfgXtorInstance* xinst = mCtx->getCfg()->getXtorInstance(i);
    int row = topLevelItemCount();
    setCurrentItem(updateRow(row, xinst, false));
  }
}

void XtorTreeWidget::putContext(ApplicationContext* ctx)
{
  mCtx = ctx;
  header()->setResizeMode(colINSTNAME, QHeaderView::ResizeToContents);
  header()->setResizeMode(colLIBRARY, QHeaderView::ResizeToContents);
  header()->setResizeMode(colXTORNAME, QHeaderView::Stretch);
}

// Editor Delegate
XtorTreeEditDelegate::XtorTreeEditDelegate(QObject *parent, XtorTreeWidget* tree)
: QItemDelegate(parent), xtorTree(tree)
{
}
QWidget *XtorTreeEditDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                             const QModelIndex &index) const
{
  switch (index.column())
  {
  case XtorTreeWidget::colINSTNAME:
  {
    QLineEdit *editor = new QLineEdit(parent);
    connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
    return editor;
  }
  default:
    return NULL; // Read Only
    break;
  }

  return NULL;
}

void XtorTreeEditDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  emit commitData(editor);
  emit closeEditor(editor);
}

void XtorTreeEditDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}

void XtorTreeEditDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                         const QModelIndex &index) const
{
  QTreeWidgetItem* item = xtorTree->currentItem();
  QVariant qv = item->data(XtorTreeWidget::colINSTNAME, Qt::UserRole);
  CarbonCfgXtorInstance* xinst = (CarbonCfgXtorInstance*)qv.value<void*>();

  UtString editText;

  switch (index.column())
  {
  case XtorTreeWidget::colINSTNAME:
    {
      QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
      if (edit)
      {
        editText << edit->text();
        model->setData(index, edit->text());
        xtorTree->mCtx->getCfg()->changeXtorInstanceName(xinst, editText.c_str());
        xtorTree->mCtx->setDocumentModified(true);
      }
    }
    break;
  default:
    break;
  }
}
