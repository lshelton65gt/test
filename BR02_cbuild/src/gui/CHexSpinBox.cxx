/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CQt.h"
#include "gui/CHexSpinBox.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/DynBitVector.h"
#include <QLineEdit>

enum BVConvStat {
  eConvOK,
  eConvFail,
  eConvEmpty
};

static BVConvStat sStrToDynBV(DynBitVector* bv, const UtString& buf, UtString* errMsg = NULL)
{
  if (buf.empty())
    return eConvEmpty;
  
  bv->resize(buf.size() * 4); 
  UtIStringStream is(buf);
  bool ok = (is >> UtIO::hex >> *bv) && is.eof();
  if (! ok && (errMsg != NULL))
    *errMsg = is.getErrmsg();
  
  if (! ok)
    return eConvFail;
  
  return eConvOK;
}

static BVConvStat sStrToDynBV(DynBitVector* bv, const QString& str)
{
  UtString buf;
  buf << str;
  return sStrToDynBV(bv, buf);
}

CHexSpinBox::CHexValidator::CHexValidator(CHexSpinBox* parent)
  : QValidator(parent),
    mSpinBox(parent)
{
}

QValidator::State CHexSpinBox::CHexValidator::validate(QString& input, int& )
  const
{
  DynBitVector bv;
  switch (sStrToDynBV(&bv, input)) {
  case eConvFail: return Invalid; break;
  case eConvOK: 
    return mSpinBox->inRange(bv) ? Acceptable : Invalid;
    break;
  case eConvEmpty: return Acceptable; break;
  }
  return Invalid;
}

CHexSpinBox::CHexSpinBox() :
  mValidator(this)
{
//  setPrefix("0x");
  CHexValidator validator(this);
  lineEdit()->setValidator(&mValidator);
  mMinimum = 0;
  mMaximum = 0xffffffff;
  mSingleStep = 1;
  CQT_CONNECT(lineEdit(), textChanged(const QString&),
              this, lineEditValueChanged(const QString&));
}

CHexSpinBox::CHexSpinBox(QWidget* parent) : QAbstractSpinBox(parent), mValidator(this)
{
//  setPrefix("0x");
  CHexValidator validator(this);
  lineEdit()->setValidator(&mValidator);
  mMinimum = 0;
  mMaximum = 0xffffffff;
  mSingleStep = 1;
  CQT_CONNECT(lineEdit(), textChanged(const QString&),
              this, lineEditValueChanged(const QString&));
}

CHexSpinBox::~CHexSpinBox() {
}

void CHexSpinBox::lineEditValueChanged(const QString& val) {
  emit valueChanged(val);
  DynBitVector bv;
  if (getValue(&bv)) {
    emit valueChanged(bv);
    emit valueChanged(*bv.getUIntArray()); // low-order word only
  }
}

/*
QString CHexSpinBox::textFromValue(int v) const {
  char buf[100];
  sprintf(buf, "%x", v);
  return QString(buf);
}

int CHexSpinBox::valueFromText(const QString& v) const {
  int val;
  UtString buf = v;
  (void) sscanf(v.c_str(), "%x", &val);
  return val;
}
*/

QAbstractSpinBox::StepEnabled CHexSpinBox::stepEnabled() const {
  return StepUpEnabled | StepDownEnabled;
}



bool CHexSpinBox::getValue(DynBitVector* bv) const {
  QString qbuf(text());
  // Clear the bv. We can't be sure that the bv is initialized to
  // the minimum. If the box is empty, we want the Minimum.
  *bv = mMinimum;
  return sStrToDynBV(bv, qbuf) != eConvFail;
}

void CHexSpinBox::setValue(const DynBitVector& bv) {
  UtString buf;
  UtOStringStream os(&buf);
  os << UtIO::hex << bv;
  lineEdit()->setText(buf.c_str());
}

void CHexSpinBox::setValue(int val) {
  UtString buf;
  UtOStringStream os(&buf);
  os << UtIO::hex << val;
  lineEdit()->setText(buf.c_str());
}

bool CHexSpinBox::setValue(const char* str, UtString* errmsg) {
  // validate the input and then just blast it into the line editor
  UtString buf(str);
  DynBitVector bv;
  BVConvStat stat = sStrToDynBV(&bv, buf, errmsg);
  switch (stat)
  {
  case eConvOK:
    if (inRange(bv))
    {
      lineEdit()->setText(str);
      return true;
    }
    break;
  case eConvFail:
    {
      errmsg->clear();
      UtOStringStream os(errmsg);
      os << "Value entered is beyond maximum (" << UtIO::hex << mMaximum << ")";
      return false;
    }
    break;
  case eConvEmpty:
    {
      // empty strings are fine. This basically means default value
      // upon construction. So, if you click into nowhere the spinbox
      // will empty itself.
      lineEdit()->setText(str);
      return true;
    }
    break;
  }
  return false;
}

bool CHexSpinBox::inRange(const DynBitVector& bv) const {
  return (bv >= mMinimum) && (bv < mMaximum);
}

void CHexSpinBox::stepBy(int steps) {
  DynBitVector bv;
  if (getValue(&bv)) {
    DynBitVector newBv(bv);
    int delta = mSingleStep * steps;
    if (delta < 0) {
      // += -K will not work because there is no DBV+=(int), only DBV+=(unsigned int)
      newBv -= -delta;
    }
    else {
      newBv += delta;
    }

    // If our addition caused the value to wrap, then punt
    if ((steps > 0) && (newBv < bv)) {
      return;
    }
    if ((steps < 0) && (newBv > bv)) {
      return;
    }

    if ((newBv >= mMinimum) && (newBv <= mMaximum)) {
      setValue(newBv);
    }
  }
}

QValidator::State CHexSpinBox::validate(QString& input, int&) const {
  DynBitVector bv;
  
  switch (sStrToDynBV(&bv, input)) {
  case eConvFail: return QValidator::Invalid;
  case eConvOK: return inRange(bv) ? QValidator::Acceptable : QValidator::Invalid;
  case eConvEmpty: return QValidator::Acceptable;
  }
  return QValidator::Invalid;
}

void CHexSpinBox::setMinimum(const DynBitVector& bv) {
  mMinimum = bv;
}
void CHexSpinBox::setMaximum(const DynBitVector& bv) {
  mMaximum = bv;
}

void CHexSpinBox::setSingleStep(int val) {
  mSingleStep = val;
}
void CHexSpinBox::setMinimum(int val) {
  mMinimum = val;
}
void CHexSpinBox::setMaximum(int val) {
  mMaximum = val;
}
