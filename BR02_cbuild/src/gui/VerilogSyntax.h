// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _VERILOG_SYNTAX__H
#define _VERILOG_SYNTAX__H

#include "util/UtString.h"
#include "util/UtHashMap.h"
#include <QTextCharFormat>
#include <QSyntaxHighlighter>

class QTextDocument;

class VerilogSyntax : public QSyntaxHighlighter
{
public:
  VerilogSyntax(QTextDocument *parent = 0);
  virtual ~VerilogSyntax();

protected:
  void highlightBlock(const QString &text);

private:
  enum State {eStart, eName, ePunctuation, eWhitespace,
              eBlockComment, eLineComment, eDQuote};

  //! simple re-entrant lexer.  Every time it yields a new token,
  //! returns the type of the token just parsed
  State lex(const char* ascii, int* start, int* len, State* retstate);

  //! Map verilog keywords to Qt colors.
  /*!
   *! All keywords are drawn in boldface.  We subdivide the base
   *! keywords, from the net declaration keywords from the 95 keywords
   *! and 2001 keywords, and draw them all in different colors.
   */
  typedef UtHashMap<UtString,QColor> ColorMap;
  ColorMap mColorMap;
  QTextCharFormat mCommentFormat;
  QTextCharFormat mQuoteFormat;
  QTextCharFormat mKeywordFormat;
};

#endif
