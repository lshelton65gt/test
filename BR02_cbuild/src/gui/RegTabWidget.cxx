// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "RegTabWidget.h"
#include "ui_RegTab.h"
#include "MaxSimApplicationContext.h"
#include "gui/CQtPushButtonControl.h"
#include "gui/CQtTableWidgetControl.h"
#include "gui/CQtDialogControl.h"
#include "gui/CQt.h"
#include "ApplicationContext.h"
#include "CarbonProjectWidget.h"

RegTabWidget::RegTabWidget(QMainWindow *main_win,
                           CQtContext *cqt,
                           CarbonCfgID cfg, WizardMode mode, CarbonProjectWidget* pw)
  : mMainWin(main_win),
    mCQt(cqt),
    mCfg(cfg)
{
  mMode = mode;
  mUI = new Ui::RegTab;
  mUI->setupUi(this);
  mCtx = new MaxSimApplicationContext(0, mCfg, mCQt);
  mCtx->putMainWin(mMainWin);
  mUI->tableRegisters->putContext(mCtx);
  mUI->tableFields->putContext(mCtx, pw);

  // Fix some aspects of the UI that are different from
  // in CoWare
  mUI->tableRegisters->setColumnCount(mCtx->getRegisterTableColumnEnum("colEND"));
  mUI->tableRegisters->horizontalHeaderItem(mCtx->getRegisterTableColumnEnum("colGROUP"))->setText("Group");
  mUI->tableRegisters->horizontalHeaderItem(mCtx->getRegisterTableColumnEnum("colREGNAME"))->setText("Name");
  mUI->tableRegisters->horizontalHeaderItem(mCtx->getRegisterTableColumnEnum("colWIDTH"))->setText("Width");
  mUI->tableRegisters->horizontalHeaderItem(mCtx->getRegisterTableColumnEnum("colENDIAN"))->setText("Endian");
  mUI->tableRegisters->horizontalHeaderItem(mCtx->getRegisterTableColumnEnum("colDESCRIPTION"))->setText("Description");
  QTableWidgetItem *fieldsItem = new QTableWidgetItem();
  fieldsItem->setText("Fields");
  mUI->tableRegisters->setHorizontalHeaderItem(mCtx->getRegisterTableColumnEnum("colFIELDS"), fieldsItem);
  QTableWidgetItem *radixItem = new QTableWidgetItem();
  radixItem->setText("Radix");
  mUI->tableRegisters->setHorizontalHeaderItem(mCtx->getRegisterTableColumnEnum("colRADIX"), radixItem);
  QTableWidgetItem *portItem = new QTableWidgetItem();
  portItem->setText("Port");
  mUI->tableRegisters->setHorizontalHeaderItem(mCtx->getRegisterTableColumnEnum("colPORT"), portItem);

  // Register controls for all the widgets
  mCQt->registerWidget(new CQtPushButtonControl(mUI->btnAdd, mCQt), "reg_button_add", false);
  mCQt->registerWidget(new CQtPushButtonControl(mUI->btnDelete, mCQt), "reg_button_delete", false);
  mCQt->registerWidget(new CQtPushButtonControl(mUI->btnAddField, mCQt), "reg_button_add_field", false);
  mCQt->registerWidget(new CQtPushButtonControl(mUI->btnDeleteField, mCQt), "reg_button_delete_field", false);
  mCQt->registerWidget(new CQtPushButtonControl(mUI->btnBindConstant, mCQt), "reg_button_bind_const", false);

  mCQt->registerWidget(new CQtTableWidgetControl(mUI->tableRegisters, mCQt), "reg_table_registers", false);
  mCQt->registerWidget(new CQtTableWidgetControl(mUI->tableFields, mCQt), "reg_table_fields", false);
  mCQt->registerWidget(new CQtDialogControl(mUI->tableFields->getConstantDialog(), mCQt), "reg_dialog_constant", false);

  mUI->btnAddField->setEnabled(false);
  mUI->btnDelete->setEnabled(false);

  mSelectedNode = 0;
  mReg = 0;
  mDB = 0;

  mUI->tableFields->setAcceptDrops(true);

  CQT_CONNECT(mUI->tableFields, selectionChanged(CarbonCfgRegisterField*), this, fieldSelectionChanged(CarbonCfgRegisterField*));
  CQT_CONNECT(mUI->tableRegisters, regSelectionChanged(CarbonCfgRegister*), this, registerSelectionChanged(CarbonCfgRegister*));
}

RegTabWidget::~RegTabWidget()
{
  delete mUI;
}

void RegTabWidget::populate(CarbonDB *db)
{
  mDB = db;
  mCtx->putDB(db);
  mUI->tableRegisters->Populate();

  populateBaseAddressComboBox();
}

void RegTabWidget::clear()
{
  mDB = 0;
  mCtx->putDB(0);
  mUI->tableRegisters->clearAll();
  mUI->tableFields->clearAll();
}

// populate the base address parameter combobox 
void RegTabWidget::populateBaseAddressComboBox()
{
  UtString baseAddr = mCfg->getRegisterBaseAddress();  // get the current base address

  mUI->cbRegBaseAddr->blockSignals(true); // block signals while populating combobox

  mUI->cbRegBaseAddr->clear();

  // populate the base address combobox with parameters
  QStringList params;
  for (uint i = 0; i < mCfg->GetNumParams(); ++i) {
    CarbonCfgXtorParamInst* param = mCfg->getParam(i);
    // build list of all uint32 and uint64 parameters to put into the combo box
    if (param->GetParam()->getType() == eCarbonCfgXtorUInt32 || 
        param->GetParam()->getType() == eCarbonCfgXtorUInt64)
      params.append(param->GetParam()->getName());  
  }
  mUI->cbRegBaseAddr->addItems(params);

  int idx = mUI->cbRegBaseAddr->findText(baseAddr.c_str());
  if (idx < 0) {
    idx = 0;
    mUI->cbRegBaseAddr->insertItem(0,baseAddr.c_str());
  }
  
  mUI->cbRegBaseAddr->setCurrentIndex(idx);
  mUI->cbRegBaseAddr->blockSignals(false); // unblock signals
}

void RegTabWidget::deleteRegister()
{
  mUI->tableRegisters->DeleteRegister();
}

void RegTabWidget::fieldSelectionChanged(CarbonCfgRegisterField* /*field*/)
{
  bool fieldSelected = mUI->tableFields->selectedItems().count() > 0;

  mUI->btnBindConstant->setEnabled(fieldSelected);
  mUI->btnDelete->setEnabled(fieldSelected);
  mUI->btnDeleteField->setEnabled(fieldSelected);
}

void RegTabWidget::registerSelectionChanged(CarbonCfgRegister* reg)
{
  mReg = reg;
  mUI->tableFields->Initialize(mReg);
  mUI->btnAddField->setEnabled(mReg != NULL);
  mUI->btnDelete->setEnabled(mReg != NULL);
}


void RegTabWidget::selectionChanged(CarbonDBNode* node)
{
  mSelectedNode = node;
  const char* path = carbonDBNodeGetFullName(mDB, node);
  INFO_ASSERT(path, "Unable to find path for selected node");
}


void RegTabWidget::on_btnBindConstant_clicked()
{
   mUI->tableFields->BindConstant();
}

void RegTabWidget::on_btnDeleteField_clicked()
{
  mUI->tableFields->DeleteField();
}

void RegTabWidget::on_btnAddField_clicked()
{
  mUI->tableFields->AddField();
}

void RegTabWidget::on_btnAdd_clicked()
{
  mUI->tableRegisters->AddRegister();
}

void RegTabWidget::on_btnDelete_clicked()
{
  mUI->tableRegisters->DeleteRegister();
}

void RegTabWidget::documentWasModified()
{
  setWindowModified(true);
}

void RegTabWidget::on_cbRegBaseAddr_currentIndexChanged(const QString & text)
{
  UtString addr;
  addr << text;
  mCfg->putRegisterBaseAddress(addr.c_str());
  mCtx->setDocumentModified(true);
}

void RegTabWidget::on_cbRegBaseAddr_editTextChanged(const QString & text)
{
  UtString addr;
  addr << text;
  mCfg->putRegisterBaseAddress(addr.c_str());
  mCtx->setDocumentModified(true);
}
