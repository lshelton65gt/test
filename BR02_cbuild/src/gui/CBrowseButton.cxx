//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "CBrowseButton.h"
#include "CBrowse.h"

CBrowseButton::CBrowseButton(double x, double y, Orientation orientation,
                               CBrowse* cbrowser)
  : mX(x), mY(y), mOrientation(orientation), mState(eOff), mBrowser(cbrowser)
{
}  
CBrowseButton::~CBrowseButton() {
}

void CBrowseButton::addNode(CGraph::Node* node) {
  mNodes.push_back(node);
}

void CBrowseButton::draw(QPainter* painter) {
  bool any_invisible = false;
  for (UInt32 i = 0; i < mNodes.size(); ++i) {
    if (! mBrowser->isVisible(mNodes[i])) {
      any_invisible = true;
      break;
    }
  }
  if (any_invisible) {
    mState = eOff;
  }
  else if (mState == eOff) {
    mState = eDisabled;
  }

  switch (mState) {
  case eOff:       painter->setBrush(Qt::blue); break;
  case eOn:        painter->setBrush(Qt::red);  break;
  case eDisabled:  painter->setBrush(Qt::gray); break;
  }

  QPointF points[3];
  points[0] = QPointF(mX, mY);
  switch (mOrientation) {
  case eLeft:
    points[1] = QPointF(mX + BUTTON_WIDTH - 1, mY - BUTTON_HEIGHT / 2);
    points[2] = QPointF(mX + BUTTON_WIDTH - 1, mY + BUTTON_HEIGHT / 2 - 1);
    break;
  case eRight:
    points[1] = QPointF(mX - BUTTON_WIDTH + 1, mY - BUTTON_HEIGHT / 2);
    points[2] = QPointF(mX - BUTTON_WIDTH + 1, mY + BUTTON_HEIGHT / 2 - 1);
    break;
  case eDown:
    points[1] = QPointF(mX - BUTTON_HEIGHT/2,     mY - BUTTON_WIDTH + 1);
    points[2] = QPointF(mX + BUTTON_HEIGHT/2 - 1, mY - BUTTON_WIDTH + 1);
    break;
  }
  painter->drawConvexPolygon(points, 3);
}

bool CBrowseButton::inside(double x, double y) const {
  switch (mOrientation) {
  case eLeft:
    return ((x >= mX) &&
            (x < mX + BUTTON_WIDTH) &&
            (y >= mY - BUTTON_HEIGHT / 2) &&
            (y < mY + BUTTON_HEIGHT / 2));
  case eRight:
    return ((x <= mX) &&
            (x > mX - BUTTON_WIDTH) &&
            (y >= mY - BUTTON_HEIGHT / 2) &&
            (y < mY + BUTTON_HEIGHT / 2));
  case eDown:
    return ((y <= mY) &&
            (y >  mY - BUTTON_WIDTH) &&
            (x >= mX - BUTTON_HEIGHT / 2) &&
            (x <  mX + BUTTON_HEIGHT / 2));
  }
  return false;                 // not reached
}

void CBrowseButton::toggle() {
  switch (mState) {
  case eOff:       mState = eOn;  break;
  case eOn:        mState = eOff; break;
  case eDisabled:  break;
  }
}

double CBrowseButton::getWidth() const {
  switch (mOrientation) {
  case eLeft:
  case eRight:
    return BUTTON_WIDTH;
  default:
    break;
  }
  return BUTTON_HEIGHT;
}

double CBrowseButton::getHeight() const {
  switch (mOrientation) {
  case eLeft:
  case eRight:
    return BUTTON_HEIGHT;
  default:
    break;
  }
  return BUTTON_WIDTH;
}
