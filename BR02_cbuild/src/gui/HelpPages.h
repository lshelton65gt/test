// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*
  The first column is the tag name used in the code, and the second
  column is the location of the HTML page that we want to display when
  the user clicks on the '?' button.

  The HTML page locations are relative to
    CARBON_HOME/userdoc/soc_html/output
*/

// default page for tags we can't find here!
HELP_PAGE("default",          "SVGen-cover.html");

// help for the ESL and RTL tree widgets of the 'Ports' tab
HELP_PAGE("esl-tree",         "SVGen-2GUI.html#1050673");
HELP_PAGE("rtl-tree",         "SVGen-2GUI.html#1050673");

// 'HBrowse' hierarchy browser panes: module hierarchy, and nets
HELP_PAGE("hbrowse-modhier",  "SVGen-2GUI.html#1049582");
HELP_PAGE("hbrowse-nets",     "SVGen-2GUI.html#1049582");

// the Help->Help menu choice
HELP_PAGE("help-menu",        "SVGen-2GUI.html#1049529");

// 'Memories' tab
HELP_PAGE("mem-tree",         "SVGen-2GUI.html#1049587");

// 'Ports' tab transactor parameter editor
HELP_PAGE("param-edit",       "SVGen-2GUI.html#1049567");

// 'Ports' tab, 'port expression' editor
HELP_PAGE("port-expr",        "SVGen-2GUI.html#1054051");

// 'Profile' tab
HELP_PAGE("profile-tree",     "SVGen-2GUI.html#1050955");

// 'Registers' tab 
HELP_PAGE("reg-tree",         "SVGen-2GUI.html#1049582");

// editing clock generator parameters
HELP_PAGE("clock-gen",        "SVGen-2GUI.html#1049551");

// editing reset generator parameters
HELP_PAGE("reset-gen",        "SVGen-2GUI.html#1049551");

// 'Clock Control' panel for transactors
HELP_PAGE("xtor-clocks",      "SVGen-2GUI.html#1049551");

// CControl Properties Pane
HELP_PAGE("replay_prop",      "replay_1.html#1057663");

// CControl Control Pane
HELP_PAGE("replay_control",   "replay_1.html#1057699");

// CControl Components Pane
HELP_PAGE("replay_comps",     "replay_1.html#1057705");
