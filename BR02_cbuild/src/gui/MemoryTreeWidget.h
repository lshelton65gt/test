// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef __MEMORYTREEWIDGET_H___
#define __MEMORYTREEWIDGET_H___

#include "gui/CDragDropTreeWidget.h"

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "cfg/CarbonCfg.h"

#include "ApplicationContext.h"

#include <QObject>
#include <QTreeWidget>
#include <QtGui>

class MemoryTreeWidget : public CDragDropTreeWidget
{
   Q_OBJECT

   enum Columns {colNAME=0, colPATH, colMAXADDR, colWIDTH, colCOMMENT};

public:
  MemoryTreeWidget(QWidget *parent = 0);
  ~MemoryTreeWidget();
  void Initialize(ApplicationContext* ctx);
  void Populate(ApplicationContext* ctx, bool initPorts);
  void DeleteMemory();

private:
  CarbonDB* mDB;
  CarbonCfgID mCfg;
  ApplicationContext* mCtx;

private:
  QTreeWidgetItem* addMemoryNode(const CarbonDBNode* node);
  CarbonCfgMemory* findMemory(const char* name);
  QTreeWidgetItem* addMem(CarbonCfgMemory* mem);
  void buildSelectionList(QList<QTreeWidgetItem*>& list);

  friend class MemoryEditDelegate;

public slots:
  void dropMemory(const char*);
};

class MemoryEditDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    MemoryEditDelegate(QObject *parent = 0, MemoryTreeWidget* t=0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
        const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const;
 
private slots:
    void commitAndCloseEditor();
    void currentIndexChanged(int index);

private:
  MemoryTreeWidget* tree;

};

#endif
