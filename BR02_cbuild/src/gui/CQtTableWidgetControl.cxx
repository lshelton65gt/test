/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CQt.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "gui/CQtTableWidgetControl.h"
#include "gui/CTableWidget.h"

CQtTableWidgetControl::CQtTableWidgetControl(CTableWidget *table, CQtContext *cqt)
  : mTableWidget(table),
    mCQt(cqt)
{
  CQT_CONNECT(table, cellClicked(int, int), this, cellClicked(int, int));
  CQT_CONNECT(table, cellEdited(QTableWidgetItem*), this, cellEdited(QTableWidgetItem*));
}

void CQtTableWidgetControl::dumpState(UtOStream& stream)
{
  stream << "\n";
}

void CQtTableWidgetControl::getValue(UtString* str) const
{
  str->clear();
}

bool CQtTableWidgetControl::putValue(const char *str, UtString* /* errmsg */)
{
  UtIStringStream is(str);
  int row = -1;
  int column = -1;
  char cmd = 'X';
  UtString data;
  is >> UtIO::slashify >> cmd;
  if (cmd == 'S') {
    // cell selected
    if (!(is >> row >> column)) {
      return false;
    }
    QAbstractItemView::EditTriggers triggers = mTableWidget->editTriggers();
    mTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mTableWidget->setCurrentCell(row, column);
    mTableWidget->setEditTriggers(triggers);
  } else if (cmd == 'V') {
    // cell value
    if (!(is >> data)) {
      return false;
    }
    mTableWidget->putRecordData(data);
  } else {
    // illegal command
    return false;
  }

  return true;
}

QWidget* CQtTableWidgetControl::getWidget() const
{
  return mTableWidget;
}

void CQtTableWidgetControl::cellClicked(int row, int column)
{
  UtString buf;
  UtOStringStream os(&buf);
  os << UtIO::slashify << "S" << row << column;
  mCQt->recordPutValue(mTableWidget, buf.c_str());
}

void CQtTableWidgetControl::cellEdited(QTableWidgetItem *item)
{
  UtString buf;
  UtOStringStream os(&buf);
  os << UtIO::slashify << "V" << item->text();
  mCQt->recordPutValue(mTableWidget, buf.c_str());
}

