// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CARBON_PROFILE_EDIT_H_
#define _CARBON_PROFILE_EDIT_H_

#include "gui/CQt.h"
#include <QHBoxLayout>
#include "util/UtBiMap.h"
#include "util/UtHashSet.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "cfg/CarbonCfg.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "util/SourceLocator.h"

class QTreeWidget;
class QTreeWidgetItem;
class TextEdit;
class QStatusBar;
class CExprValidator;

class CarbonProfileEdit : public QWidget {
  Q_OBJECT
public:

  CARBONMEM_OVERRIDES

  //! ctor
  CarbonProfileEdit(CQtContext*, CarbonCfgID, AtomicCache* atomicCache,
                    QAction* deleteAction, QStatusBar*);

  //! dtor
  ~CarbonProfileEdit();

  //! clear the tree
  void clear();
  
  //! after reading the database, populate the register tree
  void populate(CarbonDB* carbonDB);

  void read();
  QWidget* buildLayout();

  void deleteCurrent();
  bool isDeleteActive() const;

public slots:
  void changeName(const QString&);
  void changeColor(int);
  void changeExpr();
  void changeTreeSel();
  void dropNet(const char*, QTreeWidgetItem*);
  void addChannel();
  void addStream();
  void addTrigger();
  void addBucket();
  void mapDropItem(QTreeWidgetItem** item);

private:
  void setExprIdents(CarbonCfgPStream* pstream);
  void enableWidgets();
  QTreeWidgetItem* addStream(CarbonCfgPStream* pstream);
  void setStreamTreeText(CarbonCfgPStream* pstream);
  void setTriggerTreeText(CarbonCfgPStream* pstream,
                          CarbonCfgPTrigger* ptrigger);
  void setChannelTreeText(CarbonCfgPStream* pstream,
                          CarbonCfgPChannel* pchannel);
  void setNetTreeText(CarbonCfgPNet* pnet);
  void setBucketTreeText(CarbonCfgPStream* pstream,
                         CarbonCfgPBucket* pbucket);
  QTreeWidgetItem* getNetsItem(CarbonCfgPStream* pstream);
  QTreeWidgetItem* getTriggersItem(CarbonCfgPStream* pstream);
  QTreeWidgetItem* addNet(CarbonCfgPStream* pstream, CarbonCfgPNet* pnet);
  QTreeWidgetItem* addTrigger(CarbonCfgPStream* pstream,
                              CarbonCfgPTrigger* ptrigger);
  QTreeWidgetItem* addChannel(CarbonCfgPStream* pstream,
                              CarbonCfgPTrigger* ptrigger,
                              CarbonCfgPChannel* pchannel);
  QTreeWidgetItem* addBucket(CarbonCfgPStream* pstream,
                             CarbonCfgPChannel* pchannel,
                             CarbonCfgPBucket* pbucket);
  void resetExprText(CarbonCfgPStream* pstream);

  void deleteBucket(CarbonCfgPStream* pstream,
                    CarbonCfgPChannel* pchannel, CarbonCfgPBucket* pbucket,
                    bool updateChannel);
  void deleteTrigger(CarbonCfgPStream* pstream, CarbonCfgPTrigger*);
  void deleteChannel(CarbonCfgPStream* pstream, CarbonCfgPChannel* pchannel);
  void deleteChannelHelper(CarbonCfgPStream* pstream,
                           CarbonCfgPChannel* pchannel);
  void deleteNet(CarbonCfgPStream* pstream, CarbonCfgPNet* pnet,
                 bool updateExprText);
  void deleteStream(CarbonCfgPStream* pstream);
  void setExprItemText(CarbonCfgPStream*, QTreeWidgetItem* item,
                       const char* label, const char* expr);
  bool isBucketChannel(CarbonCfgPStream* pstream,
                       CarbonCfgPChannel* pchannel);

  CDragDropTreeWidget* mTree;

  CExprValidator* mExpr;
  QLineEdit* mName;
  QPushButton* mAddStream;
  QPushButton* mAddChannel;
  QPushButton* mAddTrigger;
  QPushButton* mAddBucket;

  QComboBox* mColor;

  UtBiMap<CarbonCfgPStream*,QTreeWidgetItem*>  mStreamItemMap;
  UtBiMap<CarbonCfgPChannel*,QTreeWidgetItem*> mChannelItemMap;
  UtBiMap<CarbonCfgPTrigger*,QTreeWidgetItem*> mTriggerItemMap;
  UtBiMap<CarbonCfgPBucket*,QTreeWidgetItem*>  mBucketItemMap;
  UtBiMap<CarbonCfgPNet*,QTreeWidgetItem*>     mNetItemMap;

  CarbonCfgID mCfg;
  UInt32 mSelecting;
  CarbonDB* mDB;

  SourceLocatorFactory mLocatorFactory;
  AtomicCache* mAtomicCache;

  QAction* mDeleteAction;

  CarbonCfgPStream* mEditStream;
  CarbonCfgPTrigger* mEditTrigger;
  CarbonCfgPChannel* mEditChannel;
  CarbonCfgPBucket* mEditBucket;
  CarbonCfgPNet* mEditNet;
  CQtContext* mCQt;
  QStatusBar* mStatusBar;
}; // class CarbonProfileEdit : public QWidget

#endif
