// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef __code_generator_h__
#define __code_generator_h__

#include "car2cow.h"
#include <QObject>
#include <QProcess>
#include <QTextEdit>
#include <QString>

class CodeGenerator : public QObject
{
  Q_OBJECT

public:
  CodeGenerator(Car2Cow* pApp);
  virtual ~CodeGenerator();

  void Generate(QTextEdit* textWidget);

private:
  Car2Cow* mApp;
  QProcess* mProcess;
  QTextEdit* mText;
  QTextCodec* mCodec;

private:
 void appendBytes(const QByteArray& bytes);

private slots:
  void processError(QProcess::ProcessError);
  void readStderr();
  void readStdout();
  void started();
  void finished(int exit_code);
  void launch(QString& cmd, QStringList& args);

};
#endif
