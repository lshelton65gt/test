/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "car2cow.h"
#include "PortView.h"

#include <QObject>
#include <QFileDialog>
#include <QtDebug>
#include <QtGui>
#include <QLabel>
#include <QStringList>

#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "cfg/CarbonCfg.h"

PortView::PortView(Car2Cow* pMainWindow)
{
  m_pMainWnd = pMainWindow;
  if (pMainWindow)
  {
    m_pPortTable = pMainWindow->ui.tablePorts;
    m_pTieOffTable = pMainWindow->ui.tieoffList;
    m_pDisconnectTable = pMainWindow->ui.disconnectList;
  }

  mDB = NULL;
  mCfg = NULL;
}

PortView::~PortView()
{
}

void PortView::Populate(ApplicationContext* ctx, bool initPorts)
{
  m_pPortTable->Update(ctx, m_pTieOffTable, m_pDisconnectTable);  
  m_pPortTable->Populate(ctx, initPorts);
  m_pTieOffTable->Populate(ctx, initPorts);
  m_pDisconnectTable->Populate(ctx, initPorts);
}

void PortView::Update()
{
  m_pPortTable->Update(NULL, m_pTieOffTable, m_pDisconnectTable);  
}

