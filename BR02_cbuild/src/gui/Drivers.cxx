/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "Drivers.h"
#include "util/SourceLocator.h"
#include "util/AtomicCache.h"
#include "util/StringAtom.h"
#include "util/Zstream.h"
#include "util/UtHashSet.h"

Driver::~Driver() {
  for (UInt32 i = 0, n = mNested.size(); i < n; ++i) {
    delete mNested[i];
  }
}

void Driver::addNested(Driver* driver) {
  mNested.push_back(driver);
}

void Driver::addFanin(const CarbonDBNode* fanin) {
  mFanin.push_back(fanin);
}

bool Driver::operator<(const Driver& other) const {
  int cmp = SourceLocator::compare(mLoc, other.mLoc);
  if (cmp == 0) {
    cmp = strcmp(mBits->str(), other.mBits->str());
    if (cmp == 0) {
      cmp = strcmp(mType->str(), other.mType->str());
    }
  }
  return cmp < 0;
}

DriverDatabase::DriverDatabase(SourceLocatorFactory* slc,
                               AtomicCache* ac, CarbonDB* db)
  : mSourceLocatorFactory(slc),
    mAtomicCache(ac),
    mDB(db)
{
}

//! dtor
DriverDatabase::~DriverDatabase() {
  for (NodeDriverMap::iterator p = mNodeDriverMap.begin(),
         e = mNodeDriverMap.end(); p != e; ++p)
  {
    DriverVec& vec = p->second;
    for (UInt32 i = 0, n = vec.size(); i < n; ++i) {
      Driver* driver = vec[i];
      delete driver;
    }
    vec.clear();
  }
}

//! Load the drivers database from the .drivers file
bool DriverDatabase::load(const char* filename, UtString* errmsg) {
  ZISTREAMDB(db, filename);

  if (db.is_open()) {
    char key = '\0';
    UInt32 numRecords = 0;
    while ((key != 'Z') && !db.fail() && (db >> key)) {
      ++mNumRecords;
      switch (key) {
      case 'D':   readDriver(db);  break;
      case 'S':   readString(db);  break;
      case 'P':   readPath(db);    break;
      case 'Z':                    break; // explicit EOF record
      default:
        mBuffer.clear();
        mBuffer << "Invalid key: " << key << ": record " << numRecords;
        db.setError(mBuffer.c_str());
        break;
      } // switch
    } // while
  } // if

  if (db.fail() && !db.eof()) {
    *errmsg = db.getErrmsg();
    return false;
  }
  return true;
} // bool DriverDatabase::load

void DriverDatabase::readDriver(ZistreamDB& db) {
  const char* filename = NULL, *type = NULL;
  const CarbonDBNode* driverPath = NULL;
  const CarbonDBNode* net = NULL;
  UInt32 lineNumber = 0;
  UInt32 depth = 0;
  mBuffer.clear();
  UtHashSet<const CarbonDBNode*> fanin;
  UtHashSet<Driver*> nested;

  if ((db >> lineNumber) && (db >> mBuffer) && (db >> depth) &&
      db.readPointer(&filename) && 
      db.readPointer(&driverPath) &&
      db.readPointer(&net) &&
      db.readPointer(&type) &&
      db.readPointerContainer(&fanin) &&
      db.readPointerContainer(&nested))
  {
    // Note that driverPath and net may be NULL, if they
    // exist, for some reason, in the drivers database but
    // are omitted from libdesign.symtab.db.  This might
    // happen because after the driver-dumping is done, the
    // compiler decides those nets are unwaveable.  I kind
    // of object to their omission from the symtab.db file but
    // that's what we have right now.  We must create
    // all the objects the same way regardless, so that the
    // pointer-mapping scheme in Z*streamDB works.

#if 0
    if (net != NULL) {
      net = carbonDBGetMaster(mDB, net);
    }
#endif
    SourceLocator loc = mSourceLocatorFactory->create(filename, lineNumber);
    StringAtom* bitAtom = mAtomicCache->intern(mBuffer.c_str());
    StringAtom* typeAtom = mAtomicCache->intern(type);
    Driver* driver = new Driver(driverPath, net, loc, bitAtom, typeAtom);
    db.mapPtr(driver);

    for (UtHashSet<const CarbonDBNode*>::UnsortedLoop p = fanin.loopUnsorted();
         !p.atEnd(); ++p)
    {
      const CarbonDBNode* node = *p;
      driver->addFanin(node);
      mNodeReaderMap[node].push_back(driver);
    }
    for (UtHashSet<Driver*>::SortedLoop p = nested.loopSorted();
         !p.atEnd(); ++p)
    {
      driver->addNested(*p);
    }

    // Only put top-level drivers in the net->driver map.  We can recurse
    // into nesting via the tree widget
    if (depth == 0) {
      mNodeDriverMap[net].push_back(driver);
    }
  } // if
} // void DriverDatabase::readDriver

void DriverDatabase::readString(ZistreamDB& db) {
  mBuffer.clear();
  if (db >> mBuffer) {
    StringAtom* atom = mAtomicCache->intern(mBuffer);
    db.mapPtr(atom->str());
  }
}

void DriverDatabase::readPath(ZistreamDB& db) {
  const CarbonDBNode* parent = NULL;
  const char* name = NULL;
  if (db.readPointer(&parent) && db.readPointer(&name)) {
    const CarbonDBNode* child = carbonDBFindChild(mDB, parent, name);
    db.mapPtr(child);
  }
}

//! Loop over all the drivers associated with a node
DriverLoop DriverDatabase::loopDrivers(const CarbonDBNode* node) {
  NodeDriverMap::iterator p = mNodeDriverMap.find(node);
  if (p == mNodeDriverMap.end()) {
    return DriverLoop(mEmptyDriverVec);
  }
  DriverVec& vec = p->second;
  return DriverLoop(vec);
}

//! does this node have any drivers?
bool DriverDatabase::hasDrivers(const CarbonDBNode* node) const {
  return mNodeDriverMap.find(node) != mNodeDriverMap.end();
}

//! count drivers
UInt32 DriverDatabase::numDrivers(const CarbonDBNode* node) const {
  UInt32 numDrivers = 0;
  NodeDriverMap::const_iterator p = mNodeDriverMap.find(node);
  if (p != mNodeDriverMap.end()) {
    const DriverVec& vec = p->second;
    numDrivers = vec.size();
  }
  return numDrivers;
}

//! Loop over all the drivers associated with a node
DriverLoop DriverDatabase::loopReaders(const CarbonDBNode* node) {
  NodeDriverMap::iterator p = mNodeReaderMap.find(node);
  if (p == mNodeReaderMap.end()) {
    return DriverLoop(mEmptyDriverVec);
  }
  DriverVec& vec = p->second;
  return DriverLoop(vec);
}

//! does this node have any drivers?
bool DriverDatabase::hasReaders(const CarbonDBNode* node) const {
  return mNodeReaderMap.find(node) != mNodeReaderMap.end();
}

//! count readers
UInt32 DriverDatabase::numReaders(const CarbonDBNode* node) const {
  UInt32 numReaders = 0;
  NodeDriverMap::const_iterator p = mNodeReaderMap.find(node);
  if (p != mNodeReaderMap.end()) {
    const DriverVec& vec = p->second;
    numReaders = vec.size();
  }
  return numReaders;
}

