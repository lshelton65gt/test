// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef __app_context___
#define __app_context___

#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include <QTableWidget>
#include <QHeaderView>
#include <QDebug>
#include <QMessageBox>

#define APP_NAME "Carbon Component Wizard"

enum HierarchyMode { eCarbonDBHierarchy, eCarbonPCHierarchy };

struct CarbonCfg;

class BlockTableUpdates
{
public:
  BlockTableUpdates(QTableWidget* t)
  {
    mTable = t;

    mUpdates = mTable->updatesEnabled();
    mSorting = mTable->isSortingEnabled();

    mTable->setUpdatesEnabled(false);
    mTable->setSortingEnabled(false);
  }
  virtual ~BlockTableUpdates()
  {
    mTable->setUpdatesEnabled(mUpdates);
    mTable->setSortingEnabled(mSorting);
  }

private:
  bool mUpdates;
  bool mSorting;
  QTableWidget* mTable;
};

class CQtContext;

class MaxSimApplicationContext;
class CowareApplicationContext;

class ApplicationContext
{
public:
  ApplicationContext(CarbonDB* db, CarbonCfg* cfg, CQtContext *cqt)
    : mDB(db)
    , mCfg(cfg)
    , mCQt(cqt)
    {
    }

  virtual ~ApplicationContext();

  void putDB(CarbonDB* db) {mDB=db;}
  void putCfg(CarbonCfg* cfg) {mCfg=cfg;}
  void putCQt(CQtContext *cqt) {mCQt=cqt;}
  CarbonDB* getDB() {return mDB;}
  CarbonCfg* getCfg() {return mCfg;}
  CQtContext *getCQt() {return mCQt;}
  CarbonSInt32 getRegisterTableColumnEnum(const UtString &name);

  virtual void setDocumentModified(bool value) = 0;


  virtual MaxSimApplicationContext *castMaxSim();
  virtual CowareApplicationContext *castCoware();

protected:
  typedef UtHashMap<UtString, CarbonSInt32> EnumMapType;
  EnumMapType mRegisterTableColumnEnumMap;

private:
  CarbonDB* mDB;
  CarbonCfg* mCfg;
  CQtContext *mCQt;
};

#endif

