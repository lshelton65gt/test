// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "gui/CQt.h"
#include "gui/CMainWindow.h"
#include <QMainWindow>
#include <QObject>
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "util/SourceLocator.h"
#include <QtGui>

class QAction;
class QMenu;
class QTextEdit;
class CarbonClkPanel;
class RegTabWidget;
class CarbonBuild;
class DriverDatabase;
class CarbonHBrowser;
class CarbonMemEdit;
class CarbonPortEdit;
class CarbonGenProp;
class CarbonProfileEdit;
class TextEdit;
class CGraph;

class MainWindow : public CMainWindow
{
  Q_OBJECT
    
public:
  enum ParseResult {eIODB, eFullDB, eCcfg, eNoMatch};

  MainWindow(CQtContext* cqt);
  
  ~MainWindow();

  static ParseResult parseFileName(const char* filename, UtString* baseName);
  bool loadFile(const char* filename, ParseResult);

  const char* getCcfgFilename();
  const char* getMakefileName();

  void showWindow();
  // Get the Qt Context
  CQtContext* getCQtContext() { return mCQt; }

protected:
  void closeEvent(QCloseEvent *event);

private slots:
  void newFile();
  void open();
  bool saveAs();
  void about();
  void help();
  void documentWasModified();
  void textEditDestroyed(QObject*);
  
public slots:
  bool save();
  void deleteCurrent();
  void tabSwitch(int);
  void createBrowser();

private:
  void buildLayout();
  QWidget* buildTab();
  void createActions();
  void createMenus();
  void createToolBars();
  void createStatusBar();
  void readSettings();
  void writeSettings();
  bool maybeSave();
  bool loadDatabase(const char* iodbFile, bool initPorts);
  bool loadConfig(const QString &fileName);
  bool saveFile(const char* fileName);
  void setCurrentFile(const QString &fileName);
  QString strippedName(const QString &fullFileName);

  void populateLoop(const char* label, CarbonDBNodeIter* iter,
                    QString* str);
  
  static eCarbonMsgCBStatus sMsgCallback(CarbonClientData, CarbonMsgSeverity,
                                         int number, const char* text,
                                         unsigned int len);

  CarbonMsgCBDataID* mMsgCallback;
  RegTabWidget* mRegTabWidget;
  CarbonBuild* mBuild;
  //CarbonClkPanel* mClkPanel;
  UtString mCurFile;
  UtString mMakefile;
  
  QMenu *mFileMenu;
  QMenu *mEditMenu;
  QMenu *mHelpMenu;
  QToolBar *mFileToolBar;
  QToolBar *mEditToolBar;
  QAction *mNewAct;
  QAction *mOpenAct;
  QAction *mSaveAct;
  QAction *mSaveAsAct;
  QAction *mExitAct;
  QAction *mCutAct;
  QAction *mDeleteAct;
  QAction *mCopyAct;
  QAction *mPasteAct;
  QAction *mAboutAct;
  QAction *mHelpAct;
  QAction *mBrowseAct;
  TextEdit* mErrorTextEditor;

  CQtContext* mCQt;
  AtomicCache* mAtomicCache;
  CQtAbortOverride mAssertOverride;
  SourceLocatorFactory mLocatorFactory;
  DriverDatabase* mDrivers;
  CarbonMemEdit* mMemEdit;
  CarbonGenProp* mGenProp;
  CarbonPortEdit* mPortEdit;
  CarbonProfileEdit* mProfileEdit;

  CarbonDB* mDB;
  CarbonCfgID mCfg;
  UInt32 mCurrentTabIndex;
  UInt32 mNumBrowsers;
  CGraph* mCGraph;
};

#endif
