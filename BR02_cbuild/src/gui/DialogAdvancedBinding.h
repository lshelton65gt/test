// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef DIALOGADVCONSTANT_H
#define DIALOGADVCONSTANT_H

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "cfg/CarbonCfg.h"
#include "gui/CQt.h"
#include "gui/CDialog.h"
#include "ui_DialogAdvancedBinding.h"

class DialogAdvancedBinding : public CDialog
{
  Q_OBJECT

public:
  DialogAdvancedBinding(QWidget *parent = 0);
  ~DialogAdvancedBinding();
  
  void initialize()
  {
    m_prefix.clear();
    m_suffix.clear();
    ui.lineEditPrefix->setText("");
    ui.lineEditSuffix->setText("");
  }

  bool hasPrefix()
  {
    m_prefix.clear();
    m_prefix << ui.lineEditPrefix->text();
    return m_prefix.length() > 0;
  }

  bool hasSuffix()
  {
    m_suffix.clear();
    m_suffix << ui.lineEditSuffix->text();
    return m_suffix.length() > 0;
  }

  const char* getSuffix()
  {
    m_suffix.clear();
    m_suffix << ui.lineEditSuffix->text();
    return m_suffix.c_str();
  }
  const char* getPrefix()
  {
    m_prefix.clear();
    m_prefix << ui.lineEditPrefix->text();
    return m_prefix.c_str();
  }

private:
  Ui::DialogAdvancedBindingClass ui;
  UtString m_prefix;
  UtString m_suffix;

public:
  // reimplemented function from CDialog
  virtual void setData(const UtString &data);

};

#endif // DIALOGADVCONSTANT_H
