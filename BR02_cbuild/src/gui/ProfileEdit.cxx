/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "ProfileEdit.h"
#include <QTreeWidgetItem>
#include <QVBoxLayout>
#include <QStringList>
#include <QTimer>
#include <cassert>
#include "util/SourceLocator.h"
#include "util/UtIStream.h"
#include "util/UtStringUtil.h"
#include "util/AtomicCache.h"
#include "util/UtStringArray.h"
#include "util/Zstream.h"
#include "textedit.h"
#include "gui/CDragDropTreeWidget.h"
#include <QtGui>
#define APP_NAME "Carbon Component Wizard"
#include "gui/CExprValidator.h"

CarbonProfileEdit::CarbonProfileEdit(CQtContext* cqt,
                                     CarbonCfgID cfg,
                                     AtomicCache* atomicCache,
                                     QAction* deleteAction,
                                     QStatusBar* statusBar)
  : mCfg(cfg),
    mAtomicCache(atomicCache),
    mDeleteAction(deleteAction),
    mEditStream(NULL),
    mEditTrigger(NULL),
    mEditChannel(NULL),
    mEditBucket(NULL),
    mEditNet(NULL),
    mCQt(cqt),
    mStatusBar(statusBar)
{
  CQtRecordBlocker blocker(cqt);
  mSelecting = 0;

  QStringList headers;
  mTree = cqt->dragDropTreeWidget("profileTree", true);
  //mTree->setSelectionMode(QAbstractItemView::ExtendedSelection);
  mTree->setAlternatingRowColors(true);
  mTree->setColumnCount(2);
  headers.clear();
  headers << "Name";
  headers << "Properties";

  mTree->setHeaderLabels(headers);
  CQT_CONNECT(mTree, dropReceived(const char*,QTreeWidgetItem*),
              this, dropNet(const char*,QTreeWidgetItem*));
  CQT_CONNECT(mTree, mapDropItem(QTreeWidgetItem**),
              this, mapDropItem(QTreeWidgetItem**));
  
  mName = cqt->lineEdit("profile_name");
  mColor = cqt->enumBox<CarbonCfgColorIO>("profile_color");

  mExpr = new CExprValidator(cqt, "profile_expr", false);

  mAddStream = cqt->pushButton("profile_add_stream", "Add Stream");
  mAddTrigger = cqt->pushButton("profile_add_trigger", "Add Trigger");
  mAddChannel = cqt->pushButton("profile_add_channel", "Add Channel ");
  mAddBucket = cqt->pushButton("profile_add_bucket", "Add Bucket");

  CQT_CONNECT(mTree, selectionChanged(), this, changeTreeSel());
  CQT_CONNECT(mColor, currentIndexChanged(int), this, changeColor(int));
  CQT_CONNECT(mName, textChanged(const QString&),
              this, changeName(const QString&));
  CQT_CONNECT(mExpr, textChanged(const QString&), this, changeExpr());
  CQT_CONNECT(mAddStream, clicked(), this, addStream());
  CQT_CONNECT(mAddTrigger, clicked(), this, addTrigger());
  CQT_CONNECT(mAddChannel, clicked(), this, addChannel());
  CQT_CONNECT(mAddBucket, clicked(), this, addBucket());

  enableWidgets();

  mDB = NULL;
} // CarbonProfileEdit::CarbonProfileEdit

CarbonProfileEdit::~CarbonProfileEdit() {
}

void CarbonProfileEdit::populate(CarbonDB* carbonDB) {
  mDB = carbonDB;
}

void CarbonProfileEdit::enableWidgets() {
  if (mEditBucket != NULL) {
    mExpr->setEnabled(true);
  }
  else if (mEditChannel != NULL) {
    mExpr->setEnabled(!isBucketChannel(mEditStream, mEditChannel));
  }
  else {
    mExpr->setEnabled((mEditStream != NULL) && (mEditNet == NULL));
  }
  if (mDeleteAction)
    mDeleteAction->setEnabled(mEditStream != NULL);
  mColor->setEnabled(mEditBucket != NULL);
  mName->setEnabled(mEditNet || mEditStream || mEditBucket || mEditChannel);
  mAddStream->setEnabled(true);
  mAddTrigger->setEnabled(mEditStream != NULL);
  mAddChannel->setEnabled(mEditStream != NULL);
  mAddBucket->setEnabled(mEditChannel != NULL);
}

void CarbonProfileEdit::clear() {
  for (int i = mTree->topLevelItemCount() - 1; i >= 0; --i) {
    QTreeWidgetItem* streamItem = mTree->topLevelItem(i);
    deleteStream(mStreamItemMap.find(streamItem));
  }
  INFO_ASSERT(mStreamItemMap.empty(), "Should have emptied stream map");
  INFO_ASSERT(mTriggerItemMap.empty(), "Should have emptied trigger map");
  INFO_ASSERT(mBucketItemMap.empty(), "Should have emptied bucket map");
  INFO_ASSERT(mChannelItemMap.empty(), "Should have emptied channel map");
  INFO_ASSERT(mNetItemMap.empty(), "Should have emptied net map");
  INFO_ASSERT(mTree->topLevelItemCount() == 0, "should have emptied tree");
}

void CarbonProfileEdit::read() {
  for (unsigned int p = 0, np = mCfg->numPStreams(); p < np; ++p) {
    CarbonCfgPStream* pstream = mCfg->getPStream(p);
    addStream(pstream);
    for (UInt32 n = 0, nn = pstream->numNets(); n < nn; ++n) {
      CarbonCfgPNet* pnet = pstream->getNet(n);
      addNet(pstream, pnet);
    }
    for (UInt32 t = 0, nt = pstream->numTriggers(); t < nt; ++t) {
      CarbonCfgPTrigger* ptrigger = pstream->getTrigger(t);
      addTrigger(pstream, ptrigger);
    }
  }
  mTree->resizeColumnToContents(0);

  //mTree->resizeColumnToContents(0);
  //mTree->resizeColumnToContents(1);
  //mTree->resizeColumnToContents(2);
}

QTreeWidgetItem* CarbonProfileEdit::addStream(CarbonCfgPStream* pstream) {
  QTreeWidgetItem* item = new QTreeWidgetItem(mTree);
  mTree->expandItem(item);
  mStreamItemMap.map(pstream, item);
  QTreeWidgetItem* netsItem = new QTreeWidgetItem(item);
  netsItem->setText(0, "Nets");
  QTreeWidgetItem* triggersItem = new QTreeWidgetItem(item);
  triggersItem->setText(0, "Triggers");
  setStreamTreeText(pstream);

  return item;
}

// Tell the Expr Validator about the set of legal names for this stream
void CarbonProfileEdit::setExprIdents(CarbonCfgPStream* pstream) {
  mExpr->clearIdents();
  for (UInt32 i = 0, n = pstream->numNets(); i < n; ++i) {
    CarbonCfgPNet* pnet = pstream->getNet(i);
    mExpr->addIdent(pnet->getName());
  }

  // Also about our builtin methods
  mExpr->addIdent("posedge");
  mExpr->addIdent("negedge");
  mExpr->addIdent("changed");
}

void CarbonProfileEdit::setExprItemText(CarbonCfgPStream* pstream,
                                        QTreeWidgetItem* item,
                                        const char* label,
                                        const char* expr)
{
  setExprIdents(pstream);
  mExpr->setText(expr);

  // By default, copy the colors from column 0
  QColor color = item->textColor(0);

  UtString buf(label);
  if (mExpr->validate()) {
    buf << expr;
  }
  else {
    buf << "***" << mExpr->getErrorText() << ": " << expr;
    color = QColor(mExpr->getColor());
  }
  item->setText(1, buf.c_str());
  item->setTextColor(1, color);
} // void CarbonProfileEdit::setExprItemText

void CarbonProfileEdit::setStreamTreeText(CarbonCfgPStream* pstream) {
  QTreeWidgetItem* item = mStreamItemMap.find(pstream);
  item->setText(0, pstream->getName());
}

void CarbonProfileEdit::setTriggerTreeText(CarbonCfgPStream* pstream,
                                           CarbonCfgPTrigger* ptrigger)
{
  QTreeWidgetItem* item = mTriggerItemMap.find(ptrigger);
  item->setText(0, "Trigger expr:");
  setExprItemText(pstream, item, "", ptrigger->getTriggerExpr());
}

void CarbonProfileEdit::setChannelTreeText(CarbonCfgPStream* pstream,
                                           CarbonCfgPChannel* pchannel)
{
  QTreeWidgetItem* item = mChannelItemMap.find(pchannel);
  item->setText(0, pchannel->getName());

  // Channels must have blank expressions if they have buckets
  const char* expr = StringUtil::skip(pchannel->getExpr());
  if (isBucketChannel(pstream, pchannel)) {
    UtString buf;
    buf << pchannel->numBuckets() << " buckets";
    item->setText(1, buf.c_str());
  }
  else {
    setExprItemText(pstream, item, "Filter expr: ", expr);
  }
} // void CarbonProfileEdit::setChannelTreeText

void CarbonProfileEdit::setNetTreeText(CarbonCfgPNet* pnet) {
  QTreeWidgetItem* item = mNetItemMap.find(pnet);
  item->setText(0, pnet->getName());
  UtString buf(pnet->getPath());
  buf << " (" << pnet->getWidth() << ")";
  item->setText(1, buf.c_str());
}

void CarbonProfileEdit::setBucketTreeText(CarbonCfgPStream* pstream,
                                          CarbonCfgPBucket* pbucket)
{
  QTreeWidgetItem* item = mBucketItemMap.find(pbucket);
  item->setText(0, pbucket->getName());
  QColor color = Qt::red;

  // dark colors look better in Qt trees.  The colors might not
  // match Maxsim exactly.
  //
  // orange/purple/brown do not have symbolic Qt names.  The RGB
  // values are from /usr/lib/X11/rgb.txt
  switch (pbucket->getColor()) {
  case eCarbonCfgRed:       color = Qt::red;              break;
  case eCarbonCfgBlue:      color = Qt::darkBlue;         break;
  case eCarbonCfgGreen:     color = Qt::darkGreen;        break;
  case eCarbonCfgYellow:    color = Qt::yellow;           break;
  case eCarbonCfgOrange:    color = QColor(255, 165, 0);  break;
  case eCarbonCfgBrown:     color = QColor(165, 42, 42);  break;
  case eCarbonCfgPurple:    color = QColor(160, 32, 240); break;
  case eCarbonCfgLightGray:    color = QColor(211, 211,211); break;  
  case eCarbonCfgLightBlue:    color = QColor(173, 216,230); break;
  case eCarbonCfgLightCyan:    color = QColor(224, 255,255); break;
  case eCarbonCfgLightSeaGreen:color = QColor(32,  178,170); break;
  case eCarbonCfgLightCoral:   color = QColor(240, 128,128); break;
  case eCarbonCfgLightYellow1: color = QColor(255, 255,224); break;
  case eCarbonCfgLightSalmon1: color = QColor(255, 160,122); break;
  case eCarbonCfgLightGreen:   color = QColor(144, 238,144); break;

  };
  item->setTextColor(0, color);
  setExprItemText(pstream, item, "", pbucket->getExpr());
}

QTreeWidgetItem* CarbonProfileEdit::getNetsItem(CarbonCfgPStream* pstream) {
  QTreeWidgetItem* item = mStreamItemMap.find(pstream);
  return item->child(0);
}

QTreeWidgetItem* CarbonProfileEdit::getTriggersItem(CarbonCfgPStream* pstream)
{
  QTreeWidgetItem* item = mStreamItemMap.find(pstream);
  return item->child(1);
}

// When the set of available nets changes, we need to re-evaluate the
// validity of each expression in the stream branch.
void CarbonProfileEdit::resetExprText(CarbonCfgPStream* pstream) {
  for (UInt32 i = 0; i < pstream->numTriggers(); ++i) {
    CarbonCfgPTrigger* ptrigger = pstream->getTrigger(i);
    setExprItemText(pstream, mTriggerItemMap.find(ptrigger), "",
                    ptrigger->getTriggerExpr());

    for (UInt32 i = 0; i < pstream->numChannels(); ++i) {
      CarbonCfgPChannel* pchannel = ptrigger->getChannel(i);
      if (isBucketChannel(pstream, pchannel)) {
        for (UInt32 j = 0; j < pchannel->numBuckets(); ++j) {
          CarbonCfgPBucket* pbucket = pchannel->getBucket(j);
          setExprItemText(pstream, mBucketItemMap.find(pbucket), "",
                          pbucket->getExpr());
        }
      }
      else {
        setExprItemText(pstream, mChannelItemMap.find(pchannel),
                        "Filter expr: ", pchannel->getExpr());
      }
    }
  }
}

QTreeWidgetItem* CarbonProfileEdit::addNet(CarbonCfgPStream* pstream,
                                           CarbonCfgPNet* pnet)
{
  QTreeWidgetItem* item = new QTreeWidgetItem(getNetsItem(pstream));
  mTree->expandItem(item);
  mNetItemMap.map(pnet, item);
  setNetTreeText(pnet);
  return item;
}

QTreeWidgetItem* CarbonProfileEdit::addTrigger(CarbonCfgPStream* pstream,
                                               CarbonCfgPTrigger* ptrigger)
{
  QTreeWidgetItem* item = new QTreeWidgetItem(getTriggersItem(pstream));
  mTree->expandItem(item);
  mTriggerItemMap.map(ptrigger, item);
  setTriggerTreeText(pstream, ptrigger);

  for (UInt32 c = 0, nc = pstream->numChannels(); c < nc; ++c) {
    CarbonCfgPChannel* pchannel = ptrigger->getChannel(c);
    addChannel(pstream, ptrigger, pchannel);
    setChannelTreeText(pstream, pchannel);
  }

  return item;
}

QTreeWidgetItem* CarbonProfileEdit::addChannel(CarbonCfgPStream* pstream,
                                               CarbonCfgPTrigger* ptrigger,
                                               CarbonCfgPChannel* pchannel)
{
  QTreeWidgetItem* triggerItem = mTriggerItemMap.find(ptrigger);
  mTree->expandItem(triggerItem);
  QTreeWidgetItem* channelItem = new QTreeWidgetItem(triggerItem);
  mTree->expandItem(channelItem);
  mChannelItemMap.map(pchannel, channelItem);

  for (UInt32 i = 0; i < pchannel->numBuckets(); ++i) {
    addBucket(pstream, pchannel, pchannel->getBucket(i));
  }

  return channelItem;
}

QTreeWidgetItem* CarbonProfileEdit::addBucket(CarbonCfgPStream* pstream,
                                              CarbonCfgPChannel* pchannel,
                                              CarbonCfgPBucket* pbucket)
{
  QTreeWidgetItem* item = new QTreeWidgetItem(mChannelItemMap.find(pchannel));
  mTree->expandItem(item);
  mBucketItemMap.map(pbucket, item);
  setBucketTreeText(pstream, pbucket);
  return item;
}


void CarbonProfileEdit::changeName(const QString& qeditName) {
  UtString resolvedName, editName;
  editName << qeditName;

  if (mSelecting == 0) {
    if (mEditBucket != NULL) {
      mEditChannel->getUniqueBucketName(&resolvedName, editName.c_str());
      mEditChannel->changeBucketName(mEditBucket, resolvedName.c_str());
      setBucketTreeText(mEditStream, mEditBucket);
    }
    else if (mEditChannel != NULL) {
      mEditStream->getUniqueChannelName(&resolvedName, editName.c_str());
      SInt32 idx = mEditStream->findChannelIndex(mEditChannel->getName());
      INFO_ASSERT(idx >= 0, "cannot find channel in stream");
      mEditStream->changeChannelName(idx, resolvedName.c_str());
      for (UInt32 i = 0; i < mEditStream->numTriggers(); ++i) {
        CarbonCfgPTrigger* ptrigger = mEditStream->getTrigger(i);
        CarbonCfgPChannel* pchannel = ptrigger->getChannel(idx);
        setChannelTreeText(mEditStream, pchannel);
      }
    }
    else if (mEditNet != NULL) {
      mEditStream->getUniqueNetName(&resolvedName, editName.c_str());
      mEditStream->changeNetName(mEditNet, resolvedName.c_str());
      setNetTreeText(mEditNet);
      resetExprText(mEditStream);
    }
    else if (mEditStream != NULL) {
      mCfg->getUniquePStreamName(&resolvedName, editName.c_str());
      mCfg->changePStreamName(mEditStream, resolvedName.c_str());
      setStreamTreeText(mEditStream);
    }
  } // if
} // void CarbonProfileEdit::changeName

void CarbonProfileEdit::changeExpr() {
  if (mSelecting == 0) {
    const char* expr = mExpr->getValue();
    if (mEditBucket != NULL) {
      mEditBucket->putExpr(expr);
      setBucketTreeText(mEditStream, mEditBucket);
    }
    else if (mEditChannel != NULL) {
      mEditChannel->putExpr(expr);
      setChannelTreeText(mEditStream, mEditChannel);
    }
    else if (mEditTrigger != NULL) {
      mEditTrigger->putTriggerExpr(expr);
      setTriggerTreeText(mEditStream, mEditTrigger);
    }
  }
}

void CarbonProfileEdit::changeColor(int index) {
  if ((mEditBucket != NULL) && (mSelecting == 0)) {
    CarbonCfgColor color = (CarbonCfgColor) index;
    mEditBucket->putColor(color);
    setBucketTreeText(mEditStream, mEditBucket);
  }
}

void CarbonProfileEdit::changeTreeSel() {
  CQtRecordBlocker blocker(mCQt);
  ++mSelecting;

  QTreeItemList sel = mTree->selectedItems();
  mEditBucket = NULL;
  mEditChannel = NULL;
  mEditNet = NULL;
  mEditStream = NULL;
  mEditTrigger = NULL;
  QTreeWidgetItem* item = NULL;

  UtString dragText;

  // Find the currently selected register group/name and populate the editing
  // widgets
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    item = *p;

    if (mBucketItemMap.test(item)) {
      mEditBucket = mBucketItemMap.find(item);
      QTreeWidgetItem* channelItem = item->parent();
      QTreeWidgetItem* triggerItem = channelItem->parent();
      mEditTrigger = mTriggerItemMap.find(triggerItem);
      QTreeWidgetItem* streamItem = triggerItem->parent()->parent();
      mEditChannel = mChannelItemMap.find(channelItem);
      mEditStream = mStreamItemMap.find(streamItem);
      setExprIdents(mEditStream);
      mExpr->setText(mEditBucket->getExpr());
      mName->setText(mEditBucket->getName());
      mColor->setCurrentIndex(mEditBucket->getColor());
    }
    else if (mChannelItemMap.test(item)) {
      QTreeWidgetItem* triggerItem = item->parent();
      QTreeWidgetItem* streamItem = triggerItem->parent()->parent();
      mEditTrigger = mTriggerItemMap.find(triggerItem);
      mEditChannel = mChannelItemMap.find(item);
      mEditStream = mStreamItemMap.find(streamItem);
      setExprIdents(mEditStream);
      mExpr->setText(mEditChannel->getExpr());
      mName->setText(mEditChannel->getName());
    }
    else if (mNetItemMap.test(item)) {
      QTreeWidgetItem* streamItem = item->parent()->parent();
      mEditNet = mNetItemMap.find(item);
      mEditStream = mStreamItemMap.find(streamItem);
      mTree->putDragData(mEditNet->getPath());
      mName->setText(mEditNet->getName());
    }
    else if (mTriggerItemMap.test(item)) {
      QTreeWidgetItem* streamItem = item->parent()->parent();
      mEditTrigger = mTriggerItemMap.find(item);
      mEditStream = mStreamItemMap.find(streamItem);
      setExprIdents(mEditStream);
      mExpr->setText(mEditTrigger->getTriggerExpr());
    }
    else {
      // It might be the nets or channels items.  Try the parent.
      QTreeWidgetItem* parent = item->parent();
      if (parent != NULL) {
        item = parent;
      }
      mEditStream = mStreamItemMap.find(item);
      mExpr->setText("");
      mExpr->setEnabled(false);
      mName->setText(mEditStream->getName());
    }
  }
  enableWidgets();
  --mSelecting;
} // void CarbonProfileEdit::changeTreeSel

QWidget* CarbonProfileEdit::buildLayout() {
  // Master trees of registers and memories
  QWidget* tree = mCQt->makeGroup("Profile Streams (drag and drop nets from Design Hierarchy)",
                                  "profile-tree", mTree, false);

  // Common controls between registers and memories
  QWidget* name = CQt::makeGroup("Name", mName, true);

  QLayout* hbox = CQt::hbox(name, mAddStream, mAddTrigger, mAddChannel,
                            mAddBucket, mColor);
  QLayout* vbox = CQt::vbox();
  QWidget* expr = CQt::makeGroup("Expression", mExpr->getWidget(), true);
  *vbox << tree << hbox << expr;
  return CQt::makeWidget(vbox);
}


bool CarbonProfileEdit::isDeleteActive() const {
  return mEditStream != NULL;
}

bool CarbonProfileEdit::isBucketChannel(CarbonCfgPStream* pstream,
                                        CarbonCfgPChannel* pchannel)
{
  SInt32 channelIndex = pstream->findChannelIndex(pchannel->getName());
  INFO_ASSERT(channelIndex >= 0, "Cannot find channel");
  return pstream->isBucketChannel(channelIndex);
}                                        

void CarbonProfileEdit::deleteBucket(CarbonCfgPStream* pstream,
                                     CarbonCfgPChannel* pchannel,
                                     CarbonCfgPBucket* pbucket,
                                     bool updateChannel)
{
  QTreeWidgetItem* bucketItem = mBucketItemMap.find(pbucket);
  mBucketItemMap.unmap(pbucket);
  mTree->deleteItem(bucketItem);
  pchannel->removeBucket(pbucket);

  // If this is the last bucket, and none of the other triggers images
  // of this channel have any buckets, then show their expressions
  if (updateChannel) {
    if ((pchannel->numBuckets() == 0) && !isBucketChannel(pstream, pchannel)) {
      SInt32 channelIndex = pstream->findChannelIndex(pchannel->getName());
      INFO_ASSERT(channelIndex >= 0, "Cannot find channel");

      for (UInt32 t = 0, nt = pstream->numTriggers(); t < nt; ++t) {
        CarbonCfgPTrigger* ptrigger = pstream->getTrigger(t);
        pchannel = ptrigger->getChannel(channelIndex);
        setChannelTreeText(pstream, pchannel);
      }
    }
  }
} // void CarbonProfileEdit::deleteBucket

// Delete a channel as part of a trigger -- doesn't remove it from
// other triggers, and the channel name remains part of the stream.
// This routine should only be called from deleteTrigger -- when 
// cleaning out a single trigger, or from deleteChannel, which will
// remove the channel from every trigger.
void CarbonProfileEdit::deleteChannelHelper(CarbonCfgPStream* pstream,
                                            CarbonCfgPChannel* pchannel)
{
  for (SInt32 i = pchannel->numBuckets() - 1; i >= 0; --i) {
    deleteBucket(pstream, pchannel, pchannel->getBucket(i), false);
  }
  QTreeWidgetItem* channelItem = mChannelItemMap.find(pchannel);
  mChannelItemMap.unmap(pchannel);
  mTree->deleteItem(channelItem);
}

// Delete a channel out of every trigger in which it appears, and
// remove its name from the stream
void CarbonProfileEdit::deleteChannel(CarbonCfgPStream* pstream,
                                      CarbonCfgPChannel* pchannel)
{
  SInt32 channelIndex = pstream->findChannelIndex(pchannel->getName());
  INFO_ASSERT(channelIndex >= 0, "Cannot find channel");

  // PStream::removeChannel will delete this channel for every trigger.
  // But first, walk all the doomed channels and buckets and remove
  // any tree items associated with those

  for (SInt32 i = pstream->numTriggers() - 1; i >= 0; --i) {
    CarbonCfgPTrigger* ptrigger = pstream->getTrigger(i);
    deleteChannelHelper(pstream, ptrigger->getChannel(channelIndex));
  }
  pstream->removeChannel(channelIndex);
}

// delete a trigger, including its channels, but do not change the
// master set of channel names, which is owned by the stream
void CarbonProfileEdit::deleteTrigger(CarbonCfgPStream* pstream,
                                      CarbonCfgPTrigger* ptrigger)
{
  UtArray<UInt32> wasBucketChanVec(pstream->numChannels());
  for (SInt32 i = pstream->numChannels() - 1; i >= 0; --i) {
    wasBucketChanVec[i] = pstream->isBucketChannel(i);
    deleteChannelHelper(pstream, ptrigger->getChannel(i));
  }
  QTreeWidgetItem* triggerItem = mTriggerItemMap.find(ptrigger);
  mTriggerItemMap.unmap(ptrigger);
  mTree->deleteItem(triggerItem);
  pstream->removeTrigger(ptrigger);

  // If deleting this trigger caused any bucket-channels to cease
  // to be bucket channels, we will need to update the channel tree text.
  for (SInt32 i = pstream->numChannels() - 1; i >= 0; --i) {
    if (wasBucketChanVec[i] && !pstream->isBucketChannel(i)) {
      for (UInt32 t = 0, nt = pstream->numTriggers(); t < nt; ++t) {
        CarbonCfgPTrigger* ptrigger = pstream->getTrigger(t);
        CarbonCfgPChannel* pchannel = ptrigger->getChannel(i);
        setChannelTreeText(pstream, pchannel);
      }
    }
  }
} // void CarbonProfileEdit::deleteTrigger

void CarbonProfileEdit::deleteNet(CarbonCfgPStream* pstream,
                                  CarbonCfgPNet* pnet,
                                  bool updateExprText)
{
  QTreeWidgetItem* netItem = mNetItemMap.find(pnet);
  mNetItemMap.unmap(pnet);
  mTree->deleteItem(netItem);
  pstream->removeNet(pnet);
  if (updateExprText) {
    resetExprText(pstream);
  }
}

void CarbonProfileEdit::deleteStream(CarbonCfgPStream* pstream) {
  for (SInt32 i = pstream->numNets() - 1; i >= 0; --i) {
    deleteNet(pstream, pstream->getNet(i), false);
  }
  for (SInt32 i = pstream->numTriggers() - 1; i >= 0; --i) {
    deleteTrigger(pstream, pstream->getTrigger(i));
  }
  QTreeWidgetItem* streamItem = mStreamItemMap.find(pstream);
  mStreamItemMap.unmap(pstream);
  mTree->deleteItem(streamItem);
  mCfg->removePStream(pstream);
}

void CarbonProfileEdit::deleteCurrent() {
  CQtRecordBlocker blocker(mCQt);

  if (mEditBucket != NULL) {
    deleteBucket(mEditStream, mEditChannel, mEditBucket, true);
  }
  else if (mEditChannel != NULL) {
    deleteChannel(mEditStream, mEditChannel);
  }
  else if (mEditTrigger != NULL) {
    deleteTrigger(mEditStream, mEditTrigger);
  }
  else if (mEditNet != NULL) {
    deleteNet(mEditStream, mEditNet, true);
  }
  else if (mEditStream != NULL) {
    deleteStream(mEditStream);
  }
  changeTreeSel();
} // void CarbonProfileEdit::deleteCurrent

void CarbonProfileEdit::addStream() {
  UtString buf;
  mCfg->getUniquePStreamName(&buf, "stream_0");
  CarbonCfgPStream* pstream = mCfg->addPStream(buf.c_str());

  mTree->deselectAll();
  QTreeWidgetItem* item = addStream(pstream);

  mTree->setItemSelected(item, true);
  addTrigger();
  addChannel();
  mTree->setItemSelected(item, true);

  mTree->scrollToItem(item, QAbstractItemView::PositionAtTop);

  if (mCfg->numPStreams() == 1) {
    mTree->resizeColumnToContents(0);
  }

  enableWidgets();
}

void CarbonProfileEdit::addTrigger() {
  CQtRecordBlocker blocker(mCQt);

  if (mEditStream != NULL) {
    mTree->expandItem(getTriggersItem(mEditStream));

    // Find a group name that is not yet used
    CarbonCfgPTrigger* ptrigger = mEditStream->addTrigger();
    ptrigger->putTriggerExpr("1");
    QTreeWidgetItem* triggerItem = addTrigger(mEditStream, ptrigger);
    mTree->deselectAll();
    mTree->setItemSelected(triggerItem, true);
    mTree->scrollToItem(triggerItem, QAbstractItemView::EnsureVisible);
    enableWidgets();
  }
}

// Although channels are owned by triggers, all triggers must
// have exactly the same set of channels.  The trigger names are
// kept in the stream.
void CarbonProfileEdit::addChannel() {
  CQtRecordBlocker blocker(mCQt);

  if (mEditStream != NULL) {
    // Find a group name that is not yet used
    UInt32 channelIndex = mEditStream->numChannels();
    UtString channelName;
    mEditStream->getUniqueChannelName(&channelName, "channel_0");
    mEditStream->addChannel(channelName.c_str());

    QTreeWidgetItem* selChannel = NULL;

    for (UInt32 t = 0, nt = mEditStream->numTriggers(); t < nt; ++t) {
      CarbonCfgPTrigger* ptrigger = mEditStream->getTrigger(t);
      CarbonCfgPChannel* pchannel = ptrigger->addChannel(channelName.c_str());
      QTreeWidgetItem* channelItem = addChannel(mEditStream, ptrigger,
                                                pchannel);
      if (ptrigger == mEditTrigger) {
        selChannel = channelItem;
      }

      // do not attempt to update the channel-tree text here because not
      // all the triggers have been updated yet, so the test to see whether
      // any trigger has buckets for a particular channel cannot work.
      // Do that in a second loop.
    }

    // reset the channel tree text now that all the triggers are established.
    for (UInt32 t = 0, nt = mEditStream->numTriggers(); t < nt; ++t) {
      CarbonCfgPTrigger* ptrigger = mEditStream->getTrigger(t);
      CarbonCfgPChannel* pchannel = ptrigger->getChannel(channelIndex);
      setChannelTreeText(mEditStream, pchannel);
    }

    mTree->deselectAll();
    if (selChannel != NULL) {
      mTree->setItemSelected(selChannel, true);
      mTree->scrollToItem(selChannel, QAbstractItemView::EnsureVisible);
    }
    enableWidgets();
  } // if
} // void CarbonProfileEdit::addChannel

void CarbonProfileEdit::addBucket() {
  CQtRecordBlocker blocker(mCQt);

  // Find a group name that is not yet used
  if (mEditChannel != NULL) {
    mTree->expandItem(mChannelItemMap.find(mEditChannel));

    UtString name;
    mEditChannel->getUniqueBucketName(&name, "bucket_0");

    // Assume "red" is taken up by the auto-generated default bucket.
    // Walk through the existing buckets and pick a color that is not
    // yet covered
    UtHashMap<int, int> colorsTaken;
    colorsTaken[(int) eCarbonCfgRed] = 1; // default-bucket color
    for (UInt32 i = 0; i < mEditChannel->numBuckets(); ++i) {
      CarbonCfgPBucket* pbucket = mEditChannel->getBucket(i);
      ++colorsTaken[(int) pbucket->getColor()];
    }
    CarbonCfgColor bestColor = eCarbonCfgRed;
    int bestColorFrequency = colorsTaken[(int) eCarbonCfgRed];
    for (UInt32 i = 0; gCarbonCfgColors[i] != NULL; ++i) {
      CarbonCfgColor color = (CarbonCfgColor) i;
      int colorFrequency = colorsTaken[i];
      if (colorFrequency < bestColorFrequency) {
        bestColorFrequency = colorFrequency;
        bestColor = color;
      }
    }
    CarbonCfgPBucket* pbucket = mEditChannel->addBucket(name.c_str());
    pbucket->putColor(bestColor);

    SInt32 channelIndex = mEditStream->findChannelIndex(mEditChannel->getName());
    INFO_ASSERT(channelIndex >= 0, "Cannot find channel");
    for (UInt32 t = 0, nt = mEditStream->numTriggers(); t < nt; ++t) {
      CarbonCfgPTrigger* ptrigger = mEditStream->getTrigger(t);
      CarbonCfgPChannel* pchannel = ptrigger->getChannel(channelIndex);
      if (pchannel != mEditChannel) {
        setChannelTreeText(mEditStream, pchannel);
      }
    }

    pbucket->putExpr("1");
    QTreeWidgetItem* item = addBucket(mEditStream, mEditChannel, pbucket);
    setChannelTreeText(mEditStream, mEditChannel);
    mTree->deselectAll();
    mTree->setItemSelected(item, true);
    mTree->scrollToItem(item, QAbstractItemView::EnsureVisible);
    enableWidgets();
  }
}

void CarbonProfileEdit::mapDropItem(QTreeWidgetItem** item) {
  QTreeWidgetItem* streamItem = *item;

  // If the user dropped over a net, bucket, or channel, pop up to the
  // stream
  QTreeWidgetItem* parent;
  if (streamItem != NULL) {
    while ((parent = streamItem->parent()) != NULL) {
      streamItem = parent;
    }
  }
  else {
    // Drop over empty space goes into the last stream
    UInt32 nstreams = mTree->topLevelItemCount();
    if (nstreams > 0) {
      streamItem = mTree->topLevelItem(nstreams - 1);
    }
  }

  if (streamItem != NULL) {
    CarbonCfgPStream* pstream = mStreamItemMap.find(streamItem);
    mTree->expandItem(streamItem);
    *item = getNetsItem(pstream);
  }  
} // void CarbonProfileEdit::mapDropItem

void CarbonProfileEdit::dropNet(const char* path, QTreeWidgetItem* streamItem)
{
  // If we didn't land over a stream, pick the last one.  If there are
  // no streams then create one.

  UtString group;

  // multiple drops may be separated by newlines
  UtIStringStream is(path);
  UtString buf;
  UInt32 numDropped = 0;
  mTree->deselectAll();
  QTreeWidgetItem* item = NULL;
  CarbonCfgPStream* pstream = NULL;
  UtString warning;

  // If the user dropped over a net, bucket, or channel, pop up to the
  // stream
  QTreeWidgetItem* parent;
  if (streamItem != NULL) {
    while ((parent = streamItem->parent()) != NULL) {
      streamItem = parent;
    }
  }

  while (is.getline(&buf)) {
    StringUtil::strip(&buf, "\n");
    path = buf.c_str();
    
    if (mDB == NULL) {
      warning << "Design Database not loaded\n";
      break;
    }

    const CarbonDBNode* node = carbonDBFindNode(mDB, path);
    if (node == NULL) {
      warning << "No such net in the design: " << buf.c_str() << "\n";
    }
    else if (!carbonDBCanBeCarbonNet(mDB, node))
    {
      warning << "Node " << buf
              << " is a composite and must be a net a net\n";
    }
    else if (! carbonDBIsVector(mDB, node) &&
             ! carbonDBIsScalar(mDB, node))
    {
      warning << "Net " << buf << " is not a vector or scalar\n";
    }
    else if (! carbonDBIsVisible(mDB, node)) {
      warning << "Net " << buf << " is not visible -- declare it observable\n";
    }
    else {
      if (streamItem == NULL) {
        // If there are no streams yet, then automatically create one,
        // otherwise take the last one.
        UInt32 nstreams = mTree->topLevelItemCount();
        if (nstreams == 0) {
          addStream();
          nstreams = 1;
        }
        streamItem = mTree->topLevelItem(nstreams - 1);
      }
      pstream = mStreamItemMap.find(streamItem);

      QTreeWidgetItem* nets = getNetsItem(pstream);
      mTree->expandItem(streamItem);
      mTree->expandItem(nets);

      // Unlike the 'registers' panel, we will allow a net to appear
      // in multiple profile streams.  So there is no need to remove
      // it if we already know about it. 

      UtString name;
      pstream->getUniqueNetName(&name, carbonDBNodeGetLeafName(mDB, node));
      UInt32 width = carbonDBGetWidth(mDB, node);
      CarbonCfgPNet* pnet = pstream->addNet(name.c_str(), path, width);
      item = addNet(pstream, pnet);
      mTree->expandItem(getNetsItem(pstream));
      mTree->setItemSelected(item, true);
      mTree->scrollToItem(item, QAbstractItemView::EnsureVisible);
      ++numDropped;
    } // else
  } // while

  if (pstream != NULL) {
    resetExprText(pstream);
  }

  if (! warning.empty()) {
    mCQt->warning(this, warning.c_str());
  }

  if (item != NULL) {
    mTree->scrollToItem(item);
  }
  changeTreeSel();
} // void CarbonProfileEdit::dropNet
