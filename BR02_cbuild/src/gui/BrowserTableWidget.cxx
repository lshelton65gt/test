/*********************************************************
Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "BrowserTableWidget.h"
#include "BrowserTreeWidget.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonMakerContext.h"
#include "DirectivesEditor.h"
#include <QtGui>
#include "shell/CarbonDatabasePriv.h"
#include "symtab/STSymbolTableNode.h"
#include "DesignXML.h"
#include "gui/CQt.h"
#include "cfg/carbon_cfg.h"

enum ColumnTypes { Nets=0, NetRange, NetDirection, NetSize, NetType, NetAttributes, NetFlags } ;

class MyTreeWidgetItem : public QTreeWidgetItem
{
public:
  MyTreeWidgetItem(CarbonDB* db, QTreeWidget* parent) : QTreeWidgetItem(parent)
  {
    mDB = db;
  }
  MyTreeWidgetItem(CarbonDB* db, QTreeWidgetItem *parent) : QTreeWidgetItem(parent)
  {
    mDB = db;
  }

private:
  bool operator<(const QTreeWidgetItem &other)const
  {
    int column = treeWidget()->sortColumn();
    ColumnTypes colType = (ColumnTypes)column;

    switch (colType)
    {
    case Nets:
      {
        QVariant qv1 = this->data(0, Qt::UserRole);
        QVariant qv2 = other.data(0, Qt::UserRole);
        CarbonDBNode* node1 = (CarbonDBNode*)qv1.value<void*>(); 
        CarbonDBNode* node2 = (CarbonDBNode*)qv2.value<void*>(); 
        CarbonCowareHBrowserNodeSorter sorter(mDB);
        return sorter(node1,node2);
      }
      break;
    case NetSize:
      bool ok;
      text(column).toInt(&ok, 10);
      if (ok)
        return text(column).toInt() < other.text(column).toInt();
      break;
    default:
    case NetRange:
    case NetDirection:
    case NetType:
    case NetAttributes:
    case NetFlags:
      break; // All fall through to text compare
    }
    return text(column) < other.text(column);
  }
  CarbonDB* mDB;
};

BrowserTableWidget::BrowserTableWidget(QWidget *parent)
: CDragDropTreeWidget(false, parent)
{
  setContextMenuPolicy(Qt::CustomContextMenu);
  mHierMode = eCarbonDBHierarchy;
  mShowTypes = eAllNets;
  ui.setupUi(this);
  clearAll();
  setSortingEnabled(true);
  mCtx = 0;
  createActions();
  mParentNode = NULL;
  mParentTop = NULL;
  mParentInst = NULL;
  mExplicitObserves = false;
  mExplicitDeposits = false;
  mTree = NULL;

  CQT_CONNECT(this, customContextMenuRequested(QPoint), this, showContextMenu(QPoint));
  CQT_CONNECT(this, itemExpanded(QTreeWidgetItem*), this, itemExpanded(QTreeWidgetItem*));
  CQT_CONNECT(this, itemSelectionChanged(), this, itemSelectionChanged());
}

BrowserTableWidget::~BrowserTableWidget()
{
}

void BrowserTableWidget::applyFilters(const QString& filterStr, bool explicitObserves, bool explicitDeposits)
{
  mExplicitObserves = explicitObserves;
  mExplicitDeposits = explicitDeposits;
  UtString buf;
  buf << filterStr;
  mFilterPattern.putExpr(buf.c_str());

  if (mParentNode)
    nodeSelectionChanged(mParentNode, mParentTop, mParentInst);
}

void BrowserTableWidget::showContextMenu(const QPoint &pos)
{
  QTreeWidgetItem* item = itemAt(pos);
  if (item)
  {
    QVariant qv = item->data(0, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>(); 

      bool allowDirectives = !carbonDBIsContainedByComposite(mDB, node);

      updateContextMenuActions();

      const char* text;
      text = carbonDBNodeGetFullName(mDB, node);
      CarbonProjectWidget *carbonProjectWidget = mContext->getCarbonProjectWidget();
      DirectivesEditor* directivesEditor = carbonProjectWidget->directivesEditor();
      if (directivesEditor == NULL)
        directivesEditor = carbonProjectWidget->compilerDirectives();
      bool isAlreadyPresent;
      
      bool dbForcible = carbonDBIsForcible(mDB,node);
      bool dbDepositable = carbonDBIsDepositable(mDB, node);
      bool dbObserveable = carbonDBIsObservable(mDB, node);

      bool value = directivesEditor->getNetDirectivesTab()->getSpecialDirectiveValue(text,"observeSignal", &isAlreadyPresent);
      if (isAlreadyPresent && value)
      {
        mActionObserve->setChecked(value);
        mActionObserve->setEnabled(allowDirectives);
      }
      else
      {
        mActionObserve->setChecked(dbObserveable);
        mActionObserve->setEnabled(allowDirectives && !dbObserveable);
      }

      value = directivesEditor->getNetDirectivesTab()->getSpecialDirectiveValue(text,"depositSignal", &isAlreadyPresent);
      if (isAlreadyPresent && value)
      {
        mActionDeposit->setChecked(value);
        mActionDeposit->setEnabled(allowDirectives);
      }
      else
      {
        mActionDeposit->setChecked(dbDepositable);
        mActionDeposit->setEnabled(allowDirectives && !dbDepositable);
      }

      value = directivesEditor->getNetDirectivesTab()->getSpecialDirectiveValue(text,"forceSignal", &isAlreadyPresent);
      if (isAlreadyPresent && value)
      {
        mActionForce->setChecked(value);
        mActionForce->setEnabled(allowDirectives);
      }
      else
      {
        mActionForce->setChecked(dbForcible);
        mActionForce->setEnabled(allowDirectives && !dbForcible);
      }

      //setContextMenuNode(node);
      QMenu menu;
      menu.addAction(mActionNavigateSource);
      menu.addAction(mActionObserve);
      menu.addAction(mActionDeposit);
      menu.addAction(mActionForce);

      menu.exec(viewport()->mapToGlobal(pos));

      //setContextMenuNode(NULL);
    }
  }
}

// Emit signals indicating the status of components
void BrowserTableWidget::updateContextMenuActions()
{
}

QAction *BrowserTableWidget::createIndividualAction(const char *text, const char *objectName, const char *iconName, const char *statusTip) {

  QAction *action = new QAction(this);
  action->setText(text);
  action->setObjectName(QString::fromUtf8(objectName));
  if (iconName)
    action->setIcon(QIcon(QString::fromUtf8(iconName)));
  action->setStatusTip(statusTip);

  return action;
}

void BrowserTableWidget::createActions()
{
  //bool bCanCreateMV = mContext->getCanCreateMV();

  mActionNavigateSource = createIndividualAction("Navigate to RTL location", "cpwProperties", ":/cmm/Resources/properties.png", "Navigate to RTL source location");
  /* new ram - new names */
  mActionObserve = createIndividualAction("observeSignal Directive", "cpwProperties", NULL, "Make Observable");
  mActionDeposit = createIndividualAction("depositSignal Directive",  "cpwProperties", NULL, "Make Depositable");
  mActionForce = createIndividualAction("forceSignal Directive", "cpwProperties", NULL, "Make Forcible");

  mActionObserve->setData(eObserve);
  mActionObserve->setCheckable(true);
  mActionDeposit->setData(eDeposit);
  mActionDeposit->setCheckable(true); 
  mActionForce->setData(eForce);
  mActionForce->setCheckable(true); 

  CQT_CONNECT(mActionNavigateSource, triggered(), this, actionNavigateSource());
  CQT_CONNECT(mActionObserve, triggered(), this, actionApplyDirective());
  CQT_CONNECT(mActionDeposit, triggered(), this, actionApplyDirective());
  CQT_CONNECT(mActionForce, triggered(), this, actionApplyDirective());
}

void BrowserTableWidget::actionApplyDirective()
{
  QAction* action = qobject_cast<QAction *>(sender());
  INFO_ASSERT(action, "Expecting a QAction");
  bool isChecked = action->isChecked();
  QVariant v = action->data();

  if (v.isValid())
  {
    DesignDirective directive = static_cast<DesignDirective>(v.toInt());

    QList<QTreeWidgetItem*> itemList;
    buildSelectionList(itemList);
    // mDragText.clear();

    foreach (QTreeWidgetItem *item, itemList)
    {
      QVariant qv = item->data(0, Qt::UserRole);
      CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();

      emit applySignalDirective(directive, carbonDBNodeGetFullName(mDB, node), isChecked);
    }
  }
  else
    qDebug() << "action missing value";
}


void BrowserTableWidget::actionNavigateSource()
{
  QList<QTreeWidgetItem*> itemList;
  buildSelectionList(itemList);
  // mDragText.clear();

  foreach (QTreeWidgetItem *item, itemList)
  {
    QVariant qv = item->data(0, Qt::UserRole);
    CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();

    const char*src = carbonDBGetSourceFile(mDB, node);
    int lineNo = carbonDBGetSourceLine(mDB, node);

    qDebug()<< "Source is " << src;
    qDebug()<< "Line is: " << lineNo;

    emit navigateSource(src, lineNo);

    break;
  }
}


void BrowserTableWidget::selectNet(const char* netName)
{
  clearSelection();
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    QVariant qv = item->data(0, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();
      UtString text;
      text << carbonDBNodeGetFullName(mDB, node);
      if (0 == strcmp(text.c_str(), netName))
      {
        item->setSelected(true);
        scrollToItem(item, QAbstractItemView::PositionAtCenter);
        break;
      }
    }
  }
}

void BrowserTableWidget::itemExpanded(QTreeWidgetItem* item)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);
  QVariant qv = item->data(0, Qt::UserRole);
  if (!qv.isNull())
  {
    CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();
    INFO_ASSERT(node, "Expecting carbondbnode");

    bool doIt = true;
    if (carbonDBIsArray(mDB, node))
    {
      int range = std::abs(carbonDBGetArrayLeftBound(mDB,node)-carbonDBGetArrayRightBound(mDB, node));
      if (range > 1024)
      {
        UtString msg;
        msg << "There are " << range << " items underneath this which might take a long time\n";
        msg << "to expand, are you sure you want to do this?";

        if (QMessageBox::question(this, MODELSTUDIO_TITLE, msg.c_str(), 
          QMessageBox::No|QMessageBox::Yes, QMessageBox::No) == QMessageBox::No)
          doIt = false;
      }
    }
    if (doIt)
    {
      const char* nodeName = carbonDBNodeGetFullName(mDB, node);
      int n = item->childCount();
      while (n >= 0) {
        item->takeChild(n);
        n--;
      }
      //  emit nodeDoubleClicked(nodeName, NULL, NULL);
      AddNetNodes(0, node,item);
      qDebug()<<nodeName;
    }
    else
      collapseItem(item);
  }

 QApplication::restoreOverrideCursor();
}

QColor BrowserTableWidget::getDefaultTextColor()
{
  // Get the default text color of a tree widget item
  QTreeWidget tmpTree;
  tmpTree.setColumnCount(1);
  QTreeWidgetItem tmpItem(&tmpTree);
  return tmpItem.textColor(0);
}

void BrowserTableWidget::putContext(ApplicationContext* ctx, CarbonMakerContext* appContext)
{
  mCtx = ctx;
  mContext = appContext;
}

void BrowserTableWidget::Initialize(HierarchyMode mode)
{
  mParentNode = NULL;
  mHierMode = mode;
  clearAll();
  mDB = mCtx->getDB();
  mCfg = mCtx->getCfg();
}

void BrowserTableWidget::Connect(BrowserTreeWidget* tree)
{
  if (mTree == NULL)
  {
    CQT_CONNECT(tree, nodeSelectionChanged(CarbonDBNode*,XToplevelInterface*,XInstance*), this, nodeSelectionChanged(CarbonDBNode*,XToplevelInterface*,XInstance*));
  }

  mTree = tree;
}

void BrowserTableWidget::clearAll()
{
  mDB = 0;
  mCfg = 0;
  clear();
}

bool BrowserTableWidget::qualifyNet(const CarbonDBNode* node,
                                    UInt32* filteredNets)
{
  const char* leafName = carbonDBNodeGetLeafName(mDB, node);
  if (*leafName == '$') {
    return false;
  }
  if (! (carbonDBCanBeCarbonNet(mDB, node) || carbonDBIs2DArray(mDB, node)) ) {
    return false;
  }

  if (! (mFilterPattern.empty() || mFilterPattern.isMatch(leafName))) {
    *filteredNets += 1;
    return false;
  }

  if (mShowTypes != eAllNets) {
    bool typeMatch =
      ((mShowTypes == eScalars) && carbonDBIsScalar(mDB, node)) ||
      ((mShowTypes == eMemories) && carbonDBIs2DArray(mDB, node)) ||
      ((mShowTypes == eVectors) && carbonDBIsVector(mDB, node));
    if (! typeMatch) {
      *filteredNets += 1;
      return false;
    }
  }
  return true;
} 

static XParameter* getInstanceParameter(XInstance* xinst, const QString& paramName)
{
  if (xinst)
  {
    int numParams = xinst->getParameters()->numParameters();
    if (numParams > 0)
    {
      for (int i=0; i<numParams; i++)
      {
        XParameter* parm = xinst->getParameters()->getAt(i);
        if (parm->getName() == paramName)
          return parm;
      }
    }
  }
  return NULL;
}

void BrowserTableWidget::nodeSelectionChanged(CarbonDBNode* parentNode, XToplevelInterface* xtop, XInstance* xinst)
{
  mParentNode = parentNode;
  mParentTop = xtop;
  mParentInst = xinst;

  bool bs = signalsBlocked();
  blockSignals(true);

  CQtRecordBlocker blocker(mCtx->getCQt());
  QApplication::setOverrideCursor(Qt::WaitCursor);

  // Empty out the list
  clear();


  if (xinst || xtop)
  {
    XInterface* iface = NULL;
    if (xinst)
      iface = xinst->getInterface();
    else if (xtop)
    {
      QString ifaceName = xtop->getName();
      XDesignHierarchy* dh = mTree->getHierarchy();
      iface = dh->findInterface(ifaceName);
    }


    if (iface)
    {
      XParameters* params = iface->getParameters();
      int numParams = params->numParameters();
      if (numParams > 0)
      {
        QTreeWidgetItem* paramsNode = new QTreeWidgetItem(this);
        paramsNode->setText(0, "Parameters");
        QFont font = paramsNode->font(0);
        font.setBold(true);
        paramsNode->setFont(0, font);  

        for (int i=0; i<numParams; i++)
        {
          XParameter* parm = params->getAt(i);
          QTreeWidgetItem* parmItem = new QTreeWidgetItem(paramsNode);
          XParameter* instParm = getInstanceParameter(xinst, parm->getName());

          QString value;
          QString paramType = parm->getType();

          if (instParm)
            value = QString("%1=%2 (%3)")
              .arg(parm->getName())
              .arg(instParm->getValue())
              .arg(parm->getValue());
          else
            value = QString("%1=%2")
              .arg(parm->getName())
              .arg(parm->getValue());

          parmItem->setText(0, value);
          parmItem->setText(4, paramType);

          QVariant qv = qVariantFromValue((void*)parm);
          parmItem->setData(1, Qt::UserRole, qv);
        }
        paramsNode->setExpanded(true);
      }
    }
  }


  AddNetNodes(0, parentNode, NULL);

  header()->resizeSections(QHeaderView::ResizeToContents);

  header()->setResizeMode(0, QHeaderView::Interactive);
  header()->setResizeMode(1, QHeaderView::Interactive);
  header()->setResizeMode(2, QHeaderView::Interactive);
  header()->setResizeMode(3, QHeaderView::Interactive);
  header()->setResizeMode(4, QHeaderView::Interactive);
  header()->setResizeMode(5, QHeaderView::Interactive);
  header()->setResizeMode(6, QHeaderView::Stretch);


  QApplication::restoreOverrideCursor();

  blockSignals(bs);
}

void BrowserTableWidget::AddNetNodes(int depth, CarbonDBNode* mod, QTreeWidgetItem *parent)
{
  if (mod == NULL)
    return;

  UtArray<const CarbonDBNode*> nodes;

  if (mod == NULL)
    return;

  const CarbonDBNode* node;

  CarbonDBNodeIter* iter = carbonDBLoopChildren(mDB, mod);
  UInt32 filteredNets = 0;

  while ((node = carbonDBNodeIterNext(iter)) != NULL)
  {
    // Skip if filtered
    if (mExplicitObserves && !carbonDBIsObservable(mDB, node))
      continue;

    // Skip if filtered
    if (mExplicitDeposits && !carbonDBIsDepositable(mDB, node))
      continue;

    bool filteredNet = false;
    if (qualifyNet(node, &filteredNets))
    {
      nodes.push_back(node);
      continue;
    }
    else
      filteredNet = true;

    if (filteredNet)
      continue;

    if (carbonDBIsArray(mDB, node) || carbonDBIsStruct(mDB, node) || carbonDBIsScalar(mDB, node))
    {
      nodes.push_back(node);
    }
  }
  carbonDBFreeNodeIter(iter);

  int row = 0;
  QTreeWidgetItem* item = NULL;

  std::sort(nodes.begin(), nodes.end(), CarbonCowareHBrowserNodeSorter(mDB));

  for (UInt32 i = 0, n = nodes.size(); i < n; ++i, ++row)
  {
    node = nodes[i];
    if (parent == NULL)
      item = new MyTreeWidgetItem(mDB, this);
    else item = new MyTreeWidgetItem(mDB, parent);

    const char* leafName = carbonDBNodeGetLeafName(mDB, node);
    item->setText(0, leafName);
    // Store the reference to the node in the tree node
    QVariant qv = qVariantFromValue((void*)node);
    item->setData(0, Qt::UserRole, qv);

    ShowTypes(mDB, node, item);
    ShowSize(mDB, node, item);
    ShowDirection(mDB, node, item);
    ShowAttributes(mDB, node, item);
    ShowRanges(mDB, node, item);

    // After the first pass (depth == 0), we need to recurse into
    // nodes that might be able to be expanded (e.g. arrays and
    // structs) and populate their first level of children.  This
    // causes the [+] tree expansion icon to be displayed.
    if ((depth == 0) && (carbonDBIsArray(mDB, node) || carbonDBIsStruct(mDB, node)))
    {
      item->setChildIndicatorPolicy(QTreeWidgetItem::ShowIndicator);
      //AddNetNodes(1, (CarbonDBNode *) node, item);
    }

    ShowFlags(mDB, node, item);
  }
}

void BrowserTableWidget::ShowSize(CarbonDB *mDB, const CarbonDBNode *node, QTreeWidgetItem *item)
{
  UtString sizeString;
  sizeString << carbonDBGetBitSize(mDB, node);
  item->setText(3,sizeString.c_str());
}

void BrowserTableWidget::ShowTypes(CarbonDB *mDB, const CarbonDBNode *node, QTreeWidgetItem *item) 
{
  UtString typeString;

  if (carbonDBIsScalar(mDB, node)) 
    typeString += "Scalar";
  else if (carbonDBIsArray(mDB, node))
    typeString += "Array";
  else if (carbonDBIs2DArray(mDB, node))
    typeString += "2D Array";
  else if (carbonDBIsStruct(mDB, node))
    typeString += "Structure";

  item->setText(4, typeString.c_str());
}

void BrowserTableWidget::ShowRanges(CarbonDB *mDB, const CarbonDBNode *node, QTreeWidgetItem *item) {
  UtString rangeString;
  UtString buf;

  //if (carbonDBIsVector(mDB, node) || carbonDBIs2DArray(mDB, node)) {
  //  buf << "[" << carbonDBGetMSB(mDB, node)
  //    << ":" << carbonDBGetLSB(mDB, node) << "]";
  //  if (carbonDBIs2DArray(mDB, node)) {
  //    buf << "[" << carbonDBGet2DArrayLeftAddr(mDB, node)
  //      << ":" << carbonDBGet2DArrayRightAddr(mDB, node) << "]";
  //  }
  //  buf << "";
  //}

  if (carbonDBIsArray(mDB, node)) {
    rangeString << "[" <<  carbonDBGetArrayLeftBound(mDB,node) << ":" << carbonDBGetArrayRightBound(mDB, node) << "]";
    item->setText(1,rangeString.c_str());
  }
  else
    item->setText(1, "");
}

void BrowserTableWidget::ShowFlags(CarbonDB *mDB, const CarbonDBNode *node, QTreeWidgetItem *item) {
  UtString flagString;
  flagString = "";

  bool hasVisibilityIssue = true;

  // Observability is a property of symbol table leaf nodes, not
  // branches.  If we have a structure, or an array of structures, the
  // underlying symtab node will be a branch.  In that case, treat the
  // node as visible.  When expanded, its fields (provided they're not
  // themselves records or arrays of records) will have accurate
  // observability.
  const STSymbolTableNode* stNode = CarbonDatabasePriv::getSymTabNode(node);
  bool isSTBranch = (stNode->castBranch() != NULL);

  if (carbonDBIsObservable(mDB, node) || carbonDBIsVisible(mDB, node) || isSTBranch) {
    hasVisibilityIssue = false;
  }
  if (carbonDBIsDepositable(mDB, node))         { flagString += " Depositable"; }
  if (carbonDBIsScDepositable(mDB, node))       { flagString += " SCDepositable"; }
  if (carbonDBIsObservable(mDB, node))          { flagString += " Observable"; }
  if (carbonDBIsScObservable(mDB, node))        { flagString += " SCObservable"; }
  if (carbonDBIsForcible(mDB, node))            { flagString += " Forcible"; }
  if (carbonDBIsOnDemandIdleDeposit(mDB, node)) {flagString += " OnDemandIdleDeposit"; }
  if (carbonDBIsOnDemandExcluded(mDB, node)) {flagString += " OnDemandExcluded"; }

  item->setText(6, flagString.c_str());

  QFont font("Helvetica", 8, QFont::Bold);
  item->setFont(6, font);


  // There is no way to reset a QColor using a Qt::GlobalColor, you
  // can only construct with that. So, I'll just create a tmp with
  // the default color. And, I'll reassign it to a different QColor
  // if needed.
  QColor netTextColor(getDefaultTextColor());

  // Paint a non-visible net red
  if (hasVisibilityIssue)
  {
    QColor red(Qt::red);
    netTextColor = red;
  }

  for (int col = 0; col < columnCount() ; ++col)
    item->setTextColor(col, netTextColor);
}

void BrowserTableWidget::ShowAttributes(CarbonDB *mDB, const CarbonDBNode *node, QTreeWidgetItem *item) {
  UtString attributesString;
  if (carbonDBIsAsyncPosReset(mDB, node)) 
    attributesString += " PReset";
  if (carbonDBIsAsyncNegReset(mDB, node))  
    attributesString += " NReset";
  if (carbonDBIsClk(mDB, node))   
    attributesString += " Clock";
  if (carbonDBIsClkTree(mDB, node))       
    attributesString += " ClockTree";
  if (carbonDBIsAsyncOutput(mDB, node))
    attributesString += " AsyncOutput";
  if (carbonDBIsAsync(mDB, node))
    attributesString += " Async";
  if (carbonDBIsAsyncDeposit(mDB, node))
    attributesString += " AsyncDeposit";
  if (carbonDBIsConstant(mDB, node))
    attributesString += " Constant";
  if (carbonDBIsTristate(mDB, node))
    attributesString += " Tristate";
  if (carbonDBIsBothEdgeTrigger(mDB, node))
    attributesString += " BothEdgeTrigger";
  else if (carbonDBIsNegedgeTrigger(mDB, node))
    attributesString += " NEdgeTrigger";
  else if (carbonDBIsPosedgeTrigger(mDB, node))
    attributesString += " PEdgeTrigger";
  if (carbonDBIsTriggerUsedAsData(mDB, node))
    attributesString += " TriggerUsed";
  //if (carbonDBIsLiveInput(mDB, node))
  //	attributesString += " LiveInput";
  //else if (carbonDBIsLiveOutput(mDB, node))
  //	attributesString += " LiveOutput";

  item->setText(5, attributesString.c_str());
}

void BrowserTableWidget::ShowDirection(CarbonDB *mDB, const CarbonDBNode *node, QTreeWidgetItem *item) {
  UtString directionString;

  if (carbonDBIsInput(mDB, node)) 
    directionString = "Input";
  else if (carbonDBIsOutput(mDB, node))
    directionString = "Output";
  else if (carbonDBIsBidi(mDB, node))
    directionString = "Bidirectional";
  else if (carbonDBIsPrimaryInput(mDB, node))
    directionString = "Primary Input";
  else if (carbonDBIsPrimaryOutput(mDB, node))
    directionString = "Primary Output";
  else if (carbonDBIsPrimaryBidi(mDB, node))
    directionString = "Primary Bidirectional";

  item->setText(2, directionString.c_str());
}

bool CarbonCowareHBrowserNodeSorter::operator()(const CarbonDBNode* n1,
                                                const CarbonDBNode* n2)
{
  int cmp = 0;

  // If these nodes have the same parent, then just compare the
  // leaves.  Otherwise we have a more complex comparison we
  // will need to do
  if (carbonDBNodeGetParent(mDB, n1) == carbonDBNodeGetParent(mDB, n2))
  {
    // Numeric sort if the parent is an array
    if (carbonDBIsArray(mDB, carbonDBNodeGetParent(mDB, n1)) && carbonDBIsArray(mDB, carbonDBNodeGetParent(mDB, n2)))
    {
      int i1 = CarbonDatabasePriv::getNodeIndex(n1);
      int i2 = CarbonDatabasePriv::getNodeIndex(n2);
      cmp = i1 - i2;
    }
    else // Textual Sort
    {
      const char* l1 = carbonDBNodeGetLeafName(mDB, n1);
      const char* l2 = carbonDBNodeGetLeafName(mDB, n2);
      cmp = strcmp(l1, l2);
    }

  }
  else
  {
    // We are comparing across hierarchy, which can happen when sorting
    // drivers.  Use the full name.  We could do this faster but
    // not right now.
    UtString name1, name2;
    name1 = carbonDBNodeGetFullName(mDB, n1);
    name2 = carbonDBNodeGetFullName(mDB, n2);
    cmp = strcmp(name1.c_str(), name2.c_str());

  }
  return cmp < 0;
}

void BrowserTableWidget::buildSelectionList(QList<QTreeWidgetItem*>& list)
{
  // The list gives items for every column and row, we just want it row
  // oriented.
  list.clear();

  foreach (QTreeWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(0, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();
      if (node != NULL)
      {
        list.append(item);
      }
    }
  }  
}
void BrowserTableWidget::itemSelectionChanged()
{
  QList<QTreeWidgetItem*> itemList;
  buildSelectionList(itemList);
  mDragText.clear();

  // Now, disconnect each item
  // Now walk the list of items to be deleted
  foreach (QTreeWidgetItem *item, itemList)
  {
    QVariant qv = item->data(0, Qt::UserRole);
    CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();

    if (!mDragText.empty())
      mDragText << "\n";

    mDragText << carbonDBNodeGetFullName(mDB, node);

    if (!signalsBlocked())
      emit selectionChanged(node);
  }

  // Show nothing is selected
  if (itemList.count() == 0)
    emit selectionChanged(NULL);
}
