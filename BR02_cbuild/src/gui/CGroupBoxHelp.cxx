/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "gui/CGroupBoxHelp.h"
#include <QResizeEvent>
#include <QApplication>
#include <QPushButton>
#include <QPalette>
#include "util/UtIOStream.h"

CGroupBoxHelp::CGroupBoxHelp(CQtContext* cqt,
                             const char* name, const char* helpTag):
  QGroupBox(name),
  mCQt(cqt),
  mHelpTag(helpTag)
{
  mPushButton = NULL;

#if 0
#if 1
  mPushButton = new QPushButton("?", this);
#if pfWINDOWS                   // 9 seems too big on Windows
  QFont f("Helvetica", 8, QFont::Bold);
#else
  QFont f("Helvetica", 9, QFont::Bold);
#endif
  mPushButton->setFont(f);
  QPalette pal = mPushButton->palette();
  pal.setColor(QPalette::Button, Qt::white);
  pal.setColor(QPalette::ButtonText, Qt::red);
  mPushButton->setPalette(pal);

#else
  QIcon question(":/images/question.png");
  mPushButton = new QPushButton(question, "?", this);
#endif
  CQT_CONNECT(mPushButton, clicked(), this, help());
#endif
  mButtonWidth = 0;
}


void CGroupBoxHelp::resizeEvent(QResizeEvent* event)
{
  QGroupBox::resizeEvent(event);
  if (mPushButton == NULL)
    return;

  if (mButtonWidth == 0) {
    QSize sz = mPushButton->sizeHint();
    //UtIO::cerr() << "button size " << sz.width() << " x " << sz.height() << UtIO::endl;
    // I'm not sure why, but in Windows the button width seems way too wide.
    // Don't let it exceed the height.
    if (sz.width() > sz.height()) {
      sz.setWidth((4*sz.height()) / 5);
    }
    mPushButton->resize(sz);
    mButtonWidth = sz.width();
  }

  // move the push-button vaguely to the upper-right corner and see
  // what happens
  SInt32 width = event->size().width();
  SInt32 button_x = width - 2*mButtonWidth;
  if (button_x < 0) {
    button_x = 0;
  }
  mPushButton->move(button_x, 0);
}

void CGroupBoxHelp::help() {
  mCQt->launchHelp(mHelpTag.c_str());
}
