// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CBROWSER_H_
#define _CBROWSER_H_

#include "gui/CQt.h"
#include <QHBoxLayout>
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "util/SourceLocator.h"
#include "Drivers.h"
#include "util/UtWildcard.h"
#include <QMainWindow>

class TextEdit;

class CarbonCBrowser : public QMainWindow {
  Q_OBJECT
public:

  //! ctor
  CarbonCBrowser(CQtContext* cqt, const char* name,
                 DriverDatabase*, CarbonDB*, const CarbonDBNode*);

  //! dtor
  ~CarbonCBrowser();

public slots:
  void changeDriverSel();
  void changeLoadSel();
  void textEditDestroyed(QObject* obj);
  void changeFaninTreeSel();
  void popupSource(QTreeWidgetItem*,int);
  void dropNet(const char* path, QTreeWidgetItem*);

private:
  QWidget* buildLayout();
  void clear();
  void setFocus(const CarbonDBNode* node);
  void clearDriverMap();
  //void showAliases(const CarbonDBNode* node);
  void showDrivers(const CarbonDBNode* node, DriverLoop drivers,
                   CDragDropTreeWidget* tree);
  void showDriver(const CarbonDBNode* node,
                  Driver* driver,
                  CDragDropTreeWidget* tree,
                  QTreeWidgetItem* parent);
  void changeDriverTreeSel(CDragDropTreeWidget* tree);

  CDragDropTreeWidget* mDriverTree;
  CDragDropTreeWidget* mLoadTree;
  CDragDropTreeWidget* mFaninTree;
  //QTreeWidget* mAliasTree;

  CarbonDB* mDB;

  typedef UtHashMap<QTreeWidgetItem*,Driver*> ItemDriverMap;
  ItemDriverMap mItemDriverMap;

  typedef UtHashMap<QTreeWidgetItem*,const CarbonDBNode*> ItemNetMap;
  ItemNetMap mItemNetMap;
  ItemNetMap mItemFaninMap;

  typedef UtHashMap<const char*,TextEdit*> FileEditorMap;
  FileEditorMap mFileEditorMap;

  typedef UtHashMap<TextEdit*,const char*> FileEditorReverseMap;
  FileEditorReverseMap mFileEditorReverseMap;

  DriverDatabase* mDrivers;

  const CarbonDBNode* mNode;
  CQtContext* mCQt;
}; // class CarbonCBrowser : public QWidget

#endif
