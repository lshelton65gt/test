// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef CAR2COW_H
#define CAR2COW_H

#include "gui/CMainWindow.h"
#include "ui_car2cow.h"

#include "util/CarbonPlatform.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "util/SourceLocator.h"
#include "util/UtString.h"

#include "CowareApplicationContext.h"
#include "gui/CQt.h"
#include "shell/CarbonAbstractRegister.h"

#include <QObject>

class CodeGenerator;
class PortView;
class CQtContext;

class Car2Cow : public CMainWindow
{
  Q_OBJECT

  enum ParseResult {eIODB, eFullDB, eCcfg, eNoMatch};
  enum CowizTabs {tabPORTS=0, tabXTORS, tabREGISTERS, tabMEMORIES, tabBUILD};  

public:
  Car2Cow(CQtContext *cqt, QWidget *parent = 0, Qt::WFlags flags = 0);
  Car2Cow(CQtContext *cqt, const QString& filename);

  ~Car2Cow();

  CowareApplicationContext* getCtx() {return mContext;}
  CarbonDB* getDB() { return m_pDB; }
  CarbonCfgID getCcfgID() { return m_cfgID; }
  UtString getCcfgFilename() {return m_ccfgName; }
  void updateTabs();

  bool licenseValid() {return m_pReg != NULL ? true : false;}
  bool emptyDocument() {return isUntitled; }
  void documentModified(bool value);

  void loadFile(const QString &fileName);

protected:
  void populateXtorLibComboBox();
  void populateXtorComboBox(const char* libname);
  void closeEvent(QCloseEvent *event);
  void setupToolbar(const char* filename);
  bool save();
  void generateCode();
  void registerWidgets(const char* filename);
  void registerUniqueAction(QAction* action, const char* name, const char* filename);
  void registerUniqueWidget(const char* filename, CQtControl* control, const char* name, bool saveEvents);

private:
  Ui::Car2CowClass ui;

  CarbonAbstractRegisterID m_pReg;
  UtString m_dbName;
  CarbonDB* m_pDB;
  CarbonCfgID m_cfgID;
  CQtContext *mCQt;
  CarbonMsgCBDataID* mMsgCallback;
  PortView* m_pPortView;
  UtString m_ccfgName;
  AtomicCache* mAtomicCache;
  SourceLocatorFactory mLocatorFactory;
  CarbonCfgXtorInstance* mXtorInst;

  static eCarbonMsgCBStatus sMsgCallback(CarbonClientData, CarbonMsgSeverity,
    int number, const char* text,
    unsigned int len);

  const char* append(UtString& buf, const char* s1, const char* s2);
  ParseResult parseFileName(const char* filename, UtString* baseName);
  bool loadDesignFile(const char* filename, ParseResult parseResult);
  bool loadDatabase(const char* iodbName, bool initPorts);
  bool loadConfig(const QString &fileName);
  bool saveAs();

  friend class PortView;

  CowareApplicationContext* mContext;
  CodeGenerator* mCodegen;

// SDI Handling
private:
  QString curFile;
  bool isUntitled;
  bool isModified;

private:
  bool maybeSave();
  void setCurrentFile(const QString &fileName);
  QString strippedName(const QString &fullFileName);
  Car2Cow *findMainWindow(const QString &fileName);
  CarbonDBNode* mSelectedNode;
  CarbonDBNode* mMemorySelectedNode;
  CarbonCfgRegister* mReg;

  QAction* openAct;
  QAction* saveAct;

private:
  void initialize(const char* filename=0);
  void warning(QWidget* widget, const QString& msg);

private slots:
  void on_btnAdd_clicked();
  void on_btnDelete_clicked();
  void on_pushButtonGenerate_clicked();
  void on_action_Open_triggered();
  void on_actionSave_triggered();
  void on_actionSave_As_triggered();
  void on_actionExit_triggered();
  void documentWasModified();
  void selectionChanged(CarbonDBNode* node);
  void fieldSelectionChanged(CarbonCfgRegisterField* field);
  void registerSelectionChanged(CarbonCfgRegister* reg);
  void on_btnDeleteField_clicked();
  void on_btnBindConstant_clicked();
  void on_btnBindTo_clicked();
  void on_btnAddField_clicked();
  void on_pushButtonAddXtor_clicked();
  void on_pushButtonDelXtor_clicked();
  void tabChanged(int index);
  void memSelectionChanged(CarbonDBNode* node);
  void memSelectionChanged(CarbonCfgMemory* mem);
  void on_btnDeleteMem_clicked();
  void xtorSelectionChanged(CarbonCfgXtorInstance* xinst);
  void xtorBindingSelectionChanged(CarbonCfgXtorConn* xconn);
  void on_pushButtonBindByName_clicked();
  void xtorBindingsChanged();
  void xtorLibraryChanged(int index);
  void on_pushButtonBindAdv_clicked();
  void on_pushButtonDeleteBinding_clicked();
};

#endif // CAR2COW_H
