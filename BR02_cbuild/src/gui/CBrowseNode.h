// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef CBROWSE_NODE_H
#define CBROWSE_NODE_H

#include "shell/carbon_dbapi.h"
#include <QGraphicsItem>
#include <QObject>
#include "iodb/CGraph.h"
#include "CBrowseButton.h"

class CBrowse;
class CBrowseButton;
class Driver;

//! visual representation of a CGraph node in the connectivity browser
class CBrowseNode : public QObject, public QGraphicsItem
{
  Q_OBJECT
    
public:
  CBrowseNode(CBrowse* browser, CGraph::Node*);
  ~CBrowseNode();
  
  QRectF boundingRect() const;
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
             QWidget *widget);
  void layout();
  UInt32 getIndex() const {return mIndex;}
  void putIndex(UInt32 idx) {mIndex = idx;}
  
  double getWidth() const {return mBoundingRect.width();}
  double getHeight() const {return mBoundingRect.height();}
  void mark();

  CBrowseButton* findButton(CGraph::Node* node);

  // To facilitate undo/redo navigation, supply a state object
  // which indicates the values of all the buttons
  class State {
  public:
    CARBONMEM_OVERRIDES

    friend class CBrowseNode;
    void restore() const {mNode->restoreState(*this);}
    CBrowseNode* getNode() const {return mNode;}
  private:
    CBrowseNode* mNode;
    UtArray<CBrowseButton::State> mButtonState;
    QPointF mPos;
  };

  void saveState(State* state);
  void restoreState(const State& state);

protected:
  void mousePressEvent(QGraphicsSceneMouseEvent *event);
  void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
  void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
  void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);

  
private:
  void drawText(QPainter *painter, const char* str, double x, double y,
                double* width, double* height);
  void clearButtons();

  typedef std::pair<double,double> DoublePair;

  CBrowse* mBrowser;
  CarbonDB* mDB;
  CGraph::Node* mCGNode;
  QRectF mBoundingRect;
  UInt32 mIndex;
  double mTextStart;
  double mTextEnd;
  UtArray<CBrowseButton*> mButtons;
  UtHashMap<CGraph::Node*,CBrowseButton*> mNodeButtonMap;
};

#endif
