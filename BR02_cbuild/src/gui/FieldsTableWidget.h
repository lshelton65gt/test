// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef FIELDSTABLEWIDGET_H
#define FIELDSTABLEWIDGET_H

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "cfg/CarbonCfg.h"

#include "ApplicationContext.h"

#include <QObject>
#include <QTableWidget>
#include <QtGui>

#include "gui/CTableWidget.h"
#include <QItemDelegate>
#include "ui_FieldsTableWidget.h"

class FieldsEditDelegate;
class DialogBindConstant;
class CDialog;
class CarbonProjectWidget;

class FieldsTableWidget : public CTableWidget
{
  Q_OBJECT

  enum Columns {colNAME=0, colDSTRANGE, colACCESS, colCONTENT, colSRCRANGE, colMEMINDEX, colSTATUS};

public:
  FieldsTableWidget(QWidget *parent = 0);
  ~FieldsTableWidget();
  void putContext(ApplicationContext *ctx, CarbonProjectWidget* pw);
  void Initialize(CarbonCfgRegister* reg);
  void AddField();
  void BindField(CarbonDBNode* node);
  void BindConstant();
  void DeleteField();
  void FinishEdit();
  void clearAll();
  CDialog *getConstantDialog();

  // reimplemented from CTableWidget
  void setModelData(const UtString &data, QAbstractItemModel *model,
                    const QModelIndex &index);

private:
  void createRow(CarbonCfgRegisterField* field);
  void updateRow(int row, CarbonCfgRegisterField* field);
  friend class FieldsEditDelegate;
  void buildSelectionList(QList<QTableWidgetItem*>& list);
  bool hasValidLocation(CarbonCfgRegisterField* field);
  void bind(QTableWidgetItem* item, CarbonCfgRegisterField* field, CarbonDBNode* node);
  void checkRegisterFields();
  bool checkField(QTableWidgetItem* item, CarbonCfgRegisterField* field);
  bool colorField(QTableWidgetItem* item, bool signalError, const char* errMsg);
  QColor getDefaultTextColor();
  CarbonCfgRegAccessTypeIO computeAccess(CarbonDBNode* node);
  UInt32 numUsedBits(CarbonCfgRegister* reg);

private:
  Ui::FieldsTableWidgetClass ui;
  CarbonCfgRegister* mReg;
  CarbonDB* mDB;
  ApplicationContext* mCtx;
  FieldsEditDelegate* mDelegate;
  QWidget* mEditItem;
  DialogBindConstant* mConstantDialog;
  CarbonProjectWidget* mProjectWidget;

private:
  virtual void dragEnterEvent(QDragEnterEvent* ev);
  virtual void dropEvent(QDropEvent* ev);
  virtual void dragMoveEvent(QDragMoveEvent *ev);

private slots:
  void itemSelectionChanged();
  void itemClicked(QTableWidgetItem* item);

signals:
  void statusText(const char* statusMsg);
  void selectionChanged(CarbonCfgRegisterField* field);

};


class FieldsEditDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    FieldsEditDelegate(QObject *parent = 0, FieldsTableWidget* t=0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
        const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const;
 
    void finishEdit()
    {
      currentIndexChanged(0);
    }
private slots:
    void commitAndCloseEditor();
    void currentIndexChanged(int index);

private:
  FieldsTableWidget* table;

};
#endif // FIELDSTABLEWIDGET_H
