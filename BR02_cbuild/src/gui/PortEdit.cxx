/******************************************************************************
 Copyright (c) 2006-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

// Second implementation of port page.
//  - allow user to connect RTL ports to more than one transactor
//  - use internal class structures for representing the state,
//    rather than trying to keep the carbon_cfg in sync.  This
//    is easier to maintain, but it has the disadvantage that 
//    any new fields added to carbon_cfg will be lost on read/write
//    from the GUI unless the GUI nows how to store them (not necessarily
//    how to edit them.
//  - allow transactor functors to be created by the user to re-encode
//    RTL physical signals if the ESL encoding is different.

#include "PortEdit.h"
#include <QTreeWidgetItem>
#include <QVBoxLayout>
#include <QStringList>
#include <QTimer>
#include <cassert>
#include <QAction>
#include "gui/CFilenameEdit.h"
#include "gui/CDragDropTreeWidget.h"
#include "cfg/CarbonCfg.h"
#include <QLabel>
#include "gui/CExprValidator.h"
#include "DialogAdvancedBinding.h"
#include "TieOffTableWidget.h"
#include "CarbonProjectWidget.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "shell/CarbonDatabasePriv.h"
#include "iodb/IODBRuntime.h"
#include "util/DynBitVector.h"
#include "DlgNewParam.h"
#include "CHexDecValidator.h"

CarbonPortEdit::CarbonPortEdit(CQtContext* cqt,
                               CarbonCfgID cfg,
                               AtomicCache* atomicCache,
                               QAction* deleteAction, RegTabWidget::WizardMode wizardMode)
  : mCQt(cqt), mCfg(cfg),
    mAtomicCache(atomicCache),
    mDeleteAction(deleteAction),
    mMode(wizardMode)
{
  build();
  clearSelections();

  mParamInstance = NULL;
  mSelectingRTL = 0;
  mSelectingESL = 0;
  mSelectingPRM = 0;
  mClockWidgetMode = eNone;

  if (mMode == RegTabWidget::ModelMaker)
    mDialog = new DialogAdvancedBinding(this);
  else
    mDialog = NULL;

} // CarbonPortEdit::CarbonPortEdit

CarbonPortEdit::~CarbonPortEdit() {
}
  
void CarbonPortEdit::addTransactorsToComboBox() {
  // Clear selections first, or they may go haywire if we add transactors
  // to the box
  clearSelections();

  // Clear the combo box
  mSwitchXtorType->clear();
  
  // Add back the transactors
  CarbonCfgXtorLib* xlib = mCfg->findXtorLib(CARBON_DEFAULT_XTOR_LIB);
  if (xlib)
  {
    for (UInt32 i = 0, n = xlib->numXtors(); i < n; ++i) {
      CarbonCfgXtor* xtor = xlib->getXtor(i);
      mSwitchXtorType->addItem(xtor->getName());
    }
  }
}

void CarbonPortEdit::build() {
  CQtRecordBlocker blocker(mCQt);

  // There are two trees kept on-screen at all times: the RTL signals
  // and the transactors.  When a design first comes up, RTL signals
  // will be auto-connected to ESL ports matching the leaf names,
  // and uniquified.  The uniquification may be needed if the user
  // has specified directives scObserveSignal on top.sub1.a and top.sub2.a,
  // if we use the leaf name "a" as the auto-generated ESL name.
  //
  // The user can change a connection-type at any time by selecting the
  // connection switching its type.  E.g. he can change an ESL port
  // connection to a ClockGen, at which point all the clock parameters
  // will come up with default values that can be changed in widgets
  // that will become enabled when they make sense.
  //
  // New connections can be added at any time -- they will come up,
  // by default, as ESL signals.  The type can then be changed.  Changing
  // any connection to a ClockGen, ResetGen, or Tie, will remove all
  // the other connections.

  mRTLTree = mCQt->dragDropTreeWidget("rtl_tree", true);
  mRTLTree->setSelectionMode(QAbstractItemView::ExtendedSelection);
  mRTLTree->setColumnCount(4);
  //mRTLTree->setAlternatingRowColors(true);
  QStringList headers;
  headers << "Type";
  headers << "RTL Name";
  headers << "Width";
  headers << "Properties";
  mRTLTree->setHeaderLabels(headers);
  mRTLTree->setSortingEnabled(true);
  CQT_CONNECT(mRTLTree, dropReceived(const char*,QTreeWidgetItem*),
              this, dropXtorConn(const char*,QTreeWidgetItem*));

  mESLTree = mCQt->dragDropTreeWidget("esl_tree", true);
  mESLTree->setSelectionMode(QAbstractItemView::ExtendedSelection);
  mESLTree->setColumnCount(4);
  //mESLTree->setAlternatingRowColors(true);
  headers.clear();
  headers << "Type";
  headers << "ESL Name";
  headers << "RTL Name (expr)";
  headers << "Parameter";

  mESLTree->setHeaderLabels(headers);
  mESLTree->setSortingEnabled(true);
  CQT_CONNECT(mESLTree, dropReceived(const char*,QTreeWidgetItem*),
              this, dropRTLPort(const char*,QTreeWidgetItem*));

  mPRMTree = mCQt->dragDropTreeWidget("prm_tree", true);
  mPRMTree->setSelectionMode(QAbstractItemView::SingleSelection);
  mPRMTree->setColumnCount(6); // rjc why is this not done later with: mPRMTree->setColumnCount(headers.length())?
  mPRMTree->setRootIsDecorated(true);
  mPRMTree->setAlternatingRowColors(false);

  headers.clear();
  headers << "Parameter";
  headers << "Type";
  headers << "Scope";
  headers << "Default value";
  headers << "Description";
  headers << "Enum choices";

  mPRMTree->setHeaderLabels(headers);
  mPRMTree->setSortingEnabled(true);
  CQT_CONNECT(mPRMTree, dropReceived(const char*,QTreeWidgetItem*),
    this, dropPRM(const char*,QTreeWidgetItem*));

  mSwitchRTLType = mCQt->comboBox("rtl_switch_type");
  mSwitchRTLType->addItem("ESL Input");
  mSwitchRTLType->addItem("ESL Output");
  mSwitchRTLType->addItem("Generated Clock");
  mSwitchRTLType->addItem("Generated Reset");
  mSwitchRTLType->addItem("Tie");
  mSwitchRTLType->addItem("Disconnect");

  mSwitchXtorType = mCQt->comboBox("xtor_types");

  addTransactorsToComboBox();

  // Chooser for I/Os to pick which transactor they should
  // be attached to.  The "Signal" entry means they are not attached
  // to a transactor.  Once we decide that a port is connected
  // to a transactor, we will also have to pick which transactor
  // port
  //mXtorNameChooser = mCQt->comboBox("xtor_name");
  //mXtorNameChooser = NULL;
  //sHStretch(mXtorNameChooser, 2);

  //mXtorNameChooser->addItem("Signal");
  //CQT_CONNECT(mXtorNameChooser, currentIndexChanged(int),
  //            this, changeXtorName(int));

  mESLName = mCQt->lineEdit("esl_name");
  CQT_CONNECT(mESLName, textChanged(const QString&),
              this, updateESLName(const QString&));

  mESLPortExpr = new CExprValidator(mCQt, "esl_port_expr", true);
  CQT_CONNECT(mESLPortExpr, textChanged(const QString&),
              this, updateESLPortExpr(const QString&));

  mParamEditor = mCQt->lineEdit("esl_param_edit");
  CQT_CONNECT(mParamEditor, textChanged(const QString&),
              this, updateESLParamEdit(const QString&));

  mAddPort = mCQt->pushButton("add_esl_port", "Add ESL Port");
  mAddXtor = mCQt->pushButton("add_xtor", "Add Xtor");
  mAssignByName = mCQt->pushButton("assign_by_name", "Assign By Name");

  if (mMode == RegTabWidget::ModelMaker)
  {
    mAdvancedBind = mCQt->pushButton("assign_by_name_adv", "Binding...");
    CQT_CONNECT(mAdvancedBind, clicked(), this, advancedBinding());
  }
  addClockButtons();

  CQT_CONNECT(mRTLTree, selectionChanged(), this, changeRTLTreeSel());
  CQT_CONNECT(mESLTree, selectionChanged(), this, changeESLTreeSel());
  CQT_CONNECT(mPRMTree, selectionChanged(), this, changePRMTreeSel());

  CQT_CONNECT(mSwitchRTLType, currentIndexChanged(int),
              this, switchRTLType(int));
  CQT_CONNECT(mSwitchXtorType, currentIndexChanged(int),
              this, switchXtorType(int));
  CQT_CONNECT(mAddPort, clicked(), this, addPort());
  CQT_CONNECT(mAddXtor, clicked(), this, addXtor());
  CQT_CONNECT(mAssignByName, clicked(), this, assignByName());
} // void CarbonPortEdit::build


void CarbonPortEdit::createGlobalParam(CarbonCfgXtorParamInst* paramInst)
{
  QTreeWidgetItem* item = new QTreeWidgetItem(mPRMTree);
  QVariant qv = qVariantFromValue((void*)paramInst);
  item->setData(0, Qt::UserRole, qv);
  item->setText(0, paramInst->getParam()->getName());

  switch (paramInst->getParam()->getType())
  {
  case eCarbonCfgXtorBool:    item->setText(1, "Boolean"); break;
  case eCarbonCfgXtorUInt32:  item->setText(1, "UInt32"); break;
  case eCarbonCfgXtorUInt64:  item->setText(1, "UInt64"); break;
  case eCarbonCfgXtorDouble:  item->setText(1, "Double"); break;
  case eCarbonCfgXtorString:  item->setText(1, "String"); break;
  }

  switch (paramInst->getParam()->getFlag())
  {
  case eCarbonCfgXtorParamRuntime:  item->setText(2, "Run-Time"); break;
  case eCarbonCfgXtorParamCompile:  item->setText(2, "Compile-Time"); break;
  case eCarbonCfgXtorParamBoth:     item->setText(2, "Both"); break;
  case eCarbonCfgXtorParamInit:     item->setText(2, "Init-Time"); break;
  }
  item->setText(3, paramInst->getParam()->getDefaultValue());
  item->setText(4, paramInst->getParam()->getDescription());
  item->setText(5, paramInst->getParam()->getEnumChoices());
}

void CarbonPortEdit::newParameter()
{
  DlgNewParam dlg(mCfg, this);
  if (dlg.exec() == QDialog::Accepted)
  {
    UtString name;
    name << dlg.getName();
    UtString value;
    value << dlg.getValue();
    UtString descr;
    descr << dlg.getDescription();
    UtString enumChoices;
    enumChoices << dlg.getEnumChoices();

    CarbonCfgXtorParamInst* paramInst = mCfg->addParam(name.c_str(), 
      dlg.getType(), value.c_str(), dlg.getScope(), descr.c_str(), enumChoices.c_str());


    createGlobalParam(paramInst);
  }
}

void CarbonPortEdit::delParameter()
{
  foreach (QTreeWidgetItem *item, mPRMTree->selectedItems())
  {
    int index = mPRMTree->indexOfTopLevelItem(item);
    if (index != -1)
    {
      QVariant qv = item->data(0, Qt::UserRole);
      CarbonCfgXtorParamInst* paramInst = (CarbonCfgXtorParamInst*)qv.value<void*>();
      // remove all its connections
      for (UInt32 pi=0; pi<paramInst->numRTLPorts(); pi++)
      {
        CarbonCfgRTLPort* rtlPort = paramInst->getRTLPort(pi);
        for (UInt32 i=0; i<rtlPort->numConnections(); i++)
        {
          CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
          if (conn->castTieParam())
            removeConnection(conn, true, i);
        }
      }

      // Now, remove the ports
      for (UInt32 pi=0; pi<paramInst->numRTLPorts(); pi++)
      {
        CarbonCfgRTLPort* rtlPort = paramInst->getRTLPort(pi);
        paramInst->removeRTLPort(rtlPort);
      }

      mCfg->removeParam(paramInst);
      mPRMTree->takeTopLevelItem(index);
    }
    else // it's an RTL Item
    {
      UtString rtlPortName;
      rtlPortName << item->text(0);
      QVariant qv = item->data(0, Qt::UserRole);
      CarbonCfgXtorParamInst* paramInst = (CarbonCfgXtorParamInst*)qv.value<void*>();
      CarbonCfgRTLPort* rtlPort = paramInst->findRTLPort(rtlPortName.c_str());
      int childIndex = findChildIndex(item->parent(), 0, rtlPort->getName());
      if (childIndex != -1)
        item->parent()->takeChild(childIndex);
      paramInst->removeRTLPort(rtlPort);
      for (UInt32 i=0; i<rtlPort->numConnections(); i++)
      {
        CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
        if (conn->castTieParam())
          removeConnection(conn, true, i);
      }
    }
  } 
   mCQt->setModified();
}

void CarbonPortEdit::advancedBinding()
{
  mDialog->initialize();
  bool playback_active = mCQt->isPlaybackActive();
  if (playback_active)
    mDialog->updateData();

  if (playback_active || (CDialog::Accepted == mDialog->exec()))
  {
    UtString buf;
    UtOStringStream os(&buf);

    os << UtIO::slashify << mDialog->getPrefix() << mDialog->getSuffix();
    
    mDialog->setData(buf);
    assignByName( mDialog->hasPrefix() ? mDialog->getPrefix() : NULL, mDialog->hasSuffix() ? mDialog->getSuffix() : NULL);
  }

}

void CarbonPortEdit::addClockButtons() {
  mClockInitialValue = mCQt->comboBox("clk_init");
  mClockInitialValue->addItem("Low");
  mClockInitialValue->addItem("High");
  CQT_CONNECT(mClockInitialValue, currentIndexChanged(int), this, changeClkInit(int));

  mClockDelay = mCQt->spinBox("clk_delay");
  mClockDelay->setRange(0, 2000000000);
  mClockDelay->setSingleStep(5);
  mClockDelay->setSuffix("%");
  mClockDelay->setFrame(false);
  CQT_CONNECT(mClockDelay, valueChanged(int), this, changeClkDelay(int));

  mCompCycles = mCQt->spinBox("clk_compCycles");
  mCompCycles->setRange(1, 999);
  mCompCycles->setSingleStep(1);
  mCompCycles->setFrame(false);
  CQT_CONNECT(mCompCycles, valueChanged(int), this, changeClkCompCycles(int));

  mClockCycles = mCQt->spinBox("clk_clockCycles");
  mClockCycles->setRange(1, 999);
  mClockCycles->setSingleStep(1);
  mClockCycles->setFrame(false);
  CQT_CONNECT(mClockCycles, valueChanged(int), this, changeClkClockCycles(int));

  mDutyCycle = mCQt->spinBox("clk_dutyCycle");
  mDutyCycle->setRange(1, 100);
  mDutyCycle->setSingleStep(5);
  mDutyCycle->setSuffix("%");
  mDutyCycle->setFrame(false);
  CQT_CONNECT(mDutyCycle, valueChanged(int), this, changeClkDutyCycle(int));

  mResetValidator = new CHexDecValidator(this);
  mResetActiveValue = mCQt->lineEdit("reset_activeValue");
  mResetActiveValue->setValidator(mResetValidator);
  CQT_CONNECT(mResetActiveValue, textChanged(const QString&), this, changeResetActiveValue(const QString&));

  mResetInactiveValue = mCQt->lineEdit("reset_inactiveValue");
  mResetInactiveValue->setValidator(mResetValidator);
  CQT_CONNECT(mResetInactiveValue, textChanged(const QString&), this, changeResetInactiveValue(const QString&));

  mResetCompCycles = mCQt->spinBox("reset_compCycles");
  mResetCompCycles->setRange(1, 999);
  mResetCompCycles->setSingleStep(1);
  mResetCompCycles->setFrame(false);
  CQT_CONNECT(mResetCompCycles, valueChanged(int), this, changeResetCompCycles(int));

  mResetClockCycles = mCQt->spinBox("reset_clockCycles");
  mResetClockCycles->setRange(1, 999);
  mResetClockCycles->setSingleStep(1);
  mResetClockCycles->setFrame(false);
  CQT_CONNECT(mResetClockCycles, valueChanged(int), this, changeResetClockCycles(int));

  mResetCyclesBefore = mCQt->spinBox("reset_CyclesBefore");
  mResetCyclesBefore->setRange(0, 999);
  mResetCyclesBefore->setSingleStep(1);
  mResetCyclesBefore->setFrame(false);
  CQT_CONNECT(mResetCyclesBefore, valueChanged(int), this, changeResetCyclesBefore(int));

  mResetCyclesAsserted = mCQt->spinBox("reset_CyclesAsserted");
  mResetCyclesAsserted->setRange(0, 999);
  mResetCyclesAsserted->setSingleStep(1);
  mResetCyclesAsserted->setFrame(false);
  CQT_CONNECT(mResetCyclesAsserted, valueChanged(int), this, changeResetCyclesAsserted(int));

  mResetCyclesAfter = mCQt->spinBox("reset_CyclesAfter");
  mResetCyclesAfter->setRange(0, 999);
  mResetCyclesAfter->setSingleStep(1);
  mResetCyclesAfter->setFrame(false);
  CQT_CONNECT(mResetCyclesAfter, valueChanged(int), this, changeResetCyclesAfter(int));

  mXtorClockMaster = mCQt->comboBox("xtor_clockMaster");
  CQT_CONNECT(mXtorClockMaster, currentIndexChanged(int), this, changeXtorClockMaster(int));
} // void CarbonPortEdit::addClockButtons


void CarbonPortEdit::clear() {
  mRTLTree->clear();
  mESLTree->clear();
  mPRMTree->clear();
  mItemPortMap.clear();
  mPortItemMap.clear();
  mConnItemMap.clear();
  mItemConnMap.clear();
  mItemXtorMap.clear();
  mXtorItemMap.clear();

  //mXtorNameChooser->clear();
  //mXtorNameChooserMap.clear();
  clearSelections();
  enableWidgets();
  mRTLTree->putDragData(NULL);
  mESLTree->putDragData(NULL);
  mPRMTree->putDragData(NULL);
  mPortItemMap.clear();
  mXtorIndexMap.clear();
  mXtorPortsMap.clear();
}

void CarbonPortEdit::enableWidgets() {
  // Enable the ESL Name editor only if exactly one of the
  // following is true:
  //   - there is exactly one ESL port selected
  //   - there are any number of Ties selected
  //   - there is exactly one transactor instance selected
  UInt32 numESLExprUsers = 
    mCurrentESLPorts.size() +
    mCurrentXtorConns.size();
  UInt32 numESLNameUsers =
    numESLExprUsers +
    (mCurrentTies.empty() ? 0 : 1) + // ok to have more than one tie
    mCurrentXtorInstances.size();
  mESLName->setEnabled(numESLNameUsers == 1);
  mESLPortExpr->setEnabled(numESLExprUsers == 1);
  bool xtors_or_esl_ports = !mCurrentXtorInstances.empty() ||
    !mCurrentESLPorts.empty();
  mSwitchXtorType->setEnabled(xtors_or_esl_ports);

  // Init/Delay/Hi are shared between Reset, Clock, and Xtors, but cannot
  // be used if there are multiple types active
  UInt32 numTypes = (!mCurrentClockGens.empty() +
                     !mCurrentResetGens.empty() +
                     !mCurrentXtorInstances.empty() +
                     !mCurrentESLPorts.empty());

  if ((numTypes == 1) && !mCurrentXtorInstances.empty()) {
    // The xtor enabling is taken care of entirely in selectXtorInst
    mPortExprFrame->hide();
    mClockGenFrame->hide();
    mResetGenFrame->hide();
    mXtorClockFrame->show();
    mParamEditFrame->hide();
  }
  else if (numESLExprUsers != 0) {
    // ESL Ports have one edit field: the port expression
    mPortExprFrame->show();
    mClockGenFrame->hide();
    mResetGenFrame->hide();
    mXtorClockFrame->hide();
    mParamEditFrame->hide();
  }
  else if (numTypes == 0 && numESLExprUsers == 0 && numESLNameUsers == 0) {
    mPortExprFrame->hide();
    mClockGenFrame->hide();
    mResetGenFrame->hide();
    mXtorClockFrame->hide();
    mParamEditFrame->show();
  }
  else {
    mParamEditFrame->hide();
    mPortExprFrame->hide();
    mXtorClockFrame->hide();
    bool ena_clk_rst = (numTypes == 1);
    if (ena_clk_rst) {
      if (!mCurrentClockGens.empty()) {
        mClockGenFrame->show();
        mResetGenFrame->hide();
      }
      if (!mCurrentResetGens.empty()) {
        mClockGenFrame->hide();
        mResetGenFrame->show();
      }
    } // if
    else {
      mClockGenFrame->hide();
      mResetGenFrame->hide();
    }
  } // else

  // Xtor widgets
  mAssignByName->setEnabled(!mCurrentXtorInstances.empty());

  if (mMode == RegTabWidget::ModelMaker)
    mAdvancedBind->setEnabled(!mCurrentXtorInstances.empty());

  mSwitchRTLType->setEnabled(!mCurrentRTLPorts.empty());
  mAddPort->setEnabled(!mCurrentRTLPorts.empty());
  if (mDeleteAction)
    mDeleteAction->setEnabled(isDeleteActive());
} // void CarbonPortEdit::enableWidgets

CarbonCfgESLPortID CarbonPortEdit::addConnection(CarbonCfgRTLPortID rtlPort) {
  CarbonCfgESLPortType eslType = CarbonCfg::convertRTLPortType(rtlPort);
  return addESLConnection(rtlPort, eslType);
}

CarbonCfgESLPortID CarbonPortEdit::addESLConnection(CarbonCfgRTLPortID rtlPort,
                                                    CarbonCfgESLPortType type)
{
  // Get a leaf name for the RTL port, use that as a starting point for
  // the ESL name
  const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
  UtString buf;
  if (node == NULL) {
    mCfg->getUniqueESLName(&buf, rtlPort->getName());
  }
  else {
    mCfg->getUniqueESLName(&buf, carbonDBNodeGetLeafName(mDB, node));
  }

  // Remove any exclusive connections (tie, reset, or clock)
  for (SInt32 i = rtlPort->numConnections() - 1; i >= 0; --i) {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    if (conn->isExclusive()) {
      removeConnection(conn, false, i);
    }
  }

  CarbonCfgESLPortID eslPort = carbonCfgAddESLPort(mCfg, rtlPort,
                                                   buf.c_str(), type);

  INFO_ASSERT(eslPort, "Failed to create ESL port");
  return eslPort;
}

QTreeWidgetItem* CarbonPortEdit::showParam(QTreeWidgetItem* parent, CarbonCfgXtorParamInst* paramInst)
{
  QTreeWidgetItem* paramItem = new QTreeWidgetItem(parent);

  CarbonCfgXtorParam* param = paramInst->getParam();
  CarbonCfgParamFlag flag = param->getFlag();

  switch (flag)
  {
  case eCarbonCfgXtorParamRuntime:
    {
     paramItem->setText(0, "run-time-param");
     qDebug() << "connections: " << paramInst->numRTLPorts();
    }
    break;
  case eCarbonCfgXtorParamCompile:
    paramItem->setText(0, "compile-time-param");
    break;
  case eCarbonCfgXtorParamBoth:
    paramItem->setText(0, "param");
    break;
  case eCarbonCfgXtorParamInit:
      paramItem->setText(0, "init-time-param");
    break;
  }

  if (paramInst->numRTLPorts() > 0)
  {
    qDebug() << "Connections found: " << paramInst->numRTLPorts();
  }
 
  paramItem->setText(1, paramInst->getParam()->getName());

  showParamValue(paramItem, paramInst);

  QVariant qv = qVariantFromValue((void*)paramInst);
  paramItem->setData(0, Qt::UserRole, qv);
 

  return paramItem;
}

QTreeWidgetItem* CarbonPortEdit::showConnection(CarbonCfgRTLConnectionID conn)
{
  CarbonCfgRTLPort* rtlPort = conn->getRTLPort();

  UtString connDesc;
  conn->getText(&connDesc);

  // The rtlPort can be null if not bound, we need to allow this
  if (rtlPort)
  {
    const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
    if (carbonDBIsTied(mDB, node))
    {
      connDesc.clear();
      const IODBRuntime* iodb = CarbonDatabasePriv::getIODB(mDB);
      const STSymbolTableNode* stNode = CarbonDatabasePriv::getSymTabNode(node);
      const DynBitVector* bv = iodb->getTieValue(stNode);
      UtString valStr;
      bv->format(&valStr, eCarbonHex);
      connDesc << "(" << valStr << ")";
    }
  }

  QTreeWidgetItem* conItem = mConnItemMap[conn]; // may already be created

  switch (conn->getType()) {
  case eCarbonCfgESLPort:
    conItem = showESLPort(conn->castESLPort(), conItem);
    break;
  case eCarbonCfgXtorConn:
    conItem = showXtorConn(conn->castXtorConn(), conItem);
    break;
  default: {
    QTreeWidgetItem* portItem = mPortItemMap[rtlPort];
    INFO_ASSERT(portItem, "Cannot find item from RTLPort");
    CarbonCfgRTLConnectionTypeIO type = conn->getType();
    if (conItem == NULL) {
      conItem = new QTreeWidgetItem(portItem);
    }
    mRTLTree->expandItem(portItem);
    
    qDebug() << type.getString() << connDesc.c_str();

    // Tie Params are specially handled for display purposes
    if (conn->getType() == eCarbonCfgTieParam)
    {
      CarbonCfgTieParam* tiep = conn->castTieParam();
      const char* xtorInstanceName = carbonCfgTieParamGetXtorInstanceName(tiep);
      const char* paramName = carbonCfgTieParamGetParam(tiep);
      const char* fullParamName = carbonCfgTieParamGetFullParam(tiep);
      CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(xtorInstanceName);
      if (xtorInst)
      {
        CarbonCfgXtorParamInst* paramInst = xtorInst->findParameterInst(paramName);
        CarbonCfgRTLPort* rp = paramInst->findRTLPort(tiep->getRTLPort()->getName());
        if (rp == NULL)
        {
          // We need to add it to the ESL tree here
          paramInst->addRTLPort(tiep->getRTLPort());
        }

        updateParamInstance(paramInst);
        qDebug() << paramInst->getValue();
      }
      else // Global parameter?
      {
        for (int i=0; i<mPRMTree->topLevelItemCount(); i++)
        {
          const char* paramName = carbonCfgTieParamGetParam(tiep);
          QTreeWidgetItem* topItem = mPRMTree->topLevelItem(i);
          QVariant qv = topItem->data(0, Qt::UserRole);
          CarbonCfgXtorParamInst* paramInst = (CarbonCfgXtorParamInst*)qv.value<void*>();
          CarbonCfgRTLPort* rp = paramInst->findRTLPort(tiep->getRTLPort()->getName());
          if (rp == NULL)
            paramInst->addRTLPort(tiep->getRTLPort());

          if (0 == strcmp(paramInst->getParam()->getName(), paramName))
          {
            updateParamInstance(paramInst, topItem);
            qDebug() << "got it";
            break;
          }
        }
        qDebug() << "Global";
      }

      connDesc.clear();
      connDesc << fullParamName;
    }

    conItem->setText(0, type.getString());
    conItem->setText(1, connDesc.c_str());
  }
  } // case eCarbonCfgESLPort:
  mItemConnMap[conItem] = conn;
  mConnItemMap[conn] = conItem;
  return conItem;
} // QTreeWidgetItem* CarbonPortEdit::showConnection


// Locate this parameter instance in
// the ESL Tree and show its connection
void CarbonPortEdit::updateParamInstance(CarbonCfgXtorParamInst* paramInst, QTreeWidgetItem* paramItem)
{
  QTreeWidgetItem* xtorItem = mXtorItemMap[paramInst->getInstance()];
  if (xtorItem)
  {
    for (int i=0; i<xtorItem->childCount(); i++)
    {
      QTreeWidgetItem* item = xtorItem->child(i);
      QVariant qv = item->data(0, Qt::UserRole);
      CarbonCfgXtorParamInst* pinst = (CarbonCfgXtorParamInst*)qv.value<void*>();
      if (paramInst == pinst)
      {
        UtString rtlPorts;
        for (UInt32 ir=0; ir<paramInst->numRTLPorts(); ir++)
        {
          CarbonCfgRTLPort* rtlPort = paramInst->getRTLPort(ir);
          rtlPorts << rtlPort->getName();
          if (ir < paramInst->numRTLPorts()-1)
            rtlPorts << ",";
        }
        item->setText(2, rtlPorts.c_str());
      }
    }
  }
  else // Global Parameter
  {
    for (UInt32 ir=0; ir<paramInst->numRTLPorts(); ir++)
    {
      CarbonCfgRTLPort* rtlPort = paramInst->getRTLPort(ir);
      QTreeWidgetItem* item = findChild(paramItem, 0, rtlPort->getName());
      if (item == NULL)
      {
        item = new QTreeWidgetItem(paramItem);
        QVariant qv = qVariantFromValue((void*)paramInst);
        item->setData(0, Qt::UserRole, qv);
        paramItem->setExpanded(true);
      }
      item->setText(0, rtlPort->getName());
    }
    mPRMTree->resizeColumnToContents(0);
  }
}

int CarbonPortEdit::findChildIndex(QTreeWidgetItem* parent, int col, const char* text)
{
  for (int i=0; i<parent->childCount(); i++)
  {
    QTreeWidgetItem* item = parent->child(i);
    UtString value;
    value << item->text(col);
    if (0 == strcmp(value.c_str(), text))
      return i;
  }
  return -1;
}
QTreeWidgetItem* CarbonPortEdit::findChild(QTreeWidgetItem* parent, int col, const char* text)
{
  for (int i=0; i<parent->childCount(); i++)
  {
    QTreeWidgetItem* item = parent->child(i);
    UtString value;
    value << item->text(col);
    if (0 == strcmp(value.c_str(), text))
      return item;
  }
  return NULL;
}

QTreeWidgetItem* CarbonPortEdit::showESLPort(CarbonCfgESLPort* eslPort,
                                             QTreeWidgetItem* conItem)
{
  // Distinguish between inputs and outputs in the type field
  CarbonCfgESLPortType type = eslPort->mType;
  const char* etxt = "Unknown";
  switch (type) {
    case eCarbonCfgESLInput:
      etxt = "ESLInput";
      break;

    case eCarbonCfgESLOutput:
      etxt = "ESLOutput";
      break;

    case eCarbonCfgESLInout:
      etxt = "ESLInout";
      break;
    case eCarbonCfgESLUndefined:
      INFO_ASSERT(type != eCarbonCfgESLUndefined, "Found an undefined component signal port type");
      break;
  }
  if (conItem == NULL) {
    conItem = new QTreeWidgetItem(mESLTree);
  }
  conItem->setText(0, etxt);
  conItem->setText(1, eslPort->getName());
  if (eslPort->getParamInstance() != NULL) {
    if (eslPort->getParamInstance()->getInstance() != NULL) 
    {
      UtString temp;
      temp << eslPort->getParamInstance()->getInstance()->getName() << ":" << eslPort->getParamInstance()->getParam()->getName();
      conItem->setText(3,temp.c_str());
    }
    else conItem->setText(3, eslPort->getParamInstance()->getParam()->getName());
  }
  CarbonCfgRTLPort* rtlPort = eslPort->getRTLPort();
  showPortExpr(conItem, rtlPort->getName(), eslPort->getName(),
               eslPort->getExpr());
  return conItem;
}

void CarbonPortEdit::showPortExpr(QTreeWidgetItem* conItem,
                                  const char* default_text,
                                  const char* var_name,
                                  const char* expr)
{
  if (*expr == '\0') {
    conItem->setText(2, default_text);
  }
  else {
    UtString buf;

    mESLPortExpr->clearIdents();
    if (var_name != NULL) {
      mESLPortExpr->addIdent(var_name);
    }
    if (mESLPortExpr->validate()) {
      // By default, copy the colors from column 0
      conItem->setTextColor(2, conItem->textColor(0));
      buf << default_text << " (" << expr << ")";
      conItem->setText(2, buf.c_str());
    }
    else {
      buf << "[" << mESLPortExpr->getErrorText() << "]";
      conItem->setText(2, buf.c_str());
      conItem->setTextColor(2, mESLPortExpr->getColor());
    }
  } // else
} // void CarbonPortEdit::showPortExpr

QTreeWidgetItem* CarbonPortEdit::showXtorConn(CarbonCfgXtorConn* xcon,
                                              QTreeWidgetItem* conItem)
{
  CarbonCfgXtorInstance* xi = xcon->getInstance();
  CarbonCfgXtorPort* xp = xcon->getPort();
  QTreeWidgetItem* xitem = mXtorItemMap[xi];
  INFO_ASSERT(xitem, "cannot find item from xtor");
  if (conItem == NULL) {
    conItem = new QTreeWidgetItem(xitem);
  }
  conItem->setText(0, gCarbonRTLPortTypes[(int) xp->getType()]);
  conItem->setText(1, xp->getName());
  CarbonCfgRTLPort* rtlPort = xcon->getRTLPort();
  const char* expr = xcon->getExpr();

  if (rtlPort == NULL) {
    // An expression on an unconnected xtor-field could be a legitmate tie
    showPortExpr(conItem, "", NULL, expr);

    // An xtor conn with neither an RTL connection or an expression
    // displays like this:
    if (*expr == '\0') {
      conItem->setText(2, "(drag and drop to connect RTL)");
      conItem->setTextColor(2, QColor(Qt::darkRed));
    }
  }
  else {
    showPortExpr(conItem, rtlPort->getName(), xp->getName(), expr);
  }
  mESLTree->expandItem(xitem);
  return conItem;
} // QTreeWidgetItem* CarbonPortEdit::showXtorConn

void CarbonPortEdit::updateWidgets() {
  CQtRecordBlocker blocker(mCQt);
  clear();

  UtStringArray deferredIOs;

  // Create the global parameters
  for (UInt32 i = 0; i < mCfg->numParams(); i++)
  {
    CarbonCfgXtorParamInst* paramInst = mCfg->getParam(i);
    createGlobalParam(paramInst);
  }

  // First, show the transactor top level items in the ESL tree.
  for (unsigned int i = 0, n = mCfg->numXtorInstances(); i < n; ++i) {
    CarbonCfgXtorInstance* xtorInstance = mCfg->getXtorInstance(i);
    showXtorInstance(xtorInstance);
  }

  // First pass: display the RTL ports and the ESL signal ports.  Ignore
  // the xtor connections to the RTL ports
  for (unsigned int i = 0, n = mCfg->numRTLPorts(); i < n; ++i) {
    CarbonCfgRTLPort* port = mCfg->getRTLPort(i);
    QTreeWidgetItem* item = new QTreeWidgetItem(mRTLTree);
    mPortItemMap[port] = item;
    mItemPortMap[item] = port;

    drawRTLPortItem(port);
    for (UInt32 i = 0, n = port->numConnections(); i < n; ++i) {
      CarbonCfgRTLConnection* conn = port->getConnection(i);
      showConnection(conn);
    }
  } // for

  mCurrentRTLSwitchIndex = 0;

  ++mSelectingRTL;
  mESLTree->deselectAll();

  for (UInt32 i = 0, n = mCfg->numRTLPorts(); i < n; ++i) {
    CarbonCfgRTLPort* port = mCfg->getRTLPort(i);
    if (carbonDBIsTied(mDB, carbonDBFindNode(mDB, port->getName()))) {
      selectRTL(port);
      switchParticularRTLPortType(port, 4);
    }
  }
  --mSelectingRTL;
  changeRTLTreeSel();

  // Set up the 'width' column to be small
  mRTLTree->resizeColumnToContents(2);

  // Set the 'properties' column to be larger, potentially
  mRTLTree->resizeColumnToContents(3);

/*
  We are not going to do all of these because the 'clockgen'
  entries will screw us up, as they have very long entries in
  column 1.  Maybe I could split it up and put some of their
  entries in column 3, as column 2 should be small, since it's
  the width
  for (UInt32 i = 0; i < 4; ++i) {
    mRTLTree->resizeColumnToContents(i);
  }
*/
  for (UInt32 i = 0; i < 3; ++i) {
    mESLTree->resizeColumnToContents(i);
  }

} // void CarbonPortEdit::updateWidgets

QTreeWidgetItem* CarbonPortEdit::showXtorInstance(CarbonCfgXtorInstance* xtorInstance)
{
  QTreeWidgetItem* item = mXtorItemMap[xtorInstance];
  if (item == NULL) {
    item = new QTreeWidgetItem(mESLTree);
  }
  CarbonCfgXtor* xtor = xtorInstance->getType();
  item->setText(0, xtor->getName());
  item->setText(1, xtorInstance->getName());
  item->setText(2, "");

  mXtorItemMap[xtorInstance] = item;
  mItemXtorMap[item] = xtorInstance;


  // First, show the parameters (if any)
  CarbonCfgXtor::ParamLoop params = xtor->loopParams();
  for (; !params.atEnd(); ++params) {
    UtString paramName = params.getKey();
    CarbonCfgXtorParamInst* paramInst = xtorInstance->findParameterInst(paramName);
    showParam(item, paramInst);
  }

  for (UInt32 i = 0, n = xtor->numPorts(); i < n; ++i) {
    CarbonCfgXtorConn* conn = xtorInstance->getConnection(i);
    showConnection(conn);
  }

  return item;
} // void CarbonPortEdit::showXtorInstance

QTreeWidgetItem* CarbonPortEdit::drawRTLPortItem(CarbonCfgRTLPort* port) {
  QTreeWidgetItem* item = mPortItemMap[port];
  INFO_ASSERT(item, "cannot find item from rtl port");
  switch (port->getType()) {
  case eCarbonCfgRTLInput:  item->setText(0, "input");  break;
  case eCarbonCfgRTLOutput: item->setText(0, "output"); break;
  case eCarbonCfgRTLInout:  item->setText(0, "inout");  break;
  }
  item->setText(1, port->getName());
  UtString buf;
  buf << port->getWidth();
  item->setText(2, buf.c_str());
  buf.clear();

  for (UInt32 i = 0, n = port->numConnections(); i < n; ++i) {
    CarbonCfgRTLConnection* conn = port->getConnection(i);
    if (!buf.empty()) {
      buf << ", ";
    }

    const CarbonDBNode* node = carbonDBFindNode(mDB, port->getName());
   
    if (conn->castTie() && carbonDBIsTied(mDB, node))
    {
      buf.clear();
      const IODBRuntime* iodb = CarbonDatabasePriv::getIODB(mDB);
      const STSymbolTableNode* stNode = CarbonDatabasePriv::getSymTabNode(node);
      const DynBitVector* bv = iodb->getTieValue(stNode);
      UtString valStr;
      bv->format(&valStr, eCarbonHex);
      buf << "(" << valStr << ")";
    }
    else if (conn->getType() == eCarbonCfgTieParam)
    {
      CarbonCfgTieParam* tiep = conn->castTieParam();
      buf.clear();
      buf = carbonCfgTieParamGetXtorInstanceName(tiep);
    }
    else
      conn->getText(&buf);
  }
  item->setText(3, buf.c_str());
  return item;
}

void CarbonPortEdit::populate(CarbonDB* carbonDB, bool initPorts) {
  mDB = carbonDB;
  if (initPorts) {
    clear();
    populateLoop(carbonDB, eCarbonCfgRTLInput,
                 carbonDBLoopPrimaryInputs(carbonDB));
    populateLoop(carbonDB, eCarbonCfgRTLInout,
                 carbonDBLoopPrimaryBidis(carbonDB));
    populateLoop(carbonDB, eCarbonCfgRTLOutput,
                 carbonDBLoopPrimaryOutputs(carbonDB));
    populateLoop(carbonDB, eCarbonCfgRTLInput,
                 carbonDBLoopScDepositable(carbonDB));
    populateLoop(carbonDB, eCarbonCfgRTLOutput,
                 carbonDBLoopScObservable(carbonDB));
    updateWidgets();
  }
} // void CarbonPortEdit::loadDatabase


void CarbonPortEdit::populateLoop(CarbonDB* carbonDB,
                                 CarbonCfgRTLPortType type,
                                 CarbonDBNodeIter* iter)
{
  const CarbonDBNode* node;
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    int width = carbonDBGetWidth(carbonDB, node);
    const char* name = carbonDBNodeGetFullName(carbonDB, node);
    CarbonCfgRTLPortID port = carbonCfgAddRTLPort(mCfg, name, width, type);
    if (port != NULL) {
      (void) addConnection(port);
    }
  }
  carbonDBFreeNodeIter(iter);
}

/*
int CarbonPortEdit::value() const
{
    return slider->value();
}

void CarbonPortEdit::setValue(int value)
{
    slider->setValue(value);
}

void CarbonPortEdit::itemExpanded(QTreeWidgetItem* item) {
  UtIO::cout() << "expanded!" << UtIO::endl;
  for (int j = 0; j < 3; ++j) {
    QTreeWidgetItem* sub = new QTreeWidgetItem(item);
    char buf[100];
    sprintf(buf, "sub_item-%d", j);
    sub->setText(0, buf);
    sprintf(buf, "0x%lx", (long) sub);
    sub->setText(1, buf);
  }
}
*/

void CarbonPortEdit::changePRMTreeSel()
{
  if (mSelectingPRM != 0) 
    return;

  ++mSelectingPRM;

  if (mSelectingESL == 0) 
  {
    clearSelections();
    mESLTree->deselectAll();
  }

  if (mSelectingRTL == 0)
  {
    clearSelections();
    mRTLTree->deselectAll();
  }

  //mDelParam->setEnabled(mPRMTree->selectedItems().count() > 0);
  mGlobParam.clear();
  UtOStringStream os(&mGlobParam);
 
  // Walk through the tree and discover which item is now selected
  QTreeItemList sel = mPRMTree->selectedItems();
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) 
  {
    QTreeWidgetItem* item = *p;    
    QVariant qv = item->data(0, Qt::UserRole);

    CarbonCfgXtorParamInst* paramInst = (CarbonCfgXtorParamInst*)qv.value<void*>();
    if (paramInst)
      os << UtIO::slashify << paramInst->getParam()->getName();
  }

  if (mGlobParam.length() > 0)
    mPRMTree->putDragData(mGlobParam.c_str());
  else
    mPRMTree->putDragData(NULL);

  //UtOStringStream os(&mGlobParam);
  //os << UtIO::slashify << paramInst->getInstance()->getName() << paramInst->getParam()->getName();
  //
  //mGlobParam = paramInst;
  //
  //qDebug() << "new value: '" << mGlobParam.c_str() << "'";
  //mPRMTree->putDragData(mGlobParam.c_str());

  --mSelectingPRM;
}

void CarbonPortEdit::changeRTLTreeSel() {
  if (mSelectingRTL != 0) {
    return;
  }

  CQtRecordBlocker blocker(mCQt);
  ++mSelectingRTL;
  if (mSelectingESL == 0) {
    clearSelections();
    mESLTree->deselectAll();
  }

  // Walk through the tree and discover which item is now selected
  QTreeItemList sel = mRTLTree->selectedItems();
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;    
    selectItem(item);
  }
  if (mSelectingESL == 0) {
    enableWidgets();
  }
  --mSelectingRTL;
} // void CarbonPortEdit::changeRTLTreeSel

void CarbonPortEdit::selectItem(QTreeWidgetItem* item) {
  mParamInstance = NULL;

  ItemPortMap::iterator p = mItemPortMap.find(item);
  if (p != mItemPortMap.end()) {
    CarbonCfgRTLPortID port = p->second;
    selectRTL(port);
  }
  else {
    ItemConnMap::iterator p = mItemConnMap.find(item);
    if (p != mItemConnMap.end()) {
      CarbonCfgRTLConnectionID conn = p->second;
      selectConnection(conn);
    }
    else {
      bool paramSelected = false;
      ItemXtorMap::iterator p = mItemXtorMap.find(item);
      if (p == mItemXtorMap.end()) {
        QVariant qv = item->data(0, Qt::UserRole);

        CarbonCfgXtorParamInst* paramInst = (CarbonCfgXtorParamInst*)qv.value<void*>();
        if (paramInst != NULL) {
          paramSelected = true;
          mParamInst.clear();
          UtOStringStream os(&mParamInst);
          os << UtIO::slashify << paramInst->getInstance()->getName() << paramInst->getParam()->getName();
          mParamInstance = paramInst;
          qDebug() << "new value: '" << mParamInst.c_str() << "'";

          mESLTree->putDragData(mParamInst.c_str());
          showParamEditor(paramInst);

          // Update the RTL port mapped to this
          if (mSelectingESL != 0)
          {
            for (UInt32 ri=0; ri<paramInst->numRTLPorts(); ri++)
            {
              CarbonCfgRTLPort* rtlPort = paramInst->getRTLPort(ri);
              QTreeWidgetItem* portItem = mPortItemMap[rtlPort];
              if (portItem)
                portItem->setSelected(true);
            }
          }
        }
      }

      if (!paramSelected) {
        INFO_ASSERT(p != mItemXtorMap.end(), "could not find selected xtor");
        CarbonCfgXtorInstance* xinst = p->second;
        selectXtorInst(xinst);
      }
    }
  }
}

void CarbonPortEdit::showParamEditor(CarbonCfgXtorParamInst* paramInst)
{
  mParamEditFrame->setTitle(tr("Parameter %1 - %2")
                            .arg(paramInst->getParam()->getName())
                            .arg(paramInst->getParam()->getDescription()));
  mParamEditor->setText(paramInst->getValue());
}


void CarbonPortEdit::selectXtorInst(CarbonCfgXtorInstance* xinst) {
  CarbonCfgXtor* xtor = xinst->getType();
  for (UInt32 i = 0, n = xtor->numPorts(); i < n; ++i) {
    CarbonCfgXtorConn* conn = xinst->getConnection(i);
    CarbonCfgRTLPort* rtlPort = conn->getRTLPort();
    if (rtlPort != NULL) {
      selectConnection(conn);
    }
  }
  mESLName->setText(xinst->getName());

  // see if any clock master tranactors are selected.
  bool clockMasterSelected = xinst->getType()->isXtorClockMaster();
  if (!clockMasterSelected) {
    for (UInt32 i = 0; i < mCurrentXtorInstances.size(); ++i) {
      CarbonCfgXtorInstance* inst = mCurrentXtorInstances[i];
      if (inst->getType()->isXtorClockMaster()) {
        clockMasterSelected = true;
        break;
      }
    }
  }

  // if there are no clock masters in the selected list, then build up
  // the pull-down list of potential clocks by adding all instances of
  // clock master transactors.
  int selectedIndex = 0;
  mXtorClockMaster->clear();
  mXtorClockMaster->addItem("Use component clock");
  if (!clockMasterSelected) {
    int itemIndex = 0;
    for (unsigned int i = 0, n = carbonCfgNumXtorInstances(mCfg); i < n; ++i) {
      CarbonCfgXtorInstanceID inst = carbonCfgGetXtorInstance(mCfg, i);
      CarbonCfgXtorID xtorType = carbonCfgXtorInstanceGetXtor(inst);
      if (carbonCfgXtorIsXtorClockMaster(xtorType)) {
        mXtorClockMaster->addItem(carbonCfgXtorInstanceGetName(inst));
        ++itemIndex;
        if (inst == xinst->getClockMaster()) {
          selectedIndex = itemIndex;
        }
      }
    }
  }

  mClockWidgetMode = eXtor;

  // clock master xtors cannot be clocked by other xtors
  mXtorClockMaster->setEnabled(!clockMasterSelected);

  // select the current clock master
  mXtorClockMaster->setCurrentIndex(selectedIndex);

  // Search for the xtor type
  for (CarbonCfg::XtorLibs::SortedLoop l = mCfg->loopXtorLibs(); !l.atEnd(); ++l) {
    CarbonCfgXtorLib* xlib = l.getValue();
    for (UInt32 i = 0; i < xlib->numXtors(); ++i) {
      if (xtor == xlib->getXtor(i)) {
        mCurrentXtorSwitchIndex = i;
        mSwitchXtorType->setCurrentIndex(i);
        break;
      }
    }
  }
  mCurrentXtorInstances.push_back(xinst);
} // void CarbonPortEdit::selectXtorInst

void CarbonPortEdit::selectConnection(CarbonCfgRTLConnectionID conn) {
  // If we are working with the ESL tree, clear any selections on
  // the RTL tree and select any RTL port associated with what we
  // are looking at now.
  if (mSelectingRTL == 0) {
    CarbonCfgRTLPortID rtlPort = conn->getRTLPort();
    if (rtlPort != NULL) {
      QTreeWidgetItem* portItem = mPortItemMap[rtlPort];
      INFO_ASSERT(portItem, "Cannot find item for RTLPort");
      mRTLTree->setItemSelected(portItem, true);
    }
  }

  CarbonCfgRTLPortID rtlPort = conn->getRTLPort();
  if (rtlPort == NULL)
    return;

  // Update & show the new widgets
  mCurrentType = conn->getType();
  
  switch (mCurrentType) {
  case eCarbonCfgESLPort:
    updateESLWidgets(conn->castESLPort());
    break;
  case eCarbonCfgClockGen:
    updateClkWidgets(conn->castClockGen());
    break;
  case eCarbonCfgResetGen:
    updateResetWidgets(conn->castResetGen());
    break;
  case eCarbonCfgTieParam:
    updateTieParamWidgets(conn->castTieParam());
    break;
  case eCarbonCfgTie:
    updateTieWidgets(conn->castTie());
    break;
  case eCarbonCfgXtorConn:
    updateXtorConn(conn->castXtorConn());
    break;
  case eCarbonCfgSystemCClock:
    // Not supported in old GUI
    break;
  }
  mCurrentConnections.insert(conn);
} // void CarbonPortEdit::selectConnection

void CarbonPortEdit::clearSelections() {
  mCurrentESLPorts.clear();
  mCurrentXtorConns.clear();
  mCurrentClockGens.clear();
  mCurrentResetGens.clear();
  mCurrentTies.clear();
  mCurrentXtorInstances.clear();
  mCurrentXtorSwitchIndex = -1;
  mCurrentRTLSwitchIndex = -1;
  mCurrentConnections.clear();
  mCurrentRTLPorts.clear();
}

void CarbonPortEdit::selectRTL(CarbonCfgRTLPortID port) {
  ++mSelectingRTL;

  mPRMTree->deselectAll();

  // Any port connections should be highlighted in the ESL tree
  int switchIndex = 5;        // disconnect
  for (UInt32 i = 0; i < port->numConnections(); ++i) {
    CarbonCfgRTLConnection* conn = port->getConnection(i);
    switch (conn->getType()) {
    case eCarbonCfgClockGen:   switchIndex = 2;  break;
    case eCarbonCfgResetGen:   switchIndex = 3;  break;
    case eCarbonCfgTie:        switchIndex = 4;  break;
    case eCarbonCfgSystemCClock: switchIndex = 5; break;
    case eCarbonCfgTieParam:
      {
        CarbonCfgTieParam* tiep = conn->castTieParam();
        if (mSelectingESL == 0) 
          selectTieParam(tiep);
      }                             
      break;
    case eCarbonCfgXtorConn: {
      switchIndex = -1;
      if (mSelectingESL == 0) {
        QTreeWidgetItem* conItem = mConnItemMap[conn];
        INFO_ASSERT(conItem, "Cannot find item for connection");
        QTreeWidget* tree = conItem->treeWidget();
        if (tree == mESLTree) {
          mESLTree->setItemSelected(conItem, true);
        }
      }
      break;
    }
    case eCarbonCfgESLPort: {
      CarbonCfgESLPort* eslPort = conn->castESLPort();
      switchIndex = (eslPort->mType == eCarbonCfgESLInput) ? 0 : 1;
      if (mSelectingESL == 0) {
        QTreeWidgetItem* conItem = mConnItemMap[conn];
        INFO_ASSERT(conItem, "Cannot find item for connection");
        QTreeWidget* tree = conItem->treeWidget();
        if (tree == mESLTree) {
          mESLTree->setItemSelected(conItem, true);
          //QColor dropHighlightColor = QColor(Qt::green);
          //for (UInt32 ci = 0; ci < mESLTree->columnCount(); ++ci) 
          //{
          //  conItem->setBackgroundColor(ci, dropHighlightColor);
          //}
        }
      }
      break;
    }
    }
  } // for

  mCurrentRTLSwitchIndex = switchIndex;
  if (switchIndex != -1) {
    mSwitchRTLType->setCurrentIndex(switchIndex);
  }
  mCurrentRTLPorts.push_back(port);
  mRTLTree->putDragData(port->getName());
  --mSelectingRTL;
} // void CarbonPortEdit::selectRTL

void CarbonPortEdit::changeESLTreeSel() {
  if (mSelectingESL != 0) {
    return;
  }

  CQtRecordBlocker blocker(mCQt);
  ++mSelectingESL;
  if (mSelectingRTL == 0) {
    clearSelections();
    mRTLTree->deselectAll();
  }

  // Walk through the tree and discover which item is now selected
  QTreeItemList sel = mESLTree->selectedItems();
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;
    selectItem(item);
  }
  if (mSelectingRTL == 0) {
    enableWidgets();
  }
  --mSelectingESL;
} // void CarbonPortEdit::changeESLTreeSel

void CarbonPortEdit::updateClkWidgets(CarbonCfgClockGen* clock) {
  mClockInitialValue->setCurrentIndex(clock->mInitialValue);
  mClockDelay->setValue(clock->mDelay);
  mClockCycles->setValue(clock->mFrequency.getClockCycles());
  mCompCycles->setValue(clock->mFrequency.getCompCycles());
  mDutyCycle->setValue(clock->mDutyCycle);
  mCurrentClockGens.push_back(clock);
}


void CarbonPortEdit::removeTieParam(CarbonCfgTieParam* tiep)
{
  const char* xtorInstanceName = carbonCfgTieParamGetXtorInstanceName(tiep);
  const char* paramName = carbonCfgTieParamGetParam(tiep);
  CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(xtorInstanceName);
  if (xtorInst)
  {
    CarbonCfgXtorParamInst* paramInst = xtorInst->findParameterInst(paramName);
    if (paramInst)
    {
      QTreeWidgetItem* xtorItem = mXtorItemMap[paramInst->getInstance()];
      if (xtorItem)
      {
        for (int i=0; i<xtorItem->childCount(); i++)
        {
          QTreeWidgetItem* item = xtorItem->child(i);
          QVariant qv = item->data(0, Qt::UserRole);
          CarbonCfgXtorParamInst* pinst = (CarbonCfgXtorParamInst*)qv.value<void*>();
          if (paramInst == pinst)
          {
            CarbonCfgRTLPort* rtlPort = tiep->getRTLPort();
            pinst->removeRTLPort(rtlPort);
            item->setText(2, "");
            break;
          }
        }
      }
    }
  }
  else // Global Parameter
  {
    CarbonCfgRTLPort* rtlPort = tiep->getRTLPort();
    for (int i=0; i<mPRMTree->topLevelItemCount(); i++)
    {
      QTreeWidgetItem* topItem = mPRMTree->topLevelItem(i);
      QVariant qv = topItem->data(0, Qt::UserRole);
      CarbonCfgXtorParamInst* paramInst = (CarbonCfgXtorParamInst*)qv.value<void*>();
      if (0 == strcmp(paramInst->getParam()->getName(), paramName))
      {
        int childIndex = findChildIndex(topItem, 0, rtlPort->getName());
        if (childIndex != -1)
          topItem->takeChild(childIndex);
        paramInst->removeRTLPort(rtlPort);
      }
    }
  }
}

void CarbonPortEdit::selectTieParam(CarbonCfgTieParam* tiep)
{
  const char* xtorInstanceName = carbonCfgTieParamGetXtorInstanceName(tiep);
  const char* paramName = carbonCfgTieParamGetParam(tiep);
  CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(xtorInstanceName);
  if (xtorInst)
  {
    CarbonCfgXtorParamInst* paramInst = xtorInst->findParameterInst(paramName);
    if (paramInst)
    {
      QTreeWidgetItem* xtorItem = mXtorItemMap[paramInst->getInstance()];
      if (xtorItem)
      {
        for (int i=0; i<xtorItem->childCount(); i++)
        {
          QTreeWidgetItem* item = xtorItem->child(i);
          QVariant qv = item->data(0, Qt::UserRole);
          CarbonCfgXtorParamInst* pinst = (CarbonCfgXtorParamInst*)qv.value<void*>();
          if (paramInst == pinst)
          {
            item->setSelected(true);
            break;
          }
        }
      }
    }
  }
  else // Global Parameter
  {
    for (int i=0; i<mPRMTree->topLevelItemCount(); i++)
    {
      QTreeWidgetItem* topItem = mPRMTree->topLevelItem(i);
      QVariant qv = topItem->data(0, Qt::UserRole);
      CarbonCfgXtorParamInst* paramInst = (CarbonCfgXtorParamInst*)qv.value<void*>();
      if (0 == strcmp(paramInst->getParam()->getName(), paramName))
      {
        topItem->setSelected(true);
      }
    }
  }
}

void CarbonPortEdit::updateTieParamWidgets(CarbonCfgTieParam* tiep)
{
  // We clicked in the RTL pane
  if (mSelectingRTL != 0)
    selectTieParam(tiep);

 // const char* paramName = carbonCfgTieParamGetParam(tiep);
  const char* instanceName = carbonCfgTieParamGetXtorInstanceName(tiep);
  mESLName->setText(instanceName);
}


void CarbonPortEdit::updateTieWidgets(CarbonCfgTie* tie) {
  UtString buf;
  UtOStringStream ss(&buf);
  ss << UtIO::hex << tie->mValue;
 
  const CarbonDBNode* node = carbonDBFindNode(mDB, tie->getRTLPort()->getName());
  if (carbonDBIsTied(mDB, node))
  {
    const IODBRuntime* iodb = CarbonDatabasePriv::getIODB(mDB);
    const STSymbolTableNode* stNode = CarbonDatabasePriv::getSymTabNode(node);
    const DynBitVector* bv = iodb->getTieValue(stNode);
    UtString valStr;
    bv->format(&valStr, eCarbonHex);
    mESLName->setText(valStr.c_str());
  }
  else
    mESLName->setText(buf.c_str());
  
  mCurrentTies.push_back(tie);
}

void CarbonPortEdit::updateResetWidgets(CarbonCfgResetGen* reset) {
  // Set the range of the validator based on the port width
  // So that a user can't type in a too large value
  // Both Active and Inactive value is using the same instance of the validator
  UInt32 rtlWidth = reset->getRTLPort()->getWidth();
  mResetValidator->setRange(0, (1LL<<rtlWidth)-1);
  mResetActiveValue->setText(QString("0x") + QString().setNum(reset->mActiveValue, 16));
  mResetInactiveValue->setText(QString("0x") + QString().setNum(reset->mInactiveValue, 16));
  mResetClockCycles->setValue((int) reset->mFrequency.getClockCycles());
  mResetCompCycles->setValue((int) reset->mFrequency.getCompCycles());
  mResetCyclesBefore->setValue((int) reset->mCyclesBefore);
  mResetCyclesAsserted->setValue((int) reset->mCyclesAsserted);
  mResetCyclesAfter->setValue((int) reset->mCyclesAfter);
  mCurrentResetGens.push_back(reset);
}

void CarbonPortEdit::updateESLWidgets(CarbonCfgESLPort* eslPort) {
  const char* name = eslPort->getName();
  mESLName->setText(name);
  const char* expr = eslPort->getExpr();
  mESLPortExpr->clearIdents();
  mESLPortExpr->addIdent(name);
  mESLPortExpr->setText(expr);
  CarbonCfgRTLPort* rtlPort = eslPort->getRTLPort();
  QTreeWidgetItem* conItem = mConnItemMap[eslPort];
  showPortExpr(conItem, rtlPort->getName(), name, expr);
  mCurrentESLPorts.push_back(eslPort);
  mCurrentRTLSwitchIndex = (eslPort->mType == eCarbonCfgESLInput) ? 0 : 1;
  mSwitchRTLType->setCurrentIndex(mCurrentRTLSwitchIndex);
}

void CarbonPortEdit::updateXtorConn(CarbonCfgXtorConn* xtorConn) {
  if (mSelectingESL != 0) {
    UtString buf;
    UtOStringStream os(&buf);
    os << UtIO::slashify << xtorConn->getInstance()->getName()
       << xtorConn->getPortIndex();
    mESLTree->putDragData(buf.c_str());
    mCurrentXtorConns.push_back(xtorConn);
    mESLPortExpr->clearIdents();

    // Only a connected Xtor port can use a port expression that references
    // the field name.  Unconnected xtor ports can only be tied.
    if (xtorConn->getRTLPort() != NULL) {
      mESLPortExpr->addIdent(xtorConn->getPort()->getName());
    }
    mESLPortExpr->setText(xtorConn->getExpr());
  }
}


bool CarbonPortEdit::deleteHelper(bool dry_run) {
  for (SInt32 i = mCurrentXtorInstances.size() - 1; i >= 0; --i) {
    if (dry_run) {
      return true;
    }
    removeXtorInstance(mCurrentXtorInstances[i]);
  }
  bool check_rtl = true;

  // Copy the current connections before iterating because we
  // will be removing as we iterate
  ConSet curCon(mCurrentConnections);
  for (ConSet::iterator p = curCon.begin(), e = curCon.end(); p != e; ++p) {
    CarbonCfgRTLConnection* conn = *p;
    CarbonCfgRTLPort* rtlPort = conn->getRTLPort();
    if (rtlPort != NULL) {
      if (dry_run) {
        return true;
      }
      bool removeIt = true;
      const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
      if (carbonDBIsTied(mDB, node))
      {
        UtString msg;
        msg << "The port: " << rtlPort->getName() << " is tied off by a compiler directive.\n";
        msg << "Are you sure you want to remove it?";
        
        if (QMessageBox::question(this, MODELSTUDIO_TITLE, msg.c_str(), 
          QMessageBox::No|QMessageBox::Yes, QMessageBox::No) == QMessageBox::No)
          removeIt = false;
      }

      if (removeIt)
      {
        removeConnection(conn, true);
        check_rtl = false;
      }
    }
  }
  if (check_rtl) {
    for (SInt32 i = mCurrentRTLPorts.size() - 1; i >= 0; --i) {
      if (dry_run) {
        return true;
      }
      disconnectRTLPort(mCurrentRTLPorts[i], true);
    }
  }
  return false;
} // bool CarbonPortEdit::deleteHelper

void CarbonPortEdit::deleteCurrent() {
  ++mSelectingESL;
  ++mSelectingRTL;
  deleteHelper(false);
  --mSelectingESL;
  --mSelectingRTL;
  clearSelections();
  changeRTLTreeSel();
  delParameter();
}

bool CarbonPortEdit::isDeleteActive() const {
  CarbonPortEdit* pe = const_cast<CarbonPortEdit*>(this);
  return pe->deleteHelper(true);
}

void CarbonPortEdit::changeClkInit(int val) {
  CQtRecordBlocker blocker(mCQt);

  if ((val == -1) || (mSelectingESL != 0)) {
    return;
  }

  for (UInt32 i = 0; i < mCurrentClockGens.size(); ++i) {
    CarbonCfgClockGen* clock = mCurrentClockGens[i];
    clock->mInitialValue = val;
    showConnection(clock);
    drawRTLPortItem(clock->getRTLPort());
  }
}

void CarbonPortEdit::changeClkDelay(int val) {
  if ((val == -1) || (mSelectingESL != 0)) {
    return;
  }

  CQtRecordBlocker blocker(mCQt);
  for (UInt32 i = 0; i < mCurrentClockGens.size(); ++i) {
    CarbonCfgClockGen* clock = mCurrentClockGens[i];
    clock->mDelay = val;
    showConnection(clock);
    drawRTLPortItem(clock->getRTLPort());
  }
}

void CarbonPortEdit::changeClkDutyCycle(int val) {
  if ((val == -1) || (mSelectingESL != 0)) {
    return;
  }

  CQtRecordBlocker blocker(mCQt);
  for (UInt32 i = 0; i < mCurrentClockGens.size(); ++i) {
    CarbonCfgClockGen* clock = mCurrentClockGens[i];
    clock->mDutyCycle = val;
    showConnection(clock);
    drawRTLPortItem(clock->getRTLPort());
  }
}

void CarbonPortEdit::changeClkCompCycles(int val) {
  if ((val == -1) || (mSelectingESL != 0)) {
    return;
  }

  CQtRecordBlocker blocker(mCQt);
  for (UInt32 i = 0; i < mCurrentClockGens.size(); ++i) {
    CarbonCfgClockGen* clock = mCurrentClockGens[i];
    clock->mFrequency.putCompCycles(val);
    showConnection(clock);
    drawRTLPortItem(clock->getRTLPort());
  }
}

void CarbonPortEdit::changeClkClockCycles(int val) {
  if ((val == -1) || (mSelectingESL != 0)) {
    return;
  }

  CQtRecordBlocker blocker(mCQt);
  for (UInt32 i = 0; i < mCurrentClockGens.size(); ++i) {
    CarbonCfgClockGen* clock = mCurrentClockGens[i];
    clock->mFrequency.putClockCycles(val);
    showConnection(clock);
    drawRTLPortItem(clock->getRTLPort());
  }
}

void CarbonPortEdit::changeResetActiveValue(const QString& str) {
  if (mSelectingESL != 0) {
    return;
  }
  CQtRecordBlocker blocker(mCQt);
  for (UInt32 i = 0; i < mCurrentResetGens.size(); ++i) {
    CarbonCfgResetGen* reset = mCurrentResetGens[i];

    // Check if it's a hex number
    if(str.left(2) == "0x") {
      bool ok = false;
      UInt64 val = str.right(str.length()-2).toULongLong(&ok, 16);
      if(ok) reset->mActiveValue = val;
    }
    // Otherwise it must be a decimal value
    else {
      bool ok = false;
      UInt64 val = str.toULongLong(&ok);
      if(ok) reset->mActiveValue = val;
    }
    showConnection(reset);
    drawRTLPortItem(reset->getRTLPort());
  }
}

void CarbonPortEdit::changeResetInactiveValue(const QString& str) {
  if (mSelectingESL != 0) {
    return;
  }
  CQtRecordBlocker blocker(mCQt);
  for (UInt32 i = 0; i < mCurrentResetGens.size(); ++i) {
    CarbonCfgResetGen* reset = mCurrentResetGens[i];

    // Check if it's a hex number
    if(str.left(2) == "0x") {
      bool ok = false;
      int val = str.right(str.length()-2).toULong(&ok, 16);
      if(ok) reset->mInactiveValue = val;
    }
    // Otherwise it must be a decimal value
    else {
      bool ok = false;
      int val = str.toULong(&ok);
      if(ok) reset->mInactiveValue = val;
    }
    showConnection(reset);
    drawRTLPortItem(reset->getRTLPort());
  }
}

void CarbonPortEdit::changeResetClockCycles(int val) {
  if ((val == -1) || (mSelectingESL != 0)) {
    return;
  }
  CQtRecordBlocker blocker(mCQt);
  for (UInt32 i = 0; i < mCurrentResetGens.size(); ++i) {
    CarbonCfgResetGen* reset = mCurrentResetGens[i];
    reset->mFrequency.putClockCycles(val);
    showConnection(reset);
    drawRTLPortItem(reset->getRTLPort());
  }
}

void CarbonPortEdit::changeResetCompCycles(int val) {
  if ((val == -1) || (mSelectingESL != 0)) {
    return;
  }
  CQtRecordBlocker blocker(mCQt);
  for (UInt32 i = 0; i < mCurrentResetGens.size(); ++i) {
    CarbonCfgResetGen* reset = mCurrentResetGens[i];
    reset->mFrequency.putCompCycles(val);
    showConnection(reset);
    drawRTLPortItem(reset->getRTLPort());
  }
}

void CarbonPortEdit::changeResetCyclesBefore(int val) {
  if ((val == -1) || (mSelectingESL != 0)) {
    return;
  }
  CQtRecordBlocker blocker(mCQt);
  for (UInt32 i = 0; i < mCurrentResetGens.size(); ++i) {
    CarbonCfgResetGen* reset = mCurrentResetGens[i];
    reset->mCyclesBefore = val;
    showConnection(reset);
    drawRTLPortItem(reset->getRTLPort());
  }
}

void CarbonPortEdit::changeResetCyclesAsserted(int val) {
  if ((val == -1) || (mSelectingESL != 0)) {
    return;
  }
  CQtRecordBlocker blocker(mCQt);
  for (UInt32 i = 0; i < mCurrentResetGens.size(); ++i) {
    CarbonCfgResetGen* reset = mCurrentResetGens[i];
    reset->mCyclesAsserted = val;
    showConnection(reset);
    drawRTLPortItem(reset->getRTLPort());
  }
}

void CarbonPortEdit::changeResetCyclesAfter(int val) {
  if ((val == -1) || (mSelectingESL != 0)) {
    return;
  }
  CQtRecordBlocker blocker(mCQt);
  for (UInt32 i = 0; i < mCurrentResetGens.size(); ++i) {
    CarbonCfgResetGen* reset = mCurrentResetGens[i];
    reset->mCyclesAfter = val;
    showConnection(reset);
    drawRTLPortItem(reset->getRTLPort());
  }
}

void CarbonPortEdit::changeXtorClockMaster(int val) {
  CQtRecordBlocker blocker(mCQt);

  if ((val == -1) || (mSelectingESL != 0)) {
    return;
  }

  for (SInt32 i = mCurrentXtorInstances.size() - 1; i >= 0; --i) {
    CarbonCfgXtorInstance* xinst = mCurrentXtorInstances[i];

    if (val == 0) {
      // User has selected "use component clock".
      xinst->putClockMaster(NULL);
    } else {
      // user has selected the name of a transactor instance to clock the
      // current transactor.

      // get the clock master instance name
      UtString clockMasterName;
      clockMasterName << mXtorClockMaster->currentText();
      
      // find the instance and associate it with the selected xtor port
      CarbonCfgXtorInstance *clockMaster = mCfg->findXtorInstance(clockMasterName.c_str());
      if (clockMaster != NULL) {
        xinst->putClockMaster(clockMaster);
      }
    }
  }
}

void CarbonPortEdit::addPort() {
  CQtRecordBlocker blocker(mCQt);
  ++mSelectingRTL;
  mESLTree->deselectAll();
  for (UInt32 i = 0; i < mCurrentRTLPorts.size(); ++i) {
    CarbonCfgRTLPort* rtlPort = mCurrentRTLPorts[i];
    CarbonCfgESLPort* eslPort = addConnection(rtlPort);
    QTreeWidgetItem* item = showConnection(eslPort);
    mESLTree->setItemSelected(item, true);
    mESLTree->scrollToItem(item, QAbstractItemView::EnsureVisible);
    drawRTLPortItem(rtlPort);
  }
  --mSelectingRTL;
  changeESLTreeSel();
}

void CarbonPortEdit::addXtor() {
  CQtRecordBlocker blocker(mCQt);
  ++mSelectingESL;

  // Add a null xtor if there is no rtl port selected
  CarbonCfgXtorLib* xlib = mCfg->findXtorLib(CARBON_DEFAULT_XTOR_LIB);
  CarbonCfgXtor* xtor = xlib->findXtor("Null_Input");
  if (xtor != NULL) {
    addXtor(xtor);
  }
  --mSelectingESL;
  changeESLTreeSel();
}


void CarbonPortEdit::disconnectRTLPort(CarbonCfgRTLPortID rtlPort,
                                       bool removeXtorConns)
{
  for (int i = rtlPort->numConnections() - 1; i >= 0; --i) {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    if (removeXtorConns || (conn->getType() != eCarbonCfgXtorConn)) {
      removeConnection(conn, false, i);
    }
  }
  drawRTLPortItem(rtlPort);
}

void CarbonPortEdit::removeXtorInstance(CarbonCfgXtorInstance* xinst) {
  mRTLTree->deselectAll();

  // Remove the connections first.  They will be replaced with empty ones
  // in the xtor instance itself, but any connected RTL ports will get
  // disconnected properly and redrawn
  for (int i = xinst->getType()->numPorts() - 1; i >= 0; --i) {
    CarbonCfgXtorConn* conn = xinst->getConnection(i);
    removeConnection(conn, true, i);
  }

  QTreeWidgetItem* xitem = mXtorItemMap[xinst];
  INFO_ASSERT(xitem, "Cannot find item from xtor");

  // clear out all the port connections tree items -- don't just disconnect
  // them from the RTL ports
  for (UInt32 i = 0, n = xinst->getType()->numPorts(); i < n; ++i) {
    CarbonCfgRTLConnection* conn = xinst->getConnection(i);
    QTreeWidgetItem* portItem = mConnItemMap[conn];
    INFO_ASSERT(portItem, "conn item not found");
    mItemConnMap.erase(portItem);
    mConnItemMap.erase(conn);
  }
/*
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* portItem = *p;
    CarbonCfgRTLConnection* conn = mItemConnMap[portItem];
    INFO_ASSERT(conn, "port item not found");
    delete portItem;
    // the empty connections will be deleted by mCfg when we remove the
    // instance below
  }
*/

  mESLTree->deleteItem(xitem);
  mXtorItemMap.erase(xinst);
  mItemXtorMap.erase(xitem);
  mCfg->removeXtorInstance(xinst);
  mCurrentXtorInstances.remove(xinst);
} // void CarbonPortEdit::removeXtorInstance

void CarbonPortEdit::removeConnection(CarbonCfgRTLConnection* conn,
                                      bool redraw, int idx)
{
  QTreeWidgetItem* connItem = mConnItemMap[conn];
  INFO_ASSERT(connItem, "cannot find item from connection");
  CarbonCfgRTLPort* rtlPort = conn->getRTLPort();
  mConnItemMap.erase(conn);
  mItemConnMap.erase(connItem);
  mCurrentConnections.erase(conn);

  switch (conn->getType()) {
  case eCarbonCfgClockGen:
  case eCarbonCfgResetGen:
  case eCarbonCfgTieParam:
  case eCarbonCfgSystemCClock:
  case eCarbonCfgTie: {
    INFO_ASSERT(rtlPort, "no rtl port for clock/reset/tie");
    QTreeWidgetItem* portItem = mPortItemMap[rtlPort];;
    INFO_ASSERT(portItem, "Cannot find item for port");
    if (idx == -1) {
      idx = portItem->indexOfChild(connItem);
      INFO_ASSERT(idx >= 0, "Cannot find index of connection in RTLport");
    }
    else {
      INFO_ASSERT(portItem->child(idx) == connItem, "Item insanity");
    }

    if (conn->getType() == eCarbonCfgTieParam)
      removeTieParam(conn->castTieParam());

    portItem->takeChild(idx);
    delete connItem;
    mCfg->disconnect(conn);
    break;
  }
  case eCarbonCfgESLPort: {
    mESLTree->deleteItem(connItem);
    mCurrentESLPorts.remove((CarbonCfgESLPort*)conn); // order N
    mCfg->disconnect(conn);
    break;
  }
  case eCarbonCfgXtorConn: {
    CarbonCfgXtorConn* xconn = conn->castXtorConn();
    if (rtlPort != NULL) {// only makes sense to delete real connections
      CarbonCfgXtorInstance* xinst = xconn->getInstance();
      UInt32 portIndex = xconn->getPortIndex();

      // When we delete an xtor connection, it is replaced with
      // a new empty one, which needs to be re-associated with the same
      // item.
      mCfg->disconnect(conn);
      conn = xinst->getConnection(portIndex);
    }
    mCurrentXtorConns.remove(xconn); // order N
    mConnItemMap[conn] = connItem;
    mItemConnMap[connItem] = conn;
    showConnection(conn);
    break;
  }
  }
  if (redraw && rtlPort != NULL) {
    drawRTLPortItem(rtlPort);
  }
} // void CarbonPortEdit::removeConnection

void CarbonPortEdit::switchParticularRTLPortType(CarbonCfgRTLPort *rtlPort, int index) {
    disconnectRTLPort(rtlPort, true);
    CarbonCfgRTLConnectionID conn = NULL;
    UInt32 zero = 0;
    switch (index) {
    case 0: conn = addESLConnection(rtlPort, eCarbonCfgESLInput);  break;
    case 1: conn = addESLConnection(rtlPort, eCarbonCfgESLOutput); break;
    case 2: conn = carbonCfgClockGen(rtlPort, 0, 0, 1, 1, 50);     break;
    case 3: 
      // Don't allow reset ports larger than 64 bits because we can't handle it
      if(rtlPort->getWidth() <= 64) {
        // Default active value to ones for all bits
        UInt64 activeValue = (1LL<<rtlPort->getWidth())-1;
        conn = carbonCfgResetGen(rtlPort, activeValue, 0, 1, 1, 0, 1, 1);
      }
      break;
    case 4: conn = carbonCfgTie(rtlPort, &zero, 1);                break;
    case 5:    /* disconnect */                                    break;
    case 6: conn = carbonCfgTieParam(rtlPort, "", "");             break;
    }
    drawRTLPortItem(rtlPort);
    if (conn != NULL) {
      QTreeWidgetItem* item = showConnection(conn);
      item->treeWidget()->setItemSelected(item, true);
    }
    else {
      mRTLTree->setItemSelected(mPortItemMap[rtlPort], true);
    }
    mRTLTree->resizeColumnToContents(3);
}


void CarbonPortEdit::switchRTLType(int index) {
  CQtRecordBlocker blocker(mCQt);
  if (index == mCurrentRTLSwitchIndex) {
    return;
  }
  mCurrentRTLSwitchIndex = index;

  ++mSelectingRTL;
  mESLTree->deselectAll();

  for (UInt32 i = 0; i < mCurrentRTLPorts.size(); ++i) 
  {
    CarbonCfgRTLPort* rtlPort = mCurrentRTLPorts[i];
    switchParticularRTLPortType(rtlPort, index);
  } // for
  --mSelectingRTL;
  changeRTLTreeSel();
} // void CarbonPortEdit::switchRTLType

void CarbonPortEdit::switchXtorType(int index) {
  if (mCurrentXtorSwitchIndex == index) {
    return;
  }
  CQtRecordBlocker blocker(mCQt);
  mCurrentXtorSwitchIndex = index;
  ++mSelectingESL;

  // Change all selected xtor instances to the specified transactor.
  // Leave alone all the xtorInstances that are already of the desired
  // type
  CarbonCfgXtorLib* xlib = mCfg->findXtorLib(CARBON_DEFAULT_XTOR_LIB);
  CarbonCfgXtor* xtor = xlib->getXtor(index);

  UInt32 numNewXtors = 0;

  // Remove & count all selected ESL ports
  for (SInt32 i = mCurrentESLPorts.size() - 1; i >= 0; --i) {
    removeConnection(mCurrentESLPorts[i], true);
    ++numNewXtors;
  }

  // Remove & count all selected xtor instances that are not already of
  // the requested transctor type
  for (SInt32 i = mCurrentXtorInstances.size() - 1; i >= 0; --i) {
    CarbonCfgXtorInstance* xinst = mCurrentXtorInstances[i];
    if (xinst->getType() != xtor) {
      removeXtorInstance(xinst);
      ++numNewXtors;
    }
  }

  mESLTree->deselectAll();
  clearSelections();
  for (UInt32 i = 0; i < numNewXtors; ++i) {
    addXtorHelper(xtor);
  }

  --mSelectingESL;
  changeESLTreeSel();
} // void CarbonPortEdit::switchXtorType


void CarbonPortEdit::addXtor(CarbonCfgXtor* xtor) {
  ++mSelectingESL;
  mESLTree->deselectAll();
  clearSelections();
  addXtorHelper(xtor);
  --mSelectingESL;
  changeESLTreeSel();
}

void CarbonPortEdit::addXtorHelper(CarbonCfgXtor* xtor) {
  UtString buf;
  mCfg->getUniqueESLName(&buf, xtor->getName());
  CarbonCfgXtorInstance* xinst = carbonCfgAddXtor(mCfg, buf.c_str(), xtor);
  QTreeWidgetItem* xitem = showXtorInstance(xinst);
  mESLTree->setItemSelected(xitem, true);
  mESLTree->scrollToItem(xitem, QAbstractItemView::PositionAtTop);
  selectXtorInst(xinst);
}

// See if the types are compatible.  Inouts are compatible with anything.
static bool sArePortsCompatible(CarbonCfgRTLPortType x, CarbonCfgRTLPortType y)
{
  return ((x == eCarbonCfgRTLInout) || (y == eCarbonCfgRTLInout) || (x != y));
}

void CarbonPortEdit::assignByName(const char* prefix, const char* suffix) {
  ++mSelectingESL;
  CQtRecordBlocker blocker(mCQt);
  assignByNameHelper(false, prefix, suffix);
  mRTLTree->deselectAll();

  for (UInt32 i = 0; i < mCurrentXtorInstances.size(); ++i) {
    CarbonCfgXtorInstance* xinst = mCurrentXtorInstances[i];
    mESLTree->setItemSelected(mXtorItemMap[xinst], true);
  }
  --mSelectingESL;
  changeESLTreeSel();

  mCQt->setModified();
}

// The guts of assignByName are in a helper function so that
// it can be called with 'true' for a dry run, to see whether
// or not pressing it would do anything.  
bool CarbonPortEdit::assignByNameHelper(bool dry_run, const char* prefix, const char* suffix) {
  CQtRecordBlocker blocker(mCQt);
  UInt32 numAssigned = 0;


  bool hasPrefix = (prefix != NULL) ? true : false;
  bool hasSuffix = (suffix != NULL) ? true : false;

  for (UInt32 i = 0; i < mCurrentXtorInstances.size(); ++i) {
    CarbonCfgXtorInstance* xinst = mCurrentXtorInstances[i];
    CarbonCfgXtor* xtor = xinst->getType();

    // First look for exact matches, then substrings

    // Collect up substring matches, because the ahb-slave at
    // the moment has "hsel" where the model has vic.HSELBLAH.
    // but it also has partial + exact matches for ready/readyin
    // so prefer exact matches.
    UtArray<CarbonCfgRTLPort*> partialActuals;
    UtArray<UInt32> partialFormals;

    for (UInt32 portIndex = 0; portIndex < xtor->numPorts(); ++portIndex) {
      CarbonCfgXtorPort* xport = xtor->getPort(portIndex);
      CarbonCfgXtorConn* xconn = xinst->getConnection(portIndex);

      // Can the XTOR port be connected already?  No.
      if (xconn->getRTLPort() != NULL) {
        continue;
      }

      for (UInt32 i = 0, n = mCfg->numRTLPorts(); i < n; ++i) {
        CarbonCfgRTLPort* rtlPort = mCfg->getRTLPort(i);

        // The directions have to be compatible
        if (! sArePortsCompatible(rtlPort->getType(), xport->getType())) {
          continue;
        }


        // Can the RTL port be assigned already?  Or should it be
        // disconnected?  I'm going to, for the moment, allow it
        // to be auto-assigned even if it's connected, as long as
        // it's not connected to a generator.
        bool exclusive = false;
        for (UInt32 c = 0, nc = rtlPort->numConnections(); c < nc; ++c) {
          if (rtlPort->getConnection(c)->isExclusive()) {
            exclusive = true;
            break;
          }
        }
        if (exclusive) {
          continue;
        }
        
        // rtlPort->getName() will give me top.portname.  I just want
        // portname.
        const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
        if (node == NULL) {
          continue;
        }
  
        const char* rtlPortName = carbonDBNodeGetLeafName(mDB, node);
        const char* baseXtorPortName = xport->getName();
 
        UtString xtorPortName;
        if (hasPrefix)
          xtorPortName << prefix;
        xtorPortName << baseXtorPortName;
        if (hasSuffix)
          xtorPortName << suffix;

        
        if (strcasecmp(rtlPortName, xtorPortName.c_str()) == 0) {
          // We have a match.
          if (dry_run) {
            return true;
          }
          ++numAssigned;
          connectXtor(rtlPort, xinst, portIndex);
        }

        else if (strncasecmp(rtlPortName, xtorPortName.c_str(),
                             std::min(strlen(rtlPortName),
                                      xtorPortName.length())) == 0)
        {
          partialActuals.push_back(rtlPort);
          partialFormals.push_back(portIndex);
        }
      } // for
    } // for

    // If we didn't assign them all, match by substring
    for (UInt32 i = 0, n = partialActuals.size(); i < n; ++i) {
      UInt32 portIndex = partialFormals[i];
      CarbonCfgXtorConn* xconn = xinst->getConnection(portIndex);
      
      if (xconn->getRTLPort() == NULL) {
        if (dry_run) {
          return true;
        }
        ++numAssigned;
        connectXtor(partialActuals[i], xinst, portIndex);
      }
    }
  } // for

  if (dry_run) {
    return false;
  }

  return numAssigned != 0;
} // bool CarbonPortEdit::assignByNameHelper

// The ESLName widget is also used for ties and xtor instances
void CarbonPortEdit::updateESLName(QString const& qstr) {
  if (mSelectingESL != 0) {
    return;
  }
  
  UtString buf;
  buf << qstr;

  CQtRecordBlocker blocker(mCQt);
  if (mCurrentESLPorts.size() == 1) {
    CarbonCfgESLPort* eslPort = mCurrentESLPorts[0];

    // Need to be careful.  We are changing the ESL name as that's the key
    // in a couple of maps.  Let mCfg manage it
    mCfg->changeESLName(eslPort, buf.c_str());
    QTreeWidgetItem* item = mConnItemMap[eslPort];
    const char* name = eslPort->getName();
    item->setText(1, name);
    CarbonCfgRTLPort* rtlPort = eslPort->getRTLPort();
    showPortExpr(item, rtlPort->getName(), name, eslPort->getExpr());
  }
  else if (! mCurrentTies.empty()) {
    for (UInt32 i = 0; i < mCurrentTies.size(); ++i) {
      CarbonCfgTie* tie = mCurrentTies[i];
      UtIStringStream ss(buf);
      DynBitVector bv(tie->getRTLPort()->getWidth());
      if (ss >> UtIO::hex >> bv) {
        tie->mValue = bv;
        showConnection(tie);
        // no error message for now.  We could put it in the status window...
      }
    }
  }
  else if (mCurrentXtorInstances.size() == 1) {
    CarbonCfgXtorInstance* xtorInst = mCurrentXtorInstances[0];
    mCfg->changeXtorInstanceName(xtorInst, buf.c_str());
    QTreeWidgetItem* item = mXtorItemMap[xtorInst];
    item->setText(1, xtorInst->getName());

    // Update the Properties field of any connected RTL signals,
    // to reflect the new xtor namen
    CarbonCfgXtor* xtor = xtorInst->getType();
    for (UInt32 i = 0; i < xtor->numPorts(); ++i) {
      CarbonCfgXtorConn* xconn = xtorInst->getConnection(i);
      CarbonCfgRTLPort* rtlPort = xconn->getRTLPort();
      if (rtlPort != NULL) {
        (void) drawRTLPortItem(rtlPort);
      }
    }
  }
} // void CarbonPortEdit::updateESLName

// Display the value of the parameter in the ESL tree
void CarbonPortEdit::showParamValue(QTreeWidgetItem* item, CarbonCfgXtorParamInst* paramInst)
{
  const char* value = paramInst->getValue();
  if (value == NULL || 0 == strlen(value)) { // Then it will get the default value
    const char* defValue = paramInst->getDefaultValue();
    item->setText(3, tr("{%1}").arg(defValue));
  } else
    item->setText(3, value);
}

// Called whenever the value is changed in the edit box for a parameter value
void CarbonPortEdit::updateESLParamEdit(QString const& qstr) {
  QTreeItemList sel = mESLTree->selectedItems();
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;
    QVariant qv = item->data(0, Qt::UserRole);
    CarbonCfgXtorParamInst* paramInst = (CarbonCfgXtorParamInst*)qv.value<void*>();
    INFO_ASSERT(paramInst != NULL, "Missing setData for Param");
    UtString buf;
    buf << qstr;
    paramInst->putValue(buf.c_str());
    showParamValue(item, paramInst);
  }
}

void CarbonPortEdit::updateESLPortExpr(QString const& qstr) {
  UInt32 numESLExprUsers = 
    mCurrentESLPorts.size() +
    mCurrentXtorConns.size();
  if ((mSelectingESL != 0) || (numESLExprUsers != 1)) {
    return;
  }
  UtString buf;
  buf << qstr;
  CQtRecordBlocker blocker(mCQt);
  
  if (mCurrentESLPorts.size() == 1) {
    CarbonCfgESLPort* eslPort = mCurrentESLPorts[0];
    eslPort->putExpr(buf.c_str());
    showConnection(eslPort);
  }
  else {
    CarbonCfgXtorConn* xtorConn = mCurrentXtorConns[0];
    xtorConn->putExpr(buf.c_str());
    showConnection(xtorConn);
  }
}

void CarbonPortEdit::connectXtor(CarbonCfgRTLPort* rtlPort,
                                 CarbonCfgXtorInstance* xinst,
                                 UInt32 portIndex)
{
  CarbonCfgXtor* xtor = xinst->getType();
  if (portIndex < xtor->numPorts()) {
    mRTLTree->deselectAll();
    mESLTree->deselectAll();
    mPRMTree->deselectAll();

    // we already have the connection item in the xtor, and
    // it will be cleaned up in the CarbonCfg.  But we need
    // to fix the item.  We don't want to remove the item
    // from the tree widget -- we just want to redraw it
    CarbonCfgXtorConnID xconn = xinst->getConnection(portIndex);
    QTreeWidgetItem* conItem = mConnItemMap[xconn];
    INFO_ASSERT(conItem, "cannot find xtor con item");
    mConnItemMap.erase(xconn);
    mItemConnMap.erase(conItem);

    // The RTL port needs to have some of its connections
    // cleared.  For now, let's clear all the connections on it
    // except those to other xtors.  Any other connections could
    // be re-added.
    disconnectRTLPort(rtlPort, false);

    // Now we can connect the old one and show it.
    xconn = carbonCfgConnectXtor(rtlPort, xinst, portIndex);
    mConnItemMap[xconn] = conItem;
    mItemConnMap[conItem] = xconn;
    showConnection(xconn);

    // Finally, redraw the RTL port as its annotation will
    // change
    drawRTLPortItem(rtlPort);

    mESLTree->setItemSelected(conItem, true);
    mCQt->setModified();
  }
} // void CarbonPortEdit::connectXtor

void CarbonPortEdit::dropPRM(const char* rtlName, QTreeWidgetItem* item)
{
  qDebug() << rtlName << "dropped on parameter";

  if (mCfg && item)
  {
    UtString xtorName;
    xtorName << item->text(0);

    for (UInt32 i=0; i<mCfg->numParams(); i++)
    {
      CarbonCfgXtorParamInst* globParam = mCfg->getParam(i);
      if (0 == strcmp(globParam->getParam()->getName(), xtorName.c_str()))
      {
        CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(rtlName);
        CarbonCfgRTLConnectionID newConn = carbonCfgTieParam(rtlPort, "", xtorName.c_str());            
        globParam->addRTLPort(rtlPort);
        showConnection(newConn);
        drawRTLPortItem(rtlPort);
        mCQt->setModified();
        break;
      }
    }
  }
}

void CarbonPortEdit::dropRTLPort(const char* rtlPortName,
                                 QTreeWidgetItem* item)
{
  // Figure which xtor connection we are depositing to.
  ItemConnMap::iterator p = mItemConnMap.find(item);
  if (p != mItemConnMap.end()) {
    CarbonCfgRTLConnection* conn = p->second;
    CarbonCfgXtorConn* xconn = conn->castXtorConn();
    if (xconn != NULL) {
      CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(rtlPortName);
      if (rtlPort != NULL) {
        connectXtor(rtlPort, xconn->getInstance(), xconn->getPortIndex());
      }
    }
    CarbonCfgESLPort* econn = conn->castESLPort();
    if (econn != NULL) {
      QString a(rtlPortName);

      a = a.trimmed();
      for (UInt32 i=0; i<mCfg->numParams(); i++)
      {
        CarbonCfgXtorParamInst* paramInst = mCfg->getParam(i);
        QString b(paramInst->getParam()->getName());
        if (a.compare(b) == 0) {
          econn->putParamInstance(paramInst);
          mCQt->setModified();
        }
      }

      for (UInt32 j=0; j<mCfg->numXtorInstances(); j++)
      {
        CarbonCfgXtorInstance* xtorInst = mCfg->getXtorInstance(j);
        for (UInt32 k=0; k < xtorInst->numParams(); k++) {
          // handle both hidden and non-hidden parameters - We don't
          // believe the concept of hidden parameters will apply to
          // CMS/RTL use cases - it is currently only a model kit
          // feature.
          CarbonCfgXtorParamInst* paramInst = xtorInst->getParamInstance(k, true);
          QString b(paramInst->getParam()->getName());
          UtString temp;
          temp << xtorInst->getName() << " " << b;
          if (a.compare(temp.c_str()) == 0) {
            econn->putParamInstance(paramInst);
          }
        }
      }


      updateWidgets();
    }
  }
  // If a xtor connection was not found this is probably a parameter
  else if(item->parent()) {
    UtString xtorName;
    xtorName << item->parent()->text(1);
    UtString xtorParam;
    xtorParam << item->text(1); 
    CarbonCfgXtorInstance* xinst = mCfg->findXtorInstance(xtorName.c_str());
    if(xinst) {
      // Connect RTL port to the parameter
      CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(rtlPortName);
      disconnectRTLPort(rtlPort, false);
      CarbonCfgRTLConnectionID tiep = carbonCfgTieParam(rtlPort, xtorName.c_str(), xtorParam.c_str());
      showConnection(tiep);
      drawRTLPortItem(rtlPort);
      mCQt->setModified();
    }
  }
}

void CarbonPortEdit::dropXtorConn(const char* xtor_conn,
                                  QTreeWidgetItem* item)
{
  // Figure which port we are depositing to.
  if (item != NULL) 
  {
    UtIStringStream is1(xtor_conn);
    is1 >> UtIO::slashify;
    UtString xtorParam;

    ItemPortMap::iterator p = mItemPortMap.find(item);
    if (p != mItemPortMap.end()) {
      CarbonCfgRTLPort* rtlPort = p->second;
      UtIStringStream is(xtor_conn);
      is >> UtIO::slashify;
      UtString xtorName;
      UInt32 portIndex;
      if (is >> xtorName >> portIndex) {
        CarbonCfgXtorInstance* xinst =
          mCfg->findXtorInstance(xtorName.c_str());
        if (xinst != NULL) {
          connectXtor(rtlPort, xinst, portIndex);
        }
      }
      else if (is1 >> xtorName >> xtorParam)
      {
        if (mParamInstance)
        {
          if (0 == strcmp(mParamInstance->getParam()->getName(), xtorParam.c_str()))
          {
            disconnectRTLPort(rtlPort, false);
            CarbonCfgRTLConnectionID tiep = carbonCfgTieParam(rtlPort, xtorName.c_str(), xtorParam.c_str());
            showConnection(tiep);
            drawRTLPortItem(rtlPort);
            mCQt->setModified();
          }
        }
      }
      else if (mGlobParam.length() > 0)
      {
        qDebug() << xtorName.c_str() << mGlobParam.c_str();
        for (UInt32 i=0; i<mCfg->numParams(); i++)
        {
          CarbonCfgXtorParamInst* globParam = mCfg->getParam(i);
          bool hasRtlPort = globParam->findRTLPort(rtlPort->getName());

          if (!hasRtlPort && 0 == strcmp(globParam->getParam()->getName(), xtorName.c_str()))
          {
            CarbonCfgRTLConnectionID newConn = carbonCfgTieParam(rtlPort, "", xtorName.c_str());            
            globParam->addRTLPort(rtlPort);
            showConnection(newConn);
            drawRTLPortItem(rtlPort);
            mCQt->setModified();
            break;
          }
        }
      }
    }
  }
}

QGroupBox *CarbonPortEdit::frequencyGroup(const char *objName, QSpinBox *clockCycleEditor, QSpinBox *compCycleEditor) {
  QGroupBox *freqGroup = new QGroupBox("Frequency");
  QLabel *equalsLabel, *compCycleLabel;

  QString equalsLabelString(objName);
  equalsLabelString += " cycle(s) =";
  equalsLabel = new QLabel(equalsLabelString); 
  compCycleLabel = new QLabel("component cycle(s)");
  freqGroup->setLayout(CQt::hbox(clockCycleEditor, equalsLabel, compCycleEditor, compCycleLabel));
  return freqGroup;
}

QGroupBox *CarbonPortEdit::simpleGroup(const char *name, QWidget *widget) {
  QGroupBox *group = new QGroupBox(name);
  group->setLayout(CQt::hbox(widget));
  return group;
}

QWidget* CarbonPortEdit::layout() {
  
  QWidget* signal_buttons;

  if (mMode == RegTabWidget::Standalone)
  {
    signal_buttons =
      CQt::makeGroup("Signal Control",
                     CQt::hbox(mSwitchRTLType,
                               mAddPort,
                               mAddXtor,
                               mSwitchXtorType,
                               mAssignByName,
                               mESLName));
  }
  else
  {
    signal_buttons =
      CQt::makeGroup("Signal Control",
                     CQt::hbox(mSwitchRTLType,
                               mAddPort,
                               mAddXtor,
                               mSwitchXtorType,
                               mAssignByName,
                               mAdvancedBind,
                               mESLName));

  }

    signal_buttons->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

  mClockGenFrame =
    mCQt->makeGroup("Clock Control", "clock-gen",
                    CQt::hbox(frequencyGroup("clock", mClockCycles, mCompCycles),
                              simpleGroup("Duty Cycle", mDutyCycle),
                              simpleGroup("Initial Value", mClockInitialValue),
                              simpleGroup("Initial Delay", mClockDelay)));
  mClockGenFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

  mResetGenFrame =
    mCQt->makeGroup("Reset Control", "reset-gen",
                    CQt::hbox(frequencyGroup("reset", mResetClockCycles, mResetCompCycles),
                              simpleGroup("Active Value", mResetActiveValue),
                              simpleGroup("Inactive Value", mResetInactiveValue),
                              simpleGroup("Cycles Before", mResetCyclesBefore),
                              simpleGroup("Cycles Asserted", mResetCyclesAsserted),
                              simpleGroup("Cycles After", mResetCyclesAfter)));
  mResetGenFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

  mXtorClockFrame =
    mCQt->makeGroup("Clock Control", "xtor-clocks", CQt::hbox(mXtorClockMaster));
  mXtorClockFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

  mPortExprFrame =
    mCQt->makeGroup("Port Expression", "port-expr",
                     mESLPortExpr->getWidget());
  mPortExprFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    

  mParamEditFrame = 
    mCQt->makeGroup("Parameter Value", "param-edit", mParamEditor);
  mParamEditFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

  QWidget* rtl = mCQt->makeGroup("RTL Ports", "rtl-tree", mRTLTree);
  QWidget* esl = mCQt->makeGroup("ESL Ports", "esl-tree", mESLTree);

  QLayout* prmVbox = CQt::vbox();
  *prmVbox << mPRMTree;

  QWidget* prm = mCQt->makeGroup("Parameters", "prm-tree", prmVbox);

  QWidget* right = CQt::vSplit(esl, prm);

  QWidget* top = CQt::hSplit(rtl, right);
  QLayout* all = CQt::vbox();
  *all << top << signal_buttons << mClockGenFrame << mResetGenFrame << mXtorClockFrame 
       << mPortExprFrame << mParamEditFrame;

  enableWidgets();

  return CQt::makeWidget(all);
} // QWidget* CarbonPortEdit::layout
