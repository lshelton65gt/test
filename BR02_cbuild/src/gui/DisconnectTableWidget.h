// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef __DISCONNECTTABLEWIDGET_H__
#define __DISCONNECTTABLEWIDGET_H__

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"

#include "ApplicationContext.h"

#include <QObject>
#include <QTableWidget>
#include <QtGui>

class PortTableWidget;

class DisconnectTableWidget : public QTableWidget
{
   Q_OBJECT

  enum Column {colHDLNAME = 0, colWIDTH};
public:
  DisconnectTableWidget(QWidget* parent);
  virtual ~DisconnectTableWidget();


  void Populate(ApplicationContext* ctx, bool initPorts);
  void AddDisconnect(CarbonCfgRTLPort* rtlPort);
  void BeginUpdate();
  void EndUpdate();
  void SetPortWidget(PortTableWidget* portWidget) { m_pPortTable = portWidget; }

private:
  void initialize();
  void createActions();

private slots:
  void showContextMenu(const QPoint &pos);
  void remove();

private:
  CarbonDB* mDB;
  CarbonCfgID mCfg;
  ApplicationContext* mCtx;

  PortTableWidget* m_pPortTable;
  QAction *removeAction;
};

#endif

