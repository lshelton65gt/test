/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CQt.h"
#include "util/UtIOStream.h"
#include "gui/CQtProcessControl.h"
#include <QProcess>

CQtProcessControl::CQtProcessControl(QProcess* pb, CQtContext* cqt):
  mProcess(pb),
  mCQt(cqt)
{
  CQT_CONNECT(pb, started(), this, started());
  CQT_CONNECT(pb, finished(int), this, finished(int));
  mActive = false;
  mWidget = new QWidget;        // dummy
  mLastExitStatus = 0;
  mNumExits = 0;
}

void CQtProcessControl::dumpState(UtOStream& stream) {
  if ((mNumExits != 0) || mActive) {
    stream << (mActive ? "active\n" : "inactive\n");
    stream << "num exits = " << mNumExits << "\n";
    stream << "last exit status = " << mLastExitStatus << "\n";
  }
}

void CQtProcessControl::getValue(UtString* str) const {
  str->clear();
}

bool CQtProcessControl::putValue(const char*, UtString* /* errmsg */) {
  // When 'putValue' is called, that means the script expects the
  // process to have exited.  So tell CQt not to read any more events
  // from the file until the process actually does exit
  mCQt->waitForProcess(mProcess);
  return true;
}

QWidget* CQtProcessControl::getWidget() const {
  return mWidget;
}

void CQtProcessControl::finished(int exit_status) {
  mActive = false;
  mCQt->recordPutValue(mWidget, "");
  mCQt->processFinished(mProcess);
  ++mNumExits;
  mLastExitStatus = exit_status;
}

void CQtProcessControl::started() {
  mActive = true;
}
