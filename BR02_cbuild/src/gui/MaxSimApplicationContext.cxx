/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "MaxSimApplicationContext.h"
#include "mainwindow.h"

MaxSimApplicationContext::MaxSimApplicationContext(CarbonDB *db, CarbonCfgID cfg, CQtContext *cqt)
  : ApplicationContext(db, cfg, cqt)
{
  mMainWin = 0;
  mWizard = 0;

  mRegisterTableColumnEnumMap["colOFFSET"] = colOFFSET;
  mRegisterTableColumnEnumMap["colGROUP"] = colGROUP;
  mRegisterTableColumnEnumMap["colREGNAME"] = colREGNAME;
  mRegisterTableColumnEnumMap["colWIDTH"] = colWIDTH;
  mRegisterTableColumnEnumMap["colENDIAN"] = colENDIAN;
  mRegisterTableColumnEnumMap["colDESCRIPTION"] = colDESCRIPTION;
  mRegisterTableColumnEnumMap["colFIELDS"] = colFIELDS;
  mRegisterTableColumnEnumMap["colRADIX"] = colRADIX;
  mRegisterTableColumnEnumMap["colPORT"] = colPORT;
  mRegisterTableColumnEnumMap["colEND"] = colEND;

}

MaxSimApplicationContext::~MaxSimApplicationContext()
{
}

void MaxSimApplicationContext::setDocumentModified(bool value)
{
  mMainWin->setWindowModified(value);
  if (mWizard)
    mWizard->setWindowModified(value);
}

MaxSimApplicationContext *MaxSimApplicationContext::castMaxSim()
{
  return this;
}
