/******************************************************************************
 Copyright (c) 2006-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "util/CarbonPlatform.h"  //new

#include "cmm.h"  //new
#include "CarbonMakerContext.h"  //new
#include "CarbonProjectWidget.h"  //new

#include "BrowserTreeWidget.h"
#include "gui/CQt.h"
#include <QFile>
#include "DirectivesEditor.h"
#include "DesignXML.h"

QString line;

BrowserTreeWidget::BrowserTreeWidget(QWidget *parent)
  : CDragDropTreeWidget(false, parent)
{
  mCtx = 0;

  ui.setupUi(this);
  clearAll();
  
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  mHierMode = eCarbonDBHierarchy;

  mDesignHierarchy = NULL;
  mToplevelInterface = NULL;
  mInstance = NULL;
  mContextMenuTopLevelInterface = NULL;
  mContextMenuInstance = NULL;

  mProjectWidget = pw;

  mActionNavigateSource = new QAction(this);
  mActionNavigateSource->setText("Visit Module");
  mActionNavigateSource->setStatusTip("Navigate to the RTL source");

  mActionNavigateInstance = new QAction(this);
  mActionNavigateInstance->setText("Visit Instance");
  mActionNavigateInstance->setStatusTip("Navigate to the instance");

  mActionNavigateArchitecture = new QAction(this);
  mActionNavigateArchitecture->setText("Visit Architecture");
  mActionNavigateArchitecture->setStatusTip("Navigate to the architecture");

  mActionObserveInstance = new QAction(this);
  mActionObserveInstance->setText("Instance");
  mActionObserveInstance->setStatusTip("Navigate to the RTL source");

  mActionObserveModule = new QAction(this);
  mActionObserveModule->setText("Module");
  mActionObserveModule->setStatusTip("Navigate to the RTL source");

  mActionDepositInstance = new QAction(this);
  mActionDepositInstance->setText("Instance");
  mActionDepositInstance->setStatusTip("Navigate to the RTL source");

  mActionDepositModule = new QAction(this);
  mActionDepositModule->setText("Module");
  mActionDepositModule->setStatusTip("Navigate to the RTL source");

  CQT_CONNECT(this, itemSelectionChanged(), this, itemSelectionChanged());
  CQT_CONNECT(this, itemDoubleClicked(QTreeWidgetItem*, int), this, itemDoubleClicked(QTreeWidgetItem*, int));
  CQT_CONNECT(this, customContextMenuRequested(QPoint), this, showContextMenu(QPoint));
  CQT_CONNECT(mActionNavigateSource, triggered(), this, navigateModuleSource());
  CQT_CONNECT(mActionNavigateInstance, triggered(), this, navigateInstanceSource());
  CQT_CONNECT(mActionNavigateArchitecture, triggered(), this, navigateArchitecture());
  CQT_CONNECT(mActionObserveInstance, triggered(), this, observeInstance());
  CQT_CONNECT(mActionObserveModule, triggered(), this, observeModule());
  CQT_CONNECT(mActionDepositInstance, triggered(), this, depositInstance());
  CQT_CONNECT(mActionDepositModule, triggered(), this, depositModule());
}

void BrowserTreeWidget::navigateInstanceSource()
{  
  QRegExp sourceLocator("(.*):([0-9]+)");
  if (mContextMenuInstance && sourceLocator.exactMatch(mContextMenuInstance->getLocator()))
  {
    QString src = sourceLocator.cap(1);
    UtString source;
    source << src;
    QString sLineNumber = sourceLocator.cap(2);
    QVariant iLineNumber(sLineNumber);
    mProjectWidget->navigateSource(source.c_str(), iLineNumber.toInt());

  }
  else if (mContextMenuTopLevelInterface && sourceLocator.exactMatch(mContextMenuTopLevelInterface->getLocator()))
  {
    QString src = sourceLocator.cap(1);
    UtString source;
    source << src;
    QString sLineNumber = sourceLocator.cap(2);
    QVariant iLineNumber(sLineNumber);
    mProjectWidget->navigateSource(source.c_str(), iLineNumber.toInt());
  }
}

void BrowserTreeWidget::navigateModuleSource()
{  
  QRegExp sourceLocator("(.*):([0-9]+)");
  if (mContextMenuInstance && sourceLocator.exactMatch(mContextMenuInstance->getInterface()->getStart()))
  {
    QString src = sourceLocator.cap(1);
    UtString source;
    source << src;
    QString sLineNumber = sourceLocator.cap(2);
    QVariant iLineNumber(sLineNumber);
    mProjectWidget->navigateSource(source.c_str(), iLineNumber.toInt());

  }
  else if (mContextMenuTopLevelInterface && sourceLocator.exactMatch(mContextMenuTopLevelInterface->getLocator()))
  {
    QString src = sourceLocator.cap(1);
    UtString source;
    source << src;
    QString sLineNumber = sourceLocator.cap(2);
    QVariant iLineNumber(sLineNumber);
    mProjectWidget->navigateSource(source.c_str(), iLineNumber.toInt());
  }
}

void BrowserTreeWidget::navigateArchitecture()
{  
  QRegExp sourceLocator("(.*):([0-9]+)");
  XInterfaceArchitecture* arch = NULL;
  if(mContextMenuInstance && !(mContextMenuInstance->getArchitectureName().isEmpty()))
    arch = mContextMenuInstance->getInterface()->findArchitecture(mContextMenuInstance->getArchitectureName());
  else if (mContextMenuInstance && (mContextMenuInstance->getInterface()->getArchitectures()->numArchitectures() > 0))
    arch = mContextMenuInstance->getInterface()->getArchitecture();
  if (mContextMenuInstance && sourceLocator.exactMatch(arch->getStart()))
  {
    QString src = sourceLocator.cap(1);
    UtString source;
    source << src;
    QString sLineNumber = sourceLocator.cap(2);
    QVariant iLineNumber(sLineNumber);
    mProjectWidget->navigateSource(source.c_str(), iLineNumber.toInt());

  }
  else if (mContextMenuTopLevelInterface && sourceLocator.exactMatch(mContextMenuTopLevelInterface->getLocator()))
  {
    QString src = sourceLocator.cap(1);
    UtString source;
    source << src;
    QString sLineNumber = sourceLocator.cap(2);
    QVariant iLineNumber(sLineNumber);
    mProjectWidget->navigateSource(source.c_str(), iLineNumber.toInt());
  }
}

void BrowserTreeWidget::observeInstance()
{
  DirectivesEditor* dirEditor = mProjectWidget->compilerDirectives();
  if (dirEditor)
  {
    TabModuleDirectives* moduleDirectives = dirEditor->getModuleDirectivesTab();
    UtString s;
    s << mDragText << ".*";
    moduleDirectives->addOrChange(s.c_str(), "observeSignal", NULL);
  }
}

void BrowserTreeWidget::observeModule()
{
  DirectivesEditor* dirEditor = mProjectWidget->compilerDirectives();
  if (dirEditor && mContextMenuInstance)
  {
    TabModuleDirectives* moduleDirectives = dirEditor->getModuleDirectivesTab();
    UtString moduleName;
    moduleName << mContextMenuInstance->getInterface()->getName() << ".*";   
    moduleDirectives->addOrChange(moduleName.c_str(), "observeSignal", NULL);
  }
}

void BrowserTreeWidget::depositInstance()
{
  DirectivesEditor* dirEditor = mProjectWidget->compilerDirectives();
  if (dirEditor)
  {
    TabModuleDirectives* moduleDirectives = dirEditor->getModuleDirectivesTab();
    UtString s;
    s << mDragText << ".*";
    moduleDirectives->addOrChange(s.c_str(), "depositSignal", NULL);
  }
}

void BrowserTreeWidget::depositModule()
{
  DirectivesEditor* dirEditor = mProjectWidget->compilerDirectives();
  if (dirEditor && mContextMenuInstance)
  {
    TabModuleDirectives* moduleDirectives = dirEditor->getModuleDirectivesTab();
    UtString moduleName;
    moduleName << mContextMenuInstance->getInterface()->getName() << ".*";   
    moduleDirectives->addOrChange(moduleName.c_str(), "depositSignal", NULL);
  }
}

BrowserTreeWidget::~BrowserTreeWidget()
{
}

void BrowserTreeWidget::showContextMenu(QPoint pos)
{
  mContextMenuTopLevelInterface = NULL;
  mContextMenuInstance = NULL;

  QTreeWidgetItem* item = itemAt(pos);
  if (item)
  {
    QVariant qv = item->data(0, Qt::UserRole);
    QVariant qv1 = item->data(1, Qt::UserRole);
    if (!qv.isNull())
    {
      mContextMenuTopLevelInterface = (XToplevelInterface*)qv.value<void*>();
      mContextMenuInstance = (XInstance*)qv1.value<void*>();
      QMenu menu;
      observe = new QMenu(this);
      observe->setTitle("Observe");
      observe->addAction(mActionObserveInstance);
      observe->addAction(mActionObserveModule);
      deposit = new QMenu(this);
      deposit->setTitle("Deposit");
      deposit->addAction(mActionDepositInstance);
      deposit->addAction(mActionDepositModule);
      menu.addMenu(observe);
      menu.addMenu(deposit);
      menu.addAction(mActionNavigateSource);
      menu.addAction(mActionNavigateInstance);
      if(mContextMenuInstance && (!(mContextMenuInstance->getArchitectureName().isEmpty()) || (mContextMenuInstance->getInterface()->getArchitectures()->numArchitectures() > 0)))
        menu.addAction(mActionNavigateArchitecture);
      menu.exec(viewport()->mapToGlobal(pos));
    }
  }
}

void BrowserTreeWidget::itemDoubleClicked(QTreeWidgetItem* item, int /*col*/)
{
  QVariant qv = item->data(0, Qt::UserRole);
  QVariant qv1 = item->data(1, Qt::UserRole);
  if (!qv.isNull())
  {
    XToplevelInterface* topi = (XToplevelInterface*)qv.value<void*>();
    XInstance* inst = (XInstance*)qv1.value<void*>();

    UtString fullName;
    if (inst)
      fullName << inst->getFullName();
    else
      fullName << topi->getName();

    UtString module, moduleLocator;
    if (inst)
    {
      module << inst->getID();
      moduleLocator << inst->getLocator();
    }

    if (module.length() > 0)
      emit nodeDoubleClicked(fullName.c_str(), module.c_str(), moduleLocator.c_str());
    else
      emit nodeDoubleClicked(fullName.c_str(), NULL, NULL);
  }
}

void BrowserTreeWidget::selectModule(QTreeWidgetItem* parent, const char* moduleName, QStringList& moduleList)
{
  for (int i=0; i<parent->childCount(); i++)
  {
    QTreeWidgetItem* item = parent->child(i);

    QVariant qv = item->data(0, Qt::UserRole);
    XToplevelInterface* topi = (XToplevelInterface*)qv.value<void*>();

    QVariant qv1 = item->data(1, Qt::UserRole);
    XInstance* inst = (XInstance*)qv1.value<void*>();

    if (topi || inst)
    {
      UtString text;
      if (inst)
        text << inst->getID();
      else
        text << topi->getName();

      if (0 == strcmp(moduleName, text.c_str()))
      {
        if (!moduleList.isEmpty())
        {
          UtString next;
          next << moduleList.takeFirst();
          selectModule(item, next.c_str(), moduleList);
        }
        else
        {
          item->setSelected(true);
          scrollToItem(item, QAbstractItemView::PositionAtCenter);
        }
      }
    }
  }
}

void BrowserTreeWidget::expandModules(QStringList& moduleList)
{
  clearSelection();

  QString topModule = moduleList.takeFirst();
  UtString top;
  top << topModule;

  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);

    QVariant qv = item->data(0, Qt::UserRole);
    XToplevelInterface* topi = (XToplevelInterface*)qv.value<void*>();

    QVariant qv1 = item->data(1, Qt::UserRole);
    XInstance* inst = (XInstance*)qv1.value<void*>();

    if (topi || inst)
    {
      UtString text;
      if (inst)
        text << inst->getID();
      else
        text << topi->getName();
      if (0 == strcmp(top.c_str(), text.c_str()))
      {
        if (moduleList.count() > 0)
        {
          UtString next;
          next << moduleList.takeFirst();
          selectModule(item, next.c_str(), moduleList);
          break;
        }
        else
        {
          item->setSelected(true);
        }
      }
    }
  }
}


void BrowserTreeWidget::selectModule(const char* moduleName)
{
  clearSelection();
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    QVariant qv = item->data(0, Qt::UserRole);
    if (!qv.isNull())
    {
      XInstance* inst = (XInstance*)qv.value<void*>();
      UtString text;
      text << inst->getFullName();
      if (text == moduleName)
        item->setSelected(true);
    }
  }
}

void BrowserTreeWidget::putContext(ApplicationContext* ctx, bool loadHierarchy)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  mCtx = ctx;

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  // Free old one
  if (mDesignHierarchy)
  {
    delete mDesignHierarchy;
    mDesignHierarchy = NULL;
    clearAll();
  }

  if (proj && proj->getActive() && loadHierarchy)
  {
    const char* designHierarchyFile = proj->getDesignFilePath(".designHierarchy");
    mDesignHierarchy = new XDesignHierarchy(designHierarchyFile);
    XmlErrorHandler* eh = mDesignHierarchy->getErrorHandler();      
    if (eh->hasErrors())
    {
      QString msg = QString("Error reading design hierarchy: %1\n%2")
        .arg(designHierarchyFile)
        .arg(eh->errorMessages().join("\n"));
    }
    else
      Initialize(mHierMode);
  }
  QApplication::restoreOverrideCursor();
}

void BrowserTreeWidget::Initialize(HierarchyMode mode)
{
  mHierMode = mode;

  clearAll();

  QApplication::setOverrideCursor(Qt::WaitCursor);
 
  mDB = mCtx->getDB();
  mCfg = mCtx->getCfg();

  if (mDesignHierarchy)
  {
    XTopLevelInterfaces* topInterfaces = mDesignHierarchy->getToplevelInterfaces();   
    
    for (int i=0; i<topInterfaces->numInterfaces(); i++)
    {
      XToplevelInterface* topi = topInterfaces->getAt(i);
      QTreeWidgetItem* item = new QTreeWidgetItem(this);
      item->setText(0, topi->getName());
      item->setExpanded(true);
      QVariant qv = qVariantFromValue((void*)topi);
      item->setData(0, Qt::UserRole, qv);
      QList<XInstance*>* instances = topi->getChildren();
      foreach (XInstance* inst, *instances)
      {
        addInstance(item, topi, inst);
      }
    }

    XLibraries* libraries = mDesignHierarchy->getLibraries();
    QTreeWidgetItem* item = new QTreeWidgetItem(this);
    item->setText(0, "Libraries");
    item->setExpanded(true);

    QMap<QString, QTreeWidgetItem*> libMap;

    foreach (XLibrary* lib, libraries->getLibraries())
    {
      QVariant qv = qVariantFromValue((void*)libraries);
      QVariant qv1 = qVariantFromValue((void*)lib);

      QTreeWidgetItem* item2 = new QTreeWidgetItem(item);

      if (lib->getDefault())
      {
        QFont font = item2->font(0);
        font.setBold(true);
        item2->setFont(0, font);  
      }

      libMap[lib->getName()] = item2;

      item2->setText(0, lib->getName());
      item2->setData(2, Qt::UserRole, qv);
      item2->setData(3, Qt::UserRole, qv1);
      //item2->setExpanded(true);
      item2->setText(1, lib->getPath());
      item2->setToolTip(1, lib->getPath());
    }

    // Walk all interfaces, and add
    // child node to corresponding library.

    topInterfaces = mDesignHierarchy->getToplevelInterfaces();   
    
    for (int i=0; i<topInterfaces->numInterfaces(); i++)
    {
      XToplevelInterface* topi = topInterfaces->getAt(i);

      QString name;
      name = topi->getLibrary();
      if (name != "")
      {
        QTreeWidgetItem* item = new QTreeWidgetItem(libMap[name]);
        item->setText(0, topi->getName());
        item->setExpanded(true);
        //QVariant qv = qVariantFromValue((void*)topi);
        //item->setData(0, Qt::UserRole, qv);
      }
      QList<XInstance*>* instances = topi->getChildren();
      foreach (XInstance* inst, *instances)
      {
        popLibrary(libMap, topi, inst);
      }
    }
  }

  header()->setResizeMode(0, QHeaderView::Interactive);
  header()->setStretchLastSection(true);

  header()->resizeSections(QHeaderView::ResizeToContents);

  QApplication::restoreOverrideCursor();

  show();
}

void BrowserTreeWidget::popLibrary(QMap<QString, QTreeWidgetItem*> libMap, XToplevelInterface* topi, XInstance* inst)
{
  QVariant qv = qVariantFromValue((void*)topi);
  QVariant qv1 = qVariantFromValue((void*)inst);

  QString name;
  if (inst->getInterface() == NULL)
    return;

  name = inst->getInterface()->getLibrary();
  if (name != "")
  {
    QTreeWidgetItem* item = new QTreeWidgetItem(libMap[name]);
 
    UtString name;
    if(inst && !(inst->getArchitectureName().isEmpty()))
      name << inst->getID() << " (" << inst->getInterface()->getName() << ":" << inst->getArchitectureName() << ")";
    else if (inst && (inst->getInterface()->getArchitectures()->numArchitectures() > 0))
      name << inst->getID() << " (" << inst->getInterface()->getName() << ":" << inst->getInterface()->getArchitecture()->getName() << ")";
    else
      name << inst->getID() << " (" << inst->getInterface()->getName() << ")";

    item->setText(0, name.c_str());
    item->setData(0, Qt::UserRole, qv);
    item->setData(1, Qt::UserRole, qv1);
    item->setExpanded(true);

    item->setText(1, inst->getLocator());
  }
  QList<XInstance*>* instances = inst->getChildren();
  foreach (XInstance* childInst, *instances)
    popLibrary(libMap, topi, childInst);
}

void BrowserTreeWidget::clearAll()
{
  mDB = 0;
  mCfg = 0;
  clear();
}

void BrowserTreeWidget::addInstance(QTreeWidgetItem* parent, XToplevelInterface* topi, XInstance* inst) //new
{
  QVariant qv = qVariantFromValue((void*)topi);
  QVariant qv1 = qVariantFromValue((void*)inst);

  QTreeWidgetItem* item = new QTreeWidgetItem(parent);

  UtString name;
  
  if (inst && inst->getInterface() == NULL)
  {
    // Tell the user if this is a VHDL process or generate label 
    if (inst->getInterfaceType() == NULL)
      name << inst->getID() << " (hidden)";
    else if (inst->isVlogGenerateLabel())
      name << inst->getID() << " (generate)";
    else
      name << inst->getID() << " (" << inst->getInterfaceType() << ")";
  }
  else if(inst && !(inst->getArchitectureName().isEmpty()))
    name << inst->getID() << " (" << inst->getInterface()->getName() << ":" << inst->getArchitectureName() << ")";
  else if (inst && (inst->getInterface()->getArchitectures()->numArchitectures() > 0))
    name << inst->getID() << " (" << inst->getInterface()->getName() << ":" << inst->getInterface()->getArchitecture()->getName() << ")";
  else
    name << inst->getID() << " (" << inst->getInterface()->getName() << ")";

  item->setText(0, name.c_str());
  item->setData(0, Qt::UserRole, qv);
  item->setData(1, Qt::UserRole, qv1);
  //item->setExpanded(true);

  item->setText(1, inst->getLocator());
  item->setToolTip(1, inst->getLocator());

  QList<XInstance*>* instances = inst->getChildren();
  foreach (XInstance* childInst, *instances)
    addInstance(item, topi, childInst);
}

void BrowserTreeWidget::buildSelectionList(QList<QTreeWidgetItem*>& list)
{
  // The list gives items for every column and row, we just want it row
  // oriented.

  list.clear();

  foreach (QTreeWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(0, Qt::UserRole);
    if (!qv.isNull())
    {
      //CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();
      XInstance* inst = (XInstance*)qv.value<void*>();
      if (inst != NULL)
      {
        list.append(item);
      }
    }
  }  
}

void BrowserTreeWidget::itemSelectionChanged()
{
  QList<QTreeWidgetItem*> itemList;
  buildSelectionList(itemList);
  mDragText.clear();

  foreach (QTreeWidgetItem *item, itemList)
  {
    QVariant qv = item->data(0, Qt::UserRole);
    QVariant qv1 = item->data(1, Qt::UserRole);
    QVariant qv2 = item->data(2, Qt::UserRole);
    QVariant qv3 = item->data(3, Qt::UserRole);

    XToplevelInterface* topi = (XToplevelInterface*)qv.value<void*>();
    XInstance* inst = (XInstance*)qv1.value<void*>();

    XLibraries* libs = (XLibraries*)qv2.value<void*>();
    XLibrary* lib = (XLibrary*)qv3.value<void*>();

    if (!mDragText.empty())
      mDragText << "\n";

    UtString fullName;
    if (inst)
      fullName << inst->getFullName();
    else if (topi)
      fullName << topi->getName();  
    else // Library node?
    {
      if (lib)  
      {
        qDebug() << "handle library here" << libs;
      }
    }
    mDragText << fullName;

    // The node might be null because the compiler won't compile certain
    // constructs, in that case, we don't need to broadcast anything.
    const CarbonDBNode* node = carbonDBFindNode(mDB, fullName.c_str());

    if (node == NULL)
      qDebug() << "Error: Unable to locate" << fullName.c_str();

    if (mDragText.length() > 0 && !signalsBlocked())
      emit nodeSelectionChanged((CarbonDBNode*)node, topi, inst);
  }
}
