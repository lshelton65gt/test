// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef DIALOGBINDCONSTANT_H
#define DIALOGBINDCONSTANT_H

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "cfg/CarbonCfg.h"

#include "gui/CDialog.h"
#include "ui_DialogBindConstant.h"

class DialogBindConstant : public CDialog
{
  Q_OBJECT

public:
  DialogBindConstant(QWidget *parent = 0);
  ~DialogBindConstant();
  void setup(CarbonDB* db, CarbonCfgRegister* reg, CarbonCfgRegisterField* field);
  CarbonUInt64 getValue() const {return mValue;}

private:
  Ui::DialogBindConstantClass ui;
  CarbonDB* mDB;
  CarbonCfgRegister* mReg;
  CarbonCfgRegisterField* mField;

  CarbonUInt64 mValue;

protected:
  // reimplemented function from CDialog
  virtual void setData(const UtString &data);

private slots:
  void on_pushButtonCancel_clicked();
  void on_pushButtonOK_clicked();
};

#endif // DIALOGBINDCONSTANT_H
