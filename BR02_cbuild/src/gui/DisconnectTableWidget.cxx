/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "util/CarbonPlatform.h"

#include <QFileDialog>
#include <QtDebug>
#include <QtGui>
#include <QLabel>
#include <QStringList>

#include "car2cow.h"
#include "PortView.h"
#include "DisconnectTableWidget.h"


#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "shell/carbon_capi.h"
#include "cfg/CarbonCfg.h"

DisconnectTableWidget::DisconnectTableWidget(QWidget* parent) : QTableWidget(parent)
{
  mDB = NULL;
  mCfg = NULL;
  m_pPortTable = NULL;

  initialize();

  connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(QPoint)));

  createActions();
}

DisconnectTableWidget::~DisconnectTableWidget()
{


}

// Remove
void DisconnectTableWidget::remove()
{
  QList<QTableWidgetItem*> removeItems;

  // The list gives items for every column and row, we just want it row
  // oriented.
  foreach (QTableWidgetItem *item, selectedItems())
  {
    if (item->column() == 0)
    {
      removeItems.append(item);
    }
  }

  // Now walk the list of items to be deleted
  foreach (QTableWidgetItem *item, removeItems)
  {
    QVariant qv = item->data(Qt::UserRole);
//    qDebug() << "Removing: " << item->text();

    CarbonCfgRTLPort* rtlPort = (CarbonCfgRTLPort*)qv.value<void*>();

    // Remove it from the Table
    removeRow(item->row());

    // Put it back as an ESL Port
    if (m_pPortTable != NULL)
      m_pPortTable->CreateESLPort(rtlPort);
  }
}

void DisconnectTableWidget::Populate(ApplicationContext* ctx, bool initPorts)
{
  mCtx = ctx;
  mDB = ctx->getDB();
  mCfg = ctx->getCfg();

  // We are not not reading from a .ccfg file if initPorts=true
  if (initPorts)
  {
    // Do Nothing special here, we can't tell what needs to be tied off
  }
}

void DisconnectTableWidget::AddDisconnect(CarbonCfgRTLPort* rtlPort)
{
  bool updates = updatesEnabled();
  bool sorting = isSortingEnabled();

  setUpdatesEnabled(false);
  setSortingEnabled(false);

  int row = rowCount();
  // Bump the RowCount first, otherwise the cells will look empty
  setRowCount(row+1);

  // First Item; HDLName
  QTableWidgetItem *itemName = new QTableWidgetItem;
  itemName->setText(rtlPort->getName());
  setItem(row, colHDLNAME, itemName);

  QVariant qv = qVariantFromValue((void*)rtlPort);

  itemName->setData(Qt::UserRole, qv);

  // Second Item: Width
  UtString buf;
  buf << rtlPort->getWidth();
  QTableWidgetItem *itemWidth = new QTableWidgetItem;
  itemWidth->setText(buf.c_str());
  setItem(row, colWIDTH, itemWidth);

  setUpdatesEnabled(updates);
  setSortingEnabled(sorting);
}

void DisconnectTableWidget::initialize()
{
  QStringList labels;
  labels << "HDL Port" << "Width";

  setColumnCount(labels.count());
  setHorizontalHeaderLabels(labels);
  horizontalHeader()->setResizeMode(0, QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(columnCount()-1, QHeaderView::Stretch);

  verticalHeader()->setVisible(false);

}

void DisconnectTableWidget::BeginUpdate()
{
  setRowCount(0);
  setUpdatesEnabled(false);
  setSortingEnabled(false);

}

void DisconnectTableWidget::EndUpdate()
{
  horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(columnCount()-1, QHeaderView::Stretch);

  setUpdatesEnabled(true);
  setSortingEnabled(true);
}

void DisconnectTableWidget::showContextMenu(const QPoint &pos)
{
  bool enableAction = true; 
  QTableWidgetItem *item = itemAt(pos);
  if (item == NULL)
    return;

  QVariant qv = item->data(Qt::UserRole);
  CarbonCfgRTLPort* rtlPort = (CarbonCfgRTLPort*)qv.value<void*>();
  if (rtlPort)
  {
    const CarbonDBNode* portNode = carbonDBFindNode(mDB, rtlPort->getName());
    if (portNode && carbonDBIsTied(mDB, portNode))
      enableAction = false;

    QMenu menu(this);
    
    removeAction->setEnabled(enableAction);

    menu.addAction(removeAction);
    menu.exec(viewport()->mapToGlobal(pos));
  }
}

void DisconnectTableWidget::createActions()
{
  removeAction = new QAction(tr("Remove"), this);
  connect(removeAction, SIGNAL(triggered()), this, SLOT(remove()));
}
