<!doctype HTML public "-//W3C//DTD HTML 4.0 Frameset//EN">

<!-- saved from url=(0014)about:internet -->
<html>

<head>
<title>Runtime Control window</title>
<meta http-equiv="content-type" content="text/html; charset=windows-1252">
<meta name="generator" content="Adobe RoboHelp - www.adobe.com">
<link rel="stylesheet" href="../../default_ns.css"><script type="text/javascript" language="JavaScript" title="WebHelpSplitCss">
<!--
if (navigator.appName !="Netscape")
{   document.write("<link rel='stylesheet' href='../../default.css'>");}
//-->
</script>
<style type="text/css">
<!--
p.whs1 { text-align:center; }
img_whs2 { border:none; margin-top:7px; margin-bottom:6px; float:none; width:540px; height:477px; border-style:none; }
ul.whs3 { list-style:disc; }
-->
</style><script type="text/javascript" language="JavaScript" title="WebHelpInlineScript">
<!--
function reDo() {
  if (innerWidth != origWidth || innerHeight != origHeight)
     location.reload();
}
if ((parseInt(navigator.appVersion) == 4) && (navigator.appName == "Netscape")) {
	origWidth = innerWidth;
	origHeight = innerHeight;
	onresize = reDo;
}
onerror = null; 
//-->
</script>
<style type="text/css">
<!--
div.WebHelpPopupMenu { position:absolute; left:0px; top:0px; z-index:4; visibility:hidden; }
-->
</style><script type="text/javascript" language="javascript1.2" src="../../whmsg.js"></script>
<script type="text/javascript" language="javascript" src="../../whver.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whproxy.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whutils.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whtopic.js"></script>
</head>
<body><script type="text/javascript" language="javascript1.2">
<!--
if (window.gbWhTopic)
{
	if (window.addTocInfo)
	{
	addButton("show",BTN_TEXT,"Show","","","","",0,0,"","","");

	}
	if (window.writeBtnStyle)
		writeBtnStyle();

	if (window.writeIntopicBar)
		writeIntopicBar(1);

	if (window.setRelStartPage)
	{
	setRelStartPage("../../cms.htm");

		autoSync(1);
		sendSyncInfo();
		sendAveInfoOut();
	}
}
else
	if (window.gbIE4)
		document.location.reload();
//-->
</script>
<h1>Runtime Control window</h1>

<p>The <span style="font-style: italic;"><I>Runtime Control</I></span> window 
 enables you to select and view the performance of instantiated models 
 (components) when using Carbon <span style="font-style: italic;"><I><a href="../replay/replay_overview.htm">Replay</a></I></span> 
 and Carbon <span style="font-style: italic;"><I><a href="../ondemand/ondemand_overview.htm">OnDemand</a></I></span>. 
 This window also enables you to configure and tune <span style="font-style: italic;"><I>Replay</I></span> 
 and <span style="font-style: italic;"><I>OnDemand</I></span>. The data in this 
 window is contained in the Carbon Simulation State (<span style="font-style: italic;"><I>.css</I></span>) 
 file.</p>

<p align="center" class="whs1"><script type="text/javascript" language="JavaScript"><!--
if ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) <= 4)) 
{  if (parseInt(navigator.appVersion) != 2) document.write("<img src='../../screenshots/runtime_control_tab.png' x-maintain-ratio='TRUE' width='540px' height='477px' border='0' class='img_whs2'>");}
else
{   document.write("<img src='../../screenshots/runtime_control_tab.png' x-maintain-ratio='TRUE' style='margin-top:7px;margin-bottom:6px;' width='540px' height='477px' border='0' class='img_whs2'>");}
//--></script><noscript><img src='../../screenshots/runtime_control_tab.png' x-maintain-ratio='TRUE' style='margin-top:7px;margin-bottom:6px;' width='540px' height='477px' border='0' class='img_whs2'></noscript></p>

<p>Use the <span style="font-style: italic;"><I>Metrics menu</I></span> to select 
 the desired metric to be displayed in the X axis:</p>

<ul type="disc" class="whs3">
	
	<li class=kadov-p><p><span style="font-weight: bold;"><B>CPU Seconds</B></span>. 
 The CPU process usage, in seconds, for both the user and the operating 
 system.</p></li>
	
	<li class=kadov-p><p><span style="font-weight: bold;"><B>Real Time</B></span>. 
 The number of seconds between the beginning and completion of the simulation.</p></li>
	
	<li class=kadov-p><p><span style="font-weight: bold;"><B>Cycles</B></span>. 
 (OnDemand only) The number of simulation cycles completed.</p></li>
	
	<li class=kadov-p><p><span style="font-weight: bold;"><B>Sim Time</B></span>. 
 (Replay only) The actual time spent in the simulation.</p></li>
	
	<li class=kadov-p><p><span style="font-weight: bold;"><B>Aggregate Schedule 
 Calls</B></span>. The total number of times that <span class=Courier>carbonSchedule</span> 
 is called (typically one or more per cycle).</p></li>
</ul>

<p>Configuration of the OnDemand and Replay settings for each component 
 is done through the <span style="font-style: italic;"><I><a href="../ondemand/ondemand_properties_window.htm">OnDemand 
 Properties </I></span>window</a> and the <span style="font-style: italic;"><I><a href="../replay/replay_runtime_properties.htm">Replay Properties</I></span> 
 window</a>.</p>

<script type="text/javascript" language="javascript1.2">
<!--
if (window.writeIntopicBar)
	writeIntopicBar(0);
//-->
</script>
</body>
</html>
