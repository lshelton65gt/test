<!doctype HTML public "-//W3C//DTD HTML 4.0 Frameset//EN">

<!-- saved from url=(0014)about:internet -->
<html>

<head>
<title>Using the Configuration Manager</title>
<meta http-equiv="content-type" content="text/html; charset=windows-1252">
<meta name="generator" content="Adobe RoboHelp - www.adobe.com">
<link rel="stylesheet" href="../../default_ns.css"><script type="text/javascript" language="JavaScript" title="WebHelpSplitCss">
<!--
if (navigator.appName !="Netscape")
{   document.write("<link rel='stylesheet' href='../../default.css'>");}
//-->
</script>
<style type="text/css">
<!--
p.whs1 { text-align:center; }
img_whs2 { border:none; margin-top:7px; margin-bottom:6px; float:none; border-style:none; width:305px; height:59px; }
img_whs3 { border:none; width:24px; height:17px; float:none; border-style:none; }
ol.whs4 { list-style:decimal; }
img_whs5 { border:none; margin-top:7px; margin-bottom:6px; float:none; border-style:none; width:308px; height:58px; }
h2.whs6 { margin-bottom:9.5pt; }
img_whs7 { border:none; width:26px; height:23px; float:none; border-style:none; }
p.whs8 { margin-bottom:10.5pt; }
p.whs9 { margin-top:9pt; }
-->
</style><script type="text/javascript" language="JavaScript" title="WebHelpInlineScript">
<!--
function reDo() {
  if (innerWidth != origWidth || innerHeight != origHeight)
     location.reload();
}
if ((parseInt(navigator.appVersion) == 4) && (navigator.appName == "Netscape")) {
	origWidth = innerWidth;
	origHeight = innerHeight;
	onresize = reDo;
}
onerror = null; 
//-->
</script>
<style type="text/css">
<!--
div.WebHelpPopupMenu { position:absolute; left:0px; top:0px; z-index:4; visibility:hidden; }
-->
</style><script type="text/javascript" language="javascript1.2" src="../../whmsg.js"></script>
<script type="text/javascript" language="javascript" src="../../whver.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whproxy.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whutils.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whtopic.js"></script>
</head>
<body><script type="text/javascript" language="javascript1.2">
<!--
if (window.gbWhTopic)
{
	if (window.addTocInfo)
	{
	addTocInfo("Creating a Carbon Model\nSetting Compiler Properties\nUsing the Configuration Manager");
addButton("show",BTN_TEXT,"Show","","","","",0,0,"","","");

	}
	if (window.writeBtnStyle)
		writeBtnStyle();

	if (window.writeIntopicBar)
		writeIntopicBar(1);

	if (window.setRelStartPage)
	{
	setRelStartPage("../../cms.htm");

		autoSync(1);
		sendSyncInfo();
		sendAveInfoOut();
	}
}
else
	if (window.gbIE4)
		document.location.reload();
//-->
</script>
<h1>Using the Configuration Manager</h1>

<p>The <span style="font-style: italic;"><I>Configuration Manager</I></span> enables 
 you to create a new &quot;configuration&quot;. The <span style="font-style: italic;"><I>configuration</I></span> 
 contains all compiler settings, directive settings, and component configurations 
 that have been set in Carbon Model Studio. The configuration that is active 
 (selected from the pull-down menu in the toolbar area) is used during 
 the next compile to create the Carbon Model and other related files. Initially 
 there is one configuration, named <span style="font-style: italic;"><I>Default</I></span>, 
 and all settings are saved in the Default configuration.</p>

<p align="center" class="whs1"><script type="text/javascript" language="JavaScript"><!--
if ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) <= 4)) 
{  if (parseInt(navigator.appVersion) != 2) document.write("<img src='../../screenshots/configuration1.png' x-maintain-ratio='TRUE' width='305px' height='59px' border='0' class='img_whs2'>");}
else
{   document.write("<img src='../../screenshots/configuration1.png' x-maintain-ratio='TRUE' style='margin-top:7px;margin-bottom:6px;' width='305px' height='59px' border='0' class='img_whs2'>");}
//--></script><noscript><img src='../../screenshots/configuration1.png' x-maintain-ratio='TRUE' style='margin-top:7px;margin-bottom:6px;' width='305px' height='59px' border='0' class='img_whs2'></noscript></p>

<p>You can create new configurations that apply unique compiler settings, 
 directives settings, etc. to the RTL files in your projects. This may 
 be helpful for creating unique Carbon Models for different parts of your 
 development cycle. For example, you can create one configuration for debug 
 versions, one for optimized runtime version, etc.</p>

<p class=Tip><img src="../../buttons_icons/_lightbulb.png" x-maintain-ratio="TRUE" width="24px" height="17px" border="0" class="img_whs3">Note that each defined configuration uses 
 the configuration name as the last element of the target directory. For 
 example, the Default configuration creates output files in /home/cds/user/Projects/Project1/Default. 
 </p>

<p>To access the Configuration Manager, from the <span style="font-weight: bold;"><B>Project</B></span> 
 menu, select <span style="font-weight: bold;"><B>Configuration Manager</B></span>, 
 and the <a href="../dialog_boxes/edit_configurations_dialog.htm">Edit 
 Configurations</a> dialog displays.</p>

<h2>Create a New Configuration</h2>

<p>To create a new configuration:</p>

<ol type="1" class="whs4">
	
	<li class=kadov-p><p>From the <span style="font-style: italic;"><I>Edit 
 Configurations</I></span> dialog, click the <span style="font-weight: bold;"><B>New</B></span> 
 button and the <a href="../dialog_boxes/create_configuration_dialog.htm">Create 
 Configuration</a> dialog box appears with the default configuration name 
 &lt;New&gt;.</p></li>
	
	<li class=kadov-p><p>Enter the configuration name in the <span style="font-style: italic;"><I>New 
 Configuration Name</I></span> field and leave the <span style="font-style: italic;"><I>Copy 
 settings from</I></span> field as &lt;Empty&gt;.</p></li>
	
	<li class=kadov-p><p>Click <span style="font-weight: bold;"><B>OK</B></span> 
 to add the name. The new configuration name is added and appears at the 
 top of Carbon Model Studio in the Configuration drop-down menu.</p></li>
</ol>

<p align="center" class="whs1"><script type="text/javascript" language="JavaScript"><!--
if ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) <= 4)) 
{  if (parseInt(navigator.appVersion) != 2) document.write("<img src='../../screenshots/configuration2.png' x-maintain-ratio='TRUE' width='308px' height='58px' border='0' class='img_whs5'>");}
else
{   document.write("<img src='../../screenshots/configuration2.png' x-maintain-ratio='TRUE' style='margin-top:7px;margin-bottom:6px;' width='308px' height='58px' border='0' class='img_whs5'>");}
//--></script><noscript><img src='../../screenshots/configuration2.png' x-maintain-ratio='TRUE' style='margin-top:7px;margin-bottom:6px;' width='308px' height='58px' border='0' class='img_whs5'></noscript></p>

<h2>Create a New Configuration based on an Existing Configuration</h2>

<p>To create a new configuration based on the settings in another configuration:</p>

<ol type="1" class="whs4">
	
	<li class=kadov-p><p>From the <span style="font-style: italic;"><I>Edit 
 Configurations</I></span> dialog, click the <span style="font-weight: bold;"><B>New</B></span> 
 button and the <a href="../dialog_boxes/create_configuration_dialog.htm">Create 
 Configuration</a> dialog box appears with the default configuration name 
 &lt;New&gt;.</p></li>
	
	<li class=kadov-p><p>Enter the configuration name in the <span style="font-style: italic;"><I>New 
 Configuration Name</I></span> field and from the pull-down menu in the <span 
 style="font-style: italic;"><I>Copy settings from</I></span> field select the 
 configuration on which you want the setting to be based.</p></li>
	
	<li class=kadov-p><p>Click <span style="font-weight: bold;"><B>OK</B></span> 
 to add the name. The new configuration name is added and appears at the 
 top of Carbon Model Studio in the Configuration drop-down menu.</p></li>
</ol>

<h2 class="whs6">Copy Configuration Settings from one 
 Configuration to Another</h2>

<p class=Warning
	style="margin-top: 7.5pt;"><img src="../../buttons_icons/warning.png" x-maintain-ratio="TRUE" width="26px" height="23px" border="0" class="img_whs7">Copying a configuration will 
 permanently overwrite all existing compiler settings, directive settings, 
 and component configurations. Make sure you want to overwrite these settings 
 in the target configuration before clicking OK.</p>

<p>To copy all the configuration settings from one configuration to another 
 configuration:</p>

<ol type="1" class="whs4">
	
	<li class=kadov-p><p>From the <span style="font-style: italic;"><I>Edit 
 Configurations</I></span> dialog, click the <span style="font-weight: bold;"><B>Copy 
 </B></span>button and the <a href="../dialog_boxes/copy_configuration_dialog.htm">Copy 
 Configuration</a> dialog box appears.</p></li>
	
	<li class=kadov-p><p>Select the name of the configuration that contains 
 the desired settings in the <span style="font-style: italic;"><I>Copy settings 
 from</I></span> field, and select the name of the configuration where you 
 want the settings to be copied in the <span style="font-style: italic;"><I>Copy 
 settings to</I></span> field.</p></li>
	
	<li class=kadov-p><p class="whs8">Click <span style="font-weight: bold;"><B>OK</B></span> 
 and the two configurations will be identical. Now you can edit one of 
 the configurations.</p></li>
</ol>

<p class="whs9">You can also rename and delete existing configurations 
 using the <span style="font-weight: bold;"><B>Remove</B></span> and <span style="font-weight: bold;"><B>Rename</B></span> 
 buttons on the <a href="../dialog_boxes/edit_configurations_dialog.htm">Edit 
 Configurations</a> dialog.</p>

<p>You can use the <span style="font-weight: bold;"><B>Batch Build</B></span> 
 option from the <span style="font-weight: bold;"><B>Build</B></span> menu to 
 compile many configurations at the same time. See <a href="building_multiple_configurations.htm">Building 
 Multiple Configurations</a> for more information.</p>

<script type="text/javascript" language="javascript1.2">
<!--
if (window.writeIntopicBar)
	writeIntopicBar(0);
//-->
</script>
</body>
</html>
