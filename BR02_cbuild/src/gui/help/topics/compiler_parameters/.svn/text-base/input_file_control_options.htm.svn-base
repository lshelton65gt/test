<!doctype HTML public "-//W3C//DTD HTML 4.0 Frameset//EN">

<!-- saved from url=(0014)about:internet -->
<html>

<head>
<title>Input File Control options</title>
<meta http-equiv="content-type" content="text/html; charset=windows-1252">
<meta name="generator" content="Adobe RoboHelp - www.adobe.com">
<link rel="stylesheet" href="../../default_ns.css"><script type="text/javascript" language="JavaScript" title="WebHelpSplitCss">
<!--
if (navigator.appName !="Netscape")
{   document.write("<link rel='stylesheet' href='../../default.css'>");}
//-->
</script>
<style type="text/css">
<!--
img_whs1 { border:none; width:20px; height:14px; float:none; border-style:none; }
ul.whs2 { list-style:disc; }
p.whs3 { margin-left:40px; }
img_whs4 { border:none; width:24px; height:17px; float:none; border-style:none; }
-->
</style><script type="text/javascript" language="JavaScript" title="WebHelpInlineScript">
<!--
function reDo() {
  if (innerWidth != origWidth || innerHeight != origHeight)
     location.reload();
}
if ((parseInt(navigator.appVersion) == 4) && (navigator.appName == "Netscape")) {
	origWidth = innerWidth;
	origHeight = innerHeight;
	onresize = reDo;
}
onerror = null; 
//-->
</script>
<style type="text/css">
<!--
div.WebHelpPopupMenu { position:absolute; left:0px; top:0px; z-index:4; visibility:hidden; }
-->
</style><script type="text/javascript" language="javascript1.2" src="../../whmsg.js"></script>
<script type="text/javascript" language="javascript" src="../../whver.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whproxy.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whutils.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whtopic.js"></script>
</head>
<body><script type="text/javascript" language="javascript1.2">
<!--
if (window.gbWhTopic)
{
	if (window.addTocInfo)
	{
	addButton("show",BTN_TEXT,"Show","","","","",0,0,"","","");

	}
	if (window.writeBtnStyle)
		writeBtnStyle();

	if (window.writeIntopicBar)
		writeIntopicBar(1);

	if (window.setRelStartPage)
	{
	setRelStartPage("../../cms.htm");

		autoSync(1);
		sendSyncInfo();
		sendAveInfoOut();
	}
}
else
	if (window.gbIE4)
		document.location.reload();
//-->
</script>
<h1>Input File Control options</h1>

<p>The Input File Control options are used to define compiler actions that 
 are performed on specified input files.</p>

<h4><a name=directive></a>-directive</h4>

<p>Use this option to specify the name of a directive file that will be 
 used during compilation of the Carbon Model. You do this by clicking the 
 <span style="font-weight: bold;"><B>Browse</B></span> button (<img src="../../buttons_icons/btn_browse1.png" x-maintain-ratio="TRUE" width="20px" height="14px" border="0" class="img_whs1">) 
 in the <span style="font-style: italic;"><I>Value</I></span> column and then 
 selecting the file in the dialog box. There is no restriction on file 
 naming, however it is standard to use a �.dir� or �.dct� suffix for directive 
 files. See <a href="using_a_directives_file.htm">Using a Directives File</a> 
 for more information. </p>

<p>Note that directives specified in files are cumulative. The syntax of 
 a directives file is line oriented�each directive and its values must 
 be specified on its own line.</p>

<h4><a name=f></a>-f</h4>

<p>Use this option to specify a <a href="compiler_options_file.htm">compiler 
 command file</a> name from which the Carbon compiler reads command-line 
 options. The compiler treats these options as if they have been entered 
 on the command line. Note that options specified in files are cumulative. 
 You do this by clicking the <span style="font-weight: bold;"><B>Browse</B></span> 
 button (<img src="../../buttons_icons/btn_browse1.png" x-maintain-ratio="TRUE" width="20px" height="14px" border="0" class="img_whs1">) in the <span style="font-style: italic;"><I>Value</I></span> 
 column and then selecting the file in the dialog box. There is no restriction 
 on file naming.</p>

<p>The syntax of the file is simply a white-space separated list of options 
 and arguments; each option does not need to be specified on a new line. 
 The file may contain comments that use any of the following delimiters: 
 # (shell), // or /* ... */ (C++ style).</p>

<p>The file may also include entries that include environment variable 
 expansion, as follows:</p>

<ul type="disc" class="whs2">
	
	<li class=kadov-p><p>$varname. The variable name must be followed by 
 a space or a recognized delimiter.</p></li>
	
	<li class=kadov-p><p>${varname}. Curly braces can be used in cases 
 where there is no recognized delimiter.</p></li>
</ul>

<p>For example, the following items show valid variables usage in a command 
 file:</p>

<p class="whs3">$TEST_DIR/test1.vhdl - valid because / is 
 a recognized delimiter</p>

<p class="whs3">${TEST_DIR}test1.vhdl - the curly braces 
 are required because no / is used</p>

<p class="whs3">${TEST_DIR}/test1.vhdl - the curly braces 
 are not required in this case, but they are ignored when not needed</p>

<p>Note that the environment variable TEST_DIR must be defined before the 
 Carbon compiler uses the environment variable or you will receive an error.</p>

<p class=Tip><img src="../../buttons_icons/_lightbulb.png" x-maintain-ratio="TRUE" width="24px" height="17px" border="0" class="img_whs4">The syntax $(varname) is not supported. Parenthesis 
 are not recognized as valid delimiters for environment variables. This 
 follows the same rules as the tcsh, csh, bash, and sh shells, as well 
 as the nc and mti simulators.</p>

<h4>-showParseMessages</h4>

<p>If you enable this option, the Carbon compiler writes messages to stdout 
 as it analyzes HDL source files. The current source file being analyzed 
 is printed, and the modules found within each source file are printed. 
 This can be useful during initial model compilation when you want to ensure 
 that the Carbon compiler uses the correct source files.</p>

<p>Example output:</p>

<p class=CodeNoGrey>Note 20001: Analyzing source file &quot;test.v&quot; 
 ...<br>
test.v:1: Note 20004: Analyzing module (top).<br>
test.v:5: Note 20004: Analyzing module (child).<br>
Note 20025: 0 error(s) 0 warning(s).</p>

<script type="text/javascript" language="javascript1.2">
<!--
if (window.writeIntopicBar)
	writeIntopicBar(0);
//-->
</script>
</body>
</html>
