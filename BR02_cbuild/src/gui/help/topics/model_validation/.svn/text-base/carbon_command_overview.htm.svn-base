<!doctype HTML public "-//W3C//DTD HTML 4.0 Frameset//EN">

<!-- saved from url=(0014)about:internet -->
<html>

<head>
<title>Carbon Command Overview</title>
<meta http-equiv="content-type" content="text/html; charset=windows-1252">
<meta name="generator" content="Adobe RoboHelp - www.adobe.com">
<link rel="stylesheet" href="../../default_ns.css"><script type="text/javascript" language="JavaScript" title="WebHelpSplitCss">
<!--
if (navigator.appName !="Netscape")
{   document.write("<link rel='stylesheet' href='../../default.css'>");}
//-->
</script>
<style type="text/css">
<!--
ul.whs1 { list-style:disc; }
p.whs2 { margin-top:9pt; }
img_whs3 { border:none; width:24px; height:17px; float:none; border-style:none; }
-->
</style><script type="text/javascript" language="JavaScript" title="WebHelpInlineScript">
<!--
function reDo() {
  if (innerWidth != origWidth || innerHeight != origHeight)
     location.reload();
}
if ((parseInt(navigator.appVersion) == 4) && (navigator.appName == "Netscape")) {
	origWidth = innerWidth;
	origHeight = innerHeight;
	onresize = reDo;
}
onerror = null; 
//-->
</script>
<style type="text/css">
<!--
div.WebHelpPopupMenu { position:absolute; left:0px; top:0px; z-index:4; visibility:hidden; }
-->
</style><script type="text/javascript" language="javascript1.2" src="../../whmsg.js"></script>
<script type="text/javascript" language="javascript" src="../../whver.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whproxy.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whutils.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whtopic.js"></script>
</head>
<body><script type="text/javascript" language="javascript1.2">
<!--
if (window.gbWhTopic)
{
	if (window.addTocInfo)
	{
	addTocInfo("Creating a Component for Model Validation\nUsing the Carbon Command during Simulation\nCarbon Command Overview");
addButton("show",BTN_TEXT,"Show","","","","",0,0,"","","");

	}
	if (window.writeBtnStyle)
		writeBtnStyle();

	if (window.writeIntopicBar)
		writeIntopicBar(1);

	if (window.setRelStartPage)
	{
	setRelStartPage("../../cms.htm");

		autoSync(1);
		sendSyncInfo();
		sendAveInfoOut();
	}
}
else
	if (window.gbIE4)
		document.location.reload();
//-->
</script>
<h1>Carbon Command overview</h1>

<p>Model Validation provides the ability for your testbench to have some 
 control over the Carbon Model. Specifically, the testbench can control:</p>

<ul type="disc" class="whs1">
	
	<li class=kadov-p><p>Reading and writing memories</p></li>
	
	<li class=kadov-p><p>Forcing and releasing of signals</p></li>
	
	<li class=kadov-p><p>Emitting waveform dump data from the Carbon Model</p></li>
	
	<li class=kadov-p><p>Suppressing Carbon runtime messages</p></li>
</ul>

<p class="whs2">These functions are controlled through calls 
 to a �<span style="font-style: italic;"><I>carbon command</I></span>�. This is 
 a command that Model Validation adds to the simulator�s scripting interface 
 and/or command-line interface. For Verilog, it also exists as a new user-defined 
 function call (often referred to as a �dollar function�).</p>

<p>The <span style="font-weight: bold;"><B>carbon</B></span> command accepts subcommands 
 that perform the actual functions. The subcommands are described in detail 
 in <a href="carbon_subcommands.htm">Carbon Subcommands</a>.</p>

<p>Each simulator environment will have this command added to its command-line 
 interface. The syntax for each simulator is different because they support 
 different mechanisms, but the underlying commands are the same. See <a href="invoking_the_carbon_command.htm">Invoking the Carbon Command</a> 
 for syntax and usage.</p>

<p class=Tip><img src="../../buttons_icons/_lightbulb.png" x-maintain-ratio="TRUE" width="24px" height="17px" border="0" class="img_whs3">The commands all map to a corresponding Carbon 
 C API function. See the Carbon Model API Reference Manual for more details 
 on each command.</p>

<script type="text/javascript" language="javascript1.2">
<!--
if (window.writeIntopicBar)
	writeIntopicBar(0);
//-->
</script>
</body>
</html>
