<!doctype HTML public "-//W3C//DTD HTML 4.0 Frameset//EN">

<!-- saved from url=(0014)about:internet -->
<html>

<head>
<title>Running the Simulation with OnDemand</title>
<meta http-equiv="content-type" content="text/html; charset=windows-1252">
<meta name="generator" content="Adobe RoboHelp - www.adobe.com">
<link rel="stylesheet" href="../../default_ns.css"><script type="text/javascript" language="JavaScript" title="WebHelpSplitCss">
<!--
if (navigator.appName !="Netscape")
{   document.write("<link rel='stylesheet' href='../../default.css'>");}
//-->
</script>
<style type="text/css">
<!--
ol.whs1 { list-style:decimal; }
img_whs2 { border:none; width:24px; height:17px; float:none; border-style:none; }
p.whs3 { margin-left:40px; }
p.whs4 { text-align:center; }
img_whs5 { border:none; margin-top:7px; margin-bottom:6px; float:none; width:611px; height:382px; border-style:none; }
-->
</style><script type="text/javascript" language="JavaScript" title="WebHelpInlineScript">
<!--
function reDo() {
  if (innerWidth != origWidth || innerHeight != origHeight)
     location.reload();
}
if ((parseInt(navigator.appVersion) == 4) && (navigator.appName == "Netscape")) {
	origWidth = innerWidth;
	origHeight = innerHeight;
	onresize = reDo;
}
onerror = null; 
//-->
</script>
<style type="text/css">
<!--
div.WebHelpPopupMenu { position:absolute; left:0px; top:0px; z-index:4; visibility:hidden; }
-->
</style><script type="text/javascript" language="javascript1.2" src="../../whmsg.js"></script>
<script type="text/javascript" language="javascript" src="../../whver.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whproxy.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whutils.js"></script>
<script type="text/javascript" language="javascript1.2" src="../../whtopic.js"></script>
</head>
<body><script type="text/javascript" language="javascript1.2">
<!--
if (window.gbWhTopic)
{
	if (window.addTocInfo)
	{
	addTocInfo("Using OnDemand to Tune Components\nRunning the Simulation with OnDemand\nRunning the Simulation");
addButton("show",BTN_TEXT,"Show","","","","",0,0,"","","");

	}
	if (window.writeBtnStyle)
		writeBtnStyle();

	if (window.writeIntopicBar)
		writeIntopicBar(1);

	if (window.setRelStartPage)
	{
	setRelStartPage("../../cms.htm");

		autoSync(1);
		sendSyncInfo();
		sendAveInfoOut();
	}
}
else
	if (window.gbIE4)
		document.location.reload();
//-->
</script>
<h1>Running the Simulation with OnDemand</h1>

<p>After <a href="configuring_your_model_for_ondemand.htm">Configuring 
 your Model for OnDemand</a>, follow the steps below to define and run 
 the simulation environment.</p>

<ol type="1" class="whs1">
	
	<li class=kadov-p><p>In the <span style="font-style: italic;"><I>Project 
 Explorer</I></span> view, select the top-level project (the <span style="font-style: italic;"><I>&lt;Project_Name&gt;.carbon</I></span> 
 file) to display the <span style="font-style: italic;"><I><a href="../dialog_boxes/project_properties_window.htm">Project 
 Properties</a></I></span> window.</p></li>
	
	<li class=kadov-p><p>In the <span style="font-style: italic;"><I>Simulation 
 Control</I></span> section, enter the name of the simulation script that you 
 want to run in the <span style="font-style: italic;"><I>Simulation Script</I></span> 
 field. For example, <span style="font-weight: bold;"><B>./count.exe</B></span>. 
 This parameter specifies a script or executable that will set up an environment 
 and launch the simulation tool.</p></li>
	
	<li class=kadov-p><p>Optionally, you may need to enter arguments in 
 the <span style="font-style: italic;"><I>Simulation Script Arguments</I></span> 
 field. This parameter is a list of arguments that will be passed to the 
 script on the script�s command line.</p></li>
	
	<li class=kadov-p><p>Make sure the <span style="font-style: italic;"><I>Simulation 
 Script Execution</I></span> field is set to <span style="font-weight: bold;"><B>local</B></span> 
 for UNIX environments, and to <span style="font-weight: bold;"><B>remote</B></span> 
 in Windows environments. This parameter sets the location where the Simulation 
 script is to be executed.</p></li>
	
	<li class=kadov-p><p>Make sure the <span style="font-style: italic;"><I>Simulation 
 User Interface</I></span> field is set to <span style="font-weight: bold;"><B>true</B></span>. 
 This parameter enables Carbon Model Studio to control the simulation options.</p></li>
</ol>

<p class=Tip><img src="../../buttons_icons/_lightbulb.png" x-maintain-ratio="TRUE" width="24px" height="17px" border="0" class="img_whs2">See <a href="../simulation/setting_simulation_properties.htm">Setting 
 Simulation Properties</a> for more information on acceptable values when 
 running simulation on different platforms.</p>

<ol start="6" type="1" class="whs1">
	
	<li class=kadov-p><p>In the toolbar, click the <span style="font-weight: bold;"><B>Simulate</B></span> 
 button. You can also select <span style="font-weight: bold;"><B>Simulate 
 Project</B></span> from the <span style="font-style: italic;"><I>Project</I></span> 
 menu. This creates the Carbon Simulation State (<span style="font-style: italic;"><I>.css</I></span>) 
 file.</p></li>
</ol>

<p class="whs3">In the <span style="font-style: italic;"><I>Main</I></span> 
 view, the <a href="../simulation/runtime_control_window.htm">Runtime Control</a> 
 tab will appear with the file <span style="font-style: italic;"><I>&lt;simulation_script&gt;.css</I></span> 
 in a paused state, allowing you to configure runtime properties of the 
 Carbon Models in the system. This provides you with the opportunity to 
 adjust OnDemand settings prior to actually running the simulation.</p>

<p align="center" class="whs4"><script type="text/javascript" language="JavaScript"><!--
if ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) <= 4)) 
{  if (parseInt(navigator.appVersion) != 2) document.write("<img src='../../screenshots/cssfile1.png' x-maintain-ratio='TRUE' width='611px' height='382px' border='0' class='img_whs5'>");}
else
{   document.write("<img src='../../screenshots/cssfile1.png' x-maintain-ratio='TRUE' style='margin-top:7px;margin-bottom:6px;' width='611px' height='382px' border='0' class='img_whs5'>");}
//--></script><noscript><img src='../../screenshots/cssfile1.png' x-maintain-ratio='TRUE' style='margin-top:7px;margin-bottom:6px;' width='611px' height='382px' border='0' class='img_whs5'></noscript></p>

<p class="whs3">Note that the <span style="font-style: italic;"><I>count</I></span> 
 design used in this example is instantiated three times, as <span style="font-style: italic;"><I>count1</I></span>, 
 <span style="font-style: italic;"><I>count2</I></span>, and <span style="font-style: italic;"><I>count3</I></span>. 
 Each of the instantiations is represented as a row, with icons indicating 
 the current state. </p>

<ol start="7" type="1" class="whs1">
	
	<li class=kadov-p><p>In the <span style="font-style: italic;"><I>OnDemand 
 Control</I></span> column, click to the right of the icon and select <span 
 style="font-weight: bold;"><B>start</B></span> for each of the three instances.</p></li>
	
	<li class=kadov-p><p>In the toolbar, click <span style="font-weight: bold;"><B>Save 
 All</B></span>.</p></li>
	
	<li class=kadov-p><p>In the toolbar, click the <span style="font-weight: bold;"><B>Continue 
 Simulation</B></span> button to run the simulation.</p></li>
	
	<li class=kadov-p><p>Next, see <a href="viewing_the_simulation_results.htm">Viewing 
 the Simulation Results</a>.</p></li>
</ol>

<p>&nbsp;</p>

<script type="text/javascript" language="javascript1.2">
<!--
if (window.writeIntopicBar)
	writeIntopicBar(0);
//-->
</script>
</body>
</html>
