/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "DialogBindConstant.h"
#include <QMessageBox>
#include "gui/CQt.h"
#include <QString>

DialogBindConstant::DialogBindConstant(QWidget *parent)
: CDialog(parent)
, mDB(0)
, mReg(0)
, mField(0)
{
  ui.setupUi(this);

  mValue = 0;
}

DialogBindConstant::~DialogBindConstant()
{

}

void DialogBindConstant::setup(CarbonDB* db, CarbonCfgRegister* reg, CarbonCfgRegisterField* field)
{
  mDB = db;
  mReg = reg;
  mField = field;

  UtString label;

  label << "Register: " << mReg->getName() << " field: " << mField->getName() << " Width: " << field->getWidth();

  int w = field->getWidth();
  
  QString val;

  // Default
  val.sprintf("%0*llx", w/4, 0LL);

  // Check if already mapped to a constant
  if(field->getLoc()) {
    if(field->getLoc()->getType() == eCfgRegLocConstant) {
      val.sprintf("%0*llx", w/4, field->getLoc()->castConstant()->getValue());
    }
  }

  ui.labelRegInfo->setText(label.c_str());
  ui.lineEditValue->setText(val);
}

void DialogBindConstant::setData(const UtString &data)
{
  UtIO::Radix radix = UtIO::dec;
  if (Qt::Checked == ui.checkBoxHex->checkState())
    radix = UtIO::hex;

  UtIStringStream ss(data);
  DynBitVector bv(mField->getWidth());
  if (ss >> radix >> bv) 
  {
    mValue = bv.llvalue();
    accept();
    CDialog::setData(data);
  }
  else
  {
    UtString err;
    err << "Unable to convert: " << data.c_str() << " to constant of width: " << mField->getWidth();

    QMessageBox::critical(this, "Carbon", err.c_str());
  }
}

void DialogBindConstant::on_pushButtonOK_clicked()
{
  UtString v;
  v << ui.lineEditValue->text();
  setData(v);
}

void DialogBindConstant::on_pushButtonCancel_clicked()
{
  reject();
}
