/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CTextEdit.h"
#include <QTextFrame>

CTextEdit::CTextEdit() {
  mPreferredHeight = 0;
}

void CTextEdit::putRowSizeConstraint(UInt32 minRows, UInt32 preferredRows) {
  QFontMetrics fm(font());
  qreal margin = document()->rootFrame()->frameFormat().margin();

  UInt32 minHeight = (UInt32) (fm.height() * minRows + 2*margin);
  setMinimumHeight(minHeight);

  mPreferredHeight = (UInt32) (fm.height() * preferredRows + 2*margin);
}

QSize CTextEdit::sizeHint() const {
  QSize textEditSizeHint = QTextEdit::sizeHint();
  if (mPreferredHeight != 0) {
    textEditSizeHint.setHeight(mPreferredHeight);
  }
  return textEditSizeHint;
}
