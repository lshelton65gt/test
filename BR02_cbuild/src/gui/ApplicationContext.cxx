/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "ApplicationContext.h"

ApplicationContext::~ApplicationContext()
{
}

CarbonSInt32 ApplicationContext::getRegisterTableColumnEnum(const UtString &name)
{
  EnumMapType::iterator iter = mRegisterTableColumnEnumMap.find(name);
  if (iter == mRegisterTableColumnEnumMap.end())
    return -1;
  return iter->second;
}

MaxSimApplicationContext *ApplicationContext::castMaxSim()
{
  return 0;
}

CowareApplicationContext *ApplicationContext::castCoware()
{
  return 0;
}
