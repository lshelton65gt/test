// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef _CARBON_GEN_PROP_H_
#define _CARBON_GEN_PROP_H_

#include "gui/CQt.h"

class CarbonCfg;

class CarbonGenProp : public QWidget {
  Q_OBJECT
public:
  //! ctor
  CarbonGenProp(CQtContext* cqt, CarbonCfg*);

  //! dtor
  ~CarbonGenProp();

  //! clear and read the tree from a carbon config object
  void readConfig();

  //! copy the settings from the widgets to the cfg object
  void updateCfg();

  //! lay out the widgets and return the top level layout
  QWidget* layout();

public slots:
  void changeWaveType(int waveType);

private:
  void build();

  CQtContext* mCQt;
  CarbonCfg* mCfg;

# define ENA_DEBUG_MESSAGES 0
# define MX_CA_SWITCH 0
#if MX_CA_SWITCH
  QRadioButton* mRadioMaxsim;
  QRadioButton* mRadioRealview;
#endif
#if ENA_DEBUG_MESSAGES
  QCheckBox* mEnaDebugMsg;
#endif
  QTextEdit* mComponentDescription;
  QLineEdit* mCompName;
  QComboBox* mESLName;
  QCheckBox* mResetInZeroTime;
  QLineEdit* mCxxFlags;
  QLineEdit* mLinkFlags;

  CFilenameEdit* mWaveFile;
  CFilenameEdit* mIODBFile;
  CFilenameEdit* mLibFile;
  QComboBox* mWaveType;
};

#endif
