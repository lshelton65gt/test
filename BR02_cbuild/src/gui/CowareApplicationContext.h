// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef __COWARE_APPLICATION_CONTEXT_H__
#define __COWARE_APPLICATION_CONTEXT_H__

#include "ApplicationContext.h"

class Car2Cow;

class CowareApplicationContext : public ApplicationContext
{
public:
  CowareApplicationContext(CarbonDB *db, CarbonCfg* cfg, CQtContext *cqt);
  virtual ~CowareApplicationContext();

  // reimplemented functions from base class
  virtual void setDocumentModified(bool value);
  virtual CowareApplicationContext *castCoware();

  void putApp(Car2Cow* app) {mApp=app;}
  Car2Cow* getApp() {return mApp;}

private:
  Car2Cow *mApp;
  enum RegisterTableColumn {colOFFSET = 0, colREGNAME, colWIDTH, colENDIAN, colDESCRIPTION, colFIELDS};
};

#endif
