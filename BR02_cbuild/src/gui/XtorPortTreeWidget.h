#ifndef XtorPortTreeWidget_H
#define XtorPortTreeWidget_H

#include "gui/CDragDropTreeWidget.h"
#include "gui/CDialog.h"

#include <QTableWidget>
#include "ui_XtorPortTreeWidget.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "cfg/CarbonCfg.h"
#include "util/UtWildcard.h"

#include "ApplicationContext.h"


class DialogAdvancedBinding;

class XtorPortTreeWidget : public CDragDropTreeWidget
{
  Q_OBJECT

  enum Column {colPORTNAME=0, colWIDTH, colRTL};

public:
  XtorPortTreeWidget(QWidget *parent = 0);
  ~XtorPortTreeWidget();
  void putContext(ApplicationContext* ctx);
  void Select(CarbonCfgXtorInstance* xinst);
  int BindByName(CarbonCfgXtorInstance* xinst, const char* prefix=NULL, const char* suffix=NULL);
  int AdvancedBind(CarbonCfgXtorInstance* xinst);
  CDialog* getAdvancedDialog();
  void DeleteBinding(CarbonCfgXtorInstance* xinst);

private:
  void updateRow(int row, CarbonCfgXtorConn* xconn, bool clearSelect);
  QTreeWidgetItem* getItem(int row, Column col);
  void connectXtor(CarbonCfgRTLPort* rtlPort, CarbonCfgXtorInstance* xinst, UInt32 portIndex);
  QTreeWidgetItem* findXtorConnItem(CarbonCfgXtorConn* xconn);
  bool isPort(const CarbonDBNode* node);
  void buildSelectionList(QList<QTreeWidgetItem*>& list);

public slots:
  void dropPort(const char*,QTreeWidgetItem* item);

private slots:
  void itemSelectionChanged();

private:
  Ui::XtorPortTreeWidgetClass ui;
  ApplicationContext* mCtx;
  CarbonCfgXtorInstance* mInst;
  DialogAdvancedBinding* mDialog;

signals:
  void xtorBindingsChanged();
  void xtorBindingSelectionChanged(CarbonCfgXtorConn*);
};

#endif // XtorPortTreeWidget_H
