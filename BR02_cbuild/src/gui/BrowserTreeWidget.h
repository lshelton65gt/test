// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef BROWSERTREEWIDGET_H
#define BROWSERTREEWIDGET_H

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"

#include "CarbonMakerContext.h" //new
#include "CarbonProjectWidget.h" //new
#include "DesignXML.h" //new

#include "ApplicationContext.h"

#include <QObject>
#include <QFile>
#include <QTextStream>

#include "gui/CDragDropTreeWidget.h"
#include "ui_BrowserTreeWidget.h"

class XToplevelInterface;
class XInstance;
class CarbonCfg;

class BrowserTreeWidget : public CDragDropTreeWidget
{
  Q_OBJECT

public:
  BrowserTreeWidget(QWidget *parent = 0);
  ~BrowserTreeWidget();
  void putContext(ApplicationContext* ctx, bool loadDesign = true);
  void Initialize(HierarchyMode mode);
  void clearAll();
  const char* getDragText() { return mDragText.c_str(); }
  void selectModule(const char* module);
  void expandModules(QStringList& moduleList);

  XInstance* getInstance() { return mInstance; } //new
  XToplevelInterface* getToplevelInterface() { return mToplevelInterface; } //new
  XDesignHierarchy* getHierarchy() { return mDesignHierarchy; } //new

signals:
  void nodeSelectionChanged(CarbonDBNode* node, XToplevelInterface* iface, XInstance* xinst);
  void nodeDoubleClicked(const char* node, const char* module, const char* locator);

private:
  void addInstance(QTreeWidgetItem* parent, XToplevelInterface* topi, XInstance* inst);
  void popLibrary(QMap<QString, QTreeWidgetItem*> libMap, XToplevelInterface* topi, XInstance* inst);
  void buildSelectionList(QList<QTreeWidgetItem*>& list);
  void selectModule(QTreeWidgetItem* parent, const char* moduleName, QStringList& moduleList);

private:
  CarbonDB* mDB;
  CarbonCfg* mCfg;
  ApplicationContext* mCtx;
  UtString mDragText;
  QString mHierLine;
  HierarchyMode mHierMode;
  XToplevelInterface* mContextMenuTopLevelInterface; 
  XInstance* mContextMenuInstance;

  XDesignHierarchy* mDesignHierarchy; //new
  XInstance* mInstance; //new
  XToplevelInterface* mToplevelInterface; //new
  CarbonProjectWidget* mProjectWidget; //new
  QAction* mActionNavigateSource;
  QAction* mActionNavigateInstance;
  QAction* mActionNavigateArchitecture;
  QAction* mActionObserveInstance;
  QAction* mActionObserveModule;
  QAction* mActionDepositInstance;
  QAction* mActionDepositModule;
  QMenu* observe;
  QMenu* deposit;

private:
  Ui::BrowserTreeWidgetClass ui;

  private slots:
    void itemSelectionChanged();
    void itemDoubleClicked(QTreeWidgetItem* item, int col);
    void showContextMenu(QPoint);
    void navigateInstanceSource();
    void navigateModuleSource();
    void navigateArchitecture();
    void observeInstance();
    void observeModule();
    void depositInstance();
    void depositModule();
};

#endif // BROWSERTREEWIDGET_H
