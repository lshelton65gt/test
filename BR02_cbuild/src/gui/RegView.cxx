//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// to do: 
//   get multiple selections working
//     delete
//     change name, comment, radix
//     drag-under animation
//   add reg group

#include "util/CarbonPlatform.h"
#include "util/SourceLocator.h"
#include "util/UtIStream.h"
#include "util/UtStringUtil.h"
#include "util/AtomicCache.h"
#include "util/UtStringArray.h"
#include "util/DynBitVector.h"
#include "textedit.h"
#include "gui/CDragDropTreeWidget.h"
#include "RegView.h"
#include "DialogRegProperties.h"
#include "ReplayGUI.h"
#include <QtGui>
#include <QTreeWidgetItem>
#include <QVBoxLayout>
#include <QStringList>
#include <QTimer>
#include <cassert>

RegView::RegView(CQtContext* cqt, AtomicCache* atomicCache,
                 QAction* deleteAction, ReplayGUI* replay_gui)
  : mAtomicCache(atomicCache),
    mDeleteAction(deleteAction),
    mEditRegister(NULL),
    mEditGroup(NULL),
    mCQt(cqt),
    mDefaultTextColor(CQt::getDefaultTextColor()),
    mGroupIndex(0),
    mMainWindow(NULL),
    mReplayGUI(replay_gui),
    mDialogRegProperties(NULL),
    mNextIndex(0)
{
  CQtRecordBlocker blocker(cqt);

  QStringList headers;
  mRegTree = cqt->dragDropTreeWidget("regTree", true);
  mRegTree->setSelectionMode(QAbstractItemView::ExtendedSelection);
  mRegTree->setAlternatingRowColors(true);
  mRegTree->setColumnCount(4);
  
  headers.clear();
  headers << "RTL Path";
  headers << "Radix";
  headers << "Current Value";
  headers << "Breakpoint";
  mRegTree->setHeaderLabels(headers);
  CQT_CONNECT(mRegTree, dropReceived(const char*,QTreeWidgetItem*),
              this, dropRegister(const char*,QTreeWidgetItem*));
  CQT_CONNECT(mRegTree, mapDropItem(QTreeWidgetItem**),
              this, mapDropItem(QTreeWidgetItem**));
  CQT_CONNECT(mRegTree, itemActivated(QTreeWidgetItem*,int),
              this, doubleClick(QTreeWidgetItem*,int));
  
/*
  mBreakpointType = cqt->enumBox<CarbonBreakpointTypeIO>("bkpt_type");
  mBreakpointValue = cqt->lineEdit("bkpt_val");
  mRadix = cqt->enumBox<CarbonRadixIO>("reg_radix");

*/
  mContinueSim = cqt->pushButton("reg_cont_sim", "Continue Sim");
  mEditObj = cqt->pushButton("edit_obj", "Edit");
  mCreateGroup = cqt->pushButton("create_reg_group", "Create Group");
  mDeleteObj = cqt->pushButton("delete", "Delete");

  mEditObj->setEnabled(false);
  mDeleteObj->setEnabled(false);
  QPalette palette;
  mSaveSimContForeground = palette.color(QPalette::Active,
                                         mContinueSim->foregroundRole());
  mSaveSimContBackground = palette.color(QPalette::Active,
                                         mContinueSim->backgroundRole());
  mContinueSim->setEnabled(false);

  CQT_CONNECT(mRegTree, selectionChanged(), this, changeRegTreeSel());
  CQT_CONNECT(mCreateGroup, clicked(), this, createGroup());
  CQT_CONNECT(mDeleteObj, clicked(), this, deleteObj());
  CQT_CONNECT(mEditObj, clicked(), this, editObj());
  CQT_CONNECT(mContinueSim, clicked(), this, continueSim());

/*
  CQT_CONNECT(mRadix, currentIndexChanged(int),
              this, changeRegRadix(int));
  CQT_CONNECT(mBreakpointType, currentIndexChanged(int),
              this, changeBreakpointType(int));
  CQT_CONNECT(mBreakpointValue, textChanged(const QString&),
              this, changeBreakpointValue(const QString&));
*/
  mDisable = false;
} // RegView::RegView

RegView::~RegView() {
}


class HidableMainWindow : public QMainWindow {
public:
  HidableMainWindow(QWidget* parent) : QMainWindow(parent) {
    readSettings();
  }

  virtual void closeEvent(QCloseEvent *event) {
    writeSettings();
    event->ignore();
    hide();
  }

  void writeSettings() {
    QSettings settings("Carbon", "Registers");
    settings.setValue("pos", pos());
    settings.setValue("size", size());
  }

  void readSettings() {
    QSettings settings("Carbon", "Registers");
    QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
    QSize size = settings.value("size", QSize(400, 400)).toSize();
    resize(size);
    move(pos);
  }
};

void RegView::buildWindow(QWidget* parent) {
  if (mMainWindow == NULL) {
    mMainWindow = new HidableMainWindow(parent);
    mMainWindow->setWindowTitle("Carbon Registers");
    mMainWindow->setCentralWidget(buildLayout());
  }
  mMainWindow->show();
  mMainWindow->raise();
}

void RegView::clear() {
  for (int i = mRegTree->topLevelItemCount() - 1; i >= 0; --i) {
    QTreeWidgetItem* groupItem = mRegTree->topLevelItem(i);
    deleteGroup(groupItem, i);
  }
  INFO_ASSERT(mItemRegMap.empty(), "Should have emptied map");
  INFO_ASSERT(mRegTree->topLevelItemCount() == 0, "should have emptied tree");
}

void RegView::launchRegPropDialog(const ItemVec& v) {
  if (!v.empty()) {
    // base the defaults on the first item
    QTreeWidgetItem* item = v[0];
    CarbonReg* reg = mItemRegMap.find(item);
    UtString current_val;
    current_val << item->text(eCurrentValue);
    if (mDialogRegProperties == NULL) {
      mDialogRegProperties = new DialogRegProperties(mMainWindow);
    }
    mDialogRegProperties->setValues(reg->getRadix(), reg->getBreakpointType(),
                                    reg->getBreakpointValue(),
                                    current_val.c_str());

    if (mDialogRegProperties->exec()) {
      // apply them to each item
      for (UInt32 i = 0; i < v.size(); ++i) {
        item = v[i];
        reg = mItemRegMap.find(item);

        reg->putRadix(mDialogRegProperties->mRadix);
        reg->putBreakpointType(mDialogRegProperties->mBreakpointType);
        reg->putBreakpointValue(mDialogRegProperties->mBreakpointValue.c_str());
        setRegisterTreeText(item, reg);
      }
      mReplayGUI->netViewChanged();
    }
  } // if
} // void RegView::launchRegPropDialog

void RegView::doubleClick(QTreeWidgetItem* item, int /* col */) {
  ItemVec v;
  if (mItemRegMap.test(item)) {
    v.push_back(item);
    launchRegPropDialog(v);
  }
}

/*
void RegView::changeBreakpointType(int index) {
  CarbonBreakpointType bt = (CarbonBreakpointType) index;
  applyChanges(NULL, &bt);
}

void RegView::changeBreakpointValue(int index) {
  CarbonBreakpointType bt = (CarbonBreakpointType) index;
  applyChanges(NULL, &bt);
}
*/

void RegView::changeRegTreeSel() {
  CQtRecordBlocker blocker(mCQt);

  QTreeItemList sel = mRegTree->selectedItems();
  int numSelected = 0;
  QTreeWidgetItem* item = NULL;

  mEditGroup = NULL;
  mEditRegister = NULL;
  bool save_disable = mDisable;
  mDisable = true;

  UtString dragText;

  // Find the currently selected register group/name and populate the editing
  // widgets
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    item = *p;

    bool isGroup = (item->parent() == NULL);
    if (isGroup) {
      mEditGroup = mItemGroupMap.find(item);
    }
    else {
      mEditRegister = mItemRegMap.find(item);
      dragText << mEditRegister->getPath() << "\n";
    }
    ++numSelected;
  }
  mRegTree->putDragData(dragText.c_str());

  mDeleteObj->setEnabled(numSelected > 0);
  mEditObj->setEnabled(numSelected > 0);

  if (mEditRegister != NULL) {
    setRegisterWidgets(numSelected, mEditRegister);
  }
  mDisable = save_disable;
} // void RegView::changeRegTreeSel

void RegView::setRegisterWidgets(int /*numSelected*/, CarbonReg* /*reg*/) {
  CQtRecordBlocker blocker(mCQt);

  mDisable = true;
/*
  if (numSelected == 1) {
    mRadix->setCurrentIndex(reg->getRadix());
    CarbonBreakpointType bt = reg->getBreakpointType();
    mBreakpointType->setCurrentIndex(bt);
    if ((bt == eCarbonBreapointOnEqual) || (bt == eCarbonBreapointOnNotEqual))
    {
      mBreakpointValue->setEnabled(true);
      mBreakpointValue->setText(reg->getBreakpointValue());
    }
    else {
      mBreakpointValue->setEnabled(false);
      mBreakpointValue->setText("");
    }
    mRadix->setEnabled(true);
    mBreakpointType->setEnabled(true);
  }
  else {
    mRadix->setEnabled(true);
    mBreakpointType->setEnabled(false);
    mBreakpointValue->setEnabled(false);
  }
*/
  mDisable = false;
} // void RegView::setRegisterWidgets

void RegView::setRegisterTreeText(QTreeWidgetItem* item,
                                  CarbonReg* reg)
{
  item->setText(eRTLPath, reg->getPath());
  item->setText(eRadix, reg->getRadix().getString());
  UtString bkpt;
  switch (reg->getBreakpointType()) {
  case eCarbonBreakpointOff:
    break;
  case eCarbonBreakpointOnChange:
    bkpt = "On Change";
    break;
  case eCarbonBreakpointOnEqual:
    bkpt << "On Equal: " << reg->getBreakpointValue();
    break;
  case eCarbonBreakpointOnNotEqual:
    bkpt << "On Not Equal: " << reg->getBreakpointValue();
    break;
  }
  item->setText(eBreakpoint, bkpt.c_str());
}

void RegView::applyChanges(CarbonRadix* radix, CarbonBreakpointType* bt) {
  if (mDisable) {
    return;
  }

  QTreeItemList sel = mRegTree->selectedItems();

  // Find the currently selected register group/name and populate the editing
  // widgets
  int numSelected = 0;
  CarbonReg* reg = NULL;
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;
    reg = mItemRegMap.find(item);
    ++numSelected;

    if (radix != NULL) {
      reg->putRadix(*radix);
    }
    else if (bt != NULL) {
      reg->putBreakpointType(*bt);
      changeRegTreeSel();       // may have to enable/disable bkpt val
    }
    setRegisterTreeText(item, reg);
  } // for
} // void RegView::applyChanges

QWidget* RegView::buildLayout() {
  QBoxLayout* all_ctrls = CQt::hbox();
  *all_ctrls << CQt::eStretch << mContinueSim
             << CQt::eSpace << mEditObj
             << CQt::eSpace << mCreateGroup
             << CQt::eSpace << mDeleteObj
             << CQt::eStretch;
  // , mRadix, new QLabel("Breakpoints: "), mBreakpointType, mBreakpointValue);

  QBoxLayout* layout = CQt::vbox();
  *layout << mRegTree << all_ctrls;
  return CQt::makeWidget(layout);
}


bool RegView::isDeleteActive() const {
  return (mEditGroup != NULL) || (mEditRegister != NULL);
}

void RegView::deleteNetItem(QTreeWidgetItem* item, int childIndex,
                            bool removeReg)
{
  QTreeWidgetItem* groupItem = item->parent();

  QTreeWidgetItem* check = groupItem->takeChild(childIndex);
  INFO_ASSERT(check == item, "Tree item inconsistency");

  CarbonReg* reg = mItemRegMap.find(item);
  mNetRegisterNameMap.erase(reg->getPath());
  mItemRegMap.unmap(reg);
  if (mEditRegister == reg) {
    mEditRegister = NULL;
  }
  if (removeReg) {
    reg->getGroup()->removeReg(reg);
  }

  delete item;
}

void RegView::deleteGroup(QTreeWidgetItem* groupItem, int idx) {
  CarbonGroup* group = mItemGroupMap.find(groupItem);
  for (int i = groupItem->childCount() - 1; i >= 0; --i) {
    deleteNetItem(groupItem->child(i), i, true);
  }
  mRegTree->takeTopLevelItem(idx);
  mGroups.remove(group);
  mItemGroupMap.unmap(groupItem);
  delete groupItem;
}

void RegView::continueSim() {
  mContinueSim->setEnabled(false);
  mReplayGUI->continueSim();
  QPalette palette;
  palette.setColor(mContinueSim->backgroundRole(), mSaveSimContBackground);
  palette.setColor(mContinueSim->foregroundRole(), mSaveSimContForeground);
  mContinueSim->setPalette(palette);
}
  
void RegView::showContinueButton() {
  mContinueSim->setEnabled(true);
  QPalette palette;
  palette.setColor(mContinueSim->backgroundRole(), QColor(Qt::red));
  palette.setColor(mContinueSim->foregroundRole(), QColor(Qt::black));
  mContinueSim->setPalette(palette);
}

void RegView::editObj() {
  QTreeItemList sel = mRegTree->selectedItems();
  ItemVec items;

  // Find the currently selected register group/name and populate the editing
  // widgets
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;

    // Do not edit groups, only registers
    if (mItemRegMap.test(item)) {
      items.push_back(item);
    }
  }

  launchRegPropDialog(items);
}

void RegView::deleteObj() {
  // First, delete all the item group items and their children
  QTreeItemList sel = mRegTree->selectedItems();
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* groupItem = *p;
    QTreeWidgetItem* parent = groupItem->parent();
    if (parent == NULL) {
      int idx = mRegTree->indexOfTopLevelItem(groupItem);
      INFO_ASSERT(idx != -1, "cannot find groupItem in tree");
      deleteGroup(groupItem, idx);
    }
  }

  // Now, delete all the remaining selected children
  sel = mRegTree->selectedItems();
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;
    QTreeWidgetItem* groupItem = item->parent();
    INFO_ASSERT(groupItem != NULL, "There should be no selected groups");
    int idx = groupItem->indexOfChild(item);
    INFO_ASSERT(idx != -1, "cannot find net item in group");
    deleteNetItem(item, idx, true);
  }

  // There should be no more selected children
  sel = mRegTree->selectedItems();
  INFO_ASSERT(sel.empty(), "Should be no more children");

  setRegisterWidgets(0, NULL);
  if (mReplayGUI != NULL) {
    mReplayGUI->netViewChanged();
  }
} // void RegView::deleteObj

void RegView::createGroup() {
  CQtRecordBlocker blocker(mCQt);

  // Find a group name that is not yet used
  UtString groupName;
  groupName << "group_" << mGroupIndex++;
  //mCfg->getUniqueGroupName(&groupName, "");
  CarbonGroup* group = new CarbonGroup(groupName.c_str());
  QTreeWidgetItem* groupItem = addGroup(group, false);
  mRegTree->deselectAll();
  mRegTree->setItemSelected(groupItem, true);
}

void RegView::mapDropItem(QTreeWidgetItem** item) {
  QTreeWidgetItem* groupItem = *item;
  
  // If the user dropped over a register, pop to the group
  QTreeWidgetItem* parent;
  if (groupItem != NULL) {
    while ((parent = groupItem->parent()) != NULL) {
      groupItem = parent;
    }
  }
  else {
    // Drop over empty space goes into the last group
    UInt32 ngroups = mRegTree->topLevelItemCount();
    if (ngroups > 0) {
      groupItem = mRegTree->topLevelItem(ngroups - 1);
    }
  }

  if (groupItem != NULL) {
    *item = groupItem;
  }
}

void RegView::dropRegister(const char* path, QTreeWidgetItem* groupItem)
{
  // Figure which group we are depositing into.  We will only
  // create one auto-group for a multi-drag action
  CarbonGroup* group = NULL;

  // multiple drops may be separated by newlines
  UtIStringStream is(path);
  UtString buf;
  UInt32 numDropped = 0;
  CarbonReg* reg = NULL;
  mRegTree->deselectAll();
  QTreeWidgetItem* item = NULL;

  UtString warning;

  while (is.getline(&buf)) {
    StringUtil::strip(&buf, "\n");
    path = buf.c_str();
    
    if (group == NULL) {      // only build a group once for N drops
      if (groupItem == NULL) {
        // If there are no groups yet, then automatically create one, otherwise
        // take the last one.
        UInt32 ngroups = mRegTree->topLevelItemCount();
        if (ngroups == 0) {
          createGroup();
          mRegTree->deselectAll();
          ngroups = 1;
        }
        groupItem = mRegTree->topLevelItem(ngroups - 1);
      }
      else {
        QTreeWidgetItem* parent = groupItem->parent();
        if (parent != NULL) {
          groupItem = parent;
        }
      }
      group = mItemGroupMap.find(groupItem);
    }

    mRegTree->expandItem(groupItem);

    // If this net is already in another group, remove its item and regroup
    // it, but retain the previous name, radix, and comment settings
    // put it in the new group
    reg = mNetRegisterNameMap[path];
    if (reg != NULL) {
      QTreeWidgetItem* oldGroupItem = mItemGroupMap.find(reg->getGroup());
      QTreeWidgetItem* oldItem = mItemRegMap.find(reg);
      int idx = oldGroupItem->indexOfChild(oldItem);
      INFO_ASSERT(idx != -1, "cannot find net item in group");
      deleteNetItem(oldItem, idx, false);
      reg->putGroup(group);
    }
    else {
      reg = new CarbonReg(path, group, eCarbonHex, mNextIndex++);
      group->addReg(reg);
    }
    item = addReg(groupItem, reg);
    mRegTree->setItemSelected(item, true);
    ++numDropped;
  } // while

  if (! warning.empty()) {
    mCQt->warning(this, warning.c_str());
  }

  if (item != NULL) {
    mRegTree->scrollToItem(item);
  }
  setRegisterWidgets(numDropped, reg);
  if (numDropped != 0) {
    resizeColumns();
    if (mReplayGUI != NULL) {
      mReplayGUI->netViewChanged();
    }
  }
} // void RegView::dropRegister

void RegView::resizeColumns() {
  mRegTree->resizeColumnToContents(eRTLPath);
  mRegTree->resizeColumnToContents(eRadix);
}

QTreeWidgetItem* RegView::addGroup(CarbonGroup* group, bool expand_item) {
  mGroups.push_back(group);
  QTreeWidgetItem* groupItem = new QTreeWidgetItem(mRegTree);
  groupItem->setText(0, group->getName());
  mItemGroupMap.map(groupItem, group);
  if (expand_item) {
    mRegTree->expandItem(groupItem);
  }
  return groupItem;
}

QTreeWidgetItem* RegView::addReg(QTreeWidgetItem* groupItem,
                                 CarbonReg* reg)
{
  QTreeWidgetItem* item = new QTreeWidgetItem(groupItem);
  mItemRegMap.map(item, reg);
  mNetRegisterNameMap[reg->getPath()] = reg;
  setRegisterTreeText(item, reg);
  return item;
} // QTreeWidgetItem* RegView::addReg

void RegView::putRegValue(CarbonReg* reg, const char* val, bool is_fired,
                          bool sim_advance)
{
  QTreeWidgetItem* item = mItemRegMap.find(reg);
  UtString new_val;

  // The value always comes in a hex string from the simulator,
  // so parse it in and translate it.
  if ((reg->getRadix() == eCarbonHex) || (*val == '\0')) {
    // no need to translate
    new_val = val;
  }
  else {
    UInt32 num_bits = 4*strlen(val); // TO DO: signed dec. needs exact # bits
    UtIStringStream is(val);
    DynBitVector bv(num_bits);
    if (is >> eCarbonHex >> bv) {
      UtOStringStream os(&new_val);
      CarbonRadix radix = reg->getRadix();
      os << radix << bv;
    }
    else {
      new_val = "Could not parse value";
    }
  }

  if (new_val == reg->getCurrentValue()) {
    // The same value.  Set the color back to normal, but only if
    // the simulation advanced since the previous value update.  When
    // we hit a breakpoint, we may get new simulation values without
    // having time advance, so we should leave the changed values green.
    if (sim_advance) {
      item->setTextColor(eCurrentValue, QColor(Qt::black));
    }
  }
  else {
    item->setText(eCurrentValue, new_val.c_str());
    item->setTextColor(eCurrentValue, QColor(Qt::darkGreen));
    reg->putCurrentValue(new_val.c_str());
  }


  
  item->setTextColor(eBreakpoint,
                     is_fired ? QColor(Qt::darkRed) : QColor(Qt::black));
} // void RegView::putRegValue

void RegView::changeName(const QString&) {
}

void CarbonGroup::removeReg(CarbonReg* reg) {
  mRegs.remove(reg);
  delete reg;
}

CarbonReg::CarbonReg(const char* signal, CarbonGroup* group,
                     CarbonRadix radix, UInt32 idx)
  : mSignal(signal), mGroup(group), mRadix(radix), mIndex(idx)
{
  mBreakpointType = eCarbonBreakpointOff;
  mIsLocalized = false;
}

CarbonReg::~CarbonReg() {
}

void CarbonReg::localize(const char* comp, const char* design_path) {
  mComponent = comp;
  mDesignPath = design_path;
  mIsLocalized = true;
}
