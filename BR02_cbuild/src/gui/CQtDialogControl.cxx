/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CQt.h"
#include "util/UtIOStream.h"
#include "gui/CQtDialogControl.h"
#include "gui/CDialog.h"

CQtDialogControl::CQtDialogControl(CDialog *dialog, CQtContext *cqt)
  : mDialog(dialog),
    mCQt(cqt)
{
  CQT_CONNECT(dialog, dataEntered(const UtString&), this, dataEntered(const UtString&));
}

void CQtDialogControl::dumpState(UtOStream& stream)
{
  stream << mDialog->getData() << "\n";
}

void CQtDialogControl::getValue(UtString* str) const
{
  str->clear();
  *str << mDialog->getData();
}

bool CQtDialogControl::putValue(const char *str, UtString* /* errmsg */)
{
  UtString val(str);
  mDialog->putRecordData(val);

  return true;
}

QWidget* CQtDialogControl::getWidget() const
{
  return mDialog;
}

void CQtDialogControl::dataEntered(const UtString &data)
{
  mCQt->recordPutValue(mDialog, data.c_str());
}

