// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef DIALOGREGPROP_H
#define DIALOGREGPROP_H

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "carbon/carbon_dbapi.h"
#include "gui/CQt.h"
#include "gui/CDialog.h"
#include "ui_DialogRegProperties.h"

class DialogRegProperties : public CDialog
{
  Q_OBJECT

public:
  DialogRegProperties(QWidget *parent = 0);
  ~DialogRegProperties();

  void setValues(CarbonRadix radix,
                 CarbonBreakpointType,
                 const char* breakpoint_val,
                 const char* current_val);

private:
  void setupValueField();
  void changeRadix(CarbonRadix);
  Ui_DialogRegProperties ui;

private slots:
  void on_binary_toggled(bool);
  void on_octal_toggled(bool);
  void on_decimal_toggled(bool);
  void on_udecimal_toggled(bool);
  void on_hexadecimal_toggled(bool);
  void on_bkpt_off_toggled(bool on);
  void on_bkpt_on_not_eq_toggled(bool on);
  void on_bkpt_on_eq_toggled(bool on);
  void on_bkpt_on_change_toggled(bool on);
  void on_bkpt_value_textChanged(const QString&);

public:
  // reimplemented function from CDialog
  // virtual void setData(const UtString &data);

  CarbonRadix mRadix;
  CarbonBreakpointType mBreakpointType;
  UtString mBreakpointValue;
  UtString mCurrentValue;
};

#endif // DIALOGREGPROP_H
