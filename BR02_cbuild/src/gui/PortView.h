// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef __PORTVIEW_H__
#define __PORTVIEW_H__

#include "car2cow.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"

#include "TieOffTableWidget.h"
#include "PortTableWidget.h"

#include "ApplicationContext.h"

#include <QObject>
#include <QTableWidget>
#include <QtGui>

class PortView
{
public:
  PortView(Car2Cow* pMainWnd);
  virtual ~PortView();

  void Populate(ApplicationContext* ctx, bool initPorts);
  void Update();
  void putPortTable(PortTableWidget* newVal) { m_pPortTable=newVal; }
  void putTieoffTable(TieOffTableWidget* newVal) { m_pTieOffTable=newVal; }
  void putDisconnectTable(DisconnectTableWidget* newVal) { m_pDisconnectTable=newVal; }

private:

private:
  Car2Cow* m_pMainWnd;

  PortTableWidget* m_pPortTable;
  TieOffTableWidget* m_pTieOffTable;
  DisconnectTableWidget* m_pDisconnectTable;

  CarbonDB* mDB;
  CarbonCfgID mCfg;
  ApplicationContext* mCtx;
};
#endif

