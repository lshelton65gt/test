/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CQtAction.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"
#include "util/UtIOStream.h"
#include "gui/CQt.h"
#include <QAction>

CQtAction::CQtAction(StringAtom* name, QAction* action)
  : mName(name),
    mAction(action)
{
}

void CQtAction::dumpState(UtOStream& stream)
{
  stream << mAction->text() << "\n";
}

void CQtAction::triggered(bool checked) {
  emit notify(mName, checked);
}

void CQtAction::textChanged(const QString& str) {
  emit notifyText(mName, str);
}

void CQtAction::indexChanged(int idx) {
  emit notifyIndex(mName, idx);
}

QWidget* CQtAction::getWidget() const {
  return NULL;
}

void CQtAction::getValue(UtString* str) const {
  str->clear();
}

bool CQtAction::putValue(const char*, UtString*) {
  INFO_ASSERT(mAction->isEnabled(), "action insanity");
  mAction->activate(QAction::Trigger);
  return true;
}
