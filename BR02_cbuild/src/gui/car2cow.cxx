/******************************************************************************
 Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "car2cow.h"
#include <QFileDialog>
#include <QtDebug>
#include <QtGui>
#include <QProcess>

#include "PortView.h"
#include "RegisterTableWidget.h"
#include "DialogAdvancedBinding.h"

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/AtomicCache.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/ArgProc.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/RandomValGen.h"
#include "shell/CarbonAbstractRegister.h"

#include "cfg/carbon_cfg.h"
#include "cfg/carbon_cfg_misc.h"
#include "cfg/CarbonCfg.h"

#include "gui/CQt.h"

#include "shell/carbon_misc.h"

#include "shell/carbon_capi.h"

#include "CodeGenerator.h"


#include "gui/CQtPushButtonControl.h"
#include "gui/CQtTableWidgetControl.h"
#include "gui/CQtDialogControl.h"
#include "gui/CQt.h"


struct librarySorter {
  librarySorter() {}

  bool operator()(const char* s1, const char* s2) const
  {
    return (strcasecmp(s1, s2) < 0);
  }
};

Car2Cow::Car2Cow(CQtContext *cqt, const QString& qFilename)
  : CMainWindow(0, 0),
    mCQt(cqt)
{
  m_pReg = carbonAbstractRegisterCreate(0,0,0);

  m_pDB = NULL;
  m_cfgID = NULL;
  mReg = NULL;
  mSelectedNode = NULL;
  mMemorySelectedNode = NULL;
  mXtorInst = NULL;

  mCodegen = new CodeGenerator(this); 

  ui.setupUi(this);

  isModified = false;

  m_pPortView = new PortView(this);

  UtString f;
  f << qFilename;

  initialize(f.c_str());

  loadFile(qFilename);
}

Car2Cow::Car2Cow(CQtContext *cqt, QWidget *parent, Qt::WFlags flags)
  : CMainWindow(parent, flags),
    mCQt(cqt)
{
  m_pReg = carbonAbstractRegisterCreate(0,0,0);
  m_pDB = NULL;
  m_cfgID = NULL;
  mReg = NULL;
  mSelectedNode = NULL;
  mCodegen = new CodeGenerator(this); 

  ui.setupUi(this);

  isModified = false;

  m_pPortView = new PortView(this);

  setCurrentFile("");

  initialize();
}

void Car2Cow::setCurrentFile(const QString &fileName)
{
//  static int sequenceNumber = 1;

  isUntitled = fileName.isEmpty();
  if (isUntitled) {
    curFile = "";
  } else {
    curFile = QFileInfo(fileName).canonicalFilePath();
  }

  setWindowModified(false);

  setWindowTitle(tr("%1[*] - %2").arg(strippedName(curFile))
    .arg(tr("Carbon CoWare Wizard")));
}


void Car2Cow::initialize(const char* filename)
{
  isUntitled = true;

  mMsgCallback = carbonAddMsgCB(NULL, sMsgCallback, this);

  m_pDB = NULL;

  // Initialize Carbon Cfg
  m_cfgID = carbonCfgModeCreate(CarbonCfg::ePlatarch);
  if (carbonCfgReadXtorLib(m_cfgID, eCarbonXtorsCoware) == eCarbonCfgFailure) {
    warning(this, carbonCfgGetErrmsg(m_cfgID));
  }

  populateXtorLibComboBox();
  xtorLibraryChanged(0);

  mContext = new CowareApplicationContext(m_pDB, m_cfgID, mCQt);
  mContext->putApp(this);
  ui.tableRegisters->putContext(mContext);
  ui.tableFields->putContext(mContext, NULL);
  ui.browserTree->putContext(mContext);
  
  ui.browserList->putContext(mContext, NULL);
  ui.browserTreeMem->putContext(mContext);
  ui.browserListMem->putContext(mContext, NULL);

  ui.tableTransactors->putContext(mContext);
  ui.tableTransactorPorts->putContext(mContext);
  ui.xtorPortTree->putContext(mContext);
  
  // Registers
  CQT_CONNECT(ui.tableFields, selectionChanged(CarbonCfgRegisterField*), this, fieldSelectionChanged(CarbonCfgRegisterField*));
  CQT_CONNECT(ui.browserList, selectionChanged(CarbonDBNode*), this, selectionChanged(CarbonDBNode*));
  CQT_CONNECT(ui.tableRegisters, regSelectionChanged(CarbonCfgRegister*), this, registerSelectionChanged(CarbonCfgRegister*));
  CQT_CONNECT(ui.tabRegisters, currentChanged(int), this, tabChanged(int));


  // Memories
  CQT_CONNECT(ui.browserListMem, selectionChanged(CarbonDBNode*), this, memSelectionChanged(CarbonDBNode*));
  CQT_CONNECT(ui.treeWidgetMem, memSelectionChanged(CarbonCfgMemory*), this, memSelectionChanged(CarbonCfgMemory*));


  // Transactors
  CQT_CONNECT(ui.tableTransactorPorts, xtorBindingSelectionChanged(CarbonCfgXtorConn*), this, xtorBindingSelectionChanged(CarbonCfgXtorConn*));
  CQT_CONNECT(ui.tableTransactors, xtorSelectionChanged(CarbonCfgXtorInstance*), this, xtorSelectionChanged(CarbonCfgXtorInstance*));
  CQT_CONNECT(ui.tableTransactorPorts, xtorBindingsChanged(), this, xtorBindingsChanged());

  CQT_CONNECT(ui.comboBoxXtorLib, currentIndexChanged(int), this, xtorLibraryChanged(int));

  // Register Widgets

  registerWidgets(filename);

  setupToolbar(filename);
}

void Car2Cow::xtorBindingsChanged()
{
  ui.xtorPortTree->Refresh();
}


void Car2Cow::on_pushButtonBindAdv_clicked()
{
  ui.tableTransactorPorts->AdvancedBind(mXtorInst);
}

void Car2Cow::on_pushButtonDeleteBinding_clicked()
{
  ui.tableTransactorPorts->DeleteBinding(mXtorInst);
}

void Car2Cow::on_pushButtonBindByName_clicked()
{
  ui.tableTransactorPorts->BindByName(mXtorInst);
}

void Car2Cow::on_pushButtonAddXtor_clicked()
{
  QComboBox* cbox = ui.comboBoxXtor;
  int index = cbox->currentIndex();
  if (index != -1)
  {
    QVariant qv = cbox->itemData(index);
    CarbonCfgXtor* xtor = (CarbonCfgXtor*)qv.value<void*>();
    ui.tableTransactors->AddXtor(xtor);
  }
}

void Car2Cow::on_pushButtonDelXtor_clicked()
{
  ui.tableTransactors->DeleteXtor();
}


const char* Car2Cow::append(UtString& buf, const char* s1, const char* s2)
{
  buf.clear();
  if (s1)
    buf << s1;
  if (s2)
    buf << s2;
  return buf.c_str();
}

void Car2Cow::registerWidgets(const char* filename)
{
  UtString buf;
  mCQt->comboBox(append(buf, filename,"xtor_xtor_combobox"), ui.comboBoxXtor);
  mCQt->comboBox(append(buf, filename,"xtor_xtor_lib_combobox"), ui.comboBoxXtorLib);

  registerUniqueWidget(filename, new CQtPushButtonControl(ui.btnAdd, mCQt), "reg_button_add", false);
  registerUniqueWidget(filename, new CQtPushButtonControl(ui.btnDelete, mCQt), "reg_button_delete", false);
  registerUniqueWidget(filename, new CQtPushButtonControl(ui.btnAddField, mCQt), "reg_button_add_field", false);
  registerUniqueWidget(filename, new CQtPushButtonControl(ui.btnDeleteField, mCQt), "reg_button_delete_field", false);
  registerUniqueWidget(filename, new CQtPushButtonControl(ui.btnBindConstant, mCQt), "reg_button_bind_const", false);
  registerUniqueWidget(filename, new CQtPushButtonControl(ui.btnBindTo, mCQt), "reg_button_bind_to", false);
  registerUniqueWidget(filename, new CQtDragDropWidgetControl(ui.browserTree, mCQt), "reg_browser_tree", false);
  registerUniqueWidget(filename, new CQtDragDropWidgetControl(ui.browserList, mCQt), "reg_browser_list", false);
  registerUniqueWidget(filename, new CQtTableWidgetControl(ui.tableRegisters, mCQt), "reg_table_registers", false);
  registerUniqueWidget(filename, new CQtTableWidgetControl(ui.tableFields, mCQt), "reg_table_fields", false);
  registerUniqueWidget(filename, new CQtDialogControl(ui.tableFields->getConstantDialog(), mCQt), "reg_dialog_constant", false);
  registerUniqueWidget(filename, new CQtPushButtonControl(ui.pushButtonAddXtor, mCQt), "xtor_button_add", false);
  registerUniqueWidget(filename, new CQtPushButtonControl(ui.pushButtonDelXtor, mCQt), "xtor_button_delete", false);
  registerUniqueWidget(filename, new CQtPushButtonControl(ui.pushButtonBindByName, mCQt), "xtor_button_bindbyname", false);
  registerUniqueWidget(filename, new CQtPushButtonControl(ui.pushButtonBindAdv, mCQt), "xtor_button_bind_adv", false);
  registerUniqueWidget(filename, new CQtPushButtonControl(ui.pushButtonDeleteBinding, mCQt), "xtor_button_delbinding", false);

  registerUniqueWidget(filename, new CQtDragDropWidgetControl(ui.xtorPortTree, mCQt), "xtor_portselect_tree", false);
  registerUniqueWidget(filename, new CQtDragDropWidgetControl(ui.tableTransactorPorts, mCQt), "xtor_port_bindings", false);
  registerUniqueWidget(filename, new CQtDragDropWidgetControl(ui.tableTransactors, mCQt), "xtor_transactors", false);
  
  registerUniqueWidget(filename, new CQtDialogControl(ui.tableTransactorPorts->getAdvancedDialog(), mCQt), "reg_dialog_advanced", false);
}

void Car2Cow::tabChanged(int)
{
  ui.tableFields->FinishEdit();
}

void Car2Cow::setupToolbar(const char* filename)
{
  QAction* action = ui.mainToolBar->addAction(QIcon(":/Car2Cow/Resources/save.png"), tr("&Save"), this, SLOT(on_actionSave_triggered()));
  action->setShortcut(tr("Ctrl+S"));
  action->setStatusTip(tr("Save the document to disk"));
  
  UtString file;
  UtString dir;

  if (filename != NULL)
    OSParseFileName(filename, &dir, &file);

  if (filename == NULL)
    mCQt->registerAction(action, "save");
  else
  {
    UtString actName("save");
    actName << file;
    mCQt->registerAction(action, actName.c_str());
  }

  action = ui.mainToolBar->addAction(QIcon(":/Car2Cow/Resources/open.png"), tr("&Open"), this, SLOT(on_action_Open_triggered()));
  action->setShortcut(tr("Ctrl+O"));
  action->setStatusTip(tr("Open a new database"));

  if (filename == NULL)
    mCQt->registerAction(action, "open");
  else
  {
    UtString actName("open");
    actName << file;
    mCQt->registerAction(action, actName.c_str());
  }

  action = ui.mainToolBar->addAction(QIcon(":/Car2Cow/Resources/build.png"), tr("&Generate"), this, SLOT(on_pushButtonGenerate_clicked()));
  action->setShortcut(tr("Ctrl+B"));
  action->setStatusTip(tr("Generate code"));

  if (filename == NULL)
    mCQt->registerAction(action, "generate");
  else
  {
    UtString actName("generate");
    actName << file;
    mCQt->registerAction(action, actName.c_str());
  } 

  registerUniqueAction(ui.action_Open, "fileOpen", filename);
  registerUniqueAction(ui.actionSave, "fileSave", filename);
  registerUniqueAction(ui.actionExit, "fileExit", filename);
  registerUniqueAction(ui.actionSave_As, "fileSaveas", filename);
}

void Car2Cow::registerUniqueAction(QAction* action, const char* name, const char* filename)
{
  UtString actionName;
  if (filename == NULL)
    actionName << name;
  else
    actionName << name << filename;

  mCQt->registerAction(action, actionName.c_str());
}

void Car2Cow::registerUniqueWidget(const char* filename, CQtControl* control, const char* name, bool saveEvents)
{
  UtString actionName;
  if (filename == NULL)
    actionName << name;
  else
    actionName << name << filename;

  mCQt->registerWidget(control, actionName.c_str(), saveEvents);
}

void Car2Cow::on_pushButtonGenerate_clicked()
{
  save();
  ui.tabRegisters->setCurrentIndex((int)tabBUILD);
  generateCode();
}

void Car2Cow::on_actionExit_triggered()
{
  close();
}

void Car2Cow::on_actionSave_triggered()
{
  save();
}

void Car2Cow::on_actionSave_As_triggered()
{
  saveAs();
}

bool Car2Cow::save()
{
  if (m_ccfgName.length() == 0)
    return saveAs();
  else
  {
    m_cfgID->write(m_ccfgName.c_str());
    documentModified(false);
    return true;
  }
}

bool Car2Cow::saveAs()
{
  UtString filename;
  
  bool ret = mCQt->popupFileDialog(this, false, // readFile
                                   "Create a Carbon Configuration",
                                   "Carbon Configuration (*.ccfg)",
                                   &filename);
  if (ret)
  {
    m_ccfgName.clear();
    m_ccfgName << filename.c_str();
    m_cfgID->write(m_ccfgName.c_str());
    documentModified(false);
    setCurrentFile(filename.c_str());
    return true;
  }
  else
    return false;
}

void Car2Cow::on_action_Open_triggered()
{
  if (!maybeSave())
  {
    UtString filename, basename;
    if (mCQt->isPlaybackActive())
    {
      mCQt->getPlaybackFile(&filename);
    }
  }
  else
  {
    QString qFilename = QFileDialog::getOpenFileName(this, tr("Design Database"), "", tr("Database (*.db *.ccfg)"));
//    qDebug() << "Opening file: " << qFilename;

    if (qFilename.length() == 0)
      return;

#if 0
    if (mCQt->isRecordActive())
    {
      UtString f;
      f << qFilename;
        mCQt->recordFilename(f.c_str());
    }
#endif
    Car2Cow *existing = findMainWindow(qFilename);
    if (existing) 
    {
      existing->show();
      existing->raise();
      existing->activateWindow();
      return;
    }

    if (isUntitled && !isWindowModified()) 
    {
      loadFile(qFilename);
    }
    else
    {
      Car2Cow *other = new Car2Cow(mCQt, qFilename);
      if (other->isUntitled) 
      {
        delete other;
        return;
      }
      other->move(x() + 40, y() + 40);
      other->show();
    }
  }
}

void Car2Cow::loadFile(const QString& qFilename)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  UtString filename, basename;
  filename << qFilename;
  ParseResult parseResult = parseFileName(filename.c_str(), &basename);

  loadDesignFile(filename.c_str(), parseResult);

  if (mCQt->isRecordActive()) 
    mCQt->recordFilename(filename.c_str());

  if (eCcfg == parseResult)
    m_ccfgName = filename;

  if (m_pDB != NULL)
  {
    mContext->putDB(m_pDB);
    mContext->putCfg(m_cfgID);

    carbonCfgPutCcfgMode(m_cfgID, eCarbonCfgCOWARE);

    setCurrentFile(filename.c_str());
    ui.browserTree->Initialize(eCarbonDBHierarchy);
    ui.browserList->Initialize(eCarbonDBHierarchy);
    ui.browserList->Connect(ui.browserTree);

    ui.browserTreeMem->Initialize(eCarbonDBHierarchy);
    ui.browserListMem->Initialize(eCarbonDBHierarchy);
    ui.browserListMem->Connect(ui.browserTreeMem);


    //ui.browserTreeXtor->Initialize();
    //ui.browserListXtor->Initialize();
    //ui.browserListXtor->Connect(ui.browserTreeXtor);

    ui.treeWidgetMem->Initialize(mContext);
  }

  QApplication::restoreOverrideCursor();
}


void Car2Cow::fieldSelectionChanged(CarbonCfgRegisterField* /*field*/)
{
  bool fieldSelected = ui.tableFields->selectedItems().count() > 0;
  ui.btnBindTo->setEnabled(fieldSelected && mSelectedNode != NULL);
  ui.btnBindConstant->setEnabled(fieldSelected);
  ui.btnDelete->setEnabled(fieldSelected);
  ui.btnDeleteField->setEnabled(fieldSelected);
}

void Car2Cow::registerSelectionChanged(CarbonCfgRegister* reg)
{
  mReg = reg;
  ui.tableFields->Initialize(mReg);
  ui.btnAddField->setEnabled(mReg != NULL);
  ui.btnDelete->setEnabled(mReg != NULL);
}

void Car2Cow::xtorSelectionChanged(CarbonCfgXtorInstance* xinst)
{
  mXtorInst = xinst;
  ui.pushButtonDelXtor->setEnabled(xinst != NULL);
  ui.tableTransactorPorts->Select(xinst);
  ui.pushButtonBindByName->setEnabled(xinst != NULL);
  ui.pushButtonBindAdv->setEnabled(xinst != NULL);
  ui.xtorPortTree->Refresh();
}

void Car2Cow::xtorBindingSelectionChanged(CarbonCfgXtorConn* xconn)
{
  ui.pushButtonDeleteBinding->setEnabled(mXtorInst != NULL && xconn != NULL);
}

void Car2Cow::selectionChanged(CarbonDBNode* node)
{
  mSelectedNode = node;
  bool fieldSelected = ui.tableFields->selectedItems().count() > 0;

  ui.btnBindTo->setEnabled(fieldSelected && mSelectedNode != NULL);

  const char* path = carbonDBNodeGetFullName(m_pDB, node);
  INFO_ASSERT(path, "Unable to find path for selected node");

  ui.browserList->putDragData(path);
}

void Car2Cow::memSelectionChanged(CarbonDBNode* node)
{
  mMemorySelectedNode = node;

  const char* path = carbonDBNodeGetFullName(m_pDB, node);

  INFO_ASSERT(path, "Unable to find path for selected node");

  ui.browserListMem->putDragData(path);
}

void Car2Cow::memSelectionChanged(CarbonCfgMemory* mem)
{
  ui.btnDeleteMem->setEnabled(mem != NULL);
}

Car2Cow::~Car2Cow()
{
  if (m_pReg != NULL)
    carbonAbstractRegisterDestroy(m_pReg);

  if (m_pDB != NULL)
    carbonDBFree(m_pDB);

  delete mContext;
}

void Car2Cow::warning(QWidget* widget, const QString& msg) {
  
  bool do_message_box = !(mCQt->isPlaybackActive() || mCQt->isRecordActive());
  UtIO::cerr() << msg << UtIO::endl;
  UtIO::cerr().flush();
  if (do_message_box) {
    QMessageBox::warning(widget, tr(APP_NAME), msg);
  }
}

eCarbonMsgCBStatus Car2Cow::sMsgCallback(CarbonClientData clientData,
                                         CarbonMsgSeverity severity,
                                         int number, const char* text,
                                         unsigned int)
{
  Car2Cow* mw = (Car2Cow*) clientData;
  const char* severityStr = "";
  switch (severity) {
  case eCarbonMsgStatus:   severityStr = "Status"; break;
  case eCarbonMsgNote:     severityStr = "Note"; break;
  case eCarbonMsgWarning:  severityStr = "Warning"; break;
  case eCarbonMsgError:    severityStr = "Error"; break;
  case eCarbonMsgFatal:    severityStr = "Fatal"; break;
  case eCarbonMsgSuppress: severityStr = "Suppress"; break;
  case eCarbonMsgAlert:    severityStr = "Alert"; break;
  }

  UtString buf;
  buf << severityStr << " " << number << ": " << text;

  mw->warning(mw, buf.c_str());
  return eCarbonMsgContinue;
}

bool Car2Cow::loadDesignFile(const char* filename, ParseResult parseResult) 
{
  bool ret = false;
  if ((parseResult == eFullDB) || (parseResult == eIODB)) {
    ret = loadDatabase(filename, true);
  }
  if (parseResult == eCcfg) {
    ret = loadConfig(filename);
  }  
  return ret;

  setCurrentFile(filename);

}

Car2Cow::ParseResult Car2Cow::parseFileName(const char* filename,
                                            UtString* baseName)
{
  *baseName = filename;
  UInt32 sz = strlen(filename);

  ParseResult parseResult = eNoMatch;
  baseName->clear();

  if ((sz > 6) && (strcmp(filename + sz - 6, ".io.db") == 0)) {
    parseResult = eIODB;
    baseName->append(filename, sz - 6);
  }
  else if ((sz > 10) && (strcmp(filename + sz - 10, ".symtab.db") == 0)) {
    parseResult = eFullDB;
    baseName->append(filename, sz - 10);
  }
  else if ((sz > 5) && (strcmp(filename + sz - 5, ".ccfg") == 0)) {
    parseResult = eCcfg;
    baseName->append(filename, sz - 5);
  }
  return parseResult;
} 


bool Car2Cow::loadDatabase(const char* iodbName, bool initPorts)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  bool success = true;
  if (m_cfgID->getDB() == NULL)
  {
    SInt32 key;
    RANDOM_SEEDGEN_1(key, "PLAT", 4);

    CarbonDB* newDB = carbonDBPlatArchInit(key, iodbName, false);

    if (newDB == NULL) 
    {
      QApplication::restoreOverrideCursor();
      return false;
    }
    m_pDB = newDB;
    m_cfgID->putDB(m_pDB);  // Ownership transferred to CarbonCfg
  }
  else
    m_pDB = m_cfgID->getDB(); 

  mContext->putDB(m_pDB);
  mContext->putCfg(m_cfgID);

  UtString baseFile, drivers, errmsg;
  parseFileName(iodbName, &baseFile);

  if (initPorts) 
  {
    UtString libFile(baseFile);
#ifdef Q_OS_WIN
    libFile += ".dll";
#else
    libFile += ".so";
#endif
    carbonCfgPutLibName(m_cfgID, libFile.c_str());
  }

  m_pPortView->Populate(mContext, initPorts);

  ui.tableRegisters->Populate();
  ui.treeWidgetMem->Populate(mContext, initPorts);
  ui.tableTransactors->Populate(mContext, initPorts);
  ui.xtorPortTree->Populate(mContext, initPorts);

  if (initPorts)
    m_pPortView->Update();

  carbonCfgPutIODBFile(m_cfgID, iodbName);

  if (initPorts)
  {
    carbonCfgPutCompName(m_cfgID, carbonDBGetTopLevelModuleName(m_pDB));
    carbonCfgPutTopModuleName(m_cfgID, carbonDBGetTopLevelModuleName(m_pDB));
  }

  QApplication::restoreOverrideCursor();

  return success;
}

void Car2Cow::populateXtorLibComboBox()
{
  QComboBox* xtorLibBox = ui.comboBoxXtorLib;
  xtorLibBox->clear();
  xtorLibBox->setMinimumContentsLength(0);
  for (CarbonCfg::XtorLibs::SortedLoop l = m_cfgID->loopXtorLibs(); !l.atEnd(); ++l) {
    const UtString& libName = l.getKey();
    CarbonCfgXtorLib* xlib = l.getValue();
    xtorLibBox->addItem(libName.c_str());
  }
  xtorLibBox->setCurrentIndex(0);
}

void Car2Cow::xtorLibraryChanged(int)
{
  UtString lib;
  lib << ui.comboBoxXtorLib->currentText();
  
  const char* libName = lib.c_str();
  
   populateXtorComboBox(libName);
}

void Car2Cow::populateXtorComboBox(const char*)
{
  QComboBox* xtorCbox = ui.comboBoxXtor;
  
  xtorCbox->clear();
  xtorCbox->setMinimumContentsLength(0);

  UtString libName;
  libName << ui.comboBoxXtorLib->currentText();
  UInt32 maxChars = 0;
  CarbonCfgXtorLib* xlib = m_cfgID->findXtorLib(libName.c_str());
  if (xlib)
  {
    for (UInt32 i = 0, n = xlib->numXtors(); i < n; ++i)
    {
      CarbonCfgXtor* xtor = xlib->getXtor(i);
      UtString libraryName;
      libraryName << xtor->getLibraryName();

      if (0 == strcmp(libraryName.c_str(), libName.c_str()))
      {
        UtString name;
        name << xtor->getName();
        QVariant qv = qVariantFromValue((void*)xtor);
        if (name.length() > maxChars)
          maxChars = name.length();
        xtorCbox->addItem(name.c_str(), qv);
      }
    }
    xtorCbox->setMinimumContentsLength(maxChars);
  }
}

bool Car2Cow::loadConfig(const QString &qfileName) {
  UtString fileName;
  fileName << qfileName;
  QApplication::setOverrideCursor(Qt::WaitCursor);
  carbonCfgClear(m_cfgID);
  CarbonCfgStatus cfgStatus = carbonCfgRead(m_cfgID, fileName.c_str());
  bool success = cfgStatus != eCarbonCfgFailure;
  QApplication::restoreOverrideCursor();

  if (success) 
  {
    // Read in the IODB file to populate the debug tree
    const char* iodbFile = carbonCfgGetIODBFile(m_cfgID);
    success = loadDatabase(iodbFile, false);
    if (success) 
    {
      m_pPortView->Update();
    }
  }
  else 
  {
    warning(this, tr("Cannot read file %1:\n%2.")
      .arg(qfileName)
      .arg(carbonCfgGetErrmsg(m_cfgID)));
  }

  QApplication::restoreOverrideCursor();

  return success;
}

void Car2Cow::on_btnBindTo_clicked()
{
   ui.tableFields->BindField(mSelectedNode);
}

void Car2Cow::on_btnBindConstant_clicked()
{
   ui.tableFields->BindConstant();
}

void Car2Cow::on_btnDeleteField_clicked()
{
  ui.tableFields->DeleteField();
}

void Car2Cow::on_btnAddField_clicked()
{
  ui.tableFields->AddField();
}

void Car2Cow::on_btnAdd_clicked()
{
  ui.tableRegisters->AddRegister();
}

void Car2Cow::on_btnDeleteMem_clicked()
{
  ui.treeWidgetMem->DeleteMemory();
}

void Car2Cow::on_btnDelete_clicked()
{
  ui.tableRegisters->DeleteRegister();
}

void Car2Cow::closeEvent(QCloseEvent *event)
{
  mCQt->recordExit();

  if (maybeSave())
  {
    mCQt->dumpStateHierarchy();
    event->accept();
  }
  else 
    event->ignore();
}

bool Car2Cow::maybeSave()
{
  bool status = true;

  if (isWindowModified())
  {
    int ret = QMessageBox::warning(this, tr(APP_NAME),
                                   tr("The configuration has been modified.\n"
                                      "Do you want to save your changes?"),
                                   QMessageBox::Yes | QMessageBox::Default,
                                   QMessageBox::No,
                                   QMessageBox::Cancel | QMessageBox::Escape);
    if (ret == QMessageBox::Yes) {
      status = save();
    }
    else if (ret == QMessageBox::Cancel) {
      status = false;
    }
  }

  return status;
}

QString Car2Cow::strippedName(const QString &fullFileName)
{
  return QFileInfo(fullFileName).fileName();
}
Car2Cow *Car2Cow::findMainWindow(const QString &fileName)
{
  QString canonicalFilePath = QFileInfo(fileName).canonicalFilePath();

  foreach (QWidget *widget, qApp->topLevelWidgets()) 
  {
    Car2Cow *mainWin = qobject_cast<Car2Cow *>(widget);
    if (mainWin && mainWin->curFile == canonicalFilePath)
      return mainWin;
  }
  return 0;
}

void Car2Cow::documentModified(bool value)
{
  setWindowModified(value);
}
void Car2Cow::documentWasModified()
{
  setWindowModified(true);
}


void Car2Cow::generateCode()
{
  mCodegen->Generate(ui.textEdit);
}

void Car2Cow::updateTabs()
{
  ui.tableFields->FinishEdit();
}


