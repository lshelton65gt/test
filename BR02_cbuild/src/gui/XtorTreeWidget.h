#ifndef XTORTreeWidget_H
#define XTORTreeWidget_H
#include "gui/CDragDropTreeWidget.h"


#include <QTreeWidget>
#include <QtGui>
#include <QObject>

#include "ui_XtorTreeWidget.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "cfg/CarbonCfg.h"
#include "util/UtWildcard.h"

#include "ApplicationContext.h"

class XtorTreeWidget : public CDragDropTreeWidget
{
  Q_OBJECT

  enum Column {colINSTNAME=0, colLIBRARY, colXTORNAME};

public:
  XtorTreeWidget(QWidget *parent = 0);
  ~XtorTreeWidget();
  void putContext(ApplicationContext* ctx);
  void Populate(ApplicationContext* ctx, bool initPorts);
  QTreeWidgetItem* AddXtor(CarbonCfgXtor* xtor);
  void DeleteXtor();

private:
  QTreeWidgetItem* updateRow(int row, CarbonCfgXtorInstance* xinst, bool clearSelect);
  QTreeWidgetItem* getItem(int row, Column col);
  void buildSelectionList(QList<QTreeWidgetItem*>& list);
  void deleteXtorInst(QTreeWidgetItem* item, CarbonCfgXtorInstance* xinst);

private slots:
  void itemSelectionChanged();

private:
  Ui::XtorTreeWidgetClass ui;
  ApplicationContext* mCtx;

signals:
  void xtorSelectionChanged(CarbonCfgXtorInstance* xinst);

  friend class XtorTreeEditDelegate;
};

class XtorTreeEditDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    XtorTreeEditDelegate(QObject *parent = 0, XtorTreeWidget* tree=0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
        const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const;
private slots:
    void commitAndCloseEditor();

private:
  XtorTreeWidget* xtorTree;
};

#endif // XTORTreeWidget_H
