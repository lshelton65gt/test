/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CQt.h"
#include "util/UtIOStream.h"
#include "gui/CQtRadioButtonControl.h"
#include "util/CarbonAssert.h"  // move to CQt.h?

CQtRadioButtonControl::CQtRadioButtonControl(QRadioButton* pb, CQtContext* cqt):
  mRadioButton(pb),
  mCQt(cqt)
{
  CQT_CONNECT(pb, toggled(bool), this, toggled(bool));
}

void CQtRadioButtonControl::dumpState(UtOStream& stream)
{
  stream << mRadioButton->text() << "\n";
}

void CQtRadioButtonControl::getValue(UtString* str) const {
  *str = mRadioButton->isChecked() ? "on" : "off";
}

bool CQtRadioButtonControl::putValue(const char* val, UtString* /*errmsg*/ ) {
  bool checked = strcmp(val, "off") != 0;
  mRadioButton->setChecked(checked);
  return true;
}

QWidget* CQtRadioButtonControl::getWidget() const {
  return mRadioButton;
}

void CQtRadioButtonControl::toggled(bool checked) {
  mCQt->recordPutValue(mRadioButton, checked ? "on" : "off");
}
