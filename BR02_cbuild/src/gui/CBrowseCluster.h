// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef CBROWSE_CLUSTER_H
#define CBROWSE_CLUSTER_H

#include "shell/carbon_dbapi.h"
#include <QGraphicsItem>
#include <QObject>
#include "iodb/CGraph.h"

class CBrowse;
class CBrowseButton;
class Driver;

//! visual representation of a cluster of combinational nodes.
/*!
  This works like a CBrowseNode with a fanin button and a fanout buton.
  When the fanin button is first pressed, then CBrowseNodes are created for
  the nodes that are topologically adjacent to the fanin node.  If it's
  pressed again, then the nodes that are 2 steps from the fanin are shown.

  There are also buttons that pull the fanin & fanout back into the cluster.
*/
class CBrowseCluster : public QObject, public QGraphicsItem
{
  Q_OBJECT
    
  enum ButtonID {
    eFaninExpand,
    eFaninCollapse,
    eFanoutExpand,
    eFanoutCollapse,
    eNumButtons = 4
  };

public:
  CBrowseCluster(CBrowse* browser, CGraph::Cluster*);
  ~CBrowseCluster();
  
  QRectF boundingRect() const;
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
             QWidget *widget);
  void layout();
  UInt32 getIndex() const {return mIndex;}
  void putIndex(UInt32 idx) {mIndex = idx;}
  
  double getWidth() const {return mBoundingRect.width();}
  double getHeight() const {return mBoundingRect.height();}
  void mark();

  CBrowseButton* findButton(CGraph::Cluster* cluster);

  // To facilitate undo/redo navigation, supply a state object
  // which indicates the values of all the buttons
  class State {
  public:
    CARBONMEM_OVERRIDES

    friend class CBrowseCluster;
    void restore() const {mCluster->restoreState(*this);}
    CBrowseCluster* getCluster() const {return mCluster;}
  private:
    CBrowseCluster* mCluster;
    CBrowseButton::State mButtonState[4];
    QPointF mPos;
  };

  void saveState(State* state);
  void restoreState(const State& state);

protected:
/*
  void mousePressEvent(QGraphicsSceneMouseEvent *event);
  void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
  void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
  void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
*/
  
private:
  void drawText(QPainter *painter, const char* str, double x, double y,
                double* width, double* height);
  void clearButtons();

  CBrowse* mBrowser;
  CarbonDB* mDB;
  CGraph::Cluster mCGCluster;
  QRectF mBoundingRect;
  UInt32 mIndex;
  double mTextStart;
  double mTextEnd;

  CBrowseButton* mButtons[4];   // fanin expand, fanin collapse, 
};

#endif
