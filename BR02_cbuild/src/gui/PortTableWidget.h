// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef __PORTTABLEWIDGET_H__
#define __PORTTABLEWIDGET_H__

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"

#include <QObject>
#include <QTableWidget>
#include <QtGui>

class TieOffTableWidget;
class DisconnectTableWidget;

#include "ApplicationContext.h"

class PortTableWidget : public QTableWidget
{
   Q_OBJECT

private:
  enum Column {colESLPORT=0, colWIDTH, colDIRECTION, colTYPE, colRTLMODE, colNAME};

public:
  PortTableWidget(QWidget* parent);
  virtual ~PortTableWidget();

  void Populate(ApplicationContext* ctx, bool initPorts);
  void Update(ApplicationContext* ctx, TieOffTableWidget* tieOffTableWidget, DisconnectTableWidget* disconnectTable);
  void CreateESLPort(CarbonCfgRTLPortID rtlPort);
  
private:
  static void computeMode(CarbonDB* db, const CarbonDBNode* node, CarbonCfgESLPort* eslPort);
  void initialize();
  void createActions();
  void populateLoop(CarbonDB* db, CarbonCfgRTLPortType type,
                    CarbonDBNodeIter* iter);
  CarbonCfgESLPortID addESLConnection(CarbonCfgRTLPortID rtlPort,
                                                    CarbonCfgESLPortType type);
  CarbonCfgESLPortID addConnection(CarbonCfgRTLPortID rtlPort);
  void removeConnection(CarbonCfgRTLConnection* conn, bool redraw, int idx = -1);
  bool isRealRTLPort(CarbonCfgRTLPort* port);
  CarbonCfgTie* getTie(CarbonCfgRTLPort* port);
  const char* getSystemCType(CarbonCfgRTLPort* port);
  void addESLPort(CarbonCfgRTLPort* port);
  void removeESLPort(CarbonCfgRTLPortID rtlPort);
  void tieOffSelected(UInt32 value);
  void buildSelectionList(QList<QTableWidgetItem*>& list);

  friend class PortTableEditDelegate;

protected:
  virtual void mousePressEvent(QMouseEvent* mouseEvent);
  virtual void mouseMoveEvent(QMouseEvent* mouseEvent);
 
private slots:
  void showContextMenu(const QPoint &pos);
  void menuTieToZero();
  void menuTieToOne();
  void menuDisconnect();

private:
  CarbonDB* mDB;
  CarbonCfgID mCfg;
  ApplicationContext* mCtx;

  TieOffTableWidget* m_pTieOffTable;
  DisconnectTableWidget* m_pDisconnectTable;

  QAction* actionTieToOne;
  QAction* actionTieToZero;
  QAction* actionDisconnect;

  QPoint mPressPos;
};

class PortTableEditDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    PortTableEditDelegate(QObject *parent = 0, PortTableWidget* table=0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
        const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const;
 
private slots:
    void commitAndCloseEditor();
    void commitAndCloseComboBox(int);

private:
  PortTableWidget* portTable;
};

#endif

