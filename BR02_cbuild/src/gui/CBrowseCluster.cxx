//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "CBrowseCluster.h"

CBrowseCluster::CBrowseCluster(CBrowse* browser, CGraph::Cluster*) {
  for (UInt32 i = 0; i < eNumButtons; ++i) {
  }
}

CBrowseCluster::~CBrowseCluster() {
}

QRectF CBrowseCluster::boundingRect() const {
  return mBoundingRect;
}

void CBrowseCluster::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                           QWidget *widget)
{
}

void CBrowseCluster::layout() {
}

void CBrowseCluster::mark() {
}

CBrowseButton* CBrowseCluster::findButton(CGraph::Node* node) {
  if ((node == mCGCluster->getFanin()) || mCGCluster->isChild(node)) {
    return mButtons[eFaninExpand];
  }
  return mButtons[eFanoutExpand];
}

void CBrowseCluster::saveState(State* state) {
}

void CBrowseCluster::restoreState(const State& state) {
}

/*
void CBrowseCluster::mousePressEvent(QGraphicsSceneMouseEvent *event);
void CBrowseCluster::mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
void CBrowseCluster::hoverEnterEvent(QGraphicsSceneHoverEvent *event);
void CBrowseCluster::hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
*/

void CBrowseCluster::drawText(QPainter *painter, const char* str,
                              double x, double y,
                              double* width, double* height)
{
}

void CBrowseCluster::clearButtons() {
}
