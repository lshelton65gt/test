/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// This widget was derived from from Qwt's 'cpuplot' example.

#include "gui/CQt.h"
#include <qapplication.h>
#include <qlayout.h>
#include <qlabel.h>
#include <qpainter.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_curve.h>
#include <qwt_scale_draw.h>
#include <qwt_scale_widget.h>
#include <qwt_legend.h>
#include <qwt_legend_item.h>
#include <qwt_symbol.h>

#include "ReplayPerfPlot.h"
#include "util/UtDoublePointArray.h"

#define INITIAL_MAX_CPS 1000
#define INITIAL_SCHED_CALLS 1000
#define MIN_HEIGHT 50
#define INIT_HEIGHT 260
  
//! class to represent the background coloring on a replay performance plot
class Background: public QwtPlotItem {
public:
  Background(ReplayPerfPlot* replayPerfPlot, double max_x, double max_y) :
    mMaxX(max_x), mMaxY(max_y), mReplayPerfPlot(replayPerfPlot),
    mIsFinished(false)
  {
    setZ(0.0);
  }
  
  virtual int rtti() const {
    return QwtPlotItem::Rtti_PlotUserItem;
  }
  
  virtual void draw(QPainter *painter,
                    const QwtScaleMap &xMap, const QwtScaleMap &,
                    const QRect &rect) const
  {
    QColor c(Qt::white);
    QRect r = rect;
    CarbonReplayEventType type = eReplayEventExit;

    mPrevX = -1;
    
    for (UInt32 i = 0, n = mEvents.size(); i < n; ++i) {
      CarbonReplayEventType ntype = mEvents[i]->mType;
      double dx = mReplayPerfPlot->eventX(mEvents[i]);
      int x = xMap.transform(dx);

      if (ntype == eReplayEventCheckpoint) {
        draw_range(painter, rect, x - 1, type);
        draw_range(painter, rect, x, ntype);
      }
      else if (ntype != type) {
        draw_range(painter, rect, x, type);
        type = ntype;
      }
    }
    int x = xMap.transform(mMaxX);
    draw_range(painter, rect, x, type);
  } // virtual void draw

  void draw_range(QPainter* painter, QRect rect, 
                  int x, CarbonReplayEventType type) const
  {
    if (mPrevX < x) {
      rect.setLeft(mPrevX + 1);
      rect.setRight(x);
      mPrevX = x;
      QColor color;
      if (mIsFinished) {
        switch (type) {
        case eReplayEventCheckpoint:   color = QColor(Qt::black);     break;
        case eReplayEventRecord:       color = QColor(170,0,0);       break;
        case eReplayEventPlayback:     color = QColor(0,150,0);       break;
        case eReplayEventRecover:      color = QColor(Qt::yellow);    break;
        case eReplayEventNormal:
        case eOnDemandEventDisabled:
        case eReplayEventExit:         color = QColor(Qt::lightGray); break;
        case eOnDemandEventNotIdle:    color = QColor(0,180,180);     break;
        case eOnDemandEventIdle:       color = QColor(180,0,180);     break;
        default: break;
        }
      }
      else {
        switch (type) {
        case eReplayEventCheckpoint:   color = QColor(Qt::black);     break;
        case eReplayEventRecord:       color = QColor(Qt::red);       break;
        case eReplayEventPlayback:     color = QColor(Qt::green);     break;
        case eReplayEventRecover:      color = QColor(Qt::yellow);    break;
        case eReplayEventNormal:
        case eOnDemandEventDisabled:
        case eReplayEventExit:         color = QColor(Qt::lightGray); break;
        case eOnDemandEventNotIdle:    color = QColor(Qt::cyan);      break;
        case eOnDemandEventIdle:       color = QColor(Qt::magenta);   break;
        default: break;
        }
      }
      painter->fillRect(rect, color);
    }
  }

  void putMaxXY(double x, double y) {
    mMaxX = x;
    mMaxY = y;
  }

  void clearEvents() {mEvents.clear();}
  void addEvent(CarbonReplaySystem::Event* event) {mEvents.push_back(event);}

  void putIsFinished(bool finished) {mIsFinished = finished;}

private:
  double mMaxX;
  double mMaxY;
  UtArray<CarbonReplaySystem::Event*> mEvents;
  ReplayPerfPlot* mReplayPerfPlot;
  mutable int mPrevX;
  bool mIsFinished;
}; // class Background: public QwtPlotItem

//! CpuCurve class
/*!
 *! This class maintains two arrays of doubles in the format required
 *! by QwtPlotCurve::setRawData.  We can't use UtArray for this
 *! because it has not (yet) implemented value semantics supporting
 *! data that's more than 4 bytes.  We can't use UtVector for this
 *! because std::vector has linking issues on the cross-compiled
 *! Windows for us.  So we maintain these arrays with raw alloc/free
 *! calls and explicit capacity.
 *!
 *! The best solution to clean this up is probably to give UtArray
 *! value-semantics, following the code in UtHashSet.  But for now
 *! we must need to implement this properly.
 *!
 */
class CpuCurve: public QwtPlotCurve {
public:
  //! ctor
  CpuCurve(const QString& title) :
    QwtPlotCurve(title)
  {
#if QT_VERSION >= 0x040000
    //setRenderHint(QwtPlotItem::RenderAntialiased);
#endif
  }
  
  //! dtor
  ~CpuCurve() {
  }

  //! set the color of this curve
  void setColor(const QColor &color)
  {
#if QT_VERSION >= 0x040000
    QColor c = color;
    //c.setAlpha(150);
    
    setPen(c);
    setBrush(c);
#else
    setPen(color);
    setBrush(QBrush(color, Qt::Dense4Pattern));
#endif
  }

  void push_back(double x, double y) {
    // This assertion fired at Broadcom.  Somehow they wound up
    // with a file with negative CPU time recorded.  I don't
    // understand how this can occur; it doesn't happen in our
    // tests.  But I guess we are depending on an underlying
    // system call to produce monotonically increasing values.
    // If that fails we shouldn't crash.  So we will just let
    // the graphics look a little confusing when that occurs
    // rather than crashing.
    // 
    // if (mNumPoints > 0) {
    //   INFO_ASSERT(x >= mXData[mNumPoints - 1], "x insanity");
    // }
    mPoints.push_back(x, y);
  }

  //! clear the curve
  void clear() {
    mPoints.clear();
  }

  //! remove a point from this curve
  void pop_back() {
    mPoints.pop_back();
  }

  bool empty() const {return mPoints.empty();}

  double lastX() {return mPoints.lastX();}
  double lastY() {return mPoints.lastY();}

  //! graphically render the point-array buffers, copying them into the base class curve
  void finalize() {
    setRawData(mPoints.getXData(), mPoints.getYData(), mPoints.size());
  }

  UtDoublePointArray mPoints;
};

CpuCurve* ReplayPerfPlot::addLegend(const char* name, QwtSymbol::Style style,
                                    int x, int y, const QColor& color)
{
  QwtSymbol block;
  block.setStyle(style);
  block.setSize(QSize(x, y));
  block.setBrush(QBrush(color));
  block.setPen(color);

  CpuCurve* curve = new CpuCurve(name);
  curve->setSymbol(block);
  curve->setColor(color);
  curve->attach(this);

  return curve;
}

ReplayPerfPlot::ReplayPerfPlot(CarbonReplaySystem* rs, QWidget *parent):
  REPLAY_PERF_PLOT_SUPERCLASS(parent),
  mXAxis(eCycles),
  mReplaySystem(rs),
  mSimUnit(eUSchedCalls)
{
  setAutoReplot(false);

  plotLayout()->setAlignCanvasToScales(true);
  
  QwtLegend* right_legend = new QwtLegend;
  insertLegend(right_legend, QwtPlot::RightLegend);
  
  setAxisScale(QwtPlot::xBottom, 0, INITIAL_SCHED_CALLS);
  mXAxisMax = INITIAL_SCHED_CALLS;
  setAxisLabelAlignment(QwtPlot::xBottom, Qt::AlignLeft | Qt::AlignBottom);
  
  /*
    In situations, when there is a label at the most right position of the
    scale, additional space is needed to display the overlapping part
    of the label would be taken by reducing the width of scale and canvas.
    To avoid this "jumping canvas" effect, we add a permanent margin.
    We don't need to do the same for the left border, because there
    is enough space for the overlapping label below the left scale.
  */
  
  QwtScaleWidget *scaleWidget = axisWidget(QwtPlot::xBottom);
  const int fmh = QFontMetrics(scaleWidget->font()).height();
  scaleWidget->setMinBorderDist(0, fmh / 2);
  
  mTimeUnits = rs->getSimTimeUnits();
  resetYAxisTitle();
  setAxisScale(QwtPlot::yLeft, 0, INITIAL_MAX_CPS);
  mYAxisMax = INITIAL_MAX_CPS;
  
  mBackground = new Background(this, INITIAL_SCHED_CALLS, INITIAL_MAX_CPS);
  mBackground->attach(this);
  
  // mPerformance has the performance data.
  //
  // Most these curves have no data.  They are only there
  // to populate the legend with the background color mapping.  The
  // mode-changes seem to work much better as a background, for 2
  // reasons:
  //   - during recover the performance is 0, so there would be no area
  //     to draw in yellow
  //   - we can't draw the checkpoints using curve symbols because
  //     the symbols have to stay active for the entire curve, and
  //     we don't want to make it look like there were checkpoints
  //     at every mode-change or update call.
  // However we set up a Rect symbol so that the color is clearer on
  // the legend
  mPerformance =
    addLegend("Performance", QwtSymbol::NoSymbol,  12, 12, QColor(Qt::blue));
  addLegend("Normal",        QwtSymbol::Rect,      12, 12, QColor(Qt::lightGray));
  addLegend("Replay Record",        QwtSymbol::Rect,      12, 12, QColor(Qt::red));
  addLegend("Replay Playback",      QwtSymbol::Rect,      12, 12, QColor(Qt::green));
  addLegend("Replay Recover",       QwtSymbol::Rect,      12, 12, QColor(Qt::yellow));
  addLegend("Replay Checkpoint",    QwtSymbol::Rect,      12, 12, QColor(Qt::black));
  addLegend("OnDemand Not Idle",    QwtSymbol::Rect,      12, 12, QColor(Qt::cyan));
  addLegend("OnDemand Idle",        QwtSymbol::Rect,      12, 12, QColor(Qt::magenta));

  setMinimumHeight(MIN_HEIGHT);
} // ReplayPerfPlot::ReplayPerfPlot

ReplayPerfPlot::~ReplayPerfPlot() {
}

void ReplayPerfPlot::resetYAxisTitle() {
  switch (mSimUnit) {
  case eUCycles:   setAxisTitle(QwtPlot::yLeft, "Cycles Per Second"); break;
  case eUSimTime: {
    UtString buf;
    buf << "Sim Time (" << mTimeUnits << ") per Sec";
    setAxisTitle(QwtPlot::yLeft, buf.c_str());
    break;
  }
  default:         setAxisTitle(QwtPlot::yLeft, "Sched Per Second"); break;
  }
}

double ReplayPerfPlot::eventX(CarbonReplaySystem::Event* event) {
  switch (mXAxis) {
  case eRealTime:   return event->mReal;
  case eCycles:     return event->mCycles;
  case eSchedCalls: return event->mTotalSchedCalls;
  case eSimTime:    return event->mSimTime;
  case eCPUTime:    return eventCpu(event);
  }
  return 0.0;
}

double ReplayPerfPlot::eventCpu(CarbonReplaySystem::Event* event) {
  return event->mUser + event->mSys;
}

void ReplayPerfPlot::update() {
  SimUnit new_sim_unit = eUSchedCalls;
  if (mReplaySystem->isCycleCountSpecified()) {
    new_sim_unit = eUCycles;
  }
  else if (mReplaySystem->isSimTimeSpecified()) {
    new_sim_unit = eUSimTime;
  }
  UtString time_units = mReplaySystem->getSimTimeUnits();
  if ((new_sim_unit != mSimUnit) || (time_units != mTimeUnits)) {
    mSimUnit = new_sim_unit;
    mTimeUnits = time_units;
    resetYAxisTitle();
  }

  // Walk the checkpoints and find max CPS
  double maxCPS = (double) INITIAL_MAX_CPS;
  SInt32 n = mReplaySystem->numEvents();

  mBackground->clearEvents();
  double prev_x = -1.0;
  double last_x = INITIAL_SCHED_CALLS;

  mPerformance->clear();
  double cyclesPerSecond = 0;
  double prevCyclesPerSecond = -1.0;
  CarbonReplaySystem::Event* last_mode_change = NULL;

  // consider this scenario:
  //     event type  # cycles  cpu time
  //     ----------  --------  --------
  //     ....
  //     checkpoint  180k      1.34
  //     update      225k      1.56
  //     update      240k      1.64
  //     recover     240k      1.68
  //     normal      240k      1.86
  //
  // with cpu time on the x axis, we want to draw green from 1.34 to 1.68,
  // yellow from 1.68 to 1.86, and blue from 1.86 on.
  // Note that the instantaneous cyclesPerSecond cannot be updated on every
  // cycle because when there is insufficient cpu-time delta.  So we keep
  // a trailing cpu-time marker just for comparing CPU time
  for (SInt32 i = 0; i < n; ++i) {
    CarbonReplaySystem::Event* event = mReplaySystem->getEvent(i);

    // Calculate instantaneous CPU time by walking backward from i
    // till we get a non-zero cpu-delta.
    for (SInt32 j = i - 1; j >= 0; --j) {
      CarbonReplaySystem::Event* cpuMarker = mReplaySystem->getEvent(j);
      double deltaUserTime = eventCpu(event) - eventCpu(cpuMarker);
      if (deltaUserTime >= 0.1) {
        switch (mSimUnit) {
        case eUCycles: {
          double deltaCycles = (double) event->mCycles -
            (double) cpuMarker->mCycles;
          cyclesPerSecond = deltaCycles / deltaUserTime;
          break;
        }
        case eUSimTime: {
          double deltaSimTime = (double) event->mSimTime -
            (double) cpuMarker->mSimTime;
          cyclesPerSecond = deltaSimTime / deltaUserTime;
          break;
        }
        default: {
          double deltaSchedCalls = (double) event->mTotalSchedCalls -
            (double) cpuMarker->mTotalSchedCalls;
          cyclesPerSecond = deltaSchedCalls / deltaUserTime;
          break;
        }
        }
        maxCPS = std::max(maxCPS, cyclesPerSecond);
        break;
      } // if
    } // for

    last_x = eventX(event);
    if ((last_x != prev_x) || (cyclesPerSecond != prevCyclesPerSecond)) {
      mPerformance->push_back(last_x, cyclesPerSecond);
      prev_x = last_x;
      prevCyclesPerSecond = cyclesPerSecond;
    }

    // at the point where recovery starts, there is an interesting CPU
    // period.  But we should immediately record a transition to 0 CPS
    if (event->mType == eReplayEventRecover) {
      mPerformance->push_back(last_x, 0);
    }

    // copy events that indicate a mode-change or checkpoint to the background
    switch (event->mType) {
    case eReplayEventCheckpoint:
    case eReplayEventExit:
    case eReplayEventNormal:
    case eReplayEventRecord:
    case eReplayEventPlayback:
    case eReplayEventRecover:
    case eOnDemandEventNotIdle:
    case eOnDemandEventIdle:
    case eOnDemandEventDisabled:
      mBackground->addEvent(event);
      last_mode_change = event;
      break;
    default:
      break;
    }
  } // for
  
  // If the simulation has exited, then set up the scale so we fit
  // precisely.  Otherwise leave some headroom so we are not constantly
  // resizing the graph as the simulation progresses
  if ((last_mode_change != NULL) &&
      (last_mode_change->mType == eReplayEventExit))
  {
    mXAxisMax = last_x;
    mYAxisMax = 1.25 * maxCPS;  // still looks good to have y headroom
    setAxisScale(QwtPlot::yLeft, 0, mYAxisMax);
    setAxisScale(QwtPlot::xBottom, 0, mXAxisMax);
    mBackground->putIsFinished(true);
    mPerformance->setColor(QColor(0,0,170));
  }

  else {
    // Grow the Y-axis with a little bit of headroom because we
    // expect CPS to remain fairly stable over time.  Shrink Y-axis
    // if the maxCPS is way too small, which happens if the user
    // restarts the simulation
    if ((maxCPS > mYAxisMax) ||
        ((1.75 * maxCPS) < mYAxisMax))
    {
      mYAxisMax = 1.25 * maxCPS;
      setAxisScale(QwtPlot::yLeft, 0, mYAxisMax);
    }

    // Grow the X-axis with a bit more headroom because this will
    // continue to increase over time and I don't want the display
    // to have to rescale on every checkpoint
    if ((last_x > mXAxisMax) ||
        ((last_x * 3) < mXAxisMax))
    {
      mXAxisMax = 2*last_x;
      setAxisScale(QwtPlot::xBottom, 0, mXAxisMax);
    }
    mBackground->putIsFinished(false);
    mPerformance->setColor(QColor(Qt::blue));
  } // else

  mBackground->putMaxXY(mXAxisMax, mYAxisMax);
  mPerformance->finalize();

  replot();
} // void ReplayPerfPlot::update

void ReplayPerfPlot::getExtents(double* xsize, double* ysize) {
  *xsize = mXAxisMax;
  *ysize = mYAxisMax;
}

void ReplayPerfPlot::putXAxis(XAxis axis) {
  if (axis != mXAxis) {
    mXAxis = axis;
    update();
  }
}

QSize ReplayPerfPlot::sizeHint() const {
  QSize plotSizeHint = QwtPlot::sizeHint();
  plotSizeHint.setHeight(INIT_HEIGHT);
  return plotSizeHint;
}

void ReplayPerfPlot::putReplaySystem(CarbonReplaySystem* rs) {
  mReplaySystem = rs;
}

// explicit unit test for the array resizing problem in bug7586.  The
// failure cause was that we would lose the data when increasing
// the array capacity after the first 100 (INIT_ARRAY_CAPACITY) entries 
int ReplayPerfPlot::testCpuArray() {
  UtDoublePointArray points;
  points.unitTest();
  return 0;
}

