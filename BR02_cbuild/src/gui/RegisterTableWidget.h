// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef REGISTERTABLEWIDGET_H
#define REGISTERTABLEWIDGET_H

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "cfg/CarbonCfg.h"

#include "ApplicationContext.h"

#include <QObject>
#include "gui/CTableWidget.h"
#include <QItemDelegate>

#include "ui_RegisterTableWidget.h"

class RegisterTableWidget : public CTableWidget
{
  Q_OBJECT

public:
  RegisterTableWidget(QWidget *parent = 0);
  ~RegisterTableWidget();
  void AddRegister(); 
  void putContext(ApplicationContext *ctx);
  void Populate();
  void DeleteRegister();
  void clearAll();

  // reimplemented from CTableWidget
  void setModelData(const UtString &data, QAbstractItemModel *model,
                    const QModelIndex &index);

private:
  void updateRow(int row, CarbonCfgRegister* reg, bool clearSelect=false);
  void buildSelectionList(QList<QTableWidgetItem*>& list);
  virtual void dropEvent(QDropEvent* ev);
  virtual void dragEnterEvent(QDragEnterEvent *event);
  virtual void dragMoveEvent(QDragMoveEvent* event);
 friend class RegisterEditDelegate;


private slots:
  void itemSelectionChanged();
  void itemClicked(QTableWidgetItem* item);

private:
  Ui::RegisterTableWidgetClass ui;
  CarbonCfgID mCfg;
  CarbonCfgGroup* mGroup;
  ApplicationContext* mCtx;

signals:
  void regSelectionChanged(CarbonCfgRegister* reg);
};

class RegisterEditDelegate : public QItemDelegate
{
  Q_OBJECT

public:
  RegisterEditDelegate(QObject *parent = 0, RegisterTableWidget* t=0, CarbonCfgID cfg=0);
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const;
  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QAbstractItemModel *model,
  const QModelIndex &index) const;

private slots:
  void commitAndCloseEditor();
  void commitAndCloseComboBox(int);

private:
  RegisterTableWidget* table;
  CarbonCfgID mCfg;
};

#endif // REGISTERTABLEWIDGET_H
