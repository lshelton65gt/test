/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CQt.h"
#include "CQtWizardContext.h"
#include "mainwindow.h"
#include "car2cow.h"
#include "util/UtString.h"
#include "util/UtIO.h"

#ifndef Q_OS_WIN
#include <QtPlugin>
Q_IMPORT_PLUGIN(qgif)
#endif

extern bool gDisableCarbonMemSystem;
extern bool gCarbonMemSystemIsInitialized();

int main(int argc, char **argv)
{
  int ret = 0;
  if (gCarbonMemSystemIsInitialized())
  {
    printf("CarbonMemory system has been initialized, aborting.\n");
    return 1;
  }
  // QT has a nasty FileOpen dialog bug which causes problems interacting
  // with ModelStudio and other Carbon Apps, this disables it.
  gDisableCarbonMemSystem = true;

  // At the moment there some carbon-controlled memory leaks which
  // need to be fixed.  So we can't use this.  In addition, Qt is
  // not written in a manner which works well with our memory-leak
  // detector.  For example, it uses static constructors.  So we
  // would need to do some work to use this with Qt and our
  // global operator new override.  But I like our global opertor new
  // override for debugging purposes (deadbeef, CarbonMem::watch), so
  // I'm leaving it on for now (link in libcarbonmem.a in Makefile.am)
  // and turning off memory-leak detection.
  //
  // To do:
  //   1. do not link with libcarbonmem, but just rely on the explicit
  //      operator new overrides in our class definitions
  //      (CARBONMEM_OVERRIDES).
  //   2. Fix the Carbon memory leaks.
  //   3. Link in libcarbonmem only in debug mode.
  //   4. Figure out a way to use the global operator new overrride with
  //      Qt (e.g. account for Qt's static ctors and anything else that
  //      comes up).
  // 
  // CarbonMem memContext(&argc, argv, getenv("CARBON_WIZARD_DUMPFILE"));

  {
    Q_INIT_RESOURCE(application);

    // Allow the user to specify the db filename from the command-line to avoid
    // the heinous slow Qt open-file dialog
    MainWindow::ParseResult parseStatus = MainWindow::eNoMatch;
    UtString filename, baseName;

    for (int i = 1; i < argc; ++i) {
      parseStatus = MainWindow::parseFileName(argv[i], &baseName);
      if (parseStatus != MainWindow::eNoMatch) {
        filename = argv[i];
        memmove(&argv[i], &argv[i + 1], ((argc - i) * (sizeof(char*))));
        --argc;
        break;
      }
    }


    CQtWizardContext app(argc, argv);
    app.init(argc, argv);
    CQtWizardContext::WizardType type = app.parseTypeArgs();

    CMainWindow *mainWin = 0;

    if (type == CQtWizardContext::eMaxSim) {
      mainWin = new MainWindow(&app);
    } else if (type == CQtWizardContext::eCoware) {
      mainWin = new Car2Cow(&app);
    } else if (type == CQtWizardContext::eNone) {
      UtIO::cout() << "Wizard type not specified\n";
      return 1;
    } else if (type == CQtWizardContext::eMult) {
      UtIO::cout() << "Multiple wizard types specified\n";
      return 1;
    }

    app.putMainWindow(mainWin);


    // If the user specified neither dbfile or ccfgfile, prompt
    if (parseStatus == MainWindow::eNoMatch) {
      QString qfileName =
        QFileDialog::getOpenFileName(mainWin,
                                     "Choose a Carbon Configuration or Database",
                                     "", 
                                     "Carbon Configuration (*.ccfg *.db)");
      filename.clear();
      filename << qfileName;
      parseStatus = MainWindow::parseFileName(filename.c_str(), &baseName);
    }

    if (parseStatus == MainWindow::eNoMatch) {
      return 1;                   // no file, no GUI!
    }

    // can't do dynamic_cast<>, so check our type and
    // do a reinterpret_cast<>.
    if (type == CQtWizardContext::eMaxSim) {
      MainWindow *m = reinterpret_cast<MainWindow*>(mainWin);
      if (!m->loadFile(filename.c_str(), parseStatus)) {
        return 1;
      }

      mainWin->adjustSize();       // account for min-size tweaks
      m->showWindow();
    } else if (type == CQtWizardContext::eCoware) {
      Car2Cow *w = reinterpret_cast<Car2Cow*>(mainWin);
      w->loadFile(filename.c_str());
      if (!w->emptyDocument() && w->licenseValid()) {
        w->show();

        app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
      }
      else
        return 1;
    }
    ret = app.exec();
  }
  return ret;
}
