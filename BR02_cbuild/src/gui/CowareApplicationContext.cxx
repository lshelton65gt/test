/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "CowareApplicationContext.h"
#include "car2cow.h"

CowareApplicationContext::CowareApplicationContext(CarbonDB *db, CarbonCfgID cfg, CQtContext *cqt)
  : ApplicationContext(db, cfg, cqt)
{
  mApp = 0;

  mRegisterTableColumnEnumMap["colOFFSET"] = colOFFSET;
  mRegisterTableColumnEnumMap["colREGNAME"] = colREGNAME;
  mRegisterTableColumnEnumMap["colWIDTH"] = colWIDTH;
  mRegisterTableColumnEnumMap["colENDIAN"] = colENDIAN;
  mRegisterTableColumnEnumMap["colDESCRIPTION"] = colDESCRIPTION;
  mRegisterTableColumnEnumMap["colFIELDS"] = colFIELDS;
}

CowareApplicationContext::~CowareApplicationContext()
{
}

void CowareApplicationContext::setDocumentModified(bool value)
{
  mApp->setWindowModified(value);
}

CowareApplicationContext *CowareApplicationContext::castCoware()
{
  return this;
}
