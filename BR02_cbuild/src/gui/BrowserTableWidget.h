// -*-c++-*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef BROWSERTABLEWIDGET_H
#define BROWSERTABLEWIDGET_H

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "util/UtWildcard.h"

#include "ApplicationContext.h"

#include <QObject>
#include "gui/CDragDropTreeWidget.h"
#include "ui_BrowserTableWidget.h"
#include "Directives.h"

class BrowserTreeWidget;
class CarbonMakerContext;

class XToplevelInterface;
class XInstance;
class CarbonCfg;

class BrowserTableWidget : public CDragDropTreeWidget
{
  Q_OBJECT

  enum FilterNetType {
    eAllNets, eScalars, eVectors, eMemories
  };

public:
  BrowserTableWidget(QWidget *parent = 0);
  ~BrowserTableWidget();
  void putContext(ApplicationContext* ctx, CarbonMakerContext* appContext);
  void Initialize(HierarchyMode mode);
  void Connect(BrowserTreeWidget* browserTree);
  void clearAll();
  const char* getDragText() { return mDragText.c_str(); }
  void selectNet(const char* netName);
  void AddNetNodes(int depth, CarbonDBNode* node, QTreeWidgetItem *widgetItem);
  void ShowDirection(CarbonDB *mDB, const CarbonDBNode *node, QTreeWidgetItem *item);
  void ShowAttributes(CarbonDB *mDB, const CarbonDBNode *node, QTreeWidgetItem *item);
  void ShowFlags(CarbonDB *mDB, const CarbonDBNode *node, QTreeWidgetItem *item);
  void ShowTypes(CarbonDB *mDB, const CarbonDBNode *node, QTreeWidgetItem *item);
  void ShowRanges(CarbonDB *mDB, const CarbonDBNode *node, QTreeWidgetItem *item);
  void ShowSize(CarbonDB *mDB, const CarbonDBNode *node, QTreeWidgetItem *item);
  void updateContextMenuActions();
  void applyFilters(const QString& filterStr, bool explicitObserves, bool explicitDeposits);

signals:
  void selectionChanged(CarbonDBNode *node);
  void navigateSource(const char *filename, int lineNumber);
  void applySignalDirective(DesignDirective, const char*, bool);

private slots:
  void nodeSelectionChanged(CarbonDBNode* node,XToplevelInterface*,XInstance*);
  void itemSelectionChanged();
  void showContextMenu(const QPoint &pos);

private:
  CarbonDB* mDB;
  CarbonCfg* mCfg;
  ApplicationContext* mCtx;
  BrowserTreeWidget* mTree;
  UtWildcard mFilterPattern;
  FilterNetType mShowTypes;
  UtString mDragText;
  HierarchyMode mHierMode;
  QAction* mActionNavigateSource;
  QAction* mActionObserve;
  QAction* mActionDeposit;
  QAction *mActionForce;
  CarbonMakerContext* mContext;
  CarbonDBNode* mParentNode;
  XToplevelInterface* mParentTop;
  XInstance* mParentInst;

private:
  void buildSelectionList(QList<QTreeWidgetItem*>& list);
  QColor getDefaultTextColor();
  bool qualifyNet(const CarbonDBNode* node,UInt32* filteredNets);
  void createActions();
  QAction *createIndividualAction(const char *text, const char *objectName, const char *iconName, const char *statusTip);

private:
  Ui::BrowserTableWidgetClass ui;
  bool mExplicitObserves;
  bool mExplicitDeposits;

public slots:
  void itemExpanded(QTreeWidgetItem* item);
  void actionNavigateSource();
  void actionApplyDirective();
};

struct CarbonCowareHBrowserNodeSorter {
  CarbonCowareHBrowserNodeSorter(CarbonDB* db): mDB(db) {}

  bool operator()(const CarbonDBNode* n1, const CarbonDBNode* n2);
  
  CarbonDB* mDB;
};
  

#endif // BROWSERTABLEWIDGET_H
