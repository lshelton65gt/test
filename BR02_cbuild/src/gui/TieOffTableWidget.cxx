/******************************************************************************
 Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "car2cow.h"
#include "PortView.h"
#include "TieOffTableWidget.h"
#include "CarbonProjectWidget.h"

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtStringUtil.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "shell/carbon_capi.h"
#include "cfg/CarbonCfg.h"
#include "util/UtConv.h"

#include "util/DynBitVector.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "shell/CarbonDatabasePriv.h"
#include "iodb/IODBRuntime.h"
#include "util/DynBitVector.h"

#include <QFileDialog>
#include <QtDebug>
#include <QtGui>
#include <QLabel>
#include <QStringList>

TieOffTableWidget::TieOffTableWidget(QWidget* parent) : QTableWidget(parent)
{
  mDB = NULL;
  mCfg = NULL;
  m_pPortTable = NULL;

  initialize();

  connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(QPoint)));

  createActions();
}

void TieOffTableWidget::initialize()
{
  QStringList labels;
  labels << "HDL Port" << "Width" << "Tied";

  setColumnCount(labels.count());
  setHorizontalHeaderLabels(labels);
  horizontalHeader()->setResizeMode(0, QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(columnCount()-1, QHeaderView::Stretch);

  setItemDelegate(new TieOffTableEditDelegate(0, this));

  verticalHeader()->setVisible(false);
}

void TieOffTableWidget::Populate(ApplicationContext* ctx, bool initPorts)
{
  mCtx = ctx;
  mDB = ctx->getDB();
  mCfg = ctx->getCfg();

  // We are not not reading from a .ccfg file if initPorts=true
  if (initPorts)
  {
    // Do Nothing special here, we can't tell what needs to be tied off
  }
}

CarbonCfgTie* TieOffTableWidget::getTie(CarbonCfgRTLPort* port)
{
  int nConnections = port->numConnections();

  INFO_ASSERT(nConnections >= 1, "Port has no connections");

  CarbonCfgRTLConnection* conn = port->getConnection(0);

  if (nConnections == 1 && conn != NULL && conn->getType() == eCarbonCfgTie)
    return conn->castTie();
  else
    return NULL;
}

void TieOffTableWidget::dropEvent(QDropEvent* ev)
{
  UtString rtlPath;
  rtlPath << ev->mimeData()->text();

  UtIStringStream is(rtlPath);
  UtString buf;

  clearSelection();

  while (is.getline(&buf)) 
  {
    StringUtil::strip(&buf, "\n");
    rtlPath = buf.c_str();
    CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(rtlPath.c_str());
    INFO_ASSERT(rtlPort != NULL, "Unable to find Carbon RTL Port");

    // Disconnect the Ports
    UInt32 numConnections = rtlPort->numConnections();
    UtArray<CarbonCfgRTLConnection*> disconnects;
    for (UInt32 i = 0, n = numConnections; i < n; ++i) {
      CarbonCfgRTLConnection* con = rtlPort->getConnection(i);
      disconnects.push_back(con);
    }
    for (UtArray<CarbonCfgRTLConnection*>::iterator i = disconnects.begin(); i != disconnects.end(); ++i) {
      CarbonCfgRTLConnection* con = *i;
      mCfg->disconnect(con);
    }

    UInt32 value = 0;
    CarbonCfgRTLConnectionID conn = carbonCfgTie(rtlPort, &value, 1);

    //int nConns = rtlPort->numConnections();

    CarbonCfgTie* tie = conn->castTie();

    AddTie(rtlPort, tie);
    mCtx->setDocumentModified(true);
  }

  ev->acceptProposedAction();
  horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(columnCount()-1, QHeaderView::Stretch);

  setFocus();
}

QMimeData* TieOffTableWidget::mimeData(const QList<QTableWidgetItem*>) const
{
  return 0;
}

void TieOffTableWidget::dragMoveEvent(QDragMoveEvent *ev)
{
  ev->setDropAction(Qt::MoveAction);
  ev->accept();
}

void TieOffTableWidget::dragEnterEvent(QDragEnterEvent* ev)
{
  ev->acceptProposedAction();
}

void TieOffTableWidget::BeginUpdate()
{
  setRowCount(0);
  setUpdatesEnabled(false);
  setSortingEnabled(false);

}

void TieOffTableWidget::EndUpdate()
{
  setUpdatesEnabled(true);
  setSortingEnabled(true);
  clearSelection();

  horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
  horizontalHeader()->setResizeMode(columnCount()-1, QHeaderView::Stretch);
}

void TieOffTableWidget::showContextMenu(const QPoint &pos)
{
//  qDebug() << " Show context menu here";   

//  QTableWidgetItem *item = itemAt(pos);

  QMenu menu(this);
  menu.addAction(removeAction);
  menu.exec(viewport()->mapToGlobal(pos));
}

void TieOffTableWidget::removeTie(CarbonCfgTie* tie)
{
  INFO_ASSERT(tie != NULL, "NULL userData tie found");

  CarbonCfgRTLPort* port = tie->getRTLPort();

  port->disconnect(tie);
}

// Remove
void TieOffTableWidget::remove()
{
  QList<QTableWidgetItem*> removeItems;

  // The list gives items for every column and row, we just want it row
  // oriented.
  foreach (QTableWidgetItem *item, selectedItems())
  {
    if (item->column() == 0)
      removeItems.append(item);
  }

  // Now walk the list of items to be deleted
  foreach (QTableWidgetItem *item, removeItems)
  {
    QVariant qv = item->data(Qt::UserRole);

    CarbonCfgTie* tie = (CarbonCfgTie*)qv.value<void*>();
    CarbonCfgRTLPort* rtlPort = tie->getRTLPort();
    bool removeIt = true;
    const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
    if (node && carbonDBIsTied(mDB, node))
    {
        UtString msg;
        msg << "The port: " << rtlPort->getName() << " is tied off by a compiler directive.\n";
        msg << "Are you sure you want to remove it?";
        
        if (QMessageBox::question(this, MODELSTUDIO_TITLE, msg.c_str(), 
          QMessageBox::No|QMessageBox::Yes, QMessageBox::No) == QMessageBox::No)
          removeIt = false;
    }

    if (removeIt)
    {
      // Remove it from the Table
      removeRow(item->row());

      // Remove it from the Data model
      removeTie(tie);

      if (m_pPortTable != NULL)
        m_pPortTable->CreateESLPort(rtlPort);
    }
  }
}

void TieOffTableWidget::createActions()
{
  removeAction = new QAction(tr("Remove"), this);
  connect(removeAction, SIGNAL(triggered()), this, SLOT(remove()));
}

QTableWidgetItem* TieOffTableWidget::AddTie(CarbonCfgRTLPort* rtlPort, CarbonCfgTie* tie, bool isDirectiveTied)
{
  BlockTableUpdates block(this);

  int row = rowCount();
  // Bump the RowCount first, otherwise the cells will look empty
  setRowCount(row+1);

  // First Item; HDLName
  QTableWidgetItem *itemName = new QTableWidgetItem;
  itemName->setText(rtlPort->getName());
  setItem(row, colHDLNAME, itemName);

  QVariant qv = qVariantFromValue((void*)tie);

  itemName->setData(Qt::UserRole, qv);
  itemName->setSelected(true);

  // Second Item: Width
  UtString buf;
  buf << rtlPort->getWidth();
  QTableWidgetItem *itemWidth = new QTableWidgetItem;
  itemWidth->setText(buf.c_str());
  setItem(row, colWIDTH, itemWidth);
  itemWidth->setSelected(true);

  // Third Item: Tied Value
  QTableWidgetItem *itemTied = new QTableWidgetItem;

  UtString tieValue;
  UtOStringStream ss(&tieValue);
  ss << "0x" << UtIO::hex << tie->mValue;

  if (isDirectiveTied)
  {
    QFont font = itemTied->font();
    font.setItalic(true);
    itemTied->setFont(font);  
    const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
    const IODBRuntime* iodb = CarbonDatabasePriv::getIODB(mDB);
    const STSymbolTableNode* stNode = CarbonDatabasePriv::getSymTabNode(node);
    const DynBitVector* bv = iodb->getTieValue(stNode);
    UtString valStr;
    bv->format(&valStr, eCarbonHex);
    itemTied->setText(valStr.c_str());
  }
  else
    itemTied->setText(tieValue.c_str());

  setItem(row, colTIED, itemTied); 
  itemTied->setSelected(true);

  return itemName;
}

TieOffTableWidget::~TieOffTableWidget()
{
}

void TieOffTableWidget::ClearSelection()
{
  clearSelection();
}

TieOffTableEditDelegate::TieOffTableEditDelegate(QObject *parent, TieOffTableWidget* table)
: QItemDelegate(parent), tieOffTable(table)
{
}
QWidget *TieOffTableEditDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                               const QModelIndex &index) const
{
  switch (index.column())
  {
  case TieOffTableWidget::colTIED:
    {
      QLineEdit *editor = new QLineEdit(parent);
      connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
      return editor;
    }
    break;
  default:
    return NULL; // Read Only
    break;
  }

  QLineEdit *editor = new QLineEdit(parent);
  connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
  return editor;
}

void TieOffTableEditDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  emit commitData(editor);
  emit closeEditor(editor);
}

void TieOffTableEditDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit) {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}

void TieOffTableEditDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                           const QModelIndex &index) const
{
  QTableWidgetItem* item = tieOffTable->item(index.row(), TieOffTableWidget::colHDLNAME);
  QVariant qv = item->data(Qt::UserRole);
  CarbonCfgTie* tie = (CarbonCfgTie*)qv.value<void*>();

  switch (index.column())
  {
  case TieOffTableWidget::colTIED:
    {
      QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
      if (edit)
      {
        tieOffTable->mCtx->setDocumentModified(true);

        UtString newVal;
        newVal << edit->text();
        UtIO::Radix radix = UtIO::hex;

        if (0 == strncasecmp(newVal.c_str(), "0x", 2))
        {
          newVal.erase(0, 2);
          radix = UtIO::hex;
        }
        else
        {
          radix = UtIO::dec;
        }


        UtIStringStream ss(newVal);
        DynBitVector bv(tie->getRTLPort()->getWidth());
        if (ss >> radix >> bv) 
        {
          tie->mValue = bv;
        } 

        UtString hexValue;
        UtOStringStream hss(&hexValue);
        hss << "0x" << UtIO::hex << tie->mValue;
        model->setData(index, hexValue.c_str());

      }
    }
    break;
  default:
    break;
  }
}
