/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CQt.h"
#include "util/UtIOStream.h"
#include "gui/CQtPushButtonControl.h"
#include "util/CarbonAssert.h"  // move to CQt.h?

CQtPushButtonControl::CQtPushButtonControl(QPushButton* pb, CQtContext* cqt):
  mPushButton(pb),
  mCQt(cqt)
{
  CQT_CONNECT(pb, clicked(), this, clicked());
}

void CQtPushButtonControl::dumpState(UtOStream&) {
  // do not put the button text into the dump file.  It's generally static.
}

void CQtPushButtonControl::getValue(UtString* str) const {
  str->clear();
}

bool CQtPushButtonControl::putValue(const char*, UtString* /* errmsg */) {
  mPushButton->click();
  return true;
}

QWidget* CQtPushButtonControl::getWidget() const {
  return mPushButton;
}

void CQtPushButtonControl::clicked() {
  mCQt->recordPutValue(mPushButton, "");
}
