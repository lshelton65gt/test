// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "CBrowseNode.h"
#include "CBrowse.h"
#include "symtab/STSymbolTableNode.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/UtStringUtil.h"
#include "CBrowseButton.h"

#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>
#include <QGraphicsSceneMouseEvent>

#define SLACK_PIXELS 10 // pixels

CBrowseNode::CBrowseNode(CBrowse* browser, CGraph::Node* node) :
  mBrowser(browser), mDB(browser->getDB()), mCGNode(node)
{
  mIndex = 0;
  mTextStart = 0;
  mTextEnd = 0;
  setAcceptsHoverEvents(true);
}

CBrowseNode::~CBrowseNode() {
  clearButtons();
}

void CBrowseNode::clearButtons() {
  for (UInt32 i = 0; i < mButtons.size(); ++i) {
    delete mButtons[i];
  }
  mButtons.clear();
}

static void findStackedBounds(QFontMetrics* metrics, const char* str, double* width,
                              double* height)
{
  // There can be embedded newlines in 'str', so walk through the text line
  // by line aligning the ntext vertically, and MAXing the width.
  UtIStringStream is(str);
  UtString line;
  while (is.getline(&line)) {
    QRect r = metrics->boundingRect(line.c_str());
    if (r.width() > *width) {
      *width = r.width();
    }
    *height += r.height();
  }
}

static void drawTextLines(CBrowse* browser, QPainter* painter,
                          const char* str, double x, double y,
                          double* width, double* height)
{
  // There can be embedded newlines in 'str', so walk through the text line
  // by line aligning the ntext vertically, and MAXing the width.
  UtIStringStream is(str);
  UtString line;
  while (is.getline(&line)) {
    double line_width, line_height;
    browser->drawText(painter, line.c_str(), x, y, &line_width, &line_height);
    if (line_width > *width) {
      *width = line_width;
    }
    *height += line_height;
    y += line_height;
  }
}

CBrowseButton* CBrowseNode::findButton(CGraph::Node* node) {
  return mNodeButtonMap[node];
}

// This ifdef'd out code was used to try to show an abbreviated part
// of an assignment statement so that the graphics boxes would be small.
// This code is probably obsolete because I've gone to a more coarse-grained
// approach where there are fewer boxes but I'm willing to let the boxes
// be larger.
//
// Also as each node can write to multiple nets, we need to show the LHS
// in the assignment text.
#if 0
static void sStripDescription(const char* desc, UtString* out) {
  // remove "assign " prefix if it's there
  if (strncmp(desc, "assign ", 7) == 0) {
    desc += 7;
  }

  // if the first part is just "signal = ", then skip the LHS.  This
  // should cover most assignments and remove noise from the
  // connectivity graph.
  const char* eq = strstr(desc, " = ");
  if (eq != NULL) {
    UtString lhs(desc, eq - desc);
    if ((strchr(lhs.c_str(), '[') == NULL) &&
        (strchr(lhs.c_str(), '{') == NULL))
    {
      // not a concat-lvalue or varsel-lvalue, so strip it.
      desc = eq + 1;            // leave the "= " in
    }
  }

  // Remove the "// xyz.v" text, if there is any
  const char* comment = strstr(desc, " // ");
  if (comment != NULL) {
    out->append(desc, comment - desc);
  }
  else {
    out->append(desc);
  }
  StringUtil::strip(out);

  // Remove the trailing ';', if any
  UInt32 out_size = out->size();
  if ((out_size > 0) && ((*out)[out_size - 1] == ';')) {
    --out_size;
    out->resize(out_size);
  }

  // Finally, limit the # of chars to (for now) 24
  if (out_size > 24) {
    out->resize(21);
    *out << "...";
  }
} // static void sStripDescription
#endif

void CBrowseNode::layout() {
  /*
   * We are going to lay out the node like this:
   *
   *    +-----------------------------+
   *   /|then   net [bits]            |
   *   \|       path                  |
   *   /|else                         |\ fanout
   *   \|          if (cond)          |/
   *   /|cond                         |
   *   \|                             |
   *    +-----------------------------+
   *
   *
   * The left-triangles expands to show the fanin nets
   * The right-triangle expands to show the net being driven
   *
   * A distinct left-triangle is drawn for:
   *   - all fanin to an if-statement "then" branch
   *   - all fanin to an if-statement "else" branch
   *   - all fanin to a case-statement item
   * The bottom-triangle is drawn for:
   *   - all "control" fanin, e.g. all fanin nets of an if condition,
   *     or the edge fanin to a sequential block
   *   
   * "black box" nodes just get one left fanin button which navigates
   * to all the fanin.
   *
   * A triangle is drawn in blue if pressing it will expose more nodes
   * A triangle is drawn in red if pressing it will remove nodes
   * A triangle is drawn in gray if pressing it will have no visible effect
   */
  QFontMetrics* metrics = mBrowser->getFontMetrics();
  double width = 0, height = 0;

  double x = 0;
  UInt32 num_left_buttons = mCGNode->numSubNodes();
  if (mCGNode->numFanin() != 0) {
    ++num_left_buttons;
  }

  if (num_left_buttons != 0) {
    x += BUTTON_WIDTH;
  }
  mTextStart = x + SLACK_PIXELS;

  if (mCGNode->isContinuousDriver()) {

    // In the CGraph::Node representation, there can be multiple nets
    // driven from a single node, so display all of them in the graphics
    // box that represents the node, along with the bits in those nets
    // that are driven by this box.

    for (CGraph::NetAndBitsLoop p(mCGNode->loopNetAndBits()); !p.atEnd(); ++p)
    {
      const CGraph::NetAndBits& nb = *p;
      const STSymbolTableNode* net = nb.first;
      StringAtom* bits = nb.second;
      UtString net_bits;
      net_bits << carbonDBNodeGetFullName(mDB, net);
      net_bits << " " << bits->str();
      findStackedBounds(metrics, net_bits.c_str(), &width, &height);
    }
  }

  const char* drvpath = carbonDBNodeGetFullName(mDB, mCGNode->getScope());
  findStackedBounds(metrics, drvpath, &width, &height);

  UtString descr;
  mBrowser->getCGraph()->getNodeDescription(mCGNode, &descr);
  findStackedBounds(metrics, descr.c_str(), &width, &height);

  mTextEnd = mTextStart + width;
  x = mTextEnd + SLACK_PIXELS;

  // 'height' has the height-requirements based on the text to be
  // drawn inside the rectangle.  We also need the height-requirements
  // for the buttons, and then MAX them.
  double buttons_height = BUTTON_HEIGHT*num_left_buttons;
  double button_spacing = 0;
  clearButtons();
  if (height < buttons_height) {
    height = buttons_height;
  }
  else {
    button_spacing = (height - buttons_height) / (num_left_buttons + 1);
  }

  double y = button_spacing + BUTTON_HEIGHT / 2;

  for (UInt32 i = 0, n = mCGNode->numSubNodes(); i < n; ++i) {
    CBrowseButton* button =
      new CBrowseButton(0, y, CBrowseButton::eLeft, mBrowser);
    y += button->getHeight() + button_spacing;
    const CGraph::NodeVec& nodes = mCGNode->getSubNodes(i);
    for (UInt32 j = 0; j < nodes.size(); ++j) {
      CGraph::Node* node = nodes[j];
      button->addNode(node);
      mNodeButtonMap[node] = button;
    }
    mButtons.push_back(button);
  }

  // question: should one button open up all the control fanin?  Or
  // should there be one fanin per distinct fanin?  For now, one button
  // to consume all the fanin.  This avoids blowing up the vertical size
  // of the node when if-statements have complex conditionals
  if (mCGNode->numFanin() != 0) {
    CBrowseButton* button = 
      new CBrowseButton(0, y, CBrowseButton::eLeft, mBrowser);
    y += button->getHeight() + button_spacing;
    mButtons.push_back(button);
    for (CGraph::NodeSetLoop p(mCGNode->loopFanin()); !p.atEnd(); ++p) {
      CGraph::Node* node = *p;
      button->addNode(node);
      mNodeButtonMap[node] = button;
    }
  }

  CBrowseButton* right_button = NULL;
  for (CGraph::NodeSetLoop p(mCGNode->loopFanout()); !p.atEnd(); ++p) {
    CGraph::Node* fanout = *p;
    if (right_button == NULL) {
      x += BUTTON_WIDTH;
      right_button = new CBrowseButton(x, height/2,
                                        CBrowseButton::eRight, mBrowser);
      mButtons.push_back(right_button);
    }
    right_button->addNode(fanout);
    mNodeButtonMap[fanout] = right_button;
  }

  mBoundingRect.setWidth(x);
  mBoundingRect.setHeight(height);
} // void CBrowseNode::layout

QRectF CBrowseNode::boundingRect() const {
  return mBoundingRect;
}

void CBrowseNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
  switch (mCGNode->getType()) {
  case CGraph::eSequential:  painter->setBrush(Qt::green);  break;
  case CGraph::eIf:          painter->setBrush(Qt::yellow); break;
  case CGraph::eComplexCase:
  case CGraph::eCase:        painter->setBrush(Qt::yellow); break;
  case CGraph::eBlackBox:    painter->setBrush(Qt::cyan);   break;
  case CGraph::eUnbound:     painter->setBrush(Qt::gray);   break;
  case CGraph::eInput:
  case CGraph::eOutput:
  case CGraph::eBidirect:
  case CGraph::eForce:
  case CGraph::eDeposit:
  case CGraph::eObserve:     painter->setBrush(Qt::white);   break;
  }

  QRectF r(mTextStart - SLACK_PIXELS, 0,
           mTextEnd - mTextStart + 2*SLACK_PIXELS,
           mBoundingRect.height());
  painter->drawRect(r);
  painter->setFont(*mBrowser->getFont());

  for (UInt32 i = 0; i < mButtons.size(); ++i) {
    mButtons[i]->draw(painter);
  }

  double width, height, x = mTextStart, y = 0;
  painter->setBrush(Qt::black);

  if (mCGNode->isContinuousDriver()) {

    // In the CGraph::Node representation, there can be multiple nets
    // driven from a single node, so display all of them in the graphics
    // box that represents the node, along with the bits in those nets
    // that are driven by this box.

    for (CGraph::NetAndBitsLoop p(mCGNode->loopNetAndBits()); !p.atEnd(); ++p)
    {
      const CGraph::NetAndBits& nb = *p;
      const STSymbolTableNode* net = nb.first;
      StringAtom* bits = nb.second;
      UtString net_bits;
      net_bits << carbonDBNodeGetFullName(mDB, net);
      net_bits << " " << bits->str();
      mBrowser->drawText(painter, net_bits.c_str(), x, y, &width, &height);
      y += height;
    }
  }

  const char* drvpath = carbonDBNodeGetFullName(mDB, mCGNode->getScope());
  mBrowser->drawText(painter, drvpath, x, y, &width, &height);
  y += height;

  UtString descr;
  mBrowser->getCGraph()->getNodeDescription(mCGNode, &descr);
  drawTextLines(mBrowser, painter, descr.c_str(), x, y, &width, &height);
} // void CBrowseNode::paint

void CBrowseNode::mark() {
  for (UInt32 i = 0; i < mButtons.size(); ++i) {
    CBrowseButton* button = mButtons[i];

    // Even if the button is on, off, or disabled, another buttton
    // might have enabled one or more adjacent nodes, in which case we
    // should draw the edge.

    for (UInt32 j = 0; j < button->numNodes(); ++j) {
      CGraph::Node* node = button->getNode(j);
      if (mBrowser->isVisible(node) ||
          (button->getState() == CBrowseButton::eOn))
      {
/*
        bool show_node;
        for (CGraph::ClusterSetLoop p(node->loopParents()); !p.atEnd(); ++p) {
          CGraph::Cluster* cluster = *p;
        }
*/

        CBrowseNode* cnode = mBrowser->layoutNode(node);
        CBrowseButton* cnode_button = cnode->findButton(mCGNode);
        INFO_ASSERT(cnode_button, "button insanity");
        if (button->getOrientation() == CBrowseButton::eRight) {
          mBrowser->addEdge(this, button, cnode, cnode_button);
        }
        else {
          mBrowser->addEdge(cnode, cnode_button, this, button);
        }
      }
    }
  }
}

void CBrowseNode::mousePressEvent(QGraphicsSceneMouseEvent* event) {
  double x = event->pos().x();
  double y = event->pos().y();

  // A button-press inside the text area pops up a text editor on
  // the source locator
  if ((x > mTextStart) && (x < mTextEnd)) {
/*
  We should ressurect this functionality, where clicking on a node
  brings up the original source.  But at the moment we are not populating
  the source locator information.  We are just populating the dumpVerilog,
  because it's more reliable at the moment.  When we get good source-locator
  info we'll re-enable this functionality.

    mBrowser->popupSource(mCGNode->getLoc());
*/
  }

  for (UInt32 i = 0; i < mButtons.size(); ++i) {
    CBrowseButton* button = mButtons[i];
    if (button->inside(x, y)) {
      if (button->getState() != CBrowseButton::eDisabled) {
        mBrowser->saveState();
        button->toggle();
        mBrowser->stabilize(this);
        mBrowser->redraw(mBoundingRect);
        mBrowser->layout();
      }
      return;
    }
  }
}

void CBrowseNode::mouseReleaseEvent(QGraphicsSceneMouseEvent* /*event*/) {
//  UtIO::cerr() << event << std::endl;
}

void CBrowseNode::hoverEnterEvent(QGraphicsSceneHoverEvent* event) {
  mBrowser->enterNode(this);
  QGraphicsItem::hoverEnterEvent(event);
}

void CBrowseNode::hoverLeaveEvent(QGraphicsSceneHoverEvent* event) {
  mBrowser->leaveNode(this);
  QGraphicsItem::hoverLeaveEvent(event);
}

void CBrowseNode::saveState(State* state) {
  state->mPos = pos();
  state->mButtonState.clear();
  state->mNode = this;
  for (UInt32 i = 0; i < mButtons.size(); ++i) {
    CBrowseButton* button = mButtons[i];
    state->mButtonState.push_back(button->getState());
  }
}

void CBrowseNode::restoreState(const State& state) {
  setPos(state.mPos);
  INFO_ASSERT(mButtons.size() == state.mButtonState.size(),
              "button state insanity");
  for (UInt32 i = 0; i < mButtons.size(); ++i) {
    CBrowseButton* button = mButtons[i];
    button->putState(state.mButtonState[i]);
  }
}
