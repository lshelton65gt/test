/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#include "gui/CQtControl.h"
#include "util/UtIStream.h"
#include "util/UtString.h"
#include "util/UtIOStream.h"
#include <QComboBox>
#include <QLineEdit>
#include <QSpinBox>
#include "gui/CQt.h"
#include "util/CarbonAssert.h"
#include "gui/CTextEdit.h"
#include "gui/CHexSpinBox.h"

CQtControl::CQtControl() {
}

CQtControl::~CQtControl() {
}

bool CQtControl::putIndex(UInt32, UtString* errmsg) {
  *errmsg << "putIndex not supported by this control";
  return false;
}

// default implementation of xy/pos converters simply encode
// the integers in a string
bool CQtControl::XYToLocation(int x, int y, UtString* location,
                                UtString*)
{
  *location << x << " " << y;
  return true;
}

bool CQtControl::locationToXY(const char* location, int* x, int* y,
                                UtString* errmsg)
{
  UtIStringStream is(location);
  SInt32 xx, yy;
  if ((is >> xx) && (is >> yy)) {
    *x = xx;
    *y = yy;
    return true;
  }
  *errmsg = is.getErrmsg();
  return false;
}


void CQtControl::dumpWidgetProperties(UtOStream& )
{
}

// Combo-boxes pull up another widget when you click on the
// arrow.  Let's not record that:
//  registerWidget(new CQtGenericControl, widget, widgetName);
// instead, let's record just the final result
CQtComboBoxControl::CQtComboBoxControl(QComboBox* cb):
  mComboBox(cb)
{}


void CQtComboBoxControl::dumpState(UtOStream& stream)
{
  QString str = mComboBox->currentText();
  stream << str;
  stream << "\n";

  for (int ci=0; ci<mComboBox->count(); ++ci) {
    QString itemText = mComboBox->itemText(ci);
    stream << itemText << "\n";
  }
}

void CQtComboBoxControl::getValue(UtString* str) const {
  str->clear();
  *str << mComboBox->currentText();
}

bool CQtComboBoxControl::putValue(const char* str, UtString* /* errmsg */) {
  if (mComboBox->isEditable()) {
    mComboBox->setEditText(str);
  }
  // lookup string in list and return failure (?)
  return true;
}

bool CQtComboBoxControl::putIndex(UInt32 index, UtString* errmsg) {
  if (index < ((UInt32) mComboBox->count())) {
    mComboBox->setCurrentIndex(index);
    return true;
  }
  *errmsg << "index " << index << " exceeds item-count "
          << mComboBox->count();
  return false;
}

QWidget* CQtComboBoxControl::getWidget() const {
  return mComboBox;
}


// ---- line editor


CQtLineEditControl::CQtLineEditControl(QLineEdit* le, CQtContext* cqt):
  mLineEdit(le),
  mCQt(cqt)
{
  CQT_CONNECT(le, textChanged(const QString&),
              this, textChanged(const QString&));
}

void CQtLineEditControl::dumpState(UtOStream& stream)
{
  stream << mLineEdit->text() << "\n";
}

void CQtLineEditControl::getValue(UtString* str) const {
  str->clear();
  *str << mLineEdit->text();
}

bool CQtLineEditControl::putValue(const char* str, UtString* /* errmsg */) {
  mLineEdit->setText(str);
  return true;
}

QWidget* CQtLineEditControl::getWidget() const {
  return mLineEdit;
}

void CQtLineEditControl::textChanged(const QString& qbuf) {
  UtString buf;
  buf << qbuf;
  mCQt->recordPutValue(mLineEdit, buf.c_str());
}

// ---- text editor


CQtTextEditControl::CQtTextEditControl(CTextEdit* le, CQtContext* cqt):
  mTextEdit(le),
  mCQt(cqt)
{
  CQT_CONNECT(le, textChanged(), this, textChanged());
}

void CQtTextEditControl::dumpState(UtOStream& stream)
{
  stream << mTextEdit->toPlainText() << "\n";
}

void CQtTextEditControl::getValue(UtString* str) const {
  str->clear();
  *str << mTextEdit->toPlainText();
}

bool CQtTextEditControl::putValue(const char* str, UtString* /* errmsg */) {
  mTextEdit->setPlainText(str);
  return true;
}

QWidget* CQtTextEditControl::getWidget() const {
  return mTextEdit;
}

void CQtTextEditControl::textChanged() {
  UtString buf;
  buf << mTextEdit->toPlainText();
  mCQt->recordPutValue(mTextEdit, buf.c_str());
}

// ---- spin box


CQtSpinBoxControl::CQtSpinBoxControl(QSpinBox* sb, CQtContext* cqt):
  mSpinBox(sb),
  mCQt(cqt)
{
  CQT_CONNECT(sb, valueChanged(int), this, valueChanged(int));
}

void CQtSpinBoxControl::dumpState(UtOStream& stream)
{
  stream << mSpinBox->text() << "\n";
}

void CQtSpinBoxControl::getValue(UtString* str) const {
  int val = mSpinBox->value();
  str->clear();
  *str << val;
}

bool CQtSpinBoxControl::putValue(const char* str, UtString* errmsg) {
  UtIStringStream is(str);
  int val;
  if (is >> val) {
    mSpinBox->setValue(val);
    return true;
  }
  *errmsg << is.getErrmsg();
  return false;
}

QWidget* CQtSpinBoxControl::getWidget() const {
  return mSpinBox;
}

void CQtSpinBoxControl::valueChanged(int val) {
  UtString buf;
  buf << val;
  mCQt->recordPutValue(mSpinBox, buf.c_str());
}

// ---- hex spin box


CQtHexSpinBoxControl::CQtHexSpinBoxControl(CHexSpinBox* sb, CQtContext* cqt):
  mSpinBox(sb),
  mCQt(cqt)
{
  CQT_CONNECT(sb, valueChanged(const QString&), this, valueChanged(const QString&));
}

void CQtHexSpinBoxControl::dumpState(UtOStream& stream)
{
  stream << mSpinBox->text() << "\n";
}

void CQtHexSpinBoxControl::getValue(UtString* str) const {
  str->clear();
  *str << mSpinBox->text();
}

bool CQtHexSpinBoxControl::putValue(const char* str, UtString* errmsg) {
  return mSpinBox->setValue(str, errmsg);
}

QWidget* CQtHexSpinBoxControl::getWidget() const {
  return mSpinBox;
}

void CQtHexSpinBoxControl::valueChanged(const QString& qbuf) {
  UtString buf;
  buf << qbuf;
  mCQt->recordPutValue(mSpinBox, buf.c_str());
}

// ---- checkbox


CQtCheckBoxControl::CQtCheckBoxControl(QCheckBox* sb, CQtContext* cqt):
  mCheckBox(sb),
  mCQt(cqt)
{
  CQT_CONNECT(sb, stateChanged(int), this, stateChanged());
}

void CQtCheckBoxControl::dumpState(UtOStream&) {
}

void CQtCheckBoxControl::getValue(UtString* str) const {
  switch (mCheckBox->checkState()) {
  case Qt::Checked: *str = "checked"; break;
  case Qt::Unchecked: *str = "unchecked"; break;
  case Qt::PartiallyChecked: *str = "partial"; break;
  }
}

bool CQtCheckBoxControl::putValue(const char* str, UtString* errmsg) {
  if (strcmp(str, "checked") == 0) {
    mCheckBox->setCheckState(Qt::Checked);
  }
  else if (strcmp(str, "unchecked") == 0) {
    mCheckBox->setCheckState(Qt::Unchecked);
  }
  else if (strcmp(str, "partial") == 0) {
    mCheckBox->setCheckState(Qt::PartiallyChecked);
  }
  else {
    *errmsg << "Invalid checkbox state: " << str;
    return false;
  }
  return true;
}

QWidget* CQtCheckBoxControl::getWidget() const {
  return mCheckBox;
}

void CQtCheckBoxControl::stateChanged() {
  UtString buf;
  getValue(&buf);
  mCQt->recordPutValue(mCheckBox, buf.c_str());
}
