// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implement legal identifier mapping support to convert Verilog names
  into legal C++ names.
*/

#include <cstdlib>
#include <cctype>
#include <cstring>
#include "util/UtMap.h"

#include "codegen/codegen.h"
#include "codegen/CodeNames.h"
#include "compiler_driver/CarbonContext.h"
#include "util/AtomicCache.h"
#include "codegen/PerfectHash.h"
#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "util/Zstream.h"
#include "util/RandomValGen.h"

#include "emitUtil.h"

// Because we create files using class names, we have to worry about
// exceeding the NAME_MAX limits imposed by the operating systems.
//
// Solaris defaults NAME_MAX to 14, so we can't use the static form.
// Just assume we can do 200 char names - Linux, Solaris and Windows
// permit 255 or larger.  JDM -- I really would like shorter names.
// When they get long they are pretty useless from a human comprehensibility
// perspective
const UInt32 cLargestLegalComponent = 70;
const UInt32 cLargestGeneratedComponent = 55; // leave 15 chars for index

CodeNames::CodeNames (bool obfuscate, AtomicCache* cache)
  : mUuid(0),
    mCache(cache),
    mObfuscate(obfuscate)
{
  mph = new PerfectHash ();
  mRandom = new RandomValGen (12231950); // seed with my birthday...
}

CodeNames::~CodeNames()
{
  delete mph;
  delete mRandom;
}

StringAtom* CodeNames::validate(const StringAtom* name)
{
  // Check for invalid characters or reserved words.
  UtString buf(name->str());
  bool uniquify = false;

  if (buf.size() > cLargestLegalComponent)
  {
    buf.resize(cLargestGeneratedComponent);
    uniquify = true;
  }

  // Replacing illegal characters with '_'
  for (size_t i = 0; i < buf.size(); ++i)
  {
    char c = buf[i];
    if ((!isalnum (c) && (c != '_') && (c != '$')) ||
        ((i == 0) && isdigit(c)))
    {
      buf[i] = '_';
      uniquify = true;
    }
  }
  if (uniquify)
    name = unique(buf.c_str(), true);

  // const StringAtom* is redundant -- they are all const
  return (StringAtom*) name;
} // StringAtom* CodeNames::validate

static const char sHeaderChars[] = "01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
static const size_t sNumHeaderChars = sizeof(sHeaderChars) - 1;
//! Append \a size bytes of random alphanums to the contents of \a buf
void CodeNames::genRandomIdent (UtString* buf, UInt32 size)
{
  for (UInt32 i = 0; i < size; ++i)
  {
    *buf << sHeaderChars[mRandom->URRandom(0, sNumHeaderChars - 1)];
  }
}

StringAtom* CodeNames::unique (const char *prefix, bool force) {
  UtString buf(prefix);
  size_t prefixSize = buf.size();

  // If the prefix ends with a digit, then separate it with another
  // underscore, because otherwise we alias "foo54", uid 1, with
  // "foo5", uid 41.  Laugh all you want, but that happens with
  // test/bugs/bug5449 (netEffect) and LUT conversion turned on,
  // unless we do this.  I'm mystified sure why the getIntern check did
  // not solve the problem.
  if ((prefixSize > 0) && isdigit(buf[prefixSize - 1])) {
    buf << "_";
  }

  // Assumed size of the random name - between 2 and 6 characters, gives us 15 MILLION
  // distinct names- eg (62)**6

  UInt32 size = ( 2 + mRandom->URRandom (0, 4));
  while ( force || ( buf.size() == 0 ) || mCache->getIntern(buf.c_str()) != NULL) {

    if (prefixSize != 0) {
      // Simple case - just uniquify the identifier with a number_
      buf << mUuid++ << "_";
    } else {
      // Try something more random and friendlier to gcc's symbol hashing.
      // If this name collides, make next name one character longer to reduce the likelihood
      // of colliding the second time.
      genRandomIdent (&buf, size);
      size = 1;                 // append one more random char to end if we iterate
    }
    force = false;
  } 

  // We have a buffer that isn't in the AtomicCache now.
  // Put it in the cache and give that back as the "print name"
  return mCache->intern(buf.c_str(), buf.size());
}
    
void CodeNames::print (UtOStream& out)
{
  // dump the name table as comments

  for (NameMap_t::iterator i = mNameTable.begin ();
      i != mNameTable.end (); ++i)
    {
      if ((*i).first != (*i).second)
	out << "// [" << (*i).first->str () << "] => " 
	    << (*i).second->str () << "\n";
    }
}

bool CodeNames::dumpTable(const char* filename, UtString* errmsg)
{
  ZOSTREAM(f, filename);
  if (!f)
  {
    *errmsg = f.getError();
    return false;
  }

  // dump the name table as a sed script.  When the user decrypts he gets
  // a sed script that will make any of our generated files readable.
  f << "sed \\\n";
  for (NameMap_t::iterator i = mNameTable.begin ();
      i != mNameTable.end (); ++i)
  {
    if ((*i).first != (*i).second)
    {
      f << "  -e 's"
        << "@" << (*i).first->str ()
        << "@" << (*i).second->str ()
        << "@g' \\\n";
    }
  }
  f << "\n";
  if (!f.close())
  {
    *errmsg = f.getError();
    return false;
  }
  return true;
} // void CodeNames::dumpTable

StringAtom* CodeNames::translate(const UtString* name, bool suppressObfuscate)
{
  StringAtom* sym = translate(mCache->intern(*name), suppressObfuscate);
  return sym;
}

StringAtom* CodeNames::translate(const StringAtom *name,
                                 bool suppressObfuscate)
{
  NameMap_t::const_iterator entry = mNameTable.find ((StringAtom*) name);


  if (entry != mNameTable.end ())
    // Name found in lookup table means we've already verified that
    // it's a legal one - so return its mapped printable value.
    //
    return entry->second;

  StringAtom* newName = NULL;

  // Name not already validated.
  if (mObfuscate && !suppressObfuscate)
    newName = unique("", false);

  else if (mph->is_reserved_word (name->str (), strlen (name->str ())))
    // It's a reserved word in C/C++
    newName = unique (name->str(), true);	// Replace with a name like _x
  else
    // Check for characters not permitted in C++ names and replace
    // identifier by _<number>
    //
    newName = validate(name);

  // Insert it and return the translation.

  mNameTable[(StringAtom*) name] = newName;
  mNameTable[newName] = newName; // don't re-translate newName

  return newName;
} // StringAtom* CodeNames::translate

void CodeGen::CxxBoundName::constructScopePrefix(UtString* buf) const
{
  if (mScope && mScope->getScopeType() == NUScope::eNamedDeclarationScope) {
    sConstructScopePrefix(mScope, buf);
  }
}

void CodeGen::CxxBoundName::sConstructScopePrefix(const NUScope* scope, UtString* buf)
{
  const NUScope * parent_scope = scope->getParentScope();
  if (parent_scope->getScopeType() == NUScope::eNamedDeclarationScope) {
    // Block is nested inside another, traverse hierarchy
    sConstructScopePrefix (parent_scope, buf);
  }
  (*buf) << emitUtil::block(scope->getName()) << "$";
}

// Format a bound name and return as string
CodeGen::CxxBoundName::operator const UtString () const {
  StringAtom *xName = gCodeGen->getCodeNames ()->translate (mName);
  
  UtString temp;
  constructScopePrefix(&temp);
  temp += n.mPrefix;
  temp += xName->str ();
  temp += n.mSuffix;
  return temp;
}  

//! pretty-printing names for C++ formatting
UtOStream& operator <<(UtOStream& os, const CodeGen::CxxBoundName& bn)
{
  // Check for reserved name and replace as necessary by translation.
  StringAtom *xName
    = gCodeGen->getCodeNames ()->translate (bn.mName);

  UtString scopePrefix;
  bn.constructScopePrefix(&scopePrefix);
  os << scopePrefix.c_str() << bn.n.mPrefix << xName->str() << bn.n.mSuffix;
  return os;
}

UtString& operator<<(UtString &buf, const CodeGen::CxxBoundName& bn)
{
  StringAtom *xName = gCodeGen->getCodeNames ()->translate (bn.mName);

  UtString scopePrefix;
  bn.constructScopePrefix(&scopePrefix);
  buf << scopePrefix.c_str() << bn.n.mPrefix << xName->str () << bn.n.mSuffix;
  return buf;
}
