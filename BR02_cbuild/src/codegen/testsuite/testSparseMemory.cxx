// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/carbon_shelltypes.h"
#include "codegen/SparseMemory.h"
#include "util/UtIOStream.h"
#include <iomanip>

static int errors = 0;

#define FAIL(stmt)	\
    do {UtIO::cout() << stmt << "\n"; errors++;} while (0)

static SparseStateUpdateMemory<12,UInt7, 7, 12, 5> SUM7_a;
static SparseStateUpdateMemory<12,UInt7, 7, 12, 5> SUM7_b;
static SparseDynStateUpdateMemory<UInt7, 7, 12, 5> DSUM7_a;
static SparseDynStateUpdateMemory<UInt7, 7, 12, 5> DSUM7_b;
static SparseMemory<UInt7, 7, 12, 5> MEM7;
static SparseMemory<UInt7, 7, 12, 5> MEM7_2;
static StaticTempMemory<12,SparseMemory<UInt7, 7, 12, 5>  > STM7_a (MEM7);
static StaticTempMemory<12,SparseMemory<UInt7, 7, 12, 5>  > STM7_b (MEM7);
static DynTempMemory< SparseMemory<UInt7, 7, 12, 5> > DTM7_a (MEM7);
static DynTempMemory< SparseMemory<UInt7, 7, 12, 5> > DTM7_b (MEM7);

static SparseStateUpdateMemory<16, BitVector<129>, 129, 5, 20 > SUM129;
static SparseDynStateUpdateMemory<BitVector<129>, 129, 5, 20 > DSUM129;
static SparseMemory<BitVector<129>, 129, 5, 20 > MEM129;
static SparseMemory<BitVector<129>, 129, 5, 20 > MEM129_2;
static StaticTempMemory<16, SparseMemory<BitVector<129>, 129, 5,20> > STM129 (MEM129);
static DynTempMemory<SparseMemory<BitVector<129>,129,5,20> > DTM129 (MEM129_2);

UInt32 gCarbonMemoryBitbucket[(129+31)/32] COMMON;

template <typename MemType, typename VType>
void initializeMemory (MemType *mem, VType w, int ub, int lb)
{
  int s = (ub > lb ? lb : ub);
  int e = (ub > lb ? ub : lb);
  for (int i = s; i <= e; i++) {
    mem->write (i) = w;
  }
}

void testMem ()
{
  initializeMemory (&MEM7, 7, 12, 5);
  STM7_a = (const SparseMemory<UInt7, 7, 12, 5>&)MEM7;
  DTM7_a = (const SparseMemory<UInt7, 7, 12, 5>&)MEM7;
  SUM7_a = (const SparseMemory<UInt7, 7, 12, 5>&)MEM7;
  DSUM7_a = (const SparseMemory<UInt7, 7, 12, 5>&)MEM7;
  MEM7_2 = (const SparseMemory<UInt7, 7, 12, 5>&)MEM7;
  MEM7_2 = (const StaticTempMemory<12,SparseMemory<UInt7, 7, 12, 5> >&) STM7_a;
  MEM7_2 = (const DynTempMemory<SparseMemory<UInt7, 7, 12, 5> >&) DTM7_a;
  
}
void testSUM ()
{
  initializeMemory (&MEM7, 7, 12, 5);

  // assignments to/from different state-update memory types.

  SUM7_a = (const SparseMemory<UInt7, 7, 12, 5>&)MEM7;                // static = memory
  DSUM7_a = (const SparseMemory<UInt7, 7, 12, 5>&)MEM7;               // dynamic = memory

  DSUM7_a = (const SparseStateUpdateMemory<12,UInt7, 7, 12, 5>&) SUM7_a; // dynamic = static
  DSUM7_b = (const SparseDynStateUpdateMemory<UInt7, 7, 12, 5>&) DSUM7_a; // dynamic = dynamic
  SUM7_b = (const SparseDynStateUpdateMemory<UInt7, 7, 12, 5>&)DSUM7_b; // static = dynamic
  SUM7_a = (const SparseStateUpdateMemory<12,UInt7, 7, 12, 5>&)SUM7_b; // static = static

  STM7_a = (const SparseStateUpdateMemory<12,UInt7, 7, 12, 5>&)SUM7_a; // static temp = static
  DTM7_a = (const SparseStateUpdateMemory<12,UInt7, 7, 12, 5>&)SUM7_a; // dynamic temp = static
  DTM7_b = (const SparseDynStateUpdateMemory<UInt7, 7, 12, 5>&) DSUM7_a; // dynamic temp = dynamic
  STM7_b = (const SparseDynStateUpdateMemory<UInt7, 7, 12, 5>&) DSUM7_a; // static temp = dynamic

  SUM7_a.localSync ();
  SUM7_a.sync ();
}

void testSTM ()
{
  initializeMemory (&MEM7, 7, 12, 5);

  // Flush the temp memories...
  STM7_a.executeQueuedWrites (true);
  STM7_b.executeQueuedWrites (true);
  
  STM7_a.write (6) = 127;
 STM7_b.write (6) = 126;
  if (MEM7 == STM7_b)
    FAIL ("temp memory compare fails");

  // assignments to/from different temp memory types.

  STM7_a = (const SparseMemory<UInt7, 7, 12, 5>&)MEM7;                // static = memory
  DTM7_a = (const SparseMemory<UInt7, 7, 12, 5>&)MEM7;                // dynamic = memory

  DTM7_a = (const StaticTempMemory<12,SparseMemory<UInt7, 7, 12, 5> >&) STM7_a;              // dynamic = static
  DTM7_b = (const DynTempMemory< SparseMemory<UInt7, 7, 12, 5> >&)DTM7_a;              // dynamic = dynamic
  STM7_b = (const DynTempMemory< SparseMemory<UInt7, 7, 12, 5> >&) DTM7_b;              // static = dynamic
  STM7_a = (const StaticTempMemory<12,SparseMemory<UInt7, 7, 12, 5> >&)STM7_b;              // static = static

  STM7_b.write (6) = 22;
  MEM7_2 = (const StaticTempMemory<12,SparseMemory<UInt7, 7, 12, 5> >&)STM7_b;              // a copy (not a SYNC)
  if (MEM7_2.read (6) != 22)
    FAIL ("MEM=TM update fails");
  if (MEM7 == STM7_b)
    FAIL ("Updates applied too early");

  // NOW SYNC the queued writes.
  MEM7 = (const StaticTempMemory<12,SparseMemory<UInt7, 7, 12, 5> >&)STM7_b;
  MEM7 = (const StaticTempMemory<12,SparseMemory<UInt7, 7, 12, 5> >&)STM7_a;                // memory = static
  MEM7 = (const DynTempMemory< SparseMemory<UInt7, 7, 12, 5> >&)DTM7_a;                // memory = dynamic

  if (MEM7 != STM7_a)
    FAIL ("mem != staticmem");
  else if (MEM7 != DTM7_a)
    FAIL ("mem != dynmem");
}

void testMem2 ()
{
  BitVector<129> c129 (129u);
  initializeMemory (&MEM129, c129, 12, 5);
  MEM129_2 = (const SparseMemory<BitVector<129>, 129, 5, 20>& ) MEM129;
  STM129 =  (const SparseMemory<BitVector<129>, 129, 5, 20>&)MEM129;
  DTM129 =  (const SparseMemory<BitVector<129>, 129, 5, 20>& )MEM129;
  SUM129 =  (const SparseMemory<BitVector<129>, 129, 5, 20>& )MEM129;

  DSUM129 =  (const SparseMemory<BitVector<129>, 129, 5, 20>& )MEM129;
  MEM129_2 = (const StaticTempMemory<16, SparseMemory<BitVector<129>,129, 5, 20> >&) STM129;            // regular copy
  MEM129_2 = (const DynTempMemory<SparseMemory<BitVector<129>,129,5,20> >&)DTM129;            // sync
  MEM129 = (const StaticTempMemory<16, SparseMemory<BitVector<129>,129, 5, 20> >&)STM129;             // sync

  SUM129.localSync ();
  SUM129.sync ();
  
}

typedef SparseMemory<UInt32, 32, 0, 11> SMem12;
typedef DynTempMemory<SMem12> TSMem12;
static void initMems(SMem12* m1, SMem12* m2, TSMem12* t1, TSMem12* t2) {
  // Cover all the cases of m1 | m2 being written, t1 | t2 being written
  m1->write(0) =   10;    m2->write(0) =  110;
  m1->write(1) =   11;    m2->write(1) =  111;
  m1->write(2) =   12;    m2->write(2) =  112;
  m1->write(3) =   13;    m2->write(3) =  113;
                          m2->write(4) =  114;
                          m2->write(5) =  115;
                          m2->write(6) =  116;
                          m2->write(7) =  117;
  m1->write(8) =   18;                           
  m1->write(9) =   19;                           
  m1->write(10) =  20;                           
  m1->write(11) =  21;                           

  t1->write(0) =  210;
  t1->write(1) =  211;    t2->write(1) =  311;
                          t2->write(2) =  312;

  t1->write(4) =  214;
  t1->write(5) =  215;    t2->write(5) =  315;
                          t2->write(6) =  316;

  t1->write(8) =  218;
  t1->write(9) =  219;    t2->write(9) =  319;
                          t2->write(10) = 320;
}

static void testTempCopies1() {
  // Problem discussed between Aron & I where an implementation of
  // DynTempMemory& DynTempMemory::operator=(const DynTempMemory& src)
  // was incorrect.  This code illustrates the issue I had and verifies that it
  // stays fixed.  It might be cumbersome to write this in HDL, but it's easy
  // in C.
  SMem12 m1, m2;
  TSMem12 t1(m1), t2(m2);

  initMems(&m1, &m2, &t1, &t2);
  assert(m1 != m2);

  t1 = t2;
  m1 = t1;                      // sync everything up
  m2 = t2;                      // sync everything up

  // m1 should now have all of m2 and t2's modifications, overwriting
  // everything that was in m1 before.
  assert(m1.read(0) == 110);
  assert(m1.read(1) == 311);
  assert(m1.read(2) == 312);
  assert(m1.read(3) == 113);
  assert(m1.read(4) == 114);
  assert(m1.read(5) == 315);
  assert(m1.read(6) == 316);
  assert(m1.read(7) == 117);
  assert(m1.read(8) == 0);      // default value
  assert(m1.read(9) == 319);
  assert(m1.read(10) == 320);
  assert(m1.read(11) == 0);     // default value

  assert(m1 == m2);             // whole memory comparison
  m2.write(11) = 0;
  assert(m1 == m2);
  m2.write(0) = 0;
  assert(m1 != m2);
} // static void testTempCopies

static void testTempCopies2() {
  // In this variation, we ignore t2, and just copy from m2, with
  // slightly different expected results
  SMem12 m1, m2;
  TSMem12 t1(m1), t2(m2);

  initMems(&m1, &m2, &t1, &t2);
  assert(m1 != m2);

  t1 = m2;                      // in testTempCopies1 it was t1 = t2
  m1 = t1;                      // sync everything up

  // m1 should now have all of m2 and t2's modifications, overwriting
  // everything that was in m1 before.
  assert(m1.read(0) == 110);
  assert(m1.read(1) == 111);
  assert(m1.read(2) == 112);
  assert(m1.read(3) == 113);
  assert(m1.read(4) == 114);
  assert(m1.read(5) == 115);
  assert(m1.read(6) == 116);
  assert(m1.read(7) == 117);
  assert(m1.read(8) == 0);      // default value
  assert(m1.read(9) == 0);
  assert(m1.read(10) == 0);
  assert(m1.read(11) == 0);     // default value

  assert(m1 == m2);
  m2 = t2;                      // sync everything up
  assert(m1 != m2);             // whole memory comparison
} // static void testTempCopies2

int main (void) 
{
  testSUM ();
  testSTM ();
  testMem2 ();
  testTempCopies1();
  testTempCopies2();

  return errors;
}
