// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004, 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "codegen/TristateBV.h"
#include "util/UtIOStream.h"
#include "codegen/Forcible.h"
#include "codegen/ForciblePOD.h"
#include <iomanip>

static int errors = 0;

#define FAIL(stmt)	\
    do {UtIO::cout() << stmt; errors++;} while (0)

void dummyone (int & v)
{
  v ++;
}

int dummytwo (const int &v)
{
  return v+1;
}

// Test Forcible<T> for PODs
void podtests ()
{
  ForciblePOD<UInt32> x (0,0);

  x.getData () = x.getT() + 1;

  assert (x.getData () == x.getT());


}

int main (void)
{
 // Simple tests
  podtests (); 


  // Tests of bitvectors
  Forcible<BitVector<128> > bv;
  bv.set ();			// Set the whole thing
  if (bv.count () != 128)
    FAIL ("initialization by assignment fails\n");

  bv.setMask (64, 32);		// force bv[95:64]

  BitVector<128> zero (0u);

  bv = zero;			// Try to zero masked bits

  if (bv.getData ().value (64,32) != 0xffffffff)
    FAIL ("forced bits wrong!\n");

  return errors;
}
