// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/CarbonPlatform.h"
#include "shell/carbon_shelltypes.h"
#include "util/MemManager.h"
#include "codegen/carbon.h"
#include "codegen/TristateBV.h"
#include "util/UtIOStream.h"
#include <iomanip>

static int errors = 0;

#define FAIL(stmt)	\
    do {UtIO::cout() << stmt; errors++;} while (0)

void dummyone (UInt8* v) { (*v)++;}

int main (void)
{
  // Simple tests

  Tristate<UInt8,8> i (1);

  if (i != 1)
    FAIL ("initialization and free conversion\n");

  i=i+1;

  if (i != 2)
    FAIL ("++ broken\n");

  i.set (3, eStrDrive);		//  strong write

  i.set (0, eStrPull);		//  should not change...
  if (i != 3)
    FAIL ("strength broken\n");

  dummyone ((UInt1*)&i);			// Should convert to base T
  if (i != 4)
    FAIL ("T& downcast doesn't pass reference\n");

  i = 0;
  Tristate<UInt32, 32> j (1);
  SET_BIT (j, 5,i);
  if (j != 1)
    FAIL ("SET_BIT on tristate fails. expected 1, got" << j << "\n");
  
  
  i = 1;
  SET_BIT (j, 6,i);
  if (j != 65)
    FAIL ("SET_BIT on tristate fails. expected 65, got" << j << "\n");

  // Look at parsing problem?

  j = ((j) & ~4) | (((i)<<2) & 4);

  // Harder examples

  Tristate<BitVector<64>,64> word;
  word = BitVector<64>(0U);	// Assignment

  if (word.llvalue () != 0)	// Show that basetype operands available
    FAIL ("explicit conversion errors\n");

  if (word.partsel (0,32).value () != 0)
    FAIL ("Assignment to reference fails\n");

  // Set the strength to indicate 16 bits driven.
  Tristate<BitVector<64>, 64 >::SignalT HighWord (0xFFFF0000);
  word.reset (HighWord);

  // Simple assignment ignores Strengths
  word = 1;

  Tristate<BitVector<64>, 64> Pulled255 (BitVector<64>(255u), BitVector<64>(255u));
  if (Pulled255.llvalue () != 255u
      || Pulled255.getStrength ().llvalue () != 255u)
    FAIL ("Initialized Tristate<BitVector<64>> fails\n");

  if (word.llvalue () != 1)
    FAIL ("Untristated assignment fails\n");

  
  if (word.getStrength ().llvalue () != 0xFFFF0000u)
    FAIL ("Strength setting is wrong\n");

  BitVector<64> temp (word);	// Can we make a copy?

  if (temp != word)
    FAIL ("Equality fails \n");

  return errors;
}
