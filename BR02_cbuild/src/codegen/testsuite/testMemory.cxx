// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/carbon_shelltypes.h"
#include "codegen/Memory.h"
#include "util/UtIOStream.h"
#include <iomanip>

static int errors = 0;

#define FAIL(stmt)	\
    do {UtIO::cout() << "FAILED : " << stmt << "\n"; errors++;} while (0)

static StateUpdateMemory<8,UInt14, 14, 12, 5> SUM14_a;
static StateUpdateMemory<8,UInt14, 14, 12, 5> SUM14_b;
static DynStateUpdateMemory<UInt14, 14, 12, 5> DSUM14_a;
static DynStateUpdateMemory<UInt14, 14, 12, 5> DSUM14_b;
static Memory<UInt14, 14, 12, 5> MEM14;
static Memory<UInt14, 14, 7, 0> MEM14_2;
static Memory<UInt14, 14, 12, 5> MEM14_3;
static StaticTempMemory<12,Memory<UInt14, 14, 12, 5>  > STM14_a (MEM14);
static StaticTempMemory<12,Memory<UInt14, 14, 12, 5>  > STM14_b (MEM14);
static DynTempMemory< Memory<UInt14, 14, 12, 5> > DTM14_a (MEM14);
static DynTempMemory< Memory<UInt14, 14, 12, 5> > DTM14_b (MEM14);

static StateUpdateMemory<1, BitVector<129>, 129, 12, 7> SUM129;
static DynStateUpdateMemory<BitVector<129>, 129, 12, 7> DSUM129;
static Memory<BitVector<129>, 129, 12, 7> MEM129;
static Memory<BitVector<129>, 129, 5, 0> MEM129_2;
static Memory<BitVector<129>, 129, 12, 7> MEM129_3;
static StaticTempMemory<5, Memory<BitVector<129>, 129, 12, 7> > STM129;
static DynTempMemory<Memory<BitVector<129>,129,12, 7> > DTM129;

UInt32 gCarbonMemoryBitbucket[(129+31)/32] COMMON;

template<int _ub,int _lb>
void initializeMemory (Memory<UInt14, 14, _ub, _lb> *mem, int w, int ub, int lb)
{
  int s = (ub > lb ? lb : ub);
  int e = (ub > lb ? ub : lb);
  for (int i = s; i <= e; i++) {
    mem->write(i) = MASK(UInt14, w);
  }
}

template<int _ub,int _lb>
void initializeMemoryN (Memory<UInt14, 14, _ub, _lb> *mem, int w, int ub, int lb)
{
  int s = (ub > lb ? lb : ub);
  int e = (ub > lb ? ub : lb);
  for (int i = s; i <= e; i++) {
    mem->write(i) = ~(MASK(UInt14, w));
  }
}

template<int _ub,int _lb>
void initializeMemory129_one (Memory<BitVector<129>, 129, _ub, _lb>* mem, int ub, int lb)
{
  BitVector<129> zero (0u);
  BitVector<129> neg (~zero);
  int s = (ub > lb ? lb : ub);
  int e = (ub > lb ? ub : lb);
  for (int i = s; i <= e; i++) {
    mem->write(i) = neg;
  }
}
template<int _ub,int _lb>
void initializeMemory129_zero (Memory<BitVector<129>, 129, _ub, _lb>* mem, int ub, int lb)
{
  BitVector<129> zero (0u);
  int s = (ub > lb ? lb : ub);
  int e = (ub > lb ? ub : lb);
  for (int i = s; i <= e; i++) {
    mem->write(i) = zero;
  }
}

void testMem ()
{
  printf ("testing mem14 ..\n");
  initializeMemory (&MEM14, 14, 12, 5);
  initializeMemory (&MEM14_2, 14, 7, 0);

  if (MEM14 == MEM14_2)
    ;
  else
    FAIL("memory comparison nql of UINT14 \n");

  initializeMemoryN (&MEM14_2, 14, 7, 0);

  MEM14_3 = MEM14_2;  

  if (MEM14 == MEM14_2)
    FAIL("memory comparison neq of UINT14 \n");

  if (MEM14 == MEM14_3)
    FAIL("memory comparison neq of UINT14 \n");

  MEM14 = (const Memory<UInt14, 14,7,0>&) MEM14_2;
  if (MEM14 == MEM14_2)
    ;
  else
    FAIL("memory comparison eql of UINT14 \n");
}
void testSUM ()
{
  initializeMemory (&MEM14, 14, 12, 5);

  // assignments to/from different state-update memory types.

  SUM14_a = (const Memory<UInt14, 14, 12, 5>&) MEM14;              // static = memory
  DSUM14_a = (const Memory<UInt14, 14, 12, 5>&) MEM14;             // dynamic = memory

  DSUM14_a = (const StateUpdateMemory<8,UInt14, 14, 12, 5>&) SUM14_a;           // dynamic = static
  DSUM14_b = (const DynStateUpdateMemory<UInt14, 14, 12, 5>&)DSUM14_a;          // dynamic = dynamic
  SUM14_b = (const DynStateUpdateMemory<UInt14, 14, 12,5>&) DSUM14_b;           // static = dynamic
  SUM14_a = (const StateUpdateMemory<8, UInt14, 14, 12, 5>&)SUM14_b;            // static = static

  STM14_a = (const StateUpdateMemory<8,UInt14, 14, 12, 5>&)SUM14_a;            // static temp = static
  DTM14_a = (const StateUpdateMemory<8,UInt14, 14, 12, 5>&)SUM14_a;            // dynamic temp = static
  DTM14_b = (const DynStateUpdateMemory<UInt14, 14, 12, 5>&)DSUM14_a;           // dynamic temp = dynamic
  STM14_b = (const DynStateUpdateMemory<UInt14, 14, 12, 5>&)DSUM14_a;           // static temp = dynamic
}

void testTM ()
{
  initializeMemory (&MEM14, 14, 12, 5);

  // assignments to/from different temp memory types.

  STM14_a = (const Memory<UInt14, 14, 12, 5>&)MEM14;              // static = memory
  DTM14_a = (const Memory<UInt14, 14, 12, 5>&)MEM14;              // dynamic = memory

  DTM14_a = (const StaticTempMemory<12,Memory<UInt14, 14, 12, 5> >&)STM14_a;            // dynamic = static
  DTM14_b = (const DynTempMemory<Memory<UInt14, 14, 12,5> >&) DTM14_a;            // dynamic = dynamic
  STM14_b = (const DynTempMemory<Memory<UInt14, 14, 12,5> >&) DTM14_b;            // static = dynamic
  STM14_a = (const StaticTempMemory<12,Memory<UInt14, 14, 12, 5> >&)STM14_b;            // static = static

  MEM14 = (const StaticTempMemory<12,Memory<UInt14, 14, 12, 5> >&)STM14_a;              // memory = static
  MEM14 = (const DynTempMemory<Memory<UInt14, 14, 12,5> >&) DTM14_a;              // memory = dynamic

  // Test compares
  if (MEM14 != STM14_a)
    FAIL ("MEM != STM14");

  if (MEM14 != DTM14_a)
    FAIL ("MEM != DTM14_a");
}


void testMem2 ()
{
  printf ("testing mem129 ..\n");
  initializeMemory129_one (&MEM129, 12, 7);

  MEM129_2 = (const Memory<BitVector<129>, 129, 12, 7>&)MEM129;
  if (MEM129_3.getStorage() == MEM129.getStorage())
    FAIL("BitVector memories should be different locations");
  MEM129_3 = (const Memory<BitVector<129>, 129, 12, 7>&)MEM129;
  if (MEM129_3.getStorage() == MEM129.getStorage())
    FAIL("Memory of BitVector operator= misfunction");

  if (MEM129_2 == MEM129)
    ;
  else
    FAIL("memory comparison eql of BitVector<129>\n");


  initializeMemory129_zero (&MEM129, 12, 7);

  // not equal

  if (MEM129_2 == MEM129)
    FAIL("BitVector memory comparison of diff _MSW/_LSW for neq \n");

  if (MEM129_3 == MEM129)
    FAIL("BitVector memory comparison of same _MSW/_LSW for neq \n");
}

typedef Memory<UInt32, 32, 0, 11> Mem12;
typedef DynTempMemory<Mem12> TMem12;
static void initMems(Mem12* m1, Mem12* m2, TMem12* t1, TMem12* t2) {
  // Cover all the cases of m1 | m2 being written, t1 | t2 being written
  m1->write(0) =   10;    m2->write(0) =  110;
  m1->write(1) =   11;    m2->write(1) =  111;
  m1->write(2) =   12;    m2->write(2) =  112;
  m1->write(3) =   13;    m2->write(3) =  113;
                          m2->write(4) =  114;
                          m2->write(5) =  115;
                          m2->write(6) =  116;
                          m2->write(7) =  117;
  m1->write(8) =   18;                           
  m1->write(9) =   19;                           
  m1->write(10) =  20;                           
  m1->write(11) =  21;                           

  t1->write(0) =  210;
  t1->write(1) =  211;    t2->write(1) =  311;
                          t2->write(2) =  312;

  t1->write(4) =  214;
  t1->write(5) =  215;    t2->write(5) =  315;
                          t2->write(6) =  316;

  t1->write(8) =  218;
  t1->write(9) =  219;    t2->write(9) =  319;
                          t2->write(10) = 320;
}

static void testTempCopies1() {
  // Problem discussed between Aron & I where an implementation of
  // DynTempMemory& DynTempMemory::operator=(const DynTempMemory& src)
  // was incorrect.  This code illustrates the issue I had and verifies that it
  // stays fixed.  It might be cumbersome to write this in HDL, but it's easy
  // in C.
  Mem12 m1, m2;
  TMem12 t1(m1), t2(m2);

  initMems(&m1, &m2, &t1, &t2);
  assert(m1 != m2);

  t1 = t2;
  m1 = t1;                      // sync everything up
  m2 = t2;                      // sync everything up

  // m1 should now have all of m2 and t2's modifications, overwriting
  // everything that was in m1 before.
  assert(m1.read(0) == 110);
  assert(m1.read(1) == 311);
  assert(m1.read(2) == 312);
  assert(m1.read(3) == 113);
  assert(m1.read(4) == 114);
  assert(m1.read(5) == 315);
  assert(m1.read(6) == 316);
  assert(m1.read(7) == 117);
  assert(m1.read(8) == 0);      // default value
  assert(m1.read(9) == 319);
  assert(m1.read(10) == 320);
  assert(m1.read(11) == 0);     // default value

  assert(m1 == m2);             // whole memory comparison
  m2.write(11) = 0;
  assert(m1 == m2);
  m2.write(0) = 0;
  assert(m1 != m2);
} // static void testTempCopies

static void testTempCopies2() {
  // In this variation, we ignore t2, and just copy from m2, with
  // slightly different expected results
  Mem12 m1, m2;
  TMem12 t1(m1), t2(m2);

  initMems(&m1, &m2, &t1, &t2);
  assert(m1 != m2);

  t1 = m2;                      // in testTempCopies1 it was t1 = t2
  m1 = t1;                      // sync everything up

  // m1 should now have all of m2 and t2's modifications, overwriting
  // everything that was in m1 before.
  assert(m1.read(0) == 110);
  assert(m1.read(1) == 111);
  assert(m1.read(2) == 112);
  assert(m1.read(3) == 113);
  assert(m1.read(4) == 114);
  assert(m1.read(5) == 115);
  assert(m1.read(6) == 116);
  assert(m1.read(7) == 117);
  assert(m1.read(8) == 0);      // default value
  assert(m1.read(9) == 0);      // default value
  assert(m1.read(10) == 0);     // default value
  assert(m1.read(11) == 0);     // default value

  assert(m1 == m2);
  m2 = t2;                      // sync everything up
  assert(m1 != m2);             // whole memory comparison
} // static void testTempCopies2


// In bug6201, the copy of a StaticTempMemory to a Memory that is not
// the first memory's master would not occur if the STM had a descending
// range (e.g. [1:0] failed, where [0:1] would work).
static void
testBug6201()
{
  // Data types and values are lifted from the original testcase
  StaticTempMemory<3, Memory<CarbonUInt19, 19, 1, 0> > stm;
  Memory<CarbonUInt19, 19, 1, 0> baseMem;
  Memory<CarbonUInt19, 19, 1, 0> targetMem;

  stm.putBaseMemory( &baseMem );
  stm.write(0) = (CarbonUInt19)0x7ffff;
  targetMem = stm;
  if ( targetMem.read(0) != (CarbonUInt19)0x7ffff )
    FAIL("Bug6201 StaticTempMemory->Memory (descending bounds) copy failed.\n");

  // Verify that ascending ranges copy properly, too
  StaticTempMemory<3, Memory<CarbonUInt19, 19, 0, 1> > stm_r;
  Memory<CarbonUInt19, 19, 0, 1> baseMem_r;
  Memory<CarbonUInt19, 19, 0, 1> targetMem_r;

  stm_r.putBaseMemory( &baseMem_r );
  stm_r.write(0) = (CarbonUInt19)0x7ffff;
  targetMem_r = stm_r;
  if ( targetMem_r.read(0) != (CarbonUInt19)0x7ffff )
    FAIL("Bug6201 StaticTempMemory->Memory (ascending bounds) copy failed.\n");

}

int main (void) 
{
  testMem ();
  testMem2 ();
  testSUM ();
  testTM ();
  testTempCopies1();
  testTempCopies2();
  testBug6201();

  return errors;
}
