// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Specialized translation support functions for codegen to create the hidden
  .carbon directory where .cxx, .h, .lo, and .la files are kept.
*/

#include "codegen/codegen.h"

#include <limits.h>
#include <cctype>
#include "compiler_driver/CarbonContext.h"
#include "util/ArgProc.h"
#include "util/OSWrapper.h"
#include "util/CbuildMsgContext.h"
#include "util/OSWrapper.h"

// Setup the variables needed to run the backend compile
void CodeGen::setupBuildDirectoryVars (void)
{
  const char *targetlib;

  // Remember our current working directory
  OSGetCurrentDir(&mcurrentDirectory);

  // Check what the command line says about -o
  ArgProc *args = mCarbonContext->getArgs();

  args->getStrValue ("-o", &targetlib);


  OSParseFileName (targetlib, &mTargetDirectory, &mTargetFile);

  // Find the filename without any extension.
  mTargetName.assign (mTargetFile, 0, mTargetFile.rfind ('.'));

  // Construct a simulation name to tag the external interface with.
  // If we said "-o libdesign.a", then the tag should be 'design'.
  INFO_ASSERT (mTargetName.substr (0,3) == "lib", mTargetName.c_str());
  mIfaceTag = mTargetName.substr (3);


  // The interface-tag must contain valid characters for a C++ identifier
  // So replace anything but [A-Za-z0-9_$] by legal chars.
  bool giveWarning = false;
  for(UtString::size_type i = 0;
      i < mIfaceTag.size ();
      ++i)
    {
      char c = mIfaceTag[i];
      if (!isalnum (c) && (c != '_') && (c != '$'))
	{
	  mIfaceTag[i] = '_';
	  giveWarning = true;
	}
    }

  if (giveWarning)
    mMsgContext->CGFileNameChanged (mTargetName.c_str (), mIfaceTag.c_str ());

  // Change any non-absolute path into an absolute one!
  if (mTargetDirectory.empty() ||
      ! OSIsDirectoryDelimiter(mTargetDirectory[0]))
    {
      OSConstructFilePath(&mTargetDirectory, mcurrentDirectory.c_str(),
                          mTargetDirectory.c_str());
    }

  UtString dirName(".carbon.");
  dirName << mTargetName;
  OSConstructFilePath(&mbuildDirectory, mTargetDirectory.c_str(),
                      dirName.c_str());
}

/*!
  Set the current working directory to a hidden directory so that all created
  files end up obscured.  The argument to '-o' has already been validated, so
  we can assume that it's of the form \<path\>/lib\<XXXXX\>.[a|so]
 */
void CodeGen::setupBuildDirectory (void)
{
  setupBuildDirectoryVars();

  // Fully specified name-space for this Carbon Model
  mNamespace.append (mIfaceTag.c_str ());


  // Try to create the build directory.

  UtString fileMsg (mbuildDirectory);
  fileMsg += ":";

  UtString buildDirStat;
  bool dirExists = true;
  int statResult = OSStatFile(mbuildDirectory.c_str (), "edrw", &buildDirStat);
  if (statResult == 1) {
    UtString reason;
    int status = OSDeleteRecursive (mbuildDirectory.c_str (), &reason);


    if (status != 0) {
      UtString msg("Unable to remove directory ");
      msg << fileMsg << reason;
      mMsgContext->CGFail (msg.c_str());
      exit(1);
    }
      
    // Only way out without exiting is to successfully delete
    dirExists = false;
  } else if (statResult == -1) {
    // Some systemic error...
    mMsgContext->CGFail (buildDirStat.c_str());
    exit (1);
  }
  
  dirExists &= (buildDirStat[0] == '1');
  if (! dirExists)
    {
      UtString chdirErr;
      // Try to create it
      if (OSMkdir (mbuildDirectory.c_str (), 0770, &chdirErr))
        mMsgContext->CGFail (chdirErr.c_str());
      else if (OSChdir (mbuildDirectory.c_str (), &chdirErr))
        mMsgContext->CGFail (chdirErr.c_str());
      else
        return;
      exit (1);
    }
  else if (buildDirStat[1] == '0')
    {
      UtString msg(mbuildDirectory);
      msg << "  exists but isn't a directory.";
      mMsgContext->CGFail (msg.c_str());
      exit (1);
    }
  

  // We created it as an empty direc
  if (OSChdir (mbuildDirectory.c_str (), &fileMsg))
    {
      mMsgContext->CGFail (fileMsg.c_str ());
      exit (1);
    }
}


void CodeGen::cleanupBuildDirectory (void)
{

#if 0
  // We should destroy our scratch directory and all its contents, but not NOW, please!
  // -profileGenerate/-profileUse depends on this directory's existence.
  if (rmdir (mbuildDirectory.c_str ()))
    {
      mMsgContext->CGFail (sys_errlist[errno]);
    }
#endif

  chdirToOrigDir();
}

void CodeGen::chdirToBuildDir (void)
{
  UtString err;
  if (OSChdir (mbuildDirectory.c_str (), &err))
    mMsgContext->CGFail (err.c_str());
}

void CodeGen::chdirToOrigDir (void)
{
  UtString err;
  if (OSChdir (mcurrentDirectory.c_str (), &err))
    mMsgContext->CGFail (err.c_str());
}
