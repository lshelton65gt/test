// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*
 * \file
 *  Concatenation related emitCode routines.
 */
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
//#include "nucleus/NUBlock.h"
#include "nucleus/NUNetSet.h"

#include "reduce/Fold.h"

#include "util/DynBitVector.h"
#include "util/CbuildMsgContext.h"

#include "emitUtil.h"
#include "codegen/UnrollConcat.h"

using namespace emitUtil;


// Concat's can't code themself - must return a special flag to warn the
// assignment that special action is required.
//
CGContext_t
NUConcatLvalue::emitCode (CGContext_t context) const
{
  NU_ASSERT(!isDirtyOk(context), this);

  if (getNumArgs () == 1)
    return (getArg(0)->emitCode(context));
  else
    NU_ASSERT (getNumArgs () > 1, this);

  return eCGDelayed;
}

//! Used privately to fool unrollConcat()
//!
typedef std::pair<const NUExpr *, size_t> ConcatShift_t;

// Extract constant members from a concatenation and do as one big blob.
static UInt64
ExtractSmallConcatConstants(UtVector<ConcatShift_t> *shiftTable, UInt32 *n_extracted)
{
  UInt64 value = 0;
  *n_extracted = 0;
  for(UtVector<ConcatShift_t>::reverse_iterator i = shiftTable->rbegin();
      i!=shiftTable->rend(); ++i)
    {
      UInt64 term;
      const NUExpr *exp = dynamic_cast<const NUExpr*>((*i).first);

      if (exp && isCTCE(exp, &term)) {
        (*n_extracted)++;
        value |= term;
      }

      value <<= (*i).second;
    }

  return value;
}

// Find constants within a large concatenation and initially populate
// a static constant object.
// Returns FALSE if no constant members.
static bool
ExtractLargeConcatConstants (UtVector<ConcatShift_t> *shiftTable,
			     DynBitVector *value)
{
  bool found = false;		// Did we encounter any constants?

  for(UtVector<ConcatShift_t>::reverse_iterator i = shiftTable->rbegin();
      i!=shiftTable->rend(); ++i)
    {
      DynBitVector term;

      const NUExpr *exp = dynamic_cast<const NUExpr*>((*i).first);

      if (exp && isCTCE(exp, &term))
	{
	  // Mixed mode operations aren't supported on DynBitVector, so
	  // change the term to the size of the result object.
	  term.resize (value->size ());
	  *value |= term;
	  found = true;
	}

      *value <<= (*i).second;
    }

  return found;
}

// Look for sign-extension candidates

//
enum eSignCases {
  eNone = 0,	//    No optimization possible
  eSimple = 1,  //    { (e[k]? MASK(k):0), e[k:0]}
  eMessy = 2	//    { (e[k]? MASK(k+1):0), e[k-1:0]}
};


#ifdef BUG12917FIXED
// this function is probably incorrect in the way it calculates eSimple
static enum eSignCases
sCheckSignExtension (const NUConcatOp* cat)
{
  if (cat->getRepeatCount () != 1 || cat->getNumArgs () != 2)
    return eNone;

  int totWidth = cat->getBitSize ();
  if (totWidth > 64)
    // With Extender<O,I,osize,isize>() we can handle pretty much anything machine
    // addressable
    return eNone;
  

  // Check that we're trying to sign-extend
  const NUTernaryOp *signExpr
    = dynamic_cast<const NUTernaryOp *>(cat->getArg (0));

  if (not signExpr
      || (signExpr->getOp () != NUOp::eTeCond)
      || (signExpr->getArg (0)->getType () != NUExpr::eNUVarselRvalue))
    return eNone;

  // Figure out which bit we're checking...
  const NUVarselRvalue* bs = dynamic_cast<const NUVarselRvalue*>(signExpr->getArg (0));
  if (not bs)
    return eNone;

  if (not bs->isConstIndex ())
    return eNone;
  
  ConstantRange bounds (bs->getIdentExpr ()->getBitSize () - 1, 0);
  SInt32 bitNum = ComputeIndex (bs->getConstIndex (), bounds);

  // Check that sign-expr is bit widener...
  const NUConst *ct = signExpr->getArg(1)->castConst();
  if (not ct || not ct->isOnes ())
    return eNone;

  const NUConst *cf = signExpr->getArg(2)->castConst();
  if (not cf || not cf->isZero ())
    return eNone;

  // Check that we're really accessing a variable to find the putative sign
  //
  if (not bs->getIdentExpr ()->isWholeIdentifier ())
    return eNone;

  // Also check that 2nd argument to concat is a range ending in the
  // bit being tested, or a simple variable.
  //
  switch (cat->getArg (1)->getType ())
    {
    case NUExpr::eNUIdentRvalue:
      {
	const NUIdentRvalue *rv
	  = dynamic_cast<const NUIdentRvalue*>(cat->getArg (1));

        if (*(rv) != *(bs->getIdentExpr ()))
          // Must be equivalent expressions
	  return eNone;

	// Make sure we're testing the MSB of this source
	SInt32 varSize = rv->getIdent ()->getBitSize ();
	if (varSize != (bitNum + 1))
	  return eNone;
      }
      break;

    case NUExpr::eNUVarselRvalue:
      {
	const NUVarselRvalue *rv
	  = dynamic_cast<const NUVarselRvalue*>(cat->getArg (1));
        
	if (*(rv->getIdentExpr ()) != *(bs->getIdentExpr ())
          || not rv->isConstIndex ())
	  return eNone;
	
	// Make sure we're testing the MSB
	ConstantRange range (*rv->getRange ());

	const ConstantRange bounds (rv->getIdentExpr ()->getBitSize ()-1, 0);
	SInt32 msbBit = ComputeIndex (range.getMsb (), bounds);
	if (msbBit == bitNum)
	  // Simplest case {{k{bit[s]}}, bit[s:low]}
	  return eSimple;
	else if ((msbBit+1) == bitNum)
	  //      {{k+1{bit[s]}}, bit[s-1:low]}
	  return eMessy;
      }
      break;

    default:
      return eNone;
    }

  return eNone;
}
#endif

// Construct
//	((op[0]<< bitsize(op[1])) | op[1]) << bitsize(op[2])....
CGContext_t
NUConcatOp::emitCode (CGContext_t context ) const
{
  CGContext_t inContext = context;
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  NU_ASSERT(!isDirtyOk(context), this);

  if (isWholeIdentifier())
    {
      return coerceRvalue(getWholeIdentifier(), NULL, getBitSize(),
			  isSignedResult (), inContext);
    }

  UtOStream &out = gCodeGen->CGOUT ();
  const NUExpr *exp;
  CGContext_t rctxt = 0;

  // Check for special cases first...

  if (isFlowValue (inContext)) {
    NUExprCLoop catops (loopExprs ());
    out << TAG;
    emitCodeList (&catops, inContext, "(", "||", ")");
    return eCGVoid;
  }

  // Look for { {16{BIT[15]}}, BIT[15:0]} and generate simple sign extensions
  // (this is actually transformed already into:
  //	{ BIT[15]?65535:0, BIT[15:0]}
  //
  // bug 12917: for now, disable this kind of optimization 
  //12917 enum eSignCases concat_pattern = sCheckSignExtension(this); 
  enum eSignCases concat_pattern = eNone;
  switch (concat_pattern)
    {
    case eMessy:
      // Need a different partselect...
      {
	out << TAG << "Extender<" << CBaseTypeFmt (getBitSize (), eCGVoid, true) // SInt<k>
	    << "," << CBaseTypeFmt (getArg (1)->getBitSize () + 1)
	    << "," << getBitSize () << "," << (getArg (1)->getBitSize () + 1)
	    << ">(";

	const NUVarselRvalue *ps = dynamic_cast<const NUVarselRvalue*>(getArg (1));
	
        NU_ASSERT (ps->isConstIndex (), this);
	ConstantRange range (*ps->getRange ());

	range.setMsb (1 + range.getMsb ());
	
	NUVarselRvalue part (ps->getIdent (), range, getLoc ());
	part.resize (getBitSize ());
	rctxt = part.emitCode (context);
	
	out << ")()";
	return Precise (rctxt);
      }
      break;
      
    case eSimple:
      // we have a winner! Cast to wider signed type
      gCodeGen->addExtraHeader (gCodeGen->cCarbonRunTime_h_path);
      out << TAG << "Extender<"
	  << CBaseTypeFmt (getBitSize (), eCGVoid, true) // SInt<k>
	  << "," << CBaseTypeFmt (getArg (1)->getBitSize ()) // UInt<m>
	  << "," << getBitSize () << "," << getArg (1)->getBitSize ()
	  << ">(";

      rctxt = getArg (1)->emitCode (context);
      out << ")()";
      return Precise (rctxt);

    case eNone:
    default:
      if (getRepeatCount () > 1
          && getNumArgs () == 1
          && (exp = dynamic_cast<const NUExpr*>(getArg (0)))
          && (exp->getBitSize () == 1))
      {
        if (NeedsATemp (context))
        {
          context &= ~eCGNeedsATemp;
          rctxt = eCGIsATemp;
          out << TAG << CBaseTypeFmt (getRepeatCount (), eCGVoid, exp->isSignedResult());
        }

        out << TAG << "((";
        exp->emitCode (context);

        UInt32 rptWidth = getRepeatCount ();
        if (rptWidth > LLONG_BIT)
          // ((e) ? BitVector<66>(cv$1) : BitVector<66>(cv$2))
        {
          DynBitVector ones (rptWidth);
          ones.set ();      	     // all ones..
          DynBitVector zeros (rptWidth); // Default is initialized to zero
          
          out << ") ? ";
          emitConstant (out, ones, rptWidth, eCGVoid, false);
          out << " : ";
          emitConstant (out, zeros, rptWidth, eCGVoid, false);
          out << ")";
        }
        else
          // Either a zero or an all-one's bitset
          // ((e) ? UInt7(127) : UInt7(0))
        {
          out << ") ? " << CBaseTypeFmt (rptWidth) << "(";
          genMask(out, rptWidth);
          out << "):"
              << TAG << CBaseTypeFmt (rptWidth) << "(0))";
        }

        return Precise (rctxt);
      }
    }

  // Unroll the operands
  UtVector<ConcatShift_t> shiftTable;

  unrollConcat<NUConcatOp, CRepeatRHS, ConcatShift_t>(*this, &shiftTable, 0, gCodeGen->getMsgContext ());

  size_t totWidth = ((determineBitSize () > getBitSize())? 
		     determineBitSize() : 
		     getBitSize());

  if (totWidth <= LLONG_BIT)
    {
      // We're always going to build a POD constructor for this concat to protect it
      // from getting sign/size information from its leading term.
      //
      if (NeedsATemp (context))
	{
	  rctxt = eCGIsATemp;
	  context &= ~eCGNeedsATemp;
	}

      out << TAG << "static_cast <" << CBaseTypeFmt (totWidth, eCGDeclare, isSignedResult()) << ">";
      out << "(";
      UInt32 n_extracted;
      UInt64 constVal = ExtractSmallConcatConstants (&shiftTable, &n_extracted);

      // Emit enough left parens to match all shifts and or's
      //
      out << UtIO::setw (2*(shiftTable.size () - n_extracted)) << UtIO::setfill('(') << "";

      UInt32 n_terms = 0;
      for(UtVector<ConcatShift_t>::reverse_iterator i = shiftTable.rbegin ();
	  i != shiftTable.rend (); ++i)
	{
	  size_t shiftCount = (*i).second;
          if (isCTCE ((*i).first)) {
            // Constants are folded into constVal.  They are all or'ed in at
            // the tail.
            if (n_terms > 0) {
              out << " << " << shiftCount;
            }
            continue;
          }
          if (n_terms++ > 0) {
            out << " | ";
          }
          bool isSigned = (*i).first->isSignedResult ();
	  out << "(";
 	  out << TAG << "static_cast <" << CBaseTypeFmt (totWidth, eCGDeclare, isSigned) << ">";
	  out << "(";
	  CGContext_t ctxt = (*i).first->emitCode (context);
	  if (!isPrecise (ctxt) || isSigned)
            // Don't allow dirty bits or sign-extensions to impact total concat
	    mask ((*i).first->getBitSize (), "&");
	  out << "))";

	  if (shiftCount)
	    out << ") << " << shiftCount << ")";
	  else
	    out << "))";
	}
      if (n_terms == 0) {
        // output the constant all by itself
        emitConstant (out, constVal, totWidth, eCGVoid, false);
      } else if (constVal != 0) {
        // OR the constant into the concat
        out << " | ";
        emitConstant (out, constVal, totWidth, eCGVoid, false);
      }
    }
  else
    // Large bitvectors.
    // 
    // dst = *reinterpret_cast<const BitVector<N>*>(concatstuff.base(n))
    {
      gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path);

      out << TAG << "(";

      DynBitVector constTerms (totWidth);

      // We need to copy-construct the constant part of the concat if it contains
      // literal values or if it needs to be larger than the self-determined size
      // because of the context that the concat appears in.
      //
      bool needsConst = ExtractLargeConcatConstants (&shiftTable, &constTerms)
        || (getBitSize () > determineBitSize ());

      if (NeedsATemp (context))
	// We are already generating a temp, but it's nominally
	// a "const BitVector<N>".  Remove the const if we need
	// a temporary at the next level up of the expression.
	{
	  rctxt |= eCGIsATemp;	// Returning a temp for sure!
	  context &= ~eCGNeedsATemp; // Don't pass down
	  out << "*const_cast<" << CBaseTypeFmt (totWidth, eCGVoid, isSignedResult())
	      << "*>";
	}
      else
	out << "*";

      out << "(reinterpret_cast<const " << CBaseTypeFmt (totWidth, eCGVoid, isSignedResult())
	  << "*>(";

      out << UtIO::setw (shiftTable.size ()) << UtIO::setfill('(') << "";

      // Start with a BitVector<K>() temporary
      out << CBaseTypeFmt (totWidth, eCGVoid, isSignedResult()) << "(";
      if (needsConst) {
	// We had constant components to this catenation, copy-construct
	// from the constant into a BitVector temporary
	//
        out << TAG;
	emitConstant (out, constTerms, constTerms.size (), eCGVoid, false);
      }
      out << ")";

      // This loop is different because we start with the LOW-order bits
      // of the concatenation and partsel our way up, keeping track of the
      // total amount we've partsel-ed and then calling the .base() method
      // to get a valid BitVector<>& for the assignment...
      size_t totalPositionOffset = 0;
      size_t currentPos = 0;	// Remember where next piece goes...
      for (UtVector<ConcatShift_t>::iterator i = shiftTable.begin ();
	   i != shiftTable.end (); ++i)
	{
	  const NUExpr *expr = (*i).first;

	  // Remember how far from the start of the bitvector we've advanced
	  // a reference.
	  totalPositionOffset += currentPos;

	  size_t size = expr->getBitSize ();

	  if (currentPos == 0) {
            // Unchecked because we know position and size are constants
            // and to get this far they must be legit
            NU_ASSERT(currentPos + size <= getBitSize(), this);
	    out << ".lpartsel_unchecked (" << currentPos << ", " << size << ")" << TAG;
          }
	  else {
            // special form that advances by the size of the previous item
	    out << ".lpartsel_advance (" << size << ")" << TAG;
          }

	  if (!isCTCE (expr))
	    {
	      out << TAG << "=";
	      expr->emitCode (context& ~eCGNeedsATemp);
	    }
	      
	  out << ")";

	  currentPos = size;	// Next piece comes right after...
	}

      // Lastly recover a BitVector<> & so we can perform the assignment
      out << ".base<" << CBaseTypeFmt (totWidth, eCGVoid, isSignedResult ())
          << " >(" << totalPositionOffset << ")))" << TAG;

      out << ")";			// Match whole expression parentheses
      if (totWidth > getBitSize())
	emitUtil::genBVpartsel (out, 0, getBitSize());

      return(Precise(rctxt));
    }

  out << ")";			// Match whole expression parentheses
  return ((totWidth > getBitSize())? Imprecise(rctxt): Precise(rctxt));
}
/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
