// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 /*
 * \file
 * Centralized implementation of all code emitters for classes derived from
 * class NUBase.
 *
 * We emit code by passing down context flags that guide each method
 * in establishing what kinds of code are needed (declarations,
 * expressions, or various kinds of names. 
 *
 * Context flags are returned for those places that need to be aware of
 * the generated context for a subordinate tree.  (See the NUAssign and
 * NUIdentLvalue emitCode() methods for an example.
 *
 * A very few routines do complex analysis of their operands or are
 * aware of the existance of multiple output files (see NUModule::emitCode())
 * 
 */

#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTriRegInit.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUCModelInterface.h"

#include "util/StringAtom.h"
#include "util/CbuildMsgContext.h"
#include "util/AtomicCache.h"

#include "iodb/IODBNucleus.h"

#include "compiler_driver/CarbonContext.h"
#include "codegen/CGAuxInfo.h"
#include "codegen/ScheduleBucket.h"

#include "emitUtil.h"
#include "emitTFCall.h"
#include "RankNets.h"

#include <algorithm>
#include <functional>

using namespace emitUtil;

// Forward declared
static void
sVisitConstructor (const NUModule* mod);

// Is this net a port that is not in the constructor argument list?
// Anything that's allocated isn't a reference parameter, and anything
// that was port-aliased to a lower-level allocated variable isn't
// constructed either!
//
static bool
isNotConstructorArg (const NUNet *n)
{
  if (n->isAllocated ())
    // If it was allocated here, then it can't be a constructor parameter.
    return true;
    
  if (! isNetReferenced(n))
    return true;		// If it's never used don't construct it

  if (n->getCGOp()->isPortAllocated())
    return true;

  // We need an instantiation to pass in a reference to initialize
  // our object to connect the port to the memory representation of
  // the net.
  return false;
}

static bool
isNotReferenceParam (const NUNet *n)
{
  if (isNotConstructorArg(n)) 
    return true;

  if (n->getCGOp()->isPortAliased())
    return true;

  return false;
}


// Was this parameter actually generated as a constructor argument?
// No fooling around!
static bool
isNotConstructorPort (const NUPortConnection *p)
{
  const NUNet *formal = p->getFormal ();

  const CGAuxInfo* cgop = formal->getCGOp ();

  return (not cgop->isConstructed ());
}

static void
sVisitModuleForConnections (const NUModule *mod)
{
  gCodeGen->getCGOFiles()->restoreExtraHeaders (mod);

  sVisitConstructor (mod);

  gCodeGen->getCGOFiles()->saveExtraHeaders (mod);
}

// Initialization syntax for a constructor member initialization
// of a reference.
//
static void
sConstructMemberInit (const NUNet* formal, const NUNet* actual, CGContext_t context, bool input)
{
  UtOStream& out = gCodeGen->CGOUT ();

  if (not isConstructor (context))
  {
    if (not input)
    {
      // If we refer to this actual in any code here or in a higher
      // level class, then we must mark the formal in the lower
      // level as being used.
      if (isNetReferenced(actual)) {
        formal->emitCode (eCGMarkNets);
      }
      
      if (isNotConstructorArg (formal))
        return;
      
      if (isNetReferenced(formal)) {
        actual->emitCode (eCGMarkNets);
      }
    } else if (isNetReferenced(formal)) {
      // Input passed to module that does reference the value?
      // Mark the net passed as being referenced too
      actual->emitCode (eCGMarkNets);
    }
    
  }
  else
  {    
    // Either generating the prototype (for the .h) or the macro defining the
    // constructor's implementation.
    //
    // generating the initializer for a class instance in the containing classes
    // default constructor.
    
    // Check the types of the formal and actual because we may need to
    // coerce the input when we're called with a reference to a StateUpdate,
    // but the instantiated module expects a simple value.
    //
    if (not isTypeCompatible (actual, formal))
    {
      context &= ~(eCGDeclare | eCGLvalue);
      
      if ((gCodeGen->isTristate(actual) && !gCodeGen->isTristate(formal)) ||
          (netIsDeclaredForcible(actual) && !netIsDeclaredForcible(formal)))
      {
        // Need to get at the data member, use the & operator to accomplish that.
        // Note there is NO bug here anymore.  We are using getT which returns
        // a const reference, and so there is no danger of a submodule writing
        // to the tristate value without updating the strength.
	//
	// Bug 12540 changed the way input ports are marked undriven. Now if a input
	// port has an actual expression associated with it, it is then considered
	// driven, no matter if the actual itself is undriven. This changed the way 
	// ports were coerced in past, leading to situation where the formal port
	// could adquire the property of being hierarchically written. This check was
	// missing from the if statement below, leading to codegen errors, since the
	// port can no longer be constant if its hierarchically written.

        if (formal->isWritten () || formal->isTriWritten () || formal->isHierRefWritten()) {
          out << TAG;
          gCodeGen->getMsgContext ()->CGWriteCoercedPort (&(actual->getLoc ()),
                                                          formal->getName ()->str (),
                                                          actual->getName ()->str ());
                                                          
          // Tristate::getT() returns a const reference.  If we issue an alert, we can
          // const_cast that away...
          out << "const_cast<" << CType (actual) << "::SignalT&>(";
        }
        out << TAG;
        actual->emitCode (context);
        
        out << TAG << ".getT()";
        // a second ::getT() call is necessary if the actual is both forcible and tristate.
        if ((gCodeGen->isTristate(actual) && !gCodeGen->isTristate(formal)) &&
            (netIsDeclaredForcible(actual) && !netIsDeclaredForcible(formal)))
        {
          out << TAG << ".getT()";
        }

        if (formal->isWritten () || formal->isTriWritten () || formal->isHierRefWritten())
          out << ")";           // close the const_cast<>()
      }
      else
      {
        if ((gCodeGen->isTristate (formal) && !gCodeGen->isTristate (actual)) ||
            (netIsDeclaredForcible(formal) && !netIsDeclaredForcible(actual)))

          // passing a non-tristate or non-forcible to a port expecting a tristate
          // or forcible
        {
          gCodeGen->getMsgContext ()->CGCoercedPort (&(actual->getLoc ()),
                                                     actual->getName ()->str (),
                                                     formal->getName ()->str ());
          out << "* new " << TAG << CType (formal) << "(";
          actual->emitCode (context);
          out << ")";
        }
        else
        {
          // before bug 15978 we had no testcase that visited this section of the code
          if (formal->isInput())
            NU_ASSERT(!formal->isWritten() && !formal->isHierRefWritten(), formal);

          const char* const_flag = "const ";
          if (formal->isWritten() || formal->isHierRefWritten())
            const_flag = "";

          if ( eGCC4 == gCodeGen->getTarget() ) {
            // With bug 15978 it was discovered that gcc 4 generates incorrect code if 'const' is
            // used to typecast the argument of a constructor, when the object being constructed is
            // declared as a const ref.  It is not known if other versions of GCC have the same problem.
            // This appears to be a problem when the constructor in question is used in a class
            // initialization list like this:
            //
            // c_mid::c_mid(CarbonUInt2 const & a_mid_in):m_mid_in(a_mid_in){}  // ctor for class c_mid
            // c_bug15978::c_bug15978():mTop (fixup_hierref_pointers ()),
            // 	m_c_mid_1((const CarbonUInt2  & )m_counter){}  // the const on this line is the problem
            //
            // Not using the const in this situation is acceptable since gcc will still see the
            // const in the declaration of the constructor for of the object (c_mid in example)
            // and do the conversion anyway. The problem appears to be that gcc creates an
            // intermediate but does not create the instructions to copy from the original
            // to the intermediate as it should.  When the indicated 'const' is not emitted
            // then no intermediate is created and the ref works as expected.

            // the fix here is to not emit the 'const' if we are currently emitting code for a constructor initialization list
            // 
            if ( isConstructor (context) ){
              const_flag = "";
            }
          }
          
          out << "(" << const_flag << TAG << CType (formal) << "& )";

          actual->emitCode (context);
        }
      } // else
    } // if
    else
      actual->emitCode (context | eCGLvalue);
  } // else

  if (!isMarkNets (context) && !gCodeGen->isObfuscating())
  {
    NU_ASSERT(&out != NULL, formal);

    // in test/zpropagate, we do 'grep CTOR .carbon.libdesign/c_top.(cxx|h)'.
    // Then we can diff to make sure we are passing tristate/bidirect/force
    // references correctly to submodules
    out << "       /* CTOR: ";
    UtString buf;
    formal->compose(&buf, NULL);
    buf << " ";
    formal->composeDeclarePrefix(&buf);
    buf << ": ";
    formal->composeFlags(&buf);

    out << buf << "*/ " << TAG << "\\\n\t";
  }
} // sConstructMemberInit

// Declaration syntax for a reference member of the class
//
// 	T & net;
static void
sDeclareRefNet (const NUNet*var)
{
  if (var->getCGOp ()->hasAccessor ())
    // Already generated - probably for an allocated member or port alias.
    return;

  NU_ASSERT (not var->isAllocated (), var);

  var->getCGOp ()->setMember ();
  var->getCGOp ()->setAccessor ();
  var->getCGOp ()->setReferenced ();
  var->getCGOp ()->setDereferenced ();
  var->getCGOp ()->setOffset (gCodeGen->allocation (ptrSize (), ptrSize (), 1));

  // [const] TYPEDECL& m_foo;
  generateMemberVariable (var, true);

  // [const] TYPEDECL& get_foo() { return m_foo;}
  generateAccessorFunction (var);

} // sDeclareRefNet

static void
sEmitRefParams (const NUModule *mod)
{
  // Now look at the port connections to find the parameters that
  // aren't allocated in my class and aren't aliased to a member
  // variable of an instantiated sub-module.
  //
  // Create a reference variable and accessor function for these.  The
  // T& member will be initialized in the class constructor, right
  // after we initialize the inputs.
  //
  NUNetList refParms;

  NUNetVectorCLoop ports (mod->loopPorts ());
  remove_copy_if (ports.begin (), ports.end (),
		  std::back_inserter (refParms),
		  std::ptr_fun (&isNotReferenceParam));

  // Write a "T& net;" declaration
  gCodeGen->CGOUT () << "  // input, inout & output references" << ENDL;
  for_each ( refParms.begin (), refParms.end (),
	     &sDeclareRefNet);
}

// Process each module, generating the class.h and class.cxx files and
// iterating over the various lists to code the class prototypes and
// function bodies.
//

// Candidate ports that could be constructor parameters.  They aren't
// allocated or port aliased.
//
static void
sGetConstructorParams (NUNetCVector* refParams, const NUModule *mod)
{
  NUNetVectorCLoop ports (mod->loopPorts ());

  // Put all the ports EXCEPT the ones that aren't constructor args
  // (because they're locally allocated or port aliased or never referenced
  // by any code in the module)  Those things never need to be constructed,
  // so we won't!
  //
  remove_copy_if (ports.begin(), ports.end(),std::back_inserter(*refParams),
		  std::ptr_fun (&isNotConstructorArg));
}

// Visit the class's constructor and determine which nets are needed
static void
sVisitConstructor (const NUModule* mod)
{
  // Visit the port connections to see which ones want to be output aliased.
  // Mark them so that sGetConstructorParams() doesn't take too much.
  emitUtil::emitAliasedPortConnections (mod, eCGVoid);

  // Look at the objects that are instantiated inside this class and
  // mark their ports as referenced appropriately.
  //
  // Do this TWICE because we need to see ALL the input connections
  // and mark them so that when we see output or inouts, we can
  // properly mark the formals that have live actuals.
  for (int i = 0; i<2; ++i)
    {
      NUModuleInstanceMultiCLoop inst = mod->loopInstances (); {
        emitCodeList (&inst, eCGMarkNets);
      }
    }

}

/*!
 * The class constructor is emitted into both the .h and .cxx files; with
 * the prototype in the header and the implementation in the .cxx file.
 * This helps in compiling the code in schedule.h that constructs the
 * carbon_simulation object by reducing the memory requirements and the time
 * spent trying to optimize constructors that basically don't have any
 * opportunity for optimization.
 */
static void
sEmitConstructor (const NUModule* mod, CGContext_t context)
{
  UtOStream& hf = gCodeGen->CGOUT ();

  // parameter for each reference parameter
  NUNetCVector constructorParams;
  sGetConstructorParams (&constructorParams, mod);

  // Remember that each of these needs to be a dereferenced object
  std::for_each (constructorParams.begin (), constructorParams.end (),
		 MarkAuxInfo (OpFlags (eOpDereferenced | eOpConstructorParam)));


  //
  // If there are C models to initialize, or passed-down parameters needed to initialize
  // reference members, then we have to emit the class constructor separately
  // 
  NUCModelCLoop cmodels = mod->loopCModels ();

  NUNetCVector refParams;
  remove_copy_if (constructorParams.begin (), constructorParams.end (),
		  std::back_inserter (refParams),
		  std::ptr_fun (&isNotReferenceParam));

  // If we haven't put anything else into the .cxx file, we might as well leave the
  // constructor and destructor in the header file too.  But if we said -noInline
  // then emit constructors out-of-line

  bool inlineConstructor = gCodeGen->isEmptyCxx (mod) && isInlineCandidate (context)
    // Similarly, if we have cmodels or need to pass reference parameters down, force
    // out-of-line
    && cmodels.atEnd () && refParams.empty ()
    // The top module ALWAYS has a separately compiled constructor
    && not mod->atTopLevel ();

  if (not inlineConstructor)
    // We're gonna need the .cxx file!
    gCodeGen->putNonEmptyCxx (mod);

  // Now, write a constructor to initialize all the reference parameters.
  // Looks like:
  //		c_<module> (UInt8 &x, UInt32 &y);
  //
  if (isDeclare (context)) {
    hf << "\n  // default constructor" << ENDL;
    hf << "  " << mclass (mod->getName ());

    // Emit the list of constructor parameters and MARK them as being
    // referenced and constructed, so that callers know they have to
    // pass them!
    //
    emitCodeList (&constructorParams,
                  (eCGNoPrefix
                   | eCGMarkNets
                   | eCGConstructor | eCGDeclare),
                  "(", ", ", ")");

    // Put the constructors into their own program section so that they're
    // out of the way once we get started execution.
    if (not inlineConstructor) {
      hf << ENDL;
      gCodeGen->emitSection(hf, CRYPT("carbon_constructors"));

      // Finished the declaration.
      hf << ";" << ENDL;
      return;
    }
  } else if (isDefine (context) and isSecondPass (context)) {
    if (inlineConstructor)
      return;                   // Don't need one in the .cxx, it was inlined

    // Now generate
    //	c_module::c_module (UInt8 &x, UInt32 &y): m_x(x), m_y(y) {}
    //
    hf << ENDL;
    hf << mclass (mod->getName ()) << "::" << mclass (mod->getName ());

    emitCodeList (&constructorParams,
                  (eCGNoPrefix | eCGConstructor | eCGDeclare),
                  "(", ", ", ")");
  }

  UtString punct (":");         // changes after first initializer

  // now member(value) ,....  Also have to include the instantiations
  // of class members that have input connections

  if (mod->atTopLevel()) {
    // At the top-level, call the constructor for our parent class,
    // which includes the call to fixup hierref pointers.
    hf << punct << "mTop (fixup_hierref_pointers ())";
    punct = ",\n\t";
  }

  // Reorder the parameters in allocation order so that we initialize in
  // the same order we allocated.
  emitUtil::sort_by_address addressSorter;

  std::stable_sort (refParams.begin (), refParams.end (), addressSorter);

  for(NUNetCVector::const_iterator i=refParams.begin();
      i != refParams.end(); ++i)
    {
      const NUNet * memberVar = *i;

      // should this be emitUtil::isNetDeclared?
      if (!isNetReferenced(memberVar)) 
        {
          hf << " /* unused " << member (memberVar->getName ()) << " */ ";
          continue;
        }

      hf << punct;			// Maybe comma
      punct = ",\n\t";
      hf << TAG;
      memberVar->emitCode (eCGNameOnly);	// Class member
      hf << "(";
      memberVar->emitCode (eCGConstructor | eCGNoPrefix); // constructor formal
      hf << ")";
    }

  // Emit the c-model initializations
  if (!cmodels.atEnd())
  {
    hf << ENDL;
    emitCodeList(&cmodels, eCGConstructor, punct.c_str (), ",", "");
    punct = ",";
  }	

  // Look at all the instantiations and pass in their input connections
  // generates inst1(init) , inst2(init2)...
  // Must resort the instances so that we initialize in same order
  // we allocate
  NUModuleInstanceMultiCLoop inst (mod->loopInstances ());
  UtVector<const NUModuleInstance*> sortedInst (inst.begin (), inst.end ());
  emitUtil::sort_by_align alignedSorter;

  std::stable_sort (sortedInst.begin (), sortedInst.end (), alignedSorter);

  if (!sortedInst.empty ()) {
    hf << TAG;
    emitCodeList (&sortedInst, eCGConstructor, punct.c_str (), ",", "");
  }

  // And lastly the constructor body
  hf << "{}" << ENDL;
}

/*!
 * Similar to the constructor, the class destructor is emitted into both
 * the .h and .cxx files; with the prototype in the header and the
 * implementation in the .cxx file.
 *
 * Note that the only reason we are declaring the destructor is to explicitly
 * place it in a section.
 */
static void
sEmitDestructor (const NUModule* mod, CGContext_t context)
{
  if (gCodeGen->isEmptyCxx (mod))
    // Didn't write a constructor, so don't need the destructor
    return;

  UtOStream& hf = gCodeGen->CGOUT ();

  if (isDeclare (context)) {
    // Destructor declaration.
    hf << "\n  // default destructor" << ENDL
       << "  ~" << mclass (mod->getName ())
       << "()";

    // Put the destructors in their own sections, so that they're
    // out of the way once we get started execution.
    gCodeGen->emitSection(hf, CRYPT("carbon_destructors"));

    // Finish the declaration.
    hf << ";" << ENDL;
  }

  if (isDefine (context)) {
    hf << mclass (mod->getName ()) << "::~" << mclass (mod->getName ()) << "() {}" << ENDL;
  }
}


static bool sAllocatedNetOrReferencedHierRefNet(const NUNet * net)
{
  if (net->isHierRef()) {
    CGAuxInfo *aux = net->getCGOp();
    return ((aux!=NULL) and (not aux->isUnreferenced()));
  } else {
    return net->isAllocated();
  }
}

// Generate the class member variable declarations
static void
sEmitMemberVariables (const NUModule *mod, CGContext_t context)
{

  // Emit the member variables from this Module by iterating over the
  // Hierarchical refs, InputPorts, OutputPorts, Bidirects and Locals.
  //
  NUNetCVector memberVariables;

  // Remove locals that don't have an allocation here
  //
  NUModule::NetCLoop memberLoop (mod->loopNets ());

  remove_copy_if (memberLoop.begin(), memberLoop.end(),
		  std::back_inserter(memberVariables),
		  std::not1 (std::ptr_fun (&sAllocatedNetOrReferencedHierRefNet)));

  // Order the member variables for performance/model size (ordering
  // method determined by -locality cmd-line option).
  gCodeGen->getRankNets()->orderByRank(mod, &memberVariables);

  // Allocate storage and compute the class alignment

  std::for_each (memberVariables.begin (), memberVariables.end (),
		 allocateNet);

  // Materialize BOTH an accessor function and the storage for everything
  // that's allocated in this module
  gCodeGen->CGOUT () << "  // allocated members" << ENDL;

  emitCodeList (&memberVariables, context);
}

static void
sEmitChangeDetectInit(const NUChangeDetectVector& changeDetects,
                      CodeGen* codeGen)
{
  // If there are no change detects, don't emit the function
  if (changeDetects.empty()) {
    return;
  }

  gCodeGen->putNonEmptyCxx (gCodeGen->getCurrentModule ());

  // Emit the function prologue
  UtOStream &hout = codeGen->switchStream(eCGDeclare);
  hout << "  void initChangeDetects() {" << ENDL;

  // Emit the various inits. We emit temp = (expr) ^ 1; so that temp
  // holds a value other than the current value of expr
  for (NUChangeDetectVectorCLoop l(changeDetects); !l.atEnd(); ++l) {
    // Gather the parts of the change detect
    NUChangeDetect* changeDetect = *l;
    NUExpr* expr = changeDetect->getArg(0);
    NUNet* net = changeDetect->getTempNet();

    // Emit the assignment
    hout << "    " << TAG;
    net->emitCode(eCGDefine);
    hout << " = (";
    expr->emitCode(eCGDefine);
    hout << ") ^ (UInt32)1;" << ENDL;
  }

  // Emit function epilogue
  hout << "  }" << ENDL;
} // sEmitChangeDetectInit


//! LutEmitCallback
/*!
 * Class to walk a single module, finding all the Luts.
 * It gathers them and emits table declarations and the definition
 * macro in the header.
 */
class LutEmitCallback : public NUDesignCallback
{
public:
  LutEmitCallback(NUModule *mod, CGContext_t context) : mModule(mod), mContext (context) {}
  ~LutEmitCallback() {}
  Status operator()(Phase, NUBase*) { return eNormal; }

  // Do not traverse into submodules
  Status operator()(Phase, NUModuleInstance*) { return eSkip; }

  Status operator()(Phase phase, NUOp *op)
  {
    if ((phase == ePre) && (op->getOp() == NUOp::eNaLut)) {
      mLuts.insert(dynamic_cast<NULut*>(op));
    }
    return eNormal;
  }

  /*
    We have to use an extra level of indirection of LUTs of
    BitVectors.  Otherwise, we would emit this for a 128 bit lut:

        static BitVector<128>  m_$lut_2[4];
        BitVector<128>  c_test::m_$lut_2[4] = {
          *reinterpret_cast<const BitVector<128> *>(cv$3),
          *reinterpret_cast<const BitVector<128> *>(cv$4),
          *reinterpret_cast<const BitVector<128> *>(cv$5),
          *reinterpret_cast<const BitVector<128> *>(cv$6)
        };

    That would cause g++ to emit a static copy-constructor constructor,
    to copy the 4-element array of BitVectors from the RHS to the LHS
    in the second statement.

    There are all sorts of reasons why we ought not to use static
    constructors, but the most compelling is that we allow C linking in
    our runtime, and the static constructors are collected & called by the
    C++ linker.  So any static constructors we define are not guaranteed
    to be called.

    Instead, we want to emit this:

        static const BitVector<128>*  m_$lut_2[4];
        const BitVector<128>*  c_test::m_$lut_2[4] = {
          &(*reinterpret_cast<const BitVector<128> *>(cv$3)),
          &(*reinterpret_cast<const BitVector<128> *>(cv$4)),
          &(*reinterpret_cast<const BitVector<128> *>(cv$5)),
          &(*reinterpret_cast<const BitVector<128> *>(cv$6))
        };

    and the C++ compiler does not need to emit a static construcor
    to copy over an array of pointers.


    I found this issue because test/bvtest compiles with -DBVPERF, 
    and that crashes because during the pre-main() call to the
    copy-constructor we were trying to accumulate BVPERF statistics
    and the BVPERF context had not yet been initialized.  So that
    testcase SEGVd before main().

    But really the problem is worse than that.  Even without turning on
    BVPERF, the right bits would not go to the right place unless you
    linked with the right linker.
  */

  // Call this routine after the walk.  It will emit the declarations and
  // the macro to define the Luts.
  void emit(UtOStream &out)
  {
    // Walk once to emit the declaration.
    if (isDeclare (mContext)) {
      for (UtSet<NULut*>::iterator iter = mLuts.begin();
           iter != mLuts.end();
           ++iter) {
        NULut *lut = *iter;

        out << TAG << "static ";
        emitLutDecl(out, lut);
        out << ";" << ENDL;
      }
    } else if (isDefine (mContext)) {
      // emit the luts.
      for (UtSet<NULut*>::iterator iter = mLuts.begin();
         iter != mLuts.end();
         ++iter) {
        NULut *lut = *iter;
        out << "  " << TAG;
        emitLutDecl(out, lut, mModule);
        out << " = ";
        NUExprLoop rows = lut->loopExprs();
        ++rows; // skip the select, just want to emit the rows here
        bool isBitVec = lut->getBitSize() > LLONG_BIT;
        if (isBitVec) {
          out << TAG;
          emitCodeList(&rows, eCGVoid, "{&", ", &", "};");
        } else {
          out << TAG;
          emitCodeList(&rows, eCGVoid, "{", ", ", "};");
        }
        out << ENDL;
      }
      out << "\n";

      if (not mLuts.empty ()) {
        gCodeGen->putNonEmptyCxx (mModule);
      }
    }
  }

  //! Emit lut declaration: "\<type\> \<name\>[\<size\>]"
  /*!
   * If module is given, \<name\> will be qualified with the module's name:
   * "\<module\>::\<name\>"
   */
  void emitLutDecl(UtOStream &out, NULut *lut, NUModule *module=0)
  {
    out << TAG;
    bool isBitVec = lut->getBitSize() > LLONG_BIT;
    if (isBitVec) {
      out << "const ";
    }
    out << CBaseTypeFmt(lut->getBitSize(), eCGVoid, lut->isSignedResult()) << " ";
    if (isBitVec) {
      out << "* ";
    }
    if (module) {
      out << mclass(module->getName()) << "::";
    }
    out << member(lut->getTableName());
    out << "[" << lut->numTableRows() << "]";
  }

private:
  NUModule* mModule;
  UtSet<NULut*> mLuts;
  CGContext_t mContext;
};


// Emit Lut declarations and macro to define them for use in the .cxx file
static void sEmitLuts (NUModule* mod, UtOStream &out, CGContext_t context)
{
  out << ENDL;
  LutEmitCallback callback(mod, context);
  NUDesignWalker walker(callback, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.module(mod);
  callback.emit(out);
  out << ENDL;
}


// Common sequence of function header or body.
static void sGenerateClassFunctions (const NUModule* mod, CGContext_t context)
{
  // Scope needed in case inlined functions are strip-mined
  gCodeGen->pushScope (mod);

  NUNamedDeclarationScopeCLoop declarations (mod->loopDeclarationScopes ());
  emitCodeList (&declarations, context);

  NUModule::InitialBlockCLoop initial (mod->loopInitialBlocks ());
  emitCodeList (&initial, context);

  NUModule::AlwaysBlockCLoop always (mod->loopAlwaysBlocks ());
  emitCodeList (&always, context);

  NUModule::ContAssignCLoop assigns (mod->loopContAssigns ());
  emitCodeList (&assigns, context);

  NUModule::ContEnabledDriverCLoop tristates (mod->loopContEnabledDrivers ());
  emitCodeList (&tristates, context);

  NUModule::TriRegInitCLoop triInits (mod->loopTriRegInit ());
  emitCodeList (&triInits, context);

  NUTaskCLoop tasks (mod->loopTasks ());
  emitCodeList (&tasks, context);

  NUCModelCLoop cmodels (mod->loopCModels ());
  emitCodeList (&cmodels, context);

  // We have to create a function to initialize all the change detect
  // old value temporaries so that at time 0, the change detect always
  // returns true. We add one function per module to reduce the call
  // overhead.
  if (isDeclare(context)) {
    NUChangeDetectVector changeDetects;
    mod->getChangeDetects(&changeDetects,true);
    sEmitChangeDetectInit(changeDetects, gCodeGen);
    if (not changeDetects.empty ())
      gCodeGen->putNonEmptyCxx (mod);
  }

  gCodeGen->popScope ();
}

// Generate the .h file for a class
static void
sGenerateClassDeclaration (const NUModule* mod, CGContext_t context)
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  bool topBehavior = mod->atTopLevel();

  ////////////////////////////////////////////////////////////////////////////
  //
  // Emit the class header declarations.
  //
  ////////////////////////////////////////////////////////////////////////////
  gCodeGen->getCGOFiles()->restoreExtraHeaders (mod);
  gCodeGen->getCGOFiles()->addClassFile (mod, true, gCodeGen->precompiledHeaders()); // Create the .h file
  UtOStream &hout = gCodeGen->switchStream(eCGDeclare);
  hout << ENDL;

  // Emit the list of include files we need by walking the
  // instantiated module list, outputting a guarded #include
  // "classname.h" for each one
  NUModuleList module_list;
  mod->getInstantiatedModules (&module_list);
  for(NUModuleList::const_iterator m = module_list.begin (); m != module_list.end (); ++m) {
    gCodeGen->addExtraHeader (*m);
  }

  // New model C interface
  gCodeGen->addExtraHeader("shell/carbon_interface_aux.h");

  gCodeGen->addExtraHeader ("codegen/carbon_priv.h");
  gCodeGen->addExtraHeader ("shell/carbon_model.h");

  gCodeGen->restoreConstantPool (mod);
  
  // Emit the c-model header file includes
  //
  NUCModelCLoop cmodels (mod->loopCModels());
  hout << ENDL;
  emitCodeList (&cmodels, eCGNameOnly, "#include \"", "\"\n");

  hout << ENDL;

  // Track everything we encountered in coding the body of this method.
  // 

  // We might have ports that are unreferenced, in which case we won't have
  // called checkNetIncludes on them.  So, just run thru everything declared in the
  // module and make sure we have all the headers we might need.
  //
  hout << ENDL;
  NUModule::NetCLoop allNets (mod->loopNets ());
  emitCodeList (&allNets, eCGVoid);

  gCodeGen->getCGOFiles()->printExtraHeaders ();
  hout << ENDL;

  // Before we emit any class members, take one last look at the
  // constructor to see if we need to mark anything else
  sVisitConstructor (mod);

  // Emit the name space for our design
  //
  hout << "namespace " << gCodeGen->getNamespace() << " {" << ENDL;

  // Generate variable to store Model's current profiling bucket.
  hout << "\nextern volatile SInt32 " << gCodeGen->getBucketVarName() << ";" << ENDL;
  hout << "\nextern volatile SInt32 " << gCodeGen->getScheduleBucketVarName() << ";" << ENDL;

  // Gather the modules for which we store pointers for. These are the
  // ones in which we call the class'es tasks through hier refs.
  //
  NUTaskEnableVector allHierTaskEnables;
  mod->getTaskEnableHierRefs(&allHierTaskEnables);
  NUTaskEnableVector nonRelHierTaskEnables;
  for (NUTaskEnableCLoop l(allHierTaskEnables); !l.atEnd(); ++l)
  {
    // Check if this is not locally relative. Those are the only
    // hier refs that need forward declarations.
    NUTaskEnable* enable = *l;
    if (!enable->getHierRef()->isLocallyRelative())
      nonRelHierTaskEnables.push_back(enable);
  }
  allHierTaskEnables.clear();
      
  // Emit forward class declarations for modules we store pointers for
  //
  NUModuleSet modules;
  for (NUTaskEnableCLoop l(nonRelHierTaskEnables); !l.atEnd(); ++l)
  {
    NUTaskEnable* enable = *l;
    NUTaskHierRef* taskRef = enable->getHierRef();
    for (NUTaskHierRef::TaskVectorLoop l = taskRef->loopResolutions();
         !l.atEnd(); ++l)
    {
      NUTask* task = *l;
      NUModule* module = task->findParentModule();
      modules.insert(module);
    }
  }
  for (NUModuleSetIter i = modules.begin(); i != modules.end(); ++i)
  {
    NUModule* module = *i;
    hout << "class " << mclass(module->getName()) << ";" << ENDL;
  }


  if (topBehavior)
  {
    // Generate a undef topname to protect against name-space pollution from various
    // system classes.
    //
    hout << "#undef " << gCodeGen->getTopName ()->str () << ENDL;
    hout << "#undef " << mclass (mod->getName ()) << ENDL;
  }

  // Emit the class declaration.  Everything's public, but in the 'Carbon'
  // namespace.  If we're a toplevel module, then we have to inherit
  // from the carbon-id class.
  //
  
  hout << "class " << mclass(mod->getName ()) << " {" << ENDL
       << "public:\n";

  if (topBehavior) {
    hout << "  carbon_top mTop;" << ENDL << ENDL;
  }

  // Before we emit the member variables, we need to look at the
  // constructor instantiation and see which variables are mentioned here
  // but not elsewhere - they need to be marked as referenced in "code".

  // Output aliased port connections - these may be passed to
  // the constructor, so we need to know if they exist.
  hout << ENDL;
  emitUtil::emitAliasedPortConnections (mod, context);

  // Generate the class member variables
  hout << ENDL;
  sEmitMemberVariables (mod, context);

  // Generate port references that will be initialized by the constructor
  // MOVE BEFORE MEMBER VARIABLES FOR BETTER ALIGNMENTS?
  hout << ENDL;
  sEmitRefParams (mod);

  // Emit the pointer(s) in the class so that we can make the
  // non-relative calls.
  UtSet<UtString> covered;
  for (NUTaskEnableCLoop l(nonRelHierTaskEnables); !l.atEnd(); ++l)
  {
    NUTaskEnable* enable = *l;
    NUTaskHierRef* taskHierRef = enable->getHierRef();
    UtString parentVar;
    CGTFgenHierRefTaskName( eParentVar, taskHierRef, NULL, &parentVar );

    // Prune out duplicates of the paths for single resolution hier
    // refs because if two tasks are called in the same module
    // instance, we don't need two module pointers.
    if( covered.find( parentVar ) == covered.end() ) {
      covered.insert( parentVar );
      hout << ENDL;
      taskHierRef->emitCode(context);
    }
  }
  covered.clear();
  nonRelHierTaskEnables.clear();

  // If this module has $display's with %m then emit a pointer to the
  // instance name. This will be set by code generated in
  // CodeSchedule.cxx.
  if (mod->hasDisplaysWithHierarchicalName()) {
    hout << "  // Ponter to the instance name for this module" << ENDL
         << "  const char* m_cds_inst_name;\n"
         << "  const char* get_inst_name() { return m_cds_inst_name; }\n\n";
    gCodeGen->allocation(ptrSize(), ptrSize(), 1);
  }

  // Optimize the order of instantiations to reduce alignment problems.
  //
  NUModuleInstanceMultiCLoop instances (mod->loopInstances ());
  UtVector<const NUModuleInstance*> sortedInstances (instances.begin (),
						     instances.end ());

  emitUtil::sort_by_align alignedSorter;
  std::stable_sort (sortedInstances.begin (), sortedInstances.end (),
		    alignedSorter);

  // Emit the signatures for ALWAYS blocks and continuous assigns.
  // This include both the function prototypes AND any static block-local
  // symbols.
  hout << "  // schedule functions" << ENDL;

  // Run thru all the function objects in the translated module
  // generating prototypes.
  sGenerateClassFunctions (mod, context);

  hout << ENDL;
  // Now remember the alignment required.  No more storage can be
  // allocated for this class without potential trouble ensuing...
  //
  mod->setClassDataAlignment (gCodeGen->alignment ());


  // Remember how big this module's class is.  This size is exclusive
  // of the sizes of any instantiated class members.
  //
  mod->setClassDataSize (gCodeGen->allocation (0));
  mod->setClassInstDataSize (gCodeGen->allocation (0));

  // Now emit the directly instantiated modules as class members.
  // They come AFTER the allocated inputs, locals and outputs in case you need
  // to use a reference parameter to instantiate one of these sub modules.
  //
  // The instantiated contained classes have to come before the aliased outputs
  // that are bound to accessor functions so that the names are visible.

  hout << ENDL;
  emitCodeList (&sortedInstances, eCGDeclare);

  // If the size of this class is zero after instantiating subclasses,
  // then we have to change it to a size of 1, because every instance of
  // an object is required to have a unique address.
  // Note: we change the ClassInstDataSize (instead of the ClassDataSize),
  // because this extra padding gets counted at the end of the module, after
  // any instantiations it has.
  //
  if (gCodeGen->allocation (0) == 0)
  {
    gCodeGen->allocation(1);
    mod->setClassInstDataSize (gCodeGen->allocation(0));
  }

  /*
   * Adjuct the alignment required to include the largest imposed by
   * our instantiated modules.  One of the instantiated sub-modules
   * may have larger alignment requirements than any imposed by the
   * class-member variables, so inherit that alignment. (example: a
   * subclass contains a UInt64 which forces this class to be aligned
   * on a 64-bit boundary as well.)
   */
  mod->setClassDataAlignment (gCodeGen->alignment (mod->getClassDataAlignment ()));

  // Pad class size out to its alignment.
  mod->setClassInstDataSize(gCodeGen->allocation(0, mod->getClassDataAlignment()));

  UtString sizeStats ("class ");
  sizeStats << mclass (mod->getName ());
  sizeStats << ": align=" << mod->getClassDataAlignment ()
	    << ", size=" << gCodeGen->allocation (0) << "\n";

  if (gCodeGen->getCarbonContext ()->getArgs ()->getBoolValue (CRYPT("-dumpCGStats")))
    UtIO::cout () << TAG << "CGStats: " << sizeStats;

  hout << "// " << sizeStats << ENDL;


  // The top level class has to have new and delete methods defined to
  // correctly zero memory.
  // The infrequently called functions are placed into their own section,
  // so that they will be mixed in with the functions which make up the
  // main part of the schedule.
  if (topBehavior)
    {
      gCodeGen->emitCycleDeclarations ();
      hout << ENDL
	   << "  void debug_schedule(void)";
      gCodeGen->emitSection(hout, CRYPT("carbon_infrequent_code"));
      hout << ";" << ENDL
           << "  void cmodel_create(void)";
      gCodeGen->emitSection (hout, CRYPT ("carbon_infrequent_code"));
      hout << ";" << ENDL
           << "  void cmodel_init(void* cmodelData)";
      gCodeGen->emitSection (hout, CRYPT ("carbon_infrequent_code"));
      hout << ";" << ENDL
           << "  void cmodel_register(CarbonObjectID* descr)";
      gCodeGen->emitSection (hout, CRYPT ("carbon_infrequent_code"));
      hout << ";" << ENDL
           << "  void cmodel_modeChange(CarbonObjectID* descr, void*, CarbonVHMMode from, CarbonVHMMode to)";
      gCodeGen->emitSection (hout, CRYPT ("carbon_infrequent_code"));
      hout << ";" << ENDL;
      hout << "  static void* operator new(size_t len)\n"
	   << "  {\n"
           << "    void* ptr = carbonmem_malloc(len);\n"
           << "    memset(ptr, 0, len);\n"
	   << "    return ptr;\n"
	   << "  }\n"
           << "  static void operator delete (void *p)\n"
           << "  {\n"
           << "    carbonmem_free(p);\n"
           << "  }\n";

      hout << "  UInt32 fixup_hierref_pointers ()";
      gCodeGen->emitSection(hout, CRYPT("carbon_infrequent_code"));
      hout << ";" << ENDL;

      if (gCodeGen->mDoCheckpoint) {
        hout << "#ifdef CARBON_SAVE_RESTORE" << ENDL;
        hout << "  // Supply concrete implementation for abstract save/restore" << ENDL;
        hout << "  bool simSave(UtOCheckpointStream& out, CarbonObjectID *descr){ save(out, descr); return true; }\n";
        hout << "  bool simRestore(UtICheckpointStream& in, CarbonObjectID *descr) { restore(in, descr); return true; }\n";
        // Add onDemand versions, if enabled
        if (gCodeGen->isOnDemand()) {
          hout << "  bool simSaveOnDemand(UtOCheckpointStream& out, CarbonObjectID *descr){ saveOnDemand(out, descr); return true; }\n";
          hout << "  bool simRestoreOnDemand(UtICheckpointStream& in, CarbonObjectID *descr) { restoreOnDemand(in, descr); return true; }\n";
        }
        hout << "#endif // CARBON_SAVE_RESTORE\n";
      }

    }

  if (gCodeGen->mDoCheckpoint) {
    // Codex the class declarations for save/restore
    hout << ENDL;
    GenerateSaveRestore (mod, context, eSaveRestoreNormal);
    if (gCodeGen->isOnDemand())
      GenerateSaveRestore (mod, context, eSaveRestoreOnDemand);
  }

  StringAtom* name (mod->getName ());

  // Generate table declarations for all the Lut's in the module.
  // The tables are static to the class so doesn't really matter where we put them.
  hout << ENDL;
  sEmitLuts (const_cast<NUModule*>(mod), hout, context);

  // Output the constructor and destructor
  hout << ENDL;
  sEmitConstructor (mod, context);
  sEmitDestructor (mod, context);

  // Hide the copy constructor and assignment operator
  hout << "private:" << ENDL
       << "  " << mclass (name) << "(const " << mclass (name) << "&);\n"
       << "  " << mclass (name) << "& operator=(const " << mclass (name) << "&);" << ENDL;


  // Declare the class's constant pool
  hout << ENDL;
  gCodeGen->dumpConstantPool (hout, eCGDeclare);
  gCodeGen->saveConstantPool (mod);

  // Close the class declaration AND the namespace.  Make a
  // standardized alias for the class that we can use in schedule.cxx
  // or anywhere else we need to get at this class...
  //
  hout << "\n};"
    "\n}\n"
    "using namespace " << gCodeGen->getNamespace() << ";" << ENDL;

  if (topBehavior)
    {
      hout << "typedef " << mclass (name) << " carbon_simulation;" << ENDL;
    }

  hout << "#endif\n";

  // Save the headers for the second pass of the code generator
  gCodeGen->getCGOFiles()->saveExtraHeaders (mod);

  // Finished with this class's header

  gCodeGen->getCGOFiles()->closeClassFile (true);

}

/*!
 * Create the .cxx file to tranlate module \param mod.  Class bodies
 * are coded in top-down order so that we can mark port connections
 * that are output-port aliased such that we only generate the accessor
 * functions for those nets that are actually used.
 */
static void
sGenerateClassDefinition (const NUModule* mod, CGContext_t context)
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  gCodeGen->getCGOFiles()->restoreExtraHeaders (mod);

  if (mod->atTopLevel()) {
    // If this is the top module, mark all our ports as referenced
    NUNetVectorCLoop ports (mod->loopPorts ());
    emitCodeList (&ports, eCGMarkNets);
    gCodeGen->putNonEmptyCxx (mod); // We must generate the .cxx for the top class
  } else {
    // Assume that we DON'T need the .cxx until someone tells us otherwise.
    gCodeGen->putEmptyCxx (mod);
  }



  context |= eCGMarkNets;	// Every net we code gets marked as needed

  gCodeGen->getCGOFiles()->addClassFile (mod, false, gCodeGen->precompiledHeaders ());

  UtOStream &out = gCodeGen->switchStream (eCGDefine);

  // Emit functor functions for the resolutions (these need to be outside
  // the namespace (because they're wrapped separately in the namespace
  // already.
  //
  NUTaskEnableVector hierTaskEnables;
  out << ENDL;
  gCodeGen->generateTaskHierRefs(mod, hierTaskEnables);
  out << ENDL;
  gCodeGen->generateTaskHeaders (hierTaskEnables);
  if (not hierTaskEnables.empty ())
    gCodeGen->putNonEmptyCxx (mod);

  out << "namespace " << gCodeGen->getNamespace() << " {" << ENDL;

  // Generate variable to store Model's current profiling bucket.

  if (mod->atTopLevel()) {
    // Generate global variable to store Model's current profiling
    // bucket.  
    out << "\nvolatile SInt32 " << gCodeGen->getBucketVarName() << " = -1;" << ENDL;
    out << "\nvolatile SInt32 " << gCodeGen->getScheduleBucketVarName() << " = -1;" << ENDL;
  }

  // Generate the constant pool definitions
  out << ENDL;
  gCodeGen->restoreConstantPool (mod);
  out << ENDL;
  gCodeGen->dumpConstantPool (out, eCGDefine);

  // Generate the Luts arrays corresponding to the declarations in
  // the .h file
  out << ENDL;
  sEmitLuts (const_cast<NUModule*>(mod), out, context);

  // Now construct the functors inside the namespace
  //
  for (NUTaskEnableCLoop loop(hierTaskEnables); !loop.atEnd(); ++loop)
  {
    // Check if this is not locally relative. Those are the only
    // hier refs that need functors
    NUTaskEnable* enable = *loop;
    if (!enable->getHierRef()->isLocallyRelative())
    {
      NUTaskHierRef* taskRef = enable->getHierRef();
      if (taskRef->resolutionCount() > 1) {
        out << ENDL;
        taskRef->emitCode(eCGFunctor);
      }
      gCodeGen->putNonEmptyCxx (mod);
    }
  }
  hierTaskEnables.clear();


  // Mark any hierref nets as used
  NUNetCLoop hierLoop (mod->loopNetHierRefs ());
  if (not hierLoop.atEnd ())
    gCodeGen->putNonEmptyCxx (mod); // we emit code into the .cxx file

  out << ENDL;
  emitCodeList (&hierLoop, eCGMarkNets);
    
  out << ENDL;
  sGenerateClassFunctions (mod, context);

  // Visit the constructor to mark the nets that are used to construct
  // other sub-instances, but not otherwise referenced within always
  // blocks of this module
  sVisitConstructor (mod);


  if (gCodeGen->mDoCheckpoint) {
    // Emit the save-restore methods
    out << ENDL;
    GenerateSaveRestore (mod, eCGDefine, eSaveRestoreNormal);
    if (gCodeGen->isOnDemand())
      GenerateSaveRestore (mod, eCGDefine, eSaveRestoreOnDemand);
    gCodeGen->putNonEmptyCxx (mod);
  }
  // Now save the constant pool (if any)
  gCodeGen->saveConstantPool (mod);

  // Output the constructor and destructor if we need them in the .cxx file
  if (isSecondPass (context)) {
    out << ENDL;
    sEmitConstructor (mod, context);
    out << ENDL;
    sEmitDestructor (mod, context);
  }


  // Save the class headers
  if (isSecondPass (context))
    gCodeGen->getCGOFiles ()->flushExtraHeaders (mod);
  else
    gCodeGen->getCGOFiles()->saveExtraHeaders (mod);

  // Close the class declaration
  out << "}" << ENDL;

  // And declare the single instance of the design-specific bit bucket
  // for memories inside the namespace, but not inside a class!
  if (mod->atTopLevel ()) {
    out << "  extern \"C\" UInt32 gCarbonMemoryBitbucket[] COMMON;" << ENDL
        << "  UInt32 gCarbonMemoryBitbucket["
        << (gCodeGen->getMaxMemoryWidth () / LONG_BIT) + 1
        << "] COMMON;" << ENDL << "\n";
  }

  gCodeGen->getCGOFiles()->closeClassFile (false);

  // If this module is still in the empty modules list, we never emitted 
  // any code into the .cxx file that we need to compile - delete the .cxx
  // to speed up compilation.
  //
  if (gCodeGen->isEmptyCxx (mod))
    gCodeGen->getCGOFiles ()->deleteModuleFilename (mod);
}

//! Helper function to mark all nets in collection as referenced
static void
sVisitAll (IODBNucleus* iodb, IODB::NameSetLoop p)
{
  for (; !p.atEnd (); ++p)
  {
    // Get the elaborated net associated with this ST entry
    NUNetElab *netElab = iodb->getNet (*p);
    netElab->emitCode (eCGMarkNets);
  }
}

/* Top level block of Verilog design. */
CGContext_t
NUDesign::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  NUModuleList module_list;
  getTopLevelModules(&module_list);
  NU_ASSERT (module_list.size () == 1, this);

  NUModule *mod = *(module_list.begin ());

  // The following loops may generate headers if the net we visit is the
  // only one that needs a Tristate, Memory, BitVector, etc header...
  gCodeGen->getCGOFiles()->restoreExtraHeaders(mod);

  // Mark all the observable, depositable or forcible nets as "coded"
  // so that they are not eliminated from the class declarations.
  //
  IODBNucleus* iodb = gCodeGen->getCarbonContext ()->getIODB ();

  sVisitAll (iodb, iodb->loopObserved ());
  sVisitAll (iodb, iodb->loopDeposit ());
  sVisitAll (iodb, iodb->loopForced ());

  // Save any headers which have been calculated so far to the top module.
  gCodeGen->getCGOFiles()->saveExtraHeaders(mod);

  // Order the modules so that for each module that instantiates a
  // specific module K, that we visit all the instantiators before
  // we visit module K.
  NUModuleList visit;
  getModulesBottomUp(&visit);

  // Code the CXX files first.  As expressions are coded, we will mark
  // nets as used.  When we know everything that's used, we can code
  // the header files and only emit declarations for those nets we need.
  //
  // We are going to throw away the generated code from this pass (since we
  // don't have the constructors or constant pool yet.)
  //
  std::for_each (visit.rbegin (), visit.rend (),
		 std::bind2nd (std::mem_fun (&NUBase::emitCode), eCGDefine));

  // Revisit each modules port connections propagating the usage information
  // that we've now calculated for sub-modules thru the input and output ports
  // in order to establish the set of ports that need to be declared and
  // the port alias accessors that need to be materialized.
  std::for_each (visit.begin (), visit.end (),
		 std::ptr_fun (sVisitModuleForConnections));

  // Now generate the headers in reverse order, such that we compute
  // the class sizes BEFORE we even instantiate an instance of the class
  // Forward thru the visit collection gives us the leaf-first order
  emitCodeList (&visit, eCGDeclare);

  // Code the CXX files a second time, now that we know the constant pool and
  // constructors.  We could filter out the modules without a constant pool or
  // non-trivial constructors, but why bother.
  emitCodeList (&visit, eCGDefine | eCGSecondPass);

  // Emit the header files for all c-models
  NUCModelInterfaceCLoop cmodelInterfaces = loopCModelInterfaces();
  emitCodeList (&cmodelInterfaces, context);
  return eCGVoid;
}



/*!
 * This is also called in eCGVoid context to indicate that we only
 * need to emit an "#include class" line.
 *
 * The top-level class has special properties
 * including:
 * \li	Including C++ standard classes
 * \li	top-level primary inputs are allocated storage
 * \li	Override new/delete for our constructors
 */
CGContext_t
NUModule::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);


  if (isNameOnly (context)) {
    gCodeGen->CGOUT () << mclass (mName);
    return eCGVoid;
  }

  gCodeGen->putCurrentModule (this);

  // Compute the set of NUCModelCall's for this given NUModule so that
  // we can look them up from an NUCModelInterface.
  gCodeGen->findCModelCalls(this);

  // Check to see if this module is instantiated more than once.  In
  // that case, inlining may not be a good idea.
  //
  const NUCost *cost = gCodeGen->mCosts->findCost (this, false);
  if (gCodeGen->mDoInline
      && cost && cost->mUnelabInstances == 1)
    // Aggressively inline...
    context |= eCGInline;

  // Reset the class offset calculations.  Have to do this before definition or
  // declare because of named-scopes encountered inside tasks.
  //
  gCodeGen->startClassData ();

  if (isDefine (context)) {
    // The .cxx file
    sGenerateClassDefinition (this, context | eCGMarkNets);
  } else if (isDeclare (context)) {
    // The .h file
    sGenerateClassDeclaration (this, context);
  }
  gCodeGen->stopClassData ();

  // All done with the cmodel calls memory
  gCodeGen->freeCModelCalls();

  return eCGVoid;
}

CGContext_t
NUScopeElab::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  if (isHierName (context)) {
    // print hierarchical name
    gCodeGen->CGOUT () << ENDL;
    gCodeGen->outputHierPath (gCodeGen->CGOUT (), getHier (), false);
  }

  return eCGVoid;
}


// Module instances, called for
// 1/ declaring instances of a module's
//    class within some instantiating module/class
// 2/ initializing class member A.B in the class constructor for A
//
CGContext_t
NUModuleInstance::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  if (isDeclare (context))
    // In eCGDeclare context, we just need to generate "classname objectname;"
    {
      gCodeGen->CGOUT () << mclass (mModule)
			 << " "
			 << member (this)
			 << "; // " << mModule->getClassDataAlignment () << ", "
			 << mModule->getClassDataSize () << ", "
			 << mModule->getClassInstDataSize () << ", "
			 << gCodeGen->allocation (0)
			 << ENDL;

      // Update the alignment of the current class by the storage required
      // for this instance.
      //
      gCodeGen->allocation (getModule ()->getClassInstDataSize (),
			   getModule ()->getClassDataAlignment (),
			   1);

      // Update the size of this class to include the instantiated class.
      getParentModule()->setClassInstDataSize(gCodeGen->allocation(0));
    }
  else if (isConstructor (context))
    // Emitting the member initialization in the parent constructor
    // 
    {
      gCodeGen->CGOUT () << TAG << member (this);

      // Get a list of the parameters and remove the ones that are
      // aliased instead of being actually passed as references
      // to initialize a pointer in the subclass.
      //
      NUPortConnectionCLoop ports (loopPortConnections ());
      NUPortConnectionVector params;

      remove_copy_if (ports.begin (), ports.end (),std::back_inserter (params),
		      std::ptr_fun (isNotConstructorPort));


      // Check that the class constructor has corresponding port declarations
      // (constructor parameters) for each of the instance port-connections that
      // we think are getting emitted.
      NUNetCVector constructorFormals;
      sGetConstructorParams (&constructorFormals, getModule ());

      NU_ASSERT (constructorFormals.size () == params.size (), this);
      

      // Walk whatever's left
      emitCodeList (&params, context, "(", ", ", ")");
    }

  else if (isNameOnly (context))
    gCodeGen->CGOUT () << member (this);
  else if (isMarkNets (context))
    // Mark the port-connections for things we actually need to generate
    // code for.  If we have an unreferenced net, it may get aliased to
    // a subnet - but if the local name is unused, we don't want to act
    // on that or force the actual net to be marked if it's not already
    // marked.
    {
      NUPortConnectionCLoop ports (loopPortConnections ());
      emitCodeList (&ports, context);
    }
	
  return eCGVoid;
}

CGContext_t
NUPortConnection::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

/*
  if (isUnconnectedActual ())
    return eCGVoid;
*/
  NU_ASSERT(!isUnconnectedActual(), this);
  NUNet* actual = getActualNet();
  NUNet* formal = getFormal();

  bool isInput = getType() == eNUPortConnectionInput;
  // Mark the actual as referenced because it will be passed
  // down in the constructor to the instantiation (which may never
  // reference its formal.
  actual->emitCode (eCGMarkNets);

  sConstructMemberInit (formal, actual, context, isInput);
  
  return eCGVoid;
}
