// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Handles nested NUVarselLvalue, generate bounds checking code.
*/
#ifndef __NESTEDPARTSEL_H__
#define __NESTEDPARTSEL_H__

#include "nucleus/Nucleus.h"
#include "nucleus/NUExpr.h"	// For NUOp::OpT

class CopyContext;

//!
/*!
 * 
 * 
 */

class NestedPartselLvalue
{
public:
  NestedPartselLvalue (const NUVarselLvalue* lv, 
		       const NUExpr* rhs, 
		       Fold* f, 
		       UtOStream&  o, 
		       bool oob):
    mLowerBound("nestedPartsel_lb"),
    mUpperBound("nestedPartsel_ub"),
    mLv(lv),mInnermost(NULL), 
    mNewIndexExpr(NULL), 
    mRHS(rhs), 
    mFold(f), 
    mOut(o), 
    mNoOOB(oob)
      {}

  ~NestedPartselLvalue() 
  {
    if (mNewIndexExpr)
      delete mNewIndexExpr;
  }

  //! true if there are any variable indices.
  bool needsBoundsChecking();    

  //! top level function.
  CGContext_t emitNestedPartselLvalue (CGContext_t c);

private:

  //!The output stream we are currently writing to
  /*!\return std::ostream & to current output file. */
  UtOStream& OUT() { return (UtOStream &) mOut;  }

  //! true if there are more than one variable indices.
  bool needsTempBounds( );

  //! to by-pass bounds checking if so specified or 
  /*! determined smartly.  NYI */
  inline bool noBoundsChecking(/*const NUVarselLvalue * lv*/);

  //! emit bounds checking code
  NUExpr* emitBoundsChecking (const NUVarselLvalue * lv, CGContext_t context);

  //! emit lower and upper bound temporaries declaraion/initialization.
  void emitBoundsTemp ();

  //! emit upper/lower bounds calculation statements  
  void emitBoundsCalc (const NUVarselLvalue * lv, CGContext_t c);

  //! emit size calculation for no temporary case
  void emitSizeCalc(CGContext_t c);

  //! emit SET_SLICE code
  CGContext_t emitSetSlice (CGContext_t c, bool t);
  CGContext_t genSetSlice(CGContext_t context, bool t);

  //! emit SET_BIT code
  CGContext_t emitSetBit(CGContext_t c, bool t);

  //! emit the index variable via emitUtil::emitIndex
  void emitIndex (const NUVarselLvalue * lv, CGContext_t context);

  //! emit the new index variable via emitUtil::emitIndex
  void emitNewIndex (CGContext_t context);

  //! print out the upper/lower bounds ifdef DEBUG_BOUNDS 
  void debugPrint (char* s);

  //! reset the new index field, delete the expr tree.
  void resetNewIndexExpr() {
    delete mNewIndexExpr; mNewIndexExpr=NULL;}

  //! find and set the innermost level of NUVarselLvalue
  void setInnermost(); 

  //! returns the new index expr.
  NUExpr * getNewIndexExpr() { return mNewIndexExpr; }

  //! set the new index expr with the given expr, make a 
  /*! copy if necessary. */
  void putNewIndexExpr(const NUExpr * e, bool cp);

  //! combine the current level index to the new index
  void combineNewIndexExpr(const NUExpr * idx);

  // easy formatting
  inline void endLine() {OUT() << ";\n       "; }
  inline void newLine() {OUT() <<  "\n       ";  }
  inline void newLine5() {newLine(); OUT() << "    ";}
  inline void newLine10() {newLine(); OUT() << "          ";}
  inline void newLine15() {newLine(); OUT() << "              ";}

  // special tests to skip already covered bounds checking
  bool isConditionOneCovered(const NUVarselLvalue * lv);
  bool isConditionTwoCovered(const NUVarselLvalue * lv);

  // temporary name for lower bound
  const char* mLowerBound;

  // temporary name for upper bound
  const char* mUpperBound;

  // the outermost NUVarselLvalue to process
  const NUVarselLvalue * mLv;

  // the innermost NUVarselLvalue
  const NUVarselLvalue * mInnermost;

  // the combined index expr at the varsel level
  NUExpr*    mNewIndexExpr;

  // the right hand side source to be assigned to the nested 
  // partselect lvalue
  const NUExpr*    mRHS;

  //! Fold optimization driver (from codegen)
  Fold*            mFold;

  //! File where we are currently writing to (from codegen)
  UtOStream&       mOut;

  // the flag for no out of bound checking. NYI.
  bool mNoOOB;

};  // class nestedPartselLvalue

#endif

/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
