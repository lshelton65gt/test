// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*/

#include "CGTopLevelSchedule.h"
#include "compiler_driver/CarbonContext.h"
#include "codegen/CGNetElabCount.h"
#include "langcpp/LangCppWalker.h"
#include "schedule/Event.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Schedule.h"
#include "NetMapCompare.h"
#include "GenSchedFactory.h"
#include "emitUtil.h"
#include "GenProfile.h"
#include "nucleus/NUDesign.h"

using namespace emitUtil;

//! Walk a LangCpp constructs and allocate registers
/*!
  After walking a LangCpp construct this will have the 
  LangCppVariable that is read the most but at least 5
  times. Obviously, if there are no variables or none of the variables
  are used that many times no register will be allocated. 

  Here, register allocation, means declaring a variable with the
  'register' keyword. gcc does use this keyword in some situations,
  but declaring a variable as a register does not guarantee that it
  will be assigned a register. Most likely, gcc will use it to break a
  tie or as a starting point. But, if you mark everything as a
  register it definitely has no effect. 
  
  I've seen this work with the Maxsim testcase in bug 6090 when
  having a recurring if in ResetGuard code use a temporary.
  For some reason that temporary was not in a register. The
  code before which looks more inefficient was actually faster because
  it used a register. Anyway, by using the register keyword on the
  temporary, it was assigned a register and it performed as well as
  the untemped ifs. See bug 6208.
  I ended up not doing anything with that ResetGuard code, since I
  couldn't get it to perform any faster, even by merging all of them
  into 1 if.

  I'm limiting the number of register allocations to one per function
  currently. There are only 4 global registers available on an x86,
  and I don't want to try to allocate to all of them. So, I only want
  to mark the most used variable.

  This should be used on a per-function basis.

  Experiments with allocating 2, 3, and 4 registers were done and none
  performed as well as only allocating 1. The speed improvement for 1
  register allocation is, of course, design dependent, but it isn't
  much for most designs. 0% for most, 1% for some. For a small design
  it could be 5% as I saw with the maxsim testcase in bug 6090. More
  research should be done on this, especially when gcc4 is used.
*/
class CGTopLevelSchedule::RegisterAllocator : public virtual LangCppWalker
{
  struct Register
  {
    Register() : mNumReads(0), mVar(NULL)
    {}

    
    Register& operator=(const Register& src)
    {
      mNumReads = src.mNumReads;
      mVar = src.mVar;
      return *this;
    }
  
    void put(UInt32 numReads, LangCppVariable* var)
    {
      mNumReads = numReads;
      mVar = var;
    }

    UInt32 mNumReads;
    LangCppVariable* mVar;
  };
  
public:
  CARBONMEM_OVERRIDES

  RegisterAllocator() {}
  virtual ~RegisterAllocator() {}
  
  
  virtual void visitVariable(LangCppVariable* variable)
  {
    // Only assign registers to pods that aren't references.
    // Also don't mark an 'unused' variable as a register, even if it
    // is used a lot.
    const LangCppType* type = variable->getType();
    if (! variable->isDeclaredUnused() && (type->castPod() != NULL) && (type->castReference() == NULL))
    {
      UInt32 numReads = variable->numReads();
      // Only care if the variable is read often.
      if (numReads > 4)
        maybeFillRegister(numReads, variable);
    }
  }

  void allocate()
  {
    LangCppVariable* var1 = mRegister.mVar;
    if (var1)
      var1->addAttribute(LangCppVariable::eAttribRegister);
  }

private:
  Register mRegister;

  void maybeFillRegister(UInt32 numReads, LangCppVariable* variable)
  {
    if (mRegister.mNumReads < numReads)
    {
      mRegister.put(numReads, variable);
    }
  }
};

//! Constructor
CGTopLevelSchedule::CGTopLevelSchedule(CodeGen* codegen, int top_level_sched)
  :
  mCodegen(codegen), 
  mNumExtParams(0),
  mDerivedClockCount(0),
  mChangedIdent(NULL),
  mChangeArrayMemberIdent(NULL),
  mDclChangeArrayIdent(NULL),
  mTopLevelVarIdent(NULL),
  mHdlVarIdent(NULL),
  mDescrVarIdent (NULL),
  mUpdateInputsIdent(NULL),
  mTimeChangedIdent(NULL),
  mDoInitIdent(NULL),
  mDclDoInitIdent(NULL),
  mSettledIdent(NULL),
  mTriggerFactory(&mLangCppFactory),
  mTopLevelSchedID(top_level_sched)
{
  mSched = codegen->getSched();
  mCppStmtTree = new LangCppFileScope(&mCppGlobal);
  mTriggerFactory.putCppGlobal(&mCppGlobal);
  mChangedMap = codegen->mChangedMap;
#if pfLINUX64
  if (mCodegen->is32bit()) {
    LangCppType::sPutArchitecture(LangCppType::eArch32);
  }
#endif
  mHdlVarIdent = mLangCppFactory.createIdent(CRYPT("hdl"), 4);
  mDescrVarIdent = mLangCppFactory.createIdent(CRYPT("descr"), 4);
  mHdlMemberIdent = mLangCppFactory.createIdent(CRYPT("mHdl"), 4);
}

//! destructor
CGTopLevelSchedule::~CGTopLevelSchedule() {
  delete mCppStmtTree;
}
  
//! Call this before emitting the schedule to initialize data
/*!
  The clocks and primary inputs are calculated here. That
  information is used later to determine change array indices and
  the size of the change array.
*/
void CGTopLevelSchedule::analyzeTopLevelSchedule()
{
  calcDerivedClocks();
  calcPrimaryInputs();

  mChangedIdent = mLangCppFactory.createArrayIdent(CRYPT("changed"), mNumExtParams, 1);
  mChangeArrayMemberIdent = mLangCppFactory.createIdent(CRYPT("mChangeArray"), 4);
  mDclChangeArrayIdent = mLangCppFactory.createArrayIdent(CRYPT("dcl_changed"), mNumExtParams, 1);
}

//! Generate the glitch test
/*!
  If glitch detection is on due to -g then this does nothing, so we
  will run the glitch test unconditionally.
    
  Otherwise, this generates 
  \code
  if (hdl->checkClockGlitches())
  \endcode
*/
LangCppScope* CGTopLevelSchedule::codeGlitchTest(LangCppScope* scope)
{
  LangCppScope* ret = NULL;
  if (mCodegen->getCarbonContext()->getArgs()->getBoolValue("-g"))
  {
    // We are always doing glitch detection. No if scope needed
    scope->addLineComment(CRYPT("-g turns glitch detection on"));
    ret = scope;
  } else {
    LangCppVariable* descr = scope->findVariable(mDescrVarIdent);
    CE_ASSERT(descr, mDescrVarIdent);
    LangCppFuncCall* cond = scope->callMethod(descr, CRYPT("checkClockGlitches"));
    LangCppIf* ifScope = scope->addIf(cond);
    ret = ifScope->getClauseScope();
  }
  return ret;
}

//! Check if a derived clock caused a glitch
/*!
  This generates
  \code
  if ((changed[index] & CARBON_CHANGE_GLITCH_MASK) ==
  CARBON_CHANGE_GLITCH_MASK) {
    hdl->SHLClockGlitch(clkIndex, dclk == 0, dclk);
  }
  \endcode
*/
void CGTopLevelSchedule::codeDerivedClockGlitchDetect(UInt32 clkIndex, 
                                                      const NUNetElab* derivedClk, 
                                                      LangCppScope* glitchScope)
{
  SInt32 chgArrayIndex = mChangedMap->getIndex(derivedClk);
  NU_ASSERT(chgArrayIndex != -1, derivedClk);

  // if ((changed[index] & CARBON_CHANGE_GLITCH_MASK) == CARBON_CHANGE_GLITCH_MASK
  CarbonExpr* triggerIdent = 
    mLangCppFactory.createIdentBitsel(mChangedIdent, chgArrayIndex, 1);
  CarbonExpr* triggerExpr = 
    mTriggerFactory.createGlitchExpr(triggerIdent);
  LangCppCarbonExpr* cond = 
    mLangCppFactory.createLangCppExpr(triggerExpr, glitchScope);
  LangCppIf* ifGlitch = glitchScope->addIf(cond);
  LangCppScope* ifGlitchScope = ifGlitch->getClauseScope();

  //  hdl->SHLClockGlitch(clkIndex, dclk == 0, dclk);
  UtString buf;
  buf << clkIndex;
  LangCppExpr* userExpr = ifGlitchScope->createUserExpr(buf.c_str());

  UtString dclkBuf;
  mCodegen->getNetElabEmitString(&dclkBuf, derivedClk);

  // just declare the net as a bool for now. We don't know what it
  // really is. Can figure it out, but isn't worth it here. We only
  // need a type to declare the variable in the scope so we can pass
  // it to the function
  LangCppVariable* dclkVar = mLangCppFactory.declareVariable(mCppGlobal.getBoolType(), dclkBuf.c_str(), ifGlitchScope);
  dclkVar->addAttribute(LangCppVariable::eAttribNoDeclare);
    
  LangCppExpr* notDclk = mLangCppFactory.createLogicalNot(dclkVar, ifGlitchScope);

  LangCppVariable* descr = glitchScope->findVariable(mDescrVarIdent);    
  LangCppExpr* glitchCallParams[] = NTARRAY(userExpr, notDclk, dclkVar);
  ifGlitchScope->addCall(ifGlitchScope->callMethod(descr, CRYPT("SHLClockGlitch"), glitchCallParams));
}

//! Create a DynBitVector of a given size
/*!
  This is used for checking primary clock races.
  Creates 
  DynBitVector varName(bvSize)
*/
LangCppVariable* CGTopLevelSchedule::createDynBVVar(const char* varName, UInt32 bvSize, LangCppScope* scope)
{
  LangCppClassType* dynClass = mCppGlobal.declareClassType(CRYPT("DynBitVector"));
  LangCppVariable* curChanges;
  // For models with a C interface to libcarbon, the DynBitVector is
  // allocated dynamically and assigned to a pointer.
  LangCppPointerType* pointerClass = dynClass->pointer();
  curChanges = mLangCppFactory.declareVariable(pointerClass, varName, scope);
  LangCppExpr* paramList[] = NTARRAY(mLangCppFactory.createSIntExpr(bvSize, scope));
  LangCppFuncCall* init = scope->callFunction(CRYPT("carbonInterfaceAllocDynBitVector"), paramList);
  scope->addAssign(curChanges, init);
  return curChanges;
}

//! Set a bit in a DynBitVector if a dci changed
/*!
  \param clkElab The derived clock that may have changed
  \param curChanges The DynBitVector
  \param index The DynBitVector index
  \param scope Current cpp scope
  \code
  if (changed[clkElabIndex])
  {
    curChanges.set(index);
  }
  \endcode
*/
void CGTopLevelSchedule::codeSetDynBVIfDciChanged(const NUNetElab* clkElab, LangCppVariable* curChanges, SInt32 index, LangCppScope* scope)
{
  SInt32 chgArrayIndex = mChangedMap->getIndex(clkElab);
  NU_ASSERT(chgArrayIndex != -1, clkElab);
    
  CarbonExpr* changedSelectIdent = 
    mLangCppFactory.createIdentBitsel(mChangedIdent, chgArrayIndex, 1);
  
  LangCppExpr* changedSelect = mLangCppFactory.createLangCppExpr(changedSelectIdent, scope);
  LangCppIf* ifScope = scope->addIf(changedSelect);
  LangCppScope* ifSubScope = ifScope->getClauseScope();
  LangCppExpr* paramList[] = NTARRAY(curChanges, mLangCppFactory.createSIntExpr(index, ifSubScope));
  ifSubScope->addCall(ifSubScope->callFunction(CRYPT("carbonInterfaceSetDynBitVector"), paramList));
}

//! Call hdl->checkPrimaryClockRace(curChanges)
void CGTopLevelSchedule::codeCheckPrimaryClkRace(LangCppScope* scope, LangCppVariable* curChanges)
{
  LangCppVariable* descr = scope->findVariable(mDescrVarIdent);
  LangCppExpr* paramList[] = {NULL, NULL};

  // Build the argument list for the function call, which takes a
  // DynBitVector reference.  For legacy models, the DynBitVector is
  // allocated on the stack, so we can use it as-is.  For new models,
  // we have a pointer to the DynBitVector so we need to dereference
  // it.
  LangCppDereference* curChangesDeref = scope->dereference(curChanges);
  paramList[0] = curChangesDeref;
  scope->addCall(scope->callMethod(descr, CRYPT("checkPrimaryClockRace"), paramList));
  // We're done with the DynBitVector now.  In the legacy model, its
  // destructor will be called when it goes out of scope, but in the
  // new model we need to free it explicitly.
  paramList[0] = curChanges;
  scope->addCall(scope->callFunction(CRYPT("carbonInterfaceFreeDynBitVector"), paramList));
}

//! Call hdl->clearPrimaryClockChanges()
void CGTopLevelSchedule::codeClearPrimaryClkChanges(LangCppScope* scope)
{
  LangCppVariable* timeChanged = scope->findVariable(mTimeChangedIdent);
  LangCppIf* ifTimeChangedMain = scope->addIf(timeChanged);
  LangCppScope* ifTimeChanged = ifTimeChangedMain->getClauseScope();
  LangCppScope* ifGlitch = codeGlitchTest(ifTimeChanged);

  LangCppVariable* descr = ifGlitch->findVariable(mDescrVarIdent);
  ifGlitch->addCall(ifGlitch->callMethod(descr, CRYPT("clearPrimaryClockChanges")));
}

//! Save value of a derived clock to oldclock_dciIndex
/*!
  \param prefix "dcl_" or ""
  \param derivedClk Derived clock's value being saved
  \param scope current scope
  \code 
  bool oldClock_dci_ = derivedClk;
  \endcode
*/
void CGTopLevelSchedule::codeSaveDclValue(const NUNetElab* derivedClk, LangCppScope* scope, const char* prefix)
{
  int dci = mChangedMap->getIndex(derivedClk);
    
  UtString netElabBuf;
  mCodegen->getNetElabEmitString(&netElabBuf, derivedClk);
    
  UtString oldClockName(prefix);
  oldClockName << CRYPT("oldclock_") << dci << "_";
  LangCppVariable* oldClock = mLangCppFactory.declareVariable(mCppGlobal.getBoolType(), oldClockName.c_str(), scope);
  LangCppExpr* clockVal = scope->createUserExpr(netElabBuf.c_str());
  scope->addDeclAssign(oldClock, clockVal);
}

//! Call the input/async schedule
/*!
  \code
  if (updateInputs) {
    carbon_design_data(hdl);
  }
  \endcode
*/
void CGTopLevelSchedule::codeInputFlowCondRunData(LangCppScope* scope)
{
  LangCppVariable* updateInputs = scope->findVariable(mUpdateInputsIdent);
  LangCppIf* ifInputFlowMain = scope->addIf(updateInputs);
  LangCppScope* ifInputFlow = ifInputFlowMain->getClauseScope();
    
  UtString functionName("carbon_");
  functionName << *(mCodegen->getIfaceTag()) << CRYPT("_data");
  LangCppVariable* descr = scope->findVariable(mDescrVarIdent);
  LangCppExpr* paramList[] = NTARRAY(descr);
  LangCppFuncCall* inputSchedCall = ifInputFlow->callFunction(functionName.c_str(), paramList);
  ifInputFlow->addCall(inputSchedCall);
}
  
//! Code a ResetGuard to the current scope
LangCppScope* CGTopLevelSchedule::codeResetGuardToScope(const ResetGuards& rg, 
                                                        LangCppScope* cppScope)
{
  const SCHScheduleMask* mask = rg.mask;
  // If the mask is empty, there are no reset guards required for
  // this schedule.
  if (! mask)
    return NULL;

  /* 
     Loop the mask and find any resets (clocks) that must be stable
     off, so if it is a posedge reset, we look for it to be 0.

     If the reset was marked fast or if the clock in the reset guard
     is marked as a slow clock then we are done. Otherwise, we need to
     see if the corresponding clock changes at all. If so, we return
     the scope inside an if scope which constitutes the ResetGuard.
     
     If nothing interesting happens here we return NULL, which means
     that there is nothing to guard.
  */
  
  bool isRGClockSlow = mCodegen->isSlowClock(rg.mClk);
  
  CarbonIdent* changedIdent = mChangedIdent;

  // The mask is the resets that must be false.
  LangCppCarbonExpr* currentExpr = NULL;
  for (SCHScheduleMask::SortedEvents l = rg.mask->loopEventsSorted ();
       !l.atEnd (); ++l)
  {
    const SCHEvent *event = *l;
    if( !event->isClockEvent() )
      continue;
      
    UtString rstName;
    const NUNetElab* reset = SCHSchedule::clockFromName(event->getClock());
    mCodegen->getNetElabEmitString(&rstName, reset);
      
    LangCppVariable* rstVar = mLangCppFactory.declareVariable(mCppGlobal.getBoolType(), rstName.c_str(), cppScope);
    rstVar->addAttribute(LangCppVariable::eAttribNoDeclare);

    LangCppCarbonExpr* rstPolarity = rstVar;
    switch( event->getClockEdge() ) {
    case eClockPosedge:
    case eLevelHigh:
      rstPolarity = mLangCppFactory.createLogicalNot(rstVar, cppScope);
      break;
    case eClockNegedge:
    case eLevelLow:
      break;
    default:
      NU_ASSERT ("unexpected clock edge" == 0, 
                 reset);
    }

    if (currentExpr)
      // This won't happen first time through the for
      currentExpr = mLangCppFactory.createBinaryOp(CarbonExpr::eBiBitAnd,
                                                   currentExpr, rstPolarity,
                                                   cppScope);
    else
      // this will happen first time through the for
      currentExpr = rstPolarity;
      
    // Also check if the clock changed. This is because when
    // reset guards go away they have to be delayed by one call
    // to carbon_xxx_schedule
    CodeGen::ClockIter i = find_if( rg.clockTable.begin(), 
                                    rg.clockTable.end(),
                                    NetMapCompare (reset));
    NU_ASSERT(i != rg.clockTable.end(), reset);

    if (! isRGClockSlow &&
        ! mCodegen->isFastReset(reset))
    {
      const NUNetElab* n = i->first;

      // check if the clock changes
      CarbonExpr* changedBitsel = mLangCppFactory.createIdentBitsel(changedIdent, mChangedMap->getIndex(n), 1);
      CarbonExpr* notChanged = mLangCppFactory.createLogicalNot(changedBitsel);
      currentExpr = mLangCppFactory.createBinaryOp(CarbonExpr::eBiBitAnd, currentExpr, notChanged, cppScope);
    }
  }

  LangCppScope* ret = NULL;
  if (currentExpr)
  {
    // Keep this dead code for now. Enabling this slowed down an Arm
    // primecell in Maxsim, which reused the temporary 10 times. The
    // slowdown is counter-intuitive, but is due to the loss of the
    // use of a register. Gcc was allocating a register before. With
    // the event temp a register is not allocated. See bug 6208.
#if 0
    // Try to reduce the computation of reset guards by assigning to
    // an event tmp first.
    LangCppVariable* var = cppScope->getAlias(currentExpr);
    if (var == NULL)
    {
      var = mTriggerFactory.genEventTmp(cppScope);
      mLangCppFactory.addAssignAlias(var, currentExpr, cppScope);
    }
    LangCppIf* ifMain = cppScope->addIf(var);
#else
    LangCppIf* ifMain = cppScope->addIf(currentExpr);
#endif
    ret = ifMain->getClauseScope();
  }
  return ret;
}

bool CGTopLevelSchedule::sIsDclPrefix(const char* varPrefix)
{
  bool isdcl = false;
  if (strlen(varPrefix) > 0)
  {
    // we only expect an empty varPrefix or dcl_
    // Will change this to be a bool or enum-based selection instead
    // of string-based
    INFO_ASSERT(strcmp(varPrefix, "dcl_") == 0, varPrefix);
    isdcl = true;
  }
  return isdcl;
}

//! Code Edge and ExtraConds to current scope
LangCppScope* CGTopLevelSchedule::codeEdgeNetAndExtraEdgesToScope(const char* varPrefix, 
                                                                  const NUNetElab* clk, 
                                                                  SInt32 foundClockIndex, 
                                                                  const SCHScheduleMask* extraEdges, 
                                                                  bool isPosedge, 
                                                                  LangCppScope* cppScope)
{
  CarbonIdent* changedIdent = mChangedIdent;
  if (sIsDclPrefix(varPrefix))
    changedIdent = mDclChangeArrayIdent;    

  /*
    Create an expression like the following
    if (((changed[foundClockIndex] & CARBON_CHANGE_RISE_MASK) || 
    (reset && (extraEdges))))
  */
  CarbonExpr* changedSelectIdent = 
    mLangCppFactory.createIdentBitsel(changedIdent, foundClockIndex, 1);

  CarbonExpr* triggerExpr = 
    mTriggerFactory.createEdgeExpr(changedSelectIdent, 
                                   isPosedge ? eClockPosedge : eClockNegedge);
  
  // Look if we have event tmp'd the trigger expression. If not, just
  // use the expression. Don't bother event tmping if it already
  // hasn't. In all likelihood the expression will only appear once. I
  // slowdown was discovered on an Arm primecell in Maxsim when
  // creating too many event tmps.
#if 1
  LangCppVariable* triggerVar = cppScope->getAlias(triggerExpr);
#else
  LangCppVariable* triggerVar = NULL;
#endif
  LangCppExpr* langTriggerExpr = triggerVar;
  if (langTriggerExpr == NULL)
    langTriggerExpr = mLangCppFactory.createLangCppExpr(triggerExpr, cppScope);

  LangCppExpr* cond = NULL;    
  if (extraEdges)
  {
    // Make sure the reset is steady on
    UtString clkName;
    mCodegen->getNetElabEmitString(&clkName, clk);    
    CarbonIdent* clkIdent = mLangCppFactory.createIdent(clkName.c_str(), 1);
    CarbonExpr* clkVal = clkIdent;
    if (! isPosedge)
      clkVal = mLangCppFactory.createLogicalNot(clkIdent);
      
    CGScheduleTrigger* maskTrigger = createMaskTrigger(extraEdges);    
    if (maskTrigger)
    {
      if (changedIdent != mChangedIdent)
        // We have to recalculate the trigger for the other change
        // array (the dcl_changed array)
        maskTrigger = mTriggerFactory.recalculateTrigger(maskTrigger, changedIdent, mChangedIdent);
      
      LangCppCarbonExpr* schedTrigExpr = 
        mTriggerFactory.translateTriggerToCpp(maskTrigger, cppScope);
#if 0
      // Note, I am no longer able to reproduce the speed up with
      // having event temps be unsigned chars instead of
      // booleans. This may warrant further investigation.

      // schedule triggers are not booleans. We need to do a reduction
      // or on it ( != 0) so we can & with the scalar reset.
      schedTrigExpr = mLangCppFactory.createNeqZero(schedTrigExpr, cppScope);
#endif
      LangCppExpr* trigAndClkExpr = mLangCppFactory.createBinaryOp(CarbonExpr::eBiBitAnd,
                                                                   schedTrigExpr, clkVal, 
                                                                   cppScope);
      cond = mLangCppFactory.createBinaryOp(CarbonExpr::eBiBitOr,
                                            trigAndClkExpr, langTriggerExpr,
                                            cppScope);
    }
  }
    
  if (cond == NULL)
    // No extra edges
    cond = langTriggerExpr;
  
  LangCppIf* ifScopeMain = cppScope->addIf(cond);
  return ifScopeMain->getClauseScope();
}
  
//! Used by CodeGen::SyncOnClock.
LangCppScope* CGTopLevelSchedule::codeChangedRefsToScope(CodeGen::UInt32Set& sensed, LangCppScope* cppScope)
{
  // For the sensed set, we are creating a level trigger for each
  // index into the change array.
  CGScheduleTriggerVec triggers;    
  for( CodeGen::UInt32Set::const_iterator i = sensed.begin(),
         e = sensed.end(); i != e; ++i)
  {
    CarbonExpr* inputIdent = 
      mLangCppFactory.createIdentBitsel(mChangedIdent, *i, 1);
    CGScheduleTrigger* inputTrigger = mTriggerFactory.createLevelTrigger(inputIdent);
    triggers.push_back(inputTrigger);
  }
    
  // if there is more than one trigger, create a compound trigger.
  CGScheduleTrigger* assocTrigger = NULL;
  if (! triggers.empty())
  {
    if (triggers.size() == 1)
      assocTrigger = triggers.back();
    else
      assocTrigger = mTriggerFactory.createCompoundTrigger(triggers, CarbonExpr::eBiBitOr);
  }
  
  // If there is no trigger there is nothing to do.
  LangCppScope* ret = NULL;
  if (assocTrigger)
  {
    LangCppExpr* cond = mTriggerFactory.translateTriggerToCpp(assocTrigger, cppScope);
    LangCppIf* ifMain = cppScope->addIf(cond);
    ret = ifMain->getClauseScope();
  }
  return ret;
}

//! Code a derived clock call trigger
/*!
  This creates a trigger based on the dcl mask and dcl input nets.
  The actual triggers are based on the 'changed' variable, but that
  is translated to 'dcl_changed'. This makes it easier to keep a map
  of masks to triggers.

  This end results is an if statement with the condition to call a
  dcl schedule.
*/
LangCppScope* CGTopLevelSchedule::codeDclTriggerToScope(SCHDerivedClockLogic* dcl,
                                                        GenSched* derivedSched, 
                                                        const char* varPrefix, 
                                                        LangCppScope* cppScope)
{
  CarbonIdent* changedIdent = mChangedIdent;
  if (sIsDclPrefix(varPrefix))
    changedIdent = mDclChangeArrayIdent;

  const SCHInputNets* levelInputs = dcl->getCombinationalSchedule()->getAsyncInputNets();
  const SCHScheduleMask* mask = dcl->getMask();

  // If there are no level or edge triggers return NULL. This means
  // we are not to call the underlying schedule.
  if ((levelInputs == NULL) && (mask == NULL))
    return NULL;

  CGScheduleTrigger* inputNetTrig = NULL;
  if (levelInputs)
    inputNetTrig = createInputNetTrigger(levelInputs);

  CGScheduleTrigger* edgeTrig = NULL;
  if (mask)
    edgeTrig = createMaskTrigger(mask);

  // Note that either, both, or none of the triggers could be NULL.

  UInt32 edgeTrigCost = 0;
  UInt32 inputNetCost = 0;
  if (inputNetTrig)
    inputNetCost = inputNetTrig->getCost();
  if (edgeTrig)
    edgeTrigCost = edgeTrig->getCost();
    
  LangCppScope* ret = NULL;
  if (isCostEffective(inputNetCost + edgeTrigCost, derivedSched->getCost(), cppScope))
  {
    // If we are dealing with dcl variables, recalculate the triggers
    if (changedIdent != mChangedIdent)
    {
      if (inputNetTrig)
        inputNetTrig = mTriggerFactory.recalculateTrigger(inputNetTrig, changedIdent, mChangedIdent);
      if (edgeTrig)
        edgeTrig = mTriggerFactory.recalculateTrigger(edgeTrig, changedIdent, mChangedIdent);      
    }
      
    // emit the triggers into LangCppExprs
    LangCppExpr* levelCond = NULL;
    if (inputNetTrig)
      levelCond = mTriggerFactory.translateTriggerToCpp(inputNetTrig, cppScope);
    LangCppExpr* edgeCond = NULL;
    if (edgeTrig)
      edgeCond = mTriggerFactory.translateTriggerToCpp(edgeTrig, cppScope);

    // If we have both a level and an edge trigger, combine them with
    // an eBiBitOr; otherwise, just assign it to the one that isn't
    // NULL, or keep it NULL if both are NULL.
    LangCppExpr* cond = NULL;
    if (levelCond && edgeCond)
      cond = mLangCppFactory.createBinaryOp(CarbonExpr::eBiBitOr, levelCond, edgeCond, cppScope);
    else if (levelCond)
      cond = levelCond;
    else
      cond = edgeCond;
      
    if (cond) {
      LangCppIf* ifMain = cppScope->addIf(cond);
      ret = ifMain->getClauseScope();
    }
  }
  else
    // It isn't cost effective to trigger the schedule just call it.
    ret = cppScope;
  return ret;
}

//! Update the changed array based on old and new vals of derived clks
/*!
  dcl change[index] = 
  or 
  change[index] |=
  do_init | (oldclock ^ newclock) << newclock;
*/
void CGTopLevelSchedule::codeDerivedChangedAssign(const char* writeVarPrefix, 
                                                  const char* readVarPrefix,
                                                  SInt32 derivedClkIndex, 
                                                  bool orChange,
                                                  const char* netElabName, 
                                                  LangCppScope* cppScope)
{
  CarbonIdent* changeIdent = mChangedIdent;
  CarbonIdent* doInitIdent = mDoInitIdent;

  if (sIsDclPrefix(writeVarPrefix))
    changeIdent = mDclChangeArrayIdent;
      
  if (sIsDclPrefix(readVarPrefix))
    doInitIdent = mDclDoInitIdent;

  /*
    Creating the following type of expression:

    changed[index] |= 
    ((do_init | (oldclockVal ^ newclockVal)) << newclockVal;

    The assignment may be a '=' or a '!='
  */
  LangCppExpr* lhs = mLangCppFactory.createIdentBitselExpr(changeIdent, derivedClkIndex, 1, cppScope);
  
  // gather up the rhs variables
  UtString oldClockName;
  oldClockName << readVarPrefix << CRYPT("oldclock_") << derivedClkIndex << "_";
  CarbonIdent* oldClockIdent = mLangCppFactory.createIdent(oldClockName.c_str(), 1);
  LangCppVariable* oldClock = cppScope->findVariable(oldClockIdent);
  INFO_ASSERT(oldClock, oldClockName.c_str());
    
  LangCppVariable* clkVar = mLangCppFactory.declareVariable(mCppGlobal.getBoolType(), netElabName, cppScope);
  clkVar->addAttribute(LangCppVariable::eAttribNoDeclare);
  
  LangCppVariable* changedVar = cppScope->findVariable(changeIdent);
  CE_ASSERT(changedVar, changeIdent);

  LangCppVariable* doInitVar = cppScope->findVariable(doInitIdent);
  CE_ASSERT(doInitVar, doInitIdent);


  // Now combine them into the change detect expression
  LangCppExpr* expr1 = mLangCppFactory.createBinaryOp(CarbonExpr::eBiBitXor, oldClock, clkVar, cppScope);
  LangCppExpr* expr2 = mLangCppFactory.createBinaryOp(CarbonExpr::eBiBitOr, doInitVar, expr1, cppScope);
  LangCppExpr* rhs = mLangCppFactory.createBinaryOp(CarbonExpr::eBiLshift, expr2, clkVar, cppScope);

  if (! orChange)
    cppScope->addAssign(lhs, rhs);
  else
    cppScope->addOrAssign(lhs, rhs);
}

//! Declare any needed vars for dcl cycles in this scope
void CGTopLevelSchedule::declareDclCycleVars(LangCppScope* cppScope)
{
  // dcl_changed
  LangCppType* carbChgTypeId = mCppGlobal.getType(CRYPT("CarbonChangeType"));
  LangCppVariable* dclChange = cppScope->declareVariable(carbChgTypeId, mDclChangeArrayIdent);
  dclChange->addAttribute(LangCppVariable::eAttribUnused);

  // dcl_do_init
  mDclDoInitIdent = mLangCppFactory.createIdent(CRYPT("dcl_do_init"), 1);
  LangCppVariable* dclDoInit = cppScope->declareVariable(mCppGlobal.getSInt8Type(), mDclDoInitIdent);
  dclDoInit->addAttribute(LangCppVariable::eAttribUnused);
  LangCppExpr* zero = mLangCppFactory.createZeroExpr(4, cppScope);
  cppScope->addDeclAssign(dclDoInit, zero);
}

//! memcpy(dcl_change, change)
void CGTopLevelSchedule::copyChangeToDclChange(LangCppScope* cppScope)
{
  // copy change array to dcl_change array

  LangCppVariable* dclChange = cppScope->findVariable(mDclChangeArrayIdent);
  LangCppVariable* change = cppScope->findVariable(mChangedIdent);
  CE_ASSERT(dclChange, mDclChangeArrayIdent);
  CE_ASSERT(change, mChangedIdent);
    
  cppScope->addMemCpy(dclChange, change, mNumExtParams);
}
  
//! changed[index] = dcl_changed[index]
void CGTopLevelSchedule::copyDclChangeIndexToChangeIndex(LangCppScope* cppScope, SInt32 index)
{
  LangCppExpr* lhs = mLangCppFactory.createIdentBitselExpr(mChangedIdent, index, 1, cppScope);
  LangCppExpr* rhs = mLangCppFactory.createIdentBitselExpr(mDclChangeArrayIdent, index, 1, cppScope);
  cppScope->addAssign(lhs, rhs);
}

//! memset(dcl_changed, CARBON_CHANGE_NONE_MASK)
void CGTopLevelSchedule::clearDclChanged(LangCppScope* scope)
{
  LangCppVariable* dclChange = scope->findVariable(mDclChangeArrayIdent);
  CE_ASSERT(dclChange, mDclChangeArrayIdent);
    
  LangCppVariable* noneMask = mLangCppFactory.declareVariable(mCppGlobal.getUCharType(), "CARBON_CHANGE_NONE_MASK", scope);
  noneMask->addAttribute(LangCppVariable::eAttribNoDeclare);
  scope->addMemSet(dclChange, noneMask, mNumExtParams);
}

//! [dcl_]changed[index] = CARBON_CHANGE_NONE_MASK
void
CGTopLevelSchedule::clearChangedFlag(SInt32 changeIndex, LangCppScope* scope,
                                     bool dcl)
{
  // Clear the ident they requested
  CarbonIdent* changedIdent;
  if (dcl) {
    changedIdent = mDclChangeArrayIdent;
  } else {
    changedIdent = mChangedIdent;
  }
  LangCppVariable* changed = scope->findVariable(changedIdent);
  CE_ASSERT(changed, mChangedIdent);

  LangCppExpr* lhs = mLangCppFactory.createIdentBitselExpr(changedIdent, changeIndex, 1, scope);
  
  LangCppVariable* noneMask = mLangCppFactory.declareVariable(mCppGlobal.getUCharType(), "CARBON_CHANGE_NONE_MASK", scope);
  noneMask->addAttribute(LangCppVariable::eAttribNoDeclare);
  scope->addAssign(lhs, noneMask);
}
  
//! settled = 1; dcl_do_init = 0;
void CGTopLevelSchedule::codeSetDclSettledFlags(LangCppScope* scope)
{
  LangCppVariable* settled = scope->findVariable(mSettledIdent);
  CE_ASSERT(settled, mSettledIdent);

  LangCppVariable* dclDoInit = scope->findVariable(mDclDoInitIdent);
  CE_ASSERT(dclDoInit, mDclDoInitIdent);
    
  LangCppExpr* falseExpr =  mLangCppFactory.createZeroExpr(4, scope);
  LangCppExpr* trueExpr = mLangCppFactory.createOneExpr(4, scope);
  scope->addAssign(settled, trueExpr);
  scope->addAssign(dclDoInit, falseExpr);
}

//! Save oldnet values
/*!
  \code
  netType oldnet_netIndex;
  oldnet_netIndex = netElab;
  \endcode
*/
void CGTopLevelSchedule::saveNetValue(const char* netType, UInt32 netIndex, const NUNetElab* netElab, 
                                      LangCppScope* scope)
{
  UtString stmt;
  const char* oldNetCrypt = CRYPT(" oldnet_");
  UtString oldNet;
  oldNet << oldNetCrypt << netIndex <<  "_";
  // Cannot declAssign because Memory<>'s do not have explicit
  // copy constructors.

  // declare
  stmt << " " << netType << oldNet << ";";
  scope->addUserStatement(stmt);
    
  stmt.clear();
    
  // assign
  stmt << oldNet << " = ";
  mCodegen->getNetElabEmitString(&stmt, netElab);
  stmt << ";";
  scope->addUserStatement(stmt);
}

//! settled &= (oldnet_netIndex == netElab);
void CGTopLevelSchedule::updateSettledWithOldNetChk(SInt32 netIndex, const NUNetElab* netElab, 
                                                    LangCppScope* scope)
{
  LangCppVariable* settled = scope->findVariable(mSettledIdent);
  CE_ASSERT(settled, mSettledIdent);
    
  UtString rhs;
  rhs << CRYPT("(oldnet_") << netIndex << "_ == ";
  mCodegen->getNetElabEmitString(&rhs, netElab);
  rhs << ")";
  LangCppExpr* expr = scope->createUserExpr(rhs.c_str());
    
  scope->addAndAssign(settled, expr);
}

//! Initialize loop for a dcl cycle
/*!
  This used to be a while loop in the original codegen. I changed it
  to a for loop as they tend to be more useful.

  \code
  settled = 0;
  dcl_do_init = do_init;
  for (SInt32 iterations = 0; (iterations < 32) & (settled == 0);
  ++iterations) {
  \endcode
*/
LangCppScope* CGTopLevelSchedule::initDclCycleLoop(LangCppScope* cppScope)
{
  cppScope->addLineComment(CRYPT("Loop until the cycle settles or 32 iterations"));

  // settled
  if (! mSettledIdent)
    mSettledIdent = mLangCppFactory.createIdent(CRYPT("settled"), 1);    
  LangCppVariable* settled = cppScope->declareVariable(mCppGlobal.getSInt8Type(), mSettledIdent);
  LangCppExpr* zero = mLangCppFactory.createZeroExpr(4, cppScope);
  cppScope->addAssign(settled, zero);
    
  // dcl_do_init
  INFO_ASSERT(mDclDoInitIdent, "dcl cycle var not initialized.");
  INFO_ASSERT(mDoInitIdent, "schedule var not initialized.");
  LangCppVariable* dclDoInit = cppScope->declareVariable(mCppGlobal.getSInt8Type(), mDclDoInitIdent);
  LangCppVariable* doInit = cppScope->declareVariable(mCppGlobal.getSInt8Type(), mDoInitIdent);
  cppScope->addAssign(dclDoInit, doInit);

  // iterations
  LangCppFor* forMain = cppScope->addFor();        
  LangCppVariable* iterations = mLangCppFactory.declareVariable(mCppGlobal.getSInt32Type(), CRYPT("iterations"), forMain);
  
  LangCppAssign* forInit = forMain->addDeclAssign(iterations, zero);
  forMain->putInitClause(forInit);
    
  LangCppExpr* forCond1 = mLangCppFactory.createLessThanInt(iterations, 32, forMain);
  LangCppExpr* forCond2 = mLangCppFactory.createLogicalNot(settled, forMain);
  LangCppExpr* forCond = 
    mLangCppFactory.createBinaryOp(CarbonExpr::eBiBitAnd, forCond1, forCond2, forMain);
  forMain->putLoopConditional(forCond);
  
  LangCppAssign* forPost = forMain->addPrefixIncrement(iterations);
  forMain->putPostClause(forPost);
  
  return forMain->getClauseScope();
}

//! changed[index] |= dcl_changed[index];
void CGTopLevelSchedule::updateChangedWithDclChanged(LangCppScope* cppScope, SInt32 index)
{
  LangCppExpr* lhs = mLangCppFactory.createIdentBitselExpr(mChangedIdent, index, 1, cppScope);
  LangCppExpr* rhs = mLangCppFactory.createIdentBitselExpr(mDclChangeArrayIdent, index, 1, cppScope);
  cppScope->addOrAssign(lhs, rhs);
}

// if (dcl_changed[index]) settled = 0;
void CGTopLevelSchedule::clearSettledIfDclChanged(LangCppScope* cppScope, SInt32 index)
{
  LangCppExpr* cond = mLangCppFactory.createIdentBitselExpr(mDclChangeArrayIdent, index, 1, cppScope);
  
  LangCppIf* ifScopeMain = cppScope->addIf(cond);
  LangCppScope* ifScope = ifScopeMain->getClauseScope();
  
  LangCppVariable* settled = cppScope->findVariable(mSettledIdent);
  CE_ASSERT(settled, mSettledIdent);
  LangCppExpr* zero = mLangCppFactory.createZeroExpr(4, cppScope);
  
  ifScope->addAssign(settled, zero);
}
  
//! Emit the top-level schedule into schedSrc
/*!
  This emits the top-level schedule into an IR (LangCppStmtTree),
  and then emits that to the UtOStream.
*/
void CGTopLevelSchedule::emit(UtOStream& schedSrc)
{
  // Declare oft-used classes
  mCppGlobal.declareClassType(CRYPT("carbon_model_descr"));
  mCppGlobal.declarePodType(CRYPT("CarbonChangeType"), mCppGlobal.getUCharType());
  mCppGlobal.declareClassType(CRYPT("CarbonChangeArray"));
  mCppGlobal.declareClassType(CRYPT("carbon_simulation"));
  mCppGlobal.declarePodType(CRYPT("CarbonStatus"), mCppGlobal.getSInt32Type());
  codeDataFunction(schedSrc);
  codePostSchedFunction(schedSrc);
  codeClockFunction(schedSrc);
  // This codes the control functions that call the previous three.
  codeScheduleFunction(schedSrc);
}

//! run the deposit_combo_schedule
/*!
  if (hdl->checkAndClearRunDepositComboSchedule())
  hdl->deposit_combo_schedule()
*/
void CGTopLevelSchedule::codeDepositComboSchedule(LangCppScope* scope)
{
  SCHCombinational* forceDepositSched = mSched->getForceDepositSchedule();
  if (!forceDepositSched->empty()) {
    scope->addLineComment(CRYPT("Run the deposit/force update schedule if necessary"));
    LangCppExpr* actuals[] = NTARRAY(scope->findVariable(mDescrVarIdent));
    LangCppIf* ifMainScope = scope->addIf(scope->callFunction(CRYPT("carbonPrivateCheckAndClearRunDepositComboSchedule"), actuals));
    LangCppScope* ifScope = ifMainScope->getClauseScope();
    ifScope->addCall(ifScope->callFunction(mCodegen->getDepositComboScheduleName (), actuals));
  }
}

//! Code level triggers to scope
/*!
  This codes a conditional of the or of all the input nets in the
  SCHInputeNet set.
*/
LangCppScope* CGTopLevelSchedule::codeInputNetTriggerToScope(const SCHInputNets* inputNets, LangCppScope* cppScope, 
                                                             LangCppIf** ifScopeMain)
{
  LangCppScope* ret = NULL;
  CGScheduleTrigger* inputNetTrigger = createInputNetTrigger(inputNets);
  if (inputNetTrigger)
  {
    LangCppExpr* cond = mTriggerFactory.translateTriggerToCpp(inputNetTrigger, cppScope);
    LangCppIf* ifMain = cppScope->addIf(cond);
    if (ifScopeMain)
      *ifScopeMain = ifMain;
    ret = ifMain->getClauseScope();
  } 
  return ret;
}
  
//! Code edge trigger to scope
/*!
  This creates an or of all the edges of clocks in the given
  mask.
*/
LangCppScope* CGTopLevelSchedule::codeEdgeTriggerToScope(UInt32 scheduleCost, const SCHScheduleMask* mask, 
                                                         LangCppScope* cppScope)
{

  LangCppScope* ret = NULL;
  CGScheduleTrigger* schedTrig = createMaskTrigger(mask);
  if (schedTrig)
  {
    if (isCostEffective(schedTrig->getCost(), scheduleCost, cppScope))
    {
      LangCppExpr* cond = mTriggerFactory.translateTriggerToCpp(schedTrig, cppScope);
      LangCppIf* ifMain = cppScope->addIf(cond);
      ret = ifMain->getClauseScope();
    }
    else
      ret = cppScope;
  }
  return ret;
}

//! Returns true if the trigger cost is less than the schedule cost
/*!
  If it isn't cost effective, a comment is written to the scope
  saying that the call is unguarded.
*/
bool CGTopLevelSchedule::isCostEffective(UInt32 trigCost, UInt32 schedCost, LangCppScope* scope) const
{
  bool effective = true;
  if (trigCost > schedCost)
  {
    if (scope)
    {
      UtString comment(CRYPT("Unguarded (test cost="));
      comment << trigCost << CRYPT(", schedule cost=") << schedCost << ")";
      scope->addLineComment(comment.c_str());
    }
    effective = false;
  }
  return effective;
}
  
//! Setup the function with common parameters/variables
LangCppVariable* CGTopLevelSchedule::genStandardFunctionVars(LangCppFunction* dataFunc)
{
  LangCppClassType* modelDescrType = mCppGlobal.getClassType(CRYPT("carbon_model_descr"));
  LangCppType* carbChgTypeId = mCppGlobal.getType(CRYPT("CarbonChangeType"));
  LangCppClassType* carbSimType = mCppGlobal.getClassType(CRYPT("carbon_simulation"));

  // changed = carbonPrivateGetChanged (descr)
  LangCppVariable* descr = dataFunc->declareParameter(modelDescrType->pointer(), mDescrVarIdent);    
  LangCppVariable* changed = dataFunc->declareVariable(carbChgTypeId->pointer(), mChangedIdent);
  changed->addAttribute(LangCppVariable::eAttribUnused); // always gets emitted.

  LangCppExpr* actuals [] = NTARRAY(descr);
  dataFunc->addAssign(changed, dataFunc->callFunction ("carbonPrivateGetChanged", actuals));

  if (! mTopLevelVarIdent)
  {
    const StringAtom* topName = mCodegen->getTopName();    
    UtString topLevelVarName;
    UtOStringStream varNameCreator(&topLevelVarName);
    varNameCreator << noprefix(topName);
      
    mTopLevelVarIdent = mLangCppFactory.createIdent(topLevelVarName.c_str(), 4);
  }

  LangCppVariable* topObject = dataFunc->declareVariable(carbSimType->reference(), mTopLevelVarIdent);
  topObject->addAttribute(LangCppVariable::eAttribUnused); // always gets emitted.
  dataFunc->addDeclAssign(topObject, 
    dataFunc->staticCast(carbSimType->reference(), 
      dataFunc->getMember (descr, mHdlMemberIdent, mCppGlobal.getVoidType ()->pointer ())));
  return topObject;
}

//! Code the input/async schedule
void CGTopLevelSchedule::codeDataFunction(UtOStream& out)
{
  /*
    The input/async/deposit schedule call. This gets called by the
    carbon_<design>_clocks function with inputFlow. With noInputFlow
    (shell) this gets called on the second (and not-optional) call to
    carbonSchedule for a timeslice.
  */
  UtString functionName("carbon_");
  functionName << *(mCodegen->getIfaceTag()) << CRYPT("_data");

  LangCppFileScope* fileScope = mCppStmtTree;
  LangCppFunction* dataFunc = fileScope->addFunction(functionName.c_str(), LangCppFunction::eStatic, mCppGlobal.getVoidType());
  genStandardFunctionVars(dataFunc);

  mCodegen->genDataScheduleCalls(dataFunc, this);  

  //  dataFunc->addAttribute(LangCppFunction::eAttribInline);
  RegisterAllocator regAlloc;
  regAlloc.visitStmt(dataFunc);
  regAlloc.allocate();
  dataFunc->emit(out, 0);
}

//! hdl->finalizeScheduleCall();
void CGTopLevelSchedule::codeFinalizeScheduleCall(LangCppScope* postFunc, LangCppVariable *descr)
{
  /*
    all this for
    if (hdl->getCarbonStatus() != eCarbon_ERROR)
    hdl->finalizeScheduleCall();

    There is no != CarbonExpr, and I have to deal with idents or
    LangCppCarbonExpr when doing logical operations, so a little
    dancing around has to be done.

    We end up with:
    CarbonStatus carbonStat = hdl->getCarbonStatus();
    SInt32 isCarbonError = (carbonStat == eCarbon_ERROR);
    if (isCarbonError == 0)
    hdl->finalizeScheduleCall();
  */
  LangCppType* carbonStatusType = mCppGlobal.getType(CRYPT("CarbonStatus"));
  LangCppVariable* carbonStat = mLangCppFactory.declareVariable(carbonStatusType, CRYPT("carbonStat"), postFunc);
  
  LangCppExpr* getStat = postFunc->callMethod(descr, CRYPT("getCarbonStatus"));
  postFunc->addDeclAssign(carbonStat, getStat);

  LangCppVariable* carbonError = mLangCppFactory.declareVariable(mCppGlobal.getSInt32Type(), "eCarbon_ERROR", postFunc);
  carbonError->addAttribute(LangCppVariable::eAttribNoDeclare);
  
  LangCppVariable* isCarbonError = mLangCppFactory.declareVariable(mCppGlobal.getSInt32Type(), CRYPT("isCarbonError"), postFunc);
  LangCppExpr* isCarbonErrorCond = mLangCppFactory.createBinaryOp(CarbonExpr::eBiEq, carbonStat, carbonError, postFunc);
  postFunc->addDeclAssign(isCarbonError, isCarbonErrorCond);
  
  LangCppExpr* cond = mLangCppFactory.createLogicalNot(isCarbonError, postFunc);
  LangCppIf* ifMain = postFunc->addIf(cond);
  LangCppScope* ifScope = ifMain->getClauseScope();
  LangCppExpr* actuals[] = NTARRAY(postFunc->findVariable(mDescrVarIdent));
  ifScope->addCall(ifScope->callFunction(CRYPT("carbonPrivateFinalizeScheduleCall"), actuals));
}

//! code the post schedule function (carbon_design_postsched)
void CGTopLevelSchedule::codePostSchedFunction(UtOStream& out)
{
  /*
    Post-schedule cleanup function. This always gets called in the
    inputFlow mode. In noInputFlow mode (in the shell), this gets
    called with the data scheduling (the second and not-optional call
    to carbonSchedule during a timeslice).
  */
  UtString functionName("carbon_");
  functionName << *(mCodegen->getIfaceTag()) << CRYPT("_postsched");
    
  LangCppFileScope* fileScope = mCppStmtTree;
  LangCppFunction* postFunc = fileScope->addFunction(functionName.c_str(), LangCppFunction::eStatic, mCppGlobal.getVoidType());
  genStandardFunctionVars(postFunc);
    
  mCodegen->codeClockGlitchDetection(postFunc, this);
  mCodegen->codeClearNets(postFunc, this);

  LangCppVariable* descr = postFunc->findVariable(mDescrVarIdent);
  // update the waveform
  codeFinalizeScheduleCall(postFunc, descr);
  // clear the change array
  LangCppType* carbChgArrayTypeId = mCppGlobal.getType(CRYPT("CarbonChangeArray"));
  LangCppExpr* actuals[] = NTARRAY(
    postFunc->getMember (descr, mChangeArrayMemberIdent, carbChgArrayTypeId));
  postFunc->addCall (postFunc->callFunction ("carbonPrivateReInitChangeArray", actuals));
  //  postFunc->addAttribute(LangCppFunction::eAttribInline);
  postFunc->emit(out, 0);
}

//! Code the clock function (derived/sequential/sample/transition) (carbon_design_clocks)
void CGTopLevelSchedule::codeClockFunction(UtOStream& out)
{
  /*
    Clocks schedule. With inputFlow this updates the inputs before
    running the clocks. With noInputFlow the inputs are not updated
    before the clocks are run. With noInputFlow this gets called on
    the first call to carbonSchedule.
  */
  UtString functionName("carbon_");
  functionName << *(mCodegen->getIfaceTag()) << CRYPT("_clocks");

  LangCppFileScope* fileScope = mCppStmtTree;
  LangCppFunction* clockFunc = fileScope->addFunction(functionName.c_str(), LangCppFunction::eStatic,
                                                      mCppGlobal.getVoidType());

  genStandardFunctionVars(clockFunc); 
    
  mUpdateInputsIdent = mLangCppFactory.createIdent(CRYPT("updateInputs"), 1);
  mTimeChangedIdent = mLangCppFactory.createIdent(CRYPT("timeChanged"), 1);
  clockFunc->declareParameter(mCppGlobal.getBoolType(), mUpdateInputsIdent);
  clockFunc->declareParameter(mCppGlobal.getBoolType(), mTimeChangedIdent);

  // dcl_init
  mDoInitIdent = mLangCppFactory.createIdent(CRYPT("do_init"), 1);
  LangCppVariable* doInit = clockFunc->declareVariable(mCppGlobal.getSInt8Type(), mDoInitIdent);
  doInit->addAttribute(LangCppVariable::eAttribUnused);
  LangCppVariable* descr = clockFunc->findVariable(mDescrVarIdent);
  LangCppExpr *actuals [] = NTARRAY (descr);
  clockFunc->addDeclAssign(doInit, 
    clockFunc->callFunction ("carbonPrivateGetChangeInit", actuals));
    
  // If there are DCL cycles, make a copy of the changed array for DCL
  // cycle processing.
  if (mSched->hasDCLCycles()) {
    declareDclCycleVars(clockFunc);
  }


  // Emit code to clear the clocks changed data  
  codeClearPrimaryClkChanges(clockFunc);

  mCodegen->codeClockFunction(clockFunc, this);
  //  clockFunc->addAttribute(LangCppFunction::eAttribInline);
  RegisterAllocator regAlloc;
  regAlloc.visitStmt(clockFunc);
  regAlloc.allocate();
  clockFunc->emit(out, 0);
}

//! Given an input set create a CGScheduleTrigger
CGScheduleTrigger* CGTopLevelSchedule::createInputNetTrigger(const SCHInputNets* inputNets)
{
  // Trigger associated with inputNets
  CGScheduleTrigger* assocTrigger = NULL;
    
  // InputNets are level triggers, so we only combine then with || and
  // no edge detection.
  InputNetTriggerMap::iterator q = mSchInputNetTriggers.find(inputNets);
  if (q == mSchInputNetTriggers.end())
  {
    CGScheduleTriggerVec triggers;
    for (SCHInputNetsCLoop iter(*inputNets); ! iter.atEnd(); ++iter)
    {
      const NUNetElab* elab = *iter;
      SInt32 chgArrayIndex = mChangedMap->getIndex(elab);
      if (chgArrayIndex != -1)
      {
        CarbonExpr* inputIdent = 
          mLangCppFactory.createIdentBitsel(mChangedIdent, chgArrayIndex, 1);
        CGScheduleTrigger* inputTrigger = mTriggerFactory.createLevelTrigger(inputIdent);
        triggers.push_back(inputTrigger);
      }
    }
      
    if (! triggers.empty())
    {
      if (triggers.size() == 1)
        assocTrigger = triggers.back();
      else
        assocTrigger = mTriggerFactory.createCompoundTrigger(triggers, CarbonExpr::eBiBitOr);
    }
    mSchInputNetTriggers[inputNets] = assocTrigger;
  }
  else
    assocTrigger = q->second;
    
  return assocTrigger;
}

//! Given a mask create a CGScheduleTrigger
/*!
  A mask trigger is an edge trigger. This combines the changed array
  value with the corresponding edge for the clock. We then combine
  those expressions with ||

  This keeps track of edges of clocks. If both edges of a clock
  appear in the mask, the edge trigger is transformed to a level
  trigger.

*/
CGScheduleTrigger* CGTopLevelSchedule::createMaskTrigger(const SCHScheduleMask* mask)
{
  CGScheduleTrigger* ret = NULL;
  MaskTriggerMap::iterator p = mMaskTriggers.find(mask);
  if (p == mMaskTriggers.end())
  {
    typedef UtArray<const SCHEvent*> SchedEventList;
    typedef Loop<SchedEventList> SchedEventListLoop;

    // List of clock events that need to be considered for the trigger
    // creation
    SchedEventList eventList;

    typedef UtHashMap<const STSymbolTableNode*, int> NodeToIntMap;
    // Map of clock node to number of edges we encounter
    NodeToIntMap clockCount;

    // Haven't seen this mask yet.
    // 1. Generate temporaries for each complex sub-test (ie. for clock events).
    for (SCHScheduleMask::SortedEvents loop = mask->loopEventsSorted();
         not loop.atEnd(); ++loop) {
      const SCHEvent * event = (*loop);
      if (event->isClockEvent()) {
        eventList.push_back(event);
          
        const STSymbolTableNode* clockNode = event->getClock();
        EventExprMap::iterator p = mEventTriggers.find(event);
        if (p == mEventTriggers.end())
        {

          // Create the edge expression here (changed[index] & edge)
          const NUNetElab* clock = SCHSchedule::clockFromName(clockNode);
          SInt32 index = mChangedMap->getIndex(clock);
          NU_ASSERT(index >= 0, clock);
          CarbonExpr* triggerIdent = 
            mLangCppFactory.createIdentBitsel(mChangedIdent, index, 1);
          CarbonExpr* triggerExpr = 
            mTriggerFactory.createEdgeExpr(triggerIdent, 
                                           event->getClockEdge());
          mEventTriggers[event] = triggerExpr;
        }
        ++(clockCount[clockNode]);
      } // if clock
    } // loop

    CGScheduleTriggerVec maskTriggers;
    for (SchedEventListLoop evLoop(eventList.begin(), eventList.end());
         !evLoop.atEnd(); ++evLoop)
    {
      const SCHEvent * event = *evLoop;
      const STSymbolTableNode* node = event->getClock();
      NodeToIntMap::iterator cp = clockCount.find(node);
      // If we have a double edge we remove the clock from
      // clockCount after creating a level expression.
      if (cp != clockCount.end())
      {
        int numEdges = cp->second;
        CGScheduleTrigger* maskTrigger = NULL;
        if (numEdges < 2)
        {
          // We only saw 1 edge of this clock so create the edge
          // trigger 
          CarbonExpr* triggerExpr = mEventTriggers[event];
          ST_ASSERT(triggerExpr, node);
          // Create a mask trigger and assign a cost of an edge
          maskTrigger = mTriggerFactory.createEdgeTrigger(triggerExpr);
        }
        else 
        {
          // We saw both edges of this clock so create a level trigger.
          const NUNetElab* clock = SCHSchedule::clockFromName(node);
          SInt32 index = mChangedMap->getIndex(clock);
            
          CarbonExpr* triggerIdent = 
            mLangCppFactory.createIdentBitsel(mChangedIdent, index, 1);
            
          maskTrigger = mTriggerFactory.createLevelTrigger(triggerIdent);
          // remove the node from the clockCount set so we don't see
          // it again.
          clockCount.erase(cp);
        }
        maskTriggers.push_back(maskTrigger);
      } // if node in clockCount
    } // evLoop
        
    if (! maskTriggers.empty())
    {
      if (maskTriggers.size() == 1)
      {
        ret = maskTriggers.back();
        mMaskTriggers[mask] = ret;
      }
      else
      {
        CGScheduleTrigger* compoundTrigger =
          mTriggerFactory.createCompoundTrigger(maskTriggers,
                                                CarbonExpr::eBiBitOr);
        mMaskTriggers[mask] = compoundTrigger;
        ret = compoundTrigger;
      }
    } // If any triggers in mask
  } // if mask not processed
  else
    ret = p->second;
  return ret;
}

//! Helper function to code the three top-level schedule functions.
/*!
  Some common stuff that needs to get done at the beginning of each
  top-level function.
  
  The top level functions we are dealing with here are
  carbon_design_schedule
  carbon_design_clkSchedule
  carbon_design_dataSchedule

  This is pretty messy but useful since we are creating much of the
  same stuff for each schedule.
*/
LangCppFunction* CGTopLevelSchedule::codeScheduleCommonPre(LangCppFileScope* fileScope, 
                                                           const UtString& functionName,
                                                           LangCppType* carbonStatType,
                                                           LangCppClassType* carbonClosureType,
                                                           LangCppClassType* /*hookupType*/,
                                                           CarbonIdent* closureIdent,
                                                           CarbonIdent* timeIdent,
                                                           CarbonIdent* /*modelHookupIdent*/,
                                                           int schedule_bucket)
{
  // Add the function to the scope
  LangCppFunction* schedFunc = 
    fileScope->addFunction(functionName.c_str(), 
                           LangCppFunction::eGlobal,
                           carbonStatType);
  
  // Add the CarbonModel parameter
  LangCppVariable* model = 
    schedFunc->declareParameter(carbonClosureType->pointer(), closureIdent);
    
  // Add the current time parameter
  schedFunc->declareParameter(mCppGlobal.getUInt64Type(), timeIdent);

  // These three functions all share the same top-level schedule bucket ID.
  mCodegen->startSampleSchedBucket(mCodegen->mGenProfile->getBucketName(schedule_bucket),
                                   schedFunc,
                                   mTopLevelSchedID);
    

  // call resetCarbonStatus()
  UtString buf;
  UtOStringStream bufStream(&buf);
  mCodegen->genSetCurrentModel(bufStream);
  schedFunc->addUserStatement(buf);
  schedFunc->addCall(schedFunc->callMethod(model, CRYPT("resetCarbonStatus")));

  LangCppClassType* carbSimType = mCppGlobal.getClassType(CRYPT("carbon_simulation"));        
  LangCppVariable* topObject = schedFunc->declareVariable(carbSimType->reference(), mTopLevelVarIdent);
  topObject->addAttribute(LangCppVariable::eAttribUnused); // always gets emitted.
  LangCppVariable* descr = schedFunc->findVariable(mDescrVarIdent);
  LangCppVariable* hdl = schedFunc->getMember (descr, mHdlMemberIdent, 
    mCppGlobal.getVoidType ()->pointer ());
  schedFunc->addDeclAssign(topObject, 
    schedFunc->staticCast(carbSimType->reference(),hdl));

  return schedFunc;
}

//! Helper function to create each of the top-level sched functions
/*!
  Stuff that needs to get done at the end of each top-level schedule
  functions.

  The top level functions we are dealing with here are
  carbon_design_schedule
  carbon_design_clkSchedule
  carbon_design_dataSchedule
*/
void CGTopLevelSchedule::codeScheduleCommonPost(LangCppScope* schedFunc,
  LangCppVariable *descr, UtOStream& out)
{
  UtString buf;
  UtOStringStream bufStream(&buf);

  // End profiling for schedule
  mCodegen->stopSampleSchedBucket(schedFunc, false); // false->don't pop

  // Compute the activity
  schedFunc->addIfDef(CRYPT("CARBON_ACTIVITY"));
  schedFunc->addCall(schedFunc->callMethod(descr, CRYPT("incrActivity")));
  schedFunc->addEndIf();

  // Restore the global current model to what it was when we were called.
  buf.clear();
  mCodegen->genRestoreCurrentModel(bufStream);
  schedFunc->addUserStatement(buf);
  schedFunc->addReturn(schedFunc->callMethod(descr, CRYPT("getCarbonStatus")));
  schedFunc->emit(out, 0);
}

// call top.setTime(time)
/*
  If detectChange is true this becomes
  bool timeChanged = top.setTime(time);
*/
void CGTopLevelSchedule::setSimulationTime(LangCppFunction* schedFunc, CarbonIdent* timeIdent, LangCppVariable** timeChanged)
{
  LangCppVariable* descr = schedFunc->findVariable(mDescrVarIdent);
  LangCppVariable* timeVar = schedFunc->findVariable(timeIdent);
  LangCppExpr* paramList[] = NTARRAY(timeVar);
  LangCppFuncCall* setTimeCall = schedFunc->callMethod(descr, CRYPT("setTime"), paramList);
  if (timeChanged)
  {
    *timeChanged = schedFunc->declareVariable(mCppGlobal.getBoolType(), mTimeChangedIdent);
    schedFunc->addDeclAssign(*timeChanged, setTimeCall);
  }
  else
    schedFunc->addCall(setTimeCall);
}

//! Create carbon_design_schedule()
void CGTopLevelSchedule::codeCarbon_Design_Schedule(LangCppFileScope* fileScope, 
                                                    LangCppType* carbonStatType,
                                                    LangCppClassType* carbonClosureType,
                                                    LangCppClassType* hookupType,
                                                    CarbonIdent* closureIdent,
                                                    CarbonIdent* timeIdent,
                                                    CarbonIdent* modelHookupIdent,
                                                    UtOStream& out)
{
  UtString functionName("carbon_");
  functionName << *(mCodegen->getIfaceTag()) << CRYPT("_schedule");

  UtString clockSchedName;
  clockSchedName << "carbon_" << *(mCodegen->getIfaceTag()) << CRYPT("_clocks");
  UtString postSchedName;
  postSchedName << "carbon_" << *(mCodegen->getIfaceTag()) << CRYPT("_postsched");

  UtString schedule_bucket_name;
  schedule_bucket_name << *(mCodegen->getIfaceTag());
  schedule_bucket_name.append(CRYPT("_schedule"));
  int schedule_bucket = mCodegen->mGenProfile->getBucketId(schedule_bucket_name);

  LangCppFunction* schedFunc = codeScheduleCommonPre(fileScope, functionName, carbonStatType, carbonClosureType, hookupType,  closureIdent, timeIdent, modelHookupIdent, schedule_bucket);
  
  LangCppVariable* descr = schedFunc->findVariable(mDescrVarIdent);
  
  LangCppVariable* timeChanged;
  // Apply any changes to simulation time    
  setSimulationTime(schedFunc, timeIdent, &timeChanged);


  // call carbon_design_clocks(descr, 1, timeChanged)
  // the '1' means call the input schedule
  LangCppExpr* one = mLangCppFactory.createOneExpr(4, schedFunc);
  LangCppExpr* clocksParam[] = NTARRAY(descr, one, timeChanged);
  LangCppFuncCall* clocksCall = 
    schedFunc->callFunction(clockSchedName.c_str(), clocksParam);
  schedFunc->addCall(clocksCall);
  
  // call carbon_design_postsched(descr)
  LangCppExpr* postParam[] = NTARRAY(descr);
  LangCppFuncCall* postCall = 
    schedFunc->callFunction(postSchedName.c_str(), postParam);
  schedFunc->addCall(postCall);
  codeScheduleCommonPost(schedFunc, descr, out);
}

//! Create carbon_design_clkSchedule()
void CGTopLevelSchedule::codeCarbon_Design_ClkSchedule(LangCppFileScope* fileScope, 
                                                       LangCppType* carbonStatType,
                                                       LangCppClassType* carbonClosureType,
                                                       LangCppClassType* hookupType,
                                                       CarbonIdent* closureIdent,
                                                       CarbonIdent* timeIdent,
                                                       CarbonIdent* modelHookupIdent,
                                                       UtOStream& out)
{
  UtString functionName("carbon_");
  functionName << *(mCodegen->getIfaceTag()) 
               << CRYPT("_clkSchedule");

  UtString clockSchedName;
  clockSchedName << "carbon_" << *(mCodegen->getIfaceTag()) << CRYPT("_clocks");
  UtString clkSchedule_bucket_name(*(mCodegen->getIfaceTag()));
  clkSchedule_bucket_name.append(CRYPT("_clkSchedule"));
  int schedule_bucket = mCodegen->mGenProfile->getBucketId(clkSchedule_bucket_name);

  LangCppFunction* schedFunc = codeScheduleCommonPre(fileScope, functionName, carbonStatType, carbonClosureType, hookupType,  closureIdent, timeIdent, modelHookupIdent, schedule_bucket);
  LangCppVariable* descr = schedFunc->findVariable(mDescrVarIdent);

  LangCppVariable* timeChanged;
  // Apply any changes to simulation time    
  setSimulationTime(schedFunc, timeIdent, &timeChanged);

  // call carbon_design_clocks(hdl, 0, timeChanged);
  // The '0' means do not call the input schedule
  LangCppExpr* zero = mLangCppFactory.createZeroExpr(4, schedFunc);
  LangCppExpr* clocksParam[] = NTARRAY(descr, zero, timeChanged);
  LangCppFuncCall* clocksCall = 
    schedFunc->callFunction(clockSchedName.c_str(), clocksParam);
  schedFunc->addCall(clocksCall);
  // Update the waveform.
  codeFinalizeScheduleCall(schedFunc, descr);
  codeScheduleCommonPost(schedFunc, descr, out);
}

//! Create carbon_design_dataSchedule()
void CGTopLevelSchedule::codeCarbon_Design_DataSchedule(LangCppFileScope* fileScope, 
                                                        LangCppType* carbonStatType,
                                                        LangCppClassType* carbonClosureType,
                                                        LangCppClassType* hookupType,
                                                        CarbonIdent* closureIdent,
                                                        CarbonIdent* timeIdent,
                                                        CarbonIdent* modelHookupIdent,
                                                        UtOStream& out)
{
  UtString functionName("carbon_");
  functionName << *(mCodegen->getIfaceTag()) 
               << CRYPT("_dataSchedule");

  UtString postSchedName;
  postSchedName << "carbon_" << *(mCodegen->getIfaceTag()) << CRYPT("_postsched");
  UtString dataSchedName;
  dataSchedName<< "carbon_" << *(mCodegen->getIfaceTag()) << CRYPT("_data");
  UtString dataSchedule_bucket_name(*(mCodegen->getIfaceTag()));
  dataSchedule_bucket_name.append(CRYPT("_dataSchedule"));
  int schedule_bucket = mCodegen->mGenProfile->getBucketId(dataSchedule_bucket_name);

  LangCppFunction* schedFunc = codeScheduleCommonPre(fileScope, functionName, carbonStatType, carbonClosureType, hookupType,  closureIdent, timeIdent, modelHookupIdent, schedule_bucket);
  LangCppVariable* descr = schedFunc->findVariable(mDescrVarIdent);
  
  // Apply any changes to simulation time    
  setSimulationTime(schedFunc, timeIdent, NULL);

  // Add a call to the deposit schedule
  codeDepositComboSchedule(schedFunc);

  // call carbon_design_data(descr);
  LangCppExpr* dataParam[] = NTARRAY(descr);
  LangCppFuncCall* dataCall = schedFunc->callFunction(dataSchedName.c_str(),
                                                      dataParam);
  schedFunc->addCall(dataCall);
  
  // call carbon_design_postsched(descr)
  LangCppExpr* postParam[] = NTARRAY(descr);
  LangCppFuncCall* postCall = schedFunc->callFunction(postSchedName.c_str(),
                                                      postParam);
  schedFunc->addCall(postCall);
  codeScheduleCommonPost(schedFunc, descr, out);
}

void
CGTopLevelSchedule::codeCarbon_Design_AsyncSchedule(LangCppFileScope* fileScope, 
                                                    LangCppType* carbonStatType,
                                                    LangCppClassType* carbonClosureType,
                                                    LangCppClassType* hookupType,
                                                    CarbonIdent* closureIdent,
                                                    CarbonIdent* timeIdent,
                                                    CarbonIdent* modelHookupIdent,
                                                    UtOStream& out)
{
  // Create the function name
  UtString functionName("carbon_");
  functionName << *(mCodegen->getIfaceTag()) 
               << CRYPT("_asyncSchedule");

  // Create a profiling bucket
  UtString asyncSchedule_bucket_name(*(mCodegen->getIfaceTag()));
  asyncSchedule_bucket_name.append(CRYPT("_asyncSchedule"));
  int schedule_bucket = 
    mCodegen->mGenProfile->getBucketId(asyncSchedule_bucket_name);

  // Emit code to initialize top callable schedule variables
  LangCppFunction* asyncFunc = codeScheduleCommonPre(fileScope, functionName,
                                                     carbonStatType,
                                                     carbonClosureType, hookupType,
                                                     closureIdent, timeIdent,
                                                     modelHookupIdent,
                                                     schedule_bucket);
  LangCppVariable* descr = asyncFunc->findVariable(mDescrVarIdent);

  // Apply any changes to simulation time    
  LangCppVariable* timeChanged;
  setSimulationTime(asyncFunc, timeIdent, &timeChanged);
  codeClearPrimaryClkChanges(asyncFunc);

  // Emit code to get the change array and the do_init variable
  LangCppType* carbChgTypeId = mCppGlobal.getType(CRYPT("CarbonChangeType"));
  LangCppVariable* changed = asyncFunc->declareVariable(carbChgTypeId->pointer(), mChangedIdent);
  changed->addAttribute(LangCppVariable::eAttribUnused); // always gets emitted.
  LangCppExpr* actuals [] = NTARRAY(descr);
  asyncFunc->addAssign(changed, asyncFunc->callFunction ("carbonPrivateGetChanged", actuals));
  LangCppVariable* doInit = asyncFunc->declareVariable(mCppGlobal.getSInt8Type(), mDoInitIdent);
  doInit->addAttribute(LangCppVariable::eAttribUnused);
  asyncFunc->addDeclAssign(doInit, 
    asyncFunc->callFunction ("carbonPrivateGetChangeInit", actuals));
    
  // Initialize old values for all computed clocks
  asyncFunc->addLineComment(CRYPT("Initialize any flow through clocks"));
  mCodegen->codeOldClockInit(asyncFunc, this, mSched->loopAsyncClocks());

  // Emit the calls to the async schedules
  mCodegen->genCombScheduleCalls(asyncFunc, this, eCombAsync, "Async",
                                 "asynchronous");

  // Compute the derived clocks
  asyncFunc->addLineComment(CRYPT("Compute change flag for flow through clocks"));
  for (SCHClocksLoop c = mSched->loopAsyncClocks(); !c.atEnd(); ++c) {
    const NUNetElab* clkElab = *c;
    mCodegen->codeDerivedChanged(clkElab, "", "", true, asyncFunc, this);
  }

  // Do the post schedule calls for the async schedule. Note that we
  // don't do the glitch detection or reset the changed array. This
  // must be done as part of a clock or normal schedule for the flops
  // to behave correctly. See the spec for adding flow through paths
  // to MaxSim for details.
  codeFinalizeScheduleCall(asyncFunc, descr);
  codeScheduleCommonPost(asyncFunc, descr, out);
}


//! This generates the 3 top-level functions
/*!
  Creates
  carbon_design_schedule()
  carbon_design_clkSchedule()
  carbon_design_dataSchedule()
*/
void CGTopLevelSchedule::codeScheduleFunction(UtOStream& out)
{
  // Generate top-level scheduler
  //
  LangCppFileScope* fileScope = mCppStmtTree;

  LangCppType* carbonStatType = mCppGlobal.getType(CRYPT("CarbonStatus"));

  //CarbonIdent* modelIdent = mLangCppFactory.createIdent(CRYPT("model"), 4);
  CarbonIdent* closureIdent = mLangCppFactory.createIdent(CRYPT("descr"), 4);
  CarbonIdent* timeIdent = mLangCppFactory.createIdent(CRYPT("time"), 4);
  CarbonIdent* modelHookupIdent = mLangCppFactory.createIdent(CRYPT("modelHookup"), 4);

  //LangCppClassType* carbonModelType = 
  //  mCppGlobal.declareClassType(CRYPT("CarbonModel"));
  LangCppClassType* carbonClosureType = 
    mCppGlobal.declareClassType(CRYPT("carbon_model_descr"));
  LangCppClassType* hookupType = 
    mCppGlobal.declareClassType(CRYPT("CarbonHookup"));

  codeCarbon_Design_Schedule(fileScope, carbonStatType, carbonClosureType, hookupType, closureIdent, timeIdent, modelHookupIdent, out);
  codeCarbon_Design_ClkSchedule(fileScope, carbonStatType, carbonClosureType, hookupType, closureIdent, timeIdent, modelHookupIdent, out);
  codeCarbon_Design_DataSchedule(fileScope, carbonStatType, carbonClosureType, hookupType, closureIdent, timeIdent, modelHookupIdent, out);
  codeCarbon_Design_AsyncSchedule(fileScope, carbonStatType, carbonClosureType, hookupType, closureIdent, timeIdent, modelHookupIdent, out);
}
    
/*!
 * evaluate list of elaborated nets, assign to slots in changed[] if
 * they're suitable (and not already assigned a slot).
 * \arg l - a loop<T>
 */
void CGTopLevelSchedule::evalPI(SCHInputNetsCLoop l)
{
  // Every net on the list is sensitive
  for(;!l.atEnd (); ++l)
  {
    const NUNetElab *qualifiedNet = *l;
    // Enter it into the map and reserve an entry in the changed[] array
    //
    mChangedMap->insert(qualifiedNet); 
  }
}

void CGTopLevelSchedule::calcDerivedClocks()
{
  for (CodeGen::ClockIter cm = mCodegen->mClockTable.begin (); 
       cm != mCodegen->mClockTable.end ();
       ++cm) {
    if (cm->second == CodeGen::eDerivedClock) {
      ++mDerivedClockCount;
    }
  }
}

void CGTopLevelSchedule::calcPrimaryInputs()
{
  // Now, run through all the schedules and assign change array
  // indices to primary inputs
  //  Asynch sensitive PIs
  SCHSchedule::CombinationalLoop l;
  for (l = mSched->loopCombinational(eCombAsync); !l.atEnd(); ++l)
  {
    SCHCombinational* async = *l;
    evalPI (async->loopInputNets ());
  }

  // Input sensitive PIs
  for (l = mSched->loopCombinational(eCombInput); !l.atEnd(); ++l)
  {
    SCHCombinational* input = *l;
    evalPI (input->loopInputNets ());
  }
  
  // Synchronous sensitive PIs
  evalPI (mSched->loopClocks ());

  {
    UtHashSet<const SCHInputNets*> covered;
    for (SCHSchedulesLoop l = mSched->loopAllSimulationSchedules();
         !l.atEnd(); ++l) {
      SCHScheduleBase* schedBase = l.getScheduleBase();
      const SCHInputNets* branchNets = schedBase->getBranchNets();
      if ((branchNets != NULL) && covered.insertWithCheck(branchNets))
        evalPI (schedBase->loopBranchNets());
    }
      
  }
  
  // Now run thru ALL the parameters that aren't schedule sensitive and
  // assign them a -1 slot index value.
  // PIs without schedule sensitivities
  evalOtherPIs ();

  mNumExtParams = mChangedMap->size();
}


void CGTopLevelSchedule::evalOtherPIs()
{
  for(NUDesign::ModuleLoop m = mCodegen->getDesign()->loopTopLevelModules(); 
      !m.atEnd(); ++m)
  {
    NUModule *inst = *m;
    NUModuleElab *elab = inst->lookupElab (mCodegen->getCarbonContext()->getSymbolTable ());
    for (NUNetFilterLoop p = inst->loopInputPorts();
         !p.atEnd ();
         ++p)
    {
      const NUNetElab *qualifiedNet =
        (*p)->lookupElab (elab->getHier (), true);
      if ((qualifiedNet != NULL) &&
          (! mChangedMap->exists (qualifiedNet)))
        mChangedMap->insertNoCount(qualifiedNet);
            
    }
        
    for (NUNetFilterLoop p = inst->loopBidPorts ();
         !p.atEnd ();
         ++p)
    {
      const NUNetElab *qualifiedNet = (*p)->lookupElab (elab->getHier ());
      if (! mChangedMap->exists (qualifiedNet))
        mChangedMap->insertNoCount(qualifiedNet);
    }
  }
}
