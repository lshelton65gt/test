// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef FUNCLAYOUT_H_
#define FUNCLAYOUT_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUCost.h"

#include "codegen/CGContext_t.h"

class SCHSchedule;
class IODBNucleus;
class CodeGen;
class CGOFiles;

//! Class to layout functions according to schedule order.
/*!
 * This class emits functions in schedule call order to improved code locality
 * in the design.  Previously, functions were emitted in class order, which
 * is essentially random with respect to schedule call order.
 *
 * There are knobs which control the number of files and the size of the files
 * which are created:
 *  . Function Limit : the maximum number of functions which can be in one file.
 *  . Class Limit : the maximum number of classes which can be in one file.
 *  . Instruction Cost Limit:  the maximum total instruction cost of functions which
 *    can be in one file.
 * If any of the limits are 0, that limit is ignored.
 */
class FuncLayout
{
public:
  //! constructor
  FuncLayout(NUCostContext *costs, IODBNucleus *iodb, 
	     CGOFiles *cgOFiles, CodeGen *cg, UInt32 funcCostLimit) :

    mLimitFuncs(0),
    mLimitClasses(0),
    mLimitInstrCost(funcCostLimit),
    mFileCounter(0),
    mCosts(costs),
    mIODB(iodb),
    mCGOFiles(cgOFiles),
    mCodeGen(cg),
    mOrder (0)
  {
    resetFileInfo();
  }

  //! destructor
  ~FuncLayout();

  //! Emit the functions in schedule call order.
  void layoutFunctions(SCHSchedule *schedule);
  CGOFiles * getCGOFiles() { return mCGOFiles;}
  
  //! Return non-zero if we have already processed this node.  Value is relative order
  UInt32 haveSeen(const NUUseDefNode *node) const;

private:
  //! Hide copy and assign constructors.
  FuncLayout(const FuncLayout&);
  FuncLayout& operator=(const FuncLayout&);

  //! Reset the per-file counters/information.  Call after a file is emitted.
  void resetFileInfo();

  //! Add the given function to the current file.
  void addFuncToFile(NUUseDefNode *node);

  //! Return true if the given function, if added, would be within the current file limits.
  bool testAddFuncToFile(NUUseDefNode *node);

  //! Emit the given node, if the node is a cycle, loop through the nodes in the cycle.
  void emitHandleCycles(NUUseDefNode *node);

  //! "Emit" the function - add it to the current file, if it fits,
  //! or emit the current file and start a new one.
  void emitFunction(NUUseDefNode *node);

  //! Emit the current file, if there is anything to do.
  void emitFile();

  //! Remember the node as being seen
  void rememberSeen(NUUseDefNode *node);

  //! Maximum functions per file
  UInt32 mLimitFuncs;

  //! Maximum classes per file
  UInt32 mLimitClasses;

  //! Maximum total instruction cost per file
  UInt32 mLimitInstrCost;

  //! Current file number.
  UInt32 mFileCounter;

  //! Costing information
  NUCostContext *mCosts;

  //! IODB Nucleus
  IODBNucleus *mIODB;

  //! CGOFiles
  CGOFiles *mCGOFiles;

  //! the codegen
  CodeGen *mCodeGen;

  //! Total cost for the current file.
  /*!
   * Currently only need the instruction count, but using a NUCost to
   * accumulate in case we need to modify the constraints in the future.
   */
  NUCost mCurAccumCost;

  //! Ordered vector of functions to be emitted in the current file.
  NUUseDefVector mCurFuncs;

  //! Classes which are used in the current file.  Sorted for consistency, no other reason.
  typedef UtSet<NUModule*, NUModuleCmp> OrderedModuleSet;
  OrderedModuleSet mCurClasses;

  //! Set of nodes which have already been seen, and the order they should be in memory
  typedef UtMap<const NUUseDefNode*, UInt32> SeenOrder;
  SeenOrder mSeenOrder;


  //! sort order counter
  UInt32 mOrder;
};

/*!
 * Function layout has its own emitter because it does not emit on a per-class basis,
 * but instead emits across different classes.  So, before emit is called for a node,
 * class context needs to be established.
 */
struct SimpleListFuncLayout {
  CGContext_t ctxt;
  CodeGen *mCodeGen;
  CGOFiles *mCGOFiles;

  SimpleListFuncLayout(CGContext_t c, CodeGen *cg, CGOFiles *fio) : 
    ctxt (c | eCGFunctionLayout), 
    mCodeGen (cg),
    mCGOFiles (fio)
  {}
  void operator() (const NUUseDefNode *n) const;
};
    
template <typename T>  void emitCodeListFuncLayout (T* container,
                                                    CGContext_t context,
						    CodeGen *cg,
						    CGOFiles *fio)
{
  std::for_each (container->begin(), container->end(), SimpleListFuncLayout (context, cg, fio));
}


#endif
