// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __CGTriggerFactory_h_
#define __CGTriggerFactory_h_

#include "exprsynth/ExprFactory.h"
#include "exprsynth/Expr.h"
#include "schedule/ScheduleTypes.h"
#include "util/UtString.h"
#include "util/AtomicCache.h"
#include "symtab/STSymbolTable.h"
#include "hdl/HdlVerilogPath.h"
#include "langcpp/LangCppCarbonGlobal.h"

class CGScheduleTrigger;

typedef UtArray<CGScheduleTrigger*> CGScheduleTriggerVec;
typedef Loop<CGScheduleTriggerVec> CGScheduleTriggerVecLoop;
typedef CLoop<CGScheduleTriggerVec> CGScheduleTriggerVecCLoop;

class CGSingleTrigger;
class CGCompoundTrigger;

class LangCppFactory;
class LangCppCarbonGlobal;
class LangCppExpr;
class LangCppVariable;
class LangCppScope;

//! Trigger class for calling schedules
/*!
  There are two very basic kinds of triggers. Level and edge. A level
  trigger is a simple 'if (changed)'. An edge trigger is a simple 'if
  (changed & CARBON_CHANGED_RISE_MASK)', e.g., The 'if' is not part of
  the trigger. The trigger is the condition itself. 
  A CGSingleTrigger is one of these basic types. A Compound trigger is
  a binary combination of single triggers.
 */
class CGScheduleTrigger
{
public:
  virtual ~CGScheduleTrigger();

  //! equality operator
  bool operator==(const CGScheduleTrigger& other) const;

  //! Cast to a single trigger. NULL if it isn't
  virtual const CGSingleTrigger* castSingleTrigger() const;
  CGSingleTrigger* castSingleTrigger()
  {
    const CGScheduleTrigger* me = const_cast<const CGScheduleTrigger*>(this);
    return const_cast<CGSingleTrigger*>(me->castSingleTrigger());
  }

  //! Cast to a compound trigger. NULL if it isn't
  virtual const CGCompoundTrigger* castCompoundTrigger() const;
  CGCompoundTrigger* castCompoundTrigger()
  {
    const CGScheduleTrigger* me = const_cast<const CGScheduleTrigger*>(this);
    return const_cast<CGCompoundTrigger*>(me->castCompoundTrigger());
  }

  //! hash function
  virtual size_t hash() const = 0;
  //! Compare for sorting
  virtual ptrdiff_t compare(const CGScheduleTrigger* other) const = 0;
  
  //! Get the cost of this trigger.
  /*!
    This could be expensive if this a compound trigger.
  */
  virtual UInt32 getCost() const = 0;
};

//! Single level or edge trigger.
/*!
  The basic building block of a single trigger is a CarbonExpr.
*/
class CGSingleTrigger : public CGScheduleTrigger
{
public:
  CGSingleTrigger(CarbonExpr* expr, UInt32 cost);
  virtual ~CGSingleTrigger();

  virtual const CGSingleTrigger* castSingleTrigger() const;

  virtual UInt32 getCost() const;

  virtual size_t hash() const;
  virtual ptrdiff_t compare(const CGScheduleTrigger* other) const;

  //! Get the underlying CarbonExpr.
  CarbonExpr* getExpr() const { return mExpr; }

private:
  CarbonExpr* mExpr;
  UInt32 mCost;
};

//! Compound trigger
class CGCompoundTrigger : public CGScheduleTrigger
{
public:
  CGCompoundTrigger(const CGScheduleTriggerVec& elems, 
                    CarbonExpr::ExprT binaryOp);
  virtual ~CGCompoundTrigger();

  virtual const CGCompoundTrigger* castCompoundTrigger() const;

  virtual UInt32 getCost() const;

  virtual size_t hash() const;
  virtual ptrdiff_t compare(const CGScheduleTrigger* other) const;
  
  //! Loop the single triggers within this compound.
  CGScheduleTriggerVecCLoop loopTriggers() const;

  //! Get the binary operator for this compound.
  CarbonExpr::ExprT getBinOp() const { return mBinaryOp; }

private:
  CGScheduleTriggerVec mScheduleTriggers;
  CarbonExpr::ExprT mBinaryOp;
};

//! Trigger factory.
/*!
  This keeps a set of all the triggers based on their underlying
  CarbonExprs.

  Unfortunately, during initial coding, this also became the variable
  manager for CodeSchedule as well as helper layer over the
  LangCppStmtTree. That will change. A new class will be created that
  will help with the creation and use of the LangCppStmtTree.
*/
class CGSchedTriggerFactory
{
public:
  CGSchedTriggerFactory(LangCppFactory* cppFactory);
  ~CGSchedTriggerFactory();

  //! Cost of an edge trigger
  static const UInt32 cEdgeCost = 2;
  //! Cost of a level trigger
  static const UInt32 cLevelCost = 2;

  //! Reference to the Global context of the stmt tree.
  void putCppGlobal(LangCppCarbonGlobal* cppGlobal) { mCppGlobal = cppGlobal; }

  //! Create an edge expression
  CarbonExpr* createEdgeExpr(CarbonExpr* triggerIdent, ClockEdge edge);
  //! Create a glitch expression (like an edge expression)
  CarbonExpr* createGlitchExpr(CarbonExpr* triggerIdent);
  //! Create an edge trigger based on the expression
  CGScheduleTrigger* createEdgeTrigger(CarbonExpr* triggerExpr);
  //! Createa a level trigger based on the expression
  CGScheduleTrigger* createLevelTrigger(CarbonExpr* triggerExpr);

  //! Create a compound trigger 
  /*!
    The elements in the array are combined into one expression based
    on the binary operator passed in.
  */
  CGScheduleTrigger* createCompoundTrigger(const CGScheduleTriggerVec& elems, 
                                           CarbonExpr::ExprT binaryOp);

  //! Translate a trigger to a cpp scope
  LangCppCarbonExpr* translateTriggerToCpp(CGScheduleTrigger* schedTrig, LangCppScope* scope);


  //! Recalculate the schedule trigger with the given changeIdent
  /*!
    This allows us to always create triggers based on the 'changed'
    variable. When we need a trigger with 'dcl_changed', we can simply
    rebuild it with the dcl_changed ident.
  */
  CGScheduleTrigger* recalculateTrigger(CGScheduleTrigger* origTrig, CarbonIdent* changeIdent, CarbonIdent* origIdent);

  //! Generate an unique eventTmp variable in the given scope
  LangCppVariable* genEventTmp(LangCppScope* scope);
  
private:
  ESFactory* mExprFactory;
  LangCppFactory* mLangCppFactory;
  CarbonIdent* mRiseEdgeVar;
  CarbonIdent* mFallEdgeVar;
  CarbonIdent* mGlitchEdgeVar;
  LangCppCarbonGlobal* mCppGlobal;
  UInt32 mGenEventTmpCount;
  
  typedef UtHashSet<CGScheduleTrigger*, HashPointerValue<CGScheduleTrigger*> > ScheduleTriggerSet;
  ScheduleTriggerSet mScheduleTriggers;


  class IdentFinder;
  class TriggerTransform;
};

#endif
