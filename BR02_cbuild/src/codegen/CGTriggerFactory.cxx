// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUNet.h"
#include "CGTriggerFactory.h"
#include "langcpp/LangCppFactory.h"
#include "util/UtOrder.h"       // For carbonPtrCompare()

CGScheduleTrigger::~CGScheduleTrigger()
{}

const CGSingleTrigger* CGScheduleTrigger::castSingleTrigger() const
{
  return NULL;
}

const CGCompoundTrigger* CGScheduleTrigger::castCompoundTrigger() const
{
  return NULL;
}

bool CGScheduleTrigger::operator==(const CGScheduleTrigger& other) const
{
  return (compare(&other) == 0);
}

CGSingleTrigger::CGSingleTrigger(CarbonExpr* expr, UInt32 cost) :
  mExpr(expr), mCost(cost)
{}

CGSingleTrigger::~CGSingleTrigger()
{}

const CGSingleTrigger* CGSingleTrigger::castSingleTrigger() const
{
  return this;
}

UInt32 CGSingleTrigger::getCost() const
{
  return mCost;
}

size_t CGSingleTrigger::hash() const
{
  // only add the cost because we don't expect cost to be a large
  // number, probably 2 or 4. This uses the 16 zeroed bits of the
  // pointer more effectively. 
  return (size_t) mExpr + mCost;
}

ptrdiff_t CGSingleTrigger::compare(const CGScheduleTrigger* other) const
{
  const CGSingleTrigger* otherSingle = other->castSingleTrigger();
  if (! otherSingle)
    return carbonPtrCompare((CGSingleTrigger*)other, this);
  else
  {
    ptrdiff_t ret = otherSingle->mCost - mCost;
    if (ret == 0)
      ret = carbonPtrCompare(otherSingle->mExpr, mExpr);
    return ret;
  }
  return 0;
}

CGCompoundTrigger::CGCompoundTrigger(const CGScheduleTriggerVec& elems, 
                                     CarbonExpr::ExprT binaryOp)
  : mScheduleTriggers(elems), mBinaryOp(binaryOp)
{}

CGCompoundTrigger::~CGCompoundTrigger()
{}

const CGCompoundTrigger* CGCompoundTrigger::castCompoundTrigger() const
{
  return this;
}

UInt32 CGCompoundTrigger::getCost() const
{
  UInt32 cost = 0;
  for (CGScheduleTriggerVecCLoop l(mScheduleTriggers);
       ! l.atEnd(); ++l)
  {
    const CGScheduleTrigger* trig = *l;
    cost += trig->getCost();
  }
  return cost;
}


size_t CGCompoundTrigger::hash() const
{
  size_t ret = 0;
  for (CGScheduleTriggerVecCLoop p(mScheduleTriggers);
       ! p.atEnd(); ++p)
    ret ^= (size_t)(*p);
  return ret;
}

ptrdiff_t CGCompoundTrigger::compare(const CGScheduleTrigger* other) const
{
  const CGCompoundTrigger* otherCompound = other->castCompoundTrigger();
  if (! otherCompound)
    return (ptrdiff_t)other - (ptrdiff_t)this;
  else
  {
    ptrdiff_t ret = otherCompound->mBinaryOp - mBinaryOp; 
    if (ret == 0)
    {
      UInt32 otherSize = otherCompound->mScheduleTriggers.size();
      UInt32 mySize = mScheduleTriggers.size();
      if (otherSize != mySize)
        ret = mySize - otherSize;
      
      if (ret == 0)
      {
        // yuck. First run through both arrays and see if we are
        // exactly the same. If so return 0. At the point there is a
        // mismatch return -1 or 1 depending on whether other is less
        // than or greater than at that point.
        for (UInt32 i = 0; i < mySize; ++i)
        {
          if (mScheduleTriggers[i] != otherCompound->mScheduleTriggers[i])
            return carbonPtrCompare(otherCompound->mScheduleTriggers[i], mScheduleTriggers[i]);
        }
      }
    }
    return ret;
  }
  return 0;
}

CGScheduleTriggerVecCLoop CGCompoundTrigger::loopTriggers() const
{
  return CGScheduleTriggerVecCLoop(mScheduleTriggers);
}


class CGSchedTriggerFactory::TriggerTransform : public CopyWalker
{
public:
  TriggerTransform(CarbonIdent* replace, CarbonIdent* changeIdent, ESFactory* factory) : 
    CopyWalker(factory), mChangedIdent(changeIdent), mReplaceThis(replace)
  {}

  virtual ~TriggerTransform() {}

  virtual CarbonExpr* transformIdent(CarbonIdent* ident)
  {
    if (ident == mReplaceThis)
      return mChangedIdent;
    return ident;
  }

private:
  CarbonIdent* mChangedIdent;
  CarbonIdent* mReplaceThis;
};

CGSchedTriggerFactory::CGSchedTriggerFactory(LangCppFactory* cppFactory) :
  mLangCppFactory(cppFactory), mCppGlobal(NULL), mGenEventTmpCount(0)
{
  mExprFactory = mLangCppFactory->getExprFactory();
  mRiseEdgeVar = mLangCppFactory->createIdent("CARBON_CHANGE_RISE_MASK", 1);
  mFallEdgeVar = mLangCppFactory->createIdent("CARBON_CHANGE_FALL_MASK", 1);
  mGlitchEdgeVar = mLangCppFactory->createIdent("CARBON_CHANGE_GLITCH_MASK", 1);
}

CGSchedTriggerFactory::~CGSchedTriggerFactory()
{
  mScheduleTriggers.clearPointers();
}

CarbonExpr* 
CGSchedTriggerFactory::createEdgeExpr(CarbonExpr* triggerIdent, ClockEdge edge)
{
  CarbonExpr* edgeVar = mRiseEdgeVar;
  if (edge == eClockNegedge)
    edgeVar = mFallEdgeVar;
  
  CarbonExpr* edgeDetect = mExprFactory->createBinaryOp(CarbonExpr::eBiBitAnd, triggerIdent, edgeVar, 8, false, false);
  
  return edgeDetect;
}

CarbonExpr* 
CGSchedTriggerFactory::createGlitchExpr(CarbonExpr* triggerIdent)
{
  CarbonExpr* edgeDetect = mExprFactory->createBinaryOp(CarbonExpr::eBiBitAnd, triggerIdent, mGlitchEdgeVar, 8, false, false);
  
  return mExprFactory->createBinaryOp(CarbonExpr::eBiEq, edgeDetect, mGlitchEdgeVar, 8, false, false);
}

CGScheduleTrigger* CGSchedTriggerFactory::createEdgeTrigger(CarbonExpr* triggerExpr)
{
  CGSingleTrigger* edgeTrig = new CGSingleTrigger(triggerExpr, cEdgeCost);
  ScheduleTriggerSet::IterBoolPair pPair = mScheduleTriggers.insert(edgeTrig);
  if (! pPair.second)
  {
    delete edgeTrig;
    CGScheduleTrigger* found = *(pPair.first);
    edgeTrig = found->castSingleTrigger();
    INFO_ASSERT(edgeTrig, "Not a single trigger (edge).");
  }
  return edgeTrig;
}

CGScheduleTrigger* CGSchedTriggerFactory::createLevelTrigger(CarbonExpr* triggerExpr)
{
  CGSingleTrigger* levelTrig = new CGSingleTrigger(triggerExpr, cLevelCost);
  ScheduleTriggerSet::IterBoolPair pPair = mScheduleTriggers.insert(levelTrig);
  if (! pPair.second)
  {
    delete levelTrig;
    CGScheduleTrigger* found = *(pPair.first);
    levelTrig = found->castSingleTrigger();
    INFO_ASSERT(levelTrig, "Not a single trigger (level).");
  }
  return levelTrig;
}

CGScheduleTrigger* 
CGSchedTriggerFactory::createCompoundTrigger(const CGScheduleTriggerVec& elems,
                                             CarbonExpr::ExprT binaryOp)
{
  INFO_ASSERT(CarbonExpr::isBinaryOp(binaryOp), "Non-binary operator passed into trigger factory.");

  CGCompoundTrigger* compoundTrig = new CGCompoundTrigger(elems, binaryOp);
  ScheduleTriggerSet::IterBoolPair pPair = mScheduleTriggers.insert(compoundTrig);
  if (! pPair.second)
  {
    delete compoundTrig;
    CGScheduleTrigger* found = *(pPair.first);
    compoundTrig = found->castCompoundTrigger();
    INFO_ASSERT(compoundTrig, "Trigger not compound.");
  }
  return compoundTrig;
}

LangCppVariable* CGSchedTriggerFactory::genEventTmp(LangCppScope* scope)
{
  UtString name("eventTmp");
  name << mGenEventTmpCount;
  ++mGenEventTmpCount;
  CarbonIdent* varIdent = mLangCppFactory->createIdent(name.c_str(), 1);
  LangCppVariable* var = scope->declareVariable(mCppGlobal->getBoolType(), varIdent);
  //var->addAttribute(LangCppVariable::eAttribRegister);
  return var;
}

LangCppCarbonExpr* CGSchedTriggerFactory::translateTriggerToCpp(CGScheduleTrigger* schedTrig, LangCppScope* scope)
{
  INFO_ASSERT(mCppGlobal, "Cpp global not set.");
  LangCppCarbonExpr* ret = NULL;
  const CGSingleTrigger* single = schedTrig->castSingleTrigger();
  if (single)
  {
    CarbonExpr* singleExpr = single->getExpr();
    LangCppVariable* var = scope->getAlias(singleExpr);
    if (var == NULL)
    {
      var = genEventTmp(scope);
      mLangCppFactory->addAssignAlias(var, singleExpr, scope);
    }
    ret = var;
  }
  else
  {
    LangCppExprArray exprList;
    const CGCompoundTrigger* compound = schedTrig->castCompoundTrigger();
    INFO_ASSERT(compound, "Trigger is neither compound nor single.");
    CarbonExpr::ExprT binOp = compound->getBinOp();
    for (CGScheduleTriggerVecCLoop p = compound->loopTriggers(); 
         !p.atEnd(); ++p)
    {
      CGScheduleTrigger* trig = *p;
      LangCppExpr* expr = translateTriggerToCpp(trig, scope);
      exprList.push_back(expr);
    }

    LangCppExprArray::iterator q = exprList.begin();
    LangCppExpr* currentCppExpr = *q;
    ++q;
    CarbonExpr* currentExpr = currentCppExpr->getExpr();
    for (LangCppExprArray::iterator r = exprList.end();
         q != r; ++q)
    {
      LangCppExpr* nextCppExpr = *q;
      // Avoiding using the cpp factory here because we don't want to
      // walk the expressions in an n^2 fashion. Create one expression
      // and walk it once.
      CarbonExpr* nextExpr = nextCppExpr->getExpr();
      currentExpr = mExprFactory->createBinaryOp(binOp, currentExpr, nextExpr, 8, false, false);
    }

    ret = mLangCppFactory->createLangCppExpr(currentExpr, scope);
  }
  return ret;
}

CGScheduleTrigger* CGSchedTriggerFactory::recalculateTrigger(CGScheduleTrigger* origTrig, 
                                                             CarbonIdent* changeIdent,
                                                             CarbonIdent* origIdent)
{
  CGScheduleTrigger* ret = NULL;
  const CGSingleTrigger* single = origTrig->castSingleTrigger();
  if (single)
  {
    CarbonExpr* singleExpr = single->getExpr();
    TriggerTransform xform(origIdent, changeIdent, mExprFactory);
    xform.visitExpr(singleExpr);
    CarbonExpr* result = xform.getResult();
    
    CGSingleTrigger* trig = new CGSingleTrigger(result, single->getCost());
    ScheduleTriggerSet::IterBoolPair pPair = mScheduleTriggers.insert(trig);
    if (! pPair.second)
    {
      delete trig;
      ret = *(pPair.first);
    }
    else
      ret = trig;
  }
  else
  {
    CGScheduleTriggerVec trigList;
    const CGCompoundTrigger* compound = origTrig->castCompoundTrigger();
    INFO_ASSERT(compound, "Trigger is neither compound nor single.");
    for (CGScheduleTriggerVecCLoop p = compound->loopTriggers(); 
         !p.atEnd(); ++p)
    {
      CGScheduleTrigger* trig = *p;
      CGScheduleTrigger* recalced = recalculateTrigger(trig, changeIdent, origIdent);
      trigList.push_back(recalced);
    }
    
    ret = createCompoundTrigger(trigList, compound->getBinOp());
  }
  return ret;
}
