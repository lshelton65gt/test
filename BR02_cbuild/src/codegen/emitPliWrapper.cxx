// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "emitUtil.h"
#include "PrimaryPort.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModelPort.h"
#include "nucleus/NUCModelArgConnection.h"
#include "nucleus/NUCModelFn.h"
#include "nucleus/NUCModel.h"
#include "util/StringAtom.h"
#include "compiler_driver/CarbonContext.h"

#define TFCELL_H_EXT "pli_tfcell.h"

using namespace emitUtil;

typedef UtVector<CGPrimaryPort*> CGPrimaryPorts;

static void emitParameterTypeCheck(UtOStream& out, CGPrimaryPort* port, int parm, bool isBidirect)
{
  if (port->isInput() || (port->isBid() && !isBidirect))
    out << "  ok = ok && CarbonPliPort::checkPliType(" << parm << ", true, false);" << ENDL;
  else if (port->isOutput())
    out << "  ok = ok && CarbonPliPort::checkPliType(" << parm << ", false, true);" << ENDL;
  else if (port->isBid())
  {
    // Inout ports are represented by two parameters.
    // Generate a read check for the given parameter, and a write check for the next
    out << "  ok = ok && CarbonPliPort::checkPliType(" << parm << ", true, false);" << ENDL;
    out << "  ok = ok && CarbonPliPort::checkPliType(" << parm + 1 << ", false, true);" << ENDL;
  }
}

static void emitModelTfcell(UtOStream& out, UtString& iFace)
{
  out <<
    ENDL
    "  {" << ENDL
    "    usertask,\n"
    "    0,\n"
    "    carbon_" << iFace << "_instance_checktf,\n"
    "    0,\n"
    "    0,\n"
    "    carbon_" << iFace << "_instance_misctf,\n"
    "    \"$carbon_" << iFace << "_instance\",\n"
    "    0,\n"
    "    0,0,0,0,0,0,0\n"
    "  },\n";
}

static void emitCarbonTfcellArray(UtOStream& out, UtString& iFace, const UtString *targetName)
{
  UtString include;
  // can't use CarbonContext::getFileRoot() here because tat returns an absolute
  // path, and we need just the file name w/ no path.
  include << "#include \"" << targetName->c_str() << "." << TFCELL_H_EXT "\"\n"; 

  out <<
    ENDL
    "#define PROTO_PARAMS(params) params\n"
    "#define EXTERN extern\n"
    "#define PLI_EXTRAS\n"
    "#include \"carbon/veriuser.h\"\n"
    "#undef PROTO_PARAMS\n"
    "#undef EXTERN\n"
    "#include \"carbon/carbon_udtf.h\"\n"
    "\n"
    "#define GENERIC_PLI 1\n"
    "\n"
    "extern int carbon_" << iFace << "_instance_checktf();\n"  
    "extern int carbon_" << iFace << "_instance_misctf();\n"  
    "\n"
    "/*\n"
    " * Aldec Riviera PLI interface\n"
    " */\n"
    "#ifdef ALDEC_PLI\n"
    "#undef GENERIC_PLI\n"
    "#include \"aldecpli.h\"\n"
    "s_tfcell veriusertfs[] =\n"
    "{\n"
    "#include \"carbon/carbon_tfcell.h\"\n"
          << include << 
    "  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }" << ENDL
    "};\n"
    "#endif /* ALDEC_PLI */\n"
    "\n"
    "\n"
    "/*\n"
    " * Cadence Verilog-XL and NC-Verilog interface\n"
    " */\n"
    "#ifdef CADENCE_PLI\n"
    "#undef GENERIC_PLI\n"
    "#include \"vxl_veriuser.h\"\n"
    "p_tfcell carbon_bootstrap()\n"
    "{\n"
    "  static s_tfcell carbon_tfs[] =\n"
    "  {\n"
    "#include \"carbon/carbon_tfcell.h\"\n"
          << include <<
    "    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }" << ENDL
    "  };\n"
    "  return carbon_tfs;\n"
    "}\n"
    "#endif /* CADENCE_PLI */\n"
    "\n"
    "\n"
    "/*\n"
    " * If no simulator-specific PLI interface has been selected, \n"
    " * assume a generic veriusertfs[] interface\n"
    " */\n"
    "#ifdef GENERIC_PLI\n"
    "#define usertask 1\n"
    "#define userfunction 2\n"
    "typedef struct t_tfcell\n"
    "{\n"
    "  PLI_INT16 type;\n"
    "  PLI_INT16 data;\n"
    "  PLI_INT32 (*checktf)();\n"
    "  PLI_INT32 (*sizetf)();\n"
    "  PLI_INT32 (*calltf)();\n"
    "  PLI_INT32 (*misctf)();\n"
    "  PLI_BYTE8 *tfname;\n"
    "  PLI_INT32 forwref;\n"
    "  PLI_BYTE8 *tfveritool;\n"
    "  PLI_BYTE8 *tferrmessage;\n"
    "  PLI_INT32 hash;\n"
    "  struct t_tfcell *left_p;\n"
    "  struct t_tfcell *right_p;\n"
    "  PLI_BYTE8 *namecell_p;\n"
    "  PLI_INT32 warning_printed;\n"
    "} s_tfcell, *p_tfcell;\n"
    "s_tfcell veriusertfs[] =\n"
    " {\n"
    "#include \"carbon/carbon_tfcell.h\"\n"
          << include << 
    "  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }" << ENDL
    "};\n"
    "#endif /* GENERIC_PLI */\n";
}

static void emitVcsTabFile(UtOStream& out, UtString& iFace)
// Note that out is not a CodeStream so no annotations are allowed in this function
{
  // start with the common system tasks that are shared by all models
  out  << 
    "$carbon_deposit check=carbon_deposit_checktf call=carbon_deposit_calltf\n"
    "$carbon_examine size=32 check=carbon_examine_checktf call=carbon_examine_calltf\n"
    "$carbon_force check=carbon_force_checktf call=carbon_force_calltf\n"
    "$carbon_force_range check=carbon_force_range_checktf call=carbon_force_range_calltf\n"
    "$carbon_release check=carbon_release_checktf call=carbon_release_calltf\n"
    "$carbon_release_range check=carbon_release_range_checktf call=carbon_release_range_calltf\n"
    "$carbon_readmemh data=1 check=carbon_readmem_checktf call=carbon_readmem_calltf\n"
    "$carbon_readmemb data=0 check=carbon_readmem_checktf call=carbon_readmem_calltf\n"
    "$carbon_dumpfile check=carbon_dumpfile_checktf call=carbon_dumpfile_calltf\n"
    "$carbon_dumpfile_fsdb_auto_switch check=carbon_dumpfile_fsdb_auto_switch_checktf call=carbon_dumpfile_fsdb_auto_switch_calltf\n"
    "$carbon_dumpvars check=carbon_dumpvars_checktf call=carbon_dumpvars_calltf\n"
    "$carbon_dumpon data=1 check=carbon_dump_control_checktf call=carbon_dump_control_calltf\n"
    "$carbon_dumpoff data=0 check=carbon_dump_control_checktf call=carbon_dump_control_calltf\n"
    "$carbon_deposit_memory check=carbon_deposit_memory_checktf call=carbon_deposit_memory_calltf\n"
    "$carbon_examine_memory size=32 check=carbon_examine_memory_checktf call=carbon_examine_memory_calltf\n"
    "\n\n";

  // now the model-specific task
  out <<
    "$carbon_" << iFace << "_instance"
    " check=carbon_" << iFace << "_instance_checktf"
    " misc=carbon_" << iFace << "_instance_misctf\n";
}

static void sEmitCallback(UtOStream& out, const char *prefix, UtString& iFace, const char *suffix)
{
  
  out << 
    "#if CARBON_PLI_WRAPPER_CALLBACKS" << ENDL
      << prefix << "carbon_pli_wrapper_" << iFace << "_" << suffix << 
    "#endif\n";
}

/*
 * Sometimes NUNet::isBid() will return true even though 
 * we are not treating the net as a bidirect.
 * We need to also check CodeGen::isBidirect() to be sure
 * we have a 'real' bidirect.
 */
static bool sIsBidirect(CGPrimaryPort* port)
{
  return port->isBid() && port->isCodeGenBidirect();
}

static void sEmitUpdateOutputs(UtOStream& out, CGPrimaryPorts& ports)
{
  // For each output, fetch the value and set the output parameter
  int i = 0;
  for (Loop<CGPrimaryPorts> l(ports); !l.atEnd(); ++l, ++i) {
    CGPrimaryPort* port = *l;
    if (port->isOutput() || port->isBid())
    {
      if (sIsBidirect(port))
      {
        ++i;  // for bidirects, write to second parameter for the port
        /*
         * It is possible that we should call carbonResolveBidi() here, but I have 
         * not yet hit a test case that requires it, and I'm not sure it is harmless.
         */
        // out << "      carbonResolveBidi(data->getModel(), data->getPort(" << i << ")->getNet());\n";
      }
      out << "      data->getPort(" << i << ")->carbonToPli(descr);" << ENDL;
    }
  }
}

static UtString sNetName(CGPrimaryPort* port, CodeGen *codeGen)
{
  UtString name;
  if (port->isBid() && codeGen != NULL && port->isCodeGenBidirect())
    name << "pli_buf_";
  name << port->getNet()->getName()->str();
  return name;
}

/*
 * Computes the edgs sensitivity of the given net.
 *
 * Returns the name of a value from this enum:
 *   enum carbonPliEdge {
 *     ePliNoEdges,
 *     ePliPosedge,
 *     ePliNegedge,
 *     ePliBothEdges
 *   };
 *
 * Currently all clocks and asyncs get ePliBothEdges, and data signals
 * get ePliNoEdges.
 *
 * The next level of optimization is to check which edge to use for a
 * given clock or async.
 */
static const char *sPliEdge(CNetSet &bothEdgeClks,
                            CNetSet &clksUsedAsData,
                            ClkToEvent &clkEdgeUses,
                            const CGPrimaryPort* port)
{
  (void) clksUsedAsData;  // not currently used
  (void) bothEdgeClks;    // not currently used

  // default to schedule on both edges
  const char *pliEdge = "ePliBothEdges";

  // If net is an input, see if it can cause a schedule run.
  if (port->isInput())
  {
    if (clkEdgeUses.find(port->getNet()) == clkEdgeUses.end() &&
        bothEdgeClks.find(port->getNet()) == bothEdgeClks.end())
      // not sensitive to any edges - do not call schedule if it changes.
      pliEdge = "ePliNoEdges";
  } 

  return pliEdge;
}

void
CodeGen::codePliInterface(NUDesign* design,
  CNetSet &bothEdgeClks,
  PortNetSet &validOutPorts,
  PortNetSet &validInPorts,
  PortNetSet &validTriOutPorts,
  PortNetSet &validBidiPorts,
  CNetSet &clksUsedAsData,
  ClkToEvent &clkEdgeUses )
{
  // Put the pli interface into the build dir to be compiled into libdesign.a
  chdirToBuildDir();
  
  //### Add a use of these vars to eliminate warnings for now.  FIXME
  if( 0 ) {
    bool bollocks;
    bollocks = &bothEdgeClks == 0;  
    bollocks = &validOutPorts == 0;
    bollocks = &validInPorts == 0;
    bollocks = &validTriOutPorts == 0;
    bollocks = &validBidiPorts == 0;
    bollocks = &clksUsedAsData == 0;
    bollocks = &clksUsedAsData == 0;
    bollocks = &clkEdgeUses == 0;
    if(bollocks)
      design = NULL;
  }
  
  NUModule* module = NULL;
  for (NUDesign::ModuleLoop l = design->loopTopLevelModules();
       !l.atEnd(); ++l)
  {
    NU_ASSERT(module == NULL, module);
    module = *l;
  }

  const char * moduleName = module->getName()->str();

  // Gather the primary ports for processing. Can't use the top module
  // nets because of port splitting.
  IODBNucleus* iodb = getIODB();
  CGPrimaryPorts primaryPorts;
  for (IODB::NameVecLoop l = iodb->loopPrimaryPorts(); !l.atEnd(); ++l) {
    STSymbolTableNode* node = *l;
    CGPrimaryPort* primaryPort = new CGPrimaryPort(iodb, this, node);
    primaryPorts.push_back(primaryPort);
  }

  // obscure our includes by writing them to a non-cleartext file
  UtOStream* hout = CGFileOpen("pli_wrapper.h", "/* includes for pli_wrapper.cxx */",
			       gCodeGen->isObfuscating());  
  *hout <<
    ENDL << "\n"
    "extern \"C\" {\n"
    "\n"
    "#define PROTO_PARAMS(params) params\n"
    "#define EXTERN extern \"C\"\n"
    "#define PLI_EXTRAS\n"
    "#include \"carbon/veriuser.h\"\n"
    "#undef PROTO_PARAMS\n"
    "#undef EXTERN\n"
    "}\n"
    "\n"
    "#include \"pli-wrapper/CarbonPliWrapper.h\"\n"
    "\n"
    "extern \"C\" CarbonObjectID* carbon_" << mIfaceTag 
        <<"_create(CarbonDBType dbType, CarbonInitFlags flags);\n";
  delete hout;

  // Open a file in build dir named cmodel_wrapper.cxx to hold the wrapper code
  UtOStream* cout = CGFileOpen("pli_wrapper.cxx", "", gCodeGen->isObfuscating(),
			       true /* needCleartext */);  
  if (not cout)
    return;                     // already error'ed

  // Emit header comment
  *cout << "/* Verilog PLI wrapper for Carbon model " << moduleName << " */" << ENDL << "\n";

  // #include the obfuscated definitions
  *cout << "#include \"pli_wrapper.h\"" << ENDL << "\n";

  // provide default definition for callback enable macro
  *cout << 
    "#ifndef CARBON_PLI_WRAPPER_CALLBACKS" << ENDL
    "#define CARBON_PLI_WRAPPER_CALLBACKS 0\n"
    "#endif\n\n";


  // declare extern defs for the PLI wrapper callbacks
  *cout << 
    "#if CARBON_PLI_WRAPPER_CALLBACKS" << ENDL
    "  extern \"C\" void * carbon_pli_wrapper_" << mIfaceTag << "_create(CarbonObjectID *descr);\n"
    "  extern \"C\" void carbon_pli_wrapper_" << mIfaceTag << "_pre_schedule(CarbonObjectID *descr, void *data, CarbonTime t);\n"
    "  extern \"C\" void carbon_pli_wrapper_" << mIfaceTag << "_post_schedule(CarbonObjectID *descr, void *data, CarbonTime t);\n"
    "  extern \"C\" void carbon_pli_wrapper_" << mIfaceTag << "_destroy(CarbonObjectID *descr, void *data);\n"
    "#endif\n\n";

  // provide default definition for waveform correctness macro
  *cout << 
    "#ifndef CARBON_SCHED_ALL_CLKEDGES" << ENDL
    "#define CARBON_SCHED_ALL_CLKEDGES 0\n"
    "#endif\n\n";

  /* 
   * Output the checktf() function: check the incoming parameters.
   * There should be one for each port.
   */
  *cout << 
    "extern \"C\" int carbon_" << mIfaceTag << "_instance_checktf(int user_data, int reason)" << ENDL
    "{\n"
    "  (void) user_data;\n"
    "  (void) reason;\n"
    "  bool ok = true;\n";

  int i = 1;
  for (Loop<CGPrimaryPorts> l(primaryPorts); !l.atEnd(); ++l, ++i) {
    CGPrimaryPort* port = *l;
    emitParameterTypeCheck(*cout, port, i, sIsBidirect(port));
    if (sIsBidirect(port))
      ++i; // inout ports are represented by two PLI parameters.
  }
  
  *cout << 
    "  if (!ok)" << ENDL
    "    tf_dofinish();\n"
    "  return 0;\n"
    "}\n\n";

  /* 
   * Output misctf() function: pass data values to/from Carbon model.
   */
  *cout <<
    "extern \"C\" int carbon_" << mIfaceTag << "_instance_misctf(int user_data, int reason, int paramvc)" << ENDL
    "{\n"
    "  (void) user_data;\n"
    "  (void) paramvc;\n"
    "\n"
    "  switch(reason)\n"
    "  {\n"
    "    case REASON_ENDOFCOMPILE:\n"
    "    {\n"
    "      // allocate a model instance and get handles to the ports\n"
    "      CarbonObjectID *descr = carbon_" << mIfaceTag << "_create(eCarbonFullDB, eCarbon_NoFlags);\n"
    "\n"
    "      if (descr != NULL)\n"
    "      {\n"
    "        CarbonPliModel* data = \n"
    "          new CarbonPliModel((const char *) tf_mipname(), \"" << moduleName << "\",\n"
    "                                      descr, tf_nump(), (CarbonTimescale) tf_gettimeunit());\n"
    "        CarbonPliPort *port;\n";

  // register each port handle with the instance data
  i = 0;
  for (Loop<CGPrimaryPorts> l(primaryPorts); !l.atEnd(); ++l, ++i) {
    CGPrimaryPort* port = *l;
    UtString cNet;
    StringUtil::escapeCString(port->getName(), &cNet);
    UtString depositable = (port->isInput() || sIsBidirect(port)) ? "true" : "false";
    *cout << "        port = new CarbonPliPort( descr, carbonFindNet(descr, \"" << 
             moduleName << "." << cNet << "\")," << i + 1 << ", " << depositable << ");" << ENDL;
    *cout << "        data->setPort(" << i << ", port );\n";
    *cout << "        port->setSchedTrigger(" << sPliEdge(bothEdgeClks, clksUsedAsData, clkEdgeUses, port) << ");\n";

    // if it is an inout, generate a second port object for writing values back to verilog
    if (sIsBidirect(port))
    {
      ++i;
      *cout << "        port = new CarbonPliPort( descr, carbonFindNet(descr, \"" << 
                moduleName << "." << cNet << "\")," << i + 1 << ", false);" << ENDL;
      *cout << "        data->setPort(" << i << ", port );\n";
      *cout << "        port->setSchedTrigger( ePliNoEdges );\n";
    }
  }

  *cout <<
    ENDL
    "        // make this instance available to other carbon system tasks\n"
    "        CarbonPliWrapperContextManager::getPliContext()->addModel(data);\n"
    "\n"
    "        // save instance data for future callbacks\n"
    "        tf_setworkarea((PLI_BYTE8 *) data);\n"
    "\n"
    "        // tell PLI to call us back if any of the ports change value\n"
    "        tf_asynchon(); \n"
    "\n";
  sEmitCallback(*cout, "        data->setUserData(", mIfaceTag, "create(descr));\n");  

  *cout << TAG;

  // update the PLI with initial output values
  sEmitUpdateOutputs(*cout, primaryPorts);

  *cout <<
    "      }" << ENDL
    "      else\n"
    "      {\n"
    "        tf_error(\"Unable to instantiate Carbon model for module %s\", \n"
    "                 (const char *) tf_mipname());\n"
    "        tf_dofinish();\n"
    "      }\n"
    "    }\n"
    "    break;\n"
    "\n"
    "    case REASON_PARAMVC:\n"   
    "    {\n"
    "      CarbonPliModel* data = (CarbonPliModel *) tf_getworkarea();\n"
    "\n"
    "      // update the the DesignPlayer port with the new value\n"
    "      CarbonPliPort *port = data->getPort(paramvc -1);\n"
    "      bool changed = port->pliToCarbon(data->getModel());\n"
    "\n"
    "#if CARBON_SCHED_ALL_CLKEDGES\n"
    "      // if we want waveforms to be correct, we must schedule a callback\n"
    "      // to call carbonSchedule every time we see a signal change.\n"
    "      if (changed) {\n"
    "        tf_synchronize();\n"
    "      }\n"
    "#else\n"
    "      // schedule a callback to call carbonSchedule at the end of this delta\n"
    "      // if this port is a clock/async and the appropriate edge is encountered.\n"
    "      if( changed && \n"
    "          ((port->schedTrigger() == ePliBothEdges) || \n"
    "           ((port->schedTrigger() == ePliPosedge ) && *(port->getValue()) ) ||\n"
    "           ((port->schedTrigger() == ePliNegedge ) && !(*(port->getValue())) ) ) )\n"
    "        tf_synchronize();\n"
    "#endif\n"
    "    }\n"
    "    break;\n"
    "\n"
    "    case REASON_SYNCH:\n"
    "    {\n"
    "      // send all changed inputs to model, schedule, write results back to Verilog\n"
    "      CarbonPliModel* data = (CarbonPliModel *) tf_getworkarea();\n"
    "\n"
    "      // get the simulation time\n"
    "      PLI_UINT32 low;\n"
    "      PLI_INT32 high;\n"
    "      CarbonTime time;\n"
    "      low = tf_getlongtime(&high);\n"
    "      time = (PLI_UINT32) high;\n"
    "      time = (time << 32) + low;\n"
    "\n"
    "      CarbonObjectID *descr = data->getModel();\n" << ENDL;

  // Call schedule to run the model
  sEmitCallback(*cout, "      ", mIfaceTag, "pre_schedule(descr, data->getUserData(), time);\n");  
  *cout << "      if (carbonSchedule(descr, time) == eCarbon_FINISH) tf_dofinish();" << ENDL;
  sEmitCallback(*cout, "      ", mIfaceTag, "post_schedule(descr, data->getUserData(), time);\n");  
  *cout << TAG;

  // output the PLI wth the results of the schedule
  sEmitUpdateOutputs(*cout, primaryPorts);

  *cout <<
    ENDL
    "    }\n"
    "    break;\n"
    "\n"
    "    case REASON_FINISH:\n"
    "    {\n"
    "      // clean up\n"
    "      CarbonPliModel* data = (CarbonPliModel *) tf_getworkarea();\n" << ENDL;
  sEmitCallback(*cout, "      ", mIfaceTag, "destroy(data->getModel(), data->getUserData());\n");  
  *cout <<
    "      delete data;" << ENDL
    "    }\n"
    "    break;\n"
    "  }\n"
    "  return 0;\n"
    "}\n";

  delete cout;

  // the veriusertfs and tab files go into the original working directory
  chdirToOrigDir();

  // Generate a .h file that contains the tfcell array def for the model.
  // This is #included from the .pli_tfcell.c file, and potentially from 
  // a separate file that brings multiple models together into a single tfcell
  // structure.
  UtString tfCellHeader;
  tfCellHeader << mCarbonContext->getFileRoot() << "." << TFCELL_H_EXT;
  UtOStream* cellHeader = CGFileOpen(tfCellHeader.c_str(), "/* tfcell definition for model */", 
                                     gCodeGen->isObfuscating(), true /* needCleartext */);
  if (not cellHeader) {
    return;                     // already error'ed
  }
  *cellHeader << TAG;  // don't know what his is, but it appears to be done everywhere else...
  emitModelTfcell(*cellHeader, mIfaceTag);
  delete cellHeader;

  // Generate the veriusertfs[] array used to register the tasks with a 
  // Verilog simulator.
  UtString tfCellFileName;
  tfCellFileName << mCarbonContext->getFileRoot() << ".pli_tfcell.c";
  UtOStream* out = CGFileOpen(tfCellFileName.c_str(), "/* veriusertfs definition for model */", 
			      gCodeGen->isObfuscating(), true /* needCleartext */);
  if (not out)
    return;                     // already error'ed

  *out << TAG;
  emitCarbonTfcellArray(*out, mIfaceTag, getTargetName());
  delete out;

  // Generate the Synopsys VCS PLI table file.
  UtString tableFileName;
  tableFileName << mCarbonContext->getFileRoot() << ".pli.tab";
  out = new UtOFStream(tableFileName.c_str());
  emitVcsTabFile(*out, mIfaceTag);

  // Cleanup
  for (Loop<CGPrimaryPorts> l(primaryPorts); !l.atEnd(); ++l) {
    CGPrimaryPort* port = *l;
    delete port;
  }
  delete out;
}


static void emitNetNameList(UtOStream& out,          // output stream to write to
                            CGPrimaryPorts& ports,   // ports to iterate
                            const char* prefix,      // prefix output with this string
                            const char* sep,         // separate each net with this string
                            const char* suffix,      // output after last list element
                            CodeGen *codeGen,        // if not NULL, print inout buffer name instead of inout port
                            bool bothInouts)         // if true, output both the input and output nets for input ports
// Note that this is verilog output so do not tag the output stream
{
  bool first = true;
  for (Loop<CGPrimaryPorts> l(ports); !l.atEnd(); ++l) {
    CGPrimaryPort* port = *l;

    // output prefix or separator
    if (first) {
      first = false;
      out  << prefix;
    } else {
      out << sep;
    }

    // output net name
    if (bothInouts && port->isBid() && codeGen != NULL && port->isCodeGenBidirect())
    {
      out << sNetName(port, NULL);
      out << sep;
    }
    out << sNetName(port, codeGen);
  }
  if (!first)
    out << suffix;
}

static void sEmitRange(UtOStream &out, CGPrimaryPort* port, MsgContext* msgContext)
{
  NUNet *net = port->getNet();

  if (net->getBitSize() > 1) {
    const NUVectorNet *vec = dynamic_cast<const NUVectorNet*>(net);
    if (vec != NULL) {
      const ConstantRange *width = vec->getDeclaredRange();      
      out << "[" << width->getMsb() << ":" << width->getLsb() << "] ";  
    }
    else {
      msgContext->CGPliInterfaceUnsupportedPrimaryPortType(net);
    }
  }
}

static void emitNetNameList(UtOStream& out,          // output stream to write to
                            CGPrimaryPorts ports,    // ports to iterate
                            bool (CGPrimaryPort::*keepTest)() const, // filter
                            const char* prefix,      // prefix output with this string
                            const char* sep,         // separate each net with this string
                            const char* suffix,      // output after last list element
                            CodeGen *codeGen,        // if not NULL, print inout buffer name instead of inout port
                            MsgContext *msgContext)
// Note that this is verilog output so do not tag the output stream
{
  bool first = true;
  for (Loop<CGPrimaryPorts> l(ports); !l.atEnd(); ++l) {
    CGPrimaryPort* port = *l;
    if ((port->*keepTest)()) {
      // output prefix or separator
      if (first)
      {
        first = false;
        out << prefix;
      }
      else
        out << sep;

      // output bit range
      sEmitRange(out, port, msgContext);

      // output net name
      out << sNetName(port, codeGen);
    }
  } // for
  if (!first)
    out << suffix;
}

void
CodeGen::codePliVerilogWrapper( NUDesign* design )
{
  // Get the top module, there should only be one
  NUModule* module = NULL;
  for (NUDesign::ModuleLoop l = design->loopTopLevelModules();
       !l.atEnd(); ++l)
  {
    NU_ASSERT(module == NULL, module);
    module = *l;
  }

  const char * moduleName = module->getName()->str();

  // Gather the primary ports for processing. Can't use the top module
  // nets because of port splitting.
  IODBNucleus* iodb = getIODB();
  CGPrimaryPorts primaryPorts;
  for (IODB::NameVecLoop l = iodb->loopPrimaryPorts(); !l.atEnd(); ++l) {
    STSymbolTableNode* node = *l;
    CGPrimaryPort* primaryPort = new CGPrimaryPort(iodb, this, node);
    primaryPorts.push_back(primaryPort);
  }

  // Open a file in build dir named cmodel_wrapper.cxx to hold the wrapper code
  UtString fileName;
  fileName << mCarbonContext->getFileRoot() << ".pli.v";
  // vout is Verilog not generated C++ code so do not tag it.
  UtOStream* vout = CGFileOpen(fileName.c_str(), "", gCodeGen->isObfuscating(),
			       true /* needCleartext */);  
  if (not vout)
    return;                     // already error'ed

  // Emit header comment
  *vout << "/* Verilog PLI wrapper for Carbon model "
        << moduleName << " */\n\n";

  // Emit module statement
  *vout << "module " << moduleName;
  emitNetNameList(*vout, primaryPorts, "(", ", ", ");\n", NULL, false);

  // Emit declarations for each port
  emitNetNameList(*vout, primaryPorts, &CGPrimaryPort::isInput, "  input ", ";\n  input ", ";\n", NULL, mMsgContext);
  emitNetNameList(*vout, primaryPorts, &CGPrimaryPort::isBid, "  inout ", ";\n  inout ", ";\n", NULL, mMsgContext);
  emitNetNameList(*vout, primaryPorts, &CGPrimaryPort::isOutput, "  output ", ";\n  output ", ";\n", NULL, mMsgContext);

  // Emit a wire for each input and inout
  emitNetNameList(*vout, primaryPorts, &CGPrimaryPort::isInput, "  wire ", ";\n  wire ", ";\n", NULL, mMsgContext);
  emitNetNameList(*vout, primaryPorts, &CGPrimaryPort::isBid, "  wire ", ";\n  wire ", ";\n", NULL, mMsgContext);

  // Emit a register for each output and inout; the PLI cannot write to nets.
  emitNetNameList(*vout, primaryPorts, &CGPrimaryPort::isOutput, "  reg ", ";\n  reg ", ";\n", this, mMsgContext);
  for (Loop<CGPrimaryPorts> l(primaryPorts); !l.atEnd(); ++l) {
    CGPrimaryPort* port = *l;
    if (port->isBid() && port->isCodeGenBidirect()) {
      *vout << "  reg ";
        sEmitRange(*vout, port, mMsgContext);
        *vout << sNetName(port, this) << ";\n";
    }
  }

  // Emit an instance of the system task that will call the PLI interface
  *vout << "\n  initial $carbon_" << mIfaceTag << "_instance";
  emitNetNameList(*vout, primaryPorts, "(", ", ", ");\n\n", this, true);

  // Emit an assignment from the reg we generate for each inout to the wire that 
  // represents the actual port.  PLI cannot write to wires.
  for (Loop<CGPrimaryPorts> l(primaryPorts); !l.atEnd(); ++l) {
    CGPrimaryPort* port = *l;
    if (port->isBid() && port->isCodeGenBidirect()) {
      *vout << "  assign " << sNetName(port, NULL) << " = " << sNetName(port, this) << ";\n";
    }
  }

  // end the module
  *vout << "\nendmodule\n";


  // Cleanup
  for (Loop<CGPrimaryPorts> l(primaryPorts); !l.atEnd(); ++l) {
    CGPrimaryPort* port = *l;
    delete port;
  }
  delete vout;
}
