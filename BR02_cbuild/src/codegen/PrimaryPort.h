// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Defines a class to analyze primary ports
*/

#ifndef _PRIMARYPORT_H_
#define _PRIMARYPORT_H_

class IODBNucleus;
class CodeGen;
class STSymbolTableNode;
class NUNet;

//! class CGPrimaryPort
/*! This class stores information for a primary port and allows
 *  requests such as getting the port direction and testing for
 *  various directions.
 *
 *  This is needed because of port splitting. The actual net flags for
 *  a port split net is on the split temps. But some passes want the
 *  information on the original net.
 */
class CGPrimaryPort
{
public:
  //! constructor
  CGPrimaryPort(IODBNucleus* iodb, CodeGen* codeGen, STSymbolTableNode* node);

  //! destructor
  ~CGPrimaryPort() {}

  //! Get the net for this primary port
  NUNet* getNet() const { return mNet; }

  //! Get the net name for this port
  const char* getName() const { return mNet->getName()->str(); }

  //! Get the port flags
  NetFlags getPortFlags() const { return NetFlags(mNetFlags & ePortMask); }

  //! Test if this port is an input
  bool isInput() const { return getPortFlags() == eInputNet; }

  //! Test if this port is an output
  bool isOutput() const { return getPortFlags() == eOutputNet; }

  //! Test if this port is a bid
  bool isBid() const { return getPortFlags() == eBidNet; }

  //! Test if this port is treated like a bidirect by codegen
  /*! This is true if any of the port split nets are codegen bidirects
   *  or if the net is a bidirect for non-split ports. See
   *  CodeGen::isBidirect() for details on what is a codegen bidirect.
   */
  bool isCodeGenBidirect() const { return mIsCGBidirect; }

private:
  //! The original symbol table node (passed in)
  STSymbolTableNode* mNode;

  //! The NUNet associated with this node (computed)
  NUNet* mNet;

  //! The Port flags with this node (computed)
  NetFlags mNetFlags;

  //! Summary bool if any of the port split nets are a codegen bidirect
  bool mIsCGBidirect;
}; // class CGPrimaryPort

#endif // _PRIMARYPORT_H_
