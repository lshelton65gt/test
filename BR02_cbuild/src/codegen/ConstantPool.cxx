// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Pool large BitVector constants within a module
*/

#include "util/DynBitVector.h"
#include "codegen/codegen.h"
#include "emitUtil.h"

//! Save the current constant pool for later dumping..
void CodeGen::saveConstantPool (const NUModule* module)
{
  (*mConstantPoolMap)[module] = mConstantPool;
  mConstantPool = 0;
}

//! Restoring a constant pool
void CodeGen::restoreConstantPool (const NUModule* module)
{
  // It would be a bad thing to overwrite an active pool!
  INFO_ASSERT (mConstantPool == 0, "Can not overwrite an active pool");

  mConstantPool = (*mConstantPoolMap)[module];
}

//! Constant Pool comparison functor
bool CodeGen::ConstPool_Less::operator() (const DynBitVector* p,
                                          const DynBitVector* q)
  const
{
  // Two constants are equal, ONLY if they are the same SIZE
  // and have the same bits.
  if (p->size () < q->size ())
    return true;

  if (p->size () == q->size ())
    return (*p < *q);

  return false;
}

typedef std::pair<const DynBitVector *, UInt32> PoolPair;

// Look for a constant pool entry that is larger and contains
// the candidate as an initial prefix.
struct CmpSubConst{
  const DynBitVector *mP;

  CmpSubConst (const DynBitVector *p): mP (p){}
  bool operator() (const PoolPair& poolElem) {
    if (poolElem.first->getUIntArraySize () < mP->getUIntArraySize ())
      return false;

    // Pool already has a constant that's same size or larger
    // See if the candidate (mP) is a prefix of the pool entry
    //
    return 0 == memcmp (mP->getUIntArray (), poolElem.first->getUIntArray (),
			mP->getUIntArraySize () * sizeof(UInt32));
  }
};

// Look for a constant pool entry that is smaller and is contained
// by the candidate as an initial prefix.  If that matches, we will
// replace the existing entry by the new larger entry...
struct CmpSuperConst{
  const DynBitVector *mP;
  CmpSuperConst (const DynBitVector *p): mP (p){}

  bool operator() (const PoolPair& poolElem) {
    if (poolElem.first->getUIntArraySize () >= mP->getUIntArraySize ())
      return false;

    // pool entry is smaller than new candidate.  Check if existing
    // entry is a prefix of the new candidate entry.
    return 0 == memcmp (mP->getUIntArray (), poolElem.first->getUIntArray (),
			poolElem.first->getUIntArraySize () * sizeof(UInt32));
  }
};
    

//! Add a BitVector constant value to the class constant pool
/*!
 * \a bv A DynBitVector& representing the constant value
 * \returns 32-bit UUID representing the constant's name cv$<UUID>.
 */
UInt32
CodeGen::addPooledConstant (const DynBitVector& bv, bool sign)
{
  if (not mConstantPool)
    mConstantPool = new ConstPool_t;

  // Unique 
  static UInt32 uuid=0;

  ConstPool_t::iterator hit = mConstantPool->find (&bv);

  if (hit != mConstantPool->end ())
    // exact match
    return (*hit).second;
  
  // Slower search to see if we can match a constant that is larger
  hit = find_if (mConstantPool->begin (), mConstantPool->end (),
		 CmpSubConst (&bv));
  if (hit != mConstantPool->end ())
    return (*hit).second;	// Can use this larger constant.

  // Look to see if we can replace an existing constant with a larger
  // one.
  hit = find_if (mConstantPool->begin (), mConstantPool->end (),
		 CmpSuperConst (&bv));
  UInt32 tag;
  if (hit != mConstantPool->end ())
  {
    tag = (*hit).second;

    delete (*hit).first;	// reclaim the constant
    mConstantPool->erase (hit); // and remove entry in pool
  } else {
    tag = ++uuid;               // a brand new tag
  }

  // Add a new entry using tag.
  DynBitVector* newbv = new DynBitVector (bv);
  UInt32 actualBits = bv.size ();
  UInt32 roundedBits = (actualBits + 31) & ~31;

  newbv->resize (roundedBits);
  if (sign                  // sign extension possible
      && (actualBits % 32)  // not already a word multiple
      && newbv->test (actualBits-1)) // MSB is set
  {
    newbv->setRange (actualBits, roundedBits - actualBits, true);

    // check to see if the SIGNED constant is represented already in the
    // pool
    hit = find_if (mConstantPool->begin (), mConstantPool->end (),
                   CmpSubConst (newbv));
    if (hit != mConstantPool->end ()) {
      delete newbv;
      return (*hit).second;
    }
  }

  mConstantPool->insert (std::make_pair (newbv, tag));
  return tag;
}

//! Empty the constant pool at the completion of generating all the C++ code

static void sFlushAPool (CodeGen::ConstPool_t* pool)
{
  if (not pool)
    return;

  for (CodeGen::ConstPool_t::iterator p = pool->begin ();
       p != pool->end ();
       ++p)
  {
    CodeGen::ConstPool_t::key_type bv = p->first;
    delete bv;
  }

  delete pool;
}

void
CodeGen::flushConstantPool (void)
{
  if (not mConstantPoolMap)
    return;    // No constant pool

  // Might have a constant pool for the schedule, delete it first
  sFlushAPool (mConstantPool);

  // Toss out any module-specific constants
  for (ModuleConstantPoolMap::iterator map = mConstantPoolMap->begin ();
       map != mConstantPoolMap->end ();
       ++map) {
    ModuleConstantPoolMap::mapped_type pool = map->second;

    if (mConstantPool == pool)
      continue;                 // Don't delete pool twice

    sFlushAPool (pool);
  }

  mConstantPool = 0;            // don't leave any dangling pointers.
}

// Generate constant pool (both in .h and .cxx files
void CodeGen::dumpConstantPool (UtOStream& out, CGContext_t context)
{
  if (not mConstantPool)
    return;

  UtString className;

  if (isSchedule (context))
    className = "ScheduleConstants";
  else
    className = emitUtil::mclass (getClass ());

  // This one has a constant pool, so we have to compile it.
  putNonEmptyCxx (mCurrentModule);

  for(ConstPool_t::iterator c = mConstantPool->begin ();
      c != mConstantPool->end ();
      ++c)
  {
    UInt32 i = (*c).second;	// Unique constant tag.
    ConstPool_t::key_type bv = (*c).first; // The cached value

    if (isDefine (context))
    {
      out << "const UInt32 " << className
          << "::cv$" << i << "[] = {";

      const UInt32* array = bv->getUIntArray ();

      for(size_t j = bv->getUIntArraySize ();
          j != 0;
          --j)
      {
        out << "0x" << UtIO::hex << *array++ << UtIO::dec;
        if (j > 1)
          out << ", ";
      }
      out << "};\n";
    }
    else if (isDeclare (context))
    {
      // Generate class declarations

      out << "  static const UInt32 cv$" << c->second << "["
	  << bv->getUIntArraySize () << "];\n";
    }
  }
}
