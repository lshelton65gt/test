//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <cassert>
#include <climits>
#include <cstdio>
#include <cstring>
#include <cstddef>
#include <cstdlib>
#include <clocale>
#include <cctype>
#include <ctime>
#include <cwchar>
#include <cwctype>
#include <cmath>
#include <iosfwd>               // needed by <algorithm>, included by UtHashSet

#if pfICC
#include <mmintrin.h>
#endif

// test elimination of get_, m_, f_, a_ macros by allowing these to be #defined.
#if 0
#define get_lost bad accessor
#define m_i 5
#define f_anything goes away
#define a_larm
#endif
