# -*- Makefile -*-
#
# *****************************************************************************
# Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
# *****************************************************************************
#
# compiler-dependent makefile fragment

# Define where compiler can be found
CARBON_COMPILER_DIST=${CARBON_HOME}/${CARBON_TARGET_ARCH}/gcc2

# We could potentially need to run gcc compiled for the target architecture
# and speedcc compiled for the host architecture, so we'd need both libraries.
CARBON_LIBRARY_PATH := $(CARBON_HOME)/$(CARBON_HOST_ARCH)/gcc/lib
ifneq ($(CARBON_HOST_ARCH),$(CARBON_TARGET_ARCH))
# If host doesn't match target, append target libraries.
# (Can't use += because that adds a space.)
  CARBON_LIBRARY_PATH := $(CARBON_LIBRARY_PATH):$(CARBON_HOME)/$(CARBON_TARGET_ARCH)/gcc/lib
endif

# Use "env -i" to prevent user's environment from causing problems.
# We set COMPILER_PATH to find our copies of tools like `as' and `ld'.
CARBON_COMPILER_ENV = env -i LANG=C COMPILER_PATH=${CARBON_HOME}/${CARBON_TARGET_ARCH}/bin \
	       HOME=$(HOME) LD_LIBRARY_PATH=$(CARBON_LIBRARY_PATH) \
	       GCC_EXEC_PREFIX=$(CARBON_COMPILER_DIST)/lib/gcc-lib/

CARBON_CBUILD_CC  = $(CARBON_COMPILER_DIST)/bin/gcc
CARBON_CBUILD_CXX = $(CARBON_COMPILER_DIST)/bin/g++

# Use GCC2 as linker
CARBON_CBUILD_LD = $(CARBON_CBUILD_CXX)

## Indicate which generation of compiler we're using
CARBON_GCC2=1
##

# Search all passed in options for -O<level> flags
CARBON_CXX_OPTLIST:=$(filter -O%,$(CXX_EXTRA_FLAGS))

# Find the last one specified
ifeq (,$(CARBON_CXX_OPTLIST))
CARBON_CXX_OPT := -O2
else
CARBON_CXX_OPT:=$(word $(words $(CARBON_CXX_OPTLIST)),$(CARBON_CXX_OPTLIST))
endif

# Look for any profiling flags.  This is true also if CARBON_PROFILING
# was set globally
ifneq (,$(findstring -pg,$(CXX_EXTRA_FLAGS) $(CXX_WC_FLAGS)))
CARBON_PROFILING=1
endif

CARBON_OPTFLAGS =

ifneq (0,$(findstring $(CARBON_CXX_OPT),-O2 -O3))
  ifeq ($(CARBON_TARGET_ARCH),Linux)
    CARBON_OPTFLAGS += -funroll-loops -mcpu=i686 -march=i686
  endif

  # options useful when you want to profile with gprof
  ifeq ($(CARBON_PROFILING),1)
    CARBON_OPTFLAGS += -fno-default-inline -fno-inline
  endif
else	# not -O2 or -O3
endif


# Define magic values that turn C++ exceptions off/on
CARBON_EXCEPTIONS_OFF:=-fno-exceptions
CARBON_EXCEPTIONS_ON :=-fexceptions

CARBON_CXX_FLAGS = $(CARBON_EXCEPTIONS_OFF)

CARBON_LINK_RPATH = -Wl,-rpath,$(CARBON_COMPILER_DIST)/lib -Wl,-rpath,$(CARBON_HOME)/Linux/gcc3/lib

# Basic compiler-dependent flags
#
CARBON_CXX_DEF_COMPILE_FLAGS = $(CARBON_CXX_FLAGS) -ftemplate-depth-80
CARBON_CXX_DEF_LINK_FLAGS = $(CARBON_LINK_RPATH)

CARBON_DISABLE_DYNAMIC_CAST = -fno-rtti
CARBON_ENABLE_DYNAMIC_CAST = -frtti

CARBON_CXX_SPEEDCC_COMPILE_FLAGS += -pipe $(CXX_WC_FLAGS) $(CARBON_DISABLE_DYNAMIC_CAST)

ifeq ($(CARBON_PROFILING),1)
else
ifeq ($(GCC_NO_WALL),)
CARBON_CXX_SPEEDCC_COMPILE_FLAGS += -Wall -W -Werror
endif
endif

# Where gcc wants to look for libraries
#
CARBON_COMPILER_LIB_DIRS= $(CARBON_COMPILER_DIST)/lib

# flags needed to compile shared libraries
CARBON_SHARED_COMPILE_FLAGS= -shared -fPIC
# flags needed to link shared lib. 
CARBON_SHARED_LINK_FLAGS=-Wl,-rpath,$(@D)

CARBON_GCCVER = 2.95.3

ifeq (,$(findstring $(CARBON_COMPILER_DIST),$(LD_LIBRARY_PATH)))
  LD_LIBRARY_PATH := $(LD_LIBRARY_PATH):$(CARBON_COMPILER_LIB_DIRS)
  export LD_LIBRARY_PATH
endif
