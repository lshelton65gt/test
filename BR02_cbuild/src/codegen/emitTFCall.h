// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _EMITTFCALL_H_
#define _EMITTFCALL_H_

//! An enumerated type for the various names used in hier ref tasks
enum CGHierRefTaskName
{
  eParentVar,      //!< A pointer to the parent module of a task
  eFunctorBase,    //!< Name of abstract base class for functor
  eFunctorDerived, //!< Name of derived class for functor
  eFunctorPtr      //!< Pointer to the functor class
};

void CGTFgenHierRefTaskName(CGHierRefTaskName name, const NUTaskHierRef* taskHierRef,
                            const NUModule* taskParent, UtString* hierRefName);
#endif // _EMITTFCALL_H_
