// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef _EMITUTIL_H
#define _EMITUTIL_H
 /*
 * \file
 * Common utility functions for emitCode methods.  These are mostly formatted
 * i/o or predicate functions that help guide the construction of C++ code.
 */

// Some problems with BUG1993 and workarounds
#define BUG1993 1		// 1 => workarounds turned on

#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNamedDeclarationScope.h"

class NUAssign;

#include "codegen/codegen.h"
#include "codegen/CGAuxInfo.h"
#include "codegen/CGOFiles.h"
#include "codegen/CGProfile.h"
#include "util/CodeStream.h"

namespace emitUtil
{
  // selector for various objects
  enum ObjectAlignment {
    eInt8 = 0,
    eInt16= 1,
    eInt32= 2,
    eInt64= 3,
    eBitVector= 4,
    ePointer= 5
  };

  // categories of Memories - each of these can be sparse as well.
  enum MemoryClass {
    eInvalid,                   //<! Unrecognized memory
    eMemory,                    //<! Vanilla memory
    eStateUpdateMemory,         //<! scheduled buffered memory fixed temps
    eDynStateUpdateMemory,      //<! scheduled buffered memory dynamic temps
    eDynTempMemory,             //<! linked list of buffered writes
    eStaticTempMemory,          //<! Fixed-size array of buffered writes
  };

  // modes for the save/restore code generation functions
  enum SaveRestoreGenMode {
    eSaveRestoreNormal,
    eSaveRestoreOnDemand
  };

#undef SHORT_BIT
#undef LONG_BIT
#undef LLONG_BIT

#define SHORT_BIT 16
#define LONG_BIT 32
#define LLONG_BIT 64

//! For the current target return the alignment of an object of type \a otype
size_t alignmentOf (enum ObjectAlignment otype);

// Convert bitwidth to number of words required to represent the bitvector.
inline int BVWords (size_t nbits) { return (nbits+31)/32; }


// Are there any referenced static symbols allocated in this block and
// the blocks it contains?
struct StaticFilter: public std::unary_function<NUNet*, bool>{
  bool mAllocated;

  bool operator() (const NUNet* n) const{
    if (n->isNonStatic ()) {
      if (not n->isTFNet()) {
      // Task nets are all non-static. Disqualify non-statics only
      // when the net is declared outside a task.
        return false;
      }
    }

    // It's static...

    CGAuxInfo *cg = n->getCGOp (); // Never processed, don't start now
    if (not cg || cg->isUnreferenced () // Never referenced - don't declare
        || cg->isPortAliased ())   // Not allocated here, stored elsewhere
      return false;

    // Since we walk the tree twice, we have to be careful not to get things
    // already assigned an offset.
    return (mAllocated ==  cg->hasOffset ());
  }

  //! filter statics - by default get things that are static and not
  //! already allocated.
  StaticFilter (bool alloc=false) : mAllocated (alloc){}
};

// Helper class for STL algorithms
struct MarkAuxInfo {
  MarkAuxInfo (enum OpFlags flags): mFlags (flags) {}
  void operator()(const NUNet*n) const {
    if (!n->getCGOp ())
      n->emitCode (0);

    CGAuxInfo *p = n->getCGOp ();
    p->setFlags ( OpFlags ( p->getFlags () | mFlags));
  }
  enum OpFlags mFlags;
};

//! Fancy format constructor/printer for primitive types
//
struct CBaseTypeFmt
{
  //! primitive type only depends on bits in representation.
  size_t mPrec;
  //! Extra indicator if we know something is signed.
  bool	mSigned;
  //! Extra indicator if we know something is a real number.
  bool mReal;
  //! Extra type information
  const NUNet *mNet;
  //! but how we print is a function of the context
  CGContext_t mCtxt;

  //! Constructors
  CBaseTypeFmt ()
    : mPrec (0), mSigned (false), mReal (false), mNet (0), mCtxt (eCGVoid)
  {}

  CBaseTypeFmt (int prec, CGContext_t ctxt=eCGVoid, bool sign=false, bool real=false,
		const NUNet * net=0, const NUExpr * e=0)
    : mPrec (prec), mSigned (sign), mReal (real), mNet (net), mCtxt(ctxt)
  {
    if ( net != 0 ) {
      mReal = net->isReal();
    }
    if ( e != 0 ) {
      const NUIdentRvalue *primary = dynamic_cast<const NUIdentRvalue*>(e);
      if (primary) {
	mNet = primary->getIdent ();
      }
    }
  }

  CBaseTypeFmt (const NUNet *p, CGContext_t ctxt=eCGVoid)
    : mPrec (p->getBitSize ()), mSigned (p->isSigned ()), mReal (p->isReal()),
      mNet (p), mCtxt (ctxt){}

  CBaseTypeFmt (const NUExpr *e,  CGContext_t ctxt=eCGVoid)
    : mPrec (e->getBitSize ()), mSigned (e->isSignedResult ()), mReal(e->isReal()),
      mNet (0), mCtxt (ctxt)
  {
    const NUIdentRvalue *primary = dynamic_cast<const NUIdentRvalue*>(e);
    if (primary) {
      mNet = primary->getIdent ();
    }
  };
  CBaseTypeFmt (const NUExpr *e, bool sign, CGContext_t ctxt=eCGVoid)
    : mPrec (e->getBitSize ()), mSigned (sign), mReal(e->isReal()), mNet (0),
      mCtxt (ctxt)
  {
    const NUIdentRvalue *primary = dynamic_cast<const NUIdentRvalue*>(e);
    if (primary)
      mNet = primary->getIdent ();
  }

  //! Are two type descriptors "equivalent"
  bool operator==(const CBaseTypeFmt& other);
  bool operator!=(const CBaseTypeFmt& other) { return !(*this == other); }
private:
  CBaseTypeFmt& operator=(const CBaseTypeFmt&);
};

UtString& operator<< (UtString& s, const CBaseTypeFmt& n);

UtOStream & operator << (UtOStream& f, const CBaseTypeFmt& n);

// Pretty printing strengths for tristate drivers
UtOStream& operator << (UtOStream& f, Strength s);

//! Generalized type printing output manipulator.
/*! Construct full type information from a net, including
 * details such as StateUpdate<> or BitVector<> information.  Works with
 * overloaded operator << 
 */
class CType
{
  //! the NET we want to print information about
  const NUNet *p;
public:
  friend UtOStream & operator <<(UtOStream&, const CType&);

  // Constructor
  CType (const NUNet *node) :p(node) {}

private:
  CType& operator=(const CType&);
};

//! print the full type for a net
UtOStream & operator << (UtOStream& f, const CType& n);

// Format controls for various translated Verilog names

extern CodeGen::CxxName member; // Format as class member
extern CodeGen::CxxName funct; // Format as function () 
extern CodeGen::CxxName accessor; // Format as output accessor
extern CodeGen::CxxName mclass; // Format as translated class name
extern CodeGen::CxxName pclass; // Format as c_name:: prefix
extern CodeGen::CxxName formal; // Format for function formal param
extern CodeGen::CxxName noprefix; // Format with no extra chars
extern CodeGen::CxxName local; // Format for function formal param
extern CodeGen::CxxName block; // Format for a nested block name
extern CodeGen::CxxName fname; // Format as function name

UtOStream& operator<<(UtOStream& f, const NUNet* net);

UtOStream& operator<<(UtOStream& f, const NUNetElab* net);

//!Test for type compatibility.
/*!
 * The actual parameter must be the same size, must not be more
 * complex than the formal (e.g. if it's a Tristate<T> or a
 * StateUpdate<T> and the formal is just a T) then we are going to
 * need to coerce this somehow...
 */
bool
isTypeCompatible (const NUNet* actual, const NUNet* formal);

bool
isTypeCompatible (const NUExpr* x, const NUExpr* y);

// Test for a compile-time constant value and return it...
bool
isCTCE (const NUExpr *v, DynBitVector *value);

bool
isCTCE (const NUExpr *v, UInt64 *val=0);
  
//! Test if a net will be represented by a POD
bool isPOD (const NUNet*);

//! is this net referenced in the C++ code?
bool isNetReferenced(const NUNet* net);

//! Test if net is a VHDL record that's been port aliased.
bool isVhdlRecordPort (const NUNet*);

//! Test if an expression is a primary
bool isPrimary (const NUExpr*);

// Test for a value identical to zero
//
bool
isZeroValue (const NUExpr *v);

bool
isOnesValue (const NUExpr *v);

// Generate a unique name using provided UtString prefix.
void
UniqueName (UtString* s);

// Does this operand require sign-extension?
bool needsSignExtension (const NUNet* net, UInt32 width, bool signedResult);
bool needsSignExtension (NUOp::OpT opc, const NUExpr* lop, const NUExpr* rop,
  UInt32 resultSize, UInt32 index);

// Given a function/always block/ continuous assign, etc that
// could be in a function layout ordering, return the section and true/false
// indicator
bool functionSection (const NUUseDefNode* func, UtString* sectionName);


// Emit local declarations within a scope block.  To emit static variables too,
// pass \a allDecls as true.
void genLocalDecls (const NUScope* scope, bool allDecls);

//! Given a bitstring, generate a static bitvector expression
/*!
 * \a out       - output stream
 * \a value     - DynBitVector representing integer value
 * \a vsize     - precision to print (differs from value.size() if we
 *                are BVOverloading this operand)
 * \a context   - operand context (possibly overload or initialized Lvalue constructor)
 * \a size      - treat as signed or unsigned value
 */
CGContext_t
emitConstant (UtOStream& out, const DynBitVector& value, UInt32 vsize,
              CGContext_t context, bool sign);

//! Print up to 64 bits
CGContext_t
emitConstant (UtOStream& out, UInt64 value, UInt32 size, CGContext_t context, bool sign);

// Output an operation UtString followed by a mask of SIZE 1's The
// first function always generates just the mask, the second
// conditionally generates the mask if it is needed to clear possible
// dirty bits.
void genMask(UtOStream& out, size_t size, bool signedType=false);
void mask(size_t size, const char *op, bool signedType=false);

// Utility to print out DeadCodeWalking message
CGContext_t
checkDeadCode (const SourceLocator& loc, CGContext_t context, const UtString& kindString);

/*
 * Nets marked as 'input' are usually pointers, except for those at the
 * top level of the design which actually hold values.
 */
bool isDereferenced(const NUNet *n);

/*
 * Is the lvalue a forcible net?
 */
bool sIsForcible (const NUNet* net);

//! Find the net associated with a value
NUNet* sFindNet (const NULvalue* lv);

NUNet* sFindNet (const NUExpr* rv);

/* If the bit-size provided maps directly to the storage unit, we
 * can optimize away certain things.
 */
bool
naturalSize (size_t size);

// Compute storage required for an object.  Does NOT handle composite
// objects, so a NUMemoryNet returns the size of a single word, a StateUpdate
// returns the size of the basic unit being double-buffered, not the TOTAL size
//
size_t netByteSize (const NUNet *n);

// Return the alignment required by a net
size_t alignment (const NUNet *);

// Return the alignment for an object of a specific size
size_t alignment (const size_t);

// Determine maximal alignment required at this point by a nested declaration scope
size_t alignment (const NUNamedDeclarationScope *);

// How big are runtime pointers?
size_t ptrSize ();

/*!
 * Adjust the byte \a size so that it is aligned to an \a alignment
 * byte boundary.
 */
inline size_t align2(size_t size, UInt32 alignment)
{
  size = (size + alignment - 1) & ~(alignment - 1);
  return size;
}

//! Is this a sparse memory?
bool isSparseMemory (const NUNet* net);

//! Classify the memory by what C++ class will be used to model it
MemoryClass classifyMemory (const NUNet* net);

//! Compute proper alignment for a memory object
size_t alignmentMemory (const NUMemoryNet* memNet);

//! Compute proper size for a memory
size_t sizeofMemory (const NUMemoryNet* mem);

size_t allocation (const NUNet *net);

//! Determine the allocation within the onDemand state buffer
size_t onDemandStateAllocation (const NUNet *net);

//! Return maximal alignment required for a sequence of objects.
/*
 * Typically called for a substructure to determine the alignment needed
 * before allocating an instance.
 */
size_t alignment (const NUNetVector &p);

// Collect Housekeeping information on each net as we visit it.
void checkNetIncludes (const NUNet *n);

//! Generate the qualified name for a symbol declared in a block
/*!
 * Prints b_foo.b_bar.m_fink
 *
 * \a out output stream to write to
 * \a blk NUScope point to the immediately containing block/scope
 * \a sep the component separator - normally '.', but for VHDL records passed
 *         via ports, use '$'.
 */
void emitBlockNesting (UtOStream& out, const NUScope * blk, char sep);

//! Emit a member for save/restore list, including any block nesting, if necessary
/*!
 * \a out output stream to write to
 * \a net NUNet for the member
 */
void emitSaveRestoreMember(UtOStream& out, const NUNet *net);

/*
 * Return the bit-bounds for the Net that is below a NUBitselRvalue,
 * NUPartselRvalue, NUBitselLvalue or NUPartselLvalue. 
 */ 
const ConstantRange* getNUrange(const NUNet *p);

/*
 * Return the depth-bounds for a memory that is used in a
 * NUMemSelLvalue or NUMemSelRvalue. This allows us to fix up the
 * index into the memory if the start isn't 0.
 */
const ConstantRange* getNUMemDepth(NUNet *p);

// Calculate the same way as EmitIndex does...
SInt32
ComputeIndex (SInt32 index, const ConstantRange& r);

bool
isDynRange (const NUExpr*e, const NUExpr**lowBound=0);

void
emitIndex(UtOStream& out, const NUExpr* index, SInt32 bitoff,
          CGContext_t c, const ConstantRange& r);

void
emitIndex(UtOStream& out, SInt32 index, const ConstantRange& r);

// If we need a class prefix for this context, output it.
//
void classPrefix(CGContext_t c);


//! Is this an assignment of a concatenation that can be optimized?
bool
isSimpleConcat (const NUAssign *stmt);

//! Code an assignment like A = {B,C} where A doesn't appear in B or C
CGContext_t
genSimpleConcat (const NUAssign *stmt, CGContext_t context);

//! Is the bitwidth a hardware compatible size?
bool isPhysicalSize (UInt32 size);

//! Convert width to a physical size (byte, short, int, long long int)
UInt32 physicalSize (UInt32 size);

//! Does the access to net of width \a width need to be sign-extended?
bool needsSignExtension (NUNet*net, UInt32 width, bool signedResult);

bool needsSignExtension (NUOp::OpT opc, const NUExpr*lop, const NUExpr* rop,
                         UInt32 resultSize, UInt32 index);

//! Generate sign-extension code to access this operand
CGContext_t signExtend (UtOStream&, const NUNet* net, const STBranchNode* parent,
                        CGContext_t context, UInt32 width);

//! Sign extend an expression.
CGContext_t signExtend (UtOStream&, const NUExpr* net, CGContext_t context, UInt32 width);

//! Make operand sizes compatible for mixed mode arithmetic - e.g. bitvector \<op\> UInt8
UInt32 bvSizeToOther (UInt32 thisSize, UInt32 thatSize);

//! return the size of the net unless it's a memory - then return size of access
UInt32 elementSize (const NUNet* net);

//! Is expression going to yield a BVref?
bool isBVref (const NUExpr* e);
bool isBVref (const NULvalue* lv);

// Helper function to coerce the operand to the size value we require.
// Question: if Rvalue size is smaller than subtree, should we mask off
// excess bits?
CGContext_t
coerceRvalue(const NUNet* net, const STBranchNode* parent,
             UInt32 bitSize, bool sign, CGContext_t context);

// check if the BitVector operation is supported for 
// integer arithmetic, assignment.
bool
isBvOverloaded (NUOp::OpT opc, const NUExpr *operand, const NUExpr* other,
                bool first, UInt32*newSize);

void emitHierarchy(CGContext_t context, const STBranchNode* parent);

//! Extract a partsel from a bitvector
void genBVpartsel (UtOStream& out, UInt32 pos, UInt32 size);

//! handles nested partselect assign
CGContext_t 
nestedPartselAssign(const NUVarselLvalue*, const NUExpr *, CGContext_t);

CGContext_t
concatAssign (const NUConcatLvalue*, const NUExpr *, CGContext_t);

// Identify bitvector BVref's that are candidates for reinterpret-casts
// as aligned smaller bitvectors.
bool
isAlignedBVref (const NUNet* net, const NUExpr* expr, const NUExpr* index,
                SInt32 pos, UInt32 size, bool excessOK);

CGContext_t
genAlignedBVref (UtOStream& out, const NUExpr* rvalue, const NUExpr* index,
		 CGContext_t context, SInt32 pos, UInt32 size, bool indexInBounds);

// Measure if a function/task/always-block/initial-block is a candidate for inlining
bool
isInlineCandidate (const NUUseDefNode* codeBlock, NUCost *cost);

// Dump the cost information in human readable form for a function/task/structured-proc
void
annotateCosts (const NUCost& cost);

//! Check for feasible load/store optimization
bool
isAlignedAccess (const NUNet* var, const NUExpr* expr, SInt32 pos, UInt32 size);

//! Generate an aligned byte, short, int access (being aware of endianness!)
CGContext_t sGenAlignedAccess (const NUExpr* expr, SInt32 pos, UInt32 size, bool sign, CGContext_t);

//! Generate a 64 bit value from an aligned bitvector access
CGContext_t sGenLLValue (const NUExpr* base, UInt32 wordOffset, CGContext_t context);

//! Generate a 32 bit value
CGContext_t sGenLValue (const NUExpr* base, UInt32 wordOffset, CGContext_t context);

//! Generate save/restore methods for a class
void GenerateSaveRestore (const NUModule *module, CGContext_t context, SaveRestoreGenMode mode);

void getGeneratedType(UtString* typeBuf, bool isTristate, bool isBidirect, 
                      bool isSigned, UInt32 width, bool isInterface, bool isReal);

const NUMemoryNet* sGetMemory (const NUNet*);

void allocateNet (const NUNet*);

// Sort largest alignment first; this minimizes padding.  But always sort
// memory before TempMemory because TempMemory constructor wants to connect to the
// master memory, so it must be declared after the master memory.
//
struct sort_by_align {
  bool operator() (const NUNet* a, const NUNet *b) const {

    if (b->isMemoryNet () && dynamic_cast<const NUTempMemoryNet*>(a))
      return false;           // sort temp memory after everything else
    if (a->isMemoryNet () && dynamic_cast<const NUTempMemoryNet*>(b))
      return true;            // sort temp memory after non-temp

    size_t apad = alignment (a);
    size_t bpad = alignment (b);

    return (apad > bpad);
  }
  
  // Named declaration scopes have alignment scopes too
  bool operator() (const NUNamedDeclarationScope* a, const NUNamedDeclarationScope *b) const {
    return alignment (a) > alignment (b);
  }

  bool operator() (const NUModuleInstance *a, const NUModuleInstance *b) const{

    // Choose largest alignment to go first.
    return a->getModule ()->getClassDataAlignment ()
      > b->getModule ()->getClassDataAlignment ();
  }

};

// Determine if this net is part of onDemand state.
// Used by save/restore generation as well as codegen layout
bool isOnDemandStateNet(const NUNet *net);

struct sort_by_state {
  bool operator() (const NUNet* a, const NUNet *b) const {

    // ensure that flops/latches come first
    bool a_is_state = emitUtil::isOnDemandStateNet(a);
    bool b_is_state = emitUtil::isOnDemandStateNet(b);

    if (a_is_state && !b_is_state)
      return true;
    if (!a_is_state && b_is_state)
      return false;

    // fall back on default sort
    sort_by_align s;
    return (s(a, b));
  }
};

// Sort smallest alignment first; this improves locality.  But sort Memory
// before TempMemory
struct sort_by_reverse_align {
  bool operator() (const NUNet* a, const NUNet *b) const {
    if (b->isMemoryNet () && dynamic_cast<const NUTempMemoryNet*>(a))
      return false;           // sort temp memory after everything else
    if (a->isMemoryNet () && dynamic_cast<const NUTempMemoryNet*>(b))
      return true;            // sort temp memory after non-temp

    size_t apad = alignment (a);
    size_t bpad = alignment (b);

    return (apad < bpad);
  }
  
  // Named declaration scopse have alignment scopes too
  bool operator() (const NUNamedDeclarationScope* a, const NUNamedDeclarationScope *b) const {
    return alignment (a) < alignment (b);
  }
};


// Sort by declaration order so that member initialization
// occurs in correct order.
struct sort_by_address {
  bool operator() (const NUNet* a, const NUNet *b) const{
    size_t aOff = 0;
    size_t bOff = 0;

    // when CGOp is NULL, net will be ignored.
    CGAuxInfo* pa = a->getCGOp ();
    if (pa)
      aOff = (pa->hasOffset ()) ? pa->getOffset () : 0;
    CGAuxInfo* pb = b->getCGOp ();
    if (pb)
      bOff = (pb->hasOffset ()) ? pb->getOffset () : 0;
    if (aOff == bOff)
      return (*a) < (*b);       // Force a consistent order
    else
      return aOff < bOff;
  }
  bool operator() (const NUNamedDeclarationScope*a, const NUNamedDeclarationScope*b) const{
    // a named scope contains allocated nets and should be ordered appropriately
    size_t aOff = 0;
    size_t bOff = 0;
    CGAuxInfo *pa = a->getCGOp ();
    if (pa)
      aOff = (pa->hasOffset ()) ? pa->getOffset () : 0;
    CGAuxInfo* pb = b->getCGOp ();
    if (pb)
      bOff = (pb->hasOffset ()) ? pb->getOffset () : 0;
    if (aOff == bOff)
      return a < b;
    else
      return aOff < bOff;
  }
};


// Temp memories have to be declared after any locally scoped memories
// or we can run into a use-before-declaration error.
struct sort_by_decl_order {
  bool operator() (const NUNet* a, const NUNet *b) const{
    if (b->isMemoryNet () && dynamic_cast<const NUTempMemoryNet*>(a))
      return false;           // sort temp memory after everything else
    if (a->isMemoryNet () && dynamic_cast<const NUTempMemoryNet*>(b))
      return true;            // sort temp memory after non-temp

    return alignment (a) < alignment (b); // Use alignment otherwise.
  }
};

//! True if the source expression contains part (or all) of the destination expression value
/*! \param noCover=> true says 1:1 overlap not allowed
 */
bool sDestOverlapsSrc (const NUAssign*, bool noCover);

//! Returns "const " or "".
const char* netConstPrefix(const NUNet* net);
bool isNetConst(const NUNet* net);
bool netIsDeclaredForcible(const NUNet*);
bool IsWindowsTarget ();
bool isNetDeclared(const NUNet* net);

//! Find the allocated net and parent module for a given elaborated net
NUNet* findAllocatedNet(const NUNetElab* netElab,
                        const STBranchNode** parent);

// generate the text for an identity accessor method
void generateAccessorFunction (const NUNet* net);

// generate the text for a simple member variable declaration
void generateMemberVariable (const NUNet* net, bool deref);

// Visit port connections, mark the nets and CGAuxinfo, and optionally generate the
// declarations of the port aliases 
//
void emitAliasedPortConnections (const NUModule* mod, CGContext_t context);

bool isCallOnPlaybackCModel(const NUCModel* cmodel);

//! Removes nets that will not be saved from onDemand state net set
void pruneOnDemandNets();

} // namespace emitUtil


// Utility code generation functions

//! Walk a container applying emitCode with the correct context
// This is templatized so that it can handle all the different kinds of lists
// that the nucleus contains.
//
struct SimpleList {
  CGContext_t ctxt;

  SimpleList (CGContext_t c) : ctxt (c) {}
  void operator() (const NUBase *n) const { n->emitCode (ctxt);}

  //! Specialize for UD nodes so we can keep track of what continuous drivers have been emitted.
  void operator() (const NUUseDefNode *n) const;
};

template <typename T>  void emitCodeList (T* container,
					  CGContext_t context)
{
  std::for_each (container->begin(), container->end(), SimpleList (context));
}

//! Fancy formatted list walker supporting prefix, infix and suffix
//	"(" name ',' name ',' name ")"
//
struct ListFormat
{
  //! punctuation currently in force (starts out as prefix)
  mutable const char *sep;
  //! the actual infix
  const char *infix;
  //! context to pass to emitCode()
  CGContext_t ctxt;

  //! the iterator function
  void operator ()(const NUBase *n) const
  {
    gCodeGen->getCGOFiles()->CGOUT() << sep;
    n->emitCode (ctxt | eCGList);
    sep = infix;
  }

  //! trivial constructor
  ListFormat (const char *second, CGContext_t context)
    : sep(""), infix(second), ctxt(context) {}
};

//!Templated formatted list printing algorithm
template <typename T> void emitCodeList (T* container,
					 CGContext_t context,
					 const char *prefix,
					 const char *infix,
					 const char *suffix)
{
  gCodeGen->getCGOFiles()->CGOUT () << prefix << TAG; // Force prefix out

  std::for_each (container->begin(), container->end(),
	    ListFormat(infix, context));

  gCodeGen->getCGOFiles()->CGOUT () << suffix;	// Force suffix out
}

struct PrePost {
  
  const char *prefix, *suffix;
  CGContext_t ctxt;
  // Constructor
  PrePost (const char *p, const char *s, CGContext_t c)
    : prefix (p), suffix (s), ctxt (c){}

  void operator()(const NUBase *n) const
  {
    // NUBase doesn't declare getLoc(), for some reason.  Here's at
    // least one type that does.
    const NUUseDefNode* ud_node = dynamic_cast<const NUUseDefNode*>(n);

    if (ud_node) {
      // make an HDL annotation in the code stream
      gCodeGen->getCGOFiles()->CGOUT () << HDLTAG (ud_node);
    };

#ifdef BUCKET_COMMENTS
    // Generate a comment showing the node's type, and source locator
    // if available, before genning code.

    // Avoid breaking generated #include directives with this test.
    // NUBase doesn't have getType(), so we use typeStr().
    if (strcmp(n->typeStr(), "NUCModel") != 0) { // Not NUCModel
      gCodeGen->getCGOFiles()->CGOUT () << prefix << "//+++ "
                                        << n->typeStr();
      if (ud_node) {            // If there's getLoc()...
        UtString str;
        ud_node->getLoc().compose(&str);
        gCodeGen->getCGOFiles()->CGOUT () << ' ' << str;
      }
      gCodeGen->getCGOFiles()->CGOUT () << suffix;
      // Done generating comments.
    }
#endif // BUCKET_COMMENTS

    // Start a new bucket, if different from the current bucket.
    // Only NUUseDefNode objects have getLoc(), so we can only start
    // buckets for them.
    if (ud_node)
      gCodeGen->getCGProfile()->emitStartBucket(false, ud_node->getLoc());
    
    gCodeGen->getCGOFiles()->CGOUT () << prefix;
    n->emitCode (ctxt | eCGList);
    gCodeGen->getCGOFiles()->CGOUT () << suffix;
  }
};

//!Templated formatted list printing algorithm
template <typename T> void emitCodeList (T* container,
					 CGContext_t context,
					 const char *prefix,
					 const char *suffix)
{
  std::for_each (container->begin (), container->end (),
                 PrePost (prefix, suffix, context));
}

#endif

