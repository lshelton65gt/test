// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef __TESTDRIVER_H
#define __TESTDRIVER_H

#include "compiler_driver/CarbonContext.h"
#include "iodb/IODBUserTypes.h"

class NUNet;
class NUNetElab;
class NUModule;
class IODBNucleus;

typedef UtArray<STSymbolTableNode*> NodeVec;

class VhdlPortTypeMap;
class UtRandomTestPatternGen;

class TestDriverData {
  public:
    TestDriverData(IODBNucleus*     iodb, 
                   bool             useIODB, 
                   UInt32           vector_count, 
                   SInt32           vector_seed,
                   UInt32           reset_length, 
                   UInt32           clock_delay, 
                   bool             debug, 
                   bool             dump_vcd, 
                   bool             dump_fsdb, 
                   bool             glitchDetect,
                   SInt8            timeUnit, 
                   SInt8            timePrecision, 
                   bool             timescaleSpecified, 
                   bool             checkpoint, 
                   bool             skipObservables,
                   VhdlPortTypeMap* portTypeMap);

  void addModule(NUModule *m);
  NUModule * getModule(void) const;

  UInt32 getTotalNumInputs() const;

  bool useIODB() const;
  void addInput(NUNet *n);
  const NUNetVector & getInputs(void) const;
  bool hasInputs(void) const;
  void addClock(NUNet *n);
  const NUNetVector & getClocks(void) const;
  bool hasClocks(void) const;
  void addReset(NUNet *n);
  const NUNetVector & getResets(void) const;
  bool hasResets(void) const;
  void addOutput(NUNet *n);
  const NUNetVector & getOutputs(void) const;
  bool hasOutputs(void) const;
  void addBid(NUNet *n);
  const NUNetVector & getBids(void) const;
  bool hasBids(void) const;
  void addDeposit(STSymbolTableNode *n);
  const NodeVec & getDeposits(void) const;
  bool hasDeposits(void) const;
  void addObserved(STSymbolTableNode *n);
  const NodeVec & getObserveds(void) const;
  bool hasObserveds(void) const;

  void setResetHigh(bool reset_high);
  bool getResetHigh(void) const;

  void setVectorCount (const UInt32);
  UInt32 getVectorCount(void) const;
  UInt32 getVectorWidth(void) const;

  SInt32 getVectorSeed(void) const;
  UInt32 getResetLength(void) const;

  UInt32 getClockDelay(void) const;

  bool getDebug(void) const;
  bool getDumpVCD(void) const;
  bool getDumpFSDB(void) const;
  bool getGlitchDetect(void) const;

  bool getCheckpoint(void) const;
  bool getSkipObservables(void) const;

  void dump(UtOStream &out);

  //! Checking for empty top-level module here
  bool hasPorts() const;

  //! Did the top-level module of the model have a `timescale specified?
  bool getTimescaleSpecified() const { return mTimescaleSpecified; }

  //! Get the timescale time unit
  SInt8 getTimeUnit() const { return mTimeUnit; }

  //! Get the timescale time precision
  SInt8 getTimePrecision() const { return mTimePrecision; }

 private:
  TestDriverData(void);

 private:
  NUModule *mModule;
  NUNetVector mInputs;
  NUNetVector mClocks;
  NUNetVector mResets;
  NUNetVector mOutputs;
  NUNetVector mBids;

  NodeVec mObserveds;
  NodeVec mDeposits;

  IODBNucleus *mIODB;
  UInt32 mVectorCount;
  SInt32 mVectorSeed;
  UInt32 mResetLength;
  UInt32 mClockDelay;
  VhdlPortTypeMap* mPortTypeMap;
  bool mDebug;
  bool mDumpVCD;
  bool mDumpFSDB; // Dumping of fsdb from C++/VHDL test driver enabled?
  SInt8 mTimeUnit;
  SInt8 mTimePrecision;
  bool mTimescaleSpecified;
  bool mResetHigh;
  bool mUseIODB;
  bool mGlitchDetect;
  bool mPositiveDelayForClock;
  bool mCheckpoint; // test checkpoint save/restore
  bool mSkipObservables;

  void accumulateWidth(const NUNetVector &ports, UInt32 &width) const;
  void accumulateWidth(const NodeVec &ports, UInt32 &width) const;
  void dump(UtOStream &out, const NUNetVector &ports, const UtString label);
  void dump(UtOStream &out, const NodeVec &ports, const UtString label);
};


class TestbenchEmitter {
 public:
  typedef UtHashSet<UtString> PortHash;

  TestbenchEmitter(TestDriverData *d, const UtString& designName);
  virtual ~TestbenchEmitter(void) {};

  //! Type of net name to emit
  enum EmitType {
    eEmitNormal,
    eEmitVisibility,
    eEmitDelay,
    eEmitData,
    eEmitEnable,
    eEmitTempData,
    eEmitTempEnable
  };
  enum portDir {
    ePortIn,
    ePortOut,
    ePortInOut
  };
  enum FuncType {
    eTBAccWidth,
    eTBMaxWidth,
    eTvHasConstraint,
    eTvMemoryDat,
    eCxxDeclInit,
    eCxxTestVectorLoad,
    eCxxTestVectorDeposit,
    eCxxOutputStream,
    eCxxTestVectorIncr,
    eCxxTestVectorRewind,
    eCxxTestVectorDestroy,
    eVhdlFormalPortDecl,
    eVhdlSignalDecl,
    eVhdlTestbenchReadList,
    eVhdlTestbenchDelayList,
    eVhdlTestbenchWriteList,
    eVhdlTestbenchEnableDriver,
    eVhdlPortMapDecl,
    eVerilogConcat,
    eVerilogMap
  };
  typedef struct {
    bool            isDecl;
    bool            isBid;
    bool            isElab1;
    bool            isElab2; 
    bool            getDebug; 
    UInt32          dataBitWidth; 
    UInt32          bitSize; 
    EmitType        emit1; 
    EmitType        emit2; 
    SInt32          count;
    UtString        prefix;
    portDir         direction;
    UtString        indent;
    UInt32*         index;
    SInt32*         column;
    PortHash*       pHash;
    const UserType* userType;
    bool            isVector;
    UtString*       buf;
    RandomValGen*   ranValGen;
    UInt32*         accWidth;
    UInt32*         maxWidth;
    bool*           hasConst;
    bool            initialize;
    bool*           first; // Used for comma control in Vlog concats
  } emitParams;

  void clearEmitParams(emitParams* p);
  void extractPostfix(EmitType  emit, 
                      UtString* postfix);
  void makeTmpCount(UtString* name, 
                    SInt32    count);
  void extractNUNetInfo(const NUNet* net, 
                        NodeVec*     nodeList);
  void extractSTNodeInfo(STSymbolTableNode* node, 
                         NodeVec*           nodeList);
  bool isArrayOfBits(UtOStream&      out, 
                     const UserType* nodeType);
  bool requiresConversionFunction(const UserType* ut);
  UInt32 getImplementedBitSize(const UserType* userType,
                               UInt32          bitSize);

  void emitMainCore(FuncType                funcType, 
                    UtOStream&              out, 
                    NodeVec::const_iterator s, 
                    NodeVec::const_iterator e, 
                    UtString*               varName, 
                    UtString*               nodeName, 
                    emitParams              p);
  void emitMainCoreArray(FuncType               funcType, 
                         UtOStream&             out, 
                         NodeVec::const_iterator n, 
                         NodeVec::const_iterator e, 
                         const UserType*         nodeType, 
                         UtString*               varName, 
                         UtString*               nodeName, 
                         STSymbolTableNode*      node, 
                         emitParams              p);
  void emitMainCoreRecursion(FuncType                funcType, 
                             UtOStream&              out, 
                             NodeVec::const_iterator n, 
                             NodeVec::const_iterator e, 
                             const UserType*         nodeType, 
                             UtString*               varName, 
                             UtString*               nodeName, 
                             STSymbolTableNode*      node, 
                             emitParams              p);
  UInt32 getAccWidth(UtOStream& out);
  void getAccWidth(UtOStream&         out,
                       const NUNetVector& ports, 
                       UInt32*            width);
  void getAccWidthElab(UtOStream&     out,
                           const NodeVec& ports, 
                           UInt32*        width);
  UInt32 getMaxWidth(UtOStream& out);

  virtual void hasConstraintCore(emitParams) {}
  virtual void emitMemoryDatCore(emitParams) {}
  virtual void emitMainCxxDeclInitCore(UtOStream&, 
                                       UtString*, 
                                       UtString*, 
                                       emitParams) {}
  virtual void emitMainCxxTestVectorLoadCore(UtOStream&, 
                                             UtString*, 
                                             emitParams) {}
  virtual void emitMainCxxTestVectorDepositCore(UtOStream&, 
                                                UtString*, 
                                                emitParams) {}
  virtual void emitMainCxxOutputStreamCore(UtOStream&, 
                                           UtString*, 
                                           emitParams) {}
  virtual void emitMainCxxTestVectorIncrCore(UtOStream&, 
                                             UtString*, 
                                             emitParams) {}
  virtual void emitMainCxxTestVectorRewindCore(UtOStream&, 
                                               UtString*, 
                                               emitParams) {}
  virtual void emitMainCxxTestVectorDestroyCore(UtOStream&, 
                                                UtString*, 
                                                emitParams) {}
  virtual void emitMainVhdlFormalPortDeclCore(UtOStream&, 
                                              UtString*,
                                              emitParams) {}
  virtual void emitMainVhdlSignalDeclCore(UtOStream&, 
                                          UtString*, 
                                          emitParams) {}
  virtual void emitMainVhdlTBReadListCore(UtOStream&,
                                          UtString*,
                                          emitParams) {}
  virtual void emitMainVhdlTBDelayListCore(UtOStream&,
                                           UtString*,
                                           emitParams) {}
  virtual void emitMainVhdlTBWriteListCore(UtOStream&,
                                           UtString*,
                                           emitParams) {}
  virtual void emitMainVhdlTBEnabledDriverCore(UtOStream&, 
                                               UtString*,
                                               emitParams) {}
  virtual void emitMainVhdlPortMapDeclCore(UtOStream&, 
                                           UtString*,
                                           emitParams) {}
  virtual void emitVerilogConcatCore(UtOStream&,
                                     UtString*,
                                     emitParams) {}
  virtual void emitVerilogMapCore(UtOStream&,
                                     UtString*,
                                     emitParams) {}

  static void makeHDLTempName(const NUNet* net, UtString *name, EmitType emit);
  static void makeTempName(const char* base_name, UtString* name, EmitType emit);
  static void makeDelayTempName(const char* base_name, UtString* name);
  static void makeVisibilityTempName(const char* base_name, UtString* name);
  static void makeEnableTempName(const char* base_name, UtString* name);
  static void makeDataTempName(const char* base_name, UtString* name);
  static void makeTempEnableTempName(const char* base_name, UtString* name);
  static void makeTempDataTempName(const char* base_name, UtString* name);

 protected:
  TestDriverData* mTestData;
  UtString        mTestMemoryFileName;

 private:
  TestbenchEmitter(void);
  void getAccWidthCore(emitParams p);
  void getMaxWidthCore(emitParams p);
  void getMaxWidth(UtOStream&         out,
                   const NUNetVector& ports, 
                   UInt32*            width);
  void getMaxWidthElab(UtOStream&     out,
                       const NodeVec& ports, 
                       UInt32*        width);
};


//! Emit elaborated names for use in the testbench
class TestbenchElabListEmitter
{
public:
  TestbenchElabListEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchElabListEmitter(void);
  virtual void emit(UtOStream &o, STSymbolTableNode *n);
  virtual void operator()(STSymbolTableNode *n);

private:
  UtOStream &mOut;
  bool &mFirst;
  TestbenchElabListEmitter(void);
};


//! Emit ports for elaborated nets for use in the testbench
/*!
 * These are used for both enable and data signals to control depositable
 * signals.
 *
 * We create unique names to use within the testbench to pass to the top level.
 */
class TestbenchElabPortListEmitter : public TestbenchElabListEmitter
{
public:
  TestbenchElabPortListEmitter(UtOStream &o, bool &first, UInt32 start_count=0);
  ~TestbenchElabPortListEmitter(void);
  void operator()(STSymbolTableNode *n);

protected:
  //! Current unique id to use for signal naming
  UInt32 mCount;

private:
  TestbenchElabPortListEmitter(void);
};


class TestbenchElabDataPortListEmitter : public TestbenchElabPortListEmitter
{
public:
  TestbenchElabDataPortListEmitter(UtOStream &o, bool &first);
  ~TestbenchElabDataPortListEmitter(void);
  void emit(UtOStream &out, STSymbolTableNode *n);

private:
  TestbenchElabDataPortListEmitter(void);
};


class TestbenchTempElabDataPortListEmitter : public TestbenchElabPortListEmitter
{
public:
  TestbenchTempElabDataPortListEmitter(UtOStream &o, bool &first);
  ~TestbenchTempElabDataPortListEmitter(void);
  void emit(UtOStream &out, STSymbolTableNode *n);

private:
  TestbenchTempElabDataPortListEmitter(void);
};


class TestbenchTempElabEnablePortListEmitter : public TestbenchElabPortListEmitter
{
public:
  TestbenchTempElabEnablePortListEmitter(UtOStream &o, bool &first);
  ~TestbenchTempElabEnablePortListEmitter(void);
  void emit(UtOStream &out, STSymbolTableNode *n);

private:
  TestbenchTempElabEnablePortListEmitter(void);
};


class TestbenchElabDataNamedPortListEmitter : public TestbenchElabPortListEmitter
{
public:
  TestbenchElabDataNamedPortListEmitter(UtOStream &o, bool &first);
  ~TestbenchElabDataNamedPortListEmitter(void);
  void emit(UtOStream &out, STSymbolTableNode *n);

private:
  TestbenchElabDataNamedPortListEmitter(void);
};


class TestbenchElabEnablePortListEmitter : public TestbenchElabPortListEmitter
{
public:
  TestbenchElabEnablePortListEmitter(UtOStream &o, bool &first);
  ~TestbenchElabEnablePortListEmitter(void);
  void emit(UtOStream &out, STSymbolTableNode *n);

private:
  TestbenchElabEnablePortListEmitter(void);
};


class TestbenchElabEnableNamedPortListEmitter : public TestbenchElabPortListEmitter
{
 public:
  TestbenchElabEnableNamedPortListEmitter(UtOStream &o, bool &first);
  ~TestbenchElabEnableNamedPortListEmitter(void);
  void emit(UtOStream &out, STSymbolTableNode *n);

 private:
  TestbenchElabEnableNamedPortListEmitter(void);
};


class TestbenchPortListEmitter
{
 public:
  TestbenchPortListEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchPortListEmitter(void);
  virtual void emit(UtOStream &o, NUNet *n);
  void operator()(NUNet *n);

protected:
  UtOStream &mOut;
 private:
  bool &mFirst;
  TestbenchPortListEmitter(void);
};


class TestbenchNamedPortListEmitter : public TestbenchPortListEmitter
{
 public:
  TestbenchNamedPortListEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchNamedPortListEmitter(void);
  virtual void emit(UtOStream &o, NUNet *n);

 private:
  TestbenchNamedPortListEmitter(void);
};


class TestbenchVhdlOutputEmitter
{
 public:
  TestbenchVhdlOutputEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchVhdlOutputEmitter(void);
  virtual void emit(UtOStream &o, NUNet *n);
  void operator()(NUNet *n);
 private:
  UtOStream &mOut;
  bool &mFirst;
  TestbenchVhdlOutputEmitter(void);
};


class TestbenchVhdlBidEnableEmitter : public TestbenchVhdlOutputEmitter
{
 public:
  TestbenchVhdlBidEnableEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchVhdlBidEnableEmitter(void);
  virtual void emit(UtOStream &o, NUNet *n);

 private:
  TestbenchVhdlBidEnableEmitter(void);
};


class TestbenchVhdlBidDataEmitter : public TestbenchVhdlOutputEmitter
{
 public:
  TestbenchVhdlBidDataEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchVhdlBidDataEmitter(void);
  virtual void emit(UtOStream &o, NUNet *n);

 private:
  TestbenchVhdlBidDataEmitter(void);
};


class TestbenchNamedBidPortListEmitter : public TestbenchPortListEmitter
{
 public:
  TestbenchNamedBidPortListEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchNamedBidPortListEmitter(void);
  virtual void emit(UtOStream &o, NUNet *n);

 private:
  TestbenchNamedBidPortListEmitter(void);
};


class TestbenchTempPortListEmitter : public TestbenchPortListEmitter
{
 public:
  TestbenchTempPortListEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchTempPortListEmitter(void);
  virtual void emit(UtOStream &o, NUNet *n);

 private:
  TestbenchTempPortListEmitter(void);
};


class TestbenchDataPortListEmitter : public TestbenchPortListEmitter
{
 public:
  TestbenchDataPortListEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchDataPortListEmitter(void);
  virtual void emit(UtOStream &o, NUNet *n);

 private:
  TestbenchDataPortListEmitter(void);
};


class TestbenchEnablePortListEmitter : public TestbenchPortListEmitter
{
 public:
  TestbenchEnablePortListEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchEnablePortListEmitter(void);
  virtual void emit(UtOStream &o, NUNet *n);

 private:
  TestbenchEnablePortListEmitter(void);
};


class TestbenchTempDataPortListEmitter : public TestbenchPortListEmitter
{
 public:
  TestbenchTempDataPortListEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchTempDataPortListEmitter(void);
  virtual void emit(UtOStream &o, NUNet *n);

 private:
  TestbenchTempDataPortListEmitter(void);
};


class TestbenchTempEnablePortListEmitter : public TestbenchPortListEmitter
{
 public:
  TestbenchTempEnablePortListEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchTempEnablePortListEmitter(void);
  virtual void emit(UtOStream &o, NUNet *n);

 private:
  TestbenchTempEnablePortListEmitter(void);
};


class TestbenchBidPortListEmitter : public TestbenchPortListEmitter
{
 public:
  TestbenchBidPortListEmitter(UtOStream &o, bool &first);
  virtual ~TestbenchBidPortListEmitter(void);
  virtual void emit(UtOStream &o, NUNet *n);

 private:
  TestbenchBidPortListEmitter(void);
};


class TestVectorEmitter : public TestbenchEmitter {
 public:
  TestVectorEmitter(MsgContext &, TestDriverData* d, 
                    const UtString& target_name);
  void emit(UtOStream& o);
  void copy(const char *vectorfile, UtOStream &, UInt32 *nlines);
  virtual void hasConstraintCore(emitParams p);
  virtual void emitMemoryDatCore(emitParams p);

 private:

  typedef UtArray<UtRandomTestPatternGen*> PatGenArray;

  MsgContext &mMsgContext;
  UtString mTargetName;

  bool hasConstraint(UtOStream& out);
  void hasConstraint(UtOStream&         out,
                     const NUNetVector& ports, 
                     bool*              hasConst);
  void hasConstraintElab(UtOStream&     out,
                         const NodeVec& ports,
                         bool*          hasConst);
  void emitMemoryDat(UtOStream&    out,
                     RandomValGen& ranValGen,
                     UtString*     buf);
  void emitMemoryDat(UtOStream&         out,
                     const NUNetVector& ports, 
                     RandomValGen&      valueGen,
                     UtString*          buf);
  void emitMemoryDatElab(UtOStream&     out,
                         const NodeVec& ports, 
                         RandomValGen&  valueGen,
                         UtString*      buf);
};


class CxxTestbenchEmitter : public TestbenchEmitter {
 public:
  CxxTestbenchEmitter(TestDriverData* d, 
                      const UtString& target_name, 
                      const UtString& interface_name,
                      const char*     topName, 
                      AtomicCache*    cache);
  void emit(UtOStream& o);

  virtual void emitMainCxxDeclInitCore(UtOStream& out, 
                                       UtString*  varName, 
                                       UtString*  nodeNameCxx, 
                                       emitParams p);
  virtual void emitMainCxxTestVectorLoadCore(UtOStream& out, 
                                             UtString*  varName, 
                                             emitParams p);
  virtual void emitMainCxxTestVectorDepositCore(UtOStream& out, 
                                                UtString*  varName, 
                                                emitParams p);
  virtual void emitMainCxxOutputStreamCore(UtOStream& out, 
                                           UtString*  varName, 
                                           emitParams p);
  virtual void emitMainCxxTestVectorIncrCore(UtOStream& out, 
                                             UtString*  varName, 
                                             emitParams p);
  virtual void emitMainCxxTestVectorRewindCore(UtOStream& out, 
                                               UtString*  varName, 
                                               emitParams p);
  virtual void emitMainCxxTestVectorDestroyCore(UtOStream& out, 
                                                UtString*  varName, 
                                                emitParams p);

 private:
  UtString    mTargetName;
  UtString    mInterfaceTag;
  const char* mTopName;
  CodeNames   mCodeNames;

  void emitMainCxxConditionalDeposit(UtOStream& out);
  void emitMainCxxDeclInit(UtOStream& out, 
                           bool       isDecl);
  void emitMainCxxDeclInit(UtOStream&         out, 
                           const NUNetVector& ports, 
                           bool               isDecl);
  void emitMainCxxDeclInitElab(UtOStream&     out, 
                               const NodeVec& ports, 
                               bool           isDecl, 
                               UInt32         start_count);
  void emitMainCxxTestVectorLoad(UtOStream& out, 
                                 UInt32     dataBitWidth);
  void emitMainCxxTestVectorLoad(UtOStream&         out, 
                                 const NUNetVector& ports, 
                                 UInt32*            index, 
                                 UInt32             dataBitWidth, 
                                 bool               isBid,
                                 EmitType           emit);
  void emitMainCxxTestVectorLoadElab(UtOStream&     out, 
                                     const NodeVec& ports, 
                                     UInt32*        index, 
                                     UInt32         dataBitWidth, 
                                     EmitType       emit);
  void emitMainCxxTestVectorDeposit(UtOStream& out);
  void emitMainCxxTestVectorDeposit(UtOStream&         out, 
                                    const NUNetVector& ports, 
                                    bool               isBid,
                                    EmitType           emit1, 
                                    EmitType           emit2);
  void emitMainCxxTestVectorDepositElab(UtOStream&     out, 
                                        const NodeVec& ports, 
                                        EmitType       emit1, 
                                        EmitType       emit2);
  void emitMainCxxOutputStream(UtOStream& out);
  void emitMainCxxOutputStream(UtOStream&         out, 
                               const NUNetVector& ports, 
                               bool               isBid,
                               EmitType           emit);
  void emitMainCxxOutputStreamElab(UtOStream&     out, 
                                   const NodeVec& ports, 
                                   EmitType       emit);
  void emitMainCxxOutputStreamElab2(UtOStream&     out, 
                                    const NodeVec& ports, 
                                    UInt32         start_count);
  void emitMainCxxTestVectorIncr(UtOStream& out);
  void emitMainCxxTestVectorIncr(UtOStream&         out, 
                                 const NUNetVector& ports, 
                                 EmitType           emit);
  void emitMainCxxTestVectorIncrElab(UtOStream&     out, 
                                     const NodeVec& ports, 
                                     EmitType       emit);
  void emitMainCxxTestVectorRewind(UtOStream& out);
  void emitMainCxxTestVectorRewind(UtOStream&         out, 
                                   const NUNetVector& ports, 
                                   bool               isBid,
                                   EmitType           emit);
  void emitMainCxxTestVectorRewindElab(UtOStream&     out, 
                                       const NodeVec& ports, 
                                       EmitType       emit);
  void emitMainCxxTestVectorDestroy(UtOStream& out);
  void emitMainCxxTestVectorDestroy(UtOStream&         out, 
                                    const NUNetVector& ports, 
                                    bool               isBid,
                                    EmitType           emit);
  void emitMainCxxTestVectorDestroyElab(UtOStream&     out, 
                                        const NodeVec& ports, 
                                        EmitType       emit);
};


class VerilogTestbenchEmitter : public TestbenchEmitter
{
 public:
  VerilogTestbenchEmitter(TestDriverData *d, const UtString &targetName, MsgContext *msgContext);

 public:
  void emit(UtOStream &o);
  void putMapFile(UtOStream& map_file);

 private:
  //VerilogTestbenchEmitter(void);

  void emitTopModule(UtOStream &o);
  void emitTopWireDefinitions(UtOStream &o, const NUNetVector & ports);
  void emitTopWireDefinitions(UtOStream &o, const NodeVec & ports);
  void emitTopWireDefinitionsBidi(UtOStream &out, const NUNetVector & ports);

  void emitTopEnabledDrivers(UtOStream &o, const NUNetVector & ports);
  void emitTopDepositDrivers(UtOStream &o, const NodeVec & ports);

  void emitTopNamedPortLists(UtOStream &o);
  void emitTestbenchNamedPortLists(UtOStream &o);

  void emitTestbenchModule(UtOStream &o);
  void emitTestbenchModuleName(UtOStream &o, NUModule *m);
  void emitTestbenchFormalPorts(UtOStream &o);
  void emitTestbenchFormalDeclarations(UtOStream &o);
  void emitTestbenchMemoryDeclarations(UtOStream &o);
  void emitTestbenchFormalOutputDeclarations(UtOStream &o, const NUNetVector & ports, bool delay);
  void emitTestbenchFormalInputDeclarations(UtOStream &o, const NUNetVector & ports);
  void emitTestbenchFormalBidDeclarations(UtOStream &o, const NUNetVector & ports, bool delay);
  void emitTestbenchFormalElabDeclarations(UtOStream &o, const NodeVec & ports, bool delay);
  void emitTestbenchAlwaysBlock(UtOStream &o);
  void emitTestbenchAlwaysBlockSensitivity(UtOStream &o, const NUNetVector & ports, bool &first);
  void emitTestbenchAlwaysBlockBody(UtOStream &o, const NUNetVector & ports);
  void emitTestbenchVCDBlock(UtOStream &out);
  void emitTestbenchInitialBlock(UtOStream &o);
  void emitTestbenchInitialConcatExpression1(UtOStream &o);
  void emitTestbenchInitialConcatExpression2(UtOStream &o, const NUNetVector & ports);
  void emitTestbenchInitialBidConcatExpression2(UtOStream &o, const NUNetVector & ports);
  void emitTestbenchInitialDepositConcatExpression2(UtOStream &o, const NodeVec & ports);
  void emitTestbenchInitialDisplayExpression(UtOStream &o);

  //! Emit concatenations of SystemVerilog input and output signals/variables
  //  with unpacked array loops unrolled.
  void emitVerilogConcat(UtOStream& out, const NUNetVector& ports, bool& first);
  void emitVerilogConcatElab(UtOStream& out, const NodeVec& ports, bool &first);
  virtual void emitVerilogConcatCore(UtOStream& out, UtString* buf, emitParams p);
  
  //! Emit packed/unpacked ranges for SystemVerilog NUVectors and
  //  NUMemoryNets
  void emitSystemVerilogPackedRanges(UtOStream& out, const NUNet* net);
  void emitSystemVerilogUnpackedRanges(UtOStream& out, const NUNet* net);

  // Give MapEmit access to private methods.
  friend class MapEmit;

  //! Emit SystemVerilog signal/variables, one per line, into the main.map
  //  file, with unpacked array loops unrolled.
  virtual void emitVerilogMap(UtOStream& out, const NUNet* net, SInt32& column);
  virtual void emitVerilogMapCore(UtOStream& out, UtString* buf, emitParams p);

  void emitFormalDeclaration(UtOStream &out, const NUNet * port, const UtString dec_type, EmitType emit);
  void emitFormalDeclaration(UtOStream &out, const STSymbolTableNode * port, const UtString dec_type, EmitType emit, UInt32 count);

  //! Emit the `timescale directive if needed
  void emitTimescale( UtOStream &out );

  UtString mTestMemoryName;
  UtString mTestMemoryIndexName;
  UtOStream* mMapFile;
  MsgContext *mMsgContext; // Temporarily added to enable error message reporting.
  bool mMultiDPackedArraysEncountered; // For suppressing multiple error messages
  
};


class VhdlTestbenchEmitter : public TestbenchEmitter {
 public:
  VhdlTestbenchEmitter(TestDriverData*        d, 
                       const UtString&        targetName, 
                       const VhdlPortTypeMap* portTypeMap);
  void emit(UtOStream& out);
  void putMapFile(UtOStream& map_file);
  virtual void emitMainVhdlFormalPortDeclCore(UtOStream& out, 
                                              UtString*  nodeNameVhdl, 
                                              emitParams p);
  virtual void emitMainVhdlSignalDeclCore(UtOStream& out, 
                                          UtString*  nodeNameVhdl, 
                                          emitParams p);
  virtual void emitMainVhdlTBReadListCore(UtOStream& out,
                                          UtString*  nodeNameVhdl,
                                          emitParams p);
  virtual void emitMainVhdlTBDelayListCore(UtOStream& out,
                                           UtString*  nodeNameVhdl,
                                           emitParams p);
  virtual void emitMainVhdlTBWriteListCore(UtOStream& out,
                                           UtString*  nodeNameVhdl,
                                           emitParams p);
  virtual void emitMainVhdlTBEnabledDriverCore(UtOStream& out, 
                                               UtString*  nodeNameVhdl,
                                               emitParams p);
  virtual void emitMainVhdlPortMapDeclCore(UtOStream& out, 
                                           UtString*  nodeNameVhdl,
                                           emitParams p);

 private:
  UtString               mTestMemoryName;
  UtOStream*             mMapFile;
  const VhdlPortTypeMap* mPortTypeMap;
  bool                   mFirstPortDecl;

  void getEntityName(NUModule* m, 
                     UtString* name);
  void getTBEntityName(NUModule* m, 
                       UtString* name);
  void getTypeStr(const UserType* userType, 
                  UtString        portName,
                  UtString*       typeStr,
                  bool*           ranged,
                  SInt32*         msb);
  bool isBuffer(UtString portName);
  void resolveArrayRep(UtString* portName);
  void extractPortName(UtString  nodeNameVhdl,
                       UtString* portName);
  void getConversionTypeName(const UserType* userType,
                             UtString*       conversionTypeName);
  bool isSingle(UInt32          bitSize, 
                const UserType* userType,
                bool            isVector);
  UtString initializeArray(const UserType* userType); 
  void emitMainVhdlTBConversionPackage(UtOStream& out);
  void emitMainVhdlTBEntity(UtOStream& out);
  void emitMainVhdlTBArch(UtOStream& out);
  void emitMainVhdlTopEntity(UtOStream& out);
  void emitMainVhdlTopArch(UtOStream& out);
  void emitMainVhdlFormalPortDecl(UtOStream&         out, 
                                  portDir            direction, 
                                  UtString           indent,
                                  const NUNetVector& ports,
                                  bool               initialize);
  void emitMainVhdlSignalDecl(UtOStream&         out, 
                              const NUNetVector& ports,
                              UtString           prefix,
                              EmitType           emit);
  void emitMainVhdlTBFormalPortDecl(UtOStream& out,
                                    UtString   indent);
  void emitMainVhdlDUTFormalPortDecl(UtOStream& out);
  void emitMainVhdlTBSignalDecl(UtOStream& out);
  void emitMainVhdlTopSignalDecl(UtOStream& out);
  void emitMainVhdlTBInitialBlock(UtOStream &out);
  void emitMainVhdlTBReadList(UtOStream& out, 
                              UInt32     offset);
  void emitMainVhdlTBReadList(UtOStream&         out,
                              const NUNetVector& ports,
                              UInt32*            index,
                              EmitType           emit1);
  void emitMainVhdlTBDelayList(UtOStream& out);
  void emitMainVhdlTBDelayList(UtOStream&         out,
                               const NUNetVector& ports,
                               EmitType           emit1,
                               EmitType           emit2);
  void emitMainVhdlTBWriteList(UtOStream& out);
  void emitMainVhdlTBWriteList(UtOStream&         out,
                               const NUNetVector& ports,
                               EmitType           emit,
                               SInt32*            column);
  void emitMainVhdlTBEnabledDriver(UtOStream& out);
  void emitMainVhdlTBEnabledDriver(UtOStream&         out,
                                   const NUNetVector& ports,
                                   EmitType           emit1,
                                   EmitType           emit2);
  void emitMainVhdlPortMapDecl(UtOStream& out);
  void emitMainVhdlPortMapDecl(UtOStream&         out,
                               const NUNetVector& ports);
};


// SystemC testbench emitter
class SCTestbenchEmitter : public TestbenchEmitter
{
 public:
  SCTestbenchEmitter(TestDriverData *d, const UtString &targetName, UtOStream& out);
  void SCemit();

 private:
  void SCemitAllSignals();
  void SCemitSignals( const NUNetVector & ports, const char* suffix = "" );
  const char* SCNetType( UInt32 bits );
  void SCemitInstance();
  void SCemitInstancePorts( const NUNetVector & ports );
  void SCemitVectorDisplay();
  void SCemitVectorDisplaySignals( const NUNetVector & ports, const char* suffix = "" );
  void SCemitVector();
  void SCemitVectorSlice( const NUNetVector & ports, UInt32* bit, const char* suffix = "" );
  
  UtOStream& mOut;
};


#endif
