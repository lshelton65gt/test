// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <cctype>
#include <errno.h>
#include <stdarg.h>

#include "codegen/CGOFiles.h"
#include "codegen/InducedFaultTable.h"
#include "emitUtil.h"
#include "util/OSWrapper.h"
#include "util/UtUniquify.h"
#include "util/CbuildMsgContext.h"
#include "util/UtOZStream.h"
#include "util/CodeStream.h"

CGOFiles::CGOFiles(CodeGen &codegen, InducedFaultTable *faults, MsgContext *msg)
   :
  mExtraHeaders(0),
  mClassHeaderMap(new ClassHeaderMap),
  mModuleOSNameMap (new ModuleOSNameMap),
  mClassFilenames(new UtUniquify),
  mScheduleSrc(NULL),
  mScheduleSpill(NULL),
  mScheduleSpillSize(0),
  mScheduleSpillLimit(0),
  mCurrentClassSrc(NULL),
  mCurrentClassHdr(NULL),
  mDriver(NULL),
  mCurrent(NULL),
  mIfaceHdr(NULL),
  mCodegen (codegen),
  mInducedFaults (faults),
  mMsgContext(msg)
{}
         
CGOFiles::~CGOFiles()
{
  delete mClassFilenames;
  delete mClassHeaderMap;
  delete mModuleOSNameMap;
  delete mExtraHeaders;
}


// Header guard is currently __<classfile-name>_ with '_' substituted for '.'
// NOTE here we SHOULD use a single '_' for the prefix and suffix, and we should lowercase the first character
//  (double underscore prefix, or underscrore followed by uppercase character is reserved for c++)
static void sGenHeaderGuard (UtString *guard)
{
  // transform the file name (note no upcase)
  for(size_t i=0; i<guard->size (); ++i)
    if ((*guard)[i] == '.')
      (*guard)[i] = '_';

  guard->insert (0, "__");
  guard->append ("_");
}

/*!
 * Open .h and .cxx files for module.  Output the preliminary declarations
 * and include-guards.
 * \params module - what we are generating the files for
 * \params header - generate the .h file instead of the .cxx file
 */
void CGOFiles::addClassFile (const NUModule *module, bool header, bool precompiled)
{
  const StringAtom *pClassName = emitUtil::mclass (module->getName ());
  const char* className = genModuleFilename(module);

  // Say what we're currently generating code or header for
  gCodeGen->putClass (pClassName);

  if (header)
    {
      // Create the feature test macro for the class's header file
      // along with other boilerplate.
      UtString classFeatureTest ("// ");
      UtString headerGuard ("c_");
      headerGuard << className;
      headerGuard << ".h";
      sGenHeaderGuard (&headerGuard); // Build canonical __<filename>_

      module->getLoc ().compose (&classFeatureTest);

      classFeatureTest += "\n#ifndef ";
      classFeatureTest << headerGuard << "\n#define "
                       << headerGuard << "\n";
      UtString classHdr;
      classHdr << "./c_" << className << ".h";
      mCurrentClassHdr = CGFileOpen (classHdr.c_str (), classFeatureTest.c_str(),
        gCodeGen->isObfuscating());
    }
  else
    {
      UtString classSrc;
      classSrc << "./c_" << className << ".cxx";

      mCurrentClassSrc = CGFileOpen (classSrc.c_str (), "", gCodeGen->isObfuscating());
      
      generateHeaderInclude (*mCurrentClassSrc,
                             precompiled ? gCodeGen->getTopModule () : module);
    }
}

void CGOFiles::closeClassFile (bool header)
{
  if (header)
    delete mCurrentClassHdr;	// Destructor closes the file
  else
    {
      *mCurrentClassSrc << "\n\n";
      delete mCurrentClassSrc;
      if (mCurrentClassSrc == mCurrent)
        mCurrent = NULL;
      mCurrentClassSrc = NULL;
    }

  // Indicate that we have no class in progress
  gCodeGen->putClass (0);
}

void CGOFiles::deleteModuleFilename (const NUModule* mod)
{
  const char * className = genModuleFilename (mod);
  UtString path ("./c_");
  path << className << ".cxx";

  UtString reason;
  if (OSUnlink (path.c_str (), &reason))
    mMsgContext->CGFileError ("Error deleting file", reason.c_str ());
#if 0
  else
    // Only for debugging...
    mMsgContext->CGDeletedUnusedFile (path.c_str ());
#endif
}

//! Change where CGOUT points to.
//\param context inherited attributes to pass down to subnodes
//\returns synthesized attributes describing code generated
UtOStream & CGOFiles::switchStream (CGContext_t context)
{

    if (eCGDeclare & context)
      mCurrent = mCurrentClassHdr;
    else if (eCGDefine & context)
      mCurrent = mCurrentClassSrc;
    else if (eCGSchedule & context)
      mCurrent = mScheduleSrc;
    else if (eCGIface & context)
      mCurrent = mIfaceHdr;
    else if (eCGMain & context)
      mCurrent = mDriver;
    else
      INFO_ASSERT(0, "Unexpected Codegen Output stream");

    return CGOUT ();
}


//! Open a .cxx file to which we can output functions.
UtOStream *CGOFiles::addFunctionFile (UInt32 file_num, bool precompiled)
{
  // Create a string with the number padded with zeroes, so that they come out
  // in alphabetical order.
  // This assumes there are less than 1M files.
  INFO_ASSERT (file_num < 1000000, "Can not handle more than 1M files");
  char num[7];
  sprintf(num, "%06d", file_num);

  // Open the file.
  UtString file_name;
  file_name << "./f_" << num << ".cxx";
  UtOStream *file_stream = CGFileOpen(file_name.c_str(), "", gCodeGen->isObfuscating());

  if (precompiled)
    generateHeaderInclude (*file_stream, gCodeGen->getTopModule ());

  // Definitions will go here.
  mCurrentClassSrc = file_stream;

  return file_stream;
}

// Output some boilerplate text to a file.
UtOStream *
CGOFiles::CGFileOpen (const char * path, const char *extra, bool isObfuscating, bool needCleartext)
{
  UtOStream *low_stream;
  if (needCleartext || !isObfuscating) {
    low_stream = new UtOFStream(path);
  } else {
    UtOZStream* UTOZSTREAM_ALLOC(tmpFh, path);
    low_stream = tmpFh;
  }
  
  if (low_stream->bad())
  {
    UtString errBuf;
    mMsgContext->CGFileError(path, OSGetLastErrmsg(&errBuf)); // does not return
    delete low_stream;
    return NULL;
  }

  // wrap the stream in a code stream
  UtOStream *high_stream;
  if (!mCodegen.annotateGeneratedCode ()) {
    // just return the low stream, we are not doing code annotations
    high_stream = low_stream;
  } else if (mCodegen.useIndentEngine ()) {
    high_stream = new IndentingCodeStream (path,
      mCodegen.getImplAnnotationManager (),
      mCodegen.getHdlAnnotationManager (),
      low_stream,
      mInducedFaults ? mInducedFaults->findMap (path) : NULL);
  } else {
    high_stream = new CodeStream (path,
      mCodegen.getImplAnnotationManager (),
      mCodegen.getHdlAnnotationManager (),
      low_stream,
      mInducedFaults ? mInducedFaults->findMap (path) : NULL);
  }

  UtString uid ("");
  gCodeGen->getUID (&uid);

  (*high_stream) << "/* " << uid <<  "*/\n\n";
  
  (*high_stream) << extra;

  // Make sure that the numbers we print are ISO C parsable.
  (*low_stream) << UtIO::iso_c;

  return high_stream;
}


void
CGOFiles::printExtraHeaders ()
{
  if (not mExtraHeaders)
    return;

  UtOStream& out = CGOUT ();

  for(ExtraHeadersSet::iterator header = mExtraHeaders->begin ();
      header != mExtraHeaders->end ();
      ++header)
    {
      UtString guard (**header);
      size_t slashIdx = guard.find ('/');
      if (slashIdx != UtString::npos)
	guard.erase (0, slashIdx+1);

      
      sGenHeaderGuard (&guard);

      // Exterior include guard to speed up parsing
      out << "#ifndef " << guard << "\n"
	  << "#include \"" << **header << "\"\n"
	  << "#endif" << ENDL;
    }

  // Note that we don't clear out the header set when we print it,
  // as when we code the prototypes and inline functions we should
  // also end up adding the same headers to the set...
}

void CGOFiles::addExtraHeader (const UtString& hdr)
{
  if (not mExtraHeaders)
    mExtraHeaders = new ExtraHeadersSet;

  if (mExtraHeaders->find (&hdr) == mExtraHeaders->end ())
    mExtraHeaders->insert (new UtString (hdr));
}

void CGOFiles::addExtraHeader (const NUModule *mod)
{
  UtString classHeader ("c_");
  classHeader << genModuleFilename (mod);
  classHeader << ".h";

  addExtraHeader (classHeader);
}

void CGOFiles::saveExtraHeaders (const NUModule *mod)
{
  (*mClassHeaderMap)[mod] = mExtraHeaders ? mExtraHeaders : new ExtraHeadersSet;
  mExtraHeaders = 0;
}

void CGOFiles::restoreExtraHeaders (const NUModule *mod)
{
  LOC_ASSERT (not mExtraHeaders, mod->getLoc ());

  mExtraHeaders = (*mClassHeaderMap)[mod];
}

void CGOFiles::flushExtraHeaders (const NUModule* mod)
{
  // Delete the map associated with this module
  ClassHeaderMap::const_iterator i = mClassHeaderMap->find (mod);

  if (i != mClassHeaderMap->end ())
  {
    // Check that if we didn't need any headers when we compiled the 
    // .cxx file, that we didn't suddenly discover that we needed
    // headers for the .h file.
    ExtraHeadersSet* headers = i->second;
    LOC_ASSERT (headers == mExtraHeaders, mod->getLoc ());

    if (mExtraHeaders) {
      // Free the header strings....
      for (ExtraHeadersSet::iterator h = headers->begin (); h != headers->end (); ++h)
        delete *h;
    }
                     
    mClassHeaderMap->erase (mod);
  }
  else
  {
    LOC_ASSERT (not mExtraHeaders, mod->getLoc ());
  }

  if (mExtraHeaders)
    mExtraHeaders->clear ();

  delete mExtraHeaders;
  mExtraHeaders = 0;
}
// Generate the header include for one module
void
CGOFiles::generateHeaderInclude (UtOStream & out, const NUModule *mod)
{
  UtString className ("c_");
  className << genModuleFilename(mod);
  className += ".h";

  UtString guard (className);
  sGenHeaderGuard (&guard);

  out << "#ifndef " << guard << "\n"
      << "#include \"" << className << "\"\n"
      << "#endif" << ENDL;
}

// Generate the header include files for a list of modules
void
CGOFiles::generateHeaderIncludes (UtOStream & out, const NUModuleList& module_list)
{
  for(NUModuleList::const_iterator inc = module_list.begin ();
      inc != module_list.end ();
      ++inc)
  {
    generateHeaderInclude(out, *inc);
  }
}


const char* CGOFiles::genModuleFilename(const NUModule* mod)
{
  const char* className = (*mModuleOSNameMap)[mod];
  if (className == NULL)
  {
    UtString buf;
    const StringAtom *pClassName = emitUtil::noprefix (mod->getName ());
    OSLegalizeFilename(pClassName->str(), &buf);

    for (size_t i = 0; i < buf.size(); ++i)
    {
      char c = buf[i];
      if (!isalnum(c) && (c != '_'))
      {
        buf[i] = '_';
      }
    }
    className = mClassFilenames->insert(buf.c_str());
    (*mModuleOSNameMap)[mod] = className;
  }
  return className;
}

bool CGOFiles::checkForSpill (int size)
{
  return ((size > gCodeGen->mFunLimit) // This is too big,
          || (mScheduleSpill          // we've started spilling things
              && (mScheduleSrc == mCurrent)) // we're writing into schedule.cxx now
          || (mScheduleSpillSize > mScheduleSpillLimit));  // We've put too much here
}
