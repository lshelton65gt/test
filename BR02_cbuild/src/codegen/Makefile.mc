# -*- Makefile -*-
#
# *****************************************************************************
# Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
# *****************************************************************************
#
# make file suitable for building the Carbon translation.
#
# Installed as ${CARBON_HOME}/include/codegen/Makefile.mc from
# src/codegen/Makefile.mc
#
# Should produce an archive or shared library for the translated
# Verilog, compile and run the symbol-table offset generator.
# 
# The carbon compiler will pass in some make macro values to allow
# fine-control of the compile-link steps....
#
# To run this manually, from within the .carbon directory,
#
#  make -f Makefile.mc TARGET=../libdesign.a CXX_EXTRA_FLAGS=-O3 [INLINED=yes] \
#                      COMPILER=[gcc2|gcc3|gcc4|spw|icc] [TOP_CLASS=classname]
#
# TARGET=qualified pathname for cbuild output.
#   can handle .a, .so, .lib or .dll extensions
#
# TOP_CLASS = XXXXX (identifies the root class for the simulation
#		     with files c_XXXXX.cxx and c_XXXXX.h)
#	
#

# Include compiler-dependent definitions (normally gcc, but should
# support icc and sparcworks among others)
#

######################################################################
#
# Figure out the library type to build and what flags are needed to
# keep the compiler happy.
#

include $(CARBON_HOME)/makefiles/Makefile.common

LIBPATH:=$(dir $(TARGET))

TARGETNAME:=$(notdir $(TARGET))
TARGETBASE:=$(basename $(TARGETNAME))

# '/path/libdesign' without an extension...
TARGETROOT:=$(basename $(TARGET))

LIBNAME=$(patsubst lib%,%,$(TARGETBASE))

#
# Get the list of c-model header files we have to install
#

CMODEL_HEADERS=$(wildcard cds_*.h)

# Link with shared libcarbon unless an internal feature that requires
# static libcarbon's C++ interface.  Possiblities are
# - BitVector performance metrics (-DBVPERF)
CARBON_LIBRARIES = $(if $(findstring -DBVPERF,${CXX_WC_FLAGS}),${CARBON_STATIC_LIB_LIST},${CARBON_LIB_LIST})

# Determine what kind of library we want (archive vs. shared)
#
DEFEXT=$(subst .,,$(suffix $(TARGETNAME)))

# Determine shared library extension for target arch
SOEXT_Win = dll
SOEXT_Linux = so
SOEXT_Linux64 = so
SOEXT = $(SOEXT_$(CARBON_TARGET_ARCH))

# Determine the proper extension for the archive
AREXT_Win = lib
AREXT_Linux = a
AREXT_Linux64 = a
AREXT = $(AREXT_$(CARBON_TARGET_ARCH))

# If we asked for libdesign.a, but we are a Windows target, generate
# libdesign.lib with a hard link to libdesign.a
#
HARDLINK=

ifeq ($(DEFEXT),a)
ifeq ($(CARBON_WINX),1)
DEFEXT:=lib
HARDLINK=1
endif
endif

# Use precompiled headers only if the compiler supports them (GCC3 or
# better) and TOP_CLASS is defined.
ifeq ($(CARBON_COMPILER_SUPPORTS_PCH),1)
ifneq (,$(TOP_CLASS))
TRY_PCH=1
ifneq (,$(wildcard c_${TOP_CLASS}.h.gch))
USE_PCH=1
endif
endif
endif


HEADERS = $(wildcard c_*.h) $(PCH_INCLUDE)
ifeq ($(USE_PCH),1)
SPEEDCC_FLAGS = -pch c_$(TOP_CLASS).h
else
SPEEDCC_FLAGS =
endif


ifeq ($(DEFEXT),$(AREXT))
CARBON_CXXFLAGS+=-static
else
ifeq ($(DEFEXT),$(SOEXT))
CARBON_CXXFLAGS+=$(CARBON_SHARED_COMPILE_FLAGS)
CARBON_LIBRARIES=$(if $(THREADED),${CARBON_SHARED_LIB_LIST_PTHREADS},${CARBON_SHARED_LIB_LIST})
else
$(error Extension ".$(DEFEXT)" in output target "$(TARGET)", must be either .$(SOEXT), or .$(AREXT))
endif
endif

# CARBON_PGO_FLAGS is set by cbuild for -profileGenerate and -profileUse.
CXX_WC_FLAGS += $(CARBON_PGO_FLAGS)

# Allow model to output a message when compiled with -profileGenerate.
# We do that by generating a .h file that defines a C++ macro
# PROFILE_GENERATE appropriately.  (Using gcc option
# -DPROFILE_GENERATE upsets precompiled headers when recompiling
# without it for -profileUse.)
ifeq ($(findstring -fprofile-generate,$(CXX_WC_FLAGS)),-fprofile-generate)
  PROFILE_GENERATE = 1
  CARBON_CXX_FLAGS += -frandom-seed='\$(*F)'
else
  PROFILE_GENERATE = 0
endif

# put the escaped pound character into a variable so that it is hidden from backslash
# processing done by shell/echo (different between RHEL5 and eariler OSs)
POUNDCHAR=\#
$(shell echo "$(POUNDCHAR)define PROFILE_GENERATE $(PROFILE_GENERATE)" > profile_generate.h)

## Previously we set MCSPEEDCC and LINK to start with $(CXX_NICE),
## which was conditionally set by Makefile.common.  This was to
## prevent the multiple g++ processes invoked by one cbuild from
## starving another cbuild process.  Now that CARBON_COMPILE and
## CARBON_LINK contain multiple commands separated by semicolons,
## prepending `nice' to the string of commands is a problem.  We think
## nice is unnecessary, so it's removed.

ifdef PCH_INCLUDE
SPEEDCC_PCH_ARGS = -pch $(PCH_INCLUDE)
endif

## Use speedcc ....
MCSPEEDCC = $(CARBON_COMPILE) $(CARBON_CXX_SPEEDCC_COMPILE_FLAGS)

ifndef CARBON_NO_SPEEDCC
  # Use SpeedCC for compilation
  MCCOMPILE = $(MCSPEEDCC) $(SPEEDCC_PCH_ARGS) $(SPEEDCC_FLAGS)
else
  ifndef CARBON_TOP
    $(error "CARBON_TOP must be defined when compiling without SpeedCC")
  endif
  ifndef CARBON_TARGET
    $(error "CARBON_TARGET must be defined when compiling without SpeedCC")
  endif
  # Compilation without SpeedCC requires a development sandbox.
  MCCOMPILE = $(CARBON_CXX_COMPILE) -I$(CARBON_TOP)/src/inc -I$(CARBON_HOME)/obj/$(CARBON_TARGET)
endif

## Normal link with env COMPILER_PATH if needed.  Use CARBON_CC_LINK to
## avoid painful linking issues, especially with shared libraries
LINK=$(CARBON_CC_LINK) $(CARBON_CXX_DEF_LINK_FLAGS)

SOURCES=schedule.cxx $(wildcard sch-*.cxx) $(wildcard f_*.cxx) \
	$(wildcard cmodel_*.cxx) \
	$(wildcard pli_*.cxx) $(wildcard c_*.cxx) \
	$(wildcard lib*.db.cxx)

CARBON_LINKSCRIPT := $(CARBON_HOME)/makefiles/LINK.script

# If we are doing maximal inlining, passed in INLINED='yes' make variable
#
ifeq ($(INLINED),yes)
  OBJECTS=inlined.o
else
  OBJECTS=$(CARBON_LINKSCRIPT:c:/cygwin%=%) $(SOURCES:.cxx=.o)
endif

# When block profiling (-profile), we need to define a C++ macro so
# generated profiling code compiles.
ifeq ($(PROFILING),yes)
  CARBON_CXXFLAGS += -DPROFILING
endif

TARGETS= $(TARGETROOT).$(DEFEXT) $(TARGETROOT).h 

ifeq ($(LINK_TESTDRIVER),yes)
  TARGETS += $(LIBPATH)$(LIBNAME).exe $(TARGETROOT).test.vectors
else
ifeq ($(BUILD_TESTDRIVER),yes)
  TARGETS += main.o $(TARGETROOT).test.vectors
endif
endif

TARGETS+= $(addprefix ${LIBPATH},${CMODEL_HEADERS})

# By default we produce whatever TARGET that "cbuild -o" requested
#
all : $(TARGETS)

AROBJECTS := $(filter-out $(CARBON_LINKSCRIPT:c:/cygwin%=%),$(OBJECTS))
$(TARGETROOT).$(AREXT) : $(AROBJECTS)
	@- rm -f $(TARGETROOT).a $(TARGETROOT).lib $(TARGETROOT).so $(TARGETROOT).dll 
	$(CARBON_AR) cr "$@" $(AROBJECTS)
	$(CARBON_RANLIB) "$@"
	@ $(if $(HARDLINK),${CARBON_LN_S} -f $(TARGETROOT).lib $(TARGETROOT).a)

# Generate assembly for all .cxx files
assembly : $(SOURCES:.cxx=.s)

## Note that we don't build the SO with any other carbon libraries.
## Let them be resolved when we link this into an executable where
## the libraries might be shared or they might be static.
##
$(TARGETROOT).so : $(OBJECTS)
	$(LINK) -o $@  -shared $(OBJECTS) $(CARBON_SHARED_LINK_FLAGS) $(ARCH_LIBS)

# A DLL also produces a definitions list.  For libdesign.dll, we have
# libdesign.def.  This can be used to produce an import library on Windows
# using the LIB tool.
#
# We *do* need the shared libraries when linking DLLS,
# as no undefined symbols are permitted in a DLL.
#
$(TARGETROOT).dll : $(OBJECTS)
	@- rm -f $(TARGETROOT).a $(TARGETROOT).lib $(TARGETROOT).so
	$(LINK) -o $@ $(OBJECTS) $(CARBON_SHARED_LINK_FLAGS) $(CARBON_LIBRARIES)
	@$(if $(HARDLINK),${CARBON_LN_S} -f $(TARGETROOT)-mingw-implib.lib $(TARGETROOT).a)
	/usr/bin/perl -p -i -e 'last if /^IMPORTS/;' $(@:.dll=.def)


$(TARGETROOT).h : $(TARGETBASE).h
ifeq ($(CARBON_HOST_ARCH),Win)
	cp $< `cygpath -m "$@"`
else
	cp $< $@
endif

${LIBPATH}cds_%.h : cds_%.h
	cp $< $@


# Generate a config script for CoWare
coware: $(TARGETROOT).coware.cmd 

$(TARGETROOT).coware.cmd :
	@echo "cwr_append_simbld_opts preprocessor -I${CARBON_HOME}/include" > $@
	@echo "cwr_append_simbld_opts linker $(TARGETROOT).$(AREXT) ${CARBON_LIB_LIST_NOMEM}" >> $@

# Building a test driver requires access to the library (either shared or static). If
# building a Windows test driver, we have to have a .a extension to access the library
#

# When using the mingw linker to link static libraries -lname does not
# locate libraries with a ".lib" suffix. For these, just hit it with the
# full pathname of the library file.
ifeq ("$(DEFEXT)","lib")
  LIB_LINK_STRING = "$(LIBPATH)lib$(LIBNAME).$(DEFEXT)"
else
  LIB_LINK_STRING = -L"$(LIBPATH)" -l$(LIBNAME) 
endif

# Testdriver does not need the -lnffw library. No fsdb option. When 
# compiling with shared libraries, this seems to matter to the linker.
# -lm is needed for log, which is needed for $random support functions.
# Link with CARBON_CC_LINK rather than CARBON_LINK to ensure that
# DesignPlayer continues to link without C++ linker support, to ensure
# our code remains independent of the user's choice of C++.
$(LIBPATH)$(LIBNAME).exe: main.o $(TARGETROOT).$(DEFEXT)
	$(CARBON_CC_LINK) -o "$@" main.o $(CARBON_LINKSCRIPT) -Wl,-rpath,$(@D) $(LIB_LINK_STRING) $(filter-out -lnffw,${CARBON_LIBRARIES}) -lm

# Export the test-data so that testdriver can find it.
# cbuild creates memory.dat, but the testdriver wants to read $(LIBNAME).dat
# in the same directory as the executable lives.
$(TARGETROOT).test.vectors: memory.dat
	cp $< "$@"

# Maximal inlining is achieved by compiling all the sources as a single
# file so that every function body is visible to the optimizer.
#
inlined.cxx : $(SOURCES)
	- rm -f $@
	env MAKEFLAGS= $(MAKE) -s $(SOURCES:.cxx=.inline) > $@

%.inline:
	echo \#include \"$(@:.inline=.cxx)\"

ifdef CARBON_NO_SPEEDCC
define do_mcccompile
	$(strip $(MCCOMPILE) -o $@ $<)
endef
else
define do_mcccompile
	$(strip $(MCCOMPILE) -o $@ -pchBase c_$(TOP_CLASS).h $<)
endef
endif

## Schedule sch-XXX files depend on all the headers and shouldn't run in
## parallel with schedule.cxx
schedule.o : schedule.cxx $(HEADERS) $(TARGETBASE).h
	$(do_mcccompile)

# if you are trying to hand-recompile schedule.cxx for debugging,
# then it's annoying to force a recompile of all the other files, so
# set NO_SCHED_DEP=1
ifeq ($(NO_SCHED_DEP),)
SCHED_DEP = schedule.o
endif

sch-%.o : sch-%.cxx $(HEADERS) $(TARGETBASE).h $(SCHED_DEP)
	$(do_mcccompile)

f_%.o :	f_%.cxx $(HEADERS) $(SCHED_DEP)
	$(do_mcccompile)

c_%.o : c_%.cxx c_%.h $(SCHED_DEP)
	$(do_mcccompile)

cmodel_%.o : cmodel_%.cxx $(HEADERS) $(SCHED_DEP)
	$(do_mcccompile)

pli_%.o : pli_%.cxx $(HEADERS)
	$(MCCOMPILE) -o $@ $<

lib%.db.o : lib%.db.cxx $(HEADERS)
	$(do_mcccompile)

ifdef CARBON_TESTDRIVER_NO_SPEEDCC
main.o : main.cxx $(TARGETBASE).h
	$(CARBON_CXX_COMPILE) ${CARBON_CXXFLAGS} -o $@ $<
else
main.o: SPEEDCC_PCH_ARGS =
main.o: SPEEDCC_FLAGS += -DCARBON_SPEEDCC
main.o : main.cxx $(TARGETBASE).h
	$(do_mcccompile)
endif

inlined.o : inlined.cxx $(HEADERS)
	$(MCCOMPILE) -o $@ $<

%.s : %.cxx
	$(MCCOMPILE) -S -g -Wa,-adhls -o $@.lst $<
	$(MCCOMPILE) -S -o $@ $<


ifeq (1,$(TRY_PCH))
PCH-target : .stamp.PCH-target

# Note: if you change the "#error PCH-failed" string to anything else
# then you *must* change the same string in SpeedCC.cxx.
.stamp.PCH-target : $(PCH_INCLUDE).gch
$(PCH_INCLUDE) $(PCH_INCLUDE).gch: c_$(TOP_CLASS).h
	if ( ${MCSPEEDCC} -x c++-header -o $(PCH_INCLUDE).gch $< ); then \
	  echo '#error PCH-failed' > $(PCH_INCLUDE) ; \
	  echo "/* precompiled header build from c_$(TOP_CLASS).h */" >> $(PCH_INCLUDE); \
	  touch .stamp.PCH-target; \
        else \
          exit; \
	fi

else
PCH-target :
	@ true
endif

clean::
	-rm -rf *.o $(TARGETROOT).$(AREXT) $(TARGETROOT).$(SOEXT) \
		$(TARGETROOT).h $(LIBPATH)$(LIBNAME).exe \
		$(TARGETROOT).dll.lib c_$(TOP_CLASS).h.gch *.gcda *.gcno \
	        .stamp.PCH-target 

ifdef PCH_INCLUDE
clean::
	-rm -f $(PCH_INCLUDE) $(PCH_INCLUDE).gch
endif

clean-obj:
	-rm -rf *.o

.PHONY : all $(TARGETROOT).test.vectors PCH-target

# This target causes anything that dies with ignored-errors to delete
# the target file.  This should cure leaving around a zero-length library
# or .o file when the compiler aborts or segfaults.
#
.DELETE_ON_ERROR:
