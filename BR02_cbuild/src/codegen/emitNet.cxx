// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*
 * \file
 * Centralized implementation of all code emitters for classes derived from
 * class NUBase.
 *
 * We emit code by passing down context flags that guide each method
 * in establishing what kinds of code are needed (declarations,
 * expressions, or various kinds of names. 
 *
 * Context flags are returned for those places that need to be aware of
 * the generated context for a subordinate tree.  (See the NUAssign and
 * NUIdentLvalue emitCode() methods for an example.
 *
 * A very few routines do complex analysis of their operands or are
 * aware of the existance of multiple output files (see NUModule::emitCode())
 * 
 */
#include "emitUtil.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUBitNetHierRef.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUVectorNetHierRef.h"
#include "util/CbuildMsgContext.h"
#include "util/ArgProc.h"
#include "util/StringAtom.h"
#include "compiler_driver/CarbonContext.h"
#include "codegen/CGAuxInfo.h"
#include "reduce/Fold.h"
#include "backend/Stripper.h"

// Bug in GCC on linux causing ICE: unrecognized insn
// (probably fixed in 4.1.1....)
#define GCC_BUG_26662 (pfLINUX && pfGCC_4_1_0)

using namespace emitUtil;

/*!
 * Generate a dummy name of the form:
 *	T x;
 * or
 *	T x[n];
 */
static void
genCdecl (UInt32 nbits, const char *member)
{
  UtOStream& out = gCodeGen->CGOUT ();

  out << "  " << TAG << CBaseTypeFmt (nbits, eCGIface) << " " << member;

  if (nbits > LLONG_BIT)
    // Need a vector
    out << "[" << (nbits + (LONG_BIT-1))/LONG_BIT << "]";

  out << ";\n";
}

// Primary bidirects generate a structure holding a value, an external drive
// mask and an internal drive mask.
//
static void
genBidStruct (const NUNet* net)
{
  size_t nbits = net->getBitSize ();

  UtOStream& out = gCodeGen->CGOUT ();

  out << TAG << "struct {\n  ";
  genCdecl (nbits, "value");
  genCdecl (nbits, "xdrive");
  genCdecl (nbits, "idrive");
  out << "} " << member (net->getName ());
}

/*!
 * Can a field of this position and size be assigned to in a SET_SLICE
 * operation?  There are restrictions on alignment and word size that
 * must be met (unaligned UInt64 would crash a SPARC).
 */
static bool
canSetSlice (SInt32 pos, size_t size, size_t *modulus)
{
  // Disallow negative positions, bug 12900
  if (pos < 0) {
    return false;
  }

  // For now just use simple common cases...

  if (((pos % LONG_BIT) + size) <= LONG_BIT)
    // Doesn't cross a word boundary...
    {
      *modulus = LONG_BIT;
      return true;
    }

  // SPARC won't allow us to do 64-bit accesses into
  // an array of 32 bit values, so only do the following on X86
  // HW.

  if ( ((pos % LONG_BIT) + size) > LONG_BIT)
    {
      *modulus = LLONG_BIT;
      return true;
    }

  return false;
}

// Can we determine that an expression will always be a positive value?
static bool
sIsPositive (const NUExpr* expr)
{
  if (not expr->isSignedResult ())
    return true;

  if (expr->getBitSize () > expr->effectiveBitSize ())
    return true;

  return false;
}

// Return true if the indexing expression is inside the object bounds.
// Called with:
// \a net - (optional) net being indexed into
// \a index - indexing expression (or constant)
// \a bounds - the ConstantRange limits that index should be constrained to
// \a loc - relevant source locator to report on errors
// \a warn - print a warning when true AND -no-OOB AND index is out of bounds.
//
static bool
sIsIndexInBounds (const NUNet* net, const NUExpr* index, const ConstantRange& bounds,
                  const SourceLocator loc, bool warn=true)
{
  const NUConst* cindex;
  if (index && (cindex = index->castConst ())) {
    // We don't care if this was XZ or simple constant - we're codegen-ing
    // an array bounds value so XZ's are ignorable now.
    SInt32 value;
    if (cindex->getL (&value) && bounds.contains (value))
      return true;
    else {
      if (warn && gCodeGen->mCheckOOB) {
        UtString buf ("");
        if (net)
          net->composeUnelaboratedName (&buf);
        gCodeGen->getMsgContext ()->CGOutOfBounds (&loc, buf.c_str ());
      }
      return false;
    }
  }
  
  if (gCodeGen->mNoOOB || not index)
    return true;                // ignore any bounds issues.

  UInt32 width = index->effectiveBitSize ();
  if (width > 16)
    return false;               // BitVector limited to 65k bits, could be OOB

  // Signed expressions are tough - only in very limited cases could
  // we be okay.
  if (not sIsPositive (index)) {
    ConstantRange indexBounds (-(1<<width), (1<<width)-1);
    return bounds.contains (indexBounds);
  }

  return bounds.contains ((1<<width)-1);
}

// static function
bool NUNet::isCodegenBidirect(const NUNet* net)
{
  return gCodeGen->isBidirect(net);
}


CGContext_t
NUBitNet::emitCode (CGContext_t context) const
{
  context &= ~eCGFlow;

  return NUNet::emitCode (context);
}

//! NET larger than one bit (but not a memory...)
CGContext_t
NUVectorNet::emitCode (CGContext_t context) const
{
  CGContext_t rc = NUNet::emitCode (context);

  if (getBitSize () > LLONG_BIT)
    rc |= eCGHuge;

  return rc;
}

// Shared utility code used for NUMemoryNet, NUTempMemoryNet and NUNet.
// 
// If this returns eCGVoid as a context, we are finished CodeGen-ing
// early.
static CGContext_t sCommonNetCodeGen (const NUNet* n, CGContext_t context)
{
  context &= ~eCGFlow;

  // Make sure that we setup a CGAuxInfo record for this net.  We'll track
  // various interesting facts about the net here.
  //
  CGAuxInfo* cgop = n->getCGOp ();
  if (cgop == NULL) {
    cgop = new CGAuxInfo;
    n->setCGOp (cgop);
  }

  if (isMarkNets (context))
    // This net is going to be emitted in a context that implies that it's really
    // used (and thus should be declared, not deleted as dead code)
    cgop->setReferenced ();

  if (not cgop->isUnreferenced () || isVoid (context)
      || (isDeclare (context) && n->isNonStatic () ))
    // 
    emitUtil::checkNetIncludes (n);

  if (isConstructor (context) && isDeclare (context))
    // Declaring the constructor formals...
    {
      if (!cgop->isConstructed ())
	return eCGVoid;		// Not constructed, ignore it
      
      UtOStream& out = gCodeGen->CGOUT ();
      out << TAG << CType (n);
      out << emitUtil::netConstPrefix(n);
      out << (isDereferenced (n) ? "& ": "  ");

      if (cgop->isUnreferenced ())
	out << "/* " << formal (n) << " */";
      else
	{
	  out << formal (n);
	  cgop->setFormal ();
	}

      return eCGVoid;           // Completed codegen-ing this
    }

  // We didn't do all the codegen required for this context, so just
  // return the passed in context.
  return context;
}

//! Memories
/*!
 * We \em really want to use the logic in NUNet::emitCode()
 * to do as much work as possible.  That's where CGAuxInfo is
 * filled in.
 */
CGContext_t
NUMemoryNet::emitCode (CGContext_t context) const
{
  context = sCommonNetCodeGen (this, context);
  if (isVoid (context))
    return eCGVoid;
  
#if 0
  if (&gCodeGen->CGOUT () != NULL) {
    UtOStream &out = gCodeGen->CGOUT ();
    out << "/* NUMemoryNet: " << __LINE__ << "*/";
  }
#endif

  if (isDeclare (context) && !isPort () && !isReference (context)) {
    // declare the memory here

    UtOStream &out = gCodeGen->CGOUT ();
    if (getCGOp ()->isUnreferenced () && not isNonStatic ()) {
      // Never used and it's static, so we'd know!
      out << "// " << member (getName ()) << ENDL;
      return eCGVoid;
    }

    if ((isBlockLocal () and isAliased ())
        || getCGOp ()->needsAccessor ())
      generateAccessorFunction (this);

    generateMemberVariable (this, false);

    return eCGVoid;

  } else {
    // Just let NUNet handle non-declaration references to the memory
    return NUNet::emitCode (context);
  }
}

//! TempMemories
/*!
 * We \em really want to use the logic in NUNet::emitCode()
 * to do as much work as possible.  That's where CGAuxInfo is
 * filled in.
 */
CGContext_t
NUTempMemoryNet::emitCode (CGContext_t context) const
{
  context = sCommonNetCodeGen (this, context);
  if (isVoid (context))
    return eCGVoid;

#if 0
  if (&gCodeGen->CGOUT () != NULL) {
    UtOStream &out = gCodeGen->CGOUT ();
    out << "/* NUTempMemoryNet: " << __LINE__ << "*/";
  }
#endif

  if (isMarkNets (context) && !isReference (context)) {
    getMaster()->emitCode(eCGMarkNets);
  }

  if (isDeclare (context) && !isPort () && !isReference (context)) {
    // Do temp memory declaration

    UtOStream &out = gCodeGen->CGOUT ();
    if (getCGOp ()->isUnreferenced () && not isNonStatic ()) {
      out << "// " << member (getName ()) << ENDL;
      return eCGVoid;
    }

    if ((isBlockLocal () and isAliased ())
        || getCGOp ()->needsAccessor ())
      generateAccessorFunction (this);

    // Can't call generateMemberVariable here!
    out << TAG << CType (this) << member (getName());
              
    // A temp memory is constructed with a reference to the
    // master memory.  But we can't do that unless the master
    // has already been declared.
    const NUMemoryNet* master = getMaster ();
    CGAuxInfo* cgop = master->getCGOp ();

    // If the master is static, then we know we've been able to
    // declare things so that declaration order is okay.  Otherwise,
    // if the master has an offset, it's already been declared and is
    // okay.  If neither condition holds, we will let dynamic
    // initialization occur.
    if (not master->isNonStatic () 
        || (cgop && cgop->hasOffset ())) {
      // Remember the declaration to detect the first
      // assignment to it; it's a 'no-op' because we set the
      // master at declaration time.
      gCodeGen->addTempNet(this);
      out << TAG << "(";
      master->emitCode (eCGDefine);
      out << ")";
    }

    if (getCGOp ()->hasOffset ()) {
      out << "; // " << alignment (this)
          << ", " << getCGOp ()->getOffset () << ENDL;
    } else {
      out << ";" << ENDL;
    }

  } else {
    return NUNet::emitCode (context);
  }
  return eCGVoid;
}

static void sDeclareANet (const NUNet* n, CGContext_t context)
{
  CGAuxInfo* cgop = n->getCGOp ();
  UtOStream &out = gCodeGen->CGOUT ();

#if 0
  if (!strncmp (n->getName ()->str (), "$memorybv_p1_taacoordv", 22)) {
    n->pr ();
  }
#endif

  if (cgop->isStaticAssign ())
  {
    // Handled in NUContAssign::emitCode()
    ;
  } else if (cgop->isPortAliased ()) {
    // Handled by AliasDecl - filters out in, out and inout
    // ports that are actually allocated at a lower level.
    ;
  } else if (not emitUtil::isNetDeclared(n)
           && not isNameOnly (context)
           && not isNoPrefix (context))

  {
    // Ignore this variable - it's never referenced, so we don't
    // have to generate a declaration.

    out << "/* unused " << member (n->getName()) << " */" << ENDL;
  }
  else if (n->isPort () && !isReference (context))
  {
    // Otherwise, it's either defined at a LOWER level in the
    // allocation hierarchy and we let a special pass over the
    // PortConnections create member functions that
    // reference the thing this variable is bound to, or it's
    // defined at a HIGHER level and we have to pass down a
    // reference in the constructor and initialize this member.

    // We can't have an accessor function inside a function.  If this
    // net is non-static, then it's inside a function
    //

    // ['const'] TYPE & get_foo() ['const'] { return m_foo;}
    generateAccessorFunction (n);

    // ['const'] TYPE [&] m_foo;
    generateMemberVariable (n, false);

  }
  else
  {
    // An something else weird.  Give it storage.

    if (!isReference (context)) {
      emitUtil::checkNetIncludes (n);	// Might not have the header files yet
    }

    if (isReference (context)) {
      // do not emit an accessor functor in the middle of a parameter list
    } else if ((n->isBlockLocal () and n->isAliased ()) || cgop->needsAccessor ()) {
      generateAccessorFunction (n);
    }

    out << TAG << CType (n);
    if (isReference (context)) {
      out << "&";
    }
    out << (isNoPrefix (context) ? noprefix (n):member (n));

    if (isZeroInit (context))
    {
      out << " UNUSED "; // Tag as possibly never used

      if ( n->isReal() )
        out << "= 0.0";
      else if ( (n->getBitSize () <= LLONG_BIT) and
                not n->is2DAnything ()          and
                not gCodeGen->isTristate(n))
      {
        // scalars and stateupdates allow zeros.
        //
        // The no-arg constructor for tristates initializes to 0 so this is
        // not necessary.
        out << "=0";
      }
    }

    if (!isList (context))
    {
      // if we're not doing a list, add punctuation...
      if (cgop->hasOffset ())
        out << "; // " << alignment (n) << ", "
            << cgop->getOffset () << ENDL;
      else
        out << ";" << ENDL;
    }
    else if (not isNoPrefix (context) && cgop->hasOffset ())
      // dump interesting offset info
      out << " /* " << alignment (n) << ", "
          << cgop->getOffset () << " */ ";
  }
}

CGContext_t
NUNet::emitCode (CGContext_t context) const
{
  context = sCommonNetCodeGen (this, context);

  if (isVoid (context))
    return eCGVoid;

#if 0
  if (!strncmp (getName ()->str (), "$memorybv_p1_taacoordv", 22)) {
    pr ();
  }
#endif

  UtOStream &out = gCodeGen->CGOUT ();
  CGAuxInfo* cgop = getCGOp();

  // Now decide how to print the name.

  if (isDeclare (context))
  {
    sDeclareANet (this, context);
    return eCGVoid;
  }
  else if (isDefine (context) || isCycleCopy(context))
  {
    // Definition or use in expressions.  Because we have yet to emit
    // the declarations, it would be wrong to check the hasAccessor()
    // or hasMember() for the CGAuxInfo.  Instead we check if this is
    // an output, or something that doesn't have storage.  These two
    // things ALWAYS have accessors.
    //

    if (isAccessor (context)) {
      out << accessor (this) << TAG;
      cgop->usedAccessor ();

    } else if (isBlockLocal () && isNonStatic ()) {
      // task formal parameters or simple local variables
      out << TAG << member (this);
    } else if (isBid () || isOutput () // simple ports - could be aliased
               || not isAllocated ()) {
      // module output or inout variables that are accessed via
      // an alias accessor function.  
      out << accessor (this);
      cgop->usedAccessor ();
    } else {
      // Anything else.
      out << TAG << member (this);
    }

    if (isForced (context))
      // We're writing to part of a forced object, and need to access
      // the forcing mask so that we can test a piece of it.
      out << ".getMask()" << TAG;
    else if (isDoubleBuffered () && not isNameOnly (context))
    {
      if ( is2DAnything () ) {
        if (isInitialBlock (context)) {
          // Avoid StateUpdate memory by accessing underlying
          // simple memory
          out << ".base()" << TAG;
        }
      }
      else {
        NU_ASSERT("StateUpdate should not exist on regular nets" == NULL, this);
      }
    }

    CGContext_t result = eCGPrecise;

    return result;
  }
  else if (isInterface (context))
  {
    // Generating the libXXX.h C/C++ interface
    size_t nbits = getBitSize();

    if (gCodeGen->isTristate (this))
      genBidStruct (this);
    else if (nbits > LLONG_BIT)
    {
      nbits = getBitSize () + (LONG_BIT-1);	// round to UInt32 multiples

      out << TAG << CBaseTypeFmt (LONG_BIT, eCGIface) << " "
          << member (this) << "["
          << (nbits / LONG_BIT) << "]";
    }
    else
      out << TAG << CBaseTypeFmt (nbits, eCGIface) << " " << member (this);

    return eCGVoid;
  }
  else if (isSchedule (context))
  {
    // On a schedule pass, we only look for things that need sync()
    // code generated.  If we don't allocate this object, we don't
    // have to sync() it.  The buffer flag is set on all the aliases
    // (otherwise we generate bad code for the accessor functions)

    if (isDoubleBuffered () && isAllocated ())
    {
      NU_ASSERT(is2DAnything(), this); // we may need to allow is2DWire here also
      out << TAG << member (this) << ".sync()";
    }
  }
  else if (isConstructor (context))
  {
    if (isNoPrefix (context))
    {
      out << TAG << formal (this);
    }
    else if (cgop->hasFormal ())
      out << TAG << formal (this);
    else if (cgop->hasAccessor ())
      // output-aliased port access
      out << TAG << accessor (this);
    else
      out << TAG << member (this);
  }
  else if (isAccessor(context))
  {
    if (isNoPrefix(context))
      out << TAG << noprefix (this);
    else if (cgop->hasAccessor ())
      out << TAG << accessor (this);
    else
      out << TAG << member (this);
  }
  else if (isNameOnly(context))
  {
    // Output JUST the name for special purposes
    if (isNoPrefix(context))
      out << TAG << noprefix (this);
    else if (cgop->hasMember ())
      out << TAG << member (this);
    else if (cgop->hasAccessor ())
      out << TAG << accessor (this);
    else
      out << TAG << member (this);
  }

  return eCGVoid;		// No useful return information
}

CGContext_t
NUBitNetHierRef::emitCode (CGContext_t context) const
{
  context &= ~eCGFlow;

#if 0
  {
    UtOStream &out = gCodeGen->CGOUT ();
    out << "/* NUBitNetHierRef: " << __LINE__ << "*/";
  }
#endif

  return mNetHierRef.emitCode (context);
}

CGContext_t
NUVectorNetHierRef::emitCode (CGContext_t context) const
{
  context &= ~eCGFlow;

#if 0
  if (&gCodeGen->CGOUT () != NULL) {
    UtOStream &out = gCodeGen->CGOUT ();
    out << "/* NUVectorNetHierRef: " << __LINE__ << "*/";
  }
#endif

  return mNetHierRef.emitCode (context);
}

CGContext_t
NUMemoryNetHierRef::emitCode (CGContext_t context) const
{
  context &= ~eCGFlow;

  return mNetHierRef.emitCode (context);
}

// Emit code for a hierarchical net ref
CGContext_t
NUNetHierRef::emitCode (CGContext_t context) const
{
  context &= ~eCGFlow;

#if 0
  if (&gCodeGen->CGOUT () != NULL) {
    UtOStream &out = gCodeGen->CGOUT ();
    out << "/* NUNetHierRef: " << __LINE__ << "*/";
  }
#endif

  CGContext_t rctx = eCGPrecise;

  // get the hier ref bit, vector, or mem net for use in this common
  // structure. NUNetHierRef is not a base class of those
  // classes. Instead it contains a pointer back to the original hier
  // ref that got us here.
  const NUNet* net = getNet();

  CGAuxInfo* cgop = net->getCGOp ();
  if (!cgop)
    {
      // We haven't visited this node before, or it'd have a CGAuxInfo...
      cgop = new CGAuxInfo;
      net->setCGOp(cgop);
    }

  if (isMarkNets (context))
  {
    // A reference while generating code implies that we need this
    // variable declared. 
/*
    cgop->setReferenced ();
    emitUtil::checkNetIncludes (net);
*/
    net->NUNet::emitCode (eCGMarkNets);

    // Mark all the real nets referenced. We have to do this for
    // non-local references only because local references get handled
    // below, but it seemed better to do both local and non-local
    // here.
    for (NUNetHierRef::NetVectorCLoop l = loopResolutions(); !l.atEnd(); ++l)
    {
      const NUNet * resolution = (*l);
      resolution->emitCode (eCGMarkNets);
    }
  }

  // Check if it is locally relative or not.
  NUModuleInstanceVector instPath;
  NUNet *resolved_net = 0;
  bool isLocal = gCodeGen->isLocallyRelativeHierRef(net, &instPath, &resolved_net);
  if (isLocal)
  {
    // If we are using this expression, emit the reference
    if (isDefine (context) || isNameOnly (context))
    {
      // Emit the path
      // Path may be empty if it resolves to this module.
      NUModuleInstanceVectorLoop loop(instPath);
      if (not loop.atEnd())
        emitCodeList (&loop, eCGNameOnly, "", ".", ".");

      // Emit the net. There should only be one
      NUNet* realNet = NULL;
      bool oneResolution = true;
      for (NUNetHierRef::NetVectorCLoop l = loopResolutions(); !l.atEnd(); ++l)
      {
        if (realNet != NULL)
        {
          // fatal error.  print detail in the assert
          realNet->print(1,0);
          oneResolution = false;
        }
        NU_ASSERT(realNet == NULL, net);
        realNet = (*l);
      }
      if (!oneResolution)
      {
        realNet->print(1,0);
        NU_ASSERT(oneResolution, net);
      }
        
      NU_ASSERT2(resolved_net == realNet, resolved_net, realNet);
      rctx = realNet->emitCode (context | eCGMarkNets);
    }
  }
  else
  {
    // For non locals we have to create a pointer for the
    // reference. But we have to do it with a unique name. Create that
    // name from the reference. We have to replace the "." with "$"
    UtString hierName;
    composeCPath(&hierName);

    // Filter out things not permitted in C/C++ names
    const StringAtom* hName = gCodeGen->getCodeNames ()->translate (&hierName);

    // Emit the data based on the context
    UtOStream &out = gCodeGen->CGOUT ();
    if (isDeclare(context))
    {
      if (not emitUtil::isNetDeclared(net)
          && not isNameOnly (context)
          && not isNoPrefix (context))
      {
        // Ignore this variable - it's never referenced, so we don't
	// have to generate a declaration.
        out << "/* unused " << member (net->getName()) << " */" << ENDL;
      }
      else
      {
        // For the declaration we need the pointer and access function.
	// Do we actually want to loop over all the instances?
	const char * const_str = netConstPrefix (net);

        out << "  " << const_str << CType (net) << "* " << member (hName)
            <<"; //" << alignment (net) << ", "
            << cgop->getOffset () << ENDL;
        out << "  " << const_str << CType (net) << "& " << accessor (hName)
            << " ALWAYS_INLINE { return *" << member (hName) << "; }" << ENDL;

        cgop->setAccessor ();
      }
      rctx = eCGVoid;
    }
    else if (isDefine(context))
    {
      // This is either a definition or expression. Call the access
      // function.
      out << TAG << accessor (hName);
      cgop->usedAccessor ();
    }
    else if (isNameOnly(context))
    {
      // Emit the member variable pointer
      out << TAG << member (hName);
    }
    else if (isConstructor(context))
    {
      // Emit a rereference of the member variable pointer
      out << TAG << "(* " << member (hName) << ")";
    }
  }

  return rctx;
}

// Elaborations provide hierarchical context for a net.
CGContext_t
NUNetElab::emitCode (CGContext_t context) const
{
  context &= ~eCGFlow;

#if 0
  if (&gCodeGen->CGOUT () != NULL) {
    UtOStream &out = gCodeGen->CGOUT ();
    out << "/* NUNetElab: " << __LINE__ << "*/";
  }
#endif

  const STBranchNode* parent;
  NUNet* net = emitUtil::findAllocatedNet(this, &parent);

  if (isHierName(context))
  {
    gCodeGen->CGOUT () << TAG;
    gCodeGen->outputHierPath(gCodeGen->CGOUT (), parent);
  }
  return net->emitCode (context);
}

// Simple LVALUE doesn't require any special processing - just get the
// name of the base object...
CGContext_t
NUIdentLvalue::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  return mNet->emitCode (context | eCGLvalue);
}

CGContext_t
NUIdentRvalue::emitCode (CGContext_t context) const
{
  UInt32 accessSize = isLvalue (context) ? elementSize (mNet) : getBitSize ();
  return coerceRvalue(mNet, NULL, accessSize, isSignedResult (), context);
}

CGContext_t
NUIdentRvalueElab::emitCode (CGContext_t context) const
{
  NU_ASSERT(isCycleCopy(context), this);
  const STBranchNode* parent;
  NUNet* net = emitUtil::findAllocatedNet(mNetElab, &parent);
  UInt32 accessSize = isLvalue (context) ? elementSize (net) : getBitSize ();
  return coerceRvalue(net, parent, accessSize, isSignedResult (),
                      context | eCGHierName);
}

// Output (<exp> + const) for bit indexing
// Must output something here even if it's just a "0".
// Index expressions are generally immune to precision-rounding
// (at least of the final result)
//
// return TRUE if index expression was a VHDL eBiDownTo, so that we
// can compute a dynamic width correctly.

static bool
sGenBitOff (const NUExpr* e, SInt32 off, CGContext_t context)
{
  context &= ~eCGDirtyOk;
  UtOStream&  out = gCodeGen->CGOUT ();
  bool wasDynWidth = false;

  if (e)
  {
    // Check for a downto range index expression
    const NUExpr* lowBound;
    if (isDynRange (e, &lowBound))
    {
      e = lowBound;
      wasDynWidth = true;
    }

    UInt32 indexSize = e->getBitSize ();

    out << TAG << "((";
    if (not isPhysicalSize (indexSize) && indexSize <= LLONG_BIT
        && e->isSignedResult ())
      // operand is a signed expression.  Make sure we extend it properly
      context = emitUtil::signExtend (out, e, context, indexSize);
    else
      context = e->emitCode (context | eCGPrecisePrimary);

    out << TAG;

    if (e->getBitSize () > LLONG_BIT && not e->castConst ())
      out << ".value()";

    if (off)
      out << " + " << off ;
    out << ")";
    if (not isPrecise (context))
      mask (e->getBitSize (), "&", e->isSignedResult ());
    out << ")";
  }
  else
  {
    out << off;
  }
  return wasDynWidth;
}


// Generate a macro call to SET or GET a BIT or SLICE from a POD.
// Inputs: \a id - the POD object to access.  It can be a lvalue or rvalue
//      \a context - codegen context passed to emitCode()
//      \a bitIndex - indexing expression (could be NULL)
//      \a size - size of the object being accessed
//      \a lsb - low-order bit being accessed
//      \a msb - high-order bit being accessed.
//      \a dirty - is this access allowed to be dirty (only relevant on read)
//      \a inbounds - true if index known to be inbounds
static CGContext_t
sGenSlice (const NUUseNode* id, CGContext_t context,
           const NUExpr* bitIndex, UInt32 size, SInt32 lsb, SInt32 msb, bool dirty, bool inbounds)
{
  UtOStream &out = gCodeGen->CGOUT ();
  bool isGET = not isLvalue (context); // It's a SET if in lvalue context, else a GET

  //TODO: listen to 'flow' and just test the bits instead of extracting and shifting
  context &= ~eCGFlow;          // We can ignore flow requests

  UInt32 width = (msb + 1 - lsb);

  // Can only load dirty operands if the width is constant and we don't need a precise
  // result.
  dirty &= not needsPrecisePrimary (context) & not isDynRange (bitIndex);

  if (dirty && isGET)
    // Cast dirty result to original size.  Needed for
    // test/unit-codgegen/test64.v.  An imprecise 32-bit operand going
    // into a 64-bit operation would not work otherwise.
    out << TAG << CBaseTypeFmt( width, eCGDefine );


  // Generate [W](GET|SET)_(BIT|SLICE)[_CHECK|_DIRTY](base, pos [,width][,|)] macro call
  // 

  if (msb != lsb)
    out << "W";

  out << (isGET ? "GET" : "SET");
  if (msb == lsb)
    out << "_BIT";
  else
    out << "_SLICE";

  // Check for dynamic index expression that is either negative or could be
  // larger than the expression unless we have NO-OOB turned on
  //
  bool check = false;
  if (bitIndex && not inbounds
      && not sIsIndexInBounds (0, bitIndex, ConstantRange (size-1,0), bitIndex->getLoc (), true)
      && ((not isGET) || width > 1)) {
    // writing bits or accessing a multi-bit field
    out << "_CHECK";
    dirty = false;
    check = true;
  }

  if (dirty)
    out << "_DIRTY";

  context &= ~eCGDirtyOk;
  out << " (";
  CGContext_t rctx = id->emitCode (context);

  out << TAG;

  if (check && not isGET)
    // only WSET_SLICE_CHECK has to know how big the object is
    out << "," << size;

  // If our base was a bitfield, we emit a malformed translation
  // like: SET_SLICE(SET_SLICE(var,p,s, p, s, val);
  //
  //
  NU_ASSERT (not isBitfield (rctx), id);

  // Set the precise-result flag as a default assumption.  We'll
  // refine that for non-constant non-checked indexes.
  if (dirty)
    rctx = Imprecise (rctx);
  else
    rctx = Precise (rctx);

  rctx &= ~eCGDirtyOk;
  out << ", ";

  bool wasDynWidth = sGenBitOff (bitIndex, lsb, context &~eCGLvalue);

  if (width != 1)
  {
    out << ", ";
    if (wasDynWidth)
    {
      context |= eCGDownToValid;
      bitIndex->emitCode (context &~eCGLvalue);
      rctx = Imprecise (rctx);  // width is unknown
    }
    else
      out << width;
  }

  if (not isGET)
    // Set still has to emit a "," <arg> and the close parenthesis after this
    out << ", " << TAG << CBaseTypeFmt (size, eCGDefine)
        << CBaseTypeFmt (msb+1-lsb, eCGDefine);
  else
    out << ")";

  return rctx;
}

// Generate a macro call to SET a BIT or SLICE for Nested NUVarselLvalue
// Inputs: \a lv - a varselLvalue under another varselLvalue
//      \a context - codegen context passed to emitCode()
//      \a bitIndex - indexing expression
//      \a size - size of the object being accessed

static CGContext_t
sGenNestedSetSlice (const NUVarselLvalue* lv, CGContext_t context,
		    const NUExpr* bitIndex, UInt32 size)
{
  UtOStream &out = gCodeGen->CGOUT ();
  out << TAG;
  NUBase * id = lv->getLvalue();
  // only 2 level of nested varsel now.
  NU_ASSERT(dynamic_cast<const NUVarselLvalue*>(id) == NULL, id); 

  // only handles constant range now.
  NU_ASSERT(lv->isConstIndex (), lv);
  ConstantRange cr (*lv->getRange());

  //! bounds checking -- if ((idx <= size) &&  (idx >0)
  /*  will not write if the idex is out of bound, even if
   *  there are partially overlap when idex < 0.
   */
  out << "if ((" ;
  bool wasDynWidth = sGenBitOff (bitIndex, 0, context &~eCGLvalue);

  out << "<=";

  if (wasDynWidth) {
    bitIndex->emitCode ((context | eCGDownToValid) & ~eCGLvalue);
    out << TAG;
  } else {
    out << cr.getLength() << TAG;
  }

  out << ") && (";
  sGenBitOff (bitIndex, 0, context &~eCGLvalue);
  out << ">=" << "0" << ")) " << ENDL;

  // Generate [W]SET_(BIT|SLICE)(base, pos [,size][,|)] macro call
  // 
  if (size == 1)
    out << "SET_BIT(";
  else
    out << "WSET_SLICE(";

  CGContext_t rctx = id->emitCode (context);

  // Will emit a malformed translation
  // like: SET_SLICE(SET_SLICE(var,p,s, p, s, val);
  //
  //
  NU_ASSERT (not isBitfield (rctx), id);

  out << ", ";

  sGenBitOff (bitIndex, cr.getLsb(), context &~eCGLvalue);
  out << TAG;

  //! size adjustment: 
  /* ((bitIndex+size) > cr.getLength() ? (cr.getLength-bitIndex+1) : size)
   * For example, x[32 +: 32][j +: 16], size is 16, and cr.getLength() is 32.
   *        
   *              |------+---------+-----------------|
   *              63     j         32                0
   *           |---------|                                    j = 18
   *              |------|                                 size = 15
   *
   */

  if (size != 1) {
    out << ", " << "(";
    sGenBitOff (bitIndex, size, context &~eCGLvalue);

    out << TAG << ">";
    if (wasDynWidth) {
      bitIndex->emitCode ((context | eCGDownToValid) & ~eCGLvalue);
      out << TAG;
    } else {
      out << TAG << cr.getLength();
    }
    out << "?" <<  "(";

    if (wasDynWidth) {
      bitIndex->emitCode ((context | eCGDownToValid) & ~eCGLvalue);
      out << TAG;
    } else {
      out << TAG << cr.getLength();
    }

    out << "-";
    sGenBitOff (bitIndex, -1, context &~eCGLvalue);
    out << TAG << "):";
    if (wasDynWidth) {
      bitIndex->emitCode ((context | eCGDownToValid) &~ eCGLvalue);
      out << TAG;
    } else {
      out << TAG << size;
    }

    out << ")";
  }

  out << ", " << TAG << CBaseTypeFmt (size, eCGDefine);

  return rctx;
}

// Fetch from a bit-slice.  Deal with signed vs. unsigned expressions.
// Verilog treats all part and bitselects as implicitly unsigned.
// However for bitvectors, BVref<SInt32,SInt64> and SBitVector produce
// signed results and may require casts to suppress GCC warnings about
// comparing signed and unsigned quantities.
//
static CGContext_t
sEmitPartsel (const NUVarselRvalue *varsel, CGContext_t context)
{
  if (varsel->isWholeIdentifier ())
    // Just ignore the partselect and fetch the correctly signed source...
    return coerceRvalue (varsel->getIdent (), NULL, varsel->getBitSize (), varsel->isSignedResult (),
                         context);

  CGContext_t rcontext = Precise(context);
  bool dirtyOk = isDirtyOk (context);
  bool flowContext = isFlowValue (context);
  context &= ~(eCGBVOverloadSupported | eCGBVExpr | eCGDirtyOk | eCGFlow);

  UtOStream &out = gCodeGen->CGOUT ();

  ConstantRange r (*varsel->getRange());
  ConstantRange base;
  
  const NUExpr* identExpr = varsel->getIdentExpr ();
  bool wholeIdent = identExpr->isWholeIdentifier ();

  bool baseSign = identExpr->isSignedResult (); // sign of underlying object

  if (wholeIdent)
  {
    base = *getNUrange (varsel->getIdent ());
  }
  else
  {
    base.setLsb(0);
    base.setMsb(identExpr->getBitSize () - 1);
  }

  UInt32 netSize = base.getLength ();

  // Check for completely out-of-bounds
  if (varsel->isConstIndex () && not r.overlaps (base))
  {
    out << TAG << CBaseTypeFmt (varsel->getBitSize ()) << "(0u)";	// Completely OOB
    return eCGBitfield;
  }

                                // Handle special non-constant indexes
  bool factoredIndex = false;   //  that are multiples of LONG_BIT

  // Reduce to overlapping piece
  if (varsel->isConstIndex ())
    r = r.overlap (base);
  else
    factoredIndex = Stripper::isFactorable (varsel->getIndex ());

  UInt32 size = r.getLength ();

  if (size > 65535 && not isDynRange (varsel->getIndex ()))
    // error - implementation limit exceeded!
    gCodeGen->getMsgContext ()->CGBigPartSel (&(varsel->getLoc ()));

  bool tempGenerated = false;

  if (NeedsATemp (context) || isActualParameter (context))
    // Return a BitVector, not a BVref
    //
    // The quick fix for bug7683 requires passing down the eCGActualParameter
    // attribute when this expression is an actual parameter for a task or
    // function. For these, output a BitVector rather than a BVref. The
    // SignedActual inherited attribute is required to indicate that the
    // corresponding formal parameter is signed and a signed bitvector is
    // needed.
    // 
    // Bug7695 suggested that further testing might be necessary to locate
    // other instances where a signed temporary is needed but cannot be
    // inferred from the expression alone. Further analyis suggests that this
    // only happens when dealing with the binding of an expression to a signed
    // task or function formal parameter. This is dealt with in emitTFActuals.
    //
    // Bug7696 suggests eliminating the requirement to generate a temporary for
    // the actual parameter. The temporary is not expensive, so this is not a
    // high priority.
    {
      if (isSignedActual (context) || varsel->isSignedResult ()) {
        out << TAG << CBaseTypeFmt (varsel->getBitSize (), eCGVoid, true) << "(";
      } else {
        out << TAG << CBaseTypeFmt (varsel->getBitSize ()) << "(";
      }
      tempGenerated = true;
      context &= ~(eCGNeedsATemp|eCGActualParameter|eCGSignedActual);
    }
  else if (isAlignedBVref (NULL, identExpr, varsel->getIndex (),
                           r.getLsb (), size, true)
           && varsel->getBitSize () == size)
  {
    // Not asking for larger bitvector from smaller
    out << TAG;
    return genAlignedBVref (out, identExpr, varsel->getIndex (),
                            context, r.getLsb (), size, varsel->isIndexInBounds ());
  }
  // convert a partselect that's equivalent to a byte|short|long
  // access into the more optimal form.
  // 
  if (varsel->isConstIndex ()
    && isAlignedAccess (NULL, identExpr, r.getLsb (), size)) {
    out << TAG;
    rcontext = sGenAlignedAccess (identExpr, r.getLsb (), size,
                              varsel->isSignedResult (), context);
    if (tempGenerated) {
      out << ")";
      rcontext |= eCGIsATemp;
    }
    return rcontext;
  }

  const NUExpr *indexExpr = varsel->isConstIndex () ? 0 : varsel->getIndex ();
  UInt32 varselSize = varsel->getBitSize ();

  if (netSize <= LLONG_BIT)
    // Whole object is no larger than a POD
    {
      bool sextFlag = false;
      if (varsel->isSignedResult ()) {
        if (varselSize > LLONG_BIT)
          out << TAG << CBaseTypeFmt (varsel);
        out << "(+Extender<" << CBaseTypeFmt (physicalSize (netSize), eCGDeclare, true)
            << "," << CBaseTypeFmt (r.getLength ()) << ","
            << physicalSize (netSize) << "," << r.getLength () << ">(";
        sextFlag = true;
      }
      rcontext = sGenSlice (identExpr, context & ~eCGLvalue,
                            indexExpr, netSize,
                            r.getLsb (), r.getMsb (),
                            (varsel->determineBitSize () == varsel->getBitSize ()|| dirtyOk), // Dirty?
                            varsel->isIndexInBounds ());
      context &= ~eCGDirtyOk;
      if (sextFlag)
        out << ")())";
    }
  else if ((varsel->isConstIndex () || factoredIndex)
           && varsel->getBitSize () <= LONG_BIT
           && wholeIdent
           && (r.getLsb () % LONG_BIT) == 0 )
  {
    // Want an 32-bit or smaller aligned piece of a BitVector
    out << TAG << "(";
    rcontext = genAlignedBVref (out, identExpr, varsel->getIndex (),
                                context & ~eCGNeedsATemp, r.getLsb (), LONG_BIT,
                                varsel->isIndexInBounds ());

    if( varsel->determineBitSize() != varsel->getBitSize() )
      mask (size, "&");
    else
      rcontext = Imprecise(rcontext);

    out << ")";
  }
  else if ((varsel->isConstIndex () || factoredIndex)
           && varsel->getBitSize () <= LONG_BIT
           && wholeIdent
           && (r.getLsb() >= 0)                            // disallow optimization when offset is negative
           && ((r.getLsb () % LONG_BIT) != 0)
           && ((r.getLsb () % LONG_BIT) + size) > LONG_BIT // cross a word boundary
           && !isLvalue (context))
  {
    // Accessing a value from a word-indexed bitvector where the desired bits cross a word
    // boundary. We can generate much better code for this than the default BitVector::value(p,s)
    // For something like:
    //    X[(i*32)+29 +: 32]
    // We can emit
    //   (x._M_w[i]>>29) + (x._M_w[i+1]<<3)
    //
    UInt32 wordMult = r.getLsb () & ~(LONG_BIT-1);
    out << TAG << "(((";
    rcontext = genAlignedBVref (out, identExpr, varsel->getIndex (),
                                context & ~eCGNeedsATemp, wordMult, LONG_BIT,
                                varsel->isIndexInBounds ());
    out << " >> " << (r.getLsb () % LONG_BIT);
    out << ") + (";
    rcontext = genAlignedBVref (out, identExpr, varsel->getIndex (),
                                context & ~eCGNeedsATemp, wordMult + LONG_BIT, LONG_BIT,
                                varsel->isIndexInBounds ());
    out << " << " << (LONG_BIT - (r.getLsb () % LONG_BIT)) << "))";
    if (varsel->determineBitSize () != varsel->getBitSize ())
      mask (size, "&");
    else
      rcontext = Imprecise (rcontext);
    out << ")";
  }
  else if (size <= LLONG_BIT && not isLvalue (context))
    // We want a small amount of a large object and we're not doing op= processing..
    {
      out << TAG;
      identExpr->emitCode (context & ~eCGNeedsATemp);

      const char* signPrefix="";
      if (baseSign and not varsel->isSignedResult ())
        signPrefix = "u";

      if (size <= LONG_BIT) {
	out << "." << signPrefix << "value(";
      } else {
	out << "." << signPrefix << "llvalue(";
      }

      if (indexExpr 
          || r.getLsb () != 0 
          || (r.getMsb () != (SInt32)size && (size != LONG_BIT && size != LLONG_BIT)))
      {
        bool wasDynWidth = sGenBitOff (indexExpr, r.getLsb (), context);
        out << ",";
        if (wasDynWidth)
          indexExpr->emitCode (context | eCGDownToValid);
        else
          out << size;
      }
      out << ")";	
    }
  else
    // Need a large amount of a large object.  This yields a const BVref
    // so it shouldn't ever match as a temporary value or be reduced...
    //
    // We might be doing an unsigned partselect of a signed object
    {
      bool downCast = baseSign && not varsel->isSignedResult ();

      out << TAG;

      if (downCast)
        out << "static_cast<" << CBaseTypeFmt (identExpr->getBitSize (), eCGDeclare) << ">(";

      rcontext = identExpr->emitCode (context);

      if (downCast)
        out << ")";

      if (indexExpr == NULL) {
        // This is a constant.  Verify it now.
        SInt32 offset = r.getLsb();
        NU_ASSERT((offset + size) <= identExpr->getBitSize(), varsel);
        // Constant index, should be positive, otherwise this code will do the wrong thing.
        NU_ASSERT(r.getLsb() >= 0, varsel);
        out << TAG << (isLvalue (context) ? ".lpartsel_unchecked(" : ".partsel_unchecked(");
      }
      else {
        out << TAG << (isLvalue (context) ? ".lpartsel(" : ".partsel(");
      }
      bool wasDynWidth = sGenBitOff (indexExpr, r.getLsb (), context);
      out << ",";
      if (wasDynWidth) {
        out << TAG;
        indexExpr->emitCode (context | eCGDownToValid);
      } else {
        out << TAG << size;
      }
      out << ")";

      if (flowContext)
        out << ".any()";
    }

  if (tempGenerated)
    {
      out << ")";
      rcontext |= eCGIsATemp;
    }
	
  return rcontext;
}

// Helper - generate a bit test for BV or POD avoiding things that might not inline
static void
sGenInlineBitTest (UtOStream& out, UInt32 objSize, SInt32 pos, const NUVarselRvalue* rvalue)
{
  NU_ASSERT(pos >= 0, rvalue);

  if (objSize > LLONG_BIT) {
    out << TAG << "._M_w[" << (pos/LONG_BIT) << "]"; // Known inbounds, avoid get_word()
    pos %= LONG_BIT;
    objSize = LONG_BIT;
  }

  out << " & ";
  emitConstant (out, UInt64 (1)<< pos, objSize,  eCGVoid, false);
}

// Select a  bit-field from some object.
//
// If in flow context, we only need to generate a bit test operation
CGContext_t
NUVarselRvalue::emitCode (CGContext_t context) const
{
  CGContext_t rcontext = Precise(eCGVoid);

  UInt32 width = mRange.getLength (); // actual width of expression
  int extraParen = 0;
  UtOStream &out = gCodeGen->CGOUT ();
  
  // Look for selects that are smaller than the context required to compute the
  // larger expression and construct a larger operand.  But if we were doing
  // bitvector short-operand overloading (e.g. something like
  //
  //    BV128 = BV128 + X[20:10]
  // we don't want to force the second operand to a bitvector - just make sure
  // it's resized to an appropriate 32/64 bit operand.
  //
  UInt32 newWidth = getBitSize ();
  if (newWidth > width)
  {
    if (isBVOverloadSupported (context) && isBVExpr (context))
      newWidth = bvSizeToOther (width, newWidth);

    if ((newWidth > LLONG_BIT)
        || (physicalSize (newWidth) != physicalSize (width)))
      // widening has a real effect
    {
      out << TAG << CBaseTypeFmt (newWidth, eCGDeclare, isSignedResult ()) << "(";
      extraParen++;
      context &= ~eCGNeedsATemp; // no longer need a temp.
    }
  }

  if (width > 1) {
    // the exression is not the top of an actual parameter
    out << TAG;
    context = sEmitPartsel (this, context);
    while (extraParen--)
      out << ")";
    return context;
  }

  //////////////////////////////////
  // Handle single bit accesses
  //////////////////////////////////

  out << TAG;

  // Do we need to sign-extend the result of the bit-select?
  bool extendSignedBit = (isSignedResult () && getBitSize () > 1);

  context &= ~eCGDirtyOk;       // Don't play with dirty bits on single bit access.

  if (extendSignedBit) {
    out << "(";
    ++extraParen;
  }
    
  ConstantRange r (mRange);
  ConstantRange base;           // bitrange of object we are fetching from
  if (mIdent->isWholeIdentifier())
  {
    base = *getNUrange (getIdent ());
  }
  else
  {
    base.setLsb(0);
    base.setMsb(mIdent->getBitSize () - 1);
  }

  // determine if our index needs bounds checking.
  bool oob_safe = isIndexInBounds ()
    ||sIsIndexInBounds (sFindNet (getIdentExpr ()), getIndex (), base, getLoc (), true);
  bool constIndex = isConstIndex ();
  bool flowContext = isFlowValue (context);
  context &= ~eCGFlow;

  if (constIndex && !base.overlaps (r))
  {
    // Check for access to bit outside of defined range
    out << "0";
  } else if (!GCC_BUG_26662 && constIndex && !isLvalue (context)) {
    // not an OP= case, and we can easily inline this
    out << "((";
    mIdent->emitCode (context | eCGHuge);
    context &= ~eCGForcible;
    sGenInlineBitTest (out, base.getLength (), r.getLsb (), this);
    out << ")!= 0)" << TAG;

  } else if (base.getLength () > LLONG_BIT) {  
    // Extracting a bit from something big.  Instead of just returning
    // the reference, get the value.
  
    mIdent->emitCode (context | eCGHuge);
    context &= ~eCGForcible;

    // Non-constant bit index expression - use runtime routines
    if (isLvalue (context)) {
      out << ".lpartsel(";
    } else {
      out << (const char *) (oob_safe ? ".test(" : ".checked_test(");
    }
    out << TAG;
    if (isConstIndex ()) {
      // Should be a positive constant, otherwise lpartsel/test will do wrong thing.
      NU_ASSERT(r.getLsb() >= 0, this);
      emitIndex (out, r.getLsb (), base);
    } else {
      emitIndex (out, getIndex (), r.getLsb (), context, base);
    }

    if (isLvalue (context))
      out << ",1";

    out << ")";
  }
  else
  {
    const NUExpr *index = getIndex ();
    UInt32 indexSize = index->effectiveBitSize ();

    if (not isConstIndex () && !oob_safe
        && ((indexSize > 16)
            || ( (1u << indexSize) - 1) > base.getLength ()))
    {
      out << TAG;
      // Non-constant indexing expression, and it is potentially
      // larger than the bounds of the vector we're accessing.
      //
      // Bounds check access and evaluate as zero if out-of range.  Could just
      // do random reads and treat as 'X' - except we might segfault!
      //
      // Construct new nucleus expression, code and delete it...
      //   (index + bitoff >= objectwidth) ? 0 : object[index+bitoff]
      //
      // Note use of unsigned calculations because negative offsets are out-of-bounds too
      // TODO:  move this into the ShiftGuard code too!

      isDynRange (index,&index); // Make sure index not still an eBiDownTo!
      CopyContext CC (0,0);

      NUExpr* bound = index->copy (CC);

      // Protect the indexing expression from excessive widening.
      //   consider a[~abit], where the guard ~abit > 32 will always be true, even
      //   though the indexed bit should be either zero or one.
      //
      if (bound->getBitSize () != 32) {
        NUConst* c = NUConst::create (false, 32, 32, bound->getLoc ());
        bound = new NUBinaryOp (bound->isSignedResult () ? NUOp::eBiVhExt : NUOp::eBiVhZxt,
                                bound, c, bound->getLoc ());
      }
          
      if (r.getLsb () != 0) {
        NUExpr* lsbExpr = NUConst::create (false, r.getLsb (), 32, index->getLoc ());
        bound = new NUBinaryOp (NUOp::eBiPlus, bound, lsbExpr, index->getLoc ());
        bound->resize (bound->determineBitSize ());
      } else
        bound->setSignedResult (false);

      NUExpr* netWidth =
        NUConst::create (false, base.getLength (), 32, index->getLoc ());

      NUExpr* condExpr = new NUBinaryOp (NUOp::eBiUGtre, bound, netWidth, index->getLoc ());
      condExpr->resize (1);
      condExpr = gCodeGen->getFold ()->fold (condExpr);

      // Does the bounds-test collapse to a constant - if so, we don't need it
      if (NUConst* rangeTest = condExpr->castConst ()) {
        if (rangeTest->isZero ()) {
          // inrange - just fall thru to actual bit access
          delete condExpr;
        } else {
          // always outrange - TODO: insure that we have always folded these away BEFORE codegen!
          out << TAG << CBaseTypeFmt (r.getLength ()) << "(0u)";
          delete condExpr;
          while(extraParen--) out<< ")";
          return Precise (rcontext);
        }
      } else {
        // Non-constant test, emit it
        ++extraParen;
        out << "((";
        CGContext_t idxCtxt = context & ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);
        if (not isPrecise (condExpr->emitCode (idxCtxt | eCGDownToValid)))
          mask (condExpr->getBitSize (), "&");
        out << ") ? " << TAG << CBaseTypeFmt (r.getLength ()) << "(0u) :";
        delete condExpr;
      }
    }

    if (flowContext && isConstIndex () && r.getLsb () < 64) {
      // test the bit without extracting it...
      context &= ~(eCGBVOverloadSupported | eCGBVExpr);

      // force this to be a TRUE/FALSE test.  C++ would normally do
      // the right thing, but if we turned on GNU's __builtin_expect(),
      // that generates bad code with a 64-bit expression like v64&(1ull<<40)
      //
      // This is a constant, should be positive.  If not, the shift below is illegal.
      NU_ASSERT(r.getLsb() >= 0, this);
      out << "(0 != (" << TAG;
      getIdentExpr ()->emitCode (context);
      out << " & " << TAG;
      emitConstant (out, static_cast<UInt64>(1) << r.getLsb (), physicalSize (r.getLsb ()+1),
                    eCGVoid, false);
      out << "))";
    }
    else
    {
      CGContext_t baseContext = context;
      if (flowContext)
        baseContext |= eCGFlow;     // pass it down after all

      rcontext = sGenSlice (mIdent, baseContext, getIndex (),
                            mIdent->getBitSize (), r.getLsb (), r.getMsb (),
                            determineBitSize () == getBitSize (),
                            isIndexInBounds ());
    }
  }

  if (extendSignedBit) {
    // The only known way to reach this code is via something like VHDL
    //    aBit = (sA + sB) mod 2;
    // where we end up partselecting the LOW bit, ala
    //
    //    aBit = ((sA[0] + sB[0]) mod 2)[0]
    //
    // emit (bit[k] ? T(-1) : T(0))
    while (extraParen > 1) {
      out << ")";
      --extraParen;
    }

    DynBitVector extendValue (getBitSize ());
    extendValue.set ();         // All bits on
    out << TAG;
    out << "?";
    emitConstant (out, extendValue, extendValue.size (), context & ~eCGLvalue, true);
    extendValue.reset ();
    out << ":";
    emitConstant (out, extendValue, extendValue.size (), context & ~eCGLvalue, true);
    out << ")";
  } else {
    while (extraParen--)
      out << ")";
  }

  return rcontext;
}

//  For now, only 2 level nested varselLvalue is supported.
//  it has to be  in the form of x[32 +:32][idx +:16]
//  i.e.   x[const indexing][variable indexing].
//  for a record - x[const indexing][const indexing] and
//                 x[variable indexing][const indexing]
//  should be folded properly, and 
//  x[variable indexing][variable indexing] is not yet 
//  supported.
//
static bool sIsNestedVarselLvalue(const NUVarselLvalue* lv)
{
  return (not lv->isConstIndex () && 
	  lv->getLvalue()-> getType() == eNUVarselLvalue );
}

// Deposit to a bit-slice
//
static CGContext_t
sEmitLPartsel (const NUVarselLvalue* lv, CGContext_t context)
{
  UtOStream &out = gCodeGen->CGOUT ();

  NUNet* net = NULL;
  NUMemoryNet* mem = NULL;
  UInt32 width;

  if (lv->isArraySelect()) {
    net = lv->getIdentNet(false);
    mem = net->getMemoryNet();
    NU_ASSERT(mem, lv);
    width = mem->getRowSize();
  } else {
    net = sFindNet (lv);
    if (net)
      width = elementSize (net);
    else
      width = lv->getLvalue ()->getBitSize ();
  }

  bool isBV = (width > LLONG_BIT);
  bool constantIndex = lv->isConstIndex ();

  const ConstantRange base (net ? *getNUrange (net) : ConstantRange (width-1, 0));
  ConstantRange r (*lv->getRange()); // Lsb:Msb for field we are depositing.


  // Check to see if the whole thing is out-of-range
  if (constantIndex) {
    if (not base.overlaps (r))
    {
      out << TAG << "(void)(";
      return eCGBitfield;	// evaluate RHS for sideeffects
    }
    // Don't evaluate any bits beyond bounds
    r = r.overlap (base);
  }

  UInt32 size = r.getLength ();

  if (size > 65535 && not isDynRange (lv->getIndex ()))
    // error - implementation limit exceeded!
    gCodeGen->getMsgContext ()->CGBigPartSel (&(lv->getLoc ()));

  CGContext_t result = eCGVoid;

  // If this is an aligned bitfield - we can treat it as a BitVector
  // and avoid BVref overhead entirely!  Be careful not to treat
  // a 'UInt64' as a BitVector, as the endian-ness could result in
  // troubles.

  if (isAlignedBVref (net, NULL, lv->getIndex (), r.getLsb (), size, false))
  {
    if (mem)
    {
      NUExpr* rvalue = lv->getLvalue()->NURvalue();
      out << TAG;
      result = genAlignedBVref (out, rvalue, lv->getIndex (), context|eCGLvalue,
                                r.getLsb(), size, lv->isIndexInBounds ());        
      delete rvalue;
      return result;
    }
    else if (net)
    {
      NUIdentRvalue rvalue (net, lv->getLoc());
      out << TAG;
      return genAlignedBVref (out, &rvalue, lv->getIndex (),
                              context|eCGLvalue, r.getLsb(), size,
                              lv->isIndexInBounds ());
    }
  }
  else if (constantIndex && not isHuge (context) && isAlignedAccess (net, NULL, r.getLsb(), size))
    // Convert store into byte|short|long aligned memory into the
    // hardware-friendly form.
  {
    if (mem)
    {
      NUExpr* rvalue = lv->getLvalue()->NURvalue();
      rvalue->resize (rvalue->determineBitSize ());
      result = sGenAlignedAccess (rvalue, r.getLsb(), size,
                                  mem->isSigned (), context | eCGLvalue);
      delete rvalue;
      return result;
    }
    else if (net)
    {
      NUIdentRvalue rvalue (net, lv->getLoc());
      rvalue.resize (rvalue.determineBitSize ());
      return sGenAlignedAccess (&rvalue, r.getLsb(), size, net->isSigned (), context | eCGLvalue);
    }
  }
  
  // Normal lvalue processing
  NUExpr *indexExpr = constantIndex ? 0 : lv->getIndex ();
  
  if (!isBV)
  {
    // We have to coerce RHS expression to size of LHS (it could be
    // 64 bits and need (UInt64) cast to preserve bits on shifting.
    // First cast to the LHS size then the LHS ident size.
    // See test/unit-codgen/trunc_ass.v
    if (sIsNestedVarselLvalue(lv))
      result = sGenNestedSetSlice (dynamic_cast<const NUVarselLvalue*>
				   (lv->getLvalue()), context | eCGLvalue,
				   indexExpr, lv->getBitSize())
	| eCGBitfield;
    else {
      result = sGenSlice (lv->getLvalue(), context | eCGLvalue,
			  indexExpr, width,
			  r.getLsb (), r.getMsb (),
			  false, // No dirty deposits...
                          lv->isIndexInBounds ())
	| eCGBitfield;
    }
  }
  else if (size <= LLONG_BIT)
    // Writing 64 bits or less to BitVector
    // We can't do 64 bit stores into a bitvector - this breaks g++
    // aliasing.
  {
    size_t modulus;		// track size of aligned store permitted
    
    if ( (constantIndex || lv->isIndexInBounds ())
        && size == LONG_BIT
        && canSetSlice (r.getLsb(), size, &modulus)
        && not isHuge (context)
        && (r.getLsb() % LONG_BIT == 0))
      // writing to an aligned LHS - we can safely ignore C++ aliasing
      // since there shouldn't be multiple writes to this same word.
    {
      out << TAG << "(";
      result = lv->getLvalue()->emitCode (context | eCGLvalue | eCGHuge);
      out << "._M_w[";
      UInt32 factor;
      NUExpr* factoredIndex = Stripper::factorIndexExpr (lv->getIndex (), &factor);
      if (factoredIndex) {
        factoredIndex->emitCode (context & ~eCGLvalue);
        if (factor != 1)
          out << "*" << factor;
        if (r.getLsb () != 0) {
          // Need to cast LONG_BIT or else C converts to unsigned expression
          out << " + " << (r.getLsb ()/ (SInt32)LONG_BIT);
        }
        delete factoredIndex;
      } else {
        // constant offset
        // Must be positive since we are indexing directly into the _M_w array
        NU_ASSERT(r.getLsb() >= 0, lv);
        out << (r.getLsb()/LONG_BIT);
      }
      out << "])"; // known inbounds, avoid get_word()
      return result;
    }
    
    // Couldn't do fullword stores - either not aligned or size is funny
    // Let BitVector special case it.
    result = lv->getLvalue()->emitCode (context | eCGLvalue | eCGHuge) | eCGBitfield;
    
    if (isHuge (context))
    {
      // Operanting on a nested partselect.  In this case, the base object needs to produce
      // a BVref that can be manipulated as a lvalue by the outer partselect.
      NU_ASSERT (constantIndex, lv);
      // Index is constant, should be positive, otherwise generated call to lpartsel below is invalid.
      NU_ASSERT (r.getLsb() >= 0, lv);
      out << ".lpartsel(" << r.getLsb () << "," << size << ")" << TAG;
      result &= ~eCGBitfield;
    }
    else if (sIsNestedVarselLvalue (lv))
    {
      // doing a nested access - create a partsel for the next piece
      out << TAG << ".lpartsel(";
      bool wasDynWidth = sGenBitOff (indexExpr, r.getLsb (), context & ~eCGLvalue);
      out << ",";
      if (wasDynWidth) {
        out << TAG;
        indexExpr->emitCode ((context | eCGDownToValid) & ~eCGLvalue);
      } else {
        out << TAG << r.getLength ();
      }
      out << ")";
      result &= ~eCGBitfield;
    }
    else if (size == 1)
    {
      if (lv->isIndexInBounds () || sIsIndexInBounds (net, indexExpr, base, lv->getLoc (), true))
        out << ".set(";
      else
        out << ".checked_set(";

      // Better not have dynamic width for something that's ONLY one bit wide; we
      // must be horribly confused to get into this situation.
      out << TAG;
      bool wasDynWidth = sGenBitOff (indexExpr, r.getLsb (), context);
      NU_ASSERT (not wasDynWidth, lv);
      out << ",";
    }
    else
    {
      if (size > LONG_BIT)
        out << ".lldeposit(";
      else
        out << ".ldeposit(";
      
      bool wasDynWidth = sGenBitOff (indexExpr, r.getLsb (), context & ~eCGLvalue);

      out << ",";
      if (wasDynWidth) {
        out << TAG;
        indexExpr->emitCode ((context | eCGDownToValid) & ~eCGLvalue);
      } else {
        out << TAG << ((r.getMsb () - r.getLsb ()) + 1);
      }
      out <<  ", "
          << TAG << CBaseTypeFmt (size, eCGDefine);
    }
  }
  else
    // Large store to BitVector
  {
    NULvalue* lval = lv->getLvalue();
    result = lval->emitCode (context | eCGLvalue);
    if (indexExpr == NULL) {
      // This is a constant.  Verify it now.
      SInt32 offset = r.getLsb();
      NU_ASSERT((offset + size) <= lval->getBitSize(), lv);
      out << TAG << ".lpartsel_unchecked(";
    } else {
      // need to dynamically check
      out << TAG << ".lpartsel(";
    }
    bool wasDynWidth = sGenBitOff (indexExpr, r.getLsb(), context & ~eCGLvalue);
    out << ",";
    if (wasDynWidth) {
      out << TAG;
      indexExpr->emitCode ((context | eCGDownToValid) & ~(eCGLvalue | eCGDirtyOk));
    } else {
      out << TAG << size;
    }
    out << ")";
  }
  
  return result;
} // sEmitLPartsel

// Code the lvalue for a single bit access.
CGContext_t
NUVarselLvalue::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);
  NU_ASSERT(!isDirtyOk(context), this);

  if (isWholeIdentifier())
  {
    return getWholeIdentifier()->emitCode (context | eCGLvalue);
  }

  UtOStream &out = gCodeGen->CGOUT ();
  NUNet *baseObject = sFindNet (getLvalue ());

  size_t lbits = getLvalue()->getBitSize ();
  size_t baseBits = (baseObject ? elementSize (baseObject) : lbits);

  if ( (getLvalue()->getType() == eNUVarselLvalue) &&
       (baseBits <= LLONG_BIT) )
    return eCGDelayed;

  if (mRange.getLength () != 1)
    return sEmitLPartsel (this, context);

  ConstantRange base (lbits-1, 0);

  ConstantRange r(mRange);

  bool oob_safe = isIndexInBounds () 
    || sIsIndexInBounds (baseObject, getIndex (), base, getLoc (), true);

  if (isConstIndex()) {
    oob_safe = base.overlaps(r);
  }

  if (baseBits > LLONG_BIT)
  {
    CGContext_t rc = getLvalue()->emitCode(context | eCGLvalue | eCGHuge);

    bool notBitfield = sIsNestedVarselLvalue (this);
    if (notBitfield) {
      rc &= ~eCGBitfield;
      out << TAG << ".lpartsel(";
    } else {
      rc |= eCGBitfield;
      if (oob_safe)
        out << TAG << ".set(";
      else
        out << TAG << ".checked_set(";
    }

    if (isConstIndex()) {
      out << TAG;
      emitIndex (out, r.getLsb (), base);
    } else {
      out << TAG;
      emitIndex (out, getIndex (), r.getLsb (), context & ~eCGLvalue, base);
    }

    if (notBitfield)
      out << ",1)";
    else
      out << ",";

    return rc;
  }
  else 
  {
    if (isConstIndex () && not base.overlaps (r))
    {
      // Trying to write a bit beyond the size of the object..
      // Just let the destination expression evaluate, but cast it to
      // void to let the compiler know we don't want the value...
      // This shuts it up about "warning: statement has no effect"
      out << TAG << "((void)";
      return eCGBitfield;
    }
    else
    {
      if (sIsNestedVarselLvalue(this))
	return ( sGenNestedSetSlice (dynamic_cast<const NUVarselLvalue*>
				     (getLvalue()), 
				     context | eCGLvalue, getIndex (), 
				     getBitSize ()) | eCGBitfield);

      else if (not isConstIndex () && !oob_safe
          && ((static_cast<UInt64>(1) << getIndex ()->getBitSize ()) > lbits))
        // Non-constant indexing expression, and it is potentially
        // larger than the bounds of the vector we're accessing.
        //
        out << TAG << "CSET_BIT (" << lbits << ",";
      else
        out << TAG << "SET_BIT (";

      // What happens if the base object is a VarselLvalue itself?
      //
      // JDM: as of 5/26/05, this situation should be eliminated by
      // ConcatRewrite.  It can only arise as a result of port splitting,
      // at least in Verilog.  It's possible VHDL might be able to get us
      // here in a way that ConcatRewrite does not yet support.
      //
      // It would need to produce an LVALUE suitable for the SET_BIT
      // macro - which will only be true if the Lvalue() generates a BVref,
      // and even then I'm dubious because the mask operations on the
      // BVref will do the wrong thing.
      //
      // We can handle memsels, or anything that isWholeIdentifier()
      NU_ASSERT (getLvalue ()->isWholeIdentifier ()
                 || getLvalue ()->getType () == eNUMemselLvalue,
                 this);

      getLvalue()->emitCode (context | eCGLvalue);
      out << ", ";

      if (isConstIndex ()) {
        out << TAG;
        emitIndex (out, r.getLsb (), base);
      } else {
        out << TAG;
        emitIndex(out, getIndex(), r.getLsb (), context & ~eCGLvalue, base);
      }

      out << ", " << TAG << CBaseTypeFmt (lbits, eCGDefine);
    }

    return eCGBitfield;
  }
}

// Is the expression \a index a constant value bounded by the \a range (which may be
// unknown (e.g. NULL).
//
static bool sIsBoundedConstant (const ConstantRange* range, const NUExpr*index)
{
  if (range == 0)
    return false;

  const NUConst*c = index->castConst ();
  SInt32 memIndex;

  return c && c->getL (&memIndex) && range->contains (memIndex);
}

// Select a word from a memory.  Must handle big-endian and
// little-endian base identifiers.
//
CGContext_t
NUMemselRvalue::emitCode (CGContext_t context) const
{
  bool doOpEqual = isOpEqual (context) || isLvalue (context);
  bool flowContext = isFlowValue (context);
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr | eCGDirtyOk | eCGOpEqual | eCGLvalue);

  UtOStream &out = gCodeGen->CGOUT ();

  NUMemoryNet *ident = getIdent()->getMemoryNet();
  NU_ASSERT (ident, this);

  size_t memBits = ident->getRowSize ();
  
  // Can't use coerceRvalue() here because it will coerce the whole memory,
  // not the element we're accessing.
  bool coerce = ((getBitSize () != memBits)
                 // Check if default access to memory has different
                 // sign than HDL semantics want for this access.
                 || (ident->isSigned () != isSignedResult ())
                 || (NeedsATemp (context) && memBits > LLONG_BIT))
    && not doOpEqual;

  out << TAG;

  if (coerce)
    out << CBaseTypeFmt (getBitSize (), eCGDeclare, isSignedResult ()) << "(";

  // Indicate that we aren't accessing the memory as a value, but as a lvalue.
  //
  mIdent->emitCode ((context | eCGLvalue) & ~eCGNeedsATemp);

  // Add the indexing code

  // Use read() methods for all memory accesses (unless we're an OP=)

  // Check to see if we need bounds checking or not.  We do not need bounds
  // checking if the index safely indexes the depth or if we've specified -noOOB
  // Note that we always read from the real memory, not the temp memory,
  // so use getMemoryNet rather than dynamic_cast.
  //
  // LValue context will be true if we're doing an OP= to a memory word...
  
  const ConstantRange *depth_range = ident->getDepthRange();
  if (sIsBoundedConstant (depth_range, getIndex (0))
      || gCodeGen->mNoOOB 
      || (depth_range && depth_range->safeIndex(getIndex(0)->getBitSize()))) {
    out << TAG << (doOpEqual ? ".writeNoCheck(" : ".readNoCheck(");
  } else {
    out << TAG << (doOpEqual ? ".write(" : ".read(");
  }

  CGContext_t icontext;
  if (getIndex (0)->getBitSize () > LLONG_BIT) {
    out << TAG;
    icontext = sGenLLValue (getIndex (0), 0, (context & ~eCGNeedsATemp));
  } else {
    out << TAG;
    icontext = getIndex(0)->emitCode(context & ~eCGNeedsATemp);
  }

  // test/unit-codegen/trunc_ass.v
  if( !isPrecise(icontext) )
    mask( getIndex(0)->getBitSize(), "&" );

  out << ")"; // close the .read(

  CGContext_t rc = eCGPrecise;  // Assume accurate value returned

  if (memBits > LLONG_BIT && not isLvalue (context)) {
    // Memory contains BitVectors members - if we want a smaller
    // amount (we can return JUST 64 or 32 bits (if this operand
    // was being BVOverload-ed), or if we only need fewer bits
    //
    UInt32 destSize = getBitSize ();
    if (destSize <= LONG_BIT)
      out << TAG << ".value()";
    else if (destSize <= LLONG_BIT)
      out << TAG << ".llvalue()";

    if (destSize != LONG_BIT || destSize != LLONG_BIT)
      rc = Imprecise (rc); // We're returning dirty bits!

    if (flowContext)
      out << ".any()";
  }

  if (coerce)
    out << ")";

  if (NeedsATemp (context) && coerce)
    return rc | eCGIsATemp;
      
  return rc;
}

// Code the lvalue for writing to a memory index.
CGContext_t
NUMemselLvalue::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr | eCGDirtyOk);

  UtOStream &out = gCodeGen->CGOUT ();

  // We always write the entire width so we can just use the natural size
  CGContext_t rc = getIdent ()->emitCode (context | eCGLvalue);

  // Check to see if we need bounds checking or not.  We do not need bounds
  // checking if the index safely indexes the depth.  We don't
  // bounds check temp memories while writing, only while updating,
  // so use dynamic_cast rather than getMemoryNet.
  NUMemoryNet *ident = dynamic_cast<NUMemoryNet*>(getIdent());
  const ConstantRange *depth_range = ident ? ident->getDepthRange() : 0;

  if (sIsBoundedConstant (depth_range, getIndex (0))
      || gCodeGen->mNoOOB or (depth_range and depth_range->safeIndex(getIndex(0)->getBitSize()))) {
    out << TAG << ".writeNoCheck(";
  } else {
    out << TAG << ".write(";
  }

  CGContext_t ctxt;

  if (getIndex (0)->getBitSize () > LLONG_BIT) {
    out << TAG;
    ctxt = sGenLLValue (getIndex (0), 0, context&~eCGLvalue);
  } else {
    out << TAG;
    ctxt = getIndex (0)->emitCode (context & ~eCGLvalue);
  }

  if(!isPrecise(ctxt))
    mask(getIndex(0)->getBitSize(), "&");
  out << ")";

  // bug4000 - if we are accessing a stateupdate memory, then the "write" method actually returns
  // a proxy object (a 'struct reference').  in order to actually write to this, we want to expose
  // a c++ reference to the underlying valuetype of the memory.  But if we're initializing the
  // memory, then we don't go thru the reference object....
  //
  if (ident && ident->isDoubleBuffered () && not isInitialBlock (context))
    out << ".ref()";


  return rc;
}

void CGAuxInfo::setReferenced ()
{
  mFlags = OpFlags (mFlags | eOpReferenced);
}
