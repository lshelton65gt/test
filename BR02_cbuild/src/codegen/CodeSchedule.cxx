// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implement schedule-related code generation.
  Code for generating schedule.cxx, libdesign.h, main.cxx
*/

#include "util/CarbonPlatform.h"

#include <cstdlib>
#include <functional>

#include "emitUtil.h"
#include "emitTFCall.h"
#include "GenSchedFactory.h"

#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/CarbonDBWrite.h"

#include "iodb/IODBNucleus.h"

#include "flow/FLNodeElab.h"
#include "nucleus/NUCycle.h"

#include "nucleus/NUDesign.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "schedule/Event.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Schedule.h"

#include "NetMapCompare.h"
#include "util/DataToObj.h"
#include "util/StringAtom.h"
#include "util/OSWrapper.h"
#include "util/CarbonTypeUtil.h"
#include "CGTriggerFactory.h"
#include "langcpp/LangCppCarbonGlobal.h"
#include "codegen/CGNetElabCount.h"
#include "codegen/ScheduleBucket.h"
#include "CGTopLevelSchedule.h"
#include "CGFindInstances.h"

extern const char* gCarbonVersion();

using namespace emitUtil;


// Test for a top-level bidirect net
//
bool CodeGen::isBidirect (const NUNet* n) const
{
  return (n->isPrimaryBid() or n->isPrimaryInput()
          or (n->isPrimaryZ() and n->isForcible()))
    and isTristate (n);
}

// Nets with vtabs need extra computations for access sizes.
// Right now, no carbon simulation classes have virtual members.
//
bool CodeGen::hasVtab(const NUNet*)
{
  return false;
}

static size_t sVtabAdjustmentTristate(size_t /*s*/)
{
  // The first data member must be correctly aligned.  This was only
  // an issue when the data member is a long long AND we're running on
  // machine with extra alignment (like solaris which is no longer supported)
  return sizeof(UInt32);
}

static size_t sVtabAdjustmentBidirect(size_t s)
{
  return sVtabAdjustmentTristate(s);
}

size_t CodeGen::vtabAdjustment(const NUNet* n)
{
  if (hasVtab(n)) {
    if (isBidirect(n)) {
      return sVtabAdjustmentBidirect(netByteSize(n));
    } else if (isTristate(n)) {
      return sVtabAdjustmentTristate(netByteSize(n));
    } else {
      NU_ASSERT ("Bidirect or Tristate expected" == 0, n);
      return 0;
    }
  } else {
    return 0;
  }
}

// Lookup a schedule by name in the 
// Format a net temporary needed to hold the old value of a PI when it is 
//   isInDataPath() && isInClkPath()
//
CodeGen::CxxName piTemp ("tm_", "");

// Net name translator for constructing schedule functions - just slap
// an underscore after the printable version of the name.
CodeGen::CxxName printable ("_","");

static int sCheckChanged = -1;
static int sReadBeforeWriteCount = 0;

// This rather simple routine is used for debugging emitting changed[n] numbers
int CodeGen::writeChanged(int changed)
{
  // very exciting, isn't it.  But handy if debugging.
  if (changed == sCheckChanged)
    fprintf(stdout, "stop here!\n");

  mChangedWritten.insert(changed);
  return changed;
}

// This rather simple routine is used for debugging emitting changed[n] numbers
int CodeGen::readChanged(int changed)
{
  // very exciting, isn't it.  But handy if debugging.
  if (changed == sCheckChanged)
    fprintf(stdout, "stop here!\n");

  if (mChangedWritten.find(changed) == mChangedWritten.end())
  {
    ++sReadBeforeWriteCount; // read before write
  }
  return changed;
}

// Output the cycle declarations into the top class header file
void CodeGen::emitCycleDeclarations()
{
  NUCycle* cycle;
  for (SCHSchedule::CycleLoop i = mSched->loopCycles(); i(&cycle);)
  {
    if (cycle->isScheduled ())
      cycle->emitDeclaration (CGOUT (), mFunLimit);
  }
}

//! Generate boiler-plate #includes to current output file.
void
CodeGen::codeScheduleBoilerPlate (void)
{
  UtOStream& out = getCGOFiles()->CGOUT ();

  // Generate #include for each instantiated top-level class
  // This needs to be the first thing in the file because it can
  // be a precompiled header file.
  //
  NUModuleList modules;
  mTree->getTopLevelModules (&modules);
  for (NUModuleList::iterator p = modules.begin(); p != modules.end(); ++p)
  {
    NUModule *mod = *p;
    mCGOFiles->generateHeaderInclude (out, mod);
  }

  // By requesting these bottom up, from private subdirs, we will find
  // them in our private carbon.ia file rather than the
  // cleartext we distribute.  We do this simply to avoid another
  // file search when compiling schedule.cxx
  out << "#include \"util/CarbonPlatform.h\"\n";
  out << "#include \"carbon/carbon_shelltypes.h\"\n";
  out << "#include \"shell/carbon_capi.h\"\n";
  out << "#include \"shell/carbon_model.h\"\n";
  out << ENDL;
  out << "\n";


  // Include the "interface" to the outside world
  //
  out << CRYPT("#include \"") << mTargetName << ".h\"\n"
      << CRYPT("#include \"schedule.h\"\n");
}


//! Class to emit fixups for any pointer based hier refs
/*!
 * This class is used to find all the hier refs and fixup the pointers
 * for any non-local hier refs. For example, for nets:
 *
 * \verbatim
 * module top (out, clk, in);
 *   output out;
 *   input  clk, in;
 *
 *   child c (out, clk);
 *
 * endmodule
 *
 * module child(q, clk);
 *   output q;
 *   input  clk;
 *
 *   reg    q;
 *   always @ (posedge clk)
 *     q <= top.in;
 * 
 * endmodule
 * \endverbatim
 *
 * In this example the class declaration for child will have:
 *
 * \verbatim
 *   UInt1* m_$top_in;
 *   UInt1& get_$top_in() { return *m_$top_in; }
 * \endverbatim
 *
 * The function for the always block in child will have:
 *
 * \verbatim
 *   get_q() = get_$top_in();
 * \endverbatim
 *
 * To make the dereference on m_$top_in work correctly the design
 * initialization must store the address of top.m_in into
 * top.m_c.m_$top_in. This class is responsible for generating that
 * code which should look like:
 *
 * \verbatim
 *   top.m_c.m_$top_in = $top.m_in;
 * \endverbatim
 *
 * A similar fixup is done for tasks but it is more complex because
 * storing a pointer to a member function is relatively complex. So
 * instead, we store a pointer to the parent module. This is
 * compounded by the fact that if we have multiple resolutions
 * (unelaboratedly), there could be more than one parent module which
 * means we also need a functor class to make the call.
 *
 * The following is a simple example:
 *
 * \verbatim
 * module top(out, in1, in2, clk);
 *   output [1:0] out;
 *   input [1:0]  in1, in2;
 *   input        clk;
 *
 *   task doand;
 *     output [1:0] o;
 *     input [1:0] i1, i2;
 *     o = i1 & i2;
 *   endtask
 *
 *   child child (out, clk, in1, in2);
 *
 * endmodule
 *
 * module child(o, clk, i1, i2);
 *   output [1:0] o;
 *   input        clk;
 *   input  [1:0] i1, i2;
 *
 *   reg [1:0] o;
 *   always @(posedge clk)
 *     top.doand(o, i1, i2);
 *
 * endmodule
 * \endverbatim
 *
 * In this example the task enable call top.doand is done by pointer
 * since it is not in a child instance. So module child must have a
 * pointer to module top as follows:
 *
 * \verbatim
 *   c_top* m_$top;
 * \endverbatim
 *
 * And the call in the always block looks like:
 *
 * \verbatim
 *   m_$top->f$doand(get_o(), m_i1, m_i2);
 * \endverbatim
 *
 * For this to work the value of the m_$top variable must be fixed up
 * after construction. This is done as follows:
 *
 * \verbatim
 *   top.m_child.m_$top = &top;
 * \endverbatim
 *
 * If on the other hand there were multiple resolutions, a situation
 * that can be created by flattening, a single pointer won't
 * work. Since this cannot be created with a Verilog test case (we
 * don't allow it), we assume that there are two resolutions to \<task\>
 * to \<module1\> and \<module2\>. The path in the original HDL of the
 * call is in \<path\>. Here is some sample output:
 *
 * \verbatim
 *   class FI$<path>$<task>
 *   {
 *   public:
 *     virtual void f$<task>(UInt1& o, const UInt1& i1, const UInt1& i2) = 0;
 *   };
 *   class FI$<path>$<task>$<module1>
 *   {
 *   public:
 *     void f$<task>(void* parent, UInt1& o, const UInt1& i1, const UInt& i2)
 *     {
 *        ((c_<module1>*)parent)->f$<task>(o, i1, i2);
 *     }
 *   };
 *   class FI$<path>$<task>$<module2>
 *   {
 *   public:
 *     void f$<task>(void* parent, UInt1& o, const UInt1& i1, const UInt& i2)
 *     {
 *        ((c_<module2>*)parent)->f$<task>(o, i1, i2);
 *     }
 *   };
 *   void* m_fp$<path>$<task>;
 *   FI$<path>$<task>* m_fp$<path>$<task>$fi;
 * \endverbatim
 *
 * Note that there is one abstract base class and two derived classes,
 * one for each resolution. These are functor classes to convert the
 * void* parent pointer and call the right task.
 *
 * This means we store two pointers in the class. One for the parent
 * module pointer (void*) and one for the functor class to call the
 * task. These must be filled in after construction as follows (which
 * this code emits):
 *
 * \verbatim
 *   <path-to-call1-inst>.m_fp$<path>$<task> = &<path-to-module1-inst>;
 *   <path-to-call1-inst>.m_fp$<path>$<task> = new FI$<path>$<task>$<module1>;
 *   <path-to-call2-inst>.m_fp$<path>$<task> = &<path-to-module2-inst>;
 *   <path-to-call2-inst>.m_fp$<path>$<task> = new FI$<path>$<task>$<module2>;
 * \endverbatim
 *
 */
class FixupHierRefCallback : public CGFindInstancesCallback
{
public:
  //! constructor
  FixupHierRefCallback(STSymbolTable* symTab, CodeGen* codeGen, CGOFiles* cgOFiles) :
    CGFindInstancesCallback(symTab, codeGen, cgOFiles)
  {}

  //! destructor
  ~FixupHierRefCallback() {}

protected:
  //! emit initialization statements for hier ref nets and tasks
  void handleModule(STBranchNode* branch, NUModule* mod)
  {
    // Create a string for the path to the hier ref in this module
    UtString hierRefPath;
    getCodeGen()->CxxHierPath(&hierRefPath, branch, ".");

    // Handle non-local net hier refs
    emitNetHierRefInits(hierRefPath, mod, branch);

    // Handle non-local task hier refs
    emitTaskHierRefInits(hierRefPath, mod, branch);
  }

private:
  void emitNetHierRefInits(const UtString& hierRefPath, NUModule* mod,
                           STBranchNode* branch)
  {
    UtOStream& out = getCGOFiles()->CGOUT ();
    // Loop over the net hier refs looking for non-local ones
    for (NUNetLoop l = mod->loopNetHierRefs(); !l.atEnd(); ++l)
    {
      NUNet* net = *l;
      const NUNetHierRef* hierRef = net->getHierRef();

      // Get the path to the real net, we can find this by
      // elaborating the net.
      NUNetElab* netElab = net->lookupElab(branch);
      NU_ASSERT(netElab != NULL, net);

      // the storage-check is to fix bug3085.  Do not emit the hier-ref
      // fixup for dead NUNetElabs.  We know that all the live ones have
      // non-null internal storage because of the NU_ASSERT calls at
      // the end of REAlloc::sanityCheck().
      CGAuxInfo *cg = net->getCGOp ();
      if (!net->getCGOp()->isUnused() && (!net->isDead () || cg->isReferenced ())
          && !getCodeGen()->isLocallyRelativeHierRef(net) &&
          (netElab->getSymNode()->getInternalStorage() != NULL))
      {
        // Create the assignment of the hier ref = & real net;
        out << "  " << hierRefPath << ".";
        hierRef->emitCode(eCGNameOnly);

        out << " = ";

// #if BUG1993
        // Work around problem with netConstPrefix() thinking that
        // inputs that are hierref-read are written.
        NUNet* allocNet;
        const STBranchNode* parent;
        allocNet = emitUtil::findAllocatedNet(netElab, &parent);
        if (emitUtil::isNetConst(allocNet) || isConstAssignNet(allocNet)) {
          out << "(" << CType (allocNet)
              << "*)";
        }
// #endif

        // Note this must use getStorageNet() because that is the net which is actually emitted.
        if (!emitUtil::isNetDeclared (netElab->getStorageNet ())) {
          // The referenced net is not declared so initialise the hierrarchical reference to NULL.
          out << "NULL";
        } else {
          out << "&";

          if (not emitUtil::isTypeCompatible (allocNet, net)
            && allocNet->getBitSize () == net->getBitSize ()
            && not gCodeGen->isTristate (net)
            && gCodeGen->isTristate (allocNet))
            {
              // These aren't type-compatible, but they're the same size and the
              // LHS is type T while RHS is Tristate<T>.
              // connecting a net to a tristate or bidirect net - use Tristate::operator T&()
              // cast to access the base type - and obey any const'ness on the dest

              out << "(" << netConstPrefix (net) << CBaseTypeFmt (net, eCGVoid) << "&)";
            }

          netElab->emitCode(eCGNameOnly|eCGHierName);
        }

        out << ";\n";

        // Check for some error conditions that we don't handle. These
        // should really be resolved earlier but in the interest of
        // expedience for a customer, we are checking for them
        // here. We have to rethink how we deal with hier refs and
        // tristates.
        if (net->isWritten() && !net->isZ() && netElab->getNet()->isZ()) {
          // We are doing a hier ref write from a non-tristate to a
          // tristate. This does not work.
          MsgContext* msgContext = getCodeGen()->getMsgContext();
          msgContext->CGInvTriWrite(net);
          
        } else if (net->isWritten() && net->isZ() &&
                   !netElab->getNet()->isZ()) {
          // A hier ref thinks it is tristate but it is referencing a
          // non-tristate. This doesn't work either.
          MsgContext* msgContext = getCodeGen()->getMsgContext();
          msgContext->CGInvNonTriWrite(net);

        }
      } // if
    } // for
  } // void emitNetHierRefInits

  bool isConstAssignNet(NUNet* net)
  {
    CGAuxInfo* cgop = net->getCGOp();
    return cgop->isStaticAssign();
  }

  //! Emit the task hier ref fixups
  /*! This routine loops over all the hierarchical references to tasks
   *  in the current module instance and emits the parent module
   *  fixups as described above. This is done by calling the
   *  emitTaskHierRefInit routine.
   *
   *  Note that for single-resolution pointers we have implemented
   *  caching so that we only store one module pointer for every
   *  unique module. But we didn't implement it for multi-resolution
   *  pointers so caching is not enabled.
   */
  void emitTaskHierRefInits(const UtString& hierRefPath, NUModule* mod,
                            STBranchNode* branch)
  {
    // Loop over the task hier refs looking for non-local ones. Emit
    // fixups for each instance. See below for details.
    UtSet<UtString> covered;
    NUTaskEnableVector taskEnables;
    mod->getTaskEnableHierRefs(&taskEnables);
    for (NUTaskEnableCLoop l(taskEnables); !l.atEnd(); ++l)
    {
      // Only care about non-locally relative task hier refs
      NUTaskEnable* enable = *l;
      NUTaskHierRef* taskHierRef = enable->getHierRef();
      if (!taskHierRef->isLocallyRelative())
      {
        UtString parentVar;
        CGTFgenHierRefTaskName( eParentVar, taskHierRef, NULL, &parentVar );
        // Cache the parent modules so we don't emit them more than once.
        //
        // See the emitTaskHierRefInit function for details on how we
        // emit the fixups for single and multi-resolution task hier
        // refs.
        if( covered.find( parentVar ) == covered.end() ) {
          covered.insert( parentVar );
          emitTaskHierRefInit(mod, hierRefPath, branch, enable);
        }
      }
    } // for
  } // void emitTaskHierRefInits

  //! Emit the fixup statements for this hier ref resolution
  /*! This routine emits the fixup for a single instance of a hier ref
   *  task enable call.
   *
   *  If it is a simple one resolution reference then there is a
   *  single fixup statement. For multi-resolution fixups there are
   *  two fixup statements, one for the parent module and one for the
   *  functor class pointer to make the appropriate call.
   *  
   */
  void emitTaskHierRefInit(NUModule* enableModule,
                           const UtString& hierRefPath,
                           STBranchNode* branch,
                           NUTaskEnable* enable)
  {
    // Count the resolutions because the type of fixup is different
    // for multiple resolutions.
    NUTaskHierRef* taskHierRef = enable->getHierRef();
    int resolutionCount = taskHierRef->resolutionCount();
    NU_ASSERT(resolutionCount > 0, enable);

    // Find the branch node for the task resolution
    const AtomArray& taskPath = taskHierRef->getPath();
    STSymbolTableNode* taskNode;
    taskNode = NUHierRef::resolveHierRef(taskPath, 0, getSymtab(), branch);
    NU_ASSERT(taskNode != NULL, enable);

    // Convert the symbol table resolution to an NUTask and get its
    // parent
    STBranchNode* taskBranch = taskNode->castBranch();
    NUTFElab* tfElab = NUTFElab::lookup(taskBranch);
    NUTask* task = dynamic_cast<NUTask*>(tfElab->getTF());
    NUModule* taskModule = task->findParentModule();

    // Find the elaborated parent scope for the task. It isn't
    // always the immediate parent because flattening might have
    // moved it.
    STBranchNode* parentScope = taskNode->getParent();
    NUModuleElab* moduleElab = NUModuleElab::lookup(parentScope);
    while (moduleElab->getModule() != taskModule)
    {
      parentScope = parentScope->getParent();
      moduleElab = NUModuleElab::lookup(parentScope);
    }

    // Create a name for the fixup data (LHS)
    UtString parentVar;
    CGTFgenHierRefTaskName(eParentVar, taskHierRef, NULL, &parentVar);

    // For both single and multi-resolution fixups, we need to store
    // the parent module for the task. Create the LHS of the
    // assignment of the hier ref =
    UtOStream& out = getCGOFiles()->CGOUT ();
    out << "  " << hierRefPath << "." << parentVar  << " = ";

    // Create the RHS of the assignment. This is the fully
    // elaborated scope of the resolved task's parent module.
    UtString actualPath;
    getCodeGen()->CxxHierPath(&actualPath, parentScope, ".");
    out << "&" << actualPath << ";\n";

    // For multi-resolution fixups, we also need to create the
    // functor class
    if (resolutionCount > 1)
    {
      // Emit the LHS of the fixup
      UtString functorPtr;
      CGTFgenHierRefTaskName(eFunctorPtr, taskHierRef, NULL, &functorPtr);
      out << "  " << hierRefPath << "." << functorPtr << " = ";

      // Create the name of the class
      UtString className;
      className << pclass(enableModule);
      CGTFgenHierRefTaskName(eFunctorDerived, taskHierRef, taskModule, &className);

      // Create the new class for the right hand side. Note this must
      // be deleted in the design destructor.
      out << "new " << className << ";\n";
    }
  } // void emitTaskHierRefInit
}; // class FixupHierRefCallback : public CGFindInstancesCallback

//! Class to emit deletes of functor clases for pointer task hier refs
/*! See the FixupHierRefCallback class above for details of when
 *  functor classes are allocated. This class does a similar walk and
 *  deletes and memory that was allocated by that class.
 */
class DeleteFunctorCallback : public CGFindInstancesCallback
{
public:
  //! constructor
  DeleteFunctorCallback(STSymbolTable* symTab, CodeGen* codeGen, CGOFiles* cgOFiles) :
    CGFindInstancesCallback(symTab, codeGen, cgOFiles)
  {}

  //! destructor
  ~DeleteFunctorCallback() {}

protected:
  //! emit deletes for functor class allocations
  void handleModule(STBranchNode* branch, NUModule* mod)
  {
    // Loop over all the task enables in this module
    NUTaskEnableVector taskEnables;
    mod->getTaskEnableHierRefs(&taskEnables);
    for (NUTaskEnableCLoop l(taskEnables); !l.atEnd(); ++l)
    {
      // Only care about non-locally relative hier refs with multiple
      // resolutions since we use functor pointers for them.
      NUTaskEnable* enable = *l;
      NUTaskHierRef* taskHierRef = enable->getHierRef();
      if (!taskHierRef->isLocallyRelative() &&
          (taskHierRef->resolutionCount() > 1))
      {
        // Create a string to represent the call. We use the text from
        // the original HDL hier ref and replace .'s (which are not
        // legal name characters) with $'s. This way we won't conflict
        // with any user names.
        UtString hierRefPath;
        getCodeGen()->CxxHierPath(&hierRefPath, branch, ".");
        UtString functorPtr;
        CGTFgenHierRefTaskName(eFunctorPtr, taskHierRef, NULL, &functorPtr);
        getCGOFiles()->CGOUT() << "  delete " << hierRefPath << "."
                              << functorPtr << ";\n";
      }
    } // for
  } // void handleModule
}; // class DeleteFunctorCallback : public CGFindInstancesCallback

/*!
 */
void CodeGen::generateCarbonSimulation(UtOStream& out, const StringAtom* name)
{
  out << CRYPT("  carbon_simulation &") << noprefix (name)
      << CRYPT(" UNUSED = *reinterpret_cast<carbon_simulation *>(descr->mHdl);\n\n");
}

//! Class to emit change detect initialization code
class ChangeDetectInitCallback : public CGFindInstancesCallback
{
public:
  //! constructor
  ChangeDetectInitCallback(STSymbolTable* symTab, CodeGen* codeGen, CGOFiles* cgOFiles) :
    CGFindInstancesCallback(symTab, codeGen, cgOFiles)
  {}

  //! destructor
  ~ChangeDetectInitCallback() {}

protected:
  //! Emit initialization statements for the change detect
  /*! Change detects should return true at time 0. To do this we emit
   *  code that set the old value of the temp as follows:
   *
   *  <inst-path>.initChangeDetects();
   */
  void handleModule(STBranchNode* branch, NUModule* mod)
  {
    // Create a string for the path to the hier ref in this module
    UtString instPath;
    getCodeGen()->CxxHierPath(&instPath, branch, ".");

    // Make sure we have some change detects in this module. We may
    // want to add a hasChangeDetects routine to NUModule.
    NUChangeDetectVector changeDetects;
    mod->getChangeDetects(&changeDetects,true);
    if (changeDetects.empty()) {
      return;
    }
    
    // There are change detects, call initChangeDetects() in this
    // instance
    getCGOFiles()->CGOUT() << "    " << instPath << ".initChangeDetects();\n";
  }
}; // class ChangeDetectInitCallback : public CGFindInstancesCallback

//! Class to emit initialization of instance name pointers
class InstNameCallback : public CGFindInstancesCallback
{
public:
  //! constructor
  InstNameCallback(STSymbolTable* symTab, CodeGen* codeGen,
                   CGOFiles* cgOFiles) :
    CGFindInstancesCallback(symTab, codeGen, cgOFiles)
  {}

  //! destructor
  ~InstNameCallback() {}

protected:
  //! Emit intialization statements for the instance names
  void handleModule(STBranchNode* branch, NUModule* mod)
  {
    // We only have instance names if this module has a $display
    // variant that prints %m.
    if (mod->hasDisplaysWithHierarchicalName()) {
      // Print a string for the path to the instance name
      UtString instPath;
      getCodeGen()->CxxHierPath(&instPath, branch, ".");
      getCGOFiles()->CGOUT() << "   " << instPath << ".m_cds_inst_name = ";

      // Print a string for the full path
      UtString instName, validated;
      branch->compose(&instName);
      StringUtil::escapeCString(instName.c_str(), &validated);
      getCGOFiles()->CGOUT() << "\"" << validated << "\";\n";
    }
  }
}; // class InstNameCallback : public CGFindInstancesCallback

/*!
 * Output the code to the mScheduleSrc stream for the
 * instantiation of the top-level C++ object, the schedule functions
 * for various clocks, asynchronous schedules, etc.
 */
void
CodeGen::codeInitialization (void)
{
  switchStream (eCGSchedule);

  codeScheduleBoilerPlate ();	// Common stuff needed to compile schedules


  // Include shell hookup object.
  CGOUT () << CRYPT("#include \"shell/carbon_model.h\"\n");
  
  if (mEmbedIODB) {
    CGOUT()  << CRYPT("\n\void carbon_") << mIfaceTag
             << "_get_iodb(const char**, int*);\n"
             << CRYPT("void carbon_") << mIfaceTag
             << "_get_fulldb(const char**, int*);\n\n";
  }

  CGOUT () << CRYPT("#ifdef BVPERF\n")
	   << CRYPT("#include \"util/BVPerf.h\"\n")
	   << CRYPT("#endif\n")
	   << CRYPT("#ifdef MEMORYPERF\n")
	   << CRYPT("#include \"util/MemoryPerf.h\"\n")
	   << CRYPT("#endif\n")
	   << CRYPT("namespace ") << getNamespace () << "{\n"
	   << CRYPT("static void carbon_check_debug_info(carbon_simulation&);")
           << ENDL;

  // Document the compiler flags in effect for this run of cbuild.
  // Make it a global embedded in the simulation's name-space so it
  // can't be deleted....
  //
  // JDM 9/24/03 - This causes EMC/sys to fail due to embedded double-quotes
  // in the command-line, which get exposed now that Carbon expands -f files.
  //
  // We could deal with this, but since we do write libdesign.cmd now, we
  // will skip it.  If we want to resurrect this code, it will need to
  // backslash embedded double-quotes, and it probably should make a string-array
  // instead of a single long string.
#if 0
  CGOUT () << "const char cbuild_command[]={\"";
  char **args = mCarbonContext->getArgv ();
  do {
    CGOUT () << (*args++) << " ";
  } while (*args);
  CGOUT () << "\"};\n\n";
#endif    

  // encapsulated cycle definitions -- must precede _init
  // to teach the cycles what their top level name is before trying
  // to invoke them.
  for (SCHSchedule::CycleLoop i = mSched->loopCycles(); !i.atEnd(); ++i)
    {
      NUCycle* cycle = *i;

      if (not cycle->isScheduled ())
        continue;

      // Create Cycle objects and generate them - possibly into a separate
      // sch-<N>.cxx file.
      UtString cycleName ("cycle$");
      cycleName << cycle->getID ();

      GenSched* cycleGenerator
        = mGenSchedFactory->generate (cycleName.c_str (), cycle,
          getCGOFiles (),
          CRYPT ("carbon_cycles"));
      cycleGenerator->genMethod ();
    }

  // Create an initial schedule object
  GenSched* initGenerator
    = mGenSchedFactory->generate (CRYPT("initial_schedule"),
      mSched->getInitialSchedule (),
      getCGOFiles(),
      eHandleArg,
      CRYPT("carbon_infrequent_code"));
  initGenerator->genMethod ();

  // Create an initSettle schedule object
  GenSched* initSettleGenerator
    = mGenSchedFactory->generate (CRYPT("init_settle_schedule"),
      mSched->getInitSettleSchedule (),
      getCGOFiles(),
      eHandleArg,
      CRYPT("carbon_infrequent_code"));
  initSettleGenerator->genMethod ();

  // Create the construction function for this simulation that does not require
  // C++ compiler compatibility between model and shell.
  UtString construct_proto;
  construct_proto << CRYPT("carbon_simulation *carbon_") << mIfaceTag
                    << CRYPT("_construct()");
  CGOUT () << construct_proto;
  emitSection(CGOUT(), CRYPT("carbon_infrequent_code"));
  CGOUT () << CRYPT(";\n");
  CGOUT () << construct_proto << CRYPT("\n{\n");

  // Allocate various simulation objects
  CGOUT () << CRYPT("  // Create a BitVector Performance object") << ENDL
           << CRYPT("#ifdef BVPERF\n")
           << CRYPT("  BVPC = new BVPerf;\n")
           << CRYPT("#endif\n")
           << CRYPT("#ifdef MEMORYPERF\n")
           << CRYPT("  MemPC = new MemoryPerf;\n")
           << CRYPT("#endif\n")
           << CRYPT("  // Allocate a simulation object\n")
           << CRYPT("  carbon_simulation& ") << noprefix (mTopName)
           << CRYPT(" = *(new carbon_simulation);\n")
           << CRYPT("#ifdef BVPERF\n")
           << CRYPT("  BVPC->resetAll();\n")
           << CRYPT("#endif\n")
           << CRYPT("#ifdef MEMORYPERF\n")
           << CRYPT("  MemPC->reset();\n")
           << CRYPT("#endif\n\n")
           << CRYPT("\n");

  // Add instance names to all the modules that need them
  CGOUT() << "\n"
          << "// Code to add instance names to all modules that need them\n";
  InstNameCallback instCallback(mIODB->getDesignSymbolTable(), this,
    getCGOFiles());
  // when looking for %m make sure we visit all instances (because
  // each has a unique name)
  NUDesignWalker instWalker(instCallback,
    false, /* not verbose */
    false /* visit all instances*/ );
  instWalker.design(mTree);
  CGOUT() << "\n";

  // Initialize the activity testing
  CGOUT () << "#ifdef CARBON_ACTIVITY" << ENDL
           << "  // Allocate space for activity detection\n"
           << "  " << noprefix (mTopName)
           << CRYPT(".allocDataCopy(sizeof(carbon_simulation));\n")
           << "#endif\n\n";

  // Create all the c-models by calling the user's create function
  codeCModelCreate ();

  // Validate the debug symbol table
  CGOUT () << CRYPT("  carbon_check_debug_info (") << noprefix (mTopName) << ");\n\n";

  // If stats are enabled, print how long init took
  CGOUT() << "\n";
  // Return the handle
  CGOUT () << CRYPT("  return &") << noprefix (mTopName) << ";\n";

  // Close the function
  CGOUT() << "}\n\n";

  // Function for hierref-fixup.
  CGOUT() << "// Method to fixup the pointers used for hierarchical references.\n";
  CGOUT() << "CarbonUInt32 " << emitUtil::mclass(getTopModule()->getName()) << "::fixup_hierref_pointers()\n";
  CGOUT() << "{\n";
  CGOUT() << "  // update non-local hierarchical references.\n";
  CGOUT() << "  carbon_simulation & " << emitUtil::noprefix (mTopName) << " UNUSED = *(this);\n";

  // Fixup all the non-local hier refs
  FixupHierRefCallback hrCallback(mIODB->getDesignSymbolTable(), this, getCGOFiles());
  NUDesignWalker hrWalker(hrCallback, false, false);
  hrWalker.design(mTree);
  
  // Close the function.
  CGOUT() << "  return 0;\n";
  CGOUT() << "}\n\n";

  //
  // Create the init function for simulation.
  // Also emit a declaration and place into the infrequent section.
  UtString init_proto;
  init_proto << CRYPT("void carbon_") << mIfaceTag
             << CRYPT("_init(carbon_model_descr *descr, ");

  // Note that if there are no c-models then the cmodel data argument
  // is not needed.
  if (!mTree->loopCModelInterfaces().atEnd())
    init_proto << CRYPT("void* cmodelData, ");
  else
    init_proto << CRYPT("void* /* cmodelData */, ");

  // We have added a couple of extra void*'s for future changes
  init_proto << CRYPT("void* /* reserved1 */, void* /* reserved2 */)");

  CGOUT () << init_proto;
  emitSection(CGOUT(), CRYPT("carbon_infrequent_code"));
  CGOUT () << CRYPT(";\n");
  CGOUT () << init_proto << CRYPT("\n{\n");

  genSetCurrentModel(CGOUT());

  // Create the carbon simulation
  generateCarbonSimulation(getCGOFiles()->getScheduleSrc(), mTopName);

  // Initialize all the c-models by calling the user's misc function
  //
  if (!mTree->loopCModelInterfaces ().atEnd ())
    // Dummy use just in case no CModel initialization needed.
    CGOUT () << "  (void) cmodelData;\n";

  codeCModelInitialization ();

  // Register all the playback type c-models with the CarbonModel
  // replay system. 
  codeCModelRegistration();

  CGOUT () << "  // only run the initial schedules if we are not in playback mode.\n";
  CGOUT () << "  if (descr->getRunMode() != eCarbonRunPlayback) {\n";
  // Run the initial schedule and init settle schedules to set everything up
  //
  CGOUT () << initGenerator->genCall ();
  CGOUT () << initSettleGenerator->genCall ();

  // Emit code to initialize all the change detects. This must run
  // after the initial schedule is run so that all nets are at an
  // initial value.
  CGOUT() << "\n"
          << "    // Code to initialize change detect temps\n";
  ChangeDetectInitCallback cdCallback(mIODB->getDesignSymbolTable(), this, getCGOFiles());
  NUDesignWalker cdWalker(cdCallback, false, false);
  cdWalker.design(mTree);

  // If stats are enabled, print how long init took
  CGOUT() << "\n";
  CGOUT() << CRYPT("  if (descr->printStatistics ())\n");
  CGOUT() << "  {\n";
  CGOUT() << CRYPT("    descr->printIntervalStatistics (\"DesInit\");\n");
  CGOUT() << CRYPT("    descr->resetScheduleCallCount ();\n");
  CGOUT() << "  }\n";
  CGOUT () << "  } // if not in playback mode\n";

  genRestoreCurrentModel(CGOUT());

  // Close the function
  CGOUT() << "}\n";
  CGOUT () << "}\n";		// and the namespace
}

/*!
 * Output the code to the mScheduleSrc stream for the top level mode
 * change function.
 */
void
CodeGen::codeModeChange(void)
{
  // Emit the c-model mode change code in its own file
  codeCModelModeChange();

  // Create the start of the mode change function
  UtString modeChangeProto;
  modeChangeProto << CRYPT("void carbon_") << mIfaceTag
                  << CRYPT("_modeChange(carbon_model_descr *descr")
                  << CRYPT(", void* userContext")
                  << CRYPT(", CarbonVHMMode from")
                  << CRYPT(", CarbonVHMMode to)");
  CGOUT() << modeChangeProto;
  emitSection(CGOUT(), CRYPT("carbon_infrequent_code"));
  CGOUT() << CRYPT(";\n");
  CGOUT() << modeChangeProto << CRYPT("\n{\n");

  // Emit code to get the carbon_simulation type
  generateCarbonSimulation(getCGOFiles()->getScheduleSrc(), mTopName);
  
  CGOUT() << "  CarbonObjectID *context = reinterpret_cast <CarbonObjectID *> (descr->mModel);\n";
  // Call the c-model mode change function
  CGOUT() << "  " << noprefix(mTopName)
          << CRYPT(".cmodel_modeChange(context, userContext, from, to);\n");

  // Terminate the function
  CGOUT() << "}\n";
}


/*!
 * Consistently generate the name suffix UtString for the schedule functions
 * that are used to generate derived clocks.  The base clock must be added
 * to the sched_<derived-names>_<base-clock>_derived_<edge> UtString that
 * is the name of the pos and neg edge functions.
 *
 * \arg name UtString to hold generated suffix
 * \arg clk the deriving clock
 * \arg suffix a constant UtString to append (usually _derived_<pos|neg>edge)
 */
static const char*
derivingClockName (UtString& name, const NUNetElab *clk, const char *suffix)
{
  name.clear();
  // In composing this name we can leave off the top level module name,
  // which will always be the same and makes the names real long.
  STBranchNode* mod = clk->getHier(), *parent;
  // note that the following gets the order wrong, it will generate:
  // bottom_middle_top_netname instead of top_middle_bottom_netname
  // while the order is wrong it is still unique so we use it for now
  while ((parent = mod->getParent()) != NULL)
  {
    name += "_";
    name += noprefix (mod);
    mod = parent;
  }
  name += "_";
  name += noprefix (clk);
  name += suffix;
  return name.c_str ();
}

/*!
 * Consistently generate a unique name for the combinational schedule
 * function by adding a suffix of the dependencies
 */
static UtString&
combinationalName (UtString& name, const SCHScheduleMask *m,
		   const char *subName)
{
  static const char* combSched = CRYPT("comb_schedule_");
  name = combSched;

  // Add the subname
  name << subName << "_";
  m->compose(&name, NULL);
  name << "_";

  // it looks better to replace "comp_schedule_posedge top.clk" with
  //                            "comp_schedule_posedge_top.clk"
  name.replace(name.begin(), name.end(), ' ', '_');

  // Do NOT use 'name << (UIntPtr)m << "_"', as that kills our ability
  // to make gold files.  The mask name may be quite long but it should
  // be truncated and uniquified before turning into a C++ name -- that
  // must be done anyway because a signal in the mask might not be a
  // legal C identifier

  return name;
}

/*!
  Generate C wrappers for the top-level save and restore methods.
 */
void CodeGen::codeSaveRestore (void)
{
  if (!mDoCheckpoint) {
    mSaveFnName << "carbon_" << *getIfaceTag () << CRYPT ("_save");
    mRestoreFnName << "carbon_" << *getIfaceTag () << CRYPT ("_restore");
    CGOUT () << "static int " << mSaveFnName 
             << " (UtOCheckpointStream *, CarbonObjectID *)" << ENDL
             << "{\n"
             << "  return false;\n"
             << "}\n" << ENDL;
    CGOUT () << "static int " << mRestoreFnName
             << " (UtICheckpointStream *, CarbonObjectID *)" << ENDL
             << "{\n"
             << "  return false;\n"
             << "}\n" << ENDL;
  } else {
    mSaveFnName << "carbon_" << *getIfaceTag () << CRYPT ("_save");
    mRestoreFnName << "carbon_" << *getIfaceTag () << CRYPT ("_restore");
    CGOUT () << "static int " << mSaveFnName 
             << " (UtOCheckpointStream *out UNUSED, CarbonObjectID *descr UNUSED)" << ENDL
             << "{\n"
             << "#ifdef CARBON_SAVE_RESTORE\n"
             << "  carbon_simulation *model = reinterpret_cast <carbon_simulation *> (descr->mHdl);\n"
             << "  model->save (*out, descr);\n"
             << "  return carbonInterfaceOCheckPointStreamFailed (out) ? 0 : 1;\n"
             << "#else\n"
             << "  return false;\n"
             << "#endif\n"
             << "}\n" << ENDL;
    CGOUT () << "static int " << mRestoreFnName
             << " (UtICheckpointStream *in UNUSED, CarbonObjectID *descr UNUSED)" << ENDL
             << "{\n"
             << "#ifdef CARBON_SAVE_RESTORE\n"
             << "  carbon_simulation *model = reinterpret_cast <carbon_simulation *> (descr->mHdl);\n"
             << "  model->restore (*in, descr);\n"
             << "  return carbonInterfaceICheckPointStreamFailed (in) ? 0 : 1;\n"
             << "#else\n"
             << "  return false;\n"
             << "#endif\n"
             << "}\n" << ENDL;
  }
  if (isOnDemand ()) {
    mOnDemandSaveFnName << "carbon_" << *getIfaceTag () << CRYPT ("_on_demand_save");
    mOnDemandRestoreFnName << "carbon_" << *getIfaceTag () << CRYPT ("_on_demand_restore");
    CGOUT () << "static int " << mOnDemandSaveFnName 
             << " (UtOCheckpointStream *out UNUSED, CarbonObjectID *descr UNUSED)" << ENDL
             << "{\n"
             << "#ifdef CARBON_SAVE_RESTORE\n"
             << "  carbon_simulation *model = reinterpret_cast <carbon_simulation *> (descr->mHdl);\n"
             << "  model->saveOnDemand (*out, descr);\n"
             << "  return carbonInterfaceOCheckPointStreamFailed (out) ? 0 : 1;\n"
             << "#else\n"
             << "  return false;\n"
             << "#endif\n"
             << "}\n" << ENDL;
    CGOUT () << "static int " << mOnDemandRestoreFnName
             << " (UtICheckpointStream *in UNUSED, CarbonObjectID *descr UNUSED)" << ENDL
             << "{\n"
             << "#ifdef CARBON_SAVE_RESTORE\n"
             << "  carbon_simulation *model = reinterpret_cast <carbon_simulation *> (descr->mHdl);\n"
             << "  model->restoreOnDemand (*in, descr);\n"
             << "  return carbonInterfaceICheckPointStreamFailed (in) ? 0 : 1;\n"
             << "#else\n"
             << "  return false;\n"
             << "#endif\n"
             << "}\n" << ENDL;
  }
}

/*!
  Generate the schedule functions that are called in order by the
  Carbon runtime schedule loop.

  There are multiple schedule routines, these are:

   0/ An input schedule which is scheduled on changes to PIs

   1/ One asynchronous schedule of expressions that don't require a
   clocked storage element.

   2/ Derived clock computations.

   3/ Two clock schedules (one rising, one falling) for each clock.

   4/ Up to 2**(2N) schedules of things involving the interaction of
   clocks, derived clocks, etc.

   Each computed schedule is provided to the code generator as a
   sequence of FLNodeElab *, each of which indicates an always-block
   or continuous assign that needs to be executed in order.

   Each schedule is called with a CarbonObjectID pointer.  That hdl pointer
   from the desrciptor pointer is static_cast<>-ed to the carbon_simulation
   class (a typedef to the topmost class in the simulation hierarchy) and
   assigned to a reference variable named after the top-level module name.
*/
void
CodeGen::codeSchedule (void)
{

  // We want all generated sample schedule buckets to be descendants
  // of the top-level carbon_design_schedule bucket.  Since the
  // schedules aren't generated from within the code that generates
  // carbon_design_schedule(), simply calling startSampleSchedBucket()
  // the normal way won't work.  We need to manually push the
  // top-level schedule, remember the value returned, and use that
  // later when generating carbon_design_schedule() (and
  // carbon_design_{clk,data}Schedule().)  So here we simply allocate
  // the bucket ID to become the parent of all other schedule buckets.
  UtString functionName("carbon_");
  functionName << *getIfaceTag() << CRYPT("_schedule");
  mTopLevelSchedID = getSchedBuckets()->push(functionName);

  CGOUT () << "namespace " << getNamespace () << "{"  << ENDL;

  // Construct Schedule objects for the input schedules.
  //
  SCHSchedule::CombinationalLoop l;
  UInt32 index = 0;
  for (l = mSched->loopCombinational(eCombInput); !l.atEnd(); ++l)
  {
    // Give the schedule a unique name
    //
    UtString inputSchedFunc ("input_schedule");
    inputSchedFunc << ++index;

    // Generate the schedule
    //
    SCHCombinational* input = *l;
    GenSched* inputGenerator = mGenSchedFactory->generate (inputSchedFunc.c_str (),
							   input,
							   getCGOFiles(),
							   eHandleArg);
    inputGenerator->genMethod ();
  }

  // Asynchronous schedule (this is private)
  //
  index = 0;
  for (l = mSched->loopCombinational(eCombAsync); !l.atEnd(); ++l)
  {
    // Give the schedule a unique name
    //
    UtString asyncSchedFunc ("asynchronous_schedule");
    asyncSchedFunc << ++index;

    // Generate the schedule
    //
    SCHCombinational* async = *l;
    GenSched* asyncGenerator
      = mGenSchedFactory->generate (asyncSchedFunc.c_str (),
				    async,
				    getCGOFiles(),
				    eHandleArg);

    asyncGenerator->genMethod ();
  }

  // Debug Schedule: this is actually called from the shell. 
  mDebugScheduleName << "carbon_" << mIfaceTag << "_debug_schedule";
  GenSched* debugGenerator
    = mGenSchedFactory->generate (mDebugScheduleName.c_str (),
                                  mSched->getDebugSchedule (),
				  getCGOFiles(),
                                  eHandleArg | eTopLevel | eCFunction,
                                  CRYPT("carbon_infrequent_code"),
                                  true); // suppress obfuscation

  debugGenerator->genMethod ();

  // ForceDeposit Schedule: this is actually called from the shell.
  //
  mDepositComboScheduleName << "carbon_" << mIfaceTag << "_deposit_combo_schedule";
  mDepositComboScheduleName << "carbon_" << mIfaceTag << "_deposit_combo_schedule";
  GenSched* forceDepositGenerator = mGenSchedFactory->generate (mDepositComboScheduleName.c_str (),
                                  mSched->getForceDepositSchedule (),
				  getCGOFiles(),
    eHandleArg|eTopLevel|eCFunction,
                                  CRYPT("carbon_infrequent_code"),
                                  true);

  forceDepositGenerator->genMethod ();

  // Index the clocks (need this in the event-loop)
  // Primary clocks need to be associated with the entry in the changed[]
  // array that corresponds to this clock.  Derived clocks need to be
  // tagged as derived.
  //
  // Populate the DCL clocks and then the primaries so we can count them
  for (SCHSchedule::DerivedClockLogicLoop l = mSched->loopDerivedClockLogic();
       !l.atEnd(); ++l) {
    SCHDerivedClockBase* dclBase = *l;
    for (SCHClocksLoop c = dclBase->loopClocks(); !c.atEnd(); ++c) {
      const NUNetElab* clkElab = *c;
      mClockTable[clkElab] = eDerivedClock;
    }
  }
  int numPrimaryClocks = 0;
  for (SCHClocksLoop c = mSched->loopClocks(); !c.atEnd(); ++c) {
    const NUNetElab* clkElab = *c;
    if (mClockTable.find(clkElab) == mClockTable.end()) {
      mClockTable[clkElab] = ePrimaryClock;
      mPrimaryClocks[clkElab] = numPrimaryClocks++;
    }
  }

  // 
  for (SCHSchedule::AsyncResetLoop r = mSched->loopAsyncResets();
       !r.atEnd(); ++r) {
    UInt32 *edge_counts = r.getValue();
    if( edge_counts[eClockPosedge] > 0 || edge_counts[eClockNegedge] > 0 )
      mAsyncResetTable[r.getKey()] = -1;
      
    if( edge_counts[eLevelHigh] > 0 || edge_counts[eLevelLow] > 0 )
      mSyncResetTable[r.getKey()] = -1;
  }


  // Generate derived clock schedules.  These are values that generate
  // derived clocks, so they must execute before the clock schedules!
  //
  for (SCHSchedule::DerivedClockLogicLoop dc = mSched->loopDerivedClockLogic();
       !dc.atEnd();
       ++dc)
    {
      // Mark the derived clocks in the table
      SCHDerivedClockBase* dclBase = *dc;
      SCHDerivedClockLogic* dcl = dclBase->castDCL();
      if (dcl != NULL) {
        codeDerivedSchedule(dcl);
      } else {
        SCHDerivedClockCycle* dclCycle = dclBase->castDCLCycle();
        SCHDerivedClockCycle::SubSchedulesLoop s;
        for (s = dclCycle->loopSubSchedules(); !s.atEnd(); ++s) {
          SCHDerivedClockLogic* dcl = *s;
          codeDerivedSchedule(dcl);
        }
      }
    }

  // Now do the various clock schedules..
  //
  for (SCHSequentials::SequentialLoop c = mSched->loopSequential(); !c.atEnd();
       ++c)
    // For Clock "c"
    {
      SCHSequential* sc = *c;  // Get the next clock.
      const NUNetElab* clk = sc->getEdgeNet();		// and its name
      
      generate_clock_schedule(clk, sc, eClockPosedge, "_posedge");

      generate_clock_schedule(clk, sc, eClockNegedge, "_negedge");
    }

  // Lastly do the combinatorial schedules
  //

  SCHSchedule::CombinationalLoop k;
  for (k = mSched->loopCombinational(eCombTransition); !k.atEnd(); ++k)
    {
      // Get the K-th schedule and the mask indicating which clocks participate
      SCHCombinational *comb = *k;

      // And emit a function declaration that describes that function.
      generate_combinational_schedule (comb, "transition");
    }
  for (k = mSched->loopCombinational(eCombSample); !k.atEnd(); ++k)
    {
      // Get the K-th schedule and the mask indicating which clocks participate
      SCHCombinational *comb = *k;

      // And emit a function declaration that describes that function.
      generate_combinational_schedule (comb, "sample");
    }

  CGOUT () << "}\n";
}

void CodeGen::codeDerivedSchedule(SCHDerivedClockLogic* dcl)
{
  // We should have seen this clock before and it should have been
  // marked as derived.
  for (SCHClocksLoop c = dcl->loopClocks(); !c.atEnd(); ++c) {
    const NUNetElab* derivedClk = *c;
    NU_ASSERT(mClockTable[derivedClk] == eDerivedClock, derivedClk);
  }

  // Pick a clock to represent the entire schedule
  const NUNetElab* derivedClk = dcl->getRepresentativeClock();

  // derived clocks can be generated from multiple PIs.
  // Each generated derived clock schedule must have a distinct name
  //
  // do the sequential schedule needed to compute this derived clock
  for (SCHSequentials::SequentialLoop c = dcl->loopSequential ();
       !c.atEnd (); ++c)
    {
      SCHSequential *sc = *c; // Next clock
      const NUNetElab *clk = sc->getEdgeNet ();
      UtString suffix;

      generate_clock_schedule (derivedClk, sc, eClockPosedge,
                               derivingClockName (suffix, clk,
                                                  "_derived_posedge"));

      generate_clock_schedule (derivedClk, sc, eClockNegedge,
                               derivingClockName (suffix, clk,
                                                  "_derived_negedge"));
    }

  // This is a combinational schedule used to compute the derived
  // clock's value.
  generate_clock_schedule (derivedClk, dcl->getCombinationalSchedule (),
                           "_derived");
} // void CodeGen::codeDerivedSchedule


// Header file containing "fixups" for things we don't know
// when we're generating code for the schedule files.

void CodeGen::codeScheduleHeader ()
{
  UtOStream* schHdr = CGFileOpen ("./schedule.h", 
                                  "/* scheduler constants. */\n",
				  isObfuscating());
  if (not schHdr)
    return;                     // already error'ed

  *schHdr << "#ifndef __CARBON_CG_SCHEDULE_H__" << ENDL;
  *schHdr << "#define __CARBON_CG_SCHEDULE_H__\n\n";
  *schHdr << "struct ScheduleConstants {\n";
  dumpConstantPool (*schHdr, eCGDeclare | eCGSchedule);
  flushConstantPool ();
  *schHdr << "};" << ENDL;

  // CHANGED_ARRAY_SIZE provides the size of the changed[] map back to the
  // code that allocates the oldclock[] array at the beginning of the
  // schedule main-loop.  We also assert if the C++ API tries to allocate
  // a different size.
  //
  *schHdr << "#define CHANGED_ARRAY_SIZE " << mChangedMap->size () << "\n";

  *schHdr << "\n#endif // __CARBON_CG_SCHEDULE_H__\n";
  delete schHdr;

}

/*!
 * Consistently generate the name used in synchronous schedules.  This
 * needs to include the clock and reset-masking signal names to uniquify
 * the function name.
 *
 * \arg name UtString containing some initial prefix ("sched_", usually).
 * \arg clock the clock or derived clock this schedule is for
 * \arg sc The schedule container - used to find the reset mask
 * \arg suffix a suffix string
 *
 * (any of \a clock, \a sc, or \a suffix may be NULL)
 *
 * \retval updated \a name string
 */
void
CodeGen::generate_schedule_name (UtString& name,
				 const NUNetElab* clock,
				 SCHSequential* sc,
				 const char* suffix)
{
  if (clock)
   {
     CxxHierPath (&name, clock->getHier (), "_");
     name << printable (clock);
   }

  // If there are reset masks, add them too
  if (sc)
  {
    const SCHScheduleMask *mask = sc->getAsyncMask ();
    if (mask)
      for (SCHScheduleMask::SortedEvents l = mask->loopEventsSorted ();
           !l.atEnd (); ++l)
	  {
	    const SCHEvent *event = *l;
        
	    if (event->isClockEvent ())
        {
          const NUNetElab *guard = SCHSchedule::clockFromName(event->getClock());
          
          ClockEdge ce = event->getClockEdge();
          name << "_" << ClockEdgeString( ce ) << "_";
		  
          CxxHierPath (&name, guard->getHier (), "_");
          name << printable (guard);
        }
	  }
    
    // If there are extra masks, add them too
    const SCHScheduleMask* extraMask = sc->getExtraEdgesMask ();
    if (extraMask)
    {
      name << "_extra";
      for (SCHScheduleMask::SortedEvents l = extraMask->loopEventsSorted ();
           !l.atEnd (); ++l)
	  {
	    const SCHEvent *event = *l;
        
	    if (event->isClockEvent ())
        {
          const NUNetElab *guard = SCHSchedule::clockFromName(event->getClock());
          name << "_" << ClockEdgeString( event->getClockEdge() ) << "_";
          CxxHierPath (&name, guard->getHier (), "_");
          name << printable (guard);
        }
	  }
    }
  }
  
  if (suffix)
    name += suffix;
}

/*!
 * Generate the schedule calls for a single clock.  This produces a
 * function definition named sched_<\a clk><\a edge> that contains calls to
 * the methods for the always-blocks and continuous assigns that need to
 * execute on the specific edge of that clock.
 *
 * \arg clk the hierarchical clock signal
 * \arg sc schedule object (possibly null)
 * \arg i iterator for collection of methods to call
 * \arg unique string to uniquify the schedule name.
 * the simulation object.
 */
void
CodeGen::generate_clock_schedule (const NUNetElab *clk,
				  SCHSequential *sc,
				  ClockEdge e,
				  const char* unique)
{
  UtString clockName("sched_");

  generate_schedule_name (clockName, clk, sc, unique);

  GenSched* clockGenerator = mGenSchedFactory->generate (clockName.c_str (),
							sc, getCGOFiles(), e, eHandleArg);

  clockGenerator->genMethod ();
}

// Just like above, but for computing derived clocks.
void
CodeGen::generate_clock_schedule (const NUNetElab *clk,
				  SCHCombinational* comb,
				  const char *unique)
{
  UtString clockName("sched_");

  generate_schedule_name (clockName, clk, 0, unique);

  GenSched* clockGenerator = mGenSchedFactory->generate (clockName.c_str (),
							comb, getCGOFiles(), eHandleArg);

  clockGenerator->genMethod ();
}

/*!
 * Emit the combinational schedule for a particular mask
 */
void
CodeGen::generate_combinational_schedule (SCHCombinational *comb,
					  const char * subName)
{
  const SCHScheduleMask *m = comb->getMask ();
  UtString maskSuffix ("_");

  UtString comment("\n// Combinational sensitive to ");
  m->compose (&comment, NULL);    // (we have no testcase in almostall that checks this line)

  UtString funcName;
  generate_schedule_name (combinationalName (funcName, m, subName));

  CGOUT () << comment << "\n";

  GenSched* combGenerator = mGenSchedFactory->generate (funcName.c_str (),
					  comb,
					  getCGOFiles(),
					  eHandleArg);
  combGenerator->genMethod ();
}


// Generate code to switch to a new schedule bucket for sample-based
// profiling.
// This method is similar to CGProfile::emitStartBucket(), which does
// block (rather than schedule) buckets.
void CodeGen::startSampleSchedBucket(const UtString& bucket_name,
                                     LangCppScope* scope,
                                     int bucket_id/*= -1*/)
{
  UtString buf;
  UtOStringStream bufStream(&buf);
  startSampleSchedBucket(bucket_name, bufStream, bucket_id);
  scope->addUserStatement(buf);
}

void CodeGen::startSampleSchedBucket(const UtString& bucket_name,
                                     UtOStream& stream,
                                     int bucket_id/*= -1*/)
{
  // If bucket_id wasn't specified, allocate/push one.
  if (bucket_id == -1)
    // Get a new ID and associate it with this bucket_name.
    bucket_id = getSchedBuckets()->push(bucket_name);
  // Generate code like:
  //   top__current_schedule_bucket = 8; /* Bucket name */
  // in an ifdef.
  stream << "#ifdef PROFILING" << ENDL
         << "  " << getScheduleBucketVarName() << " = "
         << bucket_id << ";\t/* " << bucket_name << " */\n"
         << "#endif\n";
}


// Return to the previous (higher-level) bucket.
void CodeGen::stopSampleSchedBucket(LangCppScope* scope, bool pop/* = true*/)
{
  UtString buf;
  UtOStringStream bufStream(&buf);
  stopSampleSchedBucket(bufStream, pop);
  scope->addUserStatement(buf);
}

void CodeGen::stopSampleSchedBucket(UtOStream& stream, bool pop/* = true*/)
{
  int id = -1;
  if (pop)
    id = getSchedBuckets()->pop();
  stream << "#ifdef PROFILING" << ENDL
         << "  " << getScheduleBucketVarName() << " = "
         << id << ";\n"
         << "#endif\n";
}

void CodeGen::getNetElabEmitString(UtString* buf, const NUNetElab* netElab)
{
  UtOStream* oldStream = mCGOFiles->getCurrentStream();
  UtOStringStream bufStream(buf);
  mCGOFiles->putCurrentStream(&bufStream);
  bufStream << netElab;
  mCGOFiles->putCurrentStream(oldStream);
}

void
CodeGen::genCombScheduleCalls(LangCppScope* cppScope,
                              CGTopLevelSchedule* topLevelSched,
                              SCHScheduleType schedType,
                              const char* schedName, const char* fnPrefix)
{
  UtString comment;
  comment << CRYPT("Run ") << schedName
          << CRYPT(" schedules if sensitive inputs changed");
  cppScope->addLineComment(comment.c_str());

  UInt32 index = 0;
  SCHSchedule::CombinationalLoop l = mSched->loopCombinational(schedType);

  // Only emit profiling when there is a non-empty list
  bool do_prof = not l.atEnd();

  if (do_prof) {
    // Start profiling for the schedule
    UtString bucketName;
    bucketName << schedName << CRYPT(" schedules");
    startSampleSchedBucket(bucketName, cppScope);
  }

  for (; !l.atEnd(); ++l)
  {
    // Get or create a schedule object
    SCHCombinational* comb = *l;
    UtString comb_sched;
    comb_sched << fnPrefix << "_schedule";
    GenSched* schedObj = mGenSchedFactory->findGenSched(comb_sched << ++index);

    if (! schedObj->emptySchedule()) {
      // Emit the test for the schedule. This can either use a schedule
      // mask if it is not the input mask or a test if any inputs have
      // changed. The former happens when the customer specifies timing
      // on inputs, the latter happens when they don't.
      LangCppScope* triggerScope = NULL;
      const SCHScheduleMask* mask = comb->getMask();
      if (mask && !mSched->isInputMask(mask)) {
        // The schedObj is used to determine if the conditional should
        // be emitted.
        triggerScope = topLevelSched->codeEdgeTriggerToScope(schedObj->getCost(), 
                                                             mask, cppScope);
      } else {
        const SCHInputNets* inputNets = comb->getAsyncInputNets();
        if (inputNets) {
          triggerScope = topLevelSched->codeInputNetTriggerToScope(inputNets, 
                                                                   cppScope);
        }
      }
      
      // Emit the call of the schedule
      if (triggerScope) 
        triggerScope->addUserStatement(schedObj->genCall());
    } // if ! emptySchedule
  }

  if (do_prof) {
    // Stop profiling for async
    stopSampleSchedBucket(cppScope);
  }
}

void
CodeGen::genDataScheduleCalls(LangCppScope* cppScope,
                              CGTopLevelSchedule* topLevelSched)
{
  // The asynchronous schedule must run before the input
  // schedule. This is because some of the blocks in the async
  // schedule could feed into blocks in the input schedule. The
  // reverse is not possible because otherwise the block would have
  // *output* in its sample mask and belong in the asynchronous
  // schedule.
  genCombScheduleCalls(cppScope,topLevelSched,eCombAsync,"Async","asynchronous");
  genCombScheduleCalls(cppScope,topLevelSched,eCombInput,"Input","input");
}

void CodeGen::codeClockGlitchDetection(LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched)
{
  // Set in the IODB whether the clock glitch generation code is
  // emitted.  That's needed so we can issue a warning if it's
  // requested at runtime.
  bool enableGlitch = getCarbonContext()->getArgs()->getBoolValue(CRYPT("-clockGlitchDetect"));
  mIODB->putIsClockGlitchGenerated(enableGlitch);
  // Only emit profiling for glitch detection when there is something to check
  // and the user didn't request that it be disabled.
  bool do_glitch = not mClockTable.empty() && enableGlitch;
  if (!do_glitch) {
    return;
  }

  // Emit code to test whether glitch detection is turned on or
  // not. If -g is on, then we always turn it on
  cppScope->addLineComment(CRYPT("Glitch detection on derived clocks"));
  
  LangCppScope* glitchScope = topLevelSched->codeGlitchTest(cppScope);
  
  // Start profiling for glitch detection
  startSampleSchedBucket(CRYPT("Glitch"), glitchScope);
  

  // Emit code for the derived clock glitch detection
  SCHSchedule::DerivedClockLogicLoop dc = mSched->loopDerivedClockLogic();
  for (; !dc.atEnd(); ++dc)
    {
      SCHDerivedClockBase* dclBase = *dc;
      for (SCHClocksLoop c = dclBase->loopClocks(); !c.atEnd(); ++c) {
        const NUNetElab* derivedClk = *c;
        ClockIter derivedClkMap = mClockTable.find (derivedClk);
        NU_ASSERT(derivedClkMap != mClockTable.end(), derivedClk);

        UtString clkName;
        derivedClk->compose (&clkName, NULL, true);    // include root in name
        UtString clkName2;
	StringUtil::escapeCString(clkName.c_str(), &clkName2);

        // Register this name with the IODB and use its assigned index
        // to do the glitch detection.  This prevents the name (which
        // may not be exposed in the IODB) from being included in
        // plaintext in the model.
        UInt32 clkIndex = mIODB->addClockGlitchName(clkName2);
        
        topLevelSched->codeDerivedClockGlitchDetect(clkIndex, derivedClk, glitchScope);
      }
    }

  // Emit code to test if any input clocks changed when time did not
  // advance.
  if (! mPrimaryClocks.empty())
  {
    LangCppVariable* curChanges = topLevelSched->createDynBVVar("curChanges", mPrimaryClocks.size(), glitchScope);
    
    for (ClockIter i = mPrimaryClocks.begin(); i != mPrimaryClocks.end(); ++i) {
      const NUNetElab* clkElab = i->first;
      int index = i->second;
      
      topLevelSched->codeSetDynBVIfDciChanged(clkElab, curChanges, index, glitchScope);
    }
    
    topLevelSched->codeCheckPrimaryClkRace(glitchScope, curChanges);
  }
  
  // End profiling for glitch detection
  stopSampleSchedBucket(glitchScope);
}

void CodeGen::codeClearNets(LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched)
{
  bool doProf = false;
  SCHSchedule::ClearAtEndNetsLoop c = mSched->loopClearAtEndNets();
  if (! c.atEnd())
  {
    startSampleSchedBucket(CRYPT("Clear Nets"), cppScope);
    doProf = true;
  }

  for (; !c.atEnd(); ++c) {
    // Only generate an if it doesn't have the input mask
    const SCHScheduleMask* mask = c.getKey();
    NUNetElabVector* netElabs = c.getValue();
    LangCppScope* curScope = cppScope;
    if (!mask->hasInput()) {
      curScope = topLevelSched->codeEdgeTriggerToScope(netElabs->size(), mask, cppScope);
    }
    else
      curScope = cppScope->addScope();

    if (curScope) {
      // Generate the clear statements
      for (NUNetElabVectorLoop n(*netElabs); !n.atEnd(); ++n) {
        NUNetElab* netElab = *n;
        UtString netElabBuf;
        getNetElabEmitString(&netElabBuf, netElab);
        netElabBuf << " = 0;";
        curScope->addUserStatement(netElabBuf);
      }
    }
  }
  if (doProf) {
    stopSampleSchedBucket(cppScope);
  }
}

void
CodeGen::codeResetGuardExtraCondSeqCall(const char* fn,
                                        int foundClockIndex,
                                        const ResetGuards& rg,
                                        const NUNetElab* clk,
                                        const SCHScheduleMask* extraEdges,
                                        const char* varPrefix, 
                                        LangCppScope* cppScope,
                                        CGTopLevelSchedule* topLevelSched)
{
  UtString fnPos (fn);	
  fnPos += "_";
  fnPos += ClockEdgeString( eClockPosedge );
  UtString fnNeg (fn);
  fnNeg += "_";
  fnNeg += ClockEdgeString( eClockNegedge );
  

  if (rg.mask != NULL) {
    UtString maskStr;
    rg.mask->compose(&maskStr, NULL);
    UtString commentBuf;
    commentBuf << CRYPT(" Reset guards: ") << maskStr;
    cppScope->addLineComment(commentBuf.c_str());
  }
  if (extraEdges != NULL) {
    UtString maskStr;
    extraEdges->compose(&maskStr, NULL);
    UtString commentBuf;
    commentBuf << CRYPT(" Extra edges: ") << maskStr;
    cppScope->addLineComment(commentBuf.c_str());
  }

  // Emit any extra conditions or reset guards
  LangCppScope* resetGuardScope = topLevelSched->codeResetGuardToScope(rg, cppScope);
  if (! resetGuardScope)
    resetGuardScope = cppScope->addScope();
  
  
  // Emit the posedge function
  {
    GenSched* genSched = mGenSchedFactory->findGenSched (fnPos);
    if (! genSched->emptySchedule())
    { 
      LangCppScope* posedgeIf = 
        topLevelSched->codeEdgeNetAndExtraEdgesToScope(varPrefix, clk, foundClockIndex, extraEdges, true, resetGuardScope);
      posedgeIf->addUserStatement(genSched->genCall ());
    }
  }
  
  // Emit the negedge function
  {
    GenSched* genSched = mGenSchedFactory->findGenSched (fnNeg);
    if (! genSched->emptySchedule())
    { 
      LangCppScope* negedgeIf = 
        topLevelSched->codeEdgeNetAndExtraEdgesToScope(varPrefix, clk, foundClockIndex, extraEdges, false, resetGuardScope);
      negedgeIf->addUserStatement(genSched->genCall ());
    }
  }
}

/*!
 * Evaluate which schedule for computing a derived clock needs to be run.
 * \arg primaryClock - clock signal (which edge are we on)
 * \arg fn - prefix string for schedule-function to call
 * \arg foundClockIndex - index in changed[] for primaryClock
 * \arg rg - reset guard printing object to qualify the schedule.
 *
 * \returns: void
 */
void
CodeGen::evalDerivedClock(const char* fn,
                          int foundClockIndex,
                          const ResetGuards& rg,
                          const NUNetElab* clk,
                          const SCHScheduleMask* extraEdges,
                          const char* varPrefix, 
                          LangCppScope* cppScope,
                          CGTopLevelSchedule* topLevelSched)
{
  // Emit a comment with the schedule mask(s)
  {
    UtString clkBuf;
    UtString commentBuf;
    getNetElabEmitString(&clkBuf, clk);
    commentBuf << "Clock: " << clkBuf;
    cppScope->addLineComment(commentBuf.c_str());
  }

  codeResetGuardExtraCondSeqCall(fn, foundClockIndex, rg, clk, extraEdges, varPrefix,
                                 cppScope, topLevelSched);

} // CodeGen::evalDerivedClock

/*!
 * We need to sync() state update variables if any clocks toggled, causing
 * sequential schedules to run.
 *
 * Generate: if (0 || changed[k] ...) top.sync();
 *
 * \arg sensed - set of sensitive entries in changedMap
 */
void
CodeGen::SyncOnClock (UInt32Set& sensed, SCHSequentials::SULoop l, 
                      LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched)
{
  // If there are NO nets requiring synch, then quit immediately
  if (l.atEnd ())
    return;
  
  // Count the size of the synch list
  SCHSequentials::SULoop c = l;
  UInt32 updates = 0;
  while (not c.atEnd ()) {
    ++updates;
    ++c;
  }

  static const char* stateVarComment = CRYPT("Update buffered state variables");
  
  LangCppScope* syncTestScope = NULL;
  if ((2*updates) > sensed.size ())
    // On average, let's assume a probability of .5 that one of our
    // signals changes.  So, if the number of sync's we're avoiding
    // (updates) is at least twice as large as the number of tests for
    // change, then it's better to test and avoid synching every time
  {
      cppScope->addLineComment(stateVarComment);
      syncTestScope = topLevelSched->codeChangedRefsToScope(sensed, cppScope);
  }

  if (syncTestScope == NULL)
    syncTestScope = cppScope->addScope();

  startSampleSchedBucket(CRYPT("Sync"), syncTestScope);

  // Call each StateUpdate variable's sync() function.  (The net state
  // updates are now gone. All that is left are memory state updates).
  {
    UtOStream* oldStream = mCGOFiles->getCurrentStream();
    UtString buf;
    UtOStringStream bufStream(&buf);
    mCGOFiles->putCurrentStream(&bufStream);

    // nasty. No control what goes into the scope at this point.
    emitCodeList (&l, eCGHierName | eCGSchedule, "", ";\n");
    
    mCGOFiles->putCurrentStream(oldStream);

    // All emits go into the scope as 1 statement. Yuck.
    syncTestScope->addUserStatement(buf);
  }
  
  stopSampleSchedBucket(syncTestScope);
}

void CodeGen::codeDerivedClockCall(SCHDerivedClockLogic* dcl,
                                   UInt32Set& SyncSensitives,
                                   const char* varPrefix,
                                   bool combTest,
                                   LangCppScope* cppScope,
                                   CGTopLevelSchedule* topLevelSched)
{
  // Pick a clock to represent the entire schedule
  const NUNetElab* derivedClk = dcl->getRepresentativeClock(); 
  UtString derivedName ("sched_");
  generate_schedule_name (derivedName, derivedClk, 0, "_derived");

  // Do the sequential schedule.
  bool hasSequentials = false;
  for (SCHSequentials::SequentialLoop c = dcl->loopSequential ();
       !c.atEnd (); ++c)
  {
    SCHSequential* sc = *c;
    const NUNetElab *clk = sc->getEdgeNet();
    ClockIter foundClock = mClockTable.find(clk);
    NU_ASSERT(foundClock != mClockTable.end(), clk);

    UtString clockName ("sched_");
    UtString suffix;

    generate_schedule_name (clockName, derivedClk, sc, 
			    derivingClockName (suffix, clk, "_derived"));
    UInt32 idx = mChangedMap->getIndex(foundClock->first);

    SyncSensitives.insert (idx);
    const SCHScheduleMask* extraEdges = sc->getExtraEdgesMask();
    evalDerivedClock(clockName.c_str(), idx,
		     ResetGuards (clk, mSched, sc->getAsyncMask(), mClockTable),
                     clk, extraEdges,
                     varPrefix, cppScope, topLevelSched);
    hasSequentials = true;
  }

  // Update all StateUpdate<T> instances
  if (hasSequentials)
  {
    SyncOnClock (SyncSensitives, dcl->loopSUNets (), cppScope, topLevelSched);
    SyncSensitives.clear ();
  }

  // Generate the logic for the combinational portion
  GenSched *derivedSched = mGenSchedFactory->findGenSched (derivedName);
  if (combTest) {
    LangCppScope* trigScope = topLevelSched->codeDclTriggerToScope(dcl, derivedSched, varPrefix, cppScope);
    if (trigScope)
      trigScope->addUserStatement(derivedSched->genCall ());
    else
      cppScope->addLineComment(derivedSched->genCall().c_str());
  }
  else
    cppScope->addUserStatement(derivedSched->genCall ());
} // void CodeGen::codeDerivedClockCall

int
CodeGen::codeDerivedChanged(const NUNetElab* derivedClk,
                            const char* writeVarPrefix,
                            const char* readVarPrefix,
                            bool orChange,
                            LangCppScope* cppScope,
                            CGTopLevelSchedule* topLevelSched)
{
  // Check if we should OR into the change flag. The caller may
  // indicate that we should (DCL cycles) or we do it for bidi clocks,
  // depositable clocks and async clocks.
  //
  // This is for any clock where the change flag can be set from
  // multiple places.
  NUNet* clkNet = derivedClk->getNet();
  orChange |= (clkNet->isPrimaryBid() || mSched->isWritable(derivedClk) ||
               mSched->isAsyncClock(derivedClk));

  // Emit the change flag computation
  ClockIter derivedClkMap = mClockTable.find (derivedClk);
  NU_ASSERT(derivedClkMap != mClockTable.end(), derivedClk);
  int derivedClkIndex = mChangedMap->getIndex(derivedClkMap->first);
  UtString netElabBuf;
  getNetElabEmitString(&netElabBuf, derivedClk);
  topLevelSched->codeDerivedChangedAssign(writeVarPrefix, readVarPrefix,
                                          derivedClkIndex, orChange,
                                          netElabBuf.c_str(), cppScope);
  
  return derivedClkIndex;
}

void CodeGen::codeDerivedClockCycleCall(SCHDerivedClockCycle* dclCycle,
                                        UInt32Set& SyncSensitives,
                                        LangCppScope* cppScope,
                                        CGTopLevelSchedule* topLevelSched)
{
  // Copy the changed array to the local copy for DCL cycles
  topLevelSched->copyChangeToDclChange(cppScope);

  // Set the changed flags that are set by this DCL schedule
  cppScope->addLineComment(CRYPT("Set flags that are computed in this sched"));

  for (SCHClocksLoop c = dclCycle->loopClocks(); !c.atEnd(); ++c) {
    // Find the index into the changed array for this clock
    const NUNetElab* derivedClk = *c;
    codeDerivedChanged(derivedClk, "dcl_", "", false, cppScope, topLevelSched);

    // Initialize the main changed flag to any changes that occured
    // before this point (in combodeposit or input/async
    // schedules). It will get modified inside the loop as well.
    int index = getChangedIndex(derivedClk);
    topLevelSched->copyDclChangeIndexToChangeIndex(cppScope, index);

    // We are writing the real changed array, remember it
    writeChanged(index);
  }

  // Emit while loop header
  LangCppScope* forLoop = topLevelSched->initDclCycleLoop(cppScope);
  
  // Initialize the old clocks for the clocks generated by this
  // schedule.
  for (SCHClocksLoop c = dclCycle->loopClocks(); !c.atEnd(); ++c) {
    // Find the index into the changed array for this clock
    const NUNetElab* derivedClk = *c;
    ClockIter derivedClkMap = mClockTable.find (derivedClk);
    NU_ASSERT(derivedClkMap != mClockTable.end(), derivedClk);
    //    int dci = mChangedMap.getIndex(derivedClk);

    // Emit code to initialize this old clock
    topLevelSched->codeSaveDclValue(derivedClk, forLoop, "dcl_");
  }

  // Initialize the old break nets so we can detect changes
  int netIndex = 0;
  for (SCHDerivedClockCycle::BreakNetsLoop b = dclCycle->loopBreakNets();
       !b.atEnd(); ++b) {
    const NUNetElab* netElab = *b;
    NUNet* net = netElab->getNet();
    UtString netType;
    UtOStringStream netTypeStream(&netType);
    if (net->is2DAnything()) {
      netTypeStream << CType(net);
    } else {
      netTypeStream << CBaseTypeFmt(net, eCGDeclare);
    }
    ++netIndex;
    topLevelSched->saveNetValue(netType.c_str(), netIndex, netElab, forLoop);
  }

  // Emit code for each DCL sub schedule in this cycle

  SCHDerivedClockCycle::SubSchedulesLoop s;
  for (s = dclCycle->loopSortedSubSchedules(); !s.atEnd(); ++s) {
    SCHDerivedClockLogic* dcl = *s;
    codeDerivedClockCall(dcl, SyncSensitives, "dcl_", false, forLoop, topLevelSched);

    // Compute the change flag for all clocks generated by this schedule.
    //
    // Note:
    //
    //       Break clocks are computed here and below. In the past we
    //       only executed them below. Break clocks are the clocks
    //       that are computed after they are used in the cycle (used
    //       to break the cycle). But in some cases the clock is used
    //       before and after it is computed. To get the right answer
    //       when there is gated logic, it turns out we need to
    //       compute the changed value twice. Once here to make the
    //       later schedules behave consistently and once at the end
    //       of the while loop to determine if we should break out of
    //       the while loop or not.
    //
    //       The test case that failed was Ceva/xs1200 with the local
    //       analysis latch conversion turned on and the gated flop
    //       optimization turned off. This caused a DCL cycle that
    //       glitched because the convert latch to a flop ran after
    //       the AND gate it fed (the combo schedule was unguarded)
    // 
    for (SCHClocksLoop c = dcl->loopClocks(); !c.atEnd(); ++c) {
      // If this is not the first iteration and it is a break net,
      // clear the dcl_changed flag. This is so that any changes that
      // wrap don't go past this point again. The right way to fix
      // this bug is to go to a real event wheel.
      //
      // See bug12309
      const NUNetElab* clk = *c;
      if (dclCycle->isBreakClock(clk)) {
        maybeClearDCLChanged(clk, forLoop, topLevelSched);
      }

      // Compute the changed flag
      codeDCLCycleChanged(clk, false, forLoop, topLevelSched);
    }
  }

  // Clear all the change flag copy
  topLevelSched->clearDclChanged(forLoop);


  // Detect clock changes for any clocks for DCL schedules that were
  // used to break the cycle. These have to survive to the next
  // iteration. So they cause us to clear the settled flag
  topLevelSched->codeSetDclSettledFlags(forLoop);
  for (SCHClocksLoop c = dclCycle->loopClocks(); !c.atEnd(); ++c) {
    const NUNetElab* clk = *c;
    if (dclCycle->isBreakClock(clk)) {
      codeDCLCycleChanged(clk, true, forLoop, topLevelSched);
    }
  }

  // Test if any break nets changed
  netIndex = 0;
  for (SCHDerivedClockCycle::BreakNetsLoop b = dclCycle->loopBreakNets();
       !b.atEnd(); ++b) {
    const NUNetElab* netElab = *b;
    ++netIndex;
    topLevelSched->updateSettledWithOldNetChk(netIndex, netElab, forLoop);
  }
} // void CodeGen::codeDerivedClockCycleCall

void CodeGen::codeDerivedScheduleCall(SCHDerivedClockBase* dclBase,
                                      const SCHInputNets* inputNets,
                                      UInt32Set& SyncSensitives,
                                      LangCppScope* cppScope,
                                      CGTopLevelSchedule* topLevelSched)
{
  // Emit a comment
  {
    UtString commentBuf;
    commentBuf <<  "Run the schedule for:";
    cppScope->addLineComment(commentBuf.c_str());
    for (SCHClocksLoop c = dclBase->loopClocks(); !c.atEnd(); ++c) {
      const NUNetElab* derivedClk = *c;
      UtString derivedClkBuf;
      getNetElabEmitString(&derivedClkBuf, derivedClk);

      commentBuf.clear();
      commentBuf << "  " << derivedClkBuf;
      cppScope->addLineComment(commentBuf.c_str());
    }
  }

  // Emit a comment describing the schedule input nets
  if (inputNets != NULL)
  {
    for (SCHInputNetsCLoop i(*inputNets); !i.atEnd(); ++i)
    {
      const NUNetElab* input = *i;
      UtString commentBuf;
      UtString netElabBuf;
      getNetElabEmitString(&netElabBuf, input);
      commentBuf << "Input net: " << netElabBuf << "\n";
      cppScope->addLineComment(commentBuf.c_str());
    }
  }

  // Generate the schedule depending on the type
  SCHDerivedClockLogic* dcl = dclBase->castDCL();
  if (dcl != NULL) {
    codeDerivedClockCall(dcl, SyncSensitives, "", true, cppScope, topLevelSched);

    // Generate the changed flag for this derived clocks.
    for (SCHClocksLoop c = dclBase->loopClocks(); !c.atEnd(); ++c) {
      const NUNetElab* derivedClk = *c;
      int derivedClkIndex = codeDerivedChanged(derivedClk, "", "", false, cppScope, topLevelSched);

      // We are writing the real changed array, remember it
      writeChanged(derivedClkIndex);
    }
  } else {
    SCHDerivedClockCycle* dclCycle = dclBase->castDCLCycle();
    const NUNetElab* derivedClk = dclBase->getRepresentativeClock(); 
    NU_ASSERT(dclCycle != NULL, derivedClk);
    codeDerivedClockCycleCall(dclCycle, SyncSensitives, cppScope, topLevelSched);
  }
} // void CodeGen::codeDerivedScheduleCall

void
CodeGen::codeOldClockInit(LangCppScope* cppScope, 
                          CGTopLevelSchedule* topLevelSched,
                          SCHClocksLoop c)
{
  for (; !c.atEnd(); ++c) {
    const NUNetElab* derivedClk = *c;
    ClockIter derivedClkMap = mClockTable.find (derivedClk);
    NU_ASSERT(derivedClkMap != mClockTable.end(), derivedClk);
    topLevelSched->codeSaveDclValue(derivedClk, cppScope, "");
  }
}

void CodeGen::codeDerivedClockEdgeDetection(LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched)
{
  // Save the value of the derived clocks. We should do this before
  // the input schedule because it can update derived clocks.
  cppScope->addLineComment(CRYPT("Edge Detection for derived clocks"));
  for (SCHSchedule::DerivedClockLogicLoop dc = mSched->loopDerivedClockLogic();
       !dc.atEnd();
       ++dc)
    {
      SCHDerivedClockBase* dclBase = *dc;
      codeOldClockInit(cppScope, topLevelSched, dclBase->loopClocks());
    }
}

void CodeGen::codeDerivedClockSchedules(LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched)
{
  // Call the derived clock schedule if the dependent clock changed
  // (set the changed[] flag for the derived clock to reflect the changes
  // in the derived clock that result from running the sequential and
  // combinational schedules that compute the clock.
  //
  cppScope->addLineComment(CRYPT("Run the derived clock logic schedules"));

  // Track the PIs that derived clocks are sensitive to.
  UInt32Set SyncSensitives;

  // Only emit derived profiling when there is a non-empty list

  // Create an array of derived clock schedules which has the same
  // input sensitivity
  DCLGroups dclGroups;
  bool do_derived = groupSchedulesByInputs<CodeGen::DCLGroups,SCHSchedule::DerivedClockLogicLoop>
    (dclGroups, 
     mSched->loopDerivedClockLogic()
     );

  if (do_derived) {
    // Start profiling for derived
    startSampleSchedBucket(CRYPT("Derived schedules"), cppScope);
  }
      
  int branchSize;
  if (getCarbonContext()->getArgs()->getBoolValue(CRYPT("-noSchedBranch")))
    branchSize = 0;
  else
    branchSize = 2;

  // Go through the DCL groups
  for (DCLGroups::iterator groupIter = dclGroups.begin(); 
       groupIter != dclGroups.end(); 
       ++groupIter) {
    // Check if we should emit a surrounding if statement. We do
    // so if the size is big enough
    DCLGroup & dclGroup = (*groupIter);
    const SCHInputNets * inputNets = dclGroup.first;

    // Compute size based on the number of clocks computed
    DCLScheds & dclScheds = dclGroup.second;
    int size = 0;
    for (DCLScheds::iterator iter = dclScheds.begin(); 
	 iter != dclScheds.end(); 
	 ++iter) {
      SCHDerivedClockBase* dclBase = (*iter);
      size += dclBase->numClocks();
    }

    bool check_inputs_for_group = ((inputNets != NULL) and 
				   (size >= branchSize) and
				   (branchSize != 0));

    LangCppScope* currentScope = cppScope;
    LangCppIf* ifInputNetMain = NULL;
    if (check_inputs_for_group) {
      UtString commentBuf;
      commentBuf << "Only execute these " << size << " schedules if the inputs have changed";
      cppScope->addLineComment(commentBuf.c_str());

      currentScope = topLevelSched->codeInputNetTriggerToScope(inputNets, cppScope, &ifInputNetMain);

    }

    // Emit the schedules
    for (DCLScheds::iterator iter = dclScheds.begin(); 
	 iter != dclScheds.end(); 
	 ++iter) {
      SCHDerivedClockBase* dclBase = (*iter);
      codeDerivedScheduleCall(dclBase, inputNets, SyncSensitives, currentScope, topLevelSched);
    }

    // Emit the ending of the if statement and the else if the
    // size is big enough
    if (check_inputs_for_group && currentScope) {
      INFO_ASSERT(ifInputNetMain, "No if-scope in dcl clock detection");
      LangCppElse* elseInputNet = ifInputNetMain->addElse();
      elseInputNet->addLineComment(CRYPT("Clear the changed flags"));

      for (DCLScheds::iterator i = dclScheds.begin(); i != dclScheds.end(); ++i) {
        SCHDerivedClockBase* dclBase = (*i);
        for (SCHClocksLoop c = dclBase->loopClocks(); !c.atEnd(); ++c) {
          const NUNetElab* derivedClk = *c;
          ClockIter derivedClkMap = mClockTable.find (derivedClk);
          NU_ASSERT(derivedClkMap != mClockTable.end(), derivedClk);
          int derivedClkIndex = mChangedMap->getIndex(derivedClkMap->first);
          topLevelSched->clearChangedFlag(derivedClkIndex, elseInputNet, false);
        }
      }
    }
  }

  if (do_derived) {
    // Stop profiling for derived
    stopSampleSchedBucket(cppScope);
  }
}

void
CodeGen::genCombinationalCalls (SCHScheduleType type, const char * subName,
                                LangCppScope* cppScope, 
                                CGTopLevelSchedule* topLevelSched)
{

  CombinationalGroups combinationalGroups;

  // Only emit combinational profiling when there is a non-empty list
  bool do_comb = groupSchedulesByInputs<CombinationalGroups,SCHSchedule::CombinationalLoop>
    (combinationalGroups,
     mSched->loopCombinational(type)
     );

  if (do_comb) {
    // Start profiling for schedule
    UtString this_bucket_name = "Comb ";
    this_bucket_name.append(subName);
    this_bucket_name.append(" schedule");

    startSampleSchedBucket(this_bucket_name, cppScope);
  }

  int branchSize;
  if (getCarbonContext()->getArgs()->getBoolValue(CRYPT("-noSchedBranch")))
    branchSize = 0;
  else
    branchSize = 2;

  // Go through Combinational groups.
  for (CombinationalGroups::iterator groupIter = combinationalGroups.begin();
       groupIter != combinationalGroups.end();
       ++groupIter) {
    // Check if we should emit a surrounding if statement. We do
    // so if the size is big enough
    CombinationalGroup & combinationalGroup = (*groupIter);
    const SCHInputNets * inputNets = combinationalGroup.first;
    CombinationalSchedules & combinationalSchedules = combinationalGroup.second;
    int size = combinationalSchedules.size();

    bool check_inputs_for_group = ((inputNets != NULL) and 
				   (size >= branchSize) and
				   (branchSize != 0));

    LangCppScope* currentScope = cppScope;
    if (check_inputs_for_group) {
      UtString comment;
      comment << "Only execute these " << size
              << " schedules if the inputs have changed";
      cppScope->addLineComment(comment.c_str());

      currentScope = topLevelSched->codeInputNetTriggerToScope(inputNets, cppScope);
    }

    // Emit the schedules
    for (CombinationalSchedules::iterator iter = combinationalSchedules.begin();
	 iter != combinationalSchedules.end();
	 ++iter) {
      SCHCombinational* comb = (*iter);
      const SCHScheduleMask *mask = comb->getMask();

      LangCppScope* activeScope = currentScope;
      bool codeCalls = true;
      if (! currentScope)
      {
        activeScope = cppScope;
          
        if (check_inputs_for_group)
          codeCalls = false;
      }
        
      // Emit a comment for this combinational
      UtString maskStr;
      mask->compose(&maskStr, NULL);
      UtString comment;
      comment << "Call the combinational schedule for " << maskStr;
      activeScope->addLineComment(comment.c_str());

      UtString funcName;
      generate_schedule_name (combinationalName (funcName, mask, subName));

      GenSched *schedObj = mGenSchedFactory->findGenSched (funcName);

      if (codeCalls)
      {
        LangCppScope* callIf = topLevelSched->codeEdgeTriggerToScope(schedObj->getCost(), mask, activeScope);
        LangCppScope* callScope = activeScope;
        if (callIf)
          callScope = callIf;
        
        callScope->addUserStatement(schedObj->genCall());
      }
      else
        activeScope->addLineComment(schedObj->genCall().c_str());
    }
  }
  if (do_comb) {
    // Stop profiling for schedule
    stopSampleSchedBucket(cppScope);
  }

}

void CodeGen::genSequentialCalls(UInt32Set & SyncSensitives, 
                                 LangCppScope* cppScope,
                                 CGTopLevelSchedule* topLevelSched)
{
  SequentialGroups sequentialGroups;

  // Only emit sequential profiling when there is a non-empty list
  bool do_sequential = groupSchedulesByInputs<SequentialGroups,SCHSequentials::SequentialLoop>
    (sequentialGroups,
     mSched->loopSequential()
     );

  if (do_sequential) {
    // Start profiling for sequential
    startSampleSchedBucket(CRYPT("Sequential schedules"), cppScope);
  }

  int branchSize;
  if (getCarbonContext()->getArgs()->getBoolValue(CRYPT("-noSchedBranch")))
    branchSize = 0;
  else
    branchSize = 2;

  // Go through Sequential groups.
  for (SequentialGroups::iterator groupIter = sequentialGroups.begin();
       groupIter != sequentialGroups.end();
       ++groupIter) {
    // Check if we should emit a surrounding if statement. We do
    // so if the size is big enough
    SequentialGroup & sequentialGroup = (*groupIter);
    const SCHInputNets * inputNets = sequentialGroup.first;
    SequentialSchedules & sequentialSchedules = sequentialGroup.second;
    int size = sequentialSchedules.size();

    bool check_inputs_for_group = ((inputNets != NULL) and 
				   (size >= branchSize) and
				   (branchSize != 0));

    LangCppScope* currentScope = cppScope;
    if (check_inputs_for_group) {
      UtString comment;
      comment << "Only execute these " << size
              << " schedules if the inputs have changed";
      cppScope->addLineComment(comment.c_str());

      currentScope = topLevelSched->codeInputNetTriggerToScope(inputNets, cppScope);
    }

    // Emit the schedules calls
    for (SequentialSchedules::iterator iter = sequentialSchedules.begin();
	 iter != sequentialSchedules.end();
	 ++iter) {
      SCHSequential * sc = (*iter);
      const NUNetElab* clk = sc->getEdgeNet();

      // Get the resets
      const SCHScheduleMask* asyncMask = sc->getAsyncMask ();
      const SCHScheduleMask* extraEdges = sc->getExtraEdgesMask ();

      // If there are resets (non-NULL mask), then we have to
      // generate predicates to call the reset schedules and
      // guard the clock-edge calls with the resets being off...

      // Find this clock's index in the changed[] array.
      //
      ClockIter i = find_if (mClockTable.begin(), mClockTable.end(),
			     NetMapCompare (clk));

      NU_ASSERT (i != mClockTable.end (), clk);

      UtString funcName ("sched_");
      generate_schedule_name (funcName, clk, sc);

      // if (changed[<i>] && !.....)
      //   ((<clock>) ? sched_<clock>_posedge:sched_<clock>_negedge)();
      //
      UInt32 idx = mChangedMap->getIndex(i->first);
      SyncSensitives.insert (idx);

      LangCppScope* activeScope = currentScope;
      bool codeCalls = true;
      if (! currentScope)
      {
        activeScope = cppScope;
        
        if (check_inputs_for_group)
          codeCalls = false;
      }

      if (codeCalls)
      {
        ResetGuards rg(clk, mSched, asyncMask, mClockTable);
        codeResetGuardExtraCondSeqCall(funcName.c_str(),
                                       idx,
                                       rg,
                                       clk,
                                       extraEdges,
                                       "", 
                                       activeScope,
                                       topLevelSched);
      }
    }
  }
  
  if (do_sequential) {
    // Stop profiling for sequential
    stopSampleSchedBucket(cppScope);
  }
}

void
CodeGen::codeDCLCycleChanged(const NUNetElab* clk,
                             bool clearSettled, LangCppScope* cppScope,
                             CGTopLevelSchedule* topLevelSched)
{
  // Find the index into the changed array for this clock
  const NUNetElab* derivedClk = clk;
  ClockIter derivedClkMap = mClockTable.find (derivedClk);
  NU_ASSERT(derivedClkMap != mClockTable.end(), derivedClk);
  int dci = mChangedMap->getIndex(derivedClk);

  // Compute whether the clock changed in this iteration. If so, mark
  // that we have not settled. Also update the changed flag with this
  // information. We want to record all edges so that down stream
  // logic sees the glitches.
  codeDerivedChanged(derivedClk, "dcl_", "dcl_", true, cppScope, topLevelSched);
  topLevelSched->updateChangedWithDclChanged(cppScope, dci);

  if (clearSettled) {
    topLevelSched->clearSettledIfDclChanged(cppScope, dci);
  }
}

// Emit code that does
//
//  if (iterations > 0) {
//    dcl_changed[clkid] = 0;
//  }
//
// This will allow any clock changes outside the cycle to pass
// through because dcl_changed is assigned with |=. But it won't let
// a change survive more than one iteration of the loop.
//
// See bug12309
void
CodeGen::maybeClearDCLChanged(const NUNetElab* clk,
                              LangCppScope* cppScope,
                              CGTopLevelSchedule* topLevelSched)
{
  // Create the if statement scope
  LangCppExpr* cond = cppScope->createUserExpr("iterations > 0");
  LangCppIf* ifStmt = cppScope->addIf(cond);
  LangCppScope* thenScope = ifStmt->getClauseScope();

  // Find the clock index
  ClockIter clkMap = mClockTable.find (clk);
  NU_ASSERT(clkMap != mClockTable.end(), clk);
  int clkIndex = mChangedMap->getIndex(clkMap->first);

  // Clear the dcl changed flag in the if scope
  topLevelSched->clearChangedFlag(clkIndex, thenScope, true);
}

void CodeGen::codeClockFunction(LangCppScope* cppScope, CGTopLevelSchedule* topLevelSched)
{
  // anything that a sched_XXX_derived schedule is sensitive to needs to
  // be in the changed[] map
  //
  cppScope->addLineComment(CRYPT("- Derived Clock sensitive PIs"));
  
  // Start profiling for derived/misc pi updates
  startSampleSchedBucket(CRYPT("Derived misc pi update"), cppScope);

  codeDerivedClockEdgeDetection(cppScope, topLevelSched);

  // End profiling for derived/misc pi updates
  stopSampleSchedBucket(cppScope);

  // Flow deposited signals through combinational logic
  topLevelSched->codeDepositComboSchedule(cppScope);

  // Run the input schedule if inputFlow.
  topLevelSched->codeInputFlowCondRunData(cppScope);

  // Derived clocks are computed by testing for changes in a primary clock,
  // dc == derived clock hierarchic name
  // pc == primary clock hierarchic name
  // fn == schedule function name prefix
  // c == derived clock schedule function prefix
  // o == offset in changed[] of primary clock
  // x == offset in changed[] to store edge of derived clock
  // rg == reset guard expression on clock eval logic

  if (topLevelSched->getDerivedClockCount() > 0) {
    codeDerivedClockSchedules(cppScope, topLevelSched);
  }

  // Call each of the sample combinational schedules if any of its
  // dependent clock inputs changed.
  cppScope->addLineComment("Combinational (Sample) Schedules");
  genCombinationalCalls (eCombSample, CRYPT("sample"), cppScope, topLevelSched);

  // Track the PIs that clocks are sensitive to.
  UInt32Set SyncSensitives;

  // Call the sequential schedule if the dependent clock changed AND
  // we're on the correct edge.
  //
  cppScope->addLineComment(CRYPT("Sequential Schedules on clock edges"));
  genSequentialCalls(SyncSensitives, cppScope, topLevelSched);

  // Update all StateUpdate<T> instances if a normal clock changes
  //
  SyncOnClock(SyncSensitives, mSched->loopSUNets (), cppScope, topLevelSched);

  // Call each of the transition combinational schedules if any of its
  // dependent clock inputs changed.
  
  cppScope->addLineComment(CRYPT("Combinational (Transition) Schedules"));
  genCombinationalCalls (eCombTransition, CRYPT("transition"), cppScope, topLevelSched);
}

/*!
 * Emit the simulation interface header.  This MUST be parsable as
 * either C or C++ code
*/
void
CodeGen::codeInterface(void)
{
  /**************************************************************************/
  /* Append the necessary external interface code, including the top level
     event-selection schedule to the schedule file.
     This code is compiled with C++ and can see our headers.
  */

  switchStream (eCGSchedule);
  UtOStream&	schedSrc= getCGOFiles()->getScheduleSrc();

  CGOUT () << "namespace " << getNamespace () << "{" << ENDL;

  // Free storage allocated for simulation object, debug state, et al.
  // Also print statistics if it was requested
  // Emit a declaration and place into the infrequent section.
  //
  UtString destroy_proto;
  destroy_proto << "extern void carbon_" 
		<< mIfaceTag << "_destroy (carbon_model_descr *descr)";
  schedSrc << destroy_proto;
  emitSection(schedSrc, CRYPT("carbon_infrequent_code"));
  schedSrc << ";\n";
  schedSrc << "\n// Cleanup simulation\n"
                << destroy_proto
		<< "\n{\n";

  CGOUT() << CRYPT("  carbonPrivateCallDestroyCallback (descr);\n");
  // Delete any task hier ref functor class pointers
  CGOUT() << CRYPT("\n  // Delete any task hier ref functor pointers\n");
  generateCarbonSimulation(schedSrc, mTopName);

  DeleteFunctorCallback dfCallback(mIODB->getDesignSymbolTable(), this, mCGOFiles);
  NUDesignWalker walker(dfCallback, false, false);
  walker.design(mTree);
  CGOUT() << "\n";

  schedSrc << CRYPT("  if (descr->mChangeArray) {") << ENDL
           << CRYPT("    carbonPrivateDestroyChangeArray (descr->mChangeArray);\n")
           << CRYPT("  }\n")
           << CRYPT("  descr->maybePrintStatsStdout(\"") << mIfaceTag << "\", "
           << CRYPT(" sizeof(carbon_simulation));\n")
           << CRYPT("  delete &") << noprefix(mTopName) << ";\n"
           << CRYPT("#ifdef BVPERF\n")
           << CRYPT("  delete BVPC; BVPC=0;\n")
           << CRYPT("#endif\n")
           << CRYPT("#ifdef MEMORYPERF\n")
           << CRYPT("  delete MemPC; MemPC=0;\n")
           << CRYPT("#endif\n");

  schedSrc << CRYPT("  // remove model and Programmer Error Message Context Reference\n")
           << CRYPT("  carbonPrivateRemoveModel (descr);\n");
  schedSrc << CRYPT("  carbonPrivateFreePrivateModelData (descr);\n");
  schedSrc << CRYPT("  carbonmem_free (descr);\n");

  schedSrc << "}\n\n";

  // We need to populate the CGPortInterface object so we can write it
  // out to the database.
  populatePortInterface();
  
  bool isGeneratingPortInterface = isGeneratePortInterface();
  // Define a function that initializes a carbon_design_ports
  // structure
  if (isGeneratingPortInterface)
  {
    codePortInterfaceInitialization();
  }
  
  putInternalMode();


  // Declare a function that maps the storage offset to change array
  // index
  schedSrc << CRYPT("static void ") << cChangeMapFnName
                << CRYPT("(carbon_model_descr *descr);\n");

  {
    CGTopLevelSchedule topLevelSched(this, mTopLevelSchedID);
    topLevelSched.analyzeTopLevelSchedule();
    topLevelSched.emit(schedSrc);
  }


  schedSrc << "} // end namespace\n\n";
  schedSrc << CRYPT("static ShellNet *carbon_debug_generator(UInt32, void* storage, int);\n\n");
  schedSrc
    << CRYPT("\n// Implement the model create method\n")
    << CRYPT("extern \"C\" CarbonObjectID* carbon_") << mIfaceTag
    << CRYPT("_create(CarbonDBType dbType, CarbonInitFlags flags)\n")
    << "{\n";

  // Note that since we don't have a CarbonModel object until late in
  // this function, we don't call genSetCurrentModel, so time spent by
  // this function doesn't show up in -profile output for this model.
  // It will show up as "Outside of Carbon" unfortunately.

  schedSrc << CRYPT("  int modFlags = flags;\n");
  if (getCarbonContext()->getArgs()->getBoolValue("-g")) 
    schedSrc << CRYPT("  modFlags |= eCarbon_ClockGlitchDetect;\n");

  schedSrc
    << CRYPT("  carbon_simulation* hdl = carbon_") << mIfaceTag 
    << CRYPT("_construct ();\n");

  // We're about to allocate the changed array - we'd better not have
  // more things tested than we have allocated.

  UInt32 changeSize = mChangedMap->size ();

  UtString prefix;
  prefix << CRYPT("carbon_") << mIfaceTag << "_";

  // construct the carbon_model_descr instance
  schedSrc << "  struct carbon_model_descr *descr = \n"
           << "    static_cast <carbon_model_descr *> (carbonmem_malloc (sizeof (carbon_model_descr)));\n";
  schedSrc << "  hdl->mTop.mDescr = descr;\n";
#define SET(_member_, _value_) schedSrc << "  descr->" #_member_ " = " << _value_ << ";\n";
  SET (mVersion, "CARBON_MODEL_VERSION");
  SET (mInitFlags, "CarbonInitFlags (modFlags)");
  SET (mHdl, "hdl");
  SET (mHdlSize, "sizeof (carbon_simulation)");
  SET (destroyFn, prefix << "destroy");
  SET (schedFn, prefix << "schedule");
  SET (initFn, prefix << "init");
  SET (chgIndFn, cChangeMapFnName);
  SET (simOverFn, "NULL");
  bool writeFullDB = !getCarbonContext()->getArgs()->getBoolValue(CRYPT("-nodb"));
  if (mEmbedIODB) {
    SET (getIODBFn, prefix << "get_iodb");
  } else {
    SET (getIODBFn, "NULL");
  }
  if (mEmbedIODB && writeFullDB) {
    SET (getFullDBFn, prefix << "get_fulldb");
  } else {
    SET (getFullDBFn, "NULL");
  }
  SET (clkSchedFn, prefix << "clkSchedule");
  SET (dataSchedFn, prefix << "dataSchedule");
  SET (modeChangeFn, prefix << "modeChange");
  SET (asyncSchedFn, prefix << "asyncSchedule");
  SET (debugSchedFn, mDebugScheduleName);
  SET (depositComboSchedFn, mDepositComboScheduleName);
  SET (saveFn, mSaveFnName);
  SET (restoreFn, mRestoreFnName);
  if (isOnDemand ()) {
    SET (onDemandSaveFn, mOnDemandSaveFnName);
    SET (onDemandRestoreFn, mOnDemandRestoreFnName);
  } else {
    SET (onDemandSaveFn, "(CarbonSaveFn) 0");
    SET (onDemandRestoreFn, "(CarbonRestoreFn) 0");
  }
  SET (debugGeneratorFn, "carbon_debug_generator");
  SET (mStdout, "stdout");
  SET (mStderr, "stderr");
  SET (pfwrite, "fwrite");
  SET (pfflush, "fflush");
  // The private data must be initialised after the mStdout field because the
  // error stream plucks its FILE * from the descr->mStdout.
  SET (mPrivate, "carbonPrivateCreatePrivateModelData (descr, hdl)");
  UtString uid;
  getUID (&uid);
  SET (mUID, "\"" << uid << "\"");
  SET (mModel, "NULL");
  schedSrc << CRYPT("  assert (CHANGED_ARRAY_SIZE == ") << changeSize << ");\n"; // this_assert_OK
  SET (mChangeArray, "carbonPrivateCreateChangeArray (" << changeSize << ");\n");
#undef SET

  // Emit code to allocate space for possible primary clock race conditions
  schedSrc << CRYPT("  if (");
  codeGlitchTest(schedSrc);
  schedSrc << CRYPT(") {\n")
           << CRYPT("    descr->allocatePrimaryClockChangeData(")
           << mPrimaryClocks.size() << ");\n";
  for (ClockIter i = mPrimaryClocks.begin(); i != mPrimaryClocks.end(); ++i) {
    const NUNetElab* clkElab = i->first;
    int index = i->second;
    UtString clkName;
    clkElab->compose (&clkName, NULL, true);    // include root in name
    UtString clkName2;
    StringUtil::escapeCString(clkName.c_str(), &clkName2);
    schedSrc << CRYPT("    descr->setClockName(") << index << ", \""
             << clkName2 << "\");\n";
  }
  schedSrc << "  }\n";

  CGOUT() << CRYPT("  if ((modFlags & eCarbon_EnableStats) != 0)\n");
  CGOUT() << "  {\n"
          << CRYPT("    descr->printIntervalStatistics (\"DesConstr\");\n")
          << CRYPT("    descr->resetScheduleCallCount ();\n")
          << "  }\n";

  // get the base of the fileroot
  UtString designName;
  {
    UtString basePath;
    OSParseFileName(mCarbonContext->getFileRoot(), &basePath, &designName);
  }
  UtString dbFileRoot;
  StringUtil::escapeCString(designName.c_str(), &dbFileRoot);

  // Allocate instance specific space for msgcontext, VerilogOutFileSystem
  schedSrc << CRYPT("  carbonInterfaceAddMessageContext(descr);\n\n");

  // Get the pointer to the model's simulation time.  See the definition
  // of ShellGlobal::gCarbonGetTimevarAddr for an explanation of why
  // this is done.
  schedSrc << CRYPT("  descr->mpTime = carbonPrivateGetTimeVarAddr (descr->mHdl);\n\n");

  schedSrc
    << CRYPT("  CarbonModel* model = carbonInterfaceNewModel(\"")
    << mIfaceTag << "\", \"" << dbFileRoot << "\", dbType,\n"
    << "  \"" << gCarbonVersion() << "\",\n";
  schedSrc << CRYPT("    descr);\n");
  // If CarbonModel construction failed then trash the carbon_model then the
  // descr destroy function is called by new model
  schedSrc << CRYPT ("  if (model == NULL) {\n")
           << CRYPT ("    descr = NULL;\n")
           << CRYPT ("  } else {\n")
           << CRYPT ("    assert (descr->mModel == model);\n") // this_assert_OK
           << CRYPT ("  }\n");
  // We can't set the number of schedule buckets when instantiating the model,
  // because the schedule buckets haven't yet been created when emitting the
  // model constructor, so we wait until here and call a method.
  schedSrc << CRYPT("#ifdef PROFILING") << ENDL
           << "  static CarbonProfileInfo prof_info = {\n"
           << "    \"" << mIfaceTag << "\",\n"
           << "    " << getBucketID()->numBuckets() << ",\t\t\t\t// Number of block buckets\n"
           << "    &" << getBucketVarName() << ",\t\t// Block bucket variable\n"
           << "    " << getSchedBuckets()->numBuckets() << ",\t\t\t\t// Number of schedule buckets\n"
           << "    &" << getScheduleBucketVarName() << "\t// Schedule bucket variable\n"
           << "  };\n"
           << "  if (model)\n"
           << CRYPT("    descr->setupProfiling(&prof_info);\n")
           << CRYPT("#endif\n");

  // Run the initialization unless the caller asked us not to
  schedSrc << CRYPT("  if (model && descr->runInit())\n")
           << CRYPT("    descr->initialize(NULL, NULL, NULL);\n")

    // Include file generated by Makefile.mc that sets PROFILE_GENERATE to 0 or 1.
    // Can't use a header file included by precompiled headers.
    // Need to generate same call in both cases to profiling data matches both.
           << CRYPT("#include \"profile_generate.h\"\n")
           << CRYPT("#if PROFILE_GENERATE") << ENDL
           << CRYPT("  carbonInterfaceReportProfileGenerate(true);\n")
           << CRYPT("#else\n")  
           << CRYPT("  carbonInterfaceReportProfileGenerate(false);\n")
           << CRYPT("#endif\n");

  // Return the model
  schedSrc << CRYPT("  return descr;\n") << "}\n\n";
  // The interface file is a .h file that provides a prototype for
  // calling the default schedule driver, passing in all the inputs,
  // clocks, and outputs in a structure.  This file eventually goes
  // into the directory where the '-o ' flag points to.
  //
  UtString ifaceName;
  ifaceName << mTargetName << ".h";

  
  UtOStream *ifHdr = CGFileOpen (ifaceName.c_str() ,
                                 "/* C/C++ interface to external code. */\n"
                                 "",
                                 isObfuscating(),
                                 true);
  if (not ifHdr)
    return;                     // already error'ed

  getCGOFiles()->mIfaceHdr = ifHdr;

  *ifHdr << "#ifndef __" << mIfaceTag << "_H_" << ENDL
         << "#define __" << mIfaceTag << "_H_\n"
         << "\n"
         << "#ifndef __carbon_capi_h_\n"
         << "#include \"carbon/carbon_capi.h\"\n"
         << "#endif\n"
         << ENDL;
    
  // Default code-emission to the interface file now...
  //
  switchStream (eCGIface);


  putInternalMode();

  *ifHdr << "#ifdef __cplusplus" << ENDL
         << "#define EXTERNDEF extern \"C\"\n"
         << "#else\n"
         << "#define EXTERNDEF\n"
         << "#endif"
         << ENDL;
  
  *ifHdr << "EXTERNDEF CarbonObjectID* carbon_" << mIfaceTag
         << "_create(CarbonDBType dbType, CarbonInitFlags flags);\n";
  
  // Emit the port interface (for systemc and performance-critical
  // apps)
  if (isGeneratingPortInterface)
    codePortInterface(ifHdr);
  
  // Update the iodb so we know that the port interface structure is
  // generated
  mIODB->putIsPortInterfaceGenerated(isGeneratingPortInterface);
  
  *ifHdr << "#endif\n";
    
  delete (getCGOFiles()->mIfaceHdr);		// Destructor closes the file
  
  switchStream (eCGSchedule);
} // CodeGen::codeInterface


bool CodeGen::encodeIODBFile(CarbonDBType type) {
  const char* buildDir = getBuildDirectory();
  UtString dbpath, dbfile, cgen_dbfile, errmsg, dir, libfile, cfuncname;
  const char* fileRoot = mCarbonContext->getFileRoot();
  mIODB->getDBFileName(&dbpath, type, fileRoot);
  OSParseFileName(dbpath.c_str(), &dir, &dbfile);

  // construct a filename .carbon.libdesign/libdesign.io.db.cxx.
  // Note, this must be consistent with two pieces of Makefile.mc:
  //
  //        lib%.db.o : lib%.db.cxx $(HEADERS)
  //                $(MCCOMPILE) -o $@ $<
  // and
  //        SOURCES=... 
  //        	$(wildcard lib*.db.cxx)
  OSConstructFilePath(&cgen_dbfile, buildDir, dbfile.c_str());

  // generate filename ".carbon.libdesign/design.iodb.cxx"
  cgen_dbfile << ".cxx";

  cfuncname << CRYPT("carbon_") << mIfaceTag;

  // generate C function name carbon_design_create_iodb or _fulldb.
  switch (type) {
  case eCarbonFullDB:
    cfuncname << CRYPT("_get_fulldb");
    break;
  case eCarbonIODB:
    cfuncname << CRYPT("_get_iodb");
    break;
  case eCarbonGuiDB:
    // Embedding of the GUI DB is not supported. Indeed, it does not make much
    // sense given that the GUI DB is written early in a modified compile flow
    // but embedding cannot occur until codegen time.
    ASSERT (type != eCarbonGuiDB); /* this_assert_OK */
    break;
  default:
    // Might as well catch anything weird...
    ASSERT (type == eCarbonIODB || type == eCarbonFullDB); /* this_assert_OK */
    break;
  }

  if (! UtConvertFileToCFunction(dbpath.c_str(), cgen_dbfile.c_str(),
      cfuncname.c_str(), &errmsg))
    {
      UtIO::cout() << errmsg.c_str() << "\n"; // use msg file
      return false;
    }
  return true;
} // bool Codegen::encodeIODBFile

template<class ScheduleGroupClass, class ScheduleLoopClass>
bool CodeGen::groupSchedulesByInputs(ScheduleGroupClass & scheduleGroups,
				     ScheduleLoopClass loop)
{
  int size = 0;

  typename ScheduleGroupClass::value_type * currentGroup = NULL;

  const SCHInputNets* inputNets = NULL;
  for ( /* initialized by caller */ ;
       not loop.atEnd(); 
       ++loop) {
    // Get the group for this DCL
    typename ScheduleLoopClass::value_type one_schedule = (*loop);
    const SCHInputNets * thisInputNets = one_schedule->getBranchNets();

    if ((currentGroup == NULL) or (thisInputNets != inputNets)) {
      // Start a new group
      scheduleGroups.resize(++size);
      currentGroup = & scheduleGroups.back();
      inputNets = thisInputNets;
      currentGroup->first = inputNets;
    }

    // Add this DCL schedule
    currentGroup->second.push_back(one_schedule);
  }

  return (size>0);
}

void CodeGen::codeGlitchTest(UtOStream& schedSrc)
{
  if (getCarbonContext()->getArgs()->getBoolValue("-g")) {
    schedSrc << CRYPT("1 /* -g turns glitch detection on */ ");
  } else {
    schedSrc << CRYPT("descr->checkClockGlitches()");
  }
}

void CodeGen::genSetCurrentModel(UtOStream& out)
{
  out << CRYPT("#ifdef PROFILING") << ENDL
      << CRYPT("  struct carbon_model_descr *prev_model = carbonInterfaceGetCurrentModel();\n")
      << CRYPT("  carbonInterfaceSetCurrentModel (descr);\n")
      << CRYPT("#endif\n");
}


void CodeGen::genRestoreCurrentModel(UtOStream& out)
{
  out << CRYPT("#ifdef PROFILING") << ENDL
      << CRYPT("  carbonInterfaceSetCurrentModel (prev_model);\t// Restore\n")
      << CRYPT("#endif\n");
}


void CodeGen::pushTempContext() 
{
  // Push a context for temporary caching.
  PredicateToIntMap * predicate_map     = mPredicateMapStack.top();
  PredicateToIntMap * new_predicate_map = new PredicateToIntMap;
  new_predicate_map->insert(predicate_map->begin(),predicate_map->end());
  mPredicateMapStack.push(new_predicate_map);

  EventToIntMap * event_map     = mEventMapStack.top();
  EventToIntMap * new_event_map = new EventToIntMap;
  new_event_map->insert(event_map->begin(),event_map->end());
  mEventMapStack.push(new_event_map);
}


void CodeGen::popTempContext() 
{
  // Pop the context for temporary caching.
  PredicateToIntMap * predicate_map = mPredicateMapStack.top();
  mPredicateMapStack.pop();
  delete predicate_map;

  EventToIntMap * event_map = mEventMapStack.top();
  mEventMapStack.pop();
  delete event_map;
}


void CodeGen::printChangedMap()
{
  static const char* net = CRYPT("Net ");
  for (CGNetElabCount::NetElabMapIter p = mChangedMap->loopNetElabs();
       ! p.atEnd();
       ++p)
  {
    UtIO::cout() << net << p.getKey() << " -> " << p.getValue() << UtIO::endl;
  }
}

SInt32 CodeGen::getChangedIndex(const NUNetElab* net) const {
  return mChangedMap->getIndex(net);
}
