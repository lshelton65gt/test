# -*- Makefile -*-
#
# *****************************************************************************
# Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
# *****************************************************************************
#
# compiler-dependent makefile fragment for Intel Compiler Release 8.0

# Define where compiler can be found
#
CARBON_COMPILER_DIST= ${CARBON_HOME}/Linux/icc
ifeq (,$(INTEL_LICENSE_FILE))
$(error "INTEL_LICENSE_FILE must be defined to use icc")
endif

CARBON_CBUILD_CXX = ${CARBON_COMPILER_DIST}/bin/icpc
CARBON_CBUILD_CC  = ${CARBON_COMPILER_DIST}/bin/icc 
CARBON_CBUILD_LD  = ${CARBON_CBUILD_CXX} ${CARBON_CXX_DEF_LINK_FLAGS}

# Need special archive program to do interprocedural optimizations
# But also need to wrap it in a script to suppress its chattiness
CARBON_AR = ${CARBON_HOME}/bin/xiar
CARBON_RANLIB    = ${CARBON_HOME}/bin/xiar -s

# Search all passed in options for -O<level> flags
CARBON_CXX_OPTLIST:=$(filter -O%,$(CXX_EXTRA_FLAGS))

# Find the last one specified
ifeq (,$(CARBON_CXX_OPTLIST))
CARBON_CXX_OPT := -O0
else
CARBON_CXX_OPT:=$(word $(words $(CARBON_CXX_OPTLIST)),$(CARBON_CXX_OPTLIST))
endif


# Force using OUR asm and ld as opposed to whatever's on the local
# machine and user's search path.
#
CARBON_OPTLFLAGS = -Qlocation,ld,${CARBON_HOME}/Linux/bin
CARBON_OPTFLAGS =  -Qlocation,asm,${CARBON_HOME}/Linux/bin

ifneq (,$(findstring $(CARBON_CXX_OPT), -O2 -O3 -O4))
# -ipo not working well with icc 9.0
CARBON_OPTFLAGS += $(call CARBON_ICC8,-ipo) -msse
CARBON_OPTLFLAGS += $(call CARBON_ICC8,-ipo)
else
ifeq ($(CARBON_CXX_OPT),-O1)
CARBON_OPTFLAGS +=
endif
endif

# what processor are we running on?
#CPUFAMILY=$(subst cpu family:,,$strip($(grep '^cpu family' /proc/cpuinfo)))

## Flags for Intel compiler (K=PII, W=P4, N=P4+new opts, B=PentiumM, P=P4+SSE3
## (eventually, we should default the codegenerator to the machine it's compiling from)
## but allow override by -Wc options.
#CPU15:=N			# Xeon
#CPU6:=				# Athlon



# check for icc7 vs. icc8
CARBON_ICC_VERSION:=$(shell $(CARBON_CBUILD_CXX) -dryrun -V 2>&1 )
ifneq (,$(findstring Version 7,$(CARBON_ICC_VERSION)))
CARBON_ICC7 = $(1)
CARBON_ICC8 = 
CARBON_ICC9 =
else
ifneq (,$(findstring Version 8,$(CARBON_ICC_VERSION)))
CARBON_ICC7 = 
CARBON_ICC8 = $(1)
else
CARBON_ICC7 =
CARBON_ICC8 =
CARBON_ICC9 = $(1)
endif
endif

# Basic compiler-dependent flags.
# Disable some Intel warnings and errors
#   68 - integer conversion resulted in a change of sign
# 1505 - size of class is affected by tail padding
#  654 - overloaded virtual function only partially overridden in class
#  597 - Forcible<T,MT>::operator BitVector<66>&() will not be called...
# 1476 -  field uses tail padding of a base class,  UInt8 mShadow (tmp.schedule)

CARBON_EXCEPTIONS_OFF:=-fno-exceptions
CARBON_EXCEPTIONS_ON :=-fexceptions



#
# Flags that we recommend passing to all compilation units.
#
CARBON_CXX_FLAGS = $(CARBON_EXCEPTIONS_OFF)

# We want the rpath set by default to avoid setting LD_LIBRARY_PATH
#
CARBON_LINK_RPATH = -Wl,-rpath,$(CARBON_COMPILER_DIST)/lib,-rpath,$(CARBON_HOME)/Linux/gcc3/lib

# #111 - statement is unreachable
# #271 - trailing comma is nonstandard
# #592 - variable used before initialization.
# #869 - unused function parameter
# #981 - operands evaluated in unspecified order
# #1419 - external declaration in primary source file [WTF?]
# #1683 - truncating conversion of 64 bit type to smaller
CARBON_CXX_DEF_COMPILE_FLAGS= \
	$(CARBON_CXX_FLAGS) 					\
	-cxxlib-gcc -gcc-name=$(CARBON_HOME)/Linux/gcc3/bin/gcc \
	$(CARBON_LINK_RPATH) \
	-fno-exceptions \
	-wd68,654,597,1476\
	$(call CARBON_ICC8,-wd1505) \
	$(call CARBON_ICC9,-wd111 -wd271 -wd592 -wd869 -wd981 -wd1419 -wd1505 -wd1683 -gcc-version=340 ) \
	-D__ICC__ \
	-D__ELF__ \
	-Dlinux \
	$(call CARBON_ICC9,-D__restrict=) \
	$(CARBON_OPTFLAGS)


##	-D__tune_i686__ \
##	-Asystem=posix \

CARBON_CC_DEF_COMPILE_FLAGS = $(CARBON_CXX_FLAGS) $(CARBON_LINK_RPATH) \
	-cxxlib-gcc -gcc-name=$(CARBON_HOME)/Linux/gcc3/bin/gcc $(call CARBON_ICC9,-gcc-version=340 -D__restrict= )

CARBON_CXX_DEF_LINK_FLAGS = $(CARBON_CXX_FLAGS) $(CARBON_LINK_RPATH) \
	-cxxlib-gcc -gcc-name=$(CARBON_HOME)/Linux/gcc3/bin/gcc \
	$(call CARBON_ICC9,-gcc-version=340 -D__restrict= ) \
	$(CARBON_OPTLFLAGS) \

CARBON_CXX_SPEEDCC_COMPILE_FLAGS = -Werror $(CARBON_CXX_FLAGS)

# Places to search for libraries.  Assuming a mixed icc/g++ environment
# 
# icc is only a runtime target; we need to search the icc-compiled versions
# of the carbon runtime libraries.
CARBON_COMPILER_LIB_DIRS = ${CARBON_COMPILER_DIST}/lib/ $(CARBON_HOME)/Linux/gcc3/lib


#CXXLD = $(CARBON_COMPILER_DIST)/bin/iccbin

# flags needed to compile shared libraries
CARBON_SHARED_COMPILE_FLAGS= -shared -fPIC
# flags needed to link shared lib.  Note that 'LIBPATH' is defined in Makefile.mc
CARBON_SHARED_LINK_FLAGS=-Wl,-rpath,$(@D)

CARBON_COMPILER_LIB_LIST := $(call CARBON_ICC9, -L$(CARBON_COMPILER_DIST)/lib -lcxa -lunwind)
