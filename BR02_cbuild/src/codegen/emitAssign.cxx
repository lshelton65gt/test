// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 /*
 * \file
 * Centralized implementation of all code emitters for classes derived from
 * class NUBase.
 *
 * We emit code by passing down context flags that guide each method
 * in establishing what kinds of code are needed (declarations,
 * expressions, or various kinds of names. 
 *
 * Context flags are returned for those places that need to be aware of
 * the generated context for a subordinate tree.  (See the NUAssign and
 * NUIdentLvalue emitCode() methods for an example.
 *
 * A very few routines do complex analysis of their operands or are
 * aware of the existance of multiple output files (see NUModule::emitCode())
 * 
 */

#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUTriRegInit.h"

#include "codegen/CGAuxInfo.h"
#include "codegen/codegen.h"
#include "codegen/CGProfile.h"

#include "util/CbuildMsgContext.h"
#include "util/CodeStream.h"

#include "reduce/Fold.h"

#include "emitUtil.h"

using namespace emitUtil;


/*!
 * Check for 
 * \verbatim
 * X = X op Y  ==> X op= Y tranformations.
 *           X = <op> X  ==> X.opname()  (these are only valid for vectors)
 * \a stmt - an assignment statement
 * \endverbatim
 * \returns true if transform permitted and updates the assignment node in place if it needs to
 * swap operands to place the assignment into canonical form for a = a op b;
 */

static bool sIsOpEqual (NUAssign* stmt)
{
  const NULvalue *lhs = stmt->getLvalue ();

  const NUNet *lnet = emitUtil::sFindNet (lhs);

  if (not lnet                  // probably something like a concat!
      || (lnet->is2DAnything() && lnet->isDoubleBuffered ()) // temp memory
      || netIsDeclaredForcible(lnet)) // need special handling for forcible nets
    return false;

  // If the destination isn't a POD or a BitVector (or something
  // derived from Bitvector, then it doesn't have OP= operators
  // defined for it.  BUG2400.  BitVectors implement OP=, and all
  // object types derived from Bitvector expose the bitvector operators.
  //
  if (lnet->getBitSize () <= LLONG_BIT) {
    if (not emitUtil::isPOD (lnet))
      return false;
    else if (lhs->getBitSize () != lnet->getBitSize ())
      // field-deposit that's not a bitvector sized thing.
      return false;
  }


  NUOp *src = dynamic_cast<NUBinaryOp *>(stmt->getRvalue ());

  if (not src)
    return false;

  // Get a sample Rvalue from the Lvalue to see if we can find a match on the
  // rhs
  const size_t lsize = lhs->getBitSize ();
  NUExpr *lhtemp = lhs->NURvalue ();
  
  if (not lhtemp)
    // Couldn't create a Rvalue for this LHS (concat?)
    return false;

  lhtemp->resize (lsize);
  lhtemp->setSignedResult (src->isSignedResult ());

  // Operator-specific tests.
  bool foundMatch = false;
  switch (src->getOp ())
    {
    case NUOp::eBiSMult:
    case NUOp::eBiUMult:
    case NUOp::eBiPlus:
    case NUOp::eBiBitAnd:
    case NUOp::eBiBitOr:
    case NUOp::eBiBitXor:
      // commutative operands
      if (lhtemp->equal (*(src->getArg (0))))
        foundMatch = true;
      else if (lhtemp->equal (*(src->getArg (1))))
      {
	NUExpr* temp = src->getArg (0);
        src->putArg (0, src->getArg (1));
        src->putArg (1, temp);
        foundMatch = true;
      }
      break;
      
    case NUOp::eBiMinus:
      // non-commutative, although could do a=b-a => a.negate(); a+=(b)
      // (but that's better implemented as nucleus transforms.
      // fall-thru
    case NUOp::eBiLshift:
    case NUOp::eBiLshiftArith:
    case NUOp::eBiSDiv:
    case NUOp::eBiUDiv:
    case NUOp::eBiSMod:
    case NUOp::eBiUMod:
    case NUOp::eBiRshift:
    case NUOp::eBiRshiftArith:

      // Only left operand interesting...
      foundMatch = lhtemp->equal (*(src->getArg (0)));
      break;

    case NUOp::eUnMinus:
    case NUOp::eUnBitNeg:
    case NUOp::eUnVhdlNot:
      // For bitvectors, we can do ~ and - in place
      if (lnet->getBitSize () > LLONG_BIT
          && (lhtemp->equal (*(src->getArg (0))))
          && not emitUtil::netIsDeclaredForcible (lnet))
        foundMatch = true;
      break;

    default:
      break;
    }
  
  delete lhtemp; // Done with rvalue temp

  // Now check for special cases that break codegen if tranformed into op=
  if (!foundMatch) {
    // failed the test for conversion to op=
  } else if (emitUtil::needsSignExtension (src->getOp (), src->getArg (0), src->getArg (1), lsize, 0)) {
    // The matched expression will codegen as Extender<...>. This cannot be
    // used as an lvalue in a C++ assignment so the assignment cannot be turned
    // into an op= (bug 7301)
    return false;
  }

  // Special cases with when the lvalue will be a varsel...
  NUVarselRvalue *varsel;
  if (!foundMatch) {
    // already rejected the op= tranformation
  } else if ( src->getArg (0)->getType () != NUExpr::eNUVarselRvalue) {
    // not a varsel
  } else if (lsize > 1) {
    // This will be codegened as a GET_SLICE_CHECK. This cannot be used as an
    // lvalue for op= (bug 7510)
    return false;
  } else if ((varsel = dynamic_cast <NUVarselRvalue *> (src->getArg (0))) == NULL) {
    NU_ASSERT (varsel != NULL, src->getArg (0)); // bizarre
  } else if (varsel->getIdentExpr ()->getType () == NUExpr::eNUMemselRvalue) {
    // Cannot map varsel->memsel to op= (bug710)
    return false;
  } else if (varsel->getIdentExpr ()->getType () == NUExpr::eNUVarselRvalue) {
    // this will break things as badly and memsels...
    return false;
  }
  return foundMatch;
}

// Generate an expression fragment that correctly masks the destination bits
// (which may be a partselect) and clears the masked bits in the source
// operand -- but that's the very next thing that will be emitted...
static UInt32
sGenForceMaskedValue (const NULvalue* lhs, CGContext_t *pcontext)
{
  // Assigning to a piece of a vector, but not using the SET_BIT
  // or SET_SLICE macros - so we have to mask the expression
  // ala:   F = (F&F.mask)|(E & ~F.mask)
  UInt32 lsize = lhs->getBitSize ();

  NUExpr *fnet = lhs->NURvalue (); // Corresponding partsel
  fnet->resize (lsize);

  // Accessing the getMask() member requires help from NUNet::emitCode()
  // because this might be a partsel so we need X.getMask.lpartsel(p,s) generated.
  //
  CGContext_t maskCtxt = *pcontext | eCGForcible;

  UtOStream& out = gCodeGen->CGOUT ();

  UInt32 needParens = 0;
  if (lsize == 1)
    // Generate
    //    mask[k] ? dest[k] : (...
    {
      out << TAG;
      fnet->emitCode (maskCtxt);
      out << "?";
      fnet->emitCode (*pcontext);
      out << ":(";
      needParens = 1;
      *pcontext = eCGVoid;
      out << TAG;
    }
  else
    {
      out << TAG;
      CGContext_t retc = eCGVoid;

      if (lsize > LLONG_BIT)
	// We're constructing a temp BitVector, no temp needed
	{
	  maskCtxt &= ~eCGNeedsATemp;
	  *pcontext &= ~eCGNeedsATemp;
	  retc = eCGIsATemp;
	}

      out << "(" << CBaseTypeFmt (lsize) << "(";
      retc |= fnet->emitCode (maskCtxt); // the mask
      out << ") & ";
      retc |= fnet->emitCode (*pcontext);  // the current destination value
      out << " | (~" << CBaseTypeFmt (fnet->getBitSize ()) << "(";
      fnet->emitCode (maskCtxt); // the ~mask
      out << ") & ";
      needParens = 2;

      *pcontext = retc;
      out << TAG;
    }

  delete fnet;			// Done with this piece of tree
  return needParens;
}


/*!
 * Emit code to set the strength on a tristate.  For non-tristates, do nothing.
 *
 * If the net is a primary and the strength is low, only set the
 * strength of the bits not being externally driven.
 *
 * For high strength, emit:
 *   net.setStrength();
 * For low strength, emit:
 *   net.clearStrength();
 */
static void emitResetTristate(const NULvalue *lvalue,
			      Strength strength,
			      CGContext_t context)
{
  if (not isDefine(context)) {
    return;
  }

  UtOStream& out = gCodeGen->CGOUT ();
  
  if (lvalue->isWholeIdentifier())
  {
    NUNet *net = lvalue->getWholeIdentifier();
    

    if (not gCodeGen->isTristate (net))
      // not a tristate, do not do anything
      return;
    
    if (strength == eStrWeakest) {
      
      out << TAG;
      out << "  ";
      net->emitCode(context|eCGLvalue);
      out << ".clearStrength();" << ENDL;
    } else {
      out << TAG;
      out << "  ";
      net->emitCode(context|eCGLvalue);
      out << ".setStrength();" << ENDL;
    }
  }
  else
  {
    // See if we can easily identify if this is tristated
    const NUNet* net = emitUtil::sFindNet (lvalue);

    if (net && not gCodeGen->isTristate (net))
      // No need to process as a tristate.
      return;

    if (const NUVarselLvalue* vs = dynamic_cast<const NUVarselLvalue*>(lvalue))
    {
      NU_ASSERT (net, lvalue);

      const ConstantRange* range = vs->getRange();
      const NUIdentLvalue* identLval = dynamic_cast<const NUIdentLvalue*>(vs->getLvalue());
      
      if (identLval) {
        if (gCodeGen->isTristate(net)) {
          out << TAG;
          net->emitCode(context|eCGLvalue);
          out << ".setStrength(";
          if (! vs->isConstIndex()) {
            // variable partsel
            vs->getIndex()->emitCode(context);
            if (range->getLsb ())
            {
              out << "+";
              out << range->getLsb();
            }
          }
          else {
            // constant partsel
            out << range->getLsb();
          }
          
          out << ", " << range->getLength() << ");" << ENDL;
        } // if tristate ident
      }
      else
        NU_ASSERT("Malformed varsel" == NULL, vs);
    } // if varsel
    else if (const NUMemselLvalue *msel = dynamic_cast<const NUMemselLvalue*>(lvalue))
    {
      if (not gCodeGen->isTristate (msel->getIdent ()))
        return;                 // not a tristate - can ignore
      out << "  ";
      msel->emitCode (context | eCGLvalue);
      if (strength == eStrWeakest)
        out << ".clearStrength();" << ENDL;
      else
        out << ".setStrength();" << ENDL;
    }
    else if (const NUConcatLvalue *lcat = dynamic_cast<const NUConcatLvalue*>(lvalue))
    {
      for(UInt32 i=0; i < lcat->getNumArgs (); ++i)
        // Fix each of the pieces
        emitResetTristate (lcat->getArg (i), strength, context);
    }
    else
      // What the heck *is* this?
      NU_ASSERT("Unsupported LHS type" == NULL, lvalue);
  } // if wholeident
}

// Add an assertion to check for dirty bits to an assignment.

static void emitAssert(NULvalue *lv, CGContext_t context)
{
  if (not gCodeGen->mSanitizeCheck)
    return;

  size_t bsize = lv->getBitSize();

  if( bsize >= LLONG_BIT || bsize == LONG_BIT )
    return;

  if( noSemi (context) )
    return;

  if( lv->getType() == eNUMemselLvalue )
    return;

  if(not lv->isWholeIdentifier() )
    // Slices and memory accesses aren't interesting because there are no
    // extra bits of visible padding.
    return;

  NUNet *n = lv->getWholeIdentifier();
  if( n->is2DAnything() || n->getBitSize () > LLONG_BIT || not emitUtil::isPOD (n) )
    // Arrays, bitvectors and any complex type like a Tristate or Forcible are
    // excluded from this assertion checker.
    return;

  // Do not emit assert for the compiler-supported size types because 
  // the physical implementation treats SInt8/SInt16 such that it is
  // hardware sign-extended into 32 bits when we bitwise-and it with
  // ~MASK(8)/~MASK(16). 
  //
  // Stop emitting assertion for any size when the lvalue is signed. 
  // See VHDL test vhdl/lang_predef_func/num_std_funcs_signed . --KLG
  //
  if (n->isSigned())
    return;

  UtOStream& out = gCodeGen->CGOUT ();
  NUExpr* rval = lv->NURvalue ();
  rval->resize (bsize);
  out << ";" << ENDL;
  out << "assert( !(";             // this_assert_OK
  rval->emitCode (context & ~(eCGLvalue | eCGBVOverloadSupported | eCGBVExpr));
  out << " & ~MASK(" 
      << (CBaseTypeFmt ((bsize >LONG_BIT) ? LLONG_BIT : LONG_BIT))
      << ", " << bsize << ")) )";
  delete rval;
}



// Check if the assignment is to a bitvector that has an overlap that needs
// a temporary.
static bool sIsOverlappedBVAssign (const NUAssign* asn)
{
  const NUExpr* src = asn->getRvalue ();
  if (not emitUtil::isPrimary (src) 	// an expression should yield a temporary
    || src->getBitSize () <= LLONG_BIT) // 64 bits or less is read-before-write
    return false;
  
  NUNetRefSet defs (gCodeGen->getNetRefFactory ());

  switch (asn->getType ()) {
  case eNUContAssign:
    asn->getDefs (&defs);
    break;
    
  case eNUBlockingAssign:
    asn->getBlockingDefs (&defs);
    break;

  default:
    NU_ASSERT ("Unexpected assign type" == 0, asn);  // Need to know how to find the defs...
  }

  // If there's more than ONE def, we're going to pry this apart, so we don't have
  // to worry here.
  if (defs.size () > 1)
    return false;

  NUNetRefHdl destHdl = *(defs.begin ());
  ConstantRange destRange;
  destHdl->getRange (destRange);

  NUNetRefSet uses (gCodeGen->getNetRefFactory ());
  asn->getRvalue ()->getUses (&uses);
  for (NUNetRefSet::const_iterator overlap = uses.find (destHdl, &NUNetRef::overlapsSameNet);
       overlap != uses.end ();
       ++overlap) {
    if (destHdl == *overlap)
      // The ranges are identical; since we would have eliminated any assignment-to-self
      // operations, what we're seeing here is:
      //    X[k+:128] = X[j+:128];
      // and only a runtime determination of the relationship between k and j
      // can be sure -- be conservative in this case and allow a temporary to be
      // inserted.
      return true;

    ConstantRange srcRange;
    (*overlap)->getRange (srcRange);

    // Now check for dangerous overlaps.  Because bitvector copy operations
    // run 32 bits at a time, writing to aligned destinations if at all possible,
    // we get into trouble if the source is below the dest at all.  If they overlap
    // exactly, we don't need the assignment, if they overlap such that the destination
    // starts below the source, we will have read the source bits before we stomp
    // them and everything stays happy.
    if (srcRange.contains (destRange.getLsb ()))
      return true;
  }

  return false;
}  

// Check if the assignment writes the sign bit..
static bool sModifiesSignBit (const NUAssign* asn)
{
  NUNetRefSet defs (gCodeGen->getNetRefFactory ());
  if (const NUContAssign *ca = dynamic_cast<const NUContAssign*>(asn))
    ca->getDefs (&defs);
  else if (const NUBlockingAssign *ba = dynamic_cast<const NUBlockingAssign*>(asn))
    ba->getBlockingDefs (&defs);
  else
    NU_ASSERT ("Unexpected assign type" == 0, asn);  // Need to know how to find the defs...

  for(NUNetRefSet::iterator p = defs.begin (); p != defs.end (); ++p)
  {
    // check if the sign bit is def'ed by this partial assignment
    const NUNetRefHdl h = *p;
    const NUNetRef& netref = *h;
    const NUNet* net = netref.getNet ();
    if (not net->isSigned ())
      continue;             // check next definition

    UInt32 signBit = net->getBitSize () - 1;
    for (NUNetRefRangeLoop p = netref.loopRanges (gCodeGen->getNetRefFactory ());
         !p.atEnd (); ++p)
    {
      const ConstantRange r = *p;
      if (r.contains (signBit))
        return true;
    }
  }
  return false;
}


// Simple assignment statements.
//
// Invariant: We NEVER STORE more bits than actually exist
// 	in the object.  That way, we can read machine-unit sizes
//	and not have to mask off our inputs.
//
//	This wins because you read more than you write, on average.
//
CGContext_t
NUAssign::emitCode (CGContext_t context) const
{
  context &= ~eCGFlow;

  CGContext_t lcont;
  UtOStream& out = gCodeGen->CGOUT ();

  out << HDLTAG (this);

  if (not isDefine (context))
    return eCGVoid;		// Assignments don't have declarations

  out << ENDL;

  // Check if this is the first assign to a temporary memory net. We
  // should ignore these because they are just used to make UD accurate.
  NUTempMemoryNet* tmem;
  if (isTempMemInit(&tmem)) {
    if (gCodeGen->isUndeclaredTempNet(tmem))
      // Should not emit the assign
      return eCGVoid;
    // Just set the master
    getLvalue ()->emitCode (context);
    out << ".putBaseMemory(&";
    getRvalue ()->emitCode (context);
    out << ");" << ENDL;
    return eCGVoid;
  }

  // Check for simple assignments where the source and destination overlap.
  // For BVrefs larger than 64 bits, these will potentially overwrite source bits
  // before copying them.
  //
  const NUExpr *rhs = getRvalue ();

  // Forcible nets may require special handling to only assign to the
  // unforced bits.
  const NUNet* lnet = sFindNet (getLvalue ());
  UInt32 accessSize = (lnet ? emitUtil::elementSize (lnet) : getLvalue ()->getBitSize ());

  bool forcible = (lnet != NULL) && emitUtil::netIsDeclaredForcible (lnet);


  const NUTernaryOp *qc;

  if (getLvalue ()->equalAddr (rhs)) {
    // LValue(NET) = RValue(NET) is a nop for idents, partsels or bitsels
    out << TAG;
    out << "(void)0"; // Suppressed assignment - but leave something as a placeholder
    if (not noSemi (context))
      out << ";";

    return eCGVoid;
  }

  // Look for tristated destination without strength set
  if (not isStrengthSet (context)
      && lnet && gCodeGen->isTristate (lnet))
  {
    emitResetTristate (getLvalue (), eStrDrive, context);
    context |= eCGStrengthSet;
  }
    
  if (getLvalue ()->getBitSize () > LLONG_BIT
      && (qc = dynamic_cast<const NUTernaryOp*>(rhs))
      && (qc->getOp () == NUOp::eTeCond))
    // Convert A = e?B:C;
  {
    out << TAG;
    out << "  if (";
    if( !isPrecise( qc->getArg(0)->emitCode(context | eCGFlow) ) )
      mask( qc->getArg (0)->getBitSize(), "&");
    out << ") {" << ENDL;

    CopyContext cc (NULL,NULL);
    NUStmt *asn = new NUBlockingAssign (getLvalue ()->copy (cc),
                                        qc->getArg (1)->copy (cc),
                                        false,
                                        this->getLoc ());
    asn = gCodeGen->getFold ()->stmt (asn);
    if (asn) {
      asn->emitCode (context & ~(eCGNoSemi | eCGList));
      delete asn;
    } else {
      out << "(void)0;";
    }
    out << ENDL << "} else {" << ENDL;
    asn = new NUBlockingAssign (getLvalue ()->copy (cc),
                                qc->getArg (2)->copy (cc),
                                false,
                                this->getLoc ());
    asn = gCodeGen->getFold ()->stmt (asn);
    if (asn) {
      asn->emitCode (context & ~(eCGNoSemi | eCGList));
      delete asn;
    } else {
      out << "(void)0;";
    }
    out << "}" << ENDL;

    return eCGVoid;
  }

  // Check for DEST[i] = 0|1
  //
  const NUConst *rhConst;
  const NUVarselLvalue *lhBit;
  NUNet* lhNet;
  const ConstantRange *base;

  if (getLvalue ()->getType () == eNUVarselLvalue
      && (lhBit = dynamic_cast<const NUVarselLvalue*>(getLvalue ()))
      && !lhBit->isArraySelect()
      && (lhBit->getRange ()->getLength () == 1)
      && (lhNet = lhBit->getIdentNet())
      && not forcible
      && (lhNet->getBitSize () <= LLONG_BIT)
      && (base = getNUrange (lhNet))
      && lhBit->isConstIndex ()
      && (rhConst = rhs->castConst()))
    {
      // Convert to DEST |= 1<<i  or DEST &= ~(1<<i)

      SInt32 bitIndex = lhBit->getRange ()->getLsb () + lhBit->getConstIndex ();
      SInt32 width = lhNet->getBitSize ();

      // Check for out-of-bounds deposit and ignore them
      if (bitIndex >= width)
        return eCGVoid;

      NUIdentLvalue *dest = new NUIdentLvalue (lhNet, this->getLoc ());
      dest->resize ();

      UInt64 bitMask = (KUInt64 (1) << bitIndex); // Convert to a mask value
      NUConst *mask
	= NUConst::create (false, rhConst->isZero () ? (~bitMask): bitMask,
                           width, getLoc ());
      NUExpr *rhs = 
	new NUBinaryOp (rhConst->isZero () ? NUOp::eBiBitAnd : NUOp::eBiBitOr,
			dest->NURvalue (), mask, getLoc ());
      rhs->resize (width);
      
      if (lhNet->isTristate ())
        out << "/* check drive strength */" << ENDL;
      NUStmt* asn = new NUBlockingAssign (dest, rhs, false, getLoc ());
      asn = gCodeGen->getFold ()->stmt (asn);
      if (asn) {
        context = asn->emitCode (context);
        delete asn;
      } else {
        out << "(void)0;" << ENDL;
      }

      return context;
    }

  // Generate the LVALUE
  //
  size_t lsize = getLvalue ()->getBitSize ();
  size_t rsize = rhs->getBitSize ();
  
  bool opEqual = sIsOpEqual (const_cast<NUAssign*>(this));
  bool wholeIdentifier = getLvalue ()->isWholeIdentifier ();
  NUNet* net=0;
  bool signedDest = (wholeIdentifier &&
                     (net = getLvalue ()->getWholeIdentifier ()) &&
                     net->isSigned() && not net->is2DAnything () && not net->isUnsignedSubrange ());
  
  CGContext_t rc = eCGVoid;     // return context.

  if (not opEqual) {
    //
    // The op= cases don't materialize the LHS at all here; they simply code the RHS with
    // an op= or unary self-modifying member function to provide the semantics.
    //
    lcont = getLvalue()->emitCode (context | eCGLvalue);

    // If the LHS was a dynamic-width partselect, we don't want a dirty value for the source
    // so make sure that the RHS gives us a properly masked value.
    //
    if (needsPrecisePrimary (lcont) && emitUtil::isPrimary (rhs))
      context |= eCGPrecisePrimary;

    if (isBitfield(lcont)) {
      // setting a bitfield of size less than or equal to 64 bits... The LHS generated
      // something of the form(s):
      // 		SET_BIT(var,bitnum.
      //		SET_SLICE(var, hibit, lowbit,
      //		Net.lldeposit(p,s,
      //		Net.ldeposit(p,s,
      //		(void)(
      // When SET_BIT or SET_SLICE macro-expand, the forcible class will mask
      // the value so we're okay..  If we are doing a BitVector operation, it
      // doesn't overload the lldeposit to guard the forced bits, so we have
      // to generate a masking expression.
      //

      UInt32 lhExtraParen = 1;

      if (forcible && elementSize (lnet) > LLONG_BIT)
        // Generate the partial expression to mask this assignment
        //     (mask & dest) | (~mask &
      {
        CGContext_t ctxt = context;
        lhExtraParen += sGenForceMaskedValue (getLvalue (), &ctxt);
        if (isATemp (ctxt))
        {
          context &= ~eCGNeedsATemp;
          context |= eCGIsATemp;
        }
      }

      // Casting needs to be parenthesized for test/stateupdate/reorder10
      // with gcc 2.95
      out << TAG;
      out << "(";
      rc = rhs->emitCode (context);

      if (rsize > LLONG_BIT)
      {
        // Dest is 64 bits or less, fetch only what we need
        emitUtil::genBVpartsel (out, 0, lsize);
      }

      // If the constructed value isn't precise, we need to clear
      // the extra bits to insure that memory remains accurate.
      //
      if ( rhs->isSignedResult() ) {
        mask (lsize, "&");
      } else if (!isPrecise (rc) || lsize < rsize ) {
        mask (std::min (lsize,rsize), "&");
      }
      out << ")" << TAG;

      while (lhExtraParen--)
        out << ")";


      if (sModifiesSignBit (this) && accessSize > LLONG_BIT)
      {
        out << ";" << ENDL;
        lnet->emitCode (context | eCGLvalue);
        out << "._M_do_sanitize()" << TAG;
      }
      goto finish;

    } else if (isDelayed (lcont)) {
      switch(getLvalue()->getType())
      {
      case eNUConcatLvalue:
        // LHS concat - {a,b,c} = expr;
      {
        NU_ASSERT (0, this);    // PreCodeGen or ConcatRewrite should have lowered this
        //concatAssign (dynamic_cast<const NUConcatLvalue*>(getLvalue ()), rhs, context);
        break;
      }
      case eNUVarselLvalue:
        // LHS nested NUVarselLvalue
      {
        nestedPartselAssign(dynamic_cast<const NUVarselLvalue*>(getLvalue()),
                            rhs, context);
        break;
      }
      default:
        NU_ASSERT(0, getLvalue());
      }

      goto finish;

    } else if (getLvalue ()->getBitSize () > LLONG_BIT
               && not forcible) {
      if (isZeroValue (rhs)) {
        // Zeroing bitvector
        out << ".reset()" << TAG;
        goto finish;
      } else if (isOnesValue (rhs)) {
        // Setting bitvector high
        out << ".set()" << TAG;
        goto finish;
      }
    }
    
    // Generated a normal LHS
    out << " = " << TAG;
  }


  {
    UInt32 extraParen = 0;

    // Need to generate a cast for LHS which are whole and
    // forcible.  This is because operator= is private for
    // the forcible class, thus the compiler can't resolve.
    bool wholeForcible = forcible;

    if (forcible && not wholeIdentifier)
    {
      // Generate an expression fragment like:
      // 	(mask & dest) | (~mask &
      out << TAG;
      CGContext_t ctxt = context;
      extraParen += sGenForceMaskedValue (getLvalue (), &ctxt);
      if (isATemp (ctxt))
        {
          context &= ~eCGNeedsATemp;
          context |= eCGIsATemp;
        }
      wholeForcible = false;
    }

    // To consider BitVector overload possibility only if the 
    // LHS size > LLONG_BIT, and the RHS size <= LLONG_BIT.
    bool bvOverloaded = (lsize > LLONG_BIT) && (not forcible)
      && (rhs->effectiveBitSize() <= LLONG_BIT);

    // We may need to explicitly construct the RHS to the LHS
    // for forcibles due to operator= private-ness, or for
    // assignments between real and integral values or for
    // assignments where the signedness of source/dest differs
    // and it's a POD expression that GCC may whine about.
    //
    // Also insert a constructor if the LHS and the RHS overlap,
    // are bitvector-sized and either overlap in an unpredictable way,
    // or the src is at a lower bit position than the destination and it's
    // within 32 bits
    // 
    //
    if (!bvOverloaded &&
        !opEqual &&
        (not net || not net->is2DAnything ()) && // not assigning to a whole memory
        (wholeForcible
         || (signedDest != rhs->isSignedResult () && rsize <= LLONG_BIT)
         || getLvalue ()->isReal () != rhs->isReal ()
         || sIsOverlappedBVAssign (this))
      ) {
      // Construct a result of the same size/type as the destination
      //
      out << TAG << CBaseTypeFmt (lsize, eCGDeclare, signedDest, getLvalue ()->isReal ());
    }
    if ( rhs->isSignedResult () && ( getLvalue ()->isReal () != rhs->isReal () ) ){
      // here we are converting a signed value to a real value,
      // make sure the signedness of rhs is preserved even if we mask
      // it during operand codegen
      out << TAG << "(";
      extraParen++;
      out << CBaseTypeFmt (rsize, eCGDeclare, true, false);
    }
      
    rc = eCGPrecise;

    if (bvOverloaded) {
      // set context flags to indicate overload.
      out << TAG;
      context |= eCGBVOverloadSupported;
      context |= eCGBVExpr;
    }

    if (opEqual)
      context |= eCGOpEqual;      // ask NUBinaryOp::emitCode() or NUUnaryOp::emitCode() to do op=

    if (rsize > LLONG_BIT && lsize <= LLONG_BIT)
      if (lsize <= LONG_BIT) {
        rc = sGenLValue (rhs, 0, context);
        if (lsize == LONG_BIT)
          rc = Precise (rc);
      } else {
        rc = sGenLLValue (rhs, 0, context);
        if (lsize == LLONG_BIT)
          rc = Precise (rc);
      }
    else {
      out << TAG << "(";
      extraParen++;
      rc = rhs->emitCode (context);
    }

    // If the constructed value isn't precise, we need to clear
    // the extra bits to insure that memory remains accurate.
    //
    if (opEqual && !isPrecise (rc) && physicalSize (lsize) == lsize
      && wholeIdentifier)
      // We can ignore any imprecision from the RHS, because the
      // hardware will chop the imprecise MSBs when we write the
      // data.  (consider "byte=byte+255".  This is considered imprecise
      // but the overflow will disappear when we store back to memory.
      ;
    else if (!isPrecise (rc) || lsize < rsize) {
      // must mask off dirty bits here, either via masking
      // rhs, or by masking the just-written destination.
      UInt32 maskValue = std::min (lsize, rsize);
      UInt32 maskSize = std::max ((UInt32)LONG_BIT, maskValue);
      UtString maskOp (opEqual ? "&=" : "&");
      maskOp << CBaseTypeFmt (maskSize, eCGDefine);
      mask (maskValue, maskOp.c_str ());
    }


    while(extraParen--)
      out << ")";
    out << TAG;
  }

 finish:
  // Common exit - generate a dirty-bits test, see if we need trailing semicolons and
  // return the expected context result.
  //
  emitAssert( getLvalue(), context );

  if (not noSemi(context))
    out << ";";

  return eCGVoid;
}

// Looks just like the base-class
//
CGContext_t
NUBlockingAssign::emitCode (CGContext_t context) const
{
  return NUAssign::emitCode (context & ~eCGFlow);
}

CGContext_t
NUNonBlockingAssign::emitCode (CGContext_t context) const
{
  return NUAssign::emitCode (context & ~eCGFlow);
}

/*!
 * Emit code to pull a net.
 *
 * If the net is a primary and the strength is low, only set the
 * strength of the bits not being externally driven.
 *
 * For bidirect and cpp api, emit:
 *   if pull-value is all 1's:
 *     net &= net.getXDrive();
 *     net |= net.getXData();
 *     net |= net.getXDrive();
 *   if pull-value is 0:
 *     net &= net.getXDrive();
 *     net |= net.getXData();
 *     net &= ~net.getXDrive();
 *   otherwise:
 *     net &= net.getXDrive();
 *     net |= net.getXData();
 *     net = (net & ~net.getXDrive())
 *	     | (pull-value & net.getXDrive()); // xdrive is low active
 * For bidirect and struct api, emit:
 *   if pull-value is all 1's:
 *     net |= net.getXDrive();
 *   if pull-value is 0:
 *     net &= ~net.getXDrive();
 *   otherwise:
 *     net = (net & ~net.getXDrive())
 *	     | (pull-value & net.getXDrive()); // xdrive is low active
 * For others, emit:
 *   net = pull-value;
 * Then emit strength code.
 */
static void emitPull(const NULvalue *lvalue,
		     const NUExpr *rvalue,
		     Strength strength,
		     bool inlined,
		     CGContext_t context)
{
  // Emit the correct assign based on net type.
  class emitHelper
  {
  public:
    void operator()(UtOStream& out,
		    const NUNet *net,
		    const NUExpr *rvalue,
		    CGContext_t context)
    {
      if (not isDefine(context)) {
	return;
      }

      bool is_bidirect = gCodeGen->isBidirect(net);
      const NUConst *constant = rvalue->castConst();
      bool is_constant = (rvalue != 0);

      net->emitCode(context|eCGLvalue);
      if (is_bidirect) {
        out << TAG;
	// First copy over the external data
        out << " &= ";
        net->emitCode(context);
        out << ".getXDrive();" << ENDL;
        net->emitCode(context|eCGLvalue);
        out << " |= ";
        net->emitCode(context);
        out << ".getXData();" << ENDL;
        
        // Emit the net for the pulling the undriven bits below
        net->emitCode(context|eCGLvalue);

	// Now pull the undriven bits
	if (is_constant && constant->isOnes()) {
	  out << " |= ";
	  net->emitCode(context);
	  out << ".getXDrive()";
	} else if (is_constant && constant->isZero()) {
	  out << " &= ~";
	  net->emitCode(context);
	  out << ".getXDrive()";
	} else {
	  out << " = (";
	  net->emitCode(context);
	  out << " & ~";
	  net->emitCode(context);
	  out << ".getXDrive()) | (";
	  rvalue->emitCode(context);
	  out << " & ";
	  net->emitCode(context);
	  out << ".getXDrive())";
	}
      } else {
        out << TAG;
	out << " = ";
	rvalue->emitCode(context);
      }
      out << ";" << ENDL;
    }
  };

  UtOStream& out = gCodeGen->CGOUT ();

  // Assert current restrictions/assumptions
  NU_ASSERT(strength == eStrWeakest, lvalue);
  NU_ASSERT(lvalue->isWholeIdentifier(), lvalue);
  NUNet *net = lvalue->getWholeIdentifier();

  // Note that we don't use emitStartBucket() for profiling emitPull.
  // That's because the function generated is so small.

  if (isDeclare(context)) {
    // Code the function inline for real
    if (inlined)
      out << "  {" << ENDL;

    CGContext_t subctxt = (context & ~eCGDeclare) | (inlined ? eCGDefine : eCGVoid); 
    emitHelper helper;
    helper(out, net, rvalue, subctxt);
    emitResetTristate (lvalue, strength, subctxt);

    if (inlined)
      out << "  }" << ENDL;
    else
      out << ";" << ENDL;
  } else {
    out << "  {" << ENDL;

    emitHelper helper;
    helper(out, net, rvalue, context|eCGDefine);
    emitResetTristate (lvalue, strength, context|eCGDefine);
      
    out << "  }" << ENDL;
  }
}


// Continuous assigns are implemented as functions, and are
// scheduled so that they are performed each time a component
// of the right-hand-side expression changes.
//
// We probably should make these inline methods since typically
// the code executed will be very simple compared to the function
// call overhead.
//
// Continuous assigns are also used the re-initialize tristates
// at the beginning of a simulation cycle...

CGContext_t
NUContAssign::emitCode (CGContext_t context) const
{
  context &= ~eCGFlow;
  UtOStream& out = gCodeGen->CGOUT ();

  out << HDLTAG (this);

  if (isNameOnly (context))
    {
      out << TAG << funct(mName);
      return context;
    }

  if (not isScheduled ()) {
    UtString caMsg ("continuous assign");
    return checkDeadCode (getLoc (), context, caMsg);
  }

  // Cost model this function

  NUCost cost;
  bool inlined = emitUtil::isInlineCandidate (this, &cost);

  if (inlined && isDefine (context)) {
    // Must code the function body to find uses, but don't compile
    // in .cxx - just in header.
    out << "#if 0 " << ENDL;

    
  }

  emitUtil::annotateCosts (cost);

  out << "void ";
  classPrefix (context);
  out << TAG << funct(mName);

  // Handle low-strength pull drivers separately
  if (getStrength() == eStrWeakest)
    {
      emitPull(getLvalue(), getRvalue(), getStrength(), inlined, context);
      if (inlined && isDefine (context))
	out << "#endif " << ENDL;
    }
  else if (isDeclare (context))
    {
      // Code the function inline for real
      if (inlined) {
	out << "  {" << ENDL;
        emitStartBucket();
      }
      CGContext_t subctxt = (context & ~eCGDeclare) | (inlined ? eCGDefine : eCGVoid); 
      NUAssign::emitCode (subctxt);
      emitResetTristate (getLvalue(), getStrength(), subctxt);
      if (inlined) {
	out << "\n}" << ENDL;
      } else {
        // Check for function layout
        UtString sectionName;
        if (emitUtil::functionSection (this, &sectionName))
          gCodeGen->emitSection (out, sectionName);
        else
          // not inlined and not in the f_000000.cxx file either.
          gCodeGen->putNonEmptyCxx (gCodeGen->getCurrentModule ());

	out << ";" << ENDL;
      }
    }
  else
    {
      out << "  {" << ENDL;
      emitStartBucket();
      NUAssign::emitCode (context | eCGDefine);

      out << "" << ENDL;
      emitResetTristate (getLvalue(), getStrength(), context|eCGDefine);

      out << "\n  }" << ENDL;

      if (inlined) {
        if (isDefine (context)) {
          out << "#endif\n";
        }
      } else if (not isFunctionLayout (context))
        gCodeGen->putNonEmptyCxx (gCodeGen->getCurrentModule ());
    }

  return eCGVoid;
}

// Set the strength to weak.
// Only do this for bits which are not externally driven.
CGContext_t
NUTriRegInit::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);
  UtOStream& out = gCodeGen->CGOUT ();
  out << HDLTAG (this);

  if (isNameOnly (context))
    {
      out << TAG << funct(mName);
      return context;
    }

  if (!isScheduled ()) {
    UtString trMsg ("tristate initializer for ");
    compose (&trMsg, 0, 0, false);
    return checkDeadCode (getLoc (), context, trMsg);
  }
    
  NUCost cost;
  bool inlined = emitUtil::isInlineCandidate (this, &cost);

  if (inlined && isDefine (context))
    // Must code the function body to find uses, but don't compile
    // in .cxx - just in header.
    out << "#if 0 " << ENDL;
      
  emitUtil::annotateCosts (cost);

  out << "void ";
  classPrefix (context);
  out << TAG << funct(mName);

  if (isDeclare (context))
    {
      // Code the function inline for real
      if (inlined) {
	out << "  {" << ENDL;
        emitStartBucket();
      }
      CGContext_t subctxt = (context & ~eCGDeclare) | (inlined ? eCGDefine : eCGVoid); 
      emitResetTristate (getLvalue(), eStrWeakest, subctxt);
      if (inlined)
	out << "\n  }" << ENDL;
      else {
        UtString sectionName;
        if (emitUtil::functionSection (this, &sectionName))
          gCodeGen->emitSection (out, sectionName);

	out << ";" << ENDL;
      }
    }
  else if (isDefine (context))
    {
      out << "  {" << ENDL;
      emitStartBucket();
      emitResetTristate (getLvalue(), eStrWeakest, context|eCGDefine);

      out << "\n  }" << ENDL;
      if (inlined)
	out << "#endif\n";
      else
        // not inlined and not in the f_000000.cxx file either.
        gCodeGen->putNonEmptyCxx (gCodeGen->getCurrentModule ());

    }

  return eCGVoid;
}

//! Tristate
/*
 * if (<enable>) <Lvalue>.set[|wand|wor](<driver>, <strength>);
 * 
 * Also handle more complex situations arising from
 *
 *	bufif1 a[31:0](out, in, en);
 *
 * where each of in, out and en are 32-bits (or partsels)
 */
CGContext_t
NUEnabledDriver::emitCode (CGContext_t context) const
{
  UtOStream& out = gCodeGen->CGOUT ();

  // Assuming definition context?
  context |= eCGDefine;
  context &= ~eCGDeclare;

  const NUNet *tri = NULL;
  const NULvalue *lhs = getLvalue ();

  const NUVarselLvalue* lhs_ps = NULL;

  SInt32 pos = 0; // silence gcc
  size_t size = 0; // silence gcc

  // Find the net which is being assigned-to, and get the position and range of the
  // bits being driven.
  if (lhs->isWholeIdentifier ())
    {
      tri = lhs->getWholeIdentifier ();
      pos = 0;
      size = tri->getBitSize ();
    }
  else if ((lhs_ps = dynamic_cast<const NUVarselLvalue *>(lhs)))
    {
      tri = emitUtil::sFindNet (lhs_ps);

      const ConstantRange& base(*getNUrange (tri));
      ConstantRange range(*lhs_ps->getRange ());

      pos = ComputeIndex (range.getLsb (), base);
      size = lhs_ps->getBitSize ();
    }
  else
    NU_ASSERT (!"Malformed NUEnableDriver", this);

  size_t whole_size = tri->getBitSize();

  // If this variable is not a tristate, only generate the assign,
  // don't set the strengths.
  bool is_tristate = gCodeGen->isTristate(tri);

  CopyContext copy_context(NULL,NULL);
  
  // If the enable is a multi-bit value, it's a mask of the
  // bits that are driven (1's) or high-impedence (0's)
  bool isMasked = (getEnable ()->getBitSize () > 1);

  // For wide enables, we generate masking code so that we only change
  // the bits which are enabled.  For single bit enables, I assume it
  // is cheaper to generate an if stmt.
  if (is_tristate && lhs->isWholeIdentifier ()
      && whole_size > 1
      && (whole_size == getEnable ()->getBitSize ())) {
      // It's a tristate or bidirect, we're writing the whole net and it's
      // a multi-bit object; let the class do the masking and
      // strength computations.
      out << TAG;
      lhs->emitCode (context|eCGLvalue);
      out << ".set(";
      getDriver ()->emitCode (context);
      out << ", ";
      getEnable ()->emitCode (context);
      out << ");" << ENDL;
  } else {
    // special cases of varying complexity.  Non-bidi/tristate nets, or
    // partselects and bitselects.  

    if (isMasked) {
      // emit:
      //  out = (~en & out) | (en & drive);
      //  out.setStrength(en);
      NUUnaryOp *neg_enable = new NUUnaryOp(NUOp::eUnBitNeg,
					    getEnable()->copy(copy_context),
					    getLoc());
      NUExpr *value = getLvalue()->NURvalue();
      NUBinaryOp *mask_value = new NUBinaryOp(NUOp::eBiBitAnd,
					      neg_enable,
					      value,
					      getLoc());
      NUBinaryOp *mask_drive = new NUBinaryOp(NUOp::eBiBitAnd,
					      getEnable()->copy(copy_context),
					      getDriver()->copy(copy_context),
					      getLoc());
      NUBinaryOp *or_result = new NUBinaryOp(NUOp::eBiBitOr,
					     mask_value,
					     mask_drive,
					     getLoc());
      NUBlockingAssign *assign = new NUBlockingAssign(getLvalue()->copy(copy_context),
						      or_result,
                                                      false,
						      getLoc());

      // The masks or values MIGHT be constant-foldable into simpler expressions
      if (NUStmt* stmt = gCodeGen->getFold ()->stmt (assign)) {
        stmt->emitCode(context | eCGStrengthSet);
        delete stmt;
      }
      out << ENDL;

      if (is_tristate) {
	if (lhs_ps == NULL) {
	  // the lhs was a whole identifier
	  tri->emitCode (context|eCGLvalue);
	} else {
	  lhs_ps->getLvalue ()->emitCode (context|eCGLvalue);
	}
	out << ".setStrength(";
        if (whole_size > LLONG_BIT)
          out << CBaseTypeFmt (whole_size, eCGDeclare);
        out << "(";
        getEnable ()->emitCode (context);
        out << ")" << TAG;
        
        if (lhs_ps) {
          if (lhs_ps->isConstIndex ()) {
            out << ", " << pos << ", " << size << TAG;
          } else {
            emitIndex (out, lhs_ps->getIndex (), pos, context, *lhs_ps->getRange ());
            out << TAG;
          }
	} else {
	  if (pos > 0) {
            // this next line of code dead is dead, says Coverity, because
            // 'pos' is initialized to 0 and only set to something else
            // if lhs_ps != 0.
	    out << ", " << pos << ", " << size << TAG;
	  }
	}
	out << ");" << ENDL;
      }

    } else {
      // Single bit enable to drive some number of bits...
      // emit:
      //  if (en) {
      //   out = drive;
      //   out.setStrength(pos,size)
      //  }
      if (NUConst* c = getEnable ()->castConst ()) {
        if (c->isZero ())
          return eCGVoid;       // exable is zero - quit early

        // unconditionally driven
        out << "  {" << ENDL;

      } else {
        // non-constant - evaluate
        out << "if (";
        if(! isPrecise( getEnable()->emitCode(context | eCGFlow) ) )
          mask (getEnable()->getBitSize(), "&");
        out << ") {" << ENDL;
      }

      NUStmt *assign = new NUBlockingAssign(getLvalue()->copy(copy_context),
                                            getDriver()->copy(copy_context),
                                            false,
                                            getLoc());
      assign = gCodeGen->getFold ()->stmt (assign);
      if (assign) {
        assign->emitCode (context | eCGStrengthSet);
        delete assign;
      }

      out <<"" << ENDL;

      if (is_tristate) {
	tri->emitCode (context|eCGLvalue);
	out << ".setStrength(";

        if (lhs_ps && not lhs_ps->isConstIndex ()) {
          emitIndex (out, lhs_ps->getIndex (), pos, context, *lhs_ps->getRange ());
          out << TAG;
        } else {
          out << pos << TAG;
        }

        out << "," << size << ");" << ENDL;
      }
      out << "}" << ENDL;
    }
  }

  return eCGVoid;
} // NUEnabledDriver::emitCode

CGContext_t
NUContEnabledDriver::emitCode (CGContext_t icontext) const
{
  icontext &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream& out = gCodeGen->CGOUT ();

  CGContext_t context = icontext;

  out << HDLTAG (this);

  if (isNameOnly (context))
    {
      out << TAG << funct (getName ());
      return context;
    }

  NUCost cost;
  bool inlined = emitUtil::isInlineCandidate (this, &cost);

  if (inlined && isDefine (context))
    // suppress in .cxx
    out << "#if 0 " << ENDL;

  emitUtil::annotateCosts (cost);

  out << "void ";
  classPrefix (context);
  out << TAG << funct (getName ());

  if (isDeclare (context) && not inlined) {
    UtString sectionName;
    if (emitUtil::functionSection (this, &sectionName))
      gCodeGen->emitSection (out, sectionName);
    out << ";" << ENDL;
    return eCGVoid;
  } else {
    out << "  {" << ENDL;
    emitStartBucket();
  }

  NUEnabledDriver::emitCode(context);

  // End the function body
  out << "}" << ENDL;

  if (inlined && isDefine (icontext))
    out << "#endif\n";

  return eCGVoid;
}

CGContext_t
NUBlockingEnabledDriver::emitCode (CGContext_t context) const
{
  return NUEnabledDriver::emitCode(context);
}
