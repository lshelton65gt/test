// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "codegen/codegen.h"
#include "codegen/CGPortIface.h"
#include "codegen/CGAuxInfo.h"

#include "util/Util.h"
#include "util/Loop.h"
#include "util/LoopFilter.h"
#include "util/LoopFunctor.h"
#include "util/UtVector.h"
#include "util/UtHashMap.h"
#include "util/UtIOStream.h"
#include "hdl/HdlVerilogPath.h"
#include "iodb/IODBNucleus.h"
#include "emitUtil.h"

using namespace emitUtil;

extern CodeGen* gCodeGen;


CGPortIfaceNet::CGPortIfaceNet(NUNetElab* netElab, const STAliasedLeafNode* node, bool useElabName, Flag extraFlags):
  mNetElab(netElab), mSymNode(node), mNet(netElab->getNet()), mExpr(NULL), mFlags(extraFlags), mChangeIndex(-1)
{
  init(node, useElabName);
}

CGPortIfaceNet::CGPortIfaceNet(const STSymbolTableNode* node, const NUNet* net, bool useElabName, Flag extraFlags):
  mNetElab(NULL), mSymNode(node), mNet(net), mExpr(NULL), mFlags(extraFlags), mChangeIndex(-1)
{
  NU_ASSERT(mNet, mNet);
  init(node->castLeaf(), useElabName);
}

void CGPortIfaceNet::init(const STAliasedLeafNode* node, bool useElabName)
{
  bool isObfuscating = gCodeGen->isObfuscating();
  if (isObfuscating)
    gCodeGen->putInterfaceMode();
  
  // Just going to use the HdlVerilogPath thingy for now. This may
  // change for hdl-specific stuff. But, I'm not sure.
  HdlVerilogPath pather;
  pather.compPathHier(node, &mFullName, NULL);
  
  const StringAtom* strName = node->strObject();

  const STBranchNode* branch = node->getParent();
  if (useElabName)
  {
    // We are representing an internal signal, so the member name needs to
    // include the full hierarchical path to avoid conflicts.
    mMemberName = "m_";
    gCodeGen->CxxHierPath(&mMemberName, branch, "_", NULL, false, false);
    if (mMemberName.length() > 2)
      mMemberName << "_";
    mMemberName << emitUtil::noprefix(strName);
    
    gCodeGen->CxxHierPath(&mGenerateName, branch, "_", NULL, false, false);
    if (mGenerateName.length() > 0)
      mGenerateName << "_";
    mGenerateName << emitUtil::noprefix(strName);
  }
  else
  {
    // We are representing a simple port, so the full hierarchy is not needed
    // in the names of the struct members.
    
    mMemberName << emitUtil::member(strName);
    mGenerateName << emitUtil::noprefix(strName);
    
  }
  
  if (isObfuscating)
    gCodeGen->putInternalMode();
}

UInt32 CGPortIfaceNet::getWidth() const
{
  return mNet->getBitSize();
}

StringAtom* CGPortIfaceNet::getNUNetName() const
{
  const NUNet* net = getNUNet();
  return net->getName();
}

const NUNet* CGPortIfaceNet::getNUNet() const
{
  return mNet;
}

bool CGPortIfaceNet::isPulled() const
{
  const NUNet* net = getNUNet();
  return net->isPulled();
}

struct CGPortIface::Helper
{
  typedef UtVector<CGPortIfaceNet*> NetVec;
  typedef Loop<NetVec> NetVecLoop;
  typedef UtHashMap<const STSymbolTableNode*, CGPortIfaceNet*> NodeNetMap;

  struct GetCGPortIfaceNet
  {
    typedef CGPortIfaceNet* value_type;
    typedef CGPortIfaceNet* reference;

    value_type operator()(NodeNetMap::NameValue& nameValue) const
    {
      return nameValue.second;
    }
  };

  struct InNetSelect
  {
    bool operator()(CGPortIfaceNet* net) const
    {
      return net->isInput() && ! net->isDead();
    }
  };

  struct InDirNetSelect
  {
    bool operator()(CGPortIfaceNet* net) const
    {
      return net->hasInputDir();
    }
  };

  struct OutDirNetSelect
  {
    bool operator()(CGPortIfaceNet* net) const
    {
      return net->hasOutputDir();
    }
  };

  struct OutNetSelect
  {
    bool operator()(CGPortIfaceNet* net) const
    {
      return net->isOutput();
    }
  };
  
  struct BidNetSelect
  {
    bool operator()(CGPortIfaceNet* net) const
    {
      return net->isBid();
    }
  };

  struct TriOutNetSelect
  {
    bool operator()(CGPortIfaceNet* net) const
    {
      return net->isTriOut();
    }
  };

  struct DeadInNetSelect
  {
    bool operator()(CGPortIfaceNet* net) const
    {
      return net->isInput() && net->isDead();
    }
  };

  struct NonPortNetSelect
  {
    bool operator()(CGPortIfaceNet* net) const
    {
      return net->isNotAPort();
    }
  };
  
  
  typedef LoopFunctor<NodeNetMap::SortedLoop, GetCGPortIfaceNet> NodeNetMapNetFunctor;
  typedef LoopFilter<NodeNetMapNetFunctor, InNetSelect> InNetFilter;
  typedef LoopFilter<NodeNetMapNetFunctor, OutNetSelect> OutNetFilter;
  typedef LoopFilter<NodeNetMapNetFunctor, BidNetSelect> BidNetFilter;
  typedef LoopFilter<NodeNetMapNetFunctor, TriOutNetSelect> TriOutNetFilter;
  typedef LoopFilter<NodeNetMapNetFunctor, DeadInNetSelect> DeadInNetFilter;
  
  typedef LoopFilter<NodeNetMapNetFunctor, InDirNetSelect> InDirNetFilter;
  typedef LoopFilter<NodeNetMapNetFunctor, OutDirNetSelect> OutDirNetFilter;
  typedef LoopFilter<NodeNetMapNetFunctor, NonPortNetSelect> NonPortNetFilter;


  ~Helper()
  {
    mNodeNetMap.clearPointerValues();
  }


  CGPortIfaceNet* findNet(const STSymbolTableNode* node)
  {
    CGPortIfaceNet* ret = NULL;
    NodeNetMap::iterator p = mNodeNetMap.find(node);
    if (p != mNodeNetMap.end())
      ret = p->second;
    return ret;
  }

  void addCommonFlags(CGPortIfaceNet* net)
  {
    NUNetElab* elabNet = net->getNetElab();
    if (netIsDeclaredForcible(elabNet->getStorageNet()))
      net->addFlag(CGPortIfaceNet::eForcible);
    
    NUNet* unelabNet = elabNet->getNet();
    
    if (! unelabNet->isUnsignedSubrange() && unelabNet->isSigned())
      net->addFlag(CGPortIfaceNet::eSigned);
    
    if (unelabNet->isReal())
      net->addFlag(CGPortIfaceNet::eReal);
    
    if (elabNet->isComboRun())
      net->addFlag(CGPortIfaceNet::eComboRunNet);
  }
  

  CGPortIfaceNet* createLiveNet(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags)
  {
    CGPortIfaceNet* ifaceNet = new CGPortIfaceNet(net, node, useElabName, extraFlags);
    addCommonFlags(ifaceNet);
    mNodeNetMap[node] = ifaceNet;
    return ifaceNet;
  }

  CGPortIfaceNet* createDeadNet(const STSymbolTableNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags)
  {
    CGPortIfaceNet* ifaceNet = new CGPortIfaceNet(node, NUNet::find(node), useElabName, extraFlags);
    mNodeNetMap[node] = ifaceNet;
    return ifaceNet;
  }

  CGPortIfaceNet* addIn(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags)
  {
    CGPortIfaceNet* netWrap = findNet(node);
    if (netWrap == NULL)
      netWrap = createLiveNet(net, node, useElabName, extraFlags);
    
    netWrap->addFlag(CGPortIfaceNet::eIn);
    return netWrap;
  }
  
  CGPortIfaceNet* addOut(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags)
  {
    CGPortIfaceNet* netWrap = findNet(node);
    if (netWrap == NULL)
      netWrap = createLiveNet(net, node, useElabName, extraFlags);
    
    netWrap->addFlag(CGPortIfaceNet::eOut);
    return netWrap;
  }
  
  CGPortIfaceNet* addDeadIn(const STSymbolTableNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags)
  {
    CGPortIfaceNet* netWrap = findNet(node);
    if (netWrap == NULL)
      netWrap = createDeadNet(node, useElabName, extraFlags);

    netWrap->addFlag(CGPortIfaceNet::eIn);
    return netWrap;
  }
  
  CGPortIfaceNet* addTriOut(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags)
  {
    CGPortIfaceNet* netWrap = findNet(node);
    if (netWrap == NULL)
      netWrap = createLiveNet(net, node, useElabName, extraFlags);
    
    netWrap->addFlag(CGPortIfaceNet::eTri);
    netWrap->addFlag(CGPortIfaceNet::eOut);
    return netWrap;
  }
  
  CGPortIfaceNet* addBidi(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags)
  {
    CGPortIfaceNet* netWrap = findNet(node);
    if (netWrap == NULL)
      netWrap = createLiveNet(net, node, useElabName, extraFlags);
    
    netWrap->addFlag(CGPortIfaceNet::eBid);
    return netWrap;
  }

  NetVec mPrimaryPorts;
  NodeNetMap mNodeNetMap;
};

CGPortIface::CGPortIface()
{
  mHelper = new Helper;
}

CGPortIface::~CGPortIface()
{
  delete mHelper;
}

CGPortIface::NetIter 
CGPortIface::loopInputDir()
{
  return NetIter::create(Helper::InDirNetFilter(mHelper->mNodeNetMap.loopSorted()));
}

CGPortIface::NetIter 
CGPortIface::loopOutputDir()
{
  return NetIter::create(Helper::OutDirNetFilter(mHelper->mNodeNetMap.loopSorted()));
}

CGPortIface::NetIter 
CGPortIface::loopNonPorts()
{
  return NetIter::create(Helper::NonPortNetFilter(mHelper->mNodeNetMap.loopSorted()));
}

CGPortIface::NetIter 
CGPortIface::loopIns()
{
  return NetIter::create(Helper::InNetFilter(mHelper->mNodeNetMap.loopSorted()));
}

CGPortIface::NetIter 
CGPortIface::loopDeadIns()
{
  return NetIter::create(Helper::DeadInNetFilter(mHelper->mNodeNetMap.loopSorted()));
}

CGPortIface::NetIter 
CGPortIface::loopOuts()
{
  return NetIter::create(Helper::OutNetFilter(mHelper->mNodeNetMap.loopSorted()));
}

CGPortIface::NetIter 
CGPortIface::loopTriOuts()
{
  return NetIter::create(Helper::TriOutNetFilter(mHelper->mNodeNetMap.loopSorted()));
}

CGPortIface::NetIter 
CGPortIface::loopBidis()
{
  return NetIter::create(Helper::BidNetFilter(mHelper->mNodeNetMap.loopSorted()));
}

CGPortIface::NetIter 
CGPortIface::loopPrimaryPorts(IODBNucleus *db)
{
  if (mHelper->mPrimaryPorts.size() == 0) {
    // If the vector of primary ports has not been initialized,
    // set it up now.
    for (IODB::NameVecLoop loop = db->loopPrimaryPorts(); !loop.atEnd(); ++loop) {
      Helper::NodeNetMap::iterator p = mHelper->mNodeNetMap.find(*loop);
      // may be a multi-dimensional array. We do not handle those yet.
      if (p != mHelper->mNodeNetMap.end()) {
        CGPortIfaceNet *net = p->second;
        mHelper->mPrimaryPorts.push_back(net);
      }
    }
  }
  return NetIter::create(Helper::NetVecLoop(mHelper->mPrimaryPorts));
}

CGPortIfaceNet*
CGPortIface::addIn(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags)
{
  return mHelper->addIn(net, node, useElabName, extraFlags);
}

CGPortIfaceNet* CGPortIface::addDeadIn(const STSymbolTableNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags)
{
  return mHelper->addDeadIn(node, useElabName, extraFlags);
}


CGPortIfaceNet*
CGPortIface::addOut(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags)
{
  return mHelper->addOut(net, node, useElabName, extraFlags);
}

CGPortIfaceNet*
CGPortIface::addTriOut(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags)
{
  return mHelper->addTriOut(net, node, useElabName, extraFlags);
}

CGPortIfaceNet*
CGPortIface::addBidi(NUNetElab* net, const STAliasedLeafNode* node, bool useElabName, CGPortIfaceNet::Flag extraFlags)
{
  return mHelper->addBidi(net, node, useElabName, extraFlags);
}

const char* CGPortIface::getChangeArray() const
{
  return "mChangeArray";
}

const char* CGPortIface::getRunComboPtr() const
{
  return "mRunCombo";
}

const char* CGPortIface::getExternalValue(const CGPortIfaceNet* net, UtString* buf)
{
  buf->clear();
  const char* _xdata = "_xdata";
  *buf = net->getMemberName();
  if (net->isBid() || net->isTri())
    *buf << _xdata;
  return buf->c_str();
}

const char* CGPortIface::getInternalValue(const CGPortIfaceNet* net, UtString* buf)
{
  buf->clear();
  const char* _idata = "_idata";
  NU_ASSERT(net->isBid(), net->getNUNet ());
  *buf = net->getMemberName();
  *buf << _idata;
  return buf->c_str();
}

const char* CGPortIface::getExternalDrive(const CGPortIfaceNet* net, UtString* buf)
{
  buf->clear();
  const char* _xdrive = "_xdrive";
  NU_ASSERT(net->isBid(), net->getNUNet ());
  *buf = net->getMemberName();
  *buf << _xdrive;
  return buf->c_str();
}

const char* CGPortIface::getInternalDrive(const CGPortIfaceNet* net, UtString* buf)
{
  buf->clear();
  const char* _idrive = "_idrive";
  NU_ASSERT(net->isBid() || net->isTri(), net->getNUNet ());
  *buf = net->getMemberName();
  *buf << _idrive;
  return buf->c_str();
}

const char* CGPortIface::getForceMask(const CGPortIfaceNet* net, UtString* buf)
{
  buf->clear();
  const char* _forceMask = "_forceMask";
  NU_ASSERT(net->isForcible(), net->getNUNet ());
  *buf = net->getMemberName();
  *buf << _forceMask;
  return buf->c_str();
}

const char* CGPortIface::getCType(const CGPortIfaceNet* net, UtString* buf) const
{
  const NUNet* unelabNet = net->getNetElab()->getNet();
  buf->clear();
  return getIfaceType(unelabNet, buf);
}

const char* CGPortIface::getIfaceType(const NUNet* net,  UtString* buf) const
{
  size_t nbits = net->getBitSize();
  if (nbits > LLONG_BIT)
    (*buf) << "CarbonUInt32";
  else
    (*buf) << emitUtil::CBaseTypeFmt (nbits, eCGIface);
  return buf->c_str();
}

bool CGPortIface::sIsNetHandle(const CGPortIfaceNet* ifaceNet)
{
  // signed nets are not supported in the port interface because they
  // require sign extension.
  return ifaceNet->isSigned() || ifaceNet->isExpressioned() || ifaceNet->isForcible() 
    || (ifaceNet->isTri() && ifaceNet->isDeposit());
}
