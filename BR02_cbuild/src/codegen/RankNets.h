// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 
#ifndef RANKNETS_H_
#define RANKNETS_H_

/*!
  \file
  Declare unelaborated ranking of nets based on elaborated scheduling information.
*/

#include "nucleus/Nucleus.h"

class SCHSchedule;

//! Class to order member variables by a number of techniques.
/*!
 * This class provides an interface to codegen which determines the
 * placement of member variables in the generated source. The
 * different sorting techniques place priorities on different
 * attributes of each net: the number of uses, order of encounter,
 * required padding, etc.
 *
 * The default ranking technique implements our traditional codegen
 * variable ordering: members are ordered by size in an attempt to
 * reduce the size of the model. Locality is not considered.
 */
class RankNets
{
public:
  //! Constructor
  /*!
   * \param locality_technique Select from one of a number of ordering
   *        techniques. Valid values are:
   * 
   *        0 -- Default; Order all members by size to reduce padding.
   *
   *        1 -- Rank assigned based on the order of scheduled blocks;
   *        the scheduling position of the last defining block is used
   *        as rank. All nets in a block are assigned the same rank
   *        (unless referred to by some later block).
   *
   *        2 -- Rank based on the number of uses; the rank of a given
   *        net is increased each time it is used by a scheduled
   *        block. Uses are computed on a per-block basis -- multiple
   *        uses within a block only increase the rank by one.
   *
   *        3 -- Rank based on the number of blocks which use but do
   *        not define a particular net. Technique '2' took all uses
   *        into account, while this considers only non-def uses.
   *        Otherwise, the two approaches are the same.
   *
   *        4 -- Rank assigned based on the order of scheduled blocks;
   *        the scheduling position of the first defining block is
   *        used as rank. All nets defined in a block are assigned the
   *        same rank (unless already encountered).
   *
   *        5 -- Rank assigned based on the order of scheduled blocks
   *        and position within that scheduled block; the scheduling
   *        position of the first defining block and position within
   *        that block is used as rank. All nets defined by the same
   *        statement contained within the scheduled block are
   *        assigned the same rank (unless already encountered).
   *
   *        Techniques 1-5 process each unelaborated block once in
   *        scheduled order. Technique 0 does not consider scheduled
   *        order.
   *
   *        Note: These techniques may change as we perform more
   *        experiments.
   *
   * \param verbose Should we dump debugging information?
   */
  RankNets(UInt32 locality_technique,
           bool ondemand,
           bool verbose);

  ~RankNets();

  //! Process all scheduled blocks and assign rank to all unelaborated nets.
  /*!
   * With a locality technique of '0', this function is a nop. For
   * other techniques, each scheduled NUUseDefNode is processed in the
   * order it appears in the schedule.
   *
   * \param schedule The schedule object; allows iteration over all
   * scheduled constructs.
   */
  void computeRank(SCHSchedule * schedule);

  //! Order a net vector based on computed rank.
  /*!
   * With a locality technique of '0', order nets by data size.
   * Otherwise, apply the ranking computed by 'computeRank'. Nets with
   * the same rank are subsequently sorted by data size.
   *
   * \param nets Set of nets for ordering; modified in-place.
   * \param module The module containing these nets (used for debug traces).
   */
  void orderByRank(const NUModule * module, NUNetCVector * nets) const;
private:
  //! Hide copy and assign constructors.
  RankNets(const RankNets&);
  RankNets& operator=(const RankNets&);

  //! Compute ranking for nets, taking NUCycle nodes into account.
  /*!
   * \sa rankDefinedNets, rankDefinedNetsAlways
   */
  void rankDefinedNetsCycle(const NUUseDefNode * node,
                            NUCUseDefSet * seen,
                            SInt32 & order);

  //! Compute ranking for nets, allowing a technique to loop the stmts of always blocks.
  /*!
   * \sa rankDefinedNets, rankDefinedNetsCycle
   */
  void rankDefinedNetsAlways(const NUUseDefNode * node,
                             NUCUseDefSet * seen,
                             SInt32 & order);

  //! Compute ranking for nets referenced by a NUUseDefNode
  /*!
   * Based on locality technique, determine the ordering for each net
   * referenced by this NUUseDefNode. Some techniques affect defined
   * nets, others uses.
   *
   * Each unelaborated NUUseDefNode is processed once.
   *
   * \param node  The current NUUseDefNode.
   * \param seen  Set of encountered NUUseDefNode objects.
   * \param order The scheduled order of the previous NUUseDefNode.
   * Updated whenever a new NUUseDefNode is encountered.
   *
   * \sa rankDefinedNetsCycle, rankDefinedNetsAlways
   */
  void rankDefinedNets(const NUUseDefNode * node,
                       NUCUseDefSet * seen,
                       SInt32 & order);

  //! Return the rank for a given net.
  /*!
   * \param net The net needing ranking.
   * \return the computed rank for the given net. If a rank has not
   * been computed for this net, use '0' for ports and MAXINT
   * otherwise.
   */
  SInt32 getRank(const NUNet * net) const;

  //! Populate a net set with defined nets.
  /*!
   * Use node::getDefs() if available, node::getBlockingDefs(),
   * otherwise. Statements within an always block provide
   * ::getBlockingDefs(), while continuous drivers provide
   * ::getDefs().  node::useBlockingMethods() tells you which to use.
   */
  void getDefs(const NUUseDefNode * node,
               NUNetSet * nets) const;

  //! Print debugging header (module name).
  void printRankHeader(const NUModule * module) const;

  //! Print nets in a per-rank manner.
  void printRankNets(SInt32 rank, NUNetCVector * group) const;

  //! Map from net to its ranking.
  typedef UtMap<const NUNet*,SInt32> NetOrder;

  //! Saved rank information.
  NetOrder mNetOrder;

  //! What ranking technique are we using?
  UInt32 mLocalityTechnique;

  //! Are we creating an onDemand Carbon Model?
  bool mOnDemand;

  //! Are we dumping debugging information?
  bool mVerbose;
};

#endif
