# -*- Makefile -*-
#
# *****************************************************************************
# Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
# *****************************************************************************
#
# compiler-dependent makefile fragment

# Define where compiler can be found
ifndef CARBON_GCC4_VERSION
  CARBON_GCC4_VERSION := 422
endif
CARBON_COMPILER_DIST:=${CARBON_HOME}/${CARBON_TARGET_ARCH}/gcc${CARBON_GCC4_VERSION}


# Where gcc wants to look for libraries
CARBON_GCC_LIB_Linux := lib
CARBON_GCC_LIB_Linux64 := lib64
CARBON_GCC_LIB_Host := $(CARBON_GCC_LIB_$(CARBON_HOST_ARCH))
CARBON_GCC_LIB_Target := $(CARBON_GCC_LIB_$(CARBON_TARGET_ARCH))

CARBON_COMPILER_LIB_DIRS := $(CARBON_HOME)/$(CARBON_TARGET_ARCH)/gcc4/$(CARBON_GCC_LIB_Target)

# C++ runtime libraries needed to export a stand-alone .a file
#
CARBON_GCCVER:=$(shell $(CARBON_COMPILER_DIST)/bin/g++ -dumpversion)

CARBON_COMPILER_SUPPORTS_PCH:=1

# Where gcc wants to look for executables
CARBON_GCC_BIN_Linux := i686-pc-linux-gnu
CARBON_GCC_BIN_Linux64 := x686_64-unknown-linux-gnu
CARBON_GCC_CONFIG := $(CARBON_GCC_BIN_$(CARBON_TARGET_ARCH))

# We could potentially need to run gcc compiled for the target architecture
# and speedcc compiled for the host architecture, so we'd need both libraries.
CARBON_LIBRARY_PATH := $(CARBON_HOME)/$(CARBON_HOST_ARCH)/gcc/$(CARBON_GCC_LIB_Host)
ifneq ($(CARBON_HOST_ARCH),$(CARBON_TARGET_ARCH))
# If host doesn't match target, append target libraries.
# (Can't use += because that adds a space.)
  CARBON_LIBRARY_PATH := $(CARBON_LIBRARY_PATH):$(CARBON_HOME)/$(CARBON_TARGET_ARCH)/gcc/$(CARBON_GCC_LIB_Target)
endif

# Use "env -i" to prevent user's environment from causing problems.
# We set COMPILER_PATH to find our copies of tools like `as' and `ld'.
CARBON_COMPILER_ENV = env -i LANG=C \
    COMPILER_PATH=${CARBON_HOME}/${CARBON_TARGET_ARCH}/bin \
    HOME=$(HOME) LD_LIBRARY_PATH=$(CARBON_LIBRARY_PATH)


CARBON_CBUILD_CC  = $(CARBON_COMPILER_DIST)/bin/gcc
CARBON_CBUILD_CXX = $(CARBON_COMPILER_DIST)/bin/g++
CARBON_CBUILD_LD  = $(CARBON_CBUILD_CXX)

# Indicate we're gcc 4 or better
CARBON_GCC4=1

##
# Search all passed in options for -O<level> flags
CARBON_CXX_OPTLIST:=$(filter -O%,$(CXX_EXTRA_FLAGS) $(CXX_WC_FLAGS))

# Find the last one specified
ifeq (,$(CARBON_CXX_OPTLIST))
CARBON_CXX_OPT := -O2
else
CARBON_CXX_OPT:=$(word $(words $(CARBON_CXX_OPTLIST)),$(CARBON_CXX_OPTLIST))
endif

ifneq (,$(findstring -pg,$(CXX_EXTRA_FLAGS) $(CXX_WC_FLAGS)))
CARBON_PROFILING=1
endif



CARBON_OPTFLAGS = --param large-function-growth=250
# GCC BUG26197 and/or BUG27474
ifneq (4.2,$(findstring 4.2,$(CARBON_GCCVER)))
CARBON_OPTFLAGS += -ftree-vectorize
endif

ifneq (,$(findstring $(CARBON_CXX_OPT),-O2 -O3))
  ifeq ($(CARBON_TARGET_ARCH),Linux)
    CARBON_OPTFLAGS += -funroll-loops -march=pentium4 -msse -minline-all-stringops
  endif

  ifeq ($(CARBON_TARGET_ARCH),Linux64)
    CARBON_OPTFLAGS += -funroll-loops
  endif

  # options useful when you want to profile with gprof
  ifeq ($(CARBON_PROFILING),1)
    CARBON_OPTFLAGS += -finline-limit=2000
  else
    # 4.2 snapshot 20060715 crashes with -ftracer on a neteffect test.
    CARBON_OPTFLAGS += -fvisibility-inlines-hidden \
	        -finline-functions -finline-limit=2000

  endif
else				# -Os or -O1 or -O0
  ifneq (,$(findstring $(CARBON_CXX_OPT),-Os -O1))
    ifeq ($(CARBON_TARGET_ARCH),Linux)
      CARBON_OPTFLAGS += -march=pentium4 -msse -minline-all-stringops
    endif
  else				# -O0
  endif
endif


# Basic compiler-dependent flags
#
CARBON_CXX_FLAGS_Linux = -m32
CARBON_CXX_FLAGS_Linux64 = -m64 -D__LP64__

# Define magic values that turn C++ exceptions off/on
CARBON_EXCEPTIONS_OFF:=-fno-exceptions
CARBON_EXCEPTIONS_ON :=-fexceptions

CARBON_CXX_FLAGS = $(CARBON_EXCEPTIONS_OFF) $(CARBON_CXX_FLAGS_$(CARBON_TARGET_ARCH))
CARBON_CXX_DEF_COMPILE_FLAGS= $(CARBON_CXX_FLAGS)

ifeq ($(CARBON_PROFILING),1)
else
ifeq ($(GCC_NO_WALL),)

# strict-aliasing warnings triggered by our static bitvector constants...
# uninitialized triggered by DW protected uninitialized struct.
# parentheses warnings are to clarify operator precedence where it's error-prone - not a functional error
CARBON_CXX_SPEEDCC_COMPILE_FLAGS += -Wall -Wextra -Werror -Wno-strict-aliasing -Wno-uninitialized -Wno-parentheses
endif
endif

CARBON_DISABLE_DYNAMIC_CAST = -fno-rtti
CARBON_ENABLE_DYNAMIC_CAST = -frtti

CARBON_CXX_SPEEDCC_COMPILE_FLAGS += -pipe $(CARBON_DISABLE_DYNAMIC_CAST) $(CXX_WC_FLAGS)

# gcc4 produces bad code in some structure-intensive corner cases if
# we don't use -fno-strict-aliasing.
# test/cust/s3/columbia/carbon/cwtb/PS_EU fails without -fno-tree-vrp
CARBON_CXX_SPEEDCC_COMPILE_FLAGS += -fno-strict-aliasing -fno-tree-vrp

# flags needed to compile shared libraries
CARBON_SHARED_COMPILE_FLAGS= -shared -fPIC
# flags needed to link shared lib.  Note that 'LIBPATH' is defined in Makefile.mc
CARBON_SHARED_LINK_FLAGS=-Wl,-rpath,$(@D)

ifeq (,$(findstring $(CARBON_COMPILER_DIST),$(LD_LIBRARY_PATH)))
  LD_LIBRARY_PATH := $(LD_LIBRARY_PATH):$(CARBON_COMPILER_LIB_DIRS)
  export LD_LIBRARY_PATH
endif

CARBON_COMPILER_LIB_LIST:=


