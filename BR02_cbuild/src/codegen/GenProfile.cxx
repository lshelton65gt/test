// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "GenProfile.h"
#include "emitUtil.h"
using namespace emitUtil;


/*!\file
 * Maintain mapping between profiling bucket IDs and names.
 */

int GenProfile::getBucketId(const UtString &bucket_name)
{
  StringIntMap::iterator iter = mBucketMap.find(bucket_name);
  if (iter == mBucketMap.end()) {
    int ret = mNumBuckets++;
    mBucketMap[bucket_name] = ret;
    mBucketVector.push_back(bucket_name);
    return ret;
  } else {
    return (*iter).second;
  }
}


int GenProfile::getBucketId(const char *bucket_name)
{
  UtString name(bucket_name);
  return getBucketId(name);
}

const UtString& GenProfile::getBucketName(int bucket_id)
{
  return mBucketVector[bucket_id];
}
