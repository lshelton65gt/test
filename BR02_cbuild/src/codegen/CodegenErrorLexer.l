%{
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/* \file CodegenErrorLexer.l
 *
 * This is the definition for a scanner that processes libdesign.codegen.errors.
 *
 * Two environment variable turn on tracing for debugging the lexer. Set
 * CARBON_ERRORS_FLEX_DEBUG=1 to set yyflex_debug for this scanner. This causes
 * flex to trace the rules that are triggered while scanning. Set
 * CARBON_ERRORS_DEBUG to the callbacks up from the lexer.
 */

#include <stdarg.h>
#include "util/CarbonTypes.h"
#include "util/CarbonAssert.h"
#include "util/CbuildMsgContext.h"
#include "util/OSWrapper.h"
#include "util/UtStringUtil.h"

#include "codegen/CodegenErrorLexer.h"

#include "util/UtIOStream.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// Pass a handle onto the CodegenErrorLexer object to yylex so that it can
// call the callback methods.
#define YY_DECL int yylex (CodegenErrorLexer *callback)

//! \struct ErrorContext
/*! Rules acrete filename and linenumber to the single ErrorContext object.
 */
class ErrorContext {
public:

  ErrorContext () : mFilename (NULL), mLineno (0) {}

  //! Clear the filename and linenumber severity
  void clear ()
  {
    if (mFilename != NULL) {
      StringUtil::free (mFilename);
      mFilename = NULL;
    }
    mLineno = 0;
  }

  //! Set the currently matched filename
  void setFilename (const char *s)
  {
     mFilename = StringUtil::dup (s);
     int length = strlen (mFilename);
     while (length > 0 && (isspace (mFilename [length-1]) || mFilename [length-1] == ':')) {
       mFilename [--length] = '\0';
     }
  }

  //! Set the currently matched lineno
  void setLineno (const int n) { mLineno = n; }

  //! Set the currently matched lineno from a string.
  /*! \note this method grabs the number part of " *[0-9]*.*", in particular it
   *  will consume leading whitespace and ignore trailing non-digits.
   */
  void setLineno (const char *s)
  {
    int n = 0;
    while (isspace (s [n])) {
      n++;
    }
    mLineno = 0;
    while (isdigit (s [n])) {
      mLineno = 10 * mLineno + (int)(s [n]) - (int)'0';
      n++;
    }
  }

  //! Note that a line has been ignored
  void Ignore (CodegenErrorLexer *callback, const char *line)
  {
    callback->Debug ("Ignore: %s", line);
  }

  //! Call the CxxError callback
  void CxxError (CodegenErrorLexer *callback, const char *message)
  {
    INFO_ASSERT (mFilename != NULL, "missing filename in error message");
    callback->Debug ("CxxError: %s: %d: %s:", mFilename, mLineno, message);
    callback->CxxError (mFilename, mLineno, message);
    clear ();
  }

  //! Call the MakeError callback
  void MakeError (CodegenErrorLexer *callback, const char *message)
  {
    INFO_ASSERT (mFilename != NULL, "missing filename in error message");
    callback->Debug ("MakeError: %s: %d: %s", mFilename, mLineno, message);
    callback->MakeError (mFilename, mLineno, message);
    clear ();
  }

  //! Call the SpeedCC callback
  void SpeedCC (CodegenErrorLexer *callback, const char *message)
  {
    callback->Debug ("SpeedCC: %s", message);
    callback->SpeedCC (message);
  }

private:

  char *mFilename;                      //!< currently matched filename
  UInt32 mLineno;                       //!< currently matched lineno

};

static ErrorContext gError;

#define ENTER(_x_) { \
  if (yy_flex_debug) { \
    UtIO::cout () << __LINE__ << ": enter state " << #_x_ << "\n";  \
  } \
  BEGIN (_x_); \
}

%}

%option debug
%option noyywrap
%option nodefault
%option yylineno

/* To avoid isatty problems on Windows. */
%option never-interactive

/* states that match GCC error messages */
%x xGCC0 xGCC1 xGCC2 xGCC3

/* states that match make: ... messages */
%x xMAKE0 xMAKE1 xMAKE2 xMAKE3

/* states that match SpeedCC error messages */
%x xSpeedCC0 xSpeedCC1

%%

<INITIAL>[: \t] {}  /* eat leading colons and whitespace */

  /* An error from make */
<INITIAL>make[ \t]*: { ENTER (xMAKE0); }

  /* Match a leading filename that is probably a makefile: */
<INITIAL>[^ :\t\n]+\/Make[^ :\t\n/]*[ \t]*: {
  gError.setFilename (yytext);
  ENTER (xMAKE1);
}

<INITIAL>^SpeedCC:  { ENTER (xSpeedCC0); }
<xSpeedCC0>Error:   { ENTER (xSpeedCC1); }
<xSpeedCC0>Warning:.* { /* throw out warnings */ callback->foundWarning (); gError.Ignore (callback, yytext); ENTER (INITIAL); }
<xSpeedCC0>Info:.*    { /* throw out info */     gError.Ignore (callback, yytext); ENTER (INITIAL); }
<xSpeedCC0>:          { ENTER (xSpeedCC1); }
<xSpeedCC0,xSpeedCC1>[ \t]  { /* consume whitespace */ }
<xSpeedCC0,xSpeedCC1>\n     { gError.SpeedCC (callback, ""); }
<xSpeedCC0>.          { unput (*yytext); ENTER (xSpeedCC1); }
<xSpeedCC1>.*         { gError.SpeedCC (callback, yytext); ENTER (INITIAL); }

  /* Match a leading filename: */
<INITIAL>[^ :\t\n]+[ \t]*: {
  gError.setFilename (yytext);
  ENTER (xGCC0);
}

 /* If it cannot be categorised then just ignore it... if we want to
    specifically process any other error message then new rules should be
    added. The use will still get the "look at libdesign.codegen.errors
    message. */

  /* This is a line without any colons... we have no way to pluck it apart. If
     there is a colon then either the filename or the make rules will be applied. */
<INITIAL>[^:\n]*                { gError.Ignore (callback, yytext); }

  /* End-of-line */
<INITIAL>\n                     { gError.clear (); }


<xMAKE0>[ \t\*]                 {}
<xMAKE0>[^ \t\*].*              { callback->MakeError (yytext); ENTER (INITIAL); }

 /* Look for Make compiler error messages */
<xMAKE1>[0-9]+[ \t]*:	{ gError.setLineno (yytext); ENTER (xMAKE2); }
<xMAKE1>.               { unput (*yytext); gError.setLineno (0); ENTER (xMAKE2); }
<xMAKE1>\n              { gError.setLineno (0); gError.MakeError (callback, ""); ENTER (INITIAL); }
<xMAKE2>[ \t]		{ /* consume whitespace */ }
<xMAKE2>\*		{ /* consume leading asterisks... make is obnoxious with these */ }
<xMAKE2>:[ \t]*		{ ENTER (xMAKE3); }
<xMAKE2>.		{ unput (*yytext); ENTER (xMAKE3); }
<xMAKE2,xMAKE3>\n       { gError.MakeError (callback, ""); ENTER (INITIAL); }
<xMAKE3>.* 		{ gError.MakeError (callback, yytext); ENTER (INITIAL); }

 /* Look for GCC-style compiler error messages */
<xGCC0>[ \t]*                  { /* consume space before the linenumber */ }
<xGCC0>[0-9]+[ \t]*:	       { gError.setLineno (yytext); ENTER (xGCC1); }
<xGCC1>[ \t]*                  { /* consume space after the colon */ }
<xGCC1>fatal\ error[ \t]*\:\ * { ENTER (xGCC3); }
<xGCC1>error[ \t]*:\ * 	       { ENTER (xGCC2); }
<xGCC1>.      		       { unput (*yytext); ENTER (xGCC2); }

 /* recognise some specific error messages from gcc */
<xGCC3>[ \t]*"can't seek in ".*"tmp".* { 
  gError.CxxError (callback, yytext);
  callback->getMsgContext ().CGTmpMayBeFull ();
  ENTER (INITIAL); }

<xGCC3>.*                     { gError.CxxError (callback, yytext); ENTER (INITIAL); }
<xGCC2>.*                     { gError.CxxError (callback, yytext); ENTER (INITIAL); }
<xGCC0,xGCC1,xGCC2,xGCC3>\n   { gError.CxxError (callback, "");     ENTER (INITIAL); }

<xGCC0>. {}

%%

CodegenErrorLexer::CodegenErrorLexer (MsgContext &msg_context, const char *filename,
  const UInt32 flags) : 
  mMsgContext (msg_context), mFlags (flags), mDebugging (false), mFilename (filename),
  mNWarnings (0)
{
  // yy_flex_debug can be set at instantiation
  yy_flex_debug = (mFlags & cYYDEBUG) ? 1 : 0;
  mDebugging = (mFlags & cDEBUG);
  // ... or it can be set from the environment
  const char *value;
  if ((value = getenv ("CARBON_ERRORS_FLEX_DEBUG")) != NULL && *value == '1') {
    yy_flex_debug = 1;
    mDebugging = true;
  }
  if ((value = getenv ("CARBON_ERRORS_DEBUG")) != NULL && *value == '1') {
    mDebugging = true;
  }
}

bool CodegenErrorLexer::scan ()
{
  UtString errmsg;
  if ((yyin = OSFOpen (mFilename, "r", &errmsg)) == NULL) {
    mMsgContext.CGCannotOpenCodegenErrors (mFilename, errmsg.c_str ());
    return false;
  }
  /* the generated scanner processes the entire file and calls back 
     to the CodegenError virtual methods */
  yylex (this);
  fclose (yyin);
  return true;
}

//! Output a debug message if mDebugging is set
void CodegenErrorLexer::Debug (const char *fmt, ...)
{
  if (mDebugging) {
    char buffer [256];
    va_list ap;
    va_start (ap, fmt);
    vsnprintf (buffer, sizeof (buffer), fmt, ap);
    va_end (ap);
    UtIO::cout () << mFilename << ": " << yylineno << ": " << buffer << "\n";
  }
}
