// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUAssign.h"
#include "nucleus/NUSysFunctionCall.h"
#include "nucleus/NUSysRandom.h"
#include "util/StringAtom.h"
#include "emitUtil.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUDesign.h"
#include <math.h> // for double pow(double, double)

CGContext_t NUSysFunctionCall::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);
  CGContext_t rc = eCGVoid;

  UtOStream& out = gCodeGen->CGOUT ();
  NU_ASSERT(!isHierName(context), this); // cannot handle conditional flow of func calls
  
  // Force function to appropriate result type....
  out << TAG << "static_cast<" 
      << emitUtil::CBaseTypeFmt (getBitSize (), eCGDeclare, isSignedResult (), isReal ())
      << " >(";
  
  // Cast the scaled time value to the appropriate type
  switch ( mOp ) {
  case NUOp::eZSysTime:
    // LRM (17.14) says "64 bit integer value" (implying signed...), but I
    // think that it really means to be an unsigned value.
    // Note that LRM 3.9 says "time variables are unsigned 64 bit.
    out << "static_cast<UInt64>(";
    if (getBitSize () < 64)
  
    rc = Imprecise (rc);
    break;
  case NUOp::eZSysStime:
    out << "static_cast<UInt32>(";
    if (getBitSize () < 32)
      rc = Imprecise (rc);
    break;
  case NUOp::eZSysRealTime:
    out << "static_cast<CarbonReal>(";
    if (getBitSize () != 64)
      rc = Imprecise (rc);
    break;
  case NUOp::eZEndFile:
    out << "carbonInterfaceVHDLInputFileSystemEOF(this, ";
    rc = getArg(0)->emitCode(context);
    out << "))"; 
    return Precise (eCGVoid);
  case NUOp::eNaFopen:
    NU_ASSERT ("eNaFopen not handled here" == 0, this);  // we should never get here, now handled by NUFOpenSysTask
    break;
  default:
    NU_ASSERT("Unexpected op in SysFunctionCall" == 0, this);
    break;
  }

  // Generate the scaling factor.
  const SInt8 clockTick = gCodeGen->getDesign()->getGlobalTimePrecision();
  // scaleDiff is the magnitude difference between our timescale and the
  // master clock.
  SInt32 scaleDiff = clockTick - getTimeUnit();
  // Since clockTick is the smallest possible time unit in the
  // simulation, scaleDiff must be <= 0.
  NU_ASSERT( scaleDiff <= 0 , this);
  // scaleMult is the power of 10 to multiply the clock value by to
  // account for this sysFuncCall's `timescale
  double scaleMult = pow( 10.0, scaleDiff );

  if ( mOp == NUOp::eZSysTime || mOp == NUOp::eZSysStime )
    out << "0.5 + double("; // needed to correctly round integers
  out << scaleMult << ") * (";

  out << "*(carbonPrivateGetTimeVarAddr (this))";
  out << ")";
  if ( mOp == NUOp::eZSysTime || mOp == NUOp::eZSysStime )
    out << ")";
  out << ")";                   // close outermost static_cast
  
  return rc;
}

//! Emit C++ Code 
CGContext_t NUSysRandom::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream& out = gCodeGen->CGOUT ();
  out << TAG;
  NU_ASSERT(!isHierName(context), this); // cannot handle conditional flow of func calls

  const char* func = NULL;
  switch (mFunction)
  {
  case eRandom:          func = "Random";       break;
  case eDistUniform:     func = "Uniform";      break;
  case eDistNormal:      func = "Normal";       break;
  case eDistExponential: func = "Exponential";  break;
  case eDistPoisson:     func = "Poisson";      break;
  case eDistChiSquare:   func = "ChiSquare";    break;
  case eDistT:           func = "T";            break;
  case eDistErlang:      func = "Erlang";       break;
  default: NU_ASSERT("Unexpected Function" == 0, this);
  }

  CopyContext cc (0,0);

  if (getSeedLvalue() != NULL)
  {
    // Pre-assign the lvalue with the rvalue for the seed. This does
    // nothing if the two are the same. But it used the old value of
    // the seed if it got buffered.
    NUNet* net = getSeedNet();
    NU_ASSERT(net->getBitSize() == 32, getSeedLvalue()); // should be caught at population time
    NUStmt* initSeed = new NUBlockingAssign (getSeedLvalue ()->copy (cc),
                                             mValueExprVector[0]->copy (cc),
                                             getUsesCFNet (),
                                             getLoc ());
    out << TAG;
    initSeed->emitCode (context);
    out << TAG;
    delete initSeed;
  }

  NUNet* lnet = emitUtil::sFindNet (getOutLvalue ());

  CGContext_t rc = getOutLvalue()->emitCode(context);

  NU_ASSERT (not isDelayed (rc), this); // Can't handle {a,b} = $random()
  NU_ASSERT (not emitUtil::netIsDeclaredForcible (lnet), this); // or forcible dests

  if (not isBitfield (rc))
    out << " = ";               // Not doing a SET_SLICE(lval, pos, siz, ....

  out << TAG << "carbonInterfaceVerilogDist" << func << "(";

  if (getSeedLvalue() != NULL)
  {
    NUNet* net = getSeedNet();

    out << "reinterpret_cast<SInt32*>(&"; // first arg is always SInt32*
    net->emitCode(context);
    out << ")";
    for (size_t i = 1; i < mValueExprVector.size(); ++i)
    {
      out << ", " << TAG;
      mValueExprVector[i]->emitCode(context);
    }
  }
  else
  {
    // Must find the default seed based on the model, which can be
    // found from the current instance
    out << TAG << "carbonInterfaceGetRandomSeed(this)";
  }

  out << ")";

  if (isBitfield (rc))
    out << ")";

  if (not noSemi (context))
    out << ";";

  return eCGVoid;
} // CGContext_t NUSysRandom::emitCode

class SysFuncCallback: public NUDesignCallback
{
public:
  SysFuncCallback(NUModule* mod): mModule(mod) {}
  // first define a default callback that does nothing
  virtual Status operator()(Phase, NUBase*) {return eNormal;}

  // now callbacks for the objects we are looking for, and their
  // respective actions.

  // in the following we return eNormal to keep looking for other
  // sysFuncs/Tasks since there may be other groups in the module
  // For efficiency we could return eStop if we knew that we had seen
  // and marked all groups for this module but that test could get out
  // of date if someone adds a new group.  It is simpler to just look
  // at everything.

  virtual Status operator()(Phase, NUSysFunctionCall*) {
    mModule->putRunsSystemFunctionsOfTimeGroup(true);
    return eNormal;
  }

  virtual Status operator()(Phase, NUSysRandom*) {
    mModule->putRunsSystemTasksOfRandomGroup(true);
    return eNormal;
  }

  virtual Status operator()(Phase, NUOutputSysTask* systask) {
    mModule->putRunsSystemTasksOfOutputGroup(true, systask->isVerilogTask());
    return eNormal;
  }

  virtual Status operator()(Phase, NUInputSysTask*) {
    mModule->putRunsSystemTasksOfInputGroup(true);
    return eNormal;
  }

  virtual Status operator()(Phase, NUFOpenSysTask* systask) {
    mModule->putRunsSystemTasksOfOutputGroup(true, systask->isVerilogTask());
    mModule->putRunsSystemTasksOfInputGroup(true);
    return eNormal;
  }
  virtual Status operator()(Phase, NUFCloseSysTask* systask) {
    mModule->putRunsSystemTasksOfOutputGroup(true, systask->isVerilogTask());
    mModule->putRunsSystemTasksOfInputGroup(true);
    return eNormal;
  }
  virtual Status operator()(Phase, NUFFlushSysTask* systask) {
    mModule->putRunsSystemTasksOfOutputGroup(true, systask->isVerilogTask());
    return eNormal;
  }

  virtual Status operator()(Phase, NUControlSysTask*) {
    mModule->putRunsSystemTasksOfControlGroup(true);
    return eNormal;
  }

private:
  NUModule* mModule;
};


// Walk all the expressions in a module and determine whether
// there are any system function/task calls (includes $random,
// $dist*, $(f)display, $(f)write, $fopen/close/flush, $stop/$finish)
class SysFuncWalker: public NUDesignWalker
{
public:
  SysFuncWalker(SysFuncCallback& cb): NUDesignWalker(cb, false) {}
};

void CodeGen::determineSystemCalls(NUDesign* design)
{
  for (NUDesign::ModuleLoop p = design->loopAllModules(); !p.atEnd(); ++p)
  {
    NUModule* mod = *p;
    SysFuncCallback cb(mod);
    SysFuncWalker walk(cb);
    walk.module(mod);
  }
}
