// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implement unelaborated ranking of nets based on elaborated scheduling information.
*/

#include "RankNets.h"
#include "emitUtil.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUCycle.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "schedule/Schedule.h"

#include "util/SetOps.h"


RankNets::RankNets(UInt32 locality_technique,
                   bool ondemand,
                   bool verbose) :
  mLocalityTechnique(locality_technique),
  mOnDemand(ondemand),
  mVerbose(verbose)
{
}


RankNets::~RankNets()
{
}


void RankNets::computeRank(SCHSchedule *schedule)
{
  // A technique of '0' means that we are using the simple size-based
  // ordering. There is no reason to process all scheduled elements,
  // as net ordering is self-determined.
  if (mLocalityTechnique==0) {
    return;
  }

  NUCUseDefSet seen;
  SInt32 order = 0;

  // Traverse all schedules and every node within each schedule.
  for (SCHSchedulesLoop loop = schedule->loopAllSimulationSchedules();
       not loop.atEnd();
       ++loop) {
    for (SCHSchedulesLoop::Iterator iter = loop.getSchedule();
         not iter.atEnd();
         ++iter) {
      NUUseDefNode *node = (*iter)->getUseDefNode();
      rankDefinedNetsCycle(node,&seen,order);
    }
  }

  // Initial and debug schedules are not included by
  // loopAllSimulationSchedules; process them at the end.
  for (SCHIterator iter = schedule->getInitialSchedule()->getSchedule();
       not iter.atEnd();
       ++iter) {
    NUUseDefNode *node = (*iter)->getUseDefNode();
    rankDefinedNetsCycle(node,&seen,order);
  }
  for (SCHIterator iter = schedule->getInitSettleSchedule()->getSchedule();
       not iter.atEnd();
       ++iter) {
    NUUseDefNode *node = (*iter)->getUseDefNode();
    rankDefinedNetsCycle(node,&seen,order);
  }
  for (SCHIterator iter = schedule->getDebugSchedule()->getSchedule();
       not iter.atEnd();
       ++iter) {
    NUUseDefNode *node = (*iter)->getUseDefNode();
    rankDefinedNetsCycle(node,&seen,order);
  }
}


void RankNets::rankDefinedNetsCycle(const NUUseDefNode * node,
                                    NUCUseDefSet * seen,
                                    SInt32 & order)
{
  if (node->getType() == eNUCycle) {
    // If we encounter a cycle, recurse through the referenced nodes
    // until we find a non-cycle node.
    const NUCycle *cycle = dynamic_cast<const NUCycle*>(node);
    NU_ASSERT(cycle, node);
    const FLNodeElabVector* schedule = cycle->getOrderedFlowVector();
    for (FLNodeElabVector::const_iterator p = schedule->begin(); p != schedule->end(); ++p)
    {
      FLNodeElab *flow = *p;
      if (not flow->isBoundNode()) {
        rankDefinedNetsCycle(flow->getUseDefNode(),seen,order);
      }
    }
    delete schedule;
  } else {
    // Perform rank-computation for non-cycles.
    rankDefinedNetsAlways(node,seen,order);
  }
}

void RankNets::rankDefinedNetsAlways(const NUUseDefNode * node,
                                     NUCUseDefSet * seen,
                                     SInt32 & order)
{
  if (mLocalityTechnique==5 and node->getType() == eNUAlwaysBlock) {
    // Technique 5 considers each statement within an always block
    // separately. Ranking all defs of an always block together could
    // lead to a partial ordering with tons of ambiguity, as all nets
    // would obtain the same rank. Ranking nets by the defining stmt
    // leaves fewer nets in each rank -- less ambiguity.

    // Of course, this falls apart if all nets are defined within one
    // stmt (a top-level if() stmt, for example).

    const NUAlwaysBlock * always = dynamic_cast<const NUAlwaysBlock *>(node);
    NU_ASSERT(always, node);
    const NUBlock * block = always->getBlock();
    for (NUStmtCLoop loop = block->loopStmts();
         not loop.atEnd();
         ++loop) {
      const NUStmt * stmt = (*loop);
      rankDefinedNets(stmt,seen,order);
    }
  } else {
    // directly poll the defined nets.
    rankDefinedNets(node,seen,order);
  }
}


void RankNets::rankDefinedNets(const NUUseDefNode * node,
                               NUCUseDefSet * seen,
                               SInt32 & order)
{
  // only process each node once.
  if (seen->find(node)==seen->end()) {
    seen->insert(node);

    // keep track of the schedule position of this node; some ordering
    // techniques make use of it.
    ++order; 

    // schedules have already been recursively processed by
    // rankDefinedNetsCycle.
    NU_ASSERT(not node->isCycle(), node);

    switch(mLocalityTechnique) {
    case 1: 
    {
      // Assign rank based on the last time we see the net written in
      // a block.

      NUNetSet nets;
      getDefs(node,&nets);
      for (NUNetSet::const_iterator iter = nets.begin();
           iter != nets.end();
           ++iter) {
        const NUNet * net = (*iter);
        if (not net->isNonStatic()) {
          // assign order even if we have encountered this net before
          // (compare to technique 4).
          mNetOrder[net] = order;
        }
      }
      break;
    }

    case 2: 
    {
      // Assign rank based on the number of blocks which use a
      // particular net. Nets with a high use count should float to
      // the top of a module's declared data members.

      NUNetSet nets;
      node->getUses(&nets);
      for (NUNetSet::const_iterator iter = nets.begin();
           iter != nets.end();
           ++iter) {
        const NUNet * net = (*iter);
        if (not net->isNonStatic()) {
          if (mNetOrder.find(net) == mNetOrder.end()) {
            mNetOrder[net] = 0;
          }
          // each time this net is used, increment its rank.
          ++(mNetOrder[net]); 
        }
      }
      break;
    }

    case 3: 
    {
      // Assign rank based on the number of blocks which use but do
      // not define a particular net. Nets with a high use count
      // should float to the top of a module's declared data members.

      NUNetSet uses;
      NUNetSet defs;
      node->getUses(&uses);
      getDefs(node,&defs);

      // filter out defined nets.
      NUNetSet nets;
      set_difference(uses,defs,&nets);
      for (NUNetSet::const_iterator iter = nets.begin();
           iter != nets.end();
           ++iter) {
        const NUNet * net = (*iter);
        if (not net->isNonStatic()) {
          if (mNetOrder.find(net) == mNetOrder.end()) {
            mNetOrder[net] = 0;
          }
          // each time this net is used, increment its rank.
          ++(mNetOrder[net]); 
        }
      }
      break;
    }

    case 4: 
    case 5:
    {
      // Assign rank based on the first block which defines a net.

      NUNetSet nets;
      getDefs(node,&nets);
      for (NUNetSet::const_iterator iter = nets.begin();
           iter != nets.end();
           ++iter) {
        const NUNet * net = (*iter);
        if (not net->isNonStatic()) {
          // only assign order if we haven't encountered this net
          // before (compare to technique 1).
          if (mNetOrder.find(net) == mNetOrder.end()) {
            mNetOrder[net] = order;
          }
        }
      }
      break;
    }

    default:
      NU_ASSERT("Unexpected LocalityTechnique" == 0, node);
    }
  }
}


SInt32 RankNets::getRank(const NUNet * net) const
{
  NetOrder::const_iterator location = mNetOrder.find(net);
  SInt32 rank;
  if(location != mNetOrder.end()) {
    rank = (*location).second;
  } else {
    // If a net hasn't been already assigned a rank, choose one. We
    // may want to change this default ranking based on the technique
    // used.

    // Experiment: Which is better? 

    // The default rank for unranked nets should probably depend on
    // the technique we're using. Input ports should probably appear
    // at the top of the declarations, while other nets should be
    // pushed to the back.

    // If you change the default rank for unranked nets, consider
    // changing the iteration order in ::orderByRank()

    // rank = UtUINT32_MAX;
    if (net->isPort()) {
      rank = 0;
    } else {
      rank = UtUINT32_MAX;
    }
  }
  return rank;
}


void RankNets::orderByRank(const NUModule * module, NUNetCVector * nets) const
{
  // onDemand has its own layout, designed to keep all saved state
  // contiguous in memory.
  if (mOnDemand) {
    emitUtil::sort_by_state stateSorter;
    std::stable_sort (nets->begin(), nets->end(), stateSorter);
  } else if (mLocalityTechnique==0) {
    // Old sorting technique -- purely by alignment.
    emitUtil::sort_by_align alignedSorter;
    std::stable_sort (nets->begin(), nets->end(), alignedSorter);
  } else {
    // New sorting technique:
    //   Primary:   Rank
    //   Secondary: Alignment.

    typedef UtMap<SInt32,NUNetCVector> Rankings;
    Rankings rankings;

    // Separate the incoming nets based on their assigned rank.
    for (NUNetCVector::iterator iter = nets->begin();
         iter != nets->end();
         ++iter) {
      const NUNet * net = (*iter);
      SInt32 rank = getRank(net);
      rankings[ rank ].push_back(net);
    }

    // Clear out the net vector -- we will re-populate in rank-order.
    nets->clear();

    // We may want to try forward vs. reverse size-ordering based on
    // the technique used.

    // Primary sort: pre-determined rank.

    // Experiment: forward or reverse?

    // Forward sorting is probably best for the order-based orderings
    // (techniques 1,4,5) because the nets will appear in
    // schedule-encountered order.
    
    // Reverse sorting is probably best for accumulation-based
    // orderings (techniques 2,3) because the nets with highest rank
    // (most used) will appear first.

    // If you change the iteration order, consider changing the
    // default rank for unranked nets in ::getRank().

    if (mVerbose) {
      printRankHeader(module);
    }

    for (Rankings::iterator iter = rankings.begin();
         iter != rankings.end();
         ++iter) {
      SInt32 rank = (*iter).first;
      NUNetCVector & group = (*iter).second;

      // Secondary sort: member size

      // Experiment: forward or reverse?
      
      // Tried to emitUtil::sort_by_align with technique 4; this
      // caused a slight slowdown.

      std::stable_sort(group.begin(), group.end(),
                       emitUtil::sort_by_reverse_align());

      if (mVerbose) {
        printRankNets(rank,&group);
      }

      nets->insert(nets->end(), group.begin(), group.end());
    }
  }
}


void RankNets::getDefs(const NUUseDefNode * node, NUNetSet * nets) const
{
  if (node->useBlockingMethods()) {
    const NUUseDefStmtNode * stmt = dynamic_cast<const NUUseDefStmtNode*>(node);
    NU_ASSERT(stmt, node);
    stmt->getBlockingDefs(nets);
  } else {
    node->getDefs(nets);
  }
}


void RankNets::printRankHeader(const NUModule * module) const
{
  UtIO::cout() << "RankNets for module: " << module->getName()->str() << UtIO::endl;
}


void RankNets::printRankNets(SInt32 rank, NUNetCVector * group) const
{
  UtIO::cout() << "    Rank: " << rank << UtIO::endl;
  for (NUNetCVector::const_iterator iter = group->begin();
       iter != group->end();
       ++iter) {
    const NUNet * net = (*iter);
    UtIO::cout() << "        " << net->getName()->str() << UtIO::endl;
  }
}
