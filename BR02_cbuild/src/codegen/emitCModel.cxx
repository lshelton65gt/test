// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "codegen/codegen.h"
#include "emitUtil.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModelPort.h"
#include "nucleus/NUCModelArgConnection.h"
#include "nucleus/NUCModelFn.h"
#include "nucleus/NUCModel.h"
#include "nucleus/NUExpr.h"
#include "util/StringAtom.h"
#include "compiler_driver/CarbonContext.h"

using namespace emitUtil;

enum CModelNameT { eCMCreate, eCMRun, eCMDestroy, eCMMisc, eCMSave, eCMRestore,
                   eCMSubClass, eCMObject, eCMHandle, eCMContextType,
                   eCMRunFnPtr };

static inline void
makeVarName(CModelNameT type, const char* name, UtString& str)
{
  switch(type)
  {
    case eCMSubClass:
      str << "CModel" << name;
      break;

    case eCMObject:
      str << "m_" << name;
      break;

    case eCMHandle:
      str << "m_hndl";
      break;
      
    case eCMCreate:
      str << "cds_" << name << "_create";
      break;
      
    case eCMRun:
      str << "cds_" << name << "_run";
      break;
      
    case eCMDestroy:
      str << "cds_" << name << "_destroy";
      break;
      
    case eCMContextType:
      str << "CDS" << name << "Context";
      break;

    case eCMMisc:
      str << "cds_" << name << "_misc";
      break;

    case eCMRunFnPtr:
      str << "m_run";
      break;

    default:
      INFO_ASSERT(0, "unexpected CModelNameT value");
      break;
      
  } // switch
} // makeVarName


// Common code to allocate/deallocate cmodel data wrappers for
// onDemand/replay, and to hack the cmodel's user context.
static void sEmitAllocCModelData(UtOStream &out, CGContext_t context, const NUModule *mod, const UtString *class_name, CarbonUInt32 input_words, CarbonUInt32 output_words)
{
  if (isDeclare(context)) {
    out << "    void allocCModelData(CarbonObjectID* descr);\n\n";
  } else if (isDefine(context)) {
    out << "  void " << mclass(mod->getName()) << "::" << *class_name
        << "::allocCModelData(CarbonObjectID* descr)\n"
        << "  {\n"
        << "    CarbonReplayCModelData *data = carbonInterfaceAllocReplayCModelData(descr, m_hndl,"
        << " (void*)this, " << input_words << ", " << output_words << ");\n"
        << "    m_hndl = (void*)data;\n"
        << "  }\n\n";
  }
}

static void sEmitFreeCModelData(UtOStream &out, CGContext_t context, const NUModule *mod, const UtString *class_name)
{
  if (isDeclare(context)) {
    out << "    void freeCModelData();\n\n";
  } else if (isDefine(context)) {
    out << "  void " << mclass(mod->getName()) << "::" << *class_name
        << "::freeCModelData()\n"
        << "  {\n"
        << "    CarbonReplayCModelData *data = (CarbonReplayCModelData*)m_hndl;\n"
        << "    m_hndl = carbonInterfaceGetReplayCModelUserData(data);\n"
        << "    carbonInterfaceFreeReplayCModelData(data);\n"
        << "  }\n\n";
  }
}

// TBD
CGContext_t
NUCModelCall::emitCode (CGContext_t /* context */) const

{
  // As part of that the tmp signals we declare must use the cmodel
  // name to be unique. So store that in the global area for use by
  // the conversion code.
  gCodeGen->incrCModelCallName();

  // Generate local storage for all the arguments
  UtOStream& out = gCodeGen->CGOUT ();
  out << "  // Local storage for c-model arguments" << ENDL;
  const NUCModelInterface* cmodelInterface = getCModelInterface();
  NUCModelInterface::PortsCLoopNoNull portsList;
  portsList = cmodelInterface->loopPortsNoNull();
  emitCodeList (&portsList, eCGDeclare);
  out << ENDL;

  // Convert the arguments. 
  out << "  // Convert the arguments to UInt32* as necessary" << ENDL;
  NUCModelArgConnectionCLoop argList = loopArgConnections();
  emitCodeList (&argList, eCGDefine|eCGMarkNets);
  out << ENDL;

  // Emit the call to the c-model
  out << "  // Call the c-model" << ENDL;
  const char* name = getName()->str();
  const char* instName = getCModel()->getName()->c_str();
  UtString hndlName;
  makeVarName(eCMHandle, instName, hndlName);
  UtString objName;
  makeVarName(eCMObject, instName, objName);
  UtString runFnPtr;
  makeVarName(eCMRunFnPtr, name, runFnPtr);
  out << TAG << "  (*" << objName << "." << runFnPtr << ")("
      << objName << "." << hndlName;
  const char* context = getContext();
  if (context != NULL)
    out << ", " << getContext();

  // Emit the variable arguments if there are any
  if (!portsList.atEnd()) {
    emitCodeList(&portsList, eCGDefine, ",\n\t", ",\n\t", "");
  }
  out << ");" << ENDL << "\n";

  // Copy information back for the arguments.
  out << "  // Convert any data back to Carbon format as necessary" << ENDL;
  emitCodeList(&argList, eCGVoid);
  out << ENDL;

  return eCGVoid;
}

      
static inline void makeHeaderFile(const char* name, UtString& hdrName)
{
  const char* targetName = gCodeGen->getTargetName()->c_str();
  hdrName << "cds_" << targetName << "_" << name << ".h";
}


CGContext_t
NUCModelPort::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  // Create the temp name prefix for this port
  UtString tmpName;
  tmpName << "tmp_" << gCodeGen->getCModelCallName() << "_";

  // Make a name that will compile with C
  CodeNames* codeNames = gCodeGen->getCodeNames();
  const StringAtom* name = codeNames->translate(getName());

  if (isDeclare(context))
  {
    // Declare temporary storage for these
    UtOStream& out = gCodeGen->CGOUT ();
    out << "  CarbonUInt32* " << tmpName;
    out << name->str ();
    out << " = 0;" << ENDL;
  }
  else if (isDefine(context))
  {
    // Pass the argument
    UtOStream& out = gCodeGen->CGOUT ();
    out << tmpName << name->str () << TAG;
  }

  return eCGVoid;
}

static void sEmit64To32ArrConv(UtOStream& out, UtString& left, UtString& right, const StringAtom* name)
{
  out << TAG;
  out << "  " << left << name->str() << "[0] = (UInt32)("
      << "" << right << name->str() << "& 0xFFFFFFFF);\n";
  out << "  " << left << name->str() << "[1] = (UInt32)("
      << "(" << right << name->str() << ">> 32) & 0xFFFFFFFF);\n";
}

CGContext_t
NUCModelArgConnectionInput::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  // Process this input argument
  if (isDefine(context))
  {
    // Figure out the size of the net (not the size of the c-model port)
    NUExpr* actual = getActual();
    UInt32 bitSize = actual->getBitSize();

    // Get the max size of the port
    const NUCModelPort* port = getFormal();
    UInt32 portSize = port->getMaxSize();

    // Create the unquified temp names
    UtString tmpName1;
    UtString tmpName2;
    UtString tmpName3;
    tmpName1 << "tmp_" << gCodeGen->getCModelCallName() << "_";
    tmpName2 << "tmp2_" << gCodeGen->getCModelCallName() << "_";
    tmpName3 << "tmp3_" << gCodeGen->getCModelCallName() << "_";

    // Copy the data from the rvalue to the temporary storage. This is
    // different based on the size of the rhs:
    //
    //   If the rhs is <= 32 bits and the port is one 32 bit word,
    //   copy the data to the allocated space on the stack
    //
    //   If the rhs is <= 32 bits and the port is more than one word,
    //   allocate space for the one word, fill it with data and pass
    //   its address into the c-model.
    //
    //   If the rhs is > 32 bits but < 65 bits we have to convert a
    //   ULL to an array of UL's. We need storage for it.
    //
    //   If the rhs is > 64 bits then we create the appropriate size
    //   bit vector and get the address of it.
    //
    // **NOTE**: If this code is improved, don't forget to fixup
    //           NUCost.cxx.
    //
    CodeNames* codeNames = gCodeGen->getCodeNames();
    const StringAtom* name = codeNames->translate(port->getName());
    UtOStream& out = gCodeGen->CGOUT ();
    if (bitSize <= 32)
    {
      // We have to allocate storage since the callee expects the
      // data by reference.
      out << "  CarbonUInt32 " << tmpName2 << name->str () << ";" << ENDL;
      out << "  " << tmpName1 << name->str () << " = &" << tmpName2 << name->str () << ";" << ENDL;
      out << "  " << tmpName2 << name->str () << " = ";
      if (actual->isSignedResult())
        // Since the parameter is a C/C++ POD, we can just cast it cause proper sign
        // extension to occur.  [Actually, I think this is redundant - and C ought to
        // do the widening by itself according to "the usual conversions"...
        //
        out << CBaseTypeFmt(actual, true, eCGDefine);
      actual->emitCode(context);
      out << ";" << ENDL;
    }
    else if (bitSize <= 64)
    {
      // We need to allocate space and convert the ULL to an array of UL
      NU_ASSERT(portSize > 1, this);
      out << "  CarbonUInt32 " << tmpName2 << name->str () << "[2];" << ENDL;
      out << "  " << tmpName1 << name->str () << " = &"
          << tmpName2 << name->str () << "[0];" << ENDL;
      out << "  CarbonUInt" << bitSize << " " << tmpName3 << name->str() << " = ";
      if (actual->isSignedResult())
        out << CBaseTypeFmt(actual, true, eCGDefine);
      actual->emitCode(context);
      out << ";" << ENDL;
      sEmit64To32ArrConv(out, tmpName2, tmpName3, name);
    }
    else
    {
      // Pass the address of the bitvector in
      NU_ASSERT(portSize > 2, this);
      out << CBaseTypeFmt (bitSize,eCGDeclare,actual->isSignedResult ()) << tmpName2 << name->str() << " = ";
      gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path);
      actual->emitCode(context);
      out << ";" << ENDL;
      out << "  " << tmpName1<< name->str() << " = "
          << tmpName2 << name->str() << "." << CRYPT("getUIntArray") << "();" << ENDL;
    }
  }
  return eCGVoid;
}

CGContext_t
NUCModelArgConnectionOutput::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  // Get the actual. We currently only support whole identifiers. Port
  // lowering guarantees this and any optimization should as well.
  NULvalue* actual = getActual();
  NU_ASSERT(actual->isWholeIdentifier(), actual);

  // Figure out the size of the net (not the size of the c-model port)
  UInt32 bitSize = actual->getBitSize();
  CodeNames* codeNames = gCodeGen->getCodeNames();
  const StringAtom* name = codeNames->translate(getFormal()->getName());
  UtOStream& out = gCodeGen->CGOUT ();

  // Create the unquified temp names
  UtString tmpName1;
  UtString tmpName2;
  UtString tmpName3;
  tmpName1 << "tmp_" << gCodeGen->getCModelCallName() << "_";
  tmpName2 << "tmp2_" << gCodeGen->getCModelCallName() << "_";
  tmpName3 << "tmp3_" << gCodeGen->getCModelCallName() << "_";

  if (isDefine(context))
  {
    // Pass an address in for the c-model to modify. The work is
    // different based on the size of the lvalue net:
    //
    //   If the lhs is <= 16 bits we allocate storage and pass that
    //   into the c-model. We then have to post process as described
    //   below.
    //
    //   If the lhs is > 32 bits and < 65 bits, we allocate data on
    //   the stack and post process below.
    //
    //   If the lhs is > 16 bits and <= 32 bits or > 64 bits we pass
    //   the address of the carbon data
    //
    // Note that the port size does not matter to us since we are
    // always passing the data by pointer. But the c-model better know
    // the size of the data we are passing based on the parameters.
    //
    if (bitSize <= 16)
    {
      actual->emitCode(eCGMarkNets);
      out << "  CarbonUInt32 " << tmpName2 << name->str() << ";" << ENDL;
      out << "  " << tmpName1<< name->str() << " = &" << tmpName2 << name->str()
	  << ";" << ENDL;
      out << "  " << tmpName2 << name->str() << " = ";
      actual->emitCode(context);
      out << ";\n";
    }
    else if ((bitSize > 32) && (bitSize <= 64))
    {
      actual->emitCode(eCGMarkNets);
      out << "  CarbonUInt32 " << tmpName2 << name->str() << "[2];" << ENDL;
      out << "  " << tmpName1<< name->str() << " = &" << tmpName2 << name->str()
	  << "[0];" << ENDL;
      out << "  CarbonUInt" << bitSize << " " << tmpName3 << name->str() << " = ";
      actual->emitCode(context);
      out << ";\n";
      sEmit64To32ArrConv(out, tmpName2, tmpName3, name);
    }
    else
    {
      out << "  " << TAG << tmpName1<< name->str() << " = ";
      if (bitSize <= 32)
      {
        if (actual->isWholeIdentifier() && 
            actual->getWholeIdentifier()->isSigned())
          out << TAG << "(" << CBaseTypeFmt(bitSize, eCGVoid) << "*)";
	out << "(&";
	actual->emitCode(context);
	out << ");" << ENDL;
      }
      else
      {
	actual->emitCode(context);
	out << "." << CRYPT("getUIntArray") << "();" << ENDL;
      }
    }
  }
  else
  {
    // Copy the data back from the temporary storage
    bool isSigned = false;
    if (actual->isWholeIdentifier() && 
        actual->getWholeIdentifier()->isSigned())
      isSigned = true;
    if (bitSize <= 16)
    {
      out << "  " << TAG;
      actual->emitCode(eCGDefine);
      out << " = ";
      if ( actual->isWholeIdentifier() ) // allows reals to be properly detected
        out << CBaseTypeFmt( bitSize, eCGDefine, isSigned,
                             actual->getWholeIdentifier()->isReal());
      else
        out << CBaseTypeFmt(bitSize, eCGDefine, isSigned );
      out << "(" << tmpName2 << name->str();
      emitUtil::mask (bitSize, "&");

      out << ");" << ENDL;
    } else if (bitSize <= 32)
    {
      // Mask the operand in place
      out << "  " << TAG;
      actual->emitCode (eCGDefine);
      out << "&= carbon_mask<" 
          << CBaseTypeFmt (bitSize, eCGDeclare) << ">(" << bitSize << ");" << ENDL;
    }
    else if ((bitSize > 32) && (bitSize <= 64))
    {
      out << "  " << TAG;
      actual->emitCode(eCGDefine);
      out << " = (" << CBaseTypeFmt(bitSize, eCGDefine)
          << tmpName2 << name->str() << "[1] << 32) | ("
          << CBaseTypeFmt(bitSize, eCGDefine)
          << tmpName2 << name->str() << "[0]) & carbon_mask<"
          << CBaseTypeFmt (bitSize, eCGDeclare) << ">(" << bitSize << ");" << ENDL;
    }
    else {
      // BitVectors....
      // Because we don't trust the user's C-Model, we'll sanitize the BitVector
      out << "  " << TAG;
      actual->emitCode (eCGDefine);
      out << "._M_do_sanitize();" << ENDL;
    }
  }
  return eCGVoid;
} // NUCModelArgConnectionOutput::emitCode

CGContext_t
NUCModelArgConnectionBid::emitCode (CGContext_t /*context*/) const
{
  NU_ASSERT("Not Yet Implemented" == 0, this); // NYI.
  return eCGVoid;
}

// Helper function to get the unobfuscated c-model port name
static const char* getCModelPortName(const NUCModelPort* port, UtString* str)
{
  // Switch to interface mode and then get the codeNames variable. The
  // mode changes which CodeNames structure we use
  gCodeGen->putInterfaceMode();
  CodeNames* codeNames = gCodeGen->getCodeNames();

  // Translate the name to valid C++
  const char* portName = codeNames->translate(port->getName(), true)->str();

  // Now switch back to internal mode
  gCodeGen->putInternalMode();

  // Create the port name argument
  str->clear();
  *str << "a_" << portName;
  return str->c_str();
}

// Helper function to emit declarations for all c-model call arguments
// and connect the *live* arguments to provided storage. The way
// NUModule type c-models work is that not all the parameters are
// live in each NUCModelCall, only the ones that are defined by
// the input and output directives for that timing signature.
//
// The arrays of data are assumed to be in inputs and outputs.
//
// The resulting code looks something like:
//
//  UInt32* _out1 = NULL;
//  UInt32* _out2 = NULL;
//  UInt32* _in1 = NULL;
//  UInt32* _in2 = NULL;
//  _out1 = outputs[0];
//  _in2 = inputs[0];
//
// In the above case, this call has a path from in2 to out1 and
// the other paths are not active.
//
// This code is useful when mapping the inputs and outputs to
// replay playback data arrays
static void
emitArgsDeclarations(UtOStream& out, NUCModelCall* call,
                     const char* prefix = "      ")
{
  // Loop the ports and create the declarations. We need both the live
  // and dead declarations to pass to the user's c-model.
  NUCModelInterface* interface = call->getCModelInterface();
  for (NUCModelInterface::PortsLoopNoNull l = interface->loopPortsNoNull();
       !l.atEnd(); ++l) {
    NUCModelPort* port = *l;
    UtString portName;
    getCModelPortName(port, &portName);
    out << prefix << "UInt32* " << portName << " = NULL;" << ENDL;
  }

  // Loop the connections and hook them up. This only walks the live
  // connections.
  UInt32 outIndex = 0;
  UInt32 inIndex = 0;
  for (NUCModelArgConnectionLoop a = call->loopArgConnections(); !a.atEnd(); ++a) {
    NUCModelArgConnection* arg = *a;
    UInt32 words;
    const NUCModelPort* port = arg->getFormal();
    UtString portName;
    getCModelPortName(port, &portName);
    switch(arg->getDir()) {
      case eInput:
      {
        NUCModelArgConnectionInput* input = arg->castInput();
        words = (input->getActual()->getBitSize() + 31) / 32;
        out << prefix << portName << " = &inputs[" << inIndex
            << "];" << ENDL;
        inIndex += words;
        break;
      }

      case eOutput:
      {
        NUCModelArgConnectionOutput* output = arg->castOutput();
        words = (output->getActual()->getBitSize() + 31) / 32;
        out << prefix << portName << " = &outputs[" << outIndex
            << "];" << ENDL;
        outIndex += words;
        break;
      }

      case eBid:
        NU_ASSERT(arg->getDir() != eBid, arg);
        break;
    }
  } // !a.atEnd
} // emitArgsDeclarations

// Helper function to walk a c-model interfaces calls and copy the
// data from the passed in arguments (in the c-models prototype) to
// two arrays. The arrays are inputs and outputs.
//
// Note that each context has a different set of live nodes, so
// it copies the data based on the context. For UDTs, it has
// only one context so there is no switch statement.
//
// The resulting code for an NUModule c-model looks like:
//
//       switch(context) {
//       case eCDSsubAsync:
//         memcpy(&outputs[0], _out2, 4);
//         memcpy(&outputs[1], _out3, 4);
//         memcpy(&inputs[0], _a, 4);
//         memcpy(&inputs[1], _b, 4);
//         break;
//       case eCDSsubRiseclk:
//         memcpy(&outputs[0], _out1, 4);
//         memcpy(&outputs[1], _out3, 4);
//         memcpy(&inputs[0], _a, 4);
//         memcpy(&inputs[1], _b, 4);
//         memcpy(&inputs[2], _clk, 4);
//         break;
//       }
//
// For a PLI task it looks like:
//
//         memcpy(&outputs[0], _o, 4);
//         memcpy(&inputs[0], _a, 4);
//         memcpy(&inputs[1], _b, 4);
//
// This code is useful for recording c-model I/Os.
static void
emitCopyArgsToArrays(UtOStream& out, const NUCModel* cmodel,
                     CodeGen* gCodeGen, const char* prefix = "      ")
{
  // If this has contexts, we need a switch between all the different
  // contexts
  NUCModelInterface* interface = cmodel->getCModelInterface();
  bool hasContext = interface->hasContextStrings();
  if (hasContext) {
    out << prefix << "switch(context) {" << ENDL;
  }

  // Loop the c-model calls and emit the copies
  for (NUCModelCallVectorLoop l = gCodeGen->loopCModelCalls(cmodel);
       !l.atEnd(); ++l) {
    // If this is a context based, emit the switch case
    const NUCModelCall* call = *l;
    if (hasContext) {
      out << prefix << "case " << call->getContext() << ":" << ENDL;
    }

    // Emit the code to copy the inputs and outputs
    UInt32 outIndex = 0;
    UInt32 inIndex = 0;
    for (NUCModelArgConnectionCLoop a = call->loopArgConnections();
         !a.atEnd(); ++a) {
      NUCModelArgConnection* arg = *a;
      UInt32 words;
      const NUCModelPort* port = arg->getFormal();
      UtString portName;
      getCModelPortName(port, &portName);
      switch(arg->getDir()) {
        case eInput:
        {
          NUCModelArgConnectionInput* input = arg->castInput();
          words = (input->getActual()->getBitSize() + 31) / 32;
          out << prefix << "  memcpy(&inputs[" << inIndex << "], "
              << portName << ", "
              << words * sizeof(UInt32) << ");" << ENDL;
          inIndex += words;
          break;
        }

        case eOutput:
        {
          NUCModelArgConnectionOutput* output = arg->castOutput();
          words = (output->getActual()->getBitSize() + 31) / 32;
          out << prefix << "  memcpy(&outputs[" << outIndex << "], "
              << portName << ", "
              << words * sizeof(UInt32) << ");" << ENDL;
          outIndex += words;
          break;
        }

        case eBid:
          NU_ASSERT(arg->getDir() != eBid, arg);
          break;
      }
    } // !a.atEnd

    // Emit the end of the case item if we have contexts
    if (hasContext) {
      out << prefix << "  break;\n";
    }
  } // !l.atEnd

  // Emit the end of the case statement if this has contexts
  if (hasContext) {
    out << prefix << "  default:\n"
        << prefix << "    break;\n"
        << prefix << "}" << ENDL;
  }
} // emitCopyArgsToArrays

// Helper function to walk a c-model interfaces calls and copy the
// data from an array of outputs to the passed in arguments (in the
// c-models prototype). The array is "outputs".
//
// Note that each context has a different set of live nodes, so
// it copies the data based on the context. For UDTs, it has
// only one context so there is no switch statement.
//
// The resulting code for an NUModule c-model looks like:
//
//        switch(context) {
//        case eCDSsubAsync:
//          memcpy(out2, &outputs[0], 4);
//          memcpy(out3, &outputs[1], 4);
//          break;
//        case eCDSsubRiseclk:
//          memcpy(out1, &outputs[0], 4);
//          memcpy(out3, &outputs[1], 4);
//          break;
//        }
//
// For a UDT it looks like:
//
//           memcpy(o, &outputs[0], 4);
//
// This is useful during replay recover.
static void
emitCopyArraysToOutputs(UtOStream& out, const NUCModel* cmodel,
                     CodeGen* gCodeGen, const char* prefix = "      ")
{
  // If this has contexts, we need a switch between all the different
  // contexts
  NUCModelInterface* interface = cmodel->getCModelInterface();
  bool hasContext = interface->hasContextStrings();
  if (hasContext) {
    out << prefix << "switch(context) {" << ENDL;
  }

  // Loop the c-model calls and emit the copies
  for (NUCModelCallVectorLoop l = gCodeGen->loopCModelCalls(cmodel);
       !l.atEnd(); ++l) {
    // If this is a context based, emit the switch case
    const NUCModelCall* call = *l;
    if (hasContext) {
      out << prefix << "case " << call->getContext() << ":" << ENDL;
    }

    // Emit the code to copy the inputs and outputs
    UInt32 outIndex = 0;
    for (NUCModelArgConnectionCLoop a = call->loopArgConnections();
         !a.atEnd(); ++a) {
      NUCModelArgConnection* arg = *a;
      UInt32 words;
      const NUCModelPort* port = arg->getFormal();
      UtString portName;
      getCModelPortName(port, &portName);
      switch(arg->getDir()) {
        case eInput:
          break;

        case eOutput:
        {
          NUCModelArgConnectionOutput* output = arg->castOutput();
          words = (output->getActual()->getBitSize() + 31) / 32;
          out << prefix << "  memcpy(" << portName
              << ", &outputs[" << outIndex << "], "
              << words * sizeof(UInt32) << ");" << ENDL;
          outIndex += words;
          break;
        }

        case eBid:
          NU_ASSERT(arg->getDir() != eBid, arg);
          break;
      }
    } // !a.atEnd

    // Emit the end of the case item if we have contexts
    if (hasContext) {
      out << prefix << "  break;" << ENDL;
    }
  } // !l.atEnd

  // Emit the end of the case statement if this has contexts
  if (hasContext) {
    // Also emit a default in case any contexts get optimized away
    out << prefix << "  default:\n"
        << prefix << "    break;\n"
        << prefix << "}" << ENDL;
  }
} // emitCopyArraysToOutputs

// Helper function to emit a function prototype parameters for a given
// NUCModelInterface. This allows the various functions with the same
// prototype to be emitted in one place.
//
// The resulting code looks like:
//
//          (void* hndl, CDSsubContext context
//		, CarbonUInt32* _out1 // Output, size = 1 word(s)
//		, CarbonUInt32* _out2 // Output, size = 1 word(s)
//		, CarbonUInt32* _out3 // Output, size = 1 word(s)
//		, const CarbonUInt32* _clk // Input, size = 1 word(s)
//		, const CarbonUInt32* _a // Input, size = 1 word(s)
//		, const CarbonUInt32* _b // Input, size = 1 word(s)
//          )
static void
emitRunParams(UtOStream& out, const NUCModelInterface* interface,
              bool printNames = true)
{
  // Make various names we will use below
  UtString contextType;
  const char* name = interface->getName()->str();
  makeVarName(eCMContextType, name, contextType);

  // Emit the c-model common arguments
  out << "(void* ";
  if (printNames) {
    out << "hndl";
  }
  if (interface->hasContextStrings()) {
    out << ", " << contextType;
    if (printNames) {
      out << " context";
    }
  }
  out << ENDL;

  // Now the variable arguments - not sure how to pass the new hout
  // variable to emitCodeList so I am doing it inline.
  for (NUCModelInterface::PortsCLoopNoNull l = interface->loopPortsNoNull();
       !l.atEnd(); ++l) {
    // Print the type of the port
    const NUCModelPort* port = *l;
    PortDirectionT dir = port->getDirection();
    UInt32 size = port->getMaxSize();
    out << "\t\t, ";
    if (dir == eInput)
      out << "const ";
    out << "CarbonUInt32* ";

    // Print the name of the port
    if (printNames) {
      UtString portName;
      out << getCModelPortName(port, &portName);
    }

    // Print the direction in the comment
    if (dir == eInput)
      out << " // Input";
    else if (dir == eOutput)
      out << " // Output";
    else
      NU_ASSERT("Unsupported direction type" == NULL, interface);

    // Print the size in the comment as well
    out << ", size = " << size << " word(s)" << ENDL;
  }
  out << "\t)";
} // emitRunParams

// Helper function to emit the arguments to a c-model prototype
// function. This occurs when we are passing the parameters emitted
// with emitRunParams to call another function with the same
// prototype.
//
// The emitted code looks like:
//
//  (userData, context, _out1, _out2, _out3, _clk, _a, _b)
//
static void
emitRunArgs(UtOStream& out, const NUCModelInterface* interface, 
            const char* contextStr = "context")
{
  // Emit the c-model common arguments
  out << TAG << "(userData";
  if (interface->hasContextStrings())
    out << ", " << contextStr;

  // Now the variable arguments - not sure how to pass the new hout
  // variable to emitCodeList so I am doing it inline.
  for (NUCModelInterface::PortsCLoopNoNull l = interface->loopPortsNoNull();
       !l.atEnd(); ++l) {
    // Print the name of the port
    const NUCModelPort* port = *l;
    UtString portName;
    out << ", ";
    out << getCModelPortName(port, &portName);
  }
  out << ")";
}

// Helper function to get the real user data. If this is a
// cCallOnPlayback cmodel then we have wrapped the user data. 
// This has some overhead so only use it in misc type functions.
static void
emitGetUserData(UtOStream& out, const UtString& hndlName,
                bool callOnPlayback)
{
  out << "  void* userData = " << hndlName << ";\n";
  if (callOnPlayback) {
    out << CRYPT("  CarbonReplayCModelData* data = (CarbonReplayCModelData*)")
        << hndlName << ";\n"
        << CRYPT("  if (data) {\n")
        << "    userData = carbonInterfaceGetReplayCModelUserData(data);\n"
        << "}\n";
  }
}

CGContext_t NUCModel::emitCode(CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  NUCModelInterface* cmodelInterface = getCModelInterface();
  const char* cmodName = cmodelInterface->getName()->str();
  const char* name = getName()->c_str();
  UtOStream& out = gCodeGen->CGOUT ();
  bool callOnPlayback = emitUtil::isCallOnPlaybackCModel(this);

  if (isConstructor(context))
  {
    // Emit the call to c-model constructor
    UtString objName;
    makeVarName(eCMObject, name, objName);
    out << "  " << TAG << objName << "()";
  }
  else if (isDeclare(context))
  {
    // Make the various names we will need below
    UtString className;
    makeVarName(eCMSubClass, name, className);
    UtString objName;
    makeVarName(eCMObject, name, objName);
    UtString hndlName;
    makeVarName(eCMHandle, cmodName, hndlName);
    UtString runFnPtr;
    makeVarName(eCMRunFnPtr, cmodName, runFnPtr);
    UtString run;
    makeVarName(eCMRun, cmodName, run);
    
    // Emit the sub-class for this c-model. Start with the class header
    out << "  class " << className << ENDL;
    out << "  {" << ENDL;
    out << "  public:" << ENDL;

    // Emit the constructor prototype
    out << "    " << className << "();\n";

    // Emit the destructor prototype
    out << "    ~" << className << "();\n";

    // Emit the create function prototype
    out << "    void create(const char* instName);" << ENDL << "\n";

    // Emit the misc function prototype
    out << "    void misc(CarbonCModelReason reason, void* cmodelData);" << ENDL << "\n";

    // If there are c-model calls that can be called during playback,
    // emit the replay logic associated with them.
    //
    // TBD - Why do we emit a cmodel class if the c-model got
    // optimized away? I thought we got rid of them?

    // Emit function prototypes to handle checkpoint save/restore.
    if (gCodeGen->mDoCheckpoint) {
      out << "    void save(CarbonObjectID *descr);" << ENDL << "\n";
      out << "    void restore(CarbonObjectID *descr);" << ENDL << "\n";
    }

    if (callOnPlayback) {
      // Emit the playback function prototypes
      for (NUCModelCallVectorLoop l = gCodeGen->loopCModelCalls(this);
           !l.atEnd(); ++l) {
        // Get the context, PLI tasks don't have one so use 0 since that is
        // what is passed to the register function.
        NUCModelCall* call = *l;
        const char* contextStr = call->getContext();
        if (contextStr == NULL) {
          contextStr = "0";
        }

        // Emit the playback function prototype
        out << "    static void sPlaybackRun" << contextStr
            << "(void* userData, UInt32* inputs, UInt32* outputs);" << ENDL << "\n";
      }

      // Emit the record function prototype
      out << "    static void recordRun";
      emitRunParams(out, cmodelInterface);
      out << ";" << ENDL << "\n";

      // Emit the recover function prototype
      out << "    static void recoverRun";
      emitRunParams(out, cmodelInterface);
      out << ";" << ENDL << "\n";

      // emit the normal run function wrapper
      out << "    static void normalRun";
      emitRunParams(out, cmodelInterface);
      out << ";" << ENDL << "\n";

      // Emit the do nothing function (during playback we bypass the call)
      out << "    static void doNothingRun";
      emitRunParams(out, cmodelInterface, false);
      out << " {}" << ENDL << "\n";

      if (gCodeGen->isOnDemand()) {
        // Emit the onDemand function prototype
        out << "    static void onDemandRun";
        emitRunParams(out, cmodelInterface);
        out << ";" << ENDL << "\n";
      }
    } // if

    // Emit a function prototype to change the current mode
    out << "    void modeChange(CarbonObjectID* descr, void*, CarbonVHMMode from, CarbonVHMMode to);" << ENDL << "\n";

    // Emit a function prototype to register the c-model with the Carbon model
    out << "    void registerCModel(const char* name, CarbonObjectID* descr);" << ENDL << "\n";

    // Emit function prototypes to alloc/free the onDemand/replay cmodel data
    sEmitAllocCModelData(out, context, 0 /* unused */, 0 /* unused */, 0 /* unused */, 0 /* unused */);
    sEmitFreeCModelData(out, context, 0 /* unused */, 0 /* unused */);

    // Emit the handle storage and class end
    out << "    void* " << hndlName << ";" << ENDL;
    out << "    void (*" << runFnPtr << ")";
    emitRunParams(out, cmodelInterface);
    out << ";" << ENDL;
    out << "  };" << ENDL;

    // Emit storage for the c-model object
    out << "  " << className << " " << objName << ";" << ENDL << "\n";
    gCodeGen->allocation(ptrSize() * 2, ptrSize(), 1);

  } else if (isDefine(context)) {
    // Emit the c-model functions in the .cxx file
    UtOStream &out = gCodeGen->switchStream (eCGDefine);
    bool callOnPlayback = emitUtil::isCallOnPlaybackCModel(this);
    const NUModule* mod = gCodeGen->getCurrentModule();

    // Make the various names we will need below
    UtString className;
    makeVarName(eCMSubClass, name, className);
    UtString run;
    makeVarName(eCMRun, cmodName, run);
    UtString miscName;
    makeVarName(eCMMisc, cmodName, miscName);
    UtString hndlName;
    makeVarName(eCMHandle, cmodName, hndlName);
    UtString createName;
    makeVarName(eCMCreate, cmodName, createName);
    UtString runFnPtr;
    makeVarName(eCMRunFnPtr, cmodName, runFnPtr);
    UtString destroy;
    makeVarName(eCMDestroy, cmodName, destroy);

    // Emit the constructor which initializes the handle and run function
    out << "    " << mclass(mod->getName()) << "::" << className << "::"
        << className << "()\n"
	<< "    {\n"
	<< "      " << hndlName << " = NULL;\n"
        << "      " << runFnPtr << " = ";
    if (callOnPlayback)
      out << "normalRun";
    else
      out << run;
    out << ";\n" << ENDL;
    out << "    }" << ENDL;

    // Emit the c-model destructor
    out << "    " << mclass(mod->getName()) << "::" << className << "::~"
        << className << "()\n"
        << "    {\n";

    if (callOnPlayback) {
      // Free the onDemand/replay data and restore the user's pointer
      out << "      freeCModelData();\n";
    }

    out << "      " << destroy << "(" << hndlName << ");\n"
        << "    }" << ENDL << "\n";

    // Emit the create routine which calls the c-model create function
    out << "    void " << mclass(mod->getName()) << "::" << className
        << "::create(const char* instName)\n"
	<< "    {" << ENDL;

    // Emit the parameters if there are any
    UInt32 numParams = getNumParams();
    if (numParams > 0)
    {
      out << "      CModelParam params[" << numParams << "] = ";
      const char* separator = "{ ";
      for (ParamLoop l = loopParams(); !l.atEnd(); ++l)
      {
	Param* param = *l;
	out << separator << "{ \"" << param->first.c_str() << "\", \""
	    << param->second.c_str() << "\" }";
	separator = ",\n         ";
      }
      out << " };" << ENDL;
    }

    // Emit the call to the create routine
    out << "      " << hndlName << " = " << createName << "("
	<< numParams << ", ";
    if (numParams == 0)
      out << "0";
    else
      out << "params";
    out << ", instName);" << ENDL;

    if (callOnPlayback)
      // allocate the cmodel data wrapper. The model is not yet
      // available, so we pass in NULL. It will be given the model
      // upon registration.
      out << "      allocCModelData(NULL);\n";

    // Emit the end of the create routine
    out << "    }" << ENDL << "\n";

    // Emit the misc function
    out << "    void " << mclass(mod->getName()) << "::" << className
        << "::misc(CarbonCModelReason reason, void* cmodelData)\n"
	<< "    {\n";
    emitGetUserData(out, hndlName, callOnPlayback);
    out << "      " << miscName << "(userData, reason, cmodelData);\n"
	<< "    }" << ENDL << "\n";

    // Emit code to handle checkpoint save/restore.
    // If this cmodel is called on playback do not run save/restore
    // during checkpoint events.
    if (gCodeGen->mDoCheckpoint)
    {
      out << "    void " << mclass(mod->getName()) << "::" << className
          << "::save(CarbonObjectID *descr)\n"
          << "    {\n";
      if (callOnPlayback) {
        out << TAG;
        out << "      if (! carbonPrivateIsCheckpointEvent(descr)) {\n" 
            << "  ";
      }
      emitGetUserData(out, hndlName, callOnPlayback);
      out << "      " << miscName
          << "(userData, eCarbonCModelSave, (void *) descr);" << ENDL;
      if (callOnPlayback) {
        out << "      }" << ENDL;
      }
      out << "    }" << ENDL << "\n";
      out << "    void " << mclass(mod->getName()) << "::" << className
          << "::restore(CarbonObjectID *descr)\n"
          << "    {\n";
      if (callOnPlayback)
        out << "      if (! carbonPrivateIsCheckpointEvent(descr)) {\n" 
            << "  ";
      emitGetUserData(out, hndlName, callOnPlayback);
      out << "      " << miscName
          << "(userData, eCarbonCModelRestore, (void *) descr);" << ENDL;
      if (callOnPlayback)
        out << "      }" << ENDL;
      out << "    }" << ENDL << "\n";
    }

    // If this c-model has contexts, use that below, otherwise use 0
    const char* contextParam;
    if (cmodelInterface->hasContextStrings()) {
      contextParam = "(UInt32)context";
    } else {
      contextParam = "0";
    }

    if (callOnPlayback) {
      // Emit the playback functions
      for (NUCModelCallVectorLoop l = gCodeGen->loopCModelCalls(this);
           !l.atEnd(); ++l) {
        // Get the context, PLI tasks don't have one so use 0 since that is
        // what is passed to the register function.
        NUCModelCall* call = *l;
        const char* contextStr = call->getContext();
        if (contextStr == NULL) {
          contextStr = "0";
        }

        // Emit the playback function
        out << "  void " << mclass(mod->getName()) << "::" << className
            << "::sPlaybackRun" << contextStr
            << "(void* userData, UInt32* inputs UNUSED, UInt32* outputs UNUSED)" << ENDL
            << "  {\n"
            << "    // Make the arguments point into the playback data" << ENDL;
        emitArgsDeclarations(out, call);
        out << ENDL
            << "    // Call the c-model\n"
            << "    " << run;
        emitRunArgs(out, cmodelInterface, contextStr);
        out << ";\n"
            << "  }" << ENDL << "\n";
      }

      // Emit the record function
      out << "  " << TAG << "void " << mclass(mod->getName()) << "::" << className
          << "::recordRun";
      emitRunParams(out, cmodelInterface);
      out << ENDL
          << "  {\n"
          << "    // Call the user run function\n"
          << "    CarbonReplayCModelData* data = (CarbonReplayCModelData*)hndl;\n"
          << "    UInt32* inputs;\n"
          << "    UInt32* outputs;\n"
          << "    CarbonModel* model;\n"
          << "    void* cmodel;\n"
          << "    void* userData;\n"
          << "    carbonInterfaceReplayCModelDataGetMembers(data, &model, &userData, &cmodel, &inputs, &outputs);\n"
          << "    // send the outputs to the model before we call run\n";
      emitCopyArgsToArrays(out, this, gCodeGen, "      ");
      out << "    carbonInterfacePreRecordCModel(model, cmodel, "
          << contextParam << ", outputs);\n"
          << "    " << run;
      emitRunArgs(out, cmodelInterface);
      out << ";" << ENDL << "\n"
          << "    // Convert the input/output data to an array\n";
      emitCopyArgsToArrays(out, this, gCodeGen, "      ");
      out << "\n"
          << "    // Tell the replay system about the data\n"
          << "    carbonInterfaceRecordCModel(model, cmodel, "
          << contextParam << ", inputs, outputs);\n"
          << "  }" << ENDL << "\n";
      
      // Emit the recover function
      out << "  void " << mclass(mod->getName()) << "::" << className
          << "::recoverRun";
      emitRunParams(out, cmodelInterface);
      out << ENDL
          << "  {\n"
          << "    // Get the data from the Replay database to stimulate the Carbon Model\n"
          << "    UInt32* outputs;\n"
          << "    CarbonReplayCModelData* data = (CarbonReplayCModelData*)hndl;\n"
          << "    CarbonModel* model;\n"
          << "    void* cmodel;\n"
          << "    void* userData;\n"
          << "    carbonInterfaceReplayCModelDataGetMembers(data, &model, &userData, &cmodel, NULL, NULL);\n"
          << "    bool status;\n"
          << "    status = carbonInterfaceGetCModelValues(model, cmodel, "
          << contextParam << ", &outputs);\n\n"
          << "    // Should we run the c-model or use the record values?\n"
          << "    if (status) {\n";
      emitCopyArraysToOutputs(out, this, gCodeGen, "        ");
      out << "    } else {\n"
          << "      " << run;
      emitRunArgs(out, cmodelInterface);
      out << ";\n"
          << "    }\n"
          << "  }\n\n";

      // Emit the normal function (wraps the cmodel normal function)
      out << "  void " << mclass(mod->getName()) << "::" << className
          << "::normalRun";
      emitRunParams(out, cmodelInterface);
      out << ENDL
          << "  {\n"
          << "    CarbonReplayCModelData* data = (CarbonReplayCModelData*)hndl;\n"
          << "      void* userData = carbonInterfaceGetReplayCModelUserData(data);\n"
          << "      " << run;
      emitRunArgs(out, cmodelInterface);
      out << ";\n"
          << "  }\n\n";

      if (gCodeGen->isOnDemand()) {
        // Emit the onDemand function
        out << "  " << TAG << "void " << mclass(mod->getName()) << "::" << className
            << "::onDemandRun";
        emitRunParams(out, cmodelInterface);
        out << ENDL
            << "  {\n"
            << "    // Either run the normal or recovery function\n"
            << "    // Don't extract the true user data, because these functions do\n"
            << "    CarbonReplayCModelData* data = (CarbonReplayCModelData*)hndl;\n"
            << "    void* userData = hndl;\n"
            << "    if (carbonInterfaceOnDemandDivergentCModel(data))\n"
            << "      recoverRun";
        emitRunArgs(out, cmodelInterface);
        out << ";\n"
            << "    else\n"
            << "      recordRun";
        emitRunArgs(out, cmodelInterface);
        out << ";\n"
            << "  }" << ENDL << "\n";
      }
    } // if

    // Emit a function to change the current mode
    // If there are no calls, we emit an empty function. Currently
    // this is easier than modifying the code in CodeCModel.cxx to
    // detect that there are no c-model calls.
    out << "  void " << mclass(mod->getName()) << "::" << className
        << "::modeChange(CarbonObjectID* descr UNUSED, void*, CarbonVHMMode from UNUSED, CarbonVHMMode to UNUSED)\n"
        << "    {" << ENDL;
    if (callOnPlayback) {
      out << TAG;
      out << "    switch(to) {\n"
          << "    case eCarbonRunRecord:\n"
          << "      m_run = recordRun;\n";
      out << "      break;\n\n"
          << "    case eCarbonRunNormal:\n";
      // Switch to the onDemand function (if enabled) when replay goes
      // to normal mode.
      if (gCodeGen->isOnDemand()) {
        out << "      m_run = onDemandRun;\n";
      } else {
        out << "      m_run = normalRun;\n";
      }
      out << "      break;\n\n"
          << "    case eCarbonRunRecover:\n"
          << "      m_run = recoverRun;\n";
      out << "      break;\n\n"
          << "    case eCarbonRunPlayback:\n"
          << "      m_run = doNothingRun;\n"
          << "      break;\n"
          << "    }" << ENDL;
    } // if
    out << "  }" << ENDL << "\n";
    
    // Emit code to register the c-model with the Carbon model
    // If there are no calls, we emit an empty function. Currently
    // this is easier than modifying the code in CodeCModel.cxx to
    // detect that there are no c-model calls.
    out << "  void " << mclass(mod->getName()) << "::" << className
        << "::registerCModel(const char* name UNUSED, CarbonObjectID* descr UNUSED)\n"
        << "  {" << ENDL;
    if (callOnPlayback) {

      // Emit code to get the actual user data.
      UtString hndlName("m_hndl");
      emitGetUserData(out, hndlName, true);
      
      for (NUCModelCallVectorLoop l = gCodeGen->loopCModelCalls(this);
           !l.atEnd(); ++l) {
        // Get the context, PLI tasks don't have one so use 0 since that is
        // what is passed to the register function.
        NUCModelCall* call = *l;
        const char* contextStr = call->getContext();
        if (contextStr == NULL) {
          contextStr = "0";
        }

        // Emit the register function call.
        out << "    carbonPrivateRegisterCModel(descr, (void*)this, userData, (UInt32)" << contextStr
            << ", name, " << call->inputsWordCount()
            << ", " << call->outputsWordCount()
            << ", sPlaybackRun" << contextStr
            << ");" << ENDL;
        out << "    carbonInterfacePutReplayCModelDataModel(data, descr);" << ENDL;
      }
      
      // Emit the code to allow onDemand to record inputs/outputs
      if (gCodeGen->isOnDemand()) {
        // onDemand uses the same function as replay record mode to
        // save inputs/outputs.
        //
        // We only want to replace the normal cmodel run
        // function with the onDemand version.  If replay is enabled
        // and playback is enabled before the initial schedule is run,
        // replay will have installed the playback "do nothing"
        // function, and we don't want to disturb that.
        out << "    if (m_run == normalRun)\n"
            << "      m_run = onDemandRun;\n";
      }
    } // if
    out << "  }" << ENDL << "\n";

    if (callOnPlayback) {
      // Compute the maximum number of inputs and outputs for all
      // variants of the c-model. We need to preallocate space for
      // recording.
      UInt32 maxInputWords = 0;
      UInt32 maxOutputWords = 0;
      for (NUCModelCallVectorLoop l = gCodeGen->loopCModelCalls(this);
           !l.atEnd(); ++l) {
        NUCModelCall* call = *l;
        UInt32 numOutputWords = call->outputsWordCount();
        UInt32 numInputWords = call->inputsWordCount();
        maxInputWords = std::max(maxInputWords, numInputWords);
        maxOutputWords = std::max(maxOutputWords, numOutputWords);
      }

      // Emit functions to alloc/free the onDemand/replay cmodel data
      sEmitAllocCModelData(out, context, mod, &className, maxInputWords, maxOutputWords);
      sEmitFreeCModelData(out, context, mod, &className);
    }

  } else if (isNameOnly(context)) {
    // Emit the header file name
    UtString hdrName;
    makeHeaderFile(cmodName, hdrName);
    out << hdrName;
  }
  return eCGVoid;
}

// TBD
CGContext_t
NUCModelInterface::emitCode (CGContext_t /* context */) const
{
  {
    // Create the header file for this c-model
    UtString hdrName;
    const char* name = getName()->str();
    makeHeaderFile(name, hdrName);
    UtOStream* hout;
    hout = gCodeGen->CGFileOpen(hdrName.c_str(),
                                "/* C/C++ interface to cmodel. */\n",
                                gCodeGen->isObfuscating(),
                                true /* needCleartext */);

    if (not hout)
      return eCGVoid;           // already error'ed

    // Make various names we will use below
    UtString contextType;
    makeVarName(eCMContextType, name, contextType);
    UtString run;
    makeVarName(eCMRun, name, run);
    UtString create;
    makeVarName(eCMCreate, name, create);
    UtString misc;
    makeVarName(eCMMisc, name, misc);
    UtString destroy;
    makeVarName(eCMDestroy, name, destroy);

    // Make sure we don't create duplicate definitions
    *hout << "#ifndef __CDS_CM_" << name << "_H_" << ENDL;
    *hout << "#define __CDS_CM_" << name << "_H_" << ENDL;

    // Need to include the shell types 
    *hout << "#include \"carbon/carbon_shelltypes.h\"" << ENDL << "\n";

    // Emit the context enum type if there is one
    if (hasContextStrings())
    {
      *hout << "\ntypedef enum " << contextType << "{" << ENDL;
      for (ContextStringsCLoop l = loopContextStrings(); !l.atEnd(); ++l)
      {
        const char* context = (*l)->c_str();
        *hout << "  " << context << "," << ENDL;
      }
      *hout << "} " << contextType << ";" << ENDL << "\n";
    }

    // Emit the create function type
    *hout << "extern __C__ void* " << create
          << "(int numParams, CModelParam* cmodelParams, const char* inst);" << ENDL;

    // Emit the misc function type
    *hout << "extern __C__ void " << misc
          << "(void* hndl, CarbonCModelReason reason, void* cmodelData);" << ENDL;

    // Emit the run function type
    *hout << "extern __C__ void " << run;
    emitRunParams(*hout, this);
    *hout << ";" << ENDL << "\n";

    // Emit the destroy function type
    *hout << "extern __C__ void " << destroy << "(void* hndl);" << ENDL;

    // The end of the #ifndef
    *hout << "#endif // __CDS_CM_" << name << "_H_ " << ENDL;

    // Close the file by destructing it
    delete hout;
  }
  return eCGVoid;
}

CGContext_t NUCModelFn::emitCode(CGContext_t /* context */) const
{
  NU_ASSERT("Not supported" == 0, this);
  return eCGVoid;
}


static CodeGen::CxxName cModelHandle("cmodel_", "handle");
static CodeGen::CxxName cModelParam("cmodel_", "param");

void
CodeGen::codeCModelWrapper(NUDesign* design)
{
  // Get the top module, there should only be one
  NUModule* module = NULL;
  for (NUDesign::ModuleLoop l = design->loopTopLevelModules();
       !l.atEnd(); ++l)
  {
    NU_ASSERT(module == NULL, module);
    module = *l;
  }

  StringAtom* rawModuleSym = module->getName();
  StringAtom* validatedSym = mInterfaceCodeNames->translate(rawModuleSym, true);
  const char * rawModuleName = rawModuleSym->str(); // may have be escaped
  const char * moduleName = validatedSym->str(); // legal C identifier
  NUNetList netList;
  module->getPorts(&netList);

  // Open a file in build dir named cmodel_wrapper.cxx to hold the wrapper code
  UtOStream* cout = CGFileOpen("cmodel_wrapper.cxx", "", 
			       gCodeGen->isObfuscating(),
			       true /* needCleartext */);  

  
  // Emit header comment
  *cout << "/* C-model wrapper for Carbon model "
        << rawModuleName << " */" << ENDL << "\n";

  // We could #include libXXX.h and cds_YYY_ZZZ.h here, but YYY is not
  // known, and XXX needed only for an unused enum.  For now, just generate 
  // the extern defs locally so we can compile w/o depencendies.
  *cout << 
    "#include <stdlib.h>\n"
    "#include \"carbon/carbon_capi.h\"\n\n"
    "extern \"C\" {\n"
    "CarbonObjectID* carbon_" << mIfaceTag 
        <<"_create(CarbonDBType dbType, CarbonInitFlags flags);" << ENDL << "\n";

  // Name of data type for c-model data.
  UtString dataTypeName("struct cmodel_");
  dataTypeName << moduleName << "_data";

  // Name of local data pointer for c-model data
  UtString dataName("_cmodel_");
  dataName << moduleName << "_data";

  // Emit a struct to hold the handles to the Carbon model and its ports
  *cout << dataTypeName << "\n{\n"
    "  CarbonObjectID* parent;\n"
    "  CarbonObjectID* descr;" << ENDL;
  for (NUNetList::iterator p = netList.begin(); p != netList.end(); ++p)
  {
    NUNet* net = *p;
    if (!net->isBid())
      *cout << "  CarbonNetID* " << cModelHandle(net) << ";" << ENDL;
  }
  *cout << "};" << ENDL << "\n";

  // Emit 'create' function
  *cout << TAG
        << "void* cds_" << moduleName << "_create(\n"
    "        int numParams, \n"
    "        CModelParam* cmodelParams,\n"
    "        const char* inst)\n"
    "{\n"
    "  " << dataTypeName << "* " << dataName << " = (" << dataTypeName << "*) "
    "malloc(sizeof(" << dataTypeName << "));\n"
    "\n"
    "  /* reference unused parameters to silence compiler warnings */\n"
    "  (void) numParams;\n"
    "  (void) cmodelParams;\n"
    "  (void) inst;\n"
    "\n"
    "  if (" << dataName << " != NULL)\n"
    "  {\n"
    "    " << dataName << "->parent = NULL;\n"
    "    " << dataName << "->descr = carbon_" << mIfaceTag << "_create(eCarbonFullDB, eCarbon_NoFlags);" 
        << ENDL;
  for (NUNetList::iterator p = netList.begin(); p != netList.end(); ++p)
  {
    NUNet* net = *p;
    UtString cNet;
    StringUtil::escapeCString(net->getName()->str(), &cNet);
    if (!net->isBid())
      *cout << "    " << dataName << "->" << cModelHandle(net) 
            << " = carbonFindNet(" << dataName << "->descr, \"" 
            << moduleName << "." << cNet <<"\");" << ENDL;
  }
  *cout <<
    "  }\n"
    "  return (void *) " << dataName << ";\n"
    "}" << ENDL << "\n";

  // Emit 'misc' function
  *cout << TAG <<
    "void cds_" << moduleName << "_misc(\n"
    "        void* hndl,\n"
    "        CarbonCModelReason reason,\n"
    "        void* cmodelData)\n"
    "{\n"
    "  if (reason == eCarbonCModelID && hndl != NULL)\n"
    "  {\n"
    "    " << dataTypeName << "* " << dataName << " = (" << dataTypeName << " *) hndl;\n"
    "    " << dataName << "->parent = (CarbonObjectID *) cmodelData;\n"
    "  }\n"
    "}" << ENDL << "\n";

  // Emit 'run' function declaration.
  *cout << TAG << "void cds_" << moduleName << "_run(\n"
    "        void* hndl,\n"
    "        int /* CDS" << moduleName << "Context */ context";

  // Generate a parameter for each port 
  for (NUNetList::iterator p = netList.begin(); p != netList.end(); ++p)
  {
    NUNet* net = *p;
    *cout << "," << ENDL << "        ";
    if (net->isInput())
      *cout << "const ";
    *cout << "CarbonUInt32* " << cModelParam(net);
    if (net->isBid())
      *cout << " /* WARNING: inout port not supported! */";
  } // for

  *cout << ")\n{\n"
    "  /* reference unused parameters to silence compiler warnings */\n"
    "  (void) context;" << ENDL << "\n";

  // Since inout ports are not used, generate a (void) reference to each 
  // one to avoid compiler warnings.
  for (NUNetList::iterator p = netList.begin(); p != netList.end(); ++p)
  {
    NUNet* net = *p;
    if (net->isBid())
      *cout << "  (void) " << cModelParam(net) << ";" << ENDL;
  } // for

  // Emit body of 'run' function.
  *cout << 
    "  " << dataTypeName << "* " << dataName << " = (" << dataTypeName << " *) hndl;\n"
    "  if (" << dataName << " != NULL)\n"
    "  {\n"
    "    CarbonTime time = carbonGetSimulationTime(" << dataName << "->parent);" << ENDL;
  
  // For each input, deposit the new value
  for (NUDesign::PortLoop p = design->loopInputPorts(); !p.atEnd(); ++p)
  {
    NUNet* net = *p;
    *cout << "    carbonDeposit(" << dataName << "->descr, " 
          << dataName << "->" << cModelHandle(net) << ", " 
          << cModelParam(net) << ", 0);" << ENDL;
  }

  // Call schedule to run the model
  *cout << "    carbonSchedule(" << dataName << "->descr, time);" << ENDL;

  // For each output, fetch the value and set the output parameter
  for (NUDesign::PortLoop p = design->loopOutputPorts(); !p.atEnd(); ++p)
  {
    NUNet* net = *p;
    *cout << "    carbonExamine(" << dataName << "->descr, " 
          << dataName << "->" << cModelHandle(net) << ", " 
          << cModelParam(net) << ", NULL);" << ENDL;
  }

  *cout << "  }\n}" << ENDL << "\n";

  // Emit 'destroy' function
  *cout << TAG <<
    "void cds_" << moduleName << "_destroy(void* hndl)\n"
    "{\n"
    "  " << dataTypeName << "* " << dataName << " = (" << dataTypeName << " *) hndl;\n"
    "  if (" << dataName << " != NULL)\n"
    "  {\n"
    "    if (" << dataName << "->descr != NULL)\n"
    "    {\n"
    "      carbonDestroy(&" << dataName << "->descr);\n"
    "    }\n"
    "    free(" << dataName << ");\n"
    "  }\n"
    "}" << ENDL;

  *cout << "}" << ENDL;
  delete cout;

} // void CodeGen::codeCModelWrapper

