// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implement the pointer-based port interface code
  generation. Generates the port structure and initialization code.
*/

#include "codegen/codegen.h"
#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "codegen/CGPortIface.h"
#include "nucleus/NUDesign.h"
#include "iodb/IODBNucleus.h"
#include "util/UtHashSet.h"
#include "emitUtil.h"
#include "codegen/CGNetElabCount.h"

using namespace emitUtil;

void CodeGen::writeSimplePointerDeclare(UtOStream* out, 
                                        CGPortIfaceNet* ifaceNet, 
                                        SInt32 changeArrayIndex,
                                        const UtString& name)
{
  UInt32 nbits = ifaceNet->getWidth();
  bool isSigned = ifaceNet->isSigned();
  bool isReal = ifaceNet->isReal();
  // A net marked as depositable and observable is an output and an
  // input
  bool isConst = ifaceNet->isOutput() && ! ifaceNet->isInput();
  (*out) << "  ";
  if (isConst)
    (*out) << "const ";

  if (nbits > LLONG_BIT)
    (*out) << "CarbonUInt32* " << name << ";";
  else
    (*out) << CBaseTypeFmt (nbits, eCGIface, isSigned, isReal) << "* " << name << ";";

  if (changeArrayIndex > -1)
    (*out) << " \t/* " << changeArrayIndex << " */";

  (*out) << "\n";
}

void CodeGen::writePortInterfaceDeclare(UtOStream* out,
                                        CGPortIfaceNet* ifaceNet)
{
  UtString buf;

  // Specify whether or not this net runs schedule(s)
  SInt32 changeArrayIndex = -1;
  const NUNetElab* netElab = ifaceNet->getNetElab();
  if (mChangedMap->exists(netElab))
  {
    changeArrayIndex = mChangedMap->getIndex(netElab);
    ifaceNet->setChangeIndex(changeArrayIndex);
  }
  
  if (CGPortIface::sIsNetHandle(ifaceNet))
    return;
  
  // xdata
  mPortIface->getExternalValue(ifaceNet, &buf);
  writeSimplePointerDeclare(out, ifaceNet, changeArrayIndex, buf);
  
  if (ifaceNet->isForcible())
  {
    // forceMask
    mPortIface->getForceMask(ifaceNet, &buf);
    writeSimplePointerDeclare(out, ifaceNet, false, buf);
  }
  
  bool isBid = ifaceNet->isBid();
  bool isTri = ifaceNet->isTri();
  
  if (isBid || isTri)
  {
    // idrive
    mPortIface->getInternalDrive(ifaceNet, &buf);
    writeSimplePointerDeclare(out, ifaceNet, false, buf);
  }
  
  if (isBid)
  {
    // idata
    mPortIface->getInternalValue(ifaceNet, &buf);
    writeSimplePointerDeclare(out, ifaceNet, false, buf);
    
    // xdrive
    mPortIface->getExternalDrive(ifaceNet, &buf);
    writeSimplePointerDeclare(out, ifaceNet, changeArrayIndex, buf);
  }
}

void CodeGen::codePortInterface(UtOStream* portIFace)
{
  // Emit a structure containing the input and output values to pass
  // to the top-level scheduler.
  //
  (*portIFace) << "\n\n";
  (*portIFace) << "struct carbon_" << mIfaceTag << "_ports {\n  ";
  
  // Emit the inputs, outputs.
  putInterfaceMode();

  (*portIFace) << "\n  /* input change array */\n  CarbonChangeType* " 
               << mPortIface->getChangeArray() << ";\n";

  (*portIFace) << "\n  /* run combo */\n  CarbonUInt32* " 
               << mPortIface->getRunComboPtr() << ";\n";
  
  // WARNING: We are NOT declaring expressioned nets or forcible nets
  // currently. This is because we do not have support for them in the
  // systemc wrapper. Instead, we are actually using CarbonNetID* for
  // those in the wrapper.

  (*portIFace) << "\n  /* 'in' parameters */\n";
  // To avoid adding a signal marked as both scObserveSignal and
  // scDepositSignal twice, keep a set of 'in' ports and declare
  // 'out' ports only if they aren't in the set.
  UtHashSet<const STSymbolTableNode*> inParameterSet;
  CGPortIface::NetIter ports;
  for (ports = mPortIface->loopIns(); !ports.atEnd();  ++ports)
  {
    CGPortIfaceNet* ifaceNet = *ports;
    writePortInterfaceDeclare(portIFace, ifaceNet);
    // Remember that this net has been processed
    inParameterSet.insert(ifaceNet->getSymNode());
  }

  (*portIFace) << "\n  /* 'out' parameters */\n";
  for (ports = mPortIface->loopOuts(); !ports.atEnd();  ++ports)
  {
    CGPortIfaceNet* ifaceNet = *ports;
    // If not already added as an 'in'...
    if (inParameterSet.count(ifaceNet->getSymNode()) == 0)
      writePortInterfaceDeclare(portIFace, ifaceNet);
  }
  
  (*portIFace) << "\n  /* 'tristate out' parameters */\n";
  for (ports = mPortIface->loopTriOuts(); !ports.atEnd();  ++ports)
  {
    CGPortIfaceNet* ifaceNet = *ports;
    writePortInterfaceDeclare(portIFace, ifaceNet);
  }

  (*portIFace) << "\n  /* 'inout' parameters */\n";
  for (ports = mPortIface->loopBidis(); !ports.atEnd();  ++ports)
  {
    CGPortIfaceNet* ifaceNet = *ports;
    writePortInterfaceDeclare(portIFace, ifaceNet);
  }
  
  putInternalMode();
  
  (*portIFace) << "};\n\n";
  
  (*portIFace) << "EXTERNDEF void carbon_" << mIfaceTag << "_portstruct_init(struct carbon_" << mIfaceTag << "_ports*, CarbonObjectID *);\n";
}

void CodeGen::codePortObjVarInit(CGPortIfaceNet* net, const STSymbolTableNode* node, bool isConst, PortIfaceNetSet* visited)
{
  
  if (! visited->insertWithCheck(net))
    // have already seen this net
    return;
  
  CbuildSymTabBOM* symtabBOM = getCarbonContext()->getSymbolTableBOM();
  
  const CarbonExpr* mappedExpr = symtabBOM->getMappedExpr(const_cast<STSymbolTableNode*>(node));
  if (mappedExpr)
  {
    net->putExpression(mappedExpr);
    // At this point we know sIsNetHandle is true
    return;
  }
  
  // Don't bother declaring ports that are going to be circumvented by
  // using CarbonNetID*
  if (CGPortIface::sIsNetHandle(net))
    return;

  UtString varName(net->getMemberName());
  UtString ifaceVarName;
  
  const NUNetElab* netElab = net->getNetElab();
  const NUNet* netFormal = netElab->getNet();
  const NUNet* netActual = netElab->getStorageNet();
  
  bool typeCompatible = netActual == netFormal;
  if (! typeCompatible)
    typeCompatible = emitUtil::isTypeCompatible(netActual, netFormal);

  // Just crypting the variable name for the port structure in the
  // initialization code. The rest of the strings are already public.
  static const char* portObj = CRYPT("portObj");
  static const char* _xdata = "_xdata";
  static const char* _xdrive = "_xdrive";
  static const char* _idata = "_idata";
  static const char* _idrive = "_idrive";

  static const char* getStorage = CRYPT("getStorage");
  static const char* getForceMask = CRYPT("getForceMask");
  static const char* getUIntArray = CRYPT("getUIntArray");
  const char* getInternalPtrs = CRYPT("getInternalPtrs");

  UInt32 nbits = net->getWidth();
  bool isBidirect = net->isBid();
  bool isTristate = isBidirect || net->isTri();
  bool isForcible = net->isForcible();

  UtString buf;
  getGeneratedType(&buf, isTristate, isBidirect, net->isSigned(), nbits, false, net->isReal());
  UtOStream&	schedSrc= getCGOFiles()->getScheduleSrc();

  // String that casts the initialization to the lhs type.
  UtString rhsCast; // initialize to empty
  // used for bitvector casts. The lhs needs to be cast in that case,
  // since we only need the UIntArray underneath.
  UtString rhsType; 
  if (! typeCompatible)
  {
    rhsType << CBaseTypeFmt(netActual);
    rhsCast << "(";
    if (isConst)
      rhsCast << "const ";
    rhsCast << buf;
    if ((nbits > LLONG_BIT) && ! isForcible)
    {
      rhsCast << " & ";
      rhsType << " & ";
    }
    else
      rhsCast << "* ";

    rhsCast << ")";
  }

  if (isForcible)
  {
    schedSrc << "  ";

    if ((nbits > LLONG_BIT) || isTristate)
      schedSrc << "Forcible";
    else
      schedSrc << "ForciblePOD";
    schedSrc << "<" << buf;

    
    if ((nbits > LLONG_BIT) || isTristate)
    {
      UtString forceMask;
      getGeneratedType(&forceMask, false, false, false, nbits, false, false);
      schedSrc << ", " << forceMask;
    }
    
    mPortIface->getForceMask(net, &ifaceVarName);
    schedSrc << ">& " << varName << "_force = " << rhsCast << netElab << ";\n";
    schedSrc << "  " << portObj << "->" << ifaceVarName << " = " << varName << "_force." << getForceMask << "()";
    if (nbits > LLONG_BIT)
      schedSrc << "->" << getUIntArray << "()";
    schedSrc << ";\n";
    schedSrc << "  " << buf << "& " << varName << "_tmp = *" << varName << "_force." << getStorage << "();\n";
  }
  else if (isConst && ! isTristate && (nbits > LLONG_BIT))
    // If we are not going to write to it, declare it const. This will
    // circumvent problems where we are observing a constant.
    schedSrc << "  const ";
  else 
    schedSrc << "  ";
  
  
  if (nbits > LLONG_BIT)
  {
    if (isBidirect)
    {
      schedSrc << "CarbonUInt32 *" << varName << _idata << ", *" << varName << _xdrive << ";\n";
      schedSrc << "CarbonUInt32 *" << varName << _xdata << ", *" << varName << _idrive << ";\n";
    }
    else if (isTristate)
      schedSrc << "CarbonUInt32 *" << varName << _xdata << ", *" << varName << _idrive << ";\n";

    if (! isForcible)
    {
      if (typeCompatible)
        schedSrc << buf << "& "; 
      else
        // Make the tmp variable the same as the storage net's type
        // We are getting the UInt32 array in the bitvector anyway.
        schedSrc << rhsType;
      schedSrc << varName << "_tmp = " << netElab << ";\n";
    }
    
    if (isBidirect)
    {
      schedSrc << "  " << varName << "_tmp." << getInternalPtrs << "(&" << varName << _idata <<", &" << varName << _idrive << ", &" 
                    << varName << _xdata << ", &" << varName << _xdrive << ");\n";
      schedSrc << "  " << portObj << "->" << mPortIface->getExternalValue(net, &ifaceVarName) << " = " << varName << _xdata << ";\n";
      schedSrc << "  " << portObj << "->" << mPortIface->getInternalValue(net, &ifaceVarName) << " = " << varName << _idata << ";\n";
      schedSrc << "  " << portObj << "->" << mPortIface->getExternalDrive(net, &ifaceVarName) << " = " << varName << _xdrive << ";\n";
      schedSrc << "  " << portObj << "->" << mPortIface->getInternalDrive(net, &ifaceVarName) << " = " << varName << _idrive << ";\n";
    }
    else if (isTristate)
    {
      schedSrc << "  " << varName << "_tmp." << getInternalPtrs << "(&" << varName << _xdata << ", &" << varName << _idrive << ");\n";
      schedSrc << "  " << portObj << "->" << mPortIface->getExternalValue(net, &ifaceVarName) << " = " << varName << _xdata << ";\n";
      schedSrc << "  " << portObj << "->" << mPortIface->getInternalDrive(net, &ifaceVarName) << " = " << varName << _idrive << ";\n";
    }
    else
      schedSrc << "  " << portObj << "->" << mPortIface->getExternalValue(net, &ifaceVarName) << " = " << varName << "_tmp." << getUIntArray << "();\n";
  }
  else
  {
    bool writeTmpType = false;
    UtString simpleType;
    getGeneratedType(&simpleType, false, false, net->isSigned(), nbits, false, net->isReal());
    
    if (isBidirect)
    {
      schedSrc << simpleType << " *" << varName << _idata << ", *" << varName << _xdrive << ";\n";
      schedSrc << "  " << simpleType << " *" << varName << _xdata << ", *" << varName << _idrive << ";\n";
      writeTmpType = true;
    }
    else if (isTristate)
    {
      schedSrc << simpleType << " *" << varName << _xdata << ", *" << varName << _idrive << ";\n";
      writeTmpType = true;
    }

    if (writeTmpType && ! isForcible)
      schedSrc << "  " << buf << "& " << varName << "_tmp = " << rhsCast << netElab << ";\n";
    
    
    if (isBidirect)
    {
      schedSrc << "  " << varName << "_tmp." << getInternalPtrs << "(&" << varName << _idata <<", &" << varName << _idrive << ", &" 
                    << varName << _xdata << ", &" << varName << _xdrive << ");\n";
      
      schedSrc << "  " << portObj << "->" << mPortIface->getExternalValue(net, &ifaceVarName) << " = " << varName << _xdata << ";\n";
      schedSrc << "  " << portObj << "->" << mPortIface->getInternalValue(net, &ifaceVarName) << " = " << varName << _idata << ";\n";
      schedSrc << "  " << portObj << "->" << mPortIface->getExternalDrive(net, &ifaceVarName) << " = " << varName << _xdrive << ";\n";
      schedSrc << "  " << portObj << "->" << mPortIface->getInternalDrive(net, &ifaceVarName) << " = " << varName << _idrive << ";\n";
    }
    else if (isTristate)
    {
      schedSrc << "  " << varName << "_tmp." << getInternalPtrs << "(&" << varName << _xdata << ", &" << varName << _idrive << ");\n";
      schedSrc << "  " << portObj << "->" << mPortIface->getExternalValue(net, &ifaceVarName) << " = " << varName << _xdata << ";\n";
      schedSrc << "  " << portObj << "->" << mPortIface->getInternalDrive(net, &ifaceVarName) << " = " << varName << _idrive << ";\n";
    }
    else if (! isForcible)
      schedSrc << portObj << "->" << mPortIface->getExternalValue(net, &ifaceVarName) << " = " << rhsCast << " &" << netElab << ";\n";
    else
      schedSrc << "  " << portObj << "->" << mPortIface->getExternalValue(net, &ifaceVarName) << " = &" << varName << "_tmp;\n";
  }
}

/*
 * Returns the given set of flags, with eClock, eResets, e2State, e4State
 * added if approriate.
*/
static CGPortIfaceNet::Flag sGetIodbFlags(IODB *iodb, 
                                          const STSymbolTableNode* symNode,
                                          CGPortIfaceNet::Flag flags = CGPortIfaceNet::eNone)
{
  CGPortIfaceNet::Flag result = flags;

  if (iodb->isClock(symNode))
    result = CGPortIfaceNet::Flag(result | CGPortIfaceNet::eClock);

  if (iodb->isPosedgeReset(symNode) || iodb->isNegedgeReset(symNode))
    result = CGPortIfaceNet::Flag(result | CGPortIfaceNet::eReset);

  if (iodb->isWrapperPort2State(symNode))
    result = CGPortIfaceNet::Flag(result | CGPortIfaceNet::e2State);

  if (iodb->isWrapperPort4State(symNode))
    result = CGPortIfaceNet::Flag(result | CGPortIfaceNet::e4State);
  return result;
}

CGPortIfaceNet* CodeGen::addCGPortIfaceBidiOrOut(NUNetElab* netElab, const STAliasedLeafNode* symNode, bool useElabName)
{
  NUNet* net = netElab->getNet();
  CGPortIfaceNet* ifaceNet = NULL;
  // for now, treat all mapped expressions as bidis.
  CbuildSymTabBOM* symtabBOM = getCarbonContext()->getSymbolTableBOM();
  if (symtabBOM->isExprMappedNode(symNode) || isBidirect(net))
    ifaceNet = mPortIface->addBidi(netElab, symNode, useElabName, sGetIodbFlags(mIODB, symNode));
  else if (isTristate(net))
    ifaceNet = mPortIface->addTriOut(netElab, symNode, useElabName, sGetIodbFlags(mIODB, symNode));
  else
  {
    // inout port isn't always inout - shell/stateio test
    // external value
    ifaceNet = mPortIface->addOut(netElab, symNode, useElabName, sGetIodbFlags(mIODB, symNode));
  }
  return ifaceNet;
}

static bool sIsSupportedNet(const NUNet* net)
{
  return ! net->is2DAnything() && ! net->isRecordPort();
}

void CodeGen::codePortInterfaceInitialization()
{
  UtOStream&	schedSrc= getCGOFiles()->getScheduleSrc();

  schedSrc << "extern \"C\" void carbon_" << mIfaceTag << "_portstruct_init (struct carbon_" << mIfaceTag << "_ports* portObj, CarbonObjectID *descr) {\n";
  generateCarbonSimulation(schedSrc, mTopName);


  schedSrc << "  portObj->" << mPortIface->getChangeArray() << " = " << CRYPT("carbonPrivateGetChanged (descr);\n");

  schedSrc << "  portObj->" << mPortIface->getRunComboPtr() << " = " << CRYPT("carbonPrivateGetRunDepositComboSchedPointer (descr);\n");

  // don't declare or initialize a net more than once
  PortIfaceNetSet visited;
  
  // For ALL ports/deps/obs, use the given STAliasedLeafNode from the
  // iodb to keep naming consistent with the user's directives and
  // port declarations.

  for (CGPortIface::NetIter ports = mPortIface->loopIns(); ! ports.atEnd(); ++ports) {
    CGPortIfaceNet* ifaceNet = *ports;
    codePortObjVarInit(ifaceNet, ifaceNet->getSymNode(), false, &visited);
  }

  for (CGPortIface::NetIter ports = mPortIface->loopOuts(); ! ports.atEnd(); ++ports) {
    CGPortIfaceNet* ifaceNet = *ports;
    codePortObjVarInit(ifaceNet, ifaceNet->getSymNode(), true, &visited);
  }
  
  for (CGPortIface::NetIter ports = mPortIface->loopBidis(); ! ports.atEnd(); ++ports) {
    CGPortIfaceNet* ifaceNet = *ports;
    codePortObjVarInit(ifaceNet, ifaceNet->getSymNode(), false, &visited);
  }

  for (CGPortIface::NetIter ports = mPortIface->loopTriOuts(); ! ports.atEnd(); ++ports) {
    CGPortIfaceNet* ifaceNet = *ports;
    codePortObjVarInit(ifaceNet, ifaceNet->getSymNode(), false, &visited);
  }
  
  schedSrc << "}\n";
}

void CodeGen::populatePortInterface()
{
  INFO_ASSERT(mPortIface == NULL, "Port Interface is already initialized");
  mPortIface = new CGPortIface;

  // check to see if we are generating a systemC or PLI wrapper.
  bool generatingWrapper = mCarbonContext->doGenerateSystemCWrapper() or 
                           mCarbonContext->getArgs()->getBoolValue(CRYPT("-pliWrapper"));

  // For ALL ports/deps/obs, use the given STAliasedLeafNode from the
  // iodb to keep naming consistent with the user's directives and
  // port declarations.

  for (IODB::NameSetLoop ports = mIODB->loopInputs(); 
       ! ports.atEnd(); ++ports)
    {
      STSymbolTableNode* node = *ports;
      STAliasedLeafNode* symNode = node->castLeaf();
      if (symNode)
      {
        NUNetElab* netElab = NUNetElab::find(symNode);
        ST_ASSERT(netElab, symNode);
        NUNet* net = netElab->getNet();

        if (sIsSupportedNet(net)) {
          mPortIface->addIn(netElab, symNode, false, sGetIodbFlags(mIODB, node));
        }
        else if (generatingWrapper) {
          mMsgContext->CGPortInterfaceUnsupportedPrimaryPortType(net);
        }
      }
    }
  
  for (IODB::NameSetLoop ports = mIODB->loopOutputs(); 
       ! ports.atEnd(); ++ports)
    {
      STSymbolTableNode* node = *ports;
      STAliasedLeafNode* symNode = node->castLeaf();
      if (symNode)
      {
        NUNetElab* netElab = NUNetElab::find(symNode);
        ST_ASSERT(netElab, symNode);
        NUNet* net = netElab->getNet();

        if (sIsSupportedNet(net)) {
          if (isTristate(net))
            mPortIface->addTriOut(netElab, symNode, false, sGetIodbFlags(mIODB, node));
          else
            mPortIface->addOut(netElab, symNode, false, sGetIodbFlags(mIODB, node));
        }
        else if (generatingWrapper) {
          mMsgContext->CGPortInterfaceUnsupportedPrimaryPortType(net);
        }
      }
    }
  
  for (IODB::NameSetLoop ports = mIODB->loopBidis(); 
       ! ports.atEnd(); ++ports)
    {
      STSymbolTableNode* node = *ports;
      STAliasedLeafNode* symNode = node->castLeaf();
      if (symNode)
      {
        NUNetElab* netElab = NUNetElab::find(symNode);
        ST_ASSERT(netElab, symNode);
        NUNet* net = netElab->getNet();
          
        if (sIsSupportedNet(net)) {
          addCGPortIfaceBidiOrOut(netElab, symNode, false);
        }
        else if (generatingWrapper) {
          mMsgContext->CGPortInterfaceUnsupportedPrimaryPortType(net);
        }
      }
    }

  
  for (IODB::NameSetLoop l = mIODB->loopScDeposit(); ! l.atEnd(); ++l)
    {
      const STSymbolTableNode* node = *l;
      STAliasedLeafNode* requestedNode = const_cast<STAliasedLeafNode*>(node->castLeaf());
      NUNetElab* netElab = NUNetElab::find(node);
      ST_ASSERT(netElab, requestedNode);
      NUNet *storageNet = netElab->getStorageNet();
      
      if (requestedNode && ! mIODB->isPrimary(requestedNode))
      {
        CGPortIfaceNet* ifaceNet = NULL;
        
        // check that the net has not been optimized away before adding as a port
        if ((storageNet->getCGOp() != NULL) &&
            isIncludedInTypeDictionary(requestedNode->getStorage(), mIODB))
        {
          if (sIsSupportedNet(storageNet)) {
            if (isBidirect(storageNet))
              // net is both depositable and observable so we expose it as a bidirectional port.
              ifaceNet = mPortIface->addBidi(netElab, requestedNode, true, sGetIodbFlags(mIODB, node, CGPortIfaceNet::eDeposit));
            else
            {
              ifaceNet = mPortIface->addIn(netElab, requestedNode, true,
                                           sGetIodbFlags(mIODB, node,
                                                         CGPortIfaceNet::eDeposit));
              if (isTristate(storageNet))
                ifaceNet->addFlag(CGPortIfaceNet::eTri);
            }
          }
          else if (generatingWrapper) {
            mMsgContext->CGPortInterfaceUnsupportedSignalType(storageNet);
          }
        }
      }
    }
  
  for (IODB::NameSetLoop l = mIODB->loopScObserved(); ! l.atEnd(); ++l)
    {
      const STSymbolTableNode* node = *l;
      STAliasedLeafNode* requestedNode = const_cast<STAliasedLeafNode*>(node->castLeaf());
      NUNetElab* netElab = NUNetElab::find(node);
      ST_ASSERT(netElab, requestedNode);
      NUNet *storageNet = netElab->getStorageNet();

      if (requestedNode && ! mIODB->isPrimary(requestedNode))
      {
        // check that the net has not been optimized away before adding as a port
        if ((storageNet->getCGOp() != NULL) &&
            isIncludedInTypeDictionary(requestedNode->getStorage(), mIODB))
        {
          if (sIsSupportedNet(storageNet)) {
            CGPortIfaceNet* ifaceNet = addCGPortIfaceBidiOrOut(netElab, requestedNode, true);
            ifaceNet->addFlag(CGPortIfaceNet::eObserve);
          }
          else if (generatingWrapper) {
            mMsgContext->CGPortInterfaceUnsupportedSignalType(storageNet);
          }
        }
      }
    }
}
