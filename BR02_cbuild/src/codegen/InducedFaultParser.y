/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/* \file InducedFaultParser.y
 *
 * When -induceCodegenFaults is specified a file is read with a list of faults
 * to inject into the output stream. This is the parser specification for the
 * induced fault file.
 *
 * The file should contain a list of fault * entries. Each entry is of the form:
 *
 * fault filename linenumber "string";
 *
 * (Currently this is the only type of fault currently defined)
 *
 * For example, given the line
 *
 *   fault f_000000.cxx 20 "x = 10;";
 *
 * The line "x = 10;" would be inserted as line 20 in the file f_000000.cxx.
 *
 */

%{

#include <stdarg.h>
#include "util/CarbonTypes.h"
#include "util/UtStringUtil.h"
#include "codegen/InducedFaultTable.h"

#define YYDEBUG 1
int InducedFault_lex (void);
void InducedFault_error (const char *fmt, ...);

static bool contains (const char *, const char c);

%}

%union {
  char *string;
  UInt32 integer;
}

%token _FAULT_
%token _APPEND_
%token _PREPEND_
%token _YYDEBUG_

%token <string> _STRING_
%token <integer> _INTEGER_

%%

induced_fault_file: fault_list
;

fault_list: 
  fault_list fault
| /*empty*/
;

fault: 
  /* _YYDEBUG_ just turns on the parser debugging output; use it for testing
     this parser */
  _YYDEBUG_ ';' { InducedFault_debug = 1; }
| /* Specify text to be inserted as a line in an output file */
  _FAULT_ _STRING_ _INTEGER_ _STRING_ ';'
  { 
    if (contains ($2, '/')) {
      // The filename must be just the filename part from the .carbon.libdesign
      // directory. No / allowed.
      InducedFault_error ("filename specification must not contain '/': %s", $2);
    } else if (contains ($4, '\n')) {
      // Each fault must specify a single line of text so do not allow newlines
      // in the text.
      InducedFault_error ("fault string must not contain a new line: %s", $2);
    } else {
      // Add the text to the fault table.
      InducedFaultTable::Parser::sInstance->add ($2, $3, $4); 
    }
    StringUtil::free ($2);
    StringUtil::free ($4); 
  }
| /* Common syntax error... */
  _FAULT_ _STRING_ _STRING_ ';'
  { InducedFault_error ("a line number is required in a fault specification: %s %s", $2, $3);
    delete $2; delete $3; }
| /* Specify text to append to an output file */
  _APPEND_ _STRING_ _STRING_ ';'
  {
    if (contains ($2, '/')) {
      // The filename must be just the filename part from the .carbon.libdesign
      // directory. No / allowed.
      InducedFault_error ("filename specification must not contain '/': %s", $2);
    } else if (contains ($3, '\n')) {
      // Each fault must specify a single line of text so do not allow newlines
      // in the text.
      InducedFault_error ("fault string must not contain a new line: %s", $2);
    } else {
      InducedFaultTable::Parser::sInstance->append ($2, $3); 
    }
    StringUtil::free ($2);
    StringUtil::free ($3); 
  }
| /* Specify text to prepend to an output file */
  _PREPEND_ _STRING_ _STRING_ ';'
  {
    if (contains ($2, '/')) {
      // The filename must be just the filename part from the .carbon.libdesign
      // directory. No / allowed.
      InducedFault_error ("filename specification must not contain '/': %s", $2);
    } else if (contains ($3, '\n')) {
      // Each fault must specify a single line of text so do not allow newlines
      // in the text.
      InducedFault_error ("fault string must not contain a new line: %s", $2);
    } else {
      InducedFaultTable::Parser::sInstance->prepend ($2, $3); 
    }
    StringUtil::free ($2);
    StringUtil::free ($3); 
  }
;

%%

//! Error reporting for induced fault parser
void InducedFault_error (const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  InducedFaultTable::Parser::sInstance->error (fmt, ap);
  va_end (ap);
}

//! \return iff the string contains a given character
static bool contains (const char *s, const char c)
{
  for (int n = 0; s [n] != '\0'; n++) {
    if (s [n] == c) {
      return true;
    }
  }
  return false;
}

