// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*
 * \file
 * All code-emitting routines related to NUCycle structures.
 * 
 */
#include "emitUtil.h"
#include "codegen/codegen.h"
#include "codegen/ScheduleBucket.h"
#include "nucleus/NUCycle.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNet.h"
#include "compiler_driver/CarbonContext.h" // for CRYPT

using namespace emitUtil;

//! Emit a declaration for a cycle function
void NUCycle::emitDeclaration(UtOStream& outf, int limit)
{
  if (not isScheduled ())
    // Dead cycle
    return;

  outf << "  void cycle$" << mId << "();" << ENDL;

  // Visit each scheduleable block and for any blocks larger than limit,
  // mark them as coded out-of-line and add them to the out-of-line list

  mSchedule.identifyOutlineCandidates (limit, &mOutlined, false);
}

// Utility to print the body of a cycle block
static void
sEmit (UtOStream &outf, int ident, NUCycleScheduleBlock::const_iterator b,
       NUCycleScheduleBlock::const_iterator e)
{
  for(NUCycleScheduleBlock::const_iterator i = b; i != e; ++i) {
    outf << ENDL;
    (*i)->emit (outf, ident);
  }
}


//! Emit the subfunctions for the cycle
void NUCycle::emitSubFunctions (UtOStream& outf) const
{
  for(CycleBlockVector::const_iterator i = mOutlined.begin (); i != mOutlined.end (); ++i)
  {
    NUCycleScheduleBlock* block = *i;
    NU_ASSERT (block->isOutline (), this);

    outf << TAG 
         << "static void cycle$_$" << block->getOutline () << "(" 
         << emitUtil::mclass (gCodeGen->getTopModule ()) << "& "
         << emitUtil::noprefix (gCodeGen->getTopName ()) << "){" << ENDL;
    sEmit (outf, 2, block->begin (), block->end ());
    outf << "}" << ENDL;
  }
}

//! Emit a call to a cycle function
ScheduleBucket* NUCycle::emitCall(UtOStream& outf) const
{
  UtString name;
  name << noprefix (gCodeGen->getTopName ()) << ".cycle$" << mId;
  ScheduleBucket* cycle_bucket = NULL;
  if (isScheduled ()) {
    outf << ENDL;
    gCodeGen->startSampleSchedBucket(name, outf);
    // Return the cycle's bucket from this function.
    cycle_bucket = gCodeGen->getSchedBuckets()->getCurrent();
    outf << "  " << name << "();" << ENDL;
    gCodeGen->stopSampleSchedBucket(outf);
  }
  return cycle_bucket;
}

//! Emit the definition of a cycle function
/*!
 * Emits code for evaluating a combinational cycle.
 * When possible the code tries to call the function to compute
 * each cycle node only once.  This means we emit branching paths
 * where the order of calls is different on one leg of the branch
 * from the order on the other leg.
 *
 * When we are unable to break part of a cycle down into a set of
 * predetermined call orders, we emit a loop which calls the functions
 * to compute nodes in that part of the cycle repeatedly.
 *
 * There are two styles of loops.  For cycles with ordinary
 * nets, we emit a while loop which attempts to run the cycle
 * until it settles, or until a pre-defined maximum iteration count
 * is reached.  For loops whose cut points are not easily
 * checkable, like large memories with non-constant index access,
 * we emit a simple for loop that iterates the cycle some default
 * iteration count without trying to detect a settled condition.
 */
void NUCycle::emitDefinition(UtOStream& outf) const
{
  // GenSched has already generated the function proto, open-brackets and
  // local reference to "this".
  if (isScheduled ()) {
    outf << ENDL;
    mSchedule.emit(outf, 2);
  }
  // It will do the close brackets too.
}

void NUCycleScheduleBlock::identifyOutlineCandidates (int limit, CycleBlockVector* cvlist,
                                                      bool skip)
{
  // Visit my children to see if any of them should be outlined...
  for (NUCycleScheduleBlock::iterator i = begin(); i != end(); ++i)
    (*i)->identifyOutlineCandidates (limit, cvlist, false);

  // Then check to see if this block deserves it too.  If my immediate parent was
  // a SettleLoop that settles, then this block is blasted out inline and
  // can't be outlined.
  if (not skip && (size() > limit) && not isOutline ())
  {
    putOutline();
    cvlist->push_back(this);
  }
}

void NUCycleScheduleBlock::emit(UtOStream& outf, UInt32 indent) const
{
  UtString indentString;
  indentString.append(indent, ' ');

  outf << "// size = " << size() << ENDL;

  if (isOutline())
  {
    outf << indentString << "cycle$_$" << mSubFunction << "("
         << emitUtil::noprefix (gCodeGen->getTopName()) << ");" << ENDL;
  } else {
    sEmit (outf, indent, mSequence.begin(), mSequence.end());
  }
}

int NUCycleSimple::size() const { return 1; }

void NUCycleSimple::identifyOutlineCandidates (int, CycleBlockVector*, bool){}

void NUCycleSimple::emit(UtOStream& outf, UInt32 indent) const
{
  NUUseDefNode* useDef = mFlow->getUseDefNode();
  if (useDef == NULL)     // bidis will have null usedefs
    return;

  UtString indentString;
  indentString.append(indent, ' ');
  outf << indentString;
  outf << TAG;
  gCodeGen->outputHierPath(outf, mFlow->getHier());
  useDef->emitCode(eCGNameOnly);
  outf << ";" << indentString << "// size = " << size() << ENDL;

}

void NUCycleBranch::emit(UtOStream& outf, UInt32 indent) const
{
  UtString indentString;
  indentString.append(indent, ' ');

  outf << indentString << "// size = " << size() << ENDL;
  outf << indentString << TAG << "if (";
  SourceLocator loc = mNetElab->getNet()->getLoc();
  NUExpr* condition = new NUIdentRvalueElab(mNetElab, loc);
  if (mNetElab->getNet()->getBitSize() > 1)
  {
    NUConst* indexExpr = NUConst::create(false, mBitIndex, 32, loc);
    condition = new NUVarselRvalue(condition, indexExpr, loc);
  }
  condition->resize(1);

  if (not isPrecise(condition->emitCode(eCGCycleCopy)))
    mask (condition->getBitSize(), "&");
  delete condition;

  outf << ") {" << ENDL;
  mTrueBlock.emit(outf, indent + 2);
  outf << indentString << "} else {" << ENDL;
  mFalseBlock.emit(outf, indent + 2);
  outf << indentString << "}" << ENDL;
}

void NUCycleBranch::identifyOutlineCandidates (int limit, CycleBlockVector* cvlist, bool)
{
  mTrueBlock.identifyOutlineCandidates (limit, cvlist, false);
  mFalseBlock.identifyOutlineCandidates (limit, cvlist, false);
}

int NUCycleSettleLoop::size() const { return mBody.size (); }

bool NUCycleSettleLoop::isCheckable(FLNodeElab* flow)
{
  NUUseDefNode* useDef = flow->getUseDefNode();
  if (useDef == NULL)
    return false; // bidis will have NULL usedef node
  NUNetElab* netElab = flow->getDefNet();
  if ((useDef->getType() == eNUCModelCall))
    return false; // can't check C-models
  if (netElab->getNet()->is2DAnything())
  {
#if 0
    // we can only check memory writes with a constant address
    NUAssign* assign = dynamic_cast<NUAssign*>(useDef);
    if (!assign)
      return false;
    NULvalue* lvalue = assign->getLvalue();
    NUVarselLvalue* varsel = dynamic_cast<NUVarselLvalue*>(lvalue);
    NUMemselLvalue* memsel = NULL;
    if (varsel)
    {
      if (!varsel->isConstIndex())
        return false;
      memsel = dynamic_cast<NUMemselLvalue*>(varsel->getLvalue());
    }
    else
    {
      memsel = dynamic_cast<NUMemselLvalue*>(lvalue);
    }
    if (!memsel || (memsel->getIndex()->getType() != NUExpr::eNUConstNoXZ))
      return false;
#else
    return false; // do not allow any memories for the moment
#endif
  }
  return true;
}


bool NUCycleSettleLoop::canSettleLoop() const
{
  bool canSettle = true;
  for (FLNodeElabSet::const_iterator loop = mCutPoints.begin();
       (loop != mCutPoints.end()) && canSettle;
       ++loop)
  {
    FLNodeElab* flow = *loop;
    canSettle &= isCheckable(flow);
  }
  return canSettle;
}

void NUCycleSettleLoop::emit(UtOStream& outf, UInt32 indent) const
{
  UtString indentString;
  indentString.append(indent, ' ');
  outf << indentString << "{ // begin settled logic block" << ENDL;

  bool settleThisLoop = canSettleLoop();
  if (!settleThisLoop)
  {
    // there were uncheckable blocks as cut points for the cycle!
    // just run it the max number of times!
    outf << indentString << "  // there are blocks in the loop that we can't detect settling on, so just run it\n";
    outf << indentString << "  unsigned int defaultIterations = 10;" << ENDL;
    outf << indentString << "  for (unsigned int i = 0; i < defaultIterations; ++i)" << ENDL;
    outf << indentString << "  {" << ENDL;

    mBody.emit(outf, indent + 4);

    outf << indentString << "  }" << ENDL;
  }
  else
  {
    // all cut points in the cycle were checkable, so we will create settle-detection logic

    // build a map from NUNetElab to local name
    NameMap localNameMap;
    UInt32 num = 0;
    for (FLNodeElabSet::const_iterator loop = mCutPoints.begin(); loop != mCutPoints.end(); ++loop)
    {
      FLNodeElab* flow = *loop;
      NUNet * driven_net = flow->getFLNode()->getDefNet();
      if (driven_net->isControlFlowNet() or driven_net->isSysTaskNet()) {
        continue;
      }

      FLN_ELAB_ASSERT(isCheckable(flow), flow);

      NUNetElab* netElab = flow->getDefNet();
      if (localNameMap.find(netElab) != localNameMap.end())
        continue; // we only want one copy per net
    
      // OK, we need a temporary for this one
      UtString* name = new UtString("prev_");
      *name << ++num;
      localNameMap[netElab] = name;
    }

    // write out the local declarations for change detects
    outf << indentString << "  // local declarations used for change detection" << ENDL;
    for (NameMap::iterator n = localNameMap.begin(); n != localNameMap.end(); ++n)
    {
      const NUNetElab* netElab = n->first;
      const UtString* name = n->second;

      // Get the type of the net we are actually bound to and use that for
      // the local declaration.
      const STBranchNode* parent;
      NUNet* net = emitUtil::findAllocatedNet(netElab, &parent);

      outf << indentString << "  ";

      if (net->is2DAnything())
        outf << TAG << CBaseTypeFmt(net->getBitSize());
      else
        outf << TAG << CBaseTypeFmt(net);

      outf << " " << *name << ";" << ENDL;
    }

    // write out the settle loop header
    outf << indentString << "  // run the cycle until it settles" << ENDL;
    outf << indentString << "  unsigned int remaining = 32;\n";
    outf << indentString << "  bool settled = false;\n";
    outf << indentString << "  while (!settled && (remaining > 0))\n";
    outf << indentString << "  {\n";

    // initialize settle values
    outf << indentString << "    // initialize prior values for change detection" << ENDL;
    for (NameMap::iterator n = localNameMap.begin(); n != localNameMap.end(); ++n)
    {
      NUNetElab* netElab = const_cast<NUNetElab*>(n->first);
      const UtString* name = n->second;

      // build an NUIdentRvalueElab for codegen of the RHS
      NUIdentRvalueElab rv(netElab,netElab->getNet()->getLoc());
      const STBranchNode* parent;
      NUNet *net = emitUtil::findAllocatedNet (netElab, &parent);
      rv.setSignedResult (net->isSigned ());

      outf << indentString << "    " << TAG << *name << " = ";
      rv.emitCode(eCGCycleCopy);
      outf << ";" << ENDL;
    }

    // emit the loop body statements
    outf << ENDL << indentString << "    // loop body\n";
    mBody.emit(outf, indent+4);

    // check settle values
    outf << ENDL << indentString << "    // change detection\n";
    outf << indentString << "    settled = true;" << ENDL;
    for (NameMap::iterator n = localNameMap.begin(); n != localNameMap.end(); ++n)
    {
      NUNetElab* netElab = const_cast<NUNetElab*>(n->first);
      const UtString* name = n->second;

      // build an NUIdentRvalueElab for codegen of the RHS
      NUIdentRvalueElab rv(netElab,netElab->getNet()->getLoc());
      const STBranchNode* parent;
      NUNet *net = emitUtil::findAllocatedNet (netElab, &parent);
      rv.setSignedResult (net->isSigned ());

      outf << indentString << "    " << TAG << "settled &= (" << *name << " == ";
      rv.emitCode(eCGCycleCopy);
      outf << ");" << ENDL;
    }

    outf << indentString << "    --remaining;" << ENDL;
    outf << indentString << "  }" << ENDL;

    // free the memory for the local strings
    for (NameMap::iterator n = localNameMap.begin(); n != localNameMap.end(); ++n)
      delete n->second;
  }
  
  outf << indentString << "} // end settled logic block" << ENDL;
} // void SettleLoop::emit

void NUCycleSettleLoop::identifyOutlineCandidates (int limit, CycleBlockVector* cvlist, bool)
{
  // Block immediately beneath a settle may not be outlined because it can be
  // sliced and diced...
  mBody.identifyOutlineCandidates (limit, cvlist, canSettleLoop());
}
