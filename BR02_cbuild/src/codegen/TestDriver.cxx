// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include <algorithm>
#include <cmath>
#include "codegen/CGAuxInfo.h"
#include "util/UtTestPatternGen.h"
#include "nucleus/NUAliasDB.h"
#include "emitUtil.h"
#include "TestDriver.h"
#include "util/StringAtom.h"
#include "util/UtConv.h"
#include "util/UtArray.h"
#include "compiler_driver/VhdlPortTypeMap.h"
#include "util/UtHashSet.h"
#include "iodb/IODBDesignData.h"
#include "iodb/IODBUserTypes.h"
#include "hdl/HdlTime.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "util/UtIStream.h"

static const char* scNetDataPostFix    = "_cds_carbonnetdata";
static const char* DefaultVhdlDataType = "std_logic";

//////////////////////////////////////////Test Vector Data///////////////////////////////////////////

TestDriverData::TestDriverData(IODBNucleus*     iodb, 
                               bool             useIODB,
                               UInt32           vector_count,
                               SInt32           vector_seed, 
                               UInt32           reset_length,
                               UInt32           clock_delay,
                               bool             debug, 
                               bool             dump_vcd, 
                               bool             dump_fsdb, 
                               bool             glitchDetect,
                               SInt8            timeUnit, 
                               SInt8            timePrecision,
                               bool             timescaleSpecified, 
                               bool             checkpoint,
                               bool             skipObservables,
                               VhdlPortTypeMap* portTypeMap) :
  mIODB(iodb), 
  mVectorCount(vector_count), 
  mVectorSeed(vector_seed),
  mResetLength(reset_length), 
  mClockDelay(clock_delay), 
  mPortTypeMap(portTypeMap), 
  mDebug(debug), 
  mDumpVCD(dump_vcd), 
  mDumpFSDB(dump_fsdb),
  mTimeUnit(timeUnit), 
  mTimePrecision(timePrecision),
  mTimescaleSpecified(timescaleSpecified), 
  mResetHigh(false),
  mUseIODB(useIODB), 
  mGlitchDetect(glitchDetect), 
  mCheckpoint(checkpoint),
  mSkipObservables(skipObservables) {
}


bool TestDriverData::useIODB() const {
  return mUseIODB;
}

void TestDriverData::addModule(NUModule* m) {
  mModule = m;
}

NUModule* TestDriverData::getModule(void) const {
  return mModule;
}

void TestDriverData::addInput(NUNet* n) {
  mInputs.push_back(n);
}

const NUNetVector& TestDriverData::getInputs(void) const {
  return mInputs;
}

bool TestDriverData::hasInputs(void) const {
  return !mInputs.empty();
}

void TestDriverData::addClock(NUNet* n) {
  mClocks.push_back(n);
}

UInt32 TestDriverData::getTotalNumInputs() const {
  return (mClocks.size() + mInputs.size() + mResets.size() + mBids.size() + mDeposits.size());
}

const NUNetVector& TestDriverData::getClocks(void) const {
  return mClocks;
}

bool TestDriverData::hasClocks(void) const {
  return !mClocks.empty();
}

void TestDriverData::addReset(NUNet* n) {
  mResets.push_back(n);
}

const NUNetVector& TestDriverData::getResets(void) const {
  return mResets;
}

bool TestDriverData::hasResets(void) const {
  return !mResets.empty();
}

void TestDriverData::addOutput(NUNet* n) {
  mOutputs.push_back(n);
}

const NUNetVector& TestDriverData::getOutputs(void) const {
  return mOutputs;
}

bool TestDriverData::hasOutputs(void) const {
  return !mOutputs.empty();
}

void TestDriverData::addBid(NUNet* n) {
  mBids.push_back(n);
}

const NUNetVector& TestDriverData::getBids(void) const {
  return mBids;
}

bool TestDriverData::hasBids(void) const {
  return !mBids.empty();
}

//! add \a n to the set of deposits that will be included in the test vector
void TestDriverData::addDeposit(STSymbolTableNode* node) {
  NUNetElab* n = NUNetElab::find(node);
  // here we limit the additions to the user specified deposit nets
  // that actually could be observed by the user (either directly or
  // indirectly)  If isRead then it is used in the design, if
  // primaryPort then it could be observed on the boundary of the
  // design, if marked protectedObservable then user has specified they
  // might read it from outside of the design using the carbon shell.
  if (n->queryAliases(&NUNet::isRead) or
      n->queryAliases(&NUNet::isPrimaryPort) or
      n->getStorageNet()->isProtectedObservable(mIODB)) {
    mDeposits.push_back(node);
  }
}

const NodeVec& TestDriverData::getDeposits(void) const {
  return mDeposits;
}

bool TestDriverData::hasDeposits(void) const {
  return !mDeposits.empty();
}

void TestDriverData::addObserved(STSymbolTableNode* node) {
  // Do not add memories, the shell aborts when carbonFindNet is called on a memory.
  // To support memories, would need to emit smarter specialized tb code for them.
  // Same applies to VHDL record fields.
  const NUNet* net = NUNet::find(node);

  if (not net->is2DAnything() and not net->isRecordField()) {
    mObserveds.push_back(node);
  }
}

const NodeVec& TestDriverData::getObserveds(void) const {
  return mObserveds;
}

bool TestDriverData::hasObserveds(void) const {
  return !mObserveds.empty();
}

bool TestDriverData::hasPorts() const {
  return hasBids() || hasOutputs() || hasInputs() || hasClocks() || hasResets();
}

void TestDriverData::setResetHigh(bool reset_high) {
  mResetHigh = reset_high;
}

bool TestDriverData::getResetHigh(void) const {
  return mResetHigh;
}

UInt32 TestDriverData::getVectorCount(void) const {
  return mVectorCount;
}

void TestDriverData::setVectorCount (const UInt32 n) {
  mVectorCount = n;
}

UInt32 TestDriverData::getClockDelay(void) const {
  return mClockDelay;
}

UInt32 TestDriverData::getVectorWidth(void) const {
  UInt32 vector_width = 0;

  accumulateWidth(getClocks(),   vector_width);
  accumulateWidth(getResets(),   vector_width);
  accumulateWidth(getInputs(),   vector_width);
  accumulateWidth(getBids(),     vector_width);
  accumulateWidth(getBids(),     vector_width);
  accumulateWidth(getDeposits(), vector_width);
  accumulateWidth(getDeposits(), vector_width);

  return vector_width;
}

SInt32 TestDriverData::getVectorSeed(void) const {
  return mVectorSeed;
}

UInt32 TestDriverData::getResetLength(void) const {
  return mResetLength;
}

void TestDriverData::accumulateWidth(const NUNetVector &ports, UInt32 &width) const {
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    width += (*i)->getBitSize();
  }
}

void TestDriverData::accumulateWidth(const NodeVec &ports, UInt32 &width) const {
  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++) {
    STSymbolTableNode* node = *i;
    NUNet*             net  = NUNet::find(node);

    width += net->getBitSize();
  }
}

bool TestDriverData::getDebug(void) const {
  return mDebug;
}

bool TestDriverData::getDumpVCD(void) const {
  return mDumpVCD;
}

bool TestDriverData::getDumpFSDB(void) const {
  return mDumpFSDB;
}

bool TestDriverData::getCheckpoint(void) const {
  return mCheckpoint;
}

bool TestDriverData::getSkipObservables(void) const {
  return mSkipObservables;
}

bool TestDriverData::getGlitchDetect(void) const {
  return mGlitchDetect;
}

void TestDriverData::dump(UtOStream &out) {
  dump(out, getClocks(),    "Clocks:");
  dump(out, getResets(),    "Resets:");
  dump(out, getInputs(),    "Inputs:");
  dump(out, getOutputs(),   "Outputs:");
  dump(out, getBids(),      "Bids:");
  dump(out, getObserveds(), "Observeds:");
  dump(out, getDeposits(),  "Deposits:");

  out << UtIO::endl;
  out << "Resets are " << ((mResetHigh) ? "high" : "low") 
      << " (" << mResetHigh << ")" << "." << UtIO::endl;
  out << UtIO::endl;
}

void TestDriverData::dump(UtOStream &out, const NUNetVector & ports, const UtString label) {
  out << UtIO::endl;
  out << label << UtIO::endl;

  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    out << "  " << (*i)->getName()->str() << UtIO::endl;
  }
}

void TestDriverData::dump(UtOStream &out, const NodeVec & ports, const UtString label) {
  out << UtIO::endl;
  out << label << UtIO::endl;

  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++) {
    UtString name;

    (*i)->compose(&name);
    out << "  " << name << UtIO::endl;
  }
}

TestbenchElabListEmitter::TestbenchElabListEmitter(UtOStream& o, 
                                                   bool&      first) : 
 mOut(o), 
 mFirst(first) {
}

TestbenchElabListEmitter::~TestbenchElabListEmitter() { 
}

void TestbenchElabListEmitter::operator()(STSymbolTableNode* n) { 
  if(mFirst) {
    mFirst = false;
  }
  else {
    mOut << ", ";
  }

  emit(mOut, n);
}

void TestbenchElabListEmitter::emit(UtOStream& out, STSymbolTableNode *n)
{ 
  UtString name;
  n->compose(&name);
  out << name;
}

TestbenchElabPortListEmitter::TestbenchElabPortListEmitter(UtOStream& o, 
                                                           bool&      first, 
                                                           UInt32     start_count) :
  TestbenchElabListEmitter(o, first),
  mCount(start_count) { 
}

TestbenchElabPortListEmitter::~TestbenchElabPortListEmitter() { 
}

void TestbenchElabPortListEmitter::operator()(STSymbolTableNode* n) { 
  TestbenchElabListEmitter::operator()(n);
  ++mCount;
}

TestbenchElabDataPortListEmitter::TestbenchElabDataPortListEmitter(UtOStream& o, 
                                                                   bool&      first) :
  TestbenchElabPortListEmitter(o, first) { 
}

TestbenchElabDataPortListEmitter::~TestbenchElabDataPortListEmitter() { 
}

void TestbenchElabDataPortListEmitter::emit(UtOStream& out, STSymbolTableNode* n) { 
  UtString     base_name;
  UtString     name;
  const NUNet* net = NUNet::find(n);

  base_name << net->getName()->str();
  TestbenchEmitter::makeDataTempName(base_name.c_str(), &name);
  name << "_tmp_" << mCount;
  out << name;
}

TestbenchElabDataNamedPortListEmitter::TestbenchElabDataNamedPortListEmitter(UtOStream& o, 
                                                                             bool&      first) :
  TestbenchElabPortListEmitter(o, first) { 
}

TestbenchElabDataNamedPortListEmitter::~TestbenchElabDataNamedPortListEmitter() { 
}

void TestbenchElabDataNamedPortListEmitter::emit(UtOStream& out, STSymbolTableNode* n) { 
  UtString     base_name;
  UtString     name;
  const NUNet* net = NUNet::find(n);

  base_name << net->getName()->str();
  TestbenchEmitter::makeDataTempName(base_name.c_str(), &name);
  name << "_tmp_" << mCount;
  out << "." << name << "(" << name << ")";
}

TestbenchElabEnablePortListEmitter::TestbenchElabEnablePortListEmitter(UtOStream& o, 
                                                                       bool&      first) :
  TestbenchElabPortListEmitter(o, first) { 
}

TestbenchElabEnablePortListEmitter::~TestbenchElabEnablePortListEmitter() { 
}

void TestbenchElabEnablePortListEmitter::emit(UtOStream& out, STSymbolTableNode* n) { 
  UtString     base_name;
  UtString     name;
  const NUNet* net = NUNet::find(n);

  base_name << net->getName()->str();
  TestbenchEmitter::makeEnableTempName(base_name.c_str(), &name);
  name <<"_tmp_" << mCount;
  out << name;
}

TestbenchTempElabEnablePortListEmitter::TestbenchTempElabEnablePortListEmitter(UtOStream& o, 
                                                                               bool&      first) :
  TestbenchElabPortListEmitter(o, first) { 
}

TestbenchTempElabEnablePortListEmitter::~TestbenchTempElabEnablePortListEmitter() { 
}

void TestbenchTempElabEnablePortListEmitter::emit(UtOStream& out, STSymbolTableNode* n) { 
  UtString     base_name;
  UtString     name;
  const NUNet* net = NUNet::find(n);

  base_name << net->getName()->str();
  TestbenchEmitter::makeTempEnableTempName(base_name.c_str(), &name);
  name <<"_tmp_" << mCount;
  out << name;
}

TestbenchTempElabDataPortListEmitter::TestbenchTempElabDataPortListEmitter(UtOStream& o, 
                                                                           bool&      first) :
  TestbenchElabPortListEmitter(o, first) { 
}

TestbenchTempElabDataPortListEmitter::~TestbenchTempElabDataPortListEmitter() { 
}

void TestbenchTempElabDataPortListEmitter::emit(UtOStream& out, STSymbolTableNode* n) { 
  UtString     base_name;
  UtString     name;
  const NUNet* net = NUNet::find(n);

  base_name << net->getName()->str();
  TestbenchEmitter::makeTempDataTempName(base_name.c_str(), &name);
  name <<"_tmp_" << mCount;
  out << name;
}

TestbenchElabEnableNamedPortListEmitter::TestbenchElabEnableNamedPortListEmitter(UtOStream& o, 
                                                                                 bool&      first) :
  TestbenchElabPortListEmitter(o, first) { 
}

TestbenchElabEnableNamedPortListEmitter::~TestbenchElabEnableNamedPortListEmitter() { 
}

void TestbenchElabEnableNamedPortListEmitter::emit(UtOStream& out, STSymbolTableNode* n) { 
  UtString     base_name;
  UtString     name;
  const NUNet* net = NUNet::find(n);

  base_name << net->getName()->str();
  TestbenchEmitter::makeEnableTempName(base_name.c_str(), &name);
  name << "_tmp_" << mCount;
  out << "." << name << "(" << name << ")";
}

TestbenchPortListEmitter::TestbenchPortListEmitter(UtOStream& o, 
                                                   bool&      first) :
  mOut(o), 
  mFirst(first) { 
}

TestbenchPortListEmitter::~TestbenchPortListEmitter() { 
}

void TestbenchPortListEmitter::operator()(NUNet* n) { 
  if(mFirst) {
    mFirst = false;
  }
  else {
    mOut << ", ";
  }

  emit(mOut, n);
}

void TestbenchPortListEmitter::emit(UtOStream& out, NUNet* n) { 
  out << n->getName()->str();
}

TestbenchNamedPortListEmitter::TestbenchNamedPortListEmitter(UtOStream& o, 
                                                             bool&      first) : 
  TestbenchPortListEmitter(o, first) { 
}

TestbenchNamedPortListEmitter::~TestbenchNamedPortListEmitter() { 
}

void TestbenchNamedPortListEmitter::emit(UtOStream &out, NUNet *n) { 
  out << "." << n->getName()->str() << "(" << n->getName()->str() << ")";
}

TestbenchNamedBidPortListEmitter::TestbenchNamedBidPortListEmitter(UtOStream& o, 
                                                                   bool&      first) : 
  TestbenchPortListEmitter(o, first) { 
}

TestbenchNamedBidPortListEmitter::~TestbenchNamedBidPortListEmitter() { 
}


void TestbenchNamedBidPortListEmitter::emit(UtOStream& out, NUNet* n) { 
  const char* name = n->getName()->str();
  UtString    temp_name;

  out << "." << name << "(" << name << ")";
  out << ", ";

  temp_name.clear();
  VerilogTestbenchEmitter::makeDataTempName(n->getName()->str(), &temp_name);
  out << "." << temp_name << "(" << temp_name << ")";
  out << ", ";

  temp_name.clear();
  VerilogTestbenchEmitter::makeEnableTempName(n->getName()->str(), &temp_name);
  out << "." << temp_name << "(" << temp_name << ")";
}

TestbenchTempPortListEmitter::TestbenchTempPortListEmitter(UtOStream& o, 
                                                           bool&      first) : 
  TestbenchPortListEmitter(o, first) { 
}

TestbenchTempPortListEmitter::~TestbenchTempPortListEmitter() { 
}

void TestbenchTempPortListEmitter::emit(UtOStream& out, NUNet* n) { 
  UtString name;

  VerilogTestbenchEmitter::makeDelayTempName(n->getName()->str(), &name);
  out << name;
}

TestbenchDataPortListEmitter::TestbenchDataPortListEmitter(UtOStream& o, 
                                                           bool&      first) : 
  TestbenchPortListEmitter(o, first) { 
}

TestbenchDataPortListEmitter::~TestbenchDataPortListEmitter() { 
}

void TestbenchDataPortListEmitter::emit(UtOStream& out, NUNet* n) { 
  UtString name;

  VerilogTestbenchEmitter::makeDataTempName(n->getName()->str(), &name);
  out << name;
}

TestbenchEnablePortListEmitter::TestbenchEnablePortListEmitter(UtOStream& o, 
                                                               bool&      first) : 
  TestbenchPortListEmitter(o, first) { 
}

TestbenchEnablePortListEmitter::~TestbenchEnablePortListEmitter() { 
}

void TestbenchEnablePortListEmitter::emit(UtOStream& out, NUNet* n) { 
  UtString name;

  VerilogTestbenchEmitter::makeEnableTempName(n->getName()->str(), &name);
  out << name;
}

TestbenchTempDataPortListEmitter::TestbenchTempDataPortListEmitter(UtOStream& o, 
                                                                   bool&      first) : 
  TestbenchPortListEmitter(o, first) { 
}

TestbenchTempDataPortListEmitter::~TestbenchTempDataPortListEmitter() { 
}

void TestbenchTempDataPortListEmitter::emit(UtOStream& out, NUNet* n) { 
  UtString name;

  VerilogTestbenchEmitter::makeTempDataTempName(n->getName()->str(), &name);
  out << name;
}

TestbenchTempEnablePortListEmitter::TestbenchTempEnablePortListEmitter(UtOStream& o, 
                                                                       bool&      first) : 
  TestbenchPortListEmitter(o, first) { 
}

TestbenchTempEnablePortListEmitter::~TestbenchTempEnablePortListEmitter() { 
}

void TestbenchTempEnablePortListEmitter::emit(UtOStream& out, NUNet* n) { 
  UtString name;

  VerilogTestbenchEmitter::makeTempEnableTempName(n->getName()->str(), &name);
  out << name;
}

TestbenchBidPortListEmitter::TestbenchBidPortListEmitter(UtOStream& o, 
                                                         bool&      first) : 
  TestbenchPortListEmitter(o, first) { 
}

TestbenchBidPortListEmitter::~TestbenchBidPortListEmitter() { 
}

void TestbenchBidPortListEmitter::emit(UtOStream& out, NUNet* n) { 
  const char* name = n->getName()->str();
  UtString    temp_name;

  out << name;
  out << ", ";

  temp_name.clear();
  VerilogTestbenchEmitter::makeDataTempName(n->getName()->str(), &temp_name);
  out << temp_name;
  out << ", ";

  temp_name.clear();
  VerilogTestbenchEmitter::makeEnableTempName(n->getName()->str(), &temp_name);
  out << temp_name;
}

//////////////////////////////////////////Testbench Emitter//////////////////////////////////////////

// Testbench Emitter
TestbenchEmitter::TestbenchEmitter(TestDriverData* d, 
                                   const UtString& designName) :
  mTestData(d) {
  mTestMemoryFileName << designName << ".test.vectors";
}

// Clears Parameters
void TestbenchEmitter::clearEmitParams(emitParams *p) {
    (*p).isDecl       = false;
    (*p).isBid        = false;
    (*p).isElab1      = false;
    (*p).isElab2      = false; 
    (*p).getDebug     = false; 
    (*p).dataBitWidth = 0; 
    (*p).bitSize      = 0;
    (*p).emit1        = eEmitNormal; 
    (*p).emit2        = eEmitNormal; 
    (*p).count        = -1;
    (*p).prefix.clear();
    (*p).direction    = ePortIn;
    (*p).indent.clear();
    (*p).index        = NULL;
    (*p).column       = NULL;
    (*p).pHash        = NULL;
    (*p).userType     = NULL;
    (*p).isVector     = false;
    (*p).buf          = NULL;
    (*p).ranValGen    = NULL;
    (*p).accWidth     = NULL;
    (*p).maxWidth     = NULL;
    (*p).hasConst     = NULL;
    (*p).initialize   = false;
    (*p).first        = NULL;
}

//Extracts the coresponding Postfix for the given Type
void TestbenchEmitter::extractPostfix(EmitType  emit, 
                                      UtString* postfix) {
  (*postfix).clear();

  switch (emit) {
    case eEmitNormal:   
      *postfix << "";
      break;
    case eEmitVisibility:
      *postfix << "_cds_tmp_visible";
      break;
    case eEmitDelay:      
      *postfix << "_cds_tmp_delay";
      break;
    case eEmitData:       
      *postfix << "_cds_tmp_data";
      break;
    case eEmitEnable:     
      *postfix << "_cds_tmp_enable";
      break;
    case eEmitTempData:   
      *postfix << "_cds_tmp_data_tmp";
      break;
    case eEmitTempEnable: 
      *postfix << "_cds_tmp_enable_tmp";
      break;
    default:
      *postfix << "";
  }
}

// Returns <name>_tmp_<count> Format
void TestbenchEmitter::makeTmpCount(UtString* name, 
                                    SInt32    count) {
  (*name) << "_tmp_" << count;
}

// Extracts Node Hierarchy from the DesignSymbolTable for the given NUNet
void TestbenchEmitter::extractNUNetInfo(const NUNet* net, 
                                        NodeVec*     nodeList) {
  STAliasedLeafNode*         master    = net->getNameLeaf()->getMaster();
  NUNet*                     masterNet = NUAliasBOM::castBOM(master->getBOMData())->getNet();
  NUScope*                   scope     = masterNet->getScope();
  STSymbolTable*             designST  = scope->getIODB()->getDesignSymbolTable();
  typedef UtArray<NUScope*>  ScopeList;
  ScopeList                  mScopes;
  mScopes.push_back(scope);
  while(scope->getParentScope() != 0) {
    scope = scope->getParentScope();
    mScopes.push_back(scope);
  }
  STSymbolTableNode* parent = NULL;
  for(ScopeList::reverse_iterator s=mScopes.rbegin(), e=mScopes.rend(); s!=e; ++s) {
    scope  = *s;
    parent = designST->find(parent, scope->getName());
    (*nodeList).push_back(parent);
  }
  STSymbolTableNode* node = designST->find(parent, master->strObject());
  (*nodeList).push_back(node);
}

// Extracts Node Hierarchy from the DesignSymbolTable for the given Symbol Table Node
void TestbenchEmitter::extractSTNodeInfo(STSymbolTableNode* node, 
                                         NodeVec*           nodeList) {
  NodeVec            tmpList;
  STSymbolTableNode* parent;

  tmpList.push_back(node);
  parent = node;
  while(parent->getParent() != NULL) {
    parent = parent->getParent();
    tmpList.push_back(parent);
  }
  for(NodeVec::reverse_iterator s=tmpList.rbegin(), e=tmpList.rend(); s!=e; ++s) {
    (*nodeList).push_back(*s);
  }
}

// Checks if it is an Array of Bits (i.e. a packed array).
// Some extra checking is required for VHDL, because we
// only handle certain types.
bool TestbenchEmitter::isArrayOfBits(UtOStream&      out, 
                                     const UserType* nodeType) {

  // It has to be an array 
  const UserArray* arrayType   = nodeType->castArray();
  INFO_ASSERT(arrayType, "Invalid call to 'isArrayOfBits', expected array");
  bool result = false;
  // If this is Verilog/SystemVerilog, all we need to do is
  // check to see if this is a packed array.
  if (arrayType->getLanguageType() == eLanguageTypeVerilog) {
    result = arrayType->isPackedArray();
  } else {
    // If VHDL, we need to verify that it's a type for which
    // we have conversion functions
    const UserType*  elementType = arrayType->getElementType();
    UserType::Type   elType      = elementType->getType();
    if (elType == UserType::eScalar) {
      const UserScalar* scalarType = elementType->castScalar();
      if (scalarType->getSize() == 1) {
	UtString arrayTypeName(arrayType->getTypeName()->str());
	// These are the only array types for which we have conversion functions
	// from and to std_logic_vector.
	if((arrayTypeName == "std_logic_vector")  ||
	   (arrayTypeName == "std_ulogic_vector") ||
	   (arrayTypeName == "bit_vector")        ||
	   (arrayTypeName == "signed")            ||
	   (arrayTypeName == "unsigned")) {
	  result = true;
	}
      }
    } else if ((elType == UserType::eArray)  || 
               (elType == UserType::eStruct) ||
                (elType == UserType::eEnum)) {
      result = false;
    }
    else {
      out << "  assert(0==\"Unknown Type\"); \\" << UtIO::endl; // this_assert_OK
      result = false;
    }
  }
  
  return result;
}

bool TestbenchEmitter::requiresConversionFunction(const UserType* ut) {
  bool result = true;

  if (ut == NULL)
    return false;

  if (ut->getLanguageType() == eLanguageTypeVerilog)
    return false;

  switch(ut->getVhdlType()) {
  case eVhdlTypeStdLogic:
  case eVhdlTypeStdULogic:
    result = false;
    break;
  case eVhdlTypeUnknown: 
    {
      UtString typeName(ut->getIntrinsicTypeName()->str());
      if(typeName == "std_logic_vector")
	result = false;
    }
    break;
  default:
    break;
  }

  return result;
}

// Emits Node Hierarchy with Arrays (Core)
void TestbenchEmitter::emitMainCoreArray(FuncType                funcType, 
                                         UtOStream&              out, 
                                         NodeVec::const_iterator n, 
                                         NodeVec::const_iterator e,
                                         const UserType*         nodeType,
                                         UtString*               varName, 
                                         UtString*               nodeName, 
                                         STSymbolTableNode*      node, 
                                         emitParams              p) {
  UtString         nVarName;
  UtString         nNodeName;
  const UserArray* arrayType = nodeType->castArray();
  SInt32           lsb       = arrayType->getRange()->getLsb();
  SInt32           msb       = arrayType->getRange()->getMsb();
  SInt32           i         = msb;
  bool             increment = (arrayType->getRange()->bigEndian()) ? false : true; 
  bool             done      = false;

  while(!done) {
    const UserType* elementType = arrayType->getDimElementType(0);

    nVarName.clear();
    nNodeName.clear();

    nVarName  << *varName  << "_" << i;
    StringUtil::makeIdent(&nVarName);

    // main.cxx and SystemVerilog (special case)
    if(funcType == eCxxDeclInit || funcType == eVerilogConcat || funcType == eVerilogMap)
      nNodeName << *nodeName << "[" << i << "]";
    // main.vhdl (special cases)
    else if((funcType == eVhdlFormalPortDecl)    ||
            (funcType == eVhdlTestbenchReadList) ||
            (funcType == eVhdlTestbenchWriteList)) {
      // Multi-Dimentional Array [..., ...]
      if(arrayType->getTypeName() == elementType->getTypeName())
        nNodeName << *nodeName << "(" << i << ",";
      // Not a Multi-Dimentional Array [... of ...]
      else
        nNodeName << *nodeName << "(" << i << ")";
    }
    // Otherwise
    else
      nNodeName << *nodeName;

    emitMainCoreRecursion(funcType, out, n, e, elementType, &nVarName, &nNodeName, node, p);
    
    if(i == lsb)
      done = true;
    else {
      if(increment)
	i++;
      else
	i--;
    }
  } // Do

}

// Calculates the Leaf Node Implemented Bit Size
UInt32 TestbenchEmitter::getImplementedBitSize(const UserType* userType,
                                               UInt32          bitSize) {
  if(userType != NULL) 
    // getSize() returns the actual number of implemented bits for all user types
    return(userType->getSize());
  else
    return(bitSize);
}

// Emits Node Hierarchy (Core)
void TestbenchEmitter::emitMainCore(FuncType                funcType, 
                                    UtOStream&              out, 
                                    NodeVec::const_iterator s, 
                                    NodeVec::const_iterator e, 
                                    UtString*               varName, 
                                    UtString*               nodeName, 
                                    emitParams              p) {
  UtString                nVarName(*varName);
  UtString                nNodeName(*nodeName);
  NodeVec::const_iterator bl = s; 

  bl++;

  // Stop Decomposition After Decomposing the Last Node
  if(s == e) {
    switch(funcType) {
      case eTBAccWidth :
        getAccWidthCore(p);
        break;
      case eTBMaxWidth :
        getMaxWidthCore(p);
        break;
      case eTvHasConstraint :
        hasConstraintCore(p);
        break;
      case eTvMemoryDat :
        emitMemoryDatCore(p);
        break;
      case eCxxDeclInit :
        emitMainCxxDeclInitCore(out, &nVarName, &nNodeName, p);
        break;
      case eCxxTestVectorLoad :
        emitMainCxxTestVectorLoadCore(out, &nVarName, p);
        break;
      case eCxxTestVectorDeposit :
        emitMainCxxTestVectorDepositCore(out, &nVarName, p);
        break;
      case eCxxOutputStream : 
        emitMainCxxOutputStreamCore(out, &nVarName, p);
        break;
      case eCxxTestVectorIncr :
        emitMainCxxTestVectorIncrCore(out, &nVarName, p);
        break;
      case eCxxTestVectorRewind :
        emitMainCxxTestVectorRewindCore(out, &nVarName, p);
        break;
      case eCxxTestVectorDestroy :
        emitMainCxxTestVectorDestroyCore(out, &nVarName, p);
        break;
      case eVhdlFormalPortDecl :
        emitMainVhdlFormalPortDeclCore(out, &nNodeName, p);
        break;
      case eVhdlSignalDecl :
        emitMainVhdlSignalDeclCore(out, &nNodeName, p);
        break;
      case eVhdlTestbenchReadList :
        emitMainVhdlTBReadListCore(out, &nNodeName, p);
        break;
      case eVhdlTestbenchDelayList :
        emitMainVhdlTBDelayListCore(out, &nNodeName, p);
        break;
      case eVhdlTestbenchWriteList :
        emitMainVhdlTBWriteListCore(out, &nNodeName, p);
        break;
      case eVhdlTestbenchEnableDriver :
        emitMainVhdlTBEnabledDriverCore(out, &nNodeName, p);
        break;
      case eVhdlPortMapDecl :
        emitMainVhdlPortMapDeclCore(out, &nNodeName, p);
        break;
      case eVerilogConcat:
        emitVerilogConcatCore(out, &nNodeName, p);
        break;
      case eVerilogMap:
        emitVerilogMapCore(out, &nNodeName, p);
        break;
      default :
        out << "  assert(0==\"Unknown Section\"); \\" << UtIO::endl; // this_assert_OK
    }
  }
  // Decompose Nodes
  else {
    STSymbolTableNode*      node     = *s;
    NodeVec::const_iterator n        = s;
    const UserType*         nodeType = CbuildSymTabBOM::getUserType(node);
    UtString                escapedVarName;
    UtString                escapedNodeName;
    UtString::size_type     pos;
    UtString::size_type     len;   

    n++;

    // Escaped Var Names
    escapedVarName << node->strObject()->str();
    StringUtil::makeIdent(&escapedVarName);
    if(nVarName.empty())
      nVarName << escapedVarName;
    else 
      nVarName << "_" << escapedVarName;

    // Escaped Node Names
    escapedNodeName << node->strObject()->str();
    pos = 0;
    len = escapedNodeName.length();
    while(pos<len) {
      if(escapedNodeName[pos] == '\\') {
        escapedNodeName.replace(pos, 1, "\\\\");
        pos++;
        len++;
      }
      pos++;
    }
    if(nNodeName.empty())
      nNodeName << escapedNodeName;
    else
      nNodeName << "." << escapedNodeName;

    emitMainCoreRecursion(funcType, out, n, e, nodeType, &nVarName, &nNodeName, node, p);
  }
}

// Emits Recursively Based on the UserType of the Node
void TestbenchEmitter::emitMainCoreRecursion(FuncType                funcType, 
                                             UtOStream&              out, 
                                             NodeVec::const_iterator n, 
                                             NodeVec::const_iterator e, 
                                             const UserType*         nodeType, 
                                             UtString*               varName, 
                                             UtString*               nodeName, 
                                             STSymbolTableNode*      node, 
                                             emitParams              p) {
  // Remember the Last User Type
  p.userType = nodeType;

  if(nodeType == NULL) {
    emitMainCore(funcType, out, n, e, &(*varName), &(*nodeName), p);
  }
  else {
    UserType::Type nType = nodeType->getType();

    if((nType == UserType::eScalar) ||
       (nType == UserType::eStruct) ||
       (nType == UserType::eEnum)) {
      emitMainCore(funcType, out, n, e, &(*varName), &(*nodeName), p);
    }
    else if(nType == UserType::eArray) {
      // Do Not Decompose Arrays of Bits (in SystemVerilog, a reg or net with only
      // packed dimensions is an array of bits).
      if(isArrayOfBits(out, nodeType) &&
         // Unless it is a Multi-Dimentional Array [..., ...] of Bits, and
         // it is not the Last Dimension that is being Decomposed
         !(*nodeName).empty()         &&
         ((*nodeName)[(*nodeName).length()-1] != ',')) 
        emitMainCore(funcType, out, n, e, &(*varName), &(*nodeName), p);
      // Do Not Decompose Leaf Arrays for Formal Port Decomposition in VHDL
      else if(((funcType == eVhdlFormalPortDecl)        ||
               (funcType == eVhdlSignalDecl)            ||
               (funcType == eVhdlTestbenchDelayList)    ||
               (funcType == eVhdlTestbenchEnableDriver) ||
               (funcType == eVhdlPortMapDecl)) && (n == e))
        emitMainCore(funcType, out, n, e, &(*varName), &(*nodeName), p);
      // Decompose Arrays of Non-Bits
      else 
        emitMainCoreArray(funcType, out, n, e, nodeType, &(*varName), &(*nodeName), node, p);
    }
    else {
      out << "  assert(0==\"Unknown Type\"); \\" << UtIO::endl; // this_assert_OK
    }
  }
}

// Gets the Accumulated Width of the Inputs (Core)
void TestbenchEmitter::getAccWidthCore(emitParams p) {
  *(p.accWidth) += getImplementedBitSize(p.userType, p.bitSize);
}

// Gets the Accumulated Width for Top Level Ports (Shell)
void TestbenchEmitter::getAccWidth(UtOStream&         out,
                                       const NUNetVector& ports, 
                                       UInt32*            width) {
  for(NUNetVector::const_iterator i=ports.begin(); i!=ports.end(); i++) {
    const NUNet* net = *i;
    NodeVec      nList;
    UtString     varNameTB("m");
    UtString     nodeNameTB;
    emitParams   p;

    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.bitSize  = net->getBitSize();
    p.accWidth = &(*width);
    emitMainCore(eTBAccWidth, out, nList.begin(), nList.end(), &varNameTB, &nodeNameTB, p);
    *width     = *(p.accWidth);
  }
}

// Gets the Accumulated Width for Elaborated Nodes (Shell)
void TestbenchEmitter::getAccWidthElab(UtOStream&     out,
                                           const NodeVec& ports, 
                                           UInt32*        width) {
  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++) {
    STSymbolTableNode* node = *i;
    NUNet*             net  = NUNet::find(node);
    NodeVec            nList;
    UtString           varNameTB("m");
    UtString           nodeNameTB;
    emitParams         p;

    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.bitSize  = net->getBitSize();
    p.accWidth = &(*width);
    emitMainCore(eTBAccWidth, out, nList.begin(), nList.end(), &varNameTB, &nodeNameTB, p);
    *width     = *(p.accWidth);
  }
}

// Gets the Accumulated Width of the Inputs
UInt32 TestbenchEmitter::getAccWidth(UtOStream& out) {
  UInt32 width = 0;

  // Top Level Ports
  getAccWidth(out, mTestData->getClocks(), &width);
  getAccWidth(out, mTestData->getResets(), &width);
  getAccWidth(out, mTestData->getInputs(), &width);
  getAccWidth(out, mTestData->getBids(),   &width);
  getAccWidth(out, mTestData->getBids(),   &width);
  // Elaborated Nodes
  getAccWidthElab(out, mTestData->getDeposits(), &width);
  getAccWidthElab(out, mTestData->getDeposits(), &width);

  return width;
}

// Gets the Max Width of the OutPuts (Core)
void TestbenchEmitter::getMaxWidthCore(emitParams p) {
  UInt32 bitSize = getImplementedBitSize(p.userType, p.bitSize);

  if(bitSize >= *(p.maxWidth)) 
    *(p.maxWidth) = bitSize;
}

// Gets the Max Width for Top Level Ports (Shell)
void TestbenchEmitter::getMaxWidth(UtOStream&         out,
                                   const NUNetVector& ports, 
                                   UInt32*            width) {
  for(NUNetVector::const_iterator i=ports.begin(); i!=ports.end(); i++) {
    const NUNet* net = *i;
    NodeVec      nList;
    UtString     varNameTB("m");
    UtString     nodeNameTB;
    emitParams   p;

    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.bitSize  = net->getBitSize();
    p.maxWidth = &(*width);
    emitMainCore(eTBMaxWidth, out, nList.begin(), nList.end(), &varNameTB, &nodeNameTB, p);
    if(*(p.maxWidth) >= *width)
      *width = *(p.maxWidth);
  }
}

// Gets the Max Width for Elaborated Nodes (Shell)
void TestbenchEmitter::getMaxWidthElab(UtOStream&     out,
                                       const NodeVec& ports, 
                                       UInt32*        width) {
  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++) {
    STSymbolTableNode* node = *i;
    NUNet*             net  = NUNet::find(node);
    NodeVec            nList;
    UtString           varNameTB("m");
    UtString           nodeNameTB;
    emitParams         p;

    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.bitSize  = net->getBitSize();
    p.maxWidth = &(*width);
    emitMainCore(eTBMaxWidth, out, nList.begin(), nList.end(), &varNameTB, &nodeNameTB, p);
    if(*(p.maxWidth) >= *width)
      *width = *(p.maxWidth);
  }
}

// Gets the Max Width of the OutPuts
UInt32 TestbenchEmitter::getMaxWidth(UtOStream& out) {
  UInt32 width = 0;

  // Top Level Ports
  getMaxWidth(out, mTestData->getClocks(),  &width);
  getMaxWidth(out, mTestData->getResets(),  &width);
  getMaxWidth(out, mTestData->getInputs(),  &width);
  getMaxWidth(out, mTestData->getOutputs(), &width);
  getMaxWidth(out, mTestData->getBids(),    &width);
  // Elaborated Nodes
  getMaxWidthElab(out, mTestData->getDeposits(), &width);
  if(!mTestData->getSkipObservables())
    getMaxWidthElab(out, mTestData->getObserveds(), &width);

  return width;
}

/////////////////////////////////////////Test Vector Emitter/////////////////////////////////////////

// Test Vector Emitter
TestVectorEmitter::TestVectorEmitter(MsgContext &msg, TestDriverData* d,
				     const UtString& target_name) :
  TestbenchEmitter(d, target_name),
  mMsgContext (msg),
  mTargetName(target_name) {
}

// Copies a user-supplied test vector file to memory.dat
void TestVectorEmitter::copy (const char *filename, UtOStream &out, UInt32 *nlines)
{
  // this gets run in the .carbon.libdesign directory, so unless the filename
  // is an absolute filename, prefix with "../".
  UtString filename1;
  if (*filename == '/') {
    filename1 << filename;
  } else {
    filename1 << "../" << filename;
  }
  UtIFStream vectors (filename1.c_str ());
  if (!vectors.is_open ()) {
    const char *errmsg = vectors.getErrmsg ();
    const char *reason = errmsg != NULL ? errmsg : "";
    mMsgContext.CGCannotOpenVectorFile (filename, reason);
    return;                             // give up
  }
  // This code really should check that the format of the input file is
  // valid. It does not. This could well break things at runtime.
  *nlines = 0;
  UtString line;
  while (vectors.getline (&line)) {
    (*nlines)++;
    out << line;
  }
}


// Emits Test Vector (memory.dat)
void TestVectorEmitter::emit(UtOStream& out) {
  bool         hasConst     = hasConstraint(out);
  UInt32       clock_width  = 0;
  UInt32       reset_width  = 0;
  UInt32       enable_width = 0;
  UInt32       data_width   = 0;
  RandomValGen clkRanValGen(712   + mTestData->getVectorSeed());
  RandomValGen enbRanValGen(16661 + mTestData->getVectorSeed());
  RandomValGen datRanValGen(4999  + mTestData->getVectorSeed());
  PatGenArray  dataPatterns;

  // Top Level Ports
  getAccWidth(out, mTestData->getClocks(), &clock_width);
  getAccWidth(out, mTestData->getResets(), &reset_width);
  getAccWidth(out, mTestData->getBids(),   &enable_width);
  getAccWidth(out, mTestData->getInputs(), &data_width);
  getAccWidth(out, mTestData->getBids(),   &data_width);
  // Elaborated Nodes
  getAccWidthElab(out, mTestData->getDeposits(), &enable_width);
  getAccWidthElab(out, mTestData->getDeposits(), &data_width);
  
  UtToggledTestPatternGen clock_bits(clock_width, clkRanValGen);
  UtStepTestPatternGen    reset_bits(reset_width, !mTestData->getResetHigh(), mTestData->getResetLength());
  // We probably want to make enable bits not change as frequently as data in the future.
  UtRandomTestPatternGen enable_bits(enable_width, enbRanValGen);
  if(data_width > 0) {
    UtRandomTestPatternGen* patGen = new UtRandomTestPatternGen(data_width, datRanValGen);

    dataPatterns.push_back(patGen);
  }

  for(UInt32 i=0 ; i<mTestData->getVectorCount(); i++) {
    UtString buf;

    // Clocks and Resets
    clock_bits.appendPattern(&buf);
    reset_bits.appendPattern(&buf);
    // Data Inputs
    if(hasConst)
      emitMemoryDat(out, datRanValGen, &buf);
    else {
      for(PatGenArray::const_iterator dp = dataPatterns.begin(); dp != dataPatterns.end(); ++dp) {
        UtRandomTestPatternGen* data_bits = *dp;

        data_bits->appendPattern(&buf);
        ++(*data_bits);
      }
    }
    // Enables
    enable_bits.appendPattern(&buf);

    // Test Vector
    UtConv::BinaryStrToHex(&buf);
    out << buf << UtIO::endl;
    
    // We need to keep clocks and resets from transitioning at the same time,
    // so just generate the vectors accordingly.  See bug 273.
    if(i % 2) 
      ++clock_bits;
    else 
      ++reset_bits;
    ++enable_bits;
  }

  for(PatGenArray::const_iterator dp = dataPatterns.begin(); dp != dataPatterns.end(); ++dp) {
    UtRandomTestPatternGen* data_bits = *dp;

    delete data_bits;
  } 
}

// Check if there is any Constraint (Core)
void TestVectorEmitter::hasConstraintCore(emitParams p) {
  int bitSize = getImplementedBitSize(p.userType, p.bitSize);

  *(p.hasConst) = false;

  // User Type Available
  if(p.userType != NULL) {
    UserType::Type nType = p.userType->getType();

    // Scalars
    if(nType == UserType::eScalar) {
      const UserScalar* scalarType = p.userType->castScalar();

      // Constraint
      if(scalarType->getRangeConstraint()) {
        SInt64 lsb = scalarType->getRangeConstraint()->getLsb();
        SInt64 msb = scalarType->getRangeConstraint()->getMsb();
        SInt64 max = ((msb - lsb) < 0) ? lsb - msb + 1 : msb - lsb + 1;

        if(std::pow(2.0, bitSize) != max)        
          *(p.hasConst) = true;
      }
    }
    // Enums
    else if(nType == UserType::eEnum) {
      const UserEnum* enumType = p.userType->castEnum();

      // Constraint
      if(enumType->getRange()) {
        SInt64 lsb = enumType->getRange()->getLsb();
        SInt64 msb = enumType->getRange()->getMsb();
        SInt64 max = ((msb - lsb) < 0) ? lsb - msb + 1 : msb - lsb + 1;

        if(std::pow(2.0, bitSize) != max)        
          *(p.hasConst) = true;
      }
      else if(enumType->getNumberElems() > 0) {
        SInt64 max = enumType->getNumberElems();

        if(std::pow(2.0, bitSize) != max)        
          *(p.hasConst) = true;
      }
    }
  }
}

// Check if there is any Constraint on Top Level Ports (Shell)
void TestVectorEmitter::hasConstraint(UtOStream&         out,
                                      const NUNetVector& ports, 
                                      bool*              hasConst) {
  for(NUNetVector::const_iterator i=ports.begin(); i!=ports.end() && !(*hasConst); i++) {
    const NUNet* net = *i;
    NodeVec      nList;
    UtString     varNameTv("m");
    UtString     nodeNameTv;
    emitParams   p;

    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.bitSize  = net->getBitSize();
    p.hasConst = &(*hasConst);
    emitMainCore(eTvHasConstraint, out, nList.begin(), nList.end(), &varNameTv, &nodeNameTv, p);
    if(*(p.hasConst))
      *hasConst = *(p.hasConst);
  }
}

// Check if there is any Constraint on Elaborated Nodes (Shell)
void TestVectorEmitter::hasConstraintElab(UtOStream&     out,
                                          const NodeVec& ports,
                                          bool*          hasConst) { 
  for(NodeVec::const_iterator i = ports.begin(); i != ports.end() && !(*hasConst); i++) {
    STSymbolTableNode* node = *i;
    NUNet*             net  = NUNet::find(node);
    NodeVec            nList;
    UtString           varNameTv("m");
    UtString           nodeNameTv;
    emitParams         p;

    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.bitSize  = net->getBitSize();
    p.hasConst = &(*hasConst);
    emitMainCore(eTvHasConstraint, out, nList.begin(), nList.end(), &varNameTv, &nodeNameTv, p);
    if(*(p.hasConst))
      *hasConst = *(p.hasConst);
  }
}

// Check if there is any Constraint
bool TestVectorEmitter::hasConstraint(UtOStream& out) {
  bool hasConst = false;

  // Top Level Ports
  hasConstraint(out, mTestData->getInputs(), &hasConst);  
  hasConstraint(out, mTestData->getBids(),   &hasConst);
  // Elaborated Nodes
  hasConstraintElab(out, mTestData->getDeposits(), &hasConst);  

  return hasConst;
}

// Emits Test Vectors (Core)
void TestVectorEmitter::emitMemoryDatCore(emitParams p) {
  UInt32 bitSize = getImplementedBitSize(p.userType, p.bitSize);

  // User Type Available
  if(p.userType != NULL) {
    UserType::Type nType = p.userType->getType();

    // Scalars
    if(nType == UserType::eScalar) {
      const UserScalar* scalarType = p.userType->castScalar();

      // Constraint
      if(scalarType->getRangeConstraint()) {
        UtConstrainedRandomIntGen data_bits(bitSize, 
                                            scalarType->getRangeConstraint()->getLsb(), 
                                            scalarType->getRangeConstraint()->getMsb(),  
                                            *(p.ranValGen));

        data_bits.appendPattern(p.buf);
      }
      // Non-Constraint
      else {
        UtRandomTestPatternGen data_bits(bitSize, *(p.ranValGen));

        data_bits.appendPattern(p.buf);
      }
    }
    // Enums
    else if(nType == UserType::eEnum) {
      const UserEnum* enumType = p.userType->castEnum();

      // Constraint
      if(enumType->getRange()) {
        UtConstrainedRandomIntGen data_bits(bitSize, 
                                            enumType->getRange()->getLsb(), 
                                            enumType->getRange()->getMsb(), 
                                            *(p.ranValGen));

        data_bits.appendPattern(p.buf);
      }
      // Non-Constraint      
      else if(enumType->getNumberElems() > 0) {
        UtConstrainedRandomIntGen data_bits(bitSize, 
                                            0, 
                                            enumType->getNumberElems()-1, 
                                            *(p.ranValGen));

        data_bits.appendPattern(p.buf);
      }
      // Empty
      else {
        UtRandomTestPatternGen data_bits(bitSize, *(p.ranValGen));

        data_bits.appendPattern(p.buf);
      }
    }
    // Others
    else {
      UtRandomTestPatternGen data_bits(bitSize, *(p.ranValGen));

      data_bits.appendPattern(p.buf);
    }
  }
  // No User Type Available
  else {
    UtRandomTestPatternGen data_bits(bitSize, *(p.ranValGen));

    data_bits.appendPattern(p.buf);
  }
}

// Emits Test Vectors for Top Level Ports (Shell)
void TestVectorEmitter::emitMemoryDat(UtOStream&         out,
                                      const NUNetVector& ports, 
                                      RandomValGen&      ranValGen,
                                      UtString*          buf) {
  for(NUNetVector::const_iterator i=ports.begin(); i!=ports.end(); i++) {
    const NUNet* net = *i;
    NodeVec      nList;
    UtString     varNameTv("m");
    UtString     nodeNameTv;
    emitParams   p;

    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.bitSize   = net->getBitSize();
    p.ranValGen = &ranValGen;
    p.buf       = &(*buf);
    emitMainCore(eTvMemoryDat, out, nList.begin(), nList.end(), &varNameTv, &nodeNameTv, p);
    *buf        = *(p.buf);
  }
}

// Emits Test Vectors for Elaborated Nodes (Shell)
void TestVectorEmitter::emitMemoryDatElab(UtOStream&     out,
                                          const NodeVec& ports, 
                                          RandomValGen&  ranValGen,
                                          UtString*      buf) {
  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++) {
    STSymbolTableNode* node = *i;
    NUNet*             net  = NUNet::find(node);
    NodeVec            nList;
    UtString           varNameTv("m");
    UtString           nodeNameTv;
    emitParams         p;

    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.bitSize   = net->getBitSize();
    p.ranValGen = &ranValGen;
    p.buf       = &(*buf);
    emitMainCore(eTvMemoryDat, out, nList.begin(), nList.end(), &varNameTv, &nodeNameTv, p);
    *buf        = *(p.buf);
  }
}

// Emits Test Vectors 
void TestVectorEmitter::emitMemoryDat(UtOStream&    out,
                                      RandomValGen& ranValGen,
                                      UtString*     buf) {
  // Top Level Ports
  emitMemoryDat(out, mTestData->getInputs(), ranValGen, &(*buf));  
  emitMemoryDat(out, mTestData->getBids(),   ranValGen, &(*buf));
  // Elaborated Nodes
  emitMemoryDatElab(out, mTestData->getDeposits(), ranValGen, &(*buf));  
}

////////////////////////////////////////Cxx Testbench Emitter////////////////////////////////////////

// Cxx Testbench Emitter
CxxTestbenchEmitter::CxxTestbenchEmitter(TestDriverData* d,
					 const UtString& target_name,
                                         const UtString& interface_name,
                                         const char*     topName,
                                         AtomicCache*    cache) : 
  TestbenchEmitter(d, target_name),
  mTargetName(target_name),
  mInterfaceTag(interface_name),
  mTopName(topName),
  mCodeNames(false, cache) {
}

// Emits Cxx Testbench (main.cxx)
void CxxTestbenchEmitter::emit(UtOStream& out) {
  UInt32 accWidth = getAccWidth(out);
  UInt32 maxWidth = getMaxWidth(out);

  // Header files.
  out << "#ifdef CARBON_SPEEDCC"               << UtIO::endl;
  out << "#include \"codegen/SysIncludes.h\""  << UtIO::endl;
  out << "#include \"util/c_memmanager.h\""    << UtIO::endl;
  out << "#else"                               << UtIO::endl;
  out << "#include <stdio.h>"                  << UtIO::endl;
  out << "#include <string.h>"                 << UtIO::endl;
  out << "#include <assert.h>"                 << UtIO::endl;
  out << "#include <stdlib.h>"                 << UtIO::endl;
  out << "#include \"carbon/c_memmanager.h\""  << UtIO::endl;
  out << "#endif"                              << UtIO::endl;
  out << "#include \"" << mTargetName <<".h\"" << UtIO::endl;
  out << UtIO::endl;

  // Emit helper for deposit support
  if(not mTestData->getDeposits().empty()) {
    emitMainCxxConditionalDeposit(out);
  }

  out << "// boolean to control simulation. If not ok, simulation will stop"        << UtIO::endl;
  out << "static CarbonStatus sSimulateStatus = eCarbon_OK;"                        << UtIO::endl;
  out << UtIO::endl;
  out << "// Entry point for calltree --toggle-collect=simulate --collect-state=no" << UtIO::endl;
  out << "extern \"C\" void simulate(CarbonObjectID* hdl,"                             << UtIO::endl;
  out << "                         const CarbonTime time) {"                        << UtIO::endl;
  out << "  sSimulateStatus = carbonSchedule(hdl, time);"                           << UtIO::endl;
  out << "  // Let $stop through"                                                   << UtIO::endl;
  out << "  if(sSimulateStatus == eCarbon_STOP)"                                    << UtIO::endl;
  out << "    sSimulateStatus = eCarbon_OK;"                                        << UtIO::endl;
  out << "}"                                                                        << UtIO::endl;
  out << UtIO::endl;
  out << "// control callback for $finish"                                          << UtIO::endl;
  out << "static void sControlFinishCB(CarbonObjectID*,"                            << UtIO::endl;
  out << "                             CarbonControlType,"                          << UtIO::endl;
  out << "                             CarbonClientData,"                           << UtIO::endl;
  out << "                             const char*,"                                << UtIO::endl;
  out << "                             int) {"                                      << UtIO::endl;
  out << "  sSimulateStatus = eCarbon_FINISH;"                                      << UtIO::endl;
  out << "}"                                                                        << UtIO::endl;
  out << UtIO::endl;
  out << "// our own version of abs, so we don't have to rely on std namespace"     << UtIO::endl;
  out << "#define CABS(e) ((e)>=0?(e):-(e))"                                        << UtIO::endl;
  out << UtIO::endl;
  out << "int main(int argc, char **argv) {"                                        << UtIO::endl;
  out << "  int doPrint     = 1;"                                                   << UtIO::endl;
  out << "  int repeatCount = 1;"                                                   << UtIO::endl;
  out << "  int arg         = 1;"                                                   << UtIO::endl;
  out << "  CarbonObjectID *hdl;"                                                   << UtIO::endl;

  if(accWidth > 0)
    out << "  CarbonMemFileID* memFile;" << UtIO::endl;

  out << "  CarbonTime time = 0;"                                         << UtIO::endl;
  out << "  SInt64     firstAddress;"                                     << UtIO::endl;
  out << "  SInt64     lastAddress;"                                      << UtIO::endl;
  out << "  while(argc > arg) {"                                          << UtIO::endl;
  out << "    if(strcmp(argv[arg], \"-noprint\") == 0) {"                 << UtIO::endl;
  out << "      doPrint = 0;"                                             << UtIO::endl;
  out << "      ++arg;"                                                   << UtIO::endl;
  out << "    }"                                                          << UtIO::endl;
  out << "    else if(strcmp(argv[arg], \"-repeat\") == 0) {"             << UtIO::endl;
  out << "      char* endp;"                                              << UtIO::endl;
  out << "      ++arg;"                                                   << UtIO::endl;
  out << "      repeatCount = strtol(argv[arg], &endp, 10);"              << UtIO::endl;
  out << "      ++arg;"                                                   << UtIO::endl;
  out << "      if((repeatCount == 0) || (*endp != '\\0')) {"             << UtIO::endl;
  out << "        fprintf(stderr, \"Invalid args\\n\");"                  << UtIO::endl;
  out << "        exit(1);"                                               << UtIO::endl;
  out << "      }"                                                        << UtIO::endl;
  out << "    }"                                                          << UtIO::endl;
  out << "    else {"                                                     << UtIO::endl;
  out << "      fprintf(stderr, \"Invalid argument: %s\\n\", argv[arg]);" << UtIO::endl;
  out << "      exit(1);"                                                 << UtIO::endl;
  out << "    }"                                                          << UtIO::endl;
  out << "  }"                                                            << UtIO::endl;

  // CarbonNet wrapper decls.
  out << UtIO::endl;
  emitMainCxxDeclInit(out, true /* declaration */);
  out << UtIO::endl;

  // Create a macro to initialize the model
  out << "#define INITIALIZE(INIT) \\" << UtIO::endl;
 
  const char* carbonFlags = NULL;
  if(mTestData->getGlitchDetect()) {
    carbonFlags = "CarbonInitFlags(eCarbon_NoInit|eCarbon_ClockGlitchDetect)";
  } 
  else {
    carbonFlags = "eCarbon_NoInit";
  }

  out << "  hdl = carbon_" << mInterfaceTag << "_create(";
  if(mTestData->useIODB())
    out << "eCarbonIODB";
  else
    out << "eCarbonFullDB";
  out << ", " << carbonFlags << "); \\" << UtIO::endl;

  out << "  carbonChangeMsgSeverity(hdl, 5065, eCarbonMsgSuppress); \\"             << UtIO::endl;
  out << "  CarbonRegisteredControlCBDataID* IDForFinish1 =" 
      << " carbonAdminAddControlCB(hdl, sControlFinishCB, NULL, eCarbonFinish); \\" << UtIO::endl;
  out << "  if(IDForFinish1 == NULL) { \\"                                          << UtIO::endl;
  out << "    fprintf(stderr," 
      << " \"Error: unable to register $finish callback function \\n\"); \\"        << UtIO::endl;
  out << "    return 1; \\"                                                         << UtIO::endl;
  out << "  } \\"                                                                   << UtIO::endl;
  out << "  if(INIT) \\"                                                            << UtIO::endl;
  out << "    carbonInitialize(hdl, NULL, NULL, NULL); \\"                          << UtIO::endl;

  // Setup testdriver to only dump FSDB or VCD waveform files. 
  // Both cannot be dumped at the same time.
  if(mTestData->getDumpFSDB()) {
    out << "  CarbonWaveID* wave = carbonWaveInitFSDB(hdl,\\"                              << UtIO::endl;
    out << "                                          \"" << mTopName << ".cds.fsdb\", \\" << UtIO::endl;
    out << "                                          e1ns);\\"                            << UtIO::endl;
    out << "  if(!wave)\\"                                                                 << UtIO::endl;
    out << "    fprintf(stderr, \"FSDB Init failed.\\n\"); \\"                             << UtIO::endl;
    out << "  carbonDumpSizeLimit(wave, 0); \\"                                            << UtIO::endl;
    out << "  if(carbonDumpVars(wave, 0, \"" << mTopName << "\") != eCarbon_OK)\\"         << UtIO::endl;
    out << "    fprintf(stderr, \"dumpvars failed\\n\"); \\"                               << UtIO::endl;
  }
  else if(mTestData->getDumpVCD()) {  
    out << "  CarbonWaveID* wave = carbonWaveInitVCD(hdl, \\"                              << UtIO::endl;
    out << "                                         \"" << mTopName << ".cds.dump\", \\"  << UtIO::endl;
    out << "                                         e1ns); \\"                            << UtIO::endl;
    out << "  if(!wave)\\"                                                                 << UtIO::endl;
    out << "    fprintf(stderr, \"VCD Init failed.\\n\"); \\"                              << UtIO::endl;
    out << "  if(carbonDumpVars(wave, 0, \"" << mTopName << "\") != eCarbon_OK) \\"        << UtIO::endl;
    out << "    fprintf(stderr, \"dumpvars failed\\n\"); \\"                               << UtIO::endl;
  }
  
  // CarbonNet wrapper decls.
  out << "\\" << UtIO::endl;
  emitMainCxxDeclInit(out, false /* initialization */);
  out << UtIO::endl;

  // Instantiate initilization macro
  out << UtIO::endl;
  out << "  INITIALIZE(true);" << UtIO::endl;
  out << UtIO::endl;

  if(accWidth > 0) {
    out << "  memFile = carbonReadMemFile(hdl,"                                  << UtIO::endl;
    out << "                              \"" << mTestMemoryFileName << "\","    << UtIO::endl;
    out << "                              eCarbonHex, "                          << UtIO::endl;
    out << "                              " << accWidth << ","                   << UtIO::endl;
    out << "                              0);"                                   << UtIO::endl;
    out << "  if(!memFile)"                                                      << UtIO::endl; 
    out << "    return 1;"                                                       << UtIO::endl;
    out << UtIO::endl;
    out << "  if(carbonMemFileGetFirstAndLastAddrs(memFile,"                     << UtIO::endl;
    out << "                                       &firstAddress,"               << UtIO::endl;
    out << "                                       &lastAddress) != eCarbon_OK)" << UtIO::endl;
    out << "    return 1;"                                                       << UtIO::endl;
    out << UtIO::endl;
    out << "  CarbonUInt32 numRows = CABS(lastAddress - firstAddress) + 1;"      << UtIO::endl;
    
    // Load up the vectors.  
    emitMainCxxTestVectorLoad(out, accWidth);
    
    // Close up the memfile object. No longer needed.
    out << "  carbonFreeMemFileID(&memFile);" << UtIO::endl;
    out << UtIO::endl;
  }
  else {
    out << "  firstAddress = 0;"                                         << UtIO::endl;
    out << "  lastAddress  = " << mTestData->getVectorCount() - 1 << ";" << UtIO::endl;
  }
  
  // Set up the format buffer
  if (mTestData->getDebug())
    // Don't need as many bytes for hex
    maxWidth = maxWidth/4 + ((maxWidth % 4) + 3)/4;
  // Add a byte for the terminator
  ++maxWidth;
  
  out << "  char*        s       = NULL;"                          << UtIO::endl;
  out << "  const size_t strSize = " << maxWidth << ";"            << UtIO::endl;
  out << UtIO::endl;
  out << "  if(doPrint) "                                          << UtIO::endl;
  out << "    s = CARBON_ALLOC_VEC(char, strSize);"                << UtIO::endl;
  out << UtIO::endl;
  out << "  for(int repeat = 0; repeat < repeatCount; ++repeat) {" << UtIO::endl;
  out << "    for(SInt64 i = firstAddress; (i <= lastAddress) &&" 
      << " (sSimulateStatus == eCarbon_OK); ++i) {"                << UtIO::endl;

  // If testing checkpoint save/restore, save and restore the simulation at the midpoint.
  if(mTestData->getCheckpoint()) {
    out << "      if(i == " << (mTestData->getVectorCount() / 2) << ") {"                    << UtIO::endl;
    out << "        if(carbonSave(hdl,"                                                      << UtIO::endl;
    out << "                      \"" << mInterfaceTag << ".checkpoint\") != eCarbon_OK)"    << UtIO::endl;
    out << "          fprintf(stderr, \"carbonSave failed\\n\");"                            << UtIO::endl;
    out << "        // destroy modle instance and create a new one "                         << UtIO::endl;
    out << "        carbonDestroy(&hdl);"                                                    << UtIO::endl;
    out << "        INITIALIZE(false);"                                                      << UtIO::endl;
    out << "        if(carbonRestore(hdl,"                                                   << UtIO::endl;
    out << "                         \"" << mInterfaceTag << ".checkpoint\") != eCarbon_OK)" << UtIO::endl;
    out << "          fprintf(stderr, \"carbonRestore failed\\n\");"                         << UtIO::endl;
    out << "      }"                                                                         << UtIO::endl;
  }

  // Deposit the nets
  emitMainCxxTestVectorDeposit(out);
  out << UtIO::endl;

  // Call the schedule.
  out << "      simulate(hdl, time);" << UtIO::endl;
  out << "      time += 100; "        << UtIO::endl;
  out << UtIO::endl;
  
  // Output the inputs and outputs.
  out << "      if(!doPrint)" << UtIO::endl;
  out << "        continue;"  << UtIO::endl;
  out << UtIO::endl;
  emitMainCxxOutputStream(out);
  out << "      fputc(\'\\n\', stdout);" << UtIO::endl;
  out << "      fflush(stdout);" << UtIO::endl;

  // Increment TestVectors
  emitMainCxxTestVectorIncr(out);

  out << "    }" << UtIO::endl;
  out << UtIO::endl;
  
  // Rewind all data
  emitMainCxxTestVectorRewind(out);

  out << "  }"                    << UtIO::endl;
  out << UtIO::endl;
  out << "  carbonDestroy(&hdl);" << UtIO::endl;

  // Delete all the vectors
  emitMainCxxTestVectorDestroy(out);

  // Delete the format string
  out << UtIO::endl;
  out << "  if(s)"                                << UtIO::endl;
  out << "    CARBON_FREE_VEC(s, char, strSize);" << UtIO::endl;
  out << UtIO::endl;
  out << "  return 0;"                            << UtIO::endl;
  out << "}"                                      << UtIO::endl;
}

/*!
 * Emit helper routine to conditionally set values.  
 * The API does not give a routine to set bit values
 * through a mask, so that is what we emit here.
 */
void CxxTestbenchEmitter::emitMainCxxConditionalDeposit(UtOStream& out) {
  out << "static void conditional_deposit(CarbonObjectID*  hdl,"            << UtIO::endl;
  out << "                                CarbonNet*    net,"            << UtIO::endl;
  out << "                                CarbonUInt32* data,"           << UtIO::endl;
  out << "                                CarbonUInt32* enable) {"       << UtIO::endl;
  out << "  for(int i = 0; i < carbonGetNumUInt32s(net); ++i) {"         << UtIO::endl;
  out << "    CarbonUInt32 cur_val = 0;"                                 << UtIO::endl;
  out << "    carbonExamineWord(hdl, net, &cur_val, i, 0);"              << UtIO::endl;
  out << "    cur_val = (cur_val & enable[i]) | (data[i] & ~enable[i]);" << UtIO::endl;
  out << "    carbonDepositWord(hdl, net, cur_val, i, 0);"               << UtIO::endl;
  out << "  }"                                                           << UtIO::endl;  
  out << "}"                                                             << UtIO::endl;
  out << UtIO::endl;
}

// Emits Initial Declaration (Core)
void CxxTestbenchEmitter::emitMainCxxDeclInitCore(UtOStream& out, 
                                                  UtString*  varNameCxx, 
                                                  UtString*  nodeNameCxx, 
                                                  emitParams p) {
  UtString nVarName(*varNameCxx);
  UtString nNodeNameCxx(*nodeNameCxx);

  if(p.isElab1) {
    makeTmpCount(&nVarName,  p.count);
  }
  if(p.isDecl) {
    out << "  CarbonNetID * " << nVarName << ";"          << UtIO::endl;;
  }
  else {
    out << "  " << nVarName << " = carbonFindNet(hdl, \"" 
        << nNodeNameCxx << "\"); \\"                      << UtIO::endl;
    out << "  assert(" << nVarName << "); \\"             << UtIO::endl; // this_assert_OK
  }
}

// Emits Initial Declaration for Top Level Ports (Shell)
void CxxTestbenchEmitter::emitMainCxxDeclInit(UtOStream&         out, 
                                              const NUNetVector& ports, 
                                              bool               isDecl) {
  for(NUNetVector::const_iterator i=ports.begin(); i!=ports.end(); i++) {
    const NUNet* net = *i;
    NodeVec      nList;
    UtString     varNameCxx("m");
    UtString     nodeNameCxx;
    emitParams   p;

    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.isDecl = isDecl;
    emitMainCore(eCxxDeclInit, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits Initial Declaration for Elaborated Nodes (Shell)
void CxxTestbenchEmitter::emitMainCxxDeclInitElab(UtOStream&     out, 
                                                  const NodeVec& ports, 
                                                  bool           isDecl, 
                                                  UInt32         start_count) {
  UInt32 count = start_count;

  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++, count++) {
    STSymbolTableNode* node = *i;
    NodeVec            nList;
    UtString           varNameCxx("m");
    UtString           nodeNameCxx;
    emitParams         p;

    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.isDecl  = isDecl;
    p.isElab1 = true;
    p.count   = count;
    emitMainCore(eCxxDeclInit, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits CarbonNet Initial Declarations
void CxxTestbenchEmitter::emitMainCxxDeclInit(UtOStream& out, 
                                              bool       isDecl) {
  // Top Level Ports
  emitMainCxxDeclInit(out, mTestData->getClocks(),  isDecl);
  emitMainCxxDeclInit(out, mTestData->getResets(),  isDecl);
  emitMainCxxDeclInit(out, mTestData->getInputs(),  isDecl);  
  emitMainCxxDeclInit(out, mTestData->getOutputs(), isDecl);  
  emitMainCxxDeclInit(out, mTestData->getBids(),    isDecl);
  // Elaborated Nodes
  emitMainCxxDeclInitElab(out, mTestData->getDeposits(), isDecl, 0);  
  if(!mTestData->getSkipObservables())
    emitMainCxxDeclInitElab(out, mTestData->getObserveds(), isDecl, mTestData->getDeposits().size());
}

// Emits TestVector Load (Core)
void CxxTestbenchEmitter::emitMainCxxTestVectorLoadCore(UtOStream& out, 
                                                        UtString*  varNameCxx, 
                                                        emitParams p) {
  UtString nVarName(*varNameCxx);
  UtString spacer("  ");
  UInt32   bitSize  = getImplementedBitSize(p.userType, p.bitSize);
  UInt32   numWords = (bitSize + 31)/32;
  UInt32   srcIndex = p.dataBitWidth - (*(p.index) + bitSize);
  
  if(p.isBid) {
    UtString postfix;

    extractPostfix(p.emit1, &postfix);
    nVarName << postfix;
    if(p.isElab1) {
      makeTmpCount(&nVarName, p.count);
    }
  }
  else {
    nVarName << scNetDataPostFix;
  }

  *(p.index) += bitSize;

  out << UtIO::endl;
  out << spacer << "CarbonUInt32* " << nVarName << " = "                              << UtIO::endl;
  out << spacer << spacer << "(CarbonUInt32*) carbonmem_malloc((sizeof(CarbonUInt32)) * "
      << numWords << " * numRows);"                                                   << UtIO::endl;
  out << spacer << "CarbonUInt32* " << nVarName << "_begin = "                        << UtIO::endl;
  out << spacer << spacer << nVarName << ";"                                          << UtIO::endl;
  out << spacer << "if(carbonMemFilePopulateArray(memFile, "                          << UtIO::endl;
  out << spacer << "                              " << nVarName << ", "               << UtIO::endl;
  out << spacer << "                              " << numWords << " * numRows, "     << UtIO::endl;
  out << spacer << "                              " << srcIndex << ", "               << UtIO::endl;
  out << spacer << "                              " << bitSize  << ") != eCarbon_OK)" << UtIO::endl;
  out << spacer << "  return 1;"                                                      << UtIO::endl;
}

// Emits TestVector Load for Top Level Ports (Shell)
void CxxTestbenchEmitter::emitMainCxxTestVectorLoad(UtOStream&         out, 
                                                    const NUNetVector& ports, 
                                                    UInt32*            index, 
                                                    UInt32             dataBitWidth, 
                                                    bool               isBid,
                                                    EmitType           emit) {
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet* net = *i;
    UtString     varNameCxx("m");
    UtString     nodeNameCxx;
    NodeVec      nList;
    emitParams   p;

    out << UtIO::endl;
    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.isBid        = isBid;
    p.dataBitWidth = dataBitWidth;
    p.bitSize      = net->getBitSize();
    p.emit1        = emit;
    p.index        = &(*index);
    emitMainCore(eCxxTestVectorLoad, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
    *index         = *(p.index);
  }
}

// Emits TestVector Load for Elaborated Nodes (Shell)
void CxxTestbenchEmitter::emitMainCxxTestVectorLoadElab(UtOStream&     out, 
                                                        const NodeVec& ports, 
                                                        UInt32*        index, 
                                                        UInt32         dataBitWidth, 
                                                        EmitType       emit) {
  UInt32 count = 0;

  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++, count++) {
    STSymbolTableNode* node = *i;
    NUNet*             net  = NUNet::find(node);
    UtString           varNameCxx("m");
    UtString           nodeNameCxx;
    NodeVec            nList;
    emitParams         p;

    out << UtIO::endl;
    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.isBid        = true;
    p.isElab1      = true;
    p.dataBitWidth = dataBitWidth;
    p.bitSize      = net->getBitSize();
    p.emit1        = emit;
    p.count        = count;
    p.index        = &(*index);
    emitMainCore(eCxxTestVectorLoad, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
    *index         = *(p.index);
  }
}

// Emits carbonMemFilePopulateArray for TestVector Loads 
void CxxTestbenchEmitter::emitMainCxxTestVectorLoad(UtOStream& out, 
                                                    UInt32     dataBitWidth)
{
  UInt32 index = 0;

  // Top Level Ports
  emitMainCxxTestVectorLoad(out, mTestData->getClocks(), &index, dataBitWidth, false, eEmitNormal);
  emitMainCxxTestVectorLoad(out, mTestData->getResets(), &index, dataBitWidth, false, eEmitNormal);
  emitMainCxxTestVectorLoad(out, mTestData->getInputs(), &index, dataBitWidth, false, eEmitNormal);
  emitMainCxxTestVectorLoad(out, mTestData->getBids(),   &index, dataBitWidth, true,  eEmitData);
  // Elaborated Nodes
  emitMainCxxTestVectorLoadElab(out, mTestData->getDeposits(), &index, dataBitWidth, eEmitData);

  // Although Not in Correct Order, 
  // it is just to Preserve the Order of already Golded Tests
  // Top Level Ports
  emitMainCxxTestVectorLoad(out, mTestData->getBids(), &index, dataBitWidth, true, eEmitEnable);
  // Elaborated Nodes
  emitMainCxxTestVectorLoadElab(out, mTestData->getDeposits(), &index, dataBitWidth, eEmitEnable);
}

// Emits TestVector Deposit (Core)
void CxxTestbenchEmitter::emitMainCxxTestVectorDepositCore(UtOStream& out, 
                                                           UtString*  varNameCxx, 
                                                           emitParams p) {
  UtString nVarName(*varNameCxx);
  UtString spacer("      ");
  UInt32   bitSize  = getImplementedBitSize(p.userType, p.bitSize);
  UInt32   numWords = (bitSize + 31)/32;

  if(p.isBid) {
    UtString depositeType;
    UtString tmpVarName(nVarName);
    UtString postfix1;
    UtString postfix2;

    extractPostfix(p.emit1, &postfix1);
    extractPostfix(p.emit2, &postfix2);
    if(p.isElab1) {
      depositeType << "conditional_deposit";
      makeTmpCount(&tmpVarName, p.count);
      makeTmpCount(&postfix1,   p.count);
      makeTmpCount(&postfix2,   p.count);
    }
    else
      depositeType << "carbonDeposit";
    out << spacer << depositeType << "(hdl, " << tmpVarName << ", " 
        << nVarName << postfix1 << ", " << nVarName << postfix2 << ");"    << UtIO::endl;
  }
  else {
    out << spacer << "carbonDeposit(hdl, " << nVarName << ", " << nVarName 
        << scNetDataPostFix  << ", 0);"                                    << UtIO::endl;
    out << spacer << nVarName << scNetDataPostFix << " += " << numWords 
        << ";"                                                             << UtIO::endl;
  }
}

// Emits TestVector Deposit for Top Level Ports (Shell)
void CxxTestbenchEmitter::emitMainCxxTestVectorDeposit(UtOStream&         out, 
                                                       const NUNetVector& ports, 
                                                       bool               isBid,
                                                       EmitType           emit1, 
                                                       EmitType           emit2) {
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet* net = *i;
    UtString     varNameCxx("m");
    UtString     nodeNameCxx;
    NodeVec      nList;
    emitParams   p;

    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.isBid   = isBid;
    p.bitSize = net->getBitSize();
    p.emit1   = emit1;
    p.emit2   = emit2;
    emitMainCore(eCxxTestVectorDeposit, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits TestVector Deposit for Elaborated Nodes (Shell)
void CxxTestbenchEmitter::emitMainCxxTestVectorDepositElab(UtOStream&     out, 
                                                           const NodeVec& ports, 
                                                           EmitType       emit1, 
                                                           EmitType       emit2) {
  UInt32 count = 0;

  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++, count++) {
    STSymbolTableNode* node = *i;
    NUNet*             net  = NUNet::find(node);
    UtString           varNameCxx("m");
    UtString           nodeNameCxx;
    NodeVec            nList;
    emitParams         p;

    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.isBid   = true;
    p.isElab1 = true;
    p.bitSize = net->getBitSize();
    p.emit1   = emit1;
    p.emit2   = emit2;
    p.count   = count;
    emitMainCore(eCxxTestVectorDeposit, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits carbonDeposit for TestVectors
void CxxTestbenchEmitter::emitMainCxxTestVectorDeposit(UtOStream& out) {
  // Top Level Ports
  emitMainCxxTestVectorDeposit(out, mTestData->getClocks(), false, eEmitNormal, eEmitNormal);
  emitMainCxxTestVectorDeposit(out, mTestData->getResets(), false, eEmitNormal, eEmitNormal);
  emitMainCxxTestVectorDeposit(out, mTestData->getInputs(), false, eEmitNormal, eEmitNormal);
  emitMainCxxTestVectorDeposit(out, mTestData->getBids(),   true,  eEmitData,   eEmitEnable);
  // Elaborated Nodes
  emitMainCxxTestVectorDepositElab(out, mTestData->getDeposits(), eEmitData, eEmitEnable);
}

// Emits Output Stream (Core)
void CxxTestbenchEmitter::emitMainCxxOutputStreamCore(UtOStream& out, 
                                                      UtString*  varNameCxx, 
                                                      emitParams p) {
  UtString  nVarName(*varNameCxx);

  if(p.isBid) {
    const char* format  = p.getDebug ? "eCarbonHex" : "eCarbonBin";
    UInt32      bitSize = getImplementedBitSize(p.userType, p.bitSize);
    UtString    postfix;

    extractPostfix(p.emit1, &postfix);
    nVarName << postfix;
    if(p.isElab1) {
      makeTmpCount(&nVarName, p.count);
    }
    out << "      carbonFormatGenericArray(s, strSize, " << nVarName << ", " 
        << bitSize << ", 0, " << format << ");"      << UtIO::endl;
  }
  else {
    const char* format = p.getDebug ? "Hex" : "Bin";

    if(p.isElab2) {
      makeTmpCount(&nVarName, p.count);
    }
    out << "      " << "carbonFormat(hdl, " << nVarName
        << ", s, strSize, eCarbon" << format << ");" << UtIO::endl;
  }
  out << "      fputs(s, stdout);"                   << UtIO::endl;
  if(p.getDebug)
    out << "      fputc(\' \', stdout);"             << UtIO::endl;

}

// Emits Output Stream for Top Level Ports (Shell)
void CxxTestbenchEmitter::emitMainCxxOutputStream(UtOStream&         out, 
                                                  const NUNetVector& ports, 
                                                  bool               isBid,
                                                  EmitType           emit) { 
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet* net = *i;
    UtString     varNameCxx("m");
    UtString     nodeNameCxx;
    NodeVec      nList;
    emitParams   p;

    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.isBid    = isBid;
    p.bitSize  = net->getBitSize();
    p.getDebug = mTestData->getDebug();
    p.emit1    = emit;
    emitMainCore(eCxxOutputStream, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits Output Stream for Elaborated Nodes, Case 1 (Shell)
void CxxTestbenchEmitter::emitMainCxxOutputStreamElab(UtOStream&     out, 
                                                      const NodeVec& ports, 
                                                      EmitType       emit) {
  UInt32 count = 0;

  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++, count++) {
    STSymbolTableNode* node = *i;
    NUNet*             net  = NUNet::find(node);
    UtString           varNameCxx("m");
    UtString           nodeNameCxx;
    NodeVec            nList;
    emitParams         p;

    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.isBid    = true;
    p.isElab1  = true;
    p.bitSize  = net->getBitSize();
    p.getDebug = mTestData->getDebug();
    p.emit1    = emit;
    p.count    = count;
    emitMainCore(eCxxOutputStream, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits Output Stream for Elaborated Nodes, Case 2 (Shell)
void CxxTestbenchEmitter::emitMainCxxOutputStreamElab2(UtOStream&     out, 
                                                       const NodeVec& ports, 
                                                       UInt32         start_count) {
  UInt32 count = start_count;

  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++, count++) {
    STSymbolTableNode* node = *i;
    NUNet*             net  = NUNet::find(node);
    UtString           varNameCxx("m");
    UtString           nodeNameCxx;
    NodeVec            nList;
    emitParams         p;

    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.isElab2  = true;
    p.bitSize  = net->getBitSize();
    p.getDebug = mTestData->getDebug();
    p.count    = count;
    emitMainCore(eCxxOutputStream, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits carbonFormat for Output Streams
void CxxTestbenchEmitter::emitMainCxxOutputStream(UtOStream& out)
{
  // Top Level Ports
  emitMainCxxOutputStream(out, mTestData->getClocks(), false, eEmitNormal);
  emitMainCxxOutputStream(out, mTestData->getResets(), false, eEmitNormal);
  emitMainCxxOutputStream(out, mTestData->getInputs(), false, eEmitNormal);
  emitMainCxxOutputStream(out, mTestData->getBids(),   true,  eEmitData);
  emitMainCxxOutputStream(out, mTestData->getBids(),   true,  eEmitEnable);
  // Elaborated Nodes
  emitMainCxxOutputStreamElab(out, mTestData->getDeposits(),  eEmitData);
  emitMainCxxOutputStreamElab(out, mTestData->getDeposits(),  eEmitEnable);

  // Although Not in Correct Order, 
  // it is just to Preserve the Order of already Golded Tests
  // Top Level Ports
  emitMainCxxOutputStream(out, mTestData->getOutputs(), false, eEmitNormal);
  emitMainCxxOutputStream(out, mTestData->getBids(),    false, eEmitNormal );
  // Elaborated Nodes
  if(!mTestData->getSkipObservables())
    emitMainCxxOutputStreamElab2(out, mTestData->getObserveds(), mTestData->getDeposits().size());
}

// Emits TestVector Increment (Core)
void CxxTestbenchEmitter::emitMainCxxTestVectorIncrCore(UtOStream& out, 
                                                        UtString*  varNameCxx, 
                                                        emitParams p) {
  UtString nVarName(*varNameCxx);
  UtString spacer("      ");
  UInt32   bitSize  = getImplementedBitSize(p.userType, p.bitSize);
  UInt32   numWords = (bitSize + 31)/32;

  if(p.isBid) {
    UtString postfix;

    extractPostfix(p.emit1, &postfix);
    nVarName << postfix;
    if(p.isElab1) {
      makeTmpCount(&nVarName, p.count);
    }
  }
  out << spacer << nVarName << " += " << numWords << ";" << UtIO::endl;;
}

// Emits TestVector Increment for Top Level Ports (Shell)
void CxxTestbenchEmitter::emitMainCxxTestVectorIncr(UtOStream&         out, 
                                                    const NUNetVector& ports, 
                                                    EmitType           emit) { 
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet* net = *i;
    UtString     varNameCxx("m");
    UtString     nodeNameCxx;
    NodeVec      nList;
    emitParams   p;

    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.isBid   = true;
    p.bitSize = net->getBitSize();
    p.emit1   = emit;
    emitMainCore(eCxxTestVectorIncr, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits TestVector Increment for Elaborated Nodes (Shell)
void CxxTestbenchEmitter::emitMainCxxTestVectorIncrElab(UtOStream&     out, 
                                                        const NodeVec& ports, 
                                                        EmitType       emit) {
  UInt32 count = 0;

  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++, count++) {
    STSymbolTableNode* node = *i;
    NUNet*             net  = NUNet::find(node);
    UtString           varNameCxx("m");
    UtString           nodeNameCxx;
    NodeVec            nList;
    emitParams         p;

    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.isBid   = true;
    p.isElab1 = true;
    p.bitSize = net->getBitSize();
    p.emit1   = emit;
    p.count   = count;
    emitMainCore(eCxxTestVectorIncr, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits TestVector Increments
void CxxTestbenchEmitter::emitMainCxxTestVectorIncr(UtOStream &out) {
  // Top Level Ports
  emitMainCxxTestVectorIncr(out, mTestData->getBids(), eEmitData);
  emitMainCxxTestVectorIncr(out, mTestData->getBids(), eEmitEnable);
  // Elaborated Nodes
  emitMainCxxTestVectorIncrElab(out, mTestData->getDeposits(), eEmitData);
  emitMainCxxTestVectorIncrElab(out, mTestData->getDeposits(), eEmitEnable);
}

// Emits TestVector Rewinding (Core)
void CxxTestbenchEmitter::emitMainCxxTestVectorRewindCore(UtOStream& out, 
                                                          UtString*  varNameCxx, 
                                                          emitParams p) {
  UtString nVarName(*varNameCxx);
  UtString spacer("    ");
  
  if(p.isBid) {
    UtString postfix;

    extractPostfix(p.emit1, &postfix);
    nVarName << postfix;
    if(p.isElab1) {
      makeTmpCount(&nVarName, p.count);
    }
  }
  else {
    nVarName << scNetDataPostFix;
  }
  out << spacer << nVarName << " = " << nVarName << "_begin;" << UtIO::endl;
}

// Emits TestVector Rewinding for Top Level Ports (Shell)
void CxxTestbenchEmitter::emitMainCxxTestVectorRewind(UtOStream&         out, 
                                                      const NUNetVector& ports, 
                                                      bool               isBid,
                                                      EmitType           emit) {
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet* net = *i;
    UtString     varNameCxx("m");
    UtString     nodeNameCxx;
    NodeVec      nList;
    emitParams   p;

    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.isBid = isBid;
    p.emit1 = emit;
    emitMainCore(eCxxTestVectorRewind, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits TestVector Rewinding for Elaborated Nodes (Shell)
void CxxTestbenchEmitter::emitMainCxxTestVectorRewindElab(UtOStream&     out,
                                                          const NodeVec& ports, 
                                                          EmitType       emit) {
  UInt32 count = 0;

  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++, count++) {
    STSymbolTableNode* node = *i;
    UtString           varNameCxx("m");
    UtString           nodeNameCxx;
    NodeVec            nList;
    emitParams         p;

    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.isBid   = true;
    p.isElab1 = true;
    p.emit1   = emit;
    p.count   = count;
    emitMainCore(eCxxTestVectorRewind, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits TestVector Rewiding
void CxxTestbenchEmitter::emitMainCxxTestVectorRewind(UtOStream& out) {
  // Top Level Ports
  emitMainCxxTestVectorRewind(out, mTestData->getClocks(), false, eEmitNormal);
  emitMainCxxTestVectorRewind(out, mTestData->getResets(), false, eEmitNormal);
  emitMainCxxTestVectorRewind(out, mTestData->getInputs(), false, eEmitNormal);
  emitMainCxxTestVectorRewind(out, mTestData->getBids(),   true,  eEmitData);
  emitMainCxxTestVectorRewind(out, mTestData->getBids(),   true,  eEmitEnable);
  // Elaborated Nodes
  emitMainCxxTestVectorRewindElab(out, mTestData->getDeposits(), eEmitData);
  emitMainCxxTestVectorRewindElab(out, mTestData->getDeposits(), eEmitEnable);
}

// Emits TestVector Destruction (Core)
void CxxTestbenchEmitter::emitMainCxxTestVectorDestroyCore(UtOStream& out, 
                                                           UtString*  varNameCxx,
                                                           emitParams p) {
  UtString nVarName(*varNameCxx);
  UtString spacer("  ");

  if(p.isBid) {
    UtString postfix;

    extractPostfix(p.emit1, &postfix);     
    nVarName << postfix;
    if(p.isElab1) {
      makeTmpCount(&nVarName, p.count);
    }
  }
  else {
    nVarName << scNetDataPostFix;
  }
  out << spacer << "carbonmem_free(" << nVarName << "_begin);" << UtIO::endl;
}

// Emits TestVector Destruction for Top Level Ports (Shell)
void CxxTestbenchEmitter::emitMainCxxTestVectorDestroy(UtOStream&         out, 
                                                       const NUNetVector& ports, 
                                                       bool               isBid,
                                                       EmitType           emit) {
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet* net = *i;
    UtString     varNameCxx("m");
    UtString     nodeNameCxx;
    NodeVec      nList;
    emitParams   p;

    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.isBid = isBid;
    p.emit1 = emit;
    emitMainCore(eCxxTestVectorDestroy, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits TestVector Destruction for Elaborated Nodes (Shell)
void CxxTestbenchEmitter::emitMainCxxTestVectorDestroyElab(UtOStream&     out, 
                                                           const NodeVec& ports, 
                                                           EmitType       emit) {
  UInt32 count = 0;

  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++, count++) {
    STSymbolTableNode* node = *i;
    UtString           varNameCxx("m");
    UtString           nodeNameCxx;
    NodeVec            nList;
    emitParams         p;

    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.isBid   = true;
    p.isElab1 = true;
    p.emit1   = emit;
    p.count   = count;
    emitMainCore(eCxxTestVectorDestroy, out, nList.begin(), nList.end(), &varNameCxx, &nodeNameCxx, p);
  }
}

// Emits TestVector Destruction
void CxxTestbenchEmitter::emitMainCxxTestVectorDestroy(UtOStream& out) {
  // Top Level Ports
  emitMainCxxTestVectorDestroy(out, mTestData->getClocks(), false, eEmitNormal);
  emitMainCxxTestVectorDestroy(out, mTestData->getResets(), false, eEmitNormal);
  emitMainCxxTestVectorDestroy(out, mTestData->getInputs(), false, eEmitNormal);
  emitMainCxxTestVectorDestroy(out, mTestData->getBids(),   true,  eEmitData);
  emitMainCxxTestVectorDestroy(out, mTestData->getBids(),   true,  eEmitEnable);
  // Elaborated Nodes
  emitMainCxxTestVectorDestroyElab(out, mTestData->getDeposits(), eEmitData);
  emitMainCxxTestVectorDestroyElab(out, mTestData->getDeposits(), eEmitEnable);
}

//////////////////////////////////////Verilog Testbench Emitter//////////////////////////////////////

VerilogTestbenchEmitter::VerilogTestbenchEmitter(TestDriverData *d,
						 const UtString &targetName,
                                                 MsgContext *msgContext)
  : TestbenchEmitter(d, targetName),
    mTestMemoryName("cds_testbench_memory"),
    mTestMemoryIndexName("cds_testbench_memory_i"),
    mMsgContext(msgContext)
{
  mMapFile = NULL;
  // We use this to suppress multiple prints of unusupported multi-d packed array error messages
  mMultiDPackedArraysEncountered = false;
}

void TestbenchEmitter::makeHDLTempName(const NUNet* net, UtString *name, EmitType emit) {
  makeTempName(net->getName()->str(), name, emit);
}

void TestbenchEmitter::makeTempName(const char* base_name, UtString *name,
                                    EmitType emit)
{
  switch (emit) {
  case eEmitNormal:     *name = base_name; break;
  case eEmitVisibility: makeVisibilityTempName((base_name), name); break;
  case eEmitDelay:      makeDelayTempName((base_name), name); break;
  case eEmitData:       makeDataTempName((base_name), name); break;
  case eEmitEnable:     makeEnableTempName((base_name), name); break;
  case eEmitTempData:   makeTempDataTempName((base_name), name); break;
  case eEmitTempEnable: makeTempEnableTempName((base_name), name); break;
  }
}


void TestbenchEmitter::makeDelayTempName(const char* base_name, UtString *name)
{
  name->clear();
  *name << base_name << "_cds_tmp_delay";
}


void TestbenchEmitter::makeVisibilityTempName(const char* base_name, UtString *name)
{
  name->clear();
  *name << base_name << "_cds_tmp_visible";
}


void TestbenchEmitter::makeDataTempName(const char* base_name, UtString *name)
{
  name->clear();
  *name << base_name << "_cds_tmp_data";
}


void TestbenchEmitter::makeEnableTempName(const char* base_name, UtString *name)
{
  name->clear();
  *name << base_name << "_cds_tmp_enable";
}



void TestbenchEmitter::makeTempDataTempName(const char* base_name, UtString *name)
{
  makeDataTempName(base_name, name);
  *name << "_tmp";
}


void TestbenchEmitter::makeTempEnableTempName(const char* base_name, UtString *name)
{
  makeDataTempName(base_name, name);
  *name << "_tmp";
}


void VerilogTestbenchEmitter::putMapFile(UtOStream& mapFile)
{
  mMapFile = &mapFile;
}

// This class emits main.map, which is used by wavetestbenchgen.
class MapEmit
{
public:
  MapEmit(UtOStream& mapFile, VerilogTestbenchEmitter* e)
    : mMapFile(mapFile),
      mColumn(0),
      mVlogTestbenchEmitter(e)
{
  }

  template<class Container>
  void emit(Container c, bool unrollArrays)
  {
    for(typename Container::iterator p = c.begin(), e = c.end(); p != e; ++p)
    {
      writeEntry(*p, unrollArrays);
    }
  }

  void writeEntry(const STSymbolTableNode* node, bool unrollArrays)
  {
    const NUNet* net = NUNet::find(node);
    UtString name;
    node->compose(&name);
    writeEntryWithName(net, name, unrollArrays);
  }

  void writeEntry(const NUNet* net, bool unrollArrays)
  {
    UtString buf;
    UtString name;
    net->compose(&name, NULL);
    writeEntryWithName(net, name, unrollArrays);
  } // void writeEntry

private:
  UtOStream& mMapFile;
  SInt32 mColumn;
  VerilogTestbenchEmitter* mVlogTestbenchEmitter;

  void writeEntryWithName(const NUNet* net, const UtString& buf, bool unrollArrays)
  {
    // for vectors, write every bit to get the bit-blasted column map
    const NUVectorNet* vn = net->castVectorNet();
    // for arrays, unroll them (if requested)
    const NUMemoryNet* mn = unrollArrays ? net->getMemoryNet() : NULL;
    
    if (vn != NULL)
    {
      const ConstantRange* r = vn->getDeclaredRange();
      // Most of the time vectors are bigendian declared.
      SInt32 begin = r->getMsb();
      SInt32 end = r->getLsb();
      SInt32 adder = -1;
      
      if (! r->bigEndian())
        adder = 1;

      bool done = false;
      while (! done)
      {
        if (begin == end)
          // done after this iteration
          done = true;
        
        mMapFile << mColumn++ << "\t" << buf << "[" << begin << "]" << UtIO::endl;
        begin += adder;
      }
    }
    else if (mn != NULL)
    {
      mVlogTestbenchEmitter->emitVerilogMap(mMapFile, net, mColumn);
    }
    else
      mMapFile << mColumn++ << "\t" << buf << UtIO::endl;
  }
};

// This is the main method called by CodeTestDriver.cxx
// to emit main.sv, and main.map. 
// - main.sv is used to generate simulation gold files
//   for testdriver.
// - main.map is used by wavetestbenchgen 
void VerilogTestbenchEmitter::emit(UtOStream &out)
{
  // emit main.sv
  emitTopModule(out);

  out << UtIO::endl;
  out << UtIO::endl;

  emitTestbenchModule(out);

  out << UtIO::endl;
  out << UtIO::endl;

  // emit main.map
  // unroll arrays for inputs, output, and observeds
  MapEmit me(*mMapFile, this);
  bool unrollArrays = true;
  me.emit(mTestData->getClocks(),   !unrollArrays);
  me.emit(mTestData->getResets(),   !unrollArrays);
  me.emit(mTestData->getInputs(),    unrollArrays);
  me.emit(mTestData->getBids(),     !unrollArrays);
  me.emit(mTestData->getBids(),     !unrollArrays);
  me.emit(mTestData->getDeposits(), !unrollArrays);
  me.emit(mTestData->getDeposits(), !unrollArrays);
  me.emit(mTestData->getOutputs(),   unrollArrays);
  me.emit(mTestData->getBids(),     !unrollArrays);
  me.emit(mTestData->getObserveds(), unrollArrays);
}


void VerilogTestbenchEmitter::emitTopModule(UtOStream &out)
{
  emitTimescale( out );

  out << "module carbon_test_driver();" << UtIO::endl;
  out << UtIO::endl;

  emitTopWireDefinitions(out, mTestData->getClocks());
  emitTopWireDefinitions(out, mTestData->getResets());
  emitTopWireDefinitions(out, mTestData->getInputs());
  emitTopWireDefinitions(out, mTestData->getOutputs());
  emitTopWireDefinitionsBidi(out, mTestData->getBids());
  emitTopWireDefinitions(out, mTestData->getDeposits());
  out << UtIO::endl;

  emitTopEnabledDrivers(out, mTestData->getBids());
  emitTopDepositDrivers(out, mTestData->getDeposits());

  out << "  " <<  mTestData->getModule()->getName()->str() << " " <<  mTestData->getModule()->getName()->str() << "(";
  emitTopNamedPortLists(out);
  out << ");" << UtIO::endl;
  out << "  ";
  emitTestbenchModuleName(out, mTestData->getModule());
  out << " ";
  emitTestbenchModuleName(out, mTestData->getModule());
  out << "(";
  emitTestbenchNamedPortLists(out);
  out << ");" << UtIO::endl;
  out << UtIO::endl;
  out << "endmodule" << UtIO::endl;
}

static void sEmitVerilogRange(UtOStream& out, const NUNet* net)
{
  const NUVectorNet* vn = net->castVectorNet();
  if (vn)
  {
    const ConstantRange* r = vn->getDeclaredRange();
    out << "[" << r->getMsb() << ":" << r->getLsb() << "]";
  }
  else
  {
    // currently, this is probably not the right thing, but it is what
    // we have been doing. So, I don't want to change that. At some
    // point soon, we will need to fix testdrver with memories as
    // ports.
    const NUMemoryNet* memNet = net->getMemoryNet();
    UInt32 bitSize = memNet->getBitSize();
    out << "[0:" << bitSize - 1 << "]";
  }
}

// Emit packed ranges in a SystemVerilog declaration.
// Note that there may be none.
void VerilogTestbenchEmitter::emitSystemVerilogPackedRanges(UtOStream& out, const NUNet* net)
{
  if (net->isMemoryNet()) {
    const NUMemoryNet* mn = net->getMemoryNet();
    UInt32 firstUnpackedDimensionIndex = mn->getFirstUnpackedDimensionIndex();
    for (SInt32 i = firstUnpackedDimensionIndex - 1; i >= 0; --i) {
      const ConstantRange* r = mn->getRange(i);
      out << "[" << r->getMsb() << ":" << r->getLsb() << "]";
    }
  } else if (net->isVectorNet()) {
    const NUVectorNet* vn = net->castVectorNet();
    const ConstantRange* r = vn->getDeclaredRange();
    out << "[" << r->getMsb() << ":" << r->getLsb() << "]";
  }
}

// Emit unpacked ranges in a SystemVerilog declaration.
// Note that there may be none.
void VerilogTestbenchEmitter::emitSystemVerilogUnpackedRanges(UtOStream& out, const NUNet* net)
{
  // Only memory nets have unpacked ranges
  if (net->isMemoryNet()) {
    // Multi dimensional packed arrays with no unpacked dimensions have an
    // implicit unpacked [0:0] range added. Don't include this, since it's not
    // in the RTL
    const NUMemoryNet* mn = net->getMemoryNet();
    if (!mn->isMemHaveImplicitUnpackedRange()) {
      SInt32 firstUnpackedDimensionIndex = mn->getFirstUnpackedDimensionIndex();
      SInt32 numDimensions = mn->getDeclaredDimensions();
      for (SInt32 i = numDimensions - 1; i >= firstUnpackedDimensionIndex; --i) {
        const ConstantRange* r = mn->getRange(i);
        out << "[" << r->getMsb() << ":" << r->getLsb() << "]";
      }
    }
  }
}

void VerilogTestbenchEmitter::emitTopEnabledDrivers(UtOStream &out, const NUNetVector & ports)
{
  int inst_num = 0;
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++)
  {
    const NUNet *port = *i;

    out << "  bufif0 inst" << inst_num;

    // Handle vectored instances
    UInt32 bit_size = port->getBitSize();
    if (bit_size > 1) {
      sEmitVerilogRange(out, port);
    }

    const char *port_name = port->getName()->str();
    out << " (";
    out << port_name;
    out << ", ";

    UtString temp_name;
    makeDataTempName(port_name, &temp_name);
    out << temp_name;
    out << ", ";

    temp_name.clear();
    makeEnableTempName(port_name, &temp_name);
    out << temp_name;

    out << ");" << UtIO::endl;

    ++inst_num;
  }
  out << UtIO::endl;
}


void VerilogTestbenchEmitter::emitTopDepositDrivers(UtOStream &out, const NodeVec & ports)
{
  UInt32 count = 0;
  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++)
  {
    STSymbolTableNode *node = *i;
    NUNet *net = NUNet::find(node);

    UtString data_name;
    makeHDLTempName(net, &data_name, eEmitData);
    data_name << "_tmp_" << count;

    UtString enable_name;
    makeHDLTempName(net, &enable_name, eEmitEnable);
    enable_name << "_tmp_" << count;

    UtString full_name;
    node->compose(&full_name);

    // If the net is a reg type, then assign via an always block.
    // Otherwise, assign via a continuous assign.
    if (net->isReg() or net->isInteger() or net->isTime()) {
      out << "  always @(" << data_name << " or " << enable_name << ")" << UtIO::endl;
      out << "    " << full_name << " = (" << full_name << " & " << enable_name << ") | (" << data_name << " & ~" << enable_name << ");" << UtIO::endl;
    } else {
      out << "  assign " << full_name << " = (" << full_name << " & " << enable_name << ") | (" << data_name << " & ~" << enable_name << ");" << UtIO::endl;
    }

    ++count;
  }
}



void VerilogTestbenchEmitter::emitTopWireDefinitionsBidi(UtOStream &out, const NUNetVector & ports)
{
  emitTopWireDefinitions(out, ports);

  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++)
  {
    emitFormalDeclaration(out, *i, UtString("wire"), eEmitEnable);
    emitFormalDeclaration(out, *i, UtString("wire"), eEmitData);
  }
}

void VerilogTestbenchEmitter::emitTopWireDefinitions(UtOStream &out, const NUNetVector & ports)
{
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++)
    emitFormalDeclaration(out, *i, UtString("wire"), eEmitNormal);
}


void VerilogTestbenchEmitter::emitTopWireDefinitions(UtOStream &out, const NodeVec & ports)
{
  UInt32 count = 0;
  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++)
  {
    STSymbolTableNode* node = *i;
    emitFormalDeclaration(out, node, UtString("wire"), eEmitData, count);
    emitFormalDeclaration(out, node, UtString("wire"), eEmitEnable, count);
    ++count;
  }
}


void VerilogTestbenchEmitter::emitTopNamedPortLists(UtOStream &out)
{
  bool first = true;

  std::for_each(mTestData->getClocks().begin(), mTestData->getClocks().end(), TestbenchNamedPortListEmitter(out, first));
  std::for_each(mTestData->getResets().begin(), mTestData->getResets().end(), TestbenchNamedPortListEmitter(out, first));
  std::for_each(mTestData->getInputs().begin(), mTestData->getInputs().end(), TestbenchNamedPortListEmitter(out, first));
  std::for_each(mTestData->getOutputs().begin(), mTestData->getOutputs().end(), TestbenchNamedPortListEmitter(out, first));
  std::for_each(mTestData->getBids().begin(), mTestData->getBids().end(), TestbenchNamedPortListEmitter(out, first));
}


void VerilogTestbenchEmitter::emitTestbenchNamedPortLists(UtOStream &out)
{
  bool first = true;

  std::for_each(mTestData->getClocks().begin(), mTestData->getClocks().end(), TestbenchNamedPortListEmitter(out, first));
  std::for_each(mTestData->getResets().begin(), mTestData->getResets().end(), TestbenchNamedPortListEmitter(out, first));
  std::for_each(mTestData->getInputs().begin(), mTestData->getInputs().end(), TestbenchNamedPortListEmitter(out, first));
  std::for_each(mTestData->getOutputs().begin(), mTestData->getOutputs().end(), TestbenchNamedPortListEmitter(out, first));
  std::for_each(mTestData->getBids().begin(), mTestData->getBids().end(), TestbenchNamedBidPortListEmitter(out, first));
  std::for_each(mTestData->getDeposits().begin(), mTestData->getDeposits().end(), TestbenchElabDataNamedPortListEmitter(out, first));
  std::for_each(mTestData->getDeposits().begin(), mTestData->getDeposits().end(), TestbenchElabEnableNamedPortListEmitter(out, first));
}


void VerilogTestbenchEmitter::emitTestbenchModule(UtOStream &out)
{
  /*
  ** Module declaration.
  */
  out << "module ";
  emitTestbenchModuleName(out, mTestData->getModule());
  out << "(";
  emitTestbenchFormalPorts(out);
  out << ");" << UtIO::endl;
  out << UtIO::endl;

  /*
  ** Formal declarations.
  */
  emitTestbenchFormalDeclarations(out);
  emitTestbenchMemoryDeclarations(out);
  out << UtIO::endl;

  /*
  ** VCD dumping.
  */
  if (mTestData->getDumpVCD())
  {
    emitTestbenchVCDBlock(out);
  }

  /*
  ** Visibility always block.
  */
  if (mTestData->hasOutputs() or mTestData->hasBids()) {
    emitTestbenchAlwaysBlock(out);
  }
  out << UtIO::endl;

  /*
  ** Main driving code.
  */
  emitTestbenchInitialBlock(out);
  out << UtIO::endl;

  out << "endmodule" << UtIO::endl;
}


void VerilogTestbenchEmitter::emitTestbenchModuleName(UtOStream &out, NUModule *m)
{
  const char* mname = m->getName()->str();
  if (*mname == '\\') {
    // If the module name is already escaped, put the backslash on the
    // TB name at the front, remove the space at the end of the module
    // name if there is one
    ++mname;
    int len = strlen(mname);
    if (mname[len - 1] == ' ') {
      --len;
    }
    UtString stripped_mname(mname, len);
    out << "\\carbon_" << stripped_mname << "_testbench ";
  }
  else {
    out << "carbon_" << mname << "_testbench";
  }
}


void VerilogTestbenchEmitter::emitTestbenchFormalPorts(UtOStream &out)
{
  bool first = true;

  std::for_each(mTestData->getClocks().begin(), mTestData->getClocks().end(), TestbenchPortListEmitter(out, first));
  std::for_each(mTestData->getResets().begin(), mTestData->getResets().end(), TestbenchPortListEmitter(out, first));
  std::for_each(mTestData->getInputs().begin(), mTestData->getInputs().end(), TestbenchPortListEmitter(out, first));
  std::for_each(mTestData->getOutputs().begin(), mTestData->getOutputs().end(), TestbenchPortListEmitter(out, first));
  std::for_each(mTestData->getBids().begin(), mTestData->getBids().end(), TestbenchBidPortListEmitter(out, first));
  std::for_each(mTestData->getDeposits().begin(), mTestData->getDeposits().end(), TestbenchElabDataPortListEmitter(out, first));
  std::for_each(mTestData->getDeposits().begin(), mTestData->getDeposits().end(), TestbenchElabEnablePortListEmitter(out, first));
}


void VerilogTestbenchEmitter::emitTestbenchFormalDeclarations(UtOStream &out)
{
  bool delay = true;
  emitTestbenchFormalOutputDeclarations(out,  mTestData->getClocks(), delay);
  emitTestbenchFormalOutputDeclarations(out,  mTestData->getResets(), delay);
  emitTestbenchFormalOutputDeclarations(out,  mTestData->getInputs(), !delay);
  emitTestbenchFormalInputDeclarations(out,  mTestData->getOutputs());
  emitTestbenchFormalBidDeclarations(out,  mTestData->getBids(), !delay);
  emitTestbenchFormalElabDeclarations(out, mTestData->getDeposits(), !delay);
}

// Generate a formal declaration of the form:
// input logic signed [7;0] foo [7:0]
void VerilogTestbenchEmitter::emitFormalDeclaration(UtOStream &out, const NUNet * port, const UtString dec_type, EmitType emit)
{
  UtString name;

  // Indent
  out << "  ";
  
  // Directionality (if any)
  if (dec_type.size() > 0)
    out << dec_type << " ";

  // All data types are logic, because they are compatible with the integer types (bit, byte, int, integer, etc)
  out << "logic ";
  if (port->isSigned ())
    out << "signed ";

  emitSystemVerilogPackedRanges(out, port);
  makeHDLTempName(port, &name, emit);
  out << " " << name << " ";
  emitSystemVerilogUnpackedRanges(out, port);
  out << ";" << UtIO::endl;

}

void VerilogTestbenchEmitter::emitFormalDeclaration(UtOStream &out, const STSymbolTableNode * port, const UtString dec_type, EmitType emit, UInt32 count)
{
  NUNet *net = NUNet::find(port);
  UtString name;

  out << "  " << dec_type << " ";
  if (net->getBitSize() != 1)
  {
    sEmitVerilogRange(out, net);
  }
  out << "  ";

  makeHDLTempName(net, &name, emit);
  name << "_tmp_" << count;
  out << name << ";" << UtIO::endl;
}


void VerilogTestbenchEmitter::emitTestbenchFormalBidDeclarations(UtOStream &out, const NUNetVector & ports, bool delay)
{
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++)
  {
    emitFormalDeclaration(out, *i, UtString("input"), eEmitNormal);
    emitFormalDeclaration(out, *i, UtString("reg"), eEmitVisibility);
    emitFormalDeclaration(out, *i, UtString("output"), eEmitData);
    emitFormalDeclaration(out, *i, UtString("reg"), eEmitData);
    emitFormalDeclaration(out, *i, UtString("output"), eEmitEnable);
    emitFormalDeclaration(out, *i, UtString("reg"), eEmitEnable);
    if (delay)
    {
      emitFormalDeclaration(out, *i, UtString("reg"), eEmitTempData);
      emitFormalDeclaration(out, *i, UtString("reg"), eEmitTempEnable);
    }
  }
}

// Called only for deposits
void VerilogTestbenchEmitter::emitTestbenchFormalElabDeclarations(UtOStream &out, const NodeVec & ports, bool delay)
{
  UInt32 count = 0;
  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++)
  {
    const STSymbolTableNode* node = *i;
    emitFormalDeclaration(out, node, UtString("output"), eEmitData, count);
    emitFormalDeclaration(out, node, UtString("reg"), eEmitData, count);
    emitFormalDeclaration(out, node, UtString("output"), eEmitEnable, count);
    emitFormalDeclaration(out, node, UtString("reg"), eEmitEnable, count);
    if (delay)
    {
      emitFormalDeclaration(out, node, UtString("reg"), eEmitTempData, count);
      emitFormalDeclaration(out, node, UtString("reg"), eEmitTempEnable, count);
    }
    ++count;
  }
}


void VerilogTestbenchEmitter::emitTestbenchFormalOutputDeclarations(UtOStream &out, const NUNetVector & ports, bool delay)
{
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++)
  {
    emitFormalDeclaration(out, *i, UtString("output"), eEmitNormal);
    if (delay)
    {
      emitFormalDeclaration(out, *i, UtString(""), eEmitDelay);
    }
  }
}


void VerilogTestbenchEmitter::emitTestbenchFormalInputDeclarations(UtOStream &out, const NUNetVector & ports)
{
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++)
  {
    emitFormalDeclaration(out, *i, UtString("input"), eEmitNormal);
    emitFormalDeclaration(out, *i, UtString(""), eEmitVisibility);
  }
}


void VerilogTestbenchEmitter::emitTestbenchMemoryDeclarations(UtOStream &out)
{
  out << "  integer " << mTestMemoryIndexName << ";" << UtIO::endl;
  out << "  reg [0:" << (std::max(UInt32 (1), mTestData->getVectorWidth()) - 1)
      << "]  " << mTestMemoryName
      << " [0:" << (mTestData->getVectorCount() - 1)
      << "];" << UtIO::endl;
}


void VerilogTestbenchEmitter::emitTestbenchAlwaysBlock(UtOStream &out)
{

  out << "always @(*)" << UtIO::endl;
  out << "  begin" << UtIO::endl;

  emitTestbenchAlwaysBlockBody(out, mTestData->getOutputs());
  emitTestbenchAlwaysBlockBody(out, mTestData->getBids());

  out << "  end" << UtIO::endl;
}


void VerilogTestbenchEmitter::emitTestbenchAlwaysBlockSensitivity(UtOStream &out, const NUNetVector &ports, bool &first)
{
  UInt32 j = 0;
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++, j++)
  {
    if (not first) {
      out << " or ";
    }
    out << (*i)->getName()->str();
    first = false;
  }
}


void VerilogTestbenchEmitter::emitTestbenchAlwaysBlockBody(UtOStream &out, const NUNetVector &ports)
{
  UtString name;
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++)
  {
    makeVisibilityTempName((*i)->getName()->str(), &name);
    out << "    " << name << " = " << (*i)->getName()->str() << ";" << UtIO::endl;
  }
}


void VerilogTestbenchEmitter::emitTestbenchVCDBlock(UtOStream &out)
{
  out << "initial" << UtIO::endl;
  out << "  begin" << UtIO::endl;
  out << "    $dumpfile(\"" << mTestData->getModule()->getName()->str() << ".dump\");" << UtIO::endl;
  out << "    $dumpvars(0, " << mTestData->getModule()->getName()->str() << ");" << UtIO::endl;
  out << "  end" << UtIO::endl;
}


void VerilogTestbenchEmitter::emitTestbenchInitialBlock(UtOStream &out)
{
  out << UtIO::endl;
  out << "initial" << UtIO::endl;
  out << "  begin" << UtIO::endl;
  out << "    $readmemh(\"" << mTestMemoryFileName << "\", " << mTestMemoryName << ");" << UtIO::endl;
  out << "    for(" << mTestMemoryIndexName << " = 0; " 
      << mTestMemoryIndexName << " < " << mTestData->getVectorCount() << "; " 
      << mTestMemoryIndexName << " = " << mTestMemoryIndexName << " + 1)" << UtIO::endl;
  out << "      begin" << UtIO::endl;
  out << "        ";
  
  if (mTestData->getTotalNumInputs() > 0)
  {
    emitTestbenchInitialConcatExpression1(out);
    out << " = " << mTestMemoryName<< "[" << mTestMemoryIndexName << "];" << UtIO::endl;
  }

  emitTestbenchInitialConcatExpression2(out, mTestData->getClocks());
  emitTestbenchInitialConcatExpression2(out, mTestData->getResets());
  
  out << "        ";
  emitTestbenchInitialDisplayExpression(out);
  out << "      end" << UtIO::endl;
  out << "  end" << UtIO::endl;
}

// This emits a concatenation of input vectors, which is
// assigned directly from data in the test vector file. The concatenation
// consists of clocks, resets, inputs (which, if arrays, are unrolled into
// their constituent elements), bidirectionals, and deposits (if any).
// At present, only 'inputs' are unrolled. Clocks, resets, bidirectionals, etc
// are not unrolled.
void VerilogTestbenchEmitter::emitTestbenchInitialConcatExpression1(UtOStream &out)
{
  if (mTestData->getTotalNumInputs() == 0)
    return;
  
  bool first = true;

  out << "{";

  std::for_each(mTestData->getClocks().begin(), mTestData->getClocks().end(), TestbenchTempPortListEmitter(out, first));
  std::for_each(mTestData->getResets().begin(), mTestData->getResets().end(), TestbenchPortListEmitter(out, first));
  // Generate concatenation of input vectors, unrolling array elements as required.
  emitVerilogConcat(out, mTestData->getInputs(), first);
  std::for_each(mTestData->getBids().begin(), mTestData->getBids().end(), TestbenchDataPortListEmitter(out, first));
  std::for_each(mTestData->getDeposits().begin(), mTestData->getDeposits().end(), TestbenchElabDataPortListEmitter(out, first));
  std::for_each(mTestData->getBids().begin(), mTestData->getBids().end(), TestbenchEnablePortListEmitter(out, first));
  std::for_each(mTestData->getDeposits().begin(), mTestData->getDeposits().end(), TestbenchElabEnablePortListEmitter(out, first));

  out << "}";
}


void VerilogTestbenchEmitter::emitTestbenchInitialConcatExpression2(UtOStream &out, const NUNetVector & ports)
{
  bool first = true;

  if (ports.size())
  {
    out << "        {";
    first = true;
    std::for_each(ports.begin(), ports.end(), TestbenchPortListEmitter(out, first));
    out << "} = #" << mTestData->getClockDelay() << " {";
    first = true;
    std::for_each(ports.begin(), ports.end(), TestbenchTempPortListEmitter(out, first));
    out << "};" << UtIO::endl;
  }
}


void VerilogTestbenchEmitter::emitTestbenchInitialBidConcatExpression2(UtOStream &out, const NUNetVector & ports)
{
  bool first = true;

  if (ports.size())
  {
    out << "        {";
    first = true;
    std::for_each(ports.begin(), ports.end(), TestbenchEnablePortListEmitter(out, first));
    std::for_each(ports.begin(), ports.end(), TestbenchDataPortListEmitter(out, first));
    out << "} = #" << mTestData->getClockDelay() << " {";
    first = true;
    std::for_each(ports.begin(), ports.end(), TestbenchTempEnablePortListEmitter(out, first));
    std::for_each(ports.begin(), ports.end(), TestbenchTempDataPortListEmitter(out, first));
    out << "};" << UtIO::endl;
  }
}


void VerilogTestbenchEmitter::emitTestbenchInitialDepositConcatExpression2(UtOStream &out, const NodeVec & ports)
{
  bool first = true;

  if (ports.size())
  {
    out << "        {";
    first = true;
    std::for_each(ports.begin(), ports.end(), TestbenchElabEnablePortListEmitter(out, first));
    std::for_each(ports.begin(), ports.end(), TestbenchElabDataPortListEmitter(out, first));
    out << "} = #" << mTestData->getClockDelay() << " {";
    first = true;
    std::for_each(ports.begin(), ports.end(), TestbenchTempElabEnablePortListEmitter(out, first));
    std::for_each(ports.begin(), ports.end(), TestbenchTempElabDataPortListEmitter(out, first));
    out << "};" << UtIO::endl;
  }
}

// This emits the concatenations in a display statement of the form:
//
// #100 $display("CARBON: %b%b", {<concat of inputs>}, {<concat of outputs>});

void VerilogTestbenchEmitter::emitTestbenchInitialDisplayExpression(UtOStream &out)
{
  out << "#100 $display(\"CARBON: ";

  // Output a concatentation of inputs, and a concatenation of outputs,
  // each to be displayed by $display.
  // SystemVerilog inputs and outputs with packed and unpacked dimensions
  // are supported by unrolling the array, creating a concatenation of
  // individual unpacked array elements.
  // For now, clocks, resets, deposited, and bidirectional signals cannot be
  // multi-dimensional arrays.

  // Clocks, resets, inputs, bidis, and deposits (if any)
  bool first = true; // Controls commas
  if (mTestData->getTotalNumInputs() > 0) {
    out << (mTestData->getDebug() ? "%h%h" : "%b%b") << "\", ";
    out << "{";
    std::for_each(mTestData->getClocks().begin(), mTestData->getClocks().end(), TestbenchPortListEmitter(out, first));
    std::for_each(mTestData->getResets().begin(), mTestData->getResets().end(), TestbenchPortListEmitter(out, first));
    emitVerilogConcat(out, mTestData->getInputs(), first);
    std::for_each(mTestData->getBids().begin(), mTestData->getBids().end(), TestbenchDataPortListEmitter(out, first));
    std::for_each(mTestData->getBids().begin(), mTestData->getBids().end(), TestbenchEnablePortListEmitter(out, first));
    std::for_each(mTestData->getDeposits().begin(), mTestData->getDeposits().end(), TestbenchElabDataPortListEmitter(out, first));
    std::for_each(mTestData->getDeposits().begin(), mTestData->getDeposits().end(), TestbenchElabEnablePortListEmitter(out, first));
    out << "},";
  } else {
    out << (mTestData->getDebug() ? "%h" : "%b") << "\", ";
  }

  // Outputs, bidis, and observed signals.
  out << " {";
  first = true; // Start a new concatenation
  emitVerilogConcat(out, mTestData->getOutputs(), first);
  std::for_each(mTestData->getBids().begin(), mTestData->getBids().end(), TestbenchPortListEmitter(out, first));
  emitVerilogConcatElab(out, mTestData->getObserveds(), first); // Use full path name for observed signals
  out << "}";

  out << ");" << UtIO::endl;
}


void
VerilogTestbenchEmitter::emitTimescale( UtOStream &out )
{
  if ( mTestData->getTimescaleSpecified() == true )
  {
    // The value of the timeUnit and timePrecision is the power of 10
    // being represented.  The addition of 15 is to normalize this to be
    // able to look up the appropriate string in the HdlTime::VerilogTimeUnits table
    SInt32 unit = mTestData->getTimeUnit() + 15;
    INFO_ASSERT( unit >= 0 && unit < 18, "invalid time scale" );
    SInt32 precision = mTestData->getTimePrecision() + 15;
    INFO_ASSERT (precision >= 0 && precision < 18, "invalid time precision" );
    out << "`timescale " << HdlTime::VerilogTimeUnits[ unit ] << "/"
        << HdlTime::VerilogTimeUnits[ precision ] << "\n" << UtIO::endl;
  }
}

// Output a Verilog concat item.
// This is called by emitVerilogConcat, through the recursive TestbenchEmitter methods:
//   emitMainCore
//   emitMainCoreRecursion
//   emitMainCoreArray
// These methods perform a variety of operations, but in particular they unroll
// array dimensions. This 'unrolling' is used by CxxTestbenchEmitter to deposit
// test vector values on model inputs, one array element at a time. We want to
// make sure to use the same ordering in main.sv as is used in main.cxx
void
VerilogTestbenchEmitter::emitVerilogConcatCore(UtOStream& out, UtString* var, emitParams p)
{
  if (*p.first)
    out << *var;
  else
    out << "," << *var;
  (*p.first) = false;
}

// Emits concatenations of SystemVerilog variables, wires, and array elements, 
// ultimately calling emitVerilogConcatCore. (See comments there).
void
VerilogTestbenchEmitter::emitVerilogConcat(UtOStream& out, const NUNetVector& ports, bool& first)
{
  // Initialize the boolean that keeps track of whether this is the first
  // item in the concat, or not
  bool firstItem = first;
  
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet*            net = *i;
    UtString                varNameVlog("m");
    UtString                nodeNameVlog;
    NodeVec                 nList;
    emitParams              p;
    NodeVec::const_iterator s;

    extractNUNetInfo(net, &nList);

    // No hierarchical name, skip past to level module name
    s = nList.begin();
    s++;

    clearEmitParams(&p);
    p.emit1      = eEmitNormal;
    p.bitSize    = net->getBitSize();
    p.isVector   = net->isVectorNet();
    p.first      = &firstItem; // Controls commas
    emitMainCore(eVerilogConcat, out, s, nList.end(), &varNameVlog, &nodeNameVlog, p);
    // Retain for next loop iteration
    firstItem = *p.first;
  }

  // Return indication of whether this is the first concat or not
  first = firstItem;
}

// Output an observed signal concat item, with comma control, and full path name to observed signal
void
VerilogTestbenchEmitter::emitVerilogConcatElab(UtOStream& out, const NodeVec& ports, bool& first)
{
  // Initialize the boolean that keeps track of whether this is the first
  // item in the concat, or not
  bool firstItem = first;

  for(NodeVec::const_iterator i = ports.begin(); i != ports.end(); i++) {
    STSymbolTableNode* node = *i;
    UtString           varNameVlog("m");
    UtString           nodeNameVlog;
    NodeVec            nList;
    emitParams         p;

    extractSTNodeInfo(node, &nList);
    clearEmitParams(&p);
    p.first      = &firstItem; // Controls commas
    emitMainCore(eVerilogConcat, out, nList.begin(), nList.end(), &varNameVlog, &nodeNameVlog, p);
    // Retain for next loop iteration
    firstItem = *p.first;
  }

  // Return indication of whether this is the first concat or not
  first = firstItem;
}

// Output a signal/variable into the 'main.map' file, unrolling
// multi-dimensional unpacked arrays.
void
VerilogTestbenchEmitter::emitVerilogMap(UtOStream& out, const NUNet* net, SInt32& column)
{
  UtString                varNameVlog("m");
  UtString                nodeNameVlog;
  NodeVec                 nList;
  emitParams              p;
  NodeVec::const_iterator s;
  
  extractNUNetInfo(net, &nList);
  
  // No hierarchical name, skip past to level module name
  s = nList.begin();
  s++;
  
  clearEmitParams(&p);
  p.emit1      = eEmitNormal;
  p.bitSize    = net->getBitSize();
  p.isVector   = net->isVectorNet();
  p.column     = &column; // Column count for output control
  emitMainCore(eVerilogMap, out, s, nList.end(), &varNameVlog, &nodeNameVlog, p);
  column = *p.column;
}

// This is called by 'emitVerilogMap', through the 'emitMainCore' recursive
// function. It emits unrolled unpacked arrays into the 'main.map' file.
void
VerilogTestbenchEmitter::emitVerilogMapCore(UtOStream& out, UtString* buf, emitParams p)
{
  out << *(p.column)++ <<"\t" << buf << UtIO::endl;
}

///////////////////////////////////////VHDL Testbench Emitter////////////////////////////////////////

// Vhdl Testbench Emitter
VhdlTestbenchEmitter::VhdlTestbenchEmitter(TestDriverData*        d,
                                           const UtString&        targetName,
                                           const VhdlPortTypeMap* portTypeMap) : 
  TestbenchEmitter(d, targetName),
  mTestMemoryName("cds_input_vector"),
  mPortTypeMap(portTypeMap),
  mFirstPortDecl(true) 
{
}

void VhdlTestbenchEmitter::putMapFile(UtOStream& mapFile) {
  mMapFile = &mapFile;
}

// Gets Entity Name
void VhdlTestbenchEmitter::getEntityName(NUModule* m, 
                                         UtString* name) {
  (*name) << *m->getName();
}

// Gets Testbench Entity Name
void VhdlTestbenchEmitter::getTBEntityName(NUModule* m, 
                                           UtString* name) {
  (*name) << "carbon_" << *m->getName() << "_testbench";
}

void VhdlTestbenchEmitter::getTypeStr(const UserType* userType, 
                                      UtString        portName,
                                      UtString*       typeStr,
                                      bool*           ranged,
                                      SInt32*         msb) {
  const VhdlPortType* portType = mPortTypeMap->getPortType(portName);
  UtString::size_type pos;

  *ranged = false;

  // User Type Available
  if(userType) {
    UtString       todto;
    UserType::Type nType = userType->getType();

    // Emit Type
    *typeStr << userType->getTypeName()->str();

    // Emit Range, if any
    // Scalars
    if(nType == UserType::eScalar) {
      const UserScalar*    scalarType    = userType->castScalar();
      const ConstantRange* range         = scalarType->getRangeConstraint();
      bool  isRangeRequiredInDeclaration = scalarType->isRangeRequiredInDeclaration();

      if(range) {
	if (isRangeRequiredInDeclaration) {
	  if(range->bigEndian()) 
	    todto << " downto ";
	  else
	    todto << " to ";
	  *typeStr << " range " << range->getMsb() << todto << range->getLsb();
	}
        *ranged = true;
        *msb    = range->getMsb();
      }
    }
    // Arrays
    if(nType == UserType::eArray) {
      const UserArray*     arrayType     = userType->castArray();
      const ConstantRange* range         = arrayType->getRange();
      bool  isRangeRequiredInDeclaration = arrayType->isRangeRequiredInDeclaration();

      if(range && isRangeRequiredInDeclaration) {
	bool multi;

	*typeStr << "(";
	
	do {
	  multi = false;
	  
	  *typeStr << range->getMsb();
	  if(range->bigEndian()) 
	    *typeStr << " downto ";
	  else
	    *typeStr << " to ";
	  *typeStr << range->getLsb();
	  
	  // Check for Multi-Dimensional Arrays
	  const UserType* elementType = arrayType->getDimElementType(0);
	  UserType::Type  elType      = elementType->getType();
	  if((elType == UserType::eArray) &&
	     (arrayType->getTypeName() == elementType->getTypeName())) {
	    arrayType                    = elementType->castArray();
	    range                        = arrayType->getRange();
	    isRangeRequiredInDeclaration = arrayType->isRangeRequiredInDeclaration();
	    if(range && isRangeRequiredInDeclaration) {
	      multi = true;
	      *typeStr << ", ";
	    }
	  }
	} while(multi);
	
	*typeStr << ")";
      }
    }
    // Enums
    else if(nType == UserType::eEnum) {
      const UserEnum*      enumType      = userType->castEnum();
      const ConstantRange* range         = enumType->getRange();
      bool  isRangeRequiredInDeclaration = enumType->isRangeRequiredInDeclaration();

      if(range) {
	if (isRangeRequiredInDeclaration) {
	  if(range->bigEndian()) 
	    todto << " downto ";
	  else
	    todto << " to ";
	  *typeStr << " range " << range->getMsb() << todto  << range->getLsb();
	}
        *ranged = true;
        *msb    = range->getMsb();
      }
    }
  }
  // Port Type Available
  else if(portType) {
    *typeStr = portType->getTypeStr();

    if(*typeStr != NULL) {
      if((pos = (*typeStr).find("( ")) != UtString::npos)
        (*typeStr).replace(pos, 2, "(");
      if((pos = (*typeStr).find(" )")) != UtString::npos)
        (*typeStr).replace(pos, 2, ")");
    }
  }  
  // No User Type, No Port Type
  else
    *typeStr << DefaultVhdlDataType;
}

// Checks if Port Type is Buffer
bool VhdlTestbenchEmitter::isBuffer(UtString portName) {
  const VhdlPortType* portType = NULL;

  portType = mPortTypeMap->getPortType(portName);
  if(portType &&
     (portType->getDirection() == VhdlPortType::eBuffer))
    return true;
  else
    return false;
}

// Resolves Multi-Dimentional Array Representation
void VhdlTestbenchEmitter::resolveArrayRep(UtString* portName) {
  UtString::size_type pos;

  while((pos = (*portName).find(",(")) != UtString::npos) 
    (*portName).replace(pos, 2, ",");  
  if((*portName)[(*portName).length()-1] == ',')
    (*portName)[(*portName).length()-1] = ')';
}

// Extracts Port Name from Node Name
void VhdlTestbenchEmitter::extractPortName(UtString  nodeNameVhdl,
                                           UtString* portName) {
  UtString::size_type pos;

  if((pos = nodeNameVhdl.find_first_of(".")) != UtString::npos)
    (*portName).append(nodeNameVhdl, pos+1, nodeNameVhdl.length()-pos-1);
  else
    (*portName) << nodeNameVhdl;
}

// Get Conversion Type Name
void VhdlTestbenchEmitter::getConversionTypeName(const UserType* userType,
                                                 UtString*       conversionTypeName) {
  UtString::size_type pos;

  if(userType == NULL)
    return;

  if(userType->getType() == UserType::eEnum) {
    // Enum provide their own conversion functions;
    *conversionTypeName = userType->getTypeName()->str();
  }
  else if(userType->getVhdlType() == eVhdlTypeIntType) {
    // INTTYPEs must provide their own conversion function
    *conversionTypeName = userType->getTypeName()->str();
  }
  else {
    // Use the intrinsic conversion function
    *conversionTypeName = userType->getIntrinsicTypeName()->str();
  }

  // Convert std_logic        => StdLogic
  //         std_ulogic       => StdULogic
  //         std_logic_vector => StdLogicVector
  while((pos = (*conversionTypeName).find("_")) != UtString::npos)
    (*conversionTypeName).erase(pos, 1);
}

// Check if Single Bit
bool VhdlTestbenchEmitter::isSingle(UInt32          bitSize, 
                                    const UserType* userType,
                                    bool            isVector) {
  const UserEnum* enumType = NULL;

  if((bitSize > 1)                                                     ||
     // Considering Arrays of Size 1 using User Type       
     ((userType != NULL) && (userType->getType() == UserType::eArray)) || 
     // Considering Vectors of Size 1 when No User Type Available
     ((userType == NULL) && isVector)                                  ||
     // Considering Enum with more than 2 Items
     ((userType != NULL) && (userType->getType() == UserType::eEnum) && 
      (enumType = userType->castEnum()) && (enumType->getNumberElems() > 2)))
    return false;
    // Otherwise
  else
    return true;
}

UtString VhdlTestbenchEmitter::initializeArray(const UserType* userType) {
  if (userType->getType() == UserType::eArray) {
    const UserArray* arrayType = userType->castArray();
    const UserType* elementType = arrayType->getDimElementType(0);
    if (elementType->getType() == UserType::eArray) {
      return UtString("(others => ") + initializeArray(elementType) + UtString(")");
    }
  }

  return "(others => '0')";
}

// Emits Vhdl Testbench (main.vhd)
void VhdlTestbenchEmitter::emit(UtOStream& out) {
  emitMainVhdlTBConversionPackage(out);
  out << UtIO::endl;

  emitMainVhdlTBEntity(out);
  out << UtIO::endl;

  emitMainVhdlTBArch(out);
  out << UtIO::endl;

  emitMainVhdlTopEntity(out);
  out << UtIO::endl;

  emitMainVhdlTopArch(out);
  out << UtIO::endl;
}

// Emits Vhdl Testbench Conversion Package
void VhdlTestbenchEmitter::emitMainVhdlTBConversionPackage(UtOStream& out) {
  bool hasStdLogicArith = false;
  bool hasNumericBit    = false;

  out << "library ieee;"                << UtIO::endl;
  out << "use ieee.std_logic_1164.all;" << UtIO::endl;

  if(mPortTypeMap->hasIEEEPackage("std_logic_arith")) {
    out << "use ieee.std_logic_arith.all;" << UtIO::endl;
    hasStdLogicArith = true;
  }  
  else if(mPortTypeMap->hasIEEEPackage("numeric_bit")) {
    out << "use ieee.numeric_bit.all;"     << UtIO::endl;
    hasNumericBit = true;
  }
  else {
    out << "use ieee.numeric_std.all;"     << UtIO::endl;
  }

  out << UtIO::endl;
  out << "package carbon_conv is"                                                            << UtIO::endl;
  out << "  -- Conversion Functions Provided"                                                << UtIO::endl;
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << "  -- From              |To                |Function"                               << UtIO::endl;
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << "  -- std_logic         |boolean           |To_Boolean"                             << UtIO::endl;
  out << "  -- boolean           |std_logic         |To_StdLogic"                            << UtIO::endl;
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << "  -- std_logic         |bit               |ieee.std_logic_1164.To_bit"             << UtIO::endl;
  out << "  -- bit               |std_logic         |To_StdLogic"                            << UtIO::endl;
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << "  -- std_logic         |std_ulogic        |To_StdULogic"                           << UtIO::endl;
  out << "  -- std_ulogic        |std_logic         |To_StdLogic"                            << UtIO::endl;
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << "  -- std_logic_vector  |bit_vector        |ieee.std_logic_1164.To_bitvector"       << UtIO::endl;
  out << "  -- bit_vector        |std_logic_vector  |ieee.std_logic_1164.To_StdLogicVector"  << UtIO::endl;
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << "  -- std_logic_vector  |std_ulogic_vector |ieee.std_logic_1164.To_StdULogicVector" << UtIO::endl;
  out << "  -- std_ulogic_vector |std_logic_vector  |ieee.std_logic_1164.To_StdLogicVector " << UtIO::endl; 
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << "  -- std_logic_vector  |character         |To_Character"                           << UtIO::endl;
  out << "  -- character         |std_logic_vector  |To_StdLogicVector"                      << UtIO::endl;
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << "  -- std_logic_vector  |integer           |To_Integer"                             << UtIO::endl;
  out << "  -- integer           |std_logic_vector  |To_StdLogicVector"                      << UtIO::endl;
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << "  -- std_logic_vector  |natural           |To_Natural"                             << UtIO::endl;
  out << "  -- natural           |std_logic_vector  |To_StdLogicVector"                      << UtIO::endl;
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << "  -- std_logic_vector  |signed            |To_Signed"                              << UtIO::endl;
  out << "  -- signed            |std_logic_vector  |To_StdLogicVector"                      << UtIO::endl;
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << "  -- std_logic_vector  |unsigned          |To_UnSigned"                            << UtIO::endl;
  out << "  -- unsigned          |std_logic_vector  |To_StdLogicVector"                      << UtIO::endl;
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << "  -- std_logic_vector  |string            |To_String"                              << UtIO::endl;
  out << "  -- string            |std_logic_vector  |To_StdLogicVector"                      << UtIO::endl;
  out << "  -------------------------------------------------------------------------------" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic"                                      << UtIO::endl;
  out << "  -- To:   boolean"                                        << UtIO::endl;
  out << "  function To_Boolean (param : std_logic) return boolean;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: boolean"                                         << UtIO::endl;
  out << "  -- To:   std_logic"                                       << UtIO::endl;
  out << "  function To_StdLogic (param : boolean) return std_logic;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: bit"                                         << UtIO::endl;
  out << "  -- To:   std_logic"                                   << UtIO::endl;
  out << "  function To_StdLogic (param : bit) return std_logic;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic"                                           << UtIO::endl;
  out << "  -- To:   std_ulogic"                                          << UtIO::endl;
  out << "  function To_StdULogic (param : std_logic) return std_ulogic;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_ulogic"                                         << UtIO::endl;
  out << "  -- To:   std_logic"                                          << UtIO::endl;
  out << "  function To_StdLogic (param : std_ulogic) return std_logic;" << UtIO::endl; 
  out << UtIO::endl;

  out << "  -- From: std_logic_vector"                                          << UtIO::endl;
  out << "  -- To:   character"                                                 << UtIO::endl;
  out << "  function To_Character (param : std_logic_vector) return character;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: character"                                                      << UtIO::endl;
  out << "  -- To:   std_logic_vector"                                               << UtIO::endl;
  out << "  function To_StdLogicVector (param : character) return std_logic_vector;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic_vector"                                                      << UtIO::endl;
  out << "  -- To:   integer"                                                               << UtIO::endl;
  out << "  function To_Integer (param : std_logic_vector; sign : boolean) return integer;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: integer"                                                      << UtIO::endl;
  out << "  -- To:   std_logic_vector"                                             << UtIO::endl;
  out << "  function To_StdLogicVector (param : integer; size : integer; sign : boolean) return std_logic_vector;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic_vector"                                      << UtIO::endl;
  out << "  -- To:   natural"                                               << UtIO::endl;
  out << "  function To_Natural (param : std_logic_vector) return natural;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic_vector"                                        << UtIO::endl;
  out << "  -- To:   positive"                                                << UtIO::endl;
  out << "  function To_Positive (param : std_logic_vector) return positive;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic_vector"                                    << UtIO::endl;
  out << "  -- To:   signed"                                              << UtIO::endl;
  out << "  function To_Signed (param : std_logic_vector) return signed;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: signed"                                                      << UtIO::endl;
  out << "  -- To:   std_logic_vector"                                            << UtIO::endl;
  out << "  function To_StdLogicVector (param : signed) return std_logic_vector;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic_vector"                                        << UtIO::endl;
  out << "  -- To:   unsigned"                                                << UtIO::endl;
  out << "  function To_UnSigned (param : std_logic_vector) return unsigned;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: unsigned"                                                      << UtIO::endl;
  out << "  -- To:   std_logic_vector"                                              << UtIO::endl;
  out << "  function To_StdLogicVector (param : unsigned) return std_logic_vector;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic_vector"                                    << UtIO::endl;
  out << "  -- To:   string"                                              << UtIO::endl;
  out << "  function To_String (param : std_logic_vector) return string;" << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: string"                                                      << UtIO::endl;
  out << "  -- To:   std_logic_vector "                                           << UtIO::endl; 
  out << "  function To_StdLogicVector (param : string) return std_logic_vector;" << UtIO::endl;
  out << "end carbon_conv;"                                                       << UtIO::endl;
  out << UtIO::endl;

  out << "package body carbon_conv is"                                 << UtIO::endl;
  out << "  -- From: std_logic"                                        << UtIO::endl;
  out << "  -- To:   boolean"                                          << UtIO::endl;
  out << "  function To_Boolean (param : std_logic) return boolean is" << UtIO::endl;
  out << "  begin"                                                     << UtIO::endl;
  out << "    if to_bit(param, '0') = '1' then"                        << UtIO::endl;
  out << "      return true;"                                          << UtIO::endl;
  out << "    end if;"                                                 << UtIO::endl;
  out << "    return false;"                                           << UtIO::endl;
  out << "  end To_Boolean;"                                           << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: boolean"                                           << UtIO::endl;
  out << "  -- To:   std_logic"                                         << UtIO::endl;
  out << "  function To_StdLogic (param : boolean) return std_logic is" << UtIO::endl;
  out << "  begin"                                                      << UtIO::endl;
  out << "    if param then"                                            << UtIO::endl;
  out << "      return '1';"                                            << UtIO::endl;
  out << "    end if;"                                                  << UtIO::endl;
  out << "    return '0';"                                              << UtIO::endl;
  out << "  end To_StdLogic;"                                           << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic"                                     << UtIO::endl;
  out << "  -- To:   bit"                                           << UtIO::endl;
  out << "  function To_StdLogic (param : bit) return std_logic is" << UtIO::endl;
  out << "  begin  -- To_StdLogic"                                  << UtIO::endl;
  out << "    case param is"                                        << UtIO::endl;
  out << "      when '0' => return '0';"                            << UtIO::endl;
  out << "      when '1' => return '1';"                            << UtIO::endl;
  out << "    end case;"                                            << UtIO::endl;
  out << "  end To_StdLogic;"                                       << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic"                                             << UtIO::endl;
  out << "  -- To:   std_ulogic"                                            << UtIO::endl;
  out << "  function To_StdULogic (param : std_logic) return std_ulogic is" << UtIO::endl;
  out << "  begin"                                                          << UtIO::endl;
  out << "    return std_ulogic(param);"                                    << UtIO::endl;
  out << "  end To_StdULogic;"                                              << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_ulogic"                                           << UtIO::endl;
  out << "  -- To:   std_logic"                                            << UtIO::endl;
  out << "  function To_StdLogic (param : std_ulogic) return std_logic is" << UtIO::endl;
  out << "  begin  -- To_StdLogic"                                         << UtIO::endl;
  out << "    return std_logic(param);"                                    << UtIO::endl;
  out << "  end To_StdLogic;"                                              << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic_vector"                                            << UtIO::endl;
  out << "  -- To:   character"                                                   << UtIO::endl;
  out << "  function To_Character (param : std_logic_vector) return character is" << UtIO::endl;
  out << "  begin"                                                                << UtIO::endl;
  if(hasStdLogicArith)
    out << "    return character'val(conv_integer(unsigned(param)));"             << UtIO::endl;
  else if(hasNumericBit)
    out << "    return character'val(to_integer(unsigned(to_bitvector(param))));" << UtIO::endl;
  else
    out << "    return character'val(to_integer(unsigned(param)));"               << UtIO::endl;
  out << "  end To_Character;"                                                    << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: character"                                                        << UtIO::endl;
  out << "  -- To:   std_logic_vector"                                                 << UtIO::endl;
  out << "  function To_StdLogicVector (param : character) return std_logic_vector is" << UtIO::endl;
  out << "  begin"                                                                     << UtIO::endl;
  if(hasStdLogicArith)
    out << "    return std_logic_vector(conv_unsigned(character'pos(param),8));"       << UtIO::endl;
  else if (hasNumericBit)
    out << "    return to_stdlogicvector(to_unsigned(character'pos(param),8));"        << UtIO::endl;
  else
    out << "    return std_logic_vector(to_unsigned(character'pos(param),8));"         << UtIO::endl;
  out << "  end To_StdLogicVector;"                                                    << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic_vector"                                                        << UtIO::endl;
  out << "  -- To:   integer"                                                                 << UtIO::endl;
  out << "  function To_Integer (param : std_logic_vector; sign : boolean) return integer is" << UtIO::endl;
  out << "  begin"                                                                            << UtIO::endl;
  out << "    if sign = true then"                                                            << UtIO::endl;
  if(hasStdLogicArith)
    out << "       return conv_integer(signed(param));"                                       << UtIO::endl;
  else if(hasNumericBit)
    out << "       return to_integer(signed(to_bitvector(param)));"                           << UtIO::endl;
  else
    out << "       return to_integer(signed(param));"                                         << UtIO::endl;
  out << "    else"                                                                           << UtIO::endl;
  if(hasStdLogicArith)
    out << "       return conv_integer(unsigned(param));"                                     << UtIO::endl;
  else if(hasNumericBit)
    out << "       return to_integer(unsigned(to_bitvector(param)));"                         << UtIO::endl;
  else
    out << "       return to_integer(unsigned(param));"                                       << UtIO::endl;
  out << "    end if;"                                                                        << UtIO::endl;
  out << "  end To_Integer;"                                                                  << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: integer"                                                                                        << UtIO::endl;
  out << "  -- To:   std_logic_vector"                                                                               << UtIO::endl;
  out << "  function To_StdLogicVector (param : integer; size : integer; sign : boolean) return std_logic_vector is" << UtIO::endl;
  out << "  begin"                                                                                                   << UtIO::endl;
  out << "    if sign = true then"                                                                                   << UtIO::endl;
  if(hasStdLogicArith)
    out << "       return std_logic_vector(conv_signed(param, size));"                                               << UtIO::endl;
  else if(hasNumericBit)
    out << "       return to_stdlogicvector(bit_vector(to_signed(param, size)));"                                    << UtIO::endl;
  else
    out << "       return std_logic_vector(to_signed(param, size));"                                                 << UtIO::endl;
  out << "    else"                                                                                                  << UtIO::endl;
  if(hasStdLogicArith)
    out << "       return std_logic_vector(conv_unsigned(param, size));"                                             << UtIO::endl;
  else if(hasNumericBit)
    out << "       return to_stdlogicvector(bit_vector(to_unsigned(param, size)));"                                  << UtIO::endl;
  else
    out << "       return std_logic_vector(to_unsigned(param, size));"                                               << UtIO::endl;
  out << "    end if;"                                                                                               << UtIO::endl;
  out << "  end To_StdLogicVector;"                                                                                  << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic_vector"                                        << UtIO::endl;
  out << "  -- To:   natural"                                                 << UtIO::endl;
  out << "  function To_Natural (param : std_logic_vector) return natural is" << UtIO::endl;
  out << "  begin"                                                            << UtIO::endl;
  if(hasStdLogicArith)
    out << "    return conv_integer(unsigned(param));"                        << UtIO::endl;
  else if(hasNumericBit)
    out << "    return to_integer(unsigned(to_bitvector(param)));"            << UtIO::endl;
  else
    out << "    return to_integer(unsigned(param));"                          << UtIO::endl;
  out << "  end To_Natural;"                                                  << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic_vector"                                          << UtIO::endl;
  out << "  -- To:   positive"                                                  << UtIO::endl;
  out << "  function To_Positive (param : std_logic_vector) return positive is" << UtIO::endl;
  out << "  begin"                                                              << UtIO::endl;
  if(hasStdLogicArith)
    out << "    return conv_integer(unsigned(param));"                          << UtIO::endl;
  else if(hasNumericBit)
    out << "    return to_integer(unsigned(to_bitvector(param)));"              << UtIO::endl;
  else
    out << "    return to_integer(unsigned(param));"                            << UtIO::endl;
  out << "  end To_Positive;"                                                   << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: std_logic_vector"                                      << UtIO::endl;
  out << "  -- To:   signed"                                                << UtIO::endl;
  out << "  function To_Signed (param : std_logic_vector) return signed is" << UtIO::endl;
  out << "  begin  -- To_Signed"                                            << UtIO::endl;
  if(hasNumericBit)
    out << "    return to_signed(to_integer(param, true), param'length);"   << UtIO::endl;
  else
    out << "    return signed(param);"                                      << UtIO::endl;
  out << "  end To_Signed;"                                                 << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: signed"                                                        << UtIO::endl;
  out << "  -- To:   std_logic_vector"                                              << UtIO::endl;
  out << "  function To_StdLogicVector (param : signed) return std_logic_vector is" << UtIO::endl;
  out << "  begin  -- To_StdLogicVector"                                            << UtIO::endl;
  if(hasNumericBit)
    out << "    return to_stdlogicvector(bit_vector(param));"                       << UtIO::endl;
  else
    out << "    return std_logic_vector(param);"                                    << UtIO::endl;
  out << "  end To_StdLogicVector;"                                                 << UtIO::endl;
  out <<  UtIO::endl; 

  out << "  -- From: std_logic_vector"                                          << UtIO::endl;
  out << "  -- To:   unsigned"                                                  << UtIO::endl;
  out << "  function To_UnSigned (param : std_logic_vector) return unsigned is" << UtIO::endl;
  out << "  begin  -- To_UnSigned"                                              << UtIO::endl;
  if(hasNumericBit)
    out << "    return to_unsigned(to_integer(param, false), param'length);"    << UtIO::endl;
  else
    out << "    return unsigned(param);"                                        << UtIO::endl;
  out << "  end To_UnSigned;"                                                   << UtIO::endl;
  out << UtIO::endl;

  out << "  -- From: unsigned"                                                        << UtIO::endl;
  out << "  -- To:   std_logic_vector"                                                << UtIO::endl;
  out << "  function To_StdLogicVector (param : unsigned) return std_logic_vector is" << UtIO::endl;
  out << "  begin  -- To_StdLogicVector"                                              << UtIO::endl;
  if(hasNumericBit)
    out << "    return to_stdlogicvector(bit_vector(param));"                         << UtIO::endl;  
  else
    out << "    return std_logic_vector(param);"                                      << UtIO::endl;  
  out << "  end To_StdLogicVector;"                                                   << UtIO::endl;
  out <<  UtIO::endl;

  out << "  -- From: std_logic_vector"                                      << UtIO::endl;
  out << "  -- To:   string"                                                << UtIO::endl;
  out << "  function To_String (param : std_logic_vector) return string is" << UtIO::endl;
  out << "    variable result : string(1 to param'length/8);"               << UtIO::endl;
  out << "  begin  -- To_String"                                            << UtIO::endl;
  out << "    for i in 0 to param'length/8-1 loop"                          << UtIO::endl;
  out << "      result(i+1) := To_Character(param(i*8 to i*8+7));"          << UtIO::endl;
  out << "    end loop;  -- i"                                              << UtIO::endl;
  out << "    return result;"                                               << UtIO::endl;
  out << "  end To_String;"                                                 << UtIO::endl;
  out <<  UtIO::endl; 

  out << "  -- From: string"                                                             << UtIO::endl;
  out << "  -- To:   std_logic_vector"                                                   << UtIO::endl;
  out << "  function To_StdLogicVector (param : string) return std_logic_vector is"      << UtIO::endl;
  out << "    variable result : std_logic_vector(0 to param'length*8-1);"                << UtIO::endl;
  out << "  begin  -- To_StdLogicVector"                                                 << UtIO::endl;
  out << "    for i in 1 to param'length loop"                                           << UtIO::endl;
  out << "      result((i-1)*8 to (i-1)*8+7) := To_StdLogicVector(character(param(i)));" << UtIO::endl;
  out << "    end loop;  -- i"                                                           << UtIO::endl;
  out << "    return result;"                                                            << UtIO::endl;
  out << "  end To_StdLogicVector;"                                                      << UtIO::endl;
  out << "end carbon_conv;"                                                              << UtIO::endl;
}

// Emits Vhdl TestBench Enity
void VhdlTestbenchEmitter::emitMainVhdlTBEntity(UtOStream& out) {
  UtString         tbEntityName;
  UtList<UtString> packages;
  UtList<UtString> ieeePackages;
  UtString         indent;

  out << "library ieee;"       << UtIO::endl;
  out << "use std.textio.all;" << UtIO::endl;

  if(!mPortTypeMap->hasIEEEPackage("std_logic_1164")) 
    out << "use ieee.std_logic_1164.all;" << UtIO::endl;

  if(!mPortTypeMap->hasIEEEPackage("std_logic_textio")) 
    out << "use ieee.std_logic_textio.all; -- needed for hread" << UtIO::endl;

  // Add the necessary IEEE packages
  mPortTypeMap->getIEEEPackages(&ieeePackages);
  for(UtList<UtString>::iterator p = ieeePackages.begin(); p != ieeePackages.end(); p++) {
    out << "use ieee." << *p << ".all;" << UtIO::endl;
  }

  // Include the novas package 
  // (defined in ${DEBUSSY_ROOT}/shared/PLI/LINUX/novas.vhd)
  // for fsdb dump commands.
  if(mTestData->getDumpFSDB()) 
    out << "use work.pkg.all;" << UtIO::endl;

  // Add use clauses for any packages that contain the type 
  // declarations for the top level ports of the DUT.
  mPortTypeMap->getPackageNames(&packages);
  for(UtList<UtString>::iterator p = packages.begin(); p != packages.end(); p++) {
    out << "use work." << *p << ".all;" << UtIO::endl;
  }

  out << "use work.carbon_conv.all;" << UtIO::endl;

  // Module declaration.
  getTBEntityName(mTestData->getModule(), &tbEntityName);
  out << UtIO::endl;
  out << "entity " << tbEntityName << " is port (" << UtIO::endl;
  mFirstPortDecl = true;
  emitMainVhdlTBFormalPortDecl(out, indent);
  out << ");"                                      << UtIO::endl;
  out << "end entity " << tbEntityName << ";"      << UtIO::endl;
}

// Emits Vhdl TestBench Architecture
void VhdlTestbenchEmitter::emitMainVhdlTBArch(UtOStream& out) {
  UtString tbEntityName;

  getTBEntityName(mTestData->getModule(), &tbEntityName);

  out << "architecture arch of " << tbEntityName << " is" << UtIO::endl;

  // Formal delay declarations
  emitMainVhdlTBSignalDecl(out);

  out << "begin" << UtIO::endl;

  // Bidirectional enable switching
  emitMainVhdlTBEnabledDriver(out);
  out << UtIO::endl;

  // Main driving code
  emitMainVhdlTBInitialBlock(out);

  out << "end architecture arch;" << UtIO::endl;
}

// Emits Vhdl Top Level Entity
void VhdlTestbenchEmitter::emitMainVhdlTopEntity(UtOStream& out) {
  UtList<UtString> ieeePackages;
  UtList<UtString> packages;

  out << "library ieee;" << UtIO::endl;

  if(!mPortTypeMap->hasIEEEPackage("std_logic_1164")) 
    out << "use ieee.std_logic_1164.all;" << UtIO::endl;

  // Add the necessary IEEE packages
  mPortTypeMap->getIEEEPackages(&ieeePackages);
  for(UtList<UtString>::iterator p = ieeePackages.begin(); p != ieeePackages.end(); p++) {
    out << "use ieee." << *p << ".all;" << UtIO::endl;
  }

  // Add use clauses for any packages that contain the type 
  // declarations for the top level ports of the DUT.
  mPortTypeMap->getPackageNames(&packages);
  for(UtList<UtString>::iterator p = packages.begin(); p != packages.end(); p++) {
    out << "use work." << *p << ".all;" << UtIO::endl;
  }

  out << "use work.carbon_conv.all;" << UtIO::endl;

  out << UtIO::endl;
  out << "entity carbon_test_driver is" << UtIO::endl;
  out << "end carbon_test_driver;"      << UtIO::endl;
}

// Emits Vhdl Top Level Architecture
void VhdlTestbenchEmitter::emitMainVhdlTopArch(UtOStream& out) {
  UtString topEntityName;
  UtString tbEntityName;
  UtString indent("  ");

  getEntityName(mTestData->getModule(), &topEntityName);
  getTBEntityName(mTestData->getModule(), &tbEntityName);

  out << "architecture arch of carbon_test_driver is" << UtIO::endl;

  // Signals
  emitMainVhdlTopSignalDecl(out);

  // DUT component declaration
  out << "  component " << topEntityName  << " port (" << UtIO::endl;
  mFirstPortDecl = true;

  emitMainVhdlDUTFormalPortDecl(out);

  out << ");"               << UtIO::endl;
  out << "  end component;" << UtIO::endl;

  // If this has a top level configuration, use it
  if(mPortTypeMap->hasTopLevelConfig()) {
    UtString configLib;
    UtString configName;

    mPortTypeMap->getTopLevelConfig(&configLib, &configName);

    out << "  for all : " << topEntityName << " use configuration " 
        << configLib << "." << configName << ";" << UtIO::endl;
  }
  
  // Testbench component declaration
  out << "  component " << tbEntityName << " port (" << UtIO::endl;
  mFirstPortDecl = true;

  emitMainVhdlTBFormalPortDecl(out, indent);

  out << ");"               << UtIO::endl;
  out << "  end component;" << UtIO::endl;
  out << "begin"            << UtIO::endl;

  // DUT component instance
  out << "  u_" << topEntityName << " : " << topEntityName  << " port map (" << UtIO::endl;
  mFirstPortDecl = true;

  emitMainVhdlPortMapDecl(out);

  out << ");" << UtIO::endl;

  // Testbench component instance
  out << "  u_" << tbEntityName << " : " << tbEntityName << " port map (" << UtIO::endl;
  mFirstPortDecl = true;

  emitMainVhdlPortMapDecl(out);

  out << ");"                     << UtIO::endl;
  out << "end architecture arch;" << UtIO::endl;
}

// Emits Testbench/DUT Formal Port Declarations (Core)
void VhdlTestbenchEmitter::emitMainVhdlFormalPortDeclCore(UtOStream& out, 
                                                          UtString*  nodeNameVhdl,
                                                          emitParams p) {
  UtString typeStr;
  UtString extraIndent("  ");
  UtString nNodeNameVhdl(*nodeNameVhdl);
  UtString postfix;
  UtString portName;
  UInt32   bitSize = getImplementedBitSize(p.userType, p.bitSize);
  UtString conversionTypeName;
  bool     ranged;
  SInt32   msb;

  // Extract the Leaf Name from Hierarchical Name
  extractPortName(nNodeNameVhdl, &portName);

  // Keep One Item from Structs and Avoid the Rest
  if(!(*(p.pHash)).empty() && ((*(p.pHash)).find(portName) != (*(p.pHash)).end()))
    return;
  (*(p.pHash)).insert(portName);

  getTypeStr(p.userType, portName, &typeStr, &ranged, &msb);

  extractPostfix(p.emit1, &postfix);
  portName << postfix;

  if(mFirstPortDecl == false)
    out << ";" << UtIO::endl;
  mFirstPortDecl = false;

  out << p.indent << extraIndent << portName << " : ";

  switch(p.direction) {
    case ePortIn:
      out << "in ";
      break;
    case ePortOut:
      if(isBuffer(portName))
        out << "buffer ";
      else
        out << "out ";
      break;
    case ePortInOut:
      out << "inout ";
      break;
    default :
      out << "\"Unknown Type\""; 
  }

  out << typeStr;

  // Initialize Outgoing Ports for Pure Combinational DUTs
  if(requiresConversionFunction(p.userType)) 
    getConversionTypeName(p.userType, &conversionTypeName);
  if(!mTestData->hasClocks() &&
     !mTestData->hasResets() &&
     p.initialize            &&
     (p.userType != NULL)) {
    switch(p.direction) {
      case ePortIn:
        break;
      case ePortOut:
      case ePortInOut:
        if((p.userType->getVhdlType() != eVhdlTypeUnknown) ||
           (!conversionTypeName.empty() && (conversionTypeName.find("vhdlunknown") == UtString::npos))) {
          if(!isSingle(bitSize, p.userType, p.isVector)) {
            if(ranged) {
	      if (!conversionTypeName.empty() && p.userType->getVhdlType() == eVhdlTypeUnknown) {
		out << " := To_" << conversionTypeName << "(To_StdLogicVector(" << msb << "," << bitSize << ",true))";
	      }
	      else {
		out << " := " << msb;
	      }
            }
	    else {
	      // Take care of initializing arrays
              out << " := " << initializeArray(p.userType);
	    }
          }
          else {
	    if (p.userType->getVhdlType() == eVhdlTypeStdLogic || p.userType->getVhdlType() == eVhdlTypeStdULogic) {
	      out << " := '0'";
	    }
	  }
        }
        break;
      default :
        out << "\"Unknown Type\""; 
    }
  }
}

// Emits Testbench/DUT Formal Port Declarations (Shell)
void VhdlTestbenchEmitter::emitMainVhdlFormalPortDecl(UtOStream&         out, 
                                                      portDir            direction,
                                                      UtString           indent,
                                                      const NUNetVector& ports,
                                                      bool               initialize) {
  PortHash pHash;

  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet*            net = *i;
    UtString                varNameVhdl("m");
    UtString                nodeNameVhdl;
    NodeVec                 nList;
    emitParams              p;
    NodeVec::const_iterator n;

    extractNUNetInfo(net, &nList);

    // Only Consider Top Level Ports (No Decompostion of Leaves)
    n = nList.begin();
    n++;
    if(n != nList.end())
      n++;

    clearEmitParams(&p);
    p.emit1      = eEmitNormal;
    p.direction  = direction;
    p.bitSize    = net->getBitSize();
    p.isVector   = net->isVectorNet();
    p.initialize = initialize;
    p.indent     << indent;
    p.pHash      = &pHash;
    emitMainCore(eVhdlFormalPortDecl, out, nList.begin(), n, &varNameVhdl, &nodeNameVhdl, p);
  }

  pHash.freeMemory();
}

// Emits Testbench Formal Port Declarations
// Directions are reversed from emitTopFormalPorts
void VhdlTestbenchEmitter::emitMainVhdlTBFormalPortDecl(UtOStream& out, UtString indent) {
  // Top Level Ports
  emitMainVhdlFormalPortDecl(out, ePortInOut, indent, mTestData->getClocks(),  true);
  emitMainVhdlFormalPortDecl(out, ePortInOut, indent, mTestData->getResets(),  true);
  emitMainVhdlFormalPortDecl(out, ePortInOut, indent, mTestData->getInputs(),  true);
  emitMainVhdlFormalPortDecl(out, ePortIn,    indent, mTestData->getOutputs(), true);
  emitMainVhdlFormalPortDecl(out, ePortInOut, indent, mTestData->getBids(),    true);
}

// Emits Testbench/Top Formal Signal Declarations (Core)
void VhdlTestbenchEmitter::emitMainVhdlSignalDeclCore(UtOStream& out, 
                                                      UtString*  nodeNameVhdl,
                                                      emitParams p) {
  UtString typeStr;
  UtString indent("  ");
  UtString nNodeNameVhdl(*nodeNameVhdl);
  UtString postfix;
  UtString portName;
  bool     ranged;
  SInt32   msb;

  // Extract the Leaf Name from Hierarchical Name
  extractPortName(nNodeNameVhdl, &portName);

  // Keep One Item from Structs and Avoid the Rest
  if(!(*(p.pHash)).empty() && ((*(p.pHash)).find(portName) != (*(p.pHash)).end()))
    return;
  (*(p.pHash)).insert(portName);

  extractPostfix(p.emit1, &postfix);
  portName << postfix;

  out << indent << "signal " << p.prefix << portName << " : ";

  getTypeStr(p.userType, portName, &typeStr, &ranged, &msb);

  out << typeStr << ";" << UtIO::endl;
}

// Emits Testbench/Top Signal Declarations (Shell)
void VhdlTestbenchEmitter::emitMainVhdlSignalDecl(UtOStream&         out,
                                                  const NUNetVector& ports,
                                                  UtString           prefix,
                                                  EmitType           emit) {
  PortHash pHash;

  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet*            net = *i;
    UtString                varNameVhdl("m");
    UtString                nodeNameVhdl;
    NodeVec                 nList;
    emitParams              p;
    NodeVec::const_iterator n;

    extractNUNetInfo(net, &nList);

    // Only Consider Top Level Ports (No Decompostion of Leaves)
    n = nList.begin();
    n++;
    if(n != nList.end())
      n++;

    clearEmitParams(&p);
    p.prefix << prefix;
    p.emit1  = emit;
    p.pHash  = &pHash;
    emitMainCore(eVhdlSignalDecl, out, nList.begin(), n, &varNameVhdl, &nodeNameVhdl, p);
  }

  pHash.freeMemory();
}

// Emits Testbench Delayed Signal Declarations
void VhdlTestbenchEmitter::emitMainVhdlTBSignalDecl(UtOStream& out) {
  UtString prefix;

  // Top Level Ports
  emitMainVhdlSignalDecl(out, mTestData->getClocks(), prefix, eEmitDelay);
  emitMainVhdlSignalDecl(out, mTestData->getResets(), prefix, eEmitDelay);
  emitMainVhdlSignalDecl(out, mTestData->getBids(),   prefix, eEmitData);
  emitMainVhdlSignalDecl(out, mTestData->getBids(),   prefix, eEmitEnable);
}

// Emits Testbench Initial Block
void VhdlTestbenchEmitter::emitMainVhdlTBInitialBlock(UtOStream& out) {
  UInt32 inVect  = getAccWidth(out);
  UInt32 padding = (4 - (inVect % 4 )) % 4;
  UInt32 bufSize = std::max(1u, inVect + padding);

  // The process that enables dumping fsdb from MTI.
  if(mTestData->getDumpFSDB()) {
    UtString mName(mTestData->getModule()->getName()->str());

    out << "  -- with verdi3-201403 or later and modelsim this dumping is enabled by runmti" << UtIO::endl;
    out << "  -- uncomment this process if you are using a different simulator" << UtIO::endl;
    out << "--  fsdb_dumper : process"                                          << UtIO::endl;
    out << "--    begin"                                                        << UtIO::endl;
    out << "--      fsdbDumpfile(\"" << mName << ".fsdb\");"                    << UtIO::endl;
    out << "--      fsdbDumpvars(0, \"carbon_test_driver.u_" << mName << "\");" << UtIO::endl;
    out << "--      wait;"                                                      << UtIO::endl;
    out << "--    end process;"                                                 << UtIO::endl;
  }

  out << "  tb_driver : process"                                         << UtIO::endl;
  out << "    variable " << mTestMemoryName << " : " 
      << DefaultVhdlDataType << "_vector(0 to " << (bufSize - 1) << ");" << UtIO::endl;
  out << "    file vectorfile : TEXT open READ_MODE is \"" 
      << mTestMemoryFileName << "\";"                                    << UtIO::endl;
  out << "    variable in_line, out_line : LINE;"                        << UtIO::endl;
  out << "  begin"                                                       << UtIO::endl;
  out << "    while not endfile(vectorfile) loop"                        << UtIO::endl;
  out << "      readline(vectorfile, in_line);"                          << UtIO::endl;
  out << "      hread(in_line, " << mTestMemoryName << ");"              << UtIO::endl;

  if(mTestData->getTotalNumInputs() != 0)
    emitMainVhdlTBReadList(out, padding);

  out << UtIO::endl;
  out << "      wait for " << mTestData->getClockDelay() << " ns;" << UtIO::endl;

  emitMainVhdlTBDelayList(out);
  
  out << UtIO::endl;
  out << "      wait for " <<  100 - mTestData->getClockDelay() << " ns;" << UtIO::endl;
  out << "      write(out_line, string'(\"CARBON: \"));"                  << UtIO::endl;

  emitMainVhdlTBWriteList(out);

  out << "      writeline(STD.TEXTIO.OUTPUT, out_line);" << UtIO::endl;
  out << "    end loop;"                                 << UtIO::endl;
  out << "    wait;  -- terminate the simulation"        << UtIO::endl;
  out << "  end process tb_driver;"                      << UtIO::endl;
}

// Emits Testbench Read List (Core)
void VhdlTestbenchEmitter::emitMainVhdlTBReadListCore(UtOStream& out,
                                                      UtString*  nodeNameVhdl,
                                                      emitParams p) {
  UtString nNodeNameVhdl(*nodeNameVhdl);
  UtString indent("      ");
  UtString postfix;
  UtString portName;
  UInt32   bitSize = getImplementedBitSize(p.userType, p.bitSize);

  // Remove the Top Level Name from Hierarchical Name
  extractPortName(nNodeNameVhdl, &portName);

  extractPostfix(p.emit1, &postfix);
  portName << postfix;

  resolveArrayRep(&portName);

  out << indent  << portName << " <= ";

  // Conversion Functions
  if(requiresConversionFunction(p.userType)) {
    UtString conversionTypeName;

    getConversionTypeName(p.userType, &conversionTypeName);
    out << "To_" << conversionTypeName << "(";
  }

  out << mTestMemoryName << "(" << *(p.index);

  *(p.index) += bitSize;

  if(!isSingle(bitSize, p.userType, p.isVector))
    out << " to " << *(p.index) - 1 << ")";
  else
    out << ")";

  // Conversion Functions
  if(requiresConversionFunction(p.userType)) {

    // Consider Signed or Unsigned Conversion for Integer User Types
    if(p.userType->getVhdlType() == eVhdlTypeInteger) {
      UserType::Type nType = p.userType->getType();

      // Scalars
      if(nType == UserType::eScalar) {
        const UserScalar* scalarType = p.userType->castScalar();

        // Consider All-Positive Ranges
        if(scalarType->getRangeConstraint()                  &&
           (scalarType->getRangeConstraint()->getMsb() >= 0) &&
           (scalarType->getRangeConstraint()->getLsb() >= 0)) {
          out << ", false";
        }
        else
          out << ", true";
      }
      else
        out << ", true";
    }

    out << ")";
  }

  out << ";"  << UtIO::endl;
}

// Emits Testbench Read List (Shell)
void VhdlTestbenchEmitter::emitMainVhdlTBReadList(UtOStream&         out,
                                                  const NUNetVector& ports,
                                                  UInt32*            index,
                                                  EmitType           emit1) {
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet* net = *i;
    UtString     varNameVhdl("m");
    UtString     nodeNameVhdl;
    NodeVec      nList;
    emitParams   p;

    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.bitSize  = net->getBitSize();
    p.isVector = net->isVectorNet();
    p.emit1    = emit1;
    p.index    = &(*index);
    emitMainCore(eVhdlTestbenchReadList, out, nList.begin(), nList.end(), &varNameVhdl, &nodeNameVhdl, p);
    *index     = *(p.index);
  }
}

// Emits Testbench Read List
void VhdlTestbenchEmitter::emitMainVhdlTBReadList(UtOStream& out, 
                                                  UInt32     offset) {
  UInt32 start_index = offset;

  // Top Level Ports
  emitMainVhdlTBReadList(out, mTestData->getClocks(), &start_index, eEmitDelay);
  emitMainVhdlTBReadList(out, mTestData->getResets(), &start_index, eEmitDelay);
  emitMainVhdlTBReadList(out, mTestData->getInputs(), &start_index, eEmitNormal);
  emitMainVhdlTBReadList(out, mTestData->getBids(),   &start_index, eEmitData);
  emitMainVhdlTBReadList(out, mTestData->getBids(),   &start_index, eEmitEnable);
}

// Emits Testbench Read List (Core)
void VhdlTestbenchEmitter::emitMainVhdlTBDelayListCore(UtOStream& out,
                                                       UtString*  nodeNameVhdl,
                                                       emitParams p) {
  UtString indent("      ");
  UtString nNodeNameVhdl(*nodeNameVhdl);
  UtString postfix;
  UtString portName;
  UtString delayName;

  // Extract the Leaf Name from Hierarchical Name
  extractPortName(nNodeNameVhdl, &portName);

  // Keep One Item from Structs and Avoid the Rest
  if(!(*(p.pHash)).empty() && ((*(p.pHash)).find(portName) != (*(p.pHash)).end()))
    return;
  (*(p.pHash)).insert(portName);
  
  delayName << portName;
  extractPostfix(p.emit1, &postfix);
  portName << postfix;
  extractPostfix(p.emit2, &postfix);
  delayName << postfix;

  out << indent << portName << " <= " << delayName << ";" << UtIO::endl;
}

// Emits Testbench Delay List (Shell)
void VhdlTestbenchEmitter::emitMainVhdlTBDelayList(UtOStream&         out,
                                                   const NUNetVector& ports,
                                                   EmitType           emit1,
                                                   EmitType           emit2) {
  PortHash pHash;

  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet*            net = *i;
    UtString                varNameVhdl("m");
    UtString                nodeNameVhdl;
    NodeVec                 nList;
    emitParams              p;
    NodeVec::const_iterator n;

    extractNUNetInfo(net, &nList);

    // Only Consider Top Level Ports (No Decompostion of Leaves)
    n = nList.begin();
    n++;
    if(n != nList.end())
      n++;

    clearEmitParams(&p);
    p.emit1 = emit1;
    p.emit2 = emit2;
    p.pHash = &pHash;
    emitMainCore(eVhdlTestbenchDelayList, out, nList.begin(), n, &varNameVhdl, &nodeNameVhdl, p);
  }

  pHash.freeMemory();
}

// Emits Testbench Delay List
void VhdlTestbenchEmitter::emitMainVhdlTBDelayList(UtOStream& out) {
  // Top Level Ports
  emitMainVhdlTBDelayList(out, mTestData->getClocks(), eEmitNormal, eEmitDelay);
  emitMainVhdlTBDelayList(out, mTestData->getResets(), eEmitNormal, eEmitDelay);
}

// Emits Testbench Write List (Core)
void VhdlTestbenchEmitter::emitMainVhdlTBWriteListCore(UtOStream& out,
                                                       UtString*  nodeNameVhdl,
                                                       emitParams p) {
  UtString indent("      ");
  UtString nNodeNameVhdl(*nodeNameVhdl);
  UtString postfix;
  UtString portName;
  UInt32   bitSize = getImplementedBitSize(p.userType, p.bitSize);

  // Remove the Top Level Name from Hierarchical Name
  extractPortName(nNodeNameVhdl, &portName);

  // Emit Map VHDL File
  *mMapFile << (*(p.column))++ << "\t" << portName << UtIO::endl;

  resolveArrayRep(&portName);
  
  extractPostfix(p.emit1, &postfix);
  portName << postfix;

  out << indent << "write(out_line, "; 

  // Conversion Functions
  if(requiresConversionFunction(p.userType)) {
    if(!isSingle(bitSize, p.userType, p.isVector))
      out << "To_StdLogicVector(";
    else
      out << "To_StdLogic(";
  }

  out << portName;

  // Need the Size for Integers Bases
  if((p.userType != NULL) &&
     ((p.userType->getVhdlType() == eVhdlTypeInteger) ||
      (p.userType->getVhdlType() == eVhdlTypeNatural) ||
      (p.userType->getVhdlType() == eVhdlTypePositive))) {
    out << ", " << bitSize;
    // Consider Signed or Unsigned Conversion for Integer Based User Types
    if(p.userType->getVhdlType() == eVhdlTypeInteger) {
      UserType::Type nType = p.userType->getType();

      // Scalars
      if(nType == UserType::eScalar) {
        const UserScalar* scalarType = p.userType->castScalar();

        // Consider All-Positive Ranges
        if(scalarType->getRangeConstraint()                  &&
           (scalarType->getRangeConstraint()->getMsb() >= 0) &&
           (scalarType->getRangeConstraint()->getLsb() >= 0)) {
          out << ", false";
        }
        else
          out << ", true";
      }
      else
        out << ", true";
    }
    else if((p.userType->getVhdlType() == eVhdlTypeNatural) ||
            (p.userType->getVhdlType() == eVhdlTypePositive)){
      out << ", false";
    }
  }

  // Conversion Functions
  if(requiresConversionFunction(p.userType)) 
    out << ")";

  out  << ");" << UtIO::endl;
}

// Emits Testbench Write List (Shell)
void VhdlTestbenchEmitter::emitMainVhdlTBWriteList(UtOStream&         out,
                                                   const NUNetVector& ports,
                                                   EmitType           emit,
                                                   SInt32*            column) {
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet* net = *i;
    UtString     varNameVhdl("m");
    UtString     nodeNameVhdl;
    NodeVec      nList;
    emitParams   p;

    extractNUNetInfo(net, &nList);
    clearEmitParams(&p);
    p.emit1    = emit;
    p.bitSize  = net->getBitSize();
    p.isVector = net->isVectorNet();
    p.column   = &(*column);
    emitMainCore(eVhdlTestbenchWriteList, out, nList.begin(), nList.end(), &varNameVhdl, &nodeNameVhdl, p);
    *column    = *(p.column);    
  }
}

// Emits Testbench Write List
void VhdlTestbenchEmitter::emitMainVhdlTBWriteList(UtOStream& out) {
  SInt32 column = 0;

  // Top Level Ports
  emitMainVhdlTBWriteList(out, mTestData->getClocks(), eEmitNormal, &column);
  emitMainVhdlTBWriteList(out, mTestData->getResets(), eEmitNormal, &column);
  emitMainVhdlTBWriteList(out, mTestData->getInputs(), eEmitNormal, &column);
  emitMainVhdlTBWriteList(out, mTestData->getBids(), eEmitData,   &column);
  emitMainVhdlTBWriteList(out, mTestData->getBids(), eEmitEnable, &column);
  emitMainVhdlTBWriteList(out, mTestData->getOutputs(), eEmitNormal, &column);
  emitMainVhdlTBWriteList(out, mTestData->getBids(),    eEmitNormal, &column);
}

// Emits Top Signal Declaration
void VhdlTestbenchEmitter::emitMainVhdlTopSignalDecl(UtOStream& out) {
  UtString prefix("carbon_");

  // Top Level Ports
  emitMainVhdlSignalDecl(out, mTestData->getClocks(),  prefix, eEmitNormal);
  emitMainVhdlSignalDecl(out, mTestData->getResets(),  prefix, eEmitNormal);
  emitMainVhdlSignalDecl(out, mTestData->getInputs(),  prefix, eEmitNormal);
  emitMainVhdlSignalDecl(out, mTestData->getOutputs(), prefix, eEmitNormal);
  emitMainVhdlSignalDecl(out, mTestData->getBids(),    prefix, eEmitNormal);
}

// Emits Top DUT Formal Ports
void VhdlTestbenchEmitter::emitMainVhdlDUTFormalPortDecl(UtOStream& out) {
  UtString indent("  ");  
  // Top Level Ports
  emitMainVhdlFormalPortDecl(out, ePortIn,    indent, mTestData->getClocks(),  false);
  emitMainVhdlFormalPortDecl(out, ePortIn,    indent, mTestData->getResets(),  false);
  emitMainVhdlFormalPortDecl(out, ePortIn,    indent, mTestData->getInputs(),  false);
  emitMainVhdlFormalPortDecl(out, ePortOut,   indent, mTestData->getOutputs(), false);
  emitMainVhdlFormalPortDecl(out, ePortInOut, indent, mTestData->getBids(),    false);
}

// Emits Testbench Enable Drivers (Core)
void VhdlTestbenchEmitter::emitMainVhdlTBEnabledDriverCore(UtOStream& out, 
                                                           UtString*  nodeNameVhdl,
                                                           emitParams p) {
  UtString nNodeNameVhdl(*nodeNameVhdl);
  UtString postfix1;
  UtString postfix2;
  UtString portName;
  UtString delayName;
  UInt32   bitSize = getImplementedBitSize(p.userType, p.bitSize);

  // Extract the Leaf Name from Hierarchical Name
  extractPortName(nNodeNameVhdl, &portName);

  // Keep One Item from Structs and Avoid the Rest
  if(!(*(p.pHash)).empty() && ((*(p.pHash)).find(portName) != (*(p.pHash)).end()))
    return;
  (*(p.pHash)).insert(portName);

  out << "  inst" << *(p.index) << " : " << portName << " <= ";

  (*(p.index))++;

  extractPostfix(p.emit1, &postfix1);
  extractPostfix(p.emit2, &postfix2);

  out << portName << postfix1 << " when (" << portName << postfix2 << " = ";
  
  if(!isSingle(bitSize, p.userType, p.isVector)) {
    out << "\""; 
    for(UInt32 i=0; i<bitSize; i++) 
      out << '0';
    out << "\")";
    if((p.userType != NULL) &&
       (p.userType->getVhdlType() == eVhdlTypeStdLogic))
      out << " else (others => 'Z');" << UtIO::endl;
    else
      out << ";"                     << UtIO::endl;
  }
  else {
    out << "'0')";
    if((p.userType != NULL) &&
       (p.userType->getVhdlType() == eVhdlTypeStdLogic))
      out << " else 'Z';" << UtIO::endl;
    else
      out << ";"         << UtIO::endl;
  }
}


// Emits Testbench Enable Drivers (Shell)
void VhdlTestbenchEmitter::emitMainVhdlTBEnabledDriver(UtOStream&         out,
                                                       const NUNetVector& ports,
                                                       EmitType           emit1,
                                                       EmitType           emit2) {
  PortHash pHash;
  UInt32   instNum = 0;

  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet*            net = *i;
    UtString                varNameVhdl("m");
    UtString                nodeNameVhdl;
    NodeVec                 nList;
    emitParams              p;
    NodeVec::const_iterator n;

    extractNUNetInfo(net, &nList);

    // Only Consider Top Level Ports (No Decompostion of Leaves)
    n = nList.begin();
    n++;
    if(n != nList.end())
      n++;

    clearEmitParams(&p);
    p.emit1    = emit1;
    p.emit2    = emit2;
    p.bitSize  = net->getBitSize();
    p.isVector = net->isVectorNet();
    p.index    = &instNum;
    p.pHash    = &pHash;
    emitMainCore(eVhdlTestbenchEnableDriver, out, nList.begin(), n, &varNameVhdl, &nodeNameVhdl, p);
    instNum    = *(p.index);
  }

  pHash.freeMemory();
}

// Emits Top Enable Drivers
void VhdlTestbenchEmitter::emitMainVhdlTBEnabledDriver(UtOStream& out) {
  emitMainVhdlTBEnabledDriver(out, mTestData->getBids(), eEmitData, eEmitEnable);
}

//  Emits Testbench/DUT Port Map Declarations (Core)
void VhdlTestbenchEmitter::emitMainVhdlPortMapDeclCore(UtOStream& out, 
                                                       UtString*  nodeNameVhdl,
                                                       emitParams p) {
  UtString indent("    ");
  UtString nNodeNameVhdl(*nodeNameVhdl);
  UtString postfix;
  UtString portName;

  // Extract the Leaf Name from Hierarchical Name
  extractPortName(nNodeNameVhdl, &portName);

  // Keep One Item from Structs and Avoid the Rest
  if(!(*(p.pHash)).empty() && ((*(p.pHash)).find(portName) != (*(p.pHash)).end()))
    return;
  (*(p.pHash)).insert(portName);

  extractPostfix(p.emit1, &postfix);
  portName << postfix;

  if(mFirstPortDecl == false)
    out << "," << UtIO::endl;
  mFirstPortDecl = false;

  out << indent << portName << " => carbon_" << portName;
}

// Emits Testbench/DUT Port Map Declarations (Shell)
void VhdlTestbenchEmitter::emitMainVhdlPortMapDecl(UtOStream&         out,
                                                   const NUNetVector& ports) {
  PortHash pHash;

  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const NUNet*            net = *i;
    UtString                varNameVhdl("m");
    UtString                nodeNameVhdl;
    NodeVec                 nList;
    emitParams              p;
    NodeVec::const_iterator n;

    extractNUNetInfo(net, &nList);

    // Only Consider Top Level Ports (No Decompostion of Leaves)
    n = nList.begin();
    n++;
    if(n != nList.end())
      n++;

    clearEmitParams(&p);
    p.emit1 = eEmitNormal;
    p.pHash = &pHash;
    emitMainCore(eVhdlPortMapDecl, out, nList.begin(), n, &varNameVhdl, &nodeNameVhdl, p);
  }

  pHash.freeMemory();
}

// Emits Testbench/DUT Port Map Declarations
void VhdlTestbenchEmitter::emitMainVhdlPortMapDecl(UtOStream& out) {
  // Top Level Ports
  emitMainVhdlPortMapDecl(out, mTestData->getClocks());
  emitMainVhdlPortMapDecl(out, mTestData->getResets());
  emitMainVhdlPortMapDecl(out, mTestData->getInputs());
  emitMainVhdlPortMapDecl(out, mTestData->getOutputs());
  emitMainVhdlPortMapDecl(out, mTestData->getBids());
}

//////////////////////////////////////SystemC Testbench Emitter//////////////////////////////////////

/* Object to emit a systemC testdriver. */
SCTestbenchEmitter::SCTestbenchEmitter(
   TestDriverData *d,  const UtString &targetName, UtOStream& out
                                              )
  : TestbenchEmitter(d, targetName), mOut( out )
{
}

void SCTestbenchEmitter::SCemit()
{
  UInt32 vectors =  mTestData->getVectorCount();
  UInt32 width =  mTestData->getVectorWidth();
  const char *modName = mTestData->getModule()->getName()->str();
  mOut <<
    "/* file:         sc_main.cpp */\n"
    "/* description:  drive a systemC model with test vectors */\n\n"
    "#include <fstream>\n"
    "#include \"systemc.h\"\n"
    "#include \"libdesign.systemc.h\"\n\n"
    "#define CARBON_VECTORS " << vectors << "\n"
    "#define CARBON_WIDTH " << width << "\n\n"
    "SC_MODULE( carbon_testbench ) {\n"
    "  public:\n"
    "  void carbon_readmemh();\n\n"
    "  sc_event carbon_apply_vector_event;\n"
    "  unsigned int mVectorIndex;\n"
    "  void carbon_apply_vector();\n\n";
  SCemitAllSignals();
  mOut << 
    "\n  " << modName << " inst;\n\n"
    "  SC_CTOR( carbon_testbench ) : inst(\"inst\") {\n";
  SCemitInstance();
  mOut <<
    "    mVectorIndex = 0;\n"
    "    carbon_readmemh();\n"
    "    SC_METHOD( carbon_apply_vector );\n"
    "    sensitive << carbon_apply_vector_event;\n\n"
    "  }\n\n"
    "};  // end carbon_testbench\n\n"
    "void carbon_testbench::carbon_readmemh()\n"
    "{\n"
    "  ifstream vfile( \"libdesign.test.vectors\" );\n"
    "  // Add 0x in front of the number for parsing a hex number.\n"
    "  char line_buf[ CARBON_WIDTH+3 ];\n"
    "  line_buf[0] = '0';\n"
    "  line_buf[1] = 'x';\n"
    "\n"
    "  for( unsigned int index = 0; !vfile.eof() && index < CARBON_VECTORS; index++) {\n"
    "    vfile.getline( &(line_buf[2]), CARBON_WIDTH-2 );\n"
    "    // Stop on any blank lines.\n"
    "    if( !strcmp(\"0x\", line_buf) )\n"
    "      break;\n"
    "    carbon_mem[ index ] = line_buf;\n"
    "  }\n"
    "}\n\n"
    "void carbon_testbench::carbon_apply_vector() {\n";
  SCemitVectorDisplay();
  // Avoid indexing beyond end of carbon_mem
  mOut <<
    "\n"
    "  if( mVectorIndex >= CARBON_VECTORS )\n"
    "    return;\n";
  SCemitVector();
  mOut <<
    "  if( mVectorIndex < CARBON_VECTORS ) {\n"
    "    carbon_apply_vector_event.notify( 100, SC_NS );\n"
    "    mVectorIndex++;\n"
    "  }\n"
    "}  // end carbon_apply_vector\n\n"
    "int sc_main(int argc, char* argv[]) {\n\n"
    "  carbon_testbench carbon_tb( \"carbon_tb\" );\n\n"
    "  sc_start( -1 );\n"
    "}\n";
}


void SCTestbenchEmitter::SCemitAllSignals()
{
  // Declare the array of vectors.
  mOut << "  " << SCNetType( mTestData->getVectorWidth() ) <<
          " carbon_mem[ CARBON_VECTORS ];\n";
  
  SCemitSignals( mTestData->getClocks() );
  SCemitSignals( mTestData->getResets() );
  SCemitSignals( mTestData->getInputs() );
  SCemitSignals( mTestData->getOutputs() );
  SCemitSignals( mTestData->getBids() );
  SCemitSignals( mTestData->getBids(), "_enable" );
}

void SCTestbenchEmitter::SCemitSignals( const NUNetVector & ports,
                                        const char* suffix )
{
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    mOut <<
      "  sc_signal< "  << SCNetType( (*i)->getBitSize() ) << " > " <<
      (*i)->getName()->str() << suffix << ";\n";
  }
}

const char* SCTestbenchEmitter::SCNetType( UInt32 bits )
{
  static char typeStr[64];
  if( bits <= 1 )
    strcpy( typeStr, "bool" );
  else if( bits <= 32 )
    sprintf(typeStr, "sc_uint<%d>", bits);
  else
    sprintf(typeStr, "sc_biguint<%d>", bits);
  return typeStr;
}

void SCTestbenchEmitter::SCemitInstance()
{
  SCemitInstancePorts( mTestData->getClocks() );
  SCemitInstancePorts( mTestData->getResets() );
  SCemitInstancePorts( mTestData->getInputs() );
  SCemitInstancePorts( mTestData->getOutputs() );
  SCemitInstancePorts( mTestData->getBids() );
  mOut << "\n\n";
}

void SCTestbenchEmitter::SCemitInstancePorts( const NUNetVector & ports )
{
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const char *portName = (*i)->getName()->str();
    mOut <<
      "      inst."  << portName << "(" << portName << ");\n";
  }
}

void SCTestbenchEmitter::SCemitVectorDisplay()
{
  mOut <<
    "  std::cout << \"vector=\" << mVectorIndex << std::endl;\n";
  SCemitVectorDisplaySignals( mTestData->getClocks() );
  SCemitVectorDisplaySignals( mTestData->getResets() );
  SCemitVectorDisplaySignals( mTestData->getInputs() );
  SCemitVectorDisplaySignals( mTestData->getOutputs() );
  SCemitVectorDisplaySignals( mTestData->getBids() );
  SCemitVectorDisplaySignals( mTestData->getBids(), "_enable" );
  mOut << "  std::cout << std::endl;\n";
}

void SCTestbenchEmitter::SCemitVectorDisplaySignals( const NUNetVector& ports, 
                                                     const char* suffix )
{
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    mOut <<
      "  std::cout << " << (*i)->getName()->str() << suffix << ".read();\n";
  }
}


void SCTestbenchEmitter::SCemitVector()
{
 
  mOut << "  " << SCNetType( mTestData->getVectorWidth() ) <<
    " vec = carbon_mem[ mVectorIndex ];\n\n";
  UInt32 bit_pos = 0;
  SCemitVectorSlice( mTestData->getClocks(), &bit_pos );
  SCemitVectorSlice( mTestData->getResets(), &bit_pos );
  SCemitVectorSlice( mTestData->getInputs(), &bit_pos );
  SCemitVectorSlice( mTestData->getBids(), &bit_pos );
  SCemitVectorSlice( mTestData->getBids(), &bit_pos, "_enable" );
}

void SCTestbenchEmitter::SCemitVectorSlice( const NUNetVector & ports,
                                            UInt32* bit_pos,
                                            const char* suffix )
{
  for(NUNetVector::const_iterator i = ports.begin(); i != ports.end(); i++) {
    const char *portName = (*i)->getName()->str();
    UInt32 width = (*i)->getBitSize();
    mOut << "  " << portName << suffix << ".write( vec";
    if( width == 1)
      mOut << "[" << *bit_pos << "]";
    else
      mOut << ".range(" << *bit_pos << "," << *bit_pos+(width-1) << ")";
    mOut << " );\n";
      
    *bit_pos += width;
  }
}


