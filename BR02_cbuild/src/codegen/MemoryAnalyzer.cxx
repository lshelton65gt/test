// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "MemoryAnalyzer.h"

#include "nucleus/NUDesignWalker.h"
#include "compiler_driver/CarbonContext.h"
#include "util/ArgProc.h"
#include <climits>


MemoryAnalyzer::MemoryAnalyzer(SInt32 user_memory_capacity,
                               bool verbose) :
  mMemoryCapacity(user_memory_capacity),
  mMemoryBitCapacity(0),
  mVerbose(verbose)
{
}


//! Compute a histogram for each live memory in the design.
class ConstructMemoryHistogram : public NUDesignCallback
{
public:
  //! Constructor
  ConstructMemoryHistogram(MemoryAnalyzer::BitSizeToElaborations & bit_size_to_elaborations) :
    NUDesignCallback(),
    mBitSizeToElaborations(bit_size_to_elaborations)
  {}

  //! By default, walk through everything.
  Status operator()(Phase /*phase*/, NUBase* /*node*/) { return eNormal; }

  //! Process net declarations.
  Status operator()(Phase phase, NUNet *node);

private:

  /*! 
    Histogram from bit-size of a memory to the number of elaborated
    memories with that particular bit-size.
  */
  MemoryAnalyzer::BitSizeToElaborations & mBitSizeToElaborations;
};


NUDesignCallback::Status ConstructMemoryHistogram::operator()(Phase phase, NUNet *net)
{
  if (phase==ePre) {
    if (net->is2DAnything() and not net->isDead()) {
      // TBD: Is NUNet::isDead() sufficient? Right?
      UInt64 bitSize = net->getBitSize();
      ++ (mBitSizeToElaborations[bitSize]);
    }
  }
  return eNormal;
}


void MemoryAnalyzer::design(NUDesign * design)
{
  // Compute a histogram from bit-size to the number of elaborated
  // nets with that particular bit-size
  MemoryAnalyzer::BitSizeToElaborations bit_size_to_elaborations;
  {
    ConstructMemoryHistogram construct_histogram(bit_size_to_elaborations);
    NUDesignWalker walker(construct_histogram, false, false);
    walker.putWalkDeclaredNets(true);
    walker.design(design);
  }

  if (mVerbose) {
    UtIO::cout() << "Memory Analyzer Statistics:" << UtIO::endl;
    UtIO::cout() << UtIO::Width(12) << UtIO::right << "Bit-size" 
                 << UtIO::Width(12) << UtIO::right << "Elaborate" 
                 << UtIO::Width(12) << UtIO::right << "Capacity" 
                 << UtIO::Width(12) << UtIO::right << "Total" 
                 << UtIO::Width(12) << UtIO::right << "TBytes" 
                 << UtIO::endl;
    UtIO::cout() << "---------------------------------------------------------------" << UtIO::endl;
  }

  // Convert from bytes to bits. User-capacity<0 is handled in the loop.
  UInt64 memoryCapacity = ((UInt64)mMemoryCapacity)*CHAR_BIT;

  // Greedily permit each bit-size until one exceeds the total allowed
  // capacity.
  UInt64 totalCapacity = 0;
  for (BitSizeToElaborations::iterator iter = bit_size_to_elaborations.begin();
       iter != bit_size_to_elaborations.end();
       ++iter) {
    UInt64 bitSize      = iter->first;
    UInt64 elaborations = iter->second;
      
    UInt64 currentCapacity = bitSize * elaborations;
    if ((mMemoryCapacity < 0) or // user-capacity<0 => no sparse mems.
        ((totalCapacity + currentCapacity) <= memoryCapacity)) {
      // Allowing this bit size does not exceeds the allowed memory capacity.
      mMemoryBitCapacity = bitSize;
    }
    totalCapacity += currentCapacity;

    // We don't break out of this loop early so we can compute totals and print diags.
    if (mVerbose) {
      UtIO::cout() << UtIO::Width(12) << UtIO::right << bitSize 
                   << UtIO::Width(12) << UtIO::right << elaborations 
                   << UtIO::Width(12) << UtIO::right << currentCapacity 
                   << UtIO::Width(12) << UtIO::right << totalCapacity 
                   << UtIO::Width(12) << UtIO::right << (totalCapacity/CHAR_BIT)
                   << UtIO::endl;
    }
  }

  if (mVerbose) {
    UtIO::cout() << "---------------------------------------------------------------" << UtIO::endl;
    UtIO::cout() << "Potential Memory Capacity (bytes): " << (totalCapacity/CHAR_BIT) << UtIO::endl;
    UtIO::cout() << "Memory Capacity (bytes): " << mMemoryCapacity << UtIO::endl;
    UtIO::cout() << "Memory Bit Capacity (computed): " << mMemoryBitCapacity << UtIO::endl;
    UtIO::cout() << "---------------------------------------------------------------" << UtIO::endl;
  }
}


