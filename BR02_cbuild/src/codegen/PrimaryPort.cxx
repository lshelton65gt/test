// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Defines a class to analyze primary ports
*/

#include "iodb/IODBNucleus.h"
#include "compiler_driver/CarbonDBWrite.h"

#include "nucleus/Nucleus.h"
#include "nucleus/NUNet.h"

#include "symtab/STSymbolTableNode.h"

#include "codegen/codegen.h"

#include "PrimaryPort.h"


CGPrimaryPort::CGPrimaryPort(IODBNucleus* iodb, CodeGen* codeGen,
                             STSymbolTableNode* node) :
  mNode(node)
{
  // The net is stored in the node
  mNet = NUNet::find(node);

  // The port flags are stored either in the net or in the leaf of an
  // expression
  CbuildSymTabBOM* symTabBOM = iodb->getBOMManager();
  STAliasedLeafNodeVector nodes;
  bool isExpr = symTabBOM->getMappedExprLeafNodes(node->castLeaf(), &nodes);
  if (isExpr) {
    // Get the nets and net flags
    NUNetSet allNets;
    NUNetElab::getPrimaryNetsAndFlags(nodes, &mNetFlags, &allNets);

    // Test the codegen bidirect
    mIsCGBidirect = true;
    for (Loop<NUNetSet> l(allNets); !l.atEnd() && !mIsCGBidirect; ++l) {
      NUNet* net = *l;
      mIsCGBidirect = codeGen->isBidirect(net);
    }
  } else {
    mNetFlags = mNet->getFlags();
    mIsCGBidirect = codeGen->isBidirect(mNet);
  }
}
