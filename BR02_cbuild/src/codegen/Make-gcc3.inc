# -*- Makefile -*-
#
# *****************************************************************************
# Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
# *****************************************************************************
#
# compiler-dependent makefile fragment

# Define where compiler can be found
ifndef CARBON_GCC3_VERSION
  CARBON_GCC3_VERSION := 345
endif

CARBON_COMPILER_DIST:=${CARBON_HOME}/${CARBON_TARGET_ARCH}/gcc${CARBON_GCC3_VERSION}

# Where gcc wants to look for libraries
CARBON_GCC_LIB_Linux := lib
CARBON_GCC_LIB_Linux64 := lib64
CARBON_GCC_LIB_Host := $(CARBON_GCC_LIB_$(CARBON_HOST_ARCH))
CARBON_GCC_LIB_Target := $(CARBON_GCC_LIB_$(CARBON_TARGET_ARCH))

CARBON_COMPILER_LIB_DIRS := $(CARBON_HOME)/$(CARBON_TARGET_ARCH)/gcc${GCC_VERSION}/$(CARBON_GCC_LIB_Target)

# C++ runtime libraries needed to export a stand-alone .a file
# 3.3.3 uses gcc-lib, while 3.4.x uses gcc
#
CARBON_GCCVER:=$(shell $(CARBON_COMPILER_DIST)/bin/g++ -dumpversion)

CARBON_EXTRA_OPTS=

ifneq (,$(findstring 3.4,$(CARBON_GCCVER)))
  CARBON_COMPILER_SUPPORTS_PCH := 1
else
  $(error Unsupported compiler version $(CARBON_GCCVER))
endif

# Where gcc wants to look for executables
CARBON_GCC_BIN_Linux := i686-pc-linux-gnu
CARBON_GCC_BIN_Linux64 := x686_64-unknown-linux-gnu
CARBON_GCC_CONFIG := $(CARBON_GCC_BIN_$(CARBON_TARGET_ARCH))

# We could potentially need to run gcc compiled for the target architecture
# and speedcc compiled for the host architecture, so we'd need both libraries.
CARBON_LIBRARY_PATH := $(CARBON_HOME)/$(CARBON_HOST_ARCH)/gcc/$(CARBON_GCC_LIB_Host)
ifneq ($(CARBON_HOST_ARCH),$(CARBON_TARGET_ARCH))
# If host doesn't match target, append target libraries.
# (Can't use += because that adds a space.)
  CARBON_LIBRARY_PATH := $(CARBON_LIBRARY_PATH):$(CARBON_HOME)/$(CARBON_TARGET_ARCH)/gcc/$(CARBON_GCC_LIB_Target)
endif

# Use "env -i" to prevent user's environment from causing problems.
# We set COMPILER_PATH to find our copies of tools like `as' and `ld'.
CARBON_COMPILER_ENV = env -i LANG=C COMPILER_PATH=${CARBON_HOME}/${CARBON_TARGET_ARCH}/bin \
		      HOME=$(HOME) LD_LIBRARY_PATH=$(CARBON_LIBRARY_PATH)

CARBON_CBUILD_CC  = $(CARBON_COMPILER_DIST)/bin/gcc
CARBON_CBUILD_CXX = $(CARBON_COMPILER_DIST)/bin/g++
CARBON_CBUILD_LD  = $(CARBON_CBUILD_CXX)

# Indicate we're gcc 3.x
CARBON_GCC3=1

##
# Search all passed in options for -O<level> flags
CARBON_CXX_OPTLIST:=$(filter -O%,$(CXX_EXTRA_FLAGS) $(CXX_WC_FLAGS))

# Find the last one specified
ifeq (,$(CARBON_CXX_OPTLIST))
CARBON_CXX_OPT := -O2
else
CARBON_CXX_OPT:=$(word $(words $(CARBON_CXX_OPTLIST)),$(CARBON_CXX_OPTLIST))
endif

ifneq (,$(findstring -pg,$(CXX_EXTRA_FLAGS) $(CXX_WC_FLAGS)))
CARBON_PROFILING=1
endif

CARBON_OPTFLAGS = --param large-function-growth=250
ifneq (,$(findstring $(CARBON_CXX_OPT),-O2 -O3))
  CARBON_OPTFLAGS += -finline-functions 
  ifeq ($(CARBON_TARGET_ARCH),Linux)
    CARBON_OPTFLAGS += -fno-unroll-loops -march=pentium4 -msse -minline-all-stringops
  endif

  ifeq ($(CARBON_TARGET_ARCH),Linux64)
    CARBON_OPTFLAGS +=
  endif



  # options useful when you want to profile with gprof
  ifeq ($(CARBON_PROFILING),1)
    CARBON_OPTFLAGS += -finline-limit=4000
  else
    CARBON_OPTFLAGS += -finline-limit=4000 $(CARBON_EXTRA_OPTS)
  endif
else				# Not -02 or -O3
  ifneq (,$(findstring $(CARBON_CXX_OPT),-Os -O1))
    ifeq ($(CARBON_TARGET_ARCH),Linux)
      CARBON_OPTFLAGS += -march=pentium4 -msse
    endif
  else				# -O0
  endif
endif


ifeq (3.4.2,$(CARBON_GCCVER))
# 3.4.2 has problems with global cse, but 3.4.3 seems to work
# (hmmm?)
  CARBON_OPTFLAGS += -fno-gcse
endif

# Basic compiler-dependent flags
#
CARBON_CXX_FLAGS_Linux = -m32
CARBON_CXX_FLAGS_Linux64 = -m64 -D__LP64__

# Define magic values that turn C++ exceptions off/on
CARBON_EXCEPTIONS_OFF:=-fno-exceptions
CARBON_EXCEPTIONS_ON :=-fexceptions

CARBON_CXX_FLAGS = $(CARBON_EXCEPTIONS_OFF) $(CARBON_CXX_FLAGS_$(CARBON_TARGET_ARCH))
CARBON_CXX_DEF_COMPILE_FLAGS= $(CARBON_CXX_FLAGS)

ifeq ($(CARBON_PROFILING),1)
else
ifeq ($(GCC_NO_WALL),)
CARBON_CXX_SPEEDCC_COMPILE_FLAGS += -Wall -Wextra -Werror
endif
endif

CARBON_DISABLE_DYNAMIC_CAST = -fno-rtti
CARBON_ENABLE_DYNAMIC_CAST = -frtti

CARBON_CXX_SPEEDCC_COMPILE_FLAGS += -pipe $(CARBON_DISABLE_DYNAMIC_CAST) $(CXX_WC_FLAGS)


# flags needed to compile shared libraries
CARBON_SHARED_COMPILE_FLAGS= -shared -fPIC
# flags needed to link shared lib.  Note that 'LIBPATH' is defined in Makefile.mc
CARBON_SHARED_LINK_FLAGS=-Wl,-rpath,$(@D)

ifeq (,$(findstring $(CARBON_COMPILER_DIST),$(LD_LIBRARY_PATH)))
  LD_LIBRARY_PATH := $(LD_LIBRARY_PATH):$(CARBON_COMPILER_LIB_DIRS)
  export LD_LIBRARY_PATH
endif

CARBON_COMPILER_LIB_LIST:=
