// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implement CodeGen basic functionality, especially static member functions.
*/



#include <cctype>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "codegen/codegen.h"
#include "codegen/CGOFiles.h"
#include "codegen/ScheduleBucket.h"
#include "FuncLayout.h"
#include "MemoryAnalyzer.h"
#include "compiler_driver/CarbonDBWrite.h"

#include "emitUtil.h"
#include "GenSchedFactory.h"
#include "GenProfile.h"
#include "RankNets.h"

#include "util/ArgProc.h"
#include "util/UtCheckpointStream.h"
#include "util/Stats.h"
#include "util/OSWrapper.h"
#include "util/AtomicCache.h"
#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "util/UtUniquify.h"
#include "util/CbuildMsgContext.h"

#include "nucleus/NUDesign.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUInstanceWalker.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModelInterface.h"

#include "localflow/MarkReadWriteNets.h"

#include "compiler_driver/CarbonContext.h"

#include "iodb/IODBNucleus.h"

#include "codegen/CGPortIface.h"
#include "codegen/CGNetElabCount.h"
#include "codegen/InducedFaultTable.h"

#define PCH_INCLUDE "carbon_pch.h"

CodeGen* gCodeGen = NULL;


static const char* scInline = NULL;
static const char* scInlineInstrAlways = NULL;
static const char* scInlineInstrTotal = NULL;
static const char* scTestdriver = NULL;
static const char* scTestdriverVectorFile = NULL;
static const char* scTestdriverVectorCount = NULL;
static const char* scTestdriverVectorSeed = NULL;
static const char* scTestdriverClockDelay = NULL;
static const char* scTestdriverDebugOutput = NULL;
static const char* scTestdriverDumpVCD = NULL;
static const char* scTestdriverDumpFSDB = NULL;
static const char* scTestdriverNoLink = NULL;
static const char* scTestdriverResetLength = NULL;
static const char* scTestdriverUseIODB = NULL;
static const char* scTestdriverNoSpeedCC = NULL;
static const char* scTestdriverGlitchDetect = NULL;
static const char* scTestdriverCheckpoint = NULL;
static const char* scTestdriverSkipObservables = NULL;
static const char* scSchedFileLimit = NULL;
static const char* scFunctionCostLimit = NULL;
static const char* scSchedFileSpillLimit = NULL;
static const char* scLocality = NULL;
static const char* scVerboseLocality = NULL;
static const char* scNoEncrypt = NULL;
static const char* scWc = NULL;
static const char* scNoCC = NULL;
static const char* scM64 = NULL;
static const char* scM32 = NULL;
static const char* scCppAPI = NULL;
static const char* scCodegenOffsetSanity = NULL;
static const char* scVerboseDeadCode = NULL;
static const char* scNoStripMine = NULL;
static const char* scSchedPrefix = NULL;
static const char* scVerboseStripMine = NULL;
static const char* scStripMine = NULL;
static const char* scCheckpoint = NULL;
static const char* scNoCheckpoint = NULL;
static const char* scSparseMemoryThreshold = NULL;
static const char* scVerboseMemoryAnalyzer = NULL;
static const char* scMemoryCapacity = NULL;
static const char* scFunctionLayout = NULL;
static const char* scPliWrapper = NULL;
static const char* scSmallVectorCopies = NULL;
static const char* scCreatePortInterface = NULL;
static const char* scTarget = NULL;
static const char* scEmbedIODB = NULL;
static const char* scNoEmbedDB = NULL;
static const char* scProfileGenerate = NULL;
static const char* scProfileUse = NULL;
static const char* scPassBuildErrors = NULL;
static const char* scUseIndentEngine = NULL;
static const char* scAnnotateCode = NULL;
static const char* scInduceCodegenFaults = NULL;
static const char* scNoAnnotationStore = NULL;
static const char* scHideImplAnnotations = NULL;
static const char* scShowImplAnnotations = NULL;
static const char* scHideHdlAnnotations = NULL;
static const char* scShowHdlAnnotations = NULL;
static const char* scNoCompressAnnotationStore = NULL;
static const char* scCompressAnnotationStore = NULL;
static const char* scOnDemandMemoryStateThreshold = NULL;
static const char* scNoSpeedCC = NULL;
static const char* scClockGlitchDetect = NULL;
static const char* scNoClockGlitchDetect = NULL;

// Note that when the Linux-hosted cbuild is targetting Windows, this
// will unfortunately default to libdesign.a.
#if pfWINDOWS
#define CARBON_DEFOUT "libdesign.lib"
#else
#define CARBON_DEFOUT "libdesign.a"
#endif

#if pfGCC_4
#define COMPILER_NAME "gcc4"
#elif pfGCC_3 || pfWINDOWS
#define COMPILER_NAME "gcc3"
#elif pfGCC_2
#define COMPILER_NAME "gcc2"
#elif pfICC
#define COMPILER_NAME "icc"
#endif

/*+
 */
bool
CodeGen::isTristate (const NUNet *n) const
{
  bool isTristate =
    n->isPrimaryZ() or
    (n->isZ() and (mTriStateMode == eTristateModeZ));

  return isTristate;
}

CodeGen::~CodeGen ()
{
  delete mChangedMap;
  // mPortIface is not always allocated.
  if (mPortIface)
    delete mPortIface;
  
  delete mPortAliasedHierRefs;

  delete mCosts;
  mCosts = NULL;
  delete mGenProfile;
  mGenProfile = NULL;
  delete mCGProfile;
  if (mTopLevelSchedID != -1)
    getSchedBuckets()->pop();
  mCGProfile = NULL;
  delete mBucketID;
  mBucketID = NULL;
  delete mSchedBuckets;
  mSchedBuckets = NULL;

  for (NetToCGAuxMap::iterator p = mNetToCGAuxMap->begin();
       p != mNetToCGAuxMap->end(); ++p)
  {
    CGAuxInfo* cai = p->second;
    delete cai;
  }
  delete mNetToCGAuxMap;

  for (BlockToCGAuxMap::iterator p = mBlockToCGAuxMap->begin ();
       p!= mBlockToCGAuxMap->end (); ++p)
  {
    CGAuxInfo* cai = p->second;
    delete cai;
  }
  delete mBlockToCGAuxMap;

  if (mGenSchedFactory  != NULL)
    delete mGenSchedFactory;
  delete mInternalCodeNames;
  delete mInterfaceCodeNames;
  delete mConstantPoolMap;

  delete mPredicateMapStack.top();
  mPredicateMapStack.pop();
  INFO_ASSERT(mPredicateMapStack.empty(), "PredicateMapStack is not empty");
  delete mEventMapStack.top();
  mEventMapStack.pop();
  INFO_ASSERT(mEventMapStack.empty(), "EventMapStack is not empty");
  delete mCModelCallName;
#ifdef INTERPRETER
  if (mModuleOffsetMap != NULL)
    delete mModuleOffsetMap;
#endif
  delete mCGOFiles;
  mCGOFiles = 0;
  delete mLayout;
  delete mUndeclaredTemps;
  delete mCModelCalls;

  if (mInducedFaults != NULL) {
    // A table of faults to induce for regression was created
    delete mInducedFaults;
  }
}

NUScope* CodeGen::getScope () const
{
  INFO_ASSERT (not mScopeStack.empty(), "Get from an empty scope stack");
  return mScopeStack.top ();
}

void CodeGen::pushScope (const NUScope* b)
{
  mScopeStack.push (const_cast<NUScope*>(b));
}

void CodeGen::popScope (void)
{
  INFO_ASSERT (not mScopeStack.empty (), "Pop from an empty scope stack");

  mScopeStack.pop ();
}

void CodeGen::putAllocation (size_t alloc)
{
  mClassAllocation = alloc;
}

void CodeGen::putAlignment (size_t alignment)
{
  mClassAlignment = alignment;
}

size_t CodeGen::getAllocation()
{
  return mClassAllocation;
}

size_t CodeGen::alignment (size_t align)
  {
    INFO_ASSERT (mClassAlignment != UtUINT32_MAX, "Alignment required exceeds limit");
    INFO_ASSERT (align <= 8, "alignment requested greater than 8" );  // nothing should ever be more than 8;
    mClassAlignment = std::max (mClassAlignment, align);
    return mClassAlignment;
  }


// Track storage allocation for class members.
size_t
CodeGen::allocation (size_t size, size_t align, size_t numElem)
{
  int mask = (align - 1);	// MBZ bits expected in aligned address

  INFO_ASSERT (mClassAllocation != UtUINT32_MAX, "allocation exceeds limit");

  if (mClassAllocation & mask)
    // Need to align
    mClassAllocation = (mClassAllocation + mask) & ~mask;

  size_t current = mClassAllocation;	// where current allocation goes
  mClassAllocation += size*numElem;

  alignment (align);
  return current;
}

static void sGetMakefileName(UtString* mkfilename, const UtString& buildDir)
{
  OSConstructFilePath(mkfilename, buildDir.c_str(), "Makefile");
}

void CodeGen::createCompileConfigFiles (void)
{
  ArgProc *args = mCarbonContext->getArgs();
  UtString cxxflags;

  if (mDebuggableModel) {
    cxxflags << "-g";
  } else {
    cxxflags << "-O" << (char)(mOptLevel);
  }
  
  if (mDoCheckpoint)
    cxxflags << CRYPT(" -DCARBON_SAVE_RESTORE");
  
  bool threaded = (args->getBoolValue("-multi-thread"));
  if (threaded)
    cxxflags << CRYPT(" -DMULTI_THREAD_SUPPORT");
  
  if (args->getBoolValue(scSmallVectorCopies))
    cxxflags << CRYPT(" -DCARBON_BV_SMALL_ANYTOANY");
  
  if (args->getBoolValue("-enableReplay"))
    cxxflags << CRYPT(" -DCARBON_REPLAY");

  if (isOnDemand())
    cxxflags << CRYPT(" -DCARBON_ONDEMAND");

  cxxflags << CRYPT (" -DCARBON_NO_OOB=") << int(mNoOOB);
  cxxflags << CRYPT (" -DCARBON_CHECK_OOB=") << int(mCheckOOB);
  UtString mkfilename;
  sGetMakefileName(&mkfilename, mbuildDirectory);
  
  UtString openFailReason;
  FILE* f = OSFOpen(mkfilename.c_str(), "w", &openFailReason);
  if (f == NULL)
  {
    fprintf(stderr, "%s\n", openFailReason.c_str());
    exit(1);
  }
  
  CodeStream::InducedFaultMap *induced_faults = mInducedFaults->findMap ("Makefile");

  if (induced_faults != NULL) {
    // Write the prepended lines from the induced fault map for Makefile
    UtList <UtString>::const_iterator it;
    for (it = induced_faults->begin_prepended (); it != induced_faults->end_prepended (); ++it) {
      fprintf (f, "%s\n", it->c_str ());
    }
  }

  UtString targetCompiler;
  UtString targetCompilerVersion;

  switch (mTargetCompiler) {
  case eGCC4:   targetCompiler = "gcc4"; break;
  case eGCC3:	targetCompiler = "gcc3"; break;
  case eICC:	targetCompiler = "icc"; break;
  case eSPW:	targetCompiler = "spw"; break;
  case eWINX:	targetCompiler = "winx"; break;
  case eWIN:	targetCompiler = "winx"; break; // "win" when we build native runtime
  default:	INFO_ASSERT (0, "unknown target compiler");
  }

  fprintf(f, "TARGET = %s/%s\n", mTargetDirectory.c_str(), mTargetFile.c_str());
  fprintf(f, "CARBON_REQUESTED_COMPILER = %s\n", targetCompiler.c_str ());
  fprintf(f, "CXX_EXTRA_FLAGS = %s\n", cxxflags.c_str());

  switch (mTargetCompiler) {
  case eGCC3:
    switch (mTargetCompilerVersion) {
    case eGCC_343:
      fprintf (f, "CARBON_GCC3_VERSION = 343\n");
      break;
    case eVanilla_Compiler:             // default gcc3 is gcc 3.4.5
    case eGCC_345:
      fprintf (f, "CARBON_GCC3_VERSION = 345\n");
      break;
    case eGCC_346:
      fprintf (f, "CARBON_GCC3_VERSION = 346\n");
      break;
    default:
      INFO_ASSERT (false, "bogus GCC3 version");
      break;
    }
    break;
  case eGCC4:
    switch (mTargetCompilerVersion) {
    case eVanilla_Compiler:             // default gcc4 is gcc 4.2.2
    case eGCC_422:
      fprintf (f, "CARBON_GCC4_VERSION=422\n");
      break;
    case eGCC_424:
      fprintf (f, "CARBON_GCC4_VERSION=424\n");
      break;
    case eGCC_432:
      fprintf (f, "CARBON_GCC4_VERSION=432\n");
      break;
    default:
      INFO_ASSERT (false, "bogus GCC4 version");
      break;
    }
    break;
  default:
    fprintf (f, "# no compiler version string\n");
    break;
  }
  if (precompiledHeaders ()) {
    // Safe for 3.4 and up, but no easy way to assert that...
    UtString topClass (getCGOFiles ()->genModuleFilename (getTopModule ()));
    fprintf(f, "TOP_CLASS = %s\n", topClass.c_str ());
  }
  
  // Put flags derived from the environment into the Makefile so that the
  // Makefile does not have to spawn numerous subshells
  const char* arch = getenv("CARBON_ARCH");
  if (arch == NULL) {
#if pfLINUX || pfLINUX64
    arch = "Linux";
#elif pfWINDOWS
    arch = "Win";
#endif
  }
  const char* hostArch = getenv("CARBON_HOST_ARCH");
  if (hostArch == NULL)
    hostArch = arch;
  fprintf(f, "CARBON_ARCH = %s\n", arch);
  fprintf(f, "CARBON_HOST_ARCH = %s\n", hostArch);
  
  const char* target = args->getStrValue(scTarget);
  if (! target)
    target = "(null)"; // just in case. Shouldn't happen.
  fprintf(f, "CARBON_CCVERSION = %s\n", target);
  
  if (threaded)
    fprintf(f, "THREADED = yes\n");
  
  fprintf(f, "CXX_WC_FLAGS = %s\n", mIODB->getTargetFlags ());


  // Add testdriver target if asked for on command line.
  //
  if (mDoTestDriver)
  {
    if (args->getBoolValue(scTestdriverNoSpeedCC))
      fprintf(f, "CARBON_TESTDRIVER_NO_SPEEDCC = yes\n");
  }

  if (mIsNoSpeedCC) {
    // Construct a makefile that just uses a naked C++ compiler without pumping
    // it all through SpeedCC. This is convenient for codegen debugging.
    fprintf(f, "CARBON_NO_SPEEDCC = 1\n");
  }

  if (mDoTestDriver && mLinkTestDriver)
    fprintf(f, "LINK_TESTDRIVER = yes\n");

  // We build the testdriver under almost all circumstances, cuz it's useful
  // but ONLY compile it if we think we really are going to need it.
  if (mCompileTestDriver)
    fprintf(f, "BUILD_TESTDRIVER = yes\n");

  if (getCarbonContext()->isProfiling())
    fprintf(f, "PROFILING = yes\n");

  if (precompiledHeaders ()) {
    // Define PCH_INCLUDE to be the name of the PCH file (without the .gch
    // extension). This must be defined before including Makefile.mc because
    // the behaviour is modified when PCH_INCLUDE is defined.
    fprintf(f, "PCH_INCLUDE = " PCH_INCLUDE "\n");
  }

  fprintf(f, "include $(CARBON_HOME)/makefiles/Makefile.mc\n");

  if (precompiledHeaders ()) {
    // Add a dependency to PCH-target to the makefile. This needs to appear
    // after the include of Makefile.mc so that $(TARGET) does not become the
    // default make target.
    fprintf(f, "$(TARGET): PCH-target\n");
  }

  if (induced_faults != NULL) {
    // Write the appended lines from the induced fault map for Makefile
    UtList <UtString>::const_iterator it;
    for (it = induced_faults->begin_appended (); it != induced_faults->end_appended (); ++it) {
      fprintf (f, "%s\n", it->c_str ());
    }
  }
  fclose(f);
}

//! Parse the backend compilation output with an explicit libdesign.codegen.errors file
void CodeGen::parseMakeOutput (UtString &filename, bool debugging, bool inBuildDirectory)
{
  CompilationErrorScanner errors (mSourceLocatorFactory, *mMsgContext, filename.c_str (),
    (debugging ? CodegenErrorLexer::cDEBUG : 0));
  errors.scan ();

  if (errors.numWarnings () > 0) {
    mMsgContext->CGCompilerWarnings (filename.c_str ());
  }

  if ( annotateGeneratedCode () ) {
    UtString codeAnnotationScriptPathname;
    codeAnnotationScriptPathname << (inBuildDirectory ? "../" : "./") 
                                 << mTargetName << ".searchannotations.csh";
    errors.buildSearchAnnotationsScript ( codeAnnotationScriptPathname,
      mMsgContext);
  }
}

// Set up a make command to do the target, outputting stdout to logFile.
int CodeGen::runMakeStep (const UtString& target, const char* logFile, const char* errMsg)
{
  UtString makeCmd;
#if pfWINDOWS
//  makeCmd = "C:/cygwin/tools/windows/bin/make.exe";
  makeCmd = "C:/cygwin/bin/make.exe CARBON_TARGET_ARCH=Win CARBON_HOST_ARCH=Win CARBON_COMPILER_DIST="; // Why doesn't PATH resolve this w/o dir?
#elif pfICC
  makeCmd = "env MAKEFLAGS= nice ${CARBON_HOME}/bin/make -l4";
#else
  long cpus = sysconf(_SC_NPROCESSORS_ONLN);
  makeCmd = "env MAKEFLAGS= nice ${CARBON_HOME}/bin/make -l";
  makeCmd << (UInt32)(3*cpus);
#endif
  makeCmd += " --no-print-directory ";
  if (isProfileGenerate())
    makeCmd += "CARBON_PGO_FLAGS=-fprofile-generate ";
  if (isProfileUse())
    makeCmd += "CARBON_PGO_FLAGS=-fprofile-use ";
  UtString mkfilename;
  sGetMakefileName(&mkfilename, mbuildDirectory);
  makeCmd << " -f " << mkfilename;

  ArgProc* args = getCarbonContext ()->getArgs ();

  SInt32 jflag = -1;		// Assume no jflag present..
  args->getIntLast("-j", &jflag);
  makeCmd << " -j" << std::max((SInt32) 1, jflag);
  makeCmd << " " << target << " >" << logFile;

  // redirect STDERR in make subprocess to libdesign.codegen.errors file.
  UtString buildErrorFile (mTargetName);
  buildErrorFile << ".codegen.errors";
  UtString ulinkError;
  UtString path ("../");
  path << buildErrorFile;    

  // delete any existing file before we start, but only once for a cbuild run
  static bool deleted_error_file = false;
  if (not deleted_error_file)
  {
    // The intent is to allow accretion of any errors during the backend 'make'
    // run as appended lines onto the libdesign.codegen.errors file.  (Note
    // that this function may be called up to 4 times in one cbuild execution.)

    deleted_error_file = true;
    OSUnlink (path.c_str (), &ulinkError); // Ignore any error (such as ENOFILE)
  }

  if (isPassingBuildErrors ()) {
    // Do not redirect standard error of the make... -passBuildErrors was specified.
  } else {
    makeCmd << " 2>>" << path;	// Note that output lines append to existing file
  }
  UtString sysMsg;

  int status = OSSystem(makeCmd.c_str(), &sysMsg);

  // Scan libdesign.codegen.errors before writing the more general error
  // message. The error scanner may be more specific in diagnosing errors.
  SInt64 file_size = 0;
  UtString sizeError;
  if (isPassingBuildErrors ()) {
    // no libdesign.codegen.errors file will exist
  } else if (!OSGetFileSize(path.c_str(), &file_size, &sizeError)) {
      // something broke getting the size of the file... maybe it doesn't even
      // exist... whatever, just complain the error stream
      mMsgContext->CGFail (sizeError.c_str ());
  } else if (file_size > 0) {
    // non-empty libdesign.codegen.errors file... pump it through the error scanner
    parseMakeOutput (path, /*debugging=*/ false, /*inBuildDirectory=*/ true);
  } else if (OSUnlink (path.c_str (), &ulinkError) != 0) {
    // the file was empty but deletion failed
    mMsgContext->CGFail (ulinkError.c_str ());
  } else {
    // deleted the empty libdesign.codegen.errors file
  }

  // Write the more general message... with the admonition to look at
  // libdesign.codegen.errors
  if ((status == -1) && ! sysMsg.empty()) {
    UtString msg;
    msg << "Compilation " << sysMsg;
    mMsgContext->CGCompilerErrors(msg.c_str(), buildErrorFile.c_str ());
  } else if (status != 0) {
    mMsgContext->CGCompilerErrors(errMsg, buildErrorFile.c_str ());
  }

  return status;
}

//! Creates a shell script file to process gcc-form errors with 'searchannotations'
/*! A list of SourceLocator items had been created by the prior action of scanning
 *  a non-empty libdesign.codegen.errors.file for gcc-form error lines. If the list
 *  is non-empty, generate the shell script with an invocation of 'searchannotations'
 *  for each item. (See Bug #7682 or
 *  http://intranet/twiki/bin/view/Eng/CodeAnnotation )
 */
void
CodeGen::CompilationErrorScanner::buildSearchAnnotationsScript( UtString scriptFilePathAndName,
							        MsgContext *mMsgContext)
{
  if ( ! mSlocList.empty() ) {

    // delete existing script file before we start.
    UtString ulinkError;
    OSUnlink (scriptFilePathAndName.c_str (), &ulinkError); // Ignore any error (such as ENOFILE)
    
    UtOStream* fscript = new UtOBStream (scriptFilePathAndName.c_str () );
    if ( !fscript->is_open() ) {
      // Message is only a warning: don't kill rest of this compilation
      mMsgContext->CGCannotOpenScriptFile ( scriptFilePathAndName.c_str (),
					    fscript->getErrmsg() );
      return;
    }

    *fscript << "#!/bin/csh -f\n";
    *fscript << "# Machine-generated script.  Please do not edit.\n";
    *fscript << "echo \"\"\n";
    *fscript << "date\n";
    *fscript << "pwd\n";

    // Test for existence of $CARBON_HOME/bin/searchannotations file in the script
    *fscript << "if (! -e $CARBON_HOME/bin/searchannotations) then\n";
    *fscript << "  echo Check product installation:\n";
    *fscript << "  echo   $CARBON_HOME/bin/searchannotations   file not found.\n";
    *fscript << "  echo Rerun this script when it is present.\n";
    *fscript << "  exit 1\n";
    *fscript << "endif\n";

    UtList <SourceLocator>::const_iterator it;
    for( it = mSlocList.begin () ;
	 it != mSlocList.end () ;
	 it++ ) {
      SourceLocator loc = *it;
      UtString location;
      loc.compose(&location);

      // Write individual "search-this-file" invocations
      *fscript << "$CARBON_HOME/bin/searchannotations  " << location << "\n";
      *fscript << "echo \"\"\n";
    }

    *fscript << "echo \"\"\n";
    *fscript << "echo \"Please report the above to Carbon Design Customer Support.\"\n";
    *fscript << "echo \"\"\n";

    fscript->flush();		// make sure there is some content at closing

    // "chmod +x" the script file
#if !pfWINDOWS
    UtString sys_msg;
    UtString chmod_cmd = "chmod +x ";
    chmod_cmd << scriptFilePathAndName;
    int status;
    status = OSSystem( chmod_cmd.c_str (), &sys_msg);
    if ( status != 0 ) {
      mMsgContext->CGCannotChmodScriptFile ( chmod_cmd.c_str (), sys_msg.c_str () );
    }
#endif

    mMsgContext->CGPleaseRunScriptFile( scriptFilePathAndName.c_str () );

  } //  ! mSlocList.empty()
}

// Compile the simulation.  Do this by running a make file that just
// compiles all the source files it finds and produces a TARGETLIB in
// the location specified by the -o flag (but relative to the user's
// source directory, NOT the hidden directory we've chdir-ed into.
//
void
CodeGen::compileSimulation (void)
{
  ArgProc* args = mCarbonContext->getArgs();

  bool noCC = args->getBoolValue (scNoCC);

  bool isBackendCompileOnly = mCarbonContext->isBackendCompileOnly();
  if (isBackendCompileOnly or isProfileUse())
  {
    if (isBackendCompileOnly && ! args->getBoolValue("-q"))
      mMsgContext->CGBackendCompile();
    
    // have to set up the builddir variable before we can cd to it.
    setupBuildDirectoryVars();
    chdirToBuildDir();
    if (isProfileUse())
      // Complain if data from running -profileGenerate model isn't here.
      verifyFeedbackExists();
  }
  else
  {
    // Create table for mapped names
    UtString tableFilename;
    tableFilename << mCarbonContext->getFileRoot() << ".xref";
    UtString errmsg;
    if (!mCodeNames->dumpTable(tableFilename.c_str(), &errmsg))
      mMsgContext->CGFileError(tableFilename.c_str(), errmsg.c_str());
    
    chdirToBuildDir();
    createCompileConfigFiles();

    if (args->isParsed(CarbonContext::scVSPCompileFileFlag) == ArgProc::eParsed)
    {
      // This is an internal compile flag and I don't expect an
      // absolute path on the filename.
      // vspCompFlagFile is the name of the file that vspcompiler will
      // look for to know that the front-end of the compile has
      // finished and the backend can commence.
      const char* vspCompFlagFile = args->getStrValue(CarbonContext::scVSPCompileFileFlag);
      UtString compFlagFile, compFlagDir;
      OSParseFileName(vspCompFlagFile, &compFlagDir, &compFlagFile);
      INFO_ASSERT(compFlagDir.empty(), "Misuse of compile file flag. No absolute paths allowed.");
      
      OSConstructFilePath(&compFlagFile, mcurrentDirectory.c_str(), compFlagFile.c_str());
      
      // The creation of this file tells vspcompiler to run the
      // backend compile.
      UtOBStream vspCompFlagFileStream(compFlagFile.c_str());
      if (! (vspCompFlagFileStream.is_open() && vspCompFlagFileStream.close()))
        mMsgContext->CGFileProbNonFatal(vspCompFlagFileStream.getErrmsg(), "cbuild will not run the backend compile automatically. Run cbuild again with the same options but add -backendCompileOnly");
    }
  }
  
  // If we're using precompiled headers, build the .pch first
  bool make_pch_has_failed = false;
  if (precompiledHeaders ()
      and not noCC
      and not isProfileUse()) {
    UtString pchCmd ("PCH-target");
    if (runMakeStep (pchCmd, "make.pch.log", "Precompiled header errors:" ) == 0) {
      // making of the pch file was successful
    } else if (getenv ("CARBON_BIN") != NULL) {
      // CARBON_BIN is set only when we are running internally. So, make
      // NoPCHFile an error forcing the developer to do something about the
      // missing PCH file... even if it's only raising a bug report.
      mMsgContext->CGPCHMakeFailedInternal (mTargetName.c_str (), "make.pch.log");
      make_pch_has_failed = true;
    }

    if ( not make_pch_has_failed ) {
      // the make did not fail, now check that the pch file was actually created
      UtString statResult;
      if ( not (OSStatFile (PCH_INCLUDE ".gch", "e", &statResult)) ){
        make_pch_has_failed = true;
      }
    }
    
    if (make_pch_has_failed) {
      // For some reason there is no PCH file.
      if (getenv ("CARBON_BIN") != NULL) {
        // When running within carbon regression tests consider this
        // an error condition so we don't waste regression test time
        mMsgContext->putMsgNumSeverity (MsgContext::e_NoPCHFile, MsgContextBase::eError);
      }
      mMsgContext->NoPCHFile ();
    }
  }

  // If doing -profileUse, we need to delete the .o files so they'll recompile.
  if (isProfileUse()) {
    UtString pchCmd("clean-obj");
    runMakeStep(pchCmd, "make.clean-obj.log", "Errors deleting object files:");
  }

  // Lastly run the makefile.  Limit the load-factor to 4 so that our -j's
  // don't runaway with the machine....
  int buildStat = 0;
  if (make_pch_has_failed) {
    // make PCH-target has failed. Do not run again because we will just get
    // the same errors
  } else if (not noCC)
  {
    UtString defaultTarget ("");
    buildStat = runMakeStep (defaultTarget, "make.log", "Compilation errors:");
  }

  // If generating a SystemC wrapper, also run make on the 'coware' target
  // to generate a Tcl command file to set CoWare compile and link options.
  if (!noCC && (buildStat == 0) && mCarbonContext->doGenerateSystemCWrapper())
  {
    UtString cowareCmd(" -s coware");
    runMakeStep (cowareCmd, "make.coware.log", "CoWare command file errors: ");
  }

  // Cleanup any temporaries
  cleanupBuildDirectory ();

} // CodeGen::compileSimulation

CodeGen::CodeGen(CarbonContext* cbn, IODBNucleus* iodb, IODBTypeDictionary* dict,
		 NUNetRefFactory* nrf,
		 SourceLocatorFactory* srcLocFactory,
		 AtomicCache* atomicCache,
		 ESPopulateExpr* exprsyn, MsgContext* msgContext)
  : mIODB(iodb),
    mCarbonContext(cbn),
    mTypeDictionary(dict),
    mTriStateMode (eTristateModeX),
    mNetRefFactory (nrf),
    mSched (NULL),
    mMsgContext(msgContext),
    mSourceLocatorFactory(srcLocFactory),
    mAtomicCache(atomicCache),
    mTargetCompiler (eNoCC),
    mTargetCompilerVersion (eVanilla_Compiler),
    mESPopulate (exprsyn),
    mFold (0),
    mGenSchedFactory (NULL),
    mCGOFiles (0),
#ifdef INTERPRETER
    mModuleOffsetMap(NULL),
#endif
    mPortIface (0),
    mLayout (0),
    mTopLevelSchedID(-1),
    mInducedFaults (NULL),
    mHdlAnnotations (CodeAnnotation::cDEFAULT_HDL),
    mImplAnnotations (CodeAnnotation::cDEFAULT_IMPL)
{
  INFO_ASSERT(gCodeGen == NULL, "gCodeGen is not clear");
  gCodeGen = this;

  mChangedMap = new CGNetElabCount;
  
  mNumExtParams = 0;
  mTopName = 0;
  mOptLevel = '2';                // default
  mInternalCodeNames = 0;
  mInterfaceCodeNames = 0;
  mCodeNames = 0;

  mMaxMemoryWidth = 0;          // no memories seen yet by codegen...

  // Initialize to garbage to identify protocol transgressions
  stopClassData ();

  mNetToCGAuxMap = new NetToCGAuxMap;
  mBlockToCGAuxMap = new BlockToCGAuxMap;

  // Initialize caches used to allow reuse of schedule.cxx temporaries.
  mPredicateTempCount = 0;
  mPredicateMapStack.push(new PredicateToIntMap);
  mEventTempCount = 0;
  mEventMapStack.push(new EventToIntMap);

  mNamespace = CRYPT("CarbonDesignSystems__");
  cBitVector_h_path= CRYPT("util/BitVector.h");
  cMemory_h_path= CRYPT("codegen/Memory.h");
  cSparseMemory_h_path= CRYPT("codegen/SparseMemory.h");
  cTristate_h_path= CRYPT("codegen/Tristate.h");
  cTristateBV_h_path= CRYPT("codegen/TristateBV.h");
  cCheckpointStream_h_path = CRYPT("util/UtCheckpointStream.h");
  cCarbonRunTime_h_path = CRYPT ("util/CarbonRunTime.h");
  cForcible_h_path = CRYPT ("codegen/Forcible.h");
  cForciblePOD_h_path = CRYPT ("codegen/ForciblePOD.h");
  cShellNetCreate_h_path = CRYPT("shell/carbon_type_create.h");
  cMemoryCreateInfo_h_path = CRYPT("shell/ShellMemoryCreateInfo.h");
  cChangeMapFnName = CRYPT("sMapChangeArrayToStorage");

  mPrecompiledHeaders = pfPRECOMPILED_HEADERS; // Default based on compiler used to build cbuild

  mDoTestDriver = false;
  mCompileTestDriver = false;
  mLinkTestDriver = true;
  //  mGeneralTypeAttribs = eTypeNoAttrib;
  mGeneratingMem = false;
  mGeneratingSparseMem = false;
  mGeneratingBVs = false;
  mDebuggableModel = false;

  mCosts = new NUCostContext;
  mGenProfile = new GenProfile();

  // Assume there are no pooled constants required.
  mConstantPool = 0;
  mConstantPoolMap = new ModuleConstantPoolMap ();

  mCModelCallName  = new UtString;
  mCModelCallIndex = 0;

  mPortAliasedHierRefs = new NUNetSet;
  mUndeclaredTemps = new UndeclaredTemps;
  mCGProfile = new CGProfile;
  mBucketID = new CGBucketID;
  mSchedBuckets = new ScheduleBuckets;
  mCModelCalls = new NUCModelCalls;

  // Init onDemand state offset tracker
  mOnDemandStateSize = 0;
}

void CodeGen::setupOptions() {
  ArgProc* args = mCarbonContext->getArgs();
  
  scInline = CRYPT("-inline");
  scInlineInstrAlways = CRYPT("-inlineInstrAlways");
  scInlineInstrTotal = CRYPT("-inlineInstrTotal");
  scTestdriver = CRYPT("-testdriver");
  scTestdriverVectorFile = CRYPT("-testdriverVectorFile");
  scTestdriverVectorCount = CRYPT("-testdriverVectorCount");
  scTestdriverVectorSeed = CRYPT("-testdriverVectorSeed");
  scTestdriverClockDelay = CRYPT("-testdriverClockDelay");
  scTestdriverDebugOutput = CRYPT("-testdriverDebugOutput");
  scTestdriverDumpVCD = CRYPT("-testdriverDumpVCD");
  scTestdriverDumpFSDB = CRYPT("-testdriverDumpFSDB");
  scTestdriverNoLink = CRYPT("-testdriverNoLink");
  scTestdriverResetLength = CRYPT("-testdriverResetLength");
  scTestdriverUseIODB = CRYPT("-testdriverUseIODB");
  scTestdriverNoSpeedCC = CRYPT("-testdriverNoSpeedCC");
  scTestdriverCheckpoint = CRYPT("-testdriverCheckpoint");
  scTestdriverGlitchDetect = CRYPT("-testdriverGlitchDetect");
  scTestdriverSkipObservables = CRYPT("-testdriverSkipObservables");
  scSchedFileLimit = CRYPT("-schedFileLimit");
  scFunctionCostLimit = CRYPT ("-codegenFunctionCostLimit");
  scSchedFileSpillLimit = CRYPT("-schedFileSpillLimit");
  scLocality = CRYPT("-locality");
  scVerboseLocality = CRYPT("-verboseLocality");
  scNoEncrypt = CRYPT("-noEncrypt");
  scWc = CRYPT("-Wc");
  scNoCC = CRYPT("-nocc");
  scM64 = CRYPT("-m64");
  scM32 = CRYPT("-m32");
  scCppAPI = CRYPT("-cppAPI");
  scCodegenOffsetSanity = CRYPT("-codegenOffsetSanity");
  scVerboseDeadCode = CRYPT ("-verboseDeadCode");
  scNoStripMine = CRYPT("-noStripMine");
  scVerboseStripMine = CRYPT ("-verboseStripMine");
  scStripMine = CRYPT ("-stripMine");
  scSchedPrefix = CRYPT("-schedPrefix");
  scCheckpoint = CRYPT("-checkpoint");
  scNoCheckpoint = CRYPT("-noCheckpoint");
  scSparseMemoryThreshold = CRYPT("-sparseMemoryThreshold");
  scVerboseMemoryAnalyzer = CRYPT("-verboseMemoryAnalyzer");
  scMemoryCapacity = CRYPT("-memoryCapacity");
  scFunctionLayout = CRYPT("-codegenFunctionLayout");
  scPliWrapper = CRYPT("-pliWrapper");
  scSmallVectorCopies = CRYPT("-smallVectorCopies");
  scCreatePortInterface = CRYPT("-createPortInterface");
  scTarget = CRYPT("-target");
  scEmbedIODB = CRYPT("-embedIODB");
  scNoEmbedDB = CRYPT("-noEmbedDB");
  scProfileGenerate = CRYPT("-profileGenerate");
  scProfileUse = CRYPT("-profileUse");
  scPassBuildErrors = CRYPT("-passBuildErrors");
  scUseIndentEngine = CRYPT("-useIndentEngine");
  scAnnotateCode = CRYPT("-annotateCode");
  scInduceCodegenFaults = CRYPT("-induceCodegenFaults");
  scNoAnnotationStore = CRYPT("-noAnnotationStore");
  scHideImplAnnotations = CRYPT("-hideImplAnnotations");
  scShowImplAnnotations = CRYPT("-showImplAnnotations");
  scHideHdlAnnotations = CRYPT("-hideHdlAnnotations");
  scShowHdlAnnotations = CRYPT("-showHdlAnnotations");
  scNoCompressAnnotationStore = CRYPT("-noCompressAnnotationStore");
  scCompressAnnotationStore = CRYPT("-compressAnnotationStore");
  scOnDemandMemoryStateThreshold = CRYPT("-onDemandMemoryStateLimit");
  scNoSpeedCC = CRYPT("-noSpeedCC");
  scClockGlitchDetect = CRYPT("-clockGlitchDetect");
  scNoClockGlitchDetect = CRYPT("-noClockGlitchDetect");

  args->addBool("-debug", "Emit a makefile that builds a debuggable model", 
                false, CarbonContext::ePassCarbon);
  args->addString("-O", "Use this option to control the design optimization level. The level can be an integer between 0 and 3, with 0 implying no optimization, or 's', which optimizes for space.",
               (pfGCC_3_4 | pfGCC_4) ? "2" : "1",
	       false, false, CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scGenCompileControl, "-O");
  
  args->addInt ("-j", "Use this option to limit parallel make sub-jobs, i.e., the number of object compilations running in parallel. Set this to 1 for serial runs. You may set this option to any positive non-zero value you want; however, compile performance degradation may occur if it is set too high.", 4, false, false, CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scGenCompileControl, "-j");

  args->addBool (CRYPT("-dumpCGStats"), 
                 CRYPT("Dump codegen statistics"), false, 
                 CarbonContext::ePassCarbon);
  args->addBool (scInline, 
                 CRYPT("Enable smart inlining"), true, 
                 CarbonContext::ePassCarbon);
  args->addBoolOverride (CRYPT("-noInline"), scInline);
  
  args->addInt (scInlineInstrAlways, CRYPT("Maximum instruction count to always inline"), 10, false, false, CarbonContext::ePassCarbon);
  args->addInt (scInlineInstrTotal, CRYPT("Maximum instruction * calls count to inline"), 20, false, false, CarbonContext::ePassCarbon);
  args->addBool(scTestdriver, 
                CRYPT("Generate an automated test driver"), false, 
                CarbonContext::ePassCarbon);
  args->addBool(scTestdriverDebugOutput, 
                CRYPT("Generate vector display in a more human friendly format"), false, CarbonContext::ePassCarbon);
  args->addString(scTestdriverVectorFile, CRYPT("File of vector for the test driver to read"),
    NULL, true, false, CarbonContext::ePassCarbon);
  args->addInt(scTestdriverVectorCount, CRYPT("Number of vectors to create for the test driver"), 64, false, false, CarbonContext::ePassCarbon);
  args->addInt(scTestdriverVectorSeed, CRYPT("Provide a new seed for generating random testdriver vectors"), 0, false, false, CarbonContext::ePassCarbon);
  args->addInt(scTestdriverClockDelay, CRYPT("Time (in ns) that the clock signal will be delayed after data inputs are changed"), 0, false, false, CarbonContext::ePassCarbon);
  args->addBool(scTestdriverDumpVCD, CRYPT("Generate a Verilog/VHDL and C testbench that dumps out VCD"), false, CarbonContext::ePassCarbon);
  args->addBool(scTestdriverDumpFSDB, CRYPT("Generate a VHDL and C testbench that dumps out FSDB"), false, CarbonContext::ePassCarbon);
  args->addBool(scTestdriverNoLink, CRYPT("Don't link the final executable, just leave the main.o"), false, CarbonContext::ePassCarbon);
  args->addInt(scTestdriverResetLength, CRYPT("Provide the number of cycles reset should be held on"), 5, false, false, CarbonContext::ePassCarbon);
  args->addBool(scTestdriverUseIODB, CRYPT("Generate a testbench that uses the io db instead of the default full symtab db."), false,
                CarbonContext::ePassCarbon);

  args->addBool(scTestdriverNoSpeedCC, CRYPT("When compiling main.cxx for testdriver, do not build it with speedcc but rather the default C++ compiler."), 
                false, CarbonContext::ePassCarbon);
  args->addBool(scTestdriverCheckpoint, CRYPT("When compiling main.cxx for testdriver, include code to test checkpoint save/restore."), 
                false, CarbonContext::ePassCarbon);
  args->addBool(scTestdriverGlitchDetect, 
                CRYPT("Enable run-time clock glitch detection"),
                false, CarbonContext::ePassCarbon);
  args->addBool(scTestdriverSkipObservables, CRYPT("Generate a VHDL and C testbench that dumps out FSDB, but skips observable signals"), false, CarbonContext::ePassCarbon);
  args->addBool(scCheckpoint, CRYPT("Enable generation of checkpoint save/restore support in the Carbon Model. When enabled, Carbon API functions may be used to save and restore the state of the model during run time."), true, CarbonContext::ePassCarbon);
  args->addSynonym(scCheckpoint, CRYPT("-chkpt"), true);
  args->addBoolOverride(scNoCheckpoint, scCheckpoint);
  args->addToSection(CarbonContext::scModuleControl, scCheckpoint);
  args->addToSection(CarbonContext::scModuleControl, scNoCheckpoint);

  args->addInt( scSparseMemoryThreshold, CRYPT("Deprecated. Please use the -memoryCapacity switch."), 2049, false, false, CarbonContext::ePassCarbon );
  args->putIsDeprecated(scSparseMemoryThreshold, true);

  args->addInt( scMemoryCapacity, CRYPT("Specify the total amount of runtime memory allocated for modelling memories. Memories which do not fit into this threshold will be modelled using a space-efficient representation. A value of 0 will produce only sparse memories, while a value of -1 will produce no sparse memories."), 4*1024*1024, false, false, CarbonContext::ePassCarbon );
  args->addToSection(CarbonContext::scNetControl, scMemoryCapacity);
  args->addBool( scVerboseMemoryAnalyzer, CRYPT("Dump a histogram of memory bit sizes and elaboration counts."), false, CarbonContext::ePassCarbon );

  args->addBool(scFunctionLayout, CRYPT("Layout functions according to schedule call order.  Improves runtime at the expense of compile time."), true, CarbonContext::ePassCarbon);
  args->addBoolOverride (CRYPT("-noCodegenFunctionLayout"), scFunctionLayout);

  UtString defout (CARBON_DEFOUT);

  args->addInt (scFunctionCostLimit,
                CRYPT ("Limit the number of functions in a schedule layout file by total cost."),
                30000, false, false, CarbonContext::ePassCarbon);

  args->addInt (scSchedFileLimit, 
		CRYPT("Schedule functions larger than this limit are separately compiled."), 
		GenSched::LARGEST_INLINE_FUNCTION,
                false, false, CarbonContext::ePassCarbon);
  args->addInt (scSchedFileSpillLimit, 
		CRYPT("Large schedule functions (determined by -schedFileLimit) are compiled together until the containing file reaches this threshold."),
		GenSched::LINES_PER_SCHEDULE_SPILL_FILE,
                false, false, CarbonContext::ePassCarbon);
  args->addInt (scLocality, 
		CRYPT("Sort data members by one of a number of techniques.\n\<verbatim>        0 --> variable-size\n        1 --> schedule position of last def\n        2 --> number of uses\n        3 --> number of non-def uses\n        4 --> schedule position of first def\n        5 --> schedule and function position of first def</verbatim>"), 0, false, true, CarbonContext::ePassCarbon);
  args->addBool (scVerboseLocality, CRYPT("Generate debugging information for locality computation."), false, CarbonContext::ePassCarbon);

  args->addOutputFile("-o", "Use this option to specify the name of all output files for the compiled model. You must specify an extension for object files of either '.a', which generates a traditional archive, '.so', which generates a linked shared library, '.lib', which generates a static Windows library, or '.dll' which generates a Windows DLL. The file argument can be a full path or a relative file name.", defout.c_str (), 
                      false, ArgProc::eRegFile, 
                      false, CarbonContext::ePassMode);
  args->addToSection(CarbonContext::scOutputControl, "-o");

  args->addBool (scPliWrapper, CRYPT("Generate wrapper to allow instantiation of design by a Verilog simulator."), false, CarbonContext::ePassCarbon);
  args->putIsDeprecated(scPliWrapper, true);

  // obfuscation is on by default.
  args->addBool (scNoEncrypt, CRYPT("Generate code that is not obfuscated and not encrypted"), false, CarbonContext::ePassCarbon);

  args->addString (scWc, 
                   CRYPT("Pass flags to C++ compiler invocations."), "", false, true, CarbonContext::ePassCarbon);
  args->allowUsage(scWc);
  
  args->addBool (scNoCC, CRYPT("Do not run the C++ compiler"), false, CarbonContext::ePassCarbon);
  args->addString (scTarget, "Use this option to set the backend compiler.  Choices: gcc (gcc-3.4.5), winx (gcc-3.4.5-mingw), win (gcc-3.4.5-mingw native), gcc343 (gcc-3.4.3), gcc346 (gcc-3.4.6 if configured), icc (Intel C Compiler), spw (SparcWorks).",
#if pfICC
		   "icc",
#elif pfSPARCWORKS
		   "spw",
#elif pfWINDOWS
		   "win",
#elif pfGCC_4
                   "gcc4",
#elif pfGCC_3
		   "gcc3",
#elif pfGCC_2
                   "gcc2",
#else
#error "Unknown target compiler"                   
#endif
                   false, true, CarbonContext::ePassCarbon); // no default, but allow multiple...
  args->addToSection(CarbonContext::scGenCompileControl, scTarget);

  args->addBool (scM64, CRYPT("Generate code for a 64-bit architecture instead of host architecture."), false, CarbonContext::ePassCarbon);
  args->addBool (scM32, CRYPT("Generate code for a 32-bit architecture instead of host architecture."), false, CarbonContext::ePassCarbon);
#if pfLINUX64
  // For now, these are internal only except on Linux64.
  args->addToSection(CarbonContext::scOutputControl, scM64);
  args->addToSection(CarbonContext::scOutputControl, scM32);
#endif
  args->addBool (scCppAPI, CRYPT("Use the internal-only C++ API"), false, CarbonContext::ePassCarbon);
  
  args->addBool(scCodegenOffsetSanity,
                CRYPT("Generate extra code to validate symbol table offsets"), false, CarbonContext::ePassCarbon);
  args->addBool(scVerboseDeadCode,
                CRYPT("Report code eliminated by CodeGen due to being unscheduled"), false, CarbonContext::ePassCarbon);

  args->addBool (scVerboseStripMine, CRYPT ("Print information about strip-mining vectors"), false, CarbonContext::ePassCarbon);
  args->addInt (scStripMine, CRYPT ("Adjust minimum width for stripmining vector expressions"), LLONG_BIT, false, false, CarbonContext::ePassCarbon);
  args->addBool (scNoStripMine, CRYPT("disable bitvector strip-mining"), 
                 false, CarbonContext::ePassCarbon);
  args->addBool ("-multi-thread", "Use this option to generate a thread-safe model. Multi-threading is supported in the Carbon API, with some exceptions. So, you may want to enable this option if you intend the model to be used in a multi-threaded environment.", false, CarbonContext::ePassCarbon);

  args->addToSection(CarbonContext::scModuleControl, "-multi-thread");
  
  args->addBool (scSchedPrefix, CRYPT("Optimize equivalent schedules"), true, CarbonContext::ePassCarbon);
  args->addBoolOverride (CRYPT("-noSchedPrefix"), scSchedPrefix);
  // temporary debugging aid
  args->addInt (CRYPT("-alignedAccess"), 
                CRYPT("Use hardware aligned accesses <= width"), 0,
                false, false, CarbonContext::ePassCarbon);
  args->addBool (CRYPT("-noSchedBranch"), 
                 CRYPT("disable branching around a large number of schedules"), false, CarbonContext::ePassCarbon);
  args->addBool (scSmallVectorCopies,
                 CRYPT("The algorithm to copy one vector to another is currently optimal for large designs and vector copies greater than 1 word. For small designs that are resident mostly in cache this switch may improve performance by using a smaller, inlined, but otherwise less optimal algorithm. This switch only has effect on the generated code compilation."), false, CarbonContext::ePassCarbon);

  args->addInt (CRYPT ("-expect"),
                CRYPT ("Turn on GCC's __builtin_expect() logic, with a 0|1 default"), -1, false, false, CarbonContext::ePassCarbon);

  args->addBool (scCreatePortInterface, CRYPT("Normally, the carbon_<design>_ports structure is not created unless -systemCWrapper is specified. This forces the creation of that structure if that option is not specified."), false, CarbonContext::ePassCarbon);

  args->addBool (CRYPT ("-noPCH"),
                 CRYPT ("Turn off precompiled headers even if compiler supports them."), false, CarbonContext::ePassCarbon);

  // DB embedding is on by default
  args->addBool (scEmbedIODB,
                 CRYPT("Embed design database in the generated library.  This makes it easier to distribute the library without needing to distribute the database files as well."),
                 true, 
                 CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scOutputControl, scEmbedIODB);
  // For clarity, the override flags is -noEmbedDB, not -noEmbedIODB.
  // Both the full and IO DBs are embedded unless the full one is
  // explicitly excluded.
  args->addBoolOverride(scNoEmbedDB, scEmbedIODB);
  


  args->addBool(scProfileGenerate, CRYPT("Compile with instrumentation for profile based optimization.  After compiling: (1) Link using gcc's -fprofile-generate option, (2) run the model, (3) recompile using Carbon compiler's -profileUse option, (4) relink."), false, CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scGenCompileControl, scProfileGenerate);

  args->addBool(scProfileUse, CRYPT("Recompile using feedback from profile directed optimization.  Use this after compiling with -profileGenerate and linking with gcc's -fprofile-generate option and running the model.  This option reuses much of the previous Carbon compiler compilation, so HDL changes and many Carbon compiler options are ignored."), false, CarbonContext::ePassMode);
  args->addToSection(CarbonContext::scGenCompileControl, scProfileUse);

  args->addBool(scPassBuildErrors, CRYPT("Write any model compilation directly to standard error instead of capturing in libdesign.codegen.errors"), false, CarbonContext::ePassCarbon);

  args->addBool(scUseIndentEngine, CRYPT("Use the indentation engine for generated C++ code; WARNING this will considerably slow down code generation, (requires -annotateCode)"), false, CarbonContext::ePassCarbon);

  args->addBool(scAnnotateCode, CRYPT("Annotate the generated C++ for C++ fault diagnosis"), false,
    CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scGenCompileControl, scAnnotateCode);

  args->addInputFile (scInduceCodegenFaults, "File containing directives for inducing codegen faults. Thisis intended for regression testing use only.", NULL, true, true, CarbonContext::ePassCarbon);

  args->addBool (scNoAnnotationStore, CRYPT ("Do not write a code annotation store"), false, CarbonContext::ePassCarbon);

  // HDL annotations default to visible
  args->addBool (scShowHdlAnnotations, CRYPT ("Write HDL-source annotations as comments to the C++ model."), true, CarbonContext::ePassCarbon);
  args->addBoolOverride (scHideHdlAnnotations, scShowHdlAnnotations);

  // Implementation annotations default to not visible
  args->addBool (scHideImplAnnotations, CRYPT ("Do not write implementation annotations as comments to the C++ mode."), true, CarbonContext::ePassCarbon);
  args->addBoolOverride (scShowImplAnnotations, scHideImplAnnotations);

  // By default, the annotation store is compressed
  args->addBool (scCompressAnnotationStore, CRYPT("Compress the annotation store directory"), true, CarbonContext::ePassCarbon);
  args->addBoolOverride (scNoCompressAnnotationStore, scCompressAnnotationStore);

  args->addInt(scOnDemandMemoryStateThreshold, CRYPT("Specify the upper threshold for memories to be included in onDemand state comparisons.  Non-sparse memories less than or equal to this size (in bits) will be included in state comparisons.  All other memories will not be saved, and changes to them will invalidate onDemand idle state searches."), 4096, false, false, CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scNetControl, scOnDemandMemoryStateThreshold);
  
  // Do not use speedcc to compile the model
  args->addBool (scNoSpeedCC, CRYPT("Do not used SpeedCC to compile the model. Requires -noEncrypt"), false, CarbonContext::ePassCarbon);

  // By default, we emit code to do clock glitch detection
  args->addBool(scClockGlitchDetect, CRYPT("Enable support for clock glitch detection in the model.  This still needs to be enabled at model creation time."), true, CarbonContext::ePassCarbon);
  args->addBoolOverride (scNoClockGlitchDetect, scClockGlitchDetect);
  args->addToSection(CarbonContext::scGenCompileControl, scClockGlitchDetect);
  args->addToSection(CarbonContext::scGenCompileControl, scNoClockGlitchDetect);

} // void CodeGen::setupOptions

const char* CodeGen::getNamespace() const
{
  return mNamespace.c_str();
}


//! Class to walk the design and remember which hierarchical references appear as port connections.
class CGFindHierRefPorts : public NUDesignCallback
{
public:
  CGFindHierRefPorts(CodeGen * codegen) : mCodeGen(codegen) {}

  Status operator()(Phase,NUBase *) { return eNormal; }
  Status operator()(Phase phase, NUPortConnectionInput * input) {
    if (phase==ePre) {
      NUExpr * actual = input->getActual();
      NUNet * net = actual->getWholeIdentifier();
      if (net->isHierRef()) {
        mCodeGen->rememberPortAliasedHierRef(net);
      }
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUPortConnectionOutput * output) {
    if (phase==ePre) {
      NULvalue * actual = output->getActual();
      NUNet * net = actual->getWholeIdentifier();
      if (net->isHierRef()) {
        mCodeGen->rememberPortAliasedHierRef(net);
      }
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUPortConnectionBid * inout) {
    if (phase==ePre) {
      NULvalue * actual = inout->getActual();
      NUNet * net = actual->getWholeIdentifier();
      if (net->isHierRef()) {
        mCodeGen->rememberPortAliasedHierRef(net);
      }
    }
    return eNormal;
  }
private:
  CodeGen * mCodeGen;
};

//! Class to walk all the modules and run initializations
class CGModuleWalk : public NUInstanceCallback
{
public:
  //! constructor
  CGModuleWalk(STSymbolTable* symTab) : 
    NUInstanceCallback(symTab), mSymTab(symTab)
  {}

  //! destructor
  ~CGModuleWalk() {}

private:
  virtual void handleModule(STBranchNode* branch, NUModule* module)
  {
    // Add all initializations here
    initializeChangeDetects(branch, module);
  }
  void handleDeclScope(STBranchNode*,NUNamedDeclarationScope*) {}
  void handleInstance(STBranchNode*,NUModuleInstance*) {}
  void initializeChangeDetects(STBranchNode* branch, NUModule* module);

  STSymbolTable* mSymTab;
}; // class CGModuleWalk : public NUInstanceCallback

// We have to create a hidden temporary net during the define phase
// step. This is because handling optimizing a change detect with a
// hidden temp net is difficult to maintain. So we delay its
// creation until the last possible time.
void
CGModuleWalk::initializeChangeDetects(STBranchNode* branch, NUModule* module)
{
  // Get all the change detects in this module
  NUChangeDetectVector changeDetects;
  module->getChangeDetects(&changeDetects,true);

  // Create a temp net for each one
  for (NUChangeDetectVectorLoop l(changeDetects); !l.atEnd(); ++l) {
    // Only create the temp net if we haven't encountered this module
    // before.
    NUChangeDetect* changeDetect = *l;
    NUNet* tempNet = changeDetect->getTempNet();
    if (tempNet == NULL) {
      // Gather the data bout this change detect to create a temp
      NUExpr* expr = changeDetect->getArg(0);
      UInt32 bitSize = expr->getBitSize();
      bool isSigned = expr->isSignedResult();
      const SourceLocator& loc = expr->getLoc();

      // Create the temp net and store it both in the module and in
      // the change detect so that we create the storage and use it
      // during simulation.
      StringAtom* atom = module->gensym("changetemp");
      tempNet = module->createTempNet(atom, bitSize, isSigned, loc);
      changeDetect->putTempNet(tempNet);
      // initChangeDetects is emitted after the class declaration that must
      // declare the member for the change temporary. So, explicitly mark the
      // tempNet for declaration of a member. (bug 6481).
      tempNet->emitCode (eCGMarkNets);
    }

    // Elaborate the temp net to make later passes happy
    tempNet->createElab(branch, mSymTab);
  }
} // CGModuleWalk::initializeChangeDetects


//! Walk every lut and create table names for them.
class LutNameCallback : public NUDesignCallback
{
public:
  LutNameCallback() {}
  ~LutNameCallback() {}

  Status operator()(Phase,NUBase*) { return eNormal; }

  //! Keep a stack of the current module so we can create unique names.
  Status operator()(Phase phase, NUModule *module)
  {
    if (phase == ePre) {
      mModuleStack.push(module);
    } else {
      mModuleStack.pop();
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUOp *op)
  {
    if ((phase == ePre) && (op->getOp() == NUOp::eNaLut)) {
      NUModule *cur_module = mModuleStack.top();
      NULut *lut = dynamic_cast<NULut*>(op);
      NU_ASSERT(lut, op);
      lut->putTableName(cur_module->gensym("lut"));
    }
    return eNormal;
  }

  UtStack<NUModule*> mModuleStack;
};


//! Create table names for look-up tables.
static void sCreateTableNames(NUDesign *nuctree)
{
  LutNameCallback callback;
  NUDesignWalker walker(callback, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.design(nuctree);
}


bool CodeGen::checkTargetArgs ()
{
  bool ret = true;
  ArgProc* arghdl = mCarbonContext->getArgs();

  // Find out which compiler we're targetting to.
  const char* compiler = arghdl->getStrLast (scTarget);
  if (strcmp (compiler, "gcc") == 0) {
    compiler = COMPILER_NAME;           // default is what we built cbuild with.
    mTargetCompilerVersion = eVanilla_Compiler;
                                        // default version of that compiler
  }

  if (strcmp (compiler, "gcc3") == 0) {
    mTargetCompiler = eGCC3;
  } else if (strcmp (compiler, "gcc343") == 0 || strcmp (compiler, "gcc-3.4.3") == 0) {
    mTargetCompiler = eGCC3;
    mTargetCompilerVersion = eGCC_343;
  } else if (strcmp (compiler, "gcc345") == 0 || strcmp (compiler, "gcc-3.4.5") == 0) {
    mTargetCompiler = eGCC3;
    mTargetCompilerVersion = eGCC_345;
  } else if (strcmp (compiler, "gcc346") == 0 || strcmp (compiler, "gcc-3.4.6") == 0) {
    mTargetCompiler = eGCC3;
    mTargetCompilerVersion = eGCC_346;
  } else if (strcmp (compiler, "gcc4") == 0) {
    mTargetCompiler = eGCC4;
  } else if (strcmp (compiler, "gcc422") == 0) {
    mTargetCompiler = eGCC4;
    mTargetCompilerVersion = eGCC_422;
  } else if (strcmp (compiler, "gcc424") == 0) {
    mTargetCompiler = eGCC4;
    mTargetCompilerVersion = eGCC_424;
  } else if (strcmp (compiler, "gcc432") == 0) {
    mTargetCompiler = eGCC4;
    mTargetCompilerVersion = eGCC_432;
  } else if (strcmp (compiler, "icc") == 0) {
    mTargetCompiler = eICC;
  } else if (strcmp (compiler, "spw") == 0) {
    mTargetCompiler = eSPW;
  } else if (strcmp (compiler, "winx") == 0) {
    mTargetCompiler = eWINX;
  } else if (strcmp (compiler, "win") == 0) {
    mTargetCompiler = eWIN;
  } else {
    // Invalid target - error!
    mMsgContext->CGUnknownTarget (compiler);
    ret = false;
  }

  // If we are building with GCC and the system was built with gcc 3/4 then default
  // the target compiler version to the same version used for the system.
  if (mTargetCompiler == eGCC3 && mTargetCompilerVersion == eVanilla_Compiler) {
#if pfGCC_3_4_3
    mTargetCompilerVersion = eGCC_343;
#endif
#if pfGCC_3_4_5
    mTargetCompilerVersion = eGCC_345;
#endif
#if pfGCC_3_4_6
    mTargetCompilerVersion = eGCC_346;
#endif
  }

  if (mTargetCompiler == eGCC4 && mTargetCompilerVersion == eVanilla_Compiler) {
#if pfGCC_4_2_2
    mTargetCompilerVersion = eGCC_422;
#endif
#if pfGCC_4_2_4
    mTargetCompilerVersion = eGCC_424;
#endif
#if pfGCC_4_3_2
    mTargetCompilerVersion = eGCC_432;
#endif
  }

  // Give a warning if the target compiler is a more recent release than the
  // compiler used to compile the system.
#if pfGCC_3_4_3
  if (mTargetCompiler == eGCC3 && mTargetCompilerVersion > eGCC_343) {
    mMsgContext->CGTargetVersionNewer ("gcc 3.4.3");
  }
#endif
#if pfGCC_3_4_5
  if (mTargetCompiler == eGCC3 && mTargetCompilerVersion > eGCC_345) {
    mMsgContext->CGTargetVersionNewer ("gcc 3.4.5");
  }
#endif

  // We believe the -target, but check for the file extensions and
  // switch to -winx if the extension is .lib or .dll. [BUG1907]

  bool do_windows = false;
  const char *targetFile;
  arghdl->getStrValue ("-o", &targetFile);

  const char *dot = strrchr (targetFile, '.');
  bool is_windows_name = (dot != NULL) && (!strcmp (dot, ".lib") || !strcmp (dot, ".dll"));
  if (dot == NULL) {
    // no suffix was specified
  } else if (is_windows_name) {
    // these suffixes are windows specific so switch the target to eWINX.
    do_windows = true;
  } else if ((mTargetCompiler == eWINX || mTargetCompiler == eWIN)
             && !is_windows_name && strcmp (targetFile, CARBON_DEFOUT)) {
#if 0
    // Windows has been specified as the target, but the requested suffix is
    // not windows friendly.
    //
    // While it seems like a good idea to write out this error message here, it
    // turns out that a number of regression tests are broken. For now, it is
    // just easier to comment out the warning and come back later to fix the
    // regression tests.
    mMsgContext->CGUnfriendlyLibraryName (targetFile, "Windows");
#endif
  }

  if (!do_windows) {
    // the library name did not imply windows
  } else if (mTargetCompiler == eWINX || mTargetCompiler == eWIN) {
    // I guess this should work for windows...
  } else if (mTargetCompiler != eGCC4 || mTargetCompilerVersion > eGCC_422) {
    mMsgContext->CGBadWindowsCompiler (compiler);
    ret = false;
  } else {
    // switch the compiler to eWINX
    mTargetCompiler = eWINX;
    mTargetCompilerVersion = eGCC_345;
  }

  // Now that we have a target compiler, check that it really is supported on
  // this host. CGBadPlatform is an alert, so it can be demoted if the user
  // really wants to shoot themselves in the foot.

#if pfLINUX
  switch (mTargetCompiler) {
  case eGCC3:
  case eGCC4:
  case eWINX:
  case eICC:
    break;
  case eWIN:
    mMsgContext->CGBadPlatform ("win", "Linux");
    break;
  case eSPW:
    mMsgContext->CGBadPlatform ("spw", "Linux");
    break;
  default:
    // This is not good... the compiler never got assigned...
    mMsgContext->CGBadPlatform ("unknown", "Linux");
    return false;
    break;
  }
#endif

#if pfLINUX64
  switch (mTargetCompiler) {
  case eGCC3:
  case eGCC4:
    break;
  case eWINX:
    mMsgContext->CGBadPlatform ("winx", "Linux64");
    break;
  case eWIN:
    mMsgContext->CGBadPlatform ("win", "Linux64");
    break;
  case eICC:
    mMsgContext->CGBadPlatform ("icc", "Linux64");
    break;
  case eSPW:
    mMsgContext->CGBadPlatform ("spw", "Linux64");
    break;
  default:
    // This is not good... the compiler never got assigned...
    mMsgContext->CGBadPlatform ("unknown", "Linux64");
    return false;
    break;
  }
#endif

  return ret;
}

bool CodeGen::checkArgs()
{
  bool ret = true;
  
  ArgProc* arghdl = mCarbonContext->getArgs();

  mDoTestDriver =
    arghdl->getBoolValue(scTestdriver)            ||
    arghdl->getBoolValue(scTestdriverDebugOutput) ||
    arghdl->getBoolValue(scTestdriverDumpVCD)     ||
    arghdl->getBoolValue(scTestdriverDumpFSDB)    ||
    arghdl->getBoolValue(scTestdriverSkipObservables);
  if (arghdl->getBoolValue(scTestdriverNoLink))
  {
    // Most likely someone wants to compile cmodels and link in the testdriver later.
    mLinkTestDriver = false;
    mDoTestDriver = true;
    mCompileTestDriver = true;
  }

  // infer the cbuild target
  checkTargetArgs ();

  // at one time there was an idea:
  //     If we are not generating the legacy structure API, we might as
  //     well write test testdriver file and not compile it.  It can be
  //     useful as a template for AEs to build clock/reset testbenches
  // this idea was rejected SEE BUG 6006 - MS
  if (!mDoTestDriver) {
    mLinkTestDriver = false;
    mCompileTestDriver = false;
  }

  mIODB->putTargetCompiler(mTargetCompiler);
  mIODB->putTargetCompilerVersion(mTargetCompilerVersion);

  // Add the -Wc flags to the iodb.
  // Look for extra flags to pass (there could be multiple instances!)
  UtString cxx_Wc_flags;
  
  ArgProc::StrIter Wc_flags;
  
  if (ArgProc::eKnown == arghdl->getStrIter (scWc, &Wc_flags))
    while (! Wc_flags.atEnd ())
      {
	cxx_Wc_flags += *Wc_flags;
	cxx_Wc_flags += " ";
	++Wc_flags;
      }

  if (arghdl->getBoolValue (CRYPT ("-nofold")) && 
    (mTargetCompiler == eGCC3 || mTargetCompiler == eWINX ||
     mTargetCompiler == eWIN || mTargetCompiler == eGCC4)) {
    // Specifying -nofold for any gcc-based target should turn off -Werror, as we can't
    // guarantee to not produce code that has warnings if we can't fold things.
    cxx_Wc_flags += CRYPT ("-Wno-error ");
  }

#ifdef CDB
  // If cbuild is compiled for debug, turn on runtime debug.
  // Currently this turns on assertions for dirty data in carbon_priv.h.
  // For internal use only.
  cxx_Wc_flags += CRYPT( "-DCARBON_RUNTIME_DEBUG " );
#endif

  mIODB->putTargetFlags (cxx_Wc_flags);

  determineGenerating64BitModel(arghdl);

  // Check for PCH suppression
  
  if (arghdl->getBoolValue (CRYPT ("-noPCH")))
    mPrecompiledHeaders = false;

  // Check for SpeedCC suppression

  mIsNoSpeedCC = arghdl->getBoolValue (scNoSpeedCC);

  if (mIsNoSpeedCC) {
    // Turn off precompiled headers when not using SpeedCC... but only because
    // I haven't untangled the makefiles enough to work out how to do this.
    mPrecompiledHeaders = false;
  }

  if (mIsNoSpeedCC && !arghdl->getBoolValue (scNoEncrypt)) {
    // Compilation without SpeedCC is just not possible if the generated model
    // is encrypted. An intentional side-effect of this test is that -noSpeedCC
    // will require the same license as required by -noEncrypt.
    mMsgContext->CGNoSpeedCCRequiresNoEncrypt (scNoSpeedCC, scNoEncrypt);
  }

  // Get the limit on schedule function size before splitting
  ArgProc::IntIter schedLimit;
  for (arghdl->getIntIter (scSchedFileLimit, &schedLimit);
       !schedLimit.atEnd ();
       ++ schedLimit)
    mFunLimit = *schedLimit;


  // What level of optimization?
  ArgProc::StrIter optimize;

  // In case there were MULTIPLE -On flags, just look at last one
  for (arghdl->getStrIter ("-O", &optimize); !optimize.atEnd(); ++optimize)
  {
    UtString level (*optimize);
    if (level.size () != 1
        || !(level[0] == 's' || (level[0] >= '0' && level[0] <= '3'))) {
      // invalid optimization level
      mMsgContext->CGOptLevel (level.c_str ());
      ret = false;
    }
    else
      mOptLevel = level[0];
  }

  mDebuggableModel = arghdl->getBoolValue ("-debug");

  // Initialize the memory capacity. It's not set for real until we
  // analyze the design.
  mSparseMemoryBitCapacity = 0;

  // Set the builtin_expect...
  arghdl->getIntFirst (CRYPT ("-expect"), &mBuiltinExpectValue);

  // Inline limits
  ArgProc::IntIter inlineLimit;
  for (arghdl->getIntIter (scInlineInstrAlways, &inlineLimit);
       !inlineLimit.atEnd ();
       ++ inlineLimit)
    mInlineAlwaysInstrLimit = *inlineLimit;

  for (arghdl->getIntIter (scInlineInstrTotal, &inlineLimit);
       !inlineLimit.atEnd ();
       ++ inlineLimit)
    mInlineTotalLimit = *inlineLimit;

  mDoInline = arghdl->getBoolValue (scInline);
  mDoCheckpoint = arghdl->getBoolValue(scCheckpoint);
  mNoOOB = arghdl->getBoolValue ("-no-OOB");
  mCheckOOB = arghdl->getBoolValue ("-checkOOB");

  if (mNoOOB && mCheckOOB)
    // Can't check for OOB violations if we disable OOB checking!
    mMsgContext->ArgsConflict ("-no-OOB", "-checkOOB");

  mSanitizeCheck = arghdl->getBoolValue (CRYPT ("-sanitizeCheck"));

  mIsObfuscating = ! arghdl->getBoolValue (scNoEncrypt)
    && ! arghdl->getBoolValue("-generateCleartextSource");

  mShareConstants = arghdl->getBoolValue (CRYPT("-shareConstants"));
  mCodegenOffsetSanity = arghdl->getBoolValue (scCodegenOffsetSanity);
  mVerboseDeadCode = arghdl->getBoolValue (scVerboseDeadCode);
  mEmbedIODB = arghdl->getBoolValue(scEmbedIODB);
  mOnDemand = arghdl->getBoolValue("-onDemand");

  // Look for an induced error specification file
  ArgProc::StrIter induced_fault_iter;
  if (arghdl->getStrIter (scInduceCodegenFaults, &induced_fault_iter) == ArgProc::eKnown) {
    mInducedFaults = new InducedFaultTable (mMsgContext);
  }
  while (!induced_fault_iter.atEnd ()) {
    if (!mInducedFaults->read (*induced_fault_iter)) {
      mMsgContext->CGBadInducedFaultFile (*induced_fault_iter);
    }
    ++induced_fault_iter;
  }

  // Check for code annotation options
  if (arghdl->getBoolValue (scNoAnnotationStore)) {
    mHdlAnnotations.set (CodeAnnotation::cNO_STORE);
    mImplAnnotations.set (CodeAnnotation::cNO_STORE);
  }
  if (mCarbonContext->isBackendCompileOnly ()) {
    // Prevent writing the annotation store when just doing a backend
    // compile... this would overwrite an existing store with an empty
    // database.
    mHdlAnnotations.set (CodeAnnotation::cNO_STORE);
    mImplAnnotations.set (CodeAnnotation::cNO_STORE);
  }
  if (arghdl->getBoolValue (scShowHdlAnnotations)) {
    mHdlAnnotations.set (CodeAnnotation::cVISIBLE);
  }
  if (arghdl->getBoolValue (scShowImplAnnotations)) {
    mImplAnnotations.set (CodeAnnotation::cVISIBLE);
  }
  if (arghdl->getBoolValue (scCompressAnnotationStore)) {
    mImplAnnotations.set (CodeAnnotation::cCOMPRESS_STORE);
    mHdlAnnotations.set (CodeAnnotation::cCOMPRESS_STORE);
  }

  
  if (annotateGeneratedCode () ){
    bool ok_to_annotate = true;
    const char* linelim = getenv("CARBON_LINE_LIMIT");
    UtString errmsg;

    if (linelim == NULL) {
      ok_to_annotate = false;
      errmsg << "undefined";
    } else {
      char* endptr;
      UtString conversionErrorMsg;
      UInt32 line_limit = OSStrToU32(linelim, &endptr, 10, &conversionErrorMsg);
      if (*endptr != '\0') {
        ok_to_annotate = false;
        errmsg << "defined but has an invalid value, " << conversionErrorMsg;
      } else if (0 != line_limit) {
        errmsg << line_limit;
        ok_to_annotate = false;
      }
    }
    if (!ok_to_annotate) {
      mMsgContext->CGannotateCodeNeedsCARBON_LINE_LIMIT_setToZero (errmsg.c_str ());
    }
  }
  // Open the code annotation store
  UtString hdl_errmsg;
  if (!annotateGeneratedCode () || mHdlAnnotations.test (CodeAnnotation::cNO_STORE)) {
    // do not create an annotation database
  } else if (mHdlAnnotations.openStore (mCarbonContext->getFileRoot (), 0, &hdl_errmsg)) {
    mMsgContext->CGCannotOpenAnnotationStore (hdl_errmsg.c_str ());
    ret = false;
  }
  UtString impl_errmsg;
  if (!annotateGeneratedCode () || mImplAnnotations.test (CodeAnnotation::cNO_STORE)) {
    // do not create an annotation database
  } else if (mImplAnnotations.openStore (mCarbonContext->getFileRoot (), 0, &impl_errmsg)) {
    mMsgContext->CGCannotOpenAnnotationStore (impl_errmsg.c_str ());
    ret = false;
  }
  return ret;
}

void CodeGen::printBackendTarget () const
{
  switch (mTargetCompiler) {
#define CASE(_x_, _name_) case _x_: UtIO::cout () << _name_ << UtIO::endl; break
  CASE (eICC, "ICC");
  CASE (eSPW, "SPW");
  CASE (eWINX, "WINX");
  CASE (eWIN, "WIN");
  case eGCC3:
    switch (mTargetCompilerVersion) {
    CASE (eGCC_343, "GCC343");
    CASE (eGCC_345, "GCC345");
    CASE (eGCC_346, "GCC346");
    default:
      INFO_ASSERT (0, "Unknown GCC3 compiler version");
    };
    break;
  case eGCC4:
    switch (mTargetCompilerVersion) {
    CASE (eGCC_422, "GCC422");
    CASE (eGCC_424, "GCC424");
    CASE (eGCC_432, "GCC432");
    default:
      INFO_ASSERT (0, "Unknown GCC4 compiler version");
    };
    break;
  default:	
    INFO_ASSERT (0, "unknown target compiler");
    break;
#undef CASE
  }
}

//!Translate Nucleus IL into C++ code

void CodeGen::generateCode (SCHSchedule *sched, NUDesign *nuctree,
                            Stats* stats,
			    bool phaseStats)
{
  mTree = nuctree;
  mSched = sched;
  mCGOFiles = new CGOFiles (*this, mInducedFaults, mMsgContext);
  mGenSchedFactory = new GenSchedFactory(mAtomicCache);

  ArgProc* arghdl = mCarbonContext->getArgs ();

  // Track the blocks that are actually scheduled at execution time.
  // We mark each always, initial, continuous assign, enable-driver or task
  // as reached.  When we get to emitcode, we will discard anything NOT marked
  // as scheduled and issue an INFO diagnostic
  //
  visitSchedule ();

  // Run initialization code over the modules. Today this only
  // includes creating temp nets for NUChangeDetect operators. More
  // stuff should be added here.
  {
    CGModuleWalk callback(mCarbonContext->getSymbolTable());
    NUDesignWalker walker(callback, false, false);
    walker.putWalkTasksFromTaskEnables(false);
    walker.design(nuctree);
  }

  // Locate the hierarchical references which appear as port connections.
  {
    CGFindHierRefPorts callback(this);
    NUDesignWalker walker(callback, false);
    walker.putWalkTasksFromTaskEnables(false);
    walker.design(nuctree);
  }

  // One last computation of the read/write flags.
  {
    MarkReadWriteNets marker (mIODB, arghdl, true);
    marker.design (nuctree);
  }

  // Determine which memories will be coded with a sparse representation.
  {
    SInt32 user_memory_capacity=0;
    arghdl->getIntLast(scMemoryCapacity,&user_memory_capacity);
    MemoryAnalyzer memory_analyzer(user_memory_capacity,
                                   arghdl->getBoolValue(scVerboseMemoryAnalyzer));
    memory_analyzer.design(nuctree);
    mSparseMemoryBitCapacity = memory_analyzer.getMemoryBitCapacity();
  }

  // Get the size for schedule spill files.
  ArgProc::IntIter schedSpillLimit;
  for (arghdl->getIntIter (scSchedFileSpillLimit, &schedSpillLimit);
       !schedSpillLimit.atEnd ();
       ++ schedSpillLimit) {
    getCGOFiles()->mScheduleSpillLimit = (*schedSpillLimit);
  }

  // Prime the schedule with some overhead.
  getCGOFiles ()->mScheduleSpillSize = getCGOFiles ()->mScheduleSpillLimit/4;

  // Start a new sub-interval timer for codegen
  if (phaseStats)
    stats->pushIntervalTimer();

  mInternalCodeNames = new CodeNames(mIsObfuscating, mAtomicCache);
  mInterfaceCodeNames = new CodeNames(false, mAtomicCache);
  mCodeNames = mInternalCodeNames;


  // One last computation of the read/write flags.  Folding may have redone UD on a module
  // which will have mucked the r/w flags.
  {
    MarkReadWriteNets marker (mIODB, arghdl, true);
    marker.design (nuctree);
  }

  // Quick sanity check...

  NUModuleList modules;
  mTree->getTopLevelModules(&modules);
  if (modules.size() > 1)
    {
      mMsgContext->CGNotImpl ("multiple top level classes");
      if (phaseStats)
	stats->popIntervalTimer();
      return;
    }

  // Dump pre-codegen verilog
  

  // Determine which modules call system tasks & function
  determineSystemCalls(nuctree);

  // Identify the top-level module name

  NUModule *inst = *modules.begin ();
  putTopModule (inst);
  NUModuleElab *elab = inst->lookupElab (mCarbonContext->getSymbolTable ());

  // Save it away after translating any reserved word or illegal C++ character
  // into a valid C++ identifier name.  Also append an underscore so that it won't
  // conflict with any local variable we might have declared in some schedule function
  //
  UtString topName (elab->getModule ()->getName ()->str ());
  topName += "_";
  const char* pstr = topName.c_str ();
  mTopName = mCodeNames->translate (mAtomicCache->intern (pstr, std::strlen (pstr)), true);

  // Now that mTopName is initialized (so getUID() will work) (and
  // CarbonContext::mFileRoot is initialized), we can open the
  // compile-time database.
  mCGProfile->setFile(mCarbonContext->getFileRoot());

  // Manipulate the file system so that our .cxx, .h, .o, and .a files are
  // created in a safe place.
  //
  setupBuildDirectory ();

  // Put these things into play.

  getCGOFiles()->mScheduleSrc = CGFileOpen ("./schedule.cxx", 
    "/* Instantiate, connect, schedule. */\n",
    isObfuscating());

  if (getCGOFiles()->mScheduleSrc == NULL)
    // error opening file (already reported in CGFileOpen)
    return;

  switchStream(eCGSchedule);

  /* Basic implementation:
     i)	 Setup global context (directory to write .cxx/.h files in, etc...)
     ii) Write the class.cxx and class.h files
     iii)Write the simulation class instantiate, connect and cycle schedule
     iv) Write the debug symbol table
     v)  Close all the files
     vi) Compile and link the C++, check for build errors and report them.
     vii)Cleanup global context
  */

  // Display error if cbuild generated code for one environment and it's
  // being compiled with gcc for another environment.
  if (is64bit())
    // Generate these lines:
    // #if !__LP64__
    //   #error "cbuild generated 64-bit code, but it's being compiled as 32-bit (-m32)"
    // #endif
    CGOUT() << CRYPT("#if !__LP64__\n  #error \"cbuild generated 64-bit code, but it's being compiled as 32-bit (-m32)\"\n#endif\n");
  else
    // Generate these lines:
    // #if __LP64__
    //   #error "cbuild generated 32-bit code, but it's being compiled as 64-bit (-m64)"
    // #endif
    CGOUT() << CRYPT("#if __LP64__\n  #error \"cbuild generated 32-bit code, but it's being compiled as 64-bit (-m64)\"\n#endif\n");

  // Make sure all clocks are marked as referenced.
  markClocks ();

  // Now that sparse memories have been sorted out, we can prune the
  // set of onDemand nets.
  if (isOnDemand()) {
    emitUtil::pruneOnDemandNets();
  }

  UInt32 localityTechnique=0;
  {
    ArgProc::IntIter i;
    for (arghdl->getIntIter (scLocality, &i); !i.atEnd(); ++i) {
      localityTechnique = (*i);
    }
  }
  bool localityVerbosity = arghdl->getBoolValue(scVerboseLocality);
  mRankNets = new RankNets(localityTechnique, isOnDemand(), localityVerbosity);
  mRankNets->computeRank(mSched);

  // Save any headers which have been calculated so far to the top module.
  getCGOFiles()->saveExtraHeaders(*(nuctree->loopTopLevelModules()));

  // Compute design costing (needed by emits).
  mCosts->calcDesign(nuctree);

  // We will reuse the cost context, so make sure the instance counts don't get increased
  mCosts->putUpdateInstances(false);

  // Create table names for luts
  sCreateTableNames(nuctree);

  // Emit function layout in schedule order.
  if (arghdl->getBoolValue(scFunctionLayout)) {
    ArgProc::IntIter funcCostLimit;
    UInt32 limit = 0;
    for (arghdl->getIntIter (scFunctionCostLimit, &funcCostLimit);
         !funcCostLimit.atEnd (); ++funcCostLimit)
      limit = *funcCostLimit;

    mLayout = new FuncLayout (mCosts, mIODB, mCGOFiles, this, limit);
    mLayout->layoutFunctions(mSched);
  }

  // Walk the design to code the class .h and .cxx files.  This writes out
  // a <module>.h and <module>.cxx file for each instantiated verilog module.
  //
  nuctree->emitCode (eCGDefine | eCGDeclare);

  delete mRankNets;
  mRankNets = 0;

  // Now emit the instantiation and  connection code.
  //
  codeInitialization ();

  // Generate the code to create a single replay mode change function
  // for all c-models. It is a function that calls the individual
  // c-model mode change functions.
  codeModeChange();

  // The save restore C functions
  codeSaveRestore ();

  // As well as the top-level scheduling functions
  codeSchedule ();

  // And emit the interface header file.
  codeInterface ();
  
  // Lastly emit the 'schedule.h' file
  codeScheduleHeader ();
  
  // And dump the schedule constants into the file too
  dumpConstantPool (*(getCGOFiles ()->mScheduleSrc), eCGDefine | eCGSchedule);


  // Check for -testdriver flag
  if (mDoTestDriver)
  {
    UInt32 vector_count=0;
    UInt32 reset_length=5;
    SInt32 vector_seed=0;
    UInt32 clock_delay=0;
    ArgProc::IntIter i;

    const char *vectorfile = NULL;
    if (arghdl->getStrValue (scTestdriverVectorFile, &vectorfile) == ArgProc::eKnown) {
      // the test vector file was specified on the command line
    }

    for (arghdl->getIntIter (scTestdriverVectorCount, &i); !i.atEnd(); ++i)
    {
      vector_count = *i;
    }

    for (arghdl->getIntIter (scTestdriverVectorSeed, &i); !i.atEnd(); ++i)
    {
      vector_seed = *i;
    }

    for (arghdl->getIntIter (scTestdriverResetLength, &i); !i.atEnd(); ++i)
    {
      reset_length = *i;
    }

    for (arghdl->getIntIter (scTestdriverClockDelay, &i); !i.atEnd(); ++i)
    {
      clock_delay = *i;
    }
    // Argument of -testdriverClockDelay can be between 0 and 100 inclusive.
    if (clock_delay > 100)
    {
        mMsgContext->CGIllegalClockDelayValue();
        clock_delay = 0;
    }

    bool dumpVCD = arghdl->getBoolValue(scTestdriverDumpVCD);
    bool dumpFSDB = arghdl->getBoolValue(scTestdriverDumpFSDB);
    // Both FSDB and VCD cannot be dumped from test driver at the same time.
    // Warn user and choose FSDB dump by default.
    if (dumpVCD && dumpFSDB)
    {
      mMsgContext->CGCannotDumpVCDAndFSDB();
      dumpVCD = false;
    }

    if (! codeTestDriver (vector_count, vector_seed, reset_length, clock_delay,
                          vectorfile,
                          arghdl->getBoolValue(scTestdriverDebugOutput),
                          dumpVCD, dumpFSDB,
                          arghdl->getBoolValue(scTestdriverUseIODB),
                          arghdl->getBoolValue(scTestdriverGlitchDetect),
                          arghdl->getBoolValue(scTestdriverCheckpoint),
                          arghdl->getBoolValue(scTestdriverSkipObservables)))
      mDoTestDriver = false;
  }

  // Calculate correct offsets for each symbol -- do this independently
  // of whether -g is specified, so that the user can still access
  // io signals by name.
  getCGOFiles()->saveExtraHeaders (getTopModule ());
  getCGOFiles()->restoreExtraHeaders (getTopModule ());

  decorateSymbolTable ();

  // Generate the dictionary (a stub function is produced if we aren't
  // compiling -g or -testdriver
  //
  compileDictionary ();

  getCGOFiles()->flushExtraHeaders (getTopModule ());

  // Destroying the std::ostream's closes the files
  delete (getCGOFiles()->mScheduleSrc);
  delete (getCGOFiles()->mScheduleSpill);
  
  // Generate C-Model wrapper for the design
  if (mCarbonContext->doGenerateCModelWrapper())
  {
    codeCModelWrapper(nuctree);
  }

  chdirToOrigDir ();

  // close both the annotation stores
  mHdlAnnotations.closeStore ();
  mImplAnnotations.closeStore ();

  // At this point we can print the write portion of the phase stats
  if (phaseStats)
  {
    stats->printIntervalStatistics("CG Write");
    // Done with this interval timer
    stats->popIntervalTimer();
  }

}

// Mark all clocks as referenced, so we will emit them all.  Added because
// depositable clocks which are otherwise undriven will not show up in
// a Nucleus walk (codegen doesn't walk the edge expressions), but doesn't
// hurt to just mark all clocks.
void CodeGen::markClocks ()
{
  for (SCHClocksLoop c = mSched->loopClocks(); !c.atEnd();
       ++c) {
    (*c)->getStorageNet()->emitCode(eCGMarkNets);
  }
}

extern const char* gCarbonVersion ();

void CodeGen::getUID (UtString *s)
{
  if (mDesignUID.empty())
  {
    mDesignUID << getTopName ()->str ();
    mDesignUID << " ";

    mDesignUID << gCarbonVersion ();
    mDesignUID << " ";
    UtString timeStr;
    mDesignUID << OSGetTimeStr("%b %d, %Y  %H:%M:%S", &timeStr);
  }
  *s = mDesignUID;
}

// Construct the C++ syntaxx path for a hierarchical name. We
// must worry about reserved words and non-printable characters
// so using the STBranchNode::compose() method isn't useable.
//
void
CodeGen::outputHierPath (UtOStream &out, const STBranchNode *parent,
                         bool appendSep, const char *sep)
{
  UtString path;
  out << CxxHierPath(&path, parent, sep);
  if (appendSep) {
    if (isNamedBlock(parent)) {
      out << '$';
    } else {
      out << '.';
    }
  }
}

bool
CodeGen::isNamedBlock (const STBranchNode *node) 
{
  const NUBase* base = CbuildSymTabBOM::getNucleusObj(node);
  const NUElabBase* elab = dynamic_cast<const NUElabBase*>(base);
  const NUScope* scope = NULL;
  if (elab) {
    const NUScopeElab *eScope = dynamic_cast<const NUScopeElab*>(base);
    if (eScope){
      scope = eScope->getScope();
    }
  }
  else {
    scope = dynamic_cast<const NUScope*>(base);
  }
  if (scope)
    return (scope->getScopeType() == NUScope::eNamedDeclarationScope);
  else
    return false;
}

UtString&
CodeGen::CxxHierPath(UtString *path, const STBranchNode *parent,
                     const char *sep, const STBranchNode* limit,
                     bool includeTop, bool makeMembers)
{
  // Start with <top>.
  if (includeTop)
    *path += gCodeGen->getTopName()->str ();

  // If we have nested hierarchy, then our parent is non-null,
  // and we must generate a full path..
  if (parent) {
    UtVector<const STSymbolTableNode*> tmpVec;

    // Build a vector of the hierarchical components in reverse
    // order.
    for(const STSymbolTableNode* name = parent; name; name = name->getParent ()) {
      tmpVec.push_back (name);
      if (name == limit)
	break;
    }

    // Remove top of design - we print it specially
    tmpVec.pop_back ();

    bool first = true;
    bool parentIsNamedBlock = false;
    for(UtVector<const STSymbolTableNode*>::reverse_iterator i=tmpVec.rbegin();
	i != tmpVec.rend ();
	++i) {

      // if this is not the first element of the path, add a separator
      if (includeTop || !first) {
        if (parentIsNamedBlock) {
          *path << "$";
        } else {
          *path << sep;
        }
      }
      first = false;

      if (makeMembers) {
        parentIsNamedBlock = false;
        // treat each component as a member
	if (isNamedBlock((*i)->castBranch())){
	  *path << emitUtil::block((*i)->strObject ());
          parentIsNamedBlock = true;
	}
	else {
	  *path << emitUtil::member((*i)->strObject ());
	}
      } else
        // just add the (sanitized) component name
        *path << emitUtil::noprefix((*i)->strObject ());
    }
  }

  return *path;
}

bool CodeGen::isIncludedInTypeDictionary(const STAliasedLeafNode* leaf,
                                         IODB* iodb)
{
  bool ret = false;
  if (iodb->hasConstantOverride(leaf))
    ret = true;
  else {
    const NUNet* net = NUNet::find(leaf);
    if (not net->isHierRef() and not net->isVirtualNet()) {
      CGAuxInfo* cgop = net->getCGOp();
      NU_ASSERT(cgop, net);
      if (! cgop->isUnreferenced())
        ret = true;
      else if (net->isRead())
        ret = true;
      else if (net->isWritten())
        ret = true;
      else if (net->isTriWritten())
        ret = true;
      //            net->isAliased() ||
      else if (!net->isTemp())
        ret = true;

      // This is a per node check not a per ring check so don't look at
      // the master or the storage, just the leaf that was passed in.
      else if (gCodeGen->getCarbonContext()->getIODB()->isInIOTable(leaf))
        ret = true;
    }
  } // else

  return ret;
} // bool CodeGen::isIncludedInTypeDictionary

void CodeGen::incrCModelCallName(void)
{
  ++mCModelCallIndex;
  mCModelCallName->clear();
  (*mCModelCallName) << mCModelCallIndex;
}

CGAuxInfo* NUNet::getCGOp()
  const
{
  return gCodeGen->getCGOp(this);
}

void NUNet::setCGOp(CGAuxInfo* cgOp)
  const
{
  gCodeGen->putCGOp(this, cgOp);
}

//! Get the cgaux info associated with a net
CGAuxInfo* CodeGen::getCGOp(const NUNet* net)
{
  NetToCGAuxMap::iterator p = mNetToCGAuxMap->find(net);
  if (p == mNetToCGAuxMap->end())
    return NULL;
  return p->second;
}

//! Put the cgaux info associated with a net
void CodeGen::putCGOp(const NUNet* net, CGAuxInfo* cgOp)
{
  (*mNetToCGAuxMap)[net] = cgOp;
}

CGAuxInfo* NUNamedDeclarationScope::getCGOp() const
{
  return gCodeGen->getCGOp(this);
}

void NUNamedDeclarationScope::setCGOp(CGAuxInfo* cgOp) const
{
  gCodeGen->putCGOp(this, cgOp);
}

//! Get the cgaux info associated with a scope
CGAuxInfo* CodeGen::getCGOp(const NUNamedDeclarationScope* block)
{
  BlockToCGAuxMap::iterator p = mBlockToCGAuxMap->find(block);
  if (p == mBlockToCGAuxMap->end())
    return NULL;
  return p->second;
}

//! Put the cgaux info associated with a scope
void CodeGen::putCGOp(const NUNamedDeclarationScope* block, CGAuxInfo* cgOp)
{
  (*mBlockToCGAuxMap)[block] = cgOp;
}

void CodeGen::addToEmittedSet(const NUUseDefNode *node)
{
  NU_ASSERT(node->isContDriver(), node);
  mEmittedFuncs.insert(node);
}

bool CodeGen::inEmittedSet(const NUUseDefNode *node) const
{
  NU_ASSERT(node->isContDriver(), node);
  return (mEmittedFuncs.find(node) != mEmittedFuncs.end());
}

//! Construct list of modules which are needed by non-local task hierrefs appearing in the given node.
void CodeGen::generateTaskHierRefs(const NUUseDefNode *node,
                                          NUTaskEnableVector &hierTaskEnables)
{
  // Gather the hierarchical task enables occuring in the given node
  NUTaskEnableCallback callback(&hierTaskEnables);
  NUDesignWalker walker(callback, false);
  switch (node->getType()) {
  // Walk structured proc types (always & initial blocks)
  case eNUAlwaysBlock:    walker.alwaysBlock(dynamic_cast<NUAlwaysBlock*>(const_cast<NUUseDefNode*>(node))); break;
  case eNUInitialBlock:   walker.initialBlock(dynamic_cast<NUInitialBlock*>(const_cast<NUUseDefNode*>(node))); break;
  case eNUTask:  walker.task (dynamic_cast<NUTask*>(const_cast<NUUseDefNode*>(node))); break;
  // Walk modules.
  case eNUModule:  walker.module(dynamic_cast<NUModule*>(const_cast<NUUseDefNode*>(node))); break;

  // Simple continuous drivers cannot have tasks
  case eNUContAssign:
  case eNUContEnabledDriver:
  case eNUBlockingEnabledDriver:
  case eNUTriRegInit:
    break;

  // We should only see modules and schedulable continuous driver types here.
  default:
    NU_ASSERT("modules or schedulable continuous driver expected" == 0, node);
    break;
  }
}

void
CodeGen::generateTaskHeaders (const NUTaskEnableVector& hierTaskEnables)
{
  // Walk the task hierrefs, compute the needed include files.
  NUModuleSet covered;
  NUModuleList module_list;
  for (NUTaskEnableCLoop loop(hierTaskEnables); !loop.atEnd(); ++loop)
  {
    // Check if this is not locally relative. Those are the only
    // hier refs that need fixups.
    NUTaskEnable* enable = *loop;
    if (!enable->getHierRef()->isLocallyRelative())
    {
      // Prune out duplicate modules. We could have many hier refs to
      // the same module with either the same or different
      // instances. But we only need one #include.
      NUTaskHierRef* taskRef = enable->getHierRef();
      for (NUTaskHierRef::TaskVectorLoop l = taskRef->loopResolutions();
           !l.atEnd(); ++l)
      {
        NUTask* task = *l;
        NUModule* module = task->findParentModule();
        if (covered.find(module) == covered.end())
        {
          module_list.push_back(module);
          covered.insert(module);
        }
      }
    }
  }

  getCGOFiles()->generateHeaderIncludes (getCGOFiles()->CGOUT (), module_list);
}

#ifdef INTERPRETER
void CodeGen::putModuleOffset(STBranchNode* branch, UInt32 offset) {
  if (mModuleOffsetMap == NULL)
    mModuleOffsetMap = new ModuleOffsetMap;
  (*mModuleOffsetMap)[branch] = offset;
}

UInt32 CodeGen::getModuleOffset(STBranchNode* branch) const {
  ST_ASSERT(mModuleOffsetMap != NULL, branch);
  return (*mModuleOffsetMap)[branch];
}
#endif


void CodeGen::emitSection(UtOStream &out, const UtString &section)
{
  out << CRYPT(" CARBON_SECTION(\"")
      << section
      << CRYPT("\")");
}
bool CodeGen::isGeneratePortInterface() const
{
  ArgProc* args = mCarbonContext->getArgs ();
  return ( mCarbonContext->doGenerateSystemCWrapper() || args->getBoolValue(scCreatePortInterface) ||
           args->getBoolValue(CRYPT("-pliWrapper")) );
}

//! Are we passing build errors straight to standard error
bool CodeGen::isPassingBuildErrors () const
{
  return mCarbonContext->getArgs()->getBoolValue(scPassBuildErrors);
}

//! Should we annotate the generated code
bool CodeGen::annotateGeneratedCode () const
{
  return mCarbonContext->getArgs()->getBoolValue(scAnnotateCode);
}

//! Should we use the (rather slow) indent engine
bool CodeGen::useIndentEngine () const
{
  return mCarbonContext->getArgs()->getBoolValue(scUseIndentEngine);
}

bool CodeGen::isProfileGenerate() const
{
  return mCarbonContext->getArgs()->getBoolValue(scProfileGenerate);
}

bool CodeGen::isProfileUse() const
{
  return mCarbonContext->getArgs()->getBoolValue(scProfileUse);
}

void CodeGen::rememberPortAliasedHierRef(NUNet * hier_ref_net)
{
  mPortAliasedHierRefs->insert(hier_ref_net);
}


bool CodeGen::isLocallyRelativeHierRef(const NUNet * hier_ref_net,
                                       NUModuleInstanceVector * instance_path,
                                       NUNet ** resolved_net) const
{
  if (not hier_ref_net->isHierRef()) {
    return false;
  }

  bool port_aliased_hier_ref = (mPortAliasedHierRefs->find(const_cast<NUNet*>(hier_ref_net))
                                !=mPortAliasedHierRefs->end());
  if (port_aliased_hier_ref) {
    return false;
  }

  const NUNetHierRef * hier_ref = hier_ref_net->getHierRef();
  return hier_ref->isLocallyRelative(instance_path, resolved_net);
}


CodeGen::CxxBoundName CodeGen::CxxName::operator()(const STBranchNode* parent)
  const
{
  return CxxBoundName (*this, parent->strObject (), NULL);
}

UInt32 CodeGen::getLayoutOrder (const NUUseDefNode* node) const
{
  if (not mLayout)
    return 0;                   // no function layout ordering

  // If it's been laid-out, return the relative order...
  return mLayout->haveSeen (node);
}

bool CodeGen::sIsReferenceParameter(const NUNet* net)
{
  return (net->getBitSize() > LLONG_BIT);
}

void CodeGen::addTempNet(const NUTempMemoryNet* tmem)
{
  // It should not exist, otherwise we have a bug
  NU_ASSERT(mUndeclaredTemps->find(tmem) == mUndeclaredTemps->end(),
            tmem->getMaster());
  mUndeclaredTemps->insert(tmem);
}

bool CodeGen::isUndeclaredTempNet(const NUTempMemoryNet* tmem)
{
  // If we find it tell the caller and remove it. The temp memory is
  // going to be declared.
  UndeclaredTemps::iterator pos = mUndeclaredTemps->find(tmem);
  if (pos != mUndeclaredTemps->end()) {
    mUndeclaredTemps->erase(pos);
    return true;
  } else {
    return false;
  }
}
  
void CodeGen::clearUndeclaredTempNets(void)
{
  mUndeclaredTemps->clear();
}

void CodeGen::putNoCC(bool noCC)
{
  ArgProc* args = mCarbonContext->getArgs (); 
  INFO_ASSERT(args->setBoolValue(scNoCC, noCC) == ArgProc::eKnown, "No compilation option not found in list of known arguments.");
}

bool CodeGen::isSlowClock(const NUNetElab* net) const {
  for (STAliasedLeafNode::AliasLoop p(net->getSymNode()); !p.atEnd(); ++p) {
    STAliasedLeafNode* alias = *p;
    if (mIODB->isSlowClock(alias)) {
      return true;
    }
  }
  return false;
}

bool CodeGen::isFastReset(const NUNetElab* net) const {
  for (STAliasedLeafNode::AliasLoop p(net->getSymNode()); !p.atEnd(); ++p) {
    STAliasedLeafNode* alias = *p;
    if (mIODB->isFastReset(alias)) {
      return true;
    }
  }
  return false;
}


void CodeGen::putEmptyCxx (const NUModule* mod)
{
  // Add to the set of empty modules.
  mEmptyModules.insert (mod);
}

void CodeGen::putNonEmptyCxx(const NUModule* mod)
{
  // Remove it from the set of empty modules
  mEmptyModules.erase (mod);
}

bool CodeGen::isEmptyCxx (const NUModule* mod) const
{
  // True if this module is still in the set.
  return mEmptyModules.find (mod) != mEmptyModules.end ();
}

void
CodeGen::verifyFeedbackExists()
{
  // gcc creates feedback from -fprofile-generate in .gcda files when
  // the code is executed.  Warn user if he compiled with
  // -profileGenerate but forgot to run his model, or didn't correctly
  // link with -fprofile-generate before running his model.
  const char* const feedback_pattern = "*.gcda";
  if (OSDirLoop(mbuildDirectory.c_str(), feedback_pattern).atEnd())
    // No feedback files -- complain.
    mMsgContext->CGFeedbackMissing(feedback_pattern, mbuildDirectory.c_str());
}

/*! A class to gather the NUCModelCall's for every NUCModel
 */
class CModelFindCModelCalls : public NUDesignCallback
{
public:
  //! Constructor
  CModelFindCModelCalls(NUCModelCalls* cmodelCalls) : mCModelCalls(cmodelCalls) {}

  //! Destructor
  ~CModelFindCModelCalls() {}

  //! By default, walk though everything
  Status operator()(Phase /* phase */, NUBase* /* node */) { return eNormal; }

  //! Record all NUCModelCalls
  Status operator()(Phase phase, NUCModelCall* call)
  {
    // We are ignoring the PLI interface c-models for now. Also only
    // look at c-models that have the playback run flag.
    NUCModel* cmodel = call->getCModel();
    if (phase == ePre) {
      const char* context = call->getContext();
      NUCModelCallVector& calls = (*mCModelCalls)[cmodel];
      if (context == NULL) {
        // With PLI tasks we only have one context so it is NULL
        if (calls.empty()) {
          calls.push_back(call);
        }

      } else {
        // We have a context, make sure it isn't a duplicate. This can
        // happen if we flatten two different instances. But because they
        // came from the same population, they have the same signature.
        StringAtom* str = mAtomicCache.intern(context);
        CModelContext cmodelContext(cmodel, str);
        NU_ASSERT(mCovered.find(cmodelContext) == mCovered.end(), call);
        mCovered.insert(cmodelContext);

        // Add to the calls
        calls.push_back(call);
      }
    } // if
      
    // No need to continue into the arguments
    return eSkip;
  }

private:
  //! Storage supplied by the caller
  NUCModelCalls* mCModelCalls;

  //! String cache to make sure that we only record a context once
  AtomicCache mAtomicCache;

  //! Abstraction for a covered set
  typedef std::pair<const NUCModel*, StringAtom*> CModelContext;

  //! Storage to make sure we don't have more than one context per cmodel
  UtSet<CModelContext> mCovered;
}; // class CModelFindCModelCalls : public NUDesignCallback

void CodeGen::findCModelCalls(const NUModule* module)
{
  INFO_ASSERT(mCModelCalls->empty(),
              "Finding c-models without clearing the destination data.");
  CModelFindCModelCalls find(mCModelCalls);
  NUDesignWalker walker(find, false);
  walker.module(const_cast<NUModule*>(module));
}

void CodeGen::freeCModelCalls(void)
{
  mCModelCalls->clear();
}

NUCModelCallVectorLoop CodeGen::loopCModelCalls(const NUCModel* cmodel)
{
  NUCModelCalls::iterator pos = mCModelCalls->find(cmodel);
  NU_ASSERT(pos != mCModelCalls->end(), cmodel);
  NUCModelCallVector& calls = pos->second;
  return NUCModelCallVectorLoop(calls);
}

bool CodeGen::hasCModelCalls(const NUCModel* cmodel)
{
  NUCModelCalls::iterator pos = mCModelCalls->find(cmodel);
  return pos != mCModelCalls->end();
}

/* Callbacks from the libdesign.codegen.errors scanner */

//! Called when a "SpeedCC: ..." message is recognised.
/*virtual*/ void CodeGen::CompilationErrorScanner::SpeedCC (const char *text)
{
  mMsgContext.CGSpeedCCError (text); 
}

//! Called when "...Make...:lineno: *** ..." is recognised
/*virtual*/ void CodeGen::CompilationErrorScanner::MakeError (const char *makefilename, 
  const UInt32 lineno, const char *message) 
{
  // strip the directory part of makefilename
  const char *stripped = makefilename;
  UInt32 n = 0;
  while (makefilename [n] != '\0') {
    if (makefilename [n] == '/') {
      stripped = makefilename + n + 1;
    }
    n++;
  }
  mMsgContext.CGMakeError (stripped, lineno, message);
}


//! Called when "filename:lineno: severity: ..." is recognised
/*\note Collects error location in a list */
/*virtual*/ void CodeGen::CompilationErrorScanner::CxxError (const char *filename, 
  const UInt32 lineno, const char *message UNUSED)
{
  SourceLocator loc = mSlocFactory->create ( filename, lineno );
  mSlocList.push_back ( loc );
}


void CodeGen::determineGenerating64BitModel(ArgProc* args)
{
  // Default is to generate models matching the host architecture
#if pfLINUX64
  m64 = true;
#else
  m64 = false;
#endif

  // Get the requested target architecture.  This is either Linux
  // (i.e. 32-bit Linux), Linux64, or Win (for cross compiling).  If
  // not specified, the default value is Linux.
  const char* targetArchEnv = getenv("CARBON_TARGET_ARCH");
  if (targetArchEnv != NULL) {
    UtString requestedArch = targetArchEnv;
    if ((requestedArch == "Linux") || (requestedArch == "Win")) {
      m64 = false;
    } else if (requestedArch == "Linux64") {
      m64 = true;
    } else {
      mMsgContext->CGBadTargetArch(targetArchEnv);
    }
  }

  // There are also cbuild command lines that can set the model type
  if (args->getBoolValue(scM32) && args->getBoolValue(scM64)) {
    mMsgContext->ArgsConflict(scM32, scM64);
  }
  if (args->getBoolValue(scM64)) {
    m64 = true;
  }
  if (args->getBoolValue(scM32)) {
    m64 = false;
  }
  
#if pfLINUX
  // Bug 16036.  32-bit compiler can't generate 64-bit models
  if (m64) {
    mMsgContext->CGNo64BitModelFrom32BitCompiler();
  }
#endif
}
