// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Collective management for codegen optimizations over schedules.
*/

#include "GenSchedFactory.h"
#include "codegen/codegen.h"
#include "compiler_driver/CarbonContext.h"
#include "util/ArgProc.h"
#include "util/AtomicCache.h"

GenSchedFactory::GenSchedFactory (AtomicCache* ac)
  : mAtomicCache(ac)
{
  mGenSchedules = new ScheduleFunctionMap;
  mGenSchedMap = new GenSchedMap;
}

GenSchedFactory::~GenSchedFactory ()
{
  // Free the GenSched objects
  for(ScheduleFunctionMap::iterator i = mGenSchedules->begin ();
      i != mGenSchedules->end ();
      ++i)
    delete((*i).second);

  delete mGenSchedules;

  delete mGenSchedMap;
}

GenSched*
GenSchedFactory::findEquivalent (GenSched *gs)
{
  // Easy lookup by unique schedule name
  (*mGenSchedules)[gs->getSchedName ()] = gs;

  // Are we optimizing schedules?

  if (gs->getSize () == 0
      || gs->getRoot () == NULL
      || not gCodeGen->getCarbonContext ()->getArgs ()->getBoolValue (CRYPT("-schedPrefix")))
    return gs;

  // There can be multiple schedules with the same prefix
  const GenSchedMap::key_type key = gs->getKey ();
  typedef GenSchedMap::const_iterator I;

  I ub = mGenSchedMap->upper_bound (key);
  for(I i = mGenSchedMap->lower_bound (key); i != ub; ++i)
    {
      GenSched &other = *((*i).second);
      if (gs->getSize () == other.getSize ()
	  && gs->isEquivalent (other))
	// Bingo!
	{
	  gs->setEquivalent (&other);
	  return gs;
	}
    }

  // If we didn't find a match, and it has a prefix, add it to the
  // set of schedules we search.
  mGenSchedMap->insert (std::make_pair (key, gs));

  return gs;
}

GenSched*
GenSchedFactory::generate (const char *funName, SCHCombinational *s,
			   CGOFiles *cgOFiles,
			   int flags,
                           const char *section,
                           bool suppressObfuscate)
{
  GenSched *gs = new GenSched (funName, s, cgOFiles, flags, section,
                               suppressObfuscate);

  return findEquivalent (gs);
}

GenSched*
GenSchedFactory::generate (const char*funName, SCHSequential *s, 
			   CGOFiles *cgOFiles,
			   ClockEdge e,
			   int flags,
                           const char *section)
{
  GenSched *gs = new GenSched (funName, s, cgOFiles, e, flags, section);
  return findEquivalent (gs);
}

GenSched*
GenSchedFactory::generate (const char* funName,
                           NUCycle* cycle,
                           CGOFiles *cgOfiles,
                           const char* section)
{
  GenSched *gs = new GenSched (funName, cycle, cgOfiles, section);
  return findEquivalent (gs);
}

GenSched*
GenSchedFactory::findGenSched (const UtString& funName) const
{
  StringAtom* sym = mAtomicCache->intern(funName);
  return (*mGenSchedules)[sym];
}

/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
