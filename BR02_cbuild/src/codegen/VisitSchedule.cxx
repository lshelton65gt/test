// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Visit the scheduled nodes in the design to identify unscheduled always blocks,
  tasks, and continuous assigns and eliminate them from codegeneration.
*/
#include "codegen/codegen.h"

#include "compiler_driver/CarbonContext.h"

#include "flow/FLNodeElab.h"
#include "nucleus/NUCycle.h"

#include "nucleus/Nucleus.h"
#include "nucleus/NUDesignWalker.h"

#include "nucleus/NUBlock.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTriRegInit.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAssign.h"
#include "schedule/Schedule.h"

// Only need pre-order traversal of subnodes, so just skip over
// post calls.  We will visit and traverse any tasks we encounter
// (because they may call additional tasks or functions that need
// to be marked).
//
#define POSTSKIP(p) return ((p == ePre) ? eNormal : eSkip)

class VisitBlocks : public NUDesignCallback
{
public:
  // Constructor
  VisitBlocks () : NUDesignCallback () {}
  Status operator()(Phase phase, NUBase *) { POSTSKIP (phase); }

  // We have to search always and initial blocks
  Status operator()(Phase phase, NUAlwaysBlock *node) {
    NUBlock* block = node->getBlock ();
    block->putScheduled (true);

    POSTSKIP (phase);
  }

  Status operator()(Phase phase, NUInitialBlock *node) {
    NUBlock* block = node->getBlock ();
    block->putScheduled (true);

    POSTSKIP (phase);
  }

  // We mark any task that we enable as visited
  Status operator()(Phase phase, NUTaskEnable *node) {
    // Find the corresponding task and mark its scheduled flag
    // This TE might be a hier-ref, and have multiple resolutions to mark
    if (NUTaskHierRef* hte = node->getHierRef ()) {
      for (NUTaskHierRef::TaskVectorLoop loop = hte->loopResolutions ();
           not loop.atEnd ();
           ++loop)
        (*loop)->putScheduled (true);
      POSTSKIP (phase);
    }
    
    // Normal vanilla task enable, only one possible task to mark
    NUTask* task = node->getTask ();
    task->putScheduled (true);

    POSTSKIP (phase);
  }

  Status operator()(Phase phase, NUTaskEnableHierRef *node) {
    // Possibly more than one resolution!
    NUTaskHierRef* task = node->getHierRef ();
    for (NUTaskHierRef::TaskVectorLoop loop = task->loopResolutions ();
         not loop.atEnd ();
         ++loop)
      (*loop)->putScheduled (true);

    POSTSKIP (phase);
  }

private:
  // Suppressed 
  VisitBlocks (const VisitBlocks&);
  VisitBlocks& operator=(const VisitBlocks&);
};



// For a flow node, if it's a block-scope, mark it and then visit it
// to locate task calls.

static void sMarkFlow (NUDesignWalker &walker, FLNodeElab* f)
{
  NUDesignCallback::Status status = NUDesignCallback::eNormal;

  if (f->isEncapsulatedCycle ()) {
    // A Cycle encapsulates other flow; visit everything it executes to see
    // if it invokes continuous assigns or task/functions also
    NUCycle *cycle = f->getCycle ();
    cycle->putScheduled (true);

    for (NUCycle::FlowLoop l = cycle->loopFlows (); !l.atEnd (); ++l) {
      FLNodeElab* flow = *l;
      sMarkFlow (walker, flow);
    }
    return;
  }

  NUUseDefNode *codeBlock = f->getUseDefNode ();

  switch (codeBlock->getType ()) {

  case eNUContAssign:
  {
    // assign lval = rval;
    NUContAssign* ca = dynamic_cast<NUContAssign*>(codeBlock);
    ca->putScheduled (true);
    break;
  }

  case eNUContEnabledDriver:
  {
    NUContEnabledDriver* ce = dynamic_cast<NUContEnabledDriver*>(codeBlock);
    ce->putScheduled (true);
    break;
  }
  
  case eNUInitialBlock:
    status = walker.initialBlock (dynamic_cast<NUInitialBlock*>(codeBlock));
    break;
                         
  case eNUAlwaysBlock:
    status = walker.alwaysBlock (dynamic_cast<NUAlwaysBlock*>(codeBlock));
    break;

  case eNUTaskEnable:
    status = walker.taskEnable (dynamic_cast<NUTaskEnable*>(codeBlock));
    break;

  case eNUTriRegInit:
  {
    NUTriRegInit* tri = dynamic_cast<NUTriRegInit*>(codeBlock);
    tri->putScheduled (true);
    break;
  }

  default:
    // Should have handled this.  If we can schedule it, we should be able
    // to suppress emitting the code for it.
    //
    NU_ASSERT ( 0 == "Unrecognized flowable nucleus object", codeBlock);
    break;
  }

  NU_ASSERT (status != NUDesignCallback::eInvalid, codeBlock);
}


void CodeGen::visitSchedule (void)
{
  VisitBlocks markScheduled;
  NUDesignWalker walker (markScheduled,
                         false,  // no verbosity
                         false); // no module memory
  walker.putWalkTasksFromTaskEnables (true);

  // Rip thru the schedules, marking each BlockScope as visited.
  for (SCHSchedulesLoop l = mSched->loopAllSequentials(); !l.atEnd(); ++l) {
    SCHSequential* seq = l.getSequentialSchedule();
    for (SCHIterator i = seq->getSchedule(eClockPosedge); !i.atEnd(); ++i) {
      FLNodeElab* flowElab = *i;
      sMarkFlow (walker, flowElab);
    }

    for (SCHIterator i = seq->getSchedule(eClockNegedge); !i.atEnd(); ++i) {
      FLNodeElab* flowElab = *i;
      sMarkFlow (walker, flowElab);
    }
  }
  for (SCHSchedulesLoop l = mSched->loopAllCombinationals(); !l.atEnd(); ++l) {
    SCHCombinational* comb = l.getCombinationalSchedule();
    for (SCHIterator i = comb->getSchedule(); !i.atEnd(); ++i) {
      FLNodeElab* flowElab = *i;
      sMarkFlow (walker, flowElab);
    }
  }
}

