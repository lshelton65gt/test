// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 /*
 * \file
 * Utility class for doing codegen-centric design walks and callback.
 */


#ifndef __CGFINDINSTANCES_H_
#define __CGFINDINSTANCES_H_

#include "nucleus/NUInstanceWalker.h"

class CodeGen;
class CGOFiles;
class STSymbolTable;
class NUModuleInstance;

//! A class to walk the design looking for all module instances.
/*! This is just a wrapper class around the NUInstanceCallback which
 *  adds support for storing and accessing the main codegen class.
*/
class CGFindInstancesCallback : public NUInstanceCallback
{
public:
  //! constructor
  CGFindInstancesCallback(STSymbolTable* symTab, CodeGen* codeGen, CGOFiles* cgOFiles) :
    NUInstanceCallback(symTab), mCodeGen(codeGen), mCGOFiles(cgOFiles)
  {}

  //! destructor
  ~CGFindInstancesCallback() {}

protected:
  //! Access function for our derived class to the codegen variable
  CodeGen* getCodeGen() const { return mCodeGen; }
  CGOFiles* getCGOFiles() { return mCGOFiles; }

  virtual void handleInstance(STBranchNode*,NUModuleInstance*) {}
  virtual void handleDeclScope(STBranchNode*,NUNamedDeclarationScope*) {}

private:
  // Local data
  CodeGen* mCodeGen;
  CGOFiles * mCGOFiles;
}; // class CGFindInstancesCallback : public NUDesignCallback

#endif
