// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
 * \file
 * Implement a simple lookup helper class
 */

#ifndef __NETMAPCOMPARE_H__
#define __NETMAPCOMPARE_H__
//!Functor class to lookup clock or other elaborated net in map given a NUNet
/*!
 * These maps are used to associate clocks or primary inputs (PIs) with
 * an index value that's used to access the changed[] array.
 */
class NetMapCompare: public std::unary_function<CodeGen::NetToIntMap, bool> {
private:
  const NUNet* aNet;
  const NUNetElab* eNet;

public:
  //! Trivial constructors
  NetMapCompare (const NUNetElab* net): aNet (0), eNet (net) {}

  //! Overloaded
  NetMapCompare (const NUNet* net):aNet(net), eNet (0) {
    // If we're comparing the Map against a non-primary net, we are in trouble.
    // It's possible to have multiple derived clocks elaborated from a single
    // unelaborated net.  We could find a changedMap[] index that was not
    // associated with the instance we are looking for if we didn't use
    // an elaborated value in the constructor.
    NU_ASSERT (net->isPrimary (), net);
  }

  //! Is \a aNet in the list of NET:index pairs?
  bool operator ()(CodeGen::NetToIntMap::value_type &p)
  {
    if (eNet)
      return p.first == eNet;
    else if (aNet)
      return p.first->getNet () == aNet;
    else
      return false;
  };
};
#endif

/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
