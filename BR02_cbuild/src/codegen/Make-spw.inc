# -*- makefile -*-
#
# *****************************************************************************
# Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
# *****************************************************************************
#
# compiler-dependent makefile fragment for Solaris 11


# Define where compiler can be found
#
CARBON_COMPILER_DIST=${CARBON_HOME}/${CARBON_TARGET_ARCH}/spw-11
#SPW_LIB_Linux := lib
#SPW_LIB_Linux64 := lib64

CARBON_CBUILD_CC=$(CARBON_COMPILER_DIST)/bin/cc
CARBON_CBUILD_CXX=$(CARBON_COMPILER_DIST)/bin/CC
CARBON_CBUILD_LD = $(CARBON_CBUILD_CXX)

#SPW:=1

##
# Search all passed in options for -O<level> flags
CARBON_CXX_OPTLIST:=$(filter -O%,$(CXX_EXTRA_FLAGS) $(CXX_WC_FLAGS))

# Find the last one specified
ifeq (,$(CARBON_CXX_OPTLIST))
CARBON_CXX_OPT := -xO2
else
CARBON_CXX_OPT:=$(patsubst -O%,-xO%,$(word $(words $(CARBON_CXX_OPTLIST)),$(CARBON_CXX_OPTLIST)))
endif

ifneq (,$(findstring -p,$(CXX_EXTRA_FLAGS) $(CXX_WC_FLAGS)))
CARBON_PROFILING=1
endif


CARBON_OPTFLAGS=
ifneq (,$(findstring $(CARBON_CXX_OPT),-xO2 -xO3))
  ifeq ($(CARBON_TARGET_ARCH),Linux)
    CARBON_OPTFLAGS += -fast -xtarget=pentium4
  endif

  ifeq ($(CARBON_TARGET_ARCH),Linux64)
    CARBON_OPTFLAGS += -fast
  endif

  # options useful when you want to profile with gprof
  # which relies on stack frames to annotate the call-graphs and histograms
  # NOTE: profiling used to turn off gcc inlining, but that significantly skews
  # the results, so now profiling just turns off frame pointer omission.
  ifeq ($(CARBON_PROFILING),1)
    CARBON_OPTFLAGS +=
  else
    CARBON_OPTFLAGS +=
  endif
else				# -Os or -O1 or -O0
  ifneq (,$(findstring $(CARBON_CXX_OPT),-Os -O1))
    ifeq ($(CARBON_TARGET_ARCH),Linux)
      CARBON_OPTFLAGS +=
    endif
  else				# -O0
  endif
endif


# Basic compiler-dependent flags
#
CARBON_CXX_FLAGS_Linux = -m32
CARBON_CXX_FLAGS_Linux64 = -m64 -D__LP64__

# Define magic values that turn C++ exceptions off/on
CARBON_EXCEPTIONS_OFF:=
CARBON_EXCEPTIONS_ON :=

CARBON_CXX_FLAGS = $(CARBON_EXCEPTIONS_OFF) $(CARBON_CXX_FLAGS_$(CARBON_TARGET_ARCH)) -v
CARBON_CXX_DEF_COMPILE_FLAGS= $(CARBON_CXX_FLAGS)

CARBON_DISABLE_DYNAMIC_CAST = 
CARBON_ENABLE_DYNAMIC_CAST = 

CARBON_CXX_SPEEDCC_COMPILE_FLAGS += $(CARBON_DISABLE_DYNAMIC_CAST) $(CXX_WC_FLAGS)

ifeq ($(PROFILING),1)
else
ifeq ($(GCC_NO_WALL),)
# too many aliasing warnings for straightforward things; and reinterpret_cast<> doesn't
# suppress these warnings apparently in 4.1
CARBON_CXX_SPEEDCC_COMPILE_FLAGS += -xwe +w 
endif
endif

# flags needed to compile shared libraries
CARBON_SHARED_COMPILE_FLAGS= -shared -fPIC
# flags needed to link shared lib.  Note that 'LIBPATH' is defined in Makefile.mc
CARBON_SHARED_LINK_FLAGS=-Wl,-rpath,$(@D)

ifeq (,$(findstring $(CARBON_COMPILER_DIST),$(LD_LIBRARY_PATH)))
  LD_LIBRARY_PATH := $(LD_LIBRARY_PATH):$(CARBON_COMPILER_LIB_DIRS)
  export LD_LIBRARY_PATH
endif

COMPILER_LIB_LIST:=
