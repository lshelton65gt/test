// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "codegen/CGProfile.h"
#include "codegen/Blob.h"
#include "compiler_driver/CarbonContext.h"

CGProfile::CGProfile()
  : mCompileDB(NULL), mPrevBucket(-1)
{
}

void
CGProfile::emitStartBucket(bool force, const SourceLocator& loc)
{
  SInt32 bucket = gCodeGen->getCarbonContext()->getBlobMap()->lookup(loc)->getBucket();

  if (force || bucket != mPrevBucket) {
    gCodeGen->CGOUT() << "\n  SETPROFILEBUCKET(" << gCodeGen->getBucketVarName() << "," << bucket <<");\n";
    mPrevBucket = bucket;
  }
}

void
CGProfile::emitStopBucket()
{
  gCodeGen->CGOUT() << "\n  SETPROFILEBUCKET(" << gCodeGen->getBucketVarName() << ", -1);\n";
  mPrevBucket = -1;
}

void
CGProfile::resetBucket()
{
  mPrevBucket = -1;
}

void
CGProfile::setFile(const char* fileRoot)
{
  if (gCodeGen->getCarbonContext()->compileWithProfiling()) {
    UtString filename;
    filename << fileRoot << ".prof";
    mCompileDB = new UtOFStream(filename.c_str());
    UtString uid;
    gCodeGen->getUID(&uid);
    *mCompileDB << uid << '\n';
  }
}

void
CGProfile::recordBucket(const Blob* blob)
{
  // Write the source location of this bucket to the compile-time
  // profile data base.
  if (gCodeGen->getCarbonContext()->compileWithProfiling() && mCompileDB) {
    // Create source locator
    SourceLocator loc(blob->getLoc());
    UtString source;
    loc.compose(&source);       // Get source locator string
    bool hide = loc.isTicProtected();

    // Create name for parent module
    const char* parentName;
    const NUModule* parent = blob->getParent();
    if (parent != NULL) {
      parentName = parent->getOriginalName()->str();
    } else {
      parentName = "(none)";
    }
    
    // Output to the file
    *mCompileDB << blob->getBucket() << ' '
                << ( hide ? "protected_region" : blob->getTypeStr() )
                << ' ' << parentName
                << ' ' << source << '\n';
  }
}
