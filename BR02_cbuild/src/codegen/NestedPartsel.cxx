// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Handles nested NUVarselLvalue, generate bounds checking code.
*/

#include "nucleus/Nucleus.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetSet.h"
#include "util/ArgProc.h"

#include "compiler_driver/CarbonContext.h"

#include "codegen/codegen.h"
#include "codegen/CGOFiles.h"

#include "reduce/Fold.h"
#include "emitUtil.h"
#include "NestedPartsel.h"
//#define DEBUG_BOUNDS

class ConstantRange;

//! Top level function.
/*! emit necessary inline bounds checking, bounds
 *  calculation code, plus 
 *  WSET_SLICE[_SHIFT](dest,indexExpr,size,[shift,]rhs)
 */
CGContext_t 
NestedPartselLvalue::emitNestedPartselLvalue (CGContext_t context)
{
  CGContext_t ctx;
  bool use_temp = false;

  setInnermost();

  OUT() << "if" << "(";
  if (mNoOOB)
    OUT () << "1 || ";          // Let the compiler fold away extra guard code
  emitBoundsChecking(mLv, context);
  OUT() << ")";

  newLine();
  OUT() <<  "{";
  newLine();

  if (needsTempBounds() ) {
    emitBoundsTemp();
    resetNewIndexExpr();
    emitBoundsCalc(mLv, context);
    use_temp = true;
  }

  ConstantRange range(*mLv->getRange ());
  if (range.getLsb() != 0) {
    const NUExpr* lsb = NUConst::create (false, range.getLsb(),
                                         32,
                                         mLv->getLoc());
    combineNewIndexExpr(lsb);
    delete lsb;
  }

  if (mLv->getBitSize() == 1)
    ctx = emitSetBit(context, use_temp);
  else
    ctx = emitSetSlice(context, use_temp);

  OUT() <<  ";}";
  newLine();
  return ctx; 
}

// Produce a valid expression "left < right" and join it with
// any other bounds expressions via "&&".

static NUExpr* sGenBound (NUExpr* l, NUExpr* r, NUExpr* others)
{
  NUOp::OpT cond =
    (l->isSignedResult () || r->isSignedResult ()) ? NUOp::eBiSLt : NUOp::eBiULt;

  NUBinaryOp* bop = new NUBinaryOp (cond, l, r, l->getLoc ());

  if (others)
    bop = new NUBinaryOp (NUOp::eBiLogAnd, bop, others, others->getLoc ());

  return bop;
}

//! emit bounds checking code
NUExpr* NestedPartselLvalue::emitBoundsChecking (const NUVarselLvalue * lv, 
                                                 CGContext_t context)
{
  CopyContext cc (0,0);

  // from the innermost
  NUExpr* bounds = NULL;

  if (lv->getLvalue()->getType() == eNUVarselLvalue)
    bounds = emitBoundsChecking(dynamic_cast<const NUVarselLvalue*>(lv->getLvalue()),context);

  // assume all outer constant indices are folded. 
  //
  if (lv->isConstIndex()) {
    NU_ASSERT(lv == mInnermost, lv);
    ConstantRange cr (*lv->getRange());
    putNewIndexExpr(NUConst::create (false, cr.getLsb(), 32, mLv->getLoc()),
                    false);
    return NULL;
  }

  NUExpr* index = lv->getIndex();

  //! the comments in this routine is based on
  /*!  an example of x[a +: s1][b +: s2], x is of bitSize n.
   */

  UInt32 s1 = lv->getLvalue ()->getBitSize ();
  UInt32 s2 = mInnermost->getLvalue ()->getBitSize ();

  // ( b < s1 )

  bounds = sGenBound (index->copy (cc),
                      NUConst::create (index->isSignedResult (),
                                       s1,
                                       32, lv->getLoc ()),
                      bounds);

  // only check for negative index if isSignedResult
  // && (  -s1 < b )
  if (index->isSignedResult()) {
    bounds = sGenBound (NUConst::create (true,
                                         -s1,
                                         32, lv->getLoc ()),
                        index->copy (cc),
                        bounds);
  }

  if (lv == mInnermost) {
    NU_ASSERT(mNewIndexExpr == NULL, lv);
    putNewIndexExpr(lv->getIndex(),true);
  }
  else {
    // add the index to the newIndex
    //
    combineNewIndexExpr(index);

    NUExpr* newIndex = getNewIndexExpr();
    // && ( (a+b) < s2 )
    // don't do it if ((newIndexExpr == Index)
    // taking the offset into count, it may be 
    // covered already.
    if (!isConditionOneCovered(lv)) {
      bounds = sGenBound (newIndex->copy (cc),
                          NUConst::create (newIndex->isSignedResult (),
                                           s2,
                                           32,
                                           mInnermost->getLoc ()),
                          bounds);
    }

    // && ( (a+b) > -s2 )
    // don't do it if not isSignedResult
    // don't do it if newIndexExpr == Index and a > 0
    //
    if ((newIndex->isSignedResult()) &&
	!isConditionTwoCovered(lv)) {
      bounds = sGenBound (NUConst::create (true,
                                           - s2, 32, mInnermost->getLoc ()),
                          newIndex->copy (cc),
                          bounds);
    }
  }
  // if not the outermost, pass back to next level.
  if (lv != mLv)
    return bounds;

  // resize, fold, code and delete
  bounds->resize (1);           // Want boolean result
  bounds = gCodeGen->getFold ()->fold (bounds);
  bounds->emitCode (context);
  delete bounds;
  return NULL;
}

//! emit lower and upper bound temporaries declaraion/initialization.
/*! if the innermost level is constant index, initialize to Lsb/Msb
 *  otherwise, initialize to 0 and size of the base object.
 */
void NestedPartselLvalue::emitBoundsTemp ()
{
  bool constInnermost = mInnermost->isConstIndex();

  if (constInnermost) {
    ConstantRange cr (*mInnermost->getRange());
    OUT() << "CarbonUInt16 " << mLowerBound << " = " << cr.getLsb();
    endLine();
    OUT() << "CarbonUInt16 " << mUpperBound << " = " << cr.getMsb();
    endLine();
  }
  else {
    OUT() << "CarbonUInt16 " << mLowerBound << " = 0";
    endLine();
    OUT() << "CarbonUInt16 " << mUpperBound << " = " 
          << mInnermost->getLvalue()->getBitSize()-1;
    endLine();
  }
  newLine();
  return;
}

//! emit bounds calculation code from the innermost out, 
/*! recursively, assuming no constant index can possibly
 *  exist except the innermost level.
 *  the effect --
 *  emit code for lb and ub calculation 
 *  accumulate the index expr from inner out.
 */
void NestedPartselLvalue::emitBoundsCalc(const NUVarselLvalue * lv, 
					 CGContext_t context)
{
  // start from the innermost
  if (lv->getLvalue()->getType() == eNUVarselLvalue)
    emitBoundsCalc(dynamic_cast<const NUVarselLvalue*>(lv->getLvalue()), context);

  if (lv == mInnermost) {
    // make sure the field is clean.
    NU_ASSERT(mNewIndexExpr == NULL, lv);
    if (lv->isConstIndex()) {
      ConstantRange cr (*lv->getRange());
      // put the constant bitOffset to the new accumulating index
      putNewIndexExpr(NUConst::create (false, cr.getLsb(), 
                                       32, mLv->getLoc()),false);
      // no bounds checking is necessary for constant index, return.
      return;
    }
    else {
      putNewIndexExpr(lv->getIndex(),true);
    }
  }
  else {
     combineNewIndexExpr(lv->getIndex());
  }

  NUExpr* newIndex = getNewIndexExpr();

  //! generating code for bounds calculation --
  /*! ub = getNewIndexExpr()+ lv->getLvalue()->getBitSize() - 1 > ub ?
   *     ub :   
   *     getNewIndexExpr()+ lv->getLvalue()->getBitSize()- 1;
   */
  OUT() << mUpperBound  << " = " << "("; 
  emitNewIndex(context);
  OUT() << " + " << emitUtil::CBaseTypeFmt(newIndex) 
        << "(" << lv->getBitSize()-1 << "))" 
        << " > " << mUpperBound << " ? ";
  newLine5();
  OUT() << mUpperBound << " : ";
  newLine5();
  emitNewIndex(context);
  OUT() << " + " << emitUtil::CBaseTypeFmt(newIndex) 
        << "(" << lv->getBitSize()-1 << ") ";
  endLine();

  //! lb = getNewIndex() < lb ? 
  /*!      lb : 
   *      getNewIndex();
   */
  OUT() << mLowerBound << " = ";
  emitNewIndex(context);
  OUT() << " < " << mLowerBound << " ? ";
  newLine5();
  OUT() << mLowerBound << " : ";
  newLine5();
  emitNewIndex(context);
  endLine();

#ifdef DEBUG_BOUNDS  
  debugPrint("Bounds Calc: ");
#endif

  return;
}
//! code to generate --
/*
 *    WSET_SLICE_CHECK(x, lb, (ub-lb+1), src);
 */
CGContext_t NestedPartselLvalue::emitSetSlice(CGContext_t context, 
					      bool useTemp)
{
  newLine();
  genSetSlice(context, useTemp);
  endLine();

  return context;
}
//! actually generate code for
/*! WSET_SLICE_CHECK(x, xsize, [lb|index], (ub-lb+1), src);
 */
CGContext_t NestedPartselLvalue::genSetSlice(CGContext_t context, 
					     bool useTemp)
{
  NULvalue * id = mInnermost->getLvalue();
  NU_ASSERT(id, mInnermost);

#ifdef DEBUG_BOUNDS  
  if (useTemp)
    debugPrint("Set Slice: ");
#endif

  // TODO: if we knew the index was inbounds we could just do a WSET_SLICE()
  OUT() << "WSET_SLICE_CHECK (";

  id->emitCode(context);
  OUT() << ", " << id->getBitSize () << ", ";
  newLine10();

  // use temps -- lb, (ub-lb+1)
  //
  UInt32 lsize;
  if (useTemp) {
    OUT() << mLowerBound << ", ";
    newLine10();
    OUT() << "( " << mUpperBound << " - " << mLowerBound << " + 1 )";
    lsize = mUpperBound + 1 - mLowerBound;
  }
  // no temps -- 
  else {
    emitNewIndex(context);
    OUT() << ", ";
    newLine10();
    emitSizeCalc(context);
  }
  OUT() << ", ";
  newLine10();
  OUT() << emitUtil::CBaseTypeFmt(mRHS->getBitSize(), eCGDefine);
  mRHS->emitCode(context);

  OUT() << ")";
  return eCGVoid;               // No information to return from these!
}

//! emit SET_BIT code
CGContext_t NestedPartselLvalue::emitSetBit(CGContext_t context, 
					    bool useTemp)
{
  NULvalue * id = mInnermost->getLvalue();
  NU_ASSERT(id, mInnermost);

  //  OUT() << " int x = ";
  //  emitNewIndex(context);
  //  OUT() << ";";
  //  OUT() << " printf(\"index = %d,\\n\", x); ";
  //  newLine5();

  OUT() << "SET_BIT" << "(";
  id->emitCode(context);
  OUT() << ", ";
  newLine5();

  // use temps -- lb, 
  //
  if (useTemp)
    OUT() << mLowerBound;
  else
    emitNewIndex(context);

  OUT() << ", ";
  newLine5();

  OUT() << emitUtil::CBaseTypeFmt(id->getBitSize(), eCGDefine);
  mRHS->emitCode(context);

  OUT() << ")";
  return eCGVoid;
}

//! calculate the size for the special case - 2-level
/*! nested, i.e.,
 *  x[a :+ s1][b :+ s2] where a is a constant, and
 *                            b is a variable.
 *  no bounds temporaries are used.
 *  the complete size calculation should be
 *  ((b+s2)>s1 ? (b>0 ? s1-b : s1) : (b>0 ? s2: (b+s2>0?(b+s2):0)))
 *
 *  if b > 0, the checking is simplified to
 *  ((b+s2)>s1 ? s1-b : s2 ) 
 *  where  b <= s1, so s1-b >= 0.
 *
 */
void NestedPartselLvalue::emitSizeCalc(CGContext_t context)
{
  CopyContext cc (0,0);
  const SourceLocator loc = mLv->getLoc ();

  UInt32 s2 = mLv->getBitSize();
  UInt32 s1 = mLv->getLvalue()->getBitSize();

  NU_ASSERT( s2 <= s1, mLv);

  // Generate the width expression: (index > (s1-s2)) ? s1-b : s2

  NUExpr* index = mLv->getIndex();
  NUExpr* bounds = sGenBound (NUConst::create (index->isSignedResult (),
                                               s1-s2,
                                               32,
                                               loc),
                              index->copy (cc),
                              NULL);
  NUExpr* sizeExpr = new NUTernaryOp (NUOp::eTeCond,
                                      bounds,
                                      new NUBinaryOp (NUOp::eBiMinus,
                                                      NUConst::create (false, s1, 32, loc),
                                                      index->copy (cc),
                                                      loc),
                                      NUConst::create (false, s2, 32, loc),
                                      loc);

  sizeExpr->resize (sizeExpr->determineBitSize ());
  sizeExpr = gCodeGen->getFold ()->fold (sizeExpr);
  sizeExpr->emitCode (context);
  delete sizeExpr;
}

//! set the new index expr with the given expr, make a copy if necessary.
inline void NestedPartselLvalue::putNewIndexExpr(const NUExpr *e, bool cp)
 { 
   if (cp) {
     CopyContext cc(0,0);
     mNewIndexExpr = e->copy(cc);
   }
   else 
     mNewIndexExpr = (NUExpr *)e;
 }

//! combine the current level index to the new index
void NestedPartselLvalue::combineNewIndexExpr(const NUExpr * idx) 
{
  bool isSigned = false;
  CopyContext cc(0,0);

  if (getNewIndexExpr())
    isSigned = getNewIndexExpr()->isSignedResult () || idx->isSignedResult ();

  NUExpr * e = idx->copy(cc);
  mNewIndexExpr = new NUBinaryOp (NUOp::eBiPlus,
				  getNewIndexExpr(),
				  e,
				  mLv->getLoc());

  // Must compute natural size once we've altered the index with bounds offsets
  mNewIndexExpr->resize (mNewIndexExpr->determineBitSize ());

  // if either one is signed, treat the result as signed (override the rule.)
  //
  if (isSigned)
    mNewIndexExpr->setSignedResult(isSigned);

  mNewIndexExpr = mFold->fold(mNewIndexExpr);
}

//! returns true if there is any variable index
/*! in the nested part select lv. */
bool NestedPartselLvalue::needsBoundsChecking()
{
  if (!mLv->isConstIndex())
    return true;

  NULvalue * lv2 = mLv->getLvalue();
  while (lv2->getType() == eNUVarselLvalue ) {
        const NUVarselLvalue* vs = dynamic_cast<const NUVarselLvalue*>(lv2);
        if (!vs->isConstIndex())
	  return true;
       
        lv2 = vs->getLvalue();
  }

  return false;
}     

//! to by-pass bounds checking if so specified or determined smartly.  NYI.
inline bool NestedPartselLvalue::noBoundsChecking(/*const NUVarselLvalue * lv*/)
{
  if (mNoOOB)
    return true;

  //  Do we need this checking as it does in emitNet?
  //  UInt32 indexWidth = lv->getIndex ()->effectiveBitSize ();
  //  UInt32 baseSize = lv->getLvalue()->getBitSize();
  //  if (! (indexWidth > 16 || (((1u << indexWidth) - 1) > baseSize)))
  //    return true;

  return false;
}
//! returns true if it needs temporary lb and ub.  Temporaries are needed
/*! if there are more than one variable indices in the nested 
 *  partselect lvalue, or if any of the intervening nodes are not zero-origined subranges
 */
bool NestedPartselLvalue::needsTempBounds()
{
  UInt32 count = mLv->isConstIndex()? 0 : 1;
  NULvalue * lv2 = mLv->getLvalue();
  while ((lv2->getType() == eNUVarselLvalue ) && (count < 2)){
        const NUVarselLvalue* vs = dynamic_cast<const NUVarselLvalue*>(lv2);
        if (!vs->isConstIndex())
	  count +=1;
        lv2 = vs->getLvalue();
  }

  return (count > 1);

}


//! find and set the innermost level of NUVarselLvalue
inline void NestedPartselLvalue::setInnermost()
{
  const NUVarselLvalue * lv = dynamic_cast<const NUVarselLvalue*>(mLv->getLvalue());
  const NUVarselLvalue *lv2 = lv;
  while (lv) {
    lv2 = lv;
    lv = dynamic_cast<const NUVarselLvalue*>(lv->getLvalue());
  }
  NU_ASSERT(lv2, mLv);
  mInnermost = lv2;
  return;
}

//! special case 1 for x[const +: s1][variable +: s2],
/*! to check if the second level bounds checking 
 *  needs to do the combined new index bound
 *  checking against the base bound, i.e.,
 *  if (variable+const > n) is already covered.
 */
bool NestedPartselLvalue::isConditionOneCovered(const NUVarselLvalue * lv)
{
  if (!(mInnermost->isConstIndex()))
    return false;

  if ((getNewIndexExpr()->equal(*(lv->getIndex()))))
    return true;

  ConstantRange cr (*mInnermost->getRange());
  UInt32 size = mInnermost->getLvalue()->getBitSize();
  return ((size - cr.getLsb()) <= mInnermost->getBitSize());
}

//! special case 2 for x[const +: s1][variable +: s2],
/*! to check if the second level bounds checking need 
 *  to do the combined new index checking against -s2
 */
bool NestedPartselLvalue::isConditionTwoCovered(const NUVarselLvalue * lv)
{
  if (! mInnermost->isConstIndex())
    return false;
  if ((getNewIndexExpr()->equal(*(lv->getIndex()))))
    return true;

  ConstantRange cr (*mInnermost->getRange());
  return ( cr.getLsb() > 0);
}
//! Handles nested NUVarselLvalue assign
CGContext_t emitUtil::nestedPartselAssign(const NUVarselLvalue* lv, 
				const NUExpr* rhs, 
				CGContext_t context)
{
  CGContext_t ctx;
  NestedPartselLvalue * nlv = new NestedPartselLvalue(lv, rhs, 
			      gCodeGen->getFold(),
			      gCodeGen->getCGOFiles()->CGOUT(),
			      gCodeGen->mNoOOB);

  if (nlv->needsBoundsChecking())
     ctx = nlv -> emitNestedPartselLvalue(context);
  else {
     ctx = gCodeGen->getFold()->fold ((NULvalue *)lv)->emitCode(context);
  }

  delete nlv;
  return ctx; 
}
//! emit the new index variable via emitUtil::emitIndex
void NestedPartselLvalue::emitIndex (const NUVarselLvalue * lv, CGContext_t context)
{
  NU_ASSERT(lv, lv);

  emitUtil::emitIndex(OUT(), lv->getIndex(), 0,
		      context &~eCGLvalue, 
		      *lv->getRange());
}

//! emit the new index variable via emitUtil::emitIndex
void NestedPartselLvalue::emitNewIndex (CGContext_t context)
{
  emitUtil::emitIndex(OUT(), getNewIndexExpr(), 0,
		      context &~eCGLvalue, 
		      *mInnermost->getRange());
}

//! print out the upper/lower bounds ifdef DEBUG_BOUNDS 
void NestedPartselLvalue::debugPrint (char * s)
{
  newLine();
  OUT() << "printf" << "(\"" << s << "\")";
  endLine();
  OUT() << "printf (\"lb = %x, ub = %x\\n\"," << mLowerBound << ", " << mUpperBound << ")";
  endLine();
  return;
}
