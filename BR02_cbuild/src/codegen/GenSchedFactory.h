// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef _GENSCHEDFACTORY_H
#define _GENSCHEDFACTORY_H
 /*
 * \file
 */
/*!
 * Manage multiple GenSched objects, including creation, identification of
 * equivalent schedules and optimization.
 *
 */

#include "GenSched.h"
#include "util/UtMap.h"

//! Reuse code-gen schedules

class GenSchedFactory
{
public:
  GenSchedFactory (AtomicCache* atomicCache);
  ~GenSchedFactory ();

  //! Create a schedule generator for a combinational schedule
  /*!
   * Common initialization code to create GenSched objects.
   * Called with \arg f - a schedule function name
   * \arg s - a schedule generator
   * \arg flags - indicator flags to direct the kind of schedule generated
   * \arg section - the schedule will be emitted into this section
   * Returns a \return GenSched.
   */
  GenSched* generate (const char*f, SCHCombinational *s, CGOFiles *fio,
                      int flags=0, const char* section=0,
                      bool suppressObfuscate = false);

  //! overloaded for creating a sequential schedule
  GenSched* generate (const char*f, SCHSequential *s, CGOFiles *fio, ClockEdge e, int flags=0, const char* section=0);


  //! overloaded for creating a cycle resolution
  GenSched* generate (const char *f, NUCycle* c, CGOFiles *fio, const char* section=0);

  //! Look up a schedule by function-name
  GenSched *findGenSched (const UtString& funName) const;

private:
  GenSchedFactory (const GenSchedFactory&);
  GenSchedFactory& operator=(const GenSchedFactory&);

  //! Search for an existing equivalent schedule
  GenSched* findEquivalent (GenSched*);

  //! Collection of schedule generators, indexed by function name
  typedef UtHashMap<StringAtom*,GenSched*> ScheduleFunctionMap;
  ScheduleFunctionMap* mGenSchedules;

  //! Remember schedules with a common hierarchy prefix
  typedef UtMultiMap<STBranchNode*, GenSched*> GenSchedMap;
  GenSchedMap *mGenSchedMap;

  AtomicCache* mAtomicCache;
};

#endif // _GENSCHEDFACTORY_H
/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
