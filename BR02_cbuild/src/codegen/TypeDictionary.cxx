// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 

/*!
  \file
  Handle mapping from NUNet types to symbol table, including constructing
  a type-dictionary and type information accessors.
*/

#include <cstdlib>

#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUElabHierHelper.h"
#include "nucleus/NUNamedDeclarationScope.h"

#include "codegen/codegen.h"
#include "codegen/CGAuxInfo.h"
#include "emitUtil.h"

#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "iodb/CbuildShellDB.h"
#include "iodb/IODBNucleus.h"
#include "schedule/Schedule.h"
#include "codegen/CGNetElabCount.h"
#include "util/AtomicCache.h"

// Remember last net and offset if we are not checking
// *all* offsets due to -codegenOffsetSanity
static const STSymbolTableNode* sLastNode = NULL;
static size_t sLastOffset = 0;

// local nets that are elaboratedly dead and not allocated by the new allocator
// cannot be sanity-checked!  Nodes that are not nets can be sanity checked,
// I guess, as elaboarated modules.
static bool sHasValidOffset(const STSymbolTableNode* node)
{
  const STAliasedLeafNode* leaf = node->castLeaf();
  if ((leaf != NULL) && (leaf->getInternalStorage() == NULL))
    return false;

  const NUBase* base = CbuildSymTabBOM::getNucleusObj(node);
  if (base == NULL)
    return false;

  // if this node is a net, check to see if it's valid for codegen into
  // the class structure
  const NUNet *net = dynamic_cast<const NUNet*>(base);
  if (net == NULL)
  {
    const NUNetElab* netElab = dynamic_cast<const NUNetElab*>(base);
    if (netElab != NULL)
      net = netElab->getStorageNet();
  }
  if ((net != NULL) && !emitUtil::isNetDeclared(net))
    return false;

  return true;
}

// Given a NUNetElab or a NUModuleElab, print an assertion as needed
static void
sPrintDebugAssert (const STSymbolTableNode* node, size_t offset)
{
  if (sHasValidOffset(node))
  {
    const NUBase* base = CbuildSymTabBOM::getNucleusObj(node);
    ST_ASSERT(base, node);
    const NUElabBase* elab = dynamic_cast<const NUElabBase*>(base);
    const NUNet *net = dynamic_cast<const NUNet*>(base);
    
    // code to make debugging size issues easier
    gCodeGen->CGOUT () << "  check_off((const char*)&";

    if (elab) {
      elab->emitCode (eCGHierName | eCGNameOnly);
    } else {
      const STBranchNode* parent = node->getParent();
      if (parent) {
        const NUBase* modBase = CbuildSymTabBOM::getNucleusObj(parent);
        const NUScopeElab* scopeElab = dynamic_cast<const NUScopeElab*>(modBase);
        NU_ASSERT(scopeElab, modBase);
        NUScope::ScopeT scope_type = scopeElab->getScope()->getScopeType();
        NU_ASSERT(((scope_type == NUScope::eNamedDeclarationScope) || (scope_type == NUScope::eModule)), modBase);
        modBase->emitCode(eCGHierName | eCGNameOnly);

	gCodeGen->CGOUT () << ".";
      }
      
      gCodeGen->CGOUT() << emitUtil::member(net);
    }

    // Adjust for vtab.  Classes with a virtual pointer must also
    // implement operator&() to point to their first data member so
    // that the shell (among others) never sees the mess when accessing
    // raw data in shellnets.
    //
    const NUNetElab *net_elab = dynamic_cast<const NUNetElab*>(elab);
    if (net_elab) {
      net = net_elab->getNet();
    }

    if (net && gCodeGen->hasVtab(net))
      gCodeGen->CGOUT() << " - " << gCodeGen->vtabAdjustment(net);

    gCodeGen->CGOUT () << " - sizeof (carbon_top) - p, " << offset << ");\n";
  }
} // sPrintDebugAssert

static void
sCheckDebugOffset (const STSymbolTableNode* node, size_t offset)
{
  if (gCodeGen->genOffsetSanity())
    sPrintDebugAssert (node, offset);
  else if (sHasValidOffset(node))
  {
    sLastNode = node;
    sLastOffset = offset;
  }
}

static STAliasedLeafNode* sFindNameInSymtab(const STBranchNode* branch,
                                            StringAtom* name,
                                            STSymbolTable* symtab)
{
  STSymbolTableNode* node = symtab->find(branch, name);
  ST_ASSERT(node, branch);
  STAliasedLeafNode* leaf = node->castLeaf();
  ST_ASSERT(leaf, node);
  return leaf;
}

static STAliasedLeafNode* sFetchForceValueLeaf(const STAliasedLeafNode* 
                                               forcible,
                                               AtomicCache* atomCache,
                                               STSymbolTable* symtab)
{
  StringAtom* valueName = IODB::sGetForceValueName(forcible, atomCache);
  return sFindNameInSymtab(forcible->getParent(), valueName, symtab);
}


static STAliasedLeafNode* sFetchForceMaskLeaf(const STAliasedLeafNode* 
                                              forcible,
                                              AtomicCache* atomCache,
                                              STSymbolTable* symtab)
{
  StringAtom* maskName = IODB::sGetForceMaskName(forcible, atomCache);
  return sFindNameInSymtab(forcible->getParent(), maskName, symtab);
}

static const IODBIntrinsic* sComputeIntrinsic(const NUNet* net, IODBTypeDictionary* typeDictionary)
{
  const ConstantRange* vb = NULL;
  const IODBIntrinsic *intrinsic = NULL;
  if (const NUVectorNet* vectorNet = net->castVectorNet ())
  {
    vb = vectorNet->getDeclaredRange ();
    // Make sure that the vector intrinsic is added, if needed in
    // the shell
    intrinsic = typeDictionary->addVectorIntrinsic( vb->getMsb(), vb->getLsb() );
  }
  else if (const NUMemoryNet* memNet = net->getMemoryNet())
  {
    const ConstantRange *mb = memNet->getDepthRange ();
    vb = memNet->getDeclaredWidthRange ();
    intrinsic = typeDictionary->addMemoryIntrinsic( mb->getMsb(), mb->getLsb(),
                                                     vb->getMsb(), vb->getLsb());
  }
  else
    intrinsic = typeDictionary->addScalarIntrinsic();
  return intrinsic;
}

//! Add the type information for a Net to the dictionary.
/*! Saves the index associated with this type to assist the creation
 * of a runtime debug shell symbol table with type-accessors parameterized.
 *
 * The dictionary looks like \<typecode\>\<typeparams\>
 * where \<typecode\> is one letter indicating (S)calar, (V)ector, or
 * (M)emory, and \<typeparams\> is the primitive type (int8,BitVector, etc.
 * and any template parameters needed to supply bitvector width or memory
 * depth.
 */
void
CodeGen::AddTypeToDictionary (const NUNet* net, const NUNet* storageNet, 
                              STAliasedLeafNode* leaf,
                              STSymbolTable* symtab)
{
  // If there is no net, there is no information to add to the dictionary.
  if (! net)
    return;

  SInt32 genTypeIndex = 0;

  // Add intrinsic info for each net type into the dictionary
  const IODBIntrinsic *intrinsic = sComputeIntrinsic(net, mTypeDictionary);
  CbuildSymTabBOM::setIntrinsic(leaf, intrinsic);

  // eNoTypeId and constant types need generated types created for
  // them. Constants need them mainly for bookkeeping. eNoTypeId will
  // be set to eOffsetId at some point and the generated type will be
  // used to call the creation function.
  bool doStorageTypeCalc =  ! CbuildSymTabBOM::hasMeaningfulTagId(leaf) ||
    CbuildShellDB::isConstantType(CbuildSymTabBOM::getTypeTag(leaf));
  
  if (doStorageTypeCalc)
  {
    // Compute the intrinsic for the storage net.
    const IODBIntrinsic* storeIntrinsic = sComputeIntrinsic(storageNet, mTypeDictionary);
    CbuildSymTabBOM::setIntrinsic(leaf->getStorage(), storeIntrinsic);
    
    bool isTristateNet = isTristate(storageNet);
    // We only care if it is an input when compiling for the c++ api
    bool isPrimaryInput = false;
  
    const NUMemoryNet *storeMemNet = storageNet->getMemoryNet ();
    if (storeMemNet != NULL)
      genTypeIndex = mTypeDictionary->addTypeEntry( storeIntrinsic, IODBGenTypeEntry::eNone );
    else
    {
      IODBGenTypeEntry::AttribFlags flags = IODBGenTypeEntry::eNone;
      bool isForcible = false;
      
      if (mIODB->isForcibleNet(storageNet))
      {
        flags = IODBGenTypeEntry::eForcible;
        isForcible = true;
        NU_ASSERT (! storageNet->isDoubleBuffered(), storageNet);
      }
      
      if (isBidirect(storageNet))
        flags = IODBGenTypeEntry::mergeFlags(flags, IODBGenTypeEntry::eBidirect);
      if (storageNet->isInteger() || storageNet->isSigned())
        flags = IODBGenTypeEntry::mergeFlags(flags, IODBGenTypeEntry::eSigned);
      if (storageNet->isReal())
        flags = IODBGenTypeEntry::mergeFlags(flags, IODBGenTypeEntry::eReal);
      
      isPrimaryInput = storageNet->isPrimaryInput();
      if (! isPrimaryInput)
        isPrimaryInput = isBidirect(storageNet);
      
      const STAliasedLeafNode* master =  leaf->getMaster();
      const NUNet* masterNet = NUNet::find(master);
      NU_ASSERT(masterNet, storageNet);
      
      if (! isPrimaryInput)
        // check the master
        isPrimaryInput = masterNet->isPrimaryInput();

      // Bug 8619 exposed a problem with record ports.  Consider a
      // port top.recin with a field x.  If the storage for the record
      // port is not at the top level (i.e. top.sub.$recnet_recin_x),
      // it won't be seen as a primary.  Also, the master port
      // (i.e. top.recin.x) won't be seen as a primary because it's in
      // a record scope.
      //
      // If the actual net passed in (i.e. top.$recnet_recin_x) is a
      // record port and is also a primary input, set the flag.
      if ((net->isRecordPort()) && (net->isPrimaryInput())) {
        isPrimaryInput = true;
      }
      
      bool isInternalDepositClk = false;
      if (! isPrimaryInput)
      {
        /* see if the netelab is in the changedmap and it has a
         * valid change index. If so, it is a net that is needed to
         * branch around schedules. We need
         * the value of the net to propagate immediately. So, treat
         * it as a clock.
         */
        NUNetElab* elab = NUNetElab::find(leaf);
        if (elab)
        {
          if (mChangedMap->exists(elab) &&
              (mChangedMap->getIndex(elab) != -1) &&
              mSched->isWritable(elab))
            isInternalDepositClk = true;
        }
      }
      
      // Test if the scheduler thinks this should be treated like a
      // primary input (deposit with a change array id)
      if (!isPrimaryInput && mSched != NULL) {
        NUNetElab* netElab = NUNetElab::find(leaf);
        if ((netElab != NULL) && mSched->isFrequentDepositable(netElab)) {
          isPrimaryInput = true;
        }
      }
      
      if (isInternalDepositClk || isPrimaryInput 
        || (mSched != NULL && mSched->isNUNetPrimaryClockInput(*masterNet)) ) 
        flags = IODBGenTypeEntry::mergeFlags(flags, IODBGenTypeEntry::ePrimaryInput);
      
      // Tristate is now a flag in the genTypeEntry
      if ( isTristateNet ) {
        flags = IODBGenTypeEntry::mergeFlags( flags, IODBGenTypeEntry::eTristate );
      }
      // prepare for forcibility
      IODBGenTypeEntry::AttribFlags noForceFlags = flags;
      noForceFlags = IODBGenTypeEntry::calcAttribFlagsExclude(flags, IODBGenTypeEntry::eForcible);

      UInt32 forceValueTypeIndex = 0;
      UInt32 forceMaskTypeIndex = 0;
      
      // Must add the forcible type entries first, so we can reference
      // them.
      if (isForcible)
      {
        // I. Forcemask
        //    Forcemask has no attributes and is not a tristate
        forceMaskTypeIndex = mTypeDictionary->addTypeEntry( storeIntrinsic, IODBGenTypeEntry::eNone );
        
        // II. Storage Type
        // The flags are the same as the force type except that
        // eForcible must be masked out
        forceValueTypeIndex = mTypeDictionary->addTypeEntry( storeIntrinsic, noForceFlags );
      }

      genTypeIndex = mTypeDictionary->addTypeEntry( storeIntrinsic, flags );

      if (isForcible)
      {
        // Add the intrinsic information for each alias of the value
        // and the mask of the forcible.
        // Note there are no NUNets in the value and mask nodes.

        AtomicCache* atomCache = getAtomicCache();
        for (STAliasedLeafNode::AliasLoop j(leaf); ! j.atEnd(); ++j)
        {
          STAliasedLeafNode* current = *j;

          // Get the corresponding intrinsic for the force
          // subordinate. It is the same as the current alias' intrinsic.
          const IODBIntrinsic* currentIntrinsic = CbuildSymTabBOM::getIntrinsic(current);
          NUNet* currentNet = NUNet::find(current);
          if (currentIntrinsic == NULL && currentNet != NULL)
            // We haven't seen it yet. Create it
            currentIntrinsic = sComputeIntrinsic(currentNet, mTypeDictionary);
          
          if (currentIntrinsic)
          {
            STAliasedLeafNode* valueLeaf = sFetchForceValueLeaf(current, 
                                                                atomCache,
                                                                symtab);
            
            STAliasedLeafNode* maskLeaf = sFetchForceMaskLeaf(current, 
                                                              atomCache,
                                                              symtab);
            
            CbuildSymTabBOM::setIntrinsic(valueLeaf, currentIntrinsic);
            CbuildSymTabBOM::setIntrinsic(maskLeaf, currentIntrinsic);
            CbuildSymTabBOM::setTypeIndex(valueLeaf, forceValueTypeIndex);
            CbuildSymTabBOM::setTypeIndex(maskLeaf, forceMaskTypeIndex);
          }
        }
      }
    }
    CbuildSymTabBOM::setTypeIndex(leaf->getStorage(), UInt32(genTypeIndex));
    
    if (intrinsic->getType() == IODBIntrinsic::eMemory)
    {
      UInt32 bit_size = (intrinsic->getWidth() * 
                         intrinsic->getMemAddrRange()->getLength());
      if (isSparseMemoryBitSize(bit_size))
        mGeneratingSparseMem = true;
      else
        mGeneratingMem = true;
    }
    
    if (intrinsic->getWidth() > LLONG_BIT)
      mGeneratingBVs = true;
  }
} // CodeGen::AddTypeToDictionary

/*!
 * Generate the debug shell type from the first character of
 * \a typestring.
 * \retval a class name
 */
static const char* sStorageName(bool isTristate,  
                                const IODBGenTypeEntry* te, size_t width, 
                                UtString* buf)
{
  buf->clear();

  emitUtil::getGeneratedType(buf, isTristate, te->isBidirect(), 
                             te->isSigned(), width, false, te->isReal());
  return buf->c_str();
}

static const char* sForcibleMaskStorageName(size_t width, 
                                           UtString* buf)
{
  buf->clear();

  emitUtil::getGeneratedType(buf, false, false, 
                             false, width, false, false);
  return buf->c_str();
}

static const char* sGetPrim(size_t width, bool isSigned)
{
  if (! isSigned)
  {
    if (width <= 8)
      return "UInt8";
    else if (width <= 16)
      return "UInt16";
    else if ((width > 32) && (width <= 64))
      return "UInt64";
    else
      return "UInt32";
  }
  else
  {
    if (width <= 8)
      return "SInt8";
    else if (width <= 16)
      return "SInt16";
    else if (width <= 32)
      return "SInt32";
    else if (width <= 64)
      return "SInt64";
    else
      return "UInt32"; // bitvector
  }
  return NULL;
}

/*!
 * Generate the debug shell type from the first character of
 * \a typestring.
 * \retval a class name
 */
static const char* sTypeName(IODBIntrinsic::Type t, 
                             bool tristate, 
                             bool isSparseMem,
                             bool isSigned,
                             size_t width, 
                             UtString* buffer)
{
  buffer->clear();

  bool isVec = false;
  bool isMem = false;
  switch (t)
  {
  case IODBIntrinsic::eScalar: 
    if ( tristate == true ) {
      *buffer << CRYPT("CarbonTristateScalar"); 
    }
    else {
      *buffer << CRYPT("CarbonScalar");
    }
    break;
  case IODBIntrinsic::eVector: 
    isVec = true;
    if ( tristate == true ) {
      *buffer << CRYPT("CarbonTristateVector"); 
    }
    else {
      *buffer << CRYPT("CarbonVector");
    }
    break;
  case IODBIntrinsic::eMemory:
    isMem= true;
    if ((width <= LLONG_BIT) && isSparseMem)
      *buffer << CRYPT("ShellSparseMemory32x");
    else
      *buffer << CRYPT("ShellMemory64x");
    break;
  }

  if (isVec)
  {
    const size_t byteBits = sizeof(UInt8) * 8;
    size_t numBytes = (width + byteBits - 1)/byteBits;
    if (numBytes <= 8)
    {
      if (numBytes <= 2)
        *buffer << numBytes;
      else if (numBytes <= 4)
        *buffer << 4;
      else 
        *buffer << 8;
    }
    else
    {
      *buffer << "A";
      // the only signed type in the shell is CarbonVectorAS. That is
      // the only one that deals with BitVectors. All other types are
      // extended in the generated code.
      if (isSigned)
        *buffer << "S";
    }
  }
  else if (isMem)
  {
    if (width <= LLONG_BIT)
    {
      if (width <= 8)
        *buffer << "8";
      else if (width <= 16)
        *buffer << "16";
      else if (width <= 32)
        *buffer << "32";
      else if (width <= LLONG_BIT)
        *buffer << "64";
    }
    else
      *buffer << "A";
  }
  
  return buffer->c_str();
}

//! Centralized type include code
void CodeGen::writeDictionaryInclude(const char* headerRoot)
{
  UtOStream &schedSrc = gCodeGen->getCGOFiles()->getScheduleSrc ();
  schedSrc << "#include \"" << headerRoot << "\"\n";
}

//! This simply figures out which include files to add for types
void CodeGen::compileDictionaryIncludes (void)
{
  writeDictionaryInclude(cShellNetCreate_h_path.c_str());
  if (mGeneratingBVs)
    writeDictionaryInclude(cBitVector_h_path.c_str());
  if (mGeneratingMem)
    writeDictionaryInclude(cMemory_h_path.c_str());
  if (mGeneratingSparseMem)
    writeDictionaryInclude(cSparseMemory_h_path.c_str());
  if (mGeneratingMem || mGeneratingSparseMem)
    writeDictionaryInclude(cMemoryCreateInfo_h_path.c_str());
}


//! Write a C++ file containing the symbol-table maps
/*! The dictionary is written into the schedule.cxx file as a series
 *  of generator functions returning an object of the appropriate type
 *  derived from CarbonNet.  A pointer to the each generator is stored
 *  in the GeneratorTable[]. The SlotID for the symbol type is used
 *  to index into this generator table to construct a runtime debug
 *  symbol accessor.
 */
void CodeGen::compileDictionary (void)
{
  // generate the change array->storage mapping fn
  genChgArrToStoreFn();
  
  // Allocate a vector of UtString* large enough to hold the unique
  // types.

  // Now emit the symboltable
  //

  compileDictionaryIncludes();
  // Emit a constructor for each arch-type.
  /*
    To tightly integrate type index with debug_generator function
    name, I decided to *not* use the loopTypes() method here. Instead,
    I'm using the explicit getTypeEntry for both clarity and somewhat
    simpler looking code, albeit maybe not quite as efficient. - MS
  */
  const char* cShellNet = CRYPT("ShellNet");
  const char* cForcible = CRYPT("Forcible");
  const char* cForciblePOD = CRYPT ("ForciblePOD");
  const char* cCarbonForceNet = CRYPT("CarbonForceNet");
  const char* cGetStorage = CRYPT("getStorage");
  const char* cGetForceMask = CRYPT("getForceMask");
  const char* cForceMask = CRYPT("forceMask");
  const char* cStorage = CRYPT("storage");
  const char* cMemory = CRYPT("Memory");
  const char* cSparseMemory = CRYPT("SparseMemory");
  const char* cCarbon_hashtable = CRYPT("void"); // because UtHashMapFastIter is templatized
  const char* cTableVar = CRYPT("table");
  const char* cGetHashTable = CRYPT("getHashTable");

  SInt32 numTypes = mTypeDictionary->numGenTypes();
  const char* idata = CRYPT("idata");
  const char* idrive = CRYPT("idrive");

  const char* xdata = CRYPT("xdata");
  const char* xdrive = CRYPT("xdrive");

  const char* primBidiParams = CRYPT("idata, idrive, xdata, xdrive");
  const char* vecTriParams = CRYPT("idata, idrive");
  const char* primTriParams = CRYPT("idata, idrive");

  const char* getInternalPtrs = CRYPT("getInternalPtrs");
  const char* getUIntArray = CRYPT("getUIntArray");
  const char* isScalarVar = CRYPT("isScalar");
  const char* shlRetVar = CRYPT("shlRet");
  const char* initSection = CRYPT ("carbon_infrequent_code");
  const char* createSuffixCrypt = CRYPT("Create");

  const UInt32 cMemFunctionVersion = 2;

  UtOStream &schedSrc = gCodeGen->getCGOFiles()->getScheduleSrc ();

  // Emit forward declarations for the gen functions so that we don't
  // have to order them correctly. This was found by emc m9. It used a
  // gen function before it was defined.
  for (SInt32 genNum = 0; genNum < numTypes; ++genNum) {
    schedSrc << "static " << cShellNet << " * gen_" << genNum
                  << "(void *addr, bool isScalar)";
      
    emitSection (schedSrc, initSection);
    schedSrc << ";\n";
  }

  // Now we can safely emit the gen functions in any order
  for (SInt32 genNum = 0; genNum < numTypes; ++genNum)
  {
    const IODBGenTypeEntry* te = mTypeDictionary->getGenTypeEntry(genNum);
    const IODBIntrinsic *intrinsic = te->getIntrinsic();
    const IODBIntrinsic::Type type = intrinsic->getType();
    size_t width = intrinsic->getWidth();
    bool oneByteSpecialCase = false;
    schedSrc << "static " << cShellNet << " * gen_" << genNum
                  << "(void *addr, bool ";
    if ((width <= 8) && (type != IODBIntrinsic::eMemory))
    {
      schedSrc << isScalarVar;
      oneByteSpecialCase = true;
    }

    bool isLongVec = (width > LLONG_BIT);
    

    schedSrc << ") {\n";
    schedSrc << "  " << cShellNet << "* " << shlRetVar << " = NULL;\n";      
    bool isTristate = te->isTristate();
    bool isSigned = te->isSigned();

    // Signed tristates have to be given the proper SInt when calling
    // getInternalPtrs. Therefore, we need extern C functions to deal
    // with those. You can't overload in strict C, so the signed
    // versions have an S before Create. Long vectors, however, always
    // have UInt32 array storage only.
    UtString createSuffix;
    if (! isLongVec && isSigned && isTristate)
      createSuffix << "S";
    createSuffix << createSuffixCrypt;
    
    if (te->isForcible())
    {
      SInt32 forceMaskTI = -1;
      SInt32 storageTI = -1;

      IODBGenTypeEntry::AttribFlags noForceFlags = te->getAttribFlagsExclude(IODBGenTypeEntry::eForcible);
      if ( isTristate ) {
        noForceFlags = IODBGenTypeEntry::mergeFlags( noForceFlags,
                                                     IODBGenTypeEntry::eTristate );
      }
      switch(type)
      {
      case IODBIntrinsic::eScalar:
        forceMaskTI = mTypeDictionary->findGenTypeIndexScalar( IODBGenTypeEntry::eNone );
        storageTI = mTypeDictionary->findGenTypeIndexScalar( noForceFlags );
        break;
      case IODBIntrinsic::eVector:
        forceMaskTI = mTypeDictionary->findGenTypeIndexVector( intrinsic, IODBGenTypeEntry::eNone );
        storageTI = mTypeDictionary->findGenTypeIndexVector( intrinsic, noForceFlags );
        break;
      case IODBIntrinsic::eMemory:
        // Can't force memories
        FUNC_ASSERT(type != IODBIntrinsic::eMemory, intrinsic->print());
        break;
      }
      FUNC_ASSERT (forceMaskTI != -1, intrinsic->print());
      FUNC_ASSERT (storageTI != -1, intrinsic->print());
      UtString buf;
      UtString forcibleDecl;

      const char *cForceClass = (isTristate || width > LLONG_BIT) ? cForcible
	: cForciblePOD;
	
      forcibleDecl << cForceClass << "<" << sStorageName(isTristate, te, width, &buf);
      if (isTristate || width > LLONG_BIT)
	forcibleDecl << ", " << sForcibleMaskStorageName(width, &buf);
      forcibleDecl << ">*";

      schedSrc << "  " << forcibleDecl << " r = static_cast<"
		    << forcibleDecl << ">(addr);\n";

      schedSrc << "  " << cShellNet << "* " << cForceMask << " = gen_" << forceMaskTI << "(r->" << cGetForceMask << "(), ";
      if (oneByteSpecialCase)
        schedSrc << isScalarVar;
      else
        schedSrc << "false";
      schedSrc << ");\n";

      schedSrc << "  " << cShellNet << "* " << cStorage << " = gen_" << storageTI << "(r->" << cGetStorage << "(), ";
      if (oneByteSpecialCase)
        schedSrc << isScalarVar;
      else
        schedSrc << "false";

      schedSrc << ");\n";

      schedSrc << "  " << shlRetVar << " = " << cCarbonForceNet << createSuffix << "(" << cStorage << ", " << cForceMask << ");\n";
    }
    else
    {
      bool isBidirect = te->isBidirect();
      bool isMemory = type == IODBIntrinsic::eMemory;
      UtString shellTypeBuf;

      const char* inputType = "";

      if (te->isPrimaryInput())
      {
        if (isTristate && ! isBidirect)
          // depositable signal marked as a tristate
          inputType = "DepInput";
        else
        {
          inputType = "Input";
        }
      }
      
      UtString typeBuf;
      sStorageName(isTristate, te, width, &typeBuf);
      const char* typeVar = "genType";

      if (!isMemory)
        schedSrc << "  " << typeBuf << "* " << typeVar << " = static_cast<" << typeBuf << "*>(addr);\n";
      
      const char* primType = sGetPrim(width, false);
      const char* paramList = typeVar;
      
      bool isSparseMem = false;
      
      if (isTristate)
      {
        const char* primTypeWithSign = sGetPrim(width, isSigned);
        schedSrc << "  " << primTypeWithSign << " *" << idata << ";\n";
        schedSrc << "  " << primTypeWithSign << " *" << idrive << ";\n";

        if (isBidirect)
        {
          schedSrc << "  " << primTypeWithSign << " *" << xdata << ";\n";
          schedSrc << "  " << primTypeWithSign << " *" << xdrive << ";\n";
          schedSrc << "  " << typeVar << "->" << getInternalPtrs << "(&" << idata << ", &" << idrive << ", &" << xdata << ", &" << xdrive << ");\n";

          paramList = primBidiParams;

          if (oneByteSpecialCase)
          {
            schedSrc << "  if (" << isScalarVar <<  ")\n";
            schedSrc << "    " << shlRetVar << " = "
                     << sTypeName(IODBIntrinsic::eScalar, isTristate, false, false, width, &shellTypeBuf)
                     << inputType << createSuffix 
                     << "(" << paramList << ");\n";
            schedSrc << "  else\n";
            schedSrc << "    " << shlRetVar << " = "
                     << sTypeName(IODBIntrinsic::eVector, isTristate, false, isSigned, width, &shellTypeBuf)
                     << inputType << createSuffix
                     << "(" << paramList << ");\n";
          }
          else
            schedSrc << "  " << shlRetVar << " = "
                     << sTypeName(type, isTristate, false, isSigned, width, &shellTypeBuf)
                     << inputType << createSuffix 
                     << "(" << paramList << ");\n";
            
        }
        else
        {
          schedSrc << "  " << typeVar << "->" << getInternalPtrs << "(&" << idata << ", &" << idrive << ");\n";

          if (isLongVec)
            paramList = vecTriParams;
          else
            paramList = primTriParams;
          
          if (oneByteSpecialCase)
          {
            schedSrc << "  if (" << isScalarVar <<  ")\n";
            schedSrc << "    " << shlRetVar << " = "
                     << sTypeName(IODBIntrinsic::eScalar, isTristate, false, false, width, &shellTypeBuf)
                     << inputType << createSuffix 
                     << "(" << paramList << ");\n";
            schedSrc << "  else\n";
            schedSrc << "    " << shlRetVar << " = "
                     << sTypeName(IODBIntrinsic::eVector, isTristate, false, isSigned, width, &shellTypeBuf)
                     << inputType << createSuffix
                     << "(" << paramList << ");\n";
          }
          else
            schedSrc << "  " << shlRetVar << " = "
                     << sTypeName(type, isTristate, false, isSigned, width, &shellTypeBuf)
                     << inputType << createSuffix
                     << "(" << paramList << ");\n";
        }
      } // if tristate
      else if (!isMemory)
      {
        const char* vecVar = "vec";
        if (isLongVec)
        {
          schedSrc << "  UInt32* " << vecVar << " = " 
                        << typeVar << "->" << getUIntArray << "();\n";
          paramList = vecVar;
        } 
        
        if (oneByteSpecialCase)
        {
          schedSrc << "  if (" << isScalarVar <<  ")\n";
          schedSrc << "    " << shlRetVar << " = "
                   << sTypeName(IODBIntrinsic::eScalar, isTristate, false, false, width, &shellTypeBuf)
                   << inputType << createSuffix 
                   << "( (" << primType << "*) " << paramList << ");\n";
          schedSrc << "  else\n";
          schedSrc << "    " << shlRetVar << " = "
                   << sTypeName(IODBIntrinsic::eVector, isTristate, false, isSigned, width, &shellTypeBuf)
                   << inputType << createSuffix
                   << "( (" << primType << "*) " << paramList << ");\n";
        }
        else
          schedSrc << "  " << shlRetVar << " = "
                   << sTypeName(type, isTristate, false, isSigned, width, &shellTypeBuf)
                   << inputType << createSuffix 
                   << "( (" << primType << "*) " << paramList << ");\n";
        
      } // else if not isMemory
      else
      {
        // we have a memory

        // Add the intrinsic information to the memory closure
        schedSrc << "  ShellMemoryCreateInfo memClosure;\n"
                 << "  memClosure.putMsb(" << intrinsic->getMsb() << ");\n"
                 << "  memClosure.putLsb(" << intrinsic->getLsb() << ");\n"
                 << "  memClosure.putLeftAddr(" << intrinsic->getHighAddr() << ");\n"
                 << "  memClosure.putRightAddr(" << intrinsic->getLowAddr() << ");\n";
        
        UInt32 bit_size = (intrinsic->getWidth() * 
                           intrinsic->getMemAddrRange()->getLength());
        isSparseMem = ( gCodeGen->isSparseMemoryBitSize(bit_size) );
        if (! isSparseMem || (width > LLONG_BIT))
        {
          UtString memprimbuf;

          schedSrc << "  typedef ";
          if (isSparseMem)
            schedSrc << cSparseMemory;
          else
            schedSrc << cMemory;
          
          schedSrc << "<" << typeBuf << ", " << width << "," << intrinsic->getHighAddr() << "," << intrinsic->getLowAddr() << "> MemT;\n";
          schedSrc << "  MemT*" << typeVar << " = static_cast<MemT*>(addr);\n";
          
          // Does this memory need an onDemand callback?
          if (isOnDemand()) {
            SInt32 ondemand_mem_threshold;
            gCodeGen->getCarbonContext()->getArgs()->getIntLast(CRYPT("-onDemandMemoryStateLimit"), &ondemand_mem_threshold);
            if (bit_size > static_cast<UInt32>(ondemand_mem_threshold)) {
              // This memory is larger than the threshold for saving itself as part of
              // onDemand state, so it needs a callback instead.
              schedSrc << "  " << typeVar << "->getCallbackArray()->putNeedsOnDemandCB();\n";
            }
          }
          
          // Add the callback array to the closure
          schedSrc << "  memClosure.putMemCBArray(" << typeVar <<"->getCallbackArray());\n";
          
          if (width <= LLONG_BIT)
          {
            memprimbuf << sGetPrim(width, false) << "*";
            schedSrc << "  " << memprimbuf << " genTypeMem = ";
            schedSrc << typeVar << "-> " << cGetStorage << "();\n";
          }
          else
          {
            // must use the memory static functions to read/write due
            // to padding on BitVectors
            schedSrc << "  memClosure.putReadRowFn(" << typeVar << "->getReadRowFn());\n"
                     << "  memClosure.putWriteRowFn(" << typeVar << "->getWriteRowFn());\n"
                     << "  memClosure.putReadRowWordFn(" << typeVar << "->getReadRowWordFn());\n"
                     << "  memClosure.putWriteRowWordFn(" << typeVar << "->getWriteRowWordFn());\n"
                     << "  memClosure.putReadRowRangeFn(" << typeVar << "->getReadRowRangeFn());\n"
                     << "  memClosure.putWriteRowRangeFn(" << typeVar << "->getWriteRowRangeFn());\n"
                     << "  memClosure.putRowStoragePtrFn(" << typeVar << "->getRowStoragePtrFn());\n";
          }
          
          
          schedSrc << "  " << shlRetVar << " = Carbon"
                   << sTypeName(type, isTristate, isSparseMem, false, width, &shellTypeBuf)
                   << createSuffix << cMemFunctionVersion;
          
          schedSrc << "(&memClosure, ";
          
          if (width <= LLONG_BIT)
            schedSrc << "genTypeMem";
          else
            schedSrc << typeVar;
          schedSrc <<");\n";
        }
        else
        {
          schedSrc << "  typedef "
                   << cSparseMemory << "<" << typeBuf << ", " << width << "," << intrinsic->getHighAddr() << "," << intrinsic->getLowAddr() << "> MemT;\n";
          schedSrc << "  " << "MemT* " << typeVar << " = static_cast<MemT*>(addr);\n";

          // Sparse memories always need onDemand callbacks
          if (isOnDemand()) {
            schedSrc << "  " << typeVar << "->getCallbackArray()->putNeedsOnDemandCB();\n";
          }

          // Add the callback array to the closure
          schedSrc << "  memClosure.putMemCBArray(" << typeVar <<"->getCallbackArray());\n";
          
          schedSrc << "  " << cCarbon_hashtable << "* " << cTableVar << " = " << typeVar << "->" << cGetHashTable << "();\n";

          schedSrc << "  " << shlRetVar << " = Carbon"
                   << sTypeName(type, isTristate, isSparseMem, false, width, &shellTypeBuf)
                   << createSuffix << cMemFunctionVersion;

          schedSrc << "(&memClosure, " << cTableVar << ");\n;";
        }
      }
    } // else if not forcible
    schedSrc << "  return " << shlRetVar << ";\n}\n\n";
  } // for
  
  // Emit the vector of instances...
  // (Handle empty module (with no types) carefully because of GCC 3.4.3 bug
  // where zero-length static array with empty initializer segfaults compiler.
  //
  if (numTypes != 0)
  {
    schedSrc << CRYPT("static const CarbonNetGenerator GeneratorTable[")
             << numTypes <<"] = {";

    for(SInt32 k=0; k<numTypes;++k)
      schedSrc << ((k>0)? ",\n": " \n") << "  gen_" << k;

    schedSrc << "\n};\n\n";
  }

  schedSrc << "static " << cShellNet << " *"
           << CRYPT("carbon_debug_generator(UInt32 i, void* storage, int ") 
           << isScalarVar << ")\n{\n";

  if (numTypes == 0)
    schedSrc << "(void)i, (void)storage, (void)" << isScalarVar << ";\n  return 0;\n";
  else
    schedSrc << CRYPT("  return (GeneratorTable[i])(storage, ") 
             << isScalarVar << ");\n";
  schedSrc << "}\n\n";
} // void CodeGen::compileDictionary

void
CodeGen::genChgArrToStoreFn()
{
  UtOStream &schedSrc = gCodeGen->getCGOFiles()->getScheduleSrc ();
  schedSrc << "static void " << getNamespace () << "::"
		<< cChangeMapFnName
                << CRYPT("(carbon_model_descr *descr)\n")
                << "{\n"
                << CRYPT("  carbonPrivateBeginChangeIndToStorageMap(descr);\n");
  
  const char* cHookupMapChangeIndToStorage = CRYPT("carbonPrivateMapChangeIndToStorage");
  for (CGNetElabCount::NetElabMapIter p = mChangedMap->loopNetElabs();
       ! p.atEnd(); ++p) 
    { 
      const NUNetElab* ne = p.getKey();
      SInt32 index = p.getValue();
      if (index != -1)
      {
        const STAliasedLeafNode* leaf = ne->getSymNode();
        NU_ASSERT(leaf, ne);
        SInt32 offset = CbuildSymTabBOM::getStorageOffset(leaf);
        if (offset != -1) {
          schedSrc << "  " << cHookupMapChangeIndToStorage << "(descr, "
                        << index << ", " << offset << ");";
          // write name to comment
          const HdlHierPath* pather = 
            mCarbonContext->getSymbolTable()->getHdlHier();
          UtString sigName;
          pather->compPathHier(leaf, &sigName);
          // and put a non space character at the end of line in case
          // name was escaped (test/langcov/escape_name_1.v)
          schedSrc << " // " << sigName << " .\n";
        }
      }
    }
  
  schedSrc << CRYPT("  carbonPrivateEndChangeIndToStorageMap(descr);\n}\n\n");
}

//! Iterator helper function for debug symbol table slots
//! Walk one module instance, setting the offsets and type IDs of
//! each symbol in the symbol table, relative to the current offset
//! where this instance is allocated.
struct FillSlots {
  //! Symbol table hierarchy
  STBranchNode *mHier;

  //! Remembered offset where this module begins relative to top-level class
  size_t	moff;

  //! Trivial constructor
  FillSlots (STBranchNode *hier, size_t off) : mHier (hier), moff (off) {}

  //! iterator action function
  void operator()(const NUNet *n) {
    STBranchNode* Hier;
    if (n->isBlockLocal ()) {
      if (n->isNonStatic ())
	return;
      // n might have been opt. away, don't bother.
      if (!n->getCGOp ())
	return;
      const STBranchNode* hier;
      hier = NUElabHierHelper::resolveElabHierForNet(n, mHier);
      Hier = const_cast<STBranchNode*>(hier);
    }
    else {
      Hier = mHier;
    }
    NU_ASSERT(Hier, n);
    STSymbolTableNode* nNode = Hier->getChild(n->getSymtabIndex());
    NU_ASSERT(nNode, n);
    STAliasedLeafNode* nLeaf = nNode->castLeaf();
    NU_ASSERT(nLeaf, n);
    NUNet* leafNet = NUNet::find(nLeaf);

    // Note that the leafNet will be NULL if it is dead logic.  Otherwise
    // It should match the net.
    NU_ASSERT((leafNet == NULL) || (leafNet == n), n);
    NU_ASSERT(nLeaf->strObject() == n->getName(), n);

    // type is independent of aliasing (one bit vector could be
    // aliased to a scalar or same sized vectors with different ranges
    // could be aliased together...)
    
    // Be sure we want to update the offset for this node.  Aliases
    // aren't valid - only Primaries and storage nodes have record
    // offsets.
    //

    if (!n->getCGOp()->isVCDSymbol())
    {
      if (! CbuildSymTabBOM::isTypeTagValid(nLeaf))
        CbuildSymTabBOM::setTypeTag(nLeaf, CbuildShellDB::eNoTypeId);
      return;
    }
    
    size_t symbolOffset = moff + n->getCGOp ()->getOffset ();
    // only care about storage nodes
    if (nLeaf->getStorage() == nLeaf)
    {
      if (! CbuildSymTabBOM::isConstantTagType(nLeaf))
      {
        CbuildSymTabBOM::setTypeTag(nLeaf, CbuildShellDB::eOffsetId);
        CbuildSymTabBOM::setStorageOffset(nLeaf, SInt32(symbolOffset));
      }
    }

    // If onDemand is enabled, allocate/record this node's offset
    // into the onDemand state buffer.  Since this function is
    // called on a list of nets that have been sorted by address
    // layout, we'll allocate the onDemand storage in the correct
    // order.
    //
    // Note that this is done for all nets that are emitted as part of
    // the OnDemand state, regardless of whether they've been marked
    // constant or aliased.  Constant/aliased nets are still part of
    // the OnDemand saved state, since that's done on a per-module
    // basis, and not all instances of the net may be
    // constant/aliased.
    if (gCodeGen->isOnDemand() && emitUtil::isOnDemandStateNet(n)) {
      UInt32 offset = gCodeGen->onDemandStateAllocation(emitUtil::onDemandStateAllocation(n));

      // However, it's bad if we attempt to save non-storage nodes in
      // the db.  If there is no storage node, don't do anything.
      // While the net will be saved to the OnDemand state buffer, its
      // value will be constant, and therefore will never cause an
      // idle search to fail.  There is a scheduler sanity check to
      // make sure that only nets with storage are scheduled.
      STAliasedLeafNode *storage = nLeaf->getInternalStorage();
      if (storage) {
        gCodeGen->getIODB()->putOnDemandStateOffset(storage, offset);
      }
    }

    // emit debug support code - test that C compiler thinks members
    // are allocated at the same offset we compute and store in the
    // carbon debug information.
    //
    sCheckDebugOffset (nNode, symbolOffset);
  }
};

static size_t
setDebugSlots (NUModuleElab *me, size_t offset)
{
  STBranchNode *hier = me->getHier ();
  NUModule *m = me->getModule ();

  if (not m->atTopLevel ())
    // Don't try and debug-check the top level...
    sCheckDebugOffset (hier, offset);

  NUNetList netlist;
  m->getAllTopNets (&netlist);
  m->getAllNestedNets(&netlist);

  NUNetVector netvec (netlist.begin (), netlist.end ());

  emitUtil::sort_by_address addressSorter;
  std::sort (netvec.begin (), netvec.end (), addressSorter);

  // For each symbol, get its offset relative to the elaborations
  // offset as well as its TypeId and store them in the symboltable
  //
  std::for_each (netvec.begin (), netvec.end (),
                 FillSlots (hier,offset));
#ifdef INTERPRETER
  gCodeGen->putModuleOffset(hier, offset);
#endif

  // Advance the offset to reflect all storage allocated
  // to this point.
  offset += m->getClassDataSize ();

  return offset;
}

static size_t
walkModuleElab (NUModuleElab *elab, size_t base_offset)
{
  STBranchNode *hier = elab->getHier ();
  const NUModule *m = elab->getModule ();

  // This module is assumed to begin at a certain alignment.  Make it so.
  // 
  size_t mask = m->getClassDataAlignment () - 1;
  size_t start_offset = (base_offset + mask) & ~mask;

  // For a given module, set the slots for its members
  size_t accum_offset = setDebugSlots (elab, start_offset);

  // Now assign the values for modules instantiated by this module.
  // We must sort the instances by alignment, since that's how
  // codegen emitted them.
  //
  NUModuleInstanceMultiCLoop insts (m->loopInstances ());
  UtVector<const NUModuleInstance*> sortedInstances (insts.begin (), insts.end ());
  std::stable_sort (sortedInstances.begin (), sortedInstances.end (),
		    emitUtil::sort_by_align ());

  for (UtVector<const NUModuleInstance*>::iterator i = sortedInstances.begin ();
       i != sortedInstances.end ();
       ++i)
    accum_offset = walkModuleElab ((*i)->lookupElab (hier), accum_offset);

  // Get the final offset from the module, because there may be extra padding
  // added (for instance if the module is empty), so the calculated offset
  // at this point may be incorrect.
  size_t final_offset = start_offset + m->getClassInstDataSize();

  // Sanity check to make sure that the final offset pads out to the class
  // alignment.
  NU_ASSERT((final_offset & mask) == 0, elab);

  return final_offset;
}

static void sSetupForceSubordinate(STAliasedLeafNode* subord, 
                                   const STAliasedLeafNode* forcible,
                                   IODBNucleus* iodb)
{
  CbuildSymTabBOM::copyLeafData(forcible, subord);
  CbuildSymTabBOM::putNucleusObj(subord, NULL);
  // Make sure we cannot explicitly get these from
  // CarbonHookup::getCarbonNet(). We don't maintain the offsets into
  // forcibles for the mask and value.
  CbuildSymTabBOM::setStorageOffset(subord, -1);
  
  CbuildSymTabBOM::markForceSubordinate(subord);
  // setup the subordinate as both observable and depositable
  // Note: These markings are meaningless if the master net is not
  // depositable.
  CbuildSymTabBOM::markDepositableNet(subord);
  CbuildSymTabBOM::markObservableNet(subord);

  iodb->mapForceSubordinate(subord, const_cast<STAliasedLeafNode*>(forcible));
}

static STAliasedLeafNode* sCreateForceValueLeaf(STAliasedLeafNode* 
                                                forcible,
                                                AtomicCache* atomCache,
                                                STSymbolTable* symtab)
{
  StringAtom* valueName = IODB::sGetForceValueName(forcible, atomCache);
  return symtab->createLeaf(valueName,
                            forcible->getParent());
  
}

static STAliasedLeafNode* sCreateForceMaskLeaf(STAliasedLeafNode* 
                                               forcible,
                                               AtomicCache* atomCache,
                                               STSymbolTable* symtab)
{
  StringAtom* valueName = IODB::sGetForceMaskName(forcible, atomCache);
  return symtab->createLeaf(valueName,
                            forcible->getParent());
  
}

void CodeGen::updateTypeInformationFromSymtab(STSymbolTable * symtab)
{
  STFieldBOM* fieldBOM = symtab->getFieldBOM();
  CbuildSymTabBOM* symTabBOM = dynamic_cast<CbuildSymTabBOM*>(fieldBOM);

  if (symTabBOM == NULL){
    mMsgContext->CGIncompatibleSymTab();
    abort ();
  }
  
  // Update the symboltable to include force value and mask nets.
  typedef UtArray<STAliasedLeafNode*> LeafVector;

  // Grab all the forcible storage nets and master nets in the design.
  // Needed for aliasing the value and mask subordinates
  LeafVector forcibleStorage;
  LeafVector forcibleMaster;
  for (STSymbolTable::NodeLoop i = symtab->getNodeLoop();
       not i.atEnd(); 
       ++i) {
    STSymbolTableNode* node = *i;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf && (leaf->getStorage() == leaf)) {
      NUNet* storageNet = NUNet::find(leaf);
      if (mIODB->isForcibleNet(storageNet))
      {
        forcibleStorage.push_back(leaf);
        forcibleMaster.push_back(leaf->getMaster());
      }
    }
  }

  AtomicCache* atomCache = getAtomicCache();
  // Run through the forcible storage nets, create the value and mask
  // nets using those as storage nets for the rest of the forcible ring.
  for (LeafVector::iterator j = forcibleStorage.begin(), je = forcibleStorage.end();
       j != je; ++j)
  {
    STAliasedLeafNode* forcible = *j;
    STAliasedLeafNode* valueStore = sCreateForceValueLeaf(forcible, atomCache, symtab);
    STAliasedLeafNode* maskStore = sCreateForceMaskLeaf(forcible, atomCache, symtab);

    valueStore->setThisStorage();
    maskStore->setThisStorage();

    // Update the iodb to allow for these nets to be written to
    // the database.
    sSetupForceSubordinate(valueStore, forcible, mIODB);
    sSetupForceSubordinate(maskStore, forcible, mIODB);

    // Run through the aliases of the forcible, create leaves for the
    // value and mask of each and alias them to the storage nodes we
    // just created.
    for (STAliasedLeafNode::AliasLoop p(forcible); !p.atEnd(); ++p)
    {
      STAliasedLeafNode* current = *p;
      if (current != forcible)
      {
        STAliasedLeafNode* value = sCreateForceValueLeaf(current, atomCache, symtab);
        STAliasedLeafNode* mask = sCreateForceMaskLeaf(current, atomCache, symtab);

        valueStore->linkAlias(value);
        maskStore->linkAlias(mask);

        // Update the iodb to allow for these nets to be written to
        // the database.
        sSetupForceSubordinate(value, current, mIODB);
        sSetupForceSubordinate(mask, current, mIODB);
      }
    }
  }
  

  // Run through the masters of the forcibles and set the values and
  // masks to be their alias rings' masters.
  for (LeafVector::iterator j = forcibleMaster.begin(), je = forcibleMaster.end();
       j != je; ++j)
  {
    STAliasedLeafNode* masterLeaf = *j;
    STAliasedLeafNode* masterValue = sFetchForceValueLeaf(masterLeaf, 
                                                          atomCache,
                                                          symtab);
    
    STAliasedLeafNode* masterMask = sFetchForceMaskLeaf(masterLeaf, 
                                                        atomCache,
                                                        symtab);
    // Set the alias rings
    masterValue->setThisMaster();
    masterMask->setThisMaster();
  }

  // Now run through the symbol table (sorted because we want
  // generated type information to be created in a repeatable order).
  for (STSymbolTable::NodeLoopSorted i = symtab->getNodeLoopSorted();
       not i.atEnd(); 
       ++i) {
    STSymbolTableNode* node = *i;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf) {
      NUNet* net = NUNet::find(leaf);
      bool forceInclude = 
        mIODB->hasConstantOverride(node) || 
        symTabBOM->isExprSubordinate(node) ||
        symTabBOM->isExprMappedNode(node);
      
      bool netIsGood = (net!=NULL);
      
      if (forceInclude || netIsGood) {
	if (not net->getCGOp()) {
	  net->setCGOp(new CGAuxInfo);
	}

        NUNetElab* net_elab = NUNetElab::find(node);
        if (net_elab)
        {
          // If this node is sample scheduled, mark the storage of the
          // alias ring that it needs to cause a debug schedule run
          // during waveform dumps.
          bool isSampled = false;
          for (NUNetElab::DriverLoop l = net_elab->loopContinuousDrivers();
               !l.atEnd() && !isSampled;
               ++l) {
            FLNodeElab* flowElab = *l;
            isSampled = flowElab->isSampled();
          }
          if (isSampled)
            symTabBOM->markRunDebugSchedule(leaf);
        }

        STAliasedLeafNode* storage = leaf->getStorage();
        if (forceInclude || isIncludedInTypeDictionary(leaf, mIODB))
        {
          
          AddTypeToDictionary(net,
                              NUNet::find(storage),
                              leaf, symtab);
        }
      }
    }
  }
}

// Walk the elaborated Nets, filling in the symbol-table slots.
//
void CodeGen::decorateSymbolTable (void)
{
  // Verify that debug info is correct

  CodeGen::CxxName vclass ("", "");
  UtOStream &schedSrc = gCodeGen->getCGOFiles()->getScheduleSrc ();

  schedSrc << "static void check_off(UInt32 act, UInt32 exp) UNUSED ";
  emitSection (schedSrc, CRYPT ("carbon_infrequent_code"));
  schedSrc << ";\nstatic void check_off (UInt32 act, UInt32 exp)\n"
                << "{\n"
                << "  if (exp != act) {\n"
                << "    fprintf(stderr,\"OffsetSanity: expected offset (%x) actual offset (%x)\\n\", exp, act);\n"
                << "    assert(0);\n  }\n}\n\n";             // this_assert_OK

  CGOUT () << CRYPT("static void ") << getNamespace ()
	   << CRYPT("::carbon_check_debug_info(carbon_simulation& ")
	   << vclass (mTopName) << ")\n"
	   << "{\n";

  CGOUT () << "  char *p UNUSED = (char *)(&"
	   << vclass (mTopName) << ");\n";


  STSymbolTable* symtab = mCarbonContext->getSymbolTable();

  // Assign relative offsets (starting at zero) to all allocated
  // objects in the symbol table.
  //

  walkModuleElab (mTopModule->lookupElab (symtab), 0);

  // If onDemand is enabled, record the total onDemand allocation size.
  if (isOnDemand()) {
    mIODB->putOnDemandStateSize(onDemandStateAllocation(0));
  }

  // walk over symbol table and make sure we have populated all the sizing information.
  updateTypeInformationFromSymtab(symtab);

  // Mark symbol table nodes which need the debug schedule to get the
  // correct waveforms.
  findDebugScheduleCornerCase(symtab);

  if (sLastNode)
    sPrintDebugAssert (sLastNode, sLastOffset);
  CGOUT () << "}\n";
} // void CodeGen::decorateSymbolTable


void CodeGen::findDebugScheduleCornerCase(STSymbolTable *symtab)
{
  SCHCombinational *debug_sched = mSched->getDebugSchedule();
  if (debug_sched->empty()) {
    return;
  }

  CbuildSymTabBOM* symtab_bom = dynamic_cast<CbuildSymTabBOM*>(symtab->getFieldBOM());
  if (not symtab_bom) {
    mMsgContext->CGIncompatibleSymTab();
    abort ();
  }

  NUNetRefFactory *netref_factory = gCodeGen->getNetRefFactory();

  // Once we mark a net as needing debug schedule, we don't need to examine
  // it again.
  NUNetElabSet marked_net_set;

  // Iterate over the debug schedule.
  // For each use def node in the debug schedule, get the set of def net refs.
  // For each def net ref, determine if it is covered by continuous driver(s).
  // If it is not, then mark the symtab entry for the elaborated net as needs
  // debug schedule.
  for (SCHIterator debug_iter = debug_sched->getSchedule();
       not debug_iter.atEnd();
       ++debug_iter) {
    FLNodeElab *flow_elab = *debug_iter;
    NUUseDefNode *ud_node = flow_elab->getUseDefNode();
    if (not ud_node) {
      continue;
    }

    STBranchNode *hier = flow_elab->getHier();

    // Traverse defs of the UD node, see if any of the defs are not covered by a
    // continuous driver.
    // This may occur if, say, a partial def of a net is merged into a block,
    // and that block is the only user of that partial def.  In that case, we will
    // not have a continuous driver for that partial def, but we will need to run
    // the debug schedule to make the waveform correct.
    // See bug 4827.
    NUNetRefSet defs(netref_factory);
    ud_node->getDefs(&defs);
    for (NUNetRefSet::iterator defs_iter = defs.begin();
         defs_iter != defs.end();
         ++defs_iter) {
      NUNetRefHdl def_ref = *defs_iter;

      NUNet *net_unelab = def_ref->getNet();
      NUNetElab *net_elab = net_unelab->lookupElab(hier, true);
      if (not net_elab) {
        continue;
      }

      if (marked_net_set.find(net_elab) != marked_net_set.end()) {
        continue;
      }

      NUNetRefHdl cont_def_ref = netref_factory->createEmptyNetRef();
      for (NUNetElab::DriverLoop driver_iter = net_elab->loopContinuousDrivers();
           not driver_iter.atEnd();
           ++driver_iter) {
        FLNodeElab *driver = *driver_iter;
        cont_def_ref = netref_factory->merge(cont_def_ref, netref_factory->createNetRefImage(net_unelab, driver->getFLNode()->getDefNetRef()));
      }

      if (not (*def_ref).covers(*cont_def_ref)) {
        symtab_bom->markRunDebugSchedule(net_elab->getSymNode());
        marked_net_set.insert(net_elab);
      }
    }
  }
}


/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
