// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdio.h>
#include <stdarg.h>

#include "util/ArgProc.h"
#include "util/CbuildMsgContext.h"
#include "util/OSWrapper.h"
#include "codegen/InducedFaultTable.h"
#include "codegen/InducedFaultParser.hxx"

InducedFaultTable::InducedFaultTable (MsgContext *msg_context) : 
  UtHashMap <UtString, CodeStream::InducedFaultMap *> (), mMsgContext (msg_context)
{}

/*virtual*/ InducedFaultTable::~InducedFaultTable ()
{
  for (iterator it = begin (); it != end (); ++it) {
    delete it->second;
  }
}

//! Add a fault to the table
void InducedFaultTable::add (UtString filename, UInt32 lineno, UtString fault)
{
  // find the InducedFaultMap instance for the file
  CodeStream::InducedFaultMap *map;
  iterator found;
  if ((found = find (filename)) == end ()) {
    // need to create a new map and install into the table
    map = new CodeStream::InducedFaultMap ();
    insert (value_type (filename, map));
  } else {
    // found an extant map for the file
    map = found->second;
  }
  // add an entry to the map
  map->insert (CodeStream::InducedFaultMap::value_type (lineno, fault));
}

//! Add an appended line to a table
void InducedFaultTable::append (UtString filename, UtString fault)
{
  // find the InducedFaultMap instance for the file
  CodeStream::InducedFaultMap *map;
  iterator found;
  if ((found = find (filename)) == end ()) {
    // need to create a new map and install into the table
    map = new CodeStream::InducedFaultMap ();
    insert (value_type (filename, map));
  } else {
    // found an extant map for the file
    map = found->second;
  }
  // add an append line to the map
  map->append (fault);
}

//! Add a prepended line to a table
void InducedFaultTable::prepend (UtString filename, UtString fault)
{
  // find the InducedFaultMap instance for the file
  CodeStream::InducedFaultMap *map;
  iterator found;
  if ((found = find (filename)) == end ()) {
    // need to create a new map and install into the table
    map = new CodeStream::InducedFaultMap ();
    insert (value_type (filename, map));
  } else {
    // found an extant map for the file
    map = found->second;
  }
  // add a prepend line to the map
  map->prepend (fault);
}

//! Look for the map for a given file.
CodeStream::InducedFaultMap *InducedFaultTable::findMap (const char *path) const
{
  // look for the filename part of the path
  const char *filename = path;
  for (int n = 0; path [n] != '\0'; n++) {
    if (path [n] == '/') {
      filename = path + n + 1;
    }
  }
  // now look for a map for that filename
  const_iterator found;
  UtString key (filename);
  if ((found = find (key)) == end ()) {
    return NULL;                        // no map exists for filename
  } else {
    return found->second;               // found a map for filename
  }
}

//! Read a set of fault specifications from a file
bool InducedFaultTable::read (const char *filename)
{
  // open the input file
  FILE *f;
  UtString oserror;
  if ((f = OSFOpen (filename, "r", &oserror)) == NULL) {
    mMsgContext->CGCannotOpenInducedFaultFile (filename, oserror.c_str ());
    return false;
  }
  // call the bison-derived parser
  Parser parser (filename, f, *this, mMsgContext);
  parser.parse ();
  return true;
}

//! Dump the table to standard output
void InducedFaultTable::pr () const
{
  print (UtIO::cout ());
}

//! Dump the table to a stream
void InducedFaultTable::print (UtOStream &s) const
{
  for (const_iterator it = begin (); it != end (); ++it) {
    s << "Map for " << it->first.c_str () << "\n";
    it->second->print (s);
  }
}

// Glue to get at the bison and flex derived parser and scanner
#define yyin InducedFault_in
#define yyout InducedFault_out
#define yylex InducedFault_lex
#define yylineno InducedFault_lineno
#define yyparse InducedFault_parse

extern int yylineno;
extern FILE *yyin;
int yylex ();
int yyparse ();

//! Singleton instance of the parser, set in ::Parser, reset in ::~Parser
/*static*/ InducedFaultTable::Parser *InducedFaultTable::Parser::sInstance = NULL;

InducedFaultTable::Parser::Parser (const char *filename, FILE *in, 
  InducedFaultTable &parent, MsgContext *msg_context) : 
  mFilename (filename), mParent (parent), mMsgContext (msg_context)
{
  ASSERT (sInstance == NULL);           // there can be only one
  sInstance = this;                     // bind the singleton instance
  yyin = in;                            // set the flex scanner input stream
}

/*virtual*/ InducedFaultTable::Parser::~Parser ()
{
  sInstance = NULL;                     // unbind the single instance
}

//! Interface to the bison parser
SInt32 InducedFaultTable::Parser::parse ()
{
  return yyparse ();
}

//! Error message writer called from the parser
void InducedFaultTable::Parser::error (const char *fmt, va_list ap)
{
  char buffer [256];
  vsnprintf (buffer, sizeof (buffer), fmt, ap);
  mMsgContext->CGInducedFaultFileError (mFilename, yylineno, buffer);
}

//! Add a fault to the table
void InducedFaultTable::Parser::add (const char *filename, UInt32 lineno, const char *fault)
{
  mParent.add (filename, lineno, fault);
}

//! Add an appended line to the table
void InducedFaultTable::Parser::append (const char *filename, const char *fault)
{
  mParent.append (filename, fault);
}

//! Add an prepended line to the table
void InducedFaultTable::Parser::prepend (const char *filename, const char *fault)
{
  mParent.prepend (filename, fault);
}
