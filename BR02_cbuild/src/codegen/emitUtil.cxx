// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*
 * \File
 * Centralized implementation of all code emitters for classes derived from
 * class NUBase.
 *
 * We emit code by passing down context flags that guide each method
 * in establishing what kinds of code are needed (declarations,
 * expressions, or various kinds of names. 
 *
 * Context flags are returned for those places that need to be aware of
 * the generated context for a subordinate tree.  (See the NUAssign and
 * NUIdentLvalue emitCode() methods for an example.
 *
 * A very few routines do complex analysis of their operands or are
 * aware of the existance of multiple output files (see NUModule::emitCode())
 * 
 */

#include "emitUtil.h"

#include "util/BitVector.h"
#include "util/DynBitVector.h"
#include "util/StringAtom.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUDesign.h"

#include "codegen/CGAuxInfo.h"

#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/CarbonDBWrite.h" // for castBOM
#include "backend/Stripper.h"

#include <algorithm>

namespace emitUtil {

  static size_t sizeofStateUpdateMemory (const NUMemoryNet*);
  static size_t sizeofDynStateUpdateMemory (const NUMemoryNet*);
  static size_t sizeofSparseDynStateUpdateMemory (const NUMemoryNet*);


// Format controls for various translated Verilog names

CodeGen::CxxName member ("m_", ""); // Format as class member
CodeGen::CxxName funct  ("f_", "()"); // Format as function 
CodeGen::CxxName accessor ("get_", "()"); // Format as output accessor
CodeGen::CxxName mclass ("c_", ""); // Format as translated class name
CodeGen::CxxName pclass ("c_", "::"); // Format as c_name:: prefix
CodeGen::CxxName noprefix ("", ""); // Format with only name string
CodeGen::CxxName formal ("a_", ""); // Format for function formal param
CodeGen::CxxName local ("l_", ""); // Format for function formal param
CodeGen::CxxName block ("b_", ""); // Format for a nested block name
CodeGen::CxxName fname ("f_", ""); // Format as function name

// syntactic sugar to conveniently code-gen net names embedded in
// other stuff Print an elaborated name in expression context as a
// hierarchical class reference Note that this REALLY presumes that
// CodeGen::CGOUT() points to same stream as we're writing to!
//
UtOStream& operator<<(UtOStream& f, const NUNetElab* net)
{
  net->emitCode (eCGDefine | eCGHierName);
  return f;
}

// Most common formatting for a NUNet - just print the symbol as a class
// member.
UtOStream& operator<<(UtOStream& f, const NUNet* net)
{
  net->emitCode (eCGNameOnly);
  return f;
}

void getGeneratedType(UtString* typeBuf, bool isTristate, bool isBidirect, 
                      bool isSigned, UInt32 width, bool isInterface, bool isReal)
{
  int closeBrack = 0;
  UtString& f = *typeBuf;
  
  if (isReal)
  {
    f << "CarbonReal";
    return;
  }

  if (isBidirect)
  {
    f << "Bidirect<";
    ++closeBrack;
  }
  else if (isTristate)
  {
    f << "Tristate<";
    ++closeBrack;
  }
  
  if (isSigned)
  {
    if (width > LLONG_BIT) {
      f << "SBitVector<" << width << "> ";
      gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path);
    }
    else if (width > LONG_BIT)
      f << "SInt64 ";
    else if (width > 16)
      f << "SInt32 ";
    else if (width > 8)
      f << "SInt16 ";
    else
      f << "SInt8 ";
  }
  else if (width <= LLONG_BIT)
    {
      f << gCodeGen->CBaseType (width) << width;
    }
  else if (isInterface)
    f << "CarbonUInt32 ";
  else
    {
      f << "BitVector<" << width << "> ";
      gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path);
    }
  
  if (isTristate || isBidirect)
    f << ", " << width;

  while(closeBrack--)
    f << "> ";
}

UtString& operator<<(UtString& s, const CBaseTypeFmt& n)
{
  //! if the mNet is set and it is any sort of memory object, CBaseTypeFmt does not print 
  /*! the full type of a memory (CType does), here, just return without 
   *  printing anything here */
  if (n.mNet && n.mNet->is2DAnything())
    return s;

  if (isDefine (n.mCtxt))
    s << "(";

  bool isBidirect = false;
  bool isTristate = false;
  if (n.mNet)
    {
      isBidirect = gCodeGen->isBidirect (n.mNet);
      isTristate = gCodeGen->isTristate (n.mNet);
    }

  if (isStaticCast (n.mCtxt)) {
    s << "static_cast <";
  }
  getGeneratedType(&s, isTristate, isBidirect, n.mSigned, n.mPrec,
                   isInterface(n.mCtxt), n.mReal);
  if (isStaticCast (n.mCtxt)) {
    s << "> ";
  }

  if (isDefine (n.mCtxt))
    s << ")";
  return s;

}

UtOStream & operator << (UtOStream& f, const CBaseTypeFmt& n)
{
  //! if the mNet is set and it is a Memory (temp or regular), CBaseTypeFmt does not print 
  /*! the full type of a memory (CType does), here, just return without 
   *  printing anything here */
  if (n.mNet && n.mNet->is2DAnything())
    return f;

  f << TAG;
  
  if (isStaticCast (n.mCtxt)) {
    // The caller explicitly demanded a static_cast <...> construct. These are
    // needed when G++ chokes on some other legal concotion of casting. See,
    // for exampe, bug7955.
    f << "static_cast <";
  } else if (isDefine (n.mCtxt)) {	// In definition context, output cast
    f << "(";
  }


  bool isBidirect = false;
  bool isTristate = false;
  if (n.mNet)
    {
      isBidirect = gCodeGen->isBidirect (n.mNet);
      isTristate = gCodeGen->isTristate (n.mNet);
    }

  UtString typeName;
  getGeneratedType(&typeName, isTristate, isBidirect, n.mSigned, n.mPrec,
                   isInterface(n.mCtxt), n.mReal);
  f << typeName;

  if (isStaticCast (n.mCtxt)) {
    f << "> ";
  } else if (isDefine (n.mCtxt)) {
    f << ")";
  }
  return f;
}

// Pretty printing strengths for tristate drivers
UtOStream& operator << (UtOStream& f, Strength s)
{
  switch (s)
    {
    case eStrPull:	f << "eStrPull"; break;
    case eStrDrive:	f << "eStrDrive"; break;
    case eStrTie:       f << "eStrTie"; break;
    default:	INFO_ASSERT(0, "Unknown Strength");  // unknown strength!!!
    }
  return f;
}

bool CBaseTypeFmt::operator==(const CBaseTypeFmt& other)
{
  if (mPrec != other.mPrec
      || mSigned != other.mSigned
      || mReal != other.mReal
      || (mNet && !other.mNet)
      || (!mNet && other.mNet))
    return false;

  if (mNet && other.mNet)
    return isTypeCompatible (mNet, other.mNet);

  return true;
}

// Is this net represented by a Plain Old Data type?
bool
isPOD (const NUNet * net)
{
  if (net->is2DAnything())
    return false;

  if (net->getBitSize () > LLONG_BIT)
    return false;

  if (gCodeGen->isBidirect (net) || gCodeGen->isTristate (net))
    return false;

  return true;
}

bool isNetReferenced(const NUNet* net)
{
  CGAuxInfo *aux = net->getCGOp();
  return ((aux != NULL) && !aux->isUnreferenced()) /*||
    (net->isRead() || net->isWritten() || net->isTriWritten() ||
    net->isHierRefRead() || net->isHierRefWritten())*/;
}

// is this net a VHDL record passed as a port and aliased?
bool isVhdlRecordPort (const NUNet* net)
{
  CGAuxInfo* cgop = net->getCGOp ();

  NU_ASSERT (cgop, net);
  return cgop->isVhdlRecordPort ();
}

bool netIsDeclaredForcible(const NUNet* net)
{
  return net->isForcible() && !isNetConst(net);
}

// print the full type for a net
UtOStream & operator << (UtOStream& f, const CType& n)
{
  UtString trailing (" ");		// close brackets and memory params
  UInt32 unitSize = n.p->getBitSize (); // Assumed unit size

  f << TAG;

  if (n.p->is2DAnything ()) {
    MemoryClass memClass = classifyMemory (n.p);

    // Previously, we used SparseDynTempMemory for SparseMemory, and
    // we used DynTempMemory for Memory.  Now, we use a hash-table
    // implementation of DynTempMemory for both Memory and SparseMemory.
    // So we don't need the extra "Sparse" at the beginning of the keyword.
    // We should also merge together the StateUpdate and StaticTemp
    // versions.
    if ((memClass != eDynTempMemory) && isSparseMemory (n.p)) {
      f << "Sparse";
    }

    const NUMemoryNet *mem = n.p->getMemoryNet ();

    switch (memClass) {
    case eInvalid:
      NU_ASSERT (false, n.p);   // only expect valid memories here...
      break;

    case eMemory:
      f << "Memory<";
      break;

    case eDynTempMemory:
      f << "DynTempMemory<"
        << CType (mem) << "> ";
      return f;

    case eStaticTempMemory: {
      const NUTempMemoryNet * tmem = dynamic_cast<const NUTempMemoryNet*>(n.p);
      UInt32 write_ports = tmem->getNumWritePorts ();
      if (write_ports==0) { write_ports = 1; }
      f << "StaticTempMemory<" << write_ports << ", "
        << CType (mem) << "> ";
      return f;
    }

    case eStateUpdateMemory:
      f << "StateUpdateMemory<" << mem->getNumWritePorts () << ", ";
      break;

    case eDynStateUpdateMemory:
      f << "DynStateUpdateMemory<";
      break;
    }

    // do all the common parameters for simple memories.
    unitSize = mem->getRowSize ();

    const ConstantRange *depthRange = mem->getDepthRange ();
    const ConstantRange *widthRange = mem->getWidthRange ();

    f << CBaseTypeFmt (unitSize, eCGDeclare, mem->isSigned ()) << ", "
      << widthRange->getLength () << ", "
      << depthRange->getMsb () << ", "
      << depthRange->getLsb () << "> ";

    return f;

  } else if (netIsDeclaredForcible(n.p)) {
    if (isPOD (n.p)) {
      f << "ForciblePOD<";
      trailing.insert (0, "> ");
    } else {
      f << "Forcible<";

      UtString MaskType (",");
      MaskType << CBaseTypeFmt (unitSize) << "> ";

      trailing.insert (0, MaskType.c_str ());
    }
  }

  bool needsSize = false;
  if (gCodeGen->isBidirect (n.p))
    {
      f << "Bidirect<";
      trailing.insert (0, "> ");
      needsSize = true;
    }
  else if (gCodeGen->isTristate (n.p))
    {
      f << "Tristate<";
      trailing.insert (0, "> ");
      needsSize = true;
    }

  if (n.p->isInteger ())
    // Check for a Verilog 'integer' and map onto normal signed int
    f << "SInt32";
  else if ( n.p->isReal() )
    // Check for a Verilog 'real' or 'realtime' and map onto a double
    f << "CarbonReal";
  else if (n.p->isVHDLLineNet())
    // Check for vhdl LINE variable.
    f << "UtIOStringStream*";
  else if (n.p->isUnsignedSubrange ())
    // VHDL positive integer subrange?  Emit this as an UNSIGNED field, it will be
    // widened and coerced whenever used in an expression.
    f << CBaseTypeFmt (unitSize, eCGVoid, false); // will widen to SInt32, ASAP
  else 
    f << CBaseTypeFmt (unitSize, eCGVoid, n.p->isSigned());

  if (needsSize)
    f << ", " << unitSize;

  f << trailing;

  return f;
}

bool
isTypeCompatible (const NUNet* actual, const NUNet* formal)
{

  if (actual->getBitSize () != formal->getBitSize ()
      || (actual->isSigned () != formal->isSigned ()) // signed vs. unsigned
      || (actual->isReal () != formal->isReal ())
      || (actual->is2DAnything () != formal->is2DAnything ())
      || (gCodeGen->isTristate (actual) != gCodeGen->isTristate (formal))
      || netIsDeclaredForcible(actual) != netIsDeclaredForcible(formal))
    return false;

  return true;
}

// Overloaded
bool
isTypeCompatible (const NUExpr*x, const NUExpr* y)
{
  return (x->getBitSize () == y->getBitSize ()
          && x->isSignedResult () == y->isSignedResult ());
}

// Test for a compile-time constant value and return it...
bool
isCTCE (const NUExpr *v, DynBitVector *value)
{
  // First see if it's a constant
  const NUConst *c = v->castConst();

  if (not c or c->isReal ())
    return false;

  // Don't care about the drive and only need the value
  if (value)
    c->getSignedValue(value);

  return true;
}

bool
isCTCE (const NUExpr *v, UInt64 *val)
{
  // First see if it's a constant
  const NUConst *c = v->castConst();

  if (not c or c->isReal ())
    return false;

  if (val)
  {
    c->getULL (val);
  }

  return true;
}
  
// Test for a value identical to zero
//
bool
isZeroValue (const NUExpr *v)
{
  const NUConst *c = v->castConst();
  return (c && c->isZero ());
}

bool
isOnesValue (const NUExpr *v)
{
  const NUConst*c = v->castConst();
  return (c && c->isOnes ());
}

// Generate a unique name using provided UtString prefix.
void
UniqueName (UtString* s)
{
  char buf[32];
  static int uuid;

  sprintf (buf, "%0x", uuid++);
  *s += buf;
}

static const char* sNumSuffix (UInt32 size, bool sign)
{
  TargetCompiler tcomp = gCodeGen->getTarget ();

  if (size <= LONG_BIT)
    return sign ? "" : "U";
  else if (ptrSize () == sizeof(UInt64))
    return sign ? "L" : "UL";
  else if (tcomp == eGCC3 || tcomp == eGCC4 || tcomp == eWINX)
    return sign ? "LL" : "ULL";
  else
    return "";
}

// Given a bitstring, generate a static bitvector expression
CGContext_t
emitConstant (UtOStream& out, const DynBitVector& value, UInt32 vsize,
              CGContext_t context, bool sign)
{
  out << TAG;
  if (isFlowValue (context)) {
    out << (value.any() ? "true" : "false");
    return eCGVoid;
  } else if (vsize <= LLONG_BIT) {
    return emitConstant (out, value.llvalue (), vsize, context, sign);
  } else if (isLvalue (context)) {
    // Using a copy-constructed constant bitvector, de-constify it...
    out << "(*const_cast< BitVector<" << vsize << "," << sign
        << ">*>(reinterpret_cast<const BitVector< " << vsize << "," << sign
        << ">*>(";

    out << "cv$" << gCodeGen->addPooledConstant (value, sign)
	<< ")))";
  } else {
    if ( sign ) {
      out << "(*reinterpret_cast<const SBitVector< " << vsize << ">*>(";
    } else {
      out << "(*reinterpret_cast<const BitVector< " << vsize << "," << sign
        << ">*>(";
    }

    out << "cv$" << gCodeGen->addPooledConstant (value, sign)
	<< "))";

  }
  return eCGPrecise;
}

CGContext_t
emitConstant (UtOStream& out, UInt64 value, UInt32 size, CGContext_t context, bool sign)
{
  INFO_ASSERT (size > 0 && size <= LLONG_BIT, "bad size generating integral constant");
  CGContext_t ret = eCGPrecise;

  out << TAG;

  if (isFlowValue (context)) {
    out << (const char*)((value != 0) ? "true" : "false");
    return eCGVoid;
  }

  const char *suffix = "";
  if (sign) {
    // Signed integer values
    SInt64 sValue (value);
    sValue <<= (LLONG_BIT - size); // Sign-extend from size to 64 bit width, in two steps
    sValue >>= (LLONG_BIT - size); // now we have signed version of constant
    if (UtSINT32_MIN < sValue && sValue <= UtSINT32_MAX ) {
      ;                                       // constant can be expressed as a 32 bit signed literal, so no constant suffix needed
    } else {
      suffix = sNumSuffix (LLONG_BIT, sign);  // requires 33 to 64 bits to be expressed, so needs 'L' suffix
    }
    out << "(" << sValue;

    if (sValue < 0 && size != physicalSize (size) && !needsExtension (context)) {
      ret = Imprecise (ret);
    }
  } else {
    // Unsigned integer values
    UInt64 mask = UtUINT64_MAX >> (LLONG_BIT - size);
    value &= mask; // mask to precision requested
    if ( (size <= 32) and (value <= UtUINT32_MAX)) {
      ;                                        // constant can be expressed in 32 bits, no suffix needed, test/vhdl/lang_vslice/bug6617.vhd
    } else {
      suffix = sNumSuffix (LLONG_BIT, sign);   // constant requires 33 to 64 bits to be expressed, so needs 'UL' suffix
    }
    out << "(0x" << UtIO::hex << value << UtIO::dec;
  }

  out << suffix << ")";
  return ret;
}

CGContext_t
checkDeadCode (const SourceLocator& loc, CGContext_t context, const UtString& kindString)
{
  if (isDeclare (context) && gCodeGen->isVerboseDeadCode ())
    gCodeGen->getMsgContext ()->DeadCodeWalking (&loc, kindString.c_str ());

  return eCGVoid;
}

// Output a mask
void genMask(UtOStream& out, size_t size, bool signedType)
{
  // gcc rotates past 64-bits so if it is 64 bits we end up with 0
  UInt64 num;
  if (size < 64) {
    num = (1ULL << size) - 1ULL;
  } else if (size == 64) {
    num = 0ULL - 1ULL;
  } else {
    INFO_ASSERT(false, "Mask not supported on sizes greater than 64-bits");
    num = 0ULL;
  }

  out << "0x"
      << UtIO::hex << num << UtIO::dec << sNumSuffix (size, signedType);
}

// Output an operation UtString followed by a mask of SIZE 1's
// (assume 64 bit worst case)
void mask (size_t size, const char *op, bool signedType)
{
  if( size >= LLONG_BIT || size== LONG_BIT )
    return;			// No mask needed

  UtOStream& out = gCodeGen->CGOUT ();

  out << TAG << op << CBaseTypeFmt (size, eCGDeclare, signedType) << "(";
  genMask(out, size, signedType);
  gCodeGen->CGOUT () << ")";
}

/*
 * Is this net a storage holder or just a reference to storage elsewhere
 * in the design?
 */
bool isDereferenced(const NUNet *n)
{
  if (n->isHierRef() && !gCodeGen->isLocallyRelativeHierRef(n))
    // We must treat hier refs as dereferenced because we sometimes
    // create pointers for them.
    return true;
  else if (n->isVHDLLineNet())
    return true;  // We create UtIOStringStream* 
  else if (n->isAllocated ())
    return false;
  else if (n->isPort())
    return true;
  else
    return false;
}

/* If the bit-size provided maps directly to the storage unit, we
 * can optimize away certain things.
 */
bool
naturalSize (size_t size)
{
  if (size == LONG_BIT || size == LLONG_BIT)
    return true;		// Trivial cases on x86 & SPARC
  else if (size > LLONG_BIT)
    return true;
  else
    return false;
}


// check if the operator (of BitVector) is supported for 
// integer arithmetic, assignment, and operations between BitVectors of 
// different widths.
//
// TODO: augment effectiveBitSize() with information about the least-significant sign-bit, this
// is the first bit that is going to be copied for the sign extensions of a signed operand.  Consider
//             alu80 + x32 * y32
// where all the variables are signed.  Then x and y are sign-extended to 80 bits and multiplied.  But
// bit 63 is the effective sign-bit, and bits [79:64] are copies of that bit.  If we BVOverloaded
// the multiply we could end up calling BitVector<80,true>::operator+(SInt64(SInt64(x32)*SInt64(y32)))
// which would yield much better code than doing an 80-bit multiply.
bool
isBvOverloaded (NUOp::OpT opc, const NUExpr *operand, const NUExpr *other,
                bool first, UInt32*newSize)
{
  UInt32 eSize = operand->effectiveBitSize ();

  switch(opc)
  {
    // Incomplete - need to do full operation
    //case NUOp::eBiVhdlMod:      //!< VHDL modulus ?
    case NUOp::eBiSMod:	    //!< binary %
    case NUOp::eBiSDiv:		//!< binary /
    case NUOp::eBiUMod:	    //!< binary %
    case NUOp::eBiUDiv:		//!< binary /
    case NUOp::eBiRshift:	//!< binary >>
    case NUOp::eBiLshift:	//!< binary <<
    case NUOp::eBiLshiftArith:
    case NUOp::eBiRshiftArith:
    case NUOp::eBiVhRshiftArith:
    case NUOp::eBiVhLshiftArith:

      // These only have 32 bit rhs's
      if (first)
        return false;
      if (eSize <= LONG_BIT && not operand->isSignedResult ()) {
        if (newSize)
          *newSize = LONG_BIT;
        return true;
      } else
        return false;
        
    case NUOp::eBiMinus:	//!< binary -
      // not commutative, only if this is the second operand
      if (first) return false;

      // fall through

    case NUOp::eBiPlus:		//!< binary +
    case NUOp::eBiEq:           //!< Binary ==
    case NUOp::eBiNeq:          //!< Binary !=
    case NUOp::eBiTrieq:        //!< Binary ===
    case NUOp::eBiTrineq:       //!< Binary !==
    case NUOp::eBiBitAnd:	//!< binary &
    case NUOp::eBiBitOr:	//!< binary |
    case NUOp::eBiBitXor:	//!< binary ^
      if (eSize > 64)
        return false;           // Really nothing we can do here!

      // Consider situations where reducing this operand might change
      // it's signed-ness when computing the mixed bitvector <op> POD
      // expression.
#define RET_SIZE_WHEN(cond, size)                                       \
        do {                                                            \
          if (cond) {                                                   \
            if (newSize)                                                \
              *newSize = ((size) <= LONG_BIT) ? LONG_BIT : LLONG_BIT;   \
            return true;                                                \
          }} while(0)

      switch ((operand->isSignedResult () << 1) + other->isSignedResult ()) {
      case 0:                   // unsigned op unsigned
        RET_SIZE_WHEN (eSize <= LLONG_BIT, eSize);
        return false;

      case 1:                   // unsigned op signed
        RET_SIZE_WHEN (eSize != LONG_BIT && eSize < LLONG_BIT, eSize); // Can safely treat as positive SInt32/64
        return false;

      case 2:                   // signed op unsigned
        return false;           // The sign-extensions are important

      case 3:                   // signed op signed
        RET_SIZE_WHEN (eSize <= LLONG_BIT, eSize);
        return false;
      }
      
    case NUOp::eBiSMult:	//!< binary *
    case NUOp::eBiUMult:	//!< binary *
      // Bitsizes upto 64 bits...
      RET_SIZE_WHEN (eSize <= LLONG_BIT, eSize);

      //TODO: Since our extended precision multiply handles ANY
      //word-multiple size, we could probably support reducing shorter
      //bitVector operands to their self-determined size (rounded up
      //to a word boundary).  But right now, too much other code
      //presumes that overloads only come in 32/64 bit sizes...
      return false;
        
    case NUOp::eBiSLt:           //!< Binary <
    case NUOp::eBiSLte:          //!< Binary <=
    case NUOp::eBiSGtr:          //!< Binary >
    case NUOp::eBiSGtre:         //!< Binary >=
      // The bitvector operand MUST be signed to do signed operators
      // Only have 32 bit overloads on compares
      if (operand->isSignedResult ()  && other->isSignedResult ()) {
        RET_SIZE_WHEN ( eSize < LONG_BIT, eSize);
      }
      return false;
      
    case NUOp::eBiULt:           //!< Binary <
    case NUOp::eBiULte:          //!< Binary <=
    case NUOp::eBiUGtr:          //!< Binary >
    case NUOp::eBiUGtre:         //!< Binary >=
      // Only have 32 bit relational compares to a bitvector value, so
      // don't allow overload call with larger PODs or signed operands
      // for now.
      if (not operand->isSignedResult ()  && not other->isSignedResult ()) {
        RET_SIZE_WHEN (eSize <= LONG_BIT, eSize);
      }
      return false;

    default: 
      return false;
  }

  return false;
}
#undef RET_SIZE_WHEN

// Size of pointers
size_t
ptrSize ()
{
  if (gCodeGen->is64bit ())
    return 8;
  else if (gCodeGen->is32bit ())
    return 4;
  else
    // Right now assume targetted to self
    return sizeof(void*);
}

// Handles memories and normal nets, returning the element size
UInt32
elementSize (const NUNet *n)
{
  if (const NUMemoryNet* mem = n->getMemoryNet ())
    return mem->getRowSize ();
  else
    return n->getBitSize ();
}

// Compute storage required for an object.  Does NOT handle composite
// objects, so a NUMemoryNet returns the size of a single word, a StateUpdate
// returns the size of the basic unit being double-buffered, not the TOTAL size.
// Tristates return base object size - excluding drive strength & vtab
// Bidirects also return the basic object size
//
size_t
netByteSize (const NUNet *n)
{
  UInt32 bits = elementSize (n);

  if (isDereferenced (n))
    bits = CHAR_BIT * ptrSize ();
  else if (bits <= CHAR_BIT)
    bits = CHAR_BIT;
  else if (bits <= SHORT_BIT)
    bits = SHORT_BIT;
  else if (bits <= LONG_BIT)
    bits = LONG_BIT;
  else if (bits <= LLONG_BIT)

    bits = LLONG_BIT;
  else
  {
    // Objects larger than 64 bits are rounded to word multiples
    bits = (bits + LONG_BIT - 1) & -LONG_BIT;

    if (n->isSigned ())
      // there's a tax for empty base classes in GCC 2.95
      // and on all SBitVector classes - even in 3.3.3 (Not sure if this is true for icc!
      {
        int signedOverhead = sizeof(SBitVector<32>) - sizeof(UInt32);
        int unsignedOverhead = sizeof(BitVector<32>) - sizeof(UInt32);
#if pfGCC_2
	// Compiling under gcc2.95, so we can verify that overhead
	// is known.
        NU_ASSERT (signedOverhead == 8, n); // Check that SBitVector has the tax we expect
        NU_ASSERT (unsignedOverhead == 4, n); // Check that BitVector has the tax we expect
#endif

	int overhead = n->isSigned()? signedOverhead: unsignedOverhead;
	bits += overhead * CHAR_BIT;
      }
  }

  // Round the bits to an addressable boundary
  size_t bytes = bits/CHAR_BIT;

  return bytes;
}

//! Count: data, strength.
static size_t sizeofTristate (size_t s)
{
  size_t size = 2*s;
  return size;
}

//! Count: data, strength, xdrive.
static size_t sizeofBidirect (size_t s)
{

  size_t size = 4*s;
  return size;
}

//! Memory's save/restore size for onDemand
static size_t sizeofMemoryOnDemandState (const NUMemoryNet *mem, size_t s)
{
  size_t size = s * mem->getDepth();
  return size;
}


// POD alignments and Bitvector alignment indexed by ObjectAlignment enums
//  eInt8 = 0, eInt16= 1, eInt32= 2, eInt64= 3, eBitVector= 4, ePointer= 5

#if pfLINUX || pfWINDOWS
static UInt8 WinxAlignment[]  = { 1, 2, 4, 8, 4, 4};
#endif
#if pfLINUX || pfLINUX64 || pfWINDOWS
static UInt8 LinuxAlignment[] = { 1, 2, 4, 4, 4, 4};
#endif
#if pfLINUX64
static UInt8 Linux64Alignment[]={ 1, 2, 4, 8, 4, 8};
#endif

size_t alignmentOf (enum ObjectAlignment otype)
{
  UInt8 *al =
#if pfLINUX || pfWINDOWS
    (gCodeGen->getTarget () == eWINX) ? WinxAlignment : LinuxAlignment;
#elif pfLINUX64
    gCodeGen->is32bit() ? LinuxAlignment : Linux64Alignment;
#endif

  return al[otype];
}

// Return the alignment associated with an object of a specific size (size is in bytes, not bits!)
size_t
alignment (size_t size)
{
  switch(size)
    {
    case 1:	return alignmentOf (eInt8);
    case 2:	return alignmentOf (eInt16);
    case 4:	return alignmentOf (eInt32);
    case 8:	return alignmentOf (eInt64);
    default:	return alignmentOf (eBitVector);
    }
}

//! Tristates no longer have vptr.
static size_t alignmentTristate (size_t size)
{
  if (size == sizeof(UInt64)) {
    return alignmentOf (eInt64);
  } else {
    return alignment (size);
  }
}

static size_t alignmentBidirect (size_t size)
{
  return alignmentTristate(size);
}

// Return the alignment required by a net
size_t
alignment (const NUNet *net)
{
  if (isDereferenced (net))
    return alignment (ptrSize ());

  size_t size = netByteSize (net);
  
  // If it's a StateUpdateMemory, it has extra requirements
  if (net->is2DAnything())
  {
    const NUMemoryNet* memNet = net->getMemoryNet();
    return alignmentMemory (memNet);
  }
      
  // Due to the vtab, bidirects and tristates are handled separately
  if (gCodeGen->isBidirect (net))
    return alignmentBidirect (size);
  
  if (gCodeGen->isTristate (net))
    return alignmentTristate (size);

  // Otherwise, it just aligns to the basic alignment of the object
  return alignment (size);
}

// Allocation of an object, NOT rounded up to alignment, so it may be
// necessary to adjust when allocating arrays, etc.
size_t
allocation (const NUNet *net)
{
  if (isDereferenced (net))
    return ptrSize ();

  // Start with basic memory used by the net.
  size_t valueSize = netByteSize (net);
  size_t total = valueSize;

  if (gCodeGen->isBidirect (net))
    total = sizeofBidirect (valueSize);
  else if (gCodeGen->isTristate (net))
    total = sizeofTristate (total);

  NU_ASSERT (!net->isDoubleBuffered (), net);

  if (netIsDeclaredForcible(net))
    // A forcible net has an extra mask
    {
      if (pfGCC_2)
	total = align2 (total, alignment (net));
      total += valueSize;
    }


  total = align2 (total, alignment (net));
  return total;
}

/*!
  The allocation of any net within the onDemand state buffer is
  determined solely by the amount of data read/write in its
  save/restore method.  This is written to a byte stream, so
  alignment, pointer size, etc. have no effect.  For most net types,
  this is the same as their model allocation, but there are some
  special cases.
 */
size_t onDemandStateAllocation (const NUNet *net)
{
  // Start with basic memory used by the net.
  size_t valueSize = netByteSize (net);
  size_t total = valueSize;

  if (gCodeGen->isBidirect (net)) {
    total = sizeofBidirect(total);
  } else if (gCodeGen->isTristate (net)) {
    total = sizeofTristate(total);
  }

  if (netIsDeclaredForcible(net)) {
    // A forcible net has an extra mask
    total += valueSize;
  }

  // For memories, adjust for depth.  We should only be dealing with
  // non-sparse memories here.
  const NUMemoryNet *mem = net->getMemoryNet();
  if (mem) {
    NU_ASSERT(!isSparseMemory(mem), mem);
    total = sizeofMemoryOnDemandState(mem, total);
  }

  return total;
}

// Determine maximal alignment required at this point by a nested declaration scope.
// Must search all contained nested scopes because the outer struct must be aligned
// so that it can guarantee the alignment of the inner scope.
size_t
alignment (const NUNamedDeclarationScope * scope)
{
  NUNetList allNets;
  scope->getAllNets (&allNets);

  StaticFilter nya;		// Not Yet Allocated (static and contained in struct)

  // Build collection of nets that can appear in this block
  NUNetVector staticNets;
  remove_copy_if (allNets.begin (), allNets.end (), back_inserter (staticNets),
		  std::not1 (nya));

  // Now look at everything we have or will need and find the largest alignment

  size_t blockAlignment = alignment (staticNets);

  // Also look at contained scopes and see if they need higher alignment

  for (NUNamedDeclarationScopeCLoop innerBlocks = scope->loopDeclarationScopes();
       not innerBlocks.atEnd ();
       ++innerBlocks)
    blockAlignment = std::max (blockAlignment, alignment (*innerBlocks));

  return blockAlignment;
}

size_t
alignment (const NUNetVector& p)
{
  size_t align = 1;		// Assume byte-alignment is good enough

  for(NUNetVectorIter n = p.begin (); n!=p.end (); ++n)
    // Determine largest alignment required
    align = std::max (align, alignment (*n));

  return align;
}  

static size_t sizeofHashMapFastIter() {
  // carbon_hashtable ie 8 bytes on a 32-bit target and 12 bytes
  // on a 64-bit target
  size_t sz = gCodeGen->is32bit() ? 8 : 12;

  // In addition, UtHashMapFastIter has mChain, which is a pointer
  sz = align2(sz, alignmentOf(ePointer));
  sz += ptrSize();
  return sz;
}

//! Function to return the size of a  template <> Memory or SparseMemory
static size_t sizeofOrdinaryMemory (const NUMemoryNet *mem)
{
  size_t size = 0;

  if (pfGCC_2)
    // Empty base class optimization doesn't exist with gcc 2.95.3
    size += 1;                  // Reserve a byte for it

  // Memory contains a pointer to the out-of-line allocated memory data
  size_t ptrAlign = alignmentOf (ePointer);
  size = align2 (size, ptrAlign);
  if (isSparseMemory (mem)) {
    size += sizeofHashMapFastIter();
  }
  else {
    size += ptrSize ();
  }

  // Both Memory and SparseMemory contain a pointer to the callback
  // array
  size = align2 (size, ptrAlign);
  size += ptrSize ();

  // Sparse memories have a zero entry to hold default values (which aren't populated)
  // and a bit-bucket for OOB read/write.

  if (isSparseMemory (mem)) {
    UInt32 rowBytes = netByteSize (mem);
    size_t rowAlign = alignment (rowBytes);

    size = align2(size, rowAlign); // Align to default zero value for unpopulated entries
    size += rowBytes;           // Add zero object for reading
    // Whole object must be padded to largest alignment required.
    size = align2 (size, std::max (rowAlign, ptrAlign));    
  }

  return size;
}

// Function to return the alignment of memories
size_t alignmentMemory(const NUMemoryNet * memNet)
{
  UInt32 rowBytes = netByteSize (memNet);
  switch (classifyMemory (memNet))
  {
  case eMemory:
    return isSparseMemory (memNet) ? std::max (alignment (rowBytes), alignmentOf (ePointer))
                                   : alignmentOf (ePointer);
    
  case eStateUpdateMemory:
    return alignmentOf (eInt64);

  case eDynStateUpdateMemory:
    return isSparseMemory (memNet) ? std::max (alignment (rowBytes), alignmentOf (ePointer))
                                   : alignmentOf (ePointer);

  case eStaticTempMemory:
    return alignmentOf (eInt64);

  case eDynTempMemory:
    return alignmentOf (ePointer);

  default:
    NU_ASSERT (false, memNet);
    return 0;
  }
}	

//! Function to return the size of a static temp memory
static size_t sizeofStaticTempMemory(const NUMemoryNet* memNet)
{
  UInt32 rowBytes = netByteSize (memNet);
  int numPorts = memNet->getNumWritePorts ();

  size_t size = ptrSize ();     // BaseMemory
  size += sizeof(SInt32);       // StaticTempMemory<>.mIndex
  size = align2 (size, alignmentOf (eInt64));

  size_t writeRecordSize = sizeof(UInt64) + rowBytes;
  writeRecordSize = align2(writeRecordSize, alignmentOf (eInt64));
  size += numPorts * writeRecordSize;

  size = align2 (size, alignmentOf (eInt64));

  return size;
}


//! Function to return the size of a state update memory
static size_t sizeofStateUpdateMemory(const NUMemoryNet* memNet)
{
  // Compute Memory<> base size.
  size_t size = sizeofOrdinaryMemory(memNet);

  // The next object is two StaticTempMemory<>'s, which is an
  // instantiated structure, so it needs to begin at a 64 bit boundary
  // on a Sun.
  size = align2(size, alignmentOf (eInt64));
  size += sizeofStaticTempMemory(memNet) * 2;

  return size;
}

static size_t sizeofDynTempMemory(const NUMemoryNet* /*mem*/)
{ 
  size_t size = ptrSize ();                 // DynTempMemory<>.mBaseMemory
  size += sizeofHashMapFastIter();          // DynTempMemory<>.mWrites

  size = align2 (size, alignmentOf (ePointer));
  return size;
}

//! Function to return the size of a state update memory
static size_t sizeofDynStateUpdateMemory(const NUMemoryNet *mem)
{
  // Compute Memory<> base size.
  size_t size = sizeofOrdinaryMemory(mem);

  // The next object is two DynTempMemory<>'s, which contain pointers, so it
  // needs to begin at a pointer boundary
  size = align2(size, alignment (ptrSize ()));
  size += sizeofDynTempMemory(mem) * 2;

  return size;
}

//! Function to return the size of a state update memory
static size_t sizeofSparseDynStateUpdateMemory(const NUMemoryNet *mem)
{
  // Compute Memory<> base size.
  size_t size = sizeofOrdinaryMemory(mem);

  // The next object is two DynTempMemory<>'s, which contain pointers, so it
  // needs to begin at a pointer boundary
  size = align2(size, alignment (ptrSize ()));
  size += sizeofDynTempMemory(mem) * 2;

  return size;
}

// Function to return the size of a memory
size_t sizeofMemory (const NUMemoryNet* mem)
{
  switch (classifyMemory (mem)) {
  case eDynStateUpdateMemory:
    if (isSparseMemory (mem))
      return sizeofSparseDynStateUpdateMemory (mem);
    else
      return sizeofDynStateUpdateMemory (mem);

  case eStateUpdateMemory:
    return sizeofStateUpdateMemory (mem);

  case eMemory:
    return sizeofOrdinaryMemory (mem);

  case eStaticTempMemory:
    return sizeofStaticTempMemory (mem);

  case eDynTempMemory:
    return sizeofDynTempMemory (mem);

  default:
    NU_ASSERT (false, mem);
    return 0;
  }
}

// Collect Housekeeping information on each net as we visit it.
void
checkNetIncludes (const NUNet *n)
{
  // Check to see if we need to include one of the special header files.
  // These are only included if needed, as they make the C++ compiler
  // work very hard.
  //

  // We no longer have state update on nets
  NU_ASSERT(!n->isDoubleBuffered() || n->is2DAnything(), n);

  if (netIsDeclaredForcible(n)) {
    if (n->getBitSize () > LLONG_BIT
	|| gCodeGen->isTristate (n))
      // Not a POD - use general forcible template)
      gCodeGen->addExtraHeader (gCodeGen->cForcible_h_path);
    else
      // A POD - use simpler version.
      gCodeGen->addExtraHeader (gCodeGen->cForciblePOD_h_path);
  }

  if (n->getBitSize () > LLONG_BIT)
    gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path);

  const NUMemoryNet *memNet = n->getMemoryNet();
  if ( n->is2DAnything())
  {
    if (isSparseMemory (n)) {
      gCodeGen->addExtraHeader( gCodeGen->cSparseMemory_h_path );
    } else {
      gCodeGen->addExtraHeader( gCodeGen->cMemory_h_path );
    }

    if (memNet && memNet->getRowSize () > LLONG_BIT)
      gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path);
  } else if (n->getBitSize () > LLONG_BIT)
  {
    gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path);
  }

  if (gCodeGen->isTristate (n)) {
    if (n->getBitSize () > LLONG_BIT)
      gCodeGen->addExtraHeader (gCodeGen->cTristateBV_h_path);      
    else
      gCodeGen->addExtraHeader (gCodeGen->cTristate_h_path);
  }
  // Keep track of all the "types" we encounter in this compilation
  // in order to provide the type dictionary to the runtime.
  //
  //gCodeGen->AddTypeToDictionary (*n);
}

void
emitBlockNesting (UtOStream& out, const NUScope * scope, char sep)
{
  if (scope->getScopeType() == NUScope::eNamedDeclarationScope) {
    NUScope * parent_scope = scope->getParentScope();
    if (parent_scope->getScopeType() == NUScope::eNamedDeclarationScope) {
      // Block is nested inside another, traverse hierarchy
      out << TAG;
      emitBlockNesting (out, parent_scope, sep);
    }
  
    out << block (scope->getName ()) << sep;
  }
}

void emitSaveRestoreMember(UtOStream& out, const NUNet *net)
{
  out << member(net);
}

/*
 * Return the bit-bounds for a net
 */ 
const ConstantRange* getNUrange(const NUNet *p)
{
  static const ConstantRange oneBit(0,0);

  if (const NUVectorNet *v = p->castVectorNet ())
    return v->getRange ();
  else if (const NUMemoryNet *m = p->getMemoryNet())
    return m->getWidthRange ();
  else if (p->getBitSize() == 1)
    return  &oneBit;
  else 
    {
      NU_ASSERT ("Unexpect Net" == 0, p);
      return &oneBit;
    }
}

/*
 * Classify a NUMemoryNet according to how it will be coded as a C++ class
 * object.
 */
MemoryClass classifyMemory (const NUNet* net)
{
  if (not net->is2DAnything ())
    return eInvalid;            // not a memory

  const NUMemoryNet* mem = net->getMemoryNet ();

  bool isSparse = isSparseMemory(net);

  if (not net->isMemoryNet ()) {
    // A temp of some sort
    const NUTempMemoryNet* temp = dynamic_cast<const NUTempMemoryNet*>(net);
    NU_ASSERT (temp, net);

    if (temp->isDynamic () || isSparse)
      return eDynTempMemory;
    else
      return eStaticTempMemory;
  }

  // A normal memory


  if (mem->isDoubleBuffered ())
    if (mem->getIsDynamic () || isSparse)
      return eDynStateUpdateMemory;
    else
      return eStateUpdateMemory;
  else
    return eMemory;
}

bool isSparseMemory (const NUNet* net)
{
  if (not net->is2DAnything ())
    return false;               // not a memory at all

  // A memory is sparse if it has a lot of memory data.
  return (gCodeGen->isSparseMemoryBitSize(net->getBitSize()));
}

/*
 * Return the depth-bounds for a memory that is used in a
 * NUMemSelLvalue or NUMemSelRvalue. This allows us to fix up the
 * index into the memory if the start isn't 0.
 */
const ConstantRange* getNUMemDepth(NUNet *p)
{
  static const ConstantRange oneBit(0,0);

  if (NUMemoryNet *m = p->getMemoryNet())
    return m->getDepthRange ();
  NU_ASSERT ("MemoryNet Expected" == 0, p);
  return 0;
}

// Should calculate the same way as emitIndex does...
//
SInt32
ComputeIndex (SInt32 index, const ConstantRange& r)
{
  return r.offsetBounded (index);
}

// Is the indexing-expression a VHDL dynamic range?  If so, return
// the lowBound expression if we were passed a pointer to a return
// location.
bool
isDynRange (const NUExpr* e, const NUExpr**lowBound)
{
  if (not e)
    return false;

  const NUBinaryOp* b = dynamic_cast<const NUBinaryOp*>(e);

  if (b && b->getOp () == NUOp::eBiDownTo)
  {
    if (lowBound)
      *lowBound = b->getArg (1);
    return true;
  }

  return false;
}

// Generate indexing expression
void emitIndex(UtOStream& out, SInt32 index, const ConstantRange& r)
{
  out << TAG << r.offsetBounded (index);
}

void emitIndex(UtOStream& out, const NUExpr* index, SInt32 bitoff, CGContext_t c,
			     const ConstantRange& r)
{
  UInt64 value;

  if (isCTCE (index, &value)) {
    out << TAG;
    emitIndex (out, static_cast<SInt32>(value)+bitoff,  r);
  } else {
    // Might be a VHDL DownTo that wasn't folded away.
    out << TAG;
    const NUExpr* lowBound;
    if (isDynRange (index, &lowBound))
      return emitIndex (out, lowBound, bitoff, c, r);


    if (bitoff)
      out << "(";

    out << "(";
    c = index->emitCode (c);
    if (index->getBitSize () > LLONG_BIT)
      out << ".value()";
    else if( !isPrecise(c))
      mask( index->getBitSize(), "&" );
    out << ")";

    if (bitoff)
      out << "+" << bitoff << ")";

  }
}

// If we need a class prefix for this context, output it.
//
void classPrefix(CGContext_t c)
{
  if (isDefine (c))
    {
      gCodeGen->CGOUT () << pclass (gCodeGen->getClass ());
    }
}

void genBVpartsel (UtOStream& out, UInt32 pos, UInt32 size)
{
  if (size > LLONG_BIT)
  {
    out << ".partsel(" << pos << ", " << size << ")" << TAG;
    return;
  }
  else if (size > LONG_BIT)
  {
    out << ".llvalue(" << TAG;
    if (pos != 0 || size != LLONG_BIT)
      out << pos << "," << size;
  }
  else
  {
    if ((pos % LONG_BIT) == 0 && size == LONG_BIT) {
      // Aligned special cases
      out << "._M_w[" << (pos / LONG_BIT) << "]" << TAG; // known inbounds, avoid get_word()
      return;
    } else {
      out << ".value(" << TAG;
      if (pos != 0 || size != LONG_BIT)
        out << pos << "," << size;
    }
  }
  out << ")";
}
      
// Is this a physically sized value?
bool isPhysicalSize (UInt32 size)
{
  switch (size) {
  case 8:
  case 16:
  case 32:
  case 64:
    return true;
  default:
    return false;
  }
}

// Return the size of the nearest physical type
UInt32 physicalSize (UInt32 size)
{
  size = (size + CHAR_BIT - 1) / CHAR_BIT;        // Compute number of bytes...

  switch (size)
  {
  case 1:
    return CHAR_BIT;
  case 2:
    return SHORT_BIT;
  case 4:
  case 3:
    return LONG_BIT;
  case 5:
  case 6:
  case 7:
  case 8:
    return LLONG_BIT;
  default:
    INFO_ASSERT (0, "Zero size or BitVector not expected"); // should have avoided BitVectors
    return 0;
  }
}


// Does this operand require sign-extension?
bool needsSignExtension (const NUNet* net, UInt32 width, bool signedResult)
{
  if (not signedResult)
    return false;
  
  // We don't need to sign extend if the net is signed and a physical unit size
  // that C++ knows how to properly extend.  So, if we need 16 bits signed, and
  // have 8 bits unsigned, we must insert an Extender<>.  But if the net was signed,
  // then no Extender<> is needed.
  //
  UInt32 elemSize = elementSize (net);
  return (width > elemSize and
         not (isPhysicalSize (elemSize) and net->isSigned () ));
}  


/*!
 * Does expression \a e require that the \a index -th operand be
 * sign-extended to a physical word size so that the operation will
 * correctly compute.
 *
 * Consider:
 *      wire [3:0] i;
 *      wire isneg = $signed(i) < 1
 *
 * requires that we convert the 4-bit i into an 8 bit value and compare that
 * against zero, copying i[3] into the extended bits before generating a
 *            isneg = ((SInt8)VHEXT(i,4,8) < (SInt8)1);
 * which should expand to a byte compare.
 */
bool needsSignExtension (NUOp::OpT opc, const NUExpr* lop, const NUExpr* rop,
                                 UInt32 resultSize, UInt32 index)
{
  // If the operator size is 8, 16, 32, 64 (and so is the operand, then we
  // would seem to be okay.

  const NUExpr *operand = index==0 ? lop : rop;
  const NUExpr *other = index==1 ? rop : lop;
  UInt32 opSize = operand->getBitSize ();

  if ((emitUtil::isPhysicalSize (resultSize) && resultSize <= opSize)
      || opSize > LLONG_BIT
      || not operand->isSignedResult ())
    return false;

  // if all the operands are the same size AND physically correct (8,16,32,64) then
  // we are also set
  if (emitUtil::isPhysicalSize (opSize) && opSize == other->getBitSize ())
    return false;

  // Now check individual operators
  switch (opc)
  {
  case NUOp::eBiLshiftArith:
  case NUOp::eBiRshiftArith:
  case NUOp::eBiLshift:
  case NUOp::eBiRshift:
    // only sign-extend left operand for verilog shifts
    return (index == 0) && lop->isSignedResult ();

  case NUOp::eTeCond:
    // Don't prematurely extend the result of a ?: to a signed value.  Wait to see
    // if it's needed in a signed context.
    return false;

  case NUOp::eBiDExp:
    // Don't extend when operands are real numbers!
    return operand->isSignedResult () and not operand->isReal ();

  case NUOp::eBiBitAnd:
  case NUOp::eBiBitOr:
  case NUOp::eBiBitXor:
    // Don't extend when it is a bitwise operator and both operands are the same size
    return !(opSize == other->getBitSize ());
    break;

  default:
    return operand->isSignedResult ();
  }
}

// Generate an extender constructor to convert arbitrary-sized net into sign-extended
// larger type.  Wrap whole Extender<...>() construction in extra parens so that
// cpp-style macro expanders don't get confused.
static CGContext_t
sGenExtender (UtOStream& out, const NUBase* e, const STBranchNode* parent, UInt32 eSize, CGContext_t ctx, UInt32 pref)
{
  const NUNet* net = dynamic_cast<const NUNet*>(e);
  out << TAG;

  if (pref <= LLONG_BIT)
    pref = emitUtil::physicalSize (pref);

  if (isLvalue (ctx))
    // op= case, no extension required
    ctx = e->emitCode (ctx);
  else if (pref <= eSize) {
    out << "(";

    // is this already a signed type or expression?
    if (net) {
      if (not net->isSigned ())
        out << CBaseTypeFmt (pref, eCGDefine, true);
    } else if (const NUExpr* expr = dynamic_cast<const NUExpr*>(e)) {
      if (not expr->isSignedResult ())
        out << CBaseTypeFmt (pref, eCGDefine, true);
    }

    if (parent) {
      gCodeGen->outputHierPath(out, parent);
      out << TAG;
    }

    ctx = e->emitCode (ctx);
    if (eSize > LLONG_BIT && pref <= LLONG_BIT) {
      genBVpartsel (out, 0, pref);
      out << TAG;
    }

    out << ")";
  } else {
    if (pref > LLONG_BIT) {
      // Need a SBitVector here
      out << CBaseTypeFmt (pref, eCGVoid, true);
      if (eSize > LLONG_BIT) {
        // And here too
        out << "(" << CBaseTypeFmt (eSize, eCGVoid, true) << "(";
        if (parent) {
          gCodeGen->outputHierPath(out, parent);
          out << TAG;
        }

        ctx = e->emitCode (ctx);
        out << "))" << TAG;
        return ctx;
      }
      // We still need an extender object to go from eSize up to a size that
      // the SBitVector constructors can handle, and the expression is a POD
      pref = emitUtil::physicalSize (eSize);
    }
      
    NU_ASSERT (pref <= LLONG_BIT && eSize <= LLONG_BIT, e);

    // VHDL has signed positive subranges.  We treat them as integers, but when
    // extending from their subrange size, we zero extend rather than sign-extending
    // the subfield.
    
    if (net && net->isUnsignedSubrange ()) {
      out << TAG << CBaseTypeFmt (pref, eCGDeclare, true) << "("
          << CBaseTypeFmt (pref, eCGDefine, false);

      if (parent) {
        gCodeGen->outputHierPath(out, parent);
	out << TAG;
      }
      ctx = e->emitCode (ctx);
      out << ")" << TAG;
    } else {
      /*
       * The silly "+" is to convince the GCC 3.x parser that the
       * anonymous object reference is an expression - otherwise
       * we have trouble if this is an argument to a constructor
       */
      out << TAG << "(+Extender<" << CBaseTypeFmt (pref, eCGVoid, true) << "," 
          << CBaseTypeFmt (eSize, eCGVoid, true) << ","
          << pref << "," << eSize << ">(";
      if (parent) {
        gCodeGen->outputHierPath(out, parent);
        out << TAG;
      }

      ctx = e->emitCode (ctx);
      ctx = Precise (ctx) | eCGNeedsExtension;
      out << ")())";
    }
  }
  return ctx;
}

// Sign-extend an operand by appropriate means.  sNeedsSignExtension() will have
// already returned TRUE for this operand.
CGContext_t
signExtend (UtOStream& out, const NUExpr* e, CGContext_t ctx, UInt32 pref)
{

  switch(e->getType ()){
  case NUExpr::eNUIdentRvalue:
  case NUExpr::eNUIdentRvalueElab:
    // sign-extend handled by idents
    if (not isLvalue (ctx))
      ctx |= eCGNeedsExtension;
    out << TAG;
    ctx = e->emitCode (ctx);
    break;

  case NUExpr::eNUConstNoXZ:
  case NUExpr::eNUConstXZ:
    // Don't truncate signed constants to packed size - we need physical sizes
    out << TAG;
    ctx = e->emitCode (ctx | eCGNeedsExtension);
    break;

  default:
    // Raise size to hardware-supported value
    out << TAG;
    ctx = sGenExtender (out, e, 0, e->getBitSize (), ctx, pref);
  }

  return ctx;
}

CGContext_t
signExtend (UtOStream& out, const NUNet* net, const STBranchNode* parent, CGContext_t ctx,
            UInt32 pref)
{
  // Resulting size could be signed bitvector (pref > LLONG_BIT), in  which
  // case if we are starting with an unsigned or non-physical-sized object we
  // must raise to physical signed value and construct a SBitVector for that
  bool paren = false;
  UInt32 elemSize = elementSize (net);
  
  out << TAG;

  if (pref > LLONG_BIT) {
    out << CBaseTypeFmt (pref, eCGVoid, true) << "(";
    paren = true;    
    pref = bvSizeToOther (elemSize, pref);
  }

  ctx = sGenExtender (out, net, parent, elemSize, ctx, pref);

  if(paren)
    out << ")";

  return ctx;
}

// Simple-minded test for needing a bitvector temporary
// Will we generate a BVref?
bool
isBVref (const NUExpr *e)
{
  if (const NUVarselRvalue*ps = dynamic_cast<const NUVarselRvalue*>(e)) {
    NUNet* net = sFindNet (e);
    if (not net)
      // Something really messy will be a BVref if it's big, otherwise
      // it's not a partsel, just a masked field...
      return ps->getIdentExpr ()->getBitSize () > LLONG_BIT;

    if (net->getBitSize () <= LLONG_BIT)
      return false;

    // Any partsel that eludes the previous filters is going to end up as a
    // BVref
    return true;
  }

  return false;
}

// Overloaded
bool
isBVref (const NULvalue *lv)
{
  if (const NUVarselLvalue* ps = dynamic_cast<const NUVarselLvalue*>(lv)) {
    NULvalue* base = ps->getLvalue ();
    if (base->getBitSize () > LLONG_BIT)
      return true;
  }

  return false;
}

// BitVector operators are defined to take 32, 64 or BitVector operands.
// If we have some random tiny value, cast it to a supported size.  This
// avoids random errors like:
//
//  tmp.f_000000.cxx:16034: error: ISO C++ says that `BitVector<101,
//  __types_trait<false> > carbon::operator<<(const BitVector<101,
//  __types_trait<false> >&, unsigned int)' and `operator<<' are ambiguous even
//  though the worst conversion for the former is better than the worst
//  conversion for the latter

UInt32 bvSizeToOther (UInt32 thisSize, UInt32 thatSize)
{
  if (thatSize > LLONG_BIT)
    if (thisSize <= LONG_BIT)
      return LONG_BIT;
    else if (thisSize <= LLONG_BIT)
      return LLONG_BIT;
    else
      return thisSize;
  else
    return thisSize;
}

// Helper function to coerce the operand to the size value we require.
// Question: if Rvalue size is smaller than subtree, should we mask off
// excess bits?
CGContext_t
coerceRvalue(const NUNet* net, const STBranchNode* parent,
             UInt32 bitSize, bool sign, CGContext_t context)
{
  UtOStream &out = gCodeGen->CGOUT ();
  int openParen = 0;

  UInt32 netSize = isLvalue (context) ? elementSize (net) : net->getBitSize ();

  if (net->is2DAnything ())
    // NUMemselRvalue's NEVER call here - they do their own coercions.  So a
    // memory here is the source to an aggregate assignment and therefore we
    // don't want any coercions to be applied....
    ;
  else if (isLvalue (context))
    // op= case needs no special coercion of operand
    ;
  else if (isBVOverloadSupported(context)) {
    // Skip the casting, clear the bit after use.
    context &= ~eCGBVOverloadSupported;

    // May need to static_cast to 32 if size < 32 bits or
    // cast to correct type if net sign doesn't match expression sign,
    // or if net size is not 32/64 bits.
    //
    // This is necessary for BitVector binary operations, but not for assign.
    UInt32 newSize = bvSizeToOther (netSize, bitSize);
    if (isBVExpr(context)
        && (netSize < newSize || (sign != net->isSigned ())))
    {
      context &= ~eCGBVExpr;      // Don't pass down!

      // We might actually need to physically sign-extend non-physical
      // sized operands to a physical boundary.
      if (needsSignExtension (net, newSize, sign) || needsExtension (context)) {
        out << TAG;
        return signExtend (out, net, parent, context, newSize);
      }

      // Just need to recast, but make sure we get correct size 32/64
      // needed to do an operation with a BitVector.
      ++openParen;

      out << TAG << CBaseTypeFmt (newSize, eCGDeclare, sign)
          << "(";

      // If we are changing the sign AND growing the size
      // don't let C/C++ rules get in the way.  Change the sign
      // on the net before widening it.
      if (netSize < newSize && sign != net->isSigned ()) {
        out << TAG << CBaseTypeFmt (netSize, eCGDeclare, sign) << "(";
        ++openParen;
      }
        
      netSize = newSize;
    }
  }
  else if (!isConstructor (context)) // BUG95
  {
    if (needsSignExtension (net, bitSize, sign) || needsExtension (context)) {
      // We directly need to sign extend this, OR, we're processing an operand that
      // needed to be widened to a physical size in order to do a signed comparison (for
      // example).
      out << TAG;
      return signExtend (out, net, parent, context, bitSize);
    }

    if (bitSize > netSize
        || (bitSize != netSize && LLONG_BIT < netSize)
        || (sign != net->isSigned () && not net->isReal ()) )
    {
      UInt32 parens=1;          // need at least one
      NU_ASSERT(!net->is2DAnything(), net);
      out << TAG << CBaseTypeFmt (bitSize, eCGDeclare, sign) // Build a constructor T(name)
	  << "(";

      // Take care about converting both size and sign at the same time!
      if (bitSize > netSize && sign != net->isSigned ()) {
        out << TAG << CBaseTypeFmt (netSize, eCGDefine, sign)
            << "(";
        ++parens;
      }        

      bool gotATemp = NeedsATemp (context) && bitSize > LLONG_BIT;

      if (parent != NULL)
      {
        gCodeGen->outputHierPath(out, parent);
	out << TAG;
      }
      context = net->emitCode (context & ~eCGNeedsATemp);
      out << TAG;

      if (isForced (context))
	// we're playing with forcing masks, don't go looking at data
	;
      else if (gCodeGen->isTristate (net))
	out << TAG << ".getT()";

      // DON'T muck with partsel if we don't have to...
      //
      if (net->getBitSize () > LLONG_BIT) {
	// what if  ((bitSize > LLONG_BIT) &&
	//           (getBitSize() != bitSize) &&
	//           (getBitSize() > bitSize))
	// do we need to strip off extra bits?
	//
        if (bitSize <= LLONG_BIT) {
          emitUtil::genBVpartsel (out, 0, bitSize);
          out << TAG;
        }
        context |= eCGPrecise;
      } else if (netSize > bitSize )
	context = Imprecise(context);
      else if (isPhysicalSize (elementSize (net)) && net->isSigned ())
        // Letting gcc sign-extending a physical value to wider context.
        context |= eCGNeedsExtension;

      while (parens--)
        out << ")";

      return context | (gotATemp ? eCGIsATemp : eCGVoid);
    }
  }

  if (parent != NULL)
  {
    gCodeGen->outputHierPath(out, parent);
    out << TAG;
  }
  CGContext_t rc = net->emitCode (context);
  if (gCodeGen->isTristate (net) && !net->isMemoryNet ())
    out << TAG << ".getT()";


  if (isFlowValue (context) && bitSize > LLONG_BIT)
    out << ".any()";

  while (openParen--)
    out << ")";

  return ((netSize > bitSize )? Imprecise(rc) : rc);
} // coerceRvalue

//! Is this BVref a candidate  for reinterpret_cast-ing to a BitVector?
/*!
 * In general, a partsel that would be an aligned multiple of words is good.
 * That allows us to avoid calling anytoany() or using the BVref op BVref
 * methods for arithmetic.
 *
 * The one exception is that if we have the last bits of an object, then
 * the "remaining bits are going to be zeroes anyway, so it's safe to
 * ignore them when constructing a bitvector.  e.g. if we had
 *
 *	reg [124:0] x;
 *	reg [92:0] ,y;
 *	y = x[124:32];
 *
 * we could translate this as
 *
 *	m_y = (*reinterpret_cast<BitVector<93>*>(&m_x._M_w[1]));
 *
 * If we are performing an operation that will zero out the excess bits, it's
 * permissible to have a size that isn't word aligned, as the excess bits will
 * be scrubbed in the bitvector operation.  \a excessOK indicates that the
 * size is irrelevant.
 */
bool
isAlignedBVref (const NUNet * net, const NUExpr * expr, const NUExpr * index,
		SInt32 pos, UInt32 size, bool excessOK)
{
  if (not index->castConst ()
      and not Stripper::isFactorable (index))
    // not a constant bit position, or a multiple of 32 as the index
    return false;

  // Do not do this optimization when pos is negative.  This optimization could be done,
  // but did not seem worth the effort to implement.  See bug 12900.
  if (pos < 0) {
    return false;
  }

  if ((net == NULL) && expr->isWholeIdentifier())
    net = expr->getWholeIdentifier();
  if (net && (gCodeGen->isTristate (net)))
    return false;
  UInt32 netSize = net? elementSize (net): expr->getBitSize();

  if ( ((pos % LONG_BIT) + size) > LONG_BIT)
    // Don't allow cast of LHS to (UInt64), as it might not be
    // aligned to 8 byte boundary.  And even worse, aliasing
    // will reorder writes before reads. [BUG2759]
    return false;

  return (size > LLONG_BIT
	  && ((pos % LONG_BIT) == 0)
	  && (excessOK
	      || ((size % LONG_BIT) == 0)
	      || (pos+size) == netSize));
}

// Generate if SIZE > LLONG_BIT, reinterpret an aligned piece of rvalue as a new
// bitvector, ala
//                (*reinterpret_cast<BitVector<SIZE> *>(rvalue._M_w[index+ (pos/32)]))
// If SIZE <= LLONG_BIT, then we are accessing an aligned word from the vector and just
// need to do a
//                rvalue._M_w[index + (pos/32)]
CGContext_t
genAlignedBVref (UtOStream& out, const NUExpr *rvalue, const NUExpr* index, CGContext_t context,
                 SInt32 pos, UInt32 size, bool indexInBounds)
{
  NU_ASSERT ((pos % LONG_BIT) == 0, rvalue); // expected aligned bit offset

  CGContext_t inCtxt = context;

  out << TAG;

  if (size > LLONG_BIT) {
    out << "(*reinterpret_cast<"
        << (const char *) (isLvalue (context) ? "" : "const ")
        << CBaseTypeFmt (size, eCGVoid, rvalue->isSignedResult())
        << "*>(&";

    inCtxt |= eCGLvalue;
  } else
    out << "((";

  CGContext_t rc = rvalue->emitCode (inCtxt);


  UInt32 factor;
  if (NUExpr *factoredIndex = Stripper::factorIndexExpr (index, &factor)) {
    if (indexInBounds)
      out << "._M_w[";
    else
      out << ".get_word(";

    factoredIndex->emitCode (context & ~eCGLvalue);
    out << TAG;
    if (factor != 1)
      out << "*" << factor;
    if (pos != 0) {
      // Need to cast LONG_BIT or else C converts to unsigned expression
      SInt32 offset = pos / (SInt32)LONG_BIT;
      out << "+" << offset;
    }

    if (indexInBounds)
      out << "]";
    else
      out << ")";
    delete factoredIndex;
  } else {
    // This is a constant index, should never be negative due to normalization, make sure we don't
    // emit a negative index into the _M_w array.
    NU_ASSERT(pos >= 0, rvalue);
    out << "._M_w[";
    out << (pos / LONG_BIT);
    out << "]";
  }

  out << "))";      
  
  return Precise (rc);
}

/*!
 * Is this expression accessing a piece of a simple variable that's aligned to
 * a byte, short or word address value?  We could use address arithmetic
 * to fetch or store just the piece without needing to mask a larger value.
 */
bool isAlignedAccess (const NUNet *var, const NUExpr* expr, SInt32 pos, UInt32 size)
{
  SInt32 sizeLimit;
  gCodeGen->getCarbonContext ()->getArgs ()->getIntLast (CRYPT("-alignedAccess"),
						      &sizeLimit);

  if (sizeLimit < 0)
    return false;

  // Assume unaligned when the offset is negative.  This could be made to work, but doesn't seem
  // worth the effort to fix bug 12900.
  if (pos < 0) {
    return false;
  }

  // We need something we can use as an lvalue, so it needs to be a named variable, or an
  // array reference or a memory reference
  //
  UInt32 access;
  if (var)
    access = elementSize (var);
  else if (expr->isWholeIdentifier () && (var = expr->getWholeIdentifier ()))
    access = elementSize (var);
  else if (const NUMemselRvalue* memref = dynamic_cast<const NUMemselRvalue*>(expr))
    if (memref->getIdentExpr ()->isWholeIdentifier ()) {
      var = memref->getIdentExpr ()->getWholeIdentifier ();
      access = elementSize (var);
    } else {
      return false;
    }
  else
    return false;

  if (var && var->isForcible ())
    return false;

  // GCC Alias analysis as of 3.4.3 is not conservative enough to get
  // this right.  Because char* accesses are presumed to alias
  // arbitrary memory, we should be able to safely accept 8-bit size references.
  // BUT, it doesn't work - matrox/phoenix/carbon/cwtb/rttop miscompares
  //
#if 0
  if (sizeLimit == 0 && size == CHAR_BIT)
    sizeLimit = CHAR_BIT;     // Allow char* accesses; they alias everything
#endif

  if (size > LONG_BIT
      || access <= size          // object we're looking at is SMALLER than fetch
      || gCodeGen->isTristate (var))
    return false;

  NU_ASSERT ((pos+size) <= access, var);

  return ((size == 8 || size == 16 || size == 32)
	  && (pos % size) == 0
	  && (SInt32)size <= sizeLimit);
}

// Access a [SU]Int(8|16|32) from the variable var, being aware of
// endian-ness, sign of the result and whether we need a LVALUE or
// RVALUE access

CGContext_t sGenAlignedAccess (const NUExpr* expr, SInt32 pos, UInt32 size, bool sign, CGContext_t context)
{
  // isAlignedAccess doesn't allow negative positions, so should not have a negative position here.
  NU_ASSERT(pos >= 0, expr);

  gCodeGen->addExtraHeader (gCodeGen->cCarbonRunTime_h_path);
  UtOStream& out = gCodeGen->CGOUT ();

  UInt32 objSize = expr->getBitSize ();

  // Two cases:  For a POD, we do something like AlignedRead<TypeIN, TypeOUT>(expr,index)
  //             For a BV, we do                 AlignedRead<UInt32, TypeOUT>(expr._M_w[wordIndex],index)
  out << TAG << "Aligned" << (char *)(isLvalue (context) ? "Write" : "Read")
      << "< ";

  if (objSize > LLONG_BIT)
    out << "CarbonUInt32, ";
  else
    out << CBaseTypeFmt (expr->getBitSize (), eCGVoid, expr->isSignedResult())
        << ", ";
      
  out << CBaseTypeFmt (size, eCGVoid, sign) << ">(";

  bool memBVAccess = expr->getType () == NUExpr::eNUMemselRvalue && objSize > LLONG_BIT;
  if (memBVAccess)
    // A StateUpdateMemory::reference object indirectly contains a BitVector, but
    // doesn't INHERIT from BitVector, so we have to downcast the
    // returned reference object to the object. We don't HAVE to do this for all memories,
    // but it's safe to do so.
    //
    out << "static_cast<" << CBaseTypeFmt (objSize, eCGVoid, expr->isSignedResult ())
        << ">(";

  // The base address expression
  CGContext_t rc = expr->emitCode (context);
  out << TAG;

  if (objSize > LLONG_BIT) {
    if (memBVAccess)
      out << ")";

    // It was a BitVector, so factor out the word and byte/|short-within-word
    UInt32 wordPos = pos / LONG_BIT;
    pos %= LONG_BIT;
    objSize = LONG_BIT;
    out << "._M_w[" << wordPos << "]"; // known inbounds; avoid get_word()
  }

  out << ",";
 
  out << (pos/size);

  out << ")";

  return Precise (rc);
}

// Generate 64 bit result from aligned bitvector location, being aware
// of endian order issues, e.g. emit either
//      (base[off] | (UInt64(base[off+1])<<32))        // little-endian
//      ((UInt64(base[off]) << 32) | base[off+1])      // big-endian
//
CGContext_t sGenLLValue (const NUExpr* base, UInt32 wordOffset, CGContext_t context)
{
  CGContext_t rc;
  UtOStream& out = gCodeGen->CGOUT ();
  {
    //
    out << "(" << TAG;
    rc = base->emitCode (context);
    out << TAG
        << "._M_w[" << wordOffset++ << "] | (UInt64(";
    base->emitCode (context);
    out << TAG
        << "._M_w[" << wordOffset << "])<<32))";
  }
  
  return rc;
}

CGContext_t sGenLValue (const NUExpr* base, UInt32 wordOffset, CGContext_t context)
{
  CGContext_t rc = base->emitCode (context);
  UtOStream& out = gCodeGen->CGOUT ();
  out << TAG << "._M_w[" << wordOffset << "]";
  return rc;
}

// Returns true if the given net has state that needs to be included in 
// checkpoint files.
static bool sIsSaveRestoreNet(const NUNet *net)
{
  CGAuxInfo *aux = net->getCGOp();  // get CodeGen context for the net

  // We want to generate save/restore code for this net if:
  //  1) it has CodeGen info, indicating it will be emitted
  //  2) it is not a hierarchical reference
  //  3) it is not a reference member (except for VHDL line variables)
  return aux != NULL
    && aux->hasOffset()
    && !net->isHierRef()
    && (!aux->isDereferenced() || net->isVHDLLineNet()); 
}

static bool sIsOnDemandSaveRestoreNet(const NUNet *net)
{
  // only save flops and latches, skipping memories as well
  if (!isOnDemandStateNet(net))
    return false;

  return sIsSaveRestoreNet(net);
}

static void sGetSavedNets (const NUModule *module, NUNetList* list, SaveRestoreGenMode mode, bool nested)
{
  NUNetList allNets;
  // Choose either top-level or nested nets
  if (nested) {
    module->getAllNestedNets (&allNets);
  } else {
    // We want this ordering for efficient onDemand save/restore,
    // but simply changing the implementation of getAllTopNets()
    // breaks other uses of it, for reasons I don't understand.
    module->getAllTopNetsPortsFirst (&allNets);
  }

  bool (*filterfunc)(const NUNet*);
  if (mode == eSaveRestoreOnDemand)
    filterfunc = &sIsOnDemandSaveRestoreNet;
  else
    filterfunc = &sIsSaveRestoreNet;

  remove_copy_if (allNets.begin (), allNets.end (),
		  back_inserter (*list),
		  std::not1 (std::ptr_fun(filterfunc)));

  if (mode == eSaveRestoreOnDemand) {
    // Sort in much the same way as when members are emitted
    emitUtil::sort_by_state stateSorter;
    list->sort(stateSorter);
  }

  return;
}

static void sGenerateMemberSaveRestore(const NUNetList &srNets, SaveRestoreGenMode mode)
{
  UtOStream& out = gCodeGen->CGOUT ();
  UInt32 ondemand_bytes = 0;

  // add a line "F OP member" for each net
  for(NUNetList::const_iterator i = srNets.begin(); i!= srNets.end(); ++i) {
    NUNet* net;
    net = *i;
        
    if (mode == eSaveRestoreNormal) {
      // Call new C interface function
      out << "OP(&F, ";
      emitSaveRestoreMember(out, net);
      out << "); \\\n";

    } else {
      // For OnDemand performance, we copy data in big chunks.
      //
      // For consecutive PODs, BitVectors, Tristates, etc. -
      // anything whose state is the entirety of its allocated
      // size - we can call the stream's read()/write() method
      // directly with the address of the first member and the
      // size of the consecutive region.  To do this, we'll emit
      // the start of the function call at the beginning of the
      // block and keep track of the number of bytes of the
      // members we've seen.  When we hit the end of the OnDemand
      // state for this module, we close the function call.
      //
      // We also have to stop when we encounter a memory, because
      // its state is allocated separately.  In that case, we
      // close the function call we were working on, if necessary,
      // and emit the ordinary memory save/restore call.

      if (net->isMemoryNet()) {
        // Close the last statement, if necessary
        if (ondemand_bytes != 0) {
          out << UtIO::dec << ondemand_bytes << "); \\\n";
        }
        ondemand_bytes = 0;
        out << "OP(&F, ";
        emitSaveRestoreMember(out, net);
        out << "); \\\n";
      } else {
        // Start a new statement
        if (ondemand_bytes == 0) {
          out << "RW(&F, &";
          emitSaveRestoreMember(out, net);
          out << ", ";
        }

        // Determine the number of bytes to add to the current
        // region.  This only works if the allocated size matches
        // the OnDemand state size, and no padding was needed for
        // alignment.
        UInt32 size = onDemandStateAllocation(net);
        INFO_ASSERT(size == allocation(net), "OnDemand save/restore allocation mismatch");
        // To check the alignment, we can use the number of bytes
        // currently allocated, since it started from either the
        // beginning of the module or the last memory member, both
        // of which would have been well-aligned.
        UInt32 align_mask = alignment(net) - 1;     // which bits need to be zero?
        INFO_ASSERT((ondemand_bytes & align_mask) == 0, "OnDemand save/restore alignment mismatch");
        ondemand_bytes += size;
      }
    }
  }

  // Close the last statement, if necessary
  if (ondemand_bytes != 0) {
    out << UtIO::dec << ondemand_bytes << "); \\\n";
  }
}

static void sGenerateSaveMethod (SaveRestoreGenMode mode)
{
  UtOStream& out = gCodeGen->CGOUT ();
  UtString funcname = (mode == eSaveRestoreOnDemand) ? "saveOnDemand" : "save";
  UtString macroname = (mode == eSaveRestoreOnDemand) ? "SRLIST_ONDEMAND_" : "SRLIST_";

  out << "void " << pclass (gCodeGen->getClass ())
      << funcname << "(UtOCheckpointStream& out, CarbonObjectID *descr){" << ENDL
      << "  (void) out;\n"
      << "  (void) descr;\n"
      << "  " << macroname << noprefix (gCodeGen->getClass ()) << "(out, writeToStream, " << funcname << ", carbonInterfaceOCheckpointStreamWrite)\n}\n";
}

static void sGenerateRestoreMethod (SaveRestoreGenMode mode)
{
  UtOStream& out = gCodeGen->CGOUT ();
  UtString funcname = (mode == eSaveRestoreOnDemand) ? "restoreOnDemand" : "restore";
  UtString macroname = (mode == eSaveRestoreOnDemand) ? "SRLIST_ONDEMAND_" : "SRLIST_";

  out << "void " << pclass (gCodeGen->getClass ()) 
      << funcname << "(UtICheckpointStream& in, CarbonObjectID *descr){" << ENDL
      << "  (void) in;\n"
      << "  (void) descr;\n"
      << "  " << macroname << noprefix (gCodeGen->getClass ()) << "(in, readFromStream, " << funcname << ", carbonInterfaceICheckpointStreamRead)\n}\n";
}

// Generate the save and restore methods for a class.
void GenerateSaveRestore (const NUModule *module, CGContext_t context, SaveRestoreGenMode mode)
{
  if (isDeclare (context))
    {
      UtOStream& out = gCodeGen->CGOUT ();
      UtString savefuncname = (mode == eSaveRestoreOnDemand) ? "saveOnDemand" : "save";
      UtString restorefuncname = (mode == eSaveRestoreOnDemand) ? "restoreOnDemand" : "restore";
      UtString macroname = (mode == eSaveRestoreOnDemand) ? "SRLIST_ONDEMAND_" : "SRLIST_";


      out << "#ifdef CARBON_SAVE_RESTORE" << ENDL;
      out << "  " << TAG << "void " << savefuncname << "(UtOCheckpointStream&, CarbonObjectID *)";
      gCodeGen->emitSection(out, CRYPT("carbon_save_restore_code"));
      out << ";" << ENDL;
      out << "  void " << restorefuncname << "(UtICheckpointStream&, CarbonObjectID *)";
      gCodeGen->emitSection(out, CRYPT("carbon_save_restore_code"));
      out << ";" << ENDL;
      out << "#endif // CARBON_SAVE_RESTORE\n";

      NUNetList srNetsTopLevel, srNetsNested;
      sGetSavedNets (module, &srNetsTopLevel, mode, false);
      sGetSavedNets (module, &srNetsNested, mode, true);

      // begin the macro definition
      out << ENDL;
      out << "#define " << macroname << noprefix (gCodeGen->getClass ()) << "(F,OP,FUN,RW) \\\n";

      // All the top-level members come first
      sGenerateMemberSaveRestore(srNetsTopLevel, mode);
      // Then all the nested members
      sGenerateMemberSaveRestore(srNetsNested, mode);

      // add a line to save each cmodel, but only in normal mode
      if (mode == eSaveRestoreNormal) {
        for (NUCModelCLoop i = module->loopCModels(); !i.atEnd(); ++i) {
          const NUCModel *cmodel = *i;
          out << TAG << "m_" << cmodel->getName()->c_str() << ".FUN(descr); \\\n";
        }
      }

      // add a line to save each submodule instance
      UtString buf;
      for(NUModuleInstanceMultiCLoop i = module->loopInstances (); !i.atEnd (); ++i)
      {
        const NUModule *m = (*i)->getModule ();
        if (m->getClassInstDataSize ())
          out << TAG << member(*i) << ".FUN(F, descr);\\\n";
      }
      
      out << "\n";
   }
  else if (isDefine (context))
    {
      UtOStream& out = gCodeGen->CGOUT ();
      out << "#ifdef CARBON_SAVE_RESTORE\n";
      out << TAG;
      sGenerateSaveMethod (mode);
      out << TAG;
      sGenerateRestoreMethod (mode);
      out << "#endif // CARBON_SAVE_RESTORE\n";
    }
}

// Track the allocation and alignment information and set the basic
// properties in the CGAuxInfo structure maintained for each net.
//
void
allocateNet (const NUNet *net)
{
  if (!net->getCGOp ())
    {
      // We haven't visited this node before, or it'd have a CGAuxInfo...
      // This SHOULD mean that it's never referenced, so we don't need to even
      // emit it.
      net->setCGOp (new CGAuxInfo);
    }

  CGAuxInfo *cgop = net->getCGOp ();

  if ((net->is2DAnything())
      && (not cgop->isUnreferenced () || net->isNonStatic ()) &&
      not net->isHierRef () &&
      net->isAllocated())
    // A memory or a temp memory that's referenced, or a local
    // memory (that might be referenced).
    //
    // Note that hier refs to memories are not sized like the
    // memory. A reference to a memory is no different than a
    // reference to a net which is handled below.
    //
    // With the advent of VHDL we may have downlevel references to
    // memories.  So, need to check the allocated flag.
    {
      const NUMemoryNet *mem = net->getMemoryNet();
  
      // Keep track of largest memory seen
      UInt32 rowSize = mem->getRowSize ();
      if (rowSize > gCodeGen->getMaxMemoryWidth ())
        gCodeGen->putMaxMemoryWidth (rowSize);

      cgop->setMember ();
      cgop->setVCDSymbol ();

      size_t memSize = sizeofMemory (mem);
      
      cgop->setOffset (gCodeGen->allocation (memSize,
					     alignmentMemory (mem), 1));
    }
  else if (cgop->isStaticAssign ())
    ;
  else if (cgop->isPortAliased ())
    ;
  else if (cgop->isUnreferenced () && not net->isPrimaryPort ()
	   && not net->isNonStatic ())
    // Ignore this variable - it's never referenced, so we don't
    // have to generate a declaration.
    cgop->setUnused ();

  else if (net->isAllocated () && net->isPort ())
    {
      if (not net->isNonStatic ())
	{
	  cgop->setVCDSymbol ();
	  cgop->setOffset (gCodeGen->allocation (emitUtil::allocation(net),
						 emitUtil::alignment (net),
						 1));
	}	
    }
  else if (gCodeGen->isLocallyRelativeHierRef(net))
    // A local hier-ref is just a qualified access to a contained
    // scope - it has no allocation
    ;
  else if (not net->isNonStatic ())
    // A local net or something that's dereferenced
    // if it's dereferenced, make it a reference parameter and initialize it
    // in the constructor.
    //
    // Note that local hier refs do not require storage. This is
    // because the storage is in the sub-class.
    {
      if (isDereferenced (net))
	cgop->setDereferenced ();
      else
	cgop->setVCDSymbol();

      cgop->setOffset(gCodeGen->allocation(emitUtil::allocation (net),
					   emitUtil::alignment (net),
					   1));
    }
}

// Find the net associated with this lvalue
NUNet*
sFindNet (const NULvalue *lv)
{
  if (lv->isWholeIdentifier ())
    return lv->getWholeIdentifier ();

  switch (lv->getType ())
    {
    case eNUPartselIndexLvalue:
    case eNUVarselLvalue:
    {
      const NUVarselLvalue* varsel = dynamic_cast<const NUVarselLvalue*>(lv);
      return sFindNet (varsel->getLvalue ());
    }

    case eNUMemselLvalue:
    {
      const NUMemselLvalue* memsel = static_cast<const NUMemselLvalue*>(lv);
      return memsel->getIdent ();
    }

    case eNUConcatLvalue:
      return 0;

    default:
      NU_ASSERT ("Unexpected Lvalue" == 0, lv);
      return 0;
    }
}

NUNet* sFindNet (const NUExpr* rv)
{
  if (rv->isWholeIdentifier ())
    return rv->getWholeIdentifier ();

  switch (rv->getType ())
  {
  default:
    // Somethings just don't have an identifiable name - just gotta live
    // with it.
    return 0;

  case NUExpr::eNUVarselRvalue:
  {
    const NUVarselRvalue* ps = dynamic_cast<const NUVarselRvalue*>(rv);
    return sFindNet (ps->getIdentExpr ());
  }

  case NUExpr::eNUMemselRvalue:
    return dynamic_cast<const NUMemselRvalue*>(rv)->getIdent ();
  }
}

bool sDestOverlapsSrc (const NUAssign *assign, bool noCover)
{
  return assign->overlaps (gCodeGen->getNetRefFactory (), noCover);
}

bool isNetConst(const NUNet* net)
{
/*
 *  Although this worked last week, on 2/26/04 I had to comment
 *  out this assertion because it sun/fire now sees a primary
 *  input that is connected to a tri1 net through a pad assign
 *  in sun_io_lvttlpui.  
 *
 *    input     pad;
 *    tri1      z_in;
 *    assign 	z_in = pad;
 *
 * Something may have changed, possibly in a port coercion commit,
 * or maybe some other incremental changes, that allow z_in and pad 
 * to be assignment-aliased, retaining the tri1 flag, causing
 * the isWritten flag to be given.  I think this could be uncommented
 * if we were careful to make connections to a submodule tri1 be
 * marked as TriWritten, not Written.  I will try to refine that
 * after committing the checkpoint.
 *
 *  if (net->isInput())
 *    NU_ASSERT(!net->isWritten(), net);  // do not write to inputs
*/
  if (!net->isWritten () && !net->isBlockLocal() && !net->isTriWritten()
      //      && !net->isPrimary()

      // Why is HierRefWritten true for a hierarchical reference to an
      // input.  This messes up NUNetHierRef::emitCode's const-ing.
#if BUG1993
      && !net->isHierRefWritten()
#endif
      // Any allocated net must be writable or no value could ever be
      // passed around or initialized (except as a constant in the constructor
      // (and right now, we don't do that!)
      && !net->isAllocated())
  {
    // have to initialize undriven nets that are not inputs or bids,
    // so we better not declare them const
    if (!net->isUndriven() || net->isPort ()) // langcov/hanging_submod_output.v
      return true;
  }
  return false;
}


const char* netConstPrefix(const NUNet* net)
{
  return isNetConst(net) ? "const " : "";
}

bool isWindowsTarget ()
{
  return (gCodeGen->getTarget () == eWINX);
}

bool isNetDeclared(const NUNet* net)
{
  CGAuxInfo *cgop = net->getCGOp ();

  // CGOp should not be NULL.  If you hit this assert, maybe you are
  // not using getStorageNet() to obtain your net?  bug 13181
  NU_ASSERT(cgop, net);
  bool unused = (cgop->isUnreferenced ()
                 && not net->isPrimaryPort ()
                 && not net->isNonStatic ());
  return !unused;
}

// Is this schedule-block a candidate for inlining. 
//
// We should inline if the cost of inlining all the bodies is less than the
// cost of calling N times + the prolog/epilog + the instructions in the function
//
bool isInlineCandidate (const NUUseDefNode *codeBlock, NUCost* cost)

{
  cost->clear ();

  if (NUCost *savedCost = gCodeGen->mCosts->findCost (codeBlock, false))
    cost->addCost (*savedCost, false);
  else
    codeBlock->calcCost (cost, gCodeGen->mCosts);

  if (not gCodeGen->mDoInline)
    return false;               // no inlining at all, please.

  // We cannot inline a block that contains a hierarchical reference to
  // an uplevel class because we cannot forward-declare class members.
  //
  NUTaskEnableVector hierTaskEnables;
  gCodeGen->generateTaskHierRefs (codeBlock, hierTaskEnables);

  for(NUTaskEnableCLoop loop (hierTaskEnables); !loop.atEnd (); ++loop)
  {
    NUTaskEnable* enable = *loop;

    if (!enable->getHierRef ()->isLocallyRelative ())
      return false;
  }

  // Inline really small things where call-cost would dominate or
  // where total uses are few.
  SInt32 instances = cost->mInstances + 1;

  if ((cost->mInstructions <= gCodeGen->mInlineAlwaysInstrLimit) ||
     ((cost->mInstructions * instances) <= gCodeGen->mInlineTotalLimit))
    return true;

  return false;
}

// Print the costing information for a code block
void
annotateCosts (const NUCost& cost)
{
  gCodeGen->CGOUT () << "// calls = "
		     << cost.mInstances
		     << " cost = "
		     << cost.mInstructions
		     << " assigns = "
		     << cost.totalAssigns()
		     << " complexity = "
		     << cost.complexity()
		     << "\n";
}	

NUNet* findAllocatedNet(const NUNetElab* netElab,
                        const STBranchNode** parent)
{
  *parent = netElab->getHier();
  NUNet* net = netElab->getNet();

  // find allocated net...
  const STAliasedLeafNode* storage = netElab->getSymNode()->getStorage();
  NU_ASSERT(storage, netElab);
  NUNet* allocNet = NUNet::find(storage);
  if ((allocNet != NULL) && allocNet->isAllocated()) {
    net = allocNet;
    *parent = netElab->getStorageHier();
  }
  return net;
} // NUNet* findAllocatedNet

bool
isPrimary (const NUExpr* e)
{
  switch (e->getType ()) {
  case NUExpr::eNUIdentRvalue:
  case NUExpr::eNUIdentRvalueElab:
  case NUExpr::eNUVarselRvalue:
  case NUExpr::eNUMemselRvalue:
    return true;
  default:
    return false;
  }
}

bool
functionSection (const NUUseDefNode* func, UtString*sectionName)
{
  UInt32 order = gCodeGen->getLayoutOrder (func);

  if (order == 0) return false;

  // construct a section name that puts low-order things first in lexical ordering.

  if (not sectionName)
    return true;                // No section string needed
  *sectionName = ".text.f";
  do {
    order -= 1;
    (*sectionName) += ('A' + (order % 26));
    order /= 26u;
  } while (order != 0);

  return true;
}

void
genLocalDecls (const NUScope* scope, bool allDecls) {
  NUNetCLoop locals = scope->loopLocals ();
  NUNetCVector sortedLocals;


  if (not allDecls)
    std::remove_copy_if (locals.begin (), locals.end (), std::back_inserter (sortedLocals),
                    std::not1 (std::mem_fun (&NUNet::isNonStatic)));
  else
    std::copy (locals.begin (), locals.end (), std::back_inserter (sortedLocals));
  
  // Sort by address first so eveything lines up
  std::stable_sort (sortedLocals.begin (), sortedLocals.end (),
		    sort_by_address ());

  // Sort the locals such that Memories come before temp memories
  std::stable_sort (sortedLocals.begin (), sortedLocals.end (),
                    sort_by_decl_order ());

  emitCodeList (&sortedLocals, (eCGDeclare | eCGZeroInit), "", ";\n");
}

} // namespace emitUtil

//! If we have already emitted a function, don't emit it twice.
void SimpleList::operator() (const NUUseDefNode *n) const
{
  if (n->isContDriver() and isDefine(ctxt)) {
    if (gCodeGen->inEmittedSet(n)) {
      return;
    }

    if (isFunctionLayout (ctxt))
      gCodeGen->addToEmittedSet(n);
  }

  n->emitCode(ctxt);
}

static const char* contextNames[]={
  "void",
  "bitfield",	"huge",		"lvalue",	"declare",
  "define",	"schedule",	"strengthset",	"nameonly",
  "iface",	"hiername",	"main",		"noprefix",
  "list",	"forcible",	"flow",		"precise",
  "marknets",	"constructor",	"delayed",	"needsatemp",
  "isatemp",	"isSSA",	"nosemi",	"constant",
  "zeroinit",	"inline",	"initial",	"condflow",
  "2ndpass",	"functor",	"access",	"cyclecopy",
  "bvOlSup",	"bvExpr",	"needextension","preciseprimary",
  "DownToValid","DirtyOK",	"OpEqual"
};


//! print human-readable context flags
void
printContext (CGContext_t c)
{
  UtIO::cout () << UtIO::hex << "(" << c << ") = ";

  if (c == eCGVoid) {
    UtIO::cout () << "void\n";
    return;
  }
    
  for (UInt32 bit=0; bit < (sizeof(c) * 8); ++bit)
  {
    CGContext_t mask (1ULL << bit);
    if (c & mask) {
      UInt32 cindex = bit + 1;
      if (cindex < (sizeof(contextNames) / sizeof(const char*))) {
        UtIO::cout () << contextNames[bit+1] << " ";
      }
      else {
        UtIO::cout() << "*INVALID-CONTEXT-NAME* ";
      }
    }
  }

  UtIO::cout () << "\n";
  return;
}

//!Functor class for port aliased members (input, output or bidi)
/*!
 * Algorithm adaptor class to generate class methods to access values
 * aliased to bidirect or output declared in an instantiated class memeber.
 */
class AliasDecl: std::unary_function<const NUPortConnection, CGContext_t>
{
  //! Holds instantiation name hierarchy
  NUModuleInstance &mInstance;
  CGContext_t mCtxt;		// Context to use

public:
  //! Generate member accessor functions for aliased parameters.
  CGContext_t operator () (const NUPortConnection *port) const
  {
    const NUNet *actual = port->getActualNet();
    const NUNet* formal = port->getFormal ();

    NU_ASSERT(!port->isUnconnectedActual (), port);

    // Emit the member accessor function for "a", the variable
    // declared in this module, and bound to the formal "f"
    // in the instantiation.

    UtOStream& out = gCodeGen->CGOUT ();

    // Make sure we've got a cgop...
    formal->emitCode (eCGVoid);
    actual->emitCode (eCGVoid);

    if (actual->isAllocated ())
      // we have to allocate storage, can't make this a port alias
      return eCGVoid;

    CGAuxInfo* aCGOp = actual->getCGOp ();
    if (aCGOp->hasAccessor ())
      return eCGVoid;

    // one of the child modules covers the allocation, but not this one.
    CGAuxInfo *fCGOp = formal->getCGOp ();
    if (not formal->isAllocated() and 
        not fCGOp->isPortAllocated() and
        (actual->isHierRef() or aCGOp->isPortAllocated()))
      return eCGVoid;

    if (not actual->isHierRef()) {
      aCGOp->setPortAliased ();
    }

    if (formal->isAllocated() or fCGOp->isPortAllocated()) {
      if (not actual->isHierRef()) {
        aCGOp->setPortAllocated();
      }
    }

    if (actual->isBlockLocal () && not actual->isNonStatic ())
      // Mark this net as a VHDL record member port-aliased so that
      // we know how to print the name.  Do this even if it's not an expanded port,
      // but merely a port-alias connected to a specific record-member.
      aCGOp->setVhdlRecordPort ();


    if (!emitUtil::isNetReferenced(actual))
      // If the actual is unreferenced then we don't need to
      // materialize an accessor function.  This is a dead port that
      // doesn't require connection
      return eCGVoid;

    // We're going to uplevel access this, mark it as referenced so that
    // we generate an accessor and/or allocation.
    fCGOp->setReferenced ();

    if (not isDeclare (mCtxt))
      // Not writing out the .h file yet...
      return eCGVoid;

    if (actual->isHierRef()) {
      return eCGVoid;
    }

    // We really have to have a get_XXX() method available, either because
    // the net we're binding to is allocated in the connected module, or
    // because it's an aliased connection to some other module.
    //
#if 0
    NU_ASSERT (fCGOp->hasAccessor (), formal);
#endif

    // A port-alias doesn't have any allocated storage, but it does
    // need to get various CGAUxInfo flags set

    emitUtil::allocateNet (actual);

    /*
     * Construct a member accessor function of the form:
     *
     *    T& get_foo_bar() { return foo.m_bar; }
     *
     */
    out << "  ";

    // bug 14514, if signed/unsigned mismatch then don't use const prefix because
    // it will cause a local to be created, and you cannot return a reference to that local.
    const char* const_attr = ( actual->isSigned() == formal->isSigned() ) ? emitUtil::netConstPrefix (actual) : "";
    
    out << TAG << const_attr << emitUtil::CType (actual) << "& ";
    actual->emitCode (eCGDefine | eCGAccess);
    out << " " << TAG << "ALWAYS_INLINE { return "
      // May be related to BUG2033
	<< "(" << const_attr << emitUtil::CType (actual) << "&)"
	<< emitUtil::member (&mInstance) // instance name
	<< ".";
    formal->emitCode (eCGDefine | eCGNameOnly);

    out << ";}" << ENDL;

    // Remember that we constructed an accessor for this name.
    aCGOp->setAccessor ();

    return eCGVoid;
  }

  //! Trivial constructor
  AliasDecl(NUModuleInstance & inst, CGContext_t context)
    : mInstance(inst), mCtxt (context) {}
};


// Handle Bidi's and outputs that are aliased to lower-level instantiations.
// Allocate them first, because anything that's left has to become a
// normal ref parameter initialized by constructor when our parent
// instantiates an instance of this class.
//
// This is done by finding all the NUPortConnection (Output|Bid)
// assignments and generating member functions if the port appears to
// be aliased.
namespace emitUtil {
void
emitAliasedPortConnections (const NUModule* mod, CGContext_t context)
{
  UtOStream& hout = gCodeGen->CGOUT ();
  // Mark all actuals passed to sub-instantiations as referenced
  // so that we catch any ports that are just passed thru.
  NUCModelCLoop cmodels = mod->loopCModels ();
  if (!cmodels.atEnd())
  {
    emitCodeList (&cmodels, eCGMarkNets);
  }	

  if (isDeclare (context))
    hout << "  // aliased in, out and inout parameters" << ENDL;

  NUModuleInstanceMultiCLoop instances = mod->loopInstances ();

  for (NUModuleInstanceMultiCLoop::iterator i = instances.begin();
       i != instances.end(); ++i)
  {
    NUPortConnectionList ports;
    (*i)->getPortConnections (ports);

    std::for_each (ports.begin(), ports.end(),
                   AliasDecl(**i, context));
  }
}


// Generate a simple accessor function for net N, e.g.
//            T& get_N() ALWAYS_INLINE { return m_N; }
void generateAccessorFunction (const NUNet* n)
{
  if (n->isNonStatic ())
    return;    // Can't have a function inside a function

  UtOStream& out = gCodeGen->CGOUT ();
  const char * cqual = netConstPrefix (n);

  out << cqual << TAG << CType (n) << "& " << accessor (n)
      << cqual << " ALWAYS_INLINE { return " << member (n)
      << ";}\n    ";

  n->getCGOp ()->setAccessor ();
}

// Generate the simple member variable declaration for net N, e.g.
//            T  m_N;  // align, offset
// or         T& m_N;
void generateMemberVariable (const NUNet* n, bool deref)
{
  UtOStream &out = gCodeGen->CGOUT ();

  const char * cqual = netConstPrefix (n);
  out << TAG << cqual << CType (n);
  if (deref)
    out << "& ";
  out << member (n);
              
  CGAuxInfo* cgop = n->getCGOp ();
  if (cgop->hasOffset ())
    out << "; // " << alignment (n) << ", " << cgop->getOffset () << ENDL;
}

bool isCallOnPlaybackCModel(const NUCModel* cmodel)
{
  const NUCModelInterface* cmodelInterface = cmodel->getCModelInterface();
  return cmodelInterface->isCallOnPlayback() &&
    gCodeGen->hasCModelCalls(cmodel);
}

bool isOnDemandStateNet(const NUNet *net)
{
  // This set is built during the scheduler pass.  It contains all nets
  // that are sequential or transition scheduled.
  NUDesign *design = const_cast<NUDesign*>(gCodeGen->getDesign());
  NUConstNetSet *ondemand_nets = design->getOnDemandNets();
  return (ondemand_nets->find(net) != ondemand_nets->end());
}

void pruneOnDemandNets()
{
  // Get the list of all sequential and transition nets
  NUDesign *design = const_cast<NUDesign*>(gCodeGen->getDesign());
  NUConstNetSet *ondemand_nets = design->getOnDemandNets();
  NUConstNetSet memoriesNotSaved;
  
  // A net is part of onDemand state if it's in this set and not a memory.
  // However, non-sparse memories below a certain threshold are included in
  // the state.

  // Iterate over the entire set, removing nets that don't meet the
  // above criteria.

  NUConstNetSet prune_set;
  for (NUConstNetSet::UnsortedLoop l(*ondemand_nets); !l.atEnd(); ++l) {
    const NUNet *net = *l;
    bool prune = false;

    // VHDL line nets won't be saved either.  Any VHDL file I/O (just
    // like Verilog system tasks) aren't active during onDemand
    // playback.  The line net class has its own save/restore function
    // that isn't compatible with onDemand's.
    if (net->isVHDLLineNet()) {
      prune = true;
    } else if (net->isMemoryNet()) {
      // Cast to a memory and check its sparseness/size
      const NUMemoryNet *mem = net->getMemoryNet();
      NU_ASSERT(mem, net);
      UInt32 bit_size = mem->getBitSize();
      SInt32 mem_threshold;
      gCodeGen->getCarbonContext()->getArgs()->getIntLast(CRYPT("-onDemandMemoryStateLimit"), &mem_threshold);

      if ((bit_size > static_cast<UInt32>(mem_threshold))
          || (gCodeGen->isSparseMemoryBitSize(bit_size))) {
        prune = true;
        // Memories whose state isn't saved need additional
        // consideration later.
        memoriesNotSaved.insert(net);
      }
    }

    if (prune) {
      prune_set.insert(net);
    }
  }

  ondemand_nets->eraseSet(prune_set);

  // We may need to do a different kind of pruning, too.  If we're not
  // saving the memory's state, we can't allow it to be an idle
  // deposit net.  While the CarbonMemory API can't be used for idle
  // deposits, an ordinary carbonDeposit may be done to an expression
  // net that wraps a memory.
  IODBNucleus* iodb = design->getIODB();
  STAliasedLeafNode::LeafNodeVec nodesToPrune;
  for (IODB::NameSetLoop l = iodb->loopOnDemandIdleDeposit(); !l.atEnd(); ++l) {
    STSymbolTableNode* node = *l;
    STAliasedLeafNode* leaf = node->castLeaf();
    ST_ASSERT(leaf != NULL, node);
    // Get the corresponding NUNet and NUNetElab
    NUNetElab* netElab = NUNetElab::find(leaf);
    NUNet* net = NUNet::find(leaf);
    // If not saved, warn the user and remove the idle deposit flag.
    if (memoriesNotSaved.count(net) > 0) {
      gCodeGen->getMsgContext()->CGOnDemandIdleDepositMemory(netElab);
      nodesToPrune.push_back(leaf);
    }
  }

  for (STAliasedLeafNode::LeafNodeVec::iterator iter = nodesToPrune.begin();
       iter != nodesToPrune.end(); ++iter) {
    iodb->removeOnDemandIdleDeposit(*iter);
  }
}

} // end namespace emitUtil
