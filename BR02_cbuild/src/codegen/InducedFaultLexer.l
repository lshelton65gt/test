%{
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/* \file InducedFaultLexer.l
 *
 * When -induceCodegenFaults is specified a file is read with a list of faults
 * to inject into the output stream. This is the flex scanner specification for
 * the induced fault file.
 *
 * The scanner recognised a few keywords, integers, quoted and simple
 * unquoted strings.
 */

#include "util/CarbonTypes.h"
#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "InducedFaultParser.hxx"

void InducedFault_error (const char *fmt, ...);

%}

%option yylineno
%option noyywrap
%option nounput
/* To avoid isatty problems on Windows. */
%option never-interactive

%x STRING
%x STRING1

%x COMMENT

%%

  /* comments */
"#"              { BEGIN (COMMENT); }
<COMMENT>[^\n]*  { /* ignore */ }
<COMMENT>\n      { BEGIN (INITIAL); }

  /* keywords */
"yydebug"	  { return _YYDEBUG_; }
"fault"           { return _FAULT_; }
"append"          { return _APPEND_; }
"prepend"         { return _PREPEND_; }

  /* operators */
";"        { return ';'; }


  /* quoted strings */
\"                { BEGIN (STRING); }
<STRING>[^"\n]*   { InducedFault_lval.string = StringUtil::dup (yytext); BEGIN (STRING1); return _STRING_; }
<STRING>\"        { InducedFault_lval.string = StringUtil::dup (""); BEGIN (INITIAL); return _STRING_; }
<STRING>\n        { InducedFault_error ("unterminated string"); BEGIN (INITIAL); }
<STRING1>\"       { BEGIN (INITIAL); }
<STRING1>\n       { InducedFault_error ("unterminated string"); BEGIN (INITIAL); }
<STRING1>.        { InducedFault_error ("lexical error"); BEGIN (INITIAL); }

  /* integers */
[0-9][0-9]*       { InducedFault_lval.integer = strtoul (yytext, NULL, 10); return _INTEGER_; }

  /* simple unquoted strings - these are also used for filenames */
[a-zA-Z][a-zA-Z0-9._]* { InducedFault_lval.string = StringUtil::dup (yytext); return _STRING_; }

  /* whitespace */
[ \n\t]           { /* do nothing */ }

  /* anything else is a syntax error */
.                 { InducedFault_error ("unexpected character: '%s'", yytext); }
