# -*- Makefile -*-
#
# *****************************************************************************
# Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
# *****************************************************************************
#
# compiler-dependent makefile fragment
#
# winx is the Unix-hosted mingw gcc3 cross target build for windows.

CARBON_TARGET_ARCH := Win

# Define where compiler can be found
ifndef CARBON_COMPILER_DIST
  # This is protected by ifndef so that a test can be written to check the
  # behaviour when the cross-compiler is not found.
  CARBON_COMPILER_DIST = ${CARBON_HOME}/Win/winx
endif

# Where gcc wants to look for libraries
CARBON_COMPILER_LIB_DIRS = $(CARBON_COMPILER_DIST)/lib

ifneq (,$(wildcard $(CARBON_COMPILER_DIST)/libexec/gcc/i386-mingw32msvc/4.*))
  CARBON_GCCVER=$(notdir $(wildcard $(CARBON_COMPILER_DIST)/libexec/gcc/i386-mingw32msvc/4.*))
  CARBON_COMPILER_SUPPORTS_PCH=1
  CARBON_GCC4=1
  CARBON_FOUND_CROSS_COMPILER=1
endif

ifneq (,$(wildcard $(CARBON_COMPILER_DIST)/libexec/gcc/i386-mingw32msvc/3.4.*))
  CARBON_GCCVER=$(notdir $(wildcard $(CARBON_COMPILER_DIST)/libexec/gcc/i386-mingw32msvc/3.4.*))
  CARBON_COMPILER_SUPPORTS_PCH=1
  CARBON_GCC3=1
  CARBON_FOUND_CROSS_COMPILER=1
endif

ifneq (,$(wildcard $(CARBON_COMPILER_DIST)/lib/gcc-lib/i386-mingw32msvc/3.3.3*))
  CARBON_GCCVER=3.3.3
  CARBON_GCC3=1
  CARBON_FOUND_CROSS_COMPILER=1
endif

ifneq ($(CARBON_HOST_ARCH),Win)
ifneq (1,$(CARBON_FOUND_CROSS_COMPILER))
  $(error Installation error: Windows cross-compiler is not installed.)
endif
endif

CARBON_LIBRARY_PATH := $(CARBON_HOME)/$(CARBON_HOST_ARCH)/gcc/lib

ifeq ($(CARBON_HOST_ARCH),Win)
CARBON_CBUILD_CC  = $(shell cygpath -m /usr/bin/gcc.exe)
CARBON_CBUILD_CXX = $(shell cygpath -m /usr/bin/g++.exe)
CARBON_AR := $(shell cygpath -m /usr/bin/ar.exe)
CARBON_RANLIB := $(shell cygpath -m /usr/bin/ranlib.exe)
else
CARBON_CBUILD_CC  = $(CARBON_COMPILER_DIST)/bin/i386-mingw32msvc-gcc
CARBON_CBUILD_CXX = $(CARBON_COMPILER_DIST)/bin/i386-mingw32msvc-g++
CARBON_AR := ${CARBON_COMPILER_DIST}/bin/i386-mingw32msvc-ar
CARBON_RANLIB := $(CARBON_COMPILER_DIST)/bin/i386-mingw32msvc-ranlib

# Use "env -i" to prevent user's environment from causing problems.
# We set COMPILER_PATH to find our copies of tools like `as' and `ld'.
CARBON_COMPILER_ENV = env -i LANG=C COMPILER_PATH=$(CARBON_COMPILER_DIST) \
		      HOME=$(HOME) LD_LIBRARY_PATH=$(CARBON_LIBRARY_PATH)
endif
CARBON_CBUILD_LD  = $(CARBON_CBUILD_CXX)

CARBON_DLLTOOL := $(CARBON_COMPILER_DIST)/bin/i386-mingw32msvc-dlltool
CARBON_NM := $(CARBON_COMPILER_DIST)/bin/i386-mingw32msvc-nm
CARBON_CXXFILT := $(CARBON_COMPILER_DIST)/bin/i386-mingw32msvc-c++filt

ifneq (,$(findstring -pg,$(CXX_EXTRA_FLAGS) $(CXX_WC_FLAGS)))
CARBON_PROFILING=1
endif


# Indicate that we're cross-compiling for windows.
CARBON_WINX=1
##

ifneq (,$(findstring -pg,$(CXX_EXTRA_FLAGS) $(CXX_WC_FLAGS)))
CARBON_PROFILING=1
endif

CARBON_optimize=0
ifneq (,$(findstring -O2,$(CXX_EXTRA_FLAGS)))
  CARBON_optimize=2
endif
ifneq (,$(findstring -O3,$(CXX_EXTRA_FLAGS)))
  CARBON_optimize=3
endif
ifneq (,$(findstring -Os,$(CXX_EXTRA_FLAGS)))
  CARBON_optimize=4
endif

ifneq ($(CARBON_optimize),0)
  ifeq (4,CARBON_optimize)
    CARBON_OPTFLAGS = -march=pentium4 -msse
  else
    CARBON_OPTFLAGS = -march=pentium4 -msse
  endif

  # options useful when you want to profile with gprof
  ifeq ($(CARBON_PROFILING),1)
    CARBON_OPTFLAGS += -fno-default-inline -fno-inline
  else
    CARBON_OPTFLAGS += -finline-limit=4000
    ifeq (1,$(CARBON_GCC4))
	CARBON_OPTFLAGS +=  -ftracer -ftree-vectorize --param large-function-growth=250 \
	        -finline-functions -finline-limit=2000 -fno-strict-aliasing
    endif
  endif
else	# not -O3 or -O2
  CARBON_OPTFLAGS =
endif

CARBON_EXCEPTIONS_OFF:=-fno-exceptions
CARBON_EXCEPTIONS_ON :=-fexceptions

CARBON_DISABLE_DYNAMIC_CAST = -fno-rtti
CARBON_ENABLE_DYNAMIC_CAST = -frtti


# Default flag made visible to customers for tweaking CARBON_CXX settings
#
CARBON_CXX_FLAGS = $(CARBON_EXCEPTIONS_OFF)

# Basic compiler-dependent flags
#
CARBON_CXX_DEF_COMPILE_FLAGS = -ftemplate-depth-80 $(CARBON_CXX_FLAGS)
CARBON_CXX_SPEEDCC_COMPILE_FLAGS = -pipe $(CARBON_CXX_FLAGS) $(CARBON_DISABLE_DYNAMIC_CAST)

ifeq ($(CARBON_PROFILING),1)
else
ifeq ($(GCC_NO_WALL),)
# Need aliasing warnings suppressed
CARBON_CXX_SPEEDCC_COMPILE_FLAGS += -Wall -Wextra -Werror -Wno-uninitialized -Wno-strict-aliasing -fno-strict-aliasing
endif
endif

CARBON_CXX_SPEEDCC_COMPILE_FLAGS += $(CXX_WC_FLAGS)

CARBON_CXX_DEF_COMPILE_FLAGS = $(CARBON_CXX_FLAGS)
CARBON_CC_DEF_COMPILE_FLAGS = $(CARBON_CXX_FLAGS)

# While our model must be compiled with exceptions disabled, the final link should
# have exceptions enabled. Otherwise, there *will* be problems if the test bench
# is compiled with mingw and it touches libstdc++. In particular, writing to cerr
# will induce an unhandled exception if exceptions are disabled. Given that customers
# are expected to use Visual C++ for test bench compilation this is probably an issue
# that only affects the Carbon testing infrastructure rather than customers.
CARBON_CXX_DEF_LINK_FLAGS = $(strip $(CARBON_EXCEPTIONS_ON) $(filter-out $(CARBON_EXCEPTIONS_OFF), $(CARBON_CXX_FLAGS)))

# flags needed to compile shared libraries
CARBON_SHARED_COMPILE_FLAGS = -shared

# flags needed to link shared lib.  Note that we must include
# references to all carbon libs. CARBON_SHARED_LIB_LIST isn't defined
# yet, but that's okay.  It will be when we expand CARBON_SHARED_LINK_FLAGS
#
# We create a .def file so that Win32's LIB command can create an
# import library.  We don't need an import library for MINGW link,
# it can process dlls directly.
#
CARBON_SHARED_LINK_FLAGS = -Wl,-rpath,$(@D) -Wl,--output-def,$(@:.dll=.def)	\
		-Wl,--out-implib,$(@:.dll=-mingw-imp.lib)			\
		-shared $(CARBON_LIBRARIES)
