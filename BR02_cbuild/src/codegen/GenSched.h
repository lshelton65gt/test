// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef _GENSCHED_H
#define _GENSCHED_H
 /*
 * \file
 */
/*!
 * Class to perform code generation for schedule functions.
 * This class knows how to generate the function prototype for the various
 * special cases including:
 *
 * \item C language callable
 * \item separately compiled
 *
 * It can generate a function prototype that will be emitted into the
 * schedule.cxx file while generating the schedule function into a separate
 * file (depending on how large the schedule function is).
 *
 * It can also generate the code for the call to the function.
 */

#include "schedule/Schedule.h"
#include "util/UtString.h"
#include "nucleus/NUCost.h"

class STBranchNode;
class NUModule;
class CGOFiles;
class ScheduleBucket;

//! Affect how function signature is generated
enum ProtoFlags {
  eCFunction       = 1<<0,              //!< Callable from C
  eHandleArg       = 1<<1,              //!< Function takes a handle
  eClassMember     = 1<<2,              //!< Function is class member
  eCompiled        = 1<<3,              //!< Function body has been generated
  eSeparate        = 1<<4,              //!< Function body separately compiled
  eSharedHeirarchy = 1<<5,              //!< Schedule function is shared
  eCycle           = 1<<6,              //!< Generate a cycle resolver
  eTopLevel        = 1<<7               //!< suppress common hierarchy computation
};

class GenSched
{
  GenSched (GenSched&);		//! Unavailable
  GenSched ();			//! No copy constructors or def constructors
public:
  //! Construct a schedule generator
  GenSched (const char*fname, SCHCombinational *s,  CGOFiles *fileIOs, int flags=0, const char *section=0,
            bool suppressObfuscate = false);

  GenSched (const char*fname, SCHSequential *s,  CGOFiles *fileIOs, ClockEdge e, int flags=0, const char *section=0);

  //! Overloaded to construct a cycle generator
  GenSched (const char* fname, NUCycle* cycle, CGOFiles *fileIOs, const char* section=0);

  bool isCFunction () const      { return mFlags & eCFunction;   }
  bool hasHandleArg () const     { return mFlags & eHandleArg;   }
  bool isClassMember () const    { return mFlags & eClassMember; }
  bool isGenerated () const      { return mFlags & eCompiled;    }
  bool isSeparate () const       { return mFlags & eSeparate;    }
  bool isCycle () const          { return mFlags & eCycle;       }
  bool isTopLevel () const       { return mFlags & eTopLevel;    }

  void setGenerated () { mFlags |= eCompiled;}
  void setSeparate () { mFlags |= eSeparate;}

  int getFlags () const { return mFlags; }
  //! Cleanup
  ~GenSched () {}

  //! Generate the schedule function body
  void genMethod (void);

  //! Generate upto a fixed number of method calls
  void genSomeCalls (int limit, NUCost* costs=0);

  //! Generate the function declarations for inner unrollings
  void declareInnerFunctions (void);

  //! Generate the function framework for inner unrollings
  void genInnerFunctions (void);

  //! Call the Inner functions
  void callInnerFunctions (void);

  //! Conditionally flip to a new sch-*.cxx file.
  void maybeSchedFileSpill(int fn_size);

  void getInnerFunctionPrototype(UtString & proto, int nfuncs);

  //! Generate function prototype
  const UtString genPrototype (void) const;

  //! Generate a call on the associated schedule function
  const UtString genCall (void) const;

  //! How expensive is this schedule to run
  int getCost (void) const;

  //! Are two schedules equivalent (the same sequence of operations)
  bool isEquivalent (const GenSched&) const;

  //! size of the schedule
  int getSize () const { return mSize; }

  //! The schedule's name
  StringAtom* getSchedName () const { return mSchedName; }

  //! Get the schedule name, sanitized for use as a C Identifier
  StringAtom* getCFuncName() const { return mCName; }

  //! The set iterator
  SCHIterator getIterator () const { return mIterator; }

  //! The common hierarchy
  STBranchNode* getRoot () const { return mRootHierarchy; }

  //! The key for comparing two schedules
  STBranchNode* getKey () const;

  //! Is this schedule an alias for another schedule?
  bool isEquivalent () const { return mEquivalent != 0; }

  GenSched* getEquivalent () const { return mEquivalent; }

  void setEquivalent (GenSched *other) { mEquivalent = other; }


  //! Return NUModule specifying class of this symbol
  static const NUModule* getClass (const STBranchNode*);

  static const int LARGEST_INLINE_FUNCTION = 100;
  static const int LINES_PER_SCHEDULE_SPILL_FILE = 10000;
  CGOFiles * getCGOFiles() { return mCGOFiles;}

  //! is empty and we don't need to generate it for external consumption?
  bool emptySchedule (void) const;

private:
  typedef std::pair<StringAtom*,SInt32> SymInt;
  void calcCost();

  //! Identify the common class-member selector heirarchy top.foo.bar...
  void findCommonHierarchy (void);

  //! set up the C++ friendly name of this schedule
  void setupCName(const char* fname, bool suppressObfuscate);

  StringAtom* mSchedName;       //! detailed schedule name (including masks, etc)
  StringAtom* mCName;           //! C++-friendly name (including obfucscation)
  const UtString mSection;      //! Place schedule into this section
  SCHIterator mIterator;	//! Defines the computations scheduled
  SCHIterator mGenerator;	//! Copy of mIterator used to generate subfunctions
  CGOFiles *mCGOFiles;
  NUCost mCost;                 //! Total cost for this schedule
  STBranchNode* mRootHierarchy;	//! common schedule heirarchy
  GenSched * mEquivalent;	//! an existing equivalent schedule
  NUCycle* mCycle;              //! A schedule cycle
  int mFlags;			//! Flags describing the sched function
  int mSize;			//! Size (# of calls?) of schedule
  int mBucketId;                //! Bucket id for profiling use
  UtArray<ScheduleBucket*> mChildSchedBuckets; //! Sample schedule profiling buckets of children
  bool mEmpty;			//! Schedule is empty
};

#endif // _GENSCHED_H
