// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "codegen/CGNetElabCount.h"
#include "nucleus/NUNet.h" // NU_ASSERT
#include "util/LoopMap.h"

#define UNCOUNTED_INDEX -1

CGNetElabCount::CGNetElabCount() : mCount(0)
{
  mMap = new NetToIntMap;
}

CGNetElabCount::~CGNetElabCount()
{
  delete mMap;
}

int CGNetElabCount::insert(const NUNetElab* netElab)
{
  int ret = -1;
  NetToIntMap::const_iterator p = mMap->find(netElab);
  if (p == mMap->end())
  {
    (*mMap)[netElab] = mCount;
    ++mCount;
  }
  else
    ret = p->second;
  
  return ret;
}

void CGNetElabCount::insertNoCount(const NUNetElab* netElab)
{
  NU_ASSERT(! exists(netElab), netElab);
  (*mMap)[netElab] = UNCOUNTED_INDEX;
}

bool CGNetElabCount::exists(const NUNetElab* netElab) const
{
  return (mMap->find(netElab) != mMap->end());
}

int CGNetElabCount::getIndex(const NUNetElab* netElab) const
{
  NetToIntMap::const_iterator p = mMap->find(netElab);
  NU_ASSERT(p != mMap->end(), netElab);
  
  return p->second;
}

UInt32 CGNetElabCount::size() const
{
  return mMap->size();
}

CGNetElabCount::NetElabMapIter 
CGNetElabCount::loopNetElabs()
{
  typedef LoopMap<NetToIntMap> NetElabLooper;
  return NetElabMapIter::create(NetElabLooper(*mMap));
}
