// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "codegen/Blob.h"
#include "codegen/CGProfile.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUStructuredProc.h"


Blob::Blob(const SourceLocator& loc, const NUBase* node, const NUModule* parent) :
  mSourceLoc(loc),
  mBucket(0),
  mTypeStr(node->typeStr()),
  mParent(parent)
{
}

// Constructor used for creating the non-Blob.
Blob::Blob(const SourceLocator& loc, const char* type) :
  mSourceLoc(loc),
  mBucket(0),
  mTypeStr(type),
  mParent(NULL)
{
}

BlobMap::BlobMap(CodeGen* code_gen)
  : mBlobSet(NULL),
    mCodeGen(code_gen)
{
  SourceLocatorFactory f;
  mNonBlob = new Blob(f.create("(none)", 0), "(none)");
  mNonBlob->setBucket(code_gen->getBucketID()->next());
}

BlobMap::~BlobMap()
{
  // Delete the blobs stored in the map, and write them out to the
  // compile-time database.

  // First the nonBlob Blob.
  if (mBlobs.size() > 0)
    mCodeGen->getCGProfile()->recordBucket(mNonBlob);
  delete mNonBlob;

  for (VecMap::iterator map_iter = mBlobs.begin();
       map_iter != mBlobs.end();
       ++map_iter) {
    for (BlobVec::iterator vec_iter = map_iter->second.begin();
         vec_iter != map_iter->second.end();
         ++vec_iter) {
      const Blob* blob = vec_iter->second;
      
      // Add blob to compile-time database.
      mCodeGen->getCGProfile()->recordBucket(blob);
      delete blob;  // Delete the blob
    }
  }
}

void
BlobMap::insert(const NUUseDefNode* node, const NUModule* parent)
{
  Blob* blob = new Blob(node->getLoc(), node, parent);
  // Keep track of inserted blobs in the BlobSet.
  if (mBlobSet->insert(blob).second)
    blob->setBucket(mCodeGen->getBucketID()->next());
  else
    // Insertion failed -- blob already defined, delete this one.
    delete blob;
}

const Blob*
BlobMap::lookup(SourceLocator stmtLoc)
{
  // Search through the line numbers in the vector for the given file.
  BlobVec& vec = mBlobs[stmtLoc.getFile()];
  UtString loc_str;
  stmtLoc.compose(&loc_str);
  if (vec.empty()) {
#if VERBOSE
    UtIO::cerr() <<
              UtString(UtString("No Blobs found for file: ")
                       + loc_str + '\n').c_str();//);
#endif
    return mNonBlob;
  }
  return binarySearch(vec, stmtLoc.getLine());
}

const Blob*
BlobMap::binarySearch(const BlobVec& vec, UInt32 line)
{
  // Unfortunately std::binary_search() returns only a bool rather
  // than a way to get the item found, so we can't use it.

  // We need to return the Blob with the greatest line number less
  // than or equal to the one we're searching for.

  // Based on algorithm found at
  // http://www.nist.gov/dads/HTML/binarySearch.html.
  int low, high;
  for (low = -1, high = vec.size(); high-low > 1;) {
    int middle = (low + high) / 2;
    if (line >= vec[middle].first)
      low = middle;
    else
      high = middle;
  }

  // If line is less than vec[0], low will be -1, and the statement
  // was before the first blob found in the file.
  if (low < 0) {
#if VERBOSE
    UtIO::cerr() << "No blob found containing statement " << line << '\n';
#endif
    return mNonBlob;
  }
  return vec[low].second;
}

//! Callback class to define blobs.
class BlobCallback : public NUDesignCallback
{
public:
  BlobCallback(BlobMap* blob_map) : mBlobMap(blob_map) {}
  Status operator()(Phase, NUBase*) { return eNormal; }
  Status operator()(Phase p, NUStructuredProc* n)    { return addBlob(p, (NUUseDefNode*)n); }
  Status operator()(Phase p, NUContAssign* n)        { return addBlob(p, (NUUseDefNode*)n); }
  Status operator()(Phase p, NUContEnabledDriver* n) { return addBlob(p, (NUUseDefNode*)n); }
  Status operator()(Phase p, NUTF* n)                { return addBlob(p, (NUUseDefNode*)n); }

  //! Keep track of the parent module for all blobs
  Status operator()(Phase phase, NUModule* module)
  {
    if (phase == ePre) {
      // Push
      mModuleStack.push_back(module);
    } else {
      // Pop
      mModuleStack.pop_back();
    }
    return eNormal;
  }

  // TODO: Make sure C Models, UDP tables, primitive gate instances
  // are handled.
private:
  Status addBlob(Phase phase, NUUseDefNode* node) {
    if (phase == ePre) {
      NU_ASSERT(!mModuleStack.empty(), node);
      const NUModule* parent = mModuleStack.back();
      mBlobMap->insert(node, parent);
    }
    return eNormal;
  }
    
  BlobMap* mBlobMap;
  UtVector<const NUModule*> mModuleStack;
};

//! Walk the design, finding and storing Blobs.
void
BlobMap::findBlobs(NUDesign* design)
{
  mBlobSet = new BlobSet;

  // Do a design walk to find the locations of all the blobs.
  BlobCallback blob_callback(this);
  NUDesignWalker walker(blob_callback, false);
  walker.design(design);

  // We now have all the blobs in a BlobSet.  We walk through them and
  // put them in the VecMap in order, so we can look them up while
  // generating code with a binary search.
  for (BlobSet::iterator set_iter = mBlobSet->begin();
       set_iter != mBlobSet->end();
       ++set_iter) {
    const SourceLocator& loc = (*set_iter)->getLoc();
    mBlobs[loc.getFile()].push_back(std::make_pair(loc.getLine(), *set_iter));
  }

  delete mBlobSet;
  mBlobSet = NULL;
}
