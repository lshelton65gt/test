find_package(BISON REQUIRED)
find_package(FLEX REQUIRED)

# Nothing to be done here with MSVC.  This will be done in the GCC pass for Windows.

if(${MSVC})
  return()
endif()

# SysIncludes.h
set(SysIncludesH ${CARBON_OBJ}/codegen/SysIncludes.h)
set(SysIncludesCXX ${CMAKE_CURRENT_SOURCE_DIR}/BuildSysIncludes.cxx)
if(${WIN32})
  # Cross-compiler can't use PCH, so just copy the file
  add_custom_command(OUTPUT ${SysIncludesH}
                     COMMAND ${CMAKE_COMMAND} -E make_directory ${CARBON_OBJ}/codegen
                     COMMAND ${CMAKE_COMMAND} -E copy ${SysIncludesCXX} ${SysIncludesH}
                     DEPENDS ${SysIncludesCXX}
                     )
else()
  set(HACKFLAG "")
  set(PRECOMPILE_INCLUDES
      -I${CARBON_HOME}/src/inc
      -I${CARBON_HOME}/src/inc/util
      -I${CARBON_HOME}/src/inc/shell
      -I${CARBON_HOME}/include
      )

  add_custom_command(OUTPUT ${SysIncludesH}
                     COMMAND mkdir -p ${CARBON_OBJ}/codegen
                     COMMAND ${ECHO} "\\#ifndef __CARBON_SYS_INCLUDE_H__"                                                                            > ${SysIncludesH}
                     COMMAND ${ECHO} "\\#define __CARBON_SYS_INCLUDE_H__"                                                                            >>${SysIncludesH}
                     COMMAND ${ECHO} "\\#ifdef CARBON_USE_SYS_INCLUDES"                                                                              >>${SysIncludesH}
                     COMMAND cat ${SysIncludesCXX}                                                                                                   >>${SysIncludesH}
                     COMMAND ${ECHO} "\\#else"                                                                                                       >>${SysIncludesH}
                     COMMAND ${CXX_COMPILER_RAW} -E -dD ${PRECOMPILE_INCLUDES} -fno-exceptions ${SysIncludesCXX}                                     > ${SysIncludesH}.raw
                     COMMAND ${CARBON_HOME}/scripts/hack_sys_include ${HACKFLAG}                                                                     ${SysIncludesH}.raw
                     COMMAND egrep -v "'(^# )|(^#pragma)|(^#define __sparcv8)'" ${SysIncludesH}.raw.stripped                                         >>${SysIncludesH}
                     COMMAND ${ECHO} "\\#endif // CARBON_USE_SYS_INCLUDES"                                                                           >>${SysIncludesH}
                     COMMAND egrep "'^#define (get|[clmfa])_[a-z0-9A-Z_]+'" ${SysIncludesH}.raw.stripped | cut -d ' ' -f2 | sed -e "s/^/#undef /"    >>${SysIncludesH}
                     COMMAND ${ECHO} "\\#endif // __CARBON_SYS_INCLUDE_H__"                                                                          >>${SysIncludesH}
                     DEPENDS ${SysIncludesCXX}
                     )
endif()

# PerfectHash.h
if(${WIN32})
  set(DOLLAR $$)
else()
  set(DOLLAR '$$')
endif()
set(PerfectHashH ${CMAKE_CURRENT_BINARY_DIR}/PerfectHash.h)
set(PerfectHashGPerf ${CMAKE_CURRENT_SOURCE_DIR}/PerfectHash.gperf)
add_custom_command(OUTPUT ${PerfectHashH}
                   COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}
                   COMMAND ${GPERF} -k 1,3,5,${DOLLAR} -LC++ -Z PerfectHash -N is_reserved_word -C ${PerfectHashGPerf} > ${PerfectHashH}
                   DEPENDS ${PerfectHashGPerf}
                   )

# Ensure these all get created before anything else builds
add_custom_target(CodegenGeneratedFiles
                  DEPENDS
                  ${SysIncludesH}
                  ${PerfectHashH}
                  )

# Bison/Flex
BISON_TARGET(InducedFaultParser
             ${CMAKE_CURRENT_SOURCE_DIR}/InducedFaultParser.y
             ${CMAKE_CURRENT_BINARY_DIR}/InducedFaultParser.cxx
             COMPILE_FLAGS "-p InducedFault_")

FLEX_TARGET(InducedFaultLexer
            ${CMAKE_CURRENT_SOURCE_DIR}/InducedFaultLexer.l
            ${CMAKE_CURRENT_BINARY_DIR}/InducedFaultLexer.cxx
            COMPILE_FLAGS "-PInducedFault_"
            )

ADD_FLEX_BISON_DEPENDENCY(InducedFaultLexer InducedFaultParser)

FLEX_TARGET(CodegenErrorLexer
            ${CMAKE_CURRENT_SOURCE_DIR}/CodegenErrorLexer.l
            ${CMAKE_CURRENT_BINARY_DIR}/CodegenErrorLexer.cxx
            COMPILE_FLAGS "-PCodegenError_"
            )
