// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Generate code for each of the cbuild schedules, such as the input
  schedule, asynch schedule or the various combinational or synchronous
  schedules.
*/
#include <algorithm>

#include "GenSched.h"
#include "GenProfile.h"
#include "emitUtil.h"

#include "schedule/ScheduleMask.h"
#include "schedule/Schedule.h"
#include "schedule/Combinational.h"

#include "flow/FLNodeElab.h"

#include "iodb/CbuildShellDB.h"

#include "nucleus/NUCycle.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUTriRegInit.h"

#include "compiler_driver/CarbonDBWrite.h"
#include "compiler_driver/CarbonContext.h"

#include "symtab/STSymbolTable.h"
#include "symtab/STBranchNode.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"

#include "codegen/codegen.h"
#include "codegen/CGProfile.h"
#include "codegen/ScheduleBucket.h"

using namespace emitUtil;

static const int LARGEST_FUNCTION = 1000;

//! Compute a cost function (instruction count?)
int
GenSched::getCost () const
{
  return mCost.mInstructions;
}

/*!
 * Check if the given schedule is empty (and thus some
 * short-circuited computations are possible.
 */
bool
GenSched::emptySchedule (void) const
{
  return (not isCFunction () && not isClassMember ()
	  && mEmpty);
}

//! Constructor for a Combinational Schedule
GenSched::GenSched (const char*fname, SCHCombinational *s, CGOFiles *fileIOs, int flags,
                    const char *section, bool suppressObfuscate)
  : mSection (section ? section : ""),
    mIterator (s->getSchedule ()),
    mGenerator (mIterator),
    mCGOFiles (fileIOs),
    mRootHierarchy (0),
    mEquivalent (0),
    mCycle (0),
    mFlags (ProtoFlags (flags)),
    mSize (s->size ()),
    mBucketId (gCodeGen->mGenProfile->getBucketId(fname)),
    mEmpty (mIterator.atEnd ())
{
  setupCName(fname, suppressObfuscate);
  calcCost();
  findCommonHierarchy ();
}

//! Constructor for a Sequential Schedule
GenSched::GenSched (const char*fname, SCHSequential *s, CGOFiles *fileIOs, ClockEdge e, int flags, const char *section)
  : mSection (section ? section : ""),
    mIterator (s->getSchedule (e)),
    mGenerator (mIterator),	// Copy needed only if we actually generate this schedule
    mCGOFiles (fileIOs),
    mRootHierarchy (0),
    mEquivalent (0),
    mCycle (0),
    mFlags (flags),
    mSize (s->size (e)),
    mBucketId (gCodeGen->mGenProfile->getBucketId(fname)),
    mEmpty (mIterator.atEnd ())
{
  setupCName(fname, false);
  calcCost();
  findCommonHierarchy ();
}

//! Constructor for a Cycle Schedule
GenSched::GenSched (const char *fname, NUCycle* cycle, CGOFiles *fileIOs, const char *section)
  : mSection (section ? section : ""),
    mIterator (),
    mGenerator (),	// Copy needed only to generate this schedule
    mCGOFiles (fileIOs),
    mRootHierarchy (0),
    mEquivalent (0),
    mCycle (cycle),
    mFlags (eCycle | eClassMember | eSeparate),
    mSize (0),
    mBucketId (gCodeGen->mGenProfile->getBucketId(fname)),
    mEmpty (false)
{
  setupCName(fname, true);
  mCost.clear ();
  mCycle->calcCost (&mCost, gCodeGen->mCosts);
  mSize = mCost.mInstructions;  // Different size metric for cycles....
}

// Get a C++-friendly function name
void GenSched::setupCName(const char* fname, bool suppressObfuscate) {
  AtomicCache* atomicCache = gCodeGen->getAtomicCache();
  mSchedName = atomicCache->intern(fname);
  if (suppressObfuscate) {
    mCName = mSchedName;
  }
  else {
    CodeNames* codeNames = gCodeGen->getCodeNames();
    UtString buf;
    // Avoid the use of "s" as a prefix because the random-tiny-string
    // generator will eventually throw up "un" and "sun" is a predefined
    // CPP macro on suns.
    buf << "z" << codeNames->translate(mSchedName)->str();
    mCName = atomicCache->intern(buf);
  }
}

//! Calculate the cost of this schedule by using the nucleus cost structure
void
GenSched::calcCost ()
{
  NUCost node_cost;
  mCost.clear();

  // Compute the cost of running the schedule
  SCHIterator i = mIterator;
  for(FLNodeElab* e; i (&e);)
    {
      NUUseDefNode *node = e->getUseDefNode ();
      if (not node)
	{
	  continue;
	}
      NUCost* cost = gCodeGen->mCosts->findCost (node, false);
      if (cost)
	mCost.addCost(*cost, true);
      else
	{
	  node_cost.clear();
	  node->calcCost (&node_cost, gCodeGen->mCosts);
	  mCost.addCost(node_cost, true);
	}
    }
}

static int HowManyFunctions (UInt32 schedSize)
{
  return (schedSize + LARGEST_FUNCTION - 1) / LARGEST_FUNCTION;
}


void GenSched::getInnerFunctionPrototype(UtString & proto, int nfuncs)
{
  proto << "void " << getCFuncName()->str() << "_innerSched" << nfuncs << "(";

  if (getRoot ())
    proto << emitUtil::mclass (getClass (getRoot ())->getName ());
  else
    proto << "carbon_simulation";

  proto << "& " << noprefix (gCodeGen->mTopName) << ")";
}


void GenSched::maybeSchedFileSpill(int fn_size)
{
  static UInt32 schedfile = 0;  // remember last sch-<N> file in use...
  bool open_new_sch_file = ((not mCGOFiles->mScheduleSpill) or
                            ((mCGOFiles->mScheduleSpillSize+fn_size) > mCGOFiles->mScheduleSpillLimit));
  if (open_new_sch_file) {
    delete mCGOFiles->mScheduleSpill; // close previous file.

    UtString fname ("sch-");
    fname << (++schedfile);
    fname << ".cxx";
	
    mCGOFiles->mScheduleSpill = mCGOFiles->CGFileOpen (fname.c_str (), "", gCodeGen->isObfuscating());
    mCGOFiles->mScheduleSpillSize = 0;
    
    INFO_ASSERT(mCGOFiles->mScheduleSpill,"Error opening Schedule Spill file");

    mCGOFiles->mCurrent = mCGOFiles->mScheduleSpill;
    gCodeGen->codeScheduleBoilerPlate ();
  } else if (mCGOFiles->mScheduleSpill) {
    // reuse the current schedule spill file until we overflow it.
    mCGOFiles->mCurrent = mCGOFiles->mScheduleSpill;
  }
  
}


//! Declare inner functions as necessary...
void GenSched::declareInnerFunctions (void)
{
  UtOStream &out = mCGOFiles->CGOUT ();

  for(int nfuncs = HowManyFunctions (mSize); nfuncs > 0; --nfuncs)
    {
      UtString proto;
      getInnerFunctionPrototype(proto,nfuncs);

      out << proto;
      // If a section was given, emit declaration with section label to force into that section.
      if (mSection.length() > 0) {
        gCodeGen->emitSection(out, mSection);
      }
      out << ";\n";
    }
}


//! Generate inner functions as necessary...
void
GenSched::genInnerFunctions (void)
{
  int left_to_emit = mSize;
  for(int nfuncs = HowManyFunctions (mSize); nfuncs > 0; --nfuncs)
    {
      int inner_function_size = (left_to_emit > LARGEST_FUNCTION) ? LARGEST_FUNCTION : left_to_emit;
      left_to_emit -= inner_function_size;

      maybeSchedFileSpill(inner_function_size);

      UtString proto;
      getInnerFunctionPrototype(proto,nfuncs);

      UtOStream &out = mCGOFiles->CGOUT ();

      // only emit the namespace if we are using the schedule spill file.
      bool emit_namespace = (mCGOFiles->mCurrent == mCGOFiles->mScheduleSpill);

      if (emit_namespace) {
        // Start a namespace
        out << "namespace " << gCodeGen->getNamespace () << "{" << ENDL;
      }

      out << proto << "{\n";

      NUCost totCosts;
      genSomeCalls (LARGEST_FUNCTION, &totCosts);
      out << "}\t// " << totCosts.mInstructions << "\n";

      if (emit_namespace) {
        out << "}\n";		// Close the namespace
      }

      out << "\n";
    }
}

//! Call the previously generated inner functions
void
GenSched::callInnerFunctions (void)
{
  UtOStream &out = mCGOFiles->CGOUT ();

  for (int nfuncs = HowManyFunctions (mSize); nfuncs > 0; --nfuncs)
    out << "  " << getCFuncName()->str() << "_innerSched" << nfuncs << "("
        << noprefix (gCodeGen->mTopName) << ");\n";
}

//! Generate up to \a limit schedule calls.
void
GenSched::genSomeCalls (int limit, NUCost *costs)
{
  if (costs)
    costs->clear ();

  UtOStream &out = mCGOFiles->CGOUT ();

  // We use the mGenerator, so that we only run thru the schedule ONCE no matter
  // how many times we call this function....
  //
  for (FLNodeElab* eval; (limit != 0) && mGenerator(&eval);)
  {
    if (costs)
    {
      // We want to print the partial costs, so accumulate 'em
      NUUseDefNode *node = eval->getUseDefNode ();
      if (node)
      {
        NUCost *aCost = gCodeGen->mCosts->findCost (node, false);
        if (aCost)
          costs->addCost (*aCost, true);
        else
        {
          NUCost node_cost;
          node_cost.clear ();
          node->calcCost (&node_cost, gCodeGen->mCosts);
          costs->addCost (node_cost, true);
        }
      }
    }

    NUCycle* cycle = eval->getCycle();
    if (cycle != NULL) {
      // Remember the cycle's bucket, because we'll need to change its
      // parent to our bucket once we have a bucket, in genCall().
      ScheduleBucket* bucket = cycle->emitCall (out);
      mChildSchedBuckets.push_back(bucket);
    }
    else
    {
      NUUseDefNode *codeBlock = eval->getUseDefNode();

      out << "  ";
      if (codeBlock->getType () == eNUModuleInstance)
      {
        NU_ASSERT("Module Instance Expected" == 0, codeBlock);
      }
      else
	{
	  UtString fluff;
	  out << gCodeGen->CxxHierPath (&fluff, eval->getHier (), ".",
					 mRootHierarchy);
	  out << ".";
	  codeBlock->emitCode (eCGNameOnly);

	  NUCost codeCost;
	  codeBlock->calcCost(&codeCost, gCodeGen->mCosts);
	  out << "; // " << codeCost.mInstructions << "\n";
	}
    }

    --limit;
  } // for
}

const NUModule*
GenSched::getClass (const STBranchNode* sym)
{

  const NUBase* base = CbuildSymTabBOM::getNucleusObj (sym);
  const NUModuleElab* moduleElab = dynamic_cast<const NUModuleElab*> (base);

  // If this is not a module, then it's a named declaration scope.
  const NUModule* mod = NULL;
  if (moduleElab == NULL) {
    const NUNamedDeclarationScopeElab* declScopeElab = 
      dynamic_cast<const NUNamedDeclarationScopeElab*> (base);
    NUNamedDeclarationScope* declScope = declScopeElab->getScope();
    mod = declScope->getParentModule();
  } else {
    mod = moduleElab->getModule ();
  }

  return mod;
}


/*!
 * Emit function-calls in the specified order.
 */
void
GenSched::genMethod (void)
{
  static const char* scDumpStats = CRYPT("-dumpSchedule");

  if (emptySchedule ())
    // Don't bother materializing functions we don't need
    return;

  if (isGenerated ())
    return;			// Already compiled?

  setGenerated ();

  if (isEquivalent ())
    {
      getEquivalent ()->genMethod ();

      // Should leave a note in the schedule file about this...
      // (should also add a -dumpCGStats note.)
      UtString stats ("CGStats: ");
      stats << getSchedName()->str() << " replaced by "
            << getEquivalent ()->getSchedName()->str() << "\n";

      mCGOFiles->CGOUT () << "// " << stats;

      if (gCodeGen->getCarbonContext ()->getArgs ()->getBoolValue (scDumpStats))
	UtIO::cout () << stats;

      return;
    }

  // If the schedule we are compiling is large, we will emit it into a
  // separate file, and only place a function prototype in the schedule.cxx
  // file

  int SchedFunctionLimit = (4*gCodeGen->mFunLimit);
  bool nestFunctions = (mCost.mInstructions > SchedFunctionLimit) && not isCycle ();

  UtOStream * saved_mCurrent = mCGOFiles->mCurrent;

  // If we have really huge schedules, split them into multiple smaller
  // functions.
  //
  if (nestFunctions) {
    genInnerFunctions ();
  }

  // The number of calls in our generated function. If we have created
  // nested schedules, the number of functions is the number of nested
  // schedules. Otherwise, it is the number of scheduled blocks.
  int schedule_fn_size = nestFunctions ? HowManyFunctions(mSize) : mSize;

  // Conditionally spill out of our current file if this schedule is really big, or
  // if we've accumumulated a lot of small schedules into schedule.cxx.
  //
  if (mCGOFiles->checkForSpill (schedule_fn_size)) {
    maybeSchedFileSpill(schedule_fn_size);
  }

  // Are we emitting this schedule outside the default schedule.cxx?
  bool spilled_schedule = (saved_mCurrent != mCGOFiles->mCurrent);

  if (spilled_schedule) {
    setSeparate ();		// Mark as needing external interfaces

    // Start a namespace
    mCGOFiles->CGOUT () << "namespace " << gCodeGen->getNamespace () << "{" << ENDL;

  }

  if (nestFunctions) {
    // Declare any nested functions.
    declareInnerFunctions();
  }

  UtOStream &out = mCGOFiles->CGOUT ();

  if (mCycle)
    mCycle->emitSubFunctions (out);

  // If a section was given, emit declaration to force into that section.
  // But, only if this is not a class member.  If it is a class member,
  // it has already been declared in the header file.
  if ((mSection.length() > 0) and not isClassMember()) {
    out << genPrototype();
    gCodeGen->emitSection(out, mSection);
    out << ";\n";
  }

  (out << genPrototype ()) << "{\n";

  UtString pathComment ("// common hierarchy prefix is : ");
  out << gCodeGen->CxxHierPath (&pathComment, mRootHierarchy, ".") << "\n";

  // If needed then convert the input arg into a top-level reference (it's
  // either C-callable or a method taking an explicit handle)
  if (mEmpty) {
    // not required
  } else if (isCycle ()) {
    INFO_ASSERT (!hasHandleArg () && isClassMember (), "bogus cycle schedule");
    out << emitUtil::mclass (gCodeGen->mTopModule) << "& "
        << noprefix (gCodeGen->mTopName) << "= *this;\n";
  } else if (!hasHandleArg ()) {
    // no argument to convert
  } else if (isCFunction ()) {
    out << "carbon_simulation &" << noprefix (gCodeGen->mTopName)
        << " = *static_cast <carbon_simulation *> ("
        << formal (gCodeGen->mTopName) << "->mHdl);\n";
  } else if (isClassMember () && getRoot ()) {
    INFO_ASSERT (false, "non-degenerate hierarchy in class member");
  } else if (isClassMember () && !getRoot ()) {
    out << "carbon_simulation &" << noprefix (gCodeGen->mTopName)
        << " = *static_cast <carbon_simulation *> (this);\n";
  }
  if (nestFunctions) {
    callInnerFunctions ();
    mCGOFiles->mScheduleSpillSize +=
      HowManyFunctions (mSize) + mCost.mInstructions;
  } else if (isCycle ()) {
    mCycle->emitDefinition (out);
    mCGOFiles->mScheduleSpillSize += mCost.mInstructions;
  } else {
    genSomeCalls (mSize+1);	// All schedule calls inlined here
    mCGOFiles->mScheduleSpillSize += mCost.mInstructions;
  }

  // Close the new profiling bucket for which we just generated code.
  if (!mEmpty) {
    gCodeGen->getCGProfile()->emitStopBucket();
  }

  // Close the function body
  out << "}\n";

  if (spilled_schedule) {
    out << "}\n";		// Close the namespace
  }

  mCGOFiles->mCurrent = saved_mCurrent;

  if (spilled_schedule) {
    if (not isClassMember ()) {
      // No prototype needed - it's already in the class declaration?
      mCGOFiles->CGOUT () << genPrototype ();
      mCGOFiles->CGOUT () << ";\n";
    }
  }

} // genMethod

const UtString
GenSched::genCall () const
{
  if (emptySchedule ())
    return UtString ("  (void)0;\n");
	
  UtString temp;
  UtOStringStream tempStream(&temp);

  // Do schedule profiling.
  //
  // It might be considered more normal to put buckets in the schedule
  // functions themselves, but since they're generated before the
  // top-level schedule functions, that wouldn't be sufficient to
  // capture the hierarchy.  (We'd always call stopSampleSchedBucket()
  // for the grouping function before calling startSampleSchedBucket()
  // for the functions it contains.)  So instead we create buckets
  // around the calls to the low-level schedule functions (here)
  // rather than in their definitions.  This also allows for having
  // two different buckets to two calls to the same schedule function
  // can be profiled separately.
  gCodeGen->startSampleSchedBucket(gCodeGen->mGenProfile->getBucketName(mBucketId),
                                   tempStream);
  // If we're generating a call to a cycle, we need to be that
  // cycle's parent.  We stored the cycle's bucket when we called emitCall().
  for (UtArray<ScheduleBucket*>::const_iterator p = mChildSchedBuckets.begin();
       p != mChildSchedBuckets.end();
       ++p)
    (*p)->setParent(gCodeGen->getSchedBuckets()->getCurrent());

  temp += "  ";

  // If we have an equivalent schedule, call its schedule function
  // but with OUR simulation object sub-tree
  if (isEquivalent ())
    temp += getEquivalent ()->getCFuncName()->str();
  else
    temp += getCFuncName()->str();

  temp += "(";

  if (hasHandleArg () && not isClassMember ())
    // A C function or an internal static function has an explicit arg.
    {
      if (isCFunction ())
	// Pass pointers, not references
	temp += "&";

      gCodeGen->CxxHierPath (&temp, mRootHierarchy, ".");
    }

  temp += ");\n";

  // End the bucket
  gCodeGen->stopSampleSchedBucket(tempStream);

  return temp;
}

const UtString
GenSched::genPrototype () const
{
  if (isEquivalent ())
    return getEquivalent ()->genPrototype ();

  if (emptySchedule ())
    return UtString ("");

  UtString proto;

  proto << "//"
	<< "  size = "
	<< mSize
	<< "  cost = "
	<< mCost.mInstructions
	<< "  assigns = "
	<< mCost.totalAssigns()
	<< "  complexity = "
	<< mCost.complexity()
	<< "\n";

  if (isCFunction ())
    proto += "extern \"C\" void ";
  else if (isClassMember ())
    {
      proto += " void ";
      proto += pclass (gCodeGen->getTopModule ());
    }
  else if (isSeparate ())
    proto += "void ";
  else
    proto += "static void ";

  proto += getCFuncName()->str();

  proto += "(";

  if (hasHandleArg () && not isClassMember ())
    {
      if (isCFunction () || isTopLevel ())
	proto += "carbon_model_descr *";
      else
	if (mRootHierarchy)
	  // Print the module name that this instance is
	  // associated with
	  {
	    proto <<  emitUtil::mclass (getClass (mRootHierarchy)->getName ()) << "&";
	  }
	else
	  proto += "carbon_simulation& ";

      if (not mEmpty) {
	if (isCFunction ())
	  proto += formal (gCodeGen->mTopName);
	else
	  proto += noprefix (gCodeGen->mTopName);
      }
    }

  proto += ")";

  return proto;
}

STBranchNode*
GenSched::getKey () const
{
  return getRoot () ? getRoot ()->getParent () : 0;
}

/*!
 * When a sub-module is multiply-instantiated with distinct clocking, we can
 * end up with two or more schedules that are identical except for the naming
 * hierarchy.  In this case, all the flownodes in the schedule will have
 * a common prefix, such as
 *
 *		top.a.b.foo1
 *		top.a.b.foo2
 *		top.a.b.foo3
 *
 * These flownodes all share the prefix 'top.a.b'.
 */
void
GenSched::findCommonHierarchy (void)
{
  static const char* scSchedPrefix = CRYPT("-schedPrefix");
  if(!gCodeGen->getCarbonContext ()->getArgs ()->getBoolValue (scSchedPrefix))
    return;

  if (mEmpty || isClassMember () || isTopLevel ())
    // No schedule, or it always is called from the top (vis. debug schedule)
    // [TODO] if we searched the guard expressions, we could probably
    // include them in the largest hierarchy...
    return;

  STBranchNode *best = NULL;
  SCHIterator i = getIterator ();

  // Loop over the schedule finding the largest hierarchy that's common
  for (FLNodeElab* eval; i (&eval); ++eval)
    {
      // The candidate hierarchy can only be as long as the first
      // flow-node's hierarchy or as short as the design top.
      STBranchNode *candidate = eval->getHier ();

      if (!candidate)
	return;			// Nothing to chase

      if (!best)
	best = candidate;
      else if (best == candidate)
	continue;
      else
	// could have best == "a.b" and candidate "a.b.c.d"
	// or vice versa.  In general best will be shorter already
	//
	do {
	  STBranchNode *trial;

	  // Look for a prefix of candidate that matches our best
	  // prefix
	  for (trial = candidate;
	       trial;		// Stop at root!
	       trial = trial->getParent ())
	    if (trial == best)
	      // If we found a match, the best is still the best,
	      // so look at the next flow node
	      goto eval_next;

	  // Didn't match with existing best, shorten the best and
	  // try again.
	  best = best->getParent ();
	  FLN_ELAB_ASSERT (best, eval);	// There must be SOME common hierarchy

	} while (true);

    eval_next:
      ;
    }

  if (!best) 
    {
      UtString msg (" No Common Hierarchy Found for ");
      msg << getSchedName()->str();
      INFO_ASSERT (best, msg.c_str() );
    }
  else
  {
    // If the common hierarchy is a named block, find the STBranchNode for it's
    // parent module.
    while (CodeGen::isNamedBlock(best)) {
      best = best->getParent();
    }
  }
  // Remember what we found as the common hierarchy
  mRootHierarchy = best;
  return;
}

struct FLEquiv:
  public std::binary_function<const FLNodeElab*, const FLNodeElab*, bool>
{
  bool operator() (const FLNodeElab *a, const FLNodeElab *b) const
  {
    if (a->getUseDefNode () != b->getUseDefNode ())
      return false;

    STBranchNode *ha = a->getHier ();
    STBranchNode *hb = b->getHier ();

    while (ha)
      {
	if (ha->strObject () != hb->strObject ())
	  // If these are different, but they are the point at which the
	  // object we're referencing in each hierarchy is the same type
	  // and we're at the root
	  return (ha->getParent () == mKey
		  && GenSched::getClass (ha) == GenSched::getClass (hb));
	else
	  // Equivalent components, check their parents
	  {
	    hb = hb->getParent ();
	    ha = ha->getParent ();
	  }
      }
    return false;
  }
  // Required constructor to access Schedule info
  FLEquiv (const STBranchNode* key) : mKey (key){};

  const STBranchNode* mKey;
};

bool
GenSched::isEquivalent (const GenSched& other) const
{
  if (getSize () != other.getSize ()
      //      || getKey () != other.getKey ()
      || getClass (mRootHierarchy) != other.getClass (other.getRoot ())
      || ((getFlags () & (eCFunction|eHandleArg|eClassMember))
	  != (other.getFlags () & (eCFunction|eHandleArg|eClassMember))))
  return false;

  // They look VERY similar.  Now make sure that each schedule's respective
  // FLNode's UseDefNode is identical and that the hierarchical reference
  // is identical up to the common root,    

  SCHIterator me = getIterator ();
  SCHIterator you = other.getIterator ();
  return std::equal (me.begin (), me.end (),
		     you.begin (), FLEquiv (getKey ()));
}

/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
