// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "FuncLayout.h"
#include "emitUtil.h"
#include "schedule/Schedule.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUCycle.h"
#include "util/UtString.h"
#include "util/UtStream.h"
#include "iodb/ScheduleFactory.h"
#include "iodb/IODBNucleus.h"
#include <math.h>


FuncLayout::~FuncLayout()
{
}


void FuncLayout::resetFileInfo()
{
  mCurFuncs.clear();
  mCurClasses.clear();
  mCurAccumCost.clear();
}


void FuncLayout::addFuncToFile(NUUseDefNode *node)
{
  // Bug 2234, module instances show up in schedules, but
  // are not really scheduled.  Ignore them.
  if (node->getType() == eNUModuleInstance) {
    return;
  }

  NUModule *module = node->findParentModule();
  mCurFuncs.push_back(node);

  if (mCurClasses.find (module) == mCurClasses.end ())
  {
    // mark any VHDL port connections
    mCGOFiles->restoreExtraHeaders (module);
    emitUtil::emitAliasedPortConnections (module, eCGMarkNets);
    mCGOFiles->saveExtraHeaders (module);
  }

  mCurClasses.insert(module);

  NUCost cost;
  cost.clear();
  node->calcCost(&cost, mCosts);
  mCurAccumCost.addCost(cost, false);
}


bool FuncLayout::testAddFuncToFile(NUUseDefNode *node)
{
  // Always allow 1 node to be added to the file.
  if (mCurFuncs.empty()) {
    return true;
  }

  // Any limits may be 0, which means they are ignored.
  if (mLimitFuncs != 0) {
    if (mCurFuncs.size() + 1 > mLimitFuncs) {
      return false;
    }
  }

  if (mLimitClasses != 0) {
    NUModule *module = node->findParentModule();
    UInt32 num_classes = mCurClasses.size();
    if (mCurClasses.find(module) == mCurClasses.end()) {
      ++num_classes;
    }
    if (num_classes > mLimitClasses) {
      return false;
    }
  }

  if (mLimitInstrCost != 0) {
    NUCost cost;
    cost.clear();
    node->calcCost(&cost, mCosts);
    if ((UInt32) (mCurAccumCost.mInstructions + cost.mInstructions) > mLimitInstrCost) {
      return false;
    }
  }

  return true;
}


//! FlowOrder class
/*!
 * This class allows for ordering of flow nodes by clock speed, and by
 * order in which the node appears in the schedule.
 */
class FlowOrder
{
public:
  /*!
    \param node The flow node
    \param order The order in which this node appears in the schedule (0 being earliest)
    \param factory The schedule factory, used to obtain clock speeds
    \param max_speed The maximum clock speed in the design.
   */
  FlowOrder(FLNodeElab *node, UInt32 order, SCHScheduleFactory *factory, UInt32 max_speed) :
    mNode(node),
    mOrder(order)
  {
    mRank = getSpeedRank(getSpeed(node, factory), max_speed);
  }

  FlowOrder() :
    mNode(0),
    mOrder(0),
    mRank(0)
  {}

  ~FlowOrder() {}

  FlowOrder (const FlowOrder &other)
  {
    mNode = other.mNode;
    mOrder = other.mOrder;
    mRank = other.mRank;
  }

  FlowOrder &operator=(const FlowOrder &other)
  {
    if (this != &other) {
      mNode = other.mNode;
      mOrder = other.mOrder;
      mRank = other.mRank;
    }
    return *this;
  }

  bool operator==(const FlowOrder &other) const
  {
    return ((mNode == other.mNode) and (mOrder == other.mOrder) and (mRank == other.mRank));
  }

  /*!
   * Sort criteria:
   * 1 - Fastest clocks come first, but don't do strictly by clock
   *     frequency, instead consider any 2 clocks within 2 orders
   *     of magnitude of another equivalent.
   * 2 - If clocks are equivalent, then order in which they are
   *     encountered in the schedule breaks the tie.
   */
  bool operator<(const FlowOrder &other) const
  {
    if (mRank < other.mRank) {
      return true;
    } else if (mRank > other.mRank) {
      return false;
    } else {
      return mOrder < other.mOrder;
    }
  }

  //! Get the fastest clock speed for the given node.
  static UInt32 getSpeed(FLNodeElab *node, SCHScheduleFactory *factory)
  {
    const SCHScheduleMask *mask =
      node->isSampled() ?
      node->getSignature()->getSampleMask() :
      node->getSignature()->getTransitionMask();
    return factory->fastestClock(mask);
  }

  //! Return ranking from max speed, 0 being max speed, in buckets of 100 (2 orders of magnitude).
  static UInt32 getSpeedRank(UInt32 speed, UInt32 max_speed)
  {
    UInt32 max_rank = (max_speed == 0) ? 0 : (UInt32)floor(log10((double)max_speed)/2.0);
    UInt32 speed_rank = (speed == 0) ? 0 : (UInt32)floor(log10((double)speed)/2.0);
    UInt32 rank = max_rank - speed_rank;
    return rank;
  }

  //! Flow node
  FLNodeElab *mNode;

  //! Order in which this node appears in the schedule (0 being earliest)
  UInt32 mOrder;

  //! Clock rank of this node (see getSpeedRank for description of rank)
  UInt32 mRank;
};


void FuncLayout::layoutFunctions(SCHSchedule *schedule)
{
  typedef UtSet<FlowOrder> FunctionOrdering;
  FunctionOrdering order;

  SCHScheduleFactory *sched_factory = schedule->getFactory();
  UInt32 max_speed = mIODB->getHighestClockSpeed();

  // Traverse all schedules, put the flow nodes into a sorted set.
  UInt32 count = 0;
  for (SCHSchedulesLoop loop = schedule->loopAllSimulationSchedules();
       not loop.atEnd();
       ++loop) {
    for (SCHSchedulesLoop::Iterator iter = loop.getSchedule();
         not iter.atEnd();
         ++iter, ++count) {
      FLNodeElab *node = *iter;
      order.insert(FlowOrder(node, count, sched_factory, max_speed));
    }
  }
  for (SCHIterator iter = schedule->getInitialSchedule()->getSchedule();
       not iter.atEnd();
       ++iter, ++count) {
    FLNodeElab *node = *iter;
    order.insert(FlowOrder(node, count, sched_factory, max_speed));
  }
  for (SCHIterator iter = schedule->getInitSettleSchedule()->getSchedule();
       not iter.atEnd();
       ++iter, ++count) {
    FLNodeElab *node = *iter;
    order.insert(FlowOrder(node, count, sched_factory, max_speed));
  }
  for (SCHIterator iter = schedule->getDebugSchedule()->getSchedule();
       not iter.atEnd();
       ++iter, ++count) {
    FLNodeElab *node = *iter;
    order.insert(FlowOrder(node, count, sched_factory, max_speed));
  }


  // Emit according to the order of the set.
  for (FunctionOrdering::iterator iter = order.begin();
       iter != order.end();
       ++iter) {
    emitHandleCycles((*iter).mNode->getUseDefNode());
  }

  emitFile();
}


void FuncLayout::emitHandleCycles(NUUseDefNode *node)
{
  if (node->getType() == eNUCycle) {
    NUCycle *cycle = dynamic_cast<NUCycle*>(node);
    NU_ASSERT(cycle, node);
    const FLNodeElabVector* schedule = cycle->getOrderedFlowVector();
    for (FLNodeElabVector::const_iterator p = schedule->begin(); p != schedule->end(); ++p)
    {
      FLNodeElab *flow = *p;
      if (not flow->isBoundNode()) {
        emitHandleCycles(flow->getUseDefNode());
      }
    }
    delete schedule;
  } else {
    emitFunction(node);
  }
}


void FuncLayout::emitFunction(NUUseDefNode *node)
{
  NU_ASSERT(node->getType() != eNUCycle, node);

  // Only emit functions once.
  if (haveSeen(node)) {
    return;
  }

  if (not testAddFuncToFile(node)) {
    emitFile();
  }

  addFuncToFile(node);
  rememberSeen(node);
}


void FuncLayout::emitFile()
{
  // If nothing to do, return.
  if (mCurFuncs.empty()) {
    return;
  }

  UtOStream *file_stream = mCGOFiles->addFunctionFile(mFileCounter, 
						     mCodeGen->precompiledHeaders());
  mCGOFiles->switchStream(eCGDefine);

  if (not mCodeGen->precompiledHeaders ())
    // Emit the class includes.  They are sorted for consistency, no other reason.
    for (OrderedModuleSet::iterator iter = mCurClasses.begin();
         iter != mCurClasses.end();
         ++iter) {
      mCGOFiles->generateHeaderInclude(*file_stream, *iter);
    }

  // Emit the functions in order.
  CGContext_t context = eCGDefine | eCGMarkNets;
  emitCodeListFuncLayout(&mCurFuncs, context, mCodeGen, mCGOFiles);

  // Output some statistical information
  *file_stream << "\n// Function Layout"
               << "\n// Functions: " << mCurFuncs.size ()
               << "\n// Classes:   " << mCurClasses.size ()
               << "\n// Instr Cost:" << mCurAccumCost.mInstructions
               << "\n";

  // Close the file, reset the codegen output stream.
  mCGOFiles->closeClassFile(false);

  ++mFileCounter;
  resetFileInfo();
}


void FuncLayout::rememberSeen(NUUseDefNode *node)
{
  mSeenOrder[node] = ++mOrder;
}


UInt32 FuncLayout::haveSeen(const NUUseDefNode *node) const
{
  SeenOrder::const_iterator found = mSeenOrder.find (node);
  if (found != mSeenOrder.end())
    return found->second;

  return 0;                     // Not found
}

/*
 * Note: this code does some module-level context setting for each function
 * it encounters.  This is because FuncLayout uses emitCodeList
 * for mixed-module lists, so we must set up module context for each
 * function.
 */
void SimpleListFuncLayout::operator() (const NUUseDefNode *n) const
{
  NU_ASSERT(n->isContDriver() and isDefine(ctxt), n);

  if (not mCodeGen->inEmittedSet(n)) {
    const NUModule *module = n->findParentModule();
    const StringAtom *module_name = emitUtil::mclass(module->getName());

    // Setup for module
    mCodeGen->putClass(module_name);
    mCodeGen->restoreConstantPool(module);
    mCGOFiles->restoreExtraHeaders(module);
    mCodeGen->pushScope(module);

    // Generate includes for the modules we store pointers for task
    // hierarchical references.
    NUTaskEnableVector hierTaskEnables;
    mCodeGen->generateTaskHierRefs (n, hierTaskEnables);
    mCodeGen->generateTaskHeaders (hierTaskEnables);

    // Open the namespace on a per-function basis.  This is because
    // we need the include files out of the namespace qualifier.
    mCGOFiles->CGOUT() << "namespace " << mCodeGen->getNamespace() << " {" << ENDL;

    // Check to see if this module is instantiated more than once.  In
    // that case, inlining may not be a good idea.
    //
    CGContext_t this_ctxt = ctxt;
    if (mCodeGen->mDoInline) {
      NUCost *cost = mCodeGen->mCosts->findCost(module, false);
      if (cost && (cost->mUnelabInstances == 1))
        this_ctxt |= eCGInline;
    }
    n->emitCode (this_ctxt);
    mCodeGen->addToEmittedSet(n);

    // Close the namespace.
    mCGOFiles->CGOUT() << "}\n";

    // Undo setup for module
    mCodeGen->saveConstantPool(module);
    mCGOFiles->saveExtraHeaders(module);
    mCodeGen->popScope();
  }
}
