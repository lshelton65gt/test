// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef GENPROFILE_H_
#define GENPROFILE_H_

#include "util/UtMap.h"
#include "util/UtVector.h"
#include "util/UtString.h"


/*!\file
 * Definitions of a class to maintain mapping between profiling bucket
 * IDs and names.
 */

//! GenProfile class
/*!
 * Class to maintain mapping between profiling bucket IDs and names.
 */
class GenProfile
{
public:
  GenProfile() :
    mNumBuckets(0)
  {}

  ~GenProfile() {}

  //! Return a bucket id for the given name
  int getBucketId(const UtString &bucket_name);
  int getBucketId(const char *bucket_name);

  //! Return the bucket's name given the id
  const UtString& getBucketName(int bucket_id);

private:
  //! Hide copy and assign constructors.
  GenProfile(const GenProfile&);
  GenProfile& operator=(const GenProfile&);

  //! Total number of profiling buckets
  int mNumBuckets;

  //! Mapping of names to bucket ids
  typedef UtMap<UtString,int> StringIntMap;
  StringIntMap mBucketMap;

  //! Mapping of ids to names
  typedef UtVector<UtString> StringVector;
  StringVector mBucketVector;
};

#endif
