#############################################
##  Makefile.am  included by src/Makefile.top
#############################################
#

# This Makefile.am uses the value of NEXPR_ENABLE inferred in
# nexpr/Makefile.am. So, make sure that cg/Makefile.am is included
# after nexpr/Makefile.am in Makefile.top.

SUBDIRS	= . testsuite

LTLIBS += $(top_builddir)/codegen/libcodegen.a

inc=$(top_srcdir)/inc/codegen

# These headers need to be installed, as they are
# included by cbuild-generated C++ code.
#
codegen_HEADERS = \
	$(inc)/carbon_model.h \
	$(inc)/carbon.h
noinst_CODEHEADERS = \
	$(inc)/Memory.h \
	$(inc)/StateUpdate.h \
	$(inc)/StateUpdateBV.h \
	$(inc)/Forcible.h \
	$(inc)/ForciblePOD.h \
	$(inc)/Tristate.h \
	$(inc)/TristateBV.h

noinst_DATA = $(srcdir)/Makefile.mc \
	$(srcdir)/Makefile.libs \
	$(srcdir)/Make-gcc3.inc \
	$(srcdir)/Make-gcc4.inc \
	$(srcdir)/Make-gcc2.inc \
	$(srcdir)/Make-icc.inc \
	$(srcdir)/Make-win.inc \
	$(srcdir)/Make-spw.inc


BUILT_SOURCES = $(top_builddir)/codegen/PerfectHash.h \
                $(top_builddir)/codegen/SysIncludes.h \
		$(top_builddir)/codegen/InducedFaultParser.hxx \
		$(top_builddir)/codegen/InducedFaultParser.cxx \
		$(top_builddir)/codegen/InducedFaultLexer.cxx \
		$(top_builddir)/codegen/CodegenErrorLexer.cxx \

CLEANFILES = $(BUILT_SOURCES)

# Ensure these files are built before .d files so paths will be correct.
META_DEPEND += $(top_builddir)/codegen/PerfectHash.h $(top_builddir)/codegen/SysIncludes.h 

# Generate a perfect hash from the list of C++ names and cbuild
# special class names
#
$(top_builddir)/codegen/PerfectHash.h: $(top_srcdir)/codegen/PerfectHash.gperf
	@mkdir -p $(@D)
	$(GPERF) -k 1,3,5,'$$' -LC++ \
	  -Z PerfectHash -N is_reserved_word -C $< >$@

ifeq ($(CONFIGURED_COMPILER),gcc4)
USEPCH=true
# gcc4 has a busted posix_memalign declaration we have to work around
HACKFLAG=-gcc4
endif

ifeq ($(CONFIGURED_COMPILER),gcc3)
USEPCH=true
HACKFLAG=
endif

ifdef USEPCH
PRECOMPILE_INCLUDES = \
		-I$(CARBON_HOME)/src/inc \
		-I$(CARBON_HOME)/src/inc/util \
		-I$(CARBON_HOME)/src/inc/shell \
		-I$(CARBON_HOME)/include \

# GCC4 doesn't like the declaration of posix_memalign with a __malloc__ attribute
$(top_builddir)/codegen/SysIncludes.h: $(CARBON_HOME)/src/codegen/BuildSysIncludes.cxx
	@mkdir -p $(@D)
	@$(ECHO) "#ifndef __CARBON_SYS_INCLUDE_H__"                     > $@
	@$(ECHO) "#define __CARBON_SYS_INCLUDE_H__"                     >>$@
	@$(ECHO) "#ifdef CARBON_USE_SYS_INCLUDES"			>>$@
	@cat $<								>>$@
	@$(ECHO) "#else"						>>$@
	$(CXX) -E -dD $(PRECOMPILE_INCLUDES) -fno-exceptions $< > $@.raw
	$(CARBON_HOME)/scripts/hack_sys_include $(HACKFLAG)    $@.raw
	egrep -v '(^# )|(^#pragma)|(^#define __sparcv8)' $@.raw.stripped >>$@
	@$(ECHO) "#endif // CARBON_USE_SYS_INCLUDES"			  >>$@
	egrep '^#define (get|[clmfa])_[a-z0-9A-Z_]+' $@.raw.stripped |        \
           cut -d ' ' -f2 | sed -e 's/^/#undef /'                         >>$@
	@$(ECHO) "#endif // __CARBON_SYS_INCLUDE_H__"                     >>$@
	@$(ECHO) $@ built.
else

# gcc2 does not have the preprocessing support needed for preprocessing
# our system header files

$(top_builddir)/codegen/SysIncludes.h: $(CARBON_HOME)/src/codegen/BuildSysIncludes.cxx
	@mkdir -p $(@D)
	cp $< $@

endif

#$(top_builddir)/codegen/SchedIncludes.h: $(CARBON_HOME)/src/codegen/BuildSchedIncludes.cxx
#	$(CXX) -E -dD \
#		$(PRECOMPILE_INCLUDES) \
#		$< \
#		>$@ 

#GEN_INC_LIST = $(top_srcdir)/codegen/BaseIncludes.h \
#	       $(top_srcdir)/codegen/SchedIncludes.h
#
#$(top_builddir)/genincludes.com: $(top_srcdir)/codegen/BaseIncludes.h \
#				 $(top_srcdir)/codegen/BaseIncludes.h \
#$(CARBON_HOME)/src/codegen/
#	list_includes $(CXX) $(PRECOMPILE_INCLUDES) $< >$@ 
#	list_includes $(CXX) $(PRECOMPILE_INCLUDES) $< >$@ 
#

# The source files that make up the target 'xxx'

libcodegen_la_codegen_INCLUDES = $(top_builddir)/codegen/PerfectHash.h

libcodegen_la_SOURCES =					\
	$(top_srcdir)/codegen/build.cxx			\
	$(top_srcdir)/codegen/codegen.cxx		\
	$(top_srcdir)/codegen/CGOFiles.cxx		\
	$(top_srcdir)/codegen/CodeSchedule.cxx		\
	$(top_srcdir)/codegen/CodeNames.cxx		\
	$(top_srcdir)/codegen/CodeTestDriver.cxx	\
	$(top_srcdir)/codegen/ConstantPool.cxx		\
	$(top_srcdir)/codegen/emitAssign.cxx		\
	$(top_srcdir)/codegen/emitCModel.cxx		\
	$(top_srcdir)/codegen/emitCode.cxx		\
	$(top_srcdir)/codegen/emitConcat.cxx		\
	$(top_srcdir)/codegen/emitCycle.cxx		\
	$(top_srcdir)/codegen/emitModule.cxx		\
	$(top_srcdir)/codegen/emitNet.cxx		\
	$(top_srcdir)/codegen/emitPliWrapper.cxx	\
	$(top_srcdir)/codegen/emitStmt.cxx		\
	$(top_srcdir)/codegen/emitSysFunc.cxx		\
	$(top_srcdir)/codegen/emitTFCall.cxx		\
	$(top_srcdir)/codegen/emitUtil.cxx		\
	$(top_srcdir)/codegen/FuncLayout.cxx		\
	$(top_srcdir)/codegen/GenProfile.cxx		\
	$(top_srcdir)/codegen/GenSchedFactory.cxx	\
	$(top_srcdir)/codegen/GenSched.cxx		\
	$(top_srcdir)/codegen/NestedPartsel.cxx		\
	$(top_srcdir)/codegen/RankNets.cxx		\
	$(top_srcdir)/codegen/TestDriver.cxx		\
	$(top_srcdir)/codegen/TypeDictionary.cxx	\
	$(top_srcdir)/codegen/CodePortInterface.cxx	\
	$(top_srcdir)/codegen/CGPortIface.cxx		\
	$(top_srcdir)/codegen/CGNetElabCount.cxx	\
	$(top_srcdir)/codegen/VisitSchedule.cxx		\
	$(top_srcdir)/codegen/CGProfile.cxx		\
	$(top_srcdir)/codegen/ScheduleBucket.cxx	\
	$(top_srcdir)/codegen/Blob.cxx			\
	$(top_srcdir)/codegen/CodeCModel.cxx		\
	$(top_srcdir)/codegen/MemoryAnalyzer.cxx	\
	$(top_srcdir)/codegen/CGTopLevelSchedule.cxx	\
	$(top_srcdir)/codegen/CGTriggerFactory.cxx	\
	$(top_srcdir)/codegen/PrimaryPort.cxx		\
	$(top_srcdir)/codegen/InducedFaultTable.cxx	\
	$(top_srcdir)/codegen/InducedFaultLexer.l    	\
	$(top_srcdir)/codegen/InducedFaultParser.y    	\
	$(top_srcdir)/codegen/CodegenErrorLexer.l    	\

# The .hxx must be built before the .d, otherwise the .d won't have the path
# to the .hxx.
$(top_builddir)/codegen/InducedFaultTable.cxx.d \
$(top_builddir)/codegen/InducedFaultLexer.l.d : $(top_builddir)/codegen/InducedFaultParser.hxx
$(top_builddir)/codegen/InducedFaultLexer.l.d : $(top_builddir)/codegen/InducedFaultLexer.cxx
$(top_builddir)/codegen/CodegenErrorLexer.l.d : $(top_builddir)/codegen/CodegenErrorLexer.cxx
$(top_builddir)/codegen/CodegenErrorLexer.cxx: $(top_srcdir)/codegen/CodegenErrorLexer.l


.PRECIOUS: $(top_builddir)/codegen/CodegenErrorLexer.cxx

InducedFaultParser_YACC_FLAGS = -p InducedFault_
InducedFaultLexer_LEX_FLAGS = -PInducedFault_
CodegenErrorLexer_LEX_FLAGS = -PCodegenError_
