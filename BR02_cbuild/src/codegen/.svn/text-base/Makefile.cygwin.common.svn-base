# -*- Makefile -*-
# *****************************************************************************
# Copyright (c) 2003-2013 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
# *****************************************************************************

# Makefile.cygwin.common
#
#   This file is included by the various Makefile.cygwin used by the xactors
#   to run native compile link execute under windows.
#

ifeq ($(CARBON_HOME),)
  $(error You must set CARBON_HOME to the Carbon installation directory)
endif

#
# Get the definitions of these symbols:
#
# COMPILER_TARGET=winx
# winx_VERSION=3.4.3-a-mingw
# gcc_VERSION=3.4.3-a
# CONFIGURED_COMPILER=winx
#
ifeq ($(CARBON_HOST_ARCH),)
 CARBON_OBJ_DIR = Win.product
else
 ifeq ($(CARBON_BIN),)
   CARBON_OBJ_DIR = $(CARBON_HOST_ARCH).product
 else
   CARBON_OBJ_DIR = $(CARBON_HOST_ARCH).$(CARBON_BIN)
 endif
endif



# See if we are really running under CYGWIN

ARCH:=$(shell uname)
ifeq ($(ARCH),CYGWIN_NT-5.1)
  WINHOST = 1
endif
ifeq ($(ARCH),CYGWIN_NT-5.0)
  WINHOST = 1
endif

#
# define CARBON_WHOME and CARBON_UHOME for all cases
#
ifeq ($(WINHOST),1)
 # since CARBON_HOME might be set to be a native windows path
 # create the unix path and windows path to CARBON_HOME regardless
 # of how CARBON_HOME was set by the user
   CARBON_WHOME := $(shell /usr/bin/cygpath -m $(CARBON_HOME))
   CARBON_UHOME := $(shell /usr/bin/cygpath -u $(CARBON_HOME))
else
 # we might run this makefile under linux to TEST it out.
 # so for linux  CARBON_UHOME == WHOME == CARBON_HOME
  CARBON_WHOME = $CARBON_HOME
  CARBON_UHOME = $CARBON_HOME
endif

 include $(CARBON_HOME)/obj/$(CARBON_OBJ_DIR)/Makefile.compiler

# If the Make variable STATIC_DESIGNPLAYER has a non-empty value then
# build and use a static DesignPlayer.
# LINKLIB is the DesignPlayer library with which to link.  When using
# a shared library, it's the import library.

ifeq ($(STATIC_DESIGNPLAYER),)
  EXT := dll
  LINKLIB := libdesign.vc.lib
else
  EXT := lib
  LINKLIB := libdesign.lib
endif


ifeq ($(WINHOST),1)
  # Setup for windows build/link

  CDRIVE_UNIX = /cygdrive/c
  CDRIVE_WIN = C:

  # Set MSVC_VERSION to 6 or 7 to override auto-discovery
  ifeq ($(MSVC_VERSION),)
    MSVC_VERSION=6
    HAVE_DOTNET=$(shell test -d $(CDRIVE_WIN)/Program\ Files/Microsoft.NET; echo $?)
    ifeq ($(HAVE_DOTNET),0)
      MSVC_VERSION=7
    endif
  endif


 # TODO - do we need to support this going forward?

 ##############
 #  VISUAL C  #    pathnames
 ##############

  ifeq ($(MSVC_VERSION),6)
    # Where can we find the VC 6.0 stuff? The paths may be slightly different
    # on your system
    #
    VCROOT = Program\ Files/Microsoft\ Visual\ Studio
    VC = $(VCROOT)/VC98
    VCCOMMON = $(VCROOT)/Common
    VCDEV = $(VCROOT)/Common/MSDev98/bin
    LIBPATH_SDK =
  endif

  ifeq ($(MSVC_VERSION),7)
    # Where can we find the Visual Studio .NET stuff? The paths may be slightly
    # different on your system
    #
    VCROOT = Program\ Files/Microsoft\ Visual\ Studio\ .NET\ 2003
    VC = $(VCROOT)/VC7
    VCCOMMON = $(VCROOT)/Common7
    VCDEV = $(VCROOT)/Common7/IDE
    LIBPATH_SDK = /LIBPATH:$(CDRIVE_WIN)/$(VC)/PlatformSDK/Lib
    INCPATH_SDK = $(CDRIVE_WIN)/$(VC)/PlatformSDK/Include
  endif

  ifeq ($(MSVC_VERSION),8)
   VCROOT =  Program\ Files/Microsoft\ Visual\ Studio\ 8
   VC = $(VCROOT)/VC
   VCCOMMON = $(VCROOT)/Common7
   VCDEV = $(VCROOT)/Common7/IDE
   LIBPATH_SDK = /LIBPATH:$(CDRIVE_WIN)/Program\ Files/Microsoft\ Platform\ SDK/lib
   INCPATH_SDK = $(CDRIVE_WIN)/Program\ Files/Microsoft\ Platform\ SDK/Include
  endif


INCLUDES = /I $(CDRIVE_WIN)/$(VC)/Include
INCLUDES  += /I. /I$(CARBON_WHOME)/lib/xactors
CARBON_CL  = $(CDRIVE_WIN)/$(VC)/bin/cl
CARBON_CXX = $(CARBON_CL) $(INCLUDES) 
CARBON_CC = $(CARBON_CL)
CARBON_LINK = $(CDRIVE_WIN)/$(VC)/bin/link

CFLAGS	  = $(LOCAL_CFLAGS)

  # All the windows libraries we might EVER want.
  WINDOWS_LIBS =kernel32.lib user32.lib gdi32.lib winspool.lib\
                comdlg32.lib advapi32.lib shell32.lib ole32.lib\
                oleaut32.lib uuid.lib odbc32.lib odbccp32.lib 

  #########
  # PATH  #  for DLLS, etc. require a unix-style path. 
  #########
  # Cygwin creates a Windows-style PATH from this.
  # Do not use += on CARBON_TMP_PATH, it needs : between values.
  #  += inserts a space between values.

   CARBON_TMP_PATH  = $(CARBON_UHOME)/Win/lib
   CARBON_TMP_PATH := $(CARBON_TMP_PATH):$(CARBON_UHOME)/Win/lib/winx
   CARBON_TMP_PATH := $(CARBON_TMP_PATH):$(CDRIVE_UNIX)/$(VC)/Lib
   CARBON_TMP_PATH := $(CARBON_TMP_PATH):$(CDRIVE_UNIX)/$(VCDEV)
   CARBON_TMP_PATH := $(CARBON_TMP_PATH):$(CDRIVE_UNIX)/$(VC)/bin
   CARBON_TMP_PATH := $(CARBON_TMP_PATH):$(CDRIVE_UNIX)/$(VCCOMMON)/Tools
   CARBON_TMP_PATH := $(CARBON_TMP_PATH):.
   CARBON_TMP_PATH := $(CARBON_TMP_PATH):./VHM_static_Win
   CARBON_TMP_PATH := $(CARBON_TMP_PATH):$(PATH)

#   for dynamic links add this dir to path
#   CARBON_TMP_PATH :=$(CARBON_TMP_PATH):$(CARBON_UHOME)/Win/lib/winx/shared


  # this sed command escapes any unescaped spaces, which will probably be
  # scattered thoughout the PATH
  #
  CARBON_PATH:=$(shell echo $(CARBON_TMP_PATH) | sed -e 's/\([^\\]\) /\1\\ /g')


else
  # Running on non-Windows host
  WINHOST =
endif





#############
#  LIBPATH  #  for MS link command must use WINDOWS paths
#############

CARBON_VC_static_LIB_LIST =\
  /LIBPATH:$(CARBON_WHOME)/Win/lib/winx \
  /LIBPATH:$(CARBON_WHOME)/Win/winx/lib \
  /LIBPATH:$(CARBON_WHOME)/Win/winx/i386-mingw32msvc/lib \
  /LIBPATH:$(CARBON_WHOME)/Win/winx/lib/gcc/i386-mingw32msvc/$(gcc_VERSION) \
  libcarbonmem.a  libcarbonvspx.a \
  /LIBPATH:$(CARBON_WHOME)/Win/lib \
  /LIBPATH:$(CDRIVE_WIN)/$(VC)/Lib \
  libcarbon${CARBON_LIBRARY_VERSION}.lib \
  pthreadVC.lib ffwAPIexp.lib lmgr10.lib libz.a \
  libmingwex.a libstdc++.a libgcc.a \
  $(LIBPATH_SDK) \
  $(WINDOWS_LIBS)

#
# if CARBON_BIN = debug, then add these directories to link 
#
OBJDIR = $(CARBON_WHOME)/obj/Win.product

CARBON_VC_DEBUG_LIBS=  libcarbonsim.a \
  /LIBPATH:$(OBJDIR)/carbonsim \
  /LIBPATH:$(OBJDIR)/shell \
  /LIBPATH:$(OBJDIR)/wavetestbench \
  /LIBPATH:$(OBJDIR)/waveform \
  /LIBPATH:$(OBJDIR)/util \
  /LIBPATH:$(OBJDIR)/xactors/tbp


#
# subroutines for making sure we are on the proper type of system
#  usage: $(call checklinux)   or   $(call checkwin32)
checklinux = $(if $(WINHOST),echo "must execute on Linux host"||exit 1,)
checkwin32 = $(if $(WINHOST),,echo "must execute on Windows host"||exit 1)
