// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "codegen/ScheduleBucket.h"
#include "util/CarbonAssert.h"
#include "util/UtIOStream.h"

ScheduleBuckets::ScheduleBuckets() : mCurrent(NULL), mNextID(0)
{
}

ScheduleBuckets::~ScheduleBuckets()
{
  INFO_ASSERT(mCurrent == NULL, "ScheduleBucket pushed but not popped");
  for (BucketListType::const_iterator p = mBuckets.begin(), e = mBuckets.end();
       p != e;
       ++p)
    delete *p;
}
int
ScheduleBuckets::push(const UtString& name)
{
  ScheduleBucket* obj = new ScheduleBucket(name, mNextID, mCurrent /* parent */);
  mCurrent = obj;
  // Need to store buckets in a collection.
  mBuckets.push_back(mCurrent);
  return mNextID++;
}

int
ScheduleBuckets::pop()
{
  INFO_ASSERT(mCurrent != NULL, "ScheduleBuckets stack underflow");
  mCurrent = mCurrent->getParent();
  return mCurrent ? mCurrent->getID() : -1;
}

void
ScheduleBuckets::record()
{
  UtOFStream& compileDB = gCodeGen->getCGProfile()->getCompileDB();
  compileDB << "<schedule buckets>\n";

  // Write the schedule buckets in ID order.
  for (BucketListType::const_iterator p = mBuckets.begin(), e = mBuckets.end();
       p != e;
       ++p)
    compileDB << **p;
}


ScheduleBucket::ScheduleBucket(const UtString& name, int id,
                               ScheduleBucket* parent)
  : mName(name), mID(id), mParent(parent)
{
}

ScheduleBucket::operator UtString()
{
  UtString str;
  UtOStringStream stream(&str);
  stream << mID;
  // Examples, without and with parent:
  //   1 MyBucket
  //   2:1 MyChildBucket
  if (mParent != NULL)
    stream << ':' << mParent->mID;
  stream << ' '  << mName << '\n';
  return str;
}
