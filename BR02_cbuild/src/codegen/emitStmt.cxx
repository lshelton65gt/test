// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 /*
 * \file
 * Centralized implementation of all code emitters for classes derived from
 * class NUBase.
 *
 * We emit code by passing down context flags that guide each method
 * in establishing what kinds of code are needed (declarations,
 * expressions, or various kinds of names. 
 *
 * Context flags are returned for those places that need to be aware of
 * the generated context for a subordinate tree.  (See the NUAssign and
 * NUIdentLvalue emitCode() methods for an example.
 *
 * A very few routines do complex analysis of their operands or are
 * aware of the existance of multiple output files (see NUModule::emitCode())
 * 
 */

#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUSysFunctionCall.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUNetSet.h"

#include "hdl/HdlVerilogString.h"
#include "symtab/STBranchNode.h"
#include "util/CbuildMsgContext.h"
#include "util/StringAtom.h"
#include "util/UtStringUtil.h"

#include "reduce/Fold.h"
#include "reduce/RewriteUtil.h"

#include "emitUtil.h"
#include <math.h> // for double pow(double, double)

using namespace emitUtil;

// Output string buffer.  No longer need to ensure that this is aligned on
// big-endian target, because we've fixed the code that byte-swaps to not require
// a word-aligned buffer.
static void
sGenStringBuffer (UtOStream& out, const char* name, UInt32 size)
{
  out << "    char " << name << "[" << size << "]";
  out << ";" << ENDL;
}

// Calculate the minimum number of 32 bit words required
static UInt32
sGetMinNumWords (UInt32 bits)
{
  UInt32 words = bits/32;
  words += ( bits % 32 ) > 0 ? 1 : 0;
  return words;
}

// Generate the "if(condition)" part of an if or casex statement.
// (Handle BUG3115 and -expect flag processing centrally also)
// 
static void
sGenIfCondition (UtOStream& out, const NUExpr* condition, CGContext_t context)
{
  bool useExpect = (gCodeGen->getBuiltinExpectValue () != -1);

  UtString condTemp ("t$");

  // GCC BUG4794 - CARBON BUG3115.
  // conditional-test with complex boolean expression can cause GCC
  // constant-folder to hang.  Changing it to
  //   bool t$1 = expr;
  //   if (t$1) ...
  // breaks the icejam.
  //
  NUCost condCost;
  condition->calcCost (&condCost, gCodeGen->mCosts);

  bool workAround = (condCost.mInstructions > 32);

  out << TAG;

  if (workAround)
  {
    emitUtil::UniqueName (&condTemp);

    out << "  bool " << condTemp << " UNUSED =";
    if (not isPrecise (condition->emitCode (context | eCGFlow)))
    mask (condition->getBitSize(), "&");
    out << ";" << ENDL;
  }

  out << "   if (";
      
  // Generate:  if (__builtin_expect(cond, <value>)) ....
  // if we had a -expect 1|0 flag.
  //
  if (useExpect)
    out << "EXPECTED_VALUE(";
  
  if (workAround)
    // Already have the result of the conditional expression
    out << condTemp;
  else
  {
    if (not isPrecise (condition->emitCode (context | eCGFlow)))
      mask (condition->getBitSize(), "&");
  }

  if (useExpect)
    out << "," << gCodeGen->getBuiltinExpectValue () << ")";

  out << ")" << ENDL;

}

// Determine the machine data size used to represent a given number of bits
static UInt32 sPODTypeSize(UInt32 bits)
{
  UInt32 size = 0;
  if (bits <= 8) {
    size = 8;
  } else if (bits <= 16) {
    size = 16;
  } else if (bits <= 32) {
    size = 32;
  } else if (bits <= 64) {
    size = 64;
  }
  return size;
}


// if (<cond>) <then> else <else>; 
CGContext_t
NUIf::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream &out = gCodeGen->CGOUT ();

  NUExpr * condition = getCond();

  NUStmtCLoop thenLoop = loopThen();
  NUStmtCLoop elseLoop = loopElse();

  // Generate the "if (condition)", handling special-case forms like
  // expect(cond,[1|0])
  //
  out << TAG;
  sGenIfCondition (out, condition, context);

  out << "    {\n      ";
  out << TAG;
  emitCodeList (&thenLoop, context, "    ", "\n");
  out << "    } else {\n      ";
  out << TAG;
  emitCodeList (&elseLoop, context, "    ", "\n");
  out << "    }" << ENDL;

  return eCGVoid;
}

// 
// Because the initial or final stmts can be blocks, containing
// multiple statements, convert it to the semantic equivalent:
//
//	 <initial>; for (; <condition> ; ) { body; advance; }
CGContext_t 
NUFor::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGNoSemi);

  UtOStream &out = gCodeGen->CGOUT ();

  NUStmtCLoop initialLoop = loopInitial();

  out << TAG;
  emitCodeList (&initialLoop, context, "  ", "\n");

  out << "  " << TAG << "for (; (";

  // <condition>
  CGContext_t condition_context = getCondition()->emitCode (context | eCGFlow);

  if (not isPrecise (condition_context))
    mask (getCondition ()->getBitSize (), "&");

  out << "); ) {" << ENDL;

  NUStmtCLoop bodyLoop = loopBody ();
  emitCodeList(&bodyLoop, context, "\t", "\n");

  // <advance>
  out << "\t// advance" << ENDL;
  NUStmtCLoop advanceLoop = loopAdvance();
  emitCodeList (&advanceLoop, context, "\t", "\n");

  out << "    }" << ENDL;
  return eCGVoid;
}

// Extract constant indices from case expressions and print as case label
static bool
emitCaseIndex (UtOStream &out, 
               NUCaseItem::ConditionLoop conditions,
               UtSet<size_t> *caselabels)
{
  bool labelled = false;

  // Check for default action..
  if (conditions.atEnd ())
    {
      out << "  default:" << ENDL;
      return true;
    }

  for (; !conditions.atEnd(); ++conditions)
  {
    const NUCaseCondition* cond = *conditions;
    UInt64 val;
 
    const NUExpr* expr = cond->getExpr();
    VERIFY(isCTCE (expr, &val));

    if (caselabels->find ((UInt32)val) != caselabels->end ())
      // Already generated this case label
      continue;

    caselabels->insert ((UInt32)val);
    out << "  " << TAG << "case ";
    expr->emitCode (eCGDefine);
    out << ":";
    labelled = true;
  }

  return labelled;
} // emitCaseIndex

// case <sel> <caseitem-list>
CGContext_t
NUCase::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  bool newSelector = false;	// Must delete selector NUExpr
  UtOStream &out = gCodeGen->CGOUT ();

  out << "  // UserFullCase: " << isUserFullCase () << " FullySpecified: " << isFullySpecified ()
      << " Default: " << hasDefault () << ENDL;

  // Generate the if then else statements
  const NUExpr* sel = this->getSelect();

  UInt32 selSize = sel->getBitSize ();

  // C++ case size limit is LONG_BIT.
  // There was some experimentation with SHORT_BIT here, but Matrox is faster with LONG_BIT,
  // and I expect that to be true across the board.
  UInt32 limit = LONG_BIT;

  if (canEmitSwitch (false)    // Check that there are no ranges or masks
      && (selSize > 1)          // booleans are bad cases
      && (selSize <= limit))    // make sure case size is small enough
  {
    UtSet<size_t, std::less<size_t> > caselabels;
    
    size_t selWidth = sel->getBitSize ();
    CGContext_t rc;
    out << "  " << TAG << "switch (";

    if (emitUtil::needsSignExtension (NUOp::eUnPlus, sel, 0, selWidth, 0)) {
      rc = emitUtil::signExtend (out, sel, context, selWidth);
    } else {
      rc = sel->emitCode (context);
    }

    if (not isPrecise (rc)) {
      mask (selWidth, "&");
    }

    out << "){" << ENDL;
    
    if ((isUserFullCase () || isFullySpecified ()) && not hasDefault ()) {
      // Add a default branch label in the situation where there is no
      // default but the case stmt is fully specified either from the user
      // specification or we have determined it is so.  In some
      // situations this was found to be smaller if the compiler does
      // not need to worry about defaults (one example where this is done:
      // test/cust/matrox/sunex/carbon/cwtb/dmatop)
      out << "  default:" << ENDL;
    }

    for (NUCase::ItemCLoop items = this->loopItems(); !items.atEnd(); ++items) {
      NUCaseItem* caseItem = *items;
      if (emitCaseIndex(out, caseItem->loopConditions(), &caselabels)) {
        // We emitted at least one 'case <n>:' so we can emit
        // case actions.
        out << "    {" << ENDL;
        NUStmtLoop stmts = caseItem->loopStmts();
        emitCodeList (&stmts, context|eCGStmtList, "    ", "\n");
        out << "    break;\n    }" << ENDL;
      }
    }
    
    out << "}" << ENDL;
  }
  else
  {
    /*
     * Emit code that looks like:
     *   TypeT m_$temp_99 (<select_expr>); // constructor initializer
     *	 if (m_$temp_99 == cond-1)
     *	   actions-1;
     *	 else if (m_$temp_99 == cond-2)
     *	   actions-2;
     *	...
     */
    NUScope *scope = gCodeGen->getScope ();
    StringAtom* sym = scope->gensym("casesel", NULL, sel);
    NUNet *temp = scope->createTempNet (sym, sel->getBitSize (), 
                                        sel->isSignedResult(), getLoc ());
    NUIdentLvalue *val = new NUIdentLvalue (temp, getLoc ());
    CopyContext cc (NULL, NULL);

    NUStmt *asn = new NUBlockingAssign (val, sel->copy (cc), false, getLoc ());
    asn = gCodeGen->getFold ()->stmt (asn);

    NUExpr *newSel = val->NURvalue ();
    newSel->resize (sel->getBitSize ());

    sel = newSel;
    newSelector = true;

    // TODO: Implement support for initialized constructors.
    // Emit a local decl and an assign for now.
    out << TAG << CType (temp) << " " << member (temp) << ";\n  ";
    if (asn) {
      asn->emitCode (context);
      delete asn;
      out << ENDL;
    }

    UInt32 elseCount = 0;
    for (NUCase::ItemCLoop items = this->loopItems(); !items.atEnd();
         ++items)
    {
      const NUCaseItem* caseItem = *items;
      // Start with the conditional
      out << "  ";
      out << TAG;
      if (elseCount)
        out << "else {";
      
      NUExpr* expr = caseItem->buildConditional(sel);
      if (expr)
        // The default case returns a NULL for its conditional test
      {
        // if we had multiple conditions, the connection || didn't
        // get resized.  Since we now have a properly sized selector
        // temporary, the resize won't screw up lower level things
        // (see Interra CASE35.v)
        expr->resize (1);

        expr = gCodeGen->getFold ()->fold (expr);
        out << TAG;
        sGenIfCondition (out, expr, context);

        delete expr;
      }

      // Write out the statements for this condition
      out << "  " << ENDL;
      NUStmtCLoop stmts = caseItem->loopStmts();
      emitCodeList (&stmts, context|eCGStmtList, " {\n", "\n  ", "}\n");

      out << "  " << ENDL;
      ++elseCount;
    } // for

    while(elseCount > 1)
    {
      out << "}";
      --elseCount;
    }

    if (newSelector)
      delete sel;
  } // else
  
  return eCGVoid;
} // NUCase::emitCode

// ReadmemX System Task
CGContext_t
NUReadmemX::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream &out = gCodeGen->CGOUT ();

  // Emit the readmemx class
  out << TAG << "HDLReadMemX* " << local(getName()) << " = carbonInterfaceAllocReadMem(\"";
  out << mFileName->str() << "\", ";
  if (mHexFormat)
    out << "true";
  else
    out << "false";
  out << ", " << mLvalue->getWholeIdentifier ()->getMemoryNet ()->getRowSize();
  
  SInt64 first = mStartAddress;
  SInt64 second = mEndAddress;  

  if (! mEndSpecified) {
    // NC/Aldec/Finsim all do the same thing. They load the memory from lowest
    // value address to highest value address when the end is not specified
    first = std::min(mStartAddress, mEndAddress);
    second = std::max(mStartAddress, mEndAddress);
  }
  
  out << ", " << first << ", " << second << ", ";
  out << (mEndSpecified ? "true" : "false") << ", this);" << ENDL;
  
  // Call the memories readmem function
  mLvalue->emitCode (context | eCGLvalue);
  out << ".readmem(" << local(getName()) << ");" << ENDL;
  // Free the readmem object since we're done with it.
  out << "carbonInterfaceFreeReadMem(" << local(getName()) << ");" << ENDL;
  
  return eCGVoid;
}


//! Compute the number of decimal digits needed by a numBits binary number
static UInt32 
sDecimalWidth( UInt32 numBits )
{
  // 0.3010299957 = log10( 2 ), to 10 digits of precision
  return (UInt32)ceil( numBits * 0.3010299957 );
}


static CGContext_t
sEmitMasked (NUExpr *expr, CGContext_t context)
{
  UtOStream &out = gCodeGen->CGOUT ();
  out << TAG << "(";
  context = expr->emitCode (context);
  if (not isPrecise (context))
    mask (expr->getBitSize (), "&");
  out << ")";
  return Precise (context);
}

static CGContext_t
sEmitDisplayOperand (NUExpr *expr, UInt32 needed_bits, CGContext_t context)
{
  CGContext_t rc = eCGVoid;

  UtOStream &out = gCodeGen->CGOUT ();
  out << TAG << "(";

  if (emitUtil::needsSignExtension (NUOp::eUnBuf, expr, expr, needed_bits, 0)) {
    rc = emitUtil::signExtend (out, expr, context, needed_bits);
  } else {
    rc = sEmitMasked(expr, context);
  }
  out << ")";
  return rc;
}

// DollarDisplay type tasks
CGContext_t
NUOutputSysTask::emitCode(CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  SInt32 counter = 0;           // used in comments in generated code (debug aid)
  UInt32 argCounter = 0;        // the index of the current argument (0 means no arguments yet)

  UtOStream &out = gCodeGen->CGOUT ();

  out << "/* NUOutputSysTask: " << counter << " " << local(getName()) << " */" << ENDL;

  out << "\n  {" << ENDL;

  // setup for output to proper file(s)
  NUExpr* fileSpecExpr = getFileSpec();
  
  if(isVerilogTask())
  {
    if ( fileSpecExpr != NULL )
    {
      out << "    VerilogOutFileSystem* outstreamVerilog = carbonInterfacePutVerilogOutputFileDescriptor(this, ";
      fileSpecExpr->emitCode(context);
      out << " );" << ENDL << "\n";
    }
    else
    {
      out << "    VerilogOutFileSystem* outstreamVerilog = carbonInterfacePutVerilogOutputFileDescriptor(this, 1U);" << ENDL << "\n";
      // 1 (and 0x80000001) are stdout
    }
    // Explicitly upcast to UtOStream
    out << "    UtOStream* outstream UNUSED = carbonInterfaceCastVerilogOutFileSystemToOStream(outstreamVerilog);" << ENDL;
  }
  else
  {
    // Its a VHDL procedure. WRITE or WRITELINE.
    // fileSpecExpr can be :
    //    a. NUVHDLLineNet      ex. write(LINE, value)
    //    b. NUVectorNet        ex. write(file_object, value)
    //    c. NUConst            ex. write(STD.TEXTIO.OUTPUT, value)
    bool writeToLine  = (fileSpecExpr->getType() == NUConst::eNUConstNoXZ) ? 
      false : fileSpecExpr->getWholeIdentifier()->isVHDLLineNet();
    if (writeToLine)
    {
      // For write(LINE, type)
      out << "    UtOStream *outstream = carbonInterfaceCastIOStringStreamToOStream(";
      fileSpecExpr->emitCode(context);
      out << ");" << ENDL << "\n";
    }
    else
    {
      // For  write(fd, type)
      // For  writeline(fd, LINE)
      out << "    HdlOStream* outstreamVHDL = carbonInterfacePutVHDLOutputFileDescriptor(this, ";
      fileSpecExpr->emitCode(context);
      out << " );" << ENDL << "\n";
      // Explicitly upcast to UtOStream
      out << "    UtOStream* outstream UNUSED = carbonInterfaceCastHdlOStreamToOStream(outstreamVHDL);" << ENDL;
    }
  }

  UtString buf;
  NUExprVectorIter first = mValueExprVector.begin();
  if ( hasFileSpec() ) {
    ++first;                    // skip the file descriptor
    ++argCounter;
  }

  NUExprList localExprList(first,mValueExprVector.end());

  if (isWriteLineTask())
  {
    // Representing VHDLs WRITELINE procedure.
    out << "    carbonInterfaceHdlOStreamWriteLine(outstreamVHDL, ";
    NUExpr* expr = localExprList.front();
    sEmitMasked (expr, context);
    out << " );" << ENDL << "\n";

    out << "  }" << ENDL;   // close the HdlOStream scope.
    return eCGVoid;
  }

  while ( not localExprList.empty() )
  {
    NUExpr* expr = localExprList.front();
    localExprList.pop_front();
    ++argCounter;

    NUConst * constExpr =  expr->castConst ();
    if ( constExpr && constExpr->isDefinedByString() )
    {
      UInt32 curFormatStringArgNum = argCounter;
      UInt32 curFormatStringSubArgNum = 0;
      UtString formatstring;
      // the following uses composeNoProtected since we need the actual
      // string even if it was `protected (all but first arg does not matter)
      expr->composeNoProtected(&formatstring, NULL, false, false);
      formatstring = formatstring.substr(1,formatstring.size()-2); // remove the surrounding quotes
      // a format string can have any number of format specifiers ("%")
      // within it, so we print it by parts. first up to the %,
      // then process the % arg (using another argument from localExprList if
      // needed), then start again processing the remaining format string by
      // printing up to the next % ...

      UtString::size_type nextPercentPos;
      UtString::size_type numFormatChars = 1;

      while ( ( not formatstring.empty() ) and
              ( (nextPercentPos = formatstring.find_first_of("%")) != UtString::npos ) )
      {
        // first print the part before the first format code
        UtString literalPrefix = formatstring.substr(0,nextPercentPos);
        out << "    sWriteToOStream(outstream, \"" << literalPrefix << "\"); //9" << ENDL;
        if (nextPercentPos+2 > formatstring.size())  {
          // here we use curFormatStringSubArgNum+1 because we are on the cusp of the format field
          gCodeGen->getMsgContext()->CGInvalidFormatSpecifier(&(getLoc()), curFormatStringArgNum, curFormatStringSubArgNum+1);
          break;
        }

        formatstring = formatstring.substr(nextPercentPos+1, UtString::npos); // now starts with char after %
        curFormatStringSubArgNum++;

        bool left_adjustment = false;
        bool show_sign_symbol = false;
        while ( ( formatstring[0] == '-' ) or ( formatstring[0] == '+' ) ){
          if ( formatstring[0] == '-' ){
            left_adjustment = true;
            formatstring = formatstring.substr(1, UtString::npos); // now starts with char after left justify indicator '-'
          } else if ( formatstring[0] == '+' ){
            show_sign_symbol = true;
            formatstring = formatstring.substr(1, UtString::npos); // now starts with char after show_sign indicator '+'
          }
        }
        
        // now see if there is a size field
        UtString::size_type endOfSizeField             = formatstring.find_first_not_of("0123456789");
        UtString::size_type endOfSizeAndPrecisionField = endOfSizeField;
        UtString::size_type precisionLength;
        if (endOfSizeField == UtString::npos) {
          // the size character was the last character in the format string
        } else if ( formatstring[endOfSizeField] != '.' ){
          // there is no precision field
        } else if (UtString::npos == (precisionLength = 
                     formatstring.substr(endOfSizeField+1, UtString::npos).find_first_not_of("0123456789"))) {
          endOfSizeAndPrecisionField = UtString::npos;
        } else {
          endOfSizeAndPrecisionField = endOfSizeField + 1 + precisionLength;
        }
        UtString sizeField;
        UtString precisionField;
        SInt32 sizeAndPrecisionWidth = 0;
        bool user_specified_fill_with_zero = false;
        
        if ( ( endOfSizeAndPrecisionField != UtString::npos ) and ( endOfSizeAndPrecisionField != 0 ) ) {
          // if there is a sizeAndPrecisionField then part of it is the size field
          if ( endOfSizeField != endOfSizeAndPrecisionField ){
            sizeField.append(formatstring,0, endOfSizeField);
            precisionField.append(formatstring,endOfSizeField+1,endOfSizeAndPrecisionField-endOfSizeField-1);
            sizeAndPrecisionWidth = endOfSizeAndPrecisionField;
          }
          else { // endOfSizeField == endOfSizeAndPrecisionField 
            // not floating point
            // the LRM (section 17.1.1.3) says that you can only
            // specify the size of a field with a single zero
            // character: %0h, where this zero form says use the
            // minimimum width field. A non-zero form (%h) requests an
            // output width that will hold the maximum possible value
            // for the variable being displayed.
            // HOWEVER Some simulators support a more c like field
            // specification:
            // $display("test string :%04d, :%0d, :%d, :%00d,", 10, 10, 10, 10);
            // produces
            //   "test string :0010, :10, :         10, :10,"
            // the following code supports this second version (which
            // includes and extends the LRM version)
            // Also some simulators support a size field even when it
            // does not start with 0, we now support that also
            sizeAndPrecisionWidth = ( endOfSizeAndPrecisionField != UtString::npos )?  endOfSizeAndPrecisionField : 0; 
            sizeField.append(formatstring,0, endOfSizeField);
            if ( endOfSizeField != 1 ){
              // if sizefield is not a single char then check for
              // leading 0, if so fill with zeros and the width is the
              // numeric value of the size field value.
              user_specified_fill_with_zero = ( sizeField[0] == '0' );
              if ( user_specified_fill_with_zero ){
                // sizefield starts with a zero,
                // remove all leading zeros so we do not confuse this
                // with an octal constant
                UtString::size_type end_of_leading_zeros = sizeField.find_first_not_of("0");
                if ( ( end_of_leading_zeros != 0 ) and ( end_of_leading_zeros != UtString::npos ) ){
                  sizeField.erase(0, end_of_leading_zeros); // prune the leading zeros
                }
              }
            }
          }
        }
        formatstring = formatstring.substr(sizeAndPrecisionWidth, UtString::npos); // now starts with char after sizeAndPrecison

        // now some checking for correct format strings
        if ( formatstring.empty() ) {
          // I am not sure this is possible
          gCodeGen->getMsgContext()->CGInvalidFormatSpecifier(&(getLoc()), curFormatStringArgNum, curFormatStringSubArgNum);
          break;
        }


        const char* formatchar = formatstring.c_str();
        switch ( formatchar[0] )
        {
        case '%': {
          out << "    sWriteToOStream(outstream, \"%\");" << ENDL;
          break;
        }
        case 'l': {
          out << "    sWriteToOStream(outstream, \"<libraryNameGoesHere>\");" << ENDL;
          if ( not useAnotherOutputSysTaskArgument(&localExprList, &argCounter, formatchar[0], &expr) ) {
            break;
          }
          break;
        }
        case 't': {
          // consumes an argument
          if ( not useAnotherOutputSysTaskArgument(&localExprList, &argCounter, formatchar[0], &expr) ) {
            out << "    sWriteToOStream(outstream, ' ');" << ENDL;
            break;
          }

          // Set the output formatting.  The constants in setw(20) and
          // setprecision(0) come from the default definition of the
          // Verilog $timeformat.
          NUSysFunctionCall *func = dynamic_cast<NUSysFunctionCall*>( expr );

          // Find out how to scale to the $timeformat values.
          // get timeUnit from this outputSystemTask, not from the current module because
          // flattening might have happened, also not from func because it might
          // not exist, (like when we are printing a real number.
          const SInt8 units_number = gCodeGen->getDesign()->getGlobalTimePrecision();
          SInt32 formatDiff = getTimeUnit() - units_number;

          double formatMult = pow( 10.0, formatDiff );
          
          out << "    carbonInterfaceSetOStreamRadixDec(outstream);" << ENDL;
          out << "    carbonInterfaceSetOStreamFill(outstream, ' ');" << ENDL;
          out << "    carbonInterfaceSetOStreamWidth(outstream, 20);" << ENDL;
          out << "    sWriteToOStream(outstream, ";

          if ( func ) {
            // The expr will generate itself to the correct datatype.

            // We're going to scale the value; cast it back to the correct type
            switch ( func->getOp() )
            {
            case NUOp::eZSysTime:
            case NUOp::eZSysStime:
              out << TAG << "static_cast<CarbonTime>(";
              break;
            case NUOp::eZSysRealTime:
              // doubles need no cast since there's no implicit type conversion
              out << "(";
              break;
            default:
              NU_ASSERT("Unexpect system func op" == 0, expr);
              break;
            }
            // The %t output needs to be scaled to the correct time
            // units, as defined by the $timeformat directive.  We don't
            // yet support $timeformat, so the default value from the
            // $timeformat def is used.
            out << TAG << "static_cast<CarbonReal>(" << formatMult << ") * (";
            // Finally, emit the time expression
            sEmitDisplayOperand (expr, 64, context);

            out << ")));" << ENDL;
          }
          else if ( expr->isReal() )
          {
            // It is valid to pass a real number or a realtime instead
            // of a call to $realtime to the %t option.
            // Although we have a real number here, we still need to
            // scale it to the $timeformat specification
            out << TAG << "(static_cast<CarbonReal>(" << formatMult << ") * (";
            sEmitDisplayOperand (expr, 64, context);
            out << ")));" << ENDL;
          }
          else
          {
            // convert the argument into a real number, and scale to
            // the $timeformat specification.
            out << TAG << "(static_cast<CarbonReal>(" << formatMult << ") * (static_cast<CarbonReal>(";
            sEmitDisplayOperand (expr, 64, context);
            out << "))));" << ENDL;
          }
          break;
        }
        case 'm': {
          if (mPromotePath != NULL) {
            out << "    " << TAG << "sWriteToOStream(outstream, \"" << mPromotePath << "\");" << ENDL;
          }
          else {
            out << "    " << TAG << "sWriteToOStream(outstream, get_inst_name());" << ENDL;
          }
          STBranchNode* nameBranch = getNameBranch();
          if (nameBranch != NULL) {
            // Append the extra instance name because this $display
            // got flattened.
            UtString instances, slashed;
            nameBranch->compose(&instances);
            StringUtil::escapeCString(instances.c_str(), &slashed);
            out << "sWriteToOStream(outstream, \"." << slashed << "\");" << ENDL;
          }
          break;
        }

        case 'e':
        case 'f':
        case 'g':
        {
          if ( not useAnotherOutputSysTaskArgument(&localExprList, &argCounter, formatchar[0], &expr) ) {
            out << "    sWriteToOStream(outstream, ' ');" << ENDL;
            break;
          }

          bool revert_format = false;
          switch ( formatchar[0] ){
          case 'f': {
            out << "    carbonInterfaceSetOStreamFloatModeFixed(outstream);" << ENDL;
            revert_format = true;
            break;
          }
          case 'e':{
            out << "    carbonInterfaceSetOStreamFloatModeExponent(outstream);" << ENDL;
            revert_format = true;
            break;
          }
          }
          if (not  sizeField.empty() ) {
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << sizeField << ");" << ENDL;
          }
          if ( not precisionField.empty() ) {
            out << "    carbonInterfaceSetOStreamPrecision(outstream, " << precisionField << ");" << ENDL;
          }
          if ( left_adjustment ){
            out << "    carbonInterfaceSetOStreamJustificationLeft(outstream);" << ENDL;
          }
          if ( show_sign_symbol ){
            out << "    carbonInterfaceSetOStreamPosDisplayShow(outstream);" << ENDL;
          }

          out << "    sWriteToOStream(outstream, (double)";
          sEmitDisplayOperand (expr, 64, context);
          out << ");" << ENDL;

          // Revert all settings to default
          if ( show_sign_symbol ){
            out << "    carbonInterfaceSetOStreamPosDisplayNoShow(outstream);" << ENDL;
          }
          if ( left_adjustment ){
            out << "    carbonInterfaceSetOStreamJustificationRight(outstream);" << ENDL;
          }
          if ( not precisionField.empty() ) {
            out << "    carbonInterfaceSetOStreamPrecision(outstream, 0);" << ENDL;
          }
          if (not  sizeField.empty() ) {
            out << "    carbonInterfaceSetOStreamWidth(outstream, 0);" << ENDL;
          }
          if (revert_format) {
            out << "    carbonInterfaceSetOStreamFloatModeGeneral(outstream);" << ENDL;
          }
          break;
        }

        case 'c':
        {
          if ( not useAnotherOutputSysTaskArgument(&localExprList, &argCounter, formatchar[0], &expr) ) {
            out << "    sWriteToOStream(outstream, ' ');" << ENDL;
            break;
          }

          UInt32 bit_size = expr->getBitSize();
          out << "    " << TAG << "carbonInterfaceSetOStreamWidth(outstream, 1);\n";
          out << "    carbonInterfaceSetOStreamMaxWidth(outstream, 1);\n";

          out << "    carbonInterfaceWriteCharToOStream(outstream, ";
          out << "static_cast<char>";            // make sure this will be interpreted as a char
          out << "(";
          sEmitDisplayOperand (expr, 8, context);
          if ( bit_size > 64 ){
            out << ".partsel(0,8).value(0,8)";                // for bitvectors get only the low 8 bits
          }
          out << ")";
          out << ");" << ENDL;
          break;
        }
        case 'd':
        {
          if ( not useAnotherOutputSysTaskArgument(&localExprList, &argCounter, formatchar[0], &expr) ) {
            out << "    sWriteToOStream(outstream, ' ');" << ENDL;
            break;
          }

          UInt32 bit_size = expr->getBitSize();
          // note: non-LRM feature:
          // LRM says: the format specifier %0d means minimum width
          // field, (so no fill char required).
          // some simulators (but not LRM) support a format string of
          // the form %05d which means width of 5 and 0 fill
          // some simulators also support %5d which means width of 5
          // with the default fill.
          // note that in the case of %-05d (which is not in the LRM)
          // we ignore the fill with 0 since there is nothing to fill
          out << "    " << TAG << "carbonInterfaceSetOStreamRadix" <<
            ( (expr->isSignedResult()) ? "SDec" : "Dec" ) << "(outstream);\n";
          out << "    carbonInterfaceSetOStreamFill(outstream, " <<
            ( ( user_specified_fill_with_zero && (not left_adjustment) ) ? "'0'" : "' '") <<
            ");\n";
          if (not  sizeField.empty() )
          {
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << sizeField << ");\n";
          }
          else
          {
            UInt32 charWidth = sDecimalWidth( bit_size ) + (expr->isSignedResult() ? 1 : 0);
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << charWidth << ");\n";
            out << "    carbonInterfaceSetOStreamMaxWidth(outstream, " << charWidth << ");\n";
          }
          if ( left_adjustment ){
            out << "    carbonInterfaceSetOStreamJustificationLeft(outstream);\n";
          }
          if ( show_sign_symbol ){
            out << "    carbonInterfaceSetOStreamPosDisplayShow(outstream);\n";
          }

          out << "    sWriteToOStream(outstream, ";
          if ( bit_size <= 8 ){
            // if size is 8 or fewer bits then cast to a size that will not be interpreted as a char
            out << "static_cast<UInt32>";
          }
          UInt32 physical_size = (bit_size < 64)? physicalSize(bit_size) : bit_size ;
          out << "(";
          sEmitDisplayOperand (expr, physical_size, context);
          out << TAG << "));\n";

          // Revert to defaults
          if ( show_sign_symbol ){
            out << "    carbonInterfaceSetOStreamPosDisplayNoShow(outstream);\n";
          }

          if ( left_adjustment ){
            out << "    carbonInterfaceSetOStreamJustificationRight(outstream);\n";
          }
          out << "    carbonInterfaceSetOStreamFill(outstream, ' ');\n";
          // already in UtIO::dec radix, no need to switch back
          break;
        }

        case 'x':               // not really verilog but we have testcases that use this
        case 'h':
        {
          if ( not useAnotherOutputSysTaskArgument(&localExprList, &argCounter, formatchar[0], &expr) ){
            out << "    sWriteToOStream(outstream, ' ');" << ENDL;
            break;
          }

          UInt32 bit_size = expr->getBitSize();
          out << "    " << TAG << "carbonInterfaceSetOStreamRadixHex(outstream);\n";
          out << "    carbonInterfaceSetOStreamFill(outstream, '0');\n";
          if ( not sizeField.empty() ) {
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << sizeField << ");" << ENDL;
          }
          else {
            UInt32 charWidth = (bit_size+3)/4;
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << charWidth << ");" << ENDL;
            out << "    carbonInterfaceSetOStreamMaxWidth(outstream, " << charWidth << ");" << ENDL;
          }
          out << "    sWriteToOStream(outstream,  ";
          bool needs_mask = false;
          if ( bit_size <= 8 ){
            // if size is 8 or fewer bits then cast to a size that will not be interpreted as a char
            out << "static_cast<UInt32>";
            needs_mask =  expr->isSignedResult();
          }
          out << "(";
          sEmitMasked (expr, context);
          if ( needs_mask ) {
            mask(expr->getBitSize (), "&", true);
          }
          out << "));" << ENDL;
          out << "    carbonInterfaceSetOStreamRadixDec(outstream);" << ENDL;
          out << "    carbonInterfaceSetOStreamFill(outstream, ' '); //2h" << ENDL;
          break;
        }

        case 'o':
        {
          if ( not useAnotherOutputSysTaskArgument(&localExprList, &argCounter, formatchar[0], &expr) ) {
            out << "    sWriteToOStream(outstream, ' ');" << ENDL;
            break;
          }
          UInt32 bit_size = expr->getBitSize();

          out << "    " << TAG << "carbonInterfaceSetOStreamRadixOct(outstream);\n";
          out << "    carbonInterfaceSetOStreamFill(outstream, '0');\n";
          if ( not sizeField.empty() ) {
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << sizeField << ");" << ENDL;
          }
          else {
            UInt32 charWidth = (bit_size+2)/3;
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << charWidth << ");" << ENDL;
            out << "    carbonInterfaceSetOStreamMaxWidth(outstream, " << charWidth << ");" << ENDL;
          }
          out << "    sWriteToOStream(outstream,  ";

          bool needs_mask = false;
          // now we need to make sure this operand is not sign
          // extended since octal characters may require more bits
          // than we have

          // function call has opened one set of parens already
          UInt32 close_parens = 1;
          if ( expr->isSignedResult() ){
            if        ( bit_size <= 8 ){
              out << "static_cast<UInt32>(static_cast<UInt8>";
              needs_mask = true;
              close_parens++;
            } else if ( bit_size <= 16 ){
              out << "static_cast<UInt16>";
            } else if ( bit_size <= 32 ){
              out << "static_cast<UInt32>";
            } else if ( bit_size <= 64 ){
              out << "static_cast<UInt64>";
            }
          } else if ( bit_size <= 8 ){
            // if size is 8 or fewer bits then cast to a size that
            // will not be interpreted as a char on all platforms
            out << "static_cast<UInt32>";
          }
          out << "(";
          sEmitMasked(expr,context);
          if ( needs_mask ) {
            mask(expr->getBitSize (), "&", true);
          }
          out << ")";
          while ( close_parens-- ){
            out << ")";
          }
          out << ";" << ENDL;
          out << "    carbonInterfaceSetOStreamRadixDec(outstream);" << ENDL;
          out << "    carbonInterfaceSetOStreamFill(outstream, ' ');" << ENDL;
          break;
        }

	case 'u':
	{
          if ( not useAnotherOutputSysTaskArgument(&localExprList, &argCounter, formatchar[0], &expr) ) {
            out << "    sWriteToOStream(outstream, ' ');" << ENDL;
            break;
          }
	  // Calculate the number of 32 bit words needed.
	  UInt32 exprWordSize = sGetMinNumWords(expr->getBitSize());

	  out << "{" << ENDL;
	  // Create a buffer to hold the raw data
	  const char* bufName = "l_buf";
          sGenStringBuffer (out, bufName, exprWordSize*4);

	  // This variable holds the data
	  out << "    " << CBaseTypeFmt (exprWordSize*32, eCGVoid, expr->isSignedResult(), expr->isReal(), NULL, expr) << "  l_bvtemp(";
	  expr->emitCode(context);
	  out << ");" << ENDL;
	  gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path); // make sure we have BitVector.h

	  // Copy the values to a buffer.
	  out << "    carbonInterfaceConvertValueToRawURep ( " << bufName << ", " << exprWordSize << ", ";
	  out << "reinterpret_cast<const UInt32*>(& l_bvtemp));" << ENDL;

	  // Write out the buffer
	  out << "    carbonInterfaceWriteBufToOStream(outstream, " << bufName << ", " << exprWordSize*4 << ");" << ENDL;

	  out << "}" << ENDL;

	  break;
	}  
	case 'z':
	{
          if ( not useAnotherOutputSysTaskArgument(&localExprList, &argCounter, formatchar[0], &expr) ) {
            out << "    sWriteToOStream(outstream, ' ');" << ENDL;
            break;
          }

	  // Calculate the number of 32 bit words needed.
	  UInt32 exprWordSize = sGetMinNumWords(expr->getBitSize());

	  out << "{" << ENDL;

	  // Create a buffer to hold the raw data
	  const char* bufName = "l_buf";
          sGenStringBuffer (out, bufName, exprWordSize*8);

	  // This variable holds the data
	  out << "    " << CBaseTypeFmt (exprWordSize*32, eCGVoid, expr->isSignedResult(), expr->isReal(), NULL, expr) << "  l_bvtemp(";
	  expr->emitCode(context);
	  out << ");" << ENDL;

	  // This variable holds the drive values. Right now they are all 0.
	  out << "    " << CBaseTypeFmt (exprWordSize*32, eCGVoid, expr->isSignedResult(), expr->isReal(), NULL, expr) << "  l_bvdrive(";
	  out << "static_cast<" << CBaseTypeFmt (exprWordSize*32, eCGVoid, expr->isSignedResult(), expr->isReal(), NULL, expr) << ">(0)";
	  out << ");" << ENDL;
	  gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path); // make sure we have BitVector.h

	  // Copy the contents of the two variables into the buffer. The %z specifier requires two words for every 32 bits 
	  // of data. The data is written in little-endian format. The encoding is as follows, where n is a bit in the word:
	  // Word0[n] | Word1[n] | Value
	  // ---------------------------
	  //     0    |     0    |   0
	  //     1    |     0    |   1
          //     0    |     1    |   z
          //     1    |     1    |   x
	  //
	  // Since cbuild does not support "x", the bits corresponding to x can appear as "0" or "1".
	  // The case of "z" is more complicated, since cbuild does support "z". However, none of the other
	  // format specifiers emit "z". Instead they emit "0".
	  out << "    carbonInterfaceConvertValueToRawZRep ( " << bufName << ", " << exprWordSize << ", ";
	  out << "reinterpret_cast<const UInt32*>(& l_bvtemp), reinterpret_cast<const UInt32*>(& l_bvdrive));" << ENDL;
	  out << "    carbonInterfaceWriteBufToOStream(outstream, " << bufName << ", " << exprWordSize*8 << ");" << ENDL;

	  out << "}" << ENDL;

	  break;
	}
        case 'b':
        {
          if ( not useAnotherOutputSysTaskArgument(&localExprList, &argCounter, formatchar[0], &expr) ) {
            out << "    sWriteToOStream(outstream, ' ');" << ENDL;
            break;
          }
          UInt32 bit_size = expr->getBitSize();
          out << "    " << TAG << "carbonInterfaceSetOStreamRadixBin(outstream);\n";
          out << "    carbonInterfaceSetOStreamFill(outstream, '0');\n";
          if ( not sizeField.empty() ) {
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << sizeField << ");" << ENDL;
          }
          else {
            UInt32 charWidth = bit_size;
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << charWidth << ");" << ENDL;
            out << "    carbonInterfaceSetOStreamMaxWidth(outstream, " << charWidth << ");" << ENDL;
          }
          out << "    sWriteToOStream(outstream,  ";
          if ( bit_size <= 8 ){
            // if size is 8 or fewer bits then cast to a size that will not be interpreted as a char
            out << "static_cast<UInt32>";
          }
          UInt32 physical_size = (bit_size < 64)? physicalSize(bit_size) : bit_size ;
          out << "(";
          sEmitDisplayOperand (expr, physical_size, context);
          out << "));" << ENDL;
          out << "    carbonInterfaceSetOStreamRadixDec(outstream);" << ENDL;
          out << "    carbonInterfaceSetOStreamFill(outstream, ' '); //2b" << ENDL;
          break;
        }
          
        case 's':
        {
          UInt32 bufSize = 0;

          if ( not useAnotherOutputSysTaskArgument(&localExprList, &argCounter, formatchar[0], &expr) ) {
            out << "    sWriteToOStream(outstream, ' ');" << ENDL;
            break;
          }
          UInt32 exprBitSize = expr->getBitSize();

          out << "    " << TAG << "carbonInterfaceSetOStreamFill(outstream, ' ');\n";
          if ( not sizeField.empty() ) {
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << sizeField << ");" << ENDL;
            UInt32 charWidth = (exprBitSize+7)/8;
            bufSize = charWidth+1; // this is probably wrong but at least big enough
          }
          else {
            UInt32 charWidth = (exprBitSize+7)/8;
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << charWidth << ");" << ENDL;
            out << "    carbonInterfaceSetOStreamMaxWidth(outstream, " << charWidth << ");" << ENDL;
            bufSize = charWidth+1;
          }
          out << "{" << ENDL;            // begin local scope
          const char* bufName = "l_buf";

          sGenStringBuffer (out, bufName, bufSize);

          // we use bitvectors for longer than 64 bits, is there a better test?
          if ( exprBitSize > 64 ) {
            switch ( expr->getType() ) {
            case NUExpr::eNUIdentRvalue: {
              // these case(s) are never a BVRef, so we can get their value directly
              out << "    carbonInterfaceConvertPtrToStrRep ( " << bufName << ", " << bufSize << ", " << bufSize-1 << ", reinterpret_cast<const UInt32*>(&";
              sEmitMasked (expr, context);
              out << "));" << ENDL;
              break;
            }
            default: {
              // this expr may be a BVRef, since a BVRef is not a
              // BitVector we copy the value into a bitvector, then
              // use this temp bitvector for the printing
              out << "    " << CBaseTypeFmt (expr) << "  l_bvtemp(";
              sEmitMasked (expr, context);
              out << ");" << ENDL;
              gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path); // make sure we have BitVector.h
              out << "    carbonInterfaceConvertPtrToStrRep ( " << bufName << ", " << bufSize << ", " << bufSize-1 << ", reinterpret_cast<const UInt32*>(& l_bvtemp));" << ENDL;
              break;
            }
            }
          }
          else {
            out << "    " << TAG << "carbonInterfaceConvert" << sPODTypeSize(exprBitSize) << "ToStrRep ( " << bufName << ", " << bufSize << ", static_cast<"
                << CBaseTypeFmt (exprBitSize, eCGDeclare, false)
                << ">(";
            sEmitMasked (expr, context);
            out << "));" << ENDL;
          }
          out << "     sWriteToOStream(outstream, " << bufName << ");" << ENDL;
          out << "     carbonInterfaceSetOStreamFill(outstream, ' '); //2s" << ENDL;
          out << "}" << ENDL;            // end local scope

          break;
        }
        case '.':
        case '+':
        case '-':{
          // invalid format most likely a typo of the form:
          // %3.2.1f or %-2-d or %+5-d
          gCodeGen->getMsgContext()->CGTypoFormatSpecifier(&(getLoc()), formatchar[0], curFormatStringArgNum, curFormatStringSubArgNum);
          break;
        }
        
        default:
        {
          if (endOfSizeField == UtString::npos || endOfSizeAndPrecisionField == UtString::npos) {
            gCodeGen->getMsgContext()->CGMissingFormatAfterSize (&(getLoc()), curFormatStringArgNum, curFormatStringSubArgNum);
          } else {
            gCodeGen->getMsgContext()->CGUnsupportedFormat (&(getLoc()), formatchar[0], curFormatStringArgNum, curFormatStringSubArgNum);
          }

          if ( not useAnotherOutputSysTaskArgument(&localExprList, &argCounter, formatchar[0], &expr) ) {
            break;
          }
          UInt32 exprBitSize = expr->getBitSize();

          // not supported format char, so use %h instead
          out << "    " << TAG << "carbonInterfaceSetOStreamRadixHex(outstream);\n";
          out << "    carbonInterfaceSetOStreamFill(outstream, '0');\n";
          if ( not sizeField.empty() ) {
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << sizeField << ");" << ENDL;
          }
          else {
            UInt32 charWidth = (exprBitSize+3)/4;
            out << "    carbonInterfaceSetOStreamWidth(outstream, " << charWidth << ");" << ENDL;
            out << "    carbonInterfaceSetOStreamMaxWidth(outstream, " << charWidth << ");" << ENDL;
          }
          out << "    sWriteToOStream(outstream,  ";
          UInt32 physical_size = (exprBitSize < 64)? physicalSize(exprBitSize) : exprBitSize ;
          sEmitDisplayOperand (expr, physical_size, context);
          out << ");" << ENDL;
          out << "    carbonInterfaceSetOStreamRadixDec(outstream);" << ENDL;
          out << "    carbonInterfaceSetOStreamFill(outstream, ' '); //2h" << ENDL;
          break;

        }
        }
        formatstring = formatstring.substr(numFormatChars, UtString::npos); // now starts with char after format specifier
      }
      // print any remaining part of the format string that has no format specifiers
      if (not formatstring.empty()) {
        out << "    sWriteToOStream(outstream, \"" << formatstring << "\"); //6" << ENDL;
      }
    }
    else if ( expr->getType() == NUExpr::eNUNullExpr ) {
      // a nullExpr puts a space in the output
      out << "    sWriteToOStream(outstream, \" \"); //4" << ENDL;
    }
    else {
      // a display of an expression with an unspecified format string
      // is always presented decimal (for $display/$write)
      out << "    " << TAG << "carbonInterfaceSetOStreamRadix" <<
        ( (expr->isSignedResult()) ? "SDec" : "Dec" ) << "(outstream);\n";
      UInt32 exprBitSize = expr->getBitSize();
      NUSysFunctionCall *func = dynamic_cast<NUSysFunctionCall*>(expr);
      // Real numbers use fit-to-size for display; everything else has a width
      if ( func == NULL || func->getOp() != NUOp::eZSysRealTime ) {
        UInt32 charWidth = sDecimalWidth( exprBitSize ) + (expr->isSignedResult() ? 1 : 0);
        out << "    carbonInterfaceSetOStreamWidth(outstream, " << charWidth << ");" << ENDL;
      }
      UInt32 need_size = (exprBitSize < 64)? physicalSize(exprBitSize) : exprBitSize ;
      out << "    sWriteToOStream(outstream, ";
      sEmitDisplayOperand (expr, need_size, context);
      out << "); //5" << ENDL;
    }
    out << ENDL;
 
    if(not isVerilogTask())  // Add a 'space' since vhdl lacks any formating.
      out << "    sWriteToOStream(outstream, ' ');" << ENDL;
  }
  if ( mAddNewline ) {           // $display has newline, $write does not
    out << "    carbonInterfaceOStreamEndl(outstream);" << ENDL;
  }
  out << "  }" << ENDL;
  return eCGVoid;
}

// returns true if another argument is taken from localExprList, false if no expression found
bool  NUOutputSysTask::useAnotherOutputSysTaskArgument(NUExprList *localExprList, UInt32* argCounter, char formatchar, NUExpr** expr) const
{
  if ( localExprList->empty() ) {
    gCodeGen->getMsgContext()->CGMissingArgument(&(getLoc()), (*argCounter)+1, formatchar);
    return false;               // no argument found
  }
          
  (*expr) = localExprList->front();
  localExprList->pop_front();
  (*argCounter)++;

  if ( (*expr)->getType() == NUExpr::eNUNullExpr ) {
    gCodeGen->getMsgContext()->CGNullArgument(&(getLoc()), formatchar, (*argCounter) );
    return false;               // this seems to match Aldec closely
  }
  return true;
}
// $fread $fscanf READ type tasks
// Although some Verilog related code is written in the following function,
// It has only been tested for VHDL READ, READLINE subprograms.
CGContext_t
NUInputSysTask::emitCode(CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);
  UtOStream &out = gCodeGen->CGOUT ();

  out << "/* NUInputSysTask: " << local(getName()) << " */" << ENDL;

  out << "\n  {" << ENDL;

  // setup for input from proper file
  NUExpr* fileSpecExpr = getFileSpec();

  if(isVerilogTask())
  {
    // Input tasks are not supported in Verilog.  In fact, there is no VerilogInFileSystem class!
    NU_ASSERT(0, this);
  }
  else
  {
    // Its a VHDL procedure. READ,OREAD,HREAD or READLINE.
    // fileSpecExpr can be :
    //    a. NUVHDLLineNet      ex. read(LINE, value)
    //    b. NUVectorNet        ex. read(file_object, value)
    //    c. NUConst            ex. readline(STD.TEXTIO.INPUT, LINE)
    bool readFromLine  = (fileSpecExpr->getType() == NUConst::eNUConstNoXZ) ?
      false : fileSpecExpr->getWholeIdentifier()->isVHDLLineNet();
    if (readFromLine)
    {
      // for read(LINE, type);
      out << "    UtIStream *instream = "; 
      out << "carbonInterfaceCastIOStringStreamToIStream(";
      fileSpecExpr->emitCode(context);
      out << ")";
      out << " ;" << ENDL << "\n";
    }
    else
    {
      if (isReadLineTask())
      {
        // For readline(fd,LINE)
        // For readline(STD.TEXTIO.INPUT, LINE)
        out << "    HdlIStream* instream = carbonInterfacePutVHDLInputFileDescriptor(this, ";
      }
      else
      {
        // for read(fd, type)
        out << "    UtIStream *instream = carbonInterfacePutNewVHDLInputFileDescriptor(this, ";
      }
      fileSpecExpr->emitCode(context);
      out << " );" << ENDL << "\n";
    }
  }

  UtString buf;
  NUExprVectorIter first = mValueExprVector.begin();
  ++first;                    // skip the file descriptor

  NUExprList localExprList = NUExprList(first,mValueExprVector.end());

  if (isReadLineTask())
  {
    // Representing VHDLs READLINE procedure.
    out << "    carbonInterfaceHdlIStreamReadLine(instream, ";
    NUExpr* expr = localExprList.front();
    expr->emitCode( context | eCGLvalue );

    out << " );" << ENDL << "\n";

    out << "  }\n";   // close the HdlIStream scope.
    return eCGVoid;
  }

  if ( not localExprList.empty() )
  {
    NUExpr* expr = localExprList.front();
    localExprList.pop_front();

    UInt32 lvalueBitSize = mReadLvalue->getBitSize();
    NUConst * constExpr =  expr->castConstNoXZ ();
    if ( constExpr && constExpr->isDefinedByString())
    {
      // For VHDL file reads, we insert a format string during nucleus 
      // population. It is of the following types only- "%0d", "%d" ,"%b", "%c" 
      UtString formatstring;
      expr->composeNoProtected(&formatstring, NULL, 0, false);
      // remove the surrounding quotes
      formatstring = formatstring.substr(1,formatstring.size()-2); 
      NU_ASSERT (formatstring[0] == '%', expr);
      switch ( formatstring[1] )
      {
      case 'b':
        out << "    " << TAG << "carbonInterfaceSetIStreamRadixBin(instream);\n";
        // Use UtIO::mMaxWidth to define the required number of characters.
        // At runtime if (required width != provided width), issue error.
        out << "    carbonInterfaceSetIStreamMaxWidth(instream, " << lvalueBitSize << ");" << ENDL;
        break;
      case 'h':
      {
        out << "    " << TAG << "carbonInterfaceSetIStreamRadixHex(instream);\n";
        UInt32 charWidth = (lvalueBitSize+3)/4;
        // Use UtIO::mMaxWidth to define the required number of characters.
        // At runtime if (required width != provided width), issue error.
        out << "    carbonInterfaceSetIStreamMaxWidth(instream, " << charWidth << ");" << ENDL;
        break;
      }
      case 'o':
      {
        out << "    " << TAG << "carbonInterfaceSetIStreamRadixOct(instream);\n";
        UInt32 charWidth = (lvalueBitSize+2)/3;
        // Use UtIO::mMaxWidth to define the required number of characters.
        // At runtime if (required width != provided width), issue error.
        out << "    carbonInterfaceSetIStreamMaxWidth(instream, " << charWidth << ");" << ENDL;
        break;
      }
      case 'd':
      default:
        out << "    " << TAG << "carbonInterfaceSetIStreamRadixDec(instream);\n";
        break;
      }
    }
    else
    {
      out << "    " << TAG << "carbonInterfaceSetIStreamRadixDec(instream);\n";
    }
    out << "    sReadFromIStream(instream, ";

    if (lvalueBitSize > LLONG_BIT)
    {
      // Compiler confuses between UtIStream>>(char*) and UtIStream>>(BVref&)
      // when the mReadLvalue is a BitVector<>.
      // gcc 2.95.3 doesn't complains though and works either way.
      NU_ASSERT( mReadLvalue->isWholeIdentifier(), mReadLvalue); // Others not supported.
      out << "(BVref<false>(";
      mReadLvalue->emitCode ( context | eCGLvalue );
      out << "))); //7" << ENDL << "\n";
    }
    else
    {
      mReadLvalue->emitCode ( context | eCGLvalue );
      out << "); //8" << ENDL;
      out << ENDL;
    }

    // This presumes that the lvalue holding the status is a scalar variable.  If
    // bitvectors or forcible nets or any of a number of other things can be used
    // for status, then we really need to use the "construct a NUAssign" and code
    // that model.  With a RHS that's not a nucleus expression, we can't make the NUAssign
    // unless we add a special RHS NUExpr that is basically a canned string
    if (mStatusLvalue)
    {
      out << "    " ;
      mStatusLvalue->emitCode( context | eCGLvalue );
      out << " = !carbonInterfaceIStreamFail(instream);" << ENDL;
    }
  }

  out << "  }" << ENDL;
  return eCGVoid;
}

// FClose tasks
CGContext_t
NUFCloseSysTask::emitCode(CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream &out = gCodeGen->CGOUT ();

  out << "/* NUFCloseSysTask: " << local(getName()) << " */" << ENDL;
  out << ENDL;

  // setup for closing of proper file(s)
  NUExpr* fileSpecExpr = getFileSpec();
  NU_ASSERT (fileSpecExpr, this);
  if(isVerilogTask())
  {
    out << "    carbonInterfaceCloseVerilogOutputFileSystem(this, ";
  }
  else
  {
    if (mUseInputFileSystem)
      out << "    carbonInterfaceCloseVHDLInputFileSystem(this, ";
    else
      out << "    carbonInterfaceCloseVHDLOutputFileSystem(this, ";
  }
  fileSpecExpr->emitCode(context);
  out << " );" << ENDL;
  return eCGVoid;
}

// FOpen tasks
CGContext_t
NUFOpenSysTask::emitCode(CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream &out = gCodeGen->CGOUT ();

  if (!isVerilogTask() && mValueExprVector.empty())
  {
    // initialization of a LINE variable in VHDL.
    out << "/* NUFOpenSysTask: for LINE " << local(getName()) << " */" << ENDL;

    out << "    ";
    getOutNet()->emitCode(context); // NULine
    out << " = carbonInterfaceAllocStringStream();" << ENDL;
    return eCGVoid;
  }

  out << "/* NUFOpenSysTask: " << local(getName()) << " */" << ENDL;
  out << ENDL;

  // setup for open of proper file
  NU_ASSERT (not mValueExprVector.empty(), this);
  // In bug7535 an empty string used as the constant filename caused the
  // filename argument to have length zero. This causes an assertion failure in
  // emitUtil::emitConstant. This assertion verifies that the empty string case
  // has been errored out before we get to codegen.
  NU_ASSERT (mValueExprVector [0]->getBitSize () > 0, mValueExprVector [0]);

  bool hasMode = ( mValueExprVector.size() == 2 );
  UInt32 bitsInFilename = mValueExprVector[0]->getBitSize();
  UInt32 filenameBufSize = 1 + ((bitsInFilename+7)/8);
  UInt32 bitsInMode = 0;
  UInt32 modeBufSize = 0;
  if ( hasMode )
  {
    bitsInMode = mValueExprVector[1]->getBitSize();
    NU_ASSERT (bitsInMode <= 24, this);    // this was checked by NUFOpenSysTask::isValid,
                                  // so the only way we can assert here is if
                                  // memory was corrupted
    modeBufSize = 1 + ((bitsInMode+7)/8);
  }

  out << "{" << ENDL;               // local scope for l_filenameBuf
  const char* filenameBufName = "l_filenameBuf";
  const char* modeBufName = "l_modeBuf";
  sGenStringBuffer (out, filenameBufName, filenameBufSize);
  if ( bitsInFilename > 64)
    out << "    " << TAG << "carbonInterfaceConvertPtrToStrRep( " << filenameBufName << ", "
        << filenameBufSize << ", " << filenameBufSize-1 << ", reinterpret_cast<const UInt32*>(&";
  else
    out << "    " << TAG << "carbonInterfaceConvert" << sPODTypeSize(bitsInFilename) << "ToStrRep(" << filenameBufName << ", " <<  filenameBufSize
        << ",static_cast<" << CBaseTypeFmt (bitsInFilename, eCGDeclare, false) << ">(";
  mValueExprVector[0]->emitCode(context); // first arg is always the filename
  out << "));" << ENDL;

  if ( hasMode )
  {
    // lrm says the mode has 3 or fewer chars, so we know it fits into 32 bits
    out << "    UInt32 l_mode = ";
    mValueExprVector[1]->emitCode(context); // second arg is always the mode
    out << ";" << ENDL;

    sGenStringBuffer (out, modeBufName, modeBufSize);
    out << "    carbonInterfaceConvert32ToStrRep(" << modeBufName << "," << modeBufSize << ",(l_mode));" << ENDL;
  }
  out << "    ";
  getOutNet()->emitCode(context); // file descriptor temp net
  if(isVerilogTask())
  {
    out << " = carbonInterfaceOpenVerilogOutputFileSystem(this, " << filenameBufName;
    if ( hasMode )
      out << ", " << (const char *)modeBufName;
    else {
      // With the new C interface, explicitly pass NULL as the second
      // argument.  It will be interpreted as the modeless version of
      // the function.
      out << ", NULL";
    }
    out << " );" << ENDL;
  }
  else
  {
    // vhdl FILE_OPEN procedure.
    if (mUseInputFileSystem)
      out << " = carbonInterfaceOpenVHDLInputFileSystem";
    else
      out << " = carbonInterfaceOpenVHDLOutputFileSystem";

    if (mStatusLvalue)
    {
      // FILE_OPEN() can optionally have the 1st arg of type FILE_OPEN_STATUS
      // The new model's C interface can't use function overloading
      // to invoke the version that takes a status argument, so it
      // needs to be called explicitly.
      out << "Status(this, ";
      out << TAG << "&";
      mStatusLvalue->emitCode( context | eCGLvalue);
      out << ", ";
    } else {
      out << "(this, ";
    }
    // With the new C interface, explicitly pass NULL
    // as the second argument.  It will be interpreted as the
    // modeless version of the function.
    out << "l_filenameBuf, " << ( hasMode ? "l_modeBuf" : "NULL") << " );" << ENDL;
  }
  out << "}" << ENDL;               // local scope for l_filenameBuf
  return eCGVoid;
}

// FFlush tasks
CGContext_t
NUFFlushSysTask::emitCode(CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream &out = gCodeGen->CGOUT ();

  out << "/* NUFFlushSysTask: " << local(getName()) << " */" << ENDL;
  out << ENDL;

  // setup for flush of proper file(s)
  NUExpr* fileSpecExpr = getFileSpec();
  // C interface has different functions depending on whether the
  // file descriptor is specified.
  if ( fileSpecExpr ) {
    out << "    carbonInterfaceFlushVerilogOutputFileSystemFD(this, ";
    fileSpecExpr->emitCode(context);
  } else {
    out << "    carbonInterfaceFlushVerilogOutputFileSystem(this";
  }
  out << " );" << ENDL;
  return eCGVoid;
}

// Control sys tasks
CGContext_t
NUControlSysTask::emitCode(CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream &out = gCodeGen->CGOUT ();

  //  out << "/* NUControlSysTask: " << local(getName()) << " */" << ENDL;
  //  out << ENDL;

  const char * callbackTypeStr = NULL;
  switch (mControlType)
  {
  case eCarbonStop:   { callbackTypeStr = "eCarbonStop"; break; }
  case eCarbonFinish: { callbackTypeStr = "eCarbonFinish"; break; }
  default: { NU_ASSERT("Unexpected ConstrolType" == 0, this); break;}
  }
  const char * verilogFilename = getLoc().getFile();
  UInt32 verilogLineNumber = getLoc().getLine();

  // make a call to do the work of the stop/finish
  out << TAG << "carbonInterfaceRunControlSysTask( this,  (UInt32)";
  getVerbosity()->emitCode(context);
  out << ", " << callbackTypeStr << ", \"" << verilogFilename << "\", " << verilogLineNumber;
  out << ");" << ENDL;
  return eCGVoid;
}


CGContext_t
NUCaseItem::emitCode(CGContext_t /* context */) const
{
  return eCGVoid;
}

CGContext_t
NUCaseCondition::emitCode(CGContext_t /* context */) const
{
  return eCGVoid;
}

CGContext_t
NUInitialBlock::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  return NUStructuredProc::emitCode(context|eCGInitial);
}

CGContext_t
NUAlwaysBlock::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  return NUStructuredProc::emitCode(context);
}

//! Collect separated blocks for which definitions/declarations have to be generated.
class SepBlockCollector : public NUDesignCallback
{
public:
  SepBlockCollector(NUBlockList* sepBlocks)
    : mSepBlocks(sepBlocks)
  {
  }

  ~SepBlockCollector() {
  }

  virtual Status operator()(Phase, NUBase *) { return eNormal; }

  //! Look for separated blocks and add them to definables list.
  virtual Status operator()(Phase phase, NUBlock* block)
  {
    if ((phase == ePre) && block->isSeparated()) {
      mSepBlocks->push_back(block);
    }
    return eNormal;
  }

private:
  NUBlockList* mSepBlocks;
};


/* An always or initial block becomes a member function */
CGContext_t
NUStructuredProc::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream &out = gCodeGen->CGOUT ();

  // Compute the cost of this always block and see if we are willing
  // to inline it.  We should also consider whether it's called in
  // more than one schedule - in that case, we probably don't want to
  // inline it.
  //
  NUCost cost;
  bool inlined = emitUtil::isInlineCandidate (this, &cost);

  // Now walk the function emitting either the declaration or definition.

  if (isNameOnly(context))
    {
      out << funct(getName());
      return context;
    }

  out << HDLTAG (this);

  if (not getBlock ()->isScheduled ()) {
    UtString procMsg ("block ");
    procMsg << funct (getName ());
    return checkDeadCode (getLoc (), context, procMsg);
  }

  if (isDeclare (context) || isDefine (context)) {
    // Collect definable blocks (separated blocks) in this scope.
    NUBlockList sepBlocks;
    {
      SepBlockCollector sbc(&sepBlocks);
      NUDesignWalker walker(sbc, false);
      walker.structuredProc(const_cast<NUStructuredProc*>(this));
    }

    // Emit code for objects subservient to this that require declaration and definition
    NUBlockLoop block_loop(sepBlocks);
    while (!block_loop.atEnd()) {
      NUBlock* block = *block_loop;
      ++block_loop;
      if (block == getBlock ()) {
        // this is the block for the structured procedure so do nothing here...
      } else {
        out << ENDL;
        block->emitCode (context);
      }
    }
  }

  if (isDeclare (context)) {
    // Output structure containing static instance members if any
    getBlock ()->emitCode (context);
  } else if (isDefine (context)) {
    if (inlined)
      out << "#if 0 " << ENDL;
    else if (not functionSection (this, 0))
      // Not in a function layout - remember that we wrote a real function body
      // into the .cxx
      gCodeGen->putNonEmptyCxx (gCodeGen->getCurrentModule ());
  }

  emitUtil::annotateCosts (cost);

  out << TAG << "void ";
  classPrefix (context);
  out << funct(getName());
  
  if (isDeclare (context) && not inlined)
    // All thru now.
    {
      UtString sectionName;
      if (emitUtil::functionSection (this, &sectionName)) {
        out << TAG;
        gCodeGen->emitSection (out, sectionName);
      }
      out << ";" << ENDL;
      return eCGVoid;
    }

  // In Define context, emit the function body
  // In Declare context, emit the struct containing the
  //  block-local member variables.
  //
  if (isDefine (context) || inlined) {
    out << "{" << ENDL;
    // Emitting a start bucket here is wrong.  Here we're just
    // initializing locals that don't correspond to the source
    // locator.
    //emitStartBucket();
    getBlock ()->emitCode ( (context & ~eCGDeclare) | eCGDefine);

    // If there are state update memories in this always block, update
    // the global temp memory from the local temp memory. (Note we
    // don't do this for initial blocks because those should write in
    // a blocking fashion.)
    if (!isInitialBlock(context)) {
      NUNetSet defs;
      getDefs(&defs);
      for (NUNetSet::iterator i = defs.begin(); i != defs.end(); ++i) {
        NUNet* net = *i;
        if (net->is2DAnything() && net->isDoubleBuffered()) {
          out << "  " << TAG;
          net->emitCode((context & ~eCGDeclare) | eCGDefine);
          out << ".localSync();" << ENDL;
        }
      }
    }
    out << "}" << ENDL;

    // Make sure we declared all temp memory nets
    gCodeGen->clearUndeclaredTempNets();
  }

  if (inlined && isDefine (context))
    out << "#endif " << ENDL;

  return eCGVoid;
}

typedef UtVector<const NUNamedDeclarationScope*> DeclScopeVector;

// Allocate static blocks within a named-scope.  Return the initial
// offset before we allocate any storage (or zero for non-static scopes
//
void sAllocateBlockNets (const NUNamedDeclarationScope * scope)
{
  // First see if there are any scoped static symbols in this block;
  NUNetCLoop locals = scope->loopLocals ();
  NUNetCVector svars;

  // Ask for referenced static variables not yet assigned storage offset
  StaticFilter allStatics (false);

  remove_copy_if (locals.begin (), locals.end (), back_inserter (svars),
		  // exclude non-statics or unreferenced symbols
		  std::not1 (allStatics));

  // If there are no more nested blocks containing declarations, we
  // can bail out
  //
  NUNetList subNets;
  scope->getAllSubNets (&subNets);
  NUNetList innerStatics;

  remove_copy_if (subNets.begin (), subNets.end (),
		  back_inserter (innerStatics),
		  std::not1 (allStatics));

  if (svars.empty () && innerStatics.empty ()) {
    return;
  }

  // The allocation for declaration scope starts with current offset.
  CGAuxInfo* cg = new CGAuxInfo;
  cg->setOffset (gCodeGen->getAllocation());
  scope->setCGOp (cg);

  std::stable_sort (svars.begin (), svars.end (), emitUtil::sort_by_align ());
  
  std::for_each (svars.begin (), svars.end (), allocateNet);

  // Now emit symbols contained in blocks nested inside this one

  NUNamedDeclarationScopeCLoop innerBlocks = scope->loopDeclarationScopes ();
  DeclScopeVector inners (innerBlocks.begin (), innerBlocks.end ());

  std::stable_sort (inners.begin (), inners.end (),
		    emitUtil::sort_by_align ());

  std::for_each (inners.begin (), inners.end (), sAllocateBlockNets);
}

/*!
 * Declaration scope...
 *
 * We used to provide name scoping within Verilog naming blocks for regs
 * that require static semantics.  
 *
 * Non-static nets/regs/wires get handled as local variables declared
 * within an NUBlock.
 *
 */
CGContext_t 
NUNamedDeclarationScope::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream& out = gCodeGen->CGOUT ();
  
  if (isDeclare (context))
    // to handle block-local storage requiring static semantics, we
    // have to emit a structure declaration containing the static
    // symbols
    //
    {
      // We allocate all the scoped static symbols at once when we
      // process the outermost block
      if (getParentScope ()->getScopeType () == NUScope::eModule)
        sAllocateBlockNets (this);

      NUNetCLoop locals = loopLocals ();
      NUNetCVector svars;


      // If doing a task/function, all the names in this scope are
      // non-static so we don't care if the symbol has an assignment.
      // For a normal module-level or always block scope, just ask for
      // statics that have actually been given storage offsets.
      
      StaticFilter allocatedStatics (not inTFHier());
      // Get the nets we actually allocated...
      remove_copy_if (locals.begin (), locals.end (), back_inserter (svars),
                      std::not1 (allocatedStatics));

      // First see if there are any scoped static symbols in this block;
      if (svars.empty()) {
        // We have no local nets to declare; if there are no nets in
        // subscopes, we can stop processing.
        NUNetList subNets;
        getAllSubNets (&subNets);
        NUNetList innerStatics;
        remove_copy_if (subNets.begin (), subNets.end (),
                        back_inserter (innerStatics),
                        std::not1 (allocatedStatics));
        // no nested scopes contain nets we need to declare.
        if (innerStatics.empty()) {
          return eCGVoid;
        }
      }

      // Ok. We have variables to process (either locally or nested).
      // Sort by address.
      emitUtil::sort_by_address addressSorter;
      std::stable_sort (svars.begin (), svars.end (), addressSorter);

      if (not svars.empty ()) {
        out << TAG;
        emitCodeList (&svars, context, "", ";\n");
      }

      // Now emit symbols contained in blocks nested inside this one
      NUNamedDeclarationScopeCLoop innerBlocks = loopDeclarationScopes();
      UtVector<const NUNamedDeclarationScope*> inners (innerBlocks.begin (),
                                                       innerBlocks.end ());

      std::stable_sort (inners.begin (), inners.end (), emitUtil::sort_by_address ());
      emitCodeList (&inners, context);
      out << TAG;

      // If we're at the top level of a task or function we need to zero this object
      if (inTFHier () && not getParentScope ()->inTFHier ()) {
        out << "; std::memset(&" << block (getName ()) << ", 0, sizeof "
            << block (getName ()) << ")";
      }
    }
  else if (isDefine (context))
    {
      // Nothing happens for definition context. This scope only
      // contains static variables.
    }

  return eCGVoid;
}

/*!
 * Block...
 *
 * Handles function declaration (function prototype and non-static
 * variables declared inside blocks) and function definition.
 */
CGContext_t 
NUBlock::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream& out = gCodeGen->CGOUT ();

  if (isDeclare (context))
    // to handle block-local storage requiring static semantics, we
    // have to emit a structure declaration containing the static
    // symbols
    //
    {
      // Nothing happens for declaration context. This scope only
      // contains static variables.
    }
  else if (isDefine (context))
    {
      gCodeGen->pushScope (this);

      // open the block.
      out << "\n{" << ENDL;

      emitUtil::genLocalDecls (this, false);
      
      // Now the statements in the scope
      NUStmtCLoop stmts = loopStmts ();
      emitCodeList (&stmts, context|eCGStmtList, "  ", "\n");

      // close the block.
      out << "\n}" << ENDL;

      // The previous bucket is no longer relevant, because we're done
      // with this method.
      gCodeGen->getCGProfile()->resetBucket();

      gCodeGen->popScope ();
    }

  return eCGVoid;
}

//! \class MarkNetReferenced
/*! Nets used in the closure of an NUSeparatedBlock must be marked as
 *  referenced when they are added to the closure so that emitNet does the
 *  right thing.
 */
class MarkNetReferenced : public NUSeparatedBlock::Callback {
public:
  MarkNetReferenced (const NUSeparatedBlock *block) :
    NUSeparatedBlock::Callback (block) {}
  virtual bool operator () (NUNet *net)
  {
    // Make sure that we have a CGAuxInfo record for this net.
    CGAuxInfo *cg = net->getCGOp ();
    if ((cg = net->getCGOp ()) == NULL) {
      cg = new CGAuxInfo ();
      net->setCGOp (cg);
    }
    if (cg->isPortAliased ()) {
      return false;                     // prevents adding net to the closure
    }
    // mark the net as referenced
    cg->setReferenced ();
    return true;                        // allows net to be added to the closure
  }
};

// Separated blocks are emitted as functions. Dead nets that are not local
// to the scope are not passed by reference to the function. They need to
// be replaced by temps in local scope to avoid compile failure.
void sTempLocalizeDeadNets(NUSeparatedBlock* block, NUNetSet& netsToLocalize)
{
  NUNetReplacementMap net_map;
  for (NUNetSet::iterator net_itr = netsToLocalize.begin();
       net_itr != netsToLocalize.end(); ++net_itr)
  {
    NUNet* orig_net = *net_itr;
    if (orig_net != NULL)
    {
      NUNet* temp_net =
        block->createTempNetFromImage(*net_itr, "localized_dead_net");
      net_map.insert(std::make_pair(orig_net, temp_net));
    }
  }

  NULvalueReplacementMap empty_lval_map;
  NUExprReplacementMap empty_expr_map;
  NUTFReplacementMap empty_tf_map;
  NUCModelReplacementMap empty_cm_map;
  NUAlwaysBlockReplacementMap empty_ab_map;

  RewriteLeaves rewriter(net_map, empty_lval_map, empty_expr_map,
                         empty_tf_map, empty_cm_map, empty_ab_map, NULL);
  block->replaceLeaves(rewriter);
}

CGContext_t 
NUSeparatedBlock::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream& out = gCodeGen->CGOUT ();

  {
    // Construct the closure of the separated block. This is the set of all nets
    // referenced in the block that have non-static semantics. These nets must be
    // passed as reference parameters into the function.
    MarkNetReferenced notify (this);
    NUNetSet netsToLocalize;
    computeClosure (notify, netsToLocalize);

    // These are the list of nets that aren't passed as reference parameters
    // into function. These are dead nets. The dead nets need to be
    // replaced by local temporaries for a successful compile. This list is
    // computed for us by computeClosure() function.
    sTempLocalizeDeadNets(const_cast<NUSeparatedBlock*>(this), netsToLocalize);
  }

  if (isNameOnly (context)) {
    out << funct (getName ());
    return context;
  }

  if (isDeclare (context)) {
    // Output the declaration of the method
    out << TAG << "void " << fname (getName ()) << "(";
    bool is_first = true;
    // The closure enumerates the formal parameters of the net. The formal
    // parameter name is just the name of the net so that nothing special must
    // be done when generating code that references the net.
    for (NUNetSetByName::const_iterator it = getClosure ().begin (); it != getClosure ().end (); ++it) {
      NUNet *net = *it;
      if (is_first) {
        is_first = false;
      } else {
        out << "," << ENDL;
      }
      out << TAG;
      net->emitCode (eCGDeclare|eCGReference|eCGList);
    }
    out << ")";
    UtString sectionName;
    if (emitUtil::functionSection (this, &sectionName)) {
        gCodeGen->emitSection (out, sectionName);
    }
    out << ";";
  } else if (isStmtList (context)) {
    // Output a call to the method. This branch MUST come before the isDefine
    // branch.
    out << fname (getName ()) << " (";
    bool is_first = true;
    // The actual parameters are just the nets in the closure.
    for (NUNetSetByName::const_iterator it = getClosure ().begin (); it != getClosure ().end (); ++it) {
      NUNet *net = *it;
      if (is_first) {
        is_first = false;
      } else {
        out << ", ";
      }
      out << TAG;
      net->emitCode (eCGNameOnly|eCGList);
    }
    out <<");";
  } else if (isDefine (context)) {
    // Output the definition
    // output the definition of the separated block
    out << "void ";
    classPrefix (context);
    out << fname (getName ()) << "(";
    bool is_first = true;
    for (NUNetSetByName::const_iterator it = getClosure ().begin (); it != getClosure ().end (); ++it) {
      NUNet *net = *it;
      if (is_first) {
        is_first = false;
      } else {
        out << "," << ENDL;
      }
      out << TAG;
      net->emitCode (eCGDeclare|eCGReference|eCGList);
    }
    out << ")";
    UtString sectionName;
    if (emitUtil::functionSection (this, &sectionName)) {
      out << TAG;
      gCodeGen->emitSection (out, sectionName);
    }
    out << TAG;
    NUBlock::emitCode (context);
  }

  return eCGVoid;
}
