#ifndef __CGFUNCTIONDIVIDER_H_
#define __CGFUNCTIONDIVIDER_H_
/*!
 * Divide a given function up into subfunctions of no longer than the 
 * given number of lines.
 *
 * Used by PLI code generation; GCC was taking about 40 mins. to compile
 * an init function that had 8000 lines.
 *
 * NOTE: delete object to flush final function to output stream.
 */
class CGFunctionDivider
{
public:
  CGFunctionDivider(UtOStream &out,
                    const char *funcName,
                    const char *formalSignature,
                    const char *actualSignature,
                    const char *prefixText="",
                    UInt32 maxLinesPerFunc = 400):
    mOut(out), mFuncName(funcName), mFormalSignature(formalSignature), 
    mActualSignature(actualSignature), mPrefixText (prefixText),
    mMaxLines(maxLinesPerFunc), mLineCount(0), mFuncCount(0)
  {}

  ~CGFunctionDivider()
  {
    // close the currently open subfunction, if any
    if (mLineCount > 0)
    {
      mOut << CRYPT("}\n\n");
    }
    
    // generate a function with the given base function name that calls all 
    // of the subfunctions
    if (mFuncCount) {
      mOut 
        << CRYPT("static void ") << mFuncName << mFormalSignature << "\n"
        << "{\n";
      for (UInt64 i = 0; i < mFuncCount; i++)
      {
        mOut << "  " << mFuncName << i << mActualSignature << ";\n";
      }
      mOut << "}\n";
    }
  }

  // output a line of code to the function
  void writeLine(const char *lineOfCode)
  {
    // if no lines, start a new subfunction
    if (mLineCount == 0)
    {
      mOut 
        << CRYPT("static void ") << mFuncName << mFuncCount << mFormalSignature << "\n"
        << "{\n"
        << mPrefixText;
      ++mFuncCount;
    }

    // output the given line of code
    mOut << "  " << lineOfCode << "\n";
    ++mLineCount;

    // if the line count has reached the maximum, terminate the
    // subfunction.
    if (mLineCount == mMaxLines)
    {
      mOut << "}\n\n";
      mLineCount = 0;
    }
  }

private:
  UtOStream &mOut;
  const char *mFuncName;
  UtString mFormalSignature, mActualSignature, mPrefixText;
  UInt32 mMaxLines, mLineCount;
  UInt64 mFuncCount;
};

/*! A generated file wrapper that outputs include lines and encloses the 
 *  content in the namespace for the top level design.
 *
 *  Note: delete the object to close the namespace.
 */
class CGClassMethodFile
{
public:
  CGClassMethodFile(const char *fileName, CodeGen *codeGen, const char *includes = "") : mOutFile (0)
  {
    // open output file
    mOutFile = codeGen->getCGOFiles()->CGFileOpen(fileName, "", codeGen->isObfuscating());
    if (not mOutFile)
      return;                   // unable to open file - already error'ed

    // generate #include of header top level file
    codeGen->getCGOFiles()->generateHeaderInclude(*mOutFile, codeGen->getTopModule());

    // generate any additional includes
    *mOutFile << includes;

    // open namespace
    *mOutFile << CRYPT("namespace ") << codeGen->getNamespace() << "\n{\n";
  }

  ~CGClassMethodFile()
  {
    if (not mOutFile)
      return;

    // close namespace and file
    *mOutFile << "}\n";    
    delete mOutFile;
  }

  UtOStream & outStream() { return *mOutFile; }

  // construction error if no file handle...
  bool bad () { return mOutFile == NULL;}

private:
  UtOStream *mOutFile;
};
#endif
