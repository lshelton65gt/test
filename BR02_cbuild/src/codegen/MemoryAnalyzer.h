// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef MEMORY_ANALYSIS_H_
#define MEMORY_ANALYSIS_H_

#include "util/Util.h"
#include "util/CarbonTypes.h"

class ArgProc;
class NUDesign;

//!
/*!
  Determine which memories should be coded with a sparse representation.

  The following two pieces of information are considered:
  
  1. Total bit size of a given memory.

     Do not consider just the number of addresses. The number of bits
     in each row contributes to the space required by a given memory.

  2. Total elaborated capacity.

     Consider the elaborated capacity requirements for all memories in
     the design. Making capacity decisions from a local or an
     unelaborated perspective can miss capacity problems.

  This analysis relies on the -memoryCapacity switch.

  The -memoryCapacity switch specifies the total amount of runtime
  memory (in bytes) allocated to memories. Memories which cause the
  model to require more than this limit will be coded as sparse
  memories.

  A value of 0 will generate only sparse memories. A value of -1 will
  not generate any sparse memories.

  \sa doc/functional-specs/sparse-design.txt
 */
class MemoryAnalyzer
{
public:
  //! Constructor.
  MemoryAnalyzer(SInt32 user_memory_capacity,
                 bool verbose);

  //! Destructor.
  ~MemoryAnalyzer() {}

  //! Analyze the entire design.
  void design(NUDesign * design);

  //! Return the computed memory-bit capacity.
  UInt64 getMemoryBitCapacity() const { return mMemoryBitCapacity; }

  typedef UtMap<UInt64,UInt64> BitSizeToElaborations;
private:

  //! The runtime memory capacity (in bytes) specified with the -memoryCapacity switch.
  SInt32 mMemoryCapacity;

  //! The computed memory-bit capacity.
  UInt64 mMemoryBitCapacity;

  //! Dump a table of our analysis?
  bool mVerbose;
};

#endif
