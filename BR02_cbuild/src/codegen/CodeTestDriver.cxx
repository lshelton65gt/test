// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implement test-driver related code generation.
  Code for generating main.cxx
*/

#include "codegen/codegen.h"
#include "NetMapCompare.h"
#include "TestDriver.h"

#include "compiler_driver/CarbonDBWrite.h"
#include "compiler_driver/CarbonContext.h"
#include "iodb/IODBNucleus.h"
#include "nucleus/NUDesign.h"

typedef UtHashMap<const STSymbolTableNode*, int> AtomIntMap;

class CodeGen::PortOrderSort {
  public:
    PortOrderSort(AtomIntMap* piMap) : mPortIndexMap(piMap) {}
  
    ~PortOrderSort() {}

    bool operator()(const STSymbolTableNode* a, const STSymbolTableNode* b) const {
      int portIndA = -1;
      int portIndB = -1;

      AtomIntMap::iterator p = mPortIndexMap->find(a);
      if(p != mPortIndexMap->end())
        portIndA = p->second;

      AtomIntMap::iterator q = mPortIndexMap->find(b);
      if(q != mPortIndexMap->end())
        portIndB = q->second;
    
      ST_ASSERT(portIndA != -1, a);
      ST_ASSERT(portIndB != -1, b);

      return(portIndA < portIndB);
    }
  
  private:
  AtomIntMap* mPortIndexMap;

  PortOrderSort();
};


NUNet* sResolveNet(const STSymbolTableNode* netNode) {
  NUNet *net = NUNet::find(netNode);

  INFO_ASSERT(net, "Cannot find the net for a primary in the testdriver generator.");

  if(net->isRecordPort()) {
    net = net->getStorage();
    NU_ASSERT(net->isPrimaryPort(), net);
  }

  return net;
}

bool CodeGen::codeTestDriver(UInt32 vector_count, 
                             SInt32 vector_seed,
                             UInt32 reset_length, 
                             UInt32 clock_delay, 
                             const char *vectorfile,
                             bool   debug, 
                             bool   dump_vcd,
                             bool   dump_fsdb, 
                             bool   useIODB, 
                             bool   glitchDetect, 
                             bool   checkpoint,
                             bool   skipObservables) {
  // Module name
  NUModule*          module  = *(mTree->loopTopLevelModules());
  NUNet*             net     = NULL;
  STSymbolTableNode* netNode = NULL;
  UtVector<STSymbolTableNode*> inputs;
  UtVector<STSymbolTableNode*> bidis;
  UtVector<STSymbolTableNode*> outputs;
  NUNetSet           exprBidis;

  TestDriverData td(mIODB, 
                    useIODB, 
                    vector_count, 
                    vector_seed, 
                    reset_length,
                    clock_delay,
                    debug, 
                    dump_vcd, 
                    dump_fsdb, 
                    glitchDetect,
                    getDesign()->getGlobalTimePrecision(),
                    getDesign()->getGlobalTimePrecision(),
                    module->isTimescaleSpecified(), 
                    checkpoint, 
                    skipObservables,
                    getCarbonContext()->getVhdlPortTypeMap());
  td.addModule(module);

  AtomIntMap portIndexMap;
  {
    int j = 0;
    for(IODB::NameVecLoop p = mIODB->loopPrimaryPorts(); ! p.atEnd(); ++p, ++j) {
      STSymbolTableNode* port = *p;
      STAliasedLeafNode* leaf = port->castLeaf();
      ST_ASSERT(leaf != NULL, port);
      portIndexMap[leaf->getMaster()] = j;
    }
  }
  
  for(IODB::NameSetLoop i = mIODB->loopInputs(); ! i.atEnd(); ++i) {
    // Get the master node so we can match it against the primary ports
    netNode = *i;
    STAliasedLeafNode* leaf = netNode->castLeaf();
    ST_ASSERT(leaf != NULL, netNode);
    STAliasedLeafNode* master = leaf->getMaster();

    // If we find a match add it to the inputs
    if((portIndexMap.count(master) != 0)) {
      inputs.push_back(master);
    }
  }

  for(IODB::NameSetLoop i = mIODB->loopOutputs(); ! i.atEnd(); ++i) {
    // Get the master node so we can match it against the primary ports
    netNode = *i;
    STAliasedLeafNode* leaf = netNode->castLeaf();
    ST_ASSERT(leaf != NULL, netNode);
    STAliasedLeafNode* master = leaf->getMaster();

    // If we get a match, add it to the outputs
    if((portIndexMap.count(master) != 0)) {
      outputs.push_back(master);
    }
  }

  CbuildSymTabBOM* bomManager = mIODB->getBOMManager();

  for (IODB::NameSetLoop i = mIODB->loopBidis(); ! i.atEnd(); ++i) {
    // Get the master node so we can match it against the primary ports
    netNode = *i;
    STAliasedLeafNode* leaf = netNode->castLeaf();
    ST_ASSERT(leaf != NULL, netNode);
    STAliasedLeafNode* master = leaf->getMaster();

    // If we get a match add it to the bids
    if((portIndexMap.count(master) != 0)) {
      bidis.push_back(master);
    }

    // Remember the expression mapped nets for later
    if(bomManager->isExprMappedNode(netNode)) {
      net = sResolveNet( netNode );
      exprBidis.insert(net);
    }
  }
  
  // sort the nets by port order
  std::sort(inputs.begin(),  inputs.end(),  PortOrderSort(&portIndexMap));
  std::sort(outputs.begin(), outputs.end(), PortOrderSort(&portIndexMap));
  std::sort(bidis.begin(),   bidis.end(),   PortOrderSort(&portIndexMap));
  
  // now run through the vectors and add them to the testdriver object
  UtVector<STSymbolTableNode*>::iterator j;

  for(j = inputs.begin(); j != inputs.end(); ++j) {
    STSymbolTableNode* node = *j;
    net = sResolveNet(node);

    ResetIter sm = find_if (mSyncResetTable.begin(),  mSyncResetTable.end(),  NetMapCompare(net));
    ResetIter am = find_if (mAsyncResetTable.begin(), mAsyncResetTable.end(), NetMapCompare(net));
    ClockIter cm = find_if (mClockTable.begin(),      mClockTable.end(),      NetMapCompare(net));
    
    if(sm != mSyncResetTable.end()) {
      td.addInput(net);
    }
    else if(am != mAsyncResetTable.end()) {
      td.addReset(net);
    }
    else if(cm != mClockTable.end()) {
      td.addClock(net);
    }
    else {
      td.addInput(net);
    }
  }

  // Sort out outputs.
  for(j = outputs.begin(); j != outputs.end(); ++j) {
    STSymbolTableNode* node = *j;
    net = sResolveNet(node);
    td.addOutput(net);
  }
  
  // Sort out bidirects
  for(j = bidis.begin(); j != bidis.end(); ++j) {
    STSymbolTableNode* node = *j;
    net = sResolveNet(node);

    // If it isn't z then it isn't a bidirect
    if(isBidirect(net) || (exprBidis.find(net) != exprBidis.end()))
      td.addBid(net);
    // It it isn't z and it is written, then it is an output
    else if(net->isWritten())
      td.addOutput(net);
    // must be an input
    else
      td.addInput(net);
  }
  
  // Add Depositables
  STSymbolTable* symtab = mCarbonContext->getSymbolTable();

  for(IODB::NameSetLoop i = mIODB->loopDeposit(); !i.atEnd(); ++i) {
    // Prune the primary ports, we don't need to treat them as a
    // special type of deposit.
    STSymbolTableNode* node     = *i;
    NUNetElab*         netElab  = mIODB->getNet(node);
    NetFlags           netFlags = NUNetElab::getPrimaryNetsAndFlags(symtab, netElab, NULL);

    if(!NetIsPortType(netFlags, eInputNet) && !NetIsPortType(netFlags, eBidNet)) {
      td.addDeposit(node);
    }
  }
  
  // Add Observables
  for (IODB::NameSetLoop i = mIODB->loopObserved(); !i.atEnd(); ++i) {
    td.addObserved(*i);
  }
  
  if(! td.hasPorts() and ! td.hasDeposits() and ! td.hasObserveds()) {
    getMsgContext()->CGTDriveNoPorts();
    return false;
  }

  // Figure out if resets are high or low.
  UInt32 pos_resets = 0;
  UInt32 neg_resets = 0;

  for (SCHSchedule::AsyncResetLoop p = mSched->loopAsyncResets(); !p.atEnd(); ++p) {
    pos_resets += p.getValue()[eClockPosedge];
    neg_resets += p.getValue()[eClockNegedge];
  }
  td.setResetHigh(pos_resets > neg_resets);

  // Test Vectors
  TestVectorEmitter testvector_emitter(*mMsgContext, &td, mTargetName);
  UtOFStream        vector_file("memory.dat");
  if (vectorfile != NULL) {
    // an explicit file of vectors is provided
    UInt32 nlines = 0;
    testvector_emitter.copy(vectorfile, vector_file, &nlines);
    td.setVectorCount (nlines);
  } else {
    // synthesise the test vectors
    testvector_emitter.emit(vector_file);
  }
  vector_file.close();

  // SystemVerilog
  VerilogTestbenchEmitter verilog_emitter(&td, mTargetName, mMsgContext);
  UtOFStream              verilog_file("main.sv");
  UtOFStream              map_file("main.map");

  verilog_emitter.putMapFile(map_file);
  verilog_emitter.emit(verilog_file);
  verilog_file.close();
  map_file.close();

  // VHDL
  VhdlTestbenchEmitter vhdl_emitter(&td, mTargetName, getCarbonContext()->getVhdlPortTypeMap());
  UtOBStream           vhdl_file("main.vhdl");
  UtOBStream           vhdl_map_file("main.vhdl.map");

  vhdl_emitter.putMapFile(vhdl_map_file);
  vhdl_emitter.emit(vhdl_file);
  vhdl_file.close();
  vhdl_map_file.close();

  // Cxx
  UtOBStream          cxx_file("main.cxx");
  CxxTestbenchEmitter cxx_emitter(&td, mTargetName, mIfaceTag, mTopModule->getName ()->str (), mAtomicCache);

  cxx_emitter.emit(cxx_file);
  cxx_file.close();
  
  // SystemC
  UtOBStream         sc_file("sc_main.cpp");
  SCTestbenchEmitter sc_emitter(&td, mTargetName, sc_file);

  sc_emitter.SCemit();
  sc_file.close();

  return true;
}

/*
  Local Variables:
   mode: c++
   indent-level: 2
   argdecl-indent: 0
   label-offset: -4
   tab-always-indent: nil
   continued-statement-offset: 4
   End:
*/
