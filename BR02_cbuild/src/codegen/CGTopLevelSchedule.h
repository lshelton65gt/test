// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implement top-level schedule code-generation
*/

#include "util/UtHashMap.h"
#include "codegen/codegen.h"
#include "langcpp/LangCppCarbonGlobal.h"
#include "langcpp/LangCppFactory.h"
#include "CGTriggerFactory.h"

/* print reset guards */
struct ResetGuards{
  SCHSchedule *sched;
  const SCHScheduleMask *mask;
  CodeGen::NetToIntMap& clockTable;
  const NUNetElab* mClk;
  ResetGuards (const NUNetElab* clk, SCHSchedule* s, const SCHScheduleMask* m,
	       CodeGen::NetToIntMap& ct)
    : sched (s), mask (m), clockTable(ct), mClk(clk) {}
};

/* print extra conditions */
struct ExtraConds
{
  SCHSchedule* mSched;
  CodeGen::NetToIntMap& mClockTable;
  const NUNetElab* mClk;
  const SCHScheduleMask* mMask;
  bool mPosedge;
  ExtraConds (SCHSchedule* sched, CodeGen::NetToIntMap& clockTable,
              const NUNetElab* clk, const SCHScheduleMask* mask,
              bool posedge) :
    mSched(sched), mClockTable(clockTable), mClk(clk), mMask(mask),
    mPosedge(posedge)
  {}
};


//! Helper object to code the top level schedule to the stmt tree
class CGTopLevelSchedule
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CGTopLevelSchedule(CodeGen* codegen, int top_level_sched);
  //! destructor
  ~CGTopLevelSchedule();
  
  //! Call this before emitting the schedule to initialize data
  /*!
    The clocks and primary inputs are calculated here. That
    information is used later to determine change array indices and
    the size of the change array.
  */
  void analyzeTopLevelSchedule();

  //! Generate the glitch test
  /*!
    If glitch detection is on due to -g then this does nothing, so we
    will run the glitch test unconditionally.
    
    Otherwise, this generates 
    \code
    if (hdl->checkClockGlitches())
    \endcode

    \param scope Scope to add the glitch test to.
  */
  LangCppScope* codeGlitchTest(LangCppScope* scope);
  
  //! Check if a derived clock caused a glitch
  /*!
    This generates
    \code
    if ((changed[index] & CARBON_CHANGE_GLITCH_MASK) ==
    CARBON_CHANGE_GLITCH_MASK) {
      hdl->SHLClockGlitch(clkIndex, dclk == 0, dclk);
    }
    \endcode
  */
  void codeDerivedClockGlitchDetect(UInt32 clkIndex, 
                                    const NUNetElab* derivedClk, 
                                    LangCppScope* glitchScope);

  //! Create a DynBitVector of a given size
  /*!
    This is used for checking primary clock races.
    Creates DynBitVector varName(bvSize)
  */
  LangCppVariable* createDynBVVar(const char* varName, UInt32 bvSize, LangCppScope* scope);

  //! Set a bit in a DynBitVector if a dci changed
  /*!
    \param clkElab The derived clock that may have changed
    \param curChanges The DynBitVector
    \param index The DynBitVector index
    \param scope Current cpp scope
    \code
    if (changed[clkElabIndex])
    {
      curChanges.set(index);
    }
    \endcode
  */
  void codeSetDynBVIfDciChanged(const NUNetElab* clkElab, LangCppVariable* curChanges, SInt32 index, LangCppScope* scope);

  //! Call hdl->checkPrimaryClockRace(curChanges)
  void codeCheckPrimaryClkRace(LangCppScope* scope, LangCppVariable* curChanges);

  //! Call hdl->clearPrimaryClockChanges()
  void codeClearPrimaryClkChanges(LangCppScope* scope);

  //! Save value of a derived clock to oldclock_dciIndex
  /*!
    \param prefix "dcl_" or ""
    \param derivedClk Derived clock's value being saved
    \param scope current scope

    \code 
    bool oldClock_dci_ = derivedClk;
    \endcode
   */
  void codeSaveDclValue(const NUNetElab* derivedClk, LangCppScope* scope, const char* prefix);

  //! Call the input/async schedule
  /*!
    \code
    if (updateInputs) {
      carbon_design_data(hdl);
    }
    \endcode
  */
  void codeInputFlowCondRunData(LangCppScope* scope);
  
  //! Code a ResetGuard to the current scope
  /*!
    This creates an expression like (e.g., reset is posedge)
    (reset == 0) & (changed[index] == 0) ...
  */
  LangCppScope* codeResetGuardToScope(const ResetGuards& rg, 
                                      LangCppScope* cppScope);

  //! Code Edge and ExtraConds to current scope
  /*!
    Creates something like the following
    if (((changed[foundClockIndex] & CARBON_CHANGE_RISE_MASK) || 
    (reset && (extraEdges))))
  */
  LangCppScope* codeEdgeNetAndExtraEdgesToScope(const char* varPrefix, 
                                                const NUNetElab* clk, 
                                                SInt32 foundClockIndex, 
                                                const SCHScheduleMask* extraEdges, 
                                                bool isPosedge, 
                                                LangCppScope* cppScope);
  
  //! Used by CodeGen::SyncOnClock.
  LangCppScope* codeChangedRefsToScope(CodeGen::UInt32Set& sensed, LangCppScope* cppScope);

  //! Code a derived clock call trigger
  /*!
    This creates a trigger based on the dcl mask and dcl input nets.
    The actual triggers are based on the 'changed' variable, but that
    is translated to 'dcl_changed'. This makes it easier to keep a map
    of masks to triggers.

    This end results is an if statement with the condition to call a
    dcl schedule.
  */
  LangCppScope* codeDclTriggerToScope(SCHDerivedClockLogic* dcl,
                                      GenSched* derivedSched, 
                                      const char* varPrefix, 
                                      LangCppScope* cppScope);

  //! Update the changed array based on old and new vals of derived clks
  /*!
    dcl_change[index] = 
      or 
    change[index] |=
       doInit | (oldclock ^ newclock) << newclock;
  */
  void codeDerivedChangedAssign(const char* writeVarPrefix, 
                                const char* readVarPrefix,
                                SInt32 derivedClkIndex, 
                                bool orChange,
                                const char* netElabName, 
                                LangCppScope* cppScope);

  //! Declare any needed vars for dcl cycles in this scope
  void declareDclCycleVars(LangCppScope* cppScope);

  //! copy change array to dcl_change array 
  /*!
    memcpy(dcl_changed, changed)
  */
  void copyChangeToDclChange(LangCppScope* cppScope);
  
  //! changed[index] = dcl_changed[index]
  void copyDclChangeIndexToChangeIndex(LangCppScope* cppScope, SInt32 index);

  //! memset(dcl_changed, CARBON_CHANGE_NONE_MASK)
  void clearDclChanged(LangCppScope* scope);

  //! [dcl_]changed[index] = CARBON_CHANGE_NONE_MASK
  /*! If the dcl flag is set, it emits code to clear the dcl_changed
   *  entry, otherwise it emits code to clear the changed entry.
   */
  void clearChangedFlag(SInt32 changeIndex, LangCppScope* scope, bool dcl);
  
  //! settled = 1; dcl_do_init = 0;
  void codeSetDclSettledFlags(LangCppScope* scope);

  //! Save oldnet values
  /*!
    \code
    netType oldnet_netIndex;
    oldnet_netIndex = netElab;
    \endcode
  */
  void saveNetValue(const char* netType, UInt32 netIndex, const NUNetElab* netElab, 
                    LangCppScope* scope);

  //! settled &= (oldnet_netIndex == netElab);
  void updateSettledWithOldNetChk(SInt32 netIndex, const NUNetElab* netElab, 
                                  LangCppScope* scope);

  //! Initialize loop for a dcl cycle
  /*!
    This used to be a while loop in the original codegen. I changed it
    to a for loop as they tend to be more useful.

    \code
    settled = 0;
    dcl_do_init = do_init;
    for (SInt32 iterations = 0; (iterations < 32) & (settled == 0);
    ++iterations) {
    \endcode
  */
  LangCppScope* initDclCycleLoop(LangCppScope* cppScope);

  //! changed[index] |= dcl_changed[index];
  void updateChangedWithDclChanged(LangCppScope* cppScope, SInt32 index);

  // if (dcl_changed[index]) settled = 0;
  void clearSettledIfDclChanged(LangCppScope* cppScope, SInt32 index);
  
  //! Emit the top-level schedule into schedSrc
  /*!
    This emits the top-level schedule into an IR (LangCppStmtTree),
    and then emits that to the UtOStream.
  */
  void emit(UtOStream& schedSrc);

  //! run the deposit_combo_schedule
  /*!
    if (hdl->checkAndClearRunDepositComboSchedule())
      hdl->deposit_combo_schedule()
  */
  void codeDepositComboSchedule(LangCppScope* scope);

  //! Code level triggers to scope
  /*!
    This codes a conditional of the or of all the input nets in the
    SCHInputeNet set.
  */
  LangCppScope* codeInputNetTriggerToScope(const SCHInputNets* inputNets, LangCppScope* cppScope, 
                                           LangCppIf** ifScopeMain = NULL );
  
  //! Code edge trigger to scope
  /*!
    This creates an or of all the edges of clocks in the given
    mask.
  */
  LangCppScope* codeEdgeTriggerToScope(UInt32 scheduleCost, const SCHScheduleMask* mask, 
                                       LangCppScope* cppScope);

  //! Get the derived clock count
  UInt32 getDerivedClockCount() const { return mDerivedClockCount; }

private:
  CodeGen* mCodegen;
  SCHSchedule* mSched;
  UInt32 mNumExtParams;
  UInt32 mDerivedClockCount;
  CarbonIdent* mChangedIdent;
  CarbonIdent* mChangeArrayMemberIdent;
  CarbonIdent* mDclChangeArrayIdent;
  CarbonIdent* mTopLevelVarIdent;
  CarbonIdent* mHdlVarIdent;
  CarbonIdent* mHdlMemberIdent;
  CarbonIdent* mDescrVarIdent;
  CarbonIdent* mUpdateInputsIdent;
  CarbonIdent* mTimeChangedIdent;
  CarbonIdent* mDoInitIdent;
  CarbonIdent* mDclDoInitIdent;
  CarbonIdent* mSettledIdent;
  LangCppCarbonGlobal mCppGlobal;
  LangCppFileScope* mCppStmtTree;
  CGNetElabCount* mChangedMap;
  LangCppFactory mLangCppFactory;
  CGSchedTriggerFactory mTriggerFactory;
  int mTopLevelSchedID; // For sample schedule profiling

  typedef UtHashMap<const SCHScheduleMask*, CGScheduleTrigger*> MaskTriggerMap;
  MaskTriggerMap mMaskTriggers;
  
  typedef UtHashMap<const SCHEvent*, CarbonExpr*> EventExprMap;
  EventExprMap mEventTriggers;

  typedef UtHashMap<const SCHInputNets*, CGScheduleTrigger*> InputNetTriggerMap;
  InputNetTriggerMap mSchInputNetTriggers;

  //! Returns true if the trigger cost is less than the schedule cost
  /*!
    If it isn't cost effective, a comment is written to the scope
    saying that the call is unguarded.
  */
  bool isCostEffective(UInt32 trigCost, UInt32 schedCost, LangCppScope* scope) const;
  
  //! Setup the function with common parameters/variables
  LangCppVariable* genStandardFunctionVars(LangCppFunction* dataFunc);

  //! Code the input/async schedule
  void codeDataFunction(UtOStream& out);

  //! hdl->finalizeScheduleCall();
  void codeFinalizeScheduleCall(LangCppScope* postFunc, LangCppVariable *descr);

  //! code the post schedule function.
  void codePostSchedFunction(UtOStream& out);

  //! Code the clock function (derived/sequential/sample/transition)
  void codeClockFunction(UtOStream& out);

  //! Given an input set create a CGScheduleTrigger
  CGScheduleTrigger* createInputNetTrigger(const SCHInputNets* inputNets);

  //! Given a mask create a CGScheduleTrigger
  /*!
    This keeps track of edges of clocks. If both edges of a clock
    appear in the mask, the edge trigger is transformed to a level
    trigger.
  */
  CGScheduleTrigger* createMaskTrigger(const SCHScheduleMask* mask);

  //! Helper function to code the three top-level schedule functions.
  /*!
    Some common stuff that needs to get done at the beginning of each
    top-level function.
  */
  LangCppFunction* codeScheduleCommonPre(LangCppFileScope* fileScope, 
                                         const UtString& functionName,
                                         LangCppType* carbonStatType,
                                         LangCppClassType* carbonModelType,
                                         LangCppClassType* hookupType,
                                         CarbonIdent* modelIdent,
                                         CarbonIdent* timeIdent,
                                         CarbonIdent* modelHookupIdent,
                                         int schedule_bucket);

  //! Helper function to create each of the top-level sched functions
  /*!
    Stuff that needs to get done at the end of each top-level schedule
    functions.
  */
  void codeScheduleCommonPost(LangCppScope* schedFunc,
    LangCppVariable *descr, UtOStream& out);

  //! This generates the 4 top-level functions
  /*!
    Creates
    carbon_design_schedule()
    carbon_design_clkSchedule()
    carbon_design_dataSchedule()
    carbon_design_asyncSchedule()
  */
  void codeScheduleFunction(UtOStream& out);

  //! Create carbon_design_schedule()
  void codeCarbon_Design_Schedule(LangCppFileScope* fileScope, 
                                  LangCppType* carbonStatType,
                                  LangCppClassType* carbonModelType,
                                  LangCppClassType* hookupType,
                                  CarbonIdent* modelIdent,
                                  CarbonIdent* timeIdent,
                                  CarbonIdent* modelHookupIdent,
                                  UtOStream& out);
  
  //! Create carbon_design_clkSchedule()
  void codeCarbon_Design_ClkSchedule(LangCppFileScope* fileScope, 
                                     LangCppType* carbonStatType,
                                     LangCppClassType* carbonModelType,
                                     LangCppClassType* hookupType,
                                     CarbonIdent* modelIdent,
                                     CarbonIdent* timeIdent,
                                     CarbonIdent* modelHookupIdent,
                                     UtOStream& out);
  
  //! Create carbon_design_clkSchedule()
  void codeCarbon_Design_DataSchedule(LangCppFileScope* fileScope, 
                                      LangCppType* carbonStatType,
                                      LangCppClassType* carbonModelType,
                                      LangCppClassType* hookupType,
                                      CarbonIdent* modelIdent,
                                      CarbonIdent* timeIdent,
                                      CarbonIdent* modelHookupIdent,
                                      UtOStream& out);

  //! Create carbon_design_asyncSchedule()
  void codeCarbon_Design_AsyncSchedule(LangCppFileScope* fileScope, 
                                       LangCppType* carbonStatType,
                                       LangCppClassType* carbonModelType,
                                       LangCppClassType* hookupType,
                                       CarbonIdent* modelIdent,
                                       CarbonIdent* timeIdent,
                                       CarbonIdent* modelHookupIdent,
                                       UtOStream& out);

  //! call top.setTime(time)
  /*!
    If timeChanged is not NULL this becomes
    bool timeChanged = top.setTime(time);
    And the variable is passed back to the caller.
  */
  void setSimulationTime(LangCppFunction* schedFunc, CarbonIdent* timeIdent, LangCppVariable** timeChanged);
  
  /*!
   * evaluate list of elaborated nets, assign to slots in changed[] if
   * they're suitable (and not already assigned a slot).
   * \arg l - a loop<T>
   */
  void evalPI(SCHInputNetsCLoop l);

  //! Count the derived clocks
  void calcDerivedClocks();

  //! Count the PIs and put them in the mChanged map
  void calcPrimaryInputs();

  //! Put PIs that do not have chg arr indices in the change array
  /*!
    The change array index for any input that is added here is -1.
  */
  void evalOtherPIs();

  //! Returns whether or not the prefix is a dcl prefix
  static bool sIsDclPrefix(const char* prefix);

  //! Helper class for LangCpp optimizations
  class RegisterAllocator;
  
};
