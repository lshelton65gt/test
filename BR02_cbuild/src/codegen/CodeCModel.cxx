// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implement CModel initializations
*/

#include "compiler_driver/CarbonContext.h"
#include "iodb/IODBNucleus.h"

#include "nucleus/NUDesign.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModelPort.h"
#include "nucleus/NUCModelArgConnection.h"

#include "emitUtil.h"

#include "CGFindInstances.h"
#include "CGFunctionDivider.h"



using namespace emitUtil;

/*! A class to walk the design looking for NUModuleInstances that have
 *  c-models. We get the STBranchNode as we walk down
 */
class CModelWalkCallback : public CGFindInstancesCallback
{
public:
  //! Abstraction for the types of function calls to emit
  enum CallType
  {
    eCMCreate,
    eCMStart,
    eCMRegister,
    eCMModeChange
  };

  //! constructor.
  CModelWalkCallback(STSymbolTable* symTab, CodeGen* codeGen,
                     CGOFiles* cgOFiles,
                     const char* filename,
                     CallType type) :
    CGFindInstancesCallback(symTab, codeGen, cgOFiles), mCallType(type),
    mCGClassMethodFile (filename, codeGen, "#include \"shell/carbon_interface_aux.h\"\n"),
    mOutFile (0), mAnyCModels (false)
  {
    UtOStream &out = mCGClassMethodFile.outStream ();

    // Find the C++ friendly name for the instance of the top-level simulation
    UtString top;
    top << emitUtil::noprefix (codeGen->getTopName ());
    
    UtString fun, formal, actual, memberFml, memberAct;

    formal << "(carbon_simulation& " << top << " UNUSED";
    actual << "(" << top;

    // We're creating multiple  function declarations and function calls here....
    // (1)The top-level class contains cmodel_init, cmodel_create, ... member functions.
    // (2)The generated cmodel_[init|create|...].cxx file contains the definition of the
    //    function and calls the local functions created by CGFunctionDivider, passing
    //    necessary arguments.
    // We also mark the parameters UNUSED and mark the code as targetted to a cold section
    // for infrequent execution.
    switch(mCallType) {
      case eCMCreate:
        fun << "create";
        formal <<")";             // only one argument to create function
        actual << ")";
        memberFml << "(void)";
        memberAct << ")";
        break;

      case eCMStart:
        fun << "init";
        formal << ", void* cmodelData UNUSED)";
        actual << ", cmodelData)";
        memberFml << "(void *cmodelData UNUSED)";
        memberAct << ", cmodelData)";
        break;

      case eCMRegister:
        fun << "register";
        formal << ", carbon_model_descr* descr UNUSED)";
        actual << ", descr)";
        memberFml << "(carbon_model_descr* descr UNUSED)";
        memberAct << ", descr)";
        break;

      case eCMModeChange:
        fun << "modeChange";
        formal << ", CarbonObjectID* descr UNUSED, void*, CarbonVHMMode from UNUSED, CarbonVHMMode to UNUSED)";
        actual << ", descr, NULL, from, to)";
        memberFml << "(CarbonObjectID* descr UNUSED, void*, CarbonVHMMode from UNUSED, CarbonVHMMode to UNUSED)";
        memberAct << ", descr, NULL, from, to)";
        break;

    } // switch

    // First forward declare the local function
    out << "  static void slocal" << formal
        << " CARBON_SECTION(\"carbon_infrequent_code\");\n";

    // Now define the class member function and have it call the local static
    // function!
    UtString className(emitUtil::pclass(codeGen->getTopModule()->getName()));
    out << "  void " << className << "cmodel_" << fun << memberFml << "{\n"
        << "    slocal(*this" << memberAct << ";\n"
        << "  }\n";

    UtString bodyText;
    if (mCallType == eCMStart) {
      bodyText << "    void* hCarbonModel UNUSED = (void*)"<< top << ".mTop.mDescr;\n";
    }

    mOutFile = new CGFunctionDivider (mCGClassMethodFile.outStream (),
                                      "slocal",
                                      formal.c_str (), actual.c_str (),
                                      bodyText.c_str ());

    // Force at least an empty slocal()
    mOutFile->writeLine ("\n");
  }

  //! destructor
  ~CModelWalkCallback()
  {
    delete mOutFile;
  }
  
  //! Get the coded CModel Interface set so we can report design problems.
  const UtSet<NUCModelInterface*>& getInterfaceSeenSet () const {
    return mInterfaceSeenSet;
  }

  //! Were there any CModels encountered?
  bool anyCModels () const { return mAnyCModels; }

protected:
  void handleModule(STBranchNode* branch, NUModule* mod)
  {
    // Create the instance names (the model and original names)
    UtString modelName;
    getCodeGen()->CxxHierPath(&modelName, branch, ".");
    UtString raw_name, cname;

    // we need an escaped name because it's going inside a C string,
    // so we have to double backslashes
    branch->compose(&raw_name, true, true, ".", false);
    StringUtil::escapeCString(raw_name.c_str(), &cname);

    // Go through all the cmodel instances for this module
    UtString nextLine;
    for (NUCModelLoop c = mod->loopCModels(); !c.atEnd(); ++c)
    {
      // Emit the call
      NUCModel* cmodel = *c;

      // Add this model (by interface object) to the set of seen
      // models.
      NUCModelInterface *cModelIF = cmodel->getCModelInterface ();
      mInterfaceSeenSet.insert (cModelIF);

      // Figure out the prefix to get to the c-model so we can call
      // the appropriate function.
      const UtString* modName = cmodel->getName();
      const UtString* origName = cmodel->getOrigName();
      nextLine << modelName << ".m_" << *modName;

      // Call either the create, start, register, modeChange functions
      switch (mCallType) {
	case eCMCreate:
          nextLine << ".create(\"" << cname;
          if (origName != NULL) {
            UtString origCName;
            StringUtil::escapeCString(origName->c_str(), &origCName);
            nextLine << "." << origCName;
          }
          nextLine << "\");";
          mOutFile->writeLine (nextLine.c_str ());
          break;

        case eCMStart:
          // pass model handle to c-model
          nextLine << ".misc(eCarbonCModelID, hCarbonModel);";
          mOutFile->writeLine (nextLine.c_str ());
          nextLine.erase ();

          // tell c-model se are starting
          nextLine << "    " << modelName << ".m_" << *modName
                   << ".misc(eCarbonCModelStart, cmodelData);";
          mOutFile->writeLine (nextLine.c_str ());
	break;

        case eCMRegister:
          nextLine << ".registerCModel(\"" << cname;
          if (origName != NULL) {
            UtString origCName;
            StringUtil::escapeCString(origName->c_str(), &origCName);
            nextLine << "." << origCName;
          }
          nextLine << "\", descr);";
          mOutFile->writeLine (nextLine.c_str ());
          break;

        case eCMModeChange:
          nextLine << ".modeChange(descr, NULL, from, to);";
          mOutFile->writeLine(nextLine.c_str());
          break;

      } // switch

      mAnyCModels = true;
      nextLine.erase ();
    } // for
  } // void handleModule

  // Local data
  CallType mCallType;

  //! Track CModel's we've encountered and initialized.
  UtSet<NUCModelInterface*> mInterfaceSeenSet;
  CGClassMethodFile mCGClassMethodFile;
  CGFunctionDivider* mOutFile;
  bool mAnyCModels;
}; // class CModelWalkCallback : public NUDesignCallback


//! Callback used to discover all NUCModelInterfaces referenced by NUCModelCalls.
class CModelCallDiscoveryCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  //! Constructor.
  CModelCallDiscoveryCallback() {}

  //! Destructor.
  ~CModelCallDiscoveryCallback() {}

  //! By default, walk through everything.
  Status operator()(Phase /*phase*/, NUBase * /*node*/) {
    return eNormal;
  }

  //! Remember the interface referenced by this call.
  Status operator()(Phase phase, NUCModelCall * call) {
    if (phase == ePre) {
      NUCModelInterface * interface = call->getCModelInterface();
      mInterfaceSeenSet.insert(interface);
    }
    return eNormal;
  }

  //! The set of CModels with containing calls in the Nucleus tree.
  const UtSet<NUCModelInterface*>& getInterfaceSeenSet () const {
    return mInterfaceSeenSet;
  }

private:
  UtSet<NUCModelInterface*> mInterfaceSeenSet;
};



/*!
 * Output the code to create the c-models. We run this outside the
 * construct so that we can pass instance information.
 */
void
CodeGen::codeCModelCreate (void)
{

  // Discover all CModel interfaces and generate create calls.
  CModelWalkCallback interface_callback(mIODB->getDesignSymbolTable(), this,
                                        getCGOFiles (),
                                        "cmodel_create.cxx",
                                        CModelWalkCallback::eCMCreate);
  NUDesignWalker interface_walker(interface_callback, false, false);
  interface_walker.design(mTree);

  if (interface_callback.anyCModels ()) {
    // All that lives in schedule.cxx is the top-level call to the
    // design's cmodel creation routine.
    //
    UtOStream& out = gCodeGen->CGOUT ();
    out << "  " << noprefix (mTopName) << ".cmodel_create();\n";
  }

  // Discover which CModel interfaces are referenced by code.
  CModelCallDiscoveryCallback call_callback;
  NUDesignWalker call_walker(call_callback, false);
  call_walker.design(mTree);

  // Now check that we saw every model we described.  If we didn't, this
  // indicates that either the model isn't getting scheduled or that
  // the model's signals are dead-logic eliminated and the model init.
  // goes way.

  const UtSet<NUCModelInterface*>& interfaceCalledSet (call_callback.getInterfaceSeenSet ());
  const UtSet<NUCModelInterface*>& interfaceCodedSet  (interface_callback.getInterfaceSeenSet ());

  for(NUCModelInterfaceLoop loop = mTree->loopCModelInterfaces ();
      not loop.atEnd ();
      ++loop) {
    NUCModelInterface * interface = (*loop);

    {
      bool called = (interfaceCalledSet.find(interface) != interfaceCalledSet.end());
      bool coded  = (interfaceCodedSet.find(interface) != interfaceCodedSet.end());

      if (called and coded) {
        // normal -- we have a cmodel call and its interface.
      } else if (called) {
        // error -- we have a cmodel call but did not code its interface.
        mMsgContext->CGUninitializedCModelCall(interface->getName()->str());
      } else {
        // warn -- we have a cmodel that was never called (it doesn't matter if we have coded the interface or not).
        mMsgContext->CGUnusedCModel(interface->getName()->str());
      }
    }
  }
}

/*!
 * Output the code to initialize the c-models. We run this after
 * construction so that we can pass model information to
 * the user's create routine.
 */
void
CodeGen::codeCModelInitialization (void)
{
  // Walk the design finding c-models that need to be initialized
  CModelWalkCallback callback(mIODB->getDesignSymbolTable(), this,
                              getCGOFiles (),
                              "cmodel_init.cxx",
			      CModelWalkCallback::eCMStart);
  NUDesignWalker walker(callback, false, false);
  walker.design(mTree);

  if (callback.anyCModels ()) {
    // Lastly call the cmodel initialization function
    UtOStream &out = gCodeGen->CGOUT ();
    out << "  " << noprefix (mTopName) << ".cmodel_init (cmodelData);\n";
  }
}

/*!
 * Output the code to register the c-models with the CarbonModel. This
 * is needed for replay to work correctly.
 */
void CodeGen::codeCModelRegistration(void)
{
  // Walk the design finding c-models that need to be initialized
  CModelWalkCallback callback(mIODB->getDesignSymbolTable(), this,
                              getCGOFiles (),
                              "cmodel_register.cxx",
			      CModelWalkCallback::eCMRegister);
  NUDesignWalker walker(callback, false, false);
  walker.design(mTree);

  if (callback.anyCModels ()) {
    // Lastly call the cmodel initialization function
    UtOStream &out = gCodeGen->CGOUT ();
    out << "  " << noprefix (mTopName) << ".cmodel_register(descr);\n";
  }
}

/*!
 * Output the code to modeChange the c-models with the CarbonModel. This
 * is needed for replay to work correctly.
 */
void CodeGen::codeCModelModeChange(void)
{
  // Walk the design finding c-models that need to be initialized
  CModelWalkCallback callback(mIODB->getDesignSymbolTable(), this,
                              getCGOFiles (),
                              "cmodel_modeChange.cxx",
			      CModelWalkCallback::eCMModeChange);
  NUDesignWalker walker(callback, false, false);
  walker.design(mTree);
}
