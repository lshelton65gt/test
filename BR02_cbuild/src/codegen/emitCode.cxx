// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 /*
 * \file
 * emitCode methods for expression.
 */


#include "util/CarbonAssert.h"
#include "util/CbuildMsgContext.h"
#include "util/DynBitVector.h"
#include "reduce/Fold.h"
#include "emitUtil.h"

#define PRINT_NONSYNTH_VERILOG 0

#if PRINT_NONSYNTH_VERILOG
#include <cstdio>
#endif

using namespace emitUtil;



// Operators yielding a boolean result
static bool isBooleanOp (NUOp::OpT op)
{
  switch (op) {
    case NUOp::eBiSLt:
  case NUOp::eBiSLte: 
  case NUOp::eBiSGtr: 
  case NUOp::eBiSGtre:
  case NUOp::eBiULt:  
  case NUOp::eBiULte: 
  case NUOp::eBiUGtr: 
  case NUOp::eBiUGtre:
  case NUOp::eBiEq: 
  case NUOp::eBiNeq:
  case NUOp::eBiTrineq:
  case NUOp::eBiTrieq:
  case NUOp::eBiLogAnd:
  case NUOp::eBiLogOr:
  case NUOp::eUnLogNot:
  case NUOp::eUnRedAnd:
  case NUOp::eUnRedOr:
  case NUOp::eUnRedXor:
    return true;
  default:
    return false;
  }
}

// Check for an expression that can produce a BVref.  These need
// modifyable temporaries if they're the temporary for an expression
//
static bool
NeedsATemp (const NUExpr *e)
{
  UInt32 exprSize = e->getBitSize ();
  if (exprSize <= LLONG_BIT)
    return false;               // yield value, not BVREF

  switch (e->getType ()) {

  case NUExpr::eNUVarselRvalue:
    return isBVref (e);

  case NUExpr::eNUUnaryOp:
  {
    // we don't support -BVref or ~BVref (for example) with operator overloads, so
    // it's crucial that a BitVector or POD temp variable be constructed that can do
    // the right thing with temps.
    const NUUnaryOp *un = dynamic_cast<const NUUnaryOp *>(e);
    
    // Have to partsel something larger to get the bits we want for a result
    return (un->getArg (0)->getBitSize () > exprSize);
  }

  case NUExpr::eNUBinaryOp:
  {
    const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(e);
    return (bop->getArg (0)->getBitSize () > exprSize);
  }

  case NUExpr::eNUTernaryOp:
  {
    const NUTernaryOp* qc = dynamic_cast<const NUTernaryOp*>(e);

    // Get a temp if either of the legs of the ?: need a temp downsizing
    return (qc->getOp () == NUOp::eTeCond)
      && (qc->getArg (1)->getBitSize () > exprSize
          || qc->getArg (2)->getBitSize () > exprSize
          || NeedsATemp (qc->getArg (1)) || NeedsATemp (qc->getArg (2)));
  }

  default:
    return false;
  }
}

//! Determine if precise operands are required for this operator.
static bool needPrecise ( NUOp::OpT op )
{
  switch (op)
  {
    // Tricky operations that we could handle better.
    // Be pessimistic for now and require precise operands.
    //  eBiRshift:               Binary >>
    //  eTeCond:                 Ternary ?:

    case NUOp::eBiPlus:                // Binary +
    case NUOp::eBiMinus:               // Binary -

    case NUOp::eBiBitAnd:              // Binary Bitwise &
    case NUOp::eBiBitOr:               // Binary Bitwise |
    case NUOp::eBiBitXor:              // Binary Bitwise ^
      return false;

    default:
      return true;
  }
}

//! Determine if this operator creates imprecise results.
//! Parameter "e" is used to get the relevent bitlength and may not be of
//! type "op".
static bool makesImprecise ( NUOp::OpT op, const NUExpr *e )
{
  switch (op)
  {
   case NUOp::eUnMinus:               // Unary  -
   case NUOp::eBiLshift:              // Binary <<
   case NUOp::eBiMinus:               // Binary -
   case NUOp::eBiPlus:                // Binary +
   case NUOp::eBiSMult:                // Binary *
   case NUOp::eBiUMult:
     return true;

   case NUOp::eUnBitNeg:
   case NUOp::eUnVhdlNot:
     // Since we now use XOR to do bitwise complements (and let the compiler optimize
     // this when it's 8/16/32, etc.) we no longer produce an imprecise result!
     return true;

  // Special VHDL operators which imprecises the result if the lop is signed
  case NUOp::eBiVhLshiftArith: // Will do >> if shift count is signed and negative
  case NUOp::eBiVhRshiftArith: // "    "  << "    "     "    "   "     "     "
  case NUOp::eBiLshiftArith:    // Verilog <<<
  case NUOp::eBiRshiftArith:    // Verilog >>>
  case NUOp::eBiVhLshift:
  case NUOp::eBiVhRshift:
  case NUOp::eBiRoR:
  case NUOp::eBiRoL:
  case NUOp::eBiVhdlMod:
  case NUOp::eBiSDiv:
    return true;

  case NUOp::eBiRshift:
    return e->isSignedResult ();

  case NUOp::eBiVhExt:          // Conservative
  case NUOp::eUnRound:
    return true;

  case NUOp::eBiUMod:
  case NUOp::eBiUDiv:
  case NUOp::eBiVhZxt:
    return false;

  case NUOp::eBiDExp:
    // This produces a 64 bit real result.  It may be inaccurate, but it's always 'precise'.
    // It will be ROUNDED to an integral result if we aren't using this as part of a larger
    // real-valued expression.
    return false;

  case NUOp::eBiExp:
    return true;
    
    
   default:
     return false;
  }
}


// Is this operand shrinkable to a 32 bit constant?
static bool
isSmallConstant (const NUExpr* op, CGContext_t context, UInt32 size)
{
  UInt64 rhVal;

  return (not NeedsATemp (context) && isBVOverloadSupported (context) && isBVExpr (context)
          && size > LLONG_BIT
          && op->effectiveBitSize () <= LONG_BIT
          && isCTCE (op, &rhVal)
          && rhVal <= UtUINT32_MAX);
}

/*!
 * Generate C++ translation for an operand or a constant value.
 * \a op - the operand expression
 * \a context - context flags to pass in (especially lvalue and BV overload requests)
 * \a size - the desired size (e.g. if we're overloading 32 vs. 64)
 * \a sign - the resulting sign (usually the same as op->isSignedResult(), but if
 *              we short-circuit an expression we may need to wrap the operand in different signage.
 * \a opcode - the operator this is an operand of.
 *
 * \return context flags representing the generated operand (especially eCGImprecise)
 */

static CGContext_t
sGenOpOrConst (const NUExpr* op, CGContext_t context, UInt32 size, bool sign, NUOp::OpT opcode)
{
  UInt64 rhVal;
  
  UtOStream &out = gCodeGen->CGOUT ();
  CopyContext cc (0,0);

  // If we decide to resize this operand, it will alter the expression cost estimate.
  // THAT might cause us to change our mind about inlining the function and that would
  // be a VERY bad thing if we had already costed the function and decided to emit it
  // out-of-line into the function-layout or class body files.
  //
  bool copiedOperand = false;
  CGContext_t rc = eCGPrecise;

  UInt32 opSize = op->getBitSize ();

  if (isSmallConstant (op, context, size)) {
    if (opSize > LLONG_BIT && size <= LLONG_BIT)
      // Bitvector overloading this operand - try to reduce its size
      // to what we need.  For a constant, it's okay to just resize() to
      // get to where we want to be.
    {
      copiedOperand = true;
      NUExpr* new_op = op->copy (cc);
      new_op->resize (size);
      op = new_op;
      context &= ~(eCGBVOverloadSupported | eCGBVExpr);
    }

    NU_ASSERT (sign == op->isSignedResult (), op);
    isCTCE (op, &rhVal);
    emitUtil::emitConstant (out, rhVal, size, context, op->isSignedResult ());

  } else  {

    if (opSize > LLONG_BIT && size <= LLONG_BIT)
      // Bitvector overloading this operand - try to reduce its size
      // to what we need.  For a non-constant, it's messier - and the best way
      // to do this is to first try to partselect and then resize
    {

      ConstantRange r (op->effectiveBitSize ()-1, 0);
      NUExpr* newOp = gCodeGen->getFold ()->createPartsel (op, r, op->getLoc ());
      newOp = gCodeGen->getFold ()->fold (newOp);
      if (newOp->getBitSize () > size)
        // In case partselecting ended up with (A&MASK) with more context bits than
        // the overloaded BV operator can handle, FORCE a partselect
      {
        newOp = new NUVarselRvalue (newOp, ConstantRange (size-1,0), op->getLoc ());
        newOp->resize (size);
      }

      newOp->setSignedResult (sign);
      op = newOp;
      copiedOperand = true;
      opSize = op->getBitSize ();

      // We really want the correct number of bits here without any dirty bits so
      // that the BV overloading works correctly.
      context |= eCGPrecisePrimary;

      // We also disable any passed-in overloading because we resized the expression
      // to be overloaded and don't need to pass this on.
      context &= ~(eCGBVOverloadSupported | eCGBVExpr);
    }

    if ((size != opSize || sign != op->isSignedResult ()) && not isLvalue (context)) {
      // Make sure we have the right number of bits
      out << CBaseTypeFmt (size, eCGDeclare, sign);
    }

    out << TAG << "(";

    // If we've overloaded a bitvector operand to a smaller size, have to
    // extract the 32/64 bit value
    // TODO:  do we still need this code for shifts?  Can we make divide not use it too?
    //
    if (opSize > LLONG_BIT) {
      if ( opcode == NUOp::eBiRshift || opcode == NUOp::eBiLshift
           || opcode == NUOp::eBiRshiftArith || opcode == NUOp::eBiLshiftArith
           || opcode == NUOp::eBiVhRshiftArith || opcode == NUOp::eBiVhLshiftArith
           || opcode == NUOp::eBiSDiv
           || opcode == NUOp::eBiUDiv) {
        // only support shifts and divide by 32 bits or less...
        rc = emitUtil::sGenLValue (op, 0, context);
      } else if(size <= LONG_BIT) {
        rc = emitUtil::sGenLValue (op, 0, context);
      } else if (size <= LLONG_BIT) {
        out << TAG << emitUtil::sGenLLValue (op, 0, context);
      } else {
        rc = op->emitCode (context);
      }
    } else {
      // No special sizing needed - just generate the operand
      rc = op->emitCode (context);

      // If we asked for fewer bits than the size of the POD object, we need to
      // remember that this result was imprecise
      if (size < opSize)
        rc = Imprecise (rc);
    }

    if(!isLvalue (context) && !isPrecise (rc)
       && (needPrecise(opcode)
           // '&' doesn't need a precise result, because it assumes that the
           // excess bits are BEYOND the identically sized operands; but with
           // BV Overloads, we could have excess bits that are smaller than some
           // other bitvector operand in which case we must suppress them
           || copiedOperand)) {
      mask (std::min (size, opSize), "&");
      rc = Precise (rc);
    }
    out << ")";
  }

  if (copiedOperand)
    delete op;

  return rc;
}

// Constants are generated as appropriately-signed quantities and are
// sensitive to C++ rules regarding the size and precision
//
CGContext_t
NUConst::emitCode (CGContext_t context) const
{
  context &= ~eCGDirtyOk;

  UtOStream &out = gCodeGen->CGOUT ();

  if (isReal ()) {
    CarbonReal r;
    getCarbonReal (&r);
    out << TAG << UtIO::exponent << UtIO::setw(20) << UtIO::Precision(17) << r;
    return eCGPrecise;
  }
    
  size_t size=getBitSize ();

  // Check if we've BV overloaded this operand
  if (size > LLONG_BIT && isBVOverloadSupported (context) && isBVExpr (context)) {
    SInt32 s32;
    SInt64 s64;
    if (getL (&s32))
      size = LONG_BIT;          // Represented as SInt32
    else if (getUL ((UInt32*)&s32) && not isSignedResult ())
      size = LONG_BIT;          // Represented as UInt32
    else if (getLL (&s64))
      size = LLONG_BIT;         // SInt64
    else if (getULL ((UInt64*)&s64) && not isSignedResult ())
      size = LLONG_BIT;         // UInt64
  }

  DynBitVector value;
  getSignedValue (&value);
  CGContext_t ret;
  if (size <= LLONG_BIT && not isLvalue (context)) {
    ret = emitConstant (out, value, size, context, isSignedResult ());
  } else {
    gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path);

    if (NeedsATemp (context) && not isLvalue (context)) {
      out << TAG << CBaseTypeFmt (size, eCGVoid, isSignedResult()) << "(";
      ret = emitConstant (out, value, value.size (), eCGVoid, isSignedResult ());
      out << ")";
      ret |= eCGIsATemp;
    } else {
      context &= ~eCGDirtyOk;
      ret = emitConstant (out, value, value.size (), context, isSignedResult ());
    }
  }

  return ret;
}


/*!
 * Handle BitVector<> temporary copy-constructor logic.
 *
 * Because of the way boost works, it's necessary to make sure that
 * one operand of any BVref \<op\> {BVref|literal} expression is actually
 * a temporary and the OTHER operand should either be a temporary or it
 * should be explicitly "const".
 *
 * This is because operator.h will construct a 2-operand method that
 * returns a result by value, using the non-const operand as a temporary.
 *
 * If the expression doesn't require a temporary, we still parenthesize
 * it, so that expression precedence is handled correctly.
 *
 * \returns eCGIsATemp if one is created.
 */
static CGContext_t
sGenerateTemp (size_t dbits, bool sign, CGContext_t* pcontext)
{
  UtOStream& out = gCodeGen->CGOUT ();

  if (isLvalue (*pcontext)) {
    // op= LHS is its own temporary....
    out << TAG << "(";
    *pcontext &= ~eCGNeedsATemp;
    return eCGIsATemp;
  }

  if (! NeedsATemp (*pcontext))
    /*
     * If our parent doesn't require a temporary, leave it up to the
     * specific expression to decide (anything that calls sGenBinaryOperator
     * is able to figure that out now, things in NUOp::emitCode() that
     * always generate a one-bit-result typically don't need temporaries
     * for their subexpressions or to compute their result anyway.
     */
    {
      out << TAG << "(";

      return eCGVoid;
    }

  // Copy-construct an object of the appropriate size initialized to
  // the expression we compute.
  //
  out << TAG << CBaseTypeFmt (dbits, eCGDeclare, sign) << "(";

  // Disable the request for a temporary from the subtree
  *pcontext &= ~eCGNeedsATemp;

  // And return a flag to indicate that we created a temp.
  // 
  return eCGIsATemp;
}


// Is this operator a truely symmetric binary operator?  E.g. it's not one of the
// shifts or sign extends.  But it can be a divide or mod.
//
// Use this to identify operators where the signed operation is selected based on
// the sign of both operands or to identify mixed mode arithmetic that requires
// casting to deal with the BitVector overloads
//
static bool sIsSymmetric (NUOp::OpT op)
{
  if (op > NUOp::eBiEnd || op < NUOp::eBiStart)
    return false;

  switch (op) {
  case NUOp::eBiRoR:
  case NUOp::eBiRoL:
  case NUOp::eBiExp:
  case NUOp::eBiDExp:
  case NUOp::eBiVhExt:
  case NUOp::eBiVhZxt:
  case NUOp::eBiVhLshift:
  case NUOp::eBiVhRshift:
  case NUOp::eBiDownTo:
  case NUOp::eBiRshiftArith:
  case NUOp::eBiLshiftArith:
  case NUOp::eBiVhRshiftArith:
  case NUOp::eBiVhLshiftArith:
  case NUOp::eBiLshift:
  case NUOp::eBiRshift:
    return false;
  default:
    return true;
  }
}

//! Function to determine if eCGDirtyOk flag should be
/*! set, currently used for pattern matching on
 *  bit optimization results --
 *         (x shiftop c1) & c2 
 *  where c1 and c2 are constants, 
 *  x is of type IdentRvalue, MemselRvalue or VarselRvalue.
 *  shiftop is either eBiLshift or eBiRshift for now.
 */
bool 
sIsDirtyOk(const NUBinaryOp * expr)
{
  UInt64 rhVal;

  if (! isCTCE(expr->getArg(1), &rhVal))
    return false;

  const NUExpr* lop = expr->getArg(0);
  UInt32 lsize = lop->determineBitSize();

  switch (lop->getType())
    {
    case NUExpr::eNUBinaryOp:
      {
      const NUBinaryOp *bi = dynamic_cast<const NUBinaryOp *>(lop);
      if ((bi->getOp () == NUOp::eBiLshift) ||
	  (bi->getOp () == NUOp::eBiRshift))
	{
	  UInt64 sc;
	  if (! isCTCE( bi->getArg (1), &sc))
	    return false;
	  // allows only one level of dirty value
          NUExpr::Type etype = bi->getArg (0)->getType ();
	  if (etype != NUExpr::eNUIdentRvalue
              && etype != NUExpr::eNUMemselRvalue
              && etype != NUExpr::eNUVarselRvalue)
	    return false;
	}
      // only do it for left and right shifts
      else return false;
      break;
      }
    case NUExpr::eNUIdentRvalue:
    case NUExpr::eNUVarselRvalue:
    case NUExpr::eNUMemselRvalue:
      break;
    default:
      return false;
    }
  return (rhVal <= (((UInt64(1))<< lsize ) - 1) );
} // sIsDirtyOk


// Deal with mixed-mode arithmetic.  Verilog SHOULD normally obviate the need for this; VHDL
// allows mixed mode arithmetic.  We only care about it because we haven't implemented full
// SBitVector x BitVector operations and mixed partselects, etc.
//
void sFixOperandSigns (const NUExpr* lop, NUOp::OpT op, const NUExpr* rop,
                       bool* lopSign, bool* ropSign)
{
  bool ls = lop->isSignedResult ();
  bool rs = rop->isSignedResult ();


  if ((ls ^ rs) and sIsSymmetric (op)) {
    if (ls && lop->castConst () && rop->getBitSize () > LLONG_BIT)
      ls = not ls;
    else if (rs && rop->castConst () && lop->getBitSize () > LLONG_BIT)
      rs = not rs;
    else
      rs = ls;
  }


  switch (op) {
  case NUOp::eBiSMult:
  case NUOp::eBiSDiv:
  case NUOp::eBiSMod:
  case NUOp::eBiSLt:
  case NUOp::eBiSLte:
  case NUOp::eBiSGtr:
  case NUOp::eBiSGtre:
    ls = rs = true;
    break;

  case NUOp::eBiUMult:
  case NUOp::eBiUDiv:
  case NUOp::eBiUMod:
  case NUOp::eBiULt:
  case NUOp::eBiULte:
  case NUOp::eBiUGtr:
  case NUOp::eBiUGtre:
    ls = false;
    // fall thru

  case NUOp::eBiRshift:
  case NUOp::eBiLshift:
  case NUOp::eBiRshiftArith:
  case NUOp::eBiLshiftArith:
    rs = false;
    break;

  default:
    break;
  }

  *lopSign = ls;
  *ropSign = rs;
}


// Convert OpT into a printable string.  This may be an op= form.
// Inputs:
// \a opc - the operator
// \a base - the tree we're generating code for
// \a context - check for eCGOpEqual, implying special operator forms.
// \return operator as a string (suffix for unary operators!)
// 
static const char * printOpStr (NUOp::OpT opc, const NUBase* base, CGContext_t context)
{
  if (isOpEqual (context))
    switch(opc)
    {
    case NUOp::eBiSMult:	return "*=";
    case NUOp::eBiUMult:	return "*=";
    case NUOp::eBiPlus:		return "+=";
    case NUOp::eBiBitAnd:	return "&=";
    case NUOp::eBiBitOr:	return "|=";
    case NUOp::eBiBitXor:	return "^=";
    case NUOp::eBiSDiv:		return "/=";
    case NUOp::eBiUDiv:		return "/=";
    case NUOp::eBiSMod:		return "%=";
    case NUOp::eBiUMod:		return "%=";
    case NUOp::eBiMinus:	return "-=";
    case NUOp::eBiRshiftArith:
    case NUOp::eBiRshift:	return ">>=";
    case NUOp::eBiLshiftArith:
    case NUOp::eBiLshift:	return "<<=";
    case NUOp::eUnMinus:        return ".negate()";
    case NUOp::eUnBitNeg:	return ".flip()";
    case NUOp::eUnVhdlNot:	return ".flip()";
    default:
      NU_ASSERT (!"No op= form supported", base );
      return "=";
    }

  // Just a vanilla operator.
  return NUOp::convertOpToChar (opc, true); // return as C-style operators.
}


// Shifts in verilog are so different from other binary operators that we
// are special-casing them distinct from the handling of more symmetric operators.
//
static CGContext_t
sGenShift (const NUExpr *lop, NUOp::OpT op, const NUExpr *rop,
           size_t dbits, bool resultSign, CGContext_t context)
{
  CGContext_t lc, rc = eCGVoid;
  UtOStream &out = gCodeGen->CGOUT ();

  const char* lPrefix = "(";
  const char* infix = printOpStr (op, lop, context);
  UtString rSuffix(")");

  bool doOpEqual = isOpEqual (context);
  context &= ~eCGOpEqual;

  // Determine what size computation we need.  Basically if the
  // destination is larger than the sources, we may need to coerce the
  // LOP or ROP to get the right computation.
  //
  size_t lbits = lop->getBitSize (); // Compute sizes we need
  size_t rbits = rop->getBitSize ();

  bool lopSign, ropSign;
  sFixOperandSigns (lop, op, rop, &lopSign, &ropSign);

  size_t parens = 0;		// Count of extra close parens needed

  switch (op)
    {
    case NUOp::eBiRoR:
      if (dbits > LLONG_BIT)
      {
        infix = ".rotateRight(";
        rSuffix += ")";
      }
      else 
      {
        rSuffix.clear();
        lPrefix = "carbon_ror(";

        infix = ",";
        rSuffix << "," << lbits << ")";
      }
      break;

    case NUOp::eBiRoL:
      if (dbits > LLONG_BIT)
      {
        infix = ".rotateLeft(";
        rSuffix += ")";
      }
      else 
      {
        rSuffix.clear();
        lPrefix = "carbon_rol(";

        infix = ",";
        rSuffix << "," << lbits << ")";
      }
      break;

    case NUOp::eBiVhLshiftArith:
      if (dbits > LLONG_BIT)
      {
        infix = ".shiftLeftArith(";
        rSuffix += ")";
      }
      else 
      {
        rSuffix.clear();
        lPrefix = "carbon_sla(";

        infix = ",";
        rSuffix << "," << lbits << ")";
      }
      break;

    case NUOp::eBiVhRshiftArith:
      if (dbits > LLONG_BIT)
      {
        infix = ".shiftRightArith((SInt32)";
        rSuffix += ")";
      }
      else 
      {
        rSuffix.clear();
        lPrefix = "carbon_sra(";

        infix = ",";
        rSuffix << "," << lbits << ")";
      }
      break;
    case NUOp::eBiVhLshift:
      if (dbits > LLONG_BIT)
      {
        infix = ".vhdlShiftLeftLogical(";
        rSuffix += ")";
      }
      else 
      {
        rSuffix.clear();
        lPrefix = "carbon_sll(";

        infix = ",";
        rSuffix << "," << lbits << ")";
      }
      break;

    case NUOp::eBiVhRshift:
      if (dbits > LLONG_BIT)
      {
        infix = ".vhdlShiftRightLogical(";
        rSuffix += ")";
      }
      else 
      {
        rSuffix.clear();
        lPrefix = "carbon_srl(";

        infix = ",";
        rSuffix << "," << lbits << ")";
      }
      break;

    case NUOp::eBiLshiftArith:
      // fall-thru

    case NUOp::eBiLshift:
      lbits = std::min (lbits, dbits);		// only need enough room so shift is accurate
      break;

    case NUOp::eBiRshiftArith:
      if (dbits > LLONG_BIT) {
        infix = ".shiftRightArith((UInt32)";
        rSuffix += ")";
      } else if (doOpEqual) {
        infix = ">>=";
      } else {
        infix = ">>";           // Override printing '>>>'
      }
      // fall-thru

    case NUOp::eBiRshift:	// Never need to widen
      break;

    default:
      NU_ASSERT ("BiLshift or BiRshift expected" == 0, lop); // Not handling every case
    }


  // We require a bitvector result AND the lhs is not the correct size,
  // we need to coerce here...

  SInt32 exprParens = 0;
  if (dbits > LLONG_BIT && lbits < dbits) {
    lbits = dbits;
    out << TAG << CBaseTypeFmt (lbits, eCGDefine, resultSign) << "("; 
    context &= ~eCGNeedsATemp;
    ++exprParens;
  } else if (resultSign != lopSign) {
    out << TAG << CBaseTypeFmt (dbits, eCGDefine, resultSign) << "(";
    context &= ~eCGNeedsATemp;
    ++exprParens;
  } else if (dbits > lbits && physicalSize (dbits) != physicalSize (lbits)) {
    // consider  D64 = ( a!=0) << 32
    lbits = dbits;
    out << TAG << CBaseTypeFmt (lbits, eCGDeclare, resultSign) << "(";
    ++exprParens;
  }
  
  // Always cast the result of the C-style shifts to the result type
  // Consider (SA >> SB) which in Verilog performs a LOGICAL
  // right shift, but treats the resulting expression as SIGNED with
  // zeros in the SB high bits.
  //
  if (not doOpEqual
      && (op == NUOp::eBiRshift || op == NUOp::eBiLshift || op == NUOp::eBiRshiftArith
          || op == NUOp::eBiLshiftArith)) {
    out << TAG << CBaseTypeFmt (dbits, eCGDeclare, resultSign) << "(";
    rSuffix << ")";
  }

  out << TAG << lPrefix;
  if (dbits > LLONG_BIT)
    // BitVectors
    {
      CGContext_t subctxt = context;
      context &= ~eCGDirtyOk;

      if (op == NUOp::eBiRshift || op == NUOp::eBiRshiftArith)
	{
	  if (!isDirtyOk(subctxt))
	    subctxt |= eCGPrecisePrimary;
	}
      gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path);

      if (NeedsATemp (lop))
	subctxt |= eCGNeedsATemp;

      SInt32 parens = 0;
      if (lop->getBitSize () < lbits
        || lop->isSignedResult () != lopSign)
	{
	  out << TAG << CBaseTypeFmt (lbits, eCGDeclare, lopSign) << "(";
          ++parens;

	  if (lbits > LLONG_BIT)
	    {
	      subctxt &= ~eCGNeedsATemp; // Don't ask for ANOTHER temp
	      context &= ~eCGNeedsATemp; // No longer have a pending need
	    }
	}

      if (emitUtil::needsSignExtension (op, lop, rop, lbits, 0))
        lc = emitUtil::signExtend (out, lop, subctxt, lbits);
      else
        lc = lop->emitCode (subctxt | (doOpEqual ? eCGLvalue : eCGVoid));

      subctxt &= ~eCGDirtyOk;

      if (NeedsATemp (subctxt) && isATemp (lc))
	// We wanted a temp and we got one!
	{
	  subctxt &= ~eCGNeedsATemp;
	  context &= ~eCGNeedsATemp;		// We no longer need one for the sGenBinaryOperator...
	}

      while (--parens >=0)
	out << ")";

      out << TAG << infix;

      // Also check for ROP widening
      parens=0;
      if (rop->getBitSize () < rbits) {
	out << TAG << CBaseTypeFmt (rbits, eCGDefine) << "(";
        ++parens;
      }

      // Restrict shift count to something we can code
      if (emitUtil::needsSignExtension (op, lop, rop, rbits, 1))
        rc = emitUtil::signExtend (out, rop, subctxt, rbits);
      else
        rc = sGenOpOrConst (rop, subctxt | eCGPrecisePrimary, rbits, ropSign, op);

      while (--parens >= 0)
        out << ")";

      // Assume that the bitvector expression generated a temp
      lc |= eCGIsATemp;
    }
  else
    // Normal C++ expressions
    {
      // we may need to mask an imprecise result
      out << TAG << "(";

      if (not doOpEqual)
        // Since we may need to force a signed or unsigned shift for
        // Verilog, ALWAYS cast the lhs to the expected type - we also
        // may have to mask an imprecise result.
        out << CBaseTypeFmt (lbits, eCGDeclare, lopSign && (op != NUOp::eBiRshift));
      out << "(";

      bool dontMask = isDirtyOk(context)  && not needsPrecisePrimary(context);
      context &= ~eCGDirtyOk;

      if (op == NUOp::eBiRshift || op == NUOp::eBiRshiftArith)
        context |= eCGPrecisePrimary;

      if (emitUtil::needsSignExtension (op, lop, rop, lbits, 0)) {
        lc = emitUtil::signExtend (out, lop, context, lbits);

        // We might have to widen a smaller signed RHS operand to a wider width before
        // shifting - but we don't want a physically sized value that contains dirty
        // bits - so, while potentially a signed result doesn't have dirty bits, in ths
        // case right shifting a signed value must not extend the sign to a machineword size
        // or we get the wrong answer.  Marking it imprecise will get it masked before
        // doing the shift.
        //
        if (op == NUOp::eBiRshift && needsExtension (lc))
          lc = Imprecise (lc);
      } else {
        lc = lop->emitCode (context | (doOpEqual ? eCGLvalue : eCGVoid));
      }

      if( !isPrecise (lc) && needPrecise(op) && !dontMask)
	mask (lbits, "&");

      out << "))";	// Possible mask

      out << TAG << infix;             // op or op= 

      out << "(";
      // Also check for ROP widening
      if ( !rop->isReal() && ( rop->getBitSize () < rbits ))
        out << CBaseTypeFmt (rbits, eCGDefine);

      rc = sGenOpOrConst (rop, context, rbits, ropSign, op);

      out << ")";              // Close the whole expression

      if (needsExtension (lc))
        context |= eCGNeedsExtension;
    }

  out << rSuffix;

  // Parens from a bitvector expression
  while(exprParens--)
    out << ")";

  if (isATemp (lc))
    context |= eCGIsATemp;

  if (needsExtension (lc)) {
    context |= eCGNeedsExtension;
    context = Imprecise (context);
  }

  // We might have something like
  //   short = (bv >> var);
  // this will be translated to something like:
  //    short = ((var >= bv.size()) ? UInt16(0) : (var >> bv))
  // but the >> is done in bitvector context and we have to extract
  // the correct field, since we can't have resized the right shift
  // so, it needs to tack on a ".value(0,16)" so the ?: returns 16 bits
  // from either leg.
  //
  if (dbits < lbits && lbits > LLONG_BIT) {
    emitUtil::genBVpartsel (out, 0, dbits);
    context = Precise (context);
  } else if(dbits > LLONG_BIT) {
    context = Precise(context);
  } else if( makesImprecise(op, lop) ) {
    // Otherwise check the operator to see if we generate extra bits
    context = Imprecise(context);
  }

  // If this was an op= to a BVref, then the result is the lvalue, and it will
  // be implicitly precise.
  if (doOpEqual && isBVref (lop))
    context = Precise (context);

  // Extra parens from a shift-guard
  while(parens--)
    out << ")";

  return context;
}

/*
 * Code a simple binary operation
 * \arg lop left operand
 * \arg op binary operator
 * \arg rop right operand
 * \arg dbits bit size of destination value
 * \arg resultSign signedness of the resulting expression
 * \arg c context value to pass down to operands
 *
 * \returns CGContext_t precision
 */
static CGContext_t
sGenBinaryOperator (const NUExpr *lop, NUOp::OpT op, const NUExpr *rop,
                    size_t resultBits, bool resultSign, CGContext_t c)
{
  CGContext_t lc, rc;
  UtOStream &out = gCodeGen->CGOUT ();

  UtString lPrefix ("(");
  UtString infix = printOpStr (op, lop, c);

  bool doOpEqual = isOpEqual (c);
  if (doOpEqual)
    c &= ~(eCGOpEqual | eCGNeedsATemp);

  UtString rSuffix(")");

  // Determine what size computation we need.  Basically if the
  // destination is larger than the sources, we may need to coerce the
  // LOP or ROP to get the right computation.
  //
  size_t lbits = lop->getBitSize (); // Compute sizes we need
  size_t rbits = rop->getBitSize ();
  size_t dbits = resultBits;    // modifiable copy

  // If we are doing mixed-size arithmetic involving bitvectors and
  // PODs, cast the POD up to either 32 or 64 bits, no other size
  // overloads exist in the BitVector runtime library.
  //
  lbits = bvSizeToOther (lbits, rbits);
  rbits = bvSizeToOther (rbits, lbits);

  bool lopSign, ropSign;
  sFixOperandSigns (lop, op, rop, &lopSign, &ropSign);

  // Handle situations where we have expressions of consistent signedness
  // that need to end up as a result of a different signedness.
  //
  if (lopSign == ropSign && lopSign != resultSign && not isBooleanOp (op))
    out << TAG << CBaseTypeFmt (dbits, eCGDefine, resultSign);
  else if ((op == NUOp::eBiSMult && not resultSign)
           || (op == NUOp::eBiUMult && resultSign)
           || (op == NUOp::eBiSDiv && not resultSign)
           || (op == NUOp::eBiUDiv && resultSign)
           || (op == NUOp::eBiSMod && not resultSign)
           || (op == NUOp::eBiUMod && resultSign))
    out << TAG << CBaseTypeFmt (dbits, eCGDeclare, resultSign);
    
  bool booleanRelational = false;

  switch (op)
    {
    case NUOp::eBiSLt:           //!< Binary <
    case NUOp::eBiSLte:          //!< Binary <=
    case NUOp::eBiSGtr:          //!< Binary >
    case NUOp::eBiSGtre:         //!< Binary >=
    case NUOp::eBiULt:           //!< Binary <
    case NUOp::eBiULte:          //!< Binary <=
    case NUOp::eBiUGtr:          //!< Binary >
    case NUOp::eBiUGtre:         //!< Binary >=
    case NUOp::eBiEq:            //!< Binary ==
    case NUOp::eBiNeq:           //!< Binary !=
    case NUOp::eBiTrineq:	 //!< Partial support for ===.
    case NUOp::eBiTrieq:	 //!< Treat === and !== like == and !=.
      booleanRelational = true;
      
      // C++ booleans produce a 1 bit result; if we need more we have to be
      // explicit about casting for it, ala
      //     BitVector<120>( (leftop) != (rightop))
      //
      if (dbits > 1) {
        lPrefix.clear ();
        lPrefix << CBaseTypeFmt (dbits, eCGDeclare, resultSign) << "((";
        rSuffix << ")";
      }

      lbits = rbits = std::max (lbits, rbits);
      // Because of C/C++ widening rules, expressions like
      //    UInt1(x) == UInt8(y) & 0x1
      // get widened incompatibly.  The "usual conversions" for the left
      // change the 'unsigned char' into 'int', while the right side is already
      // an 'unsigned int'.  This doesn't really affect any accuracy results, because
      // the shorter operand really contains an unsigned value.  BUT, gcc generates
      // warnings about comparing signed and unsigned operands.
      //
      // Just wrap both operands in their own type - if it's not compatible THEN
      // we want a slap on the wrist from the compiler.  We only need to do this when
      // the operands are already smaller than 32 bits - otherwise, there are no pesky
      // intermediates that end up as 'int'.
      //
      if (lbits < LONG_BIT) {
        lPrefix << CBaseTypeFmt (lop, lopSign, eCGStaticCast) << "(";
        infix.insert (0, ")");
      }

      if (rbits < LONG_BIT) {
        infix << CBaseTypeFmt (rop, ropSign, eCGStaticCast) << "(";
        rSuffix << ")";
      }
      break;

    case NUOp::eBiPlus:
    case NUOp::eBiMinus:
      lbits = std::max (lbits, dbits);
      rbits = std::max (rbits, dbits);
      break;

    case NUOp::eBiSMult:
    case NUOp::eBiUMult:

      // Widen 32x32 if 64 bit result desired.
      if ((lbits < dbits || rbits < dbits)
	  && (dbits <= LLONG_BIT))
	lbits = rbits = dbits;
      else if (dbits > LLONG_BIT
               && lbits <= LLONG_BIT
               && rbits <= LLONG_BIT)
        // Handle 33 x 33 => 66 by widening one operand to result size
      {
        if (lbits >= rbits)
          lbits = dbits;
        else
          rbits = dbits;
      }
      break;

    case NUOp::eBiDExp:
      // floating point result
      if (lbits <= LLONG_BIT) {
        lPrefix = "std::pow ((CarbonReal)";
        infix = ",(CarbonReal)";
      } else {
        lPrefix.clear ();
        infix = ".pow(";
        rSuffix = ")";
      }
      break;

    case NUOp::eBiExp:
      // simple unsigned base and exponent
      lPrefix.clear ();
      lPrefix << CBaseTypeFmt (dbits, eCGDefine)
              << " carbon_pow (";
      infix = ",(int)";
      rSuffix = ")";
      break;

    case NUOp::eBiSDiv:
    case NUOp::eBiSMod:      
    case NUOp::eBiUDiv:
    case NUOp::eBiUMod:      
      if (lbits > LLONG_BIT || rbits > LLONG_BIT)
      {
        gCodeGen->getMsgContext ()->NonSynthesizedOp (&(rop->getLoc ()),
                                                      NUOp::convertOpToChar (op));
#if PRINT_NONSYNTH_VERILOG
        printf("LHS -> ");
        lop->printVerilog(true);
        printf("RHS -> ");
        rop->printVerilog(true);
#endif
      }
      break;

    case NUOp::eBiBitAnd:       //!< Binary Bitwise &
    case NUOp::eBiBitOr:        //!< Binary Bitwise |
    case NUOp::eBiBitXor:       //!< Binary Bitwise ^
      // All these compute the dest size
      lbits = rbits = dbits;
      break;

    case NUOp::eBiVhdlMod:
      rSuffix.clear();
      lPrefix = "carbon_vhmod(";

      // lbits > LLONG_BIT should already have issued an error.
      infix = ",";
      rSuffix << "," << lbits << ")";
      break;


    case NUOp::eBiVhZxt:
    case NUOp::eBiVhExt:
    {
      rSuffix.clear ();
      lPrefix.clear ();
      infix.clear ();

      bool signExt = (op == NUOp::eBiVhExt); // sign vs zero extend?

      if ( (lbits > LLONG_BIT)) {
        // Large source and small or large destination

        infix << ".vhdlExtension(" << lbits << ", ";
        rSuffix << ", " << signExt << ")";

        // If the type of the lop is different from the result, we need to
        // coerce the operand to the result type before extending
        //
        if (dbits > LLONG_BIT && (dbits != lbits || lopSign != resultSign)) {
          lPrefix << CBaseTypeFmt (dbits, eCGDeclare, resultSign) << "(";
          infix.insert (0, ")");
          
        } else if (dbits <= LLONG_BIT) {
          lPrefix << CBaseTypeFmt (dbits, eCGDefine, resultSign);
        }
      } else if (dbits > LLONG_BIT && lbits <= LLONG_BIT) {
        // small source and large destination.  Extend the source to a
        // bitvector size and then extend that appropriately. Generating
        // code that looks like:
        //
        //  BitVector<dbits,resultSign>(lop).vhdlExtension(lbits, rop, signExt)
        // 
        lPrefix << CBaseTypeFmt (dbits, eCGDeclare, resultSign) << "(";
        infix << ").vhdlExtension(" << lbits << ", ";
        rSuffix << ", " << signExt << ")";

      } else {
        // Small source and small destination

        // Might need a sign-extended operand treated as unsigned or vice-versa
        if (resultSign != signExt || dbits > LLONG_BIT)
          lPrefix << CBaseTypeFmt (dbits, eCGDefine, resultSign);

          
        // TODO: if the second argument to VhExt is a constant, we
        // should be able to use Extender<> - which I think is a
        // lighter weight approach to sign/zero extension.  than what
        // carbon_vhext<> goes thru.  But emitUtil::signExtend()
        // doesn't do the right thing if e is a simple identifier,
        // etc.
        // 
        // Construct:
        //  carbon_vhext<NEWTYPE,OLDTYPE>(expr, resultwidth)
        //

        lPrefix << "carbon_vhext<";

        // figure the return type and input types.
        lPrefix << CBaseTypeFmt (dbits, eCGDeclare, signExt)
                << ","
                << CBaseTypeFmt (lbits, eCGDeclare, lop->isSignedResult ())
                << ">(";

        infix << ",";
        rSuffix << ")";
      }
    }
    break;

    default:
      NU_ASSERT ("Unexpected binary op" == 0, lop);  // Not handling every case
    }
  
  out << lPrefix;
  if (dbits > LLONG_BIT || lbits > LLONG_BIT || rbits > LLONG_BIT)
    // BitVectors
    {
      CGContext_t subctxt = c;

      gCodeGen->addExtraHeader (gCodeGen->cBitVector_h_path);

      if (not doOpEqual && NeedsATemp (lop))
	subctxt |= eCGNeedsATemp;

      // Look to see if we can use the bitvector operators with 32/64 bit
      // operand overloads.
      UInt32 rSize = rbits, lSize = lbits;

      bool lopBVOverload = isBvOverloaded(op, lop, rop, true, &lSize);
      bool ropBVOverload = isBvOverloaded(op, rop, lop, false, &rSize );

      if (lopBVOverload && ropBVOverload)
        // Only allow the second operand to be overloaded.  Otherwise
        // we may lose precision in the result computed
        lopBVOverload = false;

      if (lopBVOverload)
        lbits = lSize;
      else if (ropBVOverload)
        rbits = rSize;

      if (doOpEqual)
        ;
      else if ((lop->getBitSize () < lbits)
          || (lopSign != lop->isSignedResult ()))
      {
        out << TAG << CBaseTypeFmt (dbits, eCGDeclare, lopSign);
        if (dbits > LLONG_BIT)
        {
          subctxt &= ~eCGNeedsATemp; // Don't ask for ANOTHER temp
          c &= ~eCGNeedsATemp;	// No longer have a pending need
        }
      } else if (lopBVOverload ){
        // Make sure this op is sized to compatible type for OP= forms
        out << TAG << CBaseTypeFmt (lbits, eCGDeclare, lopSign, lop->isReal ());
      }
      
      if (lopBVOverload) {
	subctxt |= (eCGBVOverloadSupported | eCGBVExpr);

        // If we're going to hold this to 32/64 bits, then we don't want
        // a bitvector temp from this operand.
	subctxt &= ~eCGNeedsATemp;
      }

      out << "(";

      if (emitUtil::needsSignExtension (op, lop, rop, lbits, 0))
        lc = emitUtil::signExtend (out, lop, subctxt | (doOpEqual ? eCGLvalue : eCGVoid), lbits);
      else
        lc = sGenOpOrConst (lop, subctxt | (doOpEqual ? eCGLvalue : eCGVoid), lbits, lopSign, op);

      if (doOpEqual)
        lc = Precise (lc);

      if (NeedsATemp (subctxt) && isATemp (lc))
	// We wanted a temp and we got one!
	{
	  subctxt &= ~eCGNeedsATemp;
	  c &= ~eCGNeedsATemp;		// We no longer need one for the sGenBinaryOperator...
	}

      out << ")" << infix;


      // Now start the rop argument code generation
      // 
      if (NeedsATemp (rop) && !isATemp (lc))
	subctxt |= eCGNeedsATemp;

      // Also check for ROP widening - that gives us a good temporary
      if ((rop->getBitSize () < rbits)
          || (ropSign != rop->isSignedResult ())) {
	out << TAG << CBaseTypeFmt (rbits, eCGDeclare, ropSign, rop->isReal ());
        subctxt &= ~eCGNeedsATemp;
      } else if (ropBVOverload)
        // Make sure RHS is sized to compatible type for OP= forms
        out << TAG << CBaseTypeFmt (rbits, eCGDeclare, ropSign, rop->isReal ());

      out << "(";

      // Only do  overload if the lhs (first) operand is not
      // already overloaded. 
      // To be conservative for now, only to get rid of one 
      // constructor even if both operands are small BitVectors,
      // (s0, s1.)
      // BV = BV(s0) op BV(s1);     => BV = s0 op BV(s1);
      // when op is commutative, or => BV = BV(s0) op s1;
      // when op is non-commutative.
      if (ropBVOverload) {
	subctxt |= (eCGBVOverloadSupported | eCGBVExpr);
        subctxt &= ~eCGNeedsATemp;
        
      } else {
        subctxt &= ~(eCGBVOverloadSupported | eCGBVExpr);
      }

      if (emitUtil::needsSignExtension (op, lop, rop, rbits, 1))
        rc = emitUtil::signExtend (out, rop, subctxt, rbits);
      else
        rc = sGenOpOrConst (rop, subctxt, rbits, ropSign, op);

      out << ")";

      // Assume that the bitvector expression generated a temp
      rc |= eCGIsATemp;
    }
  else
    // Normal C++ expressions
    {
      
      // we may need to mask an imprecise result
      int parens=1;
      out << "(";

      // See if we must resize - use a cast to (basetype)
      if ( !lop->isReal() &&
           ( lop->getBitSize () < lbits || // need to grow operand
             ((op != NUOp::eBiDExp) && lopSign and not ropSign))) // zero-extend mixed mode vectors.
        out << TAG << CBaseTypeFmt (lbits, eCGDefine, resultSign);
      
      bool lopIsWholeMemory = lop->isWholeIdentifier () && lop->getWholeIdentifier ()->is2DAnything ();
      bool ropIsWholeMemory = rop->isWholeIdentifier () && rop->getWholeIdentifier ()->is2DAnything ();
      if (booleanRelational && !lopIsWholeMemory) {
        // Screwcase uncovered bug7281. Two dimensional arrays are represented
        // as templated class instances. These will not cast into
        // CarbonUIntN. Only generate the case if lop is not a whole memory
        // access.
        out << TAG << CBaseTypeFmt (lbits, eCGStaticCast, lopSign, lop->isReal ());
      }

      out << "(";
      ++parens;

      if (emitUtil::needsSignExtension (op, lop, rop, lbits, 0))
        lc = emitUtil::signExtend (out, lop, c | (doOpEqual ? eCGLvalue : eCGVoid), lbits);
      else
        lc = sGenOpOrConst (lop, c | (doOpEqual ? eCGLvalue : eCGVoid), lbits, lopSign, op);

      if (lopIsWholeMemory && !ropIsWholeMemory) {
        // The right operand will be an integer type, but the left is a
        // Memory<...>. Coerce the entire memory into the appropriate integer
        // type.
        NU_ASSERT (lbits <= LLONG_BIT, lop);
        out << TAG << ".uncheckedCoerce<" << CBaseTypeFmt (lbits, eCGDeclare, lopSign, lop->isReal ())
          << ", " << lbits << ">()";
      }

      bool dontMask = isDirtyOk(c)  && not needsPrecisePrimary(c);
      if( !isPrecise (lc) && needPrecise(op) && !dontMask )
	mask (lbits, "&");

      out << "))";	// Possible mask
      parens-=2;

      out << TAG << infix;             // op or op=

      if (doOpEqual) {
        out << TAG << CBaseTypeFmt ((dbits > 32)?64:32, eCGDefine, resultSign, rop->isReal ());
        lc = Precise (lc);      // left operand was a precise LVALUE
        
      }

      if (booleanRelational && !ropIsWholeMemory) {
        // Same screwcase fron bug7281 as for lop...
        out << TAG << CBaseTypeFmt (rbits, eCGStaticCast, ropSign, rop->isReal ());
      }

      out << "(";
      ++parens;

      // Also check for ROP widening
      if ( !rop->isReal() &&
           (( rop->getBitSize () < rbits ) ||
            ( (op != NUOp::eBiDExp) && ropSign && not lopSign)))
        out << TAG << CBaseTypeFmt (rbits, eCGDefine, resultSign);

      if ((op == NUOp::eBiSDiv || op == NUOp::eBiSMod
        || op == NUOp::eBiUDiv || op == NUOp::eBiUMod)) {
        //  LOP / nonzero(ROP) - 'nonzero' is an inline template function
        out << "nonzero (";
        ++parens;
      }

      if (emitUtil::needsSignExtension (op, lop, rop, rbits, 1))
        rc = emitUtil::signExtend (out, rop, c, rbits);
      else
        rc = sGenOpOrConst (rop, c, rbits, ropSign, op);

      if (ropIsWholeMemory && !lopIsWholeMemory) {
        // The left operand will be an integer type, but the right is a
        // Memory<...>. Coerce the entire memory into the appropriate integer
        // type.
        NU_ASSERT (rbits <= LLONG_BIT, rop);
        out << TAG << ".uncheckedCoerce<" << CBaseTypeFmt (rbits, eCGDeclare, ropSign, rop->isReal ())
          << ", " << rbits << ">()";
      }

      while(parens--)
        out << ")";

      if (doOpEqual && not isPrecise (rc)) {
        // Clean the RHS operand so we're doing L op= (R&mask)
        mask (rbits, "&");
        rc = Precise (rc);
      }
    }

  out << rSuffix;

  c = lc & rc;

  // Check for binary operator where ONE of the operands is BitVector
  // but the resulting size is non-bitvector.  For the cases that come
  // into sGenBinaryOperator, these operators all produce a sized result (consider
  //   (bv80 & 1022) == 100
  // where the result of the LHS is expected to be 10 bits (because
  // of the and operation, so we need to do an extraction of the result
  //
  if ((lbits > LLONG_BIT || rbits > LLONG_BIT)
      && not booleanRelational
      && resultBits <= LLONG_BIT)
  {
    emitUtil::genBVpartsel (out, 0, dbits);
    c = Precise (c);            // guaranteed precise
  }

  if (isATemp (lc | rc))
    c |= eCGIsATemp;

  // for AND, the result is precise if either lc or rc is precise and NOT sign-extended!.
  if (op == NUOp::eBiBitAnd && not doOpEqual) {
    if ((isPrecise(lc) && not needsExtension (lc))
        || (isPrecise(rc) && not needsExtension (rc)))
      c = Precise(c);
    else
      c = Imprecise (c);
  } else if (booleanRelational)
    c = Precise (c);

  if(dbits > LLONG_BIT)
    // Bitvector results are always presumed to be precise.
    return Precise(c);

  else if (doOpEqual && isBVref (lop) )
    // The lvalue will be precise if we had something like
    //   x = y.lpartsel(13,3) += 2;
    // so there's no need to mask the return bits
    return Precise (c);
  else if( makesImprecise(op, lop))
    // Otherwise check the operator to see if we generate extra bits
    return Imprecise(c);

  return c;
}

// Handle NUChangeDetect unary operator
CGContext_t
NUChangeDetect::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr | eCGDirtyOk);

  UtOStream &out = gCodeGen->CGOUT ();

  // We have a hidden temporary net so, we have to mark it during the
  // mark phase step.
  NUNet* tempNet = getTempNet();
  if (isMarkNets(context)) {
    tempNet->emitCode(eCGMarkNets);
  }

  // Emit: changeDetect(tempNet, expr). The function contains:
  // (expr == tempNet) ? false : ((tempNet = expr), true)
  const NUExpr* expr = getArg(0);
  out << TAG << "changeDetect(";
  tempNet->emitCode(context);
  UInt32 size = expr->getBitSize();
  bool isSigned = expr->isSignedResult();
  out << ", " << TAG << CBaseTypeFmt(size, eCGDefine, isSigned);
  expr->emitCode(context);
  out << ")";
  return Precise (eCGVoid);
}


// Handle NULut operator
//
// Emit: table[expr]
CGContext_t
NULut::emitCode(CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr | eCGDirtyOk);

  UtOStream &out = gCodeGen->CGOUT ();
  out << TAG;

  const NUExpr *expr = getArg(0);

  bool isBitVec = getBitSize() > LLONG_BIT;
  if (isBitVec) {
    // Bit-vectors come in as pointers to avoid creating static constructors,
    // so dereference them
    out << "(*";
  }
  out << member(getTableName()) << "[";

  // The LUT is a zero-origined array, so the indexing expression CANNOT
  // be negative.
  UInt32 size = expr->getBitSize();
  if (size < LONG_BIT || expr->isSignedResult ())
    // if too small or signed, coerce result to unsigned index into table
    out << TAG << CBaseTypeFmt(LONG_BIT, eCGDefine, false);

  // Need a precise (masked) value here because are indexing an array.
  CGContext_t result_ctx = expr->emitCode(context);
  if (!isPrecise(result_ctx)) {
    mask(size, "&");
  }

  out << TAG << "]";

  if (isBitVec) {
    out << ")";                 // match the dereference above
  }
  
  return Precise (eCGVoid);
}


// Handle Unary operators
//
CGContext_t
NUUnaryOp::emitCode (CGContext_t context) const
{
  bool doOpEqual = isOpEqual (context);
  context &= ~(eCGFlow | eCGOpEqual);        // Allow bitvector overload to be passed thru
  NU_ASSERT(!isDirtyOk(context), this);

  UtOStream &out = gCodeGen->CGOUT ();
  out << TAG;
  const NUExpr* uop = getArg (0);
  const UInt32 uBits = uop->getBitSize(); // size of sub-expression
  UInt32 dBits = getBitSize (); // size of result
  CGContext_t expcontext = Precise (context);
  CGContext_t rc = 0;

  context = Precise (context);	// Assume bit accuracy maintained

  bool operandSign = uop->isSignedResult ();
  bool resultSign = isSignedResult ();

  // Check if this expression has an effective bitsize of only one bit, but is being asked to
  // produce a larger result.
  if (getBitSize () > LLONG_BIT && effectiveBitSize () == 1)
  {
    // We might be able to use a 32 bit value instead of constructing the
    // whole bitvector if we're allowed to use a BitVector::operator with a 32-bit
    // overload.
    if (isBVOverloadSupported (context) && isBVExpr (context)
        && not (NeedsATemp (context) || NeedsATemp (uop)))
      dBits = 32;

    out << TAG << CBaseTypeFmt (dBits, eCGDefine, resultSign);
  }

  UtString prefix (""), postfix ("");

  if (doOpEqual)
    postfix = printOpStr (mOp, this, context | eCGOpEqual);
  else
    prefix = printOpStr (mOp, this, context);

  switch (mOp)
    {
    case NUOp::eUnBitNeg:
    case NUOp::eUnVhdlNot:
    case NUOp::eUnMinus:
    case NUOp::eUnLogNot:
    case NUOp::eUnPlus:
    case NUOp::eUnBuf:
    {
      // Note: this used to transform eUnBitNeg and eUnVhdlNot
      // for single-bit operations into eUnLogNot.  However,
      // gcc will generate branches when the "!" operator is
      // used within other expressions.  An example:
      //
      // This:
      //    o = ~a & b;
      // Causes GCC to emit:
      //    movl    8(%ebp), %edx
      //    movzbl    86(%edx), %eax
      //    notb    %al
      //    andb    87(%edx), %al
      //    movb    %al, 84(%edx)
      //
      // This:
      //    o1 = !a & b;
      // Causes GCC to emit:
      //    cmpb    $0, 86(%eax)
      //    leal    85(%eax), %edx
      //    jne    .L5
      //    movzbl    87(%eax), %eax
      //    andb    $1, %al
      //.L6:
      //    movb    %al, (%edx)
      //...pop,return...
      //.L5:
      //    xorl    %eax, %eax
      //    jmp    .L6

      // Are we forcing the promotion of something smaller or with
      // a different sign?
      // C/C++ minus applies integer promotions
      UInt32 closeParens=0;
      if (doOpEqual)
      {
        // Don't cast Lvalue operations!
        ;
      }
      else if ((dBits <= LLONG_BIT && dBits > uBits
                && not isSignedResult ()
                && (mOp == NUOp::eUnMinus || mOp == NUOp::eUnBitNeg)) // C/C++ integral promotion candidate
               || (resultSign != operandSign) // signedness mismatch
               || (dBits > LLONG_BIT && dBits != uBits)) // resizing bitvector
      {
        out << TAG << CBaseTypeFmt (dBits, eCGDeclare, resultSign, isReal ());
        out <<"(";
        ++closeParens;
      } 

      // GCC likes to complain about ~ applied to a unsigned operand that is
      // then possibly promoted to a wider type.  Since we are controlling the
      // size and significance, we want to suppress this.   Fix this by doing
      // an XOR with a mask of the same size - this avoids any problems with
      // widening the bits beyond what the HDL semantics implied ANYWAY.
      //
      // Likewise for ! of a single bit, generate the XOR because it will avoid
      // extra conditional jumps if the ! appears in an expression...
      //
      if (((mOp == NUOp::eUnBitNeg || mOp == NUOp::eUnVhdlNot) && uBits < LLONG_BIT)
        || (mOp == NUOp::eUnLogNot && uBits == 1 && not isFlowValue (context)))
      {
        prefix =  "";
        postfix << "^ MASK(" << CBaseTypeFmt (uBits) << ", " << uBits << ")";
      } 

      // Make sure we enforce operator precedence
      out << "(";

      if (mOp != NUOp::eUnPlus && mOp != NUOp::eUnBuf) {
        out << prefix;
        out << "(";
        ++closeParens;
      }

      if (NeedsATemp (uop)) {
        out << TAG << CBaseTypeFmt (uop, eCGDeclare) << "(";
        ++closeParens;
      }

      rc = uop->emitCode (expcontext);
 
      if(needPrecise(mOp) && !isPrecise(rc) )
      {
        mask (uBits, "&");
        rc = Precise(rc);  // make a precise result
      }
      out << ")";

      out << TAG << postfix;

      for(; closeParens != 0; --closeParens)
      {
        out << ")";
      }

      if (makesImprecise (mOp, uop))
        rc = Imprecise(rc);

      return rc;
    }

    case NUOp::eUnRedAnd:

      {
        context &= ~(eCGBVOverloadSupported | eCGBVExpr);
	rc = sGenerateTemp (dBits, resultSign, &context); // 'T('
	if (operandSign)
	  out << TAG << CBaseTypeFmt (uBits, eCGDefine, operandSign); // Cast LHS to base type

	out << "(";
	CGContext_t lc = getArg(0)->emitCode (context);

	if (uBits > LLONG_BIT)
	  // just count them - that's real fast for BitVectors
	  out << ".count()) == (" << uBits;
	else
	  // Must be less than LLONG_BIT width just check that it's a dense
	  // mask of 1's
	  {
	    if (!isPrecise (lc))
	      mask (uBits, "&");

            UInt32 maskSize = (uBits > LONG_BIT) ? LLONG_BIT : LONG_BIT;

	    out << ") == ";

            if (uBits <= SHORT_BIT || operandSign)
              // Have to cast RHS to same size/sign as LHS to avoid
              // gcc warnings about comparisons between signed/unsigned.
              out << TAG << CBaseTypeFmt (uBits, eCGDefine, operandSign);

            out << "MASK(" << CBaseTypeFmt (maskSize)
		<< "," << uBits;
	  }

	out << "))";
	return Precise (context);
      }

    case NUOp::eUnRedOr:
      {
        context &= ~(eCGBVOverloadSupported | eCGBVExpr);
	rc = sGenerateTemp (dBits, resultSign, &context); // 'T('

	out << TAG << "(";
	CGContext_t c = getArg(0)->emitCode (context);

	if (uBits > LLONG_BIT)
	  out << ".any())";
	else
	  // if the value is natural-sized, just test for non-zero
	  {
	    if( !isPrecise (c) )
	      mask (uBits, "&");

	    out << ") != " << CBaseTypeFmt (uBits, eCGDeclare, operandSign) << "(0)";
	  }

	out << ")";
	return Precise (context);
      }

    case NUOp::eUnCount:		// population count
      {
        context &= ~(eCGBVOverloadSupported | eCGBVExpr);
	if (uBits > LLONG_BIT)
	  {
	    context = uop->emitCode (context);
	    out << ".count()" << TAG;
	  }
	else
	  {
            UInt32 aBits = (uBits > LONG_BIT) ? LLONG_BIT : LONG_BIT;
            out << TAG << "carbon_popcount(" << CBaseTypeFmt (aBits, eCGDefine) << "(";
	    context = uop->emitCode (context);
	    if (!isPrecise (context))
	      mask (uBits, "&");
	    out << "))";
	  }
      }
      // Mark the result precise if its large enough to hold all possible returned
      // counts (largest count is 'uBits' if all set, so a width of log2(uBits)
      // can represent all the values we could count.)
      return (dBits >= DynBitVector::sLog2 (uBits)) ? eCGPrecise : eCGVoid;

    case NUOp::eUnFFO:
    case NUOp::eUnFLO:
    case NUOp::eUnFLZ:
    case NUOp::eUnFFZ:
      context &= ~(eCGBVOverloadSupported | eCGBVExpr);
      if (uBits > LLONG_BIT)
      {
        // A BitVector (or possibly partselect...)
        uop->emitCode (context);
        out << "." << NUOp::convertOpToChar (mOp) << "()" << TAG;
      }
      else
      {
        out << TAG << "carbon_";
        if (uBits > LONG_BIT)
          out << "D";           // Use 64 bit versions of find functions.

        out << NUOp::convertOpToChar (mOp) << "(";
        CGContext_t rc = uop->emitCode (context);
        if (!isPrecise (rc))
          mask (uBits, "&");

        if (mOp == NUOp::eUnFLZ || mOp == NUOp::eUnFFZ)
          // Have to pass size of values so that we can mask complement when
          // finding ones
          out << ", " << uBits;

          
        out << ")";
      }
      // Returns [-1..uBits-1], so meed log2(uBits) + 1 bits
      return (dBits >= DynBitVector::sLog2 (uBits)+1) ? eCGPrecise : eCGVoid;
      break;

    case NUOp::eUnRound:
    {
      NU_ASSERT ( uop->isReal (), this);
      out << TAG << CBaseTypeFmt (dBits, eCGDefine, resultSign);

      out << "carbon_round_real(";
      CGContext_t rc = uop->emitCode (context);
      out << ")";

      if (isPhysicalSize (dBits))
        return Precise (rc);
      else
        return Imprecise (rc);
    }

    case NUOp::eUnChange:
      NU_ASSERT ("Not Yet Implemented" == 0, 
		 this); // Need to fully define intended semantics.
      break;

    case NUOp::eUnRedXor:       //!< Unary Reduction ^
      // This is only hard one, requiring bitcount
      {
        context &= ~(eCGBVOverloadSupported | eCGBVExpr);

	rc = sGenerateTemp (dBits, resultSign, &context); // 'T('

	if (uBits > LLONG_BIT)
	  {
	    context = uop->emitCode (context);
	    out << ".redxor()" << TAG;
	  }
	else
	  {
            if (uBits > LONG_BIT) {
              out << TAG << "carbon_redxord(";
            } else {
              out << TAG << "carbon_redxor(";
            }

	    context = getArg(0)->emitCode (context);

	    if (!isPrecise (context))
	      mask (uBits, "&");

	    out << ")";
	  }

	out << ")";

	return Precise (context);
      }

    case NUOp::eUnAbs:       //!< Absolute value
      {
        CGContext_t subctxt = context;
        if (NeedsATemp (uop))
	  subctxt |= eCGNeedsATemp;

	if (uBits > LLONG_BIT)
	  {
	    context = uop->emitCode (subctxt);
	    out << ".absolute()" << TAG;
            return Precise (context);
	  }
	else
	  {
            out << TAG << "carbon_abs(";

	    context = getArg(0)->emitCode (context);

	    if (!isPrecise (context))
	      mask (uBits, "&");

	    out << "," << uBits << ")";
            return Imprecise (eCGVoid);
	  }
      } 
      break;

    
    case NUOp::eUnRtoI:
    {
      NU_ASSERT ( uop->isReal (), this);
      out << TAG << CBaseTypeFmt (dBits, eCGDefine, resultSign);

      // verilog defines $rtoi as real to integer conversion by truncation,
      // here just make the assignment and c does the truncation.
      CGContext_t rc = uop->emitCode (context);

      return Precise (rc);
    }
    
    case NUOp::eUnItoR:
    {
      UInt32 extraParen = 0;
      NU_ASSERT ( not uop->isReal (), this);
      NU_ASSERT ( dBits == 64, this);
      out << TAG << CBaseTypeFmt (dBits, eCGDefine, true, true);
      if ( uBits > LLONG_BIT ){
        CGContext_t rc = uop->emitCode (context);
        out << ".rvalue()" << TAG;
        return Precise (rc);    // does carbon_real return precise results?
      } else {
        if ( operandSign ){
          out << "(";
          extraParen++;
          out << TAG << CBaseTypeFmt (uBits, eCGDeclare, true, false);
        }
        out << "(";
        CGContext_t rc = uop->emitCode (context);
        if (!isPrecise (rc))
          mask (uBits, "&");
        out << ")";
        while ( extraParen-- )
          out << ")";
        return Precise(rc);
      }
    }
    
    case NUOp::eUnRealtoBits:
    {
      NU_ASSERT ( uop->isReal (), this);
      NU_ASSERT ( dBits == 64, this);
      out << TAG << "carbon_real_to_bits(";
      CGContext_t rc = uop->emitCode (context);
      out << ")";
      return Precise (rc);
    }
    case NUOp::eUnBitstoReal:
    {
      NU_ASSERT ( not uop->isReal (), this);
      NU_ASSERT ( dBits == 64, this);
      out << TAG << "carbon_bits_to_real(";
      CGContext_t rc = uop->emitCode (context);
      out << ")";
      return Precise (rc);
    }


    case NUOp::eInvalid:         //!< Bad operator
      // msgContext->CGNotImpl ("expression operator");
      // break;

    default:
      UtIO::cerr() << "Operator " << mOp << " not implemented\n";
      NU_ASSERT ("Unexpected NUUnaryOp"==0, this);
    }
  return eCGVoid;
}


CGContext_t
NUBinaryOp::emitCode (CGContext_t context) const
{
  if (NeedsATemp (getArg (0)) || NeedsATemp (getArg (1)))
    context |= eCGNeedsATemp;

  
  UtOStream &out = gCodeGen->CGOUT ();
  out << TAG;
  UInt32 lbits = getArg(0)->getBitSize();
  UInt32 rbits = getArg(1)->getBitSize();
  UInt32 dbits = getBitSize ();

  out << "(";

  CGContext_t ret = eCGVoid;

  NUOp::OpT opcode = mOp;

  context = Precise (context);	// Assume bit accuracy maintained

  CGContext_t expcontext = context;
  expcontext &= ~(eCGBVOverloadSupported | eCGBVExpr | eCGFlow | eCGOpEqual);

  bool doOpEqual = isOpEqual (context); // Asking for op= ?

  // We might want to know if the RHS of the expression is constant
  DynBitVector rv (rbits);
  const NUConst *constRv = NULL;
  if (not getArg (1)->isReal ())
  {
    constRv = getArg(1)->castConst();
    if (constRv)
      constRv->getSignedValue (&rv);
  }

  // The binaryop node knows if it produces a signed result without looking at the operands.
  bool signedArithmetic = isSignedResult ();

  switch (opcode)
  {
  case NUOp::eBiSLt:           //!< Binary <
  case NUOp::eBiSLte:          //!< Binary <=
  case NUOp::eBiSGtr:          //!< Binary >
  case NUOp::eBiSGtre:         //!< Binary >=
    signedArithmetic = true;
    // fall-thru

  case NUOp::eBiULt:           //!< Binary <
  case NUOp::eBiULte:          //!< Binary <=
  case NUOp::eBiUGtr:          //!< Binary >
  case NUOp::eBiUGtre:         //!< Binary >=
    ret = sGenBinaryOperator (getArg (0), opcode, getArg (1), dbits,
                              signedArithmetic, expcontext);
    break;


  case NUOp::eBiEq:           //!< Binary ==
  case NUOp::eBiNeq:          //!< Binary !=
  case NUOp::eBiTrineq:	  //!< Partial support for ===.
  case NUOp::eBiTrieq:	  //!< Treat === and !== like == and !=.

    // Special cases...
    if (isZeroValue (getArg (1))
        && getArg (0)->isWholeIdentifier ()
        && getArg (0)->getBitSize () > LLONG_BIT)
      {
        out << TAG << (((opcode == NUOp::eBiEq) or (opcode == NUOp::eBiTrieq)) ? "(!" : " (");
        ret = getArg (0)->emitCode (expcontext);
        out << ".any())";
        ret = Precise (ret);
        break;
      }

    ret = sGenBinaryOperator (getArg (0), opcode, getArg (1), dbits, false, expcontext);
    ret = Precise (ret);
    break;

  case NUOp::eBiLogAnd:       //!< Binary Logical &&
  case NUOp::eBiLogOr:        //!< Binary Logical ||
    // Logicals pass down Flow context, since value only needs to be true or false
    {
      if (dbits > 1) {
        // Need a manufactured result, since logical operators only produce ONE bit
        // by default.
        out << TAG << CBaseTypeFmt (this, eCGDeclare) << "("; // Unbalanced "T("
        ret = eCGIsATemp;
      } else {
        // Check for requested temp
        ret = sGenerateTemp (dbits, signedArithmetic, &expcontext); // emits "T("
      }

      NU_ASSERT(!isDirtyOk(expcontext), this);

      out << TAG << "(";
      CGContext_t lc = getArg (0)->emitCode (expcontext | eCGFlow);
      if( !isPrecise (lc) )
        mask (lbits, "&");

      out << ")" << printOpStr(opcode, this, context) << "(";

      CGContext_t rc = getArg (1)->emitCode (expcontext | eCGFlow);
      if (!isPrecise (rc))
        mask (rbits, "&");
      out << "))";

      // Relational operators always yield a precise result
      //
      ret |= Precise (context);
      break;
    }

  case NUOp::eBiDownTo:
    // DownTo must be immediately below the partselect.  Any other situation
    // is almost certainly broken.
    //
    // Codegen a DownTo as a fieldwidth expression, so it's (1+(HI-LO))
    //
    NU_ASSERT (isDownToValid (context), this);
    out << TAG << "(1+";
    ret = sGenBinaryOperator (getArg (0), NUOp::eBiMinus, getArg (1), dbits,
                              signedArithmetic, expcontext);
    out << ")";
    break;

  case NUOp::eBiMinus:
  case NUOp::eBiPlus:
    if (constRv && constRv->isZero ()) {
      // Watch out for things that won't set the expression size correctly
      // consider:  a?(^X + 68'b0):68'b0
      // which would yield unbalanced ?: arguments if we aren't careful here!
      const NUExpr* e = getArg (0);

      if (e->getBitSize () != dbits)
        out << TAG << CBaseTypeFmt (this, isSignedResult (), eCGDefine);

      ret = getArg (0)->emitCode (expcontext); // zero was just a sizing artifact
      break;
    }

    /*
     * GCC seems to get confused about the type of (uA-uB) when it's used
     * in an expression like (uA-uB) <= uC, complaining about comparing
     * signed and unsigned operands..  But we don't need this if
     * we're dealing with bitvectors - they know their type...
     */
    if (dbits <= LLONG_BIT && not doOpEqual)
      out << TAG << CBaseTypeFmt ( this, isSignedResult (), eCGDefine );

    if (doOpEqual)
      expcontext |= eCGOpEqual;

    ret = sGenBinaryOperator (getArg(0), opcode, getArg(1), dbits,
                                  signedArithmetic, expcontext);
    break;

  case NUOp::eBiSMult:         //!< Binary *
  case NUOp::eBiUMult:
    if (doOpEqual)
      expcontext |= eCGOpEqual;

    ret = sGenBinaryOperator (getArg(0), opcode, getArg(1), dbits,
                              signedArithmetic, expcontext);
    break;

  case NUOp::eBiVhdlMod:
  {
    if ((rv.size() <= 64) || (constRv && rv.count () == 1))
    {
      if (doOpEqual)
        expcontext |= eCGOpEqual;

      // generate VHMOD(lop,rop,lsize) defined in carbon_priv.h 
      ret = sGenBinaryOperator (getArg(0), opcode, getArg(1), dbits,
                                    signedArithmetic, expcontext);
      ret = Imprecise (ret); 
      break;
    }
    else
    {
      gCodeGen->getMsgContext ()->CGNotPowerOfTwo (&getLoc (),
                                                   "Right",
                                               NUOp::convertOpToChar (opcode));
      break;
    }
  }


  case NUOp::eBiDExp:
    NU_ASSERT (isReal (), this);
    // fall-thru
  case NUOp::eBiExp:
    ret = sGenBinaryOperator (getArg (0), opcode, getArg (1), dbits, signedArithmetic,
                              expcontext);
    break;

  case NUOp::eBiUDiv:          //!< Binary /
  case NUOp::eBiSDiv:
  case NUOp::eBiBitOr:        //!< Binary Bitwise |
  case NUOp::eBiBitXor:       //!< Binary Bitwise ^
  case NUOp::eBiSMod:          //!< Binary %
  case NUOp::eBiUMod:
    if (doOpEqual)
      expcontext |= eCGOpEqual;

    ret = sGenBinaryOperator (getArg(0), opcode, getArg(1), dbits,
                              signedArithmetic, expcontext);
    break;

  case NUOp::eBiBitAnd:       //!< Binary Bitwise &
    if (sIsDirtyOk (this))
      expcontext |= eCGDirtyOk;
    if (doOpEqual)
      expcontext |= eCGOpEqual;

    ret = sGenBinaryOperator (getArg(0), opcode, getArg(1), dbits,
                              signedArithmetic, expcontext);
    break;

  case NUOp::eBiVhZxt: //!< VHDL zero-extend
    {
      UInt32 zxtWidth;
      if (!constRv || !constRv->getUL (&zxtWidth) || getArg (0)->isSignedResult ()) {
        ret = sGenBinaryOperator (getArg(0), opcode, getArg(1), dbits,
          signedArithmetic, expcontext);
      } else if (getBitSize () > zxtWidth && zxtWidth >= LLONG_BIT) {
        // The required size is larger than the zero-extend width, so coerce to
        // the larger size. This effectively further zero extends the unsigned
        // result. Without this coercion, BitVector expressions with ambiguous
        // operator overloadings may ensue (see bug 6503).        
        if (isBVOverloadSupported (context) && isBVExpr (context)) // Want to overload
            expcontext |= eCGBVOverloadSupported | eCGBVExpr;

        out << TAG;
        ret = sGenOpOrConst (getArg (0), expcontext, getBitSize (), signedArithmetic, mOp);
      } else {
        // zero extending an unsigned value....
        out << TAG;
        if (isBVOverloadSupported (context) && isBVExpr (context)) // Want to overload
            expcontext |= eCGBVOverloadSupported | eCGBVExpr;

        ret = sGenOpOrConst (getArg (0), expcontext, zxtWidth, signedArithmetic, mOp);
      }
      break;
    }

  case NUOp::eBiVhExt: //!< VHDL extension operator
    {
      UInt32 extWidth;
      // Check for simple behavior and just short circuit it...
      // e.g.     SXT(e, 2)
      if (constRv && constRv->getUL (&extWidth)) {
        if (extWidth == lbits
            && dbits <= lbits
            && getArg (0)->isSignedResult ()) {
          // sign-extending a signed quantity
          out << TAG;
          ret = getArg (0)->emitCode (expcontext);
          break;
        }
      }

      ret = sGenBinaryOperator (getArg(0), opcode, getArg(1), dbits,
                                signedArithmetic, expcontext);
      break;
    }
    
  case NUOp::eBiLshift:       //!< Binary <<
  case NUOp::eBiRshift:       //!< Binary >>
    // We expect that population make the shift-amount into an unsigned
    // expression, if it's not, then complain vigorously.

    if (doOpEqual)
      expcontext |= eCGOpEqual;

    // The unsigned shifts put zeros in, but we still need to keep the HDL
    // signedness of the expressions.
    out << TAG;
    ret = sGenShift (getArg (0), opcode, getArg (1), dbits, signedArithmetic,
                     expcontext);
    break;

  case NUOp::eBiVhLshift: //!< VHDL Shift left logical
  case NUOp::eBiVhRshift: //!< VHDL Shift right logical
  case NUOp::eBiVhRshiftArith:
  case NUOp::eBiVhLshiftArith:
  case NUOp::eBiLshiftArith: //!< Shift left arith
  case NUOp::eBiRshiftArith: //!< Shift right arith
  case NUOp::eBiRoL:         //!< Logical Rotate left
  case NUOp::eBiRoR:         //!< Logical Rotate right
    if (doOpEqual)
      expcontext |= eCGOpEqual;

    out << TAG;
    ret = sGenShift (getArg(0), opcode, getArg(1), dbits,
                     signedArithmetic, expcontext);
    break;

  case NUOp::eInvalid:         //!< Bad operator
    // msgContext->CGNotImpl ("expression operator");
    // break;

  default:
    break;
  }


  out << ")";
  return ret;
}

/*!
 * Examine \a expr using overload flags in \a context to set up \return CBaseTypeFmt 
 * describing the size/sign of the operand if we were able to overload it.
 */
static CBaseTypeFmt
sComputeOverload (CGContext_t context, const NUExpr*expr, UInt32 parentSize)
{
  UInt32 newWidth;
  if (isBVOverloadSupported (context) && isBVExpr (context)) {
    // Only apply effective size if we are overloading and then adjust
    // to appropriate physical size compatible with the parent context
    newWidth = bvSizeToOther (expr->effectiveBitSize (), parentSize);
  } else {
    // Not overloading - just use the size we would normally compute
    newWidth = expr->getBitSize ();
  }
    
  CBaseTypeFmt type (expr);     // first guess - size, sign, type
  type.mPrec = newWidth;        // Override 

  return type;
}

CGContext_t
NUTernaryOp::emitCode (CGContext_t context) const
{
  UtOStream &out = gCodeGen->CGOUT ();
  out << TAG;

  NU_ASSERT(!isDirtyOk (context) && !isOpEqual (context), this);

  context &= ~(eCGFlow);
  context = Precise (context);  // Assume bit accuracy maintained.

  CGContext_t expcontext = context;
  CGContext_t resultContext = 0;

  if (mOp != NUOp::eTeCond)
  {
    NU_ASSERT (!"Operator not implemented", this);
  }

  // ((op0) ? (type0(op1)) : (type0(op2)))
  //
  // where type0 is simple coercion to guarantee that
  // both operands are compatible - don't generate that if the type is compatible
  // with the result type of the ?:
  //
  size_t dbits = getBitSize (); // Result size

  if (NeedsATemp (getArg (1)) || NeedsATemp (getArg (2))) {
    context |= eCGNeedsATemp;
    expcontext &= ~eCGNeedsATemp;
  }

  resultContext = sGenerateTemp (dbits, isSignedResult (), &context); // 'T('

  // We only pass down the overload flags if BOTH operands have the same effective
  // bit size - otherwise the ?: will have different sizes and gcc will get upset.
  //
  bool overloading = isBVOverloadSupported (context) && isBVExpr (context);

  UInt32 leftEffectiveSize = getArg (1)->effectiveBitSize ();
  UInt32 rightEffectiveSize = getArg (2)->effectiveBitSize ();

  if ( overloading ) {
    if ( (leftEffectiveSize != rightEffectiveSize) || ( leftEffectiveSize > LLONG_BIT ) ) {
      expcontext &= ~(eCGBVOverloadSupported | eCGBVExpr);
    }
  }

  out << "((";
  // Don't pass any overload flags down to the conditional test, but do indicate that we
  // are in conditonal-flow context.
  if(!isPrecise( getArg(0)->emitCode((context | eCGFlow) & ~(eCGBVOverloadSupported | eCGBVExpr)) ) ) {
    mask( getArg(0)->getBitSize(), "&");
  }
  out << ")?";

  // Find out the sign/size information for the then/else branches
  CBaseTypeFmt thenType = sComputeOverload (expcontext, getArg (1), dbits);
  CBaseTypeFmt elseType = sComputeOverload (expcontext, getArg (2), dbits);

  if (not overloading) {
    // Not interested in tweaking for overload
    ;
  } else if (thenType != elseType) {
    // Couldn't get the same overload for the two branches, so abandon optimization and
    // recalculate 
    expcontext &= ~(eCGBVOverloadSupported | eCGBVExpr);
    overloading = false;
    thenType.mPrec = dbits;
    elseType.mPrec = dbits;
  } else if (thenType.mPrec >= dbits || thenType.mPrec > LLONG_BIT) {
    // The two branches have identical result types, but the type is LARGER
    // than the result type, or the branches weren't BV overload candidates
    // anyway.
    overloading = false;
  }

  // First process the THEN expression - handled just like binary or unary
  // operands, complete with sign-extension checks, which we expect will always
  // return false because the extension will be deferred until we get to the
  // context that the ?: appears in.  [BUG5630]  Consider the difference between
  //     x = (e ? thenExpr : elseExpr);     // no sign extension needed for assignment
  //     x >= (e ? thenExpr : elseExpr)     // compare needs to extend to machine-unit size
  // 
  if (not overloading and not isTypeCompatible (this, getArg (1)))
    out << TAG << CBaseTypeFmt (dbits, eCGDeclare, this->isSignedResult());

  out << "(";
  CGContext_t tctxt;
  if (emitUtil::needsSignExtension (mOp, getArg (1), getArg (2), thenType.mPrec, 0)) {
    out << TAG;
    tctxt = emitUtil::signExtend (out, getArg (1), expcontext, thenType.mPrec);
  } else {
    out << TAG;
    tctxt = sGenOpOrConst (getArg(1), expcontext, thenType.mPrec, thenType.mSigned, mOp);
  }
  out << "):";

  // Handle ELSE expression the same way

  if (not overloading and not isTypeCompatible (this, getArg (2)))
    out << TAG << CBaseTypeFmt (dbits, eCGDeclare, this->isSignedResult ());

  out << "(";
  CGContext_t fctxt;
  if (emitUtil::needsSignExtension (mOp, getArg (1), getArg (2), elseType.mPrec, 1)) {
    out << TAG;
    fctxt = emitUtil::signExtend (out, getArg (2), expcontext, elseType.mPrec);
  } else {
    out << TAG;
    fctxt= sGenOpOrConst (getArg(2), expcontext, elseType.mPrec, elseType.mSigned, mOp);
  }
  out << "))";

  if (getArg (1)->getBitSize () != getArg (2)->getBitSize ()
      && (dbits < getArg (1)->getBitSize ()
          || dbits < getArg (2)->getBitSize ()))
    // Even if BOTH subtrees were precise, if either one is larger
    // than the result we want, then we're not precise any more!
    resultContext |= Imprecise (tctxt & fctxt);
  else
    resultContext |= tctxt & fctxt;

  out << ")";		// Match with sGenerateTemp-generated '('
  return resultContext;
}


// Handle arbitrary unary, binary and ternary expressions.

CGContext_t
NUOp::emitCode (CGContext_t /* context*/) const
{
  NU_ASSERT ("Arbitrary op emitCode not expected"==0, this);    // never called
  return eCGVoid;
}
