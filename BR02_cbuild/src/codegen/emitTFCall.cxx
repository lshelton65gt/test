// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 /*
 * \file
 * Centralized implementation of all code emitters for Verilog task and
 * function-call code generation
 */

#include "nucleus/NUTF.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUNetSet.h"

#include "util/CbuildMsgContext.h"
#include "util/StringAtom.h"

#include "emitUtil.h"
#include "emitTFCall.h"

using namespace emitUtil;

//! Generate dummy param references, viz "(void)x"

static void sGenReferences (const NUTF* tf)
{
  UInt32 count = tf->getNumArgs ();
  if (count == 0)
    return;

  UtOStream& out = gCodeGen->CGOUT ();

  out << "  ";
  for(UInt32 i=0; i < count; ++i)
    {
      NUNet* arg = tf->getArg (i);

      if (i != 0)
	out << ", ";
      
      out << TAG << "(void)";
      arg->emitCode (eCGNameOnly | eCGList);
    }
  
  out << ";" << ENDL;
}


//! Is this parameter suitable to pass by reference or by value?
static bool
sPassByReference (const NUNet* net)
{
  // Anything that's OUTPUT or INOUT or larger than 64 bits AND not written
  // should be passed by reference
  //
  if (net->isBid() || net->isOutput())
    return true;		// Trivial - always by reference
  else if (net->is2DAnything())
    return true;		// These are implicitly passed by reference
  else if (net->getBitSize () <= LLONG_BIT)
    return false;		// pass small inputs by value

  // If this is an input port that's WRITTEN, then we
  // need to make sure we DON'T pass by reference, as we need a writeable
  // local copy.
  //
  if (net->isWritten ())
    // pass-by-value so we can twiddle with it.
    return false;

  // BitVector not written is called-by-reference for efficiency.
  return true;
}

//! Is this parameter passed as a constant?
static bool
sPassAsConst (const NUTF*, const NUNet* net)
{
  return (not isPOD( net )
	  && net->isInput ()
	  && not net->isWritten ());
}

//! Generate function params (function name defined elsewere)
/*
 * Verilog semantics may require copy-in/copy-out semantics for output and
 * inout ports if there are any aliases among the ports or via hierarchical
 * references.  Those decisions are made separately by an elaborated task-call
 * port-lowering walk that creates copys at the call-site and does the
 * copy-in/copy-out, or if it is more efficient, creates copies IN the task
 * and copies-in/out as needed to obtain proper semantics.
 */
static void sGenArgList (const NUTF* tf, bool unused = false)
{
  UtOStream& out = gCodeGen->CGOUT ();

  for(int i=0; i < tf->getNumArgs (); ++i)
    {
      if (i != 0)
	out << ", ";

      NUNet* arg = tf->getArg (i);

      out << TAG;

      if (sPassAsConst (tf, arg))
	// parameters *not* passed by reference don't want to be
	// 'const'.
	out << "const ";

      out << CType (arg);

      if (sPassByReference (arg))
	// inout, output or large inputs that aren't written are
	// passed by reference.
	out << "&";

      if (!unused) {  // If function doesn't use args, don't emit names
	out << " ";
        arg->emitCode (eCGNameOnly | eCGList | eCGMarkNets);
      }
    }
}

//! Generate function params
/*! This is the same as sGenArgList but we are not emitting the
 *  types. We just emit the formals again. This is to make nested
 *  calls through a functor class.
 */
static void sGenParamList (const NUTF* tf)
{
  UtOStream& out = gCodeGen->CGOUT ();

  for (int i = 0; i < tf->getNumArgs(); ++i)
  {
    if (i != 0)
      out << ", ";

    out << TAG;

    NUNet* arg = tf->getArg(i);
    arg->emitCode(eCGNameOnly | eCGList);
  }
}


// Just like a function but without a return value.
CGContext_t
NUTask::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream& out = gCodeGen->CGOUT ();

  out << HDLTAG (this);

  if (isNameOnly(context))
  {
    out << fname (getName ());
    return eCGVoid;
  }

  if (not isScheduled ()) {
    UtString tMsg ("task ");
    tMsg << getName ();
    return checkDeadCode (getLoc (), context, tMsg);
  }

  NUCost cost;
  bool inlineOK = emitUtil::isInlineCandidate (this, &cost);

  emitUtil::annotateCosts (cost);

  if (isDefine (context)) {
    if (inlineOK)
      out << "#if 0 " << ENDL;
    else if (not functionSection (this, 0))
      gCodeGen->putNonEmptyCxx (gCodeGen->getCurrentModule ());
  }

  out << "  " << TAG << "void ";
  if (isDefine (context))
    out << pclass (findParentModule ()->getName ());

  out << TAG << fname ( getName() ) << "(";
  sGenArgList (this);
  out << ")";

  if (isDeclare (context) && not inlineOK)
    {
      UtString sectionName;
      if (emitUtil::functionSection (this, &sectionName))
        gCodeGen->emitSection (out, sectionName);
      out << ";" << ENDL;
      return eCGVoid;
    }

  gCodeGen->pushScope (this);

  // Either inlined or this is the Definition
  out << "\n{" << ENDL << "  ";
  // Set the task's bucket here.  It gets reset by a call to
  // emitStartBucket() in NUTaskEnable::emitCode() or
  // NUTaskEnableHierRef::emitCode().
  emitStartBucket();

  // Dummy references to silence gcc 2.95
  out << TAG;
  sGenReferences (this);

  NUNetSet defs;
  getBlockingDefs(&defs);
  NUNetVector defloop;
  for (NUNetSet::SortedLoop p = defs.loopSorted(); !p.atEnd(); ++p) {
    defloop.push_back(*p);
  }
  out << TAG;
  emitCodeList(&defloop, (eCGMarkNets));

  out << TAG;
  emitUtil::genLocalDecls (this, true);

  NUNamedDeclarationScopeCLoop scopes = loopDeclarationScopes();
  out << TAG;
  emitCodeList (&scopes, (eCGDeclare | eCGZeroInit));

  NUStmtCLoop stmts = loopStmts ();
  out << TAG;
  emitCodeList (&stmts, eCGDefine|eCGMarkNets|eCGStmtList, "  ", "\n");
  out << "}" << ENDL;

  if (inlineOK && isDefine (context))
    out << "#endif\n";

  gCodeGen->popScope ();

  return eCGVoid;
}

// Explicitly iterate through arguments to mask imprecise port expressions.
static void emitTFActuals( NUTFArgConnectionCLoop arg_it, CGContext_t context )
{
  UtOStream& out = gCodeGen->CGOUT ();

  // The quick fix for 7683 passes down eCGActualParameter to sEmitPartsel in
  // emitNet.cxx. This causes sEmitPartsel to return a BitVector rather than a
  // BVref. This will disappear when 7683 is closed.
  context |= eCGActualParameter;

  bool firstTime = true;
  for (; not arg_it.atEnd(); ++arg_it)
  {
    if( firstTime )
      firstTime = false;
    else
      out << ",";
    out << TAG;

    const NUTFArgConnection *connection = *arg_it;
    CGContext_t actual_context = context;
    if (connection->getFormal ()->isSigned ()) {
      actual_context |= eCGSignedActual;
    }
    connection->emitCode (actual_context);
  }
}


CGContext_t
NUTFArgConnection::emitCode (CGContext_t /*context*/) const
{
  return eCGVoid;
}

// Check parameters to see if they need type-fiddling...
// 
static CGContext_t
sCastParameter (const NUNet* formal, const NULvalue *actual, CGContext_t context)
{
  UtOStream &out = gCodeGen->CGOUT ();

  if (sPassByReference (formal))
  {
    const NUNet *actNet = actual->getWholeIdentifier ();
    bool needParen = false;

    // Check for type compatibility (typically signed vs. unsigned)
    if (emitUtil::isTypeCompatible (formal, actNet)) {
      // The formal and actual types are compatible
    } else if (formal->isSigned () != actNet->isSigned ()) {
      // The formal is signed and the actual is not signed... or vice versa.
      // Generate something like: *reinterpret_cast<UInt32*>(&m_SINT32)
      // satisfying the parameter signature UInt32&
      needParen = true;
      out << TAG << "*reinterpret_cast<";
      if (formal->is2DAnything ()) {
        out << CType (formal);
      } else if ( formal->isUnsignedSubrange() ) {
        out << CBaseTypeFmt (formal->getBitSize(), eCGVoid, false);
      } else {
        out << CBaseTypeFmt (formal);
      }
      out << " *>(&";
    } else {
      // Any other source of type incompatibility is treated as an internal
      // error.  If we ever got here calling a task expecting a tristate but
      // getting a non-tristate actual, we'd REALLY be sorry about wallpapering
      // it over with a reinterpret_cast<>.
      NU_ASSERT2 (formal->isSigned () != actNet->isSigned (), formal, actual);
    }
    
    context = actual->emitCode (context);

    if (needParen)
      out << ")";
  }
  else
    context = actual->emitCode (context);

  return context | eCGPrecise;
}


CGContext_t
NUTFArgConnectionInput::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  const NUNet* formal = getFormal ();
  const NUExpr* actual = getActual ();

  if (not isDefine (context))
    return actual->emitCode (context);

  // Part of an actual function/task call
  UtOStream &out = gCodeGen->CGOUT ();

  out << TAG;

  // This might be an argument that's a different size than the
  // intended argument Handle some limited cases of parameter mismatch.
  // NOTE: the use of determineBitSize() to deal with the fact that we won't always
  // coerce an actual parameter if it's a partselect at a lower level.
  //
  if (actual->determineBitSize () != formal->getBitSize ()
      || actual->isSignedResult () != formal->isSigned ())
  {
    out << CBaseTypeFmt (formal, eCGDefine);
  }

  out << "(";
  context = getActual ()->emitCode(context);

  if (not isPrecise (context))
  {
    // by-value parameter can evaluate an expression
    mask(getBitSize(), "&");
  }
  out << ")";

  return context | eCGPrecise;
}

CGContext_t
NUTFArgConnectionOutput::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);
  
  if (not isDefine (context))
    return getActual ()->emitCode (context);

  return sCastParameter (getFormal (), getActual (), context);
}

CGContext_t
NUTFArgConnectionBid::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  if (not isDefine (context))
    return getActual ()->emitCode (context);

  return sCastParameter (getFormal (), getActual (), context);
}


// TBD
CGContext_t
NUTaskEnable::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream& out = gCodeGen->CGOUT ();

  out << TAG << fname( getTask()->getName() );

  out << "(";
  emitTFActuals( loopArgConnections(), context );
  out << ");";
  // This code used to handle noSemi, but it looks like it's never set.
  NU_ASSERT(!noSemi(context), this);
  // Now that we've returned from the function/task, switch back to
  // our bucket from the one that was set in NUTask::emitCode().
  out << TAG;
  emitStartBucket();

  return eCGVoid;
}

CGContext_t
NUTF::emitCode (CGContext_t /* context */) const
{
  return eCGVoid;
}

CGContext_t
NUTaskEnableHierRef::emitCode (CGContext_t context) const
{
  CGContext_t result = getHierRef()->emitCode(context);
  // Now that we've returned from the function/task, switch back to
  // our bucket from the one that was set in NUTask::emitCode().
  emitStartBucket();
  return result;
}

CGContext_t
NUTaskHierRef::emitCode (CGContext_t context) const
{
  context &= ~(eCGFlow | eCGBVOverloadSupported | eCGBVExpr);

  UtOStream& out = gCodeGen->CGOUT ();
  out << TAG;

  // Check if it is locally relative or not.
  NUModuleInstanceVector instPath;
  NUTask* resolved_task = 0;
  bool isLocal = isLocallyRelative(&instPath, &resolved_task);
  if (isLocal)
  {
    // We only have work to do in the define code. There is nothing to
    // declare.
    if (isDefine(context))
    {
      // Emit the path
      // Path may be empty if it resolves to this module.
      NUModuleInstanceVectorLoop loop(instPath);
      if (not loop.atEnd()) {
        out << TAG;
        emitCodeList (&loop, eCGNameOnly, "", ".", ".");
      }

      // Emit the task call
      const NUTaskEnable* taskEnable = getTaskEnable();
      NU_ASSERT(resolved_task == taskEnable->getTask(), taskEnable);
      taskEnable->getTask()->emitCode(eCGNameOnly);
      out << TAG;
      out << "(";
      emitTFActuals( taskEnable->loopArgConnections(), context );
      out << TAG;
      out << ");";
    }
  }

  // For non locals we have to call the task by pointer. That
  // pointer will be filled in after construction so that every
  // instance of this call can be to the correct resolution.
  // 
  // There are two cases here:
  //
  // 1. There is a single unelaborated resolution for every
  //    instance. In that case the type of the parent module is the same
  //    for all resolutions. So we just store a pointer to that parent to
  //    call the function.
  //
  // 2. There is more than one resolution so the parent modules are
  //    different. In this case we store a void* for the parent module
  //    and a pointer to a functor class that can convert the void* into
  //    the appropriate call.
  else
  {
    // All the task resolutions should have the same prototype. We
    // need one to emit the right data so pick one from all the
    // resolutions. Also count the number of resolutions because we
    // handle one vs multi-resolution differently.
    NUTask* task = getRepresentativeResolution();
    int taskCount = resolutionCount();

    // We are called in one of three contexts:
    //
    // 1. declare: emit data in the class declaration.
    //
    // 2. define: emit the call to the hier ref task.
    //
    // 3. functor: emit the functor derived class function in the cxx
    //    file.
    //
    // The first two contexts are valid for both single and
    // multi-resolution cases, but the last is only valid in the
    // multi-resolution case.
    const NUTaskEnable* taskEnable = getTaskEnable();
    if (taskCount == 1)
    {
      // Create a name for the pointer to the parent module
      UtString parentPtr;
      CGTFgenHierRefTaskName(eParentVar, this, NULL, &parentPtr);

      if (isDeclare(context))
      {
        // We can use the same parent module in all instances
        NUModule* parent = task->findParentModule();
        out << "  " << mclass (parent) << "* " << parentPtr << ";" << ENDL;
        gCodeGen->allocation(ptrSize(), ptrSize(), 1);
      }
      else if (isDefine(context))
      {
        // This is the call to the function through a known parent
        // module
        out << TAG << parentPtr << "->";
        task->emitCode(eCGNameOnly);
        out << "(";
        emitTFActuals ( taskEnable->loopArgConnections(), context );
        out << ");" << ENDL;
      }
    } // if
    else
    {
      // There is more than one resolution. We have to call the task
      // through a functor class. Create the various names for the
      // type and variable names we are going to emit.
      UtString baseClass;
      CGTFgenHierRefTaskName(eFunctorBase, this, NULL, &baseClass);
      UtString parentVar;
      CGTFgenHierRefTaskName(eParentVar, this, NULL, &parentVar);
      UtString functorPtr;
      CGTFgenHierRefTaskName(eFunctorPtr, this, NULL, &functorPtr);

      if (isDeclare(context))
      {
        // We need a set of functor classes.  One base class and N
        // derived classes with functions to call the right types.
        // The base class method would be pure virtual, but that can
        // cause a C++ runtime dependency, which we want to avoid, so
        // instead we implement a method that assert fails.  Start
        // with the base class.
        out << "  class " << baseClass << ENDL
            << "  {\n"
            << "  public: CARBONMEM_OVERRIDES\n"
            << "    virtual ~" << baseClass << "(){};\n"
            << "    virtual void " << fname(getTaskName())
            << "(void *parent";
	if( task->getNumArgs() != 0 )
	  out << ", ";
        sGenArgList(task);
        out << ");" << ENDL
            << "  };\n\n";

        // Emit the derived functor classes. We use the same base
        // class name but append the resolution tasks parent. Since
        // the base class name is unique per task we won't have
        // conflicts with other hier ref tasks to the same module.
	UtSet<UtString> covered;
        for (TaskVectorCLoop l = loopResolutions(); !l.atEnd(); ++l)
        {
          task = *l;
          NUModule* taskParent = task->findParentModule();
          UtString derivedClass;
          CGTFgenHierRefTaskName(eFunctorDerived, this, taskParent, &derivedClass);
	  // Prune duplicate classes.
	  // This can happens under -multiLevelHierTasks and -flatten
	  // Needed for fix bug2999.
          if( covered.find( derivedClass ) == covered.end() ) {
            covered.insert( derivedClass );
            out << "  class " << derivedClass << " : public " << baseClass << ENDL
                << "  {\n"
                << "  public:\n"
                << "    virtual ~" << derivedClass << "(){};\n"
                << "    void " << fname(getTaskName())
                << "(void* parent ";
    	   if( task->getNumArgs() != 0 )
	     out << ", ";
           sGenArgList(task);
           out << ");\n  };" << ENDL << "\n";
	  }
        }

        // Emit the storage for the functor pointer and void*
        out << "  void* " << parentVar << ";" << ENDL
            << "  " << baseClass << "* " << functorPtr << ";" << ENDL;
        gCodeGen->allocation(ptrSize(), ptrSize(), 2);
      } // if

      else if (isDefine(context))
      {
        // Emit the call to the functor through the base class
        out << TAG << functorPtr << "->" << fname(getTaskName())
            << "(" << parentVar;
	if( task->getNumArgs() != 0 )
	  out << ", ";
        emitTFActuals(taskEnable->loopArgConnections(), context);
        out << ");" << ENDL;
      }

      else if (isFunctor(context))
      {
        // Emit the base class method definition to ensure it's
        // overridden.  This could be a virtual function, but that
        // sometimes causes a dependency on the C++ runtime, which we
        // want to avoid.
        out << "void " << TAG << mclass(taskEnable->findParentModule()) << "::"
            << baseClass
            << "::" << fname(getTaskName()) << "(void *";
        if( task->getNumArgs() != 0 )
          out << ", ";
        sGenArgList(task, true);
        out << ")" << ENDL
            << "{\n"
            << "  assert(\"This method must be overridden\" == 0);   // This method must be overridden\n"             // this_assert_OK
            << "}\n\n";

        // Emit a functor function per resolution in the cxx file
	UtSet<UtString> covered;
        for (TaskVectorCLoop l = loopResolutions(); !l.atEnd(); ++l)
        {
          task = *l;
          NUModule* taskParent = task->findParentModule();
	  UtString derivedClass;
          CGTFgenHierRefTaskName(eFunctorDerived, this, taskParent, &derivedClass);
          if( covered.find( derivedClass ) == covered.end() ) {
            covered.insert( derivedClass );
            const NUModule* enableParent = taskEnable->findParentModule();
            out << "void " << TAG << mclass(enableParent) << "::"
                << baseClass << "$" << noprefix(taskParent)
                << "::" << fname(getTaskName()) << "(void *parent";
    	    if( task->getNumArgs() != 0 )
	      out << ", ";
            sGenArgList(task);
            out << ")" << ENDL
                << "{\n"
              << "  ((" << mclass(taskParent) << "*)parent)->";
            task->emitCode(eCGNameOnly);
            out << TAG << "(";
            sGenParamList(task);
            out << ");" << ENDL
                << "}\n\n";
          }
        } // for
      } // else if
    } // else
  } // else

  return eCGVoid;
}

static CodeGen::CxxName fp ("m_fp","");
static CodeGen::CxxName fi ("FI$","$");

void
CGTFgenHierRefTaskName(CGHierRefTaskName name, const NUTaskHierRef* taskHierRef,
                       const NUModule* taskParent, UtString* hierRefName)
{
  // Create a string to represent the call. We use the text from the
  // original HDL hier ref and replace .'s (which are not legal name
  // characters) with $'s. This way we won't conflict with any user
  // names.
  UtString callIdent;
  taskHierRef->composeCPath(&callIdent);
  const StringAtom* hName = gCodeGen->getCodeNames ()->translate (&callIdent);

  // Create the name for our caller
  switch (name)
  {
    case eParentVar:
      if (taskHierRef->resolutionCount() == 1)
        *hierRefName << emitUtil::member (hName);
      else
        *hierRefName << fp (hName)
                     << emitUtil::noprefix(taskHierRef->getTaskName());
      break;

    case eFunctorBase:
      *hierRefName << fi (hName)
                   << emitUtil::noprefix(taskHierRef->getTaskName());
      break;

    case eFunctorDerived:
      CGTFgenHierRefTaskName(eFunctorBase, taskHierRef, NULL, hierRefName);
      *hierRefName << "$" << emitUtil::noprefix(taskParent);
      break;

    case eFunctorPtr:
      *hierRefName << fp (hName)
                   << emitUtil::noprefix(taskHierRef->getTaskName())
                   << "$fi";
      break;
  } // switch
} // CGTFgenHierRefTaskName
