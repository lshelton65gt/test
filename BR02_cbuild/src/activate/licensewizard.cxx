/***************************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/****************************************************************************
**
** Copyright (C) 2007-2008 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

// This code is derived from the Qt License Wizard example.

#include <QtGui>

#include "licensewizard.h"

LicenseWizard::LicenseWizard(QWidget *parent)
    : QWizard(parent)
{
  setPage(Page_Intro, new IntroPage);
  setPage(Page_Activate, new ActivatePage);
  setPage(Page_Conclusion, new ConclusionPage);
  setStartId(Page_Intro);
  setWindowIcon(QIcon(":images/carbon.png"));

  setOption(HaveHelpButton, true);
  setPixmap(QWizard::LogoPixmap, QPixmap(":/images/carbonLogo.bmp"));

  connect(this, SIGNAL(helpRequested()), this, SLOT(showHelp()));

  setWindowTitle(tr("License Activation Wizard"));
}

void LicenseWizard::showHelp()
{
  QString message;

  switch (currentId()) {
  case Page_Intro:
    message = tr("This wizard lets you blah blah blah.");
    break;
  case Page_Activate:
    message = tr("Your serial number is blah...");
    break;
  case Page_Conclusion:
    message = tr("Blah, blah, blah.");
    break;
  default:
    message = tr("This help is likely not to be of any help.");
  }

  QMessageBox::information(this, tr("License Wizard Help"), message);
}

IntroPage::IntroPage(QWidget *parent)
    : QWizardPage(parent)
{
  setTitle(tr("License Activation Wizard"));
  setPixmap(QWizard::WatermarkPixmap, QPixmap(":/images/WelcomeImage.bmp"));

  topLabel = new QLabel(tr("This wizard will activate your license."));
  topLabel->setWordWrap(true);

  activateRadioButton = new QRadioButton(tr("&Activate a license"));
  viewRadioButton = new QRadioButton(tr("&View activated licenses"));
  activateRadioButton->setChecked(true);

  QVBoxLayout *layout = new QVBoxLayout;
  layout->addWidget(topLabel);
  layout->addWidget(activateRadioButton);
  layout->addWidget(viewRadioButton);
  setLayout(layout);
}

int IntroPage::nextId() const
{
  if (viewRadioButton->isChecked()) {
     return LicenseWizard::Page_Conclusion;
  } else {
    return LicenseWizard::Page_Activate;
  }
}

ActivatePage::ActivatePage(QWidget *parent)
    : QWizardPage(parent)
{
  setTitle(tr("Activate Your License"));
  setSubTitle(tr("Enter your license serial number, and your license will "
                 "be activated over the Internet."));

  activationIDLabel = new QLabel(tr("S&erial number:"));
  activationIDLineEdit = new QLineEdit;
  activationIDLabel->setBuddy(activationIDLineEdit);

  registerField("register.activationID*", activationIDLineEdit);

  QGridLayout *layout = new QGridLayout;
  layout->addWidget(activationIDLabel, 0, 0);
  layout->addWidget(activationIDLineEdit, 0, 1);
  setLayout(layout);
}

int ActivatePage::nextId() const
{
  return LicenseWizard::Page_Conclusion;
}

ConclusionPage::ConclusionPage(QWidget *parent)
    : QWizardPage(parent)
{
  // Output is too wide to even try to fit in window with the
  // watermark taking up space, so leave it off.

  // setPixmap(QWizard::WatermarkPixmap, QPixmap(":/images/WelcomeImage.bmp"));

  results = new QTextEdit;
  results->setReadOnly(true);
  results->setLineWrapMode(QTextEdit::NoWrap);

  QVBoxLayout *layout = new QVBoxLayout;
  layout->addWidget(results);
  setLayout(layout);

  connect(&process, SIGNAL(readyReadStandardOutput()),
          this, SLOT(updateOutputTextEdit()));
  connect(&process, SIGNAL(finished(int, QProcess::ExitStatus)),
          this, SLOT(processFinished(int, QProcess::ExitStatus)));
  connect(&process, SIGNAL(error(QProcess::ProcessError)),
          this, SLOT(processError(QProcess::ProcessError)));
}

int ConclusionPage::nextId() const
{
    return -1;
}

bool ConclusionPage::isComplete() const
{
  return mProcessFinished; // Not complete until process is done.
}
void ConclusionPage::initializePage()
{
  QStringList args;
  QString text;
  if (wizard()->hasVisitedPage(LicenseWizard::Page_Activate)) {
      text = tr("Activation results");
      args << "-activate" << "-serialNumber" << field("register.activationID").toString();
  } else {
      text = tr("Currently activated licenses");
      args << "-view";
  }
  // Send anything from standard error to standard output.
  process.setProcessChannelMode(QProcess::MergedChannels);
  process.start("licenseactivationcli", args, QIODevice::ReadOnly);

  setTitle(text);
  results->clear();
  mProcessFinished = false; // Disable Finish button until done.
}

void ConclusionPage::processFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
  if (exitStatus == QProcess::CrashExit) {
      results->append(tr("Activation program crashed"));
    } else if (exitCode != 0) {
      results->append(tr("Activation failed"));
    } else {
      // Say nothing additional if it worked.
    }
  mProcessFinished = true;
  emit completeChanged();  // Tell wizard to check isComplete()
}
void ConclusionPage::processError(QProcess::ProcessError error)
{
  if (error == QProcess::FailedToStart) {
    results->append(tr("Activation program not found"));
  }
}
void ConclusionPage::updateOutputTextEdit()
{
  QByteArray newData = process.readAllStandardOutput();
  QString text = results->toPlainText()
                 + QString::fromAscii(newData).remove(QChar('\r'));
  results->setPlainText(text);
}
