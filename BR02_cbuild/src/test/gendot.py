#!/usr/bin/python
#
# Given a debug dump from cbuild, generate a dot file for the graph
# generates the files: unelab.dot, elab.dot
#
# Example of how to use:
#  cbuild -dumpDesign -dumpElabFlow a.f > a.dump
#  gendot.py < a.dump
#  dot -Tps -ounelab.ps unelab.dot
#  dot -Tps -oelab.ps elab.dot
#

import sys, re, string

re_indent = re.compile("([ ]*)(.*)")
re_component = re.compile("\{([^}]*)\}([^(]*)\((0x?[a-f0-9]*)\)(.*)")
re_netname = re.compile(": ([^ ]*) (.*)")

re_flnode = re.compile("\((flow|flowbound) (0x?[a-f0-9]*) (.*)\)")
re_flnodeelab = re.compile("\((flowelab|flowboundelab) (0x?[a-f0-9]*) (.*)\)")
re_fanin = re.compile("\(fanin (0x?[a-f0-9]*) (0x?[a-f0-9]*)\)")
re_faninelab = re.compile("\(faninelab (0x?[a-f0-9]*) (0x?[a-f0-9]*)\)")
re_usedef = re.compile("\(usedef (0x?[a-f0-9]*)\)(.*)")
re_nested = re.compile("\(nested (0x?[a-f0-9]*)\)(.*)")
re_block = re.compile("\(block (0x?[a-f0-9]*)\)(.*)")
re_blockelab = re.compile("\(blockelab (0x?[a-f0-9]*)\)(.*)")
re_flow = re.compile("\(flow (0x?[a-f0-9]*)\)(.*)")
re_net = re.compile("\(net (0x?[a-f0-9]*)\)(.*)")
re_netelab = re.compile("\(netelab (0x?[a-f0-9]*)\)(.*)")

def main():
    ## components is tuple: (srcinfo, type, rest)
    components = {}

    elab_print_list = ["digraph Elaborated {\n"]
    print_list = ["digraph Unelaborated {\n"]
    while(1):
        line = sys.stdin.readline()
        if (line == ''):
            break
        match = re_indent.match(line)
        cur_indent = len(match.group(1))
        line = string.strip(line)

        match = re_component.match(line)
        if (match):
            srcinfo = match.group(1)
            comptype = match.group(2)
            compid = match.group(3)
            rest = string.strip(match.group(4))
            component = (srcinfo[string.rfind(srcinfo,'/')+1:], comptype, rest)
            components[compid] = component

        match = re_fanin.match(line)
        if match:
            print_list.append('"'  + match.group(1) + '" -> "' + match.group(2) + '"\n')

        # Handle unelaborated flow nodes
        match = re_flnode.match(line)
        if match:
            flnode = match.group(2)
            rest = match.group(3)
            netid = ''
            usedefid = ''
            nestedid = ''
            blockid = ''
            type = ''
            while len(rest) != 0:
                rest = string.strip(rest)
                match = re_net.match(rest)
                if match:
                    netid = match.group(1)
                    rest = match.group(2)
                    continue
                match = re_usedef.match(rest)
                if match:
                    usedefid = match.group(1)
                    rest = match.group(2)
                    continue
                match = re_nested.match(rest)
                if match:
                    nestedid = match.group(1)
                    rest = match.group(2)
                    continue
                match = re_block.match(rest)
                if match:
                    blockid = match.group(1)
                    rest = match.group(2)
                    continue
                print 'CANNOT HANDLE ' + rest
                break
            srcinfo = ''
            netname = ''
            shape='ellipse'
            if netid:
                component = components[netid]
                srcinfo = component[0]
                netname = re_netname.match(component[2]).group(1)
            if usedefid:
                component = components[usedefid]
                srcinfo = component[0]
                type = component[1]
            if nestedid:
                print_list.append( '"' + nestedid + '" -> "' + flnode + '" [style=dotted,label="nest"];\n')
                shape='box'
            if blockid:
                type = 'bound'
                print_list.append( '"' + flnode + '" -> "' + blockid + '" [style=dotted,label="bound"];\n')
            label = 'label="' + flnode + '\\n' + netname + '\\n' + type + ' ' + srcinfo + '"'
            print_list.append( '"' + flnode + '" [shape=' + shape + ',' + label + ']\n')

        match = re_faninelab.match(line)
        if match:
            elab_print_list.append('"'  + match.group(1) + '" -> "' + match.group(2) + '"\n')

        # Handle elaborated flow nodes
        match = re_flnodeelab.match(line)
        if match:
            flnode = match.group(2)
            rest = match.group(3)
            netelabid = ''
            usedefid = ''
            nestedid = ''
            blockid = ''
            type = ''
            while len(rest) != 0:
                rest = string.strip(rest)
                match = re_net.match(rest)
                if match:
                    netid = match.group(1)
                    rest = match.group(2)
                    continue
                match = re_netelab.match(rest)
                if match:
                    rest = match.group(2)
                    continue
                match = re_usedef.match(rest)
                if match:
                    usedefid = match.group(1)
                    rest = match.group(2)
                    continue
                match = re_flow.match(rest)
                if match:
                    flowid = match.group(1)
                    rest = match.group(2)
                    continue
                match = re_nested.match(rest)
                if match:
                    nestedid = match.group(1)
                    rest = match.group(2)
                    continue
                match = re_blockelab.match(rest)
                if match:
                    blockid = match.group(1)
                    rest = match.group(2)
                    continue
                print 'CANNOT HANDLE ' + rest
                break
            srcinfo = ''
            shape='ellipse'
            if netid:
                component = components[netid]
                srcinfo = component[0]
                netname = re_netname.match(component[2]).group(1)
            if usedefid:
                component = components[usedefid]
                srcinfo = component[0]
                type = component[1]
            if nestedid:
                elab_print_list.append( '"' + nestedid + '" -> "' + flnode + '" [style=dotted,label="nest"];\n')
                shape='box'
            if blockid:
                type = 'bound'
                elab_print_list.append( '"' + flnode + '" -> "' + blockid + '" [style=dotted,label="bound"];\n')
            label = 'label="' + flnode + '\\n' + netname + '\\n' + type + ' ' + srcinfo + '"'
            elab_print_list.append( '"' + flnode + '" [shape=' + shape + ',' + label + ']\n')

    elab_print_list.append( "}\n")
    print_list.append( "}\n")
    f = open("unelab.dot","w")
    f.writelines(print_list)
    f = open("elab.dot","w")
    f.writelines(elab_print_list)

if __name__ == "__main__":
    main()
