﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;

using Carbon.ModelKitServices;

namespace RegisterMetaMain
{
  class Program
  {
    static int Main(string[] args)
    {
      return RegisterMeta.Main(args);
    }
  }
}
