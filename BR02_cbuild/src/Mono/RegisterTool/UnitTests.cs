﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using NUnit.Framework;

using System.Xml;

using Carbon.ModelKitServices;

//
// Regression Tests
//    To run them:
//    linux
//       $CARBON_HOME/bin/mono /tools/windows/NUnit-2.6/bin/nunit-console.exe $CARBON_HOME/Linux/bin/RegisterTool.exe
//
//    windows
//       $CARBON_HOME\bin\mono \\dfs\acton\tools\windows/nunit-console.exe $CARBON_HOME\Win\bin\RegisterTool.exe
//

namespace RegisterTool
{
  [TestFixture]
  public class UnitTests
  {
    public string XMLAnswersFileName { get; set; }
    public string WorkingDirectory { get; set; }
    public string Test1XMLFile { get; set; }

    [SetUp]
    public void Init()
    {
      WorkingDirectory = Environment.CurrentDirectory;
      Console.WriteLine("Running RegisterTool Tests");
      XMLAnswersFileName = Path.Combine(Path.GetTempPath(), "UnitTests.xmlAnswers");
      File.WriteAllBytes(XMLAnswersFileName, TestResources.UnitTests);

      Test1XMLFile = Path.Combine(Path.GetTempPath(), "Test1.xml");
      File.WriteAllText(Test1XMLFile, TestResources.Test1);

      Assert.IsTrue(File.Exists(XMLAnswersFileName));
      Assert.IsTrue(File.Exists(Test1XMLFile));
    }

    [Test(Description = "Test 1")]
    public void Test1()
    {
      string outputFile = Path.Combine(Path.GetTempPath(), "Test1Output.xml");
      List<string> args = new List<string>();

      args.Add(Test1XMLFile);
      args.Add("-xmlAnswers");
      args.Add(XMLAnswersFileName);
      args.Add("-o");
      args.Add(outputFile);

      Assert.AreEqual(0, RegisterMeta.Main(args.ToArray()), "Non-zero exit status");
      Assert.IsTrue(File.Exists(outputFile), "Missing output file {0}", outputFile);

      XmlDocument doc = new XmlDocument();
      doc.Load(outputFile);

      Assert.IsTrue(doc.DocumentElement.Name == "RegisterMeta", "Expecting RegisterMeta");

      foreach (XmlNode childNode in doc.DocumentElement.ChildNodes)
      {
        if (childNode is XmlElement)
        {
          XmlElement e = childNode as XmlElement;
          if (childNode.Attributes["value"] != null && childNode.Attributes["expectedValue"] != null)
          {
            Assert.AreEqual(childNode.Attributes["value"].Value, childNode.Attributes["expectedValue"].Value, "Mismatch value on tag {0}", e.Name);
          }
          else if (childNode.Attributes["expectedValue"] != null)
          {
            Assert.AreEqual(childNode.Attributes["expectedValue"].Value, childNode.InnerText.Trim(), "Mismatch text value on tag {0} {1}", e.Name, e.OuterXml);
          }
        }
      }

      // Cores test;
      int numCores = doc.DocumentElement.SelectNodes("//RegisterMeta/core").Count;
      Assert.AreEqual(numCores, 2, "Expected 2 core elements");

      //Console.WriteLine(doc.DocumentElement.OuterXml);
    }
  }
}
