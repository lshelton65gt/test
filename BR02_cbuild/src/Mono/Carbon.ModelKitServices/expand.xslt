﻿<xsl:stylesheet version="2.0"
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xmlns:carbon="http://www.carbondesignsystems.com/XMLSchema/RegisterMeta/1.0"
     xmlns:my="my:my"
     exclude-result-prefixes="xsi"
                >
  <xsl:output omit-xml-declaration="yes" indent="yes" encoding="utf-8" />
  <xsl:strip-space elements="*"/>

  <xsl:template match="node()|@*">
    <xsl:param name="pVars" as="element()*"/>
    <xsl:copy>
      <xsl:apply-templates select="node()|@*">
        <xsl:with-param name="pVars" select="$pVars"/>
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>
 
  <xsl:template match="carbon:for">
    <xsl:param name="pVars" as="element()*"/>

    <xsl:variable name="vCurrentFor" select="."/>

    <xsl:for-each select="@from to @to">
      <xsl:variable name="vnewVars">
        <xsl:sequence select="$pVars"/>
        <var name="{$vCurrentFor/@var}" value="{current()}"/>
      </xsl:variable>

      <xsl:apply-templates select="$vCurrentFor/node()">
        <xsl:with-param name="pVars" select="$vnewVars/*"/>
      </xsl:apply-templates>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="text()[contains(., '$(')]">
    <xsl:param name="pVars" as="element()*"/>

    <xsl:value-of select="my:evalText($pVars, .)"/>
  </xsl:template>

  <xsl:template match="@*[contains(., '$(')]">
    <xsl:param name="pVars" as="element()*"/>

    <xsl:attribute name="{name()}">
      <xsl:value-of select="my:evalText($pVars, .)"/>
    </xsl:attribute>
  </xsl:template>

  <xsl:function name="my:evalText">
    <xsl:param name="pVars" as="element()*"/>
    <xsl:param name="pText"/>

    <xsl:analyze-string select="$pText"
      regex="\$\((.+?)\)">
      <xsl:non-matching-substring>
        <xsl:value-of select="."/>
      </xsl:non-matching-substring>
      <xsl:matching-substring>
        <xsl:variable name="vName" select="regex-group(1)"/>

        <xsl:variable name="vReplacement" select=
                "$pVars[@name eq $vName][last()]/@value"/>
        <xsl:value-of select="string($vReplacement)"/>
      </xsl:matching-substring>
    </xsl:analyze-string>
  </xsl:function>

</xsl:stylesheet>