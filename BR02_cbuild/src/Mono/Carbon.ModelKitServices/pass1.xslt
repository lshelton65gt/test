﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:CarbonExpr="carbon:EvalExpression"                
   xmlns:Carbon="carbon:EvaluateDependency"
   xmlns:carbon="http://www.carbondesignsystems.com/XMLSchema/RegisterMeta/1.0"
   xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output omit-xml-declaration="yes" indent="yes" encoding="iso-8859-1" />

    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>


  <xsl:template match="@from">
    <xsl:attribute name="{name()}">
      <xsl:value-of select="Carbon:Expand(.)"/>
    </xsl:attribute>
  </xsl:template>
  
  <xsl:template match="@to">   
    <xsl:attribute name="{name()}">
      <xsl:value-of select="Carbon:Expand(.)"/>
    </xsl:attribute>
  </xsl:template>
     
</xsl:stylesheet>
