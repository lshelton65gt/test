﻿<xsl:stylesheet version="2.0"
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xmlns:CarbonExpr="carbon:EvalExpression"
     xmlns:Carbon="carbon:EvaluateDependency"
     xmlns:carbon="http://www.carbondesignsystems.com/XMLSchema/RegisterMeta/1.0"
     xmlns:my="my:my">
  <xsl:output omit-xml-declaration="yes" indent="yes" encoding="utf-8" />

  <xsl:template match="node()|@*">    
    <xsl:copy>
      <xsl:apply-templates select="node()|@*">
       
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="carbon:choose">
    <xsl:variable name="id" select="generate-id(.)" />
    <xsl:variable name="chooseBegin" select="Carbon:BeginChoose($id)" />
    <xsl:for-each select="carbon:when">
      <xsl:variable name="test">
        <xsl:value-of select="@test"/>
      </xsl:variable>
      <xsl:if test="Carbon:TestChoose($id, @test) and CarbonExpr:EvalExpression(@test)">
        <xsl:apply-templates select="child::*">
        </xsl:apply-templates>
      </xsl:if>           
    </xsl:for-each>
    <xsl:for-each select="carbon:otherwise">
      <xsl:if test="Carbon:TestOtherwise($id)">
        <xsl:apply-templates select="child::*">
        </xsl:apply-templates>
      </xsl:if>     
    </xsl:for-each>  
  </xsl:template>
  

  <xsl:template match="carbon:if">   
    <xsl:choose>
      <xsl:when test="CarbonExpr:EvalExpression(@test)">       
          <xsl:apply-templates select="child::*">          
          </xsl:apply-templates>       
      </xsl:when>
      <xsl:otherwise/>
    </xsl:choose>    
  </xsl:template>
  

  


</xsl:stylesheet>