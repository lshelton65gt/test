﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Collections;
using System.Text.RegularExpressions;

namespace Carbon.ModelKitServices
{
  public class VariableDefCase
  {
    public string Expression { get; set; }
    public string Value { get; set; }
  }

  public class VariableDef
  {
    public string Name { get; set; }
    public List<VariableDefCase> Cases { get; set; }
    public string OtherwiseValue { get; set; }

    public VariableDef()
    {
      Cases = new List<VariableDefCase>();
      OtherwiseValue = null;
    }

    public string Evaluate(XmlDocument xmlAnswers)
    {
      foreach (VariableDefCase vdc in Cases)
      {
        XPathNavigator nav = xmlAnswers.CreateNavigator();
        XPathExpression expr1 = nav.Compile(vdc.Expression);
        object v = nav.Evaluate(expr1);
        bool result = bool.Parse(v.ToString());
        if (result)
          return vdc.Value;
      }
      return OtherwiseValue;
    }
  }

  public class Extensions
  {
    public Dictionary<string, VariableDef> VariableDefs { get; set; }
    public Dictionary<string, string> Variables { get; set; }
    public CarbonDB.CarbonDB Database { get; set; }

    Dictionary<string, bool> choseTable = new Dictionary<string, bool>();

    private XmlDocument _answers;
    public Extensions(XmlDocument xmlAnswers)
    {
      Variables = new Dictionary<string, string>();
      VariableDefs = new Dictionary<string, VariableDef>();
      _answers = xmlAnswers;
    }

    public void BeginVar(string id)
    {
      VariableDef v = new VariableDef();
      v.Name = id;
      VariableDefs[id] = v;
    }


    public void BeginChoose(string id)
    {
      choseTable[id] = false;
    }

    public bool TestChoose(string id, string expr)
    {
      if (choseTable.ContainsKey(id))
      {
        bool hit = choseTable[id];
        if (!hit)
        {
          bool newval = false;
          if (bool.TryParse(expr, out newval))
          {
            choseTable[id] = newval;
            return newval;
          }
        }
      }
      return false;
    }

    public bool TestOtherwise(string id)
    {
      if (choseTable.ContainsKey(id))
      {
        bool hit = choseTable[id];
        return !hit;
      }
      else
        return false;
    }


    public void FinishVar(string id)
    {
      string value = VariableDefs[id].Evaluate(_answers);

      Variables[id] = value;

      //Console.WriteLine(id, value);
    }

    public void AddCase(string id, string expr, string value)
    {
      VariableDefCase c = new VariableDefCase();
      c.Expression = expr;
      c.Value = Expand(value);
      VariableDefs[id].Cases.Add(c);
    }

    public void AddOtherwise(string id, string value)
    {
      VariableDefs[id].OtherwiseValue = Expand(value);
    }

    public bool EvalExpression(string expr)
    {
      if (string.IsNullOrEmpty(expr))
        return false;

      XPathNavigator nav = _answers.CreateNavigator();
      XPathExpression expr1 = nav.Compile(expr);

      // Do we have a CarbonDB?
      if (Database != null)
      {
        CarbonDBXsltContext dbCtx = new CarbonDBXsltContext(new NameTable(), Database);
        expr1.SetContext(dbCtx);
      }

      bool r = false;
      if (bool.TryParse(expr, out r))
        return r;

      object v = nav.Evaluate(expr1);
      return bool.Parse(v.ToString());
    }

    public string Expand(string input)
    {
      string output = input;
      foreach (string k in Variables.Keys)
      {
        string value = Variables[k];
        string varRef = string.Format("${{{0}}}", k);
        output = output.Replace(varRef, value);
      }

      if (_answers != null)
      {
        XPathNavigator nav = _answers.CreateNavigator();
        try
        {
          XPathExpression expr1 = nav.Compile(output);
          if (expr1 != null)
          {
            XPathNodeIterator nodes = nav.Select("//Answers/A");
            if (nodes != null && nodes.Count > 0)
            {
              if (nodes.Current == null)
                nodes.MoveNext();

              if (nodes.Current != null)
              {
                // If the string is "1.0" it would get expanded to "1" which we don't want
                // so we only want to expand if the string contains something needing expansion
				bool needsEvaluation = false;
                if (output.ToLower().Contains("id(")) needsEvaluation = true;
				if (ToBeEvaled (output)) {
					output = TrimEval (output);
					needsEvaluation = true;
				}

                XPathExpression query = nodes.Current.Compile(output);
                if (query != null && needsEvaluation)
                {
                  object qv = nav.Evaluate(query, nodes);
                  if (qv != null)
                  {
                    if (qv is XPathNodeIterator)
                    {
                      XPathNodeIterator nodeIter = qv as XPathNodeIterator;
                      if (nodeIter.Count > 0)
                      {
                        nodeIter.MoveNext();
                        if (nodeIter.Current != null)
                        {
                          XPathNavigator n = nodeIter.Current;
                          output = n.Value;
                        }
                      }
                    }
                    else
                    {
                      output = qv.ToString();
                    }
                  }
                }
              }
            }
          }         
        }
        catch (XPathException) // Ignore literal expressions such as "foo"
        {
        }
      }

      //Console.WriteLine("Finished Expanding: {0} to: {1}", input, output);
      return output;
    }
    
    private static bool ToBeEvaled (string s)
    {
      return
       Regex.Match (s, @"^eval\s*\(").Success  &&  Regex.Match (s, @"\)$").Success;
    }
    
    private static string TrimEval (string s)
    {
      return Regex.Replace (
        Regex.Replace (s, @"^eval\s*\(", ""), @"\)$", "");
    }

    public string EvalDependency(string expr)
    {
      return expr;
    }
  }

  class XsltVariable : IXsltContextVariable
  {
    public bool IsLocal
    {
      get { return false; }
    }
    public bool IsParam
    {
      get { return false; }
    }
    public XPathResultType VariableType
    {
      get { return XPathResultType.Any; }
    }
    public object Evaluate(XsltContext xsltContext)
    {
      return null;
    }
  }

  public class CarbonDBXsltContext : XsltContext
  {
    private bool _whitespace;
    private bool _preserveWhitespace;
    private CarbonDB.CarbonDB _db;

    public CarbonDBXsltContext(NameTable nt, CarbonDB.CarbonDB db)
      : base(nt)
    {
      _db = db;
      _whitespace = false;
      _preserveWhitespace = false;
    }

    public override IXsltContextVariable ResolveVariable(string prefix, string name)
    {
      // Logger.Log("ResolveVariable: prefix={0}, name={1}", prefix, name);
      return new XsltVariable();
    }

    public override IXsltContextFunction ResolveFunction(string prefix, string name, XPathResultType[] ArgTypes)
    {
      // Logger.Log("ResolveFunction: prefix={0}, name={1}", prefix, name);

      if (prefix == "CarbonDB")
      {
        switch (name)
        {
          case "findNode": return new FindNodeFunction(ArgTypes, _db);
        }
      }

      return null;
    }

    public override bool Whitespace
    {
      get { return _whitespace; }
    }
    public override bool PreserveWhitespace(XPathNavigator node)
    {
      return _preserveWhitespace;
    }
    public override int CompareDocument(string doc1, string doc2)
    {
      return 0;
    }

    public class FindNodeFunction : IXsltContextFunction
    {
      private CarbonDB.CarbonDB _db;

      private XPathResultType[] _argTypes;

      public FindNodeFunction(XPathResultType[] argTypes, CarbonDB.CarbonDB db)
      {
        _db = db;
        _argTypes = argTypes;

        foreach (XPathResultType t in argTypes)
          if (t != XPathResultType.String)
            throw new Exception("incorrect argument type: must supply string values");
      }

      public int Minargs
      {
        get { return 1; }
      }

      public int Maxargs
      {
        get { return 1; }
      }

      public XPathResultType ReturnType
      {
        get { return XPathResultType.Boolean; }
      }

      public XPathResultType[] ArgTypes
      {
        get { return _argTypes; }
      }

      public object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
      {
        string hierPath = (string)args[0];
        CarbonDB.CarbonDBNode node = _db.FindNode(hierPath);
        bool found = node != null;
        // Logger.Log("Result of findnode: {0} is {1}", hierPath, found);
        return found;
      }
    }
  }
}
