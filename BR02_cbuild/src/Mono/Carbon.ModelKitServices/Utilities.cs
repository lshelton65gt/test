﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace Carbon.ModelKitServices
{

 
  /// <summary>
  /// XML Parsing Utility Class
  /// </summary>
  public class XmlHelper
  {
    /// <summary>
    /// Returns the integer converted attribute value specified by <paramref name="attName"/> or
    /// returns <paramref name="defaultValue"/> if none found.
    /// </summary>
    /// <param name="n">The XmlNode being searched</param>
    /// <param name="attName">The name of the Attribute to locate</param>
    /// <param name="defaultValue">The default value returned if no attribute was found.</param>
    /// <returns>The attribute value or <paramref name="defaultValue"/> if not found.</returns>
    public static int AttributeValue(XmlNode n, string attName, int defaultValue)
    {
      if (n != null && n.Attributes[attName] != null)
        return int.Parse(n.Attributes[attName].Value);
      else
        return defaultValue;
    }

    /// <summary>
    /// Returns the double converted attribute value specified by <paramref name="attName"/> or
    /// returns <paramref name="defaultValue"/> if none found.
    /// </summary>
    /// <param name="n">The XmlNode being searched</param>
    /// <param name="attName">The name of the Attribute to locate</param>
    /// <param name="defaultValue">The default value returned if no attribute was found.</param>
    /// <returns>The attribute value or <paramref name="defaultValue"/> if not found.</returns>
    public static double AttributeValue(XmlNode n, string attName, double defaultValue)
    {
      if (n != null && n.Attributes[attName] != null)
        return double.Parse(n.Attributes[attName].Value);
      else
        return defaultValue;
    }
    /// <summary>
    /// Returns the string converted attribute value specified by <paramref name="attName"/> or
    /// returns <paramref name="defaultValue"/> if none found.
    /// </summary>
    /// <param name="n">The XmlNode being searched</param>
    /// <param name="attName">The name of the Attribute to locate</param>
    /// <param name="defaultValue">The default value returned if no attribute was found.</param>
    /// <returns>The attribute value or <paramref name="defaultValue"/> if not found.</returns>

    public static string AttributeValue(XmlNode n, string attName, string defaultValue)
    {
      if (n != null && n.Attributes[attName] != null)
        return n.Attributes[attName].Value;
      else
        return defaultValue;
    }


    /// <summary>
    /// Returns the bool converted attribute value specified by <paramref name="attName"/> or
    /// returns <paramref name="defaultValue"/> if none found.
    /// </summary>
    /// <param name="n">The XmlNode being searched</param>
    /// <param name="attName">The name of the Attribute to locate</param>
    /// <param name="defaultValue">The default value returned if no attribute was found.</param>
    /// <returns>The attribute value or <paramref name="defaultValue"/> if not found.</returns>
    public static bool AttributeValue(XmlNode n, string attName, bool defaultValue)
    {
      if (n != null && n.Attributes[attName] != null)
        return bool.Parse(n.Attributes[attName].Value);
      else
        return defaultValue;
    }
  }

  /// <summary>
  /// Utility class
  /// </summary>
  public class Utilities
  {
    /// <summary>
    /// Indicates we are executing on a Linux based system
    /// </summary>
    static public bool IsUnix
    {
      get
      {
        int p = (int)Environment.OSVersion.Platform;
        return (p == 4) || (p == 6) || (p == 128);
      }
    }

    /// <summary>
    /// Indicates we are running within the Mono Environment (Mono can run on both Linux and Windows)
    /// </summary>
    static public bool IsMono
    {
      get
      {
        Type t = Type.GetType("Mono.Runtime");
        return t != null;
      }
    }
  }
}
