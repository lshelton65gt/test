﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Carbon.ModelKitServices.Tools;

namespace Carbon.ModelKitServices
{
  /// <summary>
  /// Class to hold the list of files to be retained by the model kit
  /// </summary>
  public class ModelKitRetainFiles
  {
    private List<string> files;

    /// <summary>
    /// ModelKit retained file container
    /// </summary>
    public ModelKitRetainFiles()
    {
      files = new List<string>();
    }
    /// <summary>
    /// Add a file to be retained
    /// </summary>
    /// <param name="fileName">Filename to be copied to </param>
    public void AddFile(string fileName)
    {
      string file = Environment.ExpandEnvironmentVariables(fileName);
      if (!File.Exists(file))
        throw new Exception(string.Format("Error: ModelKitRetainFiles::AddFile NO such file: {0}", file));

      files.Add(file);
    }

    /// <summary>
    /// Retain the files
    /// </summary>
    /// <param name="iKit"></param>
    /// <param name="variant"></param>
    /// <param name="partner"></param>
    /// <param name="workingVersion">Can be workarea, mainline or X.Y.Z</param>
    public void RetainFiles(IModelKit iKit, string variant, string partner, string workingVersion)
    {
      if (!Utilities.IsUnix)
      {
        Logger.Warning("Running on windows development flow only files are not retained");
        return;
      }
      //
      // Special handling for release builds; CARBON_MK_SPECIALIZED_HOME will be defined and we handle that 
      // case slightly different.
      //
      // e.g. CARBON_IMK_SPECIALIZED_HOME=/o/mkwork/qa/IMK/arm/SpecializedKits
      //
      string imkBuild = Environment.GetEnvironmentVariable("CARBON_IMK_SPECIALIZED_HOME");

      string ikitVersion = workingVersion;

      // If we are building from the portal, use the version supplied by the kit.
      if (string.IsNullOrEmpty(workingVersion))
        ikitVersion = iKit.Version;

      // if is special area, use it blindly, instead of iKit.Version
      bool isSpecialArea = workingVersion == "mainline" || workingVersion == "workarea";

      if (!isSpecialArea)
      {
        if (iKit.Version != ikitVersion)
          throw new Exception(string.Format("Error: ModelKit Version {0} does not match supplied version: {1}", iKit.Version, ikitVersion));
      }

      // e.g $CARBON_MK_HOME/ARM/CortexR5/4.0.0
      string destinationBaseDir = Environment.ExpandEnvironmentVariables(Path.Combine("%CARBON_MK_HOME%", iKit.Vendor.ToString(), iKit.Name, ikitVersion));
      if (!string.IsNullOrEmpty(imkBuild))
        destinationBaseDir = Path.Combine(imkBuild, iKit.Name, ikitVersion);

      string destinationDir = Path.Combine(destinationBaseDir, "UserFiles");
      if (!Directory.Exists(destinationDir))
        Directory.CreateDirectory(destinationDir);

      if (!Directory.Exists(destinationDir))
        throw new Exception(string.Format("Error: Unable to create RetainedFiles directory: {0}", destinationDir));

      Logger.Log("Retaining files in {0}", destinationDir);

      // Copy the ModelKit library
      copyModelKitLibrary(destinationBaseDir);

      // Copy the Scripts directory
      copyScriptsDirectory(destinationBaseDir);

      // Copy the include directory
      copyMKLibInclude(destinationBaseDir);

      // Copy the MKL CSharp library
      copyMKLibCSharp(destinationBaseDir);

      // Retain the MK Defined end-user supplied files
      retainKitFiles(destinationDir);
    }

    private void retainKitFiles(string destinationDir)
    {
      if (files.Count > 0)
      {
        Logger.Log("Retaining Kit User Files");
        foreach (string file in files)
        {
          FileUtilities.CopyFile(file, destinationDir);
        }
      }
    }

    private static void copyScriptsDirectory(string destinationBaseDir)
    {
      string destScriptsDir = Path.Combine(destinationBaseDir, "Scripts");
      string scriptsDir = Environment.ExpandEnvironmentVariables(Path.Combine("%CARBON_MK_HOME%", "Scripts"));
      Logger.Log("Copying Scripts from {0} to {1}", scriptsDir, destScriptsDir);
      Carbon.ModelKitServices.Tools.FileUtilities.CopyDirectory(scriptsDir, destScriptsDir, true);
    }

    private static void copyMKLibInclude(string destinationBaseDir)
    {
      string destScriptsDir = Path.Combine(destinationBaseDir, "include");
      string scriptsDir = Environment.ExpandEnvironmentVariables(Path.Combine("%CARBON_MK_HOME%", "include"));
      Logger.Log("Copying mkl/include from {0} to {1}", scriptsDir, destScriptsDir);
      Carbon.ModelKitServices.Tools.FileUtilities.CopyDirectory(scriptsDir, destScriptsDir, true);
    }

    private static void copyMKLibCSharp(string destinationBaseDir)
    {
      string destScriptsDir = Path.Combine(destinationBaseDir, "CSharp");
      string scriptsDir = Environment.ExpandEnvironmentVariables(Path.Combine("%CARBON_MK_HOME%", "CSharp"));
      Logger.Log("Copying mkl/CSharp from {0} to {1}", scriptsDir, destScriptsDir);
      Carbon.ModelKitServices.Tools.FileUtilities.CopyDirectory(scriptsDir, destScriptsDir, true);
    }

    private static void copyModelKitLibrary(string destinationBaseDir)
    {
      string destMKLib = Path.Combine(destinationBaseDir, "lib");
      string mkLibDir = Environment.ExpandEnvironmentVariables(Path.Combine("%CARBON_MK_HOME%", "lib"));
      Logger.Log("Copying ModelKit Library from {0} to {1}", mkLibDir, destMKLib);
      Carbon.ModelKitServices.Tools.FileUtilities.CopyDirectory(mkLibDir, destMKLib, true);
    }
  }
}
