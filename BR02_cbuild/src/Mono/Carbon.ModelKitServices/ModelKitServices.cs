﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Carbon.CarbonDB;
using Carbon.Ccfg;

namespace Carbon.ModelKitServices
{
  /// <summary>
  /// ModelKitServices top level Enums and Convienience functions
  /// </summary>
  public class Model
  {

    public enum CBuildPlatform
    {
      Linux = 1,
      Windows = 2
    }
    /// <summary>
    /// Model Build Platform Flags, may be Or'd in any combination
    /// </summary>
    [Flags]
    public enum ModelBuildPlatform : uint
    {
      /// <summary>
      /// All Platforms
      /// </summary>
      All = 0xffffffff,
      /// <summary>
      /// Gcc - Linux
      /// </summary>
      Gcc = 0x00000001,
      /// <summary>
      /// VS2005 - Windows - DEPRECATED
      /// </summary>
      VS2005 = 0x00000002,
      /// <summary>
      /// VS2008 - Windows
      /// </summary>
      VS2008 = 0x00000004,
      /// <summary>
      /// VS2010 - Windows
      /// </summary>
      VS2010 = 0x00000010,
      /// <summary>
      /// VS2012 - Windows
      /// </summary>
      VS2012 = 0x00000020
    }

    /// <summary>
    /// The non-flag version (exclusive) Platform
    /// </summary>
    public enum ModelPlatform
    {
      /// <summary>
      /// GCC 
      /// </summary>
      Gcc,
      /// <summary>
      /// Visual Studio 2005
      /// </summary>
      VC2005,
      /// <summary>
      /// Visual Studio 2008
      /// </summary>
      VC2008,
      /// <summary>
      /// Visual Studio 2010
      /// </summary>
      VC2010,
      /// <summary>
      /// Visual Studio 2012
      /// </summary>
      VC2012
    }

    /// <summary>
    /// The Type of cbuild compile
    /// </summary>
    public enum CbuildType
    {
      /// <summary>
      /// A cbuild -writeGuiDB type compilation, no pgo
      /// </summary>
      Explore,
      /// <summary>
      /// A full cbuild compilation, with pgo
      /// </summary>
      Compile,
      /// <summary>
      /// Compile w/o PGO
      /// </summary>
      CompileNoPGO
    }

    /// <summary>
    /// The Type of component being built
    /// </summary>
    public enum ComponentType
    {
      /// <summary>
      /// Unspecified
      /// </summary>
      Undefined = 0,
      /// <summary>
      /// SoC Designer Component
      /// </summary>
      SoCDesigner = 1,
      /// <summary>
      /// StandAlone Component
      /// </summary>
      StandAlone = 2
    }

    /// <summary>
    /// Platform contains at least Windows flag
    /// </summary>
    /// <param name="platform">The Platform being tested</param>
    /// <returns>true if <paramref name="platform"/> contains at least one Windows flag</returns>
    public static bool isWindows(ModelBuildPlatform platform)
    {
      return isVS2005(platform) || isVS2008(platform) || isVS2010(platform) || isVS2012(platform);
    }

    /// <summary>
    /// Convert a platform "vc2005", etc. into the bit mask value
    /// </summary>
    /// <param name="platformString"></param>
    /// <returns></returns>
    public static Model.ModelBuildPlatform Convert(string platformString)
    {
      string platform = platformString.ToLower();
      if (platform == "all")
        throw new Exception("All is not allowed for conversion");

      switch (platform)
      {
        case "vc2005": return ModelBuildPlatform.VS2005;
        case "vc2008": return ModelBuildPlatform.VS2008;
        case "vc2010": return ModelBuildPlatform.VS2010;
        case "vc2012": return ModelBuildPlatform.VS2012;
        case "gcc": return ModelBuildPlatform.Gcc;
        default:
          throw new Exception(string.Format("Invalid platform string '{0}'", platform));
      }
    }

    /// <summary>
    /// convert from bitmask ModelBuildPlatform to enumerated type ModelPlatform
    /// </summary>
    /// <param name="platform">The Platform to be converted</param>
    /// <returns>returns: converted value</returns> 
    public static CBuildPlatform Convert(ModelBuildPlatform platform)
    {
      if (isLinux(platform))
      {
        return Model.CBuildPlatform.Linux;
      }
      else if (isVS2005(platform))
      {
        return Model.CBuildPlatform.Windows;
      }
      else if (isVS2008(platform))
      {
        return Model.CBuildPlatform.Windows;
      }
      else if (isVS2010(platform))
      {
        return Model.CBuildPlatform.Windows;
      }
      else if (isVS2012(platform))
      {
        return Model.CBuildPlatform.Windows;
      }
      else
      {
        throw new Exception("unsupported platform.");
      }
    }



    /// <summary>
    /// Platform contains at least Linux flag
    /// </summary>
    /// <param name="platform">The Platform being tested</param>
    /// <returns>true if <paramref name="platform"/> contains at least one Linux flag</returns> 
    public static bool isLinux(ModelBuildPlatform platform)
    {
      return isGcc(platform);
    }
    /// <summary>
    /// Platform contains Gcc flag
    /// </summary>
    /// <param name="platform">The Platform being tested</param>
    /// <returns>true if <paramref name="platform"/> contains the <seealso cref="Model.ModelBuildPlatform"/>Gcc flag</returns> 
    public static bool isGcc(ModelBuildPlatform platform)
    {
      return (platform & ModelBuildPlatform.Gcc) != 0;
    }

    /// <summary>
    /// Platform contains VS2008 flag
    /// </summary>
    /// <param name="platform">The Platform being tested</param>
    /// <returns>true if <paramref name="platform"/> contains the <seealso cref="Model.ModelBuildPlatform"/>VS2008 flag</returns> 
    public static bool isVS2008(ModelBuildPlatform platform)
    {
      return (platform & ModelBuildPlatform.VS2008) != 0;
    }

    /// <summary>
    /// Platform contains VS2005 flag
    /// </summary>
    /// <param name="platform">The Platform being tested</param>
    /// <returns>true if <paramref name="platform"/> contains the <seealso cref="Model.ModelBuildPlatform"/>VS2005 flag</returns> 
    public static bool isVS2005(ModelBuildPlatform platform)
    {
      return (platform & ModelBuildPlatform.VS2005) != 0;
    }

    /// <summary>
    /// Platform contains VS2010 flag
    /// </summary>
    /// <param name="platform">The Platform being tested</param>
    /// <returns>true if <paramref name="platform"/> contains the <seealso cref="Model.ModelBuildPlatform"/>VS2010 flag</returns> 
    public static bool isVS2010(ModelBuildPlatform platform)
    {
      return (platform & ModelBuildPlatform.VS2010) != 0;
    }

    /// <summary>
    /// Platform contains VS2012 flag
    /// </summary>
    /// <param name="platform">The Platform being tested</param>
    /// <returns>true if <paramref name="platform"/> contains the <seealso cref="Model.ModelBuildPlatform"/>VS2012 flag</returns> 
    public static bool isVS2012(ModelBuildPlatform platform)
    {
      return (platform & ModelBuildPlatform.VS2012) != 0;
    }

    //
    /// <summary>
    /// Translate a Unix CARBON_HOME to a Windows Equivalent using UNC based path names
    /// </summary>
    /// <remarks>
    /// Can handle /o/release, /o/work, /release, /mnt/home, /homes, /release, /w/
    /// </remarks>
    /// <param name="unixCarbonHome">The Unix path specification</param>
    /// <returns>UNC converted path name (abeit still using Unix style slashes)</returns>
    public static string ConvertUnixCarbonHomeToUNC(string unixCarbonHome)
    {
      string r = unixCarbonHome;

      r = r.Replace("/o/release", "\\release");
      r = r.Replace("/o/work", "\\work");
      r = r.Replace("/release", "\\release");
      r = r.Replace("/mnt/home", "\\home");
      r = r.Replace("/homes", "\\homes");
      r = r.Replace("/release", "\\release");

      // Handle /w specially
      if (r.StartsWith("/w/"))
      {
        Regex re = new Regex("^/w/(\\w+)/(.*)");
        MatchCollection mc = re.Matches(r);
        if (mc.Count == 1 && mc[0].Groups.Count >= 3)
        {
          string machineName = mc[0].Groups[1].Captures[0].Value;
          string restOfLine = mc[0].Groups[2].Captures[0].Value;
          return string.Format("\\\\{0}\\w\\{1}", machineName, restOfLine).Replace("\\", "/");
        }
      }

      return string.Format("\\\\dfs\\acton{0}", r).Replace("\\", "/");
    }

    /// <summary>
    /// Given a Linux path to MAXSIM_HOME, removes the trailing /Linux
    /// </summary>
    /// <param name="unixMaxsimHome">A Linux MAXSIM_HOME path</param>
    /// <returns>The string without the trailing /Linux</returns>
    public static string ConvertUnixMaxsimHomeToBaseUNC(string unixMaxsimHome)
    {
      const string LINUX = "/Linux";

      string convertedPath = ConvertUnixCarbonHomeToUNC(unixMaxsimHome);
      if (convertedPath.EndsWith("/"))
        convertedPath = convertedPath.Substring(0, convertedPath.Length - 1);

      // Converted path is a UNC now, test for ending in \\Linu
      if (convertedPath.EndsWith(LINUX))
        convertedPath = convertedPath.Substring(0, convertedPath.Length - LINUX.Length);

      return convertedPath;
    }

    /// <summary>
    /// Compare two Carbon Version strings, i.e. 1.7290, 1.8000
    /// </summary>
    /// <param name="v1">The Left operand</param>
    /// <param name="v2">The Right operand</param>
    /// <returns>
    /// -1 if <paramref name="v1"/> is less than <paramref name="v2"/>
    ///  0 if <paramref name="v1"/> is equal to <paramref name="v2"/>
    /// +1 if <paramref name="v1"/> is greather than <paramref name="v2"/>
    /// </returns>
    public static int CompareCarbonVersions(string v1, string v2)
    {

      int dv1 = v1.Count(c => c == '.');
      int dv2 = v2.Count(c => c == '.');

      if (dv1 > dv2)
      {
        int needed = dv1 - dv2;
        for (int i = 0; i < needed; i++)
          v2 += ".0";
      }
      else if (dv1 < dv2)
      {
        int needed = dv2 - dv1;
        for (int i = 0; i < needed; i++)
          v1 += ".0";
      }

      string[] v1parts = v1.Split('.');
      string[] v2parts = v2.Split('.');

      for (int i = 0; i < v1parts.Length; i++)
      {
        int p1 = int.Parse(v1parts[i]);
        int p2 = int.Parse(v2parts[i]);

        if (p1 > p2)
          return 1;
        else if (p1 < p2)
          return -1;
      }

      return 0;
    }

    private static string CarbonSoftwareVersion(string dbVersion)
    {
      Regex re = new Regex("^\\$Revision: ([0-9.]+) .*");
      MatchCollection mc = re.Matches(dbVersion);
      if (mc.Count == 1 && mc[0].Groups.Count > 1)
      {
        string ver = mc[0].Groups[1].Value;
        return ver;
      }
      else
        throw new Exception(string.Format("Illegal Carbon Version: {0}", dbVersion));
    }

    /// <summary>
    /// Given the input <paramref name="versions"/> specification, test it against
    /// the version of CarbonSoftware to see if any of the versions are greater 
    /// than or equal to.
    /// </summary>
    /// <param name="versions">String containing a version or versions(comma-seperated)</param>
    /// <returns>true if the criteria is met</returns>
    public static bool VersionCheck(string versions)
    {
      char[] seps = new char[] { ',' };
      CarbonCfgTop t = new CarbonCfgTop();
      string cbuildVersion = CarbonSoftwareVersion(t.cbuildVersion());
      Logger.Log("CBuild Version: {0} ({1})", cbuildVersion, t.cbuildVersion());

      if (!string.IsNullOrEmpty(versions))
      {
        foreach (string tv in versions.Split(seps))
        {
          string v = tv.Trim();

          int compareResult = CompareCarbonVersions(cbuildVersion, v);
          Logger.Log("Compared {0} with {1}, Result: {2}", cbuildVersion, v, compareResult);
          if (compareResult >= 0)
          {
            Logger.Log("Version Check: PASS ({0} >= {1})", cbuildVersion, v);
            return true;
          }
        }
        Logger.Log("Version Check: FAIL ({0})", cbuildVersion);
        return false;
      }
      else // Any version will do
        return true;
    }
  }
}
