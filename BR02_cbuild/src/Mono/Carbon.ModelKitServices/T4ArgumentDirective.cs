﻿using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TextTemplating;
using System.Runtime.Remoting.Messaging;

namespace Carbon.T4Host
{
  /// <summary>
  /// Custom T4 directive processor that makes the template arguments accessible
  /// from within the template. The argument are accesible through a
  /// generated property. The argument value is retrieved by calling the static
  /// LogicalGetData from CallContext.
  /// </summary>
  internal sealed class ArgumentDirective : DirectiveProcessor
  {
    #region private fields

    private static readonly CodeGeneratorOptions options = new CodeGeneratorOptions
    {
      BlankLinesBetweenMembers = true,
      IndentString = "        ",
      VerbatimOrder = true,
      BracingStyle = "C"
    };
    private CodeDomProvider provider;
    private readonly StringWriter classCodeWriter = new StringWriter();
    private readonly StringWriter initializationCodeWriter = new StringWriter();

    #endregion

    #region methods

    /// <summary>
    /// Starts the processing run.
    /// </summary>
    /// <param name="languageProvider">The language provider.</param>
    /// <param name="templateContents">The template contents.</param>
    /// <param name="errors">The errors.</param>
    public override void StartProcessingRun(CodeDomProvider languageProvider, string templateContents, CompilerErrorCollection errors)
    {
      base.StartProcessingRun(languageProvider, templateContents, errors);

      provider = languageProvider;
    }

    /// <summary>
    /// Processes the directive.
    /// </summary>
    /// <param name="directiveName">Name of the directive.</param>
    /// <param name="arguments">The arguments.</param>
    public override void ProcessDirective(string directiveName, IDictionary<string, string> arguments)
    {
      string name = arguments["name"];
      string fieldName = string.Format("_{0}", arguments["name"]);
      string type = arguments["type"];

      //Create field
      var field = new CodeMemberField(type, fieldName)
      {
        Attributes = MemberAttributes.Private
      };

      provider.GenerateCodeFromMember(field, classCodeWriter, options);

      //Create the property for each argument
      var property = new CodeMemberProperty
      {
        Name = name,
        Type = new CodeTypeReference(type),
        Attributes = MemberAttributes.Public,
        HasGet = true,
        HasSet = false
      };
      property.GetStatements.Add(
                new CodeMethodReturnStatement(new CodeFieldReferenceExpression(
                                                  new CodeThisReferenceExpression(),
                                                  fieldName)));

      provider.GenerateCodeFromMember(property, classCodeWriter, options);

      //Generate initialization code for each argument
      //_FieldName = (FieldType) this.GetHostOption("_FieldName");
      CodeAssignStatement assignment = new CodeAssignStatement(
                new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), fieldName),
                new CodeCastExpression(
                    new CodeTypeReference(type),
                    new CodeMethodInvokeExpression(
                        new CodeTypeReferenceExpression(typeof(CallContext)),
                        "LogicalGetData",
                        new CodePrimitiveExpression(name))));

      provider.GenerateCodeFromStatement(assignment, initializationCodeWriter, options);
    }

    /// <summary>
    /// Generates the code that will be added to the generated template. For
    /// each argument a private field is generated that will hold the argument 
    /// value. Also a readonly public property is generated to access the argument 
    /// value. When a convertor of editor is defined there is a attribute genereted 
    /// for each of them.
    /// </summary>
    /// <returns>The fields and arguments</returns>
    public override string GetClassCodeForProcessingRun()
    {
      return classCodeWriter.ToString();
    }

    /// <summary>
    /// Gets the post initialization code for processing run. This will 
    /// generate a call to the GetArgument method of the host for each 
    /// private field generated. 
    /// </summary>
    /// <returns></returns>
    public override string GetPostInitializationCodeForProcessingRun()
    {
      return initializationCodeWriter.ToString();
    }

    /// <summary>
    /// Finishes the processing run.
    /// </summary>
    public override void FinishProcessingRun()
    {
      return;
    }

    /// <summary>
    /// Gets the imports for processing run.
    /// </summary>
    /// <returns></returns>
    public override string[] GetImportsForProcessingRun()
    {
      return null;
    }

    /// <summary>
    /// Gets the pre initialization code for processing run.
    /// </summary>
    /// <returns></returns>
    public override string GetPreInitializationCodeForProcessingRun()
    {
      return null;
    }

    /// <summary>
    /// Gets the references for processing run.
    /// </summary>
    /// <returns></returns>
    public override string[] GetReferencesForProcessingRun()
    {
      return null;
    }

    /// <summary>
    /// Determines whether the directive with the specific name is supported.
    /// </summary>
    /// <param name="directiveName">Name of the directive.</param>
    /// <returns>
    /// 	<c>true</c> always returns <c>true.</c>.
    /// </returns>
    public override bool IsDirectiveSupported(string directiveName)
    {
      return true;
    }

    #endregion
  }
}