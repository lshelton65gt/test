﻿using System;
using System.Collections.Generic;
using System.IO;

using System.Linq;
using System.Text;

using Carbon.ModelKitServices.Tools;

namespace Carbon.ModelKitServices
{
  /// <summary>
  /// The ModelKitContext object is automatically constructed and is passed into each
  /// method used by the IModelKit Interface
  /// </summary>
  public class ModelKitContext
  {
    /// <summary>
    /// The path to the XML answers file used.
    /// </summary>
    public string XMLAnswersFilePath { get; set; }
    /// <summary>
    /// The WorkingDirectory from which this ModelKit is being built, usually the location
    /// where the .ZIP file a hs been extracted
    /// </summary>
    public string WorkingDirectory { get; private set; }
    /// <summary>
    /// The Directory that contains the .dll and supporting files to build this kit.
    /// </summary>
    public string ModelKitDirectory { get; private set; }

    /// <summary>
    /// The Top-level Directory containing the built models, underneath will be [componentType]/[platform]
    /// </summary>
    public string ModelDataDirectory { get; set; }

    /// <summary>
    /// The Root Path to the data directory, i.e. "data/SoCDesigner"
    /// </summary>
    /// <returns></returns>
    public string ComponentDataDirectory()
    {
      string componentDataDir = Path.Combine(ModelDataDirectory, ComponentType.ToString());
      return componentDataDir;
    }

    /// <summary>
    /// The Root Path to the data directory, i.e. "data/platform/SoCDesigner/
    /// </summary>
    /// <param name="platform">Platform type</param>
    /// <returns></returns>
    public string ComponentPlatformDataDirectory(Model.ModelPlatform platform)
    {
      string destDir = Path.Combine(ModelDataDirectory, platform.ToString().ToLower(), ComponentType.ToString());
      return destDir;
    }

    /// <summary>
    ///The Root Path to the data directory, i.e. "data/SoCDesigner/platform
    /// </summary>
    /// <param name="mbplatform">The platform flag</param>
    /// <returns></returns>
    public string ComponentPlatformDataDirectory(Model.ModelBuildPlatform mbplatform)
    {
      Model.ModelPlatform platform = Model.ModelPlatform.Gcc;
      if (Model.isGcc(mbplatform))
        platform = Model.ModelPlatform.Gcc;
      else if (Model.isVS2005(mbplatform))
        platform = Model.ModelPlatform.VC2005;
      else if (Model.isVS2008(mbplatform))
        platform = Model.ModelPlatform.VC2008;
      else if (Model.isVS2010(mbplatform))
        platform = Model.ModelPlatform.VC2010;
      else if (Model.isVS2012(mbplatform))
        platform = Model.ModelPlatform.VC2012;

      return ComponentPlatformDataDirectory(platform);
    }

    /// <summary>
    /// The Directory in which the Linux carbon model is compiled
    /// </summary>
    public string LinuxModelDirectory { get; private set; }

    /// <summary>
    /// The Directory in which the Windows carbon model is compiled
    /// </summary>
    public string WindowsModelDirectory { get; private set; }

    /// <summary>
    /// The Fullpath name to the Linux design (i.e. /foo/bar/libDesign.a)
    /// </summary>
    public string LinuxCarbonModelName { get; set; }

    /// <summary>
    /// The Fullpath name to the Linux design (i.e. /foo/bar/libDesign.a)
    /// </summary>
    public string WindowsCarbonModelName { get; set; }

    /// <summary>
    /// Base name of design i.e. if it is libDesign.a would be "Design"
    /// </summary>
    public string BaseDesignName { get; set; }

    /// <summary>
    /// Model Kit Answers File
    /// </summary>
    public ModelKitAnswers Answers { get; private set; }

    /// <summary>
    /// CARBON_HOME on remote (linux side) network when running on Windows
    /// </summary>
    public string RemoteCarbonHome { get; set; }

    /// <summary>
    /// Model Kit Home (CARBON_MK_HOME) on remote (linux side) network when running on Windows
    /// </summary>
    public string RemoteCarbonModelKitHome { get; set; }

    /// <summary>
    /// Source direcotry of model kit on remote (linux side) network when running on Windows.
    /// This is where the .csproj file is
    /// </summary>
    public string RemoteModelKitSourceDir { get; set; }

    /// <summary>
    /// If non-null/empty use the Attribute XML file and pass into cbuild
    /// </summary>
    public string AttributeXMLFile { get; set; }

    /// <summary>
    /// remote machine for executing cbuild (linux side) network when running on Windows.
    /// </summary>
    public string RemoteCBuildHost { get; set; }

    /// <summary>
    /// Disable PGO Optimization
    /// </summary>
    public bool NoPGO { get; set; }

    /// <summary>
    /// Do not render IP-XACT, Source files; use existing ones -- debug mode only!
    /// </summary>
    public bool NoRender { get; set; }

    /// <summary>
    /// Do not Execute CBuild::Run (Explorer: will return success)
    /// </summary>
    public bool NoExplore { get; set; }

    /// <summary>
    /// Do not Execute CBuild::Run (Compile: will return success)
    /// </summary>
    public bool NoCompile { get; set; }

    /// <summary>
    /// Return component build directory for one particular platform
    /// </summary>
    /// <param name="platform">A single platform flag</param>
    /// <param name="componentType">The component type</param>
    /// <returns>Full path to the directory which will contain the built model</returns>
    public string PlatformModelDirectory(Model.ModelBuildPlatform platform, Model.ComponentType componentType)
    {
      if ((platform & Model.ModelBuildPlatform.Gcc) != 0)
        return Path.Combine(LinuxModelDirectory, Path.Combine(componentType.ToString(), "gcc"));
      else if ((platform & Model.ModelBuildPlatform.VS2005) != 0)
        return Path.Combine(WindowsModelDirectory, Path.Combine(componentType.ToString(), "vc2005"));
      else if ((platform & Model.ModelBuildPlatform.VS2008) != 0)
        return Path.Combine(WindowsModelDirectory, Path.Combine(componentType.ToString(), "vc2008"));
      else if ((platform & Model.ModelBuildPlatform.VS2010) != 0)
        return Path.Combine(WindowsModelDirectory, Path.Combine(componentType.ToString(), "vc2010"));
      else if ((platform & Model.ModelBuildPlatform.VS2012) != 0)
        return Path.Combine(WindowsModelDirectory, Path.Combine(componentType.ToString(), "vc2012"));

      throw new Exception("Unable to determine PlatformModelDirectory");
    }

    /// <summary>
    /// Determines the VS environment variable name given the VS platform specified.
    /// </summary>
    /// <param name="platform">Flag should contain a single VS field</param>
    /// <returns>The name of the Environment variable used to setup this version</returns>
    public string VisualStudioEnvironmentVariable(Model.ModelBuildPlatform platform)
    {
      if ((platform & Model.ModelBuildPlatform.VS2005) != 0)
        return "VS80COMNTOOLS";
      else if ((platform & Model.ModelBuildPlatform.VS2008) != 0)
        return "VS90COMNTOOLS";
      else if ((platform & Model.ModelBuildPlatform.VS2010) != 0)
        return "VS100COMNTOOLS";
      else if ((platform & Model.ModelBuildPlatform.VS2012) != 0)
        return "VS110COMNTOOLS";

      throw new Exception(string.Format("Unsupported Windows Platform: {0}", platform));
    }

    /// <summary>
    /// Enumerate all platforms specified
    /// </summary>
    /// <param name="platform">A combination of flags</param>
    /// <returns>Yields each flag set in platform</returns>
    public IEnumerable<Model.ModelBuildPlatform> Platforms(Model.ModelBuildPlatform platform)
    {
      foreach (Model.ModelBuildPlatform plat in Enum.GetValues(typeof(Model.ModelBuildPlatform)))
      {
        if (plat == Model.ModelBuildPlatform.All)
          continue;

        if ((platform & plat) != 0)
          yield return plat;
      }
    }

    /// <summary>
    /// The Component Type being built.
    /// </summary>
    public Model.ComponentType ComponentType { get; set; }

    /// <summary>
    /// Construct the ModelKitContext.   Creates all Directories required to build this kit.
    /// </summary>
    /// <param name="modelKitAssemblyDir">The Directory containing the ModelKit Assembly to be loaded</param>
    /// <param name="answersXml">The XML Answers file</param>
    public ModelKitContext(string modelKitAssemblyDir, string answersXml)
    {
      XMLAnswersFilePath = answersXml;

      ModelKitDirectory = modelKitAssemblyDir;
      WorkingDirectory = Environment.CurrentDirectory;

      Logger.Log("Working Directory {0}", WorkingDirectory);

      ModelDataDirectory = Path.Combine(WorkingDirectory, "model", "data");
      LinuxModelDirectory = Path.Combine(WorkingDirectory, "linux");
      WindowsModelDirectory = Path.Combine(WorkingDirectory, "windows");

      Directory.CreateDirectory(ModelDataDirectory);
      if (!Directory.Exists(ModelDataDirectory))
        throw new Exception(string.Format("Error: Unable to create Data Directory: {0}", ModelDataDirectory));

      if (answersXml != null)
      {
        if (!File.Exists(answersXml))
          throw new Exception(string.Format("No Such Answers File: {0}", answersXml));

        Logger.Log("Using answers file {0}", answersXml);
        Answers = new ModelKitAnswers(answersXml);

        ComponentType = Model.ComponentType.Undefined;

        if (Answers.Answers["variant"] == "sa" && Answers.Answers["partner"] == "coware")
          ComponentType = Model.ComponentType.StandAlone;
        else if (Answers.Answers["variant"] == "sd" && Answers.Answers["partner"] == "ip")
          ComponentType = Model.ComponentType.SoCDesigner;

        foreach (Model.ModelBuildPlatform plat in Platforms(Answers.PlatformFlags))
        {
          string dir = PlatformModelDirectory(plat, ComponentType);
          if (!Directory.Exists(dir))
          {
            Logger.Log("Creating Platform Directory: {0}", dir);
            Directory.CreateDirectory(dir);
          }
        }
      }
    }


    /// <summary>
    /// Copy srcFileName into the destination model data directory "data/componenttype/platform", i.e. "data/SoCDesigner/gcc"
    /// </summary>
    /// <param name="srcFileName">The file to be copied (may contain %var% syntax)</param>
    public void CopyAndRetainFile(string srcFileName)
    {
      string srcFile = Environment.ExpandEnvironmentVariables(srcFileName);
      if (!File.Exists(srcFile))
        throw new Exception(string.Format("Error: No such file: {0}", srcFile));

      foreach (Model.ModelBuildPlatform platform in Platforms(Answers.PlatformFlags))
      {
        if (Model.isGcc(platform))
        {
          FileUtilities.CopyFile(srcFile, ComponentPlatformDataDirectory(platform));
        }
      }
    }

    /// <summary>
    ///  Copy srcFileName into the destination model data directory "data/componenttype/platform", i.e. "data/SoCDesigner/gcc"
    ///  and also into the component model directory
    /// </summary>
    /// <param name="srcFileName"></param>
    /// <param name="componentType"></param>
    public void CopyAndRetainFile(string srcFileName, Model.ComponentType componentType)
    {
      string srcFile = Environment.ExpandEnvironmentVariables(srcFileName);
      if (!File.Exists(srcFile))
        throw new Exception(string.Format("Error: No such file: {0}", srcFile));

      CopyAndRetainFile(srcFile);

      foreach (Model.ModelBuildPlatform platform in Platforms(Answers.PlatformFlags))
      {
        if (Model.isGcc(platform))
        {
          FileUtilities.CopyFile(srcFile, PlatformModelDirectory(platform, ComponentType));
        }
      }
    }


    /// <summary>
    ///  return the db file path for the requested platform, returns first .db file found when searching in this order: symtab, io, gui
    /// </summary>
    /// <param name="platform">platform desired</param>
    public string getDBFilePath(Model.CBuildPlatform platform)
    {
      string modelDirectory = String.Empty;
      switch (platform)
      {
        case Model.CBuildPlatform.Linux:
          modelDirectory = LinuxModelDirectory;
          break;
        case Model.CBuildPlatform.Windows:
          modelDirectory = WindowsModelDirectory;
          break;

        default:
          throw new Exception("unsupported platform.");
          break;
      }


      string symtabDb = Path.Combine(modelDirectory, string.Format("lib{0}.symtab.db", BaseDesignName));
      string ioDb = Path.Combine(modelDirectory, string.Format("lib{0}.io.db", BaseDesignName));
      string guiDB = Path.Combine(modelDirectory, string.Format("lib{0}.gui.db", BaseDesignName));

      if (File.Exists(symtabDb))
        return symtabDb;

      if (File.Exists(ioDb))
        return ioDb;

      if (File.Exists(guiDB))
        return guiDB;

      throw new Exception(string.Format("unable to find symtab.db, io.db or gui.db for {0}.", BaseDesignName));
    }

  }
}
