﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Carbon.ModelKitServices
{
 /// <summary>
 /// Specifies the required PGO information for this ModelKit
 /// </summary>
  public class ModelKitPGO
  {
    /// <summary>
    /// The Script used to generate the ModelKit PGO 
    /// <value>Default value is ${MODELKITS_ROOT}/Scripts/runclkstb.sh</value>
    /// </summary>
    public string Script { get; set; }
    /// <summary>
    /// The argument to be passed to the PGO script, usually the .cxx filename
    /// </summary>
    public string Argument { get; set; }

    /// <summary>
    /// Construct a default PGO object
    /// </summary>
    /// <param name="context">The ModelKitContext</param>
    public ModelKitPGO(ModelKitContext context)
    {
      Script = Path.Combine(Environment.ExpandEnvironmentVariables("%MODELKITS_ROOT%"), "Scripts", "runclkstb.sh");
      Argument = String.Empty;
    }

    /// <summary>
    /// Construct a PGO object specifiying the PGO test driver program source filename.
    /// </summary>
    /// <param name="context">The ModelKitContext</param>
    /// <param name="pgoFileName">The Source file i.e. pl190.cxx</param>
    public ModelKitPGO(ModelKitContext context, string pgoFileName)
    {
      Script = Path.Combine(Environment.ExpandEnvironmentVariables("%MODELKITS_ROOT%"), "Scripts", "runclkstb.sh");
      Argument = pgoFileName;
    }
  }

}
