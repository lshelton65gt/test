﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using log4net;
using log4net.Config;
using log4net.Appender;
using log4net.Repository.Hierarchy;
using log4net.Core;
using log4net.Layout;

namespace Carbon.ModelKitServices
{
  /// <summary>
  /// The Colorized Logging service
  /// </summary>
  public class Logger
  {
    private static log4net.ILog _log = LogManager.GetLogger(typeof(Logger));
    private static log4net.Core.Level cbuildLevel = new log4net.Core.Level(50000, "CBuild");
    private static bool enableOutputLog = true;

    private static PatternLayout _layout = new PatternLayout();
    private const string LOG_PATTERN = "%d %level %logger - %message%newline";


    public string DefaultPattern
    {
      get { return LOG_PATTERN; }
    }

    public PatternLayout DefaultLayout
    {
      get { return _layout; }
    }

    public void AddAppender(IAppender appender)
    {
      Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

      hierarchy.Root.AddAppender(appender);
    }

    /// <summary>
    /// Disable the output logger
    /// </summary>
    public static void DisableOutputLog()
    {
      enableOutputLog = false;
    }

    public Logger()
    {
      _layout.ConversionPattern = DefaultPattern;
      _layout.ActivateOptions();
    }

    static Logger()
    {
      log4net.LogManager.GetRepository().LevelMap.Add(cbuildLevel);


      Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();
      TraceAppender tracer = new TraceAppender();
      PatternLayout patternLayout = new PatternLayout();

      patternLayout.ConversionPattern = LOG_PATTERN;
      patternLayout.ActivateOptions();

      tracer.Layout = patternLayout;
      tracer.ActivateOptions();
      hierarchy.Root.AddAppender(tracer);
#if WIN32
      // Windows Native
      if (!Utilities.IsUnix && !Utilities.IsMono)
      {
        ColoredConsoleAppender cc = new ColoredConsoleAppender();
        cc.Layout = patternLayout;

        cc.AddMapping(new ColoredConsoleAppender.LevelColors() { Level = cbuildLevel, BackColor = ColoredConsoleAppender.Colors.White, ForeColor = ColoredConsoleAppender.Colors.Purple });
        cc.AddMapping(new ColoredConsoleAppender.LevelColors() { Level = Level.Info, ForeColor = ColoredConsoleAppender.Colors.Cyan });
        cc.AddMapping(new ColoredConsoleAppender.LevelColors() { Level = Level.Error, ForeColor = ColoredConsoleAppender.Colors.HighIntensity | ColoredConsoleAppender.Colors.Red });
        cc.AddMapping(new ColoredConsoleAppender.LevelColors() { Level = Level.Debug, BackColor = ColoredConsoleAppender.Colors.White, ForeColor = ColoredConsoleAppender.Colors.Green });

        cc.ActivateOptions();
        hierarchy.Root.AddAppender(cc);
      }
#endif
      // Linux/Mono
      if (Utilities.IsUnix)
      {
        AnsiColorTerminalAppender ansi = new AnsiColorTerminalAppender();

        PatternLayout ansiLayout = new PatternLayout();
        ansiLayout.ConversionPattern = LOG_PATTERN;
        ansi.Layout = ansiLayout;

        ansi.AddMapping(new AnsiColorTerminalAppender.LevelColors() { Level = cbuildLevel, BackColor = AnsiColorTerminalAppender.AnsiColor.White, ForeColor = AnsiColorTerminalAppender.AnsiColor.Magenta });
        ansi.AddMapping(new AnsiColorTerminalAppender.LevelColors() { Level = Level.Error, BackColor = AnsiColorTerminalAppender.AnsiColor.White, ForeColor = AnsiColorTerminalAppender.AnsiColor.Red, Attributes = AnsiColorTerminalAppender.AnsiAttributes.Bright });
        ansi.AddMapping(new AnsiColorTerminalAppender.LevelColors() { Level = Level.Debug, BackColor = AnsiColorTerminalAppender.AnsiColor.White, ForeColor = AnsiColorTerminalAppender.AnsiColor.Cyan });
        ansi.AddMapping(new AnsiColorTerminalAppender.LevelColors() { Level = Level.Info, BackColor = AnsiColorTerminalAppender.AnsiColor.White, ForeColor = AnsiColorTerminalAppender.AnsiColor.Blue });

        ansi.ActivateOptions();
        hierarchy.Root.AddAppender(ansi);
      }

      // Log File
      FileAppender fa = new FileAppender();
      fa.AppendToFile = false;
      fa.File = Path.Combine(Directory.GetCurrentDirectory(), "ModelKit.log");
      fa.ImmediateFlush = true;
      fa.ActivateOptions();

      PatternLayout fileLayout = new PatternLayout();
      fileLayout.ConversionPattern = "%d %level %logger - %message%newline";
      fa.Layout = fileLayout;

      Console.WriteLine("Log File Location: {0}", fa.File);

      hierarchy.Root.AddAppender(fa);

      hierarchy.Root.Level = Level.All;
      hierarchy.Configured = true;

      setupOutputLog();
    }

    private static void setupOutputLog()
    {
      if (enableOutputLog)
      {
        //foreach (log4net.Appender.IAppender appender in log4net.LogManager.GetRepository().GetAppenders())
        //{
        //  if (appender is log4net.Appender.FileAppender)
        //  {
        //    log4net.Appender.FileAppender fa = appender as log4net.Appender.FileAppender;

        //    string currDir = Directory.GetCurrentDirectory();

        //    string logLocation = Path.Combine(currDir, "ModelKit.log");
        //    Console.WriteLine("Log File Location: {0}", logLocation);

        //    fa.File = logLocation;
        //    fa.ActivateOptions();
        //    break;
        //  }
        //}
      }
    }

    /// <summary>
    /// Specifi a new Logger object
    /// </summary>
    /// <param name="log"></param>
    public static void SetLogger(log4net.ILog log)
    {
      _log = log;
    }

    /// <summary>
    /// Log message output from cbuild
    /// </summary>
    /// <param name="message"></param>
    public static void CBuild(string message)
    {
      _log.Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,
          cbuildLevel, message, null);
    }

    /// <summary>
    /// Log formatted cbuild message
    /// </summary>
    /// <param name="message">message format specification</param>
    /// <param name="args">arguments to format</param>
    public static void CBuildFormat(string message, params object[] args)
    {
      string formattedMessage = string.Format(message, args);
      _log.Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,
          cbuildLevel, formattedMessage, null);
    }

    /// <summary>
    /// Log any object
    /// </summary>
    /// <param name="o">The object's .ToString() method will be used</param>
    public static void Log(object o)
    {
      Log("{0}", o);
    }

    /// <summary>
    /// Log a formatted message 
    /// </summary>
    /// <param name="format">Format specification</param>
    /// <param name="args">Arguments to format</param>
    public static void Log(string format, params object[] args)
    {
      string line = format;
      if (args.Length > 0)
        line = string.Format(format, args);
      _log.Info(line);
    }

    /// <summary>
    /// Log a Debug level message 
    /// </summary>
    /// <param name="o">Objects .ToString() will be used</param>
    public static void Debug(object o)
    {
      Debug("{0}", o);
    }

    /// <summary>
    /// Log a Debug level formatted message 
    /// </summary>
    /// <param name="format">Format specification</param>
    /// <param name="args">Arguments to format</param>
    public static void Debug(string format, params object[] args)
    {
      string line = format;
      if (args.Length > 0)
        line = string.Format(format, args);
      _log.Debug(line);
    }


    /// <summary>
    /// Log a Warning level Object
    /// </summary>
    /// <param name="o">Objects .ToString() will be used</param>
    public static void Warning(object o)
    {
      Warning("{0}", o);
    }

    /// <summary>
    /// Log a Warning level formatted message 
    /// </summary>
    /// <param name="format">Format specification</param>
    /// <param name="args">Arguments to format</param>
    public static void Warning(string format, params object[] args)
    {
      string line = format;
      if (args.Length > 0)
        line = string.Format(format, args);
      _log.Warn(line);
    }


    /// <summary>
    /// Log a Fatal level Object
    /// </summary>
    /// <param name="o">Objects .ToString() will be used</param>
    public static void Fatal(object o)
    {
      Fatal("{0}", o);
    }

    /// <summary>
    /// Log a Fatal level formatted message 
    /// </summary>
    /// <param name="format">Format specification</param>
    /// <param name="args">Arguments to format</param>
    public static void Fatal(string format, params object[] args)
    {
      string line = format;
      if (args.Length > 0)
        line = string.Format(format, args);
      _log.Fatal(line);
    }

    /// <summary>
    /// Log a Error level Object
    /// </summary>
    /// <param name="o">Objects .ToString() will be used</param> 
    public static void Error(object o)
    {
      Error("{0}", o);
    }

    /// <summary>
    /// Log an Error level formatted message 
    /// </summary>
    /// <param name="format">Format specification</param>
    /// <param name="args">Arguments to format</param>
    public static void Error(string format, params object[] args)
    {
      _log.ErrorFormat(format, args);
    }
  }
}
