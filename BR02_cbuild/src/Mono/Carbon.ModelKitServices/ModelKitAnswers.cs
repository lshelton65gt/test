﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace Carbon.ModelKitServices
{
  /// <summary>
  /// A collection of Name,Value pairs of answers from an .xml answers file
  /// </summary>
  public class AnswerCollection : Dictionary<string, string>
  {
    /// <summary>
    /// The name of the collection, default is "Answers"
    /// </summary>
    public string CollectionName { get; set; }
  }

  /// <summary>
  /// Represents the XML answers file from the portal
  /// </summary>
  public class ModelKitAnswers
  {
    /// <summary>
    /// The top-level Answers from a Portal XML answers file (see request.answers) from
    /// the Portal website
    /// </summary>
    public AnswerCollection Answers { get; private set; }

    /// <summary>
    /// The Value of the id='componentName' answer
    /// </summary>
    public string ComponentName { get; set; }
    /// <summary>
    /// The Value of the id='configurationName' answer
    /// </summary>
    public string ConfigurationName { get; set; }
    /// <summary>
    /// The version of CMS to use to build this configuration, of the form:
    /// X.Y.N (from the id='cmsVersion') answer
    /// </summary>
    public string CMSVersion { get; private set; }
    /// <summary>
    /// The version SoC Designer to use to build this configuration, of the form:
    /// X.Y.N (from the id='socdVersion') answer
    /// </summary>
    public string SoCDVersion { get; private set; }
    /// <summary>
    /// The version of MK to use to build this configuration, of the form:
    /// X.Y.N (from the id='mkVersion') answer
    /// </summary>
    public string MKVersion { get; private set; }
    /// <summary>
    /// The Variant from id='variant'
    /// </summary>
    public string Variant { get; set; }
    /// <summary>
    /// The Platform from id='platform'
    /// </summary>
    public string Platform { get; set; }
    /// <summary>
    /// The Partner from id='partner'
    /// </summary>
    public string Partner { get; set; }
    /// <summary>
    /// The RxPy answer value (if present, otherwise empty string)
    /// </summary>
    public string RxPy { get; set; }
    /// <summary>
    /// The Flags indicating what platforms are to be built
    /// </summary>
    public Model.ModelBuildPlatform PlatformFlags { get; set; }
    /// <summary>
    /// For Multi-core questions files, the collection of answers for each sub-core
    /// </summary>
    public List<AnswerCollection> Groups { get; private set; }

    private string Lookup(string answer)
    {
      if (Answers.ContainsKey(answer))
        return Answers[answer];
      else
        return null;
    }

    /// <summary>
    /// Construct a ModelKitAnswers object from a portal XML answers file
    /// </summary>
    /// <param name="xmlFile">Path to a XML answers file to be parsed.</param>
    public ModelKitAnswers(string xmlFile)
    {
      Answers = new AnswerCollection();
      Groups = new List<AnswerCollection>();

      if (File.Exists(xmlFile))
      {
        XmlDocument xd = new XmlDocument();
        xd.Load(xmlFile);

        // Top level answers
        foreach (XmlNode answer in xd.DocumentElement.SelectNodes("//Answers/A"))
        {
          string id = answer.Attributes["id"].Value;
          string value = answer.Attributes["value"].Value;
          Answers[id] = value;
        }

        foreach (XmlNode group in xd.DocumentElement.SelectNodes("//Answers/Group"))
        {
          string id = group.Attributes["id"].Value;
          int index = int.Parse(group.Attributes["index"].Value) - 1;

          if (Groups.Count < index + 1)
            Groups.Add(new AnswerCollection());

          AnswerCollection answers = Groups[index];
          answers.CollectionName = id;

          foreach (XmlNode answer in group.SelectNodes("A"))
          {
            string _id = answer.Attributes["id"].Value;
            string _value = answer.Attributes["value"].Value;
            answers[_id] = _value;
          }
        }

        if (Answers.ContainsKey("rxpyLevel"))
          RxPy = Answers["rxpyLevel"];

        if (Answers.ContainsKey("cmsVersion"))
          CMSVersion = Answers["cmsVersion"];

        if (Answers.ContainsKey("socdVersion"))
          SoCDVersion = Answers["socdVersion"];

        if (Answers.ContainsKey("variant"))
          Variant = Answers["variant"];

        if (Answers.ContainsKey("platform"))
          Platform = Answers["platform"];

        if (Answers.ContainsKey("partner"))
          Partner = Answers["partner"];

        if (Answers.ContainsKey("componentName"))
          ComponentName = Answers["componentName"];

        if (Answers.ContainsKey("configurationName"))
          ConfigurationName = Answers["configurationName"];

        if (!string.IsNullOrEmpty(Platform))
        {
          switch (Platform.ToLower())
          {
            case "all": PlatformFlags = Model.ModelBuildPlatform.All; break;
            case "gcc": PlatformFlags |= Model.ModelBuildPlatform.Gcc; break;
            case "vc2005": PlatformFlags |= Model.ModelBuildPlatform.VS2005; break;
            case "vc2008": PlatformFlags |= Model.ModelBuildPlatform.VS2008; break;
            case "vc2010": PlatformFlags |= Model.ModelBuildPlatform.VS2010; break;
            case "vc2012": PlatformFlags |= Model.ModelBuildPlatform.VS2012; break;
          }
        }

        if (string.IsNullOrEmpty(Platform))
          throw new Exception(string.Format("Error: Expecting 'platform' from answers file: {0}", xmlFile));

        if (string.IsNullOrEmpty(Variant))
          throw new Exception(string.Format("Error: Expecting 'variant' from answers file: {0}", xmlFile));
      }
    }
  }
}
