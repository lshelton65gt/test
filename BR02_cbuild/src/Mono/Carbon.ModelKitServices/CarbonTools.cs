using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

using Carbon.CarbonDB;
using Carbon.Ccfg;
using Carbon.ModelKitServices;

namespace Carbon.ModelKitServices.Tools
{
  /// <summary>
  /// Class to indicate an exception has occurred during cbuild compilation
  /// </summary>
  public class CompileException : System.Exception
  {
    /// <summary>
    /// Construct a compilation exception
    /// </summary>CBui
    public CompileException()
    {
    }

    /// <summary>
    /// Construct a CompileException
    /// </summary>
    /// <param name="message">The error message</param>
    public CompileException(string message)
      : base(message)
    {
    }
  }

  public class MappedDriveResolver
  {

    /// <summary>Resolves the given path to a full UNC path, or full local drive path.</summary>
    /// <param name="pPath"></param>
    /// <returns></returns>
    static public string ResolveToUNC(string pPath)
    {
#if WIN32

      if (pPath.StartsWith(@"\\")) { return pPath; }

      string root = ResolveToRootUNC(pPath);

      if (pPath.StartsWith(root))
      {
        return pPath; // Local drive, no resolving occurred
      }
      else
      {
        return pPath.Replace(GetDriveLetter(pPath), root);
      }
#else
      return pPath;
#endif
    }

    /// <summary>Resolves the given path to a root UNC path, or root local drive path.</summary>
    /// <param name="pPath"></param>
    /// <returns>\\server\share OR C:\</returns>
    static public string ResolveToRootUNC(string pPath)
    {
#if WIN32
      System.Management.ManagementObject mo = new System.Management.ManagementObject();

      if (pPath.StartsWith(@"\\")) { return Directory.GetDirectoryRoot(pPath); }

      // Get just the drive letter for WMI call
      string driveletter = GetDriveLetter(pPath);

      mo.Path = new System.Management.ManagementPath(string.Format("Win32_LogicalDisk='{0}'", driveletter));

      // Get the data we need
      uint DriveType = Convert.ToUInt32(mo["DriveType"]);
      string NetworkRoot = Convert.ToString(mo["ProviderName"]);
      mo = null;

      // Return the root UNC path if network drive, otherwise return the root path to the local drive
      if (DriveType == 4)
      {
        return NetworkRoot;
      }
      else
      {
        return driveletter + Path.DirectorySeparatorChar;
      }

#else
      return pPath;
#endif

    }

    /// <summary>Checks if the given path is on a network drive.</summary>
    /// <param name="pPath"></param>
    /// <returns></returns>
    static public bool isNetworkDrive(string pPath)
    {
#if WIN32
      System.Management.ManagementObject mo = new System.Management.ManagementObject();

      if (pPath.StartsWith(@"\\")) { return true; }

      // Get just the drive letter for WMI call
      string driveletter = GetDriveLetter(pPath);

      mo.Path = new System.Management.ManagementPath(string.Format("Win32_LogicalDisk='{0}'", driveletter));

      // Get the data we need
      uint DriveType = Convert.ToUInt32(mo["DriveType"]);
      mo = null;

      return DriveType == 4;
#else
      return false;
#endif
    }

    /// <summary>Given a path will extract just the drive letter with volume separator.</summary>
    /// <param name="pPath"></param>
    /// <returns>C:</returns>
    static public string GetDriveLetter(string pPath)
    {
      if (pPath.StartsWith(@"\\")) { throw new ArgumentException("A UNC path was passed to GetDriveLetter"); }
      return Directory.GetDirectoryRoot(pPath).Replace(Path.DirectorySeparatorChar.ToString(), "");
    }

  }

  public class Pair<T, U>
  {
    public Pair()
    {
    }

    public Pair(T first, U second)
    {
      this.First = first;
      this.Second = second;
    }

    public T First { get; set; }
    public U Second { get; set; }
  };
  /// <summary>
  /// File helper utility class
  /// </summary>
  public class FileUtilities
  {
    public static string MapWindowsPathToLinux(string path)
    {
      if (Utilities.IsMono) { return path; }

      if (!MappedDriveResolver.isNetworkDrive(path))
      {
        throw new Exception(String.Format("{0} is a local drive, network path required", path));
      }

      List<Pair<string, string>> driveMappings = new List<Pair<string, string>>();

      driveMappings.Add(new Pair<string, string>(@"\\dfs\acton\homes", "/home/cds"));
      driveMappings.Add(new Pair<string, string>(@"\\dfs\acton\home", "/home/cds"));
      driveMappings.Add(new Pair<string, string>(@"\\dfs\acton\release", "/o/release"));
      driveMappings.Add(new Pair<string, string>(@"\\dfs\acton\tools", "/o/tools"));
      driveMappings.Add(new Pair<string, string>(@"\\dfs\acton\work", "/o/work"));
      driveMappings.Add(new Pair<string, string>(@"\\arm\home", "/home"));
      driveMappings.Add(new Pair<string, string>(@"\\arm\arm_release_data", "/usr/ARM_RELEASE_DATA"));

      string unc = MappedDriveResolver.ResolveToUNC(path);

      var mapping = driveMappings.Find(d => unc.StartsWith(d.First));

      if (mapping == null)
      {
        // do regex check
        char[] seps = { '\\' };
        string[] dirs = unc.Split(seps, StringSplitOptions.RemoveEmptyEntries);
        if (dirs[1] == "w")
        {
          // work drive
          dirs[1] = dirs[0];
          dirs[0] = "w";
          unc = "/" + string.Join("/", dirs);
          return unc;
        }
        else
        {
          throw new Exception(string.Format("Cannot local path {0} to network path", path));
        }

      }

      unc = unc.Replace(mapping.First, mapping.Second).Replace("\\", "/");
      return unc;
    }


    public static string MakeRelativePath(string absolutePath, string relativeToDirectory)
    {
      DirectoryInfo fi = new DirectoryInfo(relativeToDirectory);
      relativeToDirectory = fi.FullName + Path.DirectorySeparatorChar;

      System.Uri uri1 = new Uri(absolutePath);
      System.Uri uri2 = new Uri(relativeToDirectory);

      Uri relativeUri = uri2.MakeRelativeUri(uri1);

      return (relativeUri.ToString());
    }

    /// <summary>
    /// Copy a file from source to newname in destination, will overwrite file if necessary.
    /// </summary>
    /// <param name="srcName">The input source file name</param>
    /// <param name="destName">The destination source file name</param>
    /// <returns>true indicates success, false indicates failure</returns>
    public static bool CopyFileAs(string srcName, string destName)
    {
      File.Copy(srcName, destName, true);
      bool result = File.Exists(destName);
      Logger.Log("{0}Copy file from {1} to {2}", result ? "" : "ERROR: Failed to ", srcName, destName);
      return result;
    }

    /// <summary>
    /// Copy file <paramref name="srcName"/> to destination directory <paramref name="destDir"/>
    /// </summary>
    /// <param name="srcName">The full input source file name</param>
    /// <param name="destDir">The directory to copy the source file into</param>
    /// <returns>true indicates success, false indicates failed to copy</returns>
    public static bool CopyFile(string srcName, string destDir)
    {
      FileInfo fi = new FileInfo(srcName);
      if (!Directory.Exists(destDir))
        Directory.CreateDirectory(destDir);
      string destName = Path.Combine(destDir, fi.Name);
      return CopyFileAs(srcName, destName);
    }

    /// <summary>
    /// Copy a directory and all files, optionally recursive
    /// </summary>
    /// <param name="sourceDirName">Source directory</param>
    /// <param name="destDirName">Destination directory (will be created if necessary)</param>
    /// <param name="copySubDirs">Recursive or not</param>
    public static void CopyDirectory(string sourceDirName, string destDirName, bool copySubDirs)
    {
      DirectoryInfo dir = new DirectoryInfo(sourceDirName);
      DirectoryInfo[] dirs = dir.GetDirectories();

      // If the source directory does not exist, throw an exception.
      if (!dir.Exists)
      {
        throw new DirectoryNotFoundException(
            "Source directory does not exist or could not be found: "
            + sourceDirName);
      }

      // If the destination directory does not exist, create it.
      if (!Directory.Exists(destDirName))
      {
        Directory.CreateDirectory(destDirName);
      }

      // Get the file contents of the directory to copy.
      FileInfo[] files = dir.GetFiles();

      foreach (FileInfo file in files)
      {
        // Create the path to the new copy of the file.
        string temppath = Path.Combine(destDirName, file.Name);

        // Copy the file.
        Logger.Log("Copying file: {0} to {1}", file.FullName, temppath);
        file.CopyTo(temppath, true);
      }

      // If copySubDirs is true, copy the subdirectories.
      if (copySubDirs)
      {
        foreach (DirectoryInfo subdir in dirs)
        {
          // Create the subdirectory.
          string temppath = Path.Combine(destDirName, subdir.Name);

          // Copy the subdirectories.
          CopyDirectory(subdir.FullName, temppath, copySubDirs);
        }
      }
    }
  }
  /// <summary>
  /// The cbuild tool interface
  /// </summary>
  public class CBuild
  {
    /// <summary>
    /// Enums for CBuild message numbers
    /// </summary>
    public enum CBuildMessages
    {
      /// <summary>
      /// Dead net
      /// </summary>
      DeadNet = 103,
      /// <summary>
      /// Generate Asigned Name
      /// </summary>
      GenerateAssignedName = 3146,
      /// <summary>
      /// Unused Task
      /// </summary>
      TaskUnused = 4038,
      /// <summary>
      /// Cannot identify Unelaborated Symbol
      /// </summary>
      CannotIdUnelabSymbol = 6016,
      /// <summary>
      /// No Connection to Port
      /// </summary>
      NoConnectionToPort = 20034
    }

    public class CBuildOptions
    {
      private List<string> vlogSourceFiles = new List<string>();
      private List<string> vlogIncludePaths = new List<string>();
      private Dictionary<string, string> _topLevelParameters = new Dictionary<string, string>();

      /// <summary>
      /// Directives
      /// </summary>
      public CBuild.CBuildDirectives Directives
      {
        get;
        set;
      }

      /// <summary>
      /// -v2k
      /// </summary>
      public bool Verilog2001
      {
        get;
        set;
      }

      /// <summary>
      /// Ctor
      /// </summary>
      public CBuildOptions()
      {
        Directives = null;
        Verilog2001 = true;
      }

      /// <summary>
      /// Add Verilog files
      /// </summary>
      /// <param name="sourcePath"></param>
      public void AddVerilogSource(string sourcePath)
      {
        if (vlogSourceFiles.Contains(sourcePath))
        {
          Logger.Warning("{0} is already in the list of source files.  Skipping duplicate entry", sourcePath);
          return;
        }

        vlogSourceFiles.Add(sourcePath);
      }

      /// <summary>
      /// Adds the verilog sources to the cbuild command line.  cbuild doesn't like having duplicate
      /// entries in it's source list (it'll report multiple module definitions) so duplicates are removed.
      ///
      /// The function adds all the files then cleans up which is suboptimal from the standpoint of managing the
      /// list, but makes it easier to generate interesting messages to the user.
      /// </summary>
      /// <param name='sources'>
      /// Sources.
      /// </param>
      public void AddVerilogSources(IEnumerable<string> sources)
      {
        vlogSourceFiles.AddRange(sources);

        List<string> dupes = vlogSourceFiles.GroupBy(x => x)
          .Where(g => g.Count() > 1)
            .Select(g => g.Key)
            .ToList();

        Logger.Warning("Removing Duplicate file entries from source list:");
        foreach (string s in dupes)
        {
          Logger.Warning("\t{0}", s);
        }
        Logger.Warning("");

        vlogSourceFiles = vlogSourceFiles.Distinct().ToList();
      }

      public void AddVerilogIncludePath(string includePath)
      {
        if (vlogIncludePaths.Contains(includePath))
        {
          Logger.Warning("{0} is already in the list of include paths", includePath);
          return;
        }
        vlogIncludePaths.Add(includePath);
      }

      public void AddVerilogIncludePaths(IEnumerable<string> paths)
      {
        foreach (string path in paths)
        {
          AddVerilogIncludePath(path);
        }
      }

      public void SetTopLevelParameter(string parameterName, string parameterValue)
      {
        _topLevelParameters[parameterName] = parameterValue;
      }

      public string TopModule
      {
        get;
        set;
      }

      /// <summary>
      /// Generates the command line based on current settings and returns it as a string
      /// </summary>
      /// <value>
      /// The command line.
      /// </value>
      public string CommandLine
      {
        get
        {
          List<string> options = new List<string>();
          options.Add("-phaseStats"); // need to turn this into a property

          //      options.AddRange (vlogSourceFiles);
          foreach (string fname in vlogSourceFiles)
          {
            options.Add(FileUtilities.MapWindowsPathToLinux(fname));
          }

          foreach (string incdir in vlogIncludePaths)
          {
            options.Add(string.Format("+incdir+{0}", FileUtilities.MapWindowsPathToLinux(incdir)));

          }

          if (Verilog2001)
            options.Add("-2001");

          if (!string.IsNullOrEmpty(TopModule))
          {
            options.Add(string.Format("-vlogTop {0}", TopModule));
          }

          // Fixme: parameters need to be processed to escape character for shell safety
          foreach (string param in _topLevelParameters.Keys)
          {
            options.Add(string.Format("-topLevelParam {0}={1}", param, _topLevelParameters[param]));
          }

          if (Directives != null)
          {
            Directives.WriteFile();
            options.Add(string.Format("-directive {0}", FileUtilities.MapWindowsPathToLinux(Directives.FileName)));
          }

          return string.Join("\n", options);
        }
      }
    }


    public class CBuildDirectives
    {
      private List<string> _silentMessages = new List<string>();
      private List<string> _warningMessages = new List<string>();

      private string _directory;
      private const string _directivesFileName = "modelkit.directives";
      private Dictionary<string, string> _observablePaths = new Dictionary<string, string>();
      private Dictionary<string, string> _depositablePaths = new Dictionary<string, string>();
      private Dictionary<string, string> _forciblePaths = new Dictionary<string, string>();
      public void Clear()
      {
        _silentMessages.Clear();
        _warningMessages.Clear();
      }

      public CBuildDirectives(ModelKitContext ctx)
      {
        _directory = ctx.ModelKitDirectory;
      }

      public void WarningMessage(CBuildMessages msg)
      {
        WarningMessage((int)msg);
      }

      public void WarningMessage(int id)
      {
        _warningMessages.Add(id.ToString());
      }

      public void SilentMessage(CBuildMessages msg)
      {
        SilentMessage((int)msg);
      }

      public void SilentMessage(int id)
      {
        _silentMessages.Add(id.ToString());
      }

      public string Directives
      {
        get
        {
          StringBuilder directiveBuilder = new StringBuilder();
          if (_silentMessages.Count != 0)
          {
            directiveBuilder.AppendFormat("silentMsg {0}\n", string.Join(" ", _silentMessages));
          }

          foreach (string rtlPath in _observablePaths.Keys)
          {
            directiveBuilder.AppendFormat("observeSignal {0}\n", rtlPath);
          }

          foreach (string rtlPath in _depositablePaths.Keys)
          {
            directiveBuilder.AppendFormat("depositSignal {0}\n", rtlPath);
          }

          foreach (string rtlPath in _forciblePaths.Keys)
          {
            directiveBuilder.AppendFormat("forceSignal {0}\n", rtlPath);
          }

          return directiveBuilder.ToString();
        }

      }

      public void WriteFile()
      {
        // this using statement forces the file clases to be garbage
        // collected with the using scope ends.  That way there's no
        // resource issues down the road when we open the file again
        using (TextWriter tw = new StreamWriter(FileName))
        {
          tw.Write(Directives);
        }
      }

      public string FileName
      {
        get
        {
          return Path.Combine(_directory, _directivesFileName);
        }

      }

      public void MarkNetObservable(string rtlPath)
      {
        _observablePaths[rtlPath] = rtlPath;
      }

      public void MarkNetObservable(IEnumerable<string> rtlPaths)
      {
        foreach (string p in rtlPaths)
        {
          _observablePaths[p] = p;
        }
      }

      public void MarkNetDepositable(string rtlPath)
      {
        _depositablePaths[rtlPath] = rtlPath;
      }

      public void MarkNetDepositable(IEnumerable<string> rtlPaths)
      {
        foreach (string p in rtlPaths)
        {
          _depositablePaths[p] = p;
        }
      }

      public void MarkNetForcible(string rtlPath)
      {
        _forciblePaths[rtlPath] = rtlPath;
      }

      public void MarkNetForcible(IEnumerable<string> rtlPaths)
      {
        foreach (string p in rtlPaths)
        {
          _forciblePaths[p] = p;
        }
      }
    }

    /// <summary>
    /// Environment Variables to be added to process/remote process execution
    /// </summary>
    public static Dictionary<string, string> EnvironmentVariables = new Dictionary<string, string>();

    enum PlatformType
    {
      Linux,
      Windows
    };
    /// <summary>
    /// Execute a cbuild
    /// </summary>
    /// <param name="buildType">The type of compilation requested</param>
    /// <param name="modelKit">The ModelKit Interface</param>
    /// <param name="ctx">The ModelKit Context</param>
    /// <param name="platform">Platform mask (may contain or'd combination)</param>
    /// <param name="baseDesignName">Base design name i.e. "design" (do not specifiy libDesign or libDesign.a)</param>
    /// <param name="cbuildOptions">All Parameters as a multi-line string to be passed into cbuild</param>
    /// <returns></returns>
    public static int Run(Model.CbuildType buildType, IModelKit modelKit, ModelKitContext ctx, Model.ModelBuildPlatform platform, string baseDesignName, string cbuildOptions)
    {
      // No Explorer compile Skip?
      if (buildType == Model.CbuildType.Explore && ctx.NoExplore)
      {
        Logger.Log("Skipping CBuild({0}) step user requested -noExplore", buildType);
        return 0; // Success
      }
      // No Compile|CompileNoPGO Skip?
      if ((buildType == Model.CbuildType.Compile || buildType == Model.CbuildType.CompileNoPGO) && ctx.NoCompile)
      {
        Logger.Log("Skipping CBuild({0})step user requested -noCompile", buildType);
        return 0; // Success
      }

      if (modelKit != null && !string.IsNullOrEmpty(modelKit.RuntimeLicenseFeature))
      {
        cbuildOptions += string.Format("\n-allowRuntimeLicense {0}\n", modelKit.RuntimeLicenseFeature);
      }

      // If we are passing an attribute file, hand that to cbuild
      if (!string.IsNullOrEmpty(ctx.AttributeXMLFile))
      {
        Logger.Log("Using Attribute File: {0}", FileUtilities.MapWindowsPathToLinux(ctx.AttributeXMLFile));
        cbuildOptions += string.Format(" -attributeFile {0}\n", FileUtilities.MapWindowsPathToLinux(ctx.AttributeXMLFile));
      }

      // Check for CARBON_MODELKIT_DEBUG, if here, do an observe *.*
      if (Environment.GetEnvironmentVariable("CARBON_MODELKIT_DEBUG") != null)
      {
        // Check for CARBON_MODELKIT_DEBUG_LITE, if here, do nothing (just allow the fullDB which is default) 
	if (Environment.GetEnvironmentVariable("CARBON_MODELKIT_DEBUG_LITE") == null)
	{	
	  string observeAllDirFile = Path.Combine(ctx.ModelKitDirectory, "observeall.dir");
          File.WriteAllText(observeAllDirFile, "observeSignal *.*");
          cbuildOptions += string.Format("\n-directive {0}\n", observeAllDirFile);
	} else {
	  cbuildOptions += "-enableOutputSysTasks\n";
	}
      }
      else
        cbuildOptions += "-noFullDB\n";

      string compilationOptions = Environment.ExpandEnvironmentVariables(cbuildOptions);

      string cbuild = @"%CARBON_HOME%/bin/cbuild";

      ctx.BaseDesignName = baseDesignName;

      try
      {
        // Unix?
        if (Model.isLinux(platform))
        {
          Logger.Log("Compiling for Linux");

          string subDir = ctx.LinuxModelDirectory;

          if (!Directory.CreateDirectory(subDir).Exists)
            throw new Exception(string.Format("Error: Unable to create directory: {0}", subDir));

          string modelname = string.Format("lib{0}.a", baseDesignName);

          compileModelForPlatform(modelKit, PlatformType.Linux, buildType, ctx, compilationOptions, cbuild, subDir, modelname);
        }


        if (Model.isWindows(platform))
        {
          Logger.Log("Compiling for Windows");

          string subDir = ctx.WindowsModelDirectory;

          if (!Directory.CreateDirectory(subDir).Exists)
            throw new Exception(string.Format("Error: Unable to create directory: {0}", subDir));

          string modelname = string.Format("lib{0}.lib", baseDesignName);

          compileModelForPlatform(modelKit, PlatformType.Windows, buildType, ctx, compilationOptions, cbuild, subDir, modelname);
        }
      }
      catch (Exception ex)
      {
        Logger.Error(ex.Message);
        return 1;
      }

      return 0;
    }

    private static void compileModelForPlatform(IModelKit modelKit, PlatformType platformType, Model.CbuildType buildType, ModelKitContext ctx, string compilationOptions, string cbuild, string subDir, string modelname)
    {
      string modelDir = string.Empty;
      string profilePlatformType = string.Empty;
      switch (platformType)
      {
        case PlatformType.Linux:
          modelDir = ctx.LinuxModelDirectory;
          profilePlatformType = "Unix";
          break;
        case PlatformType.Windows:
          modelDir = ctx.WindowsModelDirectory;
          profilePlatformType = "Windows";
          break;
      }

      // Force -noPGO
      if (buildType == Model.CbuildType.Compile && ctx.NoPGO)
      {
        Logger.Log("Skipping PGO Optimization");
        buildType = Model.CbuildType.CompileNoPGO;
      }

      if (buildType == Model.CbuildType.Compile)
      {
        // Part 1 - cbuild -profileGenerate
        createOptionsFile(string.Format("-profileGenerate\n{0}", compilationOptions), modelname, subDir, "cbuild_profilegen.f");
        Carbon.ModelKitServices.ProcessRunner.RunProgram rp1 = new ProcessRunner.RunProgram(EnvironmentVariables);
        if (!Utilities.IsUnix)
        {
          rp1.RemoteHost = ctx.RemoteCBuildHost;
          rp1.EnvironmentVariables.Add("CARBON_HOME", ctx.RemoteCarbonHome);
          rp1.EnvironmentVariables.Add("CARBON_MK_HOME", ctx.RemoteCarbonModelKitHome);
          cbuild = "cbuild";
        }
        if (rp1.Run(subDir, OnStdout, null, cbuild, "-f", "cbuild_profilegen.f") != 0)
        {
          Logger.Error("{0} -profileGenerate Compilation failed", platformType);
          throw new CompileException(string.Format("{0} -profileGenerate Compilation Failed", platformType));
        }
        // Part 2 - runclkstb.sh  

        ModelKitPGO pgo = modelKit.PGOInformation(ctx);

        string runclkstb = pgo.Script;  // Environment.ExpandEnvironmentVariables("%MODELKITS_ROOT%/Scripts/runclkstb.sh");
        string testBenchFile = pgo.Argument;

        Carbon.ModelKitServices.ProcessRunner.RunProgram rp2 = new ProcessRunner.RunProgram(EnvironmentVariables);
        if (!Utilities.IsUnix)
        {
          rp2.RemoteHost = ctx.RemoteCBuildHost;
          rp2.EnvironmentVariables.Add("CARBON_HOME", ctx.RemoteCarbonHome);
          rp2.EnvironmentVariables.Add("CARBON_MK_HOME", ctx.RemoteCarbonModelKitHome);
        }

        if (rp2.Run(subDir, OnStdout, null, runclkstb, subDir, profilePlatformType, Path.Combine(ctx.ModelKitDirectory, testBenchFile)) != 0)
        {
          Logger.Error("{0} profile Compilation failed", platformType);
          throw new CompileException(string.Format("{0} profile Compilation Failed", platformType));
        }

        // Part 3 - cbuild -profileUse
        createOptionsFile(string.Format("-profileUse\n{0}", compilationOptions), modelname, subDir, "cbuild_profileuse.f");
        Carbon.ModelKitServices.ProcessRunner.RunProgram rp3 = new ProcessRunner.RunProgram(EnvironmentVariables);

        if (!Utilities.IsUnix)
        {
          rp3.RemoteHost = ctx.RemoteCBuildHost;
          rp3.EnvironmentVariables.Add("CARBON_HOME", ctx.RemoteCarbonHome);
          rp3.EnvironmentVariables.Add("CARBON_MK_HOME", ctx.RemoteCarbonModelKitHome);
          cbuild = "cbuild";
        }

        if (rp3.Run(subDir, OnStdout, null, cbuild, "-f", "cbuild_profileuse.f") != 0)
        {
          Logger.Error("{0} -profileUse Compilation failed", platformType);
          throw new CompileException(string.Format("{0} -profileUse Compilation Failed", platformType));
        }
        else
          verifyCompilationVisibility(modelname, subDir);
      }
      else if (buildType == Model.CbuildType.Explore)
      {
        createOptionsFile(string.Format("-writeGuiDB\n{0}", compilationOptions), modelname, subDir, "cbuild_explore.f");
        Carbon.ModelKitServices.ProcessRunner.RunProgram rp1 = new ProcessRunner.RunProgram(EnvironmentVariables);
        if (!Utilities.IsUnix)
        {
          rp1.RemoteHost = ctx.RemoteCBuildHost;
          rp1.EnvironmentVariables.Add("CARBON_HOME", ctx.RemoteCarbonHome);
          rp1.EnvironmentVariables.Add("CARBON_MK_HOME", ctx.RemoteCarbonModelKitHome);
          cbuild = "cbuild";
        }
        if (rp1.Run(subDir, OnStdout, null, cbuild, "-f", "cbuild_explore.f") != 0)
        {
          Logger.Error("{0} Explore Compilation failed", platformType);
          throw new CompileException(string.Format("{0} Explore Compilation Failed", platformType));
        }
      }
      else if (buildType == Model.CbuildType.CompileNoPGO)
      {
        // Part 1 - cbuild -profileGenerate
        createOptionsFile(compilationOptions, modelname, subDir, "cbuild_nopgo.f");
        Carbon.ModelKitServices.ProcessRunner.RunProgram rp1 = new ProcessRunner.RunProgram(EnvironmentVariables);
        if (!Utilities.IsUnix)
        {
          rp1.RemoteHost = ctx.RemoteCBuildHost;
          rp1.EnvironmentVariables.Add("CARBON_HOME", ctx.RemoteCarbonHome);
          rp1.EnvironmentVariables.Add("CARBON_MK_HOME", ctx.RemoteCarbonModelKitHome);
          cbuild = "cbuild";
        }
        if (rp1.Run(subDir, OnStdout, null, cbuild, "-f", "cbuild_nopgo.f") != 0)
        {
          Logger.Error("{0} Non-PGO Compilation failed", platformType);
          throw new CompileException(string.Format("{0} Non-PGO Compilation Failed", platformType));
        }
        else
          verifyCompilationVisibility(modelname, subDir);
      }
      else
        Logger.Error("Unknown Compile Type: {0}", buildType);
    }

    private static int searchFile(string fileName, string rePattern)
    {
      int numMatches = 0;
      // Read the file and display it line by line.
      using (System.IO.StreamReader file = new System.IO.StreamReader(fileName))
      {
        Regex re = new Regex(rePattern);
        string line = null;
        while ((line = file.ReadLine()) != null)
        {
          if (re.IsMatch(line))
            numMatches++;
        }
      }
      return numMatches;
    }
    /// <summary>
    /// Verify after the design is built that it had -noFullDB and no observeSignal *.*
    /// unless CARBON_MODELKIT_DEBUG was set, or building on the portal (MODEL_REQUEST_DIR) and (!CARBON_CBUILD_ARGS)
    /// </summary>
    /// <param name="modelName">libFoo.a|libXxx.lib</param>
    /// <param name="compileOutputDir">The directory containing libFoo.a|libXxx.lib</param>
    private static void verifyCompilationVisibility(string modelName, string compileOutputDir)
    {
      if ((Environment.GetEnvironmentVariable("CARBON_MODELKIT_DEBUG") == null) &&
        ((Environment.GetEnvironmentVariable("MODEL_REQUEST_DIR") != null) && (Environment.GetEnvironmentVariable("CARBON_CBUILD_ARGS") == null)))
      {
        string carbonModelPath = Path.Combine(compileOutputDir, modelName);
        string baseDesignName = Path.GetFileNameWithoutExtension(carbonModelPath);

        string cmdFileName = Path.Combine(compileOutputDir, string.Format("{0}.cmd", baseDesignName));
        string dirFileName = Path.Combine(compileOutputDir, string.Format("{0}.dir", baseDesignName));

        Logger.Log("Verifing model was built with -noFullDB and no observeSignal *.*");

        if (File.Exists(cmdFileName))
        {
          int numMatches = searchFile(cmdFileName, "^-noFullDB");
          if (numMatches > 0)
            Logger.Log("Pass: {0}", cmdFileName);
          else
            throw new Exception(string.Format("Error: Model built with -noFullDB (see: {0})", cmdFileName));
        }
        else
          Logger.Warning("No such Cmd File Found: {0}", cmdFileName);

        if (File.Exists(dirFileName))
        {
          int numMatches = searchFile(dirFileName, "^observeSignal \\*.\\*");
          if (numMatches == 0)
            Logger.Log("Pass: {0}", dirFileName);
          else
            throw new Exception(string.Format("Error: Model built with observeSignal *.* (see: {0})", dirFileName));
        }
        else
          Logger.Warning("No such Directives File Found: {0}", dirFileName);
      }
      else
        Logger.Log("Skipping Visibility Check: CARBON_MODELKIT_DEBUG is set");
    }

    private static void createOptionsFile(string compilationOptions, string designFileName, string subDir, string optionsFileName)
    {
      string optionsFilePath = Path.Combine(subDir, optionsFileName);

      using (StreamWriter sw = File.CreateText(optionsFilePath))
      {
        sw.WriteLine(string.Format("-o {0}", designFileName));
        sw.WriteLine(compilationOptions);
        sw.Close();
      }
    }

    private static void OnStdout(string stdout)
    {
      Logger.CBuild(stdout);
    }
  }

  /// <summary>
  /// The carmgr tool interface
  /// </summary>
  public class Carmgr
  {
    /// <summary>
    /// Run Carmgr then execute the generated Makfile to compile the Component
    /// </summary>
    /// <param name="context">ModelKit Context</param>
    /// <returns>true if was successfully built</returns>
    public static bool Run(ModelKitContext context)
    {
      if (true)
      {
        Carbon.ModelKitServices.ProcessRunner.RunProgram rp = new ProcessRunner.RunProgram(null);
        string setupmklib = @"%MODELKITS_ROOT%/Scripts/setupmklib";
        if (!Utilities.IsUnix)
        {
          setupmklib = @"setupmklib";
          rp.RemoteHost = context.RemoteCBuildHost;
          rp.EnvironmentVariables.Add("CARBON_HOME", context.RemoteCarbonHome);
          rp.EnvironmentVariables.Add("CARBON_MK_HOME", context.RemoteCarbonModelKitHome);
        }
        Logger.Log("Setting up MK Library Links");
        if (rp.Run(context.WorkingDirectory, OnStdout, null, setupmklib) != 0)
        {
          Logger.Error("Unable to create MK Library Links");
          return false;
        }
      }

      if (Model.isLinux(context.Answers.PlatformFlags))
      {
        string carbon = @"%CARBON_HOME%/bin/carbon";
        string ccfgName = string.Format("lib{0}.ccfg", context.BaseDesignName);

        Carbon.ModelKitServices.ProcessRunner.RunProgram rp = new ProcessRunner.RunProgram(null);

        string modelDir = FileUtilities.MapWindowsPathToLinux(context.PlatformModelDirectory(Model.ModelBuildPlatform.Gcc, Model.ComponentType.SoCDesigner));

        if (!Utilities.IsUnix)
        {
          carbon = "carbon";
          rp.RemoteHost = context.RemoteCBuildHost;
          rp.EnvironmentVariables.Add("CARBON_HOME", context.RemoteCarbonHome);
          rp.EnvironmentVariables.Add("CARBON_MK_HOME", context.RemoteCarbonModelKitHome);
        }
        if (rp.Run(modelDir, OnStdout, null, carbon, "carmgr", ccfgName) == 0)
        {
          string make = @"%CARBON_HOME%/bin/make";
          string makeFileName = string.Format("Makefile.lib{0}.mx", context.BaseDesignName);
          if (!Utilities.IsUnix)
          {
            make = "make";
          }
          if (rp.Run(modelDir, OnStdout, null, make, "VHM_DIR=../../", "-f", makeFileName) == 0)
          {
            Logger.Log("Built Linux Model in {0}", modelDir);
            copyModelFiles(Model.ModelPlatform.Gcc, context, modelDir);
          }
          else
          {
            Logger.Error("Component Compilation failed");
            return false;
          }
        }
        else
        {
          Logger.Error("Carmgr Compilation failed");
          return false;
        }
      }

      // Windows?
      if (Model.isWindows(context.Answers.PlatformFlags))
      {
        string wincarmgr = @"%MODELKITS_ROOT%/Scripts/runwindowscarmgr";

        Carbon.ModelKitServices.ProcessRunner.RunProgram rp = new ProcessRunner.RunProgram(null);

        string carbonHome = Environment.GetEnvironmentVariable("CARBON_HOME");
        string maxsimHome = Environment.GetEnvironmentVariable("MAXSIM_HOME");

        string arg1 = string.Format("\"{0}\"", Model.ConvertUnixMaxsimHomeToBaseUNC(maxsimHome));
        string arg2 = string.Format("\"{0}\"", Model.ConvertUnixCarbonHomeToUNC(carbonHome));
        string arg3 = "windows/CompiledModels";

        if (rp.Run(context.WorkingDirectory, OnStdout, null, wincarmgr, arg1, arg2, arg3) != 0)
        {
          Logger.Error("Windows Compilation failed");
          throw new CompileException("Windows Compilation Failed");
        }
        else // Success!
        {
          if (Model.isVS2005(context.Answers.PlatformFlags))
          {
            saveBuiltModel(context, Model.ModelPlatform.VC2005, "vc2005");
          }
          if (Model.isVS2008(context.Answers.PlatformFlags))
          {
            saveBuiltModel(context, Model.ModelPlatform.VC2008, "vc2008");
          }
          if (Model.isVS2010(context.Answers.PlatformFlags))
          {
            saveBuiltModel(context, Model.ModelPlatform.VC2010, "vc2010");
          }
          if (Model.isVS2012(context.Answers.PlatformFlags))
          {
            saveBuiltModel(context, Model.ModelPlatform.VC2012, "vc2012");
          }
        }
      }

      return true;
    }

    private static void saveBuiltModel(ModelKitContext context, Model.ModelPlatform platform, string vcPlatform)
    {
      string srcDir = Path.Combine(context.WindowsModelDirectory, "CompiledModels", "SoCDesigner", vcPlatform);
      copyModelFiles(platform, context, srcDir);
      copyModelFiles(Path.Combine(context.WindowsModelDirectory, "SoCDesigner", vcPlatform), platform, context, srcDir);
    }

    static bool PathMatchSpec(string path, string spec)
    {
      string specAsRegex = Regex.Escape(spec).Replace("\\*", ".*").Replace("\\?", ".") + "$";
      return Regex.IsMatch(path, specAsRegex);
    }

    static void copyModelFiles(Model.ModelPlatform platform, ModelKitContext context, string modelDir)
    {
      string componentDataDir = Path.Combine(context.ComponentPlatformDataDirectory(platform));
      copyModelFiles(componentDataDir, platform, context, modelDir);
    }

    static void copyModelFiles(string destinationDir, Model.ModelPlatform platform, ModelKitContext context, string modelDir)
    {
      string[] fileTypes = new string[] { "lib*.mx.so", "lib*.mx_DBG.so", "*.xpm", "*.pdf", "maxlib.*.conf", "*.ccfg", "lib*.mx.dll", "lib*.mx_DBG.dll" };

      string componentDataDir = destinationDir;

      if (!Directory.Exists(componentDataDir))
        Directory.CreateDirectory(componentDataDir);

      if (!Directory.Exists(modelDir))
        throw new Exception(string.Format("Unable to copy files from model dir: {0}, directory does not exist", modelDir));

      if (!Directory.Exists(componentDataDir))
        throw new Exception(string.Format("Unable to create component data destination directory: {0}", componentDataDir));

      Logger.Log("Copy Results from {0} to {1}", modelDir, componentDataDir);
      foreach (string file in Directory.GetFiles(modelDir))
      {
        foreach (string fileType in fileTypes)
        {
          if (PathMatchSpec(file, fileType))
          {
            FileUtilities.CopyFile(file, componentDataDir);
          }
        }
      }
    }

    private static void OnStdout(string stdout)
    {
      Logger.Log(stdout);
    }

    /// <summary>
    /// Write the .ccfg File into the correct location(s) for all platforms
    /// </summary>
    /// <param name="ccfg">User built ccfg object</param>
    /// <param name="context">Modelkit Context</param>
    /// <returns>true to indicate success, false to indicate failure</returns>
    public static bool WriteCcfg(Carbon.Ccfg.CarbonCfg ccfg, ModelKitContext context)
    {
      string ccfgFileName = string.Format("lib{0}.ccfg", context.BaseDesignName);

      if (context.ComponentType == Model.ComponentType.SoCDesigner)
      {
        StreamWriter compileBat = null;
        if (Model.isWindows(context.Answers.PlatformFlags))
          compileBat = File.CreateText(Path.Combine(context.WindowsModelDirectory, "compile.bat"));

        foreach (Model.ModelBuildPlatform plat in context.Platforms(context.Answers.PlatformFlags))
        {
          Model.CBuildPlatform cbuildPlatform = Model.Convert(plat);

          string carbonDBFilePath = context.getDBFilePath(cbuildPlatform);
          FileInfo fi = new FileInfo(carbonDBFilePath);
          string dbFileName = fi.Name;

          string relPathDir = "../../";
          // Note, that relPathDir is not platform dependent, as writing ccfg file is performed in the linux part of the modelbuilder flow, even when the target plaform is Windows.

          ccfg.putIODBFileName(string.Format("{0}{1}", relPathDir, dbFileName));

          if (Model.isLinux(plat))
          {
            FileInfo mfi = new FileInfo(context.LinuxCarbonModelName);
            ccfg.putLibName(string.Format("{0}{1}", relPathDir, mfi.Name));
            string ccfgDir = context.PlatformModelDirectory(plat, context.ComponentType);
            if (!Directory.Exists(ccfgDir))
              Directory.CreateDirectory(ccfgDir);
            string ccfgName = Path.Combine(ccfgDir, ccfgFileName);
            ccfg.write(ccfgName);
            Logger.Log("Wrote Linux {0}/{1} Ccfg: {2}", context.ComponentType, plat, ccfgName);
          }
          else if (Model.isWindows(plat))
          {
            string modelDir = context.PlatformModelDirectory(plat, context.ComponentType).Replace(context.WindowsModelDirectory, "").Replace("/", "\\");
            if (modelDir.StartsWith("\\"))
              modelDir = modelDir.Substring(1);

            compileBat.WriteLine(string.Format("pushd {0}", modelDir));
            compileBat.WriteLine(string.Format("call \"%{0}%\"\\vsvars32.bat", context.VisualStudioEnvironmentVariable(plat)));

            string socd_suffix = "Win32-UNKNOWN";
            switch (plat)
            {
              case Model.ModelBuildPlatform.VS2005: socd_suffix = "Win32-VC2005"; break;
              case Model.ModelBuildPlatform.VS2008: socd_suffix = "Win32-VC2008"; break;
              case Model.ModelBuildPlatform.VS2010: socd_suffix = "Win32-VC2010"; break;
              case Model.ModelBuildPlatform.VS2012: socd_suffix = "Win32-VC2012"; break;
            }

            compileBat.WriteLine(string.Format("set MAXSIM_HOME={0}", string.Format("%MAXSIM_HOME_BASE%\\{0}", socd_suffix)));
            compileBat.WriteLine(string.Format("set MAXSIM_PROTOCOLS={0}", string.Format("%MAXSIM_HOME%\\Protocols")));
            compileBat.WriteLine(string.Format("set"));
            compileBat.WriteLine(string.Format("call \"%CARBON_HOME%\\bin\\carbon.bat\" carmgr {0}", ccfgFileName));
            compileBat.WriteLine(string.Format("if %ERRORLEVEL% GEQ 1 exit 1"));
            compileBat.WriteLine(string.Format("call nmake /f Makefile.lib{0}.mx.windows", context.BaseDesignName));
            compileBat.WriteLine(string.Format("if %ERRORLEVEL% GEQ 1 exit 1"));
            compileBat.WriteLine(string.Format("popd"));

            FileInfo mfi = new FileInfo(context.WindowsCarbonModelName);
            ccfg.putLibName(string.Format("{0}{1}", relPathDir, mfi.Name));

            string ccfgDir = context.PlatformModelDirectory(plat, context.ComponentType);
            if (!Directory.Exists(ccfgDir))
              Directory.CreateDirectory(ccfgDir);

            string ccfgName = Path.Combine(ccfgDir, ccfgFileName);
            ccfg.write(ccfgName);
            Logger.Log("Wrote Windows {0}/{1} Ccfg: {2}", context.ComponentType, plat, ccfgName);
          }
        }

        if (compileBat != null)
        {
          compileBat.Close();
          compileBat.Dispose();
        }
      }

      return true;
    }
  }


  /// <summary>
  /// Amba Designer tool interface
  /// </summary>
  public class AmbaDesigner
  {
    /// <summary>
    /// Execute adcanvas with arguments
    /// </summary>
    /// <param name="context"></param>
    /// <param name="ambaDesignerVersion">i.e. AMBADesigner_r3p0_rel0</param>
    /// <param name="userXMLFile">filename.xml</param>
    /// <param name="ipArgument">PL330</param>
    /// <returns></returns>
    public static bool Run(ModelKitContext context, string ambaDesignerVersion, string userXMLFile, string ipArgument)
    {
      string ambaDesignerDir = Path.Combine(Path.DirectorySeparatorChar.ToString(), "usr", "ARM", ambaDesignerVersion);

      verifyEnvironment(userXMLFile, ambaDesignerDir);

      Carbon.ModelKitServices.ProcessRunner.RunProgram rp = new ProcessRunner.RunProgram(null);
      string adcanvas = @"%MODELKITS_ROOT%/Scripts/runambadesigner";
      if (!Utilities.IsUnix)
      {
        adcanvas = @"runambadesigner";
        rp.RemoteHost = context.RemoteCBuildHost;
        rp.EnvironmentVariables.Add("CARBON_HOME", context.RemoteCarbonHome);
        rp.EnvironmentVariables.Add("CARBON_MK_HOME", context.RemoteCarbonModelKitHome);
      }
      Logger.Log("Running AMBA Designer");
      if (rp.Run(context.WorkingDirectory, OnStdout, null, adcanvas,
        "-workingDir", FileUtilities.MapWindowsPathToLinux(context.WorkingDirectory),
        "-userXMLFile", userXMLFile,
        "-ambaDesignerVersion", ambaDesignerVersion,
        "-ipArgument", ipArgument
        ) != 0)
      {
        Logger.Error("AMBA Designer Failed");
        return false;
      }


      return true;
    }


    private static void OnStdout(string stdout)
    {
      Logger.Log(stdout);
    }

    private static void verifyEnvironment(string userXMLFile, string ambaDesignerDir)
    {
      if (!File.Exists(userXMLFile))
        throw new Exception(string.Format("Error: No such user amba xml file: {0}", userXMLFile));

      if (!Directory.Exists(ambaDesignerDir))
        throw new Exception(string.Format("Error: No such AMBA Designer directory: {0}", ambaDesignerDir));
    }
  }

  /// <summary>
  /// Extension classes 
  /// </summary>
  public static class ContainerExtensions
  {
    /// <summary>
    /// Manage a list with unique entries
    /// </summary>
    /// <param name="l">List to add to</param>
    /// <param name="s">String to add uniquely</param>
    /// <returns></returns>
    public static bool AddUnique(this List<string> l, string s)
    {
      if (l.Contains(s))
      {
        return false;
      }

      l.Add(s);
      return true;
    }

    /// <summary>
    /// Adds contents of one list to the other making sure that the resulting list
    /// has unique values
    /// </summary>
    /// <returns>
    /// List of strings that were duplicated in the to and from lists
    /// </returns>
    /// <param name='listAddTo'>
    /// List to add to.
    /// </param>
    /// <param name='listAddFrom'>
    /// List to add from.
    /// </param>
    /// <remarks>
    /// The alogrithm below is suboptimal, but that is intentional to clearly obtain
    /// a list of diffs.  Having that list makes error reporting better
    /// </remarks>
    public static List<string> AddUnique(this List<string> listAddTo, IEnumerable<string> listAddFrom)
    {
      listAddTo.AddRange(listAddFrom);

      List<string> dupes = listAddTo.GroupBy(x => x)
        .Where(g => g.Count() > 1)
          .Select(g => g.Key)
          .ToList();

      listAddTo = listAddTo.Distinct().ToList();
      return dupes;
    }
  }


}
