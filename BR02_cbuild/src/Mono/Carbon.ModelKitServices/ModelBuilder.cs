﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Xml;
using Ionic.Zip;

namespace Carbon.ModelKitServices
{
  /// <summary>
  /// The Class used to control the building of a ModelKit
  /// </summary>
  public class ModelBuilder
  {
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    private static bool stoppedPhase = false;

    static Assembly LoadFromCarbonHome(object sender, ResolveEventArgs args)
    {
      string folderPath = Environment.ExpandEnvironmentVariables("%CARBON_HOME%\\Win\\lib");
      string assemblyPath = Path.Combine(folderPath, new AssemblyName(args.Name).Name + ".dll");
      if (File.Exists(assemblyPath) == false) return null;
      Assembly assembly = Assembly.LoadFrom(assemblyPath);
      return assembly;
    }
    /// <summary>
    /// Execute the ModelKit flow
    /// </summary>
    /// <param name="args">Arguments see the Usage()</param>
    /// <returns>ExitCode </returns>
    public static int Main(string[] args)
    {
      Logger.SetLogger(log);

      Logger.Log("Verify Environment");

      if (!verifyEnvironment())
        return 1;

      Arguments arguments = Arguments.Parse(args);
      if (arguments == null)
        return 1;

      try
      {
        // Arguments are valid, here we go  
        FileInfo fi = new FileInfo(arguments.ModelKitAssembly);
        Logger.Log("Loading Assembly: {0}", fi.FullName);

        if (!Utilities.IsUnix)
        {
          AppDomain currentDomain = AppDomain.CurrentDomain;
          currentDomain.AssemblyResolve += new ResolveEventHandler(LoadFromCarbonHome);
          Environment.SetEnvironmentVariable("PATH",
            Environment.ExpandEnvironmentVariables("%CARBON_HOME%\\Win\\lib;%PATH%"));

        }

        Assembly assy = Assembly.LoadFrom(arguments.ModelKitAssembly);
        if (assy == null)
          throw new Exception("Unable to load assembly");

        Type type = assy.GetType("Carbon.ModelKit");
        if (type == null)
          throw new Exception("No Carbon.ModelKit type found");

        object carbonModelKit = Activator.CreateInstance(type);
        if (carbonModelKit != null)
        {
          IModelKit modelKit = carbonModelKit as IModelKit;
          if (modelKit != null)
          {
            ModelKitContext ctx = null;
            if (!arguments.RetainFiles)
            {
              ctx = new ModelKitContext(Path.GetDirectoryName(assy.Location), arguments.ModelKitAnswersFile);

              if (arguments.Clean)
                cleanDirectories(ctx.WorkingDirectory);

              // Overrrides to answers file?
              loadOverrides(arguments, ctx);

              ctx.BaseDesignName = ctx.Answers.ComponentName;
              ctx.LinuxCarbonModelName = Path.Combine(ctx.LinuxModelDirectory, string.Format("lib{0}.a", ctx.BaseDesignName));
              ctx.WindowsCarbonModelName = Path.Combine(ctx.WindowsModelDirectory, string.Format("lib{0}.lib", ctx.BaseDesignName));

              initializeArguments(arguments, ctx);       // setup the context

              // Add Standard Environment Variables
              addStandardEnvironmentVariables(ctx);

              Logger.Log("Creating Model Kit {0} from Assembly: {1}", modelKit.Name, ctx.ModelKitDirectory);

              string cbuildVersion = modelKit.MinimumCBuildVersion;
              if (!string.IsNullOrEmpty(cbuildVersion))
              {
                bool check = Carbon.ModelKitServices.Model.VersionCheck(cbuildVersion);
                if (!check)
                {
                  Logger.Log("Failed Version Check");
                  return 1;
                }
              }
            }

            try
            {
              // Retain files is a single method execution and then terminate
              if (arguments.RetainFiles)
              {
                Environment.SetEnvironmentVariable("MODELKIT_ROOT", Environment.CurrentDirectory);
                RetainFiles(arguments, modelKit, arguments.Variant, arguments.Partner, arguments.WorkingVersion);
                return 0;
              }

              // What phase are we starting with?       
              ModelKitInterface.Phase phase = arguments.Phase;

              // We always call Initialize
              modelKit.Initialize(phase, ctx);

              // We've already done initialize
              if (!stopAtPhase(arguments, phase) && phase == ModelKitInterface.Phase.Initialize)
                phase = ModelKitInterface.Phase.Create;

              if (!stopAtPhase(arguments, phase) && phase == ModelKitInterface.Phase.Create)
              {
                Logger.Log("--- Start Phase: {0}", phase);
                if (modelKit.Create(ctx))
                  phase = ModelKitInterface.Phase.AnalyzeAndTest;
                else
                {
                  throw new Exception(string.Format("Phase {0} Aborted", phase));
                }
              }

              if (!stopAtPhase(arguments, phase) && phase == ModelKitInterface.Phase.AnalyzeAndTest)
              {
                Logger.Log("--- Start Phase: {0}", phase);
                if (modelKit.AnalyzeAndTest(ctx))
                  phase = ModelKitInterface.Phase.Compile;
                else
                {
                  throw new Exception(string.Format("Phase {0} Aborted", phase));
                }
              }

              if (!stopAtPhase(arguments, phase) && phase == ModelKitInterface.Phase.Compile)
              {
                Logger.Log("--- Start Phase: {0}", phase);
                if (modelKit.Compile(ctx))
                  phase = ModelKitInterface.Phase.GenerateComponent;
                else
                {
                  throw new Exception(string.Format("Phase {0} Aborted", phase));
                }
              }

              if (!stopAtPhase(arguments, phase) && phase == ModelKitInterface.Phase.GenerateComponent)
              {
                Logger.Log("--- Start Phase: {0}", phase);
                if (modelKit.GenerateComponent(ctx))
                  phase = ModelKitInterface.Phase.Finished;
                else
                {
                  throw new Exception(string.Format("Phase {0} Aborted", phase));
                }
              }

              if (!stopAtPhase(arguments, phase) && phase == ModelKitInterface.Phase.Finished)
              {
                Logger.Log("--- Start Phase: {0}", phase);
                if (!modelKit.Finished(ctx))
                {
                  throw new Exception(string.Format("Phase {0} Aborted", phase));
                }
              }

              Logger.Log("Modelkit Build Finished Successfully");
            }
            catch (Exception ex)
            {
              Logger.Error("Error: Modelkit Build Failure: {0}", ex.Message);
              Logger.Error(ex.StackTrace);
              if (ex.InnerException != null)
              {
                Logger.Error("Inner Ex: {0}", ex.InnerException.Message);
                Logger.Error(ex.InnerException.StackTrace);
              }
              return 1;
            }
          }
          else
            throw new Exception("Internal Exception 1");
        }
        else
          throw new Exception("Unknown Errror");
      }
      catch (Exception ex)
      {
        Exception realerror = ex;
        while (realerror.InnerException != null)
          realerror = realerror.InnerException;
        Logger.Error(realerror.ToString());
      }
      return 0;
    }

    private static void cleanDirectories(string baseDir)
    {
      string[] subDirs = new string[] { "linux", "windows", "model" };
      foreach (string subDir in subDirs)
      {
        string folder = Path.Combine(baseDir, subDir);
        if (Directory.Exists(folder))
        {
          Logger.Log("Cleaning Directory: {0}", folder);
          Directory.Delete(folder, true);
        }
      }
    }

    private static void RetainFiles(Arguments arguments, IModelKit modelKit, string variant, string partner, string workingVersion)
    {
      Logger.Log("Retain Files Now: Variant: {0}, Partner: {1}", variant, partner);
      ModelKitRetainFiles rf = new ModelKitRetainFiles();
      modelKit.RetainFiles(rf);
      rf.RetainFiles(modelKit, variant, partner, workingVersion);
    }

    private static void initializeArguments(Arguments arguments, ModelKitContext ctx)
    {
      ctx.AttributeXMLFile = arguments.AttributeXMLFile;
      ctx.NoPGO = arguments.NoPGO;
	  ctx.NoRender = arguments.NoRender;
      ctx.NoCompile = arguments.NoCompile;
      ctx.NoExplore = arguments.NoExplore;
      ctx.RemoteCarbonHome = arguments.RemoteCarbonHome;
      ctx.RemoteCarbonModelKitHome = arguments.RemoteCarbonModelKitHome;
      ctx.RemoteModelKitSourceDir = arguments.RemoteModelKitSourceDir;
      ctx.RemoteCBuildHost = arguments.RemoteCBuildHost;
    }



    // Save all the model files (*.dll, *.conf, *.so, etc)
    private void saveDataFiles(ModelKitContext ctx)
    {

    }

    // Any override switches?
    private static void loadOverrides(Arguments arguments, ModelKitContext ctx)
    {
      if (arguments.RetainFiles)
        return;

      if (!string.IsNullOrEmpty(arguments.Platform))
      {
        Logger.Log("Overriding Platform {0} with {1}", ctx.Answers.Platform, arguments.Platform);
        ctx.Answers.Platform = arguments.Platform;
        ctx.Answers.PlatformFlags = Model.Convert(arguments.Platform);
        ctx.Answers.Answers["platform"] = arguments.Platform;
      }

      if (!string.IsNullOrEmpty(arguments.Variant))
      {
        Logger.Log("Overriding Variant {0} with {1}", ctx.Answers.Variant, arguments.Variant);
        ctx.Answers.Variant = arguments.Variant;
        ctx.Answers.Answers["variant"] = arguments.Variant;
      }
    }

    private static bool stopAtPhase(Arguments args, ModelKitInterface.Phase phase)
    {
      if (args.StopPhase.HasValue && phase == args.StopPhase.Value)
      {
        if (!stoppedPhase)
          Logger.Log("  **** Stopping at phase {0}", phase);
        stoppedPhase = true;
        return true;
      }
      else
        return false;
    }

    // Add stock variables used by the kit
    private static void addStandardEnvironmentVariables(ModelKitContext ctx)
    {
      Environment.SetEnvironmentVariable("MODELKIT_ROOT", ctx.WorkingDirectory);
      Environment.SetEnvironmentVariable("CARBON_PROJECT", ctx.WorkingDirectory);
    }

    private static bool verifyEnvironment()
    {
      if (!validateEnvironmentVariable("MAXSIM_HOME"))
        return false;

      if (!validateEnvironmentVariable("CARBON_HOME"))
        return false;

      if (Utilities.IsUnix)
      {
        if (!validateEnvironmentVariable("MONO_PATH"))
          return false;

        if (!validateEnvironmentVariable("MODELKITS_ROOT"))
          return false;

        if (!validateEnvironmentVariable("CARBON_MK_HOME"))
          return false;
      }
      //foreach (string evar in Environment.GetEnvironmentVariables().Keys)
      //{
      //  Logger.Log("{0} ={1}", evar, Environment.GetEnvironmentVariable(evar));
      //}
      return true;

    }

    private static bool validateEnvironmentVariable(string varName)
    {
      string envVar = Environment.GetEnvironmentVariable(varName);
      if (string.IsNullOrEmpty(envVar))
      {
        Logger.Error("Missing {0} environment variable", varName);
        return false;
      }
      else
        Logger.Log("Environment {0}={1}", varName, envVar);

      return true;
    }

    class Arguments
    {
      // Clean the directories (linux/windows/data)
      public bool Clean { get; set; }
      public string ModelKitAssembly { get; set; }
      public string ModelKitAnswersFile { get; set; }
      public string WorkingDir { get; set; }
      public ModelKitInterface.Phase Phase { get; set; }
      public ModelKitInterface.Phase? StopPhase { get; set; }
      public string Platform { get; set; }
      public string Variant { get; set; }
      public string Partner { get; set; }
      public string WorkingVersion { get; set; }
      public string AssemblyName { get; set; }
      public bool NoPGO { get; set; }
      public bool NoRender { get; set; }
      public bool NoCompile { get; set; }
      public bool NoExplore { get; set; }

      public string AttributeXMLFile { get; set; }
      public string RemoteCBuildHost { get; set; }
      public string RemoteCarbonHome { get; set; }
      public string RemoteCarbonModelKitHome { get; set; }
      public string RemoteModelKitSourceDir { get; set; }

      public List<string> AdditionalArguments { get; set; }
      public bool RetainFiles { get; set; }

      public static void Usage()
      {
        string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().Location;
        FileInfo fi = new FileInfo(assemblyName);
        string programName = fi.Name;

        Console.WriteLine("\nUsage:\tModelBuilder assembly.dll|kit.zip answersfile.xml [options]");
        Console.WriteLine("\t-assembly - Specifies assembly name (when using .zip files) to load");
        Console.WriteLine("\t-clean - Clean the directories before proceeding");				
        Console.WriteLine("\t-noRender - Do not render IP-XACT, source files");
        Console.WriteLine("\t-noCompile - Do not execute CBuild::Run(Mode: Compile)");
        Console.WriteLine("\t-noExplore - Do not execute CBuild::Run(Mode: Explore)");
		Console.WriteLine("\t-compOnly  - same as '-noRender -noExplore -noCompile'");
        Console.WriteLine("\t-noPGO - Do not use PGO optimization");
        Console.WriteLine("\t-partner name - Use the specified partner");
        Console.WriteLine("\t-platform name - Use platform rather than from answers file");
        Console.WriteLine("\t-phase phaseName - Start at phase phaseName");
        Console.WriteLine("\t-retainFiles - Execute RetainFiles() method only");
        Console.WriteLine("\t-stopPhase phaseName - Stop at phase phaseName");
        Console.WriteLine("\t-variant name - Use variant rather than from answers file");
        Console.WriteLine("\t-workingDir - Use directory to build kit in");
        Console.WriteLine("\t-attributeFile attributeXml.xml - Use XML attribute file to embed into model");

        Console.WriteLine("");
      }

      public Arguments()
      {
        WorkingDir = ".";
        StopPhase = null;
        Phase = ModelKitInterface.Phase.Initialize;
        AdditionalArguments = new List<string>();
        NoPGO = false;
		NoRender = false;
        NoCompile = false;
        NoExplore = false;
        RetainFiles = false;
        Clean = false;
      }

      public static Arguments Parse(string[] args)
      {
        Logger.Log("Working Directory: {0}", Directory.GetCurrentDirectory());
        Logger.Log("Command Line: {0}", string.Join(" ", args));

        Arguments arguments = new Arguments();

        bool results = arguments.ParseCommandLine(args.ToList(), arguments);
        if (results)
          return arguments;
        else
          return null;
      }

      bool ParseCommandLine(List<string> argList, Arguments arguments)
      {
        try
        {
          if (!ParseArguments(argList, arguments))
          {
            Usage();
            Logger.Error("Missing arguments");
            return false;
          }
        }
        catch (Exception ex)
        {
          Logger.Error("Error: {0}", ex.Message);
          return false;
        }

        int minRequiredArgs = 2;
        if (arguments.RetainFiles)
          minRequiredArgs = 1;

        if (argList.Count < minRequiredArgs)
        {
          Logger.Error("Invalid usage, missing arguments");
          Usage();
          return false;
        }

        arguments.ModelKitAssembly = argList[0];
        argList.RemoveAt(0);

        if (argList.Count > 0)
        {
          arguments.ModelKitAnswersFile = argList[0];
          argList.RemoveAt(0);
        }

        AdditionalArguments = argList;

        if (!validateAssembly(arguments))
          return false;

        if (!validateAnswers(arguments))
          return false;

        return true;
      }

      private static bool validateAnswers(Arguments arguments)
      {
        if (arguments.RetainFiles)
          return true;

        bool returnStatus = true;

        if (!File.Exists(arguments.ModelKitAnswersFile))
        {
          Logger.Error("ERROR: No such Answers file: {0}", arguments.ModelKitAnswersFile);
          returnStatus = false;
        }

        try
        {
          // Make sure it's an XML file
          XmlDocument xdoc = new XmlDocument();
          xdoc.Load(arguments.ModelKitAnswersFile);
        }
        catch (Exception ex)
        {
          Console.WriteLine("ERROR: Invalidate Answers file: {0}: {1}", arguments.ModelKitAnswersFile, ex.Message);
          returnStatus = false;
        }


        return returnStatus;
      }

      private static void OnStdout(string stdout)
      {
        Logger.Log(stdout);
      }

      private static bool validateAssembly(Arguments arguments)
      {
        bool returnStatus = true;

        if (!File.Exists(arguments.ModelKitAssembly))
        {
          Logger.Error("ERROR: No such Assembly file: {0}", arguments.ModelKitAssembly);
          returnStatus = false;
        }

        Logger.Log("Model Kit Assembly: {0}", arguments.ModelKitAssembly);

        // Is it a .zip file?
        FileInfo fi = new FileInfo(arguments.ModelKitAssembly);
        if (fi.Extension.ToLower() == ".zip")
        {
          Carbon.ModelKitServices.ProcessRunner.RunProgram rp = new ProcessRunner.RunProgram(null);
          Logger.Log("Unzipping {0}", arguments.ModelKitAssembly);
          if (rp.Run(arguments.WorkingDir, OnStdout, null, "unzip", "-o", arguments.ModelKitAssembly) != 0)
            return false;
          arguments.ModelKitAssembly = arguments.AssemblyName;
          Logger.Log("Finished Unzipping {0}", arguments.ModelKitAssembly);
        }

        Assembly assy = Assembly.LoadFrom(arguments.ModelKitAssembly);
        if (assy != null)
        {
          Type type = assy.GetType("Carbon.ModelKit");
          if (type == null)
          {
            Console.WriteLine("No such type: Carbon.ModelKit");
            returnStatus = false;
          }
          else
          {
            Type iface = type.GetInterface("IModelKit");
            if (iface == null)
            {
              Console.WriteLine("No Such Interface IModelKit found in Carbon.ModelKit class");
              returnStatus = false;
            }
          }
        }
        else
        {
          Console.WriteLine("Unable to load assembly: {0}", arguments.ModelKitAssembly);
          returnStatus = false;
        }

        Logger.Log("Validated Assembly: {0}", returnStatus);

        return returnStatus;
      }

      static string expandShortPhaseNames(string phase)
      {
        string[] allNames = Enum.GetNames(typeof(ModelKitInterface.Phase));
        var matchedArgs = allNames.Where(a => phase.ToLower() == a.Substring(0, Math.Min(phase.Length, a.Length)).ToLower());
        if (matchedArgs.Count() != 1)
        {
          throw new Exception(string.Format("{0} does not uniquely identify a defined phase", phase));
        }
        return matchedArgs.First();
      }

      static bool ParseArguments(List<string> args, Arguments arguments)
      {
        if (args.Count < 1)
          return false;

        int totalArgs = args.Count;
        int argsProcessed = 0;
        int argIndex = 0;

        while (argsProcessed < totalArgs && argIndex < args.Count)
        {
          argsProcessed++;
          string arg = args[argIndex];
          switch (arg)
          {
            case "-attributeFile":
              args.RemoveAt(argIndex);
              string attfile = NextArg(args, argIndex);
              arguments.AttributeXMLFile = attfile;
              argsProcessed++;
              break;

            case "-remoteCarbonHome":
              args.RemoveAt(argIndex);
              arguments.RemoteCarbonHome = NextArg(args, argIndex);
              argsProcessed++;
              break;

            case "-remoteCBuildHost":
              args.RemoveAt(argIndex);
              arguments.RemoteCBuildHost = NextArg(args, argIndex);
              argsProcessed++;
              break;

            case "-remoteCarbonMKHome":
              args.RemoveAt(argIndex);
              arguments.RemoteCarbonModelKitHome = NextArg(args, argIndex);
              argsProcessed++;
              break;

            case "-remoteModelKitSourceDir":
              args.RemoveAt(argIndex);
              arguments.RemoteModelKitSourceDir = NextArg(args, argIndex);
              argsProcessed++;
              break;

            case "-phase":
              args.RemoveAt(argIndex);
              string phase = expandShortPhaseNames(NextArg(args, argIndex));
              arguments.Phase = (ModelKitInterface.Phase)Enum.Parse(typeof(ModelKitInterface.Phase), phase, true);
              argsProcessed++;
              break;

            case "-stopPhase":
              args.RemoveAt(argIndex);
              string stopPhase = expandShortPhaseNames(NextArg(args, argIndex));
              arguments.StopPhase = (ModelKitInterface.Phase)Enum.Parse(typeof(ModelKitInterface.Phase), stopPhase, true);
              argsProcessed++;
              break;

            case "-clean":
              args.RemoveAt(argIndex);
              arguments.Clean = true;
              break;

            case "-noPGO":
              args.RemoveAt(argIndex);
              arguments.NoPGO = true;
              break;

            case "-noRender":
              args.RemoveAt(argIndex);
              arguments.NoRender = true;
              break;

            case "-noCompile":
              args.RemoveAt(argIndex);
              arguments.NoCompile = true;
              break;

            case "-noExplore":
              args.RemoveAt(argIndex);
              arguments.NoExplore = true;
              break;
						
			case "-compOnly":
				args.RemoveAt (argIndex);
				arguments.NoRender = arguments.NoExplore = arguments.NoCompile = true;
				break;

            case "-retainFiles":
              args.RemoveAt(argIndex);
              arguments.RetainFiles = true;
              break;

            case "-platform":
              args.RemoveAt(argIndex);
              string platform = NextArg(args, argIndex);
              arguments.Platform = platform;
              argsProcessed++;
              break;

            case "-partner":
              args.RemoveAt(argIndex);
              string partner = NextArg(args, argIndex);
              arguments.Partner = partner;
              argsProcessed++;
              break;

            case "-workingVersion":
              args.RemoveAt(argIndex);
              string workingVersion = NextArg(args, argIndex);
              arguments.WorkingVersion = workingVersion;
              argsProcessed++;
              break;

            case "-assembly":
              args.RemoveAt(argIndex);
              string assy = NextArg(args, argIndex);
              arguments.AssemblyName = assy;
              Logger.Log("Using Assembly Name: {0}", assy);
              argsProcessed++;
              break;

            case "-variant":
              args.RemoveAt(argIndex);
              string variant = NextArg(args, argIndex);
              arguments.Variant = variant;
              argsProcessed++;
              break;

            case "-workingDir":
              args.RemoveAt(argIndex);
              string workingDir = NextArg(args, argIndex);
              arguments.WorkingDir = workingDir;
              if (!Directory.Exists(arguments.WorkingDir))
                Directory.CreateDirectory(arguments.WorkingDir);
              Environment.CurrentDirectory = arguments.WorkingDir;
              Logger.Log("Working Directory Changed To: {0}", Environment.CurrentDirectory);
              argsProcessed++;
              break;

            default:
              argIndex++;
              break;
          }
        }

        return true;
      }

      static string NextArg(List<string> args, int i)
      {
        if (i < args.Count)
        {
          string v = args[i];
          args.RemoveAt(i);
          return v;
        }
        else
        {
          throw new Exception(string.Format("Error: Missing argument for {0}", args[i - 1]));
        }
      }
    }
  }
}
