﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;
using System.IO;
using System.Diagnostics;

using Carbon.ModelKitServices;

namespace Carbon.ModelKitServices.ProcessRunner
{
  /// <summary>
  /// Class used for executing programs
  /// </summary>
  public class RunProgram
  {
    /// <summary>
    /// Executes the program on a remote linux machine
    /// </summary>
    public bool Remote { get { return !string.IsNullOrEmpty(RemoteHost); } }

    /// <summary>
    /// Machine on which to execute the command (linux)
    /// </summary>
    public string RemoteHost { get; set; }

    public string RemoteWorkingDir { get; set; }

    /// <summary>
    /// Construct a RunProgram object to handle program execution.
    /// </summary>
    public RunProgram(Dictionary<string, string> extraVars)
    {
      EnvironmentVariables = new Dictionary<string, string>();
      if (extraVars != null)
      {
        foreach (string key in extraVars.Keys)
        {
          EnvironmentVariables.Add(key, Carbon.ModelKitServices.Tools.FileUtilities.MapWindowsPathToLinux(Environment.ExpandEnvironmentVariables(extraVars[key])));
        }
      }

      // Handle Maxsim Protocols
      if (Remote)
      {
        if (Environment.GetEnvironmentVariable("MAXSIM_PROTOCOLS") == null)
          EnvironmentVariables.Add("MAXSIM_PROTOCOLS", string.Format("{0}/Protocols", Carbon.ModelKitServices.Tools.FileUtilities.MapWindowsPathToLinux(EnvironmentVariables["MAXSIM_HOME"])));
        else
          EnvironmentVariables.Add("MAXSIM_PROTOCOLS", string.Format("{0}/Protocols", Carbon.ModelKitServices.Tools.FileUtilities.MapWindowsPathToLinux(EnvironmentVariables["MAXSIM_HOME"])));
      }

      EnvironmentVariables.Add("MODELKIT_ROOT", Carbon.ModelKitServices.Tools.FileUtilities.MapWindowsPathToLinux(Environment.CurrentDirectory));
    }

    /// <summary>
    /// Run the specified program <paramref name="exe"/>
    /// </summary>
    /// <param name="workingDir">The working directory to execute the program in</param>
    /// <param name="output">Encapsulates a method taking a single parameter which will receive each line of Stdout/StdErr</param>
    /// <param name="input">Optional input (may be null) to be passed to the program</param>
    /// <param name="exe">The program to be run, if no full path specification is given, the PATH will be searched to locate.</param>
    /// <param name="args">Any arguments passed to the program</param>
    /// <example>This sample shows how to execute a basic unix program ls -latr
    /// <code>
    /// RunProgram rp = new RunProgram();
    /// if (rp.Run(".", OnStdOut, null, "ls", "-latr") != 0)
    ///   throw new Exception("Unable to execute ls -latr");
    /// 
    /// void OnStdOut(string msg) { Logger.Log(msg); }
    /// 
    /// </code>
    /// </example>
    /// <returns>Exit status returned by the program</returns>
    public int Run(string workingDir, Action<string> output, TextReader input, string exe, params string[] args)
    {
      if (String.IsNullOrEmpty(exe))
        throw new FileNotFoundException("No program specified to run");

      return Execute(workingDir, output, input, exe, args);
    }

    /// <summary>
    /// Indicates we are running within the Mono environment
    /// </summary>
    static public bool IsMono
    {
      get
      {
        Type t = Type.GetType("Mono.Runtime");
        return t != null;
      }
    }

    /// <summary>
    /// Any optional environment variables you want set/passed into the program
    /// </summary>
    public Dictionary<string, string> EnvironmentVariables { get; set; }

    private string FindExePath(string exe)
    {
      exe = System.Environment.ExpandEnvironmentVariables(exe);
      if (!File.Exists(exe))
      {
        if (Path.GetDirectoryName(exe) == String.Empty)
        {
          char splitChar = ';';
          if (IsMono)
            splitChar = ':';

          foreach (string test in (System.Environment.GetEnvironmentVariable("PATH") ?? "").Split(splitChar))
          {
            string path = test.Trim();
            if (!String.IsNullOrEmpty(path) && File.Exists(path = Path.Combine(path, exe)))
              return Path.GetFullPath(path);
          }
        }
        throw new FileNotFoundException(new FileNotFoundException().Message, exe);
      }
      return Path.GetFullPath(exe);
    }

    private int Execute(string workingDir, Action<string> output, TextReader input, string programExe, string[] args)
    {
      ProcessStartInfo psi = new ProcessStartInfo();

      psi.UseShellExecute = false;
      psi.RedirectStandardError = true;
      psi.RedirectStandardOutput = true;

      // for plink.exe
      if (!Utilities.IsUnix && input == null)
        input = new StringReader("y");

      if (input != null)
        psi.RedirectStandardInput = true;

      psi.WindowStyle = ProcessWindowStyle.Hidden;
      psi.CreateNoWindow = true;
      psi.ErrorDialog = false;
      psi.WorkingDirectory = System.Environment.CurrentDirectory;

      foreach (string key in EnvironmentVariables.Keys)
      {
        if (psi.EnvironmentVariables.ContainsKey(key))
          psi.EnvironmentVariables[key] = EnvironmentVariables[key];
        else
          psi.EnvironmentVariables.Add(key, EnvironmentVariables[key]);
      }

      if (workingDir != null)
        psi.WorkingDirectory = workingDir;

      if (!Directory.Exists(psi.WorkingDirectory))
        Directory.CreateDirectory(psi.WorkingDirectory);

      string exe = Environment.ExpandEnvironmentVariables(programExe);

      if (Utilities.IsUnix)
      {
        if (!File.Exists(exe))
          exe = FindExePath(exe);

        if (!File.Exists(exe))
          throw new Exception(string.Format("Error: Unable to locate program '{0}'", exe));
      }

      for (int i = 0; i < args.Length; i++)
        args[i] = Environment.ExpandEnvironmentVariables(args[i]);

      if (IsMono)
      {
        psi.FileName = exe;
        psi.Arguments = String.Join(" ", args);
      }
      else
      {
        if (Remote)
        {
          psi.FileName = "cmd.exe";
          string runcmd = string.Format("{0}/Scripts/runcmd", psi.EnvironmentVariables["CARBON_MK_HOME"]);
          string remoteUser = Environment.ExpandEnvironmentVariables("%USERNAME%@arm_server");
          string rch = psi.EnvironmentVariables["CARBON_HOME"];
          string rmkh = psi.EnvironmentVariables["CARBON_MK_HOME"];
          string remoteWorkingDir = Carbon.ModelKitServices.Tools.FileUtilities.MapWindowsPathToLinux(workingDir);

          string evars = "";
          foreach (string key in EnvironmentVariables.Keys)
          {
            if (string.IsNullOrEmpty(evars))
              evars = string.Format("-env {0} {1}", key, EnvironmentVariables[key]);
            else
              evars += string.Format(" -env {0} {1}", key, EnvironmentVariables[key]);
          }

          input = new StringReader("y");
          string runCmdArgs = string.Format("-carbonHome {0} -carbonMKHome {1} -workingDir {2}", rch, rmkh, remoteWorkingDir);
          psi.Arguments = string.Format("/c plink.exe {0} {1} {2} {3} {4} {5}", remoteUser, runcmd, runCmdArgs, evars, programExe, String.Join(" ", args));
        }
        else
        {
          psi.FileName = "cmd.exe";
          psi.Arguments = string.Format("/c {0} {1}", exe, String.Join(" ", args));
        }
      }


      if (!Directory.Exists(psi.WorkingDirectory))
        throw new Exception(string.Format("Error: unable to execute command {0} {1}, no such working directory: {2}", psi.FileName, psi.Arguments, psi.WorkingDirectory));

      string currentDir = Directory.GetCurrentDirectory();
      int exitCode = -1;

      try
      {
        Logger.Log("Executing cd {2} && {0} {1}", psi.FileName, psi.Arguments, psi.WorkingDirectory);

        // For the moment, turn these off, there seems to be a random hang when 
        // redirecting stdout
        psi.RedirectStandardError = true;
        psi.RedirectStandardOutput = true;

        using (Process process = Process.Start(psi))
        {
          using (ManualResetEvent mreOut = new ManualResetEvent(false), mreErr = new ManualResetEvent(false))
          {
            try
            {
              process.OutputDataReceived += (o, e) =>
              {
                //Console.WriteLine("Output Received: {0}", e.Data);
                if (e.Data == null)
                  mreOut.Set();
                else if (output != null)
                  output(e.Data);
              };

              if (psi.RedirectStandardOutput)
                process.BeginOutputReadLine();

              process.ErrorDataReceived += (o, e) =>
              {
                //Console.WriteLine("Error Received: {0}", e.Data);
                if (e.Data == null)
                  mreErr.Set();
                else if (output != null)
                  output(e.Data);
              };

              if (psi.RedirectStandardError)
                process.BeginErrorReadLine();

              if (psi.RedirectStandardInput && input != null)
              {
                string line;
                while (null != (line = input.ReadLine()))
                  process.StandardInput.WriteLine(line);

                process.StandardInput.Close();
              }

              // Flush pipes
              process.WaitForExit();

              if (psi.RedirectStandardOutput)
                mreOut.WaitOne();

              if (psi.RedirectStandardError)
                mreErr.WaitOne();

              exitCode = process.ExitCode;

              return process.ExitCode;
            }
            catch (Exception ex)
            {
              Logger.Error(ex.Message);
              Logger.Error(ex.StackTrace);
            }
          }
        }
      }
      finally
      {
        Logger.Log("Completed ExitStatus: {0}", exitCode);
      }

      return -1;
    }
  }
}
