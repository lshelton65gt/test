﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;

using Carbon.ModelKitServices.ProcessRunner;

namespace Carbon.ModelKitServices
{
  [TestFixture, Description("Tests for Carbon.ProcessExecute")]
  public class TestProgram
  {
    public string CbuildVersion { get; set; }
    public string CarbonVersion { get; set; }

    [Test, Description("Validate Unix Path Conversion")]
    public void TestConversions()
    {
      string t1 = "/o/release/CMS/mainline";
      Assert.AreEqual(Model.ConvertUnixCarbonHomeToUNC(t1), "//dfs/acton/release/CMS/mainline");

      string t2 = "/o/work/eric/mainline";
      Assert.AreEqual(Model.ConvertUnixCarbonHomeToUNC(t2), "//dfs/acton/work/eric/mainline");

      string t3 = "/mnt/home/eric/mainline";
      Assert.AreEqual(Model.ConvertUnixCarbonHomeToUNC(t3), "//dfs/acton/home/eric/mainline");

      string t4 = "/w/farm228/eric/mainline/crap";
      Assert.AreEqual(Model.ConvertUnixCarbonHomeToUNC(t4), "//farm228/w/eric/mainline/crap");
    }

    [Test, Description("Test running cbuild -version")]
    public void TestAnswers()
    {
      Directory.SetCurrentDirectory(TestContext.CurrentContext.WorkDirectory);


      Assert.AreEqual(Model.isGcc(Model.ModelBuildPlatform.Gcc), true);

      Assert.AreEqual(!Model.isGcc(Model.ModelBuildPlatform.Gcc), false);

      Assert.AreEqual(Model.isVS2010(Model.ModelBuildPlatform.VS2010), true);

      Assert.AreEqual(!Model.isVS2010(Model.ModelBuildPlatform.VS2010), false);

      Assert.AreEqual(Model.isLinux(Model.ModelBuildPlatform.Gcc), true);
      Assert.AreEqual(!Model.isLinux(Model.ModelBuildPlatform.Gcc), false);

      Assert.AreEqual(Model.isWindows(Model.ModelBuildPlatform.VS2010), true);
      Assert.AreEqual(!Model.isWindows(Model.ModelBuildPlatform.VS2010), false);



      ModelKitAnswers mka = new ModelKitAnswers("z:\\a9.xml");

    }



    [Test, Description("Test running cbuild -version")]
    public void Test1()
    {
      string assemblyDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

      RunProgram rp = new RunProgram(null);
      Logger.Log("Test Environment OSVersion: {0}, Platform: {1}, IsUnix: {2}, IsMono: {3}, Is64Bit: {4}",
        System.Environment.OSVersion, Environment.OSVersion.Platform, Utilities.IsUnix, Utilities.IsMono, Environment.Is64BitOperatingSystem);


      ModelKitContext ctx = new ModelKitContext(assemblyDir, null);
      Logger.Log("ModelKit Dir: {0}", ctx.ModelKitDirectory);


      string[] args = new string[] { "version" };

      string cbuild = @"%CARBON_HOME%\bin\carbon.bat";
      if (RunProgram.IsMono)
        cbuild = @"%CARBON_HOME%/bin/carbon";

      int exitCode = rp.Run(TestContext.CurrentContext.WorkDirectory, Test1Output, null, cbuild, args);
      Assert.AreEqual(exitCode, 0, "Unexpected Non-Zero Exit Code");

      Assert.IsNotNullOrEmpty(CarbonVersion, "Never received Carbon Version");
    }
    public void Test1Output(string output)
    {
      CarbonVersion = output;
      Console.WriteLine(output);
    }

    [Test, Description("Test Cbuild")]
    public void TestCbuild()
    {
      if (Utilities.IsUnix)
      {
        Directory.SetCurrentDirectory(TestContext.CurrentContext.WorkDirectory);
        string assemblyDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        ModelKitContext ctx = new ModelKitContext(assemblyDir, null);

        string optionsFile = @"
-q
-g
-2001
%CARBON_HOME%/examples/twocounter/twocounter.v
";
        Carbon.ModelKitServices.Tools.CBuild.Run(Model.CbuildType.Explore, null, ctx, Model.ModelBuildPlatform.All, "designTest", optionsFile);

      }
    }



  }
}
