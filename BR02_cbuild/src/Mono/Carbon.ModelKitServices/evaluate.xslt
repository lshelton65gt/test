﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xmlns:CarbonExpr="carbon:EvalExpression"
   xmlns:Carbon="carbon:EvaluateDependency"
   xmlns:carbon="http://www.carbondesignsystems.com/XMLSchema/RegisterMeta/1.0"
   xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output omit-xml-declaration="yes" indent="yes" encoding="utf-8" />

  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="carbon:variable">
    <xsl:variable name="id">
      <xsl:value-of select="@name"/>
    </xsl:variable>
    <xsl:variable name="evalDefBegin" select="Carbon:BeginVar($id)" />
    <xsl:for-each select="carbon:case/carbon:when">
      <xsl:variable name="expr">
        <xsl:value-of select="@expr"/>
      </xsl:variable>
      <xsl:variable name="value">
        <xsl:value-of select="text()"/>
      </xsl:variable>
      <xsl:variable name="evalDef" select="Carbon:AddCase($id,$expr,$value)" />
    </xsl:for-each>
    <xsl:for-each select="carbon:case/carbon:otherwise">
      <xsl:variable name="value">
        <xsl:value-of select="text()"/>
      </xsl:variable>
      <xsl:variable name="evalDef" select="Carbon:AddOtherwise($id,$value)" />
    </xsl:for-each>
    <xsl:variable name="evalDefFinish" select="Carbon:FinishVar($id)" />
  </xsl:template>

  <xsl:template match="Register/@include">
    <xsl:choose>
      <xsl:when test="CarbonExpr:EvalExpression(@include)">
        <xsl:copy>
          <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise/>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="text()">
    <xsl:value-of select="Carbon:Expand(.)"/>
  </xsl:template>

  <xsl:template match="@*">
    <xsl:attribute name="{name()}">
      <xsl:value-of select="Carbon:Expand(.)"/>
    </xsl:attribute>
  </xsl:template>

</xsl:stylesheet>
