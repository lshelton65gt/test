﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

using System.Xml.Xsl;
using System.Xml.XPath;

using Mvp.Xml.Common.Xsl;
using Mvp.Xml.XInclude;

using Saxon.Api;

using ExtensionMethods;

namespace Carbon.ModelKitServices
{
  /// <summary>
  /// Handle Register meta data expansion
  /// </summary>
  public class RegisterMeta
  {
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


    public Dictionary<string, string> Variables { get; private set; }

    /// <summary>
    /// Construct a RegisterMeta object
    /// </summary>
    public RegisterMeta()
    {
      Variables = new Dictionary<string, string>();
    }

    /// <summary>
    /// Add a Variable to the map, referenced by ${var} syntax.
    /// </summary>
    /// <param name="name">Name of variable (case-sensitive)</param>
    /// <param name="value">Value to be replaced</param>
    public void AddVariable(string name, string value)
    {
      Variables[name] = value;
    }

    /// <summary>
    /// Main program test
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static int Main(string[] args)
    {
      try
      {
        Arguments arguments = Arguments.Parse(args);
        if (arguments == null)
          return 1;

        arguments.DisableLogging = true;

        try
        {
          string pass1XML = pass1 (arguments.InputXMLFileName, arguments);
          //Console.WriteLine(pass1XML);

          string expandedXML = expandXML(pass1XML);
          //Console.WriteLine(expandedXML);

          string finalXML = evaluateXML(expandedXML, arguments);

          string pass2XML = expandPass2XML(finalXML, arguments);

          if (string.IsNullOrEmpty(arguments.OutputFile))
            Console.WriteLine(pass2XML);
        }
        catch (Exception ex)
        {
          Logger.Error(ex.ToString());
          return 1;
        }
        return 0;
      }
      catch
      {
        return 1;
      }
    }

    private static void OnStdout(string stdout)
    {
      Logger.Log(stdout);
    }


    /// <summary>
    /// Elaborate the XML
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static int Elaborate(string[] args)
    {
      Carbon.ModelKitServices.ProcessRunner.RunProgram rp = new ProcessRunner.RunProgram(null);

      string platform = "Win";
      if (Utilities.IsUnix)
        platform = "Linux";

      string program = Path.Combine(Environment.ExpandEnvironmentVariables("%CARBON_HOME%"), "bin", "mono");
      string assembly = Path.Combine(Environment.ExpandEnvironmentVariables("%CARBON_HOME%"), platform, "bin", "RegisterTool.exe");

      List<string> allArgs = new List<string>();
      allArgs.Add(assembly);
      foreach (string arg in args)
        allArgs.Add(arg);


      int exitCode = rp.Run(".", OnStdout, null, program, allArgs.ToArray());

      return exitCode;
    }


    /// <summary>
    /// Evaluate XML variables and replace any variable references
    /// </summary>
    /// <param name="inputXML">Input XML string buffer to be evaluated</param>
    /// <param name="xmlAnswersFilePath">External XML Answers file (may be null)</param>
    /// <returns></returns>
    private static string pass1(string inputXmlFile, Arguments args)
    {
      MvpXslTransform xslt = new MvpXslTransform(true);
      XsltArgumentList arg = new XsltArgumentList();

      XmlReader xr = XmlReader.Create(new StringReader(Resource1.pass1));

      xslt.Load(xr, new XsltSettings(false, true), null);

      XmlDocument adoc = new XmlDocument();
      if (!string.IsNullOrEmpty(args.XMLAnswersFile) && File.Exists(args.XMLAnswersFile))
      {
        if (!args.DisableLogging)
          Logger.Log("Loading XML Answers File: {0}", args.XMLAnswersFile);
        adoc.Load(args.XMLAnswersFile);
      }

      Extensions obj = new Extensions(adoc);

      if (!string.IsNullOrEmpty(args.CarbonDBFileName) && File.Exists(args.CarbonDBFileName))
      {
        if (!args.DisableLogging)
          Logger.Log("Loading Carbon DB: {0}", args.CarbonDBFileName);
        CarbonDB.CarbonDB db = new CarbonDB.CarbonDB(args.CarbonDBFileName);
        obj.Database = db;
      }

      foreach (string name in args.Variables.Keys)
      {
        obj.Variables.Add(name, args.Variables[name]);
      }

      arg.AddExtensionObject("carbon:EvalExpression", obj);
      arg.AddExtensionObject("carbon:EvaluateDependency", obj);
			
		XmlReader incReader = new XIncludingReader (inputXmlFile);
		XmlDocument input = new XmlDocument();
      	input.Load (incReader);

      XmlInput xmlInput = new XmlInput(input.CreateNavigator());

      StringWriter sw = new StringWriter();

      XmlOutput xmlO = new XmlOutput(sw);

      XmlWriter xw = XmlWriter.Create(sw);

      xslt.Transform(xmlInput, arg, xmlO);

      XmlDocument outDoc = new XmlDocument();
      outDoc.LoadXml(sw.ToString());

      return outDoc.ToIndentedString();
    }

    private static string evaluateXML(string inputXML, Arguments args)
    {
      MvpXslTransform xslt = new MvpXslTransform(true);
      XsltArgumentList arg = new XsltArgumentList();

      XmlReader xr = XmlReader.Create(new StringReader(Resource1.evaluate));

      xslt.Load(xr, new XsltSettings(false, true), null);

      XmlDocument adoc = new XmlDocument();
      if (!string.IsNullOrEmpty(args.XMLAnswersFile) && File.Exists(args.XMLAnswersFile))
      {
        //Logger.Log("Loading XML Answers File: {0}", args.XMLAnswersFile);
        adoc.Load(args.XMLAnswersFile);
      }

      Extensions obj = new Extensions(adoc);

      if (!string.IsNullOrEmpty(args.CarbonDBFileName) && File.Exists(args.CarbonDBFileName))
      {
        if (!args.DisableLogging)
          Logger.Log("Loading Carbon DB: {0}", args.CarbonDBFileName);
        CarbonDB.CarbonDB db = new CarbonDB.CarbonDB(args.CarbonDBFileName);
        obj.Database = db;
      }

      foreach (string name in args.Variables.Keys)
      {
        obj.Variables.Add(name, args.Variables[name]);
      }

      arg.AddExtensionObject("carbon:EvalExpression", obj);
      arg.AddExtensionObject("carbon:EvaluateDependency", obj);

      XmlDocument input = new XmlDocument();
      input.LoadXml(inputXML);


      XmlInput xmlInput = new XmlInput(input.CreateNavigator());

      StringWriter sw = new StringWriter();

      XmlOutput xmlO = new XmlOutput(sw);

      XmlWriter xw = XmlWriter.Create(sw);

      xslt.Transform(xmlInput, arg, xmlO);

      XmlDocument outDoc = new XmlDocument();
      outDoc.LoadXml(sw.ToString());

      return outDoc.ToIndentedString();
    }



    /// <summary>
    /// Expand templatized XML (i.e. for loops, variables, etc.)
    /// </summary>
    /// <param name="inputXML"></param>
    /// <returns></returns>
    private static string expandXML(string inputXML)
    {
      try
      {
        Processor processor = new Processor();

        using (StringReader reader = new StringReader(inputXML))
        {
          TextWriter stringWriter = new StringWriter();

          stringWriter.Write(reader.ReadToEnd());

          stringWriter.Close();

          reader.Close();

          using (TextReader stringReader = new StringReader(stringWriter.ToString()))
          {
            XmlTextReader reader2 = new XmlTextReader(stringReader);
            reader2.XmlResolver = null;// Load the source document

            XdmNode input = processor.NewDocumentBuilder().Build(reader2);

            StringReader sri = new StringReader(Resource1.expand);

            // Create a transformer for the stylesheet.
            XsltTransformer transformer = processor.NewXsltCompiler().Compile(sri).Load();
            transformer.InputXmlResolver = null;

            // Set the root node of the source document to be the initial context node
            transformer.InitialContextNode = input;

            // Create a serializer
            Serializer serializer = new Serializer();

            StringWriter outWriter = new StringWriter();
            serializer.SetOutputWriter(outWriter);

            // Transform the source XML to the output StringWriter
            transformer.Run(serializer);

            return outWriter.ToString();
          }
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
        Console.WriteLine(ex.StackTrace);
        throw ex;
      }
    }


    private static string expandPass2XML(string inputXML, Arguments args)
    {
      MvpXslTransform xslt = new MvpXslTransform(true);
      XsltArgumentList arg = new XsltArgumentList();

      XmlReader xr = XmlReader.Create(new StringReader(Resource1.expand2));

      // to debug uncomment below
      //XmlReader xr = XmlReader.Create(@"Z:\dev\mainline\src\Mono\Carbon.ModelKitServices\expand2.xslt");

      xslt.Load(xr, new XsltSettings(false, true), null);

      XmlDocument adoc = new XmlDocument();
      if (!string.IsNullOrEmpty(args.XMLAnswersFile) && File.Exists(args.XMLAnswersFile))
      {
        //Logger.Log("Loading XML Answers File: {0}", args.XMLAnswersFile);
        adoc.Load(args.XMLAnswersFile);
      }

      Extensions obj = new Extensions(adoc);

      if (!string.IsNullOrEmpty(args.CarbonDBFileName) && File.Exists(args.CarbonDBFileName))
      {
        if (!args.DisableLogging)
          Logger.Log("Loading Carbon DB: {0}", args.CarbonDBFileName);
        CarbonDB.CarbonDB db = new CarbonDB.CarbonDB(args.CarbonDBFileName);
        obj.Database = db;
      }

      foreach (string name in args.Variables.Keys)
      {
        obj.Variables.Add(name, args.Variables[name]);
      }

      arg.AddExtensionObject("carbon:EvalExpression", obj);
      arg.AddExtensionObject("carbon:EvaluateDependency", obj);

      XmlDocument input = new XmlDocument();
      input.LoadXml(inputXML);


      XmlInput xmlInput = new XmlInput(input.CreateNavigator());

      StringWriter sw = new StringWriter();
      XmlOutput xmlO = new XmlOutput(sw);

      xslt.Transform(xmlInput, arg, xmlO);

      XmlDocument outDoc = new XmlDocument();
      outDoc.LoadXml(sw.ToString());

      string results = outDoc.ToIndentedString();

      if (!string.IsNullOrEmpty(args.OutputFile))
      {
        if (!args.DisableLogging)
          Logger.Log("Created file: {0}", args.OutputFile);
        using (FileStream fs = new FileStream(args.OutputFile, FileMode.Create))
        {
          using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
          {
            w.Write(results);
          }
        }
      }
      return results;

    }

    class Arguments
    {
      public string InputXMLFileName { get; set; }
      public string CarbonDBFileName { get; set; }
      public string XMLAnswersFile { get; set; }
      public string OutputFile { get; set; }
      public bool DisableLogging { get; set; }

      public Dictionary<string, string> Variables { get; set; }

      public Arguments()
      {
        Variables = new Dictionary<string, string>();
      }

      public static Arguments Parse(string[] args)
      {
        Arguments arguments = new Arguments();

        bool results = arguments.ParseCommandLine(args.ToList(), arguments);
        if (results)
          return arguments;
        else
          return null;
      }

      public static void Usage()
      {
        string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().Location;
        FileInfo fi = new FileInfo(assemblyName);
        string programName = Path.GetFileNameWithoutExtension(fi.Name);

        Console.WriteLine("\nUsage:\tRegisterTool.exe inputXMLFile [options]", programName);
        Console.WriteLine("\t-D Name=Value - Provide a variable definition");
        Console.WriteLine("\t-db libdesign[.io.db|.symtab.db|.gui.db] - Provide a Carbon database for context");
        Console.WriteLine("\t-xmlAnswers xmlAnswersFile - Provide XML answers");
        Console.WriteLine("\t-o outputfile.xml - Output filename");

        Console.WriteLine("");
      }


      bool ParseCommandLine(List<string> argList, Arguments arguments)
      {
        try
        {
          if (!ParseArguments(argList, arguments))
          {
            Console.WriteLine("Missing arguments");
            Usage();
            return false;
          }
        }
        catch (Exception ex)
        {
          Console.WriteLine("Error: {0}", ex.Message);
          return false;
        }

        int minRequiredArgs = 1;

        if (argList.Count < minRequiredArgs)
        {
          Console.WriteLine("Invalid usage, missing command line argument(s)");
          Usage();
          return false;
        }

        InputXMLFileName = argList[0];
        if (!File.Exists(InputXMLFileName))
        {
          Console.WriteLine("Error: No such file: {0}", InputXMLFileName);
          return false;
        }


        return true;
      }

      static bool ParseArguments(List<string> args, Arguments arguments)
      {
        if (args.Count < 1)
          return false;

        int totalArgs = args.Count;
        int argsProcessed = 0;
        int argIndex = 0;

        while (argsProcessed < totalArgs && argIndex < args.Count)
        {
          argsProcessed++;
          string arg = args[argIndex];
          switch (arg)
          {
            case "-D":
              args.RemoveAt(argIndex);
              string nameValue = NextArg(args, argIndex);
              char[] seps = { '=' };
              string[] parts = nameValue.Split(seps, StringSplitOptions.RemoveEmptyEntries);
              if (parts.Length == 2)
              {
                arguments.Variables[parts[0]] = parts[1];
              }
              argsProcessed++;
              break;

            case "-o":
              args.RemoveAt(argIndex);
              string outfileName = NextArg(args, argIndex);
              arguments.OutputFile = outfileName;
              argsProcessed++;
              break;

            case "-db":
              args.RemoveAt(argIndex);
              string dbFileName = NextArg(args, argIndex);
              arguments.CarbonDBFileName = dbFileName;
              argsProcessed++;
              break;

            case "-xmlAnswers":
              args.RemoveAt(argIndex);
              string xmlAnswers = NextArg(args, argIndex);
              arguments.XMLAnswersFile = xmlAnswers;
              argsProcessed++;
              break;


            default:
              argIndex++;
              break;
          }
        }

        return true;
      }

      static string NextArg(List<string> args, int i)
      {
        if (i < args.Count)
        {
          string v = args[i];
          args.RemoveAt(i);
          return v;
        }
        else
        {
          throw new Exception(string.Format("Error: Missing argument for {0}", args[i - 1]));
        }
      }
    }
  }
}

namespace ExtensionMethods
{
  /// <summary>
  /// Extension Methods
  /// </summary>
  public static class MyExtensions
  {
    /// <summary>
    /// Extension method for XmlDocument to return contents as indented string.
    /// </summary>
    /// <param name="doc"></param>
    /// <returns></returns>
    public static string ToIndentedString(this XmlDocument doc)
    {
      var stringWriter = new StringWriter(new StringBuilder());

      XmlWriterSettings ws = new XmlWriterSettings();

      ws.Indent = true;
      ws.Encoding = Encoding.UTF8;
      ws.OmitXmlDeclaration = true;

      using (XmlWriter writer = XmlWriter.Create(stringWriter, ws))
      {
        doc.Save(writer);
      }
      return stringWriter.ToString();
    }
  }
}

