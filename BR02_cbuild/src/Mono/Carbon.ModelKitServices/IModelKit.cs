﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Carbon.ModelKitServices
{
  /// <summary>
  /// The ModelKit Interface holder for Enumerations
  /// </summary>
  public abstract class ModelKitInterface
  {
    /// <summary>
    /// The Phases for creating a ModelKit
    /// </summary>
    public enum Phase
    {
      /// <summary>
      /// The First phase, always called, even if skipping a phase.
      /// </summary>
      Initialize,
      /// <summary>
      /// Create all necessary files required by AnalyzeAndTest and/or Compile
      /// </summary>
      Create,
      /// <summary>
      /// Analyze (pre-compile) the design, and perform any tests to ensure the model contains all
      /// the requested configuration information from the XML Answers file.
      /// </summary>
      AnalyzeAndTest,
      /// <summary>
      /// Compile the design with cbuild (for all platforms)
      /// </summary>
      Compile,
      /// <summary>
      /// Generate the Component (SoCDesigner, SystemC, etc.)
      /// </summary>
      GenerateComponent,
      /// <summary>
      /// Finished
      /// </summary>
      Finished
    }

    /// <summary>
    /// The Vendors, this will become the top level directory name
    /// in $CARBON_MK_HOME/
    /// </summary>
    public enum ModelKitVendors
    {
      /// <summary>
      /// ARM
      /// </summary>
      ARM,
      /// <summary>
      /// Arteris
      /// </summary>
      Arteris,
      /// <summary>
      /// Cadence
      /// </summary>
      Cadence,
      /// <summary>
      /// Carbon Design Systems
      /// </summary>
      Carbon,
      /// <summary>
      /// CEVA
      /// </summary>
      CEVA,
      /// <summary>
      /// Elliptic
      /// </summary>
      Elliptic,
      /// <summary>
      /// Mentor Graphics
      /// </summary>
      MentorGraphics,
      /// <summary>
      /// MIPS
      /// </summary>
      MIPS,
      /// <summary>
      /// Tensilica
      /// </summary>
      Tensilica,
      /// <summary>
      /// VeriSilicon
      /// </summary>
      VeriSilicon,
      /// <summary>
      /// Vivante
      /// </summary>
      Vivante
    }
  }

  /// <summary>
  /// The Interface which all ModelKits must implement, the execution flow is:
  /// Initialize(), Create(), AnalyzeAndTest(), Compile(), GenerateComponent() and Finished() in that order.
  /// </summary>
  /// <remarks>
  /// <code>
  /// The ModelKit flow is controlled by the following 6 steps, in order;
  /// 1. Initialize()
  /// 2. Create()
  /// 3. AnalyzeAndTest()
  /// 4. Compile()
  /// 5. GenerateComponent()
  /// 6. Finished()
  /// </code>
  /// </remarks>
  /// <see cref="IModelKit"/>
  public interface IModelKit
  {
    /// <summary>
    /// The Name of the Kit, I.E. "PL190"
    /// </summary>
    string Name { get; }

    /// <summary>
    /// The Version number of this kit (used for Independent Model Kits)
    /// </summary>
    string Version { get; }
    /// <summary>
    /// The Vendor this model kit is for
    /// </summary>
    Carbon.ModelKitServices.ModelKitInterface.ModelKitVendors Vendor { get; }

    /// <summary>
    /// The Minimum CBuild version from cbuild -verboseVersion, may be seperated by comma(s)
    /// <remarks>May be null or empty to indicate any version is acceptable</remarks>
    /// </summary>
    string MinimumCBuildVersion { get; }

    /// <summary>
    /// Runtime License Feature (i.e. CxRVML)
    /// </summary>
    string RuntimeLicenseFeature { get; }

    #region Methods
    /// <summary>
    /// Initialize - Always Called every time the assembly is loaded
    /// </summary>
    /// <param name="phase">The starting phase for this execution context</param>
    /// <param name="context">The ModelKit Context object</param>
    /// <example>Initialize is always called.  In this example we must
    /// manufacture the path to the verilog RTL, but the path is dependent upon the value
    /// of the RxPy answer, so we use that value and define an environment variable
    /// that cbuild will be able to access inside a .f file later on, we do this here
    /// in initialize becuase it is always called, even if we skip to the Compile step
    /// <code>
    /// void IModelKit.Initialize(ModelKitInterface.Phase phase, ModelKitContext context)
    /// {
    ///   string rxpy = context.Answers.RxPy;
    ///   string rtlRoot = string.Format("/usr/ARM_RELEASE_DATA/primecells/PL111/PL111-BU-0000-{0}-00rel0/PL111_VC/clcd_pl111/verilog/rtl_source", rxpy);
    ///   Environment.SetEnvironmentVariable("PL111_ROOT", rtlRoot);
    /// }
    /// </code>
    /// </example>
    void Initialize(Carbon.ModelKitServices.ModelKitInterface.Phase phase, Carbon.ModelKitServices.ModelKitContext context);

    /// <summary>
    /// Create any generated files required for compilation
    /// </summary>
    /// <param name="context">The ModelKit Context object</param>
    /// <returns>true to indicate success, false indicates failure and stops the flow</returns>
    /// <example>Create's role is to generate any domain specific file(s) required
    /// for AnalyzeAndTest or Compile in support of compiling the RTL.
    /// 
    /// For some ModelKits this means generate a Verilog source file, or a cbuild .f
    /// file, etc.  In this example the answer is controlled by a Verilog define, 
    /// so the technique here is to create a .f file which is included by the compilation.
    ///
    /// <code>
    /// void IModelKit.Create(ModelKitContext context)
    /// {
    ///    using(StreamWriter tw = File.CreateText("answers.f"))
    ///    {
    ///      if (context.Answers.Answers["useIntegratedDisplay"] == "Yes")
    ///        tw.WriteLine("+define+PL111_USE_INTEGRATED_DISPLAY");
    ///      tw.Close();
    ///    }
    ///    return true;
    /// }
    /// </code>
    /// </example>
    bool Create(Carbon.ModelKitServices.ModelKitContext context);

    /// <summary>
    /// Perform Analysis and Test the configuration against the answers.  This functionality 
    /// used to be handled in the Questions.xml file.
    /// </summary>
    /// <param name="context">The ModelKit Context object</param>
    /// <returns>true to indicate success, false indicates failure and stops the flow</returns>
    bool AnalyzeAndTest(Carbon.ModelKitServices.ModelKitContext context);

    /// <summary>
    /// Return the PGO information for this model
    /// </summary>
    /// <param name="context">The ModelKit Context object</param>
    /// <returns>true to indicate success, false indicates failure and stops the flow</returns>
    /// <example>In this example, we simply reference the test driver source file.
    /// public ModelKitPGO IModelKit.PGOInformation(ModelKitContext context)
    /// {
    ///   return new ModelKitPGO(context, "pl111.cxx");
    /// }
    /// </example>
    ModelKitPGO PGOInformation(Carbon.ModelKitServices.ModelKitContext context);

    /// <summary>
    /// Compile the model for real now
    /// </summary>
    /// <param name="context">The ModelKit Context object</param>
    /// <returns></returns>
    bool Compile(Carbon.ModelKitServices.ModelKitContext context);

    /// <summary>
    /// Make the Component now
    /// </summary>
    /// <param name="context">The ModelKit Context object</param>
    /// <returns>true to indicate success, false indicates failure and stops the flow</returns>
    bool GenerateComponent(Carbon.ModelKitServices.ModelKitContext context);

    /// <summary>
    /// We are done, handle any packaging here.
    /// </summary>
    /// <param name="context">The ModelKit Context object</param>
    /// <returns>true to indicate success, false indicates failure and stops the flow</returns>
    bool Finished(Carbon.ModelKitServices.ModelKitContext context);
   
    /// <summary>
    /// Supply files which need to get into the end-user tarball here, 
    /// </summary>
    /// <param name="retainFiles">Use retainFiles.AddFile()</param>
    void RetainFiles(Carbon.ModelKitServices.ModelKitRetainFiles retainFiles);

    #endregion
  }

}
