// -*-c++-*-
/******************************************************************************
 Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "cfg/CarbonCfg.h"
#include "util/UtIOStream.h"
#include "SoCDExportGenerator.h"

int main(int argc, char** argv)
{
  if (argc != 2) {
    UtIO::cout() << "usage: " << argv[0] << " <ccfg file>" << UtIO::endl;
    exit(1);
  }

  // Open the ccfg file
  CarbonCfgTop cfg;
  if (cfg.read(argv[1]) != eCarbonCfgSuccess) {
    UtIO::cout() << "Error opening " << argv[1] << UtIO::endl;
    exit(1);
  }

  SoCDExportGenerator generator(cfg);
  if (!generator.gen()) {
    UtIO::cout() << "SystemC export generation failed" << UtIO::endl;
    exit(1);
  }

  UtIO::cout() << "SystemC export generation complete" << UtIO::endl;
  return 0;
}
