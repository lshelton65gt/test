// -*-c++-*-
/******************************************************************************
 Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __SOCDEXPORTGENERATOR_H__
#define __SOCDEXPORTGENERATOR_H__

#include "util/UtString.h"
#include "util/UtArray.h"

class CarbonCfg;
class UtOFStream;
class SoCDExportXtorInstance;
class SoCDExportXtorLibrary;
class UtLicense;

//! Exports a SystemC wrapper around a SoCD component
class SoCDExportGenerator
{
public:
  SoCDExportGenerator(const CarbonCfg& cfg);
  ~SoCDExportGenerator();

  //! Generate the wrapper
  bool gen();

  //! Namespace of export library
  static const char* scExportLibNamespace;

private:

  //! Configure the wrapper based on the contents of the .ccfg file
  bool configure();
  //! Write the wrapper file
  bool writeFile();
  //! Write the preamble (includes, etc.)
  bool writePreamble(UtOFStream& file);
  //! Write the class definition
  bool writeClass(UtOFStream& file);
  //! Add all transactors to the wrapper
  bool addXtors();
  //! Check out a license
  bool checkoutLicense();
  //! Check in a license if checked out
  void checkinLicense();


  const CarbonCfg& mCfg;                //!< The ccfg object
  UtString mCompName;                   //!< The name of the component to be generated
  SoCDExportXtorLibrary* mXtorLibrary;  //!< The library to allocate transactor instances
  bool mLicenseCheckedOut;              //!< Was a license checked out successfully?

  class LicenseCB;
  LicenseCB* mLicenseCB;                //!< Message callback object for licensing
  UtLicense* mLicense;                  //!< Licensing object
};

#endif
