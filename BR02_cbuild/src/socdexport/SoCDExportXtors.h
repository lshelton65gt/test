// -*-c++-*-
/******************************************************************************
 Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __SOCDEXPORTXTORS_H__
#define __SOCDEXPORTXTORS_H__

#include "util/UtString.h"
#include "util/UtArray.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/Loop.h"

class CarbonCfgESLPort;
class CarbonCfgXtorInstance;
class CarbonCfgXtor;

//! Class to represent an instance of a transactor
/*!
  In this context, transactor is a converter between CASI and SystemC
  representations of a port.  That underlying port might be a simple
  signal port or a transaction port, but regardless it needs a
  CASI/SystemC transactor.
 */
class SoCDExportXtorInstance
{
public:
  SoCDExportXtorInstance(const UtString& type,
                         const UtString& instance,
                         const UtString& socdPort)
    : mType(type),
      mInstance(instance),
      mSoCDPort(socdPort),
      mClock(NULL) {}
  ~SoCDExportXtorInstance() {}

  const UtString& getType() const { return mType; }
  const UtString& getInstance() const { return mInstance; }
  const UtString& getSoCDPort() const { return mSoCDPort; }
  const SoCDExportXtorInstance* getClock() const { return mClock; }

  void putClock(const SoCDExportXtorInstance* clock) { mClock = clock; }

private:
  const UtString mType;                 //!< Transactor class type
  const UtString mInstance;             //!< Instance of the transactor in the wrapper
  const UtString mSoCDPort;             //!< SoCD port to connect to
  const SoCDExportXtorInstance* mClock; //!< Clock transactor associated with this transactor (optional)
};

//! Library class to allocate transactor instances
class SoCDExportXtorLibrary
{
public:
  SoCDExportXtorLibrary();
  ~SoCDExportXtorLibrary();

  //! Creates a SoCD/SystemC transactor for a simple ESL connection
  bool createTransactor(CarbonCfgESLPort* eslPort);

  //! Creates a SoCD/SystemC transactor for a CASI transactor
  bool createTransactor(CarbonCfgXtorInstance* casiXtorInstance);

  typedef UtHashMap<UtString, SoCDExportXtorInstance*> XtorInstanceMap;
  //! Loop over all created transactor instances
  XtorInstanceMap::SortedLoop loopXtorInstances() { return mXtorInstances.loopSorted(); }

  typedef UtHashSet<UtString> StringSet;
  //! Loop over all headers required for the transactors
  StringSet::SortedLoop loopXtorHeaders() { return mXtorHeaders.loopSorted(); }

  //! Associate clocked transactors with the corresponding SoCD component clocks
  bool associateClocks();

private:
  //! Create a transactor instance, choosing a suitable variable name
  SoCDExportXtorInstance* createTransactor(const UtString& type,
                                           const UtString& socdPort);

  //! Create a C identifier for a SoCD signal name
  void createCIdentifier(const UtString& signalName, UtString* identifier);

  //! Create an AHB transactor instance
  bool createAHBTransactor(CarbonCfgXtorInstance* casiXtorInstance,
                           CarbonCfgXtor* casiXtor,
                           const UtString& casiXtorType,
                           const UtString& socdPort);

  XtorInstanceMap mXtorInstances;      //!< Allocated transactor instances, keyed by SoCD port name
  StringSet mXtorHeaders;              //!< Headers required for transactors

  typedef std::pair<CarbonCfgXtorInstance*, SoCDExportXtorInstance*> XtorPair;
  // UtArray can only store pointers...
  typedef UtArray<XtorPair*> XtorPairArray;
  //! Transactors whose clocks need to be dealt with
  XtorPairArray mClockedXtors;
};

#endif
