// -*-c++-*-
/******************************************************************************
 Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "SoCDExportXtors.h"
#include "SoCDExportGenerator.h"
#include "cfg/CarbonCfg.h"

SoCDExportXtorLibrary::SoCDExportXtorLibrary()
{
  // All components have a clk-in port, so add the transactor for that.
  createTransactor("SCClockPort", "clk-in");
  mXtorHeaders.insert("SCClockPort.h");
}

SoCDExportXtorLibrary::~SoCDExportXtorLibrary()
{
  for (XtorInstanceMap::iterator iter = mXtorInstances.begin(); iter != mXtorInstances.end(); ++iter) {
    delete iter->second;
  }
  for (XtorPairArray::iterator iter = mClockedXtors.begin(); iter != mClockedXtors.end(); ++iter) {
    delete *iter;
  }
}

bool SoCDExportXtorLibrary::createTransactor(CarbonCfgESLPort* eslPort)
{
  // We need the RTL port to determine direction and width
  CarbonCfgRTLPort* rtlPort = eslPort->getRTLPort();
  
  // Determine the base class type depending on the direction of the port
  CarbonCfgRTLPortType portType = rtlPort->getType();
  UtString baseClass;
  switch (portType) {
  case eCarbonCfgRTLInput:
    baseClass = "SystemC2CASISignal";
    break;
  case eCarbonCfgRTLOutput:
    baseClass = "CASISignal2SystemC";
    break;
  case eCarbonCfgRTLInout:
    UtIO::cout() << "Port " << rtlPort->getName() << " is bidirectional.  This is not supported" << UtIO::endl;
    return false;
  }

  // The templated type depends on the width
  UInt32 portWidth = rtlPort->getWidth();
  UtString templateType;
  if (portWidth == 1) {
    templateType = "bool";
  } else if (portWidth <= 64) {
    templateType << "sc_uint<" << portWidth << "> ";
  } else {
    templateType << "sc_biguint<" << portWidth << "> ";
  }

  UtString classType = baseClass + "<" + templateType + ">";
  const UtString& socdPort = eslPort->getName();

  createTransactor(classType, socdPort);

  // Track the required header
  mXtorHeaders.insert(baseClass + ".h");

  return true;
}

bool SoCDExportXtorLibrary::createTransactor(CarbonCfgXtorInstance* casiXtorInstance)
{
  // Only certain types of transactors are currently supported
  CarbonCfgXtor* casiXtor = casiXtorInstance->getType();
  const UtString casiXtorType = casiXtor->getName();
  const UtString& socdPort = casiXtorInstance->getName();

  bool success = true;
  if (casiXtorType == "Debug_Master") {
    // This are used for passing debug transactions over
    // non-transaction ports.  We have a matching CASI->SystemC
    // transactor for this.
    createTransactor("SystemCDebugMasterPort", socdPort);
    mXtorHeaders.insert("SystemCDebugMasterPort.h");
  } else if (casiXtorType == "Debug_Slave") {
    // Similar to above
    createTransactor("SystemCDebugSlavePort", socdPort);
    mXtorHeaders.insert("SystemCDebugSlavePort.h");
  } else if (casiXtorType == "Reset_Input") {
    // A Reset_Input transactor is a reset generator whose RTL port
    // can also be driven directly via an ESL port.  We want to expose
    // this port, since a SystemC user has no way to reset the
    // simulation other than the automatic reset sequence that's run
    // at the beginning of the simulation.
    createTransactor("SystemC2CASISignal<bool>", socdPort);
    mXtorHeaders.insert("SystemC2CASISignal.h");
  } else if ((casiXtorType.find("AHB_") == 0) &&
             ((casiXtorType.find("_FT2S") != UtString::npos) ||
              (casiXtorType.find("_FS2T") != UtString::npos))) {
    // This appears to be an AHBv2 transactor
    success = createAHBTransactor(casiXtorInstance, casiXtor, casiXtorType, socdPort);
  } else {
    // All other transactor types are an error for now
    UtIO::cout() << "Transactor instance " << socdPort << " is of unsupported transactor type " << casiXtorType << UtIO::endl;
    success = false;
  }

  return success;
}

bool SoCDExportXtorLibrary::associateClocks()
{
  // Some CASI transactors (e.g. AHB, AXI) have clocks associated with
  // them.  The transactors are run in that clock's
  // communicate()/update() methods.  We need to duplicate that in the
  // wrapper.  Each CASI<->SystemC transactor for one of these CASI
  // transactors has an sc_in port for its clock.  That needs to be
  // bound to the clock signal of the CASI<->SystemC transactor for
  // the corresponding CASI clock port.
  //
  // This is represented in the .ccfg file entry for each transactor
  bool success = true;
  for (XtorPairArray::iterator iter = mClockedXtors.begin(); iter != mClockedXtors.end(); ++iter) {
    CarbonCfgXtorInstance* casiXtorInstance = (*iter)->first;
    SoCDExportXtorInstance* xtorInstance = (*iter)->second;
    // Look up the transactor for this ESL port.  If there is no
    // clock, use the component clock.
    UtString clockName = "clk-in";
    CarbonCfgXtorInstance* casiClockXtorInstance = casiXtorInstance->getClockMaster();
    if (casiClockXtorInstance != NULL) {
      clockName = casiClockXtorInstance->getName();
    }
    XtorInstanceMap::const_iterator findIter = mXtorInstances.find(clockName);
    if (findIter == mXtorInstances.end()) {
      UtIO::cout() << "Can't find clock " << clockName << UtIO::endl;
      success = false;
    } else {
      const SoCDExportXtorInstance* clockXtorInstance = findIter->second;
      xtorInstance->putClock(clockXtorInstance);
    }
  }
  
  return success;
}

SoCDExportXtorInstance* SoCDExportXtorLibrary::createTransactor(const UtString& type,
                                                                const UtString& socdPort)
{
  // Get an instance name based on the SoCD port name
  UtString instance;
  createCIdentifier(socdPort, &instance);
  
  SoCDExportXtorInstance* xtorInstance = new SoCDExportXtorInstance(type, instance, socdPort);
  mXtorInstances[socdPort] = xtorInstance;
  return xtorInstance;
}

void SoCDExportXtorLibrary::createCIdentifier(const UtString& signalName, UtString* identifier)
{
  // TODO - check for duplicates

  identifier->clear();
  // If first character is a digit, prefix with underscore
  if (isdigit(signalName[0])) {
    *identifier << '_';
  }

  // Now we know that all alphanumeric characters plus underscore are
  // valid.  Copy those as-is, and replace all others with
  // underscores.
  for (UtString::const_iterator iter = signalName.begin(); iter != signalName.end(); ++iter) {
    const char& c = *iter;
    if (isalnum(c)) {
      *identifier << c;
    } else {
      *identifier << '_';
    }
  }
}

bool SoCDExportXtorLibrary::createAHBTransactor(CarbonCfgXtorInstance* casiXtorInstance,
                                                CarbonCfgXtor* casiXtor,
                                                const UtString& casiXtorType,
                                                const UtString& socdPort)
{
  // We need to determine the direction, type (AHB or AHBLite), and
  // side (master or slave).
  UtString direction;
  UtString type;
  UtString side;

  if (casiXtorType == "AHB_Slave_FT2S") {
    // Note that the direction is the reverse of the CASI xtor.
    // Everything is connected at the signal level in SystemC, albeit
    // within the transactor objects.
    direction = "S2T";
    type = "Full";
    side = "Slave";
  } else if (casiXtorType == "AHB_Slave_FS2T") {
    direction = "T2S";
    type = "Full";
    side = "Slave";
  } else if (casiXtorType == "AHB_Master_FT2S") {
    direction = "S2T";
    type = "Full";
    side = "Master";
  } else if (casiXtorType == "AHB_Master_FS2T") {
    direction = "T2S";
    type = "Full";
    side = "Master";
  } else if (casiXtorType == "AHB_Lite_Slave_FT2S") {
    direction = "S2T";
    type = "Lite";
    side = "Slave";
  } else if (casiXtorType == "AHB_Lite_Slave_FS2T") {
    direction = "T2S";
    type = "Lite";
    side = "Slave";
  } else if (casiXtorType == "AHB_Lite_Master_FT2S") {
    direction = "S2T";
    type = "Lite";
    side = "Master";
  } else if (casiXtorType == "AHB_Lite_Master_FS2T") {
    direction = "T2S";
    type = "Lite";
    side = "Master";
  } else {
    UtIO::cout() << "Unknown AHB transactor type " << casiXtorType << UtIO::endl;
    return false;
  }

  // We need the data width, too.  Look at the widths of the hrdata
  // and hwdata signals.  If they're different, it's an error.  Also,
  // while there could theoretically be transactors with either hrdata
  // or hwdata disconnected, I don't think that's the case with any of
  // the models we generate, so flag that as an error, too.
  //
  // First, find the port index on the transactor for each port
  UInt32 hrdataIndex, hwdataIndex;
  if (!casiXtor->findPort("hrdata", &hrdataIndex)) {
    UtIO::cout() << "Transactor type " << casiXtorType << " has no 'hrdata' port" << UtIO::endl;
    return false;
  }
  if (!casiXtor->findPort("hwdata", &hwdataIndex)) {
    UtIO::cout() << "Transactor type " << casiXtorType << " has no 'hwdata' port" << UtIO::endl;
    return false;
  }
  // Get the connections associated with these ports on this transactor instance
  CarbonCfgXtorConn* hrdataConn = casiXtorInstance->getConnection(hrdataIndex);
  if (hrdataConn == NULL) {
    UtIO::cout() << "Port 'hrdata' on transactor instance " << socdPort << " is not connected" << UtIO::endl;
    return false;
  }
  CarbonCfgXtorConn* hwdataConn = casiXtorInstance->getConnection(hwdataIndex);
  if (hwdataConn == NULL) {
    UtIO::cout() << "Port 'hwdata' on transactor instance " << socdPort << " is not connected" << UtIO::endl;
    return false;
  }
  // Get the RTL ports and check their widths.  If they're not the same, it's an error.
  CarbonCfgRTLPort* hrdataRTLPort = hrdataConn->getRTLPort();
  CarbonCfgRTLPort* hwdataRTLPort = hwdataConn->getRTLPort();
  UInt32 width = hrdataRTLPort->getWidth();
  if (width != hwdataRTLPort->getWidth()) {
    UtIO::cout() << "Transactor instance " << socdPort << " has different read and write data widths" << UtIO::endl;
    return false;
  }

  // We now have everything we need to create the templated AHBv2 transactor type
  UtString xtorDataType;
  xtorDataType << "AHBv2_" << direction << "<" << width << ", "
               << SoCDExportGenerator::scExportLibNamespace << "::AHBType" << type << ", "
               << SoCDExportGenerator::scExportLibNamespace << "::AHBSide" << side << ">";
  SoCDExportXtorInstance* xtorInstance = createTransactor(xtorDataType, socdPort);

  // The AHB transactor is clocked.  We need to associate it with a
  // clock on the SoCD component.  This needs to be done after all
  // transactors are processed, since eventually we'll support clock
  // input transactors.
  mClockedXtors.push_back(new XtorPair(casiXtorInstance, xtorInstance));

  // Track the header we need for this
  UtString header;
  header << "AHBv2_" << direction << ".h";
  mXtorHeaders.insert(header);

  return true;
}
