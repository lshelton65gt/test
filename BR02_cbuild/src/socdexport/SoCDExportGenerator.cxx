// -*-c++-*-
/******************************************************************************
 Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "cfg/CarbonCfg.h"
#include "util/UtIOStream.h"
#include "util/UtLicense.h"
#include "util/UtLicenseMsg.h"
#include "SoCDExportGenerator.h"
#include "SoCDExportXtors.h"

const char* SoCDExportGenerator::scExportLibNamespace = "CarbonSCExport";

class SoCDExportGenerator::LicenseCB : public UtLicense::MsgCB
{
public:
  LicenseCB() {}
  virtual ~LicenseCB() {}

  virtual void waitingForLicense(const char*) {}
  virtual void queuedLicenseObtained(const char*) {}
  virtual void requeueLicense(const char*) {}
  virtual void relinquishLicense(const char*) {}
  virtual void exitNow(const char* reason)
  {
    UtIO::cout() << "License error: " << reason << UtIO::endl;
    exit(1);
  }
};

SoCDExportGenerator::SoCDExportGenerator(const CarbonCfg& cfg)
  : mCfg(cfg),
    mLicenseCheckedOut(false)
{
  mXtorLibrary = new SoCDExportXtorLibrary;
  mLicenseCB = new LicenseCB;
  mLicense = new UtLicense(mLicenseCB);
}

SoCDExportGenerator::~SoCDExportGenerator()
{
  delete mXtorLibrary;
  delete mLicenseCB;
  delete mLicense;
}

bool SoCDExportGenerator::gen()
{
  if (!checkoutLicense()) {
    return false;
  }
  if (!configure()) {
    return false;
  }
  if (!writeFile()) {
    return false;
  }

  checkinLicense();
  return true;
}

bool SoCDExportGenerator::configure()
{
  mCompName = mCfg.getCompName();

  if (!addXtors()) {
    return false;
  }

  if (!mXtorLibrary->associateClocks()) {
    return false;
  }

  return true;
}

bool SoCDExportGenerator::writeFile()
{
  UtString filename;
  filename << "mx.export." << mCompName << ".h";

  UtOFStream file(filename.c_str());
  if (!file.is_open()) {
    UtIO::cout() << "Error writing " << filename << UtIO::endl;
    return false;
  }

  writePreamble(file);
  writeClass(file);

  // Close include guard
  file << UtIO::endl;
  file << "#endif" << UtIO::endl;

  file.close();

  return true;
}

bool SoCDExportGenerator::writePreamble(UtOFStream& file)
{
  file << "/*****************************************************************************" << UtIO::endl;
  file << " Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved." << UtIO::endl;
  file << UtIO::endl;
  file << " THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON" << UtIO::endl;
  file << " DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY" << UtIO::endl;
  file << " THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE" << UtIO::endl;
  file << " APPEARS IN ALL COPIES OF THIS SOFTWARE." << UtIO::endl;
  file << "******************************************************************************/" << UtIO::endl;
  file << UtIO::endl;
  file << "// SystemC export wrapper for SoC Designer component " << mCompName << UtIO::endl;
  UtString timeStr;
  OSGetTimeStr("%b %d, %Y  %H:%M:%S", &timeStr);
  file << "// Generated " << timeStr << UtIO::endl;
  file << UtIO::endl;
  file << "#ifndef __MX_EXPORT_" << mCompName << "_H__" << UtIO::endl;
  file << "#define __MX_EXPORT_" << mCompName << "_H__" << UtIO::endl;
  file << UtIO::endl;

  // Includes
  file << "#include \"systemc.h\"" << UtIO::endl;
  file << "#include \"WrapperBase.h\"" << UtIO::endl;
  // Any headers for the various transactor types
  for (SoCDExportXtorLibrary::StringSet::SortedLoop l(mXtorLibrary->loopXtorHeaders()); !l.atEnd(); ++l) {
    const UtString& header = *l;
    file << "#include \"" << header << "\"" << UtIO::endl;
  }
  file << UtIO::endl;

  return true;
}

bool SoCDExportGenerator::writeClass(UtOFStream& file)
{
  file << "class " << mCompName << " : public " << scExportLibNamespace << "::WrapperBase" << UtIO::endl;
  file << "{" << UtIO::endl;
  file << "public:" << UtIO::endl;

  // Constructor.  We need to declare this explicitly so we can use
  // the instance name passed in.
  file << "  SC_HAS_PROCESS(" << mCompName << ");" << UtIO::endl;
  file << "  " << mCompName << "(sc_core::sc_module_name instance)" << UtIO::endl;
  // Variant and IP provider are not used
  file << "    : " << scExportLibNamespace << "::WrapperBase(instance, \"" << mCompName << "\", \"" << mCfg.getCompVersion() << "\", \"\", \"\")";

  // Add initializers for the transactor instances
  for (SoCDExportXtorLibrary::XtorInstanceMap::SortedLoop l(mXtorLibrary->loopXtorInstances()); !l.atEnd(); ++l) {
    const SoCDExportXtorInstance* xtorInstance = l.getValue();
    file << "," << UtIO::endl;
    file << "      " << xtorInstance->getInstance() << "(\"" << xtorInstance->getSoCDPort() << "\", this)";
  }

  file << UtIO::endl;
  file << "  {" << UtIO::endl;
  // If any transactors have clocks associated with them, bind the
  // clock transactor's clock signal to this transactor's clock
  // signal.
  for (SoCDExportXtorLibrary::XtorInstanceMap::SortedLoop l(mXtorLibrary->loopXtorInstances()); !l.atEnd(); ++l) {
    const SoCDExportXtorInstance* xtorInstance = l.getValue();
    const SoCDExportXtorInstance* clockXtorInstance = xtorInstance->getClock();
    if (clockXtorInstance != NULL) {
      file << "    " << xtorInstance->getInstance() << ".clk(" << clockXtorInstance->getInstance() << ".clk);" << UtIO::endl;
    }
  }
  
  file << "  }" << UtIO::endl;

  // Now the transactor declarations
  file << UtIO::endl;
  for (SoCDExportXtorLibrary::XtorInstanceMap::SortedLoop l(mXtorLibrary->loopXtorInstances()); !l.atEnd(); ++l) {
    const SoCDExportXtorInstance* xtorInstance = l.getValue();
    file << "  " << scExportLibNamespace << "::" << xtorInstance->getType() << " " << xtorInstance->getInstance() << ";" << UtIO::endl;
  }
  


  file << "};" << UtIO::endl;
  file << UtIO::endl;
  return true;
}

bool SoCDExportGenerator::addXtors()
{
  bool success = true;

  for (UInt32 i = 0; i < mCfg.numESLPorts(); ++i) {
    CarbonCfgESLPort* eslPort = mCfg.getESLPort(i);
    if (!mXtorLibrary->createTransactor(eslPort)) {
      success = false;
    }
    // UtIO::cout() << "Found ESL port " << eslPort->getName() << UtIO::endl;
  }  

  for (UInt32 i = 0; i < mCfg.numXtorInstances(); ++i) {
    CarbonCfgXtorInstance* xtorInstance = mCfg.getXtorInstance(i);
    if (!mXtorLibrary->createTransactor(xtorInstance)) {
      success = false;
    }
    // UtIO::cout() << "Found transactor instance " << xtorInstance->getName() << UtIO::endl;
  }  

  return success;
}

bool SoCDExportGenerator::checkoutLicense()
{
  if (!mLicenseCheckedOut) {
    UtString reason;
    // Try an internal license first.  If that fails, try the actual
    // feature name.
    if (mLicense->checkout(UtLicense::eDIAGNOSTICS, &reason)) {
      mLicenseCheckedOut = true;
    } else if (mLicense->checkout(UtLicense::eSoCDSystemCExport, &reason)) {
      mLicenseCheckedOut = true;
    } else {
      UtIO::cout() << "License checkout failed: " << reason << UtIO::endl;
    }
  }
  return mLicenseCheckedOut;
}

void SoCDExportGenerator::checkinLicense()
{
  if (mLicenseCheckedOut) {
    mLicense->release(UtLicense::eSoCDSystemCExport);
    mLicenseCheckedOut = false;
  }
}
