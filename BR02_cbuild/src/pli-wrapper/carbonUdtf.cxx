// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implementation of User-Defined Task/Functions to access Carbon models from
  Verilog simulators.
*/


extern "C" {
#define PLI_EXTRAS
#define PROTO_PARAMS(params) params
#define EXTERN extern "C"
#include "pli-wrapper/veriuser.h"
#undef PROTO_PARAMS
#undef EXTERN
}

#include "pli-wrapper/CarbonPliWrapper.h"
#include "shell/carbon_dbapi.h"


static SInt64 sGetTfLong(int pliParm)
{
  SInt64 value;
  UInt32 low;
  PLI_INT32 high;

  low = tf_getlongp(&high, pliParm);
  value = high;
  value = (value << 32) + low;
  return value;
}

//! Local helper to check that a signal name argument has the correct type.
//! Signal names must be given as strings, or read-only concats of strings.
static bool validSignalArgType(int argNumber)
{
  PLI_INT32 argType = tf_typep(argNumber);
  return (argType == TF_READONLY) ||
         (argType == TF_STRING);
}

//! Local helper to check that a value argument has the correct type.
//! Values can be any read/write value (but not TF_STRING or TF_NULLPARAM).
static bool validArgType(int argNumber)
{
  PLI_INT32 argType = tf_typep(argNumber);
  return (argType == TF_READONLY) ||
         (argType == TF_READWRITE);
}

//! Count the number of non-NULL arguments.
//  tf_nump() is not reliable when checking for 0 arguments, because:
//    $foo;
//  is a task call with no arguments, while this:
//     $foo();
//  is a task call with a single argument of type TF_NULLPARAM.
static UInt32 sNonNullParameters()
{
  UInt32 numberOfNonNullParameters = 0;
  for (int i = 1; i <= tf_nump(); i++) {
    if (tf_typep(i) != TF_NULLPARAM) {
      ++numberOfNonNullParameters;
    }
  }
  return numberOfNonNullParameters;
}

class CarbonPliTaskData
{
public: CARBONMEM_OVERRIDES
  CarbonPliTaskData(CarbonObjectID* model, CarbonNetID* net, CarbonMemoryID* mem, CarbonPliBuffer* buffer)
  {
    mModel = model;
    if (net != NULL) {
      mNet = net;
    }
    if (mem != NULL) {
      mMemory = mem;
    }
    mBuffer = buffer;
  };

  ~CarbonPliTaskData()
  {
    if (mBuffer != NULL)
      delete mBuffer;
  }

  CarbonObjectID*  getModel()  { return mModel; }
  CarbonPliBuffer* getBuffer() { return mBuffer; }
  CarbonNetID*     getNet()    { return mNet; }
  CarbonMemoryID*  getMemory() { return mMemory; }

private:
  CarbonObjectID*  mModel;
  CarbonPliBuffer* mBuffer;
  union {
    CarbonNetID*     mNet;
    CarbonMemoryID*  mMemory;
  };
};


static CarbonPliTaskData* getTaskData(int nameParm, int valueParm = -1, bool isMemory = false)
{
  CarbonPliModel*    instance;
  CarbonNetID*       net = NULL;
  CarbonMemoryID*    mem = NULL;
  CarbonPliWrapperContext*  context = CarbonPliWrapperContextManager::getPliContext();
  CarbonPliTaskData* data = NULL;
  CarbonPliBuffer*   buffer = NULL;
  char* path = NULL;
  char* relativePath = NULL;
  char* taskPath = NULL;
  int numBits, numUInt32s, pliGroups;

  // Lookup the Carbon model instance and net/memory handle for the given path.
  CarbonPliWrapperContext::sAllocateAndCopy(&taskPath, tf_mipname());
  CarbonPliWrapperContext::sAllocateAndCopy(&path, tf_getcstringp(nameParm));
  instance = context->findModel(taskPath, path, &relativePath);
  if (instance != NULL)
  {
    if (isMemory) {
      mem = carbonFindMemory(instance->getModel(), relativePath);
    }
    else {
      net = carbonFindNet(instance->getModel(), relativePath);
    }
  }

  if (instance == NULL || (net == NULL && mem == NULL))
  {
    tf_error("%s is not a valid Carbon Model.", tf_getcstringp(nameParm));
    tf_dofinish();
  }
  else
  {
    bool ok = true;

    // if there is a value parameter, get a buffer for it
    if (valueParm != -1)
    {
      if (isMemory) {
        numBits = carbonMemoryRowWidth(mem);
      } else {
        CarbonObjectID* obj = instance->getModel();
        CarbonDB* db = carbonGetDB(obj);
        const CarbonDBNode* node = carbonNetGetDBNode(obj, net);
        numBits = carbonDBGetWidth(db, node);
      }
      numUInt32s = (isMemory) ? carbonMemoryRowNumUInt32s(mem) : carbonGetNumUInt32s(net);
      bool useDrive = (isMemory) ? false : carbonIsTristate(net);
      pliGroups = (tf_sizep(valueParm) + 31) / 32;

      if (pliGroups == numUInt32s) {
        buffer = new CarbonPliBuffer(valueParm, numBits, numUInt32s, useDrive);
      }
      else {
        ok = false;
        tf_error("Parameter %d is not the same size as Carbon signal %s.", valueParm, tf_getcstringp(nameParm));
        tf_dofinish();
      }
    }

    if (ok) {
      // Allocate and initialize local context storage
      data = new CarbonPliTaskData(instance->getModel(), net, mem, buffer);
    }
  }

  if (path != 0) {
    carbonmem_free(path);
  }
  if (relativePath != 0) {
    carbonmem_free(relativePath);
  }
  if (taskPath != 0) {
    carbonmem_free(taskPath);
  }

  return data;
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_deposit
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_deposit_checktf(int /* user_data */, int /* reason */)
{
  tf_setworkarea(NULL);  // just to be safe

  // There must be 2 args: a signal name, and a value.
  if (tf_nump() != 2) {
    tf_error("Usage: $carbon_deposit(\"<signal_name>\", <value>)");
  }
  else {
    if (!validSignalArgType(1)) {
      tf_error("The first argument to $carbon_deposit must be a signal name string.");
    }
    if (!validArgType(2)) {
      tf_error("The second argument to $carbon_deposit must be a value to deposit.");
    }
  }
  return 0;
}

extern "C" int carbon_deposit_calltf(int /* user_data */, int /* reason */)
{
  CarbonPliTaskData* data = (CarbonPliTaskData *) tf_getworkarea();

  // if not yet initialized, do so now
  if (data == NULL)
  {
    // Arg 1 must be a valid Carbon signal path.
    // Arg 2 must be a valid value to deposit
    data = getTaskData(1, 2);
    tf_setworkarea((PLI_BYTE8 *) data);
  }

  // write the given value to the named signal
  if (data != NULL)
  {
    // read the input value
    data->getBuffer()->pliRead();

    // deposit value on Carbon net
    carbonDeposit(data->getModel(), data->getNet(), data->getBuffer()->getValue(), 0);
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_examine
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_examine_sizetf(int /* user_data */, int /* reason */)
{
  return 32; // unfortunately the size cannot be instance-specific
}

extern "C" int carbon_examine_checktf(int /* user_data */ , int /* reason */)
{
  tf_setworkarea(NULL);  // just to be safe

  if (tf_nump() < 1 || tf_nump() > 2) {
    tf_error("Usage: $carbon_examine(\"<signal_name>\" [, <result>]");
  }
  else {
    if (!validSignalArgType(1)) {
      tf_error("The first argument to $carbon_examine must be a signal name string.");
    }
    if (tf_nump() > 1) {
      if (tf_typep(2) != TF_READWRITE) {
        tf_error("The second argument to $carbon_examine must be a writable HDL signal.");
      }
    }
  }
  return 0;
}

extern "C" int carbon_examine_calltf(int /* user_data */, int /* reason */)
{
  CarbonPliTaskData* data = (CarbonPliTaskData *) tf_getworkarea();

  // if not yet initialized, do so now
  if (data == NULL)
  {
    // Arg 1 must be a valid Carbon signal path.
    // Arg 2 is either a parameter number for the output, or -1
    PLI_INT32 param = (tf_nump() > 1) ? 2 : -1;
    data = getTaskData(1, param);
    tf_setworkarea((PLI_BYTE8 *) data);
  }

  // read first 32 bits of named signal and return them
  if (data != NULL)
  {
    // itf an output parameter was given
    if (tf_nump() > 1) {
      carbonExamine(data->getModel(), data->getNet(), data->getBuffer()->getValue(), data->getBuffer()->getDrive());

      // write lower 32 bits to PLI task return value
      tf_putp(0, (PLI_UINT32) data->getBuffer()->getValue()[0]);

      // write the full value to the given parameter
      data->getBuffer()->pliWrite();
    }
    else {
      // read the value
      UInt32 value = 0;
      carbonExamineWord(data->getModel(), data->getNet(), &value, 0, NULL);
      
      // write lower 32 bits to PLI task return value
      tf_putp(0, (PLI_UINT32) value);
    }
  }

  return 0;
}



///////////////////////////////////////////////////////////////////////////////
// $carbon_auto_examine
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_auto_examine_checktf(int /* user_data */, int /* reason */)
{
  tf_setworkarea(NULL);  // just to be safe

  // There must be 2 args: a signal name, and a value.
  if (tf_nump() != 2) {
    tf_error("Usage: $carbon_auto_examine(\"<signal_name>\", <hdl_signal>)");
  }
  else {
    if (!validSignalArgType(1)) {
      tf_error("The first argument to $carbon_auto_examine must be a signal name string.");
    }
    if (tf_typep(2) != TF_READWRITE) {
      tf_error("The second argument to $carbon_auto_examine must be a writable HDL signal.");
    }
  }
  return 0;
}

static void sAutoExamineCallback(CarbonObjectID *, CarbonNetID *, CarbonClientData data, 
                                 CarbonUInt32 *value, CarbonUInt32 *drive)
{
  CarbonPliBuffer *buffer = (CarbonPliBuffer *) data;
  buffer->pliWrite(value, drive);
}

extern "C" int carbon_auto_examine_calltf(int /* user_data */, int /* reason */)
{
  char* path = NULL;
  char* relativePath = NULL;
  char* taskPath = NULL;
  CarbonNetID *net = NULL;

  // lookup the net given by the first arg
  CarbonPliWrapperContext::sAllocateAndCopy(&taskPath, tf_mipname());
  CarbonPliWrapperContext::sAllocateAndCopy(&path, tf_getcstringp(1));
  CarbonPliWrapperContext*  context = CarbonPliWrapperContextManager::getPliContext();
  CarbonPliModel* instance = context->findModel(taskPath, path, &relativePath);
  if (instance != NULL) {
    net = carbonFindNet(instance->getModel(), relativePath);
  }
  if (instance == NULL || net == NULL) {
    tf_error("%s is not a valid Carbon Model.", path);
    tf_dofinish();
    return 1;
  }

  // allocate a PliBuffer to handle the writes from the Carbon net to the PLI parameter
  CarbonObjectID* obj = instance->getModel();
  CarbonDB* db = carbonGetDB(obj);
  const CarbonDBNode* netNode = carbonNetGetDBNode(obj, net);
  CarbonPliBuffer *buffer = new CarbonPliBuffer(2, carbonDBGetWidth(db, netNode), carbonGetNumUInt32s(net), carbonIsTristate(net));

  // register for the net change callback
  carbonAddNetValueDriveChangeCB(instance->getModel(), sAutoExamineCallback, buffer, net);

  // write the current value to Verilog to get the initial state right
  carbonExamine(instance->getModel(), net, buffer->getValue(), buffer->getDrive());
  buffer->pliWrite();

  if (path != 0) {
    carbonmem_free(path);
  }
  if (relativePath != 0) {
    carbonmem_free(relativePath);
  }
  if (taskPath != 0) {
    carbonmem_free(taskPath);
  }

  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_force
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_force_checktf(int /* user_data */, int /* reason */)
{
  tf_setworkarea(NULL);  // just to be safe

  // There must be 2 args: a signal name, and a value.
  if (tf_nump() != 2) {
    tf_error("Usage: $carbon_force(\"<signal_name>\", <value>)");
  }
  else {
    if (!validSignalArgType(1)) {
      tf_error("The first argument to $carbon_force must be a signal name string.");
    }
    if (!validArgType(2)) {
      tf_error("The second argument to $carbon_force must be a value to force.");
    }
  }
  return 0;
}

extern "C" int carbon_force_calltf(int /* user_data */, int /* reason */)
{
  CarbonPliTaskData* data = (CarbonPliTaskData *) tf_getworkarea();

  // if not yet initialized, do so now
  if (data == NULL)
  {
    // Arg 1 must be a valid Carbon signal path.
    // Arg 2 mus be a valid value to force
    data = getTaskData(1, 2);
    tf_setworkarea((PLI_BYTE8 *) data);
  }

  // write the given value to the named signal
  if (data != NULL)
  {
    // read the input value
    data->getBuffer()->pliRead();

    // force value on Carbon net
    carbonForce(data->getModel(), data->getNet(), data->getBuffer()->getValue());
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_force_range
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_force_range_checktf(int /* user_data */, int /* reason */)
{
  tf_setworkarea(NULL);  // just to be safe

  // There must be 4 args: a signal name, value, msb, lsb
  if (tf_nump() != 4) {
    tf_error("Usage: $carbon_force_range(\"<signal_name>\", <value>, <range_msb>, <range_lsb>)");
  }
  else {
    if (!validSignalArgType(1)) {
      tf_error("The first argument to $carbon_force_range must be a signal name string.");
    }
    if (!validArgType(2)) {
      tf_error("The second argument to $carbon_force_range must be a value to force.");
    }
    if (!validArgType(3)) {
      tf_error("The third argument to $carbon_force_range must be the most significant bit of the range.");
    }
    if (!validArgType(4)) {
      tf_error("The fourth argument to $carbon_force_range must be the least significant bit of the range.");
    }
  }
  return 0;
}

extern "C" int carbon_force_range_calltf(int /* user_data */, int /* reason */)
{
  CarbonPliTaskData* data = (CarbonPliTaskData *) tf_getworkarea();

  // if not yet initialized, do so now
  if (data == NULL)
  {
    // Arg 1 must be a valid Carbon signal path.
    // Arg 2 must be a valid value to force
    data = getTaskData(1, 2);
    tf_setworkarea((PLI_BYTE8 *) data);
  }

  // write the given value to the named signal
  if (data != NULL)
  {
    // read the input value
    data->getBuffer()->pliRead();

    // force value on Carbon net
    carbonForceRange(data->getModel(), data->getNet(), data->getBuffer()->getValue(), tf_getp(3), tf_getp(4));
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_release
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_release_checktf(int /* user_data */, int /* reason */)
{
  tf_setworkarea(NULL);  // just to be safe

  if (tf_nump() != 1) {
    tf_error("Usage: $carbon_release(\"<signal_name>\")");
  }
  else {
    if (!validSignalArgType(1)) {
      tf_error("The argument to $carbon_release must be a signal name string.");
    }
  }
  return 0;
}

extern "C" int carbon_release_calltf(int /* user_data */, int /* reason */)
{
  CarbonPliTaskData* data = (CarbonPliTaskData *) tf_getworkarea();

  // if not yet initialized, do so now
  if (data == NULL)
  {
    // Arg 1 must be a valid Carbon signal path.
    data = getTaskData(1);
    tf_setworkarea((PLI_BYTE8 *) data);
  }

  // release given signal
  if (data != NULL)
  {
    carbonRelease(data->getModel(), data->getNet());
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_release_range
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_release_range_checktf(int /* user_data */, int /* reason */)
{
  tf_setworkarea(NULL);  // just to be safe

  if (tf_nump() != 3) {
    tf_error("Usage: $carbon_release_range(\"<signal_name>\", <range_msb>, <range_lsb>)");
  }
  else {
    if (!validSignalArgType(1)) {
      tf_error("The first argument to $carbon_release_range must be a signal name string.");
    }
    if (!validArgType(2)) {
      tf_error("The second argument to $carbon_force_range must be the most significant bit of the range.");
    }
    if (!validArgType(3)) {
      tf_error("The third argument to $carbon_force_range must be the least significant bit of the range.");
    }
  }
  return 0;
}

extern "C" int carbon_release_range_calltf(int /* user_data */, int /* reason */)
{
  CarbonPliTaskData* data = (CarbonPliTaskData *) tf_getworkarea();

  // if not yet initialized, do so now
  if (data == NULL)
  {
    // Arg 1 must be a valid Carbon signal path.
    data = getTaskData(1);
    tf_setworkarea((PLI_BYTE8 *) data);
  }

  // release given signal
  if (data != NULL)
  {
    carbonReleaseRange(data->getModel(), data->getNet(), tf_getp(2), tf_getp(3));
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_readmemh, $carbon_readmemb
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_readmem_checktf(int user_data, int /* reason */)
{
  tf_setworkarea(NULL);  // just to be safe
  const char *name = (user_data) ? "readmemh" : "readmemb";

  // There must be 2 args: a data file and a memory name.
  if (tf_nump() != 2) {
    tf_error("Usage: $carbon_%s(\"<data_file>\", \"<memory_name>\")", name);
  }
  else {
    if (tf_typep(1) != TF_STRING) {
      tf_error("The first argument to $carbon_%s must be a file name string.", name);
    }
    if (!validSignalArgType(2)) {
      tf_error("The first argument to $carbon_%s must be a memory name string.", name);
    }
  }
  return 0;
}

extern "C" int carbon_readmem_calltf(int user_data, int /* reason */)
{
  CarbonPliTaskData* data = (CarbonPliTaskData *) tf_getworkarea();

  // if not yet initialized, do so now
  if (data == NULL)
  {
    // Arg 2 must be a valid Carbon memory path.
    data = getTaskData(2, -1, true);
    tf_setworkarea((PLI_BYTE8 *) data);
  }

  // write the given value to the named memory
  if (data != NULL)
  {
    if (user_data) {
      carbonReadmemh(data->getMemory(), tf_getcstringp(1));
    }
    else {
      carbonReadmemb(data->getMemory(), tf_getcstringp(1));
    }
  }
  return 0;  
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_dumpfile
///////////////////////////////////////////////////////////////////////////////

extern "C" int carbon_dumpfile_checktf(int /* user_data */, int /* reason */)
{
  if (tf_nump() > 1) {
    tf_error("Usage: $carbon_dumpfile([\"<file_name>\"])");
  }
  else {
    if (sNonNullParameters() > 0 && tf_typep(1) != TF_STRING) {
      tf_error("The argument to $carbon_dumpfile must be a file name string.");
    }
  }
  return 0;
}

extern "C" int carbon_dumpfile_calltf(int /* user_data */, int /* reason */)
{
  // set the new dump file name in the context
  CarbonPliWrapperContext* context = CarbonPliWrapperContextManager::getPliContext();

  if (sNonNullParameters() == 0) {
    context->setDumpFile("dump.vcd"); // IEEE 1364-2001 default $dumpfile name
  }
  else {
    context->setDumpFile((const char *) tf_getcstringp(1));
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_dumpfile_fsdb_auto_switch
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_dumpfile_fsdb_auto_switch_checktf()
{
  if (tf_nump() != 3) {
    tf_error("Usage: $carbon_dumpfile_fsdb_auto_switch(\"<fname_prefix>\", <limit_megs>, <max_files>)");
  }
  else {
    if (tf_typep(1) != TF_STRING) {
      tf_error("Argument 1 (fname_prefix) must be a file name string.");
    }
    if (!validArgType(2)) {
      tf_error("Argument 2 (limit_mags) must be a number.");
    }
    if (!validArgType(3)) {
      tf_error("Argument 3 (max_files) must be a number.");
    }
  }
  return 0;
}

extern "C" int carbon_dumpfile_fsdb_auto_switch_calltf()
{
  CarbonPliWrapperContext* context = CarbonPliWrapperContextManager::getPliContext();
  context->setDumpAutoSwitch((const char *) tf_getcstringp(1), tf_getp(2), tf_getp(3));
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_dumpvars
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_dumpvars_checktf(int /* user_data */, int /* reason */)
{
  if (sNonNullParameters() > 0)
  {
    // first parameter is the 'levels' of scope to dump
    if (tf_typep(1) != tf_readonly && tf_typep(1) != tf_readwrite) {
      tf_error("The first argument to $carbon_dumpvars must be an integer.");
    }
    else {
      // remaining arguments must ve valid signal names
      for (int i = 2; i <= tf_nump(); i++) {
        if (!validSignalArgType(i)) {
          tf_error("Argument %d to $carbon_dumpvars must be a string.", i);
        }
      }
    }
  }
  return 0;
}

extern "C" int carbon_dumpvars_calltf(int /* user_data */, int /* reason */)
{
  CarbonPliWrapperContext* context = CarbonPliWrapperContextManager::getPliContext();

  int level = 0;     // if no args given, dump everything
  int relativeLevel;
  CarbonPliModel* instance;

  // read user-specified level
  if (sNonNullParameters() > 0)
    level = tf_getp(1);

  if (sNonNullParameters() <= 1)
  {
    // dump vars for all models within given level
    for (CarbonPliWrapperContext::ModelLoop model = context->loopModels(); !model.atEnd(); ++model)
    {
      CarbonPliModel* pliModel = model.getValue();
      relativeLevel = (level == 0) ? 0 : level - pliModel->getDepth();
      if (relativeLevel >= 0) {
        pliModel->dumpVars(pliModel->getCarbonName(), relativeLevel);
      }
    }
  }
  else
  {
    // dump vars for all models that match the args
    for (int i = 2; i <= tf_nump(); i++)
    {
      char* path = NULL;
      char* taskPath = NULL;
      char* relativePath = NULL;
      CarbonPliWrapperContext::sAllocateAndCopy(&taskPath, tf_mipname());
      CarbonPliWrapperContext::sAllocateAndCopy(&path, tf_getcstringp(i));
      instance = context->findModel(taskPath, path, &relativePath);
      if (instance != NULL)
      {
        instance->dumpVars(relativePath, level);
      }
      if (path != 0) {
        carbonmem_free(path);
      }
      if (relativePath != 0) {
        carbonmem_free(relativePath);
      }
      if (taskPath != 0) {
        carbonmem_free(taskPath);
      }
    }
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_dumpon, $carbon_dumpoff
//
//  user_data == 1 => dumpon
//  user_data == 0 => dumpoff
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_dump_control_checktf(int /* user_data */, int /* reason */)
{
  if (sNonNullParameters() > 0) {
    // arguments must be strings
    for (int i = 1; i <= tf_nump(); i++) {
      if (tf_typep(i) != TF_STRING) {
        tf_error("Argument %d must be a string.", i);
      }
    }
  }
  return 0;
}

extern "C" int carbon_dump_control_calltf(int user_data, int /* reason */)
{
  CarbonPliWrapperContext* context = CarbonPliWrapperContextManager::getPliContext();

  if (sNonNullParameters() == 0)
  {
    // turn dumping on/off for all model instances
    for (CarbonPliWrapperContext::ModelLoop model = context->loopModels(); !model.atEnd(); ++model)
    {
      CarbonPliModel* pliModel = model.getValue();
      pliModel->dumpControl(user_data);
    }
  }
  else
  {
    // turn on/off dumping for all named model instances
    for (int i = 1; i <= tf_nump(); i++)
    {
      // Find the model instance.
      char* path = NULL;
      char* taskPath = NULL;
      CarbonPliWrapperContext::sAllocateAndCopy(&taskPath, tf_mipname());
      CarbonPliWrapperContext::sAllocateAndCopy(&path, tf_getcstringp(i));
      CarbonPliModel* instance = context->findModel(taskPath, path);

      if (instance != NULL) {
        instance->dumpControl(user_data);
      }
      else {
        tf_error("%s is not the name of a Carbon model instance", tf_getcstringp(i));
      }
      if (path != 0) {
        carbonmem_free(path);
      }
      if (taskPath != 0) {
        carbonmem_free(taskPath);
      }
    }
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_deposit_memory
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_deposit_memory_checktf(int /* user_data */, int /* reason */)
{
  tf_setworkarea(NULL);  // just to be safe

  // There must be 3 args: a memory name, an index, and a value.
  if (tf_nump() != 3) {
    tf_error("Usage: $carbon_deposit_memory(\"<memory_name>\", <index>, <value>)");
  }
  else {
    if (!validSignalArgType(1)) {
      tf_error("The first argument to $carbon_deposit_memory must be a memory name string.");
    }
    if (!validArgType(2)) {
      tf_error("The second argument to $carbon_deposit_memory must be an index number.");
    }
    if (!validArgType(3)) {
      tf_error("The third argument to $carbon_deposit_memory must be a value to deposit.");
    }
  }
  return 0;
}

extern "C" int carbon_deposit_memory_calltf(int /* user_data */, int /* reason */)
{
  CarbonPliTaskData* data = (CarbonPliTaskData *) tf_getworkarea();

  // if not yet initialized, do so now
  if (data == NULL)
  {
    // Arg 1 must be a valid Carbon signal path.
    // Arg 3 must be a valid value to deposit
    data = getTaskData(1, 3, true);
    tf_setworkarea((PLI_BYTE8 *) data);
  }

  // write the given value to the named memory
  if (data != NULL)
  {
    // read the input value
    data->getBuffer()->pliRead();

    // get the address
    SInt64 addr = sGetTfLong(2);

    // write value to Carbon memory
    carbonDepositMemory(data->getMemory(), addr, data->getBuffer()->getValue());
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// $carbon_examine_memory
///////////////////////////////////////////////////////////////////////////////
extern "C" int carbon_examine_memory_sizetf(int /* user_data */, int /* reason */)
{
  return 32;  // unfortunately the size must be the same for all instances
}

extern "C" int carbon_examine_memory_checktf(int /* user_data */, int /* reason */)
{
  tf_setworkarea(NULL);  // just to be safe

  // There must be 2 args: a memory name and an index.
  if (tf_nump() < 2 || tf_nump() > 3) {
    tf_error("Usage: $carbon_examine_memory(\"<memory_name>\", <index> [, <result>])");
  }
  else {
    if (!validSignalArgType(1)) {
      tf_error("The first argument to $carbon_examine_memory must be a memory name string.");
    }
    if (!validArgType(2)) {
      tf_error("The second argument to $carbon_examine_memory must be an index number.");
    }
    if (tf_nump() > 2) {
      if (tf_typep(3) != TF_READWRITE) {
        tf_error("The third argument to $carbon_examine_memory must be a writable HDL signal.");
      }
    }
  }
  return 0;
}

extern "C" int carbon_examine_memory_calltf(int /* user_data */, int /* reason */)
{
  CarbonPliTaskData* data = (CarbonPliTaskData *) tf_getworkarea();

  // if not yet initialized, do so now
  if (data == NULL)
  {
    // Arg 1 must be a valid Carbon signal path.
    // Arg 2 is either a parameter number for the output, or -1
    PLI_INT32 param = (tf_nump() > 2) ? 3 : -1;
    data = getTaskData(1, param, true);
    tf_setworkarea((PLI_BYTE8 *) data);
  }

  // write the given value to the named memory
  if (data != NULL)
  {
    // get the address
    SInt64 addr = sGetTfLong(2);

    // if an output parameter was given
    if (tf_nump() > 2) {
      carbonExamineMemory(data->getMemory(), addr, data->getBuffer()->getValue());

      // write lower 32 bits to PLI task return value
      tf_putp(0, (PLI_UINT32) data->getBuffer()->getValue()[0]);

      // write the full value to the given parameter
      data->getBuffer()->pliWrite();
    }
    else { 
      // read value from Carbon memory
      UInt32 value = carbonExamineMemoryWord(data->getMemory(), addr, 0);

      // send value to PLI as return value
      tf_putp(0, (PLI_UINT32) value);
    }
  }
  return 0;
}

