// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


extern "C" {
#define PLI_EXTRAS
#include "pli-wrapper/veriuser.h"
}

#include "util/MutexWrapper.h"
#include "pli-wrapper/CarbonPliWrapper.h"
#include "shell/carbon_dbapi.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

CarbonPliBuffer::CarbonPliBuffer(int pliParm,
                                 unsigned int /* numBits */,
                                 unsigned int numUInt32s,
                                 bool useDrive)
{
  mPliParm = pliParm;
  size_t numBytes = sizeof(UInt32) * numUInt32s;

  mValue = (UInt32 *) malloc(numBytes);
  memset(mValue, 0, numBytes);

  if (useDrive)
  {
    mDrive = (UInt32 *) malloc(numBytes);
    memset(mDrive, 0, numBytes);
  }
  else
    mDrive = NULL;

  // allocate and initialize PLI info about the prameter
  mPliInfo = (p_tfexprinfo) malloc(sizeof(s_tfexprinfo));
  tf_exprinfo(pliParm, mPliInfo);

  // save a pointer to the udtf instance
  mInstance = tf_getinstance();
}

CarbonPliBuffer::~CarbonPliBuffer()
{
  if (mValue != NULL)
    free(mValue);
  if (mDrive != NULL)
    free(mDrive);
  if (mPliInfo != NULL)
    free(mPliInfo);
  mValue = mDrive = NULL;
  mPliInfo = NULL;
}

/*
 * Read a value from the PLI into the mValue and mDrive UInt32 buffers.
 * Return true if the value or drive has changed.
 *
 * When mDrive is not being used, we convert like this:
 *
 *            pli  pli   carbon
 *     value  aval bval  value
 *       0      0   0      0    
 *       1      1   0      1
 *       x      1   1      0
 *       z      0   1      1
 *
 * When mDrive is used, we convert like this:
 *            pli  pli   carbon carbon
 *     value  aval bval  value  drive
 *       0      0   0      0      0
 *       1      1   0      1      0
 *       x      1   1      0      0
 *       z      0   1      1      1
 */
bool CarbonPliBuffer::pliRead()
{
  bool valueChanged = false;
  bool driveChanged = false;
  UInt32 value, drive;

  // read current PLI parameter data into mPliInfo
  tf_evaluatep(mPliParm);

  // xfer the bits to Carbon buffer.
  for (int i = 0; i < mPliInfo->expr_ngroups; i++)
  {
    value = mPliInfo->expr_value_p[i].avalbits ^ mPliInfo->expr_value_p[i].bvalbits;
    if (mValue[i] != value)
    {
      valueChanged = true;
      mValue[i] = value;
    }

    if (mDrive != NULL)
    {
      drive = mPliInfo->expr_value_p[i].bvalbits & (~mPliInfo->expr_value_p[i].avalbits);
      if (mDrive[i] != drive)
      {
        driveChanged = true;
        mDrive[i] = drive;
      }
    }
  }
  return valueChanged || driveChanged;
}

/*
 * Write the current value and drive from the UInt32 buffers to the PLI.
 *
 * When mDrive is not in use, the conversion is:
 *
 *  carbon pli  pli  
 *  value  aval bval value
 *    0     0    0     0
 *    1     1    0     1
 *
 * When mDrive is in use, the conversion is:
 *
 *  carbon carbon pli  pli  
 *  value  drive  aval bval value
 *    0      0     0    0     0
 *    1      0     1    0     1
 *    0      1     0    1     z
 *    1      1     0    1     z
 */
void CarbonPliBuffer::pliWrite(UInt32 *value, UInt32 *drive)
{
  // xfer the bits from Carbon buffer to aval/bval
  for (int i = 0; i < mPliInfo->expr_ngroups; i++)
  {
    if (drive == NULL)
    {
      // no drive: aval = value, bval = 0
      mPliInfo->expr_value_p[i].avalbits = value[i];
      mPliInfo->expr_value_p[i].bvalbits = 0;      
    }
    else
    {
      // drive: driven values as is, undriven values go to z.
      mPliInfo->expr_value_p[i].avalbits = value[i] & (~drive[i]);
      mPliInfo->expr_value_p[i].bvalbits = drive[i];
    }
  }

  // send the bits to Verilog
  tf_ipropagatep(mPliParm, mInstance);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////




// check the given PLI parameter to make sure it is readable and/or writable
bool CarbonPliPort::checkPliType(int pliParm, bool read, bool write)
{
  bool ok = false;
  PLI_INT32 pType = tf_typep(pliParm);
  if (write)
  {
    if (!(ok = (pType == TF_READWRITE)))
      tf_error("Parameter %d must be a writeable signal", pliParm);
  }
  else if (read)
  {
    if (!(ok = (pType == TF_READONLY) || (pType == TF_READWRITE)))
      tf_error("Parameter %d must be a readable signal", pliParm);
  }
  return ok;
}

CarbonPliPort::CarbonPliPort(CarbonObjectID* obj, CarbonNetID* net, int pliParm, bool isDepositable)
{
  mIsDepositable = isDepositable;
  mNet = net;
  CarbonDB* db = carbonGetDB(obj);
  const CarbonDBNode* netNode = carbonNetGetDBNode(obj, net);
  mBuffer = new CarbonPliBuffer(pliParm, 
                                carbonDBGetWidth(db, netNode), 
                                carbonGetNumUInt32s(net),
                                carbonIsTristate(net));
}

CarbonPliPort::~CarbonPliPort()
{
  if (mBuffer != NULL)
  {
    delete mBuffer;
    mBuffer = NULL;
  }
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// helper to compute number of levels in given Verilog path
unsigned int computeDepth(const char *path)
{
  // TODO: handle escaped identifiers
  unsigned int depth = 0;

  for (const char * dot = path; dot != NULL; dot = strchr(dot, '.'))
  {
    if (dot != NULL)
    {
      ++dot;
      ++depth;
    }
  }
  return depth;
}

CarbonPliModel::CarbonPliModel(const char *hdlName,
                               const char *carbonName, 
                               CarbonObjectID *model, 
                               int numPorts,
                               CarbonTimescale timeScale)
{
  mContext = NULL;
  CarbonPliWrapperContext::sAllocateAndCopy(&mHdlName, hdlName);
  CarbonPliWrapperContext::sAllocateAndCopy(&mCarbonName, carbonName);
  mModel = model;
  mPorts = CARBON_ALLOC_VEC(CarbonPliPort *, numPorts);
  mNumPorts = numPorts;
  mDepth = computeDepth(hdlName);
  mTimeScale = timeScale;
  mWave = NULL;
}

CarbonPliModel::~CarbonPliModel()
{
  carbonmem_free(mHdlName);
  carbonmem_free(mCarbonName);
  if (mModel != NULL)
  {
    carbonDestroy(&mModel);
    mModel = NULL;
  }  
  CARBON_FREE_VEC(mPorts, CarbonPliPort *, mNumPorts);
}

void CarbonPliModel::initWaveFile()
{
  if (mWave == NULL)
  {
    char* fName = NULL;
    CarbonWaveFileType fType;

    // initialize a wave file
    mContext->getDumpFileName(mHdlName, &fName, fType);
    if (mContext->getDumpAutoSwitch()) {
      mWave = carbonWaveInitFSDBAutoSwitch(mModel, fName, mTimeScale, mContext->getDumpLimitMegs(), mContext->getDumpMaxFiles());
    }
    else {
      if (fType == eWaveFileTypeFSDB) {
        mWave = carbonWaveInitFSDB(mModel, fName, mTimeScale);
      }
      else {
        mWave = carbonWaveInitVCD(mModel, fName, mTimeScale);
      }
    }

    // set the prefix to the full path to this model
    carbonPutPrefixHierarchy(mWave, mHdlName, 1);
    if (fName != NULL) {
      carbonmem_free(fName);
    }
  }
}

void CarbonPliModel::dumpVars(const char *path, unsigned int level)
{
  initWaveFile();  // make sure we have a wave context
  if (mWave != NULL)
  {
    // dump given net/scope to give depth level
    carbonDumpVars(mWave, level, path);
  }
}

void CarbonPliModel::dumpControl(bool on)
{
  if (mWave != NULL)
  {
    if (on)
      carbonDumpOn(mWave);
    else
      carbonDumpOff(mWave);
  }
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

CarbonPliWrapperContext* CarbonPliWrapperContextManager::mContext = NULL;

// The above context object is shared among all models.  We need a
// mutex in some cases for thread safety.
MUTEX_WRAPPER_DECLARE(sContextMutex);


CarbonPliWrapperContext::CarbonPliWrapperContext()
  : mDumpType(eWaveFileTypeFSDB), mDumpLimitMegs(0),
    mDumpMaxFiles(0), mDumpAutoSwitch(false)
{
  sAllocateAndCopy(&mDumpName, "dump");
}

CarbonPliWrapperContext::~CarbonPliWrapperContext()
{
  carbonmem_free(mDumpName);
}

CarbonPliWrapperContext* CarbonPliWrapperContextManager::getPliContext()
{
  MutexWrapper mutex(&sContextMutex);
  if (mContext == NULL)
  {
    mContext = new CarbonPliWrapperContext();
  }
  return mContext;
}

void CarbonPliWrapperContext::addModel(CarbonPliModel *data)
{
  MutexWrapper mutex(&sContextMutex);
  mModelInstances.insert(CarbonModelInstanceMap::value_type(data->getHdlName(), data));
  data->setContext(this);
}

// This is only called by the user after all models are added, so a
// mutex isn't necessary.
CarbonPliWrapperContext::ModelLoop
CarbonPliWrapperContext::loopModels()
{
  return mModelInstances.loopUnsorted();
}


CarbonPliModel * 
CarbonPliWrapperContext::findModel(const char *scope, const char* path, char **localPath)
{
  CarbonPliModel* instance = NULL;

  // If there is a scope given, assume that the given path is
  // relative.  Create an absolute path by appending to the given
  // scope and try looking it up recursively.
  if (scope != NULL)
  {
    char* absPath;
    sAllocateAndCopy(&absPath, scope);
    sReallocateAndAppend(&absPath, ".");
    sReallocateAndAppend(&absPath, path);
    instance = findModel(NULL, absPath, localPath);
    carbonmem_free(absPath);
  }

  if (instance == NULL)
  {
    MutexWrapper mutex(&sContextMutex);
    // Lookup each level of hierarchy in the given path.
    // If found, the rest of the path is internal to the model.
    char* instanceName;
    const char* endOfPath = path;
    while (endOfPath != NULL)
    {
      endOfPath = strchr(endOfPath, '.');
      if (endOfPath == NULL)
        sAllocateAndCopy(&instanceName, path);
      else
        sAllocateAndCopy(&instanceName, path, endOfPath - path);

      CarbonModelInstanceMap::iterator iter = mModelInstances.find(instanceName);
      carbonmem_free(instanceName);
      if (iter != mModelInstances.end())
      {
        // model instance found!
        instance = iter->second;

        // localize path
        if (localPath != NULL)
        {
          sAllocateAndCopy(localPath, instance->getCarbonName());
          if (endOfPath != NULL)
            sReallocateAndAppend(localPath, endOfPath);
        }

        break; // model instance found!
      }

      if (endOfPath != NULL)
        ++endOfPath; // skip over '.'
    }
  }
  return instance;
}

void CarbonPliWrapperContext::setDumpFile(const char *name)
{
  MutexWrapper mutex(&sContextMutex);
  const char *dot;

  // Assume VCD format unless the file name ends in .FSDB
  dot = strrchr(name, '.');
  if (dot != NULL && (strcasecmp(dot, ".fsdb") == 0))
    mDumpType = eWaveFileTypeFSDB;
  else
    mDumpType = eWaveFileTypeVCD;

  // Save the name of the file minus the extension as the base name
  // for the wave files to be generated by all instances.
  carbonmem_free(mDumpName);
  if (dot == NULL)
    sAllocateAndCopy(&mDumpName, name);
  else
    sAllocateAndCopy(&mDumpName, name, dot - name);
}

void CarbonPliWrapperContext::setDumpAutoSwitch(const char *name, CarbonUInt32 limitMegs, CarbonUInt32 maxFiles)
{
  MutexWrapper mutex(&sContextMutex);
  mDumpAutoSwitch = true;
  carbonmem_free(mDumpName);
  sAllocateAndCopy(&mDumpName, name);
  mDumpType = eWaveFileTypeFSDB;  // auto-switch always uses fsdb
  mDumpLimitMegs = limitMegs;
  mDumpMaxFiles = maxFiles;
}

bool CarbonPliWrapperContext::getDumpAutoSwitch() 
{
  return mDumpAutoSwitch;
}

CarbonUInt32 CarbonPliWrapperContext::getDumpLimitMegs() 
{
  return mDumpLimitMegs;
}

CarbonUInt32 CarbonPliWrapperContext::getDumpMaxFiles() 
{
  return mDumpMaxFiles;
}

void CarbonPliWrapperContext::getDumpFileName(const char *scope, char **name, CarbonWaveFileType &fileType)
{
  MutexWrapper mutex(&sContextMutex);
  // generate a file name from the base name
  sAllocateAndCopy(name, mDumpName);
  sReallocateAndAppend(name, "_");
  sReallocateAndAppend(name, scope);

  // auto-switch mode sets the file extension automatically
  if (!mDumpAutoSwitch) {
    if (mDumpType == eWaveFileTypeFSDB) {
      sReallocateAndAppend(name, ".fsdb");
    }
    else {
      sReallocateAndAppend(name, ".vcd");
    }
  }

  // set the output format
  fileType = mDumpType;
}

void CarbonPliWrapperContext::sAllocateAndCopy(char** dest, const char* src)
{
  size_t len = strlen(src) + 1;
  *dest = static_cast<char*>(carbonmem_malloc(len * sizeof(char)));
  strcpy(*dest, src);
}

void CarbonPliWrapperContext::sAllocateAndCopy(char** dest, const char* src, size_t size)
{
  size_t len = size + 1;
  *dest = static_cast<char*>(carbonmem_malloc(len * sizeof(char)));
  strncpy(*dest, src, size);
  // strncpy doesn't null-terminate
  (*dest)[size] = '\0';
}

void CarbonPliWrapperContext::sReallocateAndAppend(char** dest, const char* src)
{
  size_t newLen = strlen(*dest) + strlen(src) + 1;
  char* newBuf = static_cast<char*>(carbonmem_malloc(newLen * sizeof(char)));
  // Copy, then free the old buffer
  strcpy(newBuf, *dest);
  strcat(newBuf, src);
  carbonmem_free(*dest);
  *dest = newBuf;
}
