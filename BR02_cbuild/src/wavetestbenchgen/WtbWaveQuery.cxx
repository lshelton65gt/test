// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "waveform/WfVCDFileReader.h"
#include "waveform/WfAbsSignal.h"
#include "waveform/WfAbsSignalVCIter.h"
#include "symtab/STSymbolTable.h"
#include "hdl/HdlHierPath.h"
#include "util/ArgProc.h"
#include "util/UtIOStream.h"
#include "util/AtomicCache.h"
#include "util/UtConv.h"
#include "util/DynBitVector.h"
#include "util/UtUInt64Factory.h"
#include "util/UtOrder.h"
#include "hdl/HdlId.h"

#if pfUNIX
#include "waveform/WfFsdbFileReader.h"
#endif

static const char* scChopHier = "-chopHier";
static const char* scType = "-t";
static const char* scInFile = "-in";
static const char* scMode = "-mode";

static const char* scDumpNames = "dumpnames";
static const char* scDumpHier = "dumphier";
static const char* scDiff = "diff";
static const char* scSummary = "summary";

class MainRunMode
{
public:
  MainRunMode() : mScreen(UtIO::cout()), mFileType(eWaveFileTypeVCD), mDynFactory(true)
  {}

  virtual ~MainRunMode() {}

  virtual bool run(ArgProc& args) = 0;

  WfAbsFileReader* readInputFile(const char* inFile)
  {
    WfAbsFileReader* reader = NULL;
    switch (mFileType)
    {
    case eWaveFileTypeVCD:
      reader = new WfVCDFileReader(inFile, &mStringCache, &mDynFactory, &mTimeFactory);
      break;
    case eWaveFileTypeFSDB:
#if pfUNIX
      reader = WfFsdbFileReaderCreate(inFile, &mStringCache, &mDynFactory, &mTimeFactory);
#else
      fprintf(stderr, "Fsdb file reading is only supported on linux.\n");
      exit(1);
#endif
      break;
    }
    
    UtString reason;
    if (reader->loadSignals(&reason) != WfAbsFileReader::eOK)
    {
      fprintf(stderr, "%s\n", reason.c_str());
      delete reader;
      reader = NULL;
    }
    return reader;
  }

  void setFileType(CarbonWaveFileType fileType)
  {
    mFileType = fileType;
  }

protected:
  UtOStream& mScreen;  
  CarbonWaveFileType mFileType;  
  AtomicCache mStringCache;
  DynBitVectorFactory mDynFactory;
  UtUInt64Factory mTimeFactory;
};

class DumpMode : public virtual MainRunMode
{
public:
  DumpMode() : mWaveReader(NULL), mEraseHierNum(0)
  {}

  virtual ~DumpMode() {
    delete mWaveReader;
  }

  static void addArgs(ArgProc& args, UtString& prefix) 
  {
    args.addInt(scChopHier, "Number of hierarchy levels to erase when printing out signal names. Applies only to 'dumpnames' mode", 0, false, false, 1);    
    UtString buf(prefix);
    buf << "dumpnames [" << scChopHier << " <numLevels>] " << scInFile << " <wave file>";
    args.addSynopsis(buf);
  }

  virtual bool run(ArgProc& args) 
  {
    args.getIntLast(scChopHier, &mEraseHierNum);
    if (mEraseHierNum < 0) {
      mScreen << "Warning: " << scChopHier << " option cannot have a negative number.";
      mEraseHierNum = 0;
    }
    
    const char* inFile;
    args.getStrValue(scInFile, &inFile);
    mWaveReader = readInputFile(inFile);
    if (! mWaveReader)
      return false;
    
    return dumpNames();
  }
  
  bool dumpNames()
  {
    STSymbolTable* symTab = mWaveReader->getSymbolTable();
    INFO_ASSERT(symTab, "Wave reader has no symboltable!");
    const HdlHierPath* pather = symTab->getHdlHier();
    HdlId info;
    UtString buf;
    const STAliasedLeafNode* leaf;
    for (STSymbolTable::NodeLoopSorted loop = symTab->getNodeLoopSorted();
	 not loop.atEnd(); 
	 ++loop)
    {
      const STSymbolTableNode* node = *loop;
      bool printIt = true;
      leaf = node->castLeaf();
      if (leaf)
      {
        mWaveReader->fillHdlId(&info, leaf);
        pather->compPathHier(node, &buf, &info);
        if (mEraseHierNum > 0)
        {
          HdlHierPath::StrAtomVec ids;
          pather->decompPathAtom(buf.c_str(), &ids, symTab->getAtomicCache(), &info);
          if (unsigned(mEraseHierNum) >= ids.size())
          {
            mScreen << "Error: Too many levels specified to chop for " << buf.c_str() << UtIO::endl;
            printIt = false;
          }
          else {
            HdlHierPath::StrAtomVec::iterator p = ids.begin();
            ids.erase(p, p + mEraseHierNum);
            buf.clear();
            pather->compPathAtom(ids, &buf, &info);
          }
        }
        if (printIt)
          mScreen << buf << UtIO::endl;
        buf.clear();
      }
    }
    return true;
  }

private:
  WfAbsFileReader* mWaveReader;
  SInt32 mEraseHierNum;
};

class HierMode : public virtual MainRunMode
{
public:
  HierMode() : mWaveReader(NULL)
  {}

  virtual ~HierMode() {
    delete mWaveReader;
  }

  static void addArgs(ArgProc& args, UtString& prefix) 
  {
    UtString buf(prefix);
    buf << "dumphier " << scInFile << " <wave file>";
    args.addSynopsis(buf);
  }

  virtual bool run(ArgProc& args) 
  {
    const char* inFile;
    args.getStrValue(scInFile, &inFile);
    mWaveReader = readInputFile(inFile);
    if (! mWaveReader)
      return false;
    
    return dumpHier();
  }
  
  void dumpNode(STSymbolTableNode* node, UInt32 indent) {
    STBranchNode* b = node->castBranch();

    if (b != NULL) {
      for (UInt32 i = 0; i < indent; ++i) {
        mScreen << ' ';
      }

      mScreen << node->str() << "\n";

      indent += 2;
      for (UInt32 i = 0, n = b->numChildren(); i < n; ++i) {
        dumpNode(b->getChild(i), indent);
      }
    }
  }


  bool dumpHier()
  {
    UtString sim_version, creation_date;

    mWaveReader->getSimulatorVersion(sim_version);
    mWaveReader->getSimulationDate(creation_date);

    mScreen << "Simulator:     " << sim_version << "\n";
    mScreen << "Creation date: " << creation_date << "\n";

    STSymbolTable* symTab = mWaveReader->getSymbolTable();
    for (STSymbolTable::RootIter r(symTab->getRootIter()); !r.atEnd(); ++r) {
      STSymbolTableNode* root = *r;
      dumpNode(root, 0);
    }
    return true;
  }

private:
  WfAbsFileReader* mWaveReader;
  SInt32 mEraseHierNum;
};

class SigVal
{
public:

  SigVal() : mVal(NULL)
  {}

  const DynBitVector** getValPtr() { return &mVal; }
  
  int compare(const SigVal& other) const
  {
    int stat = carbonPtrCompare(mVal, other.mVal);
    return stat;
  }
  
private:
  const DynBitVector* mVal;
};

class DiffMode : public virtual MainRunMode
{
public:
  DiffMode() : mWaveReader1(NULL), mWaveReader2(NULL)
  {}
  
  virtual ~DiffMode() {
    delete mWaveReader1;
    delete mWaveReader2;
  }

  static void addArgs(ArgProc& args, UtString& prefix)
  {
    UtString buf(prefix);
    buf << "diff ";
    buf << scInFile << " <wave file 1> ";
    buf << scInFile << " <wave file 2>";
    args.addSynopsis(buf);
  }
  
  bool run(ArgProc& args) 
  {
    ArgProc::StrIter inFiles;
    args.getStrIter(scInFile, &inFiles);
    int i = 0;
    while(! inFiles.atEnd())
    {
      ++i;
      const UtString& buf = *inFiles;
      if (i == 1)
        mWaveReader1 = readInputFile(buf.c_str());
      else if (i == 2)
        mWaveReader2 = readInputFile(buf.c_str());
      ++inFiles;
    }
    
    if (i != 2)
    {
      mScreen << "Error: Must have exactly 2 " << scInFile 
              << " references for diff mode" << UtIO::endl;
      return false;
    }
    
    if (! mWaveReader1 || !mWaveReader2)
    {
      mScreen << "Error: Problem loading wave files " << UtIO::endl;
      return false;
    }

    return doDiff();
  }
  
private:
  WfAbsFileReader* mWaveReader1;
  WfAbsFileReader* mWaveReader2;

  typedef UtVector<WfAbsFileReader::SigHier> VecSigType;

  void cleanSigs(VecSigType& /*sigs*/)
  {
#if 0
    for (VecSigType::iterator p = sigs.begin();
         p != sigs.end(); ++p)
    {
      WfAbsFileReader::SigHier& q = *p;
      delete q.mSignal;
      q.mSignal = NULL;
    }
#endif

  }

  bool doDiff() 
  {
    VecSigType sigs1;
    VecSigType  sigs2;
    mWaveReader1->listAndWatchAll(&sigs1);
    mWaveReader2->listAndWatchAll(&sigs2);

    bool mustReturn = false;
    UtString msg;
    WfAbsFileReader::Status stat = mWaveReader1->loadValues(&msg);
    if (stat != WfAbsFileReader::eOK)
    {
      fprintf(stderr, "%s\n", msg.c_str());
      if (stat == WfAbsFileReader::eError)
        mustReturn = true;
    }
    stat = mWaveReader2->loadValues(&msg);
    if (stat != WfAbsFileReader::eOK)
    {
      fprintf(stderr, "%s\n", msg.c_str());
      if (stat == WfAbsFileReader::eError)
        mustReturn = true;
    }
    
    if (mustReturn)
      return false;

    bool isGood = checkSigs(sigs1, sigs2);
    isGood = isGood && checkValues(mWaveReader1, sigs1, mWaveReader2, sigs2);
    cleanSigs(sigs1);
    cleanSigs(sigs2);
    return isGood;
  }
  
  bool checkSigs(VecSigType& sigs1, VecSigType& sigs2)
  {
    bool isGood = true;
    
    size_t wave1Num = sigs1.size();
    size_t wave2Num = sigs2.size();

    VecSigType* sigNames = NULL;
    WfAbsFileReader* checker = NULL;
    const HdlHierPath* pather = NULL;
    if (wave1Num != wave2Num)
    {
      mScreen << "Error: Number of distinct signals between wave files is different: 1 - " 
              << wave1Num << " 2 - " << wave2Num << UtIO::endl;
      isGood = false;
      if (wave1Num < wave2Num)
      {
        sigNames= &sigs2;
        checker = mWaveReader1;
        pather = mWaveReader2->getSymbolTable()->getHdlHier();
      }
      else
      {
        sigNames = &sigs1;
        checker = mWaveReader2;
        pather = mWaveReader1->getSymbolTable()->getHdlHier();
      }
      INFO_ASSERT(sigNames, "No list of signal names found.");
      INFO_ASSERT(checker, "No wavereader selected!");
    }
    else 
    {
      sigNames = &sigs1;
      checker = mWaveReader2;
      pather = mWaveReader1->getSymbolTable()->getHdlHier();
    }
    
    for (VecSigType::iterator p = sigNames->begin();
         p != sigNames->end(); ++p)
    {
      WfAbsFileReader::SigHier& sigHier = *p;
      UtString name;
      pather->compPathHier(sigHier.mLeaf, &name);
      WfAbsSignal* signal = checker->findSignal(name, NULL);
      if (signal == NULL)
      {
        mScreen << "Error: " << name.c_str() << " not found in " << checker->getFileName()
                << UtIO::endl;
        isGood = false;
      }
#if 0
      else
        delete signal;
#endif
    }

    // compare symbol tables, to do alias comparison
    // only do if isGood is true; otherwise, the messaging could be
    // senseless
    if (isGood)
    {
      STSymbolTable* sym1 = mWaveReader1->getSymbolTable();
      STSymbolTable* sym2 = mWaveReader2->getSymbolTable();
      STSymbolTable* src = sym1;
      STSymbolTable* cmp = sym2;
      WfAbsFileReader* cmpWR = mWaveReader2;
      if (sym1->numNodes() < sym2->numNodes())
      {
        src = sym2;
        cmp = sym1;
        cmpWR = mWaveReader1;
      }
      
      const HdlHierPath* pather = src->getHdlHier();
      for (STSymbolTable::NodeLoopSorted loop = src->getNodeLoopSorted();
	   not loop.atEnd(); 
	   ++loop)
      {
	const STSymbolTableNode * src_node = *loop;
        if (cmp->lookup(src_node) == NULL)
        {
          isGood = false;
          UtString name;
          pather->compPathHier(src_node, &name);
          mScreen << "Error: " << name.c_str() << " not in " << cmpWR->getFileName()
                  << UtIO::endl;
        }
      }
    }
    return isGood;
  }

  bool checkValues(WfAbsFileReader* reader1,
                   VecSigType& sigs1, 
                   WfAbsFileReader* reader2,
                   VecSigType& sigs2)
  {
    bool allMatch = true;
    WfAbsSignalVCIter* sig1Iter;
    WfAbsSignalVCIter* sig2Iter;
    size_t numSigs = sigs1.size();
    UtString buf;
    for (size_t i = 0; i < numSigs; ++i)
    {
      WfAbsSignal* sig = sigs1[i].mSignal;
      bool isReal = sig->isReal();

      buf.clear();
      sig1Iter = reader1->createSignalVCIter(sig, &buf);
      if (! buf.empty())
        fprintf(stderr, "%s\n", buf.c_str());
      
      buf.clear();

      FUNC_ASSERT(sig1Iter, sig->print());
      sig = sigs2[i].mSignal;
      sig2Iter = reader2->createSignalVCIter(sig, &buf);
      if (! buf.empty())
        fprintf(stderr, "%s\n", buf.c_str());
      
      
      FUNC_ASSERT(sig2Iter, sig->print());
      
      sig1Iter->gotoMinTime();
      sig2Iter->gotoMinTime();

      SigVal sig1Val;
      SigVal sig2Val;

      bool wave1HasVals = ! sig1Iter->atEnd();
      bool wave2HasVals = ! sig2Iter->atEnd();

      while (wave1HasVals && wave2HasVals)
      {
        if (sig1Iter->getTime() != sig2Iter->getTime())
        {
          UtString name;
          mWaveReader1->getSymbolTable()->getHdlHier()->compPathHier(sigs1[i].mLeaf, &name);
          mScreen << "Signal: " << name << " wave 1 time: " 
                  << sig1Iter->getTime() << " != wave 2 time: " 
                  << sig2Iter->getTime() << UtIO::endl;
          allMatch = false;
        }
        
        bool isDiff = false;
        if (! isReal)
        {
          sig1Iter->getCompleteValue(sig1Val.getValPtr());
          sig2Iter->getCompleteValue(sig2Val.getValPtr());
          isDiff = (sig1Val.compare(sig2Val) != 0);
        }
        else
        {
          CarbonReal sig1Real = sig1Iter->getValueReal();
          CarbonReal sig2Real = sig2Iter->getValueReal();
          isDiff = (sig1Real != sig2Real);
        }
        
        if (isDiff)
        {
          UtString name;
          mWaveReader1->getSymbolTable()->getHdlHier()->compPathHier(sigs1[i].mLeaf, &name);
          mScreen << "Signal: " << name << " time: " << sig1Iter->getTime() << UtIO::endl;
          UtString buf;
          sig1Iter->getValueStr(&buf);
          mScreen << "   1:" << buf << UtIO::endl;
          sig2Iter->getValueStr(&buf);
          mScreen << "   2:" << buf << UtIO::endl;
          allMatch = false;
        }
        
        sig1Iter->gotoNextChangeTime();
        sig2Iter->gotoNextChangeTime();
        wave1HasVals = ! sig1Iter->atEnd();
        wave2HasVals = ! sig2Iter->atEnd();
      }
      
      if (wave1HasVals)
      {
        UtString name;
        mWaveReader1->getSymbolTable()->getHdlHier()->compPathHier(sigs1[i].mLeaf, &name);
        
        mScreen << "Premature end of " << mWaveReader2->getFileName() 
                << " for " << name << UtIO::endl;
        allMatch = false;
      }
      if (wave2HasVals)
      {
        UtString name;
        mWaveReader1->getSymbolTable()->getHdlHier()->compPathHier(sigs1[i].mLeaf, &name);
        mScreen << "Premature end of " << mWaveReader1->getFileName() 
                << " for " << name << UtIO::endl;
        allMatch = false;
      }
      
      delete sig1Iter;
      delete sig2Iter;
    }
    return allMatch;
  }
};

class SummaryMode : public virtual MainRunMode
{
public:
  SummaryMode() : mWaveReader(NULL)
  {}
  
  virtual ~SummaryMode() {
    delete mWaveReader;
  }
  
  static void addArgs(ArgProc& args, UtString& prefix)
  {
    UtString buf(prefix);
    buf << "summary ";
    buf << scInFile << " <wave file> ";
    args.addSynopsis(buf);
  }
  
  bool run(ArgProc& args) 
  {
    const char* inFile = args.getStrLast(scInFile);
    mWaveReader = readInputFile(inFile);
    
    if (! mWaveReader)
    {
      mScreen << "Error: Problem loading wave file " << UtIO::endl;
      return false;
    }
        
    return doSummary();
  }
  
private:
  WfAbsFileReader* mWaveReader;

  bool doSummary()
  {
    mScreen << "Total number of signals: " << mWaveReader->getNumSignals() << UtIO::endl;
    // don't watch any signals and get the max sim time
    UtString msg;
    WfAbsFileReader::Status stat = mWaveReader->loadValues(&msg);
    if (stat == WfAbsFileReader::eError)
    {
      fprintf(stderr, "Error loading values: %s\n", msg.c_str());
      return false;
    }
    
    mScreen << "Max Sim time: " << mWaveReader->getMaxSimulationTime() << UtIO::endl;
    
    return true;
  }
};

class MainContext
{
public:
  MainContext() : mScreen(UtIO::cout()), mRunMode(NULL)

  {
    mArgProc.setDescription("Wave Query Utility","wavequery", "General wave dump utility that prints information about signals in the wave file. It currently can dump the names of all the signals, and it can do a straight diff of two wave files. VERY IMPORTANT!!! Diff mode does not check that the two files are the same. This is VERY useful in testing the reader.");
    const char* sect1 = "Modes";
    mArgProc.createSection(sect1);
    mArgProc.addString(scMode, "'dumphier': Dump the module hierarchy found in the next input wave file, 'dumpnames': Dump the names of all the nets that were found in the input wave file, 'diff' Do a straight diff of the wave files, 'summary' Currently only prints out the total number of signals and the max time.", "dumpnames", false, false, 1);
    mArgProc.addToSection(sect1, scMode);
    const char* sect2 = "Input Control";
    mArgProc.createSection(sect2);
    mArgProc.addInputFile(scInFile, "Input wave file. Depending on the mode, this option may appear more than once.", "in.dump", false, true, 1);
    mArgProc.addToSection(sect2, scInFile);
    mArgProc.addString(scType, "Input wave type. 'vcd' and 'fsdb' are currently only supported. fsdb format is only supported on Linux.", "vcd", false, false, 1);    
    mArgProc.addToSection(sect2, scType);
    
    UtString buf;
    buf << "wavequery [" << scType << " vcd] " << scMode << " ";
    DumpMode::addArgs(mArgProc, buf);
    DiffMode::addArgs(mArgProc, buf);
    HierMode::addArgs(mArgProc, buf);
    SummaryMode::addArgs(mArgProc, buf);
  }
  
  ~MainContext() {
    delete mRunMode;
  }

  int run(int* argc, char** argv)
  {
    bool isGood = true;
    isGood = isGood && processArgs(argc, argv);
    isGood = isGood && mRunMode->run(mArgProc);
    int stat = 0;
    if (! isGood)
      stat = 1;
    return stat;
  }

  
private:
  UtOStream& mScreen;  
  ArgProc mArgProc;
  MainRunMode* mRunMode;
  
  bool processArgs(int* argc, char** argv)
  {
    UtString errMsg;
    int numOptions;
    if (mArgProc.parseCommandLine(argc, argv, &numOptions, &errMsg) != ArgProc::eParsed)
    {
      mScreen << errMsg << UtIO::endl;
      return false;
    }
    
    if (numOptions == 0)
    {
      UtString usage;
      mArgProc.getUsageVerbose(&usage);
      mScreen << usage << UtIO::endl;
      mScreen.flush();
      exit(0);
    }

    const char* type;
    CarbonWaveFileType fileType = eWaveFileTypeVCD;
    if (mArgProc.isParsed(scType) == ArgProc::eParsed)
    {
      mArgProc.getStrValue(scType, &type);
      if (strcmp(type, "vcd") == 0)
        fileType = eWaveFileTypeVCD;
      else if (strcmp(type, "fsdb") == 0)
        fileType = eWaveFileTypeFSDB;
      else
      {
        mScreen << "Error: Supports only vcd and fsdb wave types. Unknown type: " << type << UtIO::endl;
        return false;
      }
    }
    
    const char* mode = mArgProc.getStrValue(scMode);
    if (strcmp(mode, scDumpNames) == 0)
      mRunMode = new DumpMode;
    else if (strcmp(mode, scDiff) == 0)
      mRunMode = new DiffMode;
    else if (strcmp(mode, scSummary) == 0)
      mRunMode = new SummaryMode;
    else if (strcmp(mode, scDumpHier) == 0)
      mRunMode = new HierMode;
    else
    {
      mScreen << "Error: Unknown mode '" << mode << "'" << UtIO::endl;
      return false;
    }
    mRunMode->setFileType(fileType);
    return true;
  }

};

int main(int argc, char* argv[])
{
  MainContext mc;
  return mc.run(&argc, argv);
}
