// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WTBGAPPLICATION_H_
#define __WTBGAPPLICATION_H_


#include "modshell/MdsApplication.h"


/*!
  \file
  The WtbgApplication class.
*/

//! WtbgApplication class
/*!
  Application class for Carbon Wave Testbench Generator.  Handles
  command line processing and top level control flow.
*/
class WtbgApplication : public MdsApplication
{
 public:
  //! Constructor.
  /*!
    Construct a top level application object.
    \param argc Number of command line arguments.
    \param argv Array of command line arguments.
   */
  WtbgApplication (int* argc, char **argv);

  //! Destructor.
  virtual ~WtbgApplication(void);

  //! Run the application.
  /*!
    Run the application.
    \return The exit status of program.
  */
  SInt32 run(void);

private:
  //! Register command line options.
  /*!
    Register the command line options.
  */
  void setupCommandLineOptions(void);
  bool checkCommandLineOptions(void);

private:
  WtbgApplication(void);
  WtbgApplication(const WtbgApplication&);
  WtbgApplication& operator=(const WtbgApplication&);
};


#endif
