// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "WtbgApplication.h"
#include "util/MemManager.h"

/*!
  \file
  The main routine for Carbon Wave Testbench generator.
*/


int main(int argc, char **argv)
{
  //  CarbonMem::overrideNewDelete();
  WtbgApplication app(&argc, argv);
  return app.run();
}
