// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "WtbgApplication.h"
#include "modshell/MdsGenerator.h"
#include "util/UtString.h"

static const char* scAllowFsdbWrite = "-allowFsdbWrite";
static const char* scAllowFsdbRead = "-allowFsdbRead";

WtbgApplication::WtbgApplication(int *argc, char** argv) : MdsApplication(argc, argv)
{
}


WtbgApplication::~WtbgApplication()
{
}


SInt32 WtbgApplication::run(void)
{
  /*
  ** Command line processing.
  */
  mArgs.setDescription("Wave Testbench Generator",
                       "cwavetestbenchgen",
		       "Creates an executable that "
		       "drives a single carbon model with a "
		       "waveform file and compares the output with the "
		       "expected results.");

  setupCommandLineOptions();
  if (! parseCommandLine())
    return 1;
  if (! checkCommandLineOptions())
    return 1;

  /*
  ** Setup the IODB.
  */
  createIODB();
  //createConfig();

  /*
  ** Dynamically generate the object wrapper for the model.  Maybe
  ** cbuild does this someday.
  */
  const char* preSched = NULL;
  const char* postSched = NULL;
  const char* userInit = NULL;
  const char* userDestroy = NULL;

  UtString preSchedStr;
  UtString postSchedStr;
  UtString userInitStr;
  UtString userDestroyStr;

  if (getPreSchedFnName(&preSchedStr))
    preSched = preSchedStr.c_str();

  if (getPostSchedFnName(&postSchedStr))
    postSched = postSchedStr.c_str();

  if (getUserInitFnName(&userInitStr))
    userInit = userInitStr.c_str();

  if (getUserDestroyFnName(&userDestroyStr))
    userDestroy = userDestroyStr.c_str();
  

  bool allowFsdbRead = mArgs.getBoolValue(scAllowFsdbRead);
  bool allowFsdbWrite = mArgs.getBoolValue(scAllowFsdbWrite);
                        
  MdsGenerator gen(mModelName, mInstallDir, mTargetDir, mMsgContext, preSched, postSched, userInit, userDestroy, allowFsdbWrite, allowFsdbRead);
  gen.generate();
  
  // Get the full path to the WtbMain.cxx file
  UtString mainSrc;
  mainSrc << mInstallDir << "/include/carbon/WtbMain.cxx";
  /*
  ** Link the C driver.
  */
  const char *exe, *lib;
  mArgs.getStrValue("-exe", &exe);
  mArgs.getStrValue("-lib", &lib);

  linkExecutable(exe, lib, mIODB->getTargetCompiler(), mIODB->getTargetCompilerVersion (), mainSrc.c_str(), allowFsdbWrite, allowFsdbRead);
  
  return 0;
}


void WtbgApplication::setupCommandLineOptions(void)
{
  MdsApplication::setupCommandLineOptions();
  mArgs.addString("-exe", "Executable name", "cwavetestbench", false, false, 1);
  mArgs.addToSection(MdsApplication::scOutputControl, "-exe");
  mArgs.addBool(scAllowFsdbWrite, "Allow the option to dump fsdb format for debugging. This increases the i-cache and may have some minor negative performance effects", false, 1);
  mArgs.addToSection(MdsApplication::scOutputControl, scAllowFsdbWrite);
  mArgs.addString("-lib", "Library name", "cwavetestbench", false, false, 1);
  mArgs.addToSection(MdsApplication::scInputControl, "-lib");
}


bool WtbgApplication::checkCommandLineOptions(void)
{
  return MdsApplication::checkCommandLineOptions();
}
