/*
*********************************************************************
**
** Copyright (C) 1992-2006 Trolltech AS. All rights reserved.
**
** This file is part of the Qt Assistant of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
*********************************************************************
*/

#ifndef UI_HELPDIALOG_H
#define UI_HELPDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListView>
#include <QtGui/QListWidget>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

class Ui_HelpDialog
{
public:
    QVBoxLayout *vboxLayout;
    QTabWidget *tabWidget;
    QWidget *contentPage;
    QVBoxLayout *vboxLayout1;
    QTreeWidget *listContents;
    QWidget *Widget8;
    QVBoxLayout *vboxLayout2;
    QLabel *TextLabel1;
    QLineEdit *editIndex;
    QListView *listIndex;
    QWidget *Widget9;
    QVBoxLayout *vboxLayout3;
    QTreeWidget *listBookmarks;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem;
    QPushButton *buttonAdd;
    QPushButton *buttonRemove;
    QWidget *searchTab;
    QGridLayout *gridLayout;
    QSpacerItem *spacerItem1;
    QLabel *TextLabel1_2;
    QLineEdit *termsEdit;
    QListWidget *resultBox;
    QLabel *TextLabel2;
    QHBoxLayout *hboxLayout1;
    QPushButton *helpButton;
    QSpacerItem *spacerItem2;
    QPushButton *searchButton;
    QFrame *framePrepare;
    QHBoxLayout *hboxLayout2;
    QLabel *labelPrepare;
    QProgressBar *progressPrepare;

    void setupUi(QWidget *HelpDialog)
    {
    HelpDialog->setObjectName(QString::fromUtf8("HelpDialog"));
    HelpDialog->resize(QSize(274, 417).expandedTo(HelpDialog->minimumSizeHint()));
    vboxLayout = new QVBoxLayout(HelpDialog);
    vboxLayout->setSpacing(6);
    vboxLayout->setMargin(0);
    vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
    tabWidget = new QTabWidget(HelpDialog);
    tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
    contentPage = new QWidget();
    contentPage->setObjectName(QString::fromUtf8("contentPage"));
    vboxLayout1 = new QVBoxLayout(contentPage);
    vboxLayout1->setSpacing(6);
    vboxLayout1->setMargin(5);
    vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
    listContents = new QTreeWidget(contentPage);
    listContents->setObjectName(QString::fromUtf8("listContents"));
    listContents->setContextMenuPolicy(Qt::CustomContextMenu);
    listContents->setRootIsDecorated(true);
    listContents->setUniformRowHeights(true);

    vboxLayout1->addWidget(listContents);

    tabWidget->addTab(contentPage, QApplication::translate("HelpDialog", "Con&tents", 0, QApplication::UnicodeUTF8));
    Widget8 = new QWidget();
    Widget8->setObjectName(QString::fromUtf8("Widget8"));
    vboxLayout2 = new QVBoxLayout(Widget8);
    vboxLayout2->setSpacing(6);
    vboxLayout2->setMargin(5);
    vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
    TextLabel1 = new QLabel(Widget8);
    TextLabel1->setObjectName(QString::fromUtf8("TextLabel1"));

    vboxLayout2->addWidget(TextLabel1);

    editIndex = new QLineEdit(Widget8);
    editIndex->setObjectName(QString::fromUtf8("editIndex"));

    vboxLayout2->addWidget(editIndex);

    listIndex = new QListView(Widget8);
    listIndex->setObjectName(QString::fromUtf8("listIndex"));
    listIndex->setContextMenuPolicy(Qt::CustomContextMenu);

    vboxLayout2->addWidget(listIndex);

    tabWidget->addTab(Widget8, QApplication::translate("HelpDialog", "&Index", 0, QApplication::UnicodeUTF8));
    Widget9 = new QWidget();
    Widget9->setObjectName(QString::fromUtf8("Widget9"));
    vboxLayout3 = new QVBoxLayout(Widget9);
    vboxLayout3->setSpacing(6);
    vboxLayout3->setMargin(5);
    vboxLayout3->setObjectName(QString::fromUtf8("vboxLayout3"));
    listBookmarks = new QTreeWidget(Widget9);
    listBookmarks->setObjectName(QString::fromUtf8("listBookmarks"));
    listBookmarks->setContextMenuPolicy(Qt::CustomContextMenu);
    listBookmarks->setUniformRowHeights(true);

    vboxLayout3->addWidget(listBookmarks);

    hboxLayout = new QHBoxLayout();
    hboxLayout->setSpacing(6);
    hboxLayout->setMargin(0);
    hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
    spacerItem = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    hboxLayout->addItem(spacerItem);

    buttonAdd = new QPushButton(Widget9);
    buttonAdd->setObjectName(QString::fromUtf8("buttonAdd"));

    hboxLayout->addWidget(buttonAdd);

    buttonRemove = new QPushButton(Widget9);
    buttonRemove->setObjectName(QString::fromUtf8("buttonRemove"));

    hboxLayout->addWidget(buttonRemove);


    vboxLayout3->addLayout(hboxLayout);

    tabWidget->addTab(Widget9, QApplication::translate("HelpDialog", "&Bookmarks", 0, QApplication::UnicodeUTF8));
    searchTab = new QWidget();
    searchTab->setObjectName(QString::fromUtf8("searchTab"));
    gridLayout = new QGridLayout(searchTab);
    gridLayout->setSpacing(6);
    gridLayout->setMargin(5);
    gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
    spacerItem1 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

    gridLayout->addItem(spacerItem1, 3, 0, 1, 1);

    TextLabel1_2 = new QLabel(searchTab);
    TextLabel1_2->setObjectName(QString::fromUtf8("TextLabel1_2"));

    gridLayout->addWidget(TextLabel1_2, 0, 0, 1, 1);

    termsEdit = new QLineEdit(searchTab);
    termsEdit->setObjectName(QString::fromUtf8("termsEdit"));

    gridLayout->addWidget(termsEdit, 1, 0, 1, 1);

    resultBox = new QListWidget(searchTab);
    resultBox->setObjectName(QString::fromUtf8("resultBox"));
    resultBox->setContextMenuPolicy(Qt::CustomContextMenu);

    gridLayout->addWidget(resultBox, 5, 0, 1, 1);

    TextLabel2 = new QLabel(searchTab);
    TextLabel2->setObjectName(QString::fromUtf8("TextLabel2"));

    gridLayout->addWidget(TextLabel2, 4, 0, 1, 1);

    hboxLayout1 = new QHBoxLayout();
    hboxLayout1->setSpacing(6);
    hboxLayout1->setMargin(1);
    hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
    helpButton = new QPushButton(searchTab);
    helpButton->setObjectName(QString::fromUtf8("helpButton"));

    hboxLayout1->addWidget(helpButton);

    spacerItem2 = new QSpacerItem(61, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

    hboxLayout1->addItem(spacerItem2);

    searchButton = new QPushButton(searchTab);
    searchButton->setObjectName(QString::fromUtf8("searchButton"));

    hboxLayout1->addWidget(searchButton);


    gridLayout->addLayout(hboxLayout1, 2, 0, 1, 1);

    tabWidget->addTab(searchTab, QApplication::translate("HelpDialog", "&Search", 0, QApplication::UnicodeUTF8));

    vboxLayout->addWidget(tabWidget);

    framePrepare = new QFrame(HelpDialog);
    framePrepare->setObjectName(QString::fromUtf8("framePrepare"));
    framePrepare->setFrameShape(QFrame::StyledPanel);
    framePrepare->setFrameShadow(QFrame::Raised);
    hboxLayout2 = new QHBoxLayout(framePrepare);
    hboxLayout2->setSpacing(6);
    hboxLayout2->setMargin(3);
    hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
    labelPrepare = new QLabel(framePrepare);
    labelPrepare->setObjectName(QString::fromUtf8("labelPrepare"));

    hboxLayout2->addWidget(labelPrepare);

    progressPrepare = new QProgressBar(framePrepare);
    progressPrepare->setObjectName(QString::fromUtf8("progressPrepare"));

    hboxLayout2->addWidget(progressPrepare);


    vboxLayout->addWidget(framePrepare);

    TextLabel1->setBuddy(editIndex);
    TextLabel1_2->setBuddy(termsEdit);
    TextLabel2->setBuddy(resultBox);
    QWidget::setTabOrder(tabWidget, listContents);
    QWidget::setTabOrder(listContents, editIndex);
    QWidget::setTabOrder(editIndex, listIndex);
    QWidget::setTabOrder(listIndex, listBookmarks);
    QWidget::setTabOrder(listBookmarks, buttonAdd);
    QWidget::setTabOrder(buttonAdd, buttonRemove);
    QWidget::setTabOrder(buttonRemove, termsEdit);
    QWidget::setTabOrder(termsEdit, searchButton);
    QWidget::setTabOrder(searchButton, helpButton);
    QWidget::setTabOrder(helpButton, resultBox);
    retranslateUi(HelpDialog);

    QMetaObject::connectSlotsByName(HelpDialog);
    } // setupUi

    void retranslateUi(QWidget *HelpDialog)
    {
    HelpDialog->setWindowTitle(QApplication::translate("HelpDialog", "Help", 0, QApplication::UnicodeUTF8));
    HelpDialog->setWhatsThis(QApplication::translate("HelpDialog", "<b>Help</b><p>Choose the topic you want help on from the contents list, or search the index for keywords.</p>", 0, QApplication::UnicodeUTF8));
    tabWidget->setWhatsThis(QApplication::translate("HelpDialog", "Displays help topics organized by category, index or bookmarks. Another tab inherits the full text search.", 0, QApplication::UnicodeUTF8));
    listContents->headerItem()->setText(0, QApplication::translate("HelpDialog", "column 1", 0, QApplication::UnicodeUTF8));
    listContents->setWhatsThis(QApplication::translate("HelpDialog", "<b>Help topics organized by category.</b><p>Double-click an item to see the topics in that category. To view a topic, just double-click it.</p>", 0, QApplication::UnicodeUTF8));
    tabWidget->setTabText(tabWidget->indexOf(contentPage), QApplication::translate("HelpDialog", "Con&tents", 0, QApplication::UnicodeUTF8));
    TextLabel1->setText(QApplication::translate("HelpDialog", "&Look For:", 0, QApplication::UnicodeUTF8));
    editIndex->setToolTip(QApplication::translate("HelpDialog", "Enter keyword", 0, QApplication::UnicodeUTF8));
    editIndex->setWhatsThis(QApplication::translate("HelpDialog", "<b>Enter a keyword.</b><p>The list will select an item that matches the entered string best.</p>", 0, QApplication::UnicodeUTF8));
    listIndex->setWhatsThis(QApplication::translate("HelpDialog", "<b>List of available help topics.</b><p>Double-click on an item to open its help page. If more than one is found, you must specify which page you want.</p>", 0, QApplication::UnicodeUTF8));
    tabWidget->setTabText(tabWidget->indexOf(Widget8), QApplication::translate("HelpDialog", "&Index", 0, QApplication::UnicodeUTF8));
    listBookmarks->headerItem()->setText(0, QApplication::translate("HelpDialog", "column 1", 0, QApplication::UnicodeUTF8));
    listBookmarks->setWhatsThis(QApplication::translate("HelpDialog", "Displays the list of bookmarks.", 0, QApplication::UnicodeUTF8));
    buttonAdd->setToolTip(QApplication::translate("HelpDialog", "Add new bookmark", 0, QApplication::UnicodeUTF8));
    buttonAdd->setWhatsThis(QApplication::translate("HelpDialog", "Add the currently displayed page as a new bookmark.", 0, QApplication::UnicodeUTF8));
    buttonAdd->setText(QApplication::translate("HelpDialog", "&New", 0, QApplication::UnicodeUTF8));
    buttonRemove->setToolTip(QApplication::translate("HelpDialog", "Delete bookmark", 0, QApplication::UnicodeUTF8));
    buttonRemove->setWhatsThis(QApplication::translate("HelpDialog", "Delete the selected bookmark.", 0, QApplication::UnicodeUTF8));
    buttonRemove->setText(QApplication::translate("HelpDialog", "&Delete", 0, QApplication::UnicodeUTF8));
    tabWidget->setTabText(tabWidget->indexOf(Widget9), QApplication::translate("HelpDialog", "&Bookmarks", 0, QApplication::UnicodeUTF8));
    TextLabel1_2->setText(QApplication::translate("HelpDialog", "Searching f&or:", 0, QApplication::UnicodeUTF8));
    termsEdit->setToolTip(QApplication::translate("HelpDialog", "Enter searchword(s).", 0, QApplication::UnicodeUTF8));
    termsEdit->setWhatsThis(QApplication::translate("HelpDialog", "<b>Enter search word(s).</b><p>Enter here the word(s) you are looking for. The words may contain wildcards (*). For a sequence of words quote them.</p>", 0, QApplication::UnicodeUTF8));
    resultBox->setWhatsThis(QApplication::translate("HelpDialog", "<b>Found documents</b><p>This list contains all found documents from the last search. The documents are ordered, i.e. the first document has the most matches.</p>", 0, QApplication::UnicodeUTF8));
    TextLabel2->setText(QApplication::translate("HelpDialog", "Found &Documents:", 0, QApplication::UnicodeUTF8));
    helpButton->setToolTip(QApplication::translate("HelpDialog", "Display the help page.", 0, QApplication::UnicodeUTF8));
    helpButton->setWhatsThis(QApplication::translate("HelpDialog", "Display the help page for the full text search.", 0, QApplication::UnicodeUTF8));
    helpButton->setText(QApplication::translate("HelpDialog", "He&lp", 0, QApplication::UnicodeUTF8));
    searchButton->setToolTip(QApplication::translate("HelpDialog", "Start searching.", 0, QApplication::UnicodeUTF8));
    searchButton->setWhatsThis(QApplication::translate("HelpDialog", "Pressing this button starts the search.", 0, QApplication::UnicodeUTF8));
    searchButton->setText(QApplication::translate("HelpDialog", "&Search", 0, QApplication::UnicodeUTF8));
    tabWidget->setTabText(tabWidget->indexOf(searchTab), QApplication::translate("HelpDialog", "&Search", 0, QApplication::UnicodeUTF8));
    labelPrepare->setText(QApplication::translate("HelpDialog", "Preparing...", 0, QApplication::UnicodeUTF8));
    Q_UNUSED(HelpDialog);
    } // retranslateUi

};

namespace Ui {
    class HelpDialog: public Ui_HelpDialog {};
} // namespace Ui

#endif // UI_HELPDIALOG_H
