/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/UtString.h"
#include "amainwindow.h"
#include "config.h"
#include "assistant/Assistant.h"
#include <QDir>

Assistant::Assistant(const char* helpDir) {
  //UtString profile;
  //OSConstructFilePath(&profile, helpDir, "help.profile");
  Config* conf = Config::loadConfig(QString());
  conf->hideSideBar( true );

  QDir dir( helpDir );
  if ( dir.exists() ) {
    conf->saveProfile(Profile::createDefaultProfile(dir.absolutePath()));
    conf->loadDefaultProfile();
    conf->setDocRebuild(true);
    conf->save();
  }
  mMainWindow = new AssistantMainWindow;
  mMainWindow->setObjectName(QLatin1String("CarbonAssistant"));
}

Assistant::~Assistant() {
  delete mMainWindow;
}

void Assistant::show(const char* filename) {
  mMainWindow->showLink( AssistantMainWindow::urlifyFileName(filename) );
  mMainWindow->show();
  mMainWindow->raise();
}
