/*
*********************************************************************
**
** Copyright (C) 1992-2006 Trolltech AS. All rights reserved.
**
** This file is part of the Qt Assistant of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
*********************************************************************
*/

#ifndef UI_TOPICCHOOSER_H
#define UI_TOPICCHOOSER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

class Ui_TopicChooser
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *label;
    QListWidget *listbox;
    QWidget *Layout16;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem;
    QPushButton *buttonDisplay;
    QPushButton *buttonCancel;

    void setupUi(QDialog *TopicChooser)
    {
    TopicChooser->setObjectName(QString::fromUtf8("TopicChooser"));
    TopicChooser->resize(QSize(391, 223).expandedTo(TopicChooser->minimumSizeHint()));
    TopicChooser->setSizeGripEnabled(true);
    vboxLayout = new QVBoxLayout(TopicChooser);
    vboxLayout->setSpacing(6);
    vboxLayout->setMargin(11);
    vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
    vboxLayout->setObjectName(QString::fromUtf8("unnamed"));
    label = new QLabel(TopicChooser);
    label->setObjectName(QString::fromUtf8("label"));

    vboxLayout->addWidget(label);

    listbox = new QListWidget(TopicChooser);
    listbox->setObjectName(QString::fromUtf8("listbox"));

    vboxLayout->addWidget(listbox);

    Layout16 = new QWidget(TopicChooser);
    Layout16->setObjectName(QString::fromUtf8("Layout16"));
    hboxLayout = new QHBoxLayout(Layout16);
    hboxLayout->setSpacing(6);
    hboxLayout->setMargin(0);
    hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
    hboxLayout->setObjectName(QString::fromUtf8("unnamed"));
    spacerItem = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    hboxLayout->addItem(spacerItem);

    buttonDisplay = new QPushButton(Layout16);
    buttonDisplay->setObjectName(QString::fromUtf8("buttonDisplay"));
    buttonDisplay->setAutoDefault(true);
    buttonDisplay->setDefault(true);

    hboxLayout->addWidget(buttonDisplay);

    buttonCancel = new QPushButton(Layout16);
    buttonCancel->setObjectName(QString::fromUtf8("buttonCancel"));
    buttonCancel->setAutoDefault(true);

    hboxLayout->addWidget(buttonCancel);


    vboxLayout->addWidget(Layout16);

    label->setBuddy(listbox);
    retranslateUi(TopicChooser);

    QMetaObject::connectSlotsByName(TopicChooser);
    } // setupUi

    void retranslateUi(QDialog *TopicChooser)
    {
    TopicChooser->setWindowTitle(QApplication::translate("TopicChooser", "Choose Topic", 0, QApplication::UnicodeUTF8));
    TopicChooser->setProperty("whatsThis", QVariant(QApplication::translate("TopicChooser", "Select a topic from the list and click the <b>Display</b>-button to open the online help.", 0, QApplication::UnicodeUTF8)));
    label->setText(QApplication::translate("TopicChooser", "&Topics", 0, QApplication::UnicodeUTF8));
    listbox->setProperty("whatsThis", QVariant(QApplication::translate("TopicChooser", "Displays a list of available help topics for the keyword.", 0, QApplication::UnicodeUTF8)));
    buttonDisplay->setText(QApplication::translate("TopicChooser", "&Display", 0, QApplication::UnicodeUTF8));
    buttonDisplay->setProperty("whatsThis", QVariant(QApplication::translate("TopicChooser", "Open the topic selected in the list.", 0, QApplication::UnicodeUTF8)));
    buttonCancel->setText(QApplication::translate("TopicChooser", "&Close", 0, QApplication::UnicodeUTF8));
    buttonCancel->setProperty("whatsThis", QVariant(QApplication::translate("TopicChooser", "Close the Dialog.", 0, QApplication::UnicodeUTF8)));
    Q_UNUSED(TopicChooser);
    } // retranslateUi

};

namespace Ui {
    class TopicChooser: public Ui_TopicChooser {};
} // namespace Ui

#endif // UI_TOPICCHOOSER_H
