/*
*********************************************************************
**
** Copyright (C) 1992-2006 Trolltech AS. All rights reserved.
**
** This file is part of the Qt Assistant of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
*********************************************************************
*/

#ifndef UI_FINDDIALOG_H
#define UI_FINDDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

class Ui_FindDialog
{
public:
    QGridLayout *gridLayout;
    QSpacerItem *spacerItem;
    QSpacerItem *spacerItem1;
    QPushButton *findButton;
    QPushButton *closeButton;
    QGroupBox *ButtonGroup1;
    QVBoxLayout *vboxLayout;
    QCheckBox *checkWords;
    QCheckBox *checkCase;
    QGroupBox *ButtonGroup2;
    QVBoxLayout *vboxLayout1;
    QRadioButton *radioForward;
    QRadioButton *radioBackward;
    QHBoxLayout *hboxLayout;
    QLabel *TextLabel1;
    QComboBox *comboFind;

    void setupUi(QWidget *FindDialog)
    {
    FindDialog->setObjectName(QString::fromUtf8("FindDialog"));
    FindDialog->resize(QSize(400, 172).expandedTo(FindDialog->minimumSizeHint()));
    gridLayout = new QGridLayout(FindDialog);
    gridLayout->setSpacing(6);
    gridLayout->setMargin(11);
    gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
    spacerItem = new QSpacerItem(20, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

    gridLayout->addItem(spacerItem, 3, 0, 1, 1);

    spacerItem1 = new QSpacerItem(110, 31, QSizePolicy::Minimum, QSizePolicy::Expanding);

    gridLayout->addItem(spacerItem1, 2, 2, 1, 1);

    findButton = new QPushButton(FindDialog);
    findButton->setObjectName(QString::fromUtf8("findButton"));
    findButton->setDefault(true);

    gridLayout->addWidget(findButton, 0, 2, 1, 1);

    closeButton = new QPushButton(FindDialog);
    closeButton->setObjectName(QString::fromUtf8("closeButton"));

    gridLayout->addWidget(closeButton, 1, 2, 1, 1);

    ButtonGroup1 = new QGroupBox(FindDialog);
    ButtonGroup1->setObjectName(QString::fromUtf8("ButtonGroup1"));
    vboxLayout = new QVBoxLayout(ButtonGroup1);
    vboxLayout->setSpacing(6);
    vboxLayout->setMargin(11);
    vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
    checkWords = new QCheckBox(ButtonGroup1);
    checkWords->setObjectName(QString::fromUtf8("checkWords"));

    vboxLayout->addWidget(checkWords);

    checkCase = new QCheckBox(ButtonGroup1);
    checkCase->setObjectName(QString::fromUtf8("checkCase"));

    vboxLayout->addWidget(checkCase);


    gridLayout->addWidget(ButtonGroup1, 1, 0, 2, 1);

    ButtonGroup2 = new QGroupBox(FindDialog);
    ButtonGroup2->setObjectName(QString::fromUtf8("ButtonGroup2"));
    vboxLayout1 = new QVBoxLayout(ButtonGroup2);
    vboxLayout1->setSpacing(6);
    vboxLayout1->setMargin(11);
    vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
    radioForward = new QRadioButton(ButtonGroup2);
    radioForward->setObjectName(QString::fromUtf8("radioForward"));
    radioForward->setChecked(true);

    vboxLayout1->addWidget(radioForward);

    radioBackward = new QRadioButton(ButtonGroup2);
    radioBackward->setObjectName(QString::fromUtf8("radioBackward"));

    vboxLayout1->addWidget(radioBackward);


    gridLayout->addWidget(ButtonGroup2, 1, 1, 2, 1);

    hboxLayout = new QHBoxLayout();
    hboxLayout->setSpacing(6);
    hboxLayout->setMargin(0);
    hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
    TextLabel1 = new QLabel(FindDialog);
    TextLabel1->setObjectName(QString::fromUtf8("TextLabel1"));

    hboxLayout->addWidget(TextLabel1);

    comboFind = new QComboBox(FindDialog);
    comboFind->setObjectName(QString::fromUtf8("comboFind"));
    QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(0));
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(comboFind->sizePolicy().hasHeightForWidth());
    comboFind->setSizePolicy(sizePolicy);
    comboFind->setEditable(true);
    comboFind->setDuplicatesEnabled(false);

    hboxLayout->addWidget(comboFind);


    gridLayout->addLayout(hboxLayout, 0, 0, 1, 2);

    TextLabel1->setBuddy(comboFind);
    QWidget::setTabOrder(comboFind, checkWords);
    QWidget::setTabOrder(checkWords, checkCase);
    QWidget::setTabOrder(checkCase, radioForward);
    QWidget::setTabOrder(radioForward, radioBackward);
    QWidget::setTabOrder(radioBackward, findButton);
    QWidget::setTabOrder(findButton, closeButton);
    retranslateUi(FindDialog);

    QMetaObject::connectSlotsByName(FindDialog);
    } // setupUi

    void retranslateUi(QWidget *FindDialog)
    {
    FindDialog->setWindowTitle(QApplication::translate("FindDialog", "Qt Assistant - Find Text", 0, QApplication::UnicodeUTF8));
    findButton->setText(QApplication::translate("FindDialog", "&Find", 0, QApplication::UnicodeUTF8));
    findButton->setShortcut(QApplication::translate("FindDialog", "Alt+F", 0, QApplication::UnicodeUTF8));
    closeButton->setText(QApplication::translate("FindDialog", "C&lose", 0, QApplication::UnicodeUTF8));
    closeButton->setShortcut(QApplication::translate("FindDialog", "Alt+L", 0, QApplication::UnicodeUTF8));
    ButtonGroup1->setTitle(QApplication::translate("FindDialog", "&Options", 0, QApplication::UnicodeUTF8));
    checkWords->setText(QApplication::translate("FindDialog", "&Whole words only", 0, QApplication::UnicodeUTF8));
    checkCase->setText(QApplication::translate("FindDialog", "&Case sensitive", 0, QApplication::UnicodeUTF8));
    ButtonGroup2->setTitle(QApplication::translate("FindDialog", "&Direction", 0, QApplication::UnicodeUTF8));
    radioForward->setText(QApplication::translate("FindDialog", "Fo&rward", 0, QApplication::UnicodeUTF8));
    radioBackward->setText(QApplication::translate("FindDialog", "&Backward", 0, QApplication::UnicodeUTF8));
    TextLabel1->setText(QApplication::translate("FindDialog", "F&ind:", 0, QApplication::UnicodeUTF8));
    Q_UNUSED(FindDialog);
    } // retranslateUi

};

namespace Ui {
    class FindDialog: public Ui_FindDialog {};
} // namespace Ui

#endif // UI_FINDDIALOG_H
