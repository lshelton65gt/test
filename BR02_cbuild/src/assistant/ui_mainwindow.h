/*
*********************************************************************
**
** Copyright (C) 1992-2006 Trolltech AS. All rights reserved.
**
** This file is part of the Qt Assistant of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
*********************************************************************
*/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

class Ui_AssistantMainWindow
{
public:
    QAction *actionFilePrint;
    QAction *actionFileExit;
    QAction *actionEditCopy;
    QAction *actionEditFind;
    QAction *actionEditFindAgain;
    QAction *actionEditFindAgainPrev;
    QAction *actionGoHome;
    QAction *actionGoPrevious;
    QAction *actionGoNext;
    QAction *actionAboutAssistant;
    QAction *actionAboutApplication;
    QAction *actionZoomIn;
    QAction *actionZoomOut;
    QAction *actionSettings;
    QAction *actionNewWindow;
    QAction *actionClose;
    QAction *actionAddBookmark;
    QAction *actionHelpWhatsThis;
    QAction *actionOpenPage;
    QAction *actionNextPage;
    QAction *actionPrevPage;
    QAction *actionClosePage;
    QAction *actionHelpAssistant;
    QAction *actionSaveAs;
    QWidget *__qt_central_widget;
    QToolBar *Toolbar;
    QToolBar *goActionToolbar;
    QMenuBar *menubar;
    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *viewMenu;
    QMenu *goMenu;
    QMenu *bookmarkMenu;
    QMenu *helpMenu;

    void setupUi(QMainWindow *AssistantMainWindow)
    {
    AssistantMainWindow->setObjectName(QString::fromUtf8("AssistantMainWindow"));
    AssistantMainWindow->resize(QSize(949, 670).expandedTo(AssistantMainWindow->minimumSizeHint()));
    actionFilePrint = new QAction(AssistantMainWindow);
    actionFilePrint->setObjectName(QString::fromUtf8("actionFilePrint"));
    actionFilePrint->setIcon(QIcon(QString::fromUtf8(":/trolltech/assistant/images/win/print.png")));
    actionFileExit = new QAction(AssistantMainWindow);
    actionFileExit->setObjectName(QString::fromUtf8("actionFileExit"));
    actionEditCopy = new QAction(AssistantMainWindow);
    actionEditCopy->setObjectName(QString::fromUtf8("actionEditCopy"));
    actionEditCopy->setIcon(QIcon(QString::fromUtf8(":/trolltech/assistant/images/win/editcopy.png")));
    actionEditFind = new QAction(AssistantMainWindow);
    actionEditFind->setObjectName(QString::fromUtf8("actionEditFind"));
    actionEditFind->setIcon(QIcon(QString::fromUtf8(":/trolltech/assistant/images/win/find.png")));
    actionEditFindAgain = new QAction(AssistantMainWindow);
    actionEditFindAgain->setObjectName(QString::fromUtf8("actionEditFindAgain"));
    actionEditFindAgainPrev = new QAction(AssistantMainWindow);
    actionEditFindAgainPrev->setObjectName(QString::fromUtf8("actionEditFindAgainPrev"));
    actionGoHome = new QAction(AssistantMainWindow);
    actionGoHome->setObjectName(QString::fromUtf8("actionGoHome"));
    actionGoHome->setIcon(QIcon(QString::fromUtf8(":/trolltech/assistant/images/win/home.png")));
    actionGoPrevious = new QAction(AssistantMainWindow);
    actionGoPrevious->setObjectName(QString::fromUtf8("actionGoPrevious"));
    actionGoPrevious->setIcon(QIcon(QString::fromUtf8(":/trolltech/assistant/images/win/previous.png")));
    actionGoNext = new QAction(AssistantMainWindow);
    actionGoNext->setObjectName(QString::fromUtf8("actionGoNext"));
    actionGoNext->setIcon(QIcon(QString::fromUtf8(":/trolltech/assistant/images/win/next.png")));
    actionAboutAssistant = new QAction(AssistantMainWindow);
    actionAboutAssistant->setObjectName(QString::fromUtf8("actionAboutAssistant"));
    actionAboutApplication = new QAction(AssistantMainWindow);
    actionAboutApplication->setObjectName(QString::fromUtf8("actionAboutApplication"));
    actionZoomIn = new QAction(AssistantMainWindow);
    actionZoomIn->setObjectName(QString::fromUtf8("actionZoomIn"));
    actionZoomIn->setIcon(QIcon(QString::fromUtf8(":/trolltech/assistant/images/win/zoomin.png")));
    actionZoomOut = new QAction(AssistantMainWindow);
    actionZoomOut->setObjectName(QString::fromUtf8("actionZoomOut"));
    actionZoomOut->setIcon(QIcon(QString::fromUtf8(":/trolltech/assistant/images/win/zoomout.png")));
    actionSettings = new QAction(AssistantMainWindow);
    actionSettings->setObjectName(QString::fromUtf8("actionSettings"));
    actionNewWindow = new QAction(AssistantMainWindow);
    actionNewWindow->setObjectName(QString::fromUtf8("actionNewWindow"));
    actionClose = new QAction(AssistantMainWindow);
    actionClose->setObjectName(QString::fromUtf8("actionClose"));
    actionAddBookmark = new QAction(AssistantMainWindow);
    actionAddBookmark->setObjectName(QString::fromUtf8("actionAddBookmark"));
    actionHelpWhatsThis = new QAction(AssistantMainWindow);
    actionHelpWhatsThis->setObjectName(QString::fromUtf8("actionHelpWhatsThis"));
    actionHelpWhatsThis->setIcon(QIcon(QString::fromUtf8(":/trolltech/assistant/images/win/whatsthis.png")));
    actionOpenPage = new QAction(AssistantMainWindow);
    actionOpenPage->setObjectName(QString::fromUtf8("actionOpenPage"));
    actionNextPage = new QAction(AssistantMainWindow);
    actionNextPage->setObjectName(QString::fromUtf8("actionNextPage"));
    actionPrevPage = new QAction(AssistantMainWindow);
    actionPrevPage->setObjectName(QString::fromUtf8("actionPrevPage"));
    actionClosePage = new QAction(AssistantMainWindow);
    actionClosePage->setObjectName(QString::fromUtf8("actionClosePage"));
    actionHelpAssistant = new QAction(AssistantMainWindow);
    actionHelpAssistant->setObjectName(QString::fromUtf8("actionHelpAssistant"));
    actionHelpAssistant->setIcon(QIcon(QString::fromUtf8(":/trolltech/assistant/images/assistant.png")));
    actionSaveAs = new QAction(AssistantMainWindow);
    actionSaveAs->setObjectName(QString::fromUtf8("actionSaveAs"));
    __qt_central_widget = new QWidget(AssistantMainWindow);
    __qt_central_widget->setObjectName(QString::fromUtf8("__qt_central_widget"));
    AssistantMainWindow->setCentralWidget(__qt_central_widget);
    Toolbar = new QToolBar(AssistantMainWindow);
    Toolbar->setObjectName(QString::fromUtf8("Toolbar"));
    AssistantMainWindow->addToolBar(Toolbar);
    goActionToolbar = new QToolBar(AssistantMainWindow);
    goActionToolbar->setObjectName(QString::fromUtf8("goActionToolbar"));
    AssistantMainWindow->addToolBar(goActionToolbar);
    menubar = new QMenuBar(AssistantMainWindow);
    menubar->setObjectName(QString::fromUtf8("menubar"));
    menubar->setGeometry(QRect(0, 0, 949, 29));
    fileMenu = new QMenu(menubar);
    fileMenu->setObjectName(QString::fromUtf8("fileMenu"));
    editMenu = new QMenu(menubar);
    editMenu->setObjectName(QString::fromUtf8("editMenu"));
    viewMenu = new QMenu(menubar);
    viewMenu->setObjectName(QString::fromUtf8("viewMenu"));
    goMenu = new QMenu(menubar);
    goMenu->setObjectName(QString::fromUtf8("goMenu"));
    bookmarkMenu = new QMenu(menubar);
    bookmarkMenu->setObjectName(QString::fromUtf8("bookmarkMenu"));
    helpMenu = new QMenu(menubar);
    helpMenu->setObjectName(QString::fromUtf8("helpMenu"));
    AssistantMainWindow->setMenuBar(menubar);

    Toolbar->addAction(actionGoPrevious);
    Toolbar->addAction(actionGoNext);
    Toolbar->addAction(actionGoHome);
    Toolbar->addSeparator();
    Toolbar->addAction(actionEditCopy);
    Toolbar->addAction(actionEditFind);
    Toolbar->addAction(actionFilePrint);
    Toolbar->addSeparator();
    Toolbar->addAction(actionZoomIn);
    Toolbar->addAction(actionZoomOut);
    Toolbar->addSeparator();
    Toolbar->addAction(actionHelpWhatsThis);
    menubar->addAction(fileMenu->menuAction());
    menubar->addAction(editMenu->menuAction());
    menubar->addAction(viewMenu->menuAction());
    menubar->addAction(goMenu->menuAction());
    menubar->addAction(bookmarkMenu->menuAction());
    menubar->addAction(helpMenu->menuAction());
    fileMenu->addAction(actionNewWindow);
    fileMenu->addAction(actionOpenPage);
    fileMenu->addAction(actionClosePage);
    fileMenu->addSeparator();
    fileMenu->addAction(actionSaveAs);
    fileMenu->addSeparator();
    fileMenu->addAction(actionFilePrint);
    fileMenu->addSeparator();
    fileMenu->addAction(actionClose);
    fileMenu->addAction(actionFileExit);
    editMenu->addAction(actionEditCopy);
    editMenu->addAction(actionEditFind);
    editMenu->addAction(actionEditFindAgain);
    editMenu->addAction(actionEditFindAgainPrev);
    editMenu->addSeparator();
    editMenu->addAction(actionSettings);
    viewMenu->addAction(actionZoomIn);
    viewMenu->addAction(actionZoomOut);
    goMenu->addAction(actionGoPrevious);
    goMenu->addAction(actionGoNext);
    goMenu->addAction(actionGoHome);
    goMenu->addSeparator();
    goMenu->addAction(actionNextPage);
    goMenu->addAction(actionPrevPage);
    helpMenu->addAction(actionHelpAssistant);
    helpMenu->addSeparator();
    helpMenu->addAction(actionAboutAssistant);
    helpMenu->addAction(actionAboutApplication);
    helpMenu->addSeparator();
    helpMenu->addAction(actionHelpWhatsThis);
    retranslateUi(AssistantMainWindow);

    QMetaObject::connectSlotsByName(AssistantMainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *AssistantMainWindow)
    {
    AssistantMainWindow->setWindowTitle(QApplication::translate("AssistantMainWindow", "Qt Assistant by Trolltech", 0, QApplication::UnicodeUTF8));
    actionFilePrint->setWhatsThis(QApplication::translate("AssistantMainWindow", "Print the currently displayed page.", 0, QApplication::UnicodeUTF8));
    actionFilePrint->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl+P", 0, QApplication::UnicodeUTF8));
    actionFilePrint->setText(QApplication::translate("AssistantMainWindow", "&Print...", 0, QApplication::UnicodeUTF8));
    actionFileExit->setWhatsThis(QApplication::translate("AssistantMainWindow", "Quit Qt Assistant.", 0, QApplication::UnicodeUTF8));
    actionFileExit->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl+Q", 0, QApplication::UnicodeUTF8));
    actionFileExit->setText(QApplication::translate("AssistantMainWindow", "E&xit", 0, QApplication::UnicodeUTF8));
    actionEditCopy->setWhatsThis(QApplication::translate("AssistantMainWindow", "Copy the selected text to the clipboard.", 0, QApplication::UnicodeUTF8));
    actionEditCopy->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl+C", 0, QApplication::UnicodeUTF8));
    actionEditCopy->setText(QApplication::translate("AssistantMainWindow", "&Copy", 0, QApplication::UnicodeUTF8));
    actionEditFind->setWhatsThis(QApplication::translate("AssistantMainWindow", "Open the Find dialog. Qt Assistant will search the currently displayed page for the text you enter.", 0, QApplication::UnicodeUTF8));
    actionEditFind->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl+F", 0, QApplication::UnicodeUTF8));
    actionEditFind->setText(QApplication::translate("AssistantMainWindow", "&Find in Text...", 0, QApplication::UnicodeUTF8));
    actionEditFindAgain->setShortcut(QApplication::translate("AssistantMainWindow", "F3", 0, QApplication::UnicodeUTF8));
    actionEditFindAgain->setText(QApplication::translate("AssistantMainWindow", "Find &Next", 0, QApplication::UnicodeUTF8));
    actionEditFindAgainPrev->setShortcut(QApplication::translate("AssistantMainWindow", "Shift+F3", 0, QApplication::UnicodeUTF8));
    actionEditFindAgainPrev->setText(QApplication::translate("AssistantMainWindow", "Find &Previous", 0, QApplication::UnicodeUTF8));
    actionGoHome->setWhatsThis(QApplication::translate("AssistantMainWindow", "Go to the home page. Qt Assistant's home page is the Qt Reference Documentation.", 0, QApplication::UnicodeUTF8));
    actionGoHome->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl+Home", 0, QApplication::UnicodeUTF8));
    actionGoHome->setText(QApplication::translate("AssistantMainWindow", "&Home", 0, QApplication::UnicodeUTF8));
    actionGoPrevious->setWhatsThis(QApplication::translate("AssistantMainWindow", "Go to the previous page.", 0, QApplication::UnicodeUTF8));
    actionGoPrevious->setShortcut(QApplication::translate("AssistantMainWindow", "Alt+Left", 0, QApplication::UnicodeUTF8));
    actionGoPrevious->setText(QApplication::translate("AssistantMainWindow", "&Previous", 0, QApplication::UnicodeUTF8));
    actionGoNext->setWhatsThis(QApplication::translate("AssistantMainWindow", "Go to the next page.", 0, QApplication::UnicodeUTF8));
    actionGoNext->setShortcut(QApplication::translate("AssistantMainWindow", "Alt+Right", 0, QApplication::UnicodeUTF8));
    actionGoNext->setText(QApplication::translate("AssistantMainWindow", "&Next", 0, QApplication::UnicodeUTF8));
    actionAboutAssistant->setWhatsThis(QApplication::translate("AssistantMainWindow", "Display further information about Qt Assistant.", 0, QApplication::UnicodeUTF8));
    actionAboutAssistant->setText(QApplication::translate("AssistantMainWindow", "About Qt Assistant", 0, QApplication::UnicodeUTF8));
    actionAboutApplication->setText(QApplication::translate("AssistantMainWindow", "About Qt", 0, QApplication::UnicodeUTF8));
    actionZoomIn->setWhatsThis(QApplication::translate("AssistantMainWindow", "Zoom in on the document, i.e. increase the font size.", 0, QApplication::UnicodeUTF8));
    actionZoomIn->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl++", 0, QApplication::UnicodeUTF8));
    actionZoomIn->setText(QApplication::translate("AssistantMainWindow", "Zoom &in", 0, QApplication::UnicodeUTF8));
    actionZoomOut->setWhatsThis(QApplication::translate("AssistantMainWindow", "Zoom out on the document, i.e. decrease the font size.", 0, QApplication::UnicodeUTF8));
    actionZoomOut->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl+-", 0, QApplication::UnicodeUTF8));
    actionZoomOut->setText(QApplication::translate("AssistantMainWindow", "Zoom &out", 0, QApplication::UnicodeUTF8));
    actionSettings->setWhatsThis(QApplication::translate("AssistantMainWindow", "Open the settings dialog.", 0, QApplication::UnicodeUTF8));
    actionSettings->setText(QApplication::translate("AssistantMainWindow", "&Settings...", 0, QApplication::UnicodeUTF8));
    actionNewWindow->setWhatsThis(QApplication::translate("AssistantMainWindow", "Open a new window.", 0, QApplication::UnicodeUTF8));
    actionNewWindow->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl+N", 0, QApplication::UnicodeUTF8));
    actionNewWindow->setText(QApplication::translate("AssistantMainWindow", "New Window", 0, QApplication::UnicodeUTF8));
    actionClose->setWhatsThis(QApplication::translate("AssistantMainWindow", "Close the current window.", 0, QApplication::UnicodeUTF8));
    actionClose->setText(QApplication::translate("AssistantMainWindow", "&Close", 0, QApplication::UnicodeUTF8));
    actionAddBookmark->setWhatsThis(QApplication::translate("AssistantMainWindow", "Add the currently displayed page as a new bookmark.", 0, QApplication::UnicodeUTF8));
    actionAddBookmark->setText(QApplication::translate("AssistantMainWindow", "&Add Bookmark", 0, QApplication::UnicodeUTF8));
    actionHelpWhatsThis->setStatusTip(QApplication::translate("AssistantMainWindow", "\"What's This?\" context sensitive help.", 0, QApplication::UnicodeUTF8));
    actionHelpWhatsThis->setWhatsThis(QApplication::translate("AssistantMainWindow", "\"What's This?\" context sensitive help.", 0, QApplication::UnicodeUTF8));
    actionHelpWhatsThis->setShortcut(QApplication::translate("AssistantMainWindow", "Shift+F1", 0, QApplication::UnicodeUTF8));
    actionHelpWhatsThis->setText(QApplication::translate("AssistantMainWindow", "What's This?", 0, QApplication::UnicodeUTF8));
    actionOpenPage->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl+Alt+N", 0, QApplication::UnicodeUTF8));
    actionOpenPage->setText(QApplication::translate("AssistantMainWindow", "Add Tab", 0, QApplication::UnicodeUTF8));
    actionNextPage->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl+Alt+Right", 0, QApplication::UnicodeUTF8));
    actionNextPage->setText(QApplication::translate("AssistantMainWindow", "Next Tab", 0, QApplication::UnicodeUTF8));
    actionPrevPage->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl+Alt+Left", 0, QApplication::UnicodeUTF8));
    actionPrevPage->setText(QApplication::translate("AssistantMainWindow", "Previous Tab", 0, QApplication::UnicodeUTF8));
    actionClosePage->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl+Alt+Q", 0, QApplication::UnicodeUTF8));
    actionClosePage->setText(QApplication::translate("AssistantMainWindow", "Close Tab", 0, QApplication::UnicodeUTF8));
    actionHelpAssistant->setShortcut(QApplication::translate("AssistantMainWindow", "F1", 0, QApplication::UnicodeUTF8));
    actionHelpAssistant->setText(QApplication::translate("AssistantMainWindow", "Qt Assistant Manual", 0, QApplication::UnicodeUTF8));
    actionSaveAs->setShortcut(QApplication::translate("AssistantMainWindow", "Ctrl+Alt+S", 0, QApplication::UnicodeUTF8));
    actionSaveAs->setText(QApplication::translate("AssistantMainWindow", "Save Page As...", 0, QApplication::UnicodeUTF8));
    Toolbar->setWindowTitle(QApplication::translate("AssistantMainWindow", "Toolbar", 0, QApplication::UnicodeUTF8));
    goActionToolbar->setWindowTitle(QApplication::translate("AssistantMainWindow", "Go", 0, QApplication::UnicodeUTF8));
    fileMenu->setTitle(QApplication::translate("AssistantMainWindow", "&File", 0, QApplication::UnicodeUTF8));
    editMenu->setTitle(QApplication::translate("AssistantMainWindow", "&Edit", 0, QApplication::UnicodeUTF8));
    viewMenu->setTitle(QApplication::translate("AssistantMainWindow", "&View", 0, QApplication::UnicodeUTF8));
    goMenu->setTitle(QApplication::translate("AssistantMainWindow", "&Go", 0, QApplication::UnicodeUTF8));
    bookmarkMenu->setTitle(QApplication::translate("AssistantMainWindow", "Boo&kmarks", 0, QApplication::UnicodeUTF8));
    helpMenu->setTitle(QApplication::translate("AssistantMainWindow", "&Help", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AssistantMainWindow: public Ui_AssistantMainWindow {};
} // namespace Ui

#endif // UI_MAINWINDOW_H
