/*
*********************************************************************
**
** Copyright (C) 1992-2006 Trolltech AS. All rights reserved.
**
** This file is part of the Qt Assistant of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
*********************************************************************
*/

#ifndef UI_SETTINGSDIALOG_H
#define UI_SETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

class Ui_SettingsDialog
{
public:
    QVBoxLayout *vboxLayout;
    QTabWidget *settingsTab;
    QWidget *tab;
    QVBoxLayout *vboxLayout1;
    QLabel *TextLabel1_3;
    QHBoxLayout *hboxLayout;
    QLineEdit *browserApp;
    QPushButton *buttonBrowse;
    QLabel *TextLabel1_3_3;
    QHBoxLayout *hboxLayout1;
    QLineEdit *homePage;
    QPushButton *buttonHome;
    QSpacerItem *spacerItem;
    QWidget *pdfTab;
    QVBoxLayout *vboxLayout2;
    QLabel *TextLabel1_3_2;
    QHBoxLayout *hboxLayout2;
    QLineEdit *pdfApp;
    QPushButton *buttonPDF;
    QSpacerItem *spacerItem1;
    QHBoxLayout *hboxLayout3;
    QSpacerItem *spacerItem2;
    QPushButton *buttonOk;
    QPushButton *buttonCancel;

    void setupUi(QDialog *SettingsDialog)
    {
    SettingsDialog->setObjectName(QString::fromUtf8("SettingsDialog"));
    SettingsDialog->resize(QSize(404, 228).expandedTo(SettingsDialog->minimumSizeHint()));
    SettingsDialog->setSizeGripEnabled(true);
    vboxLayout = new QVBoxLayout(SettingsDialog);
    vboxLayout->setSpacing(6);
    vboxLayout->setMargin(9);
    vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
    settingsTab = new QTabWidget(SettingsDialog);
    settingsTab->setObjectName(QString::fromUtf8("settingsTab"));
    QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(5));
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(settingsTab->sizePolicy().hasHeightForWidth());
    settingsTab->setSizePolicy(sizePolicy);
    tab = new QWidget();
    tab->setObjectName(QString::fromUtf8("tab"));
    vboxLayout1 = new QVBoxLayout(tab);
    vboxLayout1->setSpacing(6);
    vboxLayout1->setMargin(9);
    vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
    TextLabel1_3 = new QLabel(tab);
    TextLabel1_3->setObjectName(QString::fromUtf8("TextLabel1_3"));

    vboxLayout1->addWidget(TextLabel1_3);

    hboxLayout = new QHBoxLayout();
    hboxLayout->setSpacing(6);
    hboxLayout->setMargin(1);
    hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
    browserApp = new QLineEdit(tab);
    browserApp->setObjectName(QString::fromUtf8("browserApp"));

    hboxLayout->addWidget(browserApp);

    buttonBrowse = new QPushButton(tab);
    buttonBrowse->setObjectName(QString::fromUtf8("buttonBrowse"));
    QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(1), static_cast<QSizePolicy::Policy>(0));
    sizePolicy1.setHorizontalStretch(0);
    sizePolicy1.setVerticalStretch(0);
    sizePolicy1.setHeightForWidth(buttonBrowse->sizePolicy().hasHeightForWidth());
    buttonBrowse->setSizePolicy(sizePolicy1);
    buttonBrowse->setMaximumSize(QSize(30, 32767));

    hboxLayout->addWidget(buttonBrowse);


    vboxLayout1->addLayout(hboxLayout);

    TextLabel1_3_3 = new QLabel(tab);
    TextLabel1_3_3->setObjectName(QString::fromUtf8("TextLabel1_3_3"));

    vboxLayout1->addWidget(TextLabel1_3_3);

    hboxLayout1 = new QHBoxLayout();
    hboxLayout1->setSpacing(6);
    hboxLayout1->setMargin(1);
    hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
    homePage = new QLineEdit(tab);
    homePage->setObjectName(QString::fromUtf8("homePage"));

    hboxLayout1->addWidget(homePage);

    buttonHome = new QPushButton(tab);
    buttonHome->setObjectName(QString::fromUtf8("buttonHome"));
    QSizePolicy sizePolicy2(static_cast<QSizePolicy::Policy>(1), static_cast<QSizePolicy::Policy>(0));
    sizePolicy2.setHorizontalStretch(0);
    sizePolicy2.setVerticalStretch(0);
    sizePolicy2.setHeightForWidth(buttonHome->sizePolicy().hasHeightForWidth());
    buttonHome->setSizePolicy(sizePolicy2);
    buttonHome->setMaximumSize(QSize(30, 32767));

    hboxLayout1->addWidget(buttonHome);


    vboxLayout1->addLayout(hboxLayout1);

    spacerItem = new QSpacerItem(20, 30, QSizePolicy::Minimum, QSizePolicy::Expanding);

    vboxLayout1->addItem(spacerItem);

    settingsTab->addTab(tab, QApplication::translate("SettingsDialog", "&Web", 0, QApplication::UnicodeUTF8));
    pdfTab = new QWidget();
    pdfTab->setObjectName(QString::fromUtf8("pdfTab"));
    vboxLayout2 = new QVBoxLayout(pdfTab);
    vboxLayout2->setSpacing(6);
    vboxLayout2->setMargin(9);
    vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
    TextLabel1_3_2 = new QLabel(pdfTab);
    TextLabel1_3_2->setObjectName(QString::fromUtf8("TextLabel1_3_2"));

    vboxLayout2->addWidget(TextLabel1_3_2);

    hboxLayout2 = new QHBoxLayout();
    hboxLayout2->setSpacing(6);
    hboxLayout2->setMargin(1);
    hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
    pdfApp = new QLineEdit(pdfTab);
    pdfApp->setObjectName(QString::fromUtf8("pdfApp"));

    hboxLayout2->addWidget(pdfApp);

    buttonPDF = new QPushButton(pdfTab);
    buttonPDF->setObjectName(QString::fromUtf8("buttonPDF"));
    QSizePolicy sizePolicy3(static_cast<QSizePolicy::Policy>(1), static_cast<QSizePolicy::Policy>(0));
    sizePolicy3.setHorizontalStretch(0);
    sizePolicy3.setVerticalStretch(0);
    sizePolicy3.setHeightForWidth(buttonPDF->sizePolicy().hasHeightForWidth());
    buttonPDF->setSizePolicy(sizePolicy3);
    buttonPDF->setMaximumSize(QSize(30, 32767));

    hboxLayout2->addWidget(buttonPDF);


    vboxLayout2->addLayout(hboxLayout2);

    spacerItem1 = new QSpacerItem(20, 81, QSizePolicy::Minimum, QSizePolicy::Expanding);

    vboxLayout2->addItem(spacerItem1);

    settingsTab->addTab(pdfTab, QApplication::translate("SettingsDialog", "&PDF", 0, QApplication::UnicodeUTF8));

    vboxLayout->addWidget(settingsTab);

    hboxLayout3 = new QHBoxLayout();
    hboxLayout3->setSpacing(6);
    hboxLayout3->setMargin(1);
    hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
    spacerItem2 = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    hboxLayout3->addItem(spacerItem2);

    buttonOk = new QPushButton(SettingsDialog);
    buttonOk->setObjectName(QString::fromUtf8("buttonOk"));
    buttonOk->setAutoDefault(true);
    buttonOk->setDefault(true);

    hboxLayout3->addWidget(buttonOk);

    buttonCancel = new QPushButton(SettingsDialog);
    buttonCancel->setObjectName(QString::fromUtf8("buttonCancel"));
    buttonCancel->setAutoDefault(true);

    hboxLayout3->addWidget(buttonCancel);


    vboxLayout->addLayout(hboxLayout3);

    TextLabel1_3->setBuddy(browserApp);
    TextLabel1_3_3->setBuddy(homePage);
    TextLabel1_3_2->setBuddy(pdfApp);
    QWidget::setTabOrder(settingsTab, browserApp);
    QWidget::setTabOrder(browserApp, buttonBrowse);
    QWidget::setTabOrder(buttonBrowse, homePage);
    QWidget::setTabOrder(homePage, buttonHome);
    QWidget::setTabOrder(buttonHome, pdfApp);
    QWidget::setTabOrder(pdfApp, buttonPDF);
    QWidget::setTabOrder(buttonPDF, buttonOk);
    QWidget::setTabOrder(buttonOk, buttonCancel);
    retranslateUi(SettingsDialog);

    QMetaObject::connectSlotsByName(SettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *SettingsDialog)
    {
    SettingsDialog->setWindowTitle(QApplication::translate("SettingsDialog", "Qt Assistant - Settings", 0, QApplication::UnicodeUTF8));
    TextLabel1_3->setText(QApplication::translate("SettingsDialog", "Web &Browser Application:", 0, QApplication::UnicodeUTF8));
    buttonBrowse->setText(QApplication::translate("SettingsDialog", "...", 0, QApplication::UnicodeUTF8));
    TextLabel1_3_3->setText(QApplication::translate("SettingsDialog", "&Home page", 0, QApplication::UnicodeUTF8));
    buttonHome->setText(QApplication::translate("SettingsDialog", "...", 0, QApplication::UnicodeUTF8));
    settingsTab->setTabText(settingsTab->indexOf(tab), QApplication::translate("SettingsDialog", "&Web", 0, QApplication::UnicodeUTF8));
    TextLabel1_3_2->setText(QApplication::translate("SettingsDialog", "PDF &Application", 0, QApplication::UnicodeUTF8));
    buttonPDF->setText(QApplication::translate("SettingsDialog", "...", 0, QApplication::UnicodeUTF8));
    settingsTab->setTabText(settingsTab->indexOf(pdfTab), QApplication::translate("SettingsDialog", "&PDF", 0, QApplication::UnicodeUTF8));
    buttonOk->setText(QApplication::translate("SettingsDialog", "&OK", 0, QApplication::UnicodeUTF8));
    buttonCancel->setText(QApplication::translate("SettingsDialog", "&Cancel", 0, QApplication::UnicodeUTF8));
    Q_UNUSED(SettingsDialog);
    } // retranslateUi

};

namespace Ui {
    class SettingsDialog: public Ui_SettingsDialog {};
} // namespace Ui

#endif // UI_SETTINGSDIALOG_H
