include($${OBJROOT}/qmakevars.pri)

# Create a Visual Studio project file on Windows.  Also
# override Qt file locations.  Qmake queries the Qt
# installation for these locations, but they're probably
# not valid, since they reflect whatever path and drive
# letters were in place at the time of installation.
win32 {
  TEMPLATE = vclib
  QMAKE_INCDIR_QT = $$(QTDIR)/include
  QMAKE_LIBDIR_QT = $$(QTDIR)/lib
  QMAKE_MOC = $$(QTDIR)/bin/moc.exe
  QMAKE_UIC = $$(QTDIR)/bin/uic.exe
  QMAKE_RCC = $$(QTDIR)/bin/rcc.exe
}
unix {
  TEMPLATE = lib
}
TARGET = assistant
DESTDIR = $${OBJROOT}/assistant
QT += xml webkit script
CONFIG += $${BUILD_TYPE} uitools staticlib

DEFINES += QT_DLL \
           QT_XML_LIB \
           QT_EDITION=QT_EDITION_DESKTOP \
           _CRT_SECURE_NO_DEPRECATE \
           _HAS_ITERATOR_DEBUGGING=0 \
           QT_SCRIPT_LIB \
           QSCINTILLA_DLL \
           QT_NETWORK_LIB \
           LIBXML_STATIC \
           CARBON_NO_REGISTRATION \
           CARBON_ASSISTANT=1 \
           Profile=AssistantProfile \
           QT_EDITION=0x3ff

INCLUDEPATH += . \
               $${SANDBOX}/src/inc \
               $$(CARBON_HOME)/include

DEPENDPATH += .
MOC_DIR += $${OBJROOT}/assistant/debug
OBJECTS_DIR += $${OBJROOT}/assistant/debug
UI_DIR += $${OBJROOT}/assistant
RCC_DIR += $${OBJROOT}/assistant

FORMS += finddialog.ui \
        helpdialog.ui \
        mainwindow.ui \
        settingsdialog.ui \
        tabbedbrowser.ui \
        topicchooser.ui

SOURCES += \
        helpwindow.cxx \
        topicchooser.cxx \
        docuparser.cxx \
        settingsdialog.cxx \
        index.cxx \
        profile.cxx \
        config.cxx \
        finddialog.cxx \
        helpdialog.cxx \
        tabbedbrowser.cxx \
        amainwindow.cxx \
        Assistant.cxx

HEADERS        += helpwindow.h \
        topicchooser.h \
        docuparser.h \
        settingsdialog.h \
        index.h \
        profile.h \
        finddialog.h \
        helpdialog.h \
        tabbedbrowser.h \
        config.h \
        amainwindow.h
