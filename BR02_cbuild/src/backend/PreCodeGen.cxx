// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Lower concats and other statements that codegen doesn't want to have to deal with.
*/

#include "PreCodeGen.h"

#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUTF.h"

#include "util/CbuildMsgContext.h"
#include "util/DynBitVector.h"

#include "codegen/UnrollConcat.h"
#include "localflow/UD.h"




PreCodeGen::PreCodeGen (ArgProc* arg, AtomicCache* atomic, IODBNucleus* iodb, NUNetRefFactory* factory,
                        MsgContext * context, bool verbose)
  : CGLower (arg, atomic, iodb, factory, context, verbose)
{
}

PreCodeGen::~PreCodeGen () {}

// dump debugging info
static void sDump (NUBase* newExpr)
{
  UtIO::cout () << "CodeGen Lowered to:" << UtIO::endl;
  newExpr->print (0,2);
  UtIO::cout () << UtIO::endl;
}

// Helper function to construct a simple assignment that's well-formed.
static NUBlockingAssign*
sGenAssign (NULvalue *dest, NUExpr *src, const SourceLocator& loc)
{
  // since we are in code generation then the usesCFNet is not important, use false
  NUBlockingAssign *asn = new NUBlockingAssign (dest, src, false, loc);
  asn->resize ();

  return asn;
}

NUStmt*
PreCodeGen::concatAssign (const NUAssign* stmt)
{
  // Generate a block to hold the expanded statements
  NUBlock *block = new NUBlock (NULL, mScopeStack.top (),
                                NULL, mFactory,
                                false,  // in code generation false is good enough for usesCFNet
                                stmt->getLoc ());

  CopyContext cc (block, mAtomicCache);

  // We need to materialize a temporary for the RHS if it's an expression.
  // That's true if it's not an identifier or it's not a constant.
  //
  const NUExpr* rval = stmt->getRvalue ();
  size_t srcSize = rval->getBitSize ();
  NUExpr* src = NULL;
  bool needTemp = !(rval->isWholeIdentifier()
		    || rval->castConst());
  if (needTemp)
  {
    StringAtom* tempSym = block->gensym ("concat$", NULL, rval);
    NUNet* tempNet = block->createTempNet (tempSym, srcSize, 
                                           rval->isSignedResult(),
                                           block->getLoc ());

    block->addStmt (sGenAssign (new NUIdentLvalue (tempNet, block->getLoc ()),
                                rval->copy (cc), block->getLoc ()));

    src = new NUIdentRvalue (tempNet, block->getLoc ());
  } else {
    src = rval->copy (cc);
  }

  // Unroll the operands
  typedef std::pair<const NULvalue *, size_t> ConcatShift_t;
  UtVector<ConcatShift_t> shiftTable;

  const NUConcatLvalue* lcat = dynamic_cast<const NUConcatLvalue*>(stmt->getLvalue ());
  NU_ASSERT (lcat, stmt);
  unrollConcat<NUConcatLvalue, CRepeatLHS, ConcatShift_t>(*lcat, &shiftTable, 0, mContext);

  size_t netShift = 0;          // Initial bit-offset into RHS

  for(UtVector<ConcatShift_t>::iterator i = shiftTable.begin ();
      i != shiftTable.end ();
      ++i)
  {
    netShift += (*i).second;
    size_t lsize = (*i).first->getBitSize ();
      
    ConstantRange r (lsize + netShift -1, netShift);
    NULvalue *lval = (*i).first->copy (cc);
    NUExpr *rval = new NUVarselRvalue (src->copy (cc), r, block->getLoc ());

    block->addStmt (sGenAssign (lval, rval, block->getLoc ()));
  }

  delete src;                 // Had an extra copy to start with....

  return block;
}


NUStmt * PreCodeGen::varselConcatAssign(const NUAssign * stmt)
{
  // Generate a block to hold the expanded statements
  NUBlock *block = new NUBlock (NULL, mScopeStack.top (),
                                NULL, mFactory,
                                false,  // in code generation false is good enough for usesCFNet
                                stmt->getLoc ());

  CopyContext cc (block, mAtomicCache);

  NULvalue* lhs = stmt->getLvalue ();
  NUVarselLvalue * lhs_varsel = dynamic_cast<NUVarselLvalue*>(lhs);
  NULvalue * lhs_ident = lhs_varsel->getLvalue();
  NUConcatLvalue * lhs_concat = dynamic_cast<NUConcatLvalue*>(lhs_ident);

  // Convert:
  //    {a,b,c}[i] = 1'b1;
  // Into:
  //    s1: temp = {a,b,c}
  //    s2: temp[i] = 1'b1;
  //    s3: {a,b,c} = temp;
  UInt32 tmp_size = lhs_concat->getBitSize();
  StringAtom* tempSym = block->gensym ("concat$", NULL, lhs_concat);
  NUNet* tempNet = block->createTempNet (tempSym, tmp_size,
                                         false,
                                         block->getLoc ());

  block->addStmt(sGenAssign(new NUIdentLvalue(tempNet, block->getLoc()),
                            lhs_concat->NURvalue(), 
                            block->getLoc()));
  block->addStmt(sGenAssign(new NUVarselLvalue(tempNet, lhs_varsel->getIndex()->copy(cc), 
                                               *lhs_varsel->getRange(), block->getLoc()),
                            stmt->getRvalue()->copy(cc), 
                            block->getLoc()));
  block->addStmt(sGenAssign(lhs_concat->copy(cc),
                            new NUIdentRvalue(tempNet, block->getLoc()),
                            block->getLoc()));
  return block;
}


NUDesignCallback::Status PreCodeGen::operator() (NUDesignCallback::Phase, NUStmt*)
{
  return eNormal;
}

NUDesignCallback::Status PreCodeGen::operator() (NUDesignCallback::Phase, NUEnabledDriver*)
{
  return eSkip;
}

NUDesignCallback::Status PreCodeGen::operator() (NUDesignCallback::Phase, NUTriRegInit*)
{
  return eSkip;
}

NUDesignCallback::Status PreCodeGen::operator() (NUDesignCallback::Phase, NUContAssign* cassign)
{
  switch(qualifyLHSConcat(static_cast<NUAssign*>(cassign))) {
  case eConcatAssign:       return eDelete; break;
  case eVarselConcatAssign: return eDelete; break;
  case eUnhandledAssign:    return eSkip;   break;
  }
  return eSkip;
  // TBD: Why do we use different qualification paths for continuous
  // assignments and blocking assignments?
}

NUDesignCallback::Status PreCodeGen::operator() (NUDesignCallback::Phase phase, NUBlockingAssign* stmt)
{
  return this->operator()(phase,static_cast<NUAssign*>(stmt));
}


PreCodeGen::ConcatAssignType PreCodeGen::qualifyLHSConcat(const NUAssign * assign) const
{
  NULvalue * lhs = assign->getLvalue ();
  if (lhs->getType () == eNUConcatLvalue) {
    return eConcatAssign;       // handled by concatAssign
  } else if (lhs->getType() == eNUVarselLvalue) {
    NUVarselLvalue * lhs_varsel = dynamic_cast<NUVarselLvalue*>(lhs);
    NULvalue * lhs_ident = lhs_varsel->getLvalue();
    if (lhs_ident->getType() == eNUConcatLvalue) {
      return eVarselConcatAssign; // handled by varselConcatAssign
    }
  }
  return eUnhandledAssign;
}


/*!
 * Recognize simple concatenations on the RHS that are candidates for
 * special case optimizations.  Specifically look for things like
 *
 *	A = {B,C,D}
 * where B,C,D don't involve A.
 */

NUDesignCallback::Status PreCodeGen::operator() (Phase phase, NUAssign *stmt)
{
  if (phase != ePre)
    return eSkip;

  switch(qualifyLHSConcat(stmt)) {
  case eConcatAssign:       return eDelete; break;
  case eVarselConcatAssign: return eDelete; break;
  case eUnhandledAssign:    /*fallthrough*/ break;
  }

  const NUExpr * rhs = stmt->getRvalue ();

  // If the expression is small, we can compute it directly using
  // shifts and masks and that's more efficient that assigning pieces.
  if (rhs->getBitSize () <= 64)
    return eSkip;

  // Check that RHS is a concat
  const NUConcatOp *rop = dynamic_cast<const NUConcatOp*>(rhs);
  if (rop == NULL)
    return eSkip;

  // Can split into simple assignments if the destination storage doesn't
  // overlap the source expressions values.
  //
  if (not stmt->overlaps (mFactory, true)) // Don't allow 1:1 overlaps
    return eDelete;

  return eSkip;
}

// Generate the complete assignment
NUContAssign* PreCodeGen::replacement (NUContAssign *stmt)
{
  // Lower the {a,b} = expr into a block of assignments.
  NUStmt* repl = replacement (static_cast<NUAssign*>(stmt));
  
  // Convert that block to an always block replacing the continuous assignment
  NUBlock* block = dynamic_cast<NUBlock*>(repl);
  makeAlwaysBlock (stmt, block);

  return 0;                     // "He's dead, Jim...."
}

NUStmt* PreCodeGen::replacement (NUStmt *aStmt)
{

  NUAssign *stmt = dynamic_cast<NUAssign*>(aStmt);
  
  NU_ASSERT (stmt, aStmt);      // Only handling assignments now!

  mChanged = true;              // We're guaranteed to do something here.


  NULvalue* lhs = stmt->getLvalue ();

  NUStmt * newStmt = NULL;
  switch(qualifyLHSConcat(stmt)) {
  case eConcatAssign:       newStmt = concatAssign (stmt); break;
  case eVarselConcatAssign: newStmt = varselConcatAssign(stmt); break;
  case eUnhandledAssign:    /*fallthrough*/ break;
  }
  if (newStmt) {
    if (mVerbose) {
      sDump (newStmt);
    }
    return newStmt;
  }

  const NUConcatOp* rhs = dynamic_cast<const NUConcatOp*>(stmt->getRvalue());

  // Unroll the operands
  typedef std::pair<const NUExpr *, size_t> ConcatShift_t;
  UtVector<ConcatShift_t> shiftTable;

  unrollConcat<NUConcatOp, CRepeatRHS, ConcatShift_t>(*rhs, &shiftTable, 0, mContext);

  ConstantRange base (lhs->getBitSize ()-1, 0);

  // LHS can be a partselect, memsel or simple variable.
  switch (lhs->getType ()) {
  case eNUVarselLvalue:
  case eNUIdentLvalue:
  case eNUMemselLvalue:
    break;
  default:
      NU_ASSERT ("Bad Lvalue lowering" == 0, stmt);
  }

  CopyContext copy_context(NULL,NULL);

  // Construct a block scope and a sequence of individual assignments
  //
  NUBlock* block = new NUBlock (NULL, mScopeStack.top (), NULL, mFactory, false, stmt->getLoc ());

  UtVector<ConcatShift_t>::iterator i = shiftTable.begin ();
  for (UInt32 totSize = lhs->getBitSize (), pieceSize;
       totSize > 0;
       totSize -= pieceSize, ++i) {

    NUExpr *exp;

    if (i != shiftTable.end ()) {
      exp = (*i).first->copy (copy_context);
    } else {
      // Didn't init all the object...
      exp = NUConst::create (false, 0,  totSize, lhs->getLoc ());
    }

    pieceSize = std::min (exp->getBitSize (), totSize);

    base.setMsb (base.getLsb () + pieceSize - 1);

    NUVarselLvalue *dest = new NUVarselLvalue (lhs->copy (copy_context), base,
                                                 lhs->getLoc ());

    base.adjust (pieceSize);    // advance to where next chunk is placed.

    NUStmt *partAssign
      = new NUBlockingAssign (dest, exp, false, lhs->getLoc ());

    // Add this assignment to the block.
    block->addStmt (partAssign);
  }

  if (mVerbose)
    sDump (block);
  return block;
}

