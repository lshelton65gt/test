// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "LocalCSE.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUCopyContext.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUTF.h"
#include "localflow/UD.h"
#include "util/StringAtom.h"
#include "util/Loop.h"
#include "util/UtMap.h"
#include "util/UtSet.h"
#include "iodb/IODBNucleus.h"


//! A common subexpression
/*!
 * This is very simple for now, we only handle simple fetches.
 *
 * In the future, probably want to use the Expr stuff in Exprsynth to handle general
 * expressions.
 */
class CSE
{
public:
  CSE(NUExpr *expr) : mRefs(0), mExpr(expr)
  {
    sanityExpr(mExpr);
  }

  ~CSE()
  {
    delete mExpr;
  }

  void sanityExpr(NUExpr *expr) const
  {
    // NOTE: this is not a generalized CSE package and is not intended to be.
    // This was developed to only handle simple identifier CSE's and simple
    // unary operations of those.
    // As such, these asserts are making sure that this is not misused.
    // If one of these triggers, do not remove the assert.  Instead, fix
    // the qualify() routine.  This code is independent of the qualify() routine
    // to prevent misuse, even though the functionality overlaps somewhat.
    switch (expr->getType()) {
    case NUExpr::eNUUnaryOp:
      {
        NUUnaryOp *op = dynamic_cast<NUUnaryOp*>(expr);
        sanityExpr(op->getArg(0));
      }
      break;
    default:
      sanityFetch(expr);
      break;
    }
  }

  static void sanityFetch(NUExpr* expr)
  {
    // NOTE: this is not a generalized CSE package and is not intended to be.
    // This was developed to only handle simple identifier CSE's and simple
    // unary operations of those.
    // As such, these asserts are making sure that this is not misused.
    // If one of these triggers, do not remove the assert.  Instead, fix
    // the qualify() routine.  This code is independent of the qualify() routine
    // to prevent misuse, even though the functionality overlaps somewhat.
    //
    // Check #1 - make sure some sort of fetch node.
    NUExpr::Type type = expr->getType();
    NU_ASSERT(type == NUExpr::eNUVarselRvalue ||
               type == NUExpr::eNUMemselRvalue ||
              type == NUExpr::eNUIdentRvalue,
              expr);

    // Check #2 - for the bit selects, make sure it selects a simple identifier,
    // and a constant index.
    switch (type) {
    case NUExpr::eNUVarselRvalue:
      {
        NUVarselRvalue *varsel = dynamic_cast<NUVarselRvalue*>(expr);
        NU_ASSERT(varsel->getIdentExpr()->getType() == NUExpr::eNUIdentRvalue, expr);
        NU_ASSERT(varsel->getIndex()->getType() == NUExpr::eNUConstXZ ||
                  varsel->getIndex()->getType() == NUExpr::eNUConstNoXZ, expr);
      }
      break;
    case NUExpr::eNUMemselRvalue:
      {
        NUMemselRvalue *memsel = dynamic_cast<NUMemselRvalue*>(expr);
        NU_ASSERT(memsel->getIdentExpr()->getType() == NUExpr::eNUIdentRvalue, expr);
        NU_ASSERT(memsel->getIndex(0)->getType() == NUExpr::eNUConstXZ ||
                  memsel->getIndex(0)->getType() == NUExpr::eNUConstNoXZ, expr);
      }
      break;
    default:
      break;
    }
  }

  //! Bit size of the expression
  UInt32 getBitSize() const
  {
    return mExpr->getBitSize();
  }

  //! Bump ref count
  void incRef(UInt32 cnt)
  {
    mRefs += cnt;
  }

  //! Return number of references to this common expression
  UInt32 numRefs() const
  {
    return mRefs;
  }

  //! Return a version of this expression which is suitable for insertion into a tree.
  NUExpr* createExpr()
  {
    CopyContext ctx(0,0);
    return mExpr->copy(ctx);
  }

  //! Return the internal expression.  This is not suitable for insertion.
  const NUExpr* getExpr() const
  {
    return mExpr;
  }

  //! Return the estimated cost of one evaluation of this expression
  UInt32 cost() const
  {
    NUCostContext ctx;
    NUCost cost;
    mExpr->calcCost(&cost, &ctx);
    return cost.mInstructions;
  }

  //! Return true if this CSE can be used in place of the given expr
  bool equivalent(NUExpr *expr) const
  {
    return (*expr == *mExpr);
  }

  //! Is this an expression we want to CSE?
  /*!
   * Not CSE-ing non-static (ie, stacklocal) nets, because the cost to fetch them is very small.
   */
  static bool qualify(NUExpr *expr )
  {
    NUNet *ident = 0;

    switch (expr->getType()) {
      case NUExpr::eNUIdentRvalue:
        ident = (dynamic_cast<NUIdentRvalue*>(expr))->getIdent();

        if (ident->isNonStatic())
          //Only CSE static class members if not part select.
          return false;
        
        if (ident->is2DAnything()) {
          // Do not allow full memories to be copied.
          return false;
        }        
        break;

      case NUExpr::eNUVarselRvalue:
        {
          // Only CSE constant indices.
          NUVarselRvalue *varsel = dynamic_cast<NUVarselRvalue*>(expr);
          if ((varsel->getIndex()->getType() != NUExpr::eNUConstXZ) &&
              (varsel->getIndex()->getType() != NUExpr::eNUConstNoXZ)) {
            return false;
          }

          // Only CSE identifier selects.
          if (varsel->getIdentExpr()->getType() != NUExpr::eNUIdentRvalue) {
            return false;
          }
        }
        break;

      case NUExpr::eNUMemselRvalue:
        {
          // Only CSE constant indices.
          NUMemselRvalue *memsel = dynamic_cast<NUMemselRvalue*>(expr);
          if ((memsel->getIndex(0)->getType() != NUExpr::eNUConstXZ) &&
              (memsel->getIndex(0)->getType() != NUExpr::eNUConstNoXZ)) {
            return false;
          }

          // Only CSE identifier selects.
          if (memsel->getIdentExpr()->getType() != NUExpr::eNUIdentRvalue) {
            return false;
          }
        }
        break;

      case NUExpr::eNUUnaryOp:
      {
        NUUnaryOp * op = dynamic_cast<NUUnaryOp*>(expr);
        switch (op->getOp()) {
          case NUOp::eUnLogNot:
          case NUOp::eUnBitNeg:
          case NUOp::eUnVhdlNot:
          case NUOp::eUnRedAnd:
          case NUOp::eUnRedOr:
          case NUOp::eUnRedXor:
          {
            // the operand of the UnaryOp has to be one of IdentRvalue,
            // VarselRvalue, MemselRvalue, or another UnaryOp becuase 
            // nested CSE is not supported.
            if ((op->getArg(0)->getType()) == NUExpr::eNUConcatOp)
              return false;
            else
              return (qualify((op)->getArg(0)));
          }
          break;
          default:
            return false;
            break;
        }
      }
      break;
      default:
        return false;
        break;
    }
      
    // Only CSE exprs that are small enough
    // to be candidates for register residence.  Copying bitvectors
    // is not in general a winning strategy.
    //
    return (expr->getBitSize () <= (8 * sizeof (UInt64)));
  }

private:
  //! Hide copy and assign constructors.
  CSE(const CSE&);
  CSE& operator=(const CSE&);

  //! Number of references we've seen to this CSE
  UInt32 mRefs;

  //! The common sub expression
  NUExpr *mExpr;
};


//! Local helper class to keep track of where we are emitting.
class EmitContext
{
public:
  EmitContext(NUScope *scope, NUStmtList *stmts) :
    mScope(scope),
    mStmts(stmts),
    mInsertIter(stmts->end())
  {}

  ~EmitContext() {}

  //! Scope for any temps created
  NUScope *mScope;

  //! Emit into this stmt list
  NUStmtList *mStmts;

  //! Position to emit before
  NUStmtList::iterator mInsertIter;

  //! Source location to tag emit with
  SourceLocator mLoc;
};


//! Walk a tree, replacing CSE's with temps
class RewriteCSE : public NuToNuFn
{
public:
  RewriteCSE(CSE *cse, NUExpr *replace_expr) :
    mCSE(cse),
    mReplaceExpr(replace_expr)
  {}

  ~RewriteCSE()
  {}

  NUExpr* operator()(NUExpr *expr, Phase phase)
  {
    if (not isPre(phase)) {
      return 0;
    }

    if (mCSE->equivalent(expr)) {
      CopyContext ctx(0,0);
      delete expr;
      return mReplaceExpr->copy(ctx);
    } else {
      return 0;
    }
  }

private:
  CSE *mCSE;
  NUExpr *mReplaceExpr;
};


//! Keep track of information relevant to the application of a CSE
/*!
 * Keep track of stmts in which the CSE is found, and what list the
 * CSE can be emitted into.
 */
class CSEInfo
{
public:
  CSEInfo(CSE *cse, EmitContext &emit_ctx) :
    mCSE(cse),
    mEmitCtx(emit_ctx)
  {}

  ~CSEInfo()
  {
    delete mCSE;
  }
    
  //! Return true if it is cost-effective to emit this CSE
  /*!
   * The break even point is:  (cost of assign to temp) \<= (\#refs-1)*cost
   */
  bool costEffective() const
  {
    UInt32 cost_temp_assign = (mCSE->getBitSize() + 31) / 32;
    return (cost_temp_assign <= (mCSE->numRefs() - 1) * mCSE->cost());
  }

  //! Return the cse.
  CSE *getCSE() const
  {
    return mCSE;
  }

  //! Change the emit context to be the passed-in context.
  void updateEmitCtx(EmitContext &emit_ctx)
  {
    mEmitCtx = emit_ctx;
  }

  //! Add the statement as being one that references the CSE
  void addStmt(NUStmt *stmt)
  {
    // We may try to add the statement multiple times.  Since they are added in order,
    // just need to check the last one to see if its already there.
    if ((not mStmts.empty()) and (mStmts.back() == stmt)) {
      return;
    }
    mStmts.push_back(stmt);
  }

  //! Emit the CSE, fixup the uses of the CSE
  void emit(AtomicCache *, ElabAllocAlias *alloc_alias, bool verbose)
  {
    SourceLocator loc = mEmitCtx.mLoc;
    UtString str;
    NUExpr *rvalue = mCSE->createExpr();
    rvalue->compose(&str, NULL);
    NUScope* scope = mEmitCtx.mScope;
    StringAtom* sym = scope->gensym("localcse", NULL, rvalue);

    NUNet *temp_net;
    if ( rvalue->isReal() )
      // Real numbers are stored as 64-bit vectors internally
      temp_net = scope->createTempVectorNet( sym, 
                                             ConstantRange( mCSE->getBitSize()-1, 0 ),
                                             true, loc, eDMRealNet );
    else
      temp_net = scope->createTempNet(sym, mCSE->getBitSize(), 
                                      rvalue->isSignedResult(), loc);
    NUNet* parentNet = rvalue->isWholeIdentifier() ? 
                                            rvalue->getWholeIdentifier() : NULL;
    if (parentNet)
    {
      NetFlags flag = temp_net->getFlags();
      if ( parentNet->isInteger() & rvalue->isSignedResult ())
        // creating a CSE of a signed integer value wants to copy the 
        // integer flag
        flag = NetRedeclare(flag,eDMIntegerNet);
      temp_net->setFlags(flag);
    }

    alloc_alias->add(temp_net);

    if (verbose) {
      UtString str;
      NUScope * scope = temp_net->getScope();
      str += scope->getPrintableName(); // needed for test/localcse/cse{1,3}
      str += ".";
      temp_net->compose(&str, NULL);
      UtIO::cout() << "LocalCSE created temp:  " << str << UtIO::endl;
    }


    if (verbose) {
      UtIO::cout() << "  Replaces:  " << str << UtIO::endl;
    }

    NULvalue *lvalue = new NUIdentLvalue(temp_net, loc);
    NUBlockingAssign *assign = new NUBlockingAssign(lvalue, rvalue, false, loc);

    if (mEmitCtx.mInsertIter == mEmitCtx.mStmts->begin()) {
      mEmitCtx.mStmts->push_front(assign);
    } else {
      NUStmtList::iterator insert_at = mEmitCtx.mInsertIter;
      mEmitCtx.mStmts->insert(insert_at, assign);
    }

    NUExpr *new_expr = new NUIdentRvalue(temp_net, loc);
    if (rvalue->isSignedResult())
      new_expr->setSignedResult(true);
    new_expr->resize(mCSE->getBitSize());

    RewriteCSE rewriter(mCSE, new_expr);
    for (NUStmtList::iterator stmt_iter = mStmts.begin();
	 stmt_iter != mStmts.end();
	 ++stmt_iter) {
      NUStmt *stmt = *stmt_iter;
      if (verbose) {
	UtString str;
	stmt->compose(&str, NULL);
	UtIO::cout() << "  Used in stmt:\n" << str << UtIO::endl;
      }

      // TBD Part of the quick workaround to CSE if-stmt conditionals.
      // For if stmts, don't want to blindly replace all exprs within
      // all nested stmts, just want to replace conditionals.
      NUType stmtType = stmt->getType();
      switch (stmtType) {
      case eNUIf: {
        NUIf * ifstmt = dynamic_cast<NUIf*>(stmt);
        NUExpr * repl = ifstmt->getCond()->translate(rewriter);
        ifstmt->replaceCond(repl);
        break;
      }
      case eNUCase: {
        NUCase * casestmt = dynamic_cast<NUCase*>(stmt);
        NUExpr * repl = casestmt->getSelect()->translate(rewriter);
        casestmt->replaceSelect(repl);
        break;
      }
      case eNUBlockingAssign:
      case eNUBlockingEnabledDriver:
	stmt->replaceLeaves(rewriter);
        break;
      default:
	NU_ASSERT(0 == "Unexpected object type", stmt);
        break;
      }
    }

    delete new_expr;
  }

private:
  //! Hide copy and assign constructors.
  CSEInfo(const CSEInfo&);
  CSEInfo& operator=(const CSEInfo&);

  //! The common subexpression
  CSE *mCSE;

  //! List of stmts which use the CSE
  NUStmtList mStmts;

  //! Where to emit the temp assign
  EmitContext mEmitCtx;
};

struct CSEInfoCompare{
  bool operator()(const CSEInfo *a, const CSEInfo* b) const {
    if (a == b)
      return false;
    int cmp = NUExpr::compare(a->getCSE()->getExpr(), b->getCSE()->getExpr(),
                              false, false, true);
    return cmp < 0;
  }
};

//! Manage CSE information
class CSEInfoManager
{
private:
  typedef UtMultiMap<NUNet*,CSEInfo*, NUNetCmp> IdCSEInfoMap;
  typedef IdCSEInfoMap::iterator IdCSEInfoMapIter;
  typedef std::pair<IdCSEInfoMapIter,IdCSEInfoMapIter> IdCSEInfoMapRange;

public:
  typedef UtSet<CSEInfo*, CSEInfoCompare> CSEInfoSet;

  CSEInfoManager(ElabAllocAlias *alloc_alias) :
    mAllocAlias(alloc_alias)
  {}

  ~CSEInfoManager()
  {
    clear();
  }

  //! Remember the given expression for CSE purposes
  /*!
   * Not all expressions may be CSE-able, so not all expressions are remembered here.
   */
  void rememberExpr(NUExpr *expr, NUStmt *stmt, EmitContext &emit_ctx, UInt32 cnt)
  {
    if (not CSE::qualify(expr)) {
      return;
    }

    NUNetSet uses;
    expr->getUses(&uses);
    CSEInfoSet existing_set;
    getOverlap(&uses, &existing_set);

    // If there is an existing CSE to already use, just make the stmt be a referer
    for (CSEInfoSet::iterator exist_iter = existing_set.begin();
	 exist_iter != existing_set.end();
	 ++exist_iter) {
      CSEInfo *cse_info = *exist_iter;
      if (cse_info->getCSE()->equivalent(expr)) {
	cse_info->getCSE()->incRef(cnt);
	cse_info->addStmt(stmt);
	cse_info->updateEmitCtx(emit_ctx);
	return;
      }
    }

    // No existing CSE, create one, and create an info for it.
    CopyContext copy_ctx(0,0);
    CSE *new_cse = new CSE(expr->copy(copy_ctx));
    new_cse->incRef(cnt);
    CSEInfo *new_cse_info = new CSEInfo(new_cse, emit_ctx);
    new_cse_info->addStmt(stmt);
    mCSEInfoSet.insert(new_cse_info);
    for (NUNetSet::SortedLoop p = uses.loopSorted(); !p.atEnd(); ++p) {
      NUNet* net = *p;
      NUNet* rep_net = mAllocAlias->getRepresentativeNet(net);
      insertUnique(rep_net, new_cse_info);
    }
  }

  //! Populate a set of CSEInfo which can allocation-overlap the given net.
  /*!
   * TBD: the cse info set should be ordered such that simpler CSE's come first, more complex
   * later?  Then, can handle nested CSE's easier?
   */
  void getOverlap(NUNetSet *net_set, CSEInfoSet *cse_info_set)
  {
    for (NUNetSet::SortedLoop p = net_set->loopSorted(); !p.atEnd(); ++p) {
      NUNet* net = *p;
      NUNet* rep_net = mAllocAlias->getRepresentativeNet(net);
      IdCSEInfoMapRange range = mCSEInfoMap.equal_range(rep_net);
      for (IdCSEInfoMapIter map_iter = range.first;
	   map_iter != range.second;
	   ++map_iter) {
	cse_info_set->insert(map_iter->second);
      }
    }
  }

  //  typedef Loop<CSEInfoSet, CSEInfoSet::iterator> CSEInfoLoop;
  typedef Loop<CSEInfoSet> CSEInfoLoop;

  //! Loop over all CSE info
  CSEInfoLoop loop()
  {
    return CSEInfoLoop(mCSEInfoSet);
  }

  //! Remove any CSEInfo which overlap with the given net
  void removeOverlap(NUNet *net)
  {
    CSEInfoSet remove_set;
    NUNet* rep_net = mAllocAlias->getRepresentativeNet(net);
    IdCSEInfoMapRange range = mCSEInfoMap.equal_range(rep_net);
    for (IdCSEInfoMapIter iter = range.first;
	 iter != range.second;
	 ++iter) {
      remove_set.insert(iter->second);
    }

    for (CSEInfoSet::iterator iter = remove_set.begin();
	 iter != remove_set.end();
	 ++iter) {
      CSEInfo *cse_info = *iter;
      remove(cse_info);
    }
  }

  //! Clear the state; remove all CSE info structures and the maps
  void clear()
  {
    for (CSEInfoSet::iterator iter = mCSEInfoSet.begin();
	 iter != mCSEInfoSet.end();
	 ++iter) {
      delete *iter;
    }
    mCSEInfoSet.clear();
    mCSEInfoMap.clear();
  }

private:
  //! Hide copy and assign constructors.
  CSEInfoManager(const CSEInfoManager&);
  CSEInfoManager& operator=(const CSEInfoManager&);

  //! Insert the CSEInfo for the allocation, make sure we don't insert it twice for the same allocation.
  void insertUnique(NUNet* rep_net, CSEInfo *cse_info)
  {
    IdCSEInfoMapRange range = mCSEInfoMap.equal_range(rep_net);
    bool found = false;
    for (IdCSEInfoMapIter iter = range.first; iter != range.second; ++iter) {
      if (iter->second == cse_info) {
	found = true;
	break;
      }
    }
    if (not found) {
      mCSEInfoMap.insert(IdCSEInfoMap::value_type(rep_net, cse_info));
    }
  }

  //! Remove the given CSEInfo and delete it
  void remove(CSEInfo *cse_info)
  {
    NUNetSet use_set;
    const NUExpr *expr = cse_info->getCSE()->getExpr();
    expr->getUses(&use_set);
    for (NUNetSet::UnsortedLoop p = use_set.loopUnsorted(); !p.atEnd(); ++p) {
      NUNet* net = *p;
      NUNet* rep_net = mAllocAlias->getRepresentativeNet(net);
      IdCSEInfoMapRange range = mCSEInfoMap.equal_range(rep_net);
      bool did_remove = false;
      for (IdCSEInfoMapIter remove_iter = range.first;
	   remove_iter != range.second;
	   ++remove_iter) {
	if (remove_iter->second == cse_info) {
	  mCSEInfoMap.erase(remove_iter);
	  did_remove = true;
	  break;
	}
      }
      NU_ASSERT(did_remove, net);
    }

    CSEInfoSet::iterator set_iter = mCSEInfoSet.find(cse_info);
    NU_ASSERT(set_iter != mCSEInfoSet.end(), expr);
    mCSEInfoSet.erase(set_iter);
    delete cse_info;
  }

  //! Allocation aliases keeper
  ElabAllocAlias *mAllocAlias;

  //! Map allocation ids to the CSE-info's which use anything in that id.
  //! May be multiple entries per CSEInfo, since exprs may use many different nets.
  IdCSEInfoMap mCSEInfoMap;

  //! Container of all CSEInfo's
  CSEInfoSet mCSEInfoSet;
};


//! Walk the always and initial blocks within a module, perform CSE locally
class StructuredProcWalkerCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  StructuredProcWalkerCallback(LocalCSE *local_cse) : mLocalCSE(local_cse) {}
  ~StructuredProcWalkerCallback() {}

  Status operator() (Phase /* phase */, NUBase * /* node */)
  {
    return eSkip;
  }

  Status operator() (Phase /* phase */, NUModule * /* node */)
  {
    return eNormal;
  }

  Status operator() (Phase /* phase */, NUNamedDeclarationScope * /* node */)
  {
    return eNormal;
  }

  Status operator() (Phase /* phase */, NUStructuredProc *node)
  {
    mLocalCSE->analyze(node);
    return eSkip;
  }

private:
  LocalCSE *mLocalCSE;
};


//! Callback to find CSE-able expressions within trivially nested if-stmts
/*!
 * TBD This should be done better, but I am running out of time.
 * Ultimately, we should handle arbitrary stmt nesting.
 * For now, I am just trying to get CSE's which are in if stmt
 * conditionals, and so I am allowing trivially-nested if stmts in
 * the walk.
 */
class HandleIfCaseWalker
{
public:
  HandleIfCaseWalker(CSEInfoManager *manager, EmitContext &emit_ctx) :
    mCSEInfoManager(manager),
    mEmitCtx(emit_ctx)
  {}

  ~HandleIfCaseWalker() {}

  //! Walk the if stmt handling only simple stmt nesting
  void walkIf(NUIf * stmt);

  //! Walk the case stmt handling only simple stmt nesting
  void walkCase(NUCase * stmt);

private:
  CSEInfoManager *mCSEInfoManager;
  EmitContext mEmitCtx;
};

class CSENestedIfCaseFinderCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  CSENestedIfCaseFinderCallback(CSEInfoManager *manager, NUStmt *stmt, EmitContext &emit_ctx) :
    mCSEInfoManager(manager),
    mStmt(stmt),
    mEmitCtx(emit_ctx),
    mWalker(NULL)
  {}

  void putWalker(NUDesignWalker* walker) { mWalker = walker; }

  ~CSENestedIfCaseFinderCallback() {}

  Status operator() (Phase /* phase */, NUBase * /* node */)
  {
    return eNormal;
  }

  Status operator() (Phase /* phase */, NUStmt * /* node */)
  {
    // Found a stmt which is not an if stmt, so stop.
    return eStop;
  }

  Status operator() (Phase phase, NUIf *node)
  {
    if (phase == ePre) {
      HandleIfCaseWalker walker(mCSEInfoManager, mEmitCtx);
      walker.walkIf(node);
    }
    // bug 3128 - Stop after the first statement, because it may invalidate CSE's.
    return eStop;
  }

  Status operator() (Phase phase, NUCase *node)
  {
    if (phase == ePre) {
      HandleIfCaseWalker walker(mCSEInfoManager, mEmitCtx);
      walker.walkCase(node);
    }
    // bug 3128 - Stop after the first statement, because it may invalidate CSE's.
    return eStop;
  }

  Status operator() (Phase phase, NUExpr *expr)
  {
    // Temporary code to get Mindspeed going. Get rid of it when
    // change detects are fixed to have a separate nucleus object for
    // initialization. This is one of two copies of this code
    if ((phase == ePre) && (expr->getType() == NUExpr::eNUUnaryOp)) {
      NUChangeDetect* change = dynamic_cast<NUChangeDetect*>(expr);
      if (change != NULL) {
        return eSkip;
      }
    }

    if (phase == ePost) {
      mCSEInfoManager->rememberExpr(expr, mStmt, mEmitCtx, 1);
    }
    return eNormal;
  }

  //! For varsels, do not walk through the nested identifier.
  Status operator() (Phase /*phase*/, NUVarselRvalue *expr)
  {
    // walk the index expression, but not the identifier.
    NU_ASSERT(mWalker != NULL, expr);
    mWalker->expr(expr->getIndex());

    mCSEInfoManager->rememberExpr(expr, mStmt, mEmitCtx, 1);
    return eSkip;
  }

private:
  CSEInfoManager *mCSEInfoManager;
  NUStmt *mStmt;
  EmitContext mEmitCtx;
  NUDesignWalker * mWalker;
};


void HandleIfCaseWalker::walkIf(NUIf * stmt)
{
  CSENestedIfCaseFinderCallback cond_callback(mCSEInfoManager, stmt, mEmitCtx);
  NUDesignWalker cond_walker(cond_callback, false);
  cond_callback.putWalker(&cond_walker);
  cond_walker.expr(stmt->getCond());

  for (NUStmtLoop loop = stmt->loopThen(); not loop.atEnd(); ++loop) {
    NUStmt *this_stmt = *loop;
    CSENestedIfCaseFinderCallback nestedif_find_cse(mCSEInfoManager, this_stmt, mEmitCtx);
    NUDesignWalker walker(nestedif_find_cse, false);
    nestedif_find_cse.putWalker(&walker);
    NUDesignCallback::Status status = walker.stmt(this_stmt);
    if (status == NUDesignCallback::eStop) {
      break;
    }
  }
  for (NUStmtLoop loop = stmt->loopElse(); not loop.atEnd(); ++loop) {
    NUStmt *this_stmt = *loop;
    CSENestedIfCaseFinderCallback nestedif_find_cse(mCSEInfoManager, this_stmt, mEmitCtx);
    NUDesignWalker walker(nestedif_find_cse, false);
    nestedif_find_cse.putWalker(&walker);
    NUDesignCallback::Status status = walker.stmt(this_stmt);
    if (status == NUDesignCallback::eStop) {
      break;
    }
  }
}


void HandleIfCaseWalker::walkCase(NUCase * stmt)
{
  CSENestedIfCaseFinderCallback cond_callback(mCSEInfoManager, stmt, mEmitCtx);
  NUDesignWalker cond_walker(cond_callback, false);
  cond_callback.putWalker(&cond_walker);
  cond_walker.expr(stmt->getSelect());

  for (NUCase::ItemLoop itemloop = stmt->loopItems(); 
       not itemloop.atEnd(); 
       ++itemloop) {
    NUCaseItem * item = (*itemloop);

    // process each condition associated with this case item.
    for (NUCaseItem::ConditionLoop condloop = item->loopConditions(); 
         not condloop.atEnd();
         ++condloop) {
      NUCaseCondition * condition = (*condloop);
      cond_walker.expr(condition->getExpr());
    }

    // process each statement associated with this case item.
    for (NUStmtLoop loop = item->loopStmts(); 
         not loop.atEnd(); 
         ++loop) {
      NUStmt *this_stmt = *loop;
      CSENestedIfCaseFinderCallback nestedif_find_cse(mCSEInfoManager, this_stmt, mEmitCtx);
      NUDesignWalker walker(nestedif_find_cse, false);
      nestedif_find_cse.putWalker(&walker);
      NUDesignCallback::Status status = walker.stmt(this_stmt);
      if (status == NUDesignCallback::eStop) {
        break;
      }
    }
  }
}


//! Callback to find CSE-able expressions within a statement
class CSEFinderCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  CSEFinderCallback(CSEInfoManager *manager, NUStmt *stmt, EmitContext &emit_ctx) :
    mCSEInfoManager(manager),
    mStmt(stmt),
    mEmitCtx(emit_ctx),
    mWalker(NULL)
  {}
  ~CSEFinderCallback() {}

  void putWalker(NUDesignWalker* walker) { mWalker = walker; }
  
  Status operator() (Phase /* phase */, NUBase * /* node */)
  {
    return eNormal;
  }

  Status operator() (Phase /* phase */, NUStmt * /* node */)
  {
    return eSkip;
  }

  Status operator() (Phase /* phase */, NUEnabledDriver * /* node */)
  {
    return eNormal;
  }

  Status operator() (Phase /* phase */, NUBlockingAssign * /* node */)
  {
    return eNormal;
  }

  Status operator() (Phase phase, NUIf *node)
  {
    /* TBD This should be done better, but I am running out of time.
     * Ultimately, we should handle arbitrary stmt nesting.
     * For now, I am just trying to get CSE's which are in if stmt
     * conditionals, and so I am allowing trivially-nested if stmts in
     * the walk.
     */
    if (phase == ePre) {
      HandleIfCaseWalker walker(mCSEInfoManager, mEmitCtx);
      walker.walkIf(node);
    }
    return eSkip;
  }

  Status operator() (Phase phase, NUCase *node)
  {
    if (phase == ePre) {
      HandleIfCaseWalker walker(mCSEInfoManager, mEmitCtx);
      walker.walkCase(node);
    }
    return eSkip;
  }

  Status operator() (Phase phase, NUExpr *expr)
  {
    // Temporary code to get Mindspeed going. Get rid of it when
    // change detects are fixed to have a separate nucleus object for
    // initialization. This is one of two copies of this code
    if ((phase == ePre) && (expr->getType() == NUExpr::eNUUnaryOp)) {
      NUChangeDetect* change = dynamic_cast<NUChangeDetect*>(expr);
      if (change != NULL) {
        return eSkip;
      }
    }

    if (phase == ePost) {
      mCSEInfoManager->rememberExpr(expr, mStmt, mEmitCtx, 1);
    }
    return eNormal;
  }

  //! For varsels, do not walk through the nested identifier.
  Status operator() (Phase /*phase*/, NUVarselRvalue *expr)
  {
    // walk the index expression, but not the identifier.
    NU_ASSERT(mWalker != NULL, expr);
    mWalker->expr(expr->getIndex());

    mCSEInfoManager->rememberExpr(expr, mStmt, mEmitCtx, 1);
    return eSkip;
  }

private:
  CSEInfoManager *mCSEInfoManager;
  NUStmt *mStmt;
  EmitContext mEmitCtx;
  NUDesignWalker * mWalker;
};


LocalCSE::LocalCSE(NUNetRefFactory *netref_factory,
		   MsgContext *msg_context,
		   ElabAllocAlias *alloc_alias,
		   AtomicCache *string_cache,
		   IODBNucleus *iodb,
                   ArgProc* args,
		   bool verbose) :
  DesignWalker(verbose),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mCSEManager(new CSEInfoManager(alloc_alias)),
  mAllocAlias(alloc_alias),
  mStringCache(string_cache),
  mIODB(iodb),
  mDidSomething(false),
  mArgs(args)
{
}


LocalCSE::~LocalCSE()
{
  delete mCSEManager;
}


void LocalCSE::module(NUModule *this_module)
{
  if (haveVisited(this_module)) {
    return;
  }

  // Recurse to handle sub-modules
  NUModuleList inst_list;
  this_module->getInstantiatedModules(&inst_list);
  for (NUModuleListIter iter = inst_list.begin();
       iter != inst_list.end();
       ++iter) {
    module(*iter);
  }

  // walk the initial and always blocks
  StructuredProcWalkerCallback walker_callback(this);
  NUDesignWalker walker(walker_callback, false);
  walker.module(this_module);

  // apparently we do not process tasks
  
  rememberVisited(this_module);
}


void LocalCSE::analyze(NUStructuredProc * proc)
{
  NUBlock * block = proc->getBlock();

  UInt32 pre_cost_ops = 0;
  UInt32 pre_cost_instrs = 0;
  if (mVerbose) {
    NUCostContext cost_ctx;
    NUCost cost;
    block->calcCost(&cost, &cost_ctx);
    pre_cost_ops = cost.mOps;
    pre_cost_instrs = cost.mInstructions;
  }

  mDidSomething = false;

  NUStmtLoop loop = block->loopStmts();

  // Stmts list so we can do reverse iteration
  NUStmtList stmts(loop.begin(), loop.end());

  // Stmt list to emit into
  NUStmtList new_stmts;

  EmitContext emit_ctx(block, &new_stmts);

  for (NUStmtList::reverse_iterator iter = stmts.rbegin();
       iter != stmts.rend();
       ++iter) {
    NUStmt *stmt = *iter;
    NUNetSet defs;
    stmt->getNonBlockingDefs(&defs);
    NU_ASSERT(defs.empty(), stmt);
    stmt->getBlockingDefs(&defs);

    emitCSE(defs);
    killCSE(defs);

    emit_ctx.mLoc = stmt->getLoc();
    new_stmts.push_front(stmt);
    emit_ctx.mInsertIter = new_stmts.begin();
    rememberCSE(stmt, emit_ctx);
  }

  emitCSE();
  mCSEManager->clear();

  if (mDidSomething) {
    block->replaceStmtList(new_stmts);
    UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.structuredProc(proc);
  }

  UInt32 post_cost_ops = 0;
  UInt32 post_cost_instrs = 0;
  if (mVerbose) {
    NUCostContext cost_ctx;
    NUCost cost;
    block->calcCost(&cost, &cost_ctx);
    post_cost_ops = cost.mOps;
    post_cost_instrs = cost.mInstructions;
  }

  if (mVerbose and mDidSomething) {
    UtIO::cout() << "LocalCSE:  " << proc->getName()->str() << UtIO::endl;
    UtIO::cout() << "  Pre-CSE ops:      " << pre_cost_ops << UtIO::endl;
    UtIO::cout() << "  Pre-CSE instrs:   " << pre_cost_instrs << UtIO::endl;
    UtIO::cout() << "  Post-CSE ops:     " << post_cost_ops << UtIO::endl;
    UtIO::cout() << "  Post-CSE instrs:  " << post_cost_instrs << UtIO::endl;
  }
}


void LocalCSE::emitCSE()
{
  for (CSEInfoManager::CSEInfoLoop loop = mCSEManager->loop();
       not loop.atEnd();
       ++loop) {
    CSEInfo *cse_info = *loop;
    if (cse_info->costEffective()) {
      cse_info->emit(mStringCache, mAllocAlias, mVerbose);
      mDidSomething = true;
    }
  }
}


void LocalCSE::emitCSE(NUNetSet &defs)
{
  CSEInfoManager::CSEInfoSet overlap_set;
  mCSEManager->getOverlap(&defs, &overlap_set);
  for (CSEInfoManager::CSEInfoSet::iterator cse_info_iter = overlap_set.begin();
       cse_info_iter != overlap_set.end();
       ++cse_info_iter) {
    CSEInfo *cse_info = *cse_info_iter;
    if (cse_info->costEffective()) {
      // Note: when nested CSE's are allowed, this should be modified such that
      // emitted CSE's are removed before the next CSE is done.  This is so that
      // we get an accurate cost-effective measure of the nested exprs:
      //   a = b + (c + d);
      //   e = b + (c + d);
      // If we CSE-out 'b + (c + d)', then 'c + d' should not be CSE'd, since
      // then it will just occur once:
      //   tmp = b + (c + d);
      //   a = tmp;
      //   e = tmp;
      cse_info->emit(mStringCache, mAllocAlias, mVerbose);
      mDidSomething = true;
    }
  }
}


void LocalCSE::killCSE(NUNetSet &defs)
{
  for (NUNetSet::iterator iter = defs.begin(); iter != defs.end(); ++iter) {
    NUNet *net = *iter;
    mCSEManager->removeOverlap(net);
  }
}


void LocalCSE::rememberCSE(NUStmt *stmt,
			   EmitContext &emit_ctx)
{
  CSEFinderCallback find_cse(mCSEManager, stmt, emit_ctx);
  NUDesignWalker walker(find_cse, false);
  find_cse.putWalker(&walker);
  walker.stmt(stmt);
}
