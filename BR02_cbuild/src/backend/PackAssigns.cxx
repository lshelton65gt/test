// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "PackAssigns.h"
#include "compiler_driver/CarbonContext.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCase.h"
#include "reduce/Fold.h"
#include "util/ArgProc.h"
#include "util/UtIOStream.h"

PackAssigns::PackAssigns( NUNetRefFactory *netref_factory,
			  MsgContext *msg_context,
			  AtomicCache *str_cache,
                          UInt32 size_threshold,
                          ArgProc *arg,
                          Fold* folder,
                          PackAssignsStats *stats ) :
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mStrCache(str_cache),
  mSizeThreshold(size_threshold),
  mStats(stats),
  mArg(arg),
  mFold(folder)
{
  mVerbose = mArg->getBoolValue(CRYPT("-verbosePackAssigns"));
}


PackAssigns::~PackAssigns()
{
}

void PackAssigns::design(NUDesign *d)
{
  NUModuleList mods;
  d->getModulesBottomUp(&mods);
  for (NUModuleList::iterator iter = mods.begin();
       iter != mods.end();
       ++iter) {
    module( *iter );
  }
}

void PackAssigns::module(NUModule *this_module)
{
  bool work_done = false;

  //Skip continuous assigns for now.
  //work_done |= contAssigns(this_module);

  work_done |= tfs(this_module);
  work_done |= structuredProcs(this_module);

  if (work_done) {
    mStats->module();
  }
}


bool PackAssigns::tfs(NUModule *module)
{
  bool work_done = false;

  for (NUTaskLoop loop = module->loopTasks();
       not loop.atEnd();
       ++loop) {
    work_done |= blockScope(*loop);
  }

  return work_done;
}

/////////////////////////////////
// This code is not called for now.  FIXME.
/////////////////////////////////
bool PackAssigns::contAssigns(NUModule *module)
{
  bool work_done = false;

  // Assuming contAssigns are not ordered, we need to test each assign with
  // all other assigns.

  NUModule::ContAssignLoop i,j;

  for( i=module->loopContAssigns(); not i.atEnd(); ++i ) {

    for( j=module->loopContAssigns(); *i != *j; /*no increment*/ ) {
      NUModule::ContAssignLoop j_next = j;
      ++j_next;

      NULvalue *new_lhs;
      NUExpr *new_rhs;

      if( combineAssigns( *i, *j, &new_lhs, &new_rhs ) ) {
        work_done = true;

        // Put the new assign back into the list for potential further
        // replacement.
        delete *i;
        *i = new NUContAssign( (*j)->getName(), new_lhs, new_rhs,
                               (*j)->getLoc() );

        module->removeContAssign( *j );
        delete *j;
      }
      j = j_next;
    }
  }

  return work_done;
}

//! Get the net, msb, and lsb for a bitsel or partsel.
//! Returns true if found.
bool PackAssigns::getBitOrPart( NULvalue *lhs, NULvalue **lv,
                                SInt32 *msb, SInt32 *lsb)
{
  NUType t = lhs->getType();

  if( t == eNUVarselLvalue ) {

    NUVarselLvalue *ps = static_cast<NUVarselLvalue *>(lhs);
    NU_ASSERT( ps, lhs );
    *lv = ps->getLvalue();
    if(*lv && ps->isConstIndex ()) {
      const ConstantRange & r (*ps->getRange());
      *msb = r.getMsb();
      *lsb = r.getLsb();
      return true;
    }
  }
  return false;
}

//! Checkin if two Lvalues can be catenated together, and return
//! a new Lvalue if so.
NULvalue * PackAssigns::catenateLvalues( NULvalue *lhs_1, NULvalue *lhs_2,
                                         NUExpr* rhs_1, NUExpr* rhs_2,
                                         bool *left_to_right )
{
  NULvalue* lvalue_1;
  SInt32 msb_1, lsb_1;

  NULvalue* lvalue_2;
  SInt32 msb_2, lsb_2;

  if( !getBitOrPart( lhs_1, &lvalue_1, &msb_1, &lsb_1 ) )
    return NULL;

  if( !getBitOrPart( lhs_2, &lvalue_2, &msb_2, &lsb_2 ) )
    return NULL;

  if( !(*lvalue_1 == *lvalue_2) )
    return NULL;

  // Don't catenate if the parts straddle a multiple of the threshold size.
  const ConstantRange r(lvalue_1->determineBitSize() - 1, 0);
  ConstantRange r1(msb_1, lsb_1);
  ConstantRange r2(msb_2, lsb_2);

  // bug3412 -- disqualify out-of-bounds ranges
  if (! (r.contains(r1.getMsb())  && r.contains(r1.getLsb()) &&
         r.contains(r2.getMsb())  && r.contains(r2.getLsb())))
    return NULL;
         
      
  r1.normalize(&r);
  r2.normalize(&r);

  /* 
   * If either range crosses a 32-bit barrier, then look more carefully.
   * Constants or shared variables on the RHS would convince us to go
   * ahead and do a large pack anyway.
   */
  if( (r1.getMsb()/mSizeThreshold) != (r2.getLsb()/mSizeThreshold) ||
      (r2.getMsb()/mSizeThreshold) != (r1.getLsb()/mSizeThreshold) )
  {
    // Constants are OK, certainly.  If either side is not a constant,
    // then it's harder to predict whether the packing would be a win.
    // For now, punt.
    if ((rhs_1->castConst() == NULL) || (rhs_2->castConst() == NULL)) {
      return NULL;
    }
  }
   
  ConstantRange new_range;

  if( r1.getLsb() == (r2.getMsb()+1) ) {
    *left_to_right = true;
    new_range.setMsb( r1.getMsb() );
    new_range.setLsb( r2.getLsb() );
  } else if( r2.getLsb() == (r1.getMsb()+1) ) {
    *left_to_right = false;
    new_range.setMsb( r2.getMsb() );
    new_range.setLsb( r1.getLsb() );
  } else
    return NULL;

  new_range.denormalize(&r);

  CopyContext cc(0,0);
  return new NUVarselLvalue( lvalue_1->copy(cc), new_range, lhs_1->getLoc() );
}

bool PackAssigns::copyExprOrConcat( NUExpr *e, NUExprVector *v )
{
  CopyContext cc(0,0);

  bool sizes_match = ( e->determineBitSize() == e->getBitSize());
  bool internal_concat ( e->getType() == NUExpr::eNUConcatOp );
  if ( internal_concat && sizes_match ){
    NUConcatOp *co = (dynamic_cast<NUConcatOp *>(e));
    NU_ASSERT( co, e );
    if( co->getRepeatCount() == 1 ) {
      // In this special case it is safe to insert the operands of the
      // concat in the vector. This eliminates the need to call fold
      // to remove the internal the CONCAT
      // (and saves significant cbuild memory 10% on the test:
      //  test/cust/sun/fire/carbon/cwtb/fire)

      SInt32 nArgs = co->getNumArgs();
      for(SInt32 i=0; i<nArgs; i++) {
        v->push_back( co->getArg(i)->copy( cc ) );
      }
      return false; // the internal concat has been flattened
    }
  }
  
  NUExpr* temp_e = e->copy( cc );
  if ( not sizes_match ) {
    temp_e = temp_e->makeSizeExplicit();
  }
  v->push_back( temp_e );
  return internal_concat;
}

//! Determins if the defs of an earlier assign overlap with the uses
//! of the later assign.
//! For example:  a[1:0] = b;  a[2] = a[0] & c;  ----> overlap on def/use a[0]
bool PackAssigns::assignsOverlap( NUAssign *a1, NUAssign *a2 )
{
  NUNetRefSet uses(mNetRefFactory);
  NUNetRefSet defs(mNetRefFactory);

  if(a1->useBlockingMethods())
    a1->getBlockingDefs(&defs);
  else
    a1->getDefs(&defs);

  if(a2->useBlockingMethods())
    a2->getBlockingUses(&uses);
  else
    a2->getUses(&uses);

  return NUNetRefSet::set_has_intersection(uses, defs);
}

bool PackAssigns::combineAssigns( NUAssign *a1, NUAssign *a2,
                                  NULvalue **new_lhs, NUExpr **new_rhs )
{
  NULvalue *lhs_1 = a1->getLvalue();
  NULvalue *lhs_2 = a2->getLvalue();
  NUExpr *rhs_1 = a1->getRvalue();
  NUExpr *rhs_2 = a2->getRvalue();
  bool left_to_right;

  // For now, bets are off if the lhs sizes do not match the rhs sizes.
  // Could do the appropriate extending/cropping here if we want to be
  // more aggressive.
  // The rhs may be larger than the lhs, and in that case, the rhs
  // would need to be cropped to make the appropriate concat.
  if( lhs_1->getBitSize() != rhs_1->getBitSize() ||
      lhs_2->getBitSize() != rhs_2->getBitSize() ) {
    return false;
  }

  // Disallow if the use set of the assigns overlap the def sets.
  if( assignsOverlap( a1, a2 ) ) {
    return false;
  }  

  // Try catenating in either order.
  *new_lhs = catenateLvalues( lhs_1, lhs_2, rhs_1, rhs_2, &left_to_right );

  if( !*new_lhs )
    return false;

  // Disallow if the use set of either contains the control flow net
  // while the other does not (this would mean that a $stop was
  // between them). But also make sure this is not a temp. We allow
  // temps to move around. In that case we can handle the control flow
  // net, we can just OR the two conditions.
  NUNetSet defs;
  (*new_lhs)->getDefs(&defs);
  bool allTemps = true;
  for (NUNetSetNonconstIter i = defs.begin(); (i != defs.end()) && allTemps; ++i) {
    NUNet* net = *i;
    if (!net->isTempBlockLocalNonStatic()) {
      allTemps = false;
    }
  }
  if (!allTemps && (a1->getUsesCFNet() != a2->getUsesCFNet())) {
    // this should never be possible, but just to be safe it is included.
    // if we don't see this assert by 08/18/06 then consider this code
    // dead and remove it.
    NU_ASSERT ("Assignment packing cannot process control-flow net.(try -noPackAssigns)"==0, a1); 
    return false;
  }
    
  // Make a new concat.
  // If either child is a concat, bring the operands up to make a
  // single level concat.  Puts less of a strain on optimization later.
  // first establish the order of operands: {rhs_a,rhs_b}
  NUExpr *rhs_a;
  NUExpr *rhs_b;
  if( left_to_right ) {
    rhs_a = rhs_1;
    rhs_b = rhs_2;
  } else {
    rhs_a = rhs_2;
    rhs_b = rhs_1;
  }

  NUExprVector v;
  bool has_internal_concat = false;

  has_internal_concat |= copyExprOrConcat( rhs_a, &v);
  has_internal_concat |= copyExprOrConcat( rhs_b, &v );

  *new_rhs = new NUConcatOp( v, 1, a1->getLoc() );
  // if either of the terms just combined were left as a concat then we now
  // have a concat within a concat, call fold to expand the lower concat.
  if ( has_internal_concat and
      ( not mArg->getBoolValue (CRYPT("-nofold")))) {
    (*new_rhs)->resize((*new_rhs)->determineBitSize());
    *new_rhs = mFold->fold(*new_rhs);
  }

  return true;
}


bool PackAssigns::structuredProcs(NUModule *module)
{
  bool work_done = false;

  for (NUModule::InitialBlockLoop loop = module->loopInitialBlocks();
       not loop.atEnd();
       ++loop) {
    NUInitialBlock *initial = *loop;
    work_done |= blockScope(initial->getBlock());
  }

  for (NUModule::AlwaysBlockLoop loop = module->loopAlwaysBlocks();
       not loop.atEnd();
       ++loop) {
    NUAlwaysBlock *always = *loop;
    work_done |= blockScope(always->getBlock());
  }

  return work_done;
}


bool PackAssigns::blockScope(NUBlockScope *block)
{
  bool work_done = false;

  NUStmtLoop loop = block->loopStmts();
  NUStmtList stmts(loop.begin(), loop.end());
  work_done = stmtList(stmts);
  if (work_done) {
    block->replaceStmtList(stmts);
  }

  return work_done;
}


bool PackAssigns::stmtList(NUStmtList &stmts)
{
  bool work_done = false;

  // First process any child blocks
  for( NUStmtList::iterator k=stmts.begin(); k!=stmts.end(); ++k ) {
    work_done |= stmt( *k );
  }

  NUStmtList::iterator old_i = stmts.begin();
  NUStmtList::iterator i = old_i;
  if( i!=stmts.end() )
    ++i;

  for( ; i!=stmts.end(); ++i ) {
    NULvalue *new_lhs;
    NUExpr *new_rhs;

    if( (*old_i)->getType() != eNUBlockingAssign ||
        (*i)->getType() != eNUBlockingAssign ) {
      old_i = i;
      continue;
    }

    NUAssign *old_ass = static_cast<NUAssign *>(*old_i);
    NUAssign *ass = static_cast<NUAssign *>(*i);
    NU_ASSERT( ass && old_ass, *i );
     
    if( combineAssigns(old_ass, ass, &new_lhs, &new_rhs) ) {
      work_done = true;
      // Put the new assign back into the list for potential further
      // replacement
      bool usesCFNet = old_ass->getUsesCFNet() | ass->getUsesCFNet();
      NUBlockingAssign *new_ass = new NUBlockingAssign( new_lhs, new_rhs,
                                                        usesCFNet,
                                                        (old_ass)->getLoc() );
      if( mVerbose ) {
        UtIO::cout() << "\nOld packed assign:  ";
        ass->printVerilog(0);
        UtIO::cout() << "Old packed assign:  ";
        old_ass->printVerilog(0);
        UtIO::cout() << "New packed assign:  ";
        new_ass->printVerilog(0);
      }
      delete *i;
      *i = new_ass;
      delete *old_i;

      //  old_i is behind i so we can erase without messing the iterator.
      stmts.erase( old_i );
      mStats->proceduralAssign();
    }
    old_i = i;
  }

  return work_done;
}


bool PackAssigns::stmt( NUStmt *stmt )
{
  bool work_done = false;

  switch (stmt->getType()) {
  case eNUNonBlockingAssign:
    NU_ASSERT("Non-blocking assignment found in assignment packing" && 0, stmt);
    break;
  case eNUBlockingAssign:
    break;
  case eNUIf:
    work_done = ifStmt(dynamic_cast<NUIf*>(stmt));
    break;
  case eNUCase:
    work_done = caseStmt(dynamic_cast<NUCase*>(stmt));
    break;
  case eNUFor:
    work_done = forStmt(dynamic_cast<NUFor*>(stmt));
    break;
  default:
    break;
  }

  return work_done;
}


bool PackAssigns::ifStmt(NUIf *stmt)
{
  NUStmtLoop loop = stmt->loopThen();
  NUStmtList then_stmts(loop.begin(), loop.end());
  bool work_done_then = stmtList(then_stmts);
  if (work_done_then) {
    stmt->replaceThen(then_stmts);
  }

  loop = stmt->loopElse();
  NUStmtList else_stmts(loop.begin(), loop.end());
  bool work_done_else = stmtList(else_stmts);
  if (work_done_else) {
    stmt->replaceElse(else_stmts);
  }

  return (work_done_then or work_done_else);
}


bool PackAssigns::caseStmt(NUCase *stmt)
{
  bool work_done = false;

  for (NUCase::ItemLoop item_loop = stmt->loopItems();
       not item_loop.atEnd();
       ++item_loop) {
    NUCaseItem *item = *item_loop;
    NUStmtLoop stmt_loop = item->loopStmts();
    NUStmtList item_stmts(stmt_loop.begin(), stmt_loop.end());
    bool this_work_done = stmtList(item_stmts);
    work_done |= this_work_done;
    if (this_work_done) {
      item->replaceStmts(item_stmts);
    }
  }

  return work_done;
}


bool PackAssigns::forStmt(NUFor *stmt)
{
  NUStmtLoop loop = stmt->loopBody();
  NUStmtList stmts(loop.begin(), loop.end());
  bool work_done = stmtList(stmts);
  if (work_done) {
    stmt->replaceBody(stmts);
  }

  return work_done;
}



void PackAssignsStats::print() const
{
  UtIO::cout() << "Pack Assigns Summary: " 
	       << mProceduralAssigns << " procedural assignments packed, "
	       << mContAssigns << " continuous assigns packed, affecting "
	       << mModules  << " modules." << UtIO::endl;
}
