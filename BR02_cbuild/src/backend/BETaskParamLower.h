// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/LowerPortConnections.h"
#include "nucleus/Nucleus.h"

class AllocAlias;

//! Class to lower parameters to task enables, if necessary
/*!
  This decides to lower task enable parameters if copy-in/copy-out is
  required for correctness or for compatibility. There are two cases:
  a task output also being passed in as an input and if a connection
  to an output parameter is a forcible or tristate.

  see test/langcov/TaskFunction/alias1.v
      test/force/forceTask*.v
*/
class BETaskParamLower : public LowerPortConnections
{
public:
  //! Constructor
  BETaskParamLower(AtomicCache* str_cache,
                   MsgContext* msgContext,
                   AllocAlias* allocAlias);

  //! virtual destructor
  virtual ~BETaskParamLower();

protected:
  //! Should the input connection be lowered?
  virtual bool doLowerInput(NUTFArgConnectionInput* in_conn);
  //! Should the output connection be lowered?
  virtual bool doLowerOutput(NUTFArgConnectionOutput* out_conn);
  //! Should the bidi connection be lowered?
  virtual bool doLowerBid(NUTFArgConnectionBid* bid_conn);
  //! Called before entering a taskenable structure.
  virtual void preCallTaskEnable(NUTaskEnable* call);

private:
  NUNetSet* mCurrentInputNets;
  AllocAlias* mAllocAlias;
};

//! Callback for lowering taskenable parameters
class BELowerCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  //! Constructor
  /*!
   * \param netRefFactory The unelaborated netref factory.
   * \param args Argument processor.
   * \param str_cache The string cache.
   * \param msg_context The message context.
   * \param iodb IODB object.
   */
  BELowerCallback(NUNetRefFactory *netref_factory,
                  MsgContext *msg_context,
                  AllocAlias* allocAlias,
                  AtomicCache *string_cache,
                  IODBNucleus *iodb,
                  ArgProc* args);
  
  //! Destructor
  virtual ~BELowerCallback();

  Status operator() (Phase /* phase */, NUBase * /* node */);

  //! We only care about taskenables in structured procs in a module
  virtual Status operator()(Phase phase, NUStructuredProc *node);

  //! We only care about nested task enables within tasks
  virtual Status operator()(Phase phase, NUTF *node);

private:
  //! Generic port-lowering interface
  BETaskParamLower * mLP;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;
  
  //! Msg context
  MsgContext *mMsgContext;

  //! String cache to use to create temps
  AtomicCache *mStringCache;

  //! IODB
  IODBNucleus *mIODB;

  //! Args
  ArgProc* mArgs;

};
