// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Insert conditional guards against shifts by out-of-bounds values in places
  where the hardware would do the wrong thing (e.g. shifting by a large number
  when the hardware only uses the low 5 bits of the shift count and those bits
  are zero.
*/

#ifndef _SHIFTGUARD_H_
#define _SHIFTGUARD_H_
#include "nucleus/Nucleus.h"
#include "nucleus/NUDesignWalker.h"

// Search for any use of a shift expression
class ShiftGuardCallback : public NUDesignCallback
{
public:
  ShiftGuardCallback () {}
  ~ShiftGuardCallback () {}
  //! Catch all
  Status operator()(Phase, NUBase*) { return eNormal;}

  //! Visit all expressions in every statement
  Status operator()(Phase, NUStmt* stmt);
};

#endif
