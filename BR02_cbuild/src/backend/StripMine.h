// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Walk the tree identifing stripminable statements.
*/
#ifndef __STRIPMINE_H__
#define __STRIPMINE_H__

#include "CGLower.h"

/*!
 * Walk the design looking for candiates for strip-mining vectors into
 * 32-bit operations.
 */

class StripMine : public virtual CGLower
{
public:
  StripMine (ArgProc* arg, AtomicCache* atomic, IODBNucleus* iodb, NUNetRefFactory* factory, MsgContext* msgCtxt, bool verbose);

  ~StripMine ();

  //! Gotta provide this one
  Status operator() (NUDesignCallback::Phase /* phase */, NUBase* /* node */);

  //! This is the interesting one
  Status operator() (NUDesignCallback::Phase phase , NUBlockingAssign* node);

  //! Another interesting case
  Status operator() (NUDesignCallback::Phase phase, NUContAssign* node);

  //! Need this too
  Status operator() (NUDesignCallback::Phase phase , NUStmt* node);

  //! Visit scopes to maintain scope stack (handled by CGLower)

  //! What to do with deleted statements
  NUStmt* replacement (NUStmt* stmt);

  //! Likewise for enabled drivers
  NUEnabledDriver* replacement (NUEnabledDriver* /*stmt*/) { return 0; }

  //! Likewise for TriRegInit
  NUTriRegInit* replacement (NUTriRegInit* /*stmt*/) { return 0; }

private:
  //! Make these unavailable
  StripMine ();
  StripMine (StripMine&);
  StripMine & operator =(const StripMine&);
};

#endif
