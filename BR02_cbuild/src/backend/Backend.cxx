// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUDesign.h"
#include "backend/Backend.h"
#include "reduce/AllocAlias.h"
#include "StripMine.h"
#include "LocalCSE.h"
#include "PackAssigns.h"
#include "BETaskParamLower.h"
#include "BETransform.h"
#include "PreCodeGen.h"
#include "ShiftGuard.h"
#include "HugeExprCrusher.h"
#include "util/ArgProc.h"
#include "util/Stats.h"
#include "compiler_driver/CarbonContext.h"
#include "localflow/Reorder.h"

#include "reduce/Fold.h"
#include "reduce/MemoryLoop.h"
#include "reduce/Inference.h"
#include "reduce/Ternary.h"

static const char* scPackAssignsStats = NULL;
static const char* scVerbosePackAssigns = NULL;
static const char* scNoPackAssigns = NULL;

static const char* scVerboseLocalCSE = NULL;
static const char* scVerboseTempMems = NULL;
static const char* scNoLocalCSE = NULL;
static const char* scLocalityReorder = NULL;
static const char* scNoLocalityReorder = NULL;
static const char* scNoBETransform = NULL;
static const char* scVerboseBETransform = NULL;
static const char* scNoBitOptimization = NULL;
static const char* scVerboseBitOptimization = NULL;
static const char* scNoHugeAssign = NULL;
static const char* scNoHugeExpr = NULL;
static const char* scEnableHugeAssign = NULL;
static const char* scVerboseHugeAssign = NULL;
static const char* scVerboseHugeExpr = NULL;
static const char* scHugeAssignThreshold = NULL;
static const char* scHugeExprThreshold = NULL;
static const char* scNoHugeAssignSplit = NULL;
static const char* scNoHugeExprSplit = NULL;
static const char* scSeparateExprSize = NULL;

Backend::Backend(NUNetRefFactory *netref_factory,
		 MsgContext *msg_context,
		 STSymbolTable *symtab,
		 AtomicCache *str_cache,
		 IODBNucleus *iodb,
		 ArgProc *arg) :
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mSymtab(symtab),
  mStringCache(str_cache),
  mIODB(iodb),
  mArg(arg),
  mAllocAlias(new ElabAllocAlias(AllocAlias::eMatchedPortSizes))
{
}

void Backend::setupOptions() {
  ArgProc* arg = mArg;

  scPackAssignsStats = CRYPT("-packAssignsStats");
  scVerbosePackAssigns = CRYPT("-verbosePackAssigns");
  scNoPackAssigns = CRYPT("-noPackAssigns");
  scVerboseLocalCSE = CRYPT("-verboseLocalCSE");
  scVerboseTempMems = CRYPT("-verboseTempMems");
  scNoLocalCSE = CRYPT("-noLocalCSE");
  scLocalityReorder = CRYPT("-localityReorder");
  scNoLocalityReorder = CRYPT("-noLocalityReorder");
  scNoBETransform = CRYPT("-noBETransform");
  scVerboseBETransform = CRYPT("-verboseBETransform");
  scNoBitOptimization = CRYPT("-noBitOptimization");
  scVerboseBitOptimization = CRYPT("-verboseBitOptimization");
  scNoHugeAssign = CRYPT("-noHugeAssign");
  scNoHugeExpr = CRYPT("-noHugeExpr");
  scVerboseHugeAssign = CRYPT("-verboseHugeAssign");
  scVerboseHugeExpr = CRYPT("-verboseHugeExpr");
  scHugeAssignThreshold = CRYPT("-hugeAssignSize");
  scHugeExprThreshold = CRYPT("-hugeExprSize");
  scNoHugeAssignSplit = CRYPT("-noHugeAssignSplit");
  scNoHugeExprSplit = CRYPT("-noHugeExprSplit");
  scSeparateExprSize = CRYPT("-separateExprSize");
  scEnableHugeAssign = CRYPT("-doHugeAssign");

  arg->addBool(scPackAssignsStats, CRYPT("Write statistics for PackAssigns"), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerbosePackAssigns, CRYPT("Be verbose about assignment packing"), false, CarbonContext::ePassCarbon);
  arg->addBool(scNoPackAssigns, CRYPT("Disable assignment packing"), false, CarbonContext::ePassCarbon);

  arg->addBool(scVerboseLocalCSE, CRYPT("Be verbose about local CSE analysis"),
               false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseTempMems, CRYPT("Be verbose about temp memory analysis"),
               false, CarbonContext::ePassCarbon);
  arg->addBool(scNoLocalCSE, CRYPT("Disable local CSE analysis"), false, CarbonContext::ePassCarbon);
  
  arg->addBool(scLocalityReorder, CRYPT("Reorder blocks in a more locality-friendly order."), true, CarbonContext::ePassCarbon);
  arg->addBoolOverride(scNoLocalityReorder, scLocalityReorder, "Disable locality reordering.");
  arg->addBool(scNoBETransform, CRYPT("Disable Backend Transform/Optimization."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseBETransform, CRYPT("Be verbose about backend transform"), false, CarbonContext::ePassCarbon);
  arg->addBool(scNoBitOptimization, 
	       CRYPT("Disable bit operations optimization"), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseBitOptimization, 
	       CRYPT("Be verbose about bit operation optimization"), false, CarbonContext::ePassCarbon);

  arg->addBool(scEnableHugeAssign, CRYPT("Enable huge assignment splitting"), true, CarbonContext::ePassCarbon);
  arg->addBoolOverride (scNoHugeExpr, scEnableHugeAssign, CRYPT("Disable the late splitting of huge expressions"));
  arg->addSynonym(scNoHugeExpr, scNoHugeAssign);
  arg->addBool(scVerboseHugeExpr, CRYPT("Be noisy when crushing huge expressions"), false, CarbonContext::ePassCarbon);
  arg->addSynonym(scVerboseHugeExpr, scVerboseHugeAssign);
  arg->addBool(scNoHugeExprSplit, CRYPT("Do not split the huge expressions, but still emit the assignments that have them in separate method"), false, CarbonContext::ePassCarbon);
  arg->addSynonym(scNoHugeExprSplit, scNoHugeAssignSplit);
  arg->addInt(scHugeExprThreshold, CRYPT("If the number of nodes in an expression exceeds this value, then it is broken down into smaller pieces."), HugeExprCrusher::cDEFAULT_CRUSH_THRESHOLD, false, false, CarbonContext::ePassCarbon);
  arg->addSynonym(scHugeExprThreshold, scHugeAssignThreshold);
  arg->addInt(scSeparateExprSize, CRYPT("If the number of nodes in an expression exceeds this value, then the it is separately evaluated into a temporary and the temporary is placed in it's place in the statement."), HugeExprCrusher::cDEFAULT_SEPARATE_THRESHOLD, false, false, CarbonContext::ePassCarbon);
} // void Backend::setupOptions

Backend::~Backend()
{
  delete mAllocAlias;
}


void Backend::analyzeDesign(NUDesign *design, bool phase_stats, Stats *stats)
{
  {
    // Compute allocation alias sets
    ElabAllocAlias allocAlias(AllocAlias::eMatchedPortSizes);
    allocAlias.compute(design, mSymtab);
    if (phase_stats) {
      stats->printIntervalStatistics(CRYPT("AllocAlias"));
    }
    
    BELowerCallback lowerCB(mNetRefFactory, mMsgContext, &allocAlias, mStringCache, mIODB, mArg);
    NUDesignWalker lowerWalk(lowerCB, false, true);
    lowerWalk.design(design);
    
    if (phase_stats) {
      stats->printIntervalStatistics(CRYPT("TaskParamLower"));
    }
  }

  // PreCodeGen lowers concats and other hard-to-codegen statement types (eventually including
  // drive-strength-related assignments).
  {
    bool verbose = mArg->getBoolValue (CRYPT ("-verbosePreCodeGen"));
    PreCodeGen precode (mArg, mStringCache, mIODB, mNetRefFactory, mMsgContext,
                        verbose);
    NUDesignWalker  walker(precode, verbose, true);
    walker.putWalkReplacements (true);   // Might have further lowerings after a replacement

    walker.design (design);
    
    if (phase_stats) {
      stats->printIntervalStatistics (CRYPT ("PreCodeGen"));
    }
  }

  // Stripmining breaks bitvector operations into loops over 32-bit
  // accesses.  This avoids creating temporaries and generates aligned writes
  // to the destination.
  //
  bool disable = mArg->getBoolValue (CRYPT("-noStripMine"));
  if (not disable) {
    // Strip mine assignment statements.
    bool verbose = mArg->getBoolValue (CRYPT ("-verboseStripMine"));
    StripMine strip (mArg, mStringCache, mIODB, mNetRefFactory, mMsgContext, verbose);
    NUDesignWalker stripWalk (strip, false, true);

    stripWalk.design (design);
    if (phase_stats) {
      stats->printIntervalStatistics (CRYPT ("StripMine"));
    }
  }

  // Fold expression trees and turn on optimizations that confuse UD
  // Make local copy so that it's destroyed on exit from the block, thus
  // causing any -traceFold output to be generated even if we don't
  // cleanup all destructors.
  if (not mArg->getBoolValue (CRYPT("-nofold")))
  {
    Fold folder(mNetRefFactory,
                mArg,
                mMsgContext, mStringCache,
                mIODB,
                mArg->getBoolValue (CRYPT("-verboseFold")),
                eFoldUDValid | eFoldUDKiller | eFoldComputeUD |
                eFoldIgnoreXZ | eFoldAggressive | eFoldBDDMux |
                eFoldNoStmtDelete );

    folder.design(design);
    if (phase_stats) {
      stats->printIntervalStatistics(CRYPT("FoldDesign"));
    }
  }


  bool do_ternary_creation = (not mArg->getBoolValue(CRYPT("-noTernaryCreation")));
  if (do_ternary_creation) {
    // This is meant to transform remaining single-def if-the-else
    // statements into ?: assignments. It occurs after Fold because we
    // still want to extract constant-assigns out of if-then-else
    // statements.

    bool verbose_ternary_creation = mArg->getBoolValue(CRYPT("-verboseTernaryCreation"));
    bool verbose_inference = mArg->getBoolValue(CRYPT("-verboseInference"));
    TernaryStatistics ternary_statistics;
    InferenceStatistics inference_statistics;
    Ternary ternary_creation(mStringCache,
                             mNetRefFactory,
                             mMsgContext,
                             mIODB,
                             mArg,
                             false, // allow non-bitsel conditions
                             true, // allow balanced conditionals.
                             true, // allow bitsel definitions.
                             false, // fold results.
                             verbose_ternary_creation,
                             &ternary_statistics,
                             &inference_statistics);
    ternary_creation.design(design);
    if (verbose_ternary_creation) {
      ternary_statistics.print();
    }
    if (verbose_inference) {
      inference_statistics.print();
    }
  }

  if (not mArg->getBoolValue (CRYPT("-nofold")))
  {
    Fold folder(mNetRefFactory,
                mArg,
                mMsgContext, mStringCache,
                mIODB,
                mArg->getBoolValue (CRYPT("-verboseFold")),
                eFoldUDValid | eFoldUDKiller | eFoldComputeUD |
                eFoldIgnoreXZ | eFoldAggressive | eFoldBDDMux |
                eFoldNoStmtDelete );
    folder.design(design);
    if (phase_stats) {
      stats->printIntervalStatistics(CRYPT("FoldDesign"));
    }
  }

  // Compute allocation alias sets.  See bug4460.  This must be done
  // prior to any reordering operation.
  {
    mAllocAlias->compute(design, mSymtab);
    if (phase_stats) {
      stats->printIntervalStatistics(CRYPT("AllocAlias"));
    }
  }

  if (not mArg->getBoolValue(scNoPackAssigns)) {
    // Reorder for optimal packing.
    // Aids assignment packing which only combines adjacent assigns.
    // Also makes packing more deterministic so test/fold/relop.v does
    // not have to be regolded so often.
    Reorder reorder(mStringCache, mNetRefFactory, mIODB, mArg,
                    mMsgContext, mAllocAlias, true, false, false, false);
    reorder.design(design);
    if ( phase_stats )
      stats->printIntervalStatistics("ReorderForPackAssigns");

    Fold folder(mNetRefFactory,
                mArg,
                mMsgContext, mStringCache,
                mIODB,
                mArg->getBoolValue (CRYPT("-verboseFold")),
                eFoldUDValid | eFoldIgnoreXZ | eFoldAggressive |
                eFoldBDDMux | eFoldNoStmtDelete );

    // Assignment packing
    PackAssignsStats pa_stats;
    PackAssigns pack_ass( mNetRefFactory, mMsgContext,  mStringCache, 32,
                          mArg,
                          &folder,
                          &pa_stats);

    pack_ass.design( design );

    if( mArg->getBoolValue(scPackAssignsStats) ||
        mArg->getBoolValue(scVerbosePackAssigns) ) {
      pa_stats.print();
    }

    if( phase_stats ) {
      stats->printIntervalStatistics(CRYPT("PackAssigns"));
    }
  }

  // Fold the design prior to BETransform to optimize any generated
  // concats.
  if (not mArg->getBoolValue (CRYPT("-nofold")))
  {
    Fold folder(mNetRefFactory,
                mArg,
                mMsgContext, mStringCache,
                mIODB,
                mArg->getBoolValue (CRYPT("-verboseFold")),
                eFoldUDValid | eFoldUDKiller | eFoldComputeUD |
                eFoldIgnoreXZ | eFoldAggressive | eFoldBDDMux |
                eFoldNoStmtDelete );

    folder.design(design);
    if (phase_stats) {
      stats->printIntervalStatistics(CRYPT("FoldDesign"));
    }
  }

  // Reorder for locality
  // Interconnected assignments are grouped together.
  if (mArg->getBoolValue(scLocalityReorder)) {
    Reorder reorder(mStringCache, mNetRefFactory, mIODB, mArg,
                    mMsgContext, mAllocAlias, true, true, false, true);
    reorder.design(design);
    if ( phase_stats )
      stats->printIntervalStatistics("ReorderForLocality");
  }

  // Backend Transform
  if (not mArg->getBoolValue(scNoBETransform)) {
    BETransform be_transform( mNetRefFactory, 
			      mMsgContext,
			      mSymtab,  
			      mStringCache,
			      mIODB, 
			      mArg);

    be_transform.design(design);
    if (phase_stats) {
      stats->printIntervalStatistics(CRYPT("BETransform"));
    }

  }

  // Look for assignments that may induce code that blows out the C++ compiler.
  if (mArg->getBoolValue (scEnableHugeAssign)) {
    HugeExprCrusher::Options options (*mMsgContext);
    options.parse (*mArg);
    HugeExprCrusher crush (options, mArg, mStringCache, mIODB, mNetRefFactory, mMsgContext);
    crush (design);
    if (phase_stats) {
      stats->printIntervalStatistics(CRYPT("CrushHugeAssign"));
    }
  }

  // Local CSE analysis
  if (not mArg->getBoolValue(scNoLocalCSE)) {
    LocalCSE local_cse(mNetRefFactory, mMsgContext, mAllocAlias, mStringCache, mIODB,
                       mArg, mArg->getBoolValue(scVerboseLocalCSE));
    local_cse.design(design);
    if (phase_stats) {
      stats->printIntervalStatistics(CRYPT("LocalCSE"));
    }
  }

  // Determine which temp memories need to be generated as dynamic.
  {
    REMemoryLoop marker (mMsgContext);
    marker.design (design, mArg->getBoolValue(scVerboseTempMems));
    if (phase_stats)
      stats->printIntervalStatistics("REMemory");
  }

  // Fold the design before doing required codegen transforms such as inserting
  // guard expressions.
  if (not mArg->getBoolValue (CRYPT("-nofold")))
  {
    Fold folder(mNetRefFactory,
                mArg,
                mMsgContext, mStringCache,
                mIODB,
                mArg->getBoolValue (CRYPT("-verboseFold")),
                eFoldUDValid | eFoldUDKiller | eFoldComputeUD |
                eFoldIgnoreXZ | eFoldAggressive | eFoldBDDMux |
                eFoldNoStmtDelete );

    folder.design(design);
    if (phase_stats) {
      stats->printIntervalStatistics(CRYPT("FoldDesign"));
    }
  }

  // Also walk design inserting shift-guard expressions as required.
  ShiftGuardCallback shiftGuard;
  NUDesignWalker shiftWalker(shiftGuard, false);
  shiftWalker.design (design);
}
