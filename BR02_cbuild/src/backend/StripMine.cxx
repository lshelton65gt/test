// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Walk the tree identifying strip-able statements
*/
#include "StripMine.h"

#include "localflow/UD.h"

#include "nucleus/NUAssign.h"   //show that NUBlockingAssign is a NUStmt
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUNetSet.h"
#include "reduce/RETransform.h"

#include "flow/FLNode.h"

#include "backend/Stripper.h"

StripMine::StripMine (ArgProc* arg, AtomicCache* ac, IODBNucleus* iodb,
                    NUNetRefFactory* f, MsgContext* msgCtxt, bool verbose):
  CGLower (arg, ac, iodb, f, msgCtxt, verbose)
{
}

StripMine::~StripMine () {}

NUDesignCallback::Status StripMine::operator() (NUDesignCallback::Phase, NUBase*)
{
  return eNormal;
}

NUDesignCallback::Status StripMine::operator() (NUDesignCallback::Phase, NUStmt*)
{
  return eNormal;
}

NUDesignCallback::Status StripMine::operator() (NUDesignCallback::Phase phase, NUBlockingAssign* stmt)
{
  NUDesignCallback::Status status = eNormal;

  if (phase == ePre)
    return status;

  Stripper strip(mArg, mAtomicCache, mFactory, mContext, mScopeStack.top (), stmt);
  if (strip.isStrippable ())
    status = eDelete;

  mChanged = true;
  return status;
}

NUStmt*
StripMine::replacement (NUStmt* stmt)
{
  // if this is a blocking assign that can be stripmined, create the block and return
  // the replacement.
  
  Stripper strip (mArg, mAtomicCache, mFactory, mContext, mScopeStack.top (), stmt);
  NU_ASSERT (strip.isStrippable (), stmt);
  return strip.generate ();
}


NUDesignCallback::Status StripMine::operator() (NUDesignCallback::Phase phase, NUContAssign* cassign)
{
  if (phase == ePre)
    return eNormal;

  Stripper strip (mArg, mAtomicCache, mFactory, mContext, mScopeStack.top (), cassign);
  if (strip.isStrippable (cassign)) {
    // Create a new always block, fix up the flow nodes for the schedule and just
    // delete the existing continuous assign
    NUBlock* block = strip.generate ();
    
    // Convert the continuous assign to an always block, insert it into the appropriate
    // schedule.
    makeAlwaysBlock (cassign, block);

    // Delete the continuous assign - we've already replaced it with a new always block
    // in this module
    return eDelete;
  }
  
  return eNormal;
}
