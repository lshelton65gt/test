// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Transform vector expressions into piecewise operations.
*/

#include "backend/Stripper.h"

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUCost.h"

#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUTF.h"

#include "reduce/RewriteUtil.h"

#include "util/ArgProc.h"

#include "compiler_driver/CarbonContext.h" // defines CRYPT, et al.

static const UInt32 cDefaultStripWidth = 64;
static const UInt32 cMinimalStripWidth = 32+1;
static const UInt32 cMaximalStripWidth = 65536;
static const SInt32 cMaxUnrollWordCount = 3;

#undef LONG_BIT
#undef LLONG_BIT
static const UInt32 LLONG_BIT = 64;
static const UInt32 LONG_BIT = 32;

// Look for indexing expression that's a word index (e.g. of the form
//              EXPR * (32*W)           => return EXPR and W
//              (EXPR * W*32) + X*W*32  => return (EXPR + X) and W
//              EXPR << S               => return EXPR and 1<<(S-5)
//              EXPR ? (32*W) : 0       => return EXPR and W
//              EXPR ? 0 : (32*W)       => return ~EXPR and W
//
// where W or S are constant and S is greater than or equal to 5.
//
// Return the non-constant EXPR value to use as an index and an integral
// multiplier W, with an addition term X to be added to EXPR
// Return NULL if no factorable index expression was found.

NUExpr*
Stripper::factorIndexExpr (const NUExpr* indexExpr, UInt32* w)
{
  CopyContext cc(NULL, NULL);
  if (indexExpr->getBitSize () > LONG_BIT)
    // Don't mess with really big non-constant expressions
    return NULL;

  if (const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(indexExpr)) {
    switch (bop->getOp ()) {
    case NUOp::eBiSMult:
    {
      if (const NUConst*c = bop->getArg (1)->castConst ()) {
        SInt64 tempK;
        if (c->getLL (&tempK) && (tempK >= LONG_BIT) && ((tempK % LONG_BIT) == 0)) {
          *w = tempK / LONG_BIT;
          return bop->getArg (0)->copy(cc);
        }
      }
      return NULL;
    }

    case NUOp::eBiUMult:
    {
      if (const NUConst*c = bop->getArg (1)->castConst ()) {
        UInt64 tempK;
        if (c->getULL (&tempK) && (tempK >= LONG_BIT) && (tempK % LONG_BIT) == 0) {
          *w = tempK / LONG_BIT;
          return bop->getArg (0)->copy(cc);
        }
      }
      return NULL;
    }

#if 0
    case NUOp::eBiPlus:
    {
      // This special-case takes care of a very nice targeted performance
      // test in test/codegen/bug5506.v.  Arithmetic folding will change
      // the "traditional" stripmining of pattern ((i + 3) * 32) into
      // (i*32 + 96).  We need to be able to extract out the 3 into i,w=1,x=3
      // More generally, we also need to be able to pull the constant
      // factor 2 from (i*64 + 192), also yielding i,w=2,x=3.  Otherwise there
      // is way more code generated and test/codegen/bug5506 fails 

      if (const NUConst* x_w_32 = bop->getArg(1)->castConst ()) {
        const NUBinaryOp* op = dynamic_cast<const NUBinaryOp*>(bop->getArg(0));
        if ((op != NULL) && (op->getOp() == NUOp::eBiUMult)) {
          UInt64 w32, xw32;
          const NUConst* w_32 = op->getArg(1)->castConst();
          if ((w_32 != NULL) &&
              x_w_32->getULL(&xw32) &&
              w_32->getULL(&w32) &&
              (w32 >= LONG_BIT) && ((w32 % LONG_BIT) == 0) &&
              (xw32 >= w32) && ((xw32 % w32) == 0))
          {
            UInt64 x = xw32 / w32;
            *w = w32 / LONG_BIT;
            CopyContext cc(NULL, NULL);
            NUExpr* expr = bop->getArg(0)->copy(cc);
            const SourceLocator& loc = expr->getLoc();
            UInt32 sz = expr->getBitSize();
            NUExpr* x_expr = NUConst::create(false, x, sz, loc);
            expr = new NUBinaryOp(NUOp::eBiPlus, expr, x_expr, loc);
            expr->resize(sz);
            em->putExpr(expr);
            return expr;
          }
        }
      }
      return NULL;
    }
#endif

    case NUOp::eBiLshift:
    {
      if (const NUConst*c = bop->getArg (1)->castConst ()) {
        UInt64 tempK;
        if (c->getULL (&tempK) && tempK >= 5) { // a multiple of 32
          *w = 1 << (tempK - 5);
          return bop->getArg (0)->copy(cc);
        }
      }
      return NULL;
    }

    default:
      return NULL;
    }
  } else if (const NUTernaryOp* top = dynamic_cast<const NUTernaryOp*>(indexExpr)) {
    //
    /// The "factor" function would need to return a NEW piece of tree "!e"
    //
    if (top->getOp () == NUOp::eTeCond) {
      const NUConst *lc = top->getArg (1)->castConstNoXZ ();
      const NUConst *rc = top->getArg (2)->castConstNoXZ ();

      if (lc && rc) {
        if (rc->isZero ()) {
          SInt64 tempK;
          if (lc->getLL (&tempK) && (tempK >= LONG_BIT) && (tempK % LONG_BIT) == 0) {
            *w = tempK / LONG_BIT;
            return top->getArg (0)->copy(cc);
          }
        }
        if (lc->isZero ()) {
          SInt64 tempK;
          if (rc->getLL (&tempK) && (tempK >= LONG_BIT) && ((tempK % LONG_BIT) == 0)) {
            *w = tempK / LONG_BIT;
            NUExpr* cond = top->getArg (0)->copy(cc);
            cond = new NUUnaryOp(NUOp::eUnLogNot, cond, cond->getLoc());
            cond->resize(1);
            return cond;
          }
        }
      }
    }
  }
  
  return NULL;
} // Stripper::factorIndexExpr


//! Does the destination overlap with the source expression values?

bool Stripper::destOverlapsSrc () const
{
  NUNetRefSet defs (mFactory);
  NUNetRefSet uses (mFactory);	
  const NUContAssign *ca = NULL;
  const NUBlockingAssign *ba = NULL;

  if ((ba = dynamic_cast<const NUBlockingAssign*>(mStmt))) {
    ba->getBlockingDefs (&defs);
    ba->getBlockingUses (&uses);
  } else if ((ca = dynamic_cast<const NUContAssign*>(mStmt))) {
    ca->getDefs (&defs);
    ca->getUses (&uses);
  } else {
    // nonblocking assign should never be seen, or ???
    NU_ASSERT ("Unexpected assign type" == 0, mStmt);
  }

  for(NUNetRefSet::iterator p = defs.begin (); p != defs.end (); ++p) {
    NUNetRefSet::const_iterator ause = uses.find (*p, &NUNetRef::overlapsSameNet);

    if (ause != uses.end ())
      if (*ause != *p)
        // There's an overlap and it's not 1:1
        return true;

    // At this point, we either have no overlaps, or they're 1:1 and
    // we've decided that's okay
  }

  // no dangerous overlap detected
  return false;
}

// Helper function to construct a simple assignment that's well-formed.
static NUBlockingAssign*
sGenAssign (NULvalue *dest, NUExpr *src, const SourceLocator& loc)
{
  NUBlockingAssign *asn = new NUBlockingAssign (dest, src, false, loc);
  asn->resize ();

  return asn;
}

/*!
 * Utility class to assist code generation of expression such as:
 *	aBit = ^(aVec | bVec)
 * into the equivalent pseudo code
 *
 *	tempBit = 0;
 *	for(i=0; i<width(avec); i+=32)
 *	  tempBit ^= ^(aVec.word[i/32] | bVec.work[i/32])
 *
 * For reduction & and |, we can short-circuit the loop if
 * our tempBit ever changes from the initial value.  E.g. ANY 0 in
 * and and-reduction sticks.
 */
class ReductionFramework{
  //! Expression to be reduced
  const NUExpr* mExpr;
  //! Reduction Operator
  NUOp::OpT mOp;
  //! TempBit accumulator
  NUNet * mTemp;
public:
  ReductionFramework (const NUExpr* e, NUOp::OpT op)
    : mExpr (e), mOp (op){}
  //! Original operator
  NUOp::OpT getOpcode () const { return mOp; }

  //! Binary operator used in reduced expr
  NUOp::OpT getReducedOpcode () const;

  //! Initial value depends on the reduction op
  NUConst* getInitialValue ();

  //! Reduction expression
  const NUExpr* getExpr () const {return mExpr;}

  //! get temp for accumulating reduction value
  NUNet *getTempNet () const {return mTemp;}

  //! set temp for later reference
  void putTempNet (NUNet* net) { mTemp = net; }

  //! Is there a short-circuit exit from the forloop?
  bool isShortCircuited () const { return mOp == NUOp::eUnRedAnd
				     || mOp == NUOp::eUnRedOr;}
};


NUOp::OpT ReductionFramework::getReducedOpcode () const
{
  switch(mOp){
  case NUOp::eUnRedAnd:	return NUOp::eBiBitAnd;
  case NUOp::eUnRedOr:	return NUOp::eBiBitOr;
  case NUOp::eUnRedXor: return NUOp::eBiBitXor;
    // Never got around to supporting these reductions....
    //  case NUOp::eUnRedNand: return NUOp::eBiBitNand;
    //  case NUOp::eUnRedNor: return NUOp::eBiBitNor;
    //  case NUOp::eUnRedXnor: return NUOp::eBiBitXnor;
  default:
    NU_ASSERT ("Unexpected ReducedOp" == 0, getExpr ());
    return NUOp::eUnBuf;
  }
}

NUConst* ReductionFramework::getInitialValue ()
{
  UInt32 val;
  switch (mOp) {
  default:
    NU_ASSERT ("Unexpected ReducedOp" == 0, getExpr ());
    // fall-thru

  case NUOp::eUnRedAnd:		// 1 & x => x
    val = 1;
    break;

  case NUOp::eUnRedXor:		// 0 ^ x => x
  case NUOp::eUnRedOr:		// 0 | x => x
    val = 0;
    break;
  }

  return NUConst::create (false, val, 1, mExpr->getLoc ());
}

//! Private class for extracting expressions and inserting temps.
/*!
 * Strip-mining doesn't want to see ?: or memory accesses - they should be
 * pre computed into a temporary that we can directly index. We don't want to
 * reevaulate complex conditional tests inside the strip-mine loop.  Eventually,
 * we might choose to keep simple ?: inside the loop and index into the then and else
 * expressions (assuming that they are simple enough.)
 */
class SubExpr : public NUDesignCallback
{
  //! The strip-miner
  Stripper *mStripper;

  //! a map of expression transforms
  NUExprExprDeepHashMap mExprMap;

  //! precompute and remember substitutions
  void precompute (NUExpr* e);

#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  //! construct the converter
  SubExpr (Stripper *sp) : NUDesignCallback (), mStripper (sp) {}

  ~SubExpr () {
    mExprMap.clearPointerValuesAndKeys ();
  }

  //! Visitor tranformational functions
  Status operator() (NUDesignCallback::Phase, NUBase*);

  Status operator() (NUDesignCallback::Phase, NUOp*);
  Status operator() (NUDesignCallback::Phase, NUMemselRvalue*);
  Status operator() (NUDesignCallback::Phase, NUIdentRvalue*);

  Status operator() (NUDesignCallback::Phase, NUStmt*);
};

// Insert an initial assignment to a temp and remember the mapping
void SubExpr::precompute
 (NUExpr* e)
{
  // Look to see if 'e' itself has already been precomputed and reuse the
  // temporary we already have.
  NUExprExprDeepHashMap::const_iterator cse = mExprMap.find (e);
  if (cse != mExprMap.end ())
    return;                     // Yup, all done now

  // Didn't already precompute this expression.  Create an assignment
  // in the strip-mining block of the form:
  //          tempNet = e;
  //
  // Transform any existing substitutions within 'e' to use previously
  // evaluated expressions.  This is effectively a limited CSE mechanism.
  REReplaceDeepExprExpr xformer (mExprMap);
  e->translate (xformer);

  NUExpr* value = mStripper->genTempAssign (e);
  CopyContext cc (0,0);

  // Set up the translate map with a copy of the expression we're replacing
  // and the tempNet as the replacement value.
  mExprMap.insert (NUExprExprDeepHashMap::value_type (e->copy (cc), value));
}

// Default action on callback
NUDesignCallback::Status SubExpr::operator() (NUDesignCallback::Phase phase, NUBase* )
{
  if (phase != ePre)
    return eSkip;
  return eNormal;
}

NUDesignCallback::Status SubExpr::operator() (NUDesignCallback::Phase phase, NUStmt* stmt)
{
  if (phase == ePre)
    return eNormal;

  // We've walked the whole statement - now replace any rewrites we accumulated on the
  // whole statement.
  REReplaceDeepExprExpr xformer (mExprMap);
  stmt->replaceLeaves (xformer);

  return eSkip;                 // Don't look at this statement again
}


NUDesignCallback::Status SubExpr::operator() (NUDesignCallback::Phase phase, NUOp*node)
{
  if (phase != ePre)
    return eSkip;

  switch (node->getType ()) {
  default:
    // Just visit the binary operands normally.
    return eNormal;
    
  case NUExpr::eNUConcatOp:
    precompute (node);
    return eSkip;

  case NUExpr::eNUUnaryOp: {
    NUUnaryOp* expr = dynamic_cast<NUUnaryOp*>(node);
    NUExpr* redExpr = expr->getArg (0);

    switch (expr->getOp ()) {
    case NUOp::eUnRedXor:
    case NUOp::eUnRedAnd:
    case NUOp::eUnRedOr:
      // This might be a top-level reduction, and we now know how to
      // code them directly.  See if that's the case.
      if (mStripper->isReduction ()
          && expr->getOp () == mStripper->getReductionOp ()) {
        if (redExpr->equal (*(mStripper->getReductionExpr ())))
          // This is a copy of the reduction expression we initially
          // identified as being strippable.
          return eNormal;
      }

      // Check that expression we're reducing is large enough to be
      // interesting.  We can reduce upto 64 bits easily.
      if (redExpr->getBitSize () <= LLONG_BIT)
        return eNormal;

      // Don't bother with a simple reduction of a primary - that would
      // introduce an infinite loop in simplification, and we've implemented
      // BVref and BitVector methods to do reductions
      switch (redExpr->getType ()) {
      default:
        // Something interesting - compute it into a temp and
        // reduce the temporary.
        precompute (node);
        return eSkip;


      case NUExpr::eNUIdentRvalue:
      case NUExpr::eNUVarselRvalue:
      case NUExpr::eNUMemselRvalue:
        // Easy to compute
        return eNormal;
      }  // end switch(redExpr->getType())

    default:
      return eNormal;
    }
    break;
  }

  case NUExpr::eNUTernaryOp: {
    // A ?: expression with a conditional test that's not cheap should
    // be evaluated once into a temporary and then that temporary should
    // be tested for the strip-ends and the strip body
    NUTernaryOp* te = dynamic_cast<NUTernaryOp*>(node);
    NUExpr* cond = te->getArg(0);

    //The cond expression itself is not strip-mined,
    //it is only used to select which expression will take part in the assignment.
    //The then/else expressions will be handled below. 
    //Here we mark the cond expression as a stopper for the strip-mine analysis.

    mStripper->addStripMineStopper(cond);

    NUCostContext cost_ctx;
    NUCost cost;

    // Is the conditional expression costly enough that we should
    // avoid computing it multiple times?
    cond->calcCost (&cost, &cost_ctx);

    if (cost.mInstructions > 3
        && mStripper->needsForLoop ()) {
      precompute (cond);
      
      //Since we only extracted the conditional expression, keep looking inside the
      // then and else-parts.
      return eNormal;
    }
    return eNormal;;
  } //endcase eNUTernaryOp

  } // endswitch
  
}


NUDesignCallback::Status SubExpr::operator() (NUDesignCallback::Phase phase,
                                              NUMemselRvalue *memsel)
{
  if (phase != ePre)
    return eSkip;

  // We move memory reference out of loop into a temp to avoid the cost of
  // memory addressing.  For normal memories, this is probably not terrible
  // costly, but for sparse memories it would be bad to recompute inside the
  // strip loop.
  precompute (memsel);
  return eSkip;
}

NUDesignCallback::Status SubExpr::operator() (NUDesignCallback::Phase phase, NUIdentRvalue* rv)
{
  if (phase != ePre)
    return eSkip;

  // zero-extended vector that's too short will need a temporary!
  NUNet* whole_identifier = rv->getWholeIdentifier();
  if (whole_identifier && whole_identifier->is2DAnything())
    return eNormal;          // never replace memeories bug 5638

  if (mStripper->needsForLoop ()
      && mStripper->checkWidth (rv->getBitSize ())
      && not mStripper->checkWidth (rv->getIdent ()->getBitSize ())) {
    precompute (rv);
    return eNormal;
  }

  return eNormal;
}

//! Private class for converting partselects into pre-and-post fragments
class StripEnds : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  //! Construct a converter for prefix or suffix
  /*!
   * \arg whichEnd - true for high bits, false for low.
   * \arg sp - pointer to Stripper and Block.
   */
  StripEnds (bool whichEnd, Stripper* sp) : NUDesignCallback (),
                                            mStripper (sp),
                                            mDoHighBits (whichEnd)
  {}

  ~StripEnds () {
    mExprMap.clearPointerValuesAndKeys ();
    mLvalMap.clearPointerValuesAndKeys ();
  }

  //! Required
  Status operator() (NUDesignCallback::Phase, NUBase*) {return eNormal;}

  //! The interesting overloads
  Status operator() (NUDesignCallback::Phase, NULvalue*);
  Status operator() (NUDesignCallback::Phase, NUConst*);
  Status operator() (NUDesignCallback::Phase, NUIdentRvalue*);
  Status operator() (NUDesignCallback::Phase, NUVarselRvalue*);
  Status operator() (NUDesignCallback::Phase, NUStmt*);

private:
  void putPieceLength (UInt32 w) const {
    if (mDoHighBits)
      mStripper->putSuffixLength (w);
    else
      mStripper->putPrefixLength (w);
  }

  //! Record the changes to be performed
  void record (NULvalue* oldval, NULvalue* newval) {
    NULvalLvalDeepHashMap::const_iterator cse = mLvalMap.find (oldval);
    if (cse != mLvalMap.end ()) {
      delete newval;
      return;                 // already in the mapl
    }

    CopyContext cc (0,0);
    mLvalMap.insert (NULvalLvalDeepHashMap::value_type (oldval->copy (cc),
                                                        newval));
  }

  //! overload for rhs
  void record (NUExpr* oldval, NUExpr* newval) {
    NUExprExprDeepHashMap::const_iterator cse = mExprMap.find (oldval);
    if (cse != mExprMap.end ()) {
      delete newval;
      return;                 // already in the mapl
    }

    CopyContext cc (0,0);
    mExprMap.insert (NUExprExprDeepHashMap::value_type (oldval->copy (cc),
                                                        newval));
  }


  /*!
   * Given the partselect of a range, return the partselect for the
   * unaligned bits at the top (mDoHighBits==true) or bottom.
   */
  ConstantRange getPiece(const ConstantRange &partsel) const;

  // Allow Stripend to access Stripper knowledge...
  Stripper* mStripper;

  //! expression tranforms
  NUExprExprDeepHashMap mExprMap;

  //! lvalue transforms
  NULvalLvalDeepHashMap mLvalMap;

  //! Are we doing the high-bit piece or the low bits?
  bool mDoHighBits;
};

//! Return the MSB:LSB for the end-piece for a prefix or suffix
ConstantRange StripEnds::getPiece (const ConstantRange &partsel) const
{
  // Compute the size of the chunk we need for the "low end"
  UInt32 endWidth = (LONG_BIT - mStripper->getOffset ()) % LONG_BIT;

  SInt32 msb, lsb;

  if (mDoHighBits) {
    // Width of remaining high chunk after moving K aligned pieces
    endWidth = (partsel.getLength () - endWidth) % LONG_BIT;

    msb = partsel.getMsb ();
    lsb = msb + 1 - endWidth;
  } else {
    lsb = partsel.getLsb ();
    msb = lsb + endWidth - 1;
  }

  // Return this range object, but don't let it be any larger than the input range so
  // that we correctly zero-extend the resulting chunk
  return ConstantRange(msb, lsb).overlap (partsel);
}

NUDesignCallback::Status StripEnds::operator() (NUDesignCallback::Phase phase, NULvalue *lv)
{
  if (phase != ePre)
    // Been here already
    return eNormal;

  NUVarselLvalue* result = NULL;

  switch (lv->getType ()) {
  default:
  case eNUMemselLvalue:
    NU_ASSERT ("Unexpected Lvalue type" == 0, lv); // Shouldn't be possible
    break;

  case eNUIdentLvalue:
    // Now if the offset was N bits, we want to construct either the
    // low  [31:N] or the high [32-N:K*32] fragment

  {
    NUIdentLvalue *iv = dynamic_cast<NUIdentLvalue *>(lv);
    NUNet *net = iv->getIdent ();

    if (NUVectorNet *vnet = net->castVectorNet ()) {
      ConstantRange r (getPiece (*vnet->getRange ()));

      result = new NUVarselLvalue (net, r, iv->getLoc ());
      result->putIndexInBounds (true);
      putPieceLength (r.getLength ());
    } else {
      // if this is anything other than a bitwise reduction, we're in trouble
      NU_ASSERT (mStripper->isReduction (), lv);
    }
    break;
  }

  case eNUVarselLvalue:
    // The only case we care about right now
  {
    NUVarselLvalue *ps = dynamic_cast<NUVarselLvalue*>(lv);
    ConstantRange r (getPiece (*ps->getRange ()));
    
    CopyContext cc (NULL, NULL);
    result = new NUVarselLvalue (ps->getLvalue()->copy(cc), ps->getIndex()->copy(cc),
                                 r, ps->getLoc());
    result->putIndexInBounds (true);
    putPieceLength (r.getLength ());
  }
  } // endswitch

  if (result) {
    record (lv, result);
    return eSkip;
  }

  return eNormal;
}

NUDesignCallback::Status StripEnds::operator() (NUDesignCallback::Phase phase, NUConst* c)
{
  if (phase != ePre)
    return eNormal;

  DynBitVector value;
  c->getSignedValue (&value);

  UInt32 piece;
  UInt32 pos=0, size=LONG_BIT;

  if (mDoHighBits) {
    pos = (mStripper->getWidth () - 1u) & - UInt32 (LONG_BIT);
    size =mStripper->getWidth () - pos;
  } else {
    pos = 0;
    size = LONG_BIT - mStripper->getOffset ();
  }

  piece = value.partsel (pos,size);

  NUExpr* ret = NUConst::create (false, piece, size, c->getLoc ());
  putPieceLength (size);

  record (c, ret);
  return eSkip;
}

NUDesignCallback::Status StripEnds::operator() (NUDesignCallback::Phase phase, NUIdentRvalue *iv)
{
  if (phase != ePre)
    return eNormal;

  NUNet *net = iv->getIdent ();
  if (NUVectorNet *vnet = net->castVectorNet ()) {
    const ConstantRange &r = *vnet->getRange ();
    ConstantRange piece = getPiece (r);

    NUVarselRvalue* ret = new NUVarselRvalue (net, piece, iv->getLoc ());
    ret->putIndexInBounds (true);
    ret->resize (piece.getLength ());
    record (iv, ret);
    return eSkip;

  }
  return eNormal;
}

NUDesignCallback::Status StripEnds::operator() (NUDesignCallback::Phase phase, NUVarselRvalue *ps)
{
  if (phase != ePre)
    return eNormal;

  ConstantRange r (getPiece (*ps->getRange ()));

  CopyContext cc (0,0);
  NUVarselRvalue *ret = new NUVarselRvalue (ps->getIdentExpr ()->copy (cc),
                                    ps->getIndex ()->copy (cc),
                                    r, ps->getLoc ());
  ret->putIndexInBounds (true);
  ret->resize (r.getLength ());
  record (ps, ret);
  return eSkip;
}

NUDesignCallback::Status StripEnds::operator() (NUDesignCallback::Phase phase, NUStmt* stmt)
{
  if (phase == ePre)
    return eNormal;

  REReplaceDeep xform (mExprMap, mLvalMap);
  stmt->replaceLeaves (xform);
  return eNormal;
}

Stripper::Stripper (ArgProc* arg, AtomicCache* ac,
                    NUNetRefFactory* f, MsgContext* msgCtxt, NUScope* scope, NUStmt* stmt):
  // Sets mOffset to invalid
  mStmt (stmt), mScope (scope), mArg (arg), mAtomic (ac), mFactory (f), msgContext (msgCtxt),
  mFlags (0), mOffset (LONG_BIT), mWidth (0),
  mPrefixLength (0), mSuffixLength (0),
  mBlock (0), mOpCount (0), mRed (0)
{
   mVerbose = mArg->getBoolValue (CRYPT ("-verboseStripMine"));

  mArg->getIntLast (CRYPT ("-stripMine"), (SInt32*)&mViableStripWidth);
  if (mViableStripWidth < cMinimalStripWidth
    || mViableStripWidth > cMaximalStripWidth) {
    msgContext->CGStripWidth (mViableStripWidth);
    mViableStripWidth = cDefaultStripWidth;
  }
}

Stripper::~Stripper ()
{
  delete mRed;
}

void
Stripper::addStripMineStopper(NUExpr* e) 
{
  mStripMineStopperSet.insert(e);
  //e->printVerilog(1);
}

bool
Stripper::isStripMineStopper(NUExpr* e) {

  NUExprSet::const_iterator it = mStripMineStopperSet.find (e);
  if (it != mStripMineStopperSet.end ()) {
    return true; // it is in the map
  }
  return false;  // it is not in the map
}


//! Private class for generating indexed partselects for stripmined loop body
class StripBody : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif
public:
  StripBody (Stripper* sp, NUNet* indexNet ) : NUDesignCallback (), mStripper (sp),
                                               mIndexNet (indexNet), mIndexConst(-1)
  {}

  StripBody (Stripper* sp, SInt32 i ) : NUDesignCallback (), mStripper (sp),
                                        mIndexNet(NULL), mIndexConst(i)
  {}

  ~StripBody () {
    mExprMap.clearPointerValuesAndKeys ();
    mLvalMap.clearPointerValuesAndKeys ();
  }

  //! Required
  Status operator() (NUDesignCallback::Phase, NUBase*) {return eNormal;}

  //! The interesting overloads
  Status operator() (NUDesignCallback::Phase, NUVarselLvalue*);
  Status operator() (NUDesignCallback::Phase, NUIdentLvalue*);
  Status operator() (NUDesignCallback::Phase, NUConst*);
  Status operator() (NUDesignCallback::Phase, NUIdentRvalue*);
  Status operator() (NUDesignCallback::Phase, NUVarselRvalue*);
  Status operator() (NUDesignCallback::Phase, NUStmt*);

  NUExpr* genIndex (SInt32 wordOff, UInt32 bitOff, const SourceLocator& loc);

private:
  //! Record the changes to be performed
  void record (NULvalue* oldval, NULvalue* newval) {
    NULvalLvalDeepHashMap::const_iterator cse = mLvalMap.find (oldval);
    if (cse != mLvalMap.end ()) {
      delete newval;
      return;                 // already in the mapl
    }

    CopyContext cc (0,0);
    mLvalMap.insert (NULvalLvalDeepHashMap::value_type (oldval->copy (cc),
                                                        newval));
  }

  //! overload for rhs
  void record (NUExpr* oldval, NUExpr* newval) {
    NUExprExprDeepHashMap::const_iterator cse = mExprMap.find (oldval);
    if (cse != mExprMap.end ()) {
      delete newval;
      return;                 // already in the mapl
    }

    CopyContext cc (0,0);
    mExprMap.insert (NUExprExprDeepHashMap::value_type (oldval->copy (cc),
                                                        newval));
  }

  //! Helper to create indexing expression for word access
  NUExpr* generateIndexExpr (const NUExpr* factoredIndex, UInt32 scaleK, SInt32 wordOff,
                             const ConstantRange& range,
                             const SourceLocator& loc);

  //! Create an indexed rvalue [31:0]
  NUExpr* generateWordAccess (NUExpr* base, const NUExpr* factoredIndex, UInt32 scaleK,
                              ConstantRange range, const SourceLocator& loc);
                              

  //! Overload - create an indexed lvalue[31:0]
  NUVarselLvalue* generateWordAccess ( NULvalue* base, const NUExpr* factoredIndex, UInt32 scaleK,
                                       ConstantRange range, const SourceLocator& loc);

  // Allow StripBody to access Stripper knowledge...
  Stripper* mStripper;
  NUNet* mIndexNet;             // Either mIndexNet is used (non-NULL) or 
  SInt32 mIndexConst;           // mIndexConst is used (!= -1).  They are mutex.

  NUExprExprDeepHashMap mExprMap;
  NULvalLvalDeepHashMap mLvalMap;
};

NUExpr* StripBody::genIndex(SInt32 wordOff, UInt32 bitOff,
                            const SourceLocator& loc)
{
  NUExpr* e = NULL;
  if (mIndexNet != NULL) {
    e = new NUIdentRvalue (mIndexNet, loc);

    if (wordOff || (bitOff / LONG_BIT)) {
      // Convert bit offset into word index
      wordOff += (bitOff / LONG_BIT);
      NUConst *off = NUConst::create (true, wordOff,  LONG_BIT, loc);
      e = new NUBinaryOp (NUOp::eBiPlus, e, off, loc);
    }
  }
  else {
    // Convert bit offset into word index
    wordOff += (bitOff / LONG_BIT) + mIndexConst;
    e = NUConst::create (true, wordOff,  LONG_BIT, loc);
  }

  e->resize (LONG_BIT);
  return e;
}


// Convert vector lvalue into indexed expression
NUDesignCallback::Status StripBody::operator() (NUDesignCallback::Phase phase, NUIdentLvalue *lvalue)
{
  if (phase != ePre)
    return eNormal;

  if (not mStripper->stripSize (lvalue->getBitSize ()))
    // Not a vector-sliceable reference
    return eNormal;
  
  NULvalLvalDeepHashMap::const_iterator cse = mLvalMap.find (lvalue);
  if (cse != mLvalMap.end ()) {
    return eSkip;               // already in the mapl
  }

  ConstantRange range (31,0);

  NUNet *net = lvalue->getIdent ();
  NUVectorNet *v = net->castVectorNet ();
  NU_ASSERT (v, lvalue);

  range = *v->getRange ();

  CopyContext cc (0,0);
  NULvalue* result = generateWordAccess (lvalue->copy (cc), 0, 0, range, lvalue->getLoc ());

  record (lvalue, result);
  return eSkip;
}

NUDesignCallback::Status StripBody::operator() (NUDesignCallback::Phase phase, NUVarselLvalue *varsel)
{
  if (phase != ePre)
    return eNormal;

  if (not mStripper->stripSize (varsel->getBitSize ()))
    // Not a vector-sliceable reference
    return eNormal;

  NULvalLvalDeepHashMap::const_iterator cse = mLvalMap.find (varsel);
  if (cse != mLvalMap.end ()) {
    return eSkip;               // already in the mapl
  }

  ConstantRange range (31,0);

  UInt32 scaleK = 1;

  NUExpr* factoredIndex  = NULL;
  if (not varsel->isConstIndex ())
    factoredIndex = mStripper->factorIndexExpr (varsel->getIndex (), &scaleK);

  range = *varsel->getRange ();

  // We only support constant indexed varsels or index values that are guaranteed to be word aligned
  NU_ASSERT (varsel->isConstIndex () || factoredIndex, varsel);

  // We will have aligned to the destination, so the main loop will
  // always have word indexed targets.
  NU_ASSERT (mStripper->isReduction ()
             || (mStripper->getOffset () == (SInt32)(range.getLsb () % LONG_BIT)),
             varsel);
  
  CopyContext cc (0,0);
  NULvalue* result = generateWordAccess (varsel->getLvalue ()->copy (cc), factoredIndex, scaleK, range,
                                         varsel->getLoc ());
  record (varsel, result);
  delete factoredIndex;
  return eSkip;
}

// Convert vector rvalue into indexed expression
NUDesignCallback::Status StripBody::operator() (NUDesignCallback::Phase phase,
                                                NUIdentRvalue* rv)
{
  if (phase != ePre || not mStripper->stripSize (rv->getBitSize ()))
    // Not a vector-sliceable reference
    return eNormal;

  NUExprExprDeepHashMap::const_iterator cse = mExprMap.find (rv);
  if (cse != mExprMap.end ()) {
    return eSkip;               // already in the map
  }

  NUNet *net = rv->getIdent ();
  NUVectorNet *v = net->castVectorNet ();
  if (not v)
    return eStop;               // Not indexable...

  ConstantRange range (*v->getRange ());
  CopyContext cc (0,0);

  NUExpr* wordAccess = generateWordAccess (rv->copy (cc), 0, 0, range, rv->getLoc ());
  record (rv, wordAccess);

  return eSkip;
}

// Convert constant rvalue into indexed expression
NUDesignCallback::Status StripBody::operator() (NUDesignCallback::Phase phase,
                                                NUConst* c)
{
  if (phase != ePre || not mStripper->stripSize (c->getBitSize ()))
    return eNormal;

  NUExprExprDeepHashMap::const_iterator cse = mExprMap.find (c);
  if (cse != mExprMap.end ()) {
    return eSkip;               // already in the map
  }

  ConstantRange range (31,0);
  range.adjust (mStripper->getOffset ());

  NUExpr* base;

  if (mStripper->getOffset () != 0) {
    // This large constant needs to be adjusted by the amount the
    // stripmine is misaligned with this literal value.
    //
    // consider:            (x[127:13] & ((115'b1<<115) | 1)) 
    //
    // we'll split this into a 19 bit prefix mask and compare to get
    // the first chunk, then we'll do 3 words.  Inside the loop we
    // need the constant shifted down by 19 bits.
    
    DynBitVector v;
    c->getSignedValue (&v);
    v >>= (32 - mStripper->getOffset ());
    base = NUConst::create (false, v, c->getBitSize (), c->getLoc ());
    base->resize (c->determineBitSize ());
  } else {
    CopyContext cc (0,0);
    base = c->copy (cc);
  }

  NUExpr* wordAccess = generateWordAccess (base, 0, 0, range, c->getLoc ());

  record (c, wordAccess);

  return eSkip;
}

// Convert vector rvalue into indexed expression
NUDesignCallback::Status StripBody::operator() (NUDesignCallback::Phase phase,
                                                NUVarselRvalue* varsel)
{
  if (phase != ePre)
    return eNormal;
  // Skip this varsel if it is a stopper  
  bool isStopper = mStripper->isStripMineStopper(varsel);
  if(isStopper) {
    return eSkip;
  }

  if (not mStripper->stripSize (varsel->getBitSize ()))
    // Not a vector-sliceable reference
    return eNormal;

  NUExprExprDeepHashMap::const_iterator cse = mExprMap.find (varsel);
  if (cse != mExprMap.end ()) {
    return eSkip;               // already in the map
  }

  ConstantRange range (*varsel->getRange ());
  if (range.getLength () == 1)
    return eNormal;

  UInt32 scaleK = 1;
  NUExpr* factoredIndex = mStripper->factorIndexExpr (varsel->getIndex (),
                                                      &scaleK);

  // Either indexing expression was constant, or it was a multiple of 32 and
  // non-constant.
  NU_ASSERT (varsel->isConstIndex () || factoredIndex, varsel);

  CopyContext cc (0,0);
  NUExpr* wordAccess = generateWordAccess (varsel->getIdentExpr ()->copy (cc),
                                           factoredIndex, scaleK,
                                           range,
                                           varsel->getLoc ());

  record (varsel, wordAccess);
  delete factoredIndex;
  return eSkip;
}

NUExpr*
StripBody::generateIndexExpr (const NUExpr* factoredIndexIn, UInt32 scaleK, SInt32 wordOff,
                              const ConstantRange &range, const SourceLocator& loc)
{
  NUExpr *idx = genIndex (wordOff, range.getLsb (), loc);

  CopyContext cc (0,0);
  if (factoredIndexIn) {
    NUExpr* factoredIndex = factoredIndexIn->copy (cc);
    if (scaleK > 1)
      factoredIndex = new NUBinaryOp (NUOp::eBiUMult, factoredIndex,
                                      NUConst::create (false, scaleK, 32, loc),
                                      loc);
    idx = new NUBinaryOp (NUOp::eBiPlus, idx, factoredIndex, loc);
  }

  // Now rescale as aligned bit index
  NUConst* k = idx->castConst();
  SInt32 val;
  if ((k == NULL) || !k->getL(&val)) {
    idx = new NUBinaryOp (NUOp::eBiUMult, idx,
                          NUConst::create (false, 32, 32, loc),
                          loc);
  }
  else {
    delete idx;
    idx = NUConst::create(false, 32*val, 32, loc);
  }
    
  return idx;
}

NUVarselLvalue*
StripBody::generateWordAccess (NULvalue* base, const NUExpr* factoredIndex, UInt32 scaleK,
                               ConstantRange range, const SourceLocator& loc)
{
  // We will have aligned to the destination, so the main loop will
  // always have word indexed targets.
  NU_ASSERT (mStripper->isReduction ()
             || (mStripper->getOffset () == (SInt32)(range.getLsb () % LONG_BIT)),
             base);

  NUExpr* idx = generateIndexExpr (factoredIndex, scaleK,
                                   mStripper->getPrefixLength () ? 1 : 0, // Skip prefix alignment
                                   range, loc);

  NUVarselLvalue* result = new NUVarselLvalue (base, idx, ConstantRange (31, 0), loc);
  result->putIndexInBounds (true);
  result->resize();
  return result;
}

NUExpr*
StripBody::generateWordAccess (NUExpr* base, const NUExpr* factoredIndex, UInt32 scaleK,
                               ConstantRange range, const SourceLocator& loc)
{
  // On the RHS, we might not be aligned.  Check if this offset differs from
  // the strip-mining offset and deal with that appropriately.  When accessing a
  // constant, we know it's aligned and needs no word-offset because we've made an
  // appropriately aligned copy of the constant
  SInt32 wordOff = base->castConst () ? 0 : (mStripper->getPrefixLength () + range.getLsb ()) / LONG_BIT;

  if (range.getLsb () == mStripper->getOffset ()) {
    // The easy case: source is aligned with destination.  We're inside the loop, so
    // we've aligned the accesses
    range = ConstantRange (31,0);
  } else {
    // The adjustment for access will be the amount we stripped off
    // the bottom end of the vector
    SInt32 delta = (mStripper->getPrefixLength () + range.getLsb ()) % LONG_BIT;
    range = ConstantRange (delta + LONG_BIT - 1, delta);
  }

  NUExpr* idx = generateIndexExpr (factoredIndex, scaleK, wordOff, range, loc);

  NUVarselRvalue *result = new NUVarselRvalue (base, idx, range, loc);
  result->putIndexInBounds (true);
  result->resize (LONG_BIT);
  return result;
}

// Convert vector assignment operands into indexed expression
NUDesignCallback::Status StripBody::operator() (NUDesignCallback::Phase phase,
                                                NUStmt* stmt)
{
  if (phase == ePre)
    return eNormal;

  // On the post fix run substitute the replacements we've identified.
  REReplaceDeep xform (mExprMap, mLvalMap);
  stmt->replaceLeaves (xform);
  return eNormal;
}

/*!
 * Effective width of expressions can either be word multiples or exact width.  This depends
 * greatly on whether we're dealing with part-selects or not.
 */
UInt32 Stripper::getWidth (void) const
{
  return isWholeVectorExpr () ? (mWidth + LONG_BIT - 1u) & - UInt32 (LONG_BIT)
			      : mWidth;
}

bool Stripper::needsSuffix () const
{
  // If we're doing unaligned references just handle 32-bit offsets
  if (isWholeVectorExpr ()	// Extra bits are zero or handled
      || needsTemporary ())
    return false;

  // Are there leftover bits to compute at the MSBs after we've
  // moved a bunch of aligned words?  We handle any bits that were
  // not handled in the prefix, and are not a multiple of 32
  return ((getWidth () - mPrefixLength) % LONG_BIT) != 0;
}

//!Can only strip vector segments that are the same size
bool Stripper::checkWidth (UInt32 width) const {
  if (mWidth == 0)
    mWidth = width;

  if (isWholeVectorExpr ())
    // Don't have to worry about pieces at end.  Just make sure we have
    // same number of longwords involved.
    return (mWidth + LONG_BIT - 1)/LONG_BIT == (width + LONG_BIT - 1)/LONG_BIT;
  else
    // Need identical sizes for part-selected pieces.
    return mWidth == width;
}

//! Have we established an initial offset already?
bool Stripper::anyOffset () const {
  return (mOffset != (SInt32)LONG_BIT);
}

//! All vectors must have the same relative offset.
bool Stripper::checkOffset (SInt32 offset) const {
  offset %= LONG_BIT;

  if (not anyOffset ())         // none set already?
    mOffset = offset;
    
  bool isAligned = (mOffset == offset);

  if (not isAligned)
    // Would have to use BVref to access components.
    annotate (eUnalignedOperands);

  // Accept arbitrary offsets, but require BVref's
  return true;
}

// Given the LSB of a partselect, renormalize it to account for any non-normal
// declaration of the net.
static UInt32
sNormalizeOffset (const NUNet* net, UInt32 offset)
{
  if (not net)
    return offset;

  const NUVectorNet* vnet = net->castVectorNet ();
  if (not vnet)
    return offset;

  const ConstantRange& declRange (*vnet->getDeclaredRange ());
  if (declRange.bigEndian ())
    return offset + declRange.getLsb ();
  else
    return declRange.getLsb () - offset;
}

// The vector is large enough to make stripping a good idea
bool Stripper::stripSize (UInt32 s) const
{
  return s >= mViableStripWidth;
}

/*!
 * Simple Design walk to identify if the statement or expression is strip-mineable.
 * Uses call-back status to indicate this:
 *
 *      eStop - not strippable
 *      eNormal - strippable
 */
class Strippable : public NUDesignCallback
{
  //! The strip-miner
  Stripper *mStripper;

#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  Strippable (Stripper* stripper) : NUDesignCallback (), mStripper (stripper) {}

  Status operator() (NUDesignCallback::Phase, NUBase*) {return eNormal;}

  Status operator() (NUDesignCallback::Phase, NUExpr*);
  Status operator() (NUDesignCallback::Phase, NUConst*);
  Status operator() (NUDesignCallback::Phase, NUIdentRvalue*);
  Status operator() (NUDesignCallback::Phase, NUVarselRvalue*);
  Status operator() (NUDesignCallback::Phase, NUMemselRvalue*);
  Status operator() (NUDesignCallback::Phase, NUOp*);

  Status operator() (NUDesignCallback::Phase, NUConcatLvalue*);
  Status operator() (NUDesignCallback::Phase, NUIdentLvalue*);
  Status operator() (NUDesignCallback::Phase, NUVarselLvalue*);
  Status operator() (NUDesignCallback::Phase, NUMemselLvalue*);
private:
  Status checkNet (NUNet*net);
};

NUDesignCallback::Status
Strippable::checkNet (NUNet* net)
{
  if (net->isForcible ())
    return eStop;

  // We don't checkWidth() for the underlying net - all we care about is
  // that it is a bitvector.  If it's too small, we'll create a temporary
  // that's extended.
  //

  UInt32 netWidth = 0;
  if (const NUVectorNet* vn = net->castVectorNet())
    netWidth = vn->getBitSize ();
  else if (const NUMemoryNet* mn = net->getMemoryNet ())
    netWidth = mn->getRowSize ();


  if (netWidth > 64)
    return eNormal;
  else
    return eStop;
}

// Look thru expression trees to be sure that this is strippable.
//
// Normally we'll preevaluate these
NUDesignCallback::Status
Strippable::operator() (NUDesignCallback::Phase phase, NUMemselRvalue* mv)
{
  if (phase != ePre)
    return eSkip;

  // TODO - is checkOffset necessary since we precompute?
  if (mStripper->checkOffset (0)) {
    switch (this->operator()(phase, mv->getIdent ()))
    {
    case eNormal:
    case eSkip:
      return eSkip;

    case eStop:
      return eStop;
      
    case eDelete:
    default:
      NU_ASSERT (0, mv);
      return eStop;
    }

  } else {
    return eStop;
  }
}

// BUG2772 - can't strip small constants. we generate really
// stupid slow code if we hoist the constant out of the loop.  So,
// for now just reject the constant if it's not guaranteed to
// require at least as many bits as the stripmine's width.
NUDesignCallback::Status
Strippable::operator() (NUDesignCallback::Phase phase, NUConst* c)
{
  if (phase != ePre)
    return eNormal;

  if (not mStripper->checkWidth (c->determineBitSize ()))
    return eStop;

  // TODO do we care about offset?
  if (mStripper->checkOffset (0))
    return eNormal;

  return eStop;
}

NUDesignCallback::Status
Strippable::operator() (NUDesignCallback::Phase phase, NUIdentRvalue* id)
{
  if (phase != ePre)
    return eNormal;

  UInt32 selfSize = id->determineBitSize ();
  if (id->getBitSize () > selfSize
      && id->isSignedResult ()  // cf bug5558
      && mStripper->getWidth () > selfSize)
    // need more bits than present and might be sign bits.  If input BVs were properly
    // sign-extended in their excess bits, we could strip this as long as we didn't go
    // out-of-bounds on the word.
    return eStop;
  else if (selfSize <= cDefaultStripWidth)
    // Can't stripmine non-bitvector POD data types
    return eStop;

  if (mStripper->checkOffset (0))
    return eNormal;

  return eStop;
}

bool Stripper::isFactorable(const NUExpr* expr) {
  UInt32 dummyk;
  NUExpr* factor = Stripper::factorIndexExpr(expr, &dummyk);
  bool is_factorable = factor != NULL;
  if (factor != NULL) {
    delete factor;
  }
  return is_factorable;
}

NUDesignCallback::Status
Strippable::operator() (NUDesignCallback::Phase phase, NUVarselRvalue* ps)
{
  if (phase != ePre)
    return eNormal;

  bool is_factorable = Stripper::isFactorable(ps->getIndex());
  ConstantRange r (*ps->getRange ());
  if (r.getLength () == 1) // Don't bother with bit-selects...
    return eStop;
  else if (not ps->isConstIndex () && not is_factorable)
    // Can't deal with variable bit position when generating the strip unless it's
    //  (x*32), so that we know its word-alignment.
    return eStop;

  if (not mStripper->checkWidth (r.getLength ()))
    // Can't strip unless all the values are the same size...
    return eStop;

  // Don't try stripping the index expression to the varsel
  if (not ps->isWholeIdentifier ())
    mStripper->annotate (eHasPartsel);

  UInt32 lowOffset = r.getLsb ();

    if (is_factorable && ps->getIdentExpr ()->isWholeIdentifier ()) {
      // Because the constant delta to the indexing expression for a net
      // with non-normal bounds will be moved into the ConstantRange, we
      // have to account for that when determining if this is unaligned.
      lowOffset = sNormalizeOffset (ps->getIdent (), lowOffset);
    }

  if (not mStripper->checkOffset (lowOffset)
      or not mStripper->checkWidth (r.getLength ())
      or not ps->getIdentExpr ()->isWholeIdentifier ())
    return eStop;

  // Walk into the base address, but skip the indexing expression, as it
  // doesn't participate in the stripability as long as it's a multiple
  // of 32
  switch(this->operator()(phase, ps->getIdent ()))
  {
  case eNormal:
  case eSkip:
    return eSkip;               // okay but don't visit the index expression
  case eStop:
    return eStop;
  case eDelete:
  case eInvalid:
  default:
    return eStop;
  }
}

NUDesignCallback::Status
Strippable::operator() (NUDesignCallback::Phase phase, NUOp* expr)
{
  if (phase != ePre)
    return eNormal;

  if (not mStripper->checkWidth (expr->getBitSize ()))
    // 
    return eStop;

  switch (expr->getOp ())
  {
  default:
    // dislike anything we don't explicitly know
    return eStop;

  case NUOp::eNaConcat:
    // We code-motion these and pre-evaluate into a temp.
    return (mStripper->checkOffset (0) && expr->getBitSize () > LLONG_BIT) ? eSkip : eStop;

  case NUOp::eUnBitNeg:
  case NUOp::eUnVhdlNot:
    mStripper->countOperation (2);
    return eNormal;

  case NUOp::eBiBitAnd:
  case NUOp::eBiBitOr:
  case NUOp::eBiBitXor:
    mStripper->countOperation ();
    return eNormal;             // Let walk visit operand nodes
	      
  case NUOp::eTeCond:
    // We are going to eval the ?: into a temporary; That's got to be
    // indexable too, so reject POD-sized slices here
    //
    if (expr->getBitSize () <= LLONG_BIT)
      return eStop;

    // Don't walk the conditional expression for strip-mining,
    // just explicitly walk the THEN and ELSE parts.
    NUDesignWalker walker (*this, false, false);
    Status subStat = walker.expr(expr->getArg (1));
    if (subStat == eStop)
      return eStop;             // not strippable

    subStat = walker.expr (expr->getArg (2));
    if (subStat == eStop)
      return eStop;             // not strippable

    return eSkip;               // Don't look into cond expression
  }
}

NUDesignCallback::Status
Strippable::operator() (NUDesignCallback::Phase phase, NUExpr*)
{
  if (phase != ePre)
    return eSkip;

  return eStop;                 // Arbitrary expressions aren't handled
}

// Evaluate LHS terms for strippability.  
NUDesignCallback::Status
Strippable::operator() (NUDesignCallback::Phase phase, NUIdentLvalue* id)
{
  if (phase != ePre)
    return eNormal;

  if (not mStripper->anyOffset ())
    mStripper->checkOffset (0);

  return checkNet (id->getIdent ());
}

NUDesignCallback::Status
Strippable::operator() (NUDesignCallback::Phase phase, NUMemselLvalue* )
{
  if (phase != ePre)
    return eNormal;

  if (not mStripper->anyOffset ())
    mStripper->checkOffset (0);
  mStripper->annotate (eNeedsTemporary); // Need a temp inside the loop

  return eSkip;
}

NUDesignCallback::Status
Strippable::operator() (NUDesignCallback::Phase phase, NUVarselLvalue* ps)
{
  if (phase != ePre)
    return eNormal;

  NUNet* net = ps->getIdentNet (false);
  bool is_factorable = Stripper::isFactorable (ps->getIndex ());

  if (net == 0 || ps->getLvalue ()->getType () == eNUMemselLvalue)
    // Futile to strip this for now; with additional work we might be able to strip
    // costly RHS expressions to a temporary and then just assign the temp to the
    // memory varsel, doing something like:
    //         temp = mem[k];
    //         temp[H:L] = RHS;
    //         mem[k] = temp;
    // or 
    //         BV<K> temp&(mem[k]);
    // and then stripmine the temp = RHS, which writes directly into the
    // memory location, but doesn't reinvoke Memory::write()
    //
    return eStop;
  else if (not ps->isConstIndex () && not is_factorable)
    return eStop;

  // If there were no partsels, we can easily handle random sized
  // bitvectors because the underlying storage is word-multiple...
  //
  if (mStripper->isWholeVectorExpr ())
    mStripper->annotate (eHasPartsel);
  else
    // Only happens if we had a varsel above this and didn't have constant
    // index that was folded flat.
    NU_ASSERT (0, ps);

  // We prefer to use the destination offset.
  ConstantRange r (*ps->getRange ());
  UInt32 normalOffset = r.getLsb ();

  if (is_factorable && ps->getLvalue ()->isWholeIdentifier ()) {
    // Because the constant delta to the indexing expression for a net
    // with non-normal bounds will be moved into the ConstantRange, we
    // have to account for that when determining if this is unaligned.
    normalOffset = sNormalizeOffset (ps->getIdentNet (), normalOffset);
  }

  mStripper->checkOffset (normalOffset);

  // heck for underlying varsel or memsel -- it might be proscribed, or
  // require a temporary.  We've already set the offset, so any ident or
  // memory below won't confuse us.
  //
  NULvalue *lval = ps->getLvalue ();
  NUDesignWalker walker (*this, false, false);

  // We walk the varsel LVALUE, but will skip over the indexing expression because
  // it's not something subject to stripping.
  //
  switch (walker.lvalue (lval))
  {
  case eNormal:
  case eSkip:
    return eSkip;               // We liked the base expression
  case eStop:
    return eStop;               // The base expression isn't strippable
  case eDelete:
  default:
    NU_ASSERT (0, ps);
    return eStop;
  }
}

NUDesignCallback::Status
Strippable::operator() (NUDesignCallback::Phase, NUConcatLvalue* )
{
  return eStop;
}

bool
Stripper::isStrippable (NUContAssign *cassign)
{
  if (cassign->getStrength () == eStrWeakest)
    return false;               // Can't stripmine pull drivers yet.

  NUNetSet defs;
  cassign->getDefs (&defs);
  for(Loop<NUNetSet> d = defs.loopUnsorted (); !d.atEnd (); ++d) {
    NUNet* n = *d;
    if (n->isPrimaryZ () || n->isZ ())
      // Also reject any tristated net.
      return false;
  }

  return isStrippable (static_cast<NUAssign*>(cassign));
}

const NUExpr* Stripper::getReductionExpr () const
{
  return mRed->getExpr ();
}

//! Return the Unary reduction operator (e.g. eUnRedAnd, not eBiBitAnd)
NUOp::OpT Stripper::getReductionOp () const
{
  return mRed->getOpcode ();
}

// Look for a unary reduction over a strippable expression.
bool Stripper::checkReduction (const NUExpr* e)
{
  if (e->getType () != NUExpr::eNUUnaryOp)
    return false;

  const NUUnaryOp *unary = dynamic_cast<const NUUnaryOp*>(e);
  switch (unary->getOp ()) {
  case NUOp::eUnRedAnd:
  case NUOp::eUnRedOr:
  case NUOp::eUnRedXor:
    //  case NUOp::eUnRedNand:
    //  case NUOp::eUnRedNor: 
    //  case NUOp::eUnRedXnor:
    break;

  default:
    return false;
  }

  // Now check if the underlying expression is strippable, is something more
  // complex than a primary operand and is a bitvector.  E.g., we
  // want to handle something like:
  //    | (a & b)
  // but not
  //    | a[127:0]
  //
  Strippable reduction (this);
  NUDesignWalker walker (reduction, false, false);

  UInt32 reductionSize = unary->getArg (0)->getBitSize ();
  if (stripSize (reductionSize)
      && checkWidth (reductionSize)
      && dynamic_cast<const NUOp*>(unary->getArg (0))
      && walker.expr(const_cast<NUExpr*>(unary->getArg (0))) == NUDesignCallback::eNormal)
  {
    // Can't already be a reduction (we'll fix this soon too, by replacing this
    // term with a temporary and just doing a normal stripmine reduction of the
    //          bool = reduction (vector expression)
    //
    NU_ASSERT (not isReduction (), e);
    annotate (Notes (eReduction | eNeedsForLoop));
    mRed = new ReductionFramework (unary->getArg (0), unary->getOp ());
    return true;
  }

  // Not interesting...
  return false;
}
bool
Stripper::isStrippable (NUAssign *assign)
{
  if (not assign)
    return false;

  NULvalue *lvalue = assign->getLvalue ();

  UInt32 destWidth = lvalue->getBitSize ();

  if (destWidth == 1 && checkReduction (assign->getRvalue ())) {
    return true;
  } else if (not stripSize (destWidth) || not checkWidth (destWidth)) {
    return false;
  }

  Strippable callback(this);
  NUDesignWalker walker (callback, false, false);

  if (walker.assign (assign) == NUDesignCallback::eStop)
    return false;

  // Be careful to avoid stripping something like
  //	a[127:0] = a[159:32] | MASK;
  // we need to use a temporary and assign that to avoid overwriting
  // the destination while we're still reading parts of it.
  if (destOverlapsSrc ())
    // The overlap check permits a[127:0] = a[127:0] ^ B;
    annotate (eNeedsTemporary);


  // Examine the RHS - it might be too simple to justify stripmining.
  const NUExpr *rvalue = assign->getRvalue ();

  switch (rvalue->getType ()) {
  case NUExpr::eNUMemselRvalue:
  case NUExpr::eNUIdentRvalue:
  case NUExpr::eNUConstXZ:
  case NUExpr::eNUConstNoXZ:
  case NUExpr::eNUConcatOp:
    // Don't stripmine simple assignments - we already generate good code for these
    return false;

  case NUExpr::eNUVarselRvalue:
    // Don't stripmine partselect assignments that are unaligned; the anytoany() code
    // for the partselects should handle them.  Anything bigger than 3 words is going
    // to be helped by stripping.
    if (isUnaligned () && not isReduction () && rvalue->getBitSize () <= 96)
      return false;

    break;


  default:
    break;
  }
  
  if (needsTemporary () && (getWidth () + getOffset ()) <= LLONG_BIT)
    // If we need a temporary, and the temp is not going to be a bitvector, we
    // won't be able to index into it.
    return false;

  annotate (eNeedsForLoop);

  return true;
}


void Stripper::dump (UtOStream& out) const
{
  out << "StripMine: ";
  if (isReduction ())
    out << "reduction ";
  if (needsForLoop ())
    out << "forloop(" << ((getWidth () - (mPrefixLength + mSuffixLength)) / LONG_BIT) << ") ";
  if (needsTemporary ())
    out << "temp ";
  if (isUnaligned ())
    out << "unaligned ";
  if (needsPrefix ())
    out << "prefix(" << mPrefixLength << ") ";
  if (needsSuffix ())
    out << "suffix(" << mSuffixLength << ") ";

  out << getWidth () << " bits, aligned on bit " << getOffset ()
      << "\n         : ";

  UtString someStmt;
  mStmt->compose (&someStmt, NULL, 11); // line-up with 'StripMine: "
  out << someStmt;

  someStmt.clear ();
  mBlock->compose (&someStmt, NULL, 11, true);
  out << "\n         : " << someStmt;
}

bool Stripper::isStrippable ()
{
  // Right now only assignment stmts are supported..
  if (NUContAssign* ca = dynamic_cast<NUContAssign*>(mStmt))
    return isStrippable (ca);
  else if (NUAssign* assign = dynamic_cast<NUAssign*>(mStmt))
    return isStrippable (assign);
  
  return false;
}

// Construct simple assignment
NUExpr*
Stripper::genTempAssign (NUExpr *expr)
{
  // Construct a temporary in the current scope large enough to hold
  // the expression result.
  StringAtom* sym = getBlock()->gensym("strip", NULL, expr);
  UInt32 exprSize = expr->getBitSize ();
  NUNet *tnet = 
    getBlock ()->createTempNet (sym, exprSize,
                                expr->isSignedResult(), expr->getLoc ());

  NULvalue *tval = new NUIdentLvalue (tnet, expr->getLoc ());

  CopyContext cc (getBlock (), mAtomic);

  NUExpr* src = expr->copy (cc);

  // Place the temp assignment at the beginning of the block.
  getBlock ()->addStmt (sGenAssign (tval, src, expr->getLoc ()));

  // Mark as modified.
  setChanged ();

  NUExpr *result = tval->NURvalue ();
  result->resize (exprSize);
  return result;
}

/*!
 * Construct a series of statements that strip-mine the vector
 * assignment \arg stmt, and add them to the block mBlock.
 *
 * The vector assignment may have been altered to write to a temporary
 * if the real LHS is used on the RHS or if the alignment of the destination
 * is incompatible with the alignment of the source expressions.
 *
 * If the vector expression is unaligned, generate prefix and suffix
 * assignments to compute the fractional bit slices that aren't
 * word-aligned strip-mined operations.  If we're using a temporary, then
 * we don't need the prefix/suffix pieces because we'll ignore the extra
 * bits of the temporary when we move the result into the real destination.
 */

void
Stripper::generateForLoop (NUAssign *stmt, CopyContext& cc)
{
  // Before we do ANYTHING, extract any ?: statements or other expressions
  // that need to be evaluated ONCE before the loop.

  SubExpr fixTeCond (this);
  NUDesignWalker walk (fixTeCond, false, false);
  walk.stmt (stmt);

  // Check for an unaligned prefix piece to be moved before.
  if (needsPrefix ()) {
    NUAssign* prefix = dynamic_cast<NUAssign *>(stmt->copy (cc));
    prefix->resize ();
      
    StripEnds lowBits (false, this);
    NUDesignWalker walkLowBits (lowBits, false, false);
    walkLowBits.stmt (prefix);
    prefix->resize ();

    mBlock->addStmt (prefix);
  }

  // Check for an unaligned suffix piece to be moved

  if (needsSuffix ()) {
    NUAssign* suffix = dynamic_cast<NUAssign *>(stmt->copy (cc));
    suffix->resize ();

    StripEnds highBits (true,this);
    NUDesignWalker walkHighBits (highBits, false, false);
    walkHighBits.stmt (suffix);
    suffix->resize ();

    mBlock->addStmt (suffix);
  }

  const SourceLocator& loc = stmt->getLoc ();

  // Only generate a for-loop if there is an opportunity to do short-circuiting,
  // or if the number of iterations is high.  Otherwise, manually unroll
  bool short_circuit = isReduction () && mRed->isShortCircuited ();
  SInt32 wordCount = (getWidth () + (LONG_BIT - 1) - (mPrefixLength + mSuffixLength)) / LONG_BIT;

  if ((wordCount > cMaxUnrollWordCount) || short_circuit) {
    StringAtom* indexSym = mBlock->gensym("stripi", mGenvarPrefix.c_str());
    NUNet* indexNet = mBlock->createTempNet (indexSym, LONG_BIT, true, 
                                             mBlock->getLoc ());
    indexNet->setFlags (NetRedeclare (indexNet->getFlags (),
                                      eDMIntegerNet | eSigned));

    // Convert all the vector references to piece-wise 32-bit references
    StripBody converter (this, indexNet);
    NUDesignWalker walker (converter, false, false);
    walker.stmt (stmt);

    NUStmtList body;
    body.push_back (stmt);

    // Calculate for-loop parameters.
    NULvalue *index = new NUIdentLvalue (indexNet, loc);

    NUConst *low   = NUConst::create (true, 0, LONG_BIT, loc);
    NUConst *one    = NUConst::create (true, 1, LONG_BIT, loc);

    // Upper bound of loop is #words - 1 in vector
    NUConst *high  = NUConst::create (false, wordCount - 1, LONG_BIT, loc);

    NUBlockingAssign *init = sGenAssign (index, high, loc);

    NUStmtList init_list;
    init_list.push_back(init);


    NUExpr *indexRvalue = index->NURvalue ();

    NUExpr *limit = new NUBinaryOp (NUOp::eBiSGtre, indexRvalue, low, loc);
    limit->resize (1);

    if (short_circuit) {
      // Add the short-circuited expression to escape from a reduction
      // early if possible.

      NUExpr *tempBit = new NUIdentRvalue (mRed->getTempNet (), loc);
      NUExpr *shortTest = new NUBinaryOp (NUOp::eBiEq,
                                          tempBit,
                                          mRed->getInitialValue (), loc);
      shortTest->resize (1);
      limit = new NUBinaryOp (NUOp::eBiLogAnd, limit, shortTest, loc);
      limit->resize (1);
    }

    NUExpr* subExpr = new NUBinaryOp (NUOp::eBiMinus,
                                      index->NURvalue (),
                                      one, loc);
    subExpr->resize (LONG_BIT);

    NUBlockingAssign *incr  = sGenAssign (index->copy (cc), subExpr, loc);

    NUStmtList incr_list;
    incr_list.push_back (incr);


    NUFor* forLoop = new NUFor (init_list,
                                limit,
                                incr_list,
                                body,
                                mFactory,
                                stmt->getUsesCFNet(), 
                                loc);
    mBlock->addStmt (forLoop);
  }

  // Manually unroll if the for-loop doesn't have an early termination, and there
  // are not very many iterations.  I don't believe gcc unrolling yields the same
  // results, and gcc is likely to punt on optimizations such as unrolling if the
  // module is sufficiently complex.  Emitting it pre-unrolled seems to yield better
  // performance based on DoPerfCheck.  This has not been exhaustively proved.
  //
  // In particular, consider a two-element iteration.  The for-loop must initialize
  // an index, and for every iteration, decrement and test.  In addition, the
  // body of the for-loop generally will have dynamic part-selects in them, rather
  // than using a constant offset to the BitVector.
  else {
    for (SInt32 i = 0; i < wordCount; ++i) {
      // Convert all the vector references to piece-wise 32-bit references
      StripBody converter (this, i);
      NUDesignWalker walker (converter, false, false);

      // for the last iteration, mutate the assignment in place.  copy others
      NUStmt* assign = (i == wordCount - 1) ? stmt : stmt->copy(cc);
      walker.stmt (assign);
      mBlock->addStmt(assign);
    }
  }
}

/* Coded as
 *
 *	// unaligned partselect for LSBS
 *	// unaligned partselect for MSBs
 *	{
 *	  BitVector<N> temp;	// optional if A overlaps with RHS
 *	  UInt32 i;
 *	  for(i=upper; i >= 0; --i)
 *	     temp = B[i] | C[i];
 *	  A = temp	
 *	}

 */
NUBlock*
Stripper::generate ()
{

//  NUScope *scope = gCodeGen->getScope ();

  // Generate a block to contain the expanded statements
  //
  mBlock = new NUBlock (NULL, mScope,
                        NULL, mFactory, 
                        false, mStmt->getLoc());


  CopyContext cc(mBlock, mAtomic);

  const NUAssign *asn = dynamic_cast<const NUAssign*>(mStmt);
  NU_ASSERT (asn, mStmt);		// Can't handle anything else yet...

  if (needsForLoop ()) {
    // Create a loop index variable as a signed integer.  Using the
    // expression RHS as a variable name makes very hard-to-read code
    // for the strip variables which get repeated many times.  Let's
    // try using the lvalue.
    NULvalue* lval = asn->getLvalue();
    NUExpr* rval = asn->getRvalue();
    lval->compose(&mGenvarPrefix, NULL, 0, true);

    if (needsTemporary () || isReduction ()) {
      // Construct a temp to compute the RHS and then move
      // that into the destination. This could either be a vector temp
      // or a simple scalar for a reduction of a vector expression

      UInt32 width = isReduction () ? 1 : getOffset () + getWidth ();
      StringAtom* netSym = mBlock->gensym("stripv", mGenvarPrefix.c_str());
      // Don't know what will be the sign of this Net. Is there any
      // way to determine the sign of this temporary net ?
      NUNet *temp = mBlock->createTempNet(netSym, width, false,
                                          mBlock->getLoc());

      NULvalue *dest;
      NUExpr* rhs;

      if (isReduction ()) {
        mRed->putTempNet (temp);

        dest = new NUIdentLvalue (temp, mBlock->getLoc ());

        // Initialize the temporary accumulator value outside the loop
        mBlock->addStmt (sGenAssign (dest->copy (cc),
                                     mRed->getInitialValue (),
                                     mBlock->getLoc ()));

        // Alter the stripable stmt to be
        // bitTemp <binop> (reductionexpr)
        rhs = new NUBinaryOp (mRed->getReducedOpcode (),
                              dest->NURvalue (),
                              rval->copy (cc),
                              mBlock->getLoc ());
        rhs->resize (1);
      } else {
        // The compatibly-aligned temporary's position and size...
        ConstantRange r (getWidth () + getOffset (), getOffset ());

        dest = new NUVarselLvalue (temp, r, mBlock->getLoc ());
        rhs = rval->copy (cc);
      }

      // Generate the indexable statement to loop over
      NUAssign *assign = sGenAssign (dest, rhs, asn->getLoc ());

      generateForLoop (assign, cc);

      if (isReduction ()) {
        // Assign the temp to the real destination
        mBlock->addStmt (sGenAssign (lval->copy (cc),
                                     dest->NURvalue (),
                                     mBlock->getLoc ()));
      } else {
        // Assign the interesting piece of the temporary to its
        // real destination.

        // The compatibly-aligned temporary's position and size...
        ConstantRange r (getWidth () + getOffset (), getOffset ());


        NUVarselRvalue *psTemp = new NUVarselRvalue (temp, r, asn->getLoc ());
        psTemp->resize (r.getLength ());
        mBlock->addStmt (sGenAssign (lval->copy (cc),
                                     psTemp, asn->getLoc ()));
      }
    } else {
      // Just do the stripmined assignment.
      generateForLoop (sGenAssign (lval->copy (cc),
                                   rval->copy (cc),
                                   asn->getLoc ()),
                       cc);
    }
  }

  if (mVerbose)
    dump (UtIO::cout ());			// Indicate what we did

  return mBlock;
}
