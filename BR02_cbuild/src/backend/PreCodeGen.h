// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Lower nucleus representations prior to codegen.
*/

#ifndef _PRECODEGEN_H_
#define _PRECODEGEN_H_
#include "CGLower.h"

// Search for expressions to lower before codegen.
class PreCodeGen : public virtual CGLower
{
public:
  PreCodeGen (ArgProc* arg, AtomicCache* atomic, IODBNucleus* iodb, NUNetRefFactory* factory,
              MsgContext* msgCtxt, bool verbose);
  ~PreCodeGen ();

  //! Catch all
  Status operator()(Phase, NUBase*) { return eNormal;}

  //! Visit all expressions in every statement
  Status operator()(Phase, NUStmt* stmt);
  //! We lower some continuous assigns to always blocks with strength assignments
  Status operator()(Phase, NUContAssign*);
  Status operator()(Phase, NUBlockingAssign*);
  Status operator()(Phase, NUAssign*);
  Status operator()(Phase, NUEnabledDriver*);
  Status operator()(Phase, NUTriRegInit*);

  NUStmt* replacement (NUStmt *stmt);
  NUContAssign* replacement (NUContAssign *stmt);
//  NUStmt* replacement (NUStmt*);

private:
  enum ConcatAssignType { 
    eConcatAssign,
    eVarselConcatAssign,
    eUnhandledAssign 
  };

  /*!
    Determine if a LHS-concat assignment can be handled by either
    concatAssign or varselConcatAssign.
  */
  ConcatAssignType qualifyLHSConcat(const NUAssign * assign) const;

  //! build concat assignment statements
  NUStmt* concatAssign (const NUAssign* stmt);

  //! Build assignment statements for a varsel-of-concat.
  /*!
    Convert:
      {a,b,c}[i] = expr;
    Into:
      s1: temp = {a,b,c}
      s2: temp[i] = expr;
      s3: {a,b,c} = temp;
   */
  NUStmt * varselConcatAssign(const NUAssign * stmt);
  
  //! Make these unavailable
  PreCodeGen ();
  PreCodeGen (PreCodeGen&);
  PreCodeGen & operator =(const PreCodeGen&);
};

#endif
