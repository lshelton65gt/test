// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "BETaskParamLower.h"
#include "reduce/AllocAlias.h"

#include "codegen/codegen.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NULvalue.h"
#include "localflow/UD.h"

BETaskParamLower::BETaskParamLower(AtomicCache* str_cache,
                                   MsgContext* msgContext, 
                                   AllocAlias* allocAlias) :
  LowerPortConnections(str_cache, msgContext, false, false),
  mAllocAlias(allocAlias)
{
  mCurrentInputNets = new NUNetSet;
}

BETaskParamLower::~BETaskParamLower() 
{
  delete mCurrentInputNets;
}


void BETaskParamLower::preCallTaskEnable(NUTaskEnable* call)
{
  // get all the input args so we 
  mCurrentInputNets->clear();
  for (NUTFArgConnectionLoop loop = call->loopArgConnections();
       not loop.atEnd();
       ++loop) {
    NUTFArgConnection* conn = *loop;
    if (conn->getDir() == eInput)
    {
      NUTFArgConnectionInput* inConn = dynamic_cast<NUTFArgConnectionInput*>(conn);
      NU_ASSERT(inConn, conn);
      
      NUExpr* expr = inConn->getActual();
      expr->getUses(mCurrentInputNets);
    }
  }
}

bool BETaskParamLower::doLowerInput(NUTFArgConnectionInput* /*in_conn*/)
{
  return false;
}

static bool sNeedLower(const NUNetSet& defs, const NUNetSet& uses, 
                       const AllocAlias& allocAlias)
{
  bool needLower = false;
  for (NUNetSet::UnsortedCLoop p = defs.loopCUnsorted(); ! needLower && ! p.atEnd();
       ++p)
  {
    NUNet* out = *p;
    if (out->isForcible() || gCodeGen->isTristate(out))
      needLower = true;
  }
  
  if (! needLower)
  {
    /* Loop the defs and query the AllocAlias about potential aliasing
     * for each use. This is unfortunately, n^2, but n should be
     * tiny, until someone comes along and writes a task with
     * hundreds of ports.
     */
    if (! defs.empty() && ! uses.empty())
    {
      for (NUNetSet::UnsortedCLoop p = defs.loopCUnsorted(); 
           ! needLower && ! p.atEnd();
           ++p)
      {
        NUNet* out = *p;
        // Only non-pods get passed in by reference
        if (CodeGen::sIsReferenceParameter(out))
        {
          for (NUNetSet::UnsortedCLoop q = uses.loopCUnsorted(); 
               ! needLower && ! q.atEnd();
               ++q)
          {
            NUNet* in = *q;
            if (allocAlias.query(out, in))
              needLower = true;
          } // for uses
        } // if > 64
      } // for defs
    } // if ! empty sets
  } // if ! needsLower
  
  return needLower;
}

bool BETaskParamLower::doLowerOutput(NUTFArgConnectionOutput* out_conn)
{
  NULvalue* lval = out_conn->getActual();
  NUNetSet defs;
  lval->getDefs(&defs);
  return sNeedLower(defs, *mCurrentInputNets, *mAllocAlias);
}

bool BETaskParamLower::doLowerBid(NUTFArgConnectionBid* bid_conn)
{
  NULvalue* lval = bid_conn->getActual();
  NUNetSet defs;
  lval->getDefs(&defs);
  return sNeedLower(defs, *mCurrentInputNets, *mAllocAlias);
}

BELowerCallback::BELowerCallback(NUNetRefFactory *netref_factory,
                                 MsgContext *msg_context,
                                 AllocAlias* allocAlias,
                                 AtomicCache *string_cache,
                                 IODBNucleus *iodb,
                                 ArgProc* args) :
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mStringCache(string_cache),
  mIODB(iodb),
  mArgs(args)
{
  mLP = new BETaskParamLower(mStringCache, mMsgContext, allocAlias);
}

//! Destructor
BELowerCallback::~BELowerCallback()
{
  delete mLP;
}


NUDesignCallback::Status 
BELowerCallback::operator() (Phase /* phase */, NUBase * /* node */)
{
  return eNormal;
}

NUDesignCallback::Status 
BELowerCallback::operator()(Phase phase, NUStructuredProc *node) 
{
  if (phase == ePre)
  {
    if (mLP->structuredProc(node))
    {
      UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
      ud.structuredProc(node);
    }
  }
  return eNormal;
}

NUDesignCallback::Status  
BELowerCallback::operator()(Phase phase, NUTF *tf) {
  if (phase == ePre)
  {
    // walk through the tf looking for nested taskenables
    if (mLP->tf(tf))
    {
      UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
      ud.tf(tf);
    }
  }
  return eNormal;
}
