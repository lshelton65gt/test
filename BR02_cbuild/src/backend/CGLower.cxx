// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Walk the design doing book-keeping that's typically needed for codegen lowering
  transforms.  We need to track the block-scope, visit the modules, but not the instances
  and need to rerun UD after we have made changes to a module.

  Also centralize the logic needed to convert a continuous-assign into an always block
  complete with fixing up the scheduling of the flow-node

  CGLower is inherited by both PreCodeGen and StripMine.

*/
#include "CGLower.h"

#include "localflow/UD.h"

#include "nucleus/NUAssign.h"   //show that NUBlockingAssign is a NUStmt
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUEnabledDriver.h"
#include "reduce/RETransform.h"

#include "flow/FLNode.h"

CGLower::CGLower (ArgProc* arg, AtomicCache* ac, IODBNucleus* iodb,
                    NUNetRefFactory* f, MsgContext* msgCtxt, bool verbose):
  mArg (arg), mAtomicCache (ac), mIODB (iodb), mFactory (f), mContext (msgCtxt),
  mVerbose (verbose), mChanged (false)
{
}

CGLower::~CGLower () {}

// Maintain the scope-stack on entry/exit to a named scope
void CGLower::doScope (NUDesignCallback::Phase phase, NUScope* scope)
{
  if (phase == ePre)
    mScopeStack.push (scope);
  else
    mScopeStack.pop ();
}

NUDesignCallback::Status CGLower::operator() (NUDesignCallback::Phase phase , NUNamedDeclarationScope* node)
{
  doScope (phase, static_cast<NUScope*>(node));
  return eNormal;
}

NUDesignCallback::Status CGLower::operator() (NUDesignCallback::Phase phase , NUModule* node)
{
  doScope (phase, static_cast<NUScope*>(node));

  if (phase == ePost && mChanged) {
    UD ud (mFactory, mContext, mIODB, mArg, false);
    ud.module (node, true);
    mChanged = false;
  }
    
  return eNormal;
}

NUAlwaysBlock* CGLower::makeAlwaysBlock (NUContAssign* cassign, NUBlock* bodyBlock)
{
  NUStmt* stmt = cassign;
  return makeAlwaysBlock(stmt, bodyBlock);
}

NUAlwaysBlock* CGLower::makeAlwaysBlock (NUContEnabledDriver* cenadrv, NUBlock* bodyBlock)
{
  NUStmt* stmt = cenadrv;
  return makeAlwaysBlock(stmt, bodyBlock);
}

NUAlwaysBlock* CGLower::makeAlwaysBlock (NUStmt* stmt, NUBlock* bodyBlock)
{
  NU_ASSERT((stmt->getType() == eNUContAssign) ||
            (stmt->getType() == eNUContEnabledDriver), stmt);
    
  NUModule* mod = dynamic_cast<NUModule*>(mScopeStack.top ());
  NU_ASSERT (mod, stmt);

  // Can we use the same string name for the always block as we did for the
  // continuous assign?
  NUAlwaysBlock* always = new NUAlwaysBlock (stmt->getName (), bodyBlock,
                                             mFactory, stmt->getLoc (), false);
  mod->addAlwaysBlock (always);

  // Fixup the flow.  Schedule has created containers of flow nodes pointing to
  // the continuous assign.  We are going to delete that continuous assign and
  // insert a new always block that will be coded after we fixup the flow node to
  // point to the new always-block.

  NUNetSet defs;
  stmt->getDefs (&defs);
  for(Loop<NUNetSet> d = defs.loopUnsorted (); !d.atEnd (); ++d) {
    // find the flow pointing to the stmt and move it to the always block.
    NUNet *net = *d;
    for(NUNet::DriverLoop l = net->loopContinuousDrivers (); !l.atEnd (); ++l) {
      // Only want the flow for this continuous assign
      FLNode* flow = *l;
      if (flow->getUseDefNode () != stmt)
        continue;

      // Found the flow node that pointed to stmt.  Redirect it to the
      // new always block
      flow->setUseDefNode (always);
    }
  }

  // Remember that it's necessary to recompute UD for this module
  mChanged = true;

  return always;
}

NUDesignCallback::Status CGLower::operator() (NUDesignCallback::Phase phase , NUBlock* node)
{
  doScope (phase, static_cast<NUScope*>(node));
  return eNormal;
}

NUDesignCallback::Status CGLower::operator() (NUDesignCallback::Phase phase , NUTask* node)
{
  doScope (phase, static_cast<NUScope*>(node));
  return eNormal;
}

NUDesignCallback::Status CGLower::operator() (NUDesignCallback::Phase phase , NUTF* node)
{
  doScope (phase, static_cast<NUScope*>(node));
  return eNormal;
}

