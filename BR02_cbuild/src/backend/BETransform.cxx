// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "BETransform.h"

#include "codegen/carbon_priv.h"
#include "compiler_driver/CarbonContext.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUCopyContext.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUTF.h"
#include "util/StringAtom.h"
#include "util/UtMap.h"
#include "util/UtSet.h"
#include "iodb/IODBNucleus.h"
#include "codegen/carbon_priv.h"

BETransform::BETransform(NUNetRefFactory *netref_factory,
		 MsgContext *msg_context,
		 STSymbolTable *symtab,
		 AtomicCache *str_cache,
		 IODBNucleus *iodb,
		 ArgProc *arg) :
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mSymtab(symtab),
  mStringCache(str_cache),
  mIODB(iodb),
  mArg(arg)
{}

BETransform::~BETransform()
{}

//! Backend Transform 
/*!
 * Backend Transfrom phase framework, currently only performs bit optimization
 * for concatOp to shift/mask.
 */
void BETransform::design(NUDesign *d)
{
  if (! mArg->getBoolValue(CRYPT("-noBitOptimization"))){
    BitOptStatistics stats;
    bool verbose = mArg->getBoolValue(CRYPT("-verboseBitOptimization"));
    BitOptimizeCallback callback(this, &stats, verbose);
    NUDesignWalker walker(callback, false);
    walker.design(d);
    if(verbose)
      stats.print();
  }
}

NUDesignCallback::Status
BETransform::BitOptimizeCallback::operator()(Phase phase, NUAssign* assign)
{
  if (phase == ePre) {
    return eNormal;
  }
  const NUExpr *rhs = assign->getRvalue ();
  UInt32 lsize = assign->getLvalue ()->getBitSize ();
  SInt32 benefit = 0;

  if (mTransform->isBitOptimizable(rhs, 0, lsize, &benefit) && 
      (benefit > 0)) {
    OptimizeBitShiftMask* 
      obsm= new OptimizeBitShiftMask(assign, mTransform, getStats());
    NUAssign * new_assign = NULL;
    obsm->optimizeShiftMask(assign, &new_assign);

    if (new_assign){
      mStmtReplacements[assign] = new_assign;
      if( mVerbose ) {
	UtIO::cout() << "\nOld assign:  ";
	assign->printVerilog(0);
	UtIO::cout() << "New assign:  ";
	new_assign->printVerilog(0);
      }
    }
    delete obsm;
    if (new_assign) return eDelete;
  }	
  // no optimization
  return eNormal;

} // BitOptimizeCallback::operator

NUStmt* 
BETransform::BitOptimizeCallback::replacement(NUStmt *orig)
{
  UtMap<NUStmt*,NUStmt*>::iterator iter = mStmtReplacements.find(orig);

  if (iter != mStmtReplacements.end()) {
    NUStmt *stmt = iter->second;
    mStmtReplacements.erase(iter);

    return stmt;
  } else {
    return 0;
  }
}

NUContAssign* 
BETransform::BitOptimizeCallback::replacement(NUContAssign *cont)
{
  NUStmt* orig = dynamic_cast<NUStmt*>(cont);
  UtMap<NUStmt*,NUStmt*>::iterator iter = mStmtReplacements.find(orig);

  if (iter != mStmtReplacements.end()) {
    NUContAssign* contA = dynamic_cast<NUContAssign*>(iter->second);

    //! update flow node for the contAssign
    /*! the replacement only def one single net for now,
     *  otherwise a loop through all defined nets is required.
     *  Also ** NOTE ** that the replacement may def more bits
     *  in the net than it originally intended. 
     *  e.g.
     *  x[10:4] = {a[2:0],b[4:1]}  ==>
     *  x = x & ~0x7f0 | (a << 6) & 0x700 | (b << 3) & 0xf0
     *  therefore, the def NetRef may not be correct !!
     */
    NUNet * net = mTransform->findNet (cont->getLvalue());
    for (NUNet::DriverLoop d = net->loopContinuousDrivers(); 
	 !d.atEnd(); ++d)
      {
	FLNode* flow = *d;
	NUUseDefNode* node = flow->getUseDefNode();
	if (node && (node->getType() == eNUContAssign)) {
	  const NUContAssign* a = dynamic_cast<const NUContAssign*>(node);
	  if (a == cont)
	    flow->setUseDefNode(iter->second);
	}
      }
    mStmtReplacements.erase(iter);
    return contA;
  } else {
    return 0;
  }
}

void BitOptStatistics::print() const
{
  UtIO::cout() << UtIO::endl
	       << "Backend BitOptimization Summary: " << UtIO::endl
	       << mConcatOptimized << " CONCAT operations optimized," 
	       << UtIO::endl
	       << mAndOptimized << " AND operations optimized"
	       << UtIO::endl
	       << mOrOptimized << " OR operations optimized"
	       << UtIO::endl;
}

const size_t LLONG_BIT = CHAR_BIT * (sizeof (UInt64));
#undef LONG_BIT
const size_t LONG_BIT = (CHAR_BIT * sizeof (UInt32));

//! Check if the bit operations should be transformed.
/*! currently covers only concatOp
 *  \param expr - the rhs expr,
 *  \param level - only counts benefits if level > 0,
 *  \param size - the lhs size,
 *  \param benefit - the accumulated benefits over the expression.
 *  returns true, if the expression only contains  concatOps.
 */
bool BETransform::isBitOptimizable (const NUExpr* expr, 
				    UInt32 level, 
				    UInt32 size, 
				    SInt32* benefit)
{
  if (not expr)
    return false;

  // don't do it, if the result size is greater than 64 bits
  if (expr-> getBitSize () > LLONG_BIT )
    return false;

  switch (expr->getType ())
    {
    case NUExpr::eNUConcatOp:
      {
	const NUConcatOp* rhs = dynamic_cast<const NUConcatOp*>(expr);
	NU_ASSERT( rhs && rhs->getNumArgs() > 0, expr);

	if ( rhs->sizeVaries ())
	  return false;

	if (rhs->isSignedResult()) {
	  if (size != rhs->determineBitSize()) {
	    return false;
	  }
	}

	bool result = true;

	for (UInt32 j = 0; (result && (j < rhs->getNumArgs())); ++j)
	  result &= isBitOptimizable (rhs->getArg(j), level+1, size, benefit);
	return result;
	break;
      }
    case NUExpr::eNUBinaryOp:
      {
	const NUBinaryOp *src = dynamic_cast<const NUBinaryOp *>(expr);
	switch (src->getOp ())
	  {
	  case NUOp::eBiBitAnd:
	  case NUOp::eBiBitOr:
	  default:
	    {
	    break;
	    }
	  }
	break;
      }
    case NUExpr::eNUVarselRvalue:
      {
	const NUVarselRvalue* ps = dynamic_cast<const NUVarselRvalue*>(expr);
	if (ps){
	  if (not ps->getIdentExpr ()-> isWholeIdentifier ())
	    return false;
	}
	else
	  return false;

	if (not ps->isConstIndex ())
	  return false;

        // gcc is able to catch most of the aligned access to produce 
        // equally good code, but not for all cases, so we will NOT
        // exclude the aligned cases.
        //
        //	else {
        //	  ConstantRange r = *(ps->getRange ());
        //	  bool isAligned = isAlignedAccess(findNet(expr),
        //			      r.getLsb(), expr->determineBitSize());
        //	  if(level && isAligned)
        //	    *benefit -= 2;
        //
        //        }
	// fall through
      }
    case NUExpr::eNUIdentRvalue:
      {
	NUNet * net = findNet(expr);
	NU_ASSERT(net != NULL, expr);
	if (level && net && net->getBitSize() <= LLONG_BIT) {
	  // don't do memory
	  if ( net->getMemoryNet ())
	    return false;
	  else {
	    // varsel needs shift so +1,
	    if (expr->getType () != NUExpr::eNUIdentRvalue)
	      // must be varselRvalue
	      *benefit += 1;
	    return true;
	  }
	}
	break;
      }

    case NUExpr::eNUConstNoXZ:
      // Add constants due to new sizing, it adds constants to
      // undersized concats.
      return true;
      break;
      
    default:
      {
	break;
      }
    }
  return false;
}

bool OptimizeBitShiftMask::optimizeShiftMask (const NUAssign * stmt, 
					      NUAssign ** new_assign)
{
  NULvalue * lv = stmt->getLvalue();
  NUExpr * rv = stmt->getRvalue();
  NUExpr * new_rhs = NULL;
  UInt32 lsize = lv->getBitSize();
  SInt32 lstarting = 0;
  if (not lv->isWholeIdentifier()) {
    const NUVarselLvalue* vs = dynamic_cast<const NUVarselLvalue*>(lv);
    // Checking if LHS has a nonconstant expression. Related to bug 7663.
    if(vs && vs->getIndex()->isConstant()) {
      lsize = vs->getLvalue()->getBitSize();
      ConstantRange r = *(vs->getRange ());
      if (lsize <= LLONG_BIT)
	lstarting = r.getLsb();
    }
    else 
      return false;
  }

  switch (rv->getType ())
    {
    case NUExpr::eNUConcatOp:
      {
        new_rhs =  processConcat(dynamic_cast<const NUConcatOp*>(rv), 
				 lstarting, lsize);
	break;
      }
    case NUExpr::eNUBinaryOp:
      {
      const NUOp *src = dynamic_cast<const NUBinaryOp *>(rv);
      switch (src->getOp ())
	{
	case NUOp::eBiBitAnd:
	case NUOp::eBiBitOr:
	  return false;
	  break;
	default:
	  return false;
	  break;
	}
      }
    default:
      return false;
      break;
    }

  //!  RHS transform succeeds, take care of the LHS
  /*!  if (lhs->isWholeIdentifier () || lsize > 64-bit )
   *     lhs = new_rhs;
   *   else
   *     whole_lhs = whole_lhs & ~mask_lhs | new_rhs;
   */

  if (new_rhs) {
    NULvalue* new_lhs = lv;
    const SourceLocator& loc = stmt->getLoc();
    if (not lv->isWholeIdentifier() && (lsize <= LLONG_BIT) ) {
      // it's varselLvalue, get the lsb and length
      const NUVarselLvalue* vs = dynamic_cast<const NUVarselLvalue*>(lv);
      ConstantRange r = *(vs->getRange ());
      UInt64 mk = composeMask( r.getLsb(), r.getLength());
      new_lhs =  vs->getLvalue();
      NU_ASSERT(new_lhs, stmt);

      NUExpr* msk = NUConst::create(false, ~mk, new_lhs->getBitSize(), loc);
      NUExpr* opn = new NUBinaryOp(NUOp::eBiBitAnd, new_lhs->NURvalue(), msk, loc);
      new_rhs = new NUBinaryOp(NUOp::eBiBitOr, new_rhs, opn, loc);
    }

    CopyContext ctx (0,0);
    const NUBlockingAssign*    blk  = dynamic_cast<const NUBlockingAssign*>(stmt);
    const NUNonBlockingAssign* non  = dynamic_cast<const NUNonBlockingAssign*>(stmt);
    const NUContAssign*        cont = dynamic_cast<const NUContAssign*>(stmt);
    if (blk)
      *new_assign = new NUBlockingAssign( new_lhs->copy(ctx), new_rhs, 
					  stmt->getUsesCFNet(), loc);
    else if (non)
      *new_assign = new NUNonBlockingAssign( new_lhs->copy(ctx), new_rhs, 
					     stmt->getUsesCFNet(), loc);
    else if (cont)
      *new_assign = new NUContAssign ( stmt->getName(), new_lhs->copy(ctx), new_rhs, 
				       loc, stmt->getStrength() );
    else
      NU_ASSERT("unexpected NUAssign"==0, stmt);

    return true;
  }
  return false;
}
      
typedef std::pair<const NUExpr *, size_t> ConcatShift_t;

//! unroll a concatOp operands for rvalue only.
/*! to create a vector of value::position (of destination)
 */
template <typename T, typename PAIR> size_t
unrollConcatPos (const T& concat, UtVector<PAIR> *table, size_t pos)
{
  size_t nops = concat.getNumArgs ();	// Number of operands to concat

  // Go backward thru the list getting the operands.

  for (int j = concat.getRepeatCount (); j > 0; --j)
    {
      for (int i=nops-1; i>=0; --i)
	{
	  // common base class for NUConcatOp and NUConcatLvalue
	  const NUUseNode *cat = concat.getArg (i);
	  const T* nested = dynamic_cast<const T*>(cat);

	  // If this operand is a concat TOO, then recurse
	  if (nested)
	    pos = unrollConcatPos<T,PAIR> (*nested, table, pos);
	  else {
	    typename PAIR::first_type arg (concat.getArg (i));
	    table->push_back (PAIR(arg,pos));
	    pos += arg->getBitSize ();
	  }
	}
    }

  // Now the table is ordered in reverse.  First entry is low bits, and
  // the pair.second is the destination bit position of the element
  return pos;
}


NUExpr*
OptimizeBitShiftMask::processConcat (const NUConcatOp* cop, 
                                     SInt32 ls, UInt32 lsize)
{
  //! unrolled concat operands 
  UtVector<ConcatShift_t> shiftTablePos;
  ShiftMaskVector shiftMaskOpnds;
  bool success = true;
  SInt32 offset =  ls; 

  unrollConcatPos<NUConcatOp, ConcatShift_t>(*cop, &shiftTablePos, 0);

  for (UtVector<ConcatShift_t>::reverse_iterator i = shiftTablePos.rbegin ();
      ( success && i != shiftTablePos.rend ());
       ++ i)
    {
      // Determine the shift count for this concat operand. This
      // depends on its location in the concat and the sizes of the
      // preceding operands.
      const NUExpr * e = (*i).first;
      UInt32 destination = (*i).second+offset;
      SInt32 shiftCount = 0;
      if (e->isWholeIdentifier() || e->isConstant()) {
        // Whole identifiers and constants have simple shift counts
	shiftCount  = destination;
      }
      else {
        // Only other supported concat operand is varsel. It's shift
        // expression is a function of the current concat offset and
        // that starting point of the varsel.
	const NUVarselRvalue* vs = dynamic_cast<const NUVarselRvalue*>(e);
	NU_ASSERT(vs, e);
	ConstantRange r = *(vs->getRange ());
	shiftCount = destination - r.getLsb();
      }

      // don't do it, if shift count is greater 32 (unless the
      // expression is a constant which can be done at compile time)
      if (!e->isConstant() && (shiftCount >= 32 || shiftCount <= -32)) {
	success = false;
	continue;
      }

      bool found = false;

      for (ShiftMaskVectorNonconstIter j = shiftMaskOpnds.begin(); 
	   (not found && j != shiftMaskOpnds.end()); ++j) {
	// if matches shift counts and nets, combine the operands by
	// OR-ing the masks. But don't try to do it if the expression
	// is a constant in the concat. We could add a
	// constant/constant combination operation at some point. But
	// it is probably better done somewhere else.
	ShiftMask* sm= *j;
	if ((sm->getShift() == shiftCount) &&
            !e->isConstant() && !sm->getExpr()->isConstant()) {
	  NUNet* net1 = mTransform->findNet(e);
	  NUNet* net2 = mTransform->findNet(sm->getExpr());
	  if (net1 == net2) {
	    // or the mask
	    sm->orMask(composeMask(destination, e->determineBitSize()));
	    found = true;
	  }
	}
      }

      if (not found) {
	// create a new ShiftMask
	UInt64 msk = composeMask(destination, e->determineBitSize());
	ShiftMask* sm = new ShiftMask(e, shiftCount, msk);
	shiftMaskOpnds.push_back(sm);
      }
    }

  NUExpr* new_rhs = NULL;
  if (success){
    const SourceLocator& loc = cop->getLoc();
    new_rhs = createShiftMask( loc, shiftMaskOpnds, lsize );
    getStats()->concatOptimized();
  }

  for (ShiftMaskVectorIter j = shiftMaskOpnds.begin(); 
       j != shiftMaskOpnds.end(); ++j)
    delete (*j);
  return new_rhs;

}  // processConcat


NUExpr* 
OptimizeBitShiftMask::createShiftMask( const SourceLocator& loc,
				       const ShiftMaskVector& shiftMaskOpnds,
                                       UInt32 lsize)
{
  UInt32 i = 0;
  NUExpr* ret_expr = NULL;
  UInt32 theSize = lsize;           // the size to be used for each term
  UInt32 vector_size = shiftMaskOpnds.size();

  // now build each shift/mask term and OR it into the final expression
  CopyContext ctx (0,0);
  for (i = 0; i < vector_size; i++)
  {
    ShiftMask* sm = shiftMaskOpnds[i];
    const NUExpr * e = sm->getExpr();

    NUExpr* idR = NULL; // will hold a copy
    if ( e->isWholeIdentifier() ) {
      idR = new NUIdentRvalue (e->getWholeIdentifier (), e->getLoc ());
    } else if (e->isConstant()) {
      idR = e->copy(ctx);
    } else {
      const NUVarselRvalue* vs = dynamic_cast<const NUVarselRvalue*>(e);
      NU_ASSERT(vs, e);
      idR = dynamic_cast<NUIdentRvalue*> (vs->getIdentExpr()->copy(ctx)); 
    }
    NU_ASSERT(idR, e);

    // No sign extension should be needed with optimizations on bit operations.
    // All required sign extensions are created in population resynthesis and
    // explicitly represented in the nucleus tree as NUBinaryOp(eBiVhExt).
    idR->setSignedResult(false); 


    NUExpr*  opn = NULL;
    SInt32 shift_amount = sm->getShift();
    if ( shift_amount == 0 ) {
      opn = idR;                // no shift needed
    } else {
      NUOp::OpT shiftDirection = NUOp::eBiLshift;
      if (shift_amount < 0) {
        shiftDirection = NUOp::eBiRshift;
        shift_amount = - shift_amount;
      }
      // create expression for shift_amount
      // since the shift count has a self determined size we use 32 bits here
      NUConst* shift_amount_expr = NUConst::create(false, shift_amount, 32, loc);
      opn = new NUBinaryOp(shiftDirection, idR, shift_amount_expr, loc); 
    }

    // apply the mask.  A mask is needed if the expression was signed (because we
    // are building a new assignment with potentially a new size, and
    // we do not want to sign extend any of the component terms) or if the
    // mask doesn't cover the effective bit size of the identifier we're shifting
    // (We use effective size to catch VHDL unsigned subranges.

    // Check that the mask is dense and determine the width of the field it covers.
    // The mask could have zeros in its LSBs even after shifting down.
    //
    UInt64 fieldMask = sm->getMask () >> sm->getShift ();
    UInt32 maskWidth = carbon_DFLO (fieldMask) + 1;
    ++fieldMask;                     // If it was dense, it's now a power-of-two
    bool isDenseMask = (fieldMask & -fieldMask) == fieldMask;

    if (idR->isSignedResult()   // signs would mess things up
        || ! isDenseMask
        || maskWidth < idR->effectiveBitSize ()) {
      NUConst* ct = NUConst::create(false, sm->getMask(), theSize, loc);
      opn = new NUBinaryOp(NUOp::eBiBitAnd,opn, ct, loc);
    }

    if ( ret_expr ) {
      ret_expr = new NUBinaryOp(NUOp::eBiBitOr, ret_expr, opn, loc);
    } else {
      ret_expr = opn;
    }
  }

  ret_expr = ret_expr->makeSizeExplicit(theSize);
  return ret_expr;
} // createShiftMask

// Find the net associated with this lvalue
NUNet* BETransform::findNet (const NULvalue *lv)
{
  if (lv->isWholeIdentifier ())
    return lv->getWholeIdentifier ();

  switch (lv->getType ())
    {
    case eNUPartselIndexLvalue:
    case eNUVarselLvalue:
    {
      const NUVarselLvalue* varsel = dynamic_cast<const NUVarselLvalue*>(lv);
      return findNet (varsel->getLvalue ());
    }

    case eNUMemselLvalue:
    {
      const NUMemselLvalue* memsel = static_cast<const NUMemselLvalue*>(lv);
      return memsel->getIdent ();
    }

    case eNUConcatLvalue:
      return 0;

    default:
      NU_ASSERT (0, lv);
      return 0;
    }
}

NUNet* BETransform::findNet (const NUExpr* rv)
{
  if (rv->isWholeIdentifier ())
    return rv->getWholeIdentifier ();

  switch (rv->getType ())
  {
  default:
    NU_ASSERT (0, rv);     // don't call with arbitrary expressions!
    return 0;

  case NUExpr::eNUVarselRvalue:
  {
    const NUVarselRvalue* ps = dynamic_cast<const NUVarselRvalue*>(rv);
    return findNet (ps->getIdentExpr ());
  }

  case NUExpr::eNUMemselRvalue:
    return dynamic_cast<const NUMemselRvalue*>(rv)->getIdent ();
  }
}

bool 
BETransform::isAlignedAccess (const NUNet *var, 
			      UInt32 pos, UInt32 size)
{
  if (var == NULL)
    return false;

  NU_ASSERT ((pos+size) <= var->getBitSize(), var);

  return ((size == 8 || size == 16 || size == 32)
	  && (pos % size) == 0);
}

