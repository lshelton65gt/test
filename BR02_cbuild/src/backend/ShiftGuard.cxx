// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Insert conditional guards against shifts by out-of-bounds values in places
  where the hardware would do the wrong thing (e.g. shifting by a large number
  when the hardware only uses the low 5 bits of the shift count and those bits
  are zero.
*/

#include "ShiftGuard.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUDesignWalker.h"

#include "util/DynBitVector.h"

// convert shifts to guarded shifts.
class FixShift : public NuToNuFn
{
public:
  // All the work done here
  NUExpr* operator()(NUExpr *e, Phase phase) {
    if (phase == ePre)
      // Don't visit twice...
      return NULL;

    NUBinaryOp* expr = dynamic_cast<NUBinaryOp *>(e);
    if (not expr || expr->getArg (1)->castConst ())
      // only interested in possible shifts-by-non-constant
      return NULL;

    NUOp::OpT opcode = expr->getOp ();
    bool isRight = false;

    switch (opcode)
    {
    default:
      return NULL;              // don't handle VHDL shifts
    case NUOp::eBiLshiftArith:
    case NUOp::eBiLshift:
      break;
    case NUOp::eBiRshift:
    case NUOp::eBiRshiftArith:
      isRight = true;
      break;
    }

    const SourceLocator& loc = expr->getLoc ();
    NUExpr* shift = expr->getArg (1);
    NUExpr* value = expr->getArg (0);
    UInt32 outWidth = std::max (expr->getBitSize (), value->getBitSize ());

    // Can always shift safely if count is 5 bits or less
    UInt32 rbits = shift->effectiveBitSize ();
    if (rbits <= 5              // 0..31 handled by all hardware
        || (rbits == 6 && outWidth > 32)) // 0..63 if doing large shift
      return NULL;

    // We'd like to avoid doing guards on bitvector shifts (because they have guard code
    // wired in, but BV overloading can bypass actually materializing a bitvector, and we'd
    // endup with an unguarded 32/64 bit shift, so only skip cases where we know the
    // bitvector can't be BV-overloaded.
    //
    else if (rbits <= 32 && value->effectiveBitSize () > 64)
      return NULL;              // BitVector shifts by 32 bits or less are safe.
    
    CopyContext cc (0,0);

    NUExpr* outval;
    if (opcode != NUOp::eBiRshiftArith)
      // normal case - logical shifts yield zero on out-bounds.
      outval = NUConst::create (expr->isSignedResult (), 0, outWidth, loc);
    else {
      // Signed right shifts have to deliver a signed value for out-bounds shifts.        
      NUExpr *cond = new NUBinaryOp (NUOp::eBiSLt,
                                     value->copy (cc),
                                     NUConst::create (true, 0, 1, loc), loc);
      
      DynBitVector negone (outWidth);
      negone.flip ();
      outval = new NUTernaryOp (NUOp::eTeCond, cond,
                                NUConst::create (true, negone, outWidth, loc),
                                NUConst::create (true, 0, outWidth, loc),
                                loc);
      outval->resize (outWidth);
      outval->setSignedResult (true);
    }

    // Insert a guard here - typically (shift >= value'size) ? something: (value << shift)
    // where 'something' could be zero or a copy of the sign bit.
    //
    // But because it could be a signed bitvector expression, we may generate a
    // more complex guard like:
    //
    //   (shift >= width) || (shift < 0)
    //
    // if the shift value is a boolean, it will not have been resized; if the operator is a
    // right-shift, we could end up with an unguarded shift after some other transforms.
    // Protect this case by changing expression to 
    //   (shift != 0) ? outval : value;
    //
    NUExpr* ushift = shift->copy (cc);

    // Verilog shift amounts are ALWAYS treated as unsigned.  VHDL shifts are
    // not addressed in this function.  I'm assuming, for the moment, that
    // VHDL shifts are guarded with a runtime library, but this should be
    // verified.
    ushift->setSignedResult (false);
    UInt32 countSize = ushift->getBitSize ();
    NUExpr* cond;

    if (isRight && value->getBitSize () == 1) {
      // (a==b) >> c becomes ((c != 0) ? 0: (a==b))
      cond = new NUBinaryOp (NUOp::eBiNeq, ushift,
                             NUConst::create (false, 0, countSize, loc),
                             loc);
      value = value->copy (cc);
      delete expr;              // this invalidates the variable loc (a reference)
      e = value;
    } else {
      // How many bits do we actually have to shift?  On >>, if the effective bit
      // size is smaller, we can put tighter bounds on the guard
      UInt32 shiftRegWidth = isRight ? value->effectiveBitSize () :outWidth;

      cond = new NUBinaryOp (NUOp::eBiUGtr, ushift,
                             NUConst::create (false, shiftRegWidth-1, countSize, loc), loc);

      // If the shift amount is a wide bitvector, fix it to have a reduced-size.  The
      // guard expression insures that we only need the LSBs of the shift amount
      // because if any truncated bits were non-zero the guard would have rejected the
      // shift.
      if (rbits > 32) {
        UInt32 nbits = DynBitVector::sLog2 (shiftRegWidth);
        ConstantRange bounds(nbits - 1, 0);

        {
          // We want to create a mask that is the same size as the maximum
          // number of bits required for the shift amount.
          UInt32 shiftSize = shift->getBitSize();
          DynBitVector v(shiftSize);
          v.setRange(0, nbits, 1);
          NUConst* mask = NUConst::create(false, v, shiftSize, loc);
          shift = new NUBinaryOp(NUOp::eBiBitAnd, shift, mask, loc);
        }
        expr->putArg(1, shift);
      } else {
        shift->setSignedResult (false);
      }
    }

    // Finish building the guarded shift.
    cond = new NUTernaryOp (NUOp::eTeCond, cond, outval, e, e->getLoc() );
    cond->resize (outWidth);
    cond->setSignedResult (e->isSignedResult ());
    
    return cond;
  } // operator ()(NUExpr* Phase){}

};                              // class FixShift

NUDesignCallback::Status
ShiftGuardCallback::operator()(Phase, NUStmt* stmt)
{
  FixShift fix;
  stmt->replaceLeaves (fix);

  // If this was a statement containing statements, we don't want to
  // revisit any of them - we processed them already, so skip.
  return eSkip;
}
