// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUDesign.h"
#include "backend/Backend.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCost.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUEnabledDriver.h"
#include "compiler_driver/CarbonContext.h"
#include "util/ArgProc.h"
#include "CGLower.h"
#include "HugeExprCrusher.h"

//! \class HugeExprCallback
/*! Design walker callbacks that hunt down huge assignments. Uses CGLower to
 *  track the scope stack and subsequently create new always blocks wired into
 *  the schedule in place of exploded continuous assigns.
 */

class HugeExprCallback : public CGLower {
public:

  HugeExprCallback (HugeExprCrusher &crush, ArgProc *args, AtomicCache *atomic,
                    IODBNucleus *iodb, NUNetRefFactory *netrefs,
                    MsgContext *msg_context, bool verbose)
    : CGLower (args, atomic, iodb, netrefs, msg_context, verbose),
      mCrush (crush), mProc (NULL)
  {}

  ~HugeExprCallback() {
    INFO_ASSERT(mStmtReplaceInfoMap.empty(),
                "Internal error: Crush info map not empty.");
  }

private:

  //! Class to hold the replacement information for statement.
  class StmtReplaceInfo
  {
  public:
    StmtReplaceInfo()
      : mBlock(NULL), mIsAlwaysBlock(false)
    {
    }

    StmtReplaceInfo(const StmtReplaceInfo& sci)
      : mBlock(sci.mBlock), mIsAlwaysBlock(false), mCount(sci.mCount)
    {
    }

    const StmtReplaceInfo& operator=(const StmtReplaceInfo& sci)
    {
      mBlock = sci.mBlock;
      mIsAlwaysBlock = sci.mIsAlwaysBlock;
      mCount = sci.mCount;
      return *this;
    }

    void setBlock(NUBlock* block) { mBlock = block; }
    void setIsAlwaysBlock(bool isAlwaysBlock) { mIsAlwaysBlock = isAlwaysBlock; }

    NUBlock*                  block() { return mBlock; }
    HugeExprCrusher::Count&   count() { return mCount; }
    bool                      isAlwaysBlock() { return mIsAlwaysBlock; }

  private:
    NUBlock* mBlock;
    bool     mIsAlwaysBlock;
    HugeExprCrusher::Count mCount;
  };

  typedef UtMap<NUStmt*,StmtReplaceInfo> StmtReplaceInfoMap;
  typedef StmtReplaceInfoMap::iterator StmtReplaceInfoMapIter;

  HugeExprCrusher &mCrush;            //!< the crusher
  HugeExprCrusher::Count mCount;      //!< module-wise counter

  UtStack<NUStmt*> mStmtStack;        //!< Updated stack of statements as walk proceeds
  StmtReplaceInfoMap mStmtReplaceInfoMap; //!< Map of statement to it's replacement information

  /*!
    Codegen supports separated blocks only in structured proc scope. This variable
    helps us detect such scopes and enable separated block creation in them.
  */
  NUStructuredProc *mProc;

  /*! Replace statement with block */
  NUStmt* replacement(NUStmt* orig)
  {
    NUStmt* repl_stmt = NULL;
    StmtReplaceInfoMapIter itr = mStmtReplaceInfoMap.find(orig);
    if (itr != mStmtReplaceInfoMap.end()) {
      repl_stmt = itr->second.block();
      // The statement has been replaced and will be deleted by design walker.
      // Remove it from map since nucleus has unique statement pointers and
      // we won't need it anymore.
      mStmtReplaceInfoMap.erase(itr);
    }
    return repl_stmt;
  }

  //! Are expressions in statements of given statement type enabled for crushing?
  bool isStmtExprCrushAllowed(NUStmt* stmt)
  {
    bool isAllowed = false;
    switch (stmt->getType())
    {
    case eNUContAssign:
    case eNUBlockingAssign:
    case eNUIf:
    case eNUCase:
    case eNUTaskEnable:
    case eNUFor:
    case eNUBlockingEnabledDriver:
    case eNUContEnabledDriver:
      isAllowed = true;
      break;
    default:
      break;
    }
    return isAllowed;
  }

  //! Make a NUBlock in the given scope with the given location.
  NUBlock* makeBlock(NUScope* scope, NUStmt* stmt, bool makeSepBlock)
  {
    NU_ASSERT (dynamic_cast <NUSeparatedBlock *> (scope) == NULL, stmt);
    NUBlock* ret_block = NULL;
    if (makeSepBlock) {
      StringAtom* name = scope->gensym("crushed_separatedblock");
      ret_block = new NUSeparatedBlock(name, NULL, scope, mIODB, mFactory,
                                       false, stmt->getLoc());
    } else {
      ret_block = new NUBlock(NULL, scope, mIODB, mFactory,
                              false, stmt->getLoc());
    }
    return ret_block;
  }

  bool maybeMakeAlwaysBlock(NUStmt* stmt, NUBlock* block)
  {
    bool made_always_block = false;
    NUType stmt_type = stmt->getType();
    if (stmt_type == eNUContAssign) {
      NUContAssign* cont_assign = dynamic_cast<NUContAssign*>(stmt);
      CGLower::makeAlwaysBlock(cont_assign, block);
      made_always_block = true;
    } else if (stmt_type == eNUContEnabledDriver) {
      NUContEnabledDriver* ena_drv = dynamic_cast<NUContEnabledDriver*>(stmt);
      CGLower::makeAlwaysBlock(ena_drv, block);
      made_always_block = true;
    }
    return made_always_block;
  }

  //! Helper functions that make a replacement statement for insertion
  //! inside the block with crushed expressions and temporaries.
  NUStmt* makeReplacementStmt(NUContAssign* cassign);
  NUStmt* makeReplacementStmt(NUContEnabledDriver* cedrv, CopyContext& ctx);
  NUStmt* makeReplacementStmt(NUFor* for_stmt, NUBlock* block, CopyContext& ctx);
  NUStmt* makeReplacementStmt(NUStmt* stmt, StmtReplaceInfo& sri);

  /*! NUBase handler: by default, do not walk through anything. */
  virtual Status operator()(Phase, NUBase *) { return eNormal; }

  /*! NUDesign handler: allow the walk to continue */
  virtual Status operator()(Phase, NUDesign *) { return eNormal; }

  /*! NUModule handler: Update module-wise counter. Scopes are pushed/popped
   *  into the Inference context as we * enter/exit a module.
   */
  virtual Status operator()(Phase phase, NUModule *module);

  /*! Generic NUStmt handler: Enable crushing of expressions for specific 
   *  statement types that we support in pre phase. Push/pop statements
   *  as we enter/exit statement.
   */
  virtual Status operator()(Phase phase, NUStmt * stmt);
  
  /*! Generic NUExpr handler: Look for huge expressions if crushing is enabled.
   * crush them and add temporaries and temporary assign statements to a new block.
   */
  virtual Status operator()(Phase phase, NUExpr* expr);

  //! Generic Block handler: Avoid walking into separated blocks.
  virtual Status operator()(Phase phase, NUBlock* block);

  /*! Generic NUStructuredProc handler: Updates current structuredproc scope.
   */
  virtual Status operator()(Phase phase, NUStructuredProc *proc);

};

// Walker implementation

/*virtual*/ NUDesignCallback::Status HugeExprCallback::operator()(Phase phase, NUModule *module)
{
  CGLower::operator () (phase, module);
  if (phase == ePre) {
    mCount.reset ();                    // reset the module-wise counter
  } else if (phase == ePost && !mCount.isDegenerate ()) {
    // add the count for this module to the global count
    mCount (module);
    mCrush += mCount;
    // output stats for this module
    if (mCrush & HugeExprCrusher::cVERBOSE) {
      mCount.verbose (module);
    }
  }
  return eNormal;
}

NUDesignCallback::Status HugeExprCallback::operator()(Phase phase, NUStmt* stmt)
{
  Status status = eNormal;
  if (phase == ePre)
  {
    // Push statement to be walked into onto stack.
    mStmtStack.push(stmt);
  }
  else if (phase == ePost)
  {
    mStmtStack.pop();
    StmtReplaceInfoMapIter itr = mStmtReplaceInfoMap.find(stmt);
    if (itr != mStmtReplaceInfoMap.end())
    {
      StmtReplaceInfo& sri = itr->second;
      NUBlock* block = sri.block();
      // Get a new statement to be inserted inside the block.
      NUStmt* new_stmt = makeReplacementStmt(stmt, sri);
      // Insert the new statement inside the block. The block will
      // replace the original statement.
      block->addStmt(new_stmt);
      // Update the module crush info.
      mCount += sri.count();
      // Verbosify the crush info for statement's crushed expressions.
      if (mVerbose) {
        sri.count().verbose(stmt);
      }
      // This block is in a always block that has been added already to module
      // using makeAlwaysBlock. Remove replacement from stmt replacement map.
      if (sri.isAlwaysBlock()) {
        mStmtReplaceInfoMap.erase(itr);
      }
      // It's necessary to recompute UD for this module.
      mChanged = true;
      // Mark the statement for deletion. It will be replaced with
      // the block prior to deletion by NUDesignCallback.
      status = eDelete;
    }
  }

  return status;
}

NUDesignCallback::Status HugeExprCallback::operator()(Phase phase, NUExpr* expr)
{
  if (phase == ePre)
  {
    NUStmt* cur_stmt = NULL;
    if (!mStmtStack.empty()) { // Can happen for edge expressions of always blocks.
      cur_stmt = mStmtStack.top();
    }
    bool do_split = false;
    if ((cur_stmt != NULL) && isStmtExprCrushAllowed(cur_stmt) &&
        mCrush.splitOrSeparate(expr, &do_split))
    {
      StmtReplaceInfoMapIter itr = mStmtReplaceInfoMap.find(cur_stmt);
      StmtReplaceInfo& sci = mStmtReplaceInfoMap[cur_stmt];
      if (sci.block() == NULL)
      {
        // Make a block. This is never a separated block.
        NUBlock* block = makeBlock(getScope(), cur_stmt, false);
        sci.setBlock(block);
        // Increment statement count.
        sci.count()(cur_stmt);
        // Depending on statement type, we may have to wrap this block
        // in an always block.
        bool isAlwaysBlock = maybeMakeAlwaysBlock(cur_stmt, block);
        sci.setIsAlwaysBlock(isAlwaysBlock);
      }
      // Make a separated block for temp assigns from crushed expressions,
      // if we're within a structured proc. 
      NUBlock* crush_block = makeBlock(sci.block(), cur_stmt, (mProc != NULL));
      sci.block()->addStmt(crush_block); // Add it to block created for this statement.
      // Crush the expression and get a temporary net that the crushed expression
      // assigns to. This net will replace the expression in the statement.
      mCrush(expr, sci.block(), crush_block, cur_stmt, sci.count(), do_split);
    }
  }
  return eSkip; // Avoid diving into expression args.
}

NUDesignCallback::Status HugeExprCallback::operator()(Phase phase, NUBlock* block)
{
  CGLower::operator()(phase, block);
  // No need to walk into separated blocks again. It contains only crushed expressions.
  if ((block != NULL) && block->isSeparated()) {
    return eSkip;
  }
  return eNormal;
}

NUDesignCallback::Status HugeExprCallback::operator()(Phase phase, NUStructuredProc *proc)
{
  if (phase == ePre) {
    NU_ASSERT (mProc == NULL, proc);
    mProc = proc;
  } else if (phase == ePost) {
    NU_ASSERT (mProc == proc, proc);
    mProc = NULL;
  }
  return eNormal;
}

NUStmt* HugeExprCallback::makeReplacementStmt(NUContAssign* cassign)
{
  // Make a blocking assign statement and add it to block.
  NUStmt* new_stmt =
    new NUBlockingAssign(cassign->getLvalue(), cassign->getRvalue(),
                         false, cassign->getLoc());
  cassign->replaceLvalue(NULL, false);
  cassign->replaceRvalue(NULL);
  return new_stmt;
}

NUStmt* HugeExprCallback::makeReplacementStmt(NUContEnabledDriver* cedrv,
                                              CopyContext& ctx)
{
  // Make a blocking enabled driver and add it to block.
  NUStmt* new_stmt  = 
    new NUBlockingEnabledDriver(cedrv->getLvalue()->copy(ctx),
                                cedrv->getEnable()->copy(ctx),
                                cedrv->getDriver()->copy(ctx),
                                cedrv->getStrength(),
                                false, cedrv->getLoc());
  return new_stmt;
}

NUStmt* HugeExprCallback::makeReplacementStmt(NUFor* for_stmt, NUBlock* block,
                                              CopyContext& ctx)
{
  // Append a copy of the block statements to the advance statements list.
  // This is safe since the loop itself will move inside the block.
  NUStmtList* advance_stmts = for_stmt->getAdvance(false/*don't copy*/);
  NUStmtList* block_stmts = block->getStmts();
  for (NUStmtListIter stmt_itr = block_stmts->begin();
       stmt_itr != block_stmts->end(); ++stmt_itr) {
    advance_stmts->push_back((*stmt_itr)->copy(ctx));
  }
  for_stmt->replaceAdvance(*advance_stmts);
  delete advance_stmts;

  // Append the block statements to the end of initial statements.
  // Then remove the statements from the block.
  NUStmtList* initial_stmts = for_stmt->getInitial(false/*don't copy*/);
  for (NUStmtListIter stmt_itr = block_stmts->begin();
       stmt_itr != block_stmts->end(); ++stmt_itr) {
    initial_stmts->push_back(*stmt_itr);
  }
  for_stmt->replaceInitial(*initial_stmts);
  delete initial_stmts;

  // Remove the statements from the block since we've moved them to
  // for loop's initial.
  block_stmts->clear();
  block->replaceStmtList(*block_stmts);
  delete block_stmts;

  NUStmt* new_stmt = for_stmt->copy(ctx);
  return new_stmt;
}

NUStmt* HugeExprCallback::makeReplacementStmt(NUStmt* stmt, StmtReplaceInfo& sri)
{
  NUStmt* new_stmt = NULL;
  NUBlock* block = sri.block();
  CopyContext ctx(block, 0, true/*copy_scope_variables*/);
  NUType stmt_type = stmt->getType();

  switch (stmt_type)
  {
  case eNUContAssign:
  {
    // Make a blocking assign statement and add it to block.
    NUContAssign* cassign = dynamic_cast<NUContAssign*>(stmt);
    new_stmt = makeReplacementStmt(cassign);
    break;
  }
  case eNUContEnabledDriver:
  {
    // Make a blocking enabled driver and add it to block.
    NUContEnabledDriver* cedrv = dynamic_cast<NUContEnabledDriver*>(stmt);
    new_stmt  = makeReplacementStmt(cedrv, ctx);
    break;
  }
  case eNUFor:
  {
    // The temp assign statements have to be inserted in initial and advance.
    NUFor* for_stmt = dynamic_cast<NUFor*>(stmt);
    new_stmt = makeReplacementStmt(for_stmt, block, ctx);
    break;
  }
  default:
  {
    // The new statement is copy of the current statement.
    new_stmt = stmt->copy(ctx);
    break;
  }
  } // switch
  NU_ASSERT(new_stmt != NULL, stmt);
  return new_stmt;
}

// HugeExprCrusher implementation

HugeExprCrusher::HugeExprCrusher (Options &options, ArgProc* args, 
  AtomicCache* atomic_cache, IODBNucleus* iodb, NUNetRefFactory* factory, MsgContext* msg_context) : 
  mOptions (options), mArgProc (args), mAtomicCache (atomic_cache), mIODB (iodb),
  mNetRefFactory (factory), mMsgContext (msg_context),
  mThreshold (options.getThreshold ()),
  mSeparationThreshold (options.getSeparationThreshold())
{
}

/*virtual*/ HugeExprCrusher::~HugeExprCrusher ()
{
  if (mOptions & cVERBOSE) {
    verbose ();
  }
}

//! Parse options for huge expression crushing
// Stick this in Backend.cxx so that it can get the scCmDlInE static globals
void HugeExprCrusher::Options::parse (const ArgProc &args)
{
  if (args.getBoolValue (CRYPT ("-verboseHugeExpr"))) {
    mOptions |= cVERBOSE;
  }
  if (args.getBoolValue (CRYPT ("-noHugeExprSplit"))) {
    mOptions |= cNOSPLIT;
  }
  SInt32 value;
  if (args.getIntFirst (CRYPT ("-hugeExprSize"), &value) != ArgProc::eKnown) {
    // no value on the command line
  } else if (value < 1) {
    // Now you're just being stupid... leave it at the default. If the number
    // of nodes in an expression is greater than the threshold, then the
    // expression is broken into pieces with the maximum number of operators
    // given by the threshold.
  } else {
    mThreshold = value;
  }
  if (args.getIntFirst (CRYPT ("-separateExprSize"), &value) != ArgProc::eKnown) {
    // no value on the command line
  } else if (value < 1) {
    // Now you're just being stupid... leave it at the default. If the number
    // of nodes in an expression is greater than the threshold, then the
    // expression is evaluated separately and the result is assigned to a 
    // temporary. The temporary replaces expression in the statement.
  } else {
    mSeparationThreshold = value;
  }
}

//! Crush any huge expressions in the design
void HugeExprCrusher::operator () (NUDesign *design)
{
  HugeExprCallback callback (*this, mArgProc, mAtomicCache, mIODB, 
                             mNetRefFactory, mMsgContext, mOptions & cVERBOSE);
  NUDesignWalker crush (callback, false);
  crush.putWalkTasksFromTaskEnables(false);
  crush.design (design);
}

void HugeExprCrusher::operator()(NUExpr* expr, NUBlock* block, NUBlock* crush_block,
                                 NUStmt* stmt, Count& count, bool do_split)
{
  const SourceLocator loc = stmt->getLoc();
  HugeExprCrusher::Crusher crusher(expr, block, crush_block, stmt, mThreshold,
                                   do_split && !(mOptions & cNOSPLIT));
  count(crusher.numSubstitutions());
}

//! Compute the weight of the expression
/*! The number of nodes in the expression is used as the weight. We split huge
 *  expressions at NUOp instances anyway, so the extra expense of repeatedly
 *  computing the NUCost is unwarranted. Note that expressions with 
 *  eBiDownTo operator are considered invalid since codegen asserts if they
 *  appear in rvalues of temp assign statement. It's okay with eBiDownTo's
 *  in lvalue selects.
 *
 *  \param expr the expression to measure
 *
 *  \param threshold if non-zero then bail out as soon as it is passed
 *
 *  \param isValidExpr is set to true if expression is valid for crushing,
 *         false otherwise
 */
UInt32 HugeExprCrusher::computeWeight (const NUExpr *expr, UInt32 threshold,
                                       bool* isValidExpr) const
{
#define INCREMENT(_weight_, _delta_) {     \
    if (threshold && _weight_ > threshold) { \
      return _weight_; \
    } \
    _weight_ += _delta_; \
  }

  const NUOp* op = NULL;
  UInt32 my_weight = 1;
  if ((op = dynamic_cast<const NUOp*>(expr)) != NULL)
  {
    if (op->getOp() == NUOp::eBiDownTo) {
      *isValidExpr = false;
    } else {
      UInt32 n_args = op->getNumArgs ();
      for (UInt32 n = 0; *isValidExpr && (n < n_args); n++) {
        INCREMENT (my_weight, computeWeight(op->getArg(n), threshold, isValidExpr));
      }
    }
  }
  return my_weight;
}

//! \class Crusher
/*! Crusher does the real work of pulling apart the expression and
 *  creating the temp nets and gluing them into a separated block.
 */
HugeExprCrusher::Crusher::Crusher (NUExpr* crushee, NUBlock* block,
                                   NUBlock* crush_block, NUStmt* stmt,
                                   const UInt32 goal_weight, bool do_split)
  : mCrushee (crushee), mCrushBlock(crush_block), mBlock(block),
    mGoalWeight (goal_weight), mDoSplit (do_split)
{ 
  // Walk the expression picking out subexpressions for crushing
  if (mDoSplit) {
    split (mCrushee); 
  }

  // Also substitute original expression with a temporary, in the parent block
  // of separated block.
  substituteOrigExpr();

  // Construct the statement list containing the temporary assigns and the
  // modified assignment. 
  const SourceLocator loc = stmt->getLoc();
  NUStmtList new_stmts; // New temporary assign statements list.
  for (UtList <NUExpr *>::iterator it = mOrdering.begin (); it != mOrdering.end (); it++) {
    // Create the assignments to the temporary nets
    Substitutions::iterator found = mSubstitutions.find (*it);
    NU_ASSERT (found != mSubstitutions.end (), mCrushee);
    addNewBlockingAssign(found->first, found->second, loc, &new_stmts);
  }

  // Replace the crushed subexpressions in the huge expr stmt with references to
  // the temporary nets. The substituted expressions are not deleted because
  // they form the right sides of the assignments to the temporary nets.
  Rewrite rewrite_exprs (mSubstitutions);
  stmt->replaceLeaves (rewrite_exprs);

  // Add all the temp assign statements to the separated block.
  mCrushBlock->addStmts(&new_stmts);
}

HugeExprCrusher::Crusher::~Crusher ()
{
}

void HugeExprCrusher::Crusher::addNewBlockingAssign(NUExpr* expr, NUNet* tempnet,
                                                    const SourceLocator& loc,
                                                    NUStmtList* stmts)
{
  NUIdentLvalue* new_lval = new NUIdentLvalue(tempnet, loc);
  NUStmt* new_assign = new NUBlockingAssign (new_lval, expr, false, loc);
  stmts->push_back(new_assign);
}

SInt32 HugeExprCrusher::Crusher::numSubstitutions() const
{
  return mSubstitutions.size();
}

NUNet* HugeExprCrusher::Crusher::makeTempNet(NUBlock* block, NUExpr* expr)
{
  StringAtom* atom = block->gensym("crush");
  NUNet *net = block->createTempNet(atom, expr->getBitSize(),
                                    expr->isSignedResult(), expr->getLoc());
  // if the expression was real, then mark the net as real
  if (expr->isReal ()) {
    const UInt32 flags = (UInt32) net->getFlags ();
    net->setFlags ((NetFlags ) (flags | eDMRealNet));
  }
  return net;
}

void HugeExprCrusher::Crusher::substituteOrigExpr()
{
  makeSplitPoint(mCrushee, true);
}

void HugeExprCrusher::Crusher::makeSplitPoint (NUExpr *expr,
                                                 bool temp_orig_expr)
{
  NUNet* net = NULL;
  if (expr == mCrushee) {
    if (temp_orig_expr) {
      // The temporary net for top level crushed expression is created in
      // the parent block of separated block.
      net = makeTempNet(mBlock, expr);
    } else {
      return; // Don't replace original expression.
    }
  } else {
    // create a temporary net in separated block
    net = makeTempNet(mCrushBlock, expr);
  }
  NU_ASSERT (mSubstitutions.find (expr) == mSubstitutions.end (), expr);
  mSubstitutions.insert (Substitutions::value_type (expr, net));
  mOrdering.push_back (expr);
}

UInt32 HugeExprCrusher::Crusher::split (NUExpr *expr)
{
  UInt32 weight = 1;
  for (UInt32 n = 0; n < expr->getNumArgs (); n++) {
    NUExpr *child = expr->getArg (n);
    UInt32 child_weight = split (child);
    if (weight + child_weight > mGoalWeight 
      && mSubstitutions.find (child) == mSubstitutions.end ()
      && child_weight > 5 /* less than 5 is rather trivial*/ ) {
      // The cumulative weight of the already examined children has pushed us
      // over the edge, so split this child. If the child is trival, avoid
      // breaking at the child. The expression will instead get broken at the
      // parent of this expression when the recursion unwinds up one level.
      makeSplitPoint (child);
      weight += 1;
    } else {
      // Just increment the weight of this subexpression
      weight += child_weight;
    }
  }
  if (weight > mGoalWeight) {
    makeSplitPoint (expr);
    return 1;
  } else {
    return weight;
  }
}

//! Expression replacement for replaceLeaves
/*! Replaces substituted subexpressions with NUIdentRvalue instances over the
 *  temp net.
*/
/*virtual*/ NUExpr* HugeExprCrusher::Crusher::Rewrite::operator () (NUExpr *expr, Phase phase)
{
  Substitutions::iterator found;
  if (phase != ePost) {
    // do the replacements on the way back up the tree
    return NULL;
  } else if ((found = mMap.find (expr)) == mMap.end ()) {
    // not a crushed subexpression
    return NULL;
  } else {
    // it's a crushed subexpression that must be replaced with a reference to the temporary
    return new NUIdentRvalue (found->second, found->first->getLoc ());
  }
}

//! Verbose spew for the entire HugeExprCrusher instance
void HugeExprCrusher::verbose () const
{
  mCount.verbose ();
}

//! Verbose spew for a counter
void HugeExprCrusher::Count::verbose () const
{
  if (isDegenerate ()) {
    // output nothing
  } else if (mModules == 1 && mCrushed == 1 && mFragments == 0) {
    UtIO::cout () << "Separated 1 statement in 1 module." << UtIO::endl;
  } else if (mModules == 1 && mCrushed == 1) {
    UtIO::cout () << "Crushed 1 statement's " << mCrushedExprs
                  << " expression(s) into " << mFragments 
                  << " pieces in 1 module." << UtIO::endl;
  } else if (mCrushed == 1 && mFragments == 0) {
    UtIO::cout () << "Separated 1 statement in " << mModules << " modules." << UtIO::endl;
  } else if (mCrushed == 1) {
    UtIO::cout () << "Crushed 1 statement's " << mCrushedExprs
                  << " expression(s) into " << mFragments 
                  << " pieces in " << mModules << " modules." << UtIO::endl;
  } else if (mModules == 1 && mFragments == 0) {
    UtIO::cout () << "Separated " << mCrushed << " statements in 1 module." << UtIO::endl;
  } else if (mModules == 1) {
    UtIO::cout () << "Crushed " << mCrushed << " statement's "
                  << mCrushedExprs << " expressions into " << mFragments 
                  << " pieces in 1 module." << UtIO::endl;
  } else if (mFragments == 0) {
    UtIO::cout () << "Separated " << mCrushed << " statements in " << mModules << " modules." << UtIO::endl;
  } else {
    UtIO::cout () << "Crushed " << mCrushed << " statement's " << mCrushedExprs
                  << " expressions into " << mFragments 
                  << " pieces in " << mModules << " modules." << UtIO::endl;
  }
}

//! Verbose spew for counting of a single module
void HugeExprCrusher::Count::verbose (const NUModule *module) const
{
  if (isDegenerate ()) {
    // output nothing
  } else if (mCrushed == 1 && mFragments == 0) {
    UtIO::cout () << "Crushed 1 statement in module " << module->getName ()->str () 
                  << "." << UtIO::endl;
  } else if (mCrushed == 1) {
    UtIO::cout () << "Crushed 1 statement's " << mCrushedExprs
                  << " expression into " << mFragments 
                  << " pieces in module " << module->getName ()->str () 
                  << "." << UtIO::endl;
  } else if (mFragments == 0) {
    UtIO::cout () << "Crushed " << mCrushed << " statements in module " 
                  << module->getName ()->str () 
                  << "." << UtIO::endl;
  } else {
    UtIO::cout () << "Crushed " << mCrushed << " statement's " << mCrushedExprs
                  << " expressions into " << mFragments 
                  << " pieces in module " << module->getName ()->str () 
                  << "." << UtIO::endl;
  }
}

void HugeExprCrusher::Count::verbose (const NUStmt* stmt) const
{
  if (!isDegenerate())
  {
    UtString buf;
    stmt->getLoc().compose(&buf);
    UtIO::cout() << buf << ": Crushed " << stmt->typeStr()
                 << " statement's " << mCrushedExprs
                 << " expression(s) into " << mFragments
                 << " pieces." << UtIO::endl;
  }
}
