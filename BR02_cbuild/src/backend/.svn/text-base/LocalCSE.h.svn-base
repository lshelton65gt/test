// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef LOCALCSE_H_
#define LOCALCSE_H_

#include "nucleus/Nucleus.h"
#include "localflow/DesignWalker.h"
#include "reduce/AllocAlias.h"

class EmitContext;
class CSEInfoManager;
class MsgContext;
class IODBNucleus;
class ArgProc;

//! Local CSE analysis
/*!
 * Perform CSE analysis on all nucleus constructs for which codegen will emit
 * a function.
 *
 * This is termed local CSE analysis because it only considers one function at
 * a time.  We could also perform CSE over the whole design, which would be
 * called global CSE analysis.
 *
 *
 * The current implementation is very simplistic, it has the following restrictions:
 *  . it does not deal with generalized CSEs yet, only simple variable fetches
 *  . it only walks a single statement list;  it does not try to CSE within nested
 *    or branching statement constructs
 *  . it emits lazily, just before the first use.  This is a decent heuristic, but will
 *    need to be changed when branching statements are handled.
 *  . it only walk always blocks
 *
 *
 * The algorithm for each function is:
 * Traverse the statement list in reverse order.
 *  . remove from the set anything which is def'd by the current stmt
 *    . as they are removed, emit any expected common expressions which are worthwhile
 *      to emit and fixup the stmts
 *  . compute what subexpr is computed at the current stmt
 *  . for each subexpr, have it point to the stmt computing it
 *    . this ongoing set is the set of expected subexprs
 *  . if a subexpr is common, bump the ref count and remember the stmt
 *  . when get to the end, emit any remaining expected common expressions which are
 *    worthwhile to emit
 *  . emit change in costing under verbose switch
 */
class LocalCSE : public DesignWalker
{
public:
  LocalCSE(NUNetRefFactory *netref_factory,
	   MsgContext *msg_context,
	   ElabAllocAlias *alloc_alias,
	   AtomicCache *string_cache,
	   IODBNucleus *iodb,
           ArgProc* args,
	   bool verbose);
  ~LocalCSE();

  //! Walk the module
  void module(NUModule *module);

  //! Analyze the structured proc.
  void analyze(NUStructuredProc * proc);
private:
  //! Hide copy and assign constructors.
  LocalCSE(const LocalCSE&);
  LocalCSE& operator=(const LocalCSE&);

  /*!
   * We have encountered a stmt which defs what is in the given defs set.  That
   * stmt will invalidate some CSE's.
   *
   * Emit CSE assigns for worthwhile common subexpressions based on that.
   */
  void emitCSE(NUNetSet &defs);

  //! Emit any remaining worthwhile CSE's
  void emitCSE();

  //! Kill any CSEs which overlap with the given def set.
  void killCSE(NUNetSet &defs);

  //! Remember CSE-able exprs which this stmt uses
  void rememberCSE(NUStmt *stmt, EmitContext &emit_ctx);

  //! Netref factory
  NUNetRefFactory *mNetRefFactory;

  //! Msg context
  MsgContext *mMsgContext;

  //! CSE manager
  CSEInfoManager *mCSEManager;

  //! Allocation aliasing manager
  ElabAllocAlias *mAllocAlias;

  //! String cache to use to create temps
  AtomicCache *mStringCache;

  //! IODB
  IODBNucleus *mIODB;

  //! If true, some action was performed
  bool mDidSomething;

  //! Args
  ArgProc* mArgs;
};
#endif
