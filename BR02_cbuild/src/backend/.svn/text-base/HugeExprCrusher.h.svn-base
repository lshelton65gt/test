// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _HUGE_EXPR_CRUSHER_H_
#define _HUGE_EXPR_CRUSHER_H_

/*! \file 
 *
 *  Pass that hunts down huge expressions and breaks them into smaller
 *  pieces. (Are the fragments of a crushed assignment called assignlets?). For
 *  example, winding down the definition of huge somewhat:
 *
 *  x [5] = (a[0] & (b[15] & (p | (q & a[1]))));
 *
 *  Could be broken into:
 *
 *  tmp0 = (p | (q & a[1]));
 *  x [5] = (a[0] & (b[15] & tmp0));
 *
 *  Bugs 6744 and 6755 for the compelling customer test cases showed that just
 *  generating huge C++ expressions can blow up G++. Breaking them down at
 *  least gets model compilation to terminate successfully.
 *
 *  The temporary assignments and the hacked original assignment are embedded
 *  into NUBlock instances. Continuous assigns and continuous enabled drivers
 *  are embedded in a new always block.
 *
 *  The HugeExprCrusher crushes case statement selects, task enable arguments,
 *  if statement conditions, continuous/blocking assign statement rvalues,
 *  for statement initial/advance statements rvalues and for statement condition,
 *  continous/blocking enabled driver conditions and rvalues. The temporary
 *  assignment statements for crushed expressions are placed in a separated
 *  block which become functions. The final result temporary is used in place
 *  of the expression. Here's an example:
 *
 *  if (a[0] & (b[1] & (c[2] | d[3])))
 *    <statement body>
 *
 *  sep_block(a, b, c, d, $crush;1);
 *  if ($crush;1)
 *    <statement body>
 *
 *  The function call computes the crushed expression value and puts it in a 
 *  temporary $crush;1. The temporary is used in place of expression in original
 *  statement.
 */

class ArgProc;
class NUDesign;
class NUExpr;
class CGLower;
class NUSeparatedBlock;

#include "nucleus/NuToNu.h"

//! \class HugeExprCrusher
/*! Identifies assignments with greater than a threshold number of nodes in the
 *  expression tree and breaks them out into assignments to temporaries.
 */

class HugeExprCrusher  {
public:

  class Options;                        // forward declaration

  HugeExprCrusher (Options &options, ArgProc *, AtomicCache *, IODBNucleus *, 
    NUNetRefFactory *, MsgContext *);

  virtual ~HugeExprCrusher ();

  //! The threshold of 500 was picked empirically. It helps improve
  //! compile time and does not affect runtime performance.
  enum {
    cDEFAULT_CRUSH_THRESHOLD = 500,     //!< default cost threshold for crushing
    cDEFAULT_SEPARATE_THRESHOLD = 64    //!< default cost threshold for separating expression
                                        //!< evaluation into a separate statement 
  };

  // option bits
  enum {
    cVERBOSE = 1<<0,                    //!< enable verbose spew
    cNOSPLIT = 1<<1,                    //!< do not split the huge assigns
    cNONE = 0};

  //! Option vector for huge assignment crushing
  class Options {
  public:

    Options (MsgContext &msg_context) : 
      mMsgContext (msg_context), mOptions (0), mThreshold (cDEFAULT_CRUSH_THRESHOLD),
      mSeparationThreshold (cDEFAULT_SEPARATE_THRESHOLD) {}

    //! Scan the command line for relevant options
    void parse (const ArgProc &);

    //! Test option bits

    /*! \return iff all of the options bits in mask are set in this */
    bool operator & (const UInt32 mask) const { return (mOptions & mask) == mask; }

    //! \return the crushing threshold
    SInt32 getThreshold () const { return mThreshold; }

    //! \return the separate evaluation threshold
    SInt32 getSeparationThreshold() const { return mSeparationThreshold; }

  private:

    MsgContext &mMsgContext;            //!< message output stream
    UInt32 mOptions;                    //!< option bits
    UInt32 mThreshold;                  //!< cost threshold for hugeness and crushing
    UInt32 mSeparationThreshold;        //!< cost threshold for separate evaluation
  };

  /*! \return iff all of the options bits in mask are set in this */
  bool operator & (const UInt32 mask) const { return (mOptions & mask); }

  struct Count;                         // forward declaration

  //! Verbose spew for all the assignments crushed by this instance.
  void verbose () const;

  //! Test an expression against thresholds for splitting and separation.
  bool splitOrSeparate (const NUExpr *expr, bool* do_split) const {
    bool is_valid_expr = true;
    *do_split = false;
    UInt32 largest_threshold = mThreshold;
    if (largest_threshold < mSeparationThreshold) {
      largest_threshold = mSeparationThreshold;
    }
    UInt32 weight = computeWeight(expr, largest_threshold, &is_valid_expr);
    if (is_valid_expr) {
      *do_split = (weight > mThreshold);
      return (*do_split | (weight > mSeparationThreshold));
    }
    return false;
  }

  //! Statistics counter for huge assignment crushing

  struct Count {

    Count () : mModules (0), mCrushed (0), mCrushedExprs (0), mFragments (0) {}

    bool isDegenerate () const { return mCrushed == 0; }

    //! Increment the module count
    void operator () (NUModule *) { mModules++; }

    //! Increment the statement count
    void operator () (NUStmt *) { mCrushed++; }

    //! Increment the expression count
    void operator () (const UInt32 fragments)
    { mCrushedExprs++; mFragments += fragments; }

    //! Increment this count from another count
    Count &operator += (const Count x)
    {
      mModules += x.mModules;
      mCrushed += x.mCrushed;
      mCrushedExprs += x.mCrushedExprs;
      mFragments += x.mFragments;
      return *this;
    }

    //! Reset the count to zero
    void reset ()
    {
      mModules = 0;
      mCrushed = 0;
      mCrushedExprs = 0;
      mFragments = 0;
    }

    //! verbose spew
    void verbose () const;
    
    //! verbose spew for a single module
    void verbose (const NUModule *) const;

    //! verbose spew for a single statement
    void verbose (const NUStmt* stmt) const;

  private:

    UInt32 mModules;                    //!< number of modules affected
    UInt32 mCrushed;                    //!< number of assignments crushed
    UInt32 mCrushedExprs;               //!< number of expressions crushed
    UInt32 mFragments;                  //!< total number of fragments

  };

  //! Walk the design seeking out huge assignmens for crushing.
  void operator () (NUDesign *);

  //! Given a huge expression, crush it and add temp assigns to block.
  void operator() (NUExpr* expr, NUBlock* block, NUBlock* crush_block,
                   NUStmt* stmt, Count& count, bool do_split);

  //! Increment the counter for this from another count
  void operator += (const Count x) { mCount += x; }

private:

  Options &mOptions;                    //!< options for this instance
  ArgProc *mArgProc;                    // the...
  AtomicCache *mAtomicCache;            // ... usual suspects
  IODBNucleus *mIODB;                   // ... for this kind
  NUNetRefFactory *mNetRefFactory;      // ... of
  MsgContext *mMsgContext;              // ... things
  UInt32 mThreshold;                    //!< cost threshold for hugeness
  UInt32 mSeparationThreshold;          //!< cost threshold for separate evaluation
  Count mCount;                         //!< global count for this instance

  //! \class Crusher
  /*! Breaks out a single assignment and embeds the modified assignment and the
   *  temporary assignments in a NUSeparatedBlock instance.
   */

  class Crusher {
  public:

    /*! \param crushee the huge expression to crush
     *
     *  \param block the block where the the separated block and later the statement
     *               with huge expression will be added
     *
     *  \param crush_block the block where generated statements will be added
     *
     *  \param stmt the statement the crushee belongs to
     *
     *  \param goal_weight When an assignment is broken into pieces,
     *         the goal weight is the maximum number of nodes in each expression
     *         tree. Note that the number of nodes may exceed the goal weight
     *         by a small amount to avoid breaking at trivial subexpressions.
     * 
     *  \param split if false, then the assignment is just placed into a
     *         block but not crushed into fragments.
     */

    Crusher (
      NUExpr*  crushee,
      NUBlock* block,
      NUBlock* crush_block,
      NUStmt*  stmt,
      const UInt32 goal_weight,
      bool split);

    ~Crusher ();

    SInt32 numSubstitutions() const;

  private:
    
    //! Recursively split an expression into fragments
    /*! \note This method creates temporary nets and populates
     *  mSubexpressions. It does not actually modify the expressions.
     */
    UInt32 split (NUExpr *);

    //! Create a temp net for a subexpression and note it in substitutions list.
    void makeSplitPoint (NUExpr* expr, bool temp_orig_expr = false);

    //! Create a temp net for original expression and note it in substitutions list.
    void substituteOrigExpr();

    //! Add new blocking temporary assign statement to the separated block.
    void addNewBlockingAssign(NUExpr* expr, NUNet* tempnet,
                              const SourceLocator& loc, NUStmtList* stmts);

    //! Add a new temporary net to the specified block to hold the result
    //! of given expression.
    NUNet* makeTempNet(NUBlock* block, NUExpr* expr);

    NUExpr* mCrushee;                   //!< the expression under the knife
    NUBlock* mCrushBlock;               //!< the block to hold the temporaries and generated statements
    NUBlock* mBlock;                    //!< the parent block of separated block
    const UInt32 mGoalWeight;           //!< target weight for fragments
    const bool mDoSplit;                //!< explode expressions as well as splitting

    //! Map subexpressions to temporary nets
    typedef UtMap <NUExpr *, NUNet *> Substitutions;

    //! \class Rewrite
    /*! Substitution class for rewrite leaves that uses the subexpression to
     *  temporary variable map to map exploded subexpressions into
     *  NUIdentRvalue instances on the temporary net.
     */
    class Rewrite: public NuToNuFn {
    public:
      Rewrite (Substitutions &map) : NuToNuFn (), mMap (map) {}
      virtual NUExpr *operator() (NUExpr *, Phase);
    private:
      Substitutions &mMap;              //!< subexpression to temp net map
    };

    Substitutions mSubstitutions;       //!< map subexpression to temp nets
    UtList <NUExpr *> mOrdering;        //!< bottom up ordering of exploded subexpressions
    
  };

  //! Compute the weight of the expression
  /*! The number of nodes in the expression is used as the weight. We split
   *  huge expressions at NUOp instances anyway, so the extra expense of *
   *  repeatedly computing the NUCost is unwarranted. The threshold argument
   *  allows function to stop computing weight once it crosses it. If the
   *  value is 0, the weight of entire expression is computed and returned.
   */
  UInt32 computeWeight (const NUExpr *, UInt32 threshold, bool* isValidExpr) const;

  //! Make a new separated block in the given scope.
  NUBlock* makeSeparatedBlock(NUScope* scope, const SourceLocator& loc);
};

#endif
