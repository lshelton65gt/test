// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Package of miscellaneous transformations for late stage of compilation,
  right before codegen.

  Currently only bit optimization for concat to shift and mask, see OptimizeBitShiftMask

*/

#ifndef _BETRANSFORM_H_
#define _BETRANSFORM_H_

#include "localflow/LowerPortConnections.h"
#include "nucleus/Nucleus.h"
#include "localflow/DesignWalker.h"

class ArgProc;
class Stats;
class AtomicCache;
class ElabAllocAlias;
class MsgContext;
class RETransform;
class IODBNucleus;
class BitOptStatistics;


//! Backend Transform
/*!
 */
class BETransform
{
public:
  BETransform(NUNetRefFactory *netref_factory,
	      MsgContext *msg_context,
	      STSymbolTable *symtab,
	      AtomicCache *str_cache,
	      IODBNucleus *iodb,
	      ArgProc *arg);

  //! destructor
  ~BETransform();

  void design (NUDesign *d);

  //! Utility routines 
  //! Find the net associated with a value
  NUNet* findNet (const NULvalue* lv);
  NUNet* findNet (const NUExpr* rv);

  //! if this expr is qualified for bit optimization
  bool isBitOptimizable (const NUExpr* expr, 
			 UInt32 level, 
			 UInt32 size, 
			 SInt32* benefit);

  //! if this variable is aligned
  bool isAlignedAccess (const NUNet *var, 
			UInt32 pos, 
			UInt32 size);

  
 private:

  class BitOptimizeCallback : public NUDesignCallback
  {
  public:
    //! constructor
    BitOptimizeCallback(BETransform* t, BitOptStatistics* s, bool v) :
      mTransform(t),
      mStats(s),
      mVerbose(v)
    {}

    //! destructor
    ~BitOptimizeCallback() {}

    //! Catch all
    Status operator()(Phase, NUBase*)
    {
      return eNormal;
    }

    //! Catch all assignments with bit opeartions
    Status operator()(Phase phase, NUAssign* assign);

    NUStmt* replacement(NUStmt *orig);
    NUContAssign* replacement(NUContAssign *orig);
    BitOptStatistics* getStats() {return mStats; }

  private:
    BETransform* mTransform;
    BitOptStatistics* mStats;
    bool mVerbose;
    UtMap<NUStmt*,NUStmt*> mStmtReplacements;
    
  }; // class BitOptimizeCallback : public NUDesignCallback

  NUNetRefFactory *mNetRefFactory;
  MsgContext *mMsgContext;
  STSymbolTable *mSymtab;
  AtomicCache *mStringCache;
  IODBNucleus *mIODB;
  ArgProc *mArg;
  ElabAllocAlias *mAllocAlias;
  BitOptStatistics *mBitOptStats;
  //! If true, some action was performed
  bool mDidSomething;
}; // class BETransform  


//! Statistics for backend Bit Optimization
class BitOptStatistics
{
public:
  //! constructor
  BitOptStatistics() :
    mConcatOptimized(0),
    mAndOptimized(0),
    mOrOptimized(0)
  {}

  ~BitOptStatistics() {}

  void concatOptimized() {++mConcatOptimized;}
  void andOptimized() {++mAndOptimized;}
  void orOptimized() {++mOrOptimized;}
  void print() const;

private:
  UInt64 mConcatOptimized;
  UInt64 mAndOptimized;
  UInt64 mOrOptimized;
}; //class BitOptStatistics


/*! \class  OptimizeBitShiftMask
  \brief class to recognize and implement the reduced shift version of the concat
  assignment that is described at the top of this file.
  
  Consider the following assignment to part of x, where x is declared as
  \verbatim
       reg [7:0] x;
       x[6:4] = {a[2], b[10], c[3]}
  \endverbatim

  This could be implemented as:
  \verbatim
       x = x & 0x8f | ((a>>2)[0] << 6)| ((b>>10)[0] << 5) | ((c>>3)[0] << 4);
  \endverbatim
  An alternative that uses fewer shifts is:
  \verbatim
       x = x & 0x8f | (a << 4) & 0x40 | (b >> 5) & 0x20 | (c << 1)& 0x10;
  \endverbatim

  This alternative eliminates 3 right shifts and is the goal of the
  transformation implemented here.

  In order to decide if the transformation should be applied we calculate the
  profits while processing the RHS operands:
      if  aligned access   profits -= 1;
      if  varselRvalue     profits += 1;
  for the whole identifier, it trys not to put masks, so no points is added/taken.
  The transform is performed if the result profits > 0. 

  *** NOTE ***
  With respect to use/def information: The original form of the assignment only
  def's the bits specified in the partselect on the LHS.  In the transformed
  form of the assign statement all bits of the LHS are def'ed.  Therefore, the
  use/def information from the original assignment is usually not consistent with
  the transformed assignment. (especially for contAssign).  However having bit
  accurate def information during codegen is not necessary so this inconsistency
  can be ignored.

 */
class OptimizeBitShiftMask 
{
public:
  OptimizeBitShiftMask (NUAssign* asn, BETransform* t, BitOptStatistics* s):
    mAssign(asn),
    mNewAssign(NULL),
    mTransform(t),
    mStats(s)
  {}

  //! destructor
  ~OptimizeBitShiftMask() {}

  //! top level routine for concatOp to shift/mask optimization, 
  /*! \param stmt the NUAssign to start with.
   *  \param newStmt if transform succeeded then this holds the assignment 
   *  that should replace stmt.
   *  \return true if transform succeed.
   */
  bool optimizeShiftMask (const NUAssign * stmt, NUAssign** newStmt );

  //! returns the statistics
  BitOptStatistics* getStats () { return mStats; };

  //! compose a mask
  UInt64 composeMask(UInt32 p, UInt32 s) {
    return ((KUInt64(1) << s) - 1) << p; }

private:
  class ShiftMask
  {
  public:
    ShiftMask(const NUExpr * expr, SInt32 sft, UInt64 msk) :
      mExpr(expr),
      mShift(sft),
      mMask(msk)
    {}

    //! get the expr
    const NUExpr * getExpr() {return mExpr;}
    
    //! get the shift count
    SInt32 getShift() {return mShift;}

    //! get the mask
    UInt64 getMask() {return mMask;}
    
    //! set the shift count
    void putShift(SInt32 s) {mShift = s;}

    //! or mMask with the given mask
    void orMask(UInt64 m) {mMask = mMask | m;}

  private:
    //! the expr (id) to be shifted and masked
    const NUExpr * mExpr;

    //! the shift count
    SInt32   mShift;

    //! the mask up to date
    UInt64   mMask;
  };  // class ShiftMask

  friend class ShiftMask;
  typedef UtVector<ShiftMask*> ShiftMaskVector;
  typedef ShiftMaskVector::iterator ShiftMaskVectorNonconstIter;
  typedef ShiftMaskVector::const_iterator ShiftMaskVectorIter;

  //! processes an expr (of concatOp) returns a new expr (of shifts & masks)
  /*! \param cop concatOp to be processed
   *  \param ls  the LHS starting bit position
   *  \param lsize the size of the left-hand-side of the assign
   *  returns nucleus subtree of shift/mask transformed from the concatOp
   */
  NUExpr* processConcat (const NUConcatOp * cop, SInt32 ls, UInt32 lsize);

  //! Assemble and return a NUExpr from a vector of shiftMaskOperands
  /*! The returned NUExpr can be used as a new RHS of an assignment
   * copies are made of the individual expressions included in the vector.
   * \param loc the location information for the returned NUExpr
   * \param shiftMaskOpnds a vector contains shift/mask info for each 
   *        operand of the concatOp
   * \param lsize the size of the left-hand-side of the assign
   *  returns nucleus subtree of shift/mask created from the shift/mask
   *        vector.
   */
  NUExpr* createShiftMask(const SourceLocator& loc,
			  const ShiftMaskVector& shiftMaskOpnds,
                          UInt32 lsize);

  //! the assignment being operated upon
  NUAssign* mAssign;

  //! the new assignment created
  NUAssign* mNewAssign;

  //! the backend transform
  BETransform* mTransform;

  //! keep track of the statistics
  BitOptStatistics* mStats;

}; // OptimizeBitShiftMask

#endif  // _BETRANSFORMM_H_
