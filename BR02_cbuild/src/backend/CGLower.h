// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Utility Class that supports several distinct lowering phases used to move the nucleus
  closer to the hardware and C++ compiler representations
*/
#ifndef __CGLOWER_H__
#define __CGLOWER_H__

#include "nucleus/Nucleus.h"
#include "nucleus/NUDesignWalker.h"

class IODBNucleus;
class MsgContext;
class AtomicCache;
class ArgProc;
class RETransform;

/*!
 */

class CGLower : public virtual NUDesignCallback
{
public:
  CGLower (ArgProc* arg, AtomicCache* atomic, IODBNucleus* iodb, NUNetRefFactory* factory, MsgContext* msgCtxt, bool verbose);

  ~CGLower ();

  //! Visit scopes to maintain scope stack.
  /*!
   * If you override these, you must also call the CGLower::operator() version to maintain
   * the scope stack or explicitly manage the scope-stack yourself.
   */
  Status operator() (NUDesignCallback::Phase phase , NUNamedDeclarationScope* node);
  Status operator() (NUDesignCallback::Phase phase , NUModule* node);
  Status operator() (NUDesignCallback::Phase phase , NUBlock* node);
  Status operator() (NUDesignCallback::Phase phase , NUTask* node);
  Status operator() (NUDesignCallback::Phase phase , NUTF* node);

  // Given a continuous assign, construct an equivalent always block.
  NUAlwaysBlock* makeAlwaysBlock (NUContAssign* cassign, NUBlock* bodyBlock);
  // Given a continuous enabled driver, construct an equivalent always block.
  NUAlwaysBlock* makeAlwaysBlock (NUContEnabledDriver* cassign, NUBlock* bodyBlock);

  //! Get the top of the scope stack.
  NUScope *getScope() const { return mScopeStack.top (); }

  //! Get the stack depth
  UInt32 getStackSize () const { return mScopeStack.size (); }

private:
  //! Utility function to push/pop scope contexts.
  void doScope (Phase, NUScope*);
  //! Utility function to make always block for continuous assign/enabled-driver.
  //! This method is private to prevent user from calling it for other statement types.
  NUAlwaysBlock* makeAlwaysBlock (NUStmt* stmt, NUBlock* bodyBlock);

  //! Make these unavailable
  CGLower (CGLower&);
  CGLower & operator =(const CGLower&);
#if pfMSVC
  // MSVC bogusly complains about instantiating object of derived class
  // PreCodeGen if this default constructor is private, so work around by
  // making it protected.
protected:
#endif
  CGLower ();

protected:
  //! Argument processor context
  ArgProc* mArg;

  //! Atomic cache for generating names
  AtomicCache* mAtomicCache;

  //! Needed for computing UD
  IODBNucleus* mIODB;

  //! Netref factory
  NUNetRefFactory* mFactory;

  //! Message context
  MsgContext* mContext;

  //! Scope stack to track declaration scopes
  UtStack<NUScope*> mScopeStack;

  //! Chatty mode
  bool mVerbose;

  //! Track when we strip-mine something
  bool mChanged;
};

#endif
