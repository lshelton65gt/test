// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Definition of packing assignment phase
*/

#ifndef PACKASSIGNS_H_
#define PACKASSIGNS_H_

#include "nucleus/Nucleus.h"
#include "util/ConstantRange.h"

class MsgContext;
class AtomicCache;
class Fold;

//! Class to pack assignments together.
/*!
 * This class tries to rewrite assigns of the form:
 *  a[3:2] = x;
 *  a[1:0] = y;
 *
 * Into:
 *  a[3:0] = {x,y};
 *
 * The motivation is that the RHS concat can be constructed
 * more efficiently than repeatedly writing parts of the LHS.
 *
 * A tight loop over 32-bit assignments vs. concat/assign vector
 * showed the concat method to be 10X faster.  Packing is limited
 * to a threshold (32-bits) for efficient concat construction.
 *
 * This transformation is done late (in backend) to improve codegen
 * without adversely impacting vectorization.
 */


//! Statistics for pack assign rewrite phase
class PackAssignsStats
{
public:
   PackAssignsStats() :
    mModules(0),
    mProceduralAssigns(0),
    mContAssigns(0)
  {}

  ~PackAssignsStats() {}

  void module() {++mModules; }
  void proceduralAssign() {++mProceduralAssigns;}
  void contAssign() {++mContAssigns;}
  void print() const;

private:
  UInt64 mModules;
  UInt64 mProceduralAssigns;
  UInt64 mContAssigns;
};


class PackAssigns
{
public:
  //! constructor
  PackAssigns(  NUNetRefFactory *netref_factory,
		MsgContext *msg_context,
		AtomicCache *str_cache,
                UInt32 size_threshold,
                ArgProc *arg,
                Fold* folder,
                PackAssignsStats *pack_ass_stats );
  //! destructor
  ~PackAssigns();

  //! Traverse the module, rewriting assigns
  void design(NUDesign *d);


private:
  //! Hide copy and assign constructors.
  PackAssigns(const PackAssigns&);

  PackAssigns& operator=(const PackAssigns&);
  //! Traverse the module, rewriting assigns
  void module(NUModule *this_module);

  //! Try to rewrite continuous assigns.  Return true if anything was done.
  bool contAssigns(NUModule *module);

  //! Try to rewrite assigns in tasks and functions.  Return true if anything was done.
  bool tfs(NUModule *module);


  //! Utility to get bit and part select lvalues and ranges.
  bool getBitOrPart(NULvalue *lhs, NULvalue **lv,SInt32 *msb, SInt32 *lsb);

  //! Checkin if two Lvalues can be catenated together, and return
  //! a new Lvalue if so.
  NULvalue * catenateLvalues( NULvalue *lhs_1, NULvalue *lhs_2,
                              NUExpr* rhs_1, NUExpr* rhs_2,
                              bool *left_to_right );

  //! Copy an expression or a concat's children into an list. 
  //! \retval true if a concat expression was copied into the vector 
  bool copyExprOrConcat( NUExpr *e, NUExprVector *v);

  //! Determins if the uses or defs of a pair of assigns overlap at all.
  bool assignsOverlap( NUAssign *a1, NUAssign *a2 );

  //! If two assignments write to contiguous parts of a vector,
  //! create a new assignment that combines them.
  bool combineAssigns( NUAssign *a1, NUAssign *a2, NULvalue **lhs,
                       NUExpr ** rhs );


  //! Try to rewrite assigns in structured procs.  Return true if anything was
  //! done.
  bool structuredProcs(NUModule *module);

  //! Try to rewrite assigns in the block.  Return true if anything was done.
  bool blockScope(NUBlockScope *block);

  //! Try to rewrite assigns in the stmt list.  Return true if anything was done.
  bool stmtList(NUStmtList &stmts);

  //! Try to rewrite assigns at/under the given stmt.  Return true if anything was done.
  bool stmt(NUStmt *stmt);

  //! Try to rewrite assigns in the if stmt.  Return true if anything was done.
  bool ifStmt(NUIf *stmt);

  //! Try to rewrite assigns in the case stmt.  Return true if anything was done.
  bool caseStmt(NUCase *stmt);

  //! Try to rewrite assigns in the for stmt.  Return true if anything was done.
  bool forStmt(NUFor *stmt);


  //! Netref factory
  NUNetRefFactory *mNetRefFactory;

  //! Message context
  MsgContext *mMsgContext;

  //! String cache
  AtomicCache *mStrCache;

  //! Size of LHS vector must be above this when rewriting \<vector\> = \<concat\>
  UInt64 mSizeThreshold;

  //! Class to hold the pack assign stats
  PackAssignsStats *mStats;

  ArgProc *mArg;

  //! fold object
  Fold* mFold;
  
  //! Flag turned on for verbose dumping of pack assignment operation.
  bool mVerbose;
};


#endif
