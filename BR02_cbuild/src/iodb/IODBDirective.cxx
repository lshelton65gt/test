// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/CarbonTypes.h"
#include "util/UtIOStream.h"
#include "util/Zstream.h"
#include "iodb/IODBDirective.h"

const char* IODBDirective::dirToString (DesignDirective dir)
{
  switch (dir)
  {
    case eFastClock:
      return "fastClock";
      break;

    case eSlowClock:
      return "slowClock";
      break;

    case eFastReset:
      return "fastReset";
      break;
    
    case eAsyncReset:
      return "asyncReset";
      break;

    case eObserve:
      return "observeSignal";
      break;

    case eTraceSignal:
      return "traceSignal";
      break;

    case eScObserve:
      return "scObserveSignal";
      break;

    case eForce:
      return "forceSignal";
      break;

    case eDeposit:
      return "depositSignal";
      break;

    case eDepositFrequent:
      return "depositSignalFrequent";
      break;

    case eDepositInfrequent:
      return "depositSignalInfrequent";
      break;

    case eScDeposit:
      return "scDepositSignal";
      break;

    case eIgnoreOutput:
      return "ignoreOutput";
      break;

    case eCollapseClocks:
      return "collapseClocks";
      break;

    case eInputDir:
      return "input";
      break;

    case eCModuleBegin:
      return "cModuleBegin";
      break;

    case eCPortBegin:
      return "cPortBegin";
      break;

    case eCNullPortBegin:
      return "cNullPortBegin";
      break;

    case eCTiming:
      return "cTiming";
      break;

    case eCFanin:
      return "cFanin";
      break;

    case eCPortEnd:
      return "cPortEnd";
      break;

    case eCNullPortEnd:
      return "cNullPortEnd";
      break;

    case eCModuleEnd:
      return "cModuleEnd";
      break;

    case eClockSpeed:
      return "clockSpeed";
      break;

    case eExpose:
      return "exposeSignal";
      break;

    case eCFunctionBegin:
      return "cFunctionBegin";
      break;

    case eCFunctionEnd:
      return "cFunctionEnd";
      break;

    case eCTaskBegin:
      return "cTaskBegin";
      break;

    case eCTaskEnd:
      return "cTaskEnd";
      break;

    case eCArg:
      return "cArg";
      break;
      
    case eCHasSideEffect:
      return "cHasSideEffect";
      break;

    case eCCallOnPlayback:
      return "cCallOnPlayback";
      break;

    case eSubstituteModule:
      return "substituteModule";
      break;

    case eSubstituteEntity:
      return "substituteEntity";
      break;


    case eTieNet:
      return "tieNet";
      break;

    case eOutputsTiming:
      return "outputsTiming";
      break;
      
    case eInputsTiming:
      return "inputsTiming";
      break;

    case eBlast:
      return "blastNet";
      break;

    case eVectorMatch:
      return "vectorMatch";
      break;

    case eWrapperPort2State:
      return "wrapperPort2State";
      break;

    case eWrapperPort4State:
      return "wrapperPort4State";
      break;

    case eDelayedClock:
      return "delayedClock";
      break;

    case eOnDemandIdleDeposit:
      return "onDemandIdleDeposit";

    case eOnDemandExcluded:
      return "onDemandExcluded";
      break;

    case eMvDeposit:
      return "mvDepositSignal";
      break;

    case eMvObserve:
      return "mvObserveSignal";
      break;
    case eIgnoreSynthCheck:
      return "ignoreSynthCheck";
      break;
  } // switch
  return NULL;
} // const char* IODBNucleus::dirToString

const char* IODBDirective::dirToString (ModuleDirective eType) {
  switch(eType) {
  case eFlattenModule:         return "flattenModule";
  case eFlattenModuleContents: return "flattenModuleContents";
  case eAllowFlattening:       return "allowFlattening";
  case eDisallowFlattening:    return "disallowFlattening";
  case eHideModule:            return "hideModule";
  case eTernaryGateOptimization: return "ternaryGateOptimization";
  case eNoAsyncReset:          return "noAsyncReset";
  case eDisableOutputSysTasks: return "disableOutputSysTasks";
  case eEnableOutputSysTasks:  return "enableOutputSysTasks";
  }
  INFO_ASSERT(0, "Invalid module directive.");
  return NULL;
}

void IODBDirective::print (UtOStream &out) const
{
  for (TokenVector::const_iterator i = mTokens.begin (); i != mTokens.end (); ++i) {
    UtStringArray *strings = *i;
    for (UtStringArray::const_iterator j = strings->begin (); j != strings->end (); j++) {
      out << " " << *j;
    }
  }
  UtString b0;
  mLoc.compose (&b0);
  out << ": " << b0 << "\n";
}

void IODBDesignDirective::print (UtOStream &out) const
{
  out << dirToString (mDirective);
  IODBDirective::print (out);
}

void IODBModuleDirective::print (UtOStream &out) const
{
  out << dirToString (mDirective);
  IODBDirective::print (out);
}

void IODBDirective::pr () const
{
  print (UtIO::cout ());
}

/*virtual*/ IODBDesignDirective::List::~List ()
{
  while (!empty ()) {
    iterator p = begin ();
    IODBDirective *directive = *p;
    erase (p);
    delete directive;
  }
}

/*virtual*/ IODBModuleDirective::List::~List ()
{
  while (!empty ()) {
    iterator p = begin ();
    IODBDirective *directive = *p;
    erase (p);
    delete directive;
  }
}

bool IODBDirective::writeArgs (SourceLocatorFactory &factory, ZostreamDB &out) 
{
  // write the location
  factory.writeDB (out, mLoc);
  // write the value
  out << mValue;
  // write the tokens
  out << numTokens ();
  for (TokenVectorLoop loop = loopTokens (); !loop.atEnd (); ++loop) {
    UtStringArray *strings = *loop;
    out << *strings;
  }
  return true;
}

bool IODBDirective::readArgs (SourceLocatorFactory *tmpFactory, SourceLocatorFactory *factory,
  ZistreamDB &in)
{
  // read the location
  tmpFactory->readDB (in, &mLoc, factory);
  // read the value
  in >> mValue;
  // read the tokens
  UInt32 n_tokens;
  in >> n_tokens;
  while (n_tokens > 0) {
    UtStringArray token;
    in >> token;
    addToken (token);
    n_tokens--;
  }
  return true;
}

/*virtual*/ bool IODBDesignDirective::writeDB (SourceLocatorFactory &factory, ZostreamDB &out)
{
  // write the directive
  out << mDirective;
  // write everything else
  return writeArgs (factory, out);
}

/*virtual*/ bool IODBDesignDirective::readDB (SourceLocatorFactory *tmpFactory, 
  SourceLocatorFactory *factory, ZistreamDB &in)
{
  UInt32 tmp;
  in >> tmp;
  mDirective = static_cast <DesignDirective> (tmp);
  return readArgs (tmpFactory, factory, in);
}

/*virtual*/ bool IODBModuleDirective::writeDB (SourceLocatorFactory &factory, ZostreamDB &out)
{
  // write the directive
  out << mDirective;
  // write everything else
  return writeArgs (factory, out);
}

/*virtual*/ bool IODBModuleDirective::readDB (SourceLocatorFactory *tmpFactory, 
  SourceLocatorFactory *factory, ZistreamDB &in)
{
  UInt32 tmp;
  in >> tmp;
  mDirective = static_cast <ModuleDirective> (tmp);
  return readArgs (tmpFactory, factory, in);
}

IODBDirective::TokenVector::~TokenVector ()
{
  for (iterator it = begin (); it != end (); it++) {
    delete *it;
  } 
}

void IODBDirective::TokenVector::push_back (const UtStringArray &tokens)
{
  UtStringArray *copy = new UtStringArray (tokens);
  UtArray <UtStringArray *>::push_back (copy);
}
