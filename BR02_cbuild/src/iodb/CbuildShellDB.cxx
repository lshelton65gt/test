// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "iodb/CbuildShellDB.h"
#include "util/Zstream.h"
#include "util/UtHashSet.h"
#include "util/UtIOStream.h"
#include "util/UtIndent.h"

extern const char* gCarbonVersion();

// CbuildShellDB
int CbuildShellDB::getTypeTag(UInt32 tagTypeIndex)
{
  SInt32 preRet = tagTypeIndex >> 24;
  int ret = int(preRet);
  return ret;
}

UInt32 CbuildShellDB::getTypeIndex(UInt32 tagTypeIndex)
{
  UInt32 ret = tagTypeIndex & 0xffffff;
  return ret;
}

void CbuildShellDB::printTagTypeIndex(UInt32 indent, UInt32 tagTypeIndex)
{
  UtString indentStr;
  UtIndent indenter(&indentStr);
  indenter.tab(indent);

  TypeIndexTag typeTag = static_cast<TypeIndexTag>(getTypeTag(tagTypeIndex));
  UInt32 typeIndex = getTypeIndex(tagTypeIndex);

  UtIO::cout() << indentStr << "Type Tag: ";
  switch(typeTag)
  {
  case eNoTypeId:
    UtIO::cout() << "none";
    break;
  case eConstValWordId:
    UtIO::cout() << "constant word";
    break;
  case eConstValId:
    UtIO::cout() << "constant DynBV";
    break;
  case eOffsetId:
    UtIO::cout() << "offset";
    break;
  case eExprId:
    UtIO::cout() << "expression";
    break;
  default:
    UtIO::cout() << "undefined";
    break;
  }

  UtIO::cout() << UtIO::endl 
               << indentStr << "Type Index: " << typeIndex 
               << UtIO::endl;
}

void CbuildShellDB::enterTypeTag(UInt32* tagTypeIndex, int typeTag)
{
  INFO_ASSERT(typeTag >= 0, "Invalid type tag.");
  static const UInt32 mask = 0xff;
  {
    SInt32 typeTagL = typeTag;
    INFO_ASSERT((typeTagL & ~mask) == 0, "Invalid bits set on type tag.");
  }
  *tagTypeIndex &= ~(mask << 24);
  *tagTypeIndex |= ((typeTag & mask) << 24);
}

void CbuildShellDB::enterTypeIndex(UInt32* tagTypeIndex, UInt32 typeIndex)
{
  static const UInt32 mask = 0xffffff;
  INFO_ASSERT((typeIndex & ~mask) == 0, "Invalid bits set on type tag.");
  *tagTypeIndex &= ~mask;
  *tagTypeIndex |= (typeIndex & mask);
}

void CbuildShellDB::write(ZostreamDB& out, UInt32 netFlags, 
                          UInt32 signatureIndex, 
                          UInt32 tagtypeIndex,
                          const IODBIntrinsic *intrinsic,
                          const StorageValue& storage,
                          UInt32 exprExist,
                          UInt32 exprFactoryIndex,
                          NodeFlags nodeFlags,
                          const UserType* ut)
{
  out << netFlags;
  out << signatureIndex;
  out << tagtypeIndex;
  out.writePointer( intrinsic );
  int typeTag = getTypeTag(tagtypeIndex);
  if (typeTag == eConstValWordId)
    out << storage.mImmediateStorage;
  else if (typeTag == eConstValId)
    out.writePointer(storage.mLargeStorage);
  else
    out << storage.mOffsetToStorage;

  out << exprExist;
  out << exprFactoryIndex;
  out << (UInt32) nodeFlags;
  out.writePointer(ut);
}

void CbuildShellDB::read(ZistreamDB& in, UInt32* netFlags, 
                         UInt32* signatureIndex, UInt32* tagtypeIndex,
                         IODBIntrinsic **intrinsic, StorageValue* storage,
                         UInt32* exprExist, UInt32* exprFactoryIndex, 
                         NodeFlags* nodeFlags, UserType** ut)
{
  UInt32 nodeFlagsTmp;

  in >> *netFlags;
  in >> *signatureIndex;
  in >> *tagtypeIndex;
  in.readPointer( intrinsic );
  int typeTag = getTypeTag(*tagtypeIndex);
  if (typeTag == eConstValWordId)
    in >> storage->mImmediateStorage;
  else if (typeTag == CbuildShellDB::eConstValId)
    in.readPointer(&storage->mLargeStorage);
  else
    in >> storage->mOffsetToStorage;

  in >> *exprExist;
  in >> *exprFactoryIndex;
  in >> nodeFlagsTmp;
  *nodeFlags = (CbuildShellDB::NodeFlags) nodeFlagsTmp;
  if (ut != NULL) {
    in.readPointer(ut);
  }
}

static const char* scSignature = "CbuildShellDB 1.16";
void CbuildShellDB::writeSignature(ZostreamDB& out)
{
  out << scSignature;
  out << gCarbonVersion();
}

bool CbuildShellDB::readSignature(ZistreamDB& in, UtString* errMsg)
{
  UtString tmpSig;
  in >> tmpSig;
  UtString tmpVersion;
  in >> tmpVersion;

  bool ret = true;
  if (tmpSig.compare(scSignature) != 0)
  {
    ret = false;
    *errMsg << "Signature mismatch - expected '" << scSignature << "' got '" << tmpSig << "'";
  }

  // We are looking for something like this:
  //    "$Revision: 1.31 $, $Date: 2007/09/11 21:14:17 $",
  // If we see anything earlier than 1.4019, then we can't handle it --
  // we don't have a test for it.
  else {
    int major, minor;
    // Don't put the "$" right before the "Revision" because cvs will
    // then mutate the source (!).  skip that $ in tmpVersion by adding 1.
    if ((sscanf(tmpVersion.c_str() + 1, "Revision: %d.%d $", &major, &minor) != 2)
        || (major < 1) || ((major == 1) && minor < 4019))
    {
      ret = false;
      *errMsg << "Version mismatch - expected '" << gCarbonVersion()
              << "' got '" << tmpVersion << "'";
    }
  }

  return ret;
} // bool CbuildShellDB::readSignature

bool CbuildShellDB::isConstantType(int typeCode)
{
  return ((typeCode > CbuildShellDB::eNoTypeId) && 
          (typeCode < CbuildShellDB::eOffsetId));
}

void
CbuildShellDB::writeBranch(ZostreamDB& out, UInt32 hierFlags, StringAtom* atom,
                           const UserType* ut)
{
  out << hierFlags;
  out.writePointer(atom);
  out.writePointer(ut);
}

void
CbuildShellDB::readBranch(ZistreamDB& in, UInt32* hierFlags, StringAtom** atom,
                          UserType** ut)
{
  in >> *hierFlags;
  in.readPointer(atom);
  if (ut != NULL) {
    in.readPointer(ut);
  }
}
