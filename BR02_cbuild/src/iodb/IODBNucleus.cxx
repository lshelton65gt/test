// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/CarbonContext.h"
#include "iodb/IODBNucleus.h"
#include "iodb/IODBDesignData.h"
#include "iodb/ScheduleFactory.h"
#include "exprsynth/Expr.h"
#include "hdl/HdlVerilogPath.h"
#include "hdl/HdlId.h"
#include "util/CbuildMsgContext.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "symtab/STAliasedLeafNode.h"
#include <stdio.h>
#include "util/UtWildcard.h"
#include "nucleus/NUDesign.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"
#include "iodb/IODBTypes.h"
#include "iodb/CbuildShellDB.h"
#include "codegen/CGPortIface.h"
#include "exprsynth/SymTabExpr.h"

STSymbolTableNode*
IODBNucleus::getNode(const STSymbolTableNode* node, STSymbolTable* symtab)
{
  // If a symbol table was passed in, we need to translate from the
  // IODB symbol table to the given symbol table.
  if (symtab != NULL) {
    STSymbolTableNode* tmpNode = symtab->lookup(node);
    ST_ASSERT(tmpNode != NULL, node);
    node = tmpNode;
  }
  return const_cast<STSymbolTableNode*>(node);
}

NUNetElab*
IODBNucleus::getNet(const STSymbolTableNode* node, STSymbolTable* symtab)
{
  // Convert it to the right symbol table
  node = getNode(node, symtab);

  // Get the elaborated net
  NUNetElab *net = NUNetElab::find(node);
  if (net == NULL)
  {
    IOTable::iterator p = mIOTable.find(node);
    ST_ASSERT(p != mIOTable.end(), node);
    const SourceLocator& loc = p->second;
    NUElabBase* base = NUElabBase::find(node);
    mMsgContext->DirNotANet(&loc, *node, base? base->typeStr(): "(null)");
  }
  return net;
}

void IODBNucleus::declareIOSignal(const STSymbolTableNode* node, NetFlags flags)
{
  STSymbolTableNode* ionode = declareSignal(node);
  switch (flags)
  {
  case eInputNet:  mInputs.insert(ionode); break;
  case eBidNet:    mBidis.insert(ionode); break;
  case eOutputNet: mOutputs.insert(ionode); break;
  default: break;
  };
}

void IODBNucleus::declareClock(const STSymbolTableNode* node)
{
  mClocks.insert(declareSignal(node));
}

void IODBNucleus::declareClockTree(const STSymbolTableNode* node)
{
  mClockTree.insert(declareSignal(node));
}

void IODBNucleus::declarePrimaryClock(const STSymbolTableNode* node)
{
  mPrimaryClocks.insert(declareSignal(node));
}

void IODBNucleus::declareAsync(const STSymbolTableNode* node)
{
  mAsyncs.insert(declareSignal(node));
}

void IODBNucleus::declareAsyncOutput(const STSymbolTableNode* node, const STSymbolTableNode* faninNode)
{
  STSymbolTableNode* declNode = declareSignal(node);
  NameSetMap::iterator p = mAsyncOutputs.find(declNode);
  NameSet* fanins = NULL;
  if (p == mAsyncOutputs.end()) {
    fanins = new NameSet;
    mAsyncOutputs.insert(NameSetMap::value_type(declNode, fanins));
  } else {
    fanins = p->second;
  }
  fanins->insert(declareSignal(faninNode));
}

void IODBNucleus::declareAsyncDeposit(const STSymbolTableNode* node)
{
  mAsyncDeposits.insert(declareSignal(node));
}

void IODBNucleus::declareCModelSense(const STSymbolTableNode* node)
{
  mCModelSenseNets.insert(declareSignal(node));
}

void IODBNucleus::declareReset(const STSymbolTableNode* node, ClockEdge edge)
{
  switch (edge)
  {
  case eClockPosedge:
    mAsyncPosResets.insert(declareSignal(node));
    break;
  case eClockNegedge:
    mAsyncNegResets.insert(declareSignal(node));
    break;
  case eLevelHigh:
    mLevelHighResets.insert( declareSignal(node) );
    break;
  case eLevelLow:
    mLevelLowResets.insert( declareSignal(node) );
    break;    
  default:
    ST_ASSERT(0, node);
    break;
  }
}

void IODBNucleus::mapSymNodeToLoc(const STSymbolTableNode* node, 
                                  const SourceLocator& loc)
{
  const NUNet* net = NUNet::find(node);
  if (net)
    NU_ASSERT(! net->isHierRef(), net);
  for (const STSymbolTableNode* p = node; p != NULL; p = p->getParent())
    mIOTable[p] = loc;
}

const SourceLocator* 
IODBNucleus::getLocFromIOTable(const STSymbolTableNode* node) const
{
  const SourceLocator* loc = NULL;
  IOTable::const_iterator p = mIOTable.find(node);
  if (p != mIOTable.end())
    loc = &(p->second);
  return loc;
}

STSymbolTableNode* IODBNucleus::declareSignal(const STSymbolTableNode* node)
{
  // If elaboration has not yet occurred, then make sure node is
  // part of mParseTable
  if (mParseTable != NULL) {
    const STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf != NULL) {
      node = mParseTable->translateLeaf(leaf);
    }
    else {
      const STBranchNode* branch = node->castBranch();
      ST_ASSERT(branch, node);
      node = mParseTable->translateBranch(branch);
    }
  }

  else {
    NUNetElab* net = getNet(node);
    if (net != NULL)
    {
      const SourceLocator& loc = net->getNet()->getLoc();
      mapSymNodeToLoc(node, loc);
    }
  }
  return const_cast<STSymbolTableNode*>(node);
} 

void IODBNucleus::initTagTypes()
{
  // if a node is in the iotable then we must write it out. The type
  // tag on the bom should be initialized to eNoTypeId The value will
  // be overwritten with eOffsetId if the net has storage during
  // symboltable decoration.
  for (IOTable::UnsortedLoop p = mIOTable.loopUnsorted();
       ! p.atEnd(); ++p)
  {
    const STSymbolTableNode* node = p.getKey();
    const STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf && (leaf->getInternalStorage() != NULL) && (leaf == leaf->getStorage()))
    {
      CbuildSymTabBOM::setTypeTag(const_cast<STAliasedLeafNode*>(leaf), CbuildShellDB::eNoTypeId);
    }
  }
}

void IODBNucleus::declareInvalidWaveNet(STSymbolTableNode* node) 
{
  STAliasedLeafNode* a = node->castLeaf();
  if (a != NULL)
    CbuildSymTabBOM::markInvalidWaveNet(a);
}

bool IODBNucleus::isInvalidWaveNet(const STSymbolTableNode* node)
{
  bool ret = true;
  const STAliasedLeafNode* a = node->castLeaf();
  if (a != NULL)
    ret = CbuildSymTabBOM::isInvalidWaveNet(a);
  return ret;
}

void IODBNucleus::declareEnableExpr(const STAliasedLeafNode* node, 
                                    CarbonExpr* expr)
{
  if (mIOTable.find(node) == mIOTable.end()) {
    STAliasedLeafNode* tmpNode = declareSignal(node)->castLeaf();
    ST_ASSERT(tmpNode, node);
    node = tmpNode;
  }
  mEnableExprs[const_cast<STAliasedLeafNode*>(node)] = expr;
}

void IODBNucleus::rememberConstBits(const STAliasedLeafNode* node, 
                                    const DynBitVector& mask,
                                    Carbon4StateVal val)
{
  // Create an entry in the parse table
  if (mParseTable != NULL) {
    STAliasedLeafNode* ioLeaf = mParseTable->translateLeaf(node);
    mConstTable[ioLeaf] = ConstVal(mask, val);
  }
  else {
    declareConstBits(node, mask, val);
  }
}

void IODBNucleus::translateConstBits()
{
  for (ConstTableLoop l(mConstTable); !l.atEnd(); ++l) {
    // Get the data we stored temporarily
    STAliasedLeafNode* ioLeaf = l.getKey();
    ConstVal constVal = l.getValue();

    // Translate the parse symbol in the real symbol table
    STSymbolTableNode* node = mDesignTable->safeLookup(ioLeaf);
    STAliasedLeafNode* leaf;
    if (node == NULL) {
      // We can't handle optimized away nets right now but we want to
      // in the future. This is a possible spot for adding that
      // support.
      leaf = NULL;
    } else {
      leaf = node->castLeaf();
      ST_ASSERT(leaf != NULL, node);
    }

    // Declare the constant bits
    if (leaf != NULL) {
      declareConstBits(leaf, constVal.first, constVal.second);
    }
  }

  // All done, get rid of these so it is recallable
  mConstTable.clear();
}
    
void IODBNucleus::getCurrentConstValue(const STAliasedLeafNode* node, UInt32 bvBitSize, DynBitVector* bv) const
{
  bv->resize(bvBitSize);
  NameValueMap::iterator p = mConstNets->find(const_cast<STAliasedLeafNode*>(node));
  if (p != mConstNets->end()) {
    const DynBitVector* constValue = p->second;
    ST_ASSERT(constValue->size() == bvBitSize, node);
    bv->copy(*constValue);
  }
}

void IODBNucleus::declareConstBits(const STAliasedLeafNode* node, 
                                   const DynBitVector& mask,
                                   Carbon4StateVal val)
{
  if (mIOTable.find(node) == mIOTable.end()) {
    node = declareSignal(node)->castLeaf();
  }

  UInt32 nbits = mask.size();
  UInt32 numBaseWords = (nbits + 31)/32;
  UInt32 bvBitSize = numBaseWords * 32 * 3;

  DynBitVector bv;
  getCurrentConstValue(node, bvBitSize, &bv);

  // set the Mask bits, which are the low order N bits
  
  for (UInt32 i = 0; i < nbits; ++i) {
    if (mask.test(i)) {
      UInt32 valBit = i + numBaseWords * 32;
      UInt32 xzBit = i + 2*numBaseWords * 32;

      bool multiDrive = bv.test(i);     // constant conflict
      UInt32 saveVal = 0;
      if (multiDrive) {
        saveVal = static_cast<int>(bv.test(valBit)) | (bv.test(xzBit) << 1);
        bv.reset(valBit);
        bv.reset(xzBit);
      }
      else {
        bv.set(i); // control
      }

      switch (val) {
      case eValue0:
        break;
      case eValue1:
        bv.set(valBit);
        break;
      case eValueX:
        bv.set(valBit);
        bv.set(xzBit);
        break;
      case eValueZ:
        bv.set(xzBit);
        break;
      }
      if (multiDrive) {
        UInt32 test = static_cast<int>(bv.test(valBit)) | (bv.test(xzBit) << 1);
        if (test != saveVal) {
          // Report an X to indicate the conflict
          bv.set(valBit);
          bv.set(xzBit);
        }
      }
    }
  }
  (*mConstNets)[const_cast<STAliasedLeafNode*>(node)] = mBVPool->alloc(bv);
}

void IODBNucleus::assignOverrideMask(NUConst* expr, const STAliasedLeafNode* node)
{
  if (mIOTable.find(node) == mIOTable.end()) {
    STAliasedLeafNode* tmpNode = declareSignal(node)->castLeaf();
    ST_ASSERT(tmpNode, node);
    node = tmpNode;
  }
  
  UInt32 nbits = expr->determineBitSize();
  UInt32 numBaseWords = (nbits + 31)/32;
  UInt32 bvBitSize = numBaseWords * 32 * 3;

  DynBitVector bv;
  getCurrentConstValue(node, bvBitSize, &bv);

  DynBitVector value, drive;
  expr->getValueDrive(&value, &drive);

  // The drive is the xz mask  
  UInt32 numBaseBits = numBaseWords * 32;
  bv.lpartsel(2*numBaseWords * 32, numBaseBits) = drive;
  // The value is the valueMask
  bv.lpartsel(numBaseWords * 32, numBaseBits) = value;
  // The controlmask is all ones
  bv.setRange(0, nbits, 1);

  (*mConstNets)[const_cast<STAliasedLeafNode*>(node)] = mBVPool->alloc(bv);
}


// create an override mask {control,value,drive} that can be used in write{Bin,Oct,Dec,Hex}XZValToStr()
void IODBNucleus::sGetTempOverrideMask(const NUConst* constVar, DynBitVector* overrideMask, DynBitVector* value)
{
  DynBitVector drive;
  constVar->getValueDrive(value, &drive);

  UInt32 nbits = value->size();
  UInt32 numBaseWords = (nbits + 31)/32;
  UInt32 bvBitSize = numBaseWords * 32 * 3;
  overrideMask->resize(bvBitSize); 

  // The drive is the xz mask  
  UInt32 numBaseBits = numBaseWords * 32;
  overrideMask->lpartsel(2*numBaseWords * 32, numBaseBits) = drive;
  // The value is the valueMask
  overrideMask->lpartsel(numBaseWords * 32, numBaseBits) = *value;
  // The controlmask is all ones
  overrideMask->setRange(0, nbits, 1);
  
}

static void sComplain(const StringAtom* name, const char* msg)
{
  UtIO::cout() << name->str() << ": " << msg << UtIO::endl;
}

void IODBNucleus::translatePrimaryPorts()
{
  /* Get all the toplevel port names and translate them to elaborated
     symboltable nodes
  */
  mPrimaryPorts.clear();
  mPrimaryPorts.reserve(mDesignPortNames.size());
  STSymbolTable* symTab = getDesignSymbolTable();
  // The top-level module name gets set at the end of population.
  const char* topModNameStr = getTopLevelModuleName();
  StringAtom* topModName = mAtomicCache->intern(topModNameStr);
  STSymbolTableNode* topMod = symTab->find(NULL, topModName);
  FUNC_ASSERT(topMod, sComplain(topModName, "Top level module not found in database."));
  for (Loop<AtomVec> p(mDesignPortNames); ! p.atEnd(); ++p)
  {
    StringAtom* name = *p;
    STSymbolTableNode* portName = symTab->find(topMod, name);
    FUNC_ASSERT(portName, sComplain(name, "Top-level port was not elaborated."));
    // Be safe and declare this signal so we make sure it will get
    // written out. Note that this function has to be called after we
    // resolve directives, which removes the parsetable and any
    // declareSignals called use the elaborated table
    mPrimaryPorts.push_back(declareSignal(portName));
  }
}

void IODBNucleus::putPrimaryPorts (const NUNetList& ports)
{
  mDesignPortNames.clear();
  for (NUNetList::const_iterator p = ports.begin(), e = ports.end(); p != e; ++p)
  {
    const NUNet* net = *p;
    mDesignPortNames.push_back(net->getName());
  }
}

bool IODBNucleus::isInputNet(StringAtom* mod, StringAtom* net) const 
{
  if (mInputNetStrings.empty())
    return false;
  UtString mod_net;
  mod_net << mod->str() << '.' << net->str();
  return mInputNetStrings.find(mod_net) != mInputNetStrings.end();
}

IODBNucleus::SymTabBOM::SymTabBOM()
{}

IODBNucleus::SymTabBOM::~SymTabBOM()
{}

STFieldBOM::Data 
IODBNucleus::SymTabBOM::allocBranchData() {
  return new SymTabVal;
}

STFieldBOM::Data 
IODBNucleus::SymTabBOM::allocLeafData() {
  return new SymTabVal;
}

void IODBNucleus::SymTabBOM::freeData(Data* bomdata) {
  SymTabVal* val = castBOM(*bomdata);
  delete val;
  *bomdata = NULL;
}

void IODBNucleus::SymTabBOM::freeBranchData(const STBranchNode*, Data* bomdata) {
  freeData(bomdata);
}

void IODBNucleus::SymTabBOM::freeLeafData(const STAliasedLeafNode*, Data* bomdata) {
  freeData(bomdata);
}

void IODBNucleus::SymTabBOM::preFieldWrite(ZostreamDB&)
{}

STFieldBOM::ReadStatus 
IODBNucleus::SymTabBOM::preFieldRead(ZistreamDB&)
{
  return eReadOK;
}

void IODBNucleus::SymTabBOM::writeLeafData(const STAliasedLeafNode* /*leaf*/, 
                                    ZostreamDB& /*out*/) const
{
}

void IODBNucleus::SymTabBOM::writeBranchData(const STBranchNode* /*branch*/, 
                                             ZostreamDB& /*out*/,
                                             AtomicCache* /*atomicCache*/) const
{
}

STFieldBOM::ReadStatus 
IODBNucleus::SymTabBOM::readLeafData(STAliasedLeafNode* /*leaf*/, ZistreamDB& /*out*/, 
                              MsgContext* /*msg*/)
{
  return eReadIncompatible;
}

STFieldBOM::ReadStatus 
IODBNucleus::SymTabBOM::readBranchData(STBranchNode* /*branch*/, ZistreamDB& /*out*/, 
                                MsgContext* /*msg*/)
{
  return eReadIncompatible;
}

void IODBNucleus::SymTabBOM::printBranch(const STBranchNode* branch) const
{
  printData(branch->getBOMData());
}

void IODBNucleus::SymTabBOM::printLeaf(const STAliasedLeafNode* leaf) const
{
  printData(leaf->getBOMData());
}

void IODBNucleus::SymTabBOM::printData(const Data bomdata) const
{
  const SymTabVal* val = castBOM(bomdata);
  fprintf(stdout, "%p", (void*) val->mSrcLocID);
}

/*
const IODBNucleus::SymTabVal* 
IODBNucleus::SymTabBOM::castBOM(const Data bomdata) const
{
  return static_cast<const SymTabVal*>(bomdata);
}
*/

IODBNucleus::SymTabVal* 
IODBNucleus::SymTabBOM::castBOM(Data bomdata)
{
  return static_cast<SymTabVal*>(bomdata);
}

IODBNucleus::IODBNucleus(AtomicCache* cache, STSymbolTable* st,
                         MsgContext* msgs,
                         ArgProc* args,
                         SourceLocatorFactory* slf,
                         SCHScheduleFactory* sf) :
  IODB(cache, st, msgs, sf),
  mArgs(args),
  mParseIOTable(NULL),
  mParseTable(NULL),
  mLineNumber(0),
  mFilename(NULL),
  mDesign(NULL),
  mCModules(NULL),
  mSourceLocatorFactory(slf),
  mDirLogFile(NULL),
  mParsingDirectivesFile(false),
  mPlaybackAllCModels(false),
  mDeadBitWave(eValueX),
  mUndrivenBitWave(eValueZ),
  mDDSymTabs(NULL)
{
  mCModelContext = NULL;
  mInstanceMap = NULL;
  mHdl->putAllowWildcard(true);
  mCModules = new CModules;
  mCTFs = new CTFs;
  if(sf)
    sf->putIODB(this);
  mRemoveNets = new NameSet;
  mOutputsTimingEvents = new ClockEvents;
  mInputsTimingEvents = new ClockEvents;
  mCollapseNets = NULL;

  CbuildSymTabBOM* bom = getBOMManager();
  bom->putBVFactory(mBVPool);

  // Add the set of capabilites here
  // If you add a capability you must up the cIODBVersion number in
  // IODB.cxx, so old libcarbon's can recognize that the database is
  // incompatible.
  addCapability("deposit.v3");
  addCapability("branchbom.v1");
  addCapability("wrapperports.v1");
  addCapability("scheduletriggers.v1");
  addCapability("ondemandnets.v1");
  addCapability("observe.v2");
  addCapability("softversion.v1");
  addCapability("ondemandstateoffsets.v1");
  addCapability("asyncOutputs.v2");
  addCapability("sourcelocations.v2");
  addCapability("ondemandruntimeexclusion.v1");
  addCapability("usertypes.v1");
  addCapability("unelaboratedsymbols.v1");
  addCapability("directives.v1");
  addCapability("asyncDeposits.v1");
  addCapability("genericAttributes.v1");
  addCapability("clockGlitchNames.v1");

  mSubstituteDirective[VERILOG] = "substituteModule";
  mSubstituteDirective[VHDL]    = "substituteEntity";

}

IODBNucleus::~IODBNucleus()
{
  // Must be done here since the mCollapsedClocks member uses
  // mParseTable nodes and is hashed
  for (NameSetMapLoop p = loopCollapseClocks(); !p.atEnd(); ++p)
  {
    NameSet* ns = p.getValue();
    delete ns;
  }

  if (mParseTable != NULL)
    delete mParseTable;
  if (mParseIOTable != NULL)
    delete mParseIOTable;
  if (mInstanceMap != NULL)
    delete mInstanceMap;
  deleteDesignDataSymTabs();

  typedef UtVector<const UtString*> StrVec;
  StrVec tmp;
  for (CModules::iterator m = mCModules->begin(); m != mCModules->end(); ++m)
  {
    // Save away the key, we can't delete it till later
    tmp.push_back(m->first);

    CModuleData* data = m->second;
    for (CPortNames::iterator n = data->mInputPorts.begin();
	 n != data->mInputPorts.end(); ++n)
      delete *n;
    for (CPorts::iterator ps = data->mOutputPorts.begin();
	 ps != data->mOutputPorts.end(); ++ps)
    {
      delete ps->first;
      CPort* port = ps->second;
      for (CPort::iterator p = port->begin(); p != port->end(); ++p)
	delete *p;
      delete port;
    }
    delete data;
  }
  for (StrVec::iterator p = tmp.begin(); p != tmp.end(); ++p)
    delete *p;
  delete mCModules;

  tmp.clear();
  for (CTFs::iterator c = mCTFs->begin(); c != mCTFs->end(); ++c)
  {
    // Save away the key, we can't delete it till later
    tmp.push_back(c->first);

    CTFData* data = c->second;
    SeenArgs::iterator a;
    for (a = data->mSeenArgs.begin(); a != data->mSeenArgs.end(); ++a)
      delete *a;
    for (CTFArgsLoop l(data->mArgs); !l.atEnd(); ++l)
      delete *l;
    delete data;
  }
  for (StrVec::iterator p = tmp.begin(); p != tmp.end(); ++p)
    delete *p;
  delete mCTFs;

  INFO_ASSERT(mRemoveNets->empty(), "Remove nets set not empty.");
  delete mRemoveNets;

  freeSubstituteModules();
  delete mOutputsTimingEvents;
  delete mInputsTimingEvents;

  // Delete the wildcards for vectorMatch
  for (WildcardsLoop l = loopVectorMatches(); !l.atEnd(); ++l) {
    UtWildcard* wildcard = *l;
    delete wildcard;
  }

  // The log file is ordinarily closed after parsing, in
  // CarbonContext::freeInterraMemory(), but this is put
  // here for paranoia & subsystem robustness.
  closeLogFile();

  // We should have handled all the const bits
  INFO_ASSERT(mConstTable.empty(), "ConstTable not empty.");

  clearCollapseNets();
}

void IODBNucleus::deleteDesignDataSymTabs()
{
  delete mDDSymTabs;
  mDDSymTabs = NULL;
}

void IODBNucleus::shareBVFactory(DynBitVectorFactory* bvpool) {
  INFO_ASSERT(mBVPool->empty(), "cannot replace bvpool once it is non-empty");
  delete mBVPool;
  mBVPool = bvpool;
  mOwnBVPool = false;
  CbuildSymTabBOM* bom = getBOMManager();
  bom->putBVFactory(bvpool);
}

bool IODBNucleus::isProtectedMutable(const NUNet* net) const
{
  return (mProtectedMutableNets.find(net) != mProtectedMutableNets.end());
}

bool IODBNucleus::isProtectedObservable(const NUNet* net) const
{
  return (mProtectedObservableNets.find(net) != mProtectedObservableNets.end());
}

bool IODBNucleus::isDepositableNet(const NUNet* net) const
{
  return mDepositableNUNets.find(net) != mDepositableNUNets.end();
}

void IODBNucleus::markDepositable(NUNet* net)
{
  mDepositableNUNets.insert(net);
  net->putIsDepositable(true);
}

void IODBNucleus::removeDepositableNUNet(NUNet* net)
{
  mDepositableNUNets.erase(net);
  net->putIsDepositable(false);
}

bool IODBNucleus::isForcibleNet(const NUNet* net) const
{
  return mForcibleNets.find(net) != mForcibleNets.end();
}

void IODBNucleus::markForcible(NUNet* net)
{
  mForcibleNets.insert(net);
  net->putIsForcible(true);
}

bool IODBNucleus::isAsyncResetNet(const NUNet* net) const
{
  return mAsyncResetNets.find(net) != mAsyncResetNets.end();
}

bool IODBNucleus::hasBlastedNets() const
{
  return !mBlastedNetNames.empty();
}

bool IODBNucleus::isBlastedNet(const NUNet* net) const
{
  UtString unelabName;
  net->composeUnelaboratedName(&unelabName);
  return (mBlastedNetNames.find(unelabName) != mBlastedNetNames.end());
}

void IODBNucleus::checkMarkHidden(const STBranchNode* designNode)
{
  NUBase* base = CbuildSymTabBOM::getNucleusObj(designNode);
  const NUModuleElab* elab = dynamic_cast<const NUModuleElab*>(base);
  if (elab)
  {
    const NUModule* module = elab->getModule();
    if (module)
    {
      if (module->isHidden()) {
        const STSymbolTableNode* symNode = designNode;
        mHiddenModuleTops.insert(const_cast<STSymbolTableNode*>(symNode));
        mapSymNodeToLoc(symNode, module->getLoc());
      }
    }
  }
  
}


void IODBNucleus::putClockSpeed(const STSymbolTableNode* node, UInt32 speed)
{
  mClockSpeeds[declareSignal(node)] = speed;
}

void IODBNucleus::putIsSlowClock(const STSymbolTableNode* node)
{
  mSlowClocks.insert(declareSignal(node));
}

void IODBNucleus::putIsFastReset(const STSymbolTableNode* node)
{
  mFastResets.insert(declareSignal(node));
}

UtCustomerDB* IODBNucleus::getCustDB()
{
  return &mCustomerDB;
}

void IODBNucleus::removeNet(STAliasedLeafNode* node)
{
  mRemoveNets->insert(node);
}

void IODBNucleus::putOnDemandStateOffset(STSymbolTableNode *node, UInt32 offset)
{
  mOnDemandStateOffsets[node] = offset;
}

void IODBNucleus::putOnDemandStateSize(UInt32 size)
{
  mOnDemandStateSize = size;
}

void IODBNucleus::putOnDemandRuntimeExcluded(STSymbolTableNode *node)
{
  mOnDemandRuntimeExcludedNets.insert(node);
}

void IODBNucleus::addStringAttribute(const UtString& name, const UtString& value)
{
  mStringAttrMap[name] = value;
}

void IODBNucleus::addIntAttribute(const UtString& name, UInt32 value)
{
  mIntAttrMap[name] = value;
}

void IODBNucleus::removeOnDemandIdleDeposit(STSymbolTableNode *node)
{
  mOnDemandIdleDepositNets.erase(node);
}

UInt32 IODBNucleus::addClockGlitchName(const UtString& name)
{
  UInt32 index = mClockGlitchNames.size();
  UtString* allocName = new UtString(name);
  mClockGlitchNames.push_back(allocName);
  return index;
}

void IODBNucleus::flushRemovedNets(NameSetMap* netMap)
{
  // Remove the node from any net map by visiting
  // the nodes so that we don't have a N*M algorithm.
  typedef UtVector<STSymbolTableNode*> NodeVec;
  typedef NodeVec::iterator NodeVecIter;
  NodeVec fullRemove;
  typedef NameSet::UnsortedLoop NameSetULoop;
  for (NameSetMapLoop p = netMap->loopSorted(); !p.atEnd(); ++p) {
    // If the key gets removed, then remove all the nets associated with it
    STSymbolTableNode* node = p.getKey();
    NameSet* ns = p.getValue();
    if (mRemoveNets->find(node) != mRemoveNets->end()) {
      fullRemove.push_back(node);
      delete ns;
    } else {
      // Check the name set for removed nets
      NodeVec removeNets;
      for (NameSetULoop l = ns->loopUnsorted(); !l.atEnd(); ++l) {
	STSymbolTableNode* alias = *l;
	if (mRemoveNets->find(alias) != mRemoveNets->end())
	  removeNets.push_back(alias);
      }

      // Remove any of the nets from the set
      for (NodeVecIter i = removeNets.begin(); i != removeNets.end(); ++i) {
	STSymbolTableNode* alias = *i;
	ns->erase(alias);
      }

      // If the alias list is empty, remove the entire directive
      if (ns->empty()) {
	fullRemove.push_back(node);
	delete ns;
      }
    } // else
  } // for
  for (NodeVecIter i = fullRemove.begin(); i != fullRemove.end(); ++i) {
    STSymbolTableNode* node = *i;
    NameSetMap::iterator pos = netMap->find(node);
    ST_ASSERT(pos != netMap->end(), node);
    netMap->erase(pos);
  }
}

void IODBNucleus::flushRemovedNets()
{
  // The mCollapsedClocks and mAsyncOutputs are name set maps
  flushRemovedNets(&mCollapsedClocks);
  flushRemovedNets(&mAsyncOutputs);

  // The other sets can be removed quickly 
  for (NameSet::UnsortedLoop l = mRemoveNets->loopUnsorted(); !l.atEnd(); ++l)
  {
    STSymbolTableNode* node = *l;
    mForcedNets.erase(node);
    mExposedNets.erase(node);
    mFastClocks.erase(node);
    mSlowClocks.erase(node);
    mFastResets.erase(node);
    mObservedNets.erase(node);
    mTracedNets.erase(node);
    mDepositNets.erase(node);
    mIgnoreOutputNets.erase(node);
    mInputs.erase(node);
    mOutputs.erase(node);
    mBidis.erase(node);
    mClocks.erase(node);
    mClockTree.erase(node);
    mPrimaryClocks.erase(node);
    mAsyncs.erase(node);
    mAsyncDeposits.erase(node);
    mCModelSenseNets.erase(node);
    mAsyncPosResets.erase(node);
    mAsyncNegResets.erase(node);
    mTieNets->erase(node);
    mClockSpeeds.erase(node);
    mLevelHighResets.erase(node);
    mLevelLowResets.erase(node);
    mWrapPort2StateNets.erase(node);
    mWrapPort4StateNets.erase(node);
    mOnDemandIdleDepositNets.erase(node);
    mOnDemandExcludedNets.erase(node);

    mConstNets->erase(node);
    mIOTable.erase(node);
  } // for

  mRemoveNets->clear();
}

const DynBitVector* IODBNucleus::isTieNet(const STSymbolTableNode* node) const
{
  // If we haven't elaborated yet, convert this symbol table node to a
  // parse table node.
  const STSymbolTableNode* srchNode = node;
  if (mParseTable != NULL)
    srchNode = mParseTable->lookup(node);

  const DynBitVector* value = NULL;
  NameValueMap::iterator pos = mTieNets->find(srchNode);
  if (pos != mTieNets->end())
    value = pos->second;
  return value;
}

void
IODBNucleus::addTypeIntrinsic( const NUNet *net )
{
  // We don't need to worry about the scalar intrinsic.  This code is
  // here to ensure that all types in the original design (before port
  // splitting) have intrinsics.  Scalars can't be split, so they don't
  // need to be added.  In any case, the first memory or vector
  // intrinsic added will also add the scalar intrinsic.  If a design
  // has nothing but scalars the scalar intrinsic will be added at
  // codegen time.
  if ( net->isBitNet() ) {
    return;
  }
  const NUVectorNet *vec = dynamic_cast<const NUVectorNet*>( net );
  if ( vec != NULL ) {
    const ConstantRange *width = vec->getDeclaredRange();
    typeDictionary()->addVectorIntrinsic( width->getMsb(), width->getLsb() );
    return;
  }
  const NUMemoryNet *mem = dynamic_cast<const NUMemoryNet*>( net );
  if ( mem != NULL ) {
    // note in the following we insert incorrect dimension information for memories that were declared with more than 2 dimensions.  
    // This dimension information will be corrected later (perhaps codegen) by IODBIntrinsic* sComputeIntrinsic(). 
    // So for now we use the first two dimensions, neither is normalized
    const ConstantRange *width = mem->getRange(0);
    const ConstantRange *depth = mem->getRange(1);
    const bool hasMoreThan2 = ( mem->getNumDims() > 2);
    NU_ASSERT ( ( (! mem->isResynthesized()) || hasMoreThan2 || (*width == *(mem->getDeclaredWidthRange()))), net );
    NU_ASSERT ( ( (! mem->isResynthesized()) || hasMoreThan2 || (*depth == *(mem->getDeclaredDepthRange()))), net );
    typeDictionary()->addMemoryIntrinsic( depth->getMsb(), depth->getLsb(),
                                          width->getMsb(), width->getLsb() );
    return;
  }
  const NUTempMemoryNet *tmem = dynamic_cast<const NUTempMemoryNet*>( net );
  if ( tmem != NULL ) {
    const NUMemoryNet* mem = tmem->getMaster();
    // note in the following we insert incorrect dimension information for memories that were declared with more than 2 dimensions.  
    // This dimension information will be corrected later (perhaps codegen) by IODBIntrinsic* sComputeIntrinsic(). 
    // So for now we use the first two dimensions, neither is normalized
    const ConstantRange *width = mem->getRange(0);
    const ConstantRange *depth = mem->getRange(1);
    const bool hasMoreThan2 = ( mem->getNumDims() > 2);
    NU_ASSERT ( ( (! mem->isResynthesized()) || hasMoreThan2 || (*width == *(mem->getDeclaredWidthRange()))), net );
    NU_ASSERT ( ( (! mem->isResynthesized()) || hasMoreThan2 || (*depth == *(mem->getDeclaredDepthRange()))), net );
    typeDictionary()->addMemoryIntrinsic( depth->getMsb(), depth->getLsb(),
                                          width->getMsb(), width->getLsb() );
    return;
  }
  if ( net->isVirtualNet() or net->isCompositeNet() or net->isArrayNet() ) {
    return;
  }
  NU_ASSERT( false, net );
  return;
}

static void sAddNetElab(STAliasedLeafNode* leaf, NUNetElabSet* elabs)
{
  NUNetElab* elabNet = NUNetElab::find(leaf);
  ST_ASSERT(elabNet, leaf);
  elabs->insert(elabNet);
}

void IODBNucleus::appendProtectedNetElabs(NUNetElabSet* elabs, NUDesign* design)
{
  bool hierRefNets = mArgs->getBoolValue( CRYPT("-multiLevelHierTasks") );
  STSymbolTable* symTab = getDesignSymbolTable();
  for (STSymbolTable::NodeLoop p = symTab->getNodeLoop();
       ! p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf)
    {
      NUNet* net = NUNet::find(leaf);
      if( net ) {
	// Fix for mindspeed m27483 and test/hierref_unique/ms_fixup
	// Without this the hier ref fixups may reference dead nets.
	//
        // If enabling multiLevelHierRefs, hier reffed nets may not be included
	// in any live flow.  multiLevelHierRefs does not create flow for
	// hier ref tasks internals so hier refs within them may not be part
	// of any elaborated flow.
	// For multiLevelHierRefs, consider any hier reffed nets
	// as protected, not just the ones the user marked.
        //   net->isProtected() includes nets referred to hierarchically.
        //   iodb->isProtected() only includes nets marked by the user.
        if( isProtected(net) || (hierRefNets && net->isProtected( this )) )
          sAddNetElab(leaf, elabs);
      }
    }
  }

  // i/os are not in the protected set
  //now, run through primary ports and make sure those are allocated
  NUModuleList topLevels;
  design->getTopLevelModules(&topLevels);
  INFO_ASSERT(topLevels.size() == 1, "More than one root in design.");

  NUModule* topMod = topLevels.back();
  STSymbolTableNode* topNode = symTab->find(NULL, topMod->getName());
  NU_ASSERT(topNode, topMod);
  STBranchNode* rootNode = topNode->castBranch();
  NU_ASSERT(rootNode, topMod);

  NUNetList rootNets;
  topMod->getPorts(&rootNets);
  
  for (NUNetList::iterator q = rootNets.begin(); q != rootNets.end(); ++q)
  {
    NUNet* net = *q;
    STSymbolTableNode* topChild = rootNode->getChild(net->getSymtabIndex());
    NU_ASSERT(topChild, net);
    STAliasedLeafNode* leaf = topChild->castLeaf();
    NU_ASSERT(leaf, net); // top level ports better be leaves.
    if (leaf)
      sAddNetElab(leaf, elabs);
  }
}

bool IODBNucleus::readPortList(StrToken& tok, const SourceLocator& loc, NUPortList* portList)
{
  if( tok.atEnd() || strcmp(*tok, "portsBegin") )
    return false;
  
  ++tok;
  for( ; !tok.atEnd() && strcmp(*tok, "portsEnd"); ++tok ) {
    UtString strTok(*tok);
    portList->push_back( strTok );
  }
  
  if( tok.atEnd() ) {
    mMsgContext->PortListEnd( &loc );
    return false;
  }
  
  ++tok;
  return true;
}

void IODBNucleus::freeSubstituteModules()
{
  // Free the memory for storing the sustituteModule directives.
  // Check for unused directives.
    for(int i = Lang_MIN; i < Lang_MAX; i++) {
	while(! mSubstituteDUMap[i].empty())   {
	    NUSubstituteModuleMap::iterator p = mSubstituteDUMap[i].begin();
	    NUSubstituteModuleData* smd = p->second;
	    if( smd ) {
		// Error if this substituteModule command was not used.
		if( !smd->mFound ) {
		    mMsgContext->SubstituteModuleNotFound( p->first.c_str() );
		}
		delete smd;
	    }
	    mSubstituteDUMap[i].erase(p);
	}
    }
  // No need to free mCaseInsensitiveSubstituteDUMap since it is a copy of the data
  // from mSubstituteDUMap[i].
}

bool IODBNucleus::addSubstituteModule(StrToken& tok, const SourceLocator& loc, Lang lang)
{
  // Get the module name, make sure it is there
  const char* modName;
  if (!getCModelName(mSubstituteDirective[lang].c_str(), tok, loc, &modName))
    return false;
  UtString modName1(modName);

  NUPortList portList1;
  readPortList(tok, loc, &portList1);

  if (!getCModelName(mSubstituteDirective[lang].c_str(), tok, loc, &modName))
    return false;
  UtString modName2(modName);

  NUPortList portList2;
  readPortList(tok, loc, &portList2);
    
  if( portList1.size() != portList2.size() ) {
    mMsgContext->PortListSize( &loc );
    return false;
  }

  // Look for the original module in maps of other LANG, if found error out
  for(int i = Lang_MIN; i < Lang_MAX; i++) {
      if(lang == i) continue;
      NUSubstituteModuleMap::iterator p = mSubstituteDUMap[i].find(modName1);
      if (p != mSubstituteDUMap[i].end()) {
	  mMsgContext->SubstituteDUFound( modName1.c_str() );
	  return false;
      }
  }
  
  // Add this module substitution into the map.
  NUSubstituteModuleData* smd = new NUSubstituteModuleData( modName2 );
  (mSubstituteDUMap[lang])[ modName1 ] = smd;
  // Lower case the module name for case insensitive substitute module map.
  // This is used when comparing module names in VHDL scope.
  modName1.lowercase();
  (mCaseInsensitiveSubstituteDUMap[lang])[ modName1 ] = smd;

  // Build the port map.
  if( portList1.size() > 0 ) {
    NUPortMap* pm = new NUPortMap();
    smd->mPortMap = pm;
    for(UInt32 i=0; i<portList1.size(); ++i)
      (*pm)[ portList1[i] ] = portList2[i];
  }
  
  return true;
}


bool IODBNucleus::findSubstituteModule ( const UtString& orig_mod,
                                         bool case_insensitive_search,
                                         UtString *new_mod,
				         NUPortMap **portMap,
					 IODBNucleus::Lang& lang)
{
    bool found = false;
    (*new_mod) = "";
    if (portMap != NULL) { (*portMap) = NULL; }

    for(int i = Lang_MIN; i < Lang_MAX; i++) {
	NUSubstituteModuleData* smd = NULL;
	if (case_insensitive_search) {
	    // Looking for a case insensitive name match for VHDL scope.
	    NUCaseInsensitiveSubstituteModuleMap::iterator p =
		mCaseInsensitiveSubstituteDUMap[i].find(orig_mod);
	    if (p != mCaseInsensitiveSubstituteDUMap[i].end()) {
		smd = p->second;
	    }
	} else {
	    // Looking for a case sensitive name match in Verilog scope.
	    NUSubstituteModuleMap::iterator p =
		mSubstituteDUMap[i].find(orig_mod);
	    if (p != mSubstituteDUMap[i].end()) {
		smd = p->second;
	    }
	}

	if (smd != NULL) {
	    found = true;
	    smd->mFound = true;
	    (*new_mod) = smd->mName;
	    if (portMap != NULL) {
		(*portMap) = smd->mPortMap;
	    }
	    lang = (IODBNucleus::Lang)i; // return the new modules language
	    return found;
	}
    }
    return found;
}

IODBNucleus::ClockEventsLoop IODBNucleus::loopOutputsTimingEvents()
{
  return ClockEventsLoop(*mOutputsTimingEvents);
}

IODBNucleus::ClockEventsLoop IODBNucleus::loopInputsTimingEvents()
{
  return ClockEventsLoop(*mInputsTimingEvents);
}

void IODBNucleus::closeLogFile() {
  if (mDirLogFile != NULL) {
    if (!mDirLogFile->close()) {
      UtIO::cerr() << mDirLogFile->getErrmsg() << "\n";
    }
    delete mDirLogFile;
    mDirLogFile = NULL;
  }
}

bool IODBNucleus::openLogFile(const char* filename) {
  closeLogFile();
  mDirLogFile = new UtOBStream(filename);
  if (!mDirLogFile->is_open())
  {
    UtIO::cerr() << mDirLogFile->getErrmsg() << "\n";
    delete mDirLogFile;
    mDirLogFile = NULL;
    return false;
  }
  return true;
}

bool IODBNucleus::isInIOTable(const STSymbolTableNode* node) const
{
  return (mIOTable.find(node) != mIOTable.end());
}

IODBNucleus::WildcardsLoop IODBNucleus::loopVectorMatches()
{
  return WildcardsLoop(mVectorMatches);
}

bool IODBNucleus::hasVectorMatches() const
{
  return !mVectorMatches.empty();
}

CbuildSymTabBOM* IODBNucleus::getBOMManager()
{
  CbuildSymTabBOM* bom = (CbuildSymTabBOM*) mDesignTable->getFieldBOM();
  return bom;
}

void IODBNucleus::mapForceSubordinate(STAliasedLeafNode* subord, STAliasedLeafNode* forcible)
{
  mForceSubordinates[subord] = forcible;
}

const STAliasedLeafNode* IODBNucleus::getSubordinateMasterForcible(const STAliasedLeafNode* forceSubordinate) const
{
  const STAliasedLeafNode* ret = NULL;
  LeafToLeafMap::const_iterator p = mForceSubordinates.find(const_cast<STAliasedLeafNode*>(forceSubordinate));
  if (p != mForceSubordinates.end())
    ret = p->second;
  return ret;
}

static void sGetDesignNode(STSymbolTableNode* node, IODB::NameSet* designNodes,
                           CbuildSymTabBOM* symtabBOM)
{
  // Find the original nodes of an expression
  STAliasedLeafNodeVector originalNodes;
  symtabBOM->getOriginalNodes(node->castLeaf(), &originalNodes);
  if (originalNodes.empty())
    // This is either not an expression or an expression with no
    // nodes. In this case just add the passed-in node to the
    // designNodes set.
    designNodes->insert(node);    
  else
  {
    // Put all the originalNodes in the designNodes set
    for (STAliasedLeafNodeVector::iterator p = originalNodes.begin(),
           e = originalNodes.end(); p != e; ++p)
      designNodes->insert(const_cast<STAliasedLeafNode*>(*p));
  }
}

void 
IODBNucleus::computeScheduleTriggerSetMembership(const CGPortIfaceNet* pnet,
                                                 const CNetSet &bothEdgeClks,
                                                 const CNetSet &clksUsedAsData,
                                                 const ClkToEvent &clkEdgeUses)
{
  CbuildSymTabBOM* bomManager = getBOMManager();
  // Could be a vectorized net. Assuming more general expressions,
  // however. So, if this net is in any of the sets, all the original
  // nodes are added to the appropriate triggers set.
  STSymbolTableNode* node = const_cast<STSymbolTableNode*>(pnet->getSymNode());

  const NUNet* net = pnet->getNUNet();
  if (clksUsedAsData.count(net) || 
      isPosedgeReset(node) || 
      isNegedgeReset(node))
    sGetDesignNode(node, &mScheduleTriggersUsedAsData, bomManager);
  
  if (bothEdgeClks.count(net) > 0)
  {
    sGetDesignNode(node, &mPosedgeScheduleTriggers, bomManager);
    sGetDesignNode(node, &mNegedgeScheduleTriggers, bomManager);
  }
  else
  {
    ClkToEvent::const_iterator c = clkEdgeUses.find(net);
    if (c != clkEdgeUses.end())
    {
      const SCHEvent* clkEvent = c->second;
      switch (clkEvent->getClockEdge())
      {
      case eClockPosedge:
      case eLevelHigh:
        sGetDesignNode(node, &mPosedgeScheduleTriggers, bomManager);
        break;
      case eClockNegedge:
      case eLevelLow:
        sGetDesignNode(node, &mNegedgeScheduleTriggers, bomManager);
        break;
      }
    }
  }

  if (isAsyncNode(node) || (pnet->isBid() && (pnet->getChangeIndex() > -1)) || pnet->isRunCombo())
  {
    // This is a bid or an async that needs carbonSchedule to be
    // called on any edge.
    sGetDesignNode(node, &mPosedgeScheduleTriggers, bomManager);
    sGetDesignNode(node, &mNegedgeScheduleTriggers, bomManager);
  }
  
}

void IODBNucleus::saveWrapperSensitivity(const CNetSet &bothEdgeClks,
                                         const PortNetSet &validOutPorts,
                                         const PortNetSet &validInPorts,
                                         const PortNetSet &validTriOutPorts,
                                         const PortNetSet &validBidiPorts,
                                         const CNetSet &clksUsedAsData,
                                         const ClkToEvent &clkEdgeUses)
{
  mLiveInputDirNets.clear();
  mLiveOutputDirNets.clear();
  mPosedgeScheduleTriggers.clear();
  mNegedgeScheduleTriggers.clear();
  mScheduleTriggersUsedAsData.clear();

  // First we need to loop through the all our asyncs looking for
  // expressions. Get the backpointers of those expressions and add
  // those to the pos/neg schedule trigger sets.
  {
    CbuildSymTabBOM* bomManager = getBOMManager();
    NameSet bothEdgeAsyncs;
    for (NameSetLoop q = loopAsyncs(); ! q.atEnd(); ++q)
      sGetDesignNode(*q, &bothEdgeAsyncs, bomManager);

    NameSet posEdgeAsyncs;
    for (NameSetLoop q = loopAsyncPosResets(); ! q.atEnd(); ++q)
      sGetDesignNode(*q, &posEdgeAsyncs, bomManager);

    NameSet negEdgeAsyncs;
    for (NameSetLoop q = loopAsyncNegResets(); ! q.atEnd(); ++q)
      sGetDesignNode(*q, &negEdgeAsyncs, bomManager);
    
    for (NameSet::UnsortedLoop r = bothEdgeAsyncs.loopUnsorted();
         ! r.atEnd(); ++r)
    {
      mPosedgeScheduleTriggers.insert(*r);
      mNegedgeScheduleTriggers.insert(*r);
    }

    for (NameSet::UnsortedLoop r = posEdgeAsyncs.loopUnsorted();
         ! r.atEnd(); ++r)
      mPosedgeScheduleTriggers.insert(*r);

    for (NameSet::UnsortedLoop r = negEdgeAsyncs.loopUnsorted();
         ! r.atEnd(); ++r)
      mNegedgeScheduleTriggers.insert(*r);
  }

  // Now run through each valid set and add them into their respective
  // live sets and calculate trigger set membership.
  for (PortNetSet::const_iterator p = validInPorts.begin(),
         e = validInPorts.end(); p != e; ++p)
  {
    const CGPortIfaceNet* pnet = *p;
    STSymbolTableNode* node = const_cast<STSymbolTableNode*>(pnet->getSymNode());
    mLiveInputDirNets.insert(node);
    computeScheduleTriggerSetMembership(pnet, bothEdgeClks, 
                                        clksUsedAsData,
                                        clkEdgeUses);
  }
  
  for (PortNetSet::const_iterator p = validBidiPorts.begin(),
         e = validBidiPorts.end(); p != e; ++p)
  {
    const CGPortIfaceNet* pnet = *p;
    STSymbolTableNode* node = const_cast<STSymbolTableNode*>(pnet->getSymNode());
    mLiveInputDirNets.insert(node);
    mLiveOutputDirNets.insert(node);

    computeScheduleTriggerSetMembership(pnet, bothEdgeClks, 
                                        clksUsedAsData,
                                        clkEdgeUses);
  }

  for (PortNetSet::const_iterator p = validOutPorts.begin(),
         e = validOutPorts.end(); p != e; ++p)
  {
    const CGPortIfaceNet* pnet = *p;
    STSymbolTableNode* node = const_cast<STSymbolTableNode*>(pnet->getSymNode());
    mLiveOutputDirNets.insert(node);
  }

  for (PortNetSet::const_iterator p = validTriOutPorts.begin(),
         e = validTriOutPorts.end(); p != e; ++p)
  {
    const CGPortIfaceNet* pnet = *p;
    STSymbolTableNode* node = const_cast<STSymbolTableNode*>(pnet->getSymNode());
    mLiveOutputDirNets.insert(node);
  }

}

static void sTranslateNetSet(NUCNetSet* dst, const NUCNetSet& src, CopyContext& cc) {
  for (NUCNetSet::const_iterator p(src.begin()), e(src.end()); p != e; ++p) {
    const NUNet* srcnet = *p;
    const NUNet* dstnet = cc.lookup(const_cast<NUNet*>(srcnet));
    NU_ASSERT(dstnet, srcnet);
    dst->insert(dstnet);
  }
}

void IODBNucleus::copyProtectedNets(CopyContext& cc, const IODBNucleus& src) {
  // Make a new IODB that contains all the protected nets in src iodb, but run
  // through the net-map in the copy context.  I am not, for the moment,
  // attempting to copy the other fields in the src iodb.
  sTranslateNetSet(&mProtectedMutableNets, src.mProtectedMutableNets, cc);
  sTranslateNetSet(&mProtectedObservableNets, src.mProtectedObservableNets,
                   cc);
  sTranslateNetSet(&mDepositableNUNets, src.mDepositableNUNets, cc);
  sTranslateNetSet(&mForcibleNets, src.mDepositableNUNets, cc);
  sTranslateNetSet(&mAsyncResetNets, src.mAsyncResetNets, cc);

  // atom-sets are easy because we share atomic caches
  mHideModuleNames = src.mHideModuleNames;
  mTernaryGateOptimization = src.mTernaryGateOptimization;
  mNoAsyncResetNames = src.mNoAsyncResetNames;
  mDisableOutputSysTasksNames = src.mDisableOutputSysTasksNames;
  mEnableOutputSysTasksNames = src.mEnableOutputSysTasksNames;

  mFlattenModuleNames = src.mFlattenModuleNames;
  mFlattenModuleContentsNames = src.mFlattenModuleContentsNames;
  mAllowFlatteningModuleNames = src.mAllowFlatteningModuleNames;
  mDisallowFlatteningModuleNames = src.mDisallowFlatteningModuleNames;

  mInlineTFNames = src.mInlineTFNames;

  for (NameValueMap::iterator p(src.mTieNets->begin()),
         e(src.mTieNets->end()); p != e; ++p)
  {
    const STSymbolTableNode* src_node = p->first;
    const DynBitVector* val = p->second;
    const STSymbolTableNode* dst_node = mDesignTable->lookup(src_node);
    (*mTieNets)[dst_node] = mBVPool->alloc(*val);
  }     // mNegedgeScheduleTriggers.insert

  // the only NUNetElabSet is mCollapseNets.  Do this at some point.
} // void IODBNucleus::copyProtectedNets
