// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "schedule/Event.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Signature.h"
#include "util/UtIOStream.h"
#include "util/CarbonTypes.h"
#include "util/CarbonTypeUtil.h"
#include "util/Loop.h"

void SCHEvent::print() const
{
  UtString buf;
  compose(&buf, NULL);
  fprintf(stdout, "%s\n", buf.c_str());
  fflush(stdout);
}

void SCHScheduleMask::print() const
{
  UtString buf;
  compose(&buf, NULL);
  fprintf(stdout, "%s\n", buf.c_str());
  fflush(stdout);
}

void SCHSignature::print() const
{
  UtString buf;
  fprintf(stdout, "transitionMask=(");

  if (mTransition != NULL)
    mTransition->compose(&buf, NULL);
  else
    buf = "*empty*";
  fprintf(stdout, "%s), sampleMask=(", buf.c_str());

  buf = "";
  if (mSample != NULL)
    mSample->compose(&buf, NULL);
  else
    buf = "*empty*";
  fprintf(stdout, "%s)\n", buf.c_str());
  fflush(stdout);
}


void SCHScheduleMask::compose(UtString* buf, const STBranchNode* scope,
                              bool includeRoot, bool hierName, const char* separator)
  const
{
  bool first = true;
  for (SortedEvents l = loopEventsSorted(); !l.atEnd(); ++l)
  {
    const SCHEvent* p = *l;
    if (first) { first = false;  }
    else       { *buf += " or "; }
    p->compose(buf, scope, includeRoot, hierName, separator);
  }
}

void SCHEvent::compose(UtString* buf, const STBranchNode* /* scope */,
                       bool includeRoot,
                       bool hierName,
                       const char* separator)
  const
{
  if (isPrimaryInput())
    *buf += "*input*";
  else if (isPrimaryOutput())
    *buf += "*output*";
  else if (isConstant())
    *buf += "*constant*";
  else
  {
    // Put the edge type
    INFO_ASSERT(isClockEvent(), buf->c_str());
    *buf += ClockEdgeString( mEdge );
    
    // Add the priority if it is non-zero
    if (mPriority > 0)
    {
      *buf << mPriority;
    }

    // Add the name
    *buf += " ";
    mClock->compose(buf, includeRoot, hierName, separator);
  }
}
