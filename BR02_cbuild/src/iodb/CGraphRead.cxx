// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "iodb/CGraph.h"
#include "util/Zstream.h"
#include "util/AtomicCache.h"
#include "symtab/STSymbolTable.h"

// for the moment we'll ignore the CarbonDB because it doesn't appear
// that it contains all the design nodes at this point, so it makes
// it impossible to read the cgraph database cleanly.  So we build our
// own symtab on the fly.
bool CGraph::read(const char* filename, UtString* errmsg, CarbonDB* ) {
  ZISTREAMDB(zfile, filename);

  if (zfile.is_open()) {
    char key = '\0';
    UInt32 numRecords = 0;
    UtString buf;
    while ((key != 'Z') && !zfile.fail() && (zfile >> key)) {
      switch (key) {
      case 'A': 
      case 'S': {               // atom, shared-atom
        buf.clear();
        if (! (zfile >> buf)) {
          *errmsg = zfile.getErrmsg();
          return false;
        }
        AtomicCache* ac = (key == 'A') ? mPrivateAtomicCache
          : mSharedAtomicCache;
        StringAtom* sym = ac->intern(buf);
        buf.clear();
        zfile.mapPtr(sym);
        break;
      }
      case 'N': {               // node
        Node* node = new Node;
        mAllNodes.push_back(node);
        if (! node->read(zfile, mSourceLocatorFactory)) {
          *errmsg = zfile.getErrmsg();
          return false;
        }
        zfile.mapPtr(node);
        break;
      }
      case 'R': {               // references made by a node
        Node* node;
        if (! zfile.readPointer(&node) ||
            ! node->readRefs(zfile))
        {
          *errmsg = zfile.getErrmsg();
          return false;
        }
        break;
      }
      case 'P': {               // path
        const STSymbolTableNode* parent = NULL;
        const char* name = NULL;
        if (! zfile.readPointer(&parent) || !zfile.readPointer(&name)) {
          *errmsg = zfile.getErrmsg();
          return false;
        }          
#if USE_CARBON_DB
        const STSymbolTableNode* child = carbonDBFindChild(db, parent, name);
        if (child == NULL) {
          UtString buf;
          parent->compose(&buf);
          *errmsg << "Cannot find net " << name << " in parent " << buf;
          return false;
        }
#else
        StringAtom* atom = mSharedAtomicCache->intern(name);
        const STSymbolTableNode* child = mSymbolTable->find(parent, atom);
        if (child == NULL) {
          // we don't know at this point if this is a leaf or a branch so
          // we'll just always make a branch.  This is temporary anyway
          if (parent == NULL) {
            child = mSymbolTable->createBranch(atom, NULL);
          }
          else {
            STBranchNode* b = const_cast<STBranchNode*>(parent->castBranch());
            child = mSymbolTable->createBranch(atom, b);
          }
        }
#endif

        zfile.mapPtr(child);
        break;
      }
      case 'Z':                    break; // explicit EOF record
      default:
        buf.clear();
        buf << "Invalid key: " << key << ": record " << numRecords;
        zfile.setError(buf.c_str());
        break;
      } // switch
      ++numRecords;
    } // while
  } // if

  if (zfile.fail() && !zfile.eof()) {
    *errmsg = zfile.getErrmsg();
    return false;
  }
  return true;
} // bool CGraph::read

