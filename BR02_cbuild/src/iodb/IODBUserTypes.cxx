// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "iodb/IODBUserTypes.h"
#include "util/Zstream.h"
#include "util/UtIOStream.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"
#include <cmath>
#include "util/DynBitVector.h"
#include "util/UtXmlWriter.h"

#define HASH_SHIFT(x) ((x) << 8)

AtomicCache* UserType::sAtomicCacheForSave = NULL;
IODB*  UserType::sIODB = NULL;

UserType::UserType(CarbonLanguageType lang, 
		   CarbonVerilogType sigType, 
		   CarbonVhdlType vhdlType,
		   UtString* compositeTypeName,
		   bool isRangeRequiredInDeclaration,
		   UtString* libName,
		   UtString* packName)
  : mVerilogType(sigType),
    mVhdlType(vhdlType),
    mLanguageType(lang),
    mSortIndex(0),
    mTypeName(NULL),
    mIsRangeRequiredInDeclaration(isRangeRequiredInDeclaration),
    mLibraryName(NULL),
    mPackageName(NULL)
{
  if (compositeTypeName != NULL)
    mTypeName = sAtomicCacheForSave->intern(*compositeTypeName);
  else
    mTypeName = getIntrinsicTypeName();

  if((libName != NULL) && (packName != NULL))
  { 
    mLibraryName = sAtomicCacheForSave->intern(*libName);
    mPackageName = sAtomicCacheForSave->intern(*packName);
  }
}

UserType::~UserType()
{
}

StringAtom* UserType::getTypeName() const
{
  return mTypeName;
}

StringAtom* UserType::getLibraryName() const
{
  return mLibraryName;
}

StringAtom* UserType::getPackageName() const
{
  return mPackageName;
}

StringAtom* UserType::getIntrinsicTypeName() const
{
  UtString str;

  if (mLanguageType == eLanguageTypeVerilog)
  {
    switch (mVerilogType)
    {
      case  eVerilogTypeInteger: 
        str = "integer"; 
        break;

      case  eVerilogTypeParameter:
        str = "parameter"; 
        break;

      case  eVerilogTypeReal:
        str = "real"; 
        break;

      case  eVerilogTypeTime:
        str = "time"; 
        break;

      case  eVerilogTypeReg:
        str = "reg"; 
        break;

      case  eVerilogTypeSupply0:
        str = "supply0"; 
        break;

      case  eVerilogTypeSupply1:
        str = "supply1"; 
        break;

      case  eVerilogTypeTri:
        str = "tri"; 
        break;

      case  eVerilogTypeTriAnd:
        str = "triand"; 
        break;

      case  eVerilogTypeTriOr:
        str = "trior"; 
        break;

      case  eVerilogTypeTriReg:
        str = "trireg"; 
        break;

      case  eVerilogTypeTri0:
        str = "tri0"; 
        break;

      case  eVerilogTypeTri1:
        str = "tri1"; 
        break;

      case  eVerilogTypeWAnd:
        str = "wand"; 
        break;

      case  eVerilogTypeWire:
        str = "wire"; 
        break;

      case  eVerilogTypeWOr:
        str = "wor"; 
        break;

      case  eVerilogTypeRealTime:
	str = "realtime";
	break;

      case  eVerilogTypeUWire:
	str = "uwire";
	break;

      case  eVerilogTypeUnsetNet:
        str = "unsetnet"; 
        break;

      case  eVerilogTypeUnknown:  
        str = "versigunknown"; 
        break;

    }
  }
  else if(mLanguageType == eLanguageTypeVhdl)
  {
    switch (mVhdlType)
    {
      case  eVhdlTypeBit: 
        str = "bit"; 
        break;

      case  eVhdlTypeStdLogic: 
        str = "std_logic"; 
        break;

      case  eVhdlTypeStdULogic: 
        str = "std_ulogic"; 
        break;

      case  eVhdlTypeBoolean: 
        str = "boolean"; 
        break;

      case  eVhdlTypeInteger: 
        str = "integer"; 
        break;

      case  eVhdlTypeNatural: 
        str = "natural"; 
        break;

      case  eVhdlTypePositive: 
        str = "positive"; 
        break;

      case  eVhdlTypeReal: 
        str = "real"; 
        break;

      case  eVhdlTypeChar: 
        str = "character"; 
        break;

      case eVhdlTypeIntType:
	str = "integer";
	break;

      case  eVhdlTypeUnknown: 
        str = "vhdlunknown"; 
        break;
    }
  }

  return sAtomicCacheForSave->intern(str);
}

bool UserType::dbWrite(ZostreamDB& f) const
{
  SInt32 langInt, verTypeInt, vhdlTypeInt;
  UtString typeName, libName, packName;
  langInt = static_cast<SInt32>(getLanguageType());
  verTypeInt = static_cast<SInt32>(getVerilogType());
  vhdlTypeInt = static_cast<SInt32>(getVhdlType());
  typeName = getTypeName()->str();
  if(mLibraryName != NULL)
    libName = mLibraryName->str();
  if(mPackageName != NULL)
    packName = mPackageName->str();

  f << langInt << verTypeInt << vhdlTypeInt; 

  f << static_cast<SInt32>(getType()); 

  f << typeName;

  if(sIODB->getIODBVersion() >= 14)
  {
    // The library and package names were added in the version 14 of carbon db
    f << libName << packName;
  }

  // Call derived class writes.
  return writeToDB(f);

  //return true;
}


UserType* UserType::dbRead(ZistreamDB& f)
{
  UserType* type = NULL;

  SInt32 langInt, verTypeInt, vhdlTypeInt;
  f >> langInt >> verTypeInt >> vhdlTypeInt;

  CarbonLanguageType lang = static_cast<CarbonLanguageType> (langInt);
  CarbonVerilogType  verType = static_cast<CarbonVerilogType> (verTypeInt);
  CarbonVhdlType vhdlType = static_cast<CarbonVhdlType> (vhdlTypeInt);

  UInt32 db_version = sGetIODB()->getDBVersion();
  
  // Read type integer.
  SInt32 typeVar;
  if (f >> typeVar)
  {
    UtString typeName, libName, packName;
    f >> typeName;

    if(db_version >= 14)
    {
      // The library and package names were added in the version 14 of carbon db
      f >> libName >> packName;
    }

    switch ((UserType::Type)typeVar)
    {
      case UserType::eScalar:
        type = UserScalar::readFromDB(lang, verType, vhdlType, typeName, libName, packName, f);
        break;
      case UserType::eArray:
        type = UserArray::readFromDB(lang, verType, vhdlType, typeName, libName, packName, f);
        break;
      case UserType::eStruct:
        type = UserStruct::readFromDB(lang, verType, vhdlType, typeName, libName, packName, f);
      case UserType::eStructEnd:
        break;
      case UserType::eEnum:
        type = UserEnum::readFromDB(lang, verType, vhdlType, typeName, libName, packName, f);
        break;
    }
  }

  return type;
}


int UserType::compare(const UserType* ut) const
{
  int cmp = (int)(getLanguageType()) - (int)(ut->getLanguageType());

  // Compare types
  if (cmp == 0) 
    cmp = (int)(getType()) - (int)(ut->getType());
  if (cmp == 0) 
    cmp = (int)(getVerilogType()) - (int)(ut->getVerilogType());
  if (cmp == 0)
    cmp = (int)(getVhdlType()) - (int)(ut->getVhdlType());
  if (cmp == 0)
    cmp = strcasecmp(getTypeName()->str(), ut->getTypeName()->str());
  if (cmp == 0)
  {
    // Assert here, if  the library and package are not declared simultaneously
    // for each of the user types.
    if((getLibraryName() == NULL && getPackageName() != NULL)  ||
       (getLibraryName() != NULL && getPackageName() == NULL)  ||
       (ut->getLibraryName() == NULL && ut->getPackageName() != NULL)  ||
       (ut->getLibraryName() != NULL && ut->getPackageName() == NULL))
      INFO_ASSERT(0, "Library and Package info in User type is inconsistent.");

    if(getLibraryName() == NULL && ut->getLibraryName() != NULL)
      cmp = -1;
    else if(getLibraryName() != NULL && ut->getLibraryName() == NULL)
      cmp = 1;
    else if(getLibraryName() != NULL && ut->getLibraryName() != NULL)
    {
      cmp = strcasecmp(getLibraryName()->str(), ut->getLibraryName()->str());
      if(cmp == 0)
        cmp = strcasecmp(getPackageName()->str(), ut->getPackageName()->str());
    }
  }

  // Now compare the internals since type and type names match.
  if (cmp == 0) 
    cmp = compareHelper(ut);
  
  return cmp;
}



bool UserType::operator==(const UserType& other) const
{
  return (compare(&other) == 0);
}


const UserScalar* UserType::castScalar() const
{
  return NULL;
}

const UserArray* UserType::castArray() const
{
  return NULL;
}

const UserStruct* UserType::castStruct() const
{
  return NULL;
}

const UserEnum* UserType::castEnum() const
{
  return NULL;
}


StringAtom* UserType::translateForSave(StringAtom* atom) const
{
  return sAtomicCacheForSave->intern(atom->str());
}


size_t UserType::hash() const
{
  size_t hashVal = HASH_SHIFT( HASH_SHIFT ( HASH_SHIFT(getLanguageType() ) ) ) + 
                   HASH_SHIFT( HASH_SHIFT(getType()) ) + 
                   HASH_SHIFT(getVerilogType()) + 
                   getVhdlType();

  if((mLibraryName != NULL) && (mPackageName != NULL))
  {
    hashVal += UtString::hash(getLibraryName()->str());
    hashVal += UtString::hash(getPackageName()->str());
  }
      

  return hashVal;
}


bool UserType::writeRange(const ConstantRange* range, ZostreamDB& f) const
{
  if (!(f << range->getLsb()) || !(f << range->getMsb())) {
    return false;
  }
  return true;
}

bool UserType::readRange(ConstantRange* range, ZistreamDB& f)
{
  SInt32 msb, lsb;
  if (!(f >> lsb) || !(f >> msb)) {
    return false;
  }
  range->setMsb(msb);
  range->setLsb(lsb);
  return true;
}

void UserType::printRange(const ConstantRange* range, UtOStream& out) const
{
  out << range->getMsb();
  if (range->getMsb() > range->getLsb()) {
    out << " downto ";
  } else {
    out << " to ";
  }
  out << range->getLsb();
}

UserScalar::UserScalar(CarbonLanguageType lang, 
		       CarbonVerilogType sigType, 
		       CarbonVhdlType vhdlType, 
		       ConstantRange* rangeConstraint,
		       UtString* compositeTypeName,
		       bool isRangeRequiredInDeclaration,
                       UtString* libName,
                       UtString* packName)
  : UserType(lang, sigType, vhdlType, compositeTypeName, isRangeRequiredInDeclaration, libName, packName),
    mRangeConstraint(NULL)
{
  if (rangeConstraint) {
    mRangeConstraint = new ConstantRange(*rangeConstraint);
  }
}

const UserScalar* UserScalar::castScalar() const
{
  return this;
}


UserScalar::~UserScalar()
{
  delete mRangeConstraint;
}

void UserScalar::print(UtOStream* out) const
{
  if (out == NULL) {
    out = &UtIO::cout();
  }

  if((mLibraryName != NULL) && (mPackageName != NULL))
  {
    (*out) << "("<< mLibraryName->str() << "." << mPackageName->str() << ") ";
  }

  UtString typeName = getTypeName()->str();
  UtString intrinsicTypeName = getIntrinsicTypeName()->str();
  (*out) << "type " <<  typeName << " is scalar";
  if (typeName != intrinsicTypeName)
    (*out) << " (intrinsic type " << intrinsicTypeName << ")";
  if (mRangeConstraint) {
    if (mIsRangeRequiredInDeclaration) {
      (*out) << " [requires range when declared] ";
    }
    (*out) << " ranged ";
    printRange(mRangeConstraint, *out);
  }
  (*out) << UtIO::endl;
}

UInt32 UserScalar::getUnconstrainedSize() const {
  // Return the size of the type if its range hasn't been constrained

  UInt32 numBits = 1;

  if(mLanguageType == eLanguageTypeVhdl) {
    switch(mVhdlType) {
      case eVhdlTypeBit: 
      case eVhdlTypeBoolean: 
      case eVhdlTypeStdLogic: 
      case eVhdlTypeStdULogic: 
      case eVhdlTypeUnknown:
        numBits = 1;
        break;
      case eVhdlTypeInteger:
      case eVhdlTypeNatural:
      case eVhdlTypePositive:
      case eVhdlTypeIntType:
        numBits = 32;
        break;
      case eVhdlTypeReal:
        // We don't technically support reals in VHDL, but they'd be
        // 64 bits if we did.
        numBits = 64;
        break;
      case eVhdlTypeChar:
        numBits = 8;
        break;
    }
  } 
  else if (mLanguageType == eLanguageTypeVerilog) {
    switch (mVerilogType) {
    case eVerilogTypeReal:
    case eVerilogTypeTime:
    case eVerilogTypeRealTime:
      numBits = 64;
      break;
    case eVerilogTypeInteger:
    case eVerilogTypeParameter:
      numBits = 32;
      break;
    case eVerilogTypeUnknown:
    case eVerilogTypeUnsetNet:
    case eVerilogTypeReg:
    case eVerilogTypeSupply0:
    case eVerilogTypeSupply1:
    case eVerilogTypeTri:
    case eVerilogTypeTriAnd:
    case eVerilogTypeTriOr:
    case eVerilogTypeTriReg:
    case eVerilogTypeTri0:
    case eVerilogTypeTri1:
    case eVerilogTypeWAnd:
    case eVerilogTypeWire:
    case eVerilogTypeWOr:
    case eVerilogTypeUWire:
      numBits = 1;
      break;
    }
  }

  return numBits;
}

// Finds out the number of implemented bits for the user scalar
UInt32 UserScalar::getSize() const {

  int numBits = getUnconstrainedSize();

  // Only worry about contraints when the bit size is more than one
  if(mRangeConstraint && numBits != 1) {
    SInt64 lsb = this->getRangeConstraint()->getLsb();
    SInt64 msb = this->getRangeConstraint()->getMsb();
    SInt64 max;

    if((msb > 0) && (lsb > 0)) 
      max = (msb >= lsb) ? msb : lsb;
    else if((msb < 0) && (lsb < 0)) 
      max = (msb <= lsb) ? -msb : -lsb;
    else 
      max = ((msb - lsb) < 0) ? lsb - msb : msb - lsb;

    // sLog2() returns smallest x such that 2**x >= max.  If max is a
    // power of 2, this will be wrong (e.g. integer range 0 to 16
    // needs 5 bits, not 4), so add 1.
    numBits = DynBitVector::sLog2(max);
    if ((((UInt64)1) << numBits) == (UInt64)max) {
      ++numBits;
    }
  }

  return numBits;
}

bool UserScalar::writeToDB(ZostreamDB& f) const
{
  UInt32 hasRangeConstraint = ((mRangeConstraint != NULL) ? 1 : 0);
  if (!(f << hasRangeConstraint)) {
    return false;
  }
  if (hasRangeConstraint) {
    UInt32 isRangeRequiredInDeclaration = (mIsRangeRequiredInDeclaration == true) ? 1 : 0;
    if (!(f << isRangeRequiredInDeclaration))
      return false;

    return writeRange(mRangeConstraint, f);
  }
  return true;
}

UserType* UserScalar::readFromDB(CarbonLanguageType lang, CarbonVerilogType verType, 
				 CarbonVhdlType vhdlType, UtString& typeName,
                                 UtString& libName, UtString& packName,
				 ZistreamDB& f)
{
  UserType* retType = NULL;
  UInt32 hasRangeConstraint = 0;

  UtString *readLibName = NULL, *readPackName = NULL;
  if((libName.empty() == false) && (packName.empty() == false))
  {
    readLibName = &libName;
    readPackName = &packName;
  }

  // Read the boolean value indicating whether we should
  // be expecting the range constraint for this scalar.
  if (!(f >> hasRangeConstraint)) { 
    return NULL;
  }
  if (hasRangeConstraint) {
    UInt32 rangeRequiredInDeclaration;
    if (!(f >> rangeRequiredInDeclaration)) {
      return NULL;
    }

    bool isRangeRequiredInDeclaration = (rangeRequiredInDeclaration == 1) ? true : false;

    ConstantRange rangeConstraint;
    if (!UserType::readRange(&rangeConstraint, f)) {
      return false;
    }
    retType = new UserScalar(lang, verType, vhdlType, &rangeConstraint, &typeName, 
                             isRangeRequiredInDeclaration, readLibName, readPackName);
  } else {
    retType = new UserScalar(lang, verType, vhdlType, NULL, &typeName, false,
                             readLibName, readPackName);
  }
  return retType;
}

int UserScalar::compareHelper(const UserType* ut) const
{
  // The argument ought to be of scalar type.
  INFO_ASSERT(ut->getType() == UserType::eScalar, "Expecting scalar user type.");

  const UserScalar* us = static_cast<const UserScalar*>(ut);
  // If both scalars don't have range constraints, they match.
  const ConstantRange* cr1 = getRangeConstraint();
  const ConstantRange* cr2 = us->getRangeConstraint();
  if (cr1 == cr2) {
    return 0;
  }

  if((cr1 == NULL) || (cr2 == NULL))
  {
    if(cr1 == NULL)
      return -1;
    else 
      return 1;
  }

  return (int)cr1->compare(*cr2);
}

size_t UserScalar::hash() const
{
  size_t hashVal = HASH_SHIFT(UserType::hash());
  if (mRangeConstraint) {
    hashVal += mRangeConstraint->hash();
  }
  return hashVal;
}


void UserType::writeXml(UtXmlWriter* xmlWriter) const
{
  UtString typeName = getTypeName()->str();
  SInt32 languageTypeInt = static_cast<SInt32>(getLanguageType());
  const char* languageTypeStr = UserType::sGetCarbonLanguageTypeString(getLanguageType());

  SInt32 verilogTypeInt = static_cast<SInt32>(getVerilogType());
  const char* verilogTypeStr = UserType::sGetCarbonVerilogTypeString(getVerilogType());

  SInt32 vhdlTypeInt = static_cast<SInt32>(getVhdlType());
  const char* vhdlTypeStr = UserType::sGetCarbonVhdlTypeString(getVhdlType());

  const StringAtom*  signalIntrinsicAtom = getIntrinsicTypeName();
  const char* signalIntrinsicString = signalIntrinsicAtom->str();

  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mLanguageType");
  xmlWriter->WriteAttribute("Type",  "CarbonLanguageType");
  xmlWriter->WriteAttribute("Value", languageTypeInt);
  xmlWriter->WriteAttribute("Enum",  languageTypeStr);
  xmlWriter->EndElement();

  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mVerilogType");
  xmlWriter->WriteAttribute("Type",  "CarbonVerilogType");
  xmlWriter->WriteAttribute("Value", verilogTypeInt);
  xmlWriter->WriteAttribute("Enum",  verilogTypeStr);
  xmlWriter->WriteAttribute("Intrinsic",  signalIntrinsicString);
  xmlWriter->EndElement();

  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mVhdlType");
  xmlWriter->WriteAttribute("Type",  "CarbonVhdlType");
  xmlWriter->WriteAttribute("Value", vhdlTypeInt);
  xmlWriter->WriteAttribute("Enum",  vhdlTypeStr);
  xmlWriter->EndElement();

  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mTypeName");
  xmlWriter->WriteAttribute("Type",  "StringAtom*");
  xmlWriter->WriteAttribute("Value", typeName);
  xmlWriter->EndElement();

  return;
}

void UserScalar::writeXml(UtXmlWriter* xmlWriter) const
{
  xmlWriter->StartElement("UserScalar");
  
  UserType::writeXml(xmlWriter);

  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mRangeConstraint");
  xmlWriter->WriteAttribute("Type",  "ConstantRange*");
  if(mRangeConstraint == NULL) {
    xmlWriter->WriteAttribute("Pointer", mRangeConstraint);
  }
  else {
    SInt32 lsb = mRangeConstraint->getLsb();
    SInt32 msb = mRangeConstraint->getMsb();
    xmlWriter->WriteAttribute("Msb", msb); 
    xmlWriter->WriteAttribute("Lsb", lsb); 
  }
  xmlWriter->EndElement();

  if (mRangeConstraint) {
    xmlWriter->StartElement("Field");
    xmlWriter->WriteAttribute("Name",  "mIsRangeRequiredInDeclaration");
    xmlWriter->WriteAttribute("Type",  "bool");
    xmlWriter->WriteAttribute("Value",  mIsRangeRequiredInDeclaration);
    xmlWriter->EndElement();
  }

  xmlWriter->EndElement();
}


//////////////////////////////////////////////////////////////////////
UserArray::UserArray(CarbonLanguageType lang, 
		     CarbonVerilogType sigType, 
		     CarbonVhdlType vhdlType,
                     ConstantRange *range, 
		     const UserType* elemType, 
                     bool isPackedArray,
		     UtString* compositeTypeName,
		     bool isRangeRequiredInDeclaration,
                     UtString* libName,
                     UtString* packName)
  : UserType(lang, sigType, vhdlType, compositeTypeName, isRangeRequiredInDeclaration, libName, packName),
    mRange(NULL),
    mElementType(elemType),
    mIsPackedArray(isPackedArray)
{
  if (range) {
    mRange = new ConstantRange(*range);
  }
}

UserArray::~UserArray()
{
  delete mRange;
}

StringAtom* UserArray::getIntrinsicTypeName() const
{
  if (mLanguageType == eLanguageTypeVhdl)
  {
    CarbonVhdlType vhdlType = mVhdlType;

    // Handle the case that while the array name might not be
    // of an intrinsic type, the elements of the array could be
    // of an intrinsic type so for all intents and purposes, the
    // array will behave like an instrinsic array.
    if ((mVhdlType == eVhdlTypeUnknown) && 
	(eScalar == mElementType->getType())) {
      vhdlType = mElementType->getVhdlType();
    }

    UtString str = "vhdlunknown";

    switch(vhdlType)
    {
      case  eVhdlTypeBit: 
	// Only arrays with a NATURAL range (0 to INTEGER'HIGH) can be bit_vectors
	if (mRange != NULL && (mRange->getLsb() >= 0 && mRange->getMsb() >= 0))
	{
	  // This is not an error. If the type declaration from IEEE.numeric_bit.vhd is
	  // used, then signed and unsigned are unconstrained arrays of bits.
	  UtString typeName(mTypeName->str());
	  if (typeName == "signed")
	    str = "signed";
	  else if (typeName == "unsigned")
	    str = "unsigned";
	  else
	    str = "bit_vector";
	}
        break;

      case  eVhdlTypeStdLogic:
	// Only arrays with a NATURAL range (0 to INTEGER'HIGH) can be std_logic_vectors
	if (mRange != NULL && (mRange->getLsb() >= 0 && mRange->getMsb() >= 0))
	{
	  // This is not an error. If the type declaration from IEEE.numeric_std.vhd is
	  // used, then signed and unsigned are unconstrained arrays of std_logic.
	  UtString typeName(mTypeName->str());
	  if (typeName == "signed")
	    str = "signed";
	  else if (typeName == "unsigned")
	    str = "unsigned";
	  else
	    str = "std_logic_vector"; 
	}
        break;

      case  eVhdlTypeStdULogic: 
	// Only arrays with a NATURAL range (0 to INTEGER'HIGH) can be std_ulogic_vectors
	if (mRange != NULL && (mRange->getLsb() >= 0 && mRange->getMsb() >= 0))
	  str = "std_ulogic_vector"; 
        break;

      case  eVhdlTypeChar: 
	// Only arrays with a POSITIVE range (1 to INTEGER'HIGH) can be strings
	if (mRange != NULL && (mRange->getLsb() > 0 && mRange->getMsb() > 0))
	  str = "string"; 
        break;

      case  eVhdlTypeBoolean: 
      case  eVhdlTypeInteger: 
      case  eVhdlTypeNatural: 
      case  eVhdlTypePositive: 
      case  eVhdlTypeReal: 
      case  eVhdlTypeIntType:
      case  eVhdlTypeUnknown: 
        break;
    }

    return sAtomicCacheForSave->intern(str);
  }
  else
  {
    return UserType::getIntrinsicTypeName();
  }
}

UInt32 UserArray::getNumDims() const
{
  UInt32 numDims = 1; // This array dimension.
  if (mElementType->getType() == UserType::eArray) {
    const UserArray* arrayElem = static_cast<const UserArray*>(mElementType);
    numDims += arrayElem->getNumDims();
  }
  return numDims;
}

const UserType* UserArray::getDimElementType(UInt32 dimIdx) const
{
  if (dimIdx == 0) {
    return mElementType;
  } else if (mElementType->getType() == UserType::eArray) {
    const UserArray* arrayElem = static_cast<const UserArray*>(mElementType);
    return arrayElem->getDimElementType(dimIdx-1);
  } else {
    INFO_ASSERT(mElementType->getType() == UserType::eArray,
                "Dimension index larger than number of array dimensions.");
  }
  return NULL;
}

void UserArray::print(UtOStream* out) const
{
  if (out == NULL) {
    out = &UtIO::cout();
  }

  UtString intrinsicTypeName = getIntrinsicTypeName()->str();
  UtString typeName = getTypeName()->str();

  if((mLibraryName != NULL) && (mPackageName != NULL))
  {
    (*out) << "("<< mLibraryName->str() << "." << mPackageName->str() << ") ";
  }

  (*out) << "type " << typeName << " is array ";

  // If so required, a range must be specified when this array is declared
  if (mIsRangeRequiredInDeclaration &&
      (0 != strcasecmp(typeName.c_str(), intrinsicTypeName.c_str())))
    (*out) << " [requires range when declared] ";

  (*out) << "(";
  UserType::printRange(mRange, *out);
  (*out) << ")";

  // For intrinsic array types, there is no need to repeat the type
  if (0 != strcasecmp(typeName.c_str(), intrinsicTypeName.c_str()) && 
      0 != strcasecmp("vhdlunknown", intrinsicTypeName.c_str()))
  {
    (*out) << " (intrinsic type: " << intrinsicTypeName << ")";
  }

  (*out) << " of" <<  UtIO::endl;

  if(mElementType != NULL)
    mElementType->print(out);
}

bool UserArray::writeToDB(ZostreamDB& f) const
{
  UInt32 isRangeRequiredInDeclaration = (mIsRangeRequiredInDeclaration == true) ? 1 : 0;
  if (!(f << isRangeRequiredInDeclaration))
    return false;

  if (!f.writePointer(mElementType)) 
    return false;
  
  UInt32 isPackedArray = mIsPackedArray ? 1 : 0;
  if (!(f << isPackedArray))
    return false;
  
  // Writing ranges using ConstantRange::dbWrite() is an overkill.
  // It writes a signature and a version for every constant range written to file.
  // I'm going to bypass that and just write msb/lsb directly to file.
  return writeRange(mRange, f);
}

UserType* UserArray::readFromDB(CarbonLanguageType lang, 
				CarbonVerilogType verType, 
				CarbonVhdlType vhdlType, 
				UtString& typeName,
				UtString& libName,
				UtString& packName,
				ZistreamDB& f)
{
  UInt32 rangeRequiredInDeclaration;
  if (!(f >> rangeRequiredInDeclaration)) {
    return NULL;
  }

  bool isRangeRequiredInDeclaration = (rangeRequiredInDeclaration == 1) ? true : false;

  UserType* elemType = NULL;
  if (!f.readPointer(&elemType)) {
    return NULL;
  }

  // packed array flag was added in version 16.
  // If we're working with an older symbol table,
  // we can deduce this flag from elemType.
  // In older databases, it was not possible to have
  // more than one packed array dimension. The last dimension
  // will always be a packed array dimension. By definition,
  // the last dimension will have a scalar as an element type.
  UInt32 isPackedArrayFlag = 0;
  UInt32 db_version = sGetIODB()->getDBVersion();
  if (db_version >= 16) {
    if (!(f >> isPackedArrayFlag)) {
      return NULL;
    }
  } else {
    if (elemType->getType() == UserType::eScalar)
    {
      isPackedArrayFlag = 1;
    }
  }
  bool isPackedArray = (isPackedArrayFlag == 1);
    
  ConstantRange range;
  if (!UserType::readRange(&range, f)) {
    return NULL;
  }
  
  UtString *readLibName = NULL, *readPackName = NULL;
  if((libName.empty() == false) && (packName.empty() == false))
  {
    readLibName = &libName;
    readPackName = &packName;
  }

  return new UserArray(lang, 
		       verType, 
		       vhdlType, 
		       &range, 
		       elemType, 
                       isPackedArray,
		       &typeName,
		       isRangeRequiredInDeclaration,
                       readLibName,
                       readPackName);
}

int UserArray::compareHelper(const UserType* ut) const
{
  // The argument ought to be of array type.
  INFO_ASSERT(ut->getType() == UserType::eArray, "Expecting array user type.");

  const UserArray* ua = static_cast<const UserArray*>(ut);
  // Compare array element types.
  int cmp = mElementType->compare(ua->mElementType);
  // Compare array ranges.
  if (cmp == 0) {
    cmp = mRange->compare(*(ua->mRange));
  }
  return cmp;
}

size_t UserArray::hash() const
{
  return HASH_SHIFT(HASH_SHIFT(UserType::hash()) + mElementType->hash()) + mRange->hash();
}

// Finds out the size of the user array.
UInt32 UserArray::getSize() const {
  UInt32 len, elemSize;

  len      = mRange->getLength();
  elemSize = mElementType->getSize();
  return len * elemSize;
}

const UserArray* UserArray::castArray() const
{
  return this;
}

bool UserArray::isVector() const
{
  // It's a vector if it's a 1-D array whose element type is scalar
  UInt32 dims = getNumDims();
  const UserType *elemType = getDimElementType(dims - 1);
  bool ret = ((dims == 1) && (elemType->castScalar() != NULL));
  return ret;
}

bool UserArray::isMemory() const
{
  // It's a memory if it's a 2-D array whose element type is scalar
  UInt32 dims = getNumDims();
  const UserType *elemType = getDimElementType(dims - 1);
  bool ret = ((dims == 2) && (elemType->castScalar() != NULL));
  return ret;
}

const ConstantRange* UserArray::getVectorRange() const
{
  const ConstantRange *ret = NULL;
  if (isVector()) {
    // Use base range
    ret = getRange();
  } else if (isMemory()) {
    // Use range of first dimension, which must be an array because
    // this is a memory
    const UserType *elemType = getDimElementType(0);
    const UserArray *arrayType = elemType->castArray();
    ret = arrayType->getRange();
  }
  return ret;
}

const ConstantRange* UserArray::getMemoryRange() const
{
  const ConstantRange *ret = NULL;
  if (isMemory()) {
    // Use base range
    ret = getRange();
  }
  return ret;
}

void UserArray::writeXml(UtXmlWriter* xmlWriter) const
{
  xmlWriter->StartElement("UserArray");
  
  UserType::writeXml(xmlWriter);

  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mRange");
  xmlWriter->WriteAttribute("Type",  "ConstantRange*");
  if(mRange == NULL) {
    xmlWriter->WriteAttribute("Pointer", mRange);
  }
  else {
    SInt32 lsb = mRange->getLsb();
    SInt32 msb = mRange->getMsb();
    xmlWriter->WriteAttribute("Lsb", lsb);
    xmlWriter->WriteAttribute("Msb", msb);
  }
  xmlWriter->EndElement();


  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mElementType");
  UtString elementTypeString = mElementType->getClassName();
  elementTypeString += "*";
  xmlWriter->WriteAttribute("Type",  elementTypeString);
  if(mElementType == NULL) {
    xmlWriter->WriteAttribute("Pointer", mElementType);
  }
  else {
    mElementType->writeXml(xmlWriter);
  }
  xmlWriter->EndElement();

  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mIsRangeRequiredInDeclaration");
  xmlWriter->WriteAttribute("Type",  "bool");
  xmlWriter->WriteAttribute("Value",  mIsRangeRequiredInDeclaration);
  xmlWriter->EndElement();

  xmlWriter->EndElement();//matches UserArray


}

/////////////////////////////////////////////////////////////////
UserStruct::UserStruct(CarbonLanguageType lang, 
		       CarbonVerilogType sigType, 
		       CarbonVhdlType vhdlType, 
                       UserTypeVector& fields, 
		       AtomVector& fieldNames, 
		       UtString* compositeTypeName,
                       UtString* libName,
                       UtString* packName)
  : UserType(lang, sigType, vhdlType, compositeTypeName, false, libName, packName),
    mFieldTypes(fields),
    mFieldNames(fieldNames)
{
}

UserStruct::~UserStruct()
{
}

void UserStruct::print(UtOStream* out) const
{
  if (out == NULL) 
    out = &UtIO::cout();
  
  if((mLibraryName != NULL) && (mPackageName != NULL))
  {
    (*out) << "("<< mLibraryName->str() << "." << mPackageName->str() << ") ";
  }

  *out << "type " << getTypeName()->str() << " is record " << UtIO::endl;
  for (UInt32 i = 0; i < getNumFields(); ++i) 
  {
    const UserType* ut = getFieldType(i);
    *out << "field: ";
    ut->print();
  }
  *out << "end record" << UtIO::endl;
}

bool UserStruct::writeToDB(ZostreamDB& f) const
{
  if (!f.writePointerContainer(mFieldTypes)) {
    return false;
  }

  // Use the translated StringAtoms for the current AtomicCache
  AtomVector fieldNames;
  UInt32 numFields = getNumFields();
  for (UInt32 i = 0; i < numFields; ++i) {
    StringAtom* translatedName = translateForSave(getFieldName(i));
    fieldNames.push_back(translatedName);
  }
  if (!f.writePointerContainer(fieldNames)) 
    return false;

  return true;
}

UserType* UserStruct::readFromDB(CarbonLanguageType lang, CarbonVerilogType verType, 
				 CarbonVhdlType vhdlType, UtString& typeName,
                                 UtString& libName, UtString& packName,
				 ZistreamDB& f)
{
  UserTypeVector fields;
  if (!f.readPointerContainerVector(&fields)) {
    return NULL;
  }

  AtomVector fieldNames;
  if (!f.readPointerContainerVector(&fieldNames)) {
    return NULL;
  }

  UtString  *readLibName = NULL, *readPackName = NULL;
  if((libName.empty() == false) && (packName.empty() == false))
  {
    readLibName = &libName;
    readPackName = &packName;
  }

  return new UserStruct(lang, verType, vhdlType, fields, fieldNames, &typeName, readLibName, readPackName);
}

int UserStruct::compareHelper(const UserType* ut) const
{
  // The argument ought to be of struct type.
  INFO_ASSERT(ut->getType() == UserType::eStruct, "Expecting struct user type.");

  const UserStruct* us = static_cast<const UserStruct*>(ut);
  int cmp = mFieldTypes.size() - us->mFieldTypes.size();
  // Compare record fields.
  UserTypeVectorCIter itr1 = mFieldTypes.begin();
  UserTypeVectorCIter itr2 = us->mFieldTypes.begin();
  for ( ; (cmp == 0) && (itr1 != mFieldTypes.end()); ++itr1, ++itr2)
  {
    cmp = (*itr1)->compare(*itr2);
  }
  AtomVectorCIter aitr1 = mFieldNames.begin();
  AtomVectorCIter aitr2 = us->mFieldNames.begin();
  for ( ; (cmp == 0) && (aitr1 != mFieldNames.end()); ++aitr1, ++aitr2)
  {
    const StringAtom* atom1 = *aitr1;
    const StringAtom* atom2 = *aitr2;
    cmp = strcasecmp(atom1->str(), atom2->str());
  }
  return cmp;
}

size_t UserStruct::hash() const
{
  size_t hashVal = UserType::hash();
  UInt32 numFields = getNumFields();
  for (UInt32 i = 0; i < numFields; ++i) {
    hashVal += HASH_SHIFT(hashVal) + mFieldTypes[i]->hash();
    hashVal += UtString::hash(getFieldName(i)->str());
  }
  return hashVal;
}

// Finds out the size of the user struct
UInt32 UserStruct::getSize() const {
  UInt32 size = 0;
  UInt32 numFields = getNumFields();

  for (UInt32 i = 0; i < numFields; ++i) {
    size += getFieldType(i)->getSize();
  }
  return size;
}

// This functions saves the elements of the enum into 
// Atomic cache of current symbol table. The atoms of this
// Atomic cache are saved in the symtab.db
void UserStruct::translateStringsForSave()
{
  for(UInt32 i = 0; i < getNumFields(); i++)
  {
    StringAtom *fieldName = getFieldName(i);
    translateForSave(fieldName);
  }
}

const UserStruct* UserStruct::castStruct() const
{
  return this;
}

void UserStruct::writeXml(UtXmlWriter* xmlWriter) const
{
  UtString fieldTypeString;

  xmlWriter->StartElement("UserStruct");
  
  UserType::writeXml(xmlWriter);
  SInt32  numFields = getNumFields();

  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mFieldTypes");
  xmlWriter->WriteAttribute("Type",  "UtArray<UserType*>");


  xmlWriter->StartElement("FieldTypes");
  xmlWriter->WriteAttribute("Count", numFields);
  for(int i = 0 ; i < numFields; i++) {
    UInt32 fieldIndex = (UInt32)i;
    const UserType* fieldUserType = getFieldType(fieldIndex);
    fieldTypeString = fieldUserType->getClassName();
    fieldTypeString += "*";
    xmlWriter->StartElement("FieldType");
    xmlWriter->WriteAttribute("Type",  fieldTypeString);    
    fieldUserType->writeXml(xmlWriter);
    xmlWriter->EndElement();
  }
  xmlWriter->EndElement(); //matches FieldTypes
  xmlWriter->EndElement(); //matches Field


  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mFieldNames");
  xmlWriter->WriteAttribute("Type",  "UtArray<StringAtom*>");

  xmlWriter->StartElement("FieldNames");
  xmlWriter->WriteAttribute("Count", numFields);
  for(int i = 0 ; i < numFields; i++) {
    xmlWriter->StartElement("FieldName");
    xmlWriter->WriteAttribute("Type",  "StringAtom*");    
    UInt32 fieldIndex = (UInt32)i;
    const StringAtom* fieldNameAtom = getFieldName(fieldIndex);
    const char* fieldNameValue= fieldNameAtom->str();
    xmlWriter->WriteAttribute("Value",  fieldNameValue);
    xmlWriter->EndElement();
  }
  xmlWriter->EndElement(); // matches FieldNames
  xmlWriter->EndElement();
  

  xmlWriter->EndElement();


}

///////////////////////////////////////////////////////////////////////////////////////
UserEnum::UserEnum(CarbonLanguageType lang, 
		   CarbonVerilogType sigType, 
		   CarbonVhdlType vhdlType, 
                   ConstantRange *range, 
		   AtomVector& enumElems, 
		   AtomVector& enumEncodingElems,
		   UtString* compositeTypeName,
		   bool isRangeRequiredInDeclaration,
                   UtString* libName,
                   UtString* packName)
  : UserType(lang, sigType, vhdlType, compositeTypeName, isRangeRequiredInDeclaration, libName, packName),
    mRange(NULL),
    mElementNames(enumElems),
    mElementEncodings(enumEncodingElems)
{
  if (range) {
    mRange = new ConstantRange(*range);
  }
}

UserEnum::~UserEnum()
{
  delete mRange;
}

void UserEnum::print(UtOStream* out) const
{
  if (out == NULL) 
    out = &UtIO::cout();

  if((mLibraryName != NULL) && (mPackageName != NULL))
  {
    (*out) << "("<< mLibraryName->str() << "." << mPackageName->str() << ") ";
  }
  
  *out << "type " << getTypeName()->str() << " is enum " << UtIO::endl;
  for (UInt32 i = 0; i < getNumberElems(); ++i) 
    *out << (getElem(i))->str() << "   ";

  UInt32 numEncodingElems = getNumberEncodingElems();
  if (numEncodingElems > 0)
  {
    (*out) <<  UtIO::endl << "ENUM_ENCODING" << UtIO::endl;
    for (UInt32 i=0; i<numEncodingElems; ++i)
      (*out) << (getEncodingElem(i))->str() << "   ";
  }

  if(mRange)
  {
    if (mIsRangeRequiredInDeclaration) {
      (*out) << " [requires range when declared] ";
    }
    (*out) << UtIO::endl << " ranged ";
    printRange(mRange, *out);
  }
    
  *out << UtIO::endl << "end enum" << UtIO::endl;
}

size_t UserEnum::hash() const
{
  size_t hashVal = UserType::hash();
  for(UInt32 i = 0; i < getNumberElems(); i++)
  {
    hashVal += UtString::hash(getElem(i)->str());
  }

  if(mRange)
    hashVal += mRange->hash();

  return hashVal;
}

// Finds out the number of implemented bits for the user enum
UInt32 UserEnum::getSize() const {
  int numBits = 1;

  // Subtyped Enums
  if(mRange) {
    SInt64 lsb = this->getRange()->getLsb();
    SInt64 msb = this->getRange()->getMsb();
    SInt64 max = (msb >= lsb) ? msb : lsb;;

    // sLog2() returns smallest x such that 2**x >= max.  If max is a
    // power of 2, this will be wrong (e.g. enum with 17 values -
    // integer range 0 to 16 - needs 5 bits, not 4), so add 1.
    numBits = DynBitVector::sLog2(max);
    if ((((UInt64)1) << numBits) == (UInt64)max) {
      ++numBits;
    }
  }
  // Encoding Enums. The size is the size of the first encoding.
  else if(!mElementEncodings.empty()) {
    StringAtom* first = mElementEncodings[0];
    numBits = strlen(first->str()); 
  }
  // Enums
  else if(!mElementNames.empty()) {
    UInt32 size = this->getNumberElems();

    // 2**numBits >= size, for smallest numBits
    numBits = DynBitVector::sLog2(size);
  }
  // Default
  else {
    numBits = 32;
  }
  return numBits;
}

const UserEnum* UserEnum::castEnum() const
{
  return this;
}


int UserEnum::compareHelper(const UserType* ut) const
{
  // The argument ought to be of enum type.
  INFO_ASSERT(ut->getType() == UserType::eEnum, "Expecting enum user type.");

  const UserEnum* ue = ut->castEnum();

  int cmp = 0;
  cmp = mElementNames.size() - ue->mElementNames.size();

  // Compare enum elements
  if(cmp == 0)
  {
    for(UInt32 i = 0; i < mElementNames.size(); i++)
    {
      StringAtom* str1 = mElementNames[i];
      StringAtom* str2 = ue->mElementNames[i];
      if( (cmp = strcasecmp(str1->str(), str2->str())) != 0)
        break;
    }
  }
  
  // Compare enum ranges.
  if (cmp == 0) 
  {
    if((mRange != NULL) && (ue->mRange == NULL))
      cmp = 1;
    else if((mRange == NULL) && (ue->mRange != NULL)) 
      cmp = -1;
    else if((mRange != NULL) && (ue->mRange != NULL))
      cmp = mRange->compare(*(ue->mRange));
  }
  return cmp;
}

UserType* UserEnum::readFromDB(CarbonLanguageType lang, CarbonVerilogType verType, 
			       CarbonVhdlType vhdlType, UtString& typeName,
                               UtString& libName, UtString& packName,
			       ZistreamDB& f)
{

  AtomVector enumElems;
  if (!f.readPointerContainerVector(&enumElems))
    return NULL;

  // If this enum has an associated ENUM_ENCODING attribute
  // make sure it is read from the database
  AtomVector enumEncodingElems;
  UInt32 hasEncodingElems = 0;
  f >> hasEncodingElems;
  if (hasEncodingElems)
  {
    if (!f.readPointerContainerVector(&enumEncodingElems)) {
      return NULL;
    }
  }

  ConstantRange* range = NULL;
  ConstantRange rangeRead;
  bool isRangeRequiredInDeclaration = false;

  UInt32 hasRange = 0;
  if (!(f >> hasRange))
    return NULL;

  if(hasRange)
  {
    UInt32 rangeRequiredInDeclaration;
    if (!(f >> rangeRequiredInDeclaration)) {
      return NULL;
    }

    isRangeRequiredInDeclaration = (rangeRequiredInDeclaration == 1) ? true : false;

    if (!UserType::readRange(&rangeRead, f)) 
      return NULL;

    range = &rangeRead;
  }

  UtString *readLibName = NULL, *readPackName = NULL;
  if((libName.empty() == false) && (packName.empty() == false))
  {
    readLibName = &libName;
    readPackName = &packName;
  }

  return new UserEnum(lang, 
		      verType, 
		      vhdlType, 
		      range, 
		      enumElems, 
		      enumEncodingElems,
		      &typeName,
		      isRangeRequiredInDeclaration,
                      readLibName,
                      readPackName);
}

bool UserEnum::writeToDB(ZostreamDB& f) const
{
  UInt32 numElems = getNumberElems();

  AtomVector elemNames;
  for (UInt32 i = 0; i < numElems; ++i) 
  {
    StringAtom* translatedName = translateForSave(getElem(i));
    elemNames.push_back(translatedName);
  }

  if (!f.writePointerContainer(elemNames)) 
    return false;

  // This enum may or may not have an associated ENUM_ENCODING attribute
  // If it does, it needs to be stored in the database
  UInt32 numEncodingElems = getNumberEncodingElems();
  UInt32 hasEnumEncodingElems = numEncodingElems > 0 ? 1 : 0;

  if (!(f << hasEnumEncodingElems))
    return false;

  if (numEncodingElems > 0)
  {
    AtomVector elemEncodingNames;
    for (UInt32 i=0; i<numEncodingElems; ++i)
    {
      StringAtom* translatedName = translateForSave(getEncodingElem(i));
      elemEncodingNames.push_back(translatedName);
    }

    if (!f.writePointerContainer(elemEncodingNames))
      return false;
  }

  // Writing ranges using ConstantRange::dbWrite() is an overkill.
  // It writes a signature and a version for every constant range written to file.
  // I'm going to bypass that and just write msb/lsb directly to file.
  UInt32 hasRange = 0;
  if(mRange != NULL)
    hasRange = 1;

  if (!(f << hasRange))
    return false;

  if(hasRange) {
    UInt32 isRangeRequiredInDeclaration = (mIsRangeRequiredInDeclaration == true) ? 1 : 0;
    if (!(f << isRangeRequiredInDeclaration))
      return false;
    return writeRange(mRange, f);
  }

  return true;
}

// This functions saves the elements of the enum into 
// Atomic cache of current symbol table. The atoms of this
// Atomic cache are saved in the symtab.db
void UserEnum::translateStringsForSave()
{
  for(UInt32 i = 0; i < getNumberElems(); i++)
  {
    StringAtom *elem = getElem(i);
    translateForSave(elem);
  }

  for(UInt32 i = 0; i< getNumberEncodingElems(); i++)
  {
    StringAtom *elem = getEncodingElem(i);
    translateForSave(elem);
  }
}


void UserEnum::writeXml(UtXmlWriter* xmlWriter) const
{
  xmlWriter->StartElement("UserEnum");
  
  UserType::writeXml(xmlWriter);


  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mRange");
  xmlWriter->WriteAttribute("Type",  "ConstantRange*");
  if(mRange == NULL) {
    xmlWriter->WriteAttribute("Pointer", mRange);
  }
  else {
    SInt32 lsb = mRange->getLsb();
    SInt32 msb = mRange->getMsb();
    xmlWriter->WriteAttribute("Msb", msb);
    xmlWriter->WriteAttribute("Lsb", lsb);
  }
  xmlWriter->EndElement();


  SInt32  numElements = getNumberElems();

  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mElementNames");
  xmlWriter->WriteAttribute("Type",  "UtArray<StringAtom*>");

  xmlWriter->StartElement("Elements");
  xmlWriter->WriteAttribute("Count", numElements);
  for(int i = 0 ; i <  numElements; i++) {
    xmlWriter->StartElement("Element");
    UInt32 elemIndex = (UInt32)i;
    StringAtom*  enumElementAtom   = getElem(elemIndex);
    const char*  enumElementValue  = enumElementAtom->str();
    xmlWriter->WriteAttribute("Value",  enumElementValue);
    xmlWriter->EndElement();
  }
  xmlWriter->EndElement(); //matches Elements

  xmlWriter->EndElement();

  SInt32  numEncodings = getNumberEncodingElems();

  xmlWriter->StartElement("Field");
  xmlWriter->WriteAttribute("Name",  "mElementEncodings");
  xmlWriter->WriteAttribute("Type",  "UtArray<StringAtom*>");


  xmlWriter->StartElement("Encodings");
  xmlWriter->WriteAttribute("Count", numEncodings);
  for(int i = 0 ; i <  numEncodings; i++) {
    xmlWriter->StartElement("Encoding");
    UInt32 encodingIndex = (UInt32)i;
    StringAtom* enumEncodingAtom  = getEncodingElem(encodingIndex);
    const char* enumEncodingValue = enumEncodingAtom->str();
    xmlWriter->WriteAttribute("Value",enumEncodingValue);
    xmlWriter->EndElement();
  }
  xmlWriter->EndElement(); //matches Encodings

  xmlWriter->EndElement();
  

  xmlWriter->EndElement();


}


//////////////////////////////////////////////////////////////////////////////////////
UserTypeFactory::UserTypeFactory(AtomicCache* atomicCache, IODB* iodb)
  : mAtomicCache(atomicCache), mNumUniqTypes(0), mIODB(iodb)
{
  UserType::sSetIODB(iodb);
}

UserTypeFactory::~UserTypeFactory()
{
  for (TypeSetCIter itr = mTypeSet.begin(); itr != mTypeSet.end(); ++itr) {
    delete *itr;
  }
}


const UserType* UserTypeFactory::maybeAddScalarType(CarbonLanguageType lang, 
                                                    CarbonVerilogType sigType, 
                                                    CarbonVhdlType vhdlType,
                                                    ConstantRange* rangeConstraint,
						    UtString* compositeTypeName,
						    bool isRangeRequiredInDeclaration,
                                                    UtString* libName,
                                                    UtString* packName)
{
  UserScalar* us = new UserScalar(lang, 
				  sigType, 
				  vhdlType, 
				  rangeConstraint, 
				  compositeTypeName, 
				  isRangeRequiredInDeclaration,
                                  libName,
                                  packName);
  return maybeAddType(us);
}

const UserType* UserTypeFactory::maybeAddArrayType(CarbonLanguageType lang, 
						   CarbonVerilogType sigType, 
                                                   CarbonVhdlType vhdlType,
                                                   ConstantRange *range,
                                                   const UserType* elemType,
                                                   bool isPackedArray,
						   UtString* compositeTypeName,
						   bool isRangeRequiredInDeclaration,
                                                   UtString* libName,
                                                   UtString* packName)
{
  UserArray* ua = new UserArray(lang, 
				sigType, 
				vhdlType, 
				range, 
				elemType, 
                                isPackedArray,
				compositeTypeName, 
				isRangeRequiredInDeclaration,
                                libName,
                                packName);
  return maybeAddType(ua);
}


const UserType* UserTypeFactory::maybeAddStructType(CarbonLanguageType lang, 
						    CarbonVerilogType sigType, 
                                                    CarbonVhdlType vhdlType, 
                                                    UserTypeVector& fields, 
                                                    AtomVector& fieldNames,
						    UtString* compositeTypeName,
                                                    UtString* libName,
                                                    UtString* packName)
{
  UserStruct* us = new UserStruct(lang, 
				  sigType, 
				  vhdlType, 
				  fields, 
				  fieldNames, 
				  compositeTypeName,
                                  libName,
                                  packName);
  return maybeAddType(us);
}

const UserType* UserTypeFactory::maybeAddEnumType(CarbonLanguageType lang, 
						  CarbonVerilogType sigType, 
                                                  CarbonVhdlType vhdlType, 
                                                  ConstantRange *range, 
                                                  AtomVector& enumElems,
						  AtomVector& enumEncodingElems,
						  UtString* compositeTypeName,
						  bool isRangeRequiredInDeclaration,
                                                  UtString* libName,
                                                  UtString* packName)
{
  UserEnum* ue = new UserEnum(lang, 
			      sigType, 
			      vhdlType, 
			      range, 
			      enumElems, 
			      enumEncodingElems, 
			      compositeTypeName,
			      isRangeRequiredInDeclaration,
                              libName,
                              packName);

  return maybeAddType(ue);
}


const UserType* UserTypeFactory::maybeAddType(UserType* ut)
{
  TypeSetIter itr = mTypeSet.find(ut);
  if (itr == mTypeSet.end()) {
    mTypeSet.insert(ut);
    ++mNumUniqTypes;
    ut->setSortIndex(mNumUniqTypes); // indices are 1 based. 0 is invalid value.
  } else {
    delete ut;  
    ut = *itr;
  }
  return ut;
}

bool UserTypeFactory::save(ZostreamDB& f) const
{
  UserTypeVector sortedTypes(mNumUniqTypes);
  getSortedTypes(sortedTypes);
  // This will call writeObject() on each UserType in the set. It will inturn
  // call UserType::dbWrite(). It will also setup pointer to index map which
  // will help us call writePointer() on UserType*, for example in
  // the UserArray::writeToDB() method.
  return f.writePointerValueContainer(sortedTypes);
}

bool UserTypeFactory::restore(ZistreamDB& f)
{
  if (!f.readPointerValueContainer(&mTypeSet)) {
    return false;
  }
  
  return true;
}

void UserTypeFactory::setUserTypesAtomicCache(AtomicCache* atomicCache)
{
  UserType::sSetAtomicCacheForSave(atomicCache);
}

void UserTypeFactory::copyStringsIntoSymTable()
{
  for (TypeSetCIter itr = mTypeSet.begin(); itr != mTypeSet.end(); ++itr)
  {
    UserType* ut = *itr;
    ut->translateStringsForSave();
  }
}

void UserTypeFactory::print(UtOStream* out) const
{
  UserTypeVector sortedTypes(mNumUniqTypes);
  getSortedTypes(sortedTypes);
  for (UserTypeVectorIter itr = sortedTypes.begin();
       itr != sortedTypes.end(); ++itr) {
    (*itr)->print(out);
  }
}

void UserTypeFactory::getSortedTypes(UserTypeVector& sortedTypes) const
{
  for (TypeSetCIter itr = mTypeSet.begin(); itr != mTypeSet.end(); ++itr) {
    UInt32 sortedIndex = (*itr)->getSortIndex();
#ifdef CDB
    INFO_ASSERT(sortedIndex > 0, "User type with invalid sort index found.");
#endif
    sortedTypes[sortedIndex-1] = *itr; // indices are 1 based. 0 is invalid value.
  }
}


const char*  UserType::sGetCarbonLanguageTypeString(CarbonLanguageType languageType) 
{
  switch (languageType)
  { 
  case eLanguageTypeVerilog : return "eLanguageTypeVerilog"; break;
  case eLanguageTypeVhdl : return "eLanguageTypeVhdl"; break;
  case eLanguageTypeUnknown : return "eLanguageTypeUnknown"; break;
  }

  return NULL;
}
const char*  UserType::sGetCarbonVerilogTypeString(CarbonVerilogType verilogType)
{
  switch (verilogType) 
  {
  case eVerilogTypeUnknown : return "eVerilogTypeUnknown"; break;
  case eVerilogTypeUnsetNet : return "eVerilogTypeUnsetNet"; break;
  case eVerilogTypeInteger : return "eVerilogTypeInteger"; break;
  case eVerilogTypeParameter : return "eVerilogTypeParameter"; break;
  case eVerilogTypeReal : return "eVerilogTypeReal"; break;
  case eVerilogTypeTime : return "eVerilogTypeTime"; break;
  case eVerilogTypeReg : return "eVerilogTypeReg"; break;
  case eVerilogTypeSupply0 : return "eVerilogTypeSupply0"; break;
  case eVerilogTypeSupply1 : return "eVerilogTypeSupply1"; break;
  case eVerilogTypeTri : return "eVerilogTypeTri"; break;
  case eVerilogTypeTriAnd : return "eVerilogTypeTriAnd"; break;
  case eVerilogTypeTriOr : return "eVerilogTypeTriOr"; break;
  case eVerilogTypeTriReg : return "eVerilogTypeTriReg"; break;
  case eVerilogTypeTri0 : return "eVerilogTypeTri0"; break;
  case eVerilogTypeTri1 : return "eVerilogTypeTri1"; break;
  case eVerilogTypeWAnd : return "eVerilogTypeWAnd"; break;
  case eVerilogTypeWire : return "eVerilogTypeWire"; break;
  case eVerilogTypeWOr : return "eVerilogTypeWOr"; break;
  case eVerilogTypeRealTime : return "eVerilogTypeRealTime"; break;
  case eVerilogTypeUWire : return "eVerilogTypeUWire"; break;
  }
  return NULL;

}

const char*  UserType::sGetCarbonVhdlTypeString(CarbonVhdlType vhdlType)
{
  switch (vhdlType)
  {
  case eVhdlTypeBit : return "eVhdlTypeBit"; break;
  case eVhdlTypeStdLogic : return "eVhdlTypeStdLogic"; break;
  case eVhdlTypeStdULogic : return "eVhdlTypeStdULogic"; break;
  case eVhdlTypeBoolean : return "eVhdlTypeBoolean"; break;
  case eVhdlTypeIntType : return "eVhdlTypeIntType"; break;
  case eVhdlTypeInteger : return "eVhdlTypeInteger"; break;
  case eVhdlTypeNatural : return "eVhdlTypeNatural"; break;
  case eVhdlTypePositive : return "eVhdlTypePositive"; break;
  case eVhdlTypeReal : return "eVhdlTypeReal"; break;
  case eVhdlTypeChar : return "eVhdlTypeChar"; break;
  case eVhdlTypeUnknown : return "eVhdlTypeUnknown"; break;
  }

  return NULL;
}
