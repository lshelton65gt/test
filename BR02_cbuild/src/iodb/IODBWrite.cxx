// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdarg.h>
#include "iodb/IODBNucleus.h"
#include "util/ZstreamZip.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "util/CbuildMsgContext.h"
#include "symtab/STSymbolTable.h"
#include "iodb/IODBTypes.h"
#include "iodb/IODBDesignData.h"
#include "iodb/IODBUserTypes.h"
#include "util/UtString.h"
#include "util/AtomicCache.h"
#include "exprsynth/ExprFactory.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUDesign.h"
#include "iodb/CbuildShellDB.h"
#include "exprsynth/ExprFlatten.h"
#include "exprsynth/SymTabExpr.h"
#include "hdl/HdlVerilogPath.h"
#include "iodb/ScheduleFactory.h"
#include "nucleus/NUAliasDB.h"
#include "compiler_driver/CarbonContext.h"
#include "util/ArgProc.h"
#include "util/UtPair.h"

static void sFixNetPulls(STSymbolTableNode* node);

struct IODBNucleus::BidiEnable
{
  BidiEnable(STSymbolTableNode* bidi, CarbonExpr* enableExpr) :
    mBidi(bidi), mEnableExpr(enableExpr)
  {}
  
  STSymbolTableNode* mBidi;
  CarbonExpr* mEnableExpr;
};

typedef UtHashSet<const STSymbolTableNode*> NodeSet;
typedef UtHashMap<const STSymbolTableNode*, const STSymbolTableNode*> NodeMap;

// This class filters the DB write so we only get the design symbol table
// nodes that we have put in the IO table.  There is an alternate Selector
// in compiler_driver/CarbonContext.h that is used when writing out the
// entire symbol table -- that one can filter out temp nets.  If that
// one is specified, then the IODBSelect will return for any node that
// passes 'selector' OR is in the IODB.
class IODBSelect: public STNodeSelectDB
{
public:
  IODBSelect(const IODBNucleus* iodb, STNodeSelectDB* selector, bool strict):
    mIODB(iodb),
    mSelector(selector),
    mStrict (strict),
    mAccessibleNodes(NULL)
  {}

  void putAccessibleNodes(const NodeSet* accessibleNodes) { mAccessibleNodes = accessibleNodes; }

protected:
  virtual bool computeSelectLeaf(const STAliasedLeafNode* leaf,
                                 const HdlHierPath* hdl)
  {
    if ((mSelector != NULL) && mSelector->selectLeaf(leaf, hdl))
      return true;
    bool indb = mIODB->isInIOTable(leaf);
    if (! indb && CbuildSymTabBOM::isForceSubordinate(leaf))
    {
      // we have to check if the subordinate's forcible is being
      // selected for write
      const STAliasedLeafNode* forcible = 
        mIODB->getSubordinateMasterForcible(leaf);
      ST_ASSERT(forcible, leaf);

      if (computeSelectLeaf(forcible, hdl))
        indb = true;
    }

    // Check that the dumped net has intrinsics defined:
    // * test/bugs/bug2749 fails if this is defined due to a dead
    //   rescoped net. Is this a bug in rescoping.
    // * Bug7722 failed at runtime when a net without intrinsic data was dumped
    if (!mStrict) {
      // only test when strict checking of the IODB is enabled
    } else if (indb && (CbuildSymTabBOM::getIntrinsic(leaf) == NULL)) {
      UtString name;
      leaf->compose (&name);
      NUBase *base = CbuildSymTabBOM::getNucleusObj (leaf);
      NUNetElab *elab = dynamic_cast <NUNetElab *> (base);
      NUNet *net;
      // need to identify the nucleus type in order to get a source location
      // for the error message
      if (base == NULL) {
        SourceLocator loc;
        mIODB->getMsgContext ()->IODBNoNetIntrinsics (&loc, name.c_str ());
      } else if ((elab = dynamic_cast <NUNetElab *> (base)) != NULL) {
        // a plain old elaborated net
        NUNet *net = elab->getNet ();
        SourceLocator loc = net != NULL ? net->getLoc () : SourceLocator ();
        mIODB->getMsgContext ()->IODBNoNetIntrinsics (&loc, name.c_str ());
      } else if ((net = dynamic_cast <NUNet *> (base)) != NULL) {
        // probably a VHDL record field
        SourceLocator loc = net->getLoc ();
        mIODB->getMsgContext ()->IODBNoNetIntrinsics (&loc, name.c_str ());
      } else {
        // could not work out what it is... just write the name of the leaf in
        // the error message
        SourceLocator loc;
        mIODB->getMsgContext ()->IODBNoNetIntrinsics (&loc, name.c_str ());
      }        
    }
    return indb;
  }
  virtual bool computeSelectBranch(const STBranchNode* branch,
                                   const HdlHierPath* hdl)
  {
    if ((mSelector != NULL) && mSelector->selectBranch(branch, hdl))
      return true;
    return mIODB->isInIOTable(branch);
  }

  // DB selectors have an "always select" set, which is given priority
  // over computeSelectLeaf() and computeSelectBranch().  See
  // STNodeSelectDB::selectLeaf() and STNodeSelectDB::selectBranch().
  // This means this IODB selector's requestAlwaysSelect() must
  // respect the restricted set of nodes that can be in the IODB.
  virtual void requestAlwaysSelect(const STSymbolTableNode* node)
  {
    // If there is no restrictive set, or there is a set and the node
    // is in it, insert it.
    if ((mAccessibleNodes == NULL) || (mAccessibleNodes->find(node) != mAccessibleNodes->end())) {
      mAlwaysSelect.insert(node);
    }
  }
  
private:
  const IODBNucleus* mIODB;
  STNodeSelectDB* mSelector;
  bool mStrict;                         //!< strict checking of selected nets
  const NodeSet* mAccessibleNodes;      //!< set of nodes that are accessible to the user
};

// Helper functions for pruning nodes.
// Use templates to deal with both UtHashSets and UtHashMaps,
// keyed by both const and non-const STSymbolTableNodes.
template <typename V>
static const STSymbolTableNode* sGetNodeFromLoop(LoopMap<UtHashMap<STSymbolTableNode*, V> >& l)
{
  return l.getKey();
}

template <typename V>
static const STSymbolTableNode* sGetNodeFromLoop(LoopMap<UtHashMap<const STSymbolTableNode*, V> >& l)
{
  return l.getKey();
}

static const STSymbolTableNode* sGetNodeFromLoop(UtHashSet<STSymbolTableNode*>::UnsortedLoop& l)
{
  return *l;
}

template <typename V>
static void sEraseNodeFromContainer(UtHashMap<const STSymbolTableNode*, V>* cont, const STSymbolTableNode* node)
{
  cont->erase(node);
}

static void sEraseNodeFromContainer(IODB::NameSetMap* cont, const STSymbolTableNode* node)
{
  IODB::NameSetMap::iterator i = cont->find(const_cast<STSymbolTableNode*>(node));
  if (i != cont->end()) {
    delete i->second;
    cont->erase(i);
  }
}

static void sEraseNodeFromContainer(IODB::NameIntMap* cont, const STSymbolTableNode* node)
{
  cont->erase(const_cast<STSymbolTableNode*>(node));
}

static void sEraseNodeFromContainer(UtHashSet<STSymbolTableNode*>* cont, const STSymbolTableNode* node)
{
  cont->erase(const_cast<STSymbolTableNode*>(node));
}

template <typename V>
static void sSwapNodeInContainer(UtHashMap<const STSymbolTableNode*, V>* cont, const STSymbolTableNode* oldNode, const STSymbolTableNode* newNode)
{
  // Insert the new key with the old one's value
  (*cont)[newNode] = (*cont)[oldNode];
  // Delete the old one
  cont->erase(oldNode);
}

template <typename V>
static void sSwapNodeInContainer(UtHashMap<STSymbolTableNode*, V>* cont, const STSymbolTableNode* oldNode, const STSymbolTableNode* newNode)
{
  // Insert the new key with the old one's value
  (*cont)[const_cast<STSymbolTableNode*>(newNode)] = (*cont)[const_cast<STSymbolTableNode*>(oldNode)];
  // Delete the old one
  cont->erase(const_cast<STSymbolTableNode*>(oldNode));
}

static void sSwapNodeInContainer(UtHashSet<STSymbolTableNode*>* cont, const STSymbolTableNode* oldNode, const STSymbolTableNode* newNode)
{
  cont->insert(const_cast<STSymbolTableNode*>(newNode));
  cont->erase(const_cast<STSymbolTableNode*>(oldNode));
}

static const STSymbolTableNode* sFindAlternateNode(const STSymbolTableNode* node, const NodeSet* allowedNodes)
{
  // The node is not in the set of allowed nodes.  That doesn't
  // necessarily mean it needs to be removed.  It could be that
  // the container is keyed by a node that's an alias of one of
  // the allowed ones.
  //
  // For example, an observable node may be determined to be
  // constant.  The constant is marked using the storage node of
  // the alias ring, which might not be the same aliased marked as
  // observable by the user.  The constant needs to be written to
  // the DB so that the correct value is seen when that net is
  // examined.
  //
  // If that's the case, return the viable alias. If not, return NULL.
  const STAliasedLeafNode* keyLeaf = node->castLeaf();
  bool foundSwap = false;
  if (keyLeaf != NULL) {
    for (STAliasedLeafNode::ConstAliasLoop l(keyLeaf); !foundSwap && !l.atEnd(); ++l) {
      const STAliasedLeafNode* alias = *l;
      if (allowedNodes->find(alias) != allowedNodes->end()) {
        // Found it.  Save the mapping.
        return alias;
      }
    }
  }
  return NULL;
}

template<typename T>
static void sPruneContainerForIODB(T* cont, const NodeSet* allowedNodes)
{
  // Collect the set of nodes to be pruned.
  NodeSet pruneNodes;
  NodeMap swapNodes;
  for (typename T::UnsortedLoop l(cont->loopUnsorted()); !l.atEnd(); ++l) {
    const STSymbolTableNode* node = sGetNodeFromLoop(l);
    if (allowedNodes->find(node) == allowedNodes->end()) {
      // Not allowed, check for an alternate (look at aliases)
      const STSymbolTableNode* alternate = sFindAlternateNode(node, allowedNodes);
      if (alternate != NULL) {
        swapNodes[node] = alternate;
      } else {
        // Nothing in the alias ring matches this key in the container, so prune it.
        pruneNodes.insert(node);
      }
    }
  }
  // Not all containers have the ability to erase a set, but they all
  // can erase an individual item.
  for (NodeSet::UnsortedLoop l(pruneNodes.loopUnsorted()); !l.atEnd(); ++l) {
    const STSymbolTableNode* node = *l;
    sEraseNodeFromContainer(cont, node);
  }
  // Replace the nodes with their aliases that are allowed to be in the IODB
  for (NodeMap::UnsortedLoop l(swapNodes.loopUnsorted()); !l.atEnd(); ++l) {
    const STSymbolTableNode* oldNode = l.getKey();
    const STSymbolTableNode* newNode = l.getValue();
    sSwapNodeInContainer(cont, oldNode, newNode);
  }
  
}

class IODBNucleus::DBClosure
{
public:

  DBClosure(IODBNucleus* iodb, const HdlHierPath* hdlHier, DynBitVectorFactory* bvPool, 
    ESFactory* exprFactory, STSymbolTable* designTable,
    CbuildSymTabBOM* bomManager,
    SourceLocatorFactory &sourceLocatorFactory,
    MsgContext &msg_context) :
    mIODB(iodb),
    mExprFactory(exprFactory), mDesignTable(designTable),
    mBomManager(bomManager), mTmpSymtab(NULL), 
    mTmpUnelabSymtab (NULL),
    mDBType(eCarbonFullDB), 
    mSourceLocatorFactory (sourceLocatorFactory), 
    mMsgContext (msg_context)
  {
    mExprSymTab= new STSymbolTable(&mEmptyBom, &mLocalStrCache);
    mExprSymTab->setHdlHier(hdlHier);
    mEnableDBContext = new ExprDBContext(mExprSymTab, bvPool);
  }
  
  ~DBClosure()
  {
    for (BidiEnableVec::iterator bidEnP = mBidiExprContexts.begin();
         bidEnP != mBidiExprContexts.end(); ++bidEnP)
      delete *bidEnP;
    
    delete mEnableDBContext;
    delete mExprSymTab;
  }

  //! Object to hold everything needed for a symboltable
  /*! This is used both for an elaborated symbol table and an unelaborated
   *  symbol table that is constructed for writing to the GUI DB file. These
   *  tables have different BOM types. Processing specific to a particular BOM
   *  and tables is embedded in abstract virtual methods.
   */

  struct Symtab {

    Symtab(STFieldBOM *bom_manager, STNodeSelectDB* selector, 
      const HdlHierPath* hdlHier, bool verboseErrors) :
      mSelectIO (selector),
      mBomManager (bom_manager),
      mSymtab(mBomManager, &mAtomicCache),
      mVerbose (verboseErrors)
    {
      mSymtab.setHdlHier(hdlHier);
    }

    virtual ~Symtab () {}

    virtual bool getLoc (STSymbolTableNode *, SourceLocator *) const = 0;
    virtual void copyLeafData (STSymbolTable::LeafAssoc &) = 0;
    virtual void copyBranchData (STBranchNode *branch, STBranchNode *new_branch) = 0;
    
    STNodeSelectDB *mSelectIO;
    AtomicCache mAtomicCache;
    STFieldBOM *mBomManager;
    SCHScheduleFactory mSchedFactory;
    STSymbolTable mSymtab;
    UtHashSet<STBranchNode*> mCopiedBranches;
    bool mVerbose;
  };

  //! Object to hold everything needed for the unelaborated symbol table.
  /*! This symbol table is constructed by merging the AliasDB symbol table for
   *  each module into a single unelaborated symbol table. There is a root node
   *  for each module and the AliasDB is mapped under the root node tha
   *  corresponds to the module.
   */
  struct UnelabSymtab : public Symtab {

    UnelabSymtab (NUAliasBOM *bom_manager, STNodeSelectDB* selector, 
      const HdlHierPath* hdlHier, bool verboseErrors) :
      Symtab (bom_manager, selector, hdlHier, verboseErrors)
    {}

    virtual ~UnelabSymtab () {}

    virtual void copyLeafData (STSymbolTable::LeafAssoc &leaves)
    {
      for (STSymbolTable::LeafAssoc::iterator p = leaves.loop (); !p.atEnd (); ++p) {
        NUAliasDataBOM *src_bom = NUAliasBOM::castBOM (p->first->getBOMData ());
        NUAliasDataBOM *dst_bom = NUAliasBOM::castBOM (p->second->getBOMData ());
        dst_bom->setNet (src_bom->getNet ());
      }
    }

    virtual void copyBranchData (STBranchNode *src, STBranchNode *dst)
    {
      if (src == NULL) {
        // This should be the node for a top-level module.
        INFO_ASSERT (dst != NULL && dst->getParent () == NULL, "elaborated symbol table inconsistency");
      } else {
        NUAliasDataBOM *src_bom = NUAliasBOM::castBOM (src->getBOMData ());
        NUAliasDataBOM *dst_bom = NUAliasBOM::castBOM (dst->getBOMData ());
        dst_bom->setScope (src_bom->getScope ());
      }
    }

    virtual bool getLoc (STSymbolTableNode *node, SourceLocator *loc) const
    {
      STAliasedLeafNode *leaf;
      STBranchNode *branch;
      NUNet *net;
      NUScope *scope;
      NUAliasDataBOM *bom = NUAliasBOM::castBOM (node->getBOMData ());
      if (((leaf = node->castLeaf ()) != NULL) && ((net = bom->getNet ()) == NULL)) {
        // it is a leaf node but there is no net embedded in the bom
        return false;
      } else if (leaf != NULL) {
        // it is a leaf node and we can get the location from the net
        *loc = net->getLoc ();
        return true;
      } else if ((branch = node->castBranch ()) == NULL) {
        // neither a branch nor a leaf
        return false;
      } else if ((scope = bom->getScope ()) == NULL) {
        // it is a branch node but there is no scope embedded in the BOM
        return false;
      } else {
        *loc = scope->getSourceLocation ();
        return true;
      }
    }

  };

  struct CbuildSymtab : public Symtab {
    CbuildSymtab(CbuildSymTabBOM *bom_manager, IODBNucleus* iodb, STNodeSelectDB* selector, 
      const HdlHierPath* hdlHier, DynBitVectorFactory* bvPool, UserTypeFactory* typeFact,
      bool verboseErrors, bool strict, bool forIODB) :
      Symtab (bom_manager, &mIODBSelectIO, hdlHier, verboseErrors),
      mCbuildBomManager (bom_manager),
      mIODBSelectIO (iodb, selector, strict),
      mIODB(iodb)
    {
      mCbuildBomManager->putScheduleFactory(&mSchedFactory);
      mCbuildBomManager->putBVFactory(bvPool);
      mCbuildBomManager->putUserTypeFactory (typeFact);

      // If this is being used for an IODB (as opposed to a full DB),
      // pass the list of user-accessible nodes to the selector.  The
      // list will be populated in pruneForIODB().
      if (forIODB) {
        mIODBSelectIO.putAccessibleNodes(&mAccessibleNodes);
      }
    }
    virtual ~CbuildSymtab () {}

    virtual void copyLeafData (STSymbolTable::LeafAssoc &leaves)
    {
      for (STSymbolTable::LeafAssoc::iterator p = leaves.loop (); !p.atEnd (); ++p) {
        const STAliasedLeafNode *src_leaf = p->first;
        STAliasedLeafNode *new_leaf = p->second;
        CbuildSymTabBOM::copyLeafData(src_leaf, new_leaf);
        if ((new_leaf->getStorage() == new_leaf) && src_leaf->getStorage() != src_leaf)
          // If we set the new leaf as storage let's make sure we fill
          // it with the storage information from the source.
          CbuildSymTabBOM::copyStorageLeafInfo(src_leaf, new_leaf);
      } // for
    }

    virtual void copyBranchData (STBranchNode *parent, STBranchNode *savedParent)
    {
      while (parent && mCopiedBranches.insertWithCheck(parent))
      {
        AtomicCache* atomicCache = mSymtab.getAtomicCache();
        CbuildSymTabBOM::copyBranchData(parent, savedParent, atomicCache);
        parent = parent->getParent();
        savedParent = savedParent->getParent();
      }
    }

    virtual bool getLoc (STSymbolTableNode *node, SourceLocator *loc) const
    {
      NUBase *base;
      NUElabBase *elabBase;
      NUNet *net;
      if ((base = CbuildSymTabBOM::getNucleusObj (node)) == NULL) {
        // no object underneath the symbol
        return false;
      } else if ((elabBase = dynamic_cast <NUElabBase *> (base)) != NULL) {
        // it is an elaboration
        *loc = elabBase->getSourceLocation ();
        return true;
      } else if ((net = dynamic_cast <NUNet *> (base)) != NULL) {
        // it is a net... this can happen with record fields
        *loc = net->getLoc ();
        return true;
      } else {
        // an unexpected flavour
        if (mVerbose) {
          base->pr ();
        }
        return false;
      }
    }

    CbuildSymTabBOM *mCbuildBomManager;
    IODBSelect mIODBSelectIO;
    IODBNucleus* mIODB;
    NodeSet mAccessibleNodes;             //!< set of nodes that are accessible to the user
  };

  void writeLoc (ZostreamDB &symDB, Symtab &stClosure, STSymbolTableNode *node)
  {
    SourceLocator loc;
    if (!symDB.isMapped (node)) {
      // not an interesting node
    } else if (!stClosure.getLoc (node, &loc)) {
#if 0
      // Actually, let's not write a warning. When the source location cannot
      // be found it is because it is some internally synthesised net.
      // write a warning that the source location was not inferred
      UtString name;
      node->compose (&name);
      mMsgContext.STCannotFindSourceLocation (name.c_str ());
#endif
    } else {
      // write the source location to the database
      symDB.writePointer (node);
      mSourceLocatorFactory.writeDB (symDB, loc);
    }
  }

  void putDBType(CarbonDBType dbType)
  {
    mDBType = dbType;
  }

  void putTmpSymtab(CbuildSymtab* tmpSym)
  {
    mTmpSymtab = tmpSym;
  }

  void putTmpUnelabSymtab(Symtab* tmpSym)
  {
    mTmpUnelabSymtab = tmpSym;
  }

  CbuildSymtab* getTmpSymtab() { return mTmpSymtab; }
  Symtab* getTmpUnelabSymtab () { return mTmpUnelabSymtab; }

  CarbonDBType getDBType() const { return mDBType; }

  void prepEnableSymtab(IODB* iodb)
  {
    BidiEnable* bidiEnableContext;

    STSymbolTable::LeafAssoc dummy;
    for (IODB::NameExprMapLoop ex = iodb->loopEnableExprs(); !ex.atEnd(); ++ex)
    {
      STAliasedLeafNode* node = ex.getKey();
      CarbonExpr* ns = ex.getValue();
      bidiEnableContext = new BidiEnable(node, ns);
      mBidiExprContexts.push_back(bidiEnableContext);
      // Preps the db context with all the needed CarbonExpr and
      // implicitly populates the symtab with the names of the idents
      mExprFactory->setupDBContext(mEnableDBContext, ns, false, &dummy);
    }
  }

  bool writeExprSymtab(ZostreamDB& symDB)
  {
    // Now, the local symtab is populated. Write the local symtab
    mExprSymTab->writeDB(symDB, NULL, false);
    
    // Write the expressions.
    mEnableDBContext->writeExprs(symDB);
    if (symDB.fail())
      return false;

    INFO_ASSERT(mTmpSymtab, "Temporary symboltable was not set.");
    STSymbolTable* symtab = &mTmpSymtab->mSymtab;
    BidiEnableVec::iterator bidEnP;
    for (bidEnP = mBidiExprContexts.begin();
         bidEnP != mBidiExprContexts.end(); ++bidEnP)
    {
      BidiEnable* bidiEnableContext = *bidEnP;
      if (!symDB.writePointer(symtab->safeLookup(bidiEnableContext->mBidi)))
        return false;
      symDB << mEnableDBContext->getIndex(bidiEnableContext->mEnableExpr);
    }
    
    if (!symDB.writeNull())
      return false;
    return true;
  }

  //! Using the design construct an unelaborated symbol table
  void filterUnelabSymtab (NUDesign *design, Symtab *closure)
  {
    STSymbolTable *stab = &closure->mSymtab;
    for (NUDesign::ModuleLoop modules = design->loopAllModules (); !modules.atEnd (); ++modules) {
      NUModule *module = *modules;
      // construct a branch for the module
      StringAtom *name = closure->mAtomicCache.intern (module->getName ()->str ());
      STBranchNode *module_root = stab->createBranch (name, NULL);
      NUAliasDataBOM *bom = NUAliasBOM::castBOM (module_root->getBOMData ());
      bom->setScope (module);
      // now hang a copy of the unelaborated symbol table for the module from
      // the branch
      for (STSymbolTable::NodeLoopSorted p = module->getAliasDB ()->getNodeLoopSorted(); 
           ! p.atEnd(); ++p)
      {
        STSymbolTableNode* node = *p;
        NUAliasDataBOM *bom = NUAliasBOM::castBOM (node->getBOMData ());
        STAliasedLeafNode *leaf;
        STAliasedLeafNode *new_leaf;
        STBranchNode *branch;
        if (((leaf = node->castLeaf ()) != NULL)
          && ((new_leaf = filterSymtabLeaf (leaf, closure, module_root)) != NULL)) {
          // create a new leaf node
          NUAliasDataBOM *new_bom = NUAliasBOM::castBOM (new_leaf->getBOMData ());
          new_bom->setNet (bom->getNet ());
        } else if ((branch = node->castBranch ()) != NULL) {
          // not creating branches with no leaves
        }
      }
    }
    mLeafAssocs.clear();
  }

  //! Using the full symtab and the selector create a smaller symtab.
  void filterSymtab(CbuildSymtab* stClosure)
  {
    /* If a node is selected for write, we need to make sure that
       nodes of the idents of its describing expression (if
       applicable) are also selected. For example, top.io may be
       written to the db, but it was split into top.$portsplit_0_0 and
       top.$portsplit_1_1. We need to make sure those two port split
       nets are also selected.
    */
    STSymbolTable* subSymTab = &stClosure->mSymtab;
    STNodeSelectDB* selector = stClosure->mSelectIO;
    SCHScheduleFactory* schedFactory = &stClosure->mSchedFactory;
    mBomManager->prepareForDBWrite(mDesignTable, subSymTab, selector, &mLeafAssocs);
    
    // Now iterate and translate selected leaves.
    // Need to do this sorted to guarantee an ordering in
    // symnode indexing. Otherwise, reports in the runtime based on
    // looping the symboltable would be unstable.
    for (STSymbolTable::NodeLoopSorted p = mDesignTable->getNodeLoopSorted(); 
         ! p.atEnd(); ++p)
    {
      STSymbolTableNode* node = *p;
      STAliasedLeafNode* leaf = node->castLeaf();
      if (leaf) {
        filterSymtabLeaf(leaf, stClosure);
      }
    }

    /*
      Completely resolve the storage nodes into rings where a storage
      node has always been selected.
      Note: I've always wanted to do this. The iodb as previously
      written always had 'dangling' rings where no storage node was
      selected. This is ok, because we are putting all the storage
      information into each node anyway. But, we might want to change
      that.
      The master node is meaningless in the runtime and has been
      resolve enough already during the leaf translation. So, we
      aren't doing anything more with those. In fact, I want to remove
      the master node concept from the symboltable altogether.
    */
    typedef UtHashSet<STAliasedLeafNode*> LeafNodeSet;
    LeafNodeSet visitedLeaves;
    for (STSymbolTable::LeafAssoc::iterator p = mLeafAssocs.loop (); !p.atEnd (); ++p) {
      const STAliasedLeafNode *src_leaf = p->first;
      STAliasedLeafNode *new_leaf = p->second;
      if (visitedLeaves.insertWithCheck(new_leaf))
      {
        // haven't seen this ring from the source symtab yet
        
        // first find the storageNode, if any, in the ring
        /*
          A ring in the filtered symboltable may not have a storage
          node selected because we didn't select the storage node from
          the original symboltable to be included in the symboltable.

          We need the storage node or to select a storage node if
          applicable in order to properly link aliases together.
        */
        STAliasedLeafNode* subStorage = NULL;
        // assume that storage is selected in one of the new leaf
        // aliases.
        bool storageSelected = true;
        for (STAliasedLeafNode::ConstAliasLoop q(src_leaf); 
             ! q.atEnd() && storageSelected && (subStorage == NULL); ++q)
        {
          const STAliasedLeafNode* curSrc = *q;
          if (curSrc->getStorage() == curSrc)
          {
            STSymbolTableNode* subNode = subSymTab->safeLookup(curSrc);
            if (subNode)
              subStorage = subNode->castLeaf();
            else
              // storage was not selected when creating the new nodes.
              storageSelected = false;
          }
        }
        
        // if we don't have a storage node selected take the first
        if (subStorage == NULL)
        {
          subStorage = new_leaf;
          subStorage->setThisStorage();
        }
        
        for (STAliasedLeafNode::ConstAliasLoop q(src_leaf); 
             ! q.atEnd(); ++q)
        {
          const STAliasedLeafNode* curSrc = *q;
          STSymbolTableNode* curSubNode = subSymTab->safeLookup(curSrc);
          if (curSubNode)
          {
            STAliasedLeafNode* curSubAlias = curSubNode->castLeaf();
            ST_ASSERT(curSubAlias, curSubNode);
            subStorage->linkAlias(curSubAlias);
            // add to visited nodes
            (void) visitedLeaves.insertWithCheck(curSubAlias);
          }
        } // for
      } // if
    } // for
    
    /*
      We have the complete subset of nodes and each has a resolved
      
      Sanity check the alias rings.
    */
    for (STSymbolTable::LeafAssoc::iterator p = mLeafAssocs.loop (); !p.atEnd (); ++p) {
      ST_ASSERT(p->second->getInternalStorage() != NULL, p->second);
    }

    /*
      Next, we need to copy the leaf data from the source leaf nodes
      to the new leaf nodes. After this, we'll have the data for each
      non-storage leaf node correct. But, the storage nodes'
      signatures will be wrong since they reference src symboltable
      nodes. We'll fix that after transferring the data. 
    */
    stClosure->copyLeafData (mLeafAssocs);
    
    /*
      Now, (are we having fun yet?) we need to recreate signatures
      that use pointers from the symboltable to represent clocks.
    */
    // reusing visitedLeaves set, except this time we will only keep
    // storage nodes
    visitedLeaves.clear();
    typedef UtHashMap<const SCHSignature*, const SCHSignature*> SigToSigMap;
    SigToSigMap signatureMap;

    for (STSymbolTable::LeafAssoc::iterator p = mLeafAssocs.loop (); !p.atEnd (); ++p)    {
      const STAliasedLeafNode *src_leaf = p->first;
      STAliasedLeafNode *new_leaf = p->second;

      // Only replicate the signature if we haven't done so already. 
      if (visitedLeaves.insertWithCheck(new_leaf->getStorage()))
      {
        const SCHSignature* srcSignature = CbuildSymTabBOM::getSCHSignature(src_leaf);
        if (srcSignature)
        {
          const SCHSignature* newSignature = NULL;
          // check if we already translated this signature
          SigToSigMap::iterator sigIter = signatureMap.find(srcSignature);
          if (sigIter == signatureMap.end())
          {
            // replicate the signature with the new factory
            newSignature = schedFactory->translateSignature(srcSignature, subSymTab);
            signatureMap[srcSignature] = newSignature;
          }
          else
            newSignature = sigIter->second;

          CbuildSymTabBOM::setSCHSignature(new_leaf, newSignature);
        } //if
      } // if
    } // for


    // finally copy any expression mappings.
    /*
      Note we don't have to create new expressions. The expressions
      from the src symboltable are still valid. The db context only
      sets it up for writing according to any symboltable. So, every
      expression is valid even though the STAliasedLeafNodes in the
      idents don't correspond to the smaller symboltable. 
      I'm really happy I did it that way considering
      backpointers. Those are very tedious to recreate.
    */
    CbuildSymTabBOM& dstBomMgr = *stClosure->mCbuildBomManager;
    for (STSymbolTable::LeafAssoc::iterator p = mLeafAssocs.loop (); !p.atEnd (); ++p) {
      const STAliasedLeafNode *src_leaf = p->first;
      CarbonExpr* srcMappedExpr = NULL;
      CarbonIdent* srcIdent = CbuildSymTabBOM::getIdent(src_leaf);
      if (srcIdent)
        srcMappedExpr = mBomManager->getExpr(srcIdent);

      if (srcMappedExpr)
        // map the expression in the new bom manager
        dstBomMgr.mapExpr(srcIdent, srcMappedExpr);
    }
    
    mLeafAssocs.clear();
  }

  void pruneForIODB(CbuildSymtab* stClosure)
  {
    // Many node sets that are written to the full DB need to be pruned
    // before the IODB is written.  The only nodes that should be in the
    // IODB are primary I/Os, plus the observable, depositable, and
    // forcible nodes.

    // Build a set of all the nodes that should be accessible.  Get this
    // set from the symtab closure.  It will use this later to determine
    // which nodes can be added to the symbol table.
    NodeSet* accessibleNodes = &stClosure->mAccessibleNodes;
    gatherAccessibleNodes(accessibleNodes);

    // The IO table contains all the nodes that would normally be
    // written to the IODB.  All the nodes in the individual sets
    // (e.g. clocks, tieNets) should be part of this.  This means we
    // could theoretically walk the IO table, identify the nodes that
    // should be pruned, and erase them from all the sets.
    //
    // However, if my assumption is wrong, and there are nodes in the
    // individual sets that are not in the IO table, they'd slip
    // through.  Let's be pessimistic and walk each set separately.
    sPruneContainerForIODB(&mIODB->mIOTable, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mFastClocks, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mExposedNets, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mDelayedClocks, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mClockSpeeds, accessibleNodes);
    sPruneContainerForIODB(mIODB->mTieNets, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mInputs, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mOutputs, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mBidis, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mClocks, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mClockTree, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mPrimaryClocks, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mAsyncs, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mAsyncDeposits, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mAsyncPosResets, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mAsyncNegResets, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mCModelSenseNets, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mWrapPort2StateNets, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mWrapPort4StateNets, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mLiveInputDirNets, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mLiveOutputDirNets, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mPosedgeScheduleTriggers, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mNegedgeScheduleTriggers, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mScheduleTriggersUsedAsData, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mOnDemandIdleDepositNets, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mOnDemandExcludedNets, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mHiddenModuleTops, accessibleNodes);
    sPruneContainerForIODB(mIODB->mConstNets, accessibleNodes);
    sPruneContainerForIODB(&mIODB->mOnDemandRuntimeExcludedNets, accessibleNodes);

    // Collapsed clocks are messy.  They're not just a simple set.
    // They're a map of master clocks to all the corresponding slave
    // clocks.  So, if there are two primary clocks and an internal
    // clock collapsed like this:
    //
    // collapseClock top.clk1 top.clk2 top.sub.clk
    //
    // we have a map of top.clk1 => { top.clk2, top.sub.clk }.
    //
    // Obviously, that can't be written as-is because of the internal
    // node, but what should be done instead?  Should the internal
    // nodes be stripped from the slave set?  What if the slave set
    // contains only internal nodes?  What if an internal node is the
    // master of a set?
    //
    // None of the collaped clock information is needed at runtime by
    // the model (it's added to the DB as a nicety), so just prune the
    // set altogether.  Note that the storage for the values of the
    // map is managed manually (see ~IODBNucleus()), so we need to
    // free them before clearing the map.
    for (NameSetMapLoop l(mIODB->loopCollapseClocks()); !l.atEnd(); ++l) {
      NameSet* slaveSet = l.getValue();
      delete slaveSet;
    }
    mIODB->mCollapsedClocks.clear();

    // Async outputs are maps to their fanin. Clean up by removing
    // the whole thing if the output is hidden or each individual input.
    // This can occur for temporary nets like the cfnet
    sPruneContainerForIODB(&mIODB->mAsyncOutputs, accessibleNodes);
    for (NameSetMap::UnsortedLoop l = mIODB->mAsyncOutputs.loopUnsorted(); !l.atEnd(); ++l) {
      NameSet* ns = l.getValue();
      sPruneContainerForIODB(ns, accessibleNodes);
    }

    // We also need to go through the SCHSignature objects associated
    // with each node.  These are used at runtime to optimize waveform
    // dumping by knowing what schedule events can cause a signal's
    // value to change.  If any nodes in the events are not to be
    // written to the DB, we need to replace them with dummy nodes.
    //
    // Waveform dumping will still work, but the signal's value will
    // have to be checked pessimistically for changes.  At runtime,
    // the waveform dumper will try to get the change array index
    // associated with the events' nodes.  The dummy ones won't have
    // an index, so it will fall back on checking for changes after
    // every schedule call.  See ScheduleStimuli::addEventNodes().
    //
    // Since the IODB has many fewer signals than the full DB, that
    // shouldn't be a problem.
    SCHScheduleFactory* schedFactory = mIODB->getSchedFactory();
    AtomicCache* atomicCache = mDesignTable->getAtomicCache();
    SignatureReplacer rep(schedFactory, mDesignTable, atomicCache, accessibleNodes);
    rep.replace();
  }

  //! Walker to add nodes from an expression to a list
  class AddNodes : public CarbonExprWalker
  {
  public:
    AddNodes(NodeSet* nodes)
      : mNodes(nodes) {}
    virtual void visitIdent(CarbonIdent* ident)
    {
      SymTabIdent* symTabIdent = ident->castSymTabIdent();
      STAliasedLeafNode* leaf = symTabIdent->getNode();
      mNodes->insert(leaf);
    }
  private:
    NodeSet* mNodes;
  };

  //! Build the set of nodes that should be in the IODB
  void gatherAccessibleNodes(NodeSet* accessibleNodes)
  {
    // Start with all the primary ports, plus the observe, deposit,
    // and force nets.  Note that we need to use the explicit
    // input/output/bidi loops, in addition to loopPrimaryPorts().  In
    // the case of record ports, loopPrimaryPorts returns the $recnet_
    // nodes, while the individual loops return the user-friendly
    // names.
    accessibleNodes->insertLoop(mIODB->loopPrimaryPorts());
    accessibleNodes->insertLoop(mIODB->loopInputs());
    accessibleNodes->insertLoop(mIODB->loopOutputs());
    accessibleNodes->insertLoop(mIODB->loopBidis());
    accessibleNodes->insertLoop(mIODB->loopObserved());
    accessibleNodes->insertLoop(mIODB->loopDeposit());
    accessibleNodes->insertLoop(mIODB->loopForced());

    // If any of these is an expression net, its component nets need to
    // be added, too.
    NodeSet nodesToAdd;
    STFieldBOM* fieldBOM = mDesignTable->getFieldBOM();
    CbuildSymTabBOM* stBOM = dynamic_cast<CbuildSymTabBOM*>(fieldBOM);
    INFO_ASSERT(stBOM != NULL, "Incorrect symtab type.");

    for (NodeSet::UnsortedLoop l(accessibleNodes->loopUnsorted()); !l.atEnd(); ++l) {
      const STSymbolTableNode* node = *l;
      const STAliasedLeafNode* leaf = node->castLeaf();
      // Only leaves should be here
      ST_ASSERT(leaf != NULL, node);
      CarbonIdent* ident = CbuildSymTabBOM::getIdent(leaf);
      if (ident != NULL) {
        CarbonExpr* expr = stBOM->getExpr(ident);
        if (expr != NULL) {
          // Walk the expression, saving its nodes
          AddNodes walker(&nodesToAdd);
          walker.visitExpr(expr);
        }
      }
    }
    accessibleNodes->insertSet(nodesToAdd);
  }

  //! Validate the contents of the newly-written IODB
  bool validateIODB()
  {
    // Originally, I wanted to open up the .io.db file in the same way
    // that the CarbonDatabase class does.  Unfortunately, that would
    // mean linking a bunch of shell code into cbuild, which would get
    // messy.  Instead, just go through the symbol table that was
    // written and ensure all the nodes in it should actually be
    // there.
    const NodeSet& accessibleNodes = mTmpSymtab->mAccessibleNodes;
    bool ret = true;

    // Validate all the nodes in symbol table that was written
    STSymbolTable& writtenSymtab = mTmpSymtab->mSymtab;
    for (STSymbolTable::SortedRootIter l(writtenSymtab.getSortedRootIter()); !l.atEnd(); ++l) {
      const STSymbolTableNode* node = *l;
      ret &= validateIODBNode(node, accessibleNodes);
    }

    return ret;
  }


  //! Validate a node in the newly-written IODB
  bool validateIODBNode(const STSymbolTableNode* node, const NodeSet& accessibleNodes)
  {
    bool ret = true;
    const STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf != NULL) {
      // Leaves need to be in the accessible set.  The node we have is
      // from the temp symbol table of the DBClosure that was used to
      // write the IODB.  Look it up in the IODBNucleus design symbol
      // table, because that's what was used to build the accessible
      // set.
      const STSymbolTableNode* designNode = mDesignTable->safeLookup(node);
      ST_ASSERT(designNode != NULL, node);

      // Handle some special cases.
      const STAliasedLeafNode* designLeaf = designNode->castLeaf();
      if (designLeaf != NULL) {
        // For force subordinates, get the master node.
        const STAliasedLeafNode* forceMaster = mIODB->getSubordinateMasterForcible(designLeaf);
        if (forceMaster != NULL) {
          designNode = forceMaster;
        }
      }

      if (accessibleNodes.find(designNode) == accessibleNodes.end()) {
        UtString msg;
        msg << "Leaf node ";
        leaf->verilogCompose(&msg);
        msg << " should not exist in IODB.";
        mMsgContext.IODBValidationFailure(msg.c_str());
        ret = false;
      }
    } else {
      const STBranchNode* branch = node->castBranch();
      // Branches should only exist if they have children, and those
      // children must all be in the set.
      if (branch->numChildren() == 0) {
        UtString msg;
        msg << "Branch node ";
        branch->verilogCompose(&msg);
        msg << " has no children.";
        mMsgContext.IODBValidationFailure(msg.c_str());
        ret = false;
      } else {
        STBranchNodeIter iter(const_cast<STBranchNode*>(branch));
        const STSymbolTableNode* child;
        SInt32 dummy;
        while ((child = iter.next(&dummy)) != NULL) {
          ret &= validateIODBNode(child, accessibleNodes);
        }
      }
    }

    return ret;
  }



private:
  IODBNucleus* mIODB;
  // Empty Field BOM
  STEmptyFieldBOM mEmptyBom;
  // Need a separate string cache for the expression names
  AtomicCache mLocalStrCache;
  ESFactory* mExprFactory;
  UserTypeFactory* mUserTypeFactory;
  STSymbolTable* mDesignTable;
  STSymbolTable* mExprSymTab;
  ExprDBContext* mEnableDBContext;
  CbuildSymTabBOM* mBomManager;
  CbuildSymtab* mTmpSymtab;
  Symtab* mTmpUnelabSymtab;
  CarbonDBType mDBType;
  SourceLocatorFactory &mSourceLocatorFactory;
  MsgContext &mMsgContext;

  // Keep all the db contexts for the expressions in a vector
  typedef UtArray<BidiEnable*> BidiEnableVec;
  BidiEnableVec mBidiExprContexts;

  STSymbolTable::LeafAssoc mLeafAssocs;

  STAliasedLeafNode *filterSymtabLeaf(STAliasedLeafNode* leaf, Symtab* stClosure, 
    STBranchNode *root = NULL)
  {
    STNodeSelectDB *selector  = stClosure->mSelectIO;
    STSymbolTable& subSymTab = stClosure->mSymtab;

    if (selector->selectLeaf(leaf, mDesignTable->getHdlHier()))
    {
      STAliasedLeafNode* savedLeaf = subSymTab.translateLeaf(leaf, root);
      if (leaf->getStorage() == leaf)
        savedLeaf->setThisStorage();
      if (leaf->getMaster() == leaf)
        savedLeaf->setThisMaster();

      // We are not copying over the leaf data yet, because we need to fully
      // resolve the leaves first; however, we can get the branch data now.
      stClosure->copyBranchData (leaf->getParent (), savedLeaf->getParent ());

      // place the src and new node in the set of leafassoc array.
      mLeafAssocs.append (leaf, savedLeaf);
      return savedLeaf;
    } else {
      return NULL;
    }
  }

  /*!
    Loops over the design symbol table and the SCHSignatures
    associated with each node.  If the SCHSignatures have events whose
    clocks are not permitted to be written to the IODB, replace with
    them with temp clocks.
  */
  class SignatureReplacer
  {
  public:
    SignatureReplacer(SCHScheduleFactory* schedFactory, STSymbolTable* designTable,
                      AtomicCache* atomicCache, NodeSet* allowedNodes)
      : mSchedFactory(schedFactory), mDesignTable(designTable),
        mAtomicCache(atomicCache), mAllowedNodes(allowedNodes), mNextIndex(0)
    {
      // Get the root node
      INFO_ASSERT(mDesignTable->numRoots() == 1, "Multiple roots in design symbol table");
      STSymbolTableNode* root = *(mDesignTable->getRootIter());
      mRootNode = root->castBranch();
      ST_ASSERT(mRootNode != NULL, root);
    }

    ~SignatureReplacer() {}

    typedef UtPair<STAliasedLeafNode*, const SCHSignature*> ReplacePair;
    typedef UtList<ReplacePair> ReplaceList;
    /*!
      Do the replacement of any necessary nodes in all the signatures.
     */
    void replace()
    {
      // We'll be adding nodes to the symbol table, so find all the
      // nodes whose signatures we need to replace first.
      ReplaceList replaceList;
      for (STSymbolTable::NodeLoop l(mDesignTable->getNodeLoop()); !l.atEnd(); ++l) {
        STSymbolTableNode* node = *l;
        STAliasedLeafNode* leaf = node->castLeaf();
        if (leaf != NULL) {
          const SCHSignature* signature = CbuildSymTabBOM::getSCHSignature(leaf);
          if (signature != NULL) {
            // If either the transition or sample mask references a
            // node that will not be in the IODB, the signature needs
            // to be replaced.
            const SCHScheduleMask* transitionMask = signature->getTransitionMask();
            const SCHScheduleMask* sampleMask = signature->getSampleMask();
            if (needsReplacement(transitionMask) || needsReplacement(sampleMask)) {
              replaceList.push_back(ReplacePair(leaf, signature));
            }
          }
        }
      }
      // Now do the actual replacement.
      for (ReplaceList::iterator iter = replaceList.begin(); iter != replaceList.end(); ++iter) {
        STAliasedLeafNode* leaf = (*iter).first;
        const SCHSignature* signature = (*iter).second;
        // Create new transition and sample masks with replacement
        // nodes, if necessary.
        const SCHScheduleMask* oldTransitionMask = signature->getTransitionMask();
        const SCHScheduleMask* oldSampleMask = signature->getSampleMask();
        const SCHScheduleMask* newTransitionMask = replaceMask(oldTransitionMask);
        const SCHScheduleMask* newSampleMask = replaceMask(oldSampleMask);
        // At least one of the masks should be new, or else the
        // signature shouldn't have been included in the list.
        ST_ASSERT((newTransitionMask != oldTransitionMask) || (newSampleMask != oldSampleMask), leaf);
        // Create a new signature and associate it with the node.
        const SCHSignature* newSignature = mSchedFactory->buildSignature(newTransitionMask, newSampleMask);
        CbuildSymTabBOM::setSCHSignature(leaf, newSignature);
      }
    }

  private:
    SCHScheduleFactory* mSchedFactory;                  //!< Schedule factory for creating new events, masks, and signatures
    STSymbolTable* mDesignTable;                        //!< Full design symbol table
    AtomicCache* mAtomicCache;                          //!< Atomic cache for the design symtab
    NodeSet* mAllowedNodes;                             //!< Nodes that are allowed in the IODB
    STBranchNode* mRootNode;                            //!< Root of the design symtab
    UInt32 mNextIndex;                                  //!< Next index to use for creating a hidden clock
    static const char* scPrefix;                        //!< Prefix for the hidden clock name
    typedef UtHashMap<const STSymbolTableNode*, const STAliasedLeafNode*> ReplacementMap;
    ReplacementMap mReplacements;                       //!< Map of original clock nodes to their replacements

    /*!
      Determine if any of the nodes referenced in the mask need to be replaced.
     */
    bool needsReplacement(const SCHScheduleMask* mask)
    {
      bool replace = false;
      for (SCHScheduleMask::UnsortedEvents l(mask->loopEvents()); !replace && !l.atEnd(); ++l) {
        const SCHEvent* event = *l;
        // Only clock events have nodes associated with them.
        if (event->isClockEvent()) {
          const STSymbolTableNode* clock = event->getClock();
          if (mAllowedNodes->find(clock) == mAllowedNodes->end()) {
            // This event references a clock we can't use, so it needs to be replaced.
            replace = true;
          }
        }
      }

      return replace;
    }

    /*!
      Replace any necessary nodes in a mask, returning the new mask.
      If no replacement is necessary, the input mask is returned
      unchanged.
     */
    const SCHScheduleMask* replaceMask(const SCHScheduleMask* oldMask)
    {
      // By default, we just return the mask we started with.
      const SCHScheduleMask* newMask = oldMask;
      // The schedule factory allows you to start building a mask
      // without actually creating it.  We take advantage of that by
      // unconditionally building a replacement mask as we loop over
      // the events in the input mask, but only actually creating the
      // new mask if we discover we need it (i.e. we had to replace
      // one of the events).
      bool createNewMask = false;
      mSchedFactory->clearMaskBuilder();

      for (SCHScheduleMask::UnsortedEvents l(oldMask->loopEvents()); !l.atEnd(); ++l) {
        const SCHEvent* event = *l;
        // Only clock events have nodes associated with them.
        if (event->isClockEvent()) {
          const STSymbolTableNode* clock = event->getClock();
          if (mAllowedNodes->find(clock) == mAllowedNodes->end()) {
            // This event references a clock we can't use.  Lookup or
            // create a replacement.
            const STAliasedLeafNode* newNode;
            ReplacementMap::iterator iter = mReplacements.find(clock);
            if (iter == mReplacements.end()) {
              // Create a new node in the top scope of the symbol table.
              UtString nodeName;
              nodeName << scPrefix << mNextIndex;
              ++mNextIndex;
              StringAtom* nodeAtom = mAtomicCache->intern(nodeName);
              newNode = mDesignTable->createLeaf(nodeAtom, mRootNode);
              mReplacements[clock] = newNode;
              // We also need to include it in the set of allowed
              // nodes so it won't be excluded when the DB is written.
              mAllowedNodes->insert(newNode);
            } else {
              newNode = iter->second;
            }
            
            // Create an event that is the same as the original, but
            // with the new node.
            ClockEdge edge = event->getClockEdge();
            UInt32 priority = event->getPriority();
            event = mSchedFactory->buildClockEdge(newNode, edge, priority);
            createNewMask = true;
          }
        }
        // Add the event (which may or may not be a new one) to the
        // mask in progress.  We do this even if we didn't create a
        // new node, because other events may need replacement, in
        // which case we will need to create a new mask.  The factory
        // is stateful and creates masks from all the events it's
        // working on.
        mSchedFactory->addEvent(event);
      }

      // If we replaced any nodes and need to create a new mask, do that
      // and return it.  Otherwise return the original mask.
      if (createNewMask) {
        newMask = mSchedFactory->buildMask();
      }
      return newMask;
    }

  };
};

const char* IODBNucleus::DBClosure::SignatureReplacer::scPrefix = "$hiddenclk_;";

static void sPrintNodes(const STAliasedLeafNode* identLeaf, 
                        const STAliasedLeafNode* identStorage)
{
  UtString name;
  HdlVerilogPath pather;
  pather.compPathHier(identLeaf, &name);
  UtIO::cout() << "Ident: " << name << UtIO::endl;
  
  pather.compPathHier(identStorage, &name);
  UtIO::cout() << "Storage: " << name << UtIO::endl;
}

//! Check for cycles against a source ident
/*!
  This class also verifies net flag consistency between the source
  ident and any sub idents. So, if the src ident is observable, the
  sub idents must be, too. Ditto for forcible, depositable, etc.
*/
class IODBNucleus::IdentCycleDetect: public CarbonExprWalker
{
public:
  IdentCycleDetect(CarbonIdent* srcIdent,
                   CbuildSymTabBOM* bomManager,
                   IODBNucleus* iodb) : 
    mSrcIdent(srcIdent), mIODB(iodb), mBomManager(bomManager)
  {
    // we need to keep the actual source ident separate in mSrcIdent
    // so we can do flag consistency checking. But, we also need to
    // keep the source ident in a set so we can make sure that
    // recursive cycle checking will find a cycle with any of the
    // parent expressions.
    addSrcIdent(srcIdent);
  }
  
  virtual ~IdentCycleDetect() {}

  void addSrcIdent(CarbonIdent* srcIdent)
  {
    mSrcIdents.insert(srcIdent);
  }

  virtual void visitIdent(CarbonIdent* ident) {
    // Check if there is a cycle. There is a cycle if this ident is in
    // the set of parent idents. A parent ident is an ident that is
    // described by an expression (bommanager->getExpr(ident) != NULL and
    // CbuildSymTabBOM::getTypeTag(ident->getNode()->getStorage())  ==
    // eExprId
    FUNC_ASSERT(mSrcIdents.count(ident) == 0, ident->printVerilog());
    
    DynBitVector useMask;
    const STAliasedLeafNode* srcLeaf = mSrcIdent->getNode(&useMask);
    const NUNet* srcNet = NUNet::find(srcLeaf);
    ST_ASSERT(srcNet, srcLeaf);
    
    STAliasedLeafNode* identLeaf = ident->getNode(&useMask);
    const STAliasedLeafNode* identStorage = identLeaf->getStorage();
    const NUNet* identStoreNet = NUNet::find(identStorage);
    const NUNet* identNet = NUNet::find(identLeaf);

    // Check that the flags propagated to the split nets
    if (srcNet->isForcible())
    {
      if (! identStoreNet->isForcible())
        sPrintNodes(identLeaf, identStorage);
      NU_ASSERT(identStoreNet->isForcible(), identNet);
      mIODB->mapSymNodeToLoc(identLeaf, identNet->getLoc());
    }

    /*
      At this point, we use to do a depositable flag consistency
      check, which was way too aggressive. Bug 6820, mips/m24k

      The problem with doing that is that constant propagation may
      have marked a future splitnet an alias to a depositable. In
      addition, that splitnet will be flattened and made
      unreachable. The flag propagation in PortSplitting
      only really checks ProtectedObservable and
      ProtectedMutable. 
    */

    if (srcNet->isProtectedObservableNonHierref(mIODB))
    {
      bool isProtectedObservNonHier = identNet->isProtectedObservableNonHierref(mIODB);
      if (! isProtectedObservNonHier)
        sPrintNodes(identLeaf, identStorage);
      NU_ASSERT(isProtectedObservNonHier, identNet);
      CbuildSymTabBOM::markObservableNet(identLeaf);
    }

    if (srcNet->isProtectedMutableNonHierref(mIODB, false))
    {
      bool isProtectedMutableNonHier = identNet->isProtectedMutableNonHierref(mIODB, false);
      if (! isProtectedMutableNonHier)
        sPrintNodes(identLeaf, identStorage);
      
      NU_ASSERT(isProtectedMutableNonHier, identNet);
      CbuildSymTabBOM::markDepositableNet(identLeaf);
    }
    // recurse, if necessary. It is necessary to recurse if the ident
    // is mapped to another expression (described by another
    // expression). Currently, this is not possible. If Separation
    // Reconstruction is turned on, it is quite possible since a
    // separated net may be expressioned and then a net within the
    // expression gets separated later.
    CarbonExpr* subIdentExpr = mBomManager->getExpr(ident);
    if (subIdentExpr)
    {
      IdentCycleDetect subIdentCycleDetect(ident, mBomManager, mIODB);
      for (IdentSet::UnsortedLoop p = mSrcIdents.loopUnsorted(); 
           !p.atEnd(); ++p)
        subIdentCycleDetect.addSrcIdent(*p);
      subIdentCycleDetect.visitExpr(subIdentExpr);
    }
  }
  
private:
  CarbonIdent* mSrcIdent;
  IODBNucleus* mIODB;
  CbuildSymTabBOM* mBomManager;

  typedef UtHashSet<CarbonIdent*> IdentSet;
  IdentSet mSrcIdents;
  
  IdentCycleDetect();
  IdentCycleDetect(const IdentCycleDetect&);
  IdentCycleDetect& operator=(const IdentCycleDetect&);
};

static bool sIsPortDriver(const NUNet* net)
{
  return net->isOutput() || net->isBid();
}

// Simple enum to determine deadness
enum DeadnessEnum {
  eDeadness_ISLIVE, // not dead
  eDeadness_ISDEAD, // dead
  eDeadness_ISSUBEXPR // Don't know, it is a nested expr
};

static DeadnessEnum sNetHasRuntimeStorage(CarbonIdent* ident, IODBNucleus* iodb, CbuildSymTabBOM* bomManager)
{
  DynBitVector useMask;
  STAliasedLeafNode* leaf = ident->getNode(&useMask);
  STAliasedLeafNode* storageLeaf = leaf->getStorage();
  const NUNet* net = NUNet::find(storageLeaf);
  bool hasStorage = ((net != NULL) and (CbuildSymTabBOM::isTypeTagValid(storageLeaf)) and (CbuildSymTabBOM::getSCHSignature(storageLeaf) != NULL));
  if (hasStorage)
  {
    // check that type tag is valid for storage and if applicable
    // the offset is also valid.
    int typeCode = CbuildSymTabBOM::getTypeTag(storageLeaf);
    switch(static_cast<CbuildShellDB::TypeIndexTag>(typeCode))
    {
    case CbuildShellDB::eNoTypeId:
      hasStorage = false;
      break;
    case CbuildShellDB::eConstValWordId:        
    case CbuildShellDB::eConstValId:        
      // Will become a ShellNetConstant in the shell
      hasStorage = true;
      break;
    case CbuildShellDB::eOffsetId:
      // This gets run before the type dictionary is compiled and
      // thus all type indices are invalid.
      // check if the signature is NULL. If so, we have a hole
      if (CbuildSymTabBOM::getSCHSignature(storageLeaf) == NULL)
        hasStorage = false;
      else
        hasStorage = true;
      break;
    case CbuildShellDB::eExprId:
      // described by another expression
      hasStorage = false;
      break;
    }
  }
  
  // first, check if this net is overridden at all.
  if (net)
  {
    const DynBitVector* bv = iodb->getConstNetBitMask(leaf);
    if (bv)
    {
      // if any part of the net is overridden, this net has storage.
      hasStorage = true; // this skips the hole detection.
    }
  }

  DeadnessEnum ret = eDeadness_ISLIVE;
  if (! hasStorage)
  {
    ret = eDeadness_ISDEAD;

    // check if this node is mapped. Not the storage node, because
    // intrinsic information must be maintained between mappings.
    CarbonExpr* subIdentExpr = bomManager->getExpr(ident);
    bool isHole = subIdentExpr == NULL;
    if (! isHole)
    {
      // The ident has either been further mapped or it just points to
      // itself. If it has been mapped further then we don't know if
      // the net is dead.
      CarbonIdent* subIdentCast = subIdentExpr->castIdent();
      if (subIdentCast == ident)
        // This just points to itself. It has no storage, therefore it
        // is dead.
        ret = eDeadness_ISDEAD;
      else
        // Don't know if it is dead.
        ret = eDeadness_ISSUBEXPR;
    }
  }
  return ret;
}

// Helper class to output warnings for dead ident exprs.
/*
  Eventually, I want this to gather all the nets to be warned about,
  optimize the warnings by grouping clusters of bits and partsels of
  the same net together, and outputing a sane message.
*/
class IODBNucleus::DeadNetWarn
{
public:
  
  // Grabs the the only net expected to be seen in an expression
  class BPExclusiveNetExtractor : public CarbonExprWalker
  {
  public:
    BPExclusiveNetExtractor() : mNet(NULL) {}
    
    virtual void visitIdent(CarbonIdent* ident)
    {
      SymTabIdent* symTabIdent = ident->castSymTabIdent();
      STAliasedLeafNode* leaf = symTabIdent->getNode();
      NUNet* tmpNet = NUNet::find(leaf);
      NU_ASSERT(mNet == NULL, tmpNet);
      mNet = tmpNet;
    }

    NUNet* getNet() { return mNet; }
    
  private:
    NUNet* mNet;
  };

  DeadNetWarn(IODBNucleus* iodb, CarbonIdent* origIdent) : 
    mIODB(iodb), mSrcIdent(origIdent)
  {}

  // Returns whether or not the dead net(s) would drive a port
  bool warn(CarbonIdent* ident)
  {
    bool wouldDrive = false;
    
    SymTabIdent* symTabIdent = ident->castSymTabIdent();
    CE_ASSERT(symTabIdent, ident);
    STAliasedLeafNode* leaf = symTabIdent->getNode();
    STAliasedLeafNode* storageLeaf = leaf->getStorage();
    const NUNet* useNet = NUNet::find(storageLeaf);
    if (useNet == NULL)
    {
      // no net, look at the back pointer and use what the user
      // declared the net as.
      SymTabIdent* symTabIdent = ident->castSymTabIdent();
      FUNC_ASSERT(symTabIdent, ident->printVerilog()); 
      SymTabIdentBP* symTabIdentBP = symTabIdent->castSymTabIdentBP();
      FUNC_ASSERT(symTabIdentBP, symTabIdent->printVerilog());
      CarbonExpr* bpExpr = symTabIdentBP->getBackPointer();
      
      // Extract the first net from the expression. It as assumed that
      // all the nets in a given expression are consistent in direction
      BPExclusiveNetExtractor netExtractor;
      netExtractor.visitExpr(bpExpr);
      useNet = netExtractor.getNet();
      
      wouldDrive = sIsPortDriver(useNet);
    }
    
    const SourceLocator* srcLoc = &(useNet->getLoc());
    MsgContext* msgContext = mIODB->getMsgContext();
    
    // I want the warning to refer to the original net.
    // backpointers refer to the original net
    UtString name;
    ident->compose(&name); 
    
    const char* widthNoun = "bit";
    UInt32 identWidth = ident->getBitSize();
    if (identWidth == mSrcIdent->getBitSize())
      widthNoun = "identifier";
    else if (ident->getBitSize() > 1)
      widthNoun = "range";
    
    if (wouldDrive)
    {
      msgContext->CmplrDeadSplitNet(srcLoc, name.c_str(), widthNoun, "undriven");
    }
    else
      msgContext->CmplrDeadSplitNet(srcLoc, name.c_str(), widthNoun, "dead");
    
    return wouldDrive;
  }
  
private:
  IODBNucleus* mIODB;
  CarbonIdent* mSrcIdent;
};

class IODBNucleus::ExprIdentWalk : public CopyWalker
{
public:
  ExprIdentWalk(IODBNucleus* iodb, CbuildSymTabBOM* bomManager,
                CarbonIdent* srcIdent) : 
    CopyWalker(bomManager->getExprFactory()),
    mIODB(iodb),
    mBOMManager(bomManager),
    mSrcIdent(srcIdent)
  {}
  
  virtual ~ExprIdentWalk() {}

  virtual CarbonExpr* transformIdent(CarbonIdent* ident)
  {
    CarbonExpr* retExpr = ident;
    
    DynBitVector useMask;
    STAliasedLeafNode* leaf = ident->getNode(&useMask);
    // elab should have replaced nets that were not elab'd with
    // constants
    FUNC_ASSERT(leaf, ident->printVerilog());

    CarbonIdent* leafIdent = CbuildSymTabBOM::getIdent(leaf);
    if (leafIdent) {
      // These better be the same!
      ST_ASSERT(ident == leafIdent, leaf);
    }
    
    // Do we have a cycle?
    {
      IdentCycleDetect subIdentCycleDetect(mSrcIdent, mBOMManager, mIODB);
      subIdentCycleDetect.visitExpr(ident);
    }
    

    // Fix up the alias ring in the presence of pulls
    sFixNetPulls(leaf);


    STAliasedLeafNode* storageLeaf = leaf->getStorage();
    
    /*
      If the net does not exist for this leaf, then it is optimized
      away. Therefore, we must replace this part of the describing
      expression with a const x.
    */
    DeadnessEnum deadness = sNetHasRuntimeStorage(ident, mIODB, mBOMManager);
    switch(deadness)
    {
    case eDeadness_ISLIVE:
      break;
    case eDeadness_ISDEAD:
      {
        // create a constant override mask for this node. No net needs
        // to exist. The runtime checks for completely overridden
        // nodes. 
        // we need to know which direction this net goes
        DeadNetWarn netWarn(mIODB, mSrcIdent);
        bool wouldDrive = netWarn.warn(ident);

        Carbon4StateVal deadVal = mIODB->getDeadBitWave();
        if (wouldDrive)
          deadVal = mIODB->getUndrivenBitWave();
        

        DynBitVector mask(ident->getBitSize());
        mask.set();
        queueHoleFiller(storageLeaf, mask, deadVal);

        retExpr = ident; // for clarity
      }
      break;
    case eDeadness_ISSUBEXPR:
      {
        CarbonExpr* subIdentExpr = mBOMManager->getExpr(ident);
        ST_ASSERT(subIdentExpr, leaf);
        retExpr = subIdentExpr;
        // Ok. The ident has been optimized to a different expression.
        // We need to do a cycle detection here.
        // Cycle detect against the src ident only. When the subident
        // expression gets transformed it will do its own cycle
        // detection as above.
        IdentCycleDetect walk(mSrcIdent, mBOMManager, mIODB);
        // will assert if a cycle is detected.
        walk.visitExpr(subIdentExpr);
      }
      break;
    }
    
    return retExpr;
  }

  // This fills holes in exprs that have dead idents
  /*
    This has to be done separately from the walk because the same net
    may be in several expressions. If we fill it too soon, we won't
    get all the warnings out.
  */
  void fillHoles()
  {
    for (HoleFillerVec::iterator p = mHoleFillerVec.begin(), e =  mHoleFillerVec.end(); p != e; ++p)
    {
      HoleFiller& fillInfo = *p;
      mIODB->declareConstBits(fillInfo.mLeaf, 
                              fillInfo.mMask, 
                              fillInfo.mVal);
    }
  }

private:
  IODBNucleus* mIODB;
  CbuildSymTabBOM* mBOMManager;
  CarbonIdent* mSrcIdent;

  struct HoleFiller
  {
    HoleFiller(STAliasedLeafNode* leaf,
               const DynBitVector& mask, 
               Carbon4StateVal val)
      : mLeaf(leaf), mMask(mask), mVal(val)
    {}

    STAliasedLeafNode* mLeaf;
    DynBitVector mMask;
    Carbon4StateVal mVal;
  };

  typedef UtVector<HoleFiller> HoleFillerVec;
  HoleFillerVec mHoleFillerVec;
  
  ExprIdentWalk();
  ExprIdentWalk(const ExprIdentWalk&);
  ExprIdentWalk& operator=(const ExprIdentWalk&);

  void queueHoleFiller(STAliasedLeafNode* leaf,
                       const DynBitVector& mask,
                       Carbon4StateVal deadVal)
  {
    mHoleFillerVec.push_back(HoleFiller(leaf, mask, deadVal));
  }

};

class IODBNucleus::DeadExprDetect : public CarbonExprWalker
{
public:
  DeadExprDetect(IODBNucleus* iodb, CbuildSymTabBOM* bomManager) : 
    mIODB(iodb), mBOMManager(bomManager), mIsDead(true), mSeenIdent(false)
  {}
  
  virtual bool preVisitIdent(CarbonIdent* ident)
  {
    mSeenIdent = true;
    DeadnessEnum deadness = sNetHasRuntimeStorage(ident, mIODB, mBOMManager);
    switch(deadness)
    {
      // if live or is sub-expressioned, it is live. The sub
      // expression could be dead, but we want to avoid a cycle. Cycle
      // detection and flattening happen later. This can be run again
      // after those have been completed.
    case eDeadness_ISLIVE:
    case eDeadness_ISSUBEXPR:
      mIsDead = false;
      break;
    case eDeadness_ISDEAD:
      break;
    }

    // If this ident is live we stop the walk.
    return mIsDead;
  }
  
  bool isDead() const { return mSeenIdent && mIsDead; }
  
private:
  IODBNucleus* mIODB;
  CbuildSymTabBOM* mBOMManager;
  bool mIsDead;
  // True once we see an ident. This is used to determine whether or
  // not mIsDead refers to actual nets. If no ident is seen, that
  // means the expression is completely constant, and the expression
  // therefore is not dead.
  bool mSeenIdent;
};

class IODBNucleus::ExprAccuracyWalker : public CarbonExprWalker
{
public:
  ExprAccuracyWalker(IODBNucleus* iodb) : mIODB(iodb),
                                          mIsAccurate(true)
  {}

  virtual bool preVisitIdent(CarbonIdent* ident)
  {
    DynBitVector useMask;
    STAliasedLeafNode* node = ident->getNode(&useMask);
    if (mIODB->isInvalidWaveNet(node))
      mIsAccurate = false;
    return mIsAccurate;
  }

  bool isInaccurate() const { return ! mIsAccurate; }

private:
  IODBNucleus* mIODB;
  bool mIsAccurate;
};

// forward declaration of depositable node marker 
static void sMarkDepositableNet(STAliasedLeafNode* leaf);

class IODBNucleus::ExprMarkDeposit : public CarbonExprWalker
{
public:
  virtual void visitIdent(CarbonIdent* ident)
  {
    SymTabIdent* symTabIdent = ident->castSymTabIdent();
    STAliasedLeafNode* leaf = symTabIdent->getNode();
    sMarkDepositableNet(leaf);
  }
};

// propagate pullup/down to primary net
static void sFixNetPulls(STSymbolTableNode* node)
{
  STAliasedLeafNode* primLeaf = node->castLeaf();
  ST_ASSERT(primLeaf, node);

  NUNet* net = NULL;

  // First, run the ring looking for a pull/tri0/tri1
  bool isPulledUp = false;
  bool isPulledDown = false;
  STAliasedLeafNode* curLeaf = primLeaf;
  
  do {
    net = NUNet::find(curLeaf);
    if (net)
    {
      isPulledUp = net->isPullUp() || net->isTri1();
      isPulledDown = net->isPullDown() || net->isTri0();
    }
    curLeaf = curLeaf->getAlias();
  } while ((curLeaf != primLeaf) && ! (isPulledUp || isPulledDown));
  
  if (isPulledUp || isPulledDown)
  {
    // Now, run through the ring and set the appropriate pulls
    curLeaf = primLeaf;
    do {
      net = NUNet::find(curLeaf);
      if (net)
      {
        if (isPulledUp)
          net->putIsPullUp(true);
        else if (isPulledDown)
          net->putIsPullDown(true);
      }
      curLeaf = curLeaf->getAlias();
    } while (curLeaf != primLeaf);
  } // if actually pulled
}

void IODBNucleus::checkTypeID(const STSymbolTableNode* protectedNode, const char* marking, CbuildSymTabBOM* symTabBOM) const
{
  const STAliasedLeafNode* protectedLeaf = protectedNode->castLeaf();
  
  bool isExprNet = symTabBOM->isExprMappedNode(protectedNode);
  
  if (CbuildSymTabBOM::isTypeTagValid(protectedLeaf) &&
      (CbuildSymTabBOM::getTypeTag(protectedLeaf) == CbuildShellDB::eNoTypeId) &&
      ! isExprNet)
  {
    UtString name;
    mHdl->compPathHier(protectedNode, &name, NULL);
    mMsgContext->CmplrNoTypeIDForProtectedNet(name.c_str(), marking);
  }
}

void IODBNucleus::maybeMarkStateOutput(STAliasedLeafNode* leaf) 
{
  bool foundState = false;
  for (STAliasedLeafNode::AliasLoop aloop(leaf);
       ! aloop.atEnd() && ! foundState; ++aloop)
  {
    const STAliasedLeafNode* alias = *aloop;

    // If this leaf is a force subordinate for a forcible state
    // output, it won't have a NUNet.  Use the force master in that
    // case.
    const STAliasedLeafNode *forceMaster = getSubordinateMasterForcible(alias);
    if (forceMaster != NULL) {
      alias = forceMaster;
    }

    const NUNet* net = NUNet::find(alias);
    if (net && (net->isFlop() || net->isLatch()))
    {
      foundState = true;
      CbuildSymTabBOM::markStateOutput(leaf);
    }
  }
  
}

void IODBNucleus::findBidis(STAliasedLeafNode* leaf, NameSet* bidis) 
{
  STAliasedLeafNode* storage = NULL;
  if (leaf)
    storage = leaf->getStorage();
  if (storage && (bidis->find(storage) == bidis->end()))
  {

    if (CbuildSymTabBOM::isTypeIndexValid(storage))
    {
      UInt32 typeIndex = CbuildSymTabBOM::getTypeIndex(storage);
      const IODBGenTypeEntry* entry = mTypeDictionary->getGenTypeEntry(typeIndex);
      /*
        For some reason, yet to be determined, the shell, at times,
        gets primary inputs that are marked as input and tristate, but
        not as bidirect. This may cause problems if we need to know
        the exact number of bidirects in the runtime. This fixes that
        by keeping track of bidirects as well as tristate inputs.
      */
      if (entry && (entry->isBidirect() || (entry->isTristate() && entry->isPrimaryInput())))
        bidis->insert(storage);
    }
  }  
}

void IODBNucleus::markReconSubordinates()
{
  STFieldBOM* fieldBOM = mDesignTable->getFieldBOM();
  CbuildSymTabBOM* symTabBOM = reinterpret_cast<CbuildSymTabBOM*>(fieldBOM);
  INFO_ASSERT(symTabBOM, "Incorrect symtab type.");
  
  // Loop the table and look for mapped expressions. Expression-based
  // set closure takes place here, so the expression map actually may
  // change during the iteration. Must be sorted because we report
  // holes in the expression.
  for (STSymbolTable::NodeLoopSorted p = mDesignTable->getNodeLoopSorted();
       ! p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf)
    {
      CarbonIdent* ident = CbuildSymTabBOM::getIdent(leaf);
      if (ident)
      {
        CarbonExpr* expr = symTabBOM->getExpr(ident);
        
        // Don't NULL out a dead primary leaf (yet). We can
        // potentially do this, but it requires a change in the shell
        // to create CarbonNets for such things.
        if (expr && ! isPrimary(leaf))
        {
          /*
            Do dead expression detection. If the underlying net(s) of
            the expression are dead, remove the expression from the
            map, and rely on the fact that the replaced net has a NULL
            signature to tell the shell to not dump the value.
          */
          DeadExprDetect deadDetect(this, symTabBOM);
          deadDetect.visitExpr(expr);
          if (deadDetect.isDead())
          {
            DeadNetWarn netWarn(this, ident);
            netWarn.warn(ident);
            
            symTabBOM->removeMappedExpr(ident);
            // Set expr to NULL here so we don't do any more
            // processing on it.
            expr = NULL;
          }
        }
        
        if (expr)
        {
          // This net has been optimized. 
          // If the expression is an N-ary operator we need to search
          // for holes and mark them. 
          if (expr->castNary())
          {
            // assert that the expression is only a concat. We don't
            // have a multi-driver expression yet, but if/when we do
            // we need to add support for that.
            CE_ASSERT(expr->castConcat(), expr);
            
            ExprIdentWalk identWalker(this, symTabBOM, ident);
            identWalker.visitExpr(expr);
            identWalker.fillHoles();
            CarbonExpr* transExpr = identWalker.getResult();
            if (transExpr != expr)
            {
              // remap the ident to the transformed expression
              symTabBOM->mapExpr(ident, transExpr);
              // Reset expr to be the newly created transExpr for the
              // flattening phase
              expr = transExpr;
            }
          }

          // If the expression is an N-ary operator we need to flatten
          // it to reduce runtime overhead and to detect any cycles.
          CarbonConcatOp *concat = expr->castConcat();
          if ( concat != NULL ) {
            ExprFlatten flattenWalker( symTabBOM, ident, concat );
            flattenWalker.visitExpr( concat );
            CarbonExpr* transExpr = flattenWalker.getResult();
            if ( transExpr != expr )
              symTabBOM->mapExpr(ident, transExpr);
          }

          // If the expression contains any unwaveable nets the entire
          // expression is unwaveable. So, we need to update the db
          // here. Note this has to run after expression reduction and
          // flattening in order to flatten any nested expressions
          ExprAccuracyWalker accuracyWalk(this);
          accuracyWalk.visitExpr(expr);
          if (accuracyWalk.isInaccurate())
            declareInvalidWaveNet(leaf);
        } // if expr
      } // if ident
    } // if leaf
  } // for

  // The expressions at this point are set up. But, their types are
  // incorrect. Loop again and for each mapped expression, set its
  // type tag to eExprId.
  for (STSymbolTable::NodeLoop p = mDesignTable->getNodeLoop();
       ! p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf)
    {
      CarbonIdent* ident = CbuildSymTabBOM::getIdent(leaf);
      if (ident)
      {
        CarbonExpr* expr = symTabBOM->getExpr(ident);
        if (expr)
        {
          // Double-check that we do not have a cycle
          {
            IdentCycleDetect subIdentCycleDetect(ident, symTabBOM, this);
            subIdentCycleDetect.visitExpr(expr);
          }

          // This net has been optimized. 
          CbuildSymTabBOM::setTypeTag(leaf, CbuildShellDB::eExprId);
        }
      }
    }
  }
}

// This marks the alias ring as depositable.
static void sMarkDepositableNet(STAliasedLeafNode* leaf)
{
  leaf = leaf->getStorage();
  const NUNet* net = NUNet::find(leaf);
  if (net)
  {
    // this will never happen, but just in case
    if (! net->isHierRef()) 
    {
      // Assert if this net is not depositable or an input
      if (!net->isPrimaryInput() && ! net->isPrimaryBid())
        // This better be depositable
        NU_ASSERT(net->isDepositable(), net);
      // The runtime doesn't care if the net is a primary input or
      // not. It just want to know if it is depositable in any
      // way. Another walk marks all primary inputs as depositable
      // anyway for db purposes.
      CbuildSymTabBOM::markDepositableNet(leaf);
    }
  }
}

void IODBNucleus::preWriteCheck()
{
  // Run through the symtab looking for expressions. Fill any holes
  // with the appropriate constant marking.
  markReconSubordinates();

  // Next, we have to loop the primary outputs and bidis and
  // propagate pullup/down to the netflags.
  // Also, mark primary bidi and input rings as depositable and
  // observable.
  // Mark outputs and observable rings as observable
  {
    for (NameSetLoop outs = loopOutputs(); ! outs.atEnd(); ++outs)
    {
      STSymbolTableNode* outNode = *outs;
      sFixNetPulls(outNode);
      
      STAliasedLeafNode* primLeaf = outNode->castLeaf();
      CbuildSymTabBOM::markObservableNet(primLeaf);
    }
    
    for (NameSetLoop bids = loopBidis(); ! bids.atEnd(); ++bids)
    {
      STSymbolTableNode* bidNode = *bids;
      sFixNetPulls(bidNode);

      STAliasedLeafNode* primLeaf = bidNode->castLeaf();
      CbuildSymTabBOM::markDepositableNet(primLeaf);
      CbuildSymTabBOM::markObservableNet(primLeaf);
    }
    
    for (NameSetLoop ins = loopInputs(); ! ins.atEnd(); ++ins)
    {
      STSymbolTableNode* inNode = *ins;
      sFixNetPulls(inNode);
      
      STAliasedLeafNode* primLeaf = inNode->castLeaf();
      CbuildSymTabBOM::markDepositableNet(primLeaf);
      CbuildSymTabBOM::markObservableNet(primLeaf);
    }
    
    for (NameSetLoop obs = loopObserved(); ! obs.atEnd(); ++obs)
    {
      STSymbolTableNode* obsNode = *obs;
      sFixNetPulls(obsNode);
      STAliasedLeafNode* primLeaf = obsNode->castLeaf();
      CbuildSymTabBOM::markObservableNet(primLeaf);
    }

    // Now look for depositables, if they are in the iodb propagate the
    // depositable flag to all its aliases.
    STFieldBOM* fieldBOM = mDesignTable->getFieldBOM();
    CbuildSymTabBOM* symTabBOM = reinterpret_cast<CbuildSymTabBOM*>(fieldBOM);
    for (NameSetLoop deps = loopDeposit(); ! deps.atEnd(); ++deps)
    {
      STSymbolTableNode* node = *deps;
      sFixNetPulls(node);
      
      STAliasedLeafNode* leaf = node->castLeaf();
      if (leaf != NULL) {
        CarbonIdent* srcIdent = CbuildSymTabBOM::getIdent(leaf);
        if (srcIdent)
        {
          // we have a depositable signal that was expressioned. We
          // have to walk the expression and mark all the valid nets
          // as depositable
          CarbonExpr* srcMappedExpr = symTabBOM->getExpr(srcIdent);
          // It may be null due to expression reduction (in
          // NUAliasDB.cxx)
          if (srcMappedExpr)
          {
            ExprMarkDeposit exprMarkDepositWalk;
            exprMarkDepositWalk.visitExpr(srcMappedExpr);
          }
        }
        
        sMarkDepositableNet(leaf);
      }
    }
  }

  // Also, keep tabs on nets that could be bidis. Keep the
  // number of bidis to write to the db.
  // Also look for state outputs, marking the
  // alias ring.
  {
    NameSet visited;
    NameSet bidis;

    for (STSymbolTable::NodeLoop p = mDesignTable->getNodeLoop();
         ! p.atEnd(); ++p)
    {
      STSymbolTableNode* node = *p;
      STAliasedLeafNode* leaf = node->castLeaf();
      if (leaf && (visited.insertWithCheck(leaf->getStorage())))
      {
        findBidis(leaf, &bidis);
        maybeMarkStateOutput(leaf);
      }
    }
    mDesignInfo->putNumBidis(bidis.size());
  }
  
  // Next, loop through all the observable/depositable/forcible nets
  // and make sure that a typeID is assigned to them. Warn, otherwise.
  {
    STFieldBOM* fieldBOM = mDesignTable->getFieldBOM();
    CbuildSymTabBOM* symTabBOM = reinterpret_cast<CbuildSymTabBOM*>(fieldBOM);
    INFO_ASSERT(fieldBOM, "Incorrect symtab type.");
    
    for (NameSetLoop obs = loopObserved(); ! obs.atEnd(); ++obs)
      checkTypeID(*obs, "observable", symTabBOM);
    
    for (NameSetLoop deps = loopDeposit(); ! deps.atEnd(); ++deps)
      checkTypeID(*deps, "depositable", symTabBOM);
    
    for (NameSetLoop forced = loopForced(); ! forced.atEnd(); ++forced)
      checkTypeID(*forced, "forcible", symTabBOM);

    for (NameSetLoop delayedClocks = loopDelayedClocks(); 
         ! delayedClocks.atEnd(); ++delayedClocks) {
      checkTypeID(*delayedClocks, "delayedClocks", symTabBOM);
    }
  }


  // Loop the clocks, marking the alias rings
  for (NameSetLoop clocks = loopClocks(); ! clocks.atEnd(); ++clocks)
  {
    STSymbolTableNode* node = *clocks;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf)
      CbuildSymTabBOM::markClockNet(leaf);
  }

  // Loop the primary clocks, marking the alias rings
  for (NameSetLoop clocks = loopPrimaryClocks(); ! clocks.atEnd(); ++clocks)
  {
    STSymbolTableNode* node = *clocks;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf)
      CbuildSymTabBOM::markClockNet(leaf);
  }


  // Now loop the force subordinate map and mark any force
  // subordinates whose master is a clock.
  for (LeafToLeafMap::UnsortedLoop p = mForceSubordinates.loopUnsorted();
       ! p.atEnd(); ++p)
  {
    const STAliasedLeafNode* master = p.getValue();
    if (CbuildSymTabBOM::isClockNet(master))
      CbuildSymTabBOM::markClockNet(p.getKey());
  }
}

#if 0
ZostreamDB* IODBNucleus::createFullDBSection(const char* sectionName) {
  // Signature and version entry
  ZostreamDB* symDBPtr = symDBZip.addDatabaseEntry(sectionName);
  if (! symDBPtr)
  {
    // something went wrong
    mMsgContext->CGSymtabDBOpen(symDBZip.getFileError());
    return NULL;
  }
  return symDBPtr;
}
#endif

void IODBNucleus::getDBFileName(UtString* fname, CarbonDBType type,
                                const char* fileroot)
{
  *fname = fileroot;
  switch (type) {
  case eCarbonFullDB:
    *fname += IODB::scFullDBExt;
    break;
  case eCarbonIODB:
    *fname += IODB::scIODBExt;
    break;
  case eCarbonGuiDB:
    *fname += IODB::scGuiDBExt;
    break;
  case eCarbonAutoDB:
    // This is only valid for model creation
    INFO_ASSERT(0, "Invalid DB type");
  }
}

bool
IODBNucleus::writeNameIntMap(NameIntMapLoop p, ZostreamDB& symDB,
                             STSymbolTable* symtab)
{
  for (; !p.atEnd(); ++p) {
    STSymbolTableNode* node = p.getKey();
    STSymbolTableNode* safeNode = symtab->safeLookup(node);
    ST_ASSERT(safeNode, node);
    if (!symDB.writePointer(safeNode))
      return false;
    UInt32 value = p.getValue();
    if (! (symDB << value))
      return false;
  }
  if (!symDB.writeNull())
    return false;
  return true;
}

bool IODBNucleus::writeDB(const char* fileroot, DBClosure* dbClosure) {
  UtString fileAndExt;
  getDBFileName(&fileAndExt, dbClosure->getDBType(), fileroot);

  // Do the writing to tmp filenames so that we don't get a
  // half-written libdesign.io.db or libdesign.symtab.db file around.
  // We'll get a half-written libdesign.tmp.io.db instead, which is
  // a little better because the GUI won't to load that.
  UtString tmpname(fileAndExt);
  tmpname << ".tmp";

  bool success = writeDBHelper(tmpname, dbClosure);
  if (success) {
    UtString err;
    if (OSRenameFile(tmpname.c_str(), fileAndExt.c_str(), &err) != 0) {
      mMsgContext->CGSymtabDBOpen(err.c_str());
      success = false;
    }
  }
  else {
    UtString err;
    (void) OSUnlink(tmpname.c_str(), &err);
  }
  return success;
} // bool IODBNucleus::writeDB

bool IODBNucleus::writeDBHelper(const UtString& fileAndExt, DBClosure* dbClosure)
{
  DBClosure::CbuildSymtab* symtabClosure = dbClosure->getTmpSymtab();
  const CbuildSymTabBOM* cbuildBOM = symtabClosure->mCbuildBomManager;

  ZOSTREAMZIP(symDBZip, fileAndExt.c_str());
  if (!symDBZip)
  {
    mMsgContext->CGSymtabDBOpen(symDBZip.getFileError());
    return false;
  }

  // Signature and version entry
  UtString entryName;
  ZostreamDB* symDBPtr = symDBZip.addDatabaseEntry(getEntryName(&entryName, eEntryVersion));
  if (! symDBPtr)
  {
    // something went wrong
    mMsgContext->CGSymtabDBOpen(symDBZip.getFileError());
    return false;
  }
  
  {
    UtString signature;
    UInt32 iodbVersion = sGetSigAndVersion(&signature);
    (*symDBPtr) << signature;
    (*symDBPtr) << iodbVersion;
  }
  if (! symDBZip.endDatabaseEntry())
    return false;

  // Write out the capabilities this database has.  This identifies fields that
  // are present, so that later versions of software can read this database
  // without attempting to bring in new fields that are not present in it.
  //
  // As of 12/27/05, there are no extra fields, so just write out the 0-length
  // capability.
  ZostreamDB* capabilitiesDBPtr =
    symDBZip.addDatabaseEntry(getEntryName(&entryName, eEntryCapabilities));
  if (! capabilitiesDBPtr) {
    return false;
  }
  *capabilitiesDBPtr << (UInt32)(mCapabilities->size());
  for (UtStringSet::iterator p = mCapabilities->begin(),
         e = mCapabilities->end(); p != e; ++p)
  {
    UtString& capability = *p;
    *capabilitiesDBPtr << capability;
  }         
  
  if (! symDBZip.endDatabaseEntry())
    return false;
  
  // DesignInfo entry
  symDBPtr = symDBZip.addDatabaseEntry(getEntryName(&entryName, eEntryDesignInfo));
  if (! symDBPtr)
  {
    // something went wrong
    mMsgContext->CGSymtabDBOpen(symDBZip.getFileError());
    return false;
  }

  mDesignInfo->putDBType(dbClosure->getDBType());
  
  mDesignInfo->dbWrite(*symDBPtr);
  
  if (! symDBZip.endDatabaseEntry())
    return false;


  // UtCustomerDB entry
  symDBPtr = symDBZip.addDatabaseEntry(getEntryName(&entryName, eEntryCustDB));
  if (! symDBPtr)
  {
    // something went wrong
    mMsgContext->CGSymtabDBOpen(symDBZip.getFileError());
    return false;
  }
  mCustomerDB.dbWrite(*symDBPtr);
  if (! symDBZip.endDatabaseEntry())
    return false;

  // Dump the SourceLocatorFactory instance before writing source for any
  // symbols, then we can just dump the 32-bit payload of the SourceLocator.
  symDBPtr = symDBZip.addDatabaseEntry(getEntryName(&entryName, eEntrySourceLocator));
  if (! symDBPtr) {
    // something went wrong
    mMsgContext->CGSymtabDBOpen(symDBZip.getFileError());
    return false;
  }
  *symDBPtr << "sourcelocatorfactory";
  if (!mSourceLocatorFactory->writeDB (*symDBPtr) || !symDBZip.endDatabaseEntry ()) {
    return false;
  }

  symDBPtr = symDBZip.addDatabaseEntry(getEntryName(&entryName, eEntrySymTab));
  if (! symDBPtr)
  {
    // something went wrong
    mMsgContext->CGSymtabDBOpen(symDBZip.getFileError());
    return false;
  }
  
  if (!mTypeDictionary->save(*symDBPtr))
    return false;

  // Simpler to use reference...
  ZostreamDB& symDB = *symDBPtr;

  // prepare the symbol table. This also adds to the mBVPool
  //  IODBSelect selectIO(this, selector);

  STSymbolTable* symtab = &symtabClosure->mSymtab;
  STSymbolTable::LeafAssoc dummy_leaf_map;
  cbuildBOM->prepareForDBWrite(mDesignTable, symtab, symtabClosure->mSelectIO, &dummy_leaf_map);
  // write out the bitvector pool.
  if (! mBVPool->dbWrite(symDB))
    return false;
  

  // IODB Entry
  if (!cbuildBOM->writeData(symDB, symtab, NULL))
    return false;

  // Write the unelaborated symbol table
  if (getDBType () == eCarbonGuiDB) {
    symDB << "unelaborated";
    UnelaboratedGUISymTabSelect select;
    STSymbolTable *unelab = &dbClosure->getTmpUnelabSymtab ()->mSymtab;
    unelab->writeDB (symDB, &select, /*writeData=*/ true);
  }

  // Write out the source location for symbols written to the DB file.
  symDB << "sourcelocators";
  // Write the source locations for the elaborated symbol table.
  DBClosure::Symtab *elab_closure = dbClosure->getTmpSymtab ();
  STSymbolTable &elab = elab_closure->mSymtab;
  for (STSymbolTable::NodeLoopSorted loop = elab.getNodeLoopSorted (); !loop.atEnd (); ++loop) {
    dbClosure->writeLoc (symDB, *elab_closure, *loop);
  }
  if (getDBType () == eCarbonGuiDB) {
    // In the GUI database also write out location information for the
    // unelaborated symbol table
    DBClosure::Symtab *unelab_closure = dbClosure->getTmpUnelabSymtab ();
    STSymbolTable &unelab = unelab_closure->mSymtab;
    for (STSymbolTable::NodeLoopSorted loop = unelab.getNodeLoopSorted (); !loop.atEnd (); ++loop) {
      dbClosure->writeLoc (symDB, *unelab_closure, *loop);
    }
  }
  symDB.writeNull ();

  // Output a mapping from elaborated to unelaborated symbols
  if (getDBType () == eCarbonGuiDB) {
    symDB << "elaborationmap";
    STSymbolTable *stab = &dbClosure->getTmpSymtab ()->mSymtab;
    STSymbolTable *unelabStab = &dbClosure->getTmpUnelabSymtab ()->mSymtab;
    for (STSymbolTable::NodeLoopSorted loop = stab->getNodeLoopSorted (); !loop.atEnd (); ++loop) {
      STSymbolTableNode *elaborated = *loop;
      STSymbolTableNode *unelaborated;
      NUBase *base;
      NUElabBase *elabBase;
      NUNet *net;

      // Check if we are supressing the cannot find unelaborated node message
      MsgContextBase::Severity sev;
      mMsgContext->getMsgSeverity("STCannotFindUnelaboratedNode", &sev);
      bool printData = (sev != MsgContextBase::eSuppress);

      // hunt down the unelaborated symbol
      if ((base = CbuildSymTabBOM::getNucleusObj (elaborated)) == NULL) {
        unelaborated = NULL;
      } else if (((elabBase = dynamic_cast <NUElabBase *> (base)) != NULL)
        && elabBase->findUnelaborated (unelabStab, &unelaborated)) {
        // found the unelaborated symbol under the elaboration object
      } else if (elabBase != NULL) {
        if (printData) {
          elabBase->pr ();
        }
        unelaborated = NULL;
      } else if ((net = dynamic_cast <NUNet *> (base)) != NULL) {
        if (printData) {
          net->pr ();
        }
        unelaborated = NULL;
      } else {
        if (printData) {
          base->pr ();
        }
        unelaborated = NULL;
      }
      if (unelaborated == NULL) {
        // ignore it
      } else if (!symDB.isMapped (elaborated)) {
        unelaborated = NULL;
      } else if (!symDB.isMapped (unelaborated)) {
        unelaborated = NULL;
      }
      // write to the databse
      if (unelaborated == NULL) {
        // failed to locate the unelaborated symbol
        UtString b0;
        elaborated->compose (&b0);
        mMsgContext->STCannotFindUnelaboratedNode (b0.c_str ());
      } else {
        // write the pair of symbols to the database
        INFO_ASSERT (symDB.isMapped (elaborated) && symDB.isMapped (unelaborated),
          "error writing libdesign.gui.db");
        symDB.writePointer (elaborated);
        symDB.writePointer (unelaborated);
      }
    }
    symDB.writeNull ();
  }


  // Now write all the maps we have gathered.
  symDB << "fastClocks";
  if (!writeSet(symDB, mFastClocks, symtab))
    return false;

  symDB << "observed";
  if (!writeNameIntMap(loopObservedMap(), symDB, symtab)) {
    return false;
  }
  symDB << "deposit";
  if (!writeNameIntMap(loopDepositMap(), symDB, symtab)) {
    return false;
  }
  symDB << "force";
  if (!writeSet(symDB, mForcedNets, symtab))
    return false;
  symDB << "expose";
  if (!writeSet(symDB, mExposedNets, symtab))
    return false;
  symDB << "delayedClocks";
  if (!writeSet(symDB, mDelayedClocks, symtab))
    return false;
  
  // Print all the collapsed clocks
  symDB << "collapsedClocks";
  writeMap(symDB, mCollapsedClocks, symtab);
  
  symDB << "clockSpeeds";
  if (!writeNameIntMap(loopClockSpeeds(), symDB, symtab)) {
    return false;
  }
 
  symDB << "tieNets";
  for (NameValueLoop l = loopTieNets(); !l.atEnd(); ++l)
  {
    const STSymbolTableNode* node = l.getKey();
    if (!symDB.writePointer(symtab->safeLookup(node)))
      return false;
    const DynBitVector* value = l.getValue();
    symDB.writePointer(value);
  }
  if (!symDB.writeNull())
    return false;

  symDB << "asyncOutputs";
  writeMap(symDB, mAsyncOutputs, symtab);

  symDB << "ionets";
  if (!writeSet(symDB, mInputs, symtab) ||
      !writeSet(symDB, mOutputs, symtab) ||
      !writeSet(symDB, mBidis, symtab) ||
      !writeSet(symDB, mClocks, symtab) || 
      !writeSet(symDB, mClockTree, symtab) || 
      !writeSet(symDB, mPrimaryClocks, symtab) || 
      !writeSet(symDB, mAsyncs, symtab) ||
      !writeSet(symDB, mAsyncDeposits, symtab) ||
      !writeSet(symDB, mAsyncPosResets, symtab) ||
      !writeSet(symDB, mAsyncNegResets, symtab) ||
      !writeSet(symDB, mCModelSenseNets, symtab) ||
      !writeSet(symDB, mWrapPort2StateNets, symtab) ||
      !writeSet(symDB, mWrapPort4StateNets, symtab) ||
      !writeSet(symDB, mLiveInputDirNets, symtab) ||
      !writeSet(symDB, mLiveOutputDirNets, symtab) ||
      !writeSet(symDB, mPosedgeScheduleTriggers, symtab) ||
      !writeSet(symDB, mNegedgeScheduleTriggers, symtab) ||
      !writeSet(symDB, mScheduleTriggersUsedAsData, symtab) ||
      !writeSet(symDB, mOnDemandIdleDepositNets, symtab) ||
      !writeSet(symDB, mOnDemandExcludedNets, symtab)) {
    return false;
  }
  
  symDB << "hideModules";
  if (!writeSetSelective(symDB, mHiddenModuleTops, symtab))
    return false;

  symDB << "enableExprs";

  if (! dbClosure->writeExprSymtab(symDB))
    return false;
  

  symDB << "constNetValues";
  for (NameValueMap::UnsortedLoop l = mConstNets->loopUnsorted(); !l.atEnd(); ++l)
  {
    const STSymbolTableNode* node = l.getKey();
    const STSymbolTableNode* transNode = symtab->safeLookup(node);
    ST_ASSERT(transNode, node);

    if (!symDB.writePointer(transNode))
      return false;
    const DynBitVector* value = l.getValue();
    symDB.writePointer(value);
  }
  if (!symDB.writeNull())
    return false;

  symDB << "topLevelPorts";
  for (NameVecLoop p(mPrimaryPorts); ! p.atEnd(); ++p)
    symDB.writePointer(symtab->safeLookup(*p));
  if (! symDB.writeNull())
    return false;

  // Save the OnDemand state offsets and total state size.  For the
  // full DB, we write the actual data.  However, most of the nodes in
  // the offset map aren't part of the IODB.  In that case, write out
  // a dummy empty set.  The OnDemandDebug object will detect this at
  // runtime and warn that debug mode won't print internal names.
  NameIntMap dummy;
  NameIntMapLoop ondemand_state_loop(dummy);
  if (getDBType() == eCarbonFullDB) {
    ondemand_state_loop = loopOnDemandStateOffsets();
  }

  symDB << "onDemandStateOffsets"; 
  if (!writeNameIntMap(ondemand_state_loop, symDB, symtab)) {
    return false;
  }
  // We need to save the total size, since with only offsets we don't know
  // how large the last element is.
  if (!(symDB << mOnDemandStateSize)) {
    return false;
  }

  // Save the set of runtime excluded nodes
  symDB << "onDemandRuntimeExcludedNets";
  if (!writeSet(symDB, mOnDemandRuntimeExcludedNets, symtab)) {
    return false;
  }
 
  // Write the design directives
  if (getDBType () == eCarbonGuiDB) {
    // ... but only for the GUI database
    symDB << "designdirectives";
    symDB << mDesignDirectives.size ();
    IODBDesignDirective::List::const_iterator it0;
    for (it0 = mDesignDirectives.begin (); it0 != mDesignDirectives.end (); ++it0) {
      IODBDesignDirective *directive = *it0;
      directive->writeDB (*mSourceLocatorFactory, symDB);
    }
    symDB << "moduledirectives";
    symDB << mModuleDirectives.size ();
    IODBModuleDirective::List::const_iterator it1;
    for (it1 = mModuleDirectives.begin (); it1 != mModuleDirectives.end (); ++it1) {
      IODBModuleDirective *directive = *it1;
      directive->writeDB (*mSourceLocatorFactory, symDB);
    }
  }

  symDB << "softwareVersion";
  symDB << mSoftwareVersion;

  // Write the attributes
  symDB << "genericAttributes";
  UInt32 mapSize = mStringAttrMap.size();
  symDB << mapSize;
  for (StringAttrMap::SortedLoop l = mStringAttrMap.loopSorted(); !l.atEnd(); ++l) {
    const UtString &name = l.getKey();
    const UtString &val = l.getValue();
    symDB << name << val;
  }
  mapSize = mIntAttrMap.size();
  symDB << mapSize;
  for (IntAttrMap::SortedLoop l = mIntAttrMap.loopSorted(); !l.atEnd(); ++l) {
    const UtString &name = l.getKey();
    UInt32 val = l.getValue();
    symDB << name << val;
  }

  // Write the array of clock names for glitch detection
  symDB << "clockGlitchNames";
  UInt32 arraySize = mClockGlitchNames.size();
  symDB << arraySize;
  for (UInt32 i = 0; i < arraySize; ++i) {
    UtString* str = mClockGlitchNames[i];
    symDB << str->c_str();
  }

  symDB << "endOfDatabase";
  symDBZip.endDatabaseEntry();
  bool ret = symDBZip.finalize();
  return ret;
} // bool IODBNucleus::writeDB

bool IODBNucleus::writeSet(ZostreamDB& symDB, NameSet& ns, 
                           STSymbolTable* symtab)
{
  STSymbolTableNode* node;
  if (!(symDB << ((UInt32) ns.size())))
    return false;
  NameSet::SortedLoop loop = ns.loopSorted();
  while (loop(&node))
    if (!symDB.writePointer(symtab->safeLookup(node)))
      return false;
  return true;
}

bool IODBNucleus::writeMap(ZostreamDB& symDB, NameSetMap& nsMap, 
                           STSymbolTable* symtab)
{
  for (NameSetMapLoop p(nsMap); !p.atEnd(); ++p) {
    STSymbolTableNode* node = p.getKey();
    if (!symDB.writePointer(symtab->safeLookup(node))) {
      return false;
    }
    IODB::NameSet* ns = p.getValue();
    for (IODB::NameSet::SortedLoop q = ns->loopSorted(); !q.atEnd(); ++q) {
      if (!symDB.writePointer(symtab->safeLookup(*q))) {
        return false;
      }
    }
    if (!symDB.writeNull()) {
      return false;
    }
  }
  if (!symDB.writeNull()) {
    return false;
  }
  return true;
}

bool IODBNucleus::writeSetSelective(ZostreamDB& symDB, NameSet& ns,
                                    STSymbolTable* symtab)
{
  typedef UtVector<STSymbolTableNode*> NodeList;
  NodeList mappedNodes;

  STSymbolTableNode* node;
  NameSet::SortedLoop loop = ns.loopSorted();
  while (loop(&node))
  {
    STSymbolTableNode* transNode = symtab->safeLookup(node);
    if (transNode && symDB.isMapped(transNode))
      mappedNodes.push_back(transNode);
  }
  
  if (!symDB.writePointerContainer(mappedNodes))
    return false;

  return true;
}

bool IODBNucleus::writeFiles(bool doSymTabDB,
                             bool includeTmps, 
                             const char* fileRoot,
                             CbuildSymTabBOM* bomManager,
                             ESFactory* exprFactory,
                             bool verboseErrors,
                             bool strictIODB)
{
  bool ret = true;
  // Make sure that forcible/depositable/observable nets have shell
  // types, and do any other fixups/cleanups before writing
  preWriteCheck();
  
  // First, we need to populate a local symboltable so we can write
  // out all the hier names of the expressions efficiently.
  DBClosure dbClosure(this, mDesignTable->getHdlHier(), mBVPool, exprFactory,
    mDesignTable, bomManager, *mSourceLocatorFactory, *mMsgContext);
  dbClosure.prepEnableSymtab(this);
  
  // The full DB needs to be written before the IODB.  This is because
  // some of the common design data that is written to the DBs (node
  // sets/maps, schedule signatures) needs to be pruned before the
  // IODB is written.

  if (doSymTabDB)
  {
    // dump the symbol table DB
    CbuildSymTabBOM bom_manager;
    CSymTabSelect selector(includeTmps, bomManager, this);
    DBClosure::CbuildSymtab tmpSymtab(&bom_manager, this, &selector, mDesignTable->getHdlHier(),
      mBVPool, mUserTypeFactory, verboseErrors, strictIODB, false);

    dbClosure.filterSymtab(&tmpSymtab);
    dbClosure.putDBType(eCarbonFullDB);
    dbClosure.putTmpSymtab(&tmpSymtab);
    ret &= writeDB(fileRoot, &dbClosure);
    // The tmp symtab is about to be destroyed.
    // remove it from the closure.
    dbClosure.putTmpSymtab(NULL);
  }
  
  CbuildSymTabBOM bom_manager;
  DBClosure::CbuildSymtab tmpSymtab(&bom_manager, this, NULL, mDesignTable->getHdlHier(),
                                    mBVPool, mUserTypeFactory, verboseErrors, strictIODB, true);
  // Before writing the IODB, prune the node sets so only those that
  // should be exposed are present.
  dbClosure.pruneForIODB(&tmpSymtab);
  dbClosure.filterSymtab(&tmpSymtab);
  dbClosure.putDBType(eCarbonIODB);
  dbClosure.putTmpSymtab(&tmpSymtab);
  ret &= writeDB(fileRoot, &dbClosure);
  // Finally, validate that only the expected nodes were written.
  // Skip this if requested via command-line switch.
  if (! mArgs->getBoolValue(CRYPT("-noValidateIODB"))) {
    ret = dbClosure.validateIODB();
  }
  dbClosure.putTmpSymtab(NULL);

  return ret;
}


bool IODBNucleus::writeGuiDB (const char *fileRoot, CbuildSymTabBOM *bomManager, ESFactory *exprFactory,
  bool verboseErrors)
{
  bool ret = true;

  // Populate a local symboltable so we can write out all the hier names of the
  // expressions efficiently.
  CbuildSymTabBOM bom_manager;
  DBClosure dbClosure(this, mDesignTable->getHdlHier(), mBVPool, exprFactory,
    mDesignTable, bomManager, *mSourceLocatorFactory, *mMsgContext);
  dbClosure.prepEnableSymtab(this);
  ElaboratedGUISymTabSelect selector;
  DBClosure::CbuildSymtab tmpSymtab(&bom_manager, this, &selector, mDesignTable->getHdlHier(), 
    mBVPool, mUserTypeFactory, verboseErrors, false, false);

  dbClosure.filterSymtab(&tmpSymtab);
  dbClosure.putDBType(eCarbonGuiDB);
  dbClosure.putTmpSymtab(&tmpSymtab);
  
  // Construct an unelaborated symbol table where there is a top level branch
  // for each module and the tree under that module is isomorphic with the
  // unelaborated symbol table for the module.
  UnelaboratedGUISymTabSelect selectUnelab;
  DBClosure::UnelabSymtab tmpUnelabSymtab(mDesign->getAliasBOM (), &selectUnelab, 
    mDesignTable->getHdlHier(), verboseErrors);
  dbClosure.putTmpUnelabSymtab (&tmpUnelabSymtab);

  dbClosure.filterUnelabSymtab (mDesign, &tmpUnelabSymtab);

  // Write the database
  ret &= writeDB(fileRoot, &dbClosure);

  // The tmp symtab is about to be destroyed. Remove it from the closure.
  dbClosure.putTmpSymtab(NULL);
  dbClosure.putTmpUnelabSymtab(NULL);

  return ret;
}
