// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "schedule/Event.h"
#include "schedule/ScheduleMask.h"
#include "util/UtHashSet.h"
#include <algorithm>

bool
SCHScheduleMask::EventPtrCmp::operator()(const SCHEvent* const& ev1,
					 const SCHEvent* const& ev2) const
{
  return SCHEvent::compare(ev1, ev2) < 0;
}

SCHScheduleMask::SCHScheduleMask()
{
  mRefCnt = 0;
  mIndex = 0;
  mNumClkNets = 0;
}

SCHScheduleMask::SCHScheduleMask(const EventPtrSet& events,
                                 UInt32 numClkNets)
  : mEvents(events.size())
{
  mRefCnt = 0;
  mIndex = 0;
  mNumClkNets = numClkNets;
  size_t i = 0;
  for (EventPtrSet::SortedLoop p = events.loopSorted(); !p.atEnd(); ++p, ++i)
  {
    const SCHEvent* ev = *p;
    if (i > 0)
    {
      FUNC_ASSERT(!ev->isConstant(), ev->print());
      if (i > 1)
      {
        FUNC_ASSERT(!ev->isPrimaryInput(), ev->print());
        FUNC_ASSERT(!ev->isPrimaryOutput(), ev->print());
      }
    }
    mEvents[i] = ev;
  }
}

// Special constructor to incrementally build a new schedule mask
// from two existing ones.  This has got its own constructor because
// we can take advantage of the fact that the masks are pre-sorted,
// so we can do a merge sort.
SCHScheduleMask::SCHScheduleMask(const SCHScheduleMask* m1,
                                 const SCHScheduleMask* m2)
{
  mRefCnt = 0;
  mIndex = 0;
  mNumClkNets = 0;

  // classic freshman merge-sort
  size_t i1 = 0, i2 = 0;
  while (true) {
    const SCHEvent* e1 = (i1 < m1->mEvents.size()) ? m1->mEvents[i1] : 0;
    const SCHEvent* e2 = (i2 < m2->mEvents.size()) ? m2->mEvents[i2] : 0;
    const SCHEvent* event = e1;

    // Event is the same.  Just add it once, unless we got to the end
    if (e1 == e2) {
      if (e1 == NULL) {
        break;
      }
      ++i1;
      ++i2;
    }
    else if ((e1 == NULL) || ((e2 != NULL) && (*e2 < *e1))) {
      event = e2;
      ++i2;
    }
    else {
      ++i1;
    }

    mEvents.push_back(event);
    if (event->isClockEvent()) {
      ++mNumClkNets;
    }
  } // while
}

SCHScheduleMask::~SCHScheduleMask()
{
}

SCHScheduleMask::SCHScheduleMask(const SCHScheduleMask& src)
  : mEvents(src.mEvents)
{
  mRefCnt = 0;
  mIndex = 0;
  mNumClkNets = src.mNumClkNets;
}

SCHScheduleMask&
SCHScheduleMask::operator=(const SCHScheduleMask& src)
{
  if (&src != this)
  {
    FUNC_ASSERT(mRefCnt == 0, print());
    mEvents = src.mEvents;
    mNumClkNets = src.mNumClkNets;
    mIndex = src.mIndex;
  }
  return *this;
}

bool
SCHScheduleMask::operator==(const SCHScheduleMask& mask) const
{
  return (mEvents == mask.mEvents);
}

size_t
SCHScheduleMask::hash() const
{
  size_t hashval = 0;
  for (SortedEvents p = loopEventsSorted(); !p.atEnd(); ++p)
  {
    const SCHEvent* event = *p;
    hashval = 7*hashval + ((size_t) event);
  }

  return hashval;
}

bool SCHScheduleMask::isClockPresent(const STSymbolTableNode* clk) const
{
  bool clockPresent = false;
  for (UnsortedEvents l = loopEvents(); !l.atEnd() && !clockPresent; ++l)
  {
    const SCHEvent* ev = *l;
    if (ev->isClockEvent() && (ev->getClock() == clk))
      clockPresent = true;
  }
  return clockPresent;
}

/*
#if MASK_USE_HASH_SET
SCHScheduleMask::UnsortedEvents SCHScheduleMask::loopEvents() const
{
  UnsortedEvents loop = mEvents->loopCUnsorted();
  return loop;
}
SCHScheduleMask::SortedEvents SCHScheduleMask::loopEventsSorted() const
{
  return mEvents->loopSorted();
}
#else
SCHScheduleMask::UnsortedEvents SCHScheduleMask::loopEvents() const
{
  return UnsortedEvents(*mEvents);
}
SCHScheduleMask::SortedEvents SCHScheduleMask::loopEventsSorted() const
{
  return SortedEvents(*mEvents);
}
#endif
*/
SCHScheduleMask::UnsortedEvents SCHScheduleMask::loopEvents() const
{
  return UnsortedEvents(mEvents);
}
SCHScheduleMask::SortedEvents SCHScheduleMask::loopEventsSorted() const
{
  return SortedEvents(mEvents);
}

size_t SCHScheduleMask::numEdges() const
{
  return mEvents.size();
}

size_t SCHScheduleMask::numEventNets() const {
  return mNumClkNets;
}

int
SCHScheduleMask::compare(const SCHScheduleMask* m1, const SCHScheduleMask* m2)
{
  if (m1 == m2)
    return 0;

  // We want to iterate over masks in order of number of edges that
  // the mask is sensitive to.  This is to ensure a legal ordering
  // between combinational blocks.
  size_t n1 = m1->numEdges();
  size_t n2 = m2->numEdges();
  if (n1 < n2)
    return -1;
  else if (n2 < n1)
    return 1;

  // If the number of edges is the same, then the blocks are
  // not mutually dependent, but we want a consistent ordering
  // mainly for regression test gold-file consistency, so order
  // them alphabetically by clock name, and posedge before negedge.
//#if MASK_USE_HASH_SET
  SortedEvents p1 = m1->loopEventsSorted();
  SortedEvents p2 = m2->loopEventsSorted();
  for (; !p1.atEnd(); ++p1, ++p2)
//#else
//  EventPtrSet::const_iterator p1 = m1->mEvents->begin();
//  EventPtrSet::const_iterator p2 = m2->mEvents->begin();
//  for (; p1 != m1->mEvents->end(); ++p1, ++p2)
//#endif
  {
    int cmp = SCHEvent::compare(*p1, *p2);
    if (cmp != 0)
      return cmp;
  }
  return 0;
} // SCHScheduleMask::compare

bool SCHScheduleMask::hasEvent(const SCHEvent* event) const
{
  EventPtrCmp cmp;
  EventVector::const_iterator p =
    std::lower_bound(mEvents.begin(), mEvents.end(), event, cmp);
  return p != mEvents.end();
}

bool SCHScheduleMask::includes(const SCHScheduleMask* smallMask,
                               bool ignoreConstants)
  const
{
  int cmpSizes = (int) smallMask->mEvents.size() - (int) mEvents.size();
  if (ignoreConstants)
  {
    if (smallMask->isConstant())
      --cmpSizes;
    if (isConstant())
      ++cmpSizes;
  }
  if (cmpSizes > 0)
    return false;
  else if (cmpSizes == 0)
    return smallMask == this;

  EventVector::const_iterator first = mEvents.begin();
  EventVector::const_iterator last = mEvents.end();
  EventPtrCmp cmp;

  for (UnsortedEvents p = smallMask->loopEvents(); !p.atEnd(); ++p)
  {
    const SCHEvent* event = *p;
    if (!(ignoreConstants && event->isConstant()))
    {
      EventVector::const_iterator q = std::lower_bound(first, last, event, cmp);
      if (q == last)
        return false;
    }
  }
  return true;
} // bool SCHScheduleMask::includes

void SCHScheduleMask::clear()
{
  mEvents.clear();
  mNumClkNets = 0;
}

bool SCHScheduleMask::hasInput() const {
  // There can be at most 2 non-clock events in any mask -- input and output
  size_t sz = mEvents.size();
  if (sz > 0)
  {
    if (mEvents[0]->isPrimaryInput() ||
        ((sz > 1) && mEvents[1]->isPrimaryInput()))
      return true;
  }
  return false;
}

bool SCHScheduleMask::hasOutput() const {
  // There can be at most 2 non-clock events in any mask -- input and output
  size_t sz = mEvents.size();
  if (sz > 0)
  {
    if (mEvents[0]->isPrimaryOutput() ||
        ((sz > 1) && mEvents[1]->isPrimaryOutput()))
      return true;
  }
  return false;
}

bool SCHScheduleMask::isConstant() const {
  // There can be at most 1 constant event in any mask
  return (!mEvents.empty() && mEvents[0]->isConstant());
}
