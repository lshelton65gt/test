// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "iodb/IODBNucleus.h"
#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "util/CbuildMsgContext.h"

bool
IODBNucleus::addCModelDirective(IODBDirective::DesignDirective dir, StrToken& tok,
				const SourceLocator& loc)
{
  CModelContext& cntxt = *mCModelContext;

  // Handle the directive. If we are ignoring this
  // cModuleBegin... cModuleEnd, then don't call the helper function.
  bool ret = true;
  switch(dir)
  {
    case IODBDirective::eCModuleBegin:
      ret &= addCModule(tok, loc, cntxt);
      break;

    case IODBDirective::eCPortBegin:
      if (!cntxt.ignoreDirectives)
	ret &= addCPort(tok, loc, cntxt);
      break;

    case IODBDirective::eCNullPortBegin:
      if (!cntxt.ignoreDirectives)
	ret &= addCNullPort(loc, cntxt);
      break;

    case IODBDirective::eCTiming:
      if (!cntxt.ignoreDirectives)
	ret &= addCTiming(tok, loc, cntxt);
      break;

    case IODBDirective::eCFanin:
      if (!cntxt.ignoreDirectives)
	ret &= addCFanin(tok, loc, cntxt);
      break;

    case IODBDirective::eCPortEnd:
      if (!cntxt.ignoreDirectives)
	ret &= terminateCPort(tok, loc, cntxt, "cPortEnd");
      break;

    case IODBDirective::eCNullPortEnd:
      if (!cntxt.ignoreDirectives)
	ret &= terminateCPort(tok, loc, cntxt, "cNullPortEnd");
      break;

    case IODBDirective::eCModuleEnd:
      if (!cntxt.ignoreDirectives)
	ret &= terminateCModule(tok, loc, cntxt);
      else
	// We can re-enable parsing 
	cntxt.ignoreDirectives = false;
      break;

    case IODBDirective::eCFunctionBegin:
      ret &= addCFunction(tok, loc, cntxt);
      break;

    case IODBDirective::eCFunctionEnd:
      if (!cntxt.ignoreDirectives)
	ret &= terminateCFunction(tok, loc, cntxt);
      else
	// We can re-enable parsing
	cntxt.ignoreDirectives = false;
      break;

    case IODBDirective::eCTaskBegin:
      ret &= addCTask(tok, loc, cntxt);
      break;

    case IODBDirective::eCTaskEnd:
      if (!cntxt.ignoreDirectives)
	ret &= terminateCTask(tok, loc, cntxt);
      else
	// We can re-enable parsing
	cntxt.ignoreDirectives = false;
      break;

    case IODBDirective::eCArg:
      if (!cntxt.ignoreDirectives)
	ret &= addCArg(tok, loc, cntxt);
      break;

    case IODBDirective::eCHasSideEffect:
      if (!cntxt.ignoreDirectives)
        ret &= addCNullTaskOutput(tok, loc, cntxt);
      break;

    case IODBDirective::eCCallOnPlayback:
      if (!cntxt.ignoreDirectives)
        ret &= addCallOnPlayback(tok, loc, cntxt);
      break;

    default:
      ret = false;
      LOC_ASSERT("Unexpected c-model directive" == NULL, loc);
  } // switch

  // Check for garbage at the end of the line as long as it was parse
  // correctly.
  const char* garbage;
  if (ret && !cntxt.ignoreDirectives && tok(&garbage))
    mMsgContext->DirGarbageAtEOL(&loc, garbage);
  return ret;
} // IODBNucleus::addCModelDirective

bool IODBNucleus::getCModelName(const char* dir, StrToken& tok,
				const SourceLocator& loc, const char** modName)
{
  // Make sure we have a module/task/function name
  if (!tok(modName))
  {
    mMsgContext->DirNoName(&loc, dir);
    return false;
  }
  else
    return true;
}

IODBNucleus::CModuleData*
IODBNucleus::findCModuleData(const char* modName) const
{
  UtString srchModName(modName);
  CModules::iterator pos = mCModules->find(&srchModName);
  if (pos != mCModules->end())
    return pos->second;
  else
    return NULL;
}

bool IODBNucleus::addCModule(StrToken& tok, const SourceLocator& loc,
			     CModelContext& cntxt)
{
  // Make sure the context is empty
  if (cntxt.modelName != NULL)
  {
    mMsgContext->DirUnExpCModDir(&loc, "cModuleBegin",
				 cntxt.modelName->c_str());
    return false;
  }

  // Get the module name, make sure it is there
  const char* modName;
  if (!getCModelName("cModuleBegin", tok, loc, &modName))
    return false;

  // Make sure we haven't seen this module before. If so, ignore the
  // rest of the directives until we see an end.
  CModuleData* data = findCModuleData(modName);
  if (data != NULL)
  {
    UtString prevLoc;
    data->mLoc.compose(&prevLoc);
    mMsgContext->DirDuplicateCModName(&loc, "cModuleBegin", modName,
				      prevLoc.c_str());
    cntxt.ignoreDirectives = true;
    return false;
  }

  // We have valid data as far as IODB is concerned. Add the entry
  data = new CModuleData(loc);
  UtString* str = new UtString(modName);
  mCModules->insert(CModules::value_type(str, data));

  // Update our context
  cntxt.modelName = str;
  cntxt.modelType = eCModule;
  cntxt.moduleData = data;

  if (mPlaybackAllCModels)
    addCallOnPlayback(tok /* not used */, loc /* not used */,
                      cntxt);
  return true;
}

bool
IODBNucleus::getCPortName(StrToken& tok, const SourceLocator& loc,
			  const char* portType, const char* directive,
			  const char** portName)
{
  // Make sure we have a port name
  if (!tok(portName))
  {
    mMsgContext->DirNoPortName(&loc, portType, directive);
    return false;
  }
  else
    return true;
}

IODBNucleus::CModuleData*
IODBNucleus::getModule(const SourceLocator& loc, CModelContext& cntxt,
		       const char* directive, const char** modName)
{
  if (cntxt.moduleData == NULL)
  {
    mMsgContext->DirNoCModDir(&loc, "cModuleBegin", directive);
    return NULL;
  }
  else
  {
    if (modName != NULL)
      *modName = cntxt.modelName->c_str();
    return cntxt.moduleData;
  }
}

bool
IODBNucleus::addCPort(StrToken& tok, const SourceLocator& loc,
		      CModelContext& cntxt)
{
  // Make sure we have a valid module name
  CModuleData* data = getModule(loc, cntxt, "cPortBegin");
  if (data == NULL)
    return false;

  // Make sure we don't have a port name
  if (cntxt.portName != NULL)
  {
    mMsgContext->DirUnExpCModDir(&loc, "cPortBegin", cntxt.portName->c_str());
    return false;
  }

  // Get and set the port name
  const char* portName;
  if (!getCPortName(tok, loc, "output", "cPortBegin", &portName))
    return false;

  // Create or find the port
  createCPort(data, portName, cntxt);
  return true;
} // IODBNucleus::addCPort

bool
IODBNucleus::addCNullPort(const SourceLocator& loc, CModelContext& cntxt)
{
  // Make sure we have a valid module name
  CModuleData* data = getModule(loc, cntxt, "cNullPortBegin");
  if (data == NULL)
    return false;

  // Make sure we don't have a port name
  if (cntxt.portName != NULL)
  {
    mMsgContext->DirUnExpCModDir(&loc, "cNullPortBegin",
                                 cntxt.portName->c_str());
    return false;
  }

  // Create or find the port, use an empty string
  const char* portName = "";
  createCPort(data, portName, cntxt);
  return true;
} // IODBNucleus::addCPort

void
IODBNucleus::createCPort(CModuleData* data, const char* portName,
                         CModelContext& cntxt)
{
  // Create or find the port structure
  UtString srchPortName(portName);
  CPorts::iterator pos = data->mOutputPorts.find(&srchPortName);
  if (pos == data->mOutputPorts.end())
  {
    // Create it and insert it
    UtString* str = new UtString(portName);
    CPort* port = new CPort;
    data->mOutputPorts.insert(CPorts::value_type(str, port));

    // Update our context
    cntxt.portName = str;
    cntxt.port = port;
  }
  else
  {
    // Update the context
    cntxt.portName = pos->first;
    cntxt.port = pos->second;
  }
} // IODBNuclues::createCPort

IODBNucleus::CPort*
IODBNucleus::getPort(const SourceLocator& loc, CModelContext& cntxt,
		     const char* directive, const char** portName)
{
  if (cntxt.portName == NULL)
  {
    mMsgContext->DirNoPortDir(&loc, directive);
    return NULL;
  }
  else
  {
    if (portName != NULL)
      *portName = cntxt.portName->c_str();
    return cntxt.port;
  }
}

UtString* IODBNucleus::addInputPort(CModuleData* data, const char* clkName)
{
  // We keep unique strings for this module so make sure it doesn't exist
  UtString srchClkName(clkName);
  UtString* resClkName;
  CPortNames::iterator pos = data->mInputPorts.find(&srchClkName);
  if (pos == data->mInputPorts.end())
  {
    resClkName = new UtString(clkName);
    data->mInputPorts.insert(resClkName);
  }
  else
    resClkName = *pos;
  return resClkName;
}

bool IODBNucleus::addCTiming(StrToken& tok, const SourceLocator& loc,
			     CModelContext& cntxt)
{
  // Make sure we have a valid module and output port name
  const char* modName;
  CModuleData* data = getModule(loc, cntxt, "cTiming", &modName);
  if (data == NULL)
    return false;
  const char* portName;
  CPort* port = getPort(loc, cntxt, "cTiming", &portName);
  if (port == NULL)
    return false;

  // Get the port timing, make sure it is there
  const char* timing;
  if (!tok(&timing))
  {
    mMsgContext->DirMissingTiming(&loc, modName, portName);
    return false;
  }

  // Validate the timing
  CPortTimingType timingType = eCTInvalid;
  if (strcmp(timing, "rise") == 0)
    timingType = eCTRise;
  else if (strcmp(timing, "fall") == 0)
    timingType = eCTFall;
  else if (strcmp(timing, "async") == 0)
    timingType = eCTAsync;
  else
  {
    mMsgContext->DirInvalidTiming(&loc, modName, portName);
    return false;
  }

  // Get the clock port name for the rise/fall timing. It is null for async.
  const char* clkName = NULL;
  if (timingType != eCTAsync)
  {
    if (!getCPortName(tok, loc, "clock", "cPortTiming", &clkName))
      return false;
  }

  // Make an input port for the clock
  UtString* clk = NULL;
  if (clkName != NULL)
    clk = addInputPort(data, clkName);

  // Create the output port entry if it doesn't exist already
  CPortEntry srchPortEntry(timingType, clk, loc);
  CPort::iterator pos = port->find(&srchPortEntry);
  CPortEntry* portEntry;
  if (pos == port->end())
  {
    portEntry = new CPortEntry(timingType, clk, loc);
    port->insert(portEntry);
  }
  else
  {
    // This is a duplicate entry, warn about it but continue
    mMsgContext->DirDuplicatePortTiming(&loc, modName, portName);
    portEntry = *pos;
  }

  // Update our context. It is ok to overwrite the previous context
  cntxt.portEntry = portEntry;
  return true;
}

bool IODBNucleus::addCFanin(StrToken& tok, const SourceLocator& loc,
			    CModelContext& cntxt)
{
  // Make sure we have valid module and output port information
  const char* modName;
  CModuleData* data = getModule(loc, cntxt, "cFanin", &modName);
  if (data == NULL)
    return false;
  CPort* port = getPort(loc, cntxt, "cFanin");
  if (port == NULL)
    return false;

  // Make sure we have a port entry. If we don't then there was a
  // fanin directive without a timing directive. Assume this is an
  // async fanin statement.
  CPortEntry* portEntry = cntxt.portEntry;
  if (portEntry == NULL)
  {
    // Print a warning
    mMsgContext->DirNoTimingDir(&loc, cntxt.portName->c_str());

    // Find the async directive if there is one (can there be one?)
    CPortEntry srchPortEntry(eCTAsync, NULL, loc);
    CPort::iterator pos = port->find(&srchPortEntry);
    if (pos == port->end())
    {
      // It doesn't exist, create it
      portEntry = new CPortEntry(eCTAsync, NULL, loc);
      port->insert(portEntry);
    }
    else
      portEntry = *pos;
    cntxt.portEntry = portEntry;
  }

  // Make sure we got at least one port for async directives
  if ((portEntry->getPortTiming(NULL) == eCTAsync) && tok.atEnd())
  {
    mMsgContext->DirNoPortName(&loc, "input", "cFanin");
    return false;
  }
  else if (tok.atEnd())
    // Empty fanin
    portEntry->putFaninType(CPortEntry::eEmptyFanin);
  else
    // Assume we have listed fanin below. If we find they specified
    // '*' we will fix this below.
    portEntry->putFaninType(CPortEntry::eListedFanin);

  // In a loop get the input port names
  const char* inPortName;
  bool portsFound = false;
  while (tok(&inPortName))
  {
    if (strcmp(inPortName, "*") == 0)
      // They specified all inputs as fanin
      portEntry->putFaninType(CPortEntry::eFullFanin);
    else
    {
      UtString* inPort = addInputPort(data, inPortName);
      portEntry->addFanin(inPort);
      portsFound = true;
    }
  }

  // If they specified both full fanin and some ports, they did
  // something strange. Warn them.
  if (portsFound && (portEntry->getFaninType() == CPortEntry::eFullFanin))
    mMsgContext->DirMixedFaninUses(&loc);

  return true;
}

bool
IODBNucleus::terminateCPort(StrToken&, const SourceLocator& loc,
			    CModelContext& cntxt, const char* directive)
{
  // Make sure we have a port
  CPort* port = getPort(loc, cntxt, directive);
  if (port == NULL)
    return false;

  // An unspecified cFanin is an error condition.
  bool success = true;
  for (CPortLoop l = loopPortEntries(port); !l.atEnd(); ++l)
  {
    CPortEntry* portEntry = *l;
    if (portEntry->getFaninType() == CPortEntry::eUnspecified)
    {
      const SourceLocator& loc = portEntry->getLoc();
      mMsgContext->DirMissingFanin(&loc, directive);
      // although this is an error condition we make it appear as if
      // the user used 'cFanin *'
      portEntry->putFaninType(CPortEntry::eFullFanin);
      success = false;
    }
  }

  // NULL out the context
  cntxt.portName = NULL;
  cntxt.port = NULL;
  cntxt.portEntry = NULL;
  return success;
}

bool
IODBNucleus::terminateCModule(StrToken&, const SourceLocator& loc,
			      CModelContext& cntxt)
{
  // Make sure we have don't port
  if (cntxt.portName != NULL)
  {
    mMsgContext->DirUnExpCModDir(&loc, "cModuleEnd", "cPortBegin");
    return false;
  }

  // Make sure we do have a module
  const char* modName;
  if (getModule(loc, cntxt, "cModuleEnd", &modName) == NULL)
    return false;

  // NULL out the context
  cntxt.modelName = NULL;
  cntxt.moduleData = NULL;
  return true;
}

bool IODBNucleus::isCModel(const char* name)
{
  // Check if it is a module
  CModuleData* modData = findCModuleData(name);
  if (modData != NULL)
  {
    modData->mTouched = true;
    return true;
  }

  // Check if it is a task or function
  CTFData* tfData = findCTFData(&name[1]);
  if (tfData != NULL)
  {
    tfData->mTouched = true;
    return true;
  }

 return false;
}

IODBNucleus::CPortsLoop IODBNucleus::loopOutputPorts(const char* modName) const
{
  // Convert the data to a PortNames structure
  CModuleData* data = findCModuleData(modName);
  INFO_ASSERT(data != NULL, modName);
  return CPortsLoop(data->mOutputPorts);
}

bool IODBNucleus::hasNullPort(const char* modName) const
{
  CModuleData* data = findCModuleData(modName);
  INFO_ASSERT(data != NULL, modName);
  UtString empty;
  return data->mOutputPorts.find(&empty) != data->mOutputPorts.end();
}

IODBNucleus::CPortNamesLoop
IODBNucleus::loopInputPorts(const char* modName) const
{
  // Convert the data to a PortNames structure
  CModuleData* data = findCModuleData(modName);
  INFO_ASSERT(data != NULL, modName);
  return CPortNamesLoop(data->mInputPorts);
}

IODBNucleus::CPortLoop IODBNucleus::loopPortEntries(const CPort* port) const
{
  return CPortLoop(*port);
}

IODBNucleus::CPortNamesLoop IODBNucleus::CPortEntry::loopFanin() const
{
  return CPortNamesLoop(*mCPortNames);
}

void IODBNucleus::CPortEntry::addFanin(UtString* inPort)
{
  INFO_ASSERT(mFaninType == eListedFanin, inPort->c_str());
  mCPortNames->insert(inPort);
}

bool
IODBNucleus::addCFunction(StrToken& tok, const SourceLocator& loc,
			  CModelContext& cntxt)
{
  // Make sure the context is empty
  if (cntxt.modelName != NULL)
  {
    mMsgContext->DirUnExpCModDir(&loc, "cFunctionBegin",
				 cntxt.modelName->c_str());
    return false;
  }

  // Get the function name, make sure it is there
  const char* modName;
  if (!getCModelName("cFunctionBegin", tok, loc, &modName))
    return false;

  // If the module name starts with a $ sign skip it
  if (modName[0] == '$')
    ++modName;

  // Make sure we haven't seen this function before. If so, ignore the
  // rest of the directives until we see an end.
  CTFData* data = findCTFData(modName);
  if (data != NULL)
  {
    UtString prevLoc;
    data->mLoc.compose(&prevLoc);
    mMsgContext->DirDuplicateCModName(&loc, "cFunctionBegin", modName,
				     prevLoc.c_str());
    cntxt.ignoreDirectives = true;
    return false;
  }

  // Get the return size in bits. it must be between 1 and 32 bits
  const char* bitSizeStr;
  if (!tok(&bitSizeStr))
  {
    mMsgContext->DirNoArgSize(&loc);
    return false;
  }
  int bitSize = atoi(bitSizeStr);
  if ((bitSize < 1) || (bitSize > 32))
  {
    mMsgContext->DirInvalidSize(&loc, "cFunctionBegin result", bitSizeStr);
    return false;
  }

  // We have valid data as far as IODB is concerned. Add the entry
  data = new CTFData(loc);
  UtString* str = new UtString(modName);
  mCTFs->insert(CTFs::value_type(str, data));
  data->mFunctionResultSize = bitSize;

  // Update our context
  cntxt.modelName = str;
  cntxt.modelType = eCFunction;
  cntxt.tfData = data;

  if (mPlaybackAllCModels)
    addCallOnPlayback(tok /* not used */, loc /* not used */,
                      cntxt);

  return true;
}

bool
IODBNucleus::terminateCFunction(StrToken&, const SourceLocator& loc,
				CModelContext& cntxt)
{
  // Make sure we do have a function
  const char* modName;
  if (getTF(loc, cntxt, eCFunction, "cFunctionEnd", &modName) == NULL)
    return false;

  // NULL out the context
  cntxt.modelName = NULL;
  cntxt.tfData = NULL;
  return true;
}

bool
IODBNucleus::addCTask(StrToken& tok, const SourceLocator& loc,
		      CModelContext& cntxt)
{
  // Make sure the context is empty
  if (cntxt.modelName != NULL)
  {
    mMsgContext->DirUnExpCModDir(&loc, "cTaskBegin",
				 cntxt.modelName->c_str());
    return false;
  }

  // Get the task name, make sure it is there
  const char* modName;
  if (!getCModelName("cTaskBegin", tok, loc, &modName))
    return false;

  // If the module name starts with a $ sign skip it
  if (modName[0] == '$')
    ++modName;

  // Make sure we haven't seen this task before. If so, ignore the
  // rest of the directives until we see an end.
  CTFData* data = findCTFData(modName);
  if (data != NULL)
  {
    UtString prevLoc;
    data->mLoc.compose(&prevLoc);
    mMsgContext->DirDuplicateCModName(&loc, "cTaskBegin", modName,
				     prevLoc.c_str());
    cntxt.ignoreDirectives = true;
    return false;
  }

  // We have valid data as far as IODB is concerned. Add the entry
  data = new CTFData(loc);
  UtString* str = new UtString(modName);
  mCTFs->insert(CTFs::value_type(str, data));

  // Update our context
  cntxt.modelName = str;
  cntxt.modelType = eCTask;
  cntxt.tfData = data;

  if (mPlaybackAllCModels)
    addCallOnPlayback(tok /* not used */, loc /* not used */,
                      cntxt);
  
  return true;
}

bool
IODBNucleus::terminateCTask(StrToken&, const SourceLocator& loc,
			    CModelContext& cntxt)
{
  // Make sure we do have a task
  const char* modName;
  if (getTF(loc, cntxt, eCTask, "cTaskEnd", &modName) == NULL)
    return false;

  // NULL out the context
  cntxt.modelName = NULL;
  cntxt.tfData = NULL;
  return true;
}

bool
IODBNucleus::createCArg(CTFData* data, const char* argName, int bitSize,
                        PortDirectionT argDirection, const SourceLocator& loc)
{
  // Make sure we haven't seen this argument before and if so, create
  // the string
  UtString str(argName);
  SeenArgs::iterator pos = data->mSeenArgs.find(&str);
  if (pos == data->mSeenArgs.end())
  {
    // Insert it into the seen list
    UtString* argNameStr = new UtString(argName);
    data->mSeenArgs.insert(argNameStr);

    // Add the entry to the vector of arguments
    CTFArg* arg = new CTFArg(argDirection, bitSize, argNameStr);
    data->mArgs.push_back(arg);
  }
  else if (strcmp(argName, "") != 0)
  {
    // If it already exists, print a warning. But if it is the side
    // effect argument, then just ignore duplicates. It doesn't really
    // hurt.
    mMsgContext->DirDuplicateArg(&loc, argName);
    return false;
  }
  return true;
} // IODBNucleus::createCArg

bool
IODBNucleus::addCArg(StrToken& tok, const SourceLocator& loc,
		     CModelContext& cntxt)
{
  // Make sure we have a valid module name
  CTFData* data = getTF(loc, cntxt, eCTask, "cArg");
  if (data == NULL)
    return false;

  // Get the arg direction
  const char* directionStr;
  PortDirectionT argDirection;
  if (!tok(&directionStr))
  {
    mMsgContext->DirNoArgDirection(&loc);
    return false;
  }
  if (strcmp(directionStr, "input") == 0)
    argDirection = eInput;
  else if (strcmp(directionStr, "output") == 0)
    argDirection = eOutput;
  else
  {
    mMsgContext->DirInvalidDirection(&loc, directionStr);
    return false;
  }

  // Get the arg size
  const char* bitSizeStr;
  if (!tok(&bitSizeStr))
  {
    mMsgContext->DirNoArgSize(&loc);
    return false;
  }
  int bitSize = atoi(bitSizeStr);
  if (bitSize < 1)
  {
    mMsgContext->DirInvalidSize(&loc, "cArg", bitSizeStr);
    return false;
  }

  // Get the arg name
  const char* argName;
  if (!tok(&argName) || (strlen(argName) == 0))
  {
    mMsgContext->DirNoArgName(&loc);
    return false;
  }

  return createCArg(data, argName, bitSize, argDirection, loc);
}

bool
IODBNucleus::addCallOnPlayback(StrToken&, const SourceLocator&,
                               CModelContext& cntxt)
{
  CarbonVHMTypeFlags isApropo = CarbonVHMTypeFlags(eVHMReplay | eVHMOnDemand);
  if (getVHMTypeFlags() & isApropo)
  {
    if (cntxt.moduleData)
      cntxt.moduleData->mCallOnPlayback = true;
    else if (cntxt.tfData)
      cntxt.tfData->mCallOnPlayback = true;
  }
  return true;
}

bool
IODBNucleus::addCNullTaskOutput(StrToken&, const SourceLocator& loc,
                                CModelContext& cntxt)
{
  // Make sure we have a valid module name
  CTFData* data = getTF(loc, cntxt, eCTask, "cArg");
  if (data == NULL)
    return false;

  // Create the side effect port
  return createCArg(data, "", 1, eOutput, loc);
}

IODBNucleus::CTFData*
IODBNucleus::getTF(const SourceLocator& loc, CModelContext& cntxt,
		   CModelType type, const char* directive, const char** name)
{
  if (cntxt.tfData == NULL)
  {
    switch (type)
    {
      case eCTask:
	mMsgContext->DirNoCModDir(&loc, "cTaskBegin", directive);
	break;
      case eCFunction:
	mMsgContext->DirNoCModDir(&loc, "cFunctionBegin", directive);
	break;
      case eCModule:
        LOC_ASSERT("This should be handled in a different function" == NULL, loc);
	break;
      case eInvalid:
        LOC_ASSERT("Uninitialized c-model type" == NULL, loc);
	break;
    }
    return NULL;
  }
  else
  {
    if (name != NULL)
      *name = cntxt.modelName->c_str();
    return cntxt.tfData;
  }
}

IODBNucleus::CTFData*
IODBNucleus::findCTFData(const char* name) const
{
  UtString srchName(name);
  CTFs::iterator pos = mCTFs->find(&srchName);
  if (pos != mCTFs->end())
    return pos->second;
  else
    return NULL;
}

IODBNucleus::CTFArgsLoop
IODBNucleus::loopTFArgs(const char* name) const
{
  // Convert the data to a PortNames structure
  CTFData* data = findCTFData(name);
  INFO_ASSERT(data != NULL, name);
  return CTFArgsLoop(data->mArgs);
}

bool
IODBNucleus::CTFArg::isNullPort() const
{
  return mName->empty();
}

bool IODBNucleus::isCallOnPlayback(const char* name, bool isModule)
  const
{
  // This c-model is either a module, UDT using Carbon API, or UDT
  // using PLI.
  if (isModule) {
    // Module converted to a c-model
    CModuleData* data = findCModuleData(name);
    return data->mCallOnPlayback;

  } else {
    // UDT using the Carbon c-model API
    CTFData* data = findCTFData(name);
    return data->mCallOnPlayback;
  }
}
