// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtIOStream.h"
#include "iodb/ScheduleFactory.h"
#include "util/Loop.h"
#include "util/Zstream.h"
#include "symtab/STSymbolTable.h"
#include "util/UtIOStream.h"
#include "iodb/IODB.h"


//  Location to save the current schedule factory for passing down
//  to the compare routine.  -Dylan
static SCHScheduleFactory* ScheduleFactorySquirrel;

static const char* cSchedFactorySig = "ScheduleFactory";
static const UInt32 cSchedFactoryVersion = 0;

void SCHScheduleFactory::commonConstructor()
{
  mMasks = new SCHMaskSet;
  mEvents = new SCHEventSet;
  mSignatures = new SCHSignatureSet;
  mInputEvent = new SCHEvent(eIn);
  mInputMask = buildMask(mInputEvent);
  mOutputEvent = new SCHEvent(eOut);
  mOutputMask = buildMask(mOutputEvent);
  mConstantEvent = new SCHEvent(eConstant);
  mConstantMask = buildMask(mConstantEvent);
  mMaskComparisonsAccurate = false;
}

SCHScheduleFactory::SCHScheduleFactory(IODB* iodb) :
  mIODB(iodb)
{
  commonConstructor();
}


SCHScheduleFactory::SCHScheduleFactory()
{
  commonConstructor();
}

template<class T> void deleteContainer(T* container)
{
  container->clearPointers();
  delete container;
}

SCHScheduleFactory::~SCHScheduleFactory()
{
  deleteContainer(mSignatures);
  deleteContainer(mMasks);
  deleteContainer(mEvents);
  delete mInputEvent;
  delete mOutputEvent;
  delete mConstantEvent;
}

UInt32 SCHScheduleFactory::numSignatures() const
{
  return mSignatures->size();
}

const SCHEvent* SCHScheduleFactory::buildClockEdge(const STSymbolTableNode* clock,
                                                   ClockEdge edge,
                                                   UInt32 priority)
{
  SCHEvent* ret = NULL;
  SCHEvent ev(clock, edge, priority);
  SCHEventSet::iterator p = mEvents->find(&ev);
  if (p == mEvents->end())
  {
    ret = new SCHEvent(ev);
    mEvents->insert(ret);
  }
  else
    ret = *p;
  return ret;
}

const SCHSignature*
SCHScheduleFactory::getSignature(const SCHSignature& signature)
{
  SCHSignature* ret = const_cast<SCHSignature*>(&signature);
  SCHSignatureSet::iterator p = mSignatures->find(ret);
  if (p == mSignatures->end())
  {
    ret = new SCHSignature(signature.getTransitionMask(),
                           signature.getSampleMask());
    mSignatures->insert(ret);
  }
  else
    ret = *p;
  return ret;
}

const SCHScheduleMask* SCHScheduleFactory::buildMask()
{
  SCHScheduleMask* ret = new SCHScheduleMask(mMaskEvents, mMaskNumClkNets);
  SCHMaskSet::iterator p = mMasks->find(ret);
  if (p == mMasks->end())
  {
    // Insert this new mask
    mMasks->insert(ret);

    // Our comparison info is no longer accurate
    mMaskComparisonsAccurate = false;
  }
  else
  {
    delete ret;
    ret = *p;
  }
  return ret;
}

const SCHScheduleMask*
SCHScheduleFactory::mergeScheduleMasks(const SCHScheduleMaskSet& masks)
{
  // Use the size of the set to exclude the *constant* mask. If the
  // size is 1, then the *constant* mask is ok, otherwise we want to
  // eliminate it. *constant* in anything doesn't make sense.
  bool removeConst = masks.size() > 1;

  // Build the net's combined transition mask
  clearMaskBuilder();
  for (SCHScheduleMaskSetLoop l(masks); !l.atEnd(); ++l) {
    const SCHScheduleMask* mask = *l;
    if (!removeConst || (mask != mConstantMask)) {
      for (SCHScheduleMask::UnsortedEvents m = mask->loopEvents();
           !m.atEnd(); ++m) {
        const SCHEvent* event = *m;
        addEvent(event);
      }
    }
  }

  return buildMask();
}

const SCHSignature*
SCHScheduleFactory::mergeSignatures(const SignatureSet& sigs)
{
  // Build the net's combined Transition mask, incorporating all signatures
  SCHScheduleMaskSet masks;
  for (SignatureSet::const_iterator q = sigs.begin(), qe = sigs.end();
       q != qe; ++q) {
    const SCHSignature* sig = *q;
    const SCHScheduleMask* mask = sig->getTransitionMask();
    if (mask != NULL) {
      masks.insert(mask);
    }
  }
  const SCHScheduleMask* transitionMask = mergeScheduleMasks(masks);

  // Build the net's combined Sample mask, incorporating all signatures
  masks.clear();
  for (SignatureSet::const_iterator q = sigs.begin(), qe = sigs.end();
       q != qe; ++q) {
    const SCHSignature* sig = *q;
    const SCHScheduleMask* mask = sig->getSampleMask();
    if (mask != NULL) {
      masks.insert(mask);
    }
  }
  const SCHScheduleMask* sampleMask = mergeScheduleMasks(masks);

  return buildSignature(transitionMask, sampleMask);
} // const SCHSignature* SCHScheduleFactory::merge

bool SCHScheduleFactory::writeDatabase(ZostreamDB& db)
{
  db << cSchedFactorySig;
  db << cSchedFactoryVersion;

  // Record mapping for, but do not store, the auto-constructed elements
  db.mapPtr(mInputEvent);
  db.mapPtr(mOutputEvent);
  db.mapPtr(mConstantEvent);

  db.mapPtr(mInputMask);
  db.mapPtr(mOutputMask);
  db.mapPtr(mConstantMask);

  // We must write the signatures in sorted order so that we can
  // assign reproducible indexes for storing field values
  bool ret = ((db << "events") && db.writePointerValueContainer(*mEvents));
  ret = ret && (db << "masks");
  if (ret)
  {
    // do not write out the input, output, and constant masks
    UInt32 num = mMasks->size();
    INFO_ASSERT(num >= 3, "Invalid mask set.");
    num -=3; // remove the three basic masks

    if (!(db << num))
      ret = false;
    
    for (CLoop<SCHMaskSet> p(*mMasks); ret && !p.atEnd(); ++p)
    {
      SCHScheduleMask* mask = *p;
      if ((mask != mInputMask) && (mask != mOutputMask) && (mask != mConstantMask))
        ret = db.writeObject(*mask);
    }
  }
  
  ret = ret && ((db << "sigs") &&
                db.writePointerValueContainer(*mSignatures,
                                              mSignatures->loopSorted()));
  return ret;
}


bool SCHScheduleFactory::readDatabase(ZistreamDB& db)
{
  UtString signature;
  if (! (db >> signature))
    return false;
  
  if (signature.compare(cSchedFactorySig) != 0)
  {
    UtString buf;
    buf << "Invalid ScheduleFactory signature: " << signature;
    db.setError(buf.c_str());
    return false;
  }

  UInt32 version;
  db >> version;
  if (db.fail())
    return false;

  if (version > cSchedFactoryVersion)
  {
    UtString buf;
    buf << "Unsupported schedule factory version: " << version;
    db.setError(buf.c_str());
    return false;
  }

  // Record mapping for, but do not read, the auto-constructed elements
  db.mapPtr(mInputEvent);
  db.mapPtr(mOutputEvent);
  db.mapPtr(mConstantEvent);
  db.mapPtr(mInputMask);
  db.mapPtr(mOutputMask);
  db.mapPtr(mConstantMask);
  
  // write the objects from bottom up.  define before use.  assumes all
  // the clocks have been defined already, otherwise we'll get an assert.
  
  SCHEvent event;
  SCHScheduleMask mask;
  SCHSignature sig;

  // we could be appending to this factory so we need to read in each
  // element and only new unique ones


  bool ret = (expect(db, "events") && db.readPointerValueContainer(mEvents, event, false) &&
              expect(db, "masks") && db.readPointerValueContainer(mMasks, mask, false) &&
              expect(db, "sigs") && db.readPointerValueContainer(mSignatures, sig, false));
  return ret;
}

bool SCHScheduleFactory::expect(ZistreamDB& db, const char* token)
{
  if (!db.expect(token))
  {
    fprintf(stderr, "Invalid DB Schedule read for %s\n", token);
    return false;
  }
  return true;
}
  

const SCHSignature* SCHScheduleFactory::translateSignature(const SCHSignature* srcSignature, const STSymbolTable* symtab)
{
  // We have to run through the schedulemasks for the signature and
  // create the events which create the masks. Then, build the
  // signature.
  const SCHScheduleMask* tMask = NULL; // dst transition
  const SCHScheduleMask* sMask = NULL; // dst sample
  
  const SCHScheduleMask* srcTransMask = srcSignature->getTransitionMask();
  SCHScheduleMask::EventVector currentEvents;
  
  if (srcTransMask)
  {
    clearMaskBuilder();
    addEventsFromMask(srcTransMask, symtab);
    tMask = buildMask();
  }

  const SCHScheduleMask* srcSampleMask = srcSignature->getSampleMask();
  if (srcSampleMask)
  {
    clearMaskBuilder();
    addEventsFromMask(srcSampleMask, symtab);
    sMask = buildMask();
  }
  
  return buildSignature(tMask, sMask);
}

bool SCHEvent::dbWrite(ZostreamDB& db)
  const
{
  return (db.writePointer(mClock) &&
          (db << ((UInt32) mEdge)) &&
          (db << ((UInt32) mEventType)) &&
          (db << ((UInt32) mPriority)));
}

bool SCHEvent::dbRead(ZistreamDB& db)
{
  UInt32 edge, eventType;
  STSymbolTableNode* clkNode;
  if (db.readPointer(&clkNode) &&
      (db >> edge) &&
      (db >> eventType) &&
      (db >> mPriority))
  {
    mClock = clkNode;
    mEdge = static_cast<ClockEdge>(edge);
    mEventType = static_cast<EventType>(eventType);
    return true;
  }
  return false;
}

bool SCHScheduleMask::dbWrite(ZostreamDB& db)
  const
{
  bool ret = (db << UInt32(mEvents.size())) && (db << mNumClkNets);
  for (UnsortedEvents p = loopEvents(); ret && !p.atEnd(); ++p)
    ret = db.writePointer(*p);
  return ret;
}

bool SCHScheduleMask::dbRead(ZistreamDB& db)
{
  UInt32 numEvents;
  bool ret = (db >> numEvents) && (db >> mNumClkNets);
  if (ret)
    mEvents.resize(numEvents);
  for (size_t i = 0; ret && (i < numEvents); ++i)
  {
    SCHEvent* ev = NULL;
    ret = db.readPointer(&ev);
    mEvents[i] = ev;
  }
  return ret;
}

void SCHScheduleFactory::addEvent(const SCHEvent* ev)
{
  size_t old_size = mMaskEvents.size();
  mMaskEvents.insert(ev);
  if ((old_size != mMaskEvents.size()) && ev->isClockEvent())
    ++mMaskNumClkNets;
}

void SCHScheduleFactory::addEventsFromMask(const SCHScheduleMask* srcMask, const STSymbolTable* symTab)
{
  for (SCHScheduleMask::UnsortedEvents p = srcMask->loopEvents(); !p.atEnd(); ++p)
  {
    const SCHEvent* srcEvent = *p;
    if (srcEvent->isPrimaryInput())
      addEvent(mInputEvent);
    else if (srcEvent->isPrimaryOutput())
      addEvent(mOutputEvent);
    else if (srcEvent->isConstant())
      addEvent(mConstantEvent);
    else if (srcEvent->isClockEvent())
    {
      const STSymbolTableNode* srcClock = srcEvent->getClock();
      const STSymbolTableNode* dstClock = symTab->safeLookup(srcClock);
      ST_ASSERT(dstClock, srcClock);
      addEvent(buildClockEdge(dstClock, srcEvent->getClockEdge(), srcEvent->getPriority()));
    }
    else
      FUNC_ASSERT(0, srcEvent->print());
  }
}

bool SCHSignature::dbWrite(ZostreamDB& db)
  const
{
  return (db.writePointer(mTransition) &&
          db.writePointer(mSample));
}

bool SCHSignature::dbRead(ZistreamDB& db)
{
  SCHScheduleMask* transMask;
  SCHScheduleMask* sampleMask;
  bool ret = (db.readPointer(&transMask) &&
              db.readPointer(&sampleMask));

  if (ret)
  {
    mTransition = transMask;
    mSample = sampleMask;
  }

  return ret;
}

void SCHScheduleFactory::printScheduleMaskOrder(UtOStream& out)
{
  // Make sure the sorted order is up to date
  sortMasks();
  INFO_ASSERT(mMaskComparisonsAccurate, "Schedule Factory not sorted.");

  // Put the masks in an ordered multi set
  PrintMaskSet printMaskSet;
  for (SCHMaskSet::iterator p = mMasks->begin(); p != mMasks->end(); ++p)
  {
    const SCHScheduleMask* mask = *p;
    printMaskSet.insert(mask);
  }

  // Print the masks
  out << " RefCnt               Mask\n"
      << "-------- ----------------------------\n";
  UtString buf;
  for (PrintMaskSet::SortedLoop pos = printMaskSet.loopSorted(); ! pos.atEnd(); ++pos)
  {
    const SCHScheduleMask* mask = *pos;
    if (mask->getRefCnt() > 0)
    {
      buf.clear();
      mask->compose(&buf, NULL, true); // include root in name
      out << UtIO::setw(8) << mask->getRefCnt()
	  << " " << buf << "\n";
    }
  }
} // void printScheduleMaskOrder

void SCHScheduleFactory::clearMaskBuilder()
{
  mMaskNumClkNets = 0;
  mMaskEvents.clear();
}

const SCHScheduleMask* SCHScheduleFactory::buildMask(const SCHEvent* ev)
{
#ifdef CDB
  if ((ev != mInputEvent) &&
      (ev != mOutputEvent) &&
      (ev != mConstantEvent))
  {
    FUNC_ASSERT(mEvents->find(const_cast<SCHEvent*>(ev)) != mEvents->end(), ev->print());
  }
#endif
  clearMaskBuilder();
  addEvent(ev);
  return buildMask();
}


const SCHScheduleMask*
SCHScheduleFactory::appendMasks(const SCHScheduleMask* mask1,
				const SCHScheduleMask* mask2)
{
  // If one of the input masks is NULL, we can bypass the more complex
  // code and just return the other mask.
  if (mask1 == NULL) {
    return mask2;
  } else if (mask2 == NULL) {
    return mask1;
  }

  // Also, if one of the masks is the constant mask, then just return
  // the other mask. This is because *constant* an anything else is
  // meaningless.
  //
  // Note that we can't return a NULL mask so this is only valid if
  // the above test happens first.
  if (mask1 == mConstantMask) {
    return mask2;
  } else if (mask2 == mConstantMask) {
    return mask1;
  }

  SCHScheduleMask* ret = new SCHScheduleMask(mask1, mask2);
  SCHMaskSet::iterator p = mMasks->find(ret);
  if (p == mMasks->end())
  {
    // Insert this new mask
    mMasks->insert(ret);

    // Our comparison info is no longer accurate
    mMaskComparisonsAccurate = false;
  }
  else
  {
    delete ret;
    ret = *p;
  }
  return ret;
} // SCHScheduleFactory::appendMasks


const SCHScheduleMask*
SCHScheduleFactory::removeInputMask(const SCHScheduleMask* mask)
{
  // Go through the event list and copy over the non input events
  clearMaskBuilder();
  for (SCHScheduleMask::UnsortedEvents l = mask->loopEvents();
       !l.atEnd();
       ++l)
  {
    const SCHEvent* event = *l;
    if (!event->isPrimaryInput())
      addEvent(event);
  }
  if (mMaskEvents.empty())
    return NULL;
  return buildMask();
}


const SCHScheduleMask*
SCHScheduleFactory::removeOutputMask(const SCHScheduleMask* mask)
{
  // Go through the event list and copy over the non output events
  clearMaskBuilder();
  for (SCHScheduleMask::UnsortedEvents l = mask->loopEvents();
       !l.atEnd();
       ++l)
  {
    const SCHEvent* event = *l;
    if (!event->isPrimaryOutput())
      addEvent(event);
  }
  if (mMaskEvents.empty())
    return NULL;
  return buildMask();
}


const SCHSignature*
SCHScheduleFactory::buildSignature(const SCHScheduleMask* transitionMask)
{
  SCHSignature signature(transitionMask);
  return getSignature(signature);
}


const SCHSignature*
SCHScheduleFactory::buildSignature(const SCHScheduleMask* transitionMask,
				   const SCHScheduleMask* sampleMask)
{
  if (sampleMask == NULL)
    return buildSignature(transitionMask);
  else
  {
    SCHSignature signature(transitionMask, sampleMask);
    return getSignature(signature);
  }
}


const SCHSignature*
SCHScheduleFactory::mergeSignatures(const SCHSignature* sig1,
				    const SCHSignature* sig2)
{
  const SCHScheduleMask* transition =
    appendMasks(sig1->getTransitionMask(), sig2->getTransitionMask());
  const SCHScheduleMask* sample =
    appendMasks(sig1->getSampleMask(), sig2->getSampleMask());
  return buildSignature(transition, sample);
}


bool SCHScheduleFactory::hasInputMask(const SCHSignature* signature)
{
  const SCHScheduleMask* transitionMask = signature->getTransitionMask();
  return transitionMask->hasInput();
}


bool SCHScheduleFactory::hasOutputMask(const SCHSignature* signature)
{
  const SCHScheduleMask* sampleMask = signature->getSampleMask();
  return sampleMask->hasOutput();
}


int SCHScheduleFactory::compareSignatureMasks(const SCHSignature* signature)
{
  // Check if the database is up to date, if not make update it
  sortMasks();
  FUNC_ASSERT(mMaskComparisonsAccurate, signature->print());
  
  // Compare the indicies
  UInt32 transitionIndex = signature->getTransitionMask()->getIndex();
  UInt32 sampleIndex = signature->getSampleMask()->getIndex();
  if (transitionIndex < sampleIndex)
    return -1;
  else if (transitionIndex > sampleIndex)
    return 1;
  else
    return 0;
}


bool SCHScheduleFactory::CmpPrintOrder::lessThan(const SCHScheduleMask* m1,
                                                 const SCHScheduleMask* m2) const
{
  UInt32 index1 = m1->getIndex();
  UInt32 index2 = m2->getIndex();
  if (index1 == index2)
    // Use a canonical order
    return (SCHScheduleMask::compare(m1, m2) < 0);
  else
    return (index1 < index2);
}

SCHScheduleFactory::SignatureLoop SCHScheduleFactory::loopSignatures()
{
  return mSignatures->loopSorted();
}

// Return the frequency of the fastest clock event in the given mask.
// May return 0 if there is no clock, or just clocks  without frequency
// specified.  -Dylan
UInt32 SCHScheduleFactory::fastestClock(const SCHScheduleMask* m) const
{
  UInt32 fastestSpeed = 0;
  
  for(SCHScheduleMask::UnsortedEvents p1 = m->loopEvents(); !p1.atEnd(); ++p1)
    {
      const SCHEvent* event = *p1;
      if( event->isClockEvent() )
	{
	  // mIODB may not be available at runtime.  If not, no need to get the
	  // fastest clock.  Fastest clock is only important at compile time
	  // scheduling.  -Dylan
	  if(mIODB)
	    {
              const STSymbolTableNode* clkName = event->getClock();
              UInt32 speed = mIODB->getClockSpeed( clkName );
	      
              if( speed > fastestSpeed )
                fastestSpeed = speed;                
	    }
	}
    }
  return fastestSpeed;
}

bool
SCHScheduleFactory::CmpMasksFreq::lessThan(const SCHScheduleMask* m1,
					   const SCHScheduleMask* m2) const
{
  // Check if they are the same mask
  if (m1 == m2)
    return false;

  // Use the mask with the fastest clock as the first level of
  // sorting. -Dylan
  int c1 = ScheduleFactorySquirrel->fastestClock(m1);
  int c2 = ScheduleFactorySquirrel->fastestClock(m2);

  if(c1 < c2)
    return true;
  else if(c1 > c2)
    return false;

  // Check the number of clocks
  int numEventNets1 = m1->numEventNets();
  int numEventNets2 = m2->numEventNets();

  if (numEventNets1 < numEventNets2)
    return true;
  else if (numEventNets1 > numEventNets2)
    return false;

  // Compare the number of edges
  int numEdges1 = m1->numEdges();
  int numEdges2 = m2->numEdges();

  if (numEdges1 < numEdges2)
    return true;
  else if (numEdges1 > numEdges2)
    return false;

  // If we get here, we don't know how to compare them yet. Just
  // us their ordering
  return (SCHScheduleMask::compare(m1, m2) < 0);
} // bool SCHScheduleFactory::CmpMaskFreq::lessThan

void SCHScheduleFactory::sortMasks()
{
  // Make sure we aren't up to date already
  if (mMaskComparisonsAccurate)
    return;

  // We need the max index so that we can put the most frequent
  // masks at the end. These are special cases. There are three
  // special cases so we add 3 to the size just to be safe. This is
  // not always necessary but it doesn't hurt to have holes in the
  // indicies.
  UInt32 maxIndex = mMasks->size() + 3;

  //  Global to pass down to the OrderedMask compare routine
  ScheduleFactorySquirrel = this;

  // A place for non input, constant, or output masks
  OrderedMasks masks;

  // Go through the list of masks and assign them a frequency index.
  for (SCHMaskSet::iterator pos = mMasks->begin(); pos != mMasks->end(); ++pos)
  {
    const SCHScheduleMask* mask = *pos;

    // Check if it has *output* in it. If so, we sort this as the
    // most frequent so that we always run it with the other
    // mask. We do this because *output* can only exist in sample
    // masks.
    if (mask->hasOutput())
      mask->putIndex(maxIndex);

    // Check for *input* in it. If so, we sort this as the second
    // most frequent because we want it to be the other mask, unless
    // the other mask has *output* in it. We assume that a mask with
    // *input* in it can only be a transition mask.
    else if (mask->hasInput())
      mask->putIndex(maxIndex-1);

    // Check for a simple *constant* mask. This is the least
    // frequent and we schedule with this mask. It must be a
    // transition mask.
    else if (mask == mConstantMask)
      mask->putIndex(0);

    // Otherwise, we insert this into an ordered set of masks
    else
      masks.insert(mask);
  } // for

    // Go through the list of ordered masks and assign them indicies
  UInt32 index = 0;
  for (OrderedMasks::SortedLoop pos = masks.loopSorted(); !pos.atEnd();
       ++pos)
  {
    const SCHScheduleMask* mask = *pos;
    mask->putIndex(++index);
  }
  mMaskComparisonsAccurate = true;
} // void SCHScheduleFactory::sortMasks
