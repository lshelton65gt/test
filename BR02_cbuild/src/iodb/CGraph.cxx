// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "iodb/CGraph.h"
#include "symtab/STBranchNode.h"
#include "symtab/STFieldBOM.h"
#include "symtab/STSymbolTable.h"
#include "util/AtomicCache.h"
#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtIndent.h"
#include "util/Zstream.h"
#include "util/UtCompareTester.h"

class CGraphDummyFieldBom : public STFieldBOM {
public:
  virtual ~CGraphDummyFieldBom() {}
  virtual void writeBOMSignature(ZostreamDB&) const {}
  virtual ReadStatus readBOMSignature(ZistreamDB&, UtString* errmsg) {
    *errmsg << "NYI";
    return eReadFileError;
  }

  //! Allocate an BOMData to be placed in the symbol table branch.
  virtual Data allocBranchData() {
    return (Data) this;
  }

  //! Allocate an BOMData to be placed in the symbol table leaf.
  virtual Data allocLeafData() {
    return (Data) this;
  }

  virtual void freeBranchData(const STBranchNode*, Data*) {
  }
  
  virtual void freeLeafData(const STAliasedLeafNode*, Data*) {
  }

  virtual void preFieldWrite(ZostreamDB& ) {
  }

  //! Write the BOMData for a leaf node
  virtual void writeLeafData(const STAliasedLeafNode*, ZostreamDB&) const {
  }
  
  //! Write the BOMData for a branch node
  virtual void writeBranchData(const STBranchNode*, ZostreamDB&,
                               AtomicCache* ) const {
  }

  //! Pre-field read
  /*!
    This is only called \e once. This is called before reading in any
    of the fields.
  */
  virtual ReadStatus preFieldRead(ZistreamDB&) {
    return eReadFileError;
  }
  
  //! Read the BOMData for a leaf node
  virtual ReadStatus readLeafData(STAliasedLeafNode*, ZistreamDB&, 
                                  MsgContext*)
  {
    return eReadFileError;
  }
  
  //! Read the BOMData for a branch node
  virtual ReadStatus readBranchData(STBranchNode*, ZistreamDB&, 
                                    MsgContext*)
  {
    return eReadFileError;
  }
  
  //! Print the contents of the branch structure to stdout
  /*!
    Prints the memory pointers of the bomdata structure on a branch to
    stdout. Unless the data needs to wrap for structural reasons keep the
    printed structures on 1 line.
  */
  virtual void printBranch(const STBranchNode*) const {
  }

  //! Print the contents of the leaf structure to stdout
  /*!
    Prints the memory pointers of the bomdata structure on a branch to
    stdout. Unless the data needs to wrap for structural reasons keep the
    printed structures on 1 line.
  */
  virtual void printLeaf(const STAliasedLeafNode*) const {
  }

 //! Return BOM class name
  virtual const char* getClassName() const
  {
    return "CGraphDummyFieldBom";
  }

  //! Write the BOMData for a branch node
  virtual void xmlWriteBranchData(const STBranchNode* /*branch*/,UtXmlWriter* /*writer*/) const
  {
  }

  //! Write the BOMData for a leaf node
  virtual void xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/,UtXmlWriter* /*writer*/) const
  {
  }


}; // class CGraphDummyFieldBom : public STFieldBOM

CGraph::CGraph(SourceLocatorFactory* slc, AtomicCache* shared_cache)
  : mSourceLocatorFactory(slc),
    mSharedAtomicCache(shared_cache)
{
  // make a private atomic cache for the 'description' strings and
  // labels for sub-nodes, which I think are likely to be highly shared.
  // These should not become permanently resident in cbuild's memory,
  // so use a distinct atomic-cache rather than sharing the one in the
  // symbol-table.
  mPrivateAtomicCache = new AtomicCache;
  mCGraphDummyFieldBom = new CGraphDummyFieldBom;
  mSymbolTable = new STSymbolTable(mCGraphDummyFieldBom, shared_cache);
}

CGraph::~CGraph() {
  for (UInt32 i = 0; i < mAllClusters.size(); ++i) {
    Cluster* cluster = mAllClusters[i];
    delete cluster;
  }
  for (UInt32 i = 0; i < mAllNodes.size(); ++i) {
    Node* node = mAllNodes[i];
    delete node;
  }
  delete mSymbolTable;
  delete mCGraphDummyFieldBom;
  delete mPrivateAtomicCache;
}

bool CGraph::write(const char* filename, UtString* errmsg) {
  ZOSTREAMDB(zfile, filename);

  // First write all strings
  UtString buf;
  for (AtomicCache::CacheIter p(mPrivateAtomicCache->getCacheIter());
       !p.atEnd(); ++p)
  {
    buf = p.getKey();
    zfile << 'A' << buf;
    StringAtom* sym = p.getValue();
    zfile.mapPtr(sym);
  }

  // Next write all the nodes.  For the nodes that have sub-nodes
  // write their sub-nodes first -- that's a strict hierarchical
  // relationship.  The fanin gets written after all the nodes are
  // in the DB.
  for (UInt32 i = 0; i < mAllNodes.size(); ++i) {
    Node* node = mAllNodes[i];
    node->write(zfile);
    zfile.mapPtr(node);
  }

  // Now write fanin
  for (UInt32 i = 0; i < mAllNodes.size(); ++i) {
    Node* node = mAllNodes[i];
    node->writeRefs(zfile);
  }

  zfile << 'Z';                 // EOF record

  if (!zfile.close()) {
    *errmsg = zfile.getErrmsg();
    return false;
  }
  return true;
}

CGraph::NodeSetLoop CGraph::findDrivers(const STSymbolTableNode* node) {
  // the incoming node may be from the iodb symbol table, but cgraph
  // has its own symbol table, for now, because the iodb one is incomplete
#if !USE_CARBON_DB
  node = mSymbolTable->safeLookup(node);
#endif

  return mPathNodeSetMap[node].loopSorted();
}

CGraph::Node::Node(Type type, 
                   const STSymbolTableNode* outnet,
                   StringAtom* netBits, 
                   const STSymbolTableNode* scope,
                   StringAtom* desc)
  : mType(type),
    mScope(scope),
    mDescription(desc),
    mContinuousDriver(true),
    mIndex(-1)
{
  addOutNet(outnet, netBits);
  INFO_ASSERT(outnet, "Node created with null outnet");
}

CGraph::Node::~Node() {
  for (UInt32 i = 0; i < mNodes.size(); ++i) {
    SymNodesPair* sym_nodes = mNodes[i];
    CARBON_DELETE(sym_nodes, SymNodesPair);
  }

  // delete out all the source line information for this node
  for (UInt32 i = 0; i < mSource.size(); ++i) {
    FileAndLines* fl = mSource[i];
    CARBON_DELETE(fl, FileAndLines);
  }
}

void CGraph::Node::write(ZostreamDB& db) {
  // Note that the Description is part of an AtomicCache that is owned
  // by CGraph, and is written out in CGraph::write, and whose memory
  // is managed by CGraph.  However, the locator filenames and the
  // names in STSymbolTableNodes are all part of the 'global' atomic
  // cache, which persists beyond CGraph.  So we use writeString for
  // the names in the shared atomic cache, but we write the namees that
  // we own as pointers, as those will already have been mapped by
  // CGraph::write before this routine is called.
  
  // write out all the source line information for this node
  for (UInt32 i = 0; i < mSource.size(); ++i) {
    FileAndLines* fl = mSource[i];
    writeString(db, fl->first);
  }
  
  // write out all the hierarchy & strings for the nets & bits def'd by this node
  for (NetAndBitsSet::iterator p = mOutNets.begin(), e = mOutNets.end();
       p != e; ++p)
  {
    NetAndBits& nb = *p;
    const STSymbolTableNode* net = nb.first;
    writeHierarchy(db, net);
  }
  writeHierarchy(db, mScope);

  db << 'N' << (UInt32) mType;
  UInt32 nfiles = mSource.size();
  db << nfiles;

  // write out all source-line-ranges for this node
  for (UInt32 i = 0; i < mSource.size(); ++i) {
    FileAndLines* fl = mSource[i];
    const char* file = fl->first;
    IntVec& lines = fl->second;
    UInt32 nlines = lines.size();
    db.writePointer(file);
    db << nlines;
    for (UInt32 j = 0; j < nlines; ++j) {
      db << lines[j];
    }
  }

  // write out all def'd nets & bits for this node
  UInt32 n_outnets = mOutNets.size();
  db << n_outnets;
  for (NetAndBitsSet::iterator p = mOutNets.begin(), e = mOutNets.end();
       p != e; ++p)
  {
    NetAndBits& nb = *p;
    const STSymbolTableNode* net = nb.first;
    StringAtom* bits = nb.second;
    db.writePointer(net);
    db.writePointer(bits);
  }
  db.writePointer(mScope);
  db.writePointer(mDescription);

  // don't write fanin or sub-nodes until all nodes have been written
  // to disk & mapped.
}

void CGraph::Node::writeRefs(ZostreamDB& db) {
  db << 'R';
  db.writePointer(this);
  UInt32 sz = mFanin.size();
  db << sz;
  for (NodeSetLoop p(loopFanin()); !p.atEnd(); ++p) {
    Node* fanin = *p;
    db.writePointer(fanin);
  }

  sz = mNodes.size();
  db << sz;
  for (UInt32 i = 0; i < sz; ++i) {
    SymNodesPair* sym_nodes = mNodes[i];
    db.writePointer(sym_nodes->first);
    UInt32 n = sym_nodes->second.size();
    db << n;
    for (UInt32 j = 0; j < n; ++j) {
      db.writePointer(sym_nodes->second[j]);
    }
  }
}

void CGraph::Node::writeString(ZostreamDB& db, const char* str) {
  if (! db.isMapped(str)) {
    db << 'S' << str;
    db.mapPtr(str);
  }
}

void CGraph::Node::writeHierarchy(ZostreamDB& db,
                                  const STSymbolTableNode* node)
{
  if (! db.isMapped(node)) {
    const STBranchNode* parent = node->getParent();
    if (parent != NULL) {
      writeHierarchy(db, parent);
    }
    const char* str = node->strObject()->str();
    writeString(db, str);
    db << 'P';
    db.writePointer(parent);
    db.writePointer(str);
    db.mapPtr(node);
  }
}

bool CGraph::Node::read(ZistreamDB& db, SourceLocatorFactory* /*slc*/) {
  UInt32 typeInt = 0;
  const char* filename = NULL;
  UInt32 nfiles = 0;
  if ((db >> typeInt) && (db >> nfiles)) {

    // read all the source/line info for this node
    for (UInt32 i = 0; i < nfiles; ++i) {
      UInt32 nlines;
      if (db.readPointer(&filename) && (db >> nlines)) {
        FileAndLines* fl = CARBON_NEW(FileAndLines);
        mSource.push_back(fl);
        fl->first = filename;
        IntVec& lines = fl->second;
        for (UInt32 j = 0; j < nlines; ++j) {
          UInt32 line;
          if (db >> line) {
            lines.push_back(line);
          }
          else {
            return false;
          }
        }
      }
      else {
        return false;
      }
    }
      
    // read all the def-net and bits info for this node
    UInt32 n_nets;
    if (db >> n_nets) {
      for (UInt32 i = 0; i < n_nets; ++i) {
        const STSymbolTableNode* net;
        StringAtom* bits;
        if (db.readPointer(&net) && db.readPointer(&bits)) {
          addOutNet(net, bits);
        }
        else {
          return false;
        }
      }
    }
    if (db.readPointer(&mScope) && db.readPointer(&mDescription)) {
      mType = (Type) typeInt;
      return true;
    }
  } // if
  return false;
} // bool CGraph::Node::read

bool CGraph::Node::readRefs(ZistreamDB& db) {
  UInt32 num_fanin, num_subnodes;
  if (! (db >> num_fanin)) {
    return false;
  }
  for (UInt32 i = 0; i < num_fanin; ++i) {
    Node* fanin;
    if (! db.readPointer(&fanin)) {
      return false;
    }
    addFanin(fanin);
  }

  if (!(db >> num_subnodes)) {
    return false;
  }
  for (UInt32 i = 0; i < num_subnodes; ++i) {
    StringAtom* sym;
    Node* subnode;
    UInt32 n;
    if (! db.readPointer(&sym) ||
        ! (db >> n))
    {
      return false;
    }
    for (UInt32 j = 0; j < n; ++j) {
      if (! db.readPointer(&subnode)) {
        return false;
      }
      addNode(sym, subnode);
    }
  }
  return true;
} // bool CGraph::Node::readRefs

void CGraph::Node::addOutNet(const STSymbolTableNode* net, StringAtom* bits)
{
  NetAndBits net_and_bits(net, bits);
  mOutNets.insert(net_and_bits);
}

void CGraph::Node::addLocator(const SourceLocator& loc) {
  const char* filename = loc.getFile();
  UInt32 line = loc.getLine();
  UInt32 nfiles = mSource.size();

  // If the filename matches the last one entered, then we will
  // keep pushing lines onto that file record
  if ((nfiles == 0) || (mSource[nfiles - 1]->first != filename)) {
    FileAndLines* fl = CARBON_NEW(FileAndLines);
    fl->first = filename;
    mSource.push_back(fl);
    ++nfiles;
  }

  IntVec& lines = mSource[nfiles - 1]->second;
  UInt32 nlines = lines.size();

  // If this line does not represent a continuation of the previous
  // line, then make a new range.
  bool new_range = true;
  if (nlines != 0) {
    UInt32 prev_end = lines[nlines - 1];

    // if we are repeating the same line, then do nothing
    if (prev_end == line) {
      new_range = false;
    }

    // if we are exetending the range, then extend it
    else if (prev_end == line - 1) {
      lines[nlines - 1] = line;
      new_range = false;
    }
  }
  if (new_range) {
    lines.push_back(line);      // start
    lines.push_back(line);      // end
  }
} // void CGraph::Node::addLocator

CGraph::Node* CGraph::addNode(Type type, 
                              const STSymbolTableNode* outnet,
                              StringAtom* netBits,
                              const STSymbolTableNode* hier,
                              StringAtom* desc)
{
  Node* node = new Node(type, outnet, netBits, hier, desc);

  // Establish an initial index based on order of creation.  This should
  // be stable since CGraphFlow.cxx uses the sorted Fanin iterator rather
  // than the unsoerted FLNodeElab::loopFanin() method.  Of course if the
  // FLNodeElabs are not in stable order then there's nothing that can
  // be done.

  // Note that once we sort all the nodes we will renumber them.  But
  // that initial sort needs this index to resolve ties for unrolled
  // loops where the same verilog line can get replicated multiple times.
  node->putIndex(mAllNodes.size());

  mAllNodes.push_back(node);
  return node;
} // CGraph::Node* CGraph::addNode

//! Add a fanin node.
/*!
 *! This fanin is only for the expressions that determine whether
 *! or not the sub-nodes are live.
 */
void CGraph::Node::addFanin(Node* node) {
  mFanin.insert(node);
  node->mFanout.insert(this);
}

void CGraph::Node::addSeqFanin(Cluster* cluster)
{
  mSeqFanin.insert(cluster);
}

void CGraph::Node::addSeqFanout(Cluster* cluster)
{
  mSeqFanout.insert(cluster);
}


void CGraph::Node::addNode(StringAtom* sym, Node* node) {
  SymNodesPair* snp = NULL;
  SymIndexMap::iterator p = mSymIndexMap.find(sym);
  if (p == mSymIndexMap.end()) {
    snp = CARBON_NEW(SymNodesPair);
    snp->first = sym;
    mSymIndexMap[sym] = mNodes.size();
    mNodes.push_back(snp);
  }
  else {
    snp = mNodes[p->second];
  }
  snp->second.push_back(node);
  node->mContinuousDriver = false;
  node->mFanout.insert(this);
}

bool CGraph::getNodeDescription(Node* node, UtString* buf) const {
  *buf = node->mDescription->str();
  return true;
} // bool CGraph::getDescription

StringAtom* CGraph::intern(const char* str) {
  return mPrivateAtomicCache->intern(str);
}

void CGraph::buildNetNodeMap() {
  for (UInt32 i = 0; i < mAllNodes.size(); ++i) {
    Node* node = mAllNodes[i];
    if (node->isContinuousDriver()) {
      for (NetAndBitsLoop p(node->loopNetAndBits()); !p.atEnd(); ++p) {
        const NetAndBits& nb = *p;
        mPathNodeSetMap[nb.first].insert(node);
      }
    }
  }
}

void CGraph::Node::print(CGraph* cgraph, UInt32 indent, bool recurse) {  
  UtOStream& out = UtIO::cout();
  UtString buf, net_buf, scope_buf;
  UtIndent indenter(&buf);
  indenter.tab(indent);
  mScope->verilogCompose(&scope_buf);
  if (mIndex == -1) {
    buf << "CGraph::Node " << this;
  }
  else {
    buf << "CGraph::Node " << mIndex;
  }
  buf << (mContinuousDriver ? " continuous " : " non-contin ") <<
    getTypeStr() << "\n";

  for (NetAndBitsLoop p(loopNetAndBits()); !p.atEnd(); ++p) {
    const NetAndBits& nb = *p;
    net_buf.clear();
    nb.first->verilogCompose(&net_buf);
    buf << " Net: " << net_buf << " " << nb.second->str() << "\n";
  }
  buf << " Scope: " << scope_buf << "\n";
  out << buf; buf.clear();

  if (recurse) {
    indenter.tab(indent + 2);
    (void) cgraph->getNodeDescription(this, &buf);
    UInt32 buf_size = buf.size();
    if ((buf_size > 0) && (buf[buf_size - 1] == '\n')) {
      // remove trailing newline
      buf.resize(buf_size - 1);
    }
    out << buf << '\n'; buf.clear();    

    if (! mNodes.empty()) {
      for (UInt32 i = 0; i < mNodes.size(); ++i) {
        indenter.tab(indent + 2);
        const NodeVec& v = getSubNodes(i);
        buf << "Subnode " << getSym(i)->str() << ": (" << v.size() << ")\n";
        out << buf; buf.clear();
        for (UInt32 j = 0; j < v.size(); ++j) {
          v[j]->print(cgraph, indent + 4, false);
        }
      }
    }
    if (! mFanin.empty()) {
      indenter.tab(indent + 2);
      buf << "Fanin: (" << mFanin.size() << ")\n";
      out << buf; buf.clear();
      for (NodeSetLoop p(loopFanin()); !p.atEnd(); ++p) {
        Node* fanin = *p;
        fanin->print(cgraph, indent + 4, false);
      }
    }
    if (! mFanout.empty()) {
      indenter.tab(indent + 2);
      buf << "Fanout: (" << mFanout.size() << ")\n";
      out << buf; buf.clear();
      for (NodeSetLoop p(loopFanout()); !p.atEnd(); ++p) {
        Node* fanout = *p;
        fanout->print(cgraph, indent + 4, false);
      }
    }
    if (! mSeqFanin.empty()) {
      indenter.tab(indent + 2);
      buf << "SeqFanin: (" << mSeqFanin.size() << ")\n";
      out << buf; buf.clear();
      for (ClusterSetLoop p(loopSeqFanin()); !p.atEnd(); ++p) {
        Cluster* seqFanin = *p;
        seqFanin->print(cgraph, indent + 4, false);
      }
    }

    if (! mSeqFanout.empty()) {
      indenter.tab(indent + 2);
      buf << "SeqFanout: (" << mSeqFanout.size() << ")\n";
      out << buf; buf.clear();
      for (ClusterSetLoop p(loopSeqFanout()); !p.atEnd(); ++p) {
        Cluster* seqFanout = *p;
        seqFanout->print(cgraph, indent + 4, false);
      }
    }

    if (! mParents.empty()) {
      indenter.tab(indent + 2);
      buf << "Parents: (" << mParents.size() << ")\n";
      out << buf; buf.clear();
      for (ClusterSetLoop p(loopParents()); !p.atEnd(); ++p) {
        Cluster* parent = *p;
        parent->print(cgraph, indent + 4, false);
      }
    }
  } // if
} // void CGraph::Node::print

const char* CGraph::Node::getTypeStr() const {
  switch (mType) {
  case eSequential:  return "sequential";
  case eIf:          return "if";
  case eCase:        return "case";
  case eComplexCase: return "complex_case";
  case eBlackBox:    return "black_box";
  case eUnbound:     return "unbound";
  case eInput:       return "input";
  case eOutput:      return "output";
  case eBidirect:    return "bidirect";
  case eForce:       return "force";
  case eDeposit:     return "deposit";
  case eObserve:     return "observe";
  }
  return "?";
}

// Create Sequential edges between two nodes: sequential or input node to sequential or output node 
void CGraph::buildSequentialFanin() {
  for (UInt32 i = 0; i < mAllNodes.size(); ++i) {
    Node* node = mAllNodes[i];
    if ((node->getType() == eSequential)
#define CLUSTER_IO 0
#if CLUSTER_IO
        || (node->getType() == eOutput)
#endif
      )
    {
      // create the fanins starting from this currentNode
      // first lookup the equivalent currentCNode from currentSNode
      
      buildSequentialFaninStart(node, node);
    }
  }
}

// helper function for buildSequentialFanin
void CGraph::buildSequentialFaninStart(Node* currentNode, Node* currentSNode) {
  NodeSet covered;
  NodeVec path;

  // recurse through the fanin
  if (currentNode->numFanin() != 0) {
    for (NodeSetLoop p(currentNode->loopFanin()); !p.atEnd(); ++p) {
      Node* faninNode = *p;
      buildSequentialFaninRecurse(faninNode, currentSNode, &covered, &path);
    }
  }
  
  // recurse through the subnodes to get to eventually recurse through their fanins.
  for (UInt32 i = 0; i < currentNode->numSubNodes(); ++i) {
    NodeVec& v = currentNode->getSubNodes_nc(i);
    for (UInt32 j = 0; j < v.size(); ++j) {
      buildSequentialFaninRecurse (v[j], currentSNode, &covered, &path);
    }
  }
}


// helper function for buildSequentialFanin
void CGraph::buildSequentialFaninRecurse(Node* currentNode, Node* seqDestNode,
                                         NodeSet *covered, NodeVec* path) 
{

  // Avoid infinite recursion on cycles, although we will remove this
  // node from the covered set before returning from this routine so
  // that we trace all possible paths in order to fill the cluster.
  // otherwise, test/cgraph/reconverge.v only gets one of the continuous
  // assigns put into the cluster
  if (!covered->insertWithCheck(currentNode)) {
    return;
  }

  // look up if current node is a stopping point: if so create edge between
  // it and seqDestNode but it will only be in the stop set if it is continuous
  if (currentNode->isContinuousDriver() &&
      ((currentNode->getType() == eSequential)
#if CLUSTER_IO
       || (currentNode->getType() == eInput)
#endif
        ))
  {
    Cluster* cluster = new Cluster(currentNode, seqDestNode);
    ClusterFactory::iterator p = mClusterFactory.find(cluster);
    if (p == mClusterFactory.end()) {
      mClusterFactory.insert(cluster);
      mAllClusters.push_back(cluster);
    }
    else {
      delete cluster;
      cluster = *p;
    }
    for (UInt32 i = 0; i < path->size(); ++i) {
      Node* node = (*path)[i];
      cluster->addChild(node);
      node->addParent(cluster);
    }
    seqDestNode->addSeqFanin(cluster);
    currentNode->addSeqFanout(cluster);
  }
      
  else {
    path->push_back(currentNode);

    // Keep looking
    for (NodeSetLoop p(currentNode->loopFanin()); !p.atEnd(); ++p) {
      Node* faninNode = *p;
      buildSequentialFaninRecurse(faninNode, seqDestNode, covered, path);
    }

    // if the node is non-continuous, then traverse the subnodes to get
    // their fanins.
    for (UInt32 i = 0; i < currentNode->numSubNodes(); ++i) {
      NodeVec& v = currentNode->getSubNodes_nc(i);
      for (UInt32 j = 0; j < v.size(); ++j) {
        buildSequentialFaninRecurse(v[j], seqDestNode, covered, path);
      }
    }

    path->pop_back();
  }
  covered->erase(currentNode);
} // void CGraph::buildSequentialFaninRecurse


void CGraph::Node::addParent(Cluster* cluster) {
  mParents.insert(cluster);
}


CGraph::Cluster::Cluster(Node* fanin, Node* fanout)
  : mFanin(fanin),
    mFanout(fanout),
    mIndex(-1)
{
}

void CGraph::Cluster::addChild(Node* node) {
  mChildren.insert(node);
}

// For each child, we need to know:
//   1. how many hops does it take to get to the fanin?
//   2. how many hops does it take to get to the fanout?
void CGraph::Cluster::computeDepthFromFanin(Node* node, UInt32 depth) {
  for (NodeSetLoop p(node->loopFanin()); !p.atEnd(); ++p) {
    Node* fanin = *p;
    if (mChildren.find(fanin) != mChildren.end()) {
      IntPair& in_out_depth = mDepthMap[fanin];

      if ((in_out_depth.first == 0) || (in_out_depth.first > depth)) {
        in_out_depth.first = depth;

        // recompute fanin's depth if this depth changed
        computeDepthFromFanin(fanin, depth + 1);
      }
    }
  }
}

void CGraph::Cluster::computeDepthFromFanout(Node* node, UInt32 depth) {
  for (NodeSetLoop p(node->loopFanout()); !p.atEnd(); ++p) {
    Node* fanout = *p;
    if (mChildren.find(fanout) != mChildren.end()) {
      IntPair& in_out_depth = mDepthMap[fanout];

      if ((in_out_depth.second == 0) || (in_out_depth.second > depth)) {
        in_out_depth.second = depth;

        // recompute fanin's depth if this depth changed
        computeDepthFromFanout(fanout, depth + 1);
      }
    }
  }
}

void CGraph::Cluster::computeDepths() {
  computeDepthFromFanin(mFanout, 1);
  computeDepthFromFanout(mFanin, 1);
  INFO_ASSERT(mChildren.size() == mDepthMap.size(), "cluster depth insanity");
  for (DepthMap::iterator p = mDepthMap.begin(), e = mDepthMap.end();
       p != e; ++p)
  {
    IntPair& in_out_depth = p->second;
    INFO_ASSERT(in_out_depth.first != 0, "did not compute fanin depth");
    INFO_ASSERT(in_out_depth.second != 0, "did not compute fanout depth");
  }
}

void CGraph::Cluster::print(CGraph* cgraph, UInt32 indent, bool recurse) {  
  UtOStream& out = UtIO::cout();
  UtString buf, net_buf, scope_buf;
  UtIndent indenter(&buf);
  indenter.tab(indent);
  if (mIndex == -1) {
    buf << "CGraph::Cluster " << this << "\n";
  }
  else {
    buf << "CGraph::Cluster " << mIndex << "\n";
  }
  out << buf;
  buf.clear();
  if (recurse) {
    indenter.tab(indent + 2); buf << "Fanin:\n";  out << buf; buf.clear();
    mFanin->print(cgraph, indent + 4, false);
    indenter.tab(indent + 2); buf << "Fanout:\n"; out << buf; buf.clear();
    mFanout->print(cgraph, indent + 4, false);

    indenter.tab(indent + 2);
    buf << "Children: (" << mChildren.size() << ")\n";
    out << buf; buf.clear();
    for (NodeSetLoop p(mChildren); !p.atEnd(); ++p) {
      Node* child = *p;
      child->print(cgraph, indent + 4, false);
    }
  }
} // void CGraph::Cluster::print

void CGraph::computeClusterDepths() {
  for (UInt32 i = 0; i < mAllClusters.size(); ++i) {
    Cluster* cluster = mAllClusters[i];
    cluster->computeDepths();
  }
}

void CGraph::dump() {
  // Loop over all the nodes and print them
  for (UInt32 i = 0; i < mAllNodes.size(); ++i) {
    Node* node = mAllNodes[i];
    node->print(this, 0, true);
  }

  // Loop over all the clusters and print them
  for (UInt32 i = 0; i < mAllClusters.size(); ++i) {
    Cluster* cluster = mAllClusters[i];
    cluster->print(this, 0, true);
  }
}

static bool sCompareNodes(CGraph::Node* n1, CGraph::Node* n2) {
  return CGraph::Node::compare(n1, n2) < 0;
}

static bool sCompareClusters(CGraph::Cluster* c1, CGraph::Cluster* c2) {
  return CGraph::Cluster::compare(c1, c2) < 0;
}

int CGraph::Node::compare(const Node* n1, const Node* n2) {
  if (n1 == n2) {
    return 0;
  }

  // First sort by out-net
  const NetAndBits& nb1 = *n1->loopNetAndBits();
  const NetAndBits& nb2 = *n2->loopNetAndBits();

  int cmp = HierName::compare(nb1.first, nb2.first);
  if (cmp == 0)
  {
    cmp = HierName::compare(n1->mScope, n2->mScope);
    if (cmp == 0) {
      //cmp = SourceLocator::compare(n1->mLoc, n2->mLoc);
      if (cmp == 0) {
        cmp = (int) n1->mType - (int) n2->mType;
        if (cmp == 0) {
          cmp = strcmp(nb1.second->str(), nb2.second->str());
          if (cmp == 0) {
            cmp = strcmp(n1->mDescription->str(), n2->mDescription->str());
            if (cmp == 0) {
              cmp = n1->mIndex - n2->mIndex;
              INFO_ASSERT(cmp != 0, "compare node insanity");
            }
          }
        }
      }
    }
  }
  return cmp;
}

int CGraph::Cluster::compare(const Cluster* c1, const Cluster* c2) {
  if (c1 == c2) {
    return 0;
  }

  // First sort by out-net
  int cmp = c1->mFanout->getIndex() - c2->mFanout->getIndex();
  if (cmp == 0) {
    cmp = c1->mFanin->getIndex() - c2->mFanin->getIndex();
    INFO_ASSERT(cmp != 0, "compare cluster insanity");
  }
  return cmp;
}

void CGraph::sort() {
  // this order n^3 test should only be run if you really want it:
  // UtTestCompareFunction(mAllNodes.begin(), mAllNodes.end(), CGraph::Node::compare);

  std::sort(mAllNodes.begin(), mAllNodes.end(), sCompareNodes);

  // Now that the nodes are sorted, renumber them
  for (UInt32 i = 0; i < mAllNodes.size(); ++i) {
    mAllNodes[i]->putIndex(i);
  }

  UtTestCompareFunction(mAllClusters.begin(), mAllClusters.end(), CGraph::Cluster::compare);
  std::sort(mAllClusters.begin(), mAllClusters.end(), sCompareClusters);
  
  // Now that the nodes are sorted, renumber them
  for (UInt32 i = 0; i < mAllClusters.size(); ++i) {
    mAllClusters[i]->putIndex(i);
  }
}
