// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "iodb/IODBDesignData.h"
#include "iodb/IODBNucleus.h"
#include "iodb/IODBUserTypes.h"
#include "util/UtStackPOD.h"
#include "util/UtIOStream.h"
#include "util/Zstream.h"
#include "util/AtomicCache.h"
#include "util/UtStringArray.h"
#include "util/CbuildMsgContext.h"
#include "hdl/HdlVerilogPath.h"
#include "symtab/STSymbolTable.h"
#include "nucleus/NUNet.h"
#include "util/UtXmlWriter.h"

static const char* scIODBDesignDataBOMSig = "IODBDesignData_BOM";
static const UInt32 scIODBDesignDataBOMVersion = 1; 

IODBDesignNodeData* IODBDesignDataBOM::castBOM(STSymbolTableNode* node) {
  IODBDesignNodeData* ret = static_cast<IODBDesignNodeData*>(node->getBOMData());
  return ret;
}

const IODBDesignNodeData* IODBDesignDataBOM::castBOM(const STSymbolTableNode* node) {
  const void* bomdata = node->getBOMData();
  const IODBDesignNodeData* ret = static_cast<const IODBDesignNodeData*>(bomdata);
  return ret;
}

void IODBDesignDataBOM::writeBOMSignature(ZostreamDB& out) const
{
  out << scIODBDesignDataBOMSig << scIODBDesignDataBOMVersion;
}

STFieldBOM::ReadStatus IODBDesignDataBOM::readBOMSignature(ZistreamDB& in, UtString* errMsg)
{
  UtString tmpSig;
  in >> tmpSig;
  if (in.fail())
    return eReadFileError;
  
  UtString expSig(scIODBDesignDataBOMSig);
  expSig << scIODBDesignDataBOMVersion;
  ReadStatus stat = eReadOK;    
  if (tmpSig.compare(expSig) != 0)
  {
    *errMsg << "Signature mismatch - expected '" << expSig << "', got '" << tmpSig << "'";
    stat = eReadIncompatible;
  }
  return stat;
}
    
STFieldBOM::Data IODBDesignDataBOM::allocBranchData()
{
  return new IODBDesignNodeData();
}
    
STFieldBOM::Data IODBDesignDataBOM::allocLeafData()
{
  return new IODBDesignNodeData();
}

void IODBDesignDataBOM::freeBranchData(const STBranchNode*, Data* bomdata)
{
  if (bomdata)
  {
    IODBDesignNodeData* brData = static_cast<IODBDesignNodeData*>(*bomdata);
    delete brData;
  }
}
    
void IODBDesignDataBOM::freeLeafData(const STAliasedLeafNode*, Data* bomdata)
{
  if (bomdata)
  {
    IODBDesignNodeData* lfData = static_cast<IODBDesignNodeData*>(*bomdata);
    delete lfData;
  }
}

STFieldBOM::ReadStatus IODBDesignDataBOM::preFieldRead(ZistreamDB&) {
  return eReadOK;
}
    
STFieldBOM::ReadStatus IODBDesignDataBOM::readLeafData(STAliasedLeafNode*, ZistreamDB&, MsgContext*) {
  return eReadIncompatible;
}
    
STFieldBOM::ReadStatus IODBDesignDataBOM::readBranchData(STBranchNode*, ZistreamDB&, MsgContext*) {
  return eReadIncompatible;
}
    
void IODBDesignDataBOM::printBranch(const STBranchNode* /*branch*/) const
{
  // TBD
}
    
void IODBDesignDataBOM::printLeaf(const STAliasedLeafNode* /*leaf*/) const
{
  // TBD
}


//! Return BOM class name
const char* IODBDesignDataBOM::getClassName() const
{
  return "IODBDesignDataBOM";
}



IODBDesignDataSymTabs::IODBDesignDataSymTabs(AtomicCache* cache)
  : mAtomicCache(cache),
    mUnelabST(NULL),
    mElabST(NULL)
{
  mUnelabST = new STSymbolTable(&mUnelabBOM, cache);
  mElabST = new STSymbolTable(&mElabBOM, cache);
}

IODBDesignDataSymTabs::~IODBDesignDataSymTabs()
{
  delete mUnelabST;
  delete mElabST;
}

STSymbolTableNode* 
IODBDesignDataSymTabs::createNode(STBranchNode* parent, StringAtom* child,
                                  SymTabT stTyp, SymTabNodeT nodeTyp)
{
  STSymbolTable* symtab = mUnelabST;
  if (stTyp == eElab) {
    symtab = mElabST;
  }
  if (nodeTyp == eLeaf) {
    return symtab->createLeaf(child, parent);
  }
  return symtab->createBranch(child, parent);
}

STSymbolTableNode* IODBDesignDataSymTabs::findNode(STBranchNode* parent,
                                                   StringAtom* child,
                                                   SymTabT stTyp)
{
  STSymbolTable* symtab = mUnelabST;
  if (stTyp == eElab) {
    symtab = mElabST;
  }
  return symtab->find(parent, child);
}

bool IODBDesignDataSymTabs::markProtMutable(const UtStringArray& elemList)
{
  return markProtMutable(elemList, 0, NULL);
}

bool IODBDesignDataSymTabs::markProtMutable(const UtStringArray& elemList,
                                            SInt32 elemListIndex,
                                            STBranchNode* parent)
{
  bool success = true;
  const UtString& buf = elemList[elemListIndex];
  StringAtom* sym = mAtomicCache->intern(buf.c_str(), buf.size());
  STSymbolTableNode* node = mElabST->find(parent, sym);
  UtList<STSymbolTableNode*> matchingChildren;
  if (node != NULL) {
    matchingChildren.push_back(node);
  } else {
    // No such elaborated node. This could be a wildcarded node.
    // Find matching wildcard children.
    IODBNucleus::sGetWildcardMatchingChildren(parent, mElabST, buf.c_str(),
                                              &matchingChildren);
  }

  for (UtList<STSymbolTableNode*>::iterator itr = matchingChildren.begin();
       itr != matchingChildren.end(); ++itr)
  {
    STAliasedLeafNode* leaf = (*itr)->castLeaf();
    if (leaf == NULL) {
      // Not a leaf node. Protected mutables are always leafs. Continue traversing
      // down the hierarchy.
      STBranchNode* branch = (*itr)->castBranch();
      success = markProtMutable(elemList, elemListIndex+1, branch);
    } else {
      // Mark leaf node as protected mutable.
      IODBDesignNodeData* leafData = IODBDesignDataBOM::castBOM(leaf);
      leafData->setProtectedMutable();
      // Get the unelaborated node and mark it protected mutable too.
      IODBDesignNodeData* parentData = IODBDesignDataBOM::castBOM(parent);
      const STBranchNode* unelabParent = parentData->getUnelabScope();
      STSymbolTableNode* unelabNode = mUnelabST->find(unelabParent, leaf->strObject());
      if (unelabNode == NULL) {
        success = false;
      } else {
        STAliasedLeafNode* unelabLeaf = unelabNode->castLeaf();
        if (unelabLeaf == NULL) {
          success = false;
        } else {
          IODBDesignNodeData* unelabLeafData = 
            IODBDesignDataBOM::castBOM(unelabLeaf);
          unelabLeafData->setProtectedMutable();
        }
      }
    }
  }
  return success;
}

bool IODBDesignDataSymTabs::isUnelabProtMutable(NUNet* net) const
{
  bool isProtMutable = false;
  UtList<StringAtom*> names;
  net->composeUnelaboratedName(&names);
  // The names list will be empty for names in protected source code. Such
  // names cannot have directives on them since the names are not displayable.
  const STSymbolTableNode* node = NULL;
  const STBranchNode* parent = NULL;
  for (UtList<StringAtom*>::iterator itr = names.begin(); itr != names.end(); ++itr)
  {
    node = mUnelabST->find(parent, *itr);
    if (node == NULL) {
      break; // Failed to find the unelaborated scope/leaf name.
    }
    parent = node->castBranch();
  }

  if (node != NULL) {
    const STAliasedLeafNode* unelabLeaf = node->castLeaf();
    if (unelabLeaf != NULL) {
      const IODBDesignNodeData* unelabLeafData = 
        IODBDesignDataBOM::castBOM(unelabLeaf);
      isProtMutable = unelabLeafData->isProtectedMutable();
    }
  }

  return isProtMutable;
}

void IODBNucleus::setDesignDataSymbolTables(IODBDesignDataSymTabs* symtabs)
{
  INFO_ASSERT(mDDSymTabs == NULL, "Multiple design data symbol tables specified.");
  mDDSymTabs = symtabs;
}

IODBDesignDataSymTabs* IODBNucleus::getDesignDataSymbolTables()
{
  return mDDSymTabs;
}

//! Populate elaborated and unelaborated symbol tables with design directives.
bool IODBNucleus::populateDesignDirectives()
{
  // Protected mutable directives set on nets local to sub-programs are not
  // supported. Since IODBDesignDataSymTabs do not contain nodes for these nets.
  // The directives for them are therefore automatically ignored. The warnings
  // for these will be issued later at the evaluateDesignDirectives compiler phase.
  bool success = true;
  for (IODBDesignDirective::List::iterator itr = mDesignDirectives.begin();
       itr != mDesignDirectives.end(); ++itr)
  {
    IODBDesignDirective* dir = *itr;
    switch (dir->getDirective ())
    {
    case IODBDirective::eForce:
    case IODBDirective::eDeposit:
    case IODBDirective::eDepositFrequent:
    case IODBDirective::eDepositInfrequent:
    case IODBDirective::eScDeposit:
    case IODBDirective::eMvDeposit:
    case IODBDirective::eDelayedClock:
      // Mark the symbol table node as protected mutable.
    {
      for (IODBDirective::TokenVectorLoop p(dir->loopTokens ()); !p.atEnd(); ++p) {
        UtStringArray *strings = *p;
        success &= mDDSymTabs->markProtMutable(*strings);
      }
      break;
    }
    default:
      // Do nothing for now.
      break;
    }
  }
  return success;
}

//! Write the BOMData for a branch node
void IODBDesignDataBOM::xmlWriteBranchData(const STBranchNode* branch,UtXmlWriter* xmlWriter) const
{
  const STSymbolTableNode* node = (const STSymbolTableNode*)branch;
  const IODBDesignNodeData* designData = IODBDesignDataBOM::castBOM(node);
  if(designData == NULL) {
    return;
  }
  designData->writeXml(xmlWriter);
}

//! Write the BOMData for a leaf node
void IODBDesignDataBOM::xmlWriteLeafData(const STAliasedLeafNode* leaf,UtXmlWriter* xmlWriter) const
{
  const STSymbolTableNode* node = (const STSymbolTableNode*)leaf;
  const IODBDesignNodeData* designData = IODBDesignDataBOM::castBOM(node);
  if(designData == NULL) {
    return;
  }
  designData->writeXml(xmlWriter);
}


void IODBDesignNodeData::writeXml(UtXmlWriter* xmlWriter) const 
{

      xmlWriter->WriteAttribute("Type", "IODBDesignNodeData");

      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",     "mUnelabScope");
      xmlWriter->WriteAttribute("Type",     "STBranchNode*");
      xmlWriter->WriteAttribute("Pointer",  (const void*) mUnelabScope);
      if (mUnelabScope != NULL ) {
        UtString buf;
        mUnelabScope->compose(&buf);
        xmlWriter->WriteAttribute("Value",buf.c_str());
      }
      xmlWriter->EndElement();

      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",  "mIsProtectedMutable");
      xmlWriter->WriteAttribute("Type",  "bool");
      xmlWriter->WriteAttribute("Value", mIsProtectedMutable);
      xmlWriter->EndElement();


      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",     "mType");
      xmlWriter->WriteAttribute("Type",     "UserType*");
      xmlWriter->WriteAttribute("Pointer",  (const void*) mType);
      xmlWriter->EndElement();
      return;

}
