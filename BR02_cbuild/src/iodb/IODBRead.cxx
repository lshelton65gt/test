// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "iodb/IODBRuntime.h"
#include "iodb/IODBRuntimeAliasBOM.h"
#include "iodb/IODBDirective.h"
#include "util/ShellMsgContext.h"
#include "util/ZstreamZip.h"
#include "shell/CarbonDBRead.h"
#include "iodb/IODBTypes.h"
#include "iodb/IODBUserTypes.h"
#include "exprsynth/ExprFactory.h"
#include "shell/ShellSymNodeIdent.h"
#include "util/UtConv.h"
#include "util/StringAtom.h"

typedef bool (IODBRuntime::*IODBFlagTest)(const STSymbolTableNode*) const;

class IODBRuntime::ExprFlagChecker : public CarbonExprWalker
{
public:

  ExprFlagChecker(const IODBRuntime* db, const STAliasedLeafNode* srcLeaf,
                  IODBFlagTest flagTest)
    : mDB(db), mSrcLeaf(srcLeaf), mIsFlagSet(false), mFlagTest(flagTest)
  {}
  
  virtual ~ExprFlagChecker() {}

  virtual bool preVisitIdent(CarbonIdent* ident) {
    DynBitVector useMask;
    const STAliasedLeafNode* srcLeaf = ident->getNode(&useMask);
    // Make sure we are not in an infinite loop
    ST_ASSERT(srcLeaf != mSrcLeaf, srcLeaf);
    mIsFlagSet = (mDB->*mFlagTest)(srcLeaf);
    return !mIsFlagSet; // if any node is flag, we're done
  }
  
  bool isFlagSet() const { return mIsFlagSet; }
  
private:
  const IODBRuntime* mDB;
  const STAliasedLeafNode* mSrcLeaf;
  bool mIsFlagSet;
  IODBFlagTest mFlagTest;
};

static void sStringifyDBType(UtString* buf, CarbonDBType dbType)
{
  switch(dbType) {
  case eCarbonFullDB:
    (*buf) << "Full DB";
    break;
  case eCarbonIODB:
    (*buf) << "IO DB";
    break;
  case eCarbonGuiDB:
    (*buf) << "GUI DB";
    break;
  default:
    (*buf) << "Unknown type(" << dbType << ") - DB Corrupted?";
    break;
  }
}

bool IODBRuntime::readDB(const char* filename, 
                         CarbonDBType whichDB)
{
  ZISTREAMZIP(symDBZip, filename);
  return readDBHelper(filename, symDBZip, whichDB);
}

bool IODBRuntime::readDBFromBuffer(const char* buf, size_t sz,
                                   CarbonDBType whichDB)
{
  // open the zstream and write the symbol table
  ZISTREAMZIP_STRING(symDBZip, buf, sz);
  return readDBHelper("<buffer>", symDBZip, whichDB);
}

bool IODBRuntime::readDBHelper(const char* filename,
                               ZistreamZip& symDBZip,
                               CarbonDBType whichDB)
{

  if (!symDBZip)
  {
    mMsgContext->SHLDBFileOpenFail(filename, symDBZip.getFileError());
    return false;
  }

  // Signature and version entry
  UtString entryName;
  ZistreamEntry* symDBEntry = symDBZip.getEntry(getEntryName(&entryName, eEntryVersion));
  if (! symDBEntry)
  {
    mMsgContext->SHLDBFileOpenFail(filename, symDBZip.getFileError());
    return false;
  }
  ZistreamDB* symDBPtr = symDBEntry->castZistreamDB();
  INFO_ASSERT(symDBPtr, "Invalid database type");

  {
    UtString expectedSig;
    UInt32 thisVersion = sGetSigAndVersion(&expectedSig);

    UtString signature;
    
    (*symDBPtr) >> signature;
    (*symDBPtr) >> mInputDBVersion;
    
    if (signature.compare(expectedSig) != 0)
    {
      mMsgContext->SHLDBInvalidSignature(signature.c_str());
      return false;
    }
    
    if ((mInputDBVersion < 2) || (mInputDBVersion > thisVersion)) {
      mMsgContext->SHLDBUnsupportedVersion(filename, mInputDBVersion, thisVersion);
      return false;
    }
  }
  
  mCapabilities->clear();
  symDBEntry = symDBZip.getEntry(getEntryName(&entryName, eEntryCapabilities));
  if (symDBEntry) {
    ZistreamDB* capabilitiesDBPtr = symDBEntry->castZistreamDB();
    INFO_ASSERT(capabilitiesDBPtr, "Invalid database type");
    
    // It's OK for this field to be empty.  This just means that
    // we are reading pre-Dec-28-2005 iodb and it doesn't have
    // the capabilities set yet.  That's OK, we just assume it has
    // no extra capabilities.
    UInt32 numCapabilities = 0;
    if (!(*capabilitiesDBPtr >> numCapabilities)) {
      return false;
    }
    for (UInt32 i = 0; i < numCapabilities; ++i) {
      UtString capability;
      if (!(*capabilitiesDBPtr >> capability)) {
        return false;
      }
      mCapabilities->insert(capability);
    }         
  }

  // The sourcelocations capability indicates that source location information
  // may be stored in the database. For v1, only GUI databases contained source
  // locations. For v2, all databases are written with source locations.
  bool expectSourceLocations = testCapability ("sourcelocations.v2")
    || (whichDB == eCarbonGuiDB && testCapability ("sourcelocations.v1"));

  // DesignInfo entry
  symDBEntry = symDBZip.getEntry(getEntryName(&entryName, eEntryDesignInfo));
  if (! symDBEntry)
  {
    mMsgContext->SHLDBFileOpenFail(filename, symDBZip.getFileError());
    return false;
  }
  symDBPtr = symDBEntry->castZistreamDB();
  INFO_ASSERT(symDBPtr, "Invalid database type");
  
  if (! mDesignInfo->dbRead(*symDBPtr)) {
    mMsgContext->SHLCorruptDBRead(symDBZip.getFileError());
    return false;
  }

  if (mDesignInfo->getDBType() != whichDB)
  {
    UtString requested;
    UtString actual;
    sStringifyDBType(&requested, whichDB);
    sStringifyDBType(&actual, mDesignInfo->getDBType());
    mMsgContext->SHLDBTypeMismatch(requested.c_str(), actual.c_str());
    return false;
  }
  SourceLocatorFactory tmpSourceLocatorFactory;
  if (!expectSourceLocations) {
    // no source locations
  } else if (mSourceLocatorFactory == NULL) {
    // The database contains source locations but the application has not
    // provided a factory. So, just use SourceLocatorFactory::ignoreFactoryInDB to skip
    // over all the crap.
    symDBEntry = symDBZip.getEntry(getEntryName(&entryName, eEntrySourceLocator));
    if (! symDBEntry) {
      mMsgContext->SHLDBFileOpenFail(filename, symDBZip.getFileError());
      return false;
    }
    symDBPtr = symDBEntry->castZistreamDB();
    INFO_ASSERT (symDBPtr->expect ("sourcelocatorfactory"), "Corrupt db.");
    if (!SourceLocatorFactory::ignoreFactoryInDB (*symDBPtr)) {
      return false;
    }
  } else {
    // Read the source locator factory
    symDBEntry = symDBZip.getEntry(getEntryName(&entryName, eEntrySourceLocator));
    if (! symDBEntry) {
      mMsgContext->SHLDBFileOpenFail(filename, symDBZip.getFileError());
      return false;
    }
    symDBPtr = symDBEntry->castZistreamDB();
    INFO_ASSERT (symDBPtr->expect ("sourcelocatorfactory"), "Corrupt db.");
    if (!tmpSourceLocatorFactory.readDB (*symDBPtr)) {
      return false;
    }
  }

  // UtCustomerDB entry
  symDBEntry = symDBZip.getEntry(getEntryName(&entryName, eEntryCustDB));
  if (! symDBEntry)
  {
    mMsgContext->SHLDBFileOpenFail(filename, symDBZip.getFileError());
    return false;
  }
  symDBPtr = symDBEntry->castZistreamDB();
  INFO_ASSERT(symDBPtr, "Invalid database type");
  if (! mCustomerRuntimeDB.dbRead(*symDBPtr))
  {
    mMsgContext->SHLCorruptDBRead(symDBZip.getFileError());
    return false;
  }

  symDBEntry = symDBZip.getEntry(getEntryName(&entryName, eEntrySymTab));
  if (! symDBEntry)
  {
    mMsgContext->SHLDBFileOpenFail(filename, symDBZip.getFileError());
    return false;
  }
  
  symDBPtr = symDBEntry->castZistreamDB();
  INFO_ASSERT(symDBPtr, "Invalid database type");
  ZistreamDB& symDB = *symDBPtr;

  bool ret = mTypeDictionary->restore(symDB);
  if (! ret) {
    mMsgContext->SHLCorruptDBRead(symDB.getError());
    return false;
  }

  if (! mBVPool->dbRead(symDB)) {
    mMsgContext->SHLCorruptDBRead(symDB.getError());
    return false;
  }
  
  // Transfer all the BOM pertinent capabilities
  ShellSymTabBOM* bom = (ShellSymTabBOM*) mDesignTable->getFieldBOM();
  if (testCapability("branchbom.v1")) {
    bom->addCapability(ShellSymTabBOM::eSTCBranchBOMV1);
  }

  if (testCapability("usertypes.v1")) {
    bom->addCapability(ShellSymTabBOM::eSTCUserTypeV1);
    bom->putUserTypeFactory(mUserTypeFactory);
  }

  // Read the BOM data and apply it to the symbol table
  if (!bom->readData(symDB, this, mMsgContext))
    return false;

  // Read the unelaborated symbol table
  if (getDBType () == eCarbonGuiDB) {
    INFO_ASSERT (symDB.expect ("unelaborated"), "Corrupt DB");
    mUnelaboratedSymtab.setHdlHier (mDesignTable->getHdlHier ());
    mUnelaboratedSymtab.readDB (symDB, mMsgContext);
  }

  // Read the source location information
  if (!expectSourceLocations) {
    // the database is free of source
  } else if (mSourceLocatorFactory == NULL) {
    // just throw them away
    INFO_ASSERT (symDB.expect ("sourcelocators"), "Corrupt DB");
    STSymbolTableNode *node = NULL;
    symDB.readPointer (&node);              // read the first node
    while (node != NULL) {
      SourceLocatorFactory::ignoreEntryInDB (symDB);
      symDB.readPointer (&node);
    }
  } else {
    // have source locations, have a factory, suck them in...
    INFO_ASSERT (symDB.expect ("sourcelocators"), "Corrupt DB");
    STSymbolTableNode *node = NULL;
    symDB.readPointer (&node);              // read the first node
    while (node != NULL) {
      SourceLocator loc;
      tmpSourceLocatorFactory.readDB (symDB, &loc, mSourceLocatorFactory);
      mLocations.insert (LocationMap::value_type (node, loc));
      symDB.readPointer (&node);
    }
  }

  // Read the mapping from elaborated to unelaborated symbols
  if (getDBType () == eCarbonGuiDB && testCapability ("unelaboratedsymbols.v1")) {
    INFO_ASSERT (symDB.expect ("elaborationmap"), "Corrupt DB");
    STSymbolTableNode *elaborated = NULL;
    STSymbolTableNode *unelaborated = NULL;
    symDB.readPointer (&elaborated);
    while (elaborated != NULL) {
      symDB.readPointer (&unelaborated);
      mElaborationMap.insert (ElaborationMap::value_type (elaborated, unelaborated));
      symDB.readPointer (&elaborated);
    }
  }

  INFO_ASSERT(symDB.expect("fastClocks"), "Corrupt db.");
  if (!symDB.readPointerContainer(&mFastClocks))
    return false;
  readObserves(symDB);
  readDeposits(symDB);
  INFO_ASSERT(symDB.expect("force"), "Corrupt db.");
  if (!symDB.readPointerContainer(&mForcedNets))
    return false;
  INFO_ASSERT(symDB.expect("expose"), "Corrupt db.");
  if (!symDB.readPointerContainer(&mExposedNets))
    return false;
  INFO_ASSERT(symDB.expect("delayedClocks"), "Corrupt db.");
  if (!symDB.readPointerContainer(&mDelayedClocks))
    return false;
  INFO_ASSERT(symDB.expect("collapsedClocks"), "Corrupt db.");
  for (STSymbolTableNode* node; symDB.readPointer(&node) && (node != NULL);)
  {
    IODB::NameSet* ns = new NameSet;
    mCollapsedClocks[node] = ns;
    for (; symDB.readPointer(&node) && (node != NULL);)
      ns->insert(node);
  }

  INFO_ASSERT(symDB.expect("clockSpeeds"), "Corrupt db.");
  for (STSymbolTableNode* node; symDB.readPointer(&node) && (node != NULL);)
  {
    UInt32 clockSpeed;
    if (! (symDB >> clockSpeed))
    {
      mMsgContext->SHLCorruptDBRead(symDBZip.getFileError());
      return false;
    }
    mClockSpeeds[node] = clockSpeed;
  }

  INFO_ASSERT(symDB.expect("tieNets"), "Corrupt db.");
  for (STSymbolTableNode* node; symDB.readPointer(&node) && (node != NULL);)
  {
    const DynBitVector* val = NULL;
    symDB.readPointer(&val);
    (*mTieNets)[node] = val;
  }

  if (testCapability("asyncOutputs.v2")) {
    INFO_ASSERT(symDB.expect("asyncOutputs"), "Corrupt db.");
    for (STSymbolTableNode* node; symDB.readPointer(&node) && (node != NULL);) {
      IODB::NameSet* ns = new NameSet;
      mAsyncOutputs[node] = ns;
      for (; symDB.readPointer(&node) && (node != NULL);)
        ns->insert(node);
    }
  }

  INFO_ASSERT(symDB.expect("ionets"), "Corrupt db.");
  ret = ret & (symDB.readPointerContainer(&mInputs) &&
               symDB.readPointerContainer(&mOutputs) &&
               symDB.readPointerContainer(&mBidis) &&
               symDB.readPointerContainer(&mClocks) &&
               symDB.readPointerContainer(&mClockTree) &&
               symDB.readPointerContainer(&mPrimaryClocks) &&
               symDB.readPointerContainer(&mAsyncs));

  if (testCapability("asyncOutputs.v1")) {
    NameSet asyncOutputs;
    if (symDB.readPointerContainer(&asyncOutputs)) {
      for (NameSet::UnsortedLoop l = asyncOutputs.loopUnsorted(); !l.atEnd(); ++l) {
        STSymbolTableNode* node = *l;
        IODB::NameSet* ns = new NameSet;
        mAsyncOutputs[node] = ns;
      }
    }
  }

  if (testCapability("asyncDeposits.v1")) {
    ret &= symDB.readPointerContainer(&mAsyncDeposits);
  }

  ret &= (symDB.readPointerContainer(&mAsyncPosResets) &&
          symDB.readPointerContainer(&mAsyncNegResets) &&
          symDB.readPointerContainer(&mCModelSenseNets));

  if (testCapability("wrapperports.v1"))
    ret = ret & (symDB.readPointerContainer(&mWrapPort2StateNets) &&
                 symDB.readPointerContainer(&mWrapPort4StateNets));

  if (testCapability("scheduletriggers.v1"))
    ret = ret & (symDB.readPointerContainer(&mLiveInputDirNets) &&
                 symDB.readPointerContainer(&mLiveOutputDirNets) &&
                 symDB.readPointerContainer(&mPosedgeScheduleTriggers) &&
                 symDB.readPointerContainer(&mNegedgeScheduleTriggers) &&
                 symDB.readPointerContainer(&mScheduleTriggersUsedAsData));
  
  if (testCapability("ondemandnets.v1"))
    ret = ret & (symDB.readPointerContainer(&mOnDemandIdleDepositNets) &&
                 symDB.readPointerContainer(&mOnDemandExcludedNets));
  if (symDB.fail())
  {
    mMsgContext->SHLCorruptDBRead(symDBZip.getFileError());
    return false;
  }
  INFO_ASSERT(symDB.expect("hideModules"), "Corrupt db.");
  if (! symDB.readPointerContainer(&mHiddenModuleTops))
  {
    mMsgContext->SHLCorruptDBRead(symDBZip.getFileError());
    return false;
  }

  INFO_ASSERT(symDB.expect("enableExprs"), "Corrupt db.");
  // unlike the writer we want to use the same string cache for easy
  // HierStringName creation
  if (mExprSymTab.readDB(symDB, mMsgContext) != STFieldBOM::eReadOK)
    return false;
  
  // All the enable expression names are in the local symtab now.
  // We are looping through the non-local database bidi pointers
  // here. The CarbonExpr* will have leaf nodes from the Expression
  // SymbolTable in them. 

  SymNodeDBExprContext exprContext(this, &mExprSymTab, mBVPool);
  
  exprContext.readExprs(symDB, mExprFactory);
  
  for (STAliasedLeafNode* leaf; ret && symDB.readPointer(&leaf) && (leaf != NULL);)
  {
    ret = ret && ! symDB.fail();
    if (ret)
    {
      UInt32 exprIndex;
      ret = (symDB >> exprIndex);
      if (ret)
      {
        CarbonExpr* expr = exprContext.getExpr(exprIndex);
        ST_ASSERT(expr, leaf);
        mEnableExprs[leaf] = expr;
      }
    }
  }

  if (symDB.fail())
  {
    mMsgContext->SHLCorruptDBRead(symDBZip.getFileError());
    return false;
  }

  INFO_ASSERT(symDB.expect("constNetValues"), "Corrupt db.");
  for (STSymbolTableNode* node; symDB.readPointer(&node) && (node != NULL);)
  {
    DynBitVector* val = NULL;
    symDB.readPointer(&val);
    (*mConstNets)[node] = val;
  }
  
  INFO_ASSERT(symDB.expect("topLevelPorts"), "Corrupt db.");
  for (STSymbolTableNode* node; symDB.readPointer(&node) && (node != NULL);)
    mPrimaryPorts.push_back(node);

  if (testCapability("ondemandstateoffsets.v1")) {
    INFO_ASSERT(symDB.expect("onDemandStateOffsets"), "Corrupt db.");
    for (STSymbolTableNode* node; symDB.readPointer(&node) && (node != NULL);) {
      UInt32 offset;
      if (! (symDB >> offset)) {
        mMsgContext->SHLCorruptDBRead(symDBZip.getFileError());
        return false;
      }
      mOnDemandStateOffsets[node] = offset;
    }
    // Read in total state size
    if (! (symDB >> mOnDemandStateSize)) {
      mMsgContext->SHLCorruptDBRead(symDBZip.getFileError());
      return false;
    }
  }

  if (testCapability("ondemandruntimeexclusion.v1")) {
    INFO_ASSERT(symDB.expect("onDemandRuntimeExcludedNets"), "Corrupt db.");
    ret = ret & (symDB.readPointerContainer(&mOnDemandRuntimeExcludedNets));
  }

  // read the design directives
  if (getDBType () == eCarbonGuiDB && testCapability ("directives.v1")) {
    INFO_ASSERT (symDB.expect ("designdirectives"), "Corrupt db.");
    UInt32 n_design_directives;
    symDB >> n_design_directives;
    while (n_design_directives > 0) {
      IODBDesignDirective *directive = new IODBDesignDirective ();
      directive->readDB (&tmpSourceLocatorFactory, mSourceLocatorFactory, symDB);
      n_design_directives--;
      mDesignDirectives.push_back (directive);
    }
    INFO_ASSERT (symDB.expect ("moduledirectives"), "Corrupt db.");
    UInt32 n_module_directives;
    symDB >> n_module_directives;
    while (n_module_directives > 0) {
      IODBModuleDirective *directive = new IODBModuleDirective ();
      directive->readDB (&tmpSourceLocatorFactory, mSourceLocatorFactory, symDB);
      mModuleDirectives.push_back (directive);
      n_module_directives--;
    }
  }

  if (testCapability("softversion.v1"))
  {
    INFO_ASSERT(symDB.expect("softwareVersion"), "Corrupt db.");
    symDB >> mSoftwareVersion;
    ret = ret && ! symDB.fail();
  }

  if (testCapability("genericAttributes.v1")) {
    INFO_ASSERT(symDB.expect("genericAttributes"), "Corrupt db.");
    UInt32 numAttr;
    UtString attrName;
    UtString attrStrVal;
    UInt32 attrIntVal;

    // read string attributes
    symDB >> numAttr;
    for (UInt32 i = 0; !symDB.fail() && (i < numAttr); ++i) {
      attrName.clear();
      attrStrVal.clear();
      symDB >> attrName >> attrStrVal;
      mStringAttrMap[attrName] = attrStrVal;
    }
    // read int attributes
    symDB >> numAttr;
    for (UInt32 i = 0; !symDB.fail() && (i < numAttr); ++i) {
      attrName.clear();
      symDB >> attrName >> attrIntVal;
      mIntAttrMap[attrName] = attrIntVal;
    }
    ret = ret && ! symDB.fail();
  }

  if (testCapability("clockGlitchNames.v1")) {
    INFO_ASSERT(symDB.expect("clockGlitchNames"), "Corrupt db.");

    UInt32 numNames;
    symDB >> numNames;
    UtString* str;
    for (UInt32 i = 0; i < numNames; ++i) {
      str = new UtString;
      symDB >> *str;
      mClockGlitchNames.push_back(str);
    }
  }

  ret = ret && symDB.expect("endOfDatabase");
  ret = ret && ! symDBZip.fail();
  
  if (symDB.fail() || symDBZip.fail())
    mMsgContext->SHLCorruptDBRead(symDBZip.getFileError());
  
  return ret;
} // bool IODBRuntime::readDB

// Read the token and see what variant of observe data we have. There
// are currently two variations. The first is observes and sc observes
// as separate sets. The second is one set which is a map to a set of
// flags. If that is the case we have capability observe.v2
bool IODBRuntime::readObserves(ZistreamDB& symDB)
{
  // The observes always start with a observed string
  INFO_ASSERT(symDB.expect("observed"), "Corrupt db.");

  // Check for the observe.v2 capability
  if (testCapability("observe.v2")) {
    for (STSymbolTableNode* node; symDB.readPointer(&node) && (node != NULL);) {
      UInt32 observeFlags;
      if (!(symDB >> observeFlags)) {
        return false;
      }
      mObservedNets[node] = observeFlags;
    }

  } else {
    // The old separate sets
    readOldObserveSet(symDB, IODB::eOTSimpleMask);
    INFO_ASSERT(symDB.expect("scObserved"), "Corrupt db.");
    readOldObserveSet(symDB, IODB::eOTSystemCMask);
  }
  return true;
} // bool IODBRuntime::readObserves
  

//! \return the source location associated with a symbol
bool IODBRuntime::findLoc (const STSymbolTableNode *node, SourceLocator *loc) const
{
  LocationMap::const_iterator found = mLocations.find (node);
  if (found == mLocations.end ()) {
    return false;
  } else {
    *loc = found->second;
    return true;
  }
}


//! \return the unelaborated symbol corresponding to an elaborated symbol
bool IODBRuntime::getUnelaborated (const STSymbolTableNode *elab, STSymbolTableNode **unelab) const
{
  ElaborationMap::const_iterator found = mElaborationMap.find (elab);
  if (found == mElaborationMap.end ()) {
    *unelab = NULL;
    return false;
  } else {
    *unelab = found->second;
    return true;
  }
}

bool IODBRuntime::getStringAttribute(const char* name, UtString* value)
{
  StringAttrMap::iterator iter = mStringAttrMap.find(name);
  if (iter == mStringAttrMap.end()) {
    return false;
  }

  *value = iter->second;
  return true;
}

bool IODBRuntime::getIntAttribute(const char* name, UInt32* value)
{
  IntAttrMap::iterator iter = mIntAttrMap.find(name);
  if (iter == mIntAttrMap.end()) {
    return false;
  }

  *value = iter->second;
  return true;
}

UtString* IODBRuntime::getClockGlitchName(UInt32 index)
{
  UtString* str = NULL;
  if (index < mClockGlitchNames.size()) {
    str = mClockGlitchNames[index];
  }
  return str;
}

// Read the token and see what variation of deposit data we
// have. There are currently 3 variations:
// 
// 1. The latest varation contains a map from deposit to deposit
//    type. (If the deposit.v3 capability is there)
//
// 2. The rev before had multiple sets for each deposit type
//    including:
//
//    - A deposit set
//    - A frequently depositable set
//    - An infrequently depositable set
//    - An sc deposit set
//
// 3. The rev before that had only the deposit and SC deposit set.
//
// This code will decide what we have an convert it to the latest
// variation.
bool IODBRuntime::readDeposits(ZistreamDB& symDB)
{
  // The deposits always start with a deposit string
  INFO_ASSERT(symDB.expect("deposit"), "Corrupt db.");

  // Check for deposit.v3 capability. If it is there use the new mechanism
  if (testCapability("deposit.v3")) {
    // Case 1
    for (STSymbolTableNode* node; symDB.readPointer(&node) && (node != NULL);) {
      UInt32 depositFlags;
      if (!(symDB >> depositFlags)) {
        return false;
      }
      mDepositNets[node] = depositFlags;
    }
  } else {
    // Case 2 or three. Assume 2 and then fixup by adding the
    // appropriate flags.
    readOldDepositSet(symDB, IODB::eDTSimpleMask);

    // Check if we have a frequently depositable set
    UtString nextTok;
    symDB >> nextTok;
    if (nextTok == "frequentDeposit") {
      if (!readOldDepositSet(symDB, IODB::eDTFrequentMask)) {
        return false;
      }
      INFO_ASSERT(symDB.expect("infrequentDeposit"), "Corrupt db.");
      if (!readOldDepositSet(symDB, IODB::eDTInfrequentMask)) {
        return false;
      }
      nextTok.clear();
      symDB >> nextTok;
    }

    // In either case 2 or three we have the SC deposit set
    INFO_ASSERT(nextTok == "scDeposit", "Corrupt db.");
    if (!readOldDepositSet(symDB, IODB::eDTSystemCMask)) {
      return false;
    }
  }
  return true;
} // bool IODBRuntime::readDeposits

bool IODBRuntime::readOldDepositSet(ZistreamDB& symDB, IODB::DepositMask mask)
{
  // Start by getting the set size
  UInt32 num;
  if (!(symDB >> num)) {
    return false;
  }
  for (UInt32 i = 0; i < num; ++i) {
    // Get the pointer
    STSymbolTableNode* node;
    if (!(symDB.readPointer(&node))) {
      return false;
    }

    // Update the map. If we are setting the simple mask just set
    // it. If we are setting something else, undo the simple mask and
    // or in the new mask. This is what is meant by fixup in the main
    // readDeposits comment.
    if (mask == IODB::eDTSimpleMask) {
      mDepositNets[node] = (UInt32)mask;
    } else {
      UInt32 prevMask = mDepositNets[node] & (~(UInt32)IODB::eDTSimpleMask);
      mDepositNets[node] = prevMask | (UInt32)mask;
    }
  }
  return true;
} // bool IODBRuntime::readOldDepositSet

bool IODBRuntime::readOldObserveSet(ZistreamDB& symDB, IODB::ObserveMask mask)
{
  // Start by getting the set size
  UInt32 num;
  if (!(symDB >> num)) {
    return false;
  }
  for (UInt32 i = 0; i < num; ++i) {
    // Get the pointer
    STSymbolTableNode* node;
    if (!(symDB.readPointer(&node))) {
      return false;
    }

    // Update the map. If we are setting the simple mask just set
    // it. If we are setting something else, undo the simple mask and
    // or in the new mask. This is what is meant by fixup in the main
    // readObserves comment.
    if (mask == IODB::eOTSimpleMask) {
      mObservedNets[node] = (UInt32)mask;
    } else {
      UInt32 prevMask = mObservedNets[node] & (~(UInt32)IODB::eOTSimpleMask);
      mObservedNets[node] = prevMask | (UInt32)mask;
    }
  }
  return true;
} // bool IODBRuntime::readOldObserveSet

void IODBRuntime::doBidiCheck(UtString* buffer)
{
  const HdlHierPath* hdlHier = mDesignTable->getHdlHier();
  for (STSymbolTable::NodeLoop p = mDesignTable->getNodeLoop();
       ! p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    STAliasedLeafNode* storage = NULL;
    if (leaf)
      storage = leaf->getStorage();
    if (storage && (mBidis.find(storage) == mBidis.end()))
    {
      const IODBGenTypeEntry* entry = getLeafType(storage);
      if (entry && entry->isBidirect())
      {
        if (! buffer->empty())
          *buffer << "\n";

        UtString storeName;
        UtString curName;
        hdlHier->compPathHier(storage, &storeName);
        hdlHier->compPathHier(leaf, &curName);
        *buffer << curName << " (storage=" << storeName << "): unlisted bidi";
      }
    }  
  }
} // void IODBRuntime::doBidiCheck

//! Get the type information for a signal
const IODBGenTypeEntry* IODBRuntime::getType(const STSymbolTableNode* node)
  const
{
  const STAliasedLeafNode* leaf = node->castLeaf();
  const IODBGenTypeEntry* type = NULL;
  if (leaf)
    type = getLeafType(leaf);
  return type;
}

//! Get the intrinsic type info for a signal
const IODBIntrinsic*
IODBRuntime::getLeafIntrinsic( const STAliasedLeafNode* leaf ) const
{
  if ( leaf != NULL )
  {
    const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(leaf);
    if (! data)
    {
      UtString name;
      const HdlHierPath* pather = getDesignSymbolTable()->getHdlHier();
      pather->compPathHier(leaf, &name);
      // fatally exit
      getMsgContext()->SHLNoTypeEntry(name.c_str());
    }
    return data->getIntrinsic();
  }

  return NULL;
}

//! Get the type information for a signal
const IODBGenTypeEntry*
IODBRuntime::getLeafType(const STAliasedLeafNode* node) const
{
  const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
  UInt32 typeIndex = data->getTypeIndex();
  // At runtime this IODBGenTypeEntry, while containing a pointer to an
  // IODBIntrinsic, will not necessarily have the _correct_ intrinsic.
  // It's an open question if the genTypeEntry needs to even have a
  // pointer to the intrinsic in it.  In any case, the intrinsic data
  // off the gentype entry cannot be used for bounds information for
  // vectors, just for size info.
  IODBGenTypeEntry *gte = const_cast<IODBGenTypeEntry*>(mTypeDictionary->getGenTypeEntry( typeIndex ));
  return gte;
}

//! Get the type information for a signal
SInt32 IODBRuntime::getLeafGenTypeIndex(const STAliasedLeafNode* node)
  const
{
  const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
  UInt32 typeIndex = data->getTypeIndex();
  return typeIndex;
}

//! Get the type information for a signal
const SCHSignature* IODBRuntime::getSignature(const STSymbolTableNode* node)
  const
{
  const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
  const SCHSignature* sig = NULL;
  if (data)
    sig = data->getSCHSignature();
  return sig;
}

const UserType* IODBRuntime::getUserType(const STSymbolTableNode* node) const
{
  const UserType* ut = NULL;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf != NULL) {
    const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(leaf);
    if (data) {
      ut = data->getUserType();
    }
  } else {
    const STBranchNode* branch = node->castBranch();
    const ShellBranchDataBOM* data = ShellSymTabBOM::getBranchBOM(branch);
    if (data) {
      ut = data->getUserType();
    }
  }
  return ut;
}

IODBRuntime::IODBRuntime(AtomicCache* cache, STSymbolTable* st, 
  MsgContext* msg,
  SCHScheduleFactory* sf,
  ESFactory* exprFactory,
  SourceLocatorFactory *sourceLocatorFactory) :
  IODB(cache, st, msg, sf), mExprFactory(exprFactory), mEmptyBOM(), mUnelaboratedBOM (),
  mExprSymTab(&mEmptyBOM, cache), mSourceLocatorFactory (sourceLocatorFactory),
  mUnelaboratedSymtab (&mUnelaboratedBOM, cache),
  mLocations ()
{
  mExprSymTab.setHdlHier(st->getHdlHier());
}

IODBRuntime::~IODBRuntime()
{
  // Must be done here. See IODBNucleus's destructor
  for (NameSetMapLoop p = loopCollapseClocks(); !p.atEnd(); ++p)
  {
    NameSet* ns = p.getValue();
    delete ns;
  }
}

const UtCustomerDB* IODBRuntime::getCustomerDB() const
{
  return &mCustomerRuntimeDB;
}

const STSymbolTable* IODBRuntime::getExprSymTab() const
{
  return &mExprSymTab;
}

ESFactory* IODBRuntime::getExprFactory()
{
  return mExprFactory;
}

CarbonIdent* IODBRuntime::createExprIdent(const STAliasedLeafNode* net, UInt32 bitSize, const DynBitVector* usageVec, CbuildShellDB::CarbonIdentType type, const CarbonExprVector* exprVec)
{
  // I can get away with passing NULL for the DynBitVectorFactory
  // because I know I am not creating or retrieving one at this point
  SymNodeDBExprContext dbExprContext(this, &mExprSymTab, NULL);

  // need to cast the const enableNet to non-const in order to fit it
  // into the expression tree
  STAliasedLeafNode* castNet = const_cast<STAliasedLeafNode*>(net);

  // Must create a ShellSymNodeIdent before checking the cache.
  CarbonIdent* ident = dbExprContext.createIdent(castNet, bitSize, usageVec, type, exprVec);
  
  bool added;
  // If it already exists in the cache delete it.
  CarbonIdent* cacheExpr = mExprFactory->createIdent(ident, added);
  if (! added)
    dbExprContext.destroyIdent(ident);
  return cacheExpr;
}


bool IODBRuntime::sIsCompletelyOverridden(const DynBitVector* overrideMaskBV, const IODBIntrinsic* intrinsic)
{
  bool isTotalOverride = false;
  if (! (intrinsic->getType() == IODBIntrinsic::eMemory)
      && overrideMaskBV)
  {
    UInt32 width = intrinsic->getWidth();
    isTotalOverride = IODB::sIsOverrideMaskAllOnes(overrideMaskBV, width);
  }
  return isTotalOverride;
}

void IODBRuntime::sComposeOverrideMask(DynBitVector* override, const UInt32* value, 
                                       const UInt32* xzMask, const UInt32* control, 
                                       UInt32 numWords)
{
  override->resize(numWords * 32 * 3);
  UInt32* bvArr = override->getUIntArray();
  CarbonValRW::cpSrcToDest(bvArr, control, numWords);
  bvArr += numWords;
  CarbonValRW::cpSrcToDest(bvArr, value, numWords);
  bvArr += numWords;
  CarbonValRW::cpSrcToDest(bvArr, xzMask, numWords);  
}

void IODBRuntime::sExtractOverride(const UInt32* overrideMask, const UInt32** value, 
                                   const UInt32** xzMask, const UInt32** control, 
                                   UInt32 numWords)
{
  *control = overrideMask;
  *value = overrideMask + numWords;
  *xzMask = overrideMask + 2*numWords;
}

void IODBRuntime::sSetOverride(const UInt32* overrideMask, UInt32* val, UInt32* drive, UInt32 numWords)
{
  if (overrideMask != NULL) {
    const UInt32* valueMask = overrideMask + numWords;
    const UInt32* xzMask = overrideMask + 2*numWords;
    
    for (UInt32 i = 0; i < numWords; ++i) {
      // Copy constants into the value-array
      UInt32 mask = *overrideMask++;
      if (val != NULL) {
        *val = (*val & ~mask) | (*valueMask & mask);
        ++val;
        ++valueMask;
      }

      // Update the drive
      if (drive != NULL) {
        *drive = (*xzMask & mask);
        ++drive;
        ++xzMask;
      }
    }
  }
}

void IODBRuntime::sSetOverrideWord(const UInt32* overrideMask, UInt32* val,
                                   UInt32* drive, UInt32 numWords, int index)
{
  if (overrideMask)
  {
    const UInt32* valueMask = overrideMask + numWords;
    const UInt32* xzMask = overrideMask + 2*numWords;
    
    // Copy constants into the value-array
    UInt32 mask = overrideMask[index];
    if (val != NULL) {
      *val = (*val & ~mask) | (*valueMask & mask);
    }

    // Update the drive
    if (drive != NULL) {
      drive[index] = (xzMask[index] & mask);
    }
  }
}

bool IODBRuntime::isInvalidWaveNet(const STSymbolTableNode* node) const 
{
  bool isInvalid = false;
  const STAliasedLeafNode* a = node->castLeaf();
  if (a != NULL)
  {
    const ShellDataBOM* data = ShellSymTabBOM::getStorageDataBOM(a);
    isInvalid = data->isInvalidWaveNet();
  }
  return isInvalid;
}

bool IODBRuntime::isFullyConstant(const STAliasedLeafNode* leaf) const
{
  const STSymbolTableNode * symtab_node = leaf->getStorage();
  NameValueMap::const_iterator p = mConstNets->find(const_cast<STSymbolTableNode*>(symtab_node));
  if (p != mConstNets->end()) {
    const DynBitVector* bv = p->second;
    const IODBIntrinsic* intrinsic = getLeafIntrinsic(leaf);
    if (intrinsic != NULL) {
      return sIsCompletelyOverridden(bv, intrinsic);
    }
  }
  return false;
}

const CarbonExpr* IODBRuntime::getEnableExpr(const STSymbolTableNode* node) const
{
  CarbonExpr* ret = NULL;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    NameExprMap::const_iterator p = mEnableExprs.find(const_cast<STAliasedLeafNode*>(leaf));
    if (p != mEnableExprs.end())
      ret = p->second;
  }
  return ret;
}

bool IODBRuntime::is2DArray(const STSymbolTableNode* node) const
{
  bool ret = false;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    const IODBIntrinsic* intrinsic = getLeafIntrinsic(leaf);
    ret = intrinsic->getType() == IODBIntrinsic::eMemory;
  }
  return ret;
}

bool IODBRuntime::isVector(const STSymbolTableNode* node) const
{
  bool ret = false;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    const IODBIntrinsic* intrinsic = getLeafIntrinsic(leaf);
    ret = intrinsic->getType() == IODBIntrinsic::eVector;
  }
  return ret;
}

bool IODBRuntime::isScalar(const STSymbolTableNode* node) const
{
  bool ret = false;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    const IODBIntrinsic* intrinsic = getLeafIntrinsic(leaf);
    ret = intrinsic->getType() == IODBIntrinsic::eScalar;
  }
  return ret;
}

bool IODBRuntime::isTristate(const STSymbolTableNode* node) const
{
  bool ret = false;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    const IODBGenTypeEntry* type = getType(node);
    ret = type->isTristate();
    if (! ret)
    {
      const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(leaf);
      if (data && data->isExpression())
      {
        const CarbonExpr* expr = data->getExpr();
        ExprFlagChecker checker(this, leaf, &IODBRuntime::isTristate);
        checker.visitExpr(const_cast<CarbonExpr*>(expr));
        ret = checker.isFlagSet();
      }
    }
  }
  return ret;
}

bool IODBRuntime::isConstant(const STSymbolTableNode* node) const
{
  bool ret = false;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    const STAliasedLeafNode* storage = leaf->getStorage();
    const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(storage);
    ret = CbuildShellDB::isConstantType(bomdata->getTypeTag());
    if (! ret)
    {
      // we need to see if the net is completely overridden with a
      // constant mask
      const DynBitVector* overrideMask = getConstNetBitMask(leaf);
      const IODBIntrinsic* intrinsic = getLeafIntrinsic(leaf);
      ret = sIsCompletelyOverridden(overrideMask, intrinsic);
    }
  }
  return ret;
}

SInt32 IODBRuntime::getWidth(const STSymbolTableNode* node) const
{
  SInt32 ret = 0;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    const IODBIntrinsic* intrinsic = getLeafIntrinsic(leaf);
    ret = intrinsic->getWidth();
  }
  return ret;
}

SInt32 IODBRuntime::getLsb(const STSymbolTableNode* node) const
{
  SInt32 ret = 0;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    const IODBIntrinsic* intrinsic = getLeafIntrinsic(leaf);
    ret = intrinsic->getLsb();
  }
  return ret;
}

SInt32 IODBRuntime::getMsb(const STSymbolTableNode* node) const
{
  SInt32 ret = 0;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    const IODBIntrinsic* intrinsic = getLeafIntrinsic(leaf);
    ret = intrinsic->getMsb();
  }
  return ret;
}

SInt32 IODBRuntime::get2DArrayLeftAddr(const STSymbolTableNode* node) const
{
  SInt32 ret = 0;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    const IODBIntrinsic* intrinsic = getLeafIntrinsic(leaf);
    ret = intrinsic->getHighAddr();
  }
  return ret;
}

SInt32 IODBRuntime::get2DArrayRightAddr(const STSymbolTableNode* node) const
{
  SInt32 ret = 0;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    const IODBIntrinsic* intrinsic = getLeafIntrinsic(leaf);
    ret = intrinsic->getLowAddr();
  }
  return ret;
}

bool IODBRuntime::isInput(const STSymbolTableNode* node, 
  const ElaborationState elaborated) const 
{
  switch (elaborated) {
  case eElaborated:
    {
      const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
      if (data == NULL) {
        return false;
      } else {
        return (data->getNetFlags() & ePortMask) == eInputNet;
      }
    }
    break;
  case eUnelaborated:
    {
      const IODBRuntimeAliasDataBOM *data = IODBRuntimeAliasBOM::castBOM (node);
      if (data == NULL) {
        return false;
      } else {
        return data->isInput ();
      }
    }
    break;
  default:
    return false;
    break;
  }
}

bool IODBRuntime::isOutput(const STSymbolTableNode* node, const ElaborationState elaborated) const 
{
  switch (elaborated) {
  case eElaborated:
    {
      const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
      if (data == NULL) {
        return false;
      } else {
        return (data->getNetFlags() & ePortMask) == eOutputNet;
      }
    }
    break;
  case eUnelaborated:
    {
      const IODBRuntimeAliasDataBOM *data = IODBRuntimeAliasBOM::castBOM (node);
      if (data == NULL) {
        return false;
      } else {
        return data->isOutput ();
      }
    }
    break;
  default:
    return false;
    break;
  }
}

bool IODBRuntime::isBidirect(const STSymbolTableNode* node, const ElaborationState elaborated) const 
{
  switch (elaborated) {
  case eElaborated:
    {
      const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
      if (data == NULL) {
        return false;
      } else {
        return (data->getNetFlags() & ePortMask) == eBidNet;
      }
    }
    break;
  case eUnelaborated:
    {
      const IODBRuntimeAliasDataBOM *data = IODBRuntimeAliasBOM::castBOM (node);
      if (data == NULL) {
        return false;
      } else {
        return data->isBidi ();
      }
    }
    break;
  default:
    return false;
    break;
  }
}

bool IODBRuntime::isSampleScheduled(const STSymbolTableNode* node) const
{
  bool ret = false;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    const ShellDataBOM* data = ShellSymTabBOM::getStorageDataBOM(leaf);
    if (data)
      ret = data->needsDebugSched();
  }
  return ret;
}

bool IODBRuntime::isMarkedClock(const STSymbolTableNode* node) const
{
  bool ret = false;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf)
  {
    const ShellDataBOM* data = ShellSymTabBOM::getStorageDataBOM(leaf);
    if (data)
      ret = data->isClock();
  }
  return ret;
}

const ShellBranchDataBOM* IODBRuntime::getBranchData(const STBranchNode* branch)
  const
{
  return ShellSymTabBOM::getBranchBOM(branch);
}
  

HierFlags IODBRuntime::getSourceLanguage(const STBranchNode* branch) const
{
  // Walk up the hierarchy until we find a module which has a known
  // language.
  while ((branch != NULL) && 
         (getBranchData(branch)->getHierType() != eHTModule)) {
    branch = branch->getParent();
  }

  // Return the language flags
  if (branch != NULL) {
    return getBranchData(branch)->getSourceLanguage();
  } else {
    return eSLUnknown;
  }
}

HierFlags IODBRuntime::getSourceLanguage(const STAliasedLeafNode* leaf) const
{
  // Get the source language from this leafs parent
  const STBranchNode* branch = leaf->getParent();
  return getSourceLanguage(branch);
}

const char* IODBRuntime::declarationType(const STSymbolTableNode* node) const
{
  // Make sure this is a valid net
  const char* ret = NULL;
  const STAliasedLeafNode* leaf = node->castLeaf();
  if (leaf != NULL) {
    const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(leaf);
    if (data) {
      // It is valid, get the net flags and language
      NetFlags declareFlags = NetFlags(data->getNetFlags() & eDeclareMask);
      HierFlags sourceLanguage = getSourceLanguage(leaf);
      switch (sourceLanguage) {
        case eSLVerilog:
          switch (declareFlags) {
            case eDMTimeNet:
              ret = "time";
              break;
            case eDMWireNet:
              ret = "wire";
              break;
            case eDMTriNet:
              ret = "tri";
              break;
            case eDMTri1Net:
              ret = "tri1";
              break;
            case eDMSupply0Net:
              ret = "supply0";
              break;
            case eDMWandNet:
              ret = "wand";
              break;
            case eDMTriandNet:
              ret = "triand";
              break;
            case eDMTri0Net:
              ret = "tri0";
              break;
            case eDMSupply1Net:
              ret = "supply1";
              break;
            case eDMWorNet:
              ret = "wor";
              break;
            case eDMTriorNet:
              ret = "trior";
              break;
            case eDMTriregNet:
              ret = "trireg";
              break;
            case eDMRegNet:
              ret = "reg";
              break;
            case eDMRealNet:
              ret = "real";
              break;
            case eDMIntegerNet:
              ret = "integer";
              break;
            default:
              ret = "<invalid>";
              break;
          } // switch
          break;

        case eSLVHDL:
          switch(declareFlags) {
            case eDMWireNet:
              if (isVector(node)) {
                ret = "std_logic_vector";
              } else {
                ret = "std_logic";
              }
              break;

            case eDMRealNet:
              ret = "real";
              break;

            default:
              ret = "<invalid>";
              break;
          }
          break;

        default:
          ret = "<unknown-language>";
          break;
      } // switch
    } // if
  } // if

  return ret;
} // const char* IODBRuntime::declarationType

const char* IODBRuntime::componentName(const STSymbolTableNode* node) const
{
  // Make sure it is a branch node
  const STBranchNode* branch = node->castBranch();
  if (branch == NULL) {
    return NULL;
  }

  // Return the name from the branch data 
  return getBranchData(branch)->getModuleName()->str();
}

const char* IODBRuntime::sourceLanguage(const STSymbolTableNode* node) const
{
  // get the language flags
  HierFlags language;
  const STBranchNode* branch = node->castBranch();
  if (branch != NULL) {
    language = getSourceLanguage(branch);
  } else {
    const STAliasedLeafNode* leaf = node->castLeaf();
    ST_ASSERT(leaf != NULL, node);
    language = getSourceLanguage(leaf);
  }

  // Convert it to a string
  const char* langStr;
  switch (language) {
    case eSLVerilog:
      langStr = "Verilog";
      break;

    case eSLVHDL:
      langStr = "VHDL";
      break;
      
    default:
    case eSLUnknown:
      langStr = "Unknown";
      break;
  }
  return langStr;
} // const char* IODBRuntime::sourceLanguage

CarbonPullMode IODBRuntime::getPullMode(const STSymbolTableNode* node) const
{
  CarbonPullMode mode = eCarbonNoPull;

  const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
  NetFlags flags = data->getNetFlags();
  if (NetIsPullUp(flags))
    mode = eCarbonPullUp;
  else if (NetIsPullDown(flags))
    mode = eCarbonPullDown;
  return mode;
}

bool IODBRuntime::isTemp(const STSymbolTableNode* node) const 
{
  const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
  bool ret = false;
  if (data) {
    ret = NetIsTemp(data->getNetFlags());
  }
  return ret;
}

bool IODBRuntime::isRuntimeAsync(const STSymbolTableNode* node) const
{
  
  bool isAsync = isAsyncNode(node);
  if (! isAsync)
  {
    const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
    if (data && data->isExpression())
    {
      const CarbonExpr* expr = data->getExpr();
      ExprFlagChecker checker(this, node->castLeaf(), &IODB::isAsyncNode);
      checker.visitExpr(const_cast<CarbonExpr*>(expr));
      isAsync = checker.isFlagSet();
    }
  }
  return isAsync;
}

bool IODBRuntime::isRuntimeAsyncOutput(const STSymbolTableNode* node) const
{
  
  bool isAsync = isAsyncOutputNode(node);
  if (! isAsync)
  {
    const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
    if (data && data->isExpression())
    {
      const CarbonExpr* expr = data->getExpr();
      ExprFlagChecker checker(this, node->castLeaf(), &IODB::isAsyncOutputNode);
      checker.visitExpr(const_cast<CarbonExpr*>(expr));
      isAsync = checker.isFlagSet();
    }
  }
  return isAsync;
}

bool IODBRuntime::isRuntimeAsyncDeposit(const STSymbolTableNode* node) const
{
  
  bool isAsync = isAsyncDepositNode(node);
  if (! isAsync)
  {
    const ShellDataBOM* data = ShellSymTabBOM::getLeafBOM(node);
    if (data && data->isExpression())
    {
      const CarbonExpr* expr = data->getExpr();
      ExprFlagChecker checker(this, node->castLeaf(), &IODB::isAsyncDepositNode);
      checker.visitExpr(const_cast<CarbonExpr*>(expr));
      isAsync = checker.isFlagSet();
    }
  }
  return isAsync;
}
