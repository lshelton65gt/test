// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "iodb/IODB.h"
#include "hdl/HdlVerilogPath.h"
#include "hdl/HdlId.h"
#include "util/UtIOStream.h"
#include "iodb/IODBTypes.h"
#include "iodb/IODBUserTypes.h"
#include "util/AtomicCache.h"
#include "schedule/Event.h"
#include "schedule/ScheduleMask.h"
#include "schedule/Signature.h"
#include <stdio.h>
#include "symtab/STAliasedLeafNode.h"
#include "util/HierStringName.h"
#include "util/StringAtom.h"
#include "util/DynBitVector.h"
#include "util/UtHashMap.h"
#include "util/UtConv.h" // CarbonValRW
#include "util/Zstream.h"

const SInt32 IODB::cEndOfRecord = -1;

const char* IODB::scFullDBExt = ".symtab.db";
const char* IODB::scIODBExt = ".io.db";
const char* IODB::scGuiDBExt = ".gui.db";
const char* IODB::scAutoDBExt = ".<auto>.db";

static const char* cIODBDesignInfoSig = "IODB_DesignInfo";
// Upping the design info from 3 to 4 for memory callbacks. 
// The design info structure didn't change, but I need the dbread to
// error out if we are linked to a newer Carbon Model with memory callbacks.
// 5: Added isPortInterfaceGenerated().
// 6: Added isClockGlitchGenerated().
static const UInt32 cIODBDesignInfoVersion = 6;

static const char* cIODBSig = "IODB";
// upped the version from 3 to 4 due to schedule triggers.
// Wrappers should use this to determine if an old database can be
// used to generate a modern wrapper.
// 5: added versioning to carbon_id
// 6: added onDemand eligible directive
// 7: added observe.v2 capability (MV observe/deposit flags)
// 8: added software version (softversion.v1)
// 9: added onDemand state offsets
//10: added source locations (MJM)
//11: added user design type information (usertypes.v1) (Vrishal)
//12: added generic attribute feature
//13: added clock glitch name array
//14: added Library and Package names in the user types
//15: Modified asyncOutput to v2
//16: Added packed/unpacked UserArray flag
static const UInt32 cIODBVersion = 16;


// We must create our own private symbol table to hold the
// names we parse, because the directives get parsed before the verilog
// is read in, and the order in the "real" symbol table needs to
// be explicitly controlled to assist in name lookup.
IODB::IODB(AtomicCache* cache, STSymbolTable* st,
           MsgContext* msgs,
           SCHScheduleFactory* sf)
{
  init(cache, st, msgs, sf);
}

void IODB::init(AtomicCache* cache, STSymbolTable* st,
                MsgContext* msgs,
                SCHScheduleFactory* sf)
{
  INFO_ASSERT(st, "NULL symtab passed as parameter.");
  mHdl = new HdlVerilogPath;
  mTypeDictionary = new IODBTypeDictionary;
  mUserTypeFactory = new UserTypeFactory(cache, this);
  mAtomicCache = cache;
  mDesignTable = st;
  mMsgContext = msgs;
  mScheduleFactory = sf;
  mDesignTable->setHdlHier(mHdl);
  mDefaultSpeed = 0;
  mOnDemandStateSize = 0;
  mTieNets = new NameValueMap;
  mConstNets = new NameValueMap;
  mDesignInfo = new DesignInfo;
  mBVPool = new DynBitVectorFactory(true); // uniquify by size
  mOwnBVPool = true;
  mCapabilities = new UtStringSet;
  mInputDBVersion = cIODBVersion;

  mUserTypeFactory->setUserTypesAtomicCache(mAtomicCache);
}

IODB::~IODB() {
  delete mHdl;
  delete mTypeDictionary;
  delete mUserTypeFactory;
  delete mTieNets;
  delete mConstNets;
  delete mDesignInfo;
  if (mOwnBVPool) {
    delete mBVPool;
  }
  delete mCapabilities;

  for (NameSetMapLoop p = loopAsyncOutputs(); !p.atEnd(); ++p) {
    NameSet* ns = p.getValue();
    delete ns;
  }

  // We allocated all the clock glitch name entries, so free them
  for (UInt32 i = 0; i < mClockGlitchNames.size(); ++i) {
    delete mClockGlitchNames[i];
  }
}

MsgContext* IODB::getMsgContext() const
{
  return mMsgContext;
}

bool IODB::isPrimary(const STSymbolTableNode* node) const 
{
  return isPrimaryInput(node) || isPrimaryOutput(node) || isPrimaryBidirect(node);
}

bool IODB::isPrimaryInput(const STSymbolTableNode* node) const 
{
  NameSet::const_iterator p = mInputs.find(const_cast<STSymbolTableNode*>(node));
  return (p != mInputs.end());
}

bool IODB::isPrimaryOutput(const STSymbolTableNode* node) const 
{
  NameSet::const_iterator p = mOutputs.find(const_cast<STSymbolTableNode*>(node));
  return (p != mOutputs.end());
}

bool IODB::isPrimaryBidirect(const STSymbolTableNode* node) const 
{
  NameSet::const_iterator p = mBidis.find(const_cast<STSymbolTableNode*>(node));
  return (p != mBidis.end());
}

bool IODB::isClock(const STSymbolTableNode* node) const 
{
  NameSet::const_iterator p = mClocks.find(const_cast<STSymbolTableNode*>(node));
  return (p != mClocks.end());
}

bool IODB::isClockTree(const STSymbolTableNode* node) const 
{
  NameSet::const_iterator p = mClockTree.find(const_cast<STSymbolTableNode*>(node));
  return (p != mClockTree.end());
}

bool IODB::isAsyncNode(const STSymbolTableNode* node) const 
{
  return mAsyncs.count(const_cast<STSymbolTableNode*>(node)) != 0;
}

bool IODB::isAsyncOutputNode(const STSymbolTableNode* node) const 
{
  return mAsyncOutputs.count(const_cast<STSymbolTableNode*>(node)) != 0;
}

IODB::NameSetLoop IODB::loopAsyncFanin(const STSymbolTableNode* node)
{
  NameSetMap::const_iterator p = mAsyncOutputs.find(const_cast<STSymbolTableNode*>(node));
  if (p != mAsyncOutputs.end()) {
    NameSet* ns = p->second;
    return NameSetLoop::create(ns->loopSorted());
  } else {
    return NameSetLoop::create(mEmptySet.loopSorted());
  }
}

bool IODB::isAsyncDepositNode(const STSymbolTableNode* node) const 
{
  return mAsyncDeposits.count(const_cast<STSymbolTableNode*>(node)) != 0;
}

bool IODB::isPrimaryClock(const STSymbolTableNode* node) const 
{
  NameSet::const_iterator p =
    mPrimaryClocks.find(const_cast<STSymbolTableNode*>(node));
  return (p != mPrimaryClocks.end());
}

bool IODB::hasPosedgeEvent(const SCHSignature* signature) const 
{
  const SCHScheduleMask* transMask = signature->getTransitionMask();
  const SCHScheduleMask* sampleMask = signature->getSampleMask();
  SCHScheduleMask::UnsortedEvents loop = transMask->loopEvents();
  bool hasPosTrans = false;
  bool hasPosSample = false;
  
  while (! hasPosTrans && ! loop.atEnd())
  {
    const SCHEvent* event = *loop;
    if (event->isClockEvent())
      hasPosTrans = (event->getClockEdge() == eClockPosedge);
    ++loop;
  }
  
  
  if (! hasPosTrans)
  {
    loop = sampleMask->loopEvents();
    
    while (! hasPosSample && ! loop.atEnd())
    {
      const SCHEvent* event = *loop;
      if (event->isClockEvent())
        hasPosSample = (event->getClockEdge() == eClockPosedge);
      ++loop;
    }
  }
  
  return (hasPosTrans || hasPosSample);
}

bool IODB::hasNegedgeEvent(const SCHSignature* signature) const 
{
  const SCHScheduleMask* transMask = signature->getTransitionMask();
  const SCHScheduleMask* sampleMask = signature->getSampleMask();
  SCHScheduleMask::UnsortedEvents loop = transMask->loopEvents();
  bool hasNegTrans = false;
  bool hasNegSample = false;
  
  while (! hasNegTrans && ! loop.atEnd())
  {
    const SCHEvent* event = *loop;
    if (event->isClockEvent())
      hasNegTrans = (event->getClockEdge() == eClockNegedge);
    ++loop;
  }
  
  
  if (! hasNegTrans)
  {
    loop = sampleMask->loopEvents();
    
    while (! hasNegSample && ! loop.atEnd())
    {
      const SCHEvent* event = *loop;
      if (event->isClockEvent())
        hasNegSample = (event->getClockEdge() == eClockNegedge);
      ++loop;
    }
  }
  
  return (hasNegTrans || hasNegSample);
}

bool IODB::isPosedgeReset(const STSymbolTableNode* node) const 
{
  NameSet::const_iterator p = mAsyncPosResets.find(const_cast<STSymbolTableNode*>(node));
  return (p != mAsyncPosResets.end());
}

bool IODB::isNegedgeReset(const STSymbolTableNode* node) const 
{
  NameSet::const_iterator p = mAsyncNegResets.find(const_cast<STSymbolTableNode*>(node));
  return (p != mAsyncNegResets.end());
}

bool IODB::isForcible(const STSymbolTableNode* node) const
{
  NameSet::const_iterator p = mForcedNets.find(const_cast<STSymbolTableNode*>(node));
  return (p != mForcedNets.end());
}

bool IODB::isDepositable(const STSymbolTableNode* constNode) const
{
  STSymbolTableNode* node = const_cast<STSymbolTableNode*>(constNode);
  return mDepositNets.count(node) != 0;
}

bool IODB::isScDepositable(const STSymbolTableNode* constNode) const
{
  STSymbolTableNode* node = const_cast<STSymbolTableNode*>(constNode);
  NameIntMap::const_iterator p = mDepositNets.find(node);
  if (p == mDepositNets.end()) {
    return false;
  }
  UInt32 flags = p->second;
  return flags & eDTSystemCMask;
}

bool IODB::isMvDepositable(const STSymbolTableNode* constNode) const
{
  STSymbolTableNode* node = const_cast<STSymbolTableNode*>(constNode);
  NameIntMap::const_iterator p = mDepositNets.find(node);
  if (p == mDepositNets.end()) {
    return false;
  }
  UInt32 flags = p->second;
  return flags & eDTModelValidationMask;
}

bool IODB::isFrequentDepositable(const STSymbolTableNode* constNode) const
{
  STSymbolTableNode* node = const_cast<STSymbolTableNode*>(constNode);
  NameIntMap::const_iterator p = mDepositNets.find(node);
  if (p != mDepositNets.end()) {
    return (p->second & eDTFrequentMask) != 0;
  }
  return false;
}

bool IODB::isInfrequentDepositable(const STSymbolTableNode* constNode) const
{
  STSymbolTableNode* node = const_cast<STSymbolTableNode*>(constNode);
  NameIntMap::const_iterator p = mDepositNets.find(node);
  if (p != mDepositNets.end()) {
    return (p->second & eDTInfrequentMask) != 0;
  }
  return false;
}

bool IODB::isFastClock(const STSymbolTableNode* node) const
{
  NameSet::const_iterator p = mFastClocks.find(const_cast<STSymbolTableNode*>(node));
  return (p != mFastClocks.end());
}

bool IODB::isSlowClock(const STSymbolTableNode* node) const
{
  NameSet::const_iterator p = mSlowClocks.find(const_cast<STSymbolTableNode*>(node));
  return (p != mSlowClocks.end());
}

bool IODB::isFastReset(const STSymbolTableNode* node) const
{
  NameSet::const_iterator p = mFastResets.find(const_cast<STSymbolTableNode*>(node));
  return (p != mFastResets.end());
}

UInt32 IODB::getClockSpeed(const STSymbolTableNode* node) const
{
  NameIntMap::const_iterator p =
    mClockSpeeds.find(const_cast<STSymbolTableNode*>(node));
  if (p != mClockSpeeds.end())
    return p->second;
  return mDefaultSpeed;
}

bool IODB::hasClockSpeed(const STSymbolTableNode* node) const
{
  NameIntMap::const_iterator p =
    mClockSpeeds.find(const_cast<STSymbolTableNode*>(node));
  return (p != mClockSpeeds.end());
}

UInt32 IODB::getHighestClockSpeed() const
{
  UInt32 fastest = mDefaultSpeed;
  for (NameIntMap::UnsortedCLoop iter = mClockSpeeds.loopCUnsorted();
       ! iter.atEnd();
       ++iter) {
    UInt32 elem = iter.getValue();
    fastest = std::max(elem, fastest);
  }
  return fastest;
}

bool IODB::isObservable(const STSymbolTableNode* constNode) const
{
  STSymbolTableNode* node = const_cast<STSymbolTableNode*>(constNode);
  return mObservedNets.count(node) != 0;
}

bool IODB::isScObservable(const STSymbolTableNode* constNode) const
{
  STSymbolTableNode* node = const_cast<STSymbolTableNode*>(constNode);
  NameIntMap::const_iterator p = mObservedNets.find(node);
  if (p == mObservedNets.end()) {
    return false;
  }
  UInt32 flags = p->second;
  return flags & eOTSystemCMask;
}

bool IODB::isMvObservable(const STSymbolTableNode* constNode) const
{
  STSymbolTableNode* node = const_cast<STSymbolTableNode*>(constNode);
  NameIntMap::const_iterator p = mObservedNets.find(node);
  if (p == mObservedNets.end()) {
    return false;
  }
  UInt32 flags = p->second;
  return flags & eOTModelValidationMask;
}

bool IODB::isExposed(const STSymbolTableNode* node) const
{
  NameSet::const_iterator p = mExposedNets.find(const_cast<STSymbolTableNode*>(node));
  return (p != mExposedNets.end());
}

bool IODB::hasConstantOverride(const STSymbolTableNode* node) const
{
  NameValueMap::const_iterator p = mConstNets->find(const_cast<STSymbolTableNode*>(node));
  return (p != mConstNets->end());
}

bool IODB::isWrapperPort2State(const STSymbolTableNode* node) const
{
  NameSet::const_iterator p = mWrapPort2StateNets.find(const_cast<STSymbolTableNode*>(node));
  return (p != mWrapPort2StateNets.end());
}

bool IODB::isWrapperPort4State(const STSymbolTableNode* node) const
{
  NameSet::const_iterator p = mWrapPort4StateNets.find(const_cast<STSymbolTableNode*>(node));
  return (p != mWrapPort4StateNets.end());
}

bool IODB::isOnDemandIdleDeposit(const STSymbolTableNode* node) const
{
  NameSet::const_iterator p = mOnDemandIdleDepositNets.find(const_cast<STSymbolTableNode*>(node));
  return (p != mOnDemandIdleDepositNets.end());
}

bool IODB::isOnDemandExcluded(const STSymbolTableNode* node) const
{
  NameSet::const_iterator p = mOnDemandExcludedNets.find(const_cast<STSymbolTableNode*>(node));
  return (p != mOnDemandExcludedNets.end());
}

bool IODB::isTied(const STSymbolTableNode* node) const
{
  NameValueMap::const_iterator p = mTieNets->find(const_cast<STSymbolTableNode*>(node));
  return (p != mTieNets->end());
}

const DynBitVector* IODB::getTieValue(const STSymbolTableNode* node) const
{
  const DynBitVector* val = NULL;
  NameValueMap::const_iterator p = mTieNets->find(const_cast<STSymbolTableNode*>(node));
  if (p != mTieNets->end()) {
    val = p->second;
  }
  return val;
}

bool IODB::isAsync(const SCHSignature* signature) const 
{
  const SCHScheduleMask* transMask = signature->getTransitionMask();
  SCHScheduleMask::UnsortedEvents loop = transMask->loopEvents();
  bool hasTransInput = false;
  bool hasSampleOut = false;

  while (! hasTransInput && ! loop.atEnd())
  {
    const SCHEvent* event = *loop;
    hasTransInput = event->isPrimaryInput();
    ++loop;
  }

  if (hasTransInput)
    hasSampleOut = hasOutputEvent(signature);
  
  return (hasTransInput && hasSampleOut);
}

bool IODB::hasOutputEvent(const SCHSignature* signature) const 
{
  bool hasSampleOut = false;
  if (signature)
  {
    const SCHScheduleMask* sampleMask = signature->getSampleMask();
    
    
    for (SCHScheduleMask::UnsortedEvents loop = sampleMask->loopEvents();
         ! hasSampleOut && ! loop.atEnd(); ++loop)
    {
      const SCHEvent* event = *loop;
      hasSampleOut= event->isPrimaryOutput();
    }
  }
  return hasSampleOut;
}


bool IODB::isSync(const SCHSignature* signature) const 
{
  const SCHScheduleMask* transMask = signature->getTransitionMask();
  const SCHScheduleMask* sampleMask = signature->getSampleMask();
  SCHScheduleMask::UnsortedEvents loop = transMask->loopEvents();
  bool hasTransClk = false;
  bool hasSampleClk = false;
  
  while (! hasTransClk && ! loop.atEnd())
  {
    const SCHEvent* event = *loop;
    hasTransClk = event->isClockEvent();
    ++loop;
  }


  if (! hasTransClk)
  {
    loop = sampleMask->loopEvents();
    
    while (! hasSampleClk && ! loop.atEnd())
    {
      const SCHEvent* event = *loop;
      hasSampleClk = event->isClockEvent();
      ++loop;
    }
  }
  
  return (hasTransClk || hasSampleClk);
}

bool IODB::isModuleHidden(const STSymbolTableNode* moduleOrLeaf) const
{
  bool isHidden = false;
  const STBranchNode* module = NULL;
  if (moduleOrLeaf)
  {
    module = moduleOrLeaf->castBranch();
    if (! module)
      module = moduleOrLeaf->getParent();
    
    const STBranchNode* cur = module;
    while (! isHidden && (cur != NULL))
    {
      isHidden = (mHiddenModuleTops.find(const_cast<STBranchNode*>(cur)) != mHiddenModuleTops.end());
      cur = cur->getParent();
    }
  }
  
  return isHidden;
}

//! Is this hidden module net exposed via directives?
bool IODB::isNetExposed(const STAliasedLeafNode* leaf) const
{
  return (isDepositable(leaf) || isObservable(leaf) || 
          isForcible(leaf) || isExposed(leaf));
}

bool IODB::isNetHidden(const STAliasedLeafNode* leaf) const
{
  bool isHidden = isModuleHidden(leaf->getParent());
  if (isHidden)
  {
    // check to see if this is a depositable/observable or forcible
    // signal
    isHidden = ! isNetExposed(leaf);
  }
  
  return isHidden;
}

void IODB::putTargetCompiler(TargetCompiler tcomp)
{
  mDesignInfo->putTargetCompiler(tcomp);
}

void IODB::putTargetCompilerVersion(TargetCompilerVersion version)
{
  mDesignInfo->putTargetCompilerVersion(version);
}

TargetCompiler IODB::getTargetCompiler() const
{
  return mDesignInfo->getTargetCompiler();
}

TargetCompilerVersion IODB::getTargetCompilerVersion() const
{
  return mDesignInfo->getTargetCompilerVersion();
}

void IODB::putNoInputFlow(bool noInputFlowSpecified)
{
  mDesignInfo->putNoInputFlow(noInputFlowSpecified);
}

bool IODB::isNoInputFlow() const
{
  return mDesignInfo->isNoInputFlow();
}

UInt32 IODB::numTotalBidis() const
{
  return mDesignInfo->numBidirects();
}

void IODB::putIfaceTag(const UtString& ifaceTag)
{
  mDesignInfo->putIfaceTag(ifaceTag);
}

void IODB::putDesignId(const UtString& designId)
{
  mDesignInfo->putDesignId(designId);
}

const char* IODB::getDesignId() const
{
  return mDesignInfo->getDesignId();
}

const char* IODB::getIfaceTag() const
{
  return mDesignInfo->getIfaceTag();
}

const char* IODB::getTargetFlags() const
{
  return mDesignInfo->getTargetFlags();
}

void IODB::putTopLevelModuleName(const char* topModName)
{
  mDesignInfo->putTopLevelModuleName(topModName);
}

void IODB::putSystemCModuleName(const char* sysCName)
{
  mDesignInfo->putSystemCModuleName(sysCName);
}

const char* IODB::getTopLevelModuleName() const
{
  return mDesignInfo->getTopLevelModuleName();
}

const char* IODB::getSystemCModuleName() const
{
  return mDesignInfo->getSystemCModuleName();
}

void IODB::putTargetFlags (const UtString& targetFlags)
{
  mDesignInfo->putTargetFlags(targetFlags);
}

void IODB::setVHMTypeFlags(CarbonVHMTypeFlags vhmType)
{
  mDesignInfo->setVHMTypeFlags(vhmType);
}

void IODB::clearVHMTypeFlags(CarbonVHMTypeFlags vhmType)
{
  mDesignInfo->clearVHMTypeFlags(vhmType);
}

CarbonVHMTypeFlags IODB::getVHMTypeFlags() const
{
  return mDesignInfo->getVHMTypeFlags();
}

bool IODB::allowForceSubordinates() const
{
  return mDesignInfo->getVersion() >= 2;
}

void IODB::putIsPortInterfaceGenerated(bool isGenerated)
{
  mDesignInfo->putIsPortInterfaceGenerated(isGenerated);
}

bool IODB::isPortInterfaceGenerated() const
{
  return mDesignInfo->isPortInterfaceGenerated();
}

void IODB::putIsClockGlitchGenerated(bool isGenerated)
{
  mDesignInfo->putIsClockGlitchGenerated(isGenerated);
}

bool IODB::isClockGlitchGenerated() const
{
  return mDesignInfo->isClockGlitchGenerated();
}

const DynBitVector* IODB::getConstNetBitMask(const STAliasedLeafNode* node) const
{
  const DynBitVector* ret = NULL;
  NameValueMap::const_iterator p = mConstNets->find(node->getStorage());
  if (p != mConstNets->end())
    ret = p->second;
  return ret;
}

void IODB::composeName(UtString* name, const STSymbolTableNode* symNode) const
{
  const HdlHierPath* pather = getDesignSymbolTable()->getHdlHier();
  pather->compPathHier(symNode, name);
}

bool IODB::sIsOverrideMaskAllOnes(const DynBitVector* overrideMaskBV, 
                                  UInt32 width)
{
  bool isTotalOverride = true;
  const UInt32* overrideMask = overrideMaskBV->getUIntArray();
  
  UInt32 numWords = (width + 31)/32;
  UInt32 lastMask = CarbonValRW::getWordMask(width);
  for (size_t i = 0; (i < numWords - 1) && isTotalOverride; ++i)
    isTotalOverride = (overrideMask[i] == UtUINT32_MAX);
  
  isTotalOverride = isTotalOverride && ((overrideMask[numWords - 1] & lastMask) == lastMask);

  return isTotalOverride;
}

const char* IODB::getEntryName(UtString* buf, DBEntry entryType) const
{
  buf->clear();
  *buf << "carbon_" << UInt32(entryType);
  return buf->c_str();
}

IODB::DesignInfo::DesignInfo() : mNumBidirects(0), 
                                 mVersion(0),
                                 mTargetCompiler(eNoCC), 
                                 mTargetCompilerVersion(eVanilla_Compiler),
                                 mDBType(eCarbonIODB),
                                 mVHMTypeFlags(eVHMNormal),
                                 mNoInputFlow(false),
                                 mPortIfaceGenerated(true),
                                 mClockGlitchGenerated(true)
{}

IODB::DesignInfo::~DesignInfo()
{}

void IODB::DesignInfo::putDesignId(const UtString& designId)
{
  
  mDesignId.assign(designId);
}

const char* IODB::DesignInfo::getDesignId() const
{
  return mDesignId.c_str();
}

void IODB::DesignInfo::putIfaceTag(const UtString& ifaceTag)
{
  mIfaceTag.assign(ifaceTag);
}

const char* IODB::DesignInfo::getIfaceTag() const
{
  return mIfaceTag.c_str();
}

void IODB::DesignInfo::putTopLevelModuleName(const char* topModName)
{
  mTopLevelModuleName.assign(topModName);
}

const char* IODB::DesignInfo::getTopLevelModuleName() const
{
  return mTopLevelModuleName.c_str();
}

void IODB::DesignInfo::putSystemCModuleName(const char* sysCName)
{
  mSystemCModuleName.assign(sysCName);
}

const char* IODB::DesignInfo::getSystemCModuleName() const
{
  return mSystemCModuleName.c_str();
}

void IODB::DesignInfo::setVHMTypeFlags(CarbonVHMTypeFlags vhmType)
{
  mVHMTypeFlags = static_cast<CarbonVHMTypeFlags>(mVHMTypeFlags | vhmType);
}

void IODB::DesignInfo::clearVHMTypeFlags(CarbonVHMTypeFlags vhmType)
{
  mVHMTypeFlags = static_cast<CarbonVHMTypeFlags>(mVHMTypeFlags & ~vhmType);
}

CarbonVHMTypeFlags IODB::DesignInfo::getVHMTypeFlags() const
{
  return mVHMTypeFlags;
}

UInt32 IODB::DesignInfo::getVersion() const
{
  return mVersion;
}

void IODB::DesignInfo::putNoInputFlow(bool noInputFlow)
{
  mNoInputFlow = noInputFlow;
}
    
bool IODB::DesignInfo::isNoInputFlow() const
{
  return mNoInputFlow;
}

void IODB::DesignInfo::putNumBidis(UInt32 numBidis)
{
  mNumBidirects = numBidis;
}
    
UInt32 IODB::DesignInfo::numBidirects() const
{
  return mNumBidirects;
}
    
void IODB::DesignInfo::putTargetCompiler(TargetCompiler targetCompiler)
{
  mTargetCompiler = targetCompiler;
}

TargetCompiler IODB::DesignInfo::getTargetCompiler() const
{
  return mTargetCompiler;
}

void IODB::DesignInfo::putTargetCompilerVersion(TargetCompilerVersion targetCompilerVersion)
{
  mTargetCompilerVersion = targetCompilerVersion;
}

TargetCompilerVersion IODB::DesignInfo::getTargetCompilerVersion() const
{
  return mTargetCompilerVersion;
}

void IODB::DesignInfo::putTargetFlags(const UtString& flags)
{
  mTargetFlags.assign(flags);
}  

const char* IODB::DesignInfo::getTargetFlags() const
{
  return mTargetFlags.c_str();
}

void IODB::DesignInfo::putDBType(CarbonDBType dbType)
{
  mDBType = dbType;
}

CarbonDBType IODB::DesignInfo::getDBType() const
{
  return mDBType;
}

void IODB::DesignInfo::putIsPortInterfaceGenerated(bool isGenerated)
{
  mPortIfaceGenerated = isGenerated;
}

bool IODB::DesignInfo::isPortInterfaceGenerated() const
{
  return mPortIfaceGenerated;
}

void IODB::DesignInfo::putIsClockGlitchGenerated(bool isGenerated)
{
  mClockGlitchGenerated = isGenerated;
}

bool IODB::DesignInfo::isClockGlitchGenerated() const
{
  return mClockGlitchGenerated;
}

bool IODB::DesignInfo::dbWrite(ZostreamDB& db) const
{
  db << cIODBDesignInfoSig;
  db << cIODBDesignInfoVersion;
  db << mNumBidirects;
  db << mDesignId;
  db << mTargetFlags;
  db << SInt32(mTargetCompiler);
  db << SInt32(mTargetCompilerVersion);
  db << SInt32(mDBType);
  db << UInt32(mNoInputFlow); 
  db << mIfaceTag;
  db << mTopLevelModuleName;
  db << mSystemCModuleName;
  db << UInt32(mVHMTypeFlags);
  db << UInt32(mPortIfaceGenerated);
  db << UInt32(mClockGlitchGenerated);
  return ! db.fail();
}

bool IODB::DesignInfo::dbRead(ZistreamDB& db)
{
  UtString signature;
  if (! (db >> signature))
    return false;

  if (strcmp(signature.c_str(), cIODBDesignInfoSig) != 0)
  {
    UtString buf;
    buf << "Invalid DesignInfo signature: " << signature;
    db.setError(buf.c_str());
    return false;
  }

  if (! (db >> mVersion))
    return false;
  
  if (mVersion > cIODBDesignInfoVersion)
  {
    UtString buf;
    buf << "Unsupported DesignInfo version: " << mVersion;
    db.setError(buf.c_str());
    return false;
  }

  db >> mNumBidirects;
  db >> mDesignId;
  db >> mTargetFlags;

  if (db.fail())
    return false;

  {
    SInt32 targCompCast;
    if (db >> targCompCast)
      mTargetCompiler = TargetCompiler(targCompCast);
  }

  if (mVersion < 3) {
    mTargetCompilerVersion = eVanilla_Compiler;
  } else {
    SInt32 targCompVersionCast;
    if (db >> targCompVersionCast)
      mTargetCompilerVersion = TargetCompilerVersion(targCompVersionCast);
  }

  {
    SInt32 dbTypeCast = 111111111;
    db >> dbTypeCast;
    mDBType = CarbonDBType(dbTypeCast);
  }
  
  {
    UInt32 nifCast = 0;
    if (db >> nifCast)
      mNoInputFlow = nifCast != 0;
  }
  
  if (mVersion > 0)
  {
    db >> mIfaceTag;
    db >> mTopLevelModuleName;
    db >> mSystemCModuleName;
  }

  if (mVersion > 1)
  {
    UInt32 vhmTypeCast;
    db >> vhmTypeCast;
    if (vhmTypeCast & ~eVHMAllTypes)  // check for illegal bits being set
    {
      UtString buf;
      buf << "Unsupported Carbon Model (VHM) Type: " << vhmTypeCast;
      db.setError(buf.c_str());
    }
    else 
      mVHMTypeFlags = static_cast<CarbonVHMTypeFlags>(vhmTypeCast);
  }
  
  if (mVersion > 4)
  {
    UInt32 portIfaceCast = 0;
    db >> portIfaceCast;
    mPortIfaceGenerated = static_cast<bool>(portIfaceCast);
  }
  if (mVersion > 5) {
    UInt32 clockGlitchCast = 0;
    db >> clockGlitchCast;
    mClockGlitchGenerated = static_cast<bool>(clockGlitchCast);
  }
  return ! db.fail();
}

UInt32 IODB::getIODBVersion() const {
  return mInputDBVersion;
}

UInt32 IODB::sGetSigAndVersion(UtString* signature)
{
  signature->assign(cIODBSig);
  return cIODBVersion;
}

STSymbolTableNode* IODB::NameMapToSetFunctor::operator()(NameIntValue value)
{
  return value.first;
}

STSymbolTableNode* IODB::NameMapToSetFunctor::operator()(NameIntValue value) const
{
  return value.first;
}

void IODB::addCapability(const char* capability)
{
  UtString str(capability);
  mCapabilities->insert(str);
}

bool IODB::testCapability(const char* capability) const
{
  UtString str(capability);
  return mCapabilities->find(str) != mCapabilities->end();
}

StringAtom* IODB::sGetForceValueName(const STAliasedLeafNode* net, AtomicCache* strCache)
{
  UtString buf("$shellnet_");
  buf << net->str() << "__force_value";
  return strCache->intern(buf.c_str());
}

StringAtom* IODB::sGetForceMaskName(const STAliasedLeafNode* net, AtomicCache* strCache)
{ 
  UtString buf("$shellnet_");
  buf << net->str() << "__force_mask";
  return strCache->intern(buf.c_str());
}

bool IODB::isUsedAsLiveInput(const STSymbolTableNode* node) const
{
  return mLiveInputDirNets.count(const_cast<STSymbolTableNode*>(node)) > 0;
}

bool IODB::isUsedAsLiveOutput(const STSymbolTableNode* node) const
{
  return mLiveOutputDirNets.count(const_cast<STSymbolTableNode*>(node)) > 0;
}

bool IODB::isPosedgeScheduleTrigger(const STSymbolTableNode* node) const
{
  return mPosedgeScheduleTriggers.count(const_cast<STSymbolTableNode*>(node)) > 0;
}

bool IODB::isNegedgeScheduleTrigger(const STSymbolTableNode* node) const
{
  return mNegedgeScheduleTriggers.count(const_cast<STSymbolTableNode*>(node)) > 0;
}

void IODB::computeBothEdgeScheduleTriggers(NameSet* bothEdges) const
{
  for (NameSet::UnsortedCLoop p = mPosedgeScheduleTriggers.loopCUnsorted();
       !p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    if (mNegedgeScheduleTriggers.count(node) > 0)
      bothEdges->insert(node);
  }
}

bool IODB::isScheduleTriggerUsedAsData(const STSymbolTableNode* node) const
{
  return mScheduleTriggersUsedAsData.count(const_cast<STSymbolTableNode*>(node)) > 0;
}

void IODB::putSoftwareVersion(const char* version)
{
  mSoftwareVersion = version;
}

const char* IODB::getSoftwareVersion() const
{
  return mSoftwareVersion.c_str();
}
