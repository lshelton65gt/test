// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "iodb/IODBNucleus.h"
#include "iodb/IODBDesignData.h"
#include "iodb/IODBUserTypes.h"
#include "nucleus/NUDesign.h"
#include "util/UtString.h"
#include "util/ArgProc.h"
#include <stdio.h>
#include "hdl/HdlVerilogPath.h"
#include "hdl/HdlId.h"
#include "hdl/HdlVerilogExpr.h"
#include "util/SourceLocator.h"
#include "util/UtWildcard.h"
#include "util/CbuildMsgContext.h"
#include "util/OSWrapper.h"
#include "util/AtomicCache.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "util/StringAtom.h"
#include "util/DynBitVector.h"
#include "util/Zstream.h"
#include "nucleus/NUNetSet.h"
#include "reduce/ReachableAliases.h"
#include "shell/OnDemandMgr.h"

#include "compiler_driver/CarbonContext.h"
#include "exprsynth/Expr.h"
#include "exprsynth/SymTabExpr.h"
#include "compiler_driver/CarbonDBWrite.h"

class IODBNucleus::MarkForceWalk : public CarbonExprWalker
{
public:
  MarkForceWalk(IODBNucleus* iodb) : mIODB(iodb)
  {}
  
  virtual ~MarkForceWalk() {}

  virtual void visitIdent(CarbonIdent* ident) 
  {
    DynBitVector usageMask;
    STAliasedLeafNode* leaf = ident->getNode(&usageMask);
    mIODB->gatherMarkForcible(leaf, true);
  }
  
private:
  IODBNucleus* mIODB;
};

class IODBNucleus::ForcibleExprBPWalk : public CarbonExprWalker
{
public:
  ForcibleExprBPWalk(IODBNucleus* iodb) : mIODB(iodb)
  {}
  
  virtual ~ForcibleExprBPWalk() {}

  virtual void visitIdent(CarbonIdent* ident) 
  {
    SymTabIdent* symTabIdent = ident->castSymTabIdent();
    CE_ASSERT(symTabIdent, ident);
    SymTabIdentBP* symTabIdentBP = symTabIdent->castSymTabIdentBP();
    
    if (symTabIdentBP)
    {
      ForcibleExprBPWalk walker(mIODB);
      CarbonExpr* expr = symTabIdentBP->getBackPointer();
      CE_ASSERT(expr, symTabIdentBP);
      walker.visitExpr(expr);
    }
    else
    {
      STAliasedLeafNode* leaf = symTabIdent->getNode();
      NUNet* net = NUNet::find(leaf);
      ST_ASSERT(net, leaf);
      const SourceLocator& loc = net->getLoc();
      mIODB->mapSymNodeToLoc(leaf, loc);
      mIODB->mForcedNets.insert(leaf);
    }
  }
  
private:
  IODBNucleus* mIODB;
};

// The initial file-parse is done before we parse the verilog files.
// That's needed so that message severity adjustments in the directives
// file can be processed before we emit any messages during file parsing.
// 
// But that means that we can't directly modify the design symbol table --
// that's empty during the initial directives parse.  We have to record
// any design names and re-process them after parsing the design.
bool IODBNucleus::parseFile(const char* filename, const char* unexpandedFileName)
{
  bool ret = true;
  ZISTREAM(infile, filename);
  if (!infile.is_open())
  {
    fprintf(stderr, "%s\n", infile.getError());
    return false;
  }
  if (mDirLogFile != NULL) {
    *mDirLogFile << "\n# " << ((unexpandedFileName) ? unexpandedFileName : filename) << "\n";
  }
  bool saveParsingDirectivesFile = mParsingDirectivesFile;
  mParsingDirectivesFile = true;

  // Context for parsing and storing c-model information
  UtString line, nextLine;
  mLineNumber = 0;
  mFilename = filename;
  mCModelContext = new CModelContext;
  while (infile.readline(&nextLine) != 0)
  {
    if (mDirLogFile != NULL) {
      *mDirLogFile << nextLine;
    }
    ++mLineNumber;
    int nextLen = nextLine.size();
    if ((nextLen > 1)
        && (nextLine[nextLen - 2] == '\\')
        && (nextLine[nextLen - 1] == '\n'))
    {
      line.append(nextLine.c_str(), nextLen - 2); // continuation line
      line << ' ';
    }
    else
    {
      line << nextLine;
      SourceLocator loc = mSourceLocatorFactory->create(mFilename.c_str (), mLineNumber);
      StrToken tok(line.c_str());
      const char* keyword;
      if (tok(&keyword))
      {
        // Note if you change or add directives, make sure the
        // dirToString is updated as well.
        ret &= parseDirective(keyword, tok.curPos(), tok, loc);
      } // if
      line.clear();
    } // else
  } // while

  // Module directives are processed immediately after parsing
  delete mCModelContext;
  mCModelContext = NULL;
  if (!infile.close())
  {
    fprintf(stderr, "%s\n", infile.getError());
    ret = false;
  }

  mParsingDirectivesFile = saveParsingDirectivesFile;

  return ret;
} // bool IODBNucleus::parseFile

bool IODBNucleus::parseDirective(const char* keyword, const char* rest,
                                 StrToken& tok, const SourceLocator& loc)
{
  bool ret = false;

  // If this was called while not parsing a directives file, then separately
  // log the file/line and directive text into libdesign.dir
  if (!mParsingDirectivesFile && (mDirLogFile != NULL)) {
    const char* filename = loc.getFile();
    if (strcmp(mDirLogPrevFile.c_str(), filename) != 0) {
      mDirLogPrevFile = filename;
      *mDirLogFile << "\n";     // skip lines in long when switching files
    }
    *mDirLogFile << "# " << filename << ":" << loc.getLine() << "\n";
    *mDirLogFile << keyword << " " << rest << "\n";
  }

  if ((keyword[0] == '#') || (strncmp(keyword, "//", 2) == 0))
    ret = true;                       // skip comment
  else if (strcmp(keyword, "fastClock") == 0)
    ret = addDesignDirective(IODBDirective::eFastClock, rest, loc);
  else if (strcmp(keyword, "slowClock") == 0)
    ret = addDesignDirective(IODBDirective::eSlowClock, rest, loc);
  else if (strcmp(keyword, "fastReset") == 0)
    ret = addDesignDirective(IODBDirective::eFastReset, rest, loc);
  else if (strcmp(keyword, "clockSpeed") == 0)
    ret = addValueDirective(IODBDirective::eClockSpeed, tok, loc);
  else if (strcmp(keyword, "defaultClockSpeed") == 0)
    ret = adjustDefault(&mDefaultSpeed, &tok, loc);
  else if (strcmp(keyword, "observeSignal") == 0)
    ret = addDesignDirective(IODBDirective::eObserve, rest, loc);
  else if (strcmp(keyword, "traceSignal") == 0)
    ret = addDesignDirective(IODBDirective::eTraceSignal, rest, loc);
  else if (strcmp(keyword, "scObserveSignal") == 0)
    ret = addDesignDirective(IODBDirective::eScObserve, rest, loc);
  else if (strcmp(keyword, "mvObserveSignal") == 0)
    ret = addDesignDirective(IODBDirective::eMvObserve, rest, loc);
  else if (strcmp(keyword, "forceSignal") == 0)
    ret = addDesignDirective(IODBDirective::eForce, rest, loc);
  else if (strcmp(keyword, "asyncReset") == 0)
    ret = addDesignDirective(IODBDirective::eAsyncReset, rest, loc);
  else if (strcmp(keyword, "exposeSignal") == 0)
    ret = addDesignDirective(IODBDirective::eExpose, rest, loc);
  else if (strcmp(keyword, "tieNet") == 0)
    ret = addValueDirective(IODBDirective::eTieNet, tok, loc);
  else if (strcmp(keyword, "depositSignal") == 0)
    ret = addDesignDirective(IODBDirective::eDeposit, rest, loc);
  else if (strcmp(keyword, "depositSignalFrequent") == 0)
    ret = addDesignDirective(IODBDirective::eDepositFrequent, rest, loc);
  else if (strcmp(keyword, "depositSignalInfrequent") == 0)
    ret = addDesignDirective(IODBDirective::eDepositInfrequent, rest, loc);
  else if (strcmp(keyword, "scDepositSignal") == 0)
    ret = addDesignDirective(IODBDirective::eScDeposit, rest, loc);
  else if (strcmp(keyword, "mvDepositSignal") == 0)
    ret = addDesignDirective(IODBDirective::eMvDeposit, rest, loc);
  else if (strcmp(keyword, "blastNet") == 0)
    ret = addDesignDirective(IODBDirective::eBlast, rest, loc);
  else if (strcmp(keyword, "vectorMatch") == 0)
    ret = addDesignDirective(IODBDirective::eVectorMatch, rest, loc);
  else if (strcmp(keyword, "ignoreOutput") == 0)
    ret = addDesignDirective(IODBDirective::eIgnoreOutput, rest, loc);
  else if (strcmp(keyword, "collapseClock") == 0)
    ret = addDesignDirective(IODBDirective::eCollapseClocks, rest, loc);
  else if (strcmp(keyword, "errorMsg") == 0)
    ret = adjustSeverity(MsgContextBase::eError, &tok, loc);
  else if (strcmp(keyword, "warningMsg") == 0)
    ret = adjustSeverity(MsgContextBase::eWarning, &tok, loc);
  else if (strcmp(keyword, "infoMsg") == 0)
    ret = adjustSeverity(MsgContextBase::eNote, &tok, loc);
  else if (strcmp(keyword, "silentMsg") == 0)
    ret = adjustSeverity(MsgContextBase::eSuppress, &tok, loc);
  else if (strcmp(keyword, "input") == 0)
    ret = addDesignDirective(IODBDirective::eInputDir, rest, loc);
  else if (strcmp(keyword, "cModuleBegin") == 0)
    ret = addCModelDirective(IODBDirective::eCModuleBegin, tok, loc);
  else if (strcmp(keyword, "cPortBegin") == 0)
    ret = addCModelDirective(IODBDirective::eCPortBegin, tok, loc);
  else if (strcmp(keyword, "cNullPortBegin") == 0)
    ret = addCModelDirective(IODBDirective::eCNullPortBegin, tok, loc);
  else if (strcmp(keyword, "cTiming") == 0)
    ret = addCModelDirective(IODBDirective::eCTiming, tok, loc);
  else if (strcmp(keyword, "cFanin") == 0)
    ret = addCModelDirective(IODBDirective::eCFanin, tok, loc);
  else if (strcmp(keyword, "cPortEnd") == 0)
    ret = addCModelDirective(IODBDirective::eCPortEnd, tok, loc);
  else if (strcmp(keyword, "cNullPortEnd") == 0)
    ret = addCModelDirective(IODBDirective::eCNullPortEnd, tok, loc);
  else if (strcmp(keyword, "cModuleEnd") == 0)
    ret = addCModelDirective(IODBDirective::eCModuleEnd, tok, loc);
  else if (strcmp(keyword, "hideModule") == 0)
    ret = addModuleDirective(rest, loc, IODBDirective::eHideModule);
  else if (strcmp(keyword, "noAsyncResets") == 0)
    ret = addModuleDirective(rest, loc, IODBDirective::eNoAsyncReset);
  else if (strcmp(keyword, "ternaryGateOptimization") == 0)
    ret = addModuleDirective(rest, loc, IODBDirective::eTernaryGateOptimization);
  else if (strcmp(keyword, "flattenModule") == 0)
    ret = addModuleDirective(rest, loc, IODBDirective::eFlattenModule);
  else if (strcmp(keyword, "flattenModuleContents") == 0)
    ret = addModuleDirective(rest, loc, IODBDirective::eFlattenModuleContents);
  else if (strcmp(keyword, "allowFlattening") == 0)
    ret = addModuleDirective(rest, loc, IODBDirective::eAllowFlattening);
  else if (strcmp(keyword, "disallowFlattening") == 0)
    ret = addModuleDirective(rest, loc, IODBDirective::eDisallowFlattening);
  else if (strcmp(keyword, "disableOutputSysTasks") == 0)
    ret = addModuleDirective(rest, loc, IODBDirective::eDisableOutputSysTasks);
  else if (strcmp(keyword, "enableOutputSysTasks") == 0)
    ret = addModuleDirective(rest, loc, IODBDirective::eEnableOutputSysTasks);
  else if (strcmp(keyword, "substituteModule") == 0)
    ret = addSubstituteModule(tok, loc, IODBNucleus::VERILOG);
  else if (strcmp(keyword, "substituteEntity") == 0)
    ret = addSubstituteModule(tok, loc, IODBNucleus::VHDL);
  else if (strcmp(keyword, "inline") == 0)
    ret = addInlineTFTok(rest,loc);
#if 0
  // Removed for now, will be added later when we have time to do di
  else if (strcmp(keyword, "cFunctionBegin") == 0)
    ret = addCModelDirective(IODBDirective::eCFunctionBegin, tok, loc);
  else if (strcmp(keyword, "cFunctionEnd") == 0)
    ret = addCModelDirective(IODBDirective::eCFunctionEnd, tok, loc);
#endif
  else if (strcmp(keyword, "cTaskBegin") == 0)
    ret = addCModelDirective(IODBDirective::eCTaskBegin, tok, loc);
  else if (strcmp(keyword, "cTaskEnd") == 0)
    ret = addCModelDirective(IODBDirective::eCTaskEnd, tok, loc);
  else if (strcmp(keyword, "cArg") == 0)
    ret = addCModelDirective(IODBDirective::eCArg, tok, loc);
  else if (strcmp(keyword, "cHasSideEffect") == 0)
    ret = addCModelDirective(IODBDirective::eCHasSideEffect, tok, loc);
  else if (strcmp(keyword, "cCallOnPlayback") == 0)
    ret = addCModelDirective(IODBDirective::eCCallOnPlayback, tok, loc);
  else if (strcmp(keyword, "outputsTiming") == 0)
    ret = addEventDirective(IODBDirective::eOutputsTiming, tok, loc);
  else if (strcmp(keyword, "inputsTiming") == 0)
    ret = addEventDirective(IODBDirective::eInputsTiming, tok, loc);
  else if (strcmp(keyword, "wrapperPort2State") == 0)
    ret = addDesignDirective(IODBDirective::eWrapperPort2State, rest, loc);
  else if (strcmp(keyword, "wrapperPort4State") == 0)
    ret = addDesignDirective(IODBDirective::eWrapperPort4State, rest, loc);
  else if (strcmp(keyword, "delayedClock") == 0) {
    ret = addDesignDirective(IODBDirective::eDelayedClock, rest, loc);
  } else if (strcmp(keyword, "onDemandIdleDeposit") == 0) {
    ret = addDesignDirective(IODBDirective::eOnDemandIdleDeposit, rest, loc);
  } else if (strcmp(keyword, "onDemandExcluded") == 0) {
    ret = addDesignDirective(IODBDirective::eOnDemandExcluded, rest, loc);
  } else if (strcmp(keyword, "ignoreSynthCheck") == 0) {
    ret = addDesignDirective(IODBDirective::eIgnoreSynthCheck, rest, loc);
  } else
  {
    mMsgContext->DirInvalid(&loc, keyword);
  }
  return ret;
} // bool IODBNucleus::parseDirective

STSymbolTableNode* IODBNucleus::elabNode(StringAtom* name, NUBase* obj,
                                         STBranchNode* parent, bool isLeaf)
{
  STSymbolTableNode* node = mParseTable->find(parent, name);
  if (node == NULL)
  {
    node = mParseTable->createNode(name, parent, isLeaf);
    SymTabVal* data = SymTabBOM::castBOM(node->getBOMData());
    data->mNucleusObj = obj;
  }
  return node;
}

bool 
IODBNucleus::NameSetMapCallback::recordPaths(IODBDesignDirective *dir,
                                            const NameSet& nodes,
                                            ProtectedMode protectedMode)
{
  if (nodes.size() != 1) {
    SourceLocator loc = dir->getLocation ();
    getMsgContext()->DirNeed1(&loc, nodes.size());
  } else {
    // Check if we have processed this directive before, if not create it
    STSymbolTableNode* key = *nodes.begin();
    NameSetMap::iterator p = mNameMap->find(key);
    NameSet* names = NULL;
    if (p == mNameMap->end()) {
      names = new NameSet;
      (*mNameMap)[key] = names;
    } else {
      names = p->second;
    }

    // Process the name associations
    return mIODB->recordPaths(dir, names, protectedMode, true);
  }
  return true;
}

bool
IODBNucleus::NameIntMapCallback::recordPaths(IODBDesignDirective *dir, const NameSet& nodes, 
                                             ProtectedMode)
{
  for (NameSet::UnsortedCLoop l = nodes.loopCUnsorted(); !l.atEnd(); ++l) {
  // If we haven't computed the directive value, do so now
    STSymbolTableNode* key = *l;
    if (!mNumberComputed) {
      const char* numericStr = dir->getValue ().c_str();
      if (!StringUtil::parseNumber(numericStr, &mNumber)) {
        SourceLocator loc = dir->getLocation ();
        getMsgContext()->DirInvalidNumber(&loc, numericStr);
      }
      mNumberComputed = true;
    }

    // Record the association
    (*mNameIntMap)[key] = mNumber;
  }
  return true;
}

bool
IODBNucleus::DepositMapCallback::recordPaths(IODBDesignDirective *dir, const NameSet& nodes,
                                             ProtectedMode)
{
  for (NameSet::UnsortedCLoop l = nodes.loopCUnsorted(); !l.atEnd(); ++l) {
    // Make sure we don't have a frequent/infrequent conflict
    STSymbolTableNode* key = *l;
    UInt32 mask = (*mNameIntMap)[key];
    if (((mDepositType == eDTFrequent) && ((mask & eDTInfrequentMask) != 0)) ||
        ((mDepositType == eDTInfrequent) && ((mask & eDTFrequentMask) != 0))) {
      // Print the warning and ignore the directive
      UtString name;
      key->compose(&name);
      const char* origDir;
      const char* conflDir;
      if (mDepositType == eDTFrequent) {
        origDir = "depositSignalInfrequent";
        conflDir = "depositSignalFrequent";
      } else {
        origDir = "depositSignalFrequent";
        conflDir = "depositSignalInfrequent";
      }
      SourceLocator loc = dir->getLocation ();
      getMsgContext()->ConflDepDir(&loc, name.c_str(), origDir, conflDir);
    } else {
      // Apply the directive
      (*mNameIntMap)[key] |= (1 << mDepositType);
    }    
  }
  return true;
}

bool
IODBNucleus::ObserveMapCallback::recordPaths(IODBDesignDirective *, const NameSet& nodes,
                                             ProtectedMode)
{
  for (NameSet::UnsortedCLoop l = nodes.loopCUnsorted(); !l.atEnd(); ++l) {
    // Apply the directive, there are no conflicts currently
    STSymbolTableNode* key = *l;
    (*mNameIntMap)[key] |= (1 << mObserveType);
  }
  return true;
}

bool IODBNucleus::evaluateDesignDirectives(NUDesign* design)
{
  bool ret = true;

  mDesign = design;
  // Unconditionally populate the parse symbol table with the roots
  INFO_ASSERT(mParseTable == NULL, "Design directives have already been evaluated.");  // I should only run once
  mParseTable = new STSymbolTable(&mSymTabBOM, mAtomicCache);
  mParseIOTable = new IOTable;
  for (NUDesign::ModuleLoop m = design->loopTopLevelModules(); !m.atEnd(); ++m)
    elabNode((*m)->getOriginalName(), *m, NULL, false);

  for (IODBDesignDirective::List::iterator it = mDesignDirectives.begin ();
       it != mDesignDirectives.end (); it++) {
    IODBDesignDirective* dir = *it;
    switch (dir->getDirective ())
    {
      case IODBDirective::eFastClock:
        ret &= recordPaths(dir, &mFastClocks, eProtectedObservable);
        break;
      case IODBDirective::eSlowClock:
        ret &= recordPaths(dir, &mSlowClocks, eProtectedObservable);
        break;
      case IODBDirective::eFastReset:
        ret &= recordPaths(dir, &mFastResets, eProtectedObservable);
        break;
      case IODBDirective::eClockSpeed:
      {
        NameIntMapCallback callback(&mClockSpeeds, mMsgContext);
        ret &= recordAssocPaths(dir, eProtectedObservable, &callback);
        break;
      }
      case IODBDirective::eScObserve:
      {
        ObserveMapCallback callback(&mObservedNets, eOTSystemC, mMsgContext);
        ret &= recordAssocPaths(dir, eProtectedObservable, &callback);
        break;
      }
      case IODBDirective::eMvObserve:
      {
        ObserveMapCallback callback(&mObservedNets, eOTModelValidation,
                                    mMsgContext);
        ret &= recordAssocPaths(dir, eProtectedObservable, &callback);
        break;
      }
      case IODBDirective::eObserve:
      {
        ObserveMapCallback callback(&mObservedNets, eOTSimple, mMsgContext);
        ret &= recordAssocPaths(dir, eProtectedObservable, &callback);
        break;
      }
      case IODBDirective::eTraceSignal:
        ret &= recordPaths(dir, &mTracedNets, eProtectedUnknown);
        break;
      case IODBDirective::eForce:
        ret &= recordPaths(dir, &mForcedNets, eProtectedMutable);
        break;
      case IODBDirective::eAsyncReset:
        ret &= recordPaths(dir, &mAsyncResetPaths, eProtectedUnknown);
        break;
      case IODBDirective::eExpose:
        ret &= recordPaths(dir, &mExposedNets, eProtectedUnknown);
        break;
      case IODBDirective::eDeposit:
      {
        DepositMapCallback callback(&mDepositNets, eDTSimple, mMsgContext);
        ret &= recordAssocPaths(dir, eProtectedMutable, &callback);
        break;
      }
      case IODBDirective::eDepositFrequent:
      {
        DepositMapCallback callback(&mDepositNets, eDTFrequent, mMsgContext);
        ret &= recordAssocPaths(dir, eProtectedMutable, &callback);
        break;
      }
      case IODBDirective::eDepositInfrequent:
      {
        DepositMapCallback callback(&mDepositNets, eDTInfrequent, mMsgContext);
        ret &= recordAssocPaths(dir, eProtectedMutable, &callback);
        break;
      }
      case IODBDirective::eScDeposit:
      {
        DepositMapCallback callback(&mDepositNets, eDTSystemC, mMsgContext);
        ret &= recordAssocPaths(dir, eProtectedMutable, &callback);
        break;
      }
      case IODBDirective::eMvDeposit:
      {
        DepositMapCallback callback(&mDepositNets, eDTModelValidation, mMsgContext);
        ret &= recordAssocPaths(dir, eProtectedMutable, &callback);
        break;
      }
      case IODBDirective::eIgnoreOutput:
        ret &= recordPaths(dir, &mIgnoreOutputNets, eProtectedUnknown);
        break;
      case IODBDirective::eCollapseClocks:
      {
        NameSetMapCallback callback(this, mMsgContext, &mCollapsedClocks);
        ret &= recordAssocPaths(dir, eProtectedObservable, &callback, true);
        break;
      }
      case IODBDirective::eInputDir:
        ret &= recordPaths(dir, &mInputNets, eProtectedUnknown);
        break;
      case IODBDirective::eTieNet:
        // Note that tie nets have to mark the net protected
        // observable because we could optimize the entire module away
        // and then writing the tie net directive to the db would
        // fail. See the test/constprop/tienet4.v for an example.
        ret &= recordPathValue(dir, mTieNets, eProtectedObservable, false,
                               IODBDirective::eDirModule);
        break;

      case IODBDirective::eBlast:
        // blast nets are unelaborated names and may not persist to elaboration.
        break;

      case IODBDirective::eOutputsTiming:
        ret &= recordEventPaths(dir, mOutputsTimingEvents, eProtectedUnknown);
        break;

      case IODBDirective::eInputsTiming:
        ret &= recordEventPaths(dir, mInputsTimingEvents, eProtectedUnknown);
        break;

      case IODBDirective::eVectorMatch:
        ret &= recordWildcard(dir, &mVectorMatches);
        break;

      case IODBDirective::eWrapperPort2State:
        ret &= recordPaths(dir, &mWrapPort2StateNets, eProtectedUnknown);
        break;

      case IODBDirective::eWrapperPort4State:
        ret &= recordPaths(dir, &mWrapPort4StateNets, eProtectedUnknown);
        break;

      case IODBDirective::eDelayedClock:
        ret &= recordPaths(dir, &mDelayedClocks, eProtectedMutable);
        break;

      case IODBDirective::eOnDemandIdleDeposit:
        ret &= recordPaths(dir, &mOnDemandIdleDepositNets, eProtectedUnknown);
        // onDemandIdleDeposit implies carbonDeposit.  Ensure all
        // these nets are depositable, too.  A special enumerated type
        // is used to indicate that this is an implicit deposit.  This
        // is important, because the idle deposit net may be a primary
        // input, in which case the deposit will later be removed.
        // Normally, a warning is issued that the superfluous deposit
        // is being removed, but we don't want that to happen here.
        {
          DepositMapCallback callback(&mDepositNets, eDTODIdleDeposit, mMsgContext);
          ret &= recordAssocPaths(dir, eProtectedMutable, &callback);
        }
        break;

      case IODBDirective::eOnDemandExcluded:
        ret &= recordPaths(dir, &mOnDemandExcludedNets, eProtectedObservable);
        break;

      case IODBDirective::eIgnoreSynthCheck:
        ret &= recordPaths(dir, &mIgnoreSynthCheck, eProtectedUnknown);
        break;

      case IODBDirective::eCModuleBegin:
      case IODBDirective::eCPortBegin:
      case IODBDirective::eCNullPortBegin:
      case IODBDirective::eCTiming:
      case IODBDirective::eCFanin:
      case IODBDirective::eCPortEnd:
      case IODBDirective::eCNullPortEnd:
      case IODBDirective::eCModuleEnd:
      case IODBDirective::eCFunctionBegin:
      case IODBDirective::eCFunctionEnd:
      case IODBDirective::eCTaskBegin:
      case IODBDirective::eCTaskEnd:
      case IODBDirective::eCArg:
      case IODBDirective::eCHasSideEffect:
      case IODBDirective::eCCallOnPlayback:
      case IODBDirective::eSubstituteModule:
      case IODBDirective::eSubstituteEntity:
        // Handled somwhere else
        INFO_ASSERT(0, "unexpected cmodel directive");
        break;
    }
  } // for

  // Validate that all cmodule directives were processed
  for (CModules::iterator m = mCModules->begin(); m != mCModules->end(); ++m)
  {
    CModuleData* data = m->second;
    if (!data->mTouched)
    {
      const UtString* modName = m->first;
      mMsgContext->DirNoSuchCmod(&data->mLoc, "Module", modName->c_str());
    }
  }
  for (CTFs::iterator m = mCTFs->begin(); m != mCTFs->end(); ++m)
  {
    CTFData* data = m->second;
    if (!data->mTouched)
    {
      const UtString* tfName = m->first;
      mMsgContext->DirNoSuchCmod(&data->mLoc, "UDTF", tfName->c_str());
    }
  }

  // Check that all module directives reference valid module names.
  checkModuleDirectives();

  // Mark nets referenced in asyncReset directives
  markAsyncResets();

  // Check that tf directives reference valid module/task names.
  checkTFDirectives();

  // Add any implicit directives needed for the model
  addImplicitDirectives();

  // Sanity check that population optimized nets are not protected mutable.
  // We want to avoid optimizing away protected mutable nets since they could
  // be modified at runtime. For now, the only optimization during population
  // that can optimize a net away is constant propagation.
  // allow alerts to demote to warnings.  Note that the
  // CarbonContext phase will not advance if there were
  // errors or alerts reported, so the return-value here 
  // was superfluous
  return sanityCheckPplOptNets();

} // bool IODBNucleus::evalDesignDirectives

bool IODBNucleus::sanityCheckPplOptNets()
{
  bool checkPassed = true;
  for (NUNetSetIter itr = mPplOptNets.begin(); itr != mPplOptNets.end(); ++itr)
  {
    const NUNet* net = *itr;
    // Is net optimized by population a protected mutable net?
    if (isProtectedMutable(net)) {
      // Sanity check failed. Population phase optimized away a net that is
      // marked as protected mutable by the user.
      checkPassed = false;
      UtString netUnelabName;
      net->composeUnelaboratedName(&netUnelabName);
      mMsgContext->PplOptSanityCheckFailed(&(net->getLoc()), netUnelabName.c_str());
    }
  }

#ifdef PRINT_PPL_OPT_STATS
  // Print population optimization statistics.
  {
    UInt32 modNets = 0;
    UInt32 taskNets = 0;
    UInt32 declScopeNets = 0;
    UInt32 blockNets = 0;
    UInt32 totalNets = 0;
    NUScopeSet modSet;
    NUScopeSet taskSet;
    NUScopeSet declScopeSet;
    NUScopeSet blockSet;
    for (NUNetSetIter itr = mPplOptNets.begin(); itr != mPplOptNets.end(); ++itr)
    {
      const NUNet* net = *itr;
      ++totalNets;
      NUScope* netScope = net->getScope();
      if (netScope != NULL)
      {
        switch (netScope->getScopeType())
        {
        case NUScope::eBlock:
          ++blockNets;
          blockSet.insert(netScope);
          break;
        case NUScope::eModule:
          ++modNets;
          modSet.insert(netScope);
          break;
        case NUScope::eTask:
        case NUScope::eUserTask:
          ++taskNets;
          taskSet.insert(netScope);
          break;
        case NUScope::eNamedDeclarationScope:
          ++declScopeNets;
          declScopeSet.insert(netScope);
          break;
        }
      }
    }
    {
      UtOBStream outFile("/home/cds/vrishal/const_prop.stats", "a");
      {
        UtString testPath;
        OSGetCurrentDir(&testPath);
        outFile << "\nTestcase at path " << testPath << "\n";
      }
      outFile << "has " << totalNets << " nets optimized during population. Out of these:\n";
      outFile << blockSet.size() << " block scopes have " << blockNets << ",\n";
      outFile << modSet.size() << " module scopes have " << modNets << ",\n";
      outFile << taskSet.size() << " task scopes have " << taskNets << ",\n";
      outFile << declScopeSet.size() << " named decl scopes have " << declScopeNets << ",\n";
      outFile << " nets optimized during population.\n\n";
    }
  } // // Print population optimization statistics.
#endif

  mPplOptNets.clear();
  return checkPassed;
}

void IODBNucleus::addPopulationOptimizedNets(NUNetSet& optimizedNets)
{
  // Save the net optimized away by population.
  mPplOptNets.insertSet(optimizedNets);
}

void IODBNucleus::findMatchingModules(NUModuleList* modules,
                                      const char* modName)
{
  UtWildcard* wildcard = NULL;

  bool allowWild = true;
  for (NUDesign::ModuleLoop m = mDesign->loopAllModules(); !m.atEnd(); ++m)
  {
    NUModule* mod = *m;
    const char* modStr = mod->getName()->str();
    const char* origStr = mod->getOriginalName()->str();
    if ((strcmp(modName, modStr) == 0) ||
        (strcmp(modName, origStr) == 0))
    {
      // If we have an exact match, then throw out any any wildcard 
      // matches, but keep processing, because uselib and parameters
      // can create multiple distinct NUModule* with the same original name
      if (allowWild) {
        modules->clear();
        allowWild = false;
      }
      modules->push_back(mod);
    }
    else if (allowWild) {
      if (wildcard == NULL) {
        wildcard = new UtWildcard(modName, /* honour_escaped_identifier = */ true);
      }
      if (wildcard->isMatch(modStr) || wildcard->isMatch(origStr)) {
        modules->push_back(mod);
      }
    }
  }

  if (wildcard != NULL) {
    delete wildcard;
  }
}


void IODBNucleus::findMatchingTasks(NUModule * module,
                                    const char * taskNameToMatch,
                                    NUTaskList * tasks)
{
  UtWildcard * wildcard = NULL;

  for (NUTaskLoop loop = module->loopTasks();
       not loop.atEnd();
       ++loop) {
    NUTask * task = *loop;
    // This does not take into account any hierarchy in the task path..
    StringAtom * taskAtom = task->getOriginalName();
    const char * taskName = taskAtom->str();

    if(strcasecmp(taskName, taskNameToMatch) == 0) 
    {
      // Exact match. Throw out any wildcard matches and stop processing.
      tasks->clear();
      tasks->push_back(task);
      break;
    } 
    else 
    {
      if (wildcard==NULL) {
        // allocate wildcard as needed.
        wildcard = new UtWildcard(taskNameToMatch, /* honour_escaped_identifier = */ true);
      }
      if (wildcard->isMatch(taskName)) {
        tasks->push_back(task);
      }
    }
  }

  if (wildcard) {
    delete wildcard;
  }
}


bool IODBNucleus::buildRoots(IODBDirective::DesignDirective dir,
                             const UtStringArray& elemList,
                             const SourceLocator& loc,
                             NameSet* nodes,
                             bool findModules,
			     ProtectedMode protectedMode,
                             IODBDirective::DirectiveMode dirMode)
{
  // For module-only mode, make used to make sure we had exactly two
  // elements in the list, but with named declaration scopes, a module-based
  // net specification may have any arbitrary number of elements.

  // Find the top-level module associated with the first element
  LOC_ASSERT(!elemList.empty(), loc);
  const UtString& buf = elemList[0];
  const char* elem0 = buf.c_str();
  StringAtom* rootName = mAtomicCache->intern(elem0, buf.size());
  STSymbolTableNode* node = mParseTable->find(NULL, rootName);
  bool inserted = false;

  if (node != NULL) {
    inserted = buildNodes(dir, node->castBranch(), 1,
                          elemList, loc, nodes, 
			  findModules, false, protectedMode);
  } else {
    UtWildcard wild(elem0);
    bool findAltTop = (dirMode != IODBDirective::eDirInstance);

    // If this is a hierarchical path, then the path has to come
    // from a design root (there is currently only one of those
    // but this is coded to accommodate multiple roots)
    if (elemList.size() != 2) {
      UtList<STSymbolTableNode*> matches;
      sGetWildcardMatchingChildren(NULL, mParseTable, elem0, &matches);
      for (UtList<STSymbolTableNode*>::iterator itr = matches.begin();
           itr != matches.end(); ++itr)
      {
        node = *itr;
        findAltTop = false;
        if (buildNodes(dir, node->castBranch(),
                       1, elemList, loc, nodes, 
                       findModules, false, protectedMode))
        {
          inserted = true;
        }
      }
    }

    // If there was no match based on a top-level module, check
    // to see if buf matches another module, and then hook
    // in all the instances
    if (findAltTop)
    {
      // For each matching module, find all the instances of that
      // module in the design hierarchy.  Unfortunately this requires
      // a complete parse-table elaboration, resulting in a hash-map
      // from modules to all the instances of those modules
      fullElabParseTable();

      // Walk through all the design modules, finding the best matches.
      // Note these requirements:
      //   - parameters and uselib can imply multiple modules with the 
      //     same original name, so there can be multiple exact matches
      //   - the user may have a slashified module with a * in it.
      //     if they exactly match that, then they shouldn't wildcard
      //     match anything
      // so we take one loop through the modules.  If we all we get
      // are wildcards, then we'll take every name that matches.  But the
      // moment we find any exact match, we discard any wildcard matches
      // we've found and stop looking for wildcards.
      NUModuleList modules;
      findMatchingModules(&modules, elem0);

      // Walk through all the modules, and find all the instantiations of
      // those modules
      for (NUModuleList::iterator m = modules.begin(); m != modules.end(); ++m)
      {
        NUModule* mod = *m;
        InstanceMap::iterator p = mInstanceMap->find(mod);
        if (p != mInstanceMap->end())
        {
          STNodeVector& v = p->second;
          for (STNodeVector::iterator q = v.begin(); q != v.end(); ++q)
          {
            STBranchNode* node = *q;
            if (buildNodes(dir, node, 1, elemList, loc, nodes, 
			   findModules, false, protectedMode)) {
              inserted = true;
            }
          }
        }
      }
    }
  } // else

  // The a wildcard like "*.foo" matched nothing, then report an error
  if (inserted) {
    // Keep track of all net directives
    // and disqualify their module from congruence analysis.  Note
    // that all the module directives are implemented by setting flags
    // in the NUModule itself, and so congruence can compare modules to
    // ensure they have compatible flags.  Comparing nets is more of a
    // pain.
    for (NameSet::iterator p = nodes->begin(); p != nodes->end(); ++p) {
      STSymbolTableNode* node = *p;
      SymTabVal* data = SymTabBOM::castBOM(node->getBOMData());
      NUScope* scope = dynamic_cast<NUModule*>(data->mNucleusObj);
      if (scope == NULL) {
        NUNet* net = dynamic_cast<NUNet*>(data->mNucleusObj);
        ST_ASSERT(net, node);
        scope = net->getScope();
      }
      ST_ASSERT(scope, node);
      NUModule* mod = scope->getModule();
      ST_ASSERT(mod, node);
      mModulesSpecifiedInNetDirectives.insert(mod);
    }
  }
  else {
    UtString msgbuf;
    for (size_t i = 0; i < elemList.size(); ++i)
    {
      if (i != 0) {
        msgbuf << ".";    // We should do something different for VHDL
      }
      msgbuf << elemList[i];
    }
    mMsgContext->DirNoMatch(&loc, msgbuf.c_str());
  }
  return inserted;
} // bool IODBNucleus::buildRoots

bool IODBNucleus::isModuleSpecifiedInNetDirective(NUModule* mod) const {
  return mModulesSpecifiedInNetDirectives.find(mod) !=
    mModulesSpecifiedInNetDirectives.end();
}

bool IODBNucleus::isDisableOutputSysTasks(StringAtom* module_name) const
{
  return (mDisableOutputSysTasksNames.end() !=  mDisableOutputSysTasksNames.find(module_name));
}

bool IODBNucleus::isEnableOutputSysTasks(StringAtom* module_name) const
{
  return (mEnableOutputSysTasksNames.end() !=  mEnableOutputSysTasksNames.find(module_name));
}

void IODBNucleus::replaceNets(const NUNetReplacementMapNonConst& replacements) {
  NUNetReplacementMapNonConst::const_iterator not_found = replacements.end();
  for (STSymbolTable::NodeLoop p = mParseTable->getNodeLoop(); !p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf != NULL) {
      NUBase* base = CbuildSymTabBOM::getNucleusObj(leaf);
      NUNet* nodeNet = dynamic_cast<NUNet*>(base);
      NU_ASSERT(nodeNet, base);
      NUNetReplacementMapNonConst::const_iterator q =
        replacements.find(nodeNet);
      if (q != not_found) {
        NUNet* rep = q->second;
        CbuildSymTabBOM::putNucleusObj(leaf, rep);
      }
    }
  }
}

void IODBNucleus::declareNet(STBranchNode* parent, NUNet* net,
                             StringAtom* searchSym,
                             STSymbolTableNode** foundNode)
{
  if (! net->isHierRef())
  {
    StringAtom* name = net->getName();
    STSymbolTableNode* newNode = elabNode(name, net, parent, true);
    if (name == searchSym)
      *foundNode = newNode;
  }
}

void IODBNucleus::elaborateBranch(STBranchNode *branch,
                                  StringAtom *matchStr,
                                  STSymbolTableNode **matchNode,
                                  bool isLeaf,
                                  bool findModules)
{
  ST_ASSERT(matchNode != NULL, branch);

  SymTabVal* stv = SymTabBOM::castBOM(branch->getBOMData());

  // figure out what kind of object branch is, currently we support NUModule|NUTask|NUNamedDeclarationScope
  NUScope* scope = dynamic_cast<NUScope*>(stv->mNucleusObj);
  NUModule* mod = dynamic_cast<NUModule*>(stv->mNucleusObj);
  NUNamedDeclarationScope* dc = dynamic_cast<NUNamedDeclarationScope*>(stv->mNucleusObj);
  NUTask* task =  dynamic_cast<NUTask*>(stv->mNucleusObj);
  ST_ASSERT((mod != NULL) || (dc != NULL) || (task != NULL), branch);

  if (isLeaf && !findModules)
  {
    if (mod != NULL) 
    {
      for (NUModule::NetLoop p = mod->loopNets(); !p.atEnd(); ++p) 
        declareNet(branch, *p, matchStr, matchNode);
    } 
    else if ( task != NULL) 
    {
      for (NUNetLoop p = task->loopLocals(); !p.atEnd(); ++p) 
        declareNet(branch, *p, matchStr, matchNode);
      
      for (NUNetVectorLoop p = task->loopArgs();  !p.atEnd(); ++p) 
        declareNet(branch, *p, matchStr, matchNode);
      
    } 
    else if ( dc != NULL ) 
    {
      for (NUNetLoop p = dc->loopLocals(); !p.atEnd(); ++p) 
        declareNet(branch, *p, matchStr, matchNode);
    }
    
    // Finds the declaration scopes and checks only the ones that are 
    // for a record
    for (NUNamedDeclarationScopeLoop p = scope->loopDeclarationScopes();
         !p.atEnd(); ++p)
    {
      NUNamedDeclarationScope* declScope = *p;
      if ( (declScope->getHierFlags() & eHierTypeMask) == eHTRecord)
      {
        StringAtom* name = declScope->getName();
        STSymbolTableNode *node = elabDeclScope(branch, declScope);
	if((node != NULL) && (name == matchStr))
          *matchNode = node;
      }
    }
  }

  
  // We can also look for modules at the leaf (some directives deal
  // with modules)
  if (!isLeaf || ((*matchNode == NULL) && findModules)) {
    // note here we elab through ALL instances in the module (not the ones in
    // declaration scopes), and ALL declaration scopes in the module (not the ones
    // nested inside declaration scopes), and ALL tasks so that the partial
    // symbol-table we are building will be complete
    if (mod != NULL) {
      for (NUModuleInstanceMultiLoop p = mod->loopModuleInstances(); !p.atEnd(); ++p)
      {
        NUModuleInstance* inst = *p;
        StringAtom* name = inst->getName();
        STSymbolTableNode* newNode = elabNode(name, inst->getModule(), branch,
                                              false);
        if (name == matchStr)
          *matchNode = newNode;
      }

      for (NUNamedDeclarationScopeLoop p = mod->loopDeclarationScopes();
           !p.atEnd(); ++p)
      {
        NUNamedDeclarationScope* declScope = *p;
        StringAtom* name = declScope->getName();
        STSymbolTableNode* newNode = elabNode(name, declScope, branch, false);
        if (name == matchStr) {
          *matchNode = newNode;
        }
      }

      // nested tasks
      for (NUTaskLoop p = mod->loopTasks(); !p.atEnd(); ++p) {
        NUTF* tf = *p;
        StringAtom* name = tf->getName();
        STSymbolTableNode* newNode = elabNode(name, tf, branch, false);
        if (name == matchStr) {
          *matchNode = newNode;
        }
      }
    } else if ( task != NULL ) {
      for (NUNamedDeclarationScopeLoop p = task->loopDeclarationScopes(); !p.atEnd(); ++p) {
        NUNamedDeclarationScope* declScope = *p;
        StringAtom* name = declScope->getName();
        STSymbolTableNode* newNode = elabNode(name, declScope, branch, false);
        if (name == matchStr) {
          *matchNode = newNode;
        }
      }
    } else if ( dc != NULL ) {
      for (NUModuleInstanceMultiLoop p = dc->loopDeclScopeInstances();
           !p.atEnd(); ++p)
      {
        NUModuleInstance* inst = *p;
        StringAtom* name = inst->getName();
        STSymbolTableNode* newNode = elabNode(name, inst->getModule(), branch, false);
        if (name == matchStr)
          *matchNode = newNode;
      }
      // look for nested declaration scopes.
      // note here we elab through ALL declaration scopes
      // so that the partial symbol-table we are building will be complete
      for (NUNamedDeclarationScopeLoop p = dc->loopDeclarationScopes();
           !p.atEnd(); ++p)
      {
        NUNamedDeclarationScope* declScope = *p;
        StringAtom* name = declScope->getName();
        STSymbolTableNode* newNode = elabNode(name, declScope, branch, false);
        if (name == matchStr) {
          *matchNode = newNode;
        }
      }
    }
    
  }
}

STBranchNode* IODBNucleus::findBranch(const STBranchNode* parent,
                                      StringAtom* sym)
{
  STSymbolTableNode* node = mParseTable->find(parent, sym);
  if (node != NULL) {
    return node->castBranch();
  }
  return NULL;
}

NUBase* IODBNucleus::getNucleusObject(const STSymbolTableNode* node) {
  IODBNucleus::SymTabVal* data =
    IODBNucleus::SymTabBOM::castBOM(node->getBOMData());
  return data->mNucleusObj;
}

static void sDirPathnameToString(UtString* pathname, const UtStringArray& elemList) {
  size_t size = elemList.size();
  for (size_t i = 0; i < size; ++i)
  {
    *pathname << ( i ? "." : "") << elemList[i];
  }
}

void IODBNucleus::
sGetWildcardMatchingChildren(STBranchNode* parent, STSymbolTable* symtab,
                             const char* name, 
                             UtList<STSymbolTableNode*>* matchingChildren)
{
  matchingChildren->clear(); // Start with empty list.
  // See if it's a wildcard
  if ((strchr(name, '*') != 0) ||
      (strchr(name, '?') != 0))
  {
    UtWildcard wild(name);
    if (parent != NULL)
    {
      // Find all the matches at this level
      STBranchNodeIter iter(parent);
      SInt32 index;
      STSymbolTableNode* node = NULL;
      while ((node = iter.next(&index)) != NULL)
      {
        const char* nodeStr = node->str();
        if ((*nodeStr != '$') && wild.isMatch(nodeStr)) {
          matchingChildren->push_back(node);
        }
      }
    }
    else
    {
      STSymbolTableNode* node = NULL;
      // The roots have to be wild card matched.
      for (STSymbolTable::RootIter iter = symtab->getRootIter(); iter(&node);)
      {
        StringAtom* rootName = node->strObject();
        if (wild.isMatch(rootName->str())) {
          matchingChildren->push_back(node);
        }
      }
    }
  }
}


bool IODBNucleus::markDirNet(STSymbolTableNode* node,  NameSet* nodes, const UtStringArray& elemList, 
			     IODBDirective::DesignDirective dir, ProtectedMode protectedMode)
{
  SymTabVal* stv = SymTabBOM::castBOM(node->getBOMData());
  NUNet* net =  dynamic_cast<NUNet*>(stv->mNucleusObj);

  ST_ASSERT(net, node);

  if(net->isCompositeNet() == true)
  {
    return false;    // ignore composite nets
  }

  const char* nodeStr = node->str();
  if (*nodeStr == '$') {
    return false;                // ignore temp nets
  }
  
  SourceLocator loc = net->getLoc();
  //  Check for Mutable
  if (protectedMode&eProtectedMutable)
  {
    if (not markProtectedMutable(node) )
    {
      UtString pathname;
      sDirPathnameToString(&pathname, elemList);
      mMsgContext->DirRequiresANet(&loc, IODBDirective::dirToString(dir), pathname);
      return false;
    }
  }

  // Check for Observable 
  if (protectedMode&eProtectedObservable) 
  {
    if ( not markProtectedObservable(node) )
    {
      UtString pathname;
      sDirPathnameToString(&pathname, elemList);
      mMsgContext->DirRequiresANet(&loc, IODBDirective::dirToString(dir), pathname);
      return false;
    }
  }

  (*mParseIOTable)[node] = loc;
  nodes->insert(node);

  return true;
}


bool IODBNucleus::markDirSubNets(STBranchNode *node,  NameSet* nodes, const UtStringArray& elemList, 
				 IODBDirective::DesignDirective dir, ProtectedMode protectedMode)
{
  bool ret = false;
  int numNets = node->numChildren();
  for(int i = 0; i < numNets; i++)
  {
    STSymbolTableNode* childNode = node->getChild(i);
    STBranchNode* branch = childNode->castBranch();
    if(branch != NULL)
      ret |= markDirSubNets(branch, nodes, elemList, dir, protectedMode );
    else
      ret |= markDirNet(childNode,  nodes, elemList, dir, protectedMode);
  }

  return ret;
}


bool IODBNucleus::buildNodes(IODBDirective::DesignDirective dir,
                             STBranchNode* parent, 
                             size_t i,
                             const UtStringArray& elemList,
                             const SourceLocator& loc,
                             NameSet* nodes,
                             bool findModules,
                             bool withinATask, 
			     ProtectedMode protectedMode)
{
  if (i >= elemList.size())
    return false;
  bool inserted = false; // will be set to true if ANY net(s) is marked with protectedMode
    
  SymTabVal* stv = SymTabBOM::castBOM(parent->getBOMData());
  NUTask* task =  dynamic_cast<NUTask*>(stv->mNucleusObj);
  withinATask |= (task != NULL);

  const UtString& buf = elemList[i];
  StringAtom* sym = mAtomicCache->intern(buf.c_str(), buf.size());

  // In order to allow wildcarded directives to affect localflow
  // analysis, e.g. suppressing local optimizations to remove
  // observable nets, we need to do name-lookup directly on nucleus
  // without the benefit of the fully elaborated symbol table.  We
  // will build up a partially elaborated symbol-table to help us
  // not be heinously slow.  Note that we make sure this partially
  // elaborated symbol-table is complete (so that later calls that
  // might use the table during wildcard analysis will find all
  // symbols)
  STSymbolTableNode* node = mParseTable->find(parent, sym);
  bool isLeaf = (i == elemList.size() - 1);
  if (node == NULL)
  {
    elaborateBranch(parent, sym, &node, isLeaf, findModules);
  }

  // If we were able to find the node by exact match or by lookup, then
  // it is not a wildcard node.
  if (node != NULL)
  {
    // a matching name was found, so this is not a wildcard node
    SymTabVal* data = SymTabBOM::castBOM(node->getBOMData());
    NUMemoryNet* memNet = dynamic_cast<NUMemoryNet*>(data->mNucleusObj);

    if (isLeaf) 
    {
      if ( withinATask && ( ( protectedMode&eProtectedMutable ) ||
                            ( protectedMode&eProtectedObservable ) ) )
      {
        // do not allow user to set observeSignal type directives on
        // nets within a task, because they would be stack local and
        // thus have no where to get the signal from.
        UtString pathname;
        sDirPathnameToString(&pathname, elemList);
        mMsgContext->DirectiveNotAllowedOnNetsInTasks(&loc, IODBDirective::dirToString(dir),pathname );
      } 
      else if ((memNet != NULL) && (dir == IODBDirective::eForce || dir == IODBDirective::eTieNet)) 
      {
	  // forceSignal or tieSignal not allowed on memories
          UtString pathname;
          sDirPathnameToString(&pathname, elemList);
          mMsgContext->DirNotAllowedOnMemoryNets(&loc, IODBDirective::dirToString(dir), pathname );
      }	
      else
      {	 
        if (node->castBranch() != NULL)
        {
          SymTabVal* stv = SymTabBOM::castBOM(node->getBOMData());
          NUNamedDeclarationScope* declScope = dynamic_cast<NUNamedDeclarationScope*>(stv->mNucleusObj);
          if(declScope != NULL)
          {
            if ( ( declScope->getHierFlags() & eHierTypeMask ) == eHTRecord )
	    {
	      if (dir == IODBDirective::eTieNet)
	      {
		// For the time being, tieNet is not allowed on records
		UtString pathname;
		sDirPathnameToString(&pathname, elemList);
		mMsgContext->DirNotAllowedOnRecord(&loc, IODBDirective::dirToString(dir), pathname );
	      }
	      else 
	      {
		inserted |= markDirSubNets(node->castBranch(), nodes, elemList, dir, protectedMode);
	      }
	    }
	  }
	}
        else
        {
          inserted |= markDirNet(node, nodes, elemList, dir, protectedMode);
        }
      }
    }
    else 
    {
      parent = node->castBranch();
      if (parent != NULL)
        inserted |= buildNodes(dir, parent, i + 1, elemList, loc, nodes, 
                               findModules,
                               withinATask,
                               protectedMode);
    }
  }
  else
  {
    // a matching name was not found, so this is a wildcard node
    UtList<STSymbolTableNode*> matchingChildren;
    sGetWildcardMatchingChildren(parent, NULL, buf.c_str(), &matchingChildren);
    for (UtList<STSymbolTableNode*>::iterator itr = matchingChildren.begin();
         itr != matchingChildren.end(); ++itr)
    {
      node = *itr;
      STBranchNode* branch = node->castBranch();
      if (branch == NULL)
      {
        if (isLeaf)
        {
          // Verify that this node is the type we want.  For
          // now we only want leafs
          if (node->castLeaf() != NULL)
          {
            SymTabVal* data = SymTabBOM::castBOM(node->getBOMData());
            NUMemoryNet* memNet = dynamic_cast<NUMemoryNet*>(data->mNucleusObj);

            if ( withinATask && ( ( protectedMode&eProtectedMutable ) ||
                                  ( protectedMode&eProtectedObservable ) ) )
            {
              // do not allow user to set observeSignal type directives on
              // nets within a task, because they would be stack local and
              // thus have no where to get the signal from.
              UtString pathname;
              sDirPathnameToString(&pathname, elemList);
              UtString directive_string;
              directive_string << IODBDirective::dirToString(dir) << " " << pathname;
              UtString real_name;
              node->verilogCompose(&real_name);

              mMsgContext->DirectiveNotAllowedOnNetsInTasks(&loc, directive_string.c_str(), real_name );
	    }
            else if ((memNet != NULL) && (dir == IODBDirective::eForce || dir == IODBDirective::eTieNet) ) {
              // forceSignal or tieSignal not allowed on memories
              UtString pathname;
              sDirPathnameToString(&pathname, elemList);
              UtString directive_string;
              directive_string << IODBDirective::dirToString(dir) << " " << pathname;
              UtString real_name;
              node->verilogCompose(&real_name);

              mMsgContext->DirNotAllowedOnMemoryNets(&loc, directive_string.c_str(), real_name );
            } 
            else 
            {
	      inserted |= markDirNet(node, nodes, elemList, dir, protectedMode);
            }
          }
        }
      }
      else if((branch != NULL) && (isLeaf == true))
      {
        if (node->castBranch() != NULL)
        {
          SymTabVal* stv = SymTabBOM::castBOM(node->getBOMData());
          NUNamedDeclarationScope* declScope = dynamic_cast<NUNamedDeclarationScope*>(stv->mNucleusObj);
          if(declScope != NULL)
          {
            if ( (declScope->getHierFlags() & eHierTypeMask ) == eHTRecord)
            {
              inserted |= markDirSubNets(node->castBranch(), nodes, elemList, dir, protectedMode);
	    }
	  }
        }
      }
      else if (buildNodes(dir, branch, i + 1, elemList, loc, nodes, 
                          findModules, withinATask, protectedMode)) 
      {
        inserted = true;
      }
    }
  }

  return inserted;
}


void IODBNucleus::fullElabModule(NUModule* mod, STBranchNode* parent)
{
  for (NUNamedDeclarationScopeLoop p = mod->loopDeclarationScopes();
       !p.atEnd(); ++p)
  {
    NUNamedDeclarationScope* declScope = *p;
    elabDeclScope(parent, declScope);
  }

  for (NUTaskLoop p = mod->loopTasks(); !p.atEnd(); ++p) {
    NUTF* tf = *p;
    StringAtom* name = tf->getName();
    STSymbolTableNode* taskNode = elabNode(name, tf, parent, false);
    STBranchNode* taskBranch = taskNode->castBranch();
    NU_ASSERT(taskBranch, tf);
    for (NUNamedDeclarationScopeLoop p = tf->loopDeclarationScopes();
         !p.atEnd(); ++p)
    {
      NUNamedDeclarationScope* declScope = *p;
      elabDeclScope(taskBranch, declScope);
    }

    for (NUNetLoop p = tf->loopLocals(); !p.atEnd(); ++p) {
      NUNet* net = *p;
      if (!net->isHierRef()) {
        elabNode(net->getName(), net, taskBranch, true);
      }
    }
  }

  for (NUModule::NetLoop p = mod->loopNets(); !p.atEnd(); ++p) {
    NUNet* net = *p;
    if (!net->isHierRef()) {
      elabNode(net->getName(), net, parent, true);
    }
  }

  for (NUModuleInstanceMultiLoop p = mod->loopModuleInstances(); !p.atEnd(); ++p)
  {
    NUModuleInstance* inst = *p;
    elabInstance(inst, parent);
  }
} // void IODBNucleus::fullElabModule

STBranchNode* IODBNucleus::elabDeclScope(STBranchNode* parent,
                                         NUNamedDeclarationScope* declScope)
{
  STBranchNode *retBranchNode = NULL;

  StringAtom* dsName = declScope->getName();
  STSymbolTableNode* newNode = elabNode(dsName, declScope, parent, false);
  STBranchNode* branch = newNode->castBranch();
  ST_ASSERT(branch, newNode);
  retBranchNode = branch;
  for (NUNamedDeclarationScopeLoop p = declScope->loopDeclarationScopes();
       !p.atEnd(); ++p)
  {
    elabDeclScope(branch, *p);
  }

  for (NUNetLoop p = declScope->loopLocals(); !p.atEnd(); ++p) {
    NUNet* net = *p;
    if (!net->isHierRef()) {
      elabNode(net->getName(), net, branch, true);
    }
  }

  for (NUModuleInstanceMultiLoop iter = declScope->loopDeclScopeInstances();
       !iter.atEnd(); ++iter) {
    NUModuleInstance* inst = *iter;
    elabInstance(inst, branch);
  }

  return retBranchNode;
}

void IODBNucleus::elabInstance(NUModuleInstance* inst, STBranchNode* parent)
{
  NUModule* master = inst->getModule();
  STSymbolTableNode* newNode = elabNode(inst->getName(), master, parent,
                                        false);
  STBranchNode* newParent = newNode->castBranch();
  NU_ASSERT(newParent, inst);

  STNodeVector& v = (*mInstanceMap)[master];
  v.push_back(newParent);
  
  fullElabModule(master, newParent);

  // Hook up alias rings based on port connections with whole nets
  // as both formal & actual
}

void IODBNucleus::fullElabParseTable()
{
  if (mInstanceMap == NULL)
  {
    mInstanceMap = new InstanceMap;
    mIsElaborated = true;
    for (NUDesign::ModuleLoop m = mDesign->loopTopLevelModules(); !m.atEnd();
         ++m)
    {
      NUModule* mod = *m;
      STSymbolTableNode* node = elabNode(mod->getOriginalName(),
                                         mod, NULL, false);
      STBranchNode* parent = node->castBranch();
      NU_ASSERT(parent, mod);
      fullElabModule(mod, parent);
      STNodeVector& v = (*mInstanceMap)[mod];
      v.push_back(parent);
    }
  }
}

STSymbolTable* IODBNucleus::getParseTable() {
  return mParseTable;
}

bool IODBNucleus::markProtectedMutable(STSymbolTableNode * node)
{
  SymTabVal* stv = SymTabBOM::castBOM(node->getBOMData());
  NUNet* net = dynamic_cast<NUNet*>(stv->mNucleusObj);
  if ( net )
    markProtectedMutable(net);
  return ( net != NULL );
}

void IODBNucleus::markProtectedMutable(const NUNet* net)
{
  mProtectedMutableNets.insert(net);
  mModulesSpecifiedInNetDirectives.insert(net->getScope()->getModule());
}

void IODBNucleus::removeProtectedMutable(const NUNet* net)
{
  mProtectedMutableNets.erase(net);
}

bool IODBNucleus::markProtectedObservable(STSymbolTableNode * node)
{
  SymTabVal* stv = SymTabBOM::castBOM(node->getBOMData());
  NUNet* net = dynamic_cast<NUNet*>(stv->mNucleusObj);
  if ( net )
    markProtectedObservable(net);
  return ( net != NULL );
}

void IODBNucleus::markProtectedObservable(const NUNet* net)
{
  mProtectedObservableNets.insert(net);
  mModulesSpecifiedInNetDirectives.insert(net->getScope()->getModule());
}

void IODBNucleus::removeProtectedObservable(const NUNet* net)
{
  mProtectedObservableNets.erase(net);
}

bool IODBNucleus::addDesignDirective(IODBDirective::DesignDirective dirType,
                                     const char* p,
                                     const SourceLocator& loc,
                                     const char* val)
{
  UtStringArray elemList;
  HdlId info;

  // Check for removed directives
  if (checkRemovedDirective(dirType, p, loc, val)) {
    return false;
  }

  if(strlen(p) == 0)
  {
    mMsgContext->DirInvalidSyntax(&loc, IODBDirective::dirToString(dirType), "missing the net");
    return false;
  }

  IODBDesignDirective* dir = new IODBDesignDirective(dirType, loc, val);
  while ((mHdl->parseName(&p, &elemList, &info) == HdlHierPath::eEndPath) &&
         !elemList.empty())
  {
    dir->addToken(elemList);

    if ((dirType == IODBDirective::eInputDir) && (elemList.size() == 2))
    {
      UtString mod_net;
      mod_net << elemList[0] << '.' << elemList[1];
      mInputNetStrings.insert(mod_net);
    }
    else if (dirType == IODBDirective::eTieNet) {
      if (elemList.size() != 2) {
        mMsgContext->DirInvalidModNet(&loc, "tieNet");
      }
    }
    else if (dirType == IODBDirective::eAsyncReset) {
      if (elemList.size() != 2) {
        mMsgContext->DirInvalidModNet(&loc, "asyncReset");
      }
    }
    // the blastNet directive uses unelaborated names and is applied
    // before the directive values are resolved, so we store the
    // module and net in the mModuleBlastedNets structure for use
    // in vector separation
    else if ((dirType == IODBDirective::eBlast) && (elemList.size() > 1))
    {
      // net names start with module
      UtString netName;
      netName << elemList[0];

      // net path is formed from all remaining elements
      for (size_t i = 1; i < elemList.size(); ++i)
      {
        netName << ".";
        netName << elemList[i];
      }

      mBlastedNetNames.insert(netName);
    }

    for (size_t i = 0; i < elemList.size(); ++i)
      mDirectiveNames.insert(elemList[i]);
    elemList.clear();
  }
  mDesignDirectives.push_back(dir);
  if (*StringUtil::skip(p) != '\0')
    mMsgContext->DirGarbageAtEOL(&loc, p);
  return true;
}

bool IODBNucleus::addValueDirective(IODBDirective::DesignDirective dirType,
                                    StrToken& tok,
				    const SourceLocator& loc)
{
  bool ret = false;
  // Get the value
  const char* value;
  if (!tok(&value))
  {
    mMsgContext->DirInvalidNumber(&loc, "");
  }
  else
  {
    const char* rest = tok.curPos();
    ret =  addDesignDirective(dirType, rest, loc, value);
  }
  return ret;
}

bool
IODBNucleus::addEventDirective(IODBDirective::DesignDirective dirType,
                               StrToken& tok,
                               const SourceLocator& loc)
{
  // Add the directive
  IODBDesignDirective* dir = new IODBDesignDirective(dirType, loc, NULL);
  mDesignDirectives.push_back(dir);

  // In a loop look for clock conditions
  const char* eventStr;
  bool ok = true;
  bool done = false;
  while (ok && !done && tok(&eventStr)) {
    // Figure out the event
    if ((strcmp(eventStr, "rise") != 0)  && (strcmp(eventStr, "fall") != 0)) {
      mMsgContext->InvClockEdge(&loc, eventStr);
      ok = false;
    } else {
      // Add the string. It is simpler than making a new directive type
      UtStringArray edgeStrArray;
      edgeStrArray.push_back(eventStr);
      dir->addToken(edgeStrArray);
    }

    // Grab the clock name
    if (ok) {
      const char* clockStr;
      if (!tok(&clockStr)) {
        mMsgContext->MissingClockName(&loc);
        ok = false;
      } else {
        // We have everything, add the clock event
        HdlId info;
        UtStringArray elems;
        HdlHierPath::Status status = mHdl->parseName(&clockStr, &elems, &info);
        if ((status == HdlHierPath::eEndPath) && !elems.empty())
          dir->addToken(elems);
      }
    }

    // Grab the OR condition if there is one. If not, we are done
    const char* orStr;
    if (ok && tok(&orStr) && (strcmp(orStr, "or") != 0) &&
        (strcmp(tok.curPos(), "OR") != 0)) {
      done = true;
    }
  } // while

  if (not tok.atEnd()) {
    mMsgContext->DirGarbageAtEOL(&loc, tok.curPos());
  }
  return true;
} // IODBNucleus::addEventDirective


bool IODBNucleus::isDirectiveName(const char* name)
  const
{
  return mDirectiveNames.find(name) != mDirectiveNames.end();
}

bool IODBNucleus::evaluateModuleDirective (IODBModuleDirective *directive)
{
  bool ret = true;
  IODBDirective::ModuleDirective eType = directive->getDirective ();
  SourceLocator loc = directive->getLocation ();
  // apply the directive
  for (IODBDirective::TokenVectorLoop loop = directive->loopTokens (); !loop.atEnd (); ++loop) {
    UtStringArray *elemList = *loop;
    if (elemList->size () > 1) {
      // this is a warning
      UtString buffer;
      mHdl->compPath(*elemList, &buffer);
      mMsgContext->DirInvalidModuleName(&loc, buffer.c_str());
    } else {
      const char* modName = (*elemList)[0];
      StringAtom* atom = mAtomicCache->intern(modName);
      switch (eType) {
      case IODBDirective::eFlattenModule:
        mFlattenModuleNames.insert(atom);
        break;
      case IODBDirective::eFlattenModuleContents:
        mFlattenModuleContentsNames.insert(atom);
        break;
      case IODBDirective::eAllowFlattening:
        mAllowFlatteningModuleNames.insert(atom);
        break;
      case IODBDirective::eDisallowFlattening:
        mDisallowFlatteningModuleNames.insert(atom);
        break;
      case IODBDirective::eTernaryGateOptimization:
        mTernaryGateOptimization.insert(atom);
        break;
      case IODBDirective::eHideModule:
        mHideModuleNames.insert(atom);
        break;
      case IODBDirective::eNoAsyncReset:
        mNoAsyncResetNames.insert(atom);
        break;
      case IODBDirective::eDisableOutputSysTasks:
        mDisableOutputSysTasksNames.insert(atom);
        break;
      case IODBDirective::eEnableOutputSysTasks:
        mEnableOutputSysTasksNames.insert(atom);
        break;
#ifdef CDB
      default:
        // crash and burn when compiled for debug
        INFO_ASSERT (false, "bogus module directive");
        break;
#endif
      }
    }
  } // for IODBDirective::TokenVectorLoop

  return ret;
}

bool IODBNucleus::addModuleDirective (const char* p, 
  const SourceLocator& loc, IODBDirective::ModuleDirective eType)
{
  bool ret = true;
  // grab command line options that affect module directive processing
  bool flattening_enabled = (mArgs->getBoolValue(CRYPT("-flatten")) or
    mArgs->getBoolValue(CRYPT("-flattenEarly")));
  bool defaultDoingOutputSysTasks = (mArgs->getBoolValue("-enableOutputSysTasks"));
  // check the directive's applicability w.r.t. command line arguments
  if (!flattening_enabled 
    && (eType == IODBDirective::eFlattenModule
      || eType == IODBDirective::eFlattenModuleContents
      || eType == IODBDirective::eAllowFlattening
      || eType == IODBDirective::eDisallowFlattening)) {
    // this is a warning so return value is unaffected
    mMsgContext->FlattenDirInactive(&loc, IODBDirective::dirToString(eType));
  } 
  if (defaultDoingOutputSysTasks && eType == IODBDirective::eEnableOutputSysTasks ) {
    // this is a warning so return value is unaffected
    mMsgContext->OutputSysTasksDirInactive(&loc, IODBDirective::dirToString(eType), "not been");
  } 
  if (!defaultDoingOutputSysTasks && eType == IODBDirective::eDisableOutputSysTasks) {
    // this is a warning so return value is unaffected
    mMsgContext->OutputSysTasksDirInactive(&loc, IODBDirective::dirToString(eType), "been");
  } 
  UtStringArray elemList;
  HdlId info;
  while ((mHdl->parseName(&p, &elemList, &info) == HdlHierPath::eEndPath) && !elemList.empty()) {
    if (elemList.size() > 1) {
      UtString buffer;
      mHdl->compPath(elemList, &buffer);
      mMsgContext->DirInvalidModuleName(&loc, buffer.c_str());
      ret = false;
    } else {
      // module directives are constructed and then immediately evaluation.
      IODBModuleDirective *directive = new IODBModuleDirective (eType, loc);
      directive->addToken (elemList);
      evaluateModuleDirective (directive);
      // cache on a list for writing to the gui database file
      mModuleDirectives.push_back (directive);
    }
    elemList.clear ();
  }
  return ret;
} // bool IODBNucleus::addModuleDirective

bool IODBNucleus::addInlineTFTok(const char* p, const SourceLocator & loc)
{
  UtStringArray elemList;
  HdlId info;
  while ((mHdl->parseName(&p, &elemList, &info) == HdlHierPath::eEndPath) &&
         !elemList.empty())
  {
    if (elemList.size() != 2) {
      UtString buffer;
      mHdl->compPath(elemList, &buffer);
      mMsgContext->DirInvalidTFName(&loc, buffer.c_str());
    } else {
      const char* modName = elemList[0];
      StringAtom* modAtom = mAtomicCache->intern(modName);
      const char* tfName = elemList[1];
      StringAtom* tfAtom = mAtomicCache->intern(tfName);
      mInlineTFNames[ modAtom ].insert(tfAtom);
    }
    elemList.clear();
  }
  return true;
}


// This is intended to be called when decoding a .cem file
void IODBNucleus::addHideModuleName(const char* modName) {
  StringAtom* atom = mAtomicCache->intern(modName);
  mHideModuleNames.insert(atom);
}

void IODBNucleus::resolveModuleDirective(const AtomSet& atoms,
                                         ModuleFunction method,
                                         IODBDirective::ModuleDirective eType)
{
  for (AtomSet::const_iterator p = atoms.begin(), e = atoms.end(); p != e; ++p)
  {
    StringAtom* atom = *p;
    const char* moduleName = atom->str();
    NUModuleList modules;
    findMatchingModules(&modules, moduleName);

    if (modules.empty()) {
      mMsgContext->DirNoSuchModule(moduleName, IODBDirective::dirToString(eType));
    }
    else {
      for (NUModuleList::iterator m = modules.begin(), e = modules.end();
           m != e; ++m)
      {
        NUModule* mod = *m;
        (this->*method)(mod);

        // We don't add this module to mModulesSpecifiedInDirectives
        // because we mark the directive flags in the NUModule structure,
        // and convergence calls NUModule::areFlagsCongruent
      }
    }
  }
} // void IODBNucleus::resolveModuleDirective


bool IODBNucleus::recordPaths(IODBDesignDirective* dir, 
			      NameSet* names, 
			      ProtectedMode protectedMode,
			      bool skip_first_token,
                              bool findModules,
                              IODBDirective::DirectiveMode dirMode)
{
  bool ret = true;
  for (IODBDirective::TokenVectorLoop p = dir->loopTokens (); !p.atEnd(); ++p)
  {
    if (skip_first_token) {
      skip_first_token = false;
    } else {
      ret &= buildRoots(dir->getDirective (), **p, dir->getLocation (), names, findModules,
                        protectedMode, dirMode);
    }
  }
  return ret;
}

bool IODBNucleus::recordAssocPaths(IODBDesignDirective* dir, 
				   ProtectedMode protectedMode,
                                   AssocPathCallback* callback,
                                   bool singleNode,
                                   bool findModules,
                                   IODBDirective::DirectiveMode dirMode)
{
  NameSet nodes;
  if (singleNode) {
    if (!buildRoots(dir->getDirective (), dir->firstToken (), dir->getLocation (), &nodes,
                    findModules, protectedMode, dirMode)) {
      return true;
    }
  } else {
    if (!recordPaths(dir, &nodes, protectedMode, false, findModules, dirMode)) {
      return false;
    }
  }
  callback->recordPaths(dir, nodes, protectedMode);
  return true;                  // no warning for now on empty line (?)
} // bool IODBNucleus::recordAssocPaths

bool IODBNucleus::recordWildcard(IODBDesignDirective* dir,
                                 Wildcards* wildcards)
{
  for (IODBDirective::TokenVectorLoop p = dir->loopTokens (); !p.atEnd(); ++p) {
    UtStringArray *strArray = *p;
    for (UInt32 i = 0; i < strArray->size(); ++i) {
      UtString matchExpr = (*strArray)[i];
      UtWildcard* wildcard = new UtWildcard(matchExpr.c_str());
      wildcards->push_back(wildcard);
    }
  }
  return true;
}

bool IODBNucleus::recordEventPaths(IODBDesignDirective *dir,
                                   ClockEvents* clockEvents,
                                   ProtectedMode protectedMode)
{
  bool ret = true;
  bool lookForEdge = true;
  ClockEdge edge = eClockPosedge;
  for (IODBDirective::TokenVectorLoop p = dir->loopTokens (); !p.atEnd(); ++p) {
    UtStringArray *strArray = *p;
    if (lookForEdge) {
      // Translate the edge type
      INFO_ASSERT(strArray->size() == 1, "Directive token has more than one edge.");
      UtString edgeStr = (*strArray)[0];
      if (strcmp(edgeStr.c_str(), "rise") == 0) {
        edge = eClockPosedge;
      } else {
        INFO_ASSERT(strcmp(edgeStr.c_str(), "fall") == 0, "Invalid edge string.");
        edge = eClockNegedge;
      }

      // Now looking for net
      lookForEdge = false;

    } else {
      // Translate the name
      NameSet names;
      ret &= buildRoots(dir->getDirective (), *strArray, dir->getLocation (), &names, false,
                        protectedMode, IODBDirective::eDirBoth);

      // make sure we only got one
      if (names.size() != 1) {
        SourceLocator loc = dir->getLocation ();
        mMsgContext->DirNeed1Clk(&loc, names.size());
      } else {
        // Add this name to our event list
        STSymbolTableNode* node = *(names.begin());
        ClockEvent clockEvent(edge, node);
        clockEvents->push_back(clockEvent);
      }

      // We are now looking for an edge again
      lookForEdge = true;
    }
  } // for

  // We had to have an even number of tokens by construction
  INFO_ASSERT(lookForEdge, "Odd number of tokens.");
  return ret;
}

bool IODBNucleus::recordPathValue(IODBDesignDirective *dir,
                                  NameValueMap* nameValMap,
                                  ProtectedMode protectedMode,
                                  bool findModules,
                                  IODBDirective::DirectiveMode dirMode)
{
  // Parse the value
  DynBitVector value;
  bool status = convertValue(dir->getValue ().c_str(), dir->getLocation (), value);
  if (status)
  {
    // Assign the values
    NameSet names;
    if (!recordPaths(dir, &names, protectedMode, false, findModules, dirMode))
      return false;
    for (NameSet::iterator p = names.begin(); p != names.end(); ++p)
    {
      const STSymbolTableNode* name = *p;

      // Now check the user type info for unsupported types, which is basically
      // anything that is not based on bit or std_logic. This applies only to 
      // VHDL, since the check below will take care of unsupported types for Verilog.
      STSymbolTable* elabST = mDDSymTabs->getElabSymTab();
      STSymbolTableNode* elabNode = elabST->safeLookup(name);      
      ST_ASSERT(elabNode != NULL, name);

      IODBDesignNodeData* bomData = IODBDesignDataBOM::castBOM(elabNode);
      const UserType* ut = bomData->getUserType();
      StringAtom* intrinsicTypeName = ut->getIntrinsicTypeName();

      // All intrinsic types kike std_logic, bit, bit_vector and std_logic_vector
      // are allowed, except for string.
      if ((strcmp(intrinsicTypeName->str(), "vhdlunknown") == 0) ||
	  (strcmp(intrinsicTypeName->str(), "string") == 0))
      {
	NUBase* base = getNucleusObject(name);
	NUNet* net = dynamic_cast<NUNet*>(base);
	NU_ASSERT(net != NULL, base);
	NUModule* module = net->getScope()->getModule();
	SourceLocator loc = dir->getLocation ();
	
	mMsgContext->UnsupportedTypeForTieNet(&loc, 
					      module->getName()->str(),
					      net->getName()->str());
	continue;
      }

      // Make sure we don't have a duplicate directive. Not that we
      // print the warning during parsing, but it wasn't easy to cull
      // the directives so here we simply ignore the duplicates
      // silently. If we didn't, we would have a memory leak.
      NameValueMap::iterator pos = nameValMap->find(name);
      if (pos == nameValMap->end())
      {
        const DynBitVector* val = mBVPool->alloc(value);
        nameValMap->insert(NameValueMap::value_type(name, val));
        
      } 
      else if (dir->getDirective () == IODBDirective::eTieNet) 
      {
        // Make sure we have the same value for all tie nets
        const DynBitVector* otherVal = pos->second;
        if (value != *otherVal) 
	{
	  NUBase* base = getNucleusObject(name);
	  NUNet* net = dynamic_cast<NUNet*>(base);
	  NU_ASSERT(net != NULL, base);
	  NUModule* module = net->getScope()->getModule();
	  SourceLocator loc = dir->getLocation ();

          mMsgContext->DuplTieNet(&loc, 
				  module->getName()->str(),
                                  net->getName()->str());
        }
      }
    } // for
  } // if
  return true;                  // no warning for now on empty line (?)
} // bool IODBNucleus::recordPathValue

bool IODBNucleus::adjustSeverity(MsgContextBase::Severity sev, StrToken* tok,
                          const SourceLocator& loc)
{
  bool ret = true;
  const char* mneumonic;
  while ((*tok)(&mneumonic))
  {
    bool msgIdValid = false;;
    bool msgChangeValid = false;
    mMsgContext->putMsgSeverity(mneumonic, sev, &msgIdValid, &msgChangeValid);
    if ( not msgIdValid ) {
      mMsgContext->DirNoMessage(&loc, mneumonic);
    }
    if ( not msgChangeValid ) {
      MsgContextBase::Severity orig;
      SInt32 msgNo;
      StringUtil::parseNumber(mneumonic, &msgNo);
      mMsgContext->getMsgSeverity(msgNo, &orig);
      mMsgContext->IgnoringMsgSeverityAdjust(&loc, MsgContextBase::severityToString(sev), msgNo, MsgContextBase::severityToString(orig));
    }
  }
  return ret;
}

bool IODBNucleus::adjustDefault(UInt32* number,
                                StrToken* tok, const SourceLocator& loc)
{
  bool ret = true;
  if (tok->atEnd() || !tok->parseNumber(number))
  {
    mMsgContext->DirInvalidNumber(&loc, **tok);
  }
  return ret;
}

void
IODBNucleus::removeInvalidDeposits(STSymbolTable* symtab, 
                                   CbuildSymTabBOM* symTabBOM)
{
  // The primary ports are stored in a vector.  Create a set of
  // primary ports for easy querying.
  //
  // *NOTE*: loopPrimaryPorts() is empty during the global
  //         optimizations elaboration. Not sure if this is a
  //         bug or a feature.
  NameSet primPorts;
  for (NameVecLoop p = loopPrimaryPorts(); ! p.atEnd(); ++p) {
    primPorts.insert(*p);
  }

  // Set of a depositable nodes that need to be removed from the
  // master set
  NameSet doomedDeps;
  for (NameSetLoop p = loopDeposit(); ! p.atEnd(); ++p) {
    // Get the elaborated net. If a symbol table was passed in, we
    // need to translate from the IODB symbol table to the given
    // symbol table.
    STSymbolTableNode* node = getNode(*p, symtab);
    NUNetElab* netElab = getNet(node, symtab);

    // Check the deposit type.  When removing deposits below, we need
    // to know if this is just an OnDemand implied deposit.
    // loopDeposit() makes the depositable nets look like a set, but
    // it's really a map with the deposit type as the entry's value.
    DepositTypes depType = static_cast<DepositTypes>(mDepositNets[*p]);
    bool idleDepositOnly = (depType == 1 << eDTODIdleDeposit);

    // Get the nodes for this deposit node. This can be more than one
    // if it is a split port.
    STAliasedLeafNodeVector nodes;
    if (netElab != NULL) {
      STAliasedLeafNode* leaf = netElab->getSymNode();
      bool isExpr = symTabBOM->getMappedExprLeafNodes(leaf, &nodes);
      if (!isExpr) {
        // Not an expression, there is just the one node
        nodes.push_back(node->castLeaf());
      }
    }

    // Walk the list of nodes and print warnings/mark the node for
    // removal if it is a primary input/bid. We remove a split port if
    // all splits are primary inputs or bids.
    bool completelyPrimaryIn = true;
    for (Loop<STAliasedLeafNodeVector> l(nodes); !l.atEnd(); ++l) {
      // Get the net for this node, we don't get the net elab because
      // that is potentially a different node. We want to know if the
      // actual deposit net is a primary input or bid.
      const STAliasedLeafNode* identNode = *l;
      NUNet* unelabNet = NUNet::find(identNode);
      if (unelabNet->isPrimaryInput() || unelabNet->isPrimaryBid()) {
        // Only warn if we are actually are looking at a primary port
        // of the original design.  Also, if this is depositable only
        // as a side effect of being marked an OnDemand idle deposit,
        // skip the warning because the user didn't actually request
        // the deposit.

        if ((primPorts.count(node) != 0) && !idleDepositOnly) {
          UtString hier_name;
          node->compose(&hier_name);
          mMsgContext->DepositPrimaryInputOrBid(hier_name.c_str());
        }
        doomedDeps.insert(const_cast<STAliasedLeafNode*>(identNode));

      } else {
        // The source net is not a primary.
        completelyPrimaryIn = false;
      }
    } // for

    // If all leafs of the expression are invalid deposits, remove
    // it. Note that if this is not an expression net, we will add
    // this node a second time to the set. The set prunes the
    // duplicate.
    if (completelyPrimaryIn) {
      doomedDeps.insert(node);
    }
  } // for

  // Walk the set of deposit nets to remove. We don't do it in the
  // loop above because we don't want to iterate and remove in the
  // same pass.
  for (NameSet::UnsortedLoop p = doomedDeps.loopUnsorted(); !p.atEnd(); ++p) {
    STSymbolTableNode* node = *p;
    removeDeposit(node);
  }
} // IODBNucleus::removeInvalidDeposits

void IODBNucleus::applySplitNetDeposits(ReachableAliases* reachables,
                                        STSymbolTable* symtab,
                                        CbuildSymTabBOM* symTabBOM)
{
  // If any of the depositable nets are split, then set the mark on
  // the split nets.
  for (NameSetLoop p = loopDeposit(); ! p.atEnd(); ++p) {
    // Get the elaborated net. If a symbol table was passed in, we
    // need to translate from the IODB symbol table to the given
    // symbol table.
    STSymbolTableNode* node = getNode(*p, symtab);
    NUNetElab* netElab = getNet(node, symtab);
    if (netElab != NULL) {
      STAliasedLeafNode* leaf = netElab->getSymNode();
      STAliasedLeafNodeVector nodes;
      if (symTabBOM->getMappedExprLeafNodes(leaf, &nodes)) {
        // We cannot have a depositable node that has a constant
        // expression.
        ST_ASSERT(! nodes.empty(), leaf);

        // Go through the leaf nodes and mark them depositable.
        for (Loop<STAliasedLeafNodeVector> l(nodes); !l.atEnd(); ++l) {
          const STAliasedLeafNode* identNode = *l;
          netElab = NUNetElab::find(identNode);
          NUNet* unelabNet = NUNet::find(identNode);
          if (unelabNet->isPrimaryInput() || unelabNet->isPrimaryBid()) {
            // Remove the depositable attribute from the NUNet
            removeDepositableNUNet(unelabNet);

          } else {
            // Mark the depositable net
            unelabNet->putIsDepositable(true);
            reachables->rememberDepositable(netElab);
          }

          // Still run through the aliases of the primary port and
          // mark those depositable for the expression nets. We handle
          // the non-expression nets in depositNetMarker.
          markReachableAliasesDepositable(netElab, reachables);
        }
      } // if
    } // if
  } // for
} // void IODBNucleus::applySplitNetDeposits

// This function is responsible for propagating the deposit
// information to various data structures. While doing so it also
// validates that the deposit directive is placed on valid nets. Any
// deposits on primary inputs or bids is unecessary.
//
// The data modified structures are:
//
//  - The I/O DB mDepositNets is updated (primaries removed)
//
//  - A passed in reachable class is populated with the depositable
//    elaborated nets.
//
//  - All NUNet's in the alias ring for a depositable net are marked
//    as depositable.
//
// The various data structures require different markings with respect
// to deposits on primary inputs/bids. The I/O DB needs to have all
// deposits for aliases of a primary input/bid.  The reachable class
// and NUNet's in the alias ring should not have any deposits if any
// alias is a primary input or bid.
//
// *NOTE*: The handling of port split nets looks suspicious with
//         respect to the deposit marking. It should be investigated.
void IODBNucleus::resolveDepositable(CbuildSymTabBOM* symTabBOM,
                                     ReachableAliases *reachables,
                                     STSymbolTable* symtab,
                                     bool markDepositAliases)
{
  // This routine has three passes:
  //
  // 1. Walk the deposits and remove any deposits that are directly on
  //    primary inputs/bids.
  //
  // 2. Walk the deposits and mark depositable split nets
  //
  // 3. Walk the deposits and apply the deposit marking to aliases
  //    unless any alias is a primary input/bid. If so, remove them.
  removeInvalidDeposits(symtab, symTabBOM);
  applySplitNetDeposits(reachables, symtab, symTabBOM);
  depositNetMarker(reachables, symtab, markDepositAliases);
}

void 
IODBNucleus::getDepositableAliases(const NUNetElab* netElab,
                                   STAliasedLeafNodeVector* nodes)
{
  CbuildSymTabBOM* bomManager;
  bomManager = dynamic_cast<CbuildSymTabBOM*>(mDesignTable->getFieldBOM());

  STAliasedLeafNode* master = netElab->getSymNode();
  for (STAliasedLeafNode::AliasLoop l(master); !l.atEnd(); ++l) {
    // Check if this alias
    STAliasedLeafNode* alias = *l;
    if (isDepositable(alias)) {
      nodes->push_back(alias);
    }

    // This might be a port split node. If so, check the expression
    // leaf nodes
    STAliasedLeafNodeVector exprLeafNodes;
    CbuildSymTabBOM::getOriginalNodes(alias, &exprLeafNodes);
    for (STAliasedLeafNodeVector::iterator i = exprLeafNodes.begin(), e = exprLeafNodes.end();
         i != e; ++i)
    {
      const STAliasedLeafNode* origNode = *i;
      if (isDepositable(origNode))
        nodes->push_back(origNode);
    }
  }
}

void IODBNucleus::resolveInaccurate()
{
  for (STSymbolTable::NodeLoop loop = mDesignTable->getNodeLoop();
       not loop.atEnd();
       ++loop) {
    STSymbolTableNode * node = *loop;
    STAliasedLeafNode * leaf = node->castLeaf();
    if (leaf and (leaf==leaf->getMaster())) {
      NUNetElab * master = NUNetElab::find(leaf);
      if (master and master->queryAliases(&NUNet::isInaccurate)) {
        declareInvalidWaveNet(leaf);
      }
    }
  }
}

void IODBNucleus::resolveOnDemandExcluded()
{
  // For each node in the OnDemand excluded set, add all its aliases
  // as well.  Keep a local set of the nodes to add, and merge after
  // we loop.
  NameSet new_nodes;
  for (NameSetLoop p = loopOnDemandExcluded(); ! p.atEnd(); ++p) {
    STSymbolTableNode *node = *p;
    STAliasedLeafNode *leaf = node->castLeaf();
    ST_ASSERT(leaf, node);
    for (STAliasedLeafNode::AliasLoop a(leaf); !a.atEnd(); ++a) {
      STAliasedLeafNode *alias = *a;
      new_nodes.insert(alias);
    }
  }

  mOnDemandExcludedNets.insertSet(new_nodes);
}


void 
IODBNucleus::findCollapseNets(STSymbolTable* symtab)
{
  if (mCollapseNets != NULL) {
    mCollapseNets->clear();
  }
  else {
    mCollapseNets = new NUNetElabSet;
  }

  for (NameSetMapLoop l = loopCollapseClocks(); !l.atEnd(); ++l) {
    // Add the designated master clock
    STSymbolTableNode* node = l.getKey();
    NUNetElab* netElab = getNet(node, symtab);
    if (netElab != NULL) {
      mCollapseNets->insert(netElab);
    }

    // Get the designated equivalent clocks
    NameSet* names = l.getValue();
    for (NameSet::SortedLoop n = names->loopSorted(); !n.atEnd(); ++n) {
      STSymbolTableNode* subNode = *n;
      netElab = getNet(subNode, symtab);
      if (netElab != NULL) {
        mCollapseNets->insert(netElab);
      }
    }
  }
} // IODBNucleus::findCollapseNets

bool IODBNucleus::isCollapseSubordinate(NUNetElab* net) const {
  return (mCollapseNets != NULL) &&
    (mCollapseNets->find(net) != mCollapseNets->end());
}


void IODBNucleus::clearCollapseNets() {
  if (mCollapseNets != NULL) {
    delete mCollapseNets;
    mCollapseNets = NULL;
  }
}


bool IODBNucleus::checkDirectives(STSymbolTable *symboltable)
{
  bool success = true;
  if (mParseTable != NULL) {
    for (IOTable::iterator p = mParseIOTable->begin(); p != mParseIOTable->end(); ++p) {
      const STSymbolTableNode* ionode = p->first;
      const STSymbolTableNode* designNode = symboltable->lookup(ionode);
      if (designNode != NULL) {
        mCheckedDirectives.insert (ionode);
        success = false;
      }
    }
  }
  return success;
}

bool IODBNucleus::resolveDirectives()
{
  bool ret = true;
  if (mParseTable != NULL)
    {
      // NULL out the mParseTable entry, to indicate to declareSignal that
      // we are in post-elab mode and can therefore find net-elabs.  Save
      // the pointer to the symbol-table to delete it after remapping all the
      // names it holds.
      STSymbolTable* parseTable = mParseTable;
      mParseTable = NULL;

      for (IOTable::iterator p = mParseIOTable->begin();
           p != mParseIOTable->end(); ++p)
        {
          const STSymbolTableNode* ionode = p->first;
          const SourceLocator& loc = p->second;
          const STSymbolTableNode* designNode = mDesignTable->lookup(ionode);
          if (designNode != NULL) {
            mapSymNodeToLoc(designNode, loc);
          } else if (ionode->castLeaf () == NULL) {
            // see bug 3770.  A scope in which no variables are declared
            // will not find a place in the elaborated symbol table.  Don't
            // give an error about that.  Just ignore it
          } else if (mCheckedDirectives.find (ionode) != mCheckedDirectives.end ()) {
            mMsgContext->NetHasGone (&loc, *ionode);
          } else {
            mMsgContext->NoSuchNet(&loc, *ionode);
          }
        }

      ret &= fixSet(&mForcedNets);
      ret &= fixSet(&mExposedNets);
      ret &= fixSet(&mFastClocks);
      ret &= fixSet(&mSlowClocks);
      ret &= fixSet(&mFastResets);
      ret &= fixIntMap(&mObservedNets);
      ret &= fixSet(&mTracedNets);
      ret &= fixIntMap(&mDepositNets);
      ret &= fixSet(&mIgnoreOutputNets);
      ret &= fixSet(&mInputs);
      ret &= fixSet(&mOutputs);
      ret &= fixSet(&mBidis);
      ret &= fixSet(&mClocks);
      ret &= fixSet(&mPrimaryClocks);
      ret &= fixSet(&mAsyncs);
      ret &= fixSetMap(&mAsyncOutputs);
      ret &= fixSet(&mAsyncDeposits);
      ret &= fixSet(&mAsyncPosResets);
      ret &= fixSet(&mAsyncNegResets);
      ret &= fixSetMap(&mCollapsedClocks);
      ret &= fixIntMap(&mClockSpeeds);
      ret &= fixValueMap(mConstNets, false); // no err on computed consts missing
      ret &= fixValueMap(mTieNets, true);
      ret &= fixClockEvents(mOutputsTimingEvents);
      ret &= fixClockEvents(mInputsTimingEvents);
      ret &= fixSet(&mWrapPort2StateNets);
      ret &= fixSet(&mWrapPort4StateNets);
      ret &= fixSet(&mDelayedClocks);
      ret &= fixSet(&mOnDemandIdleDepositNets);
      ret &= fixSet(&mOnDemandExcludedNets);
      delete mParseIOTable;
      mParseIOTable = NULL;
      delete parseTable;
      delete mInstanceMap;
      mInstanceMap = NULL;
    } // if

  // Populate the hidden modules list with branch nodes
  if (! mHideModuleNames.empty())
  {
    // walk the symtab looking at each module instance's module name
    for (STSymbolTable::NodeLoop symIter = mDesignTable->getNodeLoop();
         ! symIter.atEnd(); ++symIter)
    {
      const STSymbolTableNode* symNode = *symIter;
      const STBranchNode* bnode;
      if ((bnode = symNode->castBranch()))
        checkMarkHidden(bnode);
    }
    
  }
  return ret;
} // bool IODBNucleus::resolveDirectives

void
IODBNucleus::populateDesignOutputs(NUDesign* design,
                                   CbuildSymTabBOM* symTabBOM,
                                   STSymbolTable* symtab)
{
  for (NameSetLoop p = loopObserved(); !p.atEnd(); ++p) {
    STSymbolTableNode* node = *p;
    NUNetElab* netElab = getNet(node, symtab);

    // Observe this elaborated net if it exists
    if (netElab != NULL) {
      design->observeNet(netElab);

      // This net may be a split port. If so, observe the nets that
      // make up its expression. We determine this by asking the
      // symbol table BOM if it is an expression and ask it to get
      // us the leaf nodes in the expression.
      STAliasedLeafNode* leaf = netElab->getSymNode();
      STAliasedLeafNodeVector nodes;
      bool isExpr = symTabBOM->getMappedExprLeafNodes(leaf, &nodes);

      // Go through the leaf nodes and mark them observable.
      STAliasedLeafNodeVector::const_iterator i;
      for (i = nodes.begin(); isExpr && (i != nodes.end()); ++i) {
        // Note that there is not need to translate these nodes
        // because they were gotten from the leaf node of the right
        // symbol table.
        const STAliasedLeafNode* identNode = *i;
        netElab = NUNetElab::find(identNode);
        mDesign->observeNet(netElab);
      }
    }
  }

  for (NameSetMapLoop p = loopCollapseClocks(); !p.atEnd(); ++p) {
    STSymbolTableNode* node = p.getKey();
    NUNetElab* netElab = getNet(node, symtab);

    if (netElab != NULL) {
      mDesign->addClockMaster(netElab);
    }
  }
} // void IODBNucleus::populateDesignData

void IODBNucleus::depositNetMarker(ReachableAliases *reachables,
                                   STSymbolTable* symtab,
                                   bool markDepositAliases)
{
  typedef UtArray<STSymbolTableNode*> NodeVec;
  for (NameSetLoop p = loopDeposit(); !p.atEnd(); ++p)
  {
    NUNetElab* net = getNet(*p, symtab);
    if (net != NULL)
    {
      // Run through aliases once, checking for primary input or bid
      bool is_primary_in_or_bid = false;
      for (NUNetElab::NoHierRefAliasLoop aliases = net->loopNoHierRefAliases();
           not aliases.atEnd();
           ++aliases) {
        NUNet* unelab_net = NUNet::find(*aliases);
        if (unelab_net->isPrimaryInput() or unelab_net->isPrimaryBid()) {
          is_primary_in_or_bid = true;
          break;
        }
      }
      
      // Issue msg if this is a primary input or bid specified as depositable.
      // Otherwise, mark the nets.
      if (is_primary_in_or_bid) {
        // remove the net from the NUNet set
        removeDepositableNUNet(net->getNet());
      } else {
        markReachableAliasesDepositable(net, reachables);
        reachables->rememberDepositable(net);

        // Also add the aliases to the io table so that all the
        // aliases of a deposit net are depositable.
        //
        // This was needed to make test/constprop/undriven5.v work
        // correctly with testdriver.
        if (markDepositAliases) {
          for (NUNetElab::NoHierRefAliasLoop aliases = 
                 net->loopNoHierRefAliases();
               not aliases.atEnd();
               ++aliases) {
            STAliasedLeafNode* alias = *aliases;
            mapSymNodeToLoc(alias, net->getNet()->getLoc());
          }
        }
      }
    } // if
  } // for
}


void IODBNucleus::markReachableAliasesDepositable(NUNetElab *net,
                                                  ReachableAliases *reachables)
{
  for (STAliasedLeafNode::AliasLoop aliases(net->getSymNode());
       not aliases.atEnd();
       ++aliases) {
    STAliasedLeafNode *alias = *aliases;
    NUNet* unelab_net = NUNet::find(alias);
    // Don't mark primary inputs/bids as depositable
    if (unelab_net && 
        ! unelab_net->isPrimaryInput() && 
        ! unelab_net->isPrimaryBid()) {
      // bug 3020 - Only mark reachable unelaborated nets as depositable.
      if (reachables->query(alias)) {
        markDepositable(unelab_net);
      }
    }
  }
}


void IODBNucleus::resolveForce()
{
  // Mark the forcible nets.  This is an iterative process, because
  // nets are forced by elaborated name.  All aliases of the NUNetElab
  // get NUNets that are storaged locations or are locally written
  // marked forcible.  And all instances of those those NUNets get
  // marked forcible, which must be discovered by a design walk.  Note
  // that in a gate-level design, forcing the output of a flop will
  // cause all instances of that flop to have its outputs made
  // forcible.  Bottom-flattening is extremely important to avoid
  // making every net in the whole design be forcible.
  //
  // This is needed for forcible nets, and not depositable nets,
  // because a net's C++ type must be declared consistently
  // throughout the hierarchy.
  //
  // The multiple passes through this loop are needed in test/directives/force2.v,
  // when flattening is off.
  NUNetVector nets;
  UInt32 numForcedNets = 0;
  CbuildSymTabBOM* bomManager = dynamic_cast<CbuildSymTabBOM*>(mDesignTable->getFieldBOM());
  INFO_ASSERT(bomManager, "Invalid symtab type.");
    
  while (numForcedNets < mForcedNets.size())
  {
    numForcedNets = mForcedNets.size();
    
    // Walk through every elaborated net that is marked forcible,
    // and mark all the unelaborated nets.
    for (IODB::NameSetLoop p = loopForced(); !p.atEnd(); ++p)
    {
      STSymbolTableNode* node = *p;
      gatherMarkForcible(node, false);
      
      // check if the node is expression'ed and mark its subordinates
      if (bomManager->isExprMappedNode(node))
      {
        CarbonExpr* mappedExpr = bomManager->getMappedExpr(node);
        // we have an expressioned node
        // if the elab net was null, get the net for the original
        // net and mark it forcible
        if (getNet(node) == NULL)
        {
          NUBase* base = CbuildSymTabBOM::getNucleusObj(node);
          NUNet* nodeNet = dynamic_cast<NUNet*>(base);
          // The original node MUST have its net and we know it
          // isn't an elab net here
          ST_ASSERT(nodeNet, node);
          // mark it.
          markForcible(nodeNet);
        }
            
        // Now mark its subordinates.
        MarkForceWalk walker(this);
        walker.visitExpr(mappedExpr);
      }
    }

    // Walk through the design hierarchy, looking for nodes that are
    // not yet marked forcible, that have one or more NUNet* that
    // are unelaboratedly forcible.
    NUNetElabSet netElabs;
    for (STSymbolTable::NodeLoop i = mDesignTable->getNodeLoop();
         !i.atEnd(); ++i)
    {
      STSymbolTableNode* node = *i;
      if (!isForcible(node))
      {
        NUNetElab* net = NUNetElab::find(node);
        if ((net != NULL) && (netElabs.find(net) == netElabs.end()))
        {
          netElabs.insert(net);
          nets.clear();
          net->getAliasNets(&nets, false);
          STSymbolTableNode* orig_node = node;
          node = net->getSymNode();
          for (NUNetVectorIter i = nets.begin(); i != nets.end(); ++i)
          {
            const NUNet* unelabNet = *i;
            if (! unelabNet->isHierRef() && unelabNet->isForcible())
            {
              mForcedNets.insert(declareSignal(node));
              // fix up the location for the node
              mIOTable[node] = net->getNet()->getLoc();
              
              // If the added node is described by an expression or is
              // part of an expression,  add its subordinates or
              // backpointers.
              CarbonExpr* expr = CbuildSymTabBOM::getIdent(node->castLeaf());
              if (expr)
              {
                ForcibleExprBPWalk exprWalk(this);
                exprWalk.visitExpr(expr);
              }

              // Place the original node in the IO table so it is
              // available in the IO database. This guarantees that
              // all portsplit symbols for force wires/regs are written
              // into the IO db (bug 14894)
              if (orig_node != node)
              {
                NUNet* unelabNet = NUNet::find(orig_node);
                if (!unelabNet->isHierRef() && unelabNet->isForcible())
                {
                  declareSignal(orig_node);
                  // fix up the location for the orig_node
                  mIOTable[orig_node] = net->getNet()->getLoc();
                }
              }
              break;
            }
          }
        }
      }
    } // for
  } // while
} // void IODBNucleus::resolveForce

void IODBNucleus::gatherMarkForcible(STSymbolTableNode* node, bool forceMarking)
{
  NUNetElab* net = getNet(node);
  if (net != NULL)
  {
    NUNetVector nets;
    net->getAliasNets(&nets, true);
    for (NUNetVectorIter i = nets.begin(); i != nets.end(); ++i)
    {
      NUNet* unelabNet = *i;
      if (forceMarking || unelabNet->isWritten() ||
          unelabNet->isTriWritten() ||
          unelabNet->isHierRefWritten() ||
          unelabNet->isPrimaryPort() ||
          (net->getStorageNet() == unelabNet))
      {
        markForcible(unelabNet);
      }
    }
  }
}

bool IODBNucleus::fixSet(NameSet* ns)
{
  bool ret = true;
  NameSet tmp;
  for (NameSet::iterator p = ns->begin(); p != ns->end(); ++p)
  {
    STSymbolTableNode* ionode = *p;
    STSymbolTableNode* designNode = mDesignTable->lookup(ionode);
    if (designNode == NULL)
    {
      const SourceLocator& loc = (*mParseIOTable)[ionode];
      mMsgContext->NoSuchNet(&loc, *ionode);
    }
    else if (! isHierRef(designNode))
      tmp.insert(declareSignal(designNode));
  }
  ns->swap(tmp);
  return ret;
}

bool IODBNucleus::fixSetMap(NameSetMap* nsm)
{
  bool ret = true;
  NameSetMap tmp;
  for (NameSetMap::iterator p = nsm->begin(); p != nsm->end(); ++p)
  {
    STSymbolTableNode* ionode = p->first;
    STSymbolTableNode* designNode = mDesignTable->lookup(ionode);
    if (designNode == NULL)
    {
      const SourceLocator& loc = (*mParseIOTable)[ionode];
      mMsgContext->NoSuchNet(&loc, *ionode);
    }
    else if (! isHierRef(designNode))
    {
      NameSet* ns = p->second;
      ret &= fixSet(ns);
      tmp[designNode] = ns;
    }
  }
  nsm->swap(tmp);
  return ret;
} // bool IODBNucleus::fixSetMap

bool IODBNucleus::fixIntMap(NameIntMap* nsm)
{
  bool ret = true;
  NameIntMap tmp;
  for (NameIntMap::iterator p = nsm->begin(); p != nsm->end(); ++p)
  {
    STSymbolTableNode* ionode = p->first;
    STSymbolTableNode* designNode = mDesignTable->lookup(ionode);
    if (designNode != NULL && !isHierRef (designNode)) {
      tmp[designNode] = p->second;
    } else if (designNode != NULL) {
      // do nothing
    } else if (mCheckedDirectives.find (ionode) != mCheckedDirectives.end ()) {
      const SourceLocator& loc = (*mParseIOTable)[ionode];
      mMsgContext->NetHasGone (&loc, *ionode);
    } else {
      const SourceLocator& loc = (*mParseIOTable)[ionode];
      mMsgContext->NoSuchNet(&loc, *ionode);
    }
  }
  nsm->swap(tmp);
  return ret;
}

bool IODBNucleus::fixValueMap(NameValueMap* nvm, bool errOnMissing)
{
  bool ret = true;
  NameValueMap tmp;
  for (NameValueMap::iterator p = nvm->begin(); p != nvm->end(); ++p)
  {
    const STSymbolTableNode* ionode = p->first;
    const DynBitVector *dbv = p->second; // 2.95.3b won't compile without named temp
    STSymbolTableNode* designNode = mDesignTable->lookup(ionode);

    // Be careful because a node that is visible during constant propagation
    // may not survive till elaboratation.  An example of this is
    //     test/concat/zero_rpt_concat3.v
    // The signal top.sub.out1 determined to be a constant, which is propagated
    // to top.out1.  That deadens the module instance and it is dead-logic
    // eliminated during local analysis, and therefore doesn't show in the
    // elaborated symbol table.  We could, at this point, add it to the
    // elaborated symbol table anyway.  But for now just skip this one.
    if (designNode == NULL) {
      if (errOnMissing) {
        const SourceLocator& loc = (*mParseIOTable)[ionode];
        mMsgContext->NoSuchNet(&loc, *ionode);
      }
    }
    else {
      declareSignal(designNode);
      tmp.insert(NameValueMap::value_type(designNode, dbv));
    }
  }
  nvm->swap(tmp);
  return ret;
} // bool IODBNucleus::fixValueMap

bool IODBNucleus::fixClockEvents(ClockEvents* clockEvents)
{
  bool ret = true;
  for (ClockEventsLoop l(*clockEvents); !l.atEnd(); ++l) {
    ClockEvent& clockEvent = *l;
    const STSymbolTableNode* node = clockEvent.second;
    STSymbolTableNode* designNode = mDesignTable->lookup(node);
    if (designNode == NULL) {
      const SourceLocator& loc = (*mParseIOTable)[node];
      mMsgContext->NoSuchNet(&loc, *node);
    } else {
      clockEvent.second = designNode;
    }
  }
  return ret;
}

void IODBNucleus::ternaryGateOptimization(NUModule* mod)
{
  mod->setTernaryGateOptimization();
}

void IODBNucleus::hideModule(NUModule* mod)
{
  mod->setHidden();
}

void IODBNucleus::noAsyncResets(NUModule* mod)
{
  mod->setNoAsyncResets();
}

void IODBNucleus::disableOutputSysTasks(NUModule* mod)
{
  mod->setDisableOutputSysTasks();
}
void IODBNucleus::enableOutputSysTasks(NUModule* mod)
{
  mod->setEnableOutputSysTasks();
}

void IODBNucleus::flattenModule(NUModule* mod) {
  bool addThisName = true;
  const char* modName = mod->getOriginalName()->str();
  if (mod->isFlattenAllow()) {
    // warn: allow marked on module where flattening is forced; remove allow entry.
    mMsgContext->FlattenAllowAndModule(modName);
  }

  else if (mod->isFlattenDisallow()) {
    // error: disallow marked on module where flattening is forced.
    mMsgContext->FlattenDisallowAndModule(modName);
  }

  else if (mod->isFlattenContents()) {
    // warn: forcing both inclusion and exclusion; remove contents entry.
    mMsgContext->FlattenModuleAndModuleContents(modName);
  }

  if (addThisName) {
    mod->setFlattenModule();
  }
} // void IODBNucleus::flattenModule

void IODBNucleus::flattenModuleContents(NUModule* mod) {
  bool addThisName = true;
  const char* modName = mod->getOriginalName()->str();
  if (mod->isFlattenAllow()) {
    // warn: allow marked on module where flattening is forced; remove allow entry.
    mMsgContext->FlattenAllowAndModuleContents(modName);
  }
  else if (mod->isFlattenDisallow()) {
    // error: disallow marked on module where flattening is forced.
    mMsgContext->FlattenDisallowAndModuleContents(modName);
    addThisName = false;
  }
  else if (mod->isFlattenModule()) {
    // warn: forcing both inclusion and exclusion (this is nop)
    mMsgContext->FlattenModuleAndModuleContents(modName);
    addThisName = false;
  }

  if (addThisName) {
    mod->setFlattenContents();
  }
}

void IODBNucleus::allowFlattening(NUModule* mod) {
  bool addThisName = true;
  const char* modName = mod->getOriginalName()->str();
  if (mod->isFlattenDisallow()) {
    // error: allow/disallow conflict
    mMsgContext->FlattenAllowAndDisallow(modName);
    addThisName = false;
  }

  else if (mod->isFlattenModule()) {
    // warn: allow marked on module where flattening is forced (this is a nop)
    mMsgContext->FlattenAllowAndModule(modName);
    addThisName = false;
  }
  
  if (mod->isFlattenContents()) {
    // warn: allow marked on module where flattening is forced (this is a nop)
    mMsgContext->FlattenAllowAndModuleContents(modName);
    addThisName = false;
  }

  if (addThisName) {
    mod->setFlattenAllow();
  }
}

void IODBNucleus::disallowFlattening(NUModule* mod) {
  bool addThisName = true;
  const char* modName = mod->getOriginalName()->str();
  if (mod->isFlattenAllow()) {
    // error: allow/disallow conflict
    mMsgContext->FlattenAllowAndDisallow(modName);
    addThisName = false;
  }

  else if (mod->isFlattenModule()) {
    // error: allow marked on module where flattening is forced (this is a nop)
    mMsgContext->FlattenDisallowAndModule(modName);
    addThisName = false;
  }

  else if (mod->isFlattenContents()) {
    // warn: allow marked on module where flattening is forced (this is a nop)
    mMsgContext->FlattenDisallowAndModuleContents(modName);
    addThisName = false;
  }

  if (addThisName) {
    mod->setFlattenDisallow();
  }
}


void IODBNucleus::checkModuleDirectives() 
{
  resolveModuleDirective(mFlattenModuleNames,
                         &IODBNucleus::flattenModule,
                         IODBDirective::eFlattenModule);
  resolveModuleDirective(mFlattenModuleContentsNames,
                         &IODBNucleus::flattenModuleContents,
                         IODBDirective::eFlattenModuleContents);
  resolveModuleDirective(mAllowFlatteningModuleNames,
                         &IODBNucleus::allowFlattening,
                         IODBDirective::eAllowFlattening);
  resolveModuleDirective(mDisallowFlatteningModuleNames,
                         &IODBNucleus::disallowFlattening,
                         IODBDirective::eDisallowFlattening);
  resolveModuleDirective(mTernaryGateOptimization,
                         &IODBNucleus::ternaryGateOptimization,
                         IODBDirective::eHideModule);
  resolveModuleDirective(mHideModuleNames,
                         &IODBNucleus::hideModule,
                         IODBDirective::eHideModule);
  resolveModuleDirective(mNoAsyncResetNames,
                         &IODBNucleus::noAsyncResets,
                         IODBDirective::eNoAsyncReset);
  resolveModuleDirective(mDisableOutputSysTasksNames,
                         &IODBNucleus::disableOutputSysTasks,
                         IODBDirective::eDisableOutputSysTasks);
  resolveModuleDirective(mEnableOutputSysTasksNames,
                         &IODBNucleus::enableOutputSysTasks,
                         IODBDirective::eEnableOutputSysTasks);
}


void IODBNucleus::checkTFDirectives()
{
  for (AtomToAtomSet::iterator iter = mInlineTFNames.begin();
       iter != mInlineTFNames.end();
       ++iter) {
    StringAtom * moduleAtom = iter->first;
    const char* moduleName = moduleAtom->str();

    // Inspect the module name from the directive. If it contains * or ? then
    // it is wildcarded
    bool is_wildcard = false;
    if (moduleName [0] == '\\') {
      // an escaped verilog name; do not inspect for wildcard characters
    } else {
      for (UInt32 i = 0; moduleName [i] != '\0' && !is_wildcard; i++) {
        if (moduleName [i] == '*' || moduleName [i] == '?') {
          is_wildcard = true;
        }
      }
    }
    NUModuleList modules;
    findMatchingModules(&modules, moduleName);

    if (modules.empty()) {
      mMsgContext->DirNoSuchModule(moduleName, "inline");
    } else {
      AtomSet & tfAtoms = iter->second;
      AtomSet tfMatched;
      for (NUModuleList::iterator miter = modules.begin();
           miter != modules.end(); 
           ++miter) {
        NUModule* module = *miter;
        const char * realModuleName = module->getOriginalName()->str();

        for (AtomSet::iterator titer = tfAtoms.begin();
             titer != tfAtoms.end();
             ++titer) {
          StringAtom * tfAtom = *titer;
          const char * tfName = tfAtom->str();
          NUTaskList tfs;
          findMatchingTasks(module,tfName,&tfs);
          if (!tfs.empty()) {
            tfMatched.insert (tfAtom);
            for (NUTaskList::iterator tfiter = tfs.begin();
                 tfiter != tfs.end();
                 ++tfiter) {
              NUTask * tf = *tfiter;
              if (tf->hasHierRef()) {
                mMsgContext->DirHierRefTask(tfName,realModuleName);
              }
              tf->putRequestInline(true);
            }
          } else if (is_wildcard) {
            // do not output a "no such task/function" error because the module
            // name was wildcarded
          } else {
            mMsgContext->DirNoSuchModuleTF(tfName,realModuleName, "inline");
          }
        }
      }
      // if the module was wildcarded then check that each atom matched at
      // least one tf
      if (!is_wildcard) {
        // not a wildcard module name
      } else {
        for (AtomSet::iterator titer = tfAtoms.begin (); titer != tfAtoms.end (); ++titer) {
          StringAtom *tfAtom = *titer;
          if (tfMatched.find (tfAtom) != tfMatched.end ()) {
            // there was a least one match to the atom name
          } else {
            // a wildcarded module name was used but there were no matches to the tfName
            mMsgContext->DirNoSuchWildModuleTF (tfAtom->str (), moduleName, "inline");
          }
        }
      }
    }
  }
}

void IODBNucleus::addImplicitDirectives()
{
  // Currently, there's only one implicit directive (observing the
  // OnDemand idle status net), but I've structured this to support
  // additional ones in the future.

  // Directives are stored with a source locator.  Create a dummy one.
  SourceLocator loc = mSourceLocatorFactory->create("<internal>", 0);

  NameSet implicitObserves;

  // If OnDemand is enabled, its top-level status net needs to be
  // marked observable.
  StringAtom *odIdleAtom = mAtomicCache->getIntern(OnDemandMgr::sIdleSignalName());
  if (odIdleAtom) {
    // If the idle signal's StringAtom exists, the node should exist
    // in the symtab, too.
    for (STSymbolTable::RootIter l = mParseTable->getRootIter(); !l.atEnd(); ++l) {
      STBranchNode *root = (*l)->castBranch();
      // The parse symbol table is elaborated as needed, so ensure
      // that this branch is done.  As a bonus, this routine can find
      // the node matching this StringAtom.
      STSymbolTableNode *node = NULL;
      elaborateBranch(root, odIdleAtom, &node, true, false);
      ST_ASSERT(node, root);
      STAliasedLeafNode *leaf = node->castLeaf();
      // Add the leaf to the implicit observes
      implicitObserves.insert(leaf);
    }
  }

  // We need to update a few more things for each observable node
  for (NameSet::SortedLoop l = implicitObserves.loopSorted(); !l.atEnd(); ++l) {
    STSymbolTableNode *node = *l;
    // Save it in the IO table
    (*mParseIOTable)[node] = loc;
    // Add it to the protected observable set
    markProtectedObservable(node);
  }
  
  // Apply the observes
  ObserveMapCallback observeCB(&mObservedNets, eOTSimple, mMsgContext);
  observeCB.recordPaths(NULL, implicitObserves, eProtectedObservable);
}

bool IODBNucleus::convertValue(const char* val, const SourceLocator& loc,
                               DynBitVector& value)
{
  HdlVerilogExpr hdlExpr;
  UtString errMsg;
  HdlVerilogExpr::Status status = hdlExpr.parseConst(val, value, &errMsg);
  bool success = true;
  switch(status) {
    case HdlVerilogExpr::eError:
      mMsgContext->DirInvalidConst(&loc, errMsg.c_str());
      break;

    case HdlVerilogExpr::eWarning:
      mMsgContext->DirPotInvConst(&loc, errMsg.c_str());
      break;

    case HdlVerilogExpr::eSuccess:
      break;
  }
  return success;
}

bool IODBNucleus::isHierRef(const STSymbolTableNode* node) const
{
  bool ret = false;
  if (node->castLeaf())
  {
    const NUNet* net = NUNet::find(node);
    ret = net->isHierRef();
  }
  return ret;
}

void IODBNucleus::resolveConstants() 
{
  CbuildSymTabBOM* bomManager = dynamic_cast<CbuildSymTabBOM*>(mDesignTable->getFieldBOM());
  INFO_ASSERT(bomManager, "Invalid symtab type.");

  NameValueMap tmp;
  for (NameValueMap::iterator p =  mConstNets->begin(), e = mConstNets->end();
       p != e; ++p)
  {
    const STSymbolTableNode* symNode = p->first;
    const STAliasedLeafNode* node = symNode->castLeaf();
    ST_ASSERT(node, symNode);
    const DynBitVector* dbv = p->second;
    NUNet* net = NUNet::find(node);
    NUNetElab * net_elab = NUNetElab::find(node->getMaster());

    // We do not want to preserve the constant if any of the following
    // conditions apply:
    // 1. The net is NULL, meaning it was probably optimized away.

    // 2. The net was marked mutable (observable, forcible).

    // 3. This net is aliased to a primary inout -- usually, this
    //    means that the port was coerced from output to inout during
    //    port coercion.

    // 4. This name has an expression map entry. This means that it
    //    has been port-split.

    if ((net == NULL) || 
        isProtectedMutable(net) || 
        (net_elab && net_elab->isPrimaryBid()) ||
        bomManager->isExprMappedNode(node)) {
      // not a constant, skip.
    } else {
      const STAliasedLeafNode* storage = node->getStorage();
      declareSignal(storage);

      NameValueMap::iterator q = tmp.find(storage);
      
      if (q != tmp.end()) {
        const DynBitVector* oldBvPtr = q->second;
        DynBitVector oldBv(*oldBvPtr);

        // merge the two bitvectors
        UInt32 sz = oldBv.size();
        ST_ASSERT(sz == dbv->size(), storage);
        UInt32 numBits = sz / 3;
        ST_ASSERT(numBits*3 == sz, storage); // should have been a multiple of 3
        for (UInt32 i = 0; i < numBits; ++i) {
          // We only have to merge if the new bitvector has something to say
          // for this bit
          if (dbv->test(i)) {
            UInt32 valBit = i + numBits;
            UInt32 xzBit = i + 2*numBits;

            // If the old bitvector was blank then set the value for
            // the new one
            if (!oldBv.test(i)) {
              oldBv.set(i);
              if (dbv->test(xzBit)) {
                oldBv.set(xzBit);
              }
              if (dbv->test(valBit)) {
                oldBv.set(valBit);
              }
            }
            else {
              // Both old & new claim a value for this bit.  Ignore Z contribs
              bool dbv_is_Z = !dbv->test(valBit) && dbv->test(xzBit);
              if (!dbv_is_Z) { // ignore new Z contribution

                if ((dbv->test(valBit) != oldBv.test(valBit)) ||
                    (dbv->test(xzBit) != oldBv.test(xzBit)))
                {
                  bool old_is_Z = !oldBv.test(valBit) && oldBv.test(xzBit);
                  if (old_is_Z) {
                    // pick the new value
                    if (!dbv->test(xzBit)) {
                      oldBv.reset(xzBit);
                    }
                    if (dbv->test(valBit))
                      oldBv.set(valBit);
                  }
                  else {
                    // Make it X
                    oldBv.set(valBit);
                    oldBv.set(xzBit);
                  }
                }
              }
            }
          }
        }
        tmp[storage] = mBVPool->alloc(oldBv);
      } // if tmp already has an entry
      else {
        tmp[storage] = dbv;
      }
    }
  }
  mConstNets->swap(tmp);
}

bool IODBNucleus::isConstant01(const STAliasedLeafNode* node, DynBitVector* val01) {
  NameValueMap::iterator p = mConstNets->find(node);
  if (p != mConstNets->end()) {
    const DynBitVector* val = p->second;
    NUNet* net = NUNet::find(node);
    ST_ASSERT(net, node);
    UInt32 netSize = net->getBitSize();

    /*
      The DynBitVector representing the size is complex.
      First of all, there are 3 bits per bit, because the 'val' knows
      whether each bit is
        known 0
        known 1
        known x
        known z
        unknown
      5 choices requires 3 bits.

      Second of all, it's aligned the way it is cause Mark thought
      that would be more efficient in the shell, because of the way
      that relates to the value/drive masks that are returned to the
      user in the Carbon C API.  I think he's probably right.
    */

    UInt32 numBaseWords = (netSize + 31)/32;
    UInt32 bvBitSize = numBaseWords * 32;
    NU_ASSERT(val->size() == 3*bvBitSize, net);

    val01->resize(netSize);
    val01->reset();

    // Walk through the bits of the raw value, which has 3 bits per bit.
    // Bit 0 is set if the value is known.
    for (UInt32 i = 0; i < netSize; ++i) {
      if (!val->test(i)) {
        return false;           // not known
      }
      if (val->test(i + 2*bvBitSize)) {
        return false;           // X or Z
      }
      if (val->test(i + bvBitSize)) {
        val01->set(i);          // copy 0/1 value
      }
    }
    return true;
  } // if
  return false;
} // bool IODBNucleus::isConstant01

void IODBNucleus::markAsyncResets() {
  for (NameSet::UnsortedCLoop l = mAsyncResetPaths.loopCUnsorted();
       !l.atEnd(); ++l)
  {
    STSymbolTableNode* node = *l;
    SymTabVal* data = SymTabBOM::castBOM(node->getBOMData());
    NUNet* net = dynamic_cast<NUNet*>(data->mNucleusObj);
    ST_ASSERT(net, node);
    mAsyncResetNets.insert(net);
    NUScope* scope = net->getScope();
    NUModule* mod = scope->getModule();
    mod->addAsyncResetNet(net->getNameLeaf());
  }
}


void IODBNucleus::copyUserTypeInfo()
{
  if (mUserTypeFactory->isEmpty()) {
    return; // Do nothing.
  }

  STSymbolTable* elabST = mDDSymTabs->getElabSymTab();
  for (STSymbolTable::NodeLoop looper = elabST->getNodeLoop(); !looper.atEnd(); ++looper)
  {
    STSymbolTableNode* node = *looper;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf != NULL)
    {
      IODBDesignNodeData* bomData = IODBDesignDataBOM::castBOM(leaf);
      const UserType* ut = bomData->getUserType();
      if (ut != NULL)
      {
        // This lookedup node can be a branch if the leaf is for a composite.
        STSymbolTableNode* designNode = mDesignTable->lookup(leaf);
        // The node could be null if the net got optimized out.
        if (designNode != NULL) {
          CbuildSymTabBOM::putUserType(designNode, ut);
          STBranchNode* branchNode = designNode->castBranch();
          if (branchNode != NULL) {
            copyFieldUserTypeInfo(branchNode, ut);
          }
        }
      }
    }
  }

  // Done with the use of design data symbol tables. Delete them to save space.
  deleteDesignDataSymTabs();
}


void IODBNucleus::copyFieldUserTypeInfo(STBranchNode* branch, const UserType* ut)
{
  UserType::Type typ = ut->getType();
  const UserStruct* us = NULL;
  if (typ == UserType::eStruct) {
    us = static_cast<const UserStruct*>(ut);
  } else if (typ == UserType::eArray) {
    const UserArray* ary = static_cast<const UserArray*>(ut);
    UInt32 numDims = ary->getNumDims();
    // The last dimension provides the single/multi-dimensional array element.
    const UserType* elem = ary->getDimElementType(numDims-1);
    INFO_ASSERT(elem->getType() == UserType::eStruct,
                "Branch node must have struct user type.");
    us = static_cast<const UserStruct*>(elem);
  } else {
    INFO_ASSERT((typ == UserType::eArray) || (typ == UserType::eStruct),
                "Branch node must have composite user type.");
  }
  // For every field of the struct, find the branch/leaf node. Add it's type info.
  UInt32 numFields = us->getNumFields();
  for (UInt32 fieldIdx = 0; fieldIdx < numFields; ++fieldIdx)
  {
    const UserType* fieldType = us->getFieldType(fieldIdx);
    StringAtom* fieldName = us->getFieldName(fieldIdx);
    STSymbolTableNode* designNode = mDesignTable->find(branch, fieldName);
    if (designNode != NULL) {
      CbuildSymTabBOM::putUserType(designNode, fieldType);
      STBranchNode* branchNode = designNode->castBranch();
      if (branchNode != NULL) {
        copyFieldUserTypeInfo(branchNode, fieldType);
      }
    }
  }
}

bool IODBNucleus::checkRemovedDirective(IODBDirective::DesignDirective dir, const char* path,
                                        const SourceLocator& loc, const char* value)
{
  // We don't currently have a way of deprecating directives, at least
  // as far as printing warnings during compilation.  As a result,
  // even though we may document that a directive is deprecated (or at
  // least not recommended), we can't remove it from the code without
  // causing abrupt "unknown directive" errors.
  //
  // Instead, we'll handle specific deprecated directives here with
  // custom messages.  They're alerts so a user can demote them and
  // have the compile continue (albeit without the directives'
  // functionality).
  //
  // After sufficient time has passed, these checks can be removed and
  // the directives can be completely removed.

  // MV observe/deposit points are now controlled through Model Studio
  if ((dir == IODBDirective::eMvObserve) || (dir == IODBDirective::eMvDeposit)) {
    MsgContext::Severity severity = mMsgContext->DirectiveRemoved(&loc, IODBDirective::dirToString(dir),
                                                                  "Refer to the Carbon Model Studio documentation for information on specifying Model Validation access.");

    // MV observes/deposits were just special versions of regular
    // observes/deposits.  If the alert was demoted, add the regular
    // directive.
    if (severity < MsgContextBase::eAlert) {
      IODBDirective::DesignDirective newDir;
      if (dir == IODBDirective::eMvObserve) {
        newDir = IODBDirective::eObserve;
      } else {
        newDir = IODBDirective::eDeposit;
      }
      addDesignDirective(newDir, path, loc, value);
    }

    return true;
  }

  // SystemC port types are now controlled through Model Studio
  if ((dir == IODBDirective::eWrapperPort2State) || (dir == IODBDirective::eWrapperPort4State)) {
    mMsgContext->DirectiveRemoved(&loc, IODBDirective::dirToString(dir),
                                  "Refer to the Carbon Model Studio documentation for information on specifying SystemC types.");
    return true;
  }

  return false;
}

