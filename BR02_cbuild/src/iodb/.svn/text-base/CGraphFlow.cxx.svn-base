// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "iodb/CGraphFlow.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUNetElabRef.h"
#include "nucleus/NUNetRefSet.h"
#include "symtab/STSymbolTable.h"
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STBranchNode.h"
#include "util/UtIStream.h"

typedef UtHashSet<NUUseDefNode*> UDNodeSet;

// Dump a connectivity graph.  The file format will declare all nets,
// and then the drivers that reference those nets.  For now we will
// ignore the unpleasant effect of redefining nets, and all the drivers
// to a net will be merged.  Later these should be separated via single
// state assignment (SSA).
//
// The file is a sequence of records, nets first.  The forms are:
// n (net), t (temp_net) i (if), c (case), b (black-box)
//
// n net_path
// f output_net_ref locator fanin_set node_record
// 
// fanin_set is "num fanin1 fanin2" where the fanins are also CGraph::Nodes
//
// The fanin for if-statements and case-statements is only for the select.
// the sub-nodes contain the branch-specific fanin
//
// node-record is one of:
//   i cond_expr then_flow else_flow
//   c sel_expr num expr_1 flow_1 expr_2 flow_2 ...
//   b dump_verilog
//   s edge_expr flow
//   u  (unbound)
//
// Assigns are black-boxes.  Expressions for if and case are given
// as text from -dumpVerilog.  Fanin-sets are given as "num net1 net2 ..."
//
// temp-nets are just nets, but there is a different record type.

CGraphFlow::CGraphFlow(NUNetRefFactory* netRefFactory, REAlias* alias,
                       SourceLocatorFactory* slc, AtomicCache* shared_cache)
  : CGraph(slc, shared_cache),
    mNetRefFactory(netRefFactory),
    mAlias(alias)
{
  mEmptyDesc = intern("");
}

CGraphFlow::~CGraphFlow() {
}

// Recursing through fanin can make the stack grow too large, so use a queue
// to defer that traversal.  If the subnode_symbol is NULL, then the node
// fanin will eventually be added to the general fanin set.  Otherwise it
// will be added for the subnode identified by that symbol.
void CGraphFlow::queueFanin(Node* node,
                            StringAtom* subnode_symbol,
                            FLNodeElab* fanin)
{
  Node* fanin_node = mFlowNodeMap[fanin];
  if (fanin_node != NULL) {
    // we already know about this fanin
    if (subnode_symbol == NULL) {
      node->addFanin(fanin_node);
    }
    else {
      node->addNode(subnode_symbol, fanin_node);
    }
  }
  else {
    // we don't know about it, so queue it up
    mFaninQueue.push_back(NodeConnection(node, fanin, subnode_symbol));
  }
}

void CGraphFlow::design(NUDesign* design) {
  //NUNetElabSet covered;

  // Walk over all observable outputs and collect the driving flow-nodes

  // Collect all the observable nets
  for (NUDesign::NameSetLoop p(design->loopObservable()); !p.atEnd(); ++p) {
    STSymbolTableNode* node = *p;

    NUNetElab* net_elab = NUNetElab::find(node);
    NUNet* net = net_elab->getNet();
    ST_ASSERT(net_elab, node);
    if (!net->isControlFlowNet()
        && !net->isExtNet()
        /* && covered.insertWithCheck(net_elab)*/)
    {
      STSymbolTableNode* net_node = net_elab->getSymNode();
      if (*net_node->strObject()->str() != '$') {
        CGraph::Node* cgnode =
          addNode(eObserve,
                  net_node,
                  mEmptyDesc,       // all bits
                  net_node->getParent(),
                  mEmptyDesc);
        for (NUNetElab::DriverLoop p(net_elab->loopContinuousDrivers());
             !p.atEnd(); ++p)
        {
          FLNodeElab* driver = *p;
          queueFanin(cgnode, NULL, driver);
        }
      }
    }
  }

  // collect all the design outputs
  STSymbolTable* symtab = design->getSymbolTable();
  for (STSymbolTable::RootIter p = symtab->getRootIter(); !p.atEnd(); ++p) {
    STSymbolTableNode* root_node = *p;
    STBranchNode* branch = root_node->castBranch();
    ST_ASSERT(branch, root_node);
    for (UInt32 i = 0, n = branch->numChildren(); i < n; ++i) {
      STSymbolTableNode* node = branch->getChild(i);
      if (node != NULL) {
        STAliasedLeafNode* leaf = node->castLeaf();
        if (leaf != NULL) {
          // must be a net
          NUNetElab* net_elab = NUNetElab::find(leaf);
          ST_ASSERT(net_elab, leaf);
          NUNet* net = net_elab->getNet();
          if (net->isOutput() || net->isBid()) {
            if (1 /*covered.insertWithCheck(net_elab)*/) {
              CGraph::Node* node =
                addNode(net->isOutput() ? eOutput : eBidirect,
                        net_elab->getSymNode(),
                        mEmptyDesc,       // all bits
                        net_elab->getSymNode()->getParent(),
                        mEmptyDesc);
              for (NUNetElab::DriverLoop p(net_elab->loopContinuousDrivers());
                   !p.atEnd(); ++p)
              {
                FLNodeElab* driver = *p;
                queueFanin(node, NULL, driver);
              }
            }
          }
        }
      }
    }
  }

  while (! mFaninQueue.empty()) {
    NodeConnection conn = mFaninQueue.front();
    mFaninQueue.pop_front();
    Node* fanin_node = processFlow(conn.mFanin);
    if (conn.mSym == NULL) {
      conn.mNode->addFanin(fanin_node);
    }
    else {
      conn.mNode->addNode(conn.mSym, fanin_node);
    }
  }
} // void CGraphFlow::design

CGraph::Node* CGraphFlow::processFlow(FLNodeElab* flow) {
  Node* node = mFlowNodeMap[flow];
  if (node != NULL) {
    UtString netBits;
    NUNetRefHdl ref = flow->getDefNetRef(mNetRefFactory);
    ref->printBits(&netBits, 0, true);
    const STSymbolTableNode* out_net = flow->getDefNet()->getSymNode();
    node->addOutNet(out_net, intern(netBits));
    return node;
  }

  NUUseDefNode* ud = flow->getUseDefNode();
  if (ud == NULL) {             // primary input
    node = unbound(flow);
  }                       
  else {
    if (ud->isSequential()) {
      node = makeNode(eSequential, flow);
    }
    else {
      node = makeNode(eBlackBox, flow);
    }
  }

  for (Fanin p(NULL, flow, eFlowIntoCycles); !p.atEnd(); ++p) {
    FLNodeElab* fanin = *p;
    queueFanin(node, NULL, fanin);
  }
  return node;
} // CGraph::Node* CGraphFlow::processFlow

CGraph::Node* CGraphFlow::unbound(FLNodeElab* flow) {
  NUNet* net = flow->getDefNet()->getNet();
  Type type = eUnbound;
  if (net->isPrimaryInput()) {
    type = eInput;
  }
  else if (net->isPrimaryBid()) {
    type = eBidirect;
  }
  else if (net->isForcible()) {
    type = eForce;
  }
  else if (net->isDepositable()) {
    type = eDeposit;
  }
  return makeNode(type, flow);
} // CGraph::Node* CGraphFlow::unbound

CGraph::Node* CGraphFlow::makeNode(Type type, FLNodeElab* flow) {
  UtString netBits;
  NUNetRefHdl ref = flow->getDefNetRef(mNetRefFactory);
  ref->printBits(&netBits, 0, true);

  StringAtom* desc = mEmptyDesc;
  NUUseDefNode* ud = flow->getUseDefNode();
  if (ud != NULL) {
    DumpVerilogMap::iterator p = mDumpVerilogMap.find(ud);
    desc = NULL;
    if (p == mDumpVerilogMap.end()) {
      UtString buf, line, finalized;
      ud->compose(&buf, NULL);

      // the compose method decorates the output with a large number
      // of inline and one-line comments, which we want to strip out
      // in this view.
      UtIStringStream is(buf.c_str());
      while (is.getline(&line)) {
        size_t pos = line.find(" // ");
        if (pos != UtString::npos) {
          // remove trailing whitespace too
          while ((pos > 0) && (line[pos - 1] == ' ')) {
            --pos;
          }
          line.resize(pos);
          if (pos != 0) {
            line << "\n";
          }
        }
        if (!line.empty()) {
          finalized << line;
        }
      }

      desc = intern(finalized);
      mDumpVerilogMap[ud] = desc;
    }
    else {
      desc = p->second;
    }
  }

  Node* node = addNode(type, 
                       flow->getDefNet()->getSymNode(),
                       intern(netBits),
                       flow->getHier(),
                       desc);
  mFlowNodeMap[flow] = node;
  return node;
}
