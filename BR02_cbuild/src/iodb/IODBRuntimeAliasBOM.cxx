// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtIOStream.h"
#include "util/Zstream.h"
#include "symtab/STBranchNode.h"
#include "symtab/STAliasedLeafNode.h"
#include "iodb/IODBRuntimeAliasBOM.h"

/*static*/ const char *IODBRuntimeAliasBOM::sSignature = "AliasDataBOM";
/*static*/ const UInt32 IODBRuntimeAliasBOM::sVersion = 1;

static const UInt32 scVersion = 1;

IODBRuntimeAliasDataBOM::IODBRuntimeAliasDataBOM () : mFlags (0)
{}

IODBRuntimeAliasDataBOM::~IODBRuntimeAliasDataBOM()
{}

void IODBRuntimeAliasDataBOM::print() const 
{
  UtIO::cout () << "<runtime-alias-data>" << UtIO::endl;
}

IODBRuntimeAliasBOM::IODBRuntimeAliasBOM()
{}

IODBRuntimeAliasBOM::~IODBRuntimeAliasBOM()
{}

STFieldBOM::Data IODBRuntimeAliasBOM::allocData()
{
  IODBRuntimeAliasDataBOM * obj = new IODBRuntimeAliasDataBOM;
  return static_cast<Data>(obj);
}

STFieldBOM::Data IODBRuntimeAliasBOM::allocBranchData()
{
  return allocData();
}

STFieldBOM::Data IODBRuntimeAliasBOM::allocLeafData()
{
  return allocData();
}

void IODBRuntimeAliasBOM::freeData(Data* bomdata)
{
  IODBRuntimeAliasDataBOM * obj = castBOM(*bomdata);
  delete obj;
  *bomdata = NULL;
}

void IODBRuntimeAliasBOM::freeBranchData(const STBranchNode*, Data* bomdata)
{
  freeData(bomdata);
}

void IODBRuntimeAliasBOM::freeLeafData(const STAliasedLeafNode*, Data* bomdata)
{
  freeData(bomdata);
}

void IODBRuntimeAliasBOM::printData(const Data bomdata) const
{
  const IODBRuntimeAliasDataBOM * obj = castBOM(bomdata);
  obj->print();
}

void IODBRuntimeAliasBOM::printLeaf(const STAliasedLeafNode* leaf) const
{
  printData(leaf->getBOMData());
}

void IODBRuntimeAliasBOM::printBranch(const STBranchNode* branch) const
{
  printData(branch->getBOMData());
}

IODBRuntimeAliasDataBOM * IODBRuntimeAliasBOM::castBOM(Data bomdata)
{
  return static_cast<IODBRuntimeAliasDataBOM*>(bomdata);
}

//! This casts a symbol table node to an IODBRuntimeAliasDataBOM
IODBRuntimeAliasDataBOM *IODBRuntimeAliasBOM::castBOM (const STSymbolTableNode *node)
{
  return static_cast<IODBRuntimeAliasDataBOM*>(node->getBOMData ());
}

void IODBRuntimeAliasBOM::writeBranchData(const STBranchNode*, ZostreamDB&,
                                 AtomicCache*) const
{
  INFO_ASSERT (false, "cannot write IODBRuntimeAliasBOM");
}

void IODBRuntimeAliasBOM::writeLeafData(const STAliasedLeafNode*, ZostreamDB&) const
{
  INFO_ASSERT (false, "cannot write IODBRuntimeAliasBOM");
}

//! Read data for a leaf
/*! \note The corresponding write method is NUAliasBOM::writeLeafData
 */
STFieldBOM::ReadStatus IODBRuntimeAliasBOM::readLeafData(STAliasedLeafNode *leaf, ZistreamDB &in, MsgContext*)
{
  IODBRuntimeAliasDataBOM *bom = castBOM (leaf->getBOMData ());
  in >> bom->mFlags;
  return eReadOK;
}

//! Read data for a branch
/*! \note The corresponding write method is NUAliasBOM::writeBranchData
 */
STFieldBOM::ReadStatus IODBRuntimeAliasBOM::readBranchData(STBranchNode *branch, ZistreamDB &in, MsgContext*)
{
  IODBRuntimeAliasDataBOM *bom = castBOM (branch->getBOMData ());
  UInt32 tmp;
  in >> tmp;
  bom->mHierFlags = static_cast <HierFlags> (tmp);
  return eReadOK;
}

void IODBRuntimeAliasBOM::writeBOMSignature(ZostreamDB &) const
{
  INFO_ASSERT (false, "cannot write IODBRuntimeAliasBOM");
}

//! Read the signature for this BOM
/*! \note The corresponding write method is NUAliasBOM::writeBOMSignature
 */
STFieldBOM::ReadStatus IODBRuntimeAliasBOM::readBOMSignature(ZistreamDB &in, UtString *errmsg)
{
  UtString signature;
  UInt32 version;
  
  in >> signature;
  in >> version;
  if (signature.compare (sSignature) != 0) {
    *errmsg << "Signature mismatch - expected '" << sSignature << "' got '" << signature << "'";
    return eReadIncompatible;
  } else if (version != sVersion) {
    *errmsg << "Signature version mismatch - expected " << sVersion << " got " << version;
    return eReadIncompatible;
  } else {
    return eReadOK;
  }
}

void IODBRuntimeAliasBOM::preFieldWrite(ZostreamDB&)
{
  INFO_ASSERT (false, "cannot write IODBRuntimeAliasBOM");
}

STFieldBOM::ReadStatus 
IODBRuntimeAliasBOM::preFieldRead(ZistreamDB&)
{
  return eReadOK;
}


//! Return BOM class name
const char* IODBRuntimeAliasBOM::getClassName() const
{
  return "IODBRuntimeAliasBOM";
}

//! Write the BOMData for a branch node
void IODBRuntimeAliasBOM::xmlWriteBranchData(const STBranchNode* /*branch*/,UtXmlWriter* /*writer*/) const
{
}

//! Write the BOMData for a leaf node
void IODBRuntimeAliasBOM::xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/,UtXmlWriter* /*writer*/) const
{
}
