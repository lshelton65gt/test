// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "iodb/IODBTypes.h"
#include "util/Zstream.h"
#include "util/UtIOStream.h"
#include "util/ConstantRange.h"

static const char* cTypeDBSig = "IODBType";
static const UInt32 cTypeDBVersion = 0;

//-----------------------------------------------------------------------------
//------------------------IODBIntrinsic methods--------------------------------
//-----------------------------------------------------------------------------
SInt32
IODBIntrinsic::getMsb() const 
{
  SInt32 ret = 0;
  if ( mVecRange ) {
    ret = mVecRange->getMsb();
  }
  return ret;
}


SInt32
IODBIntrinsic::getLsb() const 
{
  SInt32 ret = 0;
  if ( mVecRange ) {
    ret = mVecRange->getLsb();
  }
  return ret;
}


//! Hash function for the dictionary
size_t
IODBIntrinsic::hash() const
{
  size_t vecHash = (size_t) mVecRange;
  size_t addrHash = (size_t) mMemAddrRange;
  addrHash <<= 16;
  return (mType ^ vecHash ^ addrHash);
}


//! comparison function for hash table
bool
IODBIntrinsic::operator==( const IODBIntrinsic& other ) const {
  bool retval = false;
  if ( mType == other.mType ) {
    retval = ( mVecRange == other.mVecRange ) &&
      ( mMemAddrRange == other.mMemAddrRange );
  }
  return retval;
}


bool
IODBIntrinsic::dbWrite(ZostreamDB& f) const
{
  if (! f.writePointer(mVecRange))
    return false;

  if (! f.writePointer(mMemAddrRange))
    return false;
  
  return f << static_cast<SInt32>(mType);
}

bool
IODBIntrinsic::dbRead(ZistreamDB& f)
{
  ConstantRange* tmp;
  if (! f.readPointer(&tmp))
    return false;
  
  mVecRange = tmp;

  if (! f.readPointer(&tmp))
    return false;
  
  mMemAddrRange = tmp;
  
  SInt32 typeVar;
  if (f >> typeVar)
  {
    mType = static_cast<Type>(typeVar);
    return true;
  }
  return false;
}


void
IODBIntrinsic::print() const
{
  UtOStream& ut_cout = UtIO::cout();
  const ConstantRange* range = NULL;
  switch (mType)
  {
  case eScalar:
    ut_cout << "Scalar" << UtIO::endl;
    break;
  case eVector:
    range = mVecRange;
    ut_cout << "Vector[" << UtIO::dec << range->getMsb() << ":"
            << range->getLsb() << "]" << UtIO::endl;
    break;
  case eMemory:
    range = mVecRange;
    ut_cout << "Memory[" << UtIO::dec << range->getMsb() << ":"
            << range->getLsb() << ",";
    range = mMemAddrRange;
    ut_cout << range->getMsb() << ":" << range->getLsb() << "]" << UtIO::endl;
    break;
  }
  ut_cout.flush();
} // void IODBIntrinsic::print


size_t
IODBIntrinsic::getWidth() const 
{
  size_t ret = 1;
  if (mVecRange)
    ret = mVecRange->getLength();
  return ret;
}

SInt32
IODBIntrinsic::getHighAddr() const 
{
  // highaddr is the msb of the addr range
  SInt32 ret = 0;
  if (mMemAddrRange)
    ret = mMemAddrRange->getMsb();
  return ret;
}


SInt32
IODBIntrinsic::getLowAddr() const 
{
  // lowaddr is the lsb of the addr range
  SInt32 ret = 0;
  if (mMemAddrRange)
    ret = mMemAddrRange->getLsb();
  return ret;
}


//-----------------------------------------------------------------------------
//-----------------------IODBGenTypeEntry methods------------------------------
//-----------------------------------------------------------------------------
IODBGenTypeEntry::IODBGenTypeEntry()
{
  mIntrinsic = NULL;
  mAttrib = 0;
}


IODBGenTypeEntry::IODBGenTypeEntry(const IODBGenTypeEntry& entry)
{
  mIntrinsic = entry.mIntrinsic;
  mAttrib = entry.mAttrib;
}


IODBGenTypeEntry::IODBGenTypeEntry( const IODBIntrinsic *intrinsic,
                                    AttribFlags attribs )
{
  mIntrinsic = intrinsic;
  mAttrib = UInt32(attribs);
}


IODBGenTypeEntry::~IODBGenTypeEntry()
{}

/* Returns size of primitive in bytes needed to represent the
   fine-grained size of a value. If numWords > 8, 9 is returned
   regardless.
*/
static UInt32 sGetPrimSize(UInt32 numWords)
{
  UInt32 primSize = numWords;
  if (primSize > 2)
  {
    if (primSize <= 4)
      primSize = 4;
    else if (primSize <= 8)
      primSize = 8;
    else 
      primSize = 9;
  }

  return primSize;
}


UInt32 IODBGenTypeEntry::numBytesNeeded() const {
  UInt32 nb = 1;
  if (mIntrinsic->getType() != IODBIntrinsic::eScalar) {
    nb = mIntrinsic->getVecRange()->numBytesNeeded();
    nb = sGetPrimSize(nb);
  }
  return nb;
}

size_t
IODBGenTypeEntry::hash() const {
  // Be careful that scalars & 1-bit vectors hash the same
  size_t h = (size_t) (numBytesNeeded() +
                       IODBGenTypeEntry::sMaskForCompare(mAttrib));
    
  if (mIntrinsic->getType() == IODBIntrinsic::eMemory) {
    // the ConstantRange* pointers are factory-ized so we
    // can hash their pointers
    h += (size_t) mIntrinsic->getMemAddrRange();
  }
  return h;
}

ptrdiff_t
IODBGenTypeEntry::compare(const IODBGenTypeEntry* b) const
{
  const IODBIntrinsic* otherIntrinsic = b->mIntrinsic;

  ptrdiff_t compVal = 0;

  // special case 1 bit vectors and scalars to be the same.
  if (((mIntrinsic->getType() == IODBIntrinsic::eScalar) ||
       (mIntrinsic->getType() == IODBIntrinsic::eVector))
      &&
      ((otherIntrinsic->getType() == IODBIntrinsic::eScalar) ||
      (otherIntrinsic->getType() == IODBIntrinsic::eVector))
      &&
      (mIntrinsic->getWidth() == 1) && (otherIntrinsic->getWidth() == 1))
  {
    compVal = 0;
  }
  else {
    compVal = mIntrinsic->getType() - otherIntrinsic->getType();
  }

  if (compVal == 0)
  {
    UInt32 myFlags = IODBGenTypeEntry::sMaskForCompare( mAttrib );
    UInt32 teFlags = IODBGenTypeEntry::sMaskForCompare( b->mAttrib );
    compVal = myFlags - teFlags;
  }

  if (compVal == 0)
  {
    // Same type and same attributes, look at size
    if (mIntrinsic->getType() == IODBIntrinsic::eMemory)
    {
      const ConstantRange* myMemAddr = mIntrinsic->getMemAddrRange();
      const ConstantRange* otherMemAddr = otherIntrinsic->getMemAddrRange();
      compVal = myMemAddr->compare(*otherMemAddr);

      if (compVal == 0)
      {
        const ConstantRange* myVecRange = mIntrinsic->getVecRange();
        const ConstantRange* otherVecRange = otherIntrinsic->getVecRange();
        compVal = myVecRange->compare(*otherVecRange);
      }
    }
    else {
      compVal = numBytesNeeded() - b->numBytesNeeded();
    }
  }
  
  return compVal;
} // IODBGenTypeEntry::compare

const IODBIntrinsic*
IODBGenTypeEntry::getIntrinsic() const
{
  return mIntrinsic;
}

bool
IODBGenTypeEntry::dbWrite(ZostreamDB& f) const
{
  f << mAttrib;
  return f.writePointer( mIntrinsic );
}

bool
IODBGenTypeEntry::dbRead(ZistreamDB& f)
{
  f >> mAttrib;
  return f.readPointer( &mIntrinsic );
}


bool
IODBGenTypeEntry::isVector() const {
  return mIntrinsic->getType() == IODBIntrinsic::eVector;
}


bool
IODBGenTypeEntry::isTristate() const {
  return (mAttrib & eTristate) != 0;
}


bool
IODBGenTypeEntry::isPrimaryInput() const {
  return (mAttrib & ePrimaryInput) != 0;
}


bool
IODBGenTypeEntry::isBidirect() const {
  return (mAttrib & eBidirect) != 0;
}


bool
IODBGenTypeEntry::isSigned() const {
  return (mAttrib & eSigned) != 0;
}


bool
IODBGenTypeEntry::isForcible() const {
  return (mAttrib & eForcible) != 0;
}

bool
IODBGenTypeEntry::isReal() const {
  return ( mAttrib & eReal ) != 0;
}



IODBGenTypeEntry::AttribFlags
IODBGenTypeEntry::calcAttribFlagsExclude(AttribFlags srcFlags, 
                                         AttribFlags excludeFlags)
{
  return AttribFlags(srcFlags & ~excludeFlags);
}


IODBGenTypeEntry::AttribFlags 
IODBGenTypeEntry::mergeFlags(AttribFlags flagsOne, AttribFlags flagsTwo)
{
  return AttribFlags(flagsOne | flagsTwo);
}


IODBGenTypeEntry::AttribFlags
IODBGenTypeEntry::getAttribFlagsExclude(AttribFlags excludeFlags) const
{
  return calcAttribFlagsExclude(AttribFlags(mAttrib), excludeFlags);
}


UInt32
IODBGenTypeEntry::sMaskForCompare(UInt32 flags)
{
  return UInt32(calcAttribFlagsExclude(AttribFlags(flags), eClock));
}


void
IODBGenTypeEntry::print() const
{
  UtOStream& ut_cout = UtIO::cout();
  ut_cout << "Attribute Flag (0x" << UtIO::hex << mAttrib << UtIO::dec << "):";
  const char* sep = " ";
  for (int i = eMinAttribute; i != eMaxAttribute; i = i << 1) {
    if ((mAttrib & i) != 0) {
      ut_cout << sep;
      sep = ", ";
      switch(i) {
        case ePrimaryInput:
          ut_cout << "primary input";
          break;

        case eClock:
          // unused
#if 0
          ut_cout << "clock";
#endif
          break;

        case eNoFlow:
          ut_cout << "no flow";
          break;

        case eForcible:
          ut_cout << "forcible";
          break;

        case eBidirect:
          ut_cout << "bidirect";
          break;

        case eSigned:
          ut_cout << "signed";
          break;

        case eTristate:
          ut_cout << "tristate";
          break;

        case eReal:
          ut_cout << "real";
          break;
      } // switch
    } // if
  } // for
  ut_cout << UtIO::endl;
}


//-----------------------------------------------------------------------------
//-----------------------IODBTypeDictionary methods----------------------------
//-----------------------------------------------------------------------------
IODBTypeDictionary::IODBTypeDictionary()
{
  mParams = new ConstantRangeFactory;
}


IODBTypeDictionary::~IODBTypeDictionary()
{
  for (GenTypeVecLoop q(mGenTypeVec);
       ! q.atEnd();
       ++q)
    delete *q;
 
  for (EntryVecLoop p(mDictVector); ! p.atEnd();
       ++p)
    delete *p;

  delete mParams;
}


// There is an implicit assumption through this code that mDictVector[0]
// is the single scalar intrinsic.  Each of the add*Intrinsic methods
// all check to verify this.
const IODBIntrinsic*
IODBTypeDictionary::addScalarIntrinsic()
{
  if ( mDictVector.empty( )) {
    IODBIntrinsic *newIntrinsic = new IODBIntrinsic;
    mDictMap[newIntrinsic] = 0;
    mDictVector.push_back( newIntrinsic );
  }

  return mDictVector[0];
}

IODBIntrinsic* IODBTypeDictionary::getCreateIntrinsic(const IODBIntrinsic& key)
{
  IODBIntrinsic* actualIntrinsic = NULL;

  // See if we've already got one
  SInt32 index = findIntrinsic( key );
  if ( index < 0 ) {
    index = mDictVector.size();
    INFO_ASSERT(index != 0, "Scalar intrinsic not present in type vector.");

    switch(key.getType())
    {
    case IODBIntrinsic::eVector:
      actualIntrinsic = new IODBIntrinsic(key.getVecRange());
      break;
    case IODBIntrinsic::eMemory:
      actualIntrinsic = new IODBIntrinsic(key.getVecRange(), key.getMemAddrRange());
      break;
    case IODBIntrinsic::eScalar:
      INFO_ASSERT(key.getType() != IODBIntrinsic::eScalar, "Cannot map scalar type.");
      break;
    }

    FUNC_ASSERT(actualIntrinsic, key.print());
    
    mDictMap[actualIntrinsic] = index;
    mDictVector.push_back( actualIntrinsic );
  }
  else {
    // They say they've already got one!  May we see it?
    actualIntrinsic = getIntrinsicNonConst(index);
  }
  return actualIntrinsic;
  
}

const IODBIntrinsic*
IODBTypeDictionary::addVectorIntrinsic( int msb, int lsb )
{
  // Make sure the scalar intrinsic is in slot 0
  if ( mDictVector.empty() ) {
    addScalarIntrinsic();
  }

  const ConstantRange *vecRange = getConstantRange( msb, lsb );

  // vectorIntrinsic is the intrinsic we're trying to find/add
  IODBIntrinsic vectorIntrinsic( vecRange );
  return getCreateIntrinsic(vectorIntrinsic);
}


const IODBIntrinsic*
IODBTypeDictionary::addMemoryIntrinsic( int hi, int lo, int msb, int lsb )
{
  // Make sure the scalar intrinsic is in slot 0
  if ( mDictVector.empty() ) {
    addScalarIntrinsic();
  }

  const ConstantRange *vecRange = getConstantRange( msb, lsb );
  const ConstantRange *memRange = getConstantRange( hi, lo );

  // memoryIntrinsic is the intrinsic we're trying to find/add
  IODBIntrinsic memoryIntrinsic( vecRange, memRange );
  return getCreateIntrinsic(memoryIntrinsic);
}


SInt32
IODBTypeDictionary::addTypeEntry( const IODBIntrinsic *intrinsic,
                                  IODBGenTypeEntry::AttribFlags flags )
{
  // Find the index of the intrinsic
  SInt32 index = mDictMap[intrinsic];
  // The intrinsic should already be in the dictionary
  FUNC_ASSERT( index != -1, intrinsic->print() );

  IODBGenTypeEntry gte( getIntrinsic(index), flags );
  
  // Next, see if this genTypeEntry already exists
  SInt32 genIndex = findGenEntry( gte );
  if ( genIndex < 0 )
  {
    // It doesn't exist--create a new one and store it in both map and vector
    genIndex = mGenTypeVec.size();
    IODBGenTypeEntry* genEntry = new IODBGenTypeEntry( gte );
    mGenTypeMap[genEntry] = genIndex;
    mGenTypeVec.push_back(genEntry);
  }

  return genIndex;
}

const ConstantRange*
IODBTypeDictionary::getConstantRange(SInt32 msb, SInt32 lsb)
{
  return mParams->find(msb, lsb);
}


SInt32
IODBTypeDictionary::findIntrinsic(const IODBIntrinsic& i) const
{
  SInt32 index = -1;
  CDictIter p = mDictMap.find(&i);
  if (p != mDictMap.end())
    index = p->second;
  return index;
}


SInt32
IODBTypeDictionary::findGenEntry(const IODBGenTypeEntry& genTE ) const
{
  SInt32 index = -1;
  CGenTypeIter p = mGenTypeMap.find( const_cast<IODBGenTypeEntry*>(&genTE) );
  if (p != mGenTypeMap.end())
    index = p->second;
  return index;
}


const IODBIntrinsic* 
IODBTypeDictionary::getIntrinsic(UInt32 typeIndex) const
{
  INFO_ASSERT(typeIndex < mDictVector.size(), "Invalid intrinsic index."); // test/bugs/bug295/unconnected_z.v - top.$extnet
  return mDictVector[typeIndex];
}

IODBIntrinsic* 
IODBTypeDictionary::getIntrinsicNonConst(UInt32 typeIndex)
{
  INFO_ASSERT(typeIndex < mDictVector.size(), "Invalid intrinsic index."); // test/bugs/bug295/unconnected_z.v - top.$extnet
  return mDictVector[typeIndex];
}



const IODBGenTypeEntry* 
IODBTypeDictionary::getGenTypeEntry(UInt32 typeIndex) const
{
  INFO_ASSERT(typeIndex < mGenTypeVec.size(), "Invalid GenEntry index."); // test/bugs/bug295/unconnected_z.v - top.$extnet
  return mGenTypeVec[typeIndex];
}

IODBGenTypeEntry* 
IODBTypeDictionary::getGenTypeEntryNonConst(UInt32 typeIndex)
{
  INFO_ASSERT(typeIndex < mGenTypeVec.size(), "Invalid GenEntry index."); // test/bugs/bug295/unconnected_z.v - top.$extnet
  return mGenTypeVec[typeIndex];
}


bool
IODBTypeDictionary::save(ZostreamDB& f) const
{
  f << cTypeDBSig;
  f << cTypeDBVersion;
  
  if (! mParams->dbWrite(f))
    return false;

  if (! f.writePointerValueContainer(mDictVector))
    return false;
  
  if (! f.writePointerValueContainer(mGenTypeVec))
    return false;

  return true;
}

bool
IODBTypeDictionary::restore(ZistreamDB& f)
{
  UtString signature;
  if (! (f >> signature))
    return false;

  if (signature.compare(cTypeDBSig) != 0)
  {
    UtString buf;
    buf << "Invalid TypeDB signature: " << signature;
    f.setError(buf.c_str());
    return false;
  }
  
  UInt32 version;
  f >> version;
  if (f.fail())
    return false;

  if (version > cTypeDBVersion)
  {
    UtString buf;
    buf << "Unsupported TypeDB version: " << version;
    f.setError(buf.c_str());
    return false;
  }
  
  if (! mParams->dbRead(f))
    return false;

  // All pointer reads in dictvector are based on tmpParams
  IODBIntrinsic dummyintrinsic;
  if (! f.readPointerValueContainerList(&mDictVector, dummyintrinsic))
    return false;
  
  SInt32 sz = mDictVector.size();
  SInt32 i;
  
  for (i = 0; i < sz; ++i)
  {
    IODBIntrinsic* intr = getIntrinsicNonConst(i);
    mDictMap[intr] = i;
  }
  
  IODBGenTypeEntry dummyGenTE;
  if (! f.readPointerValueContainerList(&mGenTypeVec, dummyGenTE))
    return false;

  sz = mGenTypeVec.size();
  for (i = 0; i < sz; ++i)
  {
    IODBGenTypeEntry* gen = getGenTypeEntryNonConst(i);
    mGenTypeMap[gen] = i;
  }
  return true;
}

SInt32
IODBTypeDictionary::findGenTypeIndexScalar(IODBGenTypeEntry::AttribFlags flags) const
{
  return findGenEntry(IODBGenTypeEntry(getIntrinsic(0), flags));
}


SInt32
IODBTypeDictionary::findGenTypeIndexVector(const IODBIntrinsic* entry, 
                                           IODBGenTypeEntry::AttribFlags flags) const
{
  return findGenEntry(IODBGenTypeEntry(entry, flags));
}


// input = intrinsic index, output = GTE index
SInt32
IODBTypeDictionary::findGenTypeIndex(SInt32 typeIndex) const
{
  const IODBIntrinsic* intrinsic = getIntrinsic( typeIndex );
  return findGenEntry( IODBGenTypeEntry( intrinsic ));
}


SInt32
IODBTypeDictionary::numGenTypes() const
{
  return mGenTypeVec.size();
}
