//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// This file is a repository for experiments with PV during the research phase.


void PVPhase::copyModuleContents() {
  // Create a name->module map for the master list
  UtHashMap<StringAtom*,NUModule*> master_mod_map;
  for (NUDesign::ModuleLoop p(getMasterDesign()->loopAllModules()); !p.atEnd(); ++p)
  {
    NUModule* mod = *p;
    master_mod_map[mod->getName()] = mod;
  }

  NUModuleList mods;
  mXactorImage->getModulesBottomUp(&mods);

  // Copy the contents of all modules.  Note that the instances don't need
  // to be copied back to the master since we never cleared them, and
  // copy propagation doesn't change them.
  NUNetSet master_uses, master_defs;

  for (NUModuleList::iterator p(mods.begin()), e(mods.end()); p != e; ++p) {
    NUModule* transaction_mod = *p;
    NUModule* master_mod = master_mod_map[transaction_mod->getName()];
    //copyModuleContents(master_mod, transaction_mod);
    findUseDefNets(transaction_mod,
                   master_mod,
                   &master_uses,
                   &master_defs);
  }
}

void PVPhase::copyModuleContents(NUModule* master_mod,
                                 NUModule* transaction_mod)
{
  // We are going to blindly copy all the blocks from the transaction's
  // copy of the design over to the master.  This will leave the
  // wrong NUNets in place.  Then we will walk through all the
  // blocks we've copied and fix up the nets.  We will do that
  // with an NuToNU override rather than a CopyContext because
  // any new nets created by Constant Propagation will not be
  // translated.  We want to create a new net in the master design
  // when that happens.  CopyContext will just leave the net alone,
  // which is not the behavior I want.
  AtomicCache* ac = master_mod->getDesign()->getSymbolTable()->getAtomicCache();
  CopyContext cc(master_mod, ac);
  UtString prefix;
  prefix << "CPV_" << mIndex << "_";
  PVTransactionToMaster t2m(master_mod, prefix.c_str());

  IODBNucleus* master_iodb = getMasterDesign()->getIODB();

  // Copy over initial blocks
  for (NUModule::InitialBlockLoop p(transaction_mod->loopInitialBlocks());
       !p.atEnd(); ++p)
  {
    NUInitialBlock* ib = *p;
    NUInitialBlock* new_ib = ib->clone(cc);
    master_mod->addInitialBlock(new_ib);
    new_ib->replaceLeaves(t2m);
    mMasterBlocks.push_back(new_ib);
    new_ib->getBlock()->putIODB(master_iodb);
  }

  // We need to transform all the always-blocks using the NUModule method
  // that copies over the priority chains properly.  But we also want to
  // keep track of all the blocks we added to the master module for this
  // transaction.  The easiest way to do that, for the moment, is to
  // clear the existing blocks from the module and then re-insert them.
  NUAlwaysBlockList save_blocks, new_blocks;
  master_mod->getAlwaysBlocks(&save_blocks);
  master_mod->replaceAlwaysBlocks(new_blocks);

  transaction_mod->copyAlwaysBlocks(master_mod, cc, &t2m);

  // Now we can walk through the new always-blocks and add them to the
  // master block list:
  for (NUModule::AlwaysBlockLoop p(master_mod->loopAlwaysBlocks());
       !p.atEnd(); ++p)
  {
    NUAlwaysBlock* ab = *p;
    mMasterBlocks.push_back(ab);
    save_blocks.push_back(ab);
    ab->getBlock()->putIODB(master_iodb);
  }

  // Finally, we can re-insert the saved blocks
  master_mod->replaceAlwaysBlocks(save_blocks);

  // Copy over continuous assigns, transforming to always-blocks
  // on negedge clock for now
  for (NUModule::ContAssignLoop p(transaction_mod->loopContAssigns());
       !p.atEnd(); ++p)
  {
    NUContAssign* ca = *p;
    NUContAssign* new_ca = dynamic_cast<NUContAssign*>(ca->copy(cc));
    NU_ASSERT(new_ca, ca);
    new_ca->replaceLeaves(t2m);

    // if the new continuous assign drives only 'private' nets, then
    // we can leave it as is.  But if it drives nets that may also
    // be driven by other transactions, then we must transform this
    // into a sequential block, using the negedge of the new guard.

    master_mod->addContAssign(new_ca);
    mMasterBlocks.push_back(new_ca);
  }

  NucleusSanity nucleusSanity;
  nucleusSanity.design(getMasterDesign(), NucleusSanity::eAfterResynth);
} // void PVPhase::copyModuleContents

// Create a block in the master module which has all the statements
// from running this transaction.  This is very rough, simple schedule,
// purely transition-based.  Obviously we need to run the real scheduler.
// We have to pessimistically assume that every state element is both
// depositable and observable, because we don't know (yet) what persistent
// state is required or provided by other transaction IDs.
//  E.g.  suppose the current transaction ID is tid_2.
//       always @(posedge tclk) if (tid_1) s1 = f(PI);
//       always @(posedge tclk) if (tid_1) s2 = f(s1);
//       always @(posedge tclk) if (tid_2) s3 = f(s2);
//       always @(posedge tclk) if (tid_2) s4 = f(s3);
//       always @(posedge tclk) if (tid_2) s5 = f(s4);
//       always @(posedge tclk) if (tid_3) s6 = f(s5);
//
// In this scenario, s2 is a primary input for tid_2, although
// if you constant-propagate based on tid_1=0, tid_2=1, then
// s2 looks undriven.
//
// But we can figure out which registers are actively driven, and
// which are actually used by running UD.
void PVPhase::findUseDefNets(NUModule* transaction_mod,
                                     NUModule* master_mod,
                                     NUNetSet* master_uses,
                                     NUNetSet* master_defs)
{
  NUNetSet used_nets, def_nets;

  {
    ClearUDCallback clearUseDef(false); // do not recurse
    NUDesignWalker clearUDWalker(clearUseDef, false);
    clearUDWalker.module(transaction_mod);
  }
  {
    NUNetRefFactory* nrf = mXactorImage->getNetRefFactory();
    MsgContext* msg_context = mXactorImage->getMsgContext();
    IODBNucleus* iodb = mXactorImage->getIODB();
    ArgProc* args = mXactorImage->getArgProc();

    UD ud(nrf, msg_context, iodb, args, false);
    ud.module(transaction_mod, true);
  }

  // Copy over initial blocks
  for (NUModule::InitialBlockLoop p(transaction_mod->loopInitialBlocks());
       !p.atEnd(); ++p)
  {
    NUInitialBlock* ib = *p;
    ib->getUses(&used_nets);
    ib->getDefs(&def_nets);
  }
  // Now we can walk through the new always-blocks and add them to the
  // master block list:
  for (NUModule::AlwaysBlockLoop p(transaction_mod->loopAlwaysBlocks());
       !p.atEnd(); ++p)
  {
    NUAlwaysBlock* ab = *p;
    ab->getUses(&used_nets);
    ab->getDefs(&def_nets);
  }

  // Copy over continuous assigns, transforming to always-blocks
  // on negedge clock for now
  for (NUModule::ContAssignLoop p(transaction_mod->loopContAssigns());
       !p.atEnd(); ++p)
  {
    NUContAssign* ca = *p;
    ca->getUses(&used_nets);
    ca->getUses(&def_nets);
  }

  // Translate all the used nets that are relevant to the rest of the
  // design.  Only state-nets or nets declared as mutable are shared
  // between transaction IDs.
  PVTransactionToMaster t2m(master_mod, "");

  for (NUNetSetLoop p(used_nets); !p.atEnd(); ++p) {
    NUNet* master_net = t2m.findMasterNet(*p);
    if (master_net != NULL) {
      master_uses->insert(master_net);
    }
  }
  for (NUNetSetLoop p(def_nets); !p.atEnd(); ++p) {
    NUNet* master_net = t2m.findMasterNet(*p);
    if (master_net != NULL) {
      master_defs->insert(master_net);
    }
  }
} // void PVPhase::findUseDefNets

/* 
 * void PVPhase::replaceClocksWithGuards(NUModule* mod, STBranchNode* parent, StringAtom* name)
 * {
 *   STSymbolTable* symtab = getMasterDesign()->getSymbolTable();
 *   STBranchNode* path = symtab->find(parent, name);
 * 
 *   // Every element of the design that's optimized for this TID has to
 *   // be transferred to the master.  The only question is how we "gate"
 *   // them for combining with other TID design images.  Sequential blocks
 *   // on the transaction clock get a unique guard per TID.  For the moment,
 *   // we will copy other blocks verbatim.  We will see whether we need to
 *   // gate them based on the guards.
 *   //
 *   // All async-reset priority chains should disappear in this model because
 *   // we consider reset to 
 * 
 * 
 * 
 * 
 * 
 * 
 * be a separate transaction.  This may need to be
 *   // revisited.
 * 
 *   for (NUModule::AlwaysBlockLoop p(loopAlwaysBlocks()); !p.atEnd(); ++p) {
 *     NUAlwaysBlock* ab = *p;
 *     if (ab->isSequential()) {
 *       NU_ASSERT(ab->getPriorityBlock() == NULL, ab);
 *       NU_ASSERT(ab->getClockBlock() == NULL, ab);
 *       NUEdgeExpr* ee = ab->getEdgeExpr();
 *       NUNet* clk_net = ee->getNet();
 *       NUNetElab* clk_elab = clk_net->lookupElab(path);
 *       STSymbolTableNode* main_master_node = pvc->getMasterNode(clk_elab->getSymNode());
 *       STSymbolTableNode* tid_master_node = symtab->safeLookup(main_master_node);
 *       if (tid_master_node == mClockNode) {
 * 
 *         // This instance's clock-net matches the clock for this transaction.
 *         // Create a clone of this always-block in the master design, but using
 *         // a guard net as the clock
 *         pvc->transformAlwaysBlock(path, ab, main_master_node);
 *       }
 *     }
 *     else {
 *     }
 *   }
 * 
 *   // recurse into submodules
 *   for (NUModule::InstanceLoop p(loopInstances()); !p.atEnd(); ++p) {
 *     NUModuleInstance* inst = *p;
 *     replaceClocks(pvc, inst->getModule(), inst->getName(), path);
 *   }
 * } // void PVPhase::replaceClocksWithGuards
 * 
 * void PVContext::transformAlwaysBlock(STSymbolTableNode* path, NUAlwaysBlock* ab,
 *                                      STSymbolTableNode* clock)
 * {
 *   // Translate an always-block from a transaction-id's image of the design into
 *   // the master design.  There are two aspects of this:
 *   //   - cloning the always-block contents, translating the nets in the
 *   //     always-block locally.  We can retain the replacement map 
 *   /
 *   //   - 
 * }
 */

void PVContext::createClockMap() {
  ArgProc* args = getMasterDesign()->getArgProc();
  MsgContext* msg_context = getMasterDesign()->getMsgContext();
  SourceLocatorFactory* slc = getMasterDesign()->getSourceLocatorFactory();
  STSymbolTable* symtab = getMasterDesign()->getSymbolTable();
  AtomicCache* acache = symtab->getAtomicCache();
  IODBNucleus* iodb = getMasterDesign()->getIODB();
  NUNetRefFactory* nrf = getMasterDesign()->getNetRefFactory();
  
  GOGlobOpts globOpts(acache, iodb, stats,
                      args, msg_context, nrf, slc, 
                      "", // mCongruencyInfoFilename.c_str(),
                      NULL, // &wiredNetResynth,
                      tmode);
  globOpts.createFramework();
  REAlias* alias = globOpts.createAliases();
  alias->findAliases(stats, true);

  for (ClockMap::SortedLoop p(mClockMap.loopSorted()); !p.atEnd(); ++p) {
    const UtString& clk_name = p.getKey();
    NUNetElab* clk_elab = getMasterDesign()->findNetElab(clk_name.c_str());
    if (clk_elab == NULL) {
      // print error messages, etc
    }
    else {
      NUNetElab* master = alias->getMaster(clk_elab);
      STSymbolTableNode* master_node = master->getSymNode();
      NUNetElabVector v;
      alias->getAliases(clk_elab, &v);
      for (UInt32 i = 0; i < v.size(); ++i) {
        NUNetElab* net_elab = v[i];
        STSymbolTableNode* alias_node = net_elab->getSymNode();
        mClkMap[alias_node] = master_node;
      }
    }
  }
  delete alias;
  globOpts.destroyFramework();
} // void PVContext::createClockMap

class PVXlateNet : public NuToNuFn {
public:
  PVXlateNet(NUNet* from, NUNet* to) :
    mFrom(from), mTo(to)
  {
  }

  virtual NUNet * operator()(NUNet * net, Phase phase) {
    if (isPre(phase) && (net == mFrom)) {
      return mTo;
    }
    return NULL;
  }
private:
  NUNet* mFrom;
  NUNet* mTo;
};

void PVPhase::putGuardNet(NUNet* guard_net) {
  NUNet* transaction_clk_net = getMasterDesign()->findNet(getClkName());
  PVXlateNet xlate(transaction_clk_net, guard_net);

  for (UInt32 i = 0; i < mMasterBlocks.size(); ++i) {
    NUUseDefNode* ud = mMasterBlocks[i];
    ud->replaceLeaves(xlate);
  }
}
