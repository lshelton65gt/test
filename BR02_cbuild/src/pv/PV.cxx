//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "pv/PV.h"
#include "util/DynBitVector.h"
#include "util/UtIStream.h"

PVContext::PVContext() {
}

PVContext::~PVContext() {
  mReadTransactions.clearPointers();
  mWriteTransactions.clearPointers();
  mNetMap.clearPointerValues();
}
PVTransaction* PVContext::addTransaction(const char* tname, const char* clk,
                                         PVTransactionType type,
                                         UInt32 addr_start, UInt32 addr_end)
{
  PVTransaction* transaction = new PVTransaction(this, tname, clk,
                                                 type, addr_start, addr_end);
  if (type == ePVWrite) {
    mWriteTransactions.push_back(transaction);
  }
  else {
    mReadTransactions.push_back(transaction);
  }

  // we will not immediately validate whether this transaction has overlapping
  // addresses with others.  We will wait till we have all of them and them
  // sort them appropriately, building some sort of lookup structure so we
  // can execute the transaction lazily
  return transaction;
}

PVTransaction::PVTransaction(PVContext* pvc, const char* tname,
                             const char* clk_name, PVTransactionType type,
                             UInt32 addr_start, UInt32 addr_end)
  : mPVContext(pvc),
    mTransactionName(tname),
    mTransactionClockName(clk_name),
    mTransactionClockNet(NULL),
    mAddressStart(addr_start),
    mAddressEnd(addr_end),
    mType(type),
    mPartner(NULL)
{
}

PVTransaction::~PVTransaction() {
  for (UInt32 i = 0; i < mSequences.size(); ++i) {
    PVSequence* seq = mSequences[i];
    delete seq;
  }

  // When two transactions are made read/write partners, they will
  // share a data buffer.  
#if 0
  if (mPVBuffer != NULL) {
    if (mPartner != NULL) {
      INFO_ASSERT(mPartner->mPVBuffer == mPVBuffer, "shared buffer insanity");
      mPartner->mPVBuffer = NULL;
    }
    CARBON_FREE_VEC(mPVBuffer, char, getSize());
  }
#endif

  if (mPartner != NULL) {
    INFO_ASSERT(mPartner->mPartner == this, "partner insanity");
    mPartner->mPartner = NULL;
  }

  mReadPorts.clearPointers();
  mWritePorts.clearPointers();
}

void PVTransaction::putPartner(PVTransaction* partner) {
  INFO_ASSERT(mPartner == NULL, "only one partner allowed");
  INFO_ASSERT(partner->mPartner == NULL, "only one partner allowed");
  mPartner = partner;
  partner->mPartner = this;

#if 0
  INFO_ASSERT(mPVBuffer != NULL, "expected to have a PV Buffer");
  INFO_ASSERT(partner->mPVBuffer != NULL, "expected partner to have a PV Buffer");
  INFO_ASSERT(partner->mPVBuffer != mPVBuffer, "two PV Buffers to be different");
#endif

  INFO_ASSERT(partner->mAddressStart == mAddressStart, "addr start insanity");
  INFO_ASSERT(partner->mAddressEnd == mAddressEnd, "addr end insanity");
  INFO_ASSERT(partner->mType != mType, "partner type insanity");

#if 0
  CARBON_FREE_VEC(mPVBuffer, char, getSize());

  // finally, share the PV Buffer.
  mPVBuffer = partner->mPVBuffer;
#endif
}



PVSequence* PVTransaction::addSequence(const char* name) {
  PVSequence* sequence = new PVSequence(this, mSequences.size(), name);
  mSequences.push_back(sequence);
  return sequence;
}

PVSequence::PVSequence(PVTransaction* transaction, UInt32 index, const char* name) :
  mTransaction(transaction),
  mIndex(index),
  mSequenceClockNet(NULL)
{
  mName << transaction->getTransactionName() << "_";
  if (name == NULL) {
    mName << index;
  }
  else {
    mName << name;
  }
  mSequenceClockName << "pv_" << mName;
}
  
PVSequence::~PVSequence() {
  for (UInt32 i = 0; i < mPhases.size(); ++i) {
    PVPhase* phase = mPhases[i];
    delete phase;
  }
}


PVPhase* PVSequence::addPhase(const char* name) {
  PVPhase* phase = new PVPhase(this, mPhases.size(), name);
  mPhases.push_back(phase);
  return phase;
}


PVPhase::PVPhase(PVSequence* sequence, UInt32 idx, const char* name) :
  mSequence(sequence),
  mIndex(idx)
{
  // This is as good a time as any to nail down a unique name
  if (name == NULL) {
    mName << sequence->getName() << "_" << idx;
  }
  else {
    mName = name;
  }
}

PVPhase::~PVPhase() {
  mAddressPorts.clearPointers();
  mReadPorts.clearPointers();
  mWritePorts.clearPointers();
  mTiePorts.clearPointers();
}

PVNet::PVNet(PVContext* pvc, const char* net_name, UInt32 bit_size) :
  mNetName(net_name),
  mBitSize(bit_size),
  mShellNet(NULL),
  mPVContext(pvc),
  mIsReadable(false),
  mIsWritable(false),
  mCallback(NULL)
  
{
  mBuffer = CARBON_ALLOC_VEC(CarbonUInt32, numWords());
}

PVNet::~PVNet() {
  CARBON_FREE_VEC(mBuffer, CarbonUInt32, numWords());
}

PVDataPort::PVDataPort(PVNet* net, UInt32 byte_offset,
                       bool is_readable, bool is_writable)
  : mNet(net),
    mByteOffset(byte_offset)
{
  net->putIsReadable(is_readable);
  net->putIsReadable(is_writable);
}

PVDataPort::~PVDataPort() {
}

PVTiePort::PVTiePort(PVNet* net, const char* val)
  : mNet(net),
    mValueString(val)
{
  // Parse the value.  For now assume just a simple hex string.
  DynBitVector bv(net->numBits());
  UtIStringStream is(val);
  is >> bv;                     // error checking?
  UInt32 num_words = net->numWords();
  INFO_ASSERT(num_words == bv.numWords(), "word-size insanity");

  // Because multiple TiePorts may temporally share the same net,
  // this class needs its own private buffer to retain the value
  // for depositing at the appropriate phase
  mValue = CARBON_ALLOC_VEC(CarbonUInt32, num_words);

  memcpy(mValue, bv.getUIntArray(), num_words * sizeof(CarbonUInt32));
}

PVTiePort::~PVTiePort() {
  CARBON_FREE_VEC(mValue, CarbonUInt32, mNet->numWords());
}


PVNet* PVContext::getPVNet(const char* name) {
  PVNet* pv_net = mNetMap[name];
  if (pv_net == NULL) {
    UInt32 num_bits = findNetSize(name);
    if (num_bits == 0) {
      mNetMap.erase(name);
      return NULL;
    }
    pv_net = new PVNet(this, name, num_bits);
    mNetMap[name] = pv_net;
  }
  return pv_net;
}

void PVContext::runSchedule() {
}

void PVPhase::addTie(PVNet* pv_net, const char* val) {
  mTiePorts.push_back(new PVTiePort(pv_net, val));
}

bool PVPhase::bindInputData(PVNet* pv_net, UInt32 byte_offset) {
  mWritePorts.push_back(new PVDataPort(pv_net, byte_offset, false, true));
  return true;
}

bool PVPhase::bindOutputData(PVNet* pv_net, UInt32 byte_offset) {
  mReadPorts.push_back(new PVDataPort(pv_net, byte_offset, false, true));
  return true;
}

bool PVTransaction::bindInputData(PVNet* pv_net, UInt32 byte_offset) {
  mWritePorts.push_back(new PVDataPort(pv_net, byte_offset, false, true));
  return true;
}

bool PVTransaction::bindOutputData(PVNet* pv_net, UInt32 byte_offset) {
  mReadPorts.push_back(new PVDataPort(pv_net, byte_offset, false, true));
  return true;
}

CarbonObjectID* PVContext::getModel() const {
  return NULL;
}

bool PVContext::tieInputPort(const char* net_name,
                             const char* val,
                             PVPhase* phase)
{
  PVNet* pv_net = getPVNet(net_name);
  if (pv_net == NULL) {
    return false;
  }
  phase->addTie(pv_net, val);
  return true;
}

PVAddressPort::PVAddressPort(PVNet* pv_net, SInt32 shift, SInt32 offset) :
  mShift(shift),
  mOffset(offset),
  mNet(pv_net)
{
}

PVAddressPort::~PVAddressPort() {
}

// determine whether an input or output is multiplexed across the phases.
// If so, we will have to make a unique copy of the signal for each phase
// when optimizing
bool PVSequence::findMultiplexedIOs(bool replace_net) {
  bool ret = true;
  PVNetSet seen_read_ports, seen_write_ports, multiplexed_ios;
  for (PhaseLoop p(loopPhases()); !p.atEnd(); ++p) {
    PVPhase* phase = *p;
    for (PVDataPortLoop r(phase->loopReadPorts()); !r.atEnd(); ++r) {
      PVDataPort* reader = *r;
      PVNet* net = reader->getNet();
      if (! seen_read_ports.insertWithCheck(net)) {
        multiplexed_ios.insert(net);
      }
    }

    for (PVDataPortLoop w(phase->loopWritePorts()); !w.atEnd(); ++w) {
      PVDataPort* writer = *w;
      PVNet* net = writer->getNet();
      if (! seen_write_ports.insertWithCheck(net)) {
        multiplexed_ios.insert(net);
      }
    }
  }


  // In a second pass, loop over all the ports again and multiplex any that
  // are read or written more than once in the sequence.
  //
  // This could aslso be done without a second pass by keeping a map from
  // nets to the ports that reference them.  This is just as easy though.
  for (PhaseLoop p(loopPhases()); !p.atEnd(); ++p) {
    PVPhase* phase = *p;
    for (PVDataPortLoop r(phase->loopReadPorts()); !r.atEnd(); ++r) {
      PVDataPort* reader = *r;
      PVNet* net = reader->getNet();
      if (multiplexed_ios.find(net) != multiplexed_ios.end()) {
        ret &= phase->multiplexPort(reader, replace_net);
      }
    }

    for (PVDataPortLoop w(phase->loopWritePorts()); !w.atEnd(); ++w) {
      PVDataPort* writer = *w;
      PVNet* net = writer->getNet();
      if (multiplexed_ios.find(net) != multiplexed_ios.end()) {
        ret &= phase->multiplexPort(writer, replace_net);
      }
    }
  }
  return ret;
} // bool PVSequence::findMultiplexedIOs

void PVPhase::getPhaseNetName(const char* net_name, UtString* phase_name) {
  *phase_name
    << net_name
    << "_pv_" << mSequence->getName()
    << "_" << mName;
}

bool PVPhase::multiplexPort(PVDataPort* port, bool replace_net) {
  UtString phase_mux_name;
  getPhaseNetName(port->getNet()->getName(), &phase_mux_name);
  port->putMultiplexedName(phase_mux_name.c_str());

  // At runtime, we should find the multiplexed name via name lookup
  // in the design.  At compile-time this will silently fail.  We
  // need to make sure it doesn't silently fail at runtime...
  if (replace_net) {
    PVNet* muxed_net = getPVContext()->getPVNet(phase_mux_name.c_str());
    if (muxed_net == NULL) {
      //UtIO::cerr() << "Could not find phase net name: " << phase_mux_name
      //             << "\n";     // make errmsg
      return false;
    }
    port->putNet(muxed_net);
  }
  return true;
}

void PVDataPort::putNet(PVNet* pv_net) {
  mNet = pv_net;
}

//  
//      // Make a version for this phase
//      NUNet* orig_net = phase_design->findNet(pv_net->getName());
//      INFO_ASSERT(orig_net, "cannot find data net");
//
//      UtString name(orig_net->getName()), full_name;
//      name << "_" << 
//      StringAtom* sym = phase_design->getAtomicCache()->intern(name);
//      NUNet* muxed_net = orig_net->clone(net->getScope(), rep_map, sym,
//                                         net->getFlags());
//
//      // change the data port to reference the new name
//      full_name << "top." << name;
//      PVNet* new_pv_net = getPVNet(full_name.c_str());
//      INFO_ASSERT(new_pv_net, "full lookup on phase multiplexed net");
//      writer->putNet(new_pv_net);
//      rep_map[orig_net] = muxed_net;
//
//} // void PVSequence::findMultiplexedIOs
