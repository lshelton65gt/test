//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "PVXmlParser.h"

#include "libxml/parser.h"
#include "libxml/xmlerror.h"


PVXmlParser::PVXmlParser()
  :mErrorReported(false)
{
}

PVXmlParser::~PVXmlParser()
{
}


///////////////////////////////////////////////////////////////////////////////
void PVXmlParser::sXmlStructuredErrorHandler(void *arg, xmlErrorPtr error)
{
  PVXmlParser *cfgParser = (PVXmlParser *) arg;
  const char *fileName = error->file;
  const char *msg = error->message;

  UtString message;
  message << ((fileName == NULL) ? "<unknown>" : fileName);
  message << ":" << error->line << ": " << msg;

  cfgParser->reportError(message.c_str());
}


///////////////////////////////////////////////////////////////////////////////
void PVXmlParser::reportError(const char *message)
{
  mErrorText << message << "\n";
  mErrorReported = true;
}

///////////////////////////////////////////////////////////////////////////////
void PVXmlParser::parseSetup()
{
  // nothing has gone wrong yet
  mErrorReported = false;
  mErrorText.clear();

  // Check potential ABI mismatches between the version it was
  // compiled for and the actual shared library used.
  LIBXML_TEST_VERSION

  // replace default error handler
  xmlSetStructuredErrorFunc(this, sXmlStructuredErrorHandler);
}

///////////////////////////////////////////////////////////////////////////////
void PVXmlParser::parseCleanup()
{
  xmlCleanupParser();
}

///////////////////////////////////////////////////////////////////////////////
const char * PVXmlParser::errorText()
{
  return mErrorText.c_str();
}

///////////////////////////////////////////////////////////////////////////////
bool PVXmlParser::isElement(xmlNode *node, const char *name)
{
  if (node == NULL || node->type != XML_ELEMENT_NODE) {
    return false;
  }

  return strcmp((const char *) node->name, name) == 0;
}

///////////////////////////////////////////////////////////////////////////////
bool PVXmlParser::hasAttr(xmlNode *node, const char *name)
{
  return xmlHasProp(node, (xmlChar *) name) != NULL;
}

///////////////////////////////////////////////////////////////////////////////
void PVXmlParser::getContent(xmlNode *node, UtString *content)
{
  *content = "";
  xmlChar *text = xmlNodeGetContent(node);
  if (text != 0) {
    *content = reinterpret_cast<const char *>(text);
    xmlFree(text);
  }
}
