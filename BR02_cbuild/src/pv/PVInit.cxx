//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "pv/PVRuntime.h"

struct PVCmpTransactions {
  bool operator()(const PVTransaction* t1, const PVTransaction* t2) {
    return t1->getAddressStart() < t2->getAddressStart();
  }
};

PVRuntimeContext::PVRuntimeContext(CarbonObjectID* model,
                                   const char* base_address,
                                   CarbonUInt32 time_delta)
  : mReader(mReadTransactions),
    mWriter(mWriteTransactions),
    mBaseAddress(base_address),
    mModel(model),
    mTime(0),
    mTimeDelta(time_delta),
    mBufferPtr(0)
{
}


bool PVRuntimeContext::initTransactions(TVec& tvec) {
  PVTransaction* prev = NULL;
  bool ret = true;
  for (UInt32 i = 0; i < tvec.size(); ++i) {
    PVTransaction* transaction = tvec[i];
    ret &= transaction->initialize(mModel);
    if (prev != NULL) {
      INFO_ASSERT(prev->getAddressEnd() < transaction->getAddressStart(),
                  "addresses overlap");

      // we probably *do* want to support some transaction nesting so
      // that we can have unaccelerated registers that cover a wide
      // range of addresses, some of which are accelerated.  The
      // unaccelerated transaction may overlap several accelerated ones.
      // Alternatively there could a read-fallback transaction and a
      // write-fallback transactions.
    }
    prev = transaction;
  }
  return ret;
}


bool PVRuntimeContext::initialize() {
  bool ret = true;

  // Sort transactions by start address
  std::sort(mReadTransactions.begin(), mReadTransactions.end(),
            PVCmpTransactions());
  std::sort(mWriteTransactions.begin(), mWriteTransactions.end(),
            PVCmpTransactions());

  ret &= initTransactions(mReadTransactions);
  ret &= initTransactions(mWriteTransactions);

  for (PVNetMap::UnsortedLoop p(mNetMap); !p.atEnd(); ++p) {
    PVNet* net = p.getValue();
    ret &= net->initialize(mModel);
  }
  return ret;
}

bool PVTransaction::initialize(CarbonObjectID* model) {
  bool ret = true;
  for (SequenceLoop s(loopSequences()); !s.atEnd(); ++s) {
    PVSequence* sequence = *s;
    ret &= sequence->initialize(model);
  }

  if (! mTransactionClockName.empty()) {
    PVContext* pvc = getPVContext();
    mTransactionClockNet = pvc->getPVNet(mTransactionClockName.c_str());
  }

  return ret;
}

bool PVSequence::initialize(CarbonObjectID* /*model*/) {
  (void) findMultiplexedIOs(true);
  // we will not report an error here because if we are
  // running an optimized design then we will not have
  // the phase muxed io nets

/*
  for (PhaseLoop p(loopPhases()); !p.atEnd(); ++p) {
    PVPhase* phase = *p;
    phase->useMultiplexedIOs();
  }
*/

  if (! mSequenceClockName.empty()) {
    PVContext* pvc = getPVContext();

    // assume 'top' for the moment...
    UtString name("top.");
    name << mSequenceClockName;

    mSequenceClockNet = pvc->getPVNet(name.c_str());
    // we will not report an error here because if we are
    // running an optimized design then we will not have
    // the sequence clocks.


    // we will validate that we can find this net later in
    // PVContext::putModel when we loop through all the nets
    // we found.
  }
  return true;
}

bool PVNet::initialize(CarbonObjectID* model) {
  mShellNet = carbonFindNet(model, mNetName.c_str());

  // sanity check to make sure the shell and the API agree how many UInt32s
  // are required to write this net
  if (mShellNet != NULL) {
    CarbonUInt32 shell_num_int32s = carbonGetNumUInt32s(mShellNet);
    INFO_ASSERT(shell_num_int32s == ((mBitSize + 31) / 32),
                "net size insanity");

    if (mIsReadable) {
      mCallback = carbonAddNetValueChangeCB(model, valueChangedCB,
                                            this, mShellNet);
    }

    return true;
  }
  return false;
}
