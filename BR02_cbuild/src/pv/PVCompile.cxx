//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "compiler_driver/CarbonContext.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUNet.h"
#include "pv/PVCompile.h"

PVCompileContext::PVCompileContext(CarbonContext* cc)
  : mMasterCarbonContext(cc)
{
}


PVCompileContext::~PVCompileContext() {
  mPhaseDesignMap.clearPointerValues();
}

UInt32 PVCompileContext::findNetSize(const char* net_name) const {
  NUDesign* master_design = getMasterDesign();

// This doesn't work unless the NetElabs have been created at the
// time it's called, and the current organization of the PV optimization
// code does not have it so.
//
//  NUNetElab* net_elab = master_design->findNetElab(net_name);
//  if (net_elab == NULL) {
//    return 0;
//  }
//  NUNet* net = net_elab->getNet();
//  

  // This appears to work, but depends on the designs being
  // flat coming into PV optimization.
  NUNet* net = master_design->findNet(net_name);
  if (net == NULL) {
    return 0;
  }

  return net->getBitSize();
}


NUDesign* PVCompileContext::getMasterDesign() const {
  return mMasterCarbonContext->getDesign();
}

CarbonContext* PVCompileContext::getMasterCarbonContext() const {
  return mMasterCarbonContext;
}
