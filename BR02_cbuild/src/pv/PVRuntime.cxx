//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// Implementation of the Programmers View API.  Note that this one API
// is usable on PV-optimized models and CA-models.  In the CA models
// we need to call carbonSchedule on each PVPhase, whereas in the PV-optimized
// models we only need to call it on each PVSequence.

#include "pv/PVRuntime.h"
#include "shell/carbon_capi.h"
#include "shell/CarbonModel.h"
#include "util/UtIOStream.h"
#include "iodb/IODBRuntime.h"
#include "hdl/HdlId.h"
#include "symtab/STSymbolTable.h"

static const CarbonUInt32 sOne = 1;
static const CarbonUInt32 sZero = 0;

UInt32 PVRuntimeContext::findNetSize(const char* net_name) const {
  IODBRuntime* iodb = mModel->getIODB();
  HdlHierPath::Status stat;
  HdlId info;
  STSymbolTable* sym_tab = iodb->getIOSymbolTable();
  STSymbolTableNode* node = sym_tab->getNode(net_name, &stat, &info);
  if (node == NULL) {
    return 0;
  }
  return iodb->getWidth(node);
}

void PVRuntimeContext::runSchedule() {
  carbonSchedule(mModel, mTime);
  mTime += mTimeDelta;
}

void PVTransaction::read(char* data) {
  for (SequenceLoop s(loopSequences()); !s.atEnd(); ++s) {
    PVSequence* sequence = *s;

    // here we should be handing semantics like executing the read
    // N times where N is data dependent, and onDemand idle-checking.
    // But for now we'll just do one read.
    sequence->read(mAddressStart, data);
  }

  for (PVDataPortLoop p(mReadPorts); !p.atEnd(); ++p) {
    PVDataPort* port = *p;
    port->read(data);
  }
}

void PVSequence::read(UInt32 address, char* data) {
  if (mSequenceClockNet == NULL) {
    // unaccelerated sequence.  clock on every phase

    for (PhaseLoop p(loopPhases()); !p.atEnd(); ++p) {
      PVPhase* phase = *p;
      phase->setupTies();
      phase->setupAddress(address);
      mTransaction->toggleClock();
      phase->readData(data);
    }
  }
  else {
    // Accelerated sequence.  Run schedule only once.  Well, twice, actually.

    for (PhaseLoop p(loopPhases()); !p.atEnd(); ++p) {
      PVPhase* phase = *p;
      phase->setupAddress(address);
    }

    toggleClock();

    // Now we've clocked in the address, read out the data
    for (PhaseLoop p(loopPhases()); !p.atEnd(); ++p) {
      PVPhase* phase = *p;
      phase->readData(data);
    }
  }
}

void PVTiePort::deposit() {
  mNet->deposit(mValue);
}

void PVAddressPort::deposit(CarbonUInt32 addr) {
  if (mShift > 0) {
    addr >>= mShift;
  }
  else {
    addr <<= -mShift;
  }
  addr += mOffset;
  CarbonUInt32* buf = mNet->getBuffer();
  buf[0] = addr;
  mNet->deposit(buf);
}

void PVPhase::setupAddress(UInt32 address) {
  for (PVAddressPortLoop p(mAddressPorts); !p.atEnd(); ++p) {
    PVAddressPort* port = *p;
    port->deposit(address);
  }
}

void PVPhase::setupTies() {
  for (PVTiePortLoop t(mTiePorts); !t.atEnd(); ++t) {
    PVTiePort* tie = *t;
    tie->deposit();
  }    
}

void PVPhase::readData(char* data) {
  for (PVDataPortLoop p(mReadPorts); !p.atEnd(); ++p) {
    PVDataPort* port = *p;
    port->read(data);
  }
}

void PVTransaction::write(const char* data) {
  for (SequenceLoop s(loopSequences()); !s.atEnd(); ++s) {
    PVSequence* sequence = *s;

    // here we should be handing semantics like executing the read
    // N times where N is data dependent, and onDemand idle-checking.
    // But for now we'll just do one read.
    sequence->write(mAddressStart, data);
  }

  for (PVDataPortLoop p(mWritePorts); !p.atEnd(); ++p) {
    PVDataPort* port = *p;
    port->write(data);
  }
}

void PVSequence::toggleClock() {
  // Pulse the sequence.  The second one is really a no-op since
  // the PV-optimized model will not be doing anything on the negedge.
  // We can get rid of the second call -- need to talk to Jason.
  mSequenceClockNet->deposit(&sOne);
  getPVContext()->runSchedule();
  mSequenceClockNet->deposit(&sZero);
  getPVContext()->runSchedule();
}  

void PVTransaction::toggleClock() {
  // Pulse the sequence.  The second one is really a no-op since
  // the PV-optimized model will not be doing anything on the negedge.
  // We can get rid of the second call -- need to talk to Jason.
  mTransactionClockNet->deposit(&sOne);
  getPVContext()->runSchedule();
  mTransactionClockNet->deposit(&sZero);
  getPVContext()->runSchedule();
}

void PVSequence::write(UInt32 address, const char* data) {
  if (mSequenceClockNet == NULL) {
    for (PhaseLoop p(loopPhases()); !p.atEnd(); ++p) {
      PVPhase* phase = *p;
      phase->write(address, data);
      mTransaction->toggleClock();
    }
  }
  else {
    for (PhaseLoop p(loopPhases()); !p.atEnd(); ++p) {
      PVPhase* phase = *p;
      phase->write(address, data);
    }
    toggleClock();
  }
}

void PVPhase::write(UInt32 address, const char* data) {
  // set up the ties.  This will be empty in an optimized PV model, but will
  // have to be deposited as live nets when running a cycle-accurate model
  // in PV mode.
  for (PVTiePortLoop t(mTiePorts); !t.atEnd(); ++t) {
    PVTiePort* tie = *t;
    tie->deposit();
  }    

  // write the inpus, based on the current data
  for (PVDataPortLoop p(mWritePorts); !p.atEnd(); ++p) {
    PVDataPort* port = *p;
    port->write(data);
  }

  for (PVAddressPortLoop p(mAddressPorts); !p.atEnd(); ++p) {
    PVAddressPort* port = *p;
    port->deposit(address);
  }
}

void PVNet::deposit(const CarbonUInt32* val, const CarbonUInt32* drive) {
  (void) carbonDeposit(mPVContext->getModel(), mShellNet, val, drive);
}

void PVNet::valueChangedCB(CarbonObjectID*, CarbonNetID*,
                           CarbonClientData client_data,
                           CarbonUInt32* val, CarbonUInt32*)
{
  PVNet* pv_net = (PVNet*) client_data;
  memcpy(pv_net->mBuffer, val, pv_net->numBytes());
}

void PVNet::examine() {
  (void) carbonExamine(mPVContext->getModel(), mShellNet, mBuffer, NULL);
}

void PVDataPort::read(char* data) {
  data += mByteOffset;

  // think about endian issues!  data limiting!
  memcpy(data, (char*) mNet->getBuffer(), mNet->numBytes());
}

void PVDataPort::write(const char* data) {
  data += mByteOffset;

  // think about endian issues!  data limiting!
  CarbonUInt32* buf = mNet->getBuffer();
  memset(buf, '0', 4*mNet->numWords());
  memcpy((char*) buf, data, mNet->numBytes());
  mNet->deposit(buf);
}

struct PVFindTransaction {
  PVFindTransaction(UInt32 addr) : mAddr(addr) {}
  bool operator<(const PVTransaction* transaction) const {
    return mAddr < transaction->getAddressStart();
  }
  UInt32 mAddr;
};

bool operator<(const PVTransaction* t, const PVFindTransaction& f)
{
  return t->getAddressStart() < f.mAddr;
}

PVAddressToTransactionContext::PVAddressToTransactionContext(const PVContext::TVec& tvec) :
  mTVec(tvec)
{
  reset(0, 0, 0);
  mTransaction = NULL;
  mIndex = 0;
}

void PVAddressToTransactionContext::clear() {
  mTransaction = NULL;
}

void PVAddressToTransactionContext::reset(char* target_addr,
                                          const char* base_addr,
                                          UInt32 byte_count)
{
  mAddr = target_addr - base_addr;
  mByteCount = byte_count;
  mErrorOccurred = false;
  mIsTransactionFull = false;
  mTransactionByteCount = 0;
}

bool PVAddressToTransactionContext::transfer(char* target_addr,
                                             const char* base_addr,
                                             UInt32 byte_count)
{
  UInt32 old_addr = mAddr;
  reset(target_addr, base_addr, byte_count);

  if ((target_addr <= base_addr) || (byte_count == 0)) {
    mErrorOccurred = target_addr < base_addr;
  }
  else if ((mTransaction != NULL) && (mAddr == old_addr)) {
    // This transfer is consecutive with previous one.  Let the
    // caller decide how to manage that
    INFO_ASSERT(mTransaction->getAddressStart() <= mAddr, "xactor cont bounds");
    INFO_ASSERT(mAddr <= mTransaction->getAddressEnd(), "xactor cont bounds");
    computeTransactionBounds();
    return true;              // consecutive
  }
  findFirstTransaction();
  return false;
}

void PVAddressToTransactionContext::operator++() {
  mAddr += mTransactionByteCount;
  INFO_ASSERT(mByteCount >= mTransactionByteCount, "byte count insanity");
  mByteCount -= mTransactionByteCount;
  mTransaction = NULL;

  // You can only increment if we are not atEnd(), and we at the
  // end of the transfer if we have exhausted the byte-count without
  // finishing the transaction
  INFO_ASSERT(mIsTransactionFull, "incrementing but transfer is already complete");

  // If there are no bytes left, then we are done.
  if (mByteCount == 0) {
    // normal termination
    return;
  }

  // Otherwise we expect the next transaction's address range to abut
  // the current one.  Otherwise we have a write to an illegal zone
  ++mIndex;
  if (mIndex == mTVec.size()) {
    mErrorOccurred = true;
    return;
  }

  mTransaction = mTVec[mIndex];
  if (mTransaction->getAddressStart() != mAddr) {
    mTransaction = NULL;
    mErrorOccurred = true;
  }
  else {
    computeTransactionBounds();
  }
} // void operator++

void PVAddressToTransactionContext::findFirstTransaction() {
  // Find the transaction with the lowest address that intersects this
  // data.  See http://www.sgi.com/tech/stl/equal_range.html.
  //
  // We only need to look up the first address with this function because
  // our transaction-vector is already sorted.
  std::pair<PVContext::TVec::const_iterator,PVContext::TVec::const_iterator> p =
    std::equal_range(mTVec.begin(), mTVec.end(), PVFindTransaction(mAddr));
  UInt32 i1 = p.first - mTVec.begin();
  UInt32 i2 = p.second - mTVec.begin();

  // It's easier for me to reason about indexes.  If i1==i2 then
  // that means that we haven't landed exactly on a transaction's
  // starting address.  
  if (i1 == i2) {
    // But if i1==i2==0, that means that the address
    // is *before* the lowest-address tranansaction we know about, so
    // that's NG.  
    //
    // We will issue an error message and take no action when we are
    // out-of-bounds in either case.
    //
    // Perhaps we *should* execute the write for any in-bounds transactions???
    if (i1 != 0) {
      // Subtract 1 and we get get the index of the highest-address transaction
      // below the desired address.  See if it intersects.
      mIndex = i1 - 1;
      mTransaction = mTVec[mIndex];

      if (mTransaction->getAddressEnd() < mAddr) {
        // this address is outside the bounds of the target transaction
        mTransaction = NULL;
        mErrorOccurred = true;
      }
    }
  }

  // We don't currently allow overlapping transactions so the delta should
  // be 1.
  else if (i2 - i1 == 1) {
    // In this scenario i1 is the transaction we want, and we will have hit
    // its start address precisely -- this is an aligned access.
    mIndex = i1;
    mTransaction = mTVec[mIndex];
    INFO_ASSERT(mAddr == mTransaction->getAddressStart(), "bsearch insanity");
  }
  else {
    mErrorOccurred = true;
  }

  if (mTransaction != NULL) {
    computeTransactionBounds();
  }
} // void PVAddressToTransactionContext::findFirstTransaction

void PVAddressToTransactionContext::computeTransactionBounds() {
  UInt32 xsize = mTransaction->getSize();
  UInt32 xstart = mTransaction->getAddressStart();
  UInt32 xend = mTransaction->getAddressEnd();
  if ((mAddr == xstart) && (mByteCount >= xsize)) {
    mIsTransactionFull = true;
    mTransactionByteCount = xsize;
  }
  else {
    mIsTransactionFull = false;
    xend = std::min(mAddr + mByteCount - 1, xend);
    mTransactionByteCount = xend - mAddr + 1;
  }
}

// Write a block of data from the Programmers View.  Break down the
// address-range into a set of transactions within that range.
//
// Note that the data provided may cover more than one transaction's
// address range, and it may cover any subset of a complete transaction.
//
// This is the routine where that gets resolved.  An incomplete write will
// be executed as a read/modify/write.  At some point we should handle
// the buffering of consecutive writes, as the programmer may have written
// a multi-byte register one byte at a time, but it should be functional
// to 
// 
// Note that from the programmer's perspective, the byte is the smallest
// level of granularity.

bool PVRuntimeContext::write(char* target_addr, const char* data, UInt32 byte_count) {
  // flush any cached read data, it's no longer valid after this write
  mReader.clear();

  // Now begin carving up this block into its constituent transactions,
  // but first, if this write was not contiguous with a previous one,
  // then flush that one
  PVTransaction* transaction = mWriter.getTransaction();
  if (mWriter.transfer(target_addr, mBaseAddress, byte_count)) {
    // This new address is consecucutive with a previous incomplete write,
    // so copy the new data into the already existing buffer
    INFO_ASSERT(transaction != NULL, "consecutive with null transaction");
    UInt32 tbyte_count = mWriter.getTransactionByteCount();
    memcpy(mBufferPtr, data, tbyte_count);
    mBufferPtr += tbyte_count;
    if (mWriter.reachesEndOfTransaction()) {
      // should do read/modify/write.  For now if the transaction was
      // initiated in the middle of the buffer we will write garbage.
      // another alternative is to report an error (unaligned access).
      transaction->write(mBuffer.getBuffer());
    }
    else {
      // otherwise we must have exhausted the incoming data buffer
      // so there is no reason to continue any further
      INFO_ASSERT(mWriter.atEnd(), "inc buf insanity");
      return true;
    }
  }

  // 'transaction' was never fully  written.  Do the write now, before we
  // start iterating through the transactions pointed at by the
  // new data
  else if (transaction != NULL) {
    transaction->write(mBuffer.getBuffer());
  }    

  // Now we loop through the complete transactions & write the data
  for (; !mWriter.atEnd(); ++mWriter) {
    transaction = mWriter.getTransaction();
    transaction->write(data);
    data += mWriter.getTransactionByteCount();
  }

  // any incomplete write transactions will be deferred to the
  // next call to read or write
  transaction = mWriter.getTransaction();
  if (transaction != NULL) {
    mBuffer.resize(transaction->getSize());
    mBufferPtr = mBuffer.getBuffer();
    UInt32 tbyte_count = mWriter.getTransactionByteCount();
    memcpy(mBufferPtr, data, tbyte_count);
    mBufferPtr += tbyte_count;
  }
  return mWriter.errorOccurred();
} // bool PVRuntimeContext::write

bool PVRuntimeContext::read(char* target_addr, char* data, UInt32 byte_count) {
  // If there was a write queued, then execute it.  Note that this
  PVTransaction* transaction = mWriter.getTransaction();
  if (transaction != NULL) {
    transaction->write(mBuffer.getBuffer());
    mWriter.clear();
  }

  // Now begin carving up this block into its constituent transactions,
  // but first, if this write was not contiguous with a previous one,
  // then flush that one
  transaction = mReader.getTransaction();
  if (mReader.transfer(target_addr, mBaseAddress, byte_count)) {
    // This new address is consecucutive with a previous incomplete read.
    // We already have all the data buffered so we need only copy it
    INFO_ASSERT(transaction != NULL, "consecutive with null transaction");
    UInt32 tbyte_count = mReader.getTransactionByteCount();
    memcpy(data, mBufferPtr, tbyte_count);
    mBufferPtr += tbyte_count;
    if (! mReader.reachesEndOfTransaction()) {
      // otherwise we must have provided the number of bytes
      // that the programmers has requested to read, so
      // we are done for now, but if he re-calls us we have
      // all the data ready to go.
      INFO_ASSERT(mReader.atEnd(), "inc buf insanity");
      return true;
    }
  }

  // Now we loop through the complete transactions & read the data
  for (; !mReader.atEnd(); ++mReader) {
    transaction = mReader.getTransaction();
    transaction->read(data);
    data += mReader.getTransactionByteCount();
  }

  // any incomplete read transactions require us to execute the 
  // read transaction and buffer the results for the next call
  transaction = mReader.getTransaction();
  if (transaction != NULL) {
    mBuffer.resize(transaction->getSize());
    mBufferPtr = mBuffer.getBuffer();
    UInt32 tbyte_count = mReader.getTransactionByteCount();
    transaction->read(mBufferPtr);
    memcpy(data, mBufferPtr, tbyte_count);
    mBufferPtr += tbyte_count;
  }
  return mReader.errorOccurred();
} // bool PVRuntimeContext::read

CarbonObjectID* PVRuntimeContext::getModel() const {
  return mModel;
}
