//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "compiler_driver/CarbonContext.h"
#include "globopts/GlobOpts.h"
#include "iodb/IODBNucleus.h"
#include "iodb/ScheduleFactory.h"
#include "localflow/MarkReadWriteNets.h"
#include "localflow/NucleusSanity.h"
#include "localflow/UD.h"
#include "localflow/UpdateUD.h"
#include "localflow/UnelabFlow.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCopyContext.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUDesignWalker.h"
#include "pv/PVCompile.h"
#include "reduce/Fold.h"
#include "reduce/RemoveUnelabFlow.h"
#include "reduce/TieNets.h"
#include "schedule/Schedule.h"
#include "util/ArgProc.h"
#include "util/AtomicCache.h"
#include "util/Stats.h"
#include "util/UtArray.h"
#include "util/UtString.h"
#include "util/UtStringUtil.h"

bool PVCompileContext::tieInputPort(const char* net_name,
                                    const char* val,
                                    PVPhase* phase)
{
  bool ret = PVContext::tieInputPort(net_name, val, phase);
  if (ret) {
    ret = signalDirective(phase, "tieNet", net_name, val);
  }
  return ret;
}

/*
bool PVPhase::bindInputData(const char* net_name, UInt32 byte_offset) {
  NUDesign* master_design = getMasterDesign();
  NUNet* net = master_design->findNet(net_name);
  if (net == NULL) {
    return false;               // issue error message?
  }
  PVNet* pv_net = getPVContext()->getPVNet(net_name, net->getBitSize());
  mReadPorts.push_back(new PVDataPort(pv_net, byte_offset, true, false));
  return true;
}
*/

bool PVCompileContext::signalDirective(PVPhase* phase,
                                       const char* directive_name,
                                       const char* net_name,
                                       const char* val)
{
  UtString directive;
  directive << directive_name;
  if (val != NULL) {
    directive << " " << val;
  }
  directive << " " << net_name;

  // The interface to IODB::parseDirective involves passing
  // the StrToken context, in addition to the "rest" of the
  // string, so we must now recreate the same state that
  // the directives file parser is in when it calls parseDirective
  // for a semi-parsed line.
  StrToken dirTok(directive.c_str());
  ++dirTok;

  NUDesign* phase_design = mPhaseDesignMap[phase];
  IODBNucleus* iodb = phase_design->getIODB();
  SourceLocatorFactory* slc = mMasterCarbonContext->getSourceLocatorFactory();
  SourceLocator loc = slc->create("-", 0);
  return iodb->parseDirective(directive_name, dirTok.curPos(), dirTok, loc);
}

//! helper functor to uniquify multiplexed phase i/o nets.  E.g. in
//! test/pv/alu/alu.ccfg, the 'mult' transaction maps 'in' to 'a' in
//! phase a1, and to 'b' in phase b1.
class PVNetSwap : public NuToNuFn {
public:

  PVNetSwap(const NUNetReplacementMap& rep_map) : mRepMap(rep_map) {}

  virtual NUNet * operator()(NUNet* net, Phase phase) {
    if (!isPre(phase)) {
      return NULL;
    }
    NUNetReplacementMap::const_iterator p = mRepMap.find(net);
    if (p != mRepMap.end()) {
      return p->second;
    }
    return NULL;
  }
  
  const NUNetReplacementMap& mRepMap;
};

//! helper functor to translate nets (by name) into a new module
class PVTransactionToMaster : public NuToNuFn {
public:

  PVTransactionToMaster(NUModule* master_mod,
                        const char* prefix) :
    mMasterModule(master_mod),
    mMasterIODB(master_mod->getDesign()->getIODB()),
    mMasterTable(master_mod->getAliasDB()),
    mPrefix(prefix)
  {
    mAtomicCache = mMasterTable->getAtomicCache();
  }

  NUNet* findMasterNet(NUNet* transaction_net) {
    STSymbolTableNode* transaction_node = transaction_net->getNameLeaf();
    STSymbolTableNode* master_node = mMasterTable->lookup(transaction_node);
    NUNet* master_net = NULL;

    if (master_node != NULL) {
      master_net = NUNet::findUnelab(master_node);
      if ((master_net != NULL) &&
          !master_net->isFlop() &&
          !master_net->isLatch() &&
          !master_net->isPort() &&
          !master_net->isProtected(mMasterIODB))
      {
        master_net = NULL;
      }
    }
    return master_net;
  }

  virtual NUNet * operator()(NUNet* transaction_net, Phase phase) {
    if (!isPre(phase)) {
      return NULL;
    }

    // We do not want to share combinational nets between different nodes --
    // we can wind up with multiple driver situations.  It's better to
    // uniquify the combinational nets per transaction-id.  Dead logic
    // elimination should eliminate the copies of the drivers to the
    // combinational nets replicated for trasnactions that don't really
    // care about them.  When two transactors care about the same combinational
    // net then we might be able to share the code via Congruence at some
    // point in the future.
    NUNet* master_net = findMasterNet(transaction_net);

    if (master_net == NULL) {
      NUNetReplacementMap::iterator p = mNetReplacements.find(transaction_net);
      if (p == mNetReplacements.end()) {
        // This net may have been created by constant propagation or uniqufied
        // because it is not state.  Create a new temp net.
        master_net = mMasterModule->createTempNetFromImage(transaction_net,
                                                           mPrefix.c_str());
        mNetReplacements[transaction_net] = master_net;
      }
      else {
        master_net = p->second;
      }
    }
    return master_net;
  } // virtual NUNet * operator

private:
  NUModule* mMasterModule;
  IODBNucleus* mMasterIODB;
  STSymbolTable* mMasterTable;
  AtomicCache* mAtomicCache;
  UtString mPrefix;
  NUNetReplacementMap mNetReplacements;
};

class ObserveStateCB : public NUDesignCallback {
public:
  ObserveStateCB(PVCompileContext* cc, PVPhase* phase) :
    mCompileContext(cc),
    mPhase(phase),
    mStatus(true)
  {
  }
    
  Status operator()(Phase, NUBase*) {
    return eNormal;
  }

  Status operator()(Phase phase, NUNet* net) {
    if (phase == ePre) {
      if (net->isFlop()) {
        UtString path, local_path;
        net->getNameLeaf()->getMaster()->verilogCompose(&local_path);
        path << net->getScope()->getModule()->getOriginalName()->str()
             << "." << local_path;
        mStatus &= mCompileContext->signalDirective(mPhase, "observeSignal",
                                                    path.c_str(), NULL);
        mStatus &= mCompileContext->signalDirective(mPhase, "depositSignal",
                                                    path.c_str(), NULL);
      }
    }
    return eNormal;
  }

  bool getStatus() const {return mStatus;}

  PVCompileContext* mCompileContext;
  PVPhase* mPhase;
  bool mStatus;
}; // class ObserveStateCB : public NUDesignCallback

void PVCompileContext::createMuxedIONets(PVPhase* phase, PVDataPortLoop ports)
{
  // Create copies of multiplexed data nets in the original design, before
  // cloning the design.
  NUNetReplacementMap rep_map;
  NUDesign* master_design = getMasterDesign();
  for (; !ports.atEnd(); ++ports) {
    PVDataPort* port = *ports;
    const char* muxed_name = port->getMultiplexedName();
    if (*muxed_name != '\0') {
      PVNet* pv_net = port->getNet();
      NUNet* orig_net = master_design->findNet(pv_net->getName());
      INFO_ASSERT(orig_net, "cannot find data net");

      // Get the local name of the phase-muxed net.  Don't use muxed_name
      // cause that's qualified by the root module and I'm creating a local net
      UtString local_name;
      phase->getPhaseNetName(orig_net->getName()->str(), &local_name);
      StringAtom* sym = master_design->getAtomicCache()->intern(local_name);
      NUScope* scope = orig_net->getScope();
      NUModule* mod = scope->getModule();
      NU_ASSERT(orig_net->isPort(), orig_net);
      NetFlags flags = orig_net->getFlags();
      flags = NetFlags(flags | eTempNet);
      NUNet* phase_net = orig_net->clone(mod, rep_map, sym, flags);
      NU_ASSERT(scope == mod, orig_net);
      mod->addPort(phase_net);
    }
  }
} // void PVCompileContext::createMuxedIONets

void PVCompileContext::findMuxedIOReferences(PVPhase* phase,
                                             PVDataPortLoop ports,
                                             NUNetReplacementMap* rep_map)
{
  NUDesign* phase_design = mPhaseDesignMap[phase];
  for (; !ports.atEnd(); ++ports) {
    PVDataPort* port = *ports;
    const char* muxed_name = port->getMultiplexedName();
    if (*muxed_name != '\0') {
      PVNet* pv_net = port->getNet();
      NUNet* orig_net = phase_design->findNet(pv_net->getName());
      PVNet* muxed_pv_net = getPVNet(muxed_name);
      INFO_ASSERT(muxed_pv_net, "could not find muxed_pv_net in phase design");
      NUNet* muxed_net = phase_design->findNet(muxed_name);
      INFO_ASSERT(muxed_net, "could not find muxed_net in phase design");

      // set up the replacement map for mutating the phase_design
      (*rep_map)[orig_net] = muxed_net;
    }
  }
}

bool PVCompileContext::applyTieDirectives(PVPhase* phase) {
  bool ret = true;
  // Apply tie directives
  for (PVTiePortLoop t(phase->loopTies()); !t.atEnd(); ++t) {
    PVTiePort* tie = *t;
    PVNet* net = tie->getNet();
    UtString vlog_tie_val;
    vlog_tie_val << net->numBits() << "'h" << tie->getValueString();
    ret &= signalDirective(phase, "tieNet", net->getName(),
                           vlog_tie_val.c_str());
  }
  return ret;
}

bool PVCompileContext::optimizePhase(PVPhase* phase, NUStmtList* stmts) {
  NUDesign* master_design = getMasterDesign();

  createMuxedIONets(phase, phase->loopWritePorts());
  createMuxedIONets(phase, phase->loopReadPorts());

  // Now clone the design & check to make sure it's sane
  NUDesign* phase_design = master_design->copy();
  mPhaseDesignMap[phase] = phase_design;
  NucleusSanity nucleusSanity;
  nucleusSanity.design(phase_design, NucleusSanity::eAfterResynth);
  
  // Map all the references to multiplexed nets for this phase
  NUNetReplacementMap rep_map;
  findMuxedIOReferences(phase, phase->loopWritePorts(), &rep_map);
  findMuxedIOReferences(phase, phase->loopReadPorts(), &rep_map);

  // transform the phase design to use the muxed nets
  PVNetSwap swapper(rep_map);
  for (NUDesign::ModuleLoop m(phase_design->loopAllModules()); !m.atEnd(); ++m)
  {
    NUModule* mod = *m;
    mod->replaceLeaves(swapper);
  }

  bool ret = applyTieDirectives(phase);

  // Observe all state nets.
  ObserveStateCB oscb(this, phase);
  NUDesignWalker walker(oscb, false, false);
  walker.putWalkDeclaredNets(true);
  walker.design(phase_design);
  ret = ret &&
    oscb.getStatus() &&
    optimizeConstants(phase_design) &&
    schedulePhase(phase, stmts);
  return ret;
}

bool PVCompileContext::optimizeConstants(NUDesign* phase_design) {
  bool cont = true;
  bool status = true;

  STSymbolTable* symtab = phase_design->getSymbolTable();
  AtomicCache* acache = symtab->getAtomicCache();
  IODBNucleus* iodb = phase_design->getIODB();
  MsgContext* msg_context = phase_design->getMsgContext();
  NUNetRefFactory* nrf = phase_design->getNetRefFactory();
  ArgProc* args = phase_design->getArgProc();

  iodb->evaluateDesignDirectives(phase_design);

  {
    Fold fold (nrf, args, msg_context, acache, iodb,
               false, eFoldUDValid);     // Don't destroy defs (avoid q=q deletion)
    NUNetSet* tie_net_temps = phase_design->getTieNetTemps();
    RETieNets tieNets(phase_design, acache, iodb, msg_context, &fold,
                      true, true, tie_net_temps);
    tieNets.processDirectives();
    tieNets.optimize(true);
  }

  SourceLocatorFactory* slc = phase_design->getSourceLocatorFactory();
  NUModuleList mods;
  phase_design->getModulesBottomUp(&mods);
  
  TristateModeT tmode = mMasterCarbonContext->getTristateMode();
  Stats* stats = mMasterCarbonContext->getStats();

  for (int i = 0; (i < 3) && cont && status; ++i) {
    {
      GOGlobOpts globOpts(acache, iodb, stats,
                          args, msg_context, nrf, slc, 
                          "", // mCongruencyInfoFilename.c_str(),
                          NULL, // &wiredNetResynth,S
                          tmode);
      bool print_warnings = false;
      status = globOpts.optimize(phase_design, "libdesign", //getFileRoot(),
                                 print_warnings);

      cont = globOpts.optimizationOccurred();
    }

    if (/*cont && */ status) {
      FLNodeFactory flow_factory;
      UnelabFlow unelab_flow(&flow_factory, nrf, msg_context, iodb, false);
      UD ud(nrf, msg_context, iodb, args, false);
      MarkReadWriteNets marker(iodb, args, true);
      ClearUDCallback clearUseDef(false); // do not recurse
      NUDesignWalker clearUDWalker(clearUseDef, false);

      Fold fold(nrf, args, msg_context, acache, iodb, 
                args->getBoolValue("-verboseFold"),
                eFoldUDValid | eFoldUDKiller | eFoldComputeUD);
      cont = false;

      for (Loop<NUModuleList> p(mods); !p.atEnd(); ++p) {
        NUModule* mod = *p;
        clearUDWalker.module(mod);
        ud.module(mod, true);
        marker.module(mod);
        unelab_flow.module(mod, false);
      }
      fold.design(phase_design);
      cont = fold.isChanged();

      {
        RemoveUnelabFlow remove(&flow_factory);
        remove.remove();
      }
    }
  } // for

  // Clean up
  CarbonMem::releaseBlocks();
  return status;
} // bool PVCompileContext::optimizeConstants

// the scheduler attempts to compute 
bool PVCompileContext::schedulePhase(PVPhase* phase, NUStmtList* stmts) {
  NUDesign* phase_design = mPhaseDesignMap[phase];
  INFO_ASSERT(phase_design, "Could not find design for a phase");

  UtString suffix;
  suffix << ".phase_" << phase->getName();
  CarbonContext cc(phase_design, mMasterCarbonContext, suffix.c_str());

  for (CarbonContext::Phase compiler_phase = CarbonContext::ePortAnalysis;
       compiler_phase <= CarbonContext::eSchedule;
       compiler_phase = (CarbonContext::Phase) ((int) compiler_phase + 1))
  {
    if (!cc.runPhase(compiler_phase)) {
      return false;
    }
  }

  // Now we have scheduled the design.  We can then "codegen" back
  // into Nucleus to get a linear sequence of statements to be
  // executed for this transaction ID.  This could be a long list,
  // but let's go for it!
  
  SCHSchedule* sched = cc.getSchedule();

  PVTransaction* transaction = phase->getSequence()->getTransaction();
  NUNetElab* clk_net_elab = phase_design->findNetElab(transaction->getClkName());
  INFO_ASSERT(clk_net_elab, "cannot find elaborated clock");

  scheduleComb(stmts, sched->getInitialSchedule());
  scheduleComb(stmts, sched->getInitSettleSchedule());

  scheduleEdge(stmts, sched, clk_net_elab, eClockPosedge);
  scheduleEdge(stmts, sched, clk_net_elab, eClockNegedge);

  // Having scheduled all the statements, we can remove the blocks in the
  // phase design image.  We will need to retain the net declarations
  // however
/*
 * Cannot do this yet because I get NetRef destruction errors on 
 * local nets
 *
 * NUModuleList mods;
 * phase_design->getModulesBottomUp(&mods);
 * for (Loop<NUModuleList> p(mods); !p.atEnd(); ++p) {
 *   NUModule* mod = *p;
 *   mod->removeBlocks();
 * }
 */
  return true;
} // bool PVCompileContext::schedulePhase

static void copyStmts(NUStmtList* stmts, NUUseDefNode* node) {
  CopyContext cc(NULL, NULL);

  if (NUAlwaysBlock* ab = dynamic_cast<NUAlwaysBlock*>(node)) {
    NUBlock* blk = ab->getBlock();
    for (NUStmtLoop p(blk->loopStmts()); !p.atEnd(); ++p) {
      NUStmt* stmt = *p;
      NUStmt* cpy = stmt->copy(cc);
      stmts->push_back(cpy);
    }
  }
  else if (NUContAssign* ca = dynamic_cast<NUContAssign*>(node)) {
    // Don't copy over pull drivers from the schedule.  These are
    // created for otherwise undriven output state nets because all
    // the normal drivers were dead-logiced away for this phase.
    // We mark it depositable, but port coercion doesn't interpret
    // deposits as strong drivers, and will coerce the output to
    // an inout.  That will then get marked as a 'primary-z' and
    // get an initial driver.  We don't want to drive this shared
    // net from this phase because it's driven from another phase.
    //
    // Legitimate tri-state nets will need to get sorted out at
    // some point.  Right now they are NYI.
    if (ca->getStrength() != eStrPull) {
      NULvalue* lv = ca->getLvalue()->copy(cc);
      NUExpr* rv = ca->getRvalue()->copy(cc);
      NUStmt* ba = new NUBlockingAssign(lv, rv, false, ca->getLoc(), true);
      stmts->push_back(ba);
    }
  }
  else {
    // Handle all the other types.  For now, ignore them.
  }
} // static void copyStmts

void PVCompileContext::scheduleComb(NUStmtList* stmts, SCHCombinational* comb)
{
  // Ignore the mask for now.   We could perhaps test the mask against
  // the clock-edge we are trying to run.

  for (SCHIterator iter = comb->getSchedule(); !iter.atEnd(); ++iter) {
    FLNodeElab* flow = *iter;
    copyStmts(stmts, flow->getUseDefNode());
  }
}

void PVCompileContext::scheduleEdge(NUStmtList* stmts,
                                    SCHSchedule* sched,
                                    NUNetElab* clk_net,
                                    ClockEdge edge)
{
  // For the moment, ignore the derived clock schedules.  Run the
  // sample schedules first.
  
  scheduleComb(stmts, sched->getForceDepositSchedule());
  for (SCHSchedule::CombinationalLoop p
         = sched->loopCombinational(eCombInput); !p.atEnd(); ++p)
  {
    scheduleComb(stmts, *p);    
  }
  for (SCHSchedule::CombinationalLoop p
         = sched->loopCombinational(eCombAsync); !p.atEnd(); ++p)
  {
    scheduleComb(stmts, *p);
  }

  SCHScheduleFactory* sched_factory = sched->getFactory();
  const SCHEvent* event = sched_factory->buildClockEdge(clk_net->getSymNode(),
                                                        edge);

  for (SCHSchedule::CombinationalLoop p
         = sched->loopCombinational(eCombSample); !p.atEnd(); ++p)
  {
    SCHCombinational* comb = *p;
    const SCHScheduleMask* mask = comb->getMask();
    if (mask->hasEvent(event)) {
      scheduleComb(stmts, comb);
    }
  }

  
  for (SCHSequentials::SequentialLoop p = sched->loopSequential();
       !p.atEnd(); ++p)
  {
    SCHSequential* seq = *p;
    if (seq->getEdgeNet() == clk_net) {
      for (SCHIterator iter = seq->getSchedule(edge); !iter.atEnd(); ++iter) {
        FLNodeElab* flow = *iter;
        copyStmts(stmts, flow->getUseDefNode());
      }
    }
  }      
      
  for (SCHSchedule::CombinationalLoop p
         = sched->loopCombinational(eCombTransition); !p.atEnd(); ++p)
  {
    SCHCombinational* comb = *p;
    const SCHScheduleMask* mask = comb->getMask();
    if (mask->hasEvent(event)) {
      scheduleComb(stmts, comb);
    }
  }
} // void PVCompileContext::scheduleEdge

void PVCompileContext::copyStmtsToRoot(NUModule* root, NUStmtList* stmts,
                                       PVSequence* sequence)
{
  NUDesign* master_design = getMasterDesign();
  STSymbolTable* symtab = master_design->getSymbolTable();
  AtomicCache* acache = symtab->getAtomicCache();
  const SourceLocator& loc = root->getLoc();
  StringAtom* sym = acache->intern(sequence->getClockName());
  NUNet* guard = new NUBitNet(sym, NetFlags(eInputNet), root, loc);
  root->addPort(guard);

  
  // Now we have the statements we need to execute for a specific edge.
  // We can make a new always block out of them, although we will do it
  // in the master design.
  NUNetRefFactory* nrf = master_design->getNetRefFactory();
  NUBlock* block = new NUBlock(stmts, root, master_design->getIODB(),
                               nrf, false, loc);
  UtString str("block_");
  str << sequence->getName();
  NUAlwaysBlock* ab = new NUAlwaysBlock(acache->intern(str), block, nrf, loc);
  NUIdentRvalue* id = new NUIdentRvalue(guard, loc);
  NUEdgeExpr* ee = new NUEdgeExpr(eClockPosedge, id, loc);
  ab->addEdgeExpr(ee);

  str.clear();
  str << "cpv_" << sequence->getName();
  PVTransactionToMaster t2m(root, str.c_str());
  ab->replaceLeaves(t2m);
  root->addAlwaysBlock(ab);

/*
  for (PhaseLoop p(sequence->loopPhases()); !p.atEnd(); ++p) {
    PVPhase* phase = *p;
    phase->cleanup();
  }
*/
} // void PVCompileContext::copyStmtsToRoot


void PVCompileContext::collectSequences(TVec* tvec, PVTransaction::SVec *sv) {
  for (TransactionLoop t(*tvec); !t.atEnd(); ++t) {
    PVTransaction* transaction = *t;
    for (PVTransaction::SequenceLoop s(transaction->loopSequences());
         !s.atEnd(); ++s)
    {
      PVSequence* sequence = *s;
      sv->push_back(sequence);
    }
  }
}

void PVCompileContext::optimize() {
  //createClockMap();

  // mark all the live registers in each transactionID as observable outputs in the
  // other transactionIDs.  Otherwise they may not reach the design outputs.  TBD.

  // Create a guard in the master top-level module for each transaction ID.
  // In this block of code let's, for the moment, assume that there is just
  // one root module.  This could be generalized in some fashion if we ever
  // really support multiple root modules
  NUModuleList roots;
  NUDesign* master_design = getMasterDesign();
  master_design->getTopLevelModules(&roots);
  INFO_ASSERT(roots.size() == 1, "TBD: support multiple roots");
  NUModule* root = *roots.begin();

  // Collect all the seqences up front, as we will use them twice
  PVTransaction::SVec all_sequences;
  collectSequences(&mReadTransactions, &all_sequences);
  collectSequences(&mWriteTransactions, &all_sequences);

  bool ret = true;

  UtHashMap<PVSequence*,NUStmtList> seq_stmts;

  for (Loop<PVTransaction::SVec> s(all_sequences); !s.atEnd(); ++s) {
    PVSequence* sequence = *s;
    NUStmtList& stmts = seq_stmts[sequence];
    ret &= sequence->findMultiplexedIOs(false);
    for (PVSequence::PhaseLoop p(sequence->loopPhases()); !p.atEnd(); ++p) {
      PVPhase* phase = *p;
      ret &= optimizePhase(phase, &stmts);
    }
  }

  // Remove all the original design logic, though leave the instantation
  // hierarchy in place
  NUModuleList mods;
  master_design->getModulesBottomUp(&mods);
  for (Loop<NUModuleList> p(mods); !p.atEnd(); ++p) {
    NUModule* mod = *p;
    mod->removeBlocks();
  }

  for (Loop<PVTransaction::SVec> s(all_sequences); !s.atEnd(); ++s) {
    PVSequence* sequence = *s;
    NUStmtList& stmts = seq_stmts[sequence];
    copyStmtsToRoot(root, &stmts, sequence);
  }

  {
    ClearUDCallback clearUseDef(false); // do not recurse
    NUDesignWalker clearUDWalker(clearUseDef, false);
    clearUDWalker.module(root);
  }
  {
    NUNetRefFactory* nrf = master_design->getNetRefFactory();
    MsgContext* msg_context = master_design->getMsgContext();
    IODBNucleus* iodb = master_design->getIODB();
    ArgProc* args = master_design->getArgProc();

    UD ud(nrf, msg_context, iodb, args, false);
    ud.module(root, true);
  }
} // void PVContext::optimize
