# -*- Makefile -*-
# *****************************************************************************
#  Copyright (c) 2003-2014 by Carbon Design Systems, Inc., All Rights Reserved.
# THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
# INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
# COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
# EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
# *****************************************************************************

include $(CARBON_HOME)/makefiles/Makefile.common

SEARCHPATH= $(addprefix -L,$(CARBON_LIB_DIRS))
CARBON_LIBMODE_ON= -Wl,-Bstatic 
CARBON_LIBMODE_OFF= -Wl,-Bdynamic 
# Link Novas libs statically, just like carbon libs
NOVAS_LIBMODE_ON = $(CARBON_LIBMODE_ON)
NOVAS_LIBMODE_OFF = $(CARBON_LIBMODE_OFF)

ifneq ($(CARBON_BIN:product=),)	# If set to other than product
# If no CARBON_BIN given, the modshell libraries are in the carbon standard path
CARBON_MODSHELL_PATH = \
	$(CARBON_HOME)/obj/$(CARBON_TARGET)/modshell \
	$(CARBON_HOME)/obj/$(CARBON_TARGET)/waveform \
	$(CARBON_HOME)/obj/$(CARBON_TARGET)/wavetestbench
SEARCHPATH += $(addprefix -L,$(CARBON_MODSHELL_PATH))
endif


all: $(EXECNAME)

ifdef USER_CB_SRC
USER_CB_OBJ = $(basename $(USER_CB_SRC)).o

$(USER_CB_OBJ): $(USER_CB_SRC)
	$(CARBON_COMPILE) -o $@ $<
endif

ifdef FSDB_READ_ON

ifdef FSDB_WRITE_ON
ifneq ($(CARBON_GCC2),)
CWTB_LINK_OPTIONS = -Wl,-z,muldefs
endif
endif

CARBON_FSDBREAD_LIB = -lfsdbwaveform
ifneq ($(CARBON_TARGET_ARCH),Win)
CARBON_FSDBREAD_LIB += -lfsdbreaderconduit
endif

ifeq ($(CARBON_TARGET_ARCH),Linux64)
CARBON_FSDBREAD_LIB +=  -L$(CARBON_HOME)/Linux64/lib/$(CARBON_OS_RELEASE)/$(CARBON_COMPILER)
NOVAS_FSDBREAD_LIB += -lnffr -lnsys
endif
ifeq ($(CARBON_TARGET_ARCH),Linux)
CARBON_FSDBREAD_LIB += -L$(CARBON_HOME)/Linux/lib/$(CARBON_OS_RELEASE)/$(CARBON_COMPILER)
NOVAS_FSDBREAD_LIB += -lnffr -lnsys
endif
ifeq ($(CARBON_TARGET_ARCH),Win)
CARBON_FSDBREAD_LIB += -lfsdbreaderconduit_msvc
NOVAS_FSDBREAD_LIB += $(CARBON_HOME)/Win/lib/nffr.lib
endif

endif

CARBON_DESIGN_LIBRARY = -L. -l$(MODELNAME)
ifeq ($(CARBON_TARGET_ARCH),Win)
CWTB_HAS_DLL = $(wildcard lib$(MODELNAME).dll)
ifneq ($(CWTB_HAS_DLL),)
# Windows creates a memory leak in the carbon memory manager
# when linking with a carbon dll
CWTB_MAIN_CXXFLAGS = -DFORCE_NO_MEMLEAK_CHECK
else
# minggw does not like -ldesign with a libdesign.lib and a dynamic preference
# So, if there is a .lib just throw the file on the command line
CWTB_HAS_WINLIB = $(wildcard lib$(MODELNAME).lib)
ifneq ($(CWTB_HAS_WINLIB),)
CARBON_DESIGN_LIBRARY = ./lib$(MODELNAME).lib
else
CARBON_DESIGN_LIBRARY = -L. -l$(MODELNAME)
endif
endif
endif

MAIN_OBJ = $(notdir $(MAIN_SRC)).o
$(MAIN_OBJ) : $(MAIN_SRC)
	$(CARBON_COMPILE) $(CWTB_MAIN_CXXFLAGS) -o $@ $<

$(EXECNAME): clean $(MAIN_OBJ) MdsModelStub.o lib$(MODELNAME).h $(USER_CB_OBJ)
	$(CARBON_LINK)  \
		-o $@ $(MAIN_OBJ) MdsModelStub.o $(USER_CB_OBJ) \
		$(CARBON_DESIGN_LIBRARY)		\
		-Wl,-rpath,$(shell pwd)			\
		$(CARBON_LIBMODE_ON) \
			-l$(LIBNAME)			\
			-lmodshell     			\
		$(CARBON_LIBMODE_OFF) \
		-Wl,-Bstatic \
		$(CARBON_FSDBREAD_LIB) 			\
		-lwaveform 	\
		-Wl,-Bdynamic \
		$(NOVAS_LIBMODE_ON) \
		$(NOVAS_FSDBREAD_LIB) \
		$(CARBON_DEBUSSY_LIB) \
		$(NOVAS_LIBMODE_OFF) \
		$(SEARCHPATH)			\
		$(USER_LINK_OBJ)			\
		$(CWTB_LINK_OPTIONS)			\
		$(CARBON_LICENSE_LIB_LIST) -lpthread	\
		-lz $(CARBON_COMPILER_EXTRA) $(CARBON_PURESTUBS) $(CARBON_ARCH_LIBS)

MdsModelStub.o: MdsModelStub.cxx
	$(CARBON_COMPILE) -o $@ MdsModelStub.cxx

clean:
	$(RM) MdsModelStub.o $(MAIN_OBJ) $(USER_CB_OBJ)

.PHONY: all
