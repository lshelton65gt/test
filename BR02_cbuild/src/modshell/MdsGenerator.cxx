// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/UtIOStream.h"
#include "modshell/MdsGenerator.h"
#include "util/ShellMsgContext.h"
#include "util/OSWrapper.h"
#if pfUNIX
#include <sys/errno.h>
#endif

MdsGenerator::MdsGenerator(const UtString &model_name, const UtString &install_dir, const UtString &target_dir, MsgContext *message_context, const char* presched, const char* postsched, const char* userInit, const char* userDestroy, bool allowFsdbWrite, bool allowFsdbRead)
  : mModelName(model_name), mInstallDir(install_dir), mTargetDir(target_dir), mMsgContext(message_context), mPreSched(presched), mPostSched(postsched), mUserInit(userInit), mUserDestroy(userDestroy), mAllowFsdbWrite(allowFsdbWrite), mAllowFsdbRead(allowFsdbRead)
{
}


MdsGenerator::~MdsGenerator(void)
{
}


static void sPutSchedSig(UtString& line, const char* fnName)
{
  line << "extern void " << fnName << "(CarbonObjectID*, void*, CarbonTime);";
}

static void sPutSchedFn(UtString& line, const char* fnName)
{
  if (fnName)
    line << fnName;
  else
    line << "NULL";
}

static void sPutInitSig(UtString& line, const char* fnName)
{
  line << "extern void " << fnName << "(CarbonObjectID*, void**);";
}

static void sPutInitFn(UtString& line, const char* fnName)
{
  if (fnName)
    line << fnName;
  else
    line << "NULL";
}

static void sPutDestroySig(UtString& line, const char* fnName)
{
  line << "extern void " << fnName << "(CarbonObjectID*, void*);";
}

static void sPutDestroyFn(UtString& line, const char* fnName)
{
  if (fnName)
    line << fnName;
  else
    line << "NULL";
}

void MdsGenerator::generate(void)
{
  UtString line;
  UtString model_tag("<MODEL>");

  UtString insert_userinit_sig("<INSERT USER INIT SIG HERE>");
  UtString insert_userinit_fn("<INSERT USER INIT FN HERE>");

  UtString insert_userdestroy_sig("<INSERT USER DESTROY SIG HERE>");
  UtString insert_userdestroy_fn("<INSERT USER DESTROY FN HERE>");

  UtString insert_presched_sig("<INSERT PRE SCHEDULE SIG HERE>");
  UtString insert_presched_fn("<INSERT PRE SCHEDULE FN HERE>");

  UtString insert_postsched_sig("<INSERT POST SCHEDULE SIG HERE>");
  UtString insert_postsched_fn("<INSERT POST SCHEDULE FN HERE>");

  UtString iodb_name("<INSERT IODB NAME HERE>");

  UtString insert_fsdbinit("<INSERT FSDB INIT HERE>");
  UtString insert_fsdballow("<INSERT ALLOW FSDB BOOL HERE>");

  UtString insert_fsdbread_sig("<INSERT FSDB READ SIG HERE>");
  UtString insert_fsdbread_fn("<INSERT FSDB READ CREATE HERE>");
  
  UtString buf;
  buf << mInstallDir << "/include/carbon/MdsModelStub.cxx";
  UtString reason;
  FILE* in = OSFOpen(buf.c_str(), "rb", &reason);
  if (in == NULL)
  {
    // This error is fatal.
    mMsgContext->CouldNotOpenModelStubForInput(reason.c_str());
    return;
  }

  const char * stub_output_filename = "MdsModelStub.cxx";
  UtOBStream out(stub_output_filename);
  if (out.bad())
  {
    // This error is fatal.
    mMsgContext->CouldNotOpenModelStubForOutput(stub_output_filename);
  }

  NodeSet processed_nodes;
  UtString::size_type strPos;
  while(fgetline(in, &line))
  {
    UtString buf;
    if (line.find(model_tag) != UtString::npos)
    {
      UtString::size_type i = line.find(model_tag);
      line.replace(i, model_tag.length(), mModelName);
      out << line;
    }
    else if ((strPos = line.find(insert_presched_sig)) != UtString::npos)
    {
      if (mPreSched)
      {
        sPutSchedSig(buf, mPreSched);
        line.replace(strPos, insert_presched_sig.length(), buf);
        out << line;
      }
    }
    else if ((strPos = line.find(insert_postsched_sig)) != UtString::npos)
    {
      if (mPostSched)
      {
        sPutSchedSig(buf, mPostSched);
        line.replace(strPos, insert_postsched_sig.length(), buf);
        out << line;
      }
    }
    else if ((strPos = line.find(insert_presched_fn)) != UtString::npos)
    {
      sPutSchedFn(buf, mPreSched);
      line.replace(strPos, insert_presched_fn.length(), buf);
      out << line;
    }
    else if ((strPos = line.find(insert_postsched_fn)) != UtString::npos)
    {
      sPutSchedFn(buf, mPostSched);
      line.replace(strPos, insert_postsched_fn.length(), buf);
      out << line;
    }
    else if ((strPos = line.find(insert_userinit_sig)) != UtString::npos)
    {
      if (mUserInit)
      {
        sPutInitSig(buf, mUserInit);
        line.replace(strPos, insert_userinit_sig.length(), buf);
        out << line;
      }
    }
    else if ((strPos = line.find(insert_userinit_fn)) != UtString::npos)
    {
      sPutInitFn(buf, mUserInit);
      line.replace(strPos, insert_userinit_fn.length(), buf);
      out << line;
    }
    else if ((strPos = line.find(insert_userdestroy_sig)) != UtString::npos)
    {
      if (mUserDestroy)
      {
        sPutDestroySig(buf, mUserDestroy);
        line.replace(strPos, insert_userdestroy_sig.length(), buf);
        out << line;
      }
    }
    else if ((strPos = line.find(insert_userdestroy_fn)) != UtString::npos)
    {
      sPutDestroyFn(buf, mUserDestroy);
      line.replace(strPos, insert_userdestroy_fn.length(), buf);
      out << line;
    }
    else if ((strPos = line.find(iodb_name)) != UtString::npos)
    {
      UtString iodbName;
      iodbName << "lib" << mModelName << IODB::scIODBExt;
      line.replace(strPos, iodb_name.length(), iodbName);
      out << line;
    }
    else if ((strPos = line.find(insert_fsdbinit)) != UtString::npos)
    {
      if (mAllowFsdbWrite)
        buf << "carbonWaveInitFSDB";
      else
        buf << "NULL;";
      line.replace(strPos, insert_fsdbinit.length(), buf);
      out << line;
    }
    else if ((strPos = line.find(insert_fsdballow)) != UtString::npos)
    {
      if (mAllowFsdbWrite)
        buf << "true";
      else
        buf << "false";
      line.replace(strPos, insert_fsdballow.length(), buf);
      out << line;
    }
    else if ((strPos = line.find(insert_fsdbread_sig)) != UtString::npos)
    {
      if (mAllowFsdbRead)
        buf << "extern WfFsdbFileReader* WfFsdbFileReaderCreate(const char* fileName, AtomicCache* strCache, DynBitVectorFactory* dynFactory, UtUInt64Factory* timeFactory, WfAbsControl* control)";
      
      line.replace(strPos, insert_fsdbread_sig.length(), buf);
      out << line;
    }
    else if ((strPos = line.find(insert_fsdbread_fn)) != UtString::npos)
    {
      if (mAllowFsdbRead)
        buf << "WfFsdbFileReaderCreate";
      else
        buf << "NULL";
      line.replace(strPos, insert_fsdbread_fn.length(), buf);
      out << line;
    }
    else
    {
      out << line;
    }
  }
  
  fclose(in);
  out.close();
} // void MdsGenerator::generate
