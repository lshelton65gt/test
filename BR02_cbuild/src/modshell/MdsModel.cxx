// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "shell/CarbonModel.h"
#include "shell/CarbonHookup.h"

#include "util/UtIOStream.h"
#include "modshell/MdsModel.h"

#include "util/UtString.h"
#include "shell/carbon_capi.h"
#include "shell/carbon_misc.h"
#include "symtab/STSymbolTable.h"
#include "util/AtomicCache.h"
#include "util/ShellMsgContext.h"
#include "iodb/ScheduleFactory.h"
#include "shell/CarbonDBRead.h"
#include "iodb/IODBRuntime.h"

#include "hdl/HdlId.h"
#include "hdl/HdlVerilogPath.h"
#include "exprsynth/ExprFactory.h"
#include "util/UtHashMap.h"

static void sDefaultFn(CarbonObjectID*, void*, CarbonTime)
{
}

MdsModel::MdsModel(const UtString &iodb_filename)
  : mShellBOM(NULL), mIODB(NULL), mUserData(NULL), 
    mIODBFilename(iodb_filename), mExprFactory(NULL),
    mContext(NULL), mWaveContext(NULL),
    mAtomicCache(NULL), mErrorStream(NULL), mMsgContext(NULL),
    mScheduleFactory(NULL), mHdlPath(NULL), mSymbolTable(NULL),
    mUserInitFn(NULL), mDestroyFn(NULL), mPreSchedFn(sDefaultFn),
    mPostSchedFn(sDefaultFn), mFSDBInitFn(NULL), mFSDBReadCreateFn(NULL),
    mNodeToSignalMap(NULL), mAllowFsdbWrite(false)
{
}

MdsModel::~MdsModel(void)
{
  if (mContext != NULL) {
    carbonDestroy(&mContext);
  }
  if (mNodeToSignalMap != NULL) {
    delete mNodeToSignalMap;
  }
  if (mIODB != NULL) {
    destroyIODB();
  }
}


void MdsModel::createIODB()
{
  mNodeToSignalMap = new NodeToSignalMap;
  mAtomicCache = new AtomicCache;
  mErrorStream = new MsgStreamIO(stderr, true);
  mMsgContext = new MsgContext;
  mScheduleFactory = new SCHScheduleFactory;
  mMsgContext->addReportStream(mErrorStream);
  mShellBOM = new ShellSymTabBOM;
  mShellBOM->putScheduleFactory(mScheduleFactory);
  mSymbolTable = new STSymbolTable(mShellBOM, mAtomicCache);
  mExprFactory = new ESFactory;
  // No source locator factory is needed.
  mIODB = new IODBRuntime(mAtomicCache, mSymbolTable, mMsgContext,
                          mScheduleFactory, mExprFactory, NULL);
  INFO_ASSERT( mIODB->readDB(mIODBFilename.c_str(), eCarbonIODB),
               "Problem reading IODB file.");
}


void MdsModel::destroyIODB(void)
{
  delete mIODB;
  mIODB = NULL;
  delete mExprFactory;
  delete mSymbolTable;
  delete mShellBOM;
  delete mAtomicCache;
  delete mErrorStream;
  delete mMsgContext;
  delete mScheduleFactory;
}

CarbonNet * MdsModel::findNet(const STSymbolTableNode * node)
{
  CarbonNet * signal = (*mNodeToSignalMap)[node];

  if (signal == 0)
  {
    UtString signal_name;
    node->compose(&signal_name, true); // include root in name 

    signal = findNet(signal_name);
    (*mNodeToSignalMap)[node] = signal;
  }

  return signal;
}

bool MdsModel::isInput(STSymbolTableNode* node)
{
  STSymbolTableNode* i;
  IODB::NameSetLoop loop = mIODB->loopInputs();

  while (loop(&i))
  {
    if (i == node)
    {
      return true;
    }
  }

  return false;
}


bool MdsModel::isClock(STSymbolTableNode* node)
{
  STSymbolTableNode* i;
  IODB::NameSetLoop loop = mIODB->loopClocks();

  while (loop(&i))
  {
    if (i == node)
    {
      return true;
    }
  }

  for (IODB::NameSetMapLoop p = mIODB->loopCollapseClocks(); not p.atEnd(); ++p)
  {
    i = p.getKey();
    if (i == node)
    {
      return true;
    }
    IODB::NameSet* ns = p.getValue();
    for (IODB::NameSet::SortedLoop q = ns->loopSorted(); not q.atEnd(); ++q)
    {
      if (*q == node)
      {
	return true;
      }
    }
  }

  return false;
}


bool MdsModel::isSyncData(STSymbolTableNode* node)
{
  return isInput(node) and (not isClock(node)) and (not isReset(node));
}


bool MdsModel::isAsyncData(STSymbolTableNode* node)
{
  STSymbolTableNode* i;
  IODB::NameSetLoop loop = mIODB->loopAsyncs();

  while (loop(&i))
  {
    if (i == node)
    {
      return true;
    }
  }

  return false;
}


bool MdsModel::isReset(STSymbolTableNode* node)
{
  return isPosedgeReset(node) or isNegedgeReset(node);
}


bool MdsModel::isPosedgeReset(STSymbolTableNode* node)
{
  STSymbolTableNode* i;
  IODB::NameSetLoop loop = mIODB->loopAsyncPosResets();

  while (loop(&i))
  {
    if (i == node)
    {
      return true;
    }
  }

  return false;
}


bool MdsModel::isNegedgeReset(STSymbolTableNode* node)
{
  STSymbolTableNode* i;
  IODB::NameSetLoop loop = mIODB->loopAsyncNegResets();

  while (loop(&i))
  {
    if (i == node)
    {
      return true;
    }
  }

  return false;
}


STSymbolTableNode * MdsModel::lookupSignal(const UtString &signal) const
{
  HdlHierPath::Status status;
  HdlId info;
  return mIODB->getIOSymbolTable()->getNode(signal.c_str(), &status, &info);
}


void MdsModel::printSignalType(STSymbolTableNode *node) const
{
  UtString node_name;
  // Get the signature.
  node->compose(&node_name, true);  // include root in name
  const ShellDataBOM* dbom = ShellSymTabBOM::getLeafBOM(node);
  SInt32 type_id = dbom->getTypeIndex();
  UtIO::cerr() << "The type of " << node_name << " is :" << type_id << UtIO::endl;
}


/*
**
** Not sure if these really belong here, but just do it like
** this for the moment.
**
** These all need proper error handling.
**
*/

bool MdsModel::isDumpingWave() const {
  return (mWaveContext != NULL);
}

CarbonStatus MdsModel::dumpVars(UInt32 depth, const UtString &scopes)
{
  return carbonDumpVars(mWaveContext, depth, scopes.c_str());
}

CarbonStatus MdsModel::dumpStateIO(UInt32 depth, const UtString &scopes)
{
  return carbonDumpStateIO(mWaveContext, depth, scopes.c_str());
}

CarbonStatus  MdsModel::dumpAll(void)
{
  return carbonDumpAll(mWaveContext);
}


CarbonStatus MdsModel::dumpOn(void)
{
  return carbonDumpOn(mWaveContext);
}


CarbonStatus MdsModel::dumpOff(void)
{
  return carbonDumpOff(mWaveContext);
}


CarbonStatus MdsModel::dumpFlush(void)
{
  return carbonDumpFlush(mWaveContext);
}

CarbonTimescale MdsModel::convertWfTimescaleToCarbonTimescale(WfVCD::TimescaleUnit t_unit, WfVCD::TimescaleValue t_value)
{
  CarbonTimescale t=CarbonTimescale (0);

  INFO_ASSERT( t_unit != WfVCD::eWfVCDTimescaleUnitINVALID,
               "Problem converting waveform timescale." );
  INFO_ASSERT( t_value != WfVCD::eWfVCDTimescaleValueINVALID,
               "Problem converting waveform timescale." );


  switch (t_unit)
  {
  case WfVCD::eWfVCDTimescaleUnitFS:
    switch (t_value)
    {
    case WfVCD::eWfVCDTimescaleValue1:    t = e1fs;   break; 
    case WfVCD::eWfVCDTimescaleValue10:   t = e10fs;  break;
    case WfVCD::eWfVCDTimescaleValue100:  t = e100fs; break;
    case WfVCD::eWfVCDTimescaleValueINVALID: break;
    }
    break;
  case WfVCD::eWfVCDTimescaleUnitPS:
    switch (t_value)
    {
    case WfVCD::eWfVCDTimescaleValue1:    t = e1ps;   break; 
    case WfVCD::eWfVCDTimescaleValue10:   t = e10ps;  break;
    case WfVCD::eWfVCDTimescaleValue100:  t = e100ps; break;
    case WfVCD::eWfVCDTimescaleValueINVALID: break;
    }
    break;
  case WfVCD::eWfVCDTimescaleUnitNS:
    switch (t_value)
    {
    case WfVCD::eWfVCDTimescaleValue1:    t = e1ns;   break; 
    case WfVCD::eWfVCDTimescaleValue10:   t = e10ns;  break;
    case WfVCD::eWfVCDTimescaleValue100:  t = e100ns; break;
    case WfVCD::eWfVCDTimescaleValueINVALID: break;
    }
    break;
  case WfVCD::eWfVCDTimescaleUnitUS:
    switch (t_value)
    {
    case WfVCD::eWfVCDTimescaleValue1:    t = e1us;   break; 
    case WfVCD::eWfVCDTimescaleValue10:   t = e10us;  break;
    case WfVCD::eWfVCDTimescaleValue100:  t = e100us; break;
    case WfVCD::eWfVCDTimescaleValueINVALID: break;
    }
    break;
  case WfVCD::eWfVCDTimescaleUnitMS:
    switch (t_value)
    {
    case WfVCD::eWfVCDTimescaleValue1:    t = e1ms;   break; 
    case WfVCD::eWfVCDTimescaleValue10:   t = e10ms;  break;
    case WfVCD::eWfVCDTimescaleValue100:  t = e100ms; break;
    case WfVCD::eWfVCDTimescaleValueINVALID: break;
    }
    break;
  case WfVCD::eWfVCDTimescaleUnitS:
    switch (t_value)
    {
    case WfVCD::eWfVCDTimescaleValue1:    t = e1s;   break; 
    case WfVCD::eWfVCDTimescaleValue10:   t = e10s;  break;
    case WfVCD::eWfVCDTimescaleValue100:  t = e100s; break;
    case WfVCD::eWfVCDTimescaleValueINVALID: break;
    }
    break;
  case WfVCD::eWfVCDTimescaleUnitINVALID:
    break;
  }

  return t;
}

UInt64 MdsModel::getTime(void)
{
  return mContext->getModel ()->getSimulationTime();
}

const CarbonObjectID* MdsModel::getModelContext() const
{
  return mContext;
}


CarbonNet * MdsModel::findNet(const UtString & net_name)
{
  return mContext->getModel ()->findNet(net_name.c_str());
}

bool MdsModel::allowFsdbWrite() const
{
  return mAllowFsdbWrite;
}

void MdsModel::maybeCallUserInit()
{
  if (mUserInitFn)
    (*mUserInitFn)(mContext, &mUserData);
}

void MdsModel::callUserDestroy()
{
  if (mDestroyFn)
    (*mDestroyFn)(mContext, mUserData);
  mUserData = NULL;
}

void MdsModel::preSchedule(CarbonTime sim_time)
{
  (*mPreSchedFn)(mContext, mUserData, sim_time);
}

CarbonStatus MdsModel::schedule(CarbonTime sim_time)
{
  (*mPreSchedFn)(mContext, mUserData, sim_time);
  CarbonStatus ret = carbonSchedule(mContext, sim_time);
  (*mPostSchedFn)(mContext, mUserData, sim_time);
  return ret;
}

CarbonStatus MdsModel::clkSchedule(CarbonTime sim_time)
{
  // Call the preschedule function at this point since we know this is
  // called first
  (*mPreSchedFn)(mContext, mUserData, sim_time);

  // call clk schedule
  CarbonStatus ret = carbonClkSchedule(mContext, sim_time);
  return ret;
}

CarbonStatus MdsModel::dataSchedule(CarbonTime sim_time)
{
  CarbonStatus ret = carbonDataSchedule(mContext, sim_time);

  // The data schedule is always called in a timeslice if either clk
  // or data changes, so we can call the post sched function here.
  (*mPostSchedFn)(mContext, mUserData, sim_time);
  return ret;
}

CarbonStatus MdsModel::dumpFile(const UtString &filename, WfVCD::TimescaleUnit t_unit, WfVCD::TimescaleValue t_value, CarbonWaveFileType waveType)
{
  CarbonTimescale t = convertWfTimescaleToCarbonTimescale(t_unit, t_value);
  switch(waveType)
  {
  case eWaveFileTypeVCD:
    mWaveContext = carbonWaveInitVCD(mContext, filename.c_str(), t);
    break;
  case eWaveFileTypeFSDB:
    if (mFSDBInitFn)
      mWaveContext = (*mFSDBInitFn)(mContext, filename.c_str(), t);
    else
      mWaveContext = NULL;
    break;
  }
  
  return (mWaveContext != NULL) ? eCarbon_OK : eCarbon_ERROR;
}

const char* MdsModel::getIODBFilename() const
{
  return mIODBFilename.c_str();
}

void MdsModel::putIODBFilename(const char* name)
{
  mIODBFilename = name;
}

void MdsModel::putFnInfo(CarbonObjectID* context,
                         UserInitFn initFn,
                         DestroyFn destroyFn,
                         ScheduleFn preSchedFn,
                         ScheduleFn postSchedFn,
                         FSDBWaveInitFn dumpFileFn, 
                         FSDBReadCreateFn fsdbReadCreateFn,
                         bool allowFsdbWrite)
{
  mContext = context;
  mUserInitFn = initFn;
  mDestroyFn = destroyFn;
  if (preSchedFn)
    mPreSchedFn = preSchedFn;
  if (postSchedFn)
    mPostSchedFn = postSchedFn;
  mFSDBInitFn = dumpFileFn;
  mFSDBReadCreateFn = fsdbReadCreateFn;
  mAllowFsdbWrite = allowFsdbWrite;
}

MdsModel::FSDBReadCreateFn 
MdsModel::getFsdbReadCreateFn()
{
  return mFSDBReadCreateFn;
}

bool MdsModel::hasValidCarbonNet(const STAliasedLeafNode* node) const
{
  CarbonHookup* hookup = mContext->getModel ()->getHookup();
  return hookup->hasValidCarbonNet(node);
}
