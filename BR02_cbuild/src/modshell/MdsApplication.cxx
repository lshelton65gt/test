// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/UtIOStream.h"
#if pfUNIX
#include <unistd.h>
#include <sys/wait.h>
#endif

#include "modshell/MdsApplication.h"
#include "symtab/STSymbolTable.h"
#include "util/AtomicCache.h"
#include "util/SourceLocator.h"
#include "util/ShellMsgContext.h"
#include "util/UtString.h"
#include "util/UtConv.h"
#include "util/OSWrapper.h"
#include "util/UtLicense.h"
#include "util/UtLicenseMsg.h"

#include "iodb/ScheduleFactory.h"
#include "shell/CarbonDBRead.h"


static const char* scUserDestroyFnName = "userDestroyFn";
static const char* scUserInitFnName = "userInitFn";
static const char* scUserPreSchedName = "userPreScheduleFn";
static const char* scUserPostSchedName = "userPostScheduleFn";

const char* MdsApplication::scInputControl = "Input Control";
const char* MdsApplication::scFlowControl = "Flow Control";
const char* MdsApplication::scOutputControl = "Output Control";

class MdsApplication::LicCB : public UtLicense::MsgCB
{
public:
  CARBONMEM_OVERRIDES
  
  LicCB(MsgContext* msgContext) : mMsgContext(msgContext)
  {}

  virtual ~LicCB()
  {}

  virtual void waitingForLicense(const char*)
  {}

  virtual void queuedLicenseObtained(const char*)
  {}

  virtual void requeueLicense(const char*)
  {}

  virtual void relinquishLicense(const char*)
  {}

  virtual void exitNow(const char* reason)
  {
    mMsgContext->SHLLicenseServerFail(reason);
  }

private:
  MsgContext* mMsgContext;

  CARBON_FORBID_DEFAULT_CTORS(LicCB);
};

MdsApplication::MdsApplication(int *argc, char** argv)
  : UtApplication(argc, argv), mShellBOM(NULL), mIODB(NULL), 
    mAtomicCache(NULL), mScheduleFactory(NULL),
    mSymbolTable(NULL)
{
  mLicCB = new LicCB(mMsgContext);
  mLicense = new UtLicense(mLicCB);
}


MdsApplication::~MdsApplication(void)
{
  destroyIODB();
  mLicense->release(UtLicense::eCwaveTestbench);
  delete mLicense;
  delete mLicCB;
}


void MdsApplication::setupCommandLineOptions(void)
{
  mArgs.createSection(MdsApplication::scInputControl);

  mArgs.addString("-model", "Carbon model", "design", false, false, 1);
  mArgs.addToSection(MdsApplication::scInputControl, "-model");

  mArgs.addString("-iodbfile", "Port information", "", false, false, 1);
  mArgs.addToSection(MdsApplication::scInputControl, "-iodbfile");

  mArgs.createSection(MdsApplication::scFlowControl);
  mArgs.addString("-usersrc", "Specify the name of the file that contains all 4 required callback functions and link into the application. The four functions are, void userInitFn(CarbonMOdel*, void**), void userDestroyFn(CarbonObjectID*, void*), void userPreScheduleFn(CarbonObjectID*, void*, CarbonTime), and void userPostScheduleFn(CarbonObjectID*, void*, CarbonTime). The init function will be called once at the beginning of time. The destroy function will be called once at the end of the simulation. The pre and post schedule functions will be called before and after each schedule call, respectively. No function is called by default. The functions should be declared as 'extern'. Note that the init function allows the user to setup his environment and pass user data to the application which in turn will be passed into the schedule functions. An example initialization function:<verbatim>"
                "\n             extern void userInitFn(CarbonObjectID* model, void** data) {\n"
                "                  MyObj* obj = new MyObj(model);\n"
                "                  *data = obj;}\n </verbatim>", NULL, true, false, 1);
  mArgs.addToSection(MdsApplication::scFlowControl, "-usersrc");
  mArgs.addString("-userlib", "library (with path, if applicable) that contains the pre/post schedule functions. This can be in the form of a simple object file or files such as 'user.o' or as a library such as -L<path to library> -l<library name>. Shared and static libraries are also allowed. These linker options/objects are simply passed directly to the application linker. The default assumes libuser in the current directory. The default 'libuser' can have either a .a or a .so extension. WARNING: the user libary must be compiled with a compatible c++ compiler.", NULL, true, false, 1);
  mArgs.addToSection(MdsApplication::scFlowControl, "-userlib");
  mArgs.addString("-userlink", "Extra text to add to the link command.", NULL, true, false, 1);
  mArgs.addToSection(MdsApplication::scFlowControl, "-userlink");
  
  mArgs.addBool("-allowFsdbRead", "If specified, this will allow the usage of -fsdb in the cwavetestbench executable, which will turn on fsdb reading. Using this causes the novas/Verti reader to be linked in and will create extra overhead, so it may have some minor negative performance impact.", false, 1);
  mArgs.addToSection(MdsApplication::scFlowControl, "-allowFsdbRead");

  mArgs.createSection(MdsApplication::scOutputControl);
  mArgs.addBool("-h", "Print help information", false, 1);
  mArgs.addToSection(MdsApplication::scOutputControl, "-h");
  mArgs.addString("-O", "Control optimization (-O0 implies none). Can be a number 0-4 or s. The last instance of this option on the command line is the one used.", "3", false, true, 1);
  mArgs.addToSection(MdsApplication::scOutputControl, "-O");

  mArgs.addString("-Wc", "Pass option flags to the compiler.", "", false, false, 1);
  mArgs.addToSection(MdsApplication::scOutputControl, "-Wc");

}


bool MdsApplication::parseCommandLine(void)
{
  // checkout a cwave license
  {
    UtString licReason;
    if (! mLicense->checkout(UtLicense::eCwaveTestbench, &licReason))
      mMsgContext->MdsLicenseFailure(licReason.c_str());
  }


  bool result;
  int num_options;
  UtString error_msg;

  if ((mArgs.parseCommandLine(mArgc, mArgv, &num_options, &error_msg) == ArgProc::eParseError))
  { 
    UtIO::cerr() << error_msg;
    result = false;
  }
  else
  {
    result = true;
  }

  return result;
}


bool MdsApplication::checkCommandLineOptions(void)
{
  const char* model;

  if (mArgs.getBoolValue("-h"))
  {
    printUsageMessage();
    exit(0);
  }
  
  if ((mArgs.getStrValue("-model", &model) != ArgProc::eKnown) && (*model != '\0'))
  {
    printUsageMessage();
    return false;
  }
  else
  {
    mModelName = UtString(model);
  }

  if (*mArgc != 1)
  {
    UtString buf;
    buf << "Unrecognized option";
    if (*mArgc > 2)
      buf << "s";
    buf << ": ";
    for (SInt32 i = 1; i < *mArgc; ++i)
      buf << mArgv[i] << " ";
    mMsgContext->MdsGarbageOnCmdLine(buf.c_str());
  }
  return true;
}


void MdsApplication::printUsageMessage(void)
{
  UtString usage;
  mArgs.getUsageVerbose(&usage);
  UtIO::cout() << usage << UtIO::endl;
}


void MdsApplication::linkExecutable(const char* exec_name, 
                                    const char* lib_name,
                                    TargetCompiler compiler,
                                    TargetCompilerVersion compiler_version,
                                    const char* main_src, 
                                    bool allowFsdbWrite,
                                    bool allowFsdbRead)
{
  UtString compile_command;
  UtString cxx_extra_flags;

  /*
  ** Figure out optimization.
  */
  const char* optLevel = mArgs.getStrLast("-O");  
  SInt32 optLevelInt;
  char* endPtr;
  bool isGoodOpt = ((*optLevel == 's') && (strlen(optLevel) == 1)) || 
    (UtConv::StrToSignedDec(optLevel, &endPtr, &optLevelInt, NULL) &&
     (optLevelInt <= 4) && (optLevelInt >= 0));
  
  if (! isGoodOpt)
    // fatal
    mMsgContext->InvalidOptimizationLevel(optLevel);

  // Get the target compiler
  const char* compilerStr = "gcc3";
  switch(compiler)
  {
  case eGCC4: compilerStr = "gcc4"; break;
  case eGCC3: compilerStr = "gcc3"; break;
  case eICC: compilerStr = "icc"; break;
  case eSPW: compilerStr = "spw"; break;
  case eWINX: compilerStr = "winx"; break;
  case eWIN: compilerStr = "winx"; break; // "win" when we build native runtime
  case eNoCC: INFO_ASSERT(compiler != eNoCC, "compiler not recognized"); break;
  }

  const char *compilerVersionString = "";
  switch (compiler_version) {
  case eGCC_343: compilerVersionString = "CARBON_GCC3_VERSION=343"; break;
  case eGCC_345: compilerVersionString = "CARBON_GCC3_VERSION=345"; break;
  case eGCC_346: compilerVersionString = "CARBON_GCC3_VERSION=346"; break;
  case eGCC_422: compilerVersionString = "CARBON_GCC4_VERSION=422"; break;
  case eGCC_424: compilerVersionString = "CARBON_GCC4_VERSION=424"; break;
  case eGCC_432: compilerVersionString = "CARBON_GCC4_VERSION=432"; break;
  case eVanilla_Compiler:
    // do not add any compiler version string to the command
    break;
  }
  cxx_extra_flags += "-O";
  cxx_extra_flags += optLevel;

  const char* wcflags;
  mArgs.getStrValue("-Wc", &wcflags);
  INFO_ASSERT(wcflags, "can't find -Wc arg");

  UtString cbuild_wc_flags (mIODB->getTargetFlags ());
  cbuild_wc_flags << " " << wcflags;

  compile_command << "${CARBON_HOME}/bin/make -f " 
                  << mInstallDir << "/makefiles/Modshell.mc "
                  << "EXECNAME=" << exec_name << " "
                  << "LIBNAME=" << lib_name << " "
                  << "MODELNAME=" << mModelName << " "
                  << "COMPILER=" << compilerStr << " "
                  << compilerVersionString << " "
                  << "CXX_WC_FLAGS=\"" << cbuild_wc_flags << "\" "
                  << "CXX_EXTRA_FLAGS=" << cxx_extra_flags << " "
                  << "MAIN_SRC=" << main_src;
  
  if (allowFsdbWrite) {
    compile_command << " " << "FSDB_WRITE_ON=1";
  }
  
  UtString user_link_options;
  if (mArgs.isParsed("-userlib") == ArgProc::eParsed) {
    const char* usrObj;
    mArgs.getStrValue("-userlib", &usrObj);
    INFO_ASSERT(usrObj, "can't find -userlib arg");
    user_link_options = usrObj;
  }
  if (mArgs.isParsed("-userlink") == ArgProc::eParsed) {
    const char* usrObj;
    mArgs.getStrValue("-userlink", &usrObj);
    INFO_ASSERT(usrObj, "can't find -userlink arg");
    user_link_options << " " << usrObj;
  }
      
  if (!user_link_options.empty()) {
    compile_command << " USER_LINK_OBJ=\"" << user_link_options << "\"";
  }

  if (mArgs.isParsed("-usersrc") == ArgProc::eParsed)
  {
    const char* usrsrc;
    mArgs.getStrValue("-usersrc", &usrsrc);
    INFO_ASSERT(usrsrc, "can't find -usersrc arg");
    compile_command << " USER_CB_SRC=" << usrsrc;
  }

  if (allowFsdbRead) {
    compile_command << " FSDB_READ_ON=1";
  }

  UtIO::cout() << compile_command << UtIO::endl;

  UtString sysMsg;
  int status = OSSystem(compile_command.c_str(), &sysMsg);
  if (status != 0)
    mMsgContext->CouldNotBuildExecutable(exec_name);
}


/*
** Switches need to be processed before calling
** this routine.
*/
void MdsApplication::createIODB(void)
{
  mAtomicCache = new AtomicCache;
  mScheduleFactory = new SCHScheduleFactory;
  mShellBOM = new ShellSymTabBOM;
  mShellBOM->putScheduleFactory(mScheduleFactory);
  mSymbolTable = new STSymbolTable(mShellBOM, mAtomicCache);
  // No source locator factory is needed.
  mIODB = new IODBRuntime(mAtomicCache, mSymbolTable, mMsgContext, mScheduleFactory, &mExprFactory, NULL);

  UtString iodb_filename;
  getIODBFilename(&iodb_filename);

  if (not mIODB->readDB(iodb_filename.c_str(), eCarbonIODB))
  {
    mMsgContext->CouldNotOpenIODBFile(iodb_filename.c_str());
  }
}


void MdsApplication::destroyIODB(void)
{
  delete mIODB;
  delete mSymbolTable;
  delete mShellBOM;
  delete mAtomicCache;
  delete mScheduleFactory;
}


bool MdsApplication::getIODBFilename(UtString *iodb_filename)
{
  const char* b;

  mArgs.getStrValue("-iodbfile", &b);
  iodb_filename->clear();
  if (*b == '\0')
  {
    mArgs.getStrValue("-model", &b);
    (*iodb_filename) << "lib" << b << IODB::scIODBExt;
  }
  else
    (*iodb_filename) << b;

  return ! iodb_filename->empty();
}

bool MdsApplication::getPreSchedFnName(UtString *fnName)
{
  if (callbackEnabled())
    fnName->assign(scUserPreSchedName);
  return ! fnName->empty();
}

bool MdsApplication::getPostSchedFnName(UtString *fnName)
{
  if (callbackEnabled())
    fnName->assign(scUserPostSchedName);
  
  return ! fnName->empty();
}

bool MdsApplication::getUserInitFnName(UtString *fnName)
{
  if (callbackEnabled())
    fnName->assign(scUserInitFnName);

  return ! fnName->empty();
}

bool MdsApplication::getUserDestroyFnName(UtString *fnName)
{
  if (callbackEnabled())
    fnName->assign(scUserDestroyFnName);
  
  return ! fnName->empty();
}

bool MdsApplication::callbackEnabled() const
{
  return ((mArgs.isParsed("-userlib") == ArgProc::eParsed) ||
          (mArgs.isParsed("-usersrc") == ArgProc::eParsed));
}

