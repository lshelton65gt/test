// -*- C++ -*-
/*****************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#include "lib<MODEL>.h"
#include "modshell/MdsModel.h"

<INSERT PRE SCHEDULE SIG HERE>
<INSERT POST SCHEDULE SIG HERE>
<INSERT USER INIT SIG HERE>
<INSERT USER DESTROY SIG HERE>
<INSERT FSDB READ SIG HERE>;

extern "C" void carbonMdsModelInit(CarbonInitFlags modelFlags, 
                                   CarbonDBType dbType, MdsModel* model)
{
  CarbonObjectID* context = carbon_<MODEL>_create (dbType, modelFlags);
  if (strcmp(model->getIODBFilename(), "") == 0)
  {
    const char* iodb_name = "<INSERT IODB NAME HERE>"; 
    model->putIODBFilename(iodb_name);
  }
  MdsModel::UserInitFn initFn = <INSERT USER INIT FN HERE>;
  bool allowFsdbWrite = <INSERT ALLOW FSDB BOOL HERE>;
  MdsModel::DestroyFn destroyFn = <INSERT USER DESTROY FN HERE>;
  
  MdsModel::ScheduleFn preSchedFn = <INSERT PRE SCHEDULE FN HERE>;
  MdsModel::ScheduleFn postSchedFn = <INSERT POST SCHEDULE FN HERE>;
  MdsModel::FSDBWaveInitFn dumpFileFn = <INSERT FSDB INIT HERE>;
  MdsModel::FSDBReadCreateFn fsdbReadCreateFn = <INSERT FSDB READ CREATE HERE>;
  model->putFnInfo(context, initFn, destroyFn, preSchedFn, postSchedFn, dumpFileFn, fsdbReadCreateFn, allowFsdbWrite);
  
  model->createIODB();
}
