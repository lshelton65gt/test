// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "WtbVCDDumpContext.h"


WtbVCDDumpContext::WtbVCDDumpContext(void)
  : mLevels(0), mDumpTime(0), mDumpType(eWaveFileTypeVCD), mOnlyStateIO(false)
{
}


WtbVCDDumpContext::~WtbVCDDumpContext(void)
{
}


void WtbVCDDumpContext::setFilename(const UtString &filename)
{ 
  mFilename = filename; 
};


void WtbVCDDumpContext::setScopes(const UtString &scopes)
{
  mScopes = scopes;
}


void WtbVCDDumpContext::setLevels(UInt32 levels)
{
  mLevels = levels;
}


void WtbVCDDumpContext::setDumpTime(UInt64 dump_time)
{
  mDumpTime = dump_time;
}

void WtbVCDDumpContext::setDumpType(CarbonWaveFileType type)
{
  mDumpType = type;
}

CarbonWaveFileType WtbVCDDumpContext::getDumpType() const
{
  return mDumpType;
}

const UtString & WtbVCDDumpContext::getFilename(void) const
{
  return mFilename;
};


const UtString & WtbVCDDumpContext::getScopes(void) const
{
  return mScopes;
}


UInt32 WtbVCDDumpContext::getLevels(void) const
{
  return mLevels;
}


UInt64 WtbVCDDumpContext::getDumpTime(void) const
{
  return mDumpTime;
}

void WtbVCDDumpContext::setStateIO(bool doOnlyStateIO)
{
  mOnlyStateIO = doOnlyStateIO;
}

bool WtbVCDDumpContext::doOnlyStateIO() const
{
  return mOnlyStateIO;
}
