// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WTBEXECUTEEVENT_H_
#define __WTBEXECUTEEVENT_H_


#include "util/CarbonTypes.h"

class CarbonNet;
class CarbonModel;
class STSymbolTableNode;
class WtbModel;
class WtbSignalPartition;
class WtbSimulator;
class ZostreamDB;
class ZistreamDB;
class DynBitVector;
class STAliasedLeafNode;

/*!
  \file
  Declarations for WtbExecuteEvent and subclasses.
*/

//! WtbExecuteEvent class
/*!
  This is the abstract base class for all execute events.

  Execute events are added to the execute queue.  These are
  the events that are actually executed during the simulation
  run.  They are optimized to execute quickly while attempting
  to keep memory usage to a minimum.

  These objects should not be created directly, but should
  instead be requested from the WtbExecuteEventFactory.
*/
class WtbExecuteEvent
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  WtbExecuteEvent(void);

  //! Destructor.
  virtual ~WtbExecuteEvent(void);

  //! Event type
  /*!
    This is mainly needed for the database.
  */
  enum Type {
    eTimeEvent, //!< Time Event
    eNoXdriveEvent, //!< De-assert xdrive event
    eDataEvent, //!< Data Event
    eDataDriveEvent, //!< Data w/ drive event
    eScheduleEvent, //!< Call Schedule Event
    eDumpOff, //!< Turn dumping off event
    eDumpOn, //!< Turn dumping on event
    eDataScheduleEvent, //! Data schedule event
    eClkScheduleEvent, //! Clock schedule event
    ePreScheduleEvent, //! Pre-schedule event
    eDepComboScheduleEvent //! Deposit-combo schedule event
  };
  
  //! Event Status
  enum Status {
    eOK, //!< OK
    eError, //!< Error
    eEnd //!< A $finish encountered
  };

  //! Perform this object's action.
  /*!
    \retval eOK if the execution was successful
    \retval eError if the execution was unsuccesful
    \retval eEnd if simulation should be stopped.
  */
  virtual Status execute(WtbSimulator* simulator) = 0;

  //! Print this event to UtIO::cout().
  virtual void print(WtbModel* model) const = 0;

  //! Get the type of this event
  virtual Type getType() const = 0;

  //! Write event to db
  virtual bool dbWrite(ZostreamDB& zout) const = 0;
  //! Read event from db
  virtual bool dbRead(ZistreamDB& zin) = 0;

 private:
  WtbExecuteEvent(const WtbExecuteEvent&);
  WtbExecuteEvent& operator=(const WtbExecuteEvent&);
};


//! WtbExecuteTimeEvent class
/*!
  These objects update the current simulation time.

  These objects should not be created directly, but should
  instead be requested from the WtbExecuteEventFactory.
*/
class WtbExecuteTimeEvent : public WtbExecuteEvent
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct a time event object.
    \param event_time The time that the simualtor will be uptaded
    to.
    \param simulator The simulator that will have its time updated.
   */
  WtbExecuteTimeEvent(const UInt64* event_time);

  //! DB-based constructor
  WtbExecuteTimeEvent(void);

  //! Destructor.
  virtual ~WtbExecuteTimeEvent(void);

  //! Perform this object's action.
  /*!
    This object updates the simulator's current time.
   */
  virtual Status execute(WtbSimulator* simulator);

  //! Print this event to UtIO::cout().
  virtual void print(WtbModel* model) const;

  //! WtbExecuteEvent::getType()
  virtual Type getType() const;

  //! WtbExecuteEvent::dbWrite()
  virtual bool dbWrite(ZostreamDB& zout) const;

  //! WtbExecuteEvent::dbRead()
  virtual bool dbRead(ZistreamDB& zin);

private:
  const UInt64* mTime;

private:
  WtbExecuteTimeEvent(const WtbExecuteTimeEvent&);
  WtbExecuteTimeEvent& operator=(const WtbExecuteTimeEvent&);
};


//! WtbExecuteTimeEvent class for vector files
/*!
  This object updates the current simulation time.

  This object should not be created directly, but should
  instead be requested from the WtbExecuteEventFactory. This object is
  specifically meant for vector file reading. There is no time factory
  when reading vector files, so the time gets stored into this class
  as a UInt64 instead of a const UInt64* pointer.
*/
class WtbExecuteVectorTimeEvent : public WtbExecuteEvent
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct a time event object.
    \param event_time The time that the simualtor will be uptaded
    to.
    \param simulator The simulator that will have its time updated.
   */
  WtbExecuteVectorTimeEvent(UInt64 event_time);

  //! DB-based constructor
  WtbExecuteVectorTimeEvent(void);

  //! Destructor.
  virtual ~WtbExecuteVectorTimeEvent(void);

  //! Perform this object's action.
  /*!
    This object updates the simulator's current time.
   */
  virtual Status execute(WtbSimulator* simulator);

  //! Print this event to UtIO::cout().
  virtual void print(WtbModel* model) const;

  //! WtbExecuteEvent::getType()
  virtual Type getType() const;

  //! WtbExecuteEvent::dbWrite()
  virtual bool dbWrite(ZostreamDB& zout) const;

  //! WtbExecuteEvent::dbRead()
  virtual bool dbRead(ZistreamDB& zin);

  //! Get the time value
  UInt64 getTime() const { return mTime; }

private:
  UInt64 mTime;

private:
  WtbExecuteVectorTimeEvent(const WtbExecuteVectorTimeEvent&);
  WtbExecuteVectorTimeEvent& operator=(const WtbExecuteVectorTimeEvent&);
};

//! WtbExecuteDataEvent class
/*!
  This object loads new simulation data into a signal in
  the design.

  These objects should not be created directly, but should
  instead be requested from the WtbExecuteEventFactory.
*/
class WtbExecuteDataEvent : public WtbExecuteEvent
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct a data event object.
    \param signal The signal that is the target of the data update.
    \param model The simulation model
    \param data The data that will be deposited in the signal.
   */
  WtbExecuteDataEvent(CarbonNet* signal, const DynBitVector* data);

  //! DB-based constructor
  WtbExecuteDataEvent(CarbonNet *signal);

  //! Destructor.
  virtual ~WtbExecuteDataEvent();

  //! Perform this object's action.
  /*!
    This object deposits data with the specified signal.
   */
  virtual Status execute(WtbSimulator* simulator);

  //! Print this event to UtIO::cout().
  virtual void print(WtbModel* model) const;

  //! WtbExecuteEvent::getType()
  virtual Type getType() const;

  //! Returns the name of the signal this event is associated with
  void getSignalName(const WtbModel* model, UtString* name) const;

  //! WtbExecuteEvent::dbWrite()
  virtual bool dbWrite(ZostreamDB& zout) const;

  //! WtbExecuteEvent::dbRead()
  virtual bool dbRead(ZistreamDB& zin);

  //! Hash function
  virtual size_t hash() const;

  //! Equality for hash.
  virtual bool operator==(const WtbExecuteDataEvent&) const;

  //! Get the signal symtab node
  const STAliasedLeafNode* getNetNode() const; 

  //! modifiable version
  STAliasedLeafNode* getNetNode() {
    const WtbExecuteDataEvent* me = const_cast<const WtbExecuteDataEvent*>(this);
    return const_cast<STAliasedLeafNode*>(me->getNetNode());
  }
  
  //! Get the values
  virtual void getValues(const UInt32** data, const UInt32** drive);

  //! Get the dynbitvector value
  const DynBitVector* getDynValue() const { return mData; }

protected:
  CarbonNet* mSignal;
  const DynBitVector* mData;
  
  //! print the value to stdout
  virtual void printValue() const;

  //! Compare the signal nodes values (no drives)
  bool doBasicCompare(const WtbExecuteDataEvent&) const;
  
private:
  WtbExecuteDataEvent(void);
  WtbExecuteDataEvent(const WtbExecuteDataEvent&);
  WtbExecuteDataEvent& operator=(const WtbExecuteDataEvent&);
};

class WtbExecuteDataDriveEvent : public WtbExecuteDataEvent
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct a data event object.
    \param signal The signal that is the target of the data update.
    \param model The simulation model
    \param data The data that will be deposited in the signal.
    \param drive The drive value that will be deposited to the signal.
   */
  WtbExecuteDataDriveEvent(CarbonNet* signal, const DynBitVector* data, 
                           const DynBitVector* drive);

  //! DB-based constructor
  WtbExecuteDataDriveEvent(CarbonNet *signal);

  virtual ~WtbExecuteDataDriveEvent();

  //! Execute full data and drive setting.
  virtual Status execute(WtbSimulator* simulator);

  //! WtbExecuteEvent::getType()
  virtual Type getType() const;
  
  //! Hash function
  virtual size_t hash() const;

  //! Equality for hash.
  virtual bool operator==(const WtbExecuteDataEvent&) const;
  
  //! Get the values
  virtual void getValues(const UInt32** data, const UInt32** drive);

  //! Get the dynbitvector values
  void getDynValues(const DynBitVector** value, const DynBitVector** drive) const { 
    *value = mData; 
    *drive = mDrive;
  }
  
  //! WtbExecuteEvent::dbWrite()
  virtual bool dbWrite(ZostreamDB& zout) const;

  //! WtbExecuteEvent::dbRead()
  virtual bool dbRead(ZistreamDB& zin);

protected:
  //! print the value to stdout
  virtual void printValue() const;

private:
  const DynBitVector* mDrive; 
  bool doDriveCompare(const WtbExecuteDataDriveEvent&) const;

  // forbid
  WtbExecuteDataDriveEvent(const WtbExecuteDataDriveEvent&);
  WtbExecuteDataDriveEvent& operator=(const WtbExecuteDataDriveEvent&);
};

//! WtbExecuteNoXdriveEvent class
/*!
  This object deactivates (de-asserts) the xdrive on a bidi
  in the design.

  These objects should not be created directly, but should
  instead be requested from the WtbExecuteEventFactory.
*/
class WtbExecuteNoXdriveEvent : public WtbExecuteEvent
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct a no xdrive event object.
    \param signal The signal that is the target of the xdrive deassertion.
    \param model The simulation model
   */
  WtbExecuteNoXdriveEvent(CarbonNet *signal);

  //! Destructor.
  virtual ~WtbExecuteNoXdriveEvent();

  //! De-assert xdrive
  virtual Status execute(WtbSimulator* simulator);

  //! Print this event to UtIO::cout().
  virtual void print(WtbModel* model) const;

  //! WtbExecuteEvent::getType()
  virtual Type getType() const;

  //! Returns the name of the signal this event is associated with
  void getSignalName(const WtbModel* model, UtString* name) const;

  //! WtbExecuteEvent::dbWrite()
  virtual bool dbWrite(ZostreamDB& zout) const;

  //! WtbExecuteEvent::dbRead()
  virtual bool dbRead(ZistreamDB& zin);

  //! Get the signal symtab node
  const STAliasedLeafNode* getNetNode() const;

  //! modifiable version
  STAliasedLeafNode* getNetNode() {
    const WtbExecuteNoXdriveEvent* me = const_cast<const WtbExecuteNoXdriveEvent*>(this);
    return const_cast<STAliasedLeafNode*>(me->getNetNode());
  }

 private:
  CarbonNet* mNet;
  
 private:
  WtbExecuteNoXdriveEvent(void);
  WtbExecuteNoXdriveEvent(const WtbExecuteNoXdriveEvent&);
  WtbExecuteNoXdriveEvent& operator=(const WtbExecuteNoXdriveEvent&);
};


//! WtbExecutePreScheduleEvent class
/*!
  This object calls the pre-schedule callback function.

  These objects should not be created directly, but should
  instead be requested from the WtbExecuteEventFactory.
*/
class WtbExecutePreScheduleEvent : public WtbExecuteEvent
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  WtbExecutePreScheduleEvent(void);

  //! Destructor.
  virtual ~WtbExecutePreScheduleEvent(void);

  //! Perform this object's action.
  /*!
    This object calls the specified model's schedule method.
   */
  virtual Status execute(WtbSimulator* simulator);

  //! Print this event to UtIO::cout().
  virtual void print(WtbModel* model) const;

  //! WtbExecuteEvent::getType()
  virtual Type getType() const;

  //! WtbExecuteEvent::dbWrite()
  virtual bool dbWrite(ZostreamDB& zout) const;
  
  //! WtbExecuteEvent::dbRead()
  virtual bool dbRead(ZistreamDB& zin);
  
private:
  WtbExecutePreScheduleEvent(const WtbExecutePreScheduleEvent&);
  WtbExecutePreScheduleEvent& operator=(const WtbExecutePreScheduleEvent&);
};

//! WtbExecuteScheduleEvent class
/*!
  This object calls the schedule method of the model that
  is being simulated.

  These objects should not be created directly, but should
  instead be requested from the WtbExecuteEventFactory.
*/
class WtbExecuteScheduleEvent : public WtbExecuteEvent
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct a schedule event object.
   */
  WtbExecuteScheduleEvent(void);

  //! Destructor.
  virtual ~WtbExecuteScheduleEvent(void);

  //! Perform this object's action.
  /*!
    This object calls the specified model's schedule method.
   */
  virtual Status execute(WtbSimulator* simulator);

  //! Print this event to UtIO::cout().
  virtual void print(WtbModel* model) const;

  //! WtbExecuteEvent::getType()
  virtual Type getType() const;

  //! WtbExecuteEvent::dbWrite()
  virtual bool dbWrite(ZostreamDB& zout) const;
  
  //! WtbExecuteEvent::dbRead()
  virtual bool dbRead(ZistreamDB& zin);
  
private:
  WtbExecuteScheduleEvent(const WtbExecuteScheduleEvent&);
  WtbExecuteScheduleEvent& operator=(const WtbExecuteScheduleEvent&);
};

class WtbExecuteDataScheduleEvent : public WtbExecuteScheduleEvent
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct a data (non-clock) schedule event object.
   */
  WtbExecuteDataScheduleEvent(void);

  //! Destructor.
  virtual ~WtbExecuteDataScheduleEvent(void);

  //! Perform this object's action.
  /*!
    This object calls the specified model's schedule method.
   */
  virtual Status execute(WtbSimulator* simulator);

  //! Print this event to UtIO::cout().
  virtual void print(WtbModel* model) const;

  //! WtbExecuteEvent::getType()
  virtual Type getType() const;

  //! WtbExecuteEvent::dbWrite()
  virtual bool dbWrite(ZostreamDB& zout) const;

  //! WtbExecuteEvent::dbRead()
  virtual bool dbRead(ZistreamDB& zin);

 private:
  WtbExecuteDataScheduleEvent(const WtbExecuteDataScheduleEvent&);
  WtbExecuteDataScheduleEvent& operator=(const WtbExecuteDataScheduleEvent&);
};

class WtbExecuteClkScheduleEvent : public WtbExecuteScheduleEvent
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct a clock schedule event object.
   */
  WtbExecuteClkScheduleEvent(void);

  //! Destructor.
  virtual ~WtbExecuteClkScheduleEvent(void);

  //! Perform this object's action.
  /*!
    This object calls the specified model's schedule method.
   */
  virtual Status execute(WtbSimulator* simulator);

  //! Print this event to UtIO::cout().
  virtual void print(WtbModel* model) const;

  //! WtbExecuteEvent::getType()
  virtual Type getType() const;

  //! WtbExecuteEvent::dbWrite()
  virtual bool dbWrite(ZostreamDB& zout) const;

  //! WtbExecuteEvent::dbRead()
  virtual bool dbRead(ZistreamDB& zin);

 private:
  WtbExecuteClkScheduleEvent(const WtbExecuteClkScheduleEvent&);
  WtbExecuteClkScheduleEvent& operator=(const WtbExecuteClkScheduleEvent&);
};


class WtbExecuteDepComboScheduleEvent : public WtbExecuteEvent
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct a clock schedule event object.
   */
  WtbExecuteDepComboScheduleEvent(void);

  //! Destructor.
  virtual ~WtbExecuteDepComboScheduleEvent(void);

  //! Perform this object's action.
  /*!
    This object calls the specified model's schedule method.
   */
  virtual Status execute(WtbSimulator* simulator);

  //! Print this event to UtIO::cout().
  virtual void print(WtbModel* model) const;

  //! WtbExecuteEvent::getType()
  virtual Type getType() const;

  //! WtbExecuteEvent::dbWrite()
  virtual bool dbWrite(ZostreamDB& zout) const;

  //! WtbExecuteEvent::dbRead()
  virtual bool dbRead(ZistreamDB& zin);

 private:
  WtbExecuteDepComboScheduleEvent(const WtbExecuteDepComboScheduleEvent&);
  WtbExecuteDepComboScheduleEvent& operator=(const WtbExecuteDepComboScheduleEvent&);
};

//! WtbExecuteVCDDumpOffEvent class
/*!
  This object calls the dumpOff method of the model that
  is being simulated.

  These objects should not be created directly, but should
  instead be requested from the WtbExecuteEventFactory.
*/
class WtbExecuteVCDDumpOffEvent : public WtbExecuteEvent
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct a VCD dump off event object.
    \param model The model that will have its dumpOff method called.
   */
  WtbExecuteVCDDumpOffEvent(void);

  //! Destructor.
  virtual ~WtbExecuteVCDDumpOffEvent(void);

  //! Perform this object's action.
  /*!
    This object calls the specified model's dumpOff method.
   */
  virtual Status execute(WtbSimulator* simulator);

  //! Print this event to UtIO::cout().
  virtual void print(WtbModel* model) const;

  //! WtbExecuteEvent::getType()
  virtual Type getType() const;

  //! WtbExecuteEvent::dbWrite()
  virtual bool dbWrite(ZostreamDB& zout) const;

  //! WtbExecuteEvent::dbRead()
  virtual bool dbRead(ZistreamDB& zin);

 private:
  WtbExecuteVCDDumpOffEvent(const WtbExecuteVCDDumpOffEvent&);
  WtbExecuteVCDDumpOffEvent& operator=(const WtbExecuteVCDDumpOffEvent&);
};


//! WtbExecuteVCDDumpOnEvent class
/*!
  This object calls the dumpOn method of the model that
  is being simulated.

  These objects should not be created directly, but should
  instead be requested from the WtbExecuteEventFactory.
*/
class WtbExecuteVCDDumpOnEvent : public WtbExecuteEvent
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct a VCD dump on event object.
    \param model The model that will have its dumpOn method called.
   */
  WtbExecuteVCDDumpOnEvent(void);

  //! Destructor.
  virtual ~WtbExecuteVCDDumpOnEvent(void);
  //! Perform this object's action.
  /*!
    This object calls the specified model's dumpOn method.
   */
  virtual Status execute(WtbSimulator* simulator);

  //! Print this event to UtIO::cout().
  virtual void print(WtbModel* model) const;

  //! WtbExecuteEvent::getType()
  virtual Type getType() const;

  //! WtbExecuteEvent::dbWrite()
  virtual bool dbWrite(ZostreamDB& zout) const;

  //! WtbExecuteEvent::dbRead()
  virtual bool dbRead(ZistreamDB& zin);

 private:
  WtbExecuteVCDDumpOnEvent(const WtbExecuteVCDDumpOnEvent&);
  WtbExecuteVCDDumpOnEvent& operator=(const WtbExecuteVCDDumpOnEvent&);
};

#endif

