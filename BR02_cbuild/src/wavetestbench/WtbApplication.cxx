// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/ShellMsgContext.h"
#include "util/Stats.h"
#include "util/Zstream.h"
#include "util/ConstantRange.h"
#include "Wtb.h"
#include "modshell/WtbApplication.h"
#include "WtbModel.h"
#include "WtbTestbench.h"
#include "WtbVCDDumpContext.h"
#include "WtbUtil.h"
#include "hdl/HdlVerilogPath.h"
#include "shell/CarbonDBRead.h"
#include "shell/ReplaySystemSim.h"

const char* WtbApplication::scAllDepositablesClocks = "-allDepositablesClocks";

WtbApplication::WtbApplication(int *argc, char** argv, bool cleanMemory)
  : UtApplication(argc, argv), mModel(NULL), mStats(NULL), 
    mVCDDumpContext(NULL), mLicCB(mMsgContext), mLicense(&mLicCB), 
    mCleanMemory(cleanMemory)
{
  mReplaySystem = new CarbonSystemSim;
}


WtbApplication::~WtbApplication()
{
  destroyModel();
  destroyStats();
  destroyVCDDumpContext();
}


SInt32 WtbApplication::init( const char *progName )
{
  setupCommandLineOptions();
  if ( not checkoutLicense( progName ))
    return Wtb::eWtbStatusError;
  if (not parseCommandLine())
    return Wtb::eWtbStatusError;
  
  createStats();
  createModel();
  return Wtb::eWtbStatusSuccess;
}

static bool sFixVecFile(UtString* vecFileFix, const char* vecFile)
{
  vecFileFix->assign(vecFile);

  const char* ext = strrchr(vecFile, '.');
  bool needExtFix = (ext == NULL);
  if ((ext != NULL) && (strcmp(ext, ".cva") != 0))
    needExtFix = true;


  if (needExtFix)
    *vecFileFix << ".cva";
  return needExtFix;
}

SInt32 WtbApplication::run()
{
  bool vecOnly = (mArgs.isParsed("-vecIn") == ArgProc::eParsed);
  bool vecSave = (mArgs.isParsed("-save") == ArgProc::eParsed);
  bool verbose = ! mArgs.getBoolValue("-q");
  WtbTestbench* testbench = NULL;
  Wtb::Status status = Wtb::eWtbStatusSuccess;
  
  const char* refclk = NULL;
  if (mArgs.isParsed("-refclk") == ArgProc::eParsed)
    mArgs.getStrValue("-refclk", &refclk);
  
  if (refclk != NULL)
  {
    if (! vecOnly && ! vecSave)
    {
      // -refclk only allowed when saving or reading cva files
      mMsgContext->WtbRefClkNoCVA();
      return Wtb::eWtbStatusError;
    }
  }

  bool cycleSched = (mArgs.isParsed("-cycleSchedule") == ArgProc::eParsed);
  
  const char* vecFile = NULL;
  if (vecOnly)
  {
    vecFile = mArgs.getStrValue("-vecIn");
    // sanity check
    if (cycleSched)
      mMsgContext->WtbCycleSchedWithVec();
  }
  else if (vecSave)
    vecFile = mArgs.getStrValue("-save");
  
  UtString vecFileFix;
  if (vecFile)
  {
    if (sFixVecFile(&vecFileFix, vecFile))
    {
      mMsgContext->WtbChangingVecFileName(vecFile, vecFileFix.c_str());
      vecFile = vecFileFix.c_str();
    }
  }
  
  SInt32 limitSchedCalls;
  INFO_ASSERT(mArgs.getIntLast("-limitSchedCalls", &limitSchedCalls) == ArgProc::eKnown, "Argument processor doesn't recognize known argument string");
  
  if (mArgs.isParsed("-translateVecsToMem") == ArgProc::eParsed)
  {
    const char* outFile = mArgs.getStrValue("-translateVecsToMem");
    if (! vecOnly)
    {
      mMsgContext->WtbRequiresOption("-translateVecsToMem", "-vecIn");
      return 1;
    }
    
    if (! mArgs.isParsed("-translateMapFile") == ArgProc::eParsed)
    {
      mMsgContext->WtbRequiresOption("-translateVecsToMem", "-translateMapFile");
      return 1;
    }
    
    const char* mapFile = mArgs.getStrValue("-translateMapFile");

    testbench = new WtbTestbench(mModel, NULL, mStats, mMsgContext, mReplaySystem);
    status = testbench->translateVectorFileToTestDriver(vecFile, mapFile, outFile, verbose, limitSchedCalls);
    
    if (mCleanMemory)
      delete testbench;
    
    return status;
  }
  
  // setup replay 
  mModel->addComponentToReplaySystem(mReplaySystem);
  mReplaySystem->writeSystem();
  
  UtString base_path;
  getBasePath(&base_path);
  
  
  bool useBasePath = mArgs.getBoolValue("-renameRoot");
  
  if (! vecOnly)
  {
    UInt32 numIters = 0;
    if (! vecSave)
    {
      numIters = getNumberOfSimulations();
      createVCDDumpContext();
    }

    testbench = new WtbTestbench(mModel, &base_path, mVCDDumpContext, mStats, mMsgContext, cycleSched, mReplaySystem);
    status = testbench->createVectors(&mArgs, numIters, vecFile, &mStringCache, verbose, useBasePath, limitSchedCalls, mCleanMemory, refclk);
  }
  else
  {
    createVCDDumpContext();
    testbench = new WtbTestbench(mModel, mVCDDumpContext, mStats, mMsgContext, 
                                 mReplaySystem);
    if (mArgs.isParsed("-vecDumpVcd") == ArgProc::eParsed)
      status = testbench->dumpVectorsToVCD(mArgs.getStrValue("-vecDumpVcd"), vecFile, verbose, limitSchedCalls);
    else
      status = testbench->test(getNumberOfSimulations(), vecFile, verbose, useBasePath, limitSchedCalls, refclk);
  }
  
  if (mCleanMemory)
    delete testbench;
  
  // release the license now to avoid race conditions in the license
  // server. This used to be in the destructor, but that may not be
  // called.
  mLicense.release( UtLicense::eCwaveTestbench );

  // must destroy the model because of replay and waveforms.
  destroyModel();
  return SInt32(status);
}


bool
WtbApplication::checkoutLicense( const char *progName )
{
  UtString reason;
  bool gotLicense = mLicense.checkout( UtLicense::eCwaveTestbench, &reason );
  if ( !gotLicense )
  {
    UtString path, file;
    OSParseFileName( progName, &path, &file );
    mMsgContext->WtbLicenseCheckFail( file.c_str(), reason.c_str( ));
  }
  return gotLicense;
}

void WtbApplication::setupCommandLineOptions(void)
{
  mArgs.setDescription("Wave Testbench",
                       "cwavetestbench",
		       "Drives a single carbon model based on a source "
		       "waveform file.");
  
  const SInt32 cGenArgPass = 1;

  /*
  ** Waveform file information.
  */
  const char* inputControl = "Input Control";
  const char* outputControl = "Output Control";
  const char* flowControl = "Flow Control";
  mArgs.createSection(inputControl);
  mArgs.createSection(outputControl);
  mArgs.createSection(flowControl);

  mArgs.addString("-unlistedval", "If a vcd value is unlisted, i.e., a signal is specified but the associated glyph is never used, then this will set all the bits of the value of the signal and all of its aliases to the specified value. Valid values are 0|1|x|z. Only effective with vcd formatted input waveforms", "x", false, false, cGenArgPass);
  mArgs.addToSection(inputControl, "-unlistedval");
  mArgs.addString("-iodbfile", "Name of iodb file", "", false, false, cGenArgPass);
  mArgs.addToSection(inputControl, "-iodbfile");
  mArgs.addString("-waveFile", "Waveform file name", "", false, false, cGenArgPass);
  mArgs.addToSection(inputControl, "-waveFile");
  mArgs.addBool("-vcd", "Read VCD waveform file", false, cGenArgPass);
  mArgs.addToSection(inputControl, "-vcd");
  mArgs.addBool("-deposits", "Deposit to all nets marked as depositable as specified in the Carbon compiler directives.", false, cGenArgPass);
  mArgs.addToSection(flowControl, "-deposits");
  mArgs.addBool("-enableReplay", "Turns replay on in the Carbon Model. The Carbon Model must be compiled with the -enableReplay option. You don't need to specify this if you specify -record or -playback.", false, cGenArgPass);
  mArgs.addToSection(flowControl, "-enableReplay");
  mArgs.addBool("-onDemand", "Turns OnDemand on in the Carbon Model. The Carbon Model must be compiled with the -onDemand option.", false, cGenArgPass);
  mArgs.addToSection(flowControl, "-onDemand");
  mArgs.addBool("-fsdb", "Read FSDB waveform file", false, cGenArgPass);  
  mArgs.addToSection(inputControl, "-fsdb");
  mArgs.addString("-basePath", "Path to instance within the wave file", "", false, false, cGenArgPass);
  mArgs.addToSection(inputControl, "-basePath");
  mArgs.addInt("-limitSchedCalls", "Number of schedule calls at which the simulation will end for that iteration. For example, if you specify -limitSchedCalls 10 and -n 2, a total of 20 schedule calls will take place, because each simulation will run 10 schedule calls. This is only useful when reading the wave file. Therefore, this can be used with -save and when running the simulation without a vector file. A value of 0 (zero) will run all the schedule calls.", 0, false, false, cGenArgPass);
  mArgs.addToSection(flowControl, "-limitSchedCalls");
  mArgs.addBool("-noBidi", "Treat bidirects as inputs only. Do not do enable expression evaluation.", false, cGenArgPass);
  mArgs.addToSection(flowControl, "-noBidi");

  mArgs.addString("-translateVecsToMem", "Translate cwavetestbench vectors to memory dat file format.", "", false, false, cGenArgPass);
  mArgs.addToSection(flowControl, "-translateVecsToMem");

  mArgs.addString("-translateMapFile", "Map file used to place the signals in the correct order in the memory dat file", "", false, false, cGenArgPass);
  mArgs.addToSection(inputControl, "-translateMapFile");

  mArgs.addString("-configfile", "Configuration information. Bidirect enable information is specified here in the format:<verbatim>\n          enable <bidirect_signal> [!]<enable_signal>\n</verbatim>The bidirect_signal must be whole currently, while the enable_signal can be a scalar, bitselect, partselect or a full vector. If no range is given on a vector enable, the entire vector is used. Other configuration directives are: clock - to specify that an input/deposit should be treated as a 'clock', 'data' - to specify that an input/deposit should be treated as a non-clock input, and 'ignore' - to not stimulate an input/deposit. Each directive has the format:<verbatim>\n          <directive> <signal>\n</verbatim>If a signal is specified without the top-level (i.e., the path does not contain a '.'), the top-level of the design in the database will be used.", "", false, false, cGenArgPass);
  mArgs.addToSection(flowControl, "-configfile");

  mArgs.addBool("-cycleSchedule", "When determining the times at which the schedule function should be called, base it on clocks, inputs combinationally tied to clocks, and resets. By default, the schedule is called whenever the time changes.", false, cGenArgPass);
  mArgs.addToSection(flowControl, "-cycleSchedule");

  /*
  ** Simulation controls.
  */
  mArgs.addString("-vecDumpVcd", "If -vecIn is specified then this can be used to dump the vectors out to a vcd file. The provided string is the root of the vcd file. _<number>.vcd is added to the given root. If there are multiple partitions of vectors, each partition gets a vcd file with its corresponding partition number. The vcd file will be created and filled with all the vector changes. Time and schedule calls are kept as signals. The waveform time is meaningless, except it denotes the progress of the vector queue. After all the vectors are dumped, the program exits.", NULL, true, false, cGenArgPass);
  mArgs.addToSection(flowControl, "-vecDumpVcd");

  mArgs.addInt("-n", "Number of times to run the simulation", 1, false, false, cGenArgPass);
  mArgs.addToSection(flowControl, "-n");

  mArgs.addBool(scAllDepositablesClocks, "By default, cwave treats all signals marked as a depositSignal as primary inputs. If a depositable is not a clock, the value is delayed when creating no-input-flow vectors. Use this option to have cwave treat all signals marked as a depositSignal as clocks.", false, cGenArgPass);
  mArgs.addToSection(flowControl, scAllDepositablesClocks);

  mArgs.addBool("-inputFlow", "By default cwave runs vectors such that clocks are run first and then inputs are updated. This switch updates all the inputs prior to running schedule, which results in inputs flowing through registers.", false, cGenArgPass);
  mArgs.addToSection(flowControl, "-inputFlow");
  
  mArgs.addString("-vecIn", "Specifies the vector file to use to stimulate the design. The filename should end with a .cva extension. The extension will automatically be added if it isn't present. When running the model with -vecIn, you can decide to dump vcd/fsdb on the command line with -dumpFile, -dumpHier, -dumpLevels, etc. Those events get scheduled prior to running the design. Therefore, if -dumpFile is specified with -vecIn, dumping will be turned on at the beginning of time.", NULL, true, false, cGenArgPass);
  mArgs.addToSection(inputControl, "-vecIn");

  mArgs.addBool("-renameRoot", "When dumping waveforms rename the compiled design root to the path given by -basePath. If reading a vector file, the given base path was stored, so it will use that; otherwise, it will use the compiled design root.", false, cGenArgPass);
  mArgs.addToSection(inputControl, "-renameRoot");

  mArgs.addOutputFile("-save", "Specifies file to save vectors. A .cva extension will automatically be added to the argument, if necessary. This option does not work when -vecIn is specified. When saving vectors cwtb must read in a waveform file. That file then requires all the options necessary to map the signals in the file to the nets in the cbuild-generated model, which includes options like -basepath, and -waveFile. This does not include options like -dumpFile, -dumpHier, etc.", NULL, true, ArgProc::eRegFile, false, cGenArgPass);
  mArgs.addToSection(outputControl, "-save");

  mArgs.addString("-refclk", "Specify which signal to use as the reference clock when calculating the speed of the design. By default, cwave uses the fastest clock in the carbon model. This option can only be used when either creating or running cva files. When creating a cva file, cwave will check if the signal exists in the waveform and monitor the number of times it changes, and write that information to the cva file. When reading a cva file, this option trumps the refclk specification used, if applicable, when the cva file was written. If the specified signal is not in the list of known clocks an error will occur. The signal must be specified in terms of the source waveform hierarchy when writing the cva file. The clocks that the cwave monitors by default are in terms of the carbon-compiled design, but the clock that you specify here must be found in the source waveform, as it may not exist in the carbon model. When reading a cva file, the -refclk argument must be specified exactly as it appears in the Clock report for Note 7050 (spewed out when reading the cva file and when -q is NOT specified). This is because the cva runtime does not have the source waveform information anymore and only has the symbols that were saved.", NULL, true, false, cGenArgPass);
  mArgs.addToSection(outputControl, "-refclk");

  mArgs.addBool("-v", "Be verbose.", true, cGenArgPass);
  mArgs.putIsDeprecated("-v", true);
  mArgs.addToSection(outputControl, "-v");

  /*
  ** VCD dumping control.
  */
  const char* vcdSect = "Wave Dumping Control";
  mArgs.createSection(vcdSect);
  mArgs.addBool("-useIODB", "Use the small database for vcd dumping. This will only dump the top-level signals and any signals specified in the directives.", false, cGenArgPass);
  mArgs.addToSection(vcdSect, "-useIODB");

  mArgs.addInt("-dumpLevel", "Number of levels of hierarchy to dump. 0 will dump everything from the specified instance and its children.", 0, false, false, cGenArgPass);
  mArgs.addToSection(vcdSect, "-dumpLevel");
  //  mArgs.addInt("-dumpTime", "Begin dumping at this time", 0, false, false, cGenArgPass);
  //  mArgs.addToSection(vcdSect, "-dumpTime");
  mArgs.addString("-dumpFile", "File to which to dump waveform data. If this is not specified no waveform dumping will occur. You must specify -dumpHier as well with this option", NULL, true, false, cGenArgPass);
  mArgs.addToSection(vcdSect, "-dumpFile");
  mArgs.addString("-dumpHier", "Instance name at which to start dumping values. This is a required switch if -dumpFile is specified.", "", false, false, cGenArgPass);
  mArgs.addToSection(vcdSect, "-dumpHier");
  mArgs.addBool("-dumpStateIO", "If dumping is turned on, dump only state and primary i/os of the design.", false, cGenArgPass);
  mArgs.addToSection(vcdSect, "-dumpStateIO");

  mArgs.addBool("-dumpfsdb", "Dump fsdb format. Otherwise dump VCD",  false, cGenArgPass);
  mArgs.addToSection(vcdSect, "-dumpfsdb");

  /*
  ** Misc.
  */
  mArgs.addBool("-stats", "Print out statistics for cwavetestbench", true, cGenArgPass);  
  mArgs.putIsDeprecated("-stats", true);
  
  mArgs.addBool("-q", "Do not print out cwave statistics or unimportant messages.", false, cGenArgPass);
  
  mArgs.addBool("-modelstats", "Print out statistics for the generated model", false, cGenArgPass);
  mArgs.addToSection(outputControl, "-stats");
  mArgs.addToSection(outputControl, "-modelstats");
  mArgs.addToSection(outputControl, "-q");

  mReplaySystem->putArgProc(&mArgs);
}


bool WtbApplication::parseCommandLine(void)
{
  int num_options;
  UtString error_msg;

  if (*mArgc == 1)
  {
    UtString usage;
    mArgs.getUsageVerbose(&usage);
    UtIO::cout() << usage << UtIO::endl;
    exit(0);
  }

  ArgProc::ParseStatusT parseStat = mArgs.parseCommandLine(mArgc, mArgv, &num_options, &error_msg);
  bool printMessage = parseStat != ArgProc::eParsed;
  if (printMessage)
    UtIO::cout() << error_msg;
  if (parseStat == ArgProc::eParseError)
    return false;
  
  const char* filename;
  if ((mArgs.getStrValue("-waveFile", &filename) != ArgProc::eKnown) && (*filename != '\0'))
  {
    printUsageMessage();
    return false;
  }
  
  bool stat = true;
  // If there are any arguments left on the command line print an
  // error
  for (int i = 1; i < *mArgc; ++i)
  {
    stat = false;
    mMsgContext->WtbInvalidCmdLineArg(mArgv[i]);
  }

  /*
  ** Make sure one and only one file type is specified.
  */
  if (mArgs.isParsed("-vecIn") != ArgProc::eParsed)
  {
    bool vcd_file = mArgs.getBoolValue("-vcd");
    bool fsdb_file = mArgs.getBoolValue("-fsdb");
    if ((vcd_file and fsdb_file) or
        ((not vcd_file) and (not fsdb_file)))
    {
      mMsgContext->OnlyOneFileType();
      return false;
    }
  }
  
  if (stat)
  {
    if (! mReplaySystem->processCmdline(mArgc, mArgv, false, true))
    {
      UtIO::cout() << mReplaySystem->getErrmsg() << UtIO::endl;
      stat = false;
    }
  }
  return stat;
}


void WtbApplication::printUsageMessage(void)
{
  UtString usage;
  mArgs.getUsageVerbose(&usage);
  UtIO::cout() << usage << UtIO::endl;
}


bool WtbApplication::getBasePath(UtString *base_path)
{
  const char* b;

  mArgs.getStrValue("-basePath", &b);
  *base_path = UtString(b);

  return base_path->length() != 0;
}


UInt32 WtbApplication::getNumberOfSimulations(void)
{
  SInt32 iterations;
  mArgs.getIntLast("-n", &iterations);
  return static_cast<UInt32>(iterations);
}


bool WtbApplication::isDumpingVCD(void)
{
  return (mArgs.isParsed("-dumpFile") == ArgProc::eParsed);
}


void WtbApplication::createVCDDumpContext(void)
{
  if (isDumpingVCD())
  {
    const char* filename;
    const char* scopes = "";
    SInt32 levels;
    //    SInt32 dump_time;

    INFO_ASSERT(mVCDDumpContext == 0, "Attempting to create a vcd dump context when one has already been created.");
    mVCDDumpContext = new  WtbVCDDumpContext;

    mArgs.getStrValue("-dumpFile", &filename);
    if (mArgs.isParsed("-dumpHier") == ArgProc::eParsed)
      mArgs.getStrValue("-dumpHier", &scopes);
    else
      mMsgContext->WtbMustSpecifyDumpHier();

    mArgs.getIntLast("-dumpLevel", &levels);
    //    mArgs.getIntLast("-dumpTime", &dump_time);

    mVCDDumpContext->setFilename(UtString(filename));
    mVCDDumpContext->setScopes(UtString(scopes));
    mVCDDumpContext->setLevels(static_cast<UInt32>(levels));
    //    mVCDDumpContext->setDumpTime(static_cast<UInt64>(dump_time));
    mVCDDumpContext->setStateIO(mArgs.getBoolValue("-dumpStateIO"));
    CarbonWaveFileType fileType = eWaveFileTypeVCD;
    if (mArgs.getBoolValue("-dumpfsdb"))
    {
      if (! mModel->allowFsdbWrite())
        mMsgContext->WtbFsdbWriteNotAllowed(); // fatal
      fileType = eWaveFileTypeFSDB;
    }
    mVCDDumpContext->setDumpType(fileType);
  }
}


void WtbApplication::destroyVCDDumpContext(void)
{
  if (mVCDDumpContext)
  {
    delete mVCDDumpContext;
    mVCDDumpContext = 0;
  }
}


void WtbApplication::createModel()
{
  UtString iodb_filename;

  getIODBFilename(&iodb_filename);
  INFO_ASSERT(mModel == 0, "Attempting to create a WtbModel when one has already been created.");
  mModel = new WtbModel(iodb_filename);
}


void WtbApplication::destroyModel(void)
{
  if (mReplaySystem)
  {
    delete mReplaySystem;
    mReplaySystem = NULL;
  }
  if (mModel)
  {
    mModel->callUserDestroy();
    delete mModel;
    mModel = 0;
  }
}

void WtbApplication::createStats(void)
{
  if (! mArgs.getBoolValue("-q"))
  {
    INFO_ASSERT(mStats == 0, "createStats called more than once");
    mStats = new Stats(stdout);
  }
}


void WtbApplication::destroyStats(void)
{
  if (mStats)
  {
    mStats->printTotalStatistics();
    delete mStats;
    mStats = 0;
  }
}


bool WtbApplication::getIODBFilename(UtString *iodb_filename)
{
  const char* b;

  mArgs.getStrValue("-iodbfile", &b);
  iodb_filename->clear();
  (*iodb_filename) << b;

  return ! iodb_filename->empty();
}

bool WtbApplication::isReplayEnabled() const
{
  // We can only have one component in a cwave run, so just see if we
  // record or play was requested
  return
    mReplaySystem->isRecordAll() || 
    mReplaySystem->isPlaybackAll() ||
    mArgs.getBoolValue("-enableReplay");
}

bool WtbApplication::isOnDemandEnabled() const
{
  return mArgs.getBoolValue("-onDemand");
}

WtbApplication::Config::Config(IODBRuntime *iodb, MsgContext* msgContext)
  : mIODB(iodb), mMsgContext(msgContext)
{
}


WtbApplication::Config::~Config(void)
{
  // Not deleting the key, only the value, so don't need to do the one
  // by one erase of the hash map.
  for (SigToEnableMap::iterator p = mEnables.begin(), e = mEnables.end();
       p != e; ++p)
  {
    EnableInfo* info = p->second;
    delete info;
  }
}


WtbApplication::Config::Status
WtbApplication::Config::readFile(const char* filename)
{
  UtString buf;

  ZISTREAM(file, filename);
  if (! file)
  {
    mMsgContext->CouldNotOpenConfigFile(file.getError());
    return eError;
  }

  Status stat = eOK;
  HdlVerilogPath pather;
  
  int lineNum = 0;
  while(file.readline(&buf) != 0)
  {
    ++lineNum;

    StrToken tok(buf.c_str());
    const char* item;
    const char* rest;

    bool contParse = true;
    while (tok(&item) && contParse)
    {
      // For most cases, we only need to tokenize the first item on
      // the line. The rest of the line gets parsed by the particular
      // command context
      contParse = false;

      // skip comments
      if (item[0] == '#')
        break;

      UtStringArray ids;
      HdlId sigInfo;
      const STSymbolTableNode* signal = NULL;
      const STAliasedLeafNode* leaf = NULL;
      UtString name;
  
      if (strcmp(item, "enable") == 0)
      {
        rest = tok.curPos();
        
        UtString bidi;
        UtString enable;
        HdlId enableHdlId;
        HdlId bidInfo;
        bool inverted = false;
        
        for (UInt32 numTokens = 1; numTokens < 3; ++numTokens)
        {
          if (tok.atEnd())
          {
            mMsgContext->WtbConfigTooFewTokens(filename, lineNum, numTokens, 3);
            stat = eError;
          }
          else
          {
            UtString* sigPtr = NULL;
            // using a pointer for the HdlId as a precursor to the
            // eventual change where the bidi signal itself can be
            // specified in parts. Currently only enableHdlId is used.
            HdlId* infoPtr = NULL;
            bool isBidiName = false;
            if (numTokens == 1)
            {
              isBidiName = true;
              sigPtr = &bidi;
              infoPtr = &bidInfo;
            }
            else if (numTokens == 2)
            {
              sigPtr = &enable;
              infoPtr = &enableHdlId;
            }
            else
            {
              UtString errMsg;
              errMsg << filename << ":" << lineNum << ": Parsed beyond 2 tokens for enable";
              INFO_ASSERT(0, errMsg.c_str());
              sigPtr = NULL;
            }

            ids.clear();
            if (! isBidiName)
            {
              if (*rest == '!')
              {
                inverted = true;
                ++rest;
              }
            }

            if (pather.parseName(&rest, &ids, infoPtr) != HdlHierPath::eEndPath)
            {
              mMsgContext->WtbConfigIllegalVlogIdent(filename, lineNum, rest);
              stat = eError;
            }
            else
            {
              if (isBidiName)
                maybePrependCarbonTopLevel(&ids);
              pather.compPath(ids, sigPtr, NULL);
            }
          }
        }
        
        rest = StringUtil::skip(rest);
        if (*rest != '\0')
        {
          mMsgContext->WtbConfigTooManyTokens(filename, lineNum, 3);
          if (stat != eError)
            stat = eWarning;
        }
        
        signal = lookupDesignSignal(bidi);
        const STSymbolTableNode * enableNode = lookupEnableSignal(enable);
        
        if (! (signal && enableNode))
          stat = eError;
        else if (stat != eError)
        {
          SigToEnableMap::iterator p = mEnables.find(signal);
          if (p != mEnables.end())
          {
            mMsgContext->WtbConfigMultipleEnables(filename, lineNum, bidi.c_str());
            if (stat != eError)
              stat = eWarning;
            delete p->second;
          }
          
          EnableInfo* enableInfo = new EnableInfo(enableNode, enableHdlId, inverted);
          mEnables[signal] = enableInfo;
        }
      }
      else if (strcmp(item, "ignore") == 0)
      {
        rest = tok.curPos();
        if (pather.parseName(&rest, &ids, &sigInfo) != HdlHierPath::eEndPath)
        {
          mMsgContext->WtbConfigIllegalVlogIdent(filename, lineNum, rest);
          stat = eError;
        }
        else
        {
          maybePrependCarbonTopLevel(&ids);
          pather.compPath(ids, &name, NULL);
          signal = lookupDesignSignal(name);
          
          if (! signal)
            stat = eError;
          else
          {
            if (checkTypeExclusivity(signal, "ignore"))
            {
              // only store the storage node
              leaf = signal->castLeaf();
              ST_ASSERT(leaf, signal);
              mIgnore.insert(leaf->getStorage());
            }
            else
              stat = eError;
          }
        }
      }
      else if (strcmp(item, "clock") == 0)
      {
        rest = tok.curPos();
        if (pather.parseName(&rest, &ids, &sigInfo) != HdlHierPath::eEndPath)
        {
          mMsgContext->WtbConfigIllegalVlogIdent(filename, lineNum, rest);
          stat = eError;
        }
        else
        {
          maybePrependCarbonTopLevel(&ids);

          pather.compPath(ids, &name, NULL);
          signal = lookupDesignSignal(name);
          
          if (! signal)
            stat = eError;
          else
          {
            if (checkTypeExclusivity(signal, "clock"))
            {
              leaf = signal->castLeaf();
              ST_ASSERT(leaf, signal);
              mClocks.insert(leaf->getStorage());
            }
            else 
              stat = eError;
          }
        }
      }
      else if (strcmp(item, "data") == 0)
      {
        rest = tok.curPos();
        if (pather.parseName(&rest, &ids, &sigInfo) != HdlHierPath::eEndPath)
        {
          mMsgContext->WtbConfigIllegalVlogIdent(filename, lineNum, rest);
          stat = eError;
        }
        else
        {
          maybePrependCarbonTopLevel(&ids);
          pather.compPath(ids, &name, NULL);
          signal = lookupDesignSignal(name);
          
          if (! signal)
            stat = eError;
          else
          {
            if (checkTypeExclusivity(signal, "data"))
            {
              leaf = signal->castLeaf();
              ST_ASSERT(leaf, signal);
              mDataInputs.insert(leaf->getStorage());
            }
            else 
              stat = eError;
          }
        }
      }
      else if (strcmp(item, "module") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "width") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "compare") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "tie") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "timeunit") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "timescale") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "input") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "output") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "inout") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "bidir") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "ignoreall") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "delay") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "delayall") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else if (strcmp(item, "include") == 0)
      {
        mMsgContext->WtbConfigUnsupportedCmd(filename, lineNum, item);
        if (stat != eError)
          stat = eWarning;
      }
      else
      {
        mMsgContext->WtbConfigUnrecognizedCmd(filename, lineNum, item);
        stat = eError;
      }
    }
  }


  if (file.fail())
  {
    stat = eError;
    mMsgContext->SHLFileProblem(file.getError());
  }

  file.close();
  
  return stat;
}
  

bool WtbApplication::Config::checkTypeExclusivity(const STSymbolTableNode* node, 
                                                  const char* desiredType) const
{
  // Make sure this has not been seen in any of the following:
  //  ignore
  //  data
  //  clock
  const char* currentType = NULL;
  bool isExclusive = true;
  if (mIgnore.count(node))
    currentType = "ignore";
  else if (mClocks.count(node))
    currentType = "clock";
  else if (mDataInputs.count(node))
    currentType = "data";
  
  if (currentType && (strcmp(currentType, desiredType) != 0))
  {
    UtString buf;
    ShellSymTabBOM::composeName(node, &buf, false, true);
    
    isExclusive = false;
    mMsgContext->WtbConfigSigPrevDeclAs(buf.c_str(), desiredType, currentType);
  }
  return isExclusive;
}

bool WtbApplication::Config::isClock(const STSymbolTableNode* node) const
{
  const STAliasedLeafNode* leaf = node->castLeaf();
  return mClocks.count(leaf->getStorage()) != 0;
}

bool WtbApplication::Config::isData(const STSymbolTableNode* node) const
{
  const STAliasedLeafNode* leaf = node->castLeaf();
  return mDataInputs.count(leaf->getStorage()) != 0;
}

bool WtbApplication::Config::isIgnore(const STSymbolTableNode* node) const
{
  const STAliasedLeafNode* leaf = node->castLeaf();
  return mIgnore.count(leaf->getStorage()) != 0;
}

void WtbApplication::Config::maybePrependCarbonTopLevel(UtStringArray* ids) const
{
  if (ids->size() == 1)
  {
    // prepend the top-level name of the design
    UtString sig(ids->back());
    ids->clear();
    ids->push_back(mIODB->getTopLevelModuleName());
    ids->push_back(sig);
  }
}

const STAliasedLeafNode* 
WtbApplication::Config::getEnable(STSymbolTableNode* signal, HdlId* info, bool* inverted) const
{
  const STSymbolTableNode* ret = NULL;
  const STAliasedLeafNode* retLeaf = NULL;
  SigToEnableMap::const_iterator p = mEnables.find(signal);
  if (p != mEnables.end())
  {
    const EnableInfo* enableInfo = p->second;
    ret = enableInfo->mEnable;
    *info = enableInfo->mEnableInfo;
    *inverted = enableInfo->mInvertEnable;
  }
  if (ret)
    retLeaf = ret->castLeaf();
  return retLeaf;
}


const STSymbolTableNode* 
WtbApplication::Config::lookupDesignSignal(const UtString &signal) const
{
  HdlHierPath::Status status;
  HdlId info;
  STSymbolTable* symTab = mIODB->getDesignSymbolTable();
  const STSymbolTableNode * node = symTab->getNode(signal.c_str(), &status, &info);
  if (node == NULL)
  {
    // Not in the symbol table, check if invalid name, possibly a
    // syntax error
    if (status == HdlHierPath::eIllegal) 
      mMsgContext->SHLInvalidPath(signal.c_str());
    else
      mMsgContext->SHLPathNotFound(signal.c_str());
  }
  
  return node;
}

const STSymbolTableNode* 
WtbApplication::Config::lookupEnableSignal(const UtString &signal)
{
  HdlHierPath::Status status;
  HdlId info;
  STSymbolTable* symTab = mIODB->getExprSymTab();
  const STSymbolTableNode * node = symTab->getNode(signal.c_str(), &status, &info);
  if (node == NULL)
  {
    // Not in the symbol table, check if invalid name, possibly a
    // syntax error
    if (status == HdlHierPath::eIllegal) 
      mMsgContext->SHLInvalidPath(signal.c_str());
    else
    {
      // it has to be created
      node = symTab->createLeafFromPath(signal.c_str(), &status, &info);
      INFO_ASSERT(status != HdlHierPath::eIllegal, signal.c_str());
      INFO_ASSERT(node, "SymNode not created for legal name.");
    }
    
  }
  
  return node;
}

WtbApplication::Config::SigEnableLoop 
WtbApplication::Config::loopEnabledSignals() const
{
  return mEnables.loopSorted();
}

MdsModel* WtbApplication::getModel()
{
  return mModel;
}

WtbApplication::WtbLicCB::WtbLicCB(MsgContext* msgContext) :
  mMsgContext(msgContext)
{}

WtbApplication::WtbLicCB::~WtbLicCB()
{}

void WtbApplication::WtbLicCB::waitingForLicense(const char*)
{}

void WtbApplication::WtbLicCB::queuedLicenseObtained(const char*)
{}

void WtbApplication::WtbLicCB::requeueLicense(const char*)
{}


void WtbApplication::WtbLicCB::relinquishLicense(const char*)
{}


void WtbApplication::WtbLicCB::exitNow(const char* reason) 
{
  mMsgContext->SHLLicenseServerFail(reason);
}
