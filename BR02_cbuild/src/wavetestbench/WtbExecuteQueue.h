// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WTBEXECUTEQUEUE_H_
#define __WTBEXECUTEQUEUE_H_


#include "util/Loop.h"
#include "util/UtArray.h"
#include "Wtb.h"

class MsgContext;
class UtString;
class WfAbsFileReader;
class WtbExecuteEvent;
class WtbExecuteEventFactory;
class WtbModel;
class WtbSimulator;
class ZostreamDB;
class ZistreamDB;
class WfAbsSignal;
class DynBitVectorFactory;

/*!
  \file
  The WtbExecuteQueue class.
*/


//! WtbExecuteQueue class
/*!
  This object maintains an ordered list of WtbExecuteEvent objects.  Its
  responsibilities are to create itself from a WtbScheduleQueue and to
  provide and iterator over it's events.

  The events contained in the queue are optimized for execution speed while
  making attempts to keep memory usage a low as possible.
*/
class WtbExecuteQueue
{
 public:
  //! Constructor.
  /*!
    Construct an execute queue object.   This involves taking a WtbScheduleQueue
    and populating this queue with equivalent WtbExecuteEvent objects.
    \param schedule_queue The schedule queue that will be used to populate this queue.
    \param wave_file The object that contains the waveforms.
    \param model The model that is being simulated.
    \param simulator The simulator that will be evaluating the events.
    \param base_path The instance contained within the waveform data
    that is being simulated.
    \param message_context Context for error messages.
    \param limitSchedCalls Maximum number of schedule calls to allow. If 0
    the entire schedule queue will be copied.
    \param verbose Be verbose
   */
  WtbExecuteQueue(WfAbsFileReader* wave_file, WtbModel* model, const UtString* base_path, MsgContext *message_context, int limitSchedCalls, bool verbose);

  //! DB-based constructor
  WtbExecuteQueue(WtbModel* model, MsgContext *message_context);

  //! Destructor.
  virtual ~WtbExecuteQueue(void);  

 public:
  typedef UtArray<WtbExecuteEvent *> ExecuteQueue;
  typedef Loop<ExecuteQueue> ExecuteQueueLoop;

 public:
  //! Print the contents of the queue to UtIO::cout().
  void print(void);

  //! Provide and iterator to the events in the queue.
  ExecuteQueueLoop loopEvents(void);

  //! Save the execute queue into a db file
  void writeDB(ZostreamDB& zout) const;
  //! Read the execute queue from a db file
  bool readDB(ZistreamDB& zin, bool verbose, SInt32 limitSchedCalls);

  //! Gather all data events for current time slice. index maintains
  // position in the queue
  bool gatherValueChanges(ExecuteQueue* eventQueue, UInt32& index);

  //! Get the event factory
  WtbExecuteEventFactory* getEventFactory() { return mEventFactory; }

  void appendTimeChange(const UInt64* curTime);
  //! Returns true if the signal actually changed
  bool appendInputChange(WfAbsSignal* sig, DynBitVectorFactory* valueCache);
  void appendNoXDrive(WfAbsSignal* sig); 
  bool appendScheduleCall(Wtb::ScheduleMode schedMode);
  void endQueuePopulate(bool verbose);
  void appendDumpOn();
  void appendPreScheduleCall();
  void appendPostScheduleCall();
  void appendDepComboScheduleCall();

  //! Return the size of the queue
  UInt32 size() const;

  //! Clear out the queue
  void clear();

  //! Get the number of sched calls in this queue.
  int getNumSchedCalls() const { return mNumSchedCalls; }

  //! Write the current vector queue to a vcd file
  bool writeQueueToVcd(const char* file) const;

 private:
  bool readQueueFromDB(ZistreamDB& zin, bool verbose, SInt32 limitSchedCalls);
  bool writeQueueToDB(ZostreamDB& zout) const;

  WtbModel* mModel;
  ExecuteQueue mQueue;
  WtbExecuteEventFactory * mEventFactory;
  int mNumSchedCalls;
  int mNumTimeEvents;
  int mNumDataEvents;
  int mLimitSchedCalls;

  void init();

  class QueueWave;
 private:
  WtbExecuteQueue(void);  
  WtbExecuteQueue(const WtbExecuteQueue&);
  WtbExecuteQueue& operator=(const WtbExecuteQueue&);
};


#endif
