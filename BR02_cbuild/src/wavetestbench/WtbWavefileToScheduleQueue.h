// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WTBWAVEFILETOSCHEDULEQUEUE_H_
#define __WTBWAVEFILETOSCHEDULEQUEUE_H_


#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/UtArray.h"
#include "util/UtHashSet.h"

#include "hdl/HdlId.h"
#include "exprsynth/ExprFactory.h"
#include "modshell/WtbApplication.h"
#include "iodb/IODB.h"

class MsgContext;
class WfAbsFileReader;
class WtbModel;
class WfAbsSignal;
class STSymbolTableNode;
class STAliasedLeafNode;
class ESFactory;
class CarbonExpr;
class UtUInt64Factory;
class IODBRuntime;
class DynBitVectorFactory;

/*!
  \file
  The WtbWavefileToScheduleQueue class.
*/

class WtbExprValueVCIter;
class WtbIdentVec;
class WtbSignalData;
class ZostreamDB;

//! WtbWavefileToScheduleQueue class
/*!
  This class helps WtbSignalQueue perfom the initial population of
  events from a wavefile.
*/
class WtbWavefileToScheduleQueue
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Constuct the object.
    \param wave_file The object that contains the waveforms.
    \param model The model that is being simulated.
    \param base_path The instance contained within the waveform data
    that is being simulated.
    \param message_context Context for error messages.
   */
  WtbWavefileToScheduleQueue(WfAbsFileReader* wave_file, WtbModel* model, const UtString* base_path, MsgContext *message_context, WtbApplication::Config* config,
                             UtUInt64Factory* timeFactory);

  //! Destructor.
  virtual ~WtbWavefileToScheduleQueue(void);

  //! Loop through all the bidi enables and create cwtb expressions
  /*!
    This must be called prior to loading the values.
  */
  void createBidiEnableExprs(DynBitVectorFactory* dynFactory);

  //! Add value associations to the enable expressions
  /*!
    This must be called after loading the values.
    This will add the list of needed values of idents to expressions
    and then evaluate the expressions, creating a value change
    list. The value change list can be used to decide whether or not
    to externally drive a primary bidi.
  */
  void setupBidiEnableExprs();
  
  //! Write out the clocks and the number of changes for each
  void writeCycleInfo(const char* file);

  //! Write clock info to db
  void writeClockInfo(ZostreamDB& out) const;

  //! Clear changes in SigDatas
  void clearChanges();

  //! Setup the waveform to read the values
  void watchSignals(bool doDeposits, bool cycleSched, bool noBidiEnables, DynBitVectorFactory* dynFactory, const char* refclk, bool depositsAsClocks);

  const WfAbsFileReader* getWaveFile() const { return mWaveFile; }

  WfAbsFileReader* getWaveFile() { 
    const WtbWavefileToScheduleQueue* me = const_cast<const WtbWavefileToScheduleQueue*>(this);
    return const_cast<WfAbsFileReader*>(me->getWaveFile());
  }
  
  typedef UtArray<WtbSignalData*> SigDataVec;

 private:
  WfAbsSignal * findWfAbsSignal(const STSymbolTableNode* node) const;

  void doBidiEnableWalk(STSymbolTableNode* bidi, CarbonExpr* enableExpr, DynBitVectorFactory* dynFactory, bool mapNameToWave);

  void watchSignalsWorker(IODBRuntime* iodb, IODB::NameSetLoop loop, bool isClock, bool cycleSched, DynBitVectorFactory* dynFactory);
  void watchDepositSignalsWorker(IODBRuntime* iodb, IODB::NameSet& storageClocks, bool inputFlow, bool cycleSched, DynBitVectorFactory* dynFactory);
  void watchBidiSignalsWorker(IODBRuntime* iodb, bool noBidiEnables, DynBitVectorFactory* dynFactory);
  void watchSignalsWorkerClockTree(IODBRuntime* iodb, bool cycleSched, DynBitVectorFactory* dynFactory);
  void markInput(STSymbolTableNode* node, WfAbsSignal* waveSig, bool isClock, bool cycleSched, DynBitVectorFactory* dynFactory, IODBRuntime* iodb);
  WtbSignalData* createSigData(const STSymbolTableNode* node, 
                               WfAbsSignal* waveSig, 
                               bool isInput, bool isClock, 
                               DynBitVectorFactory* dynFactory,
                               IODBRuntime* iodb);
  
  // Loops through the aliases of nodes until it finds a waveform
  // signal. Or NULL if nothing found. Also returns the node that the
  // waveSig actually corresponds to.
  WfAbsSignal* resolveNodeToWaveSig(IODBRuntime* iodb, STSymbolTableNode* node,
                                    STAliasedLeafNode** actualLeaf, bool isDesignStim = true);
  
  // Gets the enable expression for a config-spec'd enable.
  CarbonExpr* getConfigEnableExpression(const STAliasedLeafNode* enableNet, const HdlId& enableInfo, bool inverted, UInt32 bidiWidth);  

  // Sets the drive mask in sigData to mask of constant bits
  void markConstantBits(const STSymbolTableNode* node, 
                        WtbSignalData* sigData,
                        UInt32 bitwidth,
                        DynBitVectorFactory* 
                        dynFactory,
                        IODBRuntime* iodb);
  
 private:
  ESFactory mExprFactory;
  WfAbsFileReader* mWaveFile;
  WtbModel* mModel;
  const UtString* mBasePath;
  MsgContext *mMsgContext;
  WtbApplication::Config* mConfig;
  UtUInt64Factory* mTimeFactory;
  //  typedef UtHashMap<STSymbolTableNode*, WtbExprValueVCIter*> NodeExprMap;
  //NodeExprMap mBidiEnables;
  typedef UtArray<WtbIdentVec*> IdentVecArray;
  IdentVecArray mAllIdentVecs;


  SigDataVec mSigDatas;

private:
  WtbWavefileToScheduleQueue(void);
  WtbWavefileToScheduleQueue(const WtbWavefileToScheduleQueue&);
  WtbWavefileToScheduleQueue& operator=(const WtbWavefileToScheduleQueue&);

  class WtbTransWalker;
  friend class WtbTransWalker;

  class ConstMarkTransform;
  friend class ConstMarkTransform;
  class ConstMarkIdent;
};
#endif
