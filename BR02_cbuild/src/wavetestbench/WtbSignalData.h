// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _WTBSIGNALDATA_H_
#define _WTBSIGNALDATA_H_

#include "util/UtArray.h"
#include "util/UtHashSet.h"
#include "waveform/WfAbsFileReader.h"
#include "util/Loop.h"
#include "shell/CarbonDBRead.h"
#include "iodb/IODBTypes.h"

class STSymbolTableNode;
class CarbonExpr;

class WtbSignalData
{
  typedef UtArray<WfAbsSignal*> WfSigVec;
  
public:
  CARBONMEM_OVERRIDES

  struct BidiEnable
  {
    CARBONMEM_OVERRIDES

    BidiEnable(STSymbolTableNode* bidi, CarbonExpr* enable)
      : mBidi(bidi), mEnableExpr(enable)
    {}
    
    // This may be the entire bidi (same as the mCarbonNode) or it may
    // be a part of the bidi (a portsplit node, for example)
    STSymbolTableNode* mBidi;
    CarbonExpr* mEnableExpr;
    // The scratch value. This gets computed on every value change of
    // mCarbonNode. This is used to mask the drive value of the actual
    // enable signal value.
    DynBitVector mEnableDrive;
  };

  WtbSignalData(const STSymbolTableNode* node, bool isInput) :
    mCarbonNode(node),
    mCurrentVal(NULL), mCurrentDrv(NULL), mConstBitMask(NULL),
    mTypeEntry(NULL), mBomData(NULL), mNumChanges(0), 
    mIsInput(isInput), mIsClock(false), mIsUserRefClk(false),
    mIsDepositCombo(false), mIsAsync(false)
  {}

  ~WtbSignalData()
  {
    for (BidiEnableVecLoop p = loopBidiEnables();
         !p.atEnd(); ++p)
      delete *p;
  }

  typedef UtArray<BidiEnable*> BidiEnableVec;
  typedef Loop<BidiEnableVec> BidiEnableVecLoop;

  const STSymbolTableNode* getNode() const { return mCarbonNode; }

  STSymbolTableNode* getNode() { 
    const WtbSignalData* me = const_cast<const WtbSignalData*>(this);
    return const_cast<STSymbolTableNode*>(me->getNode());
  }

  bool isInput() const { return mIsInput; }
  
  void addEnableExpr(STSymbolTableNode* bidi, CarbonExpr* enableExpr) {
    BidiEnable* bidiEnable = new BidiEnable(bidi, enableExpr);
    mBidiEnables.push_back(bidiEnable);
  }
  
  BidiEnableVecLoop loopBidiEnables() {
    return BidiEnableVecLoop(mBidiEnables);
  }
  
  void addAffectedBidi(WfAbsSignal* sig)
  {
    mAffectedBidis.push_back(sig);
  }

  void getAffectedBidis(WfAbsSignalSet* sigSet) const
  {
    for (WfSigVec::const_iterator p = mAffectedBidis.begin(), 
           e = mAffectedBidis.end();
         p != e; ++p)
    {
      WfAbsSignal* elem = *p;
      sigSet->insert(elem);
    }
  }

  bool isBidi() const
  {
    return ! mBidiEnables.empty();
  }

  //! Only useful when scheduling by cycle
  void putIsClock(bool isClock)
  {
    mIsClock = isClock;
  }

  //! Only useful when scheduling by cycle
  bool isClock() const { 
    return mIsClock;
  }

  //! Used to calculate the speed of the model. 
  /*!
    Only one of these should ever exist.
  */
  bool isUserRefClk() const { 
    return mIsUserRefClk;
  }

  //! Set this signal as the user-specified reference clock
  void putIsUserRefClk(bool isRefClk)
  {
    mIsUserRefClk = isRefClk;
  }

  //! Used to determine if the depcombo sched is called
  bool isDepositCombo() const { 
    return mIsDepositCombo;
  }

  //! Set this signal as a deposit-combo signal
  void putIsDepositCombo(bool isDepCombo)
  {
    mIsDepositCombo = isDepCombo;
  }

  //! Is this an async signal
  bool isAsync() const { 
    return mIsAsync;
  }

  //! Set this signal as an async signal
  void putIsAsync(bool isAsync)
  {
    mIsAsync = isAsync;
  }
  
  UInt32 numChanges() const { return mNumChanges; }

  void incrChange() { ++mNumChanges; }

  void clearChanges() { mNumChanges = 0; }

  
  //! Returns true if the value or drive are different than current
  /*!
    Assumes all DynBitVector pointers are from the same factory.
  */
  bool isValueDifference(const DynBitVector* val, const DynBitVector* drv) const
  {
    return (val != mCurrentVal) || (drv != mCurrentDrv);
  }

  void putCurrentValue(const DynBitVector* val, const DynBitVector* drv)
  {
    mCurrentVal = val;
    mCurrentDrv = drv;
  }

  void putConstBitMask(const DynBitVector* bitMask)
  {
    mConstBitMask = bitMask;
  }

  void putTypeEntry(const IODBGenTypeEntry* typeEntry)
  {
    mTypeEntry = typeEntry;
  }

  void putShellDataBOM(const ShellDataBOM* bomdata) 
  {
    mBomData = bomdata;
  }

  bool isTristate() const {
    // default to true in case we are looking at a net that is
    // expressioned, in which case we would expect it to be a
    // tristate.
    bool ret = true;
    if (mTypeEntry && ! mTypeEntry->isTristate() && ! isBidi())
      ret = false;
    // This is too confusing putting it into one if stmt. So, I'm
    // breaking it out. An expressioned net may not have a bidi enable
    // expression, so we need to override the return value here with
    // true if the net is expressioned.
    if (! ret && mBomData && (mBomData->getExpr() != NULL))
      ret = true;
    return ret;
  }
  const DynBitVector* getConstBitMask() const
  {
    return mConstBitMask;
  }

private:
  const STSymbolTableNode* mCarbonNode;
  // Current value. NULL if not yet set.
  const DynBitVector* mCurrentVal;
  // Current drive. NULL if not yet set.
  const DynBitVector* mCurrentDrv;
  // Constant bit mask. NULL if not set
  const DynBitVector* mConstBitMask;
  // Type entry for this signal. NULL if not set.
  const IODBGenTypeEntry* mTypeEntry;
  // Shell data bom, if available. NULL if not set
  const ShellDataBOM* mBomData;
  
  WfSigVec mAffectedBidis;
  UInt32 mNumChanges;

  // The list of the enabling parts of this bidi. For a portsplit net
  // this could have multiple bidi enables depending on how many bits
  // are considered outputs and how many bits have enabled drivers.
  BidiEnableVec mBidiEnables;

  bool mIsInput;
  bool mIsClock;
  bool mIsUserRefClk;
  bool mIsDepositCombo;
  bool mIsAsync;
};

#endif
