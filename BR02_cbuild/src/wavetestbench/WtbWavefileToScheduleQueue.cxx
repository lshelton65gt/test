// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "WtbWavefileToScheduleQueue.h"
#include "WtbModel.h"
#include "WtbUtil.h"
#include "WtbSignalData.h"

#include "util/ShellMsgContext.h"
#include "util/UtIOStream.h"
#include "util/DynBitVector.h"
#include "util/UtUInt64Factory.h"
#include "util/Zstream.h"
#include "util/UtConv.h"
#include "util/UtOrder.h"       // For carbonPtrCompare()
#include "waveform/WfAbsFileReader.h"
#include "waveform/WfAbsSignal.h"
#include "waveform/WfAbsSignalVCIter.h"
#include "util/StringAtom.h"
#include "exprsynth/ExprFactory.h"
#include "shell/ShellSymNodeIdent.h"
#include "shell/CarbonDBRead.h"
#include "iodb/IODBRuntime.h"
#include "shell/ShellGlobal.h"
#include "hdl/HdlVerilogPath.h"

static void sFindSigAttributes(WtbSignalData* sigData, 
                               const ShellDataBOM* bomdata,
                               const IODB* iodb);

class WtbIdent : public CarbonIdent
{
public:
  CARBONMEM_OVERRIDES

#if !pfGCC_2
  using CarbonIdent::getNode;
#endif

  WtbIdent(bool isSigned, UInt32 bitSize, 
           STSymbolTableNode* node,
           const DynBitVector& useMask,
           bool mapNameToWave) : 
    CarbonIdent(isSigned, bitSize),
    mWaveSignal(NULL),
    mWaveValIter(NULL),
    mNode(node),
    mUseMask(useMask),
    mMapNameToWave(mapNameToWave)
  {}

  virtual ~WtbIdent() {
    delete mWaveValIter;
  }

  virtual const char* typeStr() const {
    return "WtbIdent";
  }

  virtual void print(bool, int indent) const
  {
    UtOStream& screen = UtIO::cout();
    for (int i = 0; i < indent; i++) {
      screen << " ";
    }
    screen << typeStr() << '(' << this << ") ";
    CarbonExpr::printSize(screen);
    screen << " : ";
    UtString name;
    mNode->compose(&name);  // (we have no testcase in almostall that checks this line)
    
    screen << name << UtIO::endl;
  }  

  virtual void composeIdent(ComposeContext* context) const
  {
    UtString* str = context->getBuffer();
    mNode->compose(str);  // (we have no testcase in almostall that checks this line)
  }

  virtual SignT evaluate(ExprEvalContext* evalContext) const 
  {
    // get the value for the net
    DynBitVector netValue;
    DynBitVector netDrive;
    ST_ASSERT(mWaveSignal, mNode);
    mWaveSignal->getLastValue(&netValue, &netDrive);
    ST_ASSERT(netValue.size() == mUseMask.size(), mNode);
    
    // Apply the usage mask over the bits. This compresses holes
    DynBitVector* value = evalContext->getValue();
    DynBitVector* drive = evalContext->getDrive();
    value->resize(getBitSize());
    drive->resize(getBitSize());
    drive->reset();
    value->reset();

    bool doDrive = netDrive.any();
    bool doValue = netValue.any();

    // only check bits if we need to.
    if (doDrive || doValue)
    {
      UInt32 writeIndex = 0;
      for (UInt32 readIndex = 0; readIndex < mUseMask.size(); ++readIndex) {
        if (mUseMask[readIndex] != 0) {
          if (doValue)
            (*value)[writeIndex] = netValue[readIndex];
          if (doDrive)
            (*drive)[writeIndex] = netDrive[readIndex];
          ++writeIndex;
        }
      }
    }
    return isSigned()? ePos: eUnsigned;
  }

  virtual AssignStat assign(ExprAssignContext*)
  {
    return eReadOnly;
  }

  virtual AssignStat assignRange(ExprAssignContext*, const ConstantRange&)
  {
    return eReadOnly;
  }

  virtual ptrdiff_t compare(const CarbonExpr* other) const 
  {
    const WtbIdent* ident = reinterpret_cast<const WtbIdent*>(other);
    ptrdiff_t cmp = carbonPtrCompare(mNode, ident->mNode);
    if (cmp == 0)
      cmp = mUseMask.compare(ident->mUseMask);
    return cmp;
  }
  
  virtual const STAliasedLeafNode* getNode(DynBitVector* usageMask) const 
  {
    *usageMask = mUseMask;
    return mNode->castLeaf();
  }

  void setWaveSignal(WfAbsSignal* signal)
  {
    mWaveSignal = signal;
  }

  WfAbsSignal* mWaveSignal;
  WfAbsSignalVCIter* mWaveValIter;
  STSymbolTableNode* mNode;

  bool needAdjustName() const {
    return mMapNameToWave;
  }

  //! Returns true if this ident represents the entire net
  virtual bool isWholeIdentifier() const { return true; }

private:
  DynBitVector mUseMask;
  bool mMapNameToWave;
};

class WtbIdentVec
{
public:
  CARBONMEM_OVERRIDES

  WtbIdentVec(STSymbolTableNode* node) : mBidiNode(node)
  {}

  ~WtbIdentVec() {}

  void insert(WtbIdent* ident)
  {
    mIdents.insert(ident);
  }

  void assocValsWithSignals(WfAbsFileReader* waveFile, const UtString* basePath, MsgContext* msgContext) 
  {
    for (IdentArray::iterator p = mIdents.begin(), e = mIdents.end();
         p != e; ++p)
    {
      WtbIdent* ident = *p;
      UtString sigName;
      if (ident->needAdjustName())
      {
        if (! WtbUtil::extractAdjustedName(ident->mNode, *basePath, &sigName))
          msgContext->WtbInvalidHdlName(sigName.c_str());
      }
      else
        WtbUtil::composeVerilogName(ident->mNode, &sigName);
      
      WfAbsSignal* signal = WtbUtil::findWfAbsSignalByName(sigName, waveFile, msgContext);
      
      ident->setWaveSignal(signal);
    }
  }

  STSymbolTableNode* mBidiNode;
  
private:
  typedef UtHashSet<WtbIdent*, HashPointer<WtbIdent*> > IdentArray;
  IdentArray mIdents;
};

class WtbWavefileToScheduleQueue::WtbTransWalker : public CopyWalker
{
public:
  CARBONMEM_OVERRIDES

  WtbTransWalker(ESFactory* factory, STSymbolTable* table, IODBRuntime* iodb, MsgContext* msgContext,
                 WtbIdentVec* identVec, WfAbsFileReader* wave_file, const UtString* basePath, bool mapNameToWave, WfAbsSignal* bidiSig, 
                 DynBitVectorFactory* dynFactory,
                 WtbWavefileToScheduleQueue* waveTransform) : 
    CopyWalker(factory), mSymTab(table), mIODB(iodb), mMsgContext(msgContext), mIdentVec(identVec), mWaveFile(wave_file), mBasePath(basePath), mBidi(bidiSig),
    mDynFactory(dynFactory), mWaveTransform(waveTransform),
    mMapNameToWave(mapNameToWave)
  {}
  
  virtual ~WtbTransWalker() {}
 
  virtual void visitIdent(CarbonIdent* ident) { 
    CarbonExpr* transExpr = ownExpr(ident);
    // To allow us to continue, if the transExpr is NULL (not found in
    // the waveform, just use skip this one and use the original
    // ident. We will error out before using the expr anyway.
    CarbonIdent* transIdent = NULL;
    if (transExpr)
    {
      transIdent = transExpr->castIdent();
      FUNC_ASSERT(transIdent, transExpr->printVerilog());
    }
    else 
      transIdent = ident;

    pushResult(transIdent);
    //CarbonTransformWalker::visitIdent(transIdent); 
  }

  virtual void visitConst(CarbonConst* cConst) { 
    CarbonExpr* transExpr = ownExpr(cConst);
    CarbonConst* transConst = transExpr->castConst();
    FUNC_ASSERT(transConst, transExpr->printVerilog());
    pushResult(transConst);
    //CarbonTransformWalker::visitConst(transConst); 
  }

  virtual void visitConstXZ(CarbonConstXZ* cConstXZ) {
    CarbonExpr* transExpr = ownExpr(cConstXZ);
    CarbonConstXZ* transConst = transExpr->castConstXZ();
    FUNC_ASSERT(transConst, transExpr->printVerilog());
    pushResult(transConst);
    //CarbonTransformWalker::visitConstXZ(transConst); 
  }

private:
  STSymbolTable* mSymTab;
  IODBRuntime* mIODB;
  MsgContext* mMsgContext;
  WtbIdentVec* mIdentVec;
  WfAbsFileReader* mWaveFile;
  const UtString* mBasePath;
  WfAbsSignal* mBidi;
  DynBitVectorFactory* mDynFactory;
  WtbWavefileToScheduleQueue* mWaveTransform;
  bool mMapNameToWave;

  CarbonExpr* ownExpr(CarbonExpr* exprSrc) {
    bool isSigned = exprSrc->isSigned();
    UInt32 bitSize = exprSrc->getBitSize();
    CarbonIdent* ident = exprSrc->castIdent();
    CarbonConst* aConst = exprSrc->castConst();
    CarbonConstXZ* aConstXZ = exprSrc->castConstXZ();
    CarbonExpr* ret = NULL;
    WfAbsSignal* waveSig = NULL;
    if (ident)
    {
      DynBitVector useMask;
      STSymbolTableNode* node = ident->getNode(&useMask);

      UtString waveSignalName;
      if (mMapNameToWave)
      {
        STAliasedLeafNode* actualLeaf;
        waveSig = mWaveTransform->resolveNodeToWaveSig(mIODB, node, &actualLeaf, false);
        if (waveSig)
          node = actualLeaf;
      }
      else
      {
        WtbUtil::composeVerilogName(node, &waveSignalName);
        waveSig = mWaveFile->watchSignal(waveSignalName);
      }
      
      if (! waveSig)
      {
        UtString bidName;
        mBidi->getName(&bidName);
        mMsgContext->WtbEnableNetMissing(waveSignalName.c_str(), bidName.c_str());
      }
      else
      {
        WtbSignalData* sigData = (WtbSignalData*) waveSig->getUserData();
        if (sigData == NULL)
        {
          // This is a non-input. Note that the type entry will NOT be
          // needed for this. Passing in the IODB mainly for future use.
          sigData = mWaveTransform->createSigData(node, waveSig, false, false, mDynFactory, mIODB);
        }
        
        sigData->addAffectedBidi(mBidi);
        
        WtbIdent* transIdent = new WtbIdent(isSigned, bitSize, node, useMask, mMapNameToWave);
        bool added;
        ret = mFactory->createIdent(transIdent, added);
        if (! added)
          delete transIdent;
        
        // must use ret, *NOT* transIdent because it could be deleted.
        mIdentVec->insert(reinterpret_cast<WtbIdent*>(ret));
      }
    }
    else if (aConstXZ)
    {
      DynBitVector val;
      DynBitVector drv;
      aConstXZ->getValue(&val, &drv);
      ret = mFactory->createConstXZ(val, drv, isSigned? CarbonExpr::ePos: CarbonExpr::eUnsigned, bitSize);
    }
    else if (aConst)
    {
      DynBitVector val;
      aConst->getValue(&val);
      ret = mFactory->createConst(val, isSigned? CarbonExpr::ePos: CarbonExpr::eUnsigned, bitSize);
    }

    return ret;
  }
};

class WtbWavefileToScheduleQueue::ConstMarkIdent : public SymTabIdent
{
public:
  CARBONMEM_OVERRIDES

  ConstMarkIdent(STAliasedLeafNode * node, const DynBitVector& usageMask, UInt32 bitSize) :
    SymTabIdent(node, usageMask, bitSize), mConstDrvMask(NULL)
  {}

  SignT evaluate(ExprEvalContext* evalContext) const
  {
    DynBitVector* value = evalContext->getValue();
    DynBitVector* drive = evalContext->getDrive();
    value->resize(getBitSize());
    drive->resize(getBitSize());
    drive->reset();
    value->reset();
    
    if (mConstDrvMask)
      *drive = *mConstDrvMask;
    return eUnsigned;
  };

  size_t hash() const
  {
    size_t val = SymTabIdent::hash();
    val >>= 2;
    return val ^ (size_t) mConstDrvMask;
  }

  ptrdiff_t compare(const CarbonExpr* other) const
  {
    ptrdiff_t ret = SymTabIdent::compare(other);
    if (ret == 0)
    {
      const ConstMarkIdent* castOther = (const ConstMarkIdent*)(const void*) other;
      ret = carbonPtrCompare(mConstDrvMask, castOther->mConstDrvMask);
    }
    return ret;
  }

  void putConstDrvMask(const DynBitVector* drvMask) {
    mConstDrvMask = drvMask;
  }

private:
  const DynBitVector* mConstDrvMask;
};

class WtbWavefileToScheduleQueue::ConstMarkTransform : public CopyWalker
{
public:
  CARBONMEM_OVERRIDES

  ConstMarkTransform(DynBitVectorFactory* dynFactory, IODBRuntime* iodb) : 
    CopyWalker(&mPrivESFactory), mDynFactory(dynFactory), mIODB(iodb), 
    mNumConsts(0)
  {}
  
  virtual CarbonExpr* transformIdent(CarbonIdent* ident)
  {
    DynBitVector useMask;
    STAliasedLeafNode* leaf = ident->getNode(&useMask);
    CE_ASSERT(leaf, ident);

    UInt32 bitSize = ident->getBitSize();
    
    ConstMarkIdent* transIdent = new ConstMarkIdent(leaf, useMask, 
                                                    bitSize);
    
    
    // if this has a constant override, get the mask:
    const DynBitVector* constBitMask = mIODB->getConstNetBitMask(leaf);
    if (constBitMask)
    {
      DynBitVector drv;
      drv.resize(bitSize);
      CarbonValRW::setControlBits(drv.getUIntArray(), 
                                  constBitMask->getUIntArray(), bitSize);
      
      const DynBitVector* drvAlloc = mDynFactory->alloc(drv);
      transIdent->putConstDrvMask(drvAlloc);
      ++mNumConsts;
    }
    
    bool added;
    CarbonIdent* ret = mPrivESFactory.createIdent(transIdent, added);
    if (! added)
      delete transIdent;
    return ret;
  };

  UInt32 numConsts() const { return mNumConsts; }
  
private:
  ESFactory mPrivESFactory;
  DynBitVectorFactory* mDynFactory;
  IODBRuntime* mIODB;
  UInt32 mNumConsts;
};

class SigDataFill : public CarbonExprWalker
{
public:
  SigDataFill(IODBRuntime* iodb, WtbSignalData* sigData) : 
    mIODB(iodb), mSigData(sigData)
  {}
  
  virtual ~SigDataFill() {}
  
  virtual void visitIdent(CarbonIdent* ident)
  {
    STSymbolTable* symTab = mIODB->getDesignSymbolTable();
    DynBitVector useMask;
    STSymbolTableNode* identNode = ident->getNode(&useMask);
    STSymbolTableNode* designNode = symTab->safeLookup(identNode);
    ST_ASSERT(designNode, identNode);
    
    const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(designNode);
    sFindSigAttributes(mSigData, bomdata, mIODB);
  }

private:
  IODBRuntime* mIODB;
  WtbSignalData* mSigData;
};


static void sFindSigAttributes(WtbSignalData* sigData, 
                               const ShellDataBOM* bomdata,
                               const IODB* iodb)
{
  const SCHSignature* signature = bomdata->getSCHSignature();
  bool isAsync = iodb->hasOutputEvent(signature);
  sigData->putIsAsync(isAsync);
  // Needed for depositables
  sigData->putIsDepositCombo(bomdata->isComboRun());
}

WtbWavefileToScheduleQueue::WtbWavefileToScheduleQueue(WfAbsFileReader* wave_file, WtbModel* model, const UtString* base_path, MsgContext *message_context, WtbApplication::Config* config, UtUInt64Factory* timeFactory)
  : mWaveFile(wave_file), mModel(model), mBasePath(base_path), mMsgContext(message_context), mConfig(config), mTimeFactory(timeFactory)
{
}


WtbWavefileToScheduleQueue::~WtbWavefileToScheduleQueue(void)
{
  IdentVecArray::iterator p, e = mAllIdentVecs.end();
  for (IdentVecArray::iterator p = mAllIdentVecs.begin(), e = mAllIdentVecs.end(); p != e; ++p)
    delete *p;
  
  for (SigDataVec::iterator p = mSigDatas.begin(), e = mSigDatas.end();
       p != e; ++p)
    delete *p;

}

void WtbWavefileToScheduleQueue::createBidiEnableExprs(DynBitVectorFactory* dynFactory)
{
  IODBRuntime* iodb = mModel->getIODBRuntime();
  IODB::NameExprMapLoop loop = iodb->loopEnableExprs();
  INFO_ASSERT(mAllIdentVecs.empty(), "createBidiEnableExprs called twice.");
  
  typedef UtHashSet<const STSymbolTableNode*> NodeSet;
  NodeSet bidiSet;
  HdlId enInfo;
  CarbonExpr* enableExpr = NULL;
  
  while (! loop.atEnd())
  {
    bool mapNameToWave = true;
    if (mConfig)
    {
      bool inverted = false;
      STSymbolTableNode* key = loop.getKey();
      const STAliasedLeafNode* enableNet = mConfig->getEnable(key, &enInfo, &inverted);
      if (enableNet)
      {
        const IODBIntrinsic* intrinsic = iodb->getLeafIntrinsic(key->castLeaf());
        ST_ASSERT(intrinsic, key);
        UInt32 bidiWidth = intrinsic->getWidth();
        enableExpr = getConfigEnableExpression(enableNet, enInfo, inverted, bidiWidth);
        mapNameToWave = false;
      }
      else 
        enableExpr = loop.getValue();
    }
    else
      enableExpr = loop.getValue();

    INFO_ASSERT(enableExpr, "Design db problem. NULL found in enable expression list, or it was explicitly passed over.");
    bidiSet.insert(loop.getKey());
    doBidiEnableWalk(loop.getKey(), enableExpr, dynFactory, mapNameToWave);
    
    ++loop;
  }
  
  if (mConfig)
  {
    const HdlHierPath* pather = mModel->getSymTab()->getHdlHier();
    UtString name;
    
    IODB::NameSetLoop bidis = iodb->loopBidis();
    
    // First, get uncovered bidis in the config file
    while (! bidis.atEnd())
    {
      STSymbolTableNode * node = *bidis;
      if (bidiSet.find(node) == bidiSet.end())
      {
        bool inverted = false;
        const STAliasedLeafNode* enableNode = mConfig->getEnable(node, &enInfo, &inverted);
        if (enableNode)
        {
          pather->compPathHier(node, &name, NULL);
          mMsgContext->WtbEnableForNonEnableBidi(name.c_str());

          const IODBIntrinsic* intrinsic = iodb->getLeafIntrinsic(node->castLeaf());
          ST_ASSERT(intrinsic, node);
          UInt32 bidiWidth = intrinsic->getWidth();
          enableExpr = getConfigEnableExpression(enableNode, enInfo, inverted, bidiWidth);
          ST_ASSERT(enableExpr, enableNode);
          
          bidiSet.insert(node);
          doBidiEnableWalk(node, enableExpr, dynFactory, false);
        }
      }
      ++bidis;
    }
    
    // Now, warn about unused enables.
    WtbApplication::Config::SigEnableLoop configLoop = mConfig->loopEnabledSignals();
    while (! configLoop.atEnd())
    {
      if (bidiSet.find(configLoop.getKey()) == bidiSet.end())
      {
        pather->compPathHier(configLoop.getKey(), &name, NULL);
        mMsgContext->WtbUnusedEnable(name.c_str());
      }
      ++configLoop;
    }
  }
}

CarbonExpr* WtbWavefileToScheduleQueue::getConfigEnableExpression(const STAliasedLeafNode* enableNet, const HdlId& enableInfo, bool inverted, UInt32 bidiWidth)
{
  // Need the type entry for this node from the db to get the bitsize.
  const HdlHierPath* pather = mModel->getSymTab()->getHdlHier();
  UtString name;
  pather->compPathHier(enableNet, &name, &enableInfo);
  UInt32 bitSize = 1;

  UtString sigMsg;
  if (! mWaveFile->maybeAddWatchSignal(name))
    mMsgContext->WtbEnableSigNotInWavefile(name.c_str());

  WfAbsSignal* sig = mWaveFile->findSignal(name, &sigMsg);
  ST_ASSERT(sig, enableNet);
  
  bitSize = sig->getWidth();
  SInt32 waveLsb = sig->getLsb();
  SInt32 waveMsb = sig->getMsb();

  // no longer need sig object
  sig = NULL;

  DynBitVector usageVec(bitSize);
  // Using entire enable. Partselect and bitselect will use the
  // appropriate carbonExpr.
  usageVec.set();
  
  // check that the user specified correctly
  HdlId::Type userType = enableInfo.getType();
  SInt32 userLsb, userMsb, index;
  bool isGood = true;
  SInt32 waveHi = waveMsb;
  SInt32 waveLo = waveLsb;
  if (waveMsb < waveLsb)
  {
    waveHi = waveLsb;
    waveLo = waveMsb;
  }

  switch (userType)
  {
  case HdlId::eVectBitRange:
    userLsb = enableInfo.getVectLSB();
    userMsb = enableInfo.getVectMSB();

    if (ShellGlobal::carbonTestRangeContext(int(waveMsb), int(waveLsb), int(userMsb), int(userLsb), mMsgContext) == eCarbon_ERROR)
      isGood = false;
    break;
  case HdlId::eVectBit:
    index = enableInfo.getVectIndex();
    if ((index < waveLo) || (index > waveHi))
    {
      mMsgContext->SHLValueIndexOutOfBounds(index);
      isGood = false;
    }
  case HdlId::eScalar:
    break;
  case HdlId::eArrayIndex:
    // cannot happen
    ST_ASSERT(enableInfo.getType() != HdlId::eArrayIndex, enableNet);
    break;
  case HdlId::eInvalid:
    // cannot happen
    ST_ASSERT(enableInfo.getType() != HdlId::eInvalid, enableNet);
    break;
  }

  // We have to give up here if we are in a bad state
  if (! isGood)
    exit(1);
  
  // This is needed to create the ident in a consistent manner
  IODBRuntime* iodbRuntime = mModel->getIODBRuntime();

  CarbonIdent* ident = iodbRuntime->createExprIdent(enableNet, bitSize, &usageVec, CbuildShellDB::eSymTabIdent, NULL);
  ESFactory* dbExprFactory = iodbRuntime->getExprFactory();
  
  /*
    now, create an expression with the ident of the form:

    isContinuouslyDrivenInternally = ident != 0;

    If the ident is a partselect or bitselect, create that first.
  */
  CarbonExpr* finalIdent = ident;
  ConstantRange range;
  CarbonExpr* tmp;
  SInt64 tmpVal;
  switch(userType)
  {
  case HdlId::eVectBitRange:
    range.setLsb(enableInfo.getVectLSB());
    range.setMsb(enableInfo.getVectMSB());
    finalIdent = dbExprFactory->createPartsel(ident, range, range.getLength(),
                                              false); // not signed
    break;
  case HdlId::eVectBit:
    tmpVal = SInt64(enableInfo.getVectIndex());
    tmp = mExprFactory.createConst(tmpVal, 32, false); // not signed
    finalIdent = dbExprFactory->createBinaryOp(CarbonExpr::eBiBitSel, ident, tmp, 1,
                                               false, false); // not signed
    break;
  case HdlId::eScalar:
    break;
  case HdlId::eArrayIndex:
    // cannot get here, already checked above
    ST_ASSERT(enableInfo.getType() != HdlId::eArrayIndex, enableNet);
    break;
  case HdlId::eInvalid:
    // cannot get here, already checked above
    ST_ASSERT(enableInfo.getType() != HdlId::eInvalid, enableNet);
    break;
  }

  CarbonExpr* ret = NULL;
  if (inverted)
    ret = dbExprFactory->createEqZero(finalIdent, 1);
  else
    ret = dbExprFactory->createNeqZero(finalIdent, 1);

  if (bidiWidth > 1)
  {
    // create a concat with a repeat count
    CarbonExprVector concatElem;
    concatElem.push_back(ret);
    ret = dbExprFactory->createConcatOp(&concatElem, bidiWidth, bidiWidth, false);
  }
  return ret;
}

void WtbWavefileToScheduleQueue::doBidiEnableWalk(STSymbolTableNode* bidi, CarbonExpr* enableExpr, DynBitVectorFactory* dynFactory, bool mapNameToWave)
{
  WtbIdentVec* identVec = new WtbIdentVec(bidi);
  mAllIdentVecs.push_back(identVec);

  UtString waveSignalName;
  UtString dummy;
  if (! WtbUtil::extractAdjustedName(identVec->mBidiNode, *mBasePath, &waveSignalName))
    mMsgContext->WtbInvalidHdlName(waveSignalName.c_str());

  WfAbsSignal* bidiSig = mWaveFile->findSignal(waveSignalName, &dummy);
  // This signal may not have been found in the waveform. We would
  // have warned about that when the port list was put into the
  // waveform watch list
  if (bidiSig)
  {
    WtbSignalData* bidiSigData = (WtbSignalData*) bidiSig->getUserData();
    ST_ASSERT(bidiSigData != NULL, bidi);
  
#ifdef VERBOSE_BIDI
    {
      UtString waveSignalName;
      WtbUtil::extractAdjustedName(identVec->mBidiNode, mBasePath, waveSignalName);
      
      UtIO::cout() << "Bidi " << waveSignalName << ": "; 
      enableExpr->print();
    }
#endif
    
    WtbTransWalker walker(&mExprFactory, mModel->getSymTab(), mModel->getIODBRuntime(), mMsgContext, identVec, mWaveFile, mBasePath, mapNameToWave, bidiSig, dynFactory, this);
    walker.visitExpr(enableExpr);
    CarbonExpr* xformEnableExpr = walker.getResult();
    bidiSigData->addEnableExpr(bidi, xformEnableExpr);
    
    identVec->assocValsWithSignals(mWaveFile, mBasePath, mMsgContext);
  }
}

void WtbWavefileToScheduleQueue::watchSignals(bool doDeposits, bool cycleSched, bool noBidiEnables, DynBitVectorFactory* dynFactory, const char* refclk, bool depositsAsClocks)
{
  IODBRuntime* iodb = mModel->getIODBRuntime();
  // very important to do the clocks first.
  watchSignalsWorker(iodb, iodb->loopPrimaryClocks(), true, cycleSched, dynFactory);
  
  if (doDeposits)
  {
    /* and deposits. Treat those as data inputs. 
       Any deposits that are clocks were handled in the primary clocks
       set.
       If this is an input flow vector set then treat all deposits as
       clocks, since the depcombo schedule is run in the clock schedule.
    */

    // first create a set of clocks based on the storage node
    IODB::NameSet storageClocks;
    if (! depositsAsClocks)
    {
      for (IODB::NameSetLoop clks = iodb->loopClocks(); ! clks.atEnd(); ++clks)
      {
        STAliasedLeafNode* clkLeaf = (*clks)->castLeaf();
        ST_ASSERT(clkLeaf, *clks);
        storageClocks.insert(clkLeaf->getStorage());
      }
    }
    
    watchDepositSignalsWorker(iodb, storageClocks, depositsAsClocks, cycleSched, dynFactory);
  }
  
  watchSignalsWorker(iodb, iodb->loopInputs(), false, cycleSched, dynFactory);
  watchBidiSignalsWorker(iodb, noBidiEnables, dynFactory);

  if (refclk)
  {
    UtString sigName(refclk);
    WfAbsSignal* waveSig = mWaveFile->watchSignal(sigName);
    if (waveSig)
    {
      // if this signal is already watched it will have a sigdata
      WtbSignalData* refclkSigData = (WtbSignalData*) waveSig->getUserData();
      if (! refclkSigData)
      {
        // it isn't already watched. Add a sigdata as a non-clock,
        // non-input (we don't want to this to affect vector creation)

        // Use the symtab node from the waveform.
        const STSymbolTableNode* refNode = waveSig->getSymNode();
        refclkSigData = createSigData(refNode, waveSig, false, false, dynFactory, iodb);
      }
      
      // mark the refclkSigData as a user refclk
      refclkSigData->putIsUserRefClk(true);
    }
    else 
      mMsgContext->WtbRefClkNotInWavefile(sigName.c_str());
  }
}

static bool sIsComplexBid(const STSymbolTableNode* node)
{
  // if a node has an expression it is considered complex. And
  // currently, we only expressionize split ports. Here we only want
  // to avoid the actual port split nets. These aren't 'expressioned'
  // but are described by backpointers.
  const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);
  return (! bomdata->isExpression() && (bomdata->getExpr() != NULL));
}

static bool sCheckNotRemoved(const STSymbolTableNode* node, 
                             MsgContext* msgContext)
{
  const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);
  int typeTag = bomdata->getTypeTag();
  bool isRemoved = (int(bomdata->getStorageOffset()) == -1) &&
    ! bomdata->isExpression() && ! CbuildShellDB::isConstantType(typeTag);
  if (isRemoved)
  {
    UtString signal_name;
    ShellSymTabBOM::composeName(node, &signal_name, false, true);
    msgContext->SHLOptimizedNet(signal_name.c_str());
  }
  return ! isRemoved;
}

void WtbWavefileToScheduleQueue::watchSignalsWorkerClockTree(IODBRuntime* iodb, bool cycleSched, DynBitVectorFactory* dynFactory)
{
  for (IODB::NameSetLoop loop = iodb->loopClockTree(); ! loop.atEnd(); ++loop)
  {
    STSymbolTableNode* node = *loop;

    // clocktree nets can be non-inputs.
    if ((iodb->isPrimaryInput(node) || iodb->isPrimaryBidirect(node)) 
        && (! iodb->isFullyConstant(node->castLeaf())) &&
        sCheckNotRemoved(node, mMsgContext))
    {
      STAliasedLeafNode* actualLeaf;
      WfAbsSignal* waveSig = resolveNodeToWaveSig(iodb, node, &actualLeaf);
      if (waveSig) 
        markInput(actualLeaf, waveSig, true, cycleSched, dynFactory, iodb);
    }
  }
}

void WtbWavefileToScheduleQueue::watchSignalsWorker(IODBRuntime* iodb, IODB::NameSetLoop loop, bool isClock, bool cycleSched, DynBitVectorFactory* dynFactory)
{
  STSymbolTableNode* node;
  while (loop(&node))
  {
    if (! iodb->isFullyConstant(node->castLeaf()) && sCheckNotRemoved(node, mMsgContext))
    {
      STAliasedLeafNode* actualLeaf;
      WfAbsSignal* waveSig = resolveNodeToWaveSig(iodb, node, &actualLeaf);
      if (waveSig)
        markInput(actualLeaf, waveSig, isClock, cycleSched, dynFactory, iodb);
    }
  } // while
}

WfAbsSignal* WtbWavefileToScheduleQueue::resolveNodeToWaveSig(IODBRuntime*, STSymbolTableNode* node, STAliasedLeafNode** actualLeafPtr,
                                                              bool isDesignStim)
{
  // check if we should ignore this signal
  if (isDesignStim && mConfig && mConfig->isIgnore(node))
  {
    UtString buf;
    ShellSymTabBOM::composeName(node, &buf, false, true);
    mMsgContext->WtbIgnoringDesignStim(buf.c_str());
    return NULL;
  }
  
  STAliasedLeafNode* leaf = node->castLeaf();
  *actualLeafPtr = leaf;
  if (! leaf)
    return NULL;

  UtString signal_name;

  STAliasedLeafNode* origLeaf = leaf;
  WfAbsSignal* waveSig = NULL;
  do {
    if (WtbUtil::extractAdjustedName(leaf, *mBasePath, &signal_name))
    {
      waveSig = mWaveFile->watchSignal(signal_name);
      if (waveSig)
        *actualLeafPtr = leaf;
    }
    leaf = leaf->getAlias();
  } while ((waveSig == NULL) && (leaf != origLeaf));
  
  if (! waveSig) 
  {
    (void) WtbUtil::extractAdjustedName(leaf, *mBasePath, &signal_name);
    mMsgContext->CantFindSignalInWavefile(signal_name.c_str());
  }
  return waveSig;
}

void WtbWavefileToScheduleQueue::markConstantBits(const STSymbolTableNode* node, 
                                                  WtbSignalData* sigData,
                                                  UInt32 bitwidth,
                                                  DynBitVectorFactory* 
                                                  dynFactory,
                                                  IODBRuntime* iodb)
{
  const ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);
  const DynBitVector* drvMask = NULL;
  if (bomdata->isExpression())
  {
    CarbonExpr* descExpr = const_cast<CarbonExpr*>(bomdata->getExpr());
    ConstMarkTransform transWalk(dynFactory, iodb);
    transWalk.visitExpr(descExpr);
    // Always get the result. Otherwise, the walker doesn't think the
    // walk is complete.
    CarbonExpr* transExpr = transWalk.getResult();
    if (transWalk.numConsts() > 0)
    {
      ExprEvalContext evalContext;
      if (transExpr->evaluate(&evalContext) == CarbonExpr::eBadSign)
      {
        transExpr->print();
        mMsgContext->WtbExpressionEvalFailure();
      }
      
      drvMask = dynFactory->alloc(evalContext.getDriveRef());
    }
  }
  else
  {
    const DynBitVector* constBitMask = iodb->getConstNetBitMask(node->castLeaf());
    if (constBitMask)
    {
      DynBitVector drv;
      drv.resize(bitwidth);
      CarbonValRW::setControlBits(drv.getUIntArray(), 
                                  constBitMask->getUIntArray(), 
                                  bitwidth);
      drvMask = dynFactory->alloc(drv);
    }
  }
  sigData->putConstBitMask(drvMask);
}

WtbSignalData* 
WtbWavefileToScheduleQueue::createSigData(const STSymbolTableNode* node, 
                                          WfAbsSignal* waveSig, 
                                          bool isInput, bool isClock, 
                                          DynBitVectorFactory* dynFactory,
                                          IODBRuntime* iodb)
{
  WtbSignalData* sigData = new WtbSignalData(node, isInput);
  sigData->putIsClock(isClock);
  mSigDatas.push_back(sigData);
  waveSig->putUserData(sigData);
  
  if (isInput)
  {
    // We only care about the typeEntry for things that we are
    // driving. Enable nets may not exist in the symboltable, and we
    // only read those.
    const IODBGenTypeEntry* typeEntry = iodb->getType(node);
    sigData->putTypeEntry(typeEntry);
    ShellDataBOM* bomdata = ShellSymTabBOM::getLeafBOM(node);
    sigData->putShellDataBOM(bomdata);
    markConstantBits(node, sigData, waveSig->getWidth(), dynFactory, iodb);
    
    // Mark needed attributes of the signal
    if (bomdata->isExpression())
    {
      SigDataFill walk(iodb, sigData);
      walk.visitExpr(bomdata->getExpr());
    }
    else
      sFindSigAttributes(sigData, bomdata, iodb);
  }

  return sigData;
}


void WtbWavefileToScheduleQueue::markInput(STSymbolTableNode* node, 
                                           WfAbsSignal* waveSig, 
                                           bool isClock, bool cycleSched, 
                                           DynBitVectorFactory* dynFactory,
                                           IODBRuntime* iodb)
{
  if (mConfig)
  {
    // if we specified this in a configuration as a data or a clock,
    // then fix the isClock boolean
    if (mConfig->isClock(node))
      isClock = true;
    else if (mConfig->isData(node))
      isClock = false;
  }

  if (isClock && sIsComplexBid(node))
  {
    if (cycleSched)
    {
      UtString buf;
      ShellSymTabBOM::composeName(node, &buf, false, true);
      mMsgContext->WtbComplexBidiInClockTree(buf.c_str());
    }
  }
  else if (waveSig->getUserData() == NULL)
  {
    if (mModel->findNet(node))
    {
      createSigData(node, waveSig, true, isClock, dynFactory, iodb);
      
      if (isClock && cycleSched)
      {
        UtString name;
        waveSig->getName(&name);
        mMsgContext->WtbCycleController(name.c_str());
      }
    } // if net
  } // else if
}

void WtbWavefileToScheduleQueue::watchDepositSignalsWorker(IODBRuntime* iodb, IODB::NameSet& storageClocks, 
                                                           bool depositsAsClocks, bool cycleSched, 
                                                           DynBitVectorFactory* dynFactory)
{
  for (IODB::NameSetLoop loop = iodb->loopDeposit(); ! loop.atEnd(); ++loop)
  {
    STSymbolTableNode* node = *loop;
    if (! iodb->isFullyConstant(node->castLeaf()) && sCheckNotRemoved(node, mMsgContext))
    {
      STAliasedLeafNode* actualLeaf;
      WfAbsSignal* waveSig = resolveNodeToWaveSig(iodb, node, &actualLeaf);
      if (waveSig)
      {
        /* 
           We've already found the clocks, including depositables that
           are clocks. So, we want to mark deposits as inputs unless
           we are doing input flow or old deposit behavior. We may
           also have a depositable of a clock not in a data path,
           which may not appear in the primary clocks
           set:
           test/cust/lsi/cw000506. cw000506.cw000506_acore.RECVA1.clk
           is a clock driven by a depositable, but is not in the
           primary clocks set because the depositable doesn't drive a
           data pin.
        */
        bool isClock = depositsAsClocks || (storageClocks.count(actualLeaf->getStorage()) != 0);
        markInput(actualLeaf, waveSig, isClock, cycleSched, dynFactory, iodb);
      }
    }
  }
}

void WtbWavefileToScheduleQueue::watchBidiSignalsWorker(IODBRuntime* iodb, bool /*noBidiEnables*/, DynBitVectorFactory* dynFactory)
{
  STSymbolTableNode* node;
  UtString signal_name;
  
  for (IODB::NameSetLoop loop = iodb->loopBidis(); ! loop.atEnd(); ++loop)
  {
    node = *loop;
    if (! iodb->isFullyConstant(node->castLeaf()) && 
        sCheckNotRemoved(node, mMsgContext))
    {
      STAliasedLeafNode* actualLeaf;
      WfAbsSignal* waveSig = resolveNodeToWaveSig(iodb, node, &actualLeaf);
      if (waveSig)
      {
        if (waveSig->getUserData() == NULL)
        {
          // This is not a clock. Would have been caught in clock
          // loop.

          createSigData(actualLeaf, waveSig, true, false, dynFactory, iodb);
        } // if
      } // if
    } // if not constant
  } // for
}

void WtbWavefileToScheduleQueue::writeCycleInfo(const char* file)
{
  UtOBStream cycleFile(file);
  if (cycleFile.bad())
    mMsgContext->WtbCannotOpenDBForWrite(file, cycleFile.getErrmsg());
  else
  {
    bool firstReport = true;
    HdlVerilogPath pather;
    for (SigDataVec::iterator p = mSigDatas.begin(), e = mSigDatas.end();
         p != e; ++p)
    {
      WtbSignalData* sigData = *p;
      if (sigData->isClock())
      {
        if (firstReport)
        {
          cycleFile << "Clock/Reset\t\tNumChanges\n\n";
          firstReport = false;
        }

        UtString buffer;
        pather.compPathHier(sigData->getNode(), &buffer);
        cycleFile << buffer << "\t\t" << sigData->numChanges() << UtIO::endl;
      }
    }
  }
}

void WtbWavefileToScheduleQueue::writeClockInfo(ZostreamDB& out) const 
{
  SigDataVec tmpVec;
  WtbSignalData* refClk = NULL;
  for (SigDataVec::const_iterator p = mSigDatas.begin(), e = mSigDatas.end();
       p != e; ++p)
  {
    WtbSignalData* sigData = *p;
    if (sigData->isClock() || sigData->isUserRefClk())
      tmpVec.push_back(sigData);

    if (sigData->isUserRefClk())
      refClk = sigData;
  }
  
  HdlVerilogPath pather;
  out << tmpVec.size();
  for (SigDataVec::const_iterator p = tmpVec.begin(), e = tmpVec.end();
       p != e; ++p)
  {
    const WtbSignalData* sigData = *p;
    UtString name;
    const STSymbolTableNode* node = sigData->getNode();
    pather.compPathHier(node, &name);
    out << name;
    out << sigData->numChanges();
  }

  /*
    Write out the reference clock specified by the user. If the user
    didn't specify one, it will be empty and the fastest clock will
    be used (or the reference clock specified on the command line
    when reading in a cva file).
  */
  UtString refclkStr;
  UInt32 refclkNumChanges = 0;
  if (refClk)
  {
    const STSymbolTableNode* node = refClk->getNode();
    pather.compPathHier(node, &refclkStr);
    refclkNumChanges = refClk->numChanges();
  }
  out << refclkStr;
  out << refclkNumChanges;

} 

void WtbWavefileToScheduleQueue::clearChanges() 
{
  for (SigDataVec::iterator p = mSigDatas.begin(), e = mSigDatas.end();
       p != e; ++p)
  {
    WtbSignalData* sigData = *p;
    sigData->clearChanges();
  }
} 
