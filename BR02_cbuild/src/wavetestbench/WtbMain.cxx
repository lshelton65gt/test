// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "modshell/WtbApplication.h"
#include "modshell/MdsModel.h"
#include "util/ArgProc.h"
#include <cstdlib>

/*!
  \file
  The main routine for Carbon Wave Testbench.
*/

extern "C" void carbonMdsModelInit(CarbonInitFlags modelFlags, 
                                   CarbonDBType dbType, MdsModel* model);
  
int main(int argc, char **argv)
{
  // Before allocating any memory, see if the user passed the "-dumpMemory"
  // flag, and if so, set up memory histogramming.
  CarbonMem carbonMem(&argc, argv, NULL);

  WtbApplication* app = NULL;
  SInt32 stat = 0;
  {
    bool keepMemoryClean = carbonMem.isCheckingLeaks() || carbonMem.isValgrindRunning() || carbonMem.isPurifyRunning();

#ifdef FORCE_NO_MEMLEAK_CHECK
    keepMemoryClean = false;
    carbonMem.disableLeakDetection();
#endif

    app = new WtbApplication(&argc, argv, keepMemoryClean);
    stat = app->init( argv[0] );
    if (stat != 0)
    {
      carbonMem.disableLeakDetection();
      keepMemoryClean = false;
    }

    if (stat == 0)
    {
      MdsModel* model = app->getModel();
      ArgProc* argProc = app->getArgProc();
      
      CarbonInitFlags modelFlags = eCarbon_NoFlags;
      if (argProc->getBoolValue("-modelstats"))
        modelFlags = eCarbon_EnableStats;

      if (app->isReplayEnabled())
        modelFlags = CarbonInitFlags(modelFlags | eCarbon_Replay);
      
      if (app->isOnDemandEnabled())
        modelFlags = CarbonInitFlags(modelFlags | eCarbon_OnDemand);
      
      CarbonDBType dbType = eCarbonFullDB;
      if (argProc->getBoolValue("-useIODB"))
        dbType = eCarbonIODB;

      // Initialize tailored model
      // This has to be done outside the MdsModel library so dlls can
      // link. See bug1960
      carbonMdsModelInit(modelFlags, dbType, model);
      if (! model->getModelContext())
        exit(1);

      model->maybeCallUserInit();

      stat = app->run();
      if (stat != 0)
      {
        carbonMem.disableLeakDetection();
        keepMemoryClean = false;
      }
      
      if (! keepMemoryClean)
        // print out the statistics before we exit
        app->destroyStats();
    }
    
    if (keepMemoryClean)
      delete app;
  }
  
  return int(stat);
} // int main
