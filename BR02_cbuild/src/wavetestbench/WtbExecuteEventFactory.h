// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WTBEXECUTEEEVENTFACTORY_H_
#define __WTBEXECUTEEEVENTFACTORY_H_


#include "util/CarbonTypes.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtArray.h"
#include "Wtb.h"

class CarbonNet;
class MsgContext;
class STSymbolTableNode;
class UtString;
class WfAbsFileReader;
class WfAbsSignal;
class WfAbsSignalVCIter;
class WtbExecuteEvent;
class WtbExecuteScheduleEvent;
class WtbExecuteClkScheduleEvent;
class WtbExecuteDataScheduleEvent;
class WtbExecutePreScheduleEvent;
class WtbExecuteDepComboScheduleEvent;
class WtbExecuteVCDDumpOffEvent;
class WtbExecuteVCDDumpOnEvent;
class WtbExecuteDataEvent;
class WtbModel;
class WtbSimulator;
class ZostreamDB;
class ZistreamDB;
class DynBitVector;
class DynBitVectorFactory;

/*!
  \file
  The WtbExecuteEventFactory class.
*/


//! WtbExecuteEventFactory class
/*!
  This object provides various execute events to the caller. This
  object is respobsible for the creation and desctruction of all
  WtbExecuteEvent objects.
*/
class WtbExecuteEventFactory
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Construct an event factory object. 
    \param wave_file The object that contains the waveforms.
    \param model The model that is being simulated.
    \param simulator The simulator that will be evaluating the events.
    \param base_path The instance contained within the waveform data
    that is being simulated.
    \param message_context Context for error messages.
   */
  WtbExecuteEventFactory(WfAbsFileReader* wave_file, WtbModel* model, const UtString* base_path, MsgContext *message_context);

  //! Db-based constructor
  WtbExecuteEventFactory(WtbModel* model, MsgContext *message_context);

  //! Destructor.
  virtual ~WtbExecuteEventFactory(void);

  //! A typedef for a set of nodes
  typedef UtHashSet<const STSymbolTableNode *> NodeSet;

  //! Returns true if this node is a CarbonNet in the vector file
  /*!
    This returns false if we didn't read a vector file. Also, the
    vector file has to be read with saveVecFileNodes()
    specified before reading the vector file.
  */
  bool isVecFileNode(const STSymbolTableNode* node);
  
  //! Keep a set of nodes found in a vector file
  void saveVecFileNodes();
  
 private:
  typedef UtArray<WtbExecuteEvent *> EventList;

  class DataEventCache;

 public:
  //! Fetch a WtbExecuteTimeEvent.
  /*!
    Provide a WtbExecuteTimeEvent.  These are events are used to update
    the current time in the WtbSimulator object.
    \param event_time The time to pass to the simulator.
   */
  WtbExecuteEvent * fetchTimeEvent(const UInt64* event_time);

  //! Fetch a WtbExecuteDataEvent.
  /*!
    Provide a WtbExecuteDataEvent.  This object drives data from the
    wavefile into the provided signal.
    \param event_time The time location of data in the wave file.
    \param node The signal that will be driven with the value.
   */
  WtbExecuteEvent * fetchDataEvent(STSymbolTableNode * node, const DynBitVector& valBV, 
                                   const DynBitVector& drvBV);
  
  //! Fetch a WtbExecuteNoXdriveEvent.
  /*!
    This returns an object that deasserts the xdrive.
    \param node The signal that will have the xdrive deasserted.
  */
  WtbExecuteEvent* fetchNoXdriveEvent(STSymbolTableNode * node);

  //! Fetch a WtbExecuteScheduleEvent.
  /*!
    Provide a WtbExecuteScheduleEvent.  This object calls the schedule
    method of the model being simulated.
   */
  WtbExecuteEvent * fetchScheduleEvent(Wtb::ScheduleMode schedMode);
  
  //! Fetch a WtbExecuteVCDDumpOffEvent.
  /*!
    Provide a WtbExecuteVCDDumpOffEvent.  This object will stop the
    dumping of VCD.
   */
  WtbExecuteEvent * fetchVCDDumpOffEvent(void);

  //! Fetch a WtbExecuteVCDDumpOnEvent.
  /*!
    Provide a WtbExecuteVCDDumpOnEvent.  This object will stop the
    dumping of VCD.
   */
  WtbExecuteEvent * fetchVCDDumpOnEvent(void);

  //! Fetch a WtbExecutePreScheduleEvent
  WtbExecuteEvent * fetchPreScheduleEvent();

  //! Fetch a WtbExecuteDepComboScheduleEvent
  WtbExecuteEvent * fetchDepComboScheduleEvent();

  //! Write events to db
  bool writeEventsToDB(ZostreamDB& zout) const;
  //! Read events from db
  bool readEventsFromDB(ZistreamDB& zin);
  
  MsgContext* getMsgContext() const {
    return mMsgContext;
  }

  //! Release the data event cache
  /*!
    This frees up memory that is no longer needed. This should be
    called after the execute queue is filled.
  */
  void freeDataCache();

  //! Get the WtbModel
  WtbModel* getWtbModel() { return mModel; }


 private:
  const DynBitVector* fetchData(const STSymbolTableNode * node, CarbonNet * signal, const DynBitVector& dataBV, 
                                const DynBitVector& driveBV, const DynBitVector** mask = 0);

  //WfAbsSignalVCIter * fetchValueIter(const STSymbolTableNode * node);

 private:
  WfAbsFileReader* mWavefile;
  WtbModel* mModel;
  const UtString* mBasePath;
  MsgContext *mMsgContext;
  //NodeToValueIterMap* mNodeToValueIterMap;
  EventList mEventList;
  DataEventCache* mDataEventCache;
  DynBitVectorFactory* mValueCache;

  WtbExecuteScheduleEvent * mScheduleEvent;
  WtbExecuteClkScheduleEvent * mClkScheduleEvent;
  WtbExecuteDataScheduleEvent * mDataScheduleEvent;
  WtbExecutePreScheduleEvent * mPreScheduleEvent;
  WtbExecuteDepComboScheduleEvent * mDepComboScheduleEvent;
  WtbExecuteVCDDumpOffEvent * mVCDDumpOffEvent;
  WtbExecuteVCDDumpOnEvent * mVCDDumpOnEvent;

  NodeSet* mVecFileNodes;

 private:
  WtbExecuteEventFactory(void);
  WtbExecuteEventFactory(const WtbExecuteEventFactory&);
  WtbExecuteEventFactory& operator=(const WtbExecuteEventFactory&);
  void init();

  CarbonNet* readNetFromDB(ZistreamDB& zin);
  bool readDataEventObject(ZistreamDB& zin, WtbExecuteDataEvent* dataEvent);

  void saveCarbonNetNode(CarbonNet* net);
};


#endif
