// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WTBVCDDUMPCONTEXT_H_
#define __WTBVCDDUMPCONTEXT_H_


#include "util/CarbonTypes.h"
#include "util/UtString.h"

class UtString;


/*!
  \file
  The WtbVCDDumpContext class.
*/


//! WtbVCDDumpContext class
/*!
  This class bundles together all of the parameters associated
  with dumping VCD during a given simulation.
*/

class WtbVCDDumpContext
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  WtbVCDDumpContext(void);

  //! Destructor.
  virtual ~WtbVCDDumpContext(void);

  //! Set the name of the dump file.
  void setFilename(const UtString &filename);

  //! Set the scopes that will be dumped.
  void setScopes(const UtString &scopes);

  //! Set the number of levels of hierarchy that will be dumped.
  void setLevels(UInt32 levels);

  //! Set the time at which dumping will commence.
  void setDumpTime(UInt64 dump_time);

  //! Set the dump format type
  void setDumpType(CarbonWaveFileType type);

  //! Get the dump format
  CarbonWaveFileType getDumpType() const;

  //! Get the name of the dump file.
  const UtString & getFilename(void) const;

  //! Get the scopes that will be dumped.
  const UtString & getScopes(void) const;

  //! Get the number of levels of hierarchy that will be dumped.
  UInt32 getLevels(void) const;

  //! Get the time at which dumping will commence.
  UInt64 getDumpTime(void) const;

  //! Set whether or not to dump only state and primary i/os
  /*!
    \param doOnlyStateIO If true only primary i/os and state will be
    dumped from the list of scopes that are given by getScopes()
  */
  void setStateIO(bool doOnlyStateIO);

  //! Returns true if only state and primary i/os are to be dumped.
  bool doOnlyStateIO() const;

 private:
  UtString mFilename;
  UtString mScopes;
  UInt32 mLevels;
  UInt64 mDumpTime;
  CarbonWaveFileType mDumpType;
  bool mOnlyStateIO;

 private:
  WtbVCDDumpContext(const WtbVCDDumpContext&);
  WtbVCDDumpContext& operator=(const WtbVCDDumpContext&);
};


#endif
