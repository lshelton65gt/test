// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "iodb/IODB.h"
#include "util/CarbonTypes.h"
#include "Wtb.h"
#include "waveform/WfVCD.h"
#include "modshell/WtbApplication.h"

class MsgContext;
class Stats;
class UtString;
class WfAbsFileReader;
class WtbModel;
class WtbVCDDumpContext;
class WtbExecuteQueue;
class WtbSimulator;
class WtbExecuteEventFactory;
class UtUInt64Factory;
class DynBitVectorFactory;
class UtStringArray;
class CarbonReplaySystem;

/*!
  \file
  The WtbTestbench class.
*/


//! WtbTestbench class
/*!
  This class provides the top level control for the testbench
  activities.  It reads in the waveforms, sets up the events,
  and controls initiates the simulation of those events.
*/
class WtbTestbench
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  /*!
    Constuct a testbench object.
    \param model The model that is being simulated.
    \param base_path The instance contained within the waveform data
    that is being simulated.
    \param vcd_context Information about whan and what
    information should be put into a VCD dump.  This can be null.
    \param stats The statistics context.  This can be null.
    \param argv Array of command line arguments.
    \param message_context Context for error messages.
    \param schedByCycle If true, only call schedule if a clock or an
    async changed
    \param replaySystem The replay system.
   */
  WtbTestbench(WtbModel *model, UtString* base_path, WtbVCDDumpContext *vcd_context, Stats *stats, MsgContext *message_context, bool schedByCycle, CarbonSystemSim* replaySystem);

  //! Vector-only constructor
  WtbTestbench(WtbModel *model, WtbVCDDumpContext *vcd_context, Stats *stats, MsgContext *message_context, CarbonSystemSim* replaySystem);

  //! Destructor.
  ~WtbTestbench();

  //! Generate vectors from the wavefile
  /*!
    Not valid in vector run mode.
    \param args Argument processor
    \param iterations Run the execute queue this many times after
    creating the vectors
    \param vecfile If not NULL, save the execute queue to db
    \param verbose Be verbose
    \param useBasePath Use the base path name as the design root in
    the output dump file.
    \param limitSchedCalls If non-zero, limit the number of schedule
    calls to this number
    \param cleanMemory If true then all memory is properly cleaned up
    after it is used. For product mode, memory is not usually cleaned
    up.
    \param refclk If not NULL, the user specified the reference clock
    on the command line. This gets used in the clock selection for
    speed calculation.
  */
  Wtb::Status createVectors(ArgProc* args, UInt32 iterations, 
                            const char* vecfile, AtomicCache* strCache, 
                            bool verbose, bool useBasePath, 
                            SInt32 limitSchedCalls, bool cleanMemory,
                            const char* refclk);

  //! Run the test via vector file.
  /*
    Cause the testbench to run the actual test using a vector file.
    \param interations The number of times the complete
    waveform should be executed.
    \param vecFile Vector archive file
    \param verbose Be verbose
    \param useBasePath Use the basepath in the vector file
    \param limitSchedCalls If non-zero, limit the number of schedule
    calls to this number
    \param refclk If not NULL, the user specified the reference clock
    on the command line. This gets used in the clock selection for
    speed calculation.
  */
  Wtb::Status test(UInt32 iterations, const char* vecFile, bool verbose, 
                   bool useBasePath, 
                   SInt32 limitSchedCalls, const char* refclk);


  //! Dump vectors as vcd
  Wtb::Status dumpVectorsToVCD(const char* fileRoot, const char* vecFile, bool verbose, SInt32 limitSchedCalls);

  //! Translate cwtb vectors to testdriver vectors
  Wtb::Status translateVectorFileToTestDriver(const char* vectorFile, const char* mapFile, const char* outFile, bool verbose,
                                              SInt32 limitSchedCalls);

  MsgContext* getMsgContext() const { return mMsgContext; }

  void printStats(const char *s);

 private:
  CarbonStatus initializeVCDDumping(WfVCD::TimescaleUnit wfTsu,
                                    WfVCD::TimescaleValue wfTsv,
                                    bool useBasePath);

  Wtb::Status loadVectorEntry(const char* entry,
                              const char* vecfile,
                              WtbExecuteQueue* execute_queue2,
                              WfVCD::TimescaleUnit* wfTsu,
                              WfVCD::TimescaleValue* wfTsv,
                              UInt32* dumpContext,
                              bool verbose, SInt32 limitSchedCalls,
                              const char* cmdlineRefClk);
  
 private:
  DynBitVectorFactory* mDynFactory;
  UtUInt64Factory* mTimeFactory;

  WfAbsFileReader* mWaveFile;
  WtbModel* mModel;
  UtString* mBasePath;
  WtbVCDDumpContext *mDumpContext;
  Stats *mStats;
  MsgContext *mMsgContext;

  class WfControl;
  WfControl* mWfControl;

  // only useful when running with vectors
  struct RuntimeStats;
  RuntimeStats* mRuntimeStats;

  CarbonSystemSim* mReplaySystem;

 private:
  WtbTestbench(void);
  WtbTestbench(const WtbTestbench&);
  WtbTestbench& operator=(const WtbTestbench&);

  Wtb::Status runQueue(WtbExecuteQueue* execute_queue, 
                       WtbSimulator* simulator, 
                       UInt32 iterations);

  bool createWavefile(ArgProc* args, AtomicCache* strCache, WtbApplication::Config** config);
  bool maybeCreateConfig(ArgProc* args, WtbApplication::Config** configPtr);

  bool getEntryNames(UtStringArray* entryNames, UInt32* numSegments, const char* vecFile) const;

  void destroyWavefile();

  class TestDriverVectors;

  Wtb::Status writeTestdriverVectors(const char* outFile, TestDriverVectors* tdVectors, WtbExecuteQueue& execute_queue, bool newFile);
  Wtb::Status loadTestDriverMapFile(const char* mapFile, TestDriverVectors* tdVectors, WtbExecuteEventFactory* eventFactory);

};
