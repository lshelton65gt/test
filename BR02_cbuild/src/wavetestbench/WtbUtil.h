// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// -*- C++ -*-



#ifndef __WTBUTIL_H_
#define __WTBUTIL_H_


class MsgContext;
class STSymbolTableNode;
class UtString;
class WfAbsFileReader;
class WfAbsSignal;


/*!
  \file
  The WtbUtil namespace.
*/


//! WtbUtil namespace
/*!
  This class provides utility functions that would have no
  other place to live.
*/
namespace WtbUtil
{
  //! Simply compose the hierarchical path from node
  void composeVerilogName(const STSymbolTableNode* node, UtString* signalName);
  //! Adjust path name for lookup in a wave file.
  /*!
    Returns true if the name is valid, and signal_name will have the
    extracted name.  False otherwise,  and
    signal_name will have the invalid name.
  */
  bool extractAdjustedName(const STSymbolTableNode* node, const UtString &base, UtString* signal_name);

  //! Look up the signal in the wavefile, warn if not found.
  WfAbsSignal * findWfAbsSignalByName(const UtString& signalName, WfAbsFileReader * wave_file, MsgContext *message_context);

  //! Look up a signal, adjust the base path of the node
  /*!
    Works just like findWfAbsSignalByName except it adjusts the path
    of the node to match the wavefile hierarchy
  */
  WfAbsSignal * findWfAbsSignal(const STSymbolTableNode * node, WfAbsFileReader * wave_file, const UtString * base_path, MsgContext *message_context);
}

#endif
