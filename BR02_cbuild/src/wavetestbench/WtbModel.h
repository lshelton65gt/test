// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WTBMODEL_H_
#define __WTBMODEL_H_


#include "modshell/MdsModel.h"
#include "iodb/IODBRuntime.h"

/*!
  \file
  The WtbModel class.
*/

class CarbonSystemSim;
//! WtbModel class
/*!
  A class that extends the Carbon Model abstraction that provides additional
  capability for stimulating the model in a regression type manner.
*/
class WtbModel : public MdsModel
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor
  WtbModel(const UtString &iodb_filename);

  //! Destructor
  virtual ~WtbModel(void);

  const IODB* getIODB() const { return mIODB; }

  //! Adds this model to the replaysystem
  void addComponentToReplaySystem(CarbonSystemSim* replaySystem);

 public:  
  //! Print this object to UtIO::cout().
  void print(bool verbose=false, UInt32 indent=0);

 private:
  WtbModel(void);
  WtbModel(const WtbModel&);
  WtbModel& operator=(const WtbModel&);
};
#endif
