// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WTB_H_
#define __WTB_H_


namespace Wtb
{
  //! Enum for no-input-flow v. input-flow modes
  enum ScheduleMode {
    eScheduleNone, //!< No scheduling context
    eScheduleClks, //!< Run the clock schedules
    eScheduleData //!< Run the data/post schedules
  };

  //! Cwave status
  enum Status
  {
    eWtbStatusSuccess = 0,    //!< Successful.
    eWtbStatusDifference = 1, //!< Differences occurred.
    eWtbStatusError = 2       //!< Unspecified error.
  };
};


#endif
