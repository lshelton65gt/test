// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __WTBSIMULATOR_H_
#define __WTBSIMULATOR_H_


#include "util/CarbonTypes.h"

class WtbExecuteQueue;
class WtbModel;

/*!
  \file
  The WtbSimulator class.
*/


//! WtbSimulator class
/*!
  This class is keeps track of the current simulation time and
  executes the events that are on a WtbExecuteQueue.
*/
class WtbSimulator
{
 public:
  CARBONMEM_OVERRIDES

  //! Constructor.
  WtbSimulator(WtbModel* model, bool verbose);

  //! Destructor.
  ~WtbSimulator(void);

  //! Simulate the queue of events.
  /*!
    Set the initial time to zero and execute the events in
    the queue.
    
    \param event_queue The queue of events to be executed.
   */
  void simulate(WtbExecuteQueue* event_queue);

  //! Set the current simulation time.
  inline void setTime(UInt64 new_time) { mTime = new_time; };

  //! Get the current simulation time.
  inline UInt64 getTime(void) const { return mTime; };

  //! Get the pointer to current simulation time.
  inline const UInt64* getTimePtr(void) const { return &mTime; };

  //! Get the model
  inline WtbModel* getModel() { return mModel; }

  //! Did the simulation finish successfully?
  bool simOK() const;

  //! Are we in verbose mode?
  bool isVerbose() const { return mVerbose; }

 private:
  UInt64 mTime;
  WtbModel* mModel;
  bool mSimOK;
  bool mVerbose;
  
 private:
  WtbSimulator(const WtbSimulator&);  
  WtbSimulator& operator=(const WtbSimulator&);
};


#endif
