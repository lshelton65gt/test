// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "shell/CarbonModel.h"
#include "shell/CarbonHookup.h"
#include "shell/carbon_capi.h"
#include "WtbTestbench.h"
#include "WtbWavefileToScheduleQueue.h"
#include "WtbExecuteEventFactory.h"
#include "WtbExecuteQueue.h"
#include "WtbExecuteEvent.h"
#include "WtbModel.h"
#include "WtbSimulator.h"
#include "WtbUtil.h"
#include "WtbVCDDumpContext.h"
#include "WtbSignalData.h"
#include "util/Stats.h"
#include "util/UtString.h"
#include "waveform/WfVCDFileReader.h"
#include "waveform/WfFsdbFileReader.h"
#include "util/ShellMsgContext.h"
#include "util/ZstreamZip.h"
#include "util/ArgProc.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/UtConv.h"
#include "util/UtArray.h"
#include "util/UtHashMap.h"
#include "util/UtUInt64Factory.h"
#include "iodb/IODBTypes.h"
#include "iodb/IODBRuntime.h"
#include "shell/ReplaySystemSim.h"

static const char* scVecDBTitle = "CWTB Vectors";
static const UInt32 scVecDBVersion = 7;
#define VECDB_INPUTFLOW_VER 6
#define VECDB_REFCLK_VER 6

static void sDoAllOnes(UtString& str)
{
  UInt32 strSize = str.size();
  for (UInt32 i = 0; i < strSize; ++i)
    str[i] = '1';
}

static void sDoAssign(UtString& str, const UInt32* data)
{
  UInt32 size = 0;
  DynBitVector zero;
  const UInt32* dataFixed = data;
  if (! str.empty())
  {
    size = str.size();
    if (! data)
    {
      zero.resize(size);
      dataFixed = zero.getUIntArray();
    }
  }
  
  if (! str.empty() && dataFixed)
  {
    char* val = str.getBuffer();
    UInt32 size = str.size();
    CarbonValRW::writeBinValToStr(val, size + 1, dataFixed, size);
  }
}

class WtbTestbench::TestDriverVectors
{
  struct VecValue
  {
    CARBONMEM_OVERRIDES

    UtString mData;
  };

public:
  CARBONMEM_OVERRIDES

  TestDriverVectors() : mCurHasDataAndEnable(false), mTotalWidth(0), mCurNetBitCnt(1), mCurNode(NULL), mCurWidth(1), mNumInputNets(0) {}
  ~TestDriverVectors() {
    VecValArray::iterator e = mOrderedValue.end();
    for (VecValArray::iterator p = mOrderedValue.begin(); p != e; ++p)
      delete *p;
  }

  UInt32 getNumExpectedNets() const { return mNumInputNets; }

  void setNumInputs(UInt32 numInputs) 
  {
    mNumInputNets = numInputs;
  }

  void updateValues(WtbExecuteQueue::ExecuteQueue& eventQueue)
  {

    WtbExecuteDataEvent* dataEvent = NULL;
    WtbExecuteNoXdriveEvent* driveEvent = NULL;
    STAliasedLeafNode* netNode = NULL;
    NodeToVec::iterator q;
    
    VecValue* vecVal = NULL;
    const UInt32* data;
    const UInt32* drive;
    
    for (WtbExecuteQueue::ExecuteQueue::iterator p = eventQueue.begin(),
           e = eventQueue.end(); p != e; ++p)
    {
      WtbExecuteEvent* event = *p;      

      switch(event->getType())
      {
      case WtbExecuteEvent::eNoXdriveEvent:
        driveEvent = (WtbExecuteNoXdriveEvent*) event;
        netNode = driveEvent->getNetNode();
        q = mEnableVecs.find(netNode);
        ST_ASSERT(q != mEnableVecs.end(), netNode);
        vecVal = q->second;
        sDoAllOnes(vecVal->mData);
        break;
      case WtbExecuteEvent::eDataEvent:
      case WtbExecuteEvent::eDataDriveEvent:
        dataEvent = (WtbExecuteDataEvent*) event;
        netNode = dataEvent->getNetNode();
        q = mDataVecs.find(netNode);
        ST_ASSERT(q != mDataVecs.end(), netNode);
        vecVal = q->second;
        dataEvent->getValues(&data, &drive);
        sDoAssign(vecVal->mData, data);

        q = mEnableVecs.find(netNode);
        if (q != mEnableVecs.end())
        {
          vecVal = q->second;
          sDoAssign(vecVal->mData, drive);
        }
        break;
      case WtbExecuteEvent::eTimeEvent:
      case WtbExecuteEvent::eScheduleEvent:
      case WtbExecuteEvent::eDataScheduleEvent:
      case WtbExecuteEvent::eClkScheduleEvent:
      case WtbExecuteEvent::ePreScheduleEvent:
      case WtbExecuteEvent::eDepComboScheduleEvent:
      case WtbExecuteEvent::eDumpOff:
      case WtbExecuteEvent::eDumpOn:
        break;
      }
    }
  }

  void writeVectors(UtOBStream& outfd) const
  {
    VecValArray::const_iterator e = mOrderedValue.end();
    UtString buf;
    buf.reserve(mTotalWidth);
    for (VecValArray::const_iterator p = mOrderedValue.begin(); p != e; ++p)
    {
      const VecValue* vecVal = *p;
      buf << vecVal->mData;
    }

    UtConv::BinaryStrToHex(&buf);
    outfd << buf << UtIO::endl;
  }
  
  bool maybeAppendNet(const STAliasedLeafNode* leaf, UInt32 width, bool hasDataAndEnable, bool *done, UtString* errMsg)
  {
    bool isGood = true;
    bool forceChange = false;
    if (mCurHasDataAndEnable && (mCurNetBitCnt == mCurWidth))
      // We've come to the end of the data, now the enable is
      // starting. No other signals in-between
      forceChange = true;

    if (!forceChange && (leaf == mCurNode))
    {
      ++mCurNetBitCnt;
    }
    else
    {
      // check if the last node had all its bits accounted for
      // Guard against the first net (no previous net yet)
      UInt32 numNodes = mDataVecs.size();
      if (numNodes > 0)
      {
        if (mCurNetBitCnt != mCurWidth)
        {
          HdlVerilogPath pather;
          pather.compPathHier(mCurNode, errMsg, NULL);
          (*errMsg) << ": Internal error: columns in map file do not correspond to the design.";
          isGood = false;
        }
      }
      
      if (mNumInputNets > 0)
      {
        VecValue* vecVal = NULL;
        if (mDataVecs.find(leaf) == mDataVecs.end())
        {
          vecVal = new VecValue;
          mOrderedValue.push_back(vecVal);
          mDataVecs[leaf] = vecVal;
        }
        else if (hasDataAndEnable && mEnableVecs.find(leaf) == mEnableVecs.end())
        {
          vecVal = new VecValue;
          mOrderedValue.push_back(vecVal);
          mEnableVecs[leaf] = vecVal;
        }
        else
        {
          HdlVerilogPath pather;
          pather.compPathHier(mCurNode, errMsg, NULL);
          (*errMsg) << ": Is not a depositable or bidirect net and appears more than once in the map file.";
          return false;
        }
        
        mCurHasDataAndEnable = hasDataAndEnable;
        mCurNetBitCnt = 1;
        mCurNode = leaf;
        mCurWidth = width;

        vecVal->mData.append(mCurWidth, '0');
        mTotalWidth += mCurWidth;

        //leaf->print();
        --mNumInputNets;
      }
      else 
        *done = true;
    }
    return isGood;
  }
  
  UInt32 getTotalWidth() const { return mTotalWidth; }

private:
  bool mCurHasDataAndEnable;
  UInt32 mTotalWidth;
  UInt32 mCurNetBitCnt;
  const STAliasedLeafNode* mCurNode;
  UInt32 mCurWidth;
  UInt32 mNumInputNets;
  typedef UtHashMap<const STAliasedLeafNode*, VecValue*> NodeToVec;
  NodeToVec mDataVecs;
  NodeToVec mEnableVecs;
  typedef UtArray<VecValue*> VecValArray;
  VecValArray mOrderedValue;
};

struct WtbTestbench::RuntimeStats
{
  CARBONMEM_OVERRIDES

  RuntimeStats() : mTotalTime(0), 
                   mTotalUserSysTime(0),
                   mTotNumCyclesX10(0), 
                   mSegNumCyclesX10(0)
  {}

  // The total simulation time so far
  double mTotalTime;
  // The total user+sys time so far
  double mTotalUserSysTime;
  // The total number of cycles calculated x 10 to avoid half cycle
  // calculations
  UInt64 mTotNumCyclesX10;
  // The number of cycles in the current segment.
  UInt32 mSegNumCyclesX10;
  // name of fastest clock
  UtString mFastestClock;
};


class WtbTestbench::WfControl : public WfAbsControl
{
  // Helper class for change processing
  /*
    Stores status of change types
  */
  class ChangeInfo
  {
  public:
    ChangeInfo() : mDepositCombo(false),
                   mAsync(false)
    {}
    
    
    void addDepositCombo(bool depCombo)
    {
      mDepositCombo |= depCombo;
    }

    void addAsync(bool async)
    {
      mAsync |= async;
    }

    bool hasDepositCombo() const {
      return mDepositCombo;
    }

    bool hasAsync() const {
      return mAsync;
    }
    
  private:
    bool mDepositCombo;
    bool mAsync;
  };

public:
  CARBONMEM_OVERRIDES

  WfControl(WtbTestbench* tb, bool schedByCycle,
            const UtString* basePath,
            DynBitVectorFactory* bvFactory) 
    : mTb(tb), mExecuteQueue(NULL),
      mWaveToSched(NULL), 
      mDumpContext(NULL),
      mFlushNumber(0),
      mIterations(0),
      mLastTime(NULL),
      mVecFile(NULL),
      mBasePath(basePath),
      mFlushStream(NULL),
      mDynFactory(bvFactory),
      mSchedByCycle(schedByCycle),
      mVerbose(false),
      mInputFlow(false),
      mPendingInputData(false),
      mPendingDepComboData(false)
  {}
  
  virtual ~WfControl() {
    if (mFlushStream)
      delete mFlushStream;
  }

  virtual void initialValueWarning(const char* msg) const 
  {
    mTb->getMsgContext()->WtbLoadValueWarning(msg);
  }

  void putIsVerbose(bool isVerbose)
  {
    mVerbose = isVerbose;
  }

  void putVecFile(const char* vecFile)
  {
    mVecFile = vecFile;
  }

  void putIterations(UInt32 iterations)
  {
    mIterations = iterations;
  }

  void putInputFlow(bool inputFlow) 
  {
    mInputFlow = inputFlow;
  }

  UInt32 numIterations() const {
    return mIterations;
  }

#define EXECUTEQ_LIMIT 32000000
#define EXECUTEQ_HARD_LIMIT (1 << 25)

  virtual bool timeSlice(WfAbsSignalSet* changedSigs, 
                         const UInt64* currentTime, 
                         const UInt64* /* nextTime */)
  {
    // run through the changed signal set, separating bidis/inputs and
    // enables
    WfAbsSignalSet bidis;

    WfAbsSignalSet clocks;
    WfAbsSignalSet allClocks;
    WfAbsSignalSet bidiClocks;
    WfAbsSignalSet inputs;
    for (WfAbsSignalSet::UnsortedLoop p = changedSigs->loopUnsorted();
         ! p.atEnd(); ++p)
    {
      WfAbsSignal* sig = *p;
      WtbSignalData* sigData = (WtbSignalData*) sig->getUserData();
      // It is possible that an async is actually an output, and thus
      // we never gave it a WtbSignalData. 
      // See test/wavetestbench/bidiMod.v (_driven_bid)
      if (sigData)
      {
        sigData->incrChange();

        if (sigData->isClock())
        {
          allClocks.insert(sig);

          if (sigData->isBidi())
            bidiClocks.insert(sig);
          else
            clocks.insert(sig);
        }
        else if (sigData->isInput())
        {
          if (sigData->isBidi())
            bidis.insert(sig);
          else
            inputs.insert(sig);
        }
        sigData->getAffectedBidis(&bidis);
      }
    }

    // If sched by cycle and we have pending input data (which can
    // only happen if we are in no-flow-mode). We must run
    // the input schedule before updating the clock values, otherwise,
    // we will have problems with derived clocks from depositable
    // signals.
    if (mSchedByCycle && ! mInputFlow && mPendingInputData && 
        (! clocks.empty() || ! bidiClocks.empty()))
    {
      // We haven't run the input/async scheds in the Carbon Model from the
      // last input update. We need to run the input schedule
      // and run the combo schedule (if applicable)
      // before running the clock schedule
      
      // If this is inputflow we want the changed data to lineup with
      // this clock edge(s) - not the previous.
      if (mInputFlow)
        mExecuteQueue->appendTimeChange(currentTime);

      // Pending deposit combo data can only be true if we have
      // pending input data.
      if (mPendingDepComboData)
      {
        mExecuteQueue->appendDepComboScheduleCall();
        mPendingDepComboData = false;
      }
      
      // Note that the inputs will change in the waveform on the
      // previous 
      mExecuteQueue->appendScheduleCall(Wtb::eScheduleData);
      mPendingInputData = false;
    }
    
    bool clockEdge = processChanges(&clocks, &bidiClocks, NULL);
    
    // subtract signals from the bidi set that happened to be clocks
    // Normally, there aren't any or many bidis, but many clocks. So,
    // if there are no bidis save some time.
    if (! bidis.empty())
    {
      for (WfAbsSignalSet::UnsortedLoop p = allClocks.loopUnsorted();
           ! p.atEnd(); ++p)
        bidis.erase(*p);
    }

    // schedule call, if appropriate
    Wtb::ScheduleMode schedMode = Wtb::eScheduleClks;
    bool stop = false;
    if (! mInputFlow && clockEdge)
    {
      mExecuteQueue->appendTimeChange(currentTime);
      stop = mExecuteQueue->appendScheduleCall(schedMode);
    }

    schedMode = Wtb::eScheduleData;
    if (mInputFlow)
      schedMode = Wtb::eScheduleNone;

    ChangeInfo chgInfo;
    bool nonClockModified = processChanges(&inputs, &bidis, &chgInfo);
    
    // basic check to see if we need to call schedule after depositing
    // to the non-clock inputs. We always have to call the data
    // schedule if any clocks or data has changed because it calls the
    // postschedule.
    bool appendSchedCall = nonClockModified || clockEdge;
    // If we are scheduling by cycle the above calculation doesn't
    // matter, but it is too confusing to merge the mSchedByCycle
    // boolean into the above calculation. So, handling it separately
    // here.
    if (mSchedByCycle)
    {
      // schedule by cycle trumps the default - only run on a
      // clockedge or an async
      appendSchedCall = clockEdge || chgInfo.hasAsync();
      
      if (! mInputFlow && ! appendSchedCall)
      {
        // We have data changes that need to be applied to the input
        // scheds prior to running the next clock edge.
        mPendingInputData |= nonClockModified;
        mPendingDepComboData |= mPendingInputData && chgInfo.hasDepositCombo();
      }
    }
    
    if (appendSchedCall)
    {
      if (! mInputFlow && chgInfo.hasDepositCombo())
        // If no-input-flow make sure we run the deposit combo schedule
        // to update data paths. Note these update the changes applied
        // after running the clock schedule.
        mExecuteQueue->appendDepComboScheduleCall();

      if (! clockEdge || mInputFlow)
      {
        // need to append a time change since we didn't with clock
        // processing
        mExecuteQueue->appendTimeChange(currentTime);
        
        // Also need to append a pre schedule call if no clockedge and
        // we are creating no-input-flow vectors
        if (! clockEdge && ! mInputFlow)
          mExecuteQueue->appendPreScheduleCall();
      }
      
      stop |= mExecuteQueue->appendScheduleCall(schedMode);
    }
    
    // The execution queue should never surpass 32M (2^25)
    INFO_ASSERT(mExecuteQueue->size() < EXECUTEQ_HARD_LIMIT, "Execution queue has surpassed a hard limit of 2^25");
    if (mExecuteQueue->size() >= EXECUTEQ_LIMIT)
      flushExecQ();
    
    mLastTime = currentTime;
    return stop;
  }

  void putExecQ(WtbExecuteQueue* execQ)
  {
    mExecuteQueue = execQ;
  }

  void putWaveToSched(WtbWavefileToScheduleQueue* waveToSched)
  {
    mWaveToSched = waveToSched;
  }

  bool isSchedByCycle() const { return mSchedByCycle; }

  void putDumpContext(WtbVCDDumpContext* dumpContext)
  {
    mDumpContext = dumpContext;
  }

  void finalize()
  {
    if ((mFlushNumber > 0) && mLastTime)
      flushExecQ();
    else if (mVecFile)
    {
      ZOSTREAMZIP_ALLOC(mFlushStream, mVecFile);
      writeExecQ("0", mVerbose);
    }

    if (mFlushStream)
    {
      if (! mFlushStream->finalize())
      {
        UtString fileName;
        mFlushStream->getFilename(&fileName);
        mTb->getMsgContext()->WtbFileSysProb(fileName.c_str(), mFlushStream->getFileError());
      }
    }
    
    WfAbsFileReader* waveFile = mWaveToSched->getWaveFile();
    waveFile->unloadValues();
  }
  
private:  
  WtbTestbench* mTb;
  WtbExecuteQueue* mExecuteQueue;
  WtbWavefileToScheduleQueue* mWaveToSched;
  WtbVCDDumpContext* mDumpContext;
  UInt32 mFlushNumber;
  UInt32 mIterations;
  const UInt64* mLastTime;
  const char* mVecFile;
  const UtString* mBasePath;
  ZostreamZip* mFlushStream;
  DynBitVectorFactory* mDynFactory;
  bool mSchedByCycle;
  bool mVerbose;
  bool mInputFlow;
  bool mPendingInputData;
  bool mPendingDepComboData;


  void flushExecQ()
  {
    
    if (mFlushNumber == 0)
    {
      INFO_ASSERT(! mFlushStream, "Output CVA File has been opened before it should have been");
      UtString fileName;
      if (! mVecFile)
        fileName << "cwtb.cva";
      else
        fileName.assign(mVecFile);
      
      ZOSTREAMZIP_ALLOC(mFlushStream, fileName.c_str());

      mTb->getMsgContext()->WtbPartitioningWavefile();
    }
    
    if (mIterations > 0)
    {
      mTb->getMsgContext()->WtbNoRunMode();
      mIterations = 0;
    }

    UtString entry;
    entry << mFlushNumber;

    // up the flush number
    ++mFlushNumber;

    writeExecQ(entry.c_str(), true);

    // clear out values/events we have written out.
    mLastTime = NULL;
    mExecuteQueue->clear();
    WfAbsFileReader* waveFile = mWaveToSched->getWaveFile();
    waveFile->flushValues();
    mWaveToSched->clearChanges();
  }

  void writeExecQ(const char* entryName, bool verbose)
  {
    mTb->printStats("BuildExecQ");  

    INFO_ASSERT(mFlushStream, "Output CVA File has not been opened.");
    ZostreamDB* zout = mFlushStream->addDatabaseEntry(entryName);
    if (zout == NULL)
    {
      UtString zipName;
      mFlushStream->getFilename(&zipName);
      mTb->getMsgContext()->WtbCannotOpenDBForWrite(zipName.c_str(), mFlushStream->getFileError());
    }

    // we cannot partition at the first slice
    INFO_ASSERT(mLastTime, "Attempted to write a vector partition on the first timeslice. Vector partitions must have at least 2 timeslices.");
    if (verbose)
    {
      UtString fileName;
      mFlushStream->getFilename(&fileName);
      mTb->getMsgContext()->WtbWritingVectorFile(fileName.c_str(), *mLastTime);
    }
    mExecuteQueue->endQueuePopulate(mVerbose);
    writeVectorFile(*zout);
    mFlushStream->endDatabaseEntry();

    // Write out cycle info if we are scheduling by cycle
    if (isSchedByCycle())
    {
      UtString cycleFile;
      cycleFile << "cwtb.cycle_info";
      // The flush number has already been increased.
      if (mFlushNumber > 0)
        cycleFile << '.' << (mFlushNumber - 1);
      mWaveToSched->writeCycleInfo(cycleFile.c_str());
    }
  }

  void writeVectorFile(ZostreamDB& zout)
  {
    zout << scVecDBTitle;
    zout << scVecDBVersion;
    if (mDumpContext)
      zout << UInt32(1);
    else
      zout.writeNull();
    
    if (mInputFlow)
      zout << UInt21(1);
    else
      zout.writeNull();
    
    WfAbsFileReader* waveFile = mWaveToSched->getWaveFile();
    zout << UInt32(waveFile->getTimescaleUnit());
    zout << UInt32(waveFile->getTimescaleValue());
    zout << *mBasePath;
    mWaveToSched->writeClockInfo(zout);
    mExecuteQueue->writeDB(zout);
    mTb->printStats("VecWrite");
  }

  bool processChanges(WfAbsSignalSet* changedSigs, WfAbsSignalSet* changedBidis, ChangeInfo* info)
  {
    bool modified = false;
    for (WfAbsSignalSet::UnsortedLoop p = changedSigs->loopUnsorted();
         ! p.atEnd(); ++p)
    {
      WfAbsSignal* sig = *p;
      WtbSignalData* sigData = (WtbSignalData*) sig->getUserData();
      FUNC_ASSERT(sigData, sig->print());
      if (sigData->isInput())
      {
        // add this simple input change to the execution queue.
        (void) mExecuteQueue->appendInputChange(sig, mDynFactory);
        // we already know it changed, so we don't need to check the
        // executequeue return value.
        modified = true;

        updateChangeInfo(sigData, info);
      }
      /* 
         else just an enable signal or a user-specified reference
         clock that is not an input or a clock in the design.
      */
    }
    
    // run through bidis, evaluate the enables for each, and add
    // appropriately.
    for (WfAbsSignalSet::UnsortedLoop p = changedBidis->loopUnsorted();
         ! p.atEnd(); ++p)
    {
      WfAbsSignal* sig = *p;
      WtbSignalData* sigData = (WtbSignalData*) sig->getUserData();
      for (WtbSignalData::BidiEnableVecLoop q = sigData->loopBidiEnables();
           ! q.atEnd(); ++q)
      {
        WtbSignalData::BidiEnable* bidiEnable = *q;
        CarbonExpr* enableExpr = bidiEnable->mEnableExpr;
        ExprEvalContext evalContext;
        if (enableExpr->evaluate(&evalContext) == CarbonExpr::eBadSign)
        {
          enableExpr->print();
          mTb->getMsgContext()->WtbExpressionEvalFailure();
        }
        
        // always check the bidi value. The value may not have changed,
        // but what's driving the value (internal v. external) may have. 
        bidiEnable->mEnableDrive = evalContext.getValueRef();
      }
      
      // We may not actually be doing anything. The value change
      // may be due to an internal driver changing. So we have to
      // check the return value of appendInputChange
      if (mExecuteQueue->appendInputChange(sig, mDynFactory))
      {
        modified = true;
        updateChangeInfo(sigData, info);
      }
    }
    return modified;
  }

  void updateChangeInfo(const WtbSignalData* sigData, ChangeInfo* info)
  {
    if (info)
    {
      info->addDepositCombo(sigData->isDepositCombo());
      info->addAsync(sigData->isAsync());
    }
  }
  
};

WtbTestbench::WtbTestbench(WtbModel* model, UtString* base_path, 
                           WtbVCDDumpContext *dump_context, 
                           Stats *stats, MsgContext *message_context, 
                           bool schedByCycle,
                           CarbonSystemSim* replaySystem)
  : mWaveFile(NULL), mModel(model), mDumpContext(dump_context), mStats(stats), mMsgContext(message_context), mReplaySystem(replaySystem)
{
  mBasePath = new UtString(*base_path);
  mTimeFactory = new UtUInt64Factory;
  mDynFactory = new DynBitVectorFactory(true);
  mWfControl = new WfControl(this, schedByCycle, mBasePath, mDynFactory);
  mRuntimeStats = NULL;
}

WtbTestbench::WtbTestbench(WtbModel *model, WtbVCDDumpContext *vcd_context, 
                           Stats *stats, MsgContext *message_context,
                           CarbonSystemSim* replaySystem)
  : mDynFactory(NULL), mTimeFactory(NULL), mWaveFile(NULL), mModel(model), mBasePath(NULL), mDumpContext(vcd_context), mStats(stats), mMsgContext(message_context), mWfControl(NULL), mReplaySystem(replaySystem)
{
  mRuntimeStats = new RuntimeStats;
}

WtbTestbench::~WtbTestbench()
{
  if (mWfControl)
    delete mWfControl;
  
  if (mBasePath)
    delete mBasePath;
  if (mTimeFactory)
    delete mTimeFactory;
  if (mDynFactory)
    delete mDynFactory;
  if (mRuntimeStats)
    delete mRuntimeStats;
}

Wtb::Status WtbTestbench::createVectors(ArgProc* args, UInt32 iterations, 
                                        const char* vecFile, 
                                        AtomicCache* strCache, bool verbose, 
                                        bool useBasePath, 
                                        SInt32 limitSchedCalls, 
                                        bool cleanMemory,
                                        const char* refclk)
{
  mWfControl->putVecFile(vecFile);
  mWfControl->putIsVerbose(verbose);
  mWfControl->putIterations(iterations);
  bool isInputFlow = args->getBoolValue("-inputFlow");
  {
    IODBRuntime* iodb = mModel->getIODBRuntime();
    // Create the vectors as inputFlow if the model was compiled with
    // noInputFlow. The buffering is done in the shell in that case.
    isInputFlow = isInputFlow || iodb->isNoInputFlow();
  }
  mWfControl->putInputFlow(isInputFlow);
  
  /*
  ** Locate and open the waveform file.
  */
  WtbApplication::Config* config = NULL;
  if (! createWavefile(args, strCache, &config))
    return Wtb::eWtbStatusError;
  
  WtbWavefileToScheduleQueue* wave_to_schedule = new WtbWavefileToScheduleQueue(mWaveFile, mModel, mBasePath, mMsgContext, config, mTimeFactory);

  mWfControl->putWaveToSched(wave_to_schedule);
  
  {
    UtString errMsg;
    if (mWaveFile->loadSignals(&errMsg) != WfAbsFileReader::eOK)
      mMsgContext->CouldNotLoadWavefile(errMsg.c_str());
  }
  
  printStats("WaveLoad");
  bool doDeposits = args->getBoolValue("-deposits");
  bool skipBidis  = args->getBoolValue("-noBidi");

  bool depositsAsClocks = isInputFlow || args->getBoolValue(WtbApplication::scAllDepositablesClocks);
  
  wave_to_schedule->watchSignals(doDeposits, mWfControl->isSchedByCycle(), skipBidis, mDynFactory, refclk, depositsAsClocks);
  
  WtbExecuteQueue* execute_queue = new WtbExecuteQueue(mWaveFile, mModel, mBasePath, mMsgContext, limitSchedCalls, verbose);
  mWfControl->putExecQ(execute_queue);
  

  if (! skipBidis)
    wave_to_schedule->createBidiEnableExprs(mDynFactory);
  if (mMsgContext->getSeverityCount(MsgContext::eError) != 0)
    return Wtb::eWtbStatusError;
  
  if (mDumpContext)
  {
    if (initializeVCDDumping(mWaveFile->getTimescaleUnit(), mWaveFile->getTimescaleValue(), useBasePath) != eCarbon_OK)
      return Wtb::eWtbStatusError;

    mWfControl->putDumpContext(mDumpContext);
  }

  UtString msg;
  WfAbsFileReader::Status loadStat = mWaveFile->loadValues(&msg);
  if (loadStat == WfAbsFileReader::eError)
    mMsgContext->WtbLoadValueError(msg.c_str());
  else if (loadStat == WfAbsFileReader::eWarning)
    mMsgContext->WtbLoadValueWarning(msg.c_str());

  mWfControl->finalize();

  delete wave_to_schedule;
  wave_to_schedule = NULL;

  //execute_queue.print();
  //  printStats("Build Execute Queue");
  
  CarbonMem::releaseBlocks();
  
  // Must clean memory if we are running the queue now
  if (iterations != 0)
    cleanMemory = true;
  
  if (cleanMemory)
  {
    destroyWavefile();
    if (config)
      delete config;
  }
  printStats("MemClean");

  WtbSimulator simulator(mModel, verbose);
  Wtb::Status stat = runQueue(execute_queue, &simulator, mWfControl->numIterations());

  if (cleanMemory)
    delete execute_queue;
  
  return stat;
}

bool WtbTestbench::createWavefile(ArgProc* args, AtomicCache* strCache, WtbApplication::Config** config)
{
  const char* filename;

  INFO_ASSERT(mWaveFile == 0, "Attempted to open a wavefile when one is already opened.");
  args->getStrValue("-waveFile", &filename);

  WfVCDBase* vcdReader = NULL;
  if (args->getBoolValue("-vcd"))
    vcdReader = new WfVCDFileReader(filename, strCache, mDynFactory, mTimeFactory, mWfControl);
  else if (args->getBoolValue("-fsdb"))
  {
    MdsModel::FSDBReadCreateFn fsdb_create = mModel->getFsdbReadCreateFn();
    if (! fsdb_create)
    {
      mMsgContext->FSDBFormatNotAllowed();
      return false;
    }
    else
    {
      vcdReader = (*fsdb_create)(filename, strCache, mDynFactory, mTimeFactory, mWfControl);
      INFO_ASSERT(vcdReader, "Output Fsdb file creation failed.");
    }
  }
  else
  {
    INFO_ASSERT(0, "createWaveFile called with no output wavefile options specified on the command line."); // This should have already been captured.
  }

  const char* unlistedvalStr = args->getStrValue("-unlistedval");
  bool isGoodVal = (strlen(unlistedvalStr) == 1);
  if (isGoodVal)
  {
    char unlistedval = unlistedvalStr[0];
    unlistedval = tolower(unlistedval);
    switch(unlistedval)
    {
    case 'x': break;
    case 'z': break;
    case '0': break;
    case '1': break;
    default:
      isGoodVal = false;
      break;
    }
    if (isGoodVal)
      vcdReader->putUnlistedValueSetting(unlistedval);
  }
  if (! isGoodVal)
  {
    mMsgContext->WtbInvalidValueSetting(unlistedvalStr, "unvalued signal initialization");
    return false;
  }
  vcdReader->putDo4State(false);
  mWaveFile = vcdReader;
  
  bool isGood = true;
  // Since we are using the wavefile, create the config object, if the
  // user specified a config file.
  if (maybeCreateConfig(args, config))
  {
    // need to check if an error has occurred (mConfig is NULL)
    if (*config == NULL)
      isGood = false;
  }
  
  return isGood;
}

/*
** Switches need to be processed before calling
** this routine, and the IODB needs to exist.
*/
bool WtbTestbench::maybeCreateConfig(ArgProc* args, WtbApplication::Config** configPtr)
{

  if (args->isParsed("-configfile") == ArgProc::eNotParsed)
    return false;
  
  const char* config_filename;
  args->getStrValue("-configfile", &config_filename);
  // safety check
  if (strlen(config_filename) == 0)
    return false;

  *configPtr = new WtbApplication::Config(mModel->getIODBRuntime(), mMsgContext);

  WtbApplication::Config::Status stat = (*configPtr)->readFile(config_filename);

  if (stat == WtbApplication::Config::eError)
  {
    delete *configPtr;
    *configPtr = NULL;
  }
  
  return true;
}

void WtbTestbench::destroyWavefile()
{
  if (mWaveFile)
  {
    delete mWaveFile;
    mWaveFile = NULL;
  }
}

// Entry point for kcachegrind command line
//   calltree --toggle-collect=simulate --collect-state=no --verbose --dumps=50000000
// this command line can also be used with testdriver.
extern "C" void simulate(WtbSimulator* simulator,
                         WtbExecuteQueue* execute_queue,
                         UInt32 iterations)
{
  for (UInt32 i = 0; i < iterations; ++i)
    simulator->simulate(execute_queue);
}

Wtb::Status WtbTestbench::loadVectorEntry(const char* entry,
                                          const char* vecfile,
                                          WtbExecuteQueue* execute_queue2,
                                          WfVCD::TimescaleUnit* wfTsu,
                                          WfVCD::TimescaleValue* wfTsv,
                                          UInt32* dumpContext,
                                          bool verbose,
                                          SInt32 limitSchedCalls,
                                          const char* cmdlineRefClk)
{
  mMsgContext->WtbLoadingVectorEntry(entry, vecfile);
  
  // Don't want to keep the zip stream open the entire
  // simulation. We'll open it for each entry, then close it.
  ZISTREAMZIP(vecFileZip, vecfile);
  if (! vecFileZip)
  {
    mMsgContext->WtbFileSysProb(vecfile, vecFileZip.getFileError());
    return Wtb::eWtbStatusError;
  }
  
  ZistreamEntry* zipEntry = vecFileZip.getEntry(entry);
  if (! zipEntry)
  {
    if (vecFileZip.fail())
      mMsgContext->WtbFileSysProb(vecfile, vecFileZip.getFileError());
    else
      mMsgContext->WtbFileCorrupt(vecfile, entry);
    
    return Wtb::eWtbStatusError;
  }
  
  ZistreamDB* zin = zipEntry->castZistreamDB();
  // This *must* be a db type. If not, then there is something
  // seriously wrong.
  {
    UtString buf;
    buf << "Database entry, " << entry << ", is in an unsupported format.";
    INFO_ASSERT(zin, buf.c_str());
  }

  UInt32 version;
  {  
    UtString title;
    (*zin) >> title;
    if (title.compare(scVecDBTitle) != 0)
    {
      mMsgContext->WtbDBNotAVecEntry(vecfile, scVecDBTitle, title.c_str());
      return Wtb::eWtbStatusError;
    }
    
    (*zin) >> version;
    if (version > scVecDBVersion)
    {
      mMsgContext->WtbDBVersionMismatch(scVecDBVersion, version);
      return Wtb::eWtbStatusError;
    }
    else if (version < scVecDBVersion)
      mMsgContext->WtbReadingOldVectors(vecfile);
  }
  
  (*zin) >> *dumpContext;
  
  if (*dumpContext != 0)
    INFO_ASSERT(*dumpContext == 1, "Corrupt db entry for dumpContext.");

  UInt32 inputFlow = 1;  
  if (version >= VECDB_INPUTFLOW_VER)
    (*zin) >> inputFlow;

  if (verbose)
  {
    if (inputFlow)
      mMsgContext->WtbReadingFlowVectors(vecfile, "input flow (vectors created with -inputFlow)");
    else
      mMsgContext->WtbReadingFlowVectors(vecfile, "no input flow (default)");
  }
  
  UInt32 tsu;
  UInt32 tsv;
  (*zin) >> tsu;
  (*zin) >> tsv;
  *wfTsu = static_cast<WfVCD::TimescaleUnit>(tsu);
  *wfTsv = static_cast<WfVCD::TimescaleValue>(tsv);
  
  delete mBasePath;
  mBasePath = new UtString;
  (*zin) >> *mBasePath;

  UInt32 numClocks;
  (*zin) >> numClocks;
  if (zin->fail())
  {
    mMsgContext->WtbDBReadFail(zin->getError());
    return Wtb::eWtbStatusError;
  }

  UtString fastestClock;
  UInt32 clkChanges = 0;
  bool foundRefClk = false;
  for (UInt32 i = 0; ! zin->fail() && (i < numClocks); ++i)
  {
    UtString buf;
    UInt32 numChanges;
    (*zin) >> buf;
    (*zin) >> numChanges;
    if (! zin->fail())
    {      
      if (verbose)
        mMsgContext->WtbReportClockChanges(buf.c_str(), numChanges);
      if (! foundRefClk)
      {
        // If a user specified a refclk on the command line then make
        // that the reference clock, if found.
        if (cmdlineRefClk && (version >= VECDB_REFCLK_VER))
        {
          if (buf.compare(cmdlineRefClk) == 0)
          {
            fastestClock = buf;
            clkChanges = numChanges;
            foundRefClk = true;
          }
        }
        else if ((mRuntimeStats->mFastestClock.empty() && 
                  (numChanges > clkChanges)) ||
                 (mRuntimeStats->mFastestClock.compare(buf) == 0))
        {
          fastestClock = buf;
          clkChanges = numChanges;
        }
      }
    }
  }

  // Be careful not to read in refclk stuff in prior versions.
  if (version >= VECDB_REFCLK_VER)
  {
    if (! foundRefClk && (cmdlineRefClk != NULL))
      mMsgContext->WtbRefClkNotInClkList(cmdlineRefClk);
  
    UtString refclkBuf;
    UInt32 refclkNumChanges;
    (*zin) >> refclkBuf;
    (*zin) >> refclkNumChanges;
    
    if (! foundRefClk && ! refclkBuf.empty())
    {
      fastestClock = refclkBuf;
      clkChanges = refclkNumChanges;
      foundRefClk = true;
    }

    if (foundRefClk && verbose)
    {
      // This covers when -refclk is specified during vector creation
      // AND when -refclk is specified during cva runtime.
      mMsgContext->WtbUsingSpecifiedRefClk(fastestClock.c_str());
    }

  }
  else if (cmdlineRefClk)
    mMsgContext->WtbRefClkOnOldCVA(cmdlineRefClk);

  if (zin->fail())
  {
    mMsgContext->WtbDBReadFail(zin->getError());
    return Wtb::eWtbStatusError;
  }
  
  // report the clock that we are going to use to calculate the speed.
  // Note that there may be 0 clocks
  if (! fastestClock.empty())
  {
    // calculate the number of cycles (x 10 to avoid 1/2 cycles)
    // clkChanges/2 * 10 => clkChanges * 5, avoids truncation
    mRuntimeStats->mSegNumCyclesX10 = clkChanges * 5; 
    if ((limitSchedCalls == 0) && mRuntimeStats->mFastestClock.empty())
      mRuntimeStats->mFastestClock = fastestClock;
    
    if (verbose)
    {
      if (limitSchedCalls != 0)
        mMsgContext->WtbLimitSchedNoSpeed();
      else
      {
        double numCyclesToReport = mRuntimeStats->mSegNumCyclesX10;
        numCyclesToReport /= 10;
        mMsgContext->WtbUsingClockForHz(fastestClock.c_str(), numCyclesToReport);
      }
    }
  }
  
  if (! execute_queue2->readDB(*zin, verbose, limitSchedCalls))
    return Wtb::eWtbStatusError;
  
  return Wtb::eWtbStatusSuccess;
}

static double sCalcKHz(double totalTime, UInt64 numCyclesX10)
{
  double ret = 0;
  if (totalTime > 0)
    ret = (numCyclesX10/(totalTime * 10))/1000;
  return ret;
}

Wtb::Status WtbTestbench::runQueue(WtbExecuteQueue* execute_queue, 
                                   WtbSimulator* simulator, 
                                   UInt32 iterations)
{
  if (iterations == 0)
    return Wtb::eWtbStatusSuccess;

  // Update the replay system
  switch(mReplaySystem->readCmdline(true))
  {
  case eCarbonSystemChanged:
    if (! mReplaySystem->writeSystem())
      mMsgContext->WtbReplayWriteSystemFailed(mReplaySystem->getErrmsg());
    break;
  case eCarbonSystemUnchanged:
    break;
  case eCarbonSystemCmdError:
    mMsgContext->WtbReplayUpdateSystemFailed(mReplaySystem->getErrmsg());
    break;
  }

  /*
  ** Execute events.
  */
  Stats::StatsData statsData;
  
  simulate(simulator, execute_queue, iterations);
  Wtb::Status stat = simulator->simOK() ? Wtb::eWtbStatusSuccess : Wtb::eWtbStatusError;
  if (mStats)
  {
    mStats->printIntervalStatistics("Simulate", &statsData);
    if (mRuntimeStats && ! mRuntimeStats->mFastestClock.empty() && (stat == Wtb::eWtbStatusSuccess))
    {
      // calculate speed
      double currentTime = statsData.getRealTime();
      double userSysTime = statsData.getUserTime() + statsData.getSysTime();
      UInt64 numCycles = mRuntimeStats->mSegNumCyclesX10 * iterations;

      if (simulator->isVerbose())
      {
        if (numCycles > 0)
        {
          // calculate speed in kilohertz
          double currentRealTimeSpeed = sCalcKHz(currentTime, numCycles);
          double currentUserSysSpeed = sCalcKHz(userSysTime, numCycles);
       
          mMsgContext->WtbReportSegmentTime("real", currentTime);
          mMsgContext->WtbReportSegmentTime("user+sys",  userSysTime);
          if (currentRealTimeSpeed > 0)
            mMsgContext->WtbReportSegmentSpeed("real-time", currentRealTimeSpeed);
          else
            mMsgContext->WtbImmeasurableSpeed();
          
          if (currentUserSysSpeed > 0)
            mMsgContext->WtbReportSegmentSpeed("user+sys", currentUserSysSpeed);
          else
            mMsgContext->WtbImmeasurableSpeed();
          
        }
      }
      
      mRuntimeStats->mTotalTime += currentTime;
      mRuntimeStats->mTotalUserSysTime += userSysTime;
      mRuntimeStats->mTotNumCyclesX10 += numCycles;
    }
  }
  return stat;
}

bool WtbTestbench::getEntryNames(UtStringArray* entryNames, UInt32* numSegments, const char* vecFile) const
{
  bool badness = false;
  ZISTREAMZIP(vecFileZip, vecFile);
  if (! vecFileZip)
    badness = true;
  
  if (! badness && ! vecFileZip.getAllEntryIds(entryNames))
    badness = true;
  
  if (badness)
    mMsgContext->WtbFileSysProb(vecFile, vecFileZip.getFileError());
  else
  {
    *numSegments = entryNames->size();
    if (*numSegments == 0)
    {
      mMsgContext->WtbEmptyArchive(vecFile);
      badness = true;
    }
  }
  
  return ! badness;
}

static bool sFixLimit(SInt32* limitSchedCalls, const WtbExecuteQueue* execq)
{
  *limitSchedCalls -= execq->getNumSchedCalls();
  return (*limitSchedCalls <= 0);
}

Wtb::Status WtbTestbench::test(UInt32 iterations, const char* vecFile, 
                               bool verbose, bool useBasePath, 
                               SInt32 limitSchedCalls, const char* refclk)
{
  Wtb::Status stat = Wtb::eWtbStatusSuccess;

  // read the file and grab all the entryNames
  UtStringArray entryNames;
  UInt32 numSegments;
  if (! getEntryNames(&entryNames, &numSegments, vecFile))
    return Wtb::eWtbStatusError;
  
  // more than one file? If so, calculate average speed

  // Don't support -n <num> greater than 1 currently with vector files
  // that have more than one entry.
  UInt32 full_iterations = iterations;
  UInt32 segment_iterations = iterations;
#if 0
  if ((iterations > 1) && (numSegments > 1))
    mMsgContext->WtbMultiVecInIterCompat();
#else
  if (iterations > 1)
  {
    if (numSegments > 1)
      // iterate full sim on multiple segments
      segment_iterations = 1;
    else
      // multiple iterations on 1 segment
      full_iterations = 1;
  }
#endif
  if (verbose)
    mMsgContext->WtbNumSegments(vecFile, numSegments);

  bool stop = false;
  bool limited = (limitSchedCalls > 0);

  for (UInt32 i = 0; (i < full_iterations) && ! stop; ++i)
  {
    for(UtStringArray::UnsortedCLoop p = entryNames.loopCUnsorted(); 
        ! stop && ! p.atEnd() && (stat == Wtb::eWtbStatusSuccess); ++p)
    {
      const char* entry = *p;
      
      WtbSimulator simulator(mModel, verbose);
      WtbExecuteQueue* execute_queue2 = new WtbExecuteQueue(mModel, mMsgContext);
      
      WfVCD::TimescaleUnit wfTsu;
      WfVCD::TimescaleValue wfTsv;
      UInt32 dumpContext;
      
      {
        Wtb::Status stat = loadVectorEntry(entry, vecFile, execute_queue2, &wfTsu, &wfTsv, &dumpContext, verbose, limitSchedCalls, refclk);
        if (stat != Wtb::eWtbStatusSuccess)
          return stat;
      }
      
      if (! mModel->isDumpingWave() && mDumpContext)
      {
        if (initializeVCDDumping(wfTsu, wfTsv, useBasePath) != eCarbon_OK)
          return Wtb::eWtbStatusError;
        
        if (dumpContext == 0)
        {
          // there are no scheduled vcd events. Just dump from time 0
          CarbonStatus modStat = mModel->dumpOn();
          INFO_ASSERT(modStat == eCarbon_OK, "Turning wave dumping on failed.");
        }
      }
      
      printStats("VecLoad");
      
      stat = runQueue(execute_queue2, &simulator, segment_iterations);
      
      // If we are limiting the number of calls, check if we should
      // stop, and/or update the limit number
      if (limited)
        stop = sFixLimit(&limitSchedCalls, execute_queue2);
      
      delete execute_queue2;
      printStats("CleanQ");
    }
    mReplaySystem->updateSystem(eReplayEventUpdate);
  } // for

  if (verbose && (stat == Wtb::eWtbStatusSuccess))
  {
    mReplaySystem->updateSystem(eReplayEventUpdate);
    mMsgContext->WtbReportTotalTime("real",  mRuntimeStats->mTotalTime);
    mMsgContext->WtbReportTotalTime("user+sys",  mRuntimeStats->mTotalUserSysTime);
    double numCyclesToReport = mRuntimeStats->mTotNumCyclesX10;
    numCyclesToReport /= 10;
    mMsgContext->WtbReportTotalCycles(numCyclesToReport);

    if (mRuntimeStats->mTotNumCyclesX10 > 0)
    {
      // calculate speed in kilohertz
      double averageRealSpeed = sCalcKHz(mRuntimeStats->mTotalTime, mRuntimeStats->mTotNumCyclesX10);

      double averageUsrSysSpeed = sCalcKHz(mRuntimeStats->mTotalUserSysTime, mRuntimeStats->mTotNumCyclesX10);
      
      if (averageRealSpeed > 0)
        mMsgContext->WtbReportAvgSpeed("real-time", averageRealSpeed);
      else
        mMsgContext->WtbImmeasurableSpeed();

      if (averageUsrSysSpeed > 0)
        mMsgContext->WtbReportAvgSpeed("user+sys", averageUsrSysSpeed);
      else
        mMsgContext->WtbImmeasurableSpeed();
    }
  }
  
  return stat;
}

void WtbTestbench::printStats(const char *s)
{
  if (mStats)
  {
    mStats->printIntervalStatistics(s);
  }
}


CarbonStatus WtbTestbench::initializeVCDDumping(WfVCD::TimescaleUnit wfTsu,
                                                WfVCD::TimescaleValue wfTsv,
                                                bool useBasePath)
{
  CarbonStatus stat;
  CarbonWave* wave = NULL;

  stat = mModel->dumpFile(mDumpContext->getFilename(), wfTsu, wfTsv, mDumpContext->getDumpType());
  if (stat == eCarbon_OK)
  {
    wave = mModel->getModelContext()->getModel ()->getCarbonWave();
    
    if (useBasePath && mBasePath)
      carbonPutPrefixHierarchy(wave, mBasePath->c_str(), 1);
    
    if (mDumpContext->doOnlyStateIO())
      stat = mModel->dumpStateIO(mDumpContext->getLevels(), mDumpContext->getScopes());
    else
      stat = mModel->dumpVars(mDumpContext->getLevels(), mDumpContext->getScopes());
  }
  if (stat == eCarbon_OK)
  {
    carbonWaveHierarchyClose(wave);
    //stat = mModel->dumpOff();
  }
  return stat;
}

Wtb::Status WtbTestbench::translateVectorFileToTestDriver(const char* vectorFile, const char* mapFile, const char* outFile, bool verbose,
                                                          SInt32 limitSchedCalls)
{
  WtbSimulator simulator(mModel, verbose);

  WtbExecuteQueue* execute_queue2 = new WtbExecuteQueue(mModel, mMsgContext);

  WtbExecuteEventFactory* eventFactory = execute_queue2->getEventFactory();
  eventFactory->saveVecFileNodes();
  
  WfVCD::TimescaleUnit wfTsu;
  WfVCD::TimescaleValue wfTsv;
  UInt32 dumpContext;

  UtStringArray entryNames;
  {
    UInt32 numSegments;
    if (! getEntryNames(&entryNames, &numSegments, vectorFile))
      return Wtb::eWtbStatusError;
  }

  bool first = true;
  TestDriverVectors tdVectors;

  bool limited = (limitSchedCalls > 0);
  bool stop = false;
  Wtb::Status stat = Wtb::eWtbStatusSuccess;
  for(UtStringArray::UnsortedCLoop p = entryNames.loopCUnsorted(); 
      ! stop && ! p.atEnd() && (stat == Wtb::eWtbStatusSuccess); ++p)
  {
    const char* entry = *p;
    
    // first time through the queue is not null because we are saving
    // extra information (saveVecFileNodes())
    if (execute_queue2 == NULL)
      execute_queue2 = new WtbExecuteQueue(mModel, mMsgContext);

    Wtb::Status stat = loadVectorEntry(entry, vectorFile, execute_queue2, &wfTsu, &wfTsv, &dumpContext, verbose, limitSchedCalls, NULL);
    if (stat != Wtb::eWtbStatusSuccess)
      return stat;
    
    // the first vector partition must be loaded before we load the map
    // file. The first partition has all the signals that need to be
    // present.
    if (first)
    {
      stat = loadTestDriverMapFile(mapFile, &tdVectors, eventFactory);
      if (stat != Wtb::eWtbStatusSuccess)
        return stat;
    }
    
    stat = writeTestdriverVectors(outFile, &tdVectors, *execute_queue2, first);
    first = false;

    if (limited)
      stop = sFixLimit(&limitSchedCalls, execute_queue2);

    delete execute_queue2;
    execute_queue2 = NULL;
    eventFactory = NULL;
  }
  
  return stat;
}

Wtb::Status WtbTestbench::loadTestDriverMapFile(const char* mapFile, TestDriverVectors* tdVectors, WtbExecuteEventFactory* eventFactory)
{
  UtIBStream mapfd(mapFile);
  if (mapfd.bad())
  {
    mMsgContext->WtbFileSysProb(mapFile, mapfd.getErrmsg());
    return Wtb::eWtbStatusError;
  }

  UtString buf;
  SInt32 column;
  UtStringArray nameParts;

  HdlVerilogPath pather;
  UtString errMsg;
  HdlId info; // unused

  // need to use the carbonmodel's symtab
  CarbonModel* carbonModel = mModel->getModelContext()->getModel ();
  CarbonHookup* hookup = carbonModel->getHookup();
  IODBRuntime* iodb = hookup->getDB();

  STSymbolTable* symTab = iodb->getDesignSymbolTable();
  STSymbolTableNode* topNode = NULL;
  AtomicCache* strCache = symTab->getAtomicCache();
  

  tdVectors->setNumInputs(iodb->numPrimaryInputs() + iodb->numPrimaryBidis() * 2 + iodb->numDeposits() * 2);
  
  {  
    UInt32 numRootBranches = 0;
    for (STSymbolTable::RootIter r = symTab->getRootIter(); ! r.atEnd(); ++r)
    {
      STSymbolTableNode* root = *r;
      if (root->castBranch())
      {
        ++numRootBranches;
        topNode = root;
      }
    }
    INFO_ASSERT(numRootBranches == 1, "There is more than one root in the symboltable.");

  }

  bool isGood = true;
  bool done = false;
  int lineno = 0;
  while (isGood && ! done && ! mapfd.eof())
  {
    if (mapfd.getline(&buf))
    {
      ++lineno;
      const char* cbuf = buf.c_str();
      isGood = UtConv::strToLongModify(&cbuf, &column, eCarbonDec, &errMsg);

      nameParts.clear();
      isGood = isGood && (pather.parseName(&cbuf, &nameParts, &info) != HdlHierPath::eIllegal);
      
      if (isGood)
      {
        UtString nodeName;
        
        bool isDepositSignal = false;
        if (nameParts.size() == 1)
        {
          const UtString& name = nameParts[0];
          StringAtom* nameAtom = strCache->intern(name.c_str());
          pather.compPathHierAppend(topNode, nameAtom, &nodeName, NULL);
        }
        else
        {
          pather.compPath(nameParts, &nodeName, NULL);
          isDepositSignal = true;
        }
        
        HdlHierPath::Status parseStat;
        STSymbolTableNode* requestedNode = symTab->getNode(nodeName.c_str(), &parseStat, &info, NULL);
        if (! requestedNode)
        {
          isGood = false;
          errMsg << mapFile << ":" << lineno << ": " << nodeName << " not found in design.";
        }
        else 
        {
          STAliasedLeafNode* leafNode = requestedNode->castLeaf();
          if (! leafNode)
          {
            isGood = false;
            errMsg << mapFile << ":" << lineno << ": " << nodeName << " is a scope and not a net name.";
          }
          else
          {
            const IODBGenTypeEntry* genEntry = iodb->getLeafType(leafNode->getStorage());
            ST_ASSERT(genEntry, leafNode);
            const IODBIntrinsic* intrinsic = iodb->getLeafIntrinsic(leafNode);
            
            // check if the vector file was created with deposits, if
            // we have a depositSignal
            bool inVectorFile = eventFactory->isVecFileNode(leafNode);
            bool isBidi = genEntry->isBidirect();
            UInt32 numExpectedNets = tdVectors->getNumExpectedNets();

            if (numExpectedNets && isDepositSignal && ! inVectorFile)
            {
              isGood = false;
              errMsg << mapFile << ":" << lineno << ": " << nodeName << " is a deposit signal, but was not found in the cwtb vector file. Create the vector file with -deposits.";
            }
            // can't do this for bidis because the bidi may always be
            // internally driven. It wouldn't show up in the
            // vector file in this case. But, I currently have no way
            // of knowing that it is always driven.
            else if (numExpectedNets && !isBidi && ! inVectorFile)
            {
              isGood = false;
              errMsg << mapFile << ":" << lineno << ": " << nodeName << " is not found in the cwtb vector file. Be sure you are using the correct map file.";
            }
            else
            {
              // after looking at testdriver code, it looks like we
              // cannot have a bidi that is marked
              // depositable. Not sure that is valid, but ok...
              UInt32 netWidth = intrinsic->getWidth();
              isGood = tdVectors->maybeAppendNet(leafNode, netWidth, isBidi || isDepositSignal, &done, &errMsg);
            }
          }
        } // else
      } // if isGood
    } // if getline
  } // while

  //UtIO::cout() << tdVectors->getTotalWidth() << UtIO::endl;

  Wtb::Status stat = Wtb::eWtbStatusSuccess;
  if (! isGood)
  {
    mMsgContext->WtbMapFileParseProblem(errMsg.c_str());
    stat = Wtb::eWtbStatusError;
  }
  // Make sure the file wasn't empty
  else if ( lineno <= 0 )
  {
    errMsg << "Map file " << mapFile << " is empty. There should be at lease one port in the map file.";
    mMsgContext->WtbMapFileParseProblem(errMsg.c_str());
    stat = Wtb::eWtbStatusError;
  }
  
  
  return stat;
}


Wtb::Status WtbTestbench::writeTestdriverVectors(const char* outFile, TestDriverVectors* tdVectors, WtbExecuteQueue& execute_queue, bool newFile)
{

  WtbExecuteQueue::ExecuteQueue eventQueue;
  // gather all value changes for each timeslice and write out the
  // vectors. Append mode because we might have > 1 vector partitions
  const char* mode = newFile ? "w" : "a";
  UtOBStream outfd(outFile, mode);
  if (outfd.bad())
  {
    mMsgContext->WtbFileSysProb(outFile, outfd.getErrmsg());
    return Wtb::eWtbStatusError;
  }

  UInt32 index = 0;
  while (execute_queue.gatherValueChanges(&eventQueue, index))
  {
    tdVectors->updateValues(eventQueue);
    tdVectors->writeVectors(outfd);
  }

  if (outfd.bad())
  {
    mMsgContext->WtbFileSysProb(outFile, outfd.getErrmsg());
    return Wtb::eWtbStatusError;
  }
  
  if (! outfd.close())
  {
    mMsgContext->WtbFileSysProb(outFile, outfd.getErrmsg());
    return Wtb::eWtbStatusError;
  }

  return Wtb::eWtbStatusSuccess;
}

Wtb::Status 
WtbTestbench::dumpVectorsToVCD(const char* fileRoot, const char* vecFile, bool verbose, SInt32 limitSchedCalls)
{
  Wtb::Status stat = Wtb::eWtbStatusSuccess;
  
  // read the file and grab all the entryNames
  UtStringArray entryNames;
  UInt32 numSegments;
  if (! getEntryNames(&entryNames, &numSegments, vecFile))
    return Wtb::eWtbStatusError;
  
  if (verbose)
    mMsgContext->WtbNumSegments(vecFile, numSegments);
  
  bool stop = false;
  bool limited = (limitSchedCalls > 0);

  UInt32 vcdNum = 0;
  for(UtStringArray::UnsortedCLoop p = entryNames.loopCUnsorted(); 
      ! stop && ! p.atEnd() && (stat == Wtb::eWtbStatusSuccess); ++p)
  {
    const char* entry = *p;
    
    WtbExecuteQueue* execute_queue2 = new WtbExecuteQueue(mModel, mMsgContext);
    
    WfVCD::TimescaleUnit wfTsu;
    WfVCD::TimescaleValue wfTsv;
    UInt32 dumpContext;
    
    {
      Wtb::Status stat = loadVectorEntry(entry, vecFile, execute_queue2, &wfTsu, &wfTsv, &dumpContext, verbose, limitSchedCalls, NULL);
      if (stat != Wtb::eWtbStatusSuccess)
        return stat;
    }
    
    printStats("VecLoad");

    UtString vcdFile(fileRoot);
    vcdFile << "_" << vcdNum << ".vcd";
    ++vcdNum;
    if (! execute_queue2->writeQueueToVcd(vcdFile.c_str()))
      stat = Wtb::eWtbStatusError;
    
    printStats("VecToVcd");

    // If we are limiting the number of calls, check if we should
    // stop, and/or update the limit number
    if (limited)
      stop = sFixLimit(&limitSchedCalls, execute_queue2);
    
    delete execute_queue2;
    printStats("CleanQ");
  }
  return stat;
}
