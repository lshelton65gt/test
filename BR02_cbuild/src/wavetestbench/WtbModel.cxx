// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/UtIOStream.h"
#include "WtbModel.h"
#include "util/UtString.h"
#include "shell/ReplaySystemSim.h"

WtbModel::WtbModel(const UtString &iodb_filename)
  : MdsModel(iodb_filename)
{
}


WtbModel::~WtbModel(void)
{
}

void WtbModel::print(bool, UInt32 indent)
{
  UtString indent_buffer;

  for (UInt32 i = 0; i < indent; i++)
  {
    indent_buffer = indent_buffer + " ";
  }

  UtIO::cout() << indent_buffer << "Model" << UtIO::endl;
}

void WtbModel::addComponentToReplaySystem(CarbonSystemSim* replaySystem)
{
  replaySystem->addComponent(getIODBRuntime()->getTopLevelModuleName(), 
                             &mContext);
}
