// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/UtIOStream.h"
#include "WtbSimulator.h"
#include "WtbExecuteEvent.h"
#include "WtbExecuteQueue.h"


WtbSimulator::WtbSimulator(WtbModel* model, bool verbose) 
  : mModel(model), mSimOK(true), 
    mVerbose(verbose)
{
}


WtbSimulator::~WtbSimulator(void)
{
}


void WtbSimulator::simulate(WtbExecuteQueue* event_queue)
{
  WtbExecuteEvent * execute_event;
  WtbExecuteQueue::ExecuteQueueLoop loop = event_queue->loopEvents();

  mTime = 0;
  int eventStat = WtbExecuteEvent::eOK;
  while(loop(&execute_event) && (eventStat != WtbExecuteEvent::eEnd))
  {
    eventStat = execute_event->execute(this);
    mSimOK &= (eventStat != WtbExecuteEvent::eError);
  }
  
}

bool WtbSimulator::simOK() const
{
  return mSimOK;
}

