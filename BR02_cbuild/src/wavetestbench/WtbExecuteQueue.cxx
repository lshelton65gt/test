// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "WtbExecuteQueue.h"
#include "WtbExecuteEvent.h"
#include "WtbExecuteEventFactory.h"
#include "WtbModel.h"
#include "util/Zstream.h"
#include "util/ShellMsgContext.h"
#include "WtbSignalData.h"
#include "shell/CarbonDBRead.h"

#include "shell/VcdFile.h"
#include "hdl/HdlVerilogPath.h"
#include "util/UtConv.h"
#include "util/AtomicCache.h"

class WtbExecuteQueue::QueueWave
{
public:
  CARBONMEM_OVERRIDES

  class WaveBranchBOM
  {
  public:
    CARBONMEM_OVERRIDES
    
    WaveBranchBOM() : mWaveScope(NULL)
  {}
    
    void putWaveScope(WaveScope* handle)
    {
      mWaveScope = handle;
    }
    
    WaveScope* getWaveScope()
    {
      return mWaveScope;
    }
  
  private:
    WaveScope* mWaveScope;
  };
  
  class WaveLeafBOM
  {
  public:
    CARBONMEM_OVERRIDES
    
    WaveLeafBOM() : mWaveHandle(NULL), mValue(NULL)
    {}
    
    ~WaveLeafBOM()
    {
    }
    
    void putWaveHandle(WaveHandle* handle)
    {
      mWaveHandle = handle;
    }

    void putHdlId(const HdlId& id)
    {
      mHdlId = id;
      // Add 1 to the width for null termination. We don't need to do
      // this but I want to be impervious to any UtString changes.
      mValue.resize(id.getWidth(), '0');
    }
    
    const HdlId& getHdlId() const
    {
      return mHdlId;
    }
    
    WaveHandle* getWaveHandle()
    {
      return mWaveHandle;
    }
    
    UtString& getValue()
    {
      return mValue;
    }

  private:
    WaveHandle* mWaveHandle;
    HdlId mHdlId;
    UtString mValue;
  };
  
  class WaveBOM : public STFieldBOM
  {
  public:
    CARBONMEM_OVERRIDES
    
    WaveBOM() {}
    
    virtual ~WaveBOM() {}
    
    virtual void writeBOMSignature(ZostreamDB&) const
    {
    }
    
    virtual ReadStatus readBOMSignature(ZistreamDB&, UtString*)
    {
      return eReadIncompatible;
    }
    
    static WaveLeafBOM* castLeafBOM(Data obj)
    {
      return reinterpret_cast<WaveLeafBOM*>(obj);
    }
    
    static WaveBranchBOM* castBranchBOM(Data obj)
    {
      return reinterpret_cast<WaveBranchBOM*>(obj);
    }
    
    virtual Data allocBranchData()
    {
      return new WaveBranchBOM;
    }
    
    virtual Data allocLeafData()
    {
      return new WaveLeafBOM;
    }
    
    virtual void freeBranchData(const STBranchNode*, Data* bomdata)
    {
      WaveBranchBOM* brData = castBranchBOM(*bomdata);
      delete brData;
    }
    
    virtual void freeLeafData(const STAliasedLeafNode*, Data* bomdata)
    {
      WaveLeafBOM* leafData = castLeafBOM(*bomdata);
      delete leafData;
    }

    virtual void preFieldWrite(ZostreamDB& )
    {
    }
    
    virtual void writeLeafData(const STAliasedLeafNode*, ZostreamDB&) const
    {
    }
    
    virtual void writeBranchData(const STBranchNode*, ZostreamDB&,
                                 AtomicCache*) const
    {
    }
    
    virtual ReadStatus preFieldRead(ZistreamDB&)
    {
      return eReadIncompatible;
    }
    
    virtual ReadStatus readLeafData(STAliasedLeafNode*, ZistreamDB&, MsgContext*)
    {
      return eReadIncompatible;
    }
    
    virtual ReadStatus readBranchData(STBranchNode*, ZistreamDB&, MsgContext*)
    {
      return eReadIncompatible;
    }
    
    virtual void printBranch(const STBranchNode*) const
    {}
    
    virtual void printLeaf(const STAliasedLeafNode*) const
    {
    }

    //! Return BOM class name
    virtual const char* getClassName() const
    {
      return "WaveBOM";
    }
    
    //! Write the BOMData for a branch node
    virtual void xmlWriteBranchData(const STBranchNode* /*branch*/,UtXmlWriter* /*writer*/) const
    {
    }
    
    //! Write the BOMData for a leaf node
    virtual void xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/,UtXmlWriter* /*writer*/) const
    {
    }


  };

public:
  QueueWave(AtomicCache* waveStrings, WaveDump* waveDump) :
    mWaveStrings(waveStrings),
    mWaveTable(&mWaveBOM, mWaveStrings),
    mWaveDump(waveDump)
    
  {
    mWaveTable.setHdlHier(&mHdl);
  }
  
  ~QueueWave() {}
  
  STSymbolTable* getSymTab() { return &mWaveTable; }

  bool createWaveHierarchy()
  {
    for (STSymbolTable::RootIter roots = mWaveTable.getRootIter(); ! roots.atEnd(); ++roots)
    {
      STSymbolTableNode* node = *roots;
      STBranchNode* bnode = NULL;
      STAliasedLeafNode* lnode = NULL;
      if ((bnode = node->castBranch()))
        addScope(bnode, NULL);
      else if ((lnode = node->castLeaf()))
        addSignal(lnode, NULL);
    }
    
    UtString errMsg;
    if (mWaveDump->closeHierarchy(&errMsg) != WaveDump::eOK)
    {
      UtIO::cout() << errMsg << UtIO::endl;
      return false;
    }
    return true;
  }

  void updateSimple(const DynBitVector* value, const WtbExecuteEvent* event)
  {
    EventToBom::iterator p = mEventToBom.find(const_cast<WtbExecuteEvent*>(event));
    INFO_ASSERT(p != mEventToBom.end(), "Event not found in bom map.");
    WaveLeafBOM* leafBOM = p->second;
    UtString& strVal = leafBOM->getValue();
    const HdlId& info = leafBOM->getHdlId();
    char* buf = strVal.getBuffer();
    CarbonValRW::writeBinValToStr(buf, info.getWidth() + 1, value->getUIntArray(), info.getWidth(), false);
    WaveHandle* waveHandle = leafBOM->getWaveHandle();
    mWaveDump->addChangedWithVal(waveHandle, buf);
  }

  void updateComplex(const DynBitVector* value, const DynBitVector* drive, const WtbExecuteEvent* event)
  {
    EventToBom::iterator p = mEventToBom.find(const_cast<WtbExecuteEvent*>(event));
    INFO_ASSERT(p != mEventToBom.end(), "Event not found in bom map.");
    WaveLeafBOM* leafBOM = p->second;
    UtString& strVal = leafBOM->getValue();
    CarbonValRW::writeBin4ToStr(&strVal, value, drive);
    WaveHandle* waveHandle = leafBOM->getWaveHandle();
    mWaveDump->addChangedWithVal(waveHandle, strVal.getBuffer());
  }

  void deAssertDrive(const WtbExecuteEvent* event)
  {
    EventToBom::iterator p = mEventToBom.find(const_cast<WtbExecuteEvent*>(event));
    INFO_ASSERT(p != mEventToBom.end(), "Event not found in bom map.");
    WaveLeafBOM* leafBOM = p->second;
    UtString& strVal = leafBOM->getValue();
    const HdlId& info = leafBOM->getHdlId();
    UInt32 len = info.getWidth();
    strVal.replace(0, len, "1");
    WaveHandle* waveHandle = leafBOM->getWaveHandle();
    mWaveDump->addChangedWithVal(waveHandle, strVal.getBuffer());
  }

  void mapEvent(WtbExecuteEvent* event, STAliasedLeafNode* leaf, const HdlId& info)
  {
    WaveLeafBOM* bom = WaveBOM::castLeafBOM(leaf->getBOMData());
    bom->putHdlId(info);
    mEventToBom.insertInit(event, bom);
  }
  
private:
  HdlVerilogPath mHdl;
  WaveBOM mWaveBOM;
  AtomicCache* mWaveStrings;
  STSymbolTable mWaveTable;  
  WaveDump* mWaveDump;

  typedef UtHashMap<WtbExecuteEvent*, WaveLeafBOM*> EventToBom;
  EventToBom mEventToBom;
  
  void addScope(STBranchNode* branch, WaveScope* parent)
  {
    STSymbolTableNode* symNode;
    STBranchNode* chBr;
    STAliasedLeafNode* chLf;
    SInt32 index;
    STBranchNodeIter bIter(branch);

    WaveScope* currentScope = mWaveDump->attachScope(branch->strObject(), parent, WaveScope::eModule, NULL, eLanguageTypeUnknown);
    WaveBranchBOM* branchBOM = WaveBOM::castBranchBOM(branch->getBOMData());
    branchBOM->putWaveScope(currentScope);
    
    while ((symNode = bIter.next(&index)))
    {
      if ((chBr = symNode->castBranch()))
        addScope(chBr, currentScope);
      else if ((chLf = symNode->castLeaf()))
        addSignal(chLf, currentScope);
    }
  }

  void addSignal(STAliasedLeafNode* leaf, WaveScope* parent)
  {
    WaveLeafBOM* leafBOM = WaveBOM::castLeafBOM(leaf->getBOMData());
    const HdlId& info = leafBOM->getHdlId();
    WaveHandle* timeHandle = parent->addSignal(leaf->strObject(), eVerilogTypeReg, eCarbonVarDirectionImplicit, info);
    leafBOM->putWaveHandle(timeHandle);
  }

};

WtbExecuteQueue::WtbExecuteQueue(WfAbsFileReader* wave_file, WtbModel* model, const UtString* base_path, MsgContext *message_context, int limitSchedCalls, bool /*verbose*/)
  : mModel(model), mEventFactory(0), mLimitSchedCalls(limitSchedCalls)
{
  mEventFactory = new WtbExecuteEventFactory(wave_file, model, base_path, message_context);
  // populateFromScheduleQueue(schedule_queue, limitSchedCalls,
  // message_context, verbose);
  init();
}

WtbExecuteQueue::WtbExecuteQueue(WtbModel* model, MsgContext *message_context)
  : mModel(model), mEventFactory(0)
{
  mEventFactory = new WtbExecuteEventFactory(model, message_context);
  init();
  mLimitSchedCalls = 0;
}

void WtbExecuteQueue::init()
{
  mNumSchedCalls = 0;
  mNumTimeEvents = 0;
  mNumDataEvents = 0;
}

WtbExecuteQueue::~WtbExecuteQueue(void)
{
  delete mEventFactory;
}


WtbExecuteQueue::ExecuteQueueLoop WtbExecuteQueue::loopEvents(void)
{
  return ExecuteQueueLoop(mQueue);
}

void WtbExecuteQueue::print(void)
{
  WtbExecuteEvent * execute_event;
  WtbExecuteQueue::ExecuteQueueLoop loop = loopEvents();
  
  while(loop(&execute_event))
  {
    execute_event->print(mModel);
  }
}

void WtbExecuteQueue::writeDB(ZostreamDB& zout) const
{
  bool good = mEventFactory->writeEventsToDB(zout);
  good = good && writeQueueToDB(zout);
  if (! good)
  {
    const char* errMsg = NULL;
    if (zout.fail())
      errMsg = zout.getError();
    else
      errMsg = "System error";
    mEventFactory->getMsgContext()->WtbDBWriteFail(errMsg);
  }
}

bool WtbExecuteQueue::writeQueueToVcd(const char* file) const
{
  UtString errMsg;
  VcdFile* vcd = VcdFile::open(WaveDump::eCreate, file, e1ns, &errMsg);
  if (! vcd)
  {
    mEventFactory->getMsgContext()->SHLFailedToOpenForOutput(file, errMsg.c_str());
    return false;
  }

  AtomicCache waveStrings;
  HdlId sigInfo(63,0);
  WaveScope* cwave_root = vcd->attachScope(waveStrings.intern("carbon_cwave"), NULL, WaveScope::eModule, NULL, eLanguageTypeUnknown);
  WaveHandle* timeHandle = cwave_root->addSignal(waveStrings.intern("time"), eVerilogTypeTime, eCarbonVarDirectionImplicit, sigInfo);
  
  sigInfo.setType(HdlId::eVectBitRange);
  sigInfo.setVectLSB(0);
  sigInfo.setVectMSB(31);
  WaveHandle* schedCntHandle = cwave_root->addSignal(waveStrings.intern("schedcalls"), eVerilogTypeInteger, eCarbonVarDirectionImplicit, sigInfo);
  
  QueueWave queueWave(&waveStrings, vcd);
  STSymbolTable* waveTable = queueWave.getSymTab();


  typedef UtHashSet<WtbExecuteEvent*> EventSet;
  EventSet visited;

  // grab the hierarchy in the vectors.
  for (ExecuteQueue::const_iterator p = mQueue.begin(), e = mQueue.end();
       p != e; ++p)
  {
    WtbExecuteEvent* event = *p;

    if (visited.insertWithCheck(event))
    {
      UtString name;
      const STAliasedLeafNode* eventNode = NULL;
      
      switch(event->getType())
      {
      case WtbExecuteEvent::eDataEvent:
      case WtbExecuteEvent::eDataDriveEvent:
        {
          const WtbExecuteDataEvent* dataEvent = reinterpret_cast<const WtbExecuteDataEvent*>(event);
          dataEvent->getSignalName(mModel, &name);
          eventNode = dataEvent->getNetNode();
        }
        break;
      case WtbExecuteEvent::eNoXdriveEvent:
        {
          const WtbExecuteNoXdriveEvent* xdriveEvent = reinterpret_cast<const WtbExecuteNoXdriveEvent*>(event);
          xdriveEvent->getSignalName(mModel, &name);
          eventNode = xdriveEvent->getNetNode();
        }
        break;
      case WtbExecuteEvent::eTimeEvent:
      case WtbExecuteEvent::eScheduleEvent:
      case WtbExecuteEvent::eDataScheduleEvent:
      case WtbExecuteEvent::eClkScheduleEvent:
      case WtbExecuteEvent::ePreScheduleEvent:
      case WtbExecuteEvent::eDepComboScheduleEvent:
      case WtbExecuteEvent::eDumpOff:
      case WtbExecuteEvent::eDumpOn:
        break;
      }
      
      if (eventNode != NULL)
      {
        HdlId info;
        HdlHierPath::Status parseStat; // not used
        STAliasedLeafNode* leaf = waveTable->translateLeaf(eventNode);
        STSymbolTableNode* node = waveTable->getNode(name.c_str(), &parseStat, &info);
        INFO_ASSERT(node, name.c_str());
        ST_ASSERT(node == leaf, leaf);
        queueWave.mapEvent(event, leaf, info);
      }
    }
  }
  
  // run through the symboltable, create scopes and signals
  if (! queueWave.createWaveHierarchy())
  {
    delete vcd;
    return false;
  }

  // Dump the values, including time and sched count. Waveform time is
  // meaningless here.
  UInt32 schedCalls = 0;
  CarbonTime timeVal = 0;
  CarbonTime waveTime = 0; // meaningless, but used to dump values
  const DynBitVector* drive = NULL;
  const DynBitVector* value = NULL;
  for (ExecuteQueue::const_iterator p = mQueue.begin(), e = mQueue.end();
       p != e; ++p)
  {
    WtbExecuteEvent* event = *p;
    switch(event->getType())
    {
    case WtbExecuteEvent::eDataEvent:
      {
        WtbExecuteDataEvent* dataEvent = reinterpret_cast<WtbExecuteDataEvent*>(event);
        value = dataEvent->getDynValue();
        queueWave.updateSimple(value, dataEvent);
      }
      break;
    case WtbExecuteEvent::eDataDriveEvent:
      {
        WtbExecuteDataDriveEvent* dataDriveEvent = reinterpret_cast<WtbExecuteDataDriveEvent*>(event);
        dataDriveEvent->getDynValues(&value, &drive);
        queueWave.updateComplex(value, drive, dataDriveEvent);
      }
      break;
    case WtbExecuteEvent::eNoXdriveEvent:
      {
        const WtbExecuteNoXdriveEvent* xdriveEvent = reinterpret_cast<const WtbExecuteNoXdriveEvent*>(event);
        queueWave.deAssertDrive(xdriveEvent);
      }
      break;
    case WtbExecuteEvent::eTimeEvent:
      {
        const WtbExecuteVectorTimeEvent* timeEvent = reinterpret_cast<const WtbExecuteVectorTimeEvent*>(event);
        timeVal = timeEvent->getTime();
        vcd->addChangedWithInt64(timeHandle, timeVal);
      }
      break;
    case WtbExecuteEvent::eScheduleEvent:
    case WtbExecuteEvent::eDataScheduleEvent:
    case WtbExecuteEvent::eClkScheduleEvent:
      ++schedCalls;
      vcd->addChangedWithInt32(schedCntHandle, schedCalls);
      break;
    case WtbExecuteEvent::eDumpOff:
    case WtbExecuteEvent::eDumpOn:
    case WtbExecuteEvent::ePreScheduleEvent:
    case WtbExecuteEvent::eDepComboScheduleEvent:
      break;      
    }
    
    ++waveTime;
    vcd->advanceTime(waveTime);
  }
  
  delete vcd;
  return true;
}

bool WtbExecuteQueue::writeQueueToDB(ZostreamDB& zout) const
{
  bool ret = true;
  UInt32 objType = 10;
  WtbExecuteEvent* event;
  zout << UInt32(mQueue.size());
  for (ExecuteQueue::const_iterator p = mQueue.begin(), e = mQueue.end();
       p != e; ++p)
  {
    event = *p;
    switch (event->getType())
    {
    case WtbExecuteEvent::eTimeEvent:
    case WtbExecuteEvent::eDataEvent:
    case WtbExecuteEvent::eDataDriveEvent:
    case WtbExecuteEvent::eNoXdriveEvent:
      objType = 0;
      break;
    case WtbExecuteEvent::eScheduleEvent:
      objType = 1;
      break;
    case WtbExecuteEvent::eDumpOff:
      objType = 2;
      break;
    case WtbExecuteEvent::eDumpOn:
      objType = 3;
      break;
    case WtbExecuteEvent::eDataScheduleEvent:
      objType = 4;
      break;
    case WtbExecuteEvent::eClkScheduleEvent:
      objType = 5;
      break;
    case WtbExecuteEvent::ePreScheduleEvent:
      objType = 6;
      break;
    case WtbExecuteEvent::eDepComboScheduleEvent:
      objType = 7;
      break;
    }
    
    FUNC_ASSERT(objType <= WtbExecuteEvent::eDepComboScheduleEvent, event->print(mModel));
    zout << objType;

    switch(objType)
    {
    case 0:
      ret &= zout.writePointer(event);
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
      break;
    default:
      FUNC_ASSERT(0, event->print(mModel));
      break;
    }
  }

  ret &= ! zout.fail();
  return ret;
}

bool WtbExecuteQueue::readDB(ZistreamDB& zin, bool verbose, SInt32 limitSchedCalls)
{
  bool ret = mEventFactory->readEventsFromDB(zin);
  ret = ret && readQueueFromDB(zin, verbose, limitSchedCalls);
  if (! ret)
  {
    const char* errMsg = NULL;
    if (zin.fail())
      errMsg = zin.getError();
    else
      errMsg = "Corrupt file";

    mEventFactory->getMsgContext()->WtbDBReadFail(errMsg);
  }

  return ret;

}

bool WtbExecuteQueue::readQueueFromDB(ZistreamDB& zin, bool verbose, SInt32 limitSchedCalls)
{
  bool ret = true;
  UInt32 objType;
  UInt32 size;
  WtbExecuteEvent* event = NULL;

  zin >> size;
  ret = ! zin.fail();

  if (! ret)
    return false;
  
  if (limitSchedCalls == 0)
    mQueue.reserve(size);
  
  init();

  for (UInt32 i = 0; (i < size) &&
         ((limitSchedCalls == 0) || (mNumSchedCalls < limitSchedCalls));
       ++i)
  {
    event = NULL;
    zin >> objType;
    ret = ! zin.fail();
    switch(objType)
    {
    case 0:
      ret &= zin.readPointer(&event);
      if (verbose)
      {
        switch(event->getType())
        {
        case WtbExecuteEvent::eTimeEvent:
          ++mNumTimeEvents;
          break;
        case WtbExecuteEvent::eNoXdriveEvent:
        case WtbExecuteEvent::eDataEvent:
        case WtbExecuteEvent::eDataDriveEvent:
          ++mNumDataEvents;
          break;
        case WtbExecuteEvent::eScheduleEvent:
        case WtbExecuteEvent::eDataScheduleEvent:
        case WtbExecuteEvent::eClkScheduleEvent:
        case WtbExecuteEvent::ePreScheduleEvent:
        case WtbExecuteEvent::eDepComboScheduleEvent:
        case WtbExecuteEvent::eDumpOff:
        case WtbExecuteEvent::eDumpOn:
          FUNC_ASSERT(0, event->print(mModel));
          break;
        }
      }
      break;
    case 1:
      event = mEventFactory->fetchScheduleEvent(Wtb::eScheduleNone);
      ++mNumSchedCalls;
      break;
    case 2:
      event = mEventFactory->fetchVCDDumpOffEvent();
      break;
    case 3:
      event = mEventFactory->fetchVCDDumpOnEvent();
      break;
    case 4:
      event = mEventFactory->fetchScheduleEvent(Wtb::eScheduleData);
      ++mNumSchedCalls;
      break;
    case 5:
      event = mEventFactory->fetchScheduleEvent(Wtb::eScheduleClks);
      ++mNumSchedCalls;
      break;
    case 6:
      event = mEventFactory->fetchPreScheduleEvent();
      break;
    case 7:
      event = mEventFactory->fetchDepComboScheduleEvent();
      break;
    default:
      ret = false;
      break;
    }
    
    mQueue.push_back(event);
  }
  
  if (verbose)
  {
    if ((limitSchedCalls > 0) && (mNumSchedCalls >= limitSchedCalls))
      mEventFactory->getMsgContext()->WtbSchedLimitReached(limitSchedCalls);
    
    mEventFactory->getMsgContext()->WtbSummaryExecuteQueue(mNumSchedCalls, mNumDataEvents, mNumTimeEvents);
  }
  
  ret &= ! zin.fail();
  return ret;
}

bool WtbExecuteQueue::gatherValueChanges(ExecuteQueue* eventQueue, UInt32& index)
{
  eventQueue->clear();

  UInt32 numEvents = mQueue.size();
  if (index >= numEvents)
    return false;

  // sync point
  WtbExecuteEvent* event = mQueue[index];
  if (index > 0)
    FUNC_ASSERT(event->getType() == WtbExecuteEvent::eTimeEvent, event->print(mModel));
  
  // now grab all data events until next time slice
  do {
    switch(event->getType())
    {
    case WtbExecuteEvent::eNoXdriveEvent:
    case WtbExecuteEvent::eDataEvent:
    case WtbExecuteEvent::eDataDriveEvent:
      eventQueue->push_back(event);
      break;
    case WtbExecuteEvent::eTimeEvent:
    case WtbExecuteEvent::eScheduleEvent:
    case WtbExecuteEvent::eDataScheduleEvent:
    case WtbExecuteEvent::eClkScheduleEvent:
    case WtbExecuteEvent::ePreScheduleEvent:
    case WtbExecuteEvent::eDepComboScheduleEvent:
    case WtbExecuteEvent::eDumpOff:
    case WtbExecuteEvent::eDumpOn:
      break;
    }
    
    ++index;
    if (index >= numEvents)
      return false;
    
    event = mQueue[index];
  } while (event->getType() != WtbExecuteEvent::eTimeEvent);

  return true;
}


void WtbExecuteQueue::appendDumpOn()
{
  mQueue.push_back(mEventFactory->fetchVCDDumpOnEvent());
}

void WtbExecuteQueue::appendTimeChange(const UInt64* curTime)
{
  mQueue.push_back(mEventFactory->fetchTimeEvent(curTime));
  ++mNumTimeEvents;
}

bool WtbExecuteQueue::appendInputChange(WfAbsSignal* sig, DynBitVectorFactory* valueCache)
{
  /*
    The value that I start with is a waveform value, and is a 4-state
    value. If an input is not a tristate, I have to make sure that I
    don't try to 'de-assert' the CarbonNet (make it x or z),
    otherwise, it causes a 'attempting to de-assert non-tristate'
    warning. I have to clear the drive bits, but before I do that I
    must clear the value bits that are marked as x or z, otherwise I
    will get false positives on the isValueDifference. If the signal
    is not a tristate it also isn't a bidi, but I didn't use an else
    there. This isn't performance critical, so I wanted to play it
    safe in case I'm somehow wrong about that 
    (and with our compiler, I could be at any time in the future). 

    If the signal is a bidi, then the drive mask has to be calculated
    and applied. I could forgo the val &= ~drv until after constant
    marking, but again, this isn't performance critical and it helps
    in debugging seeing exactly what the value and the drive bits are
    at each phase of the input calculation. 

    Constant bit masking trumps any bidi/tristate calculation since
    constant bits just cannot change. 
  */
  bool changed = false;
  WtbSignalData* sigData = (WtbSignalData*) sig->getUserData();
  DynBitVector val, drv;
  sig->getLastValue(&val, &drv);
  if (! sigData->isTristate())
  {
    /* Non-tristate inputs should not de-assert unless they are
       expressioned such that part of the input is constant.
       That gets handled below before we check the value difference.
    */
    // first optimize the value to zero for the undriven bits (x/z)
    val &= ~drv;
    // now clear the drive so we don't attempt a de-assertion.
    drv.reset(); 
  }
  
  if (sigData->isBidi())
  {
    // Loop all the bidi enable expressions for this bidi and assemble
    // the drive value mask.
    for (WtbSignalData::BidiEnableVecLoop p = sigData->loopBidiEnables(); 
         !p.atEnd(); ++p)
    {
      WtbSignalData::BidiEnable* bidiEnable = *p;
      DynBitVector& driveMask = bidiEnable->mEnableDrive;

      // lengthen and shift the driveMask to the bits that it affects.
      // This can only happen if we split the original bidi
      // So, we must have backpointer expressions then.
      if (driveMask.size() != drv.size())
      {
        FUNC_ASSERT(driveMask.size() < drv.size(), sig->print());
        const CarbonExpr* backPointer = ShellSymTabBOM::getBackPointer(bidiEnable->mBidi);
        
        FUNC_ASSERT(backPointer, sig->print());
        // Calculate the number of bits we need to shift left.
        SInt32 shiftAmount = 0;
        const CarbonPartsel* partsel = backPointer->castPartsel();
        if (partsel)
        {
          // normalized subrange of partsel
          const ConstantRange& subRange = partsel->getRange();
          // the shift amount is the same as the lsb, since this is
          // normalized.
          shiftAmount = subRange.getLsb();
        }
        else
        {
          const CarbonBinaryOp* binOp = backPointer->castBinary();
          if (binOp)
          {
            const CarbonExpr* indexExpr = binOp->getArg(1);
            const CarbonConst* indexConst = indexExpr->castConst();
            // We only handle constant indices
            CE_ASSERT(indexConst, indexExpr);
            // We only handle integer indices
            CE_ASSERT(indexConst->getBitSize() <= 32, indexExpr);
            indexConst->getL(&shiftAmount);
          }
          else
            CE_ASSERT("Unhandled backpointer expression" == NULL, backPointer);
        }
        
        FUNC_ASSERT(shiftAmount >= 0, sig->print());
        
        driveMask.resize(sig->getWidth());
        driveMask <<= shiftAmount;
      } // if driveMaskSize != drvSize
      
      FUNC_ASSERT(drv.size() == driveMask.size(), sig->print());
      // update the drive value
      drv |= driveMask;
      // The drive value may have changed and we should reflect any
      // changes in the normal value.
      val &= ~drv;
    } // for bidienables
  } // if bidi
  
  const DynBitVector* constBitMask = sigData->getConstBitMask();
  if (constBitMask)
  {
    FUNC_ASSERT(constBitMask->size() == drv.size(), sig->print());
    drv |= *constBitMask;
    val &= ~drv;
  }
  
  const DynBitVector* lastVal = valueCache->alloc(val);
  const DynBitVector* lastDrv = valueCache->alloc(drv);
  
  if (sigData->isValueDifference(lastVal, lastDrv))
  {
    sigData->putCurrentValue(lastVal, lastDrv);
    mQueue.push_back(mEventFactory->fetchDataEvent(sigData->getNode(), *lastVal, *lastDrv));
    ++mNumDataEvents;
    changed = true;
  }
  return changed;
}

void WtbExecuteQueue::appendNoXDrive(WfAbsSignal* sig)
{
  WtbSignalData* sigData = (WtbSignalData*) sig->getUserData();
  mQueue.push_back(mEventFactory->fetchNoXdriveEvent(sigData->getNode()));
  ++mNumDataEvents;
}

void WtbExecuteQueue::appendPreScheduleCall()
{
  mQueue.push_back(mEventFactory->fetchPreScheduleEvent());
}

void WtbExecuteQueue::appendDepComboScheduleCall()
{
  mQueue.push_back(mEventFactory->fetchDepComboScheduleEvent());
}

bool WtbExecuteQueue::appendScheduleCall(Wtb::ScheduleMode schedMode)
{
  mQueue.push_back(mEventFactory->fetchScheduleEvent(schedMode));
  ++mNumSchedCalls;
  
  bool finished = false;    
  if ((mLimitSchedCalls > 0) && (mNumSchedCalls >= mLimitSchedCalls))
    finished = true;
  
  return finished;
}

void WtbExecuteQueue::endQueuePopulate(bool verbose)
{
  if (verbose)
  {
    if ((mLimitSchedCalls > 0) && (mNumSchedCalls >= mLimitSchedCalls))
      mEventFactory->getMsgContext()->WtbSchedLimitReached(mLimitSchedCalls);
    
    mEventFactory->getMsgContext()->WtbSummaryExecuteQueue(mNumSchedCalls, mNumDataEvents, mNumTimeEvents);
  }
  mEventFactory->freeDataCache();
}

UInt32 WtbExecuteQueue::size() const 
{
  return mQueue.size();
}

void WtbExecuteQueue::clear()
{
  mQueue.clear();
  MsgContext* msgContext = mEventFactory->getMsgContext();
  WtbModel* model = mEventFactory->getWtbModel();
  delete mEventFactory;
  mEventFactory = new WtbExecuteEventFactory(model, msgContext);
  
  // reset the counters
  init();
}
