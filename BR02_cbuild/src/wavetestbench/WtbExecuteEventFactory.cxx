// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "shell/CarbonModel.h"
#include "WtbExecuteEventFactory.h"
#include "WtbExecuteEvent.h"
#include "WtbModel.h"
#include "WtbUtil.h"
#include "shell/ShellNet.h"
#include "util/UtString.h"
#include "util/ShellMsgContext.h"
#include "util/Zstream.h"
#include "waveform/WfAbsFileReader.h"
#include "util/DynBitVector.h"

static bool sIsSingleInstance(WtbExecuteEvent::Type type)
{
  bool ret = true;
  switch(type)
  {
  case WtbExecuteEvent::eTimeEvent:
  case WtbExecuteEvent::eDataEvent:
  case WtbExecuteEvent::eDataDriveEvent:
  case WtbExecuteEvent::eNoXdriveEvent:
    ret = false;
    break;
  case WtbExecuteEvent::eScheduleEvent:
  case WtbExecuteEvent::eDataScheduleEvent:
  case WtbExecuteEvent::eClkScheduleEvent:
  case WtbExecuteEvent::ePreScheduleEvent:
  case WtbExecuteEvent::eDepComboScheduleEvent:
  case WtbExecuteEvent::eDumpOff:
  case WtbExecuteEvent::eDumpOn:
    break;
  }
  return ret;
}

class WtbExecuteEventFactory::DataEventCache
{
public:
  CARBONMEM_OVERRIDES

  DataEventCache() {}
  ~DataEventCache() {}

  WtbExecuteDataEvent* findOrInsert(WtbExecuteDataEvent* event,  bool* inserted)
  {
    *inserted = false;
    WtbExecuteDataEvent* ret = NULL;
    Cache::iterator p = mCache.find(event);
    if (p == mCache.end())
    {
      mCache.insert(event);
      ret = event;
      *inserted = true;
    }
    else
      ret = *p;
    
    return ret;
  }

private:
  typedef UtHashSet<WtbExecuteDataEvent*, HashPointerValue<WtbExecuteDataEvent*> > Cache;
  
  Cache mCache;
};

WtbExecuteEventFactory::WtbExecuteEventFactory(WfAbsFileReader* wave_file, WtbModel* model, const UtString* base_path, MsgContext *message_context)
  : mWavefile(wave_file), mModel(model), mBasePath(base_path), mMsgContext(message_context)
{
  init();
}

WtbExecuteEventFactory::WtbExecuteEventFactory(WtbModel* model, MsgContext *message_context)
  : mWavefile(NULL), mModel(model), mBasePath(NULL), mMsgContext(message_context)
{
  init();
}

void WtbExecuteEventFactory::init()
{
  mScheduleEvent = new WtbExecuteScheduleEvent;
  mClkScheduleEvent = new WtbExecuteClkScheduleEvent;
  mDataScheduleEvent = new WtbExecuteDataScheduleEvent;
  mPreScheduleEvent = new WtbExecutePreScheduleEvent;
  mDepComboScheduleEvent = new WtbExecuteDepComboScheduleEvent;

  mVCDDumpOffEvent = new WtbExecuteVCDDumpOffEvent;
  mVCDDumpOnEvent = new WtbExecuteVCDDumpOnEvent;
  mDataEventCache = new DataEventCache;
  mValueCache = new DynBitVectorFactory(true);
  mVecFileNodes = NULL;
}

WtbExecuteEventFactory::~WtbExecuteEventFactory(void)
{
  if (mVecFileNodes)
    delete mVecFileNodes;
  
  /*
  ** Clean up events.
  */
  for (EventList::iterator i = mEventList.begin(), e = mEventList.end();
       i != e; ++i)
  {
    delete *i;
  }

  delete mScheduleEvent;
  delete mClkScheduleEvent;
  delete mDataScheduleEvent;
  delete mPreScheduleEvent;
  delete mDepComboScheduleEvent;
  delete mVCDDumpOffEvent;
  delete mVCDDumpOnEvent;
  freeDataCache();
  delete mValueCache;
}


WtbExecuteEvent * WtbExecuteEventFactory::fetchTimeEvent(const UInt64* event_time)
{
  WtbExecuteEvent* timeEvent = new WtbExecuteTimeEvent(event_time);
  mEventList.push_back(timeEvent);
  return timeEvent;
}

WtbExecuteEvent* WtbExecuteEventFactory::fetchNoXdriveEvent(STSymbolTableNode * node)
{
  CarbonNet* signal = mModel->findNet(node);
  ST_ASSERT(signal, node);
  WtbExecuteEvent * event = new WtbExecuteNoXdriveEvent(signal);
  mEventList.push_back(event);
  return event;
}

WtbExecuteEvent * WtbExecuteEventFactory::fetchDataEvent(STSymbolTableNode * node, const DynBitVector& valBV, 
                                                         const DynBitVector& drvBV)
{
  CarbonNet * signal = NULL;
  const DynBitVector* data = NULL;
  const DynBitVector* drive = NULL;
  WtbExecuteDataEvent * event = NULL;
  
  signal = mModel->findNet(node);
  data = fetchData(node, signal, valBV, drvBV, &drive);

  if (drive == NULL)
    event = new WtbExecuteDataEvent(signal, data);
  else
    event = new WtbExecuteDataDriveEvent(signal, data, drive);
  ST_ASSERT(event, node);
  
  bool inserted;
  WtbExecuteDataEvent* dataEvent = mDataEventCache->findOrInsert(event, &inserted);
  if (! inserted)
    delete event;
  else
    mEventList.push_back(dataEvent);
  
  return dataEvent;
}

WtbExecuteEvent * WtbExecuteEventFactory::fetchPreScheduleEvent()
{
  return mPreScheduleEvent;
}

WtbExecuteEvent * WtbExecuteEventFactory::fetchDepComboScheduleEvent()
{
  return mDepComboScheduleEvent;
}

WtbExecuteEvent * WtbExecuteEventFactory::fetchScheduleEvent(Wtb::ScheduleMode schedMode)
{
  WtbExecuteEvent* schedEvent = NULL;
  switch(schedMode)
  {
  case Wtb::eScheduleNone:
    schedEvent = mScheduleEvent;
    break;
  case Wtb::eScheduleClks:
    schedEvent = mClkScheduleEvent;
    break;
  case Wtb::eScheduleData:
    schedEvent = mDataScheduleEvent;
    break;
  }
  return schedEvent;
}


WtbExecuteEvent * WtbExecuteEventFactory::fetchVCDDumpOffEvent(void)
{
  return mVCDDumpOffEvent;
}


WtbExecuteEvent * WtbExecuteEventFactory::fetchVCDDumpOnEvent(void)
{
  return mVCDDumpOnEvent;
}


const DynBitVector* WtbExecuteEventFactory::fetchData(const STSymbolTableNode * node, CarbonNet *signal, const DynBitVector& dataBV, 
                                                      const DynBitVector& driveBV, const DynBitVector** drive)
{
  const DynBitVector* data = NULL;

  if (UInt32(signal->getBitWidth()) != dataBV.size())
  {
    char carbonName[4000];
    mModel->getModelContext()->getModel ()->getName(signal, carbonName, 4000);
    
    UtString vcdName;
    if (! WtbUtil::extractAdjustedName(node, *mBasePath, &vcdName))
      mMsgContext->WtbInvalidHdlName(vcdName.c_str());
    
    mMsgContext->WtbMismatchSignalWidth(carbonName, UInt32(signal->getBitWidth()), vcdName.c_str(), dataBV.size());
  }
  
  // Put the dynbvs in the local cache. Must do this for vector
  // reading to work.
  data = mValueCache->alloc(dataBV);
  
  if (drive)
  {
    if (driveBV.any())
      *drive = mValueCache->alloc(driveBV);
    else
      *drive = NULL;
  }
  
  return data;
}

void WtbExecuteEventFactory::freeDataCache()
{
  if (mDataEventCache)
  {
    delete mDataEventCache;
    mDataEventCache = NULL;
  }
}

bool WtbExecuteEventFactory::writeEventsToDB(ZostreamDB& zout) const
{
  bool ret = true;
  zout.writeObject(*mValueCache);
  zout << UInt32(mEventList.size());
  for (EventList::const_iterator p = mEventList.begin(), e = mEventList.end();
       p != e; ++p) 
  {
    const WtbExecuteEvent* event = *p;
    // The following check is not needed, but is good to have in case
    // things change. Currently, the event list only contains data and
    // time events
    WtbExecuteEvent::Type type = event->getType();
    FUNC_ASSERT(! sIsSingleInstance(type), event->print(mModel));
    
    zout << UInt32(type);
    switch(type) 
    {
    case WtbExecuteEvent::eDataEvent:
    case WtbExecuteEvent::eDataDriveEvent:
      {
        const WtbExecuteDataEvent* dataEvent = reinterpret_cast<const WtbExecuteDataEvent*>(event);
        UtString name; 
        dataEvent->getSignalName(mModel, &name);
        zout << name;
      }
      break;
    case WtbExecuteEvent::eNoXdriveEvent:
      {
        const WtbExecuteNoXdriveEvent* xdriveEvent = reinterpret_cast<const WtbExecuteNoXdriveEvent*>(event);
        UtString name; 
        xdriveEvent->getSignalName(mModel, &name);
        zout << name;
      }
      break;
    case WtbExecuteEvent::eTimeEvent:
    case WtbExecuteEvent::eScheduleEvent:
    case WtbExecuteEvent::eDataScheduleEvent:
    case WtbExecuteEvent::eClkScheduleEvent:
    case WtbExecuteEvent::ePreScheduleEvent:
    case WtbExecuteEvent::eDepComboScheduleEvent:
    case WtbExecuteEvent::eDumpOff:
    case WtbExecuteEvent::eDumpOn:
      break;
    } // switch
    ret &= zout.writeObject(*event);
  }
  if (ret)
    ret = ! zout.fail();
  return ret;
}
 
bool WtbExecuteEventFactory::readEventsFromDB(ZistreamDB& zin)
{
  bool ret = true;
  UInt32 size;
  WtbExecuteEvent::Type type;
  UInt32 typeCast;
  WtbExecuteVectorTimeEvent* timeEvent = NULL ;
  WtbExecuteDataEvent* dataEvent = NULL ;
  WtbExecuteNoXdriveEvent* xdriveEvent = NULL ;
  UtString name;
  CarbonNet* net = NULL;
  zin.readObject(mValueCache);
  
  if ((zin >> size))
  {
    mEventList.reserve(size);
    for (UInt32 i = 0; (i < size) && ret; ++i)
    {
      zin >> typeCast;
      ret &= ! zin.fail();
      type = static_cast<  WtbExecuteEvent::Type>(typeCast);
      if (ret)
      {
        INFO_ASSERT(! sIsSingleInstance(type), "Invalid event type in database");
        switch (type)
        {
        case WtbExecuteEvent::eTimeEvent:
          /* time events are read differently from the database to
           * avoid using a time factory for execution events. A time
           * factory used by only an execution queue is just extra
           * overhead.
           */
          timeEvent = new WtbExecuteVectorTimeEvent;
          // map the event pointer
          ret &= zin.readObject(timeEvent);
          mEventList.push_back(timeEvent);
          break;
        case WtbExecuteEvent::eDataEvent:
          net = readNetFromDB(zin);
          if (! net)
            ret = false;
          else
          {
            dataEvent = new WtbExecuteDataEvent(net);
            ret &= readDataEventObject(zin, dataEvent);
          }
          break;
        case WtbExecuteEvent::eDataDriveEvent:
          net = readNetFromDB(zin);
          if (! net)
            ret = false;
          else
          {
            dataEvent = new WtbExecuteDataDriveEvent(net);
            ret &= readDataEventObject(zin, dataEvent);
          }            
          break;
        case WtbExecuteEvent::eNoXdriveEvent:
          name.clear();
          zin >> name;
          INFO_ASSERT(! name.empty(), "eNoXdriveEvent has no net name");
          net = mModel->findNet(name);
          if (! net)
          {
            mMsgContext->WtbDBNetNotFound(name.c_str());
            ret = false;
          }
          else
          {
            xdriveEvent = new WtbExecuteNoXdriveEvent(net);
            ret &= zin.readObject(xdriveEvent);
            mEventList.push_back(xdriveEvent);
          }
          break;
        case WtbExecuteEvent::eScheduleEvent:
        case WtbExecuteEvent::eDataScheduleEvent:
        case WtbExecuteEvent::eClkScheduleEvent:
        case WtbExecuteEvent::ePreScheduleEvent:
        case WtbExecuteEvent::eDepComboScheduleEvent:
        case WtbExecuteEvent::eDumpOff:
        case WtbExecuteEvent::eDumpOn:
          break;
        }
      }
      ret &= ! zin.fail();
    }
  }
  else
    ret = false;
 
  return ret;
}

CarbonNet* WtbExecuteEventFactory::readNetFromDB(ZistreamDB& zin)
{
  UtString name;
  zin >> name;
  CarbonNet* net = mModel->findNet(name);
  if (! net)
    mMsgContext->WtbDBNetNotFound(name.c_str());
  else
  {
    if (mVecFileNodes)
      saveCarbonNetNode(net);
  }
  return net;
}

bool WtbExecuteEventFactory::readDataEventObject(ZistreamDB& zin, WtbExecuteDataEvent* dataEvent)
{
  bool ret = zin.readObject(dataEvent);
  if (ret)
    mEventList.push_back(dataEvent);
  return ret;
}

void WtbExecuteEventFactory::saveVecFileNodes()
{
  mVecFileNodes = new NodeSet;
}

void WtbExecuteEventFactory::saveCarbonNetNode(CarbonNet* net)
{
  ShellNet* shlNet = net->castShellNet();
  const STAliasedLeafNode* node = shlNet->getNameAsLeaf();
  mVecFileNodes->insert(node);
}

bool WtbExecuteEventFactory::isVecFileNode(const STSymbolTableNode* node)
{
  return (mVecFileNodes->find(node) != mVecFileNodes->end());
}
