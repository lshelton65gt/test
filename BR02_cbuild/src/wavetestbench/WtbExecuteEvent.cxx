// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "shell/CarbonModel.h"
#include "util/UtIOStream.h"
#include "WtbExecuteEvent.h"
#include "WtbModel.h"
#include "WtbSimulator.h"
#include "shell/CarbonNet.h"
#include "shell/carbon_capi.h"
#include "shell/carbon_misc.h"
#include "shell/ShellNet.h"
#include "util/Zstream.h"
#include "util/DynBitVector.h"
#include "util/UtConv.h"

WtbExecuteEvent::WtbExecuteEvent(void)
{
}


WtbExecuteEvent::~WtbExecuteEvent(void)
{
}


/**
 **
 **
 **
 */
WtbExecuteTimeEvent::WtbExecuteTimeEvent(const UInt64* event_time)
  : mTime(event_time)
{
}

WtbExecuteTimeEvent::WtbExecuteTimeEvent()
  : mTime(NULL)
{
}

WtbExecuteTimeEvent::~WtbExecuteTimeEvent(void)
{
}


void WtbExecuteTimeEvent::print(WtbModel*) const
{
  UtIO::cout() << "Time: ";

  if (mTime)
    UtIO::cout() << *mTime;
  else
    UtIO::cout() << 0;
  
  UtIO::cout() << UtIO::endl;
  UtIO::cout().flush();
}


WtbExecuteEvent::Status
WtbExecuteTimeEvent::execute(WtbSimulator* simulator)
{
  simulator->setTime(*mTime);
  return eOK;
}

WtbExecuteEvent::Type WtbExecuteTimeEvent::getType() const
{
  return eTimeEvent;
}

bool WtbExecuteTimeEvent::dbWrite(ZostreamDB& out) const
{
  // Only write out the time value. The read is more complex than
  // this. The read will read in the value, and filter it through the
  // time factory. Then, a time event will be created with the
  // factoried time.
  // This has to be done this way to reduce memory usage of the
  // Zstream during writing/reading. The mapping of UInt64 pointers in
  // a factory in Zostream becomes too big..
  out << *mTime;
  return ! out.fail();
}

bool WtbExecuteTimeEvent::dbRead(ZistreamDB&) 
{
  // this should never be called for this
  // class. WtbExecuteVectorTimeEvent should be used when reading
  INFO_ASSERT(0, "DB read of a TimeEvent not supported");
  return false;
}


/**
 **
 **
 **
 */
WtbExecuteVectorTimeEvent::WtbExecuteVectorTimeEvent(UInt64 event_time)
  : mTime(event_time)
{
}

WtbExecuteVectorTimeEvent::WtbExecuteVectorTimeEvent()
  : mTime(0)
{
}

WtbExecuteVectorTimeEvent::~WtbExecuteVectorTimeEvent(void)
{
}


void WtbExecuteVectorTimeEvent::print(WtbModel*) const
{
  UtIO::cout() << "Time: ";

  if (mTime)
    UtIO::cout() << mTime;
  else
    UtIO::cout() << 0;
  
  UtIO::cout() << UtIO::endl;
  UtIO::cout().flush();
}


WtbExecuteEvent::Status
WtbExecuteVectorTimeEvent::execute(WtbSimulator* simulator)
{
  simulator->setTime(mTime);
  return eOK;
}

WtbExecuteEvent::Type WtbExecuteVectorTimeEvent::getType() const
{
  return eTimeEvent;
}

bool WtbExecuteVectorTimeEvent::dbWrite(ZostreamDB& out) const
{
  // This is currently never called. But, it is defined in case we
  // decide to rewrite vector files
  out << mTime;
  return ! out.fail();
}

bool WtbExecuteVectorTimeEvent::dbRead(ZistreamDB& in) 
{
  in >> mTime;
  return ! in.fail();
}


/**
 **
 **
 **
 */
WtbExecuteDataEvent::WtbExecuteDataEvent(CarbonNet* signal, 
                                         const DynBitVector* data)
  : mSignal(signal), mData(data)
{
}

WtbExecuteDataEvent::WtbExecuteDataEvent(CarbonNet *signal)
  : mSignal(signal), mData(NULL)
{
}

WtbExecuteDataEvent::~WtbExecuteDataEvent(void)
{
}

size_t WtbExecuteDataEvent::hash() const
{
  // The objects here are already cached elsewhere. Just need to look
  // for uniqueness in the pointers.
  size_t hashVal = (UIntPtr(mSignal) << 14) + 
    ((UIntPtr(mData) >> 2) & 0x00ffff);
  return size_t(hashVal);
}

bool WtbExecuteDataEvent::doBasicCompare(const WtbExecuteDataEvent& event) const
{
  bool isEqual = mSignal == event.mSignal;
  isEqual &= mData == event.mData;
  return isEqual;
}

bool WtbExecuteDataEvent::operator==(const WtbExecuteDataEvent& event) const
{
  // see hash()
  // If event has a drive, it is not equal.
  bool isEqual = event.getType() == eDataEvent;
  if (isEqual)
    isEqual = doBasicCompare(event);
  return isEqual;
}

void WtbExecuteDataEvent::printValue() const
{
  UtString value;
  CarbonValRW::writeBin4ToStr(&value, mData, NULL);
  // no x's or z's so print out hex
  UtConv::BinaryStrToHex(&value);
  UtIO::cout() << 'h' << value << UtIO::endl;
}

void WtbExecuteDataEvent::print(WtbModel* model) const
{
  // this is only a debug function, so I don't care about performance,
  // but I do care about constness.
  UtIO::cout() << "Data Event" << UtIO::endl;
  const CarbonObjectID *descr = model->getModelContext ();
  
  char name[10000];
  carbonGetNetName(descr, mSignal, name, 10000);
  UtIO::cout() << "  " << name << UtIO::endl;
  UtIO::cout() << "  " << mData->size() << "'";
  
  printValue();
  UtIO::cout().flush();
}


WtbExecuteEvent::Status 
WtbExecuteDataEvent::execute(WtbSimulator* simulator)
{
  CarbonObjectID *descr = simulator->getModel ()->getModelContext ();
#define CHECK_DEPOSIT_STATUS 0
#if CHECK_DEPOSIT_STATUS
  return context->deposit(mSignal, mData->getUIntArray(), NULL)
    == eCarbon_OK ? eOK : eError;
#else
  carbonDepositFast(descr, mSignal, mData->getUIntArray(), NULL);
  return eOK;
#endif
}

WtbExecuteEvent::Type WtbExecuteDataEvent::getType() const
{
  return eDataEvent;
}

void WtbExecuteDataEvent::getSignalName(const WtbModel* model, UtString* name) const
{
  const CarbonObjectID *descr = model->getModelContext ();
  const CarbonModel *context = descr->getModel ();
  char buffer[10000];
  CarbonStatus stat = context->getName(mSignal, buffer,  10000);
  ST_ASSERT(stat == eCarbon_OK, getNetNode());
  name->assign(buffer);
}

const STAliasedLeafNode* WtbExecuteDataEvent::getNetNode() const
{
  const ShellNet* shlNet = mSignal->castShellNet();
  return shlNet->getNameAsLeaf();
}

void WtbExecuteDataEvent::getValues(const UInt32** data, const UInt32** drive)
{
  *data = mData->getUIntArray();
  *drive = NULL;
}

bool WtbExecuteDataEvent::dbWrite(ZostreamDB& zout) const
{
  zout.writeNull();
  zout.writePointer(mData);
  return ! zout.fail();
}

bool WtbExecuteDataEvent::dbRead(ZistreamDB& zin) 
{
  UInt32 drivePresent;
  zin >> drivePresent;
  bool ret = ! zin.fail();
  if (ret)
  {
    ST_ASSERT(drivePresent == 0, getNetNode());
    zin.readPointer(&mData);
  }
  ret &= ! zin.fail();
  return ret;
}

/**
 **
 **
 **
 */
WtbExecuteDataDriveEvent::WtbExecuteDataDriveEvent(CarbonNet* signal, 
                                                   const DynBitVector* data, 
                                                   const DynBitVector* drive)
  : WtbExecuteDataEvent(signal, data), mDrive(drive)
{
}

WtbExecuteDataDriveEvent::WtbExecuteDataDriveEvent(CarbonNet *signal)
  : WtbExecuteDataEvent(signal), mDrive(NULL)
{
}

WtbExecuteDataDriveEvent::~WtbExecuteDataDriveEvent()
{
}

size_t WtbExecuteDataDriveEvent::hash() const
{
  size_t hashVal = WtbExecuteDataEvent::hash();
  hashVal += ((size_t) mDrive);
  return hashVal;
}

bool WtbExecuteDataDriveEvent::operator==(const WtbExecuteDataEvent& event) const
{
  bool isEqual = event.getType() == eDataDriveEvent;
  if (isEqual)
  {
    isEqual = doBasicCompare(event);
    
    if (isEqual)
    {
      const WtbExecuteDataDriveEvent* eventPtr = (const WtbExecuteDataDriveEvent*) &event;
      isEqual = doDriveCompare(*eventPtr);
    }
  }
  return isEqual;
}

bool WtbExecuteDataDriveEvent::doDriveCompare(const WtbExecuteDataDriveEvent& event) const
{
  return (mDrive == event.mDrive);
}

void WtbExecuteDataDriveEvent::printValue() const
{
  UtString value;
  CarbonValRW::writeBin4ToStr(&value, mData, mDrive);
  UtIO::cout() << 'b' << value << UtIO::endl;
}

WtbExecuteEvent::Status 
WtbExecuteDataDriveEvent::execute(WtbSimulator* simulator)
{
  CarbonObjectID *descr = simulator->getModel ()->getModelContext ();
#define CHECK_DEPOSIT_STATUS 0
#if CHECK_DEPOSIT_STATUS
  CarbonModel* context = descr->getModel ();
  return context->deposit(mSignal, mData->getUIntArray(), mDrive->getUIntArray())
    == eCarbon_OK ? eOK : eError;
#else
  carbonDepositFast(descr, mSignal, mData->getUIntArray(), mDrive->getUIntArray());
  return eOK;
#endif
}

WtbExecuteEvent::Type WtbExecuteDataDriveEvent::getType() const
{
  return eDataDriveEvent;
}

void WtbExecuteDataDriveEvent::getValues(const UInt32** data, const UInt32** drive)
{
  *data = mData->getUIntArray();
  *drive = mDrive->getUIntArray();
}

bool WtbExecuteDataDriveEvent::dbWrite(ZostreamDB& zout) const
{
  zout << UInt32(2);
  zout.writePointer(mData);
  zout.writePointer(mDrive);
  return ! zout.fail();
}

bool WtbExecuteDataDriveEvent::dbRead(ZistreamDB& zin) 
{
  UInt32 drivePresent;
  zin >> drivePresent;
  bool ret = ! zin.fail();
  if (ret)
  {
    ST_ASSERT(drivePresent == 2, getNetNode());
    zin.readPointer(&mData);
    zin.readPointer(&mDrive);
  }
  ret &= ! zin.fail();
  return ret;
}


WtbExecuteNoXdriveEvent::WtbExecuteNoXdriveEvent(CarbonNet* net) : 
  mNet(net)
{}

WtbExecuteNoXdriveEvent::~WtbExecuteNoXdriveEvent()
{}

WtbExecuteEvent::Type WtbExecuteNoXdriveEvent::getType() const
{
  return eNoXdriveEvent;
}

void WtbExecuteNoXdriveEvent::print(WtbModel* model) const
{
  UtIO::cout() << "NoXdrive Event" << UtIO::endl;

  char name[10000];
  carbonGetNetName(model->getModelContext(), mNet, name, 10000);
  UtIO::cout() << "  " << name << UtIO::endl;
  UtIO::cout().flush();
}

bool WtbExecuteNoXdriveEvent::dbWrite(ZostreamDB&) const
{
  return true;
}

bool WtbExecuteNoXdriveEvent::dbRead(ZistreamDB&) 
{
  return true;
}

WtbExecuteEvent::Status 
WtbExecuteNoXdriveEvent::execute(WtbSimulator* simulator)
{
  CarbonObjectID *descr = simulator->getModel ()->getModelContext ();
  CarbonModel* context = descr->getModel ();
  context->setToUndriven(mNet);
  return eOK;
}

void WtbExecuteNoXdriveEvent::getSignalName(const WtbModel* model, UtString* name) const
{
  const CarbonObjectID *descr = model->getModelContext ();
  const CarbonModel* context = descr->getModel ();
  char buffer[10000];
  CarbonStatus stat = context->getName(mNet, buffer,  10000);
  ST_ASSERT(stat == eCarbon_OK, getNetNode());
  name->assign(buffer);
}

const STAliasedLeafNode* WtbExecuteNoXdriveEvent::getNetNode() const
{
  const ShellNet* shlNet = mNet->castShellNet();
  return shlNet->getNameAsLeaf();
}

/**
 **
 **
 **
 */
WtbExecutePreScheduleEvent::WtbExecutePreScheduleEvent(void)
{
}

WtbExecutePreScheduleEvent::~WtbExecutePreScheduleEvent(void)
{
}


void WtbExecutePreScheduleEvent::print(WtbModel*) const
{
  UtIO::cout() << "PreSchedule Event" << UtIO::endl;
  UtIO::cout().flush();
}


WtbExecuteEvent::Status 
WtbExecutePreScheduleEvent::execute(WtbSimulator* simulator)
{
  simulator->getModel()->preSchedule(simulator->getTime());
  return eOK;
}

WtbExecuteEvent::Type WtbExecutePreScheduleEvent::getType() const
{
  return ePreScheduleEvent;
}

bool WtbExecutePreScheduleEvent::dbWrite(ZostreamDB&) const
{
  INFO_ASSERT(0, "DB write of a pre-schedule event not supported");
  return false;
}

bool WtbExecutePreScheduleEvent::dbRead(ZistreamDB&)
{
  INFO_ASSERT(0, "DB read of a pre-schedule event not supported");
  return false;
}

/**
 **
 **
 **
 */
WtbExecuteScheduleEvent::WtbExecuteScheduleEvent(void)
{
}

WtbExecuteScheduleEvent::~WtbExecuteScheduleEvent(void)
{
}


void WtbExecuteScheduleEvent::print(WtbModel*) const
{
  UtIO::cout() << "Schedule Event" << UtIO::endl;
  UtIO::cout().flush();
}


WtbExecuteEvent::Status 
WtbExecuteScheduleEvent::execute(WtbSimulator* simulator)
{
  CarbonStatus stat = simulator->getModel()->schedule(simulator->getTime());
  if ((stat == eCarbon_OK) || (stat == eCarbon_STOP)) // stop is ignored
    return eOK;
  else if (stat == eCarbon_FINISH) // tell the simulator to stop
    return eEnd;
  else // must be an error
    return eError;
}

WtbExecuteEvent::Type WtbExecuteScheduleEvent::getType() const
{
  return eScheduleEvent;
}

bool WtbExecuteScheduleEvent::dbWrite(ZostreamDB&) const
{
  INFO_ASSERT(0, "DB write of a schedule event not supported");
  return false;
}

bool WtbExecuteScheduleEvent::dbRead(ZistreamDB&)
{
  INFO_ASSERT(0, "DB read of a schedule event not supported");
  return false;
}

WtbExecuteDataScheduleEvent::WtbExecuteDataScheduleEvent(void)
{
}

WtbExecuteDataScheduleEvent::~WtbExecuteDataScheduleEvent(void)
{
}


void WtbExecuteDataScheduleEvent::print(WtbModel*) const
{
  UtIO::cout() << "DataSchedule Event" << UtIO::endl;
  UtIO::cout().flush();
}


WtbExecuteEvent::Status 
WtbExecuteDataScheduleEvent::execute(WtbSimulator* simulator)
{
  CarbonStatus stat = simulator->getModel()->dataSchedule(simulator->getTime());
  if ((stat == eCarbon_OK) || (stat == eCarbon_STOP)) // stop is ignored
    return eOK;
  else if (stat == eCarbon_FINISH) // tell the simulator to stop
    return eEnd;
  else // must be an error
    return eError;
}

WtbExecuteEvent::Type WtbExecuteDataScheduleEvent::getType() const
{
  return eDataScheduleEvent;
}

bool WtbExecuteDataScheduleEvent::dbWrite(ZostreamDB&) const
{
  INFO_ASSERT(0, "DB write of a data schedule event not supported");
  return false;
}

bool WtbExecuteDataScheduleEvent::dbRead(ZistreamDB&)
{
  INFO_ASSERT(0, "DB read of a data schedule event not supported");
  return false;
}

WtbExecuteClkScheduleEvent::WtbExecuteClkScheduleEvent(void)
{
}

WtbExecuteClkScheduleEvent::~WtbExecuteClkScheduleEvent(void)
{
}


void WtbExecuteClkScheduleEvent::print(WtbModel*) const
{
  UtIO::cout() << "ClkSchedule Event" << UtIO::endl;
  UtIO::cout().flush();
}


WtbExecuteEvent::Status 
WtbExecuteClkScheduleEvent::execute(WtbSimulator* simulator)
{
  CarbonStatus stat = simulator->getModel()->clkSchedule(simulator->getTime());
  if ((stat == eCarbon_OK) || (stat == eCarbon_STOP)) // stop is ignored
    return eOK;
  else if (stat == eCarbon_FINISH) // tell the simulator to stop
    return eEnd;
  else // must be an error
    return eError;
}

WtbExecuteEvent::Type WtbExecuteClkScheduleEvent::getType() const
{
  return eClkScheduleEvent;
}

bool WtbExecuteClkScheduleEvent::dbWrite(ZostreamDB&) const
{
  INFO_ASSERT(0, "DB write of a clock schedule event not supported");
  return false;
}

bool WtbExecuteClkScheduleEvent::dbRead(ZistreamDB&)
{
  INFO_ASSERT(0, "DB read of a clock schedule event not supported");
  return false;
}

WtbExecuteDepComboScheduleEvent::WtbExecuteDepComboScheduleEvent(void)
{}

WtbExecuteDepComboScheduleEvent::~WtbExecuteDepComboScheduleEvent(void)
{}

WtbExecuteEvent::Status 
WtbExecuteDepComboScheduleEvent::execute(WtbSimulator* simulator)
{
  CarbonObjectID *descr = simulator->getModel ()->getModelContext ();
  carbonDepositComboSchedule(descr);
  return eOK;
}

void WtbExecuteDepComboScheduleEvent::print(WtbModel*) const
{
  UtIO::cout() << "DepComboSchedule Event" << UtIO::endl;
  UtIO::cout().flush();
}

WtbExecuteEvent::Type 
WtbExecuteDepComboScheduleEvent::getType() const
{
  return eDepComboScheduleEvent;
}

bool WtbExecuteDepComboScheduleEvent::dbWrite(ZostreamDB&) const
{
  INFO_ASSERT(0, "DB write of a dep-combo schedule event not supported");
  return false;
}

bool WtbExecuteDepComboScheduleEvent::dbRead(ZistreamDB&)
{
  INFO_ASSERT(0, "DB read of a dep-combo schedule event not supported");
  return false;
}


/**
 **
 **
 **
 */
WtbExecuteVCDDumpOffEvent::WtbExecuteVCDDumpOffEvent(void)
{
}

WtbExecuteVCDDumpOffEvent::~WtbExecuteVCDDumpOffEvent(void)
{
}


void WtbExecuteVCDDumpOffEvent::print(WtbModel*) const
{
  UtIO::cout() << "VCD Dump Off Event" << UtIO::endl;
  UtIO::cout().flush();
}


WtbExecuteEvent::Status 
WtbExecuteVCDDumpOffEvent::execute(WtbSimulator* simulator)
{
  simulator->getModel()->dumpOff();
  return eOK;
}

WtbExecuteEvent::Type WtbExecuteVCDDumpOffEvent::getType() const
{
  return eDumpOff;
}

bool WtbExecuteVCDDumpOffEvent::dbWrite(ZostreamDB&) const
{
  INFO_ASSERT(0, "DB write of a vcd dump off event not supported");
  return false;
}

bool WtbExecuteVCDDumpOffEvent::dbRead(ZistreamDB&)
{
  INFO_ASSERT(0, "DB read of a vcd dump off event not supported");
  return false;
}

/**
 **
 **
 **
 */
WtbExecuteVCDDumpOnEvent::WtbExecuteVCDDumpOnEvent(void)
{
}

WtbExecuteVCDDumpOnEvent::~WtbExecuteVCDDumpOnEvent(void)
{
}


void WtbExecuteVCDDumpOnEvent::print(WtbModel*) const
{
  UtIO::cout() << "VCD Dump On Event" << UtIO::endl;
  UtIO::cout().flush();
}


WtbExecuteEvent::Status
WtbExecuteVCDDumpOnEvent::execute(WtbSimulator* simulator)
{
  simulator->getModel()->dumpOn();
  return eOK;
}

WtbExecuteEvent::Type WtbExecuteVCDDumpOnEvent::getType() const
{
  return eDumpOn;
}

bool WtbExecuteVCDDumpOnEvent::dbWrite(ZostreamDB&) const
{
  INFO_ASSERT(0, "DB write of a vcd dump on event not supported");
  return false;
}

bool WtbExecuteVCDDumpOnEvent::dbRead(ZistreamDB&)
{
  INFO_ASSERT(0, "DB read of a vcd dump on event not supported");
  return false;
}
