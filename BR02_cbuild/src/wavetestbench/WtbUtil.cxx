// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "WtbUtil.h"
#include "hdl/HdlId.h"
#include "hdl/HdlVerilogPath.h"
#include "symtab/STSymbolTableNode.h"
#include "symtab/STSymbolTable.h" // ST_ASSERT
#include "util/UtString.h"
#include "util/ShellMsgContext.h"
#include "waveform/WfAbsFileReader.h"
#include "waveform/WfAbsSignal.h"
#include "shell/CarbonDBRead.h"

void WtbUtil::composeVerilogName(const STSymbolTableNode* node, UtString* signalName)
{
  HdlId info;
  HdlVerilogPath vp;
  vp.compPathHier(node, signalName, NULL);
}

bool WtbUtil::extractAdjustedName(const STSymbolTableNode* node, const UtString &base, UtString* signal_name)
{
  signal_name->clear();
  ShellSymTabBOM::composeName(node, signal_name, false, true);
  if (not base.empty())
  {
    HdlId info;
    HdlVerilogPath vp;
    UtStringArray sig_ids;
    UtStringArray base_ids;
    vp.decompPath(base.c_str(), &base_ids, &info);
    if (info.getType() == HdlId::eInvalid)
    {
      signal_name->assign(base);
      return false;
    }
    //      msgContext->WtbInvalidHdlName(base.c_str());
    
    vp.decompPath(signal_name->c_str(), &sig_ids, &info);
    if (info.getType() == HdlId::eInvalid)
      return false;
      //      msgContext->WtbInvalidHdlName(signal_name->c_str());
    
    //std::copy(sig_ids.begin() + 1, sig_ids.end(), std::back_inserter(base_ids));
    UtStringArray::iterator sigEnd = sig_ids.end();
    for (UtStringArray::iterator sigIter = sig_ids.begin() + 1; sigIter != sigEnd; ++sigIter)
      base_ids.push_back(*sigIter);

    ST_ASSERT(info.getType() != HdlId::eInvalid, node);
    vp.compPath (base_ids, signal_name, &info);
  }
  return true;
}


WfAbsSignal * WtbUtil::findWfAbsSignal(const STSymbolTableNode * node, WfAbsFileReader * wave_file, const UtString * base_path, MsgContext *message_context)
{
  UtString signal_name;
  if (WtbUtil::extractAdjustedName(node, *base_path, &signal_name))
    return findWfAbsSignalByName(signal_name, wave_file, message_context);
  return NULL;
}

WfAbsSignal * WtbUtil::findWfAbsSignalByName(const UtString& signalName, WfAbsFileReader * wave_file, MsgContext *message_context)
{
  UtString msg;
  WfAbsSignal * signal = wave_file->findSignal(signalName, &msg);

  if (signal == 0)
  {
    message_context->CantFindSignalInWavefile(signalName.c_str());
  }
  if (! msg.empty())
    message_context->WtbLoadValueWarning(msg.c_str());
  return signal;
}
