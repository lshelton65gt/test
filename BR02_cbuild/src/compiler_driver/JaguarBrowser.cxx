// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "compiler_driver/JaguarBrowser.h"

#define CHECK_STATUS(status, node)  switch (status) { \
  case JaguarBrowserCallback::eNormal: break; \
  case JaguarBrowserCallback::eStop: return status; break; \
  case JaguarBrowserCallback::eSkip: return JaguarBrowserCallback::eNormal; break; \
  default: invalidNode(node); break; \
  }  \


JaguarBrowser::JaguarBrowser(JaguarBrowserCallback& callback)
  : mCallback(callback),
    mTypeBrowseEnable(false),
    mDeclInitialBrowseEnable(false)
{
}

JaguarBrowser::~JaguarBrowser()
{
}

JaguarBrowserCallback::Status JaguarBrowser::browse(vhNode node)
{
  JaguarBrowserCallback::Status status = JaguarBrowserCallback::eNormal;

  if (node != NULL)
  {
    vhObjType type = vhGetObjType(node);
    switch (type) {
    case VHVARASGN:
      status = browseVarAsgn(node);
      break;
    case VHSIGASGN:
      status = browseSigAsgn(node);
      break;
    case VHIF:
      status = browseIf(node);
      break;
    case VHELSIF:
      status = browseElsIf(node);
      break;
    case VHFOR:
      status = browseFor(node);
      break;
    case VHLOOP:
      status = browseLoop(node);
      break;
    case VHWHILE:
      status = browseWhile(node);
      break;
    case VHRETURN:
      status = browseReturn(node);
      break;
    case VHPROCEDURECALL:
      status = browseProcedureCall(node);
      break;
    case VHWAIT:
      status = browseWait(node);
      break;
    case VHCASE:
      status = browseCase(node);
      break;
    case VHCASEALTER:
      status = browseCaseAlter(node);
      break;
    case VHNEXT:
      status = browseNext(node);
      break;
    case VHEXIT:
      status = browseExit(node);
      break;
    case VHNULL:
      status = browseNull(node);
      break;
    case VHASSERT:
      status = browseAssert(node);
      break;
    case VHREPORT:
      status = browseReport(node);
      break;
    case VHSELSIGASGN:
      status = browseReport(node);
      break;
    case VHCHARLIT:
      status = browseCharLit(node);
      break;
    case VHDECLIT:
      status = browseDecLit(node);
      break;
    case VHBASELIT:
      status = browseBaseLit(node);
      break;
    case VHIDENUMLIT:
      status = browseIdeNumLit(node);
      break;
    case VHSTRING:
      status = browseString(node);
      break;
    case VHBITSTRING:
      status = browseBitString(node);
      break;
    case VHOBJECT:
      status = browseObject(node);
      break;
    case VHEXTERNAL:
      status = browseExternal(node);
      break;
    case VHFORINDEX:
    {
      // Differentiate between for and for-generate index.
      vhNode scope = vhGetScope(node);
      if (vhGetObjType(scope) == VHFOR) {
        status = browseForIndex(node);
      }
      break;
    }
    case VHSIGNAL:
      status = browseSignal(node);
      break;
    case VHVARIABLE:
      status = browseVariable(node);
      break;
    case VHFILEDECL:
      status = browseFileDecl(node);
      break;
    case VHCONSTANT:
      status = browseConstant(node);
      break;
    case VHBINARY:
      status = browseBinary(node);
      break;
    case VHUNARY:
      status = browseUnary(node);
      break;
    case VHINDNAME:
      status = browseIndName(node);
      break;
    case VHSLICENAME:
      status = browseSliceName(node);
      break;
    case VHSLICENODE:
      status = browseSliceNode(node);
      break;
    case VHFUNCNODE:
      status = browseFuncNode(node);
      break;
    case VHASSOCIATION:
      status = browseAssociation(node);
      break;
    case VHSELECTEDNAME:
      status = browseSelectedName(node);
      break;
    case VHFUNCCALL:
      status = browseFuncCall(node);
      break;
    case VHTYPECONV:
      status = browseTypeConv(node);
      break;
    case VHQEXPR:
      status = browseQExpr(node);
      break;
    case VHAGGREGATE:
      status = browseAggregate(node);
      break;
    case VHELEMENTASS:
      status = browseElementAss(node);
      break;
    case VHATTRBNAME:
      status = browseAttrbName(node);
      break;
    case VHRANGE:
      status = browseRange(node);
      break;
    case VHSIMPLENAME:
      status = browseSimpleName(node);
      break;
    case VHALIASDECL:
      status = browseAliasDecl(node);
      break;
    case VHATTRBDECL:
      status = browseAttrbDecl(node);
      break;
    case VHATTRIBUTESPEC:
      status = browseAttrbSpec(node);
      break;
      // Type information. Browse if type browsing is enabled.
    case VHCONSARRAY:
      // Array and constrained array are identical
      if (mTypeBrowseEnable) {
        status = browseConstrainedArray(node);
      }
      break;
    case VHUNCONSARRAY:
      if (mTypeBrowseEnable) {
        status = browseUnconstrainedArray(node);
      }
      break;
    case VHENUMELEMENT:
      if (mTypeBrowseEnable) {
        status = browseEnumElement(node);
      }
      break;
    case VHENUMTYPE:
      if (mTypeBrowseEnable) {
        status = browseEnumType(node);
      }
      break;
    case VHFILETYPE:
      if (mTypeBrowseEnable) {
        status = browseFileType(node);
      }
      break;
    case VHFLOATTYPE:
      if (mTypeBrowseEnable) {
        status = browseFloatType(node);
      }
      break;
    case VHINTTYPE:
      if (mTypeBrowseEnable) {
        status = browseIntType(node);
      }
      break;
    case VHRECORD:
      if (mTypeBrowseEnable) {
        status = browseRecord(node);
      }
      break;
    case VHSUBTYPEDECL:
      if (mTypeBrowseEnable) {
        status = browseSubtypeDecl(node);
      }
      break;
    case VHSUBTYPEIND:
      if (mTypeBrowseEnable) {
        status = browseSubtypeIndication(node);
      }
      break;
    case VHTYPEDECL:
      if (mTypeBrowseEnable) {
        status = browseTypeDecl(node);
      }
      break;
    case VHSELDECL:
      if (mTypeBrowseEnable) {
        status = browseSelectDecl(node);
      }
      break;
    case VHINDEXCONSTRAINT:
      if (mTypeBrowseEnable) {
        status = browseIndexConstraint(node);
      }
      break;
    case VHINDSUBTYPEDEF:
      if (mTypeBrowseEnable) {
        status = browseIndexUnconstrained(node);
      }
      break;
    case VHRANGECONSTRAINT:
      if (mTypeBrowseEnable) {
        status = browseRangeConstraint(node);
      }
      break;
    case VHELEMENTDECL:
      if (mTypeBrowseEnable) {
        status = browseRecordElement(node);
      }
      break;
    case VHPHYSICALTYPE: // Physical type not supported.
    case VHACCESSTYPE: // Access type not supported.
    case VHTYPEDEF: // Base type. All derived types are covered.
    case VHARRAY: // Base type. All derived types are covered.
      if (mTypeBrowseEnable) {
        invalidNode(node);
        status = JaguarBrowserCallback::eInvalid;
      }
      break;
      // There's no support for these yet, and are ignored.
    case VHTERMBIT:
    case VHTERMINAL:
    case VHPORTBIT:
    case VHPRIMUNIT:
    case VHPHYSICALLIT:
    case VHNETBIT:
    case VHNULLLIT:
    case VHOPEN:
    case VHOPENENTITYASPECT:
    case VHOPERATOR:
    case VHOPSTRING:
    case VHCOMMENT:
    case VHBINDIND:
    case VHALLOCATOR:
    case VHALL:
    case VHOTHERS:
    case VHGROUPDECL:
    case VHGROUPTEMPLATE:
    case VHGENMAPASPECT:
    case VHENTITYASPECT:
    case VHENTITYDESIGNATOR:
    case VHCONFIGDECL:
    case VHPACKAGEDECL:
    case VHSIGNATURE:
    case VHSUBPROGDECL:
      break;
    case VHWAVEFORM:
    case VHWAVEFORMELEMENT:
    case VHPORT:
    case VHGENERIC:
    case VHSECUNIT:
    case VHUSECLAUSE:
    case VHPROCESS:
    case VHLIBRARYCLAUSE:
    case VHINSTANCE:
    case VHIFGENERATE:
    case VHGENERATESTMNT:
    case VHFORGENERATE:
    case VHENTITYINSTANCE:
    case VHENTITY:
    case VHDESIGNUNIT:
    case VHDISCONNECTSPEC:
    case VHCONFINSTANCE:
    case VHCONFIGSPEC:
    case VHCOMPONENT:
    case VHCONDSIGASGN:
    case VHCONDWAVEFORM:
    case VHCONCASSERT:
    case VHCONCPROCCALL:
    case VHCONCSIGASGNSTMNT:
    case VHCOMPCONFIG:
    case VHCOMPINSTANCE:
    case VHBLOCKCONFIG:
    case VHSUBPROGBODY:
      // All derived types are covered.
    case VHSEQSTMNT:
      // All derived types are covered.
    case VHLIST:
      // List should be sent to browseList and not browse.
    default:
      invalidNode(node);
      status = JaguarBrowserCallback::eInvalid;
      break;
    }
  } // if
  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseList(JaguarList& list)
{
  JaguarBrowserCallback::Status status = JaguarBrowserCallback::eNormal;
  vhNode node = 0;
  while ((node = vhGetNextItem(list)))
  {
    status = browse(node);
    CHECK_STATUS(status, node);
  }
  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseVarAsgn(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhvarasgn(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode rval = static_cast<vhNode>(vhGetSource(node));
  status = browse(rval);
  CHECK_STATUS(status, node);

  vhNode lval = static_cast<vhNode>(vhGetTarget(node));
  status = browse(lval);
  CHECK_STATUS(status, node);

  status = mCallback.vhvarasgn(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseSigAsgn(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhsigasgn(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode waveform = vhGetWaveFrm(node);
  vhNode waveform_ele = vhGetNodeAt(0,vhGetWaveFrmEleList(waveform));
  vhNode rval = static_cast<vhNode>(vhGetValueExpr(waveform_ele));
  status = browse(rval);
  CHECK_STATUS(status, node);

  vhNode lval = static_cast<vhNode>(vhGetTarget(node));
  status = browse(lval);
  CHECK_STATUS(status, node);

  status = mCallback.vhsigasgn(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseIf(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhif(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode if_cond = static_cast<vhNode>(vhGetCondition(node));
  status = browse(if_cond);
  CHECK_STATUS(status, node);

  JaguarList then_list(vhGetThenSeqStmtList(node));
  status = browseList(then_list);
  CHECK_STATUS(status, node);

  JaguarList elsif_list(vhGetElsifSeqStmtList(node));
  status = browseList(elsif_list);
  CHECK_STATUS(status, node);

  JaguarList els_list(vhGetElsSeqStmtList(node));
  status = browseList(els_list);
  CHECK_STATUS(status, node);

  status = mCallback.vhif(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseElsIf(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhelsif(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode elsif_cond = static_cast<vhNode>(vhGetCondition(node));
  status = browse(elsif_cond);
  CHECK_STATUS(status, node);

  JaguarList then_list(vhGetThenSeqStmtList(node));
  status = browseList(then_list);
  CHECK_STATUS(status, node);

  status = mCallback.vhelsif(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseFor(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhfor(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode for_index = vhGetParamSpec(node);
  status = browse(for_index);
  CHECK_STATUS(status, node);

  vhNode loop_stmt = vhGetLoopStmt(node);
  status = browse(loop_stmt);
  CHECK_STATUS(status, node);

  status = mCallback.vhfor(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseWhile(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhwhile(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode cond = static_cast<vhNode>(vhGetCondition(node));
  status = browse(cond);
  CHECK_STATUS(status, node);

  vhNode loop_stmt = vhGetLoopStmt(node);
  status = browse(loop_stmt);
  CHECK_STATUS(status, node);

  status = mCallback.vhwhile(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseLoop(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhloop(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  JaguarList loop_body(vhGetSeqStmtList(node));
  status = browseList(loop_body);
  CHECK_STATUS(status, node);

  status = mCallback.vhloop(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseReturn(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhreturn(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode cond = static_cast<vhNode>(vhGetCondition(node));
  status = browse(cond);
  CHECK_STATUS(status, node);

  status = mCallback.vhreturn(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseProcedureCall(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhprocedurecall(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  JaguarList assocList(vhGetAssocList(node));
  status = browseList(assocList);
  CHECK_STATUS(status, node);

  status = mCallback.vhprocedurecall(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseWait(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhwait(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode cond = static_cast<vhNode>(vhGetConditionClause(node));
  status = browse(cond);
  CHECK_STATUS(status, node);

  status = mCallback.vhwait(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseCase(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhcase(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode sel = static_cast<vhNode>(vhGetCaseExpr(node));
  status = browse(sel);
  CHECK_STATUS(status, node);

  JaguarList caseitems(vhGetAlternativeList(node));
  status = browseList(caseitems);
  CHECK_STATUS(status, node);

  status = mCallback.vhcase(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseCaseAlter(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhcasealter(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  JaguarList caseitemchoices(vhGetChoiceList(static_cast<vhExpr>(node)));
  status = browseList(caseitemchoices);
  CHECK_STATUS(status, node);

  JaguarList caseitemstmts(vhGetSeqStmtList(node));
  status = browseList(caseitemstmts);
  CHECK_STATUS(status, node);

  status = mCallback.vhcasealter(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseNext(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhnext(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode cond = static_cast<vhNode>(vhGetCondition(node));
  status = browse(cond);
  CHECK_STATUS(status, node);

  status = mCallback.vhnext(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseExit(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhexit(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode cond = static_cast<vhNode>(vhGetCondition(node));
  status = browse(cond);
  CHECK_STATUS(status, node);

  status = mCallback.vhexit(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseNull(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhnull(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhnull(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseAssert(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhassert(JaguarBrowserCallback::ePre, node); // this_assert_OK

  CHECK_STATUS(status, node);

  status = mCallback.vhassert(JaguarBrowserCallback::ePost, node);  // this_assert_OK
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseReport(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhreport(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhreport(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseCharLit(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhcharlit(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhcharlit(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseDecLit(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhdeclit(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhdeclit(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseBaseLit(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhbaselit(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhbaselit(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseIdeNumLit(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhidenumlit(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhidenumlit(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseString(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhstring(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhstring(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseBitString(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhbitstring(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhbitstring(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseObject(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhobject(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode actual_obj = vhGetActualObj(node);
  status = browse(actual_obj);
  CHECK_STATUS(status, node);

  status = mCallback.vhobject(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseExternal(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhexternal(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode actual_obj = vhGetActualObj(node);
  status = browse(actual_obj);
  CHECK_STATUS(status, node);

  status = mCallback.vhexternal(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseForIndex(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhforindex(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  if (mDeclInitialBrowseEnable) {
    vhExpr initVal = vhGetInitialValue(node);
    status = browse(initVal);
    CHECK_STATUS(status, node);
  }

  if (mTypeBrowseEnable) {
    vhNode sigType = vhGetType(node);
    status = browse(sigType);
    CHECK_STATUS(status, node);
  }

  status = mCallback.vhforindex(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseAliasDecl(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhaliasdecl(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhaliasdecl(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseAttrbDecl(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhattrbdecl(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhattrbdecl(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseAttrbSpec(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhattrbspec(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhattrbspec(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseSignal(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhsignal(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  if (mDeclInitialBrowseEnable) {
    vhExpr initVal = vhGetInitialValue(node);
    status = browse(initVal);
    CHECK_STATUS(status, node);
  }

  if (mTypeBrowseEnable) {
    vhNode sigType = vhGetSubTypeInd(node);
    status = browse(sigType);
    CHECK_STATUS(status, node);
  }

  status = mCallback.vhsignal(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseVariable(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhvariable(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  if (mDeclInitialBrowseEnable) {
    vhExpr initVal = vhGetInitialValue(node);
    status = browse(initVal);
    CHECK_STATUS(status, node);
  }

  if (mTypeBrowseEnable) {
    vhNode sigType = vhGetSubTypeInd(node);
    status = browse(sigType);
    CHECK_STATUS(status, node);
  }

  status = mCallback.vhvariable(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseFileDecl(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhfiledecl(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  if (mDeclInitialBrowseEnable) {
    vhExpr initVal = vhGetInitialValue(node);
    status = browse(initVal);
    CHECK_STATUS(status, node);
  }

  if (mTypeBrowseEnable) {
    vhNode sigType = vhGetSubTypeInd(node);
    status = browse(sigType);
    CHECK_STATUS(status, node);
  }

  status = mCallback.vhfiledecl(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseConstant(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhconstant(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  if (mDeclInitialBrowseEnable) {
    vhExpr initVal;
    if (vhIsDeferred(node)) {
      initVal = vhGetDeferredConstantValue(node);
    }
    else {
      initVal = vhGetInitialValue(node);
    }
    status = browse(initVal);
    CHECK_STATUS(status, node);
  }

  if (mTypeBrowseEnable) {
    vhNode sigType = vhGetSubTypeInd(node);
    status = browse(sigType);
    CHECK_STATUS(status, node);
  }

  status = mCallback.vhconstant(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseBinary(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhbinary(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode left = static_cast<vhNode>(vhGetLeftOperand(static_cast<vhExpr>(node)));
  status = browse(left);
  CHECK_STATUS(status, node);

  vhNode right = static_cast<vhNode>(vhGetRightOperand(static_cast<vhExpr>(node)));
  status = browse(right);
  CHECK_STATUS(status, node);

  status = mCallback.vhbinary(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseUnary(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhunary(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode opr = static_cast<vhNode>(vhGetOperand(static_cast<vhExpr>(node)));
  status = browse(opr);
  CHECK_STATUS(status, node);

  status = mCallback.vhunary(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseIndName(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhindname(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  JaguarList indexList(vhGetExprList(static_cast<vhExpr>(node)));
  status = browseList(indexList);
  CHECK_STATUS(status, node);

  vhNode prefix = static_cast<vhNode>(vhGetPrefix(static_cast<vhExpr>(node)));
  status = browse(prefix);
  CHECK_STATUS(status, node);

  status = mCallback.vhindname(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseSliceName(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhslicename(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode range = vhGetDisRange(static_cast<vhExpr>(node));
  status = browse(range);
  CHECK_STATUS(status, node);

  vhNode prefix = static_cast<vhNode>(vhGetPrefix(static_cast<vhExpr>(node)));
  status = browse(prefix);
  CHECK_STATUS(status, node);

  status = mCallback.vhslicename(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseSliceNode(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhslicenode(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode actual = static_cast<vhNode>(vhGetActual(node));
  status = browse(actual);
  CHECK_STATUS(status, node);

  status = mCallback.vhslicenode(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseFuncNode(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhfuncnode(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode actual = static_cast<vhNode>(vhGetActual(node));
  status = browse(actual);
  CHECK_STATUS(status, node);

  status = mCallback.vhfuncnode(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseAssociation(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhassociation(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode actual = static_cast<vhNode>(vhGetActual(node));
  status = browse(actual);
  CHECK_STATUS(status, node);

  status = mCallback.vhassociation(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseSelectedName(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhselectedname(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  JaguarList expr_list(vhGetExprList(static_cast<vhExpr>(node)));
  status = browseList(expr_list);
  CHECK_STATUS(status, node);

  vhNode actual_obj = static_cast<vhNode>(vhGetActualObj(node));
  status = browse(actual_obj);
  CHECK_STATUS(status, node);

  status = mCallback.vhselectedname(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseFuncCall(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhfunccall(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  JaguarList actual_list(vhGetAssocList(node));
  status = browseList(actual_list);
  CHECK_STATUS(status, node);

  status = mCallback.vhfunccall(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseTypeConv(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhtypeconv(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode opr = static_cast<vhNode>(vhGetOperand(static_cast<vhExpr>(node)));
  status = browse(opr);
  CHECK_STATUS(status, node);

  status = mCallback.vhtypeconv(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseQExpr(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhqexpr(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode opr = static_cast<vhNode>(vhGetOperand(static_cast<vhExpr>(node)));
  status = browse(opr);
  CHECK_STATUS(status, node);

  status = mCallback.vhqexpr(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseAggregate(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhaggregate(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  JaguarList assoc_list(vhGetEleAssocList(static_cast<vhExpr>(node)));
  status = browseList(assoc_list);
  CHECK_STATUS(status, node);

  status = mCallback.vhaggregate(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseElementAss(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhelementass(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  JaguarList choice_list(vhGetChoiceList(static_cast<vhExpr>(node)));
  status = browseList(choice_list);
  CHECK_STATUS(status, node);

  vhNode assoc_expr = static_cast<vhNode>(vhGetExpr(node));
  status = browse(assoc_expr);
  CHECK_STATUS(status, node);

  status = mCallback.vhelementass(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseAttrbName(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhattrbname(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode prefix = static_cast<vhNode>(vhGetPrefix(static_cast<vhExpr>(node)));
  status = browse(prefix);
  CHECK_STATUS(status, node);

  vhNode expr = static_cast<vhNode>(vhGetExpr(static_cast<vhExpr>(node)));
  status = browse(expr);
  CHECK_STATUS(status, node);

  status = mCallback.vhattrbname(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseRange(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhrange(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  if (vhIsRangeAttribute(node) == VH_TRUE)
  {
    vhNode range_node = static_cast<vhNode>(vhGetRangeAttrib(node));
    status = browse(range_node);
    CHECK_STATUS(status, node);
  }
  else
  {
    vhExpr left = vhGetLtOfRange(node);
    JaguarBrowserCallback::Status status = 
      mCallback.vhrangeleft(JaguarBrowserCallback::ePre, left);
    CHECK_STATUS(status, node);
    status = browse(left);
    CHECK_STATUS(status, node);
    status = mCallback.vhrangeleft(JaguarBrowserCallback::ePost, left);
    CHECK_STATUS(status, node);
    vhExpr right = vhGetRtOfRange(node);
    status = mCallback.vhrangeright(JaguarBrowserCallback::ePre, right);
    CHECK_STATUS(status, node);
    status = browse(right);
    CHECK_STATUS(status, node);
    status = mCallback.vhrangeright(JaguarBrowserCallback::ePost, right);
    CHECK_STATUS(status, node);
  }

  status = mCallback.vhrange(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseSimpleName(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhsimplename(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhsimplename(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

/*
  The constrained array is made up of VHSUBTYPEIND ..subtype indication, for the
  array element type, and VHINDEXCONSTRAINT list ..constrained indices, for every
  dimension of the array.
*/
JaguarBrowserCallback::Status JaguarBrowser::browseConstrainedArray(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhconstrainedarray(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode subType = vhGetEleSubTypeInd(node);
  status = browse(subType);
  CHECK_STATUS(status, node);

  vhNode constraint = vhGetIndexConstr(node);
  status = browse(constraint);
  CHECK_STATUS(status, node);

  status = mCallback.vhconstrainedarray(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

/*
  The unconstrained array is made up of VHSUBTYPEIND ..subtype indication, for the
  array element type, and VHINDSUBTYPEDEF list ..unconstrained indices, for every
  dimension of unconstrained array.
*/
JaguarBrowserCallback::Status JaguarBrowser::browseUnconstrainedArray(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhunconstrainedarray(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode subType = vhGetEleSubTypeInd(node);
  status = browse(subType);
  CHECK_STATUS(status, node);

  JaguarList indices(vhGetIndexSubTypeDefList(node));
  status = browseList(indices);
  CHECK_STATUS(status, node);

  status = mCallback.vhunconstrainedarray(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseEnumElement(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhenumelement(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  status = mCallback.vhenumelement(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseEnumType(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhenumtype(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  JaguarList enumElems(vhGetEnumEleList(node));
  status = browseList(enumElems);
  CHECK_STATUS(status, node);

  vhNode range = vhGetRangeConstraint(node);
  status = browse(range);
  CHECK_STATUS(status, node);

  status = mCallback.vhenumtype(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseFileType(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhfiletype(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode typ = vhGetFileType(node);
  status = browse(typ);
  CHECK_STATUS(status, node);

  status = mCallback.vhfiletype(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseFloatType(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhfloattype(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode constraint = vhGetRangeConstraint(node);
  status = browse(constraint);
  CHECK_STATUS(status, node);

  status = mCallback.vhfloattype(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

JaguarBrowserCallback::Status JaguarBrowser::browseIntType(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhinttype(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode constraint = vhGetRangeConstraint(node);
  status = browse(constraint);
  CHECK_STATUS(status, node);

  status = mCallback.vhinttype(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

/*
  Browse record declaration.
*/
JaguarBrowserCallback::Status JaguarBrowser::browseRecord(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhrecord(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  JaguarList eleList(vhGetRecEleList(node));
  status = browseList(eleList);
  CHECK_STATUS(status, node);

  status = mCallback.vhrecord(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

/*
  Browse record field declaration.
*/
JaguarBrowserCallback::Status JaguarBrowser::browseRecordElement(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhrecordelement(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode recElem = vhGetEleSubTypeInd(node);
  status = browse(recElem);
  CHECK_STATUS(status, node);

  status = mCallback.vhrecordelement(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

/*
  The type declaration (VHTYPEDECL) creates a new type while subtype declaration
  (VHSUBTYPEDECL) creates a type that is a constraint of an existing type.
  Syntax:  subtype identifier is subtype_indication;
  Example: subtype small_int is integer range 0 to 10 ;

  Here "integer range 0 to 10" is subtype indication (VHSUBTYPEIND).
*/
JaguarBrowserCallback::Status JaguarBrowser::browseSubtypeDecl(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhsubtypedecl(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode subTypeInd = vhGetSubTypeInd(node);
  status = browse(subTypeInd);
  CHECK_STATUS(status, node);

  status = mCallback.vhsubtypedecl(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

/*
  A subtype indication (VHSUBTYPEIND) is made up of a type and a constraint or range
  ..which is optional. 
*/
JaguarBrowserCallback::Status JaguarBrowser::browseSubtypeIndication(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhsubtypeindication(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode origType = vhGetOrgType(node);
  status = browse(origType);
  CHECK_STATUS(status, node);

//   vhNode typeDef = vhGetTypeDef(node);
//   status = browse(typeDef);
//   CHECK_STATUS(status, node);

  vhNode range = vhGetConstraint(node);
  status = browse(range);
  CHECK_STATUS(status, node);

  status = mCallback.vhsubtypeindication(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

/*
  The type declaration is made up of the type name and type definition.
  Syntax: type <type name> is <type definition>
  Example: type rec_arr is array(3 downto 0) of rec;

  Here type name is rec_arr and type definition is "array(3 downto 0) of rec".
  The type definition is of type VHTYPEDEF.
 */
JaguarBrowserCallback::Status JaguarBrowser::browseTypeDecl(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhtypedecl(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  /*
    The type definition. For example: "array(3 downto 0) of rec". Here the type
    definition is of type VHCONSARRAY. The other types of type definitions are:
    VHACCESSTYPE, VHCONSARRAY, VHENUMTYPE, VHFILETYPE, VHFLOATTYPE, VHINTTYPE,
    VHPHYSICALTYPE, VHRECORD, VHUNCONSARRAY.
  */
  vhNode typeDef = vhGetTypeDef(node);
  status = browse(typeDef);
  CHECK_STATUS(status, node);

  status = mCallback.vhtypedecl(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

/*
  Select declaration like signal sig : ieee.std_logic_1164.std_ulogic;
*/
JaguarBrowserCallback::Status JaguarBrowser::browseSelectDecl(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhselectdecl(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  // Returns vhExpr which usually is VHSELECTEDNAME.
  vhNode selname = vhGetSelDeclExpr(node);
  status = browse(selname);
  CHECK_STATUS(status, node);

  status = mCallback.vhselectdecl(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

/*
  Index constraint like: type xyz is integer 3 to 15, where 3 to 15 is index constraint.
*/
JaguarBrowserCallback::Status JaguarBrowser::browseIndexConstraint(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhindexconstraint(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  JaguarList rangeList(vhGetDiscRangeList(static_cast<vhExpr>(node)));
  status = browseList(rangeList);
  CHECK_STATUS(status, node);

  status = mCallback.vhindexconstraint(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

/*
  Range type of unconstrained array.
  Example: type rec_array is array(natural range <>) of rec;

  Here "natural range <>" represent an unconstrained index range.
*/
JaguarBrowserCallback::Status JaguarBrowser::browseIndexUnconstrained(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhindexunconstrained(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode type = vhGetType(node);
  status = browse(type);
  CHECK_STATUS(status, node);

  status = mCallback.vhindexunconstrained(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

/*
  Range constraint for a type.
  Example: type my_int is integer range 0 to 3;

  Here "range 0 to 3" is range constraint.
*/
JaguarBrowserCallback::Status JaguarBrowser::browseRangeConstraint(vhNode node)
{
  JaguarBrowserCallback::Status status = 
    mCallback.vhrangeconstraint(JaguarBrowserCallback::ePre, node);

  CHECK_STATUS(status, node);

  vhNode range = vhGetRange(node);
  status = browse(range);
  CHECK_STATUS(status, node);

  status = mCallback.vhrangeconstraint(JaguarBrowserCallback::ePost, node);
  CHECK_STATUS(status, node);

  return status;
}

void JaguarBrowser::invalidNode(vhNode node)
{
  // Add source location.
  UtString buf;
  buf << vhGetSourceFileName(node) << ":" << vhGetLineNumber(node);
  buf << " Node type not supported : ";

  JaguarUserString str(vhStrDecompileNode(node));
  buf << str;
  StrToken tok(buf.c_str(), "\n");
  // Just use the first line from decompile, otherwise
  // we could be printing a lot, depending on node type.
  INFO_ASSERT(0, *tok);
}
