// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "Decompile.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"
#include "util/UtHashMap.h"
#include <ctype.h>              // isdigit
#include "iodb/IODBNucleus.h"
#include "util/UtIOStream.h"
#include "util/Zstream.h"
#include "util/OSWrapper.h"
#include "compiler_driver/Interra.h"

// Obfuscation methods for writing out encrypted models.  We must
// exclude the top level module name and all its ports.  Those names
// must not be obfsucated.
//
// Also excluded are any names mentioned in directives (module names,
// net names, instance names)
//
// All other names are obfuscated, but a table is used so to translate
// directives.  


// Protect the top level module name and all its ports
bool Decompile::protectModuleInterface(const char* modName)
{
  veNode file, module;
  CheetahList fileList(veRootGetFileList( mRoot ));
  while ((file = veListGetNextNode(fileList)) != NULL)
  {
    CheetahList moduleList(veFileGetModuleList( file ));
    while ((module = veListGetNextNode(moduleList)) != NULL)
    {
      CheetahStr mname(veModuleGetName(module));
      if ((veNodeGetObjType(module) == VE_MODULE) &&
          (strcmp(mname, modName) == 0))
      {
        protectName(modName);
        CheetahList ports(veModuleGetPortList(module));
        protectPortList(ports);
        return true;
      }
    }

    CheetahList udpList(veFileGetUdpList( file ));
    while ((module = veListGetNextNode(udpList)) != NULL)
    {
      CheetahStr mname(veUDPGetName(module));
      if ((veNodeGetObjType(module) == VE_UDP) &&
          (strcmp(mname, modName) == 0))
      {
        protectName(modName);
        CheetahList ports(veUDPGetPortList(module));
        protectPortList(ports);
        return true;
      }
    }
  } // while
  return false;
} // bool Decompile::protectModuleInterface

void Decompile::protectPortList(CheetahList& ports) {
  veNode port;
  while ((port = veListGetNextNode(ports)) != NULL) {
    CheetahStr portName(vePortGetName(port));
    if (portName != NULL) {
      protectName(portName);
    }
  }
}

void Decompile::protectName(const char* name)
{
  INFO_ASSERT(name, "Decompile::protectName: cannot protect NULL name");
  StringAtom* atom = mAtomicCache->intern(name);
  mProtected->insert(atom);
}

// Obfuscate a name, unless it is protected.  Always obfuscate the
// same name the same way.  Keep track of the name map, which we
// will write out to the encrypted database.
void Decompile::obfuscate(const char* name)
{
  INFO_ASSERT(name, "Decompile::obfuscate: cannot obfuscate NULL name");
  StringAtom* atom = mAtomicCache->intern(name);
    
  if (! mObfuscationEnabled || mIODB->isDirectiveName(name) ||
      (mProtected->find(atom) != mProtected->end()) ||
      (mEncryptedOutputFile == NULL))
  {
    writeOutput(name, 0);
  }
  else {
    INFO_ASSERT(!isdigit(*name), name);
    UInt32 index = 0;
    NameMap::iterator p = mNameMap->find(atom);
    if (p == mNameMap->end())
    {
      index = mNameMap->size();
      (*mNameMap)[atom] = index;
    }
    else
      index = p->second;
    *this << eObfuscate;
    *mEncryptedOutputFile << index;
  } // else
} // const char* Decompile::obfuscate

void Decompile::readObfuscate(UtString* buf, UInt32 index)
{
  if (index >= mObfuscations.size())
    mObfuscations.resize(index + 1);
  StringAtom* atom = mObfuscations[index];

  if (atom == NULL)
  {
    // Find a unique name for obfuscation.  We can't easily
    // determine if this conflicts with an existing name parsed
    // by Cheetah, so use a unique prefix
    UtString name("Carbon_IP_");
    size_t prefixLen = name.size();
    do {
      name.resize(prefixLen);
      name << mObfuscationIndex++;
      name << "x";              // helps sed get the right answer
    } while (mAtomicCache->getIntern(name.c_str()) != NULL);
    atom = mAtomicCache->intern(name.c_str());
    mObfuscations[index] = atom;
  }

  buf->append(atom->str());
} // void Decompile::readObfuscate

void Decompile::writeObfuscationTable()
{
  // We have a map that goes from names to indices.  Make one that
  // goes the other way so we can write it out in order
  UtVector<StringAtom*> reverseMap(mNameMap->size());
  for (NameMap::iterator p = mNameMap->begin(); p != mNameMap->end(); ++p)
  {
    StringAtom* name = p->first;
    UInt32 index = p->second;
    reverseMap[index] = name;
  }

  // Write out the names in order
  *mEncryptedOutputFile << reverseMap.size();
  for (size_t i = 0; i < reverseMap.size(); ++i)
  {
    StringAtom* name = reverseMap[i];
    *mEncryptedOutputFile << name->str();
  }
}

bool Decompile::readObfuscationTable(const char* filename)
{
  UInt32 numEntries;
  bool ret = false;
  UtOBStream ofile(filename);

  if (not ofile.is_open()) {
    fprintf(stderr, "%s\n", ofile.getErrmsg());
    return false;
  }

  if (*mInputFile >> numEntries)
  {
    ret = true;
    UtString buf;

    ofile << "sed ";

    for (UInt32 i = 0; i < numEntries; ++i)
    {
      buf.clear();
      if (! (*mInputFile >> buf))
      {
        // write out a sed script that will 
        ret = false;
        ofile << "string read error\n";
        break;
      }
      else
        ofile << "\\\n  -e 's|"
	      << mObfuscations[i]->str() << "|"
	      << buf.c_str() << "|@g' ";
    }
    ofile << "\n";
  } // if
  
  if (not ofile.close()) {
    fprintf(stderr, "%s\n", ofile.getErrmsg());
    ret = false;
  }

  return ret;
} // bool Decompile::readObfuscationTable
