// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include <errno.h>
#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/JaguarContext.h"
#include "compiler_driver/VhdlPortTypeMap.h"
#include "localflow/DesignPopulate.h"
#include "nucleus/NUDesign.h"
#include "util/HdlFileCollector.h"
#include "util/OSWrapper.h"

static CarbonContext* sCarbonContext = NULL;
static const char* scJaguarMem = NULL;
static const char* scVhdlNoWriteLib = NULL;

// These need to be seen by multiple files
const char *JaguarContext::scVhdlLib = "-vhdlLib";
const char *JaguarContext::scVhdlTop = "-vhdlTop";
const char *JaguarContext::scVhdlCompile = "-vhdlCompile";
const char *JaguarContext::scDefaultVhdlLib = "WORK";
const char *JaguarContext::sc87 = "-87";
const char *JaguarContext::sc93 = "-93";
const char *JaguarContext::scSuppress = "-suppress";
const char *JaguarContext::scSynthPrefix = "-vhdl_synth_prefix";
const char *JaguarContext::scVhdlCase = "-vhdlCase";
const char *JaguarContext::scVhdlExt = "-vhdlExt";
const char *JaguarContext::scVhdlErrorStack = "-vhdlErrorStack";
const char *JaguarContext::scVhdlErrorStackLimit = "-vhdlErrorStackLimit";
const char *JaguarContext::scVhdlRecursionLimit = "-vhdlRecursionLimit";
const char *JaguarContext::scNoPplConstProp = NULL;
const char *JaguarContext::scNoVhdlConstPropFuncCalls = NULL;
const char *JaguarContext::scAutoCompileOrder="-autoCompileOrder";
const char *JaguarContext::scNoAutoCompileOrder="-noAutoCompileOrder";

const SInt32 JaguarContext::scMaxRecursionLimit = 1000;

void gJaguarAssert(const char* msg, const char* type, vhNode obj)
{
  if (obj != NULL)
    UtIO::cout() << vhGetFileName(obj) << ":" << (UInt32)vhGetLineNumber(obj) << ": ";
  UtIO::cout() << msg << " - " << type << UtIO::endl;
  vhFreeJaguarLicense();
  INFO_ASSERT(0, "Parse failed.");
}

JaguarContext::JaguarContext(CarbonContext* cc)
{
  INFO_ASSERT(sCarbonContext == NULL, "only one Jaguar context may be active at a time");
  sCarbonContext = cc;
  mDesign = NULL;
  mCarbonContext = cc;
  ArgProc* args = cc->getArgs();
  scJaguarMem = CRYPT("-jaguarMem");
  scVhdlNoWriteLib = CRYPT("-vhdlNoWriteLib");
  scNoPplConstProp = CRYPT("-noPopulationConstProp");
  scNoVhdlConstPropFuncCalls = CRYPT("-noVhdlConstPropFuncCalls");
  
  // Initialize case mode
  mCase = eVHDLLower;

  args->addInt( scJaguarMem,
                CRYPT("Jaguar memory debug level"),
                0, false, false, CarbonContext::ePassCarbon );
  args->addString( scVhdlTop, 
                   "Top entity name in VHDL flow.",
                   NULL, true, false, CarbonContext::ePassCarbon );
  args->addToSection( CarbonContext::scVHDL, scVhdlTop );
  
  args->addString( scVhdlLib,
                   "Specify a VHDL library.  Multiple instances of this switch are allowed.  The last specified library is the WORK library for the design.  VHDL source files will be compiled into the last preceding library specified on the command line.  VHDL files preceding all occurrences of -vhdlLib will be compiled into the default library.",
                   "WORK:lib<design>.WORK", false, true, CarbonContext::ePassHdl );
  args->addToSection( CarbonContext::scVHDL, scVhdlLib );
  args->addBool( scVhdlCompile,
                 "This option is deprecated, see the -compileLibOnly option instead.  Old documentation: VHDL compile-only mode.  The VHDL source files will be compiled into their library on disk.  No Carbon Model will be created.",
                 false, CarbonContext::ePassMode );
  args->addToSection( CarbonContext::scVHDL, scVhdlCompile );
  args->putIsDeprecated(scVhdlCompile, true);
  args->addBool( scVhdlNoWriteLib,
                 "Do not store any parsed VHDL design units on disk, and do not create any new disk libraries.  Any preexisting user libraries will remain unmodified.  System libraries will remain unaffected.",
                 false, CarbonContext::ePassCarbon );
  args->addToSection( CarbonContext::scVHDL, scVhdlNoWriteLib );
  args->addUnprocessedBool(CarbonContext::scUnprocVHDL, 
                           CRYPT("-implicit"), 
                           CRYPT("Jaguar-specific option to implicitly define operators needed for compilation of the central VHDL libraries."), CarbonContext::ePassCarbon );
  args->addUnprocessedBool(CarbonContext::scUnprocVHDL, 
                           CRYPT("-noMax"),
                           CRYPT("Jaguar-specific option to disable errors for integer overflow; needed for compilation of some central VHDL libraries."),
                           CarbonContext::ePassCarbon );
  args->addBool( sc87,
                 CRYPT("Use VHDL-87 syntax for all following VHDL files until a \"-93\" is encountered.  If not specified, all files are parsed with VHDL-93 syntax rules."), 
                 false, CarbonContext::ePassHdl );
  args->addToSection( CarbonContext::scVHDL, sc87 );
  args->addBool( sc93,
                 CRYPT("Use VHDL-93 syntax for all following VHDL files until a \"-87\" is encountered.  If not specified, all files are parsed with VHDL-93 syntax rules."), 
                 true, CarbonContext::ePassHdl );
  args->addToSection( CarbonContext::scVHDL, sc93 );
  args->addUnprocessedString( CarbonContext::scUnprocVHDL, scSuppress, "Use this option to selectively suppress certain VHDL parser warning messages.  This option is valid for messages between 30000 and 39999.  You must remove the leading '3' from the message numbers.  Syntax: -suppress <m1>:<m2>:...", NULL, true, false, CarbonContext::ePassCarbon );
  args->putIsDeprecated(scSuppress, true);
  args->addString( scSynthPrefix, "Specify a prefix for synthesis specific compiler directives.  Multiple instances of this switch are allowed.", NULL, true, true, CarbonContext::ePassCarbon );
  args->addToSection( CarbonContext::scVHDL, scSynthPrefix );
  args->addString(scVhdlCase,
                  "Specifies treatment of VHDL identifiers.  Legal values are lower, upper, or preserve.", "lower", false, false, CarbonContext::ePassCarbon);
  args->addToSection( CarbonContext::scVHDL, scVhdlCase );

  args->addString(scVhdlExt, "Specifies colon-separated list of extensions used to identify vhdl files. The '.' in the extension is optional. The list of extensions augments the .vhd and .vhdl extensions. The extensions are case-insensitive.", "", true, true, CarbonContext::ePassCarbon);
  args->addToSection( CarbonContext::scVHDL, scVhdlExt );
  args->addBool(CarbonContext::scVhdlLoopUnroll,
                CRYPT("Force unrolling of VHDL loops during population phase."),
                false, CarbonContext::ePassCarbon);
  args->addBool(scVhdlErrorStack,
                CRYPT("Enable dumping of source location stack when error occurs."),
                false, CarbonContext::ePassCarbon);
  args->addInt(scVhdlErrorStackLimit,
               CRYPT("Limit the maximum number of stack source locations dumped."),
               10, false, false, CarbonContext::ePassCarbon);
  args->addInt(scVhdlRecursionLimit,
               CRYPT("Set the limit for maximum number of recursions for a recursive function."),
               scMaxRecursionLimit, false, false, CarbonContext::ePassCarbon);
  args->addBool(scNoPplConstProp,
                CRYPT("Disable constant propagation during population phase. Note that this may result in errors during this compile phase."),
                false, CarbonContext::ePassCarbon);
  args->addBool(scNoVhdlConstPropFuncCalls,
                CRYPT("Disable constant propagation of function calls that return constant values during population phase. Note that this may result in errors during this compile phase."),
                false, CarbonContext::ePassCarbon);
  args->addBool(scAutoCompileOrder, CRYPT("Enable automatic compile order detection"), true, CarbonContext::ePassCarbon);
  args->addBoolOverride(scNoAutoCompileOrder, scAutoCompileOrder);
}

JaguarContext::~JaguarContext()
{
  INFO_ASSERT(sCarbonContext == mCarbonContext, "carbon context tampered with during Jaguar session");
  sCarbonContext = NULL;
}


// This list was empiracally determined by reading the jaguar message
// list in the documentation.  Adjustments may be necessary.
static bool
jaguarMsgMustBeErrors( int msgNo )
{
  if (( msgNo == 2 ) ||
      ( msgNo == 5 ) ||
      ( msgNo == 6 ) ||
      ( msgNo == 8 ) ||
      ( msgNo == 11 ) ||
      ( msgNo >= 13 && msgNo <= 19 ) ||
      ( msgNo >= 21 && msgNo <= 83 ) ||
      ( msgNo >= 85 && msgNo <= 110 ) ||
      ( msgNo >= 112 && msgNo <= 135 ) ||
      ( msgNo >= 137 && msgNo <= 169 ) ||
      ( msgNo >= 171 && msgNo <= 244 ) ||
      ( msgNo == 248 ) ||
      ( msgNo >= 250 && msgNo <= 504 ) ||
      ( msgNo == 506 ) ||
      ( msgNo == 512 ) ||
      ( msgNo >= 516 && msgNo <= 530 ) ||
      ( msgNo >= 532 && msgNo <= 553 ) ||
      ( msgNo >= 555 && msgNo <= 576 ) ||
      ( msgNo >= 578 && msgNo <= 580 )
    )
  {
    return true;
  }
  else
  {
    return false;
  }
}


static bool sShowJaguarStatus = false;
static void jaguar_spew(FILE* logFilePtr, vhBoolean /*isSynthMessage*/,
                         vhMessageType severity, int jaguarMsgNo,
                         const char *sourceFileName,
                         int sourceLineNo,
                         const char *format, va_list pvar)
{
  // We've reserved message #s 30000-39999 for Jaguar
  int msgNo = JAG_MSG_OFFSET + jaguarMsgNo;
  MsgContext* msgContext = sCarbonContext->getMsgContext();
  MsgContextBase::Severity sev = MsgContextBase::eNote;
  if (!msgContext->getMsgSeverity(msgNo, &sev))
  {
    switch (severity)
    {
    case VH_INFO:
      if (sShowJaguarStatus)
        sev = MsgContextBase::eNote;
      else
        sev = MsgContextBase::eSuppress;
      break;
    case VH_UNDISPLAY:
      sev = MsgContextBase::eSuppress;      
      break;
    case VH_WARN:
    case VH_SYNTH_WARN:
      if (msgContext->allWarningsSuppressed() ||
          ! sCarbonContext->showJaguarWarnings())
      {
        sev = MsgContextBase::eSuppress;
      }
      else {
        sev = MsgContextBase::eWarning;
      }
      break;
    case VH_ERROR:
    case VH_SYNTH_ERROR:
    case VH_SYSERROR:
    default:
      if ( jaguarMsgMustBeErrors( jaguarMsgNo ))
      {
        sev = MsgContextBase::eError;
      }
      else
      {
        sev = MsgContextBase::eAlert;
      }
      break;
    } // switch
  } // if

  sCarbonContext->logAnalyzerMessages(logFilePtr, sev, msgNo, sourceFileName,
                                      sourceLineNo, format, pvar);
} // static void jaguar_spew


static void jaguar_prepop_spew(FILE* /*logFilePtr*/, vhBoolean /*isSynthMessage*/,
                               vhMessageType severity, int /*msgNo*/,
                               const char* /*sourceFileName*/,
                               int /*sourceLineNo*/,
                               const char* /*format*/, va_list /*pvar*/)
{
  
  switch(severity)
  {
  case VH_ERROR:
  case VH_SYNTH_ERROR:
  case VH_SYSERROR:
    //jaguar_spew(logFilePtr, isSynthMessage, severity, msgNo, sourceFileName, sourceLineNo, format, pvar);
    break;
  default:
    break;
  }
} // static void jaguar_prepop_spew

bool JaguarContext::checkJaguarArgs(MsgContext* msgContext) {
  int numErrors = 0;

  // The files have been associated with their libraries via the
  // ArgProcCallback.  Make sure that they are OK.
  HdlFileCollector::const_iterator lib_iter;
  for ( lib_iter = sCarbonContext->getFileCollector()->begin();
        lib_iter != sCarbonContext->getFileCollector()->end();
        ++lib_iter )
  {
    // Loop over both sets of file in each library
    LangVer vhdlVers[] = {LangVer::VHDL87, LangVer::VHDL93};

    for(UInt32 ver = 0; ver < sizeof(vhdlVers)/sizeof(LangVer); ++ver) {

	HdlLib::const_hdlFileIterator file_iter =  (*lib_iter)->begin( vhdlVers[ver] );

	for (; file_iter != (*lib_iter)->end( vhdlVers[ver] ); ++file_iter )
	{
	    const char *arg = *file_iter;

	    FILE* f;
	    if ((f = OSFOpen(arg, "r", NULL)) != NULL)
		// We can read the file
		fclose(f);
	    else
	    {
		++numErrors;
		UtString errBuf;
		msgContext->VhdlCannotOpenFile( arg, OSGetLastErrmsg(&errBuf));
	    }
	}
    }
  } // end for each library

  return (numErrors == 0);
} // static bool sCheckJaguarArgs;

bool JaguarContext::parseVhdl()
{
  // Work out the options
  ArgProc* args = mCarbonContext->getArgs();
  char** argv = mCarbonContext->getArgv();
  UtString dir, errmsg, result;
  UtStringArgv &unprocOpts = args->lookupUnprocessedGroupOccurrences( "VHDL" );

  UtStringArgv arg;
  arg.push_back( argv[0] );
  for ( int i = 0; i < unprocOpts.getArgc(); ++i )
  {
    const char* option = unprocOpts.getArgv()[i];
    arg.push_back( option );
    if (strcmp(option, "-suppress") == 0) {
      MsgContext* msgContext = sCarbonContext->getMsgContext();
      msgContext->OptionDeprecated("-suppress",
                                   "Use directive silentMsg <msg-number> instead");
    }
  }

  // Logical to physical library mappings for the system libs
#if pfLP64
#define VHDLDIR "vhdl64"
#else
#define VHDLDIR "vhdl"
#endif
  vhAddLogicalLibraryToMap(const_cast<char*>("STD"),
                           const_cast<char*>("$CARBON_HOME/lib/" VHDLDIR "/STD"));
  vhAddLogicalLibraryToMap(const_cast<char*>("IEEE"),
                           const_cast<char*>("$CARBON_HOME/lib/" VHDLDIR "/IEEE"));
  vhAddLogicalLibraryToMap(const_cast<char*>("SYNOPSYS"),
                           const_cast<char*>("$CARBON_HOME/lib/" VHDLDIR "/SYNOPSYS"));
#undef VHDLDIR

  // define meta-comments which jaguar should recognize
  vhDefineMetaComments(VH_TRUE);
  vhAllowCustomizablePragma(VH_TRUE);

  switch(mCase) {
    case eVHDLPreserve:
      vhPreserveActualNameInDump(VH_TRUE);
      vhSetNameCaseType(VH_ASIS); // note -- this slows Jaguar by 20-30%
      break;
    case eVHDLUpper:
    case eVHDLLower:
      break;
  }

  vhAddMetaDirective( "built_in", "BuiltIn" );
  vhAddMetaDirective( "map_to_operator", "OperatorMap" );
  vhAddMetaRegionMarker( "synthesis_off", "synthesis_on", "IgnoreSynthesisRegion" );
  vhAddMetaRegionMarker( "translate_off", "translate_on", "IgnoreSynthesisRegion" );


  // Define our synth pragma prefixes
  vhDefineSynthPrefix( "carbon" );
  ArgProc::StrIter strIter;
  ArgProc::OptionStateT state = args->getStrIter( scSynthPrefix, &strIter );
  if ( state == ArgProc::eKnown )
  {
    while ( !strIter.atEnd( ))
    {
      const char *prefix = *strIter;
      vhDefineSynthPrefix( const_cast<char*>( prefix ));
      ++strIter;
    }
  }

    
  for (const char** p = mCarbonContext->scNetPragmas; *p != NULL; ++p)
    (void) vhCreateUserMetaComment((char*) *p,
                                    VH_DIRECTIVE_PREV,
                                    VH_DECL_OBJ);

  for (const char** p =  mCarbonContext->scModulePragmas; *p != NULL; ++p)
    (void) vhCreateUserMetaComment((char*) *p,
                                    VH_DIRECTIVE_CURR_SCOPE,
                                    VH_SCOPE_OBJ);


  for (const char** p =  mCarbonContext->scFunctionPragmas; *p != NULL; ++p)
    (void) vhCreateUserMetaComment((char*) *p,
                                    VH_DIRECTIVE_CURR_SCOPE,
                                    VH_SCOPE_OBJ);

  (void) vhCreateUserMetaComment((char*)mCarbonContext->scLicenseStr, VH_DIRECTIVE_CURR_SCOPE, VH_SCOPE_OBJ);


  vhDisableSignalHandling(VH_TRUE);
  arg.push_back("-nl");         // don't show jaguar logo
  if (args->getBoolValue("-showParseMessages"))
    sShowJaguarStatus = true;
  else
    arg.push_back("-q");
  if ( args->getBoolValue( scVhdlNoWriteLib ) && !mCarbonContext->isLibraryCompileOnly())
    arg.push_back( "-ns" ); // Don't write libraries to disk

  // Pass the -synth flag to Jaguar in order to do the synthesis related
  // checkings and processing. One of the synthesis related processing 
  // is reset/clock detection .
  if ( !args->getBoolValue( "-noSynth" ))
  {
    arg.push_back("-synth");
  }

  // This call keeps us from seeing stuff inside --pragma translate_off blocks
  vhEnableNoDumpInSynth( VH_TRUE );
  
  adjustSeverities();
  vhRegisterVarArgsMessageHandler(jaguar_spew); 

  //vhSetMixedLanguageCmdLine(VH_TRUE);

  // Enable the flattening of the aggregates. Jaguar flattens the
  // aggregates only when the mixed language mode is on, so we need to
  // enable this all the time.
  vhEnableMixedLanguage( VH_TRUE );
  // Enable Storage of default values at signal/variable declarations
  vhStoreInitialValues( VH_TRUE );
  // Enable the ability to find out how big things are
  vhElaborateExprSizes( VH_TRUE );
  // tell Jaguar to not elaborate non static expression sizes bug5568
  vhDontElabNonStatExprSizes(VH_TRUE);
  // Enable more elaboration capabilities
  vhEnableEntHeaderInOpenDeclRegn( VH_TRUE );
  // Enable evaluation of impure functions if it has properties similar
  // to that of the pure function. In other words, the function is incorrectly
  // called impure by user.
  vhSetActualImpurity(VH_TRUE);

  vhEnablePredefinedAttribSynthCheckStd2004( VH_TRUE );

  vhStoreAbsoluteFileName(VH_TRUE);

  // For the case that a function has multiple returns of different sizes, return NULL
  vhReturnNullSizeForEvalDependReturn(VH_TRUE);

  int status = 0;
  
  // Add all the user-defined libraries to Jaguar
  HdlFileCollector::const_iterator lib_iter;
  for ( lib_iter = mCarbonContext->getFileCollector()->begin();
        lib_iter != mCarbonContext->getFileCollector()->end();
        ++lib_iter )
  {
      const UtString &libName = (*lib_iter)->getLogicalLibrary();
      const UtString &libPath = (*lib_iter)->getLibPath();

      vhAddLogicalLibraryToMap( const_cast<char*>(libName.c_str() ),
	      const_cast<char*>(libPath.c_str() ));

      if ( args->getBoolValue( scVhdlNoWriteLib ) == false ||
	      mCarbonContext->isLibraryCompileOnly() )
      {
	  // Create the physical dump directory for the library
	  const char *libPathCstr = libPath.c_str();
	  OSStatFile( libPathCstr, "ed", &result );
	  if (  result[0] != '1' || result[1] != '1' ) {
	      errmsg.clear();
	      if ( OSMkdir( libPathCstr, 0777, &errmsg ) != 0 ) {
		  mCarbonContext->getMsgContext()->VhdlCannotMkdirLibrary(errmsg.c_str());
	      }
	  }
      }
  }

  for ( lib_iter = mCarbonContext->getFileCollector()->begin();
        lib_iter != mCarbonContext->getFileCollector()->end();
        ++lib_iter )
  {
      if(LangVer::VHDL87 == (*lib_iter)->libraryLanguage() 
         ||
         LangVer::VHDL93 == (*lib_iter)->libraryLanguage()
         ||
         LangVer::LangVer_MAX == (*lib_iter)->libraryLanguage() )
      {
	  const UtString &libName = (*lib_iter)->getLogicalLibrary();
	  vhSetJaguarWorkLibrary( const_cast<char*>( libName.c_str() ));

	  // Get the files that are using the -87 and -93 versions of VHDL
	  LangVer vhdlVers[] = { LangVer::VHDL87, LangVer::VHDL93 };

	  for(UInt32 ver = 0; ver < sizeof(vhdlVers)/sizeof(LangVer); ++ver)
	      //for ( UInt32 ver = VhdlFiles::eFirst; ver <= VhdlFiles::eLast; ++ver)
	  {
	      LangVer langVer = vhdlVers[ver];

	      // Try to determine the compile order automatically.
	      if ( args->getBoolValue( scAutoCompileOrder ) )
	      {
		  // While the algorithm is working, all messages will be suppressed
		  // because the same file could be parsed several times until its
		  // proper place is determined.
		  mCarbonContext->putInPrePopWalk( true );

		  HdlLib::const_iterator file_iter = (*lib_iter)->begin( langVer );

		  // Get the total number of files for this langVer
		  UInt32 filesLeftToCompile = (*lib_iter)->getNumberOfFiles( langVer );

		  // Set the initial number of passes allowed
		  UInt32 numberOfCompilePassesLeft = filesLeftToCompile;

		  // The numberOfCompilePassesLeft is there to prevent the automatic
		  // compile order algorithm from getting stuck in a loop, which
		  // could happen if two or more files fail to compile for any reason.
		  while ( ( file_iter != (*lib_iter)->end( langVer ) ) && 
			  ( numberOfCompilePassesLeft != 0 ) )
		  {
		      if ( langVer == LangVer::VHDL87 )
		      {
			  vhSet87ModeOn( VH_TRUE );
			  // Make the VHDL-87 file decl warning go away in 87-mode
			  vhModifyMsgSeverity( 499, VH_UNDISPLAY );
		      }
		      else
		      {
			  vhSet87ModeOn( VH_FALSE );
			  // Make the VHDL-87 file decls an error in 93-mode
			  vhModifyMsgSeverity( 499, VH_ERROR );
		      }

		      // Try to analyze the file.
		      {
			  UtStringArgv localArg = arg;
			  localArg.push_back( *file_iter );
			  int localArgc = localArg.getArgc();
			  char **localArgv = localArg.getArgv();
			  status = vhJaguarAnalyze(localArgc, const_cast<const char**>(localArgv));
		      }

		      if ( status != 0)
		      {
			  // If the file failed to compile for any reason, move it to the 
			  // bottom of the list and get the next file in the list, if there
			  // is any left.
			  file_iter = (*lib_iter)->placeFileAtBottom( langVer , *file_iter );

			  numberOfCompilePassesLeft--;
		      }
		      else
		      {
			  // The file compiled. Go to the next file in the list.
			  ++file_iter;

			  // One less file left to compile
			  filesLeftToCompile--;

			  // Reset the counter
			  numberOfCompilePassesLeft = filesLeftToCompile;
		      }
		  } // while

		  // Reenable the reporting of Interra messages
		  mCarbonContext->putInPrePopWalk( false );

	      } // if ( args->getBoolValue

	      // Now compile everything in the right order
	      UtStringArgv localArg = arg;
	      bool haveFiles = false;

	      // Place all of this library's files of this VHDL version into the argv array
	      HdlLib::const_iterator file_iter;
	      for ( file_iter = (*lib_iter)->begin( langVer );
		      file_iter != (*lib_iter)->end( langVer );
		      ++file_iter )
	      {
		  localArg.push_back( *file_iter );
		  haveFiles = true;
	      }

	      // Reset the status flag.
	      status = 0;

	      if ( haveFiles == true )
	      {
		  if ( ver == LangVer::VHDL87 )
		  {
		      vhSet87ModeOn( VH_TRUE );
		      // Make the VHDL-87 file decl warning go away in 87-mode
		      vhModifyMsgSeverity( 499, VH_UNDISPLAY );
		  }
		  else
		  {
		      vhSet87ModeOn( VH_FALSE );
		      // Make the VHDL-87 file decls an error in 93-mode
		      vhModifyMsgSeverity( 499, VH_ERROR );
		  }

		  int localArgc = localArg.getArgc();
		  char **localArgv = localArg.getArgv();
		  status |= vhJaguarAnalyze(localArgc, const_cast<const char**>(localArgv));
	      }

	      if ( status != 0 )
	      {
		  // The compilation failed. Cleanup and return.
		  mCarbonContext->getMsgContext()->JaguarParseFailure( libName.c_str() );
		  mCarbonContext->freeInterraMemory();
		  // Dump the list of ordered files
		  if ( args->getBoolValue( scAutoCompileOrder ) )
		  {
		      mCarbonContext->getFileCollector()->printOrderedFileList();
		  }
		  return false;
	      }
	  } // end for each VHDL version
      }
  } // end for each library

  // Dump the list of ordered files
  if ( args->getBoolValue( scAutoCompileOrder ) )
  {
    mCarbonContext->getFileCollector()->printOrderedFileList();
  }

  return true;
}

void JaguarContext::adjustSeverities() {
  // Suppress Jaguar messages:
  static SInt32 sup[] = {
    1002, // synthesis warning:  Default initial value of variable/signal will be ignored
    1003, // synthesis warning: "%s declaration will be ignored" (occurs with aliases)
    1005, // synthesis warning: "Non-integer type STD_LOGIC_VECTOR used in declaration of generic"
    1006, // synthesis error due to RHS of dividing operator not being a static power of 2
    1010, // At least one WAIT statement is needed within a LOOP/WHILE-LOOP statement
    1015, // synthesis error: "For loop range bounds must be locally static"
    1018, // synthesis error due to guarded block
    1019, // synthesis error: Use of predefined attribute is not supported"
    1026, // synthesis error: "Port %s of unconstrained type is not synthesizable"
    1028, // synthesis error: Array defined using enumeration type as index is not supported
    1040, // synthesis error: "Assertion statement will be ignored"
    1041, // synthesis error: "Use of ACCESS type is not synthesisable"
    1042, // synthesis error: "Use of FILE type is not synthesisable"
    1047, // Sequential statements preceding or succeeding the clocked if statement are not synthesizable  
    1048, // synthesis error: "Use of pre-defined attribute RANGE with suffix value is not supported"
    -1};

  static SInt32 warn[] = {
    1046, // Function call "NOW" is not supported for synthesis
    -1};

  static SInt32 error[] = {
    11,   // Use of undeclared identifier
    48,   // Either Type mismatch or no visible function for this case
    55,   // The Type of the rhs expression does not match the type of the target
    103,  // Port and port map do not match
    269,  // Type mismatch in actual association for formal %s
    415,  // Element associations of an array aggregate must be all either positional or named
    435,  // Bad Jaguar node passed to a Jaguar call
    1043, // Use of TIME type is not synthesizable
    -1};

  static SInt32 alert[] = {
    111,  // no wait statement or sensitivity list
    170,  // size mismatch across assignment
    531,  // size mismatch across assignment (yes, largely the same message as 170)
    1014, // Reading signal %s which is not part of the process sensitivity list may
          // lead to a potential simulation mismatch
    1025, // potential clock not in sensitivity list
    -1};

  typedef UtHashMap<SInt32,MsgContextBase::Severity> SeverityMap;
  SeverityMap overrides;

  for (SInt32* p = sup;  *p != -1; ++p) {overrides[*p] = MsgContextBase::eSuppress;}
  for (SInt32* p = warn; *p != -1; ++p) {overrides[*p] = MsgContextBase::eWarning;}
  for (SInt32* p = error; *p != -1; ++p) {overrides[*p] = MsgContextBase::eError;}
  for (SInt32* p = alert;  *p != -1; ++p) {overrides[*p] = MsgContextBase::eAlert;}

  MsgContextBase::SeverityArray severities;
  MsgContextBase::IntArray msgNumbers;
  MsgContext* msgContext = sCarbonContext->getMsgContext();
  msgContext->getUnknownSeverityRange( JAG_MSG_OFFSET, JAG_MSG_OFFSET+9999,
                                       &severities, &msgNumbers );

  for (UInt32 i = 0; i < severities.size(); ++i) {
    MsgContextBase::Severity sev = severities[i];
    SInt32 msgNumber = msgNumbers[i];
    overrides[msgNumber - JAG_MSG_OFFSET] = sev;
  }

  for (SeverityMap::iterator p = overrides.begin(); p != overrides.end(); ++p) {
    SInt32 msgNumber = p->first;
    MsgContextBase::Severity sev = p->second;
    if (( sev != MsgContextBase::eError ) && jaguarMsgMustBeErrors( msgNumber ))
    {
      // warn user
      const char* sevname = NULL;
      
      switch ( sev )
      {
      case MsgContextBase::eSuppress: {sevname = "silent"; break;}
      case MsgContextBase::eNote:     {sevname = "info"; break;}
      case MsgContextBase::eWarning:  {sevname = "warning"; break;}
      case MsgContextBase::eError:    {sevname = "error"; break;}
      default:   {sevname = "<unknown>"; break;}
      }

      const SourceLocator loc;
      msgContext->IgnoringMsgSeverityAdjust(&loc, sevname, msgNumber+JAG_MSG_OFFSET, "Error" );
      // force our internal severity table back to the severity that Jaguar originally had.
      sev = MsgContextBase::eError;
    }
    
    // Note that all messages that Jaguar prints get their severities
    // adjusted by jaguar_spew, which calls getMsgSeverity.  So be
    // sure to note the exact severity there.  The only consequence to
    // telling Jaguar directly about severities is that it will not
    // successfully populate after it has an error.  So if the user
    // wants to demote a message below error, we do need to tell
    // Jaguar about it.  But the best thing to tell Jaguar is that
    // it's a warning, so that we still get the jaguar_spew callback.
    // That way we can put suppressed messages into libdesign.suppress,
    // which may be handy.
    msgContext->putMsgNumSeverity(msgNumber + JAG_MSG_OFFSET, sev);

    switch (sev) {
    case MsgContext::eSuppress:
         vhModifyMsgSeverity(msgNumber, VH_UNDISPLAY); // fix for bug4808
      break;
    case MsgContext::eNote:
    case MsgContext::eWarning:
    case MsgContext::eStatus:
    case MsgContext::eContinue:
      vhModifyMsgSeverity(msgNumber, VH_WARN);
      break;
    case MsgContext::eError:
    case MsgContext::eAlert:
    case MsgContext::eFatal:
      vhModifyMsgSeverity(msgNumber, VH_ERROR);
      break;
    }
  } // for

} // void JaguarContext::adjustSeverities

VHDLCaseT JaguarContext::determineCaseMode()
{
  ArgProc* args = mCarbonContext->getArgs();
  const char* caseStr = args->getStrValue(scVhdlCase);
  INFO_ASSERT(caseStr, "-vhdlCase option not found");
  if (strcasecmp(caseStr, "preserve") == 0) {
    mCase = eVHDLPreserve;
  }
  else if (strcasecmp(caseStr, "upper") == 0) {
    mCase = eVHDLUpper;
  }
  else if (strcasecmp(caseStr, "lower") == 0) {
    mCase = eVHDLLower;
  }
  else {
    UtString buf("Invalid -vhdlCase option `");
    buf << caseStr << "' -- must pick 'preserve', 'lower', or 'upper'";
    mCarbonContext->getMsgContext()->CmdLineError(buf.c_str());
    mCase = eVHDLLower;
  }

  return mCase;
}

bool
JaguarContext::getTopDesignUnit(mvvNode* topMvvPrimary, 
                                mvvNode* topMvvArchitecture,
                                mvvNode* topBlkCfg)
{
  bool ret = true;

  ArgProc* args = mCarbonContext->getArgs();
  const char* topModule = args->getStrValue(scVhdlTop);
  
  *topMvvPrimary = NULL;
  *topMvvArchitecture = NULL;
  *topBlkCfg = NULL;
  
  // Only move on to Nucleus population if we have something to populate
  if ( (mCarbonContext->getFileCollector()->begin() != 
        mCarbonContext->getFileCollector()->end() ))
  {
    const char* workLib = getLogicalWorkLib();
    const char* workLibName = "WORK";
    if(NULL == workLib) workLib = workLibName;
    // Find out the top level primary design unit and the optional secondary
    // design unit. User can specify the top unit(s) as: 'topEntity:arch'
    // where 'topEntity' is the top level entity name and the 'arch' is it's
    // architecture.
    StrToken strToken(topModule,":"); 
    const char* primName = NULL;
    const char* secondaryUnitName = NULL;
    strToken(&primName);
    // Keep a copy of the primaryUnitName because next call to strToken()
    // will overwrite it.
    UtString primaryUnitNameStr(primName);
    const char* primaryUnitName = primaryUnitNameStr.c_str();
    strToken(&secondaryUnitName);
    // Open the primary design unit
    vhNode topPrimary = vhOpenDesignUnit(workLib,
                                         const_cast<char*>(primaryUnitName),
                                         NULL);
    if ( NULL == topPrimary )
    {
      // The specified primary design unit not found
      mCarbonContext->getMsgContext()->VhdlPrimaryUnitNotFound(primaryUnitName,
                                                               workLib);
      return false;
    }
    // We need the configured block if the -vhdlTop is configuration
    // declaration. 
    vhNode blk_cfg = NULL;
    vhNode topArchitecture = NULL;
    // If the top primary design unit is entity, then open it's archtecture.
    // If it configuration decl then open the entity and to level arch decl 
    // corresponding to this config. decl. 
    vhObjType topDU_obj_type = vhGetObjType(topPrimary);

    switch(topDU_obj_type)
    {
      case VHENTITY:
      {
        if (secondaryUnitName) // User has specified the top level arch name
        {
          topArchitecture = vhOpenArch(workLib,
                                       const_cast<char*>(primaryUnitName),
                                       const_cast<char*>(secondaryUnitName));
        }
        else
        {
          topArchitecture = vhOpenMRArch(workLib,
                                         const_cast<char*>(primaryUnitName));
        }
        if (NULL == topArchitecture)
        {
          if (NULL == secondaryUnitName) 
          {
            mCarbonContext->getMsgContext()->
                                   VhdlTopEntityHasNoArch(primaryUnitName,
                                                          workLib);
          }
          else
          {
            mCarbonContext->getMsgContext()->
                                   VhdlArchitectureNotFound(secondaryUnitName,
                                                            primaryUnitName,
                                                            workLib);
          }
          return false;
        }
        break;
      }
      case VHCONFIGDECL:
      {
        // Get the top level block which has been configured with this
        // configuration.
        blk_cfg = vhGetBlockConfig(topPrimary);
        if (blk_cfg)
        {
          VhdlPortTypeMap* vhdlPortTypeMap = 
            mCarbonContext->getVhdlPortTypeMap();
          JaguarString configName(vhGetConfigName(topPrimary));
          JaguarString configLib(vhGetLibName(topPrimary));
          vhdlPortTypeMap->putTopLevelConfig(configLib, configName);
        }
        
        JaguarString entityName(vhGetEntityName(topPrimary));
        FUNC_ASSERT(entityName, gJaguarAssert("failed to get entity name", "VHCONFIGDECL", topPrimary));
        // Open the entity
        vhNode entityDecl = vhOpenDesignUnit(workLib,
                                             entityName,
                                             NULL);
        if ( NULL == entityDecl )
        {
          // The specified primary design unit not found
          mCarbonContext->getMsgContext()->VhdlPrimaryUnitNotFound(entityName,
                                                                   workLib);
          return false;
        }
        secondaryUnitName = vhGetArchName(topPrimary);
        FUNC_ASSERT(secondaryUnitName, gJaguarAssert("failed to get architecture name", "VHCONFIGDECL", topPrimary));
        // Open the architecture
        topArchitecture = vhOpenArch(workLib,
                                     entityName,
                                     const_cast<char*>(secondaryUnitName));
        topPrimary = entityDecl;
        if (NULL == topArchitecture)
        {
          mCarbonContext->getMsgContext()->
                                    VhdlArchitectureNotFound(secondaryUnitName,
                                                             entityName,
                                                             workLib);
          return false;
        }
        break;
      }
      case VHPACKAGEDECL:
      case VHPACKAGEBODY:
      {
          // It's an error to have a package as top design unit.
          // There is nothing to simulate for the design.
          JaguarString objTypeStr(vhGetObjTypeName(topDU_obj_type));
          mCarbonContext->getMsgContext()->VhdlInvalidTopDUType(primaryUnitName, objTypeStr);
          return false;
      }
      default:
        FUNC_ASSERT(0, gJaguarAssert("unsupported object type", "VHDL", topPrimary));
    }

    *topMvvPrimary = topPrimary;
    *topMvvArchitecture = topArchitecture;
    *topBlkCfg = blk_cfg;
  }
  return ret;
}

bool
JaguarContext::setTopLevelGenerics(vhNode topMvvPrimary)
{
  ArgProc::StrIter iter;
  bool known = mCarbonContext->getArgs()->getStrIter(CarbonContext::scTopLevelParam, &iter) == ArgProc::eKnown;

  UtString msg(CarbonContext::scTopLevelParam);
  msg += " switch not known";
  INFO_ASSERT(known, msg.c_str());

  // Process the -topLevelParam options
  UtMap<UtString, UtString> genericValPairs;
  for (char* genericVal; iter(&genericVal);) {
    UtString genericValStr(genericVal);
    size_t equal = genericValStr.find('=');
    if (equal == UtString::npos) {
      mCarbonContext->getMsgContext()->TopLevelParamMissingAssignment(genericValStr.c_str());
      return false;
    }
    UtString generic = genericValStr.substr(0, equal);
    generic.lowercase();
    UtString val = genericValStr.substr(equal+1);
    genericValPairs[generic] = val;
  }

  vhNode genericClause = vhGetGenericClause(topMvvPrimary);

  if (genericClause == NULL) {
    // This entity has no generics, but the user expected them.
    JaguarString name(vhGetEntityName(topMvvPrimary));
    mCarbonContext->getMsgContext()->TopLevelModuleHasNoParameters(name);
    return false;
  }

  JaguarList genericList(vhGetFlatGenericList(genericClause));

  vhNode generic;
  UtMap<UtString,UtString>::iterator genericIter;

  UtSet<UtString> actualGenerics;

  while ((generic = vhGetNextItem(genericList))) {

    JaguarString name(vhGetName(generic));
    UtString nameStr(name);
    nameStr.lowercase();
    genericIter = genericValPairs.find(nameStr);

    actualGenerics.insert(nameStr);

    if (genericIter == genericValPairs.end()) {
      // This particular generic was not overridden in the command line
      continue;
    }

    // Extract the value from the command line option
    vhNode pGenericVal = NULL;

    // Only create an expression if the string is not empty
    if (!genericIter->second.empty()) {

      UtString paramValStr = genericIter->second;

      // Create a valid generic value
      if (paramValStr.find('\"') != UtString::npos) {
	vhwSetCreateScope(topMvvPrimary, vhGetSourceFileName(generic));
	// Strip the "" 
	paramValStr = paramValStr.substr(1, paramValStr.find_last_of('\"')-1);
	vhNode initialValue = vhGetInitialValue(generic);
	pGenericVal = vhwCreateString(paramValStr.c_str(), vhGetExpressionType(static_cast<vhExpr>(initialValue)));

	JaguarString initialValueStr(vhGetString(static_cast<vhExpr>(initialValue)));
	if (strlen(initialValueStr) != paramValStr.length()) {
	  mCarbonContext->getMsgContext()->TopLevelParamStringValueHasSizeMismatch(name, 
										   initialValueStr,
										   paramValStr.c_str());

	}
      }
      else {
	pGenericVal = vhCreateExprFromString(paramValStr.c_str(), topMvvPrimary);
      }

      if (pGenericVal != NULL) {
	// Verify that the new value matches the type of the generic
	if (vhGetInitialValue(generic) != NULL) {
	  JaguarString genericBaseType(vhGetTypeName(vhGetExpressionType(vhGetInitialValue(generic))));
	  JaguarString newValueBaseType(vhGetTypeName(vhGetExpressionType(static_cast<vhExpr>(pGenericVal))));
	  
	  if (strcasecmp(genericBaseType, newValueBaseType) != 0) {
	    mCarbonContext->getMsgContext()->TopLevelParamValueHasTypeMismatch(name, 
									       genericBaseType,
									       genericIter->second.c_str(), 
									       newValueBaseType);
	  }
	}
	vhSetLineNumber(pGenericVal, vhGetLineNumber(generic));
	vhSetEndLineNumber(pGenericVal, vhGetEndLineNumber(generic));
      }
    }

    // Set the new initial value of this generic
    vhSetInitialValue(generic, static_cast<vhExpr>(pGenericVal));
  }

  // Verify that all the generics specified in the command line actually exist in
  // in the top level design unit.
  for (genericIter = genericValPairs.begin(); genericIter != genericValPairs.end(); ++genericIter) {
    if (actualGenerics.find(genericIter->first) == actualGenerics.end()) {
      JaguarString name(vhGetEntityName(topMvvPrimary));
      mCarbonContext->getMsgContext()->TopLevelParamDoesNotExist(genericIter->first.c_str(), name);
      return false;
    }
  }

  return true;
}

bool
JaguarContext::prepPopulateVhdl(mvvNode* topMvvPrimary, mvvNode* topMvvArchitecture, 
                                mvvNode* topBlkCfg)
{

  // Extracts the top level design unit
  getTopDesignUnit(topMvvPrimary, topMvvArchitecture, topBlkCfg);

  // Process inline directives
  processMetaDirectives( );

  return true;
}


bool
JaguarContext::postPopulate(mvvNode topMvvPrimary, mvvNode topMvvArchitecture,
                            Populate::ErrorCode err_code, NUDesign* design)
{
  mDesign = design;
  vhNode topPrimary = NULL;
  vhNode topArchitecture = NULL;
  if (topMvvPrimary)
    topPrimary = topMvvPrimary->castVhNode();
  if (topMvvArchitecture)
    topArchitecture = topMvvArchitecture->castVhNode();
  
  if ( err_code == Populate::eSuccess )
  {
    // gather the top-level port info for VHDL testbench generation
    vhNode vh_port_clause = vhGetPortClause( topPrimary );
    if ( vh_port_clause != NULL )
    {
      JaguarList vh_port_iter(vhGetFlatSignalList( vh_port_clause ));
      // libName will be used to indicate the lib we are getting any
      // definition of SIGNED/UNSIGNED from.  It may be numeric_bit,
      // numeric_std, or std_logic_arith.
      UtString packageName;
      VhdlPortTypeMap* vhdlPortTypeMap = mCarbonContext->getVhdlPortTypeMap();
      
      while ( vhNode vh_port = vhGetNextItem( vh_port_iter ))
      {
        JaguarString jname(vhGetName( vh_port ));
        UtString name(jname);
        switch (mCase)
        {
        case eVHDLLower:
          name.lowercase();
          break;
        case eVHDLUpper:
          name.uppercase();
          break;
        case eVHDLPreserve:
          break;
        }
        
        vhNode vh_subtype = vhGetSubTypeInd( vh_port );
        UtString typestr;
        VhdlPopulate::decompileJaguarNode( vh_subtype, &typestr );
        StringUtil::strip(&typestr, " ");
        typestr.lowercase();
        
        VhdlPortType* portType = vhdlPortTypeMap->genPortType(name, typestr);
	vhNode vh_type = vhGetExpressionType( static_cast<vhExpr>( vh_subtype ));

	while (vh_type != NULL)
	{
	  vhNode vh_scope = vhGetScope( vh_type );
	  if ( vhGetObjType( vh_scope ) == VHPACKAGEDECL )
	  {
	    UtString libName = vhGetLibName( vh_scope );
	    libName.lowercase();
	    packageName = vhGetPackName( vh_scope );
	    packageName.lowercase();
	    if (0 == libName.compare("ieee"))
	    {
	      vhdlPortTypeMap->addIEEEPackage(&packageName);
	    }
	    else if (0 != libName.compare("std"))
	    {
	      // Include packages that are not in the std or ieee libraries
	      vhdlPortTypeMap->putPackageName(&packageName);
	    }
	  }
	  vhObjType objType = vhGetObjType(vh_type);
	  switch (objType)
	  {
	  case VHSUBTYPEDECL:
	    vh_type = vhGetSubTypeBaseType(vh_type);
	    break;

	  case VHTYPEDECL:
	    vh_type = vhGetTypeDef(vh_type);
	    break;

	  case VHCONSARRAY:
	  case VHUNCONSARRAY:
	    vh_type = vhGetType(vhGetEleSubTypeInd(vh_type));
	    break;

	  default:
	    vh_type = NULL;
	    break;
	  }
	}
        
        switch (vhGetPortType(vh_port))
        {
        case VH_IN:
        case VH_DEFAULT_IN:
          portType->putDirection(VhdlPortType::eInput);
          break;
        case VH_OUT:
          portType->putDirection(VhdlPortType::eOutput);
          break;
        case VH_INOUT:
          portType->putDirection(VhdlPortType::eInout);
          break;
        case VH_BUFFER:
          portType->putDirection(VhdlPortType::eBuffer);
          break;
        case VH_LINKAGE:
          gJaguarAssert("Linkage port type not supported", "VHDL", vh_port);
          break;
        case VH_NOT_PORT:
        case VH_ERROR_PORTTYPE:
          gJaguarAssert("Port has no port type", "VHDL", vh_port);
          break;
        }        
      }
    }
  }
  // Close the top level entity
  vhCloseDesignUnit(topPrimary);
  // Close the architecture body
  vhCloseDesignUnit(topArchitecture);

  if ( err_code == Populate::eSuccess )
    return true;
  
  return false;
}

const char* gGetVhNodeDescription(vhNode jag_node)
{
  if (NULL == jag_node)
  {
    return "NULL";
  }

  switch(vhGetObjType(jag_node))
  {
  case VHDESIGNUNIT:
    return "VHDL design unit";
    break;
  case VHLIBRARYCLAUSE:
    return "LIBRARY clause";
    break;
  case VHUSECLAUSE:
    return "USE clause";
    break;
  case VHENTITY:
    return "ENTITY";
    break;
  case VHCONFIGDECL:
    return "CONFIGURATION declaration";
    break;
  case VHPACKAGEDECL:
    return "PACKAGE declaration";
    break;
  case VHARCH:
    return "ARCHITECTURE";
    break;
  case VHPACKAGEBODY:
    return "PACKAGE BODY";
    break;
  case VHBLOCKCONFIG:
    return "BLOCK CONFIGURATION";
    break;
  case VHCOMPCONFIG:
    return "COMPONENT CONFIGURATION";
    break;
  case VHCOMPSPEC:
    return "COMPONENT specification";
    break;
  case VHCONFIGSPEC:
    return "CONFIGURATION specification";
    break;
  case VHSEQSTMNT:
    return "sequential statement";
    break;
  case VHWAIT:
    return "WAIT statement";
    break;
  case VHVARASGN:
    return "VARIABLE assignment";
    break;
  case VHSIGASGN:
    return "SIGNAL assignment";
    break;
  case VHWAVEFORM:
    return "waveform";
    break;
  case VHWAVEFORMELEMENT:
    return "waveform element";
    break;
  case VHPROCEDURECALL:
    return "PROCEDURE call";
    break;
  case VHASSOCIATION:
    return "association";
    break;
  case VHIF:
    return "IF statement";
    break;
  case VHCASE:
    return "CASE statement";
    break;
  case VHCASEALTER:
    return "CASE alternative";
    break;
  case VHLOOP:
    return "LOOP statement";
    break;
  case VHWHILE:
    return "WHILE loop";
    break;
  case VHFOR:
    return "FOR loop";
    break;
  case VHASSERT:
    return "ASSERT statement";
    break;
  case VHREPORT:
    return "REPORT statement";
    break;
  case VHNEXT:
    return "NEXT statement";
    break;
  case VHEXIT:
    return "EXIT statement";
    break;
  case VHRETURN:
    return "RETURN statement";
    break;
  case VHNULL:
    return "NULL statement";
    break;
  case VHPROCESS:
    return "PROCESS statement";
    break;
  case VHGENERIC:
    return "GENERIC";
    break;
  case VHGENMAPASPECT:
    return "GENERIC MAP aspect";
    break;
  case VHPORT:
    return "PORT";
    break;
  case VHPORTMAPASPECT:
    return "PORT MAP aspect";
    break;
  case VHCOMPONENT:
    return "COMPONENT";
    break;
  case VHSIGNAL:
    return "SIGNAL";
    break;
  case VHCONSTANT:
    return "CONSTANT";
    break;
  case VHVARIABLE:
    return "VARIABLE";
    break;
  case VHBLOCK:
    return "BLOCK statement";
    break;
  case VHCONCPROCCALL:
    return "concurrent PROCEDURE call";
    break;
  case VHINSTANCE:
    return "instance";
    break;
  case VHCOMPINSTANCE:
    return "COMPONENT instance";
    break;
  case VHENTITYINSTANCE:
    return "ENTITY instance";
    break;
  case VHCONCSIGASGN:
    return "concurrent SIGNAL assignment";
    break;
  case VHCONDSIGASGN:
    return "conditional SIGNAL assignment";
    break;
  case VHCONDWAVEFORM:
    return "conditional waveform";
    break;
  case VHSELSIGASGN:
    return "selected SIGNAL assignment";
    break;
  case VHSELWAVEFORM:
    return "selected waveform";
    break;
  case VHGENERATE:
    return "GENERATE statement";
    break;
  case VHIFGENERATE:
    return "IF GENERATE statement";
    break;
  case VHFORGENERATE:
    return "FOR GENERATE statement";
    break;
  case VHEXPR:
    return "expression";
    break;
  case VHOPERATOR:
    return "operator";
    break;
  case VHUNARY:
    return "unary expression";
    break;
  case VHBINARY:
    return "binary expression";
    break;
  case VHFUNCCALL:
    return "FUNCTION call";
    break;
  case VHQEXPR:
    return "qualified expression";
    break;
  case VHTYPECONV:
    return "type conversion";
    break;
  case VHINDNAME:
    return "indexed name";
    break;
  case VHSLICENAME:
    return "slice name";
    break;
  case VHATTRBNAME:
    return "attribute name";
    break;
  case VHPHYSICALLIT:
    return "physical literal";
    break;
  case VHDECLIT:
    return "decimal literal";
    break;
  case VHBASELIT:
    return "based literal";
    break;
  case VHENUMELEMENT:
    return "ENUM element";
    break;
  case VHIDENUMLIT:
    return "ENUM identifier literal";
    break;
  case VHSTRING:
    return "STRING";
    break;
  case VHBITSTRING:
    return "bit string literal";
    break;
  case VHCHARLIT:
    return "character literal";
    break;
  case VHNULLLIT:
    return "NULL literal";
    break;
  case VHSIMPLENAME:
    return "simple name";
    break;
  case VHAGGREGATE:
    return "aggregate";
    break;
  case VHALLOCATOR:
    return "allocator";
    break;
  case VHSUBPROGBODY:
    return "SUBPROGRAM body";
    break;
  case VHSUBPROGDECL:
    return "subprogram declaration";
    break;
  case VHTYPEDECL:
    return "TYPE declaration";
    break;
  case VHTYPEDEF:
    return "TYPE definition";
    break;
  case VHSUBTYPEDECL:
    return "SUBTYPE declaration";
    break;
  case VHRANGE:
    return "RANGE";
    break;
  case VHDISRANGE:
    return "discrete RANGE";
    break;
  case VHSUBTYPEIND:
    return "SUBTYPE indication";
    break;
  case VHCONSTRAINT:
    return "constraint";
    break;
  case VHINDEXCONSTRAINT:
    return "index constraint";
    break;
  case VHRANGECONSTRAINT:
    return "RANGE constraint";
    break;
  case VHENUMTYPE:
    return "ENUM TYPE";
    break;
  case VHINTTYPE:
    return "INTEGER TYPE";
    break;
  case VHUNCONSARRAY:
    return "unconstrained ARRAY";
    break;
  case VHCONSARRAY:
    return "constrained ARRAY";
    break;
  case VHINDSUBTYPEDEF:
    return "index SUBTYPE definition";
    break;
  case VHOTHERS:
    return "OTHERS";
    break;
  case VHALL:
    return "ALL";
    break;
  case VHFORINDEX:
    return "FOR index";
    break;
  case VHOBJDECLARATION:
    return "object declaration";
    break;
  case VHLABEL:
    return "label";
    break;
  case VHOBJECT:
    return "object";
    break;
  case VHDEFAULTCONSTANT:
    return "default constant";
    break;
  case VHDEFAULTVARIABLE:
    return "default variable";
    break;
  case VHDEFAULTSIGNAL:
    return "default signal";
    break;
  case VHATTRBDECL:
    return "ATTRIBUTE declaration";
    break;
  case VHATTRIBUTESPEC:
    return "ATTRIBUTE specification";
    break;
  case VHOPEN:
    return "OPEN";
    break;
  case VHLIST:
    return "VHDL-parser's list object";
    break;
  case VHDECL:
    return "declaration";
    break;
  case VHCONFINSTANCE:
    return "CONFIGURATION instance";
    break;
  case VHCONCSIGASGNSTMNT:
    return "concurrent SIGNAL assignment";
    break;
  case VHGENERATESTMNT:
    return "GENERATE statement";
    break;
  case VHCONCASSERT:
    return "concurrent ASSERT statement";
    break;
  case VHDISCONNECTSPEC:
    return "disconnection specification";
    break;
  case VHBINDIND:
    return "binding indication";
    break;
  case VHFILEDECL:
    return "FILE declaration";
    break;
  case VHALIASDECL:
    return "ALIAS declaration";
    break;
  case VHELEMENTASS:
    return "element association";
    break;
  case VHPHYSICALTYPE:
    return "physical type";
    break;
  case VHACCESSTYPE:
    return "ACCESS type";
    break;
  case VHRECORD:
    return "RECORD type";
    break;
  case VHELSIF:
    return "ELSIF statement";
    break;
  case VHSELECTEDNAME:
  case VHSELNAME:
    return "selected name";
    break;
  case VHOPENENTITYASPECT:
    return "open entity aspect";
    break;
  case VHATTRIBASSOC:
    return "ATTRIBUTE association";
    break;
  case VHFLOATTYPE:
    return "REAL type";
    break;
  case VHGROUPTEMPLATE:
    return "GROUP TEMPLATE";
    break;
  case VHGROUPDECL:
    return "GROUP declaration";
    break;
  case VHFILETYPE:
    return "FILE type";
    break;
  case VHFILENAME:
    return "FILE name";
    break;
  case VHSLICENODE:
    return "slice node";
    break;
  case VHFUNCNODE:
    return "FUNCTION node";
    break;
  case VHABSEXPR:
    return "ABS expression";
    break;
  case VHARRAY:
    return "ARRAY";
    break;
  case VHUSERATTRIBUTE:
  case VHUSERATTRIBNODE:
    return "user defined ATTRIBUTE";
  case VHELEMENTDECL:
    return "RECORD element declaration";
    break;
  default:
    return vhGetTypeName(jag_node);
    break;
  }
}

void JaguarContext::putInPrePopWalk(bool prePopWalk)
{
  if (prePopWalk)
    vhRegisterVarArgsMessageHandler(jaguar_prepop_spew); 
  else
    vhRegisterVarArgsMessageHandler(jaguar_spew); 
}


void JaguarContext::processMetaDirectives( )
{
  JaguarList listLib = NULL;
  JaguarList listEnt = NULL;
  JaguarList packEnt = NULL;
  vhNode entity = NULL, package = NULL;
  JaguarString  libName;
  JaguarString  entityName, packageName;
  //  JaguarString  primUnitName;

  // Get all libraries in the design
  listLib = vhGetAllLibraries();
       
  while ( (libName = ( (const char *) vhGetNextItem( listLib )) ))
  {
    listEnt = vhGetAllEntities( libName );
    while ( (entityName = ( (const char *) vhGetNextItem( listEnt )) ))
    { 
      entity = vhOpenEntity( libName, entityName );
                
      //Directives on Enitity
      if(entity != NULL)
      {
        JaguarList dirList = vhGetAllDirectives(entity);
        vhNode entDir;
	while((entDir = vhGetNextItem(dirList)) != NULL)
        {
          vhNode assocObj = vhGetAssociatedObject(entDir);
          JaguarString dirStr ( (const char*) vhGetAssociatedDirectiveString(entDir));

	  if( vhGetObjType( assocObj ) == VHSIGNAL )
	  {
	    JaguarString nodeName ( (const char*)  vhGetName( assocObj ));
	    declarePragmaDirectives(dirStr, entityName, nodeName, assocObj);
	  }
	  else if( vhGetObjType( assocObj ) == VHENTITY )
	    declarePragmaDirectives(dirStr, entityName, NULL, assocObj);
        }
      
	        
	JaguarList dependencyList = vhGetAllArchitectures(libName, entityName);
        JaguarString archName;
        while ( (archName = ( (const char *) vhGetNextItem( dependencyList )) ))
        {
          vhNode arch = vhOpenArch(libName, entityName, archName);
          //Directives on the architecture
          if(arch != NULL)
	  {
            JaguarList dirList( vhGetAllDirectives(arch));
            vhNode dir;
            while( (dir=vhGetNextItem(dirList)) != NULL)
            {
              vhNode assocObj = vhGetAssociatedObject(dir);
              if(assocObj != NULL)
	      {
                // The object could be declared in the architecture or in a process (possibly within a generate)
                // If declared in the process, the process must be labeled.
                UtString scopeName(entityName);
                vhNode scope = vhGetScope(assocObj);
                bool hasProcess = (vhGetObjType(scope) == VHPROCESS);
                UtString procName;
                if ( hasProcess ) {
                  JaguarString procLabel(vhGetLabel(scope));
                  if (procLabel == NULL) {
                    SourceLocatorFactory* locs = mCarbonContext->getSourceLocatorFactory();
                    SourceLocator loc = locs->create(vhGetSourceFileName(assocObj),
                                                     vhGetLineNumber(assocObj));
                    mCarbonContext->getMsgContext()->InlineDirProcessNoLabel(&loc);
                    continue;
                  }
                  procName << "." << procLabel;
                }
                // check for (possiblly nested) VHFORGENERATE, VHIFGENERATE, VHGENERATESTMNT
                vhNode possibleGenerate = scope;
                if ( hasProcess ) {
                  possibleGenerate = vhGetScope(scope); // if within a process, then skip it for generate search
                }
                while ( possibleGenerate && ( (vhGetObjType(possibleGenerate) == VHFORGENERATE) || (vhGetObjType(possibleGenerate) == VHIFGENERATE) || (vhGetObjType(possibleGenerate) == VHGENERATESTMNT) ) ){
                  scopeName << ".*"; // use wild card so that we don't need to enumerate all the names created by VHFORGENERATE, this makes no assumption about the names created for generate blocks
                  possibleGenerate = vhGetScope(possibleGenerate);
                }
                scopeName << procName; // procName may be blank if there was no process
                JaguarString dirStr(  (const char*) vhGetAssociatedDirectiveString(dir));
                JaguarString nodeName ( (const char*)  vhGetName( assocObj ));
                declarePragmaDirectives(dirStr, scopeName.c_str(), nodeName, assocObj);
              }
            }
	  }
        } // end of while for archName
      } // end of if ( entity != NULL)
    } // end of while for entityName
      
      
    packEnt = vhGetAllPackages( libName );
    while ( (packageName = ( (const char *) vhGetNextItem( packEnt )) ))
    { 
      package = vhOpenPackDecl( libName, packageName );
      if( package != NULL)
      {
        JaguarList dirList = vhGetAllDirectives(package);
        vhNode packDir;
	while((packDir = vhGetNextItem(dirList)) != NULL)
	{
          JaguarString dirStr ( (const char*) vhGetAssociatedDirectiveString(packDir));
	  {
	    mCarbonContext->getMsgContext()->InlineDirPack((const char *) dirStr,(const char *) packageName );
          }
	}
      } // end of if ( package != NULL)
    } // end of while for package
  } // end of while for libName
   
      
  return ;
}


void JaguarContext::declarePragmaDirectives(const char* dirName,
					    const char* entName,
					    const char* sigName,
					    vhNode node)
{
  IODBNucleus* iodb = mCarbonContext->getIODB();
  SourceLocatorFactory* locs = mCarbonContext->getSourceLocatorFactory();

  UtString entNameStr(entName);
  UtString sigNameStr(sigName);

  switch(mCase)
  {
  case eVHDLPreserve:
    break;
  case eVHDLUpper:
    entNameStr.uppercase();
    sigNameStr.uppercase();
    break;
  case eVHDLLower:
    entNameStr.lowercase();
    sigNameStr.lowercase();
    break;
  }

  // Jaguar reports the full text of the comment directive, e.g.
  // "  carbon   observeSignal".  We want to tokenize this out and
  // just get "observeSignal"
  StrToken tok(dirName);

  // at one time we only allowed the 'carbon' pragmas here, we would
  // filter them here but now we accept any pragma that uses
  // 'carbon' or any other prefix that the user has defined with
  // the command line switch: -pragma_prefix 
  {
    ++tok;
    if (!tok.atEnd()) 
    {
      JaguarString fname ((const char*) vhGetSourceFileName(node));

      
      UtString keyword = *tok;

      // The IP license protection is added here for VHDL, but
      // it's practically not usable, because the 'protect is
      // not implemented for VHDL.
      if (keyword.compare( mCarbonContext->scLicenseStr) == 0)
      {
        bool isVSP = mCarbonContext->isVSPCompile();

        if ( ! isVSP)
        {
          mCarbonContext->getMsgContext()->CmplrLicensedIPRequiresVSP(fname);
          // fatal
        }
 
        ++tok;
        const char* featureName = *tok;
        if (mCarbonContext->mIPFeatures->getIntern(featureName) == NULL)
        {
          mCarbonContext->mIPFeatures->intern(featureName);
          UtCustomerDB* custdb = iodb->getCustDB();
          UtCustomerDB::Signature* custSig = custdb->getCustomerSignature();
          INFO_ASSERT(custSig, "Developer signature not yet set.");
              
          INFO_ASSERT(strlen(featureName) > 0, "Invalid IP feature name");
          custSig->addIPFeature(featureName);
          UtLicense* license = mCarbonContext->getLicense();
          mCarbonContext->getMsgContext()->CustomerRequireRuntimeLicense( mCarbonContext->getFileRoot(), 
									 featureName);
              
          // make sure a form of the license exists.
          UtString licReason;
          if (! license->doesFeatureNameExist(featureName, &licReason) && ! mCarbonContext->hasDiagnosticsLicense())
            mCarbonContext->getMsgContext()->CustomerLicenseCheckFail(featureName, licReason.c_str());
        }
      }
      
      const char* rest = tok.curPos();

      // Formulate a directive as if it came from the directives file,
      // taking the pragma text and appending on the name of the
      // object.  Thus if the user wrote:
      //    module foo;
      //       reg x;        // carbon observeSignal
      //       reg clk2;     // carbon collapseClock top.clk1
      //       reg scan_ena; // carbon tieNet 1'b0
      //
      // then we want to form the strings:
      //       "observeSignal foo.x"
      //       "collapseClock top.clk1 foo.clk2"
      //       "tieNet 1'b0 foo.scan_ena"
        
      UtString directive(rest);
      directive << " " << entNameStr;
      if (sigName != NULL)
      {
        directive << "." << sigNameStr;
      }

      // The interface to IODB::parseDirective involves passing
      // the StrToken context, in addition to the "rest" of the
      // string, so we must now recreate the same state that
      // the directives file parser is in when it calls parseDirective
      // for a semi-parsed line.
      StrToken dirTok(directive.c_str());
      ++dirTok;

      SourceLocator loc = locs->create(fname, vhGetLineNumber(node));
      if (loc.isTicProtected() ){
        mCarbonContext->getMsgContext()->CarbonDirectiveInProtectedFile(fname, directive.c_str());
      }

      // built_in functions shouldn't be submitted to the IODB.
      // They are processed in VhdlPopulate::getBuiltInFuncOrMappedOperatorType
      if ( (keyword.compare("built_in") != 0) && ( keyword.compare("BUILT_IN") != 0) &&
	   (keyword.compare("map_to_operator") != 0) && ( keyword.compare("MAP_TO_OPERATOR") != 0))
      {
	iodb->parseDirective(keyword.c_str(), dirTok.curPos(), dirTok, loc);
      }
    }
  } // if
}

const char* JaguarContext::getLogicalWorkLib()
{
    const char* defaultLib = NULL;
    HdlFileCollector::const_iterator lib_iter;
    for ( lib_iter = mCarbonContext->getFileCollector()->begin();
	    lib_iter != mCarbonContext->getFileCollector()->end();
	    ++lib_iter )
    {
	if(LangVer::VHDL87 == (*lib_iter)->libraryLanguage() 
		||
		LangVer::VHDL93 == (*lib_iter)->libraryLanguage()
		||
		LangVer::LangVer_MAX == (*lib_iter)->libraryLanguage() )
	{
	    defaultLib = (*lib_iter)->getLogicalLibrary().c_str();
	}
    }

    return defaultLib;
}

// END FILE

