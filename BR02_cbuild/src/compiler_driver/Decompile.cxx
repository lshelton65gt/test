// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/* changes to this file were derived from Interra supplied examples */
/****************************************************************
 ***************
 *****  FILE  : routines.c
 *****  AUTHOR :Punit Sindhwani
 *****  DATE   :16/07/96
 *****  RCS  HISTORY :
 
 *****  BRIEF DESC : This file defines the generic decompiler 
       functions 
 
       *****************************************************************
       ***************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>

#include "Decompile.h"
#include "util/Zstream.h"
#include "util/UtHashMap.h"
#include "iodb/IODBNucleus.h"
#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/Interra.h"
#include "compiler_driver/ProtectedSource.h"
#include "compiler_driver/CheetahContext.h"

/***** 
       Function      : decomPrintf
       
       Purpose      : Printf Function ,Takes into account more than 1024 
       character word
       
       Parameters     :outPut String
       
       Returns      :void
       
       End Header
*****/

Decompile::Decompile(ZostreamDB* outfile,
                     AtomicCache* atomicCache,
                     veNode root,
                     IODBNucleus* iodb,
                     MsgContext* msgContext,
                     CheetahContext* cheetahContext)
{
  init(outfile, NULL, atomicCache, root, iodb, NULL, msgContext, cheetahContext);
}

Decompile::Decompile(UtOStream* clearFile,
                     AtomicCache* atomicCache,
                     veNode root,
                     IODBNucleus* iodb,
                     MsgContext* msgContext,
                     CheetahContext* cheetahContext)
{
  init(NULL, clearFile, atomicCache, root, iodb, NULL, msgContext, cheetahContext);
}

Decompile::Decompile(ZistreamDB* infile, 
                     AtomicCache* atomicCache,
                     IODBNucleus* iodb,
                     MsgContext* msgContext,
                     CheetahContext* cheetahContext)
{
  init(NULL, NULL, atomicCache, NULL, iodb, infile, msgContext, cheetahContext);
}

void Decompile::init(ZostreamDB* encryptedFile,
                     UtOStream* clearFile,
                     AtomicCache* atomicCache,
                     veNode root,
                     IODBNucleus* iodb,
                     ZistreamDB* infile,
                     MsgContext* msgContext,
                     CheetahContext* cheetahContext)
{
  // Only one (1) of the three possible files can be passed in.  The others
  // must be null.
  INFO_ASSERT((encryptedFile != NULL) + (clearFile != NULL) + (infile != NULL) == 1,
              "Decompile::init: only one of (encryptedFile, clearFile, infile) can be given");
  mEncryptedOutputFile = encryptedFile;
  mClearOutputFile = clearFile;
  mProtectedSource = new ProtectedSource;
  mInputFile = infile;
  mColumn = 0;
  mObfuscationIndex = 0;
  mAtomicCache = atomicCache;
  mNameMap = new NameMap;
  mProtected = new NameSet;
  mRoot = root;
  mIODB = iodb;
  protectName("");
  mObfuscationEnabled = true;
  mCypher = 42;                 // arbitrary
  mMsgContext = msgContext;
  mDecompiledNodes = new NodeSet;
  mInProtectedBlock = false;
  mCheetahContext = cheetahContext;
  mCEMFilesReferenced = new StringSet;
}

Decompile::~Decompile()
{
  delete mNameMap;
  delete mProtected;
  delete mDecompiledNodes;
  delete mProtectedSource;
  delete mCEMFilesReferenced;
}

void Decompile::disableObfuscation()
{
  mObfuscationEnabled = false;
}

void  Decompile::writeOutput(const char* str,int /*flag*/)
{
  if (mEncryptedOutputFile != NULL) {
    *this << eLiteral;
    *mEncryptedOutputFile << str;
  }
  else if (mInProtectedBlock) {
    *mProtectedSource << str;
  }
  else {
    flushProtected();
    *mClearOutputFile << str;
  }
}

char* Decompile::int_to_str(int i)
{
  char str[20];
  sprintf(str,"%d",i);
  return strdup(str);
}

char* Decompile::double_to_str(double i)
{
  char str[40];
  size_t k = 0;
  size_t j = 0;
  
  sprintf(str,"%f",i);
  size_t len = strlen(str);
  for (k=1; k<len; k++)
    str[k-1] = str[k];
  str[k] = '\0';
  len = strlen(str);
  for (k =0; k<len; k++)
    if (str[k] !='0')
      j=k;    
  str[j+1] = '\0';
  return strdup(str);
}


/********
         Function       : rootDecompile
         
         Purpose        : Decompilation routine for the root of
         the object model.
         
         Parameters     : veNode
         
         Returns        : void
         
         End Header
********/

void Decompile::rootDecompile( veNode nodeRoot )
{
  
  CheetahList fileList;
  veNode file;
  CheetahList libList;
  veNode lib;
  
  libList = veRootGetLibList (nodeRoot);
  
  if (libList) {
    lib = veListGetNextNode (libList);
    for ( ; lib; lib = veListGetNextNode (libList)) 
      libDecompile (lib);
  }
  
  fileList = veRootGetFileList( nodeRoot );
  
  if( fileList )
  {
    file = veListGetNextNode( fileList );
    
    for( ; file; file = veListGetNextNode( fileList ) )
      fileDecompile( file );
  }

  flushProtected();

  if (mEncryptedOutputFile != NULL) {
    *this << eEOF;
    writeObfuscationTable();
  }
}


/********
         Function       : libDecompile
         
         Purpose        : Decompilation routine for a verilog
         source file(library part->config).
         
         Parameters     : configLibNode 
         
         Returns        : void
         
         End Header
********/

void
Decompile::libDecompile (veNode configLibNode) {
  CheetahList  configList;
  veNode  config;
  
  configList = veLibraryGetConfigList (configLibNode);
  
  if (configList) {
    config = veListGetNextNode (configList);
    for ( ; config; config = veListGetNextNode (configList))
      configDecompile (config);
  }
}



/********
         Function       : fileDecompile
         
         Purpose        : Decompilation routine for a verilog
         source file.
         
         Parameters     : veNode
         
         Returns        : void
         
         End Header
********/

void Decompile::fileDecompile( veNode nodeFile )
{
  
  veBool     isResetAll;
  CheetahList moduleList, udpList;
  veNode     module, udp;
  veDriveStrengthType driveType;
  int           flag=0;
  
  
  isResetAll = veFileGetIsResetAll( nodeFile );
  
  if( isResetAll )
    writeOutput(CRYPT("`resetall\n"),0);
  
  moduleList = veFileGetModuleList( nodeFile );
  
  if( moduleList )
  {
    module = veListGetNextNode( moduleList );
    
    for( ; module; module = veListGetNextNode( moduleList ) )
    {
      driveType = veModuleGetUnconnectedDrive(module);
      if (driveType != UNSET_STRENGTH)
      {
        writeOutput(CRYPT("`unconnected_drive"),0);
        if (driveType == PULL_1)
          *this << ePull1;
        else if (driveType == PULL_0)
          *this << ePull0;
        writeOutput( "\n", 1);
        flag=1;
      }
      moduleDecompile( module );
      if (flag == 1)
        writeOutput(CRYPT("`nounconnected_drive\n"),0);
      flag = 0;
    }
  }
  
  udpList = veFileGetUdpList( nodeFile );
  
  if( udpList )
  {
    udp = veListGetNextNode( udpList );
    
    for( ; udp; udp = veListGetNextNode( udpList ) )
      udpDecompile( udp );
    
  }
}

void Decompile::putSynthPrefix(const char* prefix)
{
  mSynthPrefix = prefix;
}

Decompile& Decompile::operator<<(char c)
{
  if (mEncryptedOutputFile != NULL) {
    *this << ePunc;
    UInt8 val = ((UInt8) c) ^ mCypher;
    mCypher += val + 1;
    mEncryptedOutputFile->write((char*) &val, 1);
  }
  else if (mInProtectedBlock) {
    mProtectedSource->append(1, c);
  }
  else {
    flushProtected();
    mClearOutputFile->write((char*) &c, 1);
  }
  return *this;
}

Decompile& Decompile::operator<<(Keyword kwd)
{
  if (mEncryptedOutputFile != NULL) {
    UInt8 val = ((UInt8) kwd) ^ mCypher;
    mCypher += val + 1;
    mEncryptedOutputFile->write((char*) &val, 1);
  }
  else {
    UtString buf;
    const char* str = decodeKeyword(kwd, &buf);
    INFO_ASSERT(str, "decode failed");
    if (mInProtectedBlock) {
      *mProtectedSource << str;
    }
    else {
      flushProtected();
      *mClearOutputFile << str;
    }
  } // else
  return *this;
}

const char* Decompile::decodeKeyword(Keyword kwd, UtString* buf) {
  switch (kwd)
  {
  case eInitial:      return "initial ";   break;
  case eNewline:      return "\n";         break;
  case eOpenParen:    return "(";          break;
  case eCloseParen:   return ")";          break;
  case eSemiColon:    return ";";          break;
  case eAlways:       return "always ";    break;
  case eIf:           return "if ";        break;
  case eParameter:    return "parameter "; break;
  case ePosedge:      return "posedge ";   break;
  case eNegedge:      return "negedge ";   break;
  case eElse:         return "else ";      break;
  case eForever:      return "forever ";   break;
  case eRepeat:       return "repeat ";    break;
  case eBegin:        return "begin ";     break;
  case eFor:          return "for ";       break;
  case eColon:        return ":";          break;
  case eEnd:          return "end ";       break;
  case eEndCase:      return "endcase ";   break;
  case eWhile:        return "while ";     break;
  case eCase:         return "case ";      break;
  case eCasex:        return "casex ";     break;
  case eCasez:        return "casez ";     break;
  case eTrigger:      return "->";         break;
  case eDefault:      return "default ";   break;
  case eComma:        return ",";          break;
  case eWait:         return "wait ";      break;
  case eDisable:      return "disable ";   break;
  case eForce:        return "force ";     break;
  case eRelease:      return "release ";   break;
  case eDelay:        return "#";          break;
  case eFork:         return "fork ";      break;
  case eJoin:         return "join ";      break;
  case eTask:         return "task ";      break;
  case eEndTask:      return "endtask ";   break;
  case eFunction:     return "function ";  break;
  case eSigned:       return "signed ";    break;
  case eInteger:      return "integer ";   break;
  case eReal:         return "real ";      break;
  case eRealTime:     return "realtime ";  break;
  case eEndFunction:  return "endfunction ";break;
  case eEqual:        return "=";          break;
  case eNonBlockingEqual: return "<=";     break;
  case eAssign:       return "assign ";    break;
  case eAtSign:       return "@";          break;
  case eAsterisk:     return "*";          break;
  case eEventOr:      return " or ";       break;
  case eInput:        return "input ";     break;
  case eOutput:       return "output ";    break;
  case eInout:        return "inout ";     break;
  case eReg:          return "reg ";       break;
  case eTimescale:    return "`timescale ";break;
  case eCellDefine:   return "`celldefine ";break;
  case eModule:       return "module ";    break;
  case eMacroModule:  return "macromodule";break;
  case eEndModule:    return "endmodule "; break;
  case eEndCellDefine:return "`endcelldefine ";break;
  case eConfig:       return "config ";    break;
  case eDesign:       return "design ";    break;
  case eSlash:        return "/";          break;
  case eSpace:        return " ";         break;
  case eDeassign:     return "deassign ";  break;
  case eInstance:     return "instance ";  break;
  case eLibList:      return "liblist ";   break;
  case eUse:          return "use ";       break;
  case eCell:         return "cell ";      break;
  case eEndConfig:    return "endconfig "; break;
  case eGenerate:     return "generate ";  break;
  case eEndGenerate:  return "endgenerate ";break;
  case eGenVar:       return "genvar ";    break;
  case ePeriod:       return ".";          break;
  case eOpenBracket:  return "{";          break;
  case eCloseBracket: return "}";          break;
  case eOpenBrace:    return "[";          break;
  case eCloseBrace:   return "]";          break;
  case eWire:         return "wire ";      break;
  case eTime:         return "time ";      break;
  case eLocalParam:   return "localparam ";break;
  case eTri:          return "tri ";       break;
  case eTri1:         return "tri1 ";      break;
  case eSupply0:      return "supply0 ";   break;
  case eWand:         return "wand ";      break;
  case eTriand:       return "triand ";    break;
  case eTri0:         return "tri0 ";      break;
  case eSupply1:      return "supply1 ";   break;
  case eWor:          return "wor ";       break;
  case eTrior:        return "trior ";     break;
  case eTrireg:       return "trireg ";    break;
  case eVectored:     return "vectored ";  break;
  case eEvent:        return "event ";     break;
  case eStrong0:      return "strong0 ";   break;
  case ePull0:        return "pull0 ";     break;
  case eWeak0:        return "weak0 ";     break;
  case eHighz0:       return "highz0 ";    break;
  case ePull1:        return "pull1 ";     break;
  case eWeak1:        return "weak1 ";     break;
  case eHighZ1:       return "highz1 ";    break;
  case eDefParam:     return "defparam ";  break;
  case eSpecify:      return "specify ";   break;
  case eEndSpecify:   return "endspecify ";break;
  case eShowCancelled:   return "showcancelled ";   break;
  case eNoShowCancelled: return "noshowcancelled "; break;
  case eSpecParam:    return "specparam "; break;
  case eAnd:          return "and ";       break;
  case eNand:         return "nand ";      break;
  case eOr:           return "or ";        break;
  case eNor:          return "nor ";       break;
  case eXor:          return "xor ";       break;
  case eXnor:         return "xnor ";      break;
  case eBuf:          return "buf ";       break;
  case eBufif0:       return "bufif0 ";    break;
  case eBufif1:       return "bufif1 ";    break;
  case eNot:          return "not ";       break;
  case eNotif0:       return "notif0 ";    break;
  case ePulldown:     return "pulldown ";  break;
  case ePullup:       return "pullup ";    break;
  case eNmos:         return "nmos ";      break;
  case eRnmos:        return "rnmos ";     break;
  case ePmos:         return "pmos ";      break;
  case eRpnmos:       return "rpnmos ";    break;
  case eCmos:         return "cmos ";      break;
  case eRcmos:        return "rcmos ";     break;
  case eTran:         return "tran ";      break;
  case eRtran:        return "rtran ";     break;
  case eTranif0:      return "tranif0 ";   break;
  case eRtranif0:     return "rtranif0 ";  break;
  case eTranif1:      return "tranif1 ";   break;
  case eRtranif1:     return "rtranif1 ";  break;
  case eStrong1:      return "strong1 ";   break;
  case eHighz1:       return "highz1 ";    break;
  case eNotif1:       return "notif1 ";    break;
  case eRpmos:        return "rpmos ";     break;
  case ePunc:
  {
    INFO_ASSERT(mInputFile, "no input file");
    UInt8 c;
    if (*mInputFile >> c)
    {
      char ch = (char) (c ^ mCypher);
      buf->append(1, ch);
      mCypher += c + 1;
      return buf->c_str();
    }        
    break;
  }
  case eFullCase:
    *buf << "// " << mSynthPrefix << CRYPT(" full_case\n");
    return buf->c_str();
  case eParallelCase:
    *buf << "// " << mSynthPrefix << CRYPT(" parallel_case\n");
    return buf->c_str();
  case eFullParallelCase:
    *buf << "// " << mSynthPrefix << CRYPT(" full_case parallel_case\n");
    return buf->c_str();
  case eLiteral: {
    INFO_ASSERT(mInputFile, "no input file");
    UtString tmp;
    if (*mInputFile >> tmp) {
      buf->append(tmp);
      return buf->c_str();
    }
    else
      return NULL;
    break;
  }
  case eObfuscate: {
    INFO_ASSERT(mInputFile, "no input file");
    UInt32 index;
    if (*mInputFile >> index) {
      readObfuscate(buf, index);
      return buf->c_str();
    }
    break;
  }
  case eEOF:
    return NULL;
    break;
  case ePrimitive:     return "primitive ";    break;
  case eTable:         return "table ";        break;
  case eEndTable:      return "endtable ";     break;
  case eEndPrimitive:  return "endprimitive "; break;
  case eUWire:         return "uwire ";        break;
  } // switch
  INFO_ASSERT(0, "unknown keyword");
  return NULL;
} // static const char* Decompile::decodeKeyword

bool Decompile::readToken(UtString* buf)
{
  UInt8 val;
  bool ret = true;
  if (mInputFile->read((char*) &val, 1) > 0)
  {
    Keyword kwd = (Keyword) (val ^ mCypher);
    mCypher += val + 1;
    UtString buf2;
    if (kwd == eEOF) {
      ret = false;
    }
    else {
      *buf << decodeKeyword(kwd, &buf2);
    }
  } // if
  else
    ret = false;
  return ret;
} // bool Decompile::readToken

bool Decompile::requiresProtection(veNode node) {
  bool ret = false;

  // If are already protecting text we generate, we don't need further
  // protection.
  if ((mClearOutputFile != NULL) && !mInProtectedBlock && (node != NULL)) {
    CheetahStr filename(veNodeGetFileName(node));
    INFO_ASSERT(filename, "unable to determine filename");
    ret = mCheetahContext->requiresProtection(filename, veNodeGetLineNumber(node));
  }
  return ret;
}

void Decompile::beginProtection() {
  mInProtectedBlock = true;
}

void Decompile::endProtection() {
  mInProtectedBlock = false;
}

void Decompile::flushProtected() {
  if (!mProtectedSource->empty()) {
    if (mProtectedSource->protect() ) {
      (*mClearOutputFile) << "\n`protected\n"
                          << '@' << *mProtectedSource
                          << "\n`endprotected\n";
    }
    mProtectedSource->clear();
  }
}
