// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2012-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/VerificContext.h"

#include "compiler_driver/CarbonContext.h"
#include "util/HdlFileCollector.h"
#include "util/UtString.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/UtIStream.h"
#include "util/UtUniquify.h"
#include "util/Zstream.h"

#include <iostream>

// Options that require a filename paramater (omitted from tokens
// file re-compilation command, in lieu of the tokens file which has
// all the verilog source code, independent of source).
static const char* sVerilogFileOptions[] = {
  "-f", "-l", "-la", "-v", "-F", "-y", "-topModuleListDumpFile", "-incdir",
  NULL
};

// Verilog options that take arguments that are not files
static const char* sVerilogValOptions[] = {
  "-Dblack_box", "-synth_prefix", "-duplicate", "-hierlevel",
  NULL
};

static const char* sVerilogIllegalOptions[] = {
  "-incr", "-work", "-print_msg_list", "-version", "-q", "-d",
  "-nl", "-out", "-black_box", "+incrext+", "-pd",
  NULL
};

static const char* sVerilogOptions[] = {
  "-w",
  "-u",
  "-ovi2",
  "-synth",
  "-warnForSysTask",
  "-categorize",
  "-analyze",
  "-libduplicates",
  "-allow",
  "-override",
  "-keepfirst",
  "-noBehav",
  "-no_translate",
  "+mindelay",
  "+typdelay",
  "+maxdelay",
  "-sysTaskFuncCheck",
  "-checkFanIn",
  "-createScalarNet",
  "-incdir",
  NULL,
};

static CarbonContext* sCarbonContext = NULL;
//static const char* scVhdlSynthPrefix = "-vhdl_synth_prefix";
bool showVerificStatus = false;

#define UNDEFINEDSVNVERSION "Unversioned"
#ifndef SVNVERSION
  #define SVNVERSION UNDEFINEDSVNVERSION
#endif
#define UNDEFINEDSVNURL "undefined"
#ifndef SVNURL
  #define SVNURL UNDEFINEDSVNURL
#endif

VerificContext::VerificContext(CarbonContext* cc)
{
  sCarbonContext = cc;
}


VerificContext::~VerificContext() {
             Verific::RuntimeFlags::DeleteAllFlags() ;
}


// process all command line arguments that directly affect the verific parser
void VerificContext::processCommandLineSwitches(){

  {
    // setup the verific runtime switch that controls the handling of concats with a 0 repeat count: e.g. {(2-2){a},b}

    bool languageSaysEmptyConcatTermsAreIgnored = sCarbonContext->getArgs()->getBoolValue(CarbonContext::scSVerilog);
    // The following check for user specified switch specification must check both the enable/disable version of the switches.  However
    // getting the value it is only necessary that one switch be checked.
    bool userSpecifiedSwitchForEmptyConcatTerms  = ((sCarbonContext->getArgs()->isParsed(CarbonContext::scEnableIgnoreZeroReplicationConcatItem) == ArgProc::eParsed) ||
                                                    (sCarbonContext->getArgs()->isParsed(CarbonContext::scDisableIgnoreZeroReplicationConcatItem) == ArgProc::eParsed)  );
    bool userSaysEmptyConcatTermsAreIgnored = sCarbonContext->getArgs()->getBoolValue(CarbonContext::scEnableIgnoreZeroReplicationConcatItem);

    bool emptyConcatTermsAreIgnored = ( ( languageSaysEmptyConcatTermsAreIgnored && !userSpecifiedSwitchForEmptyConcatTerms ) ||
                                        ( userSpecifiedSwitchForEmptyConcatTerms && userSaysEmptyConcatTermsAreIgnored ) );

    Verific::RuntimeFlags::SetVar("veri_carbon_ignore_concat_items_with_0_repeat_count", emptyConcatTermsAreIgnored ? 1 : 0);

    Verific::RuntimeFlags::SetVar("vhdl_max_array_size", 29); // tell verific to report error for arrays with more than 29 bits of address
  }

  if (sCarbonContext->getArgs()->getBoolValue("-w")) {
    suppressAllWarnings();
  }
}

#define VERI_MSG_OFFSET 50000
#define VHDL_MSG_OFFSET 60000


// attempts to convert a carbon message number into a Verific message number of the form "VERI-1111" or "VHDL-2222"
// if conversion is successful then true is returned, and \a verific_msg_numer will hold the verific message number
// if failed conversion then returns false and \a verific_msg_numer is unchanged
static bool sConvertToVerificMsgNumber( int carbon_msg_number, UtString* verific_msg_number ){
  if ((VERI_MSG_OFFSET <= carbon_msg_number) && (carbon_msg_number <= (VHDL_MSG_OFFSET + 9999))) {
    // this is in the range of a verific messages
    if ((VERI_MSG_OFFSET < carbon_msg_number) && (VERI_MSG_OFFSET + 9999 > carbon_msg_number)) {
      *verific_msg_number = "VERI-";
      *verific_msg_number << (carbon_msg_number-VERI_MSG_OFFSET);
      return true;
    } else if ((VHDL_MSG_OFFSET < carbon_msg_number) && (VHDL_MSG_OFFSET + 9999 > carbon_msg_number)) { 
      *verific_msg_number = "VHDL-";
      *verific_msg_number << (carbon_msg_number-VHDL_MSG_OFFSET);
      return true;
    }
    INFO_ASSERT((carbon_msg_number != 0), "Internal inconsistency, message id out of known range");
  }
  return false;
}

// this function returns true if the \a msgNumber identifies a verific message that cannot have its severity changed from 'error'.
static bool verificMsgMustBeErrors(int msgNumber) {
  UtString verificMsgNumber;
  if ( sConvertToVerificMsgNumber(msgNumber, &verificMsgNumber) ) {
    // we now know that this is in the verific message number range.

    // TODO: what we need here is list of all the message IDs that must remain as errors.
    // it appears that there is no such table in the Verific code so instead the table must be created here.
    // currently this switch statement is incomplete
    switch (msgNumber){
    case 51137:
    case 51138:
    case 61174:
    case 61261:
    case 61622:
    case 61284:
    {
      return true;
    }
    case 51107:{
      return (0 != Verific::RuntimeFlags::GetVar("veri_carbon_ignore_concat_items_with_0_repeat_count"));
    }
    }
  }
  return false;
}

//! all changes to message text should go here
void VerificContext::adjustMessageText() {
  Verific::Message::SetNewFormattedString("VERI-1098", "Direction already specified for the object (%s)") ; // matches interra message to reduce regolds, was "port %s is already defined","VERI-1098") ;
  bool ignoreZeroRepeatCountItems = (0 != Verific::RuntimeFlags::GetVar("veri_carbon_ignore_concat_items_with_0_repeat_count"));
  if (! ignoreZeroRepeatCountItems ){
    Verific::Message::SetNewFormattedString("VERI-1107", "Zero or negative repetition multiplier in concatenation, expression with this multiplier will be replaced by 1'b0") ; // matches interra message to reduce regolds, was "repetition multiplier must be positive", VERI-1107);
  }
  Verific::Message::SetNewFormattedString("VHDL-1686", "signal %s is used in subtype-indication/type-definition - cannot elaborate statically"); // was "signal %s is used in subtype-indication/type-definition"
}

//! adjust the format argument to follow Carbon's message ID
void VerificContext::adjustSeverities() {
  // Verific messages to be silenced
  // the following message(s) were once silenced do we need to re-silence them again?
  ////Verific::Message::SetMessageType("VERI-1136", Verific::VERIFIC_IGNORE); // %s was previously declared with a different range

  static SInt32 error[] = {
    51042,  // illegal expression in target (VeriTimingCheck args for $setuphold) 
    51063,  // instantiating unknown module bar
    51098,  // Direction already specified for the object (out1)
    51445,  // event expressions must result in a singular type 
    51979,  // illegal to use all zero replications in concat
    61174,  // OTHERS is illegal aggregate choice for unconstrained target
    61241,  // entity is not declared
    61250,  //  xxx remains a black-box since it has no binding entity
    61346,  // partially associated formal b cannot have actual OPEN
    61617,  // value in initialization depends on signal 
    61081,  // formal n has no actual or default value
    61622,  // condition in if generate must be static
    61686,  // signal a is used in subtype-indication/type-definition
    -1}; 
  
  static SInt32 warning[] = {
    51393,  // cannot have packed dimensions of type %s
    51452,  // element index %d into %s is out of bounds
    -1};

  static SInt32 alert[] = {
    51158,  // use of undefined macro, (this message is also printed for unrecognized syntax for a defined macro ie `elsif`  (where there is no expression after elsif)
    51361,  // operator overloading is not supported yet
    51928,  // module %s does not have a parameter named %s to override
    61196,  // possible infinite loop; process does not have a wait statement
    -1};

  static SInt32 note[] = {
    -1
  };

  // there are two silent arrays.
  //  One for messages that are to be always silenced, and another
  //      for messages that are silenced unless the -showParseMessages switch is set.

  static SInt32 silent[] = {
    51376, // Warning 51376: missing or empty argument against format specification for display (A better msg is produced during populate)
    51865, // Silence specparam message (no Interra counterpart): Warning 51865: only one terminal allowed as the source or destination of parallel paths
    51492, // Pretty printing all modules in all libraries to file ...
    61382, // Silent message about standard directive options, Interra has no analog of it
    //61087, //Silent message (function <function_name> does not always return a value)
    61490, // Pretty printing all units in library work to file ...
    51493, //Alert 51493: The library libName was not found
    61255, //Alert 61255 subprogram func does not have a body
    61123, //Alert 61123 index %s is out of array constraint %s for target %s", "VHDL-1123
    61124, //Alert 61124: index value %s is out of range %s to %s
    61496, //Alert 61496: The library $libraryName was not found
    -1
  };

  // this array is for messages that are normally silenced, but are enabled if -showParseMessages command line switch is used
  static SInt32 silent_unless_showParseMessages[] = {
    51018,  // " compiling module word (VERI-1018). "
    51214,  // assignment to input %s
    51328,  // analyzing includeed file...
    51482,  // 'Analyzing Verilog file ...' message
    51489,  // Resolving module ...
    51842,  // 'ignoring incorrect/unknown option', as Carbon specific options may exist in -F filelist
    51931,  // potential always loop found
    61010,  // analyzing architecture
    61011,  // analyzing configuration
    61012,  // analyzing entity
    61013,  // analyzing package body
    61014,  // analyzing package
    61066,  // executing configuration
    61067,  // executing 
    61481,  // Analyzing VHDL file
    61493,  // Restoring VHDL parse tree
    61504,  // Analyzing VHDL library search path
    61505, //Note 61505: The vhdl library search path for library "work" is now $path
    51509, //Note 51509: The veri library search path for library "work" is now $path
    61499, //Note 61499: Saving VHDL parse-tree work.vh into $path
    61508, //Note 61508: Binary saving entity() || inary saving architecture(body)
    51523, //Note 51523: Saving Verilog parse-tree
    51231, //Note 51231: going to vhdl side to elaborate module $moduleName
    51232, //Note 51232: back to verilog to continue elaboration
    51503, //Note 51503: Restoring Verilog parse-tree ve_test5b.top from
    51206, //Warning 51206: overwriting previous definition of module top
    61399, //Note 61399: going to verilog side to elaborate module
    61400, //Note 61400: back to vhdl to continue elaboration
    -1
  };
  
  typedef UtHashMap<SInt32,MsgContextBase::Severity> SeverityMap;
  SeverityMap overrides;

  for (SInt32* p = error;  *p != -1; ++p) {overrides[*p] = MsgContextBase::eError;}
  for (SInt32* p = alert;  *p != -1; ++p) {overrides[*p] = MsgContextBase::eAlert;}
  for (SInt32* p = note;  *p != -1; ++p) {overrides[*p] = MsgContextBase::eNote;}
  for (SInt32* p = silent;  *p != -1; ++p) {overrides[*p] = MsgContextBase::eSuppress;}
  if (! sCarbonContext->getArgs()->getBoolValue("-showParseMessages")){
    // if showParseMessages is not specified then we also want to silence the messages in silent_unless_showParseMessages
    for (SInt32* p = silent_unless_showParseMessages;  *p != -1; ++p) {overrides[*p] = MsgContextBase::eSuppress;}
  }
  for (SInt32* p = warning;  *p != -1; ++p) {overrides[*p] = MsgContextBase::eWarning;}

  // now adjust severities of messages based on the side effect of command line arguments 
  bool ignoreZeroRepeatCountItems = (0 != Verific::RuntimeFlags::GetVar("veri_carbon_ignore_concat_items_with_0_repeat_count"));
  if ( ignoreZeroRepeatCountItems ){
    // if user wants 0 repeat count to be ignored (SystemVerilog or via -enableIgnoreZeroReplicationConcatItem cmd line) then the only time
    // cbuild prints this message is when the repeat count is negative, which should be an error (verific is too relaxed on this).
    overrides[51107] = MsgContextBase::eError;  // message about negative repeat count in replication operator: was "repetition multiplier must be positive", VERI-1107);
  }

  MsgContextBase::SeverityArray severities;
  MsgContextBase::IntArray msgNumbers;
  MsgContext* msgContext = sCarbonContext->getMsgContext();
  msgContext->getUnknownSeverityRange(VERI_MSG_OFFSET, VHDL_MSG_OFFSET + 9999, &severities, &msgNumbers );

  for (UInt32 i = 0; i < severities.size(); ++i) {
    MsgContextBase::Severity sev = severities[i];
    SInt32 msgNumber = msgNumbers[i];
    overrides[msgNumber] = sev;
  }

  for (SeverityMap::iterator p = overrides.begin(); p != overrides.end(); ++p) {
    SInt32 msgNumber = p->first;
    MsgContextBase::Severity sev = p->second;
    if (( sev != MsgContextBase::eError ) && verificMsgMustBeErrors( msgNumber)) {
      // warn user because this change to severity is not allowed
      const char* sevname = NULL ;
      
      switch ( sev ){
      case MsgContextBase::eSuppress: {sevname = "silent"; break;}
      case MsgContextBase::eNote:     {sevname = "info"; break;}
      case MsgContextBase::eWarning:  {sevname = "warning"; break;}
      case MsgContextBase::eAlert:    {sevname = "alert"; break;}
      case MsgContextBase::eError:    {sevname = "error"; break;}
      default:   {sevname = "<unknown>"; break;}
      }

      const SourceLocator loc;
      msgContext->IgnoringMsgSeverityAdjust(&loc,sevname, msgNumber, "Error" );
      // force our internal severity table back to the original severity
      sev = MsgContextBase::eError;
    }

    // now tell the parser what severity is to be used for each message
    // Take only messages from VERI_MSG_OFFSET : VHDL_MSG_OFFSET + 9999  - which contain all verific specific messages
    // Verilog messages 50000-59999, VHDL messages 60000-69999

    UtString verificMsgNumber;
    if ( sConvertToVerificMsgNumber(msgNumber, &verificMsgNumber) ) {
        // Get verific message severity type we want to specify
        Verific::msg_type_t verificMsgSeverity;
        switch ( sev ){
            case MsgContextBase::eSuppress: {verificMsgSeverity = Verific::VERIFIC_IGNORE; break;}
            case MsgContextBase::eNote:     {verificMsgSeverity = Verific::VERIFIC_INFO; break;}
            case MsgContextBase::eWarning:  {verificMsgSeverity = Verific::VERIFIC_WARNING; break;}
            case MsgContextBase::eAlert:    {verificMsgSeverity = Verific::VERIFIC_ALERT; break;} // there is now a verific equivalent to Alert so use it (at one time we had to use Error here)
            case MsgContextBase::eError:    {verificMsgSeverity = Verific::VERIFIC_ERROR; break;}
            default:                        {verificMsgSeverity = Verific::VERIFIC_IGNORE; break;}
        }
        // Set verific message severity
        Verific::Message::SetMessageType(verificMsgNumber.c_str(), verificMsgSeverity);
        // and also tell msgContext about this change, so that the messages will get to the correct libdesign.suppress or libdesign.warning type files
        msgContext->putMsgNumSeverity(msgNumber, sev);
    }
  }
}

void VerificContext::suppressAllWarnings()
{
    Verific::Message::SetAllMessageType(Verific::VERIFIC_WARNING, Verific::VERIFIC_IGNORE);
}


const char* VerificContext::verificReleaseString()
{
  // the release string is combination of Verific release number, and optionally the SVN revision number,
  // once all changes from SVN have been integrated into CVS the SVN revision number can be removed.
  if ( mVerificReleaseIdentifier == "" ){
    mVerificReleaseIdentifier = Verific::Message::ReleaseString();
    if (! (strncmp(SVNVERSION, UNDEFINEDSVNVERSION, strlen(UNDEFINEDSVNVERSION)) == 0) ) {
      mVerificReleaseIdentifier += ", URL: ";
      mVerificReleaseIdentifier += SVNURL;
      mVerificReleaseIdentifier += ", Revision: ";
      mVerificReleaseIdentifier += SVNVERSION;
    }
  }
  return mVerificReleaseIdentifier.c_str();
}

static bool sCheckArg(const char** names, const char* arg, bool isPrefix)
{
  for (const char** p = names; *p != '\0'; ++p)
  {
    if ((isPrefix && (strncmp(arg, *p, strlen(*p)) == 0)) ||
        (!isPrefix && (strcmp(arg, *p) == 0)))
      return true;
  }
  return false;
}


static bool sCheckVerilogArgs(UtStringArgv &allOpts, 
                              bool hasLibs,
                              MsgContext* msgContext)
{
  // Verify that there is at least one valid filename, and that
  // all the options we pass to Verific match options we are
  // comfortable with.

  static const char* sVerilogPrefixOptions[] = {
    "+libext+",
    "+incdir+",
    "+define+",
    "+topmodule+",
    //"+stopmodule+",  Removed for bug2517.
    "+decompile+",
    "+suppress+",
    "+info+",
    "+warning+",
    "+error+",
    NULL
  };


  // all the arguments in argv should be vetted by us against the
  // lists above.  There should be at least one explicit existing file or
  // Verific is likely to complain.
  int numFiles = 0;
  int numErrors = 0;
  FILE* f = NULL;

  int argc = allOpts.getArgc();
  char **argv = allOpts.getArgv();
  for (int i = 1; i < argc; ++i)
  {
    const char* arg = argv[i];
    if (sCheckArg(sVerilogFileOptions, arg, false) ||
        sCheckArg(sVerilogValOptions, arg, false) )
    {
      ++i;                      // skip next arg
      if (i == argc)
      {
        msgContext->VlogMissingArg(arg);
        ++numErrors;
      }
    }
    else if (sCheckArg(sVerilogIllegalOptions, arg, false))
    {
      msgContext->VlogInvalidOption(arg);
      ++numErrors;
    }
    else if (sCheckArg(sVerilogOptions, arg, false))
      ;                         // this option is OK
    else if (sCheckArg(sVerilogPrefixOptions, arg, true))
    {
      if (strncmp(arg, "+topmodule+", 11) == 0)
      {
        StrToken tok(arg + 11, "+");
        if (!tok.atEnd())
        {
          msgContext->OptionDeprecated("+topmodule+", "Use option -vlogTop instead");
          sCarbonContext->putTopModuleName(*tok);
          ++tok;
          if (!tok.atEnd())
          {
            msgContext->MultipleTopLevelModules(arg);
            ++numErrors;
          }
        }
      }
      else if (strncmp(arg, "+suppress+", 10) == 0) {
        msgContext->OptionDeprecated("+suppress+",
                                     "Use directive silentMsg <msg-number> instead");
      }
    }
    else if ((*arg == '-') || (*arg == '+'))
    {
      // extraneous +args go unwarned per verilog convention
      if (*arg == '-')
      {
        msgContext->NoSuchOption(arg);
        ++numErrors;
      }
    }
    else if ((f = OSFOpen(arg, "r", NULL)) != NULL)
    {
      ++numFiles;               // we can read it.
      fclose(f);
    }
    else
    {
      UtString errBuf;
      msgContext->VlogCannotOpenFile(arg, OSGetLastErrmsg(&errBuf));
      ++numFiles;
      ++numErrors;
    }
  }

  if ((numFiles == 0) && !hasLibs)
  {
    msgContext->VlogNoFiles();
    ++numErrors;
  }

  return (numErrors == 0);
}

bool VerificContext::parseVerilogArgs()
{
  bool ret = true;
  
  // Reassemble the options.  Get the ordered Verilog files from the
  // ArgProc.  unprocOpts has the unprocessed options to be passed
  // directly to Verific.
  ArgProc* args = sCarbonContext->getArgs();
  UtStringArgv verificArgList;
  verificArgList.push_back( sCarbonContext->getArgv()[0] ); // cbuild path

  HdlFileCollector::const_iterator lib_iter = sCarbonContext->getFileCollector()->begin();
  for (; lib_iter != sCarbonContext->getFileCollector()->end(); ++lib_iter )
  {
      LangVer VerilogVers[] = { LangVer::Verilog, LangVer::Verilog2001 };
      for(UInt32 ver = 0; ver < sizeof(VerilogVers)/sizeof(LangVer); ++ver)
      {
	  LangVer langVer = VerilogVers[ver];
	  
	  HdlLib::const_iterator file_iter = (*lib_iter)->begin( langVer );
	  for (; file_iter != (*lib_iter)->end( VerilogVers[ver] ); ++file_iter )
	  {
	      verificArgList.push_back( *file_iter );
	  }
      }
  }

  bool haveVhdlFiles =
      sCarbonContext->getFileCollector()->hasHdlFiles(LangVer::VHDL87)
      ||
      sCarbonContext->getFileCollector()->hasHdlFiles(LangVer::VHDL93);

  bool hasLibMap = (sCarbonContext->getArgs()->isParsed("-libMap") == ArgProc::eParsed);

  // If there are no VHDL files specified, complain if there are no
  // Verilog files or libraries present.
  if ( !sCarbonContext->getFileCollector()->hasHdlFiles(LangVer::Verilog) &&
       !sCarbonContext->getFileCollector()->hasHdlFiles(LangVer::Verilog2001) &&
       !sCarbonContext->getFileCollector()->hasLangLib(LangVer::Verilog) &&
       !hasLibMap )
  {
    if ( haveVhdlFiles == false )
    {
      sCarbonContext->getMsgContext()->VlogNoFiles();
      ret = false;
    }
    return ret;
  }

  UtStringArgv &unprocOpts = args->lookupUnprocessedGroupOccurrences( "Verilog" );
  int uargc = unprocOpts.getArgc();
  char** uargv = unprocOpts.getArgv();

  // Push the unprocessed Verilog args into arg.
  for ( int i = 0; i < uargc; ++i )
  {
    UtString buf(uargv[i]);
    if ( buf[0] == '+' ) {
      buf[0] = '-';
      bool req_value, is_input_file, is_output_file;
      if ( args->isValid(buf.c_str(), &req_value, &is_input_file, &is_output_file) ){
        sCarbonContext->getMsgContext()->MatchingPlusAndCarbonArg(uargv[i]);
      }
    }

    // note the following replacement of -incdir with +incdir+ is only for argument checking
    // in sCheckVerilogArgs, it does not affect the switches handled by -f processing done
    // by verific parser.

    // If -incdir <directory> was specified, convert it to +incdir+<directory>
    if ( buf == "-incdir" ) {
      UtString dir = "+incdir+";
      dir += uargv[++i];        // note this increments i so that we don't process the <directory> again by FORloop
      verificArgList.push_back( dir );
    }
    else {
      verificArgList.push_back( uargv[i] );
    }
  }

  if ( !sCheckVerilogArgs( verificArgList,
                           sCarbonContext->getFileCollector()->hasLangLib(LangVer::Verilog) || hasLibMap,
                           sCarbonContext->getMsgContext() ))
    return false;

  return ret;
}

