%{ /* -*-C++-*- */
#include <stdlib.h>
#include <cstring>
#include <string>
#include "compiler_driver/ProtectedSource.h"
#include "util/CbuildMsgContext.h"

static void sNewline();
static void sBeginProtect(const char *text, int len);
static void sEndProtect(const char *text, int len);

static long sLineNumber;

#define protector  SourceProtector::currentProtector
#define fileName   SourceProtector::currentFileName
#define msgContext SourceProtector::currentMsgContext
#define YY_NO_UNPUT
%}

/* To avoid isatty problems on Windows. */
%option never-interactive

%x COMMENT
%x PROTECT
%x STRING

W [\n\r\t\b ]

%%
"/*"				   { protector->write(yytext, yyleng); BEGIN COMMENT; /* consume block comments */ }
<COMMENT>"*/"			   { protector->write(yytext, yyleng); BEGIN 0; }

"//"[^\n]*\n			   { protector->write(yytext, yyleng); sNewline();  /* consume line comments */ }

\"				   { protector->write(yytext, yyleng); BEGIN STRING; /* consume constant string */ }
<STRING>\\\"			   { protector->write(yytext, yyleng); }
<STRING>\"			   { protector->write(yytext, yyleng); sNewline(); BEGIN 0; }

`protect{W}			   { sBeginProtect(yytext, yyleng); BEGIN PROTECT; }
<PROTECT>`endprotect{W} 	   { sEndProtect(yytext, yyleng); BEGIN 0; }

<*>\n                              { protector->write(yytext, yyleng); sNewline(); }
<*>. 				   { protector->write(yytext, yyleng); }
%%

int Protect_wrap()
{
  return 1;  // no more input
}

static void sNewline()
{
  ++sLineNumber;
}

static void sCountNewlines(const char *text, int len)
{
  for (int i = 0; i < len; i++)
    if (text[i] == '\n')
      ++sLineNumber;
}

static void sBeginProtect(const char *text, int len)
{
  if (protector->getEncrypt())
  {
    msgContext->NestedProtect(fileName, sLineNumber);
  }
  protector->write("`protected\n", 11); // replace `protect with `protected
  sCountNewlines(text, len);            // bump line number if whitespace contained \n
  protector->setEncrypt(true);          // enter encrypted output mode
  protector->write(text + 8, len - 8);  // output whatever whitespace followed `protect
  // If we're allowing -generateCleartextSource with this HDL...
  if (protector->exposeProtected())
    protector->write(ProtectedSource::cleartextComment(),
                     strlen(ProtectedSource::cleartextComment()));
}

static void sEndProtect(const char *text, int len)
{
  if (!protector->getEncrypt())
  {
    msgContext->UnexpectedEndProtect(fileName, sLineNumber);
  }  
  protector->setEncrypt(false);
  protector->write("\n`endprotected", 14);
  protector->write(text + 11, len - 11);
  sCountNewlines(text, len);
}

