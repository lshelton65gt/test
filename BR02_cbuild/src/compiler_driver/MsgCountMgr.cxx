// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/MsgCountMgr.h"

// Determine whether the exact text of a message has been printed
// more than the user controllable limit. If so, print out a
// warning indicating that the limit has been reached. 
eCarbonMsgCBStatus 
MsgCountManager::getMsgCBStatus(CarbonMsgSeverity severity,
                                int msgNumber, const char* text,
                                unsigned int len)
{
  eCarbonMsgCBStatus msgStatus = eCarbonMsgContinue;
  if ((severity != eCarbonMsgSuppress) && (text != NULL) && len > 0)
  {
    MsgCount& mc = mMsgCountMap[msgNumber];
    mc.addMessage(text, len);
    if (mc.getCount() >= mMaxMsgRepeatCount) {
      if (mc.getCount() == mMaxMsgRepeatCount) {
        mMsgContext->MaxMsgRepeatCountLimitReached(msgNumber, mc.getMsg());
      }
      msgStatus = eCarbonMsgStop;
    }
  }
  return msgStatus;
}

// Check to see if this is the same message previously printed for this
// message number. (Note that it may be different, due to different
// substitution variables.) If so, increment the count. If not,
// clear out the old message, replace it with the new one, and clear
// the counter.
void MsgCountManager::MsgCount::addMessage(const char* msg, unsigned int len)
{
  if ((mMsg.length() == len) &&
      (strncmp(mMsg.c_str(), msg, len) == 0)) {
    ++mCount;
  } else {
    mMsg.clear();
    mMsg.append(msg, len);
    mCount = 0;
  }
}
