// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/* changes to this file were derived from Interra supplied examples */
/****************************************************************
***************
*****  FILE  : udpDecomp.c
*****  AUTHOR :Punit Sindhwani
*****  DATE   :16/07/96
*****  RCS  HISTORY :

*****  BRIEF DESC : This file defines the routines for decompiling
       the UDP and related verilog constructs

*****************************************************************
***************/

#include "Decompile.h"

/***********************************************************
   Function       : udpDecompile

   Purpose        : Decompilation routine for an UDP.

   Parameters     : veNode

   Returns        : void

   End Header
**************************************************************/

void 
Decompile::udpDecompile (veNode nodeUdp)
{
  Protector protector(this, nodeUdp);

  
  CheetahList     udpPortList, udpNetList, tableEntryList;
  veNode          port, net, initialStmt, tableEntry;
  CheetahList     attrInstanceList;
  
  /* see if there is a timescale directive */
  
  veString timeUnit = veUDPGetTimeUnit(nodeUdp);
  veString timePrecision = veUDPGetTimePrecision(nodeUdp);
  
  if ( timeUnit && timePrecision )
  {
    *this << eTimescale;
    writeOutput( timeUnit, 1);
    *this << eSlash;
    writeOutput( timePrecision, 1);
    *this << eNewline;
  }
  
  *this << ePrimitive;
  if ((attrInstanceList = veNodeGetAttrInstance(nodeUdp)) != NULL) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  obfuscate(CheetahStr(veUDPGetName (nodeUdp)));
  
  udpPortList = veUDPGetPortList (nodeUdp);
  
  if (udpPortList)
  {
    *this << eOpenParen;
    
    port = veListGetNextNode (udpPortList);
    
    obfuscate(CheetahStr(veNamedObjectGetName (port)));
    
    for (; (port = veListGetNextNode (udpPortList));)
    {
      *this << eComma;
      obfuscate(CheetahStr(veNamedObjectGetName (port)));
    }
  }
  *this << eCloseParen << eSemiColon << eNewline;
  
/*
  if ( veUdpGetIsSequential(nodeUdp) == VE_TRUE )
    decomPrintf (outputFile, " // Sequential UDP ", 1);
  decomPrintf (outputFile, "\n", 0);
*/
  
  udpNetList = veUDPGetNetList (nodeUdp);
  
  if (udpNetList)
  {
    net = veListGetNextNode (udpNetList);
    
    for (; net; net = veListGetNextNode (udpNetList))
      netDecompile (net);
    
  }
  initialStmt = veUDPGetInitialStmt (nodeUdp);
  
  if (initialStmt)
  {
    initialDecompile (initialStmt);
    *this << eSemiColon;
  }
  *this << eTable;
  
  tableEntryList = veUDPGetTableEntryList (nodeUdp);
  
  if (tableEntryList)
  {
    tableEntry = veListGetNextNode (tableEntryList);
    
    for (; tableEntry; tableEntry = veListGetNextNode (tableEntryList))
    {
      if (veNodeGetObjType (tableEntry) == VE_COMBINATIONALENTRY)
        combinationalEntryDecompile (tableEntry);
      else
        sequentialEntryDecompile (tableEntry);
      
      *this << eSemiColon << eNewline;
    }
  }
  *this << eEndTable << eEndPrimitive << eNewline;
}


/***********************************************************
   Function       : combinationalEntryDecompile

   Purpose        : Decompilation routine for a
                    combinational entry in an UDP

   Parameters     : veNode

   Returns        : void

   End Header
**************************************************************/

void 
Decompile::combinationalEntryDecompile (veNode nodeCombEntry)
{
  Protector protector(this, nodeCombEntry);

  
  CheetahList       levelInputList;
  veNode            levelInput, outputSymbol;
  
  
  levelInputList = veCombEntryGetLevelInputList (nodeCombEntry);
  
  if (levelInputList)
  {
    levelInput = veListGetNextNode (levelInputList);
    
    for (; levelInput; levelInput = veListGetNextNode (levelInputList))
    {
      levelSymbolDecompile (levelInput);
      *this << eSpace;
    }
  }
  *this << eColon;
  
  outputSymbol = veTableEntryGetOutputSymbol (nodeCombEntry);
  
  if (outputSymbol)
    outputSymbolDecompile (outputSymbol);
  
}



/***********************************************************
   Function       : sequentialEntryDecompile

   Purpose        : Decompilation routine for a
                    sequential UDP entry.

   Parameters     : veNode

   Returns        : void

   End Header
**************************************************************/

void 
Decompile::sequentialEntryDecompile (veNode nodeSeqEntry)
{
  Protector protector(this, nodeSeqEntry);

  
  veNode          udpInputList;
  veNode          currentState, outputSymbol;
  
  
  udpInputList = veSeqEntryGetUdpInputList (nodeSeqEntry);
  
  if (udpInputList)
  {
    if (veNodeGetObjType (udpInputList) == VE_UDPLEVELINPUTLIST)
      levelInputListDecompile (udpInputList);
    else
      edgeInputListDecompile (udpInputList);
  }
  currentState = veSeqEntryGetCurrentState (nodeSeqEntry);
  
  if (currentState)
  {
    *this << eColon;
    levelSymbolDecompile (currentState);
  }
  outputSymbol = veTableEntryGetOutputSymbol (nodeSeqEntry);
  
  if (outputSymbol)
  {
    *this << eColon;
    outputSymbolDecompile (outputSymbol);
  }
}



/***********************************************************
   Function       : levelSymbolDecompile

   Purpose        : Decompilation routine for a Level Symbol
                    in an UDP table.

   Parameters     : veNode

   Returns        : void

   End Header
**************************************************************/


void 
Decompile::levelSymbolDecompile (veNode nodeLevelSymbol)
{
  Protector protector(this, nodeLevelSymbol);

  
  switch (veLevelSymbolGetSymbol (nodeLevelSymbol))
  {
  case LEVEL_ONE:
    *this << '1';
    break;
    
  case LEVEL_ZERO:
    *this << '0';
    break;
    
  case LEVEL_X:
    *this << 'x';
    break;
    
  case LEVEL_UNKNOWN:
    *this << '?';
    break;
    
  case LEVEL_B:
    *this << 'b';
    break;
  case LEVEL_UNSET:
    break;
  }
}

/***********************************************************
   Function       : outputSymbolDecompile

   Purpose        : Decompilation routine for Output
                    Symbols in an  UDP table.

   Parameters     : veNode

   Returns        : void

   End Header
**************************************************************/


void 
Decompile::outputSymbolDecompile (veNode nodeOutputSymbol)
{
  Protector protector(this, nodeOutputSymbol);

  switch (veOutputSymbolGetSymbol (nodeOutputSymbol))
  {
  case OUTPUT_ONE:
    *this << '1';
    break;
    
  case OUTPUT_ZERO:
    *this << '0';
    break;
    
  case OUTPUT_X:
    *this << 'x';
    break;
    
  case OUTPUT_NO_CHANGE:
    *this << '-';
    break;
  case OUTPUT_UNSET:
    break;
  }
  
}

/***********************************************************
   Function       : edgeSymbolType1Decompile

   Purpose        : Decompilation routine for edge symbols
                    of the type (01) in a sequential UDP
                    table entry.

   Parameters     : veNode

   Returns        : void

   End Header
**************************************************************/


void 
Decompile::edgeSymbolType1Decompile (veNode nodeEdgeSymbolType1)
{
  Protector protector(this, nodeEdgeSymbolType1);

  
  veNode          levelSymbol1, levelSymbol2;
  
  
  *this << eOpenParen;
  
  levelSymbol1 = veEdgeSymbolType1GetLevelSymbol1 (nodeEdgeSymbolType1);
  
  if (levelSymbol1)
    levelSymbolDecompile (levelSymbol1);
  
  levelSymbol2 = veEdgeSymbolType1GetLevelSymbol2 (nodeEdgeSymbolType1);
  
  if (levelSymbol2)
    levelSymbolDecompile (levelSymbol2);
  
  *this << eCloseParen;
  
}


/***********************************************************
   Function       : edgeSymbolType2Decompile

   Purpose        : Decompilation routine for edge symbols
                    of the type "r", "f", etc. in a sequential UDP
                    table entry.

   Parameters     : veNode

   Returns        : void

   End Header
**************************************************************/


void 
Decompile::edgeSymbolType2Decompile (veNode nodeEdgeSymbolType2)
{
  Protector protector(this, nodeEdgeSymbolType2);

  switch (veEdgeSymbolType2GetSymbol (nodeEdgeSymbolType2))
  {
  case RISING_EDGE:
    *this << 'r';
    break;
    
  case FALLING_EDGE:
    *this << 'f';
    break;
    
  case POSITIVE_EDGE:
    *this << 'p';
    break;
    
  case NEGATIVE_EDGE:
    *this << 'n';
    break;
    
  case ALL_EDGE:
    *this << '*';
    break;
  case UNKNOWN_EDGE:
    break;
  }
  
  *this << eSpace;
  
}

/***********************************************************

   Function       : edgeInputListDecompile

   Purpose        : Decompilation routine for a  edge input
                    list in a sequential UDP.

   Parameters     : veNode

   Returns        : void

   End Header
**************************************************************/


void 
Decompile::edgeInputListDecompile (veNode nodeEdgeInputList)
{
  Protector protector(this, nodeEdgeInputList);

  
  CheetahList       levelInputList1, levelInputList2;
  veNode            levelInput, edgeSymbol;
  
  
  levelInputList1 = veUdpEdgeInputListGetUdpLevelInputList1 (nodeEdgeInputList);
  
  if (levelInputList1)
  {
    levelInput = veListGetNextNode (levelInputList1);
    
    for (; levelInput; levelInput = veListGetNextNode (levelInputList1))
    {
      levelSymbolDecompile (levelInput);
      *this << eSpace;
    }
  }
  edgeSymbol = veUdpEdgeInputListGetEdge (nodeEdgeInputList);
  
  if (edgeSymbol)
  {
    if (veNodeGetObjType (edgeSymbol) == VE_EDGESYMBOLTYPE1)
      edgeSymbolType1Decompile (edgeSymbol);
    
    else
      edgeSymbolType2Decompile (edgeSymbol);
    
  }
  
  *this << eSpace;
  
  levelInputList2 = veUdpEdgeInputListGetUdpLevelInputList2 (nodeEdgeInputList);
  
  if (levelInputList2)
  {
    levelInput = veListGetNextNode (levelInputList2);
    
    for (; levelInput; levelInput = veListGetNextNode (levelInputList2))
    {
      levelSymbolDecompile (levelInput);
      *this << eSpace;
    }
  }
}

/***********************************************************

   Function       : levelInputListDecompile

   Purpose        : Decompilation routine for a  level input
                    list in a sequential UDP.

   Parameters     : veNode

   Returns        : void

   End Header
**************************************************************/



void 
Decompile::levelInputListDecompile (veNode nodeLevelInputList)
{
  Protector protector(this, nodeLevelInputList);

  
  CheetahList       levelInputList;
  veNode            levelInput;
  
  levelInputList = veUdpLevelInputListGetUdpLevelInputList (nodeLevelInputList);
  
  if (levelInputList)
  {
    levelInput = veListGetNextNode (levelInputList);
    
    for (; levelInput; levelInput = veListGetNextNode (levelInputList))
    {
      levelSymbolDecompile (levelInput);
      *this << eSpace;
    }
  }
}
