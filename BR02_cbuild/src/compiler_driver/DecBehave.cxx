// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/* changes to this file were derived from Interra supplied examples */
/****************************************************************
 ***************
 *****  FILE  : behavioralDecomp.c
 *****  AUTHOR :Punit Sindhwani
 *****  DATE   :16/07/96
 *****  RCS  HISTORY :

 *****  BRIEF DESC : This file defines the routines for decompiling
       the behavioral verilog constructs.

       *****************************************************************
       ***************/

#include <stdio.h>
#include <string.h>

#include "Decompile.h"
#include "compiler_driver/Interra.h"

/*****
      Function       : initialDecompile

      Purpose        : Decompilation routine for INITIAL block.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/

void 
Decompile::initialDecompile (veNode nodeInitial)
{
  Protector protector(this, nodeInitial);

  CheetahList     attrInstanceList;
  veNode          stmt = veInitialGetStatement (nodeInitial);
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeInitial)) ) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  if (stmt)
  {
    *this << eInitial << eNewline;
    stmtDecompile (stmt);
  }
}

/*****
      Function       : alwaysDecompile
      
      Purpose        : Decompilation routine for ALWAYS block.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/

void 
Decompile::alwaysDecompile (veNode nodeAlways)
{
  Protector protector(this, nodeAlways);

  veNode          stmt = veAlwaysGetStatement (nodeAlways);
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeAlways))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eAlways;
  if (veNodeGetObjType (stmt)==VE_WAIT)
    alwayswaitDecompile (stmt);
  else
    stmtDecompile (stmt);
}

/*****
      Function       : ifDecompile
      
      Purpose        : Decompilation routine for IF construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/

void 
Decompile::ifDecompile (veNode nodeIf)
{
  Protector protector(this, nodeIf);

  veNode          ifCondition;
  veNode          thenPart, elsePart;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeIf))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eIf << eOpenParen;

  ifCondition = veIfGetCondition (nodeIf);
  
  exprDecompile (ifCondition);
  
  *this << eCloseParen << eNewline;
  
  thenPart = veIfGetThenPart (nodeIf);
  
  
  if (thenPart)
    stmtDecompile (thenPart);
  else
    *this << eSemiColon << eNewline;
  
  elsePart = veIfGetElsePart (nodeIf);
  
  if (elsePart)
  {
    *this << eElse;
    stmtDecompile (elsePart);
  }
}


/*****
      Function       : foreverDecompile
      
      Purpose        : Decompilation routine for FOREVER construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::foreverDecompile (veNode nodeForever)
{
  Protector protector(this, nodeForever);

  veNode          forEverStmt;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeForever))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eForever << eNewline;
  forEverStmt = veForeverGetStatement (nodeForever);
  stmtDecompile (forEverStmt);
  
}

/*****
      Function       : repeatDecompile
      
      Purpose        : Decompilation routine for REPEAT construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::repeatDecompile (veNode nodeRepeat)
{
  Protector protector(this, nodeRepeat);

  veNode          repeatCondition;
  veNode          repeatStmt;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeRepeat))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eRepeat << eOpenParen;
  
  repeatCondition = veRepeatGetCondition (nodeRepeat);
  
  exprDecompile (repeatCondition);
  
  *this << eCloseParen << eNewline;
  
  repeatStmt = veRepeatGetStatement (nodeRepeat);
  
  stmtDecompile (repeatStmt);
  
}

/*****
      Function       : whileDecompile
      
      Purpose        : Decompilation routine for WHILE construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::whileDecompile (veNode nodeWhile)
{
  Protector protector(this, nodeWhile);
  
  veNode          whileCondition;
  veNode          whileStmt;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeWhile))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eWhile << eOpenParen;
  
  whileCondition = veWhileGetCondition (nodeWhile);
  
  exprDecompile (whileCondition);
  
  *this << eCloseParen << eNewline;
  
  whileStmt = veWhileGetStatement (nodeWhile);
  
  stmtDecompile (whileStmt);
}





/*****
      Function       : forDecompile
      
      Purpose        : Decompilation routine for FOR construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::forDecompile (veNode nodeFor)
{
  Protector protector(this, nodeFor);
  
  veNode          forCondition;
  veNode          forInitial, forStep, forStmt;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeFor))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eFor << eOpenParen;
  
  forInitial = veForGetInitial (nodeFor);
  
  stmtDecompile (forInitial);
  
  *this << eSemiColon;
  
  forCondition = veForGetCondition (nodeFor);
  
  exprDecompile (forCondition);
  
  *this << eSemiColon;
  
  forStep = veForGetStep (nodeFor);
  
  stmtDecompile (forStep);
  
  *this << eCloseParen << eNewline;
  
  forStmt = veForGetStmtPart (nodeFor);
  
  stmtDecompile (forStmt);
}


/*****
      Function   :generateForDecompile
      
      Purpose    :decompiler for the object of type VeGenerateFor
      
      Parameters :veNode ::VeGenerateFor  Object type pointer
      
      Returns    :void
      
      End Header
*****/

void
Decompile::generateForDecompile (veNode generateForNode) {
  Protector protector(this, generateForNode);

  veNode          genForCondition;
  veNode          genForInitial, genForStep;
  veNode          block;
  CheetahList     blockItemList;
  veNode          blockItem; 
  
  *this << eFor << eOpenParen;
  genForInitial = veGenerateForGetInitial (generateForNode);
  stmtDecompile (genForInitial);
  *this << eSemiColon;
  genForCondition = veGenerateForGetCondition (generateForNode);
  exprDecompile (genForCondition);
  *this << eSemiColon;
  genForStep = veGenerateForGetStep (generateForNode);
  stmtDecompile (genForStep);
  *this << eCloseParen << eNewline;
  block = veGenerateForGetBlock (generateForNode);
  *this << eBegin << eColon;
  obfuscate(CheetahStr(veGenerateBlockGetName (block)));
  *this << eNewline;
  blockItemList = veGenerateBlockGetItemList (block);
  
  while ((blockItem = veListGetNextNode (blockItemList))) {
    generateItemDecompile (blockItem);
  }
  *this << eEnd << eNewline;
  
}


/*****
      Function   :generateBlockDecompile
      
      Purpose    :decompiler for the object of type VeGenerateBlock
      
      Parameters :veNode ::VeGenerateBlock  Object type pointer
      
      Returns    :void
      
      End Header
*****/
void
Decompile::generateBlockDecompile (veNode generateBlockNode) {
  Protector protector(this, generateBlockNode);

  CheetahList  blockItemList;
  veNode       blockItem;
  
  *this << eBegin;
  CheetahStr blockName(veGenerateBlockGetName (generateBlockNode));
  if (blockName != NULL) {
    *this << eColon;
    obfuscate(blockName);
  }
  *this << eNewline;
  blockItemList = veGenerateBlockGetItemList (generateBlockNode);
  
  while ((blockItem = veListGetNextNode (blockItemList))) {
    generateItemDecompile (blockItem);
  }
  *this << eEnd << eNewline;
  
}



/*****
      Function   :generateIfDecompile
      
      Purpose    :decompiler for the object of type VeGenerateIf
      
      Parameters :veNode ::VeGenerateIf  Object type pointer
      
      Returns    :void
      
      End Header
*****/
void
Decompile::generateIfDecompile (veNode generateIfNode) {
  Protector protector(this, generateIfNode);

  veNode          genIfCondition;
  veNode          genThenPart, genElsePart;
  
  
  *this << eIf << eOpenParen;
  
  genIfCondition = veGenerateIfGetCondition (generateIfNode);
  
  exprDecompile (genIfCondition);
  
  *this << eCloseParen << eNewline;
  
  genThenPart = veGenerateIfGetThenPart (generateIfNode);
  
  generateItemDecompile (genThenPart);
  
  genElsePart = veGenerateIfGetElsePart (generateIfNode);
  
  if (genElsePart)
  {
    *this << eElse << eNewline;
    generateItemDecompile (genElsePart);
  }
  
}

// Write any full_case or parallel_case directive as appropriate
void Decompile::writeCaseDirective(veNode node)
{
  Protector protector(this, node);

  CheetahList directives_iter;
  directives_iter = veNodeGetSynthDirectiveList(node);
  if (directives_iter != NULL)
  {
    bool hasFull = false;
    bool hasParallel = false;
    veNode ve_synth_dir;
    veSynthDirectiveType ve_synth_dir_type;
    while ((ve_synth_dir = veListGetNextNode(directives_iter)) != NULL)
    {
      ve_synth_dir_type = veSynthDirectiveGetDirectiveType(ve_synth_dir);
      switch (ve_synth_dir_type)
      {
      case VE_PARALLEL_CASE:
        hasParallel = true;
        break;
      case VE_FULL_CASE:
        hasFull = true;
        break;
      case VE_BOTH_CASE:
        hasFull = true;
        hasParallel = true;
        break;
      default:
        break;
      }
    }

    if (hasFull && hasParallel)
      *this << eFullParallelCase;
    else if (hasFull)
      *this << eFullCase;
    else if (hasParallel)
      *this << eParallelCase;
  }
} // void Decompile::writeCaseDirective

/*****
      Function   :generateCaseDecompile
      
      Purpose    :decompiler for the object of type VeGenerateCase
      
      Parameters :veNode ::VeGenerateCase  Object type pointer
      
      Returns    :void
      
      End Header
*****/
void
Decompile::generateCaseDecompile (veNode generateCaseNode) {
  Protector protector(this, generateCaseNode);

  veNode     caseExpr;
  CheetahList   caseItemItr;
  veNode     caseItem;
  
  *this << eCase << eOpenParen;
  caseExpr = veGenerateCaseGetExpr (generateCaseNode);
  exprDecompile (caseExpr);
  *this << eCloseParen;
  writeCaseDirective(generateCaseNode);
  *this << eNewline;


  caseItemItr = veGenerateCaseGetItemList (generateCaseNode);
  
  caseItem = veListGetNextNode (caseItemItr);
  
  for (; caseItem; caseItem = veListGetNextNode (caseItemItr))
    
    generateCaseItemDecompile (caseItem);
  
  *this << eEndCase;
}



/*****
      Function       : caseDecompile
      
      Purpose        : Decompilation routine for CASE construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/

void 
Decompile::caseDecompile (veNode nodeCase)
{
  Protector protector(this, nodeCase);

  veNode          caseExpr;
  CheetahList     caseItemItr;
  veNode          caseItem;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeCase))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  switch (veNodeGetObjType (nodeCase))
  {
  case VE_CASE:
    *this << eCase;
    break;
    
  case VE_CASEX:
    *this << eCasex;
    break;
    
  case VE_CASEZ:
    *this << eCasez;
    break;
  default :
    break;
  }
  
  *this << eOpenParen;
  caseExpr = veCaseGetCaseExpr (nodeCase);
  
  exprDecompile (caseExpr);
  
  *this << eCloseParen;
  writeCaseDirective(nodeCase);
  *this << eNewline;
  
  caseItemItr = veCaseGetCaseItemList (nodeCase);
  
  caseItem = veListGetNextNode (caseItemItr);
  
  for (; caseItem; caseItem = veListGetNextNode (caseItemItr))
    
    caseItemDecompile (caseItem);
  
  
  *this << eEndCase << eNewline;
}

/*****
      Function   :generateCaseItemDecompile
      
      Purpose    :decompiler for the object of type VeGenerateCaseItemNode
      
      Parameters :veNode ::VeGenerateCaseItemNode  Object type pointer
      
      Returns    :void
      
      End Header
*****/

void 
Decompile::generateCaseItemDecompile (veNode generateCaseItemNode) {
  Protector protector(this, generateCaseItemNode);

  CheetahList     generateCaseItemExprList;
  veNode            expr;
  veNode            generateCaseItemStmt;
  
  generateCaseItemExprList = veGenerateCaseItemGetExprList (generateCaseItemNode); 
  if (generateCaseItemExprList == NULL)
    *this << eDefault << eColon << eNewline;
  
  else {
    expr = veListGetNextNode (generateCaseItemExprList);
    
    exprDecompile (expr);
    
    for (; (expr = veListGetNextNode (generateCaseItemExprList));) {
      *this << eComma << eNewline;
      exprDecompile (expr);
    }
    *this << eColon << eNewline;
  }
  generateCaseItemStmt = veGenerateCaseItemGetStmt (generateCaseItemNode);
  
  if (generateCaseItemStmt)
    generateItemDecompile (generateCaseItemStmt);
  
  else
    *this << eSemiColon << eNewline;
  
}




/*****
      Function       : caseItemDecompile
      
      Purpose        : Decompilation routine for each
      case item in a CASE construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::caseItemDecompile (veNode nodeCaseItem)
{
  Protector protector(this, nodeCaseItem);

  CheetahList     caseItemExprList;
  veNode          expr;
  veNode          caseItemStmt;
  
  caseItemExprList = veCaseItemGetExprList (nodeCaseItem);
  
  if (caseItemExprList == NULL)
    *this << eDefault << eColon;
  
  else
  {
    expr = veListGetNextNode (caseItemExprList);
    
    exprDecompile (expr);
    
    for (; (expr = veListGetNextNode (caseItemExprList));)
    {
      *this << eComma << eNewline;
      exprDecompile (expr);
      
    }
    *this << eColon << eNewline;
  }
  
  caseItemStmt = veCaseItemGetStatement (nodeCaseItem);
  
  if (caseItemStmt)
    stmtDecompile (caseItemStmt);
  
  else
    *this << eSemiColon << eNewline;
  
}


/*****
      Function       : waitDecompile
      
      Purpose        : Decompilation routine for WAIT construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::waitDecompile (veNode nodeWait)
{
  Protector protector(this, nodeWait);

  veNode          waitCondition;
  veNode          waitStmt;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeWait))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  *this << eWait << eOpenParen;
  
  waitCondition = veWaitGetCondition (nodeWait);
  
  exprDecompile (waitCondition);
  
  *this << eCloseParen << eNewline;
  
  waitStmt = veWaitGetStmt (nodeWait);
  if (waitStmt)
    stmtDecompile (waitStmt);
  else
    *this << eSemiColon << eNewline;
  
}

void
Decompile::alwayswaitDecompile (veNode nodeWait)
{
  Protector protector(this, nodeWait);

  veNode          waitCondition;
  veNode          waitStmt;
  
  
  *this << eWait << eOpenParen;
  
  waitCondition = veWaitGetCondition (nodeWait);
  
  exprDecompile (waitCondition);
  
  *this << eCloseParen << eNewline;
  
  waitStmt = veWaitGetStmt (nodeWait);
  
  stmtDecompile (waitStmt);
  
}

/*****
      Function       : disableDecompile
      
      Purpose        : Decompilation routine for DISABLE construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::disableDecompile (veNode nodeDisable)
{
  Protector protector(this, nodeDisable);

  veNode          taskOrBlock;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeDisable))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  *this << eDisable;
  
  taskOrBlock = veDisableGetTaskOrBlock (nodeDisable);
  
  CheetahStr nCheetahStr;
  if (veNodeGetObjType (taskOrBlock) == VE_SCOPEVARIABLE) {
    nCheetahStr = veScopeVariableGetHierName (taskOrBlock);
  } else {
    nCheetahStr = veNamedObjectGetName (taskOrBlock);
  }
  obfuscate(nCheetahStr);
  *this << eSemiColon << eNewline;
  
}

/*****
      Function       : forceDecompile
      
      Purpose        : Decompilation routine for FORCE construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::forceDecompile (veNode nodeForce)
{
  Protector protector(this, nodeForce);

  veNode          forceAssignStmt;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeForce))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eForce;
  
  forceAssignStmt = veForceGetAssignStmt (nodeForce);
  
  stmtDecompile (forceAssignStmt);
  
  *this << eSemiColon << eNewline;
  
}

/*****
      Function       : releaseDecompile
      
      Purpose        : Decompilation routine for RELEASE construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::releaseDecompile (veNode nodeRelease)
{
  Protector protector(this, nodeRelease);

  veNode          releaseExpr;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeRelease))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  *this << eRelease;
  
  releaseExpr = veReleaseGetExpr (nodeRelease);
  
  exprDecompile (releaseExpr);
  
  *this << eSemiColon << eNewline;
  
}

/*****
      Function       : deAssignDecompile
      
      Purpose        : Decompilation routine for DEASSIGN construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/

void 
Decompile::deAssignDecompile (veNode nodeDeAssign)
{
  Protector protector(this, nodeDeAssign);

  veNode          deAssignExpr;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeDeAssign))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eDeassign;
  
  deAssignExpr = veDeAssignGetExpr (nodeDeAssign);
  
  exprDecompile (deAssignExpr);
  
  *this << eSemiColon << eNewline;
}


/*****
      Function       : delayOrEventControlDecompile
      
      Purpose        : Decompilation routine for  procedural
      statements with delay( # )  or
      event control( @ )  constructs.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::delayOrEventControlDecompile (veNode nodeDelOrEventControl)
{
  Protector protector(this, nodeDelOrEventControl);

  veNode          timingControlExpr;
  veNode          delOrEventControlStmt;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeDelOrEventControl))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  
  timingControlExpr = veDelOrEventControlGetTimingControlExpr (nodeDelOrEventControl);
  
  if ((veNodeGetObjType (timingControlExpr) != VE_EVENT_CONTROL) && (veNodeGetObjType (timingControlExpr) != VE_REPEAT_OR_DELAY))
    *this << eDelay;
  
  
  exprDecompile (timingControlExpr);
  
  *this << eSpace;
  
  delOrEventControlStmt = veDelOrEventControlGetStatement (nodeDelOrEventControl);
  
  if (delOrEventControlStmt)
    stmtDecompile (delOrEventControlStmt);
  
  else
    *this << eSemiColon << eNewline;
  
}


/*****
      Function       : repeatDelayControlDecompile
      
      Purpose        : Decompilation routine for constructs like
      
      REPEAT ( expression ) event_control expression
      
      Parameters     : Expr
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::repeatDelayControlDecompile (veNode exprRepeatDelayControl)
{
  Protector protector(this, exprRepeatDelayControl);

  veNode          repeatExpr, eventExpr;
  
  *this << eRepeat << eOpenParen;
  
  repeatExpr = veRepeatDelayControlGetRepeatExpr (exprRepeatDelayControl);
  
  exprDecompile (repeatExpr);
  
  *this << eCloseParen;
  
  eventExpr = veRepeatDelayControlGetEventExpr (exprRepeatDelayControl);
  
  exprDecompile (eventExpr);
  
}

/*****
      Function       : seqBlockDecompile
      
      Purpose        : Decompilation routine for a sequential block.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::seqBlockDecompile (veNode nodeSeqBlock)
{
  Protector protector(this, nodeSeqBlock);

  CheetahList     paramListItr, netListItr, varListItr, eventListItr,
    stmtListItr;
  veNode          param, net, var, event, stmt;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeSeqBlock))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  *this << eBegin;
  
  CheetahStr blockName(veBlockGetName (nodeSeqBlock));
  
  if (blockName)
  {
    *this << eColon;
    obfuscate(blockName);
  }
  *this << eNewline;
  
  paramListItr = veBlockGetParamList (nodeSeqBlock);
  
  if (paramListItr)
  {
    param = veListGetNextNode (paramListItr);
    
    for (; param; param = veListGetNextNode (paramListItr))
      paramDecompile (param);
    
  }
  netListItr = veBlockGetNetList (nodeSeqBlock);
  
  if (netListItr)
  {
    net = veListGetNextNode (netListItr);
    
    for (; net; net = veListGetNextNode (netListItr))
      netDecompile (net);
    
  }
  varListItr = veBlockGetVariableList (nodeSeqBlock);
  
  if (varListItr)
  {
    var = veListGetNextNode (varListItr);
    
    for (; var; var = veListGetNextNode (varListItr))
      varDecompile (var);
    
  }
  eventListItr = veBlockGetEventList (nodeSeqBlock);
  
  if (eventListItr)
  {
    event = veListGetNextNode (eventListItr);
    
    for (; event; event = veListGetNextNode (eventListItr))
      eventDecompile (event);
    
  }
  stmtListItr = veBlockGetStmtList (nodeSeqBlock);
  
  if (stmtListItr)
  {
    stmt = veListGetNextNode (stmtListItr);
    
    for (; stmt; stmt = veListGetNextNode (stmtListItr))
      stmtDecompile (stmt);
    
  }
  *this << eEnd << eNewline;
}

/*****
      Function       : parBlockDecompile
      
      Purpose        : Decompilation routine for a parallel block.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/



void 
Decompile::parBlockDecompile (veNode nodeParBlock)
{
  Protector protector(this, nodeParBlock);

  CheetahList     paramListItr, netListItr, varListItr, eventListItr,
    stmtListItr;
  veNode          param, net, var, event, stmt;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeParBlock))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eFork;
  
  CheetahStr blockName(veBlockGetName (nodeParBlock));
  
  if (blockName)
  {
    *this << eColon;
    obfuscate(blockName);
  }
  *this << eNewline;
  
  paramListItr = veBlockGetParamList (nodeParBlock);
  
  if (paramListItr)
  {
    param = veListGetNextNode (paramListItr);
    
    for (; param; param = veListGetNextNode (paramListItr))
      paramDecompile (param);
    
  }
  netListItr = veBlockGetNetList (nodeParBlock);
  
  if (netListItr)
  {
    net = veListGetNextNode (netListItr);
    
    for (; net; net = veListGetNextNode (netListItr))
      netDecompile (net);
    
  }
  varListItr = veBlockGetVariableList (nodeParBlock);
  
  if (varListItr)
  {
    var = veListGetNextNode (varListItr);
    
    for (; var; var = veListGetNextNode (varListItr))
      varDecompile (var);
    
  }
  eventListItr = veBlockGetEventList (nodeParBlock);
  
  if (eventListItr)
  {
    event = veListGetNextNode (eventListItr);
    
    for (; event; event = veListGetNextNode (eventListItr))
      eventDecompile (event);
    
  }
  stmtListItr = veBlockGetStmtList (nodeParBlock);
  
  if (stmtListItr)
  {
    stmt = veListGetNextNode (stmtListItr);
    
    for (; stmt; stmt = veListGetNextNode (stmtListItr))
      stmtDecompile (stmt);
    
  }
  *this << eJoin << eNewline;
}


/*****
      Function       : taskDecompile
      
      Purpose        : Decompilation routine for a task definition.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::taskDecompile (veNode nodeTask)
{
  Protector protector(this, nodeTask);

  veNode          taskStmt, orderedListItem;
  CheetahList     orderedListIter;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeTask))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  *this << eTask;
  
  obfuscate(CheetahStr(veNamedObjectGetName (nodeTask)));
  
  *this << eSemiColon << eNewline;
  
  orderedListIter = veTfGetOrderedList (nodeTask);
  
  if (orderedListIter)
  {
    orderedListItem = veListGetNextNode (orderedListIter);
    
    for (; orderedListItem; orderedListItem = veListGetNextNode (orderedListIter))
    {
      switch (veNodeGetObjType (orderedListItem))
      {
      case VE_TFARG:
        tfArgDecompile (orderedListItem);
        break;
        
      case VE_PARAM:
        paramDecompile (orderedListItem);
        break;
        
      case VE_VARIABLE:
      case VE_MEMORY:
      case VE_INTEGER:
      case VE_REAL:
      case VE_REG:
      case VE_TIME:
      case VE_EVENT:
        varDecompile (orderedListItem);
        break;
      default :
        break;
      }
    }
    
  }
  taskStmt = veTfGetStmt (nodeTask);
  
  if (taskStmt)
    stmtDecompile (taskStmt);
  
  else
    *this << eSemiColon << eNewline;
  
  *this << eEndTask << eNewline;
}



/*****
      Function       : functionDecompile
      
      Purpose        : Decompilation routine for a function definition.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::functionDecompile (veNode nodeFunc)
{
  Protector protector(this, nodeFunc);

  veNode          returnTypeRange, funcStmt, orderedListItem;
  CheetahList     orderedListIter;
  veBool         isSigned;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeFunc))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  *this << eFunction;
  
  returnTypeRange = veFunctionGetRange (nodeFunc);
  isSigned = veFunctionGetIsSigned (nodeFunc);
  
  switch (veFunctionGetReturnType (nodeFunc))
  {
  case SIGN_USE:
  case UNSET_USE:
    if (isSigned)
      *this << eSigned;
    break;
    
  case INTEGER_USE:
    *this << eInteger;
    break;
  case REAL_USE:
    *this << eReal;
    break;
  case REALTIME_USE:
    *this << eRealTime;
    break;
  default :
    break;
  }
  
  if (returnTypeRange && (!veFunctionGetIsRangeImplicit(nodeFunc)))
    rangeDecompile (returnTypeRange);
  
  obfuscate(CheetahStr(veNamedObjectGetName (nodeFunc)));
  
  *this << eSemiColon << eNewline;
  
  orderedListIter = veTfGetOrderedList (nodeFunc);
  
  if (orderedListIter)
  {
    orderedListItem = veListGetNextNode (orderedListIter);
    
    for (; orderedListItem; orderedListItem = veListGetNextNode (orderedListIter))
    {
      switch (veNodeGetObjType (orderedListItem))
      {
      case VE_TFARG:
        tfArgDecompile (orderedListItem);
        break;
        
      case VE_PARAM:
        paramDecompile (orderedListItem);
        break;
      case VE_VARIABLE:
      case VE_MEMORY:
      case VE_ARRAY:
      case VE_INTEGER:
      case VE_REAL:
      case VE_REG:
      case VE_TIME:
      case VE_EVENT:
        varDecompile (orderedListItem);
        break;
      case VE_REALTIME:
        realtimedecompile (orderedListItem);
        break;
      default :
        break;
      }
    }
  }
  funcStmt = veTfGetStmt (nodeFunc);
  
  stmtDecompile (funcStmt);
  
  *this << eEndFunction << eNewline;
}


/*****
      Function       : taskEnableDecompile
      
      Purpose        : Decompilation routine for a task enable
      statement.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::taskEnableDecompile (veNode nodeTaskEnable)
{
  Protector protector(this, nodeTaskEnable);

  veNode          taskName, expr;
  CheetahList     taskEnableExprList;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeTaskEnable))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  taskName = veTaskEnableGetTask (nodeTaskEnable);
  exprDecompile (taskName);
  
  taskEnableExprList = veTaskEnableGetExprList (nodeTaskEnable);
  
  if (taskEnableExprList)
  {
    *this << eOpenParen;
    
    expr = veListGetNextNode (taskEnableExprList);
    
    exprDecompile (expr);
    
    for (; (expr = veListGetNextNode (taskEnableExprList));)
    {
      *this << eComma;
      exprDecompile (expr);
    }
    *this << eCloseParen;
  }
  *this << eSemiColon << eNewline;
}


/*****
      Function       : sysTaskEnableDecompile
      
      Purpose        : Decompilation routine for a system task enable
      statement.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::sysTaskEnableDecompile (veNode nodeSysTaskEnable)
{
  Protector protector(this, nodeSysTaskEnable);

  CheetahList     sysTaskEnableExprList;
  veNode          expr;
  int         listSize;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeSysTaskEnable))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  writeOutput(CheetahStr(veSysTaskEnableGetName(nodeSysTaskEnable)), 0);
  
  sysTaskEnableExprList = veSysTaskEnableGetExprList (nodeSysTaskEnable);
  
  if (sysTaskEnableExprList)
  {
    *this << eOpenParen;
    
    listSize = veListGetSize( sysTaskEnableExprList );
    
    expr = veListGetNextNode (sysTaskEnableExprList);
    
    exprDecompile (expr);
    
    listSize--;
    
    for( ; listSize; listSize-- )
    {
      *this << eComma;
      
      if( (expr = veListGetNextNode (sysTaskEnableExprList)))
        exprDecompile (expr);
    }
    
    *this << eCloseParen;
  }
  
  *this << eSemiColon << eNewline;
}


/*****
      Function       : sysFunctionCallDecompile
      
      Purpose        : Decompilation routine for a system
      function call expression.
      
      Parameters     : Expr
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::sysFunctionCallDecompile (veNode exprSysFuncCall)
{
  Protector protector(this, exprSysFuncCall);

  CheetahList     sysFuncCallExprList;
  veNode          expr;
  int        listSize;
  
  writeOutput(CheetahStr(veSysFuncCallGetName (exprSysFuncCall)), 0);
  
  sysFuncCallExprList = veSysFuncCallGetExprList (exprSysFuncCall);
  
  if (sysFuncCallExprList)
  {
    *this << eOpenParen;
    
    listSize = veListGetSize( sysFuncCallExprList );
    
    expr = veListGetNextNode (sysFuncCallExprList);
    
    exprDecompile (expr);
    
    listSize--;
    
    for( ; listSize; listSize-- )
    {
      *this << eComma;
      
      if( (expr = veListGetNextNode (sysFuncCallExprList)))
        exprDecompile (expr);
    }
    
    *this << eCloseParen;
  }
}


/*****
      Function       : eventTriggerDecompile
      
      Purpose        : Decompilation routine for an event
      trigger ( ->event_name ) construct.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::eventTriggerDecompile (veNode nodeEventTrigger)
{
  Protector protector(this, nodeEventTrigger);

  veNode          triggerExpr;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeEventTrigger))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eTrigger;
  
  triggerExpr = veTriggerGetEvent (nodeEventTrigger);
  
  exprDecompile (triggerExpr);
  
  *this << eSemiColon << eNewline;
  
}

/*****
      Function       : blockAssignDecompile
      
      Purpose        : Decompilation routine for a blocking
      assignment statement in a procedural
      block.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::blockAssignDecompile (veNode nodeBlockAssign)
{
  Protector protector(this, nodeBlockAssign);

  veNode          lvalue, rhsExpr, timingControlExpr;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeBlockAssign))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  lvalue = veAssignGetLvalue (nodeBlockAssign);
  
  exprDecompile (lvalue);
  
  *this << eEqual;
  
  timingControlExpr = veProcAssignGetTimingControl (nodeBlockAssign);
  
  if (timingControlExpr)
  {
    if ((veNodeGetObjType (timingControlExpr) != VE_EVENT_CONTROL) && (veNodeGetObjType (timingControlExpr) != VE_REPEAT_OR_DELAY))
      *this << eDelay;
    
    exprDecompile (timingControlExpr);
  }
  *this << eSpace;
  
  rhsExpr = veAssignGetRhsExpr (nodeBlockAssign);
  
  exprDecompile (rhsExpr);
  
  *this << eSemiColon << eNewline;
}


/*****
      Function       : nonBlockAssignDecompile
      
      Purpose        : Decompilation routine for a non-blocking
      assignment statement in a procedural
      block.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::nonBlockAssignDecompile (veNode nodeNonBlockAssign)
{
  Protector protector(this, nodeNonBlockAssign);

  veNode          lvalue, rhsExpr, timingControlExpr;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeNonBlockAssign))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  lvalue = veAssignGetLvalue (nodeNonBlockAssign);
  
  exprDecompile (lvalue);
  
  *this << eNonBlockingEqual;
  
  timingControlExpr = veProcAssignGetTimingControl (nodeNonBlockAssign);
  
  if (timingControlExpr)
  {
    if ((veNodeGetObjType (timingControlExpr) != VE_EVENT_CONTROL) && (veNodeGetObjType (timingControlExpr) != VE_REPEAT_OR_DELAY))
      *this << eDelay;
    
    exprDecompile (timingControlExpr);
  }
  *this << eSpace;
  
  rhsExpr = veAssignGetRhsExpr (nodeNonBlockAssign);
  
  exprDecompile (rhsExpr);
  
  *this << eSemiColon << eNewline;
  
}


/*****
      Function       : quasiContAssignDecompile
      
      Purpose        : Decompilation routine for a quasi-continuous
      assignment statement in a procedural
      block.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::quasiContAssignDecompile (veNode nodeQuasiContAssign)
{
  Protector protector(this, nodeQuasiContAssign);

  veNode          lvalue, rhsExpr;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeQuasiContAssign))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eAssign;
  
  lvalue = veQuasiContAssignGetLvalue (nodeQuasiContAssign);
  
  exprDecompile (lvalue);
  
  *this << eEqual;
  
  rhsExpr = veQuasiContAssignGetRhsExpr (nodeQuasiContAssign);
  
  exprDecompile (rhsExpr);
  
  *this << eSemiColon << eNewline;
  
}


/*****
      Function       : eventControlDecompile
      
      Purpose        : Decompilation routine for an
      event control ( @ expression ) expression.
      
      Parameters     : Expr
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::eventControlDecompile (veNode exprEventControl)
{
  Protector protector(this, exprEventControl);

  CheetahList     eventControlExprList;
  veNode          expr;
  
  *this << eAtSign << eOpenParen;
  
  eventControlExprList = veEventControlGetExprList (exprEventControl);
  
  expr = veListGetNextNode (eventControlExprList);
  
  
/*********
 * Addition for 2000 :ssc
 ***********/
  
  if(expr==NULL){
    *this << eAsterisk << eCloseParen << eNewline;
    return ;
  }
  
/*********
 * End Addition :ssc
 ***********/
  
  
  
  exprDecompile (expr);
  
  for (; (expr = veListGetNextNode (eventControlExprList));)
  {
    *this << eEventOr;
    exprDecompile (expr);
  }
  
  *this << eCloseParen << eNewline;
}


/*****
      Function       : posEdgeExprDecompile
      
      Purpose        : Decompilation routine for an
      posedge event control ( @ posedge expression )
      expression.
      
      Parameters     : Expr
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::posEdgeExprDecompile (veNode exprPosEdgeExpr)
{
  Protector protector(this, exprPosEdgeExpr);

  veNode          posEdgeExpr;
  
  *this << ePosedge;
  
  posEdgeExpr = vePosEdgeExprGetEventExpr (exprPosEdgeExpr);
  
  exprDecompile (posEdgeExpr);
  
}


/*****
      Function       : negEdgeExprDecompile
      
      Purpose        : Decompilation routine for an
      negedge event control ( @ negedge expression )
      expression.
      
      Parameters     : Expr
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::negEdgeExprDecompile (veNode exprNegEdgeExpr)
{
  Protector protector(this, exprNegEdgeExpr);

  veNode          negEdgeExpr;
  
  *this << eNegedge;
  
  negEdgeExpr = veNegEdgeExprGetEventExpr (exprNegEdgeExpr);
  
  exprDecompile (negEdgeExpr);
  
}


/*****
      Function       : functionCallDecompile
      
      Purpose        : Decompilation routine for an
      function call expression.
      
      Parameters     : Expr
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::functionCallDecompile (veNode exprFuncCall)
{
  Protector protector(this, exprFuncCall);

  CheetahList     funcCallExprList;
  veNode          expr;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(exprFuncCall))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  expr = veFuncCallGetFunction (exprFuncCall);
  
  exprDecompile (expr);
  
  funcCallExprList = veFuncCallGetExprList (exprFuncCall);
  
  if (funcCallExprList)
  {
    *this << eOpenParen;
    
    expr = veListGetNextNode (funcCallExprList);
    
    exprDecompile (expr);
    
    for (; (expr = veListGetNextNode (funcCallExprList));)
    {
      *this << eComma;
      exprDecompile (expr);
    }
    
    *this << eCloseParen;
  }
}

/*****
      Function       : tfArgDecompile
      
      Purpose        : Decompilation routine for  a task
      or function argument.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::tfArgDecompile (veNode nodeTfArg)
{
  Protector protector(this, nodeTfArg);

  veNode          tfRange;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeTfArg))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  switch (veTfArgGetDirection (nodeTfArg))
  {
  case INPUT_DIR:
    *this << eInput;
    break;
  case OUTPUT_DIR:
    *this << eOutput;
    break;
    
  case INOUT_DIR:
    *this << eInout;
    break;
    
  case UNSET_DIRECTION:
    break;
  }
  
  tfRange = veTfArgGetRange (nodeTfArg);
  
  if (tfRange)
    rangeDecompile (tfRange);
  
  obfuscate(CheetahStr(veNamedObjectGetName (nodeTfArg)));
  
  *this << eSemiColon << eNewline;
}


/*****
      Function       : regDecompile
      
      Purpose        : Decompilation routine for  a task
      or function argument which is of type reg.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::regDecompile (veNode nodeReg)
{
  Protector protector(this, nodeReg);

  veNode          regRange;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(nodeReg))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  if (veRegGetIsDefault (nodeReg) == VE_TRUE)
    return;
  *this << eReg;
  
  regRange = veRegGetRange (nodeReg);
  
  if (regRange)
    rangeDecompile (regRange);
  
  obfuscate(CheetahStr(veNamedObjectGetName (nodeReg)));
  
  *this << eSemiColon << eNewline;
}


/*****
      Function       : stmtDecompile
      
      Purpose        : Generic decompilation routine for statement
      which, depending on the statement type, calls
      the particular statement decompiler routines.
      
      Parameters     : veNode
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::stmtDecompile (veNode nodeStmt)
{
  Protector protector(this, nodeStmt);

  
  switch (veNodeGetObjType (nodeStmt))
  {
  case VE_IF:
  {
    ifDecompile (nodeStmt);
    break;
  }
  
  case VE_FOREVER:
  {
    foreverDecompile (nodeStmt);
    break;
  }
  
  case VE_REPEAT:
  {
    repeatDecompile (nodeStmt);
    break;
  }
  
  case VE_WHILE:
  {
    whileDecompile (nodeStmt);
    break;
  }
  
  case VE_FOR:
  {
    forDecompile (nodeStmt);
    break;
  }
  
  case VE_WAIT:
  {
    waitDecompile (nodeStmt);
    break;
  }
  
  case VE_DELAY_OR_EVENT:
  {
    delayOrEventControlDecompile (nodeStmt);
    break;
  }
  
  case VE_CASEITEM:
  {
    caseItemDecompile (nodeStmt);
    break;
  }
  
  
  case VE_DISABLE:
  {
    disableDecompile (nodeStmt);
    break;
  }
  
  case VE_FORCE:
  {
    forceDecompile (nodeStmt);
    break;
  }
  
  case VE_RELEASE:
  {
    releaseDecompile (nodeStmt);
    break;
  }
  
  case VE_DEASSIGN:
  {
    deAssignDecompile (nodeStmt);
    break;
  }
  
  case VE_QUASI_CONT_ASSIGN:
  {
    quasiContAssignDecompile (nodeStmt);
    break;
  }
  
  case VE_SEQ_BLOCK:
  {
    seqBlockDecompile (nodeStmt);
    break;
  }
  
  case VE_PAR_BLOCK:
  {
    parBlockDecompile (nodeStmt);
    break;
  }
  
  case VE_TASK_ENABLE:
  {
    taskEnableDecompile (nodeStmt);
    break;
  }
  
  case VE_SYS_TASK_ENABLE:
  {
    sysTaskEnableDecompile (nodeStmt);
    break;
  }
  
  case VE_FUNCTION_CALL:
  {
    functionCallDecompile (nodeStmt);
    break;
  }
  
  case VE_SYS_FUNC_CALL:
  {
    sysFunctionCallDecompile (nodeStmt);
    break;
  }
  
  case VE_REPEAT_OR_DELAY:
  {
    repeatOrDelayDecompile (nodeStmt);
    break;
  }
  
  
  case VE_TASK:
  {
    taskDecompile (nodeStmt);
    break;
  }
  
  case VE_NON_BLOCK_ASSIGN:
  {
    nonBlockAssignDecompile (nodeStmt);
    break;
  }
  
  case VE_BLOCK_ASSIGN:
  {
    blockAssignDecompile (nodeStmt);
    break;
  }
  
  case VE_FUNCTION:
  {
    functionDecompile (nodeStmt);
    break;
  }
  
  case VE_TFARG:
  {
    tfArgDecompile (nodeStmt);
    break;
  }
  
  case VE_CASE:
  case VE_CASEZ:
  case VE_CASEX:
  {
    caseDecompile (nodeStmt);
    break;
  }
  
  
  case VE_EVENT_TRIGGER:
  {
    eventTriggerDecompile (nodeStmt);
    break;
  }
  
  case VE_EVENT_CONTROL:
  {
    eventControlDecompile (nodeStmt);
    break;
  }
  case VE_POSE_SCALAR_EVENT_EXPR:
  {
    posEdgeExprDecompile (nodeStmt);
    break;
  }
  
  case VE_NEG_SCALAR_EVENT_EXPR:
  {
    negEdgeExprDecompile (nodeStmt);
    break;
  }
  
  case VE_ASSIGN:
  {
    assignDecompile (nodeStmt);
    break;
  }
  default :
    break;
  }
}



/*****
      Function       : repeatOrDelayDecompile
      
      Purpose        : Decompilation routine for constructs like
      REPEAT ( expression ) event_control expression
      
      
      Parameters     : Expr
      
      Returns        : void
      
      End Header
*****/


void 
Decompile::repeatOrDelayDecompile (veNode exprRepeatOrDelay)
{
  Protector protector(this, exprRepeatOrDelay);

  veNode          repeatExpr, eventExpr;
  
  
  *this << eRepeat << eOpenParen;
  
  repeatExpr = veRepeatDelayControlGetRepeatExpr (exprRepeatOrDelay);
  
  exprDecompile (repeatExpr);
  
  *this << eCloseParen;
  
  eventExpr = veRepeatDelayControlGetEventExpr (exprRepeatOrDelay);
  
  eventControlDecompile (eventExpr);
  
}

void 
Decompile::assignDecompile (veNode assign)
{
  Protector protector(this, assign);

  veNode          lhsExpr, rhsExpr;
  
  lhsExpr = veAssignGetLvalue (assign);
  
  if (lhsExpr)
    exprDecompile (lhsExpr);
  
  *this << eEqual;
  
  rhsExpr = veAssignGetRhsExpr (assign);
  
  if (rhsExpr)
    exprDecompile (rhsExpr);
}
