// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/CheetahContext.h"
#include "compiler_driver/JaguarContext.h"
#include "compiler_driver/MVVContext.h"
#include "compiler_driver/VerificContext.h"
#include "compiler_driver/ARMContext.h" 
#include "compiler_driver/VhdlPortTypeMap.h"
#include "util/HdlFileCollector.h"
#include "codegen/codegen.h"
#include "codegen/Blob.h"
#include "codegen/ScheduleBucket.h"
#include "codegen/CGPortIface.h"
#include "localflow/LocalAnalysis.h"
#include "localflow/BreakResynth.h"
#include "localflow/CompositeResynth.h"
#include "localflow/DowntoResynth.h"
#include "localflow/MemoryResynth.h"
#include "localflow/MemoryBVResynth.h"
#include "localflow/WiredNetResynth.h"
#include "localflow/Elaborate.h"
#include "localflow/ExplicitSize.h"
#include "localflow/NucleusSanity.h"
#include "localflow/TicProtectedNameManager.h"
#include "localflow/ProcessResynth.h"
#include "localflow/STBuildWalk.h"
#include "nucleus/NUBase.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUNetRef.h"
#include "util/CbuildMsgContext.h"    // This is generated.  Include elsewhere?
#include "backend/Backend.h"
#include "schedule/Schedule.h"
#include "reduce/RemoveDeadCode.h"
#include "reduce/DeadNets.h"
#include "reduce/OccurrenceLogger.h"
#include "reduce/SanityUnelab.h"
#include "reduce/SanityElab.h"
#include "reduce/ReachableAliases.h"
#include "reduce/RETransform.h"
#include "reduce/Reduce.h"
#include "reduce/CleanElabFlow.h"
#include "reduce/Split.h"
#include "reduce/CleanTaskResolutions.h"
#include "reduce/CrossHierarchyMerge.h"
#include "reduce/TieNets.h"
#include "flow/FLFactory.h"
#include "util/MemManager.h"
#include "util/Stats.h"
#include "iodb/IODBNucleus.h"
#include "iodb/IODBDesignData.h"
#include "iodb/ScheduleFactory.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "util/AtomicCache.h"
#include "symtab/STSymbolTable.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "reduce/Congruent.h"
#include "reduce/Fold.h"
#include "reduce/MarkSweepUnusedTasks.h"
#include "reduce/RENetWarning.h"
#include "util/UtIOStream.h"
#include "exprsynth/ExprFactory.h"
#include "exprsynth/ESPopulateExpr.h"
#include "util/OSWrapper.h"
#include "util/UtUniquify.h"
#include "globopts/GlobOpts.h"
#include "util/UtShellTok.h"
#include "util/Zstream.h"
#include "util/StringAtom.h"
#include "util/UtIndent.h"
#include "hdl/HdlVerilogPath.h"
#include "compiler_driver/ProtectedSource.h"
#include "bdd/BDD.h"
#include "localflow/TFPromote.h"
#include "localflow/InterraDesignWalker.h"
#include "localflow/NucleusInterraCB.h"
#include "localflow/PrePopInterraCB.h"
#include "localflow/STBuildWalk.h"
#include "localflow/UserTypePopulate.h"
#include "localflow/DumpHierarchy.h"
#include "reduce/Logic2Lut.h"
#include "util/CarbonVersion.h"
#include "AttributeParser.h"
#include "util/UtLibXmlWriter.h" 
#include "util/TimebombHelper.h"

// Verific headers
#include "VeriLibrary.h"

// verific2nucleus headers
#include "verific2nucleus/VerificNucleusBuilder.h"
#include "verific2nucleus/VerificVerilogDesignWalker.h"
#include "verific2nucleus/VerificVhdlDesignWalker.h"
#include "verific2nucleus/VerificDesignWalker.h"
#include "verific2nucleus/VerificSTBuildWalker.h"
#include "verific2nucleus/VerificUserTypePopulate.h"
#include "verific2nucleus/VerificPrePopInfoWalker.h"
#include "verific2nucleus/V2NDesignScope.h"
#include "verific2nucleus/Verific2NucleusUtilities.h"
#include "verific2nucleus/VerificVerilogSTBuildWalker.h"
#include "verific2nucleus/VerificDumpHierarchy.h"
#include "verific2nucleus/VerificTicProtectedNameManager.h"
#include "verific2nucleus/CarbonStaticElaborator.h"
// temp, to be commentted out before commmit
//#include "verific2nucleus/HierarchyDumper.h"
#include <iostream>
#include <fstream>

#include "svinspector/SvinspectorCB.h"

#ifdef CARBON_PV
#include "pv/PVCompile.h"
#include "cfg/CarbonCfg.h"
#endif

#include <ctype.h>              // for isalnum
#ifdef INTERPRETER
#include "interp/Interp.h"
#endif
#include "flow/FlowGraph.h"
#include "util/HierStringName.h"
#include "util/UtMD5.h"

#include <csignal>
#include <sys/fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

// Used for Reading Stack Size in Linux
#if pfLINUX || pfLINUX64
#include <sys/resource.h>
#endif

/*
 * In order to provide realistic and maintainable %-done reports during
 * cbuild, we have a semi-automated process for feeding back phase-time
 * estimates into cbuild, involving running the nightly tests to collect
 * some data, and then a perl script to collate the information into a
 * C array showing the phase-time to %done-map:
 *
 * 1.  mkdir /home/cds/USER/tmpdir
 * 2.  setenv CARBON_CBUILD_ARGS "-gatherStatsDir /home/cds/USER/tmpdir"
 * 3.  runlist -nightly -farm -o
 * 4.  cd /home/cds/USER/tmpdir
 * 5.  find . -name "*_*" > ~/files.txt
 * 6.  cat ~/files.txt | $CARBON_HOME/scripts/collate_stats 
 * 7.  cut & paste the resulting output, replacing the similar section
 *     in CbuildStats.h.  remember to retain the terminating {NULL,0}
 * 8.  rebuild & test
 */
#include "CbuildStats.h"


static const char* const scCbuildReleaseID = CARBON_RELEASE_ID;

extern "C" int veDisableLicenseCheck(long,long,long);
extern const char* sCheetahMemLeaks;

static const char* scIncludeTmps = NULL;
static const char* scDumpMarkConst = NULL;
static const char* scDumpNetFlags = NULL;
static const char* scVerboseHierarchy = NULL;
static const char* scVerboseCongruency = NULL;
static const char* scCongruencyParamCostLimit = NULL;
static const char* scCongruency = NULL;
static const char* scNoLateCongruency = NULL;
static const char* scEarlyCongruencyFactoring = NULL;
static const char* scNoCongruency = NULL;
//static const char* scNoLogic2LUT = NULL;
static const char* scEnableLogic2LUT = NULL;
static const char* scVerboseLogic2LUT = NULL;
static const char* scForceLogic2LUT = NULL;
static const char* scNoDB = NULL;
static const char* scNoFullDB = NULL;
static const char* scDumpCodegen = NULL;
static const char* scDumpDesignState = NULL;
const char* CarbonContext::scDumpVerilog ="-dumpVerilog";
const char* CarbonContext::scDumpVerilogCSE ="-dumpVerilogCSE";
static const char* scDumpSchedule = NULL;
static const char* scDumpScheduleStats = NULL;
static const char* scDumpDesign = NULL;
static const char* scDumpDesignPopulate = NULL;
static const char* scDumpSymtab = NULL;
static const char* scDumpElabFlow = NULL;
static const char* scLastPhase = NULL;
static const char* scVerboseTriInit = NULL;
static const char* scPhaseStats = NULL;
static const char* scModulePhaseStats = NULL;
static const char* scGatherStatsDir = NULL;
static const char* scGatherCongruenceDir = NULL;
static const char* scCostPhase = NULL;
static const char* scNoCosts = NULL;
static const char* scDeepCosts = NULL;
static const char* scCSVCosts = NULL;
static const char* scColumnCosts = NULL;
static const char* scVerboseCleanup = NULL;
static const char* scCreateAMemLeak = NULL;
static const char* scCreateASegfault = NULL;
static const char* scAnalyzerSpew = NULL;
static const char* scNewAllocator = NULL;
static const char* scNewSizing = NULL;
static const char* scMFCU = NULL;
static const char* scSFCU = NULL;
static const char* scSVInspector = NULL;
static const char* scUseVerific = NULL;
static const char* scUseInterra = NULL;
static const char* scNoNewAllocator = NULL;
static const char* scDumpAllocation = NULL;
static const char* scDumpMemory = NULL;
static const char* scDumpXHier = NULL;
static const char* scNoCModelWrapper = NULL;
static const char* scSystemCWrapper = NULL;
static const char* scCustDB = NULL;
static const char* scWrapperSchedAllClkEdges = NULL;
static const char* scSystemCTristates = NULL;
static const char* scSystemCModuleName = NULL;
static const char* scNoOOB = NULL;
static const char* scCheckOOB = NULL;
static const char* scSanitizeCheck = NULL;
static const char* scEstimateCPS = NULL;
static const char* scFredo = NULL; // the slow-witted brother
static const char* scVendorString = NULL;
static const char* scKeyString = NULL;
static const char* scAllowCompileLicense = NULL;
static const char* scAllowRuntimeLicense = NULL;
static const char* scDoADChecksum = NULL;
static const char* scReportADChecksumAsNote = NULL;
static const char* scSkipADChecksumFile = NULL;
static const char* scEF = NULL;
static const char* scAllowNoOpt = NULL;
static const char* scVerboseLicensing = NULL;
static const char* scVSP = NULL;
static const char* scVSPNoCC = NULL; // script enforced -nocc
static const char* scSIGEq = NULL;
static const char* scGenTokenFile = NULL;
static const char* scNoRemoveUnusedTasks = NULL;
static const char* scRecursiveTasks = NULL;
static const char* scCaseEquivFoldThreshold = NULL;
static const char* scPromoteHierTasks = NULL;
static const char* scNoPromoteHierTasks = NULL;
static const char* scPromoteAllTasks = NULL;
static const char* scVerbosePromoteTasks = NULL;
static const char* scBackendCompileOnly = NULL;
static const char* scParseMakeOutputOnly = NULL;
static const char* scCollapseThreshold = NULL;
static const char* scVerboseVectorMetrics = NULL;
static const char* scVerboseCommonConcat = NULL;
static const char* scDisableCommonConcat = NULL;
static const char* scCommonConcatFlags = NULL;
static const char* scEnableCommonConcat = NULL;
static const char* scLogCommonConcat = NULL;
static const char* scDisableConcatNetElimination = NULL;
static const char* scDisableProcConcatElimination = NULL;
static const char* scDisableConcatCollapse = NULL;
static const char* scVerboseUniquify = NULL;
static const char* scProfile = NULL;
static const char* scSimpleProfile = NULL;
static const char* scMemoryBVLimit = NULL;
static const char* scMemoryBVDynIndexLimit = NULL;
static const char* scMemoryBVWidthLimit = NULL;
static const char* scMemoryBVStats = NULL;
static const char* scOldRecord = NULL;
static const char* scVerboseAlerts = NULL;
static const char* scVerbosePreCodeGen = NULL;
static const char* scStrictIODB = NULL;
static const char* scCompileLibOnly = NULL;
static const char* scDisableReplay = NULL;
static const char* scGenerateCleartextSource = NULL;
static const char* scProtectHDLOnly = NULL;
static const char* scVerboseMemAccessResynth = NULL;
static const char* scPlaybackAllCModels = NULL;
static const char* scWriteGuiDB = NULL;
static const char* scTestGuiDB = NULL;
static const char* scFailGuiDBWrite = NULL;
static const char* scLowCG = NULL;
static const char* scNoSynth = NULL;
static const char* scSynth = NULL;

#ifdef INTERPRETER
static const char* scInterp = NULL;
#endif
static const char* scInternalAssertTest = NULL;
static const char* sDisallowHierRefs = NULL;
static const char* sVerboseHierRefAnalysis = NULL;
static const char* scVerboseUserTypePpl = NULL;
static const char* scEnableUserTypePpl = NULL;
static const char* scEnableCompositeTypeDump = NULL;
static const char* scDisableCompositeTypeDump = NULL;
static const char* scVerboseCompositeTypeDump = NULL;
static const char* scDisableLegacyGenerateHierarchyNames = NULL;
static const char* scEnableLegacyGenerateHierarchyNames = NULL;
static const char* scAttributeFile = NULL;
static const char* scWaveformDumpSizeLimit = NULL;
static const char* scLibMap = NULL;
static const char* scL = NULL;
static const char* scLf = NULL;
static const char* scScan = NULL;
static const char* scTimeBomb = NULL;
static const char* scNoValidateIODB = NULL;
static const char* scNoDesignHierarchy = NULL;

// Option groups
const char* CarbonContext::scVerilog = "Verilog Options";
const char* CarbonContext::scVHDL = "VHDL Options";
const char* CarbonContext::scGenCompileControl = "General Compile Control";
const char* CarbonContext::scInputFileControl = "Input File Control";
const char* CarbonContext::scModuleControl = "Module Control";
const char* CarbonContext::scNetControl = "Net Control";
const char* CarbonContext::scOutputControl = "Output Control";

// Globally-seen options
const char* CarbonContext::scVSPCompileFileFlag = "-vspCompileFileFlag";
const char* CarbonContext::scMaxMsgRepeatCount = NULL;
const char* CarbonContext::scTopLevelParam = "-topLevelParam";

// Unprocessed argument group names
const char* CarbonContext::scUnprocVlog = "Verilog";
const char* CarbonContext::scUnprocVHDL = "VHDL";

const char* CarbonContext::scEnableIgnoreZeroReplicationConcatItem = "-enableIgnoreZeroReplicationConcatItem";
const char* CarbonContext::scDisableIgnoreZeroReplicationConcatItem = "-disableIgnoreZeroReplicationConcatItem";
const char* CarbonContext::scEnable2005StylePowerOperator = "-enable2005StylePowerOperator";
const char* CarbonContext::scDisable2005StylePowerOperator = "-disable2005StylePowerOperator";
const char* CarbonContext::scSVerilog = "-sverilog";


// No need to hide this option. But, I want to use a variable.
static const char* scGetTarget = "-getTarget";

static const char scProtectWC[] = "+protect*";

static const char* scEnableReplay = "-enableReplay";
static const char* scEnableOnDemand = "-onDemand";

const char *CarbonContext::scVhdlLoopUnroll = "-vhdlLoopUnroll";
const char *CarbonContext::scVhdlEnableFileIO = "-vhdlEnableFileIO";
const char *CarbonContext::scEnableCSElab = "-enableCSElab";
const char *CarbonContext::scDisableCSElab = "-disableCSElab";


extern const char* gCarbonVersion();

//needed for crypt initialization
static MsgContext* sMsgContext = NULL;
static const char* sCarbonHome = NULL;
static const char* scProtectF = NULL; 
static const char* scUnProtectF = NULL; 

// crypt db
CarbonContext::CarbonCfg* CarbonContext::sCarbonCfg = NULL;

const char* CarbonContext::scLicenseStr = NULL;

const char* CarbonContext::scNetPragmas[] = {
    "fastClock",
    "slowClock",
    "fastReset",
    "asyncReset",
    "clockSpeed",
    "observeSignal",
    "forceSignal",
    "exposeSignal",
    "tieNet",
    "depositSignal",
    "blastNet",
    "ignoreOutput",
    "collapseClock",
    "input",
    "scObserveSignal",
    "scDepositSignal",
    "onDemandIdleDeposit",
    "onDemandExcluded",
    "mvObserveSignal",
    "mvDepositSignal",
    "ignoreSynthCheck",
    NULL
  };
 
const char* CarbonContext::scModulePragmas[] = {
    "flattenModule",
    "flattenModuleContents",
    "allowFlattening",
    "disallowFlattening",
    "ternaryGateOptimization",
    "hideModule",
    "noAsyncResets",
    "disableOutputSysTasks",
    "enableOutputSysTasks",
    NULL
  };

const char* CarbonContext::scFunctionPragmas[] = {
    "inline",
    NULL
  };

// Need to keep a static context here because MVV doesn't let us
// pass client data thru to the message redirector.
static CarbonContext* sCarbonContext = NULL;

// Suffix added to Vhdl subprograms to make them
// unique.
const char* CarbonContext::sUniqSubprogramSuffix = "_$carbon_uniq_";

//----------------------------------------------
//Globals needed to control lex/yacc for Verific
//----------------------------------------------
bool gMakeVhdlIdsLower=false;
bool gMakeVhdlIdsUpper=false;



static void sHandleSig(int sigType)
{
  //UtIO::cout() << "In sig handler " << UtIO::endl;
  if (sCarbonContext)
    sCarbonContext->emergencyCheckinLicenses();
  fflush(stdout);
  OSKillMe(sigType);
}

static void sHandleSigSegv(int sigType)
{
  // Signal Handling doesn't Work Well with Valgrind
  if(!CarbonMem::isValgrindRunning()) {
    // Setup SegFault Signal Handler back to Default
    OSSigaction(SIGSEGV, sHandleSig);
  }

#if pfLINUX || pfLINUX64
  char          dummyStr[16];
  char          currentStackUnit[8];
  int           currentStackSize = 0;
  bool          found            = false;
  FILE*         statfile         = OSFOpen("/proc/self/status", "r", NULL);

  // Read the Current Stack Size
  if(statfile) {
    while(!feof(statfile) && !found) {
      fscanf(statfile, "%s", dummyStr);
      if(!strcmp(dummyStr, "VmStk:")) {
        fscanf(statfile, "%d %s", &currentStackSize, currentStackUnit);
        if(!strcmp(currentStackUnit, "kB"))
          currentStackSize *= 1024;
        found = true;
      }
    }
    fclose(statfile);
  }
#endif

#if pfLINUX || pfLINUX64
  struct rlimit rlim;
  int           stackSizeLimit = 0;

  // Read the Stack Size Limit
  if(getrlimit(RLIMIT_STACK, &rlim) == -1)  
    INFO_ASSERT(0, "Failed to Run getrlimit");
  stackSizeLimit = rlim.rlim_cur;

  if(statfile) {
    // Printing the "INFO_ASSERT(expression)" confuses the users, so use an assert which does
    // not print it.  The real value is in the informational message.

    // Unlimited Stack Size
    INFO_ASSERT_NO_EXPRESSION(stackSizeLimit != -1, "Fatal 164: Segmentation Fault.");
    // Limited Stack Size
    INFO_ASSERT_NO_EXPRESSION(stackSizeLimit  > currentStackSize, "Fatal 165: Stack Overflow; Please increase the stack size.");
    // Segmentation Fault
    INFO_ASSERT_NO_EXPRESSION(stackSizeLimit <= currentStackSize, "Fatal 164: Segmentation Fault.");
  }
#endif

#if pfWINDOWS && !pfLINUX
#endif

  if(sCarbonContext)
    sCarbonContext->emergencyCheckinLicenses();
  fflush(stdout);
  OSKillMe(sigType);
}

//! Class to read in the message database
/*!
  \warning NO static strings in this class can be encrypted. We would
  have a chicken/egg problem
*/
class CarbonContext::CarbonCfg
{
public: CARBONMEM_OVERRIDES
  CarbonCfg() {}

  ~CarbonCfg() 
  {
    mFiles.clearPointerValues();
  }


  void readDB()
  {
    getDBName();
    INFO_ASSERT(not mConfigName.empty(), "Message database name could not be computed.");
    
    ZISTREAM(in, mConfigName.c_str());
    if (!in)
      sMsgContext->CmplrFatalOpenFile(in.getError());
    
    UtString line;
    FileContext* curContext = NULL;
    
    while (in.readline(&line) > 0)
    {
      StringUtil::strip(&line);
      if (not line.empty())
      {
        const char* lineP = line.c_str();
        if (lineP[0] == '`')
        {
          ++lineP;
          curContext = new FileContext(lineP);
          UtString fileStr(lineP);
          simplifyFileName(&fileStr);
          mFiles[fileStr] = curContext;
        }
        else if (curContext)
        {
          // first token is the line number
          UInt32 lineNumber;
          bool isGood = (line >> lineNumber);
          if (! isGood)
            sMsgContext->CmplrConfigCorrupted(mConfigName.c_str(), "invalid syntax");
          
          // Next token is the static string in quotes
          // get rid of any extraneous whitespace
          StringUtil::strip(&line);
          UtString::size_type strSize = line.size();
          INFO_ASSERT(strSize >= 2, "Message database format error.");
          // now remove the quotes
          line.erase(0, 1);
          line.erase(line.end() - 1);
          // And, finally, de-slashify
          StringUtil::deSlashify(&line);
          curContext->addString(lineNumber, line);
        }
      }
    }
  }

  const char* getStr(const char* file, UInt32 lineNumber)
  {
    // Get the file context
    const char* ret = NULL;
    UtString fileStr(file);
    simplifyFileName(&fileStr);
    
    FileHash::iterator p =  mFiles.find(fileStr);
    if (p != mFiles.end())
    {
      FileContext* fc = p->second;
      ret = fc->getString(lineNumber);
    }
    
    if (ret == NULL) {
#if CDB
      UtString buffer;
      buffer << "could not retrieve string: " << file << ": " << lineNumber;
      sMsgContext->CmplrConfigCorrupted(mConfigName.c_str(), buffer.c_str ());
#else
      sMsgContext->CmplrConfigCorrupted(mConfigName.c_str(), "Out of date");
#endif
    }
    return ret;
  }

  class FileContext
  {
  public: CARBONMEM_OVERRIDES
    FileContext(const char* fileName) :
      mName(fileName)
    {}

    ~FileContext() 
    {
    }

    const UtString& getName() const { return mName; }

    void addString(UInt32 lineNumber, const UtString& str)
    {
      mStrings[lineNumber] = str;
    }

    const char* getString(UInt32 lineNumber)
    {
      const char* ret = NULL;
      StrHash::const_iterator p = mStrings.find(lineNumber);
      if (p != mStrings.end())
      {
        const UtString& str = p->second;
        ret = str.c_str();
      }
      return ret;
    }
    
  private:
    UtString mName;
    typedef UtHashMap<UInt32, UtString> StrHash;
    StrHash mStrings;
  };
  
private:

  AtomicCache mAllStrs;  
  typedef UtHashMap<UtString, FileContext*, HashValue<UtString> > FileHash;
  FileHash mFiles;
  UtString mConfigName;
  
  void getDBName()
  {
    INFO_ASSERT(sCarbonHome, "$CARBON_HOME value is not known.");
    INFO_ASSERT(sMsgContext, "Message context not available.");
    // db is located in $CARBON_HOME/lib.  Previously it was in
    // $CARBON_HOME/obj/$arch.debug/ for debug.
    const char* dbName = "carbon_cfg.db";
    
    OSConstructFilePath(&mConfigName, sCarbonHome, "lib");
    OSConstructFilePath(&mConfigName, mConfigName.c_str(), dbName);
  }

  void simplifyFileName(UtString* file)
  {
    UtString path = *file;  // copy original string
    UtString rightmost;
    UtString& simpleName = *file; // result goes here
    simpleName.clear();
    // Loop through path from right until we find "src".
    while(!path.empty() && rightmost != "src") {
      // Put "filename" in rightmost, rest in path.
      OSParseFileName(path.c_str(), &path, &rightmost);
      if (simpleName.empty())
        simpleName = rightmost;
      else if (rightmost != "src")
        // Prepend "rightmost" in front of simpleName.
        OSConstructFilePath(&simpleName, rightmost.c_str(), simpleName.c_str());
    }
    if (rightmost != "src")
      sMsgContext->CmplrConfigCorrupted(mConfigName.c_str(), "Invalid file token");

#if pfMSVC
    // In VC++, __FILE__ is downcased.
    simpleName.lowercase();
#endif
  }
};

class CarbonLog: public MsgStream
{
public: CARBONMEM_OVERRIDES
  CarbonLog(const char* prefix)
  {
    // Open all the log files or die trying.  We need to be able
    // to write logfiles, otherwise we are just not going to
    // be able to write anything else, such as the generated model

    UtString fname;
    fname << prefix << ".errors";
    UtString reason;
    mErrFile = OSFOpen(fname.c_str(), "w", &reason);
    if (mErrFile == NULL)
    {
      fprintf(stderr, "%s\n", reason.c_str());
      exit(1);
    }

    fname.clear();
    fname << prefix << ".warnings";
    reason.clear();
    mWarnFile = OSFOpen(fname.c_str(), "w", &reason);
    if (mWarnFile == NULL)
    {
      fprintf(stderr, "%s\n", reason.c_str());
      exit(1);
    }

    fname.clear();
    fname << prefix << ".suppress";
    reason.clear();
    mSuppressFile = OSFOpen(fname.c_str(), "w", &reason);
    if (mSuppressFile == NULL)
    {
      fprintf(stderr, "%s\n", reason.c_str());
      exit(1);
    }

    // Use suppress for the last severity. The last severity is used to
    // tell in what bucket a continue message should go. That way if
    // there are any continues before no other messages, we throw them
    // away like we used to. I would like to detect this case but the
    // only way I can think of doing it is adding a new severity which
    // has other issues.
    mLastServerity = MsgContextBase::eSuppress;
  } // CarbonLog

  virtual ~CarbonLog()
  {
    if (mErrFile != NULL)
      fclose(mErrFile);
    if (mWarnFile != NULL)
      fclose(mWarnFile);
    if (mSuppressFile != NULL)
      fclose(mSuppressFile);
  }

  virtual void report(MsgContextBase::MsgObject* msg)
  {
    // Figure out what severity to use. If this is a continue we use
    // the last severity. Otherwise we use the msg severity
    MsgContextBase::Severity severity = msg->getSeverity();
    if (severity == MsgContextBase::eContinue) {
      severity = mLastServerity;
    } else {
      mLastServerity = severity;
    }

    // Report the message to the appropriate file
    switch (severity)
    {
    case MsgContextBase::eWarning:
      msg->report(mWarnFile, false);
      break;
    case MsgContextBase::eSuppress:
      msg->report(mSuppressFile, true);
      break;
    case MsgContextBase::eAlert:
    case MsgContextBase::eError:
    case MsgContextBase::eFatal:
      msg->report(mErrFile, true);
      break;
    default:
      // ignore other severities
      break;
    }
  }

  FILE* mErrFile;
  FILE* mWarnFile;
  FILE* mSuppressFile;
  MsgContextBase::Severity mLastServerity;
}; // class CarbonLog: public MsgStream

//! class to maintain data for callbacks used with interra parsers for preparser checks
class CarbonContext::PreParseData
{
public:
  PreParseData(CarbonContext* cc) : mCarbonContext(cc),
                                    mRequiresADChecksumCheck(false),
                                    mReportADCheckFailureAsNote(false)
  {}

  ~PreParseData() {}

  void setRequireADChecksum()
  {
    mRequiresADChecksumCheck = true;
  }

  void setReportADCheckFailureAsNote()
  {
    mReportADCheckFailureAsNote = true;
  }

  static mvvBool sPreParseCB(const char* fname, mvvLanguageType langType, void* client_data)
  {
    PreParseData* me = (PreParseData*) client_data;
    return me->doPreParseCallback(fname, langType);
  }
  
private:
  CarbonContext* mCarbonContext;
  ARMContext mARMContext;  // create object so we can use one of it's static methods
  bool mRequiresADChecksumCheck;
  bool mReportADCheckFailureAsNote;

  // returns true if a modelkit has indicated (via command line) that the AD checksum test can be skipped for fname
  bool canChecksumBeIgnored(const char* fname)
  {
    bool ok_to_ignore = false;
    UInt32 fname_len = strlen(fname);
    ArgProc::StrIter iter;
    if (mCarbonContext->getArgs()->getStrIter(scSkipADChecksumFile, &iter) == ArgProc::eKnown){
      for (; !ok_to_ignore && ! iter.atEnd(); ++iter)
      {
        char * ignored_name = *iter;
        UInt32 ignored_name_len = strlen(ignored_name);
        if ( ( fname_len == ignored_name_len ) && ((strncmp(fname, ignored_name, fname_len) == 0))){
          ok_to_ignore = true;
        }
      }
    }

    return ok_to_ignore;
  }

  // this will be executed before each file is parsed by mvv,cheetah,jaguar
  mvvBool doPreParseCallback(const char* fname, mvvLanguageType /* langType */)
  {
   // you can use the cbuild argument "-showParseMessages" to trace
   // which files are being processed.


    mvvBool checksum_result = MVV_FALSE;  // by default setup to fail, this is safer when this
                                          // callback is used to implement security checks such
                                          //  as -doADChecksum checks
    if ( mRequiresADChecksumCheck ){
      UtString errMsg;

      // do the ambadesigner checksum test.
      bool check_sum_ok = mARMContext.csTest(fname, &errMsg);

      if ( ! check_sum_ok ){
        check_sum_ok = canChecksumBeIgnored(fname);  // did user/modelkit decide that the checksum test is not important?
      }
      if ( ! check_sum_ok ){
        mCarbonContext->reportADChecksumFailure(mReportADCheckFailureAsNote, errMsg, fname);
        check_sum_ok = mReportADCheckFailureAsNote; // if reported as a Note then this function says the test passed
      }
      checksum_result = check_sum_ok ? MVV_TRUE : MVV_FALSE;
    } else {
      checksum_result = MVV_TRUE; 
    }


    // if there are multiple pre-parse tests then those results would be combined here.
    return checksum_result;
  }
};

CarbonContext::CarbonContext(int* argc, char** argv, Stats* stats)
  : mStats(stats),          // Must be first to call sbrk early
    mArgs(*new ArgProc),
    mSourceLocatorFactory(*new SourceLocatorFactory),
    mPostSchedWarnings(NULL),
    mWritingGuiDB (false), mForkingWriteGuiDB (false),  mVerboseGuiDB (false), mWriteGuiDBPID (0),
    mMaster(NULL),
    mCurrentPrePopWalkFlag(false)
{
  mVerificDesignManager.setCarbonContext(this);
  mLicenseCB = createLicenseCB(this, &mLicense);
  INFO_ASSERT(sCarbonContext == NULL, "CarbonContext::CarbonContext: there can be only one CarbonContext");
  sCarbonContext = this;
  mHaveDiagLicense = false;
  mGenEncryptedModel = false;
  mAtomicCache = new AtomicCache;
  mErrStream = new MsgStreamIO(stdout, true);
  mLogStream = NULL;
  mMsgContext = new MsgContext;
  sMsgContext = mMsgContext;
  mMsgContext->addReportStream(mErrStream);
  mJaguarSuppressDepth = 0;
  mPortTypeMap = new VhdlPortTypeMap;
  mTicProtectedNameMgr = new TicProtectedNameManager(&mSourceLocatorFactory);
  mVerificTicProtectedNameMgr = new verific2nucleus::VerificTicProtectedNameManager(&mSourceLocatorFactory);

// used for license feature uniqueness
  mIPFeatures = new AtomicCache; 

  // need this before we do any crypts
  findInstallDir();

  scIncludeTmps = CRYPT("-includeTmps");
  scDumpMarkConst = CRYPT("-dumpMarkConst");
  scDumpNetFlags = CRYPT("-dumpNetFlags");
  scVerboseHierarchy = CRYPT("-verboseHierarchy");
  scVerboseCongruency = CRYPT("-verboseCongruency");
  scCongruencyParamCostLimit = CRYPT("-congruencyParamCostLimit");
  scCongruency = CRYPT("-congruency");
  scNoLateCongruency = CRYPT("-noLateCongruency");
  scEarlyCongruencyFactoring = CRYPT("-earlyCongruencyFactoring");
  scNoCongruency = CRYPT("-noCongruency");
//  scNoLogic2LUT = CRYPT("-noLogic2LUT");
  scEnableLogic2LUT = CRYPT("-enableLogic2LUT");
  scForceLogic2LUT = CRYPT("-forceLogic2LUT");
  scVerboseLogic2LUT = CRYPT("-verboseLogic2LUT");
  scNoDB = CRYPT("-nodb");
  scNoFullDB = CRYPT("-noFullDB");
  scDumpCodegen = CRYPT("-dumpCodeGen");
  scDumpDesignState = CRYPT("-dumpDesignState");
  scDumpSchedule = CRYPT("-dumpSchedule");
  scDumpScheduleStats = CRYPT("-dumpScheduleStats");
  scDumpDesign = CRYPT("-dumpDesign");
  scDumpDesignPopulate = CRYPT("-dumpDesignPopulate");
  scDumpSymtab = CRYPT("-dumpSymTab");
  scDumpElabFlow = CRYPT("-dumpElabFlow");
  scLastPhase = CRYPT("-lastPhase");
  scVerboseTriInit = CRYPT("-verboseTriInit");
  scPhaseStats = CRYPT("-phaseStats");
  scGatherStatsDir = CRYPT("-gatherStatsDir");
  scGatherCongruenceDir = CRYPT("-gatherCongruenceDir");
  scModulePhaseStats = CRYPT("-modulePhaseStats");
  scCostPhase = CRYPT("-costPhase");
  scNoCosts = CRYPT("-nocosts");
  scDeepCosts = CRYPT("-deepCosts");
  scCSVCosts = CRYPT("-csvCosts");
  scColumnCosts = CRYPT("-columnCosts");
  scVerboseCleanup = CRYPT("-verboseCleanup");
  scCreateAMemLeak = CRYPT("-createAMemLeak");
  scCreateASegfault = CRYPT("-createASegfault");
  scAnalyzerSpew = CRYPT("-showParseMessages");
  scNewAllocator = CRYPT("-newAllocator");
  scNewSizing = CRYPT("-newSizing");
  CarbonContext::scEnableIgnoreZeroReplicationConcatItem = CRYPT("-enableIgnoreZeroReplicationConcatItem");
  CarbonContext::scDisableIgnoreZeroReplicationConcatItem = CRYPT("-disableIgnoreZeroReplicationConcatItem");
  CarbonContext::scEnable2005StylePowerOperator = CRYPT("-enable2005StylePowerOperator");
  CarbonContext::scDisable2005StylePowerOperator = CRYPT("-disable2005StylePowerOperator");
  CarbonContext::scSVerilog = CRYPT("-sverilog");
  scMFCU = CRYPT("-mfcu");
  scSFCU = CRYPT("-sfcu");
  scSVInspector = CRYPT("-SVInspector");
  scUseVerific = CRYPT("-useVerific");
  scUseInterra = CRYPT("-useInterra");
  scNoNewAllocator = CRYPT ("-noNewAllocator");
  scDumpAllocation = CRYPT("-dumpAllocation");
  scDumpMemory = CRYPT("-dumpMemory");
  mDumpMemory = false;
  scDumpXHier = CRYPT("-dumpXHier");
  scNoCModelWrapper = CRYPT("-noCModelWrapper");
  scSystemCWrapper = CRYPT("-systemCWrapper");
  scCustDB = CRYPT("-custdb");
  // crypting is not required for public switches.
  scWrapperSchedAllClkEdges = "-wrapperSchedAllClkEdges"; // public
  scSystemCTristates = "-systemCTristates"; // public
  scSystemCModuleName = "-systemCModuleName"; // public

  scNoOOB = CRYPT ("-no-OOB");
  scCheckOOB = CRYPT ("-checkOOB");
  scSanitizeCheck = CRYPT ("-sanitizeCheck");
  scEstimateCPS = CRYPT ("-estimateCPS");
  scFredo = CRYPT ("-fredo");
  scVendorString = "-vendorString";
  scKeyString = CRYPT("-keyString");
  scAllowCompileLicense = CRYPT("-allowCompileLicense");
  scAllowRuntimeLicense = CRYPT("-allowRuntimeLicense");
  scDoADChecksum = CRYPT("-doADChecksum");
  scReportADChecksumAsNote = CRYPT("-reportADChecksumAsNote");
  scSkipADChecksumFile = CRYPT("-skipADChecksumFile");
  scEF = CRYPT("-ef");
  scAllowNoOpt = CRYPT("-allowNoOpt");
  scVerboseLicensing = CRYPT("-verboseLicensing");
  scVSP = CRYPT("-vsp");
  scVSPNoCC = CRYPT("-novspcc");
  scSIGEq = CRYPT("SIG=");
  scGenTokenFile = CRYPT("-genTokenFile");
  scNoRemoveUnusedTasks = CRYPT("-noRemoveUnusedTasks");
  scRecursiveTasks = CRYPT("-recursiveTasks");
  scCaseEquivFoldThreshold = CRYPT("-caseEquivFoldThreshold");
  scPromoteHierTasks = CRYPT("-promoteHierTasks");
  scNoPromoteHierTasks = CRYPT("-noPromoteHierTasks");
  scPromoteAllTasks = CRYPT("-promoteAllTasks");
  scVerbosePromoteTasks = CRYPT("-verbosePromoteTasks");
  scBackendCompileOnly = CRYPT("-backendCompileOnly");
  scParseMakeOutputOnly = CRYPT("-parseMakeOutputOnly");
  scCollapseThreshold = CRYPT("-collapseThreshold");
  scVerboseVectorMetrics = CRYPT("-verboseVectorMetrics");
  scVerboseCommonConcat = CRYPT("-verboseCommonConcat");
  scVerbosePreCodeGen = CRYPT ("-verbosePreCodeGen");
  scDisableCommonConcat = CRYPT("-noCommonConcat");
  scCommonConcatFlags = CRYPT("-commonConcat");
  scEnableCommonConcat = CRYPT("-doCommonConcat");
  scLogCommonConcat = CRYPT("-logCommonConcat");
  scDisableProcConcatElimination = CRYPT("-noProcConcatNetElimination");
  scDisableConcatNetElimination = CRYPT("-noConcatNetElimination");
  scDisableConcatCollapse = CRYPT("-noConcatCollapse");
  scVerboseUniquify = CRYPT("-verboseUniquify");
  scVerboseAlerts = CRYPT("-verboseAlerts");
  scStrictIODB = CRYPT("-strictIODB");
  scProfile = CRYPT("-profile");
  scSimpleProfile = CRYPT("-simpleProfile");
  scMemoryBVLimit = CRYPT("-memoryBVLimit");
  scMemoryBVDynIndexLimit = CRYPT("-memoryBVDynIndexLimit");
  scMemoryBVWidthLimit = CRYPT("-memoryBVWidthLimit");
  scMemoryBVStats = CRYPT("-memoryBVStats");
  scOldRecord = CRYPT("-oldRecord");
  scCompileLibOnly = CRYPT("-compileLibOnly");
  scDisableReplay = CRYPT("-disableReplay");
  scGenerateCleartextSource = CRYPT("-generateCleartextSource");
  scProtectHDLOnly = CRYPT("+protectHDLOnly");
  scVerboseMemAccessResynth = CRYPT("-verboseMemAccessResynth");
  scPlaybackAllCModels = CRYPT("-playbackAllCModels");
  scMaxMsgRepeatCount = CRYPT("-maxMsgRepeatCount");
  scWriteGuiDB = CRYPT("-writeGuiDB");
  scTestGuiDB = CRYPT("-testGuiDB");
  scFailGuiDBWrite = CRYPT("-failGuiDB");
  scLowCG = CRYPT("-lowCG");
  scLicenseStr = CRYPT("license");
  scNoSynth = CRYPT("-noSynth");
  scSynth = CRYPT("-synth");


#ifdef INTERPRETER
  scInterp = CRYPT ("-interp");
#endif

  scInternalAssertTest = CRYPT("-internalAssertTest");
  sDisallowHierRefs = CRYPT("-disallowHierRefs");
  sVerboseHierRefAnalysis = CRYPT("-verboseHierRefAnalysis");
  scVerboseUserTypePpl = CRYPT("-verboseUserTypePopulate");
  scEnableUserTypePpl = CRYPT("-userTypePopulate");
  scEnableCompositeTypeDump = CRYPT("-enableCompositeTypeDump");
  scDisableCompositeTypeDump = CRYPT("-disableCompositeTypeDump");
  scVerboseCompositeTypeDump = CRYPT("-verboseCompositeTypeDump");
  scDisableLegacyGenerateHierarchyNames = CRYPT("-disableLegacyGenerateHierarchyNames");
  scEnableLegacyGenerateHierarchyNames = CRYPT("-enableLegacyGenerateHierarchyNames");
  scAttributeFile = CRYPT("-attributeFile");
  scWaveformDumpSizeLimit = CRYPT("-waveformDumpSizeLimit");
  scLibMap = CRYPT("-libMap");
  scL = CRYPT("-L");
  scLf = CRYPT("-Lf");
  scProtectF = CRYPT("-fprotect");
  scUnProtectF = CRYPT("-funprotect");
  scScan = CRYPT("-scan");
  scTimeBomb = CRYPT("-timeBomb");
  scNoValidateIODB = CRYPT("-noValidateIODB");
  scNoDesignHierarchy = CRYPT("-noDesignHierarchy");

  mCbuildSymTabBOM = new CbuildSymTabBOM;

  mArgc = *argc;
  mArgv = argv;
  mLastPhase = eLastPhase;
  mCostPhase = ePostLocalAnalysis;
  mTristateMode = eTristateModeX;
  mModulePhaseStats = false;
  mProtectSource = false;
  mProtectedSourceCount = 0;

  setupOptions();
  mScheduleFactory = new SCHScheduleFactory;
  mNetRefFactory = new NUNetRefFactory;
  mSymbolTable   = new STSymbolTable(mCbuildSymTabBOM,  mAtomicCache);

  mIODB   = new IODBNucleus(mAtomicCache,
                            mSymbolTable,
                            mMsgContext,
                            &mArgs,
                            &mSourceLocatorFactory,
                            mScheduleFactory);
  mScheduleFactory->putIODB(mIODB);

  mDesign = new NUDesign(mSymbolTable,
                         mIODB,
                         &mSourceLocatorFactory,
                         mScheduleFactory,
                         mMsgContext,
                         &mArgs,
                         mNetRefFactory);
  
  
  LocalAnalysis::addCommandlineArgs(&mArgs);
  GOGlobOpts::addCommandlineArgs(&mArgs);
  RENetWarning::addCommandlineArgs(&mArgs);

  mCheetahContext = new CheetahContext(this, mTicProtectedNameMgr);
  mJaguarContext = new JaguarContext(this);
  mMVVContext = new MVVContext(this);
  mVerificContext = new VerificContext(this);
  //mHaveVhdlFiles = false;
  mFileCollector = new HdlFileCollector;
  mFlatteningTraceFile = NULL;


  mPreParseData = NULL;
  commonInit();
  mSchedule->setupOptions();
  mBackend->setupOptions();
  mCodeGen->setupOptions();
#if CG_ENABLE
  CG::addCommandLineArgs (&mArgs);
#endif
}

CarbonContext::CarbonContext(NUDesign* design, CarbonContext* master,
                             const char* root_suffix)
  : mStats(master->mStats),
    mArgs(master->mArgs),
    mSourceLocatorFactory(master->mSourceLocatorFactory),
    mPostSchedWarnings(NULL),
    mMaster(master)
{
  mFileRoot << master->mFileRoot << root_suffix;
  mCongruencyInfoFilename << mFileRoot << ".congruence";

  // Note that nothing is allocated on behalf of this slave CarbonContext,
  // except for Split, ReachableAliases and a few other odds & ends.
  // so in the destructor the slave CarbonContext does very little.

  mDesign = design;
  INFO_ASSERT(sCarbonContext == master,
              "CarbonContext::CarbonContext: there can be only one CarbonContext");
  mHaveDiagLicense = master->mHaveDiagLicense;
  //mHaveCleartextLicense = master->mHaveCleartextLicense;
  mGenEncryptedModel = false;
  mSymbolTable = design->getSymbolTable();
  mAtomicCache = mSymbolTable->getAtomicCache();
  mErrStream = master->mErrStream;
  mLogStream = master->mLogStream;
  mLicense = master->mLicense;
  mLicenseCB = master->mLicenseCB;

  mMsgContext = design->getMsgContext();
  mJaguarSuppressDepth = 0;
  mPortTypeMap = master->mPortTypeMap;
  mTicProtectedNameMgr = master->mTicProtectedNameMgr;
  mVerificTicProtectedNameMgr = master->mVerificTicProtectedNameMgr;

// used for license feature uniqueness
  mIPFeatures = master->mIPFeatures;

  // need this before we do any crypts
  mInstallDir = master->mInstallDir;

  mCbuildSymTabBOM = master->mCbuildSymTabBOM;

  // here is the only set of stuff we allocate for a slave context

  mArgc = master->mArgc;
  mArgv = master->mArgv;
  mLastPhase = eLastPhase;
  mCostPhase = eLastPhase;
  mTristateMode = eTristateModeX;
  mModulePhaseStats = false;
  mProtectSource = false;
  mProtectedSourceCount = 0;

  mNetRefFactory = design->getNetRefFactory();
  mIODB = mDesign->getIODB();

  mIODB->putTopLevelModuleName(master->mIODB->getTopLevelModuleName());

  mCheetahContext = master->mCheetahContext;
  mJaguarContext = master->mJaguarContext;
  mMVVContext = master->mMVVContext;
  mVerificContext = master->mVerificContext;
  //mHaveVhdlFiles = master->mHaveVhdlFiles;
  mFileCollector = master->mFileCollector;

  if (master->mFlatteningTraceFile != NULL) {
    UtString filename;
    filename << mFileRoot << ".flattening";
    mFlatteningTraceFile = new UtOBStream(filename.c_str());
  }
  else {
    mFlatteningTraceFile = NULL;
  }

  mPreParseData = master->mPreParseData;
  commonInit();

} // CarbonContext::CarbonContext

void CarbonContext::commonInit() {
  mFlowFactory = new FLNodeFactory();
  mFlowElabFactory = new FLNodeElabFactory();
  mExprFactory = new ESFactory();
  mPopulateExpr = new ESPopulateExpr(mExprFactory, mNetRefFactory);

  mSplit = new Split(mSymbolTable,
                     mAtomicCache,
                     &mSourceLocatorFactory,
                     mFlowFactory,
                     mFlowElabFactory,
                     mNetRefFactory,
                     mMsgContext,
                     mIODB,
                     &mArgs);
  mTransform = new RETransform(mSymbolTable,
                               mAtomicCache,
                               &mSourceLocatorFactory,
                               mFlowFactory,
                               mFlowElabFactory,
                               mNetRefFactory,
                               mMsgContext,
                               mIODB,
                               &mArgs);
  mSchedule = new SCHSchedule(mNetRefFactory,
                              mMsgContext,
                              mSymbolTable,
                              mTransform,
                              mSplit,
                              mFlowFactory,
                              mFlowElabFactory,
                              &mArgs,
                              mIODB,
                              mPopulateExpr,
                              &mSourceLocatorFactory);

  mBackend = new Backend(mNetRefFactory,
                         mMsgContext,
                         mSymbolTable,
                         mAtomicCache,
                         mIODB,
                         &mArgs);

  mElaborate = new Elaborate(mSymbolTable,
                             mAtomicCache,
                             &mSourceLocatorFactory,
                             mFlowFactory,
                             mFlowElabFactory,
                             mNetRefFactory,
                             mMsgContext,
                             &mArgs,
                             mIODB);

  mReachableAliases = new ReachableAliases();

  if (gCodeGen != NULL) {
    INFO_ASSERT(mMaster != NULL, "additional CarbonContext has no master");
    INFO_ASSERT(gCodeGen == mMaster->mCodeGen, "codegen/master insanity");
    gCodeGen = NULL;
  }
  mCodeGen = new CodeGen(this,
                         mIODB,
                         mIODB->typeDictionary(),
                         mNetRefFactory,
                         &mSourceLocatorFactory,
                         mAtomicCache,
                         mPopulateExpr,
                         mMsgContext);
  mBlobMap = new BlobMap(gCodeGen);

  setupPhases();
}

void CarbonContext::commonDestruct() {
  NUElabBase::deleteObjects(mSymbolTable);
  delete mSchedule; // Needs to occur before expr factory deletion
  delete mFlowElabFactory;
  delete mFlowFactory;
  delete mExprFactory;
  delete mPopulateExpr;
  delete mElaborate;
  delete mTransform;
  delete mSplit;
  delete mReachableAliases;
  delete mBackend;
  delete mBlobMap;
  delete mCodeGen;
  delete mFlatteningTraceFile;
}

//! destructor
CarbonContext::~CarbonContext()
{
  commonDestruct();

  if (mMaster != NULL) {
    gCodeGen = mMaster->mCodeGen;
    return;                     // all other member vars owned by master
  }
  destroyLicense();

  delete mIPFeatures;
  
  delete mCheetahContext;
  delete mJaguarContext;
  delete mMVVContext;
  delete mVerificContext;
  delete mFileCollector;

  delete mPreParseData;
  delete mPortTypeMap;
  delete mTicProtectedNameMgr;
  delete mVerificTicProtectedNameMgr;

  delete sCarbonCfg;
  sCarbonCfg = NULL;
  
  delete mDesign;
  delete mIODB;
  delete mSymbolTable;
  delete mNetRefFactory;
  delete mScheduleFactory;
  delete mCbuildSymTabBOM;      // must come after deleting mDesign
  delete mAtomicCache;

  mMsgContext->removeReportStream(mErrStream);
  if (mLogStream != NULL)
    mMsgContext->removeReportStream(mLogStream);
  delete mMsgContext;           // This should be last
  delete mErrStream;
  delete mLogStream;

  delete mPostSchedWarnings;
  delete &mSourceLocatorFactory;
  delete &mArgs;
}

void CarbonContext::destroyLicense()
{
  if (mLicense)
  {
    mLicense->releaseAll();
    delete mLicense;
    mLicense = NULL;
    destroyLicenseCB(mLicenseCB);
    mLicenseCB = NULL;
  }
}

void CarbonContext::emergencyCheckinLicenses()
{
  // We are crashing. Check the license back in. But don't free the
  // job to avoid hanging.
  if (mLicense)
  {
    mLicense->emergencyCheckin();
    // let the memory leak. We could hang trying to delete the license.
    mLicense = NULL;
    mLicenseCB = NULL;
  }
}

void CarbonContext::findInstallDir()
{
  const char* carbonHome = getenv("CARBON_HOME");
  if (carbonHome == NULL)
    mMsgContext->CarbonHomeUnset();

  mInstallDir = carbonHome;
  sCarbonHome = mInstallDir.c_str();
  mLicense->putInstallDir(&mInstallDir);
}


bool CarbonContext::applyCustomerTag()
{
  bool retval = true;
  UtCustomerDB* custDB = mIODB->getCustDB();

  UtString customerList;
  const UtCustomerDB::Signature* custSig = custDB->getCustomerSignature();
  if (! custSig)
  {
    retval = false;
    mMsgContext->CmplrCustDbEmptyTag();
  }
  else {
    customerList << *(custSig->getStr());
  }
    
  if (! mArgs.getBoolValue("-q"))
    mMsgContext->CmplrCustDbOutputTag(customerList.c_str());

  return retval;
}


void CarbonContext::doLicenseExit(const char* reason)
{
  mMsgContext->CmplrLicenseTimeout(reason);
}

void CarbonContext::setupOptions()
{
  mArgs.setDescription("Carbon Compiler",
                       NULL, // Do not output a NAME field in usage
                       "Maps Verilog and/or VHDL RTL source into a high-speed linkable "
                       "object library. This object library can be linked into "
                       "a C testbench environment, or linked with a behavioral "
                       "simulator, to aid in software integration and debug");

  mArgs.createSection(CarbonContext::scGenCompileControl);
  mArgs.createSection(CarbonContext::scInputFileControl);
  mArgs.createSection(CarbonContext::scModuleControl);
  mArgs.createSection(CarbonContext::scNetControl);
  mArgs.createSection(CarbonContext::scOutputControl);
  mArgs.createSection(CarbonContext::scVHDL);
  mArgs.createSection(CarbonContext::scVerilog);

  mArgs.createUnprocessedGroup(CarbonContext::scUnprocVlog);
  mArgs.createUnprocessedGroup(CarbonContext::scUnprocVHDL);

  mArgs.addBool(scGetTarget, "Use this option to find out to what backend compiler the Carbon compiler is targeting. If this appears without a -target option on the command it will return the default value of -target; otherwise it will return the target inferred from -target and -o. After the backend compiler name is printed the Carbon compiler exits.", false, ePassMode);
  mArgs.allowUsage(scGetTarget);
  //  mArgs.addToSection(CarbonContext::scGenCompileControl, scGetTarget);

  // This needs to be ePassCarbon because it depends on licensing (diagnostics)
  mArgs.addBool("-h", "Print the documentation for compiler options and exit.", false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scGenCompileControl, "-h");
  mArgs.addSynonym("-h", "-help", false);

  mArgs.addBool(scIncludeTmps, CRYPT("When dumping the symbol table, if there are carbon-generated temporaries then dump those as well as user nets. Normally, only user nets are dumped."), false, ePassCarbon);
  mArgs.addBool(scDumpSchedule, CRYPT("Dump the schedule to stdout"), false, ePassCarbon);
  mArgs.addBool(scDumpScheduleStats, CRYPT("Print some statistics that show us how well scheduling is doing"), false, ePassCarbon);
  mArgs.addString(scLastPhase, CRYPT("Run only until specified phase"), 
                  CRYPT("end"), false, false, ePassCarbon);
  mArgs.addBool(scDumpElabFlow, CRYPT("Dump the elaborated flow to stdout"), false, ePassCarbon);
  mArgs.addBool(scDumpSymtab, CRYPT("Dump the symbol table to stdout"), false, ePassCarbon);
  mArgs.addBool(scDumpDesign, CRYPT("Dump the design to stdout"), false, ePassCarbon);
  mArgs.addBool(scDumpDesignPopulate, CRYPT("Dump the design to stdout after population"), false, ePassCarbon);
  mArgs.addBool(scDumpCodegen, CRYPT("Dump the design to stdout after CodeGen"), false, ePassCarbon); 
  mArgs.addBool(scDumpDesignState, CRYPT("Create a verilog task that will dump the 'state' of a design."), false, ePassCarbon);
  mArgs.addBool(scDumpVerilog, CRYPT("Dump transformed verilog to libdesign_<phase>.v after each compile phase"), false, ePassCarbon);
  mArgs.addBool(scDumpVerilogCSE, CRYPT("Enable normal -dumpVerilog and dump extra verilog to libdesign_<phase>.v during sub-phases of CarbonStaticElaboration"), false, ePassCarbon);
  mArgs.addSideEffect(scDumpVerilogCSE, scDumpVerilog); // automatically turn on -dumpVerilog when -dumpVerilogCSE is specified
  mArgs.addBool(scDumpMarkConst, CRYPT("Dump the symbol table after marking the constants"), false, ePassCarbon); 
  mArgs.addBool(scDumpNetFlags, CRYPT("Dump the net flags after tristate analysis"), false, ePassCarbon); 

  mArgs.addBool(scVerboseHierarchy, CRYPT("Dump the hierarchy after both population and local analysis"), false, ePassCarbon); 

  mArgs.addBool(scBackendCompileOnly, CRYPT("Change to the compile directory and run the backend c++ compile."), false, ePassMode);
  mArgs.allowUsage(scBackendCompileOnly);
  mArgs.addString(scParseMakeOutputOnly, CRYPT("Analyse the output from the back-end compile"), NULL, true, false, ePassMode);
  mArgs.allowUsage(scParseMakeOutputOnly);

  mArgs.addBool(scNoRemoveUnusedTasks, CRYPT("Do not remove unused tasks (tasks that have no task enable referencing them)."), false, ePassCarbon);
  mArgs.addBool(scRecursiveTasks, CRYPT("Allow recursive tasks (experimental)"),
                false, ePassCarbon);
  mArgs.addBool(scPromoteHierTasks, CRYPT("Promote hierarchically referenced tasks that make hierchical calls to other tasks to the root module."), true, ePassCarbon);
  mArgs.addBoolOverride(scNoPromoteHierTasks, scPromoteHierTasks);
  mArgs.addBool(scPromoteAllTasks, CRYPT("Promote all tasks to the root.  This is intended only for aggressively testing the rewriting infrastructure behind -promoteHierTasks, and is expected to reduce performance"), false, ePassCarbon);
  mArgs.addBool(scVerbosePromoteTasks, CRYPT("Dump debug information when promoting tasks"), false, ePassCarbon);

  mArgs.addBool(scVerboseCongruency,
                CRYPT("Dump the congruency map after analysis"),
                false, ePassCarbon); 
  mArgs.addBool(scCongruency,
                CRYPT("Do congruency analysis on modules"),
                true, ePassCarbon); 
  mArgs.addBool(scNoLateCongruency,
                CRYPT("Disable congruency pass at the end of local analysis"),
                false, ePassCarbon); 
  mArgs.addBool(scEarlyCongruencyFactoring,
                CRYPT("Do constant factoring during early congruence pass"),
                false, ePassCarbon); 
  mArgs.addBoolOverride(scNoCongruency, scCongruency);

  mArgs.addBool (scVerbosePreCodeGen,
                 CRYPT ("Dump expression-lowering before codegen"),
                 false, ePassCarbon);

//  mArgs.addBool(scNoLogic2LUT, CRYPT("Do not convert logic into LUTs"),
//                false, ePassCarbon);
  mArgs.addBool(scEnableLogic2LUT, CRYPT("Convert logic into LUTs"),
                false, ePassCarbon);
  mArgs.addBool(scVerboseLogic2LUT,
                CRYPT("Print out info about logic-to-LUT xforms"),
                false, ePassCarbon);
  mArgs.addBool(scForceLogic2LUT,
                CRYPT("Do logic-to-LUT xforms even if costing looks bad (for testing)"),
                false, ePassCarbon);

  mArgs.addInt(scCollapseThreshold,
               CRYPT("Define the threshold for allowing vectorisation of assignment sequences. The number is interpreted as a percentage. 120 means that it the cost of the collapsed statement is not more than 120% of the aggregate cost of the initial statements, then the collapsed statement is used."),
               100, true, false, ePassCarbon);

  mArgs.addBool(scVerboseVectorMetrics, CRYPT("Dump metrics associated with the collapser"), false, ePassCarbon);
  mArgs.addBool(scVerboseCommonConcat, CRYPT("Dump common concat discovery"), false, ePassCarbon);
  mArgs.addBool(scDisableCommonConcat, CRYPT("Disable common concat discovery"), true, ePassCarbon);
  mArgs.addBoolOverride(scEnableCommonConcat, scDisableCommonConcat, CRYPT("Enable common concat discovery (override -noCommonConcat)"));
  mArgs.addBool(scLogCommonConcat, CRYPT("Log common concat discovery"), false, ePassCarbon);
  mArgs.addBool(scDisableConcatNetElimination, CRYPT("Disable common concat scalar net elimination"), false, ePassCarbon);
  mArgs.addBool(scDisableProcConcatElimination, CRYPT("Disable local net elimination when running common concat on procedural assigns"), false, ePassCarbon);
  mArgs.addString(scCommonConcatFlags, CRYPT("Pass options to common concat (experimental use only)"),
                  NULL, true, true, ePassCarbon);

  mArgs.addBool(scDisableConcatCollapse, CRYPT("Disable collapsing assignment into left-hand-side concats"), false, ePassCarbon);

  // Default factor of 4 is based on test/congruent/rom2.v, to make
  // 4*4 constants < 18 instructions for the case
  mArgs.addInt(scCongruencyParamCostLimit,
               CRYPT("Congruency paramaterization cost ratio.  A higher number means we are less likely to declare modules congruent that require factorization.  0 means we will apply congruency independent of how much parameterization is required."),
               4, false, false, ePassCarbon); 

  mArgs.addBool("-version", "Use this option to obtain the version of the Carbon compiler. The compiler will exit after printing the version.", false, ePassMode);
  mArgs.addToSection(CarbonContext::scOutputControl, "-version");
  mArgs.addBool("-verboseVersion", "Print various internal version strings. The compiler will exit after printing the version information.", false, ePassMode);
  mArgs.addToSection(CarbonContext::scOutputControl, "-verboseVersion");
  mArgs.addSynonym("-verboseVersion", "-VV", false);

  mArgs.addBool("-licq",
                "Wait for a license rather than exiting, if none is available",
                false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scGenCompileControl, "-licq");

  // deprecated
  mArgs.addBool(CRYPT("-cdbList"), 
                CRYPT("List all the cdb files including the customer and version of each found in the paths specified by -cdbPath or by CARBON_CDB_PATH, and exit. -cdbPath overrides CARBON_CDB_PATH."), false, ePassCarbon);

  mArgs.putIsDeprecated(CRYPT("-cdbList"), true);

  // test scaffolding for -licq.  Check out a license and then poll for
  // a file to be created.  Then continue.
  mArgs.addOutputFile("-licPollFile",
                      "After checking out license, write a file, then wait for it to be removed",
                      "", true, ArgProc::eRegFile, false, ePassCarbon);

  mArgs.addBool(CRYPT("-verboseResolution"), 
                CRYPT("Be verbose as resolution function generation occurs"), false, ePassCarbon);
  
  mArgs.addBool(scVerboseTriInit, CRYPT("Be verbose as tristate initialization generation occurs"), false, ePassCarbon);

  mArgs.addBool("-stats", "Use this option to print time and memory statistics for the compile to stdout.", false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scOutputControl, "-stats");
  
  mArgs.addBool(scPhaseStats, CRYPT("Print time and memory statistics for each compile phase."), false, ePassCarbon);
  mArgs.addBool(scModulePhaseStats, CRYPT("If -phaseStats is also given, prints out module-level statistics."), false, ePassCarbon);

  mArgs.addBool(scSynth, CRYPT("Compile design with synthesis subset restrictions. For VHDL designs this switch processes the full synthesis subset check. For Verilog designs the synthesis check is limited to the sensitivity list of always blocks."), true, ePassCarbon);
  mArgs.addToSection(CarbonContext::scGenCompileControl, scSynth);
  mArgs.addBoolOverride(scNoSynth, scSynth);
  mArgs.addToSection(CarbonContext::scGenCompileControl, scNoSynth);


  // the following (CarbonContext::scEnableCSElab, CarbonContext::scDisableCSElab) can be removed when this is on by default
  mArgs.addBool(CarbonContext::scEnableCSElab, CRYPT("Enable the use of the Carbon Static Elaboration Phase."), true, ePassCarbon);
  mArgs.addBoolOverride(CarbonContext::scDisableCSElab, CarbonContext::scEnableCSElab);

  mArgs.addInputFile("-directive", "Use this option to include a directives file on the command line. Note that directives specified in files are additive. The syntax of a directives file is line-oriented. Each directive and its value(s) must be specified on its own line. The file argument can be a full path or a relative file name. There is no restriction on file naming; however, it is standard to use the suffixes '.dct' or '.dir' for directives files.", "", true, true, ePassCarbon);
  mArgs.addToSection(CarbonContext::scInputFileControl, "-directive");

  mArgs.addOutputFile(scGatherStatsDir,
                      CRYPT("Gather statistics for each compile phase into directory."),
                      NULL,     // default value
                      true,     // no default
                      ArgProc::eDirExist,
                      false,
                      ePassCarbon
                      );    

  mArgs.addOutputFile(scGatherCongruenceDir,
                      CRYPT("Gather congruence data into directory."),
                      NULL,     // default value
                      true,     // no default
                      ArgProc::eDirExist,
                      false,
                      ePassCarbon
                      );    


  // deprecated
  mArgs.addInputFile(scCustDB, 
                     CRYPT("Specify the customer database file, which controls licensing of generated Carbon Models.  This file must be supplied by Carbon, but its installation location may be specified with this switch.  If left blank, the file is found in $CARBON_HOME/lib/customer.cdb. This option has been deprecated and should not be used. Use the clmtoolsp executable, instead."),
                     "", true, false, ePassCarbon);
  mArgs.putIsDeprecated(scCustDB, true);

  // deprecated
  mArgs.addString(CRYPT("-cdbPath"), 
                  CRYPT("Specify a list of directories to search for the customer cdb file. Multiple paths are separated by ':', e.g., /tools/carbon/cdb:/home/cdb. No more than one cdb file can exist in the paths specified. If a single file is specified, it is treated as if -custdb was used to specify it and if any other cdb files exist in that directory they are ignored. This switch overrides the CARBON_CDB_PATH environment variable, which is can also be used to specify a list of directories where the cdb file could exist."),
                  "", true, false, ePassCarbon);
  mArgs.putIsDeprecated(CRYPT("-cdbPath"), true);

  mArgs.addInputArgFile("-f", "Use this option to specify a file name from which the Carbon compiler will read command-line options. Note that options specified in files are additive. The syntax of the file is simply a space-separated list of options and arguments; each option does not need to be specified on a new line. The file argument can be a full path or a relative file name. There is no restriction on file naming. Shell comments, specified with #, and C++-style comments, // and /*...*/, are allowed.");
  mArgs.addToSection(CarbonContext::scInputFileControl, "-f");
  mArgs.addSynonym("-f", "-F", false);
  
  // This is not a user-visible option,
  // IT SHOULD NOT be defined with addInputArgFile because that definition has side effects like automatic
  // substitution of the contents of the file for the '-f filename' when it is found in a command line.  We don't want automatic expansion.
  // ef stands for encrypted file
  mArgs.addString(scEF, CRYPT("Use this option to specify an encrypted file from which the Carbon compiler will read command-line options. Note that only the -q option is allowed on command line when the -ef option is used."),NULL,true,false,ePassCarbon); // actually this is processed before ePassMode
  mArgs.allowUsage(scEF);

  // This is not a user-visible option
  mArgs.addInputFile(scProtectF, CRYPT("Use this option to encrypt a file."), "", false, true, ePassCarbon);
  mArgs.addInputFile(scUnProtectF, CRYPT("Use this option to decrypt a file."), "", false, true, ePassCarbon);

  mArgs.addInputFile(scAttributeFile, "Specify a file name from which the Carbon compiler will read model attributes.  These attributes are used by the generated model at runtime.  In general, the attribute file is generated by Model Studio and should not be edited by the end user.", "", true, true, ePassCarbon);
  mArgs.addToSection(CarbonContext::scInputFileControl, scAttributeFile);

  mArgs.addBool("-g", "Use this option to disable optimizations that reduce debug visibility to design nets.  Note that -g allows optimizations to run that don't compromise debug visibility.  Also note that not all nets in the design are preserved by -g.  Dead nets (nets that do not reach primary outputs or observed signals) are not livened by -g.  Dead nets can be livened by the observeSignal directive, which allows wildcards.", false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scNetControl, "-g");

  mArgs.addBool(scNoDB, CRYPT("Suppress debug database generation"), false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scOutputControl, scNoDB);
  // -noFullDB is a more accurate name than -nodb
  mArgs.addSynonym(scNoDB, scNoFullDB, false);

  mArgs.addString("-tristate","Use this option to set how internal tristate data values will appear in waveforms and net examinations. Valid modes: 1, 0, x, z (case-insensitive)", "x", false, false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scModuleControl, "-tristate");

#ifdef CARBON_PV
  mArgs.addBool(CRYPT("-pv"),
                CRYPT("Experimental programmer's View"), false,
                ePassCarbon);

  mArgs.addInputFile("-psd", CRYPT("for Experimental Programmer's View: read a ccfg file that contains xmltransaction specifications. There is no restriction on file naming; however, it is standard to use the suffix '.ccfg'."), "", true, true, ePassCarbon);
  mArgs.addToSection(CarbonContext::scInputFileControl, "-psd");
#endif

  mArgs.addBool("-q", "Use this option to enable quiet mode and suppress banner output.", false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scOutputControl, "-q");
  mArgs.addBool("-w", "Specify this option to suppress all warnings generated by the Carbon compiler.", false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scOutputControl, "-w");

  mArgs.addString(scCostPhase, CRYPT("Phase at which to print costs"), 
                  CRYPT("analysis"), false, false, ePassCarbon);

  mArgs.addBool(scNoCosts, CRYPT("Suppress printing of cost information"), 
                false, ePassCarbon);
  mArgs.addBool(scDeepCosts, CRYPT("Measure costs hierarchically"), false, ePassCarbon);
  mArgs.addBool(scCSVCosts, CRYPT("Separate columns with commas for loading into a spreadsheet"), true, ePassCarbon);
  mArgs.addBoolOverride(scColumnCosts, scCSVCosts);

  mArgs.addBool(scCreateAMemLeak, CRYPT("Create a memory leak"), false, ePassCarbon);
  mArgs.addBool(scCreateASegfault, CRYPT("Create a segfault"), false, ePassCarbon);
  mArgs.addBool(scAnalyzerSpew,
                CRYPT("Print informational messages during HDL design file parsing"), false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scInputFileControl, scAnalyzerSpew);

  mArgs.addBool(scNewAllocator,
                CRYPT("Use new symbol-table driven allocator, rather than the old flow-based allocator"), true, ePassCarbon);
  mArgs.addBoolOverride(scNoNewAllocator, scNewAllocator);

  mArgs.addBool(scNewSizing,
                CRYPT("Use new sizing methdology, where concatenations and part-selects are applied to expressions to make their natural size == their context size"), false, ePassCarbon);
  mArgs.putIsDeprecated(scNewSizing, true);

  mArgs.addBool(CarbonContext::scDisableIgnoreZeroReplicationConcatItem,
                CRYPT("Disable support for the creation of null expressions when a concat replication has the value if zero, instead use 1'b0 in its place.  This is the default for Verilog 2001."), true, ePassCarbon);
  mArgs.addBoolOverride(CarbonContext::scEnableIgnoreZeroReplicationConcatItem, CarbonContext::scDisableIgnoreZeroReplicationConcatItem, CRYPT("Enable the feature of Verilog 2005 (and later) where concat items with a replication constant of zero are ignored. This is the default for SystemVerilog, see also -disableIgnoreZeroReplicationConcatItem."));

  mArgs.addBool(CarbonContext::scDisable2005StylePowerOperator,
                CRYPT("Disable the support for the 2005 style Verilog power operator and instead use the Verilog 95 and 2001 style.  Thus the result of the power operator is of type Real if either operand is real, signed, or an integer. This is the default for Verilog 2001."), true, ePassCarbon);
  mArgs.addBoolOverride(CarbonContext::scEnable2005StylePowerOperator, CarbonContext::scDisable2005StylePowerOperator, CRYPT("Enable the support for the 2005/SystemVerilog style Verilog power operator. Thus the result of the power operator is of type Real only if one (or both) of the operands are Real. This is the default if -sverilog is specified. See also -disable2005StylePowerOperator."));

  mArgs.addBool(CarbonContext::scSVerilog,
                CRYPT("This option enables SystemVerilog compilation mode.  All Verilog files encountered during compilation will be treated as SystemVerilog source files"), false, ePassCarbon);
  mArgs.addSideEffect(CarbonContext::scSVerilog, scUseVerific); // automatically turn on -useVerific when -sverilog is specified

  mArgs.addInt(scSVInspector,
               CRYPT("Use the Verific parser to gather and report statistics about SV language usage, value is the limit of unique items reported"),
               0, true, false, ePassCarbon);

  mArgs.addBool(scMFCU,
                CRYPT("This option enables 'multi-file compilation unit' mode when -sverilog is specified.  All source files will be compiled as one compilation unit. See also -sfcu"), true, ePassCarbon);
  mArgs.addBoolOverride(scSFCU, scMFCU); // single-file compilation unit mode

// uncomment the following line to make the verific flow the default  
// #define USEVERIFICISDEFAULT
#ifdef USEVERIFICISDEFAULT
  // we must make scUseVerific the main switch because addBoolOverride only works if it sets the value of the switch to false
  mArgs.addBool(scUseVerific,
                CRYPT("Use the Verific based parser"), true, ePassCarbon);
  mArgs.addBoolOverride(scUseInterra, scUseVerific);
#else
  // we must make scUseInterra the main switch because addBoolOverride only works if it sets the value of the switch to false
  mArgs.addBool(scUseInterra,
                CRYPT("Use the Interra based parser"), true, ePassCarbon);
  mArgs.addBoolOverride(scUseVerific, scUseInterra);
#endif

  // mArgs.addToSection(CarbonContext::scInputFileControl, scUseVerific); // when useVerific becomes supported this line needs to be uncommented out (see also another line with 'when useVerific becomes')

  mArgs.addBool(scDumpAllocation,
                CRYPT("Dump the allocation performed by the new allocator"),
                false, ePassCarbon);
  mArgs.addOutputFile(scDumpMemory,
                      CRYPT("Dump memory histogram after every phase to file lib<design>.mem.phase"),
                      NULL,     // Default value
                      true,    // No default
                      ArgProc::eRegFile,
                      false, ePassCarbon);      
  
  mArgs.addBool(scDumpXHier,
                CRYPT("Dump out cross-hierarchy merge opportunities"),
                false, ePassCarbon);

  mArgs.addBool(scStrictIODB, "Promote IODB write warnings or errors to fatal errors", false, ePassCarbon);
  mArgs.addBool (scNoCModelWrapper, CRYPT("Disable generation of C-Model wrapper.  Use this option to prevent generation of code and directives that make the design callable as a C-Model from other Carbon models."), false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scOutputControl, scNoCModelWrapper);
  mArgs.addBool (scSystemCWrapper, CRYPT("Generate wrapper to allow instantiation of design by a SystemC simulator.  Replaced with command:\"carbon systemCWrapper\""), false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scOutputControl, scSystemCWrapper);
  mArgs.putIsDeprecated(scSystemCWrapper, true);
  mArgs.addBool (scWrapperSchedAllClkEdges, "Wrapper generation is normally optimized for speed where changes in clocks and edge triggered asynchronous signals that have only 1 relevant edge cause scheduling to be called only on the relevant edges. For example, if an entire design is clocked on the positive edge of clkA, then the schedule function is only called when clkA rises. This switch changes the default behavior to call schedule on all clock edges regardless of edge relevance. This is useful if a callback is needed on a clock change.", false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scOutputControl, scWrapperSchedAllClkEdges);
  mArgs.putIsDeprecated(scWrapperSchedAllClkEdges, true);
  mArgs.addBool(scSystemCTristates, "By default, the SystemC wrapper will only transmit 2-state values from the Carbon Model to the SystemC instantiation. For designs that have true tristates and the testbench needs to know whether or not a port is driving this switch will turn on tristate synthesis in the wrapper generation. This will require the user to use sc_logic (for 1 bit values) and sc_lv for > 1 bit values as types for tristates. Non-tristates, ie. 2-state nets, will still use the primitive types. One exception to this is bidirect nets. Because bidirect nets require resolution, they cannot be strongly driven to 0 or 1 all the time. Therefore, bidirects are always 3 state.", false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scOutputControl, scSystemCTristates);
  mArgs.putIsDeprecated(scSystemCTristates, true);
  mArgs.addString(scSystemCModuleName, "Specify the name to use for the SystemC module when generating the SystemC wrapper.  Defaults to the name of the top-level Verilog/VHDL module.", "", true, false, ePassCarbon);
  mArgs.putIsDeprecated(scSystemCModuleName, true);
  mArgs.addToSection(CarbonContext::scOutputControl, scSystemCModuleName);
  mArgs.addBool(scProfile, CRYPT("Enable block profiling.  Compiling creates lib<design>.prof, simulating creates carbon_profile.dat.  Run \"carbon profile\" to see results."), false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scOutputControl, scProfile);
  // Simple profiling has been removed, but we want to give users a nice error message if they use it.
  mArgs.addBool(scSimpleProfile, CRYPT("Enable sample-based profiling.  Replaced by -profile."), false, ePassCarbon);
  mArgs.putIsDeprecated(scSimpleProfile, true);

  mArgs.addBool (scNoOOB,
                 "Use this option to ignore out-of-bounds bit references, i.e., the Carbon compiler will not check for such references. As a result, the compile time will be faster, the generated code will be smaller and the resulting design should run faster. If you specify this option, the model will exhibit unpredictable behavior during run time if there are out-of-bounds bit or memory references.",
                 false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scNetControl, scNoOOB);

  mArgs.addBool (scCheckOOB,
                 "Use this option to report out-of-bounds bit and memory accesses at runtime.  The Carbon compiler will emit runtime checking code.",
                 false, ePassCarbon);
  mArgs.addToSection (CarbonContext::scNetControl, scCheckOOB);

  mArgs.addBool (scSanitizeCheck,
                 "Use this option to check for dirty writes. After every write to a net, it will check for non-zero bits outside the declared size of the net.  This helps to detect out-of-bound reads/writes permitted by the -no-OOB flag.",
                 false, ePassCarbon
    );

  mArgs.addToSection (CarbonContext::scNetControl, scSanitizeCheck);

  mArgs.addBool(scProtectHDLOnly,
                "This option modifies +protect so that the resulting files don't prevent use of -generateCleartextSource.",
                false, ePassCarbon);
  // For now, we don't advertise this switch.
  //  mArgs.addToSection(CarbonContext::scVerilog, scProtectHDLOnly);

  mArgs.addUnprocessedBoolWildcard(CarbonContext::scUnprocVlog, scProtectWC, "+protect[.ext]", "Use this option to generate protected source versions of all given Verilog input files.  New files are generated for each input, with all source between `protect and `endprotect compiler directives in encoded form.  By default, the output file is named with a .vp extension.", ePassMode);
  mArgs.addToSection(CarbonContext::scVerilog, scProtectWC);

  // Add option for customers similar to -noEncrypt.
  mArgs.addBool(scGenerateCleartextSource,
                "Generate code that is not obfuscated and not encrypted.",
                false, ePassCarbon);
  // For now, we don't advertise this switch (or +protectHDLOnly).
  //  mArgs.addToSection(CarbonContext::scOutputControl, scGenerateCleartextSource);

  mArgs.addBool(scEstimateCPS, CRYPT("When dumping costs, add the cps estimate to the end of the file."), false, ePassCarbon);

#ifdef INTERPRETER
  mArgs.addBool (scInterp,
                 "Generate an interpreted model.",
                 false, ePassCarbon);
#endif
  mArgs.addInt(scInternalAssertTest, CRYPT("Tests various asserts on elaborated flow nodes"), 0, true, false, ePassCarbon);

  mArgs.addInt("-genMultiDUT", "Generate a new top-level DUT that instantiates the design's top-level module N times, to assist making models bigger for benchamrking", 0, true, false, ePassCarbon);

  mArgs.addInt(scCaseEquivFoldThreshold, CRYPT("do case equivlence fold optimization if case item number is less than the threshold"), 50000, false, false, ePassCarbon);
  mArgs.addBool (scFredo,
                 "Generate a slow model.",
                 false, ePassCarbon);
  mArgs.addBool (scAllowNoOpt, "Allows cbuild to compile unoptimized in the absence of a optimization license", false, ePassCarbon);

  mArgs.addString (scVendorString,
                   "If a license file has more than one vendor string associated with a particular Carbon compiler license (a Carbon compiler, for example) then one can be chosen with this option.",
                   "", true, false, ePassCarbon);
  mArgs.allowUsage(scVendorString);
  mArgs.putIsDeprecated(scVendorString, true);

  mArgs.addString (scKeyString, "Comma-separated ordered list of license variants to attempt to check out at compilation and runtime instead of the default compilation or runtime license.  Each entry will be appended to the default runtime license feature name (even during compilation), with DEFAULT indicating that the default feature should be used.",  "", true, false, ePassCarbon);
  mArgs.setIsAllowedOnlyInEFFile(scKeyString);

  mArgs.addString (scAllowCompileLicense, "Comma-separated ordered list of license variants to attempt to check out at compilation instead of the default license.  Each entry will be appended to the default compile license feature name, with DEFAULT indicating that the default feature should be used.",  "", true, false, ePassCarbon);
  mArgs.addString (scAllowRuntimeLicense, "Comma-separated ordered list of license variants to attempt to check out at runtime instead of the default license.  Each entry will be appended to the default runtime license feature name, with DEFAULT indicating that the default feature should be used.",  "", true, false, ePassCarbon);

  mArgs.addBool (scDoADChecksum, "Requires that AMBA Designer generated checksum files exist for all parsed design files", false, ePassCarbon);
  mArgs.setIsAllowedOnlyInEFFile(scDoADChecksum);

  mArgs.addBool (scReportADChecksumAsNote, CRYPT("Any failures of the check for AMBA Designer generated checksum files will be reported as a note (instead of as an error (the default))."), false, ePassCarbon);
  mArgs.setIsAllowedOnlyInEFFile(scReportADChecksumAsNote);

  mArgs.addString(scSkipADChecksumFile,
                  CRYPT("Identify a file that does not require the ADChecksum check."),
                  NULL, // Default value
                  true, // No default
                  true, // Allow multiple
                  ePassCarbon // Pass
                  );
  mArgs.setIsAllowedOnlyInEFFile(scSkipADChecksumFile);

  mArgs.addString(scGenTokenFile,
                  "Write a 'tokens' directory based on the preprocessed Verilog input files.  The directory will contain tokens.v -- all the Verilog needed for the design, cbuild.cmd -- the command file used to re-compile from the tokens file, tokens.dir, which has all the directives used in the original compile.",
                  NULL,     // default value
                  true,    // no default
                  false, ePassCarbon);

  mArgs.addBool (scVerboseLicensing, CRYPT("This will print out exactly what license has been checked out, if any. If the compile is a single hdl compile and both the mixed and the single hdl license exist and the single hdl license fails to checkout the error will be printed. The mixed license then will be attempted and if retrieved a note saying that the mixed license is being used will occur."), false, ePassCarbon);

  mArgs.addBool(scVerboseAlerts, CRYPT("output internal state dumps for some alerts"), false, ePassCarbon);
  mArgs.addBool (scVSP, CRYPT("This turns on VSP licensing. SPEEDCompiler licensing is off by default."), false, ePassMode);
  mArgs.allowUsage(scVSP);

  mArgs.addBool (scVSPNoCC, CRYPT("This is used by the vspcompiler script to control compilation flow. This option is just like -nocc except that this option will never be written to a cmd file by cbuild."), false, ePassCarbon);
  
  // Option to touch a file when done with the frontend compile.
  mArgs.addOutputFile(scVSPCompileFileFlag,
                      CRYPT("When finished with the frontend compile create a file with the given name. This alerts the vspcompiler script that it can do the backend compile phase. This option will never be written to a cmd file by cbuild."),
                      "", true, ArgProc::eRegFile, false, ePassCarbon);

  mArgs.addBool (scVerboseUniquify, CRYPT("Print naming information when uniquifying module names that are not immediately uniquified by parameters. For example, Module A is instantiated more than once and it instantiates another module, Module B, that is elaboratedly configured with a defparam. In the instantation of Module A where Module B is defparamed, Module B is a different module requiring an uniquification of Module A."), false, ePassCarbon);

  mArgs.addInt(scMemoryBVLimit, CRYPT("rewrite non-dynamically-indexed memories to bitvectors if the total bit size of the memory is less than or equal to the threshold.  A value of 0 will turn off memory to bitvector conversion."), 512, false, false, ePassCarbon);
  mArgs.addInt(scMemoryBVDynIndexLimit, CRYPT("rewrite dynamically-indexed memories to bitvectors if the bit size of the memory is less than or equal to the threshold"), 64, false, false, ePassCarbon);
  mArgs.addInt(scMemoryBVWidthLimit, CRYPT("rewrite dynamically-indexed memories to bitvectors if the width of the memory is less than or equal to the threshold"), 32, false, false, ePassCarbon);
  mArgs.addBool(scMemoryBVStats, CRYPT("dump memory to bitvector conversion stats"), false, ePassCarbon);
  mArgs.addBool(scOldRecord, CRYPT("Use the old record population implementation.  Use this if the new record population implementation doesn't work for you. Also file a defect."), false, ePassCarbon);
  mArgs.putIsDeprecated(scOldRecord, true);

  mArgs.addBool(scCompileLibOnly, CRYPT("Compile hdl into specified libraries. Carbon model will not be generated."), false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scGenCompileControl, scCompileLibOnly);

  // This is a way to make replay impossible in the runtime, if
  // needed. Keeping as an unpublished option for now.
  /*
  mArgs.addBool(scDisableReplay, CRYPT("Create a Carbon Model that does not support Carbon replay. By default, a replayable Carbon Model is created. This option turns this ability off."), false, ePassCarbon);
  */
  // When replay is on by default, delete the addBool below and
  // uncomment the addBool above.
  mArgs.addBool(scDisableReplay, CRYPT("Internal use only. Replay will be on by default at some point. We will need a way to disable it in some cases. Tests were written with this in mind. So, this option is here for testing."), false, ePassCarbon);
  mArgs.putIsDeprecated(scDisableReplay, true);

  // When replay is on by default, change the comment for
  // -enableReplay and default it to true
  mArgs.addBool(scEnableReplay, "Create a Carbon Model that supports Carbon replay. By default, an unreplayable Carbon Model is created. This option turns the replay feature on.", false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scOutputControl, scEnableReplay);
  mArgs.putIsDeprecated(scEnableReplay, true);

  mArgs.addBool(scEnableOnDemand, "Create a Carbon Model that supports Carbon OnDemand. By default, Carbon Model which does not support OnDemand is created. This option turns the OnDemand feature on.", false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scOutputControl, scEnableOnDemand);
  mArgs.putIsDeprecated(scEnableOnDemand, true);

  mArgs.addBool(scVerboseMemAccessResynth, CRYPT("Print verbose messages during the multi-word to single-word conversion for memory access."), false, ePassCarbon);

  mArgs.addBool(scPlaybackAllCModels, CRYPT("Mark all CModels with cCallOnPlayback. By default, only CModels that have the cCallOnPlayback directive will be called by the replay playback mechanism. This makes all CModels callable by the replay playback mechanism."), false, ePassCarbon);
  mArgs.addInt(scMaxMsgRepeatCount, CRYPT("Specify the number of times the message with same message number and text can be issued."), 10, false, false, ePassCarbon);
  mArgs.addBool(scWriteGuiDB, CRYPT("Write the GUI symbol database and exit"), false, ePassCarbon);
  mArgs.addBool(scTestGuiDB, CRYPT("Test writing the GUI symbol database and exit"), false, ePassCarbon);
  mArgs.addBool(scFailGuiDBWrite, CRYPT("Force GUI database write failure to test the test infrastructure"), false, ePassCarbon);
  mArgs.addBool(sDisallowHierRefs, "Turn off hierarchical reference support", false, ePassCarbon);
  mArgs.addBool(sVerboseHierRefAnalysis, "Be verbose as hierarchical reference analysis occurs", false, ePassCarbon);


  mArgs.addBool(scVerboseCompositeTypeDump, CRYPT("Dumps composite types to the screen"), false, ePassCarbon);
  mArgs.addSynonym (scVerboseCompositeTypeDump, scVerboseUserTypePpl, false);

  mArgs.addBool(scDisableLegacyGenerateHierarchyNames, CRYPT("Disable use of legacy style names for named generate blocks"), true, ePassCarbon);
  mArgs.addBoolOverride(scEnableLegacyGenerateHierarchyNames, scDisableLegacyGenerateHierarchyNames, CRYPT("Enable use of legacy style names for named generate blocks"));

  mArgs.addBool(scEnableCompositeTypeDump, CRYPT("Enable composite type dumping to the database"), true, ePassCarbon);
  mArgs.addBoolOverride(scDisableCompositeTypeDump, scEnableCompositeTypeDump);
  mArgs.addSynonym (scEnableCompositeTypeDump, scEnableUserTypePpl, false);

  mArgs.addInt(scWaveformDumpSizeLimit, CRYPT("Specify the maximum size (in bits) of design elements that should be dumped to waveform files.  This is an alternative to the carbonDumpSizeLimit() API function, although the API function has precedence."), 0, true, true, ePassCarbon);
  mArgs.addToSection(CarbonContext::scNetControl, scWaveformDumpSizeLimit);

  mArgs.addBool(scLowCG, CRYPT("Run the experimental low-level code generator"), false, ePassCarbon);
  mArgs.addOutputFile(scLibMap, CRYPT("Specifies a library mapping file that allows the compiler to find the physical location of any logical library that has been precompiled. The format of the file should be <logical library name> => <physical library location>, one library per line. The last entry in the file will be the WORK library."), NULL, true, ArgProc::eRegFile, false, ePassCarbon);
  mArgs.addToSection(CarbonContext::scGenCompileControl, scLibMap);
  mArgs.addString(scL, CRYPT("Specifies a library to be searched when resolving a design unit.  Multiple instances of this switch are allowed."), NULL, true, true, CarbonContext::ePassCarbon);
  mArgs.addToSection(CarbonContext::scGenCompileControl, scL);
  mArgs.addString(scLf, CRYPT("Specifies a library to be searched first when resolving a design unit.  Multiple instances of this switch are allowed."), NULL, true, true, CarbonContext::ePassCarbon);
  mArgs.addToSection(CarbonContext::scGenCompileControl, scLf);
  mArgs.addBool(scScan, CRYPT("Scans files to generate design hierarchy file."), false, ePassCarbon);
  mArgs.addString(scTopLevelParam, // Option
                  CRYPT("Override the value of a parameter or generic in a top level module or entity. The format should be <parameter or generic name>=<value>."), // Description
                  NULL, // Default value
                  true, // No default
                  true, // Allow multiple
                  ePassCarbon // Pass
                  );
  mArgs.addToSection(CarbonContext::scGenCompileControl, scTopLevelParam);
  mArgs.addString(scTimeBomb, CRYPT("Embed a timebombed license in the model, in the format YYYY-MM-DD.  This requires a special license feature."), NULL, true, false, ePassCarbon);

  // We want to show this to users only if they have a valid timebomb
  // license.  TimebombHelper::getLatestTimebomb() returns non-zero if
  // there is a timebomb-enabling license available.
  TimebombHelper helper;
  UtString unusedDate, unusedFeature;
  if (helper.getLatestTimebomb(&unusedDate, &unusedFeature) != 0) {
    mArgs.addToSection(CarbonContext::scGenCompileControl, scTimeBomb);
  }

  mArgs.addBool(scNoValidateIODB, CRYPT("Skip validation of IODB contents."), false, ePassCarbon);
  mArgs.addBool(scNoDesignHierarchy, CRYPT("Skip the dump hierarchy stage."), false, ePassCarbon);
} // void CarbonContext::setupOptions


void CarbonContext::setupPhases(void)
{

  mPhases[eParseCmdline] = &CarbonContext::runParseCmdline;
  mPhaseNames[eParseCmdline] = CRYPT("CmdLine");
  mPhaseNamePhrases[eParseCmdline] = CRYPT("Command Line Processing");

  mPhases[ePrintCbuildBanner] = &CarbonContext::runPrintCbuildBanner;
  mPhaseNames[ePrintCbuildBanner] = CRYPT("PrintBanner");
  mPhaseNamePhrases[ePrintCbuildBanner] = CRYPT("Banner Printing");

  mPhases[eCheckoutCbuildLicense] = &CarbonContext::runCheckoutCbuildLicense;
  mPhaseNames[eCheckoutCbuildLicense] = CRYPT("AllocateGlobals");
  mPhaseNamePhrases[eCheckoutCbuildLicense] = CRYPT("Global Allocation");

  mPhases[eParseAttributes] = &CarbonContext::runParseAttributes;
  mPhaseNames[eParseAttributes] = CRYPT("ParseAttributes");
  mPhaseNamePhrases[eParseAttributes] = CRYPT("XML Attribute Parsing");

  mPhases[eParseDirectives] = &CarbonContext::runParseDirectives;
  mPhaseNames[eParseDirectives] = CRYPT("ParseDirectives");
  mPhaseNamePhrases[eParseDirectives] = CRYPT("Directive Collection");

  mPhases[eParseLanguages] = &CarbonContext::runParseLanguages;
  mPhaseNames[eParseLanguages] = CRYPT("LangParse");
  mPhaseNamePhrases[eParseLanguages] = CRYPT("HDL Language Parsing");

  mPhases[eDumpHierarchy] = &CarbonContext::runDumpHierarchy;
  mPhaseNames[eDumpHierarchy] = CRYPT("DumpHierarchy");
  mPhaseNamePhrases[eDumpHierarchy] = CRYPT("Dump Instance Hierarchy");

  mPhases[ePopulateNucleus] = &CarbonContext::runPopulateNucleus;
  mPhaseNames[ePopulateNucleus] = CRYPT("PopulateNucleus");
  mPhaseNamePhrases[ePopulateNucleus] = CRYPT("Internal Representation Construction");

  mPhases[eDumpDesignPopulate] = &CarbonContext::runDumpDesignPopulate; 
  mPhaseNames[eDumpDesignPopulate] = CRYPT("DumpDesignPopulate");
  mPhaseNamePhrases[eDumpDesignPopulate] = CRYPT("Population Results Dumping");

  mPhases[eFindBlobs] = &CarbonContext::runFindBlobs; 
  mPhaseNames[eFindBlobs] = CRYPT("FindBlobs");
  mPhaseNamePhrases[eFindBlobs] = CRYPT("Blob Location");

  mPhases[eEvalDesignDirectives] = &CarbonContext::runEvalDesignDirectives;
  mPhaseNames[eEvalDesignDirectives] = CRYPT("evalDesignDirectives");
  mPhaseNamePhrases[eEvalDesignDirectives] = CRYPT("Design Directives Processing");

  mPhases[ePostPopulateResynth] = &CarbonContext::runPostPopulateResynth; 
  mPhaseNames[ePostPopulateResynth] = CRYPT("PostPopulateResynth");
  mPhaseNamePhrases[ePostPopulateResynth] = CRYPT("Internal Representation Adjustment");

  mPhases[eMakeSizesExplicit] = &CarbonContext::runMakeSizesExplicit; 
  mPhaseNames[eMakeSizesExplicit] = CRYPT("MakeSizesExplicit");
  mPhaseNamePhrases[eMakeSizesExplicit] = CRYPT("Adjust expression sizing");

  mPhases[eEarlyFlatteningAnalysis] = &CarbonContext::runEarlyFlatteningAnalysis;
  mPhaseNames[eEarlyFlatteningAnalysis] = CRYPT("EarlyFlatteningAnalysis");
  mPhaseNamePhrases[eEarlyFlatteningAnalysis] = CRYPT("Early Design Flattening");

  mPhases[eGlobalOptimizations] = &CarbonContext::runGlobalOptimizations;
  mPhaseNames[eGlobalOptimizations] = CRYPT("GlobOpts");
  mPhaseNamePhrases[eGlobalOptimizations] = CRYPT("Full Design Optimization");

#ifdef CARBON_PV
  mPhases[eProgrammersView] = &CarbonContext::runProgrammersView;
  mPhaseNames[eProgrammersView] = CRYPT("ProgView");
  mPhaseNamePhrases[eProgrammersView] = CRYPT("Full Design Optimization");
#endif

  mPhases[ePortAnalysis] = &CarbonContext::runPortAnalysis;
  mPhaseNames[ePortAnalysis] = CRYPT("PortAnalysis");
  mPhaseNamePhrases[ePortAnalysis] = CRYPT("Port Analysis");

  mPhases[eLocalAnalysis] = &CarbonContext::runLocalAnalysis;
  mPhaseNames[eLocalAnalysis] = CRYPT("Analysis");
  mPhaseNamePhrases[eLocalAnalysis] = CRYPT("Design Analysis");

  mPhases[ePostLocalAnalysis] = &CarbonContext::runPostLocalAnalysis;
  mPhaseNames[ePostLocalAnalysis] = CRYPT("Post Analysis");
  mPhaseNamePhrases[ePostLocalAnalysis] = CRYPT("Design Post Analysis");

  mPhases[eElaboratedAnalysis] = &CarbonContext::runElaboratedAnalysis;
  mPhaseNames[eElaboratedAnalysis] = CRYPT("ElabAnalysis");
  mPhaseNamePhrases[eElaboratedAnalysis] = CRYPT("Full Design Elaboration");

  mPhases[eDebugPrint] = &CarbonContext::runDebugPrint;
  mPhaseNames[eDebugPrint] = CRYPT("DBGPrint");
  mPhaseNamePhrases[eDebugPrint] = CRYPT("Debug Print");

  mPhases[eHierAlias] = &CarbonContext::runHierAlias;
  mPhaseNames[eHierAlias] = CRYPT("HierAlias");
  mPhaseNamePhrases[eHierAlias] = CRYPT("Hierarchical Alias Processing");

  mPhases[eReduce] = &CarbonContext::runReduce;
  mPhaseNames[eReduce] = CRYPT("Reduce");
  mPhaseNamePhrases[eReduce] = CRYPT("Reduce Transformation");

  mPhases[eSchedule] = &CarbonContext::runSchedule;
  mPhaseNames[eSchedule] = CRYPT("Schedule");
  mPhaseNamePhrases[eSchedule] = CRYPT("Schedule Creation");

  mPhases[eMarkConstants] = &CarbonContext::runMarkConstants;
  mPhaseNames[eMarkConstants] = CRYPT("MarkConstants");
  mPhaseNamePhrases[eMarkConstants] = CRYPT("Constant Identification");

  mPhases[eBackend] = &CarbonContext::runBackend;
  mPhaseNames[eBackend] = CRYPT("Backend");
  mPhaseNamePhrases[eBackend] = CRYPT("Backend Transformation");

  mPhases[eCodeGen] = &CarbonContext::runCodeGen;
  mPhaseNames[eCodeGen] = CRYPT("CodeGen");
  mPhaseNamePhrases[eCodeGen] = CRYPT("Code Generation");

  mPhases[eLowCG] = &CarbonContext::runLowCG;
  mPhaseNames[eLowCG] = CRYPT("LowCG");
  mPhaseNamePhrases[eLowCG] = CRYPT("Low-level CG");

  mPhases[eBackendCompile] = &CarbonContext::runBackendCompile;  
  mPhaseNames[eBackendCompile] = CRYPT("BackendCompile");
  mPhaseNamePhrases[eBackendCompile] = CRYPT("Backend Compilation");

  mPhases[eGuiDBFork] = &CarbonContext::runGuiDBFork;
  mPhaseNames[eGuiDBFork] = CRYPT("GuiDBFork");
  mPhaseNamePhrases[eGuiDBFork] = CRYPT("GUI DB Fork");

  mPhases[eGuiDBWait] = &CarbonContext::runGuiDBWait;
  mPhaseNames[eGuiDBWait] = CRYPT("GuiDBWait");
  mPhaseNamePhrases[eGuiDBWait] = CRYPT("GUI DB Wait");

  mPhases[eGuiDBWrite] = &CarbonContext::runGuiDBWrite;
  mPhaseNames[eGuiDBWrite] = CRYPT("GuiDBWrite");
  mPhaseNamePhrases[eGuiDBWrite] = CRYPT("Early Design Database Write");

  mPhases[eGuiBuildSymtab] = &CarbonContext::runGuiBuildSymtab;
  mPhaseNames[eGuiBuildSymtab] = CRYPT("GuiBuildSymtab");
  mPhaseNamePhrases[eGuiBuildSymtab] = CRYPT("Early Design Database Write Symbol Table");

  mPhases[eDBWrite] = &CarbonContext::runDBWrite;
  mPhaseNames[eDBWrite] = CRYPT("DBWrite");
  mPhaseNamePhrases[eDBWrite] = CRYPT("Design Database Write");

  mPhases[eProtectSource] = &CarbonContext::runProtectSource;
  mPhaseNames[eProtectSource] = CRYPT("ProtectSource");
  mPhaseNamePhrases[eProtectSource] = CRYPT("Source Protection");

#ifdef INTERPRETER
  mPhases[eInterpGen] = &CarbonContext::runInterpGen;
  mPhaseNames[eInterpGen] = "InterpGen";
  mPhaseNamePhrases[eInterpGen] = "InterpGen";
#endif

  mPhases[eSVInspector] = &CarbonContext::runSVInspector;
  mPhaseNames[eSVInspector] = CRYPT("SVInspector");
  mPhaseNamePhrases[eSVInspector] = CRYPT("SystemVerilog Source file inspection");


  mPhaseNames[eLastPhase] = CRYPT("end");
  mPhaseNamePhrases[eLastPhase] = CRYPT("end");
}


void CarbonContext::setupPhaseFlow(void)
{
  if (isBackendCompileOnly())
  {
    mPhaseFlow.push_back(eBackendCompile);
  }
  else if (mCodeGen->isProfileUse())
  {
    // When recompiling with -fprofile-use, we don't need to
    // regenerate cxx files.  Just check args and do backend.

    // Needed to report bogus options.
    mPhaseFlow.push_back(eCheckoutCbuildLicense);
    mPhaseFlow.push_back(eParseDirectives);
    mPhaseFlow.push_back(eBackendCompile);
  }
  else
  {
    mPhaseFlow.push_back(ePrintCbuildBanner);
    if (mProtectSource) 
    {
      mPhaseFlow.push_back(eProtectSource);
    }
    else if ( mSVInspectorCount ){
      mPhaseFlow.push_back(eCheckoutCbuildLicense);
      mPhaseFlow.push_back(eParseLanguages);
      mPhaseFlow.push_back(eDumpHierarchy);
      mPhaseFlow.push_back(eSVInspector);
    }
    else
    {
      if (mArgs.getBoolValue(scProtectHDLOnly))
        // "Option +protectHDLOnly has effect only when option
        // +protect is specified."
        mMsgContext->OptionRequiresOption(scProtectHDLOnly, "+protect");

      mPhaseFlow.push_back(eCheckoutCbuildLicense);
      mPhaseFlow.push_back(eParseAttributes);
      mPhaseFlow.push_back(eParseDirectives);
      mPhaseFlow.push_back(eParseLanguages);
      mPhaseFlow.push_back(eDumpHierarchy);
      mPhaseFlow.push_back(ePopulateNucleus);
      mPhaseFlow.push_back(eDumpDesignPopulate);
      if (compileWithProfiling())
        mPhaseFlow.push_back(eFindBlobs);
      mPhaseFlow.push_back(eEvalDesignDirectives);
      mPhaseFlow.push_back(ePostPopulateResynth);
      if (mArgs.getBoolValue (scTestGuiDB)) {
        mPhaseFlow.push_back (eGuiDBFork);
        mPhaseFlow.push_back (eGuiDBWait);
      } else if (mArgs.getBoolValue (scWriteGuiDB)) {
        mWritingGuiDB = true;           // run the quick libdesign.gui.db flow
      }
      if (mWritingGuiDB) {
        // runs just enough to emit libdesign.gui.db and then terminates
        mPhaseFlow.push_back(eElaboratedAnalysis);
        mPhaseFlow.push_back(eGuiBuildSymtab);
        mPhaseFlow.push_back(eGuiDBWrite);
      } else {
        mPhaseFlow.push_back(eMakeSizesExplicit);
        mPhaseFlow.push_back(eEarlyFlatteningAnalysis);
        mPhaseFlow.push_back(eGlobalOptimizations);
#ifdef CARBON_PV
        if (mArgs.getBoolValue(CRYPT("-pv"))) {
          mPhaseFlow.push_back(eProgrammersView);
        }
#endif
        mPhaseFlow.push_back(ePortAnalysis);
        mPhaseFlow.push_back(eLocalAnalysis);
        mPhaseFlow.push_back(ePostLocalAnalysis);
        mPhaseFlow.push_back(eElaboratedAnalysis);
        mPhaseFlow.push_back(eDebugPrint);
        mPhaseFlow.push_back(eHierAlias);
        mPhaseFlow.push_back(eReduce);
        mPhaseFlow.push_back(eSchedule);
        mPhaseFlow.push_back(eMarkConstants);
        mPhaseFlow.push_back(eBackend);
        mPhaseFlow.push_back(eCodeGen);
        if (mArgs.getBoolValue (scLowCG)) {
          mPhaseFlow.push_back(eLowCG);
        }
        mPhaseFlow.push_back(eDBWrite);
#ifdef INTERPRETER
        mPhaseFlow.push_back(eInterpGen);
#endif
        mPhaseFlow.push_back(eBackendCompile);
      }
    }
  }
}
    
bool CarbonContext::parsePhase(const char* phaseName, Phase* phase)
  const
{
  if (strcasecmp(phaseName, CRYPT("localAnalysis")) == 0) // upward compatibility
  {
    *phase = eLocalAnalysis;
    return true;
  }
  for (int i = 0; i <= (int) eLastPhase; ++i)
  {
    PhaseToNameTable::const_iterator x = mPhaseNames.find(static_cast<Phase>(i));
    UtString s = (*x).second;

    if (strcasecmp(phaseName, s.c_str()) == 0)
    {
      *phase = (Phase) i;
      return true;
    }
  }
  return false;
}

void CarbonContext::printLastPhaseStrings()
  const
{
  for (int i = 0; i <= (int) eLastPhase; ++i)
  {
    PhaseToNameTable::const_iterator x = mPhaseNames.find(static_cast<Phase>(i));
    UtString s = (*x).second;
    UtIO::cout() << s << UtIO::endl;
  }
}

void CarbonContext::printBackendTarget() const
{
  const char* target = mArgs.getStrValue("-target");
  if (! target)
    target = "(null)"; // just in case. Shouldn't happen.

  UtIO::cout() << target << UtIO::endl;
}

// In addition to the basic uses of this function, this is also used
// to add the software version to the database. This should only print
// the software version into the string.
void CarbonContext::printVersion(UtOStream& out) const
{
  out << scCbuildReleaseID << UtIO::endl;
#ifdef PATCH_STRING
  out << "UNOFFICIAL PATCH:   " << PATCH_STRING << "\n";
#endif
}

void CarbonContext::printVerboseVersion() const
{
  UtIO::cout() << "Release identifier: ";
  printVersion(UtIO::cout());
  UtIO::cout() << "CVS id string       " << gCarbonVersion() << UtIO::endl;
  UtIO::cout() << "Cheetah version:    " << veGetVersionInfo() << UtIO::endl;
  UtIO::cout() << "Jaguar version:     " << vhGetVersionInfo() << UtIO::endl;
  UtIO::cout() << "MVV version:        " << mvvGetVersionNumber() << UtIO::endl;
  UtIO::cout() << "Verific version:    " << mVerificContext->verificReleaseString() << UtIO::endl;

  const char* ex_platform = "Unknown";
#if pfLINUX
      ex_platform = "Linux";
#elif pfLINUX64
      ex_platform = "Linux64";
#endif
  UtIO::cout() << "Carbon compiler platform: " << ex_platform << UtIO::endl;
}

void CarbonContext::printUsage()
{
  runPrintCbuildBanner();
  UtIO::cout() << UtIO::endl;
  
  UtString usage;
  bool noInternalOptions = (getenv(CRYPT("CARBON_NO_INTERNAL_DOC")) != NULL);
  mArgs.getUsageVerbose(&usage, noInternalOptions);
  UtIO::cout() << usage << UtIO::endl;
}

/* runParseCmdline runs three pre-scans plus 2 phases of command line
 * parsing. First pre-scan processes -protect and -unprotect switches, 
 * the second one processes the -ef switch and the third one parses all 
 * the -f arguments, and places the full command line into a UtStringArgv. 
 * The first phase parses all the carbon options that don't contribute 
 * to command line state. The second phase parses all the hdl/Interra 
 * specific options that may contain state (such as -93 and -87).
 *
 * This phase *must* find any commandline-related hdl-specific
 * problems before checking out a license (which is the next carbon
 * context phase). So, things such as -vhdlTop and -vlogTop being used
 * simultaneously must be resolved here. Otherwise, we may try to
 * checkout the wrong license - a license that a user may not have. 
 *
 */
bool CarbonContext::runParseCmdline()
{
  int numOptions;
  UtString errmsg;
  bool ret = true;
  ArgProc::ParseStatusT parseStat;
  bool isProtUnprot = false;

  // Process -ef switch
  unsigned int ef_index = 0;
  UtString argv_ef_buffer; // string of args in .ef file
  UtStringArgv argv_ef;
  parseStat = mArgs.preParseCommandLineEF(mArgc, mArgv,
                                          1,  // allow only a single -ef on the command line
                                          &ef_index,
                                          &errmsg);

  if (parseStat == ArgProc::eParseError)
  {
    mMsgContext->CmdLineError(errmsg.c_str());
    return false;
  }
  else if (parseStat == ArgProc::eParseWarning)
  {
    mMsgContext->CmdLineWarning(errmsg.c_str());
  }
  else if (parseStat == ArgProc::eParseNote)
  {
    mMsgContext->CmdLineNote(errmsg.c_str());
  }
  else if(parseStat == ArgProc::eParsed)
  {
    // expand the contents of the ef file
    if(getEFfileContext(mArgv[ef_index+1], &argv_ef_buffer, &errmsg) == false)
    {
      mMsgContext->CmdLineError(errmsg.c_str());
      return false;
    }


    // replace the '-ef file_name' on the command line with the contents of file_name.
    mArgs.replaceArgvForEF(mArgc, mArgv, ef_index, &argv_ef_buffer, &argv_ef, &errmsg);

    mArgv = argv_ef.getArgv();
    mArgc = argv_ef.getArgc();
  }

  // Parse the command line, but don't expand any environment variable
  // except the ones used in include file names.
  parseStat = mArgs.preParseCommandLine(mArgc, 
                                        mArgv,
                                        &mUnexpandedArgv,
                                        &errmsg,
                                        false);
  
  if (parseStat == ArgProc::eParseError)
  {
    mMsgContext->CmdLineError(errmsg.c_str());
    return false;
  }
  else if (parseStat == ArgProc::eParseWarning)
  {
    mMsgContext->CmdLineWarning(errmsg.c_str());
  }
  else if (parseStat == ArgProc::eParseNote)
  {
    mMsgContext->CmdLineNote(errmsg.c_str());
  } 

  // Pre-parse scan to get the full command line
  parseStat = mArgs.preParseCommandLine(mArgc, 
                                        mArgv,
                                        &mArgvBuffer,
                                        &errmsg);
  
  if (parseStat == ArgProc::eParseError)
  {
    mMsgContext->CmdLineError(errmsg.c_str());
    return false;
  }
  else if (parseStat == ArgProc::eParseWarning)
  {
    mMsgContext->CmdLineWarning(errmsg.c_str());
  }
  else if (parseStat == ArgProc::eParseNote)
  {
    mMsgContext->CmdLineNote(errmsg.c_str());
  } 
   
  // Now parse any programmer overrides (twice - once for expaned args, and once for unexpanded args)
  const char* override = getenv("CARBON_CBUILD_ARGS");
  if (override != NULL)
  {
    mArgs.putWarnOnDuplicates(false); // avoids spurious reg-test issues
    if ( (mArgs.tokenizeArgString(override, &mArgvBuffer,     &errmsg, true) != ArgProc::eParsed) ||
         (mArgs.tokenizeArgString(override, &mUnexpandedArgv, &errmsg, false) != ArgProc::eParsed)  )
    {
      UtIO::cerr() << "CARBON_CBUILD_ARGS: " << errmsg.c_str() << UtIO::endl;
      return false;
    }
  }

  // now a last check for -ef on the command line, it might have appeared during processing due
  // to nesting of -f files. (this is not supported)
  parseStat = mArgs.preParseCommandLineEF(mArgc, mArgv,
                                          0,  // do not allow any -ef options
                                          &ef_index,
                                          &errmsg);
  
  switch ( parseStat ){
  case ArgProc::eParsed:       { /* one -ef found, wanted a count of zero */ return false;}
  case ArgProc::eParseError:   {mMsgContext->CmdLineError(errmsg.c_str());   return false;}
  case ArgProc::eParseWarning: {mMsgContext->CmdLineWarning(errmsg.c_str()); break; }
  case ArgProc::eParseNote:    {mMsgContext->CmdLineNote(errmsg.c_str());    break; }
  case ArgProc::eNotParsed:    {/* correct number (0) of -ef found*/         break; }
  }

  mSaveArgv = mArgvBuffer;
  mArgv = mArgvBuffer.getArgv();
  mArgc = mArgvBuffer.getArgc();

  // now make sure that any options that are restricted to .ef files only appeared there
  if ( mArgs.testForMisuseOfEFOptions(argv_ef_buffer, mArgc, mArgv, &errmsg) ){
    mMsgContext->CmdLineError(errmsg.c_str());
    return false;
  }

  // Copy the mUnexpandedArgv so that no modifications are applied to it
  UtStringArgv tmpArgv = mUnexpandedArgv;
  int unexpandedArgc = tmpArgv.getArgc();
  char** unexpandedArgv = tmpArgv.getArgv();

  // PARSE PHASE 1: Get mode args -- ePassMode
  // first check if we only want to know which compiler we are
  // targetting. We want to do this now so we don't have to connect to
  // the license server if we don't need to. Also, check if we just
  // want to know the version
  parseStat = mArgs.parseCommandLine( &mArgc, 
                                      mArgv, 
                                      &numOptions, 
                                      &errmsg,
                                      NULL,
                                      &unexpandedArgc,
                                      unexpandedArgv);

  if (parseStat == ArgProc::eParseError)
  {
    mMsgContext->CmdLineError(errmsg.c_str());
    return false;
  }
  else if (parseStat == ArgProc::eParseWarning) {
    mMsgContext->CmdLineWarning(errmsg.c_str());
  }

  if (mArgs.getBoolValue("-version"))
  {
    printVersion(UtIO::cout());
    exit(0);
  }

  if (mArgs.getBoolValue("-verboseVersion"))
  {
    printVerboseVersion();
    exit(0);
  }

  if (mArgs.getBoolValue(scGetTarget))
  {
    printBackendTarget ();
    exit(0);
  }

  // Look for +protect plusarg.
  if (mArgs.isParsed(scProtectWC) == ArgProc::eParsed)
  {
    ArgProc::StrIter p = mArgs.getMatchedArgs(scProtectWC);
    INFO_ASSERT(! p.atEnd(), "ArgProc error parsing +protect command line"); // ArgProc bogosity
    
    const char* protectArg = *p;

    // get last occurrence of +protect
    for (; ! p.atEnd(); ++p)
      protectArg = *p;
    
    //scProtectWC is a char array, not a char*
    // don't include the * and \0 when advancing
    protectArg += sizeof(scProtectWC) - 2; 
    mProtectSource = true;
    
    // Arg may be of the form +protect[.X], where X is the optional
    // filename extension for generated protected source files.
    if (*protectArg == '\0')
      mProtectedSourceExtension = ".vp";  // default extension is .vp
    else if (*protectArg == '.')
      mProtectedSourceExtension = protectArg;
    else
    {
      errmsg.assign("Internal error parsing ");
      errmsg << protectArg;
      mMsgContext->CmdLineError(errmsg.c_str());
      return false;
    }
  }

  // Now check if we have a diagnostics license. If we do we can
  // print out and use the internal options.
  {
    UtString reason;
    // if we are doing a +protect there is no reason to checkout a
    // diags license
    if (! mProtectSource)
      mHaveDiagLicense = mLicense->checkout(UtLicense::eDIAGNOSTICS, &reason);
  }

  if (mHaveDiagLicense)
    mArgs.accessHiddenOptions();
    
  // Note -- options specific to individual phases should be set up
  // in the constructors for the classes that implement those phases.
  // That way the option UtString "use" and "def" is localized.
  
  // All the command-line options should be set in the constructor.
  if (mArgc == 1)
  {
    printUsage();
    exit(0);
  }

  // allow usage of all internal switches except -noEncrypt
  mArgs.allowUsageAll();
  // Now you must explicitly identify the switches that must have an
  // internal license.  This is done here by calling disallowUsage on
  // each such switch.

  mArgs.disallowUsage(CRYPT("-noEncrypt"));
  mArgs.disallowUsage(CRYPT("-dumpClockTree"));
  mArgs.disallowUsage(scCustDB);
  mArgs.disallowUsage(scProtectF);
  mArgs.disallowUsage(scUnProtectF);

  // Special handling here: If we have CDS Internal, then these switches
  // don't have to be in an .ef file
  if (!mHaveDiagLicense)
  {
    mArgs.setIsAllowedOnlyInEFFile(scAllowCompileLicense);
    mArgs.setIsAllowedOnlyInEFFile(scAllowRuntimeLicense);
  }

  // PARSE PHASE 2: Get Carbon options -- ePassCarbon
  parseStat =  mArgs.parseCommandLine( &mArgc, 
                                       mArgv, 
                                       &numOptions, 
                                       &errmsg, 
                                       NULL, 
                                       &unexpandedArgc,
                                       unexpandedArgv);

  if (parseStat == ArgProc::eParseError)
  {
    mMsgContext->CmdLineError(errmsg.c_str());
    return false;
  }
  else if (parseStat == ArgProc::eParseWarning) {
    mMsgContext->CmdLineWarning(errmsg.c_str());
  }

  if (mArgs.getBoolValue("-h"))
  {
    printUsage();
    exit(0);
  }

  mArgs.setBoolValue(scVSP, true);
  
  // Get the library mapping file, if specified by the user
  if (mArgs.isParsed(scLibMap) == ArgProc::eParsed)
  {
    UtString libMapFile(mArgs.getStrValue(scLibMap));
    if (! mFileCollector->parseLibMapFile(libMapFile.c_str()))
      return false;
  }
  else
  {
    // Set the default logical lib name.  We don't know the path name yet,
    // but this must be initialized before parsing the command line.
    // The same default lib is use for all VHDL language versions and all Verilog language versions
    for(LangVer langVer=LangVer::LangVer_MIN; langVer < LangVer::LangVer_MAX; langVer++) {
      mFileCollector->setDefaultLib( JaguarContext::scDefaultVhdlLib, NULL, langVer );
    }
  }
  
  //! Add any vhdl file extensions specified by the user
  if (mArgs.isParsed(JaguarContext::scVhdlExt) == ArgProc::eParsed)
  {
    ArgProc::StrIter extP;
    INFO_ASSERT(mArgs.getStrIter(JaguarContext::scVhdlExt, &extP) == ArgProc::eKnown,
                "VHDL command line switches not initialized.");
    for (; ! extP.atEnd(); ++extP)
    {
      for (StrToken tok(*extP, ":"); ! tok.atEnd(); ++tok) {
        mFileCollector->addHdlExt(LangVer::VHDL87, *tok);
        mFileCollector->addHdlExt(LangVer::VHDL93, *tok);
      }
    }
  }

  // Check for any switches that have been removed, but need more than
  // the generic "unknown option" message.
  if (checkRemovedSwitches()) {
    return false;
  }
 
  // moved following code up here as we need fileRoot for comparing the
  // libmap absolute path with the relative library path specified in cmdline.
  // This enables checking of different absolute path for give logical library.

  // Find the destination dir.  -o was declared in codegen's ctor
  const char* targetlib;
  UtString file;
  mArgs.getStrValue("-o", &targetlib);
  OSParseFileName(targetlib, &mFileRoot, &file);
  if (mFileRoot.empty())
    OSGetCurrentDir(&mFileRoot);
  const char* dot = strrchr(file.c_str(), '.');
  if ((dot == NULL) ||
      (strncmp(file.c_str(), "lib", 3) != 0) ||
      ((strcmp(dot, ".so") != 0) &&
       (strcmp(dot, ".dll") != 0) &&
       (strcmp(dot, ".lib") != 0) &&
       (strcmp(dot, ".a") != 0)))
  {
    mMsgContext->InvalidLibrary(targetlib);
    return false;
  }
  mFileRoot << "/" << file.c_str();
  mFileRoot.resize(mFileRoot.size() - strlen(dot));

  mFileCollector->preParseCmdlineSetFileRoot(mFileRoot.c_str());

  // The file collector needs to know which language version is to be used for parsing the HDL files.
  // Command line switches define the language version (ie -v2k) and are normally processed
  // by the call to parseCommandLine (phase 1 above). 
  // However any hidden options are skipped by that call, and so here we make sure that the file
  // collector knows about those switches before phase 3.
  if ( mArgs.getBoolValue(CarbonContext::scSVerilog) )  {
    // currently -sverilog is a hidden option so parseCommandLine will have skipped it, so we need to tell the file collector about it here
    // TODO: this should be SystemVerilog2012, but for some reason when changed the HdlFileCollector does not work correctly
    mFileCollector->setVerilogVersion(LangVer::SystemVerilog2009); // when useVerific becomes supported this line needs to be commented out (see also another line with 'when useVerific becomes')
  } 

  // PARSE PHASE 3: Grab files and Interra options -- ePassHdl
  parseStat = mArgs.parseCommandLine( &mArgc, 
                                      mArgv, 
                                      &numOptions, 
                                      &errmsg,
                                      mFileCollector );

  if (parseStat == ArgProc::eParseError)
  {
    mMsgContext->CmdLineError(errmsg.c_str());
    return false;
  }
  else if (parseStat == ArgProc::eParseWarning) {
    mMsgContext->CmdLineWarning(errmsg.c_str());
  }

  if (mArgs.getBoolValue("-w"))
    mMsgContext->suppressAllWarnings(true);



  // process -fprotect and -funprotect switches
  bool rtn = protectUnprotectFile(mSaveArgv.getArgc(), mSaveArgv.getArgv(), &isProtUnprot, &errmsg);
  if( isProtUnprot == true)
  {
    if(rtn == false)
      mMsgContext->CmdLineError(errmsg.c_str());
    else
      mMsgContext->CmdLineNote(errmsg.c_str());

    exit(0);
  }



  // If we are in +protect mode,  RETURN NOW!
  if (mProtectSource)
    return true;
  
  mGenerateCModelWrapper = !mArgs.getBoolValue(scNoCModelWrapper);
  mGenerateSystemCWrapper = mArgs.getBoolValue(scSystemCWrapper);
  mIsProfiling = mArgs.getBoolValue(scProfile);
  if (ArgProc::eKnown != mArgs.getIntFirst (scSVInspector, &mSVInspectorCount) ) {
    mSVInspectorCount = 0;           // indicate that command line switch not specified
  }
  mSVerilog = mArgs.getBoolValue(CarbonContext::scSVerilog);

  if (mArgs.getBoolValue(scFredo)) 
    initFredo();

  // -vspNoCC sets codegen into a NO-BACKEND-COMPILE mode.
  // This is only called by the vspcompiler script. This is the same
  // as -nocc, except -vspNoCC will never appear in a cmd file.
  if (mCodeGen->isProfileUse())
    mArgs.setBoolValue(scBackendCompileOnly, true);
  else if (mArgs.getBoolValue(scVSPNoCC))
    mCodeGen->putNoCC(true);
  
  if (isBackendCompileOnly()) {
    mCodeGen->checkArgs ();
    return true;
  }

  mLogStream = new CarbonLog(mFileRoot.c_str());
  mMsgContext->addReportStream(mLogStream);

#ifdef CARBON_PV
  // PSD spec file specified with -psd
  const char* psdfile;
  if (mArgs.getStrValue("-psd", &psdfile) == ArgProc::eKnown) {
    mPSDFileName << psdfile;
  }
#endif

  UtString defaultCmdLine ("+define+CARBON\n"); // this is added to the top of both .ncmd and .cmd files, make sure you insert a newline after (at least) the last item in this string.

  // Write the pre-parsed but not expanded parameters to carbon.ncmd
  file.clear();
  file << mFileRoot << ".ncmd";
  writeCmdFile(file.c_str(), true, defaultCmdLine.c_str(), true/*writing .ncmd file*/);

  // Write the pre-parsed parameters to carbon.cmd
  file.clear();
  file << mFileRoot << ".cmd";
  writeCmdFile(file.c_str(), true, defaultCmdLine.c_str(), false/*writing .cmd file*/);

  const char* val;
  if ((mArgs.getStrValue(scLastPhase, &val) == ArgProc::eKnown) &&
      !parsePhase(val, &mLastPhase))
  {
    UtIO::cout() << "Invalid " << scLastPhase << " switch value:  " << val << "\nChoices:" <<  UtIO::endl;
    printLastPhaseStrings();
    ret = false;
  }
  // -vhdlCompile stops after parsing the VHDL.
  // -compileLibOnly stops cbuild after parsing HDL (VHDL/Verilog) and dumping
  // it in the specified/default libraries.
  if (isLibraryCompileOnly())
  {
    mLastPhase = eParseLanguages;
  }

  // Specify file root so that missing library paths can be constructed 
  // in default file root location.
  mFileCollector->setFileRoot(mFileRoot.c_str());

  // -cemgen stops after population
  ArgProc::StrIter iter;
  bool known = mArgs.getStrIter("-cemgen", &iter) == ArgProc::eKnown;
  INFO_ASSERT(known, "-cemgen must be a known command line option");
  if (!iter.atEnd()) {
    mLastPhase = ePopulateNucleus;
  }

  if ((mArgs.getStrValue(scCostPhase, &val) == ArgProc::eKnown) &&
      !parsePhase(val, &mCostPhase))
  {
    UtIO::cout() << "Invalid " << scCostPhase << " switch value:  " << val << UtIO::endl;
    ret = false;
  }

  if (mArgs.getStrValue("-tristate", &val) == ArgProc::eKnown)
  {
    if ((strcmp(val, "z") == 0) || (strcmp(val, "Z") == 0))                  
      mTristateMode = eTristateModeZ;
    else if ((strcmp(val, "x") == 0) || (strcmp(val, "X") == 0))
      mTristateMode = eTristateModeX;
    else if (strcmp(val, "0") == 0)             mTristateMode = eTristateMode0;
    else if (strcmp(val, "1") == 0)             mTristateMode = eTristateMode1;
    else {
      UtIO::cout() << "Invalid -tristate switch value:  " << val << UtIO::endl;
      ret = false;
    }
    mCodeGen->setTristateMode(mTristateMode);
  }

  if (mArgs.getBoolValue(scModulePhaseStats)) {
    mModulePhaseStats = true;
  }


  if ( mArgs.getBoolValue(scCreateAMemLeak) )
  {
    // Using UInt64 because it's the same size on all our platforms.
    UInt64* unused = new UInt64; // do not fix this memory leak -- it is
                                // used to make sure that the memory
                                // override code is working properly.
    // Add this line to prevent a compile warning.
    unused = unused;
  }

  mDumpMemory = CarbonMem::isMemDebugOn();
  if ( not mDumpMemory ) {
    
    const char* memDumpFile = mArgs.getStrValue(scDumpMemory);
    if ( memDumpFile != NULL )
    {
      // a -dumpMemory switch was specified but memoryDump is not
      // on. It must have been specified too late 
      // (like in CARBON_CBUILD_ARGS)
      mMsgContext->CmdLineError(CRYPT("-dumpMemory is only valid on the command line."));
      return false;
    }
  }

  // Some minor sanity checking.
  // 1) Make sure that -noCosts is not specified if -estimateCPS is
  // specified
  if (mArgs.getBoolValue(scEstimateCPS) && mArgs.getBoolValue(scNoCosts))
    mMsgContext->CmplrIncompatibleSwitches(scEstimateCPS, scNoCosts);

  // 2) Cannot use -vhdlTop and vlogTop together
  if ((mArgs.isParsed( JaguarContext::scVhdlTop ) == ArgProc::eParsed ) &&
      (mArgs.isParsed( "-vlogTop" ) == ArgProc::eParsed ))
    mMsgContext->CmplrIncompatibleSwitches(JaguarContext::scVhdlTop, "-vlogTop" );

  // 3) Cannot use -enableReplay and -noCheckpoint
  if ((mArgs.isParsed(scEnableReplay) == ArgProc::eParsed) &&
      (mArgs.isParsed("-noCheckpoint") == ArgProc::eParsed))
    mMsgContext->CmplrIncompatibleSwitches(scEnableReplay, "-noCheckpoint" );

  // 4) Cannot use -enableReplay and -noCoercePorts
  // The replay mechanism depends on the sets in the db to be
  // consistent with the scheduler. -noCoercePorts makes
  // uni-directional bidis inconsistent
  const char* noCoercePorts = CRYPT("-noCoercePorts");
  if ((mArgs.isParsed(scEnableReplay) == ArgProc::eParsed) &&
      (mArgs.isParsed(noCoercePorts) == ArgProc::eParsed))
    mMsgContext->CmplrIncompatibleSwitches(scEnableReplay, noCoercePorts);
  
  // -pliWrapper doesn't work when generating a 64-bit model.  Since
  // the switch is being deprecated, there's no point in making it
  // work.  Instead, print an error message.
  const char* targetArch = getenv("CARBON_TARGET_ARCH");
  bool pliWrapper = mArgs.getBoolValue(CRYPT("-pliWrapper"));
  // A 64-bit model can be requested with the -m64 command-line switch
  // or by setting the CARBON_TARGET_ARCH environment variable to
  // Linux64.
  bool target64 = mArgs.getBoolValue(CRYPT("-m64")) || ((targetArch != NULL) && (strcmp(targetArch, "Linux64") == 0));
  if (pliWrapper && target64) {
    mMsgContext->CmdLineError("The -pliWrapper switch is not supported when generating a 64-bit model.");
  }

  // Put the Carbon Model type we are creating.
  mIODB->clearVHMTypeFlags(eVHMAllTypes);
  // Speedcompiler does not create replayable or onDemand Carbon Models.
  if (isVSPCompile()) {
    if (!mArgs.getBoolValue(scDisableReplay) && mArgs.getBoolValue(scEnableReplay))
      mIODB->setVHMTypeFlags(eVHMReplay);
    if (mArgs.getBoolValue(scEnableOnDemand))
      mIODB->setVHMTypeFlags(eVHMOnDemand);
  }

  // Put the version of the software in the database
  UtString softwareVersion;
  UtOStringStream versionStream(&softwareVersion);
  printVersion(versionStream);
  mIODB->putSoftwareVersion(softwareVersion.c_str());

  // Write the lib map file. If it already exists, overwrite it.
  if (mArgs.isParsed(scLibMap) == ArgProc::eParsed)
  {
    UtString libMapFile(mArgs.getStrValue(scLibMap));
    if (! mFileCollector->saveLibMapFile(libMapFile.c_str()))
      return false;
  }
  
  return ret;
} // bool CarbonContext::runParseCmdline

void CarbonContext::writeCmdFile(const char* filename, 
                                 bool includeFilenames,
                                 const char* filePrefix,
                                 bool writeNCmdFile)
{
  UtString openReason;
  FILE* f = OSFOpen(filename, "w", &openReason);
  if (f == NULL) {
    mMsgContext->VlogCannotOpenFile(filename, openReason.c_str());
  }
  else
  {
    UtStringArgv ArgvBuffer = writeNCmdFile ? mUnexpandedArgv : mSaveArgv;
    // Don't write out the $CARBON_HOME/bin/cbuild in the .ncmd file
    if (!writeNCmdFile) {
      const char* exe = ArgvBuffer.getArgv()[0];
      fprintf(f, "// %s\n", exe); // comment exe name so -cmd can be used in -f
    }
    UtString buf;
    bool requiresValue, isInFile, isOutFile;

    if (filePrefix != NULL) {
      fprintf(f, "%s", filePrefix);
    }

    for (int i = 1; i < ArgvBuffer.getArgc(); ++i)
    {
      const char* arg = ArgvBuffer.getArgv()[i];
      if (arg && strcmp(arg, scVSPNoCC) != 0)
      {
        requiresValue = false;
        
        if (i < ArgvBuffer.getArgc() - 1) {
          mArgs.isValid(arg, &requiresValue, &isInFile, &isOutFile);
          if (!requiresValue) {
            isInFile = mCheetahContext->isFileOption(arg);
            requiresValue = isInFile || mCheetahContext->isValueOption(arg);
          }

          // Command line options that require a value
          if (requiresValue) {
            if ((strcmp(arg, scVSPCompileFileFlag) != 0) &&
                (includeFilenames ||
                 (!isInFile &&
                  (strcmp(arg, scGenTokenFile) != 0)))) // Don't overwrite tokens!
            {
              // print -v <file> on one line
              fprintf(f, "%s %s\n", arg,
                      UtShellTok::quote(ArgvBuffer.getArgv()[i + 1], &buf));
            }
            ++i;
          }
        }
        if (!requiresValue &&
            (includeFilenames || (arg[0] == '+') || (arg[0] == '-')))
        {
          // These command line options do not require a value.
          // plusargs fall into this category.
          //
          // plusargs that accept strings with embedded spaces cannot
          // have beginning/ending quotes inserted, otherwise the
          // Verific -f file parser will not recognize them. These 
          // need special processing to place quotes in a place that
          // can be recognized by Verific and Interra.
          const char* plusDefine = "+define+";
          if (strncmp(arg, plusDefine, strlen(plusDefine)) == 0)
          {
            fprintf(f, "%s\n", quotePlusDefine(arg, &buf));
          }
          else
            fprintf(f, "%s\n", UtShellTok::quote(arg, &buf));
        }
      } // if not -vspNoCC
    } // for
    fclose(f);
  } // else
} // void CarbonContext::writeCmdFile

// Quote +define args.
// +define args are passed to Verific through a command file.
// The linux shell will strip off quotes around the value, so the 
// quotes have to be added back in to enable the Verific
// -f file processing. 
//
// There are a couple cases that have to be handled:
//
// 1) +define+D="foo bar"
//
//    This should expand to two tokens: foo bar
//
// 2) +define+D='"foo bar"'
//
//    This should expand to one quoted token: "foo bar"
//
// 3) +define+D='\"foo bar\"'
//
//    This should be expanded to one quoted token: "foo bar"
// 
// The linux shell will turn the first into:
//
// +define+D=foo bar
//
// But Verific needs to see:
//
// +define+D="foo bar"
//
// The linux shell will turn the second into:
//
// +define+D="foo bar"
//
// And the third into:
//
// +define+D=\"foo bar\"
//
// But for both of these cases, Verific needs to see:
//
// +define+D="\"foo bar\""
//

const char* CarbonContext::quotePlusDefine(const char* arg, UtString *buf)
{
  buf->clear();
  
  const char* eq = NULL;
  
  // Quote with double quotes.
  char quoteChar = '"';
  char escChar = '\\';

  // Double quotes need to be escaped if the
  // macro value is already surrounded by quotes.
  bool escapeDoubleQuotes = false;
  
  // Find location of '=', and place quotes around everything after.
  // If it's already double quoted, escape those double quotes,
  // and add an extra layer of double quotes
  for (const char* s = arg; *s; ++s)
  {
    if (escapeDoubleQuotes && (*s == '"'))
      *buf << escChar;
    *buf << *s;
    // Everything after first '=' is part of the parameter
    if ((NULL == eq) && (*s == '='))
    {
      eq = s;
      // Is it already quoted with double quotes?
      // If so, escape remaining double quotes
      char nextChar = *(s + 1);
      if (nextChar == quoteChar)
      {
        escapeDoubleQuotes = true;
        *buf << quoteChar;
      }
      // If no double quotes, add them.
      else 
      {
        *buf << quoteChar;
      }
    }
  }

  // Add trailing doublequote
  if (eq) 
    *buf << quoteChar;

  return buf->c_str();
}

void CarbonContext::initFredo()
{
  if ((getenv(CRYPT("CARBON_FREDO_VERBOSE")) != NULL) && 
      ! isVSPCompile())
    UtIO::cout() << "Using SPEEDCompiler AMICUS\n";
  mArgs.setBoolValue(CRYPT("-nofoldIfCase"), true);
  mArgs.setBoolValue(CRYPT("-noVectorInference"), true);
  mArgs.setBoolValue(CRYPT("-noUnroll"), true);
  mArgs.setBoolValue(CRYPT("-noTernaryCreation"), true);
  mArgs.setBoolValue(CRYPT("-g"), true);
  mArgs.setBoolValue(CRYPT("-noSeparation"), true);
  mArgs.setBoolValue(CRYPT("-noSampleSchedule"), true);
  mArgs.setBoolValue(CRYPT("-noRewritePriorityEncoders"), true);
  mArgs.setBoolValue(CRYPT("-noRescope"), true);
  mArgs.setBoolValue(CRYPT("-noPackAssigns"), true);
  mArgs.setBoolValue(CRYPT("-noinline"), true);
  mArgs.setBoolValue(CRYPT("-noMergeUnelab"), true);
  mArgs.setBoolValue(CRYPT("-noMergeControl"), true);
  mArgs.setBoolValue(CRYPT("-noMergeBlocks"), true);
  mArgs.setBoolValue(CRYPT("-noLocalCSE"), true);
  mArgs.setBoolValue(CRYPT("-noFunctionalAliases"), true);
  mArgs.setBoolValue(CRYPT("-noFoldCopyPropagation"), true);
  mArgs.setBoolValue(CRYPT("-noFoldBDD"), true);
  mArgs.setBoolValue(CRYPT("-noExtractControl"), true);
  mArgs.setBoolValue(CRYPT("-noConstantPropagation"), true);
  mArgs.setBoolValue(CRYPT("-flatten"), false);
  mArgs.setBoolValue(CRYPT("-noUndrivenConstants"), true);
  
  /*
   * concat rewrite is not an optimization.  It is needed, in
   * test/assign/concat.v and concat2.v, to get the right answer
   *
   * mArgs.setBoolValue(CRYPT("-noConcatRewrite"), true);
   */
  mArgs.setBoolValue(CRYPT("-noCodegenFunctionLayout"), true);
  mArgs.setBoolValue(CRYPT("-noCodeMotion"), true);
  mArgs.setBoolValue(CRYPT("-noClockAliasing"), true);
  mArgs.setBoolValue(CRYPT("-O0"), true);
  mArgs.setBoolValue(CRYPT("-noStripMine"), true);
  
  // I need clock aliasing to work because clockCollapsing is both
  // functional directive and an optimization.  It has to work functionally.
  // Also we need basic constant-clock analysis to avoid race conditions
  // at time 0, which causes spurious sim-diffs in the first few cycles
  // of test/constprop.state11.v, and 2 in test/block-merging.
  BDDContext::cripple();
}

bool CarbonContext::runPrintCbuildBanner()
{
  const char* shortName = "CBUILD";
  const char* longName = NULL;
  longName = CRYPT("Carbon Compiler");
  printBanner(shortName, 
              longName, 
              scCbuildReleaseID);
  return true;
}


void CarbonContext::printBanner(const char * short_name, const char * long_name, const char * version)
{
  if (!mArgs.getBoolValue("-q"))
  {
    mStats->putEchoProgress(true);
#ifdef PATCH_STRING
    UtIO::cout() << "UNOFFICIAL PATCH: " << PATCH_STRING << "\n";
#endif
    UtIO::cout() << 
      "        " << short_name << ", " << long_name << ", Version " << version << "\n" <<
      "\n" <<
      CarbonMem::CarbonGetRightsReservedLine() <<                              
      "\n" <<
      CarbonMem::CarbonGetCopyrightDateLine() <<
      "\n" <<
      CarbonMem::CarbonGetTechSupportLine();
  }
}

// Licensing is getting confusing now that we have multiple features for
// a single program.  The general concept here for licensing is that the
// command line is examined to see if there are VHDL and/or Verilog
// files specified on the command line.  Then, the appropriate license
// is requested--Verilog, VHDL, or mixed.  If a single language is
// specified, and that single-language license is not available, then we
// will attempt to obtain a mixed license.  If both are not available,
// then licensing has failed.  If the user requests license requests to
// be queued, and they are in single-language mode, the
// language-specific license will be queued upon in preference over the
// mixed langauge license.  With Verilog-only, the licensing is just
// that much more complicated, as we have to factor in checking for the
// obsoleted cbuild license feature from pre-summer 2004.
bool CarbonContext::runCheckoutCbuildLicense()
{
  // now checkout a license
  UtString reason;
  if (mArgs.getBoolValue("-licq"))
    mLicense->putBlocking(true);

  // the following includes SystemVerilog files
  bool haveVlogFiles = getFileCollector()->hasVerilogFiles() || getFileCollector()->hasLangLib(LangVer::Verilog);

  bool haveVhdlFiles = getFileCollector()->hasVHDLFiles();

  bool errorFound = false;

  if (haveVhdlFiles)
  {
    if (! mJaguarContext->checkJaguarArgs(getMsgContext()))
      errorFound = true;
  }

  // Check verilog options against cheetah
  HdlFileCollector::const_iterator lib_iter;
  for ( lib_iter = mFileCollector->begin(); lib_iter != mFileCollector->end(); ++lib_iter )
  {
    HdlLib::const_hdlFileIterator file_iter =  (*lib_iter)->begin(LangVer::Verilog);
    for (; file_iter != (*lib_iter)->end(LangVer::Verilog); ++file_iter )
    {
      if(sIsOption(*file_iter)) {
        if (! mCheetahContext->isVerilogOption(*file_iter))
        {
            mMsgContext->NoSuchOption(*file_iter);
            errorFound = true;
        }
      }
    }
  }

  
  if (errorFound)
    return false; // no point in checking out a license
    
  // scan the verilog files. If we only have cems, vhdl files and
  // are compiling with vhdl on top, we only need a vhdl license
  if ((mArgs.isParsed(JaguarContext::scVhdlTop) == ArgProc::eParsed)
      && haveVhdlFiles && haveVlogFiles)
  {
    LangVer supportedVerilogVersions[] = { LangVer::Verilog, LangVer::Verilog2001, LangVer::SystemVerilog2005, LangVer::SystemVerilog2009, LangVer::SystemVerilog2012 };
    bool hasNonCEM = false;
    HdlFileCollector::const_iterator lib_iter;
    for ( lib_iter = mFileCollector->begin(); lib_iter != mFileCollector->end(); ++lib_iter )
    {
      for(UInt32 langIndex = 0; langIndex < sizeof(supportedVerilogVersions)/sizeof(LangVer); ++langIndex) {
        LangVer langVersion = supportedVerilogVersions[langIndex];

        HdlLib::const_hdlFileIterator file_iter =  (*lib_iter)->begin(langVersion);
        for (; (file_iter != (*lib_iter)->end(langVersion)); ++file_iter )
        {
          if(!sIsOption(*file_iter)) {
            const char* ext = strrchr(*file_iter, '.');
            if (! ext) {
              hasNonCEM = true;
              break;
            }
            else if (strcmp(ext, ".cem") != 0) {
              hasNonCEM = true;
              break;
            }
          }
        }
      }
    }

    if (! hasNonCEM)
      // only CEM files, we don't have vlog file for licensing
      // purposes
      haveVlogFiles = false;
  }

  // license BEGIN
  bool ret = true;
  ret = doVSPLicensing();

  if (ret)
  {
    // This is for internal testing purposes only!
    const char* licPollFile = mArgs.getStrValue("-licPollFile");
    if (licPollFile != NULL)
    {
      // Write the file, so test/licq/test_licq knows we've got
      // the license we've wanted.
      {
        UtOBStream pollFile(licPollFile);
        if (! pollFile.is_open() || ! pollFile.close())
          mMsgContext->PollFileFail(licPollFile, pollFile.getErrmsg());
      }
      
      // Now wait for the file to be removed, so we can finish up
      // and release the license
      UtString resultStr;
      while (OSStatFile(licPollFile, "r", &resultStr) == 1)
        OSSleep(1);
    }
  }

  return ret;
  
}

void CarbonContext::reportVSPCheckoutFailure(UtString& reason)
{
  mMsgContext->CmplrVSPLicenseCheckFail("Carbon compiler", reason.c_str());
}

void CarbonContext::reportADChecksumFailure( bool report_as_note, const UtString& reason, const char* fname )
{
  if ( report_as_note ) {
    mMsgContext->ADChecksumNote(reason.c_str(), fname);  
  } else {
    mMsgContext->ADChecksumFailure(reason.c_str(), fname);
  }
}

bool CarbonContext::doVSPLicensing()
{
  UtString vhdlReason, vlogReason;
  UtString errorReason; // generic licensing error msg

  typedef UtArray<UtLicense::Type> LicTypeVec;
  LicTypeVec checkedOut;
  bool result = true;
  
  // Some notes on the ability to override compile and runtime
  // licenses that are checked out.
  //
  // First of all, the switches that allow this can only be used from
  // within a .ef file.  Since those files can only be created by
  // cbuild with an internal license or from within model kit
  // javascript, there's no way a customer could override licensing on
  // their own.
  //
  // The -keyString switch was added (well, repurposed really) to
  // override both the license checked out at compile time and the
  // license checked out at runtime.  Instead of the default
  // crbn_vsp_comp (compile) and crbn_vsp (runtime), crbn_vsp_<string>
  // would be checked out for *both* compilation and runtime.
  // <string> in this case was the value of the -keyString switch.
  //
  // Two new switches, -allowCompileLicense and -allowRuntimeLicense
  // are similar, except for the following points:
  //
  // 1. As indicated by the names, the compile and runtime licenses
  // can be controlled separately.
  //
  // 2. Instead of simply a single string to append to a base license
  // feature name, a comma-separated list can be used.  The special
  // keyword DEFAULT means that the base feature name with no suffix
  // should be used.  The list is ordered, i.e. the first checkout
  // attempt is for the feature created from the first entry in the
  // list, if that fails the second checkout is for the next entry,
  // etc.
  //
  // 3. The base license feature for -allowCompileLicense is the
  // crbn_vsp_comp (the compile feature), not crbn_vsp (the runtime
  // feature).
  //
  // For simplicity, -keyString is now enhanced to accept
  // comma-separated lists of strings, just like the two new switches.
  // Its behavior otherwise remains unchanged.  Note that this is an
  // extension, not a change in behavior, as a single string will
  // still work (it's a comma-separated list of length 1).

  bool haveKeyString = mArgs.isParsed(scKeyString) == ArgProc::eParsed;
  bool haveAllowCompileLicense = mArgs.isParsed(scAllowCompileLicense) == ArgProc::eParsed;
  bool haveAllowRuntimeLicense = mArgs.isParsed(scAllowRuntimeLicense) == ArgProc::eParsed;
  // You can't override the compile or runtime licenses through two means.
  if (haveKeyString && haveAllowCompileLicense) {
    mMsgContext->CmdLineError("-keyString and -allowCompileLicense cannot be used together");
    haveKeyString = false;
    haveAllowCompileLicense = false;
    result = false;
  }
  if (haveKeyString && haveAllowRuntimeLicense) {
    mMsgContext->CmdLineError("-keyString and -allowRuntimeLicense cannot be used together");
    haveKeyString = false;
    haveAllowRuntimeLicense = false;
    result = false;
  }


  // If we have -keyString or -allowCompileLicense, use the requested license(s).
  // otherwise we checkout normal cbuild license
  if (haveKeyString || haveAllowCompileLicense)
  {
    // Which one are we using?
    const char* whichSwitch;
    // Base feature for -keyString is the runtime license.
    // Base feature for -allowCompileLicense is the compile license.
    UtLicense::Type baseType;
    if (haveKeyString) {
      whichSwitch = scKeyString;
      baseType = UtLicense::eVSPRuntime;
    } else {
      whichSwitch = scAllowCompileLicense;
      baseType = UtLicense::eVSPCompiler;
    }
    UtString baseFeature;
    mLicense->getFeatureName(&baseFeature, baseType);

    // Decompose the list of features and try to check one out.
    // Also build a string containing all the attempted features.
    // It will be used in error reporting.
    UtStringArray featureList;
    UtString featureName, licReason, commaSeparatedList;
    const char* suffixList = mArgs.getStrValue(whichSwitch);
    for (UtShellTok tok(suffixList, false, ","); !tok.atEnd(); ++tok) {
      const char* suffix = *tok;
      // Append this to the base feature, unless it's the special
      // keyword DEFAULT.
      featureName = baseFeature;
      if (strcmp(suffix, "DEFAULT") != 0) {
        featureName << "_" << suffix;
      }
      featureList.push_back(featureName);

      // Add to the string list, with a comma if necessary.
      if (commaSeparatedList.size() != 0) {
        commaSeparatedList << ",";
      }
      commaSeparatedList << featureName;
    }

    if (!mLicense->checkoutFeatureNameFromList(featureList, &licReason)) {
      errorReason << "Checkout of " << commaSeparatedList.c_str() << " failed: No feature match for " << suffixList;
      errorReason << " - " << licReason;
      result = false;
      reportVSPCheckoutFailure(errorReason);
    }
  }
  else if (result)
  {
    if (! (mLicense->checkout(UtLicense::eVSPCompiler, &errorReason))) {
      result = false;
      reportVSPCheckoutFailure(errorReason);
    } else {
      checkedOut.push_back(UtLicense::eVSPCompiler);
    }
  }

  if ( result ) {
    if ( mArgs.getBoolValue(scDoADChecksum) ){
      if ( mPreParseData == NULL ) {
        mPreParseData = new PreParseData(this);
      }
      mPreParseData->setRequireADChecksum();

      // if AD checksum checks have been enabled then also consider how they should be reported
      if ( mArgs.getBoolValue(scReportADChecksumAsNote) ){
        mPreParseData->setReportADCheckFailureAsNote();
      }
    }
  }
  
  if (result)
  {
    // If this is a fredo compile do not checkout an opt license
    if (! mArgs.getBoolValue(scFredo))
    {
      // checkout the optimization license
      if (! mLicense->checkout(UtLicense::eVSPCompOpt, &errorReason))
      {
        // No optimization license. See if they specified the non opt
        // switch
        if (mArgs.getBoolValue(scAllowNoOpt))
        {
          // print a note and initialize fredo
          mMsgContext->CmplrRunningWithNoOptimization();
          initFredo();
        }
        else
        {
          // error
          reportVSPCheckoutFailure(errorReason);
          result = false;
        }
      }
      else
        checkedOut.push_back(UtLicense::eVSPCompOpt);
    }
    // else already initialized fredo in runParseCmdLine
  }

  if (result)
  {
    // Allow -dumpVerilog only with
    // crbn_vsp_dumpverilog (or cds_internal) license.
    if (mArgs.getBoolValue(scDumpVerilog) && ! mHaveDiagLicense)
    {
      if (! mLicense->checkout(UtLicense::eDumpVerilog, &errorReason))
      {
        reportVSPCheckoutFailure(errorReason);
        // "Use of -dumpVerilog option requires crbn_vsp_dumpverilog license."
        mMsgContext->NoDumpVerilogLicense();
        result = false;
      }
      else
      {
        // Successful checkout
        checkedOut.push_back(UtLicense::eDumpVerilog);
      }
    }
  }

  // as of 2014/03/13 -sverilog does not require  crbn_vsp_comp_sv (or cds_internal) license.

  if (result)
  {
    // Allow -generateCleartextSource only with
    // crbn_vsp_gencleartextsource (or cds_internal) license.
    if (mArgs.getBoolValue(scGenerateCleartextSource) && ! mHaveDiagLicense)
    {
      if (! mLicense->checkout(UtLicense::eCleartext, &errorReason))
      {
        reportVSPCheckoutFailure(errorReason);
        // "Use of -generateCleartextSource option requires crbn_vsp_gencleartextsource license."
        mMsgContext->NoCleartextLicense();
        result = false;
      }
      else
      {
        // Successful checkout
        checkedOut.push_back(UtLicense::eCleartext);
      }
    }
  }

  if (result)
  {
    // update the UtCustomerDB object.
    UtCustomerDB* custDB = mIODB->getCustDB();
    // The signature contains a tag that determines the custom runtime
    // license feature needed by the model, if requested by the user
    // (via -keyString or -allowRuntimeLicense).  For more detail on
    // those switches, see the comment at the beginning of this
    // function.

    // An empty customer signature leaves the default runtime license
    // behavior unchanged.
    const char* custSigStr = "";
    if (haveKeyString) {
      custSigStr = mArgs.getStrValue(scKeyString);
    } else if (haveAllowRuntimeLicense) {
      custSigStr = mArgs.getStrValue(scAllowRuntimeLicense);
    }
    UtCustomerDB::Signature* custSig = custDB->setCustomerSignature(custSigStr, &errorReason);
    if (! custSig)
    {
      mMsgContext->CustomerDBError(errorReason.c_str());
      result = false;
    } else {
      // If a timebomb was requested, set that here, too.
      result = doTimeBomb(custSig);
    }
  }

  if (result && mArgs.getBoolValue(scVerboseLicensing))
  {
    for (LicTypeVec::const_iterator p = checkedOut.begin(), 
           e = checkedOut.end(); p != e; ++p)
    {
      UtString name;
      mLicense->getFeatureName(&name, *p);
      mMsgContext->CmplrLicenseType(name.c_str());
    }
  }

  return result;
}

bool CarbonContext::runParseAttributes()
{
  // This function adds attribute-value pairs to the DB.  These can
  // come from either user-provided files containing the pairs, or
  // inferred from other command-line options.

  // Before parsing any user-supplied attributes, add the simple CMS
  // version string.
  mIODB->addStringAttribute("CMSVersion", scCbuildReleaseID);

  // And the PGO compiled flag if we are building a PGO model
  if (mCodeGen->isProfileGenerate() || mCodeGen->isProfileUse()) {
    mIODB->addStringAttribute("PGO Build", "true");
  }

  bool success = true;
  ArgProc::StrIter iter;
  if (mArgs.getStrIter(scAttributeFile, &iter) == ArgProc::eKnown) {
    AttributeParser attrParser(mIODB);
    char *file;
    while (success && iter(&file)) {
      success = attrParser.parse(file);
      if (!success) {
        mMsgContext->AttributeFileError(file, attrParser.getError());
      }
    }
  }

  // We can leverage the attribute feature to support other simple
  // command line options that just get passed to the DB.  This means
  // we don't need to add custom read/write code or change the DB
  // version number and capabilities.
  SInt32 dumpSizeLimit;
  if (mArgs.getIntLast(scWaveformDumpSizeLimit, &dumpSizeLimit) == ArgProc::eKnown) {
    UtString attrName("waveformDumpSizeLimit");
    mIODB->addIntAttribute(attrName, dumpSizeLimit);
  }

  return success;
}

bool CarbonContext::runParseDirectives()
{
  // Set up any parse options in the IODB first:
  bool markAllCModelsForPlayback = mArgs.getBoolValue(scPlaybackAllCModels);
  mIODB->putPlaybackAllCModels(markAllCModelsForPlayback);

  // We test here for CARBON_SUPPRESS_CBUILD_ADJ_SYNTH variable.
  // If it is set to 0, and the file /test/adjust_synth.directives exist, we submit that file to MVV.
  // If CARBON_SUPPRESS_CBUILD_ADJ_SYNTH is set to 1 or not set, we skip this step.
  // The reason of doing this is that -synth switch for Cheetah is added now (May 2008). That
  // causes a lot of test cases to fail. This file adjusts the severity of the messages to
  // match the previous behavior during regression testing.
  UtString carbon_home = getenv("CARBON_HOME");
  UtString carbon_suppress_synt_adj = getenv("CARBON_SUPPRESS_CBUILD_ADJ_SYNTH");
  UtString zero = "0";

  if((carbon_home != NULL) && (carbon_suppress_synt_adj != NULL) && (carbon_suppress_synt_adj == zero))
  {
    UtString directives_file;
    UtString adj_file("/test/adjust_synth.directives");
    OSConstructFilePath(&directives_file, carbon_home.getBuffer(), adj_file.getBuffer());
    UtString reason;
    OSStatEntry  sysFileEntry;
    int file_exists = OSStatFileEntry(directives_file.getBuffer(), &sysFileEntry, &reason);
    if(file_exists == 0)
    {
      mIODB->parseFile(directives_file.c_str(), directives_file.c_str());    
    }
  }

  ArgProc::StrIter iter;
  ArgProc::StrIter niter;
  UtString directivesLogFilename(mFileRoot);
  directivesLogFilename << ".dir";
  bool ret = mIODB->openLogFile(directivesLogFilename.c_str());
  if (ret) {
    if (mArgs.getStrIter("-directive", &iter) == ArgProc::eKnown) {
      mArgs.getUnexpandedStrIter("-directive", &niter);
      char* file = NULL;
      char* nfile = NULL;
      for (; iter(&file);) {
        if (!niter.atEnd()) {
          // if -directive specifications  appeared in CARBON_CBUILD_ARGS then mArgs.getUnexpandedStrIter
          // will return an iterator over a smaller set than mArgs.getStrIter.  The two iters are in sync
          // for all the items that niter includes, after that we will have to use iter/file (the expaned version) by itself
          niter(&nfile);
        } else {
          nfile = NULL;
        }
        (void) mIODB->parseFile(file, nfile);

        // we depend on setting the MsgContext error code to avoid
        // advancing to the next phase, rather than the return value
        // from parseFile.  That way directives Alerts can be demoted
        // to warnings and the compilation can proceed
      }
    }
  }

  // We have to wait to check the arguments for codegen until after
  // directives parsing so we can use directives such as silentMsg.
  if (! mCodeGen->checkArgs())
    ret = false;

  return ret;
}

bool
CarbonContext::runParseLanguages()
{
  bool usingVerific = mArgs.getBoolValue(scUseVerific);

  // Create a database attribute that identifies the
  // parser being used. This is useful, for example,
  // when modelkits need to do things a little differently
  // depending on the parser. (0=>Interra, 1=>Verific)
  UtString attrName("parserNumber");
  mIODB->addIntAttribute(attrName, usingVerific ? 1 : 0);

  if ( usingVerific ) {
    return runVerificParseLanguages();
  } else {
    return runInterraParseLanguages();
  }
}

bool
CarbonContext::runInterraParseLanguages()
{
  // ALWAYS have Jaguar determine the case mode. DesignPopulate needs
  // to be initialized with the specified or default vhdlCase. This
  // allows verilog-on-top to work properly.
  mJaguarContext->determineCaseMode();

  bool retval = true;
  mvvNode mvvRoot = NULL;

  // Set various pre-init Cheetah, Jaguar, and MVV parameters

  // Cheetah allocates "nondump" memory (aka the Temp plane) whenever you
  // run an API function that returns a string or an iterator.  All that
  // memory gets freed with veFreeMemory so the Carbon leak detection is
  // bypassed and will not help you discover bloating that happens during
  // nucleus population.  By uncommenting the following line you make
  // Cheetah call 'operator new' for every temp (nondump) allocation
  // and hence we can track it with memory histogramming.  Unfortunately
  // Cheetah leaks nondump memory during initialization in this mode
  // that does not get freed by veFreeMemory, so our leak detection will
  // be a little too aggressive and our tests will fail with this uncommented.
  // But you can still uncomment this and use memory histogramming to find
  // the places where *we* are leaking.
  if (getArgs()->getBoolValue(sCheetahMemLeaks)) {
    veDisableMemMgr(VE_NONDUMP_PLANE_USE_SYSMEM);
  }

  const int memChunkSize = 65536;
  veRegisterProtectedInputFilter(mCheetahContext, CheetahContext::decrypt);
  veDisableLicenseCheck(790257312L, 170035984L, 560117349L);
  VeSetMemoryAllocationByNew(VE_TRUE);
  VeSetMemoryAllocationChunkSize( memChunkSize ); // see util/MemSystem.cxx
  veDisableSignalHandling( VE_TRUE );

  // Set various pre-init Jaguar parameters
  vhJaguarHomeUndefined(VH_TRUE);
  vhDisableJaguarLicenseCheck(87642362, 76243873, 43908435);
  vhDisableSignalHandling( VH_TRUE );
  vhDefineLibraryMappings(VH_TRUE);
  vhReturnMaximumSize(VH_TRUE); // To get the expr size corresponding to funccall
  vhSetMemoryAllocationByNew(VH_TRUE);
  vhSetMemoryAllocationChunkSize( memChunkSize ); // see util/MemSystem.cxx
  vhEnableEscapeSeqChar(VH_TRUE); // for messagecallback, tell jaguar to escape special chars
  vhDetectGatedClock( VH_TRUE );

  // Depending on the command line args, these variables control the
  // parser initialization and language parsing.
  bool libsOnCmdLine    = getFileCollector()->librarySpecifiedOnCmdLine();
  bool hasVerilogFiles  = getFileCollector()->hasHdlFiles(LangVer::Verilog) || getFileCollector()->hasHdlFiles(LangVer::Verilog2001);// for interra there are only 2 Verilog language versions
  bool processVlogFiles = ( hasVerilogFiles || libsOnCmdLine );
  bool processVhdlFiles =
      getFileCollector()->hasHdlFiles(LangVer::VHDL87)
      ||
      getFileCollector()->hasHdlFiles(LangVer::VHDL93)
      ||
      libsOnCmdLine;

  if ( (mArgs.getBoolValue(JaguarContext::scVhdlCompile) == true) && (processVlogFiles == true) )
  {
    // In VHDL compile-only mode, we ignore any Verilog files.
    processVlogFiles = false;
    mMsgContext->IgnoringVerilogFiles( JaguarContext::scVhdlCompile );
  }

  // Set various pre-init MVV memory management parameters
  mvvEnableMvvMemoryManager(MVV_TRUE);
  mvvSetMemoryAllocationByNew(MVV_TRUE);
  mvvUseSystemMemory(MVV_FALSE);
  mvvSetMemoryAllocationChunkSize( memChunkSize ); // see util/MemSystem.cxx

  if (processVlogFiles == false && processVhdlFiles == true)
  {
    // For the pure VHDL case we will not follow mvv case type (VH_ASIS), we will go in for Jaguar default behavior (VH_UPPER)
    mvvKeepVhdlNamesInUpperCase(MVV_TRUE); // must be called before mvvInit
  }

  if (hasVerilogFiles == true && libsOnCmdLine == false && processVhdlFiles == false)
  {
    // If the design is pure verilog one there is no need to do redundant mixed-mode stuff.
    // However, if precompiled verilog files are present, setting this to true prevents
    // the autoloading of modules in those precompiled libraries (bug 7602).
    mvvDesignIsVerilogOnly(MVV_TRUE); // must be called before mvvInit
  }

  // bug7063 - allow an identifier "INPUT" in a verilog file compiled with -u
  veKeepKeywordsCaseSensitiveWithOptionU(VE_TRUE);

  if ( mvvInit( 0, NULL, &mvvRoot ))
  {
    getMsgContext()->MVVInitFailure();
    return false;
  }

  // set various post mvvInit variables

  // Set up MVV message handling
  mvvRegisterMessageHandler( mvv_spew, NULL, 0 );
  if (mArgs.getBoolValue("-showParseMessages")) {
    showMVVStatus = true;
  }

  // Handle -L and -Lf command line options.
  ArgProc::StrIter iter;
  if (mArgs.getStrIter(scLf, &iter) == ArgProc::eKnown) {
    while (!iter.atEnd()) {
      mvvSetFirstSearchLibList(*iter);
      mvvEnableModelSimCompliantLibSearch(MVV_TRUE);
      ++iter;
    }
  }
  if (mArgs.getStrIter(scL, &iter) == ArgProc::eKnown) {
    while (!iter.atEnd()) {
      mvvSetSearchLibList(*iter);
      mvvEnableModelSimCompliantLibSearch(MVV_TRUE);
      ++iter;
    }
  }

  // Disable black box creation, when the master of instance is undefined.
  mvvAllowBlackBoxCreation(MVV_FALSE);

  // Change the severities of MVV message , if needed.
  mMVVContext->adjustSeverities();

  // Set up pre-parse callback handling. This is used to deal with
  // licensing of ip in single language environments
  if (mPreParseData)
  {
    mvvRegisterPreParseFileHandler(PreParseData::sPreParseCB, MVV_VERILOG, mPreParseData);
    mvvRegisterPreParseFileHandler(PreParseData::sPreParseCB, MVV_VHDL, mPreParseData);
  }

  // allow parser to find verilog modules with escaped names in library files that do
  // not have escaped file names.  test/bugs/bug2589
  veSetToolsCompatibility(VE_TRUE);

  // Call Cheetah/Jaguar parsers.
  if ( processVlogFiles == true) {
    if (!mCheetahContext->parseVerilog( )) {
      retval = false;
    }
  }

  if ( retval == true && processVhdlFiles == true ) {
     if (!mJaguarContext->parseVhdl( )) {
       retval = false;
     }
  }

  // We don't want semantic check after elaboration. If we call this PI then we
  // are ignoring some information. If the port range is dependent on
  // verilog parameter value and that does not match after elaboration, we won't
  // get any WARNING.
  mvvDisableElabTimeSemCheckForVlogVlog(MVV_TRUE);

  // tell mvv that we want to see different messages when the user has
  // explicitly left a port disconnected
  mvvDifferentWarnMsgForExplicitPortDisconnect ( MVV_TRUE);

  mvvEnableEscapeSeqChar(MVV_TRUE); // for messagecallback, tell mvv to escape special chars
  
  if ( retval ){
    retval = applyCustomerTag();
  }
  
  if (isLibraryCompileOnly())
    freeInterraMemory();

  return retval;
}


// examine the 'unprocessed' command line arguments for "-synth_prefix" or "-pragma_prefix", and for each of them setup the necessary verific pragma triggers
// historical note: the -synth_prefix type arguments were in the 'unprocessed' group because interra parsers could handle them.  When
// switching to Verific parsers they were left in the 'unprocessed' group for 2 reasons: 1) minimize impact on the interra based flow and 2)
// it is unclear how these switches would be handled by verific flow if they were buried in multiple levels of include files (-f filename).
// When you try to convert them to 'processed' command line args make sure -synth_prefix definitions within included (-f) files.
void CarbonContext::handleVerificSynthPrefix()
{
  ArgProc* args = getArgs();
  UtStringArgv &unprocOpts = args->lookupUnprocessedGroupOccurrences( "Verilog" );
  int uargc = unprocOpts.getArgc();
  char** uargv = unprocOpts.getArgv();

  // process the -synth_prefix Verilog args that were seen on the command line (or -f files)
  UtVector<UtString> pragmaTriggers; // collection of all triggers seen
  for ( int i = 0; i < uargc; ++i )
  {
    UtString buf(uargv[i]);
    // if -synth_prefix or then make the call
    if (( buf == "-synth_prefix" ) || ( buf == "-pragma_prefix" )) {
      ++i;
      // Process the options -synth_prefix and -pragma_prefix (they are synonyms)
      UtString prefixStr(uargv[i]);
      pragmaTriggers.push_back(prefixStr);
    }
  }
  if ( ! pragmaTriggers.empty() ) {
    mVerificDesignManager.setPragmaTriggers(pragmaTriggers, true);
  }
}

void CarbonContext::handleVerificTranslatePragmas() 
{
  ArgProc* args = getArgs();
  UtStringArgv &unprocOpts = args->lookupUnprocessedGroupOccurrences( "Verilog" );
  int uargc = unprocOpts.getArgc();
  char** uargv = unprocOpts.getArgv();
  bool translatePragmas = false;
  // process the -ignore_translate_pragmas or -no_translate Verilog args that were seen on the command line (or -f files)
  UtVector<UtString> pragmaTriggers; // collection of all triggers seen
  for ( int i = 0; i < uargc; ++i )
  {
    UtString buf(uargv[i]);
    //  -no_translate|-ignore_translate_pragmas
    if (( buf == "-ignore_translate_pragmas" ) || ( buf == "-no_translate" )) {
      ++i;
      // Process the options -ignore_translate_pragmas and -no_translate (they are synonyms)
      translatePragmas = true;
    }
  }
  if (  translatePragmas ) {
     mVerificDesignManager.setIgnoreTranslatePragma(1);
  }

}

bool CarbonContext::runVerificParseLanguages()
{
    analyzeAndRegisterValueForVhdlExt();
    //mMsgContext->Verific2NUInfoNoLoc("START: runVerificParseLanguages ");
    //mvvDesignIsVerilogOnly(MVV_TRUE); // must be called before mvvInit
    //mvvDisableElabTimeSemCheckForVlogVlog(MVV_TRUE);
    //mvvDifferentWarnMsgForExplicitPortDisconnect ( MVV_TRUE);
    //mvvEnableEscapeSeqChar(MVV_TRUE); 
    //mMsgContext->suppressAllNotes(true);
    //mMsgContext->suppressAllWarnings(true);

  mVerificContext->processCommandLineSwitches(); // must come before adjustMessageText() and adjustSeverities()

  mVerificContext->adjustMessageText();
  mVerificContext->adjustSeverities();
  // Set Verific Message limit to a high number (default is 20)
  // This keeps Verific from suppressing messages.
  Verific::Message::SetMaxErrorCount(1000);
  




  // RJC RC#10, can we not clutter up runVerificParseLanguages with the handling of command line options,
  // RJC I would suggest a refactor to call these methods here and put the looping over the mArgs iterator in the individual methods:
  // RJC handleVerificSynthPrefix();
  // RJC setTopLevelParameters()  or handleTopLevelParameters()
  // TODO needs refactoring
  // ADDRESSED FD: Some of this code will go away as soon as I get confirm if .cmd file is always created, so leaving hear TODO: to refactor the code during command-line processing refactoring
  
  // Process the VHDL options -vhdl_synth_prefix command line arg
  ArgProc::StrIter iter1;
  bool known1 = mArgs.getStrIter("-vhdl_synth_prefix", &iter1) == ArgProc::eKnown;
  UtString msg1("-vhdl_synth_prefix");
  msg1 += " switch not known";
  INFO_ASSERT(known1, msg1.c_str());
  UtVector<UtString> pragmaTriggers1;
  for (char* ptVal; iter1(&ptVal);) {
    pragmaTriggers1.push_back(UtString(ptVal));
  }
  mVerificDesignManager.setPragmaTriggers(pragmaTriggers1, false);
  handleVerificSynthPrefix();
  //handel for -ignore_translate_pragmas or -no_translate command line switch
  handleVerificTranslatePragmas();
  // Process the -topLevelParam options
  // rjc this code should be moved to VerificContext::setTopLevelParameters
  // ADDRESSED FD: I see the point. So far the strategy was to keep Carbon specific data in CarbonContext.cxx and verific2nuclues data in VerificDesignManager now arg processing needs to be moved into VerificDesignManager, 
  ArgProc::StrIter iter3;
  bool known3 = mArgs.getStrIter(scTopLevelParam, &iter3) == ArgProc::eKnown;
  UtString msg3(scTopLevelParam);
  msg3 += " switch not known";
  INFO_ASSERT(known3, msg3.c_str());
  UtMap<UtString, UtString> paramValPairs;
  for (char* paramVal; iter3(&paramVal);) {
    UtString paramValStr(paramVal);
    size_t equal = paramValStr.find('=');
    if (equal == UtString::npos) {
      mMsgContext->TopLevelParamMissingAssignment(paramValStr.c_str());
      return 0;
    }
    UtString param = paramValStr.substr(0, equal);
    UtString val = paramValStr.substr(equal+1);
    paramValPairs[param] = val;
  }
  mVerificDesignManager.setTopLevelParameters(paramValPairs);
  // Read cmd file
  UtString filelist;
  // May need mFileRoot.ncmd (non-expanded version). TODO need to check with Rich
  filelist << mFileRoot << ".cmd";
  bool usingVerific = mArgs.getBoolValue(scUseVerific);
  INFO_ASSERT(usingVerific, CRYPT("This method should only be used if -useVerific specified on command line"));
  int result = 0;
  bool v2001_mode = mArgs.getBoolValue("-2001");
  bool vhdl87_mode = mArgs.getBoolValue("-87");
  bool vhdl93_mode =  mArgs.getBoolValue("-93");
  
  // -vhdlCompile stops after parsing the VHDL.
  // -compileLibOnly stops cbuild after parsing HDL (VHDL/Verilog) and dumping
  // it in the specified/default libraries.
  bool compileOnly = false;
  if (isLibraryCompileOnly()) {
      compileOnly = true;
  }
  UtString vhdl_top =  UtString(mArgs.getStrValue( "-vhdlTop" ));
  UtString vlog_top =  UtString(mArgs.getStrValue( "-vlogTop" ));
  bool autoCompileOrder = mArgs.getBoolValue(JaguarContext::scAutoCompileOrder); // no need to get both autoCompileOrder and noAutoCompileOrder values since one is a boolOveride for the other 
  bool sv_mode = mArgs.getBoolValue(CarbonContext::scSVerilog);
  LangVer langVer = 0;
  mVerificDesignManager.setMsgContext(mMsgContext);
  INFO_ASSERT(filelist.c_str(), "Incorrect empty filelist filename");
  if ( sv_mode ) {
    langVer = LangVer::SystemVerilog2012; // user specified -sverilog command line option
  } else if (v2001_mode) {
      langVer = LangVer::Verilog2001; // user specified a -v2k type command line option
  } else if (0 != vhdl_top.length()) {
      mVerificDesignManager.setVhdlTop(vhdl_top);
      if(vhdl87_mode) {
         langVer = LangVer::VHDL87;
      } else if(vhdl93_mode) {
         langVer = LangVer::VHDL93;
      } else {
         langVer = LangVer::VHDL93; // default is VHDL93
      }
  } else {
      langVer = LangVer::Verilog; // default is Verilog 95
  }
  if ( ! vlog_top.empty()) {
      mVerificDesignManager.setVlogTop(vlog_top);
  }

  //Check for -vhdlNoWriteLib
  bool hasVhdlNoWriteLib = false;
  hasVhdlNoWriteLib = (getArgs()->isParsed("-vhdlNoWriteLib") == ArgProc::eParsed);
  //Check for -vlogLib
  bool hasVerilogLib = false;
  hasVerilogLib = (getArgs()->isParsed("-vlogLib") == ArgProc::eParsed);
  //Check for -vhdlLib
  bool hasVhdlLib = false;
  hasVhdlLib = (getArgs()->isParsed("-vhdlLib") == ArgProc::eParsed);
  //Check for -libmap
  bool hasLibmap = false;
  hasLibmap = (getArgs()->isParsed(scLibMap) == ArgProc::eParsed);

  //check -vhdlCase option
  checkVhdlCase();
  

  // Check command-line 
  bool libsOnCmdLine    = getFileCollector()->librarySpecifiedOnCmdLine();
  bool hasVerilogFiles  = getFileCollector()->hasHdlFiles(LangVer::Verilog) || getFileCollector()->hasHdlFiles(LangVer::Verilog2001) || getFileCollector()->hasHdlFiles(LangVer::LangVer::SystemVerilog2012);// for interra there are only 2 Verilog language versions
  bool processVlogFiles = ( hasVerilogFiles || libsOnCmdLine );
  if ( (mArgs.getBoolValue(JaguarContext::scVhdlCompile) == true) && (processVlogFiles == true) )
  {
    // In VHDL compile-only mode, we ignore any Verilog files.
    processVlogFiles = false;
    mMsgContext->IgnoringVerilogFiles( JaguarContext::scVhdlCompile );
  }
  if ( processVlogFiles == true) {
    bool ok = mVerificContext->parseVerilogArgs();
    if (!ok) return 0;
  }

  // Process filelist using Verific XL compilance , Analyze/Elaborate
  result = callVerificParserAndStaticElaboration(langVer,autoCompileOrder,compileOnly, hasVerilogLib, hasVhdlLib, hasLibmap, hasVhdlNoWriteLib);

  // Doing post-elaboration adjustments, those are typically cases not supported by Verific static elaboration
  // we may wish to make separate phase for this routine in the future, instead of calling it here

  bool usingCarbonStaticElaboration = mArgs.getBoolValue(CarbonContext::scEnableCSElab);
  if ( usingCarbonStaticElaboration ) {
    result &= callCarbonStaticElaboration();
  }

  if ( result ){
    result = applyCustomerTag();
  }

  //mMsgContext->Verific2NUInfoNoLoc("END: runVerificParseLanguages ");
  return result;
}

bool CarbonContext::callCarbonStaticElaboration()
{
  bool result = true;
  //get the top level verilog module
  Verific::VeriModule * topModule = mVerificDesignManager.GetTopModule();
  //get the top level vhdl module
  Verific::VhdlPrimaryUnit * topEntity = mVerificDesignManager.GetTopEntity();
  // calling carbon static elaborator 
  verific2nucleus::CarbonStaticElaborator cse(topModule, topEntity, this);
  result = cse.elaborate();
  return result;
}

bool CarbonContext::runDumpHierarchy() {
  bool usingVerific = mArgs.getBoolValue(scUseVerific);
  if ( usingVerific ) {
      return runVerificDumpHierarchy();
  } else {
      return runInterraDumpHierarchy();
  }
}

bool CarbonContext::runVerificDumpHierarchy() {
    // Depending on the command line args, these variables control the
    // parser initialization and language parsing.

    bool libsOnCmdLine    = getFileCollector()->librarySpecifiedOnCmdLine();
    //bool hasVerilogFiles  = getFileCollector()->hasHdlFiles(LangVer::Verilog) || getFileCollector()->hasHdlFiles(LangVer::Verilog2001) || getFileCollector()->hasHdlFiles(LangVer::LangVer::SystemVerilog2012);// for interra there are only 2 Verilog language versions
    //bool processVlogFiles = hasVerilogFiles || libsOnCmdLine;
    bool processVhdlFiles =
        getFileCollector()->hasHdlFiles(LangVer::VHDL87)
        ||
        getFileCollector()->hasHdlFiles(LangVer::VHDL93)
        ||
        libsOnCmdLine;
    bool retval = true;

    // Skip mixed and VHDL cases
    if (processVhdlFiles)
    {
        return retval;
    }

    // Check if the user has decided to skip the dump hierarchy stage
    if ( mArgs.getBoolValue( scNoDesignHierarchy ) ) {
        return retval;
    }

    //bool vhdlOnTop = ( mArgs.getStrValue( "-vhdlTop" ) != NULL );
    bool vlogOnTop = ( mArgs.getStrValue( "-vlogTop" ) != NULL );

    Verific::VeriModule* topPrimary = NULL;

    mMsgContext->suppressAllNotes(true);
    mMsgContext->suppressAllWarnings(true);

    if ( vlogOnTop == true ) {
        // populate Verilog on top
        topPrimary = mVerificDesignManager.GetTopModule();
    }

    UtString fileName(mFileRoot);
    fileName << ".designHierarchy";

    // Create callback
    verific2nucleus::VerilogDumpHierarchyCB verilogDumpHierarchyCB(fileName, getIODB(), mVerificTicProtectedNameMgr);

    // Setup walker
    Verific::VerilogDesignWalker verilogDumpHierarchyWalk;
    // Add callback to walker
    verilogDumpHierarchyWalk.sCallBack(&verilogDumpHierarchyCB);

    if ( topPrimary != NULL ) {    
        // Walk the design from the top level design unit or module    
        topPrimary->Accept(verilogDumpHierarchyWalk);
    } else {
        // Walk all the verilog modules 
        // (in all libraries probably)
        //In case we need work lib only
        //Verific::VeriLibrary* workLib = Verific::veri_file::GetWorkLib(); // we have this correctly set in VerificDesignManager
        Verific::VeriLibrary* lib;
        Verific::MapIter mi; // 1, mi2;
        FOREACH_VERILOG_LIBRARY(mi, lib) {
            //std::cerr << "Lib: " << lib->GetName() << std::endl;
            Verific::Array* allTopModules = Verific::veri_file::GetTopModules(lib->GetName()); // get all top modules from 'lib'
            unsigned i = 0;
            Verific::VeriModule* mod;
            FOREACH_ARRAY_ITEM(allTopModules, i, mod) {
                mod->Accept(verilogDumpHierarchyWalk);
            }
            // Free top module array (if any).
            delete allTopModules;
        }
    }

    // Create the design hierarchy file
    verilogDumpHierarchyCB.dumpXml();

    mMsgContext->suppressAllNotes(false);

    // If the -w option was specified, don't unsuppress Warnings.
    if (!mArgs.getBoolValue("-w")) {
        mMsgContext->suppressAllWarnings(false);
    }

    if ( mArgs.getBoolValue( scScan ) ) {
        // If just scanning, stop right away
        exit(0);
    }

    return retval;
}

bool CarbonContext::runInterraDumpHierarchy() {
  bool retval = true;

  // Check if the user has decided to skip the dump hierarchy stage
  if ( mArgs.getBoolValue( scNoDesignHierarchy ) ) {
    return retval;
  }

  bool vhdlOnTop = ( mArgs.getStrValue( "-vhdlTop" ) != NULL );
  bool vlogOnTop = ( mArgs.getStrValue( "-vlogTop" ) != NULL );

  //AF: hack...
  //is use Verific is set turn off vhdlOnTop flag
  if (mArgs.getBoolValue(scUseVerific))
    vhdlOnTop=false;

  mvvNode topPrimary = NULL;
  mvvNode topArchitecture = NULL;
  mvvNode topBlkCfg = NULL;

  mMsgContext->suppressAllNotes(true);
  mMsgContext->suppressAllWarnings(true);

  if ( vlogOnTop == true ) {
    // populate Verilog on top
    topPrimary = mCheetahContext->getTopModule();

    if ( mArgs.isParsed( scTopLevelParam ) == ArgProc::eParsed ) {
      // Top level parameters have been specified. 
      // Override the existing parameter values in the top level module
      if ( !mCheetahContext->setTopLevelParameters( topPrimary->castVeNode() ) ) {
        return false;
      }
    }
  }
  else if ( vhdlOnTop == true ) {
    // populate VHDL on top
    mJaguarContext->getTopDesignUnit( &topPrimary, &topArchitecture, &topBlkCfg );

    if ( mArgs.isParsed( scTopLevelParam ) == ArgProc::eParsed ) {
      // Top level generics have been specified. 
      // Override the existing generic values in the top level design unit
      if ( !mJaguarContext->setTopLevelGenerics( topPrimary->castVhNode() ) ) {
        return false;
      }
    }
  }

  // Get the case for the VHDL side of things
  VHDLCaseT vhdlCaseSpec = mJaguarContext->getCase();

  UtString fileName(mFileRoot);
  fileName << ".designHierarchy";

  DumpHierarchyCB dumpHierarchyCB( fileName, mTicProtectedNameMgr, vhdlCaseSpec, getIODB());

  InterraDesignWalker dumpHierarchyWalk( &dumpHierarchyCB, mTicProtectedNameMgr );

  if ( topPrimary != NULL ) {    
    // Walk the design from the top level design unit or module    
    (void)dumpHierarchyWalk.design( topPrimary, 
                                    topArchitecture, 
                                    topBlkCfg,
                                    mJaguarContext->getLogicalWorkLib() );    
  }
  else {

    // Walk all the verilog modules
    veNode root = veCheetahGetRoot();

    if (root != NULL) {
      // There are Verilog modules to walk
      CheetahList ve_iter( veRootGetTopLevelModuleUdpList( root ) );
      veNode topLevelModule; 
      while ( ( topLevelModule = veListGetNextNode( ve_iter ) ) ) {

        if ( mArgs.isParsed( scTopLevelParam ) == ArgProc::eParsed ) {
          // Top level parameters have been specified. 
          // Override the existing parameter values in the top level module
          if ( ! mCheetahContext->setTopLevelParameters( topLevelModule ) ) {
            return false;
          }
        }
        
        // Walk each top level module
        (void)dumpHierarchyWalk.design( topLevelModule, 
                                        topArchitecture, 
                                        topBlkCfg,
                                        mJaguarContext->getLogicalWorkLib() );    
        
      }
    }
    
    // Walk all the VHDL entities
    JaguarString workLib(mJaguarContext->getLogicalWorkLib());

    if (workLib != NULL) {
      JaguarList entities = vhGetAllEntities( workLib );
      JaguarString entityName;
      
      // Doing all this because the call to vhGetAllEntities() does not guarantee
      // the order of the entities that it returns.
      UtSet<UtString> entityNames;
      while ( ( entityName = ( (const char*) vhGetNextItem( entities ) ) ) ) {
        entityNames.insert( UtString( entityName ) );
      }
      
      for (UtSet<UtString>::iterator iter = entityNames.begin(); iter != entityNames.end(); ++iter) {
        
        vhNode entity = vhOpenEntity( workLib, (*iter).c_str() );
        JaguarList architectures = vhGetAllArchitectures( workLib, (*iter).c_str() );
        JaguarString architectureName;
        
        while ( ( architectureName = ( ( const char* ) vhGetNextItem( architectures ) ) ) ) {
          
          vhNode architecture = vhOpenArch( workLib, (*iter).c_str(), architectureName );
          // Walk each top level entity
          (void)dumpHierarchyWalk.design( static_cast<mvvNode>(entity), 
                                          static_cast<mvvNode>(architecture), 
                                          NULL,
                                          workLib );
        }
      }
    }
  }

  // Create the design hierarchy file
  dumpHierarchyCB.dumpXml();

  mMsgContext->suppressAllNotes(false);

  // If the -w option was specified, don't unsuppress Warnings.
  if (!mArgs.getBoolValue("-w")) {
    mMsgContext->suppressAllWarnings(false);
  }
   
  if ( mArgs.getBoolValue( scScan ) ) {
    // If just scanning, stop right away
    exit(0);
  }

  return retval;
}

bool CarbonContext::freeInterraMemory() {
  mIODB->closeLogFile();

  // If we were writing a tokens file (see CheetahContext::genTokenFile)
  // then we should also copy the directives file into the token subdir
  UtString directivesLogFilename(mFileRoot);
  directivesLogFilename << ".dir";
  bool ret =
    mCheetahContext->copyDirectivesFileToTokenDir(directivesLogFilename.c_str());

  mvvFreeMemory(); // Must be called before vhFreeJaguarMemory() and veFreeMemory()
  vhFreeJaguarMemory();
  vhFreeJaguarLicense();
  veFreeMemory();
  veFreeLicense();
  CarbonMem::releaseBlocks();
  return ret;
}


bool
CarbonContext::runPopulateNucleus()
{
  bool usingVerific = mArgs.getBoolValue(scUseVerific);
  if ( usingVerific ) {
    return runVerificPopulateNucleus();
  } else {
    return runInterraPopulateNucleus();
  }
}

bool
CarbonContext::runVerificPopulateNucleus()
{
  //mMsgContext->Verific2NUInfoNoLoc("START: runVerificPopulateNucleus ");


  mVerificDesignManager.setMsgContext(mMsgContext);
  //Initial status
  bool buildStatus=false;
  //get the top level verilog module
  Verific::VeriModule * topModule = mVerificDesignManager.GetTopModule();
  //get the top level vhdl module
  Verific::VhdlPrimaryUnit * topEntity = mVerificDesignManager.GetTopEntity();
  //construct the builder
  /* mAtomicCache, mSourceLocatorFactory, mSymbolTable, mNetRefFactory, mIODB, mMessageContext
     all taken from carbon context */

  // Keeps elaborated/unelaborated symbol tables to hold design data like
  // directives, user design type information for visibility etc.
  IODBDesignDataSymTabs* symTabs = new IODBDesignDataSymTabs(getAtomicCache());
  bool synthSpecifiedOnCmdLine = mArgs.getBoolValue("-synth");

    verific2nucleus::VerificNucleusBuilder* verificNucleusBuilder = 
    new verific2nucleus::VerificNucleusBuilder(mDesign, 
                                               mAtomicCache, 
                                               mSourceLocatorFactory, 
                                               mSymbolTable, 
                                               mNetRefFactory, 
                                               mIODB,
                                               mMsgContext,
                                               this,
                                               synthSpecifiedOnCmdLine,
                                               scModulePragmas,
                                               scNetPragmas,
                                               scFunctionPragmas,
                                               symTabs,
                                               &mArgs,
                                               mVerificTicProtectedNameMgr);

  // Set nucleus builder reference for V2NDesignScope
  verific2nucleus::V2NDesignScope::sNucleusBuilder(verificNucleusBuilder);

  // Setup vhdl walker
  verific2nucleus::VerificVhdlDesignWalker verificVhdlDesignWalker(verificNucleusBuilder);

  // Setup verilog walker
  // Getting output system task enable switch (this affects only to the following list :  $display $fdisplay $write $fwrite $fclose $fflush
  bool doingOutputSysTasks = mArgs.getBoolValue ("-enableOutputSysTasks");
  bool inlineTasks = mArgs.getBoolValue(CRYPT("-inlineTasks"));
  // Disallows references to hierarchical objects. In Interra flow, this
  // seems to only disallow hier refs to tasks.
  bool allow_hierrefs = not mArgs.getBoolValue(sDisallowHierRefs);
  bool warnForSysTask = (mArgs.isParsed("-warnForSysTask") == ArgProc::eParsed);
;
  int rjcDebugValue = 0;
  (void) mArgs.getIntLast(CRYPT("-rjcDebug"), &rjcDebugValue);
  verific2nucleus::VerificVerilogDesignWalker verificVerilogDesignWalker(verificNucleusBuilder, getFileRoot(), mMsgContext, allow_hierrefs, inlineTasks, doingOutputSysTasks, warnForSysTask, rjcDebugValue);

  // Initialize Common Utility Class with all objects we got 
  verific2nucleus::Verific2NucleusUtilities::InitObjects(verificNucleusBuilder, &mVerificDesignManager,
          &verificVerilogDesignWalker, &verificVhdlDesignWalker);
  //verilog on top
  if (topModule){
      // Need to do this for verilog specifically, vhdl is using different version;
      verificNucleusBuilder->sUserTypesAtomicCache();
      //topModule->Accept(verilogDesignWalker);
      // the entry point is VERI_VISIT(VeriModule, node)
      topModule->Accept(verificVerilogDesignWalker);
      //mSymbolTable->print();
      // Don't even try this if has error 
      if (! verificNucleusBuilder->hasError()) {
          // Setup symbol table build walker (verilog only)
          verific2nucleus::VerificVerilogSTBuildWalker verificVerilogSTBuildWalker(verificNucleusBuilder, verificVerilogDesignWalker.gNameRegistry(), verificVerilogDesignWalker.gDeclarationRegistry());

          // the entry point for Symbol table build walker; 
          topModule->Accept(verificVerilogSTBuildWalker);
          ///STSymbolTable* designElaboratedSymTab   = (STSymbolTable*)symTabs->getElabSymTab();
          ///designElaboratedSymTab->print();
          ///STSymbolTable* designUnelaboratedSymTab = (STSymbolTable*)symTabs->getUnelabSymbolTable();
          ///designUnelaboratedSymTab->print();
          // doing this here as VHDL has it's own version
          getIODB()->setDesignDataSymbolTables(symTabs);
          getIODB()->populateDesignDirectives();
      }

      //if (verificNucleusBuilder->hasError()) {
      //  getMsgContext()->PopulationFailure();
      //}
      //topModule->Accept(verilogDesignWalker);

      //RJC RC#2013/08/09 (tigran) why are we setting up to return true here instead of the status returned by the walker?
      buildStatus = true;
  }
  //vhdl on top
  else {
    delete symTabs; // vhdl is using it's own version of symbol table
    topEntity->Accept(verificVhdlDesignWalker);

    if (verificNucleusBuilder->hasError()) {
      return false;
    }

    if (! verificVhdlDesignWalker.gEntityVisit()) {
        SourceLocator loc = verific2nucleus::Verific2NucleusUtilities::gSourceLocator(*verificNucleusBuilder,topEntity->Linefile());
        mMsgContext->Verific2NUDesignNotFound(&loc, topEntity->Name());
    }
        

    /**
     * Build symbol table
     *
     * Symbol table building process consists of in 3 phase
     *   1. VerificPrePopInfoVisit - create skeleton of symbol table, create scope branches
     *   2. STBuildWalkCB - add net leafs to the symbol table
     *   3. UserTypePopulate - add user type information to the symbol table
     */
    verific2nucleus::VerificPrePopInfoWalker prePopInfo(verificNucleusBuilder, verificVhdlDesignWalker.getNameRegistry());
    topEntity->Accept(prePopInfo);
    IODBDesignDataSymTabs* symtabs = new IODBDesignDataSymTabs(getAtomicCache());
    verific2nucleus::VerificSTBuildWalker stBuildWalk(&prePopInfo, verificNucleusBuilder,
        verificVhdlDesignWalker.getNameRegistry(), symtabs);
    topEntity->Accept(stBuildWalk);
    getIODB()->setDesignDataSymbolTables(symtabs);
    getIODB()->populateDesignDirectives();
    if (symtabs) {
        verific2nucleus::UserTypePopulate utp(&verificVhdlDesignWalker, getIODB(), false, getMsgContext());
        if (! utp.populate(mDesign) ) {
            ;//err_code = Populate::eFailPopulate;
        }
    }

    //RJC RC#2013/08/09 (tigran) why are we setting up to return true here instead of the status returned by the walker?
    buildStatus = true;//verificVhdlDesignWalker.status();
  }

  mIODB->closeLogFile();        // close the libdesign.dir file

  // free up memory allocated by the Verific parser and related code
  verificVhdlDesignWalker.cleanModifiedConstraints();
  delete verificNucleusBuilder; verificNucleusBuilder = NULL;
  mVerificDesignManager.freeAllocatedMemory(false);


  // note the top level moduleName was added to the IODB in VerificNucleusBuilder::uDesign (but it was done every time the design is walked
  // which seems excessive)

  if (!(topModule || topEntity)){
    mMsgContext->NoTopModule();
    buildStatus = false;
  }

  return buildStatus; 
}

bool
CarbonContext::runInterraPopulateNucleus()
{
  VHDLCaseT vhdlCaseSpec = mJaguarContext->getCase();
  
  bool vhdlOnTop;
  vhdlOnTop = ( mArgs.getStrValue( JaguarContext::scVhdlTop ) != NULL );
  
  if (vhdlOnTop && (getFileCollector()->begin() == getFileCollector()->end() ))
  {
    // Error about no vhdl files being specified
    mMsgContext->VhdlNoFiles();
    // Error about the -vhdlTop not turning up anything
    mMsgContext->VhdlPrimaryUnitNotFound(mArgs.getStrValue(JaguarContext::scVhdlTop), JaguarContext::scDefaultVhdlLib);
    // now get out of here
    return false;
  }

  bool hasLibMap = (getArgs()->isParsed("-libMap") == ArgProc::eParsed);
  
  if ( vhdlOnTop == false &&
       ! getFileCollector()->hasHdlFiles(LangVer::Verilog) &&
       ! getFileCollector()->hasHdlFiles(LangVer::Verilog2001) &&
       ! getFileCollector()->hasLangLib(LangVer::Verilog) &&
       ! hasLibMap)
  {
    // Must have exactly one top module--we have none
    mMsgContext->NoTopModule();
    return false;
  }

  mvvNode topPrimary = NULL;
  mvvNode topArchitecture = NULL;
  mvvNode topBlkCfg = NULL;
  bool allow_hierrefs = not mArgs.getBoolValue(sDisallowHierRefs);
  bool verbose_hierref_analysis = mArgs.getBoolValue(sVerboseHierRefAnalysis);
  bool useLegacyGenerateHierarchyNames = ! (mArgs.getBoolValue(scDisableLegacyGenerateHierarchyNames));

  bool ret = true;
  if ( vhdlOnTop == false ) {
    // populate Verilog on top
    ret &= mCheetahContext->prepPopulateVerilog(&topPrimary, &topArchitecture, &topBlkCfg);
  } else {
    // populate VHDL on top
    ret &= mJaguarContext->prepPopulateVhdl(&topPrimary, &topArchitecture, &topBlkCfg);
  }
  
  // Do the actual population here
  if (! genEncryptedModel())
  {
    DesignPopulate populator(getSymbolTable(),
                             getAtomicCache(),
                             getIODB(),
                             getSourceLocatorFactory(),
                             getFlowFactory(),
                             getNetRefFactory(),
                             getMsgContext(),
                             getArgs(),
                             mTicProtectedNameMgr,
                             allow_hierrefs,
                             verbose_hierref_analysis,
                             useLegacyGenerateHierarchyNames,
                             vhdlCaseSpec,
                             this);
    
    PrePopInterraCB prePopCB(&populator, 
                             isVerboseUniquify());
    InterraDesignWalker prePopWalk(&prePopCB, mTicProtectedNameMgr);
      
    putInPrePopWalk(true);      // true silences all messages

    // here we ignore the return status of the walker because even if
    // there were problems, no message would have been printed.  Leave
    // error handling for later.
    (void) prePopWalk.design(topPrimary, topArchitecture, topBlkCfg,
                             mJaguarContext->getLogicalWorkLib());

    putInPrePopWalk(false);
    
    if (getMsgContext()->getExitStatus() != 0)
    {
      // This was in the verilog exit status check but not in the
      // vhdl, originally.
      getMsgContext()->PopulationFailure();
      return false;
    }
    else
    {
      //prePopCB.print();
      prePopCB.uniquify();
      //prePopCB.print();

      Populate::ErrorCode err_code = Populate::eSuccess;
      { 
        // Build elaborated/unelaborated symbol tables to hold design data like
        // directives, user design type information for visibility etc.
        IODBDesignDataSymTabs* symtabs = new IODBDesignDataSymTabs(getAtomicCache());
        STBuildWalkCB stbw(&populator, &prePopCB, symtabs);
        InterraDesignWalker designWalk(&stbw, mTicProtectedNameMgr);
        putInPrePopWalk(true);      // true silences all messages
        (void) designWalk.design(topPrimary, topArchitecture, topBlkCfg,
                                 mJaguarContext->getLogicalWorkLib());
        putInPrePopWalk(false);
        err_code = stbw.getErrCode();
        if (err_code == Populate::eSuccess) {
          // Set the design data symbol tables in IODB.
          getIODB()->setDesignDataSymbolTables(symtabs);
          // Populate design directives in the design data symbol tables.
          getIODB()->populateDesignDirectives();
        } else {
          delete symtabs;
        }
      }
  
      if (err_code == Populate::eSuccess) {
        NucleusInterraCB walkCB(&populator, mDesign, &prePopCB);
        InterraDesignWalker interraWalk(&walkCB, mTicProtectedNameMgr);
        populator.putDesignWalk(&interraWalk);
        populator.putWalkCB(&walkCB);

        InterraDesignWalkerCB::Status walk_status = InterraDesignWalkerCB::eNormal;
        walk_status = interraWalk.design(topPrimary, topArchitecture, topBlkCfg,
                                         mJaguarContext->getLogicalWorkLib());
        
        err_code = walkCB.getErrCode();   // check the status seen by the callbacks
        if ( err_code == Populate::eSuccess ) {
          // if none of the callbacks reported a problem then also
          // make sure that the walker finished without problems
          if ( walk_status != InterraDesignWalkerCB::eNormal ) {
            err_code = Populate::eFailPopulate; // the design walker failed to visit all sub items
          }
        }
      }

      if (err_code == Populate::eSuccess && mArgs.getBoolValue(scEnableCompositeTypeDump))
      {
        IODBDesignDataSymTabs* symtabs = getIODB()->getDesignDataSymbolTables();
        if (symtabs) {
          bool verbose = mArgs.getBoolValue(scVerboseCompositeTypeDump);
          UserTypePopulate utp(&populator, getIODB(), verbose, getMsgContext());
          if (!utp.populate(mDesign)) {
            err_code = Populate::eFailPopulate; // Populated nucleus successfully but
                                                // failed to populate type info.
          }
        }
      }

      if (vhdlOnTop)
        ret &= mJaguarContext->postPopulate(topPrimary, topArchitecture, err_code, mDesign);
      else
        ret &= mCheetahContext->postPopulate(err_code, mDesign);
    }
    //Currently xml write of symbol tables is used only for debug purposes.
    //Set the value of debugSymbolTable to true to activate xml write of population symbol tables.
    bool debugSymbolTable = false;

    STSymbolTable* declarationsSymTab = prePopCB.getDeclarationsSymTab();
    STSymbolTable* elaborationsSymTab = prePopCB.getElaborationsSymTab();
    if(debugSymbolTable) {
      debugXmlWriteSymbolTables(declarationsSymTab, elaborationsSymTab);
    }
  }


  ret &= freeInterraMemory() && (mDesign != NULL);
  
  if (ret)
  {
    // Set the top level module name in the iodb. Also, set the systemC
    // module name in the iodb.
    NUModuleList topMods;
    mDesign->getTopLevelModules(&topMods);
    INFO_ASSERT(topMods.size() == 1, "Design has multiple top-level modules");
    const NUModule* topMod = topMods.back();
    const char* topModName = topMod->getOriginalName()->str();
    mIODB->putTopLevelModuleName(topModName);
    if (mArgs.isParsed(scSystemCModuleName) == ArgProc::eParsed)
      mIODB->putSystemCModuleName(mArgs.getStrValue(scSystemCModuleName));
    else
      mIODB->putSystemCModuleName(topModName);
  }
  return ret;
}

bool CarbonContext::runDumpDesignPopulate()
{
  if (mDesign != NULL) {
    dumpHierarchy(NULL);
    if (mArgs.getBoolValue(scDumpDesignPopulate)) {
      mDesign->print(true, 0);
    }

    // Dump a behavioral verilog file to emit hierarchical reference assigns
    // to initialize all nets declared as 'reg' to 0
    dumpRegInit();
  }

  if (mArgs.getBoolValue(scDumpVerilog)) {
    dumpVerilog("populate");
  }
  NUBase::checkDesignWalker(mDesign, "populate");

  SInt32 numInstances;
  if ((mArgs.getIntLast("-genMultiDUT", &numInstances) == ArgProc::eKnown) &&
      (numInstances > 0))
  {
    genMultiDUT(numInstances);
  }
      
  // show the names of all memories if requested by user
  createDumpMemoryTask();

  return (mDesign != NULL);   
}

static void sDumpRegModHelper(UtOBStream&, NUModule*,
                              STBranchNode*, STSymbolTable*);

static void sDumpRegNetHelper(UtOBStream& ofile, NUNet* net,
                              const UtString& pathBuf)
{
  if (net->isReg()) {
    ofile << "  " << pathBuf << "." << net->getName()->str() << " = 1'b0;\n";
  }
}

static void sDumpRegScopeHelper(UtOBStream& ofile,
                                STSymbolTable* symtab,
                                STBranchNode* parent,
                                NUNamedDeclarationScope* scope)
{
  STBranchNode* path = symtab->createBranch(scope->getName(), parent);
  UtString pathBuf;
  path->compose(&pathBuf);
  
  for (NUNetLoop p = scope->loopLocals(); !p.atEnd(); ++p) {
    sDumpRegNetHelper(ofile, *p, pathBuf);
  }

  for (NUModuleInstanceMultiLoop p = scope->loopInstances();
       !p.atEnd(); ++p) {
    NUModuleInstance* inst = *p;
    STBranchNode* instPath = symtab->createBranch(inst->getName(), path);
    sDumpRegModHelper(ofile, inst->getModule(), instPath, symtab);
  }

  for (NUNamedDeclarationScopeLoop p = scope->loopDeclarationScopes();
       !p.atEnd(); ++p)
  {
    NUNamedDeclarationScope* declScope = *p;
    sDumpRegScopeHelper(ofile, symtab, path, declScope);
  }
} // static void sDumpRegScopeHelper

static void sDumpRegModHelper(UtOBStream& ofile,
                              NUModule* mod,
                              STBranchNode* parent,
                              STSymbolTable* symtab)
{
  UtString pathBuf;
  parent->compose(&pathBuf);

  for (NUModule::NetLoop q = mod->loopNets(); !q.atEnd(); ++q) {
    sDumpRegNetHelper(ofile, *q, pathBuf);
  }

  for (NUNamedDeclarationScopeLoop p = mod->loopDeclarationScopes();
       !p.atEnd(); ++p)
  {
    NUNamedDeclarationScope* declScope = *p;
    sDumpRegScopeHelper(ofile, symtab, parent, declScope);
  }

  for (NUModuleInstanceMultiLoop p = mod->loopModuleInstances();
       !p.atEnd(); ++p) {
    NUModuleInstance* inst = *p;
    STBranchNode* path = symtab->createBranch(inst->getName(), parent);
    sDumpRegModHelper(ofile, inst->getModule(), path, symtab);
  }
} // static void sDumpRegModHelper

// Dump behavioral code to initialize all regs to 0
void CarbonContext::dumpRegInit()
{
  UtString filename;
  filename << mFileRoot << ".init.v";
  UtOBStream outfile(filename.c_str());
  if (outfile.is_open()) {
    CbuildSymTabBOM symTabBOM;
    STSymbolTable symtab(&symTabBOM, mAtomicCache);

    outfile << "initial begin\n";
    for (NUDesign::ModuleLoop p = mDesign->loopTopLevelModules();
         !p.atEnd(); ++p)
    {
      NUModule* mod = *p;
      STBranchNode* root = symtab.createBranch(mod->getName(), NULL);
      sDumpRegModHelper(outfile, mod, root, &symtab);
    }
    outfile << "end\n";
  }
  else {
    mMsgContext->VlogCannotOpenFile(filename.c_str(), outfile.getErrmsg());
  }
} // void CarbonContext::dumpRegInit

bool CarbonContext::runFindBlobs()
{
  mBlobMap->findBlobs(mDesign);
  return true;
} // bool CarbonContext::runFindBlobs

bool CarbonContext::runEvalDesignDirectives()
{
  bool ret = mIODB->evaluateDesignDirectives(mDesign);
  if (ret) {
    // Write out empty prototypes for the hidden modules, so that we can debug
    // with debussy
    UtOBStream* protos = NULL;
    for (NUDesign::ModuleLoop p = mDesign->loopAllModules(); !p.atEnd(); ++p) {
      NUModule* mod = *p;
      if (mod->isHidden()) {
        if (protos == NULL) {
          UtString protoFile, errmsg;
          protoFile << getFileRoot() << ".cem_protos.v";
          protos = new UtOBStream(protoFile.c_str());
          if (!protos->is_open()) {
            mMsgContext->VlogCannotOpenFile(protoFile.c_str(), 
                                            protos->getErrmsg());
            break;
          }
        }
        UtString buf;
        mod->compose(&buf, NULL, 0, false); // no recurse, print proto only
        (*protos) << buf << "\n\n";
      }
    }
    if (protos != NULL) {
      delete protos;            // closes the file
    }
  } // if

  return ret;
} // bool CarbonContext::runEvalDesignDirectives



bool CarbonContext::removeUncalledTasks(MsgContext * msg_ctx, bool verbose)
{
  bool status = true;
  bool removeDeadTasks = ! mArgs.getBoolValue(scNoRemoveUnusedTasks);

  if (removeDeadTasks) {
    // Remove unused constructs. Currently, only unused tasks are removed.
    // We don't need to repeat the recusive check in the TFPromote call
    // to removeUncalledTasks, so allow recursive tasks if verbose is off.
    bool recursive = mArgs.getBoolValue(scRecursiveTasks) || !verbose;
    MarkSweepUnusedTasks msUnused(msg_ctx, verbose, recursive);
    bool recursion_found = false;
    (void) msUnused.walkAndRemove(mDesign, &recursion_found);
    if ( recursion_found ){
      status = false;
    }
    mStats->printIntervalStatistics(CRYPT("DeadTaskCleanup"));
  }
  return status;
}

// Generate a filename that incorporates the current directory
// PID, and hostname
static void sUniquifyFilename(const char* destDir, UtString* pathname) {
  UtString cwd, filename, pid, host;

  // Try to uniquify by getting the current directory -- presumably the
  // test directory, getting the pathname under /test/, converting
  // all the remaining / to _
  OSGetCurrentDir(&cwd);
  char* testDir = strstr(cwd.getBuffer(), "/test/");
  if (testDir != NULL) {
    char* testName = testDir + 6;
    for (char* p = testName; *p != '\0'; ++p) {
      if (*p == '/') {
        *p = '_';
      }
    }
    filename << testName << "_";
  }


  // Make the filename unique by combining hostname and pid.  All
  // other attempts may fall short.
  OSGetHostname(&host);
  OSGetPidStr(&pid);
  filename << host << "_" << pid;
  OSConstructFilePath(pathname, destDir, filename.c_str());
}

// Resynthesize constructs that emerge from populate, that we cannot represent
// downstream, such as breaks (Verilog disable, VHDL exit/next/return).
bool CarbonContext::runPostPopulateResynth() 
{
  // Remove tasks without calls.
  bool task_removal_status = removeUncalledTasks(mMsgContext, true);
  if ( not task_removal_status ){
    return false;    // there was a problem during task removal
  }

  {
    // Resynthesize Composite Nets and their ilk into their subnets,
    // etc.  This must come before MemoryResynth because it can create
    // 3+D nets that have to be resynthesized.
    CompositeResynth compositeResynth( mMsgContext );
    compositeResynth.design( mDesign );
    if (mArgs.getBoolValue(scDumpVerilog)) {
      dumpVerilog("compresynth");
    }
  }

  // Get rid of 'break' statements before we start moving
  // blocks around
  {
    BreakResynth breakResynth(mNetRefFactory, mMsgContext);
    breakResynth.design(mDesign);
  }

  {
    WiredNetResynth wiredNetResynth(mMsgContext);
    wiredNetResynth.propagate(mDesign);
  }

  // Get rid of 'downto' statements in concats
  {
    DowntoResynth downtoResynth(mNetRefFactory, &mArgs, mMsgContext,
                                mAtomicCache, mIODB);
    downtoResynth.design(mDesign);
  }

  {
    // Resynthesize 3+dimensional memories to two normalized dimensions
    MemoryResynth memoryResynth( mMsgContext );
    bool success = memoryResynth.design(mDesign);
    if ( ! success ) return false; // problem during mem resynthesis
  }

  {
    ProcessResynth proc_resynth(mMsgContext, mNetRefFactory, mIODB,
                                mAtomicCache, &mArgs, false);
    if (!proc_resynth.design(mDesign)) {
      return false; // Failed to resynthesize a VHDL process into always block.
    }
  }

  if (mArgs.getBoolValue (scWriteGuiDB))  {
    // Do not run MemoryBVResynth when dumping the GUI symbol table because it
    // will make some nets disappear.
  } else {
    // Resynthesize memories into bit vectors.
    SInt32 size_threshold = 0;
    mArgs.getIntLast(scMemoryBVLimit, &size_threshold);
    SInt32 dyn_index_threshold = 0;
    mArgs.getIntLast(scMemoryBVDynIndexLimit, &dyn_index_threshold);
    SInt32 width_threshold = 0;
    mArgs.getIntLast(scMemoryBVWidthLimit, &width_threshold);
    Fold fold(mNetRefFactory, &mArgs, mMsgContext, mAtomicCache, mIODB, mArgs.getBoolValue(CRYPT("-verboseFold")), eFoldPreserveReset|eFoldNoStmtDelete);
    MemoryBVResynth mem_bv_resynth(size_threshold, dyn_index_threshold, width_threshold, mArgs.getBoolValue(scMemoryBVStats), &fold, mIODB, mMsgContext);
    mem_bv_resynth.design(mDesign);
  }

  bool allTasks = mArgs.getBoolValue(scPromoteAllTasks);
  if (allTasks || mArgs.getBoolValue(scPromoteHierTasks)) {
    TFPromote tfPromote(mNetRefFactory, mMsgContext, mAtomicCache,
                        mArgs.getBoolValue(scVerbosePromoteTasks),
                        mIODB, &mArgs, allTasks);
    tfPromote.design(mDesign);

    // TFPromote does not delete the sources for the tasks it
    // promotes -- it depends on dead task elimination to do that.
    // This is because it a task might be referenced both locally
    // and hierarchically and it maybe be better to leave the local
    // reference as is.  Provide a false verbose argument so that we don't
    // warn during GC on the tasks that were moved.
    bool task_removal_status = removeUncalledTasks(mMsgContext, false);
    if ( not task_removal_status ){
      return false;
    }
  }

  // Perform congruence analysis on the design.  This is done after
  // TFPromote because TFPromote generally should reduce hierarchical
  // references in (and to) sub-modules.  In the case where a promoted
  // task has local references, those will become hierachical
  // references, so there is no change in congruence eligibility.  But
  // promoted tasks that only reference their I/Os do not leave
  // hierarchical references behind, and thus could improve the number
  // of modules that can be reduced for congruency
  if (mArgs.getBoolValue(scCongruency)) {
    const char* gatherCongruenceDir = mArgs.getStrValue(scGatherCongruenceDir);
    if (gatherCongruenceDir == NULL) {
      mCongruencyInfoFilename << mFileRoot << ".congruence";
    }
    else {
      sUniquifyFilename(gatherCongruenceDir, &mCongruencyInfoFilename);
    }
    SInt32 paramCostLimit;
    (void) mArgs.getIntLast(scCongruencyParamCostLimit, &paramCostLimit);
    bool factorConstants = mArgs.getBoolValue(CRYPT("-earlyCongruencyFactoring"));
    UtString reason;
    (void) OSUnlink(mCongruencyInfoFilename.c_str(), &reason);

    // Do task congruence before module congruence, because converging two
    // tasks in a module may make the module congruent with another module
    // where those tasks were already converged
    for (NUDesign::ModuleLoop p = mDesign->loopAllModules(); !p.atEnd(); ++p) {
      NUModule* mod = *p;
      Congruent congruent(mIODB,
                          mArgs.getBoolValue(scVerboseCongruency),
                          paramCostLimit,
                          mCongruencyInfoFilename.c_str(),
                          mMsgContext,
                          false); // do not factor contstants
      congruent.moduleTasks(mod);
    }
    {
      Congruent congruent(mIODB,
                          mArgs.getBoolValue(scVerboseCongruency),
                          paramCostLimit,
                          mCongruencyInfoFilename.c_str(),
                          mMsgContext,
                          factorConstants);
      congruent.designModules(mDesign);
    }
  }
  if (mArgs.getBoolValue(scDumpVerilog)) {
    dumpVerilog("popresynth");
  }
  NUBase::checkDesignWalker(mDesign, "popresynth");

  return true;
}

bool CarbonContext::runMakeSizesExplicit() {
 ExplicitSize explicitSize;
 explicitSize.mutateDesign(mDesign);
 if (mArgs.getBoolValue(scDumpVerilog)) {
   dumpVerilog("explicit_size");
 }
 return true;
}

bool CarbonContext::runGlobalOptimizations()
{
  /*
    Only run wirednet resynth on a module once. Currently, that is
    done in minimumAnalyzeModule, which gets called for every module
    for every instance. So, WiredNetResynth keeps a set of visited
    modules during assignment resynthesis.
  */
  WiredNetResynth wiredNetResynth(mMsgContext);

  GOGlobOpts globOpts(mAtomicCache, mIODB, mStats,
                      &mArgs, mMsgContext, mNetRefFactory,
                      &mSourceLocatorFactory,
                      mCongruencyInfoFilename.c_str(),
                      &wiredNetResynth,
                      mTristateMode);

  bool status = globOpts.optimize(mDesign, getFileRoot(), true);
  if (mArgs.getBoolValue(scDumpVerilog)) {
    dumpVerilog("constantPropagation");
  }
  NUBase::checkDesignWalker(mDesign, "constantPropagation");

  // Clean up
  CarbonMem::releaseBlocks();

  return status;
}

#ifdef CARBON_PV
bool CarbonContext::runProgrammersView() {
  bool status = true;

  if ((mMaster == NULL) && mArgs.getBoolValue(CRYPT("-pv"))) {
    PVCompileContext pv(this);

#if 0
    struct NameVal {
      const char* name;
      const char* val_sequence;
    };

    if (getenv("VIC") != NULL) {
      NameVal vic_sequence[] = {
        {"Vic.HRESETn",  "01" /*"1000001"*/},
  //      {"Vic.HCLK",     "0010100"},
        {"Vic.HADDR",    "03" /*"0000000"*/},
        {"Vic.HPROT",    "01" /*"0000000"*/},
        {"Vic.HSIZE",    "02" /*"0000000"*/},
        {"Vic.HTRANS",   "01" /*"0000000"*/},
        {"Vic.HREADYIN", "11" /*"1111111"*/},
        {"Vic.HWRITE",   "01" /*"0000000"*/}
      };

      UInt32 num_sigs = sizeof(vic_sequence) / sizeof(NameVal);
      PVTransaction* reset = pv.addTransaction("reset", "Vic.HCLK",
                                               ePVWrite, 0, 0);
      PVSequence* seq = reset->addSequence();
      PVPhase* phases[2];
      phases[0] = seq->addPhase();
      PVTransaction* reg3 = pv.addTransaction("reg3", "Vic.HCLK",
                                              ePVWrite, 1, 1);
      seq = reg3->addSequence();
      phases[1] = seq->addPhase();

      for (UInt32 i = 0; i < num_sigs; ++i) {
        for (UInt32 p = 0; p < 2; ++p) {
          char val[100];
          sprintf(val, "4'h%c", vic_sequence[i].val_sequence[p]);
          phases[p]->tieInputPort(vic_sequence[i].name, val);
        }
      }
    } // if

    else if (getenv("ALU") != NULL) {
      NameVal alu_sequence[] = {
        {"mul", "2"},
        {"add", "3"},
        {"sub", "4"},
        {"div", "5"}
      };
      UInt32 num_transactions = sizeof(alu_sequence) / sizeof(NameVal);
      for (UInt32 i = 0; i < num_transactions; ++i) {
        PVTransaction* transaction = pv.addTransaction(alu_sequence[i].name,
                                                       "top.clk",
                                                       ePVWrite,
                                                       i, i);
        PVSequence* seq = transaction->addSequence();
        char val[100];
        sprintf(val, "4'h%s", alu_sequence[i].val_sequence);

        // Every transaction for this circuit must do LOAD_A, LOAD_B, and then


        PVPhase* phase = seq->addPhase();
        phase->tieInputPort("top.phase", "1'b0");
        phase->tieInputPort("top.in", val);
      }
    } // else if

    else {
      NameVal avg_sequence[] = {
        {"idle", "0"},
        {"load", "1"},
        {"avg", "2"},
        {"clr", "3"},
        {"clr_addr", "4"},
        {"read", "5"},
      };
      UInt32 num_transactions = sizeof(avg_sequence) / sizeof(NameVal);

      for (UInt32 i = 0; i < num_transactions; ++i) {
        PVTransaction* transaction = pv.addTransaction(avg_sequence[i].name,
                                                       "top.clk",
                                                       ePVWrite,
                                                       i, i);
        PVSequence* seq = transaction->addSequence();
        char val[100];
        sprintf(val, "4'h%s", avg_sequence[i].val_sequence);

        PVPhase* phase = seq->addPhase();
        phase->tieInputPort("top.cmd", val);
        phase->tieInputPort("top.phase", "1'b0");
        phase = seq->addPhase();
        phase->tieInputPort("top.cmd", val);
        phase->tieInputPort("top.phase", "1'b1");
      }
    };
#else
    ::CarbonCfg cfg(this);
    if (mPSDFileName != NULL) {
      cfg.putPVContext(&pv);
      if (cfg.readNoCheck(mPSDFileName.c_str()) == eCarbonCfgFailure) {
        fprintf (stderr, cfg.getErrmsg());
        return false;
      }
    } else {  // no -psd switch found
      fprintf(stderr, "PSD file not specified (use -psd <filename>)\n");
      return false;
    }
#endif

    pv.optimize();
    if (mArgs.getBoolValue(scDumpVerilog)) {
      dumpVerilog("pv");
    }
  } // if

  return status;
} // bool CarbonContext::runProgrammersView
#endif

void CarbonContext::dumpVerilog(const char* dirName)
{
  if (mForkingWriteGuiDB && mWriteGuiDBPID == 0) {
    // This is the child process for writing out a GUI database. Do not dump
    // any verilog from the child process.
  } else {
    mDesign->dumpVerilog(mFileRoot.c_str(), dirName, mMsgContext);
  }
} // void CarbonConext::dumpVerilog

static const char* sMultiDUTOutputPortName(UtString* buf,
                                           const char* fileRoot,
                                           const char* portName,
                                           UInt32 instanceNumber)
{
  buf->clear();
  *buf << fileRoot << "_" << instanceNumber << "_" << portName;
  return buf->c_str();
}

// Generate a new DUT that has the same pinout as the original DUT,
// but instantiates the original DUT N times.  Route the inputs
// to all the instances.  OR the outputs together.  Wire inouts
// together.  This helps make customer designs bigger for benchmarking.
// The generated DUT goes to stdout
void CarbonContext::genMultiDUT(UInt32 numInstances)
{
  NUModule* topLevel = NULL;
  for (NUDesign::ModuleLoop m = mDesign->loopTopLevelModules(); !m.atEnd(); ++m)
  {
    NUModule* mod = *m;
    NU_ASSERT(topLevel == NULL, topLevel);
    topLevel = mod;
  }

  UtString dir, multiBuf;
  OSParseFileName(mFileRoot.c_str(), &dir, &multiBuf);
  const char* multiName = multiBuf.c_str();
  if (strncmp(multiName, "lib", 3) == 0) {
    multiName += 3;
  }
  const char* modName = topLevel->getName()->str();
  if (strcmp(multiName, modName) == 0) {
    UtString tmp(multiName);
    tmp << "_multiDUT";
    mMsgContext->MultiDUTRename(multiName, tmp.c_str());
    multiBuf = tmp;
    multiName = multiBuf.c_str();
  }

  // Module name will be based on 'fileroot'.  Declare the interface
  UtString fileName;
  fileName << mFileRoot << ".multiDUT.v";
  UtOBStream f(fileName.c_str());
  if (!f.is_open()) {
    mMsgContext->VlogCannotOpenFile(fileName.c_str(), f.getErrmsg());
    return;
  }

  f << "module " << multiName << "(";
  const char* prefix = "\n  ";
  for (NUNetVectorLoop p = topLevel->loopPorts(); !p.atEnd(); ++p) {
    NUNet* port = *p;
    UtString portName;
    port->compose(&portName, NULL);
    f << prefix << portName;
    prefix = ",\n  ";
  }
  f << ");\n\n";

  // Declare the port directions
  for (NUNetVectorLoop p = topLevel->loopPorts(); !p.atEnd(); ++p) {
    NUNet* port = *p;

    if (port->isBid()) {
      f << "  inout ";
    }
    else if (port->isInput()) {
      f << "  input ";
    }
    else {
      NU_ASSERT(port->isOutput(), port);
      f << "  output ";
    }

    UtString portDecl;
    port->composeDeclaration(&portDecl);
    f << portDecl << "\n";
  }
  f << "\n";

  UtString buf;

  f << "  // Declare output port temps\n";
  for (NUNetVectorLoop p = topLevel->loopPorts(); !p.atEnd(); ++p) {
    NUNet* port = *p;

    if (port->isOutput()) {
      
      for (UInt32 i = 0; i < numInstances; ++i) {
        UtString portDecl;
        port->composeDeclarePrefix(&portDecl);
        f << "  wire " << portDecl << " "
          << sMultiDUTOutputPortName(&buf,
                                     multiName,
                                     port->getName()->str(),
                                     i)
          << ";\n";
      }
    }
  }


  f << "  // Instantiate the DUT N times\n";
  for (UInt32 i = 0; i < numInstances; ++i) {
    f << "  " << modName << " " << "u" << i << "(";
    prefix = "\n    ";

    for (NUNetVectorLoop p = topLevel->loopPorts(); !p.atEnd(); ++p) {
      NUNet* port = *p;
      UtString portDecl;

      f << prefix << "." << port->getName()->str() << "(";

      // Wire up the inputs and BIDs together
      if (port->isBid() || port->isInput()) {
        f << port->getName()->str();
      }
      else {
        NU_ASSERT(port->isOutput(), port);
        // OR together all the outputs using wires named "fileroot_i_portname";
        f << sMultiDUTOutputPortName(&buf,
                                     multiName,
                                     port->getName()->str(),
                                     i);
      }

      f << ")";
      prefix = ",\n    ";
    }
    f << ");\n\n";
  }

  f << "  // OR together all the output port temps\n";
  for (NUNetVectorLoop p = topLevel->loopPorts(); !p.atEnd(); ++p) {
    NUNet* port = *p;

    if (port->isOutput()) {
      f << "  assign " << port->getName()->str() << " =";
      
      prefix = " ";
      for (UInt32 i = 0; i < numInstances; ++i) {
        f << prefix << sMultiDUTOutputPortName(&buf,
                                               multiName,
                                               port->getName()->str(),
                                               i);
        prefix = " | ";
      }
      f << ";\n";
    }        
  }

  f << "\nendmodule\n";
} // void CarbonContext::genMultiDUT

void CarbonContext::findNameWidths(NUModule* mod, UInt32* maxModWidth,
                                   UInt32* maxInstWidth, UInt32 depth)
{
  for (NUModuleInstanceMultiLoop p = mod->loopInstances(); !p.atEnd(); ++p) {
    NUModuleInstance* inst = *p;
    NUModule* submod = inst->getModule();

    if ( ( inst->getLoc().isTicProtected() ) ||
         ( submod->getLoc().isTicProtected() )  ) {
      *maxModWidth = std::max(*maxModWidth, 11U); // "<protected>" is 11 chars
      continue; // this is protected, do not show this or any hierarchy below this
    }

    UtString scoped_instance_name;
    bool previous_was_protected = false;
    inst->getScopedName(&scoped_instance_name, &previous_was_protected);
    if ( previous_was_protected ){
      continue;
    }
    UInt32 instLen = strlen(scoped_instance_name.c_str());
    *maxInstWidth = std::max(*maxInstWidth, instLen);

    UInt32 modLen = strlen(submod->getOriginalName()->str()) + depth*2;
    *maxModWidth = std::max(*maxModWidth, modLen);

    findNameWidths(submod, maxModWidth, maxInstWidth, depth + 1);
  }
}
  
void CarbonContext::printHierarchy(UtOBStream& file, NUModule* mod,
                                   bool* previous_was_protected,
                                   UInt32 instanceColumn,
                                   UInt32 locColumn, UInt32 depth)
{
  UtString buf;

  for (NUModuleInstanceMultiLoop p = mod->loopInstances(); !p.atEnd(); ++p) {
    buf.clear();
    UtIndent indent(&buf);
    buf.append(depth*2, ' ');

    NUModuleInstance* inst = *p;
    NUModule* submod = inst->getModule();

    if ( ( inst->getLoc().isTicProtected() ) ||
         ( submod->getLoc().isTicProtected() ) ) {

      if ( not *previous_was_protected ) {
        buf << "<protected>";
        indent.newline();
        file << buf;
      }
      *previous_was_protected = true;
      continue;
    }

    *previous_was_protected = false;
    // Put in module name with indentation
    buf << submod->getOriginalName()->str();

    if (!indent.tab(instanceColumn)) {
      buf << " ";
    }

    UtString scoped_instance_name;
    inst->getScopedName(&scoped_instance_name, previous_was_protected);
    buf << scoped_instance_name;

    if (!indent.tab(locColumn)) {
      buf << " ";
    }
    submod->getLoc().compose(&buf);
    indent.newline();
    file << buf;

    printHierarchy(file, submod, previous_was_protected, instanceColumn, locColumn, depth + 1);
  }
}
  

void CarbonContext::dumpHierarchy(const char* phase)
{
  UtString fname;
  fname << mFileRoot;
  if (phase != NULL) {
    fname << "." << phase;
  }
  fname << ".hierarchy";
  UtOBStream hierFile(fname.c_str());
  if (!hierFile.is_open()) {
    mMsgContext->VlogCannotOpenFile(fname.c_str(), hierFile.getErrmsg());
  }
  else {
    // First, rip through the whole hierarchy on a dry run to get the
    // columns widths.  We are going to lay things out like this:
    //
    // Module      Instance        Location
    // ------------------------------------
    // top                         /foo/top.v:55
    //   sub       sub1            /foo/sub.v:32
    //     leaf    u1              /foo/leaf.v:83
    //     leaf    u2              /foo/leaf.v:83
    //     leaf    u3              /foo/leaf.v:83
    //   sub       sub2            /foo/sub.v:32
    //     leaf    u1              /foo/leaf.v:83
    //     leaf    u2              /foo/leaf.v:83
    //     leaf    u3              /foo/leaf.v:83
    //
    // so we need to know the widest module name (including depth)
    // and the widest instance name, before we start.
    static const char modHeader[] = "Module";
    static const char instHeader[] = "Instance";
    UInt32 modWidth = strlen(modHeader);
    UInt32 instWidth = strlen(instHeader);
    for (NUDesign::ModuleLoop p = mDesign->loopTopLevelModules(); !p.atEnd(); ++p) {
      NUModule* mod = *p;

      if ( mod->getLoc().isTicProtected() ) {
        modWidth = std::max(modWidth, 11U); // "<protected>" is 11 chars
        continue; // this is protected, do not show this or any hierarchy below this
      }

      // Take into account the root module's width
      UInt32 modLen = strlen(mod->getOriginalName()->str());
      modWidth = std::max(modWidth, modLen);

      findNameWidths(mod, &modWidth, &instWidth, 1);
    }

    // Max out the column width so this display is useful in ascii,
    // even if this forces some of the columns to not line up.
    // Maybe we should write out csv as well...
    modWidth = std::min(modWidth, 30U);
    instWidth = std::min(instWidth, 60U); 

    bool previous_was_protected = false;
    // Lay out the header
    UtString buf, dashes;
    UtIndent indent(&buf);
    UInt32 instanceColumn = modWidth + 2;
    UInt32 locColumn = instanceColumn + instWidth + 2;
    buf << modHeader;
    indent.tab(instanceColumn);
    buf << instHeader;
    indent.tab(locColumn);
    buf << "Location";
    dashes.append(buf.size(), '-');
    hierFile << buf << "\n" << dashes << "\n";
    for (NUDesign::ModuleLoop p = mDesign->loopTopLevelModules(); !p.atEnd(); ++p) {
      NUModule* mod = *p;
      if ( mod->getLoc().isTicProtected() ) {
        if ( not previous_was_protected ) {
          indent.clear();
          buf << "<protected>";
          indent.newline();
          hierFile << buf;
          previous_was_protected = true;
        }
        continue;
      }

      previous_was_protected = false;
      indent.clear();
      buf << mod->getOriginalName()->str();
      if (!indent.tab(locColumn)) {
        buf << " ";
      }
      mod->getLoc().compose(&buf);
      hierFile << buf << "\n";
      printHierarchy(hierFile, mod, &previous_was_protected, instanceColumn, locColumn, 1);
    }
  } // else
} // void CarbonContext::dumpHierarchy

bool CarbonContext::createDirectory( const UtString * dir)
{
  bool status = false;
  UtString errmsg, cmd;

  cmd << "/bin/rm -rf " << *dir;
  (void) OSSystem(cmd.c_str(), &errmsg);
  errmsg.clear();
  if (OSMkdir(dir->c_str(), 0777, &errmsg) != 0) {
    mMsgContext->VlogCannotOpenFile(dir->c_str(), errmsg.c_str());
    status = true;
  }
  return status;
}

void CarbonContext::createDumpMemoryTask()
{
  
  if ( ( mDesign == NULL ) or
       ( not mArgs.getBoolValue( scDumpDesignState ))) {
    return;
  }

  UtString errmsg;
  UtString vfile(mFileRoot);

  vfile << ".memoryDumpTask";

  FILE* f = OSFOpen(vfile.c_str(), "w", &errmsg);
  if (f == NULL) {
    mMsgContext->VlogCannotOpenFile(vfile.c_str(), errmsg.c_str());
    errmsg.clear();
    return;
  }
    
  for (NUDesign::ModuleLoop p = mDesign->loopAllModules(); !p.atEnd(); ++p)
  {
    NUModule* mod = *p;
    bool first = true;
    const char* modName = mod->getName()->str();
    for (NUModule::NetLoop q = mod->loopNets(); !q.atEnd(); ++q)
    {
      NUNet* net = *q;
      if (net->isMemoryNet()) {
        const NUMemoryNet* mn = net->getMemoryNet();
        NU_ASSERT(mn, net);
        if ( first ) {
          fprintf(f, "module: %s\n", modName);
          first = false;
        }
        UtString buf;
        net->composeType(&buf);
        net->composeDeclaration(&buf);
        fprintf(f, "  %s\n", buf.c_str());
      }
    }
  }

  fclose(f);
} // void CarbonConext::createDumpMemoryTask


bool CarbonContext::runEarlyFlatteningAnalysis()
{
  mStats->pushIntervalTimer();

  // unelaborated analysis
  LocalAnalysis local_analysis(mArgs.getBoolValue(scPhaseStats),
                               mModulePhaseStats,
                               mStats,
                               mAtomicCache,
                               mNetRefFactory,
                               mFlowFactory,
                               mMsgContext,
                               mIODB,
                               &mArgs,
                               mSymbolTable,
                               &mSourceLocatorFactory,
                               mFlowElabFactory,
                               mCongruencyInfoFilename.c_str(),
                               /* WiredNetResynth only needed for globopts*/
                               NULL,
                               mTristateMode,
                               getFileRoot ());
  
  // Setup the flattening trace file.
  INFO_ASSERT(mFlatteningTraceFile==NULL, "There is already a flattening trace file.");

  UtString filename;
  filename << mFileRoot << ".flattening";
  mFlatteningTraceFile = new UtOBStream(filename.c_str());

  // save away the original flattening threshold.
  ArgProc::OptionStateT arg_status;
  SInt32 original_threshold;
  arg_status = mArgs.getIntLast(CRYPT("-flattenThreshold"),
                                &original_threshold);
  INFO_ASSERT(arg_status==ArgProc::eKnown, "flattening switches not properly initialized");
  // determine the early threshold
  SInt32 early_threshold;
  arg_status = mArgs.getIntLast(CRYPT("-flattenEarlyThreshold"),
                                &early_threshold);
  INFO_ASSERT(arg_status==ArgProc::eKnown, "flattening switches not properly initialized");

  // save away the original flattening value.
  bool original_flatten = mArgs.getBoolValue(CRYPT("-flatten"));
  bool early_flatten = mArgs.getBoolValue(CRYPT("-flattenEarly"));

  // setup for early flattening.
  mArgs.setIntValue(CRYPT("-flattenThreshold"), early_threshold);
  mArgs.setBoolValue(CRYPT("-flatten"), early_flatten);

  // flatten bottom-up without other analysis
  local_analysis.analyzeDesign(mDesign,
                               LocalAnalysis::eAnalysisModeEarlyFlatten,
                               mFlatteningTraceFile);

  // update the flattening post-early flattening
  mArgs.setIntValue(CRYPT("-flattenThreshold"), original_threshold);
  mArgs.setBoolValue(CRYPT("-flatten"), original_flatten);

  mStats->popIntervalTimer();

  if (mArgs.getBoolValue(scDumpVerilog))
    dumpVerilog("earlyFlatteningAnalysis");
  NUBase::checkDesignWalker(mDesign, "earlyFlatteningAnalysis");

  if (mArgs.getBoolValue(scDumpNetFlags)) {
    dumpHierarchy("earlyFlatteningAnalysis");
  }

  return true;
} // bool CarbonContext::runEarlyFlatteningAnalysis


bool CarbonContext::runPortAnalysis()
{
  mStats->pushIntervalTimer();

  // unelaborated analysis
  LocalAnalysis local_analysis(mArgs.getBoolValue(scPhaseStats),
                               mModulePhaseStats,
                               mStats,
                               mAtomicCache,
                               mNetRefFactory,
                               mFlowFactory,
                               mMsgContext,
                               mIODB,
                               &mArgs,
                               mSymbolTable,
                               &mSourceLocatorFactory,
                               mFlowElabFactory,
                               mCongruencyInfoFilename.c_str(),
                               /* WiredNetResynth not needed for this */
                               NULL,
                               mTristateMode, 
                               getFileRoot ());

  // Perform port analysis.
  local_analysis.analyzeDesignPorts(mDesign);

  mStats->popIntervalTimer();

  if (mArgs.getBoolValue(scDumpVerilog))
    dumpVerilog("portAnalysis");
  NUBase::checkDesignWalker(mDesign, "portAnalysis");

  if (mArgs.getBoolValue(scDumpNetFlags)) {
    dumpHierarchy("portAnalysis");
  }

  return true;
} // bool CarbonContext::runPortAnalysis


bool CarbonContext::runLocalAnalysis()
{
  mStats->pushIntervalTimer();

  // unelaborated analysis
  LocalAnalysis local_analysis(mArgs.getBoolValue(scPhaseStats),
                               mModulePhaseStats,
                               mStats,
                               mAtomicCache,
                               mNetRefFactory,
                               mFlowFactory,
                               mMsgContext,
                               mIODB,
                               &mArgs,
                               mSymbolTable,
                               &mSourceLocatorFactory,
                               mFlowElabFactory,
                               mCongruencyInfoFilename.c_str(),
                               /* WiredNetResynth only needed for
                                  minimal analysis */
                               NULL,
                               mTristateMode,
                               getFileRoot ());
  
  INFO_ASSERT(mFlatteningTraceFile!=NULL, "flattening trace file has not been initialized");

  // bottom-up analysis
  local_analysis.analyzeDesign(mDesign,
                               LocalAnalysis::eAnalysisModeNormal,
                               mFlatteningTraceFile);
  delete mFlatteningTraceFile;
  mFlatteningTraceFile = NULL;

  if (mArgs.getBoolValue(scDumpVerilog)) {
    dumpVerilog("localAnalysis");
  }

  // GC on net-ref factory.
  mNetRefFactory->cleanup();

  // Release memory.
  CarbonMem::releaseBlocks();

  if (mArgs.getBoolValue(scDumpNetFlags)) {
    dumpHierarchy("localAnalysis");
  }

  mStats->popIntervalTimer();
  return true;
} // bool CarbonContext::runLocalAnalysis


bool CarbonContext::runPostLocalAnalysis()
{
  mStats->pushIntervalTimer();

  // bottom-up analysis after flattening
  LocalAnalysis local_analysis(mArgs.getBoolValue(scPhaseStats),
                               mModulePhaseStats,
                               mStats,
                               mAtomicCache,
                               mNetRefFactory,
                               mFlowFactory,
                               mMsgContext,
                               mIODB,
                               &mArgs,
                               mSymbolTable,
                               &mSourceLocatorFactory,
                               mFlowElabFactory,
                               mCongruencyInfoFilename.c_str(),
                               /* WiredNetResynth only needed for
                                  minimal analysis */
                               NULL,
                               mTristateMode,
                               getFileRoot ());
  local_analysis.analyzeDesign(mDesign,
                               LocalAnalysis::eAnalysisModePostFlatten,
                               mFlatteningTraceFile);

  delete mFlatteningTraceFile;
  mFlatteningTraceFile = NULL;

  if (mArgs.getBoolValue(scDumpVerilog)) {
    dumpVerilog("postLocalAnalysis");
  }

  // GC on net-ref factory.
  mNetRefFactory->cleanup();

  // For the moment, do not do LUT conversion early, as it yields expressions
  // that can't be further optimized (yet).
#if 0
  if (! mArgs.getBoolValue(scNoLogic2LUT)) {
    Fold fold(mNetRefFactory,
              &mArgs,
              mMsgContext, mAtomicCache,
              mIODB,
              false,            // verbose fold
              eFoldUDValid | eFoldUDKiller | eFoldComputeUD |
              eFoldIgnoreXZ | eFoldAggressive | eFoldBDDMux |
              eFoldNoStmtDelete);

    // The constant reductions done by the LUT construction engine
    // are not interesting, so don't report them even if -traceFold has
    // been specified.
    (void) fold.putTraceFold(false);

    Logic2Lut l2l(mNetRefFactory, &fold,
                  mFlowFactory,
                  &mArgs,
                  mMsgContext,
                  mIODB,
                  mAtomicCache,
                  mArgs.getBoolValue(scVerboseLogic2LUT),
                  mArgs.getBoolValue(scForceLogic2LUT));
    l2l.design(mDesign);
  }
#endif

  // Release memory.
  CarbonMem::releaseBlocks();

  if (mArgs.getBoolValue(scDumpNetFlags)) {
    dumpHierarchy("postLocalAnalysis");
  }

  mStats->popIntervalTimer();
  return true;
} // bool CarbonContext::runPostLocalAnalysis


bool CarbonContext::runElaboratedAnalysis()
{
  bool success = true;
  mStats->pushIntervalTimer();

  bool verboseCleanup = mArgs.getBoolValue(scVerboseCleanup);

  // remove any locally dead code before elaborating
  DeadNetCallback dead_net_callback(mSymbolTable);
  RemoveDeadCode unelabCleanup(mNetRefFactory, mFlowFactory,
                               mIODB, &mArgs, mMsgContext,
                               false, // only start at important nets.
                               true,  // always delete dead code.
                               false, // do not delete module instances
                               verboseCleanup);
  unelabCleanup.design(mDesign, &dead_net_callback);

  // Check all nucleus is reachable after local analysis.  We
  // can't do that until after cleanup, of course.
  NUBase::checkDesignWalker(mDesign, "unelabCleanup");

  // Run elaboration
  if (mArgs.getBoolValue(scWriteGuiDB)) {
    // We are just building the symbol table to dump out for the GUI. So there
    // is no need to elaborate flow
    mElaborate->design(mDesign, /*elaboratePorts=*/ true, /*elaborateFlow=*/ false);
  } else {
    mElaborate->design(mDesign, /*elaboratePorts=*/ true, /*elaborateFlow=*/ true);
  }

  mStats->printIntervalStatistics(CRYPT("DesElab"));

  // Apply any constant bits found before elaboration. This must run
  // before resolve directives because it deletes the IODB symbol
  // table.
  mIODB->translateConstBits();
  mStats->printIntervalStatistics(CRYPT("STConstBits"));

  success = mIODB->resolveDirectives();
  mStats->printIntervalStatistics(CRYPT("ResolveDirectives"));

  mIODB->resolveInaccurate();
  mStats->printIntervalStatistics(CRYPT("ResolveInaccurate"));

  // Save off the elaborated ports into the database. This must be
  // run after resolveDirectives.
  mIODB->translatePrimaryPorts();
  mStats->printIntervalStatistics(CRYPT("STDesignPorts"));
 
  // Populate the design's additional outputs. This includes data from
  // the IODB and module internal nets that need to be made live.
  mIODB->populateDesignOutputs(mDesign, mCbuildSymTabBOM);
  mDesign->handleElabInternalNets();
  mStats->printIntervalStatistics(CRYPT("DesOutputs"));

  // Copy the design's user type information to the elaborated symbol table.
  mIODB->copyUserTypeInfo();

  // remove dead elab flow.
  CleanElabFlow cleanup(mFlowElabFactory,
                        mSymbolTable,
                        verboseCleanup);
  FLNodeElabSet deleted_flow_elabs;
  cleanup.design(mDesign, &deleted_flow_elabs);

  // Walk all the NUNetElabs and clear any references to deleted flow
  for (STSymbolTable::NodeLoop p = mSymbolTable->getNodeLoop(); !p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf != NULL) {
      NUNetElab* netElab = NUNetElab::find(leaf);
      if (netElab != NULL) {
        UInt32 numRemoved = netElab->removeContinuousDriverSet(deleted_flow_elabs);
        NU_ASSERT(numRemoved == 0,netElab);
        netElab->trimExcessCapacity();
        netElab->getNet()->trimExcessCapacity();
      }
    }
  }

  deleted_flow_elabs.clear();
  cleanup.design(mDesign, &deleted_flow_elabs);
  deleted_flow_elabs.clear();

  mStats->popIntervalTimer();

  return success;
}

bool CarbonContext::runDebugPrint()
{
  if (mArgs.getBoolValue(scDumpDesign))
    mDesign->print(true, 0);
  if (mArgs.getBoolValue(scDumpSymtab))
    mSymbolTable->print();
  if (mArgs.getBoolValue(scDumpElabFlow))
    mDesign->printElab();

  // This is a useful place for setting a breakpoint, and ensures
  // that the FlowGraph routines are available in the debugger.
#if !pfGCC_4_2
  UnelabFlowGraph::placeHolder();
  ElabFlowGraph::placeHolder();
#endif
  return true;
}

bool CarbonContext::runHierAlias() {
  int internal_assert_test = 0;
  ArgProc::OptionStateT state = mArgs.getIntLast(scInternalAssertTest, &internal_assert_test);
  if (state != ArgProc::eKnown) {
    internal_assert_test = 0;
  }

  Reduce reduce(mStats, mDesign,
                mArgs.getBoolValue(scVerboseCleanup),
                mArgs.getBoolValue(CRYPT("-verboseSanity")),
                mArgs.getBoolValue(scDumpXHier),
                mArgs.getBoolValue(scVerboseTriInit),
                mArgs.getBoolValue(scDumpAllocation),
                internal_assert_test,
                mFlowElabFactory,
                mFlowFactory,
                mReachableAliases,
                mTristateMode);
  return reduce.runHierAlias();
}

bool CarbonContext::runReduce() {
  int internal_assert_test = 0;
  ArgProc::OptionStateT state = mArgs.getIntLast(scInternalAssertTest, &internal_assert_test);
  if (state != ArgProc::eKnown) {
    internal_assert_test = 0;
  }

  Reduce reduce(mStats, mDesign,
                mArgs.getBoolValue(scVerboseCleanup),
                mArgs.getBoolValue(CRYPT("-verboseSanity")),
                mArgs.getBoolValue(scDumpXHier),
                mArgs.getBoolValue(scVerboseTriInit),
                mArgs.getBoolValue(scDumpAllocation),
                internal_assert_test,
                mFlowElabFactory,
                mFlowFactory,
                mReachableAliases,
                mTristateMode);
                
  bool ret = reduce.runReduce();

  if (mArgs.getBoolValue(scDumpNetFlags)) {
    dumpNetFlags();
  }

  if (mArgs.getBoolValue(scDumpVerilog)) {
    dumpVerilog("reduce");
  }
  NUBase::checkDesignWalker(mDesign, "reduce");

  mStats->popIntervalTimer();

  return ret;
} // bool CarbonContext::runReduce

struct CmpModules {
  bool operator ()(const NUModule* m1, const NUModule* m2) const {
    return *m1 < *m2;
  }
};

void CarbonContext::dumpNetFlags()
{
  NUModuleList mods;
  mDesign->getModulesBottomUp(&mods);
  UtVector<NUModule*> modv(mods.begin(), mods.end());
  std::sort(modv.begin(), modv.end(), CmpModules());
  for (NUModuleVectorLoop p = NUModuleVectorLoop(modv); !p.atEnd(); ++p) {
    NUModule* mod = *p;
    UtIO::cout() << "module " << mod->getName()->str() << ";" << UtIO::endl;
    for (NUModule::NetLoop q = mod->loopNets(); !q.atEnd(); ++q)
    {
      NUNet* net = *q;
      UtString buf;
      UtIndent indent(&buf);
      net->composeDeclaration(&buf);
      indent.tab(30);
      net->composeFlags(&buf);
#     define TEST_NETREF_ASSERT 0
#     if TEST_NETREF_ASSERT
      ConstantRange range(5, 0);
      NUNetRefHdl hdl = mNetRefFactory->createVectorNetRef(net, range);
#     endif
      UtIO::cout() << "  " << buf << UtIO::endl;
    }
    UtIO::cout() << "\n";
  }
}

bool CarbonContext::runSchedule()
{
  bool dumpSchedule = mArgs.getBoolValue(scDumpSchedule);
  bool ret = mSchedule->buildSchedule(mDesign, mStats, true,
                                      dumpSchedule, mGenerateCModelWrapper,
                                      mTristateMode,
                                      mFileRoot.c_str());
  if (ret) {
    if (dumpSchedule) {
      mSchedule->printScheduleStats(mFileRoot.c_str(), true);
      mSchedule->print();
    } else {
      bool dumpScheduleStats = mArgs.getBoolValue(scDumpScheduleStats);
      mSchedule->printScheduleStats(mFileRoot.c_str(), dumpScheduleStats);
    }
  }

  if (mArgs.getBoolValue(scDumpVerilog)) {
    dumpVerilog("schedule");
  }

  return ret;
}

bool CarbonContext::runBackend()
{
  // Before doing aggressive back-end optimizations, do a final
  // design flow walk to capture the elaborated bitwise read/written
  // database.  We will then use this after code generation, when
  // we discover which nets are wave-able, to print dead-net warnings
  // and mark them as X in the IODB.
  INFO_ASSERT(mPostSchedWarnings == NULL, "post-schedule warnings already allocated.");
  mIODB->findCollapseNets(mSymbolTable);
  mPostSchedWarnings = new RENetWarning(mNetRefFactory, mSymbolTable, mIODB,
                                        mMsgContext, mReachableAliases,
                                        (REAlias*) 0, // aliasing already done
                                        mDesign, &mArgs, mFlowElabFactory);
  mPostSchedWarnings->walkDesign();

  mBackend->analyzeDesign(mDesign, true, mStats);

  if (mArgs.getBoolValue(scDumpVerilog)) {
    dumpVerilog("backend");
  }

  return true;
}

bool CarbonContext::runLowCG ()
{
#if !CG_ENABLE
  mMsgContext->CGDisabled ();
  return false;
#else
  CG cg (this, mMsgContext);
  cg.generate (mDesign);
  return true;
#endif
}

bool CarbonContext::runCodeGen()
{

  bool old_noFoldBDD = mArgs.getBoolValue(CRYPT("-noFoldBDD"));
  if (mArgs.getBoolValue(CRYPT("-noFoldBDDCodegen"))){
    mArgs.setBoolValue(CRYPT("-foldBDD"), false);
  }

  Fold* fold = new Fold(mNetRefFactory,
                        &mArgs,
                        mMsgContext, mAtomicCache,
                        mIODB,
                        mArgs.getBoolValue (CRYPT("-verboseFold")),
                        eFoldUDValid | eFoldUDKiller | eFoldComputeUD |
                        eFoldIgnoreXZ | eFoldAggressive | eFoldBDDMux |
                        eFoldNoStmtDelete);

  int bdd_fold_trace = 0;
  (void) mArgs.getIntLast(CRYPT("-traceBDDFold"), &bdd_fold_trace);
  UInt32 old_value = fold->putTraceBDDFold(bdd_fold_trace);
  
  if (not mArgs.getBoolValue(CRYPT("-nofold"))) {
    fold->design(mDesign);
  }

  // Eventually I'd like to move the logic-to-LUT transformation earlier
  // in the compiler, but for now it's here, so I surrounded it with #if 1
  // so it'll be easy to find and delete later.
#if 1
  if (mArgs.getBoolValue(scEnableLogic2LUT)) {
    // The constant reductions done by the LUT construction engine
    // are not interesting, so don't report them even if -traceFold has
    // been specified.
    bool saveTrace = fold->putTraceFold(false);

    Logic2Lut l2l(mNetRefFactory, fold,
                  mFlowFactory,
                  &mArgs,
                  mMsgContext,
                  mIODB,
                  mAtomicCache,
                  mArgs.getBoolValue(scVerboseLogic2LUT),
                  mArgs.getBoolValue(scForceLogic2LUT));
    l2l.design(mDesign);

    fold->putTraceFold(saveTrace);
  }
#endif

  if (mArgs.getBoolValue(scDumpVerilog)) {
    dumpVerilog("codegen");
  }

  mCodeGen->putFold(fold);
  mCodeGen->generateCode(mSchedule, mDesign, mStats, true);
  mCodeGen->putFold(NULL);
  fold->putTraceBDDFold(old_value);
  delete fold;

  bool generatePliWrapper = mArgs.getBoolValue( CRYPT("-pliWrapper") );

  // Calculate sensitivities used by all wrappers.
  CNetSet bothEdgeClks;
  PortNetSet validOutPorts;
  PortNetSet validInPorts;
  PortNetSet validTriOutPorts;
  PortNetSet validBidiPorts;
  CNetSet clksUsedAsData;
  ClkToEvent clkEdgeUses;
  bool synthTristates = mArgs.getBoolValue(scSystemCTristates);
  
  bool generateWrapper = mGenerateSystemCWrapper || generatePliWrapper;
  bool err = mSchedule->wrapperSensitivity( mCodeGen->getPortIface(), bothEdgeClks,
                                            validOutPorts, validInPorts, validTriOutPorts,
                                            validBidiPorts, clksUsedAsData, clkEdgeUses, generateWrapper);

  mIODB->saveWrapperSensitivity(bothEdgeClks, validOutPorts, validInPorts, validTriOutPorts, validBidiPorts, clksUsedAsData, clkEdgeUses);

  if( generateWrapper ) {
    if( mGenerateSystemCWrapper && !err ) {

      // if the user doesn't want tristates, but the design has bidis,
      // warn that we must synthesize bidirects as tristates
      if (!synthTristates && (! validBidiPorts.empty()))
      {
        for (PortNetSet::iterator p = validBidiPorts.begin(); p != validBidiPorts.end(); ++p)
        {
          const CGPortIfaceNet* net = *p;
          mMsgContext->SchSysCBidiTristates(net->getNetElab());
        }
      }
      mMsgContext->SystemCWrapperDeprecated();
      mSchedule->generateSystemCWrapper( getFileRoot(), mArgs.getBoolValue(scWrapperSchedAllClkEdges),
                                         synthTristates,
                                         bothEdgeClks, validOutPorts, validInPorts, validTriOutPorts,
                                         validBidiPorts, clksUsedAsData, clkEdgeUses );
    }
    
    // Generate Verilog HDL wrapper module for the design
    if( generatePliWrapper && !err ) {
      mCodeGen->codePliInterface(mDesign, bothEdgeClks, validOutPorts, validInPorts,
                                 validTriOutPorts, validBidiPorts, clksUsedAsData,
                                 clkEdgeUses );
      mCodeGen->codePliVerilogWrapper( mDesign );
    }
    
  }
  UtString designId;
  mCodeGen->getUID(&designId);
  mIODB->putDesignId(designId);
  UtString targetName(*(mCodeGen->getTargetName()));
  if (strncmp(targetName.c_str(), "lib", 3) == 0)
    targetName.erase(0, 3);

  mIODB->putIfaceTag(targetName);

  if (mArgs.getBoolValue(scDumpCodegen))
  {
    UtIO::cout() << CRYPT("\n\n After CodeGen\n");
    mDesign->print(true,0);
    mSymbolTable->print ();
  }

  // At the last possible moment, remove any constants that we have
  // recorded on nets that proved mutable, or that disappeared from
  // the design due to nucleus rewrites.  Note that normally we would
  // leave all nets in the design, but if we make a temp, prove it
  // constant, and then rewrite that temp, then we need to remove it
  // from the constant database. 
  //
  // We do this here, and not right before codegen, because pack-assigns
  // deletes the usedef nodes that we need during RENetWarning's walk,
  // and that causes it to crash.

  if (mArgs.getBoolValue (scStrictIODB)) {
    mMsgContext->putMsgNumSeverity (MsgContext::e_IODBNoNetIntrinsics, MsgContextBase::eFatal);
  }

  INFO_ASSERT(mPostSchedWarnings, "post-schedule warnings not allocated");
  mPostSchedWarnings->printWarnings(RENetWarning::eWarnDead);
  mPostSchedWarnings->fixPartialDeadConstants();
  delete mPostSchedWarnings;
  mPostSchedWarnings = NULL; 
  mStats->printIntervalStatistics(CRYPT("NetWarnings"));

  // Do a final pass to declare dead bits -- nets where some of
  // the bits are not used, and their drivers have been dead-logic
  // eliminated, but other bits are used and driven.  So the nets
  // appear in waveforms (on behalf of their live bits) but some
  // of the bits are (perhaps surprisingly to the user) showing up as Z.
  mIODB->resolveConstants();

  // The BlobMap is deleted in this object's (CarbonContext's)
  // destructor, but this object doesn't get deleted in a product
  // build.  The BlobMap destructor does important things (writes out
  // its file), so we must ensure it's deleted.
  delete mBlobMap;
  mBlobMap = NULL;
  gCodeGen->getSchedBuckets()->record(); // Writes schedule profiling data to
                                         // .prof file.

  mArgs.setBoolValue(CRYPT("-noFoldBDD"), old_noFoldBDD);
  return true;
}

bool CarbonContext::runMarkConstants()
{
  /*
    if (phaseStats)
    mStats->pushIntervalTimer();
  */
  mSchedule->markWaveableNets();

  if (mArgs.getBoolValue(scDumpMarkConst))
  {
    UtIO::cout() << CRYPT("\n\n After Marking Constants\n");
    mSymbolTable->print ();
  }
  
  return true;
  /*
  if (phaseStats)
  {
    mStats->printIntervalStatistics("MarkConstants");
    mStats->popIntervalTimer();
  }
  */
}

bool CarbonContext::runBackendCompile()
{
  // At this point relinquish the license.
  /*
    The license currently uses pthreads to do heartbeats. We don't
  want this interfering with our fork of the make call. Anyway, a
  license is not needed to do the backend compile.
  */
  destroyLicense();

  UtString make_output_filename;
  if (isParseMakeOutputOnly (&make_output_filename)) {
    mCodeGen->parseMakeOutput (make_output_filename, /*debugging=*/ true, 
      /*inBuildDirectory=*/ false);
  } else {
    mCodeGen->compileSimulation();
  }

  /*
  if (phaseStats)
  {
    mStats->printIntervalStatistics("CG Make");
    mStats->popIntervalTimer();
  }
  */
  return true;
}

bool CarbonContext::runGuiDBFork ()
{
  mForkingWriteGuiDB = true;
  UtString errmsg;
  mVerboseGuiDB = true;
  if (mMsgContext->getExitStatus () != 0) {
    // do not bother with the fork if errors were found in an earlier phase
    UtIO::cout () << __FUNCTION__ << ": extant errors; not forking for GUI database\n";
    return true;
  }
#if GUIDBFORK_USES_STRDUP
  // construct a filename for the error file
  mWriteGUIOutputFile << mFileRoot.c_str () << ".gui.log";
  // The output of the libdesign.gui.db write subprocess is captured in a
  // file. I don't know how to do this on Windows so, for now, this is only
  // done on a Unix box.
  int fd;
  if ((fd = OSSysOpen (mWriteGUIOutputFile.c_str (), O_WRONLY|O_CREAT|O_TRUNC, 0644, &errmsg)) < 0) {
    // open the file before forking so that any errors opening the file are reported
    mMsgContext->GuiDBWriteError (mWriteGUIOutputFile.c_str (), errmsg.c_str ()); // fatal
  }
#endif
  if ((mWriteGuiDBPID = OSFork (&errmsg)) > 0) {
    // this is the parent process
    mPhaseFlow.push_back (eGuiDBWait);  // push the phase that waits for the writer to exit
#if GUIDBFORK_USES_STRDUP
    OSSysClose (fd, &errmsg);           // close the output file in the parent
#endif
    return true;
  } else if (mWriteGuiDBPID == 0) {
    // This is the child process that actually writes the GUI database.
    // Rewrite the phase flow vector with the GUI database writer flow.
    // Note: because we are shortening mPhaseFlow we will not invalidate any
    // extant iterators.
    PhaseFlow::iterator it = mPhaseFlow.begin ();
    while (it != mPhaseFlow.end () && *it != eGuiDBWait) {
      ++it;
    }
    INFO_ASSERT (it != mPhaseFlow.end (), "expected eGuiDBWait in phase flow");
    mPhaseFlow.erase (it, mPhaseFlow.end ());
    mPhaseFlow.push_back(eElaboratedAnalysis);
    mPhaseFlow.push_back(eGuiBuildSymtab);
    mPhaseFlow.push_back(eGuiDBWrite);
    mWritingGuiDB = true;
    // rename the file root so that the parent and child do not stomp on each
    // other.
    mFileRoot << "_child";
#if GUIDBFORK_USES_STRDUP
    // Redirect stdout and stderror to the output file. Not sure how this is
    // done in Windows, but given that this is intended only for testing just
    // do it for Unix.
    if (OSSysClose (1, &errmsg) < 0 || OSSysClose (2, &errmsg) < 0) {
      UtString msg;
      msg << "Failed to redirect output: " << errmsg;
      INFO_ASSERT (false, msg.c_str ());
    }
    if (dup2 (fd, 1) < 0 || dup2 (fd, 2) < 0) {
      // Redirection failed, who knows where stdout will go so try to write a
      // message to the output file before asserting out.
      UtString msg, buffer;
      msg << "dup2 failed redirecting child process output: " << OSGetLastErrmsg (&buffer);
      write (fd, msg.c_str (), msg.length ());
      INFO_ASSERT (false, "unable to redirect child process output");
    } else if (OSSysClose (fd, &errmsg) < 0) {
      UtString msg;
      msg << "Failed to close descriptor: " << errmsg;
      INFO_ASSERT (false, msg.c_str ());
    }
#endif
    return true;
  } else {
    // the fork failed
    mMsgContext->WriteGuiForkError (errmsg.c_str ()); // this is a fatal error
    return false;
  }
}

bool CarbonContext::runGuiDBWait ()
{
  INFO_ASSERT (mForkingWriteGuiDB && mWriteGuiDBPID > 0, "no child forked");
  int status;
  UtString error;
  if ((status = OSWaitPid (mWriteGuiDBPID, &error)) < 0) {
    // something went wrong with the wait
    mMsgContext->WriteGuiWaitError (error.c_str ()); // fatal
    return false;
  } else if (status > 0) {
    // the child terminated with a non-zero exit status
#if GUIDBFORK_USES_STRDUP
    // want the filename to be just the basename so that it does not introduce
    // a path dependency in the cbld.log file for testing.
    const char *trimmed = mWriteGUIOutputFile.c_str ();
    for (const char *p = mWriteGUIOutputFile.c_str (); *p != '\0'; p++) {
      if (*p == '/') {
        trimmed = p + 1;
      }
    }
    mMsgContext->GuiDBWriteFailed (trimmed);   // fatal
#else
    mMsgContext->GuiDBWriteFailed ("stdout");   // fatal
#endif
    return false;
  } else {
    // the child process successfully wrote a libdesign.gui.db file
    return true;
  }
}

bool CarbonContext::runGuiBuildSymtab ()
{
  // Decorate the symbol table with type information. This information is
  // required by the GUI but in the normal compile flow is not created until
  // codegen time.
  mCodeGen->updateTypeInformationFromSymtab (getSymbolTable ());
  return true;
}

bool CarbonContext::runGuiDBWrite()
{
  bool ret = true;
  if (mArgs.getBoolValue (scFailGuiDBWrite)) {
    mMsgContext->GuiDBWriteError (scFailGuiDBWrite, "forced failure");
  }
  if (mArgs.getBoolValue (scStrictIODB)) {
    // When -strictIODB is specified promote the GUI database writing errors
    // to fatal.
    mVerboseGuiDB = true;
    mMsgContext->putMsgNumSeverity (MsgContext::e_STCannotFindSourceLocation, MsgContextBase::eFatal);
    mMsgContext->putMsgNumSeverity (MsgContext::e_STCannotFindUnelaboratedNode, MsgContextBase::eFatal);
  }
  ret &=  mIODB->writeGuiDB (mFileRoot.c_str (), mCbuildSymTabBOM, mExprFactory, mVerboseGuiDB);
  // If this is a forked child process writing the GUI database then
  // immediately exit. Otherwise, execution will hang releasing he license when
  // exiting normally
  if (!mForkingWriteGuiDB) {
    // just a normal compilation
  } else if (mMsgContext->getExitStatus () != 0) {
    // errors were reported
    mMsgContext->MessageSummary (
      mMsgContext->getSeverityCount (MsgContext::eWarning),
        mMsgContext->getSeverityCount (MsgContext::eError),
        mMsgContext->getSeverityCount (MsgContext::eAlert));
    exit (mMsgContext->getExitStatus ());
  } else {
    exit (0);
  }
  return ret;
}

bool CarbonContext::runDBWrite()
{
  bool writeFullDB = ! mArgs.getBoolValue(scNoDB);
  bool strictIODB = mArgs.getBoolValue(scStrictIODB);
  bool ret =  mIODB->writeFiles(writeFullDB,
                                mArgs.getBoolValue(scIncludeTmps),
                                mFileRoot.c_str(), 
                                mCbuildSymTabBOM, 
                                mExprFactory, false, strictIODB);
  if (ret && mCodeGen->embedIODB()) {
    if (writeFullDB) {
      ret &= mCodeGen->encodeIODBFile(eCarbonFullDB);
    }
    ret &= mCodeGen->encodeIODBFile(eCarbonIODB);
  }
  return ret;
}

bool CarbonContext::runProtectSource()
{
  SourceProtector protector(mProtectedSourceExtension);

  bool quiet = mArgs.getBoolValue("-q");
  LangVer supportedVerilogVersions[] = { LangVer::Verilog, LangVer::Verilog2001 }; // the language versions currently supported

  HdlFileCollector::const_iterator lib_iter;
  for ( lib_iter = mFileCollector->begin(); lib_iter != mFileCollector->end(); ++lib_iter )
  {
    for(UInt32 langIndex = 0; langIndex < sizeof(supportedVerilogVersions)/sizeof(LangVer); ++langIndex) {
      LangVer langVersion = supportedVerilogVersions[langIndex];
      HdlLib::const_hdlFileIterator file_iter =  (*lib_iter)->begin(langVersion);
      for (; (file_iter != (*lib_iter)->end(langVersion)); ++file_iter )
      {
        if(!sIsOption(*file_iter)) {
          if (protector.protectFile(*file_iter, mMsgContext, mArgs.getBoolValue(scProtectHDLOnly)))
          {
            ++mProtectedSourceCount;
            if (!quiet)
            {
              mMsgContext->ProtectedFile(*file_iter);
            }
          }
        }
      }
    }
  }

  return true;
}

// inspect and report stats for SystemVerilog sources
bool CarbonContext::runSVInspector()
{
  bool inspectionStatus = false;

  Verific::VeriModule * topModule = mVerificDesignManager.GetTopModule();

  // use general purpose walker
  Verific::VerilogDesignWalker walker;

  int countLimit = 0;
  if (ArgProc::eKnown != mArgs.getIntFirst (scSVInspector, &countLimit)) {
    countLimit = 0;
  }

  // Create SystemVerilog inspection CallBack object
  SvinspectorCB svinspectorCB(countLimit);
  // tell walker about the inspection callback
  walker.sCallBack(&svinspectorCB);

  if (topModule){
      topModule->Accept(walker);
      inspectionStatus = true;
  }

  return inspectionStatus;
}

#ifdef INTERPRETER
bool CarbonContext::runInterpGen()
{
  if (mArgs.getBoolValue(scInterp))
    Interpreter::Populate(mDesign, mSymbolTable, mSchedule, mFileRoot.c_str(),
                          mMsgContext, mIODB, mAtomicCache, &mArgs,
                          mNetRefFactory, &mSourceLocatorFactory);
  return true;
}
#endif

void CarbonContext::doHeartbeat()
{
  mLicense->heartbeat();
}

bool CarbonContext::runPhase(Phase phase) {
  const char* phaseName = mPhaseNames[phase].c_str();
  return runOnePhase(mPhases[phase], phase, phaseName);
}

bool CarbonContext::runOnePhase(PhaseExec phase, Phase phase_id, const char * phase_name)
{
  bool result = true;

  doHeartbeat();

  /*
  ** Run the phase.
  */
  result = (this->*phase)();
  if (mMsgContext->getExitStatus() != 0 || result == false )
  {
    mMsgContext->PhaseFailure( mPhaseNamePhrases[phase_id].c_str(), mPhaseNames[ phase_id ].c_str() );
    result = false;
  }    

  /*
  ** Print the phase statistics if it was requested.
  */
  mStats->printIntervalStatistics(phase_name);


  // Do not do any sort of design walks if there was a failure in running
  // the phase.  The failure might have left an invalid nucleus tree and
  // we might crash
  if (result) {
    /*
    ** Print out early statistics.
    */
    if ((phase_id == mCostPhase) && (not mArgs.getBoolValue(scNoCosts)))
    {
      UtString fname;
      fname << mFileRoot << CRYPT(".costs");
      NUCostContext costs;
      costs.calcDesign(mDesign);
      costs.printCostTable(fname.c_str(),
                           mArgs.getBoolValue(scDeepCosts),
                           mArgs.getBoolValue(scCSVCosts),
                           mArgs.getBoolValue(scEstimateCPS));
    }


    if (mDesign != NULL) {
      NucleusSanity nucleusSanity;
      NucleusSanity::SanityStatus status = NucleusSanity::eAfterResynth;
      switch ( phase_id )
      {
      case ePrintCbuildBanner:
      case eProtectSource:
      case eCheckoutCbuildLicense:
      case eParseCmdline:
      case eParseAttributes:
      case eParseDirectives:
      case eParseLanguages:
      case eSVInspector:
      case eDumpHierarchy:
      case ePopulateNucleus:
      case eDumpDesignPopulate: 
      case eFindBlobs:
      case eEvalDesignDirectives:
        status = NucleusSanity::eBeforeResynth;
        break;
      case ePostPopulateResynth:
      case eMakeSizesExplicit:
      case eEarlyFlatteningAnalysis:
      case eGlobalOptimizations:
#ifdef CARBON_PV
      case eProgrammersView:
#endif
      case ePortAnalysis:
      case eLocalAnalysis:
      case ePostLocalAnalysis:
      case eElaboratedAnalysis:
      case eHierAlias:
      case eReduce:
      case eDebugPrint:
      case eGuiDBWrite:
      case eGuiDBFork:
      case eGuiDBWait:
      case eGuiBuildSymtab:
        status = NucleusSanity::eAfterResynth;
        break;
      case eSchedule:
      case eMarkConstants:
      case eBackend:
      case eCodeGen:
      case eDBWrite:
#ifdef INTERPRETER
      case eInterpGen:
#endif
      case eBackendCompile:
      case eLastPhase:
      case eLowCG:
        status = NucleusSanity::eAfterSchedule;
        break;
      }
      nucleusSanity.design(mDesign, status);
    }
  }

  return result;
}

//! Run though all compiler phases
bool CarbonContext::run()
{
  // signal handling doesn't work well with valgrind
  if (! CarbonMem::isValgrindRunning())
  {
    // setup sig handlers
    OSSigaction(SIGILL, sHandleSig);
    OSSigaction(SIGABRT, sHandleSig);
    OSSigaction(SIGFPE, sHandleSig);
    OSSigSegvaction(SIGSEGV, sHandleSigSegv);
    OSSigaction(SIGINT, sHandleSig);
    OSSigaction(SIGTERM, sHandleSig);
#if ! pfMSVC
	// SIGBUS doesn't exist on Windows.  Should we trap SIGBREAK on Windows?
    OSSigaction(SIGBUS, sHandleSig);
#endif
  }
  
  bool cont = true;

  mStats->pushIntervalTimer();

  /*
  ** Run to get the switches.  Force the explicitly check to see
  ** if we need to dump stats for the parse phase.
  */
  cont = runOnePhase(&CarbonContext::runParseCmdline, eParseCmdline,
                     mPhaseNames[eParseCmdline].c_str());
  bool phaseStats = mArgs.getBoolValue(scPhaseStats);
  const char* gatherStatsDir = mArgs.getStrValue(scGatherStatsDir);

  if (gatherStatsDir != NULL) {
    UtString pathname, err;
    sUniquifyFilename(gatherStatsDir, &pathname);

    if (!mStats->gatherStatistics(pathname.c_str(), &err)) {
      UtIO::cerr() << err << "\n";
      return false;
    }
  }
  else {
    mStats->loadStatistics(sCbuildStats);
  }

  mStats->putEchoStats(phaseStats);
  if (phaseStats) {
    mStats->putEchoProgress(true);
  }
  mStats->printIntervalStatistics(mPhaseNames[eParseCmdline].c_str());
  
  /*
  ** Setup for the tool we're running.
  */
  setupPhaseFlow();

  /*
  ** Setup occurrence logging in reduce.
  */
  OccurrenceLogger::Wrapper wrap_log (*getMsgContext (), mArgs, mFileRoot.c_str ());

  /*
  ** Run all the other phases.
  */
  for (PhaseFlowLoop i = PhaseFlowLoop(mPhaseFlow); cont and (not i.atEnd()); ++i)
  {
    Phase phase = *i;
    const char* phaseName = mPhaseNames[phase].c_str();
    cont = runOnePhase(mPhases[phase], phase, phaseName);

    if (mDumpMemory)
    {
      UtString fname;
      fname << mFileRoot << ".mem." << phaseName;
      CarbonMem::checkpoint(fname.c_str());
    }

    /*
    ** If the user asked us to stop early, do so.
    */
    if (phase == mLastPhase)
    {
      break;
    }
  }

  if ( mArgs.getBoolValue(scCreateASegfault) )
  {
    // this code is used to check that we have the proper signal
    // handler setup. It is placed here so that we are well within the
    // cbuild process, you can use lastPhase to check that the proper
    // handler is setup at the end of any particular phase.
    MsgContext *nullMsgContext(0);

    // the following line should segfault;
    nullMsgContext->CmdLineError(CRYPT("-createASegfault was on the command line."));
  }

  /*
  ** See if we should dump the final stats.
  */
  // Pop the compilation timer
  mStats->popIntervalTimer();
  mStats->printIntervalStatistics("cbuild");

  return cont;
}


//! get the FILE* for the error log
FILE* CarbonContext::getErrorFile()
{
  if (mLogStream != NULL)
    return mLogStream->mErrFile;
  return NULL;
}

//! get the FILE* for the warning log
FILE* CarbonContext::getWarningFile()
{
  if (mLogStream != NULL)
    return mLogStream->mWarnFile;
  return NULL;
}

//! get the FILE* for the suppress log
FILE* CarbonContext::getSuppressFile()
{
  if (mLogStream != NULL)
    return mLogStream->mSuppressFile;
  return NULL;
}

const char* CarbonContext::sDoCrypt(const char* file, UInt32 lineNumber)
{
  if (! sCarbonCfg)
  {
    sCarbonCfg = new CarbonCfg;
    sCarbonCfg->readDB();
  }

  return sCarbonCfg->getStr(file, lineNumber);
}

//! Specify the top-level module name (work around Cheetah bug)
void CarbonContext::putTopModuleName(const char* modname)
{
  mTopModuleName = modname;
}

//! Find the top-level module name (work around Cheetah bug)
const char*  CarbonContext::getTopModuleName()
  const
{
  if (mTopModuleName.empty())
    return NULL;
  return mTopModuleName.c_str();
}

void CarbonContext::logAnalyzerMessages(FILE* logFilePtr, MsgContextBase::Severity sev,
                         int msgNo, const char *sourceFileName,
                         unsigned long sourceLineNo,
                         const char *format, va_list pvar)
{
  // we send our msgs to stdout for better or worse
  logFilePtr = stdout;    
  UtString location;
  if (sourceFileName != NULL)
    location << sourceFileName << ":" << ((UInt32) sourceLineNo) << ": ";

  FILE* ccLog = NULL;

  // The MsgContext is not used to issue analyzer messages. Instead they're printed
  // out to logfile/stdout directly. The incrSeverityCount() method helps keep
  // MsgContext updated with message severity count. The processCallbacks() helps
  // serve callbacks user added to MsgContext before issuing the messages.
  getMsgContext()->incrSeverityCount(sev);
  {
    SourceLocator loc;
    if (sourceFileName != NULL) {
      loc = mSourceLocatorFactory.create(sourceFileName, sourceLineNo);
    } else {
      loc = mSourceLocatorFactory.create("unknown", 0);
    }
    UtString location;
    loc.compose(&location);
    MsgContextBase::MsgObject mo(msgNo, "AnalyzerMessage", sev, location.c_str(), format);
    // Return status is mostly used by processCallbacks itself, which avoids calling
    // any more callbacks if status is eCarbonMsgStop.
    getMsgContext()->processCallbacks(&mo);
  }

  UtString sevString;
  sevString.clear();
  switch (sev)
  {
  case MsgContextBase::eSuppress:
    ccLog = getSuppressFile();
    if (ccLog != NULL)
      fprintf(ccLog, "%sSuppress %d: ", location.c_str(), msgNo);
      break;
  case MsgContextBase::eNote:
  case MsgContextBase::eStatus:
  case MsgContextBase::eContinue:
    fprintf(logFilePtr, "%sNote %d: ", location.c_str(), msgNo);
    break;
  case MsgContextBase::eWarning:
    fprintf(logFilePtr, "%sWarning %d: ", location.c_str(), msgNo);
    ccLog = getWarningFile();
    if (ccLog != NULL)
      fprintf(ccLog, "%sWarning %d: ", location.c_str(), msgNo);
    break;
  // The 3 classes of error messages below have deliberate fallthrough
  // to shared code following all of them.
  case MsgContextBase::eFatal:
    sevString << "Fatal ";
    // deliberate fallthrough
  case MsgContextBase::eError:
    if ( sevString.empty())
      sevString << "Error";
    // deliberate fallthrough
  case MsgContextBase::eAlert:
    if ( sevString.empty())
      sevString << "Alert";
    fprintf(logFilePtr, "%s%s %d: ", location.c_str(), sevString.c_str(), msgNo);
    ccLog = getErrorFile();
    if (ccLog != NULL)
      fprintf(ccLog, "%s%s %d: ", location.c_str(), sevString.c_str(), msgNo);
    break;
  } // switch
  if (sev != MsgContextBase::eSuppress)
  {
    // ISO C doesn't allow using a va_list after passing it to another
    // function that uses va_arg() on it, so we need to copy pvar and pass
    // out the copy.
    va_list pvar_copy;
    // gcc 2.95 doesn't have va_copy() but has a __va_copy() macro.
#ifdef __va_copy
    __va_copy(pvar_copy, pvar);
#elif defined(va_copy)
    va_copy(pvar_copy, pvar);
#else
	// Windows doesn't have va_copy() or __va_copy().
	pvar_copy = pvar;
#endif

    vfprintf(logFilePtr, format, pvar_copy);
    fprintf(logFilePtr, "\n");
//    fflush(logFilePtr);  // no need to flush here, will be done on fileclose or by our signal handler on segv

#if defined(__va_copy) || defined(va_copy)
	// If we called va_copy() we need va_end().
    va_end(pvar_copy);
#endif
  }
  if (ccLog != NULL)
  {
    vfprintf(ccLog, format, pvar);
    fprintf(ccLog, "\n");
    // we need to flush this file because on a segfault this will be missed
    fflush(ccLog);
  }
  if (sev == MsgContextBase::eFatal)
    exit(1);
}

bool CarbonContext::showJaguarWarnings() const {
  return mJaguarSuppressDepth == 0;
}

void CarbonContext::pushSuppressJaguarWarnings() {
  ++mJaguarSuppressDepth;
}

void CarbonContext::popSuppressJaguarWarnings() {
  INFO_ASSERT(mJaguarSuppressDepth != 0, "CarbonContext::popSuppressJaguarWarnings: cannot pop empty stack");
  --mJaguarSuppressDepth;
}

bool CarbonContext::isVSPCompile() const
{
  return mArgs.getBoolValue(scVSP);
}

bool CarbonContext::isBackendCompileOnly() const
{
  UtString ignored;
  return mArgs.getBoolValue(scBackendCompileOnly) || isParseMakeOutputOnly (&ignored);
}

bool CarbonContext::isParseMakeOutputOnly(UtString *filename) const
{
  const char *value;
  if (mArgs.getStrValue (scParseMakeOutputOnly, &value) == ArgProc::eKnown) {
    *filename = value;
    return true;
  } else {
    return false;
  }
}

void CarbonContext::putInPrePopWalk(bool prePopWalk)
{
  mCurrentPrePopWalkFlag = prePopWalk;

  if (mCurrentPrePopWalkFlag) {
    mvvRegisterMessageHandler( mvv_prepop_spew, NULL, 0 );
  } else {
    mvvRegisterMessageHandler( mvv_spew, NULL, 0 );
  }
  
  mCheetahContext->putInPrePopWalk(mCurrentPrePopWalkFlag);
  mJaguarContext->putInPrePopWalk(mCurrentPrePopWalkFlag);
}

bool CarbonContext::isVerboseUniquify() const
{
  return mArgs.getBoolValue(scVerboseUniquify);
}


bool CarbonContext::isNewRecord() const
{
  // Old record mode is deprecated
  // Always return true.
  return true;
}

bool CarbonContext::isLibraryCompileOnly() const
{
  return ( (mArgs.getBoolValue(JaguarContext::scVhdlCompile) == true) ||
           (mArgs.getBoolValue(scCompileLibOnly) == true) );
}

// -noEncrypt overrides -generateCleartextSource.
bool CarbonContext::isGenerateCleartextSource() const
{
  return mArgs.getBoolValue(scGenerateCleartextSource)
    && ! mArgs.getBoolValue(CRYPT("-noEncrypt"));
}


bool CarbonContext::protectUnprotectFile(int argc, char* const* argv, bool*  isProtUnprot, UtString* errMsg)
{
  bool status = true;

  for (int i = 0; (i < argc) && (status == true); ++i)
  {
    if(strcmp(argv[i], "-fprotect") == 0)
    {
      UtString ext(".ef");
      SourceProtector file_protect(ext);
      status = file_protect.protectEFfile(argv[i+1], errMsg);
      *isProtUnprot = true;
      if(status == true)
        errMsg->append("The protected file was created.\n");
    }
    else if(strcmp(argv[i], "-funprotect") == 0)
    {
      UtString ext(".ori");
      SourceProtector file_protect(ext);
      status = file_protect.unprotectEFfile(argv[i+1], errMsg);
      *isProtUnprot = true;
      if(status == true)
        errMsg->append("The unprotected file was created.\n");
    }
  }

  return status;
}

bool CarbonContext::getEFfileContext(const char* file_name, UtString* buffer, UtString* errMsg)
{
  UtString ext("");
  SourceProtector file_open(ext);
  bool status = file_open.processEFfile(file_name, buffer, errMsg);

  return status;
}


bool CarbonContext::checkRemovedSwitches()
{
  // Check for all removed switches before returning
  bool found = false;

  // -simpleProfile was removed in favor of -profile
  if (mArgs.getBoolValue(scSimpleProfile)) {
    mMsgContext->RemovedSwitch(scSimpleProfile, CRYPT("Use -profile to capture profiling data."));
    found = true;
  }

  return found;
}


void CarbonContext::debugXmlWriteSymbolTables(STSymbolTable* declarationsSymTab,STSymbolTable* elaborationsSymTab)
{

  UtString targetFilePrefix(mFileRoot);
  UtString targetFileName;
  UtString tableName;
  UtString targetFileSuffix(".xml"); 
  UtLibXmlWriter* xmlWriter = new UtLibXmlWriter();
  UtArray<STSymbolTable*> prePopulationTablesVector;
  tableName = "PrePopulationDeclarations";
  declarationsSymTab->setName(tableName);
  tableName = "PrePopulationElaborations";
  elaborationsSymTab->setName(tableName);

  prePopulationTablesVector.push_back(declarationsSymTab);
  prePopulationTablesVector.push_back(elaborationsSymTab);

  targetFileName = targetFilePrefix + ".symbolTable.PrePopulation.xml";
  STSymbolTable::writeXml(xmlWriter,prePopulationTablesVector, targetFileName);



  IODBDesignDataSymTabs* symtabs    = getIODB()->getDesignDataSymbolTables();
  STSymbolTable* designElaboratedSymTab   = (STSymbolTable*)symtabs->getElabSymTab();
  STSymbolTable* designUnelaboratedSymTab = (STSymbolTable*)symtabs->getUnelabSymbolTable();
  UtArray<STSymbolTable*> designWalkTablesVector;
  tableName = "DesignWalkElaborated";
  designElaboratedSymTab->setName(tableName);
  tableName = "DesignWalkUnelaborated";
  designUnelaboratedSymTab->setName(tableName);

  designWalkTablesVector.push_back(designUnelaboratedSymTab);
  designWalkTablesVector.push_back(designElaboratedSymTab);


  targetFileName = targetFilePrefix + ".symbolTable.DesignWalk.xml";
  STSymbolTable::writeXml(xmlWriter,designWalkTablesVector, targetFileName);

  UtString targetModulePrefix = targetFilePrefix + ".symbolTable.Module.";      

  for (NUDesign::ModuleLoop p = mDesign->loopAllModules(); !p.atEnd(); ++p) {
      NUModule* mod = *p;
      const StringAtom* modAtom = mod->getName();
      const char* modName = modAtom->str();
      STSymbolTable* modSymTab = mod->getAliasDB();
      UtString targetModuleName(modName);
      targetFileName = targetModulePrefix + targetModuleName + targetFileSuffix;
      modSymTab->writeXml(xmlWriter,targetFileName);
  }
  delete xmlWriter;

  return;
} // void CarbonContext::xmlWriteSymbolTables

bool CarbonContext::doTimeBomb(UtCustomerDB::Signature* sig)
{
  bool result = false;

  const char* timeStr = mArgs.getStrValue(scTimeBomb);
  if (timeStr != NULL) {
    // Convert the time to a 64-bit integer
    SInt64 bomb;
    if (!TimebombHelper::sCreate64BitTimebomb(timeStr, &bomb)) {
      mMsgContext->TimeBombBadDate(timeStr);
    } else {
      // An internal license allows the creation of any timebomb.
      // Otherwise, a special timebomb-enabling license is required.
      bool dateCheckOK = mHaveDiagLicense;
      if (!dateCheckOK) {
        // Get the latest timebomb date in the same 64-bit form as the
        // requested one.  Also get the corresponding user-friendly
        // timebomb date string (for reporting out-of-range dates) and
        // the license feature to check out.
        TimebombHelper helper(mLicense);
        // Initialize to a user-friendly string.  If no timebomb
        // licenses exist, this is what will be reported as the latest
        // available timebomb date.
        UtString latestValidBombStr("undefined (Perhaps you don't have a timebomb license?)");
        UtString licenseFeature;
        SInt64 latestValidBomb = helper.getLatestTimebomb(&latestValidBombStr, &licenseFeature);

        // Make sure the requested date isn't too late
        if (bomb > latestValidBomb) {
          mMsgContext->TimeBombTooLate(timeStr, latestValidBombStr.c_str());
        } else {
          // Actually check out the license.
          UtString errorReason;
          if (!mLicense->checkoutFeatureName(licenseFeature.c_str(), &errorReason)) {
            reportVSPCheckoutFailure(errorReason);
          } else {
            // Date check succeeded
            dateCheckOK = true;
          }
        }
      }
      
      if (dateCheckOK) {
        // For timebombed models, if Replay or OnDemand is requested
        // at compile time, we need to do an existence check of the
        // corresponding runtime licenses.  This is to support a
        // marketing requirement that Replay/OnDemand licensees who
        // also have the right to create timebombed models can create
        // Replay- and OnDemand-enabled timebombed models.
        //
        // As with most things, an internal license lets you do
        // whatever you want.
        UtString reason;
        bool replayCheckOK = mHaveDiagLicense;
        bool onDemandCheckOK = mHaveDiagLicense;
        if (!replayCheckOK) {
          if (!mArgs.getBoolValue(scDisableReplay) && mArgs.getBoolValue(scEnableReplay) &&
              !mLicense->doesFeatureExist(UtLicense::eVSPReplay, &reason)) {
            mMsgContext->TimeBombNoAddonFeature("Replay");
          } else {
            replayCheckOK = true;
          }
        }
        if (!onDemandCheckOK) {
          if (mArgs.getBoolValue(scEnableOnDemand) && !mLicense->doesFeatureExist(UtLicense::eOnDemand, &reason)) {
            mMsgContext->TimeBombNoAddonFeature("OnDemand");
          } else {
            onDemandCheckOK = true;
          }
        }

        // If all the checks passed, we can set the timebomb
        if (replayCheckOK && onDemandCheckOK) {
          sig->putTimeBomb(bomb);
          result = true;
        }
      }
    }
  } else {
    // No timebomb was requested, so report success
    result = true;
  }

  return result;
}

//Check vhdlTop 
bool CarbonContext::checkVhdlOnTop() {
    bool vhdlOnTop;
    vhdlOnTop = ( mArgs.getStrValue( JaguarContext::scVhdlTop ) != NULL );

    bool hasLibMap = (getArgs()->isParsed("-libMap") == ArgProc::eParsed);
  
    if ( vhdlOnTop == false &&
       ! getFileCollector()->hasHdlFiles(LangVer::Verilog) &&
       ! getFileCollector()->hasHdlFiles(LangVer::Verilog2001) &&
       ! getFileCollector()->hasLangLib(LangVer::Verilog) &&
       ! hasLibMap)
    {
        // Must have exactly one top module--we have none
        mMsgContext->NoTopModule();
        return false;
    }
    return true;
}

//Check -vhdlCase option
void CarbonContext::checkVhdlCase()
{
    const char * vhdlCaseValue = mArgs.getStrValue("-vhdlCase");
    gMakeVhdlIdsLower = false;
    gMakeVhdlIdsUpper = false;
    if (vhdlCaseValue) {
        if (!strcmp(vhdlCaseValue,"lower")) {
            gMakeVhdlIdsLower=true;
        }
        else if (!strcmp(vhdlCaseValue,"upper")) {
            gMakeVhdlIdsUpper=true;
        }
    }
    else {
        //default
        gMakeVhdlIdsLower=true;
     }
}
//RJC RC#2013/08/27 (lilit)
// what is the goal of this method? it appears to be a wrapper for the compiletoparsetree method, but the name of this method says something
//about create Lang ver(ilog?) ver(sion?) to File name? can you explain the name and document this method?  The doc in the .h file says this creates a
//filenam/language pair but it looks like this actually does parsing/analyze/elaboration?
// method: createLangVer2FileName
// returns true if successful
bool CarbonContext::callVerificParserAndStaticElaboration(LangVer langVer, bool autoCompileOrder, bool compileOnly, bool hasVerilogLib, bool hasVhdlLib,
                                           bool hasLibmap, bool hasVhdlNoWriteLib) {

    //UtMap for keeping filename and LangVer pair      
    UtMap<UtString,unsigned> filename2langver;
    //UtVector for keeping libName and libPath
    UtVector<UtPair<UtString,UtString> > libNameLibPath;

    UtMap<UtString, UtString> fileName2LibName;
    // Read cmd file
    UtString filelist;
    // May need mFileRoot.ncmd (non-expanded version). TODO need to check with Rich
    //RJC RC#2013/08/27 (frunze) Yes we will need a .ncmd version of the cmd file
    // FD. OK, switching .ncmd should be carefully tested will mark addressed as soon as I get it done. 
    // I would suggest asking Lilit doing this (as part of Infrastructure work), verilog,vhdl,mixed language cases should be tested for that purpose
     filelist << mFileRoot << ".cmd";

    bool assumeVerilogFilesAreSV = isSystemVerilogMode();
    //Verific::veri_file veri_reader;
    //bool seenHdlFile = false;
    
    for (HdlFileCollector::const_hdlLibListIterator hdlLibIter = getFileCollector() -> begin();
              hdlLibIter != getFileCollector() -> end(); 
              ++hdlLibIter)
      {
          HdlLib* hdlLib = (*hdlLibIter);

          UtString libName;
          UtString libPath;
          UtString libFullName;
          libName = UtString(hdlLib->getLogicalLibrary());
          libPath = UtString(hdlLib->getLibPath());
          libFullName = UtString(libPath + libName);
          UtPair<UtString, UtString> libNameLibPathPair(libName,libPath);
          libNameLibPath.push_back(libNameLibPathPair);
          //UtIO::cout() << "-lib " << libName << ":" << libPath << UtIO::endl;
          for ( UInt32 ver = 0; ver < LangVer::LangVer_MAX; ++ver) {
              LangVer langVer(ver);
              if ( hdlLib->getNumberOfFiles( langVer ) ) {
                  // Apparently that's not true run can have no Hdl but have Libs and top module specified and should be processed correctly
                  //seenHdlFile = true;
                  if ( LangVer::VHDL87 == langVer ) {
                     //UtIO::cout() << "-87" << UtIO::endl;
                     //vhdlLib
                     hasVhdlLib = (getArgs()->isParsed("-vhdlLib") == ArgProc::eParsed);
                  }
                  else if ( LangVer::VHDL93 == langVer ) {
                      ///UtIO::cout() << "-93" << UtIO::endl;
                     //vhdlLib
                     hasVhdlLib = (getArgs()->isParsed("-vhdlLib") == ArgProc::eParsed);
                  } 
                  else 
                  {
                     hasVhdlLib = (getArgs()->isParsed("-vhdlLib") == ArgProc::eParsed);
                  }
                  if ( LangVer::Verilog2001 == langVer ) {
                     //vlogLib
                     hasVerilogLib = (getArgs()->isParsed("-vlogLib") == ArgProc::eParsed);
                  }
                  else if ( LangVer::Verilog == langVer ) {
                    //vlogLib
                     hasVerilogLib = (getArgs()->isParsed("-vlogLib") == ArgProc::eParsed);
                  }
                  /// Register each file with it's type
                  for (HdlLib::const_hdlFileIterator fileIter = hdlLib->begin(langVer); 
                          fileIter != hdlLib->end(langVer); 
                          ++fileIter) {
                      UtString file_name(*fileIter);
                      fileName2LibName[file_name] = libName;
                      {
                          bool thisIsAVHDLFile = true;
                          unsigned f_analysis_mode = mVerificDesignManager.convertToVerificLanguageVersion(langVer, &thisIsAVHDLFile);
                          filename2langver[file_name] = f_analysis_mode;
                      }
                      mVerificDesignManager.RegisterFile(file_name, langVer, libFullName);
                  }
              }
          }
      }
    
    // Process filelist using Verific XL compilance , Analyze/Elaborate
    return mVerificDesignManager.CompileToParseTree(filelist.c_str(), assumeVerilogFilesAreSV, langVer, filename2langver, 
                                                    autoCompileOrder, mFileRoot,libNameLibPath, fileName2LibName, compileOnly,
                                                    hasVerilogLib, hasVhdlLib, hasLibmap, hasVhdlNoWriteLib);
}

//Takes list of extensions used to identify vhdl
//files. The '.' in the extension is optional. The list of extensions
//augments the .vhd and .vhdl extensions.
void CarbonContext::analyzeAndRegisterValueForVhdlExt()
{
        //analyze and register value for -vhdlExt option
        ArgProc::StrIter iter;
        mArgs.getStrIter("-vhdlExt", &iter);
        char* ptVal = 0;
        while (iter(&ptVal)) {
            UtString val = UtString(ptVal);
            std::cout << "val = " << val.data() << std::endl;
            char dot = '.';
            size_t pos = 0;
            size_t sz = val.size();
            std::cout << "size of val sz = " << sz << std::endl;
            while (pos < sz) {
                std::cout <<"Start to search from pos = " <<pos << std::endl;
                size_t p = val.find_first_of(":", pos, 1);
                std::cout << "the position of : p =  " << p << std::endl;
                //single extension eg -vhdlExt .ext1 (.ext1 case)
                //or 
                //last extension in the chain eg -vhdlExt .ext1:.ext2:.ext3 (.ext3 case)
                if (UtString::npos == p) {
                     std::cout << "Don't found :" << std::endl;
                        UtString str = val.substr(pos, sz - pos);
                        std::cout << "str = " << str.data() << std::endl;
                        //add '.' in front of extension value if missing
                        if (dot != str[0]) {
                            std::cout << "Create . string" << std::endl;
                            UtString optVal(".");
                            optVal += str;
                            str = optVal;
                        }
                        mVerificDesignManager.setVhdlExt(str);
                        //get out of loop because we process single extension or last extension 
                        //specification in chain
                        //eg -vhdlExt .myext
                        //   -vhdlExt myext1:myext2 (chained)
                        break;
                //process current extension eg -vhdlExt .ext1:.ext2:.ext3 (.ext1 and .ext2 case)
                } else {
                  std::cout << "found :" << std::endl;
                  UtString str = val.substr(pos, p - pos);
                  std::cout << "str = " << str.data() << std::endl;
                    //add '.' in front of extension value if missing
                   if (dot != str[0]) {
                       std::cout << "Create . string" << std::endl;
                       UtString optVal(".");
                       optVal += str;
                       str = optVal;
                   }
                   mVerificDesignManager.setVhdlExt(str);
                   pos = p + 1;
                }
            }
        }
}



bool CarbonContext::usingVerilog2005StylePowerOperator()
{

  bool languageSaysUse2005Style = getArgs()->getBoolValue(CarbonContext::scSVerilog);

  // The following check for user specified switch specification must check both the enable/disable version of the switches.
  // However getting the value it is only necessary that one switch be checked.
  bool userSpecifiedSwitchForStyle  = ((getArgs()->isParsed(CarbonContext::scEnable2005StylePowerOperator) == ArgProc::eParsed) ||
                                       (getArgs()->isParsed(CarbonContext::scDisable2005StylePowerOperator) == ArgProc::eParsed)  );
  bool userRequested2005Style = getArgs()->getBoolValue(CarbonContext::scEnable2005StylePowerOperator);

  bool using2005Style = ( ( languageSaysUse2005Style && !userSpecifiedSwitchForStyle ) ||
                          ( userSpecifiedSwitchForStyle && userRequested2005Style ) );

  return using2005Style;
}
