// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __ATTRIBUTE_PARSER_H__
#define __ATTRIBUTE_PARSER_H__

#include "util/UtString.h"

class IODBNucleus;
typedef struct _xmlError xmlError;

class AttributeParser
{
public:
  CARBONMEM_OVERRIDES

  AttributeParser(IODBNucleus* iodb);
  ~AttributeParser();

  //! Open and parse an attribute file, returning success
  bool parse(const char* file);
  //! Return the error string after a failed parse
  const char* getError() { return mErrStr.c_str(); }


protected:
  IODBNucleus *mIODB;
  UtString mErrStr;

  //! Error reporting function
  static void sErrorHandler(void* data, xmlError* error);
};

#endif
