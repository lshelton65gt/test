// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/ProtectedSource.h"
#include "util/Zstream.h"
#include <time.h>
#include "util/RandomValGen.h"
#include "util/CbuildMsgContext.h"
#include "util/CarbonAssert.h"
#include "util/UtStringUtil.h"
#include "util/UtIStream.h"

extern int Protect_lex();  // flex-generated scanner
extern FILE *Protect_in;   // input stream used by Protect_lex()
extern FILE *Protect_out;  // output stream used by Protect_lex()

// 11/8/05 -- added a version number to the protection scheme.  This
// will help ensure that older readers cannot read encrypted files produced
// by newer writers.  Prior to this date, the header delimeter was '@'.
// Newer readers will be able to continue to read @-delimited headers,
// which lack a version number.  
// NOTE if you change PROTECT_VERSION you should see at least
// test/protected-source/version_check_future fail.
//  See comments in the file for the steps needed for the updates.
#define PROTECT_VERSION 3
#define OLD_DELIM '@'
#define NEW_DELIM '$'

ProtectedSource::ProtectedSource() {
  // on first invocation, seed random number generator with current time.
  mRandom = new RandomValGen(time(NULL));
}

ProtectedSource::~ProtectedSource() {
  delete mRandom;
}

static const char sHeaderChars[] = "01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
static const size_t sNumHeaderChars = sizeof(sHeaderChars) - 1;

bool ProtectedSource::protect()
{
  UtString buf, encrypted;

  // A header consisting of 40 random characters followed by an $version$
  // preceeds the Verilog source to make it more difficult for people to crack
  // the encryption algorithm.
  for (int i = 0; i < 40; ++i)
  {
    buf << sHeaderChars[mRandom->URRandom(0, sNumHeaderChars - 1)];
  }
  buf << NEW_DELIM;     // prevent old readers from reading new protected blocks
  buf << PROTECT_VERSION << NEW_DELIM;
  buf << c_str();

  // encrypt the text
  {
    ZOSTRINGSTREAM(zbuf, &encrypted);
    zbuf.write(buf.c_str(), buf.length());
    zbuf.flush();
  }

  // convert to ASCII, saving result in this
  clear();
  StringUtil::asciiEncode(encrypted.data(), encrypted.length(), this);

  return true;
}

bool ProtectedSource::unprotect()
{
  UtString buf;

  // convert to binary and decrypt
  if(!StringUtil::asciiDecode(c_str(), length(), &buf)) {
    return false;
  }

  {
    clear();
    UtIStringStream iss(buf);
    ZISTREAM(zbuf, &iss);
    char charBuf[65536];
    do
    {
      UInt32 numRead = zbuf.read(charBuf, sizeof(charBuf));
      append(charBuf, numRead);
    } while (! zbuf.eof());
  }

  // strip off the header
  UtString::size_type atPos = find_first_not_of(sHeaderChars);
  if (atPos == UtString::npos) {
    return false;
  }
  const char* p = c_str() + atPos;
  if (*p == NEW_DELIM) {
    ++p;  // move past the delimeter

    // New delimeters are followed by version numbers.  We should not
    // read newer protected blocks into older readers
    
    const char* delim = strchr(p, NEW_DELIM);
    if (delim == NULL) {
      return false;
    }
    UtString number(p, delim - p);
    UInt32 version;
    if (!StringUtil::parseNumber(number.c_str(), &version)) {
      return false;
    }
    if (version > PROTECT_VERSION) {
      // Do not allow older software to read newer protected blocks
      // Should we print an error here beyond "invalid encryption syntax"?
      return false;
    }
    atPos = delim - c_str();
    erase(0, atPos + 1); // remove header and NEW_DELIM
  } else if (*p == OLD_DELIM) {
    erase(0, atPos + 1); // remove header and OLD_DELIM
  } else {
    // there is no delim character (this form is used by -fprotect)
  }

  return true;
} // bool ProtectedSource::unprotect

///////////////////////////////////////////////////////////////////////////////
void sCloseFiles()
{
  if (Protect_in != NULL)
  {
    fclose(Protect_in);
    Protect_in = NULL;
  }
  if (Protect_out != NULL)
  {
    fclose(Protect_out);
    Protect_out = NULL;
  }
}

SourceProtector* SourceProtector::currentProtector = NULL;
const char *SourceProtector::currentFileName;
MsgContext *SourceProtector::currentMsgContext;

SourceProtector::SourceProtector(UtString &extension):mExposeProtected(false),
                                                      mExtension(extension)
{
  Protect_in = NULL;
  Protect_out = NULL;
}

SourceProtector::~SourceProtector()
{
  sCloseFiles();
}

bool SourceProtector::protectFile(const char *fileName, MsgContext *msgContext,
                                  bool exposeProtected)
{
  bool ok = true;

  // Protect_lex() needs a pointer to us to call methods during analysis.
  // It also needs the current file name and message context for errors.
  INFO_ASSERT(currentProtector == NULL, "only one SourceProtector may be active at a time");
  currentProtector = this;
  currentFileName = fileName;
  currentMsgContext = msgContext;

  // Ensure a clean initial state.
  mExposeProtected = exposeProtected; // True if +protectHDLOnly
  mEncrypt = false;
  mProtectBuf.clear();
  sCloseFiles();

  // set up the input and output FILE pointers
  ok = openFile(fileName);

  // call lex(), which will call back the write() and setEncrypt() methods
  // as it analyzes the input file.
  if (ok)
    Protect_lex();

  // Protect_lex() is done.
  currentProtector = NULL;

  return ok;
}

// Called by Protect_lex(); write encrypted or unencrypted text out to the
// protected version of the source file.
void SourceProtector::write(const char *text, int len)
{
  if (mEncrypt)
  {
    mProtectBuf.append(text, len);
  }
  else
  {
    fwrite(text, len, 1, Protect_out);
  }
}

// Called by Protect_lex() when it finds `protect and `unprotect in the input
// to toggle the output mode.
void SourceProtector::setEncrypt(bool encrypt)
{
  if (mEncrypt != encrypt)
  {
    mEncrypt = encrypt;

    if (encrypt)
    {
      // Encryption => on - set up to start encrypting text.
      INFO_ASSERT(mProtectBuf.length() == 0,
                  "protected text buffer must be empty at encryption start");
    }
    else
    {
      // Encryption => off - encrypt buffer, convert to ASCII, and append to 
      // output file.
      bool status = mProtectBuf.protect();

      if ( status ) {
        // The @ delimeter here is not related to OLD_DELIM or NEW_DELIM
        // defined in this .cxx file, but to the buf[0]=='@' comparison in
        // CheetahContext::decrypt.
        fprintf(Protect_out, "@");
        fwrite(mProtectBuf.data(), mProtectBuf.length(), 1, Protect_out);
      }
      mProtectBuf.clear();
    }
  }
}

// Open the given input and the matching protected output file.
// The globals Protect_in and Protect_out are used by the flex-generated
// scanner to read/write.
bool SourceProtector::openFile(const char *fileName)
{
  UtString errMessage, statResult;
  if ((Protect_in = OSFOpen(fileName, "r", &errMessage)) == NULL)
    return false;

  UtString outFileName(fileName);
  UtString::size_type dot = outFileName.rfind('.', outFileName.length() -1);
  if (dot != UtString::npos)
    outFileName = outFileName.substr(0, dot);
  outFileName += mExtension;
  if (OSStatFile(outFileName.c_str(), "e", &statResult) != 0)
    currentMsgContext->ProtectedFileExists(outFileName.c_str());
  else
    Protect_out = OSFOpen(outFileName.c_str(), "w", &errMessage);

  if (Protect_out == NULL)
  {
    fclose(Protect_in);
    Protect_in = NULL;
    return false;
  }
  return true; // success
}

bool SourceProtector::protectEFfile(const char *fileName, UtString* errMsg)
{
  if(openProtUnprotFile(fileName, errMsg) == false)
    return false;

  SInt64 size = 0;
  if (OSGetFileSize(fileName, &size, errMsg) == false) 
    return false;

  UtString buffer(size+1, '\0');
  fread(buffer.getBuffer(), size, 1, Protect_in);

  mProtectBuf.append(buffer.getBuffer(), size+1);
  bool status = mProtectBuf.protect();

  if ( status ) {
    fwrite(mProtectBuf.data(), mProtectBuf.length(), 1, Protect_out);
  } else {
    *errMsg << "Error during protect processing";
  }
  mProtectBuf.clear();

  return true;
}

bool SourceProtector::unprotectEFfile(const char *fileName, UtString* errMsg)
{
  if(openProtUnprotFile(fileName, errMsg) == false)
    return false;

  SInt64 size = 0;
  if (OSGetFileSize(fileName, &size, errMsg) == false) 
    return false;

  UtString buffer(size+1, '\0');
  fread(buffer.getBuffer(), size, 1, Protect_in);

  mProtectBuf.append(buffer.getBuffer());
  bool status = mProtectBuf.unprotect();

  if ( status ) {
    fwrite(mProtectBuf.data(), mProtectBuf.length(), 1, Protect_out);
  } else {
    *errMsg << "Error during unprotect processing";
  }
  mProtectBuf.clear();

  return status;
}

bool SourceProtector::openProtUnprotFile(const char *fileName, UtString* errMsg)
{
  UtString statResult;
  if ((Protect_in = OSFOpen(fileName, "r", errMsg)) == NULL)
    return false;

  UtString outFileName(fileName);
  UtString::size_type dot = outFileName.rfind('.', outFileName.length() -1);
  if (dot != UtString::npos)
    outFileName = outFileName.substr(0, dot);
  outFileName += mExtension;
  if (OSStatFile(outFileName.c_str(), "e", &statResult) != 0)
  {
    errMsg->append(outFileName.c_str());
    errMsg->append(" file already exists.\n");
  }
  else
    Protect_out = OSFOpen(outFileName.c_str(), "w", errMsg);

  if (Protect_out == NULL)
  {
    fclose(Protect_in);
    Protect_in = NULL;
    return false;
  }
  return true; // success
}


bool SourceProtector::processEFfile(const char *fileName, UtString* unprotected_buffer, UtString* errMsg)
{
  // Check that file exists
  if ((Protect_in = OSFOpen(fileName, "r", errMsg)) == NULL)
    return false;

  // Get the size
  SInt64 size = 0;
  if (OSGetFileSize(fileName, &size, errMsg) == false) 
    return false;

  // Create a buffer for that size and read it
  UtString buffer(size+1, '\0');
  fread(buffer.getBuffer(), size, 1, Protect_in);

  // Unprotect it
  mProtectBuf.append(buffer.getBuffer());
  bool status = mProtectBuf.unprotect();

  if ( status ){
    unprotected_buffer->append(mProtectBuf.getBuffer());
  } else {
    *errMsg << "Error during unprotect processing";
  }
  mProtectBuf.clear();

  return status;
}
