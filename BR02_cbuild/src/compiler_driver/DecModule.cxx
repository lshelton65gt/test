// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/* changes to this file were derived from Interra supplied examples */
/****************************************************************
 ***************
 *****  FILE  : moduleDecomp.c
 *****  AUTHOR :Punit Sindhwani
 *****  DATE   :16/07/96
 *****  RCS  HISTORY :

 *****  BRIEF DESC : This file defines the routines for decompiling
       the module and related verilog constructs

       *****************************************************************
       ***************/



#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "Decompile.h"
#include "util/CarbonAssert.h"
#include "util/SourceLocator.h"
#include "util/CbuildMsgContext.h"
#include "util/UtHashSet.h"
#include "compiler_driver/Interra.h"
#include "compiler_driver/CheetahContext.h"


/* Module decompiler routine  */

/*****
      Function      :moduleDecompile
      
      Purpose      :Module decompiler function
      
      Parameters     :veNode ::Pointer to object of type module
      
      Returns      :void
      
      End Header
*****/


void
Decompile::moduleDecompile (veNode module)
{
  // If this module came from a CEM file, then just ask for the CEM
  // file via protected block
  if ((mClearOutputFile != NULL) && (module != NULL)) {
    bool wasProtected = mInProtectedBlock;
    if (mInProtectedBlock) {
      endProtection();
      flushProtected();
    }      
    CheetahStr filename(veNodeGetFileName(module));
    UInt32 line = veNodeGetLineNumber(module);
    UtString cemName;
    if (mCheetahContext->isCEMFile(filename, line, &cemName)) {
      if (veModuleGetIsLibrary(module)) {
        // If the .cem file was pulled in via a -v file, then
        // token generation better generate it that way too.
        mCheetahContext->putIsCemDashV(cemName.c_str());
      }
      return;
    }
    if (wasProtected) {
      beginProtection();
    }
  } // if

  Protector protector(this, module);

  /* Declarations */
  CheetahList     portiter,  orderiter;
  veNode          nodeport;
  veNode          exprport;
  veNode          nodebase;
  CheetahList     attrInstanceList;
  
  
  /* see if is a timescale directive */
  veString timeUnit = veModuleGetTimeUnit(module);
  veString timePrecision = veModuleGetTimePrecision(module);
  
  if ( timeUnit && timePrecision )
  {
    *this << eTimescale;
    writeOutput( timeUnit, 1);
    *this << eSlash;
    writeOutput( timePrecision, 1);
    *this << eNewline;
  }
  
  /* see if this cell needs a celldefine */
  if (veModuleGetIsCellDefined (module))
    *this << eCellDefine;
  
  *this << eNewline;    
  if ((attrInstanceList = veNodeGetAttrInstance(module))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  if (veModuleGetIsMacroModule (module) == VE_TRUE)
    *this << eMacroModule;
  else
    *this << eModule;
  
  CheetahStr mname(veModuleGetName(module));
  obfuscate(mname);
  
  /* print ports now */
  portiter = veModuleGetPortList (module);
  if (portiter)
  {
    *this << eOpenParen;
    nodeport = veListGetNextNode (portiter);
    if (nodeport)
      portDecompile(nodeport);
    nodeport = veListGetNextNode (portiter);
    for (; nodeport; nodeport = veListGetNextNode (portiter))
    {
      *this << eComma;
      if (CheetahStr(vePortGetName (nodeport)))
        portDecompile (nodeport);
      else
        /* for the case of unnamed ports */
      {
        *this << eSpace;
        exprport = vePortGetNetExpr(nodeport);
        if (exprport)
          exprDecompile (exprport);
      }
    }
    *this << eCloseParen;
  }
  *this << eSemiColon << eNewline;
  
  // If the module name was uniquified by uselib, then leave
  // behind a comment directive so that we can note that when
  // we parse the token file downstream
  if (mClearOutputFile != NULL) {
    CheetahStr origName(veModuleGetOriginalName(module));
    if (strcmp(origName, mname) != 0) {
      UtString buf;
      buf <<  "  // carbon originalName " << mname << "  " << origName;
      writeOutput(buf.c_str(), 0);
    }
  }


  /*
   * decompile the declarations and statements. Declarations are
   * written out in the order that they appeared in the input source
   */
  
  orderiter = veModuleGetOrderedList (module);
  if (orderiter)
  {
    nodebase = veListGetNextNode (orderiter);
    for (; nodebase; nodebase = veListGetNextNode (orderiter))
    {
      switch (veNodeGetObjType (nodebase))
      {
      case VE_PARAM:
      {
        paramDecompile (nodebase);
        break;
      }
      case VE_LOCALPARAM:
      {
        localparamDecompile (nodebase);
        break;
      }
      case VE_VARIABLE:
      case VE_MEMORY:
      case VE_INTEGER:
      case VE_INTORTIMEARRAY:
      case VE_REAL:
      case VE_TIME:
      case VE_EVENT:
      case  VE_ARRAY:
      case VE_REG:
      {
        varDecompile (nodebase);
        break;
      }

      case VE_NETARRAY:
      {
        netArrayDecompile(nodebase);
        break;
      }

      case VE_NET:
      {
        netDecompile (nodebase);
        break;
      }
      case VE_REALTIME:
      {
        realtimedecompile (nodebase);
        break;
      }
      
      case VE_DEFPARAM:
      {
        defParamDecompile (nodebase);
        *this << eSemiColon << eNewline;
        break;
      }
      
      
      case VE_CONTASSIGN:
      {
        contAssignDecompile (nodebase);
        *this << eSemiColon << eNewline;
        break;
      }
      case VE_SPECIFYBLOCK:
      {
        specBlkDecompile (nodebase);
        *this << eNewline;
        break;
      }
      case VE_INITIAL:
      {
        initialDecompile (nodebase);
        break;
      }
      case VE_GENVAR:
      {
        genvarDecompile (nodebase);
        break;
      }
      case VE_GENERATE:
      {
        generateDecompile (nodebase);
        break;
      }
      
      case VE_ALWAYS:
      {
        alwaysDecompile (nodebase);
        break;
      }
      case VE_TASK:
      {
        taskDecompile (nodebase);
        break;
      }
      
      case VE_FUNCTION:
      {
        functionDecompile (nodebase);
        break;
      }
      case VE_MODULEORUDPINSTANCE:
      {
        moduleOrUdpInstanceDecompile (nodebase);
        *this << eSemiColon << eNewline;
        break;
      }
      case VE_GATEINSTANCE:
      {
        gateInstanceDecompile (nodebase);
        *this << eSemiColon << eNewline;
        break;
      }
      
      
      default :
        break;
      }    /*****Switch ends here   *****/
    }
    
  }
  *this << eEndModule << eNewline;
  
  if (veModuleGetIsCellDefined (module))
    *this << eEndCellDefine;
}




/*****
      Function      :configDecompile
      
      Purpose      :Configure decompiler function
      
      Parameters     :veNode ::Pointer to object of type veConfig
      
      Returns      :void
      
      End Header
*****/

void
Decompile::configDecompile (veNode configNode){
  Protector protector(this, configNode);

  CheetahList instanceList;
  CheetahList cellList;
  CheetahList defaultLibList;
  veNode instance;
  veNode cell;
  CheetahList libList;
  veNode lib;
  veNode defaultLib;
  veNode    topDesign;
  veString  useCellName;
  veString  useConfigName;
  CheetahList    topDesignList;
  
  *this << eConfig;
  obfuscate(CheetahStr(veNamedObjectGetName(configNode)));
  *this << eNewline << eDesign << eSpace;
  topDesignList = veConfigGetTopLevel (configNode);
  while ((topDesign = veListGetNextNode(topDesignList))) {
    obfuscate(veStringGetString(topDesign));
  }
  *this << eSemiColon << eNewline;
  
  instanceList = veConfigGetInstanceList (configNode);
  cellList = veConfigGetCellList (configNode);
  defaultLibList = veConfigGetDefaultLibList (configNode);
  
  if (defaultLibList) {    
    *this << eDefault << eLibList;
    while ((defaultLib = veListGetNextNode (defaultLibList))) {
      obfuscate(veStringGetString(defaultLib));
      *this << eSpace;
    }
    *this << eSemiColon << eNewline;
  }
  
  if (instanceList) {
    while ((instance = veListGetNextNode (instanceList))) {
      *this << eInstance;
      obfuscate(CheetahStr(veConfigInstanceGetName (instance)));
      libList = veConfigInstanceGetLibList (instance);
      if (libList) {
        *this << eLibList;
        while ((lib = veListGetNextNode (libList)))  { 
          obfuscate((char*) lib);
          *this << eSpace;
        }                    
        *this << eSemiColon << eNewline;
      }
      useCellName = veConfigInstanceGetUseCellName (instance);
      if (useCellName) {
        *this << eUse;
        obfuscate(useCellName);
        *this << eSemiColon << eNewline;
      }
      useConfigName = veConfigInstanceGetUseConfigName (instance);
      if (useConfigName) {
        *this << eUse;
        obfuscate(useConfigName);
        *this << eColon << eConfig << eSemiColon << eNewline;
      }
    }
  }
  if (cellList) {
    while ((cell = veListGetNextNode (cellList))) {
      *this << eCell;
      CheetahStr cellName(veConfigCellGetName (cell));
      obfuscate(cellName);
      libList = veConfigCellGetLibList (cell);
      if (libList) {
        *this << eLibList;
        while ((lib = veListGetNextNode (libList))) {
          obfuscate((char*) lib);
          *this << eSpace;
        }
        *this << eSemiColon << eNewline;
      }
      useCellName = veConfigCellGetUseCellName (cell);
      if (useCellName) {
        *this << eUse;
        obfuscate(useCellName);
        *this << eSemiColon << eNewline;
      }
      useConfigName = veConfigCellGetUseConfigName (cell);
      if (useConfigName) {
        *this << eUse << eColon << eConfig << eSemiColon << eNewline;
      }
    }
  }
  
  *this << eEndConfig;
}


/*****
      Function      :generateDecompile
      
      Purpose      :decompiler for the object of type VeGenerate
      
      Parameters :veNode ::VeGenerate  Object type pointer
      
      Returns      :void
      
      End Header
*****/
void
Decompile::generateDecompile (veNode generateNode) {
  Protector protector(this, generateNode);

  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(generateNode))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eGenerate;

  generateItemDecompile (generateNode);
  
  *this << eEndGenerate;
}


/*****
      Function      :generateItemDecompile
      
      Purpose      :decompiler for the object of type VeGenerate
      
      Parameters :veNode ::VeGenerateItem  Object type pointer
      [i.e. VE_GENERATE_BLOCK,VE_GENERATE_FOR,VE_GENERATE_IF,VE_GENERATE_CASE etc....]
      
      Returns      :void
      
      End Header
*****/

void
Decompile::generateItemDecompile (veNode generateItem){
  Protector protector(this, generateItem);

  switch (veNodeGetObjType (generateItem)){
  case VE_GENERATE_FOR:
  {
    generateForDecompile (generateItem);    
    break;
  }
  case VE_GENERATE_IF:
  {
    generateIfDecompile (generateItem);
    break;
  }
  case VE_GENERATE_CASE:
  {
    generateCaseDecompile (generateItem); 
    break;
  }
  case VE_GENERATE_BLOCK:
  {
    generateBlockDecompile (generateItem);
    break;
  }  
  case VE_CONTASSIGN:
  {
    contAssignDecompile (generateItem);
    *this << eSemiColon << eNewline;
    break;
  }
  case VE_ALWAYS:
  {
    alwaysDecompile (generateItem);
    break;
  }
  case VE_MODULEORUDPINSTANCE:
  {
    moduleOrUdpInstanceDecompile (generateItem);
    *this << eSemiColon << eNewline;
    break;
  }
  case VE_INITIAL:
  {
    initialDecompile (generateItem);
    break;
  }
  case VE_TASK:
  {
    taskDecompile (generateItem);
    break;
  }
  case VE_FUNCTION:
  {
    functionDecompile (generateItem);
    break;
  }
  case VE_GENVAR:
  {
    genvarDecompile (generateItem);
    break;
  }
  case VE_DEFPARAM:
  {
    defParamDecompile (generateItem);
    *this << eSemiColon << eNewline;
    break;
  }
  case VE_LOCALPARAM:
  {
    localparamDecompile (generateItem);
    break;
  }
  case VE_GATEINSTANCE:
  {
    gateInstanceDecompile (generateItem);
    *this << eSemiColon << eNewline;
    break;
  }
  case VE_VARIABLE:
  case VE_MEMORY:
  case VE_INTEGER:
  case VE_INTORTIMEARRAY:
  case VE_REAL:
  case VE_TIME:
  case VE_EVENT:
  case VE_ARRAY:
  case VE_REG:
  {
    varDecompile (generateItem);
    break;
  }
  case VE_NETARRAY:
  {
    netArrayDecompile(generateItem);
  }
  case VE_NET:
  {
    netDecompile (generateItem);
    break;
  }
  case VE_REALTIME:
  {
    realtimedecompile (generateItem);   
    break;
  }
  default :
    break;
  }
  
}



/*****
      Function      :genvarDecompile
      
      Purpose      :decompiler for the object of type VeVar
      
      Parameters :veNode ::VeVar Object type pointer
      
      Returns      :void
      
      End Header
*****/
void
Decompile::genvarDecompile (veNode genvarNode) {
  Protector protector(this, genvarNode);

  *this << eGenVar;
  obfuscate( CheetahStr(veNamedObjectGetName (genvarNode) ));
  *this << eSemiColon << eNewline;
  
}




/*****
      Function      :portDecompile
      
      Purpose      :decompiler for the object of type VePort
      
      Parameters :veNode ::VePort Object type pointer
      
      Returns      :void
      
      End Header
*****/


void
Decompile::portDecompile (veNode portnode)
{
  Protector protector(this, portnode);

  CheetahStr        name(vePortGetName (portnode));
  veNode          netexpr;
  
  netexpr = vePortGetNetExpr(portnode);
  
  if (vePortGetIsNamedPort (portnode))
  {
    *this << ePeriod;
    obfuscate( name);
    *this << eOpenParen;
    netexpr = vePortGetNetExpr(portnode);
    if (netexpr)
    {
      exprDecompile (netexpr);
    }
    *this << eCloseParen;
  } else
    /***if it's not named port  *******/
    
  {
    if (netexpr)
      exprDecompile (netexpr);
    else
    {
      if (name)
        obfuscate( name);
      else
        *this << eSpace;
    }
  }
}




/*****
      Function      :exprDecompile
      
      Purpose      :decompiler for  expression
      
      Parameter      :Pointer to the function of type VeExpr or Derieved from VeExpr
      
      Returns      :void
      
      End Header
*****/





void
Decompile::exprDecompile (veNode expr)
{
  Protector protector(this, expr);

  veObjType   ObjType;
  
  ObjType = veNodeGetObjType (expr);
  switch (ObjType)
  {
  case VE_UNARY:
  {
    unaryDecompile (expr);
    break;
  }
  case VE_BINARY:
  {
    binaryDecompile (expr);
    break;
  }
  case VE_CONCAT:
  {
    concatDecompile (expr);
    break;
  }
  case VE_MULTIPLECONCAT:
  {
    mulConcatDecompile (expr);
    break;
  }
  case VE_BITSELECT:
  {
    bitSelectDecompile (expr);
    break;
  }
  
  case VE_ARRAYPARTSELECT:
  {
    arrayPartSelectDecompile (expr);
    break;
  }
  case VE_ARRAYBITSELECT:
  {
    arrayBitSelectDecompile (expr);
    break;
  }
  case VE_PARTSELECT:
  {
    partSelectDecompile (expr);
    break;
  }
  case VE_CONDOP:
  {
    condOpDecompile (expr);
    break;
  }
  case VE_CONST:
  {
    constDecompile (expr);
    break;
  }
  case VE_STRING:
  {
    stringDecompile (expr);
    break;
  }
  case VE_BASEDNUMBER:
  {
    basedNumberDecompile (expr);
    break;
  }
  case VE_FLOATNUMBER:
  {
    floatNumberDecompile (expr);
    break;
  }
  case VE_EXPNUMBER:
  {
    expNumberDecompile (expr);
    break;
  }
  case VE_MINTYPMAXEXPR:
  {
    minTypMaxExprDecompile (expr);
    break;
  }
  case VE_NAMEDOBJECTUSE:
  {
    namedObjectUseDecompile (expr);
    break;
  }
  case VE_REPEAT_OR_DELAY:
  {
    repeatOrDelayDecompile (expr);
    break;
  }
  case VE_EVENT_CONTROL:
  {
    eventControlDecompile (expr);
    break;
  }
  case VE_POSE_SCALAR_EVENT_EXPR:
  {
    posEdgeExprDecompile (expr);
    break;
  }
  case VE_NEG_SCALAR_EVENT_EXPR:
  {
    negEdgeExprDecompile (expr);
    break;
  }
  case VE_FUNCTION_CALL:
  {
    functionCallDecompile (expr);
    break;
  }
  case VE_SCOPEVARIABLE:
  {
    scopevariableDecompile (expr);
    break;
  }
  case VE_SYS_FUNC_CALL:
  {
    sysFunctionCallDecompile (expr);
    break;
  }
  default :
    break;
  }
}








/*****
      Function      :unaryDecompile
      
      Purpose      :Decompiler for the object of type Ve_Unary
      
      Parameters     :Pointer to the object of type VeUnary
      
      Returns      :void
      
      End Header
*****/



void
Decompile::unaryDecompile (veNode unary)
{
  Protector protector(this, unary);

  veBuiltinObjectType   ObjType;
  veNode                operand;
  CheetahList     attrInstanceList;
  
  ObjType = veUnaryGetOpType (unary);
  switch (ObjType)
  {
  case UNARY_PLUS_USE:
  {
    *this << '(' << '+';
    break;
  }
  case UNARY_MINUS_USE:
  {
    *this << '(' << '-';
    break;
  }
  case LOGICAL_NEGATION_USE:
  {
    *this << '(' << '!';
    break;
  }
  case REDUCT_AND_USE:
  {
    *this << '(' << '&';
    break;
  }
  case REDUCT_OR_USE:
  {
    *this << '(' << '|';
    break;
  }
  case REDUCT_XOR_USE:
  {
    *this << '(' << '^';
    break;
  }
  case BITWISE_NEGATION_USE:
  {
    *this << '(' << '~';
    break;
  }
  case BITWISE_XOR_USE:
  {
    *this << '(' << '^';
    break;
  }
  case REDUCT_NAND_USE:
  {
    *this << ' ' << '(' << '~' << '&';
    break;
  }
  case REDUCT_NOR_USE:
  {
    *this << ' ' << '(' << '~' << '|';
    break;
  }
  case REDUCT_XNOR_OR_BIT_EQUIVAL_USE:
  {
    *this << ' ' << '(' << '~' << '^';
    break;
  }
  default :
    break;
  }
  
  if ((attrInstanceList = veNodeGetAttrInstance(unary))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  operand = veUnaryGetOperand (unary);
  if (operand)
    exprDecompile (operand);
  *this << eCloseParen;
} // Decompile::unaryDecompile




/*****
      Function      :binaryDecompile
      
      Purpose      :Decompiler for the object of type Ve_Binary
      
      Parameters     :pointer to object of type Ve_Binary
      
      Returns      :void
      
      End Header
*****/



void
Decompile::binaryDecompile (veNode binary)
{
  Protector protector(this, binary);

  veBuiltinObjectType ObjType;
  veNode              leftOperand, rightOperand;
  CheetahList     attrInstanceList;
  
  *this << eOpenParen;
  leftOperand = veBinaryGetLeftOp (binary);
  exprDecompile (leftOperand);
  
  ObjType = veBinaryGetOpType (binary);
  switch (ObjType)
  {
    
  case POWER_USE:
  {
    *this << ' ' << '*' << '*' << ' ';
    break;
  }
  
  case PLUS_USE:
  {
    *this << ' ' << '+' << ' ';
    break;
  }
  case MINUS_USE:
  {
    *this << ' ' << '-' << ' ';
    break;
  }
  case STAR_USE:
  {
    *this << ' ' << '*' << ' ';
    break;
  }
  case DIV_USE:
  {
    *this << ' ' << '/' << ' ';
    break;
  }
  case PERCENT_USE:
  {
    *this << ' ' << '%' << ' ';
    break;
  }
  case LOGICAL_EQUAL_USE:
  {
    *this << ' ' << '=' << '=' << ' ';
    break;
  }
  case LOGICAL_INEQUAL_USE:
  {
    *this << ' ' << '!' << '=' << ' ';
    break;
  }
  case CASE_EQUAL_USE:
  {
    *this << ' ' << '=' << '=' << '=' << ' ';
    break;
  }
  case CASE_INEQUAL_USE:
  {
    *this << ' ' << '!' << '=' << '=' << ' ';
    break;
  }
  case LOGICAL_OR_USE:
  {
    *this << ' ' << '|' << '|' << ' ';
    break;
  }
  case LOGICAL_AND_USE:
  {
    *this << ' ' << '&' << '&' << ' ';
    break;
  }
  case LT_USE:
  {
    *this << ' ' << '<' << ' ';
    break;
  }
  case GT_USE:
  {
    *this << ' ' << '>' << ' ';
    break;
  }
  case LT_EQUAL_USE:
  {
    *this << ' ' << '<' << '=' << ' ';
    break;
  }
  case GT_EQUAL_USE:
  {
    *this << ' ' << '>' << '=' << ' ';
    break;
  }
  case REDUCT_XNOR_OR_BIT_EQUIVAL_USE:
  {
    *this << ' ' << '~' << '^' << ' ';
    break;
  }
  case LEFT_SHIFT_USE:
  {
    *this << ' ' << '<' << '<' << ' ';
    break;
  }
  case RIGHT_SHIFT_USE:
  {
    *this << ' ' << '>' << '>' << ' ';
    break;
  }
  case ARITH_LEFT_SHIFT_USE:
  {
    *this << ' ' << '<' << '<' << '<' << ' ';
    break;
  }
  case ARITH_RIGHT_SHIFT_USE:
  {
    *this << ' ' << '>' << '>' << '>' << ' ';
    break;
  }
  case BITWISE_AND_USE:
  {
    *this << ' ' << '&' << ' ';
    break;
  }
  case BITWISE_OR_USE:
  {
    *this << ' ' << '|' << ' ';
    break;
  }
  case BITWISE_XOR_USE:
  {
    *this << ' ' << '^' << ' ';
    break;
  }
  default :
    break;
  } // switch
  
  if ((attrInstanceList = veNodeGetAttrInstance(binary))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  rightOperand = veBinaryGetRightOp (binary);
  exprDecompile (rightOperand);
  *this << eCloseParen;
} // Decompile::binaryDecompile







/*****
      Function      :concatDecompile
      
      Purpose      :Decompiler for the object of type VeConcat
      
      Parameters :Pointer to the object of type VeConcat
      
      Returns      :void
      
      End Header
*****/

void
Decompile::concatDecompile (veNode concat)
{
  Protector protector(this, concat);

  CheetahList     concatlist;
  veNode          funcexpr;
  
  concatlist = veConcatGetConcatList (concat);
  *this << eOpenBracket;
  funcexpr = veListGetNextNode (concatlist);
  if (funcexpr)
    exprDecompile (funcexpr);
  
  funcexpr = veListGetNextNode (concatlist);
  for (; funcexpr; funcexpr = veListGetNextNode (concatlist))
  {
    *this << eComma;
    exprDecompile (funcexpr);
  }
  
  *this << eCloseBracket;
}








/*****
      Function      : mulConcatDecompile
      
      Purpose      :Decompiler for the object of type VeMultipleConact
      
      Parameters     :Pointer to the object of type VeMultipleConcat
      
      Returns      :void
      
      End Header
*****/


void
Decompile::mulConcatDecompile (veNode mulconcat)
{
  Protector protector(this, mulconcat);

  veNode            commonexpr;
  veNode funcexpr = NULL;
  CheetahList       concatlist;
  
  commonexpr = veMultiConcatGetCommonExpr (mulconcat);
  *this << eOpenBracket;
  if (commonexpr)
    exprDecompile (commonexpr);
  
  concatlist = veConcatGetConcatList (mulconcat);
  *this << eOpenBracket;
  if (concatlist)
    funcexpr = veListGetNextNode (concatlist);
  
  if (funcexpr)
    exprDecompile (funcexpr);
  
  funcexpr = veListGetNextNode (concatlist);
  for (; funcexpr; funcexpr = veListGetNextNode (concatlist))
  {
    *this << eComma;
    exprDecompile (funcexpr);
  }
  
  *this << eCloseBracket;
  *this << eCloseBracket;
  
}

//! put the name of cheetah \a obj into \a buf
/*! returns true if the object had a name, false otherwise */
bool gCheetahGetName(UtString* buf, veNode obj)
{
  if ( obj == NULL ){
    return false;
  }
  veObjType type = veNodeGetObjType(obj);
  switch (type) {
  case VE_SCOPEVARIABLE:{
    CheetahStr cstr(veScopeVariableGetHierName(obj));
    *buf = cstr;
    return true;
  }
  case VE_NET: {
    CheetahStr cstr(veNetGetName(obj));
    *buf = cstr;
    return true;
  }
  case VE_MODULE: {
    CheetahStr cstr(veModuleGetName(obj));
    *buf = cstr;
    return true;
  }
  case VE_INSTANCE:
  case VE_MODULEORUDPINSTANCE:
  case VE_GATEINSTANCE:
  case VE_CONFIG_INSTANCE:{
    CheetahStr cstr(veInstanceGetName(obj));
    *buf = cstr;
    return true;
  }
  case VE_TASK_ENABLE: {
    CheetahStr cstr(veNamedObjectGetName(veTaskEnableGetTask(obj)));
    *buf = cstr;
    return true;
  }
  case VE_REAL:
  case VE_INTEGER:
  case VE_TIME:
  case VE_REALTIME:
  case VE_TASK:
  case VE_FUNCTION:
  case VE_MEMORY:
  case VE_INTORTIMEARRAY:
  case VE_ARRAY:
  case VE_NETARRAY:
  case VE_GENVAR:
  case VE_NAMEDOBJECT:
  {
    //    CheetahStr name(veVarGetName(ve_node));
    CheetahStr cstr(veNamedObjectGetName(obj));
    *buf = cstr;
    return true;
  }
  case VE_PORT:
  {
    //    CheetahStr name(veVarGetName(ve_node));
    CheetahStr cstr(veNamedObjectGetName(obj));
    if (cstr) {
      *buf = cstr;
      return true;
    } else {
      // must be an unnamed port (e.g. a concat in formal list)
      return false;
    }
  }
  case VE_BLOCK:
  case VE_PAR_BLOCK:
  case VE_SEQ_BLOCK: {
    CheetahStr cstr(veBlockGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    }
    return false;               // no name

  }
  case VE_UDP: {
    CheetahStr cstr(veUDPGetName(obj));
    *buf = cstr;
    return true;
  }
  case VE_CONST: {
    *buf << veConstGetDecNumber(obj);
    return false;               // not a name
  }
  case VE_RANGE: {
    gCheetahGetName(buf, veRangeGetLeftRange(obj));
    *buf << ":";
    gCheetahGetName(buf, veRangeGetRightRange(obj));
    return false;               // not a name
  }
  case VE_REG:{
    CheetahStr cstr(veRegGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }
  case VE_VARIABLE:
  case VE_EVENT:
  {
    CheetahStr cstr(veVarGetName(obj));
    *buf = cstr;
    return true;
  }
  case VE_PARAM:{
    CheetahStr cstr(veParamGetName(obj));
    *buf = cstr;
    return true;
  }
  case VE_LOCALPARAM: {
    CheetahStr cstr(veNamedObjectGetName(obj));
    *buf = cstr;
    return true;
  }
  case VE_PARAMCONN: {
    CheetahStr cstr(veParamConnGetParamName(obj));
    *buf = cstr;
    return true;
  }

  case VE_GENERATE_BLOCK: {
    CheetahStr cstr(veGenerateBlockGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }

  case VE_FILE: {
    CheetahStr cstr(veFileGetName(obj));
    *buf = cstr;
    return true;
  }
    
  case VE_ATTRSPEC:{
    CheetahStr cstr(veAttrSpecGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }

  case VE_NETBIT:{
    CheetahStr cstr(veNetbitGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }
  case VE_DEFPARAM:{
    CheetahStr cstr(veDefParamGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }
  case VE_SPECPARAM:{
    CheetahStr cstr(veSpecParamGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }
  case VE_CONFIG_CELL:{
    CheetahStr cstr(veConfigCellGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }
  case VE_CONFIG:{
    CheetahStr cstr(veConfigGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }
  case VE_SYSTIMINGCHECK:{
    CheetahStr cstr(veSysTimeChkGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }
  case VE_SYS_FUNC_CALL:{
    CheetahStr cstr(veSysFuncCallGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }
  case VE_SYS_TASK_ENABLE:{
    CheetahStr cstr(veSysTaskEnableGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }
  case VE_MACRO:    
  case VE_COMPILER_DIRECTIVE:{
    CheetahStr cstr(veDefineGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }
  case VE_TFARG:
  case VE_TF:{
    CheetahStr cstr(veNamedObjectGetName(obj));
    if ( cstr ) {
      *buf = cstr;
      return true;
    } else {
      return false;             // no name
    }
  }
    
  case VE_TABLEENTRY:
  case VE_COMBINATIONALENTRY:
  case VE_SEQUENTIALENTRY:
  case VE_UDPINPUTLIST:
  case VE_UDPEDGEINPUTLIST:
  case VE_UDPLEVELINPUTLIST:
  case VE_EDGESYMBOL:
  case VE_EDGESYMBOLTYPE1:
  case VE_EDGESYMBOLTYPE2:
  case VE_LEVELSYMBOL:
  case VE_OUTPUTSYMBOL:
  case VE_EXPR:
  {
    // no routine to get the name, 
    return false;               // has no name
  }


  case VE_OPERATOR:
  case VE_UNARY:
  case VE_BINARY:
  case VE_CONCAT:
  case VE_MULTIPLECONCAT:
  case VE_BITSELECT:
  case VE_PARTSELECT:
  case VE_CONDOP:
  case VE_LITERAL:
  case VE_STRING:
  case VE_BASEDNUMBER:
  case VE_EXPNUMBER:
  case VE_FLOATNUMBER:
  case VE_MINTYPMAXEXPR:
  case VE_INITALWAYS:
  case VE_INITIAL:
  case VE_ALWAYS:
  case VE_STATEMENT:
  case VE_ASSIGN:
  case VE_SPECIFYBLOCK:
  case VE_PATH:
  case VE_LEVELPATH:
  case VE_EDGEPATH:
  case VE_SYSTIMINGEVENT:
  case VE_TIMINGEVENTCONTROL:
  case VE_ROOT:
  case VE_ATTRIBUTEUSE:
  case VE_ATTRIBUTEDECL:

  case VE_INSTANCEATTRIBUTE:
  case VE_ATTRIBUTECLASS:
  case VE_NAMEDOBJECTUSE:
  case VE_PORTCONNECTION:
  case VE_CONTASSIGN:
  case VE_LIST:
  case VE_LISTNODE:
  case VE_ITERATOR:
  case VE_IF:
  case VE_FOREVER:
  case VE_REPEAT:
  case VE_WHILE:
  case VE_FOR:
  case VE_WAIT:
  case VE_DELAY_OR_EVENT:
  case VE_CASEITEM:
  case VE_DISABLE:
  case VE_FORCE:
  case VE_RELEASE:
  case VE_DEASSIGN:
  case VE_QUASI_CONT_ASSIGN:
  case VE_FUNCTION_CALL:
  case VE_REPEAT_OR_DELAY:
  case VE_NON_BLOCK_ASSIGN:
  case VE_BLOCK_ASSIGN:
  case VE_CASE:
  case VE_CASEZ:
  case VE_CASEX:
  case VE_EVENT_TRIGGER:
  case VE_EVENT_CONTROL:
  case VE_POSE_SCALAR_EVENT_EXPR:
  case VE_NEG_SCALAR_EVENT_EXPR:
  case VE_SYNTHDIRECTIVE:
  case VE_MEMBASE:
  case VE_SYMTABENTRY:
  case VE_SYMTAB:
  case VE_EDGE_DESCRIPTOR:
  case VE_MACRO_PARAM:
  case VE_MACRO_TEXT:
  case VE_MACROUSEINFO:
  case VE_PROC_ASSIGN:
  case VE_MODULE_TEMPLATE:
  case VE_LIBRARY_FILE_TEMPLATE:
  case VE_TIMING_EVENT:
  case VE_COMMANDLINE_FILE:
  case VE_COMMANDLINE:
  case VE_FLAG:
  case VE_DATAFLAG:
  case VE_SOURCEINFO:
  case VE_IFDEFEDOUT_RANGE:
  case VE_PORTBIT:
  case VE_TERMBIT:
  case VE_BASEDNUMBERBIT:
  case VE_STACK:
  case VE_PROTECTED_REGION:
  case VE_COMMENT:
  case VE_SCANOBJ:
  case VE_GENSYMTAB:
  case VE_GENSYMTABENTRY:
  case VE_VALUEOBJECT:
  case VE_GENERATE:
  case VE_GENERATE_FOR:
  case VE_GENERATE_IF:
  case VE_GENERATE_CASE:
  case VE_GENERATE_CASE_ITEM:
  case VE_USERATTRIBUTEBLOCK:
  case VE_LIBRARY:
  case VE_LIBRARYMANAGER:
  case VE_CONFIG_LIBRARY:
  case VE_CONFIG_LIBMAP_MANAGER:
  case VE_LIBINFO:
  case VE_INSTINFO:
  case VE_HASHARRAY:
  case VE_USERMETACOMMENT:
  case VE_ARRAYBITSELECT:
  case VE_ARRAYPARTSELECT:
  case VE_USERATTRIBUTE:
  case VE_ATTRTABLE:
  case VE_WANTEDINSTINFO:
  case VE_PULSESTYLE:
  case VE_SHOWCANCELLED:
  case VE_ATTRINSTANCE:
  case VE_INTEGERSTACK:
  case VE_PHYSICALLIBRARY:
  case VE_LISTSYMTAB:
  case VE_SYNTHPREFIX:
  case VE_LINEFILEDIRECTIVE:
  case VE_CHARSTRING:
  case VE_IDENTIFIER:
  case VE_USERPRAGMACOMMENT:
  case VE_USERPRAGMACOMMENTMANAGER:
  case VE_CONFIGINFO:
  case VE_END_UNSET_OBJECT: {
    // fall through
  }


  case UNSET_OBJECT:
  case VE_BASE:
  default:

    CheetahStr filename( veNodeGetFileName(obj) );
    SInt32 line = veNodeGetLineNumber(obj);
    *buf << filename << ":" << line << ": unexpected object type: " << gGetObjTypeStr(type) << " ";
    INFO_ASSERT(0, buf->c_str());
    break;
  }
  return false;
}

static void reportHierRefError(MsgContext* msgContext, UtString& bad_name, veNode obj) {
  SourceLocatorFactory slf;
  SourceLocator loc = slf.create(veNodeGetFileName(obj),
                                 veNodeGetLineNumber(obj));
  veObjType type = veNodeGetObjType(obj);
  const char* objType = veObjTypeGetString(type);
  msgContext->EModelInvalidHierRef(&loc, objType, bad_name);
}

void 
Decompile::scopevariableDecompile(veNode scopevariable)
{
  Protector protector(this, scopevariable);


  CheetahList vars(veScopeVariableGetElementList(scopevariable));
  veNode node;
  veNode prev = NULL;
  UtString name;
  while ((node = veListGetNextNode(vars)) != NULL)
  {
    if (prev != NULL)
    {
      if (gCheetahGetName(&name, prev)) {
        obfuscate(name.c_str());
        *this << ePeriod;
      }
      else {
        UtString bad_name;
        gCheetahGetName(&bad_name, prev);
        reportHierRefError(mMsgContext, bad_name, prev);
      }
    }
    prev = node;
  }

  if (prev && gCheetahGetName(&name, prev)) {
    obfuscate(name.c_str());
  } else {
    UtString bad_name;
    if ( prev ){
      gCheetahGetName(&bad_name, prev);
    }
    reportHierRefError(mMsgContext, bad_name, (prev ? prev : scopevariable));
  }
} // Decompile::scopevariableDecompile


/*****
      Function      : bitSelectDecompile
      
      Purpose      :Decompiler for the object of type VeBitSelect
      
      Parameters     :Pointer to the object of type VeBitSelect
      
      Returns      :void
      
      End Header
*****/



void
Decompile::bitSelectDecompile (veNode bitselect)
{
  Protector protector(this, bitselect);

  veNode          variable, selectedBit;
  
  variable = veBitSelectGetVariable (bitselect);
  selectedBit = veBitSelectGetSelectedBit (bitselect);
  if (variable)
    exprDecompile (variable);
  
  *this << eOpenBrace;
  if (selectedBit)
    exprDecompile (selectedBit);
  
  *this << eCloseBrace;
}






/*****
      Function      : partSelectDecompile
      
      Purpose      :Decompiler for the object of type VePartSelect
      
      Parameters     :Pointer to the object of type VePartSelect
      
      Returns      :void
      
      End Header
*****/

void
Decompile::partSelectDecompile (veNode partselect)
{
  Protector protector(this, partselect);

  veNode          variable;
  veNode          selectedBit;
  
  variable = vePartSelectGetVariable (partselect);
  
  if (variable)
    exprDecompile (variable);
  
  selectedBit = vePartSelectGetSelectedBit (partselect);
  
  if (selectedBit)
    rangeDecompile (selectedBit);
  
}




/*****
      Function      : arrayPartSelectDecompile
      
      Purpose      :Decompiler for the object of type VeArrayPartSelect
      
      Parameters     :Pointer to the object of type VeArrayPartSelect
      
      Returns      :void
      
      End Header
*****/

void
Decompile::arrayPartSelectDecompile (veNode arrayPartselect)
{
  Protector protector(this, arrayPartselect);

  veNode          variable;
  CheetahList     selBitList;
  veNode          range;
  veNode          selBit;
  
  variable = veArrayPartSelectGetVariable (arrayPartselect);
  
  if (variable)
    exprDecompile (variable);
  
  selBitList= veArrayPartSelectGetSelBitList (arrayPartselect);
  while ((selBit = veListGetNextNode (selBitList))){
    *this << eOpenBrace;
    exprDecompile (selBit);
    *this << eCloseBrace;
  }
  
  range = veArrayPartSelectGetRange(arrayPartselect);
  if (range) {
    rangeDecompile (range);
  }
  
}


/*****
      Function      : arrayBitSelectDecompile
      
      Purpose      :Decompiler for the object of type VeArrayBitSelect
      
      Parameters     :Pointer to the object of type VeArrayBitSelect
      
      Returns      :void
      
      End Header
*****/

void
Decompile::arrayBitSelectDecompile (veNode arrayBitselect) {
  Protector protector(this, arrayBitselect);

  veNode          variable;
  CheetahList     selBitList;
  veNode          selBit;
  
  variable = veArrayBitSelectGetVariable (arrayBitselect);
  
  if (variable)
    exprDecompile (variable);
  
  selBitList= veArrayBitSelectGetSelBitList (arrayBitselect);
  while ((selBit = veListGetNextNode (selBitList))){
    *this << eOpenBrace;
    exprDecompile (selBit);
    *this << eCloseBrace;
  }
  
}



/*****
      Function      : condOpDecompile
      
      Purpose      :Decompiler for the object of type VeCondOp
      
      Parameters     :Pointer to the object of type VeCondOp
      
      Returns      :void
      
      End Header
*****/

void
Decompile::condOpDecompile (veNode condop)
{
  Protector protector(this, condop);

  veNode          condition, leftexpr, rightexpr;
  CheetahList     attrInstanceList;
  
  condition = veCondOpGetCondition (condop);
  leftexpr = veCondOpGetLeftExpr (condop);
  rightexpr = veCondOpGetRightExpr (condop);
  
  *this << eOpenParen;
  exprDecompile (condition);
  *this << ' ' << '?' << ' ';
  
  if ((attrInstanceList = veNodeGetAttrInstance(condop))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  exprDecompile (leftexpr);
  *this << ' ' << ':' << ' ';
  exprDecompile (rightexpr);
  *this << eCloseParen;
}




/*****
      Function      : minTypMaxExprDecompile
      
      Purpose      :Decompiler for the object of type VeMinTypMaxExpr
      
      Purpose      :Pointer to the object of type VeMinTypMaxExpr
      
      Returns      :void
      
      End Header
*****/




void
Decompile::minTypMaxExprDecompile (veNode minTypMaxExpr)
{
  Protector protector(this, minTypMaxExpr);

  veNode          minDelay, maxDelay, typicalDelay;
  
  minDelay = veMinTypMaxExprGetMinDelay (minTypMaxExpr);
  maxDelay = veMinTypMaxExprGetMaxDelay (minTypMaxExpr);
  typicalDelay = veMinTypMaxExprGetTypDelay (minTypMaxExpr);
  
  if ((minDelay == NULL) && (maxDelay == NULL))
    exprDecompile (typicalDelay);
  else
  {
    *this << eOpenParen;
    if (minDelay)
      exprDecompile (minDelay);
    
    *this << eColon;
    if (typicalDelay)
      exprDecompile (typicalDelay);
    
    *this << eColon;
    if (maxDelay)
      exprDecompile (maxDelay);
    
    *this << eCloseParen;
  }
  
}




/*****
      Function      : namedObjectUseDecompile
      
      Purpose      :Decompiler for the Object of type VeNamedObjectUse
      
      Purpose      :Pointer to the object of type VeNamedObjectUse
      
      Returns      :void
      
      End Header
*****/

void
Decompile::namedObjectUseDecompile (veNode namedObjectUse)
{
  Protector protector(this, namedObjectUse);

  veNode          parentObject;
  
  parentObject = veNamedObjectUseGetParent (namedObjectUse);
  CheetahStr name(veNamedObjectGetName (parentObject));
  
  if (parentObject)
  {
    obfuscate( name);
    *this << eSpace;
  }
}







/*****
      Function      :paramDecompile
      
      Purpose      :VeParam Decompiler
      
      Purpose      :object of type VeParam
      
      Returns      :void
      
      End Header
*****/

void
Decompile::paramDecompile (veNode param)
{
  Protector protector(this, param);

  veNode          paramrange;
  veNode          exprparamvalue;
  veBuiltinObjectType paramType;
  veBool          isSigned;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(param))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  CheetahStr name(veParamGetName (param));
  paramrange = veParamGetParamRange (param);
  *this << eParameter;
  paramType = veParamGetParamType (param);
  isSigned = veParamGetIsSigned (param);
  switch (paramType) {
  case INTEGER_USE:
    *this << eInteger;
    break;
  case REAL_USE:
    *this << eReal;
    break;
  case TIME_USE:
    *this << eTime;
    break;
  case REALTIME_USE:
    *this << eRealTime;
    break;
  default:
    if (isSigned)
      *this << eSigned;
    break;
  }
  if (paramrange)
    rangeDecompile (paramrange);
  
  obfuscate( name);
  *this << eEqual;
  exprparamvalue = veParamGetParamValue (param);
  if (exprparamvalue)
    exprDecompile (exprparamvalue);
  
  *this << eSemiColon << eNewline;
}



/*****
      Function      :localparamDecompile
      
      Purpose      :VeLocalParam Decompiler
      
      Purpose      :object of type VeLocalParam
      
      Returns      :void
      
      End Header
*****/

void
Decompile::localparamDecompile (veNode localparam)
{
  Protector protector(this, localparam);

  veNode          localparamrange;
  veNode          exprlocalparamvalue;
  veBuiltinObjectType paramType;
  veBool          isSigned; 
  
  CheetahStr name(veNamedObjectGetName (localparam));
  localparamrange = veLocalParamGetRange (localparam);
  *this << eLocalParam;
  paramType = veLocalParamGetParamType (localparam);
  isSigned = veLocalParamGetIsSigned (localparam);
  switch (paramType) {
  case INTEGER_USE:
    *this << eInteger;
    break;
  case REAL_USE:
    *this << eReal;
    break;
  case TIME_USE:
    *this << eTime;
    break;
  case REALTIME_USE:
    *this << eRealTime;
    break;
  default:
    if (isSigned)
      *this << eSigned;
    break;
  }
  if (localparamrange)
    rangeDecompile (localparamrange);
  
  obfuscate( name);
  *this << eEqual;
  exprlocalparamvalue = veLocalParamGetValue (localparam);
  if (exprlocalparamvalue)
    exprDecompile (exprlocalparamvalue);
  
  *this << eSemiColon << eNewline;
}


/*****
      Function      : varDecompile
      
      Purpose      : decopiler for VeVariable class objects
      
      Purpose      :veNode :: VeVariable class pointer
      
      Returns      :void
      
      End Header
*****/





void
Decompile::varDecompile (veNode var)
{
  Protector protector(this, var);

  veObjType   ObjType;
  
  ObjType = veNodeGetObjType (var);
  switch (ObjType)
  {
  case VE_NETARRAY:
  {
    netArrayDecompile(var);
    break;
  }
  case VE_MEMORY:
  {
    memoryDecompile (var);
    break;
  }
  case  VE_ARRAY:
  {
    arrayDecompile (var);
    break;
  }
  case VE_INTEGER:
  {
    integerdecompile (var);
    break;
  }
  case VE_INTORTIMEARRAY:
  {
    intortimearraydecompile (var);
    break;
  }
  case VE_REAL:
  {
    realdecompile (var);
    break;
  }
  case VE_TIME:
  {
    TimeDecompile (var);
    break;
  }
  case VE_EVENT:
  {
    eventDecompile (var);
    break;
  }
  case VE_REG:
  {
    regDecompile (var);
    break;
  }
  default :
    break;
  }
  
}



/*****
      Function       : arrayDecompile
      
      Purpose        :decompiler for objects of type Ve_Array
      
      Parameters    :VeArray   class pointer
      
      Returns        :void
      
      End Header
*****/
void
Decompile::netArrayDecompile(veNode netArray)
{
  Protector protector(this, netArray);

  veNode          netRange,dimRange;
  CheetahList     attrInstanceList;
  CheetahList     dimensionList;
  veBool  isSigned = veNetGetIsSigned(netArray);
  veNetType type = veNetGetNetType(netArray);
  veBool  isVectored = veNetGetIsVectored(netArray);
  
  if ((attrInstanceList = veNodeGetAttrInstance(netArray))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  switch (type) {
  case WIRE_NET:
    *this << eWire;
    break;
  case TRI_NET:
    *this << eTri;
    break;
  case TRI1_NET:
    *this << eTri1;
    break;
  case SUPPLY0_NET:
    *this << eSupply0;
    break;
  case WAND_NET:
    *this << eWand;
    break;
  case TRIAND_NET:
    *this << eTriand;
    break;
  case TRI0_NET:
    *this << eTri0;
    break;
  case SUPPLY1_NET:
    *this << eSupply1;
    break;
  case WOR_NET:
    *this << eWor;
    break;
  case TRIOR_NET:
    *this << eTrior;
    break;
  case TRIREG_NET:
    *this << eTrireg;
    break;
  case REG_NET:
    *this << eReg;
    break;
  default:
    break;
  }
  
  if (isVectored && (type != REG_NET))
    *this << eVectored;
  if (isSigned)
    *this << eSigned;
  netRange = veNetGetNetRange(netArray);
  if (netRange)
    rangeDecompile(netRange);
  CheetahStr nname(veNetGetName(netArray));
  obfuscate(nname);
  dimensionList = veArrayGetDimensionList(netArray);
  if (dimensionList) {
    while ((dimRange = veListGetNextNode(dimensionList)) != NULL ) {
      rangeDecompile(dimRange);
    }
  }
  *this << eSemiColon << eNewline;
  
}



/*****
      Function      : memoryDecompile
      
      Purpose      :decompiler for objects of type Ve_Memory
      
      Parameters     :VeMemory class pointer
      
      Returns      :void
      
      End Header
*****/



void
Decompile::memoryDecompile (veNode mem)
{
  Protector protector(this, mem);

  veNode          rangenode, rangememsize;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(mem))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  rangenode = veMemoryGetWordSize (mem);
  rangememsize = veMemoryGetMemSize (mem);
  
  *this << eReg;
  if (rangenode)
    rangeDecompile (rangenode);
  
  obfuscate( CheetahStr(veVarGetName (mem)));
  if (rangememsize)
    rangeDecompile (rangememsize);
  
  *this << eSemiColon << eNewline;
  
}

/*****
      Function       : arrayDecompile
      
      Purpose        :decompiler for objects of type Ve_Array
      
      Parameters    :VeArray   class pointer
      
      Returns        :void
      
      End Header
*****/



void
Decompile::arrayDecompile (veNode array)
{
  Protector protector(this, array);

  veNode          rangenode, rangeArray;
  CheetahList     dimensionList;
  CheetahList     attrInstanceList;
  
  rangenode = veArrayGetArrayBitRange (array);
  dimensionList = veArrayGetDimensionList (array);
  if ((attrInstanceList = veNodeGetAttrInstance(array))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
/***********************************************************
 *
 * This part is for type writing e.g. reg/integer/wire.......
 *
 **********************************************************/
  
  switch (veArrayGetArrayType (array)){
  case VE_REG:
    *this << eInteger;
    break;
  case  VE_INTEGER:
    *this << eInteger;
    break;
  case  VE_REAL:
    *this << eReal;
    break;
  case  VE_REALTIME:
    *this << eRealTime;
    break;
  case  VE_TIME:
    *this << eTime;
    break;
  default :
    break;
  }
  
/********************************************************
 * End of type writing ......
 *******************************************************/
  
  if ((!veVariableGetIsRangeImplicit(array)) && rangenode)
    rangeDecompile (rangenode);
  
  obfuscate( CheetahStr(veVarGetName (array)));
  
  while ((rangeArray = veListGetNextNode (dimensionList))){
    rangeDecompile (rangeArray);
  }
  
  *this << eSemiColon << eNewline;
  
}




/*****
      Function      : integerdecompile
      
      Purpose      :Decompiler for the object of type Ve_Integer
      
      Purpose      :veNode  :: Ve_Integer class pointer
      
      Returns      :void
      
      End Header
*****/



void
Decompile::integerdecompile (veNode integer)
{
  Protector protector(this, integer);

  veNode          rangesize;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(integer))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eInteger;
  rangesize = veIntegerGetSize (integer);
  if ((!veVariableGetIsRangeImplicit(integer)) && rangesize)
    rangeDecompile (rangesize);
  
  obfuscate( CheetahStr(veVarGetName (integer)));
  *this << eSemiColon << eNewline;
}

void
Decompile::intortimearraydecompile( veNode intortimearray)
{
  Protector protector(this, intortimearray);

  veObjType arrayType;
  veNode  arrayRange,wordSize;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(intortimearray))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  arrayType = veIntOrTimeArrayGetArrayType(intortimearray);
  wordSize = veMemoryGetWordSize(intortimearray);
  
  switch (arrayType)
  {
  case VE_INTEGER:
    *this << eInteger;
    break;
  case VE_TIME:
    *this << eTime;
    break;
  default:
    return;
  }
  
  if ((!veVariableGetIsRangeImplicit(intortimearray)) && wordSize)
    rangeDecompile(wordSize);
  obfuscate( CheetahStr(veVarGetName(intortimearray)));
  arrayRange = veIntOrTimeArrayGetArrayRange(intortimearray);
  if (arrayRange)
    rangeDecompile(arrayRange);
  *this << eSemiColon << eNewline;
}


/*****
      Function      :realdecompile
      
      Purpose      :decompiler for the object of type Ve_Real
      
      Purpose      :Pointer to object of type Ve_Real!!
      
      Returns      :void
      
      End Header
*****/




void
Decompile::realdecompile (veNode real)
{
  Protector protector(this, real);

  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(real))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eReal;
  obfuscate( CheetahStr(veVarGetName (real)));
  *this << eSemiColon << eNewline;
}



/*****
      Function      :realtimedecompile
      
      Purpose      :decompiler for the object of type Ve_RealTime
      
      Purpose      :Pointer to object of type Ve_Real!!
      
      Returns      :void
      
      End Header
*****/




void
Decompile::realtimedecompile (veNode realtime)
{
  Protector protector(this, realtime);

  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(realtime))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eRealTime;
  obfuscate( CheetahStr(veVarGetName (realtime)));
  *this << eSemiColon << eNewline;
}



/*****
      Function      : TimeDecompile
      
      Purpose      :Decompiler for the object of type Ve_Time
      
      Purpose      :veNode ::Ve_time class pointer
      
      Returns      :
      
      End Header
*****/



void
Decompile::TimeDecompile (veNode time)
{
  Protector protector(this, time);

  veNode          rangesize;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(time))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  rangesize = veTimeGetSize (time);
  
  *this << eTime;
  if ((!veVariableGetIsRangeImplicit(time)) && rangesize)
    rangeDecompile (rangesize);
  obfuscate( CheetahStr(veVarGetName (time)));
  
  *this << eSemiColon << eNewline;
}




/*****
      Function      :eventDecompile
      
      Purpose      :Decompiler for the object of type Ve_Event
      
      Purpose      :veNode ::pointeer to the object of type Ve_Event
      
      Returns      :void
      
      End Header
*****/

void
Decompile::eventDecompile (veNode event)
{
  Protector protector(this, event);

  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(event))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  *this << eEvent;
  obfuscate( CheetahStr(veVarGetName (event)));
  *this << eSemiColon << eNewline;
}



/*****
      Function      :netDecompile
      
      Purpose      :decompiler for VeNet class objects
      
      Purpose      :VeNet class pointer
      
      Returns      :void
      
      End Header
*****/


void
Decompile::netDecompile (veNode net)
{
  Protector protector(this, net);

  veBool       isVectored, isPortNet, isImplicit, isDecompiled;
  veBool       isSigned;
  veNetType nettype;
  veDirectionType  netDirection;
  veChargeStrengthType chargeStrength;
  veDriveStrengthType strength0, strength1;
  veNode          portrange, netrange;
  CheetahList       netdelay;
  veNode          CurrentScopePtr;
  veObjType   ObjType;
  veNode          rhsexpr, funcexpr;
  CheetahList     attrInstanceList;
  
  
  /***End Declaration*************/
  isVectored = veNetGetIsVectored (net);
  isPortNet = veNetGetIsPortNet (net);
  isImplicit = veNetGetIsImplicit (net);
  isDecompiled = mDecompiledNodes->find(net) != mDecompiledNodes->end()
    ? VE_TRUE: VE_FALSE;
  chargeStrength = veNetGetChargeStrength (net);
  strength0 = veNetGetStrength0 (net);
  strength1 = veNetGetStrength1 (net);
  netDirection = veNetGetNetDirection (net);
  netdelay = veNetGetNetDelayList (net);
  nettype = veNetGetNetType (net);
  netrange = veNetGetNetRange (net);
  portrange = veNetGetPortRange (net);
  rhsexpr = veNetGetRhsExpr (net);
  
  isSigned = veNetGetIsSigned (net);
/*******************************************************
 * isSigned stores weather the net is signed or not....
 *
 ********************************************************/
  
  
  if ((isPortNet == VE_TRUE) && (isDecompiled == VE_FALSE))
  {
    if ((attrInstanceList = veNodeGetPortAttrInstance (net))) {
      atttrInstanceDecompile (attrInstanceList);
      *this << eNewline;
    }
    
    switch (netDirection)
    {
    case INPUT_DIR:
    {
      *this << eInput;
      break;
    }
    case OUTPUT_DIR:
    {
      *this << eOutput;
      break;
    }
    case INOUT_DIR:
    {
      *this << eInout;
      break;
    }
    case UNSET_DIRECTION:
      break;
    }
    
    if (isSigned == VE_TRUE){
      *this << eSigned;
    }
    
    portrange = veNetGetPortRange (net);
    if (portrange)
      rangeDecompile (portrange);
    
    CheetahStr name(veNetGetName (net));
    obfuscate( name);
    *this << eSemiColon << eNewline;
    CurrentScopePtr = veNetGetCurrentScope(net);
    ObjType = veNodeGetObjType (CurrentScopePtr);
    
    if (ObjType == VE_UDP)
    {
      if ((attrInstanceList = veNodeGetAttrInstance(net))) {
        atttrInstanceDecompile (attrInstanceList);
        *this << eNewline;
      }
      if ((netDirection == OUTPUT_DIR) && (nettype == REG_NET))
      {
        *this << eReg;
        obfuscate( name);
        *this << eSemiColon << eNewline;
      }
    }
    mDecompiledNodes->insert(net);
  } else if (veNetGetIsImplicit (net) == VE_FALSE)
  {
    if ((attrInstanceList = veNodeGetAttrInstance(net))) {
      atttrInstanceDecompile (attrInstanceList);
      *this << eNewline;
    }
    switch (nettype)
    {
    case WIRE_NET:
    {
      *this << eWire;
      break;
    }
    case TRI_NET:
    {
      *this << eTri;
      break;
    }
    case TRI1_NET:
    {
      *this << eTri1;
      break;
    }
    case SUPPLY0_NET:
    {
      *this << eSupply0;
      break;
    }
    case WAND_NET:
    {
      *this << eWand;
      break;
    }
    case TRIAND_NET:
    {
      *this << eTriand;
      break;
    }
    case TRI0_NET:
    {
      *this << eTri0;
      break;
    }
    case SUPPLY1_NET:
    {
      *this << eSupply1;
      break;
    }
    case WOR_NET:
    {
      *this << eWor;
      break;
    }
    case TRIOR_NET:
    {
      *this << eTrior;
      break;
    }
    case TRIREG_NET:
    {
      *this << eTrireg;
      break;
    }
    case REG_NET:
    {
      *this << eReg;
      break;
    }
    case UWIRE_NET:
    {
      *this << eUWire;
      break;
    }
    case UNSET_NET:
      INFO_ASSERT(0, "unsupported net type UNSET_NET");
      break;
    }
    
    if ((nettype == TRIREG_NET) && (chargeStrength != UNSET_CHARGE))
    {
      switch (chargeStrength)
      {
      case SMALL_CHARGE:
      {
        writeOutput( "(small)", 1);
        break;
      }
      case MEDIUM_CHARGE:
      {
        writeOutput( "(medium)", 1);
        break;
      }
      case LARGE_CHARGE:
      {
        writeOutput( "(large)", 1);
        break;
      }
      case UNSET_CHARGE:
        break;
      }
      *this << eSpace;
    }
    
    if ((rhsexpr) && (strength0 != UNSET_STRENGTH) && (strength1 != UNSET_STRENGTH))
    {
      *this << eOpenParen;
      switch (strength0)
      {
      case SUPPLY_0:
      {
        *this << eSupply0;
        break;
      }
      case STRONG_0:
      {
        *this << eStrong0;
        break;
      }
      case PULL_0:
      {
        *this << ePull0;
        break;
      }
      case WEAK_0:
      {
        *this << eWeak0;
        break;
      }
      case HIGHZ_0:
      {
        *this << eHighz0;
        break;
      }
      default :
        break;
      }
      writeOutput( ",", 1);
      
      switch (strength1)
      {
      case SUPPLY_1:
      {
        *this << eSupply1;
        break;
      }
      case STRONG_1:
      {
        *this << eStrong1;
        break;
      }
      case PULL_1:
      {
        *this << ePull1;
        break;
      }
      case WEAK_1:
      {
        *this << eWeak1;
        break;
      }
      case HIGHZ_1:
      {
        *this << eHighz1;
        break;
      }
      default :
        break;
      }
      
      writeOutput( ")", 1);
      *this << eSpace;
    }
    
    if ((isVectored == VE_TRUE) && (nettype != REG_NET))
      *this << eVectored;
    
    if (isSigned == VE_TRUE){
      *this << eSigned;
    }
    
    netrange = veNetGetNetRange (net);
    if (netrange)
      rangeDecompile (netrange);
    else if (portrange)
      rangeDecompile (portrange);
    
    
    if (netdelay)
    {
      writeOutput( "#(", 1);
      funcexpr = veListGetNextNode (netdelay);
      if (funcexpr)
        exprDecompile (funcexpr);
      
      funcexpr = veListGetNextNode (netdelay);
      for (; funcexpr; funcexpr = veListGetNextNode (netdelay))
      {
        writeOutput( ",", 1);
        exprDecompile (funcexpr);
      }
      
      writeOutput( ")", 1);
    }
    *this << eSpace;
    CheetahStr nname(veNetGetName (net));
    obfuscate( nname);
    if (rhsexpr)
    {
      writeOutput( " = ", 1);
      exprDecompile (rhsexpr);
    }
    writeOutput( " ;\n", 1);
  }
}














/*****
      Function      :defParamDecompile
      
      Purpose      :Decompiler for the object of type VeDefParam
      
      Purpose      :veNode ::Pointer to object of type VeDefParam
      
      Returns      :void
      
      End Header
*****/




void
Decompile::defParamDecompile (veNode defparam)
{
  Protector protector(this, defparam);

  veNode          Variable;
  veString        hiername;
  veNode          paramvalue;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(defparam))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  Variable = veDefParamGetVariable (defparam);
  hiername = veScopeVariableGetHierName (Variable);
  
  *this << eDefParam;
  obfuscate( hiername);
  writeOutput( " = ", 0);
  paramvalue = veDefParamGetParamValue (defparam);
  if (paramvalue)
    exprDecompile (paramvalue);
}








/*****
      Function      :contAssignDecompile
      
      Purpose      :Decompiler for VeContAssign Class
      
      Purpose      :veNode :: Pointer to object of type VeContAssign
      
      Returns      :void
      
      End Header
*****/

void
Decompile::contAssignDecompile (veNode contassign)
{
  Protector protector(this, contassign);

  veDriveStrengthType strength0, strength1;
  veNode              delay;
  CheetahList         delayList;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(contassign))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  strength0 = veContAssignGetStrength0 (contassign);
  strength1 = veContAssignGetStrength1 (contassign);
  
  *this << eAssign;
  
  if ((strength0 != UNSET_STRENGTH) && (strength1 != UNSET_STRENGTH))
  {
    *this << eOpenParen;
    switch (strength0)
    {
    case SUPPLY_0:
    {
      *this << eSupply0;
      break;
    }
    case STRONG_0:
    {
      *this << eStrong0;
      break;
    }
    case PULL_0:
    {
      *this << ePull0;
      break;
    }
    case WEAK_0:
    {
      *this << eWeak0;
      break;
    }
    case HIGHZ_0:
    {
      *this << eHighz0;
      break;
    }
    default:
      break;
    }
    
    switch (strength1)
    {
    case SUPPLY_1:
    {
      *this << eSupply1;
      break;
    }
    case STRONG_1:
    {
      *this << eStrong1;
      break;
    }
    case PULL_1:
    {
      *this << ePull1;
      break;
    }
    case WEAK_1:
    {
      *this << eWeak1;
      break;
    }
    case HIGHZ_1:
    {
      *this << eHighz1;
      break;
    }
    default:
      break;
    }
    *this << eCloseParen;
    
  }
  delayList = veContAssignGetAssignDelayList (contassign);
  
  if (delayList)
  {
    writeOutput( "#(", 0);
    
    delay = veListGetNextNode (delayList);
    
    exprDecompile (delay);
    
    delay = veListGetNextNode (delayList);
    
    for (; delay; delay = veListGetNextNode (delayList))
    {
      writeOutput( " , ", 0);
      exprDecompile (delay);
    }
    *this << eCloseParen;
  }
  exprDecompile (veContAssignGetLvalue (contassign));
  
  writeOutput( "= ", 0);
  
  exprDecompile (veContAssignGetRhsExpr (contassign));
  
}



/*****
      Function      :specBlkDecompile
      
      
      Purpose      :Decompiler for VeSpecifyBlock
      
      Purpose      :Pointer to the object of type VeSpecifyBlock
      
      Returns      :void
      
      End Header
*****/


void
Decompile::specBlkDecompile (veNode specblk)
{
  Protector protector(this, specblk);

  // The easiest thing for now is to simply not put specify blocks
  // into the generated CEM file
  veNode          pathCond;
  veObjType       obj;
  CheetahList     attrInstanceList;
  CheetahList     orderedList;
  veNode          orderedItem;
  int             listCount;
  
  if ((attrInstanceList = veNodeGetAttrInstance(specblk))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  
  *this << eSpecify << eNewline;
  
  orderedList = veSpecBlockGetOrderedList(specblk);
  if (orderedList) {
    listCount = veListGetSize(orderedList);
    while (listCount) {
      orderedItem = veListGetNextNode(orderedList);
      if (orderedItem == NULL) {
        listCount--;
        continue;
      }
      
      switch (veNodeGetObjType(orderedItem)) {
      case VE_SPECPARAM:
      {
        specParamDecompile (orderedItem);
        *this << eSemiColon << eNewline;
        break;
      }
      case VE_PATH:
      {
        pathDecompile (orderedItem);
        *this << eSemiColon << eNewline;
        break;
      }
      case VE_LEVELPATH:
      {
        pathCond = veLevelPathGetCondition(orderedItem);
        if (pathCond) {
          obj = veNodeGetObjType(pathCond);
          *this << eIf;
          if ((obj==VE_NAMEDOBJECTUSE)||(obj==VE_BITSELECT))
            writeOutput( " (", 0);
          exprDecompile(pathCond);
          if ((obj==VE_NAMEDOBJECTUSE)||(obj==VE_BITSELECT))
            writeOutput( " )", 0);
        }
        pathDecompile (orderedItem);
        *this << eSemiColon << eNewline;
        break;
      }
      case VE_EDGEPATH:
      {
        pathCond = veEdgePathGetCondition(orderedItem);
        if (pathCond) {
          obj = veNodeGetObjType(pathCond);
          *this << eIf;
          if (obj==VE_NAMEDOBJECTUSE)
            writeOutput( " (", 0);
          exprDecompile(pathCond);
          if (obj==VE_NAMEDOBJECTUSE)
            writeOutput( " )", 0);
        }
        pathDecompile (orderedItem);
        *this << eSemiColon << eNewline;
        break;
      }
      case VE_SYSTIMINGCHECK:
      {
        sysTimingCheckDecompile (orderedItem);
        break;
      }
      case VE_PULSESTYLE:
      {
        pulsestyleDecompile (orderedItem);
        *this << eSemiColon << eNewline;
        break;
      }
      case VE_SHOWCANCELLED:
      {
        showcancelledDecompile (orderedItem);
        *this << eSemiColon << eNewline;
        break;
      }
      default:
      {
        break;
      }
      }
      listCount--;
    }
  }

  *this << eEndSpecify;
} // Decompile::specBlkDecompile



/*****
      Function      :pulsestyleDecompile
      
      
      Purpose      :Decompiler for VePulseStyle 
      
      Purpose      :Pointer to the object of type VePulseStyle 
      
      Returns      :void
      
      End Header
*****/

void
Decompile::pulsestyleDecompile (veNode pulseStyle) {
  Protector protector(this, pulseStyle);

  CheetahList  outputPathList;
  veNode  outputPath;
  
  if (vePulsestyleGetType (pulseStyle) == VE_EVENT_PULSE)
    writeOutput( "pulsestyle_onevent ", 0);    
  else if (vePulsestyleGetType (pulseStyle) == VE_DETECT_PULSE)
    writeOutput( "pulsestyle_ondetect ",0);
  outputPathList = vePulsestyleGetPathList (pulseStyle);
  outputPath = veListGetNextNode (outputPathList);
  exprDecompile (outputPath);
  while ((outputPath = veListGetNextNode (outputPathList))) {
    writeOutput( ",",0);
    exprDecompile (outputPath);
  }    
  writeOutput( ";",0);
  
}


/*****
      Function      :showcancelledDecompile
      
      
      Purpose      :Decompiler for VeShowCancelled
      
      Purpose      :Pointer to the object of type VeshowCancelled
      
      Returns      :void
      
      End Header
*****/

void
Decompile::showcancelledDecompile (veNode showCancelled) {
  Protector protector(this, showCancelled);

  CheetahList  outputPathList;
  veNode  outputPath;
  if (veShowcancelledGetType (showCancelled) == VE_SHOW) 
    *this << eShowCancelled;
  else if (veShowcancelledGetType (showCancelled) == VE_NOSHOW)
    *this << eNoShowCancelled;
  outputPathList = veShowcancelledGetPathList (showCancelled);
  outputPath = veListGetNextNode (outputPathList);
  exprDecompile (outputPath);
  while ((outputPath = veListGetNextNode (outputPathList))) {  
    *this << eComma;    
    exprDecompile (outputPath); 
  }
  writeOutput( ";", 0);
  
}

/*****
      Function      : pathDecompile
      
      Purpose      :Decompiler for the Object of type VePath
      
      Purpose      :Pointer to the Object of type VePath
      
      Returns      :void
      
      End Header
*****/

void
Decompile::pathDecompile (veNode path)
{
  Protector protector(this, path);

  vePathType         PathType;
  CheetahList        InputPathList, OutputPathList, pathDelayList;
  veNode             funcexpr,DataSrcExpr,parent;
  vePolarityType       pathPolarity;
  veEdgeType           edgeId;
  veBool               IsWidthSpec;
  veBaseType           baseType;
  int                   width,decNumber;
  veObjType           objtype;
  char *tempVal = NULL;
  veBool              sign;
  
  PathType = vePathGetPathType (path);
  InputPathList = vePathGetInputPathList (path);
  OutputPathList = vePathGetOutputPathList (path);
  pathDelayList = vePathGetPathDelayList (path);
  objtype = veNodeGetObjType(path);
  DataSrcExpr = NULL;
  
  if (InputPathList)
  {
    funcexpr = veListGetNextNode (InputPathList);
    *this << eOpenParen;
    if (objtype == VE_EDGEPATH)
    {
      edgeId = veEdgePathGetEdgeType(path);
      DataSrcExpr = veEdgePathGetDataSrcExpr(path);
      switch(edgeId)
      {
      case POS_EDGE : *this << ePosedge;
      case NEG_EDGE : *this << eNegedge;
      case UNSET_EDGE : break ;
      }
    }
    if (funcexpr)
      exprDecompile (funcexpr);
    funcexpr = veListGetNextNode (InputPathList);
    for (; funcexpr; funcexpr = veListGetNextNode (InputPathList))
    {
      *this << eComma;
      exprDecompile (funcexpr);
    }
    
  }
  if (objtype == VE_PATH)
  {
    pathPolarity = vePathGetPolarity(path);
    if (pathPolarity != UNKNOWN_POLARITY)
    {
      switch (pathPolarity)
      {
      case POSITIVE_POLARITY : writeOutput( "+", 0);break ;
      case NEGATIVE_POLARITY : writeOutput( "-", 0); break ;
      case UNKNOWN_POLARITY  : break ;
      }
    }
    
  }
  switch (PathType)
  {
  case FULL_PATH:
  {
    writeOutput( "*>", 0);
    break;
  }
  case PAR_PATH:
  {
    writeOutput( "=>", 0);
    break;
  }
  }
  if (OutputPathList)
  {
    funcexpr = veListGetNextNode (OutputPathList);
    if (funcexpr)
    {
      if (DataSrcExpr)
        *this << eOpenParen;
      exprDecompile (funcexpr);
    }
    funcexpr = veListGetNextNode (OutputPathList);
    for (; funcexpr; funcexpr = veListGetNextNode (OutputPathList))
    {
      *this << eComma;
      exprDecompile (funcexpr);
    }
    
    if (objtype == VE_EDGEPATH)
    {
      objtype = veNodeGetObjType(DataSrcExpr);
      pathPolarity = veEdgePathGetPolarity(path);
      if (pathPolarity != UNKNOWN_POLARITY)
      {
        switch (pathPolarity)
        {
        case POSITIVE_POLARITY : writeOutput( "+", 0);break ;
        case NEGATIVE_POLARITY : writeOutput( "-", 0); break ;
        case UNKNOWN_POLARITY  : break ;
        }
      }
      writeOutput( ": ", 0);
    }
    
    if (objtype == VE_BASEDNUMBER)
    {
      IsWidthSpec = veBasedNumberGetIsWidthSpec(DataSrcExpr);
      baseType = veBasedNumberGetBaseType(DataSrcExpr);
      
      //width = veBasedNumberGetWidth(DataSrcExpr);
      width = veExprEvaluateWidth(DataSrcExpr);
      sign = veExprIsSigned(DataSrcExpr);
      //value = veBasedNumberGetBinaryValueString(DataSrcExpr);
      CheetahStr value(veExprEvaluateValueInAdjustedBinaryString(DataSrcExpr,
                                                                 width,
                                                                 sign));
      
      if (IsWidthSpec == VE_TRUE) {
#if 0
        writeOutput( int_to_str(width), 0);
        manish
#endif
          tempVal =  int_to_str(width);
        writeOutput( tempVal, 0);
        free(tempVal);
      }
      
      writeOutput( "'", 0);
      switch (baseType)
      {
      case VE_BIN : writeOutput( "b", 0);break;
      case VE_DECIMAL : writeOutput( "d", 0);break;
      case VE_OCTAL    : writeOutput( "o", 0);break;
      default     : break;
      }
      writeOutput( value, 0);
    }
    else if (objtype == VE_NAMEDOBJECTUSE)
    {
      parent = veNamedObjectUseGetParent(DataSrcExpr);
      CheetahStr name(veNetGetName(parent));
      obfuscate( name);
    }
    else if (objtype == VE_CONST)
    {
      decNumber = veConstGetDecNumber(DataSrcExpr);
      /* writeOutput( int_to_str(decNumber), 0);
         manish */
      tempVal =  int_to_str(decNumber);
      writeOutput( tempVal, 0);
      free(tempVal);
    }
    else if ((objtype == VE_BINARY)||(VE_UNARY))
    {
      exprDecompile (DataSrcExpr);
    }
    
    /*funcexpr = veListGetNextNode (OutputPathList);
      for (; funcexpr; funcexpr = veListGetNextNode (OutputPathList))
      {
      *this << eComma;
      exprDecompile (funcexpr);
      }*/
    
    *this << eCloseParen;
    if (DataSrcExpr)
      *this << eCloseParen;
  }
  writeOutput( "=", 0);
  
  if (pathDelayList)
  {
    if (veListGetSize (pathDelayList) > 1)
      *this << eOpenParen;
    
    funcexpr = veListGetNextNode (pathDelayList);
    if (funcexpr)
      exprDecompile (funcexpr);
    
    funcexpr = veListGetNextNode (pathDelayList);
    for (; funcexpr; funcexpr = veListGetNextNode (pathDelayList))
    {
      *this << eComma;
      exprDecompile (funcexpr);
    }
    
    if (veListGetSize (pathDelayList) > 1)
      *this << eCloseParen;
    
  }
}




/*****
      Function      : specParamDecompile
      
      Purpose      :Decompiler for the class of type VeSpecParam
      
      Purpose      :Pointer to the type of VeSpecParam
      
      Returns      :void
      
      End Header
*****/



void
Decompile::specParamDecompile (veNode specparam)
{
  Protector protector(this, specparam);

  CheetahList       valueList;
  veNode            funcexpr;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(specparam))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  CheetahStr name(veSpecParamGetName (specparam));
  valueList = veSpecParamGetParamValueList (specparam);
  
  *this << eSpecParam;
  obfuscate(name);
  *this << eEqual;
  
  if (valueList)
  {
    funcexpr = veListGetNextNode (valueList);
    *this << eOpenParen;
    if (funcexpr)
      exprDecompile (funcexpr);
    
    funcexpr = veListGetNextNode (valueList);
    for (; funcexpr; funcexpr = veListGetNextNode (valueList))
    {
      *this << eComma;
      exprDecompile (funcexpr);
    }
    
    *this << eCloseParen;
  }
}





/*****
      Function      : sysTimingCheckDecompile
      
      Purpose      :Decompiler for the object of type VeSysTimingCheck
      
      Purpose      :Pointer to the function fof type VeSysTimingCheck
      
      Returns      : void
      
      End Header
*****/


void
Decompile::sysTimingCheckDecompile (veNode systimingcheck)
{
  Protector protector(this, systimingcheck);

  CheetahStr sysTaskname(veSysTimeChkGetName (systimingcheck));
  INFO_ASSERT(sysTaskname, "system timing check requires a name");

  /*
   * the LRM defines two groups of timing checks: 
   *   $setup $hold $setuphold
   *   $recovery $removal $recrem
   * and
   *   $nochange (check involves three events):
   *   $skew $timeskew $fullskew
   *   $width $period $nochange
   *
   * The only ones we know how to handle are setup,hold,setuphold.
   */
  bool isSetup = strcmp(sysTaskname, "$setup") == 0;
  bool isHold = strcmp(sysTaskname, "$hold") == 0;
  bool isSetupHold = strcmp(sysTaskname, "$setuphold") == 0;

  if (!isSetup && !isHold && !isSetupHold) {
    if ((strcmp(sysTaskname, "$width") != 0) &&
        (strcmp(sysTaskname, "$recovery") != 0) &&
        (strcmp(sysTaskname, "$removal") != 0) &&
        (strcmp(sysTaskname, "$recrem") != 0) &&
        (strcmp(sysTaskname, "$skew") != 0) &&
        (strcmp(sysTaskname, "$timeskew") != 0) &&
        (strcmp(sysTaskname, "$fullskew") != 0) &&
        (strcmp(sysTaskname, "$period") != 0) &&
        (strcmp(sysTaskname, "$nochange") != 0))
    {
      // If we get here, it means that the user is trying to make a CEM file
      // or tokenize a timing construct that we haven't anticipated.  There
      // is a possibility that this may be significant and so we should find
      // out about it rather than have the construct mysteriously be omitted
      // from the CEM/token file
      SourceLocatorFactory slf;
      SourceLocator loc = slf.create(veNodeGetFileName(systimingcheck),
                                     veNodeGetLineNumber(systimingcheck));
      mMsgContext->UnexpectedTimingCheck(&loc, sysTaskname);
    }
    return;
  } // if

  veNode refEvent = veSysTimeChkGetRefEvent (systimingcheck);
  INFO_ASSERT(refEvent, "system timing check requires a reference signal");
  veNode dataEvent = veSysTimeChkGetDataEvent (systimingcheck);
  INFO_ASSERT(dataEvent, "system timing check requires a data signal");
  //veNode threshold = veSysTimeChkGetThreshold (systimingcheck);
  veNode notifier = veSysTimeChkGetNotifier (systimingcheck);
  CheetahList timingLimitList(veSysTimeChkGetTimingLimitList(systimingcheck));
  INFO_ASSERT(timingLimitList, "at least one timing check arg required");
  INFO_ASSERT(veListGetSize(timingLimitList) >= 1, "at least one timing check arg required");
  veNode startEdgeOffset = veSysTimeChkGetStartEdgeOffset(systimingcheck);
  veNode endEdgeOffset = veSysTimeChkGetEndEdgeOffset(systimingcheck);
  veNode delayedRef = veSysTimeChkGetDelayedReference(systimingcheck);
  veNode delayedData = veSysTimeChkGetDelayedData(systimingcheck);
  
  // For our purposes, we only use specify-blocks for one thing,
  // which is to create explicit assigns between the refEvent and
  // delayedRef.  See VerilogPopulate::specifyBlock in
  // localflow/PopulateModule.cxx.
  //
  // We are looking for something like this:
  //  specify
  //    $setuphold(posedge clk, posedge in, 1, 1, , , , clk_d, in_d);
  // We don't really care about of the other parameters 

  *this << eSpace;
  writeOutput( sysTaskname, 0);
  *this << eOpenParen;

  if (isSetup) {
    timingEventDecompile (dataEvent);
    *this << eComma;
    timingEventDecompile (refEvent);
    *this << eComma;
  }
  else {
    timingEventDecompile (refEvent);
    *this << eComma;
    timingEventDecompile (dataEvent);
    *this << eComma;
  }

  // There must be 1 or 2 timing checks
  veNode funcexpr = veListGetNextNode (timingLimitList);
  if (funcexpr) {
    exprDecompile (funcexpr);
  }
  *this << eComma;

  if (isSetupHold) {
    // $setuphold has a second timing check, but $setup and $hold do
    // not.  However, $setup and $hold *do* have optional notifiers
    if (funcexpr) {
      funcexpr = veListGetNextNode (timingLimitList);
      if (funcexpr) {
        exprDecompile (funcexpr);
      }
    }

    *this << eComma;
    if ( startEdgeOffset ) {
      exprDecompile (startEdgeOffset);
    }
    *this << eComma;
    if( endEdgeOffset ) {
      exprDecompile (endEdgeOffset);
    }

    *this << eComma;
    if (delayedRef) {
      exprDecompile(delayedRef);
    }
    *this << eComma;
    if (delayedData) {
      exprDecompile(delayedData);
    }
  } // if

  else {
    if (notifier) {
      exprDecompile (notifier);
    }
  }

  *this << eCloseParen << eSemiColon << eNewline;
} // Decompile::sysTimingCheckDecompile







/*****
      Function      : timingEventDecompile
      
      Purpose      :Decompiler for the object of type VeTimingEvent
      
      Purpose      :Pointer to the object of type VeTimingEvent
      
      Returns      :void
      
      End Header
*****/

void
Decompile::timingEventDecompile (veNode timingEvent)
{
  Protector protector(this, timingEvent);

  veNode          eventControl;
  veNode          specTerminal;
  veNode          timingCheckEvent;
  
  eventControl = veTimingEventGetTimingEventControl (timingEvent);
  specTerminal = veTimingEventGetSpecTerminal (timingEvent);
  timingCheckEvent = veTimingEventGetTimingCheckEvent(timingEvent);
  
  if (eventControl)
    timingEventControlDecompile (eventControl);
  
  if (specTerminal)
    exprDecompile (specTerminal);
  
  if (timingCheckEvent)
  {
    writeOutput( " &&& ", 0);
    exprDecompile (timingCheckEvent);
  }
}




/*****
      Function      :timingEventControlDecompile
      
      Purpose      :Decompiler for the function of type VetimingEventControlDecompile
      
      Purpose      :Pointer to the function of type VeTimingEventControl
      
      Returns      :void
      
      End Header
*****/

void
Decompile::timingEventControlDecompile (veNode timingEventCtrl)
{
  Protector protector(this, timingEventCtrl);

  veEdgeType        edgeType;
  CheetahList       edgeDescList;
  veNode            edgeDesc;
  
  edgeType = veTimingEventCtrlGetEdgeType (timingEventCtrl);
  edgeDescList = veTimingEventCtrlGetEdgeDescList (timingEventCtrl);
  
  if (edgeType == POS_EDGE)
  {
    *this << ePosedge;
  } else if (edgeType == NEG_EDGE)
  {
    *this << eNegedge;
  } else if (edgeDescList)
  {
    writeOutput( " edge [", 0);
    edgeDesc = veListGetNextNode (edgeDescList);
    
    /*for (; edgeDesc; edgeDesc = veListGetNextNode (edgeDescList))
      {
      edgeDescriptorDecompile (edgeDesc);
      if (edgeDesc)
      *this << eComma;
      }*/
    while (edgeDesc)
    {
      edgeDescriptorDecompile (edgeDesc);
      edgeDesc = veListGetNextNode (edgeDescList);
      if (edgeDesc)
        *this << eComma;
    }
    *this << eCloseBrace;
  }
}




/*****
      Function      :edgeDescriptorDecompile
      
      Purpose      :Decompiler for the object of type VeEdgeDescriptor
      
      Purpose      :Pointer to the object of type VeEdgeDescriptor
      
      Returns      :void
      
      End Header
*****/


void
Decompile::edgeDescriptorDecompile (veNode edgeDesc)
{
  Protector protector(this, edgeDesc);

  veEdgeDescType  type;
  
  type = veEdgeDescriptorGetType (edgeDesc);
  
  switch (type)
  {
  case ZERO_TO_ONE:
  {
    writeOutput( "01", 0);
    break;
  }
  case ONE_TO_ZERO:
  {
    writeOutput( "10", 0);
    break;
  }
  case ZERO_TO_X:
  {
    writeOutput( "0x", 0);
    break;
  }
  case X_TO_ZERO:
  {
    writeOutput( "x0", 0);
    break;
  }
  case ONE_TO_X:
  {
    writeOutput( "1x", 0);
    break;
  }
  case X_TO_ONE:
  {
    writeOutput( "x1", 0);
    break;
  }
  case UNSET_EDGE_DESC:
  {
    break;
  }
  }
  
}



/*****
      Function      :rangeDecompile
      
      Purpose      :Decompiler for the Object of type VeRange
      
      Purpose      :VeNode::Pointer to the object of type VeRaneg
      
      Returns      :void
      
      End Header
*****/






void
Decompile::rangeDecompile (veNode range)
{
  Protector protector(this, range);

  veNode          leftRange, rightRange;
  veSignType      rangeSign;
  
  leftRange = veRangeGetLeftRange (range);
  rightRange = veRangeGetRightRange (range);
  
  *this << eOpenBrace;
  exprDecompile (leftRange);
  
  rangeSign = veRangeGetSignType(range);
  if (rangeSign == POS_SIGN)
    writeOutput( "+ ", 0);
  else  if (rangeSign == NEG_SIGN)
    writeOutput( "- ", 0);
  
  writeOutput( ": ", 0);
  exprDecompile (rightRange);
  *this << eCloseBrace;
}





/*****
      Function      :constDecompile
      
      Purpose      :DEcompiler for the object of type VeConst
      
      Purpose      :Pointer to the onject of type VeConst
      
      Returns      :void
      
      End Header
*****/

void
Decompile::constDecompile (veNode expr)
{
  Protector protector(this, expr);

  int             decNumber;
  char *tempVal = NULL;
  
  decNumber = veConstGetDecNumber (expr);
  /*
    writeOutput( int_to_str(decNumber), 0);
    manish
  */
  tempVal =  int_to_str(decNumber);
  writeOutput(tempVal, 0);
  free(tempVal);
  *this << eSpace;
  
}




/*****
      Function      :stringDecompile
      
      Purpose      :Decompiler for the object of type VeString
      
      Purpose      :Pointer to the object of type VeString
      
      Returns      :void
      
      End Header
*****/

void
Decompile::stringDecompile (veNode expr)
{
  Protector protector(this, expr);

  CheetahStr charString(veStringGetString (expr));
  writeOutput( charString, 0);
  
}




/*****
      Function      :basedNumberDecompile
      
      Purpose      :Decompiler for the object of type VebasedNumber
      
      Purpose      :Pointer to the object of tupe VeBasedNumber
      
      Returns      :void
      
      End Header
*****/


void
Decompile::basedNumberDecompile (veNode basedNumber)
{
  Protector protector(this, basedNumber);

  veBaseType      baseType;
  int             width;
  veBool          IsWidthSpec;
  veString        value;
  veBool          isSigned;
  
  baseType = veBasedNumberGetBaseType (basedNumber);
  width = veBasedNumberGetWidth (basedNumber);
  IsWidthSpec = veBasedNumberGetIsWidthSpec (basedNumber);
  value = veBasedNumberGetValue (basedNumber);
  isSigned = veBasedNumberGetIsSigned (basedNumber);
  if (IsWidthSpec == VE_TRUE) {
    /*
      writeOutput( int_to_str(width), 1);
      manish
    */
    char *tempVal =  int_to_str(width);
    writeOutput( tempVal, 1);
    free(tempVal);
  }
  writeOutput( "'", 1);
  if (isSigned == VE_TRUE)
    writeOutput( "s",1);
  
  switch (baseType)
  {
  case VE_BIN:
  {
    writeOutput( "b", 1);
    break;
  }
  case VE_DECIMAL:
  {
    writeOutput( "d", 1);
    break;
  }
  case VE_HEX:
  {
    writeOutput( "h", 1);
    break;
  }
  case VE_OCTAL:
  {
    writeOutput( "o", 1);
    break;
  }
  case VE_UNSET_BASE:
  {
    break;
  }
  }
  writeOutput( value, 1);
}





/*****
      Function      : floatNumberDecompile
      
      Purpose      :Decompiler for the object of type VeFloatNumber
      
      Purpose      :Pointer to the object of type VeFloatNumber
      
      Returns      :void
      
      End Header
*****/



void
Decompile::floatNumberDecompile (veNode expr)
{
  Protector protector(this, expr);

  veString          IntegerPart;
  veString          FloatPart;
  
  IntegerPart = veFloatNumGetInt (expr);
  FloatPart = veFloatNumGetFloat (expr);
  
  writeOutput( IntegerPart, 1);
  writeOutput( ".", 1);
  writeOutput( FloatPart, 0);
  
}






/*****
      Function      :expNumberDecompile
      
      Purpose      :Decompiler for the object of type VeExpNumber
      
      Purpose      :Pointer to the object of type VeExpNumber
      
      Returns      :void
      
      End Header
*****/

void
Decompile::expNumberDecompile (veNode expr)
{
  Protector protector(this, expr);

  veSignType      SignType;
  int             integerPart,  expPart;
  double            floatPart;
  char *tempVal = NULL;
  
  integerPart = veExpNumberGetInt (expr);
  floatPart = veExpNumberGetFloat (expr);
  expPart = veExpNumberGetExp (expr);
  SignType = veExpNumberGetSignType (expr);
  
  /*
    writeOutput( int_to_str(integerPart), 1);
    manish
  */
  tempVal =  int_to_str(integerPart);
  writeOutput( tempVal, 1);
  free(tempVal);
  
  if (floatPart)
  {
    /*
      writeOutput( double_to_str(floatPart), 1);
      manish
    */
    tempVal =  double_to_str(floatPart);
    writeOutput( tempVal, 1);
    free(tempVal);
  }
  writeOutput( "e", 1);
  
  switch (SignType)
  {
  case POS_SIGN:
  {
    writeOutput( "+", 1);
    break;
  }
  case NEG_SIGN:
  {
    writeOutput( "-", 1);
    break;
  }
  case UNSET_SIGN:
    INFO_ASSERT(0, "unsupported sign type UNSET_SIGN");
    break;
  }
  /*
    writeOutput( int_to_str(expPart), 1);
    manish
  */
  tempVal =  int_to_str(expPart);
  writeOutput( tempVal, 1);
  free(tempVal);
  
}


/*****
      Function       :moduleOrUdpInstanceDecompile
      
      Purpose        :Decompiler for a module or UDP instance
      
      Parameters     :veNode
      
      Returns        :void
      
      End Header
*****/


void
Decompile::moduleOrUdpInstanceDecompile (veNode node)
{
  Protector protector(this, node);

  CheetahList     delayIter, terminalIter;
  veNode          delay, terminal, range;
  veBool          isParamConn;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(node))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  obfuscate( veModuleOrUDPInstanceGetParentName (node));
  
  *this << eSpace;
  
  if ((veInstanceGetStrength0 (node) != UNSET_STRENGTH) && (veInstanceGetStrength1 (node) != UNSET_STRENGTH))
  {
    *this << eOpenParen;
    
    switch (veInstanceGetStrength0 (node))
    {
    case SUPPLY_0:
      *this << eSupply0;
      break;
    case STRONG_0:
      *this << eStrong0;
      break;
    case PULL_0:
      *this << ePull0;
      break;
    case WEAK_0:
      *this << eWeak0;
      break;
    case HIGHZ_0:
      *this << eHighz0;
      break;
    default :
      break;
    }
    
    *this << eComma;
    
    switch (veInstanceGetStrength1 (node))
    {
    case SUPPLY_1:
      *this << eSupply1;
      break;
    case STRONG_1:
      *this << eStrong1;
      break;
    case PULL_1:
      *this << ePull1;
      break;
    case WEAK_1:
      *this << eWeak1;
      break;
    case HIGHZ_1:
      *this << eHighz1;
      break;
    default :
      break;
    }
    
    *this << eCloseParen;
  }
  delayIter = veInstanceGetDelayOrParamAssignList (node);
  
  if (delayIter)
  {
    writeOutput( "#(", 0);
    
    delay = veListGetNextNode (delayIter);
    
    isParamConn = VE_FALSE;
    if (veNodeGetObjType (delay) == VE_PARAMCONN) {
      isParamConn = VE_TRUE; 
      writeOutput( ".", 0);
      obfuscate( veParamConnGetParamName (delay));
      *this << eOpenParen;
      delay = veParamConnGetValue (delay);
    }
    
    exprDecompile (delay);
    
    if (isParamConn == VE_TRUE)
      *this << eCloseParen;
    
    delay = veListGetNextNode (delayIter);
    
    for (; delay; delay = veListGetNextNode (delayIter)){
      *this << eComma;
      isParamConn = VE_FALSE; 
      if (veNodeGetObjType (delay) == VE_PARAMCONN){
        isParamConn = VE_TRUE; 
        writeOutput( ".", 0);
        obfuscate( veParamConnGetParamName (delay));
        *this << eOpenParen;
        delay = veParamConnGetValue (delay);
      }
      
      exprDecompile (delay);
      if (isParamConn == VE_TRUE)
        *this << eCloseParen;
    }
    
    *this << eCloseParen;
  }

  // I don't think we can free this casted attribute...
  CheetahStr iname(veInstanceGetName(node));
  veString name = (char *)veNodeGetNamedUserAttribute (node, "_Generate_");
  if (name == NULL) {
    name = iname;
  }
  
  if ( (name) && ( veModuleOrUDPInstanceIsDummyName(node) == VE_FALSE )) {
    obfuscate( name);
  }
  
  range = veInstanceGetRange (node);
  
  if (range)
    rangeDecompile (range);
  
  terminalIter = veInstanceGetTerminalConnectionList (node);
  
  if (terminalIter)
  {
    *this << eOpenParen;
    
    terminal = veListGetNextNode (terminalIter);
    
    portConnectionDecompile (terminal);
    
    terminal = veListGetNextNode (terminalIter);
    
    for (; terminal; terminal = veListGetNextNode (terminalIter))
    {
      *this << eComma;
      portConnectionDecompile (terminal);
    }
    *this << eCloseParen;
    
  }
  else {
    *this << eOpenParen;
    *this << eCloseParen;
  }
}




/*****
      Function       :portConnectionDecompile
      
      Purpose        :Decompiler for a terminal
      
      Parameters     : veNode
      
      Returns        :void
      
      End Header
*****/


void
Decompile::portConnectionDecompile (veNode node)
{
  Protector protector(this, node);

  veNode          connectedExpr;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(node))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  connectedExpr = vePortConnGetConnectedExpr(node);
  
  if (vePortConnGetPortName (node))
  {
    writeOutput( ".", 1);
    obfuscate( vePortConnGetPortName (node));
    *this << eOpenParen;
    if (connectedExpr)
      exprDecompile (connectedExpr);
    writeOutput( ")", 1);
    
  } else if (connectedExpr)
    exprDecompile (connectedExpr);
}

/*****
      Function       :gateInstanceDecompile
      
      Purpose        :Decompiler for a gate  instance
      
      Parameters     :veNode
      
      Returns        :void
      
      End Header
*****/


void
Decompile::gateInstanceDecompile (veNode gate)
{
  Protector protector(this, gate);

  CheetahList     delayIter, terminalIter;
  veNode          delay, terminal, range;
  veDriveStrengthType strength0, strength1;
  CheetahList     attrInstanceList;
  
  if ((attrInstanceList = veNodeGetAttrInstance(gate))) {
    atttrInstanceDecompile (attrInstanceList);
    *this << eNewline;
  }
  switch (veGateInstanceGetType (gate))
  {
  case AND_USE:
    *this << eAnd;
    break;
  case NAND_USE:
    *this << eNand;
    break;
  case OR_USE:
    *this << eOr;
    break;
  case NOR_USE:
    *this << eNor;
    break;
  case XOR_USE:
    *this << eXor;
    break;
  case XNOR_USE:
    *this << eXnor;
    break;
  case BUF_USE:
    *this << eBuf;
    break;
  case BUFIF0_USE:
    *this << eBufif0;
    break;
  case BUFIF1_USE:
    *this << eBufif1;
    break;
  case NOT_USE:
    *this << eNot;
    break;
  case NOTIF0_USE:
    *this << eNotif0;
    break;
  case NOTIF1_USE:
    *this << eNotif1;
    break;
  case PULLDOWN_USE:
    *this << ePulldown;
    break;
  case PULLUP_USE:
    *this << ePullup;
    break;
  case NMOS_USE:
    *this << eNmos;
    break;
  case RNMOS_USE:
    *this << eRnmos;
    break;
  case PMOS_USE:
    *this << ePmos;
    break;
  case RPMOS_USE:
    *this << eRpmos;
    break;
  case CMOS_USE:
    *this << eCmos;
    break;
  case RCMOS_USE:
    *this << eRcmos;
    break;
  case TRAN_USE:
    *this << eTran;
    break;
  case RTRAN_USE:
    *this << eRtran;
    break;
  case TRANIF0_USE:
    *this << eTranif0;
    break;
  case RTRANIF0_USE:
    *this << eRtranif0;
    break;
  case TRANIF1_USE:
    *this << eTranif1;
    break;
  case RTRANIF1_USE:
    *this << eRtranif1;
    break;
  default :
    break;
  }
  
  strength0 = veInstanceGetStrength0 (gate);
  strength1 = veInstanceGetStrength1 (gate);
  
  if ((strength0 != UNSET_STRENGTH) || (strength1 != UNSET_STRENGTH))
  {
    *this << eOpenParen;
    switch (strength0)
    {
    case SUPPLY_0:
      *this << eSupply0;
      break;
    case STRONG_0:
      *this << eStrong0;
      break;
    case PULL_0:
      *this << ePull0;
      break;
    case WEAK_0:
      *this << eWeak0;
      break;
    case HIGHZ_0:
      *this << eHighz0;
      break;
    default: *this << eStrong0;
      break;
    }
    *this << eComma;
    
    switch (strength1)
    {
    case SUPPLY_1:
      *this << eSupply1;
      break;
    case STRONG_1:
      *this << eStrong1;
      break;
    case PULL_1:
      *this << ePull1;
      break;
    case WEAK_1:
      *this << eWeak1;
      break;
    case HIGHZ_1:
      *this << eHighz1;
      break;
    default: *this << eStrong1;
      break;
    }
    
    *this << eCloseParen;
    
  }
  delayIter = veGateInstanceGetDelay (gate);
  
  if (delayIter)
  {
    writeOutput( "#(", 0);
    
    delay = veListGetNextNode (delayIter);
    
    exprDecompile (delay);
    
    delay = veListGetNextNode (delayIter);
    
    for (; delay; delay = veListGetNextNode (delayIter))
    {
      *this << eComma;
      exprDecompile (delay);
    }
    *this << eCloseParen;
  }
  
  CheetahStr gateName(veInstanceGetName(gate));
  
  if( gateName)/* && isalpha( gateName[0]) )*/
    obfuscate( gateName);
  
  range = veInstanceGetRange (gate);
  
  if (range)
    rangeDecompile (range);
  
  terminalIter = veGateInstanceGetTerminalList (gate);
  
  if (terminalIter)
  {
    *this << eOpenParen;
    
    terminal = veListGetNextNode (terminalIter);
    
    portConnectionDecompile (terminal);
    
    terminal = veListGetNextNode (terminalIter);
    
    for (; terminal; terminal = veListGetNextNode (terminalIter))
    {
      *this << eComma;
      portConnectionDecompile (terminal);
    }
    *this << eCloseParen;
    
  }
}

void
Decompile::atttrInstanceDecompile(CheetahList& attrInstanceList)
{
  veNode attrInstance;
  while ((attrInstance = veListGetNextNode (attrInstanceList)) != NULL){

    Protector protector(this, attrInstance);

    CheetahList attrSpecList;
    veNode attrSpec;
    veNode expr;
  
    attrSpecList = veAttrInstanceGetAttrSpecList (attrInstance);
    if (attrSpecList) {
      writeOutput( "(* ",0);
      attrSpec = veListGetNextNode (attrSpecList);
      CheetahStr name(veAttrSpecGetName (attrSpec));
      expr = veAttrSpecGetExpr (attrSpec);
      obfuscate( name);
      if (expr) {
        writeOutput( " = ",0);
        exprDecompile (expr);
      }
    
      while ((attrSpec = veListGetNextNode (attrSpecList))) {
        writeOutput( ", ", 0);
        CheetahStr name(veAttrSpecGetName (attrSpec));
        expr = veAttrSpecGetExpr (attrSpec);
        obfuscate( name);
        if (expr) {
          writeOutput( " = ",0);
          exprDecompile (expr);
        }
      }    
      writeOutput( " *)",0);
    }
  }
}
