// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
// Dummy main to force libcarbon.so to be created

#include "util/CarbonPlatform.h"
#include "shell/carbon_shelltypes.h"
#include "util/MemManager.h"
#include "codegen/carbon_priv.h"
#include "codegen/carbon_model.h"

int main ()
{
  struct carbon_model_descr * dummy=0;

  (void) dummy;

  return 0;
}


