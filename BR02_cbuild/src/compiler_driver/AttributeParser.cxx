// -*-c++-*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "AttributeParser.h"
#include "iodb/IODBNucleus.h"
#include "util/XmlParsing.h"
#include "libxml/parser.h"
#include "libxml/xmlerror.h"

AttributeParser::AttributeParser(IODBNucleus* iodb)
  : mIODB(iodb)
{
  xmlSetStructuredErrorFunc(this, sErrorHandler);
}

AttributeParser::~AttributeParser()
{
  xmlCleanupParser();
}

bool AttributeParser::parse(const char* file)
{
  // Clear the error string before parsing
  mErrStr.clear();

  // Attempt to read the file
  bool success = true;
  xmlDoc *doc = xmlReadFile(file, NULL, 0);
  if (doc == NULL) {
    success = false;
  }

  if (success) {
    // The root node must be DatabaseAttributes
    xmlNode *rootNode = xmlDocGetRootElement(doc);
    if (!XmlParsing::isElement(rootNode, "DatabaseAttributes")) {
      UtString rootElement;
      rootElement << XmlParsing::elementName(rootNode);
      mErrStr << "Root element of register XML must be 'DatabaseAttributes', saw '" << rootElement << "'";
      success = false;
    }

    // For now, only support version 1
    UtString version;
    XmlParsing::getProp(rootNode, "version", &version);
    if (version != "1") {
      mErrStr << "Unsupported attribute file version: " << version;
      success = false;
    }
    

    // Look for any Attribute elements
    for (xmlNode *node = rootNode->children; success && (node != NULL); node = node->next) {
      if (XmlParsing::isElement(node, "Attribute")) {
        // This must have two properties - name and type, and a child
        // text node with the value.
        xmlNode *child = node->children;
        UtString name, type;
        if (XmlParsing::getProp(node, "name", &name) &&
            XmlParsing::getProp(node, "type", &type) &&
            XmlParsing::isTextNode(child)) {
          UtString value;
          UInt32 intVal;
          // Interpret the value as a string or integer, as appropriate
          XmlParsing::getContent(child, &value);
          if (type == "string") {
            mIODB->addStringAttribute(name, value);
          } else if ((type == "integer") && (value >> intVal)) {
            mIODB->addIntAttribute(name, intVal);
          } else {
            mErrStr << "Illegal attribute type '" << type << "'";
            success = false;
          }
        } else {
          mErrStr << "Invalid attribute with name '" << name << "', type '" << type << "'";
          success = false;
        }
      }
    }
  }

  xmlFreeDoc(doc);
  return success;
}

void AttributeParser::sErrorHandler(void* data, xmlError* error)
{
  AttributeParser *parser = static_cast<AttributeParser*>(data);
  const char *msg = error->message;
  parser->mErrStr << msg << '\n';
}
