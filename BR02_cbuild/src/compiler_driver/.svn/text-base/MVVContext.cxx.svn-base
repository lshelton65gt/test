// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/MVVContext.h"
#include "compiler_driver/CarbonContext.h"
#include "util/HdlFileCollector.h"

static CarbonContext* sCarbonContext = NULL;
bool showMVVStatus = false;


MVVContext::MVVContext(CarbonContext* cc) 
{
  sCarbonContext = cc;
}


MVVContext::~MVVContext() {}


static bool mvvMsgMustBeErrors(int msgNo)
{
  // note that the message numbers listed in the mvv documentation are
  // off by 1, (the document is incorrect as of of 06/02/06)
  // (the first should be listed as 001 not 000)
  // 
  // Messages from mvv use three different messages IDs for each kind
  // of message it reports, the three ranges start at the values defined
  // in mvvEnum.h:
  // #define CHEETAH_MESG_START    0
  // #define JAGUAR_MESG_START  1500
  // #define MVV_MESG_START     3000

  SInt32 normalizedMsgNo = -1;
  
  if ( msgNo >= MVV_MESG_START ){
    normalizedMsgNo = msgNo - MVV_MESG_START;
  } else if ( msgNo >= JAGUAR_MESG_START ){
    normalizedMsgNo = msgNo - JAGUAR_MESG_START;
  } else {
    normalizedMsgNo = msgNo;
  }
  
  if ( ( (    0 <= normalizedMsgNo ) && ( normalizedMsgNo <= 100 ) ) ||
       ( (  551 <= normalizedMsgNo ) && ( normalizedMsgNo <= 575 ) ) ||
       ( (  577 <= normalizedMsgNo ) && ( normalizedMsgNo <= 600 ) )   )
  {
    return true;
  }
  else
  {
    return false;
  }
}

//! adjust the format argument to follow Carbon's message ID
static bool sRewriteMessage43002ToVerilogStyle(const char* format, UtString* new_format)
{
  // Design unit fooA is not found in library WORK
  if ( strncmp(format, "Design unit ",12) == 0 ) {
    UtString unit(format+12);
    size_t end = unit.find_first_of(" ",1,1);
    unit.erase(end+1);
    (*new_format) << " Module or UDP : (  " << unit << "  ) not defined.";
    return true;
  }
  return false;
}


void mvv_spew(FILE* logFilePtr,
              mvvMessageType severity, int mvvMsgNo,
              const char *sourceFileName,
              int sourceLineNo,
              const char *format, va_list pvar)
{
  // We've reserved message #s 40000-49999 for MVV
  const int msgNo = MVV_MSG_OFFSET + mvvMsgNo;
  MsgContext* msgContext = sCarbonContext->getMsgContext();
  MsgContextBase::Severity sev = MsgContextBase::eNote;
  if (!msgContext->getMsgSeverity(msgNo, &sev))
  {
    switch (severity)
    {
    case MVV_INFO:
    case MVV_DEBUG_INFO:
    case MVV_INFORMATION:
      if (showMVVStatus)
        sev = MsgContextBase::eNote;
      else
        sev = MsgContextBase::eSuppress;
      break;
    case MVV_SUPPRESSED:
      sev = MsgContextBase::eSuppress;      
      break;
    case MVV_WARNING:
    case MVV_SYNTH_WARN:
      if (msgContext->allWarningsSuppressed())
        sev = MsgContextBase::eSuppress;
      else
        sev = MsgContextBase::eWarning;
      break;
    case MVV_ERROR:
    case MVV_FATAL:
    case MVV_INTERNAL_ERROR:
    case MVV_SYSTEM_ERROR:
    case MVV_SYNTH_ERROR:
    case MVV_UNSET_SEVERITY:
      if ( mvvMsgMustBeErrors( mvvMsgNo ))
      {
        sev = MsgContextBase::eError;
      }
      else
      {
        sev = MsgContextBase::eAlert;
      }
      break;
    case MVV_SHOWALWAYS:
      sev = MsgContextBase::eNote;
      break;
    } // switch
  } // if

  UtString new_format;

  // check to see if we need to rewrite a message from mvv to make it
  // look like it is from cheetah (bug7122)
  // (missing entity, and verilog only design)
  bool haveVhdlFiles =
      sCarbonContext->getFileCollector()->hasHdlFiles(LangVer::VHDL87)
      ||
      sCarbonContext->getFileCollector()->hasHdlFiles(LangVer::VHDL93);

  bool missing_entity_message_should_be_from_cheetah = ( ( mvvMsgNo == 3002 ) && (not haveVhdlFiles ) );

  if ( missing_entity_message_should_be_from_cheetah && sRewriteMessage43002ToVerilogStyle( format, &new_format) ) {
    sCarbonContext->logAnalyzerMessages(logFilePtr, sev, msgNo, sourceFileName,
                                        sourceLineNo, new_format.c_str(), pvar);
  } else {
    sCarbonContext->logAnalyzerMessages(logFilePtr, sev, msgNo, sourceFileName,
                                        sourceLineNo, format, pvar);
  }
}

void mvv_prepop_spew(FILE* /*logFilePtr*/,
                     mvvMessageType severity, int /*msgNo*/,
                     const char* /*sourceFileName*/,
                     int /*sourceLineNo*/,
                     const char* /*format*/, va_list /*pvar*/)
{
  switch (severity)
  {
  case MVV_ERROR:
  case MVV_FATAL:
  case MVV_INTERNAL_ERROR:
  case MVV_SYSTEM_ERROR:
  case MVV_SYNTH_ERROR:
  case MVV_UNSET_SEVERITY:
    //mvv_spew(logFilePtr, severity, msgNo, sourceFileName, sourceLineNo, format, pvar);
    break;
  default:
    break;
  } // switch
} // mvv_prepop_spew


void MVVContext::adjustSeverities() {
  static SInt32 alert[] = {
    3515,  // " Port size mismatch. actual:size %d mapped to formal (%s):size %d. "
    -1};

  static SInt32 note[] = {
    3516,  // " No connection specified (explicit open) for formal port (<name>) in instance (<instance>). "
    -1
  };


  typedef UtHashMap<SInt32,MsgContextBase::Severity> SeverityMap;
  SeverityMap overrides;

  for (SInt32* p = alert;  *p != -1; ++p) {overrides[*p] = MsgContextBase::eAlert;}
  for (SInt32* p = note;  *p != -1; ++p) {overrides[*p] = MsgContextBase::eNote;}

  MsgContextBase::SeverityArray severities;
  MsgContextBase::IntArray msgNumbers;
  MsgContext* msgContext = sCarbonContext->getMsgContext();
  msgContext->getUnknownSeverityRange( MVV_MSG_OFFSET, MVV_MSG_OFFSET+9999,
                                       &severities, &msgNumbers );

  for (UInt32 i = 0; i < severities.size(); ++i) {
    MsgContextBase::Severity sev = severities[i];
    SInt32 msgNumber = msgNumbers[i];
    overrides[msgNumber - MVV_MSG_OFFSET] = sev;
  }

  for (SeverityMap::iterator p = overrides.begin(); p != overrides.end(); ++p) {    SInt32 msgNumber = p->first;
    MsgContextBase::Severity sev = p->second;
    if (( sev != MsgContextBase::eError ) && mvvMsgMustBeErrors( msgNumber)) {
      // warn user
      const char* sevname = NULL ;
      
      switch ( sev ){
      case MsgContextBase::eSuppress: {sevname = "silent"; break;}
      case MsgContextBase::eNote:     {sevname = "info"; break;}
      case MsgContextBase::eWarning:  {sevname = "warning"; break;}
      case MsgContextBase::eError:    {sevname = "error"; break;}
      default:   {sevname = "<unknown>"; break;}
      }

      const SourceLocator loc;
      msgContext->IgnoringMsgSeverityAdjust(&loc,sevname, msgNumber+MVV_MSG_OFFSET, "Error" );
      // force our internal severity table back to the severity that MVV originally had.
      sev = MsgContextBase::eError;
    }
    // Note that all messages that MVV prints get their severities
    // adjusted by mvv_spew, which calls getMsgSeverity.  So be
    // sure to note the exact severity there.  The only consequence to
    // telling MVV directly about severities is that it will not
    // successfully populate after it has an error.  So if the user
    // wants to demote a message below error, we do need to tell
    // MVV about it.  But the best thing to tell MVV is that
    // it's a warning, so that we still get the mvv_spew callback.
    // That way we can put suppressed messages into libdesign.suppress,
    // which may be handy.
    msgContext->putMsgNumSeverity(msgNumber + MVV_MSG_OFFSET, sev);

    switch (sev) {
    case MsgContext::eSuppress:
      // an undocumented feature is that MVV_SUPPRESSED is not a valid severity
      // value for mvvMessageSetSeverity, instead use mvvMessageSuppress() to suppress
      // messages. As of MVV_2004_1.21.d valid severity values for 
      // mvvMessageSetSeverity are:
      // MVV_INFO, MVV_WARNING, MVV_ERROR, MVV_FATAL, MVV_UNSET_SEVERITY.
      mvvMessageSuppress(msgNumber);
      break;
    case MsgContext::eNote:
    case MsgContext::eWarning:
    case MsgContext::eStatus:
    case MsgContext::eContinue:
      mvvMessageSetSeverity(msgNumber, MVV_WARNING);
      break;
    case MsgContext::eError:
    case MsgContext::eAlert:
    case MsgContext::eFatal:
      mvvMessageSetSeverity(msgNumber, MVV_ERROR);
      break;
    }
  } // for
} // void MVVContext::adjustSeverities

