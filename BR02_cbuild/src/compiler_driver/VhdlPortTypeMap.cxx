// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/VhdlPortTypeMap.h"
#include "util/UtHashMap.h"
#include "util/UtSet.h"
#include "util/UtList.h"
#include "util/ConstantRange.h"
#include "util/UtIOStream.h"

class VhdlPortTypeMap::Helper
{
public:
  CARBONMEM_OVERRIDES

  Helper() {}

  ~Helper() 
  {
    mPortTypeMap.clearPointerValues();
  }

  VhdlPortType* genPortType(const UtString& nameStr, const UtString& type)
  {
    VhdlPortType* vpt = NULL;
    StrToPortType::iterator p = mPortTypeMap.find(const_cast<UtString&>(nameStr));
    if (p == mPortTypeMap.end())
    {
      vpt = new VhdlPortType(type);
      mPortTypeMap[nameStr] = vpt;
    }
    else
    {
      vpt = p->second;
      vpt->putTypeStr(type);
    }
    return vpt;
  }

  VhdlPortType* genPortTypeBasic(const UtString& nameStr)
  {
    VhdlPortType* vpt = NULL;
    StrToPortType::iterator p = mPortTypeMap.find(const_cast<UtString&>(nameStr));
    if (p == mPortTypeMap.end())
    {
      vpt = new VhdlPortType;
      mPortTypeMap[nameStr] = vpt;
    }
    else
      vpt = p->second;
    return vpt;
  }
  
  const VhdlPortType* getPortType(const UtString& nameStr) const
  {
    const VhdlPortType* vpt = NULL;
    StrToPortType::const_iterator p = mPortTypeMap.find(nameStr);
    if (p != mPortTypeMap.end())
      vpt = p->second;
    return vpt;
  }
  
  void putTopLevelConfig(const char* workLib, const char* configName)
  {
    mTopLevelConfigLib.assign(workLib);
    mTopLevelConfigName.assign(configName);
  }

  bool hasTopLevelConfig() const 
  {
    return ! mTopLevelConfigName.empty();
  }

  void getTopLevelConfig(UtString* workLib, UtString* configName) const
  {
    workLib->assign(mTopLevelConfigLib);
    configName->assign(mTopLevelConfigName);
  }

  void addIEEEPackage(UtString* package)
  {
    if (false == hasIEEEPackage(*package))
    {
      mIEEEPackages.insert(*package);
    }
  }

  bool hasIEEEPackage(const UtString& package) const
  {
    PackageNames::iterator p = mIEEEPackages.find(package);

    if (p == mIEEEPackages.end())
      return false;
    else
      return true;
  }

  void getIEEEPackages(UtList<UtString>* packages) const
  {
    for (PackageNames::iterator p = mIEEEPackages.begin(); p != mIEEEPackages.end(); p++)
    {
      packages->push_back(*p);
    }
  }

  void putPackageName(UtString* packageName)
  {

    PackageNames::iterator p = mPackageNames.find(*packageName);

    if (p == mPackageNames.end())
    {
      mPackageNames.insert(*packageName);
    }
  }

  void getPackageNames(UtList<UtString>* packages) const
  {
    for (PackageNames::iterator p = mPackageNames.begin(); p != mPackageNames.end(); p++)
    {
      packages->push_back(*p);
    }
  }
  
private:
  typedef UtHashMap<UtString, VhdlPortType*> StrToPortType;
  typedef UtSet<UtString> PackageNames;
  StrToPortType mPortTypeMap;
  PackageNames mIEEEPackages;
  PackageNames mPackageNames;
  UtString mTopLevelConfigLib;
  UtString mTopLevelConfigName;
};

VhdlPortTypeMap::VhdlPortTypeMap()
{
  mHelper = new Helper;
}

VhdlPortTypeMap::~VhdlPortTypeMap()
{
  delete mHelper;
}

VhdlPortType* VhdlPortTypeMap::genPortType(const UtString& name, const UtString& type)
{
  return mHelper->genPortType(name, type);
}

VhdlPortType* VhdlPortTypeMap::genPortTypeBasic(const char* name)
{
  UtString nameStr(name);
  return mHelper->genPortTypeBasic(nameStr);
}

const VhdlPortType* VhdlPortTypeMap::getPortType(const UtString& name) const
{
  return mHelper->getPortType(name);
}

void VhdlPortTypeMap::addIEEEPackage(UtString* package)
{
  mHelper->addIEEEPackage(package);
}

bool VhdlPortTypeMap::hasIEEEPackage(const UtString& package) const
{
  return mHelper->hasIEEEPackage(package);
}

void VhdlPortTypeMap::getIEEEPackages(UtList<UtString>* packages) const
{
  mHelper->getIEEEPackages(packages);
}

void VhdlPortTypeMap::putTopLevelConfig(const char* workLib, const char* configName)
{
  mHelper->putTopLevelConfig(workLib, configName);
}

bool VhdlPortTypeMap::hasTopLevelConfig() const
{
  return mHelper->hasTopLevelConfig();
}

void VhdlPortTypeMap::getTopLevelConfig(UtString* workLib, UtString* configName) const
{
  mHelper->getTopLevelConfig(workLib, configName);
}

void VhdlPortTypeMap::putPackageName(UtString* packageName)
{
  mHelper->putPackageName(packageName);
}

void VhdlPortTypeMap::getPackageNames(UtList<UtString>* packages) const
{
  mHelper->getPackageNames(packages);
}

VhdlPortType::VhdlPortType(const UtString& type)
  : mType(type),
    mRange(NULL)
{}

VhdlPortType::VhdlPortType()
  : mRange(NULL)
{}

VhdlPortType::~VhdlPortType()
{
  delete mRange;
}

void VhdlPortType::putTypeStr(const UtString& type)
{
  mType = type;
}

const UtString& VhdlPortType::getTypeStr() const
{
  return mType;
}

static void sComplain(const UtString& name, const char* msg)
{
  UtIO::cout() << name << ": " << msg << UtIO::endl;
}

void VhdlPortType::putIntegerValueRange(SInt32 left, SInt32 right)
{
  FUNC_ASSERT(mRange == NULL, sComplain(mType, "Integer sub range already set."));
  mRange = new ConstantRange(left, right);
}

