// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/ArgProc.h"
#include "util/UtString.h"
#include "util/UtMD5.h"
#include "util/OSWrapper.h"
#include "util/Zstream.h"
#include <stdio.h>

static int sDoCrbnFile(const char* outFile, int argc, char* argv[])
{
  if (argc > 2)
  {
    fprintf(stderr, "-crbn requires exactly 3 arguments, -crbn <out> <in>.\n");
    return 1;
  }

  UtString outFileStr(outFile);

  const char* ext = strrchr(outFile, '.');
  bool needExtFix = (ext == NULL);
  if ((ext != NULL) && (strcmp(ext, ".crbn") != 0))
    needExtFix = true;
  if (needExtFix)
    outFileStr << ".crbn";

  ZOSTREAMDB(outDB, outFileStr.c_str());
  if (! outDB)
  {
    fprintf (stderr, "%s\n", outDB.getError());
    return 1;
  }

  UtMD5 mdhash;
  if (! mdhash.dbWriteCrbn(outDB, argv[1]))
  {
    fprintf (stderr, "%s\n", outDB.getError());
    return 1;
  }
  return 0;
}

int main (int argc, char* argv[])
{
  int stat = 0;
  unsigned char digest[16];

  ArgProc args;
  args.setDescription("Carbon MD5 Sum Utility",
                      "cmd5sum",
                      "Generates the 16 byte md5 digest for each file on the comand line.");
  {
    UtString synop;
    synop << "cmd5sum [-code] [-readcrbn]  file1 file2 ...";
    args.addSynopsis(synop);
    synop.clear();
    synop << "cmd5sum [-crbn] outfile infile";
    args.addSynopsis(synop);

    args.createSection("General");
    args.addBool("-code", "Generate a static array of unsigned chars that can be pasted into code. The static array contains the 16 bytes of the md5hash. However, all 16 bytes are bit-flipped for carbon purposes. By bit-flipping, it is much harder to find the code in an executable", false, 1);
    args.addToSection("General", "-code");

    args.addString("-crbn", "Generate a .crbn file. If the supplied filename does not have the .crbn extension, it will be added.", "", true, false, 1);
    args.addToSection("General", "-crbn");

    args.addBool("-readcrbn", "Read a .crbn file and print out the 16 byte md5 digest contained within. If a file in the filelist does not have a .crbn extension it will be added. The output will show the digest for the file that the .crbn file was created for. For example, -readcrbn myfile.crbn will output the saved digest for myfile", false, 1);
    args.addToSection("General", "-readcrbn");

    args.addBool("-h", "Print help", false, 1);
    args.addToSection("General", "-h");
  }

  int numOptions;
  UtString errMsg;
  if (args.parseCommandLine(&argc, argv, &numOptions, &errMsg) != ArgProc::eParsed)
  {
    UtString usageBrief;
    args.getSynopsis(&usageBrief);
    fprintf(stderr, "%s\n\n", usageBrief.c_str());
    fprintf(stderr, "%s\n", errMsg.c_str());
    return 1;
  }

  if (args.getBoolValue("-h") || (argc < 2))
  {
    UtString usage;
    args.getUsageVerbose(&usage);
    fprintf(stdout, "%s\n", usage.c_str());
    return 0;
  }

  if (args.isParsed("-crbn") == ArgProc::eParsed)
    stat = sDoCrbnFile(args.getStrValue("-crbn"), argc, argv);
  else
  {
    bool outputCode = args.getBoolValue("-code");
    bool readCrbnFiles = args.getBoolValue("-readcrbn");

    UtMD5 mdhash;

    // Used for printing when reading crbn files
    UtString nonCrbnFile;

    for (int i = 1; i < argc; ++i)
    {
      int error = 0;
      // used in print of file name
      const char* srcFile = argv[i];

      if (! readCrbnFiles)
      {
        UtString reason;
        if (! mdhash.sumFile(argv[i], digest, &reason))
        {
          fprintf(stderr, "%s\n", reason.c_str());
          stat = 1;
          error = 1;
          continue;
        }
      } // if ! readcrbnfiles
      else
      {
        UtString crbnFile(argv[i]);
        char* ext = strrchr(argv[i], '.');
        bool needExtFix = (ext == NULL);
        if ((ext != NULL) && (strcmp(ext, ".crbn") != 0))
          needExtFix = true;
        if (needExtFix)
          crbnFile << ".crbn";
        else
        {
          INFO_ASSERT(ext, "File extension is NULL");
          nonCrbnFile.clear();
          nonCrbnFile.append(crbnFile, 0, crbnFile.size() - 5);
          srcFile = nonCrbnFile.c_str();
        }
        
        ZISTREAMDB(in, crbnFile.c_str());
        if (! mdhash.dbReadCrbn(in, digest))
        {
          fprintf(stderr, "%s\n", in.getError());
          stat = 1;
          error = 1;
          continue;
        }
      }
      
      if (error == 0)
      {
        UInt32 val;
        if (outputCode)
        {
          fprintf(stdout, "{");
          for (int j = 0; j < 16; ++j)
          {
            val = ~digest[j] & 0xff;
            fprintf(stdout, "0x%2.2x", val);
            if (j != 15)
            {
              fprintf(stdout, ", ");
              if (((j + 1) % 4) == 0)
                fprintf(stdout, "\n ");
            }
          }
          fprintf(stdout, "};");
        }
        else
        {
          for (int j = 0; j < 16; ++j)
          {
            val = digest[j];
            fprintf(stdout, "%2.2x", val);
          }
        }
        
        fprintf(stdout, "  %s\n", srcFile);
      } // if error == 0
    } // for argc
  } // else
  
  return stat;
}
