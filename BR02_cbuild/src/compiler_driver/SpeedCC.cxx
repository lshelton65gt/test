// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Driver for the C++ compiler process

  This handles expanding include files in the c++ source based on a
  a binary include-file database.  The include-file database is 
  created by running

  \verbatim
  speedcc -build-db $CARBON_HOME/src/includes.bom $CARBON_HOME/lib/includes.lib
  \endverbatim

  That reads the ascii bill-of-materials to find grab the list of header files
  and creates an encrypted, compressed, amalgamation of them, including an
  index for fast lookup.

  So the first challenge is to replicate most of the functionality of

  \code
  #include "FILE"
  #define SYMBOL
  #ifndef SYMBOL
  #else
  #endif
  \endcode

  we need this so we can deal with include guards correctly.
  But also, so we don't get confused about nesting, be able to parse

  \code
  #if
  #endif
  \endcode

  we must pass through \#define,\#ifndef,\#else,\#endif,\#if,\#endif to
  the output file.  We also must pass through \#include \<FILE\>
  without interpreting it.

  When run without -build-db, we find one .cxx file on the command line, and 
  write out a temporary version of that with the includes unencrypted and
  expanded.  It then simply does fork & exec, which will let the
  C++ compiler run.  The args must be hacked to substitute the preprocessed
  file.  In addition #line directives are injected in the temp file if
  CARBON_LINE_LIMIT=0, this allows codeAnnotation to work without
  requiring that the user have a carbon internal license

  For now, no error messages are issued on an invalid command line for
  security reasons, or maybe laziness.

  Diagnostics are enabled when then environment variable CARBON_SPEEDCC_VERBOSE
  is set. When enabled, log messages to stdout trace the execution of speedcc
  and a map file logs where include files were found. The map file is formed by
  appending ".map" to the SpeedCC output file name.

*/

#include <stdarg.h>
#include "util/CarbonAssert.h"
#include "util/DynBitVector.h" 
#include "util/OSWrapper.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtLicense.h"
#include "util/UtLicenseMsg.h"
#include "util/UtMD5.h"
#include "util/UtSet.h"
#include "util/UtSimpleCrypt.h"
#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "util/UtVector.h"
#include "util/UtWildcard.h"
#include "util/Zstream.h"
#include "util/ZstreamZip.h"
#if (!pfWINDOWS)
#include "util/subprocesses.h"
#endif
#include "util/Stats.h"
#if pfLINUX|pfLINUX64
#include <sys/resource.h>
#include <string.h>
#include <errno.h>
#endif

//! \class Diagnostics
/*! The Diagnostic object outputs trace and diagnostic messages if the
 *  environment variable CARBON_SPEEDCC_VERBOSE is set.
 */
class Diagnostics {
public:

  Diagnostics () : mVerbose (false), mTestPchFallback (false)
  {
    const char *value;
    if ((value = getenv ("CARBON_SPEEDCC_VERBOSE")) != NULL && *value != '0') {
      mVerbose = true;
    }
    if ((value = getenv ("CARBON_SPEEDCC_TEST_PCH_FALLBACK")) != NULL && *value != '0') {
      mTestPchFallback = true;
    }
    mStats.putEchoStats (true);
    Verbose ("SpeedCC - %s\n", __DATE__);
    const char *carbon_home;
    if ((carbon_home = getenv ("CARBON_HOME)")) == NULL) {
      Verbose ("CARBON_HOME is not set.\n");
    } else {
      Verbose ("CARBON_HOME is \"%s\".\n", carbon_home);
    }
    printLimits ();
  }

  virtual ~Diagnostics ()
  {
    if (mVerbose) {
      mStats.printTotalStatistics ();
    }
  }

  //! \return iff verbose mode is enable
  bool isVerbose () const { return mVerbose; }

  //! \return iff we need to make the PCH compilation fail for testing
  bool isTestingPchFallback () const { return mTestPchFallback; }

  void printLimits ()
  {
#if 0
    // The implementation of printLimit is dependent on the particular flavour
    // of Linux running on the compilation host. If this information is needed
    // in future, then reactivate this code and make sure that the call to
    // getrlimit in the printLimit method works on all our Linuces.
    if (mVerbose) {
#if pfLINUX|pfLINUX64
      printLimit (RLIMIT_CPU,   "cpu:  ");
      printLimit (RLIMIT_DATA,  "data: ");
      printLimit (RLIMIT_FSIZE, "fsize:");
      printLimit (RLIMIT_STACK, "stack:");
      printLimit (RLIMIT_AS,    "as:   ");
#else
      Verbose ("Resource limits are not available.\n");
#endif
    }
#endif
  }

  //! Dump a argv vector
  void Verbose (UtStringArgv &argv0) const
  {
    if (mVerbose) {
      char **argv = argv0.getArgv ();
      fprintf (stdout, "+");
      while (*argv != NULL) {
        fprintf (stdout, " %s", *argv);
        argv++;
      }
      fprintf (stdout, "\n");
      fflush (stdout);
    }
  }

  //! Output a trace message if verbose mode is enabled
  void Verbose (const char *fmt, ...) const
#ifdef __GNUC__
    __attribute__ ((__format__ (printf, 2, 3)))
#endif
  {
    if (mVerbose) {
      va_list ap;
      va_start (ap, fmt);
      fprintf (stdout, "- ");
      vfprintf (stdout, fmt, ap);
      fflush (stdout);
    }    
  }

  void pushTimer ()
  {
    if (mVerbose) {
      mStats.pushIntervalTimer ();
    }
  }

  void popTimer (const char *tag)
  {
    if (mVerbose) {
      mStats.printIntervalStatistics (tag);
      mStats.popIntervalTimer ();
    }
  }

  void printTimer (const char *tag)
  {
    if (mVerbose) {
      mStats.printIntervalStatistics (tag);
    }
  }

private:

  bool mVerbose;                        //!< iff verbose message are visibile
  bool mTestPchFallback;                //!< iff testing fallback compilation
  Stats mStats;

#if 0
  // This is commented out because the call to getrlimit is sensitive to the
  // particular Linux release.
#if pfLINUX|pfLINUX64
  void printLimit (const int resource, const char *tag)
  {
    struct rlimit limit;
    if (getrlimit (resource, &limit) < 0) {
      UtString errmsg;
      OSGetLastErrmsg (&errmsg);
      fprintf (stderr, "ERROR: getrlimit failed for %s: %s\n", tag, errmsg.c_str ());
    } else {
      Verbose ("Limit %s %ld %ld\n", tag, limit.rlim_cur, limit.rlim_max);
    }
  }
#endif
#endif

};

// If you add another crypted string. Add an assertion to the CDB
// block in sDecryptStrings

static int sSlashIncCrypt[4];
static char sSlashIncDecrypt[5];

static int sUUCplusplusCrypt[11];
static char sUUCplusplusDecrypt[12];

static int sIncludeCrypt[7];
static char sIncludeDecrypt[8];

static int sEndifCrypt[5];
static char sEndifDecrypt[6];

static int sIfCrypt[2];
static char sIfDecrypt[3];

static int sIfdefCrypt[5];
static char sIfdefDecrypt[6];

static int sIfndefCrypt[6];
static char sIfndefDecrypt[7];

static int sElseCrypt[4];
static char sElseDecrypt[5];

static int sDefineCrypt[6];
static char sDefineDecrypt[7];

static int sUndefCrypt[5];
static char sUndefDecrypt[6];

static int sCarbonNoDeleteCrypt[16];
static char sCarbonNoDeleteDecrypt[17];

static int sCarbonNoTmpFileCrypt[18];
static char sCarbonNoTmpFileDecrypt[19];

static int sBuildDbCrypt[9];
static char sBuildDbDecrypt[10];

static int sStripCrypt[6];
static char sStripDecrypt[7];

static int sDashIDotCrypt[3];
static char sDashIDotDecrypt[4];

static int sDashICrypt[2];
static char sDashIDecrypt[3];

static int sDashSCrypt[2];
static char sDashSDecrypt[3];

static int sDashECrypt[2];
static char sDashEDecrypt[3];

static int sSrcIncCrypt[8];
static char sSrcIncDecrypt[9];

static int sSlashObjSlashCrypt[5];
static char sSlashObjSlashDecrypt[6];

static int sRequiresCdsInternalCrypt[31];
static char sRequiresCdsInternalDecrypt[32];

static int sCompilerOptionDashCrypt[17];
static char sCompilerOptionDashDecrypt[18];

static void sDecryptStrings()
{
  SCRAMBLECODE4("/inc", sSlashIncCrypt);
  sSlashIncDecrypt[4] = '\0';
  SCRAMBLECODE4(sSlashIncCrypt, sSlashIncDecrypt);

  SCRAMBLECODE11("__cplusplus", sUUCplusplusCrypt);
  sUUCplusplusDecrypt[11] = '\0';
  SCRAMBLECODE11(sUUCplusplusCrypt, sUUCplusplusDecrypt);

  SCRAMBLECODE7("include", sIncludeCrypt);
  sIncludeDecrypt[7] = '\0';
  SCRAMBLECODE7(sIncludeCrypt, sIncludeDecrypt);

  SCRAMBLECODE5("endif", sEndifCrypt);
  sEndifDecrypt[5] = '\0';
  SCRAMBLECODE5(sEndifCrypt, sEndifDecrypt);

  SCRAMBLECODE2("if", sIfCrypt);
  sIfDecrypt[2] = '\0';
  SCRAMBLECODE2(sIfCrypt, sIfDecrypt);

  SCRAMBLECODE5("ifdef", sIfdefCrypt);
  sIfdefDecrypt[5] = '\0';
  SCRAMBLECODE5(sIfdefCrypt, sIfdefDecrypt);

  SCRAMBLECODE6("ifndef", sIfndefCrypt);
  sIfndefDecrypt[6] = '\0';
  SCRAMBLECODE6(sIfndefCrypt, sIfndefDecrypt);

  SCRAMBLECODE4("else", sElseCrypt);
  sElseDecrypt[4] = '\0';
  SCRAMBLECODE4(sElseCrypt, sElseDecrypt);

  SCRAMBLECODE6("define", sDefineCrypt);
  sDefineDecrypt[6] = '\0';
  SCRAMBLECODE6(sDefineCrypt, sDefineDecrypt);

  SCRAMBLECODE5("undef", sUndefCrypt);
  sUndefDecrypt[5] = '\0';
  SCRAMBLECODE5(sUndefCrypt, sUndefDecrypt);
 
  SCRAMBLECODE16("CARBON_NO_DELETE", sCarbonNoDeleteCrypt);
  sCarbonNoDeleteDecrypt[16] = '\0';
  SCRAMBLECODE16(sCarbonNoDeleteCrypt, sCarbonNoDeleteDecrypt);

  SCRAMBLECODE17("CARBON_NO_TMPFILE", sCarbonNoTmpFileCrypt);
  sCarbonNoTmpFileDecrypt[17] = '\0';
  SCRAMBLECODE17(sCarbonNoTmpFileCrypt, sCarbonNoTmpFileDecrypt);

  SCRAMBLECODE9("-build-db", sBuildDbCrypt);
  sBuildDbDecrypt[9] = '\0';
  SCRAMBLECODE9(sBuildDbCrypt, sBuildDbDecrypt);

  SCRAMBLECODE6("-strip", sStripCrypt);
  sStripDecrypt[6] = '\0';
  SCRAMBLECODE6(sStripCrypt, sStripDecrypt);

  SCRAMBLECODE3("-I.", sDashIDotCrypt);
  sDashIDotDecrypt[3] = '\0';
  SCRAMBLECODE3(sDashIDotCrypt, sDashIDotDecrypt);

  SCRAMBLECODE2("-S", sDashSCrypt);
  sDashSDecrypt[2] = '\0';
  SCRAMBLECODE2(sDashSCrypt, sDashSDecrypt);

  SCRAMBLECODE2("-E", sDashECrypt);
  sDashEDecrypt[2] = '\0';
  SCRAMBLECODE2(sDashECrypt, sDashEDecrypt);

  SCRAMBLECODE2("-I", sDashICrypt);
  sDashIDecrypt[2] = '\0';
  SCRAMBLECODE2(sDashICrypt, sDashIDecrypt);
  
  SCRAMBLECODE8("/src/inc", sSrcIncCrypt);
  sSrcIncDecrypt[8] = '\0';
  SCRAMBLECODE8(sSrcIncCrypt, sSrcIncDecrypt);

  SCRAMBLECODE5("/obj/", sSlashObjSlashCrypt);
  sSlashObjSlashDecrypt[5] = '\0';
  SCRAMBLECODE5(sSlashObjSlashCrypt, sSlashObjSlashDecrypt);

  SCRAMBLECODE31("requires a cds_internal license", sRequiresCdsInternalCrypt);
  sRequiresCdsInternalDecrypt[31] = '\0';
  SCRAMBLECODE31(sRequiresCdsInternalCrypt, sRequiresCdsInternalDecrypt);

  SCRAMBLECODE17("Compiler option -", sCompilerOptionDashCrypt);
  sCompilerOptionDashDecrypt[17] = '\0';
  SCRAMBLECODE17(sCompilerOptionDashCrypt, sCompilerOptionDashDecrypt);

#ifndef CHECKIN
#ifdef CDB
  INFO_ASSERT(strcmp("/inc", sSlashIncDecrypt) == 0, "sSlashIncDecrypt has changed");
  INFO_ASSERT(strcmp("__cplusplus", sUUCplusplusDecrypt) == 0, "sUUCplusplusDecrypt has changed");
  INFO_ASSERT(strcmp("include", sIncludeDecrypt) == 0, "sIncludeDecrypt has changed");
  INFO_ASSERT(strcmp("endif", sEndifDecrypt) == 0, "sEndifDecrypt has changed");
  INFO_ASSERT(strcmp("if", sIfDecrypt) == 0, "sIfDecrypt has changed");
  INFO_ASSERT(strcmp("ifdef", sIfdefDecrypt) == 0, "sIfdefDecrypt has changed");
  INFO_ASSERT(strcmp("ifndef", sIfndefDecrypt) == 0, "sIfndefDecrypt has changed");
  INFO_ASSERT(strcmp("else", sElseDecrypt) == 0, "sElseDecrypt has changed");
  INFO_ASSERT(strcmp("define", sDefineDecrypt) == 0, "sDefineDecrypt has changed");
  INFO_ASSERT(strcmp("undef", sUndefDecrypt) == 0, "sUndefDecrypt has changed");
  INFO_ASSERT(strcmp("CARBON_NO_DELETE", sCarbonNoDeleteDecrypt) == 0, "sCarbonNoDeleteDecrypt has changed");
  INFO_ASSERT(strcmp("-build-db", sBuildDbDecrypt) == 0, "sBuildDbDecrypt has changed");
  INFO_ASSERT(strcmp("-strip", sStripDecrypt) == 0, "sStripDecrypt has changed");
  INFO_ASSERT(strcmp("-I.", sDashIDotDecrypt) == 0, "sDashIDotDecrypt has changed");
  INFO_ASSERT(strcmp("-S", sDashSDecrypt) == 0, "sDashSDecrypt has changed");
  INFO_ASSERT(strcmp("-E", sDashEDecrypt) == 0, "sDashEDecrypt has changed");
  INFO_ASSERT(strcmp("-I", sDashIDecrypt) == 0, "sDashIDecrypt has changed");
  INFO_ASSERT(strcmp("/src/inc", sSrcIncDecrypt) == 0, "sSrcIncDecrypt has changed");
  INFO_ASSERT(strcmp("CARBON_NO_TMPFILE", sCarbonNoTmpFileDecrypt) == 0, "sCarbonNoTmpFileDecrypt has changed");
  INFO_ASSERT(strcmp("/obj/", sSlashObjSlashDecrypt) == 0, "sSlashObjSlashDecrypt has changed");
  INFO_ASSERT(strcmp("requires a cds_internal license", sRequiresCdsInternalDecrypt) == 0, "sRequiresCdsInternalDecrypt has changed");
  INFO_ASSERT(strcmp("Compiler option -", sCompilerOptionDashDecrypt) == 0, "sCompilerOptionDashDecrypt has changed");
#endif
#endif
}



/*
  To add a new compiler to to speedcc, you must generate a special md5
  hash for it. And, you must add it to the compHash array below. Note
  the comments above compHash. 

  The executable to use to create the hash is cmd5sum which is located
  under compiler_driver. You can use 
  carbon_exec compiler_driver/cmd5sum if it isn't in the bin
  directory.

  To create the special md5 hash for a compiler:
  cmd5sum -code <compiler_exec>
  
  For example, if you want to create an md5 hash for gcc-3.4.5 g++ on
  Linux, you would
  cmd5sum -code /tools/linux/gcc-3.4.5/bin/g++
  
  It will print out something like:
  {0x67, 0x6f, 0xee, 0x8c, 
  0x2f, 0x4f, 0x86, 0xbc, 
  0x19, 0xe4, 0x07, 0x66, 
  0x84, 0xac, 0xe1, 0x8d};  /tools/linux/gcc-3.4.5/bin/g++
  
  The actual file is printed out so you can decide if you want to
  use it as a comment or remove it. You take the above and create an
  unsigned char array for it like:
  
  static const unsigned char sHashGcc345[] =
  {0x67, 0x6f, 0xee, 0x8c, 
  0x2f, 0x4f, 0x86, 0xbc, 
  0x19, 0xe4, 0x07, 0x66, 
  0x84, 0xac, 0xe1, 0x8d};

  Then you add sHashGcc345 to the compHash array.

  Note you must do this for all supported platforms. You can use the
  Linux cmd5sum to perform the hash on all the different platforms for
  a given compiler. So, for the sminggw one you would simply:
  cmd5sum -code <path to bin/gxx++>/g++

  Notice the #if's below for the different platforms.
*/


// Same bits on Linux/Linux64
static const unsigned char sHashIcc90[] =
  {0xab, 0xaa, 0xe2, 0x8d, 
   0x27, 0x30, 0xa7, 0x07, 
   0x32, 0x37, 0x17, 0x19, 
   0xc2, 0x93, 0x3f, 0x9b};     // /tools/linux/icc-9.0/bin/icpc

static const unsigned char sHashIcc80[] =
  {0x90, 0x0a, 0x6b, 0x80,
   0x71, 0x24, 0xf7, 0x19,
   0x0b, 0xf7, 0x07, 0x96,
   0x11, 0xb3, 0xc4, 0x53};

#if pfLINUX
#define THISARCH "Linux"
// 2.95.3 with binutils 2.15
static const unsigned char sHashGcc2953b[] = 
  {0x7e, 0x09, 0x63, 0x49,
   0x5d, 0x8a, 0xf0, 0x5e,
   0xb5, 0x69, 0xc3, 0xd0,
   0x59, 0xb7, 0xc1, 0x06}; 

static const unsigned char sHashGcc343mingw[] =
  {0x89, 0x09, 0x3c, 0x50, 
   0xd4, 0xae, 0xa8, 0x94, 
   0xf4, 0xc5, 0xc2, 0x5c, 
   0xd4, 0xa2, 0xa4, 0x6a};  // /tools/linux/gcc-3.4.3-a-mingw/bin/i386-mingw32msvc-g++

static const unsigned char sHashGcc345mingw[] =
  {0x6a, 0x89, 0xca, 0xfb, 
   0x62, 0x66, 0xc1, 0xd4, 
   0x1b, 0xf1, 0x3c, 0xfc, 
   0x69, 0x72, 0x3c, 0x51}; // /tools/linux/gcc-3.4.5-a-mingw/bin/i386-mingw32msvc-g++

static const unsigned char sHashGcc410mingw[] =
  {0xa8, 0x44, 0x45, 0x30, 
   0xba, 0x75, 0xc8, 0x37, 
   0xaf, 0x32, 0x90, 0xf7, 
   0xc3, 0x21, 0x6b, 0x97};  // i386-mingw32msvc-g++

static const unsigned char sHashGcc343[] =
  {0x2f, 0x2e, 0x24, 0x19, 
   0x94, 0xb7, 0xbd, 0xc5, 
   0x6b, 0xcc, 0xbf, 0xb3, 
   0x7a, 0xfb, 0x69, 0xd3};  // /tools/linux/gcc-3.4.3-a/bin/g++

static const unsigned char sHashGcc345[] =
  {0xa7, 0x40, 0x6f, 0x21, 
   0x20, 0xbb, 0xc0, 0x47, 
   0x30, 0x0f, 0xba, 0x0a, 
   0xec, 0x31, 0xce, 0x9f};             // /tools/linux/gcc-3.4.5-a/bin/g++

static const unsigned char sHashGcc345_64[] =
  {0xc6, 0xe0, 0x44, 0x0c, 
   0x65, 0x63, 0x7a, 0xa6, 
   0x4a, 0x3d, 0xb3, 0xde, 
   0x97, 0x92, 0x39, 0xa2};             // /tools/linux64/gcc-3.4.5-a/bin/g++

static const unsigned char sHashGcc346[] =
  {0x67, 0x31, 0x29, 0x8c, 
   0x06, 0x00, 0x64, 0xd4, 
   0x8e, 0x6e, 0x73, 0x50, 
   0x95, 0x58, 0x58, 0x63};             // /tools/linux/gcc-3.4.6-a/bin/g++

static const unsigned char sHashGcc410[] =
  {0x0a, 0xa6, 0x6e, 0xcc, 
   0xef, 0xe2, 0x07, 0x2f, 
   0xee, 0x65, 0x1d, 0x47, 
   0xb6, 0xd2, 0x49, 0x18};             // /tools/linux/gcc-4.1.0/bin/g++

static const unsigned char sHashGcc422[] =
  {0xa4, 0xfa, 0x48, 0xc3, 
   0xe9, 0x87, 0x7a, 0x66, 
   0xe0, 0xf3, 0x05, 0x0e, 
   0xf3, 0x78, 0x7a, 0x42};             // /tools/linux/ES4/gcc-4.2.2/bin/g++

static const unsigned char sHashGcc422_64[] =
  {0x2b, 0x30, 0xb1, 0x5e, 
   0x28, 0xe6, 0xe5, 0xe9, 
   0x32, 0xb9, 0xd3, 0xe2, 
   0x10, 0x28, 0x52, 0x09};             // /tools/linux64/ES4/gcc-4.2.2/bin/g++

static const unsigned char sHashGcc422_Binutils2_20_1[] =
  {0x02, 0x97, 0x2b, 0x25, 
   0xb2, 0xb1, 0x72, 0x7f, 
   0xb4, 0xe6, 0x35, 0xc8, 
   0xd8, 0x75, 0xaf, 0xad};             // /tools/linux/ES4/gcc-4.2.2-binutils-2.20.1/bin/g++

static const unsigned char sHashGcc422_64_Binutils2_20_1[] =
  {0xbe, 0xd8, 0xa2, 0x25, 
   0xa5, 0x0f, 0x7e, 0xbb, 
   0x28, 0xb0, 0x3c, 0x3b, 
   0xf6, 0x9e, 0xc1, 0x8f};             // /tools/linux64/ES4/gcc-4.2.2-binutils-2.20.1/bin/g++

static const unsigned char sHashES5Gcc424[] =
  {0xe9, 0x60, 0x52, 0x1d, 
   0x0f, 0x8b, 0x19, 0x80, 
   0x9a, 0x53, 0x2a, 0x5b, 
   0x66, 0x60, 0x0b, 0x9a};             // /tools/linux/ES5/gcc-4.2.4/bin/g++

static const unsigned char sHashES5Gcc432[] =
  {0xb8, 0x9e, 0x55, 0xfc, 
   0xd6, 0xe5, 0x79, 0xea, 
   0x70, 0xb7, 0x85, 0xdd, 
   0x07, 0xaf, 0x58, 0x72};             // /tools/linux/ES5/gcc-4.3.2/bin/g++

static const unsigned char sHashGcc343_64[] =
  {0,0,0,0,
   0,0,0,0,
   0,0,0,0,
   0,0,0,0};

static const unsigned char sHashGcc410_64[] =
  {0,0,0,0,
   0,0,0,0,
   0,0,0,0,
   0,0,0,0};

#elif pfLINUX64

#define THISARCH "Linux64"

static const unsigned char sHashGcc2953b[16] =
{0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0};
static const unsigned char sHashGcc343mingw[16] =
{0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0};
static const unsigned char sHashGcc345mingw[16] =
{0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0};

static const unsigned char sHashGcc410mingw[] =
{0xa8, 0x44, 0x45, 0x30, 
 0xba, 0x75, 0xc8, 0x37, 
 0xaf, 0x32, 0x90, 0xf7, 
 0xc3, 0x21, 0x6b, 0x97};  // i386-mingw32msvc-g++


static const unsigned char sHashGcc410_64[16] =
{0x80, 0x76, 0xf6, 0xbb, 
 0x99, 0x12, 0x9b, 0x74, 
 0x6d, 0x1d, 0xce, 0x0f, 
 0x02, 0xb9, 0x23, 0x03};       //  /tools/linux64/gcc-4.1.0/bin/g++


static const unsigned char sHashGcc343[] =
{0x2f, 0x2e, 0x24, 0x19, 
 0x94, 0xb7, 0xbd, 0xc5, 
 0x6b, 0xcc, 0xbf, 0xb3, 
 0x7a, 0xfb, 0x69, 0xd3};  // /tools/linux/gcc-3.4.3-a/bin/g++

static const unsigned char sHashGcc410[] =
  {0x0a, 0xa6, 0x6e, 0xcc, 
   0xef, 0xe2, 0x07, 0x2f, 
   0xee, 0x65, 0x1d, 0x47, 
   0xb6, 0xd2, 0x49, 0x18};  // /tools/linux/gcc-4.1.0/bin/g++

static const unsigned char sHashGcc422[] =
  {0xa4, 0xfa, 0x48, 0xc3, 
   0xe9, 0x87, 0x7a, 0x66, 
   0xe0, 0xf3, 0x05, 0x0e, 
   0xf3, 0x78, 0x7a, 0x42};             // /tools/linux/ES4/gcc-4.2.2/bin/g++

static const unsigned char sHashGcc422_64[] =
  {0x2b, 0x30, 0xb1, 0x5e, 
   0x28, 0xe6, 0xe5, 0xe9, 
   0x32, 0xb9, 0xd3, 0xe2, 
   0x10, 0x28, 0x52, 0x09};             // /tools/linux64/ES4/gcc-4.2.2/bin/g++

static const unsigned char sHashGcc422_Binutils2_20_1[] =
  {0x02, 0x97, 0x2b, 0x25, 
   0xb2, 0xb1, 0x72, 0x7f, 
   0xb4, 0xe6, 0x35, 0xc8, 
   0xd8, 0x75, 0xaf, 0xad};             // /tools/linux/ES4/gcc-4.2.2-binutils-2.20.1/bin/g++

static const unsigned char sHashGcc422_64_Binutils2_20_1[] =
  {0xbe, 0xd8, 0xa2, 0x25, 
   0xa5, 0x0f, 0x7e, 0xbb, 
   0x28, 0xb0, 0x3c, 0x3b, 
   0xf6, 0x9e, 0xc1, 0x8f};             // /tools/linux64/ES4/gcc-4.2.2-binutils-2.20.1/bin/g++

static const unsigned char sHashES5Gcc424[] =
  {0xb1, 0xfa, 0xd8, 0xba, 
   0xb3, 0x18, 0x03, 0x8f, 
   0x05, 0x6f, 0x8d, 0xa0, 
   0x6f, 0x79, 0x4a, 0xf8};  // /tools/linux64/ES5/gcc-4.2.4/bin/g++

static const unsigned char sHashES5Gcc432[] =
  {0xa7, 0x5a, 0x70, 0xd6, 
   0xb3, 0x64, 0xfd, 0x01, 
   0x49, 0x6b, 0xfe, 0x74, 
   0xcf, 0xe9, 0x70, 0xa3};  // /tools/linux64/ES5/gcc-4.3.2/bin/g++

static const unsigned char sHashGcc343_64[] =
  {0xab, 0xe5, 0x6f, 0x7c, 
   0xb0, 0x1e, 0x3f, 0x59, 
   0xfd, 0x92, 0x5c, 0x6e, 
   0x4f, 0x03, 0xf8, 0x26};             // /tools/linux64/gcc-3.4.3-a/bin/g++

static const unsigned char sHashGcc345_64[] =
  {0xc6, 0xe0, 0x44, 0x0c, 
   0x65, 0x63, 0x7a, 0xa6, 
   0x4a, 0x3d, 0xb3, 0xde, 
   0x97, 0x92, 0x39, 0xa2};             // /tools/linux64/gcc-3.4.5-a/bin/g++

static const unsigned char sHashGcc345[] =
  {0xa7, 0x40, 0x6f, 0x21, 
   0x20, 0xbb, 0xc0, 0x47, 
   0x30, 0x0f, 0xba, 0x0a, 
   0xec, 0x31, 0xce, 0x9f};             // /tools/linux/gcc-3.4.5-a/bin/g++

static const unsigned char sHashGcc346[] =
  {0x67, 0x31, 0x29, 0x8c, 
   0x06, 0x00, 0x64, 0xd4, 
   0x8e, 0x6e, 0x73, 0x50, 
   0x95, 0x58, 0x58, 0x63};             // /tools/linux/gcc-3.4.6-a/bin/g++

#elif pfWINDOWS

#define THISARCH "Win"

#endif

char * gPchBasename = NULL;
bool gIncludedPchBaseFile = false;

#define cSysDir "system"


static void sConvertSysIncludeName(const char* sysName, UtString* newName)
{
  *newName = cSysDir;
  *newName << "/";

  // Convert all "/" to "_"
  for (StrToken tok(sysName, "/"); !tok.atEnd(); ++tok)
    *newName << "_" << *tok;
}        

static int sLoadBillOfMaterials(const char* bomFile, ZostreamZip* arFile,
  bool cvtSysFiles, FILE *mapFile)
{
  UtString openReason, buf;

  FILE* f = OSFOpen(bomFile, "rb", &openReason);
  if (f == NULL)
  {
    fprintf(stderr, "%s\n", openReason.c_str());
    return 1;
  }
  while (fgetline(f, &buf))
  {
    StringUtil::strip(&buf);
    
    if (not buf.empty()) {
      // If the file was found in an absolute path, such as
      // directory /foo/bar/xyz.h, then symlink it in a local subdirectory
      //     "system/foo_bar_xyz.h"
      if (cvtSysFiles && (buf[0] == '/')) {
        UtString newName;
        sConvertSysIncludeName(buf.c_str(), &newName);
        fprintf (mapFile, "convert %s to %s\n", buf.c_str (), newName.c_str ());
        buf = newName;
      }

      if (!arFile->addFile(buf.c_str()))
      {
        fprintf(stderr, "%s\n", arFile->getFileError());
        fclose(f);
        return 1;
      }
      fprintf (mapFile, "loaded %s\n", buf.c_str ());
    }
  }
  fclose(f);
  return 0;
} // static int sLoadBillOfMaterials

static int sBuildDB(const char* bomFile,
                    const char* libFile,
                    const char* genBomFile,
                    const char *mapFileName)
{
  UtString buf, path, file, pipeName;
  OSParseFileName(bomFile, &path, &file);

  // open the map file that contains a list of all the files added to the database
  UtString errmsg;
  FILE *mapFile;
  if ((mapFile = OSFOpen (mapFileName, "w", &errmsg)) == NULL) {
    fprintf (stderr, "%s: %s\n", mapFileName, errmsg.c_str ());
    return 1;
  }

  // Start writing the archive
  ZOSTREAMZIP(arFile, libFile);
  if (arFile.fail())
  {
    fprintf(stderr, "%s\n", arFile.getFileError());
    return 1;
  }
  
  // We are called from the top of the object tree, which
  // is where the generated includes are.  Load them first.
  if (sLoadBillOfMaterials(genBomFile, &arFile, false, mapFile) != 0)
    return 1;

  // Based on the full pathname of the bom file, find the
  // src/inc dir and CD there, before loading the source
  // include files.
  path << sSlashIncDecrypt;
  UtString chdirErr;
  if (OSChdir(path.c_str(), &chdirErr) != 0)
  {
    fprintf(stderr, "%s\n", chdirErr.c_str());
    return 1;
  }
  
  if (sLoadBillOfMaterials(bomFile, &arFile, false, mapFile) != 0)
    return 1;
  
  if (!arFile.finalize())
  {
    fprintf(stderr, "%s\n", arFile.getFileError());
    return 1;
  }

  // close the map file
  fclose (mapFile);

  return 0;
} // static int sBuildDB

struct CmpStringBySize: public HashValue<UtString> {
  bool lessThan(const UtString& s1, const UtString& s2) const {
    int cmp = (int) s1.size() - (int) s2.size();
    if (cmp == 0) {
      cmp = strcmp(s1.c_str(), s2.c_str());
    }
    return cmp < 0;
  }
};

static int sStrip(const char* prefix,
                  const char* infilename,
                  const char* outfilename,
                  char** patterns)
{
  // Examine all the patterns and 
  UtVector<UtWildcard> wildcards;
  for (; *patterns != NULL; ++patterns) {
    wildcards.push_back(UtWildcard(*patterns));
  }

  // Make sure we can write the output file
  UtOBStream outfile(outfilename);
  if (!outfile.is_open()) {
    fprintf(stderr, "%s\n", outfile.getErrmsg());
    return 1;
  }

  // Rip through an object file and look for C++ names and change
  // them to random names.

  // Read the entire file into a buffer.
  UtString fileBuffer;
  {
    ZISTREAM(infile, infilename);
    if (!infile.is_open()) {
      fprintf(stderr, "%s\n", infile.getErrmsg());
      return 1;
    }
#   define MYBUFSZ 100000
    char buf[MYBUFSZ];
    UInt32 numChars;
    while ((numChars = infile.read(buf, MYBUFSZ)) > 0) {
      fileBuffer.append(buf, numChars);
    }
    if (!infile.eof()) {
      fprintf(stderr, "%s\n", infile.getErrmsg());
      return 1;
    }
  }      
  
  

  static const unsigned char legalChars[] =
    "_@.0123456789abcdefghijklmnopqsrtuvwxyzABCDEFGHIJKLMNOPQSRTUVWXYZ";
  size_t numLegalChars = strlen((char*) legalChars);
  bool isLegal[256];
  for (int i = 0; i < 256; ++i)
    isLegal[i] = false;
  for (const unsigned char* p = legalChars; *p != '\0'; ++p)
    isLegal[(size_t) *p] = true;

  // Walk through the file buffer, looking for ascii strings that being
  // with a few key C++ mangling prefixes, and collect those into a
  // StringMap
  typedef UtHashSet<UtString,HashValue<UtString> > StringSet;
  StringSet allStrings;
  typedef UtVector<char*> PosVec;
  typedef UtHashMap<UtString,PosVec,CmpStringBySize > StringMap;
  StringMap stringMap;

  char* wordStart = NULL;
  for (char* p = &fileBuffer[0], *e = p + fileBuffer.size(); p != e; ++p) {
    unsigned char c = (unsigned char) *p;
    bool isCLegal = isLegal[c];

    if (c == '\0') {
      if ((p - wordStart > 3) && (wordStart != NULL)) {
        // Keep track of all the strings in our model so that we don't create a clash
        // when we rename the ones we want
        UtString wordString(wordStart);
        allStrings.insert(wordString);
        //fprintf(stdout, "%s\n", wordStart);

        // Now put the strings in our Map
        for (UInt32 p = 0; p < wildcards.size(); ++p) {
          UtWildcard& wildcard = wildcards[p];
          if (wildcard.isMatch(wordStart)) {
            PosVec& v = stringMap[wordString];
            v.push_back(wordStart);
            break;
          }
        }
      }
      wordStart = NULL;
    }
    else if ((wordStart == NULL) && isCLegal) {
      wordStart = p;
    }
    else if ((wordStart != NULL) && !isCLegal) {
      wordStart = NULL;
    }
  } // for

  // Now we know all the strings we want to translate.  Find unique translations
  // that fit in the same number of characters as the original string.

  SInt32 legalSpaceCounter = 0;
  SInt32 prefixLen = strlen(prefix);
  if ((prefixLen >= 4) && (strncmp(prefix, "lib", 3) == 0)) {
    prefixLen -= 3;
    prefix += 3;
  }

  // It's simpler to do the short symbols first, so sort them in size order
  // Note that the hash compartor for StringMap is based first string size
  for (StringMap::SortedLoop p = stringMap.loopSorted(); !p.atEnd(); ++p) {
    const UtString& orig = p.getKey();
    PosVec& v = p.getValue();
    SInt32 sz = orig.size();
    UtString buf(orig);
    strncpy(&buf[0], prefix, std::min(sz, prefixLen));
    SInt32 prefixOffset = prefixLen;

    while (allStrings.find(buf) != allStrings.end()) {
      // Find a word that doesn't clash with any we've encountered in the file
      // by starting with a prefix _C and walking through the legal namespace
      // until we find one that does not clash
      ++legalSpaceCounter;
      SInt32 numChars = (legalSpaceCounter + numLegalChars - 1) / numLegalChars;
      if (numChars + prefixOffset >= sz) {
        prefixOffset = sz - numChars;
        if (prefixOffset < 0) {
          fprintf(stderr, "\n\n***Internal error: strip failed for symbol %s\n",
                  orig.c_str());
          return 1;
        }
      }

      UInt32 legalToken = legalSpaceCounter;
      for (SInt32 i = 0; i < numChars; ++i) {
        char c = legalChars[legalToken % numLegalChars];
        legalToken = legalToken / numLegalChars;
        buf[i + prefixOffset] = c;
      }
    }

    // Be sure we don't do another translation that bumps into this one
    allStrings.insert(buf);
    fprintf(stdout, "[%u] %s\t%s\n", (unsigned int) v.size(),
            orig.c_str(), buf.c_str());

    // Replace all occurrences in the file buffer
    for (UInt32 i = 0; i < v.size(); ++i) {
      char* p = v[i];
      INFO_ASSERT(strlen(p) == (size_t) sz, "string size changed");
      strcpy(p, buf.c_str());
    }
  } // for

  outfile << fileBuffer;
  if (!outfile.close()) {
    fprintf(stderr, "%s\n", outfile.getErrmsg());
    return 1;
  }
  return 0;
} // static int sStrip

class PreProcessor
{
public:
  PreProcessor(const char* zipFile, const char* carbonHome, Diagnostics &diag)
    : mCarbonHome(carbonHome),
      mIncludeLib(zipFile),
      mOutfile(NULL),
      mInfile(NULL),
      mMapfile(NULL),
      mEnable(0),
      mZipArchive (NULL),
      mDiag (diag)
  {
    mPersistentDefines.insert (sUUCplusplusDecrypt);
    reset ();
    // clear the old zip archive
    if (mZipArchive != NULL) {
      delete mZipArchive;
    }
    ZISTREAMZIP_ALLOC(mZipArchive, mIncludeLib.c_str ());
    if (mZipArchive->fail())
    {
      fprintf(stderr, "%s\n", mZipArchive->getFileError());
      exit(1);
    }
    pushEnable(true);
    mIncludeDir << mCarbonHome << "/" << sIncludeDecrypt << "/";

    mLineLimit = 100000;        // gcc seems to be OK with 100k lines
    char* linelim = getenv("CARBON_LINE_LIMIT");
    if (linelim != NULL) {
      char* endptr;
      UtString errmsg;
      mLineLimit = OSStrToU32(linelim, &endptr, 10, &errmsg);
      if (*endptr != '\0') {
        fprintf(stderr, "Parse error for $CARBON_LINE_LIMIT: %s\n",
                errmsg.c_str());
        exit(1);
      }
    }
  }

  ~PreProcessor()
  {
    delete mZipArchive;
    if (mMapfile != NULL) {
      // close the SpeedCC map file
      fclose (mMapfile);
    }
  }

  void map (const char *fmt, ...)
  {
    if (mMapfile != NULL) {
      va_list ap;
      va_start (ap, fmt);
      vfprintf (mMapfile, fmt, ap);
      va_end (ap);
    }
  }

  // A comment line of the form /* %%SpeedCC%% has been found so write a map entry
  void mapSysIncludes (const char *p)
  {
    if (mMapfile != NULL) {
      // skip leading whitespace
      while (isspace (*p)) {
        p++;
      }
      // skip the %%SpeedCC%%
      if (strncmp (p, "/* %%SpeedCC%%", 14) != 0) {
        return;                           // bogus
      }
      p += 15;
      // skip embedded whitespace
      while (isspace (*p)) {
        p++;
      }
      // now seek the closing comment or newline character
      int last = 0;
      while (1) {
        if (p [last] == '\0') {
          // end of the string
          last--;
          break;
        } else if (p [last] == '*' && p [last+1] == '/') {
          // close of the comment
          last--;
          break;
        } else if (p [last] == '\n') {
          // end of the line
          last--;
          break;
        } else {
          last++;
        }
      }
      while (last > 0 && p [last] == ' ') {
        last--;
      }
      map ("sys include %.*s\n", last+1, p);
    }
  }

  const char* getCarbonHome() const {
    return mCarbonHome.c_str();
  }

  void reset ()
  {
    closeOutput ();
    UtString reason;
    mDefines.clear ();
    StringSet::iterator it;
    for (it = mPersistentDefines.begin (); it != mPersistentDefines.end (); it++) {
      define (it->c_str (), false);
    }
    // delete the old output file
    UtString errmsg;
    if (mOutfileName.size () == 0) {
      // The outfile name is not yet determined
    } else {
      OSUnlink (mOutfileName.c_str (), &errmsg);
      errmsg.clear ();
      // open a new one
      if ((mOutfile = OSFOpen (mOutfileName.c_str (), "w", &reason)) == NULL) {
        fprintf (stderr, "%s\n", reason.c_str ());
        exit (1);
      }
      openMapFile (mOutfileName.c_str ());
    }
  }

  void closeOutput ()
  {
    if (mOutfile != NULL) {
      flushOutput ();
      fclose (mOutfile);
      mOutfile = NULL;
    }
  }

  void openMapFile (const char *outfileName)
  {
    UtString mapFileName;
    mapFileName << outfileName << ".map";
    UtString errmsg;
    OSUnlink (mapFileName.c_str (), &errmsg);
    errmsg.clear ();
    if (!mDiag.isVerbose ()) {
      // Do not output any map file.
    } else if ((mMapfile = OSFOpen (mapFileName.c_str (), "w", &errmsg)) == NULL) {
      // Something broke tying to open the map file
      fprintf (stderr, "Unable to open SpeedCC map file: %s: %s\n", mapFileName.c_str (), 
        errmsg.c_str ());
      exit (1);                         // run away
    } else {
      // the map file was opened successfully
    }
    map ("writing map to %s\n", mapFileName.c_str ());
  }

  void putOutfile(FILE* outfile, const char *outfileName)
  {

    // In verbose mode the sources of all the include files are logged to a map
    // file.
    map ("writing to %s\n", outfileName);
    if (mOutfile != NULL)
    {
      fprintf(stderr, "Only one c++ file per call\n");
      exit(1);
    }
    openMapFile (outfileName);
    mOutfile = outfile;
    mOutfileName = outfileName;
  }

  void define(const char* str, bool persistent)
  {
    if (mMapfile != NULL) {
      map ("defining %s\n", str);
    }
    mDefines.insert(str);
    if (persistent) {
      mPersistentDefines.insert (str);
    }
  }
  
  struct IncludeFile
  {
    IncludeFile(const char* filename, IncludeFile* next,
      ZistreamZip* zipArchive, PreProcessor &pp)
      : mFile(0), mFilename(filename), mNext(next)
    {
      mZipArchive = zipArchive;
      mLineNumber = 0;
      mZipEntry = NULL;

      // Check to see if it's in the archive, mainly by trying to
      // open it and see if it's there.
      mZipEntry = zipArchive->getEntry(filename);
      if ((mZipEntry == NULL) && zipArchive->fail())
      {
        fprintf(stderr, "%s\n", zipArchive->getFileError());
        exit(1);
      }

      if (mZipEntry == NULL) {
        pp.map ("file include %s\n", filename);
        ZISTREAM_ALLOC(mFile, filename);
      } else {
        pp.map ("archive include %s\n", filename);
        activate();
      }
    } // IncludeFile

    bool isOpen() {return *mFile != NULL;}

    ~IncludeFile() {
      if (mZipEntry != NULL)
        mZipArchive->finishEntry(&mZipEntry);
      else
      {
        if (mFile && *mFile && !mFile->close())
        {
          perror(mFile->getError());
          exit(1);
        }
        delete mFile;
      }
    }

    void activate()
    {
      if (mZipEntry != NULL)
      {
        mZipArchive->switchEntry(mZipEntry);
        if (mZipArchive->fail())
        {
          fprintf(stderr, "%s\n", mZipArchive->getFileError());
          exit(1);
        }
        mFile = mZipEntry->castZistream(); 
      }
    }

    bool fgetline(UtString* buf)
    {
      return mFile->readline(buf) != 0;
    }

    ZistreamZip* mZipArchive;
    ZistreamEntry* mZipEntry;
    Zistream* mFile;
    UtString mFilename;
    UtString mCmd;
    IncludeFile* mNext;
    UInt32 mLineNumber;
  };

  bool getLine(UtString* buf)
  {
    while (mInfile != NULL)
    {
      if (mInfile->fgetline(buf))
      {
        ++mInfile->mLineNumber;
        return true;
      }
      IncludeFile* popFile = mInfile->mNext;
      delete mInfile;
      mInfile = popFile;
      if (mInfile != NULL)
        mInfile->activate();
    }
    return false;
  }
      

  
  bool includeFile(const char* infilename, bool using_pch)
  {
    if (using_pch && gPchBasename && !strcmp (infilename, gPchBasename)) {
      gIncludedPchBaseFile = true;
      return true;
    }
    UtString filebuf;
    IncludeFile* newF = new IncludeFile(infilename, mInfile, mZipArchive, *this);
    if (mDiag.isVerbose ()) {
      UtString tmp;
      tmp << " /*+++ #include " << infilename << " - expanded by SpeedCC */\n";
      writeOutput (tmp.c_str ());
    }
    if (newF->isOpen())
    {
      mInfile = newF;
      mDiag.Verbose ("include file %s\n", infilename);
      return true;
    }
    mDiag.Verbose ("passing on include file %s\n", infilename);
    delete newF;
    return false;
  } // bool includeFile

  typedef UtSet<UtString> StringSet;

  void pushEnable(bool enable)
  {
    if (!mDiag.isVerbose ()) {
      // don't annotate
    } else if (enable) {
      writeOutput (" /*+++ pushEnable (true) */ ");
    } else {
      writeOutput (" /*+++ pushEnable (false) */ ");
    }
    size_t sz = mEnable.size();
    mEnable.resize(sz + 1);
    mEnable[sz] = !enable;      // zero is true so we can use none()
  }

  bool popEnable()
  {
    if (mDiag.isVerbose ()) {
      writeOutput (" /*+++ popEnable */ ");
    }
    size_t sz = mEnable.size();
    if (sz <= 1) {
      UtString msg;
      msg << mInfile->mFilename << ":" << mInfile->mLineNumber
          << ": #endif without #if";
      fprintf(stderr, "%s\n", msg.c_str());
      return false;
    }
    mEnable.resize(sz - 1);
    return true;
  }

  bool flipEnable()
  {
    if (mDiag.isVerbose ()) {
      writeOutput (" /*+++ flip enable */ ");
    }
    size_t sz = mEnable.size();
    if (sz <= 1) {
      UtString msg;
      msg << mInfile->mFilename << ":" << mInfile->mLineNumber
          << ": #else without #if";
      fprintf(stderr, "%s\n", msg.c_str());
      return false;
    }
    mEnable[sz - 1] = !mEnable[sz - 1];
    return true;
  }

  bool isEnabled()
  {
    return mEnable.none();
  }

  // Expand the 'user' includes, i.e. #include "file" but not #include <file>.
  bool expandFile(const char* infilename, bool using_pch)
  {
    UtString *lastPrintedFilenamePtr = NULL;
    
    mDiag.Verbose ("Expanding %s\n", infilename);
    mDiag.pushTimer ();
    if (!includeFile(infilename, using_pch))
    {
      UtString msg;
      msg << "Internal Error: Internal " << sIncludeDecrypt << " file could not be found";
      fprintf(stderr, "%s: %s\n", msg.c_str(), infilename);
      mDiag.popTimer ("expand");
      return false;
    }

    bool status = true;

    UtString line, nextLine;
    bool inCComment = false;
    bool inSQuote = false;
    bool inDQuote = false;
    bool backslash = false;
    while (getLine(&nextLine))
    {
      int nextLen = nextLine.size();
      if ((nextLen > 1)
          && (nextLine[nextLen - 2] == '\\')
          && (nextLine[nextLen - 1] == '\n'))
      {
        line.append(nextLine.c_str(), nextLen - 2); // continuation line
        line << ' ';
      }
      else
      {
        line << nextLine;
        const char* lineStart = line.c_str();
        const char* p = lineStart;
        size_t lineLen = line.size();
        bool passThru = true;

        if ((*p == '#') && !inCComment && !inSQuote && !inDQuote && !backslash)
        {
          p = StringUtil::skip(p + 1);
          StrToken tokenizer(p);
          const char* tok = *tokenizer;

          if (strcmp(tok, sEndifDecrypt) == 0)
          {
            status &= popEnable();
          }
          else if (strcmp(tok, sIfDecrypt) == 0)
          {
            // Probably we need to keep track of values.  For now assume true.
            pushEnable(true);
          }
          else if ((strcmp(tok, sIfdefDecrypt) == 0) || (strcmp(tok, sIfndefDecrypt) == 0))
          {
            bool ifndef = strcmp(tok, sIfndefDecrypt) == 0;
            ++tokenizer;
            if (!tokenizer.atEnd())
            {
              UtString tokStr(*tokenizer);
              StringSet::iterator p = mDefines.find(tokStr);
              bool isDefined = (p != mDefines.end());
              pushEnable(isDefined ^ ifndef);
            }
          }
          else if (strcmp(tok, sElseDecrypt) == 0)
            status &= flipEnable();
          else if (isEnabled())
          {
            if (strcmp(tok, sDefineDecrypt) == 0)
            {
              ++tokenizer;
              if (!tokenizer.atEnd())
                define(*tokenizer, false);
            }
            else if (strcmp(tok, sUndefDecrypt) == 0)
            {
              ++tokenizer;
              if (!tokenizer.atEnd())
              {
                UtString tokStr(*tokenizer);
                StringSet::iterator p = mDefines.find(tokStr);
                if (p != mDefines.end())
                  mDefines.erase(p);
              }
            }
            else if (strcmp(tok, sIncludeDecrypt) == 0)
            {
              ++tokenizer;
              if (!tokenizer.atEnd())
              {
                const char* filename = *tokenizer;
                if ((*filename == '"') /*|| (*filename == '<')*/)
                {
                  UtString filebuf;
                  filebuf.append(filename + 1, strlen(filename) - 2);

/*
                  if (*filename == '<') {
                    // convert the /foo/bar/file.h to system/foo_bar_file.h
                    UtString sysFile;
                    sConvertSysIncludeName(filebuf.c_str(), &sysFile);
                    filebuf = sysFile;
                  }
*/
                  if (includeFile(filebuf.c_str(), using_pch))
                    passThru = false; // leave "codegen/carbon_priv.h" alone
                }
              }            
            }
          }
/*
          else
            passThru = false;
*/
        } // if
        else
        {
          // walk through the line to detect quotes and comments.  We
          // need to know if we are in a comment because we must
          // ignore # directives that are commented out.  We need to
          // keep track of quoting because a comment inside a double-quote
          // is not a comment!
          for (; *p != '\0'; ++p)
          {
            if (backslash)
              backslash = false;
            else if (!inSQuote && !inDQuote && !inCComment)
            {
              if (*p == '\'')
                inSQuote = true;
              else if (*p == '"')
                inDQuote = true;
              else if (strncmp(p, "//", 2) == 0) {
                if (mLineLimit != 0) {
                  // If we are concatenating lines to help with
                  // g++'s 2^19 line limitation, then we better
                  // remove the // comments
                  lineLen = p - lineStart;
                  INFO_ASSERT(lineLen < line.size(), "line too long");
                }
                break;            // skip the whole line
              }
              else if (strncmp(p, "/*", 2) == 0)
              {
                if (strncmp(p, "/* %%SpeedCC%%", 14) == 0)
                {
                  // This comment induces an entry into the map file
                  mapSysIncludes (p);
                }
                inCComment = true;
                ++p;              // past *
              }

            }
            else if (inCComment)
            {
              if (strncmp(p, "*/", 2) == 0)
              {
                inCComment = false;
                ++p;
              }
            }
            else if ((inSQuote || inDQuote) && (*p == '\\'))
              backslash = true;
            else if (inSQuote)
            {
              if (*p == '\'')
                inSQuote = false;
            }
            else if (inDQuote)
            {
              if (*p == '"')
                inDQuote = false;
            }
          } // for
        } // if
        if ( (mLineLimit == 0) && (&(mInfile->mFilename) != lastPrintedFilenamePtr )) {
          // only add the #line compiler directives if lineLimit is 0, (otherwise leave these extra lines out to reduce the file length).
          // when we get here the filename has changed, so inject a #line compiler directive
          // so that gcc will point back to the correct source file, and -annotateCode will
          // work without requiring that the tmp.* files be saved
          UtString tempBuf("#line ");
          // in next line do not allow line number 0, force to 1
          tempBuf << ((mInfile->mLineNumber) ? (mInfile->mLineNumber) : 1) << " \"" << mInfile->mFilename << '"' << "\n";
          writeOutput(tempBuf.c_str(), tempBuf.length());
          lastPrintedFilenamePtr = &(mInfile->mFilename);
        }
        if (passThru)
          writeOutput(line.c_str(), lineLen);
        line.clear();
      } // else
    } // while
    mDiag.popTimer ("expand");
    return status;
  } // bool expandFile

  void flushOutput() {
    if (! mOutputBuffer.empty()) {
      mOutputBuffer << "\n";
      fputs(mOutputBuffer.c_str(), mOutfile);
      mOutputBuffer.clear();
    }
  }

  void writeOutput (const char *str)
  {
    writeOutput (str, strlen (str));
  }

  // Generate output with all the newlines removed, in hopes of
  // avoiding gcc's 2^19 line limit.
  void writeOutput(const char* str, size_t len) {
    if (len != 0) {
      if (*str == '#') {
        // lines beginning with '#' must stay that way
        flushOutput();
        fputs(str, mOutfile);
      }
      else {
        if (mOutputBuffer.size() > mLineLimit)
          flushOutput();
        if (str[len - 1] == '\n') {
          mOutputBuffer.append(str, len - 1);
          mOutputBuffer << " ";
        }
        else
          mOutputBuffer.append(str, len);
      }
    }
  }

  void putLineLimit(UInt32 ll) {mLineLimit = ll;}

  StringSet mDefines;
  StringSet mPersistentDefines;
  UtString mCarbonHome;
  UtString mIncludeLib;
  UtString mIncludeDir;
  FILE* mOutfile;
  UtString mOutfileName;
  IncludeFile* mInfile;
  FILE* mMapfile;
  DynBitVector mEnable;
  ZistreamZip* mZipArchive;
  UtString mOutputBuffer;
  UInt32 mLineLimit;
  Diagnostics &mDiag;
};

//extern char** environ;

struct LicCB : public virtual UtLicense::MsgCB
{
  LicCB() {}
  virtual ~LicCB() {}
  virtual void waitingForLicense(const char*) {}
  virtual void queuedLicenseObtained(const char*) {}
  virtual void requeueLicense(const char*) {}
  virtual void relinquishLicense(const char*) {}

  virtual void exitNow(const char* reason)
  {
    fprintf(stderr, "%s\n", reason);
    exit(1);
  }
 
};

static inline UtLicense* sGetBaseLicense(LicCB* licCB, const UtString& carbonHome)
{
  UtLicense* license = new UtLicense(licCB);
  license->putInstallDir(&carbonHome);

  return license;
}
  
static inline void sGetDiagsLicense(UtLicense* license, bool* hasDiags, const char* internalOption)
{
  if (! *hasDiags)
  {
    UtString reason;
    if (!license->checkout(UtLicense::eDIAGNOSTICS, &reason))
    {
      fprintf(stderr, "%s\n", reason.c_str());
      if (internalOption)
      fprintf(stderr, "%s %s.\n", internalOption, sRequiresCdsInternalDecrypt);
#ifndef CARBON_IGNORE_LIC
      exit(1);
#endif
    }
    *hasDiags = true;
  }
}

static bool sValidateCompiler(const char* gcc)
{
#if pfWINDOWS
  // We don't support running a backend compiler on Windows, so
  // disable the code related to that.  We need speedcc just to
  // generate .ia files.
  (void) gcc;
  return true;
#else
  bool isValid = false;
  UtMD5 md5;
  unsigned char digest[16];
  UtString errMsg;
  if (! md5.sumFile(gcc, digest, &errMsg))
    fprintf(stderr, "%s\n", errMsg.c_str());
  else
  {
    // keep the most used compilers at the front of the array to
    // lessen any performance impact of checking the compiler
    // legitimacy linearly.
    const unsigned char* compHash[] = {
      // This is what we are releasing
      sHashGcc422_Binutils2_20_1,
      sHashGcc422_64_Binutils2_20_1,
      sHashGcc422,
      sHashGcc422_64,
      sHashGcc343,
      sHashGcc345,
      sHashGcc343_64,
      sHashGcc345_64,
      sHashGcc343mingw,
      sHashGcc345mingw,
      sHashGcc346,
      sHashES5Gcc424,
      sHashES5Gcc432,
      // Experimental
      sHashGcc410,
      sHashGcc410_64,
      sHashGcc410mingw,
      // Used by checkin
      sHashIcc90,
      // obsolete
      sHashIcc80,
      sHashGcc2953b
    };
    for (unsigned int j = 0;
	 (j < (sizeof compHash / sizeof(compHash[0]))) && ! isValid;
	 ++j)
    {
      const unsigned char* curHash = compHash[j];
      bool matches = true;
      for (int i = 0; (i < 16) && matches; ++i)
        matches = (digest[i] == (~curHash[i] & 0xff));
      if (matches)
        isValid = true;
    }
  }

  return isValid;
#endif // pfMSVC
}

#if (!pfWINDOWS)
//! \function runGcc
/*! Spawns off the process denoted by gccArgs, optionally adding -include foo.h.
 *  \param gccArgs The argument vector for the subprocess.
 *  \param outlog Output from the subprocess.
 *  \param pch_filename If non-null, then -include pch_filename is appended to the args.
 *  \param pch_failed Set if PCH usage failed.
 *
 *  \note gccArgs must be passed by value not reference because if a pch file
 *  is specified then the function appends to the argument list.
 */
static UInt32 runGcc (UtStringArgv gccArgs, UtString *outlog, const char *pch_filename,
  Diagnostics &diag)
{
  diag.pushTimer ();
  diag.Verbose ("Running GCC\n");
  bool retval = 0;
  outlog->clear ();
  if (pch_filename != NULL) {
    diag.Verbose ("Using %s for compilation\n", pch_filename);
    gccArgs.push_back ("-include");
    gccArgs.push_back (pch_filename);
  }
  diag.Verbose (gccArgs);
  backtick_subprocess* pc = new backtick_subprocess;
  // GCC4 looks at the LANG environment variable. If the value allows UTF-8,
  // then it will use 8-bit characters to highlight parts of error
  // messages. This confuses the code that pulls warnings messages out of the
  // GCC output. So, set LANG to en_US to get just plain old printable 7-bit
  // ASCII.
  pc->add_variable ("LANG", "en_US");
  pc->setVerbose (true);
  char** argv = gccArgs.getArgv();
  arg_vector av(argv);
  if (!pc->spawn(*argv, av)) {
    *outlog = pc->error_text();
    diag.Verbose ("Subprocess spawn failed: %s\n", outlog->c_str ());
    retval = 1;
  } else {
    diag.Verbose ("Subprocess terminated with status %d\n", pc->error_number ());
    retval = pc->error_number();
  }
  // Collect the output from the string-vector saved in the subprocess
  UtStringArray v = pc->text();
  if (v.empty ()) {
    diag.Verbose ("- no output from gcc\n");
  } else {
    for (UInt32 i = 0; i < v.size(); ++i) {
      diag.Verbose ("- %s", v [i]);
      *outlog << v [i];
    }
  }
  delete pc;
  diag.popTimer ("gcc");
  return retval;
}

static bool compileWithPch (Diagnostics &diag, UtStringArgv &gccArgs, 
  char *pch_filename, UtString *outlog)
{
  if (runGcc (gccArgs, outlog, pch_filename, diag) != 0) {
    diag.Verbose ("PCH compilation failed.\n");
    return false;
  } else if (diag.isTestingPchFallback ()) {
    // Even though compilation with precompiled headers was successfull, return
    // failure so that we can test the fallback non-PCH compilation.
    diag.Verbose ("Testing PCH compilation failure fallback.\n");
    *outlog << "Faking an error\n";
    return false;
  } else {
    diag.Verbose ("PCH compilation was successful.\n");
    return true;
  }
}

static bool compileWithoutPch (Diagnostics &diag, UtStringArgv &gccArgs, UtString *outlog)
{
  if (runGcc (gccArgs, outlog, NULL, diag) != 0) {
    diag.Verbose ("Non-PCH compilation failed.\n");
    return false;
  } else {
    diag.Verbose ("Non-PCH compilation was successful.\n");
    return true;
  }
}
#endif   // !pfWINDOWS

static bool compileFile (Diagnostics &diag, PreProcessor &pp, UtStringArgv &gccArgs, 
  const char *filename, bool noExpand, char *pch_filename, UtString *outlog,
  bool *is_pch_compilation)
{
#if pfWINDOWS
  (void) diag;
  (void) pp;
  (void) gccArgs;
  (void) filename;
  (void) noExpand;
  (void) pch_filename;
  (void) outlog;
  (void) is_pch_compilation;
  return false;
#else
  // Expand the source file
  gIncludedPchBaseFile = false;
  if (noExpand) {
    // no need to construct the tmpfile
  } else if (filename == NULL) {
    // no filename specified
    fprintf (stderr, "No file specified\n");
    return false;
  } else if (!pp.expandFile (filename, pch_filename != NULL)) {
    // expansion failed
    return false;
  } else {
    // the expansion terminated successfully so flush and close the output file
    // prior to invoking the C++ compiler
    pp.closeOutput ();
  }
  *is_pch_compilation = pch_filename != NULL && gIncludedPchBaseFile;
  if (*is_pch_compilation) {
    return compileWithPch (diag, gccArgs, pch_filename, outlog);
  } else {
    return compileWithoutPch (diag, gccArgs, outlog);
  }
#endif
}

int main(int argc, char** argv)
{
  Diagnostics diag;

  sDecryptStrings();

  UtString carbonHome;
  {
    char* carbonHomeEnv = getenv("CARBON_HOME");
    if (carbonHomeEnv == NULL)
    {
      fprintf(stderr, "$CARBON_HOME not set\n");
      return 1;
    }
    carbonHome.assign(carbonHomeEnv);
  }
  

  int ret = 0;
  bool deleteFiles = getenv(sCarbonNoDeleteDecrypt) == NULL;
  LicCB dummyCB;
  UtLicense* license = sGetBaseLicense(&dummyCB, carbonHome);
  bool hasDiags = false;
  bool noExpand = getenv(sCarbonNoTmpFileDecrypt) != NULL;
  const char* prodOrDebug = NULL;
  bool promote_errors_to_warnings = false;
  
  char *pch_filename = NULL;

  if (!deleteFiles)
  {
    UtString internalMod(sCarbonNoDeleteDecrypt);
    sGetDiagsLicense(license, &hasDiags, internalMod.c_str());
  }

  if (noExpand)
  {
    UtString internalMod(sCarbonNoTmpFileDecrypt);
    sGetDiagsLicense(license, &hasDiags, internalMod.c_str());
    if (hasDiags)
    {
      prodOrDebug = getenv("CARBON_BIN");
      INFO_ASSERT(prodOrDebug, "CARBON_BIN not set");
    }
  }
  
  if (argc >= 4)
  {
    if (strcmp(argv[1], sBuildDbDecrypt) == 0)
    {
      diag.Verbose ("Constructing database\n");
      if (argc != 5) {
        return 1;                 // no error message for now
      }
      // form a map file by appending ".map" to the lib filename
      UtString mapFile;
      mapFile << argv [3] << ".map";
      ret = sBuildDB(argv[2], argv[3], argv[4], mapFile.c_str ());
    }
    else if (strcmp(argv[1], sStripDecrypt) == 0)
    {
      // Undocumented usage:
      //   speedcc -strip prefix infile outfile pattern1 pattern2 pattern3....
      //
      //   the prefix specifies a common mangling prefix so that
      //   this strip will mangle differently from other strips.
      //
      // The patterns can be shell style wildcards with * and ?,
      // of course you must be carefull in the shell to quote them
      // so SpeedCC sees them.
      diag.Verbose ("Stripping\n");
      if (argc < 5)
        return 0;                 // no error/usage message for now
      ret = sStrip(argv[2], argv[3], argv[4], &argv[4]);
    }
    else
    {
      // The first argument is the zip filename.  Get its index.
      const char* zipFile = argv[1];
      UtVector<UtString> deleteList;
      const char* infile = NULL;
      
      const char* gcc = argv[2];
      // Validate that we are really talking to a gcc binary
      if (! sValidateCompiler(gcc))
      {
        // print a warning
        fprintf(stderr, "Unrecognized compiler: %s\n", gcc);
        // only allowed if we have a diagnostic license
        sGetDiagsLicense(license, &hasDiags, NULL);
      }
      diag.Verbose ("Compiler is %s\n", gcc);

      // construct an argv for g++
      UtStringArgv gccArgs;
      gccArgs.push_back(gcc);
      PreProcessor pp(zipFile, carbonHome.c_str(), diag);
      FILE* outFile = NULL;

      // scan args, find C++ file.  expand it into named pipe.
      bool needPCH = false;     // Remember if we're looking for -pch
                                // filename
      bool needPCHBase = false;
      UtString internalOptionBuf; // used for messaging
      for (int i = 3; i < argc; ++i)
      {
        UtString path, file, pipeName;
        char* arg = argv[i];

        if (strcmp (arg, "-pch") == 0)
        {
          needPCH = true;
          continue;
        }
        else if (needPCH)
        {
          pch_filename = arg;
          needPCH = false;
          diag.Verbose ("Precompiled headers are %s\n", pch_filename);
          continue;
        }

        if (strcmp (arg, "-pchBase") == 0) {
          needPCHBase = true;
          continue;
        } else if (needPCHBase) {
          gPchBasename = arg;
          needPCHBase = false;
          diag.Verbose ("Precompiled header base is %s\n", gPchBasename);
          continue;
        }
        OSParseFileName(arg, &path, &file);
        size_t file_size = file.size();
        if ((file_size > 4) &&
            ((strncmp(&file[file_size - 4], ".cxx", 4) == 0) ||
             (strncmp (&file[file_size - 2],  ".h", 2) == 0) ||
             (strncmp(&file[file_size - 4], ".cpp", 4) == 0)))
        {
          diag.Verbose ("Source file is %s\n", file.c_str ());
          if (! noExpand)
          {
            pipeName << "tmp." << file;
            //pipeName << "/dev/stdin";
            int status = 0;//mknod(pipeName.c_str(), S_IFIFO | 0600, 0);

            UtString reason;
            if ((status != 0) ||
                ((outFile = OSFOpen(pipeName.c_str(), "w", &reason)) == NULL))
            {
              fprintf(stderr, "%s\n", reason.c_str());
              return 1;
            }
            pp.putOutfile(outFile, pipeName.c_str ());
            infile = arg;       // don't process yet, wait till we see -g
            deleteList.push_back(pipeName);
            gccArgs.push_back(pipeName.c_str());
          }
          else
          {
            gccArgs.push_back(arg);
            file.assign(arg);

            // Add -I$CARBON_HOME/src/inc
            UtString argTmp;
            argTmp << sDashIDecrypt << pp.getCarbonHome() << sSrcIncDecrypt;
            gccArgs.push_back(argTmp);

            // Add -I$CARBON_HOME/ARCH.CARBON_BIN
            argTmp.clear();
            argTmp << sDashIDecrypt << pp.getCarbonHome() << sSlashObjSlashDecrypt << THISARCH << "." << prodOrDebug;
            gccArgs.push_back(argTmp);
          }
          
          // We've moved the C++ file to somewhere other than the
          // current directory, so any includes in the current
          // directory will not be found unless we add option -I.
          gccArgs.push_back(sDashIDotDecrypt);

          // See if the user has explicitly given a -o arg.  If not,
          // we must create one because we are changing the C++ filename.
          bool hasDashO = false;
          bool needDashO = true;
          for (int j = 3; j < argc; ++j)
          {
            if (strcmp(argv[j], "-o") == 0)
              hasDashO = true;
            else if ((strcmp(argv[j], sDashSDecrypt) == 0) ||
                     (strcmp(argv[j], sDashEDecrypt) == 0))
              needDashO = false; // creates a non-object-file output.
          }
            
          if (!hasDashO && needDashO)
          {
            // compose object filename (since default is wrong now)
            UtString ofile;
            file.resize(file_size - 3); // strip cxx/cpp
            file << "o";
            gccArgs.push_back("-o");
            gccArgs.push_back(file);
          }
        } // if
        else
        {
          if (*arg == '-')
          {
            switch (arg[1])
            {
            case 'D': {
              char* def = arg + 2;
              if (*def == '\0') // user specified "-D foo"
                def = argv[i + 1];
              char* eq = strchr(def, '=');
              if (eq != NULL)
                *eq = '\0';
              pp.define(def, true);
              if (eq != NULL)
                *eq = '=';
            }
              break;
            case 'E':
            case 'S':
            case 'g':
              internalOptionBuf.clear();
              internalOptionBuf << sCompilerOptionDashDecrypt << arg[1];
              sGetDiagsLicense(license, &hasDiags, internalOptionBuf.c_str());
              deleteFiles = false;
              pp.putLineLimit(0);   // respect newlines
            case 'W':
              if (strcmp(arg, "-Werror") == 0) {
                promote_errors_to_warnings = true;
                arg = NULL;     // don't let GCC see the -Werror
              }
              else if (strcmp(arg + 1, "-Wno-error") == 0) {
                promote_errors_to_warnings = false;
                arg = NULL;
              }
            } // switch
          } // else if
          if (arg != NULL) {
            gccArgs.push_back(arg);
          }
        }
      } // for

      // release the license now so we don't have the heartbeats
      // or signals or whatever interrupt our 'fork' system call
      delete license;
      diag.Verbose ("Released license\n");

      // First attempt compilation using the precompiled header for the Carbon model.
      UtString outlog;
      bool is_pch_compilation = true;
      bool compiled_successfully = false;
      if (pch_filename == NULL) {
        // No PCH file was provided on the command line so do not try
        // compilation with precompiled headers
      } else if (compileFile (diag, pp, gccArgs, infile, noExpand, pch_filename, &outlog,
          &is_pch_compilation)) {
        // The first compilation attempt was successfull
        UtIO::cout () << "Info: " << infile << ": compiled.\n";
        compiled_successfully = true;
      } else {
        // The first compilation attempt failed.
        UtIO::cout () << "Info: " << infile << ": compilation with precompiled headers failed.\n";
        pp.reset ();
      }
      // If compilation with precompiled headers failed, then try again without
      // precompiled headers
      if (compiled_successfully) {
        // We are done
      } else if (pch_filename != NULL && !is_pch_compilation) {
        // The first compilation attempt failed. However, expansion did not
        // encounter the PCH base file so don't bother with the second
        // compilation attempt. It would fail in just the same way.
        ret = 1;                        // compilation failed
      } else if (!compileFile (diag, pp, gccArgs, infile, noExpand, NULL, &outlog, 
          &is_pch_compilation)) {
        diag.Verbose ("Compilation with no PCH failed.\n");
        ret = 1;                        // compilation failed
      } else if (pch_filename != NULL) {
        // A precompiled header was provided but PCH compilation failed and
        // non-PCH compilation was successful. Write a warning both to stderr
        // and stdout. stderr ends up in libdesign.codegen.errors; stdout in
        // the make.log.
        diag.Verbose ("Compilation successfull\n");
        UtIO::cerr () << "SpeedCC: Warning: " << infile << " was compiled without using precompiled headers.\n";
        UtIO::cout () << "Warning: " << infile << " was compiled without using precompiled headers.\n";
      }

      // Walk through any warnings emitted by the compilation,
      // setting the error status if we find any that we are
      // certain are not benign
      UtIStringStream is(outlog.c_str());
      UtString line;

      // Most warnings will be treated with errors by default.  We will
      // only exclude certain warnings that really only suggest optimizations
      // to Carbon, and are not useful for customers.  When we run our
      // regression tests we will error on any warning.
      const char *value = getenv("CARBON_GCC_ERROR_ON_WARNING");
      bool error_on_any_warning = (value != NULL) && (strcmp (value, "0") != 0);

      // We want to allow these warnings for users, but error out on them
      // during regression tests, so that we are pointed at interesting
      // optimizations to add to our code.  Other warnings should be
      // treated as errors even for users.
      static const char* acceptable_warning_patterns[] = {
        "*: warning: comparison is always *",
        "*: warning: * shift count >= width of type*",
        "*: warning: `*' of * is always*",
        "*: warning: '*' of * is always*",
        NULL
      };

      bool promoted = false;
      if ((ret == 0) && promote_errors_to_warnings) {
        UtString new_outbuf;
        while (is.getline(&line)) {
          diag.Verbose ("Scanning %s\n", line.c_str ());
          bool ok = true;       // assume the line is a note 

          // First see if this is a warning
          size_t warning_pos = line.find(": warning: ");
          if (warning_pos != UtString::npos) {
            ok = false;         // now assume it's a promoted warning

            if (error_on_any_warning) {
              // In regression tests, even warnings matching the pattern above
              // result in test failures
              promoted = true;
            }
            else {
              // For users, we see no value in making their compiles fail
              // due to optimization suggestions gcc can give us.
              for (const char** p = acceptable_warning_patterns;
                   !ok && (*p != NULL); ++p)
              {
                UtWildcard wildcard(*p);
                ok = wildcard.isMatch(line.c_str()); // this warning is ok
              }
              if (!ok) {
                promoted = true;
              }
            }
          } // if
          if (ok) {
            new_outbuf << line;
          }
          else {
            // change the "warning" label to "promoted error"
            warning_pos += 2; // include ": "
            new_outbuf.append(line, 0, warning_pos);
            new_outbuf << "PROMOTED ";
            new_outbuf.append(line.c_str() + warning_pos);
          }
        }
        outlog = new_outbuf;
        if (promoted) {
          outlog << "\nPlease report this problem to Carbon Design Systems\n";
          outlog << "Please use -Wc -Wno-error as a workaround\n";
          ret = 1;
        }
      } // if

      // Put the entire spew in stdout if successful, stderr if failed
      if (ret == 0) {
        diag.Verbose ("SpeedCC succeeded\n");
        UtIO::cout() << outlog;
      }
      else {
        diag.Verbose ("SpeedCC failed:\n");
        UtIO::cerr() << outlog;
      }
      
      if (deleteFiles) {
        UtString delErr;
        for (size_t i = 0; i < deleteList.size(); ++i)
        {
          const char* file = deleteList[i].c_str();
          delErr.clear();
          diag.Verbose ("deleting %s\n", file);
          if (OSUnlink(file, &delErr))
          {
            fprintf(stderr, "%s\n", delErr.c_str());
            ++ret;
          }
        }
      }
    }
  }
  return ret;
} // int main

