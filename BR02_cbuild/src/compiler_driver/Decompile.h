// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/****************************************************************
***************
*****  FILE  : Decompiler.h
*****  AUTHOR :punit
*****  DATE   :16/07/96
*****  RCS  HISTORY :

*****  BRIEF DESC : Header file for behavioral decompiler routines

*****************************************************************
***************/

// C++-ized & Carbonized by Josh

#ifndef __DECOMPILE_H__
#define __DECOMPILE_H__

#include "util/CarbonTypes.h"
#include "util/UtString.h"
#include "util/Util.h"
#include "util/UtVector.h"
#include "compiler_driver/Interra.h"

class ZostreamDB;
class ZistreamDB;
class AtomicCache;
class IODBNucleus;
class MsgContext;
class CheetahStr;
class CheetahContext;
class ProtectedSource;

//! Decompile class -- decompiles Cheetah representation into verilog text
/*! This is based on source code supplied by Interra, C++-ized and
 *! carbonized by Josh.  The changes:
 *!
 *!  1. Make all functions be methods of class Decompile
 *!  2. Move global variable FILE* outputFile into member variable
 *!     mOutputFile
 *!  3. Change calls to decompPrintf(outputFile, ....) be
 *!     calls to new method writeOutput, which accesses mOutputFile,
 *!  4. Make mOutputFile be a Zostream*
 *!  5. Change all void* to veNode
 *!  6. Fix all the gcc warnings about "suggested parens" on statements
 *!     like "if (foo = bar)" by changing them to "if ((foo = bar))"
 *!  7. Re-indent every line in every file to conform to Carbon standards
 */
class Decompile
{
public:
  Decompile(ZostreamDB* outputFile, AtomicCache* cache, veNode mRoot,
            IODBNucleus* iodb, MsgContext*, CheetahContext*);
  Decompile(UtOStream* outputFile, AtomicCache* cache, veNode mRoot,
            IODBNucleus* iodb, MsgContext*, CheetahContext*);
  Decompile(ZistreamDB* inputFile, AtomicCache* cache, IODBNucleus* iodb,
            MsgContext*, CheetahContext*);
  ~Decompile();

  // Note -- you must add new keywords to the *end* of this list, or you
  // will destroy our ability to read old .CEM files.  This is covered
  // by test/cem_compat, which makes sure that the current compiler
  // can read .cem files harvested from the langcov test area after
  // a run with -nocleanup.
  enum Keyword {
    eInitial, eNewline, eOpenParen, eCloseParen, eSemiColon,
    eAlways, eIf, eParameter, ePosedge, eNegedge, eElse,
    eForever, eRepeat, eBegin, eFor, eColon, eEnd, eEndCase,
    eWhile, eFullCase, eParallelCase, eFullParallelCase,
    eCase, eCasex, eCasez, eLiteral, eDefault, eComma, eWait,
    eDisable, eForce, eRelease, eDelay, eFork, eJoin,
    eTask, eEndTask, eFunction, eSigned, eInteger, eReal,
    eRealTime, eEndFunction, eTrigger, eEqual, eNonBlockingEqual,
    eAssign, eAtSign, eAsterisk, eEventOr, eInput, eOutput, eInout,
    eReg, eTimescale, eCellDefine, eModule, eMacroModule,
    eEndModule, eEndCellDefine, eConfig, eDesign, eSlash, eSpace,
    eDeassign, eObfuscate, eInstance, eLibList, eUse,
    eCell, eEndConfig, eGenerate, eEndGenerate, eGenVar, ePeriod,
    ePunc, eOpenBracket, eCloseBracket, eOpenBrace, eCloseBrace,
    eWire, eTime, eLocalParam, eTri, eTri1, eSupply0, eWand, eTriand,
    eTri0, eSupply1, eWor, eTrior, eTrireg, eVectored, eEvent,
    eStrong0, ePull0, eWeak0, eHighz0, ePull1, eWeak1, eHighZ1,
    eDefParam, eSpecify, eEndSpecify, eShowCancelled, eNoShowCancelled,
    eSpecParam, eAnd, eNand, eOr, eNor, eXor, eXnor, eBuf, eBufif0,
    eBufif1, eNot, eNotif0, ePulldown, ePullup, eNmos, eRnmos, ePmos,
    eRpnmos, eCmos, eRcmos, eTran, eRtran, eTranif0, eRtranif0, eTranif1,
    eRtranif1, eStrong1, eHighz1, eNotif1, eRpmos, eEOF,

    // UDP support
    ePrimitive, eTable, eEndTable, eEndPrimitive,

    // MVV 2006 support
    eUWire
  };

  void rootDecompile( veNode nodeRoot );
  void putSynthPrefix(const char* synthPrefix);
  bool protectModuleInterface(const char* moduleName);
  void protectPortList(CheetahList& ports);

  void disableObfuscation();

  // read a token from an encoded model file
  bool readToken(UtString* buf);

  UInt32 getObfuscationIndex() const {return mObfuscationIndex;}
  void putObfuscationIndex(UInt32 i) {mObfuscationIndex = i;}

  bool readObfuscationTable(const char*);

private:
  void init(ZostreamDB* outputFile, UtOStream*,
            AtomicCache* cache, veNode mRoot,
            IODBNucleus* iodb, ZistreamDB* inputFile, MsgContext*,
            CheetahContext*);


  // Behavioral decompiler methods
  void initialDecompile( veNode );
  void alwaysDecompile( veNode );
  void ifDecompile( veNode );
  void foreverDecompile( veNode );
  void repeatDecompile( veNode );
  void whileDecompile( veNode );
  void forDecompile( veNode );
  void generateForDecompile (veNode generateForNode);
  void generateBlockDecompile (veNode generateBlockNode);
  void generateIfDecompile (veNode generateIfNode);
  void generateCaseDecompile (veNode generateCaseNode);
  void generateCaseItemDecompile (veNode generateCaseItemNode);
  void caseDecompile( veNode );
  void caseItemDecompile( veNode );
  void waitDecompile( veNode );
  void alwayswaitDecompile( veNode );
  void disableDecompile( veNode );
  void forceDecompile( veNode );
  void releaseDecompile( veNode );
  void deAssignDecompile( veNode );
  void delayOrEventControlDecompile( veNode );
  void repeatDelayControlDecompile( veNode );
  void seqBlockDecompile( veNode );
  void parBlockDecompile( veNode );
  void taskDecompile( veNode );
  void functionDecompile( veNode );
  void taskEnableDecompile( veNode );
  void sysTaskEnableDecompile( veNode );
  void sysFunctionCallDecompile( veNode );
  void eventTriggerDecompile( veNode );
  void blockAssignDecompile( veNode );
  void nonBlockAssignDecompile( veNode );
  void quasiContAssignDecompile( veNode );
  void eventControlDecompile( veNode );
  void posEdgeExprDecompile( veNode );
  void negEdgeExprDecompile( veNode );
  void functionCallDecompile( veNode );
  void tfArgDecompile( veNode );
  void regDecompile( veNode );
  void stmtDecompile( veNode );
  void repeatOrDelayDecompile( veNode );
  void assignDecompile( veNode );

  void writeCaseDirective(veNode node);

  // module decompiler methods
  void moduleDecompile  (veNode module);
  void configDecompile (veNode configNode);
  void generateDecompile (veNode generateNode);
  void generateItemDecompile (veNode generateItem);
  void genvarDecompile (veNode genvarNode);
  void portDecompile(veNode portnode);
  void exprDecompile(veNode expr);
  void paramDecompile(veNode param);
  void localparamDecompile(veNode localparam);
  void unaryDecompile(veNode unary);
  void binaryDecompile(veNode binary);
  void concatDecompile(veNode concat);
  void scopevariableDecompile(veNode scopevariable);
  void mulConcatDecompile(veNode mulconcat);
  void bitSelectDecompile(veNode bitselect);
  void partSelectDecompile(veNode partselect);
  void arrayPartSelectDecompile(veNode arrayPartselect);
  void arrayBitSelectDecompile(veNode arrayBitSelect);
  void condOpDecompile(veNode condop);
  void minTypMaxExprDecompile(veNode minTypMaxExpr);
  void namedObjectUseDecompile(veNode namedObjectUse);
  void integerdecompile( veNode integer);
  void intortimearraydecompile(veNode intortimearray);
  void TimeDecompile( veNode time);
  void realdecompile( veNode real);
  void realtimedecompile( veNode real);
  void varDecompile( veNode var);
  void memoryDecompile(veNode mem);
  void netArrayDecompile(veNode netArray);
  void arrayDecompile(veNode mem);
  void eventDecompile (veNode event);
  void netDecompile( veNode net);
  void defParamDecompile(veNode defparam);
  void contAssignDecompile(veNode contassign);
  void specBlkDecompile(veNode specblk);
  void pulsestyleDecompile(veNode pulseStyle);
  void showcancelledDecompile(veNode showCancelled);
  void specParamDecompile(veNode specparam);
  void sysTimingCheckDecompile(veNode systimingcheck);
  void timingEventDecompile(veNode  timingEvent);
  void timingEventControlDecompile(veNode timingEventCtrl) ;
  void edgeDescriptorDecompile(veNode  edgeDesc);
  void rangeDecompile(veNode range);
  void constDecompile(veNode expr);
  void stringDecompile(veNode expr);
  void basedNumberDecompile(veNode expr);
  void floatNumberDecompile(veNode expr);
  void expNumberDecompile(veNode expr);
  void moduleOrUdpInstanceDecompile( veNode );
  void gateInstanceDecompile( veNode );
  void portConnectionDecompile( veNode );
  void pathDecompile (veNode path);
  void atttrInstanceDecompile(CheetahList& attrInstanceList);

  // UDP decompilation
  void udpDecompile( veNode );
  void combinationalEntryDecompile( veNode );
  void sequentialEntryDecompile( veNode );
  void levelSymbolDecompile( veNode );
  void outputSymbolDecompile( veNode );
  void edgeSymbolType1Decompile( veNode );
  void edgeSymbolType2Decompile( veNode );
  void edgeInputListDecompile( veNode );
  void levelInputListDecompile( veNode );

  // misc routines
  Decompile& operator<<(Keyword kwd);
  Decompile& operator<<(char c);
  void writeOutput(const char* str,int flag);
  void libDecompile(veNode configLibNode);
  void fileDecompile( veNode nodeFile );
  char* int_to_str(int i);
  char* double_to_str(double i);


  // obfuscation routines
  void protectName(const char* name);
  void obfuscate(const char* name);
  void readObfuscate(UtString* buf, UInt32 index);
  void writeObfuscationTable();

private:

  // stack-allocated object to change into protected mode, where we
  // wrap the token text in a `protected block.
  struct Protector {
    Protector(Decompile* dc, veNode node): mDecompile(dc) {
      if (dc->requiresProtection(node)) {
        dc->beginProtection();
        mProtected = true;
      }
      else {
        mProtected = false;
      }
    }
    ~Protector() {
      if (mProtected) {
        mDecompile->endProtection();
      }
    }
    Decompile* mDecompile;
    bool mProtected;
  };
  friend struct Protector;      // Needed for GCC 2.95

  //! Does this veNode require protection?  This can only be true when writing
  //! an otherwise cleartext tokens file.  It is never true when writing a cem
  //! file.  And it is not true when we are already in a protected block
  bool requiresProtection(veNode);
  void beginProtection();
  void endProtection();
  void flushProtected();

  //! This is inlined to discourage reverse engineering
  inline const char* decodeKeyword(Keyword kwd, UtString* buf);

  // State for writing the output file
  ZostreamDB* mEncryptedOutputFile;
  UtOStream* mClearOutputFile;
  ProtectedSource* mProtectedSource;
  SInt32 mColumn;
  UtString mSynthPrefix;
  UInt8 mCypher;

  // State for obfuscating the design
  IODBNucleus* mIODB;
  UInt32 mObfuscationIndex;
  AtomicCache* mAtomicCache;
  typedef UtHashMap<StringAtom*,UInt32> NameMap;
  typedef UtHashSet<StringAtom*> NameSet;
  NameMap* mNameMap;
  NameSet* mProtected;
  veNode mRoot;
  bool mObfuscationEnabled;

  // State for decoding an input file (+ mCypher above)
  ZistreamDB* mInputFile;
  UtVector<StringAtom*> mObfuscations;
  MsgContext* mMsgContext;

  typedef UtHashSet<veNode> NodeSet;
  NodeSet* mDecompiledNodes;

  bool mInProtectedBlock;
  UtString mProtectedBuffer;
  CheetahContext* mCheetahContext;

  typedef UtHashSet<UtString, HashValue<UtString> > StringSet;
  StringSet* mCEMFilesReferenced;
};


#endif // __DECOMPILE_H__
