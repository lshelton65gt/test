// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/Interra.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/UtHashSet.h"
#include <cctype>               // for isdigit

static bool sIsProtectVFile(const char* fname) {
  // see if this matches the pattern used for `protected unencrypted files
  if ( fname[0] == '-' ){
    return true;
  }
  // See if this matches the pattern "[PATH/]libDESIGN.protects.INTEGER.v"
  const char* protects = ::strstr(fname, ".protects.");
  if (protects != NULL) {
    int sz = strlen(protects);
    if ((strcmp(&protects[sz - 2], ".v") == 0) &&
        (isdigit(protects[sz - 3])))
    {
      return true;
    }
  }
  return false;
}

bool gCheetahGenTokenFile(const char* tokenFilename, veNode root,
                          int argc, char** argv)
{
  CheetahList files(veRootGetFileList(root));

  UtOBStream outfile(tokenFilename);
  if (!outfile.is_open()) {
    UtIO::cerr() << outfile.getErrmsg() << "\n";
    return false;
  }

  // Scan the command-line for +define+
  for (int i = 1; i < argc; ++i, ++argv) {
    char* arg = *argv;
    if (strncmp(arg, "+define+", 8) == 0) {
      outfile << "// " << arg << "\n";
    }
  }

  veNode file, define, ifdef;
  UtHashSet<UtString,HashValue<UtString> > coveredFiles;
  while ((file = veListGetNextNode(files)) != NULL) {
    CheetahStr fname(veFileGetName(file));
    if (!sIsProtectVFile(fname) &&
        (coveredFiles.find(UtString(fname)) == coveredFiles.end()))
    {
      coveredFiles.insert(UtString(fname));

      outfile << fname << "\n";

      CheetahList defines(veFileGetDefineList(file));
      while ((define = veListGetNextNode(defines)) != NULL) {
        CheetahStr defName(veDefineGetName(define));
        CheetahStr defVal(veDefineGetValue(define));
        outfile << "// `define " << defName;
        if ((defVal != NULL) && (*defVal != '\0')) {
          outfile << " " << defVal;
        }
        outfile << "\n";
      }

      CheetahList ifdefRanges(veFileGetIfdefedOutRangeList(file));
      while ((ifdef = veListGetNextNode(ifdefRanges)) != NULL) {
        int startLine = veIfdefedOutRangeGetStartLno(ifdef);
        int endLine = veIfdefedOutRangeGetEndLno(ifdef);
        outfile << "// ifdef " << startLine << " " << endLine << "\n";
      }
    } // if
  } // while

  if (!outfile.close()) {
    UtIO::cerr() << outfile.getErrmsg() << "\n";
    return false;
  }
  return true;
} // bool gCheetahGenTokenFile
