// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "fabric_config_checksum_validator_lib.h"
#include "compiler_driver/ARMContext.h"
#include "util/UtString.h"

bool ARMContext::csTest(const char* filepath, UtString* errMsg)
{
#if (pfLINUX || pfLINUX64) && !pfICC
  // currently perform_checksum fails to link with ICC
  // at one time we thought this might be due to a problem related to
  // the .o from ARM having been compiled without -fno-inline, 
  //   see gcc.gnu.org/ml/gcc-bugs/2004-02/msg00354.html
  // ARM provided a version that used -fno-inline and we still had
  // the linking problem so for now this code will not work with ICC
  
  char* errStrPtr = NULL;
  if (perform_checksum(filepath, errStrPtr) == 0){
    return true;
  } else {
    (*errMsg) << errStrPtr;
    return false;
  }
#else
  // for now all other platforms are not supported, return false with a message
  (*errMsg) << filepath << " Cannot be verified as an AMBA designer generated file.";
  return false;
#endif
}


