// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include <errno.h>
#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/CheetahContext.h"
#include "localflow/DesignPopulate.h"
#include "localflow/InLineDirectivesCB.h"
#include "localflow/TicProtectedNameManager.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUNet.h"
#include "util/AtomicCache.h"
#include "util/HdlFileCollector.h"
#include "util/OSWrapper.h"
#include "util/UtIStream.h"
#include "util/UtUniquify.h"
#include "util/Zstream.h"
#include "Decompile.h"
#include "compiler_driver/ProtectedSource.h"

// Need to keep a static context here because Cheetah doesn't let us
// pass client data thru to the message redirector.
static CarbonContext* sCarbonContext = NULL;

static const char* sVerboseCem = NULL;
static const char* sDefaultSynthPrefix = "carbon";
const char* sCheetahMemLeaks = NULL;

CheetahContext::CheetahContext(CarbonContext* cc,
                               TicProtectedNameManager* tic_protected_nm_mgr)
{
  INFO_ASSERT(sCarbonContext == NULL, "sCarbonContext is already in use");
  mTicProtectedNameMgr = tic_protected_nm_mgr;
  sCarbonContext = cc;
  mDesign = NULL;
  mCarbonContext = cc;
  mObfuscationIndex = 0;
  mRoot = 0;
  ArgProc* args = cc->getArgs();
  
  sCheetahMemLeaks = CRYPT("-cheetahMemLeaks");
  args->addBool(sCheetahMemLeaks,
                CRYPT("Turn off cheetah's memory manager to help valgrind/-dumpMemory find leaks in our usage of Cheetah"), false, CarbonContext::ePassCarbon);



  sVerboseCem = CRYPT("-verboseCem");
  args->addBool(sVerboseCem,
                CRYPT("Be verbose when generating and loading CEMs"),
                false, CarbonContext::ePassCarbon);
  args->putIsDeprecated(sVerboseCem, true);

  args->addString("-cemgen", "Deprecated. See +protect instead.", "", true, true, CarbonContext::ePassCarbon);
  // Do not add this to a section since it is deprecated and should not be publicly
  // documented.
  //  args->addToSection(CarbonContext::scOutputControl, "-cemgen");
  args->putIsDeprecated("-cemgen", true);

  args->addUnprocessedBool(CarbonContext::scUnprocVlog, "-u", "Use this option to make the Carbon compiler case insensitive to Verilog input. All Verilog constructs will compile properly regardless of case; however, all names will be converted to upper case.", CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, "-u");

  args->addBool("-2001",
                CRYPT("This option enables Verilog-2001 compilation mode. All files encountered during the compilation will be treated as Verilog 2001."),
                false, CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, "-2001");
  args->addSynonym("-2001", "-2000", false);
  args->addSynonym("-2001", "-v2k", false);

  args->addBool("-noPortDeclarationExtensions",
                CRYPT("This option disables the ability to use, in a single module, a mixture of the two styles of module port declarations: a simple list of port names and a list-of-ports(-2001).  Although this extension is enabled by default it is not part of the Verilog language standard."),
                false, CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, "-noPortDeclarationExtensions");

  //RJC RC#12 Warning, this setting of -synth_prefix as unprocessedString is required for both cheetah and verific, so it should be moved to carboncontext.
  //ADDRESSED FD: Ok, will refactore this
  args->addUnprocessedString(CarbonContext::scUnprocVlog, "-synth_prefix", "Specify the prefix for compiler pragmas.  Multiple instances of this switch are allowed, each one adds to the list of recognized pragma prefixes.", sDefaultSynthPrefix, false, false, CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, "-synth_prefix");
  args->addSynonym("-synth_prefix", "-pragma_prefix", false);

  args->addUnprocessedBool(CarbonContext::scUnprocVlog, "-no_translate", "Ignore translate_on/translate_off pragmas, while still adhering to other pragmas such as full_case/parallel_case.", CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, "-no_translate");
  args->addSynonym("-no_translate", "-ignore_translate_pragmas", false);
  
  const char* topMod = "+topmodule+<string>";
  args->addUnprocessedBool(CarbonContext::scUnprocVlog, topMod, "Deprecated.  Use -vlogTop <string>.  This old option also prevented parsing of multiple toplevel modules.  We need to parse them now to support the substituteModule directive.", CarbonContext::ePassCarbon);
  // Do not add this to a section since it is deprecated and should not be publicly
  // documented.
  // args->addToSection(CarbonContext::scInputFileControl, topMod);

  const char* tmod = "-vlogTop";
  args->addString( tmod, "This option allows you to specify which module in the design hierarchy is the top-most module. The Carbon compiler will analyze only the specified module and its descendents. If you do not specify this option, all modules from the top down in the given design will be compiled.", NULL, true, false, CarbonContext::ePassCarbon );
  args->addToSection( CarbonContext::scVerilog, tmod );

  args->addUnprocessedString(CarbonContext::scUnprocVlog, "-v", "Use this option to specify a library file. The Carbon compiler will scan the file for module definitions that have not been resolved in the specified design files. The file argument can be a full path or a relative file name. Note that the order in which you specify library files on the command line is very important. If you are using different versions of a library that contain identical parts, the parts that are in the last library specified on the command line will be used. Without this option, The compiler processes only those library modules that are explicitly referenced by the Verilog source files.", NULL, true, true, CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, "-v");

  args->addUnprocessedString(CarbonContext::scUnprocVlog, "-y", "Use this option to specify a library directory. The Carbon compiler will scan the directory for module definitions that have not been resolved in the specified design or library files. Enter the directory path after the option. The file names within the specified directory must match the module names that are being searched (using one of the file extensions defined by the +libext option).", NULL, true, true, CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, "-y");

  const char* libext = "+libext+<string>";
  args->addUnprocessedBool(CarbonContext::scUnprocVlog, libext, "Use this option to specify extensions used on the files you want to reference in a library directory (see the -y option). There is no default value for these extensions.  If multiple +libext+ are specified, only the last is used.   Multiple extensions may be specified in a single libext argument when separated by plus signs (+) (e.g. +libext++.v+.vlog+ defines three possible extensions: the empty string, '.v', and '.vlog') . ", CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, libext);

  const char* incdir = "+incdir+<string>";
  args->addUnprocessedBool(CarbonContext::scUnprocVlog, incdir, "Use this option to specify the directories that the Carbon compiler should search for include files. Enter the list of relative or absolute paths, linked with plus signs (+). The paths will be searched in the order specified.", CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, incdir);

  args->addUnprocessedString(CarbonContext::scUnprocVlog, "-incdir", "The NC-Verilog version of the +incdir+ Verilog-XL command line option. Will be converted internally to +incdir+. Only one directory can be specified per command line option.", NULL, true, true, CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, "-incdir");
  
  const char* define = "+define+<string>";
  args->addUnprocessedBool(CarbonContext::scUnprocVlog, define, "Use this option to specify Verilog macros to be used during compilation. Enter the variables with optional values, linked with plus signs. Syntax: +define+<var1>+<var2>+ ... +<varN>=<value>. The equals sign (=) effectively terminates the string. That is, anything after the equals sign will be treated as part of the value of the variable with which it is associated. If the value is not given the variable is defined so that `ifdef constructs will work. Later defines override earlier defines on the command line.", CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, define);

/*
   black_box causes crashes.  Don't let user specify it...

  args->addUnprocessedBool("-black_box", "Use this option to generate dummy modules for undefined instances. This option allows you to instantiate modules that are not defined in the design.", CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scModuleControl, "-black_box");
*/
  
  args->addUnprocessedBool(CarbonContext::scUnprocVlog, "-keepfirst", "Use this option to allow multiple definitions of modules, and give first occurrences of modules precedence. The Carbon compiler will maintain the first module definition it reads, even if it encounters another of the same name later in the compilation process. If you do not specify either this option or -override, the compiler will terminate and issue an error.", CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, "-keepfirst");
  
  args->addUnprocessedBool(CarbonContext::scUnprocVlog, "-override", "Use this option to allow multiple definitions of modules, and give later defined modules precedence. The Carbon compiler will use the last module definition it reads during the compilation process when it has encountered multiple modules with the same name. If you do not specify either this option or -keepfirst, the compiler will terminate and issue an error.", CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, "-override");

  args->addUnprocessedBool(CarbonContext::scUnprocVlog, "-allow", "Use this option to allow multiple definitions of a wire in the Carbon Model.", CarbonContext::ePassCarbon);
  args->addToSection(CarbonContext::scVerilog, "-allow");

  args->addUnprocessedString( CarbonContext::scUnprocVlog, "-l", "Redirect all Verilog parser messages to this file.  Any existing file contents will be deleted.", NULL, true, false, CarbonContext::ePassCarbon );
  //  args->addToSection( CarbonContext::scVerilog, "-l" );

  args->addUnprocessedString( CarbonContext::scUnprocVlog, "-la", "Redirect all Verilog parser messages to this file.  New data will be appended to this file; existing data will not be deleted.", NULL, true, false, CarbonContext::ePassCarbon );
  //  args->addToSection( CarbonContext::scVerilog, "-la" );

  // Keep this internal as it is _always_ passed to Cheetah.  Including
  // this here allows our regressions to not fail if they specify this switch.
  args->addUnprocessedBool( CarbonContext::scUnprocVlog, "-ovi2", "Make the analyzer compliant with OVI 2.0.  This switch is always passed to Cheetah.", CarbonContext::ePassCarbon );

  args->addUnprocessedBool( CarbonContext::scUnprocVlog, "+mindelay", "Use this option to select the minimum delay for all min:typ:max expressions, and evaluate static expressions using the minimum values for all min:typ:max expressions.", CarbonContext::ePassCarbon );
  args->addToSection( CarbonContext::scVerilog, "+mindelay" );

  args->addUnprocessedBool( CarbonContext::scUnprocVlog, "+typdelay", "Use this option to select the typical delay for all min:typ:max expressions, and evaluate static expressions using the typical values for all min:typ:max expressions.", CarbonContext::ePassCarbon );
  args->addToSection( CarbonContext::scVerilog, "+typdelay" );

  args->addUnprocessedBool( CarbonContext::scUnprocVlog, "+maxdelay", "Use this option to select the maximum delay for all min:typ:max expressions, and evaluate static expressions using the maximum values for all min:typ:max expressions.", CarbonContext::ePassCarbon );
  args->addToSection( CarbonContext::scVerilog, "+maxdelay" );

  args->addUnprocessedBool( CarbonContext::scUnprocVlog, "-warnForSysTask", "Use this option to generate an Alert message if an undefined system task or function is found in the design.", CarbonContext::ePassCarbon );
  args->addToSection( CarbonContext::scVerilog, "-warnForSysTask" );

  args->addUnprocessedString( CarbonContext::scUnprocVlog, "-duplicate", "Use this option to mark the file as a high priority file.  This will cause the file to be compiled first.  If a normal source file contains a module defined in the high priority file, that module will be ignored in the normal file.  It is an error if multiple high priority files define the same module.", NULL, true, true, CarbonContext::ePassCarbon );
  args->addToSection( CarbonContext::scVerilog, "-duplicate" );

  args->addUnprocessedBool( CarbonContext::scUnprocVlog, "-noBehav", "Use this option to ignore all behavioral Verilog code.  Only structural Verilog will be analyzed.", CarbonContext::ePassCarbon );
  // args->addToSection( CarbonContext::scVerilog, "-noBehav" );

  args->addUnprocessedString( CarbonContext::scUnprocVlog, "+suppress+", "Use this option to selectively suppress certain Verilog parser warning messages.  This option is valid for messages between 20000 and 29999.  You must remove the leading '2' from the message numbers.  Syntax: +suppress+<m1>+<m2>+...", NULL, true, false, CarbonContext::ePassCarbon );
  args->putIsDeprecated("+suppress+", true);

  args->addUnprocessedString( CarbonContext::scUnprocVlog, "-hierlevel", "Use this option to specify the hierarchical depth to which Verilog analysis is to be done.  The hierarchical depth of the top module is defined to be zero.  If the same module is instantiated more than once at different levels, the hierarchy level is the minimum of all instantiated levels.  A negative hierarchy level implies an infinite analysis depth.", NULL, true, false, CarbonContext::ePassCarbon );
  // args->addToSection( CarbonContext::scVerilog, "-hierlevel" ); // this is a cheetah switch

  /***********************************************************************
  ****   +stopmodule removed because of coredumps.  bug2517.
  args->addUnprocessedString( CarbonContext::scUnprocVlog, "+stopmodule+<string>", "Use this option to specify the names of modules at which analysis should stop.  This option takes prescedence over the -hierlevel option.  If a module is specified as both a top module and a stop module it is treateed as a top module.  Syntax: +stopmodule+<module1>+<module2>+...", NULL, true );
  args->addToSection( CarbonContext::scModuleControl, "+stopmodule+<string>" );
  *************************************************************************/
  args->addUnprocessedString( CarbonContext::scUnprocVlog, "-topModuleListDumpFile", "Specifies the file into which Carbon compiler will place the names of the top level modules of the Verilog design.", NULL, true, false, CarbonContext::ePassCarbon );
  args->addToSection( CarbonContext::scVerilog, "-topModuleListDumpFile" );
  args->addInt( CRYPT("-verboseUDP"), 
                CRYPT("User Defined Primitive parsing messages: 0 - none, 1 - message for each UDP parsed, 2 - decompilation of parsed UDPs"),0, false, false, CarbonContext::ePassCarbon);
  args->addBool( CRYPT("-multiLevelHierTasks") , 
                 CRYPT("Enable support for multi-level hierarchical tasks.  Using this option may cause functional differences.  Hierarchical task calls are scheduled as if they were PLI calls."), false, CarbonContext::ePassCarbon);
  args->addString(HdlFileCollector::scLegacyHdlLibOption[0], "Specify a Verilog library. With -compileLibOnly, only one library can be specified. All verilog files are compiled into that library. Without -compileLibOnly, multiple libraries can be specified. The libraries are used read-only. The last library specified becomes the WORK library for design.", "WORK:lib<design>.WORK", false, true, CarbonContext::ePassHdl);
  args->addToSection(CarbonContext::scVerilog, HdlFileCollector::scLegacyHdlLibOption[0]);
}

CheetahContext::~CheetahContext()
{
  INFO_ASSERT(sCarbonContext == mCarbonContext, "sCarbonContext modified while CheetahContext active.");
  sCarbonContext = NULL;
}

static bool sCheetahMsgMustBeErrors(int msgNo)
{
  if ( ((476 <= msgNo) && (msgNo <= 842)) ||
       ((901 <= msgNo) && (msgNo <= 950)) ||
       (412 == msgNo)                         // bug5685
    ){
    return true;
  } else {
    return false;
  }
}

//! adjust the format argument to follow Carbon's message ID
static bool sCheetahAdjustMessageIdsInMessage(int msgNo, const char* format, UtString* new_format)
{
  if ( msgNo == 910 ){
    if ( strcmp(format, " The message %d can't be %s.") == 0 ) {
      (*new_format) << " The message 20%d can't be %s.";
      return true;
    }
  }
  return false;
}

//! if message is from a protected region, provide restricted information
static bool sCheetahAdjustProtectedMessages(int msgNo, const char* sourcefilename, UtString* new_filename, UtString* new_format)
{
  if (( sourcefilename != NULL ) && ( sourcefilename[0] == '-' )) {
    new_filename->clear();
    new_filename->append(sourcefilename, 1, strlen(sourcefilename)-1);
    size_t range_back = new_filename->find_last_of(":");
    if (range_back != UtString::npos){
      new_filename->erase(range_back, new_filename->size()-range_back);
    }
    switch ( msgNo ){
    case 481:{
      (*new_format) << "Syntax error found in a protected region.";
      break;
    }
    case 495:
    case 496:{
      (*new_format) << "A protected region boundary found within a `ifdef.";
      break;
    }
    default:{
      (*new_format) << "Problem found in a protected region";
      break;
    }
    }
    return true;
  }
  return false;
}


static bool sShowCheetahStatus = false;
static void sCheetahSpew(FILE* logFilePtr, veBool isSynthMessage,
                         veMessageSeverityType severity, int cheetahMsgNo,
                         const char *sourceFileName,
                         unsigned long sourceLineNo,
                         const char *format, va_list pvar)
{
  // We've reserved message #s 20000-29999 for Cheetah
  const int msgNo = CHEETAH_MSG_OFFSET + cheetahMsgNo;
  MsgContext* msgContext = sCarbonContext->getMsgContext();
  MsgContextBase::Severity sev = MsgContextBase::eNote;
  if (!msgContext->getMsgSeverity(msgNo, &sev))
  {
    switch (severity)
    {
    case VE_UNSET_SEVERITY:
    case VE_INFORMATION:
      if (sShowCheetahStatus || isSynthMessage)
        sev = MsgContextBase::eNote;
      else
        sev = MsgContextBase::eSuppress;
      break;
    case VE_SUPPRESSED:
      sev = MsgContextBase::eSuppress;      
      break;
    case VE_WARNING:
      if (msgContext->allWarningsSuppressed())
        sev = MsgContextBase::eSuppress;
      else
        sev = MsgContextBase::eWarning;
      break;
    case VE_ERROR:
      if ( sCheetahMsgMustBeErrors(cheetahMsgNo) ) {
        sev = MsgContextBase::eError;
      } else {
        sev = MsgContextBase::eAlert;
      }
      break;
    case VE_SHOWALWAYS:
      sev = MsgContextBase::eAlert;
      break;
    }
  }
  UtString new_format;
  UtString new_filename;

  if ( sCheetahAdjustProtectedMessages(cheetahMsgNo, sourceFileName, &new_filename, &new_format ) ){
    sCarbonContext->logAnalyzerMessages(logFilePtr, sev, msgNo, new_filename.c_str(),
                                        0, new_format.c_str(), pvar); // last arg is not used but needs to be a va_list
  } else if ( sCheetahAdjustMessageIdsInMessage(cheetahMsgNo, format, &new_format) ){
    sCarbonContext->logAnalyzerMessages(logFilePtr, sev, msgNo, sourceFileName,
                                        sourceLineNo, new_format.c_str(), pvar);
  } else {
    sCarbonContext->logAnalyzerMessages(logFilePtr, sev, msgNo, sourceFileName,
                                        sourceLineNo, format, pvar);
  }
  
  

}



// turn off all messages
static void sCheetahPrepopSpew(FILE* /*logFilePtr*/, veBool /*isSynthMessage*/,
                                veMessageSeverityType severity, int /*msgNo*/,
                                const char* /*sourceFileName*/,
                                unsigned long /*sourceLineNo*/,
                                const char* /*format*/, va_list /*pvar*/)
{
  switch (severity)
  {
  case VE_ERROR:
  case VE_SHOWALWAYS:
    // sCheetahSpew(logFilePtr, isSynthMessage, severity, msgNo, sourceFileName, sourceLineNo, format, pvar);
    break;
  default:
    break;
  }
}

extern "C" void VeEnablePartialValueSetting(veBool type);

static bool sCheckArg(const char** names, const char* arg, bool isPrefix)
{
  for (const char** p = names; *p != '\0'; ++p)
  {
    if ((isPrefix && (strncmp(arg, *p, strlen(*p)) == 0)) ||
        (!isPrefix && (strcmp(arg, *p) == 0)))
      return true;
  }
  return false;
}

// Options that require a filename paramater (omitted from tokens
// file re-compilation command, in lieu of the tokens file which has
// all the verilog source code, independent of source).
static const char* sCheetahFileOptions[] = {
  "-f", "-l", "-la", "-v", "-F", "-y", "-topModuleListDumpFile", "-incdir",
  NULL
};

// Verilog options that take arguments that are not files
static const char* sCheetahValOptions[] = {
  "-Dblack_box", "-synth_prefix", "-duplicate", "-hierlevel",
  NULL
};

static const char* sCheetahIllegalOptions[] = {
  "-incr", "-work", "-print_msg_list", "-version", "-q", "-d",
  "-nl", "-out", "-black_box", "+incrext+", "-pd",
  NULL
};

static const char* sCheetahOptions[] = {
  "-w",
  "-u",
  "-ovi2",
  "-synth",
  "-warnForSysTask",
  "-categorize",
  "-analyze",
  "-libduplicates",
  "-allow",
  "-override",
  "-keepfirst",
  "-noBehav",
  "-no_translate",
  "+mindelay",
  "+typdelay",
  "+maxdelay",
  "-sysTaskFuncCheck",
  "-checkFanIn",
  "-createScalarNet",
  "-incdir",
  NULL,
};

bool CheetahContext::isVerilogOption(const char* optionName)
{
  if (*optionName == '+')
    return true;

  if (sCheckArg(sCheetahOptions, optionName, false))
    return true;

  // Don't care if it is illegal cheetah. Just want to know if we
  // handle it.
  if (sCheckArg(sCheetahIllegalOptions, optionName, false))
    return true;

  return isFileOption(optionName);
}

bool CheetahContext::isFileOption(const char* optionName)
{
  for (const char** p = sCheetahFileOptions; *p != NULL; ++p)
    if (strcmp(*p, optionName) == 0)
      return true;
  return false;
}

bool CheetahContext::isValueOption(const char* optionName)
{
  for (const char** p = sCheetahValOptions; *p != NULL; ++p)
    if (strcmp(*p, optionName) == 0)
      return true;
  return false;
}

static bool sCheckCheetahArgs(UtStringArgv &allOpts, 
                              bool hasLibs,
                              MsgContext* msgContext)
{
  // Verify that there is at least one valid filename, and that
  // all the options we pass to Cheetah match options we are
  // comfortable with.

  static const char* sCheetahPrefixOptions[] = {
    "+libext+",
    "+incdir+",
    "+define+",
    "+topmodule+",
    //"+stopmodule+",  Removed for bug2517.
    "+decompile+",
    "+suppress+",
    "+info+",
    "+warning+",
    "+error+",
    NULL
  };


  // all the arguments in argv should be vetted by us against the
  // lists above.  There should be at least one explicit existing file or
  // Cheetah is likely to complain.
  int numFiles = 0;
  int numErrors = 0;
  FILE* f = NULL;

  int argc = allOpts.getArgc();
  char **argv = allOpts.getArgv();
  for (int i = 1; i < argc; ++i)
  {
    const char* arg = argv[i];
    if (sCheckArg(sCheetahFileOptions, arg, false) ||
        sCheckArg(sCheetahValOptions, arg, false) )
    {
      ++i;                      // skip next arg
      if (i == argc)
      {
        msgContext->VlogMissingArg(arg);
        ++numErrors;
      }
    }
    else if (sCheckArg(sCheetahIllegalOptions, arg, false))
    {
      msgContext->VlogInvalidOption(arg);
      ++numErrors;
    }
    else if (sCheckArg(sCheetahOptions, arg, false))
      ;                         // this option is OK
    else if (sCheckArg(sCheetahPrefixOptions, arg, true))
    {
      if (strncmp(arg, "+topmodule+", 11) == 0)
      {
        StrToken tok(arg + 11, "+");
        if (!tok.atEnd())
        {
          msgContext->OptionDeprecated("+topmodule+", "Use option -vlogTop instead");
	  sCarbonContext->putTopModuleName(*tok);
          ++tok;
          if (!tok.atEnd())
          {
            msgContext->MultipleTopLevelModules(arg);
            ++numErrors;
          }
        }
      }
      else if (strncmp(arg, "+suppress+", 10) == 0) {
        msgContext->OptionDeprecated("+suppress+",
                                     "Use directive silentMsg <msg-number> instead");
      }
    }
    else if ((*arg == '-') || (*arg == '+'))
    {
      // extraneous +args go unwarned per verilog convention
      if (*arg == '-')
      {
        msgContext->NoSuchOption(arg);
        ++numErrors;
      }
    }
    else if ((f = OSFOpen(arg, "r", NULL)) != NULL)
    {
      ++numFiles;               // we can read it.
      fclose(f);
    }
    else
    {
      UtString errBuf;
      msgContext->VlogCannotOpenFile(arg, OSGetLastErrmsg(&errBuf));
      ++numFiles;
      ++numErrors;
    }
  }

  if ((numFiles == 0) && !hasLibs)
  {
    msgContext->VlogNoFiles();
    ++numErrors;
  }

  return (numErrors == 0);
}

//static const UInt32 cCemVersion = 1; // initial version: Oct 03 - May 05
static const UInt32 cCemVersion = 2; // Removed legacy developer tagging

bool CheetahContext::genCarbonEncryptedModel(const UtStringArray& cemModules,
                                             veNode root)
{
  UtString cemFilename;
  cemFilename << mCarbonContext->getFileRoot() << ".cem";
  ZOSTREAMDB(cemFile, cemFilename.c_str());
  if (!cemFile)
  {
    fprintf(stderr, "%s\n", cemFile.getError());
    return false;
  }

  Decompile decompile(&cemFile, mCarbonContext->getAtomicCache(), root,
                      mCarbonContext->getIODB(),
                      mCarbonContext->getMsgContext(), this);

  // Write out a version #
  cemFile << cCemVersion;

  // Write out all the top level module names we need to hide from
  // the user
  for (size_t i = 0; i < cemModules.size(); ++i)
  {
    const UtString& cemModule = cemModules[i];
    cemFile << cemModule;
    if (!decompile.protectModuleInterface(cemModule.c_str()))
    {
      MsgContext* msgContext = mCarbonContext->getMsgContext();
      msgContext->EModelGenNoSuchModule(cemModule.c_str());
      return false;
    }
  }
  cemFile << "";              // terminate with empty line

  decompile.rootDecompile(root);
  if (!cemFile.close())
  {
    fprintf(stderr, "%s\n", cemFile.getError());
    return false;
  }
  return true;
}

bool CheetahContext::genTokenFile(const char* tokenDir, veNode root) {
  mTokenDir = tokenDir;
  UtString errmsg;

  (void) OSDeleteRecursive(tokenDir, &errmsg);
  errmsg.clear();
  
  MsgContext* msgContext = mCarbonContext->getMsgContext();
  if (OSMkdir(tokenDir, 0770, &errmsg) != 0) {
    msgContext->VlogCannotOpenFile(tokenDir, errmsg.c_str());
    return false;
  }

  UtString tokenFilename, cmdFilename;
  OSConstructFilePath(&tokenFilename, tokenDir, "tokens.v");
  UtOBStream tokenFile(tokenFilename.c_str());
  if (!tokenFile.is_open()) {
    msgContext->VlogCannotOpenFile(tokenFilename.c_str(), tokenFile.getErrmsg());
    return false;
  }

  Decompile decompile(&tokenFile, mCarbonContext->getAtomicCache(), root,
                      mCarbonContext->getIODB(),
                      mCarbonContext->getMsgContext(), this);
  decompile.rootDecompile(root);

  // Write a command file that skips the input files
  OSConstructFilePath(&cmdFilename, tokenDir, "cbuild.cmd");
  UtString prefixStr;
  prefixStr << tokenFilename << "\n";

  for (AtomSet::SortedLoop p = mCemDashVFiles.loopSorted(); !p.atEnd(); ++p)
  {
    StringAtom* filename = *p;
    prefixStr << "-v " << filename->str() << "\n";
  }

  UtString tokDirectivesFile;
  OSConstructFilePath(&tokDirectivesFile, tokenDir, "tokens.dir");
  prefixStr << "-directive " << tokDirectivesFile << "\n";
  mCarbonContext->writeCmdFile(cmdFilename.c_str(), false, prefixStr.c_str());

  UtUniquify unique;
  UtString newFilename;
  for (AtomSet::SortedLoop p = mCemFiles.loopSorted(); !p.atEnd(); ++p) {
    // Change all the "/" to "_", and uniquify
    StringAtom* filename = *p;
    if (mCemDashVFiles.find(filename) == mCemDashVFiles.end()) {
      newFilename = filename->str();
      OSReplaceDirectoryDelim(&newFilename, '_');
      const char* ufile = unique.insert(newFilename.c_str());
      newFilename.clear();
      OSConstructFilePath(&newFilename, tokenDir, ufile);
      if (!OSCopyFile(filename->str(), newFilename.c_str(), &errmsg)) {
        msgContext->VlogCannotOpenFile(newFilename.c_str(), errmsg.c_str());
      }
      tokenFile << "\n`protected\n"
                << newFilename
                << "\n`endprotected\n";
    }
  }

  if (!tokenFile.close())
  {
    msgContext->VlogCannotOpenFile(tokenFilename.c_str(), tokenFile.getErrmsg());
    return false;
  }

  return true;
}

bool CheetahContext::copyDirectivesFileToTokenDir(const char* directivesFile) {
  if (!mTokenDir.empty()) {
    UtString tokDirectivesFile, err;
    OSConstructFilePath(&tokDirectivesFile, mTokenDir.c_str(), "tokens.dir");
    if (!OSCopyFile(directivesFile, tokDirectivesFile.c_str(), &err)) {
      MsgContext* msgContext = mCarbonContext->getMsgContext();
      msgContext->VlogCannotOpenFile(directivesFile, err.c_str());
      return false;
    }
  }
  return true;
}

// Note that the callers of this method are responsible for ensuring
// that -generateCleartextSource hasn't been specified, or an
// internal license is being used.
bool CheetahContext::decryptCemFile(const char* modelFile,
                                    unsigned long* index)
{
  UtString modelFileStr(modelFile);
  DecryptMap::iterator p = mDecryptMap.find(modelFileStr);
  if (p != mDecryptMap.end())
  {
    *index = p->second;
    return true;
  }

  MsgContext* const msgContext = mCarbonContext->getMsgContext();

  //  ZistreamDB model(modelFile);
  ZISTREAMDB(model, modelFile);
  if (! model)
  {
    fprintf(stderr, "%s\n", model.getError());
    return false;
  }
  UtString linebuf;

  UInt32 version = 0;
  if (! (model >> version))
  {
    msgContext->EModelCorrupt(modelFile);
    return false;
  }
  if (version > cCemVersion)
  {
    msgContext->EModelVersion(modelFile, version, cCemVersion);
    return false;
  }

  // The first strings are the modules to hide, terminating with an
  // empty string.  We will emit empty prototypes for these in cleartext,
  // for use with debussy
  while ((model >> linebuf) && !linebuf.empty())
  {
    mCarbonContext->getIODB()->addHideModuleName(linebuf.c_str());
    linebuf.clear();
  }

  // Prior to version 2, CEMs had a list of developer tags, followed
  // by an empty token.  Those were only used with the old
  // SPEEDCompiler licensing, so versions 2 and later don't have any
  // records here.  For compatibility, read and discard the tags.
  if (version < 2) {
    while ((model >> linebuf) && !linebuf.empty()) {
      linebuf.clear();
    }
  }

  Decompile decomp(&model, mCarbonContext->getAtomicCache(),
                   mCarbonContext->getIODB(),
                   mCarbonContext->getMsgContext(), this);
  decomp.putObfuscationIndex(mObfuscationIndex);
  decomp.putSynthPrefix(sDefaultSynthPrefix);
  
  UtString buf;
  while (decomp.readToken(&buf))
    ;

  // To decrypt a CEM file, break on the next line and call
  //    (gdb) p CheetahContext::writeFile("decrypt.v", &buf)
  // to dump the encrypted RTL.  Next call
  //    (gdb) p decomp.readObfuscationTable("deobf.tab")
  // to dump the decryption 'sed' command.

  mCryptBufs.push_back(buf);
  mCEMFilenames.push_back(modelFileStr);

  mObfuscationIndex = decomp.getObfuscationIndex();
  *index = mCryptBufs.size() - 1;
  mDecryptMap[modelFileStr] = *index;
  return true;
}

#ifdef CDB
bool CheetahContext::writeFile(const char* filename, UtString* contents) {
  UtOBStream ofile(filename);
  bool success = false;
  if (ofile.is_open()) {
    ofile << (*contents);
    success = ofile.close();
  }
  if (! success) {
    fprintf(stderr, "%s\n", ofile.getErrmsg());
  }
  return success;
}
#endif

bool CheetahContext::loadCarbonEncryptedModel(const char* modelFile,
                                              UtStringArgv* arg,
                                              size_t index)
{
  bool ret = true;
  unsigned long cryptIndex;

  if (mCarbonContext->isGenerateCleartextSource())
  {
    // "Cannot use -generateCleartextSource with .cem file: " + (const char* :filename) + "."
    mCarbonContext->getMsgContext()->NoCleartextCEM(modelFile);
    ret = false;
  }
  else if (!decryptCemFile(modelFile, &cryptIndex))
    ret = false;
  else
  {
    UtString buf, protectFilename;
    buf << mCarbonContext->getFileRoot() << ".protects."
        << index << ".v";
    OSContractFilename(&protectFilename, buf.c_str()); // remove cwd

    // Save an sscanf pattern for testing whether a Cheetah construct came
    // from a CEM block
    if (mCEMProtectFilePattern.empty()) {
      buf.clear();
      buf << mCarbonContext->getFileRoot() << ".protects.%d.v";
      OSContractFilename(&mCEMProtectFilePattern, buf.c_str()); // remove cwd
    }

    arg->replace(index, protectFilename.c_str());
    UtString reason;
    FILE* protectFile = OSFOpen(protectFilename.c_str(), "w", &reason);
    if (protectFile == NULL)
    {
      fprintf(stderr, "%s\n", reason.c_str());
      ret = false;
    }
    else
    {
      fprintf(protectFile, "`protected\n%lu\n`endprotected\n",
              cryptIndex);
      fclose(protectFile);
    }
    mProtectIndices.push_back(index);
  }

  return ret;
}

// Cheetah calls this when it sees a `protected/`endprotected block. 
const char* CheetahContext::decrypt(void* clientData, const char* buf,
                                    const char* filename, unsigned int line)
{
  CheetahContext* cc = reinterpret_cast<CheetahContext*>( clientData );

  const char* ret = "";
  if (cc->mCarbonContext->isLibraryCompileOnly()) {
    cc->mCarbonContext->getMsgContext()->CannotCompileProtectedSrcIntoLib(filename);
    return ret;
  }

  // We want the last line of protected text.  The 'line' variable points
  // to the `endprotected , which is one after the last line of text.
  --line; 

  SourceLocatorFactory f;
  const SourceLocator srcLocator = f.create(filename, line);

  // Construct a fake filename based on the file/line where the
  // `protected keyword was found.  We'll begin the filename with
  // '-' because in general it's a pain in Unix to have files that
  // start with '-'.  I hope that helps make uniqueness very likely,
  // but we will also put the exact name into a hash-table;
  UtString fnameBuf;
  const char* filenamePattern = NULL;
  bool changeFilenamePattern = true; // workaround for a Cheetah bug

  // there will be a newline between `protected and `endprotected
  if (*buf == '\n')
    ++buf;
  unsigned long index;
  bool isCEM = false;
  const char* cemstr;

  if (buf[0] == '@')
  {
    // Make sure we know that a line was parsed from a protected block,
    // so that if the user writes a token file, we leave the output encrypted.
    ProtectedSource source;
    ++buf; // skip over leading '@'
    source << buf;  
    if (source.unprotect())
    {
      // Skip whitespace.
      const size_t offset = source.find_first_not_of(" \t\n\r");
      // Don't allow -generateCleartextSource option unless source
      // was protected with +protectHDLOnly.
      if (cc->mCarbonContext->isGenerateCleartextSource() &&
          strncmp(source.c_str() + offset,
                  ProtectedSource::cleartextComment(),
                  strlen(ProtectedSource::cleartextComment())) != 0) // Didn't match
      {
        // "Cannot use -generateCleartextSource with `protected source."
        cc->mCarbonContext->getMsgContext()->NoCleartextProtected(&srcLocator);
        cc->mProtectBufs.push_back("");
      }
      else
      {
        cc->mProtectBufs.push_back(source);
      }
    }
    else
    {
      // error unprotecting source block
      cc->mCarbonContext->getMsgContext()->InvalidEncryptionSyntax(&srcLocator);
      cc->mProtectBufs.push_back("");
    }

    // un-comment line below to dump source encrypted via +protect
    // fprintf(stdout, "DECRYPT: %s:%d\n\t%s\n\n", filename, line, cc->mProtectBufs.back());

    ret = cc->mProtectBufs.back();
  }
  else if (sscanf(buf, "%lu\n", &index) == 1)
  {
    if (index >= cc->mCryptBufs.size())
    {
      MsgContext* msgContext = cc->mCarbonContext->getMsgContext();
      msgContext->InvalidEncryptionIndex(&srcLocator, index);
    }
    else {
      LOC_ASSERT(cc->mCEMFilenames.size() == cc->mCryptBufs.size(), srcLocator);
      isCEM = true;

      filenamePattern = cc->mCEMFilenames[index];
    }
  }
  else if ((cemstr = strstr(buf, ".cem")) != NULL)
  {
    UtString fname(buf, cemstr - buf + 4);
    FILE* f = OSFOpen(fname.c_str(), "r", NULL);
    if (f == NULL)
    {
      // Look for the filename in the directory where the protected
      // block is, if it's not there as a raw filename
      UtString path, file, newfile;
      OSParseFileName(filename, &path, &file);
      OSConstructFilePath(&newfile, path.c_str(), fname.c_str());
      f = OSFOpen(newfile.c_str(), "r", NULL);
      if (f != NULL)
      {
        fname = newfile;
        fclose(f);
      }
    }
    else
      fclose(f);

    if (cc->mCarbonContext->isGenerateCleartextSource())
      // "Cannot use -generateCleartextSource with .cem file: " + (const char* :filename) + "."
      // srcLocator represents the file referencing the .cem file.
      // fname is the name of the .cem file.
      cc->mCarbonContext->getMsgContext()->NoCleartextProtectedCEM(&srcLocator,
                                                                   fname.c_str());

    else if (cc->decryptCemFile(fname.c_str(), &index)) {
      isCEM = true;

      // There is an open Cheetah bug about the interaction of uselib and
      // cem files.  test/bugs/bug1666 is a case which fails if we call
      // veSetProtectedTextFileNamePattern to change the line numbering.  But
      // the reason for changing the line numbering was to avoid situations
      // where a small number of encrypted lines became a large number of
      // unencrypted lines, making it possible that two distinct lines of
      // source text would get the same source locator.  E.g.
      //
      //    foo.v
      //    1   `protected
      //    2   compressed_encrypted_text_for_a_big_block
      //    3   `endprotected
      //    4 
      //    5   another_line_of_verilog
      //
      // In this case, the line 4 of the expansion of
      // 'compressed_encrypted_text_for_a_big_block'
      // would look like it came from foo.v:5, which is also
      // another_line_of_verilog.  But in this particular case,
      // I don't think that's going to be an issue because people
      // put the name of a .cem file in a protected block primarily
      // to pull it in via -v or uselib.  So until the Cheetah bug
      // is fixed, don't alter the lines for this file.
#if 1
      changeFilenamePattern = false;
      filenamePattern = filename;
#else
      cc->mFilenamePatterns.push_back(fname);
      filenamePattern = cc->mFilenamePatterns.back();
#endif
    } // if
  } // else if
  else
  {
    MsgContext* msgContext = cc->mCarbonContext->getMsgContext();
    msgContext->InvalidEncryptionSyntax(&srcLocator);
  }

  if (isCEM) {
    ret = cc->mCryptBufs[index];
  }

  if (changeFilenamePattern) {
    if (filenamePattern == NULL) {
      fnameBuf << "-" << filename << ":" << line;
      cc->mFilenamePatterns.push_back(fnameBuf);
      filenamePattern = cc->mFilenamePatterns.back();
    }
    // tell Cheetah what name to use for the filename of the protected source
    veSetProtectedTextFileNamePattern(filenamePattern);
  }

  // If the original protected block has no newlines, then we
  // will not insert any into the output text.  But we will consider
  // the entire contents of line on which the protected block is inserted
  // to be protected for token generation.
  UInt32 numNewlines = std::max((size_t) 1, StringUtil::count(ret, '\n'));

  // Mark all the protected lines.
  for (UInt32 i = 1; i <= numNewlines; ++i) {
    if (isCEM) {
      cc->markCEM(filenamePattern, i, cc->mCEMFilenames[index]);
    }
    else {
      cc->markEncrypted(filenamePattern, i);
    }
  }

  return ret;
}

void CheetahContext::markEncrypted(const char* filename, unsigned int line) {
  SourceLocatorFactory* locs = mCarbonContext->getSourceLocatorFactory();
  SourceLocator loc = locs->create(filename, line);
  mEncryptedLines.insert(loc);
}

void CheetahContext::markCEM(const char* filename, unsigned int line,
                             const char* cemFilename)
{
  SourceLocatorFactory* locs = mCarbonContext->getSourceLocatorFactory();
  SourceLocator loc = locs->create(filename, line);
  AtomicCache* cache = mCarbonContext->getAtomicCache();
  StringAtom* cemFileAtom = cache->intern(cemFilename);
  mCEMLines[loc] = cemFileAtom;
  mCemFiles.insert(cemFileAtom);
}

void CheetahContext::putIsCemDashV(const char* cemDashVFilename)
{
  AtomicCache* cache = mCarbonContext->getAtomicCache();
  StringAtom* cemDashVFileAtom = cache->intern(cemDashVFilename);
  INFO_ASSERT(mCemFiles.find(cemDashVFileAtom) != mCemFiles.end(), cemDashVFilename);
  mCemDashVFiles.insert(cemDashVFileAtom);
}

bool CheetahContext::requiresProtection(const char* filename, unsigned int line)
{
  bool ret = false;
  if (filename != NULL) {
    SourceLocatorFactory* locs = mCarbonContext->getSourceLocatorFactory();
    SourceLocator loc = locs->create(filename, line);
    ret = mEncryptedLines.find(loc) != mEncryptedLines.end();
  }
  return ret;
}

bool CheetahContext::isCEMFile(const char* filename, unsigned int line,
                               UtString* cemFile)
{
  bool ret = false;
  if (filename != NULL) {
    SourceLocatorFactory* locs = mCarbonContext->getSourceLocatorFactory();
    SourceLocator loc = locs->create(filename, line);
    SourceLocatorStringMap::iterator p = mCEMLines.find(loc);
    if (p != mCEMLines.end()) {
      StringAtom* cemFileAtom = p->second;
      *cemFile = cemFileAtom->str();
      ret = true;
    }
    else if (!mCEMProtectFilePattern.empty()) {
      int cryptIndex;
      if ((sscanf(filename, mCEMProtectFilePattern.c_str(), &cryptIndex) == 1) &&
          (cryptIndex >= 0) &&
          (cryptIndex < ((int) mCEMFilenames.size())))
      {
        *cemFile = mCEMFilenames[cryptIndex];
        ret = true;
      }
    }
  }
  return ret;
}

// The first half of the original parseVerilog.  This needs to be
// separate from the populate stage for cosimulation with VHDL.
bool
CheetahContext::parseVerilog()
{
  bool ret = true;
  
  // Reassemble the options.  Get the ordered Verilog files from the
  // ArgProc.  unprocOpts has the unprocessed options to be passed
  // directly to Cheetah.
  ArgProc* args = mCarbonContext->getArgs();
  UtStringArgv cheetahArgList;
  cheetahArgList.push_back( mCarbonContext->getArgv()[0] ); // cbuild path

  HdlFileCollector::const_iterator lib_iter = mCarbonContext->getFileCollector()->begin();
  for (; lib_iter != mCarbonContext->getFileCollector()->end(); ++lib_iter )
  {
      LangVer VerilogVers[] = { LangVer::Verilog, LangVer::Verilog2001 };
      for(UInt32 ver = 0; ver < sizeof(VerilogVers)/sizeof(LangVer); ++ver)
      {
	  LangVer langVer = VerilogVers[ver];
	  
	  HdlLib::const_iterator file_iter = (*lib_iter)->begin( langVer );
	  for (; file_iter != (*lib_iter)->end( VerilogVers[ver] ); ++file_iter )
	  {
	      cheetahArgList.push_back( *file_iter );
	  }
      }
  }

  bool haveVhdlFiles =
      sCarbonContext->getFileCollector()->hasHdlFiles(LangVer::VHDL87)
      ||
      sCarbonContext->getFileCollector()->hasHdlFiles(LangVer::VHDL93);

  bool hasLibMap = (mCarbonContext->getArgs()->isParsed("-libMap") == ArgProc::eParsed);

  // If there are no VHDL files specified, complain if there are no
  // Verilog files or libraries present.
  if ( !mCarbonContext->getFileCollector()->hasHdlFiles(LangVer::Verilog) &&
       !mCarbonContext->getFileCollector()->hasHdlFiles(LangVer::Verilog2001) &&
       !mCarbonContext->getFileCollector()->hasLangLib(LangVer::Verilog) &&
       !hasLibMap )
  {
    if ( haveVhdlFiles == false )
    {
      mCarbonContext->getMsgContext()->VlogNoFiles();
      ret = false;
    }
    return ret;
  }

  UtStringArgv &unprocOpts = args->lookupUnprocessedGroupOccurrences( "Verilog" );
  int uargc = unprocOpts.getArgc();
  char** uargv = unprocOpts.getArgv();

  // Push the unprocessed Verilog args into arg.
  for ( int i = 0; i < uargc; ++i )
  {
    UtString buf(uargv[i]);
    if ( buf[0] == '+' ) {
      buf[0] = '-';
      bool req_value, is_input_file, is_output_file;
      if ( args->isValid(buf.c_str(), &req_value, &is_input_file, &is_output_file) ){
        mCarbonContext->getMsgContext()->MatchingPlusAndCarbonArg(uargv[i]);
      }
    }

    // If -incdir <directory> was specified, convert it to +incdir+<directory>
    if ( buf == "-incdir" ) {
      UtString dir = "+incdir+";
      dir += uargv[++i];
      cheetahArgList.push_back( dir );
    }
    else {
      cheetahArgList.push_back( uargv[i] );
    }
  }

  if ( !sCheckCheetahArgs( cheetahArgList,
                           mCarbonContext->getFileCollector()->hasLangLib(LangVer::Verilog) || hasLibMap,
                           mCarbonContext->getMsgContext() ))
    return false;

  // veCreateUserMetaComment defines a string that will be used to identify
  // meta comments e.g. the "carbon" in: // carbon translate_off.  
  // You could get the same results with the synth_prefix switch.  
  veCreateUserMetaComment(
    const_cast<char*>(sDefaultSynthPrefix), // Interra misdeclares API function
    VE_SINGLE_TYPE);
  veSetExprEvaluationOption (VE_CONSIDER_CONTEXT);
  
  veDisableSignalHandling(VE_TRUE);
  cheetahArgList.push_back("-nl");  // don't show cheetah logo
  if (args->getBoolValue("-showParseMessages"))
    sShowCheetahStatus = true;
  else
    cheetahArgList.push_back("-q");
  veRegisterMessageHandler(sCheetahSpew);

  char **argv = cheetahArgList.getArgv();
  int argc = cheetahArgList.getArgc();

# define XLATE_VLOGTOP_TO_TOPMODULE 0
# if XLATE_VLOGTOP_TO_TOPMODULE
  const char* vlogTopArg = mCarbonContext->getArgs()->getStrValue("-vlogTop");
  if (vlogTopArg != NULL) {
    UtString topArg;
    topArg << "+topmodule+" << vlogTopArg;

    IODBNucleus* iodb = mCarbonContext->getIODB();
    for (IODBNucleus::SubstituteLoop p(iodb->loopSubstituteModules());
         !p.atEnd(); ++p)
    {
      IODBNucleus::NUSubstituteModuleData* sub = p.getValue();
      topArg << "+" << sub->mName;
    }
    cheetahArgList.push_back(topArg.c_str());
  }
#endif

  // Walk the files in argv looking for an encrypted model
  for (int i = 1; i < argc; ++i)
  {
    const char* file = argv[i];
    size_t fsize = strlen(file);
    if ((fsize > 4) && (strcmp(file + fsize - 4, ".cem") == 0))
    {
      if (!loadCarbonEncryptedModel(file, &cheetahArgList, i))
        return false;
    }
  }

  bool v2001_mode = mCarbonContext->getArgs()->getBoolValue("-2001");
  if (v2001_mode) {
    cheetahArgList.push_back("-2001");
  }

  cheetahArgList.push_back(CRYPT("+define+CARBON"));
  cheetahArgList.push_back(CRYPT("-ovi2"));

  bool disable_port_declaration_extensions = mCarbonContext->getArgs()->getBoolValue("-noPortDeclarationExtensions");
  if ( not disable_port_declaration_extensions ){
    cheetahArgList.push_back(CRYPT("-vcs_compat"));
  }

  // All message severities are adjusted using the Cheetah API instead
  // of passing them in the argument list to veAnalyze.
  adjustSeverities();
  
  // Need to do cModule stuff...
  /*
    "cModuleBegin",
    "cPortBegin",
    "cNullPortBegin",
    "cTiming",
    "cFanin",
    "cPortEnd",
    "cNullPortEnd",
    "cModuleEnd",
    "cFunctionBegin",
    "cFunctionEnd",
    "cTaskBegin",
    "cTaskEnd",
    "cArg",
    "cHasSideEffect",
  */

  // bug 2969 - allow strange $recovery calls.
  veEnableRecoveryWith4Args(VE_TRUE);

  veEnablePragmaDetection(VE_TRUE);
  for (const char** p = mCarbonContext->scNetPragmas; *p != NULL; ++p)
    (void) veCreateUserPragmaComment((char*) *p,
                                     VE_DIRECTIVE_PREV,
                                     VE_DECL_OBJ);

  for (const char** p = mCarbonContext->scModulePragmas; *p != NULL; ++p)
    (void) veCreateUserPragmaComment((char*) *p,
                                     VE_DIRECTIVE_CURR_SCOPE,
                                     VE_CURRENT_SCOPE_OBJ);

  for (const char** p = mCarbonContext->scFunctionPragmas; *p != NULL; ++p)
    (void) veCreateUserPragmaComment((char*) *p,
                                     VE_DIRECTIVE_CURR_SCOPE,
                                     VE_CURRENT_SCOPE_OBJ);

  /*
    only do ip licensing for vsp compiles. This might change, but the
    current requirement is that speedcompiler licensing remain
    unchanged.
    The license pragma appears within a module scope and looks like
    carbon license <license_name>
    
    Obviously, this directive should appear within a protected block.

    Note that 'license' is NOT a carbon directive that can be used in
    a directives file.
    This license directive requires that the runtime will have to
    checkout a license with that feature name. A check will be done
    during cbuild that makes sure that a license feature of that name
    does exist.
  */
  (void) veCreateUserPragmaComment((char*)mCarbonContext->scLicenseStr, VE_DIRECTIVE_CURR_SCOPE, VE_CURRENT_SCOPE_OBJ);

  if(mCarbonContext->getFileCollector()->hasLangLib(LangVer::Verilog)) { // HACK to make Cheetah Happy 08/19/08
      for ( lib_iter = mCarbonContext->getFileCollector()->begin();
	      lib_iter != mCarbonContext->getFileCollector()->end();
	      ++lib_iter )
      {
	  if(LangVer::Verilog == (*lib_iter)->libraryLanguage() || LangVer::LangVer_MAX == (*lib_iter)->libraryLanguage() ) {
	      veSetLibraryMapping((*lib_iter)->getLogicalLibrary().c_str(), (*lib_iter)->getLibPath().c_str());
	  }
      }
  }

  const char* libName = mCarbonContext->getFileCollector()->getDefaultLibName(LangVer::Verilog);
  const char* libPath = mCarbonContext->getFileCollector()->getDefaultLibPath(LangVer::Verilog);

  // If library hasn't been specified, set the default library mapping.
  if (!mCarbonContext->getFileCollector()->hasLangLib(LangVer::Verilog))
    veSetLibraryMapping(libName, libPath);

  // The default or last library becomes work library.
  veSetWorkLibrary(libName);

  {
    // Create a physical directory for the library
    UtString result;
    OSStatFile(libPath, "ed", &result);
    if (result[0] != '1' || result[1] != '1') {
      UtString errMsg;
      if (OSMkdir(libPath, 0777, &errMsg) != 0) {
        mCarbonContext->getMsgContext()->VerilogCannotMkdirLibrary(errMsg.c_str());
      }
    }
  }

  int status = 0;
  // Invoke analyzer if we have verilog files to compile.
  {
    // Disallow dumping the binary if -libMap is not specified in the cmdLine.
    if ((mCarbonContext->getArgs()->isParsed("-libMap") != ArgProc::eParsed) &&
	(!mCarbonContext->isLibraryCompileOnly()))
    {
      veEnableDump(VE_FALSE);
    }

    if ( args->getBoolValue("-synth") == true)
    {
      // Do not pass the synth switch to Interra, as the Interra
      // analysis is lacking, so we have implemented our own.
      // See localflow/SensitivityList.h
      // cheetahArgList.push_back( "-synth" );
    }

    //// display the Cheetah arguments
// #   define DISPLAY_CHEETAH_ARGS
#   ifdef DISPLAY_CHEETAH_ARGS
    int CheetahArgc = cheetahArgList.getArgc();
    char** cheetahArgv = cheetahArgList.getArgv();
    UtIO::cout() << "<CheetahArgs>" << UtIO::endl;
    for(int i = 0; i < CheetahArgc; i++) {
        UtIO::cout() << '\t' << cheetahArgv[i] << UtIO::endl;
    }
    UtIO::cout() << "</CheetahArgs>" << UtIO::endl;
#   endif

    if( (mCarbonContext->getFileCollector()->hasHdlFiles(LangVer::Verilog)) ||
        (mCarbonContext->getFileCollector()->hasHdlFiles(LangVer::Verilog2001))) {
	status = veAnalyze( cheetahArgList.getArgc(), cheetahArgList.getArgv(), &mRoot );
    } else {
	// set the comandline options
	veNode cheetahCmdLine = veGetCurrentCommandLine ();
	veParseCommandLine ( cheetahArgList.getArgc(), cheetahArgList.getArgv(), &cheetahCmdLine);
	veUpdateCommandLine (cheetahCmdLine);
    }
  }

  if ( status != 0 )
  {
    mCarbonContext->getMsgContext()->CheetahParseFailure();
    ret = false;
  }

  cleanProtectTempFiles();

  return ret;
}

// The first half of the original parseVerilog.  This needs to be
// separate from the populate stage for cosimulation with VHDL.
bool
CheetahContext::parseVerificVerilogCmdLine() {
    
  bool ret = true;
  // Reassemble the options.  Get the ordered Verilog files from the
  // ArgProc.  unprocOpts has the unprocessed options to be passed
  // directly to Cheetah.
  ArgProc* args = mCarbonContext->getArgs();
  UtStringArgv cheetahArgList;
  cheetahArgList.push_back( mCarbonContext->getArgv()[0] ); // cbuild path

  HdlFileCollector::const_iterator lib_iter = mCarbonContext->getFileCollector()->begin();
  for (; lib_iter != mCarbonContext->getFileCollector()->end(); ++lib_iter )
  {
      LangVer VerilogVers[] = { LangVer::Verilog, LangVer::Verilog2001 };
      for(UInt32 ver = 0; ver < sizeof(VerilogVers)/sizeof(LangVer); ++ver)
      {
	  LangVer langVer = VerilogVers[ver];
	  
	  HdlLib::const_iterator file_iter = (*lib_iter)->begin( langVer );
	  for (; file_iter != (*lib_iter)->end( VerilogVers[ver] ); ++file_iter )
	  {
	      cheetahArgList.push_back( *file_iter );
	  }
      }
  }

  bool haveVhdlFiles =
      sCarbonContext->getFileCollector()->hasHdlFiles(LangVer::VHDL87)
      ||
      sCarbonContext->getFileCollector()->hasHdlFiles(LangVer::VHDL93);

  bool hasLibMap = (mCarbonContext->getArgs()->isParsed("-libMap") == ArgProc::eParsed);

  // If there are no VHDL files specified, complain if there are no
  // Verilog files or libraries present.
  if ( !mCarbonContext->getFileCollector()->hasHdlFiles(LangVer::Verilog) &&
       !mCarbonContext->getFileCollector()->hasHdlFiles(LangVer::Verilog2001) &&
       !mCarbonContext->getFileCollector()->hasLangLib(LangVer::Verilog) &&
       !hasLibMap )
  {
    if ( haveVhdlFiles == false )
    {
      mCarbonContext->getMsgContext()->VlogNoFiles();
      ret = false;
    }
    return ret;
  }

  UtStringArgv &unprocOpts = args->lookupUnprocessedGroupOccurrences( "Verilog" );
  int uargc = unprocOpts.getArgc();
  char** uargv = unprocOpts.getArgv();

  // Push the unprocessed Verilog args into arg.
  for ( int i = 0; i < uargc; ++i )
  {
    UtString buf(uargv[i]);
    if ( buf[0] == '+' ) {
      buf[0] = '-';
      bool req_value, is_input_file, is_output_file;
      if ( args->isValid(buf.c_str(), &req_value, &is_input_file, &is_output_file) ){
        mCarbonContext->getMsgContext()->MatchingPlusAndCarbonArg(uargv[i]);
      }
    }

    // If -incdir <directory> was specified, convert it to +incdir+<directory>
    if ( buf == "-incdir" ) {
      UtString dir = "+incdir+";
      dir += uargv[++i];
      cheetahArgList.push_back( dir );
    }
    else {
      cheetahArgList.push_back( uargv[i] );
    }
  }

  if ( !sCheckCheetahArgs( cheetahArgList,
                           mCarbonContext->getFileCollector()->hasLangLib(LangVer::Verilog) || hasLibMap,
                           mCarbonContext->getMsgContext() )) {
    ret = false;
    return ret;
  }

return ret;
}

void CheetahContext::adjustSeverities() {
  // Suppress Cheetah messages:
  static SInt32 sup[] = {
    76,  // synth warning -- Module <%s> has Non-RTL construct
    77,  // synth warning -- No associated synthesis_off found
    78,  // synth warning -- Only simple identifiers are allowed in the senstivity list, other expressions are ignored
    80,  // synth warning -- Primitive description will be ignored and its instances will be black-box instance
    81,  // synth warning -- Initialized reg < %s> is being written
    82,  // synth warning -- System task <%s> is not supported
    83,  // synth warning -- Width of the index value, <%s> in for loop, is less than that of the termination value. 
         //                  This may lead to improper synthesization
    84,  // synth warning -- The termination condition in for loop is on <%s>, and not on the actual index <%s>
    85,  // synth warning -- The for loop is being incremented by <%s>, which does not match the loop index <%s>
    86,  // synth warning -- Unrollable for loop. Non-static expression cannot be evaluated at compile time
    87,  // synth warning -- Parameter override statements using DEFPARAM are not supported
    90,  // synth warning -- Variable <%s> is being assigned at more than one places. This may cause simulation synthesis mismatch
    94,  // synth warning -- <%s> operator will be treated as <%s>
    101, // synth error   -- INITIAL statements are not supported
    102, // synth error   -- FORCE statements are not supported
    103, // synth error   -- RELEASE statements are not supported
    104, // synth error   -- DEASSIGN statements are not supported
    105, // synth error   -- REPEAT statements are not supported
    106, // synth error   -- WAIT statements are not supported
    107, // synth error   -- PRIMITIVE definitions are not supported
    108, // synth error   -- TIME declarations are not supported
    109, // synth error   -- EVENT declarations are not supported
    110, // synth error   -- Parallel blocks are not supported
    112, // synth error   -- REPEAT event control specifications are not supported
    113, // synth error   -- Arrays of integer variables are not supported
    114, // synth error   -- TRI0 net types are not supported
    115, // synth error   -- TRI1 net types are not supported
    116, // synth error   -- TRIAND net types are not supported
    117, // synth error   -- TRIOR net types are not supported
    118, // synth error   -- TRIREG net types are not supported
    119, // synth error   -- === operators are not supported
    120, // synth error   -- !== operators are not supported
    125, // synth error   -- System function <%s> is not supported
    126, // synth error   -- Procedural continuous assign statements are not supported
    127, // synth error   -- Event Control expressions within non-blocking statements are not supported
    128, // synth error   -- REAL declarations are not supported
    130, // synth error   -- <%s> gate types are not supported
    131, // synth error   -- EVENT unsupported, so <%s> cannot be triggered
    132, // synth error   -- Unsupported hierarchical reference < %s >
    133, // synth error   -- tran gate types are not supported
    141, // synth error   -- Always block with timing control, cannot have event control expressions
    143, // synth error   -- Asynchronous reset/clear always block should have only one statement
    144, // synth error   -- Asynchronous reset/clear always block should have first statement as If
    145, // synth error   -- Asynchronous always block may have comparison to 1 or 0 only.
    147, // synth error   -- Asynchronous reset/clear always block may have the reset condition only as a simple identifier or its negation
    148, // synth error   -- Invalid reset condition in asynchronous reset/clear always block
    149, // synth error   -- Only the '==' perator is allowed in validation of the reset condition
    154, // synth error   -- Both edge control & non-edge control expressions cannot be specified in the senstivity list
    155, // synth error   -- Event control list, with posedge and negedge qualifiers may have simple identifiers only
    161, // synth error   -- You must specify an 'else' clause for the clocked logic
    162, // synth error   -- Improper modelling style for a sequential machine. Clock & reset/set pins cannot be inferred from 
         //                  the event control list
    164, // synth error   -- In the implicit style sequential state machine, states can only be updated if controlled by same clock phase
    166, // synth error   -- In the implicit style sequential state machine, the event control statement may not have more 
         //                  than one edge specification
    167, // synth error   -- In the implicit style sequential state machine, the @(event_control_list) should have one edge specification
    174, // synth error   -- The clock <%s> is being read inside the always block, which is not allowed in the explicit style 
         //                  sequential state machine
    175, // synth error   -- The clock signal < %s > cannot be a vector
    176, // synth error   -- Multiple clocks are used in asynchronous always block
    186, // synth error   -- While / forever loop without an event control statement to break the infinite loop is not allowed
    188, // synth error   -- For loop index <%s> cannot be updated inside the for loop
    189, // synth error   -- Disable statement is not in the scope of block <%s >
    196, // synth error   -- Task should not have event control statements
    204, // synth error   -- Nested function call detected inside function <%s >
    206, // synth error   -- Condition in the if_elseif statement does not match those in the event control list
    207, // synth error   -- Condition expression <%s> for CASE statement has been previously assigned an unknown value
    200, // synth error   -- Non RTL forever/while loop in function/task
    222, // synth error   -- Both blocking & non-blocking assignments are being done on the variable <%s>

    278, // Unable to evaluate the constant expression -- cbuild detect this independently and prints the source line
    352, // recursive function -- cbuild does its own warnings for those
    400, // suppress bogus real number truncation error
    428, // "Register in specify block" error.  We don't care about timing stuff.
    -1};
  
  // Reduce from error to warning:
  static SInt32 warn[] = {
    79,  // synth warning -- Though not in the sensitivity list, <%s> is being referred to inside this always block
    387, // Reduction AND follows bitwise OR operator, consider using brackets to disambiguate
    388, // Reduction OR follows bitwise OR operator, consider using brackets to disambiguate
    341, // Illegal vector reference to scalar net - (w). 
    266, // missing `endcelldefine
    439, // $ferror in Verilog-95 mode
    440, // $fflush in Verilog-95 mode
    457, // has a timescale directive while previous modules do not
    465, // does not have a timescale directive while previous modules have.
    -1};

  static SInt32 note[] = {
    34,  // No connection to module port (for explicit disconnected)
    -1
  };

  static SInt32 error[] = {
    -1
  };

  static SInt32 alert[] = {
    -1};

  typedef UtHashMap<SInt32,MsgContextBase::Severity> SeverityMap;
  SeverityMap overrides;

  for (SInt32* p = sup;  *p != -1; ++p) {overrides[*p] = MsgContextBase::eSuppress;}
  for (SInt32* p = warn; *p != -1; ++p) {overrides[*p] = MsgContextBase::eWarning;}
  for (SInt32* p = note; *p != -1; ++p) {overrides[*p] = MsgContextBase::eNote;}
  for (SInt32* p = error; *p != -1; ++p) {overrides[*p] = MsgContextBase::eError;}
  for (SInt32* p = alert; *p != -1; ++p) {overrides[*p] = MsgContextBase::eAlert;}

  //bool haveVhdlFiles = 
  //    sCarbonContext->getFileCollector()->hasHdlFiles(LangVer::VHDL87)
  //    ||
  //    sCarbonContext->getFileCollector()->hasHdlFiles(LangVer::VHDL93);

  // TODO fix following 08/22/2008
  // following condition is always true in old code
  //if ( not haveVhdlFiles) {
  if (true) {
    // when there are only verilog source files then suppress the
    // cheetah message about missing modules.  This avoids messages 
    // about missing modules that are only used within an unrechable
    // branch of a generate statement. (bug 7122).
    // If there are missing modules they will be reported as a mvv error 43002.
    // NOTE this is a temp workaround until interra automatically
    // suppresses 252 for us when we call the PI:
    // mvvDesignIsVerilogOnly(MVV_TRUE)
    // (which we do when there are only verilog source files).
    overrides[252] = MsgContextBase::eSuppress;
  }


  MsgContextBase::SeverityArray severities;
  MsgContextBase::IntArray msgNumbers;
  MsgContext* msgContext = sCarbonContext->getMsgContext();
  msgContext->getUnknownSeverityRange(CHEETAH_MSG_OFFSET, CHEETAH_MSG_OFFSET+9999,
                                      &severities, &msgNumbers);

  for (UInt32 i = 0; i < severities.size(); ++i) {
    MsgContextBase::Severity sev = severities[i];
    SInt32 msgNumber = msgNumbers[i];
    overrides[msgNumber - CHEETAH_MSG_OFFSET] = sev;
  }

  for (SeverityMap::iterator p = overrides.begin(); p != overrides.end(); ++p) {
    SInt32 msgNumber = p->first;
    MsgContextBase::Severity sev = p->second;
    if (( sev != MsgContextBase::eError ) && sCheetahMsgMustBeErrors( msgNumber) ) {
      // warn user
      const char* sevname = NULL ;
      
      switch ( sev ){
      case MsgContextBase::eSuppress: {sevname = "silent"; break;}
      case MsgContextBase::eNote:     {sevname = "info"; break;}
      case MsgContextBase::eWarning:  {sevname = "warning"; break;}
      case MsgContextBase::eError:    {sevname = "error"; break;}
      default:   {sevname = "<unknown>"; break;}
      }

      SourceLocator loc;
      msgContext->IgnoringMsgSeverityAdjust(&loc, sevname, msgNumber+CHEETAH_MSG_OFFSET, "Error" );
      // force our internal severity table back to the severity that cheetah originally had.
      sev = MsgContextBase::eError;
    }
    else
    {
      // Change the severity of the message
      switch (sev) {
      case MsgContext::eSuppress:
	veMessageSuppress(msgNumber);
	break;
      case MsgContext::eNote:
      case MsgContext::eWarning:
      case MsgContext::eStatus:
      case MsgContext::eContinue:
	veMessageSetSeverity(msgNumber, VE_WARNING);
	break;
      case MsgContext::eError:
      case MsgContext::eAlert:
      case MsgContext::eFatal:
	veMessageSetSeverity(msgNumber, VE_ERROR);
	break;
      }
    }
    msgContext->putMsgNumSeverity(msgNumber + CHEETAH_MSG_OFFSET, sev);

  } // for
}

void CheetahContext::cleanProtectTempFiles() {
  for (size_t i = 0; i < mProtectIndices.size(); ++i) {
    size_t index = mProtectIndices[i];
    UtString buf, protectFilename, errmsg;
    buf << mCarbonContext->getFileRoot() << ".protects."
        << index << ".v";
    OSContractFilename(&protectFilename, buf.c_str()); // remove cwd
    if (OSUnlink(protectFilename.c_str(), &errmsg) != 0) {
      UtIO::cerr() << errmsg << "\n";
    }
  }    
}

class CheetahProtectNetsCallback : public NUDesignCallback {
public:
  CheetahProtectNetsCallback(CheetahContext* cc) : mCheetahContext(cc) {}
  Status operator() (Phase, NUBase*) { return eNormal; }

  Status operator() (Phase phase, NUNet* net) {
    if (phase == ePre) {
      const SourceLocator& loc = net->getLoc();
      if (mCheetahContext->requiresProtection(loc.getFile(), loc.getLine())) {
        net->putIsTemp(true);
      }
    }
    return eNormal;
  }

private:
  CheetahContext* mCheetahContext;
};

// The second half of the original parseVerilog.  This needs to be
// separate from the parse stage for cosimulation with VHDL.
bool
CheetahContext::prepPopulateVerilog(mvvNode* topPrimary, mvvNode* topArchitecture, mvvNode* topBlkCfg)
{
  ArgProc* args = mCarbonContext->getArgs();

  *topArchitecture = NULL;
  *topBlkCfg = NULL;
  
  bool ret = true;

  veNode rootMod = getTopModule();
  *topPrimary = rootMod;
  if (rootMod != NULL)
  {
    InLineDirectivesCB inLineDirectivesCB(mCarbonContext);
    InterraDesignWalker harvestDirectivesWalker(&inLineDirectivesCB, mTicProtectedNameMgr);
    
    {
      mCarbonContext->putInPrePopWalk(true); // true silences all messages, but the directiveWalker will report
                                             // erorrs due to licensing by disabling this if it finds a license error

      // Ignore the return status of the walker because even if
      // there were problems, no message would have been printed.
      // Leave error handling for later.
      (void) harvestDirectivesWalker.design(*topPrimary, *topArchitecture, *topBlkCfg, "");

      mCarbonContext->putInPrePopWalk(false);
    }
    ret &= ( inLineDirectivesCB.getErrCode() == Populate::eSuccess);
  }

  // should xxxx all the memory first to limit time-in-core for
  // decrypted info
  mCryptBufs.clear();
  mProtectBufs.clear();

  // Look for -cemgen flag, which contains the set of top-level modules
  UtStringArray cemModules;
  ArgProc::StrIter iter;
  bool known = args->getStrIter("-cemgen", &iter) == ArgProc::eKnown;
  INFO_ASSERT(known, "-cemgen switch not known");
  for (char* cemModule; iter(&cemModule);)
    cemModules.push_back(cemModule);
  
  if (!cemModules.empty())
    ret = genCarbonEncryptedModel(cemModules, mRoot);
  
  if (args->isParsed("-genTokenFile") == ArgProc::eParsed) {
    const char* tokenFile = args->getStrValue("-genTokenFile");
    genTokenFile(tokenFile, mRoot);
  }
  
  return ret;
}

bool CheetahContext::postPopulate(Populate::ErrorCode err_code, NUDesign* design)
{
  mDesign = design;
  bool ret = true;
  switch (err_code) {
  case Populate::eSuccess: {
    break;
  }
    case Populate::eNotApplicable:
    case Populate::eFailPopulate:
    case Populate::eFatal: {
      mCarbonContext->getMsgContext()->PopulationFailure();
      ret = false;
      break;
    }
  }
  if (CarbonMem::isMemDebugOn())
    CarbonMem::checkpoint("populateVerilogFinishing");
  veFreeMemory();
  veFreeLicense();
  CarbonMem::releaseBlocks();
  
  // Prevent waveforms from being dumped on nets declared in protected blocks
  if ( ret and mDesign ) {
    CheetahProtectNetsCallback cb(this);
    NUDesignWalker walker(cb, false, false);
    walker.putWalkTasksFromTaskEnables(false);
    walker.putWalkDeclaredNets(true);
    walker.design(mDesign);
  }
  
  return ret;
}

veNode CheetahContext::getTopModule()
{
  const char* vlogTopArg = mCarbonContext->getArgs()->getStrValue("-vlogTop");

  if (mCarbonContext->getFileCollector()->hasLangLib(LangVer::Verilog)) // we've a library
  {
    // Enforce specification of root module.
    if (vlogTopArg == NULL)
    {
      mCarbonContext->getMsgContext()->VlogHasLibButNoTopModule("-vlogTop");
      return NULL;
    }
  }

  if (vlogTopArg)
  {
    // Let mvv pick the top level design unit from either the work library or the parsed verilog.
    mvvNode topDU = mvvOpenDesignUnit(const_cast<char*>(mCarbonContext->getFileCollector()->getDefaultLibName(LangVer::Verilog)),
				      const_cast<char*>(vlogTopArg), 
				      0);
    if (topDU == NULL)
    {
      mCarbonContext->getMsgContext()->VlogTopModuleNotFound(vlogTopArg);
      return NULL;
    }

    mvvLanguageType duLang = mvvGetLanguageType(topDU);
    if (duLang != MVV_VERILOG)
    {
      mCarbonContext->getMsgContext()->VlogTopModuleNotVlogModule(vlogTopArg, "-vlogTop");
      return NULL;
    }

    veNode topMod = static_cast<veNode>(topDU);
    if ( (veNodeGetObjType(topMod) != VE_MODULE) &&
         (veNodeGetObjType(topMod) != VE_UDP) )
    {
      mCarbonContext->getMsgContext()->NoVlogTopModule(vlogTopArg);
      return NULL;
    }

    UtString modName;
    mTicProtectedNameMgr->getVisibleName(topMod, &modName, true);
    if (strcmp(vlogTopArg, modName.c_str()) != 0)
    {
      mCarbonContext->getMsgContext()->NoVlogTopModule(vlogTopArg);
      return NULL;
    }

    return topMod;
  }

  CheetahList ve_iter(veRootGetTopLevelModuleUdpList(mRoot));

  // Cheetah tests for having at least 1 top-level module.
  if (not ve_iter) {
    return NULL;
  }

  veNode ve_first = NULL;
  // No -vlogTop specified.  Use the single top level module as the
  // root, or use the old style +topmodule+ to specify it.
  // Check for multiple top-level modules, currently unimplemented.
  ve_first = veListGetNextNode(ve_iter);
  veNode ve_next = veListGetNextNode(ve_iter); 
  //! Public entry point for populating a design
  if (ve_next != NULL) {
    UtString buf;             // pessimistically assume we will report an error
    veListRewind(ve_iter);
    const char* topModName = mCarbonContext->getTopModuleName();
    for (ve_next = veListGetNextNode(ve_iter); ve_next; ve_next = veListGetNextNode(ve_iter)) {
      CheetahStr name(veNamedObjectGetName(ve_next)); // (include this commment ONLY if you know why it is here veGetName_ISOK)

      SourceLocatorFactory* locs = mCarbonContext->getSourceLocatorFactory();
      CheetahStr fname(veNodeGetFileName(ve_next));
      SourceLocator loc = locs->create(fname, veNodeGetLineNumber(ve_next));
      UtString printable_name;
      if ( loc.isTicProtected() ){
        printable_name = "<protected>";
      } else {
        printable_name = name;
      }
        
      veObjType t = veNodeGetObjType( ve_next );
      if( t != VE_MODULE && t != VE_UDP ) {
        const char* typeStr = gGetObjTypeStr(veNodeGetObjType(ve_next));
        mCarbonContext->getMsgContext()->UnsupportedInstanceType(&loc, printable_name.c_str(), typeStr);
      }
      else if ((topModName != NULL) && (strcmp(topModName, name) == 0)) {
        // Found the specified top-level module.  Use this one even though others
        // may exist.
        return ve_next;
      }
      if (not buf.empty()) {
        buf += ", ";
      }
      buf += printable_name;
    }
    mCarbonContext->getMsgContext()->MultipleTopLevelModules(buf.c_str());
    ve_first = NULL;
  } // if
  return ve_first;
}

bool CheetahContext::setTopLevelParameters(veNode topLevelModule) 
{
  ArgProc::StrIter iter;
  bool known = mCarbonContext->getArgs()->getStrIter(CarbonContext::scTopLevelParam, &iter) == ArgProc::eKnown;

  // Process the -topLevelParam options
  UtString msg(CarbonContext::scTopLevelParam);
  msg += " switch not known";
  INFO_ASSERT(known, msg.c_str());

  UtMap<UtString, UtString> paramValPairs;
  for (char* paramVal; iter(&paramVal);) {
    UtString paramValStr(paramVal);
    size_t equal = paramValStr.find('=');
    if (equal == UtString::npos) {
      mCarbonContext->getMsgContext()->TopLevelParamMissingAssignment(paramValStr.c_str());
      return false;
    }
    UtString param = paramValStr.substr(0, equal);
    UtString val = paramValStr.substr(equal+1);
    paramValPairs[param] = val;
  }

  CheetahList params(veModuleGetParamList(topLevelModule));

  if (veListGetSize(params) == 0) {
    // This module has no parameters, but the user was expecting it to.
    UtString modName;
    mTicProtectedNameMgr->getVisibleName(topLevelModule, &modName, true);
    mCarbonContext->getMsgContext()->TopLevelModuleHasNoParameters(modName.c_str());
    return false;
  }

  veNode param;
  UtMap<UtString,UtString>::iterator paramIter;

  UtSet<UtString> actualParams;

  while ((param = veListGetNextNode(params))) {

    UtString paramName;
    mTicProtectedNameMgr->getVisibleName(param, &paramName, true);
    paramIter = paramValPairs.find(paramName);

    actualParams.insert(paramName);

    if (paramIter == paramValPairs.end()) {
      // This particular parameter was not overridden in the command line
      continue;
    }

    UtString paramValStr = paramIter->second;

    veNode pParamVal = NULL;

    if (paramValStr.find('\"') != UtString::npos) {
      pParamVal = veModuleCreateString( topLevelModule, const_cast<char*>(paramValStr.c_str()));
    }
    else {
      pParamVal = veCreateExprFromString(const_cast<char*>(paramValStr.c_str()), topLevelModule);
    }

    // Create a valid parameter value
    if (pParamVal != NULL) {
      veNodeSetStartLineNumber(pParamVal, veNodeGetLineNumber(param));
      veNodeSetEndLineNumber(pParamVal, veNodeGetEndLineNumber(param));
      veNodeSetFileName(pParamVal, veNodeGetFileName(param));
    }
    veParamSetDefaultValue(param, pParamVal);
    veParamSetCurrentValue(param, pParamVal);
  }

  // Verify that all the parameters specified using -topLevelParam actually exist in the top level module
  for (paramIter = paramValPairs.begin(); paramIter != paramValPairs.end(); ++paramIter) {
    if (actualParams.find(paramIter->first) == actualParams.end()) {
      UtString modName;
      mTicProtectedNameMgr->getVisibleName(topLevelModule, &modName, true);
      mCarbonContext->getMsgContext()->TopLevelParamDoesNotExist(paramIter->first.c_str(), modName.c_str());
      return false;
    }
  }

  return true;
}

Populate::ErrorCode 
CheetahContext::declarePragmaDirectives(const char* modName,
                                        const char* netName,
                                        veNode node)
{
  Populate::ErrorCode status = Populate::eSuccess;
  
  CheetahList pragmas(veNodeGetAllPragma(node));
  if (pragmas != NULL) {
    IODBNucleus* iodb = mCarbonContext->getIODB();
    SourceLocatorFactory* locs = mCarbonContext->getSourceLocatorFactory();

    bool isVSP = mCarbonContext->isVSPCompile();

    veNode pragma;
    while ((pragma = veListGetNextNode(pragmas)) != NULL) {
      // Cheetah reports the full text of the comment directive, e.g.
      // "  carbon   observeSignal".  We want to tokenize this out and
      // just get "observeSignal"
      CheetahStr text(veSynthDirectiveGetDirectiveText(pragma));
      StrToken tok(text);

      // at one time we only allowed the 'carbon' pragmas here, we would
      // filter them here but now we accept any pragma that uses
      // 'carbon' or any other prefix that the user has defined with
      // the command line switch: -pragma_prefix 
      {
        ++tok;
        if (!tok.atEnd()) {
          UtString keyword = *tok;
          if (keyword.compare(mCarbonContext->scLicenseStr) == 0)
          {
            bool startingPrePopWalkFlag = mCarbonContext->getInPrePopWalk();
            if (! isVSP)
            {
              mCarbonContext->putInPrePopWalk(false); // make messages visible
              CheetahStr fname(veNodeGetFileName(node));
              mCarbonContext->getMsgContext()->CmplrLicensedIPRequiresVSP(fname); // fatal
              mCarbonContext->putInPrePopWalk(startingPrePopWalkFlag);
            }
            ++tok;
            const char* featureName = *tok;
            INFO_ASSERT(strlen(featureName) > 0, "Zero length IP license feature name not allowed");

            if (mCarbonContext->mIPFeatures->getIntern(featureName) == NULL)
            {
              mCarbonContext->mIPFeatures->intern(featureName);
              UtCustomerDB* custdb = iodb->getCustDB();
              UtCustomerDB::Signature* custSig = custdb->getCustomerSignature();
              INFO_ASSERT(custSig, "Developer signature not yet set.");
              
              custSig->addIPFeature(featureName);     // remember that this feature is required for this ip

              // a license context must exist so we can check that the license feature is correct
              mCarbonContext->putInPrePopWalk(false); // make messages visible
              UtLicense* license = mCarbonContext->getLicense();
              mCarbonContext->getMsgContext()->CustomerRequireRuntimeLicense(mCarbonContext->getFileRoot(), featureName);
              
              // make sure a form of the license exists, so that the generated model could actually be used
              UtString licReason;
              if (! license->doesFeatureNameExist(featureName, &licReason) && ! mCarbonContext->hasDiagnosticsLicense()) {
                mCarbonContext->getMsgContext()->CustomerLicenseCheckFail(featureName, licReason.c_str());
              }
              mCarbonContext->putInPrePopWalk(startingPrePopWalkFlag);
            }
            continue;
          }

          const char* rest = tok.curPos();

          // Formulate a directive as if it came from the directives file,
          // taking the pragma text and appending on the name of the
          // object.  Thus if the user wrote:
          //    module foo;
          //       reg x;        // carbon observeSignal
          //       reg clk2;     // carbon collapseClock top.clk1
          //       reg scan_ena; // carbon tieNet 1'b0
          //
          // then we want to form the strings:
          //       "observeSignal foo.x"
          //       "collapseClock top.clk1 foo.clk2"
          //       "tieNet 1'b0 foo.scan_ena"
        
          UtString directive(rest);
          directive << " " << modName;
          if (netName != NULL)
            directive << "." << netName;

          // The interface to IODB::parseDirective involves passing
          // the StrToken context, in addition to the "rest" of the
          // string, so we must now recreate the same state that
          // the directives file parser is in when it calls parseDirective
          // for a semi-parsed line.
          StrToken dirTok(directive.c_str());
          ++dirTok;

          CheetahStr fname(veNodeGetFileName(node));
          SourceLocator loc = locs->create(fname, veNodeGetLineNumber(node));
          if (loc.isTicProtected() ){
            mCarbonContext->getMsgContext()->CarbonDirectiveInProtectedFile(fname, directive.c_str());
            continue;
          }
          iodb->parseDirective(keyword.c_str(), dirTok.curPos(), dirTok, loc);
        } // if
      } // if
    } // while
  } // if
  return status;
}

extern const char* gGetObjTypeStr(veObjType objType)
{
  const char* ret = "unknown_cheetah_type";

  switch (objType)
  {
  case UNSET_OBJECT:
    ret = "UNSET_OBJECT";
    break;
  case VE_BASE :
    ret  = "BASE ";
    break;
  case VE_NAMEDOBJECT:
    ret  = "NAMEDOBJECT";
    break;
  case VE_FILE:
    ret  = "FILE";
    break;
  case VE_MODULE:
    ret  = "MODULE";
    break;
  case VE_UDP:
    ret  = "UDP";
    break;
  case VE_TABLEENTRY:
    ret  = "TABLEENTRY";
    break;
  case VE_COMBINATIONALENTRY:
    ret  = "COMBINATIONALENTRY";
    break;
  case VE_SEQUENTIALENTRY:
    ret  = "SEQUENTIALENTRY";
    break;
  case VE_UDPINPUTLIST:
    ret  = "UDPINPUTLIST";
    break;
  case VE_UDPEDGEINPUTLIST:
    ret  = "UDPEDGEINPUTLIST";
    break;
  case VE_UDPLEVELINPUTLIST:
    ret  = "UDPLEVELINPUTLIST";
    break;
  case VE_EDGESYMBOL:
    ret  = "EDGESYMBOL";
    break;
  case VE_EDGESYMBOLTYPE1:
    ret  = "EDGESYMBOLTYPE1";
    break;
  case VE_EDGESYMBOLTYPE2:
    ret  = "EDGESYMBOLTYPE2";
    break;
  case VE_LEVELSYMBOL:
    ret  = "LEVELSYMBOL";
    break;
  case VE_OUTPUTSYMBOL:
    ret  = "OUTPUTSYMBOL";
    break;
  case VE_PORT:
    ret  = "PORT";
    break;
  case VE_NET:
    ret  = "NET";
    break;
  case VE_PARAM:
    ret  = "PARAM";
    break;
  case VE_VARIABLE:
    ret  = "VARIABLE";
    break;
  case VE_MEMORY:
    ret  = "MEMORY";
    break;
  case VE_INTEGER:
    ret  = "INTEGER";
    break;
  case VE_REAL:
    ret  = "REAL";
    break;
  case VE_REALTIME:
    ret  = "REALTIME";
    break;
  case VE_REG:
    ret  = "REG";
    break;
  case VE_TIME:
    ret  = "TIME";
    break;
  case VE_EVENT:
    ret  = "EVENT";
    break;
  case VE_RANGE:
    ret  = "RANGE";
    break;
  case VE_EXPR:
    ret  = "EXPR";
    break;
  case VE_OPERATOR:
    ret  = "OPERATOR";
    break;
  case VE_UNARY:
    ret  = "UNARY";
    break;
  case VE_BINARY:
    ret  = "BINARY";
    break;
  case VE_CONCAT:
    ret  = "CONCAT";
    break;
  case VE_MULTIPLECONCAT:
    ret  = "MULTIPLECONCAT";
    break;
  case VE_BITSELECT:
    ret  = "BITSELECT";
    break;
  case VE_PARTSELECT:
    ret  = "PARTSELECT";
    break;
  case VE_CONDOP:
    ret  = "CONDOP";
    break;
  case VE_LITERAL:
    ret  = "LITERAL";
    break;
  case VE_CONST:
    ret  = "CONST";
    break;
  case VE_STRING:
    ret  = "STRING";
    break;
  case VE_BASEDNUMBER:
    ret  = "BASEDNUMBER";
    break;
  case VE_EXPNUMBER:
    ret  = "EXPNUMBER";
    break;
  case VE_FLOATNUMBER:
    ret  = "FLOATNUMBER";
    break;
  case VE_DEFPARAM:
    ret  = "DEFPARAM";
    break;
  case VE_MINTYPMAXEXPR:
    ret  = "MINTYPMAXEXPR";
    break;
  case VE_INITALWAYS:
    ret  = "INITALWAYS";
    break;
  case VE_INITIAL:
    ret  = "INITIAL";
    break;
  case VE_ALWAYS:
    ret  = "ALWAYS";
    break;
  case VE_STATEMENT:
    ret  = "STATEMENT";
    break;
  case VE_ASSIGN:
    ret  = "ASSIGN";
    break;
  case VE_SPECIFYBLOCK:
    ret  = "SPECIFYBLOCK";
    break;
  case VE_SPECPARAM:
    ret  = "SPECPARAM";
    break;
  case VE_PATH:
    ret  = "PATH";
    break;
  case VE_LEVELPATH:
    ret  = "LEVELPATH";
    break;
  case VE_EDGEPATH:
    ret  = "EDGEPATH";
    break;
  case VE_SYSTIMINGCHECK:
    ret  = "SYSTIMINGCHECK";
    break;
  case VE_SYSTIMINGEVENT:
    ret  = "SYSTIMINGEVENT";
    break;
  case VE_TIMINGEVENTCONTROL:
    ret  = "TIMINGEVENTCONTROL";
    break;
  case VE_ROOT:
    ret  = "ROOT";
    break;
  case VE_ATTRIBUTEUSE:
    ret  = "ATTRIBUTEUSE";
    break;
  case VE_ATTRIBUTEDECL:
    ret  = "ATTRIBUTEDECL";
    break;
  case VE_INSTANCE:
    ret  = "INSTANCE";
    break;
  case VE_MODULEORUDPINSTANCE:
    ret  = "MODULEORUDPINSTANCE";
    break;
  case VE_GATEINSTANCE:
    ret  = "GATEINSTANCE";
    break;
  case VE_INSTANCEATTRIBUTE:
    ret  = "INSTANCEATTRIBUTE";
    break;
  case VE_ATTRIBUTECLASS:
    ret  = "ATTRIBUTECLASS";
    break;
  case VE_NAMEDOBJECTUSE:
    ret  = "NAMEDOBJECTUSE";
    break;
  case VE_SCOPEVARIABLE:
    ret  = "SCOPEVARIABLE";
    break;
  case VE_PORTCONNECTION:
    ret  = "PORTCONNECTION";
    break;
  case VE_CONTASSIGN:
    ret  = "CONTASSIGN";
    break;
  case VE_LIST:
    ret  = "LIST";
    break;
  case VE_LISTNODE:
    ret  = "LISTNODE";
    break;
  case VE_ITERATOR:
    ret = "ITERATOR";
    break;
  case VE_IF:
    ret  = "IF";
    break;
  case VE_FOREVER:
    ret  = "FOREVER";
    break;
  case VE_REPEAT:
    ret  = "REPEAT";
    break;
  case VE_WHILE:
    ret  = "WHILE";
    break;
  case VE_FOR:
    ret  = "FOR";
    break;
  case VE_WAIT:
    ret  = "WAIT";
    break;
  case VE_DELAY_OR_EVENT:
    ret  = "DELAY_OR_EVENT";
    break;
  case VE_CASEITEM:
    ret  = "CASEITEM";
    break;
  case VE_DISABLE:
    ret  = "DISABLE";
    break;
  case VE_FORCE:
    ret  = "FORCE";
    break;
  case VE_RELEASE:
    ret  = "RELEASE";
    break;
  case VE_DEASSIGN:
    ret  = "DEASSIGN";
    break;
  case VE_QUASI_CONT_ASSIGN:
    ret  = "QUASI_CONT_ASSIGN";
    break;
  case VE_SEQ_BLOCK:
    ret  = "SEQ_BLOCK";
    break;
  case VE_PAR_BLOCK:
    ret  = "PAR_BLOCK";
    break;
  case VE_TASK_ENABLE:
    ret  = "TASK_ENABLE";
    break;
  case VE_SYS_TASK_ENABLE:
    ret  = "SYS_TASK_ENABLE";
    break;
  case VE_FUNCTION_CALL:
    ret  = "FUNCTION_CALL";
    break;
  case VE_SYS_FUNC_CALL:
    ret  = "SYS_FUNC_CALL";
    break;
  case VE_REPEAT_OR_DELAY:
    ret  = "REPEAT_OR_DELAY";
    break;
  case VE_TASK:
    ret  = "TASK";
    break;
  case VE_NON_BLOCK_ASSIGN:
    ret  = "NON_BLOCK_ASSIGN";
    break;
  case VE_BLOCK_ASSIGN:
    ret  = "BLOCK_ASSIGN";
    break;
  case VE_FUNCTION:
    ret  = "FUNCTION";
    break;
  case VE_TFARG:
    ret  = "TFARG";
    break;
  case VE_CASE:
    ret  = "CASE";
    break;
  case VE_CASEZ:
    ret  = "CASEZ";
    break;
  case VE_CASEX:
    ret  = "CASEX";
    break;
  case VE_EVENT_TRIGGER:
    ret  = "EVENT_TRIGGER";
    break;
  case VE_EVENT_CONTROL:
    ret  = "EVENT_CONTROL";
    break;
  case VE_POSE_SCALAR_EVENT_EXPR:
    ret  = "POSE_SCALAR_EVENT_EXPR";
    break;
  case VE_NEG_SCALAR_EVENT_EXPR:
    ret  = "NEG_SCALAR_EVENT_EXPR";
    break;
  case VE_INTORTIMEARRAY:
    ret  = "INTORTIMEARRAY";
    break;
  case VE_SYNTHDIRECTIVE:
    ret  = "SYNTHDIRECTIVE";
    break;
  case VE_MEMBASE:
    ret  = "MEMBASE";
    break;
  case VE_SYMTABENTRY:
    ret  = "SYMTABENTRY";
    break;
  case VE_SYMTAB:
    ret  = "SYMTAB";
    break;
  case VE_TF:
    ret  = "TF";
    break;
  case VE_BLOCK:
    ret  = "BLOCK";
    break;
  case VE_EDGE_DESCRIPTOR:
    ret  = "EDGE_DESCRIPTOR";
    break;
  case VE_MACRO_PARAM:
    ret  = "MACRO_PARAM";
    break;
  case VE_MACRO_TEXT:
    ret  = "MACRO_TEXT";
    break;
  case VE_MACRO:
    ret  = "MACRO";
    break;
  case VE_MACROUSEINFO:
    ret  = "MACROUSEINFO";
    break;
  case VE_PROC_ASSIGN:
    ret  = "PROC_ASSIGN";
    break;
  case VE_MODULE_TEMPLATE:
    ret  = "MODULE_TEMPLATE";
    break;
  case VE_LIBRARY_FILE_TEMPLATE:
    ret  = "LIBRARY_FILE_TEMPLATE";
    break;
  case VE_TIMING_EVENT:
    ret  = "TIMING_EVENT";
    break;
  case VE_COMMANDLINE_FILE:
    ret  = "COMMANDLINE_FILE";
    break;
  case VE_COMMANDLINE:
    ret  = "COMMANDLINE";
    break;
  case VE_FLAG:
    ret  = "FLAG";
    break;
  case VE_DATAFLAG:
    ret  = "DATAFLAG";
    break;
  case VE_COMPILER_DIRECTIVE:
    ret  = "COMPILER_DIRECTIVE";
    break;
  case VE_SOURCEINFO:
    ret  = "SOURCEINFO";
    break;
  case VE_IFDEFEDOUT_RANGE:
    ret  = "IFDEFEDOUT_RANGE";
    break;
  case VE_NETBIT:
    ret  = "NETBIT";
    break;
  case VE_PORTBIT:
    ret  = "PORTBIT";
    break;
  case VE_TERMBIT:
    ret  = "TERMBIT";
    break;
  case VE_BASEDNUMBERBIT:
    ret  = "BASEDNUMBERBIT";
    break;
  case VE_STACK:
    ret  = "STACK";
    break;
  case VE_PROTECTED_REGION:
    ret  = "PROTECTED_REGION";
    break;
  case VE_COMMENT:
    ret  = "COMMENT";
    break;
  case VE_SCANOBJ:
    ret  = "SCANOBJ";
    break;
  case VE_GENSYMTAB:
    ret  = "GENSYMTAB";
    break;
  case VE_GENSYMTABENTRY:
    ret  = "GENSYMTABENTRY";
    break;
  case VE_VALUEOBJECT:
    ret  = "VALUEOBJECT";
    break;
  case VE_GENVAR:
    ret  = "GENVAR";
    break;
  case VE_GENERATE:
    ret  = "GENERATE";
    break;
  case VE_GENERATE_BLOCK:
    ret  = "GENERATE_BLOCK";
    break;
  case VE_GENERATE_FOR:
    ret  = "GENERATE_FOR";
    break;
  case VE_GENERATE_IF:
    ret  = "GENERATE_IF";
    break;
  case VE_GENERATE_CASE:
    ret  = "GENERATE_CASE";
    break;
  case VE_GENERATE_CASE_ITEM :
    ret  = "GENERATE_CASE_ITEM ";
    break;
  case VE_USERATTRIBUTEBLOCK:
    ret  = "USERATTRIBUTEBLOCK";
    break;
  case VE_LIBRARY:
    ret  = "LIBRARY";
    break;
  case VE_LIBRARYMANAGER:
    ret  = "LIBRARYMANAGER";
    break;
  case VE_CONFIG_LIBRARY:
    ret  = "CONFIG_LIBRARY";
    break;
  case VE_CONFIG_LIBMAP_MANAGER:
    ret  = "CONFIG_LIBMAP_MANAGER";
    break;
  case VE_LIBINFO:
    ret  = "LIBINFO";
    break;
  case VE_INSTINFO:
    ret  = "INSTINFO";
    break;
  case VE_HASHARRAY:
    ret  = "HASHARRAY";
    break;
  case VE_USERMETACOMMENT:
    ret  = "USERMETACOMMENT";
    break;
  case VE_ARRAY:
    ret  = "ARRAY";
    break;
  case VE_ARRAYBITSELECT:
    ret  = "ARRAYBITSELECT";
    break;
  case VE_ARRAYPARTSELECT:
    ret  = "ARRAYPARTSELECT";
    break;
  case VE_LOCALPARAM:
    ret  = "LOCALPARAM";
    break;
  case VE_PARAMCONN:
    ret  = "PARAMCONN";
    break;
  case VE_CONFIG:
    ret  = "CONFIG";
    break;
  case VE_CONFIG_INSTANCE:
    ret  = "CONFIG_INSTANCE";
    break;
  case VE_CONFIG_CELL:
    ret  = "CONFIG_CELL";
    break;
  case VE_USERATTRIBUTE:
    ret  = "USERATTRIBUTE";
    break;
  case VE_ATTRTABLE:
    ret  = "ATTRTABLE";
    break;
  case VE_WANTEDINSTINFO:
    ret  = "WANTEDINSTINFO";
    break;
  case VE_PULSESTYLE:
    ret  = "PULSESTYLE";
    break;
  case VE_SHOWCANCELLED:
    ret  = "SHOWCANCELLED";
    break;
  case VE_ATTRINSTANCE:
    ret  = "ATTRINSTANCE";
    break;
  case VE_ATTRSPEC:
    ret  = "ATTRSPEC";
    break;
  case VE_INTEGERSTACK:
    ret  = "INTEGERSTACK";
    break;
  case VE_NETARRAY:
    ret  = "NETARRAY";
    break;
  case VE_PHYSICALLIBRARY:
    ret  = "PHYSICALLIBRARY";
    break;
  case VE_LISTSYMTAB:
    ret  = "LISTSYMTAB";
    break;
  case VE_SYNTHPREFIX:
    ret  = "SYNTHPREFIX";
    break;
  case VE_LINEFILEDIRECTIVE:
    ret  = "LINEFILEDIRECTIVE";
    break;
  case VE_CHARSTRING:
    ret  = "CHARSTRING";
    break;
  case VE_IDENTIFIER:
    ret = "VE_IDENTIFIER";
    break;
  case VE_USERPRAGMACOMMENT:
    ret = "VE_USERPRAGMACOMMENT";
    break;
  case VE_USERPRAGMACOMMENTMANAGER:
    ret = "VE_USERPRAGMACOMMENTMANAGER";
    break;
  case VE_CONFIGINFO:
    ret = "VE_CONFIGINFO";
    break;
  case VE_END_UNSET_OBJECT:
    ret = "VE_END_UNSET_OBJECT";
    break;
  case VE_GENERATEBLOCKGROUP:
    ret = "VE_GENERATEBLOCKGROUP";
    break;
  case VE_LARGESOURCEINFO:
    ret = "VE_LARGESOURCEINFO";
    break;
  }
  return ret;
}

void gCheetahAssert(const char* msg, const char* type, veNode obj)
{
  if (obj != NULL)
    UtIO::cout() << veNodeGetFileName(obj) << ":" << (UInt32)veNodeGetLineNumber(obj) << ": ";
  UtIO::cout() << msg << " - " << type << UtIO::endl;
  veFreeLicense();
  INFO_ASSERT(0, "Parse failed.");
}

void gPrintObjType(veNode node)
{
  veObjType type = veNodeGetObjType(node);
  veNodeDecompile(node, stdout);
  UtIO::cout() << ": " << gGetObjTypeStr(type) << UtIO::endl << UtIO::flush;
}


void CheetahContext::putInPrePopWalk(bool prePopWalk)
{
  if (prePopWalk)
    veRegisterMessageHandler(sCheetahPrepopSpew);    
  else
    veRegisterMessageHandler(sCheetahSpew);
}

