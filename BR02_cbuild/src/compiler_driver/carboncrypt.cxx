/* -*- C++ -*- */
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/OSWrapper.h"
#include "util/Zstream.h"
#include "util/ArgProc.h"
#include "util/UtLicense.h"
#include "util/UtLicenseMsg.h"
#include <stdio.h>
#include <errno.h>

struct LicCB : public UtLicense::MsgCB
{
  CARBONMEM_OVERRIDES

  LicCB()
  {}

  virtual ~LicCB() {}

  virtual void waitingForLicense(const char*) {}
  virtual void queuedLicenseObtained(const char*) {}
  virtual void requeueLicense(const char*) {}
  virtual void relinquishLicense(const char*) {}
  virtual void exitNow(const char* reason)
  {
    fprintf(stderr, "%s\n", reason);
    exit(1);
  }
};

int main(int argc, char* argv[])
{
  LicCB licCB;
  UtLicense license(&licCB);
  {
    UtString licReason;
    if (! license.checkout(UtLicense::eDIAGNOSTICS, &licReason))
    {
      fprintf(stderr, "%s\n", licReason.c_str());
      return 1;
    }
  }
  
  int stat = 0;
  ArgProc argproc;
  argproc.setDescription("Carbon Encryptor/Decryptor",
                         "carboncrypt",
                         "Encrypts or decrypts an input file and outputs it to an output file. If encrypting, the output file is required. If decrypting it is optional. For decrypting, if no output file is given the decrypted file is printed to the screen.");
  
  {
    UtString synop;
    synop << "carboncrypt <input-file> <output-file>  - This encrypts the input file ";
    argproc.addSynopsis(synop);
    synop.erase();
    synop << "carboncrypt -d <input-file> [output-file]  - This decrypts input file";
    argproc.addSynopsis(synop);
    argproc.createSection("General");
    argproc.addBool("-d", "Decrypt the input file.", false, 1);
    argproc.addToSection("General", "-d");
  }
  
  if (argc < 3)
  {
    UtString usage;
    argproc.getUsageVerbose(&usage);
    fprintf(stdout, "%s\n\n", usage.c_str());
    return 0;
  }

  UtString errMsg;
  int numOptions;
  if (argproc.parseCommandLine(&argc, argv, &numOptions, &errMsg) != ArgProc::eParsed)
  {
    UtString usageBrief;
    argproc.getSynopsis(&usageBrief);
    fprintf(stderr, "%s\n\n", usageBrief.c_str());
    fprintf(stderr, "%s\n", errMsg.c_str());
    return 1;
  }


  static const int cBufSize = 65536;
  char buf[cBufSize];
  UInt32 numRead;

  if (argproc.getBoolValue("-d"))
  {
    // decompress
    if ((argc < 2) || (argc > 3))
    {
      argproc.getSynopsis(&errMsg);
      fprintf(stderr, "%s\n", errMsg.c_str());
      return 1;
    }
    
    FILE* out = stdout;
    bool usingOut = true;
    if (argc == 3)
    {
      UtString reason;
      out = OSFOpen(argv[2], "w", &reason);
      if (! out)
      {
        fprintf(stderr, "%s\n", reason.c_str());
        return 1;
      }
      usingOut = false;
    }

    ZISTREAM(in, argv[1]);
    while (! in.eof() && ! in.fail())
    {
        numRead = in.read(buf, cBufSize);
        fwrite(buf, 1, numRead, out);
    }
    
    if (! usingOut)
      fclose(out);
    else
      fflush(out);
    
    if (in.fail())
    {
      fprintf(stderr, "%s\n", in.getError()); 
      stat = 1;
    }
  }
  else
  {
    // compress
    if (argc != 3)
    {
      argproc.getSynopsis(&errMsg);
      fprintf(stderr, "%s\n", errMsg.c_str());
      return 1;
    }
  
    ZISTREAM(in, argv[1]);
    ZOSTREAM(out, argv[2]);
    
    if (! in)
    {
      fprintf(stderr, "%s\n", in.getError());
      stat = 1;
    }
    
    if (! out)
    {
      fprintf(stderr, "%s\n", out.getError());
      stat = 1;
    }
    
    if (stat != 0)
      return stat;
    
    while (! in.eof() && ! in.fail() && ! out.fail())
    {
      numRead = in.read(buf, cBufSize);
      out.write(buf, numRead);
    }
    
    if (in.fail())
    {
      fprintf(stderr, "%s\n", in.getError());
      stat = 1;
    }
    
    if (out.fail())
    {
      fprintf(stderr, "%s\n", out.getError());
      stat = 1;
    }
  }

  return stat;
}
