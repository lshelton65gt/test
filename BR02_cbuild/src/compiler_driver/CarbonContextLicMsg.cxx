//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "compiler_driver/CarbonContext.h"
#include "util/UtLicense.h"
#include "util/UtLicenseMsg.h"

// call back mechanism for licensing
class CarbonContext::LicCB : public virtual UtLicense::MsgCB
{
public:
  virtual ~LicCB();

  LicCB(CarbonContext* carbonContext);

  virtual void waitingForLicense(const char* feature);
  virtual void queuedLicenseObtained(const char* feature);

  virtual void requeueLicense(const char* featureName);
  virtual void relinquishLicense(const char* featureName);
  virtual void exitNow(const char* reason);

private:
  CarbonContext* mCarbonContext;

  //forbid
  LicCB();
  LicCB(const LicCB&);
  LicCB& operator=(const LicCB&);
};


CarbonContext::LicCB::~LicCB()
{}
    
CarbonContext::LicCB::LicCB(CarbonContext* carbonContext) : mCarbonContext(carbonContext)
{}

void CarbonContext::LicCB::waitingForLicense(const char* feature)
{
  MsgContext* msgContext = mCarbonContext->getMsgContext();
  msgContext->CmplrQueuingLicense(feature);
}

void CarbonContext::LicCB::queuedLicenseObtained(const char* feature)
{
  MsgContext* msgContext = mCarbonContext->getMsgContext();
  msgContext->CmplrQueuedLicenseObtained(feature);
}

void CarbonContext::LicCB::requeueLicense(const char* feature)
{
  MsgContext* msgContext = mCarbonContext->getMsgContext();
  msgContext->CmplrRequeuingLicense(feature);
}

void CarbonContext::LicCB::relinquishLicense(const char* feature)
{
  MsgContext* msgContext = mCarbonContext->getMsgContext();
  msgContext->CmplrRelinquishingLicense(feature);
}

void CarbonContext::LicCB::exitNow(const char* reason)
{
  mCarbonContext->doLicenseExit(reason);
  // not reached
}

CarbonContext::LicCB* CarbonContext::createLicenseCB(CarbonContext* cc, UtLicense** lic) {
  LicCB* lcb = new CarbonContext::LicCB(cc);
  *lic = new UtLicense(lcb, false);
  return lcb;
}
void CarbonContext::destroyLicenseCB(CarbonContext::LicCB* lcb) {
  delete lcb;
}
