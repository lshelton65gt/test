#include "util/Stats.h"

static StatsProgressRecord sCbuildStats[] = {
  { "CmdLine", 	0.000476476561974933 }, 	// [0], 197 samples
  { "PrintBanner", 	0.000479752732559429 }, 	// [1], 195 samples
  { "AllocateGlobals", 	0.00109774563785984 }, 	// [2], 195 samples
  { "ParseDirectives", 	0.0011017881951324 }, 	// [3], 195 samples
  { "LangParse", 	0.0531581586267068 }, 	// [4], 195 samples
  { "PopulateNucleus", 	0.0594146696553653 }, 	// [5], 195 samples
  { "DumpDesignPopulate", 	0.0608250558419348 }, 	// [6], 195 samples
  { "FindBlobs", 	0.0609777053423826 }, 	// [7], 195 samples
  { "evalDesignDirectives", 	0.0611132310517047 }, 	// [8], 195 samples
  { "PostPopulateResynth", 	0.0640233903463675 }, 	// [9], 195 samples
  { "LocalHR", 	0.0811804478722639 }, 	// [10], 143 samples
  { "BlastNets", 	0.0189338235294118 }, 	// [11], 1 samples
  { "TieNets", 	0.0811982993031794 }, 	// [12], 143 samples
  { "TFRewrite", 	0.0815465125772996 }, 	// [13], 143 samples
  { "EarlyFlatModuleAnalysis", 	0.0649107637993267 }, 	// [14], 195 samples
  { "EarlyFlatteningAnalysis", 	0.066174741301192 }, 	// [15], 195 samples
  { "GO ModuleAnalysis", 	0.0881575902361246 }, 	// [16], 195 samples
  { "MinAnalysis", 	0.095891423955866 }, 	// [17], 195 samples
  { "Reachable", 	0.101512493222001 }, 	// [18], 195 samples
  { "GO Prep", 	0.101513701612098 }, 	// [19], 195 samples
  { "Aliases", 	0.116081160214766 }, 	// [20], 195 samples
  { "ElabConsts", 	0.149331041217278 }, 	// [21], 195 samples
  { "UnelabConsts", 	0.150252942698665 }, 	// [22], 195 samples
  { "PortConsts", 	0.15027629759294 }, 	// [23], 195 samples
  { "OptConsts", 	0.153651858164427 }, 	// [24], 195 samples
  { "GO ConstProp", 	0.154053910741254 }, 	// [25], 195 samples
  { "RemEFlow", 	0.154373491048205 }, 	// [26], 195 samples
  { "RemUEFlow", 	0.154483469472776 }, 	// [27], 195 samples
  { "GO Cleanup", 	0.154726437793697 }, 	// [28], 195 samples
  { "GlobOpts", 	0.15679599851236 }, 	// [29], 195 samples
  { "Coerce Lower", 	0.167859338748883 }, 	// [30], 138 samples
  { "PS: Traverse", 	0.184785873782675 }, 	// [31], 138 samples
  { "PS: Actual/Formal Population", 	0.184829667782182 }, 	// [32], 138 samples
  { "PS: HierRef Population", 	0.184843802713477 }, 	// [33], 138 samples
  { "PS: Primary IO", 	0.184849055251752 }, 	// [34], 138 samples
  { "PS: Depth", 	0.184895331439875 }, 	// [35], 138 samples
  { "PS: Process", 	0.185002099701893 }, 	// [36], 138 samples
  { "PS: Discovery", 	0.185005342872911 }, 	// [37], 138 samples
  { "PS: Rewrite", 	0.185052996888962 }, 	// [38], 138 samples
  { "Port Split", 	0.185053296227065 }, 	// [39], 138 samples
  { "Coerce Update", 	0.202086977536943 }, 	// [40], 138 samples
  { "PortCoercion", 	0.202858723632762 }, 	// [41], 138 samples
  { "PortAnalysis", 	0.192776337738493 }, 	// [42], 195 samples
  { "ModuleAnalysis", 	0.389683731636022 }, 	// [43], 195 samples
  { "Analysis", 	0.390088244533787 }, 	// [44], 195 samples
  { "PostFlattenAnalysis", 	0.431737696430003 }, 	// [45], 195 samples
  { "Post Analysis", 	0.440047725256076 }, 	// [46], 195 samples
  { "STConstBits", 	0.447713519198852 }, 	// [47], 195 samples
  { "ResolveDirectives", 	0.447818846252668 }, 	// [48], 195 samples
  { "ResolveInaccurate", 	0.447981329776954 }, 	// [49], 195 samples
  { "STDesignPorts", 	0.44798320026966 }, 	// [50], 195 samples
  { "ElabAnalysis", 	0.449431115670556 }, 	// [51], 195 samples
  { "DBGPrint", 	0.449596009130639 }, 	// [52], 195 samples
  { "WalkAliases", 	0.459846620631197 }, 	// [53], 195 samples
  { "DeadPortFixup", 	0.4598498864103 }, 	// [54], 195 samples
  { "Alias", 	0.460021984486517 }, 	// [55], 195 samples
  { "Allocation", 	0.466901451253186 }, 	// [56], 195 samples
  { "HierAlias", 	0.466902541560285 }, 	// [57], 195 samples
  { "HierResCleanup", 	0.467771109884544 }, 	// [58], 195 samples
  { "MarkNonTriInit", 	0.476407217125741 }, 	// [59], 195 samples
  { "PreReadWrite", 	0.476737027310988 }, 	// [60], 195 samples
  { "MakeAllInit", 	0.476741351948102 }, 	// [61], 195 samples
  { "UnelabUpdate", 	0.476829617274194 }, 	// [62], 195 samples
  { "ElabUpdate", 	0.476833222697514 }, 	// [63], 195 samples
  { "PostReadWrite", 	0.477208156201349 }, 	// [64], 195 samples
  { "TriInit", 	0.477221124543278 }, 	// [65], 195 samples
  { "Collect", 	0.482661232110772 }, 	// [66], 195 samples
  { "Propagate", 	0.482770451186968 }, 	// [67], 195 samples
  { "TriFlagPropagate", 	0.482798100653357 }, 	// [68], 195 samples
  { "DeadNets", 	0.485432080534985 }, 	// [69], 195 samples
  { "IODBResolveForce", 	0.4855158029553 }, 	// [70], 195 samples
  { "Reduce", 	0.48707500918239 }, 	// [71], 195 samples
  { "Latch Split", 	0.489890918180972 }, 	// [72], 195 samples
  { "Latch Cond Rewrite", 	0.490012973676537 }, 	// [73], 195 samples
  { "SCHLatchPrepare", 	0.49001370518044 }, 	// [74], 195 samples
  { "DepNets", 	0.49046701271945 }, 	// [75], 195 samples
  { "Latch Find", 	0.490487079421851 }, 	// [76], 195 samples
  { "Constr Trees", 	0.4907518257318 }, 	// [77], 195 samples
  { "Elab Trees", 	0.490752396610114 }, 	// [78], 195 samples
  { "Cntrl Trees", 	0.490754288576197 }, 	// [79], 195 samples
  { "Latch Excl", 	0.491936388809852 }, 	// [80], 195 samples
  { "Min Cntrl Trees", 	0.49193677932221 }, 	// [81], 195 samples
  { "Rej Split", 	0.491937348201683 }, 	// [82], 195 samples
  { "Edge Sets", 	0.491938904632621 }, 	// [83], 195 samples
  { "Latch Xform", 	0.493063032985126 }, 	// [84], 195 samples
  { "SCHLatchConvert", 	0.4930917852147 }, 	// [85], 195 samples
  { "CD First Pass", 	0.495581188064208 }, 	// [86], 195 samples
  { "CD TaskInline", 	0.493041717442349 }, 	// [87], 69 samples
  { "CD Task Pass", 	0.447890861108465 }, 	// [88], 5 samples
  { "CDS Discovery", 	0.494142852895359 }, 	// [89], 69 samples
  { "CDS Unelab Convert", 	0.494145712444996 }, 	// [90], 69 samples
  { "CDS Nucleus XForm", 	0.494331260448257 }, 	// [91], 69 samples
  { "CDS Recreate", 	0.499103228363131 }, 	// [92], 38 samples
  { "CD Splitting", 	0.506388953511918 }, 	// [93], 69 samples
  { "CD Last Pass", 	0.508248704551505 }, 	// [94], 69 samples
  { "CD Scheduling", 	0.51364129805575 }, 	// [95], 57 samples
  { "SCHCycle", 	0.504144233517177 }, 	// [96], 195 samples
  { "Ports1", 	0.504166169328618 }, 	// [97], 195 samples
  { "CAIDClkData", 	0.506732641931967 }, 	// [98], 195 samples
  { "KMA_keepMastersAlive", 	0.506732696475811 }, 	// [99], 195 samples
  { "KMA_preKeepSanity", 	0.509224725511131 }, 	// [100], 195 samples
  { "KMAmark", 	0.509230007471067 }, 	// [101], 195 samples
  { "KMAud", 	0.510754527519202 }, 	// [102], 118 samples
  { "KMAflow", 	0.510978576783822 }, 	// [103], 118 samples
  { "KMAcleanup", 	0.514803196507941 }, 	// [104], 195 samples
  { "CAAliveMaster", 	0.514803964716099 }, 	// [105], 195 samples
  { "CAaliasClk", 	0.516938530784973 }, 	// [106], 181 samples
  { "CAMkClkandDP", 	0.521281316718989 }, 	// [107], 195 samples
  { "ClockData", 	0.521281316718989 }, 	// [108], 195 samples
  { "Live1", 	0.521698697078696 }, 	// [109], 195 samples
  { "Flops", 	0.522134967490555 }, 	// [110], 195 samples
  { "Ports2", 	0.52215291873073 }, 	// [111], 195 samples
  { "Live2", 	0.522562544754031 }, 	// [112], 195 samples
  { "DeadClks", 	0.522563901796329 }, 	// [113], 195 samples
  { "ForceDep", 	0.522879669808671 }, 	// [114], 195 samples
  { "MultStr", 	0.522907784097623 }, 	// [115], 195 samples
  { "NonConstRst", 	0.52394353961437 }, 	// [116], 195 samples
  { "SCHMark", 	0.523944131356355 }, 	// [117], 195 samples
  { "SCHSanity", 	0.525617680898198 }, 	// [118], 195 samples
  { "FCD Create", 	0.5260762092616 }, 	// [119], 195 samples
  { "FCD Break 1", 	0.527395382186316 }, 	// [120], 195 samples
  { "FCD Break 2", 	0.528717311046967 }, 	// [121], 195 samples
  { "FCD Split", 	0.53488698355367 }, 	// [122], 195 samples
  { "SCHFCycle", 	0.534891973247681 }, 	// [123], 195 samples
  { "SCHFlowInputNets", 	0.537637025755259 }, 	// [124], 195 samples
  { "MarkStorage", 	0.537702227566116 }, 	// [125], 195 samples
  { "MarkClock", 	0.537703448729478 }, 	// [126], 195 samples
  { "TransMask", 	0.539077331219264 }, 	// [127], 195 samples
  { "SampleMask", 	0.540835646106885 }, 	// [128], 195 samples
  { "SCHTiming", 	0.540954104069789 }, 	// [129], 195 samples
  { "SCHGenCDir", 	0.54187108029803 }, 	// [130], 195 samples
  { "DCS Find", 	0.542055329032894 }, 	// [131], 195 samples
  { "DCS Split", 	0.544804993704451 }, 	// [132], 195 samples
  { "DCS Mark", 	0.544814252860058 }, 	// [133], 195 samples
  { "DCS Calc", 	0.544814313557496 }, 	// [134], 195 samples
  { "DCO Graph", 	0.546898222462572 }, 	// [135], 195 samples
  { "DCO BrkCycle", 	0.546900559704603 }, 	// [136], 195 samples
  { "DCO Print", 	0.422699848135493 }, 	// [137], 3 samples
  { "DCO Merge", 	0.546903586856334 }, 	// [138], 195 samples
  { "DCO Sort", 	0.546966815013882 }, 	// [139], 195 samples
  { "DCS Order", 	0.546967313817999 }, 	// [140], 195 samples
  { "DCS Schedule", 	0.54697496377 }, 	// [141], 195 samples
  { "ClkPI Fix", 	0.555682549606503 }, 	// [142], 195 samples
  { "DCO Dump", 	0.555690369167514 }, 	// [143], 195 samples
  { "DCS Verify", 	0.555690502379864 }, 	// [144], 195 samples
  { "DCL Creat", 	0.555690564603281 }, 	// [145], 195 samples
  { "Comb Accurate", 	0.55959263441456 }, 	// [146], 195 samples
  { "Seq Sched", 	0.559700648335246 }, 	// [147], 195 samples
  { "Seq Order", 	0.560099060730749 }, 	// [148], 195 samples
  { "Seq Multi", 	0.560189690322515 }, 	// [149], 195 samples
  { "Seq Sanity", 	0.560706164653397 }, 	// [150], 195 samples
  { "State Upd1", 	0.573608229557941 }, 	// [151], 195 samples
  { "MB ADepth", 	0.573092857505068 }, 	// [152], 188 samples
  { "Seq Merge", 	0.606167347140057 }, 	// [153], 195 samples
  { "Mixed Merge", 	0.661599626575284 }, 	// [154], 195 samples
  { "State Upd2", 	0.676376416423154 }, 	// [155], 195 samples
  { "Seq Depth2", 	0.676981994155281 }, 	// [156], 195 samples
  { "Seq Create", 	0.676995946624213 }, 	// [157], 195 samples
  { "Comb Creat", 	0.677853289638274 }, 	// [158], 195 samples
  { "Comb MSched", 	0.68886840416297 }, 	// [159], 195 samples
  { "SchedSort", 	0.689151879425208 }, 	// [160], 195 samples
  { "Node Merge", 	0.690465085742756 }, 	// [161], 195 samples
  { "MB NetDepth", 	0.692952476538183 }, 	// [162], 188 samples
  { "MB Depth", 	0.694335927019813 }, 	// [163], 188 samples
  { "MB RDepth", 	0.69439859439345 }, 	// [164], 188 samples
  { "Blk Merge", 	0.725955266003948 }, 	// [165], 195 samples
  { "Blk Sort", 	0.726470239418424 }, 	// [166], 195 samples
  { "Comb Opt", 	0.726474789757715 }, 	// [167], 195 samples
  { "Seq Sort", 	0.726487063818167 }, 	// [168], 195 samples
  { "Seq Inactive", 	0.72648759830643 }, 	// [169], 195 samples
  { "Seq Clk Verify", 	0.726488317184793 }, 	// [170], 195 samples
  { "Seq Finish", 	0.726488622871793 }, 	// [171], 195 samples
  { "Mark PI", 	0.727125375363166 }, 	// [172], 195 samples
  { "ClearNets", 	0.727268340871197 }, 	// [173], 195 samples
  { "SCHCreate", 	0.727268375829717 }, 	// [174], 195 samples
  { "Schedule", 	0.72824870689066 }, 	// [175], 195 samples
  { "MarkConstants", 	0.728947358735663 }, 	// [176], 195 samples
  { "TaskParamLower", 	0.735821786800987 }, 	// [177], 195 samples
  { "StripMine", 	0.737062619454218 }, 	// [178], 194 samples
  { "ReorderForPackAssigns", 	0.760799261545026 }, 	// [179], 194 samples
  { "PackAssigns", 	0.761129777728684 }, 	// [180], 194 samples
  { "ReorderForLocality", 	0.770728103137572 }, 	// [181], 195 samples
  { "BETransform", 	0.770800935582085 }, 	// [182], 195 samples
  { "LocalCSE", 	0.774147665559656 }, 	// [183], 195 samples
  { "REMemory", 	0.774215330047174 }, 	// [184], 195 samples
  { "Backend", 	0.781746468030904 }, 	// [185], 195 samples
  { "CG Write", 	0.797457627248324 }, 	// [186], 195 samples
  { "CodeGen", 	0.797764189359517 }, 	// [187], 195 samples
  { "DBWrite", 	0.819088577224049 }, 	// [188], 195 samples
  { "BackendCompile", 	1 }, 	// [189], 197 samples
  { NULL, 0}
};

