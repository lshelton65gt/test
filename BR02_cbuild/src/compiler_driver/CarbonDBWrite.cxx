// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/CbuildMsgContext.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAliasDB.h"
#include "codegen/codegen.h"
#include "util/Zstream.h"
#include "schedule/Schedule.h"
#include "iodb/ScheduleFactory.h"
#include "util/UtIOStream.h"
#include "util/DynBitVector.h"
#include "util/UtHashSet.h"
#include "util/UtArray.h"
#include "util/UtConv.h"
#include "util/AtomicCache.h"
#include "exprsynth/SymTabExpr.h"
#include "iodb/IODB.h"
#include "iodb/IODBUserTypes.h"

/*!
  \file
  DB Write auxiliary classes.

  If the field writing changes, change CarbonDBRead.cxx
*/

class CbuildSymTabBOM::CbuildElabInfo
{
public:
  CbuildElabInfo();
  ~CbuildElabInfo();

  NUBase* getNucleusObj() {
    const CbuildElabInfo* me = const_cast<const CbuildElabInfo*>(this);
    return const_cast<NUBase*>(me->getNucleusObj());
  }
  
  const NUBase* getNucleusObj() const;

  const IODBIntrinsic* getIntrinsic() const;
  
  CarbonIdent* getIdent() {
    const CbuildElabInfo* me = const_cast<const CbuildElabInfo*>(this);
    return const_cast<CarbonIdent*>(me->getIdent());
  }

  const CarbonIdent* getIdent() const;

  const UserType* getUserType() const;

  void putNucleusObj(NUBase* base);
  
  void putIntrinsic(const IODBIntrinsic* intrinsic);
  
  void putIdent(CarbonIdent* ident);

  void putUserType(const UserType* ut);

  int getTypeTag() const;
  UInt32 getTypeIndex() const;
  
  SInt32 getStorageOffset() const;
  
  const DynBitVector* getConstantVal() const;

  SCHSignature* getSCHSignature() {
    const CbuildElabInfo* me = const_cast<const CbuildElabInfo*>(this);
    return const_cast<SCHSignature*>(me->getSCHSignature());
  }

  const SCHSignature* getSCHSignature() const;


  void putTypeIndex(UInt32 typeIndex);
  void putTypeTag(int typeTag);
  void putStorageOffset(SInt32 offset);
  void putConstantVal(const DynBitVector* val);
  void putSCHSignature(const SCHSignature* signature);

  bool isTypeTagValid() const;
  bool isTypeIndexValid() const;

  //! print contents to stdout
  void print() const;

  CbuildShellDB::NodeFlags getNodeFlags() const { return mStoreInfo.mNodeFlags; }

  void markRunDebugSchedule()
  {
    mStoreInfo.mNodeFlags = CbuildShellDB::NodeFlags(mStoreInfo.mNodeFlags | CbuildShellDB::eRunDebugSched);
  }

  void markForceSubordinate()
  {
    mStoreInfo.mNodeFlags = CbuildShellDB::NodeFlags(mStoreInfo.mNodeFlags | CbuildShellDB::eForceSubord);
  }

  bool isForceSubordinate() const
  {
    return (mStoreInfo.mNodeFlags & CbuildShellDB::eForceSubord) != 0;
  }
  
  void markDepositable()
  {
    mStoreInfo.mNodeFlags = CbuildShellDB::NodeFlags(mStoreInfo.mNodeFlags | CbuildShellDB::eDepositable);
  }

  void markObservable()
  {
    // Paranoia. If this is an invalid wave net, don't mark it
    if (! isInvalidWaveNet())
      mStoreInfo.mNodeFlags = CbuildShellDB::NodeFlags(mStoreInfo.mNodeFlags | CbuildShellDB::eObservable);
    
  }
  
  void markInvalidWaveNet()
  {
    // remove any observability
    mStoreInfo.mNodeFlags = CbuildShellDB::NodeFlags(mStoreInfo.mNodeFlags & ~CbuildShellDB::eObservable);
    // Mark it it as an invalid wave net.
    mStoreInfo.mNodeFlags = CbuildShellDB::NodeFlags(mStoreInfo.mNodeFlags | CbuildShellDB::eInvalidWaveNet);
  }

  void markClockNet()
  {
    mStoreInfo.mNodeFlags = CbuildShellDB::NodeFlags(mStoreInfo.mNodeFlags | CbuildShellDB::eClock);
  }

  void markStateOutput()
  {
    mStoreInfo.mNodeFlags = CbuildShellDB::NodeFlags(mStoreInfo.mNodeFlags | CbuildShellDB::eStateOutput);
  }

  void markComboRun(bool isComboRun)
  {
    if (isComboRun)
      mStoreInfo.mNodeFlags = CbuildShellDB::NodeFlags(mStoreInfo.mNodeFlags | CbuildShellDB::eComboRun);
    else
      mStoreInfo.mNodeFlags = CbuildShellDB::NodeFlags(mStoreInfo.mNodeFlags & ~CbuildShellDB::eComboRun);
  }

  bool isComboRun() const
  {
    return ((mStoreInfo.mNodeFlags & CbuildShellDB::eComboRun) != 0);
  }
 
  bool isInvalidWaveNet() const
  {
    return ((mStoreInfo.mNodeFlags & CbuildShellDB::eInvalidWaveNet) != 0);
  }

  bool isObservable() const
  {
    return ((mStoreInfo.mNodeFlags & CbuildShellDB::eObservable) != 0);
  }
  
  bool isClockNet() const
  {
    return ((mStoreInfo.mNodeFlags & CbuildShellDB::eClock) != 0);
  }

  bool isStateOutput() const
  {
    return ((mStoreInfo.mNodeFlags & CbuildShellDB::eStateOutput) != 0);
  }

  void copy(CbuildElabInfo* dstBom) const
  {
    dstBom->mElabInfo = mElabInfo;
    copyStorageInfo(dstBom);
  }
  
  void copyStorageInfo(CbuildElabInfo* dstBom) const
  {
    dstBom->mStoreInfo = mStoreInfo;
  }

private:
  
  struct ElaborateInfo
  {
    ElaborateInfo() : mNucleusObj(NULL),
                      mIntrinsic(NULL),
                      mNodeIdent(NULL),
                      mUserType(NULL)
    {}

    NUBase* mNucleusObj;
    const IODBIntrinsic *mIntrinsic;
    CarbonIdent* mNodeIdent;
    const UserType* mUserType;

    // using default operator=
  };

  ElaborateInfo mElabInfo;


  union OffsetConstant
  {
    SInt32 mStorageOffset;
    const DynBitVector* mConstVal;
  };

  struct StoreInfo
  {
    StoreInfo();

    UInt32 mTagTypeIndex;
    OffsetConstant mOffsetConstant;
    const SCHSignature* mSignature;
    CbuildShellDB::NodeFlags mNodeFlags;

    // using default operator=
  };

  StoreInfo mStoreInfo;

  CbuildElabInfo(const CbuildElabInfo&);
  CbuildElabInfo& operator=(const CbuildElabInfo&);
};

//! Bill of Materials for cbuild's symboltable branches
class CbuildSymTabBOM::CbuildBranchDataBOM
{
public:
  //! constructor
  CbuildBranchDataBOM();

  //! destructor
  ~CbuildBranchDataBOM();

  //! print contents to stdout
  void print() const;

  //! Place the nucleus object
  void putNucleusObj(NUBase* base);
  
  //! Get modifiable nucleus object
  NUBase* getNucleusObj();

  //! Get const nucleus object
  const NUBase* getNucleusObj() const;

  void putUserType(const UserType* ut);

  const UserType* getUserType() const;

  //! Copy contents to dst
  void copy(CbuildBranchDataBOM* dstBom) const
  {
    dstBom->mNucleusObj = mNucleusObj;
    dstBom->mUserType   = mUserType;
  }

private:
  NUBase*         mNucleusObj;
  const UserType* mUserType;

  //forbid
  CARBON_FORBID_DEFAULT_CTORS(CbuildBranchDataBOM);
};

class CbuildSymTabBOM::ExprSelectIdentWalk : public CarbonExprWalker
{
public:
  ExprSelectIdentWalk(STNodeSelectDB* selector) : mSelector(selector)
  {}

  virtual ~ExprSelectIdentWalk() {}

  typedef UtArray<STAliasedLeafNode*> NodeArray;
  typedef Loop<NodeArray> NodeArrayLoop;
  
  virtual void visitIdent(CarbonIdent* ident)
  {
    DynBitVector notUsed;
    STAliasedLeafNode* leaf = ident->getNode(&notUsed);
    if (mSelector)
      mSelector->requestAlwaysSelectLeaf(leaf);

    mVisitedNodes.push_back(leaf);

    // place the expression in the node so we can access the
    // backpointer. The shell uses the tag type to determine whether
    // or not to create expression shell nets. These will not be
    // marked as CbuildShellDB::eExprId, but will contain their
    // self-describing identifiers.
    if (CbuildSymTabBOM::getIdent(leaf) == NULL)
      CbuildSymTabBOM::putIdent(leaf, ident);
  }

  NodeArrayLoop loopVisited() { return NodeArrayLoop(mVisitedNodes); }
  
private:
  /* The selector for the symboltable. This can be null, which means
     that all nodes in the symboltable will be written out anyway. So,
     we don't have to do anything in this class to make sure idents
     are selected.
  */
  STNodeSelectDB* mSelector;
  NodeArray mVisitedNodes;

  // forbid
  ExprSelectIdentWalk(const ExprSelectIdentWalk&);
  ExprSelectIdentWalk& operator=(const ExprSelectIdentWalk&);
};


CSymTabSelect::CSymTabSelect(bool dumpTemps, CbuildSymTabBOM* bomManager,
                             IODB* iodb)
{
  mDumpTemps = dumpTemps;
  mBOMManager = bomManager;
  mIODB = iodb;
}

CSymTabSelect::~CSymTabSelect()
{
}

bool CSymTabSelect::computeSelectLeaf(const STAliasedLeafNode* leaf,
                                      const HdlHierPath*)
{
  bool writeIt = true;
  const NUNet* net = NUNet::find(leaf);
  const CarbonIdent* ident = CbuildSymTabBOM::getIdent(leaf);
  
  // move on if this is a temp variable and we are not dumping them
  // move on if this net is dead logic (therefore NULL)
  if (net == NULL) {
    if (! CbuildSymTabBOM::isForceSubordinate(leaf)) {
      writeIt = false;
    }
  } else if (ident && (mBOMManager->getExpr(ident) != NULL)) {
    writeIt = true;
  } else if (!CodeGen::isIncludedInTypeDictionary(leaf, mIODB)) {
    writeIt = false;
  } else if ((net->isTemp() && ! mDumpTemps) && (leaf->getStorage() != leaf)) {
    // If the temporary has storage, it isn't a temporary.
    writeIt = false;
  } else if (!CbuildSymTabBOM::isTypeTagValid(leaf)) {
    writeIt = false;
  } else if (CbuildSymTabBOM::getTypeTag(leaf) == CbuildShellDB::eOffsetId) {
    ST_ASSERT(CbuildSymTabBOM::isTypeIndexValid(leaf), leaf);
  }
  return writeIt;
} // bool CSymTabSelect::computeSelectLeaf


bool ElaboratedGUISymTabSelect::computeSelectLeaf(const STAliasedLeafNode *leaf, const HdlHierPath*)
{
  const NUNet* net = NUNet::find (leaf);
  // !%^%!^&!*~ record nets.... still looking for a good way to distinguish
  // them without poking at the name
  return net != NULL && !net->isTemp () && strncmp (net->getName ()->str (), "$recnet", 7);
}

bool UnelaboratedGUISymTabSelect::computeSelectLeaf(const STAliasedLeafNode *leaf, const HdlHierPath*)
{
  NUAliasDataBOM *bom = NUAliasBOM::castBOM (leaf->getBOMData ());
  if (!bom->hasValidNet ()) {
    return false;
  } else {
    NUNet *net = bom->getNet ();
    return !net->isTemp () && strncmp (net->getName ()->str (), "$recnet", 7);
  }
}

static UInt32 cTagInit = 0xFFFFFFFF;

CbuildSymTabBOM::CbuildElabInfo::StoreInfo::StoreInfo() :
  mTagTypeIndex(cTagInit),
  mSignature(NULL),
  mNodeFlags(CbuildShellDB::eNoNodeFlags)
{
  mOffsetConstant.mStorageOffset = -1;
}

CbuildSymTabBOM::CbuildElabInfo::CbuildElabInfo() : 
  mElabInfo(), mStoreInfo()
{}

CbuildSymTabBOM::CbuildElabInfo::~CbuildElabInfo()
{}

const NUBase* CbuildSymTabBOM::CbuildElabInfo::getNucleusObj() const
{
  return mElabInfo.mNucleusObj;
}

const IODBIntrinsic* CbuildSymTabBOM::CbuildElabInfo::getIntrinsic() const
{
  return mElabInfo.mIntrinsic;
}

const CarbonIdent* CbuildSymTabBOM::CbuildElabInfo::getIdent() const
{
  return mElabInfo.mNodeIdent;
}

const UserType* CbuildSymTabBOM::CbuildElabInfo::getUserType() const
{
  return mElabInfo.mUserType;
}

void CbuildSymTabBOM::CbuildElabInfo::putNucleusObj(NUBase* base)
{
  mElabInfo.mNucleusObj = base;
}

void CbuildSymTabBOM::CbuildElabInfo::putIntrinsic(const IODBIntrinsic* intrinsic)
{
  mElabInfo.mIntrinsic = intrinsic;
}

void CbuildSymTabBOM::CbuildElabInfo::putIdent(CarbonIdent* ident)
{
  mElabInfo.mNodeIdent = ident;
}

void CbuildSymTabBOM::CbuildElabInfo::putUserType(const UserType* ut)
{
  mElabInfo.mUserType = ut;
}

void CbuildSymTabBOM::CbuildElabInfo::print() const
{
  if (mElabInfo.mNucleusObj)
    UtIO::cout() << UtIO::hex << mElabInfo.mNucleusObj;
  else
    UtIO::cout() << "(null)";

  UtIO::cout() << ", ";

  if (mElabInfo.mNodeIdent)
    UtIO::cout() << UtIO::hex << mElabInfo.mNodeIdent << " ";
  
  if (mElabInfo.mIntrinsic != NULL ) {
    mElabInfo.mIntrinsic->print(); // This will end the line and flush the stream
  }
  else {
    UtIO::cout() << UtIO::endl;
  }

  if (mElabInfo.mUserType != NULL) {
    mElabInfo.mUserType->print(); // This will also end the line.
  } else {
    UtIO::cout() << UtIO::endl;
  }

  UtIO::cout() << "STORE: ";
  UtIO::cout() << ", ";
  UtIO::cout() << UtIO::hex << mStoreInfo.mTagTypeIndex << ", ";
  UtIO::cout() << UtIO::hex << mStoreInfo.mOffsetConstant.mStorageOffset << ", ";
  if (mStoreInfo.mSignature)
    UtIO::cout() << UtIO::hex << mStoreInfo.mSignature;
  else
    UtIO::cout() << "(null) ";
  UtIO::cout() << UtIO::endl;
}

int CbuildSymTabBOM::CbuildElabInfo::getTypeTag() const
{
  NU_ASSERT(isTypeTagValid(), getNucleusObj());
  return CbuildShellDB::getTypeTag(mStoreInfo.mTagTypeIndex);
}

UInt32 CbuildSymTabBOM::CbuildElabInfo::getTypeIndex() const
{
  return CbuildShellDB::getTypeIndex(mStoreInfo.mTagTypeIndex);
}

SInt32 CbuildSymTabBOM::CbuildElabInfo::getStorageOffset() const
{
  return mStoreInfo.mOffsetConstant.mStorageOffset;
}

const DynBitVector* CbuildSymTabBOM::CbuildElabInfo::getConstantVal() const
{
  return mStoreInfo.mOffsetConstant.mConstVal;
}

const SCHSignature* CbuildSymTabBOM::CbuildElabInfo::getSCHSignature() const
{
  return mStoreInfo.mSignature;
}

void CbuildSymTabBOM::CbuildElabInfo::putTypeIndex(UInt32 typeIndex)
{
  CbuildShellDB::enterTypeIndex(&(mStoreInfo.mTagTypeIndex), typeIndex);
}

void CbuildSymTabBOM::CbuildElabInfo::putTypeTag(int typeTag)
{
  CbuildShellDB::enterTypeTag(&(mStoreInfo.mTagTypeIndex), typeTag);
}

void CbuildSymTabBOM::CbuildElabInfo::putStorageOffset(SInt32 offset)
{
  mStoreInfo.mOffsetConstant.mStorageOffset = offset;
}

void CbuildSymTabBOM::CbuildElabInfo::putConstantVal(const DynBitVector* val)
{
  mStoreInfo.mOffsetConstant.mConstVal = val;
}

void CbuildSymTabBOM::CbuildElabInfo::putSCHSignature(const SCHSignature* signature)
{
  mStoreInfo.mSignature = signature;
}

bool CbuildSymTabBOM::CbuildElabInfo::isTypeTagValid() const
{
  return (CbuildShellDB::getTypeTag(mStoreInfo.mTagTypeIndex) !=
          CbuildShellDB::getTypeTag(cTagInit));
}

bool CbuildSymTabBOM::CbuildElabInfo::isTypeIndexValid() const
{
  return (CbuildShellDB::getTypeIndex(mStoreInfo.mTagTypeIndex) !=
          CbuildShellDB::getTypeIndex(cTagInit));
}

CbuildSymTabBOM::CbuildBranchDataBOM::CbuildBranchDataBOM() :
  mNucleusObj(NULL), mUserType(NULL)
{}

              
CbuildSymTabBOM::CbuildBranchDataBOM::~CbuildBranchDataBOM()
{}

void CbuildSymTabBOM::CbuildBranchDataBOM::print() const
{
  if (mNucleusObj)
    UtIO::cout() << UtIO::hex << mNucleusObj;
  else
    UtIO::cout() << "(null)";
  UtIO::cout() << UtIO::endl;

  if (mUserType != NULL) {
    mUserType->print(); // This will end the line.
  } else {
    UtIO::cout() << UtIO::endl;
  }
}

NUBase* CbuildSymTabBOM::CbuildBranchDataBOM::getNucleusObj()
{
  const CbuildBranchDataBOM* me = const_cast<const CbuildBranchDataBOM*>(this);
  return const_cast<NUBase*>(me->getNucleusObj());
}

const NUBase* CbuildSymTabBOM::CbuildBranchDataBOM::getNucleusObj() const
{
  return mNucleusObj;
}

void CbuildSymTabBOM::CbuildBranchDataBOM::putNucleusObj(NUBase* base)
{
  mNucleusObj = base;
}

const UserType* CbuildSymTabBOM::CbuildBranchDataBOM::getUserType() const
{
  return mUserType;
}

void CbuildSymTabBOM::CbuildBranchDataBOM::putUserType(const UserType* ut)
{
  mUserType = ut;
}


// CbuildSymTab
CbuildSymTabBOM::CbuildSymTabBOM()
  : mScheduleFactory(NULL), 
    mUserTypeFactory(NULL),
    mBVPool(NULL),
    mExprDBContext(NULL)
{
}

CbuildSymTabBOM::~CbuildSymTabBOM()
{
  delete mExprDBContext;
}

void CbuildSymTabBOM::putBVFactory(DynBitVectorFactory* bvPool)
{
  mBVPool = bvPool;
}

bool CbuildSymTabBOM::isStorageNode(const STAliasedLeafNode* leaf)
{
  return ((leaf->getInternalStorage() != NULL) &&
          (leaf->getStorage() == leaf));
}

STFieldBOM::Data CbuildSymTabBOM::allocLeafData() 
{
  CbuildElabInfo* ret = new CbuildElabInfo;
  return static_cast<Data>(ret);
}

STFieldBOM::Data CbuildSymTabBOM::allocBranchData() 
{
  CbuildBranchDataBOM* ret = new CbuildBranchDataBOM;
  return static_cast<Data>(ret);
}

void CbuildSymTabBOM::freeLeafData(const STAliasedLeafNode*, Data* bomdata)
{
  CbuildElabInfo* elabInfo = castLeafBOM(*bomdata);
  delete elabInfo;
  *bomdata = NULL;
}

void CbuildSymTabBOM::freeBranchData(const STBranchNode*, Data* bomdata) 
{
  CbuildBranchDataBOM* obj = castBranchBOM(*bomdata);
  delete obj;
  *bomdata = NULL;
}

void CbuildSymTabBOM::printLeaf(const STAliasedLeafNode* leaf) const
{
  const CbuildElabInfo* elabInfo = castLeafBOM(leaf->getBOMData());
  elabInfo->print();
}

void CbuildSymTabBOM::printBranch(const STBranchNode* branch) const
{
  const CbuildBranchDataBOM* obj = castBranchBOM(branch->getBOMData());
  obj->print();
}

CbuildSymTabBOM::CbuildElabInfo* CbuildSymTabBOM::castLeafBOM(Data bomdata)
{
  return static_cast<CbuildElabInfo*>(bomdata);
}

CbuildSymTabBOM::CbuildElabInfo* CbuildSymTabBOM::castStoreLeafBOM(Data bomdata)
{
  return static_cast<CbuildElabInfo*>(bomdata);
}

CbuildSymTabBOM::CbuildBranchDataBOM* CbuildSymTabBOM::castBranchBOM(Data bomdata)
{
  return static_cast<CbuildBranchDataBOM*>(bomdata);
}

void CbuildSymTabBOM::setAtomicCache(AtomicCache* atomCache)
{
  mUserTypeFactory->setUserTypesAtomicCache(atomCache);
}

void CbuildSymTabBOM::copyStringsIntoSymTable()
{
  mUserTypeFactory->copyStringsIntoSymTable();
}


void CbuildSymTabBOM::preFieldWrite(ZostreamDB& out)
{
  INFO_ASSERT(mExprDBContext, "no expr DB context");
  mExprDBContext->writeExprs(out);
  mUserTypeFactory->save(out);
}

void CbuildSymTabBOM::writeBranchData(const STBranchNode* branch, ZostreamDB& out,
                                      AtomicCache* atomicCache)
  const
{
  // Figure out the branch type. It can be one of NUModuleElab,
  // NUTFElab, or NUNamedDeclarationScope.
  const NUElabBase* elabBase = NUElabBase::find(branch);
  const NUScopeElab* scopeElab = dynamic_cast<const NUScopeElab*>(elabBase);
  HierFlags flags = HierFlags(0);
  if (scopeElab != NULL) {
    flags = scopeElab->getHierFlags();
  }

  // For modules, also write out the module name.
  StringAtom* branchAtom = NULL;
  const NUModuleElab* moduleElab = dynamic_cast<const NUModuleElab*>(elabBase);
  if (moduleElab != NULL) {
    StringAtom* atom = moduleElab->getModule()->getOriginalName();
    branchAtom = atomicCache->getIntern(atom->str());
    NU_ASSERT(branchAtom != NULL, moduleElab);
  }

  const UserType* ut = CbuildSymTabBOM::getUserType(branch);

  // Write the branch data
  CbuildShellDB::writeBranch(out, flags, branchAtom, ut);
}

void CbuildSymTabBOM::writeLeafData(const STAliasedLeafNode* leaf , ZostreamDB& out) const
{
  const NUNet* net = NUNet::find(leaf);
  UInt32 netFlags = UInt32(eNoneNet);
  if (net)
    netFlags = UInt32(net->getFlags());
  
  // signature slot -- there's a small circular reference problem here.
  // signatures reference clock nets, and all nets reference signatures.
  // so we solve this with a map from signatures to indices and write
  // the indices.
  const SCHSignature* sig = CbuildSymTabBOM::getSCHSignature(leaf);

  CbuildShellDB::NodeFlags nodeFlags = CbuildSymTabBOM::getNodeFlags(leaf);

  CbuildShellDB::StorageValue stVal;
  bool setOffset = true;
  // For each node, we want the net-specific traits as well as the
  // storage-specific traits. So, we need the typeIndex of the leaf
  // and the typeTag of the storageLeaf.
  UInt32 tagTypeIndex = 0;
  int typeTag = 0;
  if (CbuildSymTabBOM::isTypeTagValid(leaf))
  {
    typeTag = CbuildSymTabBOM::getTypeTag(leaf);
    CbuildShellDB::enterTypeTag(&tagTypeIndex, typeTag);
    if (typeTag == CbuildShellDB::eConstValWordId)
    {
      const DynBitVector* constVal = CbuildSymTabBOM::getConstantVal(leaf);
      stVal.mImmediateStorage = constVal->value();
      setOffset = false;
    }
    else if (typeTag == CbuildShellDB::eConstValId)
    {
      stVal.mLargeStorage = CbuildSymTabBOM::getConstantVal(leaf);
      setOffset = false;
    }
    CbuildShellDB::enterTypeTag(&tagTypeIndex, typeTag);
  }
  else if (sig)
  {
    // we have a valid signature, but this net does not have a type
    // tag? Bogosity!
    
    UtString buf;
    leaf->compose(&buf);
    UtIO::cout() << "INTERNAL WARNING: writeLeaveData(" << buf.c_str()
                 << " has an invalid TypeTag)" << UtIO::endl;
  }
  if (setOffset)
    stVal.mOffsetToStorage = CbuildSymTabBOM::getStorageOffset(leaf);
  
  const IODBIntrinsic* intrinsic = CbuildSymTabBOM::getIntrinsic(leaf);

  // Get the expression for this node, if any
  const CarbonExpr* descExpr = NULL;
  const CarbonIdent* ident = CbuildSymTabBOM::getIdent(leaf);
  if (ident)
  {
    descExpr = mExprMap.getExpr(ident);
    // If a describing expression is NULL and the typeTag is eExprId
    // that just means the expression is dead. That's ok. But, if it
    // isn't an eExprId and the ident is a backpointer, let's keep
    // that backpointer so we can print out warnings/errors, etc. with
    // the original name.
    if ((descExpr == NULL) && (typeTag != CbuildShellDB::eExprId))
    {
      const SymTabIdent* symTabIdent = ident->castSymTabIdent();
      if (symTabIdent && symTabIdent->castSymTabIdentBP())
        // has a backpointer and is not an expression. We need that
        // for correct names 
        descExpr = ident;
    }
  }

  UInt32 exprExist = 0;
  UInt32 exprIndex = 0;
  // expression nodes do not have type indices.
  if (descExpr)
  {
    exprIndex = mExprDBContext->getIndex(descExpr);
    exprExist = 1;
  }

  if (CbuildSymTabBOM::isTypeIndexValid(leaf))
  {
    // For each node, we want the net-specific traits as well as the
    // storage-specific traits. So, we need the typeIndex of the leaf
    // and the typeTag of the storageLeaf.
    CbuildShellDB::enterTypeIndex(&tagTypeIndex,  CbuildSymTabBOM::getTypeIndex(leaf));
  }

  const UserType* ut = CbuildSymTabBOM::getUserType(leaf);

  CbuildShellDB::write(out, netFlags, 0, tagTypeIndex, intrinsic,
                       stVal, exprExist, exprIndex, nodeFlags, ut);
}

STFieldBOM::ReadStatus 
CbuildSymTabBOM::preFieldRead(ZistreamDB& /*in*/)
{
  return eReadOK;
}

STFieldBOM::ReadStatus 
CbuildSymTabBOM::readLeafData(STAliasedLeafNode *leaf, ZistreamDB& , 
                              MsgContext*)
{
  ST_ASSERT(0, leaf);
  return eReadIncompatible;
}
  
STFieldBOM::ReadStatus 
CbuildSymTabBOM::readBranchData(STBranchNode *branch, ZistreamDB&, 
                                MsgContext*) 
{
  ST_ASSERT(0, branch);
  return eReadIncompatible;
}

void CbuildSymTabBOM::putScheduleFactory(SCHScheduleFactory* sf) {
  mScheduleFactory = sf;
}

void CbuildSymTabBOM::putUserTypeFactory(UserTypeFactory* utf) {
  mUserTypeFactory = utf;
}

void CbuildSymTabBOM::writeBOMSignature(ZostreamDB& out) const
{
  CbuildShellDB::writeSignature(out);
}

STFieldBOM::ReadStatus
CbuildSymTabBOM::readBOMSignature(ZistreamDB& /*in*/, UtString* errMsg)
{
  *errMsg << "Cbuild BOM has no db read semantics";
  return eReadIncompatible;
}

void CbuildSymTabBOM::prepareLeafForDB(const STAliasedLeafNode* leaf, 
                                       STNodeSelectDB* selector,
                                       bool dstSymtabIsSrc,
                                       STSymbolTable::LeafAssoc *leafAssoc) const
{
  CarbonIdent* ident = CbuildSymTabBOM::getIdent(leaf);
  if (ident)
  {
    // Have to walk the back pointer expression in case a split
    // net is in the io table but its backpointer is not. This may
    // be considered a bug, but not a reason to crash at a
    // customer's site.
    SymTabIdent* symtabIdent = ident->castSymTabIdent();
    ST_ASSERT(symtabIdent, leaf);
    SymTabIdentBP* symtabIdentBP = symtabIdent->castSymTabIdentBP();
    if (symtabIdentBP)
    {
      ExprSelectIdentWalk exprWalk(selector);
      CarbonExpr* bp = symtabIdentBP->getBackPointer();
      exprWalk.visitExpr(bp);
      // nasty, but needed. The back pointer ident may have already
      // been passed up in the node loop. So, we have to prepare that
      // node as well.
      for (ExprSelectIdentWalk::NodeArrayLoop i = exprWalk.loopVisited(); ! i.atEnd(); ++i)
        prepareLeafForDB(*i, selector, dstSymtabIsSrc, leafAssoc);
    }
    
    mExprFactory.setupDBContext(mExprDBContext, ident, dstSymtabIsSrc, leafAssoc);
    
    const CarbonExpr* expr = getExpr(ident);
    if (expr)
    {
      ExprSelectIdentWalk exprWalk(selector);
      exprWalk.visitExpr(const_cast<CarbonExpr*>(expr));
      mExprFactory.setupDBContext(mExprDBContext, expr, dstSymtabIsSrc, leafAssoc);
    }
  }
}

// Any clocks in the mask will be marked as always select. This avoids
// optimization like constant propagation invalidating a signature.
static void sSelectClocks(STNodeSelectDB* selector,
                          const SCHScheduleMask* mask)
{
  if (mask)
  {
    for (SCHScheduleMask::UnsortedEvents p = mask->loopEvents(); 
         !p.atEnd(); ++p)
    {
      const SCHEvent* event = *p;
      if (event->isClockEvent())
      {
        const STSymbolTableNode* clock = event->getClock();
        selector->requestAlwaysSelectLeaf(clock->castLeaf());
      }
    }
  }
}

void CbuildSymTabBOM::prepareForDBWrite(STSymbolTable* srcSymtab,
  STSymbolTable* dstSymtab,
  STNodeSelectDB* selector,
  STSymbolTable::LeafAssoc *leafAssoc) const
{
  delete mExprDBContext;
  mExprDBContext = new ExprDBContext(dstSymtab, mBVPool);
  
  // Loop the symboltable and make sure all the clocks in schedule
  // signatures are written out. Clocks in signatures are never
  // expressions, so this should be done before walking expressions.
  const HdlHierPath* pather = srcSymtab->getHdlHier();
  for (STSymbolTable::NodeLoop p = srcSymtab->getNodeLoop();
       ! p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf)
    {
      const SCHSignature* sig = CbuildSymTabBOM::getSCHSignature(leaf);
      if (sig)
      {
        const SCHScheduleMask* tMask = sig->getTransitionMask();
        sSelectClocks(selector, tMask);
        const SCHScheduleMask* sMask = sig->getSampleMask();
        sSelectClocks(selector, sMask);
      }
    }
  }

  // Must walk through all the mapped expressions first and marked
  // any subordinates of selected leaves as also selected.
  for (CarbonIdentExprMap::UnsortedCLoop p = mExprMap.loopCUnsorted();
       ! p.atEnd(); ++p)
  {
    const CarbonIdent* ident = p.getKey();
    INFO_ASSERT(ident, "null key in CbuildSymTabBOM::mExprMap");
    // if the ident's node is not selected then don't setup the
    // expression
    
    DynBitVector usageMask;
    const STAliasedLeafNode* identLeaf = ident->getNode(&usageMask);
    if ((selector == NULL) || selector->selectLeaf(identLeaf, srcSymtab->getHdlHier()))
    {
      CarbonExpr* expr = p.getValue();
      if (expr)
      {
        // first walk the expression, making sure all subordinate
        // idents will be written
        ExprSelectIdentWalk exprWalk(selector);
        exprWalk.visitExpr(expr);
      }
    }
  }
  
  for (STSymbolTable::NodeLoopSorted p = srcSymtab->getNodeLoopSorted();
       ! p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    // if the ident's node is not selected then don't setup the
    // expression
    if (leaf && selector->selectLeaf(leaf, pather))
      prepareLeafForDB(leaf, selector, dstSymtab == srcSymtab, leafAssoc);
  }
}

bool CbuildSymTabBOM::writeData(ZostreamDB& out, STSymbolTable* symtab,
                                STNodeSelectDB* selector) const
{
  bool ret = true;
  
  symtab->writeDB(out, selector, true);
  if (!out)
    ret = false;
  
  if (!mScheduleFactory->writeDatabase(out))
    ret = false;

  // write the mapping of storage nodes to signatures
  if (! out.fail())
  {
    out << "netSignatures";
    
    for (STSymbolTable::NodeLoop p = symtab->getNodeLoop();
         ! p.atEnd(); ++p)
    {
      STSymbolTableNode* node = *p;
      STAliasedLeafNode* leaf = node->castLeaf();
      // if the ident's node is not selected then don't setup the
      // expression
      if (leaf)
      {
        const SCHSignature* sig = CbuildSymTabBOM::getSCHSignature(leaf);
        // no reason to write out the node/sig pair if the signature
        // is null (dead net). Also, don't write it out if the leaf
        // has not been written. Check the sig first -- faster.
        if (sig && out.isMapped(leaf))
        {
          out.writePointer(leaf);
          out.writePointer(sig);
        }
      }
    }
    // done with net signatures
    out.writeNull();
    
    if (out.fail())
      ret = false;
  }
  
  return ret;
}

void CbuildSymTabBOM::getMasters(BaseSet* unelab, LeafNodeSet * masters, STSymbolTable* table)
{
  for (STSymbolTable::NodeLoop symIter = table->getNodeLoop();
       not symIter.atEnd(); 
       ++symIter) {
    STSymbolTableNode * node = *symIter;
    STAliasedLeafNode * leaf = node->castLeaf();
    if (leaf) {
      NUBase * base = getNucleusObj(node);
      if (base) {
	NUBase * key = NULL;

	if (dynamic_cast<NUElabBase*>(base)) {
	  // 'base' is an elaborated object. find its unelaborated counterpart.
	  NUNetElab * elab_net = dynamic_cast<NUNetElab*>(base);
	  if (elab_net) {
	    key = elab_net->getNet();
	  }
	} else {
	  // 'base' is unelaborated.
	  key = base;
	}

	if (key) {
          if (unelab->find(key) != unelab->end()) {
	    masters->insert(leaf->getMaster());
          }
        }
      }
    }
  }
}

void CbuildSymTabBOM::getElabs(BaseSet* unelab, ElabBaseSet* elab, STSymbolTable* table)
{
  LeafNodeSet masters;
  getMasters(unelab,&masters,table);
  for (LeafNodeSet::iterator iter = masters.begin();
       iter != masters.end();
       ++iter) {
    STAliasedLeafNode * master = (*iter);
    NUBase* base = getNucleusObj(master);
    NUElabBase * elabBase = dynamic_cast<NUElabBase*>(base);

    // the master has to be an elaborated object.
    NU_ASSERT(elabBase, base);
    elab->insert(elabBase);
  }
}

const CbuildSymTabBOM::CbuildElabInfo* 
CbuildSymTabBOM::getElabInfoConst(const STAliasedLeafNode* leaf)
{
  const CbuildElabInfo* info = castLeafBOM(leaf->getBOMData());
  return info;
}

CbuildSymTabBOM::CbuildElabInfo* 
CbuildSymTabBOM::getElabInfo(const STAliasedLeafNode* leaf)
{
  const CbuildElabInfo* elabInfo = getElabInfoConst(leaf);
  return const_cast<CbuildElabInfo*>(elabInfo);
}

ESFactory* CbuildSymTabBOM::getExprFactory()
{
  return &mExprFactory;
}

void CbuildSymTabBOM::mapExpr(CarbonIdent* ident, CarbonExpr* expr)
{
  // Let's make sure that we NEVER map an ident to itself. That would
  // be a cycle.
  CarbonIdent* exprIdent = expr->castIdent();
  if (exprIdent)
  {
    CE_ASSERT(ident != exprIdent, expr);
    DynBitVector useMask;
    STAliasedLeafNode* exprLeaf = exprIdent->getNode(&useMask);
    STAliasedLeafNode* identLeaf = ident->getNode(&useMask);
    ST_ASSERT(exprLeaf != identLeaf, exprLeaf);
  }
  mExprMap.mapExpr(ident, expr);
}

void CbuildSymTabBOM::removeMappedExpr(CarbonIdent* ident)
{
  mExprMap.removeMappedExpr(ident);
}


CarbonExpr* CbuildSymTabBOM::getExpr(CarbonIdent* ident)
{
  return mExprMap.getExpr(ident);
}

const CarbonExpr* CbuildSymTabBOM::getExpr(const CarbonIdent* ident) const
{
  return mExprMap.getExpr(ident);
}

const SymTabIdentBP*
CbuildSymTabBOM::getSymTabIdentBP(const STAliasedLeafNode* node)
{
  const CarbonIdent* ident = getIdent(node);
  if (ident) {
    const SymTabIdent* symTabIdent = ident->castSymTabIdent();
    ST_ASSERT(symTabIdent, node);
    return symTabIdent->castSymTabIdentBP();
  }
  return NULL;
}

class LeafNodeExtractor : public CarbonExprWalker
{
public:
  LeafNodeExtractor(CbuildSymTabBOM* bom, STAliasedLeafNodeVector* vecToPopulate) : 
    mBOM(bom), // this may be NULL
    mNodeVec(vecToPopulate) 
  {}
    
  virtual void visitIdent(CarbonIdent* ident)
  {
    const SymTabIdent* symTabIdent = ident->castSymTabIdent();


    if (mBOM)
      // make sure there is no nesting
      CE_ASSERT(mBOM->getExpr(symTabIdent) == NULL, symTabIdent);
    
    const STAliasedLeafNode* leaf = symTabIdent->getNode();
    if (mCovered.insertWithCheck(leaf))
      mNodeVec->push_back(leaf);
  }

private:
  CbuildSymTabBOM* mBOM;
  STAliasedLeafNodeVector* mNodeVec;
  UtHashSet<const STAliasedLeafNode*> mCovered;
};

void CbuildSymTabBOM::getOriginalNodes(const STAliasedLeafNode* node,
                                       STAliasedLeafNodeVector* origNodes)
{
  const SymTabIdentBP* symTabIdentBP = getSymTabIdentBP(node);
  if (symTabIdentBP != NULL) {
    LeafNodeExtractor nodeExtractor(NULL, origNodes);
    nodeExtractor.visitExpr(symTabIdentBP->getBackPointer());
  }
}

bool CbuildSymTabBOM::isExprSubordinate(const STSymbolTableNode* node)
{
  const STAliasedLeafNode* leaf = node->castLeaf();
  bool isSubOrd = false;
  if (leaf)
  {
      isSubOrd = getSymTabIdentBP(leaf) != NULL;
  }
  return isSubOrd;
}

bool CbuildSymTabBOM::isExprMappedNode(const STSymbolTableNode* node) const
{
  const STAliasedLeafNode* leaf = node->castLeaf();
  bool isMapped = false;
  if (leaf)
  {
    const CbuildElabInfo* elabInfo = getElabInfoConst(leaf);
    const CarbonIdent* ident = elabInfo->getIdent();
    if (ident) {
      const CarbonExpr* identExpr = getExpr(ident);
      if (identExpr && (identExpr != ident))
        isMapped = true;
    }
  }
  return isMapped;
}

CarbonExpr* CbuildSymTabBOM::getMappedExpr(STSymbolTableNode* node)
{
  STAliasedLeafNode* leaf = node->castLeaf();
  CarbonExpr* expr = NULL;
  if (leaf)
  {
    CbuildElabInfo* elabInfo = getElabInfo(leaf);
    CarbonIdent* ident = elabInfo->getIdent();
    if (ident) {
      CarbonExpr* identExpr = getExpr(ident);
      if (identExpr && (identExpr != ident))
        expr = identExpr;
    }
  }
  return expr;
}

bool
CbuildSymTabBOM::getMappedExprLeafNodes(STAliasedLeafNode* node,
                                        STAliasedLeafNodeVector* nodes)
{
  bool isExpr = false;
  CbuildElabInfo* elabInfo = getElabInfo(node);
  CarbonIdent* ident = elabInfo->getIdent();
  if (ident)
  {
    CarbonExpr* expr = getExpr(ident);
    if (expr)
    {
      isExpr = true;
      // Gather the leaf nodes
      LeafNodeExtractor lfneWalk(this, nodes);
      lfneWalk.visitExpr(expr);
    } // if expr
  } // if ident

  return isExpr;
} // CbuildSymTabBOM::getMappedExprLeafNodes


NUBase* CbuildSymTabBOM::getNucleusObj(const STSymbolTableNode* node)
{
  NUBase* base = NULL;
  if (node->castBranch())
  {
    CbuildBranchDataBOM* bom = castBranchBOM(node->getBOMData());
    base = bom->getNucleusObj();
  }
  else
  {
    CbuildElabInfo* elabInfo = getElabInfo(node->castLeaf());
    base = elabInfo->getNucleusObj();
  }
  
  return base;
}

void CbuildSymTabBOM::putNucleusObj(STSymbolTableNode* node, NUBase* base)
{
  if (node->castBranch())
  {
    CbuildBranchDataBOM* bom = castBranchBOM(node->getBOMData());
    bom->putNucleusObj(base);
  }
  else
  {
    CbuildElabInfo* elabInfo = getElabInfo(node->castLeaf());
    elabInfo->putNucleusObj(base);
  }
}


SInt32 CbuildSymTabBOM::getStorageOffset(const STAliasedLeafNode* leaf)
{
  SInt32 offset = -1;
  if (isStorageNode(leaf->getStorage()))
  {
    const STAliasedLeafNode* storage = leaf->getStorage();
    const CbuildElabInfo* storeObj = castStoreLeafBOM(storage->getBOMData());
    offset = storeObj->getStorageOffset();
  }
  return offset;
}

void CbuildSymTabBOM::setTypeTag(STAliasedLeafNode* leaf, int typeIndexTag)
{
  STAliasedLeafNode* storage = leaf->getStorage();
  if (isStorageNode(storage))
  {
    CbuildElabInfo* bom = castStoreLeafBOM(storage->getBOMData());
    bom->putTypeTag(typeIndexTag);
  }
}

void CbuildSymTabBOM::setStorageOffset(STAliasedLeafNode* nLeaf, SInt32 symbolOffset)
{
  STAliasedLeafNode* storage = nLeaf->getStorage();
  if (isStorageNode(storage))
  {
    CbuildElabInfo* bom = castStoreLeafBOM(storage->getBOMData());
    bom->putStorageOffset(symbolOffset);
  }
}

CarbonIdent* CbuildSymTabBOM::getIdent(const STAliasedLeafNode* leaf)
{
  CbuildElabInfo* elabInfo = getElabInfo(leaf);
  return elabInfo->getIdent();
}

void CbuildSymTabBOM::putIdent(STAliasedLeafNode* leaf, CarbonIdent* ident)
{
  CbuildElabInfo* elabInfo = getElabInfo(leaf);
  elabInfo->putIdent(ident);
}

bool CbuildSymTabBOM::isTypeTagValid(const STAliasedLeafNode* leaf)
{
  bool ret = false;
  const STAliasedLeafNode* storage = leaf->getStorage();
  if (isStorageNode(storage))
  {
    const CbuildElabInfo* elabInfo = castStoreLeafBOM(storage->getBOMData());
    ret = elabInfo->isTypeTagValid();
  }
  return ret;
}

bool CbuildSymTabBOM::isConstantTagType(const STAliasedLeafNode* leaf)
{
  bool ret = false;
  const STAliasedLeafNode* storage = leaf->getStorage();
  if (isStorageNode(storage))
  {
    const CbuildElabInfo* elabInfo = castStoreLeafBOM(storage->getBOMData());
    if (elabInfo->isTypeTagValid())
    {
      int tagType = elabInfo->getTypeTag();
      ret = CbuildShellDB::isConstantType(tagType);
    }
  }

  return ret;
}

int CbuildSymTabBOM::getTypeTag(const STAliasedLeafNode* leaf)
{
  int typeTag = int(CbuildShellDB::eNoTypeId);
  const STAliasedLeafNode* storage = leaf->getStorage();
  if (isStorageNode(storage))
  {
    const CbuildElabInfo* elabInfo = castStoreLeafBOM(storage->getBOMData());
    typeTag = elabInfo->getTypeTag();
  }
  return typeTag;
}

bool CbuildSymTabBOM::isTypeIndexValid(const STAliasedLeafNode* leaf)
{
  bool ret = false;
  const STAliasedLeafNode* storage = leaf->getStorage();
  if (isStorageNode(storage))
  {
    const CbuildElabInfo* elabInfo = castStoreLeafBOM(storage->getBOMData());
    ret = elabInfo->isTypeIndexValid();
  }
  return ret;
}

UInt32 CbuildSymTabBOM::getTypeIndex(const STAliasedLeafNode* leaf)
{
  UInt32 typeIndex = cTagInit;
  const STAliasedLeafNode* storage = leaf->getStorage();
  if (isStorageNode(storage))
  {
    const CbuildElabInfo* elabInfo = castStoreLeafBOM(storage->getBOMData());
    typeIndex = elabInfo->getTypeIndex();
  }
  return typeIndex;
}


void CbuildSymTabBOM::setSCHSignature(STAliasedLeafNode* leaf, const SCHSignature* sig)
{
  STAliasedLeafNode* storage = leaf->getStorage();
  if (isStorageNode(storage))
  {
    CbuildElabInfo* elabInfo = castStoreLeafBOM(storage->getBOMData());
    elabInfo->putSCHSignature(sig);
  }
}

const SCHSignature* CbuildSymTabBOM::getSCHSignature(const STAliasedLeafNode* leaf)
{
  const SCHSignature* sig = NULL;
  const STAliasedLeafNode* storage = leaf->getStorage();
  if (isStorageNode(storage))
  {
    const CbuildElabInfo* elabInfo = castStoreLeafBOM(storage->getBOMData());
    sig = elabInfo->getSCHSignature();
  }
  return sig;
}

void CbuildSymTabBOM::putConstantVal(STAliasedLeafNode* leaf, NUConst* constVal)
{
  STAliasedLeafNode* storage = leaf->getStorage();
  if (isStorageNode(storage))
  {
    // Translate the NUConst to a CarbonConst. We cannot assume that
    // the NUExpr will survive.
    const DynBitVector* carbonConstVal = NULL;
    if (constVal)
    {
      DynBitVector tmp;
      constVal->getSignedValue(&tmp);
      carbonConstVal = mBVPool->alloc(tmp);
    }

    CbuildElabInfo* elabInfo = castStoreLeafBOM(storage->getBOMData());
    elabInfo->putConstantVal(carbonConstVal);
  }
}

void CbuildSymTabBOM::setTypeIndex(STAliasedLeafNode* leaf, UInt32 typeIndex)
{
  STAliasedLeafNode* storage = leaf->getStorage();
  if (isStorageNode(storage))
  {
    CbuildElabInfo* elabInfo = castStoreLeafBOM(storage->getBOMData());
    elabInfo->putTypeIndex(typeIndex);
  }
}

void CbuildSymTabBOM::setIntrinsic(STAliasedLeafNode* leaf, const IODBIntrinsic* intrinsic)
{
  CbuildElabInfo* elabInfo = getElabInfo(leaf);
  elabInfo->putIntrinsic(intrinsic);
}

const DynBitVector* CbuildSymTabBOM::getConstantVal(const STAliasedLeafNode* leaf)
{
  const DynBitVector* val = NULL;
  const STAliasedLeafNode* storage = leaf->getStorage();
  if (isStorageNode(storage))
  {
    const CbuildElabInfo* elabInfo = castStoreLeafBOM(storage->getBOMData());
    val = elabInfo->getConstantVal();
  }
  return val;
}

const IODBIntrinsic* CbuildSymTabBOM::getIntrinsic(const STAliasedLeafNode* leaf)
{
  const CbuildElabInfo* elabInfo = getElabInfoConst(leaf);
  return elabInfo->getIntrinsic();
}

void CbuildSymTabBOM::putUserType(STSymbolTableNode* node, const UserType* ut)
{
  if (node->castBranch()) {
    CbuildBranchDataBOM* bom = castBranchBOM(node->getBOMData());
    bom->putUserType(ut);
  } else {
    CbuildElabInfo* elabInfo = getElabInfo(node->castLeaf());
    elabInfo->putUserType(ut);
  }
}

const UserType* CbuildSymTabBOM::getUserType(const STSymbolTableNode* node)
{
  const UserType* ut = NULL;
  if (node->castBranch()) {
    CbuildBranchDataBOM* bom = castBranchBOM(node->getBOMData());
    ut = bom->getUserType();
  } else {
    CbuildElabInfo* elabInfo = getElabInfo(node->castLeaf());
    ut = elabInfo->getUserType();
  }
  return ut;
}

void CbuildSymTabBOM::markStorageNodes(IODB* db)
{  
  // This loop is definitely needed with the old allocator. I'm not
  // sure about the new one.
  STSymbolTable* symTab = db->getDesignSymbolTable();
  for (STSymbolTable::NodeLoop symIter = symTab->getNodeLoop();
       ! symIter.atEnd(); 
       ++symIter) {
    STSymbolTableNode * node = *symIter;
    STAliasedLeafNode * leaf = node->castLeaf();
    if (leaf) 
    {
      // Check if this node is supposed to be a storage node
      if (leaf->getAlias() == leaf)
      {
        // rings that are greater than 1 in size will either be dead
        // or live, but should be correct by now. Nodes that do not
        // have aliases may or may not be allocated.
        
        // ringlen is 1. Is it allocted?
        const STAliasedLeafNode* master = leaf->getMaster();
        ST_ASSERT(master == leaf, leaf);
        const NUBase* base = getNucleusObj(master);
        if (base)
        {
          const NUNetElab* netElab = dynamic_cast<const NUNetElab*>(base);
          if (netElab)
          {
            const NUNet* net = netElab->getNet();
            if (net->isAllocated())
              leaf->setThisStorage();
          }
        }
      }
    }
  }
}

bool CbuildSymTabBOM::hasMeaningfulTagId(const STAliasedLeafNode* leaf)
{
  bool ret = false;
  ret = isTypeTagValid(leaf);
  
  if (ret)
  {
    int typeTag = getTypeTag(leaf);
    ret =  typeTag != CbuildShellDB::eNoTypeId;
    if (ret && (typeTag == CbuildShellDB::eOffsetId))
      ret = isTypeIndexValid(leaf);
  }
  return ret;
}

CbuildShellDB::NodeFlags 
CbuildSymTabBOM::getNodeFlags(const STAliasedLeafNode* leaf)
{
  const STAliasedLeafNode* storage = leaf->getStorage();
  const CbuildElabInfo* elabInfo = getElabInfoConst(storage);
  return elabInfo->getNodeFlags();
}

bool CbuildSymTabBOM::isForceSubordinate(const STAliasedLeafNode* leaf)
{
  const STAliasedLeafNode* storage = leaf->getStorage();
  const CbuildElabInfo* elabInfo = getElabInfo(storage);
  return elabInfo->isForceSubordinate();
}

void CbuildSymTabBOM::markForceSubordinate(STAliasedLeafNode* leaf)
{
  STAliasedLeafNode* storage = leaf->getStorage();
  CbuildElabInfo* elabInfo = getElabInfo(storage);
  elabInfo->markForceSubordinate();
}

void CbuildSymTabBOM::markDepositableNet(STAliasedLeafNode* leaf)
{
  STAliasedLeafNode* storage = leaf->getStorage();
  CbuildElabInfo* elabInfo = getElabInfo(storage);
  elabInfo->markDepositable();
}

void CbuildSymTabBOM::markObservableNet(STAliasedLeafNode* leaf)
{
  STAliasedLeafNode* storage = leaf->getStorage();
  CbuildElabInfo* elabInfo = getElabInfo(storage);
  elabInfo->markObservable();
}

void CbuildSymTabBOM::markInvalidWaveNet(STAliasedLeafNode* leaf)
{
  STAliasedLeafNode* storage = leaf->getStorage();
  CbuildElabInfo* elabInfo = getElabInfo(storage);
  elabInfo->markInvalidWaveNet();
}

bool CbuildSymTabBOM::isInvalidWaveNet(const STAliasedLeafNode* leaf)
{
  const STAliasedLeafNode* storage = leaf->getStorage();
  const CbuildElabInfo* elabInfo = getElabInfo(storage);
  return elabInfo->isInvalidWaveNet();
}

void CbuildSymTabBOM::markClockNet(STAliasedLeafNode* leaf)
{
  STAliasedLeafNode* storage = leaf->getStorage();
  CbuildElabInfo* elabInfo = getElabInfo(storage);
  elabInfo->markClockNet();
}

bool CbuildSymTabBOM::isClockNet(const STAliasedLeafNode* leaf)
{
  const STAliasedLeafNode* storage = leaf->getStorage();
  const CbuildElabInfo* elabInfo = getElabInfo(storage);
  return elabInfo->isClockNet();
}

void CbuildSymTabBOM::markStateOutput(STAliasedLeafNode* leaf)
{
  STAliasedLeafNode* storage = leaf->getStorage();
  CbuildElabInfo* elabInfo = getElabInfo(storage);
  elabInfo->markStateOutput();
}

bool CbuildSymTabBOM::isStateOutput(const STAliasedLeafNode* leaf)
{
  const STAliasedLeafNode* storage = leaf->getStorage();
  const CbuildElabInfo* elabInfo = getElabInfo(storage);
  return elabInfo->isStateOutput();
}

void CbuildSymTabBOM::markRunDebugSchedule(STAliasedLeafNode* leaf)
{
  STAliasedLeafNode* storage = leaf->getStorage();
  CbuildElabInfo* elabInfo = getElabInfo(storage);
  elabInfo->markRunDebugSchedule();
}

void CbuildSymTabBOM::markComboRun(STAliasedLeafNode* leaf, bool isComboRun)
{
  STAliasedLeafNode* storage = leaf->getStorage();
  CbuildElabInfo* elabInfo = getElabInfo(storage);
  elabInfo->markComboRun(isComboRun);
}

bool CbuildSymTabBOM::isComboRun(const STAliasedLeafNode* leaf)
{
  const STAliasedLeafNode* storage = leaf->getStorage();
  const CbuildElabInfo* elabInfo = getElabInfo(storage);
  return elabInfo->isComboRun();
}

void CbuildSymTabBOM::copyBranchData(const STBranchNode* srcBranch,
                                     STBranchNode* dstBranch,
                                     AtomicCache* atomicCache)
{
  // Copy the bom
  const CbuildBranchDataBOM* srcBom = castBranchBOM(srcBranch->getBOMData());
  CbuildBranchDataBOM* dstBom = castBranchBOM(dstBranch->getBOMData());
  srcBom->copy(dstBom);

  // Register any branch names with the atomic cache so we can write
  // them. We only care about modules right now
  NUBase* base = dstBom->getNucleusObj();
  NUModuleElab* moduleElab = dynamic_cast<NUModuleElab*>(base);
  if (moduleElab != NULL) {
    StringAtom* name = moduleElab->getModule()->getOriginalName();
    atomicCache->intern(name->str());
  }
}

void CbuildSymTabBOM::copyLeafData(const STAliasedLeafNode* srcLeaf,
                                   STAliasedLeafNode* dstLeaf)
{
  const CbuildElabInfo* srcBom = getElabInfoConst(srcLeaf);
  CbuildElabInfo* dstBom = getElabInfo(dstLeaf);
  srcBom->copy(dstBom);
}

void CbuildSymTabBOM::copyStorageLeafInfo(const STAliasedLeafNode* srcLeaf,
                                          STAliasedLeafNode* dstLeaf)
{
  const CbuildElabInfo* srcBom = getElabInfoConst(srcLeaf->getStorage());
  CbuildElabInfo* dstBom = getElabInfo(dstLeaf);
  srcBom->copyStorageInfo(dstBom);
}



//! Return BOM class name
const char* CbuildSymTabBOM::getClassName() const
{
  return "CbuildSymTabBOM";
}

//! Write the BOMData for a branch node
void CbuildSymTabBOM::xmlWriteBranchData(const STBranchNode* /*branch*/,UtXmlWriter* /*writer*/) const
{
}

//! Write the BOMData for a leaf node
void CbuildSymTabBOM::xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/,UtXmlWriter* /*writer*/) const
{
}
