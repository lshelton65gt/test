// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "compiler_driver/CarbonContext.h"
#include "util/Stats.h"
#include "util/UtStream.h"
#include "util/UtIOStream.h"
#include "util/UtStackTrace.h"

int main(int argc, char **argv)
{
  int status = 0;

  // Tell UtStackTrace main()'s frame addess so it can avoid looking too
  // far on the stack.
#if pfGCC
  UtStackTrace::putMainFrame(__builtin_frame_address(0));
#endif

  // Initialize the memory system and establish a context for leak
  // detection.  Defining the environment variable CARBON_CBUILD_MEM_DUMPFILE
  // to a file location will turn on memory dumping (same as -dumpMemory).
  CarbonMem carbonMem(&argc, argv, getenv("CARBON_CBUILD_MEM_DUMPFILE"));

#ifdef CDB
  extern void cdbInit();
  cdbInit();
#endif

  // This is not ready yet...
#define TOKENS_FILE_TEST_HARNESS 0
#if TOKENS_FILE_TEST_HARNESS 
  // In order to test whether tokens-file generation really works
  // the best thing is to run all the regression tests in a mode where
  // we:
  //    1. Parse Verilog & Generate a tokens file
  //    2. quit out of the main compiler loop
  //    3. delete all the compiler objects
  //    4. re-execute the same command line, substituting the
  //       tokens file in place of all the Verilog files, CEM files,
  //       -y directories, -v files, etc.
  //    5. continue the test normally.
  // In theory this should only produce a couple of uglies that have
  // to be manually examined.  All the parser warnings will come out
  // twice, once with the proper line numbers and once with the
  // tokens-file line numbers.
  bool testTokensFile = getenv("CARBON_TEST_TOKENS_FILE") != NULL;
#endif

  Stats* stats = new Stats(stdout);
  stats->putEchoStats(false);
  CarbonContext* cc = new CarbonContext(&argc, argv, stats);

  ArgProc* args = cc->getArgs();

#if TOKENS_FILE_TEST_HARNESS 
  if (testTokensFile) {
    cc->putTestTokensFile(true);
  }
#endif

  if (!cc->run())
  {
    status = 1;
    carbonMem.disableLeakDetection();
  }

  // Print out a summary information if necessary.  Don't print anything
  // if we completed successfully and had -q on the command line.
  MsgContext *mc = cc->getMsgContext();
  bool quiet = args->getBoolValue( "-q" );
  bool nocc = args->getBoolValue( CRYPT ("-nocc") );
  ArgProc::StrIter iter;
  args->getStrIter ( "-cemgen", &iter );
  bool cemgen = not iter.atEnd ();
  
  if (!quiet && mc->getExitStatus() == 0)
  {
    if (cc->protectingSource())
    {
      // running in +protect mode; print protected file summary
      mc->ProtectSuccess(cc->protectedSourceCount());
    }
    else if ( cc->getLastPhase() == CarbonContext::eParseLanguages && 
              ! cemgen)
    {
      // compiling a vhdl library only, no backend compile.
      mc->SuccessMessage("VHDL library");
    }
    else if (not nocc and not cemgen)
    {
      // not +protect mode and not -nocc, print 'created libdesign.a' message
      const char *thingCompleted;
      args->getStrValue( "-o", &thingCompleted );
      mc->SuccessMessage( thingCompleted );
    }
  }  
  if (!quiet || mc->getExitStatus() != 0)
  {
    mc->MessageSummary( mc->getSeverityCount( MsgContext::eWarning ),
                        mc->getSeverityCount( MsgContext::eError ),
                        mc->getSeverityCount( MsgContext::eAlert ));
  }

  // release the license if we haven't already
  cc->destroyLicense();
  
  bool doStats = 
    args->getBoolValue("-phaseStats") || args->getBoolValue("-stats");
  
  CarbonMem::flush();

  if (carbonMem.isCheckingLeaks())
  {
    // only delete memory in debug mode -- deleting our whole world is
    // slow and will cause massive page-faulting prior to exit, and
    // there is no benefit in production.
    char memCheckpointName[1000];
    bool isDumpMem = cc->isDumpMemory();
    if (isDumpMem)
    {
      const char* fileRoot = cc->getFileRoot();  // will be deleted by "delete cc"!
      strncpy(memCheckpointName, fileRoot, 900);
      memCheckpointName[900] = '\0';
      strcat(memCheckpointName, ".mem.end");
    }
    delete cc;
    if (doStats)
      stats->printTotalStatistics(); // to see the memory after freeing
    delete stats;
  }
  return status;
} // int main
