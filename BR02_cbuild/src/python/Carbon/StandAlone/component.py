#******************************************************************************
# Copyright (c) 2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************
from Carbon import cpp

class StandAloneDebugSlaveClass(cpp.Class):
  def __init__(self, className, compName, xtor):
    cpp.Class.__init__(self, className, ['public CarbonDebugAccessIF'])
    self.__compName = compName
    self.__xtor = xtor
    
    func = self.addConstructor('(%(compName)s* owner)'% {'compName':compName})

    func = self.addMemberFunction('CarbonDebugAccessStatus', 'debugMemRead', '(uint64_t addr, uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0)')
    func.addCode(self.debugMemAccess('Read'))

    func = self.addMemberFunction('CarbonDebugAccessStatus', 'debugMemWrite', '(uint64_t addr, const uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0)')
    func.addCode(self.debugMemAccess('Write'))

    func = self.addMemberFunction('void', 'setBaseAddress', '(uint64_t addr)')
    func.addCode('mBaseAddr = addr;')
    
    func = self.addMemberFunction('uint64_t', 'getBaseAddress', '()')
    func.addCode('return mBaseAddr;')
    
    func = self.addMemberFunction('CarbonDebugAccessStatus', 'debugAccess', '(CarbonDebugAccessDirection dir, uint64_t addr, uint32_t numBytes, uint8_t* buf, uint32_t* numBytesProcessed, uint32_t* ctrl)')
    func.addCode(self.debugAccess())
                 
    var = self.addMemberVariable('std::string', 'mName')
    var.setConstructorInit('"%s"' % className)
    
    var = self.addMemberVariable(compName + '*', 'mOwner')
    var.setConstructorInit('owner')

    var = self.addMemberVariable('uint64_t', 'mBaseAddr')
    var.setConstructorInit('0')

  # Are there any registers or memories that connected to this port
  def useThisPortWithCadi(self):
    for cadi in self.__xtor.config().loopCadi():
      for memory in cadi.debugMemories():
        if memory.eslPort() == self.__xtor.name():
          return True
    return False

  def debugAccess(self):
    code = '%s* comp = mOwner;\n' % self.__compName
    code += self.__xtor.debugAccess(None)
    return code
  
  def debugMemAccess(self, direction):
    code =  """uint8_t* data = const_cast<uint8_t*>(buf);
uint32_t numBytesProcessed = 0;
return debugAccess(eCarbonDebugAccess%(dir)s, addr, numBytes, data, &numBytesProcessed, ctrl);
""" % {'dir':direction}
    return code

class StandAloneDebugMasterClass(cpp.Class):
  def __init__(self, className, compName, xtor):
    cpp.Class.__init__(self, className, ['public %s_DebugMasterIF' % compName])
    
    func = self.addConstructor('(%(compName)s* owner)'% {'compName':compName})
    
    # debugAccess is compatible with our CADI implementation
    func = self.addMemberFunction('CarbonDebugAccessStatus', 'debugAccess',
                                  '(CarbonDebugAccessDirection dir, uint64_t startAddress, uint32_t numBytes, uint8_t *data, uint32_t *numBytesProcessed, uint32_t* ctrl = 0)')
    func.addCode("""    if (mCallbackObj == NULL) return eCarbonDebugAccessStatusError;
  CarbonDebugAccessStatus status = eCarbonDebugAccessStatusOk;  
  if ( dir == eCarbonDebugAccessRead ){
    status = mCallbackObj->debugMemRead( startAddress, data, numBytes, ctrl);
  } else {
    status = mCallbackObj->debugMemWrite( startAddress, data, numBytes, ctrl);
  }
  if (status == eCarbonDebugAccessStatusOk)
    return eCarbonDebugAccessStatusOk;
  else
    return eCarbonDebugAccessStatusError;
""")
    
    func = self.addMemberFunction('void', 'registerDebugAccessCB', '(CarbonDebugAccessIF* cbObj)')
    func.addCode('  mCallbackObj = cbObj;')
  
    var = self.addMemberVariable('CarbonDebugAccessIF*', 'mCallbackObj')
    var.setConstructorInit('0')

    var = self.addMemberVariable('std::string', 'mName')
    var.setConstructorInit('"%s"' % className)
    
    var = self.addMemberVariable(compName + '*', 'mOwner')
    var.setConstructorInit('owner')
    
class Component(cpp.Class):
  def __init__(self, topConfig, compConfig, isTopLevel):
    self.__topConfig  = topConfig
    self.__compConfig = compConfig
    self.__isTopLevel = isTopLevel

    # Instantiate sub components
    self.__subComps = []
    if isTopLevel:
      for comp in compConfig.subComps():
        self.__subComps.append(Component(topConfig, comp, isTopLevel=False))
    
    # Setup CPP class
    cpp.Class.__init__(self, compConfig.compName(), ['public CarbonStandAloneComponentIF'])

    # If they asked for the debuggable point, create it
    self.__compConfig.addDebuggablePointMemberFunctions(self)

    # Instantiate Debug Master and Slave Ports
    self.__masterPorts = []
    self.__slavePorts = []
    if self.__isTopLevel:
      for port in compConfig.compPorts():
        if port.isTransactionMaster():
          self.__masterPorts.append(StandAloneDebugMasterClass(compConfig.compName() + "_" + port.name() + '_dbgMaster', port.compName(), port))
        if port.isTransactionSlave():
          self.__slavePorts.append(StandAloneDebugSlaveClass(compConfig.compName() + "_" + port.name() + '_dbgSlave', port.compName(), port))
          
    # Constructor
    constructor_args = '()'
    if not isTopLevel: constructor_args = '(%s* parent)' % compConfig.parentConfig().compName()
    func = self.addConstructor(constructor_args)
    func.addCode(self.constructor())
    
    # Retreive Carbon Object
    func = self.addMemberFunction('CarbonObjectID*', 'getCarbonObject')
    func.addCode(self.getCarbonObject())
    
    # Returns number of supported CADI interfaces.
    func = self.addMemberFunction('unsigned int', 'numberOfCADIIfs')
    func.addCode(self.numberOfCADIIFs())

    # Returns specified CADI object
    func = self.addMemberFunction('eslapi::CADI*', 'getCADI', '(unsigned int index)')
    func.addCode(self.getCADI())
                 
    # Loads an application file
    func = self.addMemberFunction('void', 'loadFile', '(const char* filename)')
    func.addCode(self.loadFile())
    
    # Stop at debuggable point
    func = self.addMemberFunction('void', 'stopAtDebuggablePoint', '(unsigned int core, bool stop)')
    func.addCode(self.stopAtDebuggablePoint())

    # Can Stop
    func = self.addMemberFunction('bool', 'canStop', '(unsigned int core)')
    func.addCode(self.canStop())

    # Init
    func = self.addMemberFunction('void', 'init')
    func.addCode(self.initCarbonModel())
    func.addCode(self.initCadi())
    func.addCode(self.callSubComp('init'))

    # Reset
    func = self.addMemberFunction('void', 'reset')
    func.addCode(self.callSubComp('reset'))

    # Pre Update
    if not self.__isTopLevel:
      func = self.addMemberFunction('void', 'subUpdate', '(int type)')
      # Note: The Post custom code section is used to manipulate the PC that CADI sees.
      # The CADI update call, checks the PC is there is a software breakpoint setup
      # to see if a breakpoint should occur so the CADI update call needs to be
      # after the post custom code section. if the callCADIUpdate functions is
      # changed. Please make sure that the custom code position is still at the
      # correct place (before the CADI update call). Update userCodePos accordningly
      # if not.
      func.addPreCode("""if (type == 0) { // Pre Update""")
      func.addPreMainCode("""
} else { // Post Update\n""")
      func.addPostCode("""
%(cadiUpdate)s
}""" % {'cadiUpdate': self.callCADIUpdate()} )
      func.setCustomCodeName('update')

    # Update
    func = self.addMemberFunction('void', 'update', '(CarbonTime time)')
    func.addCode(self.callSubComp('subUpdate', '0'))
    if self.__isTopLevel: func.addCode('carbonSchedule(mCarbonObject, time);\n')
    func.addCode(self.callSubComp('subUpdate', '1'))
    func.addPostCode(self.callCADIUpdate())

    # Terminate
    func = self.addMemberFunction('void', 'terminate')
    func.addCode(self.callSubComp('terminate'))
    func.addCode(self.terminateCadi())
    func.addCode(self.terminateCarbonModel())

    # Register Debug access callback
    func = self.addMemberFunction('bool', 'registerDebugAccessCB', '(const char* portName, CarbonDebugAccessIF* cbObj)')
    func.addCode(self.registerDebugAccessCB())
    
    # Retrieve debug access interface for the given port
    func = self.addMemberFunction('CarbonDebugAccessIF*', 'retrieveDebugAccessIF', '(const char* portName)')
    func.addCode(self.retrieveDebugAccessIF());
    
    # Debug transaction from Slave ports
    if self.hasDebugSlaves():
      func = self.addMemberFunction('CarbonDebugAccessStatus', 'debugAccess',
                                    '(const std::string& callingSlave, CarbonDebugAccessDirection dir, uint64_t addr, uint32_t numBytes, uint8_t* buf, uint32_t* numBytesAccepted, uint32_t* ctrl)')
      func.addPreCode('CarbonDebugAccessStatus status = eCarbonDebugAccessStatusOutRange;\n')
      func.addPostCode('\nreturn status;')

    # Parameters, this is the external interface that has no std::string
    # they will call the internal ones that matches the SoCDesigner once
    # for easy porting
    func = self.addMemberFunction('void', 'setComponentParameter', '(const char* name, const char* value)')
    func.addCode('''
std::string strName(name);
std::string strValue(value);
setParameter(strName, strValue);''')

    func = self.addMemberFunction('void', 'getComponentParameter', '(const char* name, char* value, uint32_t maxLen)')
    func.addCode('''
std::string strName(name);
std::string strValue = getParameter(strName);
strncpy(value, strValue.c_str(), maxLen-1);
value[maxLen-1] = '\\0';
''')

    # Local values for parameters and a function to define them
    func = self.addMemberFunction('void', 'defineParameter', '(const std::string& name, const std::string& value)')
    func.addCode('mParameters[name] = value;')
    self.addMemberVariable('std::map<std::string, std::string>', 'mParameters');
    
    # These are the internal methods that uses std::string to match SoCDesigner's implementation
    func = self.addMemberFunction('void', 'setParameter', '(const std::string& name, const std::string& value)')
    func.addCode('''\
// Only set the value if it has been defined
std::map<std::string, std::string>::iterator pos = mParameters.find(name);
if (pos != mParameters.end()) {
  pos->second = value;
}
''')
    func = self.addMemberFunction('std::string', 'getParameter', '(const std::string& key)')
    func.addCode('''\
// Check if it has been defined
std::map<std::string, std::string>::iterator pos = mParameters.find(key);
if (pos != mParameters.end()) {
  return pos->second;
} else {
   return "";
}
''')
    
    # Get Top Component
    if self.__isTopLevel:
      func = self.addMemberFunction(self.__compConfig.compName() + '*', 'getTopComp')
      func.addCode('return this;')
      func = self.addMemberFunction('CarbonStandAloneComponentIF*', 'getParent')
      func.addCode('return NULL;')
    else:
      func = self.addMemberFunction(self.__compConfig.parentConfig().compName() + '*', 'getTopComp')
      func.addCode('return mParent;')
      func = self.addMemberFunction('CarbonStandAloneComponentIF*', 'getParent')
      func.addCode('return mParent;')
            
    # Member Variables
    var = self.addMemberVariable('CarbonObjectID*', 'mCarbonObject', 'private')
    var.setConstructorInit('0')

    var = self.addMemberVariable('eslapi::CADI*', 'mCADI', 'private')
    var.setConstructorInit('0')

    if self.__isTopLevel:
      if self.__compConfig.numSubComponents() > 0:
        var = self.addMemberVariable('CarbonStandAloneComponentIF*', 'mSubComps', 'private', self.__compConfig.numSubComponents())
    else:
      var = self.addMemberVariable(self.__compConfig.parentConfig().compName() + '*', 'mParent', 'private')
      var.setConstructorInit('parent')

    # Debug Slave Ports
    if self.__isTopLevel and self.hasDebugSlaves():
      var = self.addMemberVariable('std::map<std::string,CarbonDebugAccessIF*>', ' mDebugSlaves', 'private')

    # Debug Master Ports
    if self.__isTopLevel and self.hasDebugMasters():
      var = self.addMemberVariable('std::map<std::string,%s_DebugMasterIF*>' % compConfig.compName(), ' mDebugMasters', 'private')
      for port in self.__compConfig.compPorts():
        if port.isTransactionMaster():
          var = self.addMemberVariable(compConfig.compName() + "_" + port.name() + '_dbgMaster*', port.instanceName(), 'public')

  def compConfig(self):
    return self.__compConfig
  
  def subComps(self):
    return self.__subComps

  def hasDebugMasters(self):
    if self.__isTopLevel:
      for trans in self.__compConfig.compPorts():
        if trans.isTransactionMaster(): return True
    return False
    
  def hasDebugSlaves(self):
    if self.__isTopLevel:
      for trans in self.__compConfig.compPorts():
        if trans.isTransactionSlave(): return True
    return False

  def debugMasterBase(self):
    code = ''
    if self.hasDebugMasters():
      code = """
// Debug transaction master interface class
class %(compName)s_DebugMasterIF {
public:
  virtual void registerDebugAccessCB(CarbonDebugAccessIF* cbObj) = 0;
};""" % {'compName': self.__compConfig.compName() } 
    return code
  
  def emitImplementation(self, userCode, config):
    code = ''

    # cpp-include custom code for sub components
    if not self.__isTopLevel:
      code += config.getCustomCodePre('cpp-include')
      code += config.getCustomCodePost('cpp-include')
    
    # Emit Master Ports
    for port in self.__masterPorts:
      code += port.emitImplementation(userCode, None)
      
    # Emit Slave Ports
    for port in self.__slavePorts:
      code += port.emitImplementation(userCode, None)
      
    # Then emit myself
    code += cpp.Class.emitImplementation(self, userCode, config)

    return code

  def emitDefinition(self, userCode, config):
    code = ''

    # Base class for debug master
    if self.__isTopLevel:
      # Emit debug transaction classes
      code += self.debugMasterBase()

    # Emit Master Ports
    for port in self.__masterPorts:
      code += port.emitDefinition(userCode, None)
      
    # Emit Slave Ports
    for port in self.__slavePorts:
      code += port.emitDefinition(userCode, None)
      
    # Then emit myself
    code += cpp.Class.emitDefinition(self, userCode, config)

    return code
    
  def constructor(self):
    code = ''
    # Instantiate sub components
    if self.__isTopLevel:
      for i in range(len(self.__subComps)):
        code += 'mSubComps[%d] = new %s(this);\n' % (i, self.__subComps[i].name())

    # Instantiate debug ports
    if self.hasDebugSlaves() or self.hasDebugMasters():
      code += '\n// Debug Ports\n'
    if self.__isTopLevel:
      for port in self.__compConfig.compPorts():
        if port.isTransactionSlave():
          code += 'mDebugSlaves["%(shortname)s"] = new %(name)s_dbgSlave(this);\n' % {'shortname': port.name(), 'name': self.__compConfig.compName() + "_" + port.name() }
        if port.isTransactionMaster():
          code += '%(instName)s = new %(name)s_dbgMaster(this);\nmDebugMasters["%(shortname)s"] = %(instName)s;\n' % {
            'instName': port.instanceName(), 'name': self.__compConfig.compName() + "_" + port.name(), 'shortname': port.name() }

    # Define parameters
    if self.__isTopLevel:
      for param in self.__compConfig.parameters():
        code += 'defineParameter("%(name)s", "%(value)s");\n' % { 'name' : param.name(),
                                                                  'value': param.value() }

    return code

  def getCarbonObject(self):
    if self.__isTopLevel:    
      return 'return mCarbonObject;'
    else:
      return 'return mParent->getCarbonObject();'

  def numberOfCADIIFs(self):
    # Number of CADI interfaces is the number of sub components + top level CADI Interface (if any)
    numCADIs = 0

    # Check if there is a top level CADI
    if self.__compConfig.mxdi().hasCadi(): numCADIs += 1

    # Sub components always has a CADI interface because thats the only reason they exists
    if self.__isTopLevel: numCADIs += self.__compConfig.numSubComponents()

    # Emit code
    return 'return %d;' % numCADIs

  def getCADI(self):
    code = """if (index == 0)
  return mCADI;
"""
    if self.__isTopLevel and self.__compConfig.numSubComponents() > 0: code += """
else if (numberOfCADIIfs() > 1)
  return mSubComps[index-1]->getCADI(0);
"""
    code += """else
  return 0;"""

    return code

  def loadFile(self):
    code = ''
    if self.__compConfig.mxdi().ELFLoader() != None:
      code = """
%(compName)s_CADI* cadi = static_cast<%(compName)s_CADI*>(mCADI);
cadi->loadFile(filename);
""" % {'compName':self.__compConfig.compName() }

    return code
  
  def stopAtDebuggablePoint(self):
    code = ''
    if self.__isTopLevel and self.__compConfig.numSubComponents() > 0:
      code += 'if (core > 0) mSubComps[core-1]->stopAtDebuggablePoint(0, stop);\n'
    elif self.__compConfig.useDebuggablePoint():
      code += 'procStopAtDebuggablePoint(stop);\n'
    return code

  def canStop(self):
    code = ''
    if self.__isTopLevel and self.__compConfig.numSubComponents() > 0:
      code += 'if (core > 0) return mSubComps[core-1]->canStop(0);\n'
      code += 'return false;\n'
    elif self.__compConfig.useDebuggablePoint():
      code += 'return procCanStop();\n'
    else:
      code += 'return false;\n'
    return code

  def initCarbonModel(self):
   if self.__isTopLevel:
     return """
// Create Carbon Model
mCarbonObject = carbon_%(carbon_obj)s_create(eCarbonAutoDB, eCarbon_NoFlags);
if (mCarbonObject == 0) {
  std::cout << "Carbon Model failed to initialize." << std::endl;
  exit(1);
}
""" % {'carbon_obj': self.__compConfig.objectName() }
   return ''

  def initCadi(self):
    code = ''
    if self.__compConfig.mxdi().hasCadi():
      code = """
// Instantiate CADI class
mCADI = new %(cadiClassName)s(this);
""" % {'cadiClassName':self.__compConfig.mxdi().mxdiClassName()}
      
    return code
  
  def terminateCarbonModel(self):
   if self.__isTopLevel:
     return """
// Destroy the Carbon Model
carbonDestroy(&mCarbonObject);
""" % {'carbon_obj': self.__compConfig.objectName() }
   return ''

  def terminateCadi(self):
    code = ''
    if self.__compConfig.mxdi().hasCadi():
      code = """
// Destroy CADI class
delete mCADI;
"""
    return code
  
  def callSubComp(self, method, params=''):
    # Call all the sub components
    code = ''
    if self.__isTopLevel and self.__compConfig.numSubComponents() > 0:
      code += """
for (uint32_t i = 0; i < %(numSubComps)d; i++) {
  mSubComps[i]->%(method)s(%(params)s);
}
""" % {'numSubComps': self.__compConfig.numSubComponents(),
       'method': method,
       'params': params}
    
    return code

  def callCADIUpdate(self):
    code = ''
    if self.__compConfig.mxdi().hasCadi():
      code += '%(compName)s_CADI* cadi = static_cast<%(compName)s_CADI*>(mCADI);\n' % {'compName': self.__compConfig.compName() }
      code += 'cadi->update();\n'
    return code

  def retrieveDebugAccessIF(self):
    code = 'return false;'
    if self.__isTopLevel and self.hasDebugSlaves():
      code = """if (mDebugSlaves.find(portName) != mDebugSlaves.end())
  return mDebugSlaves[portName];
return 0;"""

    return code

  def registerDebugAccessCB(self):
    code = 'return false;'
    if self.__isTopLevel and self.hasDebugMasters():
      code = """if (mDebugMasters.find(portName) != mDebugMasters.end()) {
  mDebugMasters[portName]->registerDebugAccessCB(cbObj);
  return true;
}
return false;"""

    return code

        
