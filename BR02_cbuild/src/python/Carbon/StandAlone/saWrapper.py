#******************************************************************************
# Copyright (c) 2009-2013 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

# $Revision: 1.21 $

import sys
import os
import re
import string
import datetime
import Carbon.outputStream
from Carbon.carboncfg import *

def headerComment(config, output, filename,
                  filetype = 'C++',
                  line_comment = '//',
                  open_block_comment = '/*',
                  cont_block_comment = ' *',
                  close_block_comment = '*/'):
  header_comment = """\
%(lineComment)s -*- %(fileType)s -*-
%(openBlockComment)s
%(contBlockComment)s This file contains generated source code for a Stand Alone component
%(contBlockComment)s that interfaces with a Carbon Model.
%(contBlockComment)s
%(contBlockComment)s File: %(fileName)s
%(contBlockComment)s Date: %(date)s
%(contBlockComment)s
%(contBlockComment)s Component Name: %(compName)s
%(contBlockComment)s
%(contBlockComment)s Carbon Model Top Level Module: %(topLevelModule)s
%(contBlockComment)s Carbon Model I/O Database: %(iodb)s
%(contBlockComment)s Carbon Model Interface Name: %(target)s
%(contBlockComment)s
%(contBlockComment)s Carbon Component Generator version: %(version)s
%(contBlockComment)s Configuration file: %(ccfgFile)s
%(contBlockComment)s
%(contBlockComment)s To generate this file:
%(contBlockComment)s   (unix)    $(CARBON_HOME)/bin/carbon carmgr %(ccfgFile)s
%(contBlockComment)s   (windows) %%CARBON_HOME%%\\bin\\carbon carmgr %(ccfgFile)s
%(closeBlockComment)s
""" % { 'date': datetime.datetime.now().ctime(),
        'compName': config.compName(),
        'topLevelModule': config.topModuleName(),
        'iodb': config.ioDbFile(),
        'target': config.objectName(),
        'version': output.version(),
        'ccfgFile': config.ccfgFile(),
        'fileName': filename,
        'fileType': filetype,
        'lineComment': line_comment,
        'openBlockComment': open_block_comment,
        'contBlockComment': cont_block_comment,
        'closeBlockComment': close_block_comment}

  return header_comment

def user_source_files(config):
  code = ""
  for source in config.sourceFiles():
    code += '%(source)s ' % { "source" : source, }
  return code

def user_include_files(config):
  code = ""
  for include in config.includeFiles():
    code += '#include "%(include)s"\n' % { "include" : include, }
  return code

def h_include(config, output):
  return """
%(customCodePreInclude)s
#include <string>
#include <map>
#include "carbon/carbon_capi.h"
#include "carbon/CarbonStandAloneComponentIF.h"
#include "carbon/CarbonDebugAccess.h"
%(customCodePostInclude)s
// Forward declarations
class %(className)s;
""" % {
  'className': config.compName(),
  'customCodePreInclude': config.getCustomCodePre('h-include'),
  'customCodePostInclude': config.getCustomCodePost('h-include')
  }
  
def cpp_include(config, output):
  # Get a list of include files
  files = [cadi.includeFile() for cadi in config.loopCadi()]

  cadi_includes = ''
  if files:
    cadi_includes = '\n// CADI Include Files\n'
    for include in files:
      cadi_includes += '#include "%(include)s"\n' % { "include" : include, }

  return """
%(customCodePreInclude)s
#include "sa.%(component)s.h"
#include "carbon/CarbonStandAloneComponentIF.h"
#include "lib%(carbonObj)s.h"
#include <iostream>
#include <map>
%(userIncludes)s
%(cadiIncludes)s
%(customCodePostInclude)s
""" % {
  'component': config.compName(),
  'carbonObj': config.objectName(),
  'userIncludes': user_include_files(config),
  'cadiIncludes': cadi_includes,
  'customCodePreInclude': config.getCustomCodePre('cpp-include'),
  'customCodePostInclude': config.getCustomCodePost('cpp-include')
  }

def emit_unix_makefile(config, output, make_file_name):

  make_template = headerComment(config, output, make_file_name,
                                'makefile', '#', '#', '#', '#')
  make_template += """\

VHM_DIR = .

%(libmk_home)s

ifeq (x$(CARBON_MK_HOME),x)
  $(error The CARBON_MK_HOME environment variable must be set to compile a standalone component.)
endif
ifneq ($(shell test -f $(CARBON_HOME)/include/carbon/carbon.h && echo yes),yes)
  $(error The CARBON_HOME environment variable is not set to a Carbon installation directory)
endif
ifneq ($(shell (test -f $(VHM_DIR)/lib%(carbon_obj)s.a || test -f $(VHM_DIR)/lib%(carbon_obj)s.so) && echo yes),yes)
  $(error This must be run in a directory containing a compiled Unix Carbon Model (lib%(carbon_obj)s.a or lib%(carbon_obj)s.so))
endif

# Check if specified g++ version is supported
CXX ?= g++
GCCVER := $(shell $(CXX) -dumpversion)
ifneq ($(GCCVER),3.2.3)
ifneq ($(GCCVER),3.4.4)
ifneq ($(GCCVER),4.2.2)
  $(error $(GCCVER) is not a supported gcc version. Supported versions are gcc 3.2.3, gcc 3.4.4, and gcc 4.2.2)
endif
endif
endif

include $(CARBON_HOME)/makefiles/Makefile.common

PACKAGE    = %(mod_name)s

# CARBON_M_FLAG adds -m32 when necessary
BUILDFLAGS     = -O2 $(CARBON_M_FLAG)

# User-specified compile-link flags
USER_CXX_FLAGS = %(cxx_flags)s
USER_LINK_FLAGS = %(link_flags)s

CXXFLAGS     = -I$(VHM_DIR) -I$(CARBON_HOME)/include -I$(CARBON_MK_HOME)/include -DCARBON_STANDALONE_COMPONENT=1
CXXFLAGS    += $(USER_CXX_FLAGS)
LDFLAGS      = -L$(VHM_DIR) -l%(carbon_obj)s

LDFLAGS += $(USER_LINK_FLAGS)
LDFLAGS += $(CARBON_LIB_LIST)

PACKAGE_SOURCES = sa.%(mod_name)s.cpp %(cadi_sources)s %(user_source_files)s

ARCHIVE = lib$(PACKAGE).sa.a

.PHONY : compile
compile : $(ARCHIVE)
\t@echo $@ > carbon_make_complete

$(ARCHIVE) : $(PACKAGE_SOURCES:.cpp=.o) %(makeFileName)s
\trm -f $(ARCHIVE)
\t$(CARBON_AR) cr $@ $(PACKAGE_SOURCES:.cpp=.o)
\t$(CARBON_HOME)/bin/carbon mergelib $@ $(VHM_DIR)/lib%(carbon_obj)s.a
\t$(CARBON_HOME)/bin/carbon mergelib $@ $(CARBON_MK_HOME)/lib/$(CARBON_TARGET_ARCH)/libmodelkit_sa-$(GCCVER).a
\t@echo \"****************************************************\"
\t@echo \"***\"  Created component $@
\t@echo \"****************************************************\"

%%.o : %%.cpp
\t$(CXX) -c $< -o $@ $(BUILDFLAGS) $(CXXFLAGS)

.PHONY: clean 
clean: 
	rm -f $(ARCHIVE)

""" % { "mod_name":   config.compName(),
        "carbon_obj": config.objectName(),
        "makeFileName": make_file_name,
        "cxx_flags": config.cxxFlags(),
        "link_flags": config.linkFlags(),
        "cadi_sources": ' '.join([cadi.sourceFile() + ' ' for cadi in config.loopCadi()]),
        "libmk_home": config.libmkHomeMakefileText("unix"),
	"user_source_files" : user_source_files(config)
      }

  return make_template
  
def emit_windows_makefile(config, output, make_file_name):

  make_template = headerComment(config, output, make_file_name,
                                'makefile', '#', '#', '#', '#')

  # Using raw string so Windows backslashes aren't interpreted by Python.
  vhm_dir = "."
  if os.path.dirname(config.ioDbFile()) != '':
    vhm_dir = os.path.dirname(config.ioDbFile())

  make_template += """\

VHM_DIR = %(vhmdir)s

!IF [cl /nologo _msvc_version.cpp] == 0
!  IF [_msvc_version.exe] == 14
VCVER = vc8
!  ELSE IF [_msvc_version.exe] == 15
VCVER = vc9
!  ELSE IF [_msvc_version.exe] == 16
VCVER = vc10
!  ELSE IF [_msvc_version.exe] == 17
VCVER = vc11
! ELSE
! ERROR Supported version of Visual Studio are 2008 and 2010
!  ENDIF
!ELSE
! ERROR Failed to find MSVC version. Make sure Visual Studio is setup.
!ENDIF

%(libmk_home)s

!IF !EXIST("$(CARBON_MK_HOME)/include/common/CarbonComponentHelper.h")
! ERROR The CARBON_MK_HOME environment variable, $(CARBON_MK_HOME), is not set to an Carbon modelkit library.
!ENDIF
!IF !EXIST("$(CARBON_HOME)/include/carbon/carbon.h")
! ERROR The CARBON_HOME environment variable is not set to a Carbon installation directory
!ENDIF
!IF !EXIST($(VHM_DIR)/lib%(carbon_obj)s.lib)
! ERROR This must be run in a directory containing a static Windows Carbon Model (lib%(carbon_obj)s.lib)
!ENDIF

VHMLIB  = $(VHM_DIR)/lib%(carbon_obj)s.lib
PACKAGE = %(mod_name)s

BUILDFLAGS     = /nologo /MD /W3 /GR /EHsc /O1 /Oi /vmg

# User-specified compile-link flags
USER_CXX_FLAGS = %(cxx_flags)s
USER_LINK_FLAGS = %(link_flags)s

CXXFLAGS     = /I $(VHM_DIR) /I "$(CARBON_MK_HOME)/include" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CARBON_STANDALONE_COMPONENT" /D "NO_IMPORT_ESLAPI" /D "NO_IMPORT_CADI" /D "_CRT_SECURE_NO_DEPRECATE" /I "$(CARBON_HOME)/include" $(XACTOR_INCLUDE) $(USER_CXX_FLAGS)
LDFLAGS      = -L$(VHM_DIR) -l%(carbon_obj)s

LIB_LIST = libcarbon%(libcarbon_version)s.lib libmodelkit_sa_$(VCVER).lib /LIBPATH:"$(CARBON_HOME)\Win\lib" /LIBPATH:"$(CARBON_HOME)\Win\lib\winx\shared" /LIBPATH:"$(CARBON_HOME)\Win\lib\winx" /LIBPATH:"$(CARBON_MK_HOME)\lib\Win"

PACKAGE_OBJECTS = sa.%(mod_name)s.obj %(cadi_object_files)s %(user_object_files)s

LIB     = lib$(PACKAGE).sa.lib

.PHONY: compile
compile: $(LIB)

$(LIB) : $(PACKAGE_OBJECTS) %(makeFileName)s $(VHMLIB)
    lib /out:$@ $(PACKAGE_OBJECTS) $(VHMLIB) $(LIB_LIST) $(USER_LINK_FLAGS)
    @echo $@ > carbon_make_complete
    @echo *******************************************
    @echo ***  Created component $@
    @echo *******************************************

.cpp.obj:
\tcl $(BUILDFLAGS) $(CXXFLAGS) /c $<

.cpp.exe:
\tcl /nologo $<

"""  % { "mod_name":   config.compName(),
        "carbon_obj": config.objectName(),
        "vhmdir": vhm_dir,
        "makeFileName": make_file_name,
        "cxx_flags": config.cxxFlags(),
        "link_flags": config.windowsLinkFlags(),
        "cadi_object_files" : ' '.join([cadi.sourceFile().replace('.cpp', '.obj') for cadi in config.loopCadi()]),
	"user_object_files" : ' '.join([source.replace('.cpp', '.obj') for source in config.sourceFiles()]),
        "libmk_home": config.libmkHomeMakefileText("windows"),
        "libcarbon_version" : Carbon.libcarbonVersion()
         }
         
  return make_template

def sawrapgen(config, output, scUseComponentName):
  import Carbon.StandAlone.component
  import Carbon.SystemC.module

  # Create top level component
  topComp = Carbon.StandAlone.component.Component(topConfig = config, compConfig = config, isTopLevel=True)
    
  # Loop through all the sub components and create them
  subComps = []
  for comp in config.subComps():
    subComps.append(Carbon.StandAlone.component.Component(topConfig = config, compConfig = comp, isTopLevel=False))

  # C++ file names
  cpp_file_name  = "sa." + config.compName() + ".cpp"
  h_file_name    = "sa." + config.compName() + ".h"
  include_guard = "_" + h_file_name.replace(".", "_") + "_"

  # Makefile name
  ccfgName = os.path.splitext(os.path.split(config.ccfgFile())[1])[0]
  make_file_name   = 'Makefile.' + ccfgName + '.sa'
  
  
  # File header comments
  template_h   = headerComment(config, output, h_file_name)
  template_cpp = headerComment(config, output, cpp_file_name)

  # Includes and such
  template_h   += h_include(config, output)
  template_cpp += cpp_include(config, output)
  
  # Emit Sub Components
  # We want a standalone user to get a header file for just the top level component
  # for that reason, we emit the class definitions for the sub components in the cpp file
  for comp in subComps:
    template_h += comp.emitDefinition(None, comp.compConfig())
  for comp in subComps:
    template_cpp += comp.emitImplementation(None, comp.compConfig())
    
  # Emit Top Level Component
  template_h   += topComp.emitDefinition(None, config)
  template_cpp += topComp.emitImplementation(None, config)

  # Write Files
  Carbon.outputStream.writeOutputFile(output, '_msvc_version.cpp', 'MSVC version checker.', 'int main() { return _MSC_VER/100; }\n')
  Carbon.outputStream.writeOutputFile(output, h_file_name, 'Component header', template_h)
  Carbon.outputStream.writeOutputFile(output, cpp_file_name, 'Component source', template_cpp)
  Carbon.outputStream.writeOutputFile(output, make_file_name, 'Unix Makefile', emit_unix_makefile(config, output, make_file_name))
  Carbon.outputStream.writeOutputFile(output, make_file_name + '.windows', 'Windows Makefile', emit_windows_makefile(config, output, make_file_name + '.windows'))

  # Write CADI files
  for cadi in config.loopCadi():
    cadi.writeHeaderFile(output)
    cadi.writeCppFile(output)

  # Generate SystemC Wrappers for standalone model
  ccfgFile = ''
  portTypeMap = {}
  defineMap = {}
  
  if scUseComponentName:
    moduleName = config.compName() + '_SC'
  else:
    moduleName = ccfgName + '_SC'
  scModule = Carbon.SystemC.module.ScModule(config.ioDbFile(), ccfgFile, portTypeMap, defineMap, moduleName, True, config.compName(), scUseComponentName,False)
  scModule.generate()
