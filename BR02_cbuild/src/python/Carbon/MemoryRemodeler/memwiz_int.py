# This is the Intellon-specific front-end to the memory wizard.

# Memory names are in the form:
# DP1168x36 (depth x width)
# OP
# ROM
# SP
# TP

# OP: Single-port register file, read or write, one clock
# For purposes of remodeling, this is the same as SP
# SP: Single-port RAM, read or write, one clock

# ROM: Simgle-port memory, no write, one clock

# TP: Dual-port register file, one port is read, the other is write, two clocks

# DP: Dual-port RAM, each port can read or write, and each has a clock
# There is a contention if the clocks are "close" (100ps).

import sys
import re
import math
import CarbonMemSync

def run(argv):

    # Open the file
    modulelistfile = open(argv[1], 'r')

    for txt in modulelistfile:
        # Strip module statement from the verilog or
        #  the .v from ls
        re_name = '(?:module\s+)?([^\.]+)'
        rg = re.compile(re_name)
        name_match = rg.search(txt)
        module_name = name_match.group(1)

        # Break down the file name pattern


        re1='(OP|SP|ROMV|DP|TP)'	# Type
        re2='(\\d+)'                    # Depth
        re3='X'                         # X
        re4='(\\d+)'                    # Width
        re5a='_BW(\\d+)'                # Optional: "_BW24" where 24 is the mask
        re5b='_\\w+$'                   # Optional: "_STRING"
        re5='(?:'+re5a+'|'+re5b+')?'

        rg = re.compile(re1+re2+re3+re4+re5,re.DOTALL)
        m = rg.search(txt)
        if m:
            type=m.group(1)
            depth=m.group(2)
            width=m.group(3)
            writeEnableWidth=m.group(4)
            if not writeEnableWidth:
                writeEnableWidth = ""
            print "//("+type+")"+"("+depth+")"+"("+width+")"+"("+writeEnableWidth+")"

            addrBits = int (math.ceil (math.log(float(depth), 2)))

            # Set the defaults
            parameters = {}
            ports = []

            parameters["WriteBeforeRead"] = 0

            # All of the instances have 0 module parameters.  Ignore them.
            parameters["IgnoreParams"] = 0

            ports.append({})
            ports[0]["ReadAddrName"] = "A"
            ports[0]["WriteAddrName"] = ""
            ports[0]["DataOutName"] = "Q"
            ports[0]["DataInName"] = "D"
            ports[0]["DataEnableName"] = "WEN"
            ports[0]["DataEnableSize"] = 0
            ports[0]["CsReadName"] = "CEN"
            ports[0]["CsWriteName"] = "CEN"
            ports[0]["Clk"] = "CLK"
            # ("name", "width string", "not", "expected value")
            ports[0]["ExtraAsserts"] = [("RETN", "", "1'b1"), ("RDT", "", "1'b0"),  ("WBT", "", "1'b0")]
            # ("name", "width string")
            ports[0]["ExtraInputs"] = [("EMA", "[2:0]")]
            ports[0]["ExtraOutputs"] = []
            ports[0]["ExtraOutputsWidth"] = []

            
            parameters["AddrWidth"] = addrBits
            parameters["DataWidth"] = width
            parameters["Depth"] = depth

            # For behavioral models, OP and SP are the same
            if type == "OP" or type == "SP":
                if writeEnableWidth:
                    ports[0]["DataEnableName"] = "WEN"
                    ports[0]["DataEnableSize"] = writeEnableWidth
                    ports[0]["CsWriteName"] = ""
                else:
                    ports[0]["DataEnableName"] = ""
                    ports[0]["CsWriteName"] = "WEN"

            elif type == "ROMV":
                # No data in means ROM
                ports[0]["DataInName"] = ""
                # ROMS don't have RDT or RETN
                ports[0]["ExtraAsserts"] = [("WBT", "", "1'b0")]

            elif type == "DP":
                ports[0]["ReadAddrName"] = "AA"
                ports[0]["WriteAddrName"] = ""
                ports[0]["DataOutName"] = "QA"
                ports[0]["DataInName"] = "DA"
                ports[0]["DataEnableName"] = ""
                ports[0]["DataEnableSize"] = 0
                ports[0]["CsReadName"] = "CENA"
                ports[0]["CsWriteName"] = "WENA"
                ports[0]["Clk"] = "CLKA"

                ports.append({})
                ports[1]["ReadAddrName"] = "AB"
                ports[1]["WriteAddrName"] = ""
                ports[1]["DataOutName"] = "QB"
                ports[1]["DataInName"] = "DB"
                ports[1]["DataEnableName"] = ""
                ports[1]["DataEnableSize"] = 0
                ports[1]["CsReadName"] = "CENB"
                ports[1]["CsWriteName"] = "WENB"
                ports[1]["Clk"] = "CLKB"

            elif type == "TP":
                ports[0]["ReadAddrName"] = "AA"
                ports[0]["WriteAddrName"] = ""
                ports[0]["DataOutName"] = "QA"
                ports[0]["DataInName"] = ""
                ports[0]["DataEnableName"] = ""
                ports[0]["DataEnableSize"] = 0
                ports[0]["CsReadName"] = "CENA"
                ports[0]["CsWriteName"] = "WENA"
                ports[0]["Clk"] = "CLKA"

                ports.append({})
                ports[1]["ReadAddrName"] = "AB"
                ports[1]["WriteAddrName"] = ""
                ports[1]["DataOutName"] = ""
                ports[1]["DataInName"] = "DB"
                ports[1]["DataEnableName"] = ""
                ports[1]["DataEnableSize"] = 0
                ports[1]["CsReadName"] = ""
                ports[1]["CsWriteName"] = "CENB"
                ports[1]["Clk"] = "CLKB"
                if writeEnableWidth:
                    ports[1]["DataEnableName"] = "WENB"
                    ports[1]["DataEnableSize"] = writeEnableWidth

                ports[0]["ExtraInputs"] = [("EMAA", "[2:0]"), ("EMAB", "[2:0]")]

            wrapper = CarbonMemSync.CarbonMemSync(module_name, parameters, ports)
            code = wrapper.emitWrapper()

            print code

run(sys.argv)

