#******************************************************************************
# Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

##
# \file
# Classes for managing user code sections within generated files
import os
import re

class UserCode:
  def __init__(self):
    self.__sections = {}

  def numSections(self):
    return len(self.__sections)

  def sectionNames(self):
    names = self.__sections.keys()
    names.sort()
    return names

  def preSection(self, name, indent='  ', defaultCode = ''):
    return self.section('PRE ' + name, indent, defaultCode)

  def postSection(self, name, indent='  ', defaultCode = ''):
    return self.section('POST ' + name, indent, defaultCode)

  def implementSection(self, name, indent='  ', defaultCode = ''):
    # IMPLEMENT sections were a bad idea (bug 6558).
    # When the generator asks for one, return PRE and POST sections
    # instead.

    # the parser converts IMPLEMENT to PRE when reading the file
    sectionName = 'PRE ' + name

    sectionCode = ''
    if sectionName in self.__sections:
      sectionCode = self.__sections[sectionName]
      del self.__sections[sectionName]

    # If the code we just found is the same as the default code, then
    # it's left over from what we generated in the IMPLEMENT section.
    # So we just ignore it.
    if sectionCode.strip() == defaultCode.strip():
      sectionCode = ''

    code = self.preSection(name, indent, sectionCode)
    code += '\n' + defaultCode + '\n'
    code += self.postSection(name, indent, '')
    return code

  # return formatted user code section for given section name
  def section(self, sectionName, indent, defaultCode):
    # Guarantee that we start with a newline, since
    # UserCode.parseFile() requires that.
    code = '\n' + indent + '// CARBON USER CODE [%s] BEGIN\n' % sectionName

    if sectionName in self.__sections:
      code += self.__sections[sectionName]
      del self.__sections[sectionName]
    else:
      code += defaultCode
    code += indent + '// CARBON USER CODE END'
    return code

  # generate a code block for all unsused user sctions
  def unusedSections(self):
    code = ''
    if len(self.__sections) > 0:
      code += """
#if 0
  ///////////////////////////////////////////////////////////////////////////////
  // The following user code sections do not match any of the code generated for
  // this component.
  ///////////////////////////////////////////////////////////////////////////////
"""
      names = self.__sections.keys()
      names.sort()
      for name in names:
        code += '\n' + self.section(name, '', '') + '\n'
      code += """
#endif
"""
    return code

  # parse user code sections out of given file
  def parseFile(self, fileName, output):
    if not os.path.exists(fileName):
      return
    f = open(fileName, 'r')

    beginExpr = re.compile(r'\s*// CARBON USER CODE \[([^\]]+)\] BEGIN')
    endExpr = re.compile(r'\s*// CARBON USER CODE END')

    section = ''
    code = []

    lineNumber = 0
    for line in f.readlines():
      lineNumber = lineNumber + 1

      # look for the beginning of a user code section
      stripped = line.strip()
      match = beginExpr.match(stripped)
      if match != None:
        name = stripped[match.start(1) : match.end(1)]
        if section == '':
          section = name
          # convert old 'IMPLEMENT' sections to 'PRE' sections
          if section.startswith('IMPLEMENT '):
            section = section.replace('IMPLEMENT ', 'PRE ')

          # For SoC Designer components convert old MXDI user code section to CADI ones
          section = re.sub(r'^(PRE|POST) (\w+)_MxDI CLASS DEFINITION$', r'\1 \2_CADI CLASS DEFINITION', section)
          section = re.sub(r'^(PRE|POST) (\w+)::MxdiMemGetBlocks$',     r'\1 \2_CADI::CADIMemGetBlocks', section)
          section = re.sub(r'^(PRE|POST) (\w+)_MXDI::(\w+)_MXDI$',      r'\1 \2_CADI::\3_CADI', section)
          section = re.sub(r'^(PRE|POST) (\w+)_MXDI::\~(\w+)_MXDI',     r'\1 \2_CADI::~\3_CADI', section)
          section = re.sub(r'^(PRE|POST) (\w+)::MxdiRegGetGroups',      r'\1 \2_CADI::CADIRegGetGroups', section)
          section = re.sub(r'^(PRE|POST) (\w+)::MxdiRegGetMap',         r'\1 \2_CADI::CADIRegGetMap', section)
          section = re.sub(r'^(PRE|POST) (\w+)::MxdiRegWrite',          r'\1 \2_CADI::CADIRegWrite', section)
          section = re.sub(r'^(PRE|POST) (\w+)::MxdiRegRead',           r'\1 \2_CADI::CADIRegRead', section)
          section = re.sub(r'^(PRE|POST) (\w+)::MxdiMemGetSpaces',      r'\1 \2_CADI::CADIMemGetSpaces', section)
          section = re.sub(r'^(PRE|POST) (\w+)::MxdiMemGetBlocks',      r'\1 \2_CADI::CADIMemGetBlocks', section)
          section = re.sub(r'^(PRE|POST) (\w+)::MxdiMemWrite',          r'\1 \2_CADI::CADIMemWrite', section)
          section = re.sub(r'^(PRE|POST) (\w+)::MxdiMemRead',           r'\1 \2_CADI::CADIMemRead', section)
          section = re.sub(r'^(PRE|POST) (\w+)::MxdiGetPC',             r'\1 \2_CADI::CADIGetPC', section)
          section = re.sub(r'^(PRE|POST) (\w+)::MxdiGetDisassembler',   r'\1 \2_CADI::CADIGetDisassembler', section)

        else:
          output.error('%s:%d New user code section [%s] started inside section [%s].' %
                       (fileName, lineNumber, name, section))
      else:
        # look for the end of a user code section
        match = endExpr.match(stripped)
        if match != None:
          if section == '':
            output.error('%s:%d CARBON USER CODE END has no matching CARBON USER CODE [...] BEGIN.' %
                         (fileName, lineNumber))
          else:
            text = ''.join(code)
            if text.strip() != '':
              self.__sections[section] = text
            section = ''
            code = []
        else:
          # just a line of code.  if in a user section, save it
          if section != '':
            code.append(line)

    if section != '':
      output.error('%s:%d End of file reached before CARBON USER CODE END for section [%s]',
                   (fileName, lineNumber, section))
    f.close()
