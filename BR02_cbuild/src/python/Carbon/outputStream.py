#******************************************************************************
# Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

import os
import shutil

##
# \file
# Classes for application console output, providing a way to report errors,
# compute exist status.

class OutputStream:
  def __init__(self):
    self.__verbose = True
    self.__numWarnings = 0
    self.__numErrors = 0

  def setVerbose(self, verbose):
    self.__verbose = verbose
    
  # write message to stdout if in verbose mode
  def write(self, text, always = False):
    if always or self.__verbose:
      print text
    
  def note(self, message):
    if self.__verbose:
      print 'Note:', message

  def warning(self, message):
    self.__numWarnings = self.__numWarnings + 1
    print 'Warning:', message

  def error(self, message):
    self.__numErrors = self.__numErrors + 1
    print 'Error:', message

  def numWarnings(self):
    return self.__numWarnings

  def numErrors(self):
    return self.__numErrors

  def exitStatus(self):
    # if no errors, return 0 (success),
    # else return number of errors
    status = self.__numErrors
    return status

# Utility function for writing content to the specified file
def writeOutputFile(output, fileName, description, content):
  output.write('  %s [%s]' % (description, fileName))
  try:
    if os.path.exists(fileName):
      output.write('    Saving existing file to %s.bak' % fileName)
      shutil.copy(fileName, fileName + '.bak')
  except Exception,e:
      output.error(e)
      output.write('The new %s will NOT be written to %s.' % (description, fileName))
  else:
    try:
      output.write('    Creating new file %s' % fileName)
      f = open(fileName, 'w')
      f.write(content)
      f.close()
    except Exception,e:
        output.error(e)
