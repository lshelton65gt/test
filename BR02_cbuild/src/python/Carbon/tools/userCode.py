#******************************************************************************
# Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

def name():
  return __name__.split('.')[-1]

def brief():
  return "Print the user code sections found in the given files."

def run(argv):
  import Carbon.userCode
  import Carbon.outputStream

  if len(argv) < 2:
    print "usage: %s file [file ...]" % argv[0]
    return 1

  outputStream = Carbon.outputStream.OutputStream()

  for fname in argv[1:]:
    userCode = Carbon.userCode.UserCode()
    userCode.parseFile(fname, outputStream)
    numSections = userCode.numSections()
    if numSections > 0:
      print '#' * 79
      print fname
      print '#' * 79

    for name in userCode.sectionNames():
      code = userCode.section(name, '', '')
      print
      print code

  return 0

if __name__ == "__main__":
  import sys
  run(sys.argv)
