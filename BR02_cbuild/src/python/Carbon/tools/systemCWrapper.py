def name():
  return __name__.split('.')[-1]

def brief():
  return "Generate a SystemC wrapper module for a Carbon VHM."

###############################################################################
# usage: systemCWrapper libdesign.io.db
###############################################################################
def run(argv):
  import os
  import os.path
  import sys
  import Carbon.SystemC.module
  import optparse

  # build a simple options parser
  usage = """\
usage: %(name)s [-moduleName name] [-portType portName=systemCType ...] libdesign.io.db
       %(name)s -ccfg config.ccfg
       %(name)s -gencowarebuild
""" % { 'name' : name() }

  dbFile = ''
  ccfgFile = ''
  moduleName = ''
  portTypeMap = {}
  defineMap = {}
  printUsage = True
  genCowareBuild = False
  useStandAlone = False
  saCompName = ''
  saUpdateClock = ''
  try:
    i = 1
    while i < len(argv):
      arg = argv[i]
      if arg.startswith('-'):
        if arg == '-moduleName':
          i = i + 1
          if i < len(argv):
            moduleName = argv[i]
          else:
            raise Exception('A module name is required for -moduleName')
        elif arg == '-ccfg':
          i = i + 1
          if i < len(argv):
            ccfgFile = argv[i]
          else:
            raise Exception('A file name is required for -ccfg')
        elif arg == '-portType':
          i = i + 1
          if i < len(argv):
            try:
              portName, portType = argv[i].split('=')
            except:
              raise Exception('Cannot parse -portType argument %s' % arg)
            portTypeMap[portName.strip()] = portType.strip()
        elif arg == '-scDefine':
          i = i + 1
          if i < len(argv):
            try:
              defineName, defineValue = argv[i].split('=')
            except:
              raise Exception('Cannot parse -scDefine argument %s' % arg)
            defineMap[defineName.strip()] = defineValue.strip()
        elif arg == '-gencowarebuild':
          genCowareBuild = True
        elif arg == '-saCompName':
          i = i + 1
          if i < len(argv):
            saCompName = argv[i]
            useStandAlone = True
          else:
            raise Exception('A name is required for -saCompName')
        elif arg == '-saUpdate':
          i = i + 1
          if i < len(argv):
            saUpdateClock = argv[i]
          else:
            raise Exception('A clock name is required for -saUpdate')
        else:
          raise Exception('Unknown argument: %s' % arg)
      else:
        if dbFile == '':
          dbFile = arg
        else:
          raise Exception('Only one .db file is allowed.')
      i = i + 1

    # check for semantic errors
    if (dbFile == '' and ccfgFile == '') or (dbFile != '' and ccfgFile != ''):
      raise Exception('Must provide either a .db file or a .ccfg file')

    if ccfgFile != '' and len(portTypeMap) > 0:
      raise Exception('-portType cannot be used with -ccfg.  The .ccfg file defines the port types.')

    if ccfgFile != '' and moduleName != '':
      raise Exception('-moduleName cannot be used with -ccfg. The .ccfg file defines the SystemC module name.')

    if ccfgFile != '':
      print "ccfgfile:", ccfgFile

    # generate a SystemC wrapper
    printUsage = False

    scModule = Carbon.SystemC.module.ScModule(dbFile, ccfgFile, portTypeMap, defineMap, moduleName,
                                              useStandAlone, saCompName, False, genCowareBuild)
    scModule.generate()

  except Exception,e:
    if os.getenv('CARBON_BIN') != None:
      import traceback
      traceback.print_exc(sys.exc_info())
    if printUsage: print usage
    print e
    return 1

  # all is well
  return 0

###############################################################################
if __name__ == "__main__":
  import sys
  run(sys.argv)
