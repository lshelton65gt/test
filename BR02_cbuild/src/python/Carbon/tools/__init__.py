class _BinTool:
  """
  Default class for scripts that exist in CARBON_HOME/bin but have no
  Python wrapper.
  """

  _descriptions = {
    'analyzeClocks': '''Examine a set of waveform files looking for
                        clocks that have similar behavior.''',

    'build-systemc': """Builds the OSCI SystemC distribution shipped with
                        Carbon, using `configure' and `make install'.""",

    'carbon_sdcxx': 'Invoke the C++ compiler supported by SOC Designer.',

    'carbondb': '''Reads a full database (libdesign.symtab.db) or an
                   I/O database (libdesign.io.db), and prints it out textually.''',

    'cbuild':      '''Maps Verilog and/or VHDL RTL source into a
                      high-speed linkable object library (the "Carbon Model").
                      This object library can be linked into a C testbench
                      environment, or linked with a behavioral
                      simulator, to aid in software integration and
                      debug.''',

    'ccontrol': """A graphical tool for viewing simulation performance and
                   controlling the Carbon Model Replay system.  We highly
                   recommend using modelstudio instead.""",

    'cwavetestbenchgen': '''Creates an executable that drives a single
                            Carbon Model with a waveform file and
                            compares the output with the expected
                            results.''',

    'install_patch': '''Installs a tar.gz file over $CARBON_HOME, first
                        saving the existing versions off somewhere else.''',

    'searchannotations': '''Searches for a location from the generated
                         C++ model in the annotation database created during compilation with
                         -annotateCode.''',

    'modelstudio': '''Graphical tool to build, package, and control runtime behavior of Carbon Models.''',

    'socdexport': 'Create a SystemC exported interface for a SoC Designer component.',

    'vspcompiler': 'Run the Carbon compiler.  (See cbuild)',

    'waveActivity': '''Examine a waveform file and build a histogram of
                       activity levels.''',

    'waveConstants': '''Examine a waveform file and report the unchanging
                        (constant) signals.''',

    'wavequery': '''General wave dump utility that prints information
                    about signals in the wave file. It currently can
                    dump the names of all the signals, and it can do a
                    straight diff of two wave files.'''
    }

  def __init__(self, name):
    self.__name = name

  def name(self):
    return self.__name

  def brief(self):
    return self._descriptions.get(self.__name, "")

  def run(self, argv):
    import os
    import os.path

    # run the script
    script = os.path.join(os.environ['CARBON_HOME'], 'bin', self.__name)
    print script
    os.execv(script, argv)

    # execv never returns... but if it does, something must be wrong!
    return 1


def names(includeHidden = False):
  import sys
  import os
  import os.path

  # these names are not working tools
  filterNames = (
    '__init__',    # __init__.py will be found, but it not a tool
    'carbon'       # 'carbon' is just a script to launch other tools
    )

  # these are tools that work, but we don't want to advertise them
  hiddenNames = (
    'ar',
    'as',
    'ld',
    'ranlib',
    'nm',
    'sysexec',
    'carbon_arch',
    'carbon_rh_release',
    'carbon_exec',       # I don't know why this ends up in the bin dir
    'cbuildinternal',    # internal compiler script
    'ccfgUpgrade',
    'collectFiles',      # Given a simulator command line, gather the list of HDL source files used to build a design. Understands the command line arguments of several external simulators
    'ccrypt',
    'check_file',        # internal file checker
    'dot',
    'dot-run',
    'dotread',
    'gen_token_file',    # not for the average user, but shipped for use by AEs
    'libversion',        # used by scripts and makefiles to determine the libcarbon version
    'make',
    'ncelabC',
    'ncsimC',
    'perl',
    'readiodb',          # appears to be for testing
    'socdsyscimport',    # soc designer system c import
    'speedcc',
    'sys2socd',
    'SystemC',           
    'tbgen',
    'vspbuild',          # deprecated name for cbuild
    'vspcompiler',       # deprecated name for cbuild
    'xiar'               # we don't ship the executable that this script invokes
    )

  win = sys.platform == 'win32' or sys.platform == 'dos' or sys.platform == 'ms-dos'
    
  nameSet = {}
  for dir in (os.path.dirname(__file__),
              os.path.join(os.environ['CARBON_HOME'], 'bin')):
    for path in os.listdir(dir):
      tool,ext = os.path.splitext(os.path.basename(path))
      if tool not in filterNames:
        if includeHidden or tool not in hiddenNames:
          # skip over .bat files if not windows
          if ext == '.bat' and not win:
            continue
          # skip /bin/sh scripts on windows
          if ext == '' and win:
            continue
          nameSet[tool] = 1
  nameList = nameSet.keys()
  nameList.sort(lambda a,b: cmp(a.lower(),b.lower()))
  return nameList

def tools(includeHidden = False):
  for name in names(includeHidden):
    try:
      exec('import %s ; tool = %s' % (name, name))
      yield tool
    except:
      yield _BinTool(name)

def find(name, includeHidden = False):
  if name in names(includeHidden):
    try:
      # If the tool is a python script, load it.
      exec('import %s ; tool = %s' % (name, name))
      return tool
    except ImportError, e:
      try:
        # If the tool is an executable, wrap it with the _BinTool class
        tool = _BinTool(name)
        return tool
      except:
        return None
  else:
    return None
