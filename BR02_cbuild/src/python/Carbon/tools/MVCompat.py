#******************************************************************************
# Copyright (c) 2008-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

# This script parses a cbuild directives file, looking for legacy
# Model Validation directives.  Two output files are generated:
# 1.  A new cbuild directives file, in which the MV directives have
#     been replaced by the corresponding general-purpose ones
# 2.  A Model Validation directives file, which can be used with
#     MVGenerate's -d switch.
#
# The script takes one argument - the input directives file.  If the
# input file is named filename.ext, the output cbuild directives file
# will be called filename.MVCompat.dir and the Model Validation
# directives file will be called filename.MVCompat.xml.

import sys
import re
import string
import xml.dom.minidom
import Carbon.outputStream

class DirectivesParser:

  def __init__(self, outStr, inFileName, cbuildFileName, mvFileName):
    self.__outStr = outStr
    self.__inFileName = inFileName
    self.__cbuildFileName = cbuildFileName
    self.__mvFileName = mvFileName
    self.__inFile = None
    self.__cbuildFile = None
    self.__mvFile = None
    
  def parse(self):
    # Open the files
    try:
      self.__inFile = open(self.__inFileName, "r")
    except:
      self.__outStr.error("Couldn't open %s" % self.__inFileName)
      return
    try:
      self.__cbuildFile = open(self.__cbuildFileName, "w")
    except:
      self.__outStr.error("Couldn't open %s" % self.__cbuildFileName)
      return
    try:
      self.__mvFile = open(self.__mvFileName, "w")
    except:
      self.__outStr.error("Couldn't open %s" % self.__mvFileName)
      return

    # Create the XML document for the MV file, and add some necessary elements
    impl = xml.dom.minidom.getDOMImplementation()
    self.__xmlDoc = impl.createDocument(None, "ModelValidation", None)
    topElement = self.__xmlDoc.documentElement
    topElement.setAttribute("version", "1.0")
    self.__netsElement = self.__xmlDoc.createElement("Nets")
    self.__netsElement.setAttribute("startsWith", "None")
    topElement.appendChild(self.__netsElement)

    # Compile the regexps
    depRE = re.compile("^\s*mvDepositSignal\s+(?P<paths>.+)$")
    obsRE = re.compile("^\s*mvObserveSignal\s+(?P<paths>.+)$")

    # Start parsing the file
    for line in self.__inFile:
      depMatch = depRE.match(line)
      obsMatch = obsRE.match(line)
      if depMatch:
        cbuildLine = line.replace("mvDepositSignal", "depositSignal", 1)
        self.addMVDirective(depMatch.group("paths"))
      elif obsMatch:
        cbuildLine = line.replace("mvObserveSignal", "observeSignal", 1)
        self.addMVDirective(obsMatch.group("paths"))
      else:
        cbuildLine = line
      self.__cbuildFile.write(cbuildLine)

    # Write the MV directives file
    self.__xmlDoc.writexml(self.__mvFile, "", " ", "\n")

    # Clean up
    self.__inFile.close()
    self.__cbuildFile.close()
    self.__mvFile.close()

  def addMVDirective(self, startPaths):
    # First build a list of paths.  Each path is itself a list of
    # tokens representing hierarchy scopes.  Escaped identifiers need
    # special consideration because their whitespace doesn't terminate
    # a path.

    tempPaths = []
    currPath = []
    currTokenStart = None
    inEscapedIdent = False
    for i in range(0, len(startPaths)):
      saveToken = False
      savePath = False
      endOfEscapedIdent = False
      isWhitespace = (string.whitespace.find(startPaths[i]) != -1)
      if inEscapedIdent:
        # Continue adding to the current token until we see a space
        if (startPaths[i] == " "):
          # The space is important, so don't actually add the token
          # yet.  That will occur on the next pass.  Also, dont
          # actually clear the flag, because we need to check it below
          # if we're reached the end of a line.
          endOfEscapedIdent = True
      elif (currTokenStart != None):
        # In a non-escaped token.  Continue until dot or whitespace
        if (startPaths[i] == "."):
          saveToken = True
        elif isWhitespace:
          saveToken = True
          savePath = True
      else:
        # Try to start a new token
        if not isWhitespace:
          currTokenStart = i
          # This may be an escaped identifier
          if (startPaths[i] == "\\"):
            inEscapedIdent = True

      # When we reach the end of the string, add the current token/path, if there is one.
      # However, don't do this if the line ends in a single extra
      # whitespace character, because that shouldn't be part of the
      # token (except when we're in an escaped identifier).
      # Increment the index so we save the last character
      if (i == len(startPaths) - 1) and (currTokenStart != None) and (inEscapedIdent or not isWhitespace):
        saveToken = True
        savePath = True
        i = i + 1

      # Update escaped identifier state
      if endOfEscapedIdent:
        inEscapedIdent = False

      # Did we complete a token/path?
      if saveToken:
        currPath.append(startPaths[currTokenStart:i])
        currTokenStart = None
        inEscapedIdent = False
      if savePath:
        tempPaths.append(currPath)
        currPath = []
          
    # Now validate the temporary paths, building a new list of paths
    newPaths = []
    for path in tempPaths:
      # The new path joins the tokens into a string
      pathString = ".".join(path)
      newPaths.append(pathString)

    # Generate the XML for the paths
    for path in newPaths:
      elem = self.__xmlDoc.createElement("Net")
      elem.setAttribute("locator", path)
      self.__netsElement.appendChild(elem)

def run(argv):
  # Create an output stream for messaging
  outStr = Carbon.outputStream.OutputStream()

  if (len(argv) != 2):
    outStr.write("usage: %s <directive file>" % name())
    return 1

  # Determine filenames
  inFileName = argv[1]
  # Strip the extension
  dot = inFileName.rfind(".")
  if (dot == -1):
    baseFileName = inFileName
  else:
    baseFileName = inFileName[:inFileName.rfind(".")]
  cbuildFileName = baseFileName + ".MVCompat.dir"
  mvFileName = baseFileName + ".MVCompat.xml"

  parser = DirectivesParser(outStr, inFileName, cbuildFileName, mvFileName)
  parser.parse()

  # Output stream tracks errors
  outStr.write("%s: %d errors, %d warnings" % (name(), outStr.numErrors(), outStr.numWarnings()))
  return outStr.exitStatus()

def name():
  return __name__.split('.')[-1]

def brief():
  return "Converts a file with legacy Model Validation directives to compatible compiler and MVGenerate directives."

if __name__ == "__main__":
    run(sys.argv)
