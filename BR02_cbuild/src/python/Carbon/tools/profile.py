#******************************************************************************
# Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************
#!/tools/linux/python/bin/python

# Program for creating a profile report.
# Expects carbon_profile.dat and *.prof in current directory or
# specified as arguments.

import sys
import glob
import signal
import os

# This matches the seconds per sample in the shell/Profile.cxx
# file. If that changes, it needs to change here. At some
# point, it might be better to compute the seconds_per_sample
# by dividing the total execution time by the number of samples.
# This will allow us to fix possible disconnects and allow
# version of Linux with better sampling rates.
seconds_per_sample = 10e-3              # Sampling every 10ms

class Bucket:

    def __init__(self, ID=0, type='', name='', source_locator='',
                 parent=None, hit_count=0):
        self.ID = ID                    # Bucket index
        self.type = type                # '<schedule>', '<API>', or blocktype
        self.name = name
        self.source_locator = source_locator
        self.parent = parent            # schedule bucket or module parent
        self.children = []
        self.hit_count = hit_count
        self.hit_count_with_children = 0

    # Recursive function to add the counts of my children to me.
    def add_child_counts(self):
        self.hit_count_with_children = self.hit_count
        for bucket in self.children:
            self.hit_count_with_children += bucket.add_child_counts()
        return self.hit_count_with_children

    # Print the bucket, followed by its children, sorted and indented.
    # cum: cumulative total before this bucket.  Returns updated cum.
    def print_schedule_bucket(self, cum, sched_total, print_children):
        cum += self.hit_count_with_children
        print '%4.1f %5.1f %8.2f %7.2f %s' % (
            100. * self.hit_count_with_children / sched_total,
            100. * cum / sched_total,
            self.hit_count_with_children * seconds_per_sample,
            self.hit_count * seconds_per_sample,
            self.name)
        if print_children:
            self.print_descendent_buckets(sched_total)
        return cum

    # Recursively print the children of this bucket, without cum or
    # self+children time, increasing indentation with depth.
    def print_descendent_buckets(self, sched_total, depth=1):
        for bucket in sorted([b for b in self.children
                              if b.hit_count_with_children > 0],
                             key=lambda obj:obj.hit_count_with_children,
                             reverse=True):
            print '%4.1f                %7.2f %s%s' % (
                100. * bucket.hit_count / sched_total,
                bucket.hit_count * seconds_per_sample,
                2 * depth * ' ',
                bucket.name)
            bucket.print_descendent_buckets(sched_total, depth + 1)

class Model:

    def __init__(self, filename, modelname):
        self.file_name = filename
        self.model_name = modelname
        self.uid = ''
        self.buckets = {}    # Dictionary (type,bucket number)->Bucket
        self.unprofiledHits = 0
        self.referenced = False         # Not referenced by .dat file

        try:
            profFile = file(self.file_name, 'r')
        except IOError:
            print "Can't open", self.file_name + '.'
            sys.exit(1)
        self.uid = profFile.next().rstrip('\r\n')
        scheduleBuckets = False         # Haven't hit schedule buckets yet
        for line in profFile:
            line = line.rstrip('\r\n')
            if line == '<schedule buckets>':
                scheduleBuckets = True  # The rest are schedule buckets
            else:
                bucket = Bucket()
                if not scheduleBuckets:
                    # Block buckets.  Example: 3 NUAlwaysBlock top clocks1.v:8
                    bucket.ID, bucket.type, bucket.parent, bucket.source_locator = \
                               line.split(' ', 3)
                    bucket.ID = int(bucket.ID)
                    self.buckets[('block', bucket.ID)] = bucket                    
                else:
                    # Schedule bucket format: BucketID[:parentID] name
                    bucket.type = '<schedule>'
                    id_with_parent, bucket.name = line.split(' ', 1)

                    if id_with_parent.find(':') == -1: # If no parent
                        id_with_parent += ':-1'
                    id, parent = id_with_parent.split(':')
                    bucket.ID = int(id)
                    # Here set parent to parent's ID, then later set it
                    # to the object.
                    bucket.parent = int(parent)

                    if bucket.name == 'carbon_' + self.model_name + '_schedule':
                        bucket.name = '<overhead>'
                    self.buckets[('schedule', bucket.ID)] = bucket
        # Now that all the schedule buckets are in the dictionary, go
        # through them replacing parent IDs with parent bucket objects.
        # We couldn't do this while reading them in because some parents
        # appear later in the file than their children.
        for bucket in self.buckets.values():
            if bucket.type != '<schedule>':
                continue
            # Treat children of carbon_design_schedule (renamed
            # '<overhead>') as top-level.  Otherwise everything
            # would be child of it.
            if bucket.parent == -1 or \
                   self.buckets[('schedule', bucket.parent)].name == '<overhead>':
                bucket.parent = None
            else:
                bucket.parent = self.buckets[('schedule', bucket.parent)]
            
    def __cmp__(self, other):
        return cmp(self.model_name, other.model_name)

    def print_schedule_buckets(self):
        schedule_list = [bucket for bucket in self.buckets.values()
                         if bucket.type == '<schedule>']
        # Apply time to parent schedules.
        # First add child links.  Set .children attribute of each parent.
        for bucket in schedule_list:
            if bucket.parent != None:
                bucket.parent.children.append(bucket)
        sum = 0                         # total hits for all schedule buckets
        # Now recursively visit children and add hits to parent.
        for bucket in schedule_list:
            if bucket.parent == None:   # Start with only top level
                bucket.add_child_counts()
                sum += bucket.hit_count_with_children
        # Print each top-level schedule bucket, reverse sorted, first
        # without children (summary), then with (details).
        if sum > 0:
            for print_children, title in [(False, 'Schedule summary'),
                                          (True, 'Schedule details')]:
                print '%s, %.2fs:' % (title, sum * seconds_per_sample)
                print '  %  Cum %     Time    Self'
                cum = 0
                for bucket in sorted([b for b in schedule_list
                                      if b.parent == None
                                      and b.hit_count_with_children > 0],
                                      key=lambda obj:obj.hit_count_with_children,
                                      reverse=True):
                    # Print each parent bucket along with its children.
                    cum = bucket.print_schedule_bucket(cum, sum, print_children)

# This class keeps track of timing information for a given Module or Entity
# Note that floating point arithmetic is used to keep track of sample
# counts to avoid losing accuracy when dividing the module/architecture
# samples by the number of instances to get the instance sample count.
class Component:

    def __init__(self, name, file):
        self.name = name
        self.file = file        # The locator for the component
        self.buckets = {}       # The buckets in this component
        self.num_instances = 0  # The total number of instances of this component
        self.samples = 0.0      # The number of samples in this component
        self.computed = False   # Whether we have computed the stats or not

    def get_name(self):
        return self.name

    def get_file(self):
        return self.file

    def add_bucket(self, bucket_number, bucket):
        self.buckets[bucket_number] = bucket

    def incr_instances(self):
        self.num_instances = self.num_instances + 1

    def get_num_instances(self):
        return self.num_instances

    def get_buckets(self):
        return self.buckets.values()

    def get_samples(self):
        if not self.computed:
            # Not yet computed, walk the buckets and sum them up
            for bucket in self.get_buckets():
                self.samples = self.samples + float(bucket.hit_count)
            self.computed = True
        return self.samples

# This class keeps track of a component instance in a hierarchy.  It
# has a pointer to the component as well as all the sub instances.
class ComponentInstance:

    def __init__(self, name, component):
        self.name = name
        self.component = component
        self.sub_instances = {}
        self.instance_samples = 0.0
        self.sub_samples = 0.0

    def get_name(self):
        return self.name

    def get_component(self):
        return self.component

    # Add a compare function so we can sort them
    def __cmp__(self, other):
        result = cmp(self.get_total_samples(), other.get_total_samples())
        if result == 0:
            result = cmp(self.get_instance_samples(), other.get_instance_samples())
        return result

    # Return the number of samples for this component instance
    def get_instance_samples(self):
        return self.instance_samples

    # return the total number of samples for this component instance and below
    def get_total_samples(self):
        return self.instance_samples + self.sub_samples

    # Get the sub instances
    def get_sub_instances(self):
        return self.sub_instances.values()

    # Function to add a new sub instance to this instance
    def add_instance(self, instance):
        # Update the total component instances
        sub_component = instance.get_component()
        sub_component.incr_instances();

        # Add it as an instance
        instance_name = instance.get_name()
        if self.sub_instances.has_key(instance_name):
            raise "Duplicate instance `%s' in instance `%s'" % ( self.get_name(),
                                                                 instance_name )
        self.sub_instances[instance_name] = instance

    # Function to populate the sample information
    def populate_samples(self):
        # First ask each sub instance to populate themselves
        for instance in self.sub_instances.values():
            instance.populate_samples()
            self.sub_samples += instance.get_total_samples()

        # Now populate our instance samples (using floating point so
        # that we maintain accuracy when dividing sample counts by the
        # number of component instances).
        component = self.get_component()
        total_instances = float(component.get_num_instances())
        samples = component.get_samples()
        self.instance_samples += (samples / total_instances)

    # Function to print the statistics
    def print_statistics(self, count, hier_level, sample_total):
        # Compute the stats for this instance only
        instPercent = (self.get_instance_samples() * 100.0) / sample_total
        instTime = self.get_instance_samples() * seconds_per_sample

        # Compute the stats for this instance and below
        totalPercent = (self.get_total_samples() * 100.0) / sample_total
        totalTime = self.get_total_samples() * seconds_per_sample

        # Compute the stats for all instances of this component
        component = self.get_component()
        compPercent = (component.get_samples() * 100.0) / sample_total
        compTime = component.get_samples() * seconds_per_sample

        # Print the stats for this module
        print "%5d %5d  %5.2f  %8.2f  %5.2f  %8.2f  %5.2f %8.2f %*c%s (%s)  %s" \
              % (count, hier_level,
                 instPercent, instTime,
                 totalPercent, totalTime,
                 compPercent, compTime,
                 hier_level, ' ',
                 self.get_name(), self.get_component().get_name(),
                 self.get_component().get_file())
        count += 1

        # Print the stats for the sub instances
        for instance in sorted(self.sub_instances.values(),
                               reverse=True):
            count = instance.print_statistics(count, hier_level+1, sample_total)
        return count

# This class keeps track of the entire design heirarchy. This includes
# The set of components with their buckets as well as an
# instance hierarchy. It gets populated from a libdesign.hierarchy
# file
class Hierarchy:
    def __init__(self, file_name, model):
        self.components = {}    # All the components by name
        
        # Add an artificial top component and instance so we can support
        # multiple top modules if we have to
        self.top = Component("", "")
        self.hierarchy = ComponentInstance("", self.top)

        # Create a stack that starts at our artificial top
        self.scope_stack = [ self.hierarchy ]

        # Populate the hierarchy from the file
        self.__populate_hierarchy(file_name)

        # Add the buckets to the hierarchy
        self.__populate_buckets(model)

    # Function that adds a component if it doesn't already exist and returns it
    def add_component(self, name, file):
        if not self.components.has_key(name):
            self.components[name] = Component(name, file)
        return self.components[name]

    # Function to return a component given its name
    def get_component(self, name):
        if self.components.has_key(name):
            return self.components[name]
        else:
            return None

    # Function that adds a new instance at the current scope
    def add_instance(self, instance_name, component_name, component_file):
        # Create a component first, it might already exist
        component = self.add_component(component_name, component_file)

        # Now create an instance at the current hierarchy
        cur_scope = self.scope_stack[0]
        instance = ComponentInstance(instance_name, component)
        cur_scope.add_instance(instance)
        return instance

    def push_scope(self, instance):
        self.scope_stack.insert(0, instance)

    def pop_scope(self):
        self.scope_stack.pop(0)

    # Print the statistics for this hierarchy
    def print_statistics(self, sample_total):
        # Walk the hierarchy and populate sample counts at all levels
        for instance in self.hierarchy.get_sub_instances():
            instance.populate_samples()

        # Walk the hierarchy and print sample counts at all levels
        print ""
        print "Top-Down Hierarchy and Component View"
        print "Order Level  Self%  SelfTime  Inst%  InstTime  Comp% CompTime  Instance (Component) Location"
        print "----- -----  -----  --------  -----  --------  ----- --------  ----------------------------------------------------"
        count = 0
        for instance in self.hierarchy.get_sub_instances():
            count = instance.print_statistics(count, 1, sample_total)
        print ""
        print "Key: Order - Use to revert to original order if the lines are sorted"
        print "     Self  - The time in this instance of the component not including"
        print "             sub-instances"
        print "     Inst  - The time in this instance including sub-instances"
        print "     Comp  - The time for all instances of this component not"
        print "             including sub-components"

    # Read in a hierarchy file
    def __populate_hierarchy(self, filename):
        # Open the file if possible
        try:
            hier_file = file(filename, 'r')
        except IOError:
            print "Can't open file `%s'." % filename
            sys.exit(1)

        # First two lines are the title
        hier_file.next()
        hier_file.next()

        # The next line is the top module which has no instance name
        line = hier_file.next().rstrip('\r\n')
        component_name, component_file = line.split(None, 1)
        instance = self.add_instance(component_name, component_name,
                                     component_file)
        num_spaces = 0

        # Read the hierarchies. The scope moves up and down based on the
        # spaces at the beginning of the line
        for line in hier_file:
            # Get the line and count the number of spaces
            line = line.rstrip('\r\n')
            cur_spaces = 0
            while line[cur_spaces] == ' ':
                cur_spaces += 1

            # Change the scope if the current number of spaces has changed.
            # The .hierarchy file uses indentation to represent levels of
            # hierarchy.
            if cur_spaces == (num_spaces + 2):
                # Increased the scope
                self.push_scope(instance)
                num_spaces = cur_spaces
                
            elif cur_spaces < num_spaces:
                # Decreased the scope, figure out how much
                while num_spaces > cur_spaces:
                    self.pop_scope()
                    num_spaces -= 2

            # Make sure something didn't go wrong, we must have adjusted or
            # been at the same hierarchy. If the number of spaces changes
            # in a hierarchy file, this will catch it. Who thought of using
            # indentation to indicate hierachy. ;-)
            if num_spaces != cur_spaces:
                print "The number of indent spaces at line `%s' is not a multiple of two in `%s'" % (line, filename)
                sys.exit(1)

            # Break up the line into component and instance names and
            # create the instance if it isn't protected
            if line.find('<protected>') < 0:
                component_name, instance_name, component_file = line.split(None, 2)
                instance = self.add_instance(instance_name, component_name,
                                             component_file)

    def __populate_buckets(self, model):
        # Add the buckets to all the components
        for ((type, id), bucket) in model.buckets.iteritems():
            if type == 'block':
                component_name = bucket.parent
                component = self.get_component(component_name)
                if component:
                    component.add_bucket(id, bucket)

class SampleData:

    def __init__(self, prof_file_names, dat_file_name, hier_file_names):
        self.models = {}                # Map model name->Model
        self.hiers = {}                 # Map model name->hierarchy
        self.sample_total = 0           # Number of samples recorded in file
        self.time_stamp = ''            # carbon_profile.dat creation time

        self.__read_prof_files(prof_file_names)
        self.__read_hier_files(hier_file_names)
        try:
            profile = self.__read_dat_file(dat_file_name)
        except IOError:
            print 'Error reading data file'
            sys.exit(1)

    def model_values(self):
        return self.models.values()

    def hier_values(self):
        return self.hiers.values()

    def __read_prof_files(self, filenames):
        # If no .prof files specified, use *.prof
        if not filenames:
            filenames = glob.glob('*.prof')
        for file in filenames:
            namePart = os.path.basename(file)
            if not namePart.startswith('lib'):
                print 'Found *.prof file not starting with "lib"'
                sys.exit(1)
            # Convert, e.g., libdesign.prof to design
            model_name = namePart[len('lib'):].split('.prof')[0]
            # Create the Model object from this .prof file.
            self.models[model_name] = Model(file, model_name)

    def __read_hier_files(self, filenames):
        # Process any files provided by the user
        for file in filenames:
            namePart = os.path.basename(file)
            if not namePart.startswith('lib'):
                print 'Found *.hierarchy file not starting with "lib"'
                sys.exit(1)
            # Convert, e.g., libdesign.hierarchy to design
            model_name = namePart[len('lib'):].split('.hierarchy')[0]
            
            # Find the corresponding model for this hierarchy file.
            # If *.hierarchy found extra files that don't match .prof
            # files, ignore them.
            if self.models.has_key(model_name):
                model = self.models[model_name]
                self.hiers[model_name] = Hierarchy(file, model)

        # If there are any missing hierarchy files as compared to
        # .prof files, look for the hierarchy files in the same
        # directory as we found the .prof files.
        for model in self.models.values():
            model_name = model.model_name
            if not self.hiers.has_key(model_name):
                # Missing hierarchy file, try the .prof directory
                prof_file = model.file_name
                dir_part = os.path.dirname(prof_file)
                if dir_part != '':
                    dir_part += os.sep
                file_part = os.path.basename(prof_file)
                hier_file = dir_part + file_part.split('.prof')[0] + '.hierarchy'
                if os.path.exists(hier_file):
                    self.hiers[model_name] = Hierarchy(hier_file, model)

    # Read the .dat file
    def __read_dat_file(self, filenames):
        if len(filenames) > 1:
            print 'Only one ".dat" file allowed.'
            sys.exit(1)
        elif filenames:
            dataFileName = filenames[0]
        else:                           # Default
            dataFileName = 'carbon_profile.dat'
        try:
            dataFile = file(dataFileName, 'r')
        except IOError:
            print "Can't open", dataFileName + '.'
            sys.exit(1)

        try:
            temp = dataFile.next()
        except StopIteration:
            print dataFileName, 'is empty.'
            sys.exit(1)

        if not temp.startswith('Carbon profiling data'):
            print 'Corrupt profiling input: ' + dataFileName + '.'
            sys.exit(1)

        self.time_stamp = dataFile.next().rstrip('\r\n')

        # This will fail if file isn't correct.
        self.sample_total = int(dataFile.next())
        if self.sample_total == 0:
            # Avoid division by zero
            print 'No samples recorded in ' + dataFileName + '.'
            sys.exit(1)

        for modelName in dataFile:
            modelName = modelName.rstrip('\r\n')
            uid = dataFile.next().rstrip('\r\n')
            # Next line contains number of block buckets, and optionally,
            # number of schedule buckets and number of API buckets.
            counts = dataFile.next()
            # If this is for a model compiled without -profile, it says unprofiled
            # and contains only one number, the number of hits for the model.
            if counts.split()[0] == 'unprofiled':
                self.models[modelName].unprofiledHits = int(counts.split()[1])
                self.models[modelName].referenced = True
            else:
                if not modelName in self.models.keys():
                    print 'Compile-time profiling information for "' + \
                          modelName + '" is missing.'
                    print 'Is lib' + modelName + '.prof missing?'
                    sys.exit(1)
                elif uid != self.models[modelName].uid:
                    # Is it OK to print this to stdout rather than stderr?
                    print 'Warning: ID mismatch:', \
                          self.models[modelName].file_name, 'was compiled with'
                    print self.models[modelName].uid
                    print 'but was run with'
                    print uid                
                self.models[modelName].referenced = True

                # If API bucket count missing, use 0 (as it will be for old .dat files)
                if len(counts.split()) < 2:
                    counts += ' 0 0'        # Now there are three
                numBuckets, numSchedBuckets, numApiBuckets = \
                            [int(num) for num in counts.split()]
                for i in range(numBuckets):
                    self.models[modelName].buckets[('block', i)].hit_count = int(dataFile.next())
                for i in range(numSchedBuckets):
                    # This fails if the bucket doesn't exist in the .prof
                    # file.  Should fix this.
                    self.models[modelName].buckets[('schedule', i)].hit_count = int(dataFile.next())
                for i in range(numApiBuckets):
                    # Need to create API Bucket objects here because they
                    # don't appear in .prof files.
                    # Last word is count, rest is API name
                    line = dataFile.next().rsplit(' ', 1)
                    bucket = Bucket(ID=i, type='<API>', name=line[0], hit_count=int(line[1]))
                    # Insert into Model.buckets map
                    self.models[modelName].buckets[('API', i)] = bucket
        dataFile.close()

def run(argv):
    # Avoid "IOError: [Errno 32] Broken pipe" error when piping output to head.
    if hasattr(signal, 'SIGPIPE'):
        signal.signal(signal.SIGPIPE, signal.SIG_DFL)

    runCountSum = 0                     # Sum of counts in each model

    # Parse command-line arguments.
    withSchedule = False
    profArgs = []
    datArgs = []
    hierArgs = []
    for arg in argv[1:]:
        if arg == '-schedule':
            withSchedule = True
        elif arg.endswith('.prof'):
            profArgs.append(arg)
        elif arg.endswith('.hierarchy'):
            hierArgs.append(arg)
        elif arg.endswith('.dat'):
            datArgs.append(arg)
        elif arg == '-help':
            printUsage(argv[0])
            sys.exit(0)
        else:
            print argv[0] + ': Unrecognized argument:', arg
            printUsage(argv[0])
            sys.exit(1)

    # Read carbon_profile.dat runtime sampling file.
    data = SampleData(profArgs, datArgs, hierArgs)

    # Process models in alphabetical order.
    # Don't report models that aren't part of this run.
    referencedModels = [model for model in sorted(data.model_values())
                        if model.referenced]
    firstModel = True
    for model in referencedModels:
        if not firstModel: print     # Separate models with blank line
        # Create list of non-schedule non-zero buckets.
        bucket_list = [bucket for bucket in model.buckets.values()
                       if bucket.type != '<schedule>' and
                       bucket.hit_count > 0]
        # Report samples that aren't associated with any model.
        # Determine the sum of counts for this model.
        modelCountSum = sum([bucket.hit_count for bucket in bucket_list])
        # If there's only one model, add bucket to represent time
        # outside of Carbon.  If there are multiple models, displaying
        # time outside of models with one of the models doesn't make
        # sense, so keep a running total to report after the last model.
        if len(referencedModels) == 1: # Only one model
            bucket = Bucket(hit_count=data.sample_total - modelCountSum,
                            type='<Outside of Carbon>')
            bucket_list.append(bucket)
        else:
            runCountSum += modelCountSum
        # Reverse sort buckets by hit count. 
        bucket_list.sort(key=lambda x:x.hit_count, reverse=True)

        if firstModel: print 'Profiling data collected', data.time_stamp
        if model.unprofiledHits > 0:
            print 'Model:', model.model_name, '(compiled without -profile option)'
            print '  %            Time'
            print '%4.1f %14.2f' \
                  % (100. * model.unprofiledHits / data.sample_total,
                     model.unprofiledHits * seconds_per_sample)
            runCountSum += model.unprofiledHits
        elif bucket_list:
            print 'Model:', model.model_name
            print '  %  Cum %     Time ' + 'Type'.ljust(23) + ' Parent'.ljust(22) + 'Location'
            cumulativeCount = 0
            for bucket in bucket_list:
                cumulativeCount += bucket.hit_count
                if bucket.parent:
                    parent = bucket.parent
                else:
                    parent = ""
                if bucket.type == '<API>':
                    # Use API name as type and empty location.
                    type = bucket.name
                    location = ''
                elif bucket.type == '<schedule>':
                    # Schedule block
                    type = bucket.name
                    location = ''
                else:
                    # Regular block bucket
                    type = bucket.type
                    if type.startswith('NU'):
                        type = type[2:]
                    location = bucket.source_locator
                print '%4.1f %5.1f %8.2f %-23s %-20s %s' \
                      % (100. * bucket.hit_count / data.sample_total,
                         100. * cumulativeCount / data.sample_total,
                         bucket.hit_count * seconds_per_sample,
                         type,
                         parent,
                         location)
        else:
            print 'Model:', model.model_name
            print 'No samples in buckets'
        if withSchedule:
            model.print_schedule_buckets()
        firstModel = False

    if runCountSum > 0:
        outside = data.sample_total - runCountSum
        print '\n%4.1f       %8.2f <Outside of Carbon>' \
              % (100. * outside / data.sample_total,
                 outside * seconds_per_sample)

    # Process hierarchies in alphabetical order.
    firstHier = True
    for hier in sorted(data.hier_values()):
        if not firstHier: print     # Separate hiers with blank line
        hier.print_statistics(data.sample_total)

def printUsage(name):
    print "Usage: %s [-schedule] [file.dat] [file.prof]... [file.hierarchy]..." % name
    print "       %s -help" % name
    print
    print "       -schedule Include schedule profiling."
    print "       -help     Print this message."
    print """

 Files default to carbon_profile.dat and all *.prof files and matching
 *.hierarchy files in the current directory. Providing hierarchy files
 allows the tool to print additional profile information organized by
 hierarchy (top down instance [elaborated] and a component
 [unelaborated] views). If the hierarchy file is not provided, the
 tool looks for it in the same directory as any provided .prof files.
 
 lib<design>.prof is created with cbuild -profile.
 lib<design>.hierarchy is created with cbuild.
 carbon_profile.dat is created when running a model compiled with -profile.
"""

def name():
    return __name__.split('.')[-1]

def brief():
    return 'Displays an execution profiling report.'

if __name__ == '__main__':
    run(sys.argv)
