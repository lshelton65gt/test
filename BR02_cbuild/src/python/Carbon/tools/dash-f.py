#******************************************************************************
# Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

# Print to stdout the entries of a -f file.  If there are -f options
# inside of it, recursively dive into those files.

# Usage:
# dash-f [-f] filename [[-f] filename ...]

import sys
import re

is_f = re.compile("\s*-f\s+(\S+)")      # search for -f in file
tab = 4                                 # Arbitrary tab setting

def run(argv):
    # Filter out all the "-f" strings.  They aren't needed.
    #filenamelist = filter (lambda x: x != "-f", argv[1:])  # also works
    filenamelist = [x for x in argv[1:] if x != "-f"]
    if len(filenamelist) < 1:
        print "usage: %s [-f] file [[-f] file [...]]" % argv[0]
        return 0

    # Call dash_f for each filename
    map (dash_f, filenamelist)
    return 1

def openfile(filename):
    try:
        f = open (filename, 'r')
        return f
    except:
        return 0

def print_indent(indent, string):
    print "%*s%s" % (indent, " ", string) # Print with an indent

def dash_f(filename, indent = 0):
    f = openfile(filename)
    if not f:
        print_indent (indent, "// *** Warning: file \""+filename+"\" doesn't exist")
        return
    print_indent (indent, "// In file \""+filename+"\":")
    for line in f:
        line = line.rstrip()            # Strip the newline
        fmatch = is_f.search(line)      # See if this is a -f
        if fmatch:
            dash_f(fmatch.group(1), indent + tab)     # Yep, recursively call me
            # This seems to make it too verbose...
            # print_indent(indent, "// Returning to file \"%s\"" % filename)
        else:
            print_indent (indent, line) # Just print the line as is
    f.close()



def name():
  return __name__.split('.')[-1]

def brief():
  return "Recursively show the contents of simulator \"-f\" input files"

# Magic so that carbon command doesn't actually run this if all it
#   wants to do is call name()
if __name__ == "__main__":
  run(sys.argv)
