#******************************************************************************
# Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

def name():
    return __name__.split('.')[-1]

def brief():
    return "Print the Carbon version number that created the given database (lib<design>.io.db or lib<design>.symtab.db). Returns an empty string if the database was created with a software version that didn't add the versioning to the database."

def run(argv):
    import Carbon.DB
    db = Carbon.DB.DBContext(argv[1])
    if db:
        print db.softwareVersion()
        return 0
    else:
        return 1

if __name__ == "__main__":
    import sys
    run(sys.argv)
