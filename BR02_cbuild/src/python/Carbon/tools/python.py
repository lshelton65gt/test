#******************************************************************************
# Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

def name():
    return __name__.split('.')[-1]

def brief():
    return "Run a python interpreter with the proper environment to import Carbon modules."

def run(argv):
    import os
    import sys

    # run python in the current environment, passing args
    os.execv(sys.executable, argv)

    # execv never returns... but if it does, something must be wrong!
    return 1

if __name__ == "__main__":
    import sys
    run(sys.argv)
