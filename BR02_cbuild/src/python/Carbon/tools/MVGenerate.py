#******************************************************************************
# Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

def run(argv):
  # Import the MV tools
  import Carbon.MV.Generate

  # Run the tool
  return Carbon.MV.Generate.run(argv)

def name():
  return __name__.split('.')[-1]

def brief():
  return "Reads a full database (libdesign.symtab.db) and config file to emit a model that can be imported into a standard HDL simulator."

if __name__ == "__main__":
    run(sys.argv)
