#******************************************************************************
# Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

def run(argv):
  # Import the MemGen tools
  import Carbon.Remodeler.Remodeler

  # Run the tool
  return Carbon.Remodeler.Remodeler.run(argv)

def name():
  return __name__.split('.')[-1]

def brief():
  return "Remodel IP blocks according to their port names and user configuration"

# Magic so that carbon command doesn't actually run this if all it
#   wants to do is call name()
if __name__ == "__main__":
    run(sys.argv)
