#******************************************************************************
# Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

def name():
    return __name__.split('.')[-1]

def brief():
    return "Print the Carbon library version number."

def run(argv):
    print getVersion()
    return 0

def getVersion():
    # This is the version string appended to libcarbon to create a versioned filename
    return "5"

if __name__ == "__main__":
    import sys
    run(sys.argv)
