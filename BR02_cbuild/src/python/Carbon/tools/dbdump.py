#******************************************************************************
# Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

import sys

def nodePrintFullName(node):
    print "\t", node
    
def nodePrint(node):
    print "\t", node
    leafName = node.leafName()
    print "\t\tLeaf Name: ", leafName
    print "\t\tWidth: ", node.getWidth()
    print "\t\tParent: ", node.getParent().leafName()
    print "\t\tDeclaration: ", node.declarationType()
    
    if node.isPrimaryInput() == 1:
        print "\t\tPrimary Input"
    if node.isPrimaryOutput() == 1:
        print "\t\tPrimary Output"
    if node.isPrimaryBidi() == 1:
        print "\t\tPrimary Bidi"
    if node.isInput() == 1:
        print "\t\tModule Input"
    if node.isOutput() == 1:
        print "\t\tModule Output"
    if node.isBidi() == 1:
        print "\t\tModule Bidi"
    if node.isClk() == 1:
        print "\t\tClock"
    if node.isClkTree() == 1:
        print "\t\tClockTree"
    if node.isAsyncInput() == 1:
        print "\t\tAsync"
    if node.isAsyncOutput() == 1:
        print "\t\tAsyncOutput"
    if node.isAsyncDeposit() == 1:
        print "\t\tAsyncDeposit"
    if node.isAsyncPosReset() == 1:
        print "\t\tAsync Posedge Reset"
    if node.isAsyncNegReset() == 1:
        print "\t\tAsync Negedge Reset"
    if node.is2DArray() == 1:
        print "\t\t2D Array"
        print "\t\tLeft Addr: ", node.get2DArrayLeftAddr()
        print "\t\tRight Addr: ", node.get2DArrayRightAddr()
        print "\t\tRow MSB: ", node.getMsb()
        print "\t\tRow LSB: ", node.getLsb()
    if node.isVector() == 1:
        print "\t\tVector"
        print "\t\tMSB: ", node.getMsb()
        print "\t\tLSB: ", node.getLsb()
    if node.isScalar() == 1:
        print "\t\tScalar"
    if node.isTristate() == 1:
        print "\t\tTristate"
    if node.isConstant() == 1:
        print "\t\tConstant"
    if node.isPosedgeTrigger() == 1:
        print "\t\tPosedge Trigger"
    if node.isNegedgeTrigger() == 1:
        print "\t\tNegedge Trigger"
    if node.isTriggerUsedAsData() == 1:
        print "\t\tTrigger Used As Data"
    if node.isLiveInput() == 1:
        print "\t\tLive Input"
    if node.isLiveOutput() == 1:
        print "\t\tLive Output"
    if node.isOnDemandIdleDeposit() == 1:
        print "\t\tOnDemand idle deposit"
    if node.isOnDemandExcluded() == 1:
        print "\t\tOnDemand excluded"
    
def run(argv):
    # Carbon Scripting API
    import Carbon
    import Carbon.DB

    # print the full Carbon release version string
    print 'Carbon', name(), 'version', Carbon.getReleaseId()
    print

    if len(argv) == 1:
        print "Must specify filename"
        return 1
        
    db = Carbon.DB.DBContext(argv[1])
    if db:
        print "Db Version:\t\t", db.version()
        print "Compiler Version:\t", db.softwareVersion()
        print "Interface Name:\t\t", db.getInterfaceName()
        print "Top-level Module:\t", db.getTopLevelModuleName()
        print "SystemC Module Name:\t", db.getSystemCModuleName()
        if db.isReplayable() == 1:
            print "Replayable"
        else:
            print "Not Replayable"
        if db.supportsOnDemand() == 1:
            print "Supports OnDemand"
        else:
            print "Does Not Support OnDemand"
        
        portList = [ node.leafName() for node in db.iterPrimaryPorts()]
        
        print "Primary Ports: ", ", ".join(portList)
        print
        
        print "Inputs:"
        for node in db.iterInputs():
            nodePrint(node)
        print "Outputs:"
        for node in db.iterOutputs():
            nodePrint(node)
        print "Bidis:"
        for node in db.iterBidis():
            nodePrint(node)
        print "Primary Clks:"            
        for node in db.iterPrimaryClks():
            nodePrint(node)
        print "ClockTree:"            
        for node in db.iterClkTree():
            nodePrint(node)
        print "Depositable:"            
        for node in db.iterDepositable():
            nodePrint(node)
        print "Observable:"            
        for node in db.iterObservable():
            nodePrint(node)
        print "SystemC Depositable:"            
        for node in db.iterScDepositable():
            nodePrint(node)
        print "SystemC Observable:"            
        for node in db.iterScObservable():
            nodePrint(node)
        print "Asyncs:"            
        for node in db.iterAsyncInputs():
            nodePrint(node)
        print "AsyncsOutputs:"            
        for node in db.iterAsyncOutputs():
            nodePrint(node)
            print "  AsyncFanin:"
            for fanin in db.iterAsyncFanin(node):
                nodePrint(fanin)
        print "AsyncsDeposits:"            
        for node in db.iterAsyncDeposits():
            nodePrint(node)
        print "Async Posedge Resets:"            
        for node in db.iterAsyncPosResets():
            nodePrint(node)
        print "Async Negedge Resets:"            
        for node in db.iterAsyncNegResets():
            nodePrint(node)
        print "Posedge Triggers:"
        for node in db.iterPosedgeTriggers():
            nodePrintFullName(node)
        print "Negedge Triggers"
        for node in db.iterNegedgeTriggers():
            nodePrintFullName(node)
        print "BothEdge Triggers"
        for node in db.iterBothEdgeTriggers():
            nodePrintFullName(node)
        print "Posedge-only Triggers"
        for node in db.iterPosedgeOnlyTriggers():
            nodePrintFullName(node)
        print "Negedge-only Triggers"
        for node in db.iterNegedgeOnlyTriggers():
            nodePrintFullName(node)
        print "Triggers Used As Data"
        for node in db.iterTriggersUsedAsData():
            nodePrintFullName(node)

    else:
        sys.exit(1)
        
def name():
    return __name__.split('.')[-1]

def brief():
    return "Reads a full database (libdesign.symtab.db) or an I/O " \
           "database (libdesign.io.db), and prints it out textually."

if __name__ == "__main__":
    run(sys.argv)
