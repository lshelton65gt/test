#******************************************************************************
# Copyright (c) 2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#******************************************************************************

# This script merges archive files together.  Unlike the X11 mergelib,
# duplicate filenames in the archive are supported.

import sys
import tempfile
import os
import subprocess
import shutil
import Carbon.outputStream

def run(argv):
  # Create an output stream for messaging
  outStr = Carbon.outputStream.OutputStream()

  if (len(argv) != 3):
    outStr.write("usage: %s <existing archive> <archive to merge>" % name())
    return 1

  existingArchive = argv[1]
  newArchive = argv[2]

  # X11's mergelib doesn't allow duplicate filenames when merging
  # archives.  We need that for the model kit library, in which there
  # are many duplicate filenames (e.g. every processor has a
  # CarbonPC.o, whose symbols are in a processor-specific namespace).
  #
  # To accomplish this, we unpack the archive to merge into a
  # temporary directory.  As we do this, duplicate object files are
  # prefixed with an index.  Then, all the uniquified object files are
  # added to the existing archive.

  # Check that the files actually exist, and that the existing archive
  # is writable
  if not os.path.isfile(existingArchive):
    outStr.write(existingArchive + " does not exist")
    return 1
  if not os.access(existingArchive, os.W_OK):
    outStr.write(existingArchive + " cannot be written")
    return 1
  if not os.path.isfile(newArchive):
    outStr.write(newArchive + " does not exist")
    return 1

  # Make a temporary directory, and a subdirectory for the unique
  # files
  tmpDir = tempfile.mkdtemp()
  uniqueDir = tmpDir + "/unique"
  os.mkdir(uniqueDir)

  # I don't think 'ar' really cares about the target architecture.
  # Anyway, this is only needed for 32-bit Linux for now.
  carbonHome = os.environ['CARBON_HOME']
  if carbonHome == None:
    outStr.write("CARBON_HOME is not set")
    return 1
  ar = carbonHome + "/Linux/bin/ar"

  # Get the list of members in the current archive
  existingFiles = {}
  p = subprocess.Popen(ar + " t " + existingArchive, shell=True, stdout=subprocess.PIPE)
  (out, err) = p.communicate()
  for file in out.splitlines():
    existingFiles[file] = 1

  # Get the list of members in the new archive, counting instances
  newFilesCounts = {}
  p = subprocess.Popen(ar + " t " + newArchive, shell=True, stdout=subprocess.PIPE)
  (out, err) = p.communicate()
  for file in out.splitlines():
    if file in newFilesCounts:
      newFilesCounts[file] += 1
    else:
      newFilesCounts[file] = 1

  # We're ready to extract and uniquify the files in the archive to
  # merge.  We'll do this in the temp directory, so we need the path
  # to the merge archive
  newArchiveAbs = os.path.abspath(newArchive)
  for file, count in newFilesCounts.iteritems():
    # Extract each file with that name, and move it to a unique
    # filename.  Note that 'ar' indicies start with 1.
    for i in range(1, count + 1):
      uniqueName = "_%d_%s" % (i, file)
      arOpts = " xN %d " % i  # extract the 'i'th file with this name
      # Unpack the file as is and move it to the unique directory
      os.system("pushd " + tmpDir + " >& /dev/null && " + ar + arOpts + newArchiveAbs + " " + file + " && mv " + file + " unique/" + uniqueName)

  # Now run ar to add all the unique object files to the existing archive
  os.system(ar + " q " + existingArchive + " " + uniqueDir + "/*")

  # Clean up the temp directory
  shutil.rmtree(tmpDir)

def name():
  return __name__.split('.')[-1]

def brief():
  return "Merges archives together, allowing duplicate filenames."

if __name__ == "__main__":
    run(sys.argv)
