#******************************************************************************
# Copyright (c) 2008-2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#******************************************************************************

# This script does these four things:
# 1) Prepends the command "strace" to the commandline and runs it.  
# 2) Parses the output and gathers all the commandline arguments for the command(s) the
# user wants to capture.  By default, these are the six Carbon-supported simulators.
# 3) If there are -f files, open the files, and collect further arguments.
# 4) Remove unwanted arguments, including those with parameters, using the list in sim_options.xml.

# Useful libraries
import popen2
import re
import os.path
import os
import sys
import fcntl
import select
import optparse
import xml.sax

# Top-level class
class FileCollector:
    def __init__(self):
	self.__Version = "collectFiles 0.6"

    # Standard Python commandline argment parser
    def createOptParser(self, progName):
	usage = '''\
usage:
%(progName)s [options] command-or-script
''' % {"progName" : progName}
	parser = optparse.OptionParser(usage=usage, version=self.__Version)
	parser.add_option("-o", "--output",
			  help="Where to write the output file generated")
	parser.add_option("-p", "--optionsFile",
			  help="Use this options XML file instead of the default")
	parser.add_option("-F", "--noFlatten",
			  help="Don't follow the -f files to flatten the results",
			  action="store_true")
	parser.add_option("-k", "--keepArgs",
			  help="Don't filter away the simulator arguments",
			  action="store_true")
	parser.add_option("-s", "--searchTerm",
			  help="Override the default search for the common simulators")
	parser.add_option("-d", "--dumpAll",
			  help="Dump the arguments for all commands.  Implies -kFC.",
			  action="store_true")
	parser.set_defaults(searchTerm="bin//?(?:vcom|vlog|vhdlan|vlogan|vcs|vcsi|ncvlog|ncvhdl)",
			    output="collectFiles.txt")
	parser.disable_interspersed_args() # All the options have come before the passed-in command
	return parser

    # Main entrypoint.  Parse the arguments, issue the command, and do the post-processing of the arguments.
    def run(self, argv):
	# get the arguments
	commandLine = " ".join (argv[:])
	parser = self.createOptParser(argv[0])
	(options, args) = parser.parse_args(args=argv[1:])

	numArgs = len(args)
	if numArgs < 1:
	    print "Error: you must pass in a command to execute"
	    parser.print_help()
	    return 1

	exec_cmd = " ".join(args[0:])

	# Get the options
	searchTerm = options.searchTerm

	# This is for truss only.  Originally is was to disambiguate a shell command from a binary command.
	# In a shell command, the first argument is the call to the shell, and the second is the name of the script.
	# But the argument filter will remove these, so we'll remove this for now, but leave in support in
	# case the issue comes up again.
	constrain = False
	# constrain = not options.noConstrain
	flatten  = not options.noFlatten
	keepArgs  = options.keepArgs
	outputFile = options.output
	dumpAll = options.dumpAll
	optionsFile = options.optionsFile

	# Look for the default options file
	if not optionsFile:
	    if not os.environ.has_key("CARBON_HOME"):
		print "$CARBON_HOME must be set in order to find the sim_options.xml file"
		return 2
	    else:
		optionsFile = os.environ["CARBON_HOME"] + "/lib/sim_options.xml"


	# Make sure output file is ok to use
	self.write(outputFile, "output", "")


	# dump all implies no constraints.
	#   Also, don't flatten because there may be -f options for
	#    non-simulator commands, which mean something completely different, like ls -f(!)
	if dumpAll:
	    searchTerm = ".+?"		# non-greedy anything.
	    constrain = False
	    flatten = False
	    keepArgs = True

	# Open options file
	filter = paramFilter()
	err =  filter.read(optionsFile)
	if err:
	    return err
	filter_dict = filter.get_dict()

	# Call strace
	opsys = os.uname()
	if opsys[0].lower() == "linux":
	    c = strace(exec_cmd, searchTerm, constrain, dumpAll)
	else:
	    print "%s is not supported" % opsys[0]
	    return 3

	paramList = c.execute()

	# Skip lines with // or /* comments
	comment_re = re.compile(r'^\s*/[/*]')

	d = dashf("(?:-f|-file)")

	# post-process.  First look for all the dash-fs, and flatten them...
	flattenedList = ""
	if flatten:
	    for line in paramList.split("\n"):
		# Skip the comments, but keep the original line
		if not comment_re.search(line):
		    flattenedList += d.dashf(line)
		else:
		    flattenedList += line + "\n"
	else:
	    flattenedList = paramList

	# ...then filter out all the unnecessary arguments
	filteredList = ""

	if not keepArgs:
	    for line in flattenedList.split("\n"):
		filtered_line = line.strip()

		# Skip comments
		if not comment_re.search(line):
		    filtered_line = filter.filter(line)

		if filtered_line: filteredList += filtered_line + "\n"

	else:
	    # Skipped the flattening
	    filteredList = flattenedList + "\n"

	self.write(outputFile, "output", filteredList)
	print "Output written to '%s'" % outputFile
	return 0

    # General file write with error checking.
    def write(self, filename, description, text):
	# Make sure we have a valid file
	try:
	    f = open(filename, "w")
	except IOError, (errno, strerror):
	    print "Error: could not open %s file '%s' for writing, reason: '%s'" % \
		  (description, filename, strerror)
	    return errno
	except:
	    print "Error: could not open %s file '%s' for writing, unexpected error: '%s'" % \
		  (description, filename, sys.exc_info()[0])
	    return 4
	try:
	    f.write(text)
	except IOError, (errno, strerror):
	    print "Error: could not write to %s file '%s', reason: '%s'" % \
		  (description, filename, strerror)
	    return errno
	except:
	    print "Error: could not write to %s file '%s', unexpected error: '%s'" % \
	      (description, filename, sys.exc_info()[0])
	    return 5

	f.close
	return 0

# Class for executing and parsing the "strace" command in Linux.
class strace:
    def __init__(self, exec_cmd, search_term, constrain, dump_all):
        self.__mExecCmd = exec_cmd
        self.__mSearchTerm = search_term
        self.__mConstrain = constrain
        self.__mDumpAll = dump_all
        self.__mStraceCmd = "strace -f -q -F -s 256 -eexecve " + exec_cmd

    # Run the strace command, and parse the output.
    def execute(self):
        # execve("/bin/uname", ["uname", "-m"], [/* 42 vars */]) = 0
        # [pid  8034] execve("/bin/uname", ["uname", "-m"], [/* 42 vars */]) = 0
        #                        otional [pid]           "cmd",        ["args", ...]
        exec_re = re.compile \
                  (r'''^(?:\[[^\]]+\]\s)?  # Optional [pid 8034]
                  execve
                  \(\"([^\"]+)\"        # Quoted command, stripping the quotes
                  ,\s                   # comma-space
                  \[([^\]]+)\]          # List of args inside []
                  ''',re.X)
        # The grouping makes sure the term is at the end of the line,
        # so e.g. "vhdlan1" isn't matched when "vhdlan" in the term
        cmd_re = re.compile("(?:" + self.__mSearchTerm + ")$")
        file_list = ""

	# Process output as it is generated, instead of waiting for it to finish.
	# Otherwise, the output could be a huge >1GB file, depending on what the command is.
        r, w, e = popen2.popen3(self.__mStraceCmd)

        # strace has everything on one a line, so it can be parsed
        #  a line at a time
        for line in e:
	    # Take a line that contains a command and its arguments
	    exec_match = exec_re.search(line)
	    if exec_match:
		command = exec_match.group(1)
		args_string = exec_match.group(2)

		# Check if the command is one in our search terms
		cmd_match = cmd_re.search(command)
		if cmd_match and line.find("ENOENT") == -1:

		    # Split out the argments from the comma-delimited list,
		    # and remove the encompassing quotation marks
		    args_quote_list = args_string.split(", ")
		    args_list = [a.strip('"') for a in args_quote_list]

		    # Now rebuild them into a space-delimited string, except the first one
		    # which is the name of our command
		    arg_list = " ".join(args_list[1:])

		    # We format it this way to make it easier for the post-processor to get the
		    # simulator command, which it will need.
		    file_list += format_line(command, arg_list)

        r.close()
        e.close()
        w.close()
        return file_list


# <Options>
#  <option sim="vcs" name="-f" [numparams="1"] [newname=""] />
# </Options>
# If numparams is absent, default to 0.
# If newname is absent, remove.
# If newname is non-empty, replace the name with that string.

class paramFilter(xml.sax.ContentHandler):
    def __init__(self):
	self.__mOptions = {}
	self.__mFileNamePatterns_re = ""

    # sax callback
    def startElement(self, name, attributes):
	if name == "option":
	    optname = str(attributes.get("name", ""))
	    if optname:
		numparams = int(attributes.get("numparams", 0))
		newname = str(attributes.get("newname", ""))
		self.__mOptions.setdefault(optname, {})["numparams"] = numparams
		self.__mOptions[optname]["newname"] = newname

	elif name == "filename":
	    pattern = str(attributes.get("pattern", ""))
	    try:
		pattern_re = re.compile("^" + pattern + "$")
	    except:
		print "Illegal regular expression in XML file"

	    self.__mFileNamePatterns_re = pattern_re

    def get_dict(self):
	return self.__mOptions

    # Take a line of arguments and the command and apply the filter.
    def filter(self, line):
	new_args = []
	# This will break for multi-word arguments inside of quotes.  Ignore for now.
	old_args = line.split()

	# Look at each argument, and see if it is in the options file.
	while old_args:
	    orig_token = old_args.pop(0)
	    token = orig_token.lower()
	    # Truncate the parts of arguments that start with + or =.
	    # e.g. +define+foo=bar becomes just +define
	    # (Note that +define is a bad example, since we want to pass +define to cbuild,
	    # so it will never actually appear in the sim_options.xml file.  +acc is
	    # a better example.)
	    findequal = token.find("=", 1)
	    if findequal != -1:
		token = token[:findequal]
	    findplus = token.find("+", 1)
	    if findplus != -1:
		token = token[:findplus]

	    # Is this an option?
	    if self.__mOptions.has_key(token):
		newname = self.__mOptions[token]["newname"]
		if newname:
		    # Substitute different parameters, if indicated.
		    new_args.append(newname)
		else:
		    new_args.append(orig_token)

		# Consume the extra parameters
		for i in xrange(self.__mOptions[token]["numparams"]):
		    if old_args:
			new_args.append(old_args.pop(0))

	    # Is this a filename?
	    elif self.__mFileNamePatterns_re.search(token):
		# Put in the original argument, e.g. +define+foo=bar
		new_args.append(orig_token)

	    # It's nothing in our list, don't put it in the output file
	    else:
		pass

	return " ".join(new_args)

    def read(self, filename):
	# Make sure we have a valid file
	try:
	    f = open(filename, "r")
	except IOError, (errno, strerror):
	    print "Error: could not read designHierarchy file '%s', reason: '%s'" % \
		  (filename, strerror)
	    return errno
	except:
	    print "Error: could not read designHierarchy file '%s', unexpected error: '%s'" % \
		  (filename, sys.exc_info()[0])
	    return 6

	# Parse the file
	try:
	    xml.sax.parse(f, self)
	except xml.sax.SAXException, msg:
	    print msg
	    return 7

	return 0

class dashf:
    def __init__(self, dashf_term):
	self.__mDashfTerm = dashf_term
	self.__mIs_f = re.compile(r"\s*((?:" + dashf_term + r")\s+(\S+))")      # search for -f in file
	self.__mTab = 4			# Arbitrary tab setting
	self.__mDashfLines = ""

    # This is the external call
    def dashf(self, line):
	self.__mDashfLines = ""
	return self.dash_f(line)

    def dash_f(self, line):
	parameters = []

	# There may be more than one -f in a line
	while 1:
	    # group(1) is the -f and parameter; group(2) is the parameter only
	    fmatch = self.__mIs_f.search(line.lower())      # See if this is a -f
	    if fmatch:
		# Save the filename
		parameters.append(fmatch.group(2))
		# Remove the -f from the line
		line = line[:fmatch.start(1)] + line[fmatch.end(1):]
		self.__mDashfLines += "// (stripping -f " + fmatch.group(2) + ")\n"
	    else:
		break

	# this is command line with the -f stripped
	self.__mDashfLines += line + "\n"

	for filename in parameters:
	    if (not self.dash_f_file(filename)):     # Open and scan the file
		self.__mDashfLines += "// *** Warning: file \""+filename+"\" doesn't exist\n"

	return self.__mDashfLines


    def dash_f_file(self, filename, indent = 0):
	f = self.openfile(filename)
	if not f:
	    return 0

	for line in f:
	    self.dash_f(line)		# Recursively look for more
	f.close()
	return 8

    def openfile(self, filename):
	try:
	    f = open (filename, 'r')
	    return f
	except:
	    return 0

    def get_lines(self):
	return  self.__mDashfLines

def format_line(command, args):
    return "/* (" + command + " " + args + ") */\n" + args + "\n"



def run(argv):
    collector = FileCollector()
    return collector.run(argv)

def name():
    return __name__.split('.')[-1]

def brief():
    return "Intercept simulator commands and extract the file names from their parameter lists."

if __name__ == "__main__":
    run(sys.argv)
