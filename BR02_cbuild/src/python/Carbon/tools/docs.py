#******************************************************************************
# Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

def name():
    return __name__.split('.')[-1]

def brief():
    return "View the Carbon documentation in a web browser."

def run(argv):
    import os.path
    import webbrowser

    if len(argv) > 2:
        print 'usage: %s [url]' % self.name()
        print '  The url (if any) should be relative to CARBON_HOME'
        return 1

    url = os.path.join('userdoc', 'index.html')
    if len(argv) == 2:
        url = argv[1]

    # find the documentation
    doc = os.path.join(os.environ['CARBON_HOME'], url)
    if not os.path.exists(doc) and not os.path.exists(doc[0:doc.rfind('#')]):
      print "File does not exist: %s" % doc
      if os.environ.get('CARBON_BIN', '') != '':
        doc = 'http://intranet/' + url
      else:
        return 1
    else:
      doc = 'file://' + doc
      
    # launch a browser with the requested document
    webbrowser.open(doc, 0, 1)

    # assume everything went OK
    return 0

if __name__ == "__main__":
    import sys
    run(sys.argv)
