#******************************************************************************
# Copyright (c) 2003-2014 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

# $Revision: 1.264 $

## NOTE if you want to insert a breakpoint into this file just put the following two stmts at the point you want to break:
##     import pdb; pdb.set_trace()

import sys
import os
import re
import string
import Carbon.outputStream
from Carbon.carboncfg import *

def generate_xtor_port_factory_decl(config):
  code = ''
  for portFactory in config.xtorPortFactories():
    code += portFactory.forwardDeclaration()
  return code

def generate_xtor_port_factories(config, userCode):
  code = ''
  for portFactory in config.xtorPortFactories():
    code += portFactory.code(userCode)
  return code
  
def generate_friend_port_class_def(config, module_name, userCode):
  code = ""
  for compPort in config.compPorts():
    code += compPort.classDefinition(userCode)
  return code

def generate_friend_port_def(config):

  friends = []
  for compPort in config.compPorts():
    friends.append(compPort.friendPortDef())
  for outPort in config.compOutputPorts():
    friends.append(outPort.friendPortDef())

  # Make sure that it is a unique list of friends
  code = ""
  for f in set(friends):
    code += f
  return code

def generate_mxdi_forward_decl(config):
  # We need a forward declaration for the top component's CADI as well
  # as any subcomponents.
  code = config.mxdi().generate_mxdi_forward_decl()
  for subcomp in config.subComps():
    code += subcomp.mxdi().generate_mxdi_forward_decl()
  return code

# a generator function that returns a sequence of the unique master clocks used in all transactors
def unique_xtorClks(config):
  uniqClks = {'Default':1}   # track the clocks have been returned
  yield 'Default'
  for xtor in config.transactors():
     clockMaster = xtor.clockMaster()
     if clockMaster != None:
       if not uniqClks.has_key(clockMaster.name()):         
         uniqClks[clockMaster.name()] = 1
         yield clockMaster.name()

def generate_event_queue_var_decl(config):
  if using_event_queues(config):
    code = ""
    for xtorClk in unique_xtorClks(config):
      code += """ 
  set<CarbonXtorEvent*> currEventQueue%(clkId)s;
  set<CarbonXtorEvent*> nextEventQueue%(clkId)s;
  bool valid_cycle_%(clkId)s;
  bool mCopyDone%(clkId)s;
  """ % { 'clkId' : xtorClk }
  else:
    code = ""
  return code

def conditionally_generate_if_has_drive_notify(config, has_drive_notify_code, not_has_drive_notify_code):
  if has_drive_notify(config):
    code = has_drive_notify_code
  else:
    code = not_has_drive_notify_code
  return code


def generate_combinatorial_func_def(config, module_name, userCode):
  code = ""
  if has_drive_notify(config):
    code += """\
void %(mod_name)s::combinatorial(bool forwardValid, bool backwardValid, bool forwardReady,  bool backwardReady)
{
%(userCodePreCombinatorial)s

  // Proces valid signal
%(process_combinatorial_events)s  
%(userCodePostCombinatorial)s
}
""" % {"mod_name":             module_name,
      'userCodePreCombinatorial': userCode.preSection('%s::combinatorial' % module_name),
      'process_combinatorial_events': generate_process_combinatorial_events(config),
      'userCodePostCombinatorial': userCode.postSection('%s::combinatorial' % module_name)
      }
  return code


# Generate code that updates the current event queue from the next
# This must be done before the carbonSchedule code so that any
# events sitting in the current queue for the next cycle are
# not clobbered. The events in the current queue get put
# there by callbacks from the carbonSchedule call.
def generate_nextevent_update(config, clockName, guard):
  code = ""
  if using_event_queues(config):
    # We only want the update to happen once based on which event
    # arrived first. Either the clock transactor update or the
    # component update.
    #
    # If the queue is unconditional no guard should be used. There is
    # assumed there is only one call to do this update and so we don't
    # have to guard it.
    if guard:
      code += """\
if (!mCopyDone%(clkId)s) {
      mCopyDone%(clkId)s = true;
    """ % { 'clkId' : clockName } 

    # Add the update in all cases
    code += """\
  currEventQueue%(clkId)s = nextEventQueue%(clkId)s;
""" % { 'clkId' : clockName }

    # close the guard if statement
    if guard:
      code += """\
    }"""
  return code


def generate_event_queue_reset(config):
  if using_event_queues(config):
    code = ""
    for xtorClk in unique_xtorClks(config):
      code += """ 
  currEventQueue%(clkId)s.clear();
  nextEventQueue%(clkId)s.clear();
  valid_cycle_%(clkId)s = false;
  mCopyDone%(clkId)s = false;
  """ % { 'clkId' : xtorClk }
  else:
    code = ""
  return code

# generate code to extract and process each event from eventQueue
def generate_process_event_queue(clockName='Default'):
  code = """\
  {
    flowThruCommunicate();
    set<CarbonXtorEvent*>::iterator event_iter;
    while (not currEventQueue%(clkId)s.empty()) {
      event_iter = currEventQueue%(clkId)s.begin();
      CarbonXtorEvent* xtor_event = *event_iter;
      currEventQueue%(clkId)s.erase(event_iter);
      (*xtor_event)();  
    }
  }
""" % { 'clkId' : clockName }
  return code

def generate_current_cycle_dcl(config):
  if has_drive_notify(config) and not has_cycle_clock(config):
    code = "  // uint64_t crtCycle = eslapi::MxSimAPI::getMxSimAPI()->getCycleCount();"
  else:
    code = ""
  return code

def generate_process_communicate_ready_valid_events(config):
  code = """\
%(generate_current_cycle_dcl)s
%(generate_component_preSendDrive)s
%(generate_component_sendDrive)s
%(generate_component_preSendNotify)s
%(generate_component_sendNotify)s
%(generate_component_postSendNotify)s
""" % {'generate_current_cycle_dcl': generate_current_cycle_dcl(config),
       'generate_component_preSendDrive': generate_component_preSendDrive(config),
       'generate_component_sendDrive': generate_component_sendDrive(config),
       'generate_component_preSendNotify': generate_component_preSendNotify(config),
       'generate_component_sendNotify': generate_component_sendNotify(config),
       'generate_component_postSendNotify': generate_component_postSendNotify(config)
       }

  return code

def generate_process_combinatorial_ready_valid_events(config):
  code = """\
%(generate_current_cycle_dcl)s
  flowThruCommunicate();
%(generate_forward_valid)s
%(generate_backward_valid)s
%(generate_forward_ready)s
%(generate_backward_ready)s
""" % {'generate_current_cycle_dcl': generate_current_cycle_dcl(config),
       'generate_forward_ready': generate_forward_ready(config),
       'generate_forward_valid': generate_forward_valid(config),
       'generate_backward_ready': generate_backward_ready(config),
       'generate_backward_valid': generate_backward_valid(config)
    }
  return code

def generate_process_communicate_events(config):
  if using_event_queues(config):
    code = "%(process_event_queue)s" % { 'process_event_queue' : generate_process_event_queue() }
  else:
    code = "%(process_communicate_ready_valid_events)s" % { 'process_communicate_ready_valid_events' : generate_process_communicate_ready_valid_events(config) }
  return code

def generate_process_combinatorial_events(config):
  code = ""
  if using_event_queues(config):
    # here we process the default queue first, is it important to do so?
    code = "%(process_event_queue)s" % { 'process_event_queue' : generate_process_event_queue() }
    for xtorClk in unique_xtorClks(config):
       if not xtorClk == 'Default':
          code += """
  if (valid_cycle_%(clkValid)s) %(process_event_queue)s""" % { 'clkValid' : xtorClk, 'process_event_queue' :  generate_process_event_queue(xtorClk)}
  else:
    code = "%(process_combinatorial_ready_valid_events)s" % { 'process_combinatorial_ready_valid_events' : generate_process_combinatorial_ready_valid_events(config) }
  return code


def port_instance_decls(config):
  code = ""
  for compPort in config.compPorts():
    code += compPort.instanceDeclaration()
  return code

def port_net_ids(config):
  code = ''
  portNames = {}
  for port in config.inputs():
    portNames[port.carbonNetId()] = 1
  for xtor in config.transactors():
    for port in xtor.inputs():
      portNames[port.carbonNetId()] = 1
    for port in xtor.outputs():
      portNames[port.carbonNetId()] = 1
  for port in config.outputs():
    portNames[port.carbonNetId()] = 1
  for port in config.resets():
    portNames[port.carbonNetId()] = 1
  for port in config.clocks():
    portNames[port.carbonNetId()] = 1
  for port in config.ties():
    portNames[port.carbonNetId()] = 1

  names = portNames.keys()
  names.sort()
  for name in names:
    code += '  CarbonNetID *%s;\n' % name
  return code

def generate_callback_members(config):
  code = ''
  for compPort in config.compPorts():
    code += compPort.generateCallbackMembers()
  return code
    

def generate_component_common_src_def(config):
  code = ""
  code += """

static CarbonUInt32 streamWriteFunc( void* stream, const void* data, CarbonUInt32 numBytes )
{
  ((eslapi::CASIODataStream*) stream)->writeBytes( (const char*)data, numBytes );
  return numBytes;
}

static CarbonUInt32 streamReadFunc( void* stream, void* data, CarbonUInt32 numBytes )
{
  char* temp_buffer = NULL;
  CarbonUInt32 nb_return = 0;
  ((eslapi::CASIIDataStream*) stream)->readBytes( temp_buffer, nb_return );
  memcpy(data, temp_buffer, nb_return);
  delete[] temp_buffer;
  return nb_return;
}
"""
  return code

def generate_reset_source_def(config, mxdi):
  code = ""

  # set the cycle number to -1 during the reset sequence
  code += """\
  mCarbonWaveCycleNumber = -1;
  if (%(waveFormat)s == "FSDB") {
    carbonWaveUserDataChange(mCarbonWaveCycleUserData);
  }
""" % {'waveFormat': config.waveFormatParameter().variable()}

  # Reset CADI
  code += mxdi.componentReset()

  # restart all clocks from the beginning
  code += """
  // Reset Simulation queue and restart all clock and reset generators
  mSimulationQueue->reset();
"""

  # Reset component ports, and clock and reset generators.
  for compPort in config.compPorts():
    code += compPort.componentReset()

  # execute the reset edges along with the rest of the generated clocks
  code += """
  runTicks(mCarbonSimulationTime + %s);
""" % config.compResetTicks()

  # Tell clock generators that reset is done
  for compPort in config.compPorts():
    code += compPort.componentEndOfReset()

  # Parameters for eslPorts
  for param in config.parameters():
    code += param.driveESLInputs()
  
  return code

def generate_interconnect_source_def(config, userCode, module_name):
  compICCode = ''
  # Get Interconnect code from each port object
  for compPort in config.compPorts():
      compICCode += compPort.componentInterconnect()

  # Legacy static scheduling which is deprecated. The customer can enable it for now
  compFTCode = ""
  if config.useStaticScheduling():
    compFTCode += """\
  // Flow Through Setup
#if MAXSIM_MAJOR_VERSION >= 7
"""  

    # Check if there are any flow-through paths in the component, if there are, we need to
    # register the flowThroughCommunicate method
    if config.hasFlowThru() :
      compFTCode += """    eslapi::CASIClockMaster* componentMaster = static_cast<eslapi::CASIClockMaster*>(getClockMaster());
   componentMaster->registerCommunicate(this,&%(module_name)s::flowThruCommunicate,"flowThruCommunicate");
""" % {'module_name' : module_name }

    # Get Flowthrough Setup code from each port object
    for compPort in config.compPorts():
      compFTCode += compPort.componentFlowThruSetup()

    compFTCode += """\
#endif
"""

  code = """
%(usercodepre)s
%(customCodePreInterconnect)s
%(interconnect_pre)s

  // Call the base class interconnect
  eslapi::CASIModule::interconnect();

%(interconnect_post)s
%(customCodePostInterconnect)s
%(usercodepost)s
""" % {'usercodepre': userCode.preSection(module_name + '::interconnect'),
       'interconnect_pre'  : compICCode,
       'interconnect_post' : compFTCode,
       'usercodepost'      : userCode.postSection(module_name + '::interconnect'),
       'customCodePreInterconnect' : config.getCustomCodePre('interconnect'),
       'customCodePostInterconnect': config.getCustomCodePost('interconnect') }

  return code

def initialize_waves(config, userCode):
  # We can only dump the SoCD cycle number to FSDB waveforms, and only
  # if we're in a SoCD context
  waveUserData = """
    if ((mMxSimAPI != NULL) && (%(waveFormat)s == "FSDB")) {
      mCarbonWaveCycleUserData = carbonAddUserData(mCarbonWave,
                        eCARBON_VT_VCD_INTEGER,
                        eCarbonVarDirectionImplicit,
                        eCARBON_DT_VERILOG_INTEGER,
                        0, 0, // msb and lsb are the same
                        &mCarbonWaveCycleNumber, 
                        "SoCDesigner_Cycle", 
                        eCARBON_BYTES_PER_BIT_4B,
                        "%(topLevelModule)s.SoCDesigner_Cycle",
                        ".");
      carbonWaveUserDataChange(mCarbonWaveCycleUserData);
    }""" % { 'waveFormat': config.waveFormatParameter().variable(),
             'topLevelModule': config.topModuleName() }
  code = """\
  if (mCarbonObj && %(dumpWaves)s) {
    CarbonTimescale timescale = sConvertStringToTimescale(%(waveTimescale)s);
    mCarbonWaveFilename = %(waveParameter)s;
    // Append/correct file extension as necessary, depending on waveform format
    const std::string fsdbExt(".fsdb");
    const std::string vcdExt(".vcd");
    size_t fsdbPos = mCarbonWaveFilename.rfind(fsdbExt);
    size_t vcdPos = mCarbonWaveFilename.rfind(vcdExt);
    bool endsInFsdb = (fsdbPos == mCarbonWaveFilename.length() - fsdbExt.length());
    bool endsInVcd = (vcdPos == mCarbonWaveFilename.length() - vcdExt.length());
    if ((%(waveFormat)s == "VCD") && !endsInVcd) {
      if (endsInFsdb) {
        mCarbonWaveFilename.erase(fsdbPos);
      }
      mCarbonWaveFilename.append(vcdExt);
    } else if ((%(waveFormat)s == "FSDB") && !endsInFsdb) {
      if (endsInVcd) {
        mCarbonWaveFilename.erase(vcdPos);
      }
      mCarbonWaveFilename.append(fsdbExt);
    }
    // If the component has been reset, create a new filename
    if (mNumResets > 0) {
      // If the original filename was design.fsdb, the new name is design_N.fsdb, where
      // N is the number of resets.
      std::ostringstream ins;
      ins << "_" << mNumResets;
      string::size_type dot = mCarbonWaveFilename.rfind('.');
      if (dot == string::npos) {
        dot = mCarbonWaveFilename.length () - 1;
      }
      mCarbonWaveFilename.insert(dot, ins.str());
    }
    message(eslapi::CASI_MSG_INFO, "Dumping waveforms to %%s", mCarbonWaveFilename.c_str());
    if (%(waveFormat)s == "VCD") {
      mCarbonWave = carbonWaveInitVCD(mCarbonObj, mCarbonWaveFilename.c_str(), timescale);
    } else if (%(waveFormat)s == "FSDB") {
      mCarbonWave = carbonWaveInitFSDB(mCarbonObj, mCarbonWaveFilename.c_str(), timescale);
    }
    %(userCodePreSection)s
    carbonDumpVars(mCarbonWave, 0, "%(mod_name)s");
    %(userCodePostSection)s
    %(waveUserData)s
    if (mMxSimAPI != NULL) {
      mMxSimAPI->registerStatusListener(this);
    }
  }
  return mCarbonWave != NULL;""" % {
      'dumpWaves': config.dumpWavesParameter().variable(),
      'waveFormat': config.waveFormatParameter().variable(),
      'waveParameter': config.waveFileParameter().variable(),
      'waveTimescale': config.waveTimescaleParameter().variable(),
      'mod_name': config.topModuleName(),
      'userCodePreSection' : userCode.preSection('DumpVars'),
      'userCodePostSection' : userCode.postSection('DumpVars'),
      'waveUserData': waveUserData
      }
  return code

def generate_init_id_source_def(config, top_mxdi):

  def generate_find_net(port):
    return  '  ' + port.carbonNetId() + ' = carbonFindNet(mCarbonObj, "' + port.carbonName() + '");\n' \
            '  if (!' + port.carbonNetId() + ') message (eslapi::CASI_MSG_ERROR, "carbonFindNet(' + port.carbonName() + ') failed");\n'

  code = ""
  for portList in (config.inputs(), config.outputs(), config.resets(),
                   config.clocks(), config.ties()):
    code += '\n'.join(map(generate_find_net, portList))
  for xtor in config.transactors():
    for port in xtor.inputs():
      code += generate_find_net(port)
    for port in xtor.outputs():
      code += generate_find_net(port)
    code += '\n'

  code += "\n\n"
  return code


def generate_init_cb_source_def(config):
  code = ""
  for compPort in config.compPorts():
    code += compPort.componentInit()
  return code


def generate_ties(config):
  code = ''
  for tie in config.ties():
    if tie.type() == 'TieParam':
      # Find the Parameter we're tied to, so we can call it to write the code
      for parm in config.parameters():
        if parm.name() == tie.getParam():
          code += parm.setTieParam(tie)
          
    else:
      # For every other type, get the words one at time
      varDict = { 'netName': tie.carbonName(), 'netHandle': tie.carbonNetId(), 'numUInt32s': tie.numWords() }
      code += """\
  {
    UInt32 value[%(numUInt32s)s];""" % varDict
      for i in range(tie.numWords()):
        code += """
    value[%d] = 0x%x;""" % (i, tie.getWord(i))
      code += """
    if (carbonIsForcible(mCarbonObj, %(netHandle)s)) {
      carbonForce(mCarbonObj, %(netHandle)s, value);
    }
    else {
      carbonDepositFast(mCarbonObj, %(netHandle)s, value, 0);
    }
  }
""" % varDict

  # Mark Parameters for eslPorts
  for param in config.parameters():
    code += param.driveESLInputs()
    
  return code


def generate_cycle_carbondeposit_def(config):
  code = ""

  # update any transactor adapter components
  for compPorts in config.compPorts():
    if not compPorts.hasCycleClock(): code += compPorts.componentUpdate()

  return code


def generate_runticks(config):
  return """
  // run all generated clocks for one SoC Designer cycle and advance time
  runTicks(mCarbonSimulationTime + CARBON_TICKS_PER_MAXSIM_CYCLE);
"""

def generate_component_communicate(config):
  code = ''
  for compPort in config.compPorts():
    if not compPort.hasCycleClock(): code += compPort.componentCommunicate()
  return code

# Check if any of the transactors use sendDrive/sendNotify
def has_drive_notify(config):
  for compPort in config.compPorts():
    if compPort.hasDriveNotify():
      return True
  return False

# Returns true if any of the transactors use event queues
def using_event_queues(config):
  for compPort in config.compPorts():
    if compPort.useXactorEventQueues():
      return True
  return False

# returns true if any transactor has a cycle clock
def has_cycle_clock(config):
  code = ''
  for compPort in config.compPorts():
    if compPort.hasCycleClock():
      return True
  return False

def generate_component_preSendDrive(config):
  # If it has a sendDrive it needs a flow thru call
  if has_drive_notify(config):
    code = '  flowThruCommunicate();'
  else:
    code = ''
  return code

def generate_component_sendDrive(config):
  code = ''
  for compPort in config.compPorts():
    if not compPort.hasCycleClock(): code += compPort.componentSendDrive()
  return code

def generate_component_preSendNotify(config):
  # If it has a sendNotify it needs a flow thru call
  if has_drive_notify(config):
    code = '  flowThruCommunicate();'
  else:
    code = ''
  return code

def generate_component_sendNotify(config):
  code = ''
  for compPort in config.compPorts():
    if not compPort.hasCycleClock(): code += compPort.componentSendNotify()
  return code

def generate_combinatorial_callback_fn(config, mod_name):
  code = ''
  if has_drive_notify(config):
    code += '''\
static void combinatorialCB(void *userData, bool forwardValid, bool backwardValid, bool forwardReady, bool backwardReady)
{
  %(mod_name)s* comp = reinterpret_cast<%(mod_name)s*>(userData);
  comp->combinatorial(forwardValid, backwardValid, forwardReady, backwardReady);
}
''' % { 'mod_name': mod_name }
  return code

def generate_component_postSendNotify(config):
  # If it has a sendNotify it needs a flow thru call
  if has_drive_notify(config):
    code = '''\
#ifdef POST_SENDNOTIFY_FLOWTHRU
  flowThruCommunicate();
'''
    for compPort in config.compPorts():
      if compPort.hasDriveNotify():
        code += compPort.componentDriveAllSignals() + '\n'
    code += '#endif\n'
  else:
    code = ''
  return code

def add_line_prefix(s, prefixString):
  s = string.split(s, '\n')
  s = [prefixString + line for line in s]
  if ( s[-1] == prefixString ):
    s = string.join(s[0:-1], '\n') + '\n'
  else:
    s = string.join(s, '\n')
  return s

def generate_forward_valid(config):
  code, actions = "", ""
  for compPort in config.compPorts():
    actions += compPort.forwardValid()
  if ( actions != "" ):
    code = "  if (forwardValid) {\n"
    code += add_line_prefix(actions,"  ")
    code += "  }"
  return code

def generate_backward_valid(config):
  code, actions = "", ""
  for compPort in config.compPorts():
    actions += compPort.backwardValid()
  if ( actions != "" ):  
    code = "  if (backwardValid) {\n"
    code += add_line_prefix(actions,"  ")
    code += "  }"
  return code

def generate_forward_ready(config):
  code, actions = "", ""
  for compPort in config.compPorts():
    actions += compPort.forwardReady()
  if ( actions != "" ):  
    code = "  if (forwardReady) {\n"
    code += add_line_prefix(actions,"  ")
    code += "  }"
  return code

def generate_backward_ready(config):
  code, actions = "", ""
  for compPort in config.compPorts():
    actions += compPort.backwardReady()
  if ( actions != "" ):  
    code = "  if (backwardReady) {\n"
    code += add_line_prefix(actions,"  ")
    code += "  }"
  return code

def generate_debug_transaction_func(config, module_name, userCode):
  code = '''\
eslapi::CASIStatus %(mod_name)s::debugTransaction(eslapi::CASITransactionInfo* info)
{
  eslapi::CASIStatus status = eslapi::CASI_STATUS_NOTSUPPORTED;
  if (getParameter(\"Enable Debug Messages\") == \"true\" ) message(eslapi::CASI_MSG_INFO, \"Debug Transaction received by %(mod_name)s.\");

%(debug_transaction_userPre)s
%(debug_transaction_custPre)s


%(debug_transaction_custPost)s
%(debug_transaction_userPost)s
  return status;
}
''' % { 'mod_name': module_name,
        'debug_transaction_userPre' : userCode.preSection('%s::debugTransaction' % module_name),
        'debug_transaction_userPost': userCode.postSection('%s::debugTransaction' % module_name),
        'debug_transaction_custPre':  config.getCustomCodePre('debugTransaction'),
        'debug_transaction_custPost': config.getCustomCodePost('debugTransaction')

        }
  return code
    
def generate_debug_transaction_decl(config):
  code = '''\
  eslapi::CASIStatus debugTransaction(eslapi::CASITransactionInfo* info);'''
  return code

def generate_debug_access_func(config, module_name, userCode):
  code = '''\
CarbonDebugAccessStatus %(mod_name)s::debugAccess(const std::string& callingSlave, CarbonDebugAccessDirection dir, uint64_t addr, uint32_t numBytes, uint8_t* buf, uint32_t* numBytesAccepted, uint32_t* ctrl)
{
  CarbonDebugAccessStatus status = eCarbonDebugAccessStatusOutRange;
  if (getParameter("Enable Debug Messages") == "true" ) message(eslapi::CASI_MSG_INFO, "Debug Transaction received by %(mod_name)s::%%s.", callingSlave.c_str()); 
%(debug_access_userPre)s
%(debug_access_custPre)s


%(debug_access_custPost)s
%(debug_access_userPost)s
  return status;
}
''' % { 'mod_name': module_name,
        'debug_access_userPre' : userCode.preSection('%s::debugAccess' % module_name),
        'debug_access_userPost': userCode.postSection('%s::debugAccess' % module_name),
        'debug_access_custPre':  config.getCustomCodePre('debugAccess'),
        'debug_access_custPost': config.getCustomCodePost('debugAccess')
        }
  return code
    
def generate_debug_access_decl(config):
  code = '''\
  CarbonDebugAccessStatus debugAccess(const std::string& callingSlave, CarbonDebugAccessDirection dir, uint64_t addr, uint32_t numBytes, uint8_t* buf, uint32_t* numBytesAccepted, uint32_t* ctrl);'''
  return code

def generateSRInterfaceDecl(config):
  if config.supportsArchSaveRestore():
    code = """\
  // implementation of CASISaveRestoreArch interface methods
  virtual eslapi::CASISaveRestoreArchCallbackIF* getCB();
  virtual eslapi::CASISaveRestoreStreamType saveStreamType();
  virtual bool saveDataBinary( eslapi::CASIODataStream &data );
  virtual bool restoreDataBinary( eslapi::CASIIDataStream &data );
  virtual bool saveDataArch( eslapi::CASIODataStream &data );
  virtual bool restoreDataArch( eslapi::CASIIDataStream &data );"""
  else:
    code = """\
  // implementation of CASISaveRestore interface methods
  virtual bool saveData( eslapi::CASIODataStream &data );
  virtual bool restoreData( eslapi::CASIIDataStream &data );"""
  return code

def generateSRArchMembers(config):
  code = ''
  if config.supportsArchSaveRestore():
    code = """\
  CarbonSaveRestoreArchCallback* mCB;
  bool mArchitecturalSave;
  bool mValidateArchitecturalRestore;"""
  return code

def generateSRArchMemberInit(config):
  code = ''
  if config.supportsArchSaveRestore():
    code = """\
  mCB = NULL;
  mArchitecturalSave = false;
  mValidateArchitecturalRestore = false;"""
  return code

def generateSRArchMemberDestroy(config):
  code = ''
  if config.supportsArchSaveRestore():
    code = """\
  delete mCB;"""
  return code

def generateSetParameterArchSave(config):
  # These parameters only exists for models that support architectural
  # save/restore.  It allows architectural save to be used for CA
  # models instead of the default binary save, and also for validation to
  # occur with the architectural restore.
  code = ''
  if config.supportsArchSaveRestore():
    code = """\
  if (name == "Architectural Save") {
    status = CASIConvertStringToValue(value, &mArchitecturalSave);
    if (status == eslapi::CASIConvert_SUCCESS) {
      return;
    }
  }
  if (name == "Validate Architectural Restore") {
    status = CASIConvertStringToValue(value, &mValidateArchitecturalRestore);
    if (status == eslapi::CASIConvert_SUCCESS) {
      return;
    }
  }"""
  return code

def generateGetParameterArchSave(config):
  # Intercept the hidden parameters
  code = ''
  if config.supportsArchSaveRestore():
    code = """\
  if (key == "Architectural Save") {
    return mArchitecturalSave ? "true" : "false";
  }
  if (key == "Validate Architectural Restore") {
    return mValidateArchitecturalRestore ? "true" : "false";
  }"""
  return code

def generate_carbonMems_def(config):
  code = ''
  if(config.mxdi().useLegacyMemories()):
    code = '  CarbonMemoryID** carbonMems;'
  else:
    code = '  CarbonDebugMemory** carbonMems;'
  return code

def generate_debuggable_point_functions(config, userCode):
  return config.debuggablePointFnDefs(userCode)

def generate_debuggable_point_functions_impl(config, userCode):
  return config.debuggablePointFnImpl(userCode)

def generate_debuggable_point_get_parameter(config):
  code = ''
  if config.procInfo().debuggablePoint():
    code = '''\
  if (key == "stalledstop: can_you_stop") {
    if (procCanStop()) {
      return "yes";
    } else {
      return "no";
    }
  }
'''
  return code

def generate_debuggable_point_set_parameter(config):
  code = ''
  if config.procInfo().debuggablePoint():
    code = '''\
  if (name == "stalledstop: prepare_to_stop") {
    procStopAtDebuggablePoint(true);
    return;
  } 
  else if( name == "stalledstop: resume_exec" ) {
    procStopAtDebuggablePoint(false);
    return;
  } 
'''
  return code

def generateSoCDPCTraceReporterInclude(config):
  # If the .ccfg specifies an accessor method for getting the
  # SoCDPCTraceReporter object, emit the include of the header and a
  # using declaration for the namespace.  We need to check the main
  # component as well as all its subcomponents, because they all get
  # emitted into the same source file.
  code = ''
  needsInclude = False
  if config.qualifiedPCTraceAccessor() != "":
    needsInclude = True
  else:
    for subcomp in config.subComps():
      if subcomp.getComp().qualifiedPCTraceAccessor() != "":
        needsInclude = True

  if needsInclude:
    code = '''\
#include "common/SoCDPCTraceReporter.h"
using namespace Carbon::PCTrace;
'''
      
  return code

def generateSoCDPCTraceReporterObtainInterface(config):
  # If the .ccfg specifies an accessor method for getting the
  # SoCDPCTraceReporter object, emit the code to return it if the
  # requested name/revision match.
  code = ''
  accessor = config.qualifiedPCTraceAccessor()
  if accessor != "":
    code = '''\
  if ((strcmp(ifName, PCTraceProviderIF::IFNAME()) == 0) &&
      (minRev <= PCTraceProviderIF::IFREVISION())) {
    *actualRev = PCTraceProviderIF::IFREVISION();
    return dynamic_cast<SoCDPCTraceReporter*>(%s);
  }
''' % accessor
      
  return code

def generateAdditionalComponentDescription(config):
  # Add in additional component description.  For now, this includes
  # versions of CMS, SoCD, etc.

  # We always include the SoCD version
  code = 'description += "\\nSoC Designer version: ' + ".".join((str(config.mxVer()), str(config.mxMinorVer()), str(config.mxRevision()))) + '";\n'
  # Look for CMS version, model kit version, and model kit IP version
  # in the DB.  Include them only if they exist.
  db = config.getDB()
  if db:
    for (attrName, attrText) in ( ("CarbonModelInformation", "Model Information"),
                                  ("CMSVersion", "Model Studio version"),
                                  ("ModelKitIPVersion", "Model Kit version"),
                                  ("PGO Build", "Model Compiled with PGO")
                                  ):
      attrValue = db.getStringAttribute(attrName)
      if attrValue:
        code += 'description += "\\n' + attrText + ': ' + attrValue.replace("\n", "\\n") + '";\n'

  return code

def generateSaveComponentDataBinary(config):
  code = """
  // Save all the component state
  stream << mNumResets << mCarbonSimulationTime << mCycleCount << mResetSimulationQueueTimeScaling;

  // The simulation queue needs to save which clock defs are currently in the queue.
  // Build a map of all clock defs (in order) to an index that will be used when the queue is restored.
  CarbonSimulationQueue<%(compName)s>::ClockDefIndexMap clockDefMap;
""" % { 'compName': config.compName() }

  index = 0
  for port in config.compPorts():
    if port.isClockDefinition():
      # All clock definition object instances are pointers
      code += '  clockDefMap[%(instance)s] = %(index)d;\n' % { 'instance': port.clockDefinitionInstance(), 'index': index }
      index += 1
  
  code += """
  ok &= mSimulationQueue->saveData(stream, clockDefMap);

"""
  # Now save all the individual ports that need it.
  for port in config.compPorts():
    if port.needsSaveRestore():
      code += '  ok &= %(instance)s->saveData(stream);\n' % { 'instance': port.instanceName() }
      # The port may have callback objects with state
      for cb in port.loopValueChangeMembers():
        code += '  ok &= %(instance)s->saveData(stream);\n' % { 'instance': cb }


  # Profiling
  code += config.profile().save('ok', 'stream')

  return code

def generateRestoreComponentDataBinary(config):
  code = """
  // Restore all the component state
  stream >> mNumResets >> mCarbonSimulationTime >> mCycleCount >> mResetSimulationQueueTimeScaling;

  // Build a vector of clock definitions just as in saveData()
  CarbonSimulationQueue<%(compName)s>::ClockDefVec clockDefVec;
""" % { 'compName': config.compName() }

  for port in config.compPorts():
    if port.isClockDefinition():
      code += '  clockDefVec.push_back(%(instance)s);\n' % { 'instance': port.clockDefinitionInstance() }

  code += """
  ok &= mSimulationQueue->restoreData(stream, clockDefVec);

"""

  # Now restore all the individual ports that need it.
  for port in config.compPorts():
    if port.needsSaveRestore():
      code += '  ok &= %(instance)s->restoreData(stream);\n' % { 'instance': port.instanceName() }
      # The port may have callback objects with state
      for cb in port.loopValueChangeMembers():
        code += '  ok &= %(instance)s->restoreData(stream);\n' % { 'instance': cb }

  # Profiling
  code += config.profile().restore('ok', 'stream')

  return code
  
def generateSRInterfaceImpl(config, userCode):
  module_name = config.compName()
  if config.supportsArchSaveRestore():
    code = """\
eslapi::CASISaveRestoreArchCallbackIF* %(mod_name)s::getCB()
{
%(userCodePreGetCB)s
%(customCodePreGetCB)s
  if (mCB != NULL) {
    delete mCB;
  }
  std::list<eslapi::CASIMemoryRange> empty;
  mCB = new CarbonSaveRestoreArchCallback(getCADI(), empty, this, getParameter("Validate Architectural Restore") == "true");
%(userCodePostGetCB)s
%(customCodePostGetCB)s
  return mCB;
}

eslapi::CASISaveRestoreStreamType %(mod_name)s::saveStreamType()
{
%(userCodePreSaveStreamType)s
%(customCodePreSaveStreamType)s
  eslapi::CASISaveRestoreStreamType type = (getParameter("Architectural Save") == "true") ? eslapi::ArchStream : eslapi::BinaryStream;
%(userCodePostSaveStreamType)s
%(customCodePostSaveStreamType)s
  return type;
}

bool %(mod_name)s::saveDataBinary( eslapi::CASIODataStream &stream )
{
  bool ok = true;
%(userCodePreSaveDataBinary)s
%(customCodePreSaveDataBinary)s
  // Save the Carbon Model onto the SoC Designer stream.
  CarbonStatus stat = carbonStreamSave(mCarbonObj, streamWriteFunc, &stream, "maxsim stream" );
  ok = (stat == eCarbon_OK);
%(saveComponentDataBinary)s
%(userCodePostSaveDataBinary)s
%(customCodePostSaveDataBinary)s
  return ok;
}

bool %(mod_name)s::restoreDataBinary( eslapi::CASIIDataStream &stream )
{
  bool ok = true;
%(userCodePreRestoreDataBinary)s
%(customCodePreRestoreDataBinary)s
  CarbonStatus stat = carbonStreamRestore(mCarbonObj, streamReadFunc, &stream, "maxsim stream" );
  ok = (stat == eCarbon_OK);
%(restoreComponentDataBinary)s
%(userCodePostRestoreDataBinary)s
%(customCodePostRestoreDataBinary)s
  return ok;
}

bool %(mod_name)s::saveDataArch( eslapi::CASIODataStream &stream )
{
  bool ok = true;
%(userCodePreSaveDataArch)s
%(customCodePreSaveDataArch)s
  // Defer to parent implementation.
  ok = eslapi::CASISaveRestoreArch::saveDataArch(stream);
%(userCodePostSaveDataArch)s
%(customCodePostSaveDataArch)s
  return ok;
}

bool %(mod_name)s::restoreDataArch( eslapi::CASIIDataStream &stream )
{
  bool ok = true;
%(userCodePreRestoreDataArch)s
%(customCodePreRestoreDataArch)s
  // Defer to parent implementation.
  ok = eslapi::CASISaveRestoreArch::restoreDataArch(stream);
%(userCodePostRestoreDataArch)s
%(customCodePostRestoreDataArch)s
  return ok;
}""" % {
    'mod_name': module_name,
    'saveComponentDataBinary': generateSaveComponentDataBinary(config),
    'restoreComponentDataBinary': generateRestoreComponentDataBinary(config),
    "userCodePreGetCB" : userCode.preSection(module_name + "::getCB"),
    "userCodePostGetCB" : userCode.postSection(module_name + "::getCB"),
    "customCodePreGetCB" : config.getCustomCodePre("getCB"),
    "customCodePostGetCB" : config.getCustomCodePost("getCB"),
    "userCodePreSaveStreamType" : userCode.preSection(module_name + "::saveStreamType"),
    "userCodePostSaveStreamType" : userCode.postSection(module_name + "::saveStreamType"),
    "customCodePreSaveStreamType" : config.getCustomCodePre("saveStreamType"),
    "customCodePostSaveStreamType" : config.getCustomCodePost("saveStreamType"),
    'userCodePreSaveDataBinary': userCode.preSection('%s::saveDataBinary' % module_name),
    'userCodePostSaveDataBinary': userCode.postSection('%s::saveDataBinary' % module_name),
    'customCodePreSaveDataBinary': config.getCustomCodePre("saveDataBinary"),
    'customCodePostSaveDataBinary': config.getCustomCodePost("saveDataBinary"),
    'userCodePreRestoreDataBinary': userCode.preSection('%s::restoreDataBinary' % module_name),
    'userCodePostRestoreDataBinary': userCode.postSection('%s::restoreDataBinary' % module_name),
    'customCodePreRestoreDataBinary': config.getCustomCodePre("restoreDataBinary"),
    'customCodePostRestoreDataBinary': config.getCustomCodePost("restoreDataBinary"),
    'userCodePreSaveDataArch': userCode.preSection('%s::saveDataArch' % module_name),
    'userCodePostSaveDataArch': userCode.postSection('%s::saveDataArch' % module_name),
    'customCodePreSaveDataArch': config.getCustomCodePre("saveDataArch"),
    'customCodePostSaveDataArch': config.getCustomCodePost("saveDataArch"),
    'userCodePreRestoreDataArch': userCode.preSection('%s::restoreDataArch' % module_name),
    'userCodePostRestoreDataArch': userCode.postSection('%s::restoreDataArch' % module_name),
    'customCodePreRestoreDataArch': config.getCustomCodePre("restoreDataArch"),
    'customCodePostRestoreDataArch': config.getCustomCodePost("restoreDataArch")
    }
  else:
    code = """\
bool %(mod_name)s::saveData( eslapi::CASIODataStream &stream )
{
  bool ok = true;
%(userCodePreSaveData)s
%(customCodePreSaveData)s
  // Save the Carbon Model onto the SoC Designer stream.
  CarbonStatus stat = carbonStreamSave(mCarbonObj, streamWriteFunc, &stream, "maxsim stream" );
  ok = (stat == eCarbon_OK);
%(saveComponentData)s
%(userCodePostSaveData)s
%(customCodePostSaveData)s
  return ok;
}

bool %(mod_name)s::restoreData( eslapi::CASIIDataStream &stream )
{
  bool ok = true;
%(userCodePreRestoreData)s
%(customCodePreRestoreData)s
  CarbonStatus stat = carbonStreamRestore(mCarbonObj, streamReadFunc, &stream, "maxsim stream" );
  ok = (stat == eCarbon_OK);
%(restoreComponentData)s
%(userCodePostRestoreData)s
%(customCodePostRestoreData)s
  return ok;
}
""" % {
    'mod_name': module_name,
    'saveComponentData': generateSaveComponentDataBinary(config),
    'restoreComponentData': generateRestoreComponentDataBinary(config),
    'userCodePreSaveData': userCode.preSection('%s::saveData' % module_name),
    'userCodePostSaveData': userCode.postSection('%s::saveData' % module_name),
    'customCodePreSaveData': config.getCustomCodePre("saveData"),
    'customCodePostSaveData': config.getCustomCodePost("saveData"),
    'userCodePreRestoreData': userCode.preSection('%s::restoreData' % module_name),
    'userCodePostRestoreData': userCode.postSection('%s::restoreData' % module_name),
    'customCodePreRestoreData': config.getCustomCodePre("restoreData"),
    'customCodePostRestoreData': config.getCustomCodePost("restoreData")
    }
  return code

# If there are new flow thru transactors and a flow thru model, we must be last
def must_be_last_property(config):
  # there is an undocumented hack to force the MaxSim scheduler to
  # put call us last.  We need to use it if we are driving any
  # adapter ports from slave clocks, so that we know if the clock
  # has been updated before the component update comes through.
  #
  # We also have to be called last if there are flow through
  # transactors and the Carbon model has flow thru.
  code = ''
  if config.numCyclePorts() > 0:
    code += '''\
    case eslapi::CASI_PROP_RESERVED_6: // force last communicate() call
      return "true";
'''
  return code

def debugger_property(config):
  # For ARM Processor model set the debugger property
  code = ''
  if (config.procInfo().isProcessor()):
    code += '''\
  case eslapi::CASI_PROP_CADI_DEBUGGER:
    return "%(debugger)s";
  case eslapi::CASI_PROP_EXECUTES_SOFTWARE:
    return "yes";
''' % {'debugger': config.procInfo().debuggerName()}
  return code

def debuggable_point_property(config):
  code = ''
  if config.procInfo().debuggablePoint():
    code += '''\
  case 1234: // debuggable point feature
     return "yes";
'''
  return code

def generate_cycle_maxsimdrive_def(config, userCode):
  code = ''
  for port in config.compOutputPorts():
    code += port.driveSignal()
  return code


def generate_mod_destructor_def(config):
  code = ""
  for compPort in config.compPorts():
    code += compPort.componentDestructor()
  return code;


def generate_sigvar_init_def(config):
  code = ""
  for compPort in config.compPorts():
    code += compPort.componentConstructor()
  return code

def generate_slaveclass_imp_def(config, module_name, userCode):
    code = ""
    for compPort in config.compPorts():
      code += compPort.classImplementation(userCode)
    return code

def generate_override_def(config):
  code = ""
  count = 0
  for compPort in config.compPorts():
    if hasattr(compPort, 'debugAccessImplemented'):
      if compPort.debugAccessImplemented():
        if (count == 0):
          code += "#if "
        else:
          code += " || \\\n    "
        code += "defined(CARBON_MEMORY_RESTORE_PORT_"
        code += compPort.name()
        code += ")"
        count += 1
  if (count > 0):
    code += "\n"
    code += "#define CARBONSAVERESTOREARCHCALLBACK_RESTOREMEMORY_OVERRIDE 1\n"
    code += "#endif\n"
  return code

def cadi_include_files(config):
  # Get a list of include files
  files = [cadi.includeFile() for cadi in config.loopCadi()]

  if files:
    code = '\n// CADI Include Files\n'
    for include in files:
      code += '#include "%(include)s"\n' % { "include" : include, }
    return code
  return ""

# these are #included by the generated .h file
def xtor_adaptor_includes(config):
  code = ''
  for portFactory in config.xtorPortFactories():
    code += portFactory.includes()
  for xtor in config.transactors():
    if (xtor.type().startswith('CarbonX') and (xtor.type().find('Pcie') == -1)):
      code += '#include "xactors/socvsp/CarbonXT2T.h"\n'
      break
  return code
    
def user_include_files(config):
  code = ""
  for include in config.includeFiles():
    code += '#include "%(include)s"\n' % { "include" : include, }
  if using_event_queues(config):
    code += '#include "xactors/arm/include/CarbonXtorEvent.h"\n'
  return code

def user_source_files(config):
  code = ""
  for source in config.sourceFiles():
    code += '%(source)s ' % { "source" : source, }
  return code

def user_object_files(sources):
  code = ""
  for source in sources:
    file_name = os.path.basename(source)
    code += file_name[0:file_name.rfind('.')] + '.obj '
  return code

def lib_list_debug(config):
  # Link with debug libraries for SoCDesigner versions less than 7.2
  code = 'LIB_LIST_DBG   = $(LIB_LIST)'
  if int(config.mxVer()) < 7 or (int(config.mxVer()) == 7 and int(config.mxMinorVer()) < 2):
    code = 'LIB_LIST_DBG   = $(LIB_LIST:Release=Debug)'
  return code

def generate_mxpi_friend_def(config):
    code =  ""
    stream_count = config.numProfileStreams()
    if stream_count > 0:
       code += """friend class %(CompName)s_MxPI;""" % { "CompName": config.compName() }
    return code

def generate_sub_component_class_def(config, userCode):
    "Iterate and generate sub-component class definitions"
    code = ""
    if (config.hasSubComps()):
      code += """
///////////////////////////////////////////////////////////////////////////////
// sub-component class definitions
///////////////////////////////////////////////////////////////////////////////
"""
    for comp in config.subComps():
      code += comp.emitDefinition(userCode)

    return code

def generate_sub_component_class_def(config, userCode):
    "Iterate and generate sub-component class definitions"
    code = ""
    if (config.hasSubComps()):
      code += """
///////////////////////////////////////////////////////////////////////////////
// sub-component class definitions
///////////////////////////////////////////////////////////////////////////////
"""
    for comp in config.subComps():
      code += comp.emitDefinition(userCode)

    return code

def generate_subcomp_instance_def(config):
    """sub-component instantiation in top-component's header file"""
    code = ""
    if config.hasSubComps():
      code += "// sub-components\n"
      for comp in config.subComps():
        code += "  %(class)s* %(inst)s;\n" % { "class": comp.className(), "inst": comp.instanceName()}

    return code

def generate_subcomp_init_src_def(config):
    """sub-component init code called in top-component's init()"""
    code = ""
    for comp in config.subComps():
      code += "  %(inst)s->init(mCarbonObj);\n" % {	"inst" : comp.instanceName(), }

    return code

def generate_subcomp_update_src_def(config):
    """sub-component update code called in top-component's update()"""
    code = ""
    for comp in config.subComps():
      code += "  %(inst)s->subUpdate(1);\n" % {	"inst" : comp.instanceName(), }

    return code

def generate_subcomp_pre_update_src_def(config):
    """sub-component pre_update code called in top-component's preUpdate()"""
    code = ""
    for comp in config.subComps():
      code += "  %(inst)s->subUpdate(0);\n" % {	"inst" : comp.instanceName(), }

    return code

def generate_subcomp_instance_src(config):
    """sub-component instantiation in top-component's header file"""
    code = ""
    for comp in config.subComps():
        code += """  %(inst)s = new %(class)s(this, "%(display_name)s");
  addSubcomponent(%(inst)s);
""" % { "name": comp.compName(), "class" : comp.className(), "inst": comp.instanceName(),
        "display_name": comp.displayName() }
    return code

def generate_mxdi_class_def(config, userCode):
    "Iterate sub components and generate _MxDI class definitions"
    code = ""
    code += """
///////////////////////////////////////////////////////////////////////////////
// Top level debug class declaration
///////////////////////////////////////////////////////////////////////////////
"""
    code += config.mxdi().generate_mxdi_class_def(userCode)

    if config.hasSubComps():
      code += """
///////////////////////////////////////////////////////////////////////////////
// Sub Component debug class definitions
///////////////////////////////////////////////////////////////////////////////
"""
    for comp in config.subComps():
      code += comp.mxdi().generate_mxdi_class_def(userCode)

    return code

def generate_mxdi_class_src_def(config, userCode):
    "Iterate sub components and generate _MxDI class implementation source code"
    code = ""
    if config.hasSubComps():
      code += """
///////////////////////////////////////////////////////////////////////////////
// Sub Component debug class implementation
///////////////////////////////////////////////////////////////////////////////
"""
    for comp in config.subComps():
      code += comp.mxdi().generate_mxdi_class_src_def(userCode)

    code += """
///////////////////////////////////////////////////////////////////////////////
// Top level debug class declaration
///////////////////////////////////////////////////////////////////////////////
"""
    code += config.mxdi().generate_mxdi_class_src_def(userCode)
    code += "\n"

    return code

def generate_param_defines_def(config):
  code = ''
  for param in config.parameters():
    code += '  '
    code += param.defineParameter()
    code += '\n'
  return code

def generate_set_param_def(config):
  code = ""
  for param in config.parameters():
    code += '  '
    code += 'if (name == "%s") {\n' % param.name()
    code += param.setParameter()
    code += '  }\n'
  return code

def generate_param_var_def(config):
  code = ""
  for param in config.parameters():
    code += param.declareVariable()
  return code

def generate_clock_and_reset_data_member_decl(config):
  code = """\
  // If a transactor that can manipulate clock speeds are driven by a clock type
  // that does not allow speed change, register this callback instead so that
  // a warning can be generated
  static void warnSpeedChange(void *userdata, CarbonUInt32)
  {
    %(module)s* comp = reinterpret_cast<%(module)s*>(userdata);
    comp->message(eslapi::CASI_MSG_WARNING, "Transactor need to be clocked by a 'Clock_Generator' pseudo transactor to support Speed change.");
  }
""" % { 'module':config.compName() }

  for clock in config.compClockPorts():
    if len(clock.xtorSlaves()) > 0: code += """
  // Callback function for calling communicate for clock %(name)s
  static void %(name)sCommunicateCb(void *userdata)
  {
    %(module)s* comp = reinterpret_cast<%(module)s*>(userdata);
    comp->%(name)s_communicate();
  }

  // Callback function for calling update for clock %(name)s
  static void %(name)sUpdateCb(void *userdata)
  {
    %(module)s* comp = reinterpret_cast<%(module)s*>(userdata);
    comp->%(name)s_update();
  }
""" % {'name': clock.name(), 'module': config.compName() }
  
  return code

def generate_transactor_clock_control_init(config):
  code = ""
  for compPort in config.compPorts():
    code += compPort.componentClockControl()
  return code

def generate_clock_comm_and_updates_decl(config, userCode):
  code = "  // Communicate and Update methods for each clock\n"
  for clock in config.compClockPorts():
    if len(clock.xtorSlaves()) > 0:
      code += "  void %(name)s_communicate();\n  void %(name)s_update();\n" % {'name':clock.name()}

  return code

def generate_clock_comm_and_updates_impl(config, userCode):
  code = ""
  for clock in config.compClockPorts():
    port_list = []
    for xtor in clock.xtorSlaves() :
        for port in config.compPorts():
            if port.name() == xtor.name(): port_list.append(port)
    
    if len(port_list) > 0:
      code += "void %(module)s::%(name)s_communicate()\n{\n" % {'module':config.compName(), 'name':clock.name() }
      code += userCode.preSection('%s::%s_communicate' % (config.compName(), clock.name()), '  ') + '\n'
      if using_event_queues(config):
        code += "  valid_cycle_%(clkId)s = true;\n" % { 'clkId': clock.name() }
        code += "  mCopyDone%(clkId)s = false;\n" % { 'clkId': clock.name() }
        code += generate_process_event_queue(clock.name())
      for port in port_list :
        code += port.componentCommunicate()
      code += userCode.postSection('%s::%s_communicate' % (config.compName(), clock.name()), '  ') + '\n'
      code += "}\n"

      code += "void %(module)s::%(name)s_update()\n{\n" % {'module':config.compName(), 'name':clock.name() }
      code += userCode.preSection('%s::%s_update' % (config.compName(), clock.name()), '  ') + '\n'
      if using_event_queues(config):
        code += """\
  valid_cycle_%(clkId)s = false;
  %(update)s""" % { 'clkId' : clock.name(),
                    'update': generate_nextevent_update(config,clock.name(), guard = True) }
      for port in port_list :
        code += port.componentUpdate()
      code += userCode.postSection('%s::%s_update' % (config.compName(), clock.name()), '  ') + '\n'
      code += "}\n"
      
  return code

def generate_update_clock_event_queues(config):
  code = ""
  for clock in config.compClockPorts():
    port_list = []
    for xtor in clock.xtorSlaves() :
        for port in config.compPorts():
            if port.name() == xtor.name(): port_list.append(port)
    
    if len(port_list) > 0:
      if using_event_queues(config):
        code += """
  if (valid_cycle_%(clkId)s) {
    %(update)s
  }""" % { 'clkId' : clock.name(),
           'update': generate_nextevent_update(config, clock.name(), guard = True) }
  return code

def generate_component_terminate(config):
  code = "\n  delete mSimulationQueue;\n"
  for compPort in config.compPorts():
    code += compPort.componentTerminate()
  return code
  
def generate_simulation_queue_init(config):
  code = """\
  CarbonTimescale timescale = sConvertStringToTimescale(%(waveTimescale)s);
  mSimulationQueue = new CarbonSimulationQueue<%(mod_name)s>(mCarbonObj, this, %(alignWaveform)s, timescale);
""" % {
    'mod_name':config.compName(),
    'alignWaveform':config.alignWavesParameter().variable(),
    'waveTimescale':config.waveTimescaleParameter().variable()
    }

  return code

def generate_init_parameters(config):
  code = '  // Initialize compile time parameters to default value\n'
  for param in config.parameters():
    if param.scope() == 'compile-time':
      code += '  setParameter("%s", "%s");\n' % (param.name(), param.value())
  return code

def gen_maxlib_conf(compName, config, soext):
    """ This function generates the text for the maxlib.conf file for Windows or Linux """
    maxlib_conf_template = """\
component "%(displayName)s"
(
	type = "%(type)s";
	version = "%(version)s";
	variant = "";
        release_with_debug_path = "./lib%(compName)s.mx_DBG.%(soExt)s";
	path = "./lib%(compName)s.mx.%(soExt)s";
""" % { 'compName': compName,
        'displayName': config.displayName(),
        'soExt': soext,
        'type': config.typeName(),
        'version': config.version() }
    # If there is one, add the doc file
    docFile = config.docFile()
    if docFile != "":
      maxlib_conf_template += """\
	documentation_file = "%(docFile)s";
      """ % { 'docFile': docFile }
    maxlib_conf_template += """\
)
"""
    return maxlib_conf_template
  
def generateTimescaleConvFunc():
  code = """
static CarbonTimescale sConvertStringToTimescale(const string& s)
{
  CarbonTimescale ts = e1ns;
  if (s == "1 fs") ts = e1fs;
  else if (s == "10 fs") ts = e10fs;
  else if (s == "100 fs") ts = e100fs;
  else if (s == "1 ps") ts = e1ps;
  else if (s == "10 ps") ts = e10ps;
  else if (s == "100 ps") ts = e100ps;
  else if (s == "1 ns") ts = e1ns;
  else if (s == "10 ns") ts = e10ns;
  else if (s == "100 ns") ts = e100ns;
  else if (s == "1 us") ts = e1us;
  else if (s == "10 us") ts = e10us;
  else if (s == "100 us") ts = e100us;
  else if (s == "1 ms") ts = e1ms;
  else if (s == "10 ms") ts = e10ms;
  else if (s == "100 ms") ts = e100ms;
  else if (s == "1 s") ts = e1s;
  else if (s == "10 s") ts = e10s;
  else if (s == "100 s") ts = e100s;

  return ts;
}
"""
  return code

def comn_tools_var(config):
  if config.mxCompiler() == 'vc8':
    return 'VS80COMNTOOLS'
  elif config.mxCompiler() == 'vc9':
    return 'VS90COMNTOOLS'
  elif config.mxCompiler() == 'vc10':
    return 'VS100COMNTOOLS'
  elif config.mxCompiler() == 'vc11':
    return 'VS110COMNTOOLS'
  else :
    return 'VS90COMNTOOLS'

def visual_studio_version_year(config):
  if config.mxCompiler() == 'vc8':
    return '2005'
  elif config.mxCompiler() == 'vc9':
    return '2008'
  elif config.mxCompiler() == 'vc10':
    return '2010'
  elif config.mxCompiler() == 'vc11':
    return '2012'
  else :
    return '2008'

# Return the gcc switch needed for the PGO step compiled
# into the model, if any.
def pgo_switch(config):
  switch=""
  if config.getPGO() == "generate":
    switch="-fprofile-generate"
  elif config.getPGO() == "use":
    switch="-fprofile-use"
  return switch

def maxwrapgen(config, output):
    """ This function generates an SoC Designer wrapper for the Carbon Model """
    import datetime
    import shutil
    import Carbon.userCode

    output.write('\nGenerating SoC Designer wrapper ...')

    carbon_object_name = config.objectName()
    module_name = config.compName()
    top_component_name = config.compName()
    top_mxdi = config.mxdi()
    hasClockSlave      = config.hasInputClock()
    interconnect_decl = ''
    interconnect_impl = ''
    flowThruCommunicate_decl = ''
    flowThruCommunicate_impl = ''

    
    namespaceCommand = ''
    if config.hasSystemCPorts():
      namespaceCommand = """
      namespace %(mod_name)s_namespace {}; // Make sure xtor namespace is defined
      using namespace %(mod_name)s_namespace;
      """ % { 'mod_name' : module_name }
      
    # Flow Through Output communicates declararations
    for compPort in config.compPorts():
        flowThruCommunicate_decl += compPort.componentFlowThruCommunicateDecl()
  
    for compPort in config.compPorts():
      interconnect_impl += compPort.componentInterconnect()

    # Process Transactor Library dependendencies
    deps = []
    for x in config.compPorts():
      if x.libDepends() != "": deps.extend(x.libDepends())
    unix_makefile_libs = ''
    for dep in set(deps):
      unix_makefile_libs += 'LDFLAGS += -l%s\n' % dep

    deps = []
    for x in config.compPorts(): deps.extend(x.libWinDepends())
    win_makefile_libs = 'XTOR_LIBS     = '
    for dep in set(deps): win_makefile_libs  += '"%s" ' % dep

    deps = []
    for x in config.compPorts(): deps.extend(x.libWinDebugDepends())
    win_makefile_libs += '\nXTOR_LIBS_DBG = '
    for dep in set(deps): win_makefile_libs  += '"%s" ' % dep

    # Process Transactor inc and lib paths
    deps = []
    for x in config.compPorts(): deps.extend(x.libPaths())
    unix_makefile_libpaths = ''
    win_makefile_libpaths = ''
    for dep in set(deps):
      if dep != "":
        unix_makefile_libpaths += '-L%s ' % dep
        win_makefile_libpaths += '/LIBPATH:"%s" ' % dep
    unix_makefile_libpaths += '\n'
    win_makefile_libpaths += '\n'

    deps = []
    for x in config.compPorts():
      for p in x.incPaths():
        deps.append(p)
    unix_makefile_incpaths = ''
    win_makefile_incpaths = ''
    for dep in set(deps):
      if dep != "":
        unix_makefile_incpaths += '-I%s ' % dep
        win_makefile_incpaths += '/I "%s" ' % dep
    unix_makefile_incpaths += '\n'
    win_makefile_incpaths += '\n'

    waveUserData = """
  CarbonWaveUserDataID *mCarbonWaveCycleUserData;
  SInt32 mCarbonWaveCycleNumber;"""

    # If there are SystemC ports in the component, we need to be based of sc_mx_import_module
    includeScMxImport    = ''
    baseClass          = 'eslapi::CASIModule'
    modNameType        = 'string'
    extraBaseClassArgs = ''
    namespaceOpen      = ''
    namespaceClose     = ''
    namespaceName      = ''
    if config.hasSystemCPorts():
      baseClass          = 'sc_mx_import_module'
      modNameType        = 'sc_module_name'
      extraBaseClassArgs = ', "%s"' % module_name
      includeScMxImport  = '#include "sc_mx_import_module.h"\n'
      namespaceName      = config.namespaceName()
      namespaceOpen      = 'namespace %s {\n' % namespaceName
      namespaceClose     = '''} // namespace %s
      

    
using namespace %s;
''' % (namespaceName, namespaceName)
      
    config.setNamespaceUsed(namespaceName)
    
    # If we support architectural save and restore, we need to use the new interface
    includeSRArch = ''
    srClass = 'eslapi::CASISaveRestore'
    defineCarbonSRArchSupport = ''
    if config.supportsArchSaveRestore():
      includeSRArch = '#include "eslapi/CASISaveRestoreArch.h"'
      srClass = 'eslapi::CASISaveRestoreArch'
      defineCarbonSRArchSupport = """\
// Use new architectural save/restore interface
#define CARBON_SR_ARCH_SUPPORT
class CarbonSaveRestoreArchCallback;"""

    # this header template will go into the generated .h and  .cpp files.
    header_comment = """\
%%(lineComment)s -*- %%(fileType)s -*-
%%(openBlockComment)s
%%(contBlockComment)s This file contains generated source code for an SoC Designer component
%%(contBlockComment)s that interfaces with a Carbon Model.
%%(contBlockComment)s
%%(contBlockComment)s File: %%(fileName)s
%%(contBlockComment)s Date: %(date)s
%%(contBlockComment)s
%%(contBlockComment)s SoC Designer Component Name: %(compName)s
%%(contBlockComment)s
%%(contBlockComment)s Carbon Model Top Level Module: %(topLevelModule)s
%%(contBlockComment)s Carbon Model I/O Database: %(iodb)s
%%(contBlockComment)s Carbon Model Interface Name: %(target)s
%%(contBlockComment)s
%%(contBlockComment)s Carbon Component Generator version: %(version)s
%%(contBlockComment)s Configuration file: %(ccfgFile)s
%%(contBlockComment)s
%%(contBlockComment)s To generate this file:
%%(contBlockComment)s   (unix)    $(CARBON_HOME)/bin/carbon carmgr %(ccfgFile)s
%%(contBlockComment)s   (windows) %%%%CARBON_HOME%%%%\\bin\\carbon carmgr %(ccfgFile)s
%%(closeBlockComment)s
""" % { 'date': datetime.datetime.now().ctime(),
        'compName': config.compName(),
        'topLevelModule': config.topModuleName(),
        'iodb': config.ioDbFile(),
        'target': config.objectName(),
        'version': output.version(),
        'ccfgFile': config.ccfgFile() }

    # --- generate the .h file --- #
    h_file_name   = "mx." + module_name + ".h"
    include_guard = "_" + h_file_name.replace(".", "_") + "_"

    userCode = Carbon.userCode.UserCode()
    userCode.parseFile(h_file_name, output)

    # Parse CADI files, purposely using same userCode object so that user codes that were written
    # when everything was in one file will be migrated to the CADI file.
    for cadi in config.loopCadi(): userCode.parseFile(cadi.includeFile(), output)
    
    h_totalUserSections = userCode.numSections()
    
    h_template = header_comment % { 'fileName': h_file_name,
                                    'fileType': 'C++',
                                    'lineComment': '//',
                                    'openBlockComment': '/*',
                                    'contBlockComment': ' *',
                                    'closeBlockComment': ' */' }
    h_template += """
#ifndef %(inc_guard)s
#define %(inc_guard)s

%(userCodePreInclude)s
%(customCodePreInclude)s

// the default SoC Designer classes
#include "maxsim.h"
%(includeScMxImport)s#include "eslapi/CASI.h"
#include "eslapi/CADI.h"
#include "eslapi/CADIDisassembler.h"
#include "MxSimAPI.h"
%(includeSRArch)s

// for callbacks at the beginning and end of each cycle
#include "MxCycleStartListener.h"
#include "MxCycleListener.h"
#include "MxSimQuitListener.h"
#include "carbon/CarbonComponentAccess.h"

using namespace std;

// for priority_queue, used to schedule generated clock and reset events.
#include <queue>

// for clock inputs and outputs
#include <list>

// for transactor event queues
#include <set>

// for ostringstream
#include <sstream>

// carbon system
#include "carbon/carbon_system.h"

// the interface to the Carbon Model
#include "lib%(carbon_obj)s.h"

// faster memory management for the priority queue objects
#include "carbon/c_memmanager.h"

// Debug access
#include "carbon/CarbonDebugAccess.h"

#include "carbon/carbon_dbapi.h"

// forward declarations
template<class COMP> class CarbonSimulationQueue;
class CarbonInternalMemMapEntry;
class CarbonClockDefinition;
class CarbonResetGenDef;
class CarbonClockGenDef;
class CarbonClockSlaveDef;
class CarbonDebugRegister;
class CarbonDebugMemory;

namespace eslapi
{
  struct CASIClockedComponentsProperties;
}

%(defineCarbonSRArchSupport)s

%(xtor_adaptor_includes)s

%(user_include_files)s

%(customCodePostInclude)s
%(userCodePostInclude)s

%(userCodePreDefs)s
#define CARBON_TICKS_PER_MAXSIM_CYCLE 100
%(userCodePostDefs)s

%(generate_profile_class_header)s

%(generate_xtor_port_factory_decl)s

%(generate_mxdi_forward_decl)s

//slave port class declarations
class %(mod_name)s;
%(namespaceOpen)s
""" % { "mod_name":              module_name,
        "inc_guard":             include_guard,
        'userCodePreInclude': userCode.preSection('INCLUDE', ''),
        'xtor_adaptor_includes': xtor_adaptor_includes(config),
        'userCodePostInclude': userCode.postSection('INCLUDE', ''),
        'customCodePreInclude': config.getCustomCodePre('h-include'),
        'customCodePostInclude': config.getCustomCodePost('h-include'),
        'userCodePreDefs': userCode.preSection('MACRO DEFINES', ''),
        'userCodePostDefs': userCode.postSection('MACRO DEFINES', ''),
        "carbon_obj":            carbon_object_name,
        "generate_profile_class_header": config.profile().classDeclaration(),
        'generate_xtor_port_factory_decl': generate_xtor_port_factory_decl(config),
        'generate_mxdi_forward_decl': generate_mxdi_forward_decl(config),
        "user_include_files" : user_include_files(config),
        "includeScMxImport" : includeScMxImport,
        "includeSRArch" : includeSRArch,
        "namespaceOpen": namespaceOpen,
        "defineCarbonSRArchSupport": defineCarbonSRArchSupport
      }

    h_template += generate_sub_component_class_def(config, userCode)

    h_template += """
%(friend_port_class_def)s
""" % { "friend_port_class_def": generate_friend_port_class_def(config, module_name, userCode), }
      
    h_template += """%(namespaceClose)s
///////////////////////////////////////////////////////////////////////////////
// This class defines the SoC Designer component.
///////////////////////////////////////////////////////////////////////////////
class %(mod_name)s: public %(baseClass)s, public %(srClass)s, public eslapi::MxSimStatusListener, public CarbonComponentAccess
{
%(userCodePreClassDef)s
%(customCodePreClass)s
  //master/slave ports are friend classes to access Carbon Model
%(friend_port_def)s

%(mxdi_friend_def)s

%(mxpi_friend_def)s

public:
  // instance declarations for ports
%(port_instance_decls)s

  // constructor and destructor
  %(mod_name)s(eslapi::CASIModuleIF* c, const %(modNameType)s &s);
  virtual ~%(mod_name)s();

  // overloaded methods
  void communicate();
  void update();
  
%(combinatorial_func_decl)s
%(debug_transaction_decl)s
%(debug_access_decl)s
  void flushWaveform();
  void simulatorStatusChanged( int running );
  
%(srInterfaceDecl)s

  //overloaded CASIModule methods
  string getName();
  void setParameter(const string &name, const string &value);
  string getParameter(const string &key);
  string getProperty( eslapi::CASIPropertyType property );
  void init();
  void terminate();
  void setClockMaster(eslapi::CASIClockMasterIF *clockFromParent);
  void interconnect();
  void reset(eslapi::CASIResetLevel level, const eslapi::CASIFileMapIF *filelist);

  eslapi::CADI* getCADI();

  // CarbonComponentAccess implementation
  virtual eslapi::CAInterface* ObtainInterface(eslapi::if_name_t ifName,
                                               eslapi::if_rev_t minRev,
                                               eslapi::if_rev_t* actualRev);
  virtual CarbonObjectID* getCarbonObject() const { return mCarbonObj; }
  virtual void addCallback(CarbonComponentAccess::Callback* cb);
                                                                                                  

  // Methods for Flow Through Paths
  void flowThruCommunicate();
%(flowThruMethods)s
%(clockCommUpdates)s
%(event_queue_var_decl)s
  //Port Property initialization
  //void initSignalPort(eslapi::CASISignalMasterIF* signalIf);
  //void initTransactionPort(eslapi::CASITransactionMasterIF* transIf);

  // initialize carbon waveform dumping
  bool carbonWaveInit();

  // for debug memories
%(carbonMemDef)s

  // how many times has communicate been called
  CarbonUInt64 getCycle() {
    return mCycleCount;
  }

  //! Recursive function to find top component from any sub component
  /*! This is the top so just return ourselves
   */
  %(mod_name)s* getTopComp() { return this; }

  // Run Profile Streams
  void runProfile() {
    %(run_prof_streams)s
  }
  
private:
  // Helper class to listen for simulation state changes
  class SystemStatusListener : public eslapi::MxSimStatusListener
  {
  public:
    SystemStatusListener(CarbonSys* system, %(mod_name)s* comp) :
      mSystem(system), mComp(comp) {}
  
    virtual void simulatorStatusChanged(int running)
    {

      // Only read from the GUI when we start running. This is because
      // the replay record/playback/normal options are sticky and if
      // we start immediately, we may start out in the wrong mode. By
      // starting before the user hits the run button (or runs in
      // batch), we give them an opportunity to change modes
      if (running) {
        if (carbonSystemReadFromGUI(mSystem, 1) == eCarbonSystemCmdError) {
          mComp->message(eslapi::CASI_MSG_ERROR, "%%s\\n", carbonSystemGetErrmsg(mSystem));
        }
      }

      // We always update. We could only update when we switch to idle
      // mode, but for now there is no harm in doing it when we start
      // running as well
      carbonSystemUpdateGUI(mSystem);
    }

  private:
    CarbonSys* mSystem;
    %(mod_name)s* mComp;
  };

  // Helper class to listen for simulation quit
  class SystemQuitListener : public eslapi::MxSimQuitListener
  {
  public:
    SystemQuitListener(CarbonSys* system) : mSystem(system) {}

    virtual void simulatorWillQuit()
    {
      carbonSystemShutdown(mSystem);
    }

  private:
    CarbonSys* mSystem;
  };

  // Helper function to update the cycle count in the Carbon control
  // GUI
  static CarbonUInt64 cycleCountCB(void* data)
  {
    return eslapi::MxSimAPI::getMxSimAPI()->getCycleCount();
  }
%(debuggablePointFunctions)s
  bool initComplete; 
  bool p_enableDbgMsg;
  bool mDisableFlowThru;
  // The number of times the component has been reset (excluding the initial one)
  CarbonSInt32 mNumResets;
  
  //carbon variables
  CarbonObjectID *mCarbonObj;
  std::string     mCarbonWaveFilename;
  CarbonWaveID   *mCarbonWave;%(waveUserData)s
  CarbonTime      mCarbonSimulationTime;
  CarbonUInt64    mCycleCount;
  eslapi::MxSimAPI* mMxSimAPI;
  SystemStatusListener* mSystemStatusListener;
  SystemQuitListener* mSystemQuitListener;

  // CarbonComponentAccess callbacks
  typedef std::vector<CarbonComponentAccess::Callback*> ComponentAccessCallbackVec;
  ComponentAccessCallbackVec mComponentAccessCBs;

  // Do we need to reset the simulation queue time scaling?  This
  // needs to be done on the first communicate call of the simulation.
  bool mResetSimulationQueueTimeScaling;

public:
%(port_net_ids)s 
%(generate_memory_inst_def)s
%(generate_callback_members)s


private:
%(param_var_def)s

  %(mxdi_class_name)s* mxdi;
%(srArchMembers)s

public:
  %(subcomp_instance_def)s

  %(mxpi_classes_def)s

  // for debug registers
  CarbonDebugRegister** carbonDebugRegs;

private:
  // clock information
  static const CarbonTime scTicksPerCycle = 100;


  // simulation queue
  CarbonSimulationQueue <%(mod_name)s> *mSimulationQueue;

  // List of connected clock masters
  std::list<eslapi::CASIClockMasterIF*> mClockMasters;

  // Representation of component's address map
  std::map<string, CarbonInternalMemMapEntry*> internalMemMap;
  
  // Reference to SoCDesigner System Clock
  eslapi::CASIClockMasterIF *mSystemClock;
  
  //run Carbon Model with generated clocks up to given simulation time
  void runTicks(CarbonTime stopTime);

  // function to register with transactors that can manipulate clock speeds
  static void setClockGeneratorMultiple(void *userdata, CarbonUInt32 mult);

%(clock_and_reset_data_member_decl)s
%(customCodePostClass)s
%(userCodePostClassDef)s
};
""" % { "mod_name":              module_name,
        "baseClass":             baseClass,
        "srClass":               srClass,
        "modNameType":           modNameType,
        "extraBaseClassArgs":    extraBaseClassArgs,
        "friend_port_def":       generate_friend_port_def(config),
        "friend_port_class_def": generate_friend_port_class_def(config, module_name, userCode),
        "port_instance_decls":   port_instance_decls(config),
        "port_net_ids":  port_net_ids(config),
        "generate_memory_inst_def": top_mxdi.generate_memory_inst_def(),
        "generate_callback_members":  generate_callback_members(config),
        'generate_xtor_port_factory_decl': generate_xtor_port_factory_decl(config),
        "generate_profile_class_header": config.profile().classDeclaration(),
        "param_var_def": generate_param_var_def(config),
        "mxdi_friend_def": top_mxdi.generate_mxdi_friend_def(),
        "clock_and_reset_data_member_decl": generate_clock_and_reset_data_member_decl(config),
        "mxpi_friend_def": generate_mxpi_friend_def(config),
        "mxpi_classes_def": config.profile().instanceDeclaration(),
        "subcomp_instance_def" : generate_subcomp_instance_def(config),
        "run_prof_streams": config.profile().update(),
        'userCodePreClassDef': userCode.preSection(module_name + ' CLASS DEFINITION'),
        'userCodePostClassDef': userCode.postSection(module_name + ' CLASS DEFINITION'),
        'customCodePreClass': config.getCustomCodePre("class"),
        'customCodePostClass': config.getCustomCodePost("class"),
        'flowThruMethods' : flowThruCommunicate_decl,
        'clockCommUpdates': generate_clock_comm_and_updates_decl(config, userCode),
        'event_queue_var_decl': generate_event_queue_var_decl(config),
        'waveUserData': waveUserData,
        'combinatorial_func_decl': conditionally_generate_if_has_drive_notify(config, "void combinatorial(bool, bool, bool, bool);", ""),
        'debug_transaction_decl': generate_debug_transaction_decl(config),
        'debug_access_decl': generate_debug_access_decl(config),
        'srInterfaceDecl': generateSRInterfaceDecl(config),
        'srArchMembers': generateSRArchMembers(config),
        'carbonMemDef': generate_carbonMems_def(config),
        'mxdi_class_name': top_mxdi.mxdiClassName(),
        'debuggablePointFunctions': generate_debuggable_point_functions(config, userCode),
        'namespaceClose': namespaceClose
        }

    # Write CADI header File. Must do this before checking unused
    # sections so that sections used by CADI can be taken into effect
    for cadi in config.loopCadi(): cadi.writeHeaderFile(output, userCode)

    # add unused user code sections separately to make sure all of the
    # used ones are consumed first
    h_unusedSectionNames = userCode.sectionNames()
    h_template += """
%(userCodeUnused)s

#endif  // %(inc_guard)s
""" % { 'inc_guard': include_guard,
        'userCodeUnused': userCode.unusedSections() }

    # Save Header User Code
    h_userCode = userCode
    
    # --- generate the .cpp file --- #
    cpp_file_name   = "mx." + module_name + ".cpp"

    userCode = Carbon.userCode.UserCode()
    userCode.parseFile(cpp_file_name, output)

    # Parse CADI files, purposely using same userCode object so that user codes that are written
    # when everything was in one file will be migrated to the CADI file.
    for cadi in config.loopCadi(): userCode.parseFile(cadi.sourceFile(), output)

    cpp_totalUserSections = userCode.numSections()

    # Create the output flow through communicate methods
    if config.hasFlowThru() :
      flowThruCommunicate_impl += "\n// Flow Through Communicate methods for output ports\n"
      for compPort in config.compPorts():
        flowThruCommunicate_impl += compPort.componentFlowThruCommunicateImpl(userCode)

    # if we are dumping the SoCDesigner cycle number to the waveforms,
    # init the user data, update it, and free it at the end
    waveUserDataInit = ''
    updateWaveCycle = ''
    freeWaveUserData = ''
    waveUserDataInit = """
  mCarbonWaveCycleUserData = 0;
  mCarbonWaveCycleNumber = 0;"""
    updateWaveCycle = """
  if (mCarbonWave && mCarbonWaveCycleUserData) {
    mCarbonWaveCycleNumber = mMxSimAPI->getCycleCount();
    carbonWaveUserDataChange(mCarbonWaveCycleUserData);
  }"""
    freeWaveUserData = """
  if (mCarbonWaveCycleUserData) {
    carbonFreeWaveUserData(&mCarbonWaveCycleUserData);
  }"""
      
    cpp_template = header_comment % { 'fileName': cpp_file_name,
                                      'fileType': 'C++',
                                      'lineComment': '//',
                                      'openBlockComment': '/*',
                                      'contBlockComment': ' *',
                                      'closeBlockComment': ' */' }
    cpp_template += """

%(userCodePreInclude)s
%(customCodePreInclude)s
%(generate_override)s
#define CARBON 1
#include "mx.%(mod_name)s.h"
#include <stdio.h>
#include "assert.h"
#include "carbon/carbon_socd.h"
#include "carbon/CarbonDebugAccess.h"
#include "CASIDelegate.h"
#include "CASIClockScheduler.h"

%(cadiIncludes)s
%(pcTraceIncludes)s
%(customCodePostInclude)s
%(userCodePostInclude)s

CarbonUInt32 ONE  = 1;
CarbonUInt32 ZERO = 0;

%(generate_profile_class_src)s
""" % { "mod_name": module_name,
        'userCodePreInclude': userCode.preSection('INCLUDE', ''),
        'userCodePostInclude': userCode.postSection('INCLUDE', ''),
        'customCodePreInclude': config.getCustomCodePre('cpp-include'),
        'customCodePostInclude': config.getCustomCodePost('cpp-include'),
        'cadiIncludes': cadi_include_files(config),
        'pcTraceIncludes': generateSoCDPCTraceReporterInclude(config),
        'generate_override': generate_override_def(config),
        "generate_profile_class_src": config.profile().classImplementation(userCode), }

    cpp_template += """%(generate_component_common_src)s
""" % { "generate_component_common_src" : generate_component_common_src_def(config), }

    # sub-component class implementations
    for comp in config.subComps():
      cpp_template += comp.emitImplementation(userCode)

    cpp_template += """
// port class implementations
%(generate_slaveclass_imp)s

static eCarbonMsgCBStatus carbon_msg(void *data, CarbonMsgSeverity sev, int, const char *text, unsigned int)
{
  if (sev != eCarbonMsgSuppress) {
    // Translate the message type
    eslapi::CASIMessageType type = eslapi::CASI_MSG_INFO;
    switch(sev) {
      case eCarbonMsgStatus:
      case eCarbonMsgNote:
        type = eslapi::CASI_MSG_INFO;
        break;
        
      case eCarbonMsgWarning:
        type = eslapi::CASI_MSG_WARNING;
        break;
        
      case eCarbonMsgAlert:
      case eCarbonMsgError:
        type = eslapi::CASI_MSG_ERROR;
        break;
        
      case eCarbonMsgFatal:
        type = eslapi::CASI_MSG_FATAL_ERROR;
        break;

      case eCarbonMsgSuppress:
        // Should not occur, but we handle it anyway
        type = eslapi::CASI_MSG_INFO;
        break;
    }

    // Print the message to the right place
    if (data != NULL) {
      // Component message
      %(mod_name)s *ptr = reinterpret_cast<%(mod_name)s*>(data);
      ptr->message(type, text);
    } else {
      // System message; use the top component if it exists
      eslapi::MxSimAPI* simAPI = eslapi::MxSimAPI::getMxSimAPI();
      eslapi::CASIModuleIF* top = simAPI->getTopComponent();
      if (top != NULL) {
        eslapi::CASIModule* mod = dynamic_cast<eslapi::CASIModule*>(top);
        if (mod != NULL) {
          mod->message(type, text);
        }
      } else {
        // Use SystemC reporting.
        if (type == eslapi::CASI_MSG_FATAL_ERROR) {
          SC_REPORT_FATAL("Carbon", text);
        } else if (type == eslapi::CASI_MSG_ERROR) {
          SC_REPORT_ERROR("Carbon", text);
        } else if (type == eslapi::CASI_MSG_WARNING) {
          SC_REPORT_WARNING("Carbon", text);
        } else {
          SC_REPORT_INFO("Carbon", text);
        }
      }
    }
  }
  return eCarbonMsgStop;
}

%(generateTimescaleConvFunc)s

%(generate_xtor_port_factories)s

void %(mod_name)s::runTicks(CarbonTime stopTick)
{
  mSimulationQueue->execute(stopTick);
  mCarbonSimulationTime = stopTick;
}

// function to register with transactors that can manipulate clock speeds
void %(mod_name)s::setClockGeneratorMultiple(void *userdata, CarbonUInt32 mult)
{
  CarbonClockDefinition *clock = reinterpret_cast<CarbonClockDefinition*>(userdata);
  clock->scale(mult);
}

%(generate_combinatorial_callback_fn)s
%(namespaceCommand)s
%(mod_name)s::%(mod_name)s(eslapi::CASIModuleIF *c, const %(modNameType)s &s) : %(baseClass)s(c, s%(extraBaseClassArgs)s)
{
%(userCodePreConstructor)s
%(customCodePreConstructor)s

  mDisableFlowThru = true;      // Enabled after init
  initComplete = false;

  mCarbonObj = 0;
  mCarbonWave = 0;%(waveUserDataInit)s
  mMxSimAPI = NULL;
  mSimulationQueue = NULL;
  mSystemClock = NULL;
%(srArchMemberInit)s
  
%(mxpi_constructor_init_src)s

  casi_clocked();
  registerPort(dynamic_cast<eslapi::CASIClockSlaveIF *>(this), "clk-in");
%(generate_sigvar_init)s  
  //define parameters
%(generate_param_defines)s
  //initialize for save/restore
  initStream( this );

  // instantiate subcomponents if any
%(generate_subcomp_instance_src)s

%(customCodePostConstructor)s
%(userCodePostConstructor)s
}

%(mod_name)s::~%(mod_name)s()
{
%(userCodePreDestructor)s
%(customCodePreDestructor)s
%(srArchMemberDestroy)s
%(generate_mod_destructor)s
%(customCodePostDestructor)s
%(userCodePostDestructor)s
}

void %(mod_name)s::flushWaveform()
{
  if (mCarbonWave) {
    message(eslapi::CASI_MSG_INFO, "Flushing waveforms to %%s", mCarbonWaveFilename.c_str());
    carbonDumpFlush(mCarbonWave);
    message(eslapi::CASI_MSG_INFO, "Waveform flush complete.");
  }
}

void %(mod_name)s::simulatorStatusChanged( int running )
{
  if (running == 0)
    flushWaveform();
}

void %(mod_name)s::communicate()
{
  if (!mComponentAccessCBs.empty()) {
    for (ComponentAccessCallbackVec::iterator iter = mComponentAccessCBs.begin(); iter != mComponentAccessCBs.end(); ++iter) {
      CarbonComponentAccess::Callback* cb = *iter;
      cb->communicate(CarbonComponentAccess::Callback::Begin, mMxSimAPI->getCycleCount());
    }
  }
%(userCodePreCommunicate)s
%(customCodePreCommunicate)s

  if (mResetSimulationQueueTimeScaling) {
    mSimulationQueue->resetTimesForScaling();
    mResetSimulationQueueTimeScaling = false;
  }

  //drive SoC Designer outputs
%(generate_cycle_maxsimdrive)s
%(generate_component_communicate)s
%(process_communicate_events)s
%(updateWaveCycle)s
%(customCodePostCommunicate)s
%(userCodePostCommunicate)s
  if (!mComponentAccessCBs.empty()) {
    for (ComponentAccessCallbackVec::iterator iter = mComponentAccessCBs.begin(); iter != mComponentAccessCBs.end(); ++iter) {
      CarbonComponentAccess::Callback* cb = *iter;
      cb->communicate(CarbonComponentAccess::Callback::End, mMxSimAPI->getCycleCount());
    }
  }
}

void %(mod_name)s::update()
{
  if (!mComponentAccessCBs.empty()) {
    for (ComponentAccessCallbackVec::iterator iter = mComponentAccessCBs.begin(); iter != mComponentAccessCBs.end(); ++iter) {
      CarbonComponentAccess::Callback* cb = *iter;
      cb->update(CarbonComponentAccess::Callback::Begin, mMxSimAPI->getCycleCount());
    }
  }
  mDisableFlowThru = true; // Only flow through during communicate
%(userCodePreUpdate)s
%(customCodePreUpdate)s
  ++mCycleCount;
  
%(update_event_queue)s
%(update_clock_event_queues)s
  //deposit to the Carbon Model
%(generate_subcomp_pre_update_src)s
%(generate_cycle_carbondeposit)s
%(generate_runticks)s
%(generate_subcomp_update_src)s
%(customCodePostUpdate)s
%(userCodePostUpdate)s
  if (!mComponentAccessCBs.empty()) {
    for (ComponentAccessCallbackVec::iterator iter = mComponentAccessCBs.begin(); iter != mComponentAccessCBs.end(); ++iter) {
      CarbonComponentAccess::Callback* cb = *iter;
      cb->update(CarbonComponentAccess::Callback::End, mMxSimAPI->getCycleCount());
    }
  }
  mDisableFlowThru = false; // Out of update, turn it back on
}

%(combinatorial_func_def)s

%(generate_debug_transaction_func)s
%(generate_debug_access_func)s
%(generate_clock_comm_and_updates)s

void %(mod_name)s::init()
{
  // check socd runtime version
  int major, minor, rev;
  std::string str;
  eslapi::getVersion(&major, &minor, &rev, &str);
  bool test_fail = false;
  test_fail =  (major < %(socd_major)s); // cds internal test - ignore this line
  test_fail |= (major == %(socd_major)s && minor < %(socd_minor)s); // cds internal test - ignore this line
  test_fail |= (major == %(socd_major)s && minor == %(socd_minor)s && rev < %(socd_rev)s); // cds internal test - ignore this line 
  if (test_fail)
  {
    message(eslapi::CASI_MSG_WARNING, "Model %(mod_name)s was created using SoCD version %(socd_major)s.%(socd_minor)s.%(socd_rev)s that is later that one presently in use - model may not work correctly"); // cds internal test - ignore this line
  }

%(userCodePreInit)s
%(customCodePreInit)s

%(generate_init_parameters)s  

  // getMxSimAPI() always returns a valid pointer.  However, if
  // MxSimAPI::getTopComponent() returns NULL, there is no SoCD
  // context, so we can't use it for getting the simulation time.
  mMxSimAPI = eslapi::MxSimAPI::getMxSimAPI();
  if (mMxSimAPI->getTopComponent() == NULL) {
    mMxSimAPI = NULL;
  }
  mNumResets = -1;
  // Get the system, this disables any replay messages we don't want to
  // see.  This relies on a SoCD simulation context, so skip if there isn't one.
  CarbonSys* system = NULL;
  int numComponents = 0;
  if (mMxSimAPI != NULL) {  
    system = carbonGetSystem(NULL);
    numComponents = carbonSystemNumComponents(system);
  } else {
    // Carbon system object suppresses messages about the model not
    // supporting Replay/OnDemand.  Replicate that here if we're not
    // using it.
    carbonChangeMsgSeverity(NULL, 5151, eCarbonMsgSuppress);
    carbonChangeMsgSeverity(NULL, 5181, eCarbonMsgSuppress);
  }

  // Add the system message callback. This needs to be done early
  // so that we capture any messages during model creation.
  if (numComponents == 0) {
    carbonAddMsgCB(NULL, carbon_msg, NULL);
  }

  //create the Carbon Model
  if (!%(carbonFilePath)s.empty()) {
    carbonSetFilePath(%(carbonFilePath)s.c_str());
  }
  mCarbonObj = carbon_%(carbon_obj)s_create(eCarbonAutoDB, static_cast<CarbonInitFlags>(eCarbon_Replay | eCarbon_OnDemand));
  mCarbonWave = 0;
  carbonDebugRegs = 0;
  carbonMems = 0;
%(generate_subcomp_init_src)s

  // Set the callbacks to NULL so we can tell if we have to delete them
  // or not upon destruction
  mSystemQuitListener = NULL;
  mSystemStatusListener = NULL;
  if (system != NULL) {
    // Gather the information to create a system. We need a system
    // name and instance name for this component. We get this from
    // walking the component hierarchy
    std::vector<eslapi::CASIModuleIF*> components;
    eslapi::CASIModuleIF* comp = this;
    while (comp != NULL) {
      components.push_back(comp);
      comp = comp->getParent();
    }
    int size = components.size();
    eslapi::CASIModuleIF* sysComp = components[size-2];
    string systemName(sysComp->getInstanceName());
    string compName;
    const char* sep="";
    for (int i = size-3; i >= 0; --i) {
      eslapi::CASIModuleIF* comp = components[i];
      compName.append(sep);
      compName.append(comp->getInstanceName());
      sep = "_";
    }

    // If this is the first Carbon component then, set the system name
    // and the callbacks to update our current state. We only want
    // to do this once.
    if (numComponents == 0) {
      // First component, setup the system name and status and quit callbacks
      carbonSystemPutName(system, systemName.c_str());
      mSystemStatusListener = new SystemStatusListener(system, this);
      mMxSimAPI->registerStatusListener(mSystemStatusListener);
      mSystemQuitListener = new SystemQuitListener(system);
      mMxSimAPI->registerSimQuitListener(mSystemQuitListener);
      carbonSystemPutCycleCountCB(system, cycleCountCB, NULL);
    }

    // Add the component to the system. We check the number first so
    // we can tell if this is the first replayable component
    carbonSystemAddComponent(system, compName.c_str(), &mCarbonObj);
  
    // Update the system after the system name has been set up
    carbonSystemUpdateGUI(system);
  }

%(mxpi_init_src)s

  // Suppress sample-scheduled net warnings
  carbonChangeMsgSeverity(mCarbonObj, 5045, eCarbonMsgSuppress);

  // If waveform alignment is enabled, multiple schedule calls will be
  // made with the same timestamp (e.g. during reset), so suppress
  // warnings about possible race conditions.
  if (%(alignWaveform)s) {
    carbonChangeMsgSeverity(mCarbonObj, 5128, eCarbonMsgSuppress);
  }

  // Add the component message callback
  carbonAddMsgCB(mCarbonObj, carbon_msg, this);
  
  // initialize waveforms
  carbonWaveInit();

  //init carbon sim time
  mCarbonSimulationTime = 0;
  mCycleCount = 0;
  mResetSimulationQueueTimeScaling = true;

  //get the carbon net id for all the nets
%(generate_init_id_src)s

  // Initialize debug classes
%(generate_init_debug_id_src)s
  //setup the callbacks for signals that need to be driven out
%(generate_init_cb_src)s

  // set the value of all tie nets
%(generate_ties)s

  // setup the clock definitions for generated clocks and resets
%(generate_transactor_clock_control_init)s
  // initialize simulation queue for generated clocks and resets
%(generate_simulation_queue_init)s
%(customCodePostInit)s
%(userCodePostInit)s

  // Call the base class after this has been initialized.
  eslapi::CASIModule::init();

  // Init is complete, we can enable flow through
  mDisableFlowThru = false;

  // Set a flag that init stage has been completed
  initComplete = true;    
}

// Drives Flow Through paths through the model 
extern "C" CarbonStatus carbonFlowThruSchedule(CarbonObjectID* context, CarbonTime time);
void %(mod_name)s::flowThruCommunicate()
{
%(userCodePreFlowThruCommunicate)s
  CarbonTime lastScaledTick = mSimulationQueue->getLastScaledTick();
  carbonFlowThruSchedule(mCarbonObj, lastScaledTick);
%(userCodePostFlowThruCommunicate)s
}
%(outputFlowThruCommunicates)s
eslapi::CADI* %(mod_name)s::getCADI()
{
  return mxdi;
}

eslapi::CAInterface* %(mod_name)s::ObtainInterface(eslapi::if_name_t ifName,
    eslapi::if_rev_t minRev,
    eslapi::if_rev_t* actualRev)
{
%(socdPCTraceReporterObtainInterface)s

  // If this is a request for the CarbonComponentAccess interface, return that.
  // Otherwise, defer to the base class.
  if ((strcmp(ifName, CarbonComponentAccess::IFNAME()) == 0) &&
      (minRev <= CarbonComponentAccess::IFREVISION())) {
    *actualRev = CarbonComponentAccess::IFREVISION();
    return static_cast<CarbonComponentAccess*>(this);
  }
  return %(baseClass)s::ObtainInterface(ifName, minRev, actualRev);
}

void %(mod_name)s::addCallback(Callback* cb)
{
  mComponentAccessCBs.push_back(cb);
}

void %(mod_name)s::reset(eslapi::CASIResetLevel level, const eslapi::CASIFileMapIF *filelist)
{  
  // Disable flow through during reset. We don't need to
  // communicate with other components.
  mDisableFlowThru = true;

%(userCodePreReset)s
%(customCodePreReset)s
%(event_queue_reset)s
  ++mNumResets;
  // Reset simulation time and reinitialize wave dumping - Only required if we're aligning waveforms.
  // Also, skip this is the first call to reset(), since init() has just been called.
  if (%(alignWaveform)s && (mNumResets > 0)) {
    mCarbonSimulationTime = 0;
    mCycleCount = 0;
    mResetSimulationQueueTimeScaling = true;
    carbonWaveClose(mCarbonObj);
    mCarbonWave = NULL; // so that no one tries to use it again
    carbonWaveInit();
  }

  // apply the reset sequence to the Carbon Model
%(generate_reset_src)s

%(customCodePostReset)s
%(userCodePostReset)s

  // Call the base class after this has been reset.
  eslapi::CASIModule::reset(level, filelist);  

  // Reset is complete, we can enable flow through
  mDisableFlowThru = false;
}

void %(mod_name)s::terminate()
{
  // Call the base class first.
  eslapi::CASIModule::terminate();

%(userCodePreTerminate)s
%(customCodePreTerminate)s

%(generate_component_terminate)s

%(freeWaveUserData)s
  if (mCarbonObj) {
    carbonDestroy(&mCarbonObj);
  }
  %(generate_terminate_debug_id_src)s
  %(mxpi_terminate)s
  if (mSystemStatusListener != NULL) {
    mMxSimAPI->unregisterStatusListener(mSystemStatusListener);
    delete mSystemStatusListener;
  }
  if (mSystemQuitListener != NULL) {
    mMxSimAPI->unregisterSimQuitListener(mSystemQuitListener);
    delete mSystemQuitListener;
  }
  carbonSystemClearCycleCountCB(carbonGetSystem(NULL));

%(customCodePostTerminate)s
%(userCodePostTerminate)s
}

void %(mod_name)s::setClockMaster(eslapi::CASIClockMasterIF *clockFromParent)
{
  if( getClockMaster() )
  {
    // system clock already registered
    return;
  }
  CASIModule::setClockMaster( clockFromParent );
}

void %(mod_name)s::interconnect()
{
%(interconnect)s
}

void %(mod_name)s::setParameter(const string &name, const string &value)
{
%(userCodePreSetParameter)s
%(customCodePreSetParameter)s
%(debuggablePointSetParameter)s

  CASIConvertErrorCodes status = CASIConvert_SUCCESS;
%(setParameterArchSave)s
  bool callBaseClass = true;
%(generate_set_param)s
  if (callBaseClass) {
    if(status == CASIConvert_SUCCESS) {
      CASIModule::setParameter(name, value);
    } else {
      message( eslapi::CASI_MSG_WARNING, "%(mod_name)s::setParameter: Illegal value passed for parameter. Assignment ignored.");
    }
  }

%(customCodePostSetParameter)s
%(userCodePostSetParameter)s
}

string %(mod_name)s::getParameter(const string &key)
{
%(userCodePreGetParameter)s
%(customCodePreGetParameter)s
%(debuggablePointGetParameter)s
%(getParameterArchSave)s
  
  std::string attributePrefix = "cma-";
  if(key.substr(0, attributePrefix.size()) == attributePrefix)
  {
    string attributeName = key.substr(attributePrefix.size());
    CarbonDB* db = carbonGetDB(mCarbonObj);
    const char* attr = carbonDBGetStringAttribute(db, attributeName.c_str());
    return attr ? (std::string(attr)) : "";

  }
  // Use the base classes getParameter by default
  string result = CASIModule::getParameter(key);

%(customCodePostGetParameter)s
%(userCodePostGetParameter)s
  return result;
}


string %(mod_name)s::getProperty( eslapi::CASIPropertyType property )
{
%(userCodePreGetProperty)s
%(customCodePreGetProperty)s

  string description; 
  switch (property) {    
    case eslapi::CASI_PROP_LOADFILE_EXTENSION:
      return "%(loadfileext)s";
    case eslapi::CASI_PROP_REPORT_FILE_EXT:
      return "yes";
    case eslapi::CASI_PROP_COMPONENT_TYPE:
      return "%(type)s"; 
    case eslapi::CASI_PROP_COMPONENT_VERSION:
      return "%(version)s";
    case eslapi::CASI_PROP_MSG_PREPEND_NAME:
      return "yes"; 
    case eslapi::CASI_PROP_DESCRIPTION:
      description = "carbon based SoC Designer component for %(mod_name)s";
      description += string(" Compiled on ") + __DATE__ + ", " + __TIME__; 
      %(additionalComponentDescription)s
      return description;
    case eslapi::CASI_PROP_CADI_SUPPORT:
      return "%(mxdi_support_status)s";
    case eslapi::CASI_PROP_SAVE_RESTORE:
      return "yes";
%(mustBeLastProperty)s
%(debuggerProperty)s
%(debuggablePointProperty)s
    default:
      return "";
  }
%(customCodePostGetProperty)s  
%(userCodePostGetProperty)s  
  return "";

}

string %(mod_name)s::getName(void)
{
  return "%(display_name)s";
}

%(srInterfaceImpl)s

bool %(mod_name)s::carbonWaveInit()
{
%(initialize_waves)s
}

%(debuggablePointFnImpl)s

//void %(mod_name)s::initSignalPort(CASISignalMasterIF* signalIf) {
//  CASISignalProperties prop_sm1;
//  memset(&prop_sm1,0,sizeof(prop_sm1));
//  prop_sm1.isOptional=false;
//  prop_sm1.bitwidth=32;
//  signalIf->setProperties(&prop_sm1);
//}

//void %(mod_name)s::initTransactionPort(CASITransactionMasterIF* transIf) {
//
//  CASITransactionProperties prop;
//  memset(&prop,0,sizeof(prop));
//  prop.casiVersion = eslapi::CASI_VERSION_1_1;   //the transaction version used for this transaction
//  prop.addressBitwidth=32;             // address bitwidth used for addressing of resources
//  prop.useMultiCycleInterface = false; // using read/write interface methods
//  prop.addressBitwidth = 32;           // address bitwidth used for addressing of resources 
//  prop.mauBitwidth = 8;                // minimal addressable unit 
//  prop.dataBitwidth = 32;              // maximum data bitwidth transferred in a single cycle 
//  prop.isLittleEndian = true;          // alignment of MAUs 
//  prop.isOptional = false;	       // if true this port can be disabled 
//  prop.supportsAddressRegions = true;  // M/S can negotiate address mapping
//  prop.numCtrlFields = 1;              // # of ctrl elements used / entry is used for transfer size
//  prop.protocolID = 0x00000001;        // magic number of the protocol (Vendor/Protocol-ID): 
//				       //The upper 16 bits = protocol implementation version
//				       //The lower 16 bits = actual protocol ID
//  sprintf(prop.protocolName,"Mx");     // The name of the  protocol being used
//
//  // MxSI 6.0: slave port address regions forwarding 
//  prop.isAddrRegionForwarded = false; //true if addr region for this slave port is actually 
//				      //forwarded to a master port, false otherwise
//  prop.forwardAddrRegionToMasterPort = NULL; //master port of the  same component to which this slave port's addr region is forwarded
//  transIf->setProperties(&prop);
//}


""" % { "mod_name":             module_name,
        "baseClass":            baseClass,
        "modNameType":          modNameType,
        "extraBaseClassArgs":   extraBaseClassArgs,
        "display_name":         config.displayName(),
        "type":                 config.typeName(),
        "version":              config.version(),
        "inc_guard":            include_guard,
        'carbonFilePath': config.dbPathParameter().variable(),
        "carbon_obj":           carbon_object_name,
        "generate_reset_src":   generate_reset_source_def(config, top_mxdi),
        "generate_init_id_src": generate_init_id_source_def(config, top_mxdi),
        "generate_init_cb_src": generate_init_cb_source_def(config),
        'outputFlowThruCommunicates': flowThruCommunicate_impl,
        'interconnect': generate_interconnect_source_def(config, userCode, module_name),
        "generate_ties": generate_ties(config),
        "generate_cycle_carbondeposit": generate_cycle_carbondeposit_def(config),
        "generate_runticks": generate_runticks(config),
        'generate_component_communicate': generate_component_communicate(config),
        "generate_cycle_maxsimdrive": generate_cycle_maxsimdrive_def(config, userCode),
        "generate_mod_destructor": generate_mod_destructor_def(config),
        "generate_sigvar_init": generate_sigvar_init_def(config),
        "generate_slaveclass_imp": generate_slaveclass_imp_def(config, module_name, userCode),
        'initialize_waves': initialize_waves(config, userCode),
        'generate_xtor_port_factories': generate_xtor_port_factories(config, userCode),
        "generate_init_debug_id_src": top_mxdi.componentInit(),
        'generateTimescaleConvFunc': generateTimescaleConvFunc(),
        'generate_combinatorial_callback_fn': generate_combinatorial_callback_fn(config, module_name),
        'namespaceCommand' : namespaceCommand,
	"generate_subcomp_init_src" : generate_subcomp_init_src_def(config),
	"generate_subcomp_update_src" : generate_subcomp_update_src_def(config),
	"generate_subcomp_pre_update_src" : generate_subcomp_pre_update_src_def(config),
        "generate_terminate_debug_id_src": top_mxdi.componentTerminate(),
        "generate_param_defines": generate_param_defines_def(config),
        "generate_set_param": generate_set_param_def(config),
        "mxdi_support_status": top_mxdi.mxdi_support_status_def(),
        'generate_transactor_clock_control_init' : generate_transactor_clock_control_init(config),
        'generate_clock_comm_and_updates' : generate_clock_comm_and_updates_impl(config, userCode),
        'generate_component_terminate': generate_component_terminate(config),
        'generate_simulation_queue_init': generate_simulation_queue_init(config),
        'generate_init_parameters': generate_init_parameters(config),
        "mxpi_init_src": config.profile().componentInit(),
        'alignWaveform': config.alignWavesParameter().variable(),
        'srArchMemberInit': generateSRArchMemberInit(config),
        'srArchMemberDestroy': generateSRArchMemberDestroy(config),
        'setParameterArchSave': generateSetParameterArchSave(config),
        'getParameterArchSave': generateGetParameterArchSave(config),
        'srInterfaceImpl': generateSRInterfaceImpl(config, userCode),
        "mxpi_constructor_init_src": config.profile().componentConstructor(),
        'generate_subcomp_instance_src' : generate_subcomp_instance_src(config),
        'userCodePreConstructor': userCode.preSection('%s::%s' % (module_name, module_name)),
        'userCodePostConstructor': userCode.postSection('%s::%s' % (module_name, module_name)),
        'customCodePreConstructor': config.getCustomCodePre("constructor"),
        'customCodePostConstructor': config.getCustomCodePost("constructor"),
        'customCodePreDestructor': config.getCustomCodePre("destructor"),
        'customCodePostDestructor': config.getCustomCodePost("destructor"),
        'userCodePreDestructor': userCode.preSection('%s::~%s' % (module_name, module_name)),
        'userCodePostDestructor': userCode.postSection('%s::~%s' % (module_name, module_name)),
        'userCodePreCycleStart': userCode.preSection('%s::cycleStartCB' % module_name),
        'userCodePostCycleStart': userCode.postSection('%s::cycleStartCB' % module_name),
        'userCodePreCycleFinish': userCode.preSection('%s::cycleFinishedCB' % module_name),
        'userCodePostCycleFinish': userCode.postSection('%s::cycleFinishedCB' % module_name),
        'userCodePreCommunicate': userCode.preSection('%s::communicate' % module_name),
        'userCodePostCommunicate': userCode.postSection('%s::communicate' % module_name),
        'customCodePreCommunicate': config.getCustomCodePre("communicate"),
        'customCodePostCommunicate': config.getCustomCodePost("communicate"),
        'userCodePreUpdate': userCode.preSection('%s::update' % module_name),
        'update_event_queue': generate_nextevent_update(config, "Default", guard = False),
        'update_clock_event_queues': generate_update_clock_event_queues(config),
        'userCodePostUpdate': userCode.postSection('%s::update' % module_name),
        'customCodePreUpdate': config.getCustomCodePre("update"),
        'customCodePostUpdate': config.getCustomCodePost("update"),
        'userCodePreInit': userCode.preSection('%s::init' % module_name),
        'userCodePostInit': userCode.postSection('%s::init' % module_name),
        'customCodePreInit': config.getCustomCodePre("init"),
        'customCodePostInit': config.getCustomCodePost("init"),
        'userCodePreReset': userCode.preSection('%s::reset' % module_name),
        'event_queue_reset': generate_event_queue_reset(config),
        'userCodePostReset': userCode.postSection('%s::reset' % module_name),
        'customCodePreReset': config.getCustomCodePre("reset"),
        'customCodePostReset': config.getCustomCodePost("reset"),
        'userCodePreTerminate': userCode.preSection('%s::terminate' % module_name),
        'userCodePostTerminate': userCode.postSection('%s::terminate' % module_name),
        'customCodePreTerminate': config.getCustomCodePre("terminate"),
        'customCodePostTerminate': config.getCustomCodePost("terminate"),
        "mxpi_terminate": config.profile().componentTerminate(),
        'userCodePreSetParameter': userCode.preSection('%s::setParameter' % module_name),
        'userCodePostSetParameter': userCode.postSection('%s::setParameter' % module_name),
        'customCodePreSetParameter': config.getCustomCodePre("setParameter"),
        'customCodePostSetParameter': config.getCustomCodePost("setParameter"),
        'userCodePreGetParameter': userCode.preSection('%s::getParameter' % module_name),
        'userCodePostGetParameter': userCode.postSection('%s::getParameter' % module_name),
        'customCodePreGetParameter': config.getCustomCodePre("getParameter"),
        'customCodePostGetParameter': config.getCustomCodePost("getParameter"),
        'userCodePreGetProperty': userCode.preSection('%s::getProperty' % module_name),
        'userCodePostGetProperty': userCode.postSection('%s::getProperty' % module_name),
        'customCodePreGetProperty': config.getCustomCodePre("getProperty"),
        'customCodePostGetProperty': config.getCustomCodePost("getProperty"),
        'userCodePreFlowThruCommunicate': userCode.preSection('%s::flowThruCommunicate' % module_name),
        'userCodePostFlowThruCommunicate': userCode.postSection('%s::flowThruCommunicate' % module_name),
        'waveUserDataInit': waveUserDataInit,
        'updateWaveCycle': updateWaveCycle,
        'process_communicate_events': generate_process_communicate_events(config),
        'freeWaveUserData': freeWaveUserData,
        'loadfileext': config.loadfileExtension(),
        'mustBeLastProperty': must_be_last_property(config),
        'combinatorial_func_def': generate_combinatorial_func_def(config, module_name, userCode),
        'debuggerProperty': debugger_property(config),
        'debuggablePointProperty': debuggable_point_property(config),
        'userCodePreCombinatorial': userCode.preSection('%s::combinatorial' % module_name),
        'userCodePostCombinatorial': userCode.postSection('%s::combinatorial' % module_name),
        'generate_debug_transaction_func': generate_debug_transaction_func(config, module_name, userCode),
        'generate_debug_access_func': generate_debug_access_func(config, module_name, userCode),
        'additionalComponentDescription': generateAdditionalComponentDescription(config),
        'debuggablePointFnImpl': generate_debuggable_point_functions_impl(config, userCode),
        'debuggablePointGetParameter': generate_debuggable_point_get_parameter(config),
        'debuggablePointSetParameter': generate_debuggable_point_set_parameter(config),
        'socdPCTraceReporterObtainInterface': generateSoCDPCTraceReporterObtainInterface(config),
        "socd_major": str(config.mxVer()),
        "socd_minor": str(config.mxMinorVer()),
        "socd_rev": str(config.mxRevision()),
        }

    if not config.hasSystemCPorts():
      cpp_template += """class %(mod_name)sFactory : public eslapi::CASIFactory
{
public:
  %(mod_name)sFactory() : eslapi::CASIFactory ( "%(display_name)s" ) {}
  eslapi::CASIModuleIF *createInstance(eslapi::CASIModuleIF *c, const string &id)
  { 
    return new %(mod_name)s(c, id); 
  }
};


extern "C" void 
MxInit(void)
{
  new %(mod_name)sFactory();
}

""" % {
  "mod_name":     module_name,
  "display_name": config.displayName(),  
  }
    else: # SystemC Component
      cpp_template += """
class %(mod_name)sFactory : public eslapi::CASIFactory
{
public:
  %(mod_name)sFactory() : eslapi::CASIFactory ( "%(display_name)s" ) {}
  eslapi::CASIModuleIF *createInstance(eslapi::CASIModuleIF *c, const string &id)
  { 
    eslapi::setDoNotRegisterInSystemC( false );
    eslapi::sc_mx_import_delete_new_module_name();
    return new %(mod_name)s(c, id.c_str()); 
  }
};

extern "C" void 
MxInit(void)
{
  new %(mod_name)sFactory();
}
extern "C" void 
MxInit_SCImport(void) 
{ 
}

""" % {
  "mod_name":     module_name,
  "display_name": config.displayName(),  
  }
      

    # Write CADI CPP File. Must do this before checking unused
    # sections so that sections used by CADI can be taken into effect
    for cadi in config.loopCadi(): cadi.writeCppFile(output, userCode)
      
    # add unused user code sections separately to make sure all of the
    # used ones are consumed first
    cpp_unusedSectionNames = userCode.sectionNames()
    cpp_template += userCode.unusedSections()

    # --- generate the linux make file --- #
    ccfgName = os.path.splitext(os.path.split(config.ccfgFile())[1])[0]
    make_file_name   = 'Makefile.' + ccfgName + '.mx'

    make_template = header_comment % { 'fileName': make_file_name,
                                       'fileType': 'makefile',
                                       'lineComment': '#',
                                       'openBlockComment': '#',
                                       'contBlockComment': '#',
                                       'closeBlockComment': '#' }
    make_template += """\

VHM_DIR = .

ifneq ($(shell test -f $(MAXSIM_HOME)/include/maxsim.h && echo yes),yes)
  $(error The MAXSIM_HOME environment variable, ($(MAXSIM_HOME)) is not set to a SoC Designer installation directory)
endif
ifneq ($(shell test -f $(CARBON_HOME)/include/carbon/carbon.h && echo yes),yes)
  $(error The CARBON_HOME environment variable is not set to a Carbon installation directory)
endif
ifneq ($(shell (test -f $(VHM_DIR)/lib%(carbon_obj)s.a || test -f $(VHM_DIR)/lib%(carbon_obj)s.so) && echo yes),yes)
  $(error This must be run in a directory containing a compiled Unix Carbon Model (lib%(carbon_obj)s.a or lib%(carbon_obj)s.so))
endif

include $(CARBON_HOME)/makefiles/Makefile.common

PACKAGE    = %(mod_name)s

# CARBON_M_FLAG adds -m32 when necessary
BUILDFLAGS     = -O3 -fPIC  $(CARBON_M_FLAG)
BUILDFLAGS_DBG = -O0 -g -fPIC  $(CARBON_M_FLAG)

%(libmk_home)s

# User-specified compile-link flags
USER_CXX_FLAGS = %(cxx_flags)s
USER_LINK_FLAGS = %(link_flags)s

SD_CXX     = $(CARBON_HOME)/bin/carbon_sdcxx
ARM_INCLUDE ?= $(CARBON_HOME)/include/xactors/arm
XACTOR_INCLUDE ?= -I$(ARM_INCLUDE)/AHB/include -I$(CARBON_HOME)/include/xactors/socvsp/pcie

CXXFLAGS     = -I$(VHM_DIR) -I$(MAXSIM_HOME)/include -DSC_INCLUDE_FX -I$(CARBON_HOME)/include -DCARBON $(XACTOR_INCLUDE) %(user_inc_paths)s
CXXFLAGS    += $(USER_CXX_FLAGS)
CXXFLAGS_DBG =  -DCARBON_DEBUG $(CXXFLAGS)
LDFLAGS      = -L$(VHM_DIR) -l%(carbon_obj)s %(user_lib_paths)s

%(xtor_libs)s
LDFLAGS += $(USER_LINK_FLAGS) %(pgoSwitch)s
LDFLAGS += $(CARBON_LIB_LIST)

PACKAGE_SOURCES = mx.%(mod_name)s.cpp %(cadi_sources)s %(user_source_files)s
PACKAGE_OBJECTS = $(patsubst %%.cpp,%%.o,$(PACKAGE_SOURCES))
PACKAGE_OBJECTS_DBG = $(patsubst %%.cpp,%%_DBG.o,$(PACKAGE_SOURCES))

SO     = lib$(PACKAGE).mx.so
SO_DBG = lib$(PACKAGE).mx_DBG.so

all : $(SO) $(SO_DBG)

$(SO) : $(PACKAGE_OBJECTS) %(makeFileName)s
	$(SD_CXX) -o $@ -shared $(PACKAGE_OBJECTS) $(LDFLAGS)
\t@echo $@ > carbon_make_complete
\t@echo \"*******************************************\"
\t@echo \"***\"  Created component $@
\t@echo \"*******************************************\"

$(SO_DBG) : $(PACKAGE_OBJECTS_DBG) %(makeFileName)s
	$(SD_CXX) -o $@ -shared $(PACKAGE_OBJECTS_DBG) $(LDFLAGS)
\t@echo $@ > carbon_make_complete
\t@echo \"*******************************************\"
\t@echo \"***\"  Created component $@
\t@echo \"*******************************************\"

%%.o : %%.cpp
	$(SD_CXX) -c $(BUILDFLAGS) $(CXXFLAGS) -o $@  $<

%%_DBG.o : %%.cpp
	$(SD_CXX) -c $(BUILDFLAGS_DBG) $(CXXFLAGS_DBG) -o $@ $<

.PHONY: clean 

clean: 
	rm -f $(SO)

""" % { "mod_name":   module_name,
        "inc_guard":  include_guard,
        "carbon_obj": carbon_object_name,
        "xtor_libs": unix_makefile_libs,
        "user_inc_paths": unix_makefile_incpaths,
        "user_lib_paths": unix_makefile_libpaths,
        "makeFileName": make_file_name,
        "cxx_flags": config.cxxFlags(),
        "link_flags": config.linkFlags(),
        "cadi_sources": ' '.join([cadi.sourceFile() + ' ' for cadi in config.loopCadi()]),
	"user_source_files" : user_source_files(config),
        "libmk_home" : config.libmkHomeMakefileText("unix"),
        "pgoSwitch" : pgo_switch(config)
      }

    # --- generate the windows nmake file --- #
    win_make_file_name   = 'Makefile.' + ccfgName + '.mx.windows'

    win_make_template = header_comment % { 'fileName': win_make_file_name,
                                           'fileType': 'makefile',
                                           'lineComment': '#',
                                           'openBlockComment': '#',
                                           'contBlockComment': '#',
                                           'closeBlockComment': '#' }
    # Using raw string so Windows backslashes aren't interpreted by Python.
    vhm_dir = "."
    if os.path.dirname(config.ioDbFile()) != '':
        vhm_dir = os.path.dirname(config.ioDbFile())

    win_make_template += r"""
VHM_DIR = %(vhmdir)s

!IF !EXIST("$(MAXSIM_HOME)/include/maxsim.h")
! ERROR The MAXSIM_HOME environment variable, $(MAXSIM_HOME), is not set to an SoC Designer installation directory
!ENDIF
!IF !EXIST("$(CARBON_HOME)/include/carbon/carbon.h")
! ERROR The CARBON_HOME environment variable is not set to a Carbon installation directory
!ENDIF

# Define CARBON_SOCVSP_SHARED_VHM to use a DLL Carbon Model.  Normally we use a static
# Carbon Model (e.g., lib%(carbon_obj)s.lib).
!IFNDEF CARBON_SOCVSP_SHARED_VHM
! IF !EXIST($(VHM_DIR)/lib%(carbon_obj)s.lib)
!  ERROR This must be run in a directory containing a static Windows Carbon Model (lib%(carbon_obj)s.lib)
! ENDIF
VHMLIB = $(VHM_DIR)/lib%(carbon_obj)s.lib
# Flags needed when linking in a static Carbon Model.
STATIC_LINK_FLAGS = /LIBPATH:"$(CARBON_HOME)\Win\winx\lib\gcc\i386-mingw32msvc\3.4.5-a" libgcc.a
!ELSE
! IF !EXIST($(VHM_DIR)/lib%(carbon_obj)s.dll)
!  ERROR This must be run in a directory containing a shared Windows Carbon Model (lib%(carbon_obj)s.dll)
! ENDIF
VHMLIB = $(VHM_DIR)/lib%(carbon_obj)s.vc.lib
STATIC_LINK_FLAGS =
!ENDIF

# If VCINSTALLDIR is defined, we assume PATH contains both necessary VC++
# directories and we'll just use them.  (Perhaps he ran vsvars32.bat
# before invoking us.)  If it's not defined, we'll prefix each command
# with vsvars32.bat.  We find vsvars32.bat using the VS71COMNTOOLS
# environment variable that gets created by Visual Studio's setup.exe,
# so this should work even without the user adding anything to PATH.

!IFNDEF VCINSTALLDIR
CARBON_VSVARSFILE="$(%(comntools)s)vsvars32.bat"
CARBON_VSVARS=$(CARBON_VSVARSFILE) > nl: && 
! IF !EXIST($(CARBON_VSVARSFILE))
!  ERROR Visual Studio .NET %(vsver)s doesn't seem to be installed.  Can't find %%%(comntools)s%%vsvars32.bat
! ENDIF
!ENDIF
PACKAGE    = %(mod_name)s

# We use /O1 and /Oi instead of /O2 because this is equivalent to /O2 without /Ot.
# The use of /Ot caused some test cases to break.
BUILDFLAGS     = /nologo /MD /W3 /GR /GX /O1 /Oi /Zi /vmg
BUILDFLAGS_DBG = /nologo /MD /W3 /GR /GX /Od /Zi /vmg

%(libmk_home)s

# User-specified compile-link flags
USER_CXX_FLAGS = %(cxx_flags)s
USER_LINK_FLAGS = %(link_flags)s

SD_CXX         = cl
ARM_INCLUDE    = $(CARBON_HOME)/include/xactors/arm
XACTOR_INCLUDE = /I "$(ARM_INCLUDE)/AHB/include" /I "$(CARBON_HOME)/include/xactors/socvsp/pcie" %(user_inc_paths)s

CXXFLAGS     = /I $(VHM_DIR) /I "$(MAXSIM_HOME)/include" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "NO_IMPORT_ESLAPI" /D "NO_IMPORT_CADI" /I "$(CARBON_HOME)/include" /D CARBON $(XACTOR_INCLUDE) $(USER_CXX_FLAGS)
CXXFLAGS_DBG =  /D "CARBON_DEBUG" $(CXXFLAGS)
PACKAGE_SOURCES = mx.$(PACKAGE).cpp %(cadi_sources)s %(user_source_files)s
PACKAGE_OBJECTS = mx.%(mod_name)s.obj %(cadi_objects)s %(user_object_files)s

DLL     = lib$(PACKAGE).mx.dll
DLL_DBG = lib$(PACKAGE).mx_DBG.dll

LIB_LIST = libcarbon%(libcarbon_version)s.lib maxsimcore.lib ffwAPIexp.lib lmgr10.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib /LIBPATH:"$(MAXSIM_HOME)\lib\Win32\Release" /LIBPATH:"$(CARBON_HOME)\Win\lib" /LIBPATH:"$(CARBON_HOME)\Win\lib\winx\shared" /LIBPATH:"$(CARBON_HOME)\Win\lib\winx" %(user_lib_paths)s
LINK_FLAGS = /nologo /subsystem:windows /dll /incremental:no /machine:I386 /export:MxInit $(STATIC_LINK_FLAGS)

%(lib_list_debug)s
LINK_FLAGS_DBG = $(LINK_FLAGS) /debug

%(xtor_libs)s

all : $(DLL) $(DLL_DBG)

!IFDEF CARBON_SOCVSP_SHARED_VHM
# Rule to build import library from a def file
lib%(carbon_obj)s.vc.lib : lib%(carbon_obj)s.def
    $(CARBON_VSVARS) lib /machine:i386 /def:lib%(carbon_obj)s.def /out:$@
!ENDIF

$(DLL) : $(PACKAGE_SOURCES) %(makeFileName)s $(VHMLIB)
    $(CARBON_VSVARS) $(SD_CXX) $(BUILDFLAGS) $(CXXFLAGS) /c $(PACKAGE_SOURCES)
    $(CARBON_VSVARS) link $(PACKAGE_OBJECTS) $(VHMLIB) $(XTOR_LIBS) $(LIB_LIST) /out:$@ /implib:$(@:.dll=.lib) $(LINK_FLAGS) $(USER_LINK_FLAGS)
    @echo lib$(PACKAGE).mx.dll > carbon_make_complete
    @echo *******************************************
    @echo ***  Created component lib$(PACKAGE).mx.dll
    @echo *******************************************

$(DLL_DBG) : $(PACKAGE_SOURCES) %(makeFileName)s $(VHMLIB)
    $(CARBON_VSVARS) $(SD_CXX) $(BUILDFLAGS_DBG) $(CXXFLAGS_DBG) /c $(PACKAGE_SOURCES)
    $(CARBON_VSVARS) link $(PACKAGE_OBJECTS) $(VHMLIB) $(XTOR_LIBS_DBG) $(LIB_LIST_DBG) /out:$@ /implib:$(@:.dll=.lib) $(LINK_FLAGS_DBG) $(USER_LINK_FLAGS)
    @echo lib$(PACKAGE).mx.dll > carbon_make_complete
    @echo *******************************************
    @echo ***  Created component lib$(PACKAGE).mx_DBG.dll
    @echo *******************************************

""" % { "mod_name":   module_name,
        "inc_guard":  include_guard,
        "vhmdir": vhm_dir,
        "carbon_obj": carbon_object_name,
        "makeFileName": win_make_file_name,
        "cxx_flags": config.cxxFlags(),
        "link_flags": config.windowsLinkFlags(),
	"user_source_files" : user_source_files(config),
	"user_object_files" : user_object_files(config.sourceFiles()),
        "cadi_sources": ' ' .join([cadi.sourceFile() for cadi in config.loopCadi()]),
	"cadi_objects" : user_object_files([cadi.sourceFile() for cadi in config.loopCadi()]),
        "libcarbon_version" : Carbon.libcarbonVersion(),
        "xtor_libs": win_makefile_libs,
        "user_inc_paths": win_makefile_incpaths,
        "user_lib_paths": win_makefile_libpaths,
        "lib_list_debug": lib_list_debug(config),
        "comntools": comn_tools_var(config),
        "vsver": visual_studio_version_year(config),
        "libmk_home": config.libmkHomeMakefileText("windows")
        }

    maxlib_conf_file_name     = 'maxlib.' + ccfgName + '.conf'
    win_maxlib_conf_file_name = 'maxlib.' + ccfgName + '.windows.conf'

    # -- write out the output files -- #
    if output.numErrors() > 0:
      print 'No component source files generated.'
      print 'Please correct the above errors and run the generator again.'
    else:
      def writeOutputFile(name, description, content, totalUserSections = 0, unusedSectionNames = []):
        unusedUserSections = len(unusedSectionNames)
        verbose = unusedUserSections > 0
        output.write('  %s [%s]' % (description, name), verbose)
        try:
          if os.path.exists(name):
            output.write('    Saving existing file to %s.bak' % name, verbose)
            shutil.copy(name, name + '.bak')
        except Exception,e:
          output.error(e)
          output.write('The new %s will NOT be written to %s.' % (description, name), True)
        else:
          try:
            output.write('    Creating new file %s' % name, verbose)
            f = open(name, 'w')
            f.write(content)
            f.close()
            if totalUserSections > 0:
              output.write('    %3d user code sections were successfully relocated.' % (totalUserSections - unusedUserSections), verbose)
              if unusedUserSections > 0:
                output.write('    %3d sections could not be placed and are saved at the end of the file.' % unusedUserSections, verbose)
                output.write('       The sections that could not be placed are:', verbose)
                for name in unusedSectionNames:
                  output.write('         [' + name + ']', verbose)
            output.write('')
          except Exception,e:
            output.error(e)

      writeOutputFile(h_file_name, 'Component header', h_template, h_totalUserSections, h_unusedSectionNames)
      writeOutputFile(cpp_file_name, 'Component source', cpp_template, cpp_totalUserSections, cpp_unusedSectionNames)
      writeOutputFile(make_file_name, 'Unix Makefile', make_template)
      writeOutputFile(win_make_file_name, 'Windows Makefile', win_make_template)
      writeOutputFile(maxlib_conf_file_name, 'maxlib.conf file', gen_maxlib_conf(module_name, config, "so"))
      writeOutputFile(win_maxlib_conf_file_name, 'Windows maxlib.conf file', gen_maxlib_conf(module_name, config, "dll"))

      totalUnsedUserCodeSections = len(h_unusedSectionNames) + len(cpp_unusedSectionNames)
      if totalUnsedUserCodeSections > 0:
        output.warning('%d user code section(s) could not be successfully relocted in the new component source code; they have been saved at the end of the file(s).' % totalUnsedUserCodeSections)

      # Print warnings about unused custom codes
      config.printUnusedCodes(output)
      for comp in config.subComps():
        comp.printUnusedCodes(output)

      output.write('')
      output.write('Run "make -f %s" to compile the SoC Designer component under linux' % make_file_name)
      output.write('Run "nmake /F %s" to compile the SoC Designer component under windows' % win_make_file_name)

def createXpmFile(compname, output):
  import shutil

  srcfile = os.path.join(os.getenv('CARBON_HOME'), 'lib', 'images', 'carbon.xpm')
  dstfile = compname + '.xpm'
  try:
    # If xpm file already exists, leave it alone
    if not os.path.exists(dstfile): 
      shutil.copy(srcfile, dstfile)
      os.chmod(dstfile, 0755)
  except Exception,e:
    output.error(e)
    output.write('Unable to create %s' % dstfile, True)

class CarmgrOutputStream(Carbon.outputStream.OutputStream):
  def version(self):
    import Carbon
    return Carbon.getReleaseId()

  def printHeader(self):
    self.write("**************************************************")
    self.write("***  SoC Designer Component Generator")
    self.write("***  version: " + self.version())
    self.write("***")
    self.write("***  Carbon Design Systems, Inc.")
    self.write("**************************************************")

  def printUsage(self, name):
    print "Usage: %s [-q] config.ccfg [-xtorDef xtor.xml]" % name
    print "       %s -help" % name
    print
    print "       -q        Run quietly, generating no informational messages."
    print
    print "       -help     Print this message."
    print
    print "       -xtorDef  Tell the generator where to find additional"
    print "                 transactor definitions.  Multiple files may be"
    print "                 specified by using -xtorDef multiple times."
    print
    print " The config.ccfg file is a configuration file that specifies"
    print " all of the component ports, debug registers, memories, etc."
    print " A .ccfg file may be created by running the Carbon Component Wizard."
    print

def run(argv):
  # This is imported here so that it only gets loaded if the script
  # is actually run.
  import Carbon.SoCDesigner.config
  import Carbon.StandAlone.saWrapper
  
  # create an output stream for error reporting
  output = CarmgrOutputStream()

  # parse command line args
  ccfgFile = ""
  modelDir = ""
  xtorDefs = []
  verbose = True
  fileIsXtorDef = False
  flagIsModelDir = False
  standAlone = False
  scUseComponentName = False
  pgo = ""
  if len(argv) < 2:
    output.printUsage(argv[0])
    return 1
  for arg in argv[1:]:
    if arg.startswith('-'):
      if arg == '-q':
        verbose = False
      elif arg == '-modelDir':
        flagIsModelDir = True
      elif arg == '-help':
        output.printUsage(argv[0])
        return 0
      elif arg == "-xtorDef":
        fileIsXtorDef = True
      elif arg == "-standAlone":
        standAlone = True
      elif arg == "-scUseComponentName":
        scUseComponentName = True
      elif arg == "-profileGenerate":
        pgo = "generate"
      elif arg == "-profileUse":
        pgo = "use"
      else:
        output.printUsage(argv[0])
        return 1
    else:
      # arg is a filename.
      if flagIsModelDir:
        flagIsModelDir = False
        modelDir = arg
      elif fileIsXtorDef:
        xtorDefs.append(arg)
        fileIsXtorDef = False
      else:
        if ccfgFile == "":
          ccfgFile = arg
        else:
          output.printUsage(argv[0])
          output.error("Only one config file is allowed.")
          return 1
  if fileIsXtorDef:
    output.printUsage(argv[0])
    output.error("-xtorDef must be followed by a file name")
    return 1
  output.setVerbose(verbose)

  # print version info, greeting, etc.
  output.printHeader()

  try:
    try:
      # load the configuration file
      config = Carbon.SoCDesigner.config.MaxConfig(ccfgFile, xtorDefs, output, modelDir, pgo)

      # generate the component source code
      if config.standAloneComponent() or standAlone:
        config.setStandAloneComponent(True) # If we got here through a commandline option, mark the project as standalone
        Carbon.StandAlone.saWrapper.sawrapgen(config, output, scUseComponentName)

      else: 
        maxwrapgen(config, output)
      

      # copy Carbon .xpm file
      createXpmFile(config.displayName(), output)

    except Exception,e:
      if os.getenv('CARBON_BIN') != None:
        import traceback
        traceback.print_exc(sys.exc_info())
      output.error(e)

  finally:
    output.write('\nGeneration complete with %d warnings, %d errors.' %
                 (output.numWarnings(), output.numErrors()))
  return output.exitStatus()

def name():
  return __name__.split('.')[-1]

def brief():
  return "Generate an SoC Designer component for a Carbon Model."

if __name__ == "__main__":
  run(sys.argv)
