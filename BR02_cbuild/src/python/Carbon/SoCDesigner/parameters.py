#******************************************************************************
# Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************
##
# \file
# Classes for representing SoC Designer parameters that can be edited in
# sdcanvas.

import error

# Parameters can be integer, double or string. they can tie RTL ports
# or drive ESL ports.
#
# In addition, the parameter can be displayed as an edit box or a pull
# down. It is a pull down if enum values are associated with this
# parameter.
#
# Enums supports enumeration of integer, double, and string types.
# The way this works is you use the 'result_type' ctor argument to
# define the value type of the parameter The default value is also the
# initial value specified in the constructor for Parameter The
# enumeration choices are represented as strings (indicated by the
# CASI_PARAM_SYMBOLS type) and you specify these choices by one or
# more calls to addValue(), the order you add them is their display
# order, and all must be added before the call to defineParameter()
class Parameter:
  def __init__(self, name, value, type, scope, signed=False, enumChoices=None):
    self.__name = name
    self.__value = value
    self.__paramType = type
    self.__scope = scope
    self.__signed = signed        ## I dont think this is ever used
    self.__variable = 'p_'
    self.__tieNet = []
    self.__eslPort = []

    # This may or may not be an enum parameter. If this list is empty it is not
    self.__enumValues = []
    if enumChoices:
      for enumChoice in enumChoices:
        self.addEnumValue(enumChoice)
    
    for c in self.name():
      code = ord(c)
      if code >= ord('a') and code <= ord('z'):
        self.__variable += c
      elif code >= ord('A') and code <= ord('Z'):
        self.__variable += c
      elif code >= ord('0') and code <= ord('9'):
        self.__variable += c
      else:
        self.__variable += '_'

    # Mappings from paramType to other types
    self.__mxType = ''
    self.__declType = type
    if type == 'bool':
      self.__mxType = 'eslapi::CASI_PARAM_BOOL'
      
    elif type == 'integer':
      self.__mxType = 'eslapi::CASI_PARAM_VALUE'
      if signed:
        self.__declType = 'int'
      else:
        self.__declType = 'unsigned int'
      
    elif type == 'double':
      self.__mxType = 'eslapi::CASI_PARAM_VALUE'
      
    elif type == 'string':
      self.__mxType = 'eslapi::CASI_PARAM_STRING'

    elif type == 'integer64':
      self.__mxType = 'eslapi::CASI_PARAM_VALUE'
      if signed:
        self.__declType = 'long long'
      else:
        self.__declType = 'unsigned long long'
        
    else:
      raise error.InternalError("Unexpected parameter type '%s'" % type)

  def name(self): return self.__name
  def value(self): return self.__value
  def paramType(self): return self.__paramType

  # return the c++ MX type corresponding to the parameter type
  def type(self):
    return self.__mxType

  def scope(self): return self.__scope
  def editable(self):
    if self.__scope == 'run-time':
      return '1'
    else: return '0'
  def signed(self): return self.__signed
  def isDouble(self): return self.__paramType == 'double'
  def isLongLong(self): return self.__paramType == 'integer64'
  def isString(self): return self.__paramType == 'string'
  def variable(self): return self.__variable
  def tieNet(self): return self.__tieNet
  def eslPort(self): return self.__eslPort
  def setValue(self, value):
    self.__value = value

  def addEnumValue(self, name):
    self.__enumValues.append(name)

  def declareVariable(self):
    code = ''
    param_var = self.variable()
    code += "  " + self.__declType + " " + param_var + ";\n"
    return code
  
  def setParameter(self):
    code = ''
    if self.scope() == 'compile-time':
      code +='    callBaseClass = false;\n'
    if self.paramType() == 'string':
      code +='    status = CASIConvert_SUCCESS;  ' + self.variable() + ' = value;\n'
    else:
      code +='    status = CASIConvertStringToValue(value, &' + self.variable() + ');\n'

    if self.scope() == 'run-time':
      for tie in self.tieNet():
        code += self.setTieParam(tie)
    code += self.driveESLInputs();
    return code

  def setTieParam(self, tie):
    code ="""
    // If Carbon ID is not set yet, we are getting called before init
    if(mCarbonObj) {
      CarbonUInt32 data[%(words)d] = {0};
%(setData)s
      if (carbonIsForcible(mCarbonObj, %(netHandle)s)) {
        carbonForce(mCarbonObj, %(netHandle)s, data);
      } else {
        carbonDepositFast(mCarbonObj, %(netHandle)s, data, 0);
      }
    }
""" % { 'words':        tie.numWords(),
        'variable':     self.variable(),
        'netHandle':    tie.carbonNetId(),
        'setData':      self.setData(tie.numWords())}

    return code

  # Generates code that fills the CarbonUInt32 data array from the
  # parameter value
  def setData(self, numWords):
    # Translation table for code generation
    varDict = { 'numWords' : numWords,
                'variable'  : self.variable() }

    # Generate code based on the parameter type
    code = '';
    if (self.paramType() == 'bool') or (self.paramType() == 'integer'):
      # One word, easy case since numWords is >= 1
      code += '''\
      data[0] = %(variable)s;''' % varDict
      
    elif (self.paramType() == 'double') or (self.paramType() == 'integer64'):
      # Two words, copy the first word first
      code += '''\
      data[0] = (CarbonUInt32)(((UInt64)(%(variable)s)) & 0xFFFFFFFF);''' % varDict

      # Copy the second word if there is space
      if numWords > 1:
        code += '''\
      data[1] = (CarbonUInt32)((((UInt64)(%(variable)s)) >> 32) & 0xFFFFFFFF);''' % varDict

    elif self.paramType() == 'string':
      # For strings we do a copy. Note we reverse the bytes to match Verilog format, not sure
      # what to do for VHDL.
      code += '''\
      UInt32 size = strlen(%(variable)s.c_str()) + 1;
      UInt32 arraySize = (%(numWords)d * sizeof(UInt32));
      if (arraySize < size) {
        size = arraySize;
      }
      UInt8* dest = (UInt8*)&data;
      UInt8* src = (UInt8*)%(variable)s.c_str();
      for (int i = 0; i < size; ++i) {
        dest[(size-1)-i] = src[i];
      }''' % varDict

    return code
      
  def driveESLInputs(self):
    code = ''
    if (self.paramType() != 'string'):
      for port in self.eslPort():
        extVal = 'NULL'
        if int(port.width()) > 32:
          extVal = 'data_' + port.name()
          code += """
    uint32_t %(extval)s[%(size)s-1];
    memset (%(extval)s, 0, sizeof(uint32_t)*(%(size)s-1));""" % {'extval':extVal, 'size':port.numWords()}
        code +="""
    if(mCarbonObj && !%(eslPortInst)s->isConnected()) { // If Carbon ID is not set yet, we are getting called before init
      %(eslPortInst)s->driveSignal(%(variable)s,%(extVal)s);
    }
""" % { 'eslPortInst':port.name() + '_SSlave', 'variable':self.variable(), 'extVal':extVal}
    return code
    
  def defineParameter(self):
    code = ''
    if (self.scope() != 'compile-time'):
      # For enum parameters we create the symbols
      if len(self.__enumValues) > 0:
        # Create all the temp structures in a sub-scope so we don't have to worry about unique names
        # A structure of the string values of all the enums
        code += '{\n'
        code += '  // Definition of Enum type parameter named: %s\n' % (self.name())
        code += '  const char* values[] = {\n'
        code += ',\n'.join(['    "' + x + '"' for x in self.__enumValues])
        code += '  };\n'
        # A "symbols" structure that encapsulates the values
        code += '  eslapi::CASIParamSymbols symbols = {\n'
        code += '    ' + str(len(self.__enumValues)) + ',     // number of values\n'
        code += '    const_cast<char**>(values) // the values\n'
        code += '  };\n'
        # A "range" structure for the corresponding integer values
        code += '  uint32_t start = 0;\n'
        code += '  uint32_t end = ' + str(len(self.__enumValues) - 1) + ';\n'
        code += '  eslapi::CASINumberRange ranges = {\n'
        code += '    1,      // number of ranges\n'
        code += '    &start, // starting index of each range\n'
        code += '    &end    // ending index of each range\n'
        code += '  };\n'
        # A final parameter structure
        code += '  eslapi::CASIParameterProperties properties = {\n'
        code += '    eslapi::CASI_PARAM_SYMBOLS, // this type is used to indicate enumeration to SoCDesigner\n'
        code += '    &ranges,                    // number range\n'
        code += '    ' + self.editable() + ',                          // if true then can be modified at runtime\n'
        code += '    false,                      // private - unused\n'
        code += '    false,                      // read-only - unused\n'
        code += '    &symbols                    // symbols\n'
        code += '  };\n'
        code += '  defineParameter("%s", "%s", &properties);\n' % (self.name(), self.value())
        code += '  }'
      else:        
        code += 'defineParameter("%s", "%s", %s, %s);' % (self.name(), self.value(), self.type(),
                                                          self.editable())
    return code

  def addTieNet(self, tieNet):
    self.__tieNet.append(tieNet)

  def addESLPort(self, ESLPort):
    self.__eslPort.append(ESLPort)    
    
class WaveformDumpParameter(Parameter):
  def __init__(self, name, value, type, scope, signed=False, enumChoices=None):
    Parameter.__init__(self, name, value, type, scope, signed, enumChoices)
  def setParameter(self):
    return """\
  status = CASIConvertStringToValue(value, &%(paramVar)s);
  if (%(paramVar)s && !mCarbonWave) {
    carbonWaveInit();
  }
  else if (mCarbonWave) {
    if (%(paramVar)s) {
      carbonDumpOn(mCarbonWave);
      if (mMxSimAPI) {
        mMxSimAPI->registerStatusListener(this);
      }
      message(eslapi::CASI_MSG_INFO, "Waveform dumping is on.");
    }
    else {
      carbonDumpOff(mCarbonWave);
      if (mMxSimAPI) {
        mMxSimAPI->unregisterStatusListener(this);
      }
      message(eslapi::CASI_MSG_INFO, "Waveform dumping is off.  Until dumping is turned on again, all values will be recorded as X.");
    }
  }
""" % { 'paramVar': self.variable() }


class SubParameter(Parameter):
  def __init__(self, name, value, type, scope, parent, signed=False, enumChoices=None):
    self.__parent = parent
    Parameter.__init__(self, name, value, type, scope, signed, enumChoices)
  def name(self):
    return self.__parent + ' ' + Parameter.name(self)

class ArmAdaptorParameter(SubParameter):
  def __init__(self, name, value, type, scope, portName, portObjectName, signed=False, enumChoices=None):
    SubParameter.__init__(self, name, value, type, scope, portName, signed, enumChoices)
    self.__portObjectName = portObjectName
  def setParameter(self):
    code = Parameter.setParameter(self)
    # call the adaptor instance to set the parameter value
    code += '    %s->setParameter("%s", value);\n' % (self.__portObjectName, Parameter.name(self))
    return code
  
