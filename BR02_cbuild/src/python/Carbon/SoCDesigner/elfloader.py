#******************************************************************************
# Copyright (c) 2008-2013 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

##
# \file
# Classes that represent Implementation of ELF Loader capability

class ELFLoader:
  def __init__(self, elf, mems, compName):
    self.__elf       = elf
    self.__mems      = mems
    self.__compName  = compName
    # If any memory requires more than 32 bits to address, then
    # generate putPacket/interface calls using CarbonUInt64 address,
    # else use CarbonUInt32 addresses.
    # The CarbonObjectLoaderInterface base class (which contains the
    # prototype for putPacket) is located in the modelkit library.
    # This distinction between 32 bit and >32 bit address widths is
    # necessary to support 64 bit addressing in the newer modelkits,
    # but to avoid breaking previously released modelkits that only
    # support 32 bit addressing. See bug 16332
    self.__addrWidth = 32
    for mem in self.__mems:
      if mem.maxAddrs() > 4294967295L:
        self.__addrWidth = 64

  def addrWidth(self):
    return self.__addrWidth
  
  def compName(self):
    return self.__compName

  def numSections(self):
    return len(self.__elf.sections())
  
  def numMemories(self):
    return len(self.__mems)
  
  def emitDefinition(self):
    return self.interfaceClassDef()

  def emitImplementation(self):
    code = ''
    code += self.addrSpaceInfo()
    code += '\n' + self.sectionInfo()
    code += '\n' + self.memoryConfInfo()
    code += '\n' + self.interfaceClassImpl()
    return code
  
  def componentReset(self):
    code = """
  if(level==eslapi::CASI_RESET_HARD)
  {
    if ( filelist->fileExists( getInstanceID() ) )
    {
      std::string inputFileName = filelist->getFile( getInstanceID() );
      mxdi->loadFile( inputFileName.c_str() );
    }
  }"""
    return code
  
  def mxdiConstructor(self):
    return """
  loaderInterface = new %(comp)s_ObjectLoaderInterface( this, &memoryConfInfo);
""" % {'comp': self.compName() }

  def mxdiDestructor(self):
    return """
  delete loaderInterface;"""
  
  def progSpace(self):
    code = '0'
    return code
    
  def addrSpaceInfo(self):
    code = """CarbonObjectLoaderAddressSpaceInfo %(comp)s::addressSpaceInfo[] =
{
""" % {'comp': self.compName() }

    for mem in self.__mems:
      code += '   {%(id)d, 1, 1, %(addrWidth)d, "%(name)s", true, false},\n' % {'id':mem.index(), 'addrWidth':self.addrWidth(), 'name':mem.name()}

    # Remove last comma and and add close bracket
    if code[-2] == ',': code = code[0:-2] + '\n};\n\n'
    
    return code

  def sectionInfo(self):
    code = """CarbonObjectLoaderSectionInfo %(comp)s::sectionInfo[] =
{
""" % {'comp': self.compName() }

    for sec in self.__elf.sections():
      code += '  {"%(name)s", %(space)d, 0, "%(access)s" },\n' % {
        'id':sec.index(), 'name': sec.name(), 'space':sec.space(), 'access':sec.access() }
      
    # Remove last comma and and add close bracket
    if code[-2] == ',': code = code[0:-2] + '\n};\n\n'
    return code

  def memoryConfInfo(self):
    code = """CarbonObjectLoaderMemoryConfInfo %(comp)s::memoryConfInfo =
{
	%(numSpaces)d,
	%(comp)s::addressSpaceInfo,
	%(numSect)d,
	%(comp)s::sectionInfo,
	%(progSpace)s
};
""" % { 'comp': self.compName(),
        'numSpaces': self.numMemories(),
        'numSect': self.numSections(),
        'progSpace': self.progSpace() }
        
    return code

  # Define ObjectLoaderInterface Class
  def interfaceClassDef(self):
    code = """
class %(comp)s_ObjectLoaderInterface: public CarbonObjectLoaderInterface
{
public:
  %(comp)s_ObjectLoaderInterface( %(comp)s *owner, const CarbonObjectLoaderMemoryConfInfo* info) : CarbonObjectLoaderInterface(info), owner( owner ) {}
  void putPacket(CarbonUInt32 memorySpaceId, CarbonUInt%(addrWidth)d address, const CarbonUInt8* packet, CarbonUInt32 mauBytes);
  void message(const std::string& msg)
  {
    // For now, just print to stderr
    std::cerr << msg;
  }
private:
	%(comp)s *owner;
};
""" % {'comp': self.compName(), 'addrWidth':self.addrWidth()}  

    return code

  def interfaceClassImpl(self):
    code = """
void
%(comp)s_ObjectLoaderInterface::putPacket(CarbonUInt32 memorySpaceId, CarbonUInt%(addrWidth)d address, const CarbonUInt8* packet, CarbonUInt32 mauBytes)
{
  owner->putPacket(memorySpaceId, address, packet, mauBytes);
}

""" % {'comp': self.compName(), 'addrWidth':self.addrWidth()}  
    return code
  
  # Add member functions to the MxDI class
  def addMxdiMemberFunctions(self, mxdi):
    # putPacket method
    putPacketPrototype = """(CarbonUInt32 memorySpaceId, CarbonUInt%(addrWidth)d address, const CarbonUInt8* packet, CarbonUInt32 mauBytes)""" % {'addrWidth': self.addrWidth()}
    func = mxdi.addMemberFunction('void', 'putPacket', putPacketPrototype)
    func.addCode("""\
eslapi::CADIAddrComplete_t startAddress(0, eslapi::CADIAddr_t(memorySpaceId, address));
uint32_t actualNumOfUnitsWritten = 0;
CADIMemWrite(startAddress, 1, mauBytes, packet, &actualNumOfUnitsWritten, 0);""")

    # loadFile method
    func = mxdi.addMemberFunction('void', 'loadFile', '(const char* filename)')
    func.addCode('loaderInterface->loadFile(filename);\n')
    
  # Define MxDI member variables
  def addMxdiMemberVariables(self, mxdi):
    mxdi.addMemberVariable('CarbonObjectLoaderInterface*', 'loaderInterface', 'private')
    mxdi.addMemberVariable('CarbonObjectLoaderSectionInfo', 'sectionInfo', 'private', self.numSections(), True)
    mxdi.addMemberVariable('CarbonObjectLoaderAddressSpaceInfo', 'addressSpaceInfo', 'private', self.numMemories(), True)
    mxdi.addMemberVariable('CarbonObjectLoaderMemoryConfInfo', 'memoryConfInfo', 'private', static=True)

