#******************************************************************************
# Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

##
# \file
# Classes that represent SoC Designer debug register interfaces

import error
import operator
from Carbon import cpp
from Carbon import Cfg
from Carbon.SoCDesigner import debugmems
from Carbon.SoCDesigner import elfloader

# Base debug register class
class CarbonDebugRegisterClass(cpp.Class):
  def __init__(self):
    cpp.Class.__init__(self, 'CarbonDebugRegister', [])

    member = self.addMemberVariable('CarbonObjectID*', 'mObj', access='protected')
    member.setDescription('Pointer to Carbon Model object')
    member.setConstructorInit('obj')
    
    func = self.addConstructor('(CarbonObjectID* obj)', inline = True)

    self.addDestructor(inline = True, virtual = True)

    func = self.addMemberFunction('void', 'reset', '()', pureVirtual = True)
    func = self.addMemberFunction('void', 'read', '(CarbonUInt32 *buf)', pureVirtual = True)
    func = self.addMemberFunction('void', 'write', '(CarbonUInt32 *buf)', pureVirtual = True)
    func = self.addMemberFunction('void', 'readOffset', '(CarbonUInt32 *buf, CarbonUInt32 offset, CarbonUInt32 shift, CarbonUInt32 mask)', pureVirtual = True)
    func = self.addMemberFunction('void', 'writeOffset', '(CarbonUInt32 *buf, CarbonUInt32 offset, CarbonUInt32 shift, CarbonUInt32 mask)', pureVirtual = True)

# Some utility functions

def shiftAndMask(val, range, dir):
  # Return an expression that shifts/masks
  # value based on a bit range.
  # The maximum bit range of the mask is 32,
  # but there is no restriction on the shift
  # amount.

  (left, right) = range

  # Convert don't care arguments so we can
  # do some math
  if (right == None):
    right = 0
  if (left == None):
    left = right + 31

  # Generate a mask
  size = left - right + 1
  full_char = size / 4
  part_char = size % 4
  if (part_char == 3):
    mask = '0x7' + operator.repeat('f', full_char)
  elif (part_char == 2):
    mask = '0x3' + operator.repeat('f', full_char)
  elif (part_char == 1):
    mask = '0x1' + operator.repeat('f', full_char)
  else:
    mask = '0x' + operator.repeat('f', full_char)

  if (val == None):
    # Just use the mask as the data
    val = mask
    # Force the mask usage to be ignored
    size = 32

  retstring = val

  if (dir == 'left'):
  # For left shift, mask first
    if (size != 32):
      retstring = '(' + retstring + ' & ' + mask + ')'
    if (right != 0):
      retstring = '(' + retstring + ' << ' + str(right) + ')'
  else:
  # For right shift, mask after shifting
    if (right != 0):
      retstring = '(' + retstring + ' >> ' + str(right) + ')'
    if (size != 32):
      retstring = '(' + retstring + ' & ' + mask + ')'

  return retstring

def normalizeRTLLoc(loc):
  # Calculate the size (in bits) of the register/memory
  # data stored in the buffer, considering endianness and
  # MSB/LSB.  Adjust left/right bits of the location
  # range selector, too.

  bigEndian = (loc.lsb() > loc.msb())
  if bigEndian:
    val_bits = loc.lsb() - loc.msb() + 1
    if loc.hasRange():
      loc_left = loc.lsb() - loc.left()
      loc_right = loc.lsb() - loc.right()
    else:
      loc_left = val_bits - 1
      loc_right = 0
  else:
    val_bits = loc.msb() - loc.lsb() + 1
    if loc.hasRange():
      loc_left = loc.left() - loc.lsb()
      loc_right = loc.right() - loc.lsb()
    else:
      loc_left = val_bits - 1
      loc_right = 0

  return (val_bits, loc_left, loc_right)

def normalizeUserLoc(field):
  # There is a 1-1 mapping for fields. So normalize
  # the user defined field to 0-N
  loc_bits = abs(field.low() - field.high()) + 1
  loc_left = loc_bits - 1
  loc_right = 0
  return (loc_bits, loc_left, loc_right)
  
def getFieldRange(bigEndian, width, field):
  # Convert field ranges to little endian for simplicity
  if bigEndian:
    left = width - 1 - field.low()
    right = width - 1 - field.high()
  else:
    left = field.high()
    right = field.low()

  return (left, right)

# Class to manage the expressions applied during debug
# register reads
class ReadExpr:
  def __init__(self, parent):
    self.__parent = parent

    # Each 32-bit word in the read storage buffer
    # has a list of expressions used to calculate its
    # value.  These expressions contain the location of
    # the actual read data (constant literal or storage
    # buffer), plus the source and destination ranges.
    self.__varFields = []
    num_words = (self.__parent.width() + 31) / 32
    for i in range(num_words):
      self.__varFields.append([])

  # Adds the locations referenced in a field
  def addField(self, field, var = None):

    # Flag this location variable as being used for reads
    if (var != None):
      var['read'] = True

    (field_left, field_right) = getFieldRange(self.__parent.bigEndian(), self.__parent.width(), field)

    # Both abstract registers and RTL locations may be wider
    # than 32 bits, but both are manipulated in 32-bit chunks.
    # We create a unique expression for each 32-bit word of the
    # abstract register, and deal with the physical location
    # splits as they arise.
    field_start_word = field_right / 32
    field_end_word = field_left / 32
    field_curr_word = field_start_word
    while (field_curr_word <= field_end_word):
      # What bits of the field word are we looking at?
      if (field_curr_word == field_start_word):
        field_curr_right = field_right % 32
      else:
        field_curr_right = 0
      if (field_curr_word == field_end_word):
        field_curr_left = field_left % 32
      else:
        field_curr_left = 31

      if (field.loc().type() == 'constant'):
        # Constants are easy, because they are limited to 64 bits
        # and can be expressed as a literal.
        # We just need to calculate what bits of the constant apply
        # to this word of the field.  Since constants themselves
        # start at bit 0, this just the current right bit position
        # within the field minus its original right bit position
        fieldEntry = {'dest': (field_curr_left, field_curr_right), 'value': str(field.loc().castConstant().value()) + 'ULL', 'src': (None, field_curr_right + field_curr_word * 32 - field_right)}
        self.__varFields[field_curr_word].append(fieldEntry)
      else:
        # Registers/arrays can be tricky, because we may need to
        # reference two words in the value storage array.
        # Since we'll never need to access more than two for any
        # 32-bit word of the field, it's easier to write two
        # special cases than to generalize for many words.
        if (field.loc().type() == 'register'):
          loc = field.loc().castRegister()
        elif (field.loc().type() == 'array'):
          loc = field.loc().castArray()
        elif (field.loc().type() == 'user'):
          loc = field.loc().castUser()
        else:
          # Something went wrong
          loc = None

        # Figure out the bits for this field
        if field.loc().type() != 'user':
          # Normalize location indices.
          # This converts the left and right ranges of the RTL
          # location to the actual bit positions in the storage
          # array.
          (loc_bits, loc_left, loc_right) = normalizeRTLLoc(loc)
        else:
          # For user fields, it is a 1-1 mapping
          (loc_bits, loc_left, loc_right) = normalizeUserLoc(field)

        # What bits of the location value are used for this
        # word of the field?
        # This is the requested range of the location, plus our
        # current offset into the field (when the field spans
        # multiple words).
        # Additionally, account for the possibility that the
        # field range is larger than the location range
        val_right = loc_right + (field_curr_right + field_curr_word * 32 - field_right)
        val_left = min((val_right + (field_curr_left - field_curr_right)), loc_left)

        # In severe cases of the field being larger than the
        # location, the entire field word might be out of
        # range, so detect this and move to the next field
        if (val_left < val_right):
          field_curr_word = field_end_word + 1
          continue

        # What words of the value buffer are used?
        val_start_word = val_right / 32
        val_end_word = val_left / 32

        if (val_start_word == val_end_word):
          # Simple - only one word
          # Add a mapping for the range of the current field word
          # to the range of the current value word
          self.__addFieldEntry(val_start_word, var['buf'], field_curr_word, field_curr_left, field_curr_right, val_left % 32, val_right % 32, loc_bits)
        else:
          # More complex - two words of the value buffer
          # Split into two parts, one for each word of the value
          # buffer

          # Word 0 starts at the original right offset into
          # the first location word, and ends at bit 31.  We
          # offset into the field word to match.
          field_left_0 = field_curr_right + (31 - val_right % 32)
          field_right_0 = field_curr_right
          val_left_0 = 31
          val_right_0 = val_right % 32

          # Word 1 starts at bit 0 of the second location word,
          # and ends at the original left offset.  Again, shift
          # into the current field word to match.
          field_left_1 = field_curr_left
          field_right_1 = field_left_0 + 1
          val_left_1 = val_left % 32
          val_right_1 = 0
          
          # Add mappings for this field word to both location words
          self.__addFieldEntry(val_start_word, var['buf'], field_curr_word, field_left_0, field_right_0, val_left_0, val_right_0, loc_bits)
          self.__addFieldEntry(val_end_word, var['buf'], field_curr_word, field_left_1, field_right_1, val_left_1, val_right_1, loc_bits)

      field_curr_word = field_curr_word + 1

  def __addFieldEntry(self, val_word, val_buf, field_word, field_left, field_right, val_left, val_right, loc_bits):
    # Clean up some aspects of this physical value -> field mapping,
    # and save it.

    if ((field_left - field_right) >= (val_left - val_right)):
      # Field is larger than/equal to the value range,
      # so no need to mask the destination
      field_left = None
    if (((val_left + val_word * 32) == loc_bits - 1) or
        ((field_left != None) and ((field_left - field_right) < (val_left - val_right)))):
      # All the valid bits of the value are used,
      # or the field is smaller than the value range,
      # so no need to mask the source
      val_left = None
    # Create and add the entry for this field word, building the
    # location buffer name/index
    fieldEntry = {'dest': (field_left, field_right), 'value': val_buf + '[' + str(val_word) + ']', 'src': (val_left, val_right)}
    self.__varFields[field_word].append(fieldEntry)
    

  def finish(self):
    # Construct the read function

    # Examine all the required nets/memories
    # Sort by variable names first for determinism
    keylist = self.__parent.nets().keys()
    keylist.sort()
    for key in keylist:
      var = self.__parent.nets()[key]
      if var['read']:
        self.__parent.addReadCode('carbonExamine(mObj, ' + var['var'] + ', ' + var['buf'] + ', 0);\n')
    keylist = self.__parent.mems().keys()
    keylist.sort()
    for key in keylist:
      var = self.__parent.mems()[key]
      # Iterate over each index used in this memory
      indexlist = var['indices'].keys()
      indexlist.sort()
      for index in indexlist:
        indexvar = var['indices'][index]
        if indexvar['read']:
          self.__parent.addReadCode('carbonExamineMemory(' + indexvar['var'] + ', ' + str(index) + ', ' + indexvar['buf'] + ');\n')

    # For each word in the read buffer, apply the read
    # data from each constant or RTL buffer
    self.__parent.addReadCode('\n')
    for i in range(len(self.__varFields)):
      self.__parent.addReadCode('buf[' + str(i) + '] = 0')
      for var in self.__varFields[i]:
        srctext = shiftAndMask(var['value'], var['src'], 'right')
        self.__parent.addReadCode(' | ' + shiftAndMask(srctext, var['dest'], 'left'))
      self.__parent.addReadCode(';\n')


# Class to manage the expressions applied during debug
# register writes
class WriteExpr:
  def __init__(self, parent):
    self.__parent = parent

    # Each RTL location has a list entry in this dictionary.
    # with an element for each 32-bit word required for the
    # value of that location.  Each of these elements is a
    # list of expressions used to calculate the value to
    # be deposited to that location.  These expressions contain
    # the source location within the debug register, plus
    # the destination range in the RTL location buffer.
    self.__varFields = {}

  # Adds the locations referenced in a field
  def addField(self, field, var):

    # Flag this location variable as being used for writes
    var['write'] = True

    # Constants fields aren't used in write expressions
    if (field.loc().type() == 'register'):
      loc = field.loc().castRegister()
    elif (field.loc().type() == 'array'):
      loc = field.loc().castArray()
    elif (field.loc().type() == 'user'):
      loc = field.loc().castUser()
    else:
      # Something went wrong
      loc = None

    (field_left, field_right) = getFieldRange(self.__parent.bigEndian(), self.__parent.width(), field)
    if field.loc().type() != 'user':
      # Normalize location indices
      # This converts the left and right ranges of the RTL
      # location to the actual bit positions in the storage
      # array.
      (loc_bits, loc_left, loc_right) = normalizeRTLLoc(loc)
    else:
      # For user fields, it is a 1-1 mapping
      (loc_bits, loc_left, loc_right) = normalizeUserLoc(field)

    # Make sure we have an entry in the dictionary for this variable
    if not self.__varFields.has_key(var['buf']):
      self.__varFields[var['buf']] = []
      # Add an empty list for each word of this buffer
      num_words = (loc_bits + 31) / 32
      for i in range(num_words):
        self.__varFields[var['buf']].append([])

    # For each word in the location buffer, determine the mapping
    # to the abstract register value
    loc_start_word = loc_right / 32
    loc_end_word = loc_left / 32
    loc_curr_word = loc_start_word
    while (loc_curr_word <= loc_end_word):
      # What bits of the location word are we looking at?
      if (loc_curr_word == loc_start_word):
        loc_curr_right = loc_right % 32
      else:
        loc_curr_right = 0
      if (loc_curr_word == loc_end_word):
        loc_curr_left = loc_left % 32
      else:
        loc_curr_left = 31

      # What bits of the abstract register are used for this
      # word of the location?
      # This is the requested range of the field, plus our
      # current offset into the location (when the location spans
      # multiple words).
      # Additionally, account for the possibility that the
      # location range is larger than the field range
      val_right = field_right + (loc_curr_right + loc_curr_word * 32 - loc_right)
      val_left = min((val_right + (loc_curr_left - loc_curr_right)), field_left)
      val_bits = val_left - val_right + 1

      # In severe cases of the location being larger than the
      # field, the entire location word might be out of
      # range, so detect this and move to the next location
      if (val_left < val_right):
        loc_curr_word = loc_end_word + 1
        continue

      # What words of the debug register value are used?
      val_start_word = val_right / 32
      val_end_word = val_left / 32

      if (val_start_word == val_end_word):
        # Simple - only one word
        # Add a mapping for the range of the current location word
        # to the range of the current value word
        self.__addFieldEntry(val_start_word, var['buf'], loc_curr_word, loc_curr_left, loc_curr_right, val_left % 32, val_right % 32, val_bits)
      else:
        # More complex - two words of the value buffer
        # Split into two parts, one for each word of the value
        # buffer

        # Word 0 starts at the original right offset into
        # the first value word, and ends at bit 31.  We
        # offset into the location word to match.
        loc_left_0 = loc_curr_right + (31 - val_right % 32)
        loc_right_0 = loc_curr_right
        val_left_0 = 31
        val_right_0 = val_right % 32

        # Word 1 starts at bit 0 of the second value word,
        # and ends at the original left offset.  Again, shift
        # into the current location word to match.
        loc_left_1 = loc_curr_left
        loc_right_1 = loc_left_0 + 1
        val_left_1 = val_left % 32
        val_right_1 = 0

        # Add mappings for this location word to both value words
        self.__addFieldEntry(val_start_word, var['buf'], loc_curr_word, loc_left_0, loc_right_0, val_left_0, val_right_0, val_bits)
        self.__addFieldEntry(val_end_word, var['buf'], loc_curr_word, loc_left_1, loc_right_1, val_left_1, val_right_1, val_bits)

      loc_curr_word = loc_curr_word + 1

  def __addFieldEntry(self, val_word, loc_buf, loc_word, loc_left, loc_right, val_left, val_right, val_bits):
    # Clean up some aspects of this physical value -> field mapping,
    # and save it.

    if (((val_left + val_word * 32) == val_bits - 1) or
        ((loc_left - loc_right) <= (val_left - val_right))):
      # All the valid bits of the value are used,
      # or the location is smaller than/equal to the
      # value range, so no need to mask the source
      val_left = None
    fieldEntry = {'dest': (loc_left, loc_right), 'value': 'buf[' + str(val_word) + ']', 'src': (val_left, val_right)}
    self.__varFields[loc_buf][loc_word].append(fieldEntry)

  def finish(self):
    # construct the write function

    # First, examine all the nets/memories, since we
    # may only be writing part of the RTL range.
    # Sort by variable names first for determinism
    keylist = self.__parent.nets().keys()
    keylist.sort()
    for key in keylist:
      var = self.__parent.nets()[key]
      if var['write']:
        self.__parent.addWriteCode('carbonExamine(mObj, ' + var['var'] + ', ' + var['buf'] + ', 0);\n')
    keylist = self.__parent.mems().keys()
    keylist.sort()
    for key in keylist:
      var = self.__parent.mems()[key]
      # Iterate over each index used in this memory
      indexlist = var['indices'].keys()
      indexlist.sort()
      for index in indexlist:
        indexvar = var['indices'][index]
        if indexvar['write']:
          self.__parent.addWriteCode('carbonExamineMemory(' + indexvar['var'] + ', ' + str(index) + ', ' + indexvar['buf'] + ');\n')

    # Clear the ranges used by the write fields and
    # mask in the new data
    self.__parent.addWriteCode('\n')
    keylist = self.__varFields.keys()
    keylist.sort()
    for key in keylist:
      varlist = self.__varFields[key]
      for i in range(len(varlist)):
        var = varlist[i]
        self.__parent.addWriteCode(key + '[' + str(i) + '] &= ~(0')
        for varfield in var:
          self.__parent.addWriteCode(' | ' + shiftAndMask(None, varfield['dest'], 'left'))
        self.__parent.addWriteCode(');\n')
        self.__parent.addWriteCode(key + '[' + str(i) + '] |= (0')
        for varfield in var:
          srctext = shiftAndMask(varfield['value'], varfield['src'], 'right')
          self.__parent.addWriteCode(' | ' + shiftAndMask(srctext, varfield['dest'], 'left'))
        self.__parent.addWriteCode(');\n')
    self.__parent.addWriteCode('\n')

    # Deposit all the nets/memories
    # Sort by variable names first for determinism
    keylist = self.__parent.nets().keys()
    keylist.sort()
    for key in keylist:
      var = self.__parent.nets()[key]
      if var['write']:
        self.__parent.addWriteCode('carbonDepositFast(mObj, ' + var['var'] + ', ' + var['buf'] + ', 0);\n')
    keylist = self.__parent.mems().keys()
    keylist.sort()
    for key in keylist:
      var = self.__parent.mems()[key]
      # Iterate over each index used in this memory
      indexlist = var['indices'].keys()
      indexlist.sort()
      for index in indexlist:
        indexvar = var['indices'][index]
        if indexvar['write']:
          self.__parent.addWriteCode('carbonDepositMemory(' + indexvar['var'] + ', ' + str(index) + ', ' + indexvar['buf'] + ');\n')

# Derived class for specific abstract registers
class CarbonDebugRegisterDerivedClass(cpp.Class):
  def __init__(self, compName, name, reg):
    cpp.Class.__init__(self, name, ['public CarbonDebugRegister'])

    self.__reg = reg

    self.__ctor = self.addConstructor('(%s* owner)' % compName, inline = True, baseClassInit = 'CarbonDebugRegister(owner->getCarbonObject())')

    self.__dtor = self.addDestructor(inline = True, virtual = True)

    self.addMemberFunction('void', 'reset', '()', inline = True)

    self.__readfunc = self.addMemberFunction('void', 'read', '(CarbonUInt32 *buf)', inline = True, virtual = True)
    self.__writefunc = self.addMemberFunction('void', 'write', '(CarbonUInt32 *buf)', inline = True, virtual = True)

    self.__readExpr = ReadExpr(self)
    self.__writeExpr = WriteExpr(self)

    self.__nets = {}
    self.__mems = {}

    for field in reg.fields():
      self.__addField(field)

    self.__readExpr.finish()
    self.__writeExpr.finish()

    func = self.addMemberFunction('void', 'readOffset', '(CarbonUInt32 *buf, CarbonUInt32 offset, CarbonUInt32 shift, CarbonUInt32 mask)', inline = True)
    func.addCode("""if(offset >= %(myoffset)d && offset < (%(myoffset)d + %(widthInBytes)d)) {
  CarbonUInt32 mybuf[%(widthInWords)d];
  read(mybuf);
  *buf = (mybuf[(offset-%(myoffset)d)/4] >> shift) & mask;
}
else *buf = 0;""" % {'myoffset':reg.offset(), 'widthInWords':(reg.width()+31)/32, 'widthInBytes':(reg.width()+7)/8 })
    
    func = self.addMemberFunction('void', 'writeOffset', '(CarbonUInt32 *buf, CarbonUInt32 offset, CarbonUInt32 shift, CarbonUInt32 mask)', inline = True)
    func.addCode("""if(offset >= %(myoffset)d && offset < (%(myoffset)d + %(widthInBytes)d)) {
  CarbonUInt32 shiftedMask = mask << shift;
  CarbonUInt32 shiftedData = *buf << shift;
  CarbonUInt32 mybuf[%(widthInWords)d];
  read(mybuf);
  CarbonUInt32 temp = mybuf[(offset-%(myoffset)d)/4] & ~shiftedMask;
  temp |= shiftedData & shiftedMask;
  mybuf[(offset-%(myoffset)d)/4] = temp;
  write(mybuf);
}""" % {'myoffset':reg.offset(), 'widthInWords':(reg.width()+31)/32, 'widthInBytes':(reg.width()+7)/8})
    
  def nets(self):
    return self.__nets

  def mems(self):
    return self.__mems

  def addReadCode(self, code):
    self.__readfunc.addCode(code)

  def addWriteCode(self, code):
    self.__writefunc.addCode(code)

  def width(self):
    return self.__reg.width()

  def bigEndian(self):
    return self.__reg.bigEndian()

  def regGroup(self):
    return self.__reg.group()

  def regName(self):
    return self.__reg.name()

  def getCustomCodePre(self, section):
    return self.__reg.getCustomCodePre(section)
  
  def getCustomCodePost(self, section):
    return self.__reg.getCustomCodePost(section)
  
  def __addNet(self, path):
    # Adds a net to our list, and returns a CarbonNetID name for it
    
    # See if we already have this net
    net = self.__nets.get(path)
    if (net == None):
      # Add a new net
      net = {'var': 'mNet' + str(len(self.__nets)), 'read': False, 'write': False}
      self.__nets[path] = net
      var = self.addMemberVariable('CarbonNetID *', net['var'], access='private')
      var.setDescription('Net ID for RTL signal ' + path)
      # Find the net in the constructor
      self.__ctor.addCode(net['var'] + ' = carbonFindNet(mObj, "' + cpp.escapeCString(path) + '");\n')
      # Add a buffer for this net's value
      bufname = net['var'] + '_buf'
      net['buf'] = bufname
      var = self.addMemberVariable('CarbonUInt32 *', bufname, access='private')
      var.setDescription('Read/write buffer for ' + path)
      # Allocate storage in the constructor
      self.__ctor.addCode(bufname + ' = new CarbonUInt32[carbonGetNumUInt32s(' + net['var'] + ')];\n')
      self.__ctor.addCode('memset(' + bufname + ', 0, carbonGetNumUInt32s(' + net['var'] + ') * sizeof(CarbonUInt32));\n')
      # Deallocate storage in the destructore
      self.__dtor.addCode('delete [] ' + bufname + ';\n')
      

    return net

  def __addMem(self, path, index):
    # Adds a memory to our list, and returns a CarbonMemoryID name for it
    
    # See if we already have this memory
    mem = self.__mems.get(path)
    if (mem == None):
      # Add a new memory
      mem = {'var': 'mMem' + str(len(self.__mems)), 'indices': {}}
      self.__mems[path] = mem
      var = self.addMemberVariable('CarbonMemoryID *', mem['var'], access='private')
      var.setDescription('Net ID for RTL memory ' + path)
      # Find the memory in the constructor
      self.__ctor.addCode(mem['var'] + ' = carbonFindMemory(mObj, "' + cpp.escapeCString(path) + '");\n')

    # See if we've seen this index
    memidx = mem['indices'].get(index)
    if (memidx == None):
      # Record the index
      # We duplicate the memory variable name here for symmetry with the
      # dictionary returned by __addNet()
      memidx = {'var': mem['var'], 'read': False, 'write': False}
      mem['indices'][index] = memidx
      # Add a buffer for this memory index's value
      bufname = mem['var'] + '_index_' + str(index) + '_buf'
      memidx['buf'] = bufname
      var = self.addMemberVariable('CarbonUInt32 *', bufname, access='private')
      var.setDescription('Read/write buffer for ' + mem['var'] + ' index ' + str(index))
      # Allocate storage in the constructor
      self.__ctor.addCode(bufname + ' = new CarbonUInt32[carbonMemoryRowNumUInt32s(' + mem['var'] + ')];\n')
      self.__ctor.addCode('memset(' + bufname + ', 0, carbonMemoryRowNumUInt32s(' + mem['var'] + ') * sizeof(CarbonUInt32));\n')
      # Deallocate storage in the destructore
      self.__dtor.addCode('delete [] ' + bufname + ';\n')

    return memidx


  def __addUser(self, field):
    # Create the field entry
    bufname = field.name() + '_buf'
    var = {'var': field.name(), 'buf': bufname, 'read': False, 'write': False}
    mem = self.addMemberVariable('CarbonUInt32* ', bufname, access='private')
    mem.setDescription('Read/write buffer for user field ' + field.name())

    # Allocate storage in the constructor
    size = abs(field.low() - field.high()) + 1
    words = str((size + 31)/32)
    self.__ctor.addCode(bufname + ' = new CarbonUInt32[' + words + '];\n')
    self.__ctor.addCode('memset(' + bufname + ', 0, ' + words + '* sizeof(CarbonUInt32));\n')
    
    # Deallocate storage in the destructore
    self.__dtor.addCode('delete [] ' + bufname + ';\n')
    return var

  def __addField(self, field):
    if (field.loc().type() == 'constant'):
      # constants are read-only
      self.__readExpr.addField(field)
    else:
      # Create a member netID/memID for the field's location
      if (field.loc().type() == 'register'):
        net = field.loc().castRegister()
        var = self.__addNet(net.path())
      elif (field.loc().type() == 'array'):
        mem = field.loc().castArray()
        var = self.__addMem(mem.path(), mem.index())
      elif field.loc().type() == 'user':
        # User defined field
        var = self.__addUser(field)
        
      else:
        # Something went wrong
        var = None
        
      # update read/write expressions
      if (field.access() != 'write-only'):
        self.__readExpr.addField(field, var)
      if (field.access() != 'read-only'):
        self.__writeExpr.addField(field, var)

class DebugRegGroup:
  def __init__(self, name, start, end):
    self.__name = name
    self.__start = start
    self.__end = end
  def name(self): return self.__name
  def start(self): return self.__start
  def end(self): return self.__end

