#******************************************************************************
# Copyright (c) 2008-2011 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

##
# \file
# Classes that represent SoC Designer CADI Memory Interfaces

from Carbon import cpp
from Carbon import Cfg
import sys

class DebugMemoryLoc:
  def __init__(self, memoryLoc, width, index):
    self.__loc        = memoryLoc
    self.__blockWidth = width
    self.__index      = index
    
  def index(self):
    return self.__index
  
  def blockWidth(self):
    return self.__blockWidth

  def type(self):
    return self.__loc.type()

  def instantiate(self, className):
    return ''
  
  def writeCode(self):
    return ''
  
  def readCode(self):
    return ''
  
class DebugMemoryLocRTL(DebugMemoryLoc):
  def __init__(self, memoryLoc, width, index):
    DebugMemoryLoc.__init__(self, memoryLoc, width, index)
    self.__loc = memoryLoc

  def path(self):
    return self.__loc.castRTL().path()
  
  def memIdName(self):
    return 'mMemID_' + str(self.index())

  ## this width is the width declared in the memLoc type "rtl", not the width of the underlying RTL memory
  def width(self):
    return self.__loc.castRTL().width()

  def displayStartWord(self):
    return self.__loc.displayStartWord()
    
  def displayEndWord(self):
    return self.__loc.displayEndWord()
    
  def displayMsb(self):
    return self.__loc.displayMsb()

  def displayLsb(self):
    return self.__loc.displayLsb()

  def startWord(self):
    return self.__loc.castRTL().addrOffset()

  def endWord(self):
    return self.__loc.castRTL().endWord()
    
  def msb(self):
    return self.__loc.castRTL().msb()
  
  def lsb(self):
    return self.__loc.castRTL().lsb()
  
  def writeCode(self):
    code = """\
      if (%(memIdName)s != NULL && displayRowAddress >= 0x%(displayStart)xULL && displayRowAddress <= 0x%(displayEnd)xULL) {  // is the address within display range of memBlock?
        CarbonUInt64 RTLMinAddr = std::min(0x%(startWord)xULL, 0x%(endWord)xULL);
        CarbonUInt64 RTLMaxAddr = std::max(0x%(startWord)xULL, 0x%(endWord)xULL);

        CarbonUInt32 RTLWordAddr = (CarbonUInt32)((displayRowAddress - 0x%(displayStart)xULL) + RTLMinAddr);  // RTLWordAddr: the address in HDL memory

        if (RTLWordAddr <= RTLMaxAddr) { // is address within HDL memory?
          if ( ! ( ( displayLowBitIndexOfRequest > %(displayMsb)s) || ( %(displayLsb)s > displayHiBitIndexOfRequest ) ) ) { // does the display range cover any bits within the request range?
            // Perform Read Modify Write on a whole RTL word
            clearBuffer(&mMemBuffer, rowWidthInUInt32s*sizeof(CarbonUInt32));
            carbonExamineMemory(%(memIdName)s, RTLWordAddr, mMemBuffer);

            CarbonUInt32 numBitsToWrite = %(displayMsb)s - %(displayLsb)s + 1;
            if ( numBitsToWrite > (unitSizeInBytes*8) )
              numBitsToWrite = (unitSizeInBytes*8);

            // adjust the bit index per the RTL bit range
            CarbonUInt32 depositBitIndex = 0;
            CarbonUInt32 extractBitIndex = (%(displayLsb)d + ((unitSizeInBytes*unit)*8));
            if ( displayLowBitIndexOfRequest >= %(displayLsb)d ) {
              depositBitIndex = displayLowBitIndexOfRequest - %(displayLsb)d;
              extractBitIndex -= %(displayLsb)d;
            }
            CarbonAbstractRegisterID ar_ID = carbonAbstractRegisterCreate( extractBitIndex, numBitsToWrite, depositBitIndex);
            CarbonAbstractRegisterXfer(ar_ID, mWriteBuffer, mMemBuffer);
            carbonAbstractRegisterDestroy(ar_ID);
            
            carbonDepositMemory(%(memIdName)s, RTLWordAddr, mMemBuffer);
         }
       }
     }
""" % {'memIdName':    self.memIdName(),
       'startWord':    self.startWord(),
       'endWord':      self.endWord(),
       'displayMsb':   self.displayMsb(),
       'displayLsb':   self.displayLsb(),
       'displayStart': self.displayStartWord(),
       'displayEnd':   self.displayEndWord()
       }
    return code

  def readCode(self):
    code = """\
      //
      
      if (%(memIdName)s != NULL && displayRowAddress >= 0x%(displayStart)xULL && displayRowAddress <= 0x%(displayEnd)xULL) {  // is the address within display range of memBlock?
        CarbonUInt64 RTLMinAddr = std::min(0x%(startWord)xULL, 0x%(endWord)xULL);
        CarbonUInt64 RTLMaxAddr = std::max(0x%(startWord)xULL, 0x%(endWord)xULL);

        CarbonUInt32 RTLWordAddr = (CarbonUInt32)((displayRowAddress - 0x%(displayStart)xULL) + RTLMinAddr);  // RTLWordAddr: the address in HDL memory

        if (RTLWordAddr <= RTLMaxAddr) { // is address within HDL memory?
          if ( ! ( ( displayLowBitIndexOfRequest > %(displayMsb)s) || ( %(displayLsb)s > displayHiBitIndexOfRequest ) ) ) { // does the display range cover any bits within the request range?
            // Perform Read
            clearBuffer(&mMemBuffer, rowWidthInUInt32s*sizeof(CarbonUInt32));          
            carbonExamineMemory(%(memIdName)s, RTLWordAddr, mMemBuffer);
            CarbonUInt32 numBitsToRead = %(displayMsb)s - %(displayLsb)s + 1;
            if ( numBitsToRead > (unitSizeInBytes*8) )
              numBitsToRead = (unitSizeInBytes*8);
            CarbonUInt32 extractBitIndex = 0;
            CarbonUInt32 depositBitIndex = (%(displayLsb)d + ((unitSizeInBytes*unit)*8));
            if ( displayLowBitIndexOfRequest >= %(displayLsb)d ) {
              extractBitIndex = displayLowBitIndexOfRequest - %(displayLsb)d;
              depositBitIndex -= %(displayLsb)d;
            }

            CarbonAbstractRegisterID ar_ID = carbonAbstractRegisterCreate(extractBitIndex, numBitsToRead, depositBitIndex);
            CarbonAbstractRegisterXfer(ar_ID, mMemBuffer, mReadBuffer);
            carbonAbstractRegisterDestroy(ar_ID);
         }
       }
     }
""" % {'memIdName':    self.memIdName(),
       'startWord':    self.startWord(),
       'endWord':      self.endWord(),
       'displayMsb':   self.displayMsb(),
       'displayLsb':   self.displayLsb(),
       'displayStart': self.displayStartWord(),
       'displayEnd':   self.displayEndWord()
       }
    return code
  
class DebugMemoryLocPort(DebugMemoryLoc):
  def __init__(self, memoryLoc, width, index, portObj):
    DebugMemoryLoc.__init__(self, memoryLoc, width, index)
    self.__loc = memoryLoc.castPort()
    self.__portObj = portObj
    
  def hasFixedAddr(self):
    return self.__loc.hasFixedAddr()

  def portInstance(self):
    return self.__portObj.instanceName()

  def instantiate(self, className):
    code = """  {
    uint32_t numRegions = comp->%(portInstance)s->getNumRegions();
    if(numRegions > 0) {
      CarbonInternalMemMapEntry map(numRegions);
      comp->%(portInstance)s->getAddressRegions(map.start, map.size, map.name);
      for(uint32_t i = 0; i < numRegions; ++i) {
        mBlocks.push_back(new %(className)s(comp, mxdi, map.name[i].c_str(), map.start[i], map.size[i]));
      }
    }
  }
""" % {'portInstance': self.portInstance(),
       'className': className }
    return code

  def writeCode(self):
    code = """
    // Write to Port, a single byte at a time
    CarbonDebugAccessStatus status;
    uint32_t numProcessed = 0;
    uint64_t startByteAddress = startAddress*unitSizeInBytes;
    uint32_t bytesToProcess = unitsToWrite*unitSizeInBytes;

    for (uint32_t i = 0; i < bytesToProcess; ++i) {
      numProcessed = 0;
      status = mComp->getTopComp()->%s->debugAccess(eCarbonDebugAccessWrite, startByteAddress+i, 1, &const_cast<uint8_t*>(data)[i], &numProcessed, NULL);
      // probably should also check numProcessed == 1 here but some xactors will return eCarbonDebugAccessStatusOk when numProcessed is zero
      // the following assumes that CASI_STATUS_OK returned from debugAccess is equivalent to eCarbonDebugAccessStatusOk (both are 0)
      if (status != eCarbonDebugAccessStatusOk) {
        result = false;
      }
    }""" % self.portInstance()
    return code
  
  def readCode(self):
    code = """
    // Read from Port, a single byte at a time
    CarbonDebugAccessStatus status;
    uint32_t numProcessed = 0;
    uint64_t startByteAddress = startAddress*unitSizeInBytes;
    uint32_t bytesToProcess = unitsToRead*unitSizeInBytes;
    for (uint32_t i = 0; i < bytesToProcess; ++i) {
      numProcessed = 0;
      status = mComp->getTopComp()->%s->debugAccess(eCarbonDebugAccessRead, startByteAddress+i, 1, &data[i], &numProcessed, NULL);
      // probably should also check numProcessed == 1 here but some xactors will return eCarbonDebugAccessStatusOk when numProcessed is zero
      // the following assumes that CASI_STATUS_OK returned from debugAccess is equivalent to eCarbonDebugAccessStatusOk (both are 0)
      if (status != eCarbonDebugAccessStatusOk) {
        result = false;
      }
    }""" % self.portInstance()
    return code
    
class DebugMemoryLocUser(DebugMemoryLoc):
  def __init__(self, memoryLoc, width, index):
    DebugMemoryLoc.__init__(self, memoryLoc, width, index)
    self.__loc = memoryLoc.castUser()

  def name(self):
    return self.__loc.name()
  
# CPP class implementation for a memory block containing RTL memory
class DebugMemoryBlockClass(cpp.Class):
  def __init__(self, compName, className, memoryBlock):
    cpp.Class.__init__(self, className, ['public CarbonDebugMemoryBlock'])
    self.__compName      = compName
    self.__block         = memoryBlock
    self.__bufferList  = ['mMemBuffer', 'mWriteBuffer', 'mReadBuffer']
    
    # constructor - allocate memory here
    func = self.addConstructor('(%(compName)s* comp, %(compName)s_CADI* mxdi, const char* name, uint64_t base, uint64_t size)' % {'compName':compName} ,
                               inline = True, baseClassInit = 'CarbonDebugMemoryBlock(name, base, size, %(access)s)' % {'access':memoryBlock.access()})
    destructor = self.addDestructor(inline = True, virtual = True)

    func.addCode('\n');
    func.addCode('unsigned int initialBufferByteSize = 0;\n');
    for loc in self.__block.locs():
      if loc.type() == 'rtl':
        func.addCode('%(memId)s = carbonFindMemory(comp->getCarbonObject(), "%(path)s");\n' % {'memId':loc.memIdName(), 'path':cpp.escapeCString(loc.path()) })
        func.addCode("initialBufferByteSize = std::max(initialBufferByteSize, (sizeof(CarbonUInt32) * carbonMemoryRowNumUInt32s(%(memId)s)));\n" % {'memId':loc.memIdName()})


    ## now do everything needed to allocate and release the temporary buffers used for readMem/writeMem
    func.addCode(' // Allocate space for the buffers\n');
    for buffer in self.__bufferList:
      func.addCode("%(bufferName)sByteSize = initialBufferByteSize;\n%(bufferName)s = new CarbonUInt32[%(bufferName)sByteSize];\n\n" % {'bufferName':buffer })
      destructor.addCode("delete [] %(bufferName)s;  %(bufferName)s = NULL;  %(bufferName)sByteSize = 0;\n" % {'bufferName':buffer })
      self.addMemberVariable('CarbonUInt32', '%(bufferName)sByteSize' % {'bufferName':buffer }, 'private')
      self.addMemberVariable('CarbonUInt32*', '%(bufferName)s' % {'bufferName':buffer }, 'private')

    func = self.addMemberFunction('bool', 'memWrite',
                                  '(uint64_t startAddress, uint32_t unitsToWrite, uint32_t unitSizeInBytes,\n\
                const uint8_t *data, uint32_t *actualNumOfUnitsWritten, uint8_t doSideEffects)', inline = True)
    func.addCode(self.memWriteCode())
    func.addPostCode("  return result;")

    func = self.addMemberFunction('bool', 'decode',
                                  '(uint64_t startAddr, uint32_t requestedUnits,uint32_t unitSizeInBytes,uint32_t *actualUnits) const', inline = True)
    func.addCode(self.memDecodeMethod())
    func.addPostCode("return result;")
      
    func = self.addMemberFunction('bool', 'memRead',
                                  '(uint64_t startAddress, uint32_t unitsToRead, uint32_t unitSizeInBytes,\n\
                uint8_t *data, uint32_t *actualNumOfUnitsRead, uint8_t doSideEffects)', inline = True)
    func.addCode(self.memReadCode())
    func.addPostCode("  return result;")

    func = self.addMemberFunction('void', 'clearBuffer',
                                  '(CarbonUInt32** bufferAddress, uint32_t requestedSizeInBytes)', inline = True)
    func.addCode(self.clearBufferCode())
    func.addPostCode("")

    for loc in self.__block.locs():
      if loc.type() == 'rtl': self.addMemberVariable('CarbonMemoryID*', 'mMemID_%d' % loc.index(), 'private')
        
    var = self.addMemberVariable('%s*'%compName, 'mComp', 'private')
    var.setConstructorInit('comp')

  def isRTL(self):
    # We don't mix and match locators, so just need to check the first one
    if len(self.__block.locs()) > 0:
      if self.__block.locs()[0].type() == 'rtl':
        return True
    return False
  
  def isPort(self):
    # We don't mix and match locators, so just need to check the first one
    if len(self.__block.locs()) > 0:
      if self.__block.locs()[0].type() == 'port':
        return True
    return False
  
  def isUser(self):
    # We don't mix and match locators, so just need to check the first one
    if self.__block.locs()[0].type() == 'user':
      return True
    return False

  def locs(self):
    return self.__block.locs()
  
  def memReadCode(self):
    readCode = ''
    if self.isRTL():
      readCode = self.rtlRead()

    else:
      for loc in self.locs():
        readCode += loc.readCode()
          
    code = ''
    if self.__block.displayAtZero():
      code += '  startAddress += (%s)/unitSizeInBytes;  // displayAtZero is true\n' % self.__block.baseAddr()
      
    return code + """\
  bool hit    = decode(startAddress, unitsToRead, unitSizeInBytes, actualNumOfUnitsRead);
  bool result = hit;

  // If it's a hit
  if(hit) {
    // Only report success when all units are read because the overlying code will loop
    // through all the blocks until successfully read
    if(unitsToRead != *actualNumOfUnitsRead)
      result = false;

%(readCode)s
  }
""" % {'width': self.__block.width(), 'readCode': readCode} 

  def rtlRead(self):
    readCode = ''
    for loc in self.locs():
      readCode += loc.readCode()

    return """\
    // some constants for this memory
    CarbonUInt32  rowWidthInBytes   = (%(width)s+7)/8;  // aka displayUnitSizeInBytes
    CarbonUInt32  rowWidthInUInt32s = (%(width)s+31)/32;
    CarbonUInt32  addressMask = ((rowWidthInBytes/unitSizeInBytes)-1); // since rowWidthInBytes is greater than or equal to unitSizeInBytes then addressMask should always be >= 0

    // Now read the memory
    // here we preload the readbuffer with the all the data array and then overwrite it with the values from memory (this allows for a memory read of fewer bytes than (unitSizeInBytes*unitsToRead))
    clearBuffer(&mReadBuffer, unitSizeInBytes*unitsToRead);
    memcpy8to32(mReadBuffer, data, unitSizeInBytes*unitsToRead);

    for(unsigned int unit = 0; unit < unitsToRead; ++unit) {
      // Calculate memory address
      CarbonUInt64 rowAddrInBytes = (((startAddress + unit) * unitSizeInBytes) );
      CarbonUInt64 displayRowAddressInBytes = rowAddrInBytes - base();
      CarbonUInt64 displayRowAddress = displayRowAddressInBytes / rowWidthInBytes;   // this is the word address in terms of the Display range
      CarbonUInt32 displayLowBitIndexOfRequest = (((startAddress+unit)&addressMask)*8);
      CarbonUInt32 displayHiBitIndexOfRequest = displayLowBitIndexOfRequest+(unitSizeInBytes*8)-1;

%(readCode)s
    }

   // Copy all the data back
   memcpy32to8(data, mReadBuffer, unitSizeInBytes*unitsToRead);
    
    """ % {'width': self.__block.width(), 'readCode': readCode, 'baseAddr':self.__block.baseAddr()} 


  def minDisplayStartWord(self):
    minValue = sys.maxint
    for loc in self.__block.locs():
      if loc.type() == 'rtl' and loc.displayStartWord() < minValue:
        minValue = loc.displayStartWord()
      if loc.type() == 'port' or loc.type() == 'user':
        return ((self.__block.baseAddr())/((self.__block.width()+7)/8))    ## the first one of this type is all we need, convert to word address
    return minValue

  def maxDisplayStartWord(self):
    maxValue = 0
    for loc in self.__block.locs():
      if loc.type() == 'rtl' and loc.displayStartWord() > maxValue:
        maxValue = loc.displayStartWord()
      if loc.type() == 'port' or loc.type() == 'user':
        return ((self.__block.baseAddr())/((self.__block.width()+7)/8))    ## the first one of this type is all we need, convert to word address
    return maxValue

  def minDisplayEndWord(self):
    minValue = sys.maxint
    for loc in self.__block.locs():
      if loc.type() == 'rtl' and loc.displayEndWord() < minValue:
        minValue = loc.displayEndWord()
      if loc.type() == 'port' or loc.type() == 'user':
        return ( ((self.__block.baseAddr() + self.__block.size())/((self.__block.width()+7)/8)) - 1)    ## the first one of this type is all we need, convert to word address, subtract 1 to get end word address
    return minValue

  def maxDisplayEndWord(self):
    maxValue = 0
    for loc in self.__block.locs():
      if loc.type() == 'rtl' and loc.displayEndWord() > maxValue:
        maxValue = loc.displayEndWord()
      if loc.type() == 'port' or loc.type() == 'user':
        return ( ((self.__block.baseAddr() + self.__block.size())/((self.__block.width()+7)/8)) - 1)    ## the first one of this type is all we need, convert to word address, subtract 1 to get end word address
    return maxValue

    
  def clearBufferCode(self):
    ## define a function that will make sure the buffer (arg 1) is at least as the requested buffer size (arg 2), it also fills the buffer with zeros
    code = """
CarbonUInt32* bufferSize;
// figure out which buffer the caller has requested
"""
    for buffer in self.__bufferList:
      code += """\
if ( *bufferAddress == %(bufferName)s ){
  bufferSize = &%(bufferName)sByteSize;
} else """ % {'bufferName':buffer }
    ## handle the case where there is no match, should this assert?
    code += """\
{
  return;  // unable to identify buffer
}
if ( *bufferSize < requestedSizeInBytes ) {
  delete [] *bufferAddress;
  *bufferAddress = new CarbonUInt32[requestedSizeInBytes];
  *bufferSize  = requestedSizeInBytes;
}
memset(*bufferAddress, 0, requestedSizeInBytes);
"""
    return code

  def memDecodeMethod(self):
    # figure out the address range of this block in terms of bytes
    # note the careful use of
    #   unitSizeInBytes which defines the size of the units in the request (arguments to this method), 
    # and displayUnitSizeInBytes  which defines the size of the underlying memory.
    # Calls to decode() may come from CADIMemRead where unitSizeInBytes and displayUnitSizeInBytes can be equal,
    #  or from debugAccess where the unitSizeInBytes is 1 whiich may be smaller than displayUnitSizeInBytes
    code = """
//! Block Address Decoder, here startAddr is an address in terms of unitSizeInBytes units
uint64_t startByteAddress = startAddr * unitSizeInBytes;
uint64_t memBlockAddrInBytes = ("True" == "%(isRTL)s") ? base() : 0;  // if type == Rtl then use base() else the displayStart/displayEnd variables below capture the block address
uint64_t displaySizeInWords = 0x%(displayEndWord)xULL - 0x%(minDisplayStartWord)xULL + 1; // in unitSizeInBytes units (word)
uint64_t displayUnitSizeInBytes = (%(displayWidth)s +7)/8;  // this is not necessarily unitSizeInBytes, it may be larger
uint64_t displayStartWordInBytes = 0x%(minDisplayStartWord)xULL * displayUnitSizeInBytes;
uint64_t displayEndWordInBytes = (0x%(displayEndWord)xULL * displayUnitSizeInBytes) + (displayUnitSizeInBytes - 1);
uint64_t displaySizeInBytes = displayEndWordInBytes - displayStartWordInBytes + 1;

bool result = false;
*actualUnits    = 0; // If not a hit, actualUnits has to reflect that nothing was accessed

// First Check address for this block to see if it's a hit, i.e. lies within your memory space
if(((memBlockAddrInBytes+displayStartWordInBytes) <= startByteAddress) && (startByteAddress < (memBlockAddrInBytes+displayStartWordInBytes + displaySizeInBytes ))) {
    result = true;
    // If we are writing above available memory return number of bytes which can be written else return the number of bytes requested.   
    if (((displaySizeInBytes+memBlockAddrInBytes ) - (startByteAddress-displayStartWordInBytes)) < ((uint64_t)requestedUnits*(uint64_t)unitSizeInBytes)) // doing arithmetic in 64 bit nums as passed in values multiplies to a num > 32 bit wide.
      *actualUnits = (uint32_t)(((displaySizeInBytes+memBlockAddrInBytes ) - (startByteAddress-displayStartWordInBytes))/unitSizeInBytes);
    else {
      *actualUnits = (uint32_t)requestedUnits;
    }
  }

""" % {'isRTL': self.isRTL(), 'minDisplayStartWord': self.minDisplayStartWord(), 'displayEndWord': self.maxDisplayEndWord(), 'displayWidth': self.__block.width()}
    return code

    
  def memWriteCode(self):
    writeCode = ''
    if self.isRTL():
      writeCode = self.rtlWrite()
      
    else:
      for loc in self.locs():
        writeCode += loc.writeCode()

    code = ''
    if self.__block.displayAtZero():
      code += '  startAddress += %s/unitSizeInBytes;  // displayAtZero is true\n' % self.__block.baseAddr()
    return code + """\
  bool hit    = decode(startAddress, unitsToWrite, unitSizeInBytes, actualNumOfUnitsWritten);
  bool result = hit;

  if(hit) {
    // Only report success when all units are written because the overlying code will loop
    // through all the blocks until successfully written
    if(unitsToWrite != *actualNumOfUnitsWritten)
      result = false;
%(writeCode)s
  }
  
""" % {'writeCode': writeCode} 

  def rtlWrite(self):
    writeCode = ''
    for loc in self.locs():
      writeCode += loc.writeCode()

    return """\
    // some constants for this memory
    CarbonUInt32  rowWidthInBytes   = (%(width)s+7)/8;  // aka displayUnitSizeInBytes
    CarbonUInt32  rowWidthInUInt32s = (%(width)s+31)/32;
    CarbonUInt32  addressMask = ((rowWidthInBytes/unitSizeInBytes)-1); // since rowWidthInBytes is greater than or equal to unitSizeInBytes then addressMask should always be >= 0

    // use a temp write buffer of UInt32's to hold all of the data
    clearBuffer(&mWriteBuffer, unitSizeInBytes*unitsToWrite);
    memcpy8to32(mWriteBuffer, data, unitSizeInBytes*unitsToWrite);

    // Now write to the memory
    // write only unit*unitSizeInBytes at a time
    // Assume for now rowWidthInBytes is equally dividable by unitSizeInBytes
    for(unsigned int unit = 0; unit < unitsToWrite; ++unit) {
      // Calculate memory address
      CarbonUInt64 rowAddrInBytes = (((startAddress + unit) * unitSizeInBytes) );
      CarbonUInt64 displayRowAddressInBytes = rowAddrInBytes - base();
      CarbonUInt64 displayRowAddress = displayRowAddressInBytes / rowWidthInBytes;   // this is the word address in terms of the Display range
      CarbonUInt32 displayLowBitIndexOfRequest = (((startAddress+unit)&addressMask)*8);
      CarbonUInt32 displayHiBitIndexOfRequest = displayLowBitIndexOfRequest+(unitSizeInBytes*8)-1;


%(writeCode)s
    }""" % {'width': self.__block.width(), 'writeCode': writeCode, 'baseAddr':self.__block.baseAddr()} 

class DebugMemoryBlock:
  def __init__(self, compName, block, parentName, parentID, config, displayAtZero = False):
    self.__compName      = compName
    self.__block         = block
    self.__parentName    = parentName
    self.__parentID      = parentID
    self.__displayAtZero = displayAtZero
    self.__locs          = []

    index = 0
    for loc in block.locs():
      if loc.type() == 'rtl':
        self.__locs.append(DebugMemoryLocRTL(loc, self.width(), index))
      if loc.type() == 'port':
        portLoc = loc.castPort()
        # Look up port object in configuration
        portObj = config.findPort(portLoc.port())
        if portObj != None:
          self.__locs.append(DebugMemoryLocPort(loc, self.width(), index, portObj))
        else:
          config.output().warning("DebugMemoryPortLoc: Port %s was not found in the configuration." % portLoc.port())
      if loc.type() == 'user':
        self.__locs.append(DebugMemoryLocUser(loc, self.width(), index))

      index = index + 1
                    
  def name(self):
    return self.__block.name()

  def index(self):
    return self.__block.index()

  def width(self):
    return self.__block.width()
  
  def hasFixedAddr(self):
    return self.__block.hasFixedAddr()

  def isPort(self):
    if self.__locs[0].type() == 'port':
      return True
    return False
  
  def displayAtZero(self):
    return self.__displayAtZero
  
  def baseAddr(self):
    return self.__block.baseAddr()

  def size(self):
    return self.__block.size()

  def access(self):
    return 'eslapi::CADI_MEM_ReadWrite'
  
  def className(self):
    return 'MemBlock_' + self.__compName + '_' + self.__parentName + '_' + self.name()

  def locs(self):
    return self.__locs

  def instanceName(self):
    return self.__parentName + '_' + self.name() + '_MB'

  def memoryConstructor(self):
    code = ''
    if self.hasFixedAddr():
      code = '  mBlocks.push_back(new %(className)s(comp, mxdi, "%(name)s", 0x%(baseAddr)xULL, 0x%(size)xULL));\n' % {'name'     : self.name(),
                                                                                                   'className': self.className(),
                                                                                                   'baseAddr' : self.baseAddr(),
                                                                                                   'size'     : self.size() }
    else:
      for loc in self.locs():
        code += loc.instantiate(self.className())      

    return code
  
  def emitDefinition(self, userCode):
    blockClass = DebugMemoryBlockClass(self.__compName, self.className(), self)
    return blockClass.emitDefinition(userCode, self.__block)

  def emitImplementation(self):
    return ''

  def minDisplayStartWord(self):
    minValue = sys.maxint
    for loc in self.__block.locs():
      if loc.type() == 'rtl' and loc.displayStartWord() < minValue:
        minValue = loc.displayStartWord()
      if loc.type() == 'port' or loc.type() == 'user':
        return ((self.baseAddr())/((self.width()+7)/8))    ## the first one of this type is all we need, convert to word address
    return minValue

  def maxDisplayStartWord(self):
    maxValue = 0
    for loc in self.__block.locs():
      if loc.type() == 'rtl' and loc.displayStartWord() > maxValue:
        maxValue = loc.displayStartWord()
      if loc.type() == 'port' or loc.type() == 'user':
        return ((self.baseAddr())/((self.width()+7)/8))    ## the first one of this type is all we need, convert to word address
    return maxValue

  def minDisplayEndWord(self):
    minValue = sys.maxint
    for loc in self.__block.locs():
      if loc.type() == 'rtl' and loc.displayEndWord() < minValue:
        minValue = loc.displayEndWord()
      if loc.type() == 'port' or loc.type() == 'user':
        return ( ((self.baseAddr() + self.size())/((self.width()+7)/8)) - 1)    ## the first one of this type is all we need, convert to word address, subtract 1 to get end word address
    return minValue

  def maxDisplayEndWord(self):
    maxValue = 0
    for loc in self.__block.locs():
      if loc.type() == 'rtl' and loc.displayEndWord() > maxValue:
        maxValue = loc.displayEndWord()
      if loc.type() == 'port' or loc.type() == 'user':
        return ( ((self.baseAddr() + self.size())/((self.width()+7)/8)) - 1)    ## the first one of this type is all we need, convert to word address, subtract 1 to get end word address
    return maxValue

  
# CPP class implementation for a memory block containing RTL memory
class DebugMemoryClass(cpp.Class):
  def __init__(self, compName, className, memory):
    cpp.Class.__init__(self, className, ['public CarbonDebugMemory'])

    func = self.addConstructor('(%(compName)s* comp, %(compName)s_CADI* mxdi)' % {'compName':compName}, inline = True)
    
    # Loop through the blocks and let them decide how they should be constructed
    func.addCode('\n'.join([block.memoryConstructor() for block in memory.blocks()]))
    
    self.addDestructor(inline = True, virtual = True)

    func = self.addMemberFunction('bool', 'memWrite', '(uint64_t startAddress, uint32_t unitsToWrite, uint32_t unitSizeInBytes, \n\
                const uint8_t *data, uint32_t *actualNumOfUnitsWritten, uint8_t doSideEffects)', inline = True)
    func.addCode("""
  bool result = false;

  // Loop through the blocks and attempt to access until successful. The blocks have been sorted in
  // address order, or if there is a partial access, it can be continued in a later block
  // but not in an earlier. The blocks cannot overlap.
  for(uint32_t i = 0; i < mBlocks.size(); ++i) {
    if(mBlocks[i]->memWrite(startAddress, unitsToWrite, unitSizeInBytes, data, actualNumOfUnitsWritten, doSideEffects)) {
      result = true;
    }
  }
  if ( result ) {
    // If there was a partial write, we need to update the start address, and number of units
    startAddress += *actualNumOfUnitsWritten;     // startAddress is in terms of units not bytes
    data         += *actualNumOfUnitsWritten*unitSizeInBytes;
    unitsToWrite -= *actualNumOfUnitsWritten;
  }
  return result;
""")
    
    func = self.addMemberFunction('bool', 'memRead', '(uint64_t startAddress, uint32_t unitsToRead, uint32_t unitSizeInBytes, \n\
                uint8_t *data, uint32_t *actualNumOfUnitsRead, uint8_t doSideEffects)', inline = True)
    func.addCode("""
  bool result = false;
  memset(data, 0, unitsToRead*unitSizeInBytes);
  
  // Loop through the blocks and attempt to access until successful. The blocks have been sorted in
  // address order, or if there is a partial access, it can be continued in a later block
  // but not in an earlier. The blocks cannot overlap.
  for(uint32_t i = 0; i < mBlocks.size(); ++i) {
    if(mBlocks[i]->memRead(startAddress, unitsToRead, unitSizeInBytes, data, actualNumOfUnitsRead, doSideEffects)) {
      result = true;
    }
  }

  if ( result ) {
    // If there was a partial read, we need to update the start address, and number of units
    startAddress += *actualNumOfUnitsRead;     // startAddress is in terms of units not bytes
    data         += *actualNumOfUnitsRead*unitSizeInBytes;
    unitsToRead  -= *actualNumOfUnitsRead;
  }
  return result;
""")
    


class DebugMemory:
  def __init__(self, compName, memory, config, useLegacy):
    self.__compName = compName
    self.__memory   = memory
    self.__blocks   = []
    self.__useLegacyMemories = useLegacy
    self.__systemAddressPorts = []

    for block in memory.blocks():
      self.__blocks.append(DebugMemoryBlock(compName, block, self.name(), self.index(), config, memory.displayAtZero()))
               
  def systemAddressPorts(self):
    return self.__memory.systemAddressPorts()
                    
  def name(self):
    return self.__memory.name()

  def index(self):
    return self.__memory.index()
  
  def description(self):
    return self.__memory.comment()

  def className(self):
    return 'DebugMemory_' + self.__compName + '_' + self.name()
  
  def instanceName(self):
    return 'carbonMems[%d]' % self.index()

  def memory(self):
    return self.__memory
  
  def width(self):
    return self.__memory.width()
    
  def maxAddrs(self):
    return self.__memory.maxAddrs()

  def numCycles(self):
    return self.__memory.numCycles()

  def initType(self):
    return self.__memory.initType()

  def eslPort(self):
    return self.__memory.programPreloadEslPort()
    
  def displayAtZero(self):
    return self.__memory.displayAtZero()

  def baseAddrHi(self):
    return self.__memory.baseAddrHi()

  def baseAddrLo(self):
    return self.__memory.baseAddrLo()

  def blocks(self):
    return self.__blocks
  
  def numBlocks(self):
    return len(self.__blocks)

  def initFile(self):
    return self.__memory.initFile()

  def initFileType(self):
    return self.__memory.initFileType()

  def signal(self):
    return self.__memory.signal()
  
  def isProgramMemory(self):
    return self.__memory.isProgramMemory()

  def isProgramMemoryTxt(self):
    if self.__memory.isProgramMemory():
       return 'true'
    return 'false'

  def friendClassDef(self):
    code = '  friend class %s;\n' % self.className()
    for block in self.blocks():
      code += '  friend class %s;\n' % block.className()

    return code

  def minDisplayStartWord(self):
    minValue = sys.maxint
    for block in self.blocks():
      if block.minDisplayStartWord() < minValue:
        minValue = block.minDisplayStartWord()
    return minValue

  def maxDisplayStartWord(self):
    maxValue = 0
    for block in self.blocks():
      if block.maxDisplayStartWord() > maxValue:
        maxValue = block.maxDisplayStartWord()
    return maxValue


  def minDisplayEndWord(self):
    minValue = sys.maxint
    for block in self.blocks():
      if block.minDisplayEndWord() < minValue:
        minValue = block.minDisplayEndWord()
    return minValue

  def maxDisplayEndWord(self):
    maxValue = 0
    for block in self.blocks():
      if block.maxDisplayEndWord() > maxValue:
        maxValue = block.maxDisplayEndWord()
    return maxValue


  def cadiSetup(self):
    code = """
memSpaceInfo[%(id)d].memSpaceId      = %(id)d;
memSpaceInfo[%(id)d].bitsPerMau      = %(width)s;
memSpaceInfo[%(id)d].maxAddress      = (( 1 + 0x%(maxAddress)xULL) * %(mau)s) - 1 ; // max address in bytes
memSpaceInfo[%(id)d].nrMemBlocks     = %(numBlocks)s;
memSpaceInfo[%(id)d].isProgramMemory = %(isProgramMemory)s;

strcpy(memSpaceInfo[%(id)d].memSpaceName, "%(name)s");
strcpy(memSpaceInfo[%(id)d].description,  "%(description)s");

memBlockInfo[%(id)d] = new eslapi::CADIMemBlockInfo_t[%(numBlocks)s];
""" % { 'id':              self.index(),
        'width':           self.width(),
        'maxAddress':      self.maxAddrs(),
        'mau':             ((self.width()+7)/8),
        'numBlocks':       self.instanceName() + '->numBlocks()',
        'isProgramMemory': self.isProgramMemoryTxt(), 
        'name' :           self.name(),
        'description':     self.description()
        }
    i = -1
    code += "// Setup block info\n"
    for block in self.blocks():
      i = i + 1
      code += '''\
memBlockInfo[%(id)d][%(j)d].id             = %(j)d;
memBlockInfo[%(id)d][%(j)d].parentID       = %(id)d;
memBlockInfo[%(id)d][%(j)d].startAddr      = (%(baseAddr)s) + (%(mau)s*0x%(minStartWordAddress)xULL);   // this is the min byte address for ANY memBlock (addr+ (mau* minDisplayStart))
memBlockInfo[%(id)d][%(j)d].endAddr        = (%(baseAddr)s) + (%(mau)s*0x%(maxEndWordAddress)xULL) + %(mau)s -1 ; // this is the max byte address for ANY memBlock (addr+(mau * maxDisplayEnd))
memBlockInfo[%(id)d][%(j)d].cyclesToAccess = 0;
memBlockInfo[%(id)d][%(j)d].readWrite      = %(instanceName)s->blockAccess(%(j)d);

strcpy(memBlockInfo[%(id)d][%(j)d].name,        %(instanceName)s->blockName(%(j)d));
strcpy(memBlockInfo[%(id)d][%(j)d].description, "");
  
''' % { 'id':              self.index(),
        'width':           self.width(),
        'mau':             ((self.width()+7)/8),
        'instanceName':    self.instanceName(),
        'isProgramMemory': self.isProgramMemoryTxt(), 
        'name' :           block.name(),
        'description':     self.description(),
        'minStartWordAddress': block.minDisplayStartWord(),
        'maxEndWordAddress': block.maxDisplayEndWord(),
        'baseAddr':          block.baseAddr(),
        'j':               i
        }

    return code

  def emitDefinition(self, userCode):
    code = ''

    # Emit Block classes
    code += '\n'.join([block.emitDefinition(userCode) for block in self.blocks()])

    # Emit Memory class
    memoryClass = DebugMemoryClass(self.__compName, self.className(), self)
    code += memoryClass.emitDefinition(userCode, self.__memory)
    
    return code
  
  def emitImplementation(self, userCode):
    code = ''
    return code
