#******************************************************************************
# Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

##
# \file
# Classes for code generation of user profile streams and channels


class Profile:
  def __init__(self, config):
    self.__config = config
    self.__streams = []
    for stream in config.profileStreams():
      self.__streams.append(ProfileStream(stream, config))

  # generate code to update all streams after carbonSchedule call
  def update(self, prefix = ''):
    code = ''
    indent = ' ' * (len(prefix) - len(prefix.strip()))
    for stream in self.__streams:
      code += prefix + stream.update()
    return code

  # Generate code to save state of streams
  def save(self, resultVar, streamVar):
    code = ''
    for stream in self.__streams:
      code += '  %(result)s &= %(instance)s.saveData(%(stream)s);\n' % { 'result': resultVar,
                                                                         'instance': stream.instanceName(),
                                                                         'stream': streamVar }
    return code

  # Generate code to restore state of streams
  def restore(self, resultVar, streamVar):
    code = ''
    for stream in self.__streams:
      code += '  %(result)s &= %(instance)s.restoreData(%(stream)s);\n' % { 'result': resultVar,
                                                                            'instance': stream.instanceName(),
                                                                            'stream': streamVar }
    return code

  def classDeclaration(self):
    code = ''
    if len(self.__streams) > 0:
      code += """
#include "MxPI.h"

class ProfSignal {
  public:
    ProfSignal()  { mVal = 0; mPrevVal = 0; }
    ~ProfSignal() { }

    void operator =(CarbonUInt32 val) { mPrevVal = mVal; mVal = val; }
    operator CarbonUInt32() const     { return mVal; }

    bool posedge() const { return (mVal >  mPrevVal); }
    bool negedge() const { return (mVal <  mPrevVal); }
    bool changed() const { return (mVal != mPrevVal); }

    bool saveData(eslapi::CASIODataStream &os)
    {
      os << mVal << mPrevVal;
      return true;
    }
    bool restoreData(eslapi::CASIIDataStream &is)
    {
      is >> mVal >> mPrevVal;
      return true;
    }

  private:
    CarbonUInt32 mVal;
    CarbonUInt32 mPrevVal;
};

class %(compName)s_CAPI : public eslapi::MxPI {
  public:
    %(compName)s_CAPI(eslapi::CASIModuleIF *comp):eslapi::MxPI(comp) {}
    eslapi::CAPIReturn_t CAPIGetProfilingStreams(uint32_t desiredNrStreams, uint32_t *actualNrStreams, eslapi::CAPIStreamInfo_t *streams);
};
""" % { 'compName': self.__config.compName() }
    for stream in self.__streams:
      code += stream.classDeclaration()
    return code

  def classImplementation(self, userCode):
    code = ''
    if len(self.__streams) > 0:

      for stream in self.__streams:
        numBucketChannels = 0
        for channelDesc in stream.channelDescriptors():
          if channelDesc.numBuckets() > 0:
            numBucketChannels = numBucketChannels + 1

        if numBucketChannels > 0:
          code += """
eslapi::CAPIChannelSymbolInfo_t %(streamPrefix)s_optype_symbols[%(numBucketChannels)s];
""" % { 'streamPrefix': stream.dataPrefix(), 'numBucketChannels': numBucketChannels }

        code += """
eslapi::CAPIChannelInfo_t %(streamPrefix)s_channels[%(numChannels)s] = {
""" % { 'streamPrefix': stream.dataPrefix(), 'numChannels': stream.numChannels() }

        lines = []
        bucketChannels = 0
        for channel in stream.channelDescriptors():
          if channel.numBuckets() > 0:
            lines.append('  {const_cast<char*>("%(channel)s"), const_cast<char*>("%(channel)s channel"), eslapi::CAPI_CHANNEL_TYPE_SYMBOL, 1, &%(streamPrefix)s_optype_symbols[%(bucketChannels)s]}' % {
              'channel': channel.name(), "streamPrefix": stream.dataPrefix(), 'bucketChannels': bucketChannels })
            bucketChannels = bucketChannels + 1
          else:
            lines.append('  {const_cast<char*>("%(channel)s"), const_cast<char*>("%(channel)s channel"), eslapi::CAPI_CHANNEL_TYPE_U32}' % { "channel": channel.name() } )
        code += ',\n'.join(lines)
        code += """
};

eslapi::CAPIChannelInfo_t *%(streamPrefix)s_channels_ptr[%(numChannels)s] = {
""" % { 'streamPrefix': stream.dataPrefix(), 'numChannels': stream.numChannels() }
        lines = []
        for channel in stream.channelDescriptors():
          lines.append('  &%(streamPrefix)s_channels[%(index)s]' % { 'streamPrefix': stream.dataPrefix(), 'index': channel.index() })
        code += ',\n'.join(lines)
        code += """
};
"""
      code += """
eslapi::CAPIStreamInfo_t %(compName)s_streams[%(streamCount)s] = {
""" % { "compName": self.__config.compName(), "streamCount": len(self.__streams) }

      lines = []
      for stream in self.__streams:
        lines.append('  {const_cast<char*>("%(stream)s"), const_cast<char*>("%(stream)s"), const_cast<char*>("%(stream)s stream"), %(numChannels)s, &%(streamPrefix)s_channels_ptr[0]}' % {
          'stream': stream.name(), 'comp': self.__config.compName(), 'streamPrefix': stream.dataPrefix(), 'numChannels': stream.numChannels() })
      code += ',\n'.join(lines)
      code += """
};

eslapi::CAPIReturn_t %(compName)s_CAPI::CAPIGetProfilingStreams(uint32_t desiredNrStreams, uint32_t *actualNrStreams, eslapi::CAPIStreamInfo_t *streams) {
""" % { "compName": self.__config.compName() }

      for stream in self.__streams:
        numBucketChannels = 0
        for channel in stream.channelDescriptors():
          if channel.numBuckets() > 0:
            code += """
  %(streamPrefix)s_optype_symbols[%(numBucketChannels)s].nrSymbolValues = %(numBuckets)s + 1;
  %(streamPrefix)s_optype_symbols[%(numBucketChannels)s].symbolValues   = new eslapi::CAPISymbolValue_t[%(numBuckets)s + 1];
  %(streamPrefix)s_optype_symbols[%(numBucketChannels)s].symbolValues[%(numBuckets)s].valueName  = strdup("OutOfRange");
  %(streamPrefix)s_optype_symbols[%(numBucketChannels)s].symbolValues[%(numBuckets)s].valueColor = eslapi::CAPI_COLOR_RED;
""" % { 'streamPrefix': stream.dataPrefix(), "numBucketChannels": numBucketChannels, "numBuckets": channel.numBuckets() }
            for bucket in channel.buckets():
              colorEnum = self.__config.profileBucketMxColor(bucket.color())
              code += """
  %(streamPrefix)s_optype_symbols[%(numBucketChannels)s].symbolValues[%(index)s].valueName  = strdup("%(name)s");
  %(streamPrefix)s_optype_symbols[%(numBucketChannels)s].symbolValues[%(index)s].valueColor = %(colorEnum)s;
""" % { 'streamPrefix': stream.dataPrefix() , "numBucketChannels": numBucketChannels, 'index': bucket.index(),
        'name': bucket.name(), 'colorEnum': colorEnum }
            numBucketChannels = numBucketChannels + 1

        code += """
  streams[%(index)s] = %(compName)s_streams[%(index)s];""" % { 'index': stream.index(), 'compName': self.__config.compName() }
      code += """  
  *actualNrStreams = %(numStreams)s;
  return eslapi::CAPI_STATUS_OK;
}

""" % { "numStreams": len(self.__streams) }
    return code
    

  def instanceDeclaration(self):
    code = ''
    if len(self.__streams) > 0:
      code += """\
  %(compName)s_CAPI *mxpi;
""" % { "compName": self.__config.compName() }

    for stream in self.__streams:
      code += stream.instanceDeclaration()
    return code
  
  def componentConstructor(self):
    code = ''
    if len(self.__streams) > 0:
      code += "  mxpi = NULL;\n"
    return code
  
  def componentInit(self):
    code = ''
    if len(self.__streams) > 0:
      code += """\
  mxpi = new %(compName)s_CAPI(this);
  eslapi::CAPIRegistry::getCAPIRegistry()->CAPIRegisterInterface(mxpi);
""" % { "compName": self.__config.compName() }

    for stream in self.__streams:
      code += stream.componentInit()
    return code
  
  def componentTerminate(self):
    code = ''
    if len(self.__streams) > 0:
      code = "delete mxpi;\n"
    return code

class ProfileStream:
  def __init__(self, cfgStream, config):
    self.__cfgStream = cfgStream
    self.__config = config

  # delegate to Carbon.Cfg object for things we don't define
  def __getattr__(self, name):
    return getattr(self.__cfgStream, name)

  def className(self):
    return '%(compName)s_stream_%(streamName)s' % {
      "compName": self.__config.compName(),
      "streamName": self.__cfgStream.name() }

  # the profile code uses this prefix a lot...
  def dataPrefix(self):
    return '%(compName)s_%(streamName)s'  % {
      "compName": self.__config.compName(),
      "streamName": self.__cfgStream.name() }

  # the name of the component member representing the stream
  def instanceName(self):
    return 'mxpi_stream%d' % self.__cfgStream.index()

  # generate code to update the stream after a call to carbonSchedule
  def update(self):
    return self.instanceName() + '.Update();\n'
    
  def classDeclaration(self):
    code = """
class %(className)s {
public:
   ~%(className)s() { }
    %(className)s() {
      mStream    = NULL;
      mCarbonObj = NULL;
""" % { "className": self.className() }

    for net in self.__cfgStream.nets():
      code += """\
      m%(netName)sID = NULL;
""" % { 'netName': net.name() }

    code += """\
    }
    void Init(eslapi::CAPIStream_t *Stream, CarbonObjectID *CarbonObj) {
      mStream    = Stream;
      mCarbonObj = CarbonObj;
"""

    for net in self.__cfgStream.nets():
      code += """\
      m%(netName)sID = carbonFindNet(mCarbonObj, "%(netPath)s");
""" % { 'netName' : net.name(), 'netPath': net.path() }

    code += """\
    }
    void Update() {
      if (CAPIIsProfilingStreamEnabled(mStream)) {"""
    for net in self.__cfgStream.nets():
      numWords = (net.width() + 31) / 32
      code += """
        CarbonUInt32 %(netName)s_val[%(numWords)s];
        carbonExamine(mCarbonObj, m%(netName)sID, %(netName)s_val, 0);
        %(netName)s = %(netName)s_val[0];
""" % { 'netName' : net.name(), 'numWords': numWords }

    code += """
        Trigger();
      }
    }

    void Trigger() {
      //trigger condition(s) (user specified)"""
    for trigger in self.__cfgStream.triggers():
    
      code += """
      if(%(triggerExpr)s) {
""" % { 'triggerExpr': trigger.expr() }

      for channel in trigger.channels():
        numBuckets = channel.numTotalBuckets()

        # if more than 0 buckets, we compute the value based on the bucket expressions
        if numBuckets > 0:
          code += """
        //channel expressions (user specified)
        uint8_t %(channelName)s_value = %(defaultBucket)s;
""" % { 'channelName': channel.name(), 'defaultBucket': numBuckets }

          firstBucket = True
          for bucket in channel.buckets():
            code += '        '
            if firstBucket:
              firstBucket = False
            else:
              code += 'else '
            code += """if(%(bucketExpr)s) {
          %(channelName)s_value = %(bucketIndex)s;
        }
""" % { 'bucketExpr': bucket.expr(), 'channelName': channel.name(), 'bucketIndex': bucket.index() }

        # no buckets: the value of the channel is the value of its expression
        else:
          code += """\
        uint32_t %(channelName)s_value = %(channelExpr)s;
""" % { "channelName": channel.name(), "channelExpr": channel.expr() }

      # record the result of the channel computation
      # Use CAPIRecordEvent1 or CAPIRecordEvent2 macros for speed if possible.
      code += """
        %s(mStream""" % { 1: "CAPIRecordEvent1",
                          2: "CAPIRecordEvent2" }.get(len(trigger.channels()),
                                                      "eslapi::MxPI::CAPIRecordEvent")
      for channel in trigger.channels():
        code += """,
                         %(channelName)s_value""" % { 'channelName': channel.name() }
      code += """);
      }
"""
    code += """
    } // Trigger
"""

    # ****************************************************
    # All members that can be changed after initialization
    # need to be saved/restored.
    # ***************************************************

    for net in self.__cfgStream.nets():
      code += """
    CarbonNetID *m%(netName)sID;
    ProfSignal   %(netName)s;
""" % { "netName": net.name() }

    code += """
    bool posedge(const ProfSignal& ps) { return (ps.posedge()); }
    bool negedge(const ProfSignal& ps) { return (ps.negedge()); }
    bool changed(const ProfSignal& ps) { return (ps.changed()); }

    bool saveData(eslapi::CASIODataStream &os)
    {
      bool ok = true;
"""
    for net in self.__cfgStream.nets():
      code += """
      ok &= %(netName)s.saveData(os);""" % { "netName": net.name() }
    code += """
      return ok;
    }

    bool restoreData(eslapi::CASIIDataStream &is)
    {
      bool ok = true;
"""
    for net in self.__cfgStream.nets():
      code += """
      ok &= %(netName)s.restoreData(is);""" % { "netName": net.name() }
    code += """
      return ok;
    }

    eslapi::CAPIStream_t   *mStream;
    CarbonObjectID *mCarbonObj;
};
"""
    return code
    
    
  def instanceDeclaration(self):
    return """\
  %(className)s %(instance)s;
""" % { "className": self.className(),
        "instance": self.instanceName() }

  def componentInit(self):
    return """\
  %(instance)s.Init(mxpi->CAPIFindStream("%(streamName)s"), mCarbonObj);  
"""  % { "streamName": self.__cfgStream.name(),
         "instance": self.instanceName() }
