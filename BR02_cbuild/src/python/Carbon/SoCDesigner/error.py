#******************************************************************************
# Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

# These message numbers are duplicated in CarbonCfg.h
carbonCfgMESSAGE_BASE = 13000
carbonCfgMESSAGE_LAST = 13999

carbonCfgERR_NOCARBONSIGNAL = carbonCfgMESSAGE_BASE+0
carbonCfgERR_NOCARBONPORT   = carbonCfgMESSAGE_BASE+1
carbonCfgERR_NOTRANSPORT    = carbonCfgMESSAGE_BASE+2
carbonCfgERR_NEWCARBONPORT  = carbonCfgMESSAGE_BASE+3
carbonCfgERR_PORTCHANGED    = carbonCfgMESSAGE_BASE+4
carbonCfgERR_NOTCARBONNET   = carbonCfgMESSAGE_BASE+5
carbonCfgERR_NOTMEMORY      = carbonCfgMESSAGE_BASE+6
carbonCfgERR_COMPOSITEPORT  = carbonCfgMESSAGE_BASE+7
carbonCfgERR_NODB           = carbonCfgMESSAGE_BASE+8
carbonCfgERR_NOXTORCONN     = carbonCfgMESSAGE_BASE+9
carbonCfgERR_NOFLOWSUPPORT  = carbonCfgMESSAGE_BASE+10
carbonCfgERR_REQRTLCONN     = carbonCfgMESSAGE_BASE+11
carbonCfgERR_UNKNOWNXTOR    = carbonCfgMESSAGE_BASE+12
carbonCfgERR_ILLPARAM       = carbonCfgMESSAGE_BASE+13
carbonCfgERR_NOMXHOME       = carbonCfgMESSAGE_BASE+14

##
# \file
# Classes for throwing exceptions

class InternalError(Exception):
  def __init__(self, message):
    self.__message = message
  def __str__(self):
    return "CARBON INTERNAL ERROR: " + self.__message
  
class ConfigError(Exception):
  def __init__(self, msg_num, message):
    self.__message = message
    self.__msg_num = msg_num
  def __str__(self):
    return "%d: %s" % (self.__msg_num, self.__message)

class RTLConnectionError(ConfigError):
  def __init__(self, xtorName, portName):
    ConfigError.__init__(self, carbonCfgERR_REQRTLCONN, xtorName + '.' + portName + ' port must be connected to an RTL signal')

class UnknownXtorError(ConfigError):
  def __init__(self, xtorType):
    ConfigError.__init__(self, carbonCfgERR_UNKNOWNXTOR, 'instance of unknown transactor type' + xtorType)
  
class XtorParamError(ConfigError):
  def __init__(self, xtorName, xtorInst, paramName):
    ConfigError.__init__(self, carbonCfgERR_ILLPARAM,
                         "%s transactor instance %s must have a value for parameter %s" % (xtorName, xtorInst, paramName))





