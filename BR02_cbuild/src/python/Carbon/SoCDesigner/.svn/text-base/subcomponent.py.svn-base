#******************************************************************************
# Copyright (c) 2008-2013 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************
## NOTE if you want to insert a breakpoint into this file just put the following two stmts at the point you want to break:
##     import pdb; pdb.set_trace()

##
# \file
# Classes that represent the Sub Component implementation of the wrapper.
from Carbon.SoCDesigner import cadi
from Carbon import cpp

class SubComponent:
  def __init__(self, config, comp):
    self.__topConfig = config
    self.__comp = comp
    if config.subComponentFullName():
      self.__compName = config.compName() + '_' + comp.compName()
    else:
      self.__compName = comp.compName()
    self.__className = cpp.LegalNamedObject(self.compName(), config.scope())
    self.__instName = cpp.LegalNamedObject(self.compName() + '_inst', config.scope())
    self.__mxdi = cadi.MxDI(False, self.className(), config, comp.debugRegs(), comp.debugMemories(), comp.procInfo(), comp.cadi(), comp.elfLoader())

    # If they asked for the debuggable point functions, create the helper class
    if self.useDebuggablePoint():
      self.__debuggablePoint = DebuggablePoint(self.compName())
    else:
      self.__debuggablePoint = None

  def parentConfig(self):
    return self.__topConfig
  
  def getComp(self):
    return self.__comp

  def compName(self):
    return self.__compName

  # displayName is most of the time same as compName, unless we are doing an ARM processor
  # that requires a specific name
  def displayName(self):
    if self.__comp.compDisplayName() == "":
      return self.__comp.compName()
    else:
      return self.__comp.compDisplayName()
  
  def getCustomCodePre(self, section):
    return self.__comp.getCustomCodePre(section)
  
  def getCustomCodePost(self, section):
    return self.__comp.getCustomCodePost(section)

  def genObtainInterfaceDecl(self):
    return 'eslapi::CAInterface* ObtainInterface(eslapi::if_name_t ifName,eslapi::if_rev_t minRev, eslapi::if_rev_t* actualRev);'

  def genObtainInterfaceImpl(self):
    code = ''
    warmCacheCode = ''
	# If using legacy memories don't add the caching warming code because the model
	# will not compile.
    if self.__comp.useLegacyMemories() == 0 :
      warmCacheCode = '''\
#if (MAXSIM_MAJOR_VERSION == 7 && MAXSIM_MINOR_VERSION > 13) || MAXSIM_MAJOR_VERSION > 7
  if (strcmp(ifName,WarmCacheIF::IFNAME()) == 0 && minRev <= WarmCacheIF::IFREVISION()) {
    *actualRev = WarmCacheIF::IFREVISION();
    return carbonMems[0]->getWarmCacheIF();
  }
#endif
'''
    accessor = self.__comp.qualifiedPCTraceAccessor()
    if accessor != "":
      code = '''\
eslapi::CAInterface* %(mod_name)s::ObtainInterface(eslapi::if_name_t ifName,
    eslapi::if_rev_t minRev,
    eslapi::if_rev_t* actualRev)
{
  if ((strcmp(ifName, PCTraceProviderIF::IFNAME()) == 0) &&
      (minRev <= PCTraceProviderIF::IFREVISION())) {
    *actualRev = PCTraceProviderIF::IFREVISION();
    return dynamic_cast<SoCDPCTraceReporter*>(%(accessor)s);
  }
  %(warm_cache_code)s
  return CASIModule::ObtainInterface(ifName, minRev, actualRev);
}
''' % {
        'mod_name': self.className(),
		'warm_cache_code': warmCacheCode,
        'accessor': accessor
      }
    else :
      code = '''\
eslapi::CAInterface* %(mod_name)s::ObtainInterface(eslapi::if_name_t ifName,
    eslapi::if_rev_t minRev,
    eslapi::if_rev_t* actualRev)
{
  %(warm_cache_code)s
  return CASIModule::ObtainInterface(ifName, minRev, actualRev);
}
''' % {
        'mod_name': self.className(),
		'warm_cache_code': warmCacheCode
      }
    return code

  def className(self):
    return self.__className.name()
  
  def instanceName(self):
    return self.__instName.name()
  
  def mxdi(self):
    return self.__mxdi
  
  def debuggerProperty(self):
    # For ARM Processor model set the debugger property
    code = ''
    if (self.__comp.procInfo().isProcessor()):
      code += '''\
    case eslapi::CASI_PROP_CADI_DEBUGGER:
      return "%(debugger)s";
    case eslapi::CASI_PROP_EXECUTES_SOFTWARE:
      return "yes";
''' % {'debugger': self.__comp.procInfo().debuggerName()}
    return code

  def debuggablePointProperty(self):
    # Check if the proc info debuggable point tag is present.
    # If so, add the 1234 property
    code = ''
    if self.__comp.procInfo().debuggablePoint():
      code += '''\
  case 1234: // debuggable point feature
     return "yes";
'''
    return code

  def debuggablePointSetParameter(self):
    # Check if the proc info debuggable point tag is present.
    # If so, add the call to procStopAtDebuggablePoint
    code = ''
    if self.__comp.procInfo().debuggablePoint():
      code += '''\
  if (name == "stalledstop: prepare_to_stop") {
    procStopAtDebuggablePoint(true);
    return;
  } 
  else if( name == "stalledstop: resume_exec" ) {
    procStopAtDebuggablePoint(false);
    return;
  } 
'''
    return code

  def debuggablePointGetParameter(self):
    # Check if the proc info debuggable point tag is present.
    # If so, add the call to procStopAtDebuggablePoint
    code = ''
    if self.__comp.procInfo().debuggablePoint():
      code += '''\
  if (key == "stalledstop: can_you_stop") {
    if (procCanStop()) {
      return "yes";
    } else {
      return "no";
    }
  }
'''
    return code

  def generateSRInterfaceDecl(self):
    if self.parentConfig().supportsArchSaveRestore():
      code = """\
  // implementation of CASISaveRestoreArch interface methods
  virtual eslapi::CASISaveRestoreArchCallbackIF* getCB();
  virtual eslapi::CASISaveRestoreStreamType saveStreamType();
  virtual bool saveDataBinary( eslapi::CASIODataStream &data );
  virtual bool restoreDataBinary( eslapi::CASIIDataStream &data );
  virtual bool saveDataArch( eslapi::CASIODataStream &data );
  virtual bool restoreDataArch( eslapi::CASIIDataStream &data );"""
    else:
      code = """\
  // implementation of CASISaveRestore interface methods
  virtual bool saveData( eslapi::CASIODataStream &data );
  virtual bool restoreData( eslapi::CASIIDataStream &data );"""
    return code

  def generateSRArchMembers(self):
    code = ''
    if self.parentConfig().supportsArchSaveRestore():
      code = """\
  CarbonSaveRestoreArchCallback* mCB;
  bool mArchitecturalSave;
  bool mValidateArchitecturalRestore;"""
    return code

  def generateSRArchMemberInit(self):
    code = ''
    if self.parentConfig().supportsArchSaveRestore():
      code = """\
  mCB = NULL;
  mArchitecturalSave = false;
  mValidateArchitecturalRestore = false;"""
    return code

  def generateSRArchMemberDestroy(self):
    code = ''
    if self.parentConfig().supportsArchSaveRestore():
      code = """\
  delete mCB;"""
    return code

  def generateSetParameterArchSave(self):
    # These parameters only exists for models that support architectural
    # save/restore.  It allows architectural save to be used for CA
    # models instead of the default binary save, and also for validation to
    # occur with the architectural restore.
    code = ''
    if self.parentConfig().supportsArchSaveRestore():
      code = """\
  if (name == "Architectural Save") {
    status = CASIConvertStringToValue(value, &mArchitecturalSave);
    if (status == eslapi::CASIConvert_SUCCESS) {
      return;
    }
  }
  if (name == "Validate Architectural Restore") {
    status = CASIConvertStringToValue(value, &mValidateArchitecturalRestore);
    if (status == eslapi::CASIConvert_SUCCESS) {
      return;
    }
  }"""
    return code

  def generateGetParameterArchSave(self):
    # Intercept the hidden parameters
    code = ''
    if self.parentConfig().supportsArchSaveRestore():
      code = """\
  if (key == "Architectural Save") {
    return mArchitecturalSave ? "true" : "false";
  }
  if (key == "Validate Architectural Restore") {
    return mValidateArchitecturalRestore ? "true" : "false";
  }"""
    return code

  def getCASIModuleConstrSignature(self):
    if self.__topConfig.hasSystemCPorts():
      return "s.c_str(), c"
    else:
      return "c, s"

  def generateSRInterfaceImpl(self, userCode):
    module_name = self.className()
    if self.parentConfig().supportsArchSaveRestore():
      code = """\
eslapi::CASISaveRestoreArchCallbackIF* %(mod_name)s::getCB()
{
%(userCodePreGetCB)s
%(customCodePreGetCB)s
  if (mCB != NULL) {
    delete mCB;
  }
  std::list<eslapi::CASIMemoryRange> empty;
  mCB = new CarbonSaveRestoreArchCallback(getCADI(), empty, this, getParameter("Validate Architectural Restore") == "true");
%(userCodePostGetCB)s
%(customCodePostGetCB)s
  return mCB;
}

eslapi::CASISaveRestoreStreamType %(mod_name)s::saveStreamType()
{
%(userCodePreSaveStreamType)s
%(customCodePreSaveStreamType)s
  eslapi::CASISaveRestoreStreamType type = (getParameter("Architectural Save") == "true") ? eslapi::ArchStream : eslapi::BinaryStream;
%(userCodePostSaveStreamType)s
%(customCodePostSaveStreamType)s
  return type;
}

bool %(mod_name)s::saveDataBinary( eslapi::CASIODataStream &stream )
{
  bool ok = true;
%(userCodePreSaveDataBinary)s
%(customCodePreSaveDataBinary)s
  // No state to save in subcomponent by default
%(userCodePostSaveDataBinary)s
%(customCodePostSaveDataBinary)s
  return ok;
}

bool %(mod_name)s::restoreDataBinary( eslapi::CASIIDataStream &stream )
{
  bool ok = true;
%(userCodePreRestoreDataBinary)s
%(customCodePreRestoreDataBinary)s
  // No state to restore in subcomponent by default
%(userCodePostRestoreDataBinary)s
%(customCodePostRestoreDataBinary)s
  return ok;
}

bool %(mod_name)s::saveDataArch( eslapi::CASIODataStream &stream )
{
  bool ok = true;
%(userCodePreSaveDataArch)s
%(customCodePreSaveDataArch)s
  // Defer to parent implementation.
  ok = eslapi::CASISaveRestoreArch::saveDataArch(stream);
%(userCodePostSaveDataArch)s
%(customCodePostSaveDataArch)s
  return ok;
}

bool %(mod_name)s::restoreDataArch( eslapi::CASIIDataStream &stream )
{
  bool ok = true;
%(userCodePreRestoreDataArch)s
%(customCodePreRestoreDataArch)s
  // Defer to parent implementation.
  ok = eslapi::CASISaveRestoreArch::restoreDataArch(stream);
%(userCodePostRestoreDataArch)s
%(customCodePostRestoreDataArch)s
  return ok;
}""" % {
    'mod_name': module_name,
    "userCodePreGetCB" : userCode.preSection(module_name + "::getCB"),
    "userCodePostGetCB" : userCode.postSection(module_name + "::getCB"),
    "customCodePreGetCB" : self.getCustomCodePre("getCB"),
    "customCodePostGetCB" : self.getCustomCodePost("getCB"),
    "userCodePreSaveStreamType" : userCode.preSection(module_name + "::saveStreamType"),
    "userCodePostSaveStreamType" : userCode.postSection(module_name + "::saveStreamType"),
    "customCodePreSaveStreamType" : self.getCustomCodePre("saveStreamType"),
    "customCodePostSaveStreamType" : self.getCustomCodePost("saveStreamType"),
    'userCodePreSaveDataBinary': userCode.preSection('%s::saveDataBinary' % module_name),
    'userCodePostSaveDataBinary': userCode.postSection('%s::saveDataBinary' % module_name),
    'customCodePreSaveDataBinary': self.getCustomCodePre("saveDataBinary"),
    'customCodePostSaveDataBinary': self.getCustomCodePost("saveDataBinary"),
    'userCodePreRestoreDataBinary': userCode.preSection('%s::restoreDataBinary' % module_name),
    'userCodePostRestoreDataBinary': userCode.postSection('%s::restoreDataBinary' % module_name),
    'customCodePreRestoreDataBinary': self.getCustomCodePre("restoreDataBinary"),
    'customCodePostRestoreDataBinary': self.getCustomCodePost("restoreDataBinary"),
    'userCodePreSaveDataArch': userCode.preSection('%s::saveDataArch' % module_name),
    'userCodePostSaveDataArch': userCode.postSection('%s::saveDataArch' % module_name),
    'customCodePreSaveDataArch': self.getCustomCodePre("saveDataArch"),
    'customCodePostSaveDataArch': self.getCustomCodePost("saveDataArch"),
    'userCodePreRestoreDataArch': userCode.preSection('%s::restoreDataArch' % module_name),
    'userCodePostRestoreDataArch': userCode.postSection('%s::restoreDataArch' % module_name),
    'customCodePreRestoreDataArch': self.getCustomCodePre("restoreDataArch"),
    'customCodePostRestoreDataArch': self.getCustomCodePost("restoreDataArch")
    }
    else:
      code = """\
bool %(mod_name)s::saveData( eslapi::CASIODataStream &stream )
{
  bool ok = true;
%(userCodePreSaveData)s
%(customCodePreSaveData)s
  // No state to save in subcomponent by default
%(userCodePostSaveData)s
%(customCodePostSaveData)s
  return ok;
}

bool %(mod_name)s::restoreData( eslapi::CASIIDataStream &stream )
{
  bool ok = true;
%(userCodePreRestoreData)s
%(customCodePreRestoreData)s
  // No state to restore in subcomponent by default
%(userCodePostRestoreData)s
%(customCodePostRestoreData)s
  return ok;
}
""" % {
    'mod_name': module_name,
    'userCodePreSaveData': userCode.preSection('%s::saveData' % module_name),
    'userCodePostSaveData': userCode.postSection('%s::saveData' % module_name),
    'customCodePreSaveData': self.getCustomCodePre("saveData"),
    'customCodePostSaveData': self.getCustomCodePost("saveData"),
    'userCodePreRestoreData': userCode.preSection('%s::restoreData' % module_name),
    'userCodePostRestoreData': userCode.postSection('%s::restoreData' % module_name),
    'customCodePreRestoreData': self.getCustomCodePre("restoreData"),
    'customCodePostRestoreData': self.getCustomCodePost("restoreData")
    }
    return code

  def emitDefinition(self, userCode):
    subCompName = self.className()
    # If we support architectural save and restore, we need to use the new interface
    srClass = 'eslapi::CASISaveRestore'
    if self.parentConfig().supportsArchSaveRestore():
      srClass = 'eslapi::CASISaveRestoreArch'
    code = ""
    code += """
// sub-component class - %(subCompName)s
class %(subCompName)s: public eslapi::CASIModule, public %(srClass)s
{
%(userCodePreDef)s
%(customCodePreDef)s

%(mxdi_friend_def)s
public:
  // constructor and destructor
  %(subCompName)s(%(parentCompName)s* c, const string &s);
  virtual ~%(subCompName)s();

  %(genObtainInterfaceDecl)s
  // overloaded methods
  void communicate();
  void interconnect();
  void subUpdate(int type);

%(srInterfaceDecl)s

  //overloaded CASIModule methods
  string getName();
  void setParameter(const string &name, const string &value);
  string getParameter(const string &name);
  string getProperty( eslapi::CASIPropertyType property );
  void init(CarbonObjectID* carbonObj);
  void terminate();
  void reset(eslapi::CASIResetLevel level, const eslapi::CASIFileMapIF *filelist);
  CADI* getCADI();
  CarbonObjectID* getCarbonObject();
  %(parentCompName)s* getParentComp();
  %(topCompName)s* getTopComp();

private:
%(debuggablePointFnDefs)s

  bool initComplete;
  bool p_enableDbgMsg;
  %(subCompName)s_CADI* mxdi;

  //carbon variables
  CarbonObjectID * mCarbonObj;
  %(parentCompName)s* mParent;
%(srArchMembers)s
%(carbonMemsDef)s
  CarbonDebugRegister** carbonDebugRegs;

%(memory_inst_def)s

%(customCodePostDef)s
%(userCodePostDef)s
};
"""
    return code % { 'subCompName' : subCompName,
                    'srClass': srClass,
                    'topCompName' : self.__topConfig.compName(),
                    'parentCompName' : self.__topConfig.compName(),
                    "genObtainInterfaceDecl" : self.genObtainInterfaceDecl(),
                    'userCodePreDef': userCode.preSection(subCompName + ' CLASS DEFINITION'),
                    "mxdi_friend_def": self.mxdi().generate_mxdi_friend_def(),
                    'memory_inst_def' : self.mxdi().generate_memory_inst_def(),
                    'carbonMemsDef': self.carbonMemsDef(),
                    'userCodePostDef': userCode.postSection(subCompName + ' CLASS DEFINITION'),
                    'customCodePreDef' : self.getCustomCodePre("class"),
                    'customCodePostDef' : self.getCustomCodePost("class"),
                    'debuggablePointFnDefs' : self.debuggablePointFnDefs(userCode),
                    'srInterfaceDecl': self.generateSRInterfaceDecl(),
                    'srArchMembers': self.generateSRArchMembers()
                    }

  def emitImplementation(self, userCode):
    mod_name = self.className()
    code = ""

    code += """
%(customCodePreCppInclude)s
#if (MAXSIM_MAJOR_VERSION == 7 && MAXSIM_MINOR_VERSION > 13) | MAXSIM_MAJOR_VERSION > 7
#include "WarmCacheIF.h"
using namespace Carbon::WarmCache;
#endif
%(customCodePostCppInclude)s
%(mod_name)s::%(mod_name)s(%(parentCompName)s *c, const string &s) : eslapi::CASIModule(%(CASIModuleConstrSignature)s)
{
%(userCodePreConstructor)s
%(customCodePreConstructor)s
  initComplete = false;
  mParent = c;
  mxdi = NULL;
%(srArchMemberInit)s

%(customCodePostConstructor)s
%(userCodePostConstructor)s
}

%(mod_name)s::~%(mod_name)s()
{
%(userCodePreDestructor)s
%(customCodePreDestructor)s
%(srArchMemberDestroy)s
%(customCodePostDestructor)s
%(userCodePostDestructor)s
}

%(genObtainInterfaceImpl)s

void %(mod_name)s::communicate()
{
%(userCodePreCommunicate)s
%(customCodePreCommunicate)s
%(customCodePostCommunicate)s
%(userCodePostCommunicate)s
}

void %(mod_name)s::subUpdate(int type)
{
  if (type == 0) { // Pre Update
%(userCodePreUpdate)s
%(customCodePreUpdate)s
}
  else { // Post Update
%(customCodePostUpdate)s
%(userCodePostUpdate)s
  }
}

void %(mod_name)s::interconnect()
{
%(userCodePreInterconnect)s
%(customCodePreInterconnect)s

%(userCodePostInterconnect)s
%(customCodePostInterconnect)s
}

void %(mod_name)s::init(CarbonObjectID* carbonObj)
{
%(userCodePreInit)s
%(customCodePreInit)s
  carbonMems = 0;
  mCarbonObj = carbonObj;

  // get the carbon net id for all nets
""" % { "mod_name" : mod_name,
        "parentCompName" : self.__topConfig.compName(),
        "userCodePreConstructor" : userCode.preSection("%s::%s" % (mod_name, mod_name)),
        "userCodePostConstructor" : userCode.postSection("%s::%s" % (mod_name, mod_name)),
        "userCodePreDestructor" : userCode.preSection("%s::~%s" % (mod_name, mod_name)),
        "userCodePostDestructor" : userCode.postSection("%s::~%s" % (mod_name, mod_name)),
        "userCodePreCommunicate" : userCode.preSection(mod_name + "::communicate"),
        "userCodePostCommunicate" : userCode.postSection(mod_name + "::communicate"),
        "userCodePreInterconnect" : userCode.preSection(mod_name + "::interconnect"),
        "userCodePostInterconnect" : userCode.postSection(mod_name + "::interconnect"),
        "userCodePreUpdate" : userCode.preSection(mod_name + "::update"),
        "userCodePostUpdate" : userCode.postSection(mod_name + "::update"),
        "userCodePreInit" : userCode.preSection(mod_name + "::init"),
        "customCodePreCppInclude" : self.getCustomCodePre("cpp-include"),
        "customCodePostCppInclude" : self.getCustomCodePost("cpp-include"),
        "customCodePreConstructor" : self.getCustomCodePre("constructor"),
        "customCodePostConstructor" : self.getCustomCodePost("constructor"),
        "customCodePreDestructor" : self.getCustomCodePre("destructor"),
        "customCodePostDestructor" : self.getCustomCodePost("destructor"),
        "genObtainInterfaceImpl" : self.genObtainInterfaceImpl(),
        "customCodePreCommunicate" : self.getCustomCodePre("communicate"),
        "customCodePostCommunicate" : self.getCustomCodePost("communicate"),
        "customCodePreInterconnect" : self.getCustomCodePre("interconnect"),
        "customCodePostInterconnect" : self.getCustomCodePost("interconnect"),
        "customCodePreUpdate" : self.getCustomCodePre("update"),
        "customCodePostUpdate" : self.getCustomCodePost("update"),
        "customCodePreInit" : self.getCustomCodePre("init"),
        "srArchMemberInit": self.generateSRArchMemberInit(),
        "srArchMemberDestroy": self.generateSRArchMemberDestroy(),
        "CASIModuleConstrSignature": self.getCASIModuleConstrSignature()
      }

    code += self.__mxdi.componentInit()
    
    code += """
%(customCodePostInit)s
%(userCodePostInit)s

  // Init save/restore
  initStream(this);

  // Call the base class after this has been initialized.
  eslapi::CASIModule::init();

  // Set a flag that init stage has been completed
  initComplete = true;
}

void %(mod_name)s::reset(eslapi::CASIResetLevel level, const eslapi::CASIFileMapIF *filelist)
{
%(userCodePreReset)s
%(customCodePreReset)s

  // apply the reset sequence to the disaassembly memory Carbon Model
%(generate_disassembly_reset_src)s

%(customCodePostReset)s
%(userCodePostReset)s

  // Call the base class after this has been reset.
  eslapi::CASIModule::reset(level, filelist);  
}

void %(mod_name)s::terminate()
{
  // Call the base class first.
  eslapi::CASIModule::terminate();

%(userCodePreTerminate)s
%(customCodePreTerminate)s

%(disassembly_terminate_debug_id_src_def)s

%(customCodePostTerminate)s
%(userCodePostTerminate)s
}

void %(mod_name)s::setParameter(const string &name, const string &value)
{
%(userCodePreSetParameter)s
%(customCodePreSetParameter)s
  eslapi::CASIConvertErrorCodes status = eslapi::CASIConvert_SUCCESS;
%(debuggablePointSetParameter)s
%(setParameterArchSave)s
  if(name == "Enable Debug Messages")
    {
      status = CASIConvertStringToValue(value, &p_enableDbgMsg);
    }

  if(status == eslapi::CASIConvert_SUCCESS) {
    eslapi::CASIModule::setParameter(name, value);
  } else {
    message( eslapi::CASI_MSG_WARNING, "%(mod_name)s::setParameter: Illegal value passed for parameter. Assignment ignored.");
  }
%(customCodePostSetParameter)s
%(userCodePostSetParameter)s
}

string %(mod_name)s::getParameter(const string &key)
{
%(userCodePreGetParameter)s
%(customCodePreGetParameter)s
%(debuggablePointGetParameter)s
%(getParameterArchSave)s
  // Use the base classes getParameter by default
  string result = eslapi::CASIModule::getParameter(key);

%(customCodePostGetParameter)s
%(userCodePostGetParameter)s
  return result;
}


string %(mod_name)s::getProperty( eslapi::CASIPropertyType property )
{
%(userCodePreGetProperty)s
%(customCodePreGetProperty)s

  string description; 
  switch (property) {    
    case eslapi::CASI_PROP_LOADFILE_EXTENSION:
      return "%(loadfileext)s";
    case eslapi::CASI_PROP_REPORT_FILE_EXT:
      return "yes";
    case eslapi::CASI_PROP_COMPONENT_TYPE:
      return "%(type)s"; 
    case eslapi::CASI_PROP_COMPONENT_VERSION:
      return "0.1";
    case eslapi::CASI_PROP_MSG_PREPEND_NAME:
      return "yes"; 
    case eslapi::CASI_PROP_DESCRIPTION:
      description = "carbon based SoC Designer component for %(mod_name)s";
      return description + " Compiled on " + __DATE__ + ", " + __TIME__; 
    case eslapi::CASI_PROP_CADI_SUPPORT:
      return "%(mxdi_support_status)s";
    case eslapi::CASI_PROP_SAVE_RESTORE:
      return "yes";
%(debuggerProperty)s
%(debuggablePointProperty)s

    default:
      return "";
  }
%(customCodePostGetProperty)s  
%(userCodePostGetProperty)s  
  return "";

}

string %(mod_name)s::getName(void)
{
  return "%(display_name)s";
}

CADI* %(mod_name)s::getCADI()
{
  return mxdi;
}

CarbonObjectID* %(mod_name)s::getCarbonObject()
{
  return mCarbonObj;
}

%(parentCompName)s* %(mod_name)s::getParentComp()
{
  return mParent;
}

%(topCompName)s* %(mod_name)s::getTopComp()
{
  return mParent->getTopComp();
}

%(srInterfaceImpl)s
%(debuggablePointFnImpl)s
""" % { "mod_name" : mod_name,
        "parentCompName" : self.__topConfig.compName(),
        "topCompName" : self.__topConfig.compName(),
        "display_name" : self.displayName(),
        "type": self.__comp.typeName(),
        "userCodePostInit" : userCode.postSection(mod_name + "::init"),
        "userCodePreReset" : userCode.preSection(mod_name + "::reset"),
        "generate_disassembly_reset_src" : self.__mxdi.componentReset(),
        "userCodePostReset" : userCode.preSection(mod_name + "::reset"),
        "userCodePreTerminate" : userCode.preSection(mod_name + "::terminate"),
        "disassembly_terminate_debug_id_src_def" : self.__mxdi.componentTerminate(),                    
        "disassembly_cleanup_debug_id_src_def": self.__mxdi.componentTerminate(),
        "userCodePostTerminate" : userCode.postSection(mod_name + "::terminate"),
        "customCodePreTerminate" : self.getCustomCodePre("terminate"),
        "customCodePostTerminate" : self.getCustomCodePost("terminate"),
        "mxdi_support_status": self.__mxdi.mxdi_support_status_def(),
        "userCodePreGetProperty" : userCode.preSection(mod_name + "::getProperty"),
        "userCodePostGetProperty" : userCode.postSection(mod_name + "::getProperty"),
        "customCodePreGetProperty" : self.getCustomCodePre("getProperty"),
        "customCodePostGetProperty" : self.getCustomCodePost("getProperty"),
        "debuggerProperty" : self.debuggerProperty(),
        'debuggablePointProperty': self.debuggablePointProperty(),
        "loadfileext" : self.__comp.loadfileExtension(),
        "userCodePreSetParameter" : userCode.preSection(mod_name + "::setParameter"),
        "userCodePostSetParameter" : userCode.postSection(mod_name + "::setParameter"),
        "customCodePreSetParameter" : self.getCustomCodePre("setParameter"),
        "customCodePostSetParameter" : self.getCustomCodePost("setParameter"),
        "userCodePreGetParameter" : userCode.preSection(mod_name + "::getParameter"),
        "userCodePostGetParameter" : userCode.postSection(mod_name + "::getParameter"),
        "customCodePreGetParameter" : self.getCustomCodePre("getParameter"),
        "customCodePostGetParameter" : self.getCustomCodePost("getParameter"),
        "customCodePostInit" : self.getCustomCodePost("init"),
        "customCodePreReset" : self.getCustomCodePre("reset"),
        "customCodePostReset" : self.getCustomCodePost("reset"),
        "debuggablePointSetParameter" : self.debuggablePointSetParameter(),
        "debuggablePointGetParameter" : self.debuggablePointGetParameter(),
        "debuggablePointFnImpl" : self.debuggablePointFnImpl(userCode),
        "setParameterArchSave": self.generateSetParameterArchSave(),
        "getParameterArchSave": self.generateGetParameterArchSave(),
        "srInterfaceImpl": self.generateSRInterfaceImpl(userCode)
        }
  
    return code

  def carbonMemsDef(self):
    code = ''
    if(self.mxdi().useLegacyMemories()):
      code = '  CarbonMemoryID** carbonMems;'
    else:
      code = '  CarbonDebugMemory** carbonMems;'
    return code

  # Print unused custom codes
  def printUnusedCodes(self, output):
    self.__comp.printUnusedCodes(output)

  def useDebuggablePoint(self):
    return self.__comp.useDebuggablePoint()

  def debuggablePointFnDefs(self, userCode):
    code = ''
    if self.useDebuggablePoint():
      code += self.__debuggablePoint.emitMembers('private', '  ', userCode, self)
    return code

  def debuggablePointFnImpl(self, userCode):
    code = ''
    if self.useDebuggablePoint():
      code = self.__debuggablePoint.emitImplementation(userCode, self)
    return code

  def addDebuggablePointMemberFunctions(self, classDef):
    if self.useDebuggablePoint():
      self.__debuggablePoint.addMemberFunctions(classDef)

# Class to manage functions to support debuggable point
class DebuggablePoint(cpp.Class):
  def __init__(self, className):
    cpp.Class.__init__(self, className, [])
    self.addMemberFunctions(self)

  # Add the members to the passed in cpp class defintion
  def addMemberFunctions(self, classDef):
    # Add the member functions
    classDef.addMemberFunction('void', 'procStopAtDebuggablePoint', '(bool stop)', access = 'private')
    func = classDef.addMemberFunction('bool', 'procCanStop', '()', access = 'private')
    func.addCode('return false;\n')
