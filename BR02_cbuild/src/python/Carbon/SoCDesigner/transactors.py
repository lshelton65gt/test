#******************************************************************************
# Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

# $Revision: 1.109 $
##
# \file
# Classes that represent SoC Designer signal and transaction ports

import re

import parameters
import error
import Carbon.Cfg
from Carbon import cpp
from Carbon.carboncfg import *


def classComment(text):
  return '/' * 79 + '\n// ' + text + '\n' + '/' * 79

class Member:
  def __init__(self, name):
    self.__attrs = {}
    self.__attrs['name'] = name

  def __getitem__(self, key):
    return self.__attrs.get(key, '')

  def __setitem__(self, key, code):
    self.__attrs[key] = code

class MemberList:
  def __init__(self):
    self.__members = []

  def append(self, member):
    self.__members.append(member)

  def code(self, key, indent='  '):
    lines = []
    for member in self.__members:
      code = member[key]
      if code != '':
        lines.append(indent + code)
    code = ''
    if len(lines) > 0:
      code = '\n'.join(lines)
    return code

class MaxSimPort:
  def __init__(self, compName, name, width, config, expr, tieParam = ''):
    self.__compName = compName
    self.__name = name
    self.__width = width
    self.__config = config
    self.__members = MemberList()
    self.__expr = expr
    self.__tieParam = tieParam

  # Override this function to modify the default port width
  def getPortWidth(self, port):
    return port.width()

  def members(self): return self.__members
  def compName(self): return self.__compName
  def name(self): return self.__name
  def width(self): return self.__width
  def numWords(self): return (self.__width + 31) / 32
  def config(self): return self.__config
  def expr(self): return self.__expr
  def tieParam(self): return self.__tieParam
  def componentInterconnect(self):
    return ''
  def componentFlowThruSetup(self):
    return ''
  def componentInit(self):
    return ''
  def componentTerminate(self):
    return ''
  def componentReset(self):
    return ''
  def componentEndOfReset(self):
    return ''
  def componentUpdate(self):
    return ''
  def componentCommunicate(self):
    return ''
  def componentFlowThruCommunicateDecl(self):
    return ''
  def componentFlowThruCommunicateImpl(self, userCode):
    return ''
  def friendPortDef(self):
    # If SystemC ports are used, port declarations are put into a
    # namespace.  We need to use that in the friend declaration.
    prefix = ''
    if self.__config.hasSystemCPorts():
      prefix = '%s::' % self.__config.namespaceName()
    return "  friend class %s%s;\n" % (prefix, self.className())
  def classDefinition(self, userCode):
    return ''
  def classImplementation(self, userCode):
    return ''
  def componentConstructor(self):
    return ''
  def componentDestructor(self):
    return """\
  delete %(instanceName)s;
""" % { 'instanceName': self.instanceName() }
  def libPaths(self):
    return []
  def incPaths(self):
    return []
  def libDepends(self):
    return []
  def libWinDepends(self):
    return []
  def libWinDebugDepends(self):
    return []
  def hasCycleClock(self):
    return False
  def isFlowThru(self):
    return False;
  def isTransactionMaster(self):
    return False;
  def isTransactionSlave(self):
    return False;
  # currently event queues are only used for xactors that have driveNotify, but this should be made more general so that we can more selectively use event queues
  def useXactorEventQueues(self):
    return self.hasDriveNotify()
  def driveSignal(self):
    return ''
  def componentClockControl(self):
    return ''
  def supportFlowThru(self):
    return False;
  def checkFlowThru(self):
    return
  def generateCallbackMembers(self):
    return ''
  def hasDriveNotify(self):
    return False
  def drivesXtors(self):
    return False
  def componentSendDrive(self):
    return ''
  def componentSendNotify(self):
    return ''
  def componentDriveAllSignals(self):
    return ''
  def forwardValid(self):
    return ''
  def forwardReady(self):
    return ''
  def backwardValid(self):
    return ''
  def backwardReady(self):
    return ''
  def forwardDebugTrans(self):
    return ''
  def isReset(self):
    return False
  # Does this object need to be saved/restored?
  def needsSaveRestore(self):
    return False
  # I can't find a cleaner way to return an iterator of an empty list
  def loopValueChangeMembers(self):
    for x in []:
      yield x
  # Is this a CarbonClockDefinition object, i.e. can it be in the simulation queue?
  def isClockDefinition(self):
    return False
  def clockDefinitionInstance(self):
    return None
  
# Translates the port expression by replacing the name with the
# given replacement. It returns a new string
def gTranslateExpr(obj, repl):
  # Create a regular expression to match the identifier. It has to be
  # <non-ident-chars><obj.name()><non-ident-chars>
  regexpr = "([^a-zA-Z0-9_]|^)" + obj.name() + "([^a-zA-Z0-9_]|$)"
  prog = re.compile(regexpr)
  return prog.sub("\\1" + repl + "\\2", obj.expr())


# helper function to generate output port expressions.  This is shared between
# MaxSimSignalMaster::generatePortExpr and ...
#
# obj must provide name() and expr(), which return strings, and numWords(),
# returning a number
def gGeneratePortExpr(obj, value32, value64):
  code = ""
  if len(obj.expr()) > 0:
    if obj.numWords() == 1:
      code += "  " + value32 + " = " + gTranslateExpr(obj, value32)
      code += "; // Apply port expression"
    elif obj.numWords() == 2:
      code += "\n  // Apply port expression\n"
      code += "  uint64_t value64 = (((uint64_t)" + value64 + "[1]) << 32) | ((uint64_t)" + value64 + "[0]);\n"
      code += "  value64 = " + gTranslateExpr(obj, "value64") + ";\n"
      code += "  " + value64 + "[0] = value64 & 0xFFFFFFFF;\n"
      code += "  " + value64 + "[1] = (value64 >> 32) & 0xFFFFFFFF;\n"
    else:
      code += '#warning "Port expression on >64-bit port ignored: '
      code += gTranslateExpr(obj, value32) + '"'
  return code

                            
class MaxSimSignalMaster(MaxSimPort):
  def __init__(self, config, name, width, carbonName, netId, expr):
    MaxSimPort.__init__(self, config.compName(), name, width, config, expr)
    self.__netId = netId
    self.__carbonName = carbonName
  def carbonName(self):
    return self.__carbonName
  def carbonNetId(self):
    return self.__netId
  def className(self):
    return self.name() + '_SM'
  def instanceName(self):
    return self.name() + '_SMaster'
  def isFlowThru(self):
    return self.config().isFlowThruOutput(self.__carbonName)
  def needsSaveRestore(self):
    return True
  def instanceDeclaration(self):
    return """
  // port %(name)s
  %(className)s* %(instanceName)s;
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className() }
  def componentInit(self):
    return "  %(instanceName)s->init();\n" % { 'instanceName': self.instanceName() }

  def componentReset(self):
    # mark signal as changed so we will drive it after reset
    return "  %(instanceName)s->reset();\n" % { 'instanceName': self.instanceName() }

  def componentConstructor(self):
    return """
  %(instanceName)s = new %(className)s(this);
""" % { 'className': self.className(), 'instanceName': self.instanceName() }

  def componentDestructor(self):
    return """\
  delete %(instanceName)s;
""" % { 'instanceName': self.instanceName() }

  def componentFlowThruSetup(self):
    code = ''
    if self.isFlowThru() :
      code += '  componentMaster->registerCommunicate(this,&%s::%sCommunicate,"%s");\n' % (self.compName(), self.instanceName(), self.name())
      code += '  componentMaster->addDependency(this,"flowThruCommunicate","%s");\n' % self.name()
      code += '  componentMaster->addDependency(this,"%s",%s->getPort());\n' % (self.name(), self.instanceName())
    else :
      # For Ports that are not flow-through and hence does not have it's own communicate function,
      # add a dependency from the regular communicate to output port so that flow-through components
      # that connect to this port will work properly
      code += '#if MAXSIM_MAJOR_VERSION == 7 && MAXSIM_MINOR_VERSION == 0\n  cm70->addDependency(this,"communicate",%s->getPort());\n#endif\n' % (self.instanceName()) 
    return code

  def componentFlowThruCommunicateDecl(self) :
    code = ''
    if self.isFlowThru() :
      code = '  void %sCommunicate();\n' % self.instanceName()
    return code
  
  def componentFlowThruCommunicateImpl(self, userCode) :
    code = ''
    if self.isFlowThru() :
      code = """
void %(compName)s::%(portName)sCommunicate()
{
%(userCodePre)s
%(driveSignal)s
%(userCodePost)s
}
""" % { 'compName' : self.compName(),
        'portName' : self.instanceName(),
        'driveSignal' : self.driveSignal(),
        'userCodePre': userCode.preSection('%s::%sCommunicate' % (self.compName(), self.instanceName())),
        'userCodePost': userCode.postSection('%s::%sCommunicate' % (self.compName(), self.instanceName())) }

    return code
  
  def classDefinition(self, userCode):
    return """
%(classComment)s
class %(className)s
{
public:
  %(constructorDeclaration)s
  ~%(className)s();

  void init();  // called on component init
  void reset(); // called on component reset

  void driveSignal();  // drive current value out to MaxSim

  CarbonUInt32 * getValue()           { return &(mValue[0]); }
  void setChanged(bool changed) { mChanged = changed; }
  
  // Set the value of the signal.
  // It will be sent to MaxSim the next time driveSignal() is called.
  void setValue(CarbonUInt32 *newValue);

  // Get the actual SoCDesigner Port object
  sc_port<eslapi::CASISignalIF>* getPort() {return &mPort; }
  
  // this function is called when the Carbon model writes a new value to the signal
  static void valueChangeCallback(
    CarbonObjectID * carbonModel,  // carbon model instance handle
    CarbonNetID * carbonNet,       // carbon handle for signal
    void * data,                   // an instance of this class
    CarbonUInt32 * value,                // the new value of the signal
    CarbonUInt32 * drive)                // the new drive strength of the signal  
  {
    // avoid unused parameter warnings
    (void) carbonModel;
    (void) carbonNet;
    (void) drive;

    // copy new value and set changed flag
    %(className)s *port = reinterpret_cast<%(className)s*>(data);
    port->setValue(value);
  }

  //! Save state to a stream
  bool saveData(eslapi::CASIODataStream &os);

  //! Restore state from a stream
  bool restoreData(eslapi::CASIIDataStream &is);

private:

  // ****************************************************
  // All members that can be changed after initialization
  // need to be saved/restored.
  // ***************************************************

  %(compName)s* mOwner;
  CarbonUInt32 mValue[%(numWords)s];
  bool mChanged;
  %(declarePort)s
}; // %(className)s
""" % { 'classComment': classComment('Master Port ' + self.name()),
        'constructorDeclaration': self.classConstructorDeclaration(),
        'declarePort': self.classDeclareMasterPort(),
        'portName': self.name(),
        'className': self.className(),
        'compName': self.compName(),
        'numWords': self.numWords() }

  def generateMasterDrive(self):
    code = ""
    if self.numWords() == 1:
      code += "mPort.driveSignal(mValue[0], 0);"
    else:
      code += "mPort.driveSignal(mValue[0], &(mValue[1]));"
    return code

  def classImplementation(self, userCode):
    if self.numWords() == 1:
      valueCopy = 'mValue[0] = *newValue;'
    else:
      valueCopy = 'memcpy(mValue, newValue, %s * sizeof(CarbonUInt32));' % self.numWords()
    return """
%(classComment)s
%(constructorImplementation)s
%(className)s::~%(className)s()
{
}

void %(className)s::init()
{
  // tell the carbon model instance to call us back when the signal value changes
  carbonAddNetValueChangeCB(mOwner->mCarbonObj, valueChangeCallback, this, mOwner->%(netId)s);

  // fetch the initial value and drive it out to MaxSim
  CarbonUInt32 value[%(numWords)s];
  carbonExamine(mOwner->mCarbonObj, mOwner->%(netId)s, value, 0);
  setValue(value);
  driveSignal();
}

void %(className)s::reset()
{
  // Mark signal as changed so we will drive it after reset
  mChanged = true;
}

void %(className)s::setValue(CarbonUInt32* newValue)
{
%(preSetValueUserCode)s
  %(valueCopy)s
%(generatePortExpr)s
  mChanged = true;
%(flowThruDriveSignal)s
%(postSetValueUserCode)s
}

void %(className)s::driveSignal()
{
%(userCodePreMasterSignal)s
  if (mChanged && mPort.isConnected()) {
    %(generateMasterDrive)s
    mChanged = false;
  }
%(userCodePostMasterSignal)s
}

bool %(className)s::saveData(eslapi::CASIODataStream &os)
{
  os.writeBytes(reinterpret_cast<const char*>(mValue), sizeof(CarbonUInt32) * %(numWords)s);
  os << mChanged;
  return true;
}

bool %(className)s::restoreData(eslapi::CASIIDataStream &is)
{
  char* data = NULL;
  uint32_t numBytes = 0;
  is.readBytes(data, numBytes);
  if (numBytes != sizeof(CarbonUInt32) * %(numWords)s) {
    return false;
  }
  memcpy(mValue, data, numBytes);
  delete [] data;
  is >> mChanged;

  return true;
}
""" % { 'classComment': classComment('Master Port ' + self.name()),
        'constructorImplementation': self.classConstructorImplementation(),
        'className': self.className(),
        'compName': self.compName(),
        'numWords': self.numWords(),
        'userCodePreMasterSignal': userCode.preSection(self.className() + '::driveSignal'),
        'userCodePostMasterSignal': userCode.postSection(self.className() + '::driveSignal'),
        'generateMasterDrive': self.generateMasterDrive(),
        'netId': self.__netId,
        'name': self.name(),
        'valueCopy': valueCopy,
        'preSetValueUserCode': userCode.preSection(self.className() + '::setValue', '    '),
        'postSetValueUserCode': userCode.postSection(self.className() + '::setValue', '    '),
        'generatePortExpr': gGeneratePortExpr(self, "mValue[0]", "mValue"),
        'flowThruDriveSignal': self.flowThruDriveSignal() }

  def classConstructorDeclaration(self):
    return '%(className)s(%(compName)s* owner);' % {
      'className': self.className(),
      'compName': self.compName()
      }    

  def classConstructorImplementation(self):
    return """\
%(className)s::%(className)s(%(compName)s* owner):
  mOwner(owner), mChanged(false), mPort(mOwner, "%(name)s")
{
  // Initialize Port Value
  memset(mValue, 0, sizeof(CarbonUInt32) * %(numWords)s);

  // Setup Port Properties
  eslapi::CASISignalProperties prop;
  prop.isOptional = false;
  prop.bitwidth = %(width)d;
  mPort.setProperties(&prop);

  // Register Port
  mOwner->registerPort(&mPort, "%(name)s");
}"""  % { 'className': self.className(),
          'compName': self.compName(),
          'numWords': self.numWords(),
          'name': self.name(),
          'width': self.width() }

  def classDeclareMasterPort(self):
    return 'sc_port<eslapi::CASISignalIF> mPort;'

  def driveSignal(self):
    return '  ' + self.instanceName() + '->driveSignal();\n'

  def flowThruDriveSignal(self):
    # If static scheduling is disabled (default) and this is a flow
    # through pin, call drive signal when communicating
    code = ''
    if not self.config().useStaticScheduling() and self.isFlowThru():
      code += """\
  if (!mOwner->mDisableFlowThru) {
    driveSignal();
  }
"""
    return code

class MaxSimInterruptMaster(MaxSimPort):
  def __init__(self, config, xtor):
    MaxSimPort.__init__(self, config.compName(), xtor.name(), 32, config, '')

  def friendPortDef(self):
    return ''

  def className(self):
    return 'sc_port<eslapi::CASISignalIF>'

  def instanceName(self):
    return self.name() + '_SMaster'

  def instanceDeclaration(self):
    return """\
  %(className)s %(instance)s;
""" % { "className": self.className(),
        "instance": self.instanceName() }

  def componentConstructor(self):
    return '  registerPort(&%s, "%s");\n' % (self.instanceName(), self.name())

  def componentDestructor(self):
    return ''

  def componentInterconnect(self):
    return ''
  
  def driveSignal(self):
    # signal is driven by the InterruptMasterInput instances
    return ''

# input to an Interrupt_Master port.
# receives value changes from the Carbon Model and passes them on to the MaxSim master port
class InterruptMasterInput(MaxSimSignalMaster):
  
  def __init__(self, config, rtlPort, eslPort):
    MaxSimSignalMaster.__init__(self, config, rtlPort.name(), rtlPort.width(), rtlPort.carbonName(), rtlPort.carbonNetId(), '')
    self.__eslPort = eslPort

    # parse the numer out of the port name as the default interrupt number
    number = ''
    for c in rtlPort.name():
      ascii = ord(c)
      if ascii >= ord('0') and ascii <= ord('9'):
        number += c
    if number == '':
      number = "0"

    # create a parameter for the interrupt number
    signed = True # Use signed parameter (int)
    self.__interruptNumberParameter = parameters.SubParameter(rtlPort.name() + " id", number, 'integer', 'run-time', eslPort.name(), signed)
    config.addParameter(self.__interruptNumberParameter)

  def className(self):
    return self.__eslPort.name() + '_' + self.name() + '_SM'

  def instanceName(self):
    return self.__eslPort.name() + '_' + self.name() + '_SMaster'

  def componentConstructor(self):
    return """
  %(instanceName)s = new %(className)s(this, %(port)s);
""" % { 'className': self.className(),
        'instanceName': self.instanceName(),
        'port': self.__eslPort.instanceName()
        }

  def classConstructorDeclaration(self):
    # the master port is passed to the constructor as a ref
    return '%(className)s(%(compName)s* owner, %(portType)s &masterPort);' % {
      'className': self.className(),
      'compName': self.compName(),
      'portType': 'sc_port<eslapi::CASISignalIF>'
      }    

  def classConstructorImplementation(self):
    return """\
%(className)s::%(className)s(%(compName)s* owner, %(portType)s &masterPort):
  mOwner(owner), mChanged(false), mPort(masterPort)
{
  memset(mValue, 0, sizeof(CarbonUInt32) * %(numWords)s);
}"""  % { 'className': self.className(),
          'compName': self.compName(),
          'portType': 'sc_port<eslapi::CASISignalIF>',
          'numWords': self.numWords()
          }

  def classDeclareMasterPort(self):
    return 'sc_port<eslapi::CASISignalIF> &mPort;'

  def generateMasterDrive(self):
    return """// Disable interrupt if the ID parameter is set to -1
    if(mOwner->%(param)s != -1)
      mPort.driveSignal(mOwner->%(param)s, &mValue[0]);""" % {'param':self.__interruptNumberParameter.variable()}

class MaxSimNullSignalMaster(MaxSimSignalMaster):
  def __init__(self, config, name, width):
    MaxSimSignalMaster.__init__(self, config, name, width, None, None, '')
  def needsSaveRestore(self):
    return False
  def instanceDeclaration(self):
    return """
  // port %(name)s
  %(className)s *%(instanceName)s;
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className() }
  def componentInit(self):
    return ''
  def componentReset(self):
    return ''
  def componentConstructor(self):
    return """
  %(instanceName)s = new %(className)s(this);
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className() }
  
  def componentDestructor(self):
    return """\
  delete %(instanceName)s;
""" % { 'instanceName': self.instanceName() }
  def classDefinition(self, userCode):
    return """
class %(className)s {
public:
%(userCodePreDef)s
  %(className)s(%(compName)s* owner);
  ~%(className)s();
  void init() {}
  void reset() {}
  void driveSignal() {}

  // Get the actual SoCDesigner Port object
  sc_port<eslapi::CASISignalIF>* getPort() {return &mPort; }

private:
  sc_port<eslapi::CASISignalIF> mPort;
%(userCodePostDef)s
};
""" % { 'className': self.className(),
        'compName': self.compName(),
       'userCodePreDef': userCode.preSection(self.className() + ' CLASS DEFINITION'),
       'userCodePostDef': userCode.postSection(self.className() + ' CLASS DEFINITION') }

  def classImplementation(self, userCode):
    return """
%(className)s::%(className)s(%(compName)s* owner) :
  mPort(owner, "")
{
  owner->registerPort(&mPort, "%(name)s");
}

%(className)s::~%(className)s()
{
}

""" % { 'className': self.className(),
        'compName': self.compName(),
        'name': self.name() }

class MaxSimSignalSlave(MaxSimPort):
  def __init__(self, config, name, width, carbonName, netId, expr, tieParam=''):
    MaxSimPort.__init__(self, config.compName(), name, width, config, expr, tieParam)
    self._netId = netId
    self._carbonName = carbonName
  def className(self):
    return self.name() + '_SS'
  def instanceName(self):
    return self.name() + '_SSlave'
  def isFlowThru(self):
    return self.config().isFlowThruInput(self._carbonName)
  def needsSaveRestore(self):
    return True
  def instanceDeclaration(self):
    return """
  // port %(name)s
  %(className)s *%(instanceName)s;
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className() }
  def componentConstructor(self):
    return """
  %(instanceName)s = new %(className)s(this);
  registerPort(%(instanceName)s, "%(name)s");
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className() }

  def componentFlowThruSetup(self):
    code = ''
    if self.isFlowThru() :
      code += '  componentMaster->addDependency(this,%s,"flowThruCommunicate");\n' % self.instanceName()
    return code

  def componentReset(self):
    return '  %(instanceName)s->reset();\n' % { 'instanceName': self.instanceName() }
  
  def saveData(self):
    # By default, nothing to do here
    return '  return true;'

  def restoreData(self):
    # By default, nothing to do here
    return '  return true;'

  def classDefinition(self, userCode):
    return """
%(classComment)s
class %(className)s: public eslapi::CASISignalSlave
{
%(userCodePreDef)s
  %(compName)s *owner;

public:
  %(className)s(%(compName)s *_owner);
  virtual ~%(className)s();

public:
  void init();
  void reset();
  void terminate();

  virtual void driveSignal(uint32_t value, uint32_t* extValue);
  virtual uint32_t readSignal();
  virtual void readSignal(uint32_t* value, uint32_t* extValue);

  //! Save state to a stream
  bool saveData(eslapi::CASIODataStream &os);

  //! Restore state from a stream
  bool restoreData(eslapi::CASIIDataStream &is);

%(memberDeclare)s
%(userCodePostDef)s
};
""" % {'classComment': classComment('Slave Port ' + self.name()),
       'className': self.className(),
       'compName': self.compName(),
       'userCodePreDef': userCode.preSection(self.className() + ' CLASS DEFINITION'),
       'userCodePostDef': userCode.postSection(self.className() + ' CLASS DEFINITION'),
       'memberDeclare': self.members().code('declare') }
  def classImplementation(self, userCode):
    className = self.className()
    code = """\
%(classComment)s
%(className)s::%(className)s(%(compName)s *_owner):
  eslapi::CASISignalSlave("%(className)s")
{
%(userCodePreConstructor)s
  owner = _owner;
  setCASIOwner(owner);
  eslapi::CASISignalProperties prop;
  memset(&prop, 0, sizeof(prop));
  prop.isOptional = false;
  prop.bitwidth = %(width)s;
  setProperties(&prop);
%(memberConstruct)s
%(userCodePostConstructor)s
}

%(className)s::~%(className)s()
{
%(userCodePreDestructor)s
%(memberDestruct)s
%(userCodePostDestructor)s
}

void %(className)s::init()
{
%(userCodePreInit)s
%(memberInit)s
%(userCodePostInit)s
}

void %(className)s::reset()
{
%(userCodePreReset)s
%(reset)s
%(userCodePostReset)s
}

void %(className)s::terminate()
{
%(userCodePreTerminate)s
%(memberTerminate)s
%(userCodePostTerminate)s
}

void %(className)s::driveSignal(uint32_t value, uint32_t* extValue)
{
%(userCodePreDriveSignal)s
%(driveSignal)s
%(userCodePostDriveSignal)s
}

uint32_t %(className)s::readSignal()
{
%(userCodeReadSignalVoid)s
}

void %(className)s::readSignal(uint32_t* value, uint32_t* extValue)
{
%(userCodeReadSignal)s
}

bool %(className)s::saveData(eslapi::CASIODataStream &os)
{
%(saveData)s
}

bool %(className)s::restoreData(eslapi::CASIIDataStream &is)
{
%(restoreData)s
}
"""
    return code % { 'classComment': classComment('Slave Port ' + self.name()),
                    'className': className,
                    'compName': self.compName(),
                    'width': self.width(),
                    'name': self.name(),
                    'netId': self._netId,
                    'numWords': self.numWords(),
                    'wordsToCopy': self.numWords() - 1,
                    'driveSignal': self.driveSignal(),
                    'userCodePreConstructor': userCode.preSection('%s::%s' % (className, className)),
                    'userCodePostConstructor': userCode.postSection('%s::%s' % (className, className)),
                    'userCodePreDestructor': userCode.preSection('%s::~%s' % (className, className)),
                    'userCodePostDestructor': userCode.postSection('%s::~%s' % (className, className)),
                    'userCodePreInit': userCode.preSection(className + '::init'),
                    'userCodePostInit': userCode.postSection(className + '::init'),
                    'userCodePreReset': userCode.preSection(className + '::reset'),
                    'userCodePostReset': userCode.postSection(className + '::reset'),
                    'userCodePreTerminate': userCode.preSection(className + '::terminate'),
                    'userCodePostTerminate': userCode.postSection(className + '::terminate'),
                    'userCodePreDriveSignal': userCode.preSection(className + '::driveSignal'),
                    'userCodePostDriveSignal': userCode.postSection(className + '::driveSignal'),
                    'userCodeReadSignalVoid': userCode.implementSection(className + '::readSignal(void)', '  ', '  return 0;\n'),
                    'userCodeReadSignal': userCode.implementSection(className + '::readSignal', '  ', '  return;\n'),
                    'saveData': self.saveData(),
                    'restoreData': self.restoreData(),
                    'reset': self.reset(),
                    'memberConstruct': self.members().code('construct'),
                    'memberInit': self.members().code('init'),
                    'memberTerminate': self.members().code('terminate'),                    
                    'memberDestruct': self.members().code('destruct') }

  def reset(self):
    if self.numWords() == 1:
      resetCode = '  driveSignal(0, NULL);'
    else:
      resetCode = """\
  CarbonUInt32 value[%(numWords)s];
  memset(value, 0, sizeof(CarbonUInt32) * %(numWords)s);
  driveSignal(0, value);
""" % {'numWords' : self.numWords() -1 }  
    return resetCode

  def driveSignal(self):
    # Write code to drive the value into the model
    code = ''
    if self.numWords() == 1:
      code += """\
%(generatePortExpr)s
  carbonDepositFast(owner->mCarbonObj, owner->%(netId)s, &value, 0);
"""
    else:
      code += """\
  CarbonUInt32 val[%(numWords)s];
  val[0] = value;
  if ( extValue ){
    memcpy(&(val[1]), extValue, sizeof(CarbonUInt32) * %(wordsToCopy)s);
  } else {
    // here we assume that there is a signal size mismatch, and we pad with zeros
    memset(&(val[1]), 0, sizeof(CarbonUInt32) * %(wordsToCopy)s);
  }
%(generatePortExpr)s
  carbonDepositFast(owner->mCarbonObj, owner->%(netId)s, val, 0);
"""

    # If static scheduling is disabled (the default) and this is a
    # flow through, call flow through communicate
    if not self.config().useStaticScheduling() and self.isFlowThru():
      code += """\
  if (!owner->mDisableFlowThru) {
    owner->flowThruCommunicate();
  }
"""

    return code % { 'netId': self._netId,
                    'numWords': self.numWords(),
                    'wordsToCopy': self.numWords() - 1,
                    'generatePortExpr': gGeneratePortExpr(self, "value", "val") }

class MaxSimNullSignalSlave(MaxSimSignalSlave):
  def __init__(self, config, name, width):
    MaxSimSignalSlave.__init__(self, config, name, width, None, None, '')

  def driveSignal(self):
    return ''

class MaxSimInterruptSlave(MaxSimSignalSlave):
  def __init__(self, config, xtor):
    # find the 'isrc' port
    isrc = None
    for port in xtor.outputs():
      if port.name() == 'isrc':
        isrc = port
        break
    if not isrc:
      raise error.RTLConnectionError(xtor.name(), 'isrc')
    MaxSimSignalSlave.__init__(self, config, xtor.name(), isrc.width(), isrc.carbonName(), isrc.carbonNetId(), '')

    # define an extra class member to hold the currently asserted interrupts

    # ****************************************************
    # All members that can be changed after initialization
    # need to be saved/restored.
    # ***************************************************

    self.__numWords = isrc.numRtlWords()
    member = Member('mInterruptVector')
    member['declare'] = 'CarbonUInt32 %s[%d];' % (member['name'], self.__numWords)
    member['construct'] = 'memset(%s, 0, sizeof(CarbonUInt32)*%d);' % (member['name'], self.__numWords)
    self.members().append(member)

  def reset(self):
    code = """\
  memset(mInterruptVector, 0, sizeof(CarbonUInt32)*%(words)d);
  carbonDepositFast(owner->mCarbonObj, owner->%(netId)s, mInterruptVector, 0);
""" % { 'netId': self._netId, 'words' : self.__numWords }
    return code

  def driveSignal(self):
    code = '''\
    if (extValue == NULL) {
      owner->message(eslapi::CASI_MSG_WARNING,
        "interrupt slave port %(portName)s driveSignal() called with NULL second parameter. "
        "The second parameter should be a pointer to an integer "
        "value indicating if the interrupt should be set or cleared.");
    }
    else {
      int word = value / 32;
      int bit = value %% 32;
      if (*extValue) {
        mInterruptVector[word] |= (1 << bit);
      }
      else {
        mInterruptVector[word] &= ~(1 << bit);
      }
      carbonDepositFast(owner->mCarbonObj, owner->%(netId)s, mInterruptVector, 0);
    }
''' % { 'netId': self._netId, 'portName': self.name() }
    return code

  def saveData(self):
    code = """
  os.writeBytes(reinterpret_cast<const char*>(mInterruptVector), sizeof(CarbonUInt32) * %(numWords)d);
  return true;""" % {'numWords': self.__numWords}
    return code

  def restoreData(self):
    code = """
  char* data = NULL;
  uint32_t numBytes = 0;
  is.readBytes(data, numBytes);
  if (numBytes != sizeof(CarbonUInt32) * %(numWords)d) {
    return false;
  }
  memcpy(mInterruptVector, data, numBytes);
  delete [] data;
  return true;""" % {'numWords': self.__numWords}
    return code


class MaxSimResetSlave(MaxSimSignalSlave):
  def __init__(self, config, xtor):
    # find the 'isrc' port
    reset = None
    for port in xtor.outputs():
      if port.name() == 'reset':
        reset = port
        break
    if not reset:
      raise error.ConfigError(xtor.name() + '.reset port must be connected to an RTL signal')
    MaxSimSignalSlave.__init__(self, config, xtor.name(), reset.width(), reset.carbonName(), reset.carbonNetId(), '')
    self.__port = port

    # Parameters
    self.__clockCycles    = int(xtor.findParam('Clock Cycles'))
    self.__compCycles     = int(xtor.findParam('Comp Cycles'))
    if xtor.findParam('Use Active Value') == 'true':
      self.__activeValue  = int(xtor.findParam('Active Value'));
      self.__inactiveValue  = int(xtor.findParam('Inactive Value'));
    else:
      if xtor.findParam('Active High') == 'true':
        self.__activeValue    = 1
        self.__inactiveValue  = 0
      else:
        self.__activeValue    = 0
        self.__inactiveValue  = 1
    self.__cyclesBefore   = int(xtor.findParam('Cycles Before'))
    self.__cyclesAsserted = int(xtor.findParam('Cycles Asserted'))
    self.__cyclesAfter    = int(xtor.findParam('Cycles After'))

    # define an extra class member to hold the clock definition object

    # ****************************************************
    # All members that can be changed after initialization
    # need to be saved/restored.
    # ***************************************************

    member = Member('mClockDef')
    member['declare'] = 'CarbonResetGenDef* %s;' % member['name']
    member['construct'] = '%s = 0;' % member['name'] 
    member['init'] = '%s = new CarbonResetGenDef(owner->%s, %s, %s, CARBON_TICKS_PER_MAXSIM_CYCLE, %s, %s, %s, %s, %s);' % (
      member['name'], self._netId,
      self.__clockCycles, self.__compCycles,
      self.__activeValue, self.__inactiveValue, self.__cyclesBefore, self.__cyclesAsserted, self.__cyclesAfter)
    member['terminate'] = 'delete mClockDef;'
    self.members().append(member)

  def saveData(self):
    code = """
  bool ok = true;
  ok &= mClockDef->saveData(os);
  return ok;"""
    return code

  def restoreData(self):
    code = """
  bool ok = true;
  ok &= mClockDef->restoreData(is);
  return ok;"""
    return code

  # Add a few methods to be compatible with Cfg.Reset
  def shortName(self):
    return self.__port.shortName()
  def carbonName(self):
    return self.__port.carbonName()
  def carbonNetId(self):
    return self.__port.carbonNetId()
  def clockCycles(self):
    return self.__clockCycles
  def compCycles(self):
    return self.__compCycles
  def activeValue(self):
    return self.__activeValue
  def activeValueHigh(self):
    return 0
  def activeValueLow(self):
    return self.__activeValue
  def activeValueLow(self):
    return self.__activeValue
  def inactiveValue(self):
    return self.__inactiveValue
  def inactiveValueHigh(self):
    return 0
  def inactiveValueLow(self):
    return self.__inactiveValue
  def cyclesBefore(self):
    return self.__cyclesBefore
  def cyclesAsserted(self):
    return self.__cyclesAsserted
  def cyclesAfter(self):
    return self.__cyclesAfter
  
  def xtorSlaves(self):
    return ''

  def reset(self):
    code = """  mClockDef->reset(owner->mCarbonSimulationTime);
  owner->mSimulationQueue->addClock(mClockDef);"""
    return code

  def componentInit(self):
    return '  %(instanceName)s->init();\n' % { 'instanceName': self.instanceName() }
  def componentTerminate(self):
    return '  %(instanceName)s->terminate();\n' % { 'instanceName': self.instanceName() }

  def isReset(self):
    return True
  
class TransactionSlaveClass(cpp.Class):
  def __init__(self, config, compPort, baseParameter, sizeParameter):
    cpp.Class.__init__(self, compPort.className(), ['public eslapi::CASITransactionSlave'])

    member = self.addMemberVariable(config.compName() + '*', 'owner', access='private')
    member.setDescription('Pointer to owning component')
    member.setConstructorInit('_owner')

    func = self.addConstructor('(' + config.compName() + '* _owner)', baseClassInit = 'eslapi::CASITransactionSlave("%s")' % compPort.className())
    func.addCode("""
owner = _owner;
setCASIOwner(owner);
eslapi::CASITransactionProperties prop;
memset(&prop,0,sizeof(prop));
AHB_INIT_TRANSACTION_PROPERTIES(prop);
setProperties(&prop);
""")
    
    self.addDestructor(virtual = True)

    func = self.addMemberFunction('eslapi::CASIStatus', 'read', '(uint64_t addr, uint32_t* value, uint32_t* ctrl)', virtual=True)
    func.addCode("return eslapi::CASI_STATUS_NOTSUPPORTED;")
    
    func = self.addMemberFunction('eslapi::CASIStatus', 'write', '(uint64_t addr, uint32_t* value, uint32_t* ctrl)', virtual=True)
    func.addCode("return eslapi::CASI_STATUS_NOTSUPPORTED;")

    func = self.addMemberFunction('eslapi::CASIStatus', 'readDbg', '(uint64_t addr, uint32_t* value, uint32_t* ctrl)', virtual=True)
    func.addCode("return eslapi::CASI_STATUS_NOTSUPPORTED;")

    func = self.addMemberFunction('eslapi::CASIStatus', 'writeDbg', '(uint64_t addr, uint32_t* value, uint32_t* ctrl)', virtual=True)
    func.addCode("return eslapi::CASI_STATUS_NOTSUPPORTED;")

    func = self.addMemberFunction('eslapi::CASIStatus', 'readReq', '(uint64_t addr, uint32_t* value, uint32_t* ctrl, eslapi::CASITransactionCallbackIF* callback)', virtual=True)
    func.addCode("return eslapi::CASI_STATUS_NOTSUPPORTED;")

    func = self.addMemberFunction('eslapi::CASIStatus', 'writeReq', '(uint64_t addr, uint32_t* value, uint32_t* ctrl, eslapi::CASITransactionCallbackIF* callback)', virtual=True)
    func.addCode("return eslapi::CASI_STATUS_NOTSUPPORTED;")

    func = self.addMemberFunction('eslapi::CASIGrant', 'requestAccess', '(uint64_t addr)', virtual=True)
    func.addCode("return eslapi::CASI_GRANT_OK;")

    func = self.addMemberFunction('eslapi::CASIGrant', 'checkForGrant', '(uint64_t addr)', virtual=True)
    func.addCode("return eslapi::CASI_GRANT_OK;")

    self.addMemberFunction('void', 'driveTransaction', '(eslapi::CASITransactionInfo* info)', virtual=True)
    self.addMemberFunction('void', 'cancelTransaction', '(eslapi::CASITransactionInfo* info)', virtual=True)

    func = self.addMemberFunction('eslapi::CASIStatus', 'debugTransaction', '(eslapi::CASITransactionInfo* info)', virtual=True)
    func.addCode("return eslapi::CASI_STATUS_NOTSUPPORTED;")

    func = self.addMemberFunction('int', 'getNumRegions', '()', virtual=True)
    func.addCode("return 1;")

    func = self.addMemberFunction('void', 'getAddressRegions', '(uint64_t* start, uint64_t* size, string* name)', virtual=True)
    func.addCode("""
if(start && size && name) {
  *start = owner->%(baseParameter)s;
  *size = owner->%(sizeParameter)s;
  *name = "%(compName)s";
}
""" % { 'compName' : config.compName(), 'baseParameter': baseParameter.variable(), 'sizeParameter': sizeParameter.variable() } )
    
    func = self.addMemberFunction('void', 'setAddressRegions', '(uint64_t* start, uint64_t* size, string* name)', virtual=True)
    func.addCode("""
  owner->%(baseParameter)s = *start;
  owner->%(sizeParameter)s = *size;
""" % { 'baseParameter': baseParameter.variable(), 'sizeParameter': sizeParameter.variable() } )

    func = self.addMemberFunction('eslapi::CASIMemoryMapConstraints *', 'getMappingConstraints', '()', virtual=True)
    func.addCode("return NULL;")

class MaxSimTransactionSlave(MaxSimPort):
  def __init__(self, compName, name, config):
    MaxSimPort.__init__(self, compName, name, None, config, '')
    self.__baseParameter = parameters.Parameter(self.name() + ' Base', "0x10000000", "integer", 'init-time')
    self.__sizeParameter = parameters.Parameter(self.name() + ' Size', "0x1000", "integer", 'init-time')
    config.addParameter(self.__sizeParameter)
    config.addParameter(self.__baseParameter)

    self.__cgClass = TransactionSlaveClass(config, self, self.__baseParameter, self.__sizeParameter)

  def className(self):
    return self.name() + '_TS'
  def instanceName(self):
    return self.name() + '_TSlave'
  def instanceDeclaration(self):
    return """
  // port %(name)s
  %(className)s *%(instanceName)s;
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className() }
  def componentConstructor(self):
    return """
  %(instanceName)s = new %(className)s(this);
  registerPort(%(instanceName)s, "%(name)s");
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className() }
  def classDefinition(self, userCode):
    return self.__cgClass.emitDefinition(userCode, None)
  def classImplementation(self, userCode):
    return self.__cgClass.emitImplementation(userCode, None)

class MaxSimClockMaster(MaxSimSignalMaster):
  def __init__(self, config, xtor):
    port = None
    for input in xtor.inputs():
      if input.name() == 'clock':
        port = input
        break
    if not port:
      raise error.RTLConnectionError(xtor.name(), 'clock')
    MaxSimSignalMaster.__init__(self, config, xtor.name(), port.width(), port.carbonName(), port.carbonNetId(), '')
  def className(self):
    return self.name() + '_CM'
  def instanceName(self):
    return self.name() + '_ClockMaster'
  def driveSignal(self):
    return ''
  def classDefinition(self, userCode):
    return """
%(classComment)s
class %(className)s: public eslapi::CASIClockMaster
{
public:
  %(className)s(%(compName)s *owner);
  virtual ~%(className)s();

  void registerClockSlave(eslapi::CASIClockSlaveIF *slave); 
  void unregisterClockSlave(eslapi::CASIClockSlaveIF *);
  void registerClockSlave(eslapi::CASIClockSlaveIF *slave,
                          eslapi::CASIPhase phase, uint32_t repetitions = 0,
                          uint32_t ratio = 1, uint32_t offset = 0);

#if MAXSIM_MAJOR_VERSION >= 7
  void schedule();
#endif
  
  void init();  // called on component init
  void reset(); // called on component reset
  
  void communicate();  // call communicate method of all registered slaves
  void update();       // call update method of all registered slaves

  // Set the value of the signal.
  void setValue(CarbonUInt32 *newValue);

  // this function is called when the Carbon model writes a new value to the signal
  static void valueChangeCallback(CarbonObjectID *, CarbonNetID *, void * data, CarbonUInt32 *, CarbonUInt32 *);

  //! Save state to a stream
  bool saveData(eslapi::CASIODataStream &os);

  //! Restore state from a stream
  bool restoreData(eslapi::CASIIDataStream &is);

private:
  typedef struct ClockSlave_s {
    eslapi::CASIClockSlaveIF *mSlave;
    eslapi::CASIPhase mPhase;
    uint32_t mRepetitions, mRatio, mOffset;
    uint64_t mCycleCount, mCommunicateCount, mUpdateCount;

    bool saveData(eslapi::CASIODataStream &os)
    {
      // Only save data that can actually change
      os << mCycleCount << mCommunicateCount << mUpdateCount;
      return true;
    }

    bool restoreData(eslapi::CASIIDataStream &is)
    {
      is >> mCycleCount >> mCommunicateCount >> mUpdateCount;
      return true;
    }

  } ClockSlave;


  // ****************************************************
  // All members that can be changed after initialization
  // need to be saved/restored.
  // ***************************************************

  %(compName)s* mOwner;
  bool mPosEdgeSeen;

  // registered clock slaves
  std::list<ClockSlave> mClockSlaves;
}; // %(className)s
""" % { 'classComment': classComment('Clock Master Port ' + self.name()),
        'className': self.className(),
        'compName': self.compName(),
        'numWords': self.numWords() }

  def classImplementation(self, userCode):
    className = self.className()
    return """
%(classComment)s
%(className)s::%(className)s(%(compName)s *owner): mOwner(owner)
{
%(userConstructor)s
}

%(className)s::~%(className)s()
{
%(userDesstructor)s
}

void %(className)s::init()
{
%(userPreInit)s
  // tell the carbon model instance to call us back when the signal value changes
  carbonAddNetValueChangeCB(mOwner->mCarbonObj, valueChangeCallback, this, mOwner->%(netId)s);
%(userPostInit)s
}

void %(className)s::reset()
{
%(userPreReset)s
  // no posedge changes yet
  mPosEdgeSeen = false;

  // start cycle counts at 0 again
  for (std::list<ClockSlave>::iterator slave = mClockSlaves.begin(); slave != mClockSlaves.end(); slave++) {
    slave->mCycleCount = 0UL;
    slave->mCommunicateCount = 0UL;
    slave->mUpdateCount = 0UL;
  }

%(userPostReset)s
}

#if MAXSIM_MAJOR_VERSION >= 7
void %(className)s::schedule()
{
%(userPreSchedule)s

  // Schedule all Slaves
  for (std::list<ClockSlave>::iterator slave = mClockSlaves.begin(); slave != mClockSlaves.end(); slave++) {
    eslapi::CASIClockMaster* master = dynamic_cast<eslapi::CASIClockMaster*>(slave->mSlave);
    if(master)
      master->schedule();
  }

%(userPostSchedule)s
}
#endif

void %(className)s::registerClockSlave(eslapi::CASIClockSlaveIF *slave,
                                       eslapi::CASIPhase phase, uint32_t repetitions,
                                       uint32_t ratio, uint32_t offset)
{
%(userPreRegisterConditional)s
  ClockSlave cs = {slave, phase, repetitions, ratio, offset, 0UL, 0UL, 0UL};
  mClockSlaves.insert(mClockSlaves.begin(), cs);
%(userPostRegisterConditional)s
}

void %(className)s::registerClockSlave(eslapi::CASIClockSlaveIF *slave)
{
%(userPreRegister)s
  registerClockSlave(slave, eslapi::CASI_PHASE_BOTH);
%(userPostRegister)s
}

void %(className)s::unregisterClockSlave(eslapi::CASIClockSlaveIF *slave)
{
%(userPreUnregister)s
  for (std::list<ClockSlave>::iterator iter = mClockSlaves.begin(); iter != mClockSlaves.end(); iter++) {
    if (iter->mSlave == slave) {
      mClockSlaves.erase(iter);
      break;
    }
  }
%(userPostUnregister)s
}

void %(className)s::communicate()
{
%(userPreCommunicate)s
  if (mPosEdgeSeen) {
    for (std::list<ClockSlave>::iterator slave = mClockSlaves.begin(); slave != mClockSlaves.end(); slave++) {
      // increment the cycle count for the slave
      ++(slave->mCycleCount);
      if ((slave->mRepetitions == 0 || slave->mCommunicateCount < slave->mRepetitions) &&
          (slave->mCycleCount > slave->mOffset) &&
          (((slave->mCycleCount -1) %% slave->mRatio) == 0) &&
          (slave->mPhase == eslapi::CASI_PHASE_BOTH || slave->mPhase == eslapi::CASI_PHASE_COMMUNICATE)) {
        slave->mSlave->communicate();
        ++(slave->mCommunicateCount);
      }
    }
  }
%(userPostCommunicate)s
}

void %(className)s::update()
{
%(userPreUpdate)s
  if (mPosEdgeSeen) {
    for (std::list<ClockSlave>::iterator slave = mClockSlaves.begin(); slave != mClockSlaves.end(); slave++) {
      if ((slave->mRepetitions == 0 || slave->mUpdateCount < slave->mRepetitions) &&
          (slave->mCycleCount > slave->mOffset) &&
          (((slave->mCycleCount -1) %% slave->mRatio) == 0) &&
          (slave->mPhase == eslapi::CASI_PHASE_BOTH || slave->mPhase == eslapi::CASI_PHASE_UPDATE)) {
        slave->mSlave->update();
        ++(slave->mUpdateCount);
      }
    }
    mPosEdgeSeen = false;
  }
%(userPostUpdate)s
}

// this function is called when the Carbon model writes a new value to the signal
void %(className)s::valueChangeCallback (
    CarbonObjectID * carbonModel,  // carbon model instance handle
    CarbonNetID * carbonNet,       // carbon handle for signal
    void * data,                   // an instance of this class
    CarbonUInt32 * value,                // the new value of the signal
    CarbonUInt32 * drive)                // the new drive strength of the signal  
{
%(userPreValueChange)s
    // avoid unused parameter warnings
    (void) carbonModel;
    (void) carbonNet;
    (void) drive;

    // get pointer to the master clock port
    %(className)s *port = reinterpret_cast<%(className)s*>(data);

    // if the new value is non-zero, count it as a rising clock edge
    if (*value) {
      port->mPosEdgeSeen = true;
    }
%(userPostValueChange)s
}

bool %(className)s::saveData(eslapi::CASIODataStream &os)
{
%(saveData)s
}

bool %(className)s::restoreData(eslapi::CASIIDataStream &is)
{
%(restoreData)s
}

""" % { 'classComment': classComment('Clock Master Port ' + self.name()),
        'className': self.className(),
        'compName': self.compName(),
        'numWords': self.numWords(),
        'netId': self.carbonNetId(),
        'name': self.name(),
        'saveData': self.saveData(),
        'restoreData': self.restoreData(),
        'userConstructor': userCode.preSection('%s::%s' % (className, className)) + '\n\n' + userCode.postSection('%s::%s' % (className, className)),
        'userDesstructor': userCode.preSection('%s::~%s' % (className, className)) + '\n\n' + userCode.postSection('%s::~%s' % (className, className)),
        'userPreInit': userCode.preSection(className + '::init'),
        'userPostInit': userCode.postSection(className + '::init'),
        'userPreReset': userCode.preSection(className + '::reset'),
        'userPostReset': userCode.postSection(className + '::reset'),
        'userPreSchedule': userCode.preSection(className + '::schedule'),
        'userPostSchedule': userCode.postSection(className + '::schedule'),
        'userPreRegisterConditional': userCode.preSection(className + '::registerClockSlaveConditional'),
        'userPostRegisterConditional': userCode.postSection(className + '::registerClockSlaveConditional'),
        'userPreRegister': userCode.preSection(className + '::registerClockSlave'),
        'userPostRegister': userCode.postSection(className + '::registerClockSlave'),
        'userPreUnregister': userCode.preSection(className + '::unregisterClockSlave'),
        'userPostUnregister': userCode.postSection(className + '::unregisterClockSlave'),
        'userPreCommunicate': userCode.preSection(className + '::communicate'),
        'userPostCommunicate': userCode.postSection(className + '::communicate'),
        'userPreUpdate': userCode.preSection(className + '::update'),
        'userPostUpdate': userCode.postSection(className + '::update'),
        'userPreValueChange': userCode.preSection(className + '::valueChangeCallback'),
        'userPostValueChange': userCode.postSection(className + '::valueChangeCallback')
      }
  def componentConstructor(self):
    return """
  %(instanceName)s = new %(className)s(this);
  registerPort(dynamic_cast< eslapi::CASIClockMasterIF* >(%(instanceName)s), "%(name)s");
  %(instanceName)s->setCASIOwner(NULL);
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className() }
  def componentInterconnect(self):
    return ''
  def componentFlowThruSetup(self):
    return ''
  def componentUpdate(self):
    return '  %(instanceName)s->update();\n' % { 'instanceName': self.instanceName() }
  def componentCommunicate(self):
    return '  %(instanceName)s->communicate();\n' % { 'instanceName': self.instanceName() }

  def saveData(self):
    code = """
  bool ok = true;
  for (std::list<ClockSlave>::iterator iter = mClockSlaves.begin(); iter != mClockSlaves.end(); ++iter) {
    ok &= (*iter).saveData(os);
  }
  os << mPosEdgeSeen;
  return ok;"""
    return code

  def restoreData(self):
    code = """
  bool ok = true;
  for (std::list<ClockSlave>::iterator iter = mClockSlaves.begin(); iter != mClockSlaves.end(); ++iter) {
    ok &= (*iter).restoreData(is);
  }
  is >> mPosEdgeSeen;
  return ok;"""
    return code

# input clock slave that controls a transaction adapter port
class ClockInputClass(cpp.Class):
  def __init__(self, className, name, config, xtor, drivesRtl, drivesXtors, slaveXtors, rtlEnable, delayEnable):
    if(int(config.mxVer()) < 7 or (int(config.mxVer()) == 7 and (int(config.mxMinorVer()) < 11 or (int(config.mxMinorVer()) == 11 and int(config.mxRevision()) == 0)))):
      # versions of SoCD up to and including 7_11_0
      cpp.Class.__init__(self, className, ['public eslapi::CASIClockSlave'])
    elif(int(config.mxVer()) == 7 and (int(config.mxMinorVer()) < 12)):
      # versions of SoCD 7_11_1 to 7_11_3+
      cpp.Class.__init__(self, className, ['public eslapi::CASIClockSlaveDefaultPriority'])
    else:
     # versions of SoCD 7_12_0 and later
      cpp.Class.__init__(self, className, ['public eslapi::CASIClockSlaveDefaultPriority2'])

    def boolMember(xtor): return 'mCalled_%s' % xtor.name()
    def boolParam(xtor):  return 'pCalled_%s' % xtor.name()
    def setBoolMember(xtor, value):
      return '*%s = %s;' % (boolMember(xtor), value)

    params =  config.compName() + '* owner'
    if drivesXtors:
      for xtor in slaveXtors:
        params += ', bool *' + boolParam(xtor)
    if(int(config.mxVer()) < 7 or (int(config.mxVer()) == 7 and (int(config.mxMinorVer()) < 11 or (int(config.mxMinorVer()) == 11 and int(config.mxRevision()) == 0)))):
      # versions of SoCD up to and including 7_11_0
      self.addConstructor('(' + params + ')')
    elif(int(config.mxVer()) == 7 and (int(config.mxMinorVer()) < 12)):
      # versions of SoCD 7_11_1 to 7_11_3+
      self.addConstructor('(' + params + ')', baseClassInit = 'eslapi::CASIClockSlaveDefaultPriority(owner)')
    else:
     # versions of SoCD 7_12_0 and later
      self.addConstructor('(' + params + ')', baseClassInit = 'eslapi::CASIClockSlaveDefaultPriority2(owner)')

    self.addDestructor()

    # Construct the init function. We need to merge the constructor
    # args based on drivesRtl and rtlEnable
    sep = ''
    args = ''
    if drivesRtl:
      args = 'CarbonNetID* net_id, CarbonUInt32 clock_cycle, CarbonUInt32 comp_cycle, CarbonTime ticks_per_cycle, CarbonUInt32 initial_val, CarbonUInt32 delay, CarbonUInt32 duty_cycle'
      sep = ', '
    else:
      args = ''
    if rtlEnable:
      args += sep + 'CarbonNetID* en_net_id'
    func = self.addMemberFunction('void', 'init', '(%s)' % args)
    func.setDescription('Called by component on CASIModule::init().')

    # Add the init code if it drives rtl
    if drivesRtl:
      func.addCode("""
mInitialClockCycles  = clock_cycle;
mInitialCompCycles   = comp_cycle;
mClockDef      = new CarbonClockSlaveDef(net_id, clock_cycle, comp_cycle, ticks_per_cycle, initial_val, delay, duty_cycle);

// Assign Names to Clock Definition objects to assist in debugging
mClockDef->setName("%(clockDefName)s");
""" % {'clockDefName':self.name() + '_clkDef' })

    # Add the init code if it has an enable
    if rtlEnable:
      func.addCode('''
mEnNetID = en_net_id;
''')

    # Add the reset function
    func = self.addMemberFunction('void', 'reset')
    func.setDescription('Called by component at the beginning of CASIModule::reset().')
    if drivesRtl:
      if (int(config.mxVer()) > 7 or (int(config.mxVer()) == 7  and (int(config.mxMinorVer()) > 11))):  
        # SoCD verions 7_12_0 and later
        getProperties = """
mHasSysClk = mOwner->getMaster() == eslapi::CASIClockScheduler::sGetSystemClockMaster();
eslapi::MasterClockPropertyAccessor* clk = dynamic_cast<eslapi::MasterClockPropertyAccessor*>(getMaster());
if (clk)
{
  mClockProperties = clk->getSchedulingProperties();
}
"""
      else:
        # SOCD versions earlier than 7_12_0
        getProperties = ""

      func.addCode("""
// Initialize members
mLastUpdateCycle = 0;
mCyclesPerUpdate = mInitialCompCycles/mInitialClockCycles;
mMasterComm = 0;

// Start Reset Clock
mClockDef->reset(mOwner->mCarbonSimulationTime);
mClockDef->setClockRatio(mInitialClockCycles, mInitialCompCycles);
mOwner->mSimulationQueue->addClock(mClockDef);

// check if component has access to system clock (component clk-in port left unconnected by user) and obtain clock properties
mHasSysClk = false;

mClockProperties = NULL; 
%(prop)s
""" % {'prop' : getProperties} )
    if rtlEnable:
      func.addCode("""
mEnValue = 0;
""")
    
    func = self.addMemberFunction('void', 'postReset')
    func.setDescription('Called by component at the end of CASIModule::reset().')
    if drivesRtl:
      func.addCode("""
// Stop Reset Sequence Clock
mOwner->mSimulationQueue->removeClock(mClockDef);

// Set initial clock period
mClockDef->setClockRatio(mInitialClockCycles, mInitialCompCycles);
""")
    if drivesXtors:
      for xtor in slaveXtors:
        func.addCode(setBoolMember(xtor, 'false'))

    # Communicate function
    func = self.addMemberFunction('void', 'communicate')
    func.setDescription('Called by connected clock master.')

    # If there is an enable, check if it is high. If it isn't return immediately
    if rtlEnable:
      func.addCode('''
// Only execute the clock function if the enable is high.
// Note the mEnValue is examined in the update phase if
// it is to be delayed or examine here if not. See the
// upate function for details.
static const bool cDelayEnable = %s;
if (!cDelayEnable) {
  carbonExamine(mOwner->getCarbonObject(), mEnNetID, &mEnValue, NULL);
}
// See the code there for details.
if (mEnValue == 0) {
  return;
}
''' % delayEnable )

    # Add code for the remaining part of communicate
    if drivesRtl:
      func.addCode("""
++mMasterComm;

CarbonUInt64 currentCycle, cyclesPerUpdate;

currentCycle = mOwner->getCycle();

if ((mMasterComm == 1) && (currentCycle > 0))
{
  // Our clock generator has moved forward in time, and
  // we have not yet seen a pulse from the master clock (mMasterComm
  // is 1). We need to set the mLastUpdateCycle to currentCycle - 1
  // to keep a consistent waveform
  if (currentCycle > mCyclesPerUpdate)
    // in case we have a different default cyclesPerUpdate value
    mLastUpdateCycle = currentCycle - mCyclesPerUpdate;
  else
    // maxsim clock speed
    mLastUpdateCycle = currentCycle - 1;
}

cyclesPerUpdate = currentCycle - mLastUpdateCycle;
mLastUpdateCycle = currentCycle;

// if the number of cycles between updates has changed, recompute the
// clock edges and update the clock definition
// if component update happens on system clock and we have access to 
// clock properties use nextCycle information since clock produced by CDIV can have irregular period  
// and scheduling next clock edge based on measured previous period is unreliable
if( mHasSysClk && mClockProperties && mClockProperties->nextCycle > 0)
{
  mCyclesPerUpdate = mClockProperties->nextCycle;
}
else
{
  if (mCyclesPerUpdate != cyclesPerUpdate && cyclesPerUpdate > 0) {
    mCyclesPerUpdate = cyclesPerUpdate;
  }
}

if(mClockDef->isActive()) {
  mOwner->mSimulationQueue->removeClock(mClockDef);
}
// Edge 3 is the start of the clock pulse wave definition
mClockDef->reset(mOwner->mCarbonSimulationTime, 3);
mClockDef->setClockRatio(1, mCyclesPerUpdate);
mOwner->mSimulationQueue->addClock(mClockDef);
""")
    if drivesXtors:
      func.addCode("mOwner->%s_communicate();\n" % (name) )

    func = self.addMemberFunction('void', 'update')
    func.setDescription('Called by connected clock master.')
    # If there is an enable, check if it is high. If it isn't return immediately
    if rtlEnable:
      func.addCode('''
// Only execute the clock function if the enable is high.
// Note that if we delay the enable mEnValue is examined
// after we check it because Carbon model outputs change
// in the update phase and the transaction executes in
// the next cycles communicate phase.
//
// The delayed enable should be used to clock master
// transactors, while the lack of a delay enable
// should be used to clock slave transactors.
static const bool cDelayEnable = %s;
bool stop = false;
if (mEnValue == 0) {
  stop = true;
}
if (cDelayEnable) {
  carbonExamine(mOwner->getCarbonObject(), mEnNetID, &mEnValue, NULL);
}
if (stop) {
  return;
}
''' % delayEnable )

    if drivesXtors:
      func.addCode("mOwner->%s_update();\n" % (name) )

    func = self.addMemberFunction('void', 'terminate')
    func.setDescription('Called by component on CASIModule::terminate().')
    if drivesRtl:
      func.addCode("delete mClockDef;\n")
      
    self.addMemberFunction('void', 'casi_clocked')

    # ****************************************************
    # All members that can be changed after initialization
    # need to be saved/restored.
    # ***************************************************

    member = self.addMemberVariable(config.compName() + '*', 'mOwner', 'private')
    member.setDescription('Pointer to owning component')
    member.setConstructorInit('owner')
    
    if drivesRtl:
      member = self.addMemberVariable('CarbonClockSlaveDef*', 'mClockDef', 'private')
      member.setDescription("Clock definition object")
      member.setConstructorInit('NULL')

      member = self.addMemberVariable('eslapi::CASIClockedComponentsProperties*','mClockProperties','private')
      member.setDescription("Clock properties object updated by CDIV component")
      member.setConstructorInit('NULL')

      member = self.addMemberVariable('bool','mHasSysClk','private')
      member.setDescription("Owner component has access to system clock (it's clk-in port was left unconnected by user)")
      member.setConstructorInit('false')

      member = self.addMemberVariable('CarbonUInt64',  'mInitialClockCycles')
      member.setDescription('Initial value of clock cycles to use the first call to the clock slave.')
      member.setConstructorInit('1')

      member = self.addMemberVariable('CarbonUInt64',  'mInitialCompCycles')
      member.setDescription('Initial value of comp cycles to use the first call to the clock slave.')
      member.setConstructorInit('1')

      member = self.addMemberVariable('CarbonUInt64',  'mLastUpdateCycle')
      member.setDescription("Last time this clock's communicate/update were called")

      member = self.addMemberVariable('CarbonUInt64',  'mCyclesPerUpdate')
      member.setDescription('How often the update method is being called.')

      member = self.addMemberVariable('CarbonUInt64',  'mMasterComm')
      member.setDescription('Number of times the master has called communicate')
      member.setConstructorInit('0')

      func = self.addMemberFunction('CarbonClockSlaveDef*', 'getClockDef', inline = True)
      func.addCode("return mClockDef;")

    # If there is an enable, check if it is high. If it isn't return immediately
    if rtlEnable:
      member = self.addMemberVariable('CarbonUInt32',  'mEnValue')
      member.setDescription("The value of the clock enable for this cycle; this is used to clock the transactor next cycle.")
      member.setConstructorInit('0')

    if drivesXtors:
      for xtor in slaveXtors:
        member = self.addMemberVariable('bool *', boolMember(xtor))
        member.setConstructorInit(boolParam(xtor))

    if rtlEnable:
      member = self.addMemberVariable('CarbonNetID*', 'mEnNetID')
      member.setDescription('Enable RTL signal to create a slower clock')

    func = self.addMemberFunction('bool', 'saveData', '(eslapi::CASIODataStream &os)')
    func.setDescription('Save state to a stream')
    func.addCode('bool ok = true;')
    srVars = []
    if drivesRtl:
      srVars.append("mLastUpdateCycle")
      srVars.append("mCyclesPerUpdate")
      srVars.append("mMasterComm")
    if rtlEnable:
      srVars.append("mEnValue")
    if len(srVars) > 0:
      func.addCode("""
os << %s;
""" % " << ".join(srVars))
    if drivesRtl:
      func.addCode("""ok = mClockDef->saveData(os);\n""")
    func.addCode('return ok;')

    func = self.addMemberFunction('bool', 'restoreData', '(eslapi::CASIIDataStream &is)')
    func.setDescription('Restore state from a stream')
    func.addCode('bool ok = true;')
    if len(srVars) > 0:
      func.addCode("""
is >> %s;
""" % " >> ".join(srVars))
    if drivesRtl:
      func.addCode("""ok = mClockDef->restoreData(is);\n""")
    func.addCode('return ok;')

class MaxSimClockSlave(MaxSimPort):  
  def __init__(self, config, xtor):
    self.__drivesRtl     = False
    self.__netId         = ''
    self.__rtlEnable     = False
    self.__enNetId       = ''
    self.__initialValue  = 0
    self.__initialDelay  = 0
    self.__clockCycles   = 1
    self.__compCycles    = 1
    self.__dutyCycle     = 50

    # look for an RTL port connected to the "clock" output
    port = None
    for output in xtor.outputs():
      if output.name() == 'clock':
        port             = output
        self.__drivesRtl = True
        self.__netId     = port.carbonNetId()
        break

    # look for an RTL port connected to the "enable" input
    enablePort = None
    for input in xtor.inputs():
      if input.name() == 'enable':
        enablePort       = input
        self.__rtlEnable = True
        self.__enNetId   = enablePort.carbonNetId()

    # Get the enable and clock parameters. These modify the behavior
    # of the clock input transactor
    self.__registerClockPort = self.getParam(xtor, 'Clock Port Visible')
    self.__delayEnable = self.getParam(xtor, 'Delay Enable')
      
    # look for transactor ports driven by this clock
    self.__xtorSlaves = []
    for transactor in config.transactors():
      clockMaster = transactor.clockMaster()
      if clockMaster != None and clockMaster.name() == xtor.name():
        self.__xtorSlaves.append(transactor)
    self.__drivesXtors = len(self.__xtorSlaves) > 0

    width = 0
    if port != None:
      width = port.width()
    MaxSimPort.__init__(self, config.compName(), xtor.name(), width, config, '')

    if self.__drivesRtl:
      # create a clock generator
      self.__initialValue  = self.getParam(xtor, 'Initial Value')
      self.__initialDelay  = self.getParam(xtor, 'Initial Delay')
      self.__clockCycles   = self.getParam(xtor, 'Clock Cycles')
      self.__compCycles    = self.getParam(xtor, 'Comp Cycles')
      self.__dutyCycle     = self.getParam(xtor, 'Duty Cycle')

    # create an object to represent the code to be generated for this port's class
    self.__cgClass = ClockInputClass(self.className(), self.name(), config, xtor, self.__drivesRtl, self.__drivesXtors, self.__xtorSlaves, self.__rtlEnable, self.__delayEnable)

  def getParam(self, xtor, paramName):
    value = xtor.findParam(paramName)
    if value == None:
      raise error.XtorParamError('Clock_Input', xtor.name(), paramName)
    return value

  def className(self):
    return self.name() + '_CS'
  def instanceName(self):
    return self.name() + '_ClockSlave'
  def xtorSlaves(self):
    return self.__xtorSlaves
  def drivesXtors(self):
    return len(self.__xtorSlaves) > 0
  def needsSaveRestore(self):
    return True
  def isClockDefinition(self):
    # If this drives RTL, it has a CarbonClockDefinition
    return self.__drivesRtl
  def clockDefinitionInstance(self):
    # The underlying CarbonClockSlaveDef is the clock definition
    return self.instanceName() + "->getClockDef()"

  def instanceDeclaration(self):
    return """
  // port %(name)s
  %(className)s *%(instanceName)s;
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className() }
  def componentConstructor(self):
    if self.__drivesRtl:
      netId = self
    params = 'this'
    if self.__drivesXtors:
      for slaveXtor in self.__xtorSlaves:
        # find the component port for the driven transactor
        for compPort in self.config().compPorts():
          if slaveXtor.name() == compPort.name():
            params += ', &%s' % compPort.cycleClockVar()
            break
    return """
  %(instanceName)s = new %(className)s(%(params)s);
  bool registerClockPort_%(instanceName)s = %(registerClockPort)s;
  if (registerClockPort_%(instanceName)s) {
    registerPort(dynamic_cast< eslapi::CASIClockSlaveIF* >(%(instanceName)s), "%(name)s");
  }
""" % { 'name': self.name(),
        'instanceName': self.instanceName(),
        'className': self.className(),
        'params': params,
        'registerClockPort': self.__registerClockPort }
  def componentInterconnect(self):
    return """
  // connect clock slave %(name)s to its clock master
  CASIClockMasterIF* master_%(instanceName)s = %(instanceName)s->getMaster();
  if (master_%(instanceName)s == NULL)
    master_%(instanceName)s = this->getClockMaster();
  master_%(instanceName)s->registerClockSlave(%(instanceName)s);
""" % { 'instanceName': self.instanceName(), 'name': self.name() }
  def componentInit(self):
    code = ''
    params = ''
    sep = ''
    if self.__drivesRtl:
      params = '%s, %s, %s, scTicksPerCycle, %s, %s, %s' % (self.__netId, self.__clockCycles, self.__compCycles, self.__initialValue, self.__initialDelay, self.__dutyCycle)
      sep = ', '
    if self.__rtlEnable:
      params += '%s%s' % (sep, self.__enNetId)
    code += "  %(instanceName)s->init(%(params)s);\n" % { 'instanceName': self.instanceName(),
                                                          'params': params }
    return code

  def componentReset(self):
    return "  %(instanceName)s->reset();\n" % { 'instanceName': self.instanceName() }

  def componentEndOfReset(self):
    return "  %(instanceName)s->postReset();\n" % { 'instanceName': self.instanceName() }

  def componentTerminate(self):
    return "  %(instanceName)s->terminate();\n" % { 'instanceName': self.instanceName() }

  def classDefinition(self, userCode):
    return self.__cgClass.emitDefinition(userCode, None)

  def classImplementation(self, userCode):
    return self.__cgClass.emitImplementation(userCode, None)

class ClockGen(MaxSimPort):
  def __init__(self, config, width, name, netID, initialVal, delay, clockCycles, compCycles, dutyCycle):
    self.__netID        = netID
    self.__initialValue = initialVal
    self.__delay        = delay
    self.__clockCycles  = clockCycles
    self.__compCycles   = compCycles
    self.__dutyCycle    = dutyCycle
    MaxSimPort.__init__(self, config.compName(), name, width, config, '')

  def className(self):
    return 'CarbonClockGenDef'
  def instanceName(self):
    return 'mClock_' + self.name()
  def xtorSlaves(self):
    return self.__xtorSlaves
  def instanceDeclaration(self):
    return """
  // port %(name)s
  %(className)s *%(instanceName)s;
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className() }
  def componentInit(self):
    update_code = ''
    if self.drivesXtors(): update_code = """
  %(instName)s->addUpdateFunc(%(compName)s::%(name)sUpdateCb, this);
""" % { 'instName': self.instanceName(), 'compName': self.compName(), 'name': self.name()}

    return """  %(instName)s = new %(className)s(%(netID)s, %(clockCycles)s, %(compCycles)s, scTicksPerCycle, %(initVal)s, %(delay)s, %(dutyCycle)s);
  %(update)s
""" % { 'instName': self.instanceName(), 'className': self.className(), 'netID': self.__netID,
        'clockCycles': self.__clockCycles, 'compCycles': self.__compCycles,
        'initVal': self.__initialValue, 'delay': self.__delay, 'dutyCycle': self.__dutyCycle,
        'update': update_code }

  def componentReset(self):
    return """
  %(instanceName)s->reset(mCarbonSimulationTime);
  mSimulationQueue->addClock(%(instanceName)s);
""" % { 'instanceName': self.instanceName() }

  def componentCommunicate(self):
    code = ''
    if self.drivesXtors():
      code = """
  if(%(instanceName)s->hasEdgeThisCycle(mCarbonSimulationTime))
    %(name)s_communicate();
""" % {'instanceName': self.instanceName(), 'name': self.name() }
      
    return code
  
  def componentTerminate(self):
    return "  delete %(instanceName)s;\n" % { 'instanceName': self.instanceName() }

  def componentDestructor(self):
    return ''

  def drivesRtl(self):
    return self.__netID != 'NULL'
  def xtorSlaves(self):
    return []
  def drivesXtors(self):
    return False
  def needsSaveRestore(self):
    return True
  def isClockDefinition(self):
    return True
  def clockDefinitionInstance(self):
    return self.instanceName()
  def friendPortDef(self):
    # This is a common class that's never wrapped in a namespace
    return "  friend class %s;\n" % self.className()
  
class ClockGenPin(ClockGen):  
  def __init__(self, config, port):
    ClockGen.__init__(self, config, port.width(), port.shortName(), port.carbonNetId(), port.initialValue(), port.delay(), port.clockCycles(), port.compCycles(), port.dutyCycle())

class ClockGenXtor(ClockGen):  
  def __init__(self, config, port):
    self.__xtorSlaves = []

    def getParam(xtor, paramName):
      value = xtor.findParam(paramName)
      if value == None:
        raise error.XtorParamError('Clock_Input', xtor.name(), paramName)
      return value

    # look for an RTL port connected to the "clock" output
    width     = 0
    net_id = 'NULL'
    for output in port.outputs():
      if output.name() == 'clock':
        width     = output.width()
        net_id    = output.carbonNetId()
        break
    
    # look for transactor ports driven by this clock
    for transactor in config.transactors():
      clockMaster = transactor.clockMaster()
      if clockMaster != None and clockMaster.name() == port.name():
        # Now we need to find the port object that corresponds to 
        self.__xtorSlaves.append(transactor)

    initialValue  = getParam(port, 'Initial Value')
    delay         = getParam(port, 'Initial Delay')
    clockCycles   = getParam(port, 'Clock Cycles')
    compCycles    = getParam(port, 'Comp Cycles')
    dutyCycle     = getParam(port, 'Duty Cycle')

    ClockGen.__init__(self, config, width, port.name(), net_id, initialValue, delay, clockCycles, compCycles, dutyCycle)

  def xtorSlaves(self):
    return self.__xtorSlaves
  def drivesXtors(self):
    return len(self.__xtorSlaves) > 0

#  def componentCommunicate(self):
#    code = ''
#    for xtor in self.xtorSlaves():
#      code += """
#  if(%(instName)s->hasEdgeThisCycle(mCarbonSimulationTime))
#  %(xtorInst)s->communicate()

class ResetGen(MaxSimPort):  
  def __init__(self, config, port):
    MaxSimPort.__init__(self, config.compName(), port.shortName(), 1, config, '')
    self.__port         = port

  def className(self):
    return 'CarbonResetGenDef'
  def instanceName(self):
    return 'mReset_' + self.__port.shortName()
  def instanceDeclaration(self):
    return """
  // port %(name)s
  %(className)s *%(instanceName)s;
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className() }
  def componentInit(self):
    actVal   = "0x%x%xULL" % (self.__port.activeValueHigh(), self.__port.activeValueLow())
    inactVal = "0x%x%xULL" % (self.__port.inactiveValueHigh(), self.__port.inactiveValueLow())
    return "  %s = new %s(%s, %s, %s, scTicksPerCycle, %s, %s, %s, %s, %s);\n  %s->setName(\"%s\");\n" % (
  self.instanceName(), self.className(), self.__port.carbonNetId(),
  self.__port.clockCycles(), self.__port.compCycles(),
  actVal, inactVal, self.__port.cyclesBefore(), self.__port.cyclesAsserted(), self.__port.cyclesAfter(),
  self.instanceName(), self.instanceName())

  def componentReset(self):
    return """
  %(instanceName)s->reset(mCarbonSimulationTime);
  mSimulationQueue->addClock(%(instanceName)s);
""" % { 'instanceName': self.instanceName() }

  def componentTerminate(self):
    return "  delete %(instanceName)s;\n" % { 'instanceName': self.instanceName() }

  def componentDestructor(self):
    return ''

  def xtorSlaves(self):
    return []

  def isReset(self):
    return True
  def needsSaveRestore(self):
    return True
  def isClockDefinition(self):
    return True
  def clockDefinitionInstance(self):
    return self.instanceName()
  def friendPortDef(self):
    # This is a common class that's never wrapped in a namespace
    return "  friend class %s;\n" % self.className()

  def clockCycles(self):
    return self.__port.clockCycles()
  def compCycles(self):
    return self.__port.compCycles()
  def activeValue(self):
    return self.__port.activeValue()
  def activeValueHigh(self):
    return 0
  def activeValueLow(self):
    return self.__port.activeValue()
  def activeValueLow(self):
    return self.__port.activeValue()
  def inactiveValue(self):
    return self.__port.inactiveValue()
  def inactiveValueHigh(self):
    return 0
  def inactiveValueLow(self):
    return self.__port.inactiveValue()
  def cyclesBefore(self):
    return self.__port.cyclesBefore()
  def cyclesAsserted(self):
    return self.__port.cyclesAsserted()
  def cyclesAfter(self):
    return self.__port.cyclesAfter()

class ConfigurableWidthPort:
  def __init__(self, port, width):
    self.__port = port
    self.__width = width

  # delegate to port any method we do not define
  def __getattr__(self, name):
    return getattr(self.__port, name)

  def width(self):
    return self.__width

class ArmTransactorAdaptor(MaxSimPort):
  def __init__(self, compName, name, config, portFactory, xtor,
               bufferedMasters = False):
    MaxSimPort.__init__(self, compName, name, 0, config, '')
    self._portFactory = portFactory
    self._xtor = xtor
    self._bufferedMasters = bufferedMasters
    self._armInputs = []
    self._armDisconnects = []
    for input in self._xtor.inputs():
      if self.isInputPort(input.name()):
        width = self.getPortWidth(input)
        if width != input.width():
          input = ConfigurableWidthPort(input, width)
        self._armInputs.append(input)
    self._armInputs.sort(lambda a, b: cmp(a.name(), b.name()))

    # List unconnected ports with port expressions, so they can be evaluated during reset
    for port in self._xtor.unconnected():
      if port.expr() != '':
        self._armDisconnects.append(port)
    self._armDisconnects.sort(lambda a, b: cmp(a.name(), b.name()))

  def ports(self):
    for port in self._xtor.inputs():
      yield port
    for port in self._xtor.outputs():
      yield port
    for port in self._xtor.unconnected():
      yield port

  def connectedPorts(self):
    for port in self._xtor.inputs():
      yield port
    for port in self._xtor.outputs():
      yield port

  def findPort(self, port_name):
    for port in self.ports():
      if port.name() == port_name:
        return port
    return None
  
  # Use RTL width as the port width for the transactor port
  def getPortWidth(self, port):
    return port.rtlWidth()

  def isInputPort(self, name):
    # is a given port name a transactor input
    raise error.InternalError('must implement isInput()')

  def valueChangeFunc(self, input):
    return self.name() + "_" + input.name() + '_cb_func'

  def valueChangeClass(self, input):
    return self.name() + "_" + input.name() + '_cb_class'

  def valueChangeMember(self, input):
    return "m_" + self.name() + "_" + input.name() + '_cb_data';

  def supportFlowThru(self):
    return self._xtor.supportFlowThru();

  def needsSaveRestore(self):
    # Query the transactor object
    return self._xtor.supportSaveRestore()

  def loopValueChangeMembers(self):
    # Flow-through transactors have input value change members that need to be saved/restored
    if self._xtor.supportFlowThru():
      for input in self._armInputs:
        yield self.valueChangeMember(input)
    else:
      # I can't find a cleaner way to return an iterator of an empty list
      for x in []:
        yield x

  def libDepends(self):
    return ['arm_adaptors%s' % self.config().mxVer()]
  def libWinDepends(self):
    return ['libarm_adaptors%s%s.lib' % (self.config().mxVer(), self.config().mxCompilerLibSuffix())]
  def libWinDebugDepends(self):
    return ['libarm_adaptors%s%s_DBG.lib' % (self.config().mxVer(), self.config().mxCompilerLibSuffix())]
  def className(self):
    # name of the transactor adaptor class to instantiate
    raise error.InternalError('must implement className()')
  def instanceName(self):
    if self.config().standAloneComponent() and self.isTransactionSlave():
      return 'mDebugSlaves["%(name)s"]' % {'name': self.name()}
    return self.name() + '_' + self.className()
  def instanceDeclaration(self):
    code = """
  // port %(name)s
  %(className)s *%(instanceName)s;
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className() }
    if self.hasCycleClock():
      # define boolean flag for comminucate/update
      code += """
  bool %(var)s;
""" % { 'var': self.cycleClockVar() }
    return code

  def friendPortDef(self):
    return ''
  def componentInit(self):
    code = ''
    code += '  %(instanceName)s->init();\n' % { 'instanceName': self.instanceName() }
    return code
  def componentTerminate(self):
    return '  %(instanceName)s->terminate();\n' % { 'instanceName': self.instanceName() }
  def componentReset(self):
    return '  %(instanceName)s->reset(level, filelist);\n' % { 'instanceName': self.instanceName() }
  def componentCommunicate(self):
    code = """\
  %(instanceName)s->communicate();
""" % { 'instanceName': self.instanceName() }
    return code
  def componentUpdate(self):
    code = """\
  %(instanceName)s->update();
""" % { 'instanceName': self.instanceName() }
    return code
  def componentConstructor(self):
    return """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s);
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className(), 'portFactory': self._portFactory.name() }
  def debugAccessImplemented(self):
    if self._xtor.useDebugAccess() and (self.isTransactionSlave() or self._xtor.writeDebug() or self._xtor.debugTransaction()):
      return 1
    else:
      return 0
  def classImplementation(self, userCode):
    code = ""
    for input in self._armInputs:
      code += self.generateValueChangeFunc(input, userCode)
    for input in self._armDisconnects:
      code += self.generateValueExpressionFunc(input, userCode)

    if self.debugAccessImplemented():
      code += """
// this is the debugAccess function for Transactor port "%(name)s"
CarbonDebugAccessStatus %(name)s_debugAccess(eslapi::CASIModuleIF *compIF, CarbonDebugAccessDirection dir,
                                             uint64_t addr, uint32_t numBytes, uint8_t* buf, uint32_t* numBytesProcessed, uint32_t* ctrl)
{
  %(compName)s* comp = static_cast<%(compName)s*>(compIF);
""" % {'compName': self.compName(), 'name':self.name() }
      code += self.debugAccess(userCode)
      code += '}\n'

      code += """
#ifdef CARBON_MEMORY_RESTORE_PORT_%(name)s
// Optional routine to restore architectural memory through this debugAccess port
bool 
CarbonSaveRestoreArchCallback::restoreMemory(eslapi::CASIMemoryValueMap* memoryValueMap) { 
  bool ok = true; 
  for (eslapi::CASIMemoryValueMap::iterator iter = memoryValueMap->begin(); 
       (iter != memoryValueMap->end()) && ok; 
       ++iter) { 
    const eslapi::CASIMemoryRange& range = (*iter).first; 
    eslapi::CASIValueLenPair& valuepair = (*iter).second; 
    uint64_t address = range.first; 
    uint32_t size = range.second; 
    uint8_t* value = valuepair.first; 
    uint32_t numBytesAccepted, ctrl; 
    CarbonDebugAccessStatus status; 
    status = %(name)s_debugAccess(mComponent->getParent(), eCarbonDebugAccessWrite, address, size, value, &numBytesAccepted, &ctrl); 
    if (status == eCarbonDebugAccessStatusOk) 
      ok = true;  
    else 
      ok = false; 
  } 
  return ok; 
}
#endif
""" % { 'name':self.name() }

    return code

  def setTransactorPortWidth(self):
    # Set the port widths for all transactor ports
    code = '\n  // Set the port widths for all transactor ports\n'
    for port in self.connectedPorts():
      code += '  %(instanceName)s->carbonSetPortWidth("%(name)s", %(portWidth)d);\n' % {
        'instanceName': self.instanceName(), 'name': port.name().lower(), 'portWidth': port.rtlWidth() }
    code += '\n'
    return code
  
  # This method is shared between standalone and SoCDesigner
  # It only include the code inside the function, not the function itself
  def debugAccess(self, userCode): #{ 
    # Are there any registers that uses this port?
    hasRegs = False
    for cadi in self.config().loopCadi():
      for reg in cadi.debugRegs():
        if reg.eslPort() == self._xtor.name():
          hasRegs = True
          break

    # Are there any memories that uses this port?
    hasMems = False   
    for cadi in self.config().loopCadi():
      for mem in cadi.debugMemories():
        for sap in mem.systemAddressPorts():
          if sap.name() == self._xtor.name():
            hasMems = True
            break
        if hasMems:
          break
      if hasMems:
        break

    code = ''

    # Create instances of the various CADI classes
    # currently only needed if there are registers, but we always create the variables
    code += '\n  // Cast to CADI derived classes\n'
    for cadi in self.config().loopCadi():
      code += '  %(className)s* cadi_%(compName)s = static_cast<%(className)s*>(comp->%(inst)s);\n' % {
        'className': cadi.mxdiClassName(),
        'compName':  cadi.compName(),
        'inst': cadi.instance()
        }

    code += """
  // First call components debugAccess to see if it handles the entire transaction
  CarbonDebugAccessStatus ret = comp->debugAccess("%s", dir, addr, numBytes, buf, numBytesProcessed, ctrl);
  
  if (ret == eCarbonDebugAccessStatusOk)
    return ret;

  // If not partial we haven't processed any bytes yet.
  if (ret != eCarbonDebugAccessStatusPartial) *numBytesProcessed = 0;

  ret = eCarbonDebugAccessStatusError;

""" % (self._xtor.name())


    if hasMems | hasRegs : #{
      if not self.config().standAloneComponent():
        # { Base Address for SoCD Components
        code += """
  // Get Base address from transactor
  CarbonUInt32 offset = addr;
  CarbonUInt32 num_regions = comp->%(portInst)s->getNumRegions();
  ret = eCarbonDebugAccessStatusOutRange;
  if (num_regions > 0) {
    CarbonInternalMemMapEntry map(num_regions);
    comp->%(portInst)s->getAddressRegions(map.start, map.size, map.name);
    offset = addr - map.start[0];
  
    // Check if access is withing range of this component
    for (CarbonUInt32 i = 0; i < num_regions; i++) {
      if      ( ( map.start[i] <= addr ) && ((addr+numBytes) <= (map.start[i]+map.size[i]))) { // Fully fits
        ret = eCarbonDebugAccessStatusOk;
        break;
      }
      else if ( ( map.start[i] <= addr ) && (addr            <  (map.start[i]+map.size[i]))) { // Partially fits
        ret = eCarbonDebugAccessStatusPartial;
      }
    }
  }
"""  % { 'portInst': self.instanceName() }
        # }
      else:
        # { Base Address for Standalone Components
        code += """
  // Get Base address from transactor
  CarbonUInt32 offset = (CarbonUInt32)(addr - getBaseAddress());
"""  % { 'portInst': self.instanceName() }
        # }
      code += """
  // Process one byte at a time
  for(uint32_t i = *numBytesProcessed; i < numBytes; ++i) {
"""
         
      # First Start with the memories to see if we get a hit
      # when we do we are considered done. The memory ranges
      # between different memory spaces should not overlap
      # When using the same slave port

      if hasMems: #{
        code += """
    // Process Memories
    uint64_t baseOffset = 0;
    int64_t systemAddress = 0;
"""
        for cadi in self.config().loopCadi():      
          # Memory Access
          for mem in cadi.debugMemories():
            for sap in mem.systemAddressPorts():
              if sap.name() == self._xtor.name():
              # If the memory configured to display the memory at address 0, we need to handle the offset ourself
                if mem.displayAtZero():
                  code += '    baseOffset = ((((uint64_t) %(baseHi)s) << 32) | ((uint64_t) %(baseLo)s)) * %(byteWidth)d;  // displayAtZero is true, systemAddress: %(systemAddress)s\n' % {
                    'baseHi': mem.baseAddrHi(),
                    'baseLo': mem.baseAddrLo(),
                    'systemAddress': sap.baseAddress(),
                    'byteWidth': (mem.width()+7)/8}
                code += """
    systemAddress = %(systemAddress)sLL;  // for memory name: %(memName)s
    if ( 0 <= ( (offset+i) - systemAddress ) ) {
      uint64_t currentAddress = (offset+i) - systemAddress;
      if ((baseOffset <= currentAddress) && (currentAddress <= (baseOffset + 0x%(maxAddr)xULL))) {
        eslapi::CADIAddrComplete_t memAddr;
        memAddr.location.space = %(memIndex)d;
        memAddr.location.addr  = currentAddress - baseOffset;
        uint32_t actualUnits = 0;
        eslapi::CADIReturn_t status;
        if (dir == eCarbonDebugAccessWrite) {
          status = comp->%(inst)s->CADIMemWrite(memAddr, 1, 1, &buf[i], &actualUnits, false);
        }
        else {
          buf[i] = 0;
          status = comp->%(inst)s->CADIMemRead(memAddr, 1, 1, &buf[i], &actualUnits, false);
        }
        if (status == eslapi::CADI_STATUS_OK) {
          (*numBytesProcessed)++;
          continue; // This byte has been processed
        }
      }
    }
""" % { 'maxAddr': ((1 + mem.maxAddrs()) * ((mem.width()+7)/8))-1,
        'memIndex': mem.index(),
        'memName': mem.name(),
        'systemAddress': sap.baseAddress(),
        'inst': cadi.instance()}
      #} end of if hasMems

      # Register Access
      if hasRegs: #{
        code += """
    // Process Registers
    CarbonUInt32 shift = (CarbonUInt32)((addr+i)&3)*8;
    CarbonUInt32 mask  = 0xff;
    (*numBytesProcessed)++; // registers always process bytes
    if (dir == eCarbonDebugAccessRead) {
      CarbonUInt32 value = 0;
      buf[i]             = 0;
"""
        for cadi in self.config().loopCadi():
          reg_num = 0
          for reg in cadi.debugRegs():
            if reg.eslPort() == self._xtor.name():
              code += "      cadi_%s->carbonDebugRegs[%d]->readOffset(&value, offset+i, shift, mask);\n" % (cadi.compName(), reg_num)
              code += '      buf[i] |= (value&0xff);\n'
            reg_num = reg_num + 1

        code += """
    }
    else {
      CarbonUInt32 value = buf[i];
"""
        for cadi in self.config().loopCadi():
          reg_num = 0
          for reg in cadi.debugRegs():
            if reg.eslPort() == self._xtor.name():
              code += "      cadi_%s->carbonDebugRegs[%d]->writeOffset(&value, offset+i, shift, mask);\n" % (cadi.compName(), reg_num)
            reg_num = reg_num + 1

        code += """
    }"""
      #} end of if hasRegs

      code += """
  }
"""
    #}
    else : #{ no mems or regs
      code += """
  // There were no memories or registers to use to try and process the remaining bytes
  
"""
    #}

    code += """
  return ret;
"""    
    return code
  #} end debugAccess


  # Is this a transaction Master Port?
  def isTransactionMaster(self):
    return self._xtor.isMaster()
  
  # Is this a transaction Slave Port?
  def isTransactionSlave(self):
    return self._xtor.isSlave()
  
  def generateCallbackMembers(self):
    # Create a class member for buffered value change callbacks
    code = ''
    if self._bufferedMasters:
      for input in self._armInputs:
        code += '''\
  class %(valueChangeClass)s;
  %(valueChangeClass)s* %(valueChangeMember)s;
''' % { 'valueChangeClass': self.valueChangeClass(input),
        'valueChangeMember': self.valueChangeMember(input) }
    return code

  # This function emits the text for the callback data value. This is
  # different for buffered and direct callbacks.
  def generateCallbackDataValue(self, input):
    if self._bufferedMasters:
      return self.valueChangeMember(input)
    else:
      return '%(instanceName)s->carbonGetPort("%(port)s")' % \
             { 'instanceName': self.instanceName(),
               'port': input.name() }
  
  def generateValueChangeInits(self):
    code = ''
    for input in self._armInputs:
      # call the value change function with the current value
      code += '''\
  {
    CarbonUInt32 value[%(numWords)s];
    carbonExamine(mCarbonObj, %(netId)s, value, NULL);
    %(callback)s(mCarbonObj, %(netId)s, %(callbackDataValue)s, value, NULL);
  }
''' % { 'numWords': (input.width() + 31)/32,
        'netId': input.carbonNetId(),
        'callback': self.valueChangeFunc(input),
        'callbackDataValue': self.generateCallbackDataValue(input) }
        
    for port in self._armDisconnects:
      # call the value change function with the current value
      code += '''\
  %(callback)s(this, %(instanceName)s->carbonGetPort("%(port)s"));
''' % { 'callback': self.valueChangeFunc(port),
        'instanceName': self.instanceName(),
        'port': port.name() }
    
    return code

  # gen the arg list for a constructor
  def generateValueChangeInstanceCtorArgList(self, input, flagname):
    code = """\
%(instanceName)s->carbonGetPort("%(inputName)s"),%(instanceName)s->%(flagname)s
""" % {'instanceName': self.instanceName(),
       'inputName': input.name(),
       'flagname': flagname
       }
    return code

  # a change here requires a change in generateValueChangeInstanceCtorArgList
  def generateValueChangeCtor(self,valueChangeClass):
    # classname(void* data, bool &gChange): mChanged(gChange), mLastCycleExamined(0)
    code = """\
  %(valueChangeClass)s(void* data, bool &gChange)
  : mChanged(gChange), mLastCycleExamined(0)
""" % {'valueChangeClass': valueChangeClass }
    return code

  def generateValueChangeCallbacks(self):
    code = ''
    for input in self._armInputs: 
      if (self._bufferedMasters):
        flagname = ""
        if input.name() == "arready":
            flagname = "arReadyChanged"
        elif input.name() == "awready":
            flagname = "awReadyChanged"
        elif input.name() == "wready":
            flagname = "wReadyChanged"
        elif input.name() == "bready":
            flagname = "bReadyChanged"
        elif input.name() == "rready":
            flagname = "rReadyChanged"
        elif input.name().startswith("ar"):
            flagname = "arChanged"
        elif input.name().startswith("aw"):
            flagname = "awChanged"
        elif input.name().startswith("w"):
            flagname = "wChanged"
        elif input.name().startswith("r"):
            flagname = "rChanged"
        elif input.name().startswith("b"):
            flagname = "bChanged"
        code += """\
  %(valueChangeMember)s = new %(valueChangeClass)s(%(valueChangeCtorArgList)s);
  carbonAddNetValueChangeCB(mCarbonObj, %(valueChangeFunc)s, %(valueChangeMember)s, %(carbonNetId)s);
""" % { 'valueChangeClass': self.valueChangeClass(input),
        'valueChangeCtorArgList': self.generateValueChangeInstanceCtorArgList(input,flagname),
        'valueChangeMember': self.valueChangeMember(input),
        'valueChangeFunc': self.valueChangeFunc(input),
        'carbonNetId': input.carbonNetId()
        }
      else:
        code += '  carbonAddNetValueChangeCB(mCarbonObj, %s, %s->carbonGetPort("%s"), %s);\n' % \
                (self.valueChangeFunc(input), self.instanceName(),
                 input.name(), input.carbonNetId())
    return code


  def generateValueChangeFuncDirect(self, input, userCode):
    code = """
// value change callback function
static void %(valueChangeFunc)s(CarbonObjectID*, CarbonNetID*, void *data, CarbonUInt32 *val, CarbonUInt32*)
{
%(userCodePreTransactorInput)s
  // call driveSignal on given port
  eslapi::CASISignalSlave *port = reinterpret_cast<eslapi::CASISignalSlave *>(data);
%(generatePortExpr)s
  %(generateValueChangeDriveSignal)s
%(userCodePostTransactorInput)s
}
""" % { 'valueChangeFunc': self.valueChangeFunc(input),
        'generateValueChangeDriveSignal': self.generateValueChangeDriveSignal("port", "val", input.width()),
        'userCodePreTransactorInput': userCode.preSection(self.name() + '_' + input.name() + '::driveSignal'),
        'userCodePostTransactorInput': userCode.postSection(self.name() + '_' + input.name() + '::driveSignal'),
        'generatePortExpr': gGeneratePortExpr(input, "val[0]", "val")
        }
    return code

  def generateValueExpressionFunc(self, input, userCode):
    code = """
// value change callback function
static void %(valueChangeFunc)s(%(compName)s* comp, eslapi::CASISignalSlave*port)
{
%(userCodePreTransactorInput)s
  // call driveSignal on given port
  CarbonUInt32 val[%(width)d];
%(generatePortExpr)s
  %(generateValueChangeDriveSignal)s
%(userCodePostTransactorInput)s
}
""" % { 'compName': self.compName(),
        'valueChangeFunc': self.valueChangeFunc(input),
        'width': (input.width()+31)/32,
        'generateValueChangeDriveSignal': self.generateValueChangeDriveSignal("port", "val", input.width()),
        'userCodePreTransactorInput': userCode.preSection(self.name() + '_' + input.name() + '::driveSignal'),
        'userCodePostTransactorInput': userCode.postSection(self.name() + '_' + input.name() + '::driveSignal'),
        'generatePortExpr': gGeneratePortExpr(input, "val[0]", "val")
        }
    return code

  def generateChangeTracking(self):
    code = "mChanged = true;"
    return code


  def generateValueChangeFunctionDcl(self):
    code = """\
  //! Test if the value changed since the last time we did a put
  bool changed(void) const { return mChanged; }

  //! Clear the changed flag
  void clearChanged(void) { mChanged = false; }
"""
    return code


  def generateChangeTrackingVariableDecl(self):
    code = "bool& mChanged;"
    return code

  def generateValueChangeFuncBuffered(self, input, userCode):
    code = """
// Class to store slave port and buffer
class %(compName)s::%(valueChangeClass)s
{
public:
  // constructor
  %(valueChangeCtor)s
  {
    mPort = reinterpret_cast<eslapi::CASISignalSlave*>(data);
    memset(mVal, 0, sizeof(mVal));
  }

  //! Put a changed value
  void putValue(CarbonUInt32* val)
  {
%(generatePortExpr)s
%(userCodePreTransactorInputDrive)s
    %(generateValueChangeDriveSignal)s
%(userCodePreTransactorInputDrive)s
    mVal[0] = val[0];
%(changeTracking)s
  }

  CarbonUInt32 getValue0(void) const
  {
    return mVal[0];
  }
  
  //! Drive the buffered signal
  void driveSignal(void)
  {
%(userCodePreTransactorInputDrive)s

%(userCodePreTransactorInputDrive)s
  }

  //! Check if we are in a new cycle and update the stored value
  bool lastCycleChanged(uint64_t curCycle)
  {
    if (curCycle != mLastCycleExamined) {
      mLastCycleExamined = curCycle;
      return true;
    } else {
      return false;
    }
  }

%(valueChangeFunctionDcl)s

  //! Save state to a stream
  bool saveData(eslapi::CASIODataStream &os)
  {
    // Record whether this event is in the current and next event queues.
    // We manage saving/restoring the queues through these classes that
    // manipulate them rather than the queues themselves, because it's
    // simpler.  To save the queues themselves, we'd need a way to lookup
    // events in the queues by name.  The order of the events in the queue
    // doesn't matter, so this isn't an issue.  The events are just calls
    // into the transactor to indicate that a particular channel's values
    // have changed, and these are accumulated over the course of a cycle.

    bool inCurrQueue = currEventQueue.find(mEvent) != currEventQueue.end();
    bool inNextQueue = nextEventQueue.find(mEvent) != nextEventQueue.end();
    os.writeBytes(reinterpret_cast<const char*>(mVal), sizeof(CarbonUInt32) * %(numWords)d);
    os << inCurrQueue << inNextQueue << mLastCycleExamined;
    return true;
  }

  //! Restore state from a stream
  bool restoreData(eslapi::CASIIDataStream &is)
  {
    char* data = NULL;
    uint32_t numBytes = 0;
    bool inCurrQueue = false;
    bool inNextQueue = false;

    // Read value and make sure it's the right size.
    is.readBytes(data, numBytes);
    if (numBytes != sizeof(CarbonUInt32) * %(numWords)d) {
      return false;
    }
    is >> inCurrQueue >> inNextQueue >> mLastCycleExamined;

    memcpy(mVal, data, numBytes);
    delete [] data;

    // Insert event in queues if needed.  Remove it if it wasn't there, because we could be
    // restoring into a model at some point in the middle of simulation.

    if (inCurrQueue) {
      currEventQueue.insert(mEvent);
    } else {
      currEventQueue.erase(mEvent);
    }
    if (inNextQueue) {
      nextEventQueue.insert(mEvent);
    } else {
      nextEventQueue.erase(mEvent);
    }
    return true;
  }

private:

  // ****************************************************
  // All members that can be changed after initialization
  // need to be saved/restored.
  // ***************************************************

  //! Storage for the port
  eslapi::CASISignalSlave* mPort;

  //! Storage for the buffered data
  CarbonUInt32 mVal[%(numWords)d];

  //! Keeps track if this buffered port changed.
%(changeTrackingVariableDecl)s  

  //! Storage for the last time we checked if this changed
  uint64_t mLastCycleExamined;
};

//! Static callback function
static void %(valueChangeFunc)s(CarbonObjectID*, CarbonNetID*, void *data, CarbonUInt32 *val, CarbonUInt32*)
{
%(userCodePreTransactorInputBuffer)s
  // Store the value in the buffer
  %(compName)s::%(valueChangeClass)s* cbData = reinterpret_cast<%(compName)s::%(valueChangeClass)s*>(data);
  cbData->putValue(val);
%(userCodePostTransactorInputBuffer)s
}
""" % { 'valueChangeCtor' : self.generateValueChangeCtor(self.valueChangeClass(input)),
        'valueChangeFunc': self.valueChangeFunc(input),
        'valueChangeClass': self.valueChangeClass(input),
        'generateValueChangeDriveSignal': self.generateValueChangeDriveSignal("mPort", "val", input.width()),
        'userCodePreTransactorInputBuffer': userCode.preSection(self.name() + '_' + input.name() + '::bufferSignal'),
        'userCodePostTransactorInputBuffer': userCode.postSection(self.name() + '_' + input.name() + '::BufferSignal'),
        'userCodePreTransactorInputDrive': userCode.preSection(self.name() + '_' + input.name() + '::driveSignal'),
        'changeTracking': self.generateChangeTracking(),
        'userCodePostTransactorInputDrive': userCode.postSection(self.name() + '_' + input.name() + '::driveSignal'),
        'generatePortExpr': gGeneratePortExpr(input, "val[0]", "val"),
        'numBytes': ((input.width() + 31)/32) * 4,
        'numWords': (input.width() + 31)/32,
        'compName': self.compName(),
        'valueChangeFunctionDcl': self.generateValueChangeFunctionDcl(),
        'changeTrackingVariableDecl': self.generateChangeTrackingVariableDecl()
        }
    return code

  def generateValueChangeFunc(self, input, userCode):
    if self._bufferedMasters:
      return self.generateValueChangeFuncBuffered(input, userCode)
    else:
      return self.generateValueChangeFuncDirect(input, userCode)
  
  def generateValueChangeDriveSignal(self, port, val, width):
    if width > 32:
      return "%(port)s->driveSignal(%(val)s[0], &%(val)s[1]);" % { 'port' : port, 'val' : val }
    else:
      return "%(port)s->driveSignal(%(val)s[0], 0);" % { 'port' : port, 'val' : val }
    
  def hasCycleClock(self):
    return self._xtor.clockMaster() != None

  def cycleClockVar(self):
    return "mCarbonCalled_%s" % self.instanceName()

  ## is this function used anymore?
  # Only valid for T2S xactors
  def writeDbg(self):
    code = ""

    # Go through the registers and see there are any register access for this port
    has_regs = False
    for reg in self.config().debugRegs():
      if reg.eslPort() == self.name():
        has_regs = True
        break

    # Go through the memories and see there are any memories for this port
    has_mems = False
    for mem in self.config().debugMemories():            
      if mem.programPreloadEslPort() == self.name():
        has_mems = True
        break

    if has_regs or has_mems > 0:
      code += "  %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);\n" % { 'compClass': self.compName() }
      code += """
  // Get Base address from transactor
  CarbonUInt32 offset = addr;
  if (owner->%(portInst)s->getNumRegions() > 0) {
    CarbonInternalMemMapEntry map(owner->%(portInst)s->getNumRegions());
    owner->%(portInst)s->getAddressRegions(map.start, map.size, map.name);
    offset = addr - map.start[0];
  }""" % { 'portInst': self.instanceName() }

    # Debug Memories
    if has_mems:
      code += """\
  uint64_t baseOffset = 0;
  uint64_t wordAddr = offset >> 2;   // here offset is assumed to be a byte address and wordAddr is assumed to be a 32 bit word address
"""
    for mem in self.config().debugMemories():
      if mem.programPreloadEslPort() == self.name():
        # If the memory configured to display the memory at address 0, we need to handle the offset ourself
        if mem.displayAtZero():
          code += '  baseOffset = (((uint64_t) %(baseHi)s) << 32) | ((uint64_t) %(baseLo)s);  // displayAtZero is true\n' % {'baseHi': mem.baseAddrHi(), 'baseLo': mem.baseAddrLo()}
          
        code += """
  if ((baseOffset <= wordAddr) && (wordAddr <= (baseOffset + 0x%(maxAddr)xULL))) {
    eslapi::CADIAddrComplete_t memAddr;
    memAddr.location.space = %(memIndex)d;
    memAddr.location.addr  = offset - (baseOffset << 2); // here baseOffset is assumed to be a 32 bit word address, convert addr to a byte address
    uint32_t actualUnitsWritten = 0;
    // Only support single byte transfers for now
    uint8_t data = *value;
    owner->getCADI()->CADIMemWrite(memAddr, 1, 1, &data, &actualUnitsWritten, false);
  }
""" % { 'maxAddr': mem.maxAddrs(),
        'memIndex' : mem.index() }

    # Debug Registers
    if has_regs:
      # There may be different access types (byte, word, etc.) that need shifting/masking.
      code += self.generateDebugShiftMask()

    reg_num = 0
    for reg in self.config().debugRegs():
      if reg.eslPort() == self.name():
        code += "  owner->carbonDebugRegs[%d]->writeOffset(value, offset, shift, mask);\n" % reg_num
      reg_num = reg_num + 1

    if  has_regs or has_mems:
      code += "  return eslapi::CASI_STATUS_OK;\n"
    else:
      # If no memories or no address matches then return unsupported
      code += "  return eslapi::CASI_STATUS_NOTSUPPORTED;\n"
    return code

  ## is this function used anymore?
  def readDbg(self):
    code = ""

    # Go through the registers and see there are any register access for this port
    has_regs = False
    for reg in self.config().debugRegs():
      if reg.eslPort() == self.name():
        has_regs = True
        break

    if has_regs:
      code += "  %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);\n" % { 'compClass': self.compName() }
      code += """
  // Get Base address from transactor
  CarbonUInt32 offset = addr;
  if (owner->%(portInst)s->getNumRegions() > 0) {
    CarbonInternalMemMapEntry map(owner->%(portInst)s->getNumRegions());
    owner->%(portInst)s->getAddressRegions(map.start, map.size, map.name);
    offset = addr - map.start[0];
  }""" % { 'portInst': self.instanceName() }
      
    if has_regs:
      code += "  CarbonUInt32 readBuf = 0;"
      code += "  *value               = 0;"
      # There may be different access types (byte, word, etc.) that need shifting/masking.
      code += self.generateDebugShiftMask()
      
    # Debug Registers
    reg_num = 0
    for reg in self.config().debugRegs():
      if reg.eslPort() == self.name():
        code += """
  // Always read 32 bits at a time.
  // The registers return 0's when not addressed so or them all together
  owner->carbonDebugRegs[%d]->readOffset(&readBuf, offset, shift, mask);
  *value |= readBuf;""" % reg_num
      reg_num = reg_num + 1

    if has_regs:
      code += "  return eslapi::CASI_STATUS_OK;\n"
    else:
      # If no memories or no address matches then return unsupported
      code += "  return eslapi::CASI_STATUS_NOTSUPPORTED;\n"
      
    return code

  def checkFlowThru(self):
    if(self.supportFlowThru() == False):
      for input in self._xtor.inputs():
        if(self.config().isFlowThruOutput(input.carbonName())):
          self.config().output().warning("%d: %s: Flow Through Net connected to a transactor(%s) that doesn't support flow through. May not simulate as expected." \
                                         % (error.carbonCfgERR_NOFLOWSUPPORT, input.carbonName(), self.name()))
      for output in self._xtor.outputs():
        if(self.config().isFlowThruInput(output.carbonName())):
          self.config().output().warning("%d: %s: Flow Through Net connected to a transactor(%s) that doesn't support flow through. May not simulate as expected." \
                                         % (error.carbonCfgERR_NOFLOWSUPPORT, output.carbonName(), self.name()))
    
  def generateDebugShiftMask(self):
    # Default adaptor doesn't handle shifting/masking for debug reads and writes.
    # Accesses are assumed to be a full word.
    return """
  CarbonUInt32 shift = 0;
  CarbonUInt32 mask = 0xffffffff;
"""
      
class SDTransactor(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor,
               bufferedMasters = True):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor, True)

    # define parameters
    for param in xtor.params():
      # Create the basic parameter and add it to the configuration for the component
      xtorParam = parameters.ArmAdaptorParameter(param.name(), param.value(), param.type(), param.scope(),
                                                 xtor.name(), self.instanceName(), False, param.enumChoices())
      config.addParameter(xtorParam)
      
  def getPortWidth(self, port):
    return port.rtlWidth()
      
  def useXactorEventQueues(self):
    return self._xtor.useEventQueue()

  def preservePortNameCase(self):
    return self._xtor.preservePortNameCase()

  def hasDriveNotify(self):
    return True

  def isInputPort(self, name):
    port = self.findPort(name)
    if port != None:
      if port in self._xtor.inputs():
        return True
    return False

  def libPaths(self):
    return self._xtor.libPaths()
  def incPaths(self):
    return self._xtor.incPaths()

  def libDepends(self):
    return self._xtor.libFiles()
  def libWinDepends(self):
    liblist = []
    for l in self._xtor.libFiles(): liblist.append(l + '.lib')
    return liblist
  def libWinDebugDepends(self):
    liblist = []
    for l in self._xtor.libFiles(): liblist.append(l + '.lib')
    return liblist
  def className(self):
    return self._xtor.className()
  def componentCommunicate(self):
    if self._xtor.useCommunicate():
      return ArmTransactorAdaptor.componentCommunicate(self)
    return '' 
  def componentConstructor(self):
    code = '  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s' % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className(), 'portFactory': self._portFactory.name() }

    if not self._xtor.useDebugAccess():
      if self._xtor.writeDebug() == 1:
        code += ", %(name)s_readDbg, %(name)s_writeDbg" % { 'name': self.name() }
    
      if self._xtor.debugTransaction() == 1:
        code += ", %(name)s_debugTransaction" % { 'name': self.name() }

    elif self._xtor.writeDebug() or self._xtor.debugTransaction():
      code += ", %(name)s_debugAccess" % { 'name': self.name() }
      
      
    code += ", combinatorialCB"
    
    constructorArgs = self._xtor.constructorArgs()
    if constructorArgs != "":
      for arg in self._xtor.constructorArgs().split():
        if arg != "":
          code += ", %(arg)s" % { 'arg': arg }

    code += ");\n"

    # Register SystemC Ports
    for port in self._xtor.systemCPorts():
      portInst = '%s->%s' % (self.instanceName(), port.name());
      portName = self.name()
      if (port.name() != ""):
        portName = self.name() + '_' + port.name()
      if port.type() == eCarbonCfgScExport:
        code += '  registerSCGenericSlavePort(&%s, "%s");\n' % (portInst, portName)
      elif port.type() == eCarbonCfgScPort:
        code += '  registerSCGenericMasterPort(&%s, "%s");\n' % (portInst, portName)
        
    # Set the port widths for all transactor ports
    code += self.setTransactorPortWidth()      
    return code

  def classImplementation(self, userCode):
    code = ArmTransactorAdaptor.classImplementation(self, userCode)
    if not self._xtor.useDebugAccess():
      code += """
// this is the readDbg function for AXI Slave port "%(name)s"
// Note: ARM processor models with an AXI interface use the debugTransaction method
//       for backdoor access to the program memory.
static eslapi::CASIStatus %(name)s_readDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(readDbgUserCode)s
}

// this is the writeDbg function for AXI Slave port "%(name)s"
// Note: ARM processor models with an AXI interface use the debugTransaction method
//       for backdoor access to the program memory.
static eslapi::CASIStatus %(name)s_writeDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(writeDbgPreSection)s
%(writeDbg)s
%(writeDbgPostSection)s
}

// this is the debugTransaction function for AXI Slave port "%(name)s"
// Note: ARM processor models with an AXI interface use this method
//       for backdoor access to the program memory.
static eslapi::CASIStatus %(name)s_debugTransaction(eslapi::CASIModuleIF *comp, eslapi::CASITransactionInfo* info)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(debugTransactionPreSection)s
%(debugTransaction)s
%(debugTransactionPostSection)s
}
""" % { 'compClass': self.compName(),
        'name': self.name(),
        'readDbgUserCode': userCode.implementSection('%s_readDbg' % self.name(), '  ', '  return eslapi::CASI_STATUS_NOTSUPPORTED;\n'),
        'writeDbgPreSection': userCode.preSection('%s_writeDbg' % self.name(), '  '),
        'writeDbg': self.writeDbg(),
        'writeDbgPostSection': userCode.postSection('%s_writeDbg' % self.name(), '  '),
        'debugTransactionPreSection': userCode.preSection('%s_debugTransaction' % self.name(), '  '),
        'debugTransaction': self.debugTransaction(),
        'debugTransactionPostSection': userCode.postSection('%s_debugTransaction' % self.name(), '  '),
        }
    return code
  
  def componentInit(self):
    code = ''
    code += '  %(instanceName)s->init();\n' % { 'instanceName': self.instanceName() }
    code += self.generateValueChangeCallbacks()
    return code

  def componentReset(self):
    code = ArmTransactorAdaptor.componentReset(self)
    code += ArmTransactorAdaptor.generateValueChangeInits(self)
    return code

  def debugTransaction(self):
    return '  return eslapi::CASI_STATUS_NOTSUPPORTED;\n'

  # gen the arg list for a constructor
  def generateValueChangeInstanceCtorArgList(self, input):
    if input.clockSensitive():
      isvalid = "true"
    else:
      isvalid = "false"

    code = ""
    if self.useXactorEventQueues():
      clockMaster = self._xtor.clockMaster()
      if ( clockMaster != None ):
        clockId = clockMaster.name()
      else:
        clockId = "Default"

      code += "this->currEventQueue%(clkId)s, this->nextEventQueue%(clkId)s, " % {'clkId': clockId }
      code += """\
%(instanceName)s->carbonGetPort("%(inputName)s"),%(instanceName)s->%(eventName)s, %(isValid)s\
""" % {'instanceName': self.instanceName(),
     'inputName': input.name(),
     'eventName': input.eventName(),
     'isValid': isvalid }
    return code

  # a change here requires a change in generateValueChangeInstanceCtorArgList
  def generateValueChangeCtor(self,valueChangeClass):
    code = """\
%(valueChangeClass)s(set<CarbonXtorEvent*>& eq, set<CarbonXtorEvent*>& vq, void* data, CarbonXtorEvent* ev, bool valid=false)
  : mEvent(ev), mValidSignal(valid),currEventQueue(eq), nextEventQueue(vq)
""" % {'valueChangeClass': valueChangeClass }
    return code

  def generateValueChangeCallbacks(self):
    code = ''
    for input in self._armInputs: 
      if (self._bufferedMasters):
        code += """\
  %(valueChangeMember)s = new %(valueChangeClass)s(%(valueChangeCtorArgList)s);
  carbonAddNetValueChangeCB(mCarbonObj, %(valueChangeFunc)s, %(valueChangeMember)s, %(carbonNetId)s);
""" % { 'valueChangeClass': self.valueChangeClass(input),
        'valueChangeCtorArgList': self.generateValueChangeInstanceCtorArgList(input),
        'valueChangeMember': self.valueChangeMember(input),
        'valueChangeFunc': self.valueChangeFunc(input),
        'carbonNetId': input.carbonNetId()
        }
      else:
        code += '  carbonAddNetValueChangeCB(mCarbonObj, %s, %s->carbonGetPort("%s"), %s);\n' % \
                (self.valueChangeFunc(input), self.instanceName(),
                 input.name(), input.carbonNetId())
    return code

  def generateChangeTracking(self):
    code = ""
    if self.useXactorEventQueues():
      code += """\
    currEventQueue.insert(mEvent);
    if (mValidSignal)
    {
        if (mVal[0])
          nextEventQueue.insert(mEvent); // signal is a response signal and is asserted, remember this event is also needed next time
        else
          nextEventQueue.erase(mEvent); // signal is a response signal but not asserted, so nothing is needed next time
    }"""
    return code

  def generateChangeTrackingVariableDecl(self):
    code = """\
  set<CarbonXtorEvent*> &currEventQueue;
  set<CarbonXtorEvent*> &nextEventQueue;
  CarbonXtorEvent* mEvent;
  
  bool mValidSignal; // true if this is a response signal, like BVALID, or BREADY
"""
    return code
  def generateValueChangeFuncBuffered(self, input, userCode):
    code = """
// Class to store slave port and buffer
class %(compName)s::%(valueChangeClass)s
{
public:
  // constructor
  %(valueChangeCtor)s
  {
    mPort = reinterpret_cast<eslapi::CASISignalSlave*>(data);
    memset(mVal, 0, sizeof(mVal));
  }

  //! Put a changed value
  void putValue(CarbonUInt32* val)
  {
%(generatePortExpr)s
%(userCodePreTransactorInputDrive)s
    %(generateValueChangeDriveSignal)s
%(userCodePreTransactorInputDrive)s
    mVal[0] = val[0];
%(changeTracking)s
  }

  CarbonUInt32 getValue0(void) const
  {
    return mVal[0];
  }
  
  //! Drive the buffered signal
  void driveSignal(void)
  {
%(userCodePreTransactorInputDrive)s

%(userCodePreTransactorInputDrive)s
  }

  //! Save state to a stream
  bool saveData(eslapi::CASIODataStream &os)
  {
    // Record whether this event is in the current and next event queues.
    // We manage saving/restoring the queues through these classes that
    // manipulate them rather than the queues themselves, because it's
    // simpler.  To save the queues themselves, we'd need a way to lookup
    // events in the queues by name.  The order of the events in the queue
    // doesn't matter, so this isn't an issue.  The events are just calls
    // into the transactor to indicate that a particular channel's values
    // have changed, and these are accumulated over the course of a cycle.

    bool inCurrQueue = currEventQueue.find(mEvent) != currEventQueue.end();
    bool inNextQueue = nextEventQueue.find(mEvent) != nextEventQueue.end();
    os.writeBytes(reinterpret_cast<const char*>(mVal), sizeof(CarbonUInt32) * %(numWords)d);
    os << inCurrQueue << inNextQueue;
    return true;
  }

  //! Restore state from a stream
  bool restoreData(eslapi::CASIIDataStream &is)
  {
    char* data = NULL;
    uint32_t numBytes = 0;
    bool inCurrQueue = false;
    bool inNextQueue = false;

    // Read value and make sure it's the right size.
    is.readBytes(data, numBytes);
    if (numBytes != sizeof(CarbonUInt32) * %(numWords)d) {
      return false;
    }
    is >> inCurrQueue >> inNextQueue;

    memcpy(mVal, data, numBytes);
    delete [] data;

    // Insert event in queues if needed.  Remove it if it wasn't there, because we could be
    // restoring into a model at some point in the middle of simulation.

    if (inCurrQueue) {
      currEventQueue.insert(mEvent);
    } else {
      currEventQueue.erase(mEvent);
    }
    if (inNextQueue) {
      nextEventQueue.insert(mEvent); // signal is a response signal and is asserted, remember this event is also needed next time
    } else {
      nextEventQueue.erase(mEvent); // signal is a response signal but not asserted, so nothing is needed next time
    }
    return true;
  }

private:

  // ****************************************************
  // All members that can be changed after initialization
  // need to be saved/restored.
  // ***************************************************

  //! Storage for the port
  eslapi::CASISignalSlave* mPort;

  //! Storage for the buffered data
  CarbonUInt32 mVal[%(numWords)d];

  //! Keeps track if this buffered port changed.
%(changeTrackingVariableDecl)s  
};

//! Static callback function
static void %(valueChangeFunc)s(CarbonObjectID*, CarbonNetID*, void *data, CarbonUInt32 *val, CarbonUInt32*)
{
%(userCodePreTransactorInputBuffer)s
  // Store the value in the buffer
  %(compName)s::%(valueChangeClass)s* cbData = reinterpret_cast<%(compName)s::%(valueChangeClass)s*>(data);
  cbData->putValue(val);
%(userCodePostTransactorInputBuffer)s
}
""" % { 'valueChangeCtor' : self.generateValueChangeCtor(self.valueChangeClass(input)),
        'valueChangeFunc': self.valueChangeFunc(input),
        'valueChangeClass': self.valueChangeClass(input),
        'generateValueChangeDriveSignal': self.generateValueChangeDriveSignal("mPort", "val", input.rtlWidth()),
        'userCodePreTransactorInputBuffer': userCode.preSection(self.name() + '_' + input.name() + '::bufferSignal'),
        'userCodePostTransactorInputBuffer': userCode.postSection(self.name() + '_' + input.name() + '::BufferSignal'),
        'userCodePreTransactorInputDrive': userCode.preSection(self.name() + '_' + input.name() + '::driveSignal'),
        'changeTracking': self.generateChangeTracking(),
        'userCodePostTransactorInputDrive': userCode.postSection(self.name() + '_' + input.name() + '::driveSignal'),
        'generatePortExpr': gGeneratePortExpr(input, "val[0]", "val"),
        'numBytes': ((input.width() + 31)/32) * 4,
        'numWords': (input.width() + 31)/32,
        'compName': self.compName(),
        'changeTrackingVariableDecl': self.generateChangeTrackingVariableDecl()
        }
    return code

# User-defined component.
class UserComponent(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor)

    # define parameters
    for param in xtor.params():
      # Create the basic parameter and add it to the configuration for the component
      xtorParam = parameters.ArmAdaptorParameter(param.name(), param.value(), param.type(), param.scope(),
                                                 xtor.name(), self.instanceName(), False, param.enumChoices())
      config.addParameter(xtorParam)

  # we've got no data to go on. so if the config says it's an input, believe it.
  def isInputPort(self, name):
    return True

  # the class name comes from the user transaction def
  def className(self):
    return self._xtor.className()

  # User components optionally have support for debug access, so
  # override constructor much like the SDTransactor class does
  def componentConstructor(self):
    code = '  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s' % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className(), 'portFactory': self._portFactory.name() }


    # Other transactors (e.g. AXIv2) enable debug access by setting
    # the write_debug attribute in the XML file.  The transactor also
    # needs to support debug access, which is an attribute in the
    # .ccfg file.
    if self._xtor.useDebugAccess() and self._xtor.writeDebug():
      code += ", %(name)s_debugAccess" % { 'name': self.name() }

    code += ");\n"
    return code

  def componentInit(self): 
    code = ArmTransactorAdaptor.componentInit(self) 
    code += ArmTransactorAdaptor.generateValueChangeCallbacks(self)
    return code

# ARM296 DTCM Master (S2T) Tranactor
# At some point this will be generalized to other TCM interfaces
class ARM926DTCMMasterXtor(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor)

    # define parameters
    for param in xtor.params():
      # Create the basic parameter and add it to the configuration for the component
      xtorParam = parameters.ArmAdaptorParameter(param.name(), param.value(), param.type(), param.scope(),
                                                 xtor.name(), self.instanceName(), False, param.enumChoices())
      config.addParameter(xtorParam)

  # we've got no data to go on. so if the config says it's an input, believe it.
  def isInputPort(self, name):
    return True

  # the class name comes from the user transaction def
  def className(self):
    return self._xtor.className()

  # User components optionally have support for debug access, so
  # override constructor much like the SDTransactor class does
  def componentConstructor(self):
    code = '  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s' % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className(), 'portFactory': self._portFactory.name() }


    # Other transactors (e.g. AXIv2) enable debug access by setting
    # the write_debug attribute in the XML file.  The transactor also
    # needs to support debug access, which is an attribute in the
    # .ccfg file.
    if self._xtor.useDebugAccess() and self._xtor.writeDebug():
      code += ", %(name)s_debugAccess" % { 'name': self.name() }

    code += ");\n"
    return code

  def componentInit(self): 
    code = ArmTransactorAdaptor.componentInit(self) 
    code += ArmTransactorAdaptor.generateValueChangeCallbacks(self)
    return code

# ARM296 DTCM Slave (T2S) Tranactor
# At some point this will be generalized to other TCM interfaces
class ARM926DTCMSlaveXtor(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor)

    # define parameters
    for param in xtor.params():
      # Create the basic parameter and add it to the configuration for the component
      xtorParam = parameters.ArmAdaptorParameter(param.name(), param.value(), param.type(), param.scope(),
                                                 xtor.name(), self.instanceName(), False, param.enumChoices())
      config.addParameter(xtorParam)

  # we've got no data to go on. so if the config says it's an input, believe it.
  def isInputPort(self, name):
    return True

  # the class name comes from the user transaction def
  def className(self):
    return self._xtor.className()

  # User components optionally have support for debug access, so
  # override constructor much like the SDTransactor class does
  def componentConstructor(self):
    code = '  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s' % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className(), 'portFactory': self._portFactory.name() }


    # Other transactors (e.g. AXIv2) enable debug access by setting
    # the write_debug attribute in the XML file.  The transactor also
    # needs to support debug access, which is an attribute in the
    # .ccfg file.
    if self._xtor.useDebugAccess() and self._xtor.writeDebug():
      code += ", %(name)s_debugAccess" % { 'name': self.name() }

    code += ");\n"
    return code

  def componentInit(self): 
    code = ArmTransactorAdaptor.componentInit(self) 
    code += ArmTransactorAdaptor.generateValueChangeCallbacks(self)
    return code

class ArmAhbT2s(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor)
    config.addParameter(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Base Address", "0x0", 'integer', 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Size", "0x100000000", 'integer', 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Big Endian", "false", 'bool', 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Align Data", "false", 'bool', 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Filter HREADYIN", "false", 'bool', 'init-time', xtor.name(), self.instanceName()))
    # ARM defaults Subtract Base Address to true, we default to false
    # to be upward compatible.
    config.addParameter(parameters.ArmAdaptorParameter("Subtract Base Address", "false", 'bool', 'init-time', xtor.name(), self.instanceName()));
    # We (Carbon) made up the Subtract Base Address Dbg parameter, and
    # default it to true for slave transactors.
    config.addParameter(parameters.ArmAdaptorParameter("Subtract Base Address Dbg", "false", 'bool', 'init-time', xtor.name(), self.instanceName()));
    config.addParameter(parameters.ArmAdaptorParameter("ahb_start1", "0x00000000", 'integer', 'init-time', xtor.name(), self.instanceName()));
    config.addParameter(parameters.ArmAdaptorParameter("ahb_size1", "0x0", 'integer', 'init-time', xtor.name(), self.instanceName()));
  def isInputPort(self, name):
    return name in ('hresp', 'hreadyout', 'hrdata')

  def className(self):
    return 'CarbonAhbSlaveT2S'
  def componentInit(self):
    code = ArmTransactorAdaptor.componentInit(self)
    code += ArmTransactorAdaptor.generateValueChangeCallbacks(self);
    return code
  def componentReset(self):
    code = ArmTransactorAdaptor.componentReset(self)
    code += ArmTransactorAdaptor.generateValueChangeInits(self)
    return code
  def componentConstructor(self):
    if self._portFactory.adaptor() == 'AHBT2SLITE':
      isAhbLite = 'true'
    else:
      isAhbLite = 'false'

    code = ''
    if not self._xtor.useDebugAccess():
      code += """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_readDbg, %(name)s_writeDbg, %(isLite)s);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name(),
        'isLite': isAhbLite }
    else :
      code += """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_debugAccess, %(isLite)s);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name(),
        'isLite': isAhbLite }

    code += self.setTransactorPortWidth()
    return code
  
  def classImplementation(self, userCode):
    code = ArmTransactorAdaptor.classImplementation(self, userCode)
    if not self._xtor.useDebugAccess():
      code += """
// this is the readDbg function for AHB Slave port "%(name)s"
static eslapi::CASIStatus %(name)s_readDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(readDbgPreSection)s
%(readDbg)s
%(readDbgPostSection)s
}

// this is the writeDbg function for AHB Slave port "%(name)s"
static eslapi::CASIStatus %(name)s_writeDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(writeDbgPreSection)s
%(writeDbg)s
%(writeDbgPostSection)s
}
""" % { 'compClass': self.compName(),
        'name': self.name(),
        'readDbgPreSection': userCode.preSection('%s_readDbg' % self.name(), '  '),
        'readDbg': self.readDbg(),
        'readDbgPostSection': userCode.postSection('%s_readDbg' % self.name(), '  '),
        'writeDbgPreSection': userCode.preSection('%s_writeDbg' % self.name(), '  '),
        'writeDbg': self.writeDbg(),
        'writeDbgPostSection': userCode.postSection('%s_writeDbg' % self.name(), '  '),
        }
    return code


  def generateDebugShiftMask(self):
    # AHB T2S can currently support word and byte debug access.
    # Set the shift and mask values accordingly, and warn on anything else.
    return """
  CarbonUInt32 shift = 0;
  CarbonUInt32 mask = 0;
  AHB_ACCESS_TYPE access = static_cast<AHB_ACCESS_TYPE>(ctrl[AHB_IDX_TYPE]);
  switch (access) {
    case AHB_TYPE_BYTE:
      shift = (addr & 0x3) * 8;
      mask = 0xff;
      break;
    case AHB_TYPE_WORD:
      shift = 0;
      mask = 0xffffffff;
      break;
    default:
      owner->message(eslapi::CASI_MSG_WARNING, "Unsupported debug write access type: %d", access);
  }
"""
      


class CarbonXT2T(MaxSimPort):
  def __init__(self, compName, name, config, xtor, registerPort):
    MaxSimPort.__init__(self, compName, name, 0, config, '')
    self.__xtor = xtor
    self.__registerThese = registerPort

    # For backward compatibility default to top_level_name + "." + transactor name
    xtorPath = xtor.findParam('Transactor Path')
    if xtorPath == '' or xtorPath == None: xtorPath = config.topModuleName() + '.' + self.name()
    config.addParameter(parameters.ArmAdaptorParameter("Transactor Path", xtorPath, 'string', 'init-time', xtor.name(), self.instanceName()))

  def className(self):
    return 'CarbonXT2T'
  def instanceName(self):
    return self.name() + '_CarbonXT2T'
  def libDepends(self):
    return ['carbonxsocvsp%s' % self.config().mxVer(), 'carbonvspx']
  def libWinDepends(self):
    return ['libcarbonxsocvsp%s%s.lib' % (self.config().mxVer(), self.config().mxCompilerLibSuffix())]
  def libWinDebugDepends(self):
    return ['libcarbonxsocvsp%s%s_DBG.lib' % (self.config().mxVer(), self.config().mxCompilerLibSuffix())]
  def xtor(self):
    return self.__xtor
  def registerThese(self):
    return self.__registerThese
  def instanceDeclaration(self):
    return """
  // port %(name)s
  %(className)s *%(instanceName)s;
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className() }
  def friendPortDef(self):
    return ''
  def componentInit(self):
    return '  %(instanceName)s->init();\n' % { 'instanceName': self.instanceName() }
  def componentTerminate(self):
    return """
   %(instanceName)s->terminate();
""" % { 'instanceName': self.instanceName()}
  def componentReset(self):
    return """
   %(instanceName)s->reset(level, filelist);
""" % { 'instanceName': self.instanceName()}
  def componentUpdate(self):
    return '  %(instanceName)s->update();\n' % { 'instanceName': self.instanceName() }
  def componentCommunicate(self):
    return '  %(instanceName)s->communicate();\n' % { 'instanceName': self.instanceName() }
  def componentConstructor(self):
    return """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(registerThese)s, new %(xtorType)s_Reset);
""" % {
  'name': self.name(),
  'instanceName': self.instanceName(),
  'className': self.className(),
  'registerThese': self.registerThese(),
  'xtorType': self.xtor().type()}
  def classImplementation(self, userCode):
    return ''

class ArmAhbS2t(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor)
    config.addParameter(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Big Endian", "false", 'bool', 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Align Data", "false", 'bool', 'init-time', xtor.name(), self.instanceName()))

  def isInputPort(self, name):
    return name in ('hbreq', 'haddr', 'hburst', 'hprot', 'hsize', 'htrans', 'hwdata', 'hwrite', 'hlock', 'hbstrb', 'hunalign', 'hdomain')

  def className(self):
    return 'CarbonAhbMasterS2T'
  def componentConstructor(self):
    if self._portFactory.adaptor() == 'AHBS2TLITE':
      isAhbLite = 'true'
    else:
      isAhbLite = 'false'
    code =  """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(isLite)s);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name(),
        'isLite': isAhbLite }

    code += self.setTransactorPortWidth()
    return code

  def componentInit(self):
    code = ArmTransactorAdaptor.componentInit(self)
    code += ArmTransactorAdaptor.generateValueChangeCallbacks(self)
    return code
  def componentReset(self):
    code = ArmTransactorAdaptor.componentReset(self)
    code += ArmTransactorAdaptor.generateValueChangeInits(self)
    return code
  def instanceDeclaration(self):
    code = ArmTransactorAdaptor.instanceDeclaration(self)
    code += """\
  eslapi::CASIStatus %(name)s_readDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl);
  eslapi::CASIStatus %(name)s_writeDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl);
""" % { 'compName': self.compName(), 'name':self.name() }
    return code
  def classImplementation(self, userCode):
    code = ArmTransactorAdaptor.classImplementation(self, userCode)
    code += """\
// this function may be used to call the readDbg() method of AHB Master port "%(name)s"
eslapi::CASIStatus %(compName)s::%(name)s_readDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  return %(instanceName)s->readDbg(addr, value, ctrl);
}

// this function may be used to call the writeDbg() method of AHB Master port "%(name)s"
eslapi::CASIStatus %(compName)s::%(name)s_writeDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  return %(instanceName)s->writeDbg(addr, value, ctrl);
}
""" % { 'compName': self.compName(),
        'name':self.name(),
        'instanceName': self.instanceName() }
    return code

class ArmAhbMT2s(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor)
    config.addParameter(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Base Address", "0x0", 'integer', 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Size", "0x100000000", 'integer', 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("ahbm port ID", "0", 'integer', 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Big Endian", "false", 'bool', 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Align Data", "false", 'bool', 'init-time', xtor.name(), self.instanceName()))
    # ARM defaults Subtract Base Address to true, we default to false
    # to be upward compatible.
    config.addParameter(parameters.ArmAdaptorParameter("Subtract Base Address", "false", 'bool', 'init-time', xtor.name(), self.instanceName()))
    # We (Carbon) made up the Subtract Base Address Dbg parameter, and
    # default it to false for master transactors.
    config.addParameter(parameters.ArmAdaptorParameter("Subtract Base Address Dbg", "false", 'bool', 'init-time', xtor.name(), self.instanceName()));

  def isInputPort(self, name):
    return name in ('hresp', 'hreadyout', 'hrdata', 'hgrant')

  def className(self):
    return 'CarbonAhbMasterT2S'

  def componentInit(self):
    code = ArmTransactorAdaptor.componentInit(self)
    code += ArmTransactorAdaptor.generateValueChangeCallbacks(self)
    return code
  def componentReset(self):
    code = ArmTransactorAdaptor.componentReset(self)
    code += ArmTransactorAdaptor.generateValueChangeInits(self)
    return code
  def componentConstructor(self):
    code = ''
    if not self._xtor.useDebugAccess():
      code += """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_readDbg, %(name)s_writeDbg);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name()}
    else :
      code += """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_debugAccess);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name() }

    # Set the port widths for all transactor ports
    code += self.setTransactorPortWidth()
    return code
  
  def classImplementation(self, userCode):
    code = ArmTransactorAdaptor.classImplementation(self, userCode)
    return code + """
// this is the readDbg function for AHB Slave port "%(name)s"
static eslapi::CASIStatus %(name)s_readDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(readDbgUserCode)s
}

// this is the writeDbg function for AHB Slave port "%(name)s"
static eslapi::CASIStatus %(name)s_writeDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(writeDbgPreSection)s
%(writeDbg)s
%(writeDbgPostSection)s
}
""" % { 'compClass': self.compName(),
        'name': self.name(),
        'readDbgUserCode': userCode.implementSection('%s_readDbg' % self.name(), '  ', '  return eslapi::CASI_STATUS_NOTSUPPORTED;\n'),
        'writeDbgPreSection': userCode.preSection('%s_writeDbg' % self.name(), '  '),
        'writeDbg': self.writeDbg(),
        'writeDbgPostSection': userCode.postSection('%s_writeDbg' % self.name(), '  '),
        }

class ArmAhbSS2t(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor)
    config.addParameter(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Big Endian", "false", 'bool', 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Align Data", "false", 'bool', 'init-time', xtor.name(), self.instanceName()))

  def isInputPort(self, name):
    return name in ('haddr', 'hburst', 'hprot', 'hsize', 'htrans', 'hwdata',
                    'hwrite', 'hmastlock', 'hmaster', 'hready', 'hsel', 'hbstrb', 'hunalign', 'hdomain')

  def className(self):
    return 'CarbonAhbSlaveS2T'
  def componentConstructor(self):
    code = """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, false);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name() }
    code += self.setTransactorPortWidth()
    return code
  def componentInit(self):
    code = ArmTransactorAdaptor.componentInit(self)
    code += ArmTransactorAdaptor.generateValueChangeCallbacks(self)
    return code
  def componentReset(self):
    code = ArmTransactorAdaptor.componentReset(self)
    code += ArmTransactorAdaptor.generateValueChangeInits(self)
    return code
  def instanceDeclaration(self):
    code = ArmTransactorAdaptor.instanceDeclaration(self)
    code += """\
  eslapi::CASIStatus %(name)s_readDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl);
  eslapi::CASIStatus %(name)s_writeDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl);
""" % { 'compName': self.compName(), 'name':self.name() }
    return code
  def classImplementation(self, userCode):
    code = ArmTransactorAdaptor.classImplementation(self, userCode)
    code += """\
// this function may be used to call the readDbg() method of AHB Master port "%(name)s"
eslapi::CASIStatus %(compName)s::%(name)s_readDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  return %(instanceName)s->readDbg(addr, value, ctrl);
}

// this function may be used to call the writeDbg() method of AHB Master port "%(name)s"
eslapi::CASIStatus %(compName)s::%(name)s_writeDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  return %(instanceName)s->writeDbg(addr, value, ctrl);
}
""" % { 'compName': self.compName(),
        'name':self.name(),
        'instanceName': self.instanceName() }
    return code

class ArmApbT2s(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor)
    config.addParameter(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Base Address", "0x0", 'integer', 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Size", "0x100000000", 'integer', 'init-time', xtor.name(), self.instanceName()))

  def isInputPort(self, name):
    return name in ('prdata', 'pready', 'pslverr')
  
  def className(self):
    return 'CarbonApbT2S'
  def componentInit(self):
    code = ArmTransactorAdaptor.componentInit(self)
    code += ArmTransactorAdaptor.generateValueChangeCallbacks(self)
    return code
  def componentReset(self):
    code = ArmTransactorAdaptor.componentReset(self)
    code += ArmTransactorAdaptor.generateValueChangeInits(self)
    return code
  def componentConstructor(self):
    if not self._xtor.useDebugAccess():
      return """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_readDbg, %(name)s_writeDbg);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name()}
    else :
      return """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_debugAccess);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name() }
  def classImplementation(self, userCode):
    code = ArmTransactorAdaptor.classImplementation(self, userCode)
    return code + """
// this is the readDbg function for APB Slave port "%(name)s"
static eslapi::CASIStatus %(name)s_readDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(readDbgPreSection)s
%(readDbg)s
%(readDbgPostSection)s
}

// this is the writeDbg function for APB Slave port "%(name)s"
static eslapi::CASIStatus %(name)s_writeDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(writeDbgPreSection)s
%(writeDbg)s
%(writeDbgPostSection)s
}
""" % { 'compClass': self.compName(),
        'name': self.name(),
        'readDbgPreSection': userCode.preSection('%s_readDbg' % self.name(), '  '),
        'readDbg': self.readDbg(),
        'readDbgPostSection': userCode.postSection('%s_readDbg' % self.name(), '  '),
        'writeDbgPreSection': userCode.preSection('%s_writeDbg' % self.name(), '  '),
        'writeDbg': self.writeDbg(),
        'writeDbgPostSection': userCode.postSection('%s_writeDbg' % self.name(), '  '),
        }

class ArmApbS2t(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor)

    # Is This an APB3 Adaptor
    self.__apb3 = 'false'
    if xtor.type() == 'APB3_Master': self.__apb3 = 'true'

    config.addParameter(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))
    if self.__apb3 == 'true':
      config.addParameter(parameters.ArmAdaptorParameter("PReady Default High", "true", 'bool', 'run-time', xtor.name(), self.instanceName()))
      
  def isInputPort(self, name):
    return name in ('psel', 'penable', 'pwrite', 'paddr', 'pwdata')
  
  def className(self):
    return 'CarbonApbS2T'
  def componentInit(self):
    code = ArmTransactorAdaptor.componentInit(self)
    code += ArmTransactorAdaptor.generateValueChangeCallbacks(self)
    return code
  def componentReset(self):
    code = ArmTransactorAdaptor.componentReset(self)
    code += ArmTransactorAdaptor.generateValueChangeInits(self)
    return code
  def componentConstructor(self):
    return """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(apb3)s, %(name)s_readDbg, %(name)s_writeDbg);
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className(), 'portFactory': self._portFactory.name(), 'apb3' : self.__apb3}
  def classImplementation(self, userCode):
    code = ArmTransactorAdaptor.classImplementation(self, userCode)
    return code + """
// this is the readDbg function for APB Master port "%(name)s"
static eslapi::CASIStatus %(name)s_readDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(readDbgUserCode)s
}

// this is the writeDbg function for APB Master port "%(name)s"
static eslapi::CASIStatus %(name)s_writeDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(writeDbgUserCode)s
}
""" % { 'compClass': self.compName(),
        'name': self.name(),
        'readDbgUserCode': userCode.implementSection('%s_readDbg' % self.name(), '  ', '  return eslapi::CASI_STATUS_NOTSUPPORTED;\n'),
        'writeDbgUserCode': userCode.implementSection('%s_writeDbg' % self.name(), '  ', '  return eslapi::CASI_STATUS_NOTSUPPORTED;\n'),
        }

class ArmDebugS(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor)
    config.addParameter(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Base Address", "0x0", 'integer', 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Size", "0x100000000", 'integer', 'init-time', xtor.name(), self.instanceName()))

  def isInputPort(self, name):
    return name in ('')
  
  def className(self):
    return 'CarbonDebugS'
  def componentInit(self):
    code = ArmTransactorAdaptor.componentInit(self)
    return code
  def componentReset(self):
    code = ArmTransactorAdaptor.componentReset(self)
    return code
  def componentConstructor(self):
    if not self._xtor.useDebugAccess():
      return """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_readDbg, %(name)s_writeDbg);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name()}
    else :
      return """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_debugAccess);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name() }
  def classImplementation(self, userCode):
    code = ArmTransactorAdaptor.classImplementation(self, userCode)
    return code + """
// this is the readDbg function for Debug Slave port "%(name)s"
static eslapi::CASIStatus %(name)s_readDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(readDbgPreSection)s
%(readDbg)s
%(readDbgPostSection)s
}

// this is the writeDbg function for Debug Slave port "%(name)s"
static eslapi::CASIStatus %(name)s_writeDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(writeDbgPreSection)s
%(writeDbg)s
%(writeDbgPostSection)s
}
""" % { 'compClass': self.compName(),
        'name': self.name(),
        'readDbgPreSection': userCode.preSection('%s_readDbg' % self.name(), '  '),
        'readDbg': self.readDbg(),
        'readDbgPostSection': userCode.postSection('%s_readDbg' % self.name(), '  '),
        'writeDbgPreSection': userCode.preSection('%s_writeDbg' % self.name(), '  '),
        'writeDbg': self.writeDbg(),
        'writeDbgPostSection': userCode.postSection('%s_writeDbg' % self.name(), '  '),
        }

class ArmDebugM(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor)

    config.addParameter(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))

  def isInputPort(self, name):
    return name in ('')
  
  def className(self):
    return 'CarbonDebugM'
  def componentInit(self):
    code = ArmTransactorAdaptor.componentInit(self)
    return code
  def componentReset(self):
    code = ArmTransactorAdaptor.componentReset(self)
    return code
  def componentConstructor(self):
    return """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_readDbg, %(name)s_writeDbg);
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className(), 'portFactory': self._portFactory.name()}
  def classImplementation(self, userCode):
    code = ArmTransactorAdaptor.classImplementation(self, userCode)
    return code + """
// this is the readDbg function for Debug Master port "%(name)s"
static eslapi::CASIStatus %(name)s_readDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(readDbgUserCode)s
}

// this is the writeDbg function for Debug Master port "%(name)s"
static eslapi::CASIStatus %(name)s_writeDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(writeDbgUserCode)s
}
""" % { 'compClass': self.compName(),
        'name': self.name(),
        'readDbgUserCode': userCode.implementSection('%s_readDbg' % self.name(), '  ', '  return eslapi::CASI_STATUS_NOTSUPPORTED;\n'),
        'writeDbgUserCode': userCode.implementSection('%s_writeDbg' % self.name(), '  ', '  return eslapi::CASI_STATUS_NOTSUPPORTED;\n'),
        }

class CarbonXPcie(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor):
    import re
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor)
    self.__configExpr = re.compile(r'CarbonX_Pcie_([0-9]+)Lane_(10Bit|Pipe)')
    self.__numLanes = self.__configExpr.match(xtor.type()).group(1)
    self.__intf = "eCarbonXPcie%sIntf" % self.__configExpr.match(xtor.type()).group(2)
    config.addParameter(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("MaxPayloadSize", "0", 'integer', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Read Completion Boundary", "128", 'integer', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("MaxInFlight", "32", 'integer', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Scramble Status", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Do Training", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Training Bypass", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Idle Is Hi True", "true", 'bool', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Force Idle Inactive", "false", 'bool', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Transactor Debug Print Level", "1", 'integer', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Bus Number", "0", 'integer', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Device Number", "0", 'integer', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Function Number", "0", 'integer', 'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("Supported Speeds", "1", 'integer', 'run-time', xtor.name(), self.instanceName()))

  def isInputPort(self, name):
    return name in [p.name() for p in self._xtor.inputs()]

  def libDepends(self):
    return ['carbonxsocvsp%s' % self.config().mxVer(), 'carbonvspx']
  def libWinDepends(self):
    return ['libcarbonxsocvsp%s%s.lib' % (self.config().mxVer(), self.config().mxCompilerLibSuffix())]
  def libWinDebugDepends(self):
    return ['libcarbonxsocvsp%s%s_DBG.lib' % (self.config().mxVer(), self.config().mxCompilerLibSuffix())]

  def className(self):
    return 'CarbonXPcie'
  def componentInit(self):
    code = ArmTransactorAdaptor.componentInit(self)
    for input in self._armInputs:
      expr = re.compile(r'([a-z|A-Z]*)([0-9]+)')
      m = expr.match(input.name())
      if m != None : code += '  %s->connect(eCarbonXPcie%s, %s, %s);\n' % (self.instanceName(), m.group(1), m.group(2), input.carbonNetId())
      else : code += '  %s->connect(eCarbonXPcie%s, 0, %s);\n' % (self.instanceName(), input.name(), input.carbonNetId())
    return code
  def componentReset(self):
    code = ArmTransactorAdaptor.componentReset(self)
    return code
  def componentConstructor(self):
    return """
  %(instanceName)s = new %(className)s(this, "%(name)s", %(intf)s, %(numLanes)s, &mCarbonObj, %(portFactory)s);
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className(),
  'intf' : self.__intf, 'numLanes': self.__numLanes, 'portFactory': self._portFactory.name() }
  def classImplementation(self, userCode):
    return ArmTransactorAdaptor.classImplementation(self, userCode)
  def componentClockControl(self):
    # go through all the clocks, trying to find the
    # one that controls us
    clock_inst_name = ''
    rate_change_valid = False
    if (self.hasCycleClock()):
      for clock in self.config().compClockPorts():
        if (clock.name() != None) and (self._xtor.clockMaster().name() == clock.name() and clock.className() == 'CarbonClockGenDef'):
          clock_inst_name = clock.instanceName()
          rate_change_valid = True

    code = ''
    if rate_change_valid:
      code =  """
  // setup transactor %(instance)s to change generated clock speeds
  %(instance)s->setRateChangeCallbackFn(%(clock_inst_name)s, %(module)s::setClockGeneratorMultiple);
""" % {'instance': self.instanceName(),
       'clock_inst_name': clock_inst_name,
       'module': self.config().compName() }
    else:
      code = """
  // Warn if transactor is trying to change speed
  %(instance)s->setRateChangeCallbackFn(this, %(module)s::warnSpeedChange);
""" % {'instance': self.instanceName(),
       'module': self.config().compName() }

    return code
  
class ArmAxiT2sBase(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor, bufferedMasters):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor, bufferedMasters)
    config.addParameter(parameters.ArmAdaptorParameter("Enable Debug Messages", "false",	    "bool",  'run-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("axi_start0", "0x00000000",  "integer", 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("axi_size0",  "0x100000000", "integer", 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("axi_start1", "0x00000000",  "integer", 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("axi_size1",  "0x0", 	 "integer", 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("axi_start2", "0x00000000",  "integer", 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("axi_size2",  "0x0", 	 "integer", 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("axi_start3", "0x00000000",  "integer", 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("axi_size3",  "0x0", 	 "integer", 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("axi_start4", "0x00000000",  "integer", 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("axi_size4",  "0x0", 	 "integer", 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("axi_start5", "0x00000000",  "integer", 'init-time', xtor.name(), self.instanceName()))
    config.addParameter(parameters.ArmAdaptorParameter("axi_size5",  "0x0", 	 "integer", 'init-time', xtor.name(), self.instanceName()))

  def isInputPort(self, name):
    return name in ('awready', 'wready', 'bid', 'bresp', 'buser',
                    'bvalid', 'arready', 'rid', 'rdata', 'rresp',
                    'rlast', 'ruser', 'rvalid')

  def componentInit(self):
    code = ArmTransactorAdaptor.componentInit(self)
    code += ArmTransactorAdaptor.generateValueChangeCallbacks(self)
    return code
  def componentReset(self):
    code = ArmTransactorAdaptor.componentReset(self)
    code += ArmTransactorAdaptor.generateValueChangeInits(self)
    return code

  def classImplementation(self, userCode):
    code = ArmTransactorAdaptor.classImplementation(self, userCode)
    return code + """
// this is the readDbg function for AXI Slave port "%(name)s"
// Note: ARM processor models with an AXI interface use the debugTransaction method
//       for backdoor access to the program memory.
static eslapi::CASIStatus %(name)s_readDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(readDbgUserCode)s
}

// this is the writeDbg function for AXI Slave port "%(name)s"
// Note: ARM processor models with an AXI interface use the debugTransaction method
//       for backdoor access to the program memory.
static eslapi::CASIStatus %(name)s_writeDbg(eslapi::CASIModuleIF *comp, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(writeDbgPreSection)s
%(writeDbg)s
%(writeDbgPostSection)s
}

// this is the debugTransaction function for AXI Slave port "%(name)s"
// Note: ARM processor models with an AXI interface use this method
//       for backdoor access to the program memory.
static eslapi::CASIStatus %(name)s_debugTransaction(eslapi::CASIModuleIF *comp, eslapi::CASITransactionInfo* info)
{
  // %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
%(debugTransactionPreSection)s
%(debugTransaction)s
%(debugTransactionPostSection)s
}
""" % { 'compClass': self.compName(),
        'name': self.name(),
        'readDbgUserCode': userCode.implementSection('%s_readDbg' % self.name(), '  ', '  return eslapi::CASI_STATUS_NOTSUPPORTED;\n'),
        'writeDbgPreSection': userCode.preSection('%s_writeDbg' % self.name(), '  '),
        'writeDbg': self.writeDbg(),
        'writeDbgPostSection': userCode.postSection('%s_writeDbg' % self.name(), '  '),
        'debugTransactionPreSection': userCode.preSection('%s_debugTransaction' % self.name(), '  '),
        'debugTransaction': self.debugTransaction(),
        'debugTransactionPostSection': userCode.postSection('%s_debugTransaction' % self.name(), '  '),
        }

  def debugTransaction(self):
    return '  return eslapi::CASI_STATUS_NOTSUPPORTED;\n'
  
class ArmAxiT2s(ArmAxiT2sBase):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmAxiT2sBase.__init__(self, compName, name, config, portFactory, xtor, False)

  def className(self):
    return 'CarbonAxiT2S'

  def componentConstructor(self):
    code = ''
    if not self._xtor.useDebugAccess():    
      code += """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_readDbg, %(name)s_writeDbg, %(name)s_debugTransaction);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name()}
    else: 
      code += """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_debugAccess);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name()}
    code += self.setTransactorPortWidth()
    return code


# Arm AXI Transactor that supports Flow thru
class ArmAxiFT2s(ArmAxiT2sBase):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmAxiT2sBase.__init__(self, compName, name, config, portFactory, xtor, True)
  def className(self):
    return 'CarbonAxiFT2S'
  # gen the arg list for a constructor
  def generateValueChangeInstanceCtorArgList(self, input, flagname):
    isvalidflag = "false"
    # some axi ports are valid
    if input.name() == "arready":
      isvalidflag = "true"
    elif input.name() == "awready":
      isvalidflag = "true"
    elif input.name() == "wready":
      isvalidflag = "true"
    elif input.name() == "bready":
      isvalidflag = "true"
    elif input.name() == "rready":
      isvalidflag = "true"
    elif input.name() == "arvalid":
      isvalidflag = "true"
    elif input.name() == "awvalid":
      isvalidflag = "true"
    elif input.name() == "wvalid":
      isvalidflag = "true"
    elif input.name() == "rvalid":
      isvalidflag = "true"
    elif input.name() == "bvalid":
      isvalidflag = "true"

    # with eventQueues the event queue variables are the first args to ctor, and a valid flag is trailing
    code = ""
    if self.useXactorEventQueues():
      clockMaster = self._xtor.clockMaster()
      if ( clockMaster != None ):
        clockId = clockMaster.name()
      else:
        clockId = "Default"

      code += "this->currEventQueue%(clkId)s, this->nextEventQueue%(clkId)s, " % {'clkId': clockId }
    code += """\
%(instanceName)s->carbonGetPort("%(inputName)s"),%(instanceName)s->%(flagname)s\
""" % {'instanceName': self.instanceName(),
     'inputName': input.name(),
     'flagname': flagname}
    if self.useXactorEventQueues():
      code += "Func, %(isvalidflag)s" % {'isvalidflag': isvalidflag}
    return code

  # a change here requires a change in generateValueChangeInstanceCtorArgList
  def generateValueChangeCtor(self,valueChangeClass):
    # classname(set<CarbonXtorEvent*>& eq, set<CarbonXtorEvent*>& vq, void* data, CarbonXtorEvent* ev, bool valid=false): mLastCycleExamined(0), mEvent(ev), mValidSignal(valid), currEventQueue(eq), nextEventQueue(vq)
    code = "%(valueChangeClass)s" % {'valueChangeClass': valueChangeClass }
    if self.useXactorEventQueues():
      code += """\
(set<CarbonXtorEvent*>& eq, set<CarbonXtorEvent*>& vq, void* data, CarbonXtorEvent* ev, bool valid=false)
  : mLastCycleExamined(0), mEvent(ev), mValidSignal(valid),currEventQueue(eq), nextEventQueue(vq)
""" 
    else:
      #  (void* data, bool &gChange): mChanged(gChange), mLastCycleExamined(0)
      code += """\
(void* data, bool &gChange)
  : mChanged(gChange), mLastCycleExamined(0)
"""
    return code

  def generateChangeTracking(self):
    if self.useXactorEventQueues():
      code = """\
    currEventQueue.insert(mEvent);
    if (mValidSignal)
    {
        if (mVal[0])
          nextEventQueue.insert(mEvent); // signal is a response signal and is asserted, remember this event is also needed next time
        else
          nextEventQueue.erase(mEvent); // signal is a response signal but not asserted, so nothing is needed next time
    }"""
    else:
      # use the definition from base class      
      code = ArmAxiT2sBase.generateChangeTracking(self)
    return code

  def generateChangeTrackingVariableDecl(self):
    if self.useXactorEventQueues():
      code = """\
  set<CarbonXtorEvent*> &currEventQueue;
  set<CarbonXtorEvent*> &nextEventQueue;
  CarbonXtorEvent* mEvent;
  
  bool mValidSignal; // true if this is a response signal, like BVALID, or BREADY
"""
    else:
      # use the definition from base class
      code = ArmAxiT2sBase.generateChangeTrackingVariableDecl(self)
    return code

  def generateValueChangeFunctionDcl(self):
    if self.useXactorEventQueues():
      code = "// no value change functions defined"
    else:
      # use the definition from base class
      code = ArmAxiT2sBase.generateValueChangeFunctionDcl(self)
    return code
  
  def componentConstructor(self):
    code = ''
    if not self._xtor.useDebugAccess():
      code += """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_readDbg, %(name)s_writeDbg, %(name)s_debugTransaction, combinatorialCB);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name()}
    else :
      code += """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, %(name)s_debugAccess, combinatorialCB);
""" % { 'name': self.name(), 'instanceName': self.instanceName(),
        'className': self.className(), 'portFactory': self._portFactory.name()}
    code += self.setTransactorPortWidth()
    return code

  def componentCommunicate(self):
    # These transactors don't have communicates. They have send drives
    return ''

  def hasDriveNotify(self):
    return True

  def libDepends(self):
    return ['arm_adaptors_newaxi%s' % self.config().mxVer()]
  def libWinDepends(self):
    return ['libarm_adaptors_newaxi%s%s.lib' % (self.config().mxVer(), self.config().mxCompilerLibSuffix())]
  def libWinDebugDepends(self):
    return ['libarm_adaptors_newaxi%s%s.lib' % (self.config().mxVer(), self.config().mxCompilerLibSuffix())]
  
  # This transactor support the debug transaction
  def debugTransaction(self):
    code = """\
   %(compClass)s *owner = dynamic_cast<%(compClass)s *>(comp);
   return owner->debugTransaction(info);
""" % { 'compClass': self.compName() }
    return code
#    return '  return eslapi::CASI_STATUS_NOTSUPPORTED;\n'

  def generate_test_signals(self, prefix):
    code = '0'
    sep = ' || '
    for input in self._armInputs:
      if re.compile('^' + prefix).search(input.name()):
        member = self.valueChangeMember(input)
        code += sep + member + '->changed()'
        sep = '\n       || '
    return code

  def generate_clear_signals(self, prefix):
    code = ''
    for input in self._armInputs:
      if re.compile('^' + prefix).search(input.name()):
        member = self.valueChangeMember(input)
        code += '  ' + member + '->clearChanged();\n'
    return code
  
  def componentSendDrive(self):
    return self.backwardValid()

  def componentSendNotify(self):
    return self.backwardReady()

  def backwardValid(self):
    code =  '''\
  if (%(instanceName)s->rChanged) {
    %(instanceName)s->rChanged = false;
    %(instanceName)s->sendRDrive();

  } 
  else if ((%(rvalid)s->getValue0() != 0) &&
              %(rvalid)s->lastCycleChanged(crtCycle)) {
    %(instanceName)s->sendRDrive();
  }

  if (%(instanceName)s->bChanged) {
    %(instanceName)s->bChanged = false;
    %(instanceName)s->sendBDrive();

  } 
  else if ((%(bvalid)s->getValue0() != 0) &&
             %(bvalid)s->lastCycleChanged(crtCycle)) {
    %(instanceName)s->sendBDrive();
  }
''' % { 
        'instanceName': self.instanceName(),
        'rvalid':self.valueChangeMember(self.findPort('rvalid')),
        'bvalid':self.valueChangeMember(self.findPort('bvalid')),
        'generate_test_r_signals' : self.generate_test_signals('r'),
        'generate_clear_r_signals' : self.generate_clear_signals('r'),
        'generate_test_b_signals' : self.generate_test_signals('b'),
        'generate_clear_b_signals' : self.generate_clear_signals('b')
        }
    return code
  
  def backwardReady(self):
    code = '''\
  if (%(instanceName)s->awReadyChanged) {
    %(instanceName)s->awReadyChanged = false;
    %(instanceName)s->sendAWNotify();
  } 
  else if ((%(awready)s->getValue0() != 0) &&
	     %(awready)s->lastCycleChanged(crtCycle)) {
    %(instanceName)s->sendAWNotify();
  }

  if (%(instanceName)s->arReadyChanged) {
    %(instanceName)s->arReadyChanged = false;
    %(instanceName)s->sendARNotify();
  } 
  else if ((%(arready)s->getValue0() != 0) &&
	     %(arready)s->lastCycleChanged(crtCycle)) {
   %(instanceName)s->sendARNotify();
  }

  if (%(instanceName)s->wReadyChanged) {
    %(instanceName)s->wReadyChanged = false;
    %(instanceName)s->sendWNotify();
  } 
  else if ((%(wready)s->getValue0() != 0) &&
             %(wready)s->lastCycleChanged(crtCycle)) {
    %(instanceName)s->sendWNotify();
  }
''' % {
        'instanceName': self.instanceName(),
        'awready':self.valueChangeMember(self.findPort('awready')),
        'wready':self.valueChangeMember(self.findPort('wready')),
        'arready':self.valueChangeMember(self.findPort('arready'))
        }
    return code

class ArmAxiS2tBase(ArmTransactorAdaptor):
  def __init__(self, compName, name, config, portFactory, xtor, bufferedMasters):
    ArmTransactorAdaptor.__init__(self, compName, name, config, portFactory, xtor, bufferedMasters)
    config.addParameter(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", "bool",  'run-time', xtor.name(), self.instanceName()))

  def isInputPort(self, name):
    return name in ('awid', 'awaddr', 'awlen', 'awsize', 'awburst',
                    'awlock', 'awcache', 'awprot', 'awuser', 'awvalid',
                    'wid', 'wdata', 'wstrb', 'wlast', 'wuser', 'wvalid',
                    'bready', 'arid', 'araddr', 'arlen', 'arsize',
                    'arburst', 'arlock', 'arcache', 'arprot', 'aruser',
                    'arvalid', 'rready')

  def componentConstructor(self):
    code = ArmTransactorAdaptor.componentConstructor(self)
    code += self.setTransactorPortWidth()
    return code
  
  def componentInit(self):
    code = ArmTransactorAdaptor.componentInit(self)
    code += ArmTransactorAdaptor.generateValueChangeCallbacks(self)
    return code
  def componentReset(self):
    code = ArmTransactorAdaptor.componentReset(self)
    code += ArmTransactorAdaptor.generateValueChangeInits(self)
    return code
  
class ArmAxiS2t(ArmAxiS2tBase):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmAxiS2tBase.__init__(self, compName, name, config, portFactory, xtor, False)

  def className(self):
    return 'CarbonAxiS2T'

class AMBATLM:
  def __init__(self):
    self.__phases = ( "tlm::BEGIN_REQ", "tlm::END_REQ"
                    , "amba::BEGIN_DATA", "amba::BEGIN_LAST_DATA", "amba::END_DATA"
                    , "tlm::BEGIN_RESP", "amba::BEGIN_LAST_RESP", "tlm::END_RESP"
                    , "BUS_REQ", "BUS_REQ_STOP"
                    , "BUS_GRANT", "BUS_UNGRANT"
                    , "DATA_CONTINUE", "DATA_SPLIT", "DATA_RETRY"
                    , "P_SELECT", "RESET_ASSERT", "RESET_DEASSERT"
                    )
    self.__axipins = ( "arvalid", "arready"
                     , "arready", "awready"
                     , "wvalid", "wready"
                     , "rvalid", "rready"
                     , "bvalid", "bready"
                     )
    self.__axiphases = ( "tlm::BEGIN_REQ", "tlm::END_REQ"
                       , "amba::BEGIN_DATA", "amba::BEGIN_LAST_DATA", "amba::END_DATA"
                       , "tlm::BEGIN_RESP", "amba::BEGIN_LAST_RESP", "tlm::END_RESP"
                       )
  def phases(self):
    return self.__phases
  def getAXIPins(self):
    return self.__axipins
  def getAXIPhases(self):
    return self.__axipphases
  def getPhasePins(self, phase, prot):
    if prot == "AXI":
      if phase == "tlm::BEGIN_REQ":
        return ("arvalid", "awvalid") 
      elif phase == "tlm::END_REQ":
        return ("arready", "awready") 
      elif phase == "BEGIN_DATA":
        return ("wvalid")
      elif phase == "BEGIN_LAST_DATA":
        return ("wvalid")
      elif phase == "END_DATA":
        return ("wready")
      elif phase == "tlm::BEGIN_RESP":
        return ("bvalid", "rvalid")
      elif phase == "BEGIN_LAST_RESP":
        return ("bvalid", "rvalid")
      elif phase == "tlm::END_RESP":
        return ("bready", "rready")
      else:
        return ("*")
  def getPhase(self, pin):
    if pin == "arvalid" or pin == "awvalid":
      return ["tlm::BEGIN_REQ"]
    elif pin == "arready" or pin == "awready":    
      return ["tlm::END_REQ"]
    elif pin == "wvalid":
      return ["amba::BEGIN_DATA", "amba::BEGIN_LAST_DATA"]
    elif pin == "wready":
      return ["amba::END_DATA"]
    elif pin == "bvalid":
      return ["tlm::BEGIN_RESP", "amba::BEGIN_LAST_RESP"]
    elif pin == "bready":
      return ["tlm::END_RESP"]
    elif pin == "rvalid":
      return ["tlm::BEGIN_RESP", "amba::BEGIN_LAST_RESP"]
    elif pin == "rready":
      return ["tlm::END_RESP"]
    else:
      return ["*"]


# Arm AXI Transactor that supports Flow thru
class ArmAxiFS2t(ArmAxiS2tBase):
  def __init__(self, compName, name, config, portFactory, xtor):
    ArmAxiS2tBase.__init__(self, compName, name, config, portFactory, xtor, True)
  def className(self):
    return 'CarbonAxiFS2T'
  # gen the arg list for a constructor
  def generateValueChangeInstanceCtorArgList(self, input, flagname):
    isvalidflag = "false"
    # some axi ports are valid
    if input.name() == "arready":
      isvalidflag = "true"
    elif input.name() == "awready":
      isvalidflag = "true"
    elif input.name() == "wready":
      isvalidflag = "true"
    elif input.name() == "bready":
      isvalidflag = "true"
    elif input.name() == "rready":
      isvalidflag = "true"
    elif input.name() == "arvalid":
      isvalidflag = "true"
    elif input.name() == "awvalid":
      isvalidflag = "true"
    elif input.name() == "wvalid":
      isvalidflag = "true"
    elif input.name() == "rvalid":
      isvalidflag = "true"
    elif input.name() == "bvalid":
      isvalidflag = "true"

    # with eventQueues the event queue variables are the first args to ctor, and a valid flag is trailing
    code = ""
    if self.useXactorEventQueues():
      clockMaster = self._xtor.clockMaster()
      if ( clockMaster != None ):
        clockId = clockMaster.name()
      else:
        clockId = "Default"

      code += "this->currEventQueue%(clkId)s, this->nextEventQueue%(clkId)s, " % {'clkId': clockId }
    code += """\
%(instanceName)s->carbonGetPort("%(inputName)s"),%(instanceName)s->%(flagname)s\
""" % {'instanceName': self.instanceName(),
     'inputName': input.name(),
     'flagname': flagname}
    if self.useXactorEventQueues():
      code += "Func, %(isvalidflag)s" % {'isvalidflag': isvalidflag}
    return code


  # a change here requires a change in generateValueChangeInstanceCtorArgList
  def generateValueChangeCtor(self,valueChangeClass):
    if self.useXactorEventQueues():
      # classname(set<CarbonXtorEvent*>& eq, set<CarbonXtorEvent*>& vq, void* data, CarbonXtorEvent* ev, bool valid=false): mLastCycleExamined(0), mEvent(ev), mValidSignal(valid), currEventQueue(eq), nextEventQueue(vq)
      code = """\
%(valueChangeClass)s(set<CarbonXtorEvent*>& eq, set<CarbonXtorEvent*>& vq, void* data, CarbonXtorEvent* ev, bool valid=false)
  : mLastCycleExamined(0), mEvent(ev), mValidSignal(valid),currEventQueue(eq), nextEventQueue(vq)
""" % {'valueChangeClass': valueChangeClass }
    else:
      # use the definition from base class      
      code = ArmAxiS2tBase.generateValueChangeCtor(self, valueChangeClass)
      
    return code

  def generateChangeTracking(self):
    if self.useXactorEventQueues():
      code = """\
    currEventQueue.insert(mEvent);
    if (mValidSignal)
    {
        if (mVal[0])
          nextEventQueue.insert(mEvent); // signal is a response signal and is asserted, remember this event is also needed next time
        else
          nextEventQueue.erase(mEvent); // signal is a response signal but not asserted, so nothing is needed next time
    }"""
    else:
      # use the definition from base class
      code = ArmAxiS2tBase.generateChangeTracking(self)
    return code

  def generateChangeTrackingVariableDecl(self):
    if self.useXactorEventQueues():
      code = """\
  set<CarbonXtorEvent*> &currEventQueue;
  set<CarbonXtorEvent*> &nextEventQueue;
  CarbonXtorEvent* mEvent;
  
  bool mValidSignal; // true if this is a response signal, like BVALID, or BREADY
"""
    else:
      # use the definition from base class
      code = ArmAxiS2tBase.generateChangeTrackingVariableDecl(self)
    return code

  def generateValueChangeFunctionDcl(self):
    if self.useXactorEventQueues():
      code = "// no value change functions defined"
    else:
      # use the definition from base class
      code = ArmAxiS2tBase.generateValueChangeFunctionDcl(self)
    return code

  def componentConstructor(self):
    code =  """
  %(instanceName)s = new %(className)s(this, "%(name)s", &mCarbonObj, %(portFactory)s, combinatorialCB);
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className(), 'portFactory': self._portFactory.name() }
    code += self.setTransactorPortWidth()
    return code
  
  def componentCommunicate(self):
    # These transactors don't have communicates. They have send drives
    return ''

  def hasDriveNotify(self):
    return True

  def libDepends(self):
    return ['arm_adaptors_newaxi%s' % self.config().mxVer()]
  def libWinDepends(self):
    return ['libarm_adaptors_newaxi%s%s.lib' % (self.config().mxVer(), self.config().mxCompilerLibSuffix())]
  def libWinDebugDepends(self):
    return ['libarm_adaptors_newaxi%s%s.lib' % (self.config().mxVer(), self.config().mxCompilerLibSuffix())]

  def componentDriveAllSignals(self):
    return '  %(instanceName)s_driveAllSignals();' % { 'instanceName': self.instanceName() }

  def generate_test_signals(self, prefix):
    code = ''
    sep = ''
    for input in self._armInputs:
      if re.compile('^' + prefix).search(input.name()):
        member = self.valueChangeMember(input)
        code += sep + member + '->changed()\n'
        sep = '       || '
    return code

  def generate_clear_signals(self, prefix):
    code = ''
    for input in self._armInputs:
      if re.compile('^' + prefix).search(input.name()):
        member = self.valueChangeMember(input)
        code += '  ' + member + '->clearChanged();\n'
    return code
  
  def componentSendDrive(self):
    return self.forwardValid()

  def componentSendNotify(self):
    return self.forwardReady()

  def forwardValid(self):
    code =  '''\
  if (%(instanceName)s->awChanged) {
    %(instanceName)s->awChanged = false;
    %(instanceName)s->sendAWDrive();

  } 
  else if ((%(awvalid)s->getValue0() != 0) && 
             %(awvalid)s->lastCycleChanged(crtCycle)) {
    %(instanceName)s->sendAWDrive();
  }

  if (%(instanceName)s->arChanged) {
    %(instanceName)s->arChanged = false;
    %(instanceName)s->sendARDrive();

  } 
  else if ((%(arvalid)s->getValue0() != 0) &&
             %(arvalid)s->lastCycleChanged(crtCycle)) {
    %(instanceName)s->sendARDrive();
  }

  if (%(instanceName)s->wChanged) {
    %(instanceName)s->wChanged = false;
    %(instanceName)s->sendWDrive();

  } 
  else if ((%(wvalid)s->getValue0() != 0) && 
             %(wvalid)s->lastCycleChanged(crtCycle)) {
    %(instanceName)s->sendWDrive();
  }
''' % { 
        'instanceName': self.instanceName(),
        'awvalid':self.valueChangeMember(self.findPort('awvalid')), 
        'arvalid':self.valueChangeMember(self.findPort('arvalid')),
        'wvalid' :self.valueChangeMember(self.findPort('wvalid' )),
        'generate_test_aw_signals': self.generate_test_signals('aw'),
        'generate_clear_aw_signals': self.generate_clear_signals('aw'),
        'generate_test_ar_signals': self.generate_test_signals('ar'),
        'generate_clear_ar_signals': self.generate_clear_signals('ar'),
        'generate_test_w_signals': self.generate_test_signals('w'),
        'generate_clear_w_signals': self.generate_clear_signals('w'),
        }
    return code

  def forwardReady(self):
    code = '''\
  if (%(instanceName)s->rReadyChanged) {
    %(instanceName)s->rReadyChanged = false;
    %(instanceName)s->sendRNotify();
  } 
  else if ((%(rready)s->getValue0() != 0) &&
             %(rready)s->lastCycleChanged(crtCycle)) {
    %(instanceName)s->sendRNotify();
  }

  if (%(instanceName)s->bReadyChanged) {
    %(instanceName)s->bReadyChanged = false;
    %(instanceName)s->sendBNotify();
  } 
  else if ((%(bready)s->getValue0() != 0) &&
             %(bready)s->lastCycleChanged(crtCycle)) {
    %(instanceName)s->sendBNotify();
  }
''' % {
        'instanceName': self.instanceName(),
        'rready':self.valueChangeMember(self.findPort('rready')),
        'bready':self.valueChangeMember(self.findPort('bready'))
        }
    return code

class XtorAdaptorPortFactory:
  def __init__(self, xtorType, adaptor, compClass, includes, upperCasePorts, hasSystemCPorts):
    self.__xtorType = xtorType       # a unique name (for every call to XtorAdaptorPortFactory) used to create the name() and className()
    self.__adaptor = adaptor         # name of adaptor (e.g. 'AHBT2S')
    self.__compClass = compClass
    self.__includes = includes
    self.__upperCasePorts = upperCasePorts
    self.__hasSystemCPorts = hasSystemCPorts
    self.__xtors = {}
  def adaptor(self):
    return self.__adaptor
  def includes(self):
    code = ''
    for include in self.__includes:
      if (include[0] == "<"):
        quote = ''
      else:
        quote = '"'
      code += '#include %(quote)s%(include)s%(quote)s\n' % {'include': include, 'quote': quote}
    return code
  def name(self):
    return 'carbon' + self.__xtorType + self.__adaptor + 'PortFactory'
  def className(self):
    return 'Carbon' + self.__xtorType +  self.__adaptor + 'Port'
  def addPort(self, xtor, port, rtlSignal, width):
    self.__xtors[xtor] = self.__xtors.get(xtor, [])
    self.__xtors[xtor].append((port, rtlSignal, width))
  def forwardDeclaration(self):
    namespaceOpen = "";
    namespaceClose = "";
    if self.__hasSystemCPorts:
      namespaceOpen = """\
namespace %(compClass)s_namespace {
""" % { 'compClass' : self.__compClass }
      namespaceClose = """\
};
"""
    return """\
%(namespaceOpen)sclass %(className)s;%(namespaceClose)s
""" % { 'namespaceOpen' : namespaceOpen, 'className' : self.className(), 'namespaceClose' : namespaceClose }
  def code(self, userCode):
    xtorNames = self.__xtors.keys()
    xtorNames.sort()
    namespaceOpen = "";
    namespacePrefix = "";
    namespaceClose = "";
    if self.__hasSystemCPorts:
      namespaceOpen = """\
      namespace %(compClass)s_namespace {
      """ % { 'compClass' : self.__compClass }
      namespacePrefix = """%(compClass)s_namespace::""" % { 'compClass' : self.__compClass }
      namespaceClose = """\
      };
      """
    code = """
%(namespaceOpen)s// This class is used by a %(name)s instance to write values to
// Carbon Model input signals.
class %(className)s : public CarbonXtorAdaptorToVhmPort
{
public:
  %(className)s(eslapi::CASIModuleIF *owner, const char *port, CarbonObjectID **carbonObject, CarbonNetID **net, CarbonUInt32 numWords):    
    mpCarbonObject(carbonObject), mpNetId(net), mNumWords(numWords)
  {
      mValue = new CarbonUInt32[mNumWords];
  }

  virtual ~%(className)s()
  {
    delete [] mValue;
  }

  CarbonObjectID **mpCarbonObject;
  CarbonNetID **mpNetId;
  CarbonUInt32 *mValue;  
  CarbonUInt32 mNumWords;
};

class %(className)s_unconnected : public %(className)s
{
public:
  %(className)s_unconnected(eslapi::CASIModuleIF *owner, const char *port, CarbonObjectID **carbonObject, CarbonNetID **net, CarbonUInt32 numWords):
    %(className)s(owner, port, carbonObject, net, numWords) {}
  virtual void driveSignal(uint32_t value, uint32_t* extValue) {}
};

%(generatePortCode)s

CarbonXtorAdaptorToVhmPort *%(name)s(eslapi::CASIModuleIF *owner, const char *xtor, const char *port, CarbonObjectID **carbonObject)
{
  %(compClass)s *comp = dynamic_cast<%(compClass)s *>(owner);
  
""" % { 'namespaceOpen' : namespaceOpen, 'name': self.name(), 'className': self.className(),
        'compClass': self.__compClass ,
        'generatePortCode' : self.generatePortCode(xtorNames, userCode) }
    for xtor in xtorNames:
      for port, rtlSignal, width in self.__xtors[xtor]:
        numWords = (width + 31)/32
        origArmPortName = port.name()
        armPortName = origArmPortName
        if self.__upperCasePorts:
          armPortName = armPortName.upper()
        code += """\
  if (strcmp(xtor, "%(name)s") == 0 && strcmp(port, "%(armPortName)s") == 0) {
    return new %(namespacePrefix)sCarbon_%(name)s_%(origArmPortName)s(owner, port, carbonObject, &(comp->%(port)s), %(numWords)s);
  }
""" % { 'name': xtor,
        'namespacePrefix' : namespacePrefix,
        'port': port.carbonNetId(),
        'rtlSignal': rtlSignal,
        'className': self.className(),
        'numWords': numWords,
        'armPortName': armPortName,
        'origArmPortName': origArmPortName }
    code += """\
  return new %(className)s_unconnected(owner, port, carbonObject, NULL, 0);
}%(namespaceClose)s
""" % { 'adaptor': self.adaptor(), 'className': self.className(), 'namespaceClose' : namespaceClose }
    return code

  def generatePortCode(self, xtorNames, userCode):
    namespaceOpen = "";
    namespacePrefix = "";
    namespaceClose = "";
    if self.__hasSystemCPorts:
      namespaceOpen = """\
namespace %(compClass)s_namespace {
      """ % { 'compClass' : self.__compClass }
      namespacePrefix = """%(compClass)s_namespace::""" % { 'compClass' : self.__compClass }
      namespaceClose = """\
      };  
      """
    code = ""
    for xtor in xtorNames:
      for port, rtlSignal, width in self.__xtors[xtor]:
        numWords = (width + 31)/32
        armPortName = port.name()
        code += """
%(namespaceOpen)s// Class to drive Carbon Model
class Carbon_%(instName)s_%(armPortName)s : public %(className)s
{
%(userCodePreDef)s
public:
  Carbon_%(instName)s_%(armPortName)s(eslapi::CASIModuleIF *owner, const char *port, CarbonObjectID **carbonObject, CarbonNetID **net, CarbonUInt32 numWords):
    %(className)s(owner, port, carbonObject, net, numWords) {}
  virtual void driveSignal(uint32_t value, uint32_t* extValue)
  {
%(userCodePreTransactorOutput)s
%(generateCarbonDeposit)s
%(userCodePostTransactorOutput)s
  }
%(userCodePostDef)s
};  %(namespaceClose)s
""" % { 'instName': xtor,
        'namespaceOpen' : namespaceOpen,
        'namespaceClose' : namespaceClose,
        'compClass' : self.__compClass,
        'armPortName': armPortName,
        'className': self.className(),
        'userCodePreDef': userCode.preSection(xtor + '_' + armPortName + ' CLASS DEFINITION'),
        'userCodePostDef': userCode.postSection(xtor + '_' + armPortName + ' CLASS DEFINITION'),
        'userCodePreTransactorOutput': userCode.preSection(xtor + '_' + armPortName + '::driveSignal'),
        'userCodePostTransactorOutput': userCode.postSection(xtor + '_' + armPortName + '::driveSignal'),
        'generateCarbonDeposit' : self.generateCarbonDeposit(numWords, port) }
    return code

  def generateCarbonDeposit(self, numWords, port):
    code = ""
    if numWords > 1:
      # here we make the somewhat dangerous assumption that somewhere (in SoCD) a check has been done 
      # that the width of all data connections match. If this code is for a data connection then there
      # is no need here to check that the array of words at extValue is large enough to hold all data.
      # Since the data widths were checked then it must be of sufficient size.
      # If this code is for an address connection (like WADDR), the master might be narrower than the
      # slave (remember this code is for the slave).  Width mis-matches for the address field are valid 
      # and supported by the protocol. However we assume here that the maximum address width is 64 bits,
      # and since numWords > 1 the width must therefore be in the range 33-64.  So the check here for 
      # a non-null extValue is sufficient to know that the master has an address width in the range 33-64 bits. 
      code += "    mValue[0] = value;\n"
      code += "    if ( extValue ){\n"
      code += "      memcpy(mValue + 1, extValue, (%d - 1) * sizeof(CarbonUInt32));\n" % numWords
      code += "    }\n"
      if (port.expr() != ""):
        code += "  " + gGeneratePortExpr(port, "mValue[0]", "mValue") + "\n"
      code += "    carbonDepositFast(*mpCarbonObject, *mpNetId, mValue, 0);"
    else:
      if (port.expr() != ""):
        code += "  " + gGeneratePortExpr(port, "value", "val") + "\n"
      code += "    carbonDepositFast(*mpCarbonObject, *mpNetId, &value, 0);"
    return code
