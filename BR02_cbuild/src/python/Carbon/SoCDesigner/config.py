#******************************************************************************
# Copyright (c) 2003-2013 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

## NOTE if you want to insert a breakpoint into this file just put the following two stmts at the point you want to break:
##     import pdb; pdb.set_trace()

##
# \file
# Classes that represent SoC Designer configuration data.
# This is a layer on top of Carbon.Cfg.Config that organizes the configuration
# into a form that is more useful for generating the code.

import os
import re

import Carbon.Cfg
import Carbon.DB
from Carbon import cpp
import profile
import parameters
import transactors
import error
import subcomponent
import cadi

def slashify(code):
  out_line = ""
  for c in code:
    if c == '\\':
      out_line += c
      out_line += c
    else:
      out_line += c
  return out_line.replace("'","")

def getMaxsimRevFile() :
    mx_home = os.getenv('MAXSIM_HOME')

    revfile = ''
    if (mx_home != None) :
      # Remove quotes from around the path name because they make it fail on windows
      if mx_home[0] == '"' and mx_home[-1] == '"':
        mx_home = mx_home[1:-1]

      # Filename of Revision file. For Linux/Unix the include directory is lowercase,
      # so try that first
      revfile = '%s/include/mxsim_revision.h' % mx_home
    
      if (not os.path.exists(revfile)) :
        # For Windows the include directory starts with a capital letter, so try that next
        revfile = '%s/Include/mxsim_revision.h' % mx_home
        
        if (not os.path.exists(revfile)) :
          # If this didn't exist either give up, MAXSIM_HOME does not point to a SoCDesigner install directory
          revfile = ''

    return revfile

def findMaxsimVersion(output) :
  """ This function returns the current SoCDesigner Major Version. """
  
  revfile = getMaxsimRevFile()
  if (revfile != '') :
    for line in file(revfile) :
      m = re.search(r"MAXSIM_VERSION\s+(\d+)\.(\d+)\.(\d+)",line)
      if (m != None) :
        # Found Match, return full Revision Number
        return (m.group(1), m.group(2), m.group(3))
      
  # If we couldn't determine MaxSim Version, assume 7.x
  mx_home = os.getenv('MAXSIM_HOME')
  output.warning("%d: The MAXSIM_HOME environment variable (%s) is not set to an SoC Designer installation directory. Assuming SoC Designer version 7.x." % (error.carbonCfgERR_NOMXHOME, mx_home))
  return (7, 2, 14)
    
def findMaxsimCompiler(output) :
  """ This function returns the compiler to use when compiling SoCDesigner Components. """
  
  revfile = getMaxsimRevFile()
  if (revfile != '') :
    for line in file(revfile) :
      m = re.search(r"SOCDESIGNER_COMPILER\s+(\d+)",line)
      if (m != None) :
        # Found Match, return Compiler
        # MSVC years are converted to release numbers
        if(m.group(1) == '2005') :
          return 'vc8'
        if(m.group(1) == '2008') :
          return 'vc9'
        if(m.group(1) == '2010') :
          return 'vc10'
        if(m.group(1) == '2012') :
          return 'vc11'
        # Others (i.e. Linux) are returned as-is
        return m.group(1)
        
  # If we couldn't determine Maxsim Compiler, it is probably an older version, so just use 343
  return '343'
      
class MaxConfig(Carbon.Cfg.Config):
  def __init__(self, ccfgName, xtorDefFiles, output, modelDir, pgo):
    self.__ccfgName = ccfgName
    self.__output = output
    self.__modelDir = modelDir
    self.__pgo = pgo

    Carbon.Cfg.Config.__init__(self, ccfgName, xtorDefFiles, Carbon.Cfg.eCarbonXtorsMaxsim)

    # Maxsim Version and Compiler Version
    (self.__mxVer, self.__mxMinorVer, self.__mxRevision) = findMaxsimVersion(output)
    self.__mxCompiler = findMaxsimCompiler(output)
    
    # Carbon IODB
    self.__db = Carbon.DB.DBContext(self.ioDbFile())
    self.__dbNodes = {}
    if self.__db :
      # Create a dictionary of DBNodes for easy lookup
      self.__dbNodes = dict([(x.fullName(), x) for x in self.__db.iterPrimaryPorts()] +
                            [(x.fullName(), x) for x in self.__db.iterScDepositable()] +
                            [(x.fullName(), x) for x in self.__db.iterScObservable()])
    else :
      self.__output.warning("%d: IODB Failed to open. Component will be built without information from the Carbon Model's IODB." % error.carbonCfgERR_NODB)

    # Scope object for Top level component
    self.__scope = cpp.Scope()
    
    # include files
    self.__includes = set([])
    
    # source files
    self.__sources = set([])
    
    # forward delarations
    self.__forwardDecls = set([])

    # link flags
    self.__linkFlags = ''
    self.__winLinkFlags = ''

    # cxx flags
    self.__cxxFlags = ''
    
    # profile context
    self.__profile = profile.Profile(self)

    # additional clock generators implied by clock slave ports
    self.__clocks = []

    # additional reset generators implied by reset slave ports
    self.__resets = []

    # True if there are any flow though paths in the component
    self.__hasFlowThru = False
    
    # transactor adaptor data (python adaptor class, adaptor name, include files, upperCasePortNames)
    xactorData = {
      'AHB_Slave_T2S':  (transactors.ArmAhbT2s, 'AHBT2S', ['xactors/arm/include/CarbonAhbSlaveT2S.h'], True),
      'AHB_Master_S2T': (transactors.ArmAhbS2t, 'AHBS2T', ['xactors/arm/include/CarbonAhbMasterS2T.h'], True),
      'AHB_Lite_Slave_T2S':  (transactors.ArmAhbT2s, 'AHBT2SLITE', ['xactors/arm/include/CarbonAhbSlaveT2S.h'], True),
      'AHB_Lite_Master_S2T': (transactors.ArmAhbS2t, 'AHBS2TLITE', ['xactors/arm/include/CarbonAhbMasterS2T.h'], True),
      'AHB_Master_T2S': (transactors.ArmAhbMT2s, 'AHBM_T2S', ['xactors/arm/include/CarbonAhbMasterT2S.h'], True),
      'AHB_Slave_S2T': (transactors.ArmAhbSS2t, 'AHBS_S2T', ['xactors/arm/include/CarbonAhbSlaveS2T.h'], True),
      'AXI_Slave':  (transactors.ArmAxiT2s, 'AXIT2S', ['xactors/arm/include/CarbonAxiT2S.h'], False),
      'AXI_Master': (transactors.ArmAxiS2t, 'AXIS2T', ['xactors/arm/include/CarbonAxiS2T.h'], False),
      'AXI_Flowthru_Slave':  (transactors.ArmAxiFT2s, 'AXIFT2S', ['<set>','xactors/arm/include/CarbonXtorEvent.h','xactors/arm/include/CarbonAxiFT2S.h'], False),
      'AXI_Flowthru_Master': (transactors.ArmAxiFS2t, 'AXIFS2T', ['<set>','xactors/arm/include/CarbonXtorEvent.h','xactors/arm/include/CarbonAxiFS2T.h'], False),
      'APB_Slave':  (transactors.ArmApbT2s, 'APBT2S', ['xactors/arm/include/CarbonApbT2S.h'], True),
      'APB3_Slave':  (transactors.ArmApbT2s, 'APBT2S', ['xactors/arm/include/CarbonApbT2S.h'], True),
      'APB_Master':  (transactors.ArmApbS2t, 'APBS2T', ['xactors/arm/include/CarbonApbS2T.h'], True),
      'APB3_Master':  (transactors.ArmApbS2t, 'APBS2T', ['xactors/arm/include/CarbonApbS2T.h'], True),
      'ARM926_DTCM_Master':  (transactors.ARM926DTCMMasterXtor, 'TCMMasterXtor', ['xactors/arm/include/CarbonTCMS2T.h'], True),
      'ARM926_DTCM_Slave':  (transactors.ARM926DTCMSlaveXtor, 'TCMSlaveXtor', ['xactors/arm/include/CarbonTCMT2S.h'], True),
      'Debug_Slave':  (transactors.ArmDebugS, 'DEBUGs', ['xactors/arm/include/CarbonDebugS.h'], True),
      'Debug_Master':  (transactors.ArmDebugM, 'DEBUGm', ['xactors/arm/include/CarbonDebugM.h'], True),
      'CarbonX_Pcie_1Lane_10Bit':   (transactors.CarbonXPcie, 'CarbonX_Pcie_1Lane_10Bit',  ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_2Lane_10Bit':   (transactors.CarbonXPcie, 'CarbonX_Pcie_2Lane_10Bit',  ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_4Lane_10Bit':   (transactors.CarbonXPcie, 'CarbonX_Pcie_4Lane_10Bit',  ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_8Lane_10Bit':   (transactors.CarbonXPcie, 'CarbonX_Pcie_8Lane_10Bit',  ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_12Lane_10Bit':  (transactors.CarbonXPcie, 'CarbonX_Pcie_12Lane_10Bit', ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_16Lane_10Bit':  (transactors.CarbonXPcie, 'CarbonX_Pcie_16Lane_10Bit', ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_32Lane_10Bit':  (transactors.CarbonXPcie, 'CarbonX_Pcie_32Lane_10Bit', ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_1Lane_Pipe':   (transactors.CarbonXPcie, 'CarbonX_Pcie_1Lane_Pipe',  ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_2Lane_Pipe':   (transactors.CarbonXPcie, 'CarbonX_Pcie_2Lane_Pipe',  ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_4Lane_Pipe':   (transactors.CarbonXPcie, 'CarbonX_Pcie_4Lane_Pipe',  ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_8Lane_Pipe':   (transactors.CarbonXPcie, 'CarbonX_Pcie_8Lane_Pipe',  ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_12Lane_Pipe':   (transactors.CarbonXPcie, 'CarbonX_Pcie_12Lane_Pipe',  ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_16Lane_Pipe':   (transactors.CarbonXPcie, 'CarbonX_Pcie_16Lane_Pipe',  ['CarbonXPcie.h'], False),
      'CarbonX_Pcie_32Lane_Pipe':   (transactors.CarbonXPcie, 'CarbonX_Pcie_32Lane_Pipe',  ['CarbonXPcie.h'], False)
      }

    # Loop over SoCD-provided and user-defined transactors and add
    # them to the table so that they get portFactories.  A transactor
    # belongs to one of these types if it has a class name and an
    # include file defined for it.
    for xtor in self.transactors():
      xtorType = xtor.type()
      # In some cases, we want to skip entries we find in xactors.xml.
      # For example, all the transactors in the table above also have
      # entries in the xactors.xml from CMS.  We don't want to
      # override the custom generator classes from the table in that
      # case.
      #
      # However, there are some xactors.xml entries that should take
      # precedence.  For example, the new shared library AXIv2
      # transactors provided in SoCD (if available) should be used in
      # place of the CMS static library ones.  Only the SoCD-provided
      # transactors will have sdXactor() return true, so don't skip
      # those.
      if (not xactorData.has_key(xtorType) or xtor.sdXactor()) and xtor.className() != '' and len(xtor.includeFiles()) > 0:
        if xtor.sdXactor():
          # This is an official SoCDesigner protocol xactor.  See if
          # we should preserve case names on logical transactor ports.
          # For some reason, that's the default for SoCD-provided
          # transactors.  However, the AXIv2 transactors were
          # originally part of CMS and should have their original case
          # preserved.
          upperCase = not xtor.preservePortNameCase()
          xactorData[xtorType] = (transactors.SDTransactor, xtorType, xtor.includeFiles(), upperCase)
        else: 
          xactorData[xtorType] = (transactors.UserComponent, xtorType, xtor.includeFiles(), False)

    # define parameters
    waveName = self.waveFilename()
    waveTypeName = self.waveTypeName()
    if waveName == '':
      waveName = 'carbon_' + self.compName() + '.' + waveTypeName.lower()
      
    # The DB path is no longer needed (since the DB in embedded in the
    # model), but we keep the parameter for compatibility with old
    # .mxp files that might try to set it.  If for some reason the
    # model is built with -noEmbedDB, the value can still be changed
    # in the canvas to find the DB file.
    self.__carbonDbPathParameter = parameters.Parameter('Carbon DB Path', '', "string", 'init-time')

    self.__waveFileParameter = parameters.Parameter('Waveform File', waveName, "string", 'init-time')
    self.__waveFormatParameter = parameters.Parameter('Waveform Format', waveTypeName, "string",'init-time')
    self.__waveFormatParameter.addEnumValue('VCD')
    self.__waveFormatParameter.addEnumValue('FSDB')
    self.__waveTimescaleParameter = parameters.Parameter('Waveform Timescale', '1 ns', "string", 'init-time') # 1ns is the default
    self.__waveTimescaleParameter.addEnumValue('1 fs')
    self.__waveTimescaleParameter.addEnumValue('10 fs')
    self.__waveTimescaleParameter.addEnumValue('100 fs')
    self.__waveTimescaleParameter.addEnumValue('1 ps')
    self.__waveTimescaleParameter.addEnumValue('10 ps')
    self.__waveTimescaleParameter.addEnumValue('100 ps')
    self.__waveTimescaleParameter.addEnumValue('1 ns')
    self.__waveTimescaleParameter.addEnumValue('10 ns')
    self.__waveTimescaleParameter.addEnumValue('100 ns')
    self.__waveTimescaleParameter.addEnumValue('1 us')
    self.__waveTimescaleParameter.addEnumValue('10 us')
    self.__waveTimescaleParameter.addEnumValue('100 us')
    self.__waveTimescaleParameter.addEnumValue('1 ms')
    self.__waveTimescaleParameter.addEnumValue('10 ms')
    self.__waveTimescaleParameter.addEnumValue('100 ms')
    self.__waveTimescaleParameter.addEnumValue('1 s')
    self.__waveTimescaleParameter.addEnumValue('10 s')
    self.__waveTimescaleParameter.addEnumValue('100 s')
    self.__dumpWavesParameter = parameters.WaveformDumpParameter('Dump Waveforms', 'false', "bool", 'run-time')
    self.__alignWavesParameter = parameters.Parameter('Align Waveforms', 'true', "bool", 'init-time')
    self.__parameters = [
      self.__carbonDbPathParameter,
      self.__dumpWavesParameter,
      self.__waveFileParameter,
      self.__waveFormatParameter,
      self.__waveTimescaleParameter,
      self.__alignWavesParameter,
      parameters.Parameter('Enable Debug Messages', "false", "bool", 'run-time'),
      ]

    # Add parameters from ccfg (this knows about enum and non-enum parameters)
    for param in Carbon.Cfg.Config.parameters(self):
      # Create the basic parameter
      tempParam = parameters.Parameter(param.name(), param.value(), param.type(), param.scope())

      # optional enum choices
      for enumChoice in param.enumChoices():
        tempParam.addEnumValue(enumChoice)
          
      # Add the paramter to the component
      self.__parameters.append(tempParam)

    # create lists of SoC Designer component ports based on the configuration
    self.__mxPorts = []
    self.__mxInputPorts = []
    self.__mxOutputPorts = []
    self.__mxClockPorts = []
    self.__xtorPortFactories = {}
    for xtor in self.transactors():
      # create a 'port factory' object the generates the #includes and
      # classes used to hook up the ports.
      xtorType = xtor.type()
      if xactorData.has_key(xtorType) and not self.__xtorPortFactories.has_key(xtorType):
        if xtorType == 'AHB_Slave_T2S' and (xtor.numInputs() == 0 and xtor.numOutputs() == 0):
          pass # AHB_Slave with no inputs or outputs means generate an empty debug interface
        else:
          pyClass, name, includes, upperCasePorts = xactorData[xtorType]
          self.__xtorPortFactories[xtorType] = transactors.XtorAdaptorPortFactory(xtorType, name, self.compName(), includes, upperCasePorts, self.hasSystemCPorts())

      # create an instance of the transactor 
      if xactorData.has_key(xtorType):
        if xtorType == 'AHB_Slave_T2S' and (xtor.numInputs() == 0 and xtor.numOutputs() == 0):
          # no ports mapped - generate the default transaction slave interface
          self.__mxInputPorts.append(transactors.MaxSimTransactionSlave(self.compName(), xtor.name(), self))
        else:
          # create an instance of the transaction adaptor port
          pyClass, name, includes, upperCasePorts = xactorData[xtorType]
          portFactory = self.__xtorPortFactories[xtorType]
          xtorPort = pyClass(self.compName(), xtor.name(), self, portFactory, xtor)
          self.__mxInputPorts.append(xtorPort)
          for port in xtor.outputs():
            # Ask the xtor if it wants to override the port width
            portWidth = xtorPort.getPortWidth(port)
            portFactory.addPort(xtor.name(), port, port.carbonName(), portWidth)
      elif xtorType in ('CarbonX_EnetMII_Slave', 'CarbonX_EnetGMII_Slave', 'CarbonX_EnetXGMII_Slave',
                        'CarbonX_PCI_Initiator_T2S', 'CarbonX_Wishbone_T2S'):
        transPort = transactors.CarbonXT2T(self.compName(), xtor.name(), self, xtor, 'eCarbonXSlave')
        self.__mxInputPorts.append(transPort)
        self.__parameters.append(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", 'bool', 'run-time', xtor.name(), transPort.instanceName()))
      elif xtorType in ('CarbonX_EnetMII_Master', 'CarbonX_EnetGMII_Master', 'CarbonX_EnetXGMII_Master'):
        transPort = transactors.CarbonXT2T(self.compName(), xtor.name(), self, xtor, 'eCarbonXCMaster')
        self.__mxInputPorts.append(transPort)
        self.__parameters.append(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", 'bool', 'run-time', xtor.name(), transPort.instanceName()))
      elif xtorType in ('CarbonX_PCI_Target_S2T'):
        transPort = transactors.CarbonXT2T(self.compName(), xtor.name(), self, xtor, 'eCarbonXMaster')
        self.__mxInputPorts.append(transPort)
        self.__parameters.append(parameters.ArmAdaptorParameter("Enable Debug Messages", "false", 'bool', 'run-time', xtor.name(), transPort.instanceName()))
      elif xtorType == 'Interrupt_Master':
        # create master port for the outbound interrupt
        master = transactors.MaxSimInterruptMaster(self, xtor)
        self.__mxOutputPorts.append(master)

        # create objects to listen to the RTL signals that drive
        # the interrupt port
        for port in xtor.inputs():
          self.__mxOutputPorts.append(transactors.InterruptMasterInput(self, port, master))
      elif xtorType == 'Interrupt_Slave':
        self.__mxInputPorts.append(transactors.MaxSimInterruptSlave(self, xtor))
      elif xtorType == 'Clock_Input':
        self.__mxClockPorts.append(transactors.MaxSimClockSlave(self, xtor))
      elif xtorType == 'Clock_Output':
        self.__mxOutputPorts.append(transactors.MaxSimClockMaster(self, xtor))
      elif xtorType == 'Clock_Generator':
        self.__mxClockPorts.append(transactors.ClockGenXtor(self, xtor))
      elif xtorType == 'Reset_Input':
        reset_obj = transactors.MaxSimResetSlave(self, xtor)
        self.__mxClockPorts.append(reset_obj)
      else:
        raise error.UnknownXtorError(xtorType)

      # Add AHB_Transaction.h for transactors that uses them 
      if xtorType in ('AHB_Master_S2T', 'AHB_Slave_T2S',
                      'AHB_Lite_Master_S2T', 'AHB_Lite_Slave_T2S',
                      'AHB_Master_T2S', 'AHB_Slave_S2T') and not self.standAloneComponent():
        self.addIncludeFile('xactors/arm/AHB/include/AHB_Transaction.h')

    for port in self.inputs():
      self.__mxInputPorts.append(transactors.MaxSimSignalSlave(self, port.name(), port.width(), port.carbonName(), port.carbonNetId(), port.expr(), port.paramName()))
    for port in self.nullInputs():
      self.__mxInputPorts.append(transactors.MaxSimNullSignalSlave(self, port.name(), 32))
    for port in self.outputs():
      self.__mxOutputPorts.append(transactors.MaxSimSignalMaster(self, port.name(), port.width(), port.carbonName(), port.carbonNetId(), port.expr()))
    for port in self.nullOutputs():
      self.__mxOutputPorts.append(transactors.MaxSimNullSignalMaster(self, port.name(), 32))
    for port in self.clocks():
      self.__mxClockPorts.append(transactors.ClockGenPin(self, port))
    for port in self.resets():
      self.__mxClockPorts.append(transactors.ResetGen(self, port))
    self.__mxPorts.extend(self.__mxInputPorts)
    self.__mxPorts.extend(self.__mxOutputPorts)
    self.__mxPorts.extend(self.__mxClockPorts)

    # Create MXDI object
    self.__mxdi = cadi.MxDI(True, self.compName(), self, self.debugRegs(), self.debugMemories(), self.procInfo(), self.cadi(), self.elfLoader())
    
    # Create Sub Components
    self.__subComps = [subcomponent.SubComponent(self, comp) for comp in self.subComponents()]

    # Assign an index to each CADI interface. Needed for Stand Alone components
    index = 0
    for c in self.loopCadi():
      c.setIndex(index)
      index += 1
      
    # Mark Parameters for Tie Nets
    for tie in self.ties():
      if tie.type() == 'TieParam':
        for param in self.parameters():
          if param.name() == tie.getParam():
            param.addTieNet(tie)

    # Mark Parameters for eslPorts
    for port in self.compPorts():
      for param in self.parameters():
        if param.name() == port.tieParam():
          param.addESLPort(port)

    # If this is a model kit config it may require the model kit
    # library. If so, add the appropriate cxx/link flags
    if self.requiresMkLibrary():
      # Determine the compiler suffix for the model kit library.
      # Windows always has one.
      suffixWin = '_' + self.mxCompiler()
      # Linux has one if the modelkit library is versioned, which
      # started with MK 3.1.0.
      if self.useVersionedMkLibrary():
        suffixLinux = '_' + self.mxCompiler()
      else:
        suffixLinux = ''

      # Add the cxx flags for the MK library
      self.__cxxFlags += ' -I$(CARBON_MK_HOME)/include'

      # Add the Linux link options
      self.addLinkFlag('-L$(CARBON_MK_HOME)/lib/Linux -L. -lmodelkit' + suffixLinux)

      # Add the windows link options
      lib = '$(CARBON_MK_HOME)\lib\Win\libmodelkit' + suffixWin + '.lib'
      self.addWindowsLinkFlag(lib)

    # Check For Flow Thru Paths
    for port in self.compPorts():
      if port.isFlowThru():
        self.__hasFlowThru = True
        break

    # Check and warn for unsupported Flow Thru paths
    for port in self.compPorts():
      port.checkFlowThru();

    # Check and warn for flow through paths from scDepositable signals
    if (self.__db) :
      for signal in self.__db.iterAsyncDeposits():
        if (signal.isScDepositable()): output.warning("%d: %s: Flow Through paths from scDepositable nets are not supported." % (error.carbonCfgERR_NOFLOWSUPPORT, signal.fullName()))

    # If they asked for the debuggable point functions, create the helper class
    if self.useDebuggablePoint():
      self.__debuggablePoint = DebuggablePoint(self.compName())
    else:
      self.__debuggablePoint = None

  def output(self):
    return self.__output
  
  def message(self, msg):
    for line in msg.splitlines():
      # Check if the message is a warning or error, in that case output it unconditinally
      if line.startswith("Warning") or line.startswith("Error") or line.startswith("Fatal") or line.startswith("Alert"):
        self.__output.write(line, always = True)
      else:
        self.__output.write(line)
    
  def profile(self): return self.__profile

  # get the SoC Designer enum name for the Cfg color
  def profileBucketMxColor(self, color):
    colorMap = {
      Carbon.Cfg.eCarbonCfgRed : 'eslapi::CAPI_COLOR_RED',
      Carbon.Cfg.eCarbonCfgBlue : 'eslapi::CAPI_COLOR_BLUE',
      Carbon.Cfg.eCarbonCfgGreen : 'eslapi::CAPI_COLOR_GREEN',
      Carbon.Cfg.eCarbonCfgYellow : 'eslapi::CAPI_COLOR_YELLOW',
      Carbon.Cfg.eCarbonCfgOrange : 'eslapi::CAPI_COLOR_ORANGE',
      Carbon.Cfg.eCarbonCfgBrown : 'eslapi::CAPI_COLOR_BROWN',
      Carbon.Cfg.eCarbonCfgPurple : 'eslapi::CAPI_COLOR_PURPLE',
      Carbon.Cfg.eCarbonCfgLightGray:    'eslapi::CAPI_COLOR_LIGHTGREY',
      Carbon.Cfg.eCarbonCfgLightBlue:    'eslapi::CAPI_COLOR_LIGHTBLUE',
      Carbon.Cfg.eCarbonCfgLightCyan:    'eslapi::CAPI_COLOR_LIGHTCYAN',
      Carbon.Cfg.eCarbonCfgLightSeaGreen:'eslapi::CAPI_COLOR_LIGHTSEAGREEN',
      Carbon.Cfg.eCarbonCfgLightCoral:   'eslapi::CAPI_COLOR_LIGHTCORAL',
      Carbon.Cfg.eCarbonCfgLightYellow1: 'eslapi::CAPI_COLOR_LIGHTYELLOW1',
      Carbon.Cfg.eCarbonCfgLightSalmon1: 'eslapi::CAPI_COLOR_LIGHTSALMON1',
      Carbon.Cfg.eCarbonCfgLightGreen:   'eslapi::CAPI_COLOR_LIGHTGREEN'
      }
    return colorMap.get(color, 'eslapi::CAPI_COLOR_BLACK')

  def xtorPortFactories(self):
    names = self.__xtorPortFactories.keys()
    names.sort()
    for name in names:
      yield self.__xtorPortFactories[name]

  # lists of SoC Designer component ports
  def compPorts(self): return iter(self.__mxPorts)
  def compInputPorts(self): return iter(self.__mxInputPorts)
  def compOutputPorts(self): return iter(self.__mxOutputPorts)
  def compClockPorts(self): return iter(self.__mxClockPorts)

  def findPort(self, name):
    for port in self.compPorts():
      if port.name() == name: return port
    return None
  
  def compFlowThruInputPorts(self):
    "Returns a list of component input ports that are connected to flow through paths in the Carbon Model."
    ftInputs = []
    for input in self.compInputPorts() :
      if input.isFlowThru(): ftInputs.append(input)
    return ftInputs
  
  def compFlowThruOutputPorts(self):
    "Returns a list of component output ports that are connected to flow through paths in the Carbon Model."
    ftOutputs = []
    for output in self.compOutputPorts() :
      if output.isFlowThru(): ftOutputs.append(output)
    return ftOutputs
  
  def hasFlowThru(self): return self.__hasFlowThru

  def hasInputClock(self):
    for xtor in self.transactors():
      if (xtor.type() == 'Clock_Input'):
        return True
    return False

  def hasOutputClock(self):
    for xtor in self.transactors():
      if (xtor.type() == 'Clock_Output'):
        return True
    return False

  def hasSystemCPorts(self):
    for xtor in self.transactors():
      if len(xtor.systemCPorts()) > 0:
        return True
    return False
    
  # clock generators
  def clocks(self):
    for clock in Carbon.Cfg.Config.clocks(self):
      yield clock
    for clock in self.__clocks:
      yield clock
  def numClocks(self):
    return len(Carbon.Cfg.Config.clocks(self)) + len(self.__clocks)
  # Adds a new clock generator to the config.
  # Returns in the index number fo the clock generator
  def addClock(self, eslName, rtlName, initialValue, delay, clockCycles, compCycles, dutyCycle):
    class ImpliedClock(Carbon.Cfg.Port):
      def __init__(self, eslName, rtlName, initialValue, delay, clockCycles, compCycles, dutyCycle):
        Carbon.Cfg.Port.__init__(self, eslName, rtlName, 1, "", "", 0)
        self.__initialValue = initialValue
        self.__delay = delay
        self.__clockCycles = clockCycles
        self.__compCycles = compCycles
        self.__dutyCycle = dutyCycle
      def initialValue(self): return self.__initialValue
      def delay(self): return self.__delay
      def clockCycles(self): return self.__clockCycles
      def compCycles(self): return self.__compCycles
      def dutyCycle(self): return self.__dutyCycle
    self.__clocks.append(ImpliedClock(eslName, rtlName, initialValue, delay, clockCycles, compCycles, dutyCycle))
    return self.numClocks() - 1

  def resets(self):
    for reset in Carbon.Cfg.Config.resets(self):
      yield reset
    for reset in self.__resets:
      yield reset
  def numResets(self):
    return len(Carbon.Cfg.Config.resets(self)) + len(self.__resets)
  def addReset(self, reset):
    self.__resets.append(reset)

  def compResetTicks(self):
    # compute the length of the reset cycle by taking the max of all resets
    resetTicks = 0
    for reset in self.compPorts():
      if reset.isReset():
        cycles = reset.cyclesBefore() + reset.cyclesAsserted() + reset.cyclesAfter()
        # scale to ticks (fixed 100 ticks per component cycle),
        # considering clock:component ratio
        ticks = cycles * reset.compCycles() * 100 / reset.clockCycles()
        if (ticks > resetTicks):
          resetTicks = ticks

    return resetTicks
  

  # number of xtor ports that need to be updated on a different schedule
  # than the SoC Designer clock
  def numCyclePorts(self):
    numCyclePorts = 0
    for port in self.__mxPorts:
      if port.hasCycleClock():
        numCyclePorts = numCyclePorts + 1
    return numCyclePorts

  def addIncludeFile(self, file):
    self.__includes.add(file)
    
  def includeFiles(self):
    # Do local files first
    for file in self.__includes:
      yield file
    # Then iterate over the files from the ccfg
    for file in Carbon.Cfg.Config.includeFiles(self):
      yield file

  def addForwardDecl(self, name):
    self.__forwardDecls.add(name)

  def forwardDecls(self):
    return self.__forwardDecls
  
  def addSourceFile(self, file):
    self.__sources.add(file)
    
  def sourceFiles(self):
    # Do local files first
    for file in self.__sources:
      yield file
    # Then iterate over the files from the ccfg
    for file in Carbon.Cfg.Config.sourceFiles(self):
      yield file
    
  def addLinkFlag(self, flag):
    self.__linkFlags += ' ' + flag
    
  def linkFlags(self):
    return self.__linkFlags + ' ' + Carbon.Cfg.Config.linkFlags(self)

  def cxxFlags(self):
    return self.__cxxFlags + ' ' + Carbon.Cfg.Config.cxxFlags(self)
    
  def addWindowsLinkFlag(self, flag):
    self.__winLinkFlags += ' ' + flag
    
  def windowsLinkFlags(self):
    return self.__winLinkFlags + ' ' + Carbon.Cfg.Config.linkFlags(self)
    
  # list of debug register groups, sorted by name
  def debugRegGroups(self): return self.__debugRegGroups

  # Check if input is flow-through
  def isFlowThruInput(self, name):
    if self.__dbNodes.has_key(name) :
      return self.__dbNodes[name].isAsyncInput()
    else :
      return False
      
  # Check if input is flow-through
  def isFlowThruOutput(self, name):
    if self.__dbNodes.has_key(name) :
      return self.__dbNodes[name].isAsyncOutput()
    else :
      return False

  # objectName is the <design> part of lib<design>.a
  def objectName(self):
    name = Carbon.Cfg.carbonCfgGetLibName(self._cfg)
    targetName = os.path.split(name)[-1].split('.')[0][3:]
    return targetName

  # displayName is most of the time same as compName, unless the config
  # wants to use a different name than the source and object files.
  def displayName(self):
    if self.compDisplayName() == "":
      return self.compName()
    else:
      return self.compDisplayName()
  
  # name of input .ccfg file
  def ccfgFile(self):
    return self.__ccfgName

  # return IO Database
  def getDB(self):
    return self.__db

  # Maxsim Version and Compiler
  # WARNING - these return strings, not integers!
  def mxVer(self):      return self.__mxVer
  def mxMinorVer(self): return self.__mxMinorVer
  def mxRevision(self): return self.__mxRevision
  def mxCompiler(self): return self.__mxCompiler
  def mxCompilerLibSuffix(self):
    if self.mxCompiler() == "vc8":
      return "_vc8"
    if self.mxCompiler() == "vc9":
      return "_vc9"
    if self.mxCompiler() == "vc10":
      return "_vc10"        
    if self.mxCompiler() == "vc11":
      return "_vc11"
    return "_vcUNKNOWN"
  
  # parameters
  def addParameter(self, param):
    self.__parameters.append(param)
  def parameters(self): return self.__parameters
  def dbPathParameter(self): return self.__carbonDbPathParameter
  def dumpWavesParameter(self): return self.__dumpWavesParameter
  def waveFileParameter(self): return self.__waveFileParameter
  def waveFormatParameter(self): return self.__waveFormatParameter
  def waveTimescaleParameter(self): return self.__waveTimescaleParameter
  def alignWavesParameter(self): return self.__alignWavesParameter

  # waveforrm type as a string
  def waveTypeName(self):
    if self.waveType() == Carbon.Cfg.eCarbonCfgNone:
      # if no wave type selected, default to VCD
      return 'VCD'
    if self.waveType() == Carbon.Cfg.eCarbonCfgVCD:
      return 'VCD'
    if self.waveType() == Carbon.Cfg.eCarbonCfgFSDB:
      return 'FSDB'

  def mxdi(self):
    return self.__mxdi

  # Loop through all mxdi(cadi) object in the component
  def loopCadi(self):
    if self.mxdi().hasCadi():
      yield self.mxdi()
    for comp in self.subComps():
      if comp.mxdi().hasCadi():
        yield comp.mxdi()
        
  def subComps(self):
    return self.__subComps

  def hasSubComps(self):
    return len(self.subComps()) > 0

  def scope(self):
    return self.__scope

  def debuggablePointFnDefs(self, userCode):
    code = ''
    if self.useDebuggablePoint():
      code += self.__debuggablePoint.emitMembers('private', '  ', userCode, self)
    return code

  def debuggablePointFnImpl(self, userCode):
    code = ''
    if self.useDebuggablePoint():
      code = self.__debuggablePoint.emitImplementation(userCode, self)
    return code

  def addDebuggablePointMemberFunctions(self, classDef):
    if self.useDebuggablePoint():
      self.__debuggablePoint.addMemberFunctions(classDef)

  def getPGO(self):
    return self.__pgo

  def supportsArchSaveRestore(self):
    # Architectural save/restore support began with SoCD 7.5
    major = int(self.mxVer())
    minor = int(self.mxMinorVer())
    if (major > 7) or ((major == 7) and (minor >= 5)):
      return True
    return False

# Class to manage functions to support debuggable point
class DebuggablePoint(cpp.Class):
  def __init__(self, className):
    cpp.Class.__init__(self, className, [])
    self.addMemberFunctions(self)

  # Add the members to the passed in cpp class defintion
  def addMemberFunctions(self, classDef):
    # Add the member functions
    classDef.addMemberFunction('void', 'procStopAtDebuggablePoint', '(bool stop)', access = 'private')
    func = classDef.addMemberFunction('bool', 'procCanStop', '()', access = 'private')
    func.addCode('return false;\n')
