
import carbondbapi
import DBIter

##
# \file
#
# This file contains the implementation of DBNode.

##
#    \defgroup CarbonDBNodeDoc Database Node
#    @{
#    @}
#
#    \addtogroup CarbonDBNodeDoc
#    \brief The following class or classes describe database node functionality 
#    @{


## This object is the context for a database node.
# It contains information about a particular signal in a design.
class DBNode:
        
    def __init__(self, cObj, node):
        self.mCObj = cObj
        self.mNode = node

    ## Returns the fully-qualified name of a signal
    #
    #  This returns a path like "a.b.c". You can use this method
    #  simply by using the print method
    def __str__(self):
        return self.fullName()

    ##  Returns the fully-qualified name of a signal
    #
    #   This returns a path like "a.b.c".
    def fullName(self):
        return carbondbapi.carbonDBNodeGetFullName(self.mCObj, self.mNode)

    ##  Returns the leaf name of a signal
    #
    #   For a signal named "a.b.c" this returns 'c'.
    def leafName(self):
        return carbondbapi.carbonDBNodeGetLeafName(self.mCObj, self.mNode)

    ## Is this a primary input signal?
    #
    #    If this is a primary input then this signal would appear in
    #    the iterInputs() iterator.
    #
    #    \retval 1 If this is a primary input
    #    \retval 0 If this is not a primary input
    def isPrimaryInput(self):
        return carbondbapi.carbonDBIsPrimaryInput(self.mCObj, self.mNode)

    ##  Is this a primary output signal?
    #
    #    If this is a primary output then this signal would appear in
    #    the iterOutputs() iterator.
    #
    #    \retval 1 If this is a primary output
    #    \retval 0 If this is not a primary output
    def isPrimaryOutput(self):
        return carbondbapi.carbonDBIsPrimaryOutput(self.mCObj, self.mNode)

    ## Is this a primary bidirect signal?
    #
    #    If this is a primary bidirect then this signal would appear in
    #    the iterBidis() iterator.
    #
    #    \retval 1 If this is a primary bidi
    #    \retval 0 If this is not a primary bidi
    def isPrimaryBidi(self):
        return carbondbapi.carbonDBIsPrimaryBidi(self.mCObj, self.mNode)

    ##  Is this SystemC observeable
    #
    #    If this is a primary output then this signal would appear in
    #    the iterScObservable() iterator.
    #
    #    \retval 1 If this is a SystemC observable
    #    \retval 0 If this is not SystemC observeable
    def isScObservable(self):
        return carbondbapi.carbonDBIsScObservable(self.mCObj, self.mNode)

    ##  Is this SystemC depositable
    #
    #    If this is a SystemC input then this signal would appear in
    #    the iterScDepositable() iterator.
    #
    #    \retval 1 If this is a SystemC depositable
    #    \retval 0 If this is not SystemC depositable
    def isScDepositable(self):
        return carbondbapi.carbonDBIsScDepositable(self.mCObj, self.mNode)

    ##  Is this net depositable?
    #
    #
    #    \retval 1 If this is depositable 
    #    \retval 0 If this is not depositable
    def isDepositable(self):
        return carbondbapi.carbonDBIsDepositable(self.mCObj, self.mNode)

    ##  Is this net observable?
    #
    #
    #    \retval 1 If this is observable 
    #    \retval 0 If this is not observable
    def isObservable(self):
        return carbondbapi.carbonDBIsObservable(self.mCObj, self.mNode)

    ## Is this an input port of a module/entity?
    #
    #
    #    \retval 1 If this is an input
    #    \retval 0 If this is not an input
    def isInput(self):
        return carbondbapi.carbonDBIsInput(self.mCObj, self.mNode)

    ##  Is this an output port of a module/entity?
    #
    #
    #    \retval 1 If this is an output
    #    \retval 0 If this is not an output
    def isOutput(self):
        return carbondbapi.carbonDBIsOutput(self.mCObj, self.mNode)

    ## Is this a bidirect port of a module/entity?
    #
    #
    #    \retval 1 If this is a bidirect
    #    \retval 0 If this is not a bidirect
    def isBidi(self):
        return carbondbapi.carbonDBIsBidi(self.mCObj, self.mNode)

    ##  Is this a clock signal?
    #
    #    A clock signal is a signal anywhere in the design that
    #    directly drives a sequential schedule (ie., flop).
    #    
    #    \retval 1 If this is a clock
    #    \retval 0 If this is not a clock
    def isClk(self):
        return carbondbapi.carbonDBIsClk(self.mCObj, self.mNode)

    ## Is this a clock tree signal?
    #
    #    If this is a primary clock tree signal then this signal would
    #    appear in the iterClkTree() iterator.
    #
    #    \retval 1 If this is a clock tree signal
    #    \retval 0 If this is not a clock tree signal
    def isClkTree(self):
        return carbondbapi.carbonDBIsClkTree(self.mCObj, self.mNode)

    ## Is this a primary asynchronous input signal?
    #
    #    If this is a primary asynchronous signal then this signal would
    #    appear in the iterAsyncInputs() iterator.
    #
    #    Note that this is the same as isAsyncInput(). This function is
    #    kept for backwards compatability.
    #
    #    \retval 1 If this is a primary async input
    #    \retval 0 If this is not a primary async input
    def isAsync(self):
        return carbondbapi.carbonDBIsAsync(self.mCObj, self.mNode)
    
    ## Is this a primary asynchronous input signal?
    #
    #    If this is a primary asynchronous signal then this signal would
    #    appear in the iterAsyncInputs() iterator.
    #
    #    \retval 1 If this is a primary async input
    #    \retval 0 If this is not a primary async input
    def isAsyncInput(self):
        return carbondbapi.carbonDBIsAsync(self.mCObj, self.mNode)
    
    ## Is this a primary asynchronous output signal?
    #
    #    If this is a primary asynchronous signal then this signal would
    #    appear in the iterAsyncOutputs() iterator.
    #
    #    \retval 1 If this is a primary async output
    #    \retval 0 If this is not a primary async output
    def isAsyncOutput(self):
        return carbondbapi.carbonDBIsAsyncOutput(self.mCObj, self.mNode)
    
    ## Is this a primary asynchronous deposit signal?
    #
    #    If this is a primary asynchronous signal then this signal would
    #    appear in the iterAsyncDeposits() iterator.
    #
    #    \retval 1 If this is a primary async deposit
    #    \retval 0 If this is not a primary async deposit
    def isAsyncDeposit(self):
        return carbondbapi.carbonDBIsAsyncDeposit(self.mCObj, self.mNode)
    
    ##  Is this a primary asynchronous posedge reset?
    #
    #    If this is a primary asynchronous posedge reset then this
    #    signal would appear in the iterAsyncPosResets() iterator.
    #
    #    \retval 1 If this is a primary asynchronous posedge reset
    #    \retval 0 If this is not a primary asynchronous posedge reset
    def isAsyncPosReset(self):
        return carbondbapi.carbonDBIsAsyncPosReset(self.mCObj, self.mNode)

    ##  Is this a primary asynchronous negedge reset?
    #
    #    If this is a primary asynchronous negedge reset then this
    #    signal would appear in the iterAsyncNegResets() iterator.
    #
    #    \retval 1 If this is a primary asynchronous negedge reset
    #    \retval 0 If this is not a primary asynchronous negedge reset
    def isAsyncNegReset(self):
        return carbondbapi.carbonDBIsAsyncNegReset(self.mCObj, self.mNode)

    ##  Is this a 2-D array?
    #
    #    \retval 1 If this is a two-dimensional array
    #    \retval 0 If this is not a two-dimensional array
    def is2DArray(self):
        return carbondbapi.carbonDBIs2DArray(self.mCObj, self.mNode)

    ##  Is this a vector?
    #
    #    A vector (or 1 dimensional array) can have a width of 1 or
    #    greater.
    #
    #    \retval 1 If this is a vector 
    #    \retval 0 If this is not a vector
    def isVector(self):
        return carbondbapi.carbonDBIsVector(self.mCObj, self.mNode)

    ##  Is this a scalar?
    #
    #    A scalar can only have a width of 1. A scalar and a 1-bit
    #    vector are not the same. One does not imply the other.
    #
    #    \retval 1 If this is a vector 
    #    \retval 0 If this is not a vector
    def isScalar(self):
        return carbondbapi.carbonDBIsScalar(self.mCObj, self.mNode)

    ##  Is this net tristate?
    #
    #
    #    \retval 1 If this is a tristate
    #    \retval 0 If this is not a tristate
    def isTristate(self):
        return carbondbapi.carbonDBIsTristate(self.mCObj, self.mNode)

    ##  Is this net constant?
    #
    #
    #    \retval 1 If this is constant 
    #    \retval 0 If this is not constant
    def isConstant(self):
        return carbondbapi.carbonDBIsConstant(self.mCObj, self.mNode)

    ## Is this net sample-scheduled?
    #
    #    Net value updates are sometimes delayed if the value is
    #    is not needed in the design at the time it changes.
    #
    #    \retval 1 If this is sample-scheduled
    #    \retval 0 If this is not sample-scheduled
    def isSampleScheduled(self):
        return carbondbapi.carbonDBIsSampleScheduled(self.mCObj, self.mNode)

    ## Returns the number of bits need to represent the signal value.
    def getWidth(self):
        return carbondbapi.carbonDBGetWidth(self.mCObj, self.mNode)

    ##  Returns the most significant bit of a vector.
    #
    #    For scalars, this returns 0. For vectors, this returns the index
    #    of the msb of the vector. For 2-D arrays, this returns the
    #    index of the msb of the row.
    def getMsb(self):
        return carbondbapi.carbonDBGetMSB(self.mCObj, self.mNode)

    ##  Returns the least significant bit of a vector.
    #
    #    For scalars, this returns 0. For vectors, this returns the index
    #    of the lsb of the vector. For 2-D arrays, this returns the
    #    index of the lsb of the row.
    def getLsb(self):
        return carbondbapi.carbonDBGetLSB(self.mCObj, self.mNode)


    ##  Get the left address of a two-dimensional array
    #
    #    For scalars and vectors, this returns 0. For two-dimensional
    #    arrays, this returns the leftmost address. For example, for a
    #    memory that has an address range of [0:100] this will return 0.
    def get2DArrayLeftAddr(self):
        return carbondbapi.carbonDBGet2DArrayLeftAddr(self.mCObj, self.mNode)


    ##  Get the right address of a two-dimensional array
    #
    #    For scalars and vectors, this returns 0. For two-dimensional
    #    arrays, this returns the rightmost address. For example, for a
    #    memory that has an address range of [0:100] this will return 100.
    def get2DArrayRightAddr(self):
        return carbondbapi.carbonDBGet2DArrayRightAddr(self.mCObj, self.mNode)


    ## Get the parent node for this parent, or NULL if it is at the top level
    def getParent(self):
        node = carbondbapi.carbonDBNodeGetParent(self.mCObj, self.mNode)
        if node:
            return DBNode(self.mCObj, node)
        else:
            return None

    ## Get the intrinsic type for this node
    def intrinsicType(self):
        return carbondbapi.carbonDBIntrinsicType(self.mCObj, self.mNode)

    ## Get the declaration type for this node
    def declarationType(self):
        return carbondbapi.carbonDBdeclarationType(self.mCObj, self.mNode)

    ## Get the library name, where the type is compiled
    def typeLibraryName(self):
        return carbondbapi.carbonDBtypeLibraryName(self.mCObj, self.mNode)

    ## Get the package name, where the type is compiled
    def typePackageName(self):
        return carbondbapi.carbonDBtypePackageName(self.mCObj, self.mNode)

    ## Get the module/architecture name for this node
    def componentName(self):
        return carbondbapi.carbonDBComponentName(self.mCObj, self.mNode)

    ## Get the source HDL for this node
    def sourceLanguage(self):
        return carbondbapi.carbonDBSourceLanguage(self.mCObj, self.mNode)

    ## Get the pull mode for this node
    #
    #  Returns one of "pullup", "pulldown", or "none"
    def getPullMode(self):
        return carbondbapi.carbonDBGetPullMode(self.mCObj, self.mNode)
    
    ## Is this a temp net
    #
    #
    #    \retval 1 If this is a temp net
    #    \retval 0 If this is not a temp net
    def isTemp(self):
        return carbondbapi.carbonDBIsTemp(self.mCObj, self.mNode)


    ## Does this node trigger a schedule on its positive edge?
    #
    #  \note Only primary inputs or nets marked depositable
    #  can return non-zero. Their aliases cannot.
    #
    #  \retval 1 If a 0->1 transition causes a schedule to run.
    #  \retval 0 If a 0->1 transition does not a cause a schedule to run.
    def isPosedgeTrigger(self):
        return carbondbapi.carbonDBIsPosedgeTrigger(self.mCObj, self.mNode)

    ## Does this node trigger a schedule on its negative edge?
    #
    #  \note Only primary inputs or nets marked depositable
    #  can return non-zero. Their aliases cannot.
    #
    #  \retval 1 If a 0->1 transition causes a schedule to run.
    #  \retval 0 If a 0->1 transition does not a cause a schedule to run.
    def isNegedgeTrigger(self):
        return carbondbapi.carbonDBIsNegedgeTrigger(self.mCObj, self.mNode)

    ## Does this node trigger a schedule on any edge?
    #
    #  Same as isNegedgeTrigger() && isPosedgeTrigger()
    #
    #  \note Only primary inputs or nets marked depositable
    #  can return non-zero. Their aliases cannot.
    #
    #  \retval 1 If any transition causes a schedule to run.
    #  \retval 0 If any transition does not a cause a schedule to run.
    def isBothEdgeTrigger(self):
        return carbondbapi.carbonDBIsBothEdgeTrigger(self.mCObj, self.mNode)

    ## Does this node trigger a schedule and feed a data path?
    #
    #  A node that feeds a data path must always be kept up to date so
    #  that async paths always have the correct value.
    #
    #  \note Only primary inputs or nets marked depositable
    #  can return non-zero. Their aliases cannot.
    #
    #  \retval 1 If this is a schedule trigger and feeds a data path.
    #  \retval 0 If this is not a schedule trigger or does not feed a
    #  data path.
    def isTriggerUsedAsData(self):
        return carbondbapi.carbonDBIsTriggerUsedAsData(self.mCObj, self.mNode)

    ## Is this node a primary input, bidi, or depositable that feeds live
    ## logic?
    #
    #
    #  \note A primary bidi is considered both a primary input and a
    #  primary output for this function.
    #
    #  If a logical path beginning at a primary input or depositable was
    #  found to be dead logic then the fanout of this node is not
    #  live. If the node is a trigger it is considered live.
    #
    #  \note Only primary inputs or nets marked depositable
    #  can return non-zero. Their aliases cannot.
    #
    #  \retval 1 If this is a depositable or primary input that feeds
    #  live logic.
    #  \retval 0 If this not a primary input or depositable, or if it is
    #  does not feed live logic.
    def isLiveInput(self):
        return carbondbapi.carbonDBIsLiveInput(self.mCObj, self.mNode)


    ## Is this node a primary output, bidi or observable driven by live
    ## logic?
    #
    #  \note A primary bidi is considered both a primary input and a
    #  primary output for this function.
    #
    #  If a logical path ending at a primary output or observable was
    #  found to be dead logic then the fanin to this node is not live.
    #
    #  \note Only primary outputs or nets marked observable
    #  can return non-zero. Their aliases cannot.
    #
    #  \retval 1 If this is an observable or primary outputs that is driven
    #  by live logic.
    #  \retval 0 If this not a primary output or observable, or if it is
    #  it is not driven
    def isLiveOutput(self):
        return carbondbapi.carbonDBIsLiveOutput(self.mCObj, self.mNode)

    ## Returns whether or not the signal is a user-specified OnDemand
    ## idle deposit net.
    #
    #    \retval 1 if the signal was compiled with the
    #    onDemandIdleDeposit directive.
    #    \retval 0 if the signal was not compiled with the
    #    onDemandIdleDeposit directive.
    def isOnDemandIdleDeposit(self):
        return carbondbapi.carbonDBIsOnDemandIdleDeposit(self.mCObj, self.mNode)

    ## Returns whether or not the signal is a user-specified OnDemand
    ## excluded state point.
    #
    #    \retval 1 if the signal was compiled with the
    #    onDemandExcluded directive.
    #    \retval 0 if the signal was not compiled with the
    #    onDemandExcluded directive.
    def isOnDemandExcluded(self):
        return carbondbapi.carbonDBIsOnDemandExcluded(self.mCObj, self.mNode)

    ## Returns whether the node represents a structure (e.g. VHDL record)
    ## as defined in the RTL.
    # 
    # \retval 1 If the node is a structure
    # \retval 0 If the node is not a structure
    def isStruct(self):
        return carbondbapi.carbonDBIsStruct(self.mCObj, self.mNode)

    ## Returns whether the node represents an array as defined in the
    ## RTL.
    # 
    # \retval 1 If the node is an array
    # \retval 0 If the node is not an array

    def isArray(self):
        return carbondbapi.carbonDBIsArray(self.mCObj, self.mNode)

    ## Returns whether the node represents an enumeration as defined in the
    ## RTL.
    # 
    # \retval 1 If the node is an enumeration
    # \retval 0 If the node is not an enumeration

    def isEnum(self):
        return carbondbapi.carbonDBIsEnum(self.mCObj, self.mNode)

    def getNumberElemInEnum(self):
        return carbondbapi.carbonDBGetNumberElemInEnum(self.mCObj, self.mNode)

    def getEnumElem(self, index):
        return carbondbapi.carbonDBGetEnumElem(self.mCObj, self.mNode, index)

    ## Returns the number of fields in a structure
    # 
    # \returns Number of fields, if the node is a structure, otherwise -1

    def getNumStructFields(self):
        return carbondbapi.carbonDBGetNumStructFields(self.mCObj, self.mNode)

    ## Returns a node representing a field of a structure.  The name is
    ## only the name of the field itself, excluding its parent hierarchy.
    # 
    # \param name Name of the field
    # \returns Node representing the field, if it exists, otherwise NULL

    def getStructFieldByName(self, name):
        node = carbondbapi.carbonDBGetStructFieldByName(self.mCObj, self.mNode, name)
        if node:
            return DBNode(self.mCObj, node)
        else:
            return None

    ## Returns the left bound of an array's range as declared in the RTL.
    ## For multidimensional arrays, the range of the outermost dimension
    ## is used.
    # 
    # \returns Left array bound, if the node is an array, otherwise -1

    def getArrayLeftBound(self):
        return carbondbapi.carbonDBGetArrayLeftBound(self.mCObj, self.mNode)

    ## Returns the right bound of an array's range as declared in the RTL.
    ## For multidimensional arrays, the range of the outermost dimension
    ## is used.
    # 
    # \returns Right array bound, if the node is an array, otherwise -1

    def getArrayRightBound(self):
        return carbondbapi.carbonDBGetArrayRightBound(self.mCObj, self.mNode)

    ## Returns the number of dimensions of an array
    # 
    # \returns Number of dimensions, if the node is an array, otherwise -1
    def getArrayDims(self):
        return carbondbapi.carbonDBGetArrayDims(self.mCObj, self.mNode)

    ## Returns the number of declared dimensions of an array
    # 
    # \returns Number of diclared dimensions, if the node is an array, otherwise -1
    def getArrayNumDeclaredDims(self):
        return carbondbapi.carbonDBGetArrayNumDeclaredDims(self.mCObj, self.mNode)

    ## Returns the right bound of an array's range for the dimension dim.
    # 
    # \returns Right array bound, if the node is an array, otherwise -1
    def getArrayDimRightBound(self, dim):
        return carbondbapi.carbonDBGetArrayDimRightBound(self.mCObj, self.mNode, dim);

    ## Returns the left bound of an array's range for the dimension dim.
    # 
    # \returns Left array bound, if the node is an array, otherwise -1
    def getArrayDimLeftBound(self, dim):
        return carbondbapi.carbonDBGetArrayDimLeftBound(self.mCObj, self.mNode, dim);


    ## Returns a node representing an element of an array.  Multiple
    ## indices can be specified at once for easier navigation of
    ## multi-dimensional arrays.  If the number of dimensions provided is
    ## less than the number of dimensions in the array, the element
    ## returned will represent the remaining dimensions.  For example, if
    ## two dimensions are specified for a three-dimensional array, the
    ## element will represent the inner one-dimensional array after the
    ## two specified dimensions are applied.
    # 
    # \param indices List of indexes, with the outermost dimension at offset 0
    # \pamam dims Number of dimensions specified in the the index array
    # \returns Node representing the element, if it exists, otherwise NULL
    def getArrayElement(self, indices, dims):
        # Convert the Python list to an int array
        a = carbondbapi.intArray(dims)
        for i in range(dims):
            a[i] = indices[i]
        node = carbondbapi.carbonDBGetArrayElement(self.mCObj, self.mNode, a, dims)
        if node:
            return DBNode(self.mCObj, node)
        else:
            return None


    ## Returns whether a CarbonNetID can be created for a CarbonDBNode
    # 
    # \param dbContext The database object for the design
    # \param node The design node of interest
    # \retval 1 If the node can be a CarbonNetID
    # \retval 0 If the node can not be a CarbonNetID

    def canBeCarbonNet(self):
        return carbondbapi.carbonDBCanBeCarbonNet(self.mCObj, self.mNode);

    ## Returns the aggregate bit size of a CarbonDBNode.  If the node is
    ## a scalar, its size is the number of bits required to represent it.
    ## For example, a bit has a size of 1, and an integer has a size of
    ## 32.  If the node is a structure, the size is the sum of the sizes
    ## of each of its fields.  If the node is an array, the size is the
    ## product of the number of elements and a single element's size.
    ## 
    ## The size of any other type of node (for example, a module
    ## instance) is 0.
    # 
    # \param dbContext The database object for the design
    # \param node The design node of interest
    # \returns The bit size of the node

    def getBitSize(self):
        return carbondbapi.carbonDBGetBitSize(self.mCObj, self.mNode);


    ## Returns The left bound of the range constraint of a CarbonDBNode, if the node is
    ## a scalar, otherwise -1.
    # 
    # \param dbContext The database object for the design
    # \param node The design node of interest
    # \returns The left bound of the range constraint the node
    def getRangeConstraintLeftBound(self):
        return carbondbapi.carbonDBGetRangeConstraintLeftBound(self.mCObj, self.mNode);

    ## Returns The right bound of the range constraint of a CarbonDBNode, if the node is
    ## a scalar, otherwise -1.
    # 
    # \param dbContext The database object for the design
    # \param node The design node of interest
    # \returns The right bound of the range constraint the node
    def getRangeConstraintRightBound(self):
        return carbondbapi.carbonDBGetRangeConstraintRightBound(self.mCObj, self.mNode);


    ## Returns 1 when initial declaration of this array requires range declaration, otherwise 0.
    ## a scalar, otherwise 0.
    # 
    # \param dbContext The database object for the design
    # \param node The design node of interest
    # \returns 1 when initial declaration of this array requires range declaration
    def isRangeRequiredInDeclaration(self):
        return carbondbapi.carbonDBIsRangeRequiredInDeclaration(self.mCObj, self.mNode);

    ## Returns the name of the source file in which a node is defined
    # 
    # \param dbContext The database object for the design
    # \param node The design node of interest
    # \returns The source file name

    def getSourceFile(self):
        return carbondbapi.carbonDBGetSourceFile(self.mCObj, self.mNode)

    ## Returns the source file line on which a node is defined
    # 
    # \param dbContext The database object for the design
    # \param node The design node of interest
    # \returns The source file line number

    def getSourceLine(self):
        return carbondbapi.carbonDBGetSourceLine(self.mCObj, self.mNode)

    ## Returns whether a node is contained by a composite
    # 
    # Returns whether a node is contained by a composite node type,
    # i.e. an array or structure.
    # 
    # \param dbContext The database object for the design
    # \param node The design node of interest
    # \retval 1 If the node is contained by a composite
    # \retval 0 If the node is not contained by a composite

    def isContainedByComposite(self):
        return carbondbapi.carbonDBIsContainedByComposite(self.mCObj, self.mNode)


    ## Returns whether or not the signal was tied with a tieNet directive.
    #
    #    \retval 1 if the signal was tied with the tieNet directive.
    #    \retval 0 if the signal was not tied with the tieNet directive.
    def isTied(self):
        return carbondbapi.carbonDBIsTied(self.mCObj, self.mNode)

    ## Returns the indeces of the dim dimension for a memory net.
    ## For example, for a  net temp[1:4][3:7] it will
    ## return [1:4] for dim = 0 and [3:7] for dim = 1
    #
    #  \param  dim - dimension of the memory net
    #  \retval rangeDim - range of indices for the dim.
    def getMemIndices(self, dim):
      numDims = self.getArrayDims()
      if dim < 0 or dim >= numDims:
        message = "The dimension %d for the Memory net %s is out of the range" % (dim, self.fullName())
        raise message

      rangeDim = []
      indices = []
      curNode = self.mNode
      for i in range(dim+1):
        leftBound = carbondbapi.carbonDBGetArrayLeftBound(self.mCObj, curNode)
        rightBound = carbondbapi.carbonDBGetArrayRightBound(self.mCObj, curNode)

        if i == dim:
          rangeDim = [leftBound, rightBound]
        else:
          indices.append(leftBound)
          # Convert the Python list to an int array
          a = carbondbapi.intArray(len(indices))
          for i in range(len(indices)):
            a[i] = indices[i]
             
          curNode = carbondbapi.carbonDBGetArrayElement(self.mCObj, self.mNode, a, len(indices))
        
      return rangeDim


    ## Returns the number of CarbonNets inside of this memory.
    ## For example, for a  net temp[1:4][3:7] it will be 4.
    #
    #  \retval total - number of CarbonNets.
    def getMemNumCarbonNets(self):
      numDims = self.getArrayDims()
      total = 1
      for i in range(numDims-1):
        rangeDim = self.getMemIndices(i)
        left = rangeDim[0]
        right = rangeDim[1]
        number = abs(left - right) + 1

        total *= number

      return total

    ## Returns the width of this memory.
    ## For example, for a  net temp[1:4][3:7] it will be 5.
    #
    #  \retval width - width of the memory net.
    def getMemWidth(self):
      numDims = self.getArrayDims()
      
      if numDims > 2:
        indices = self.getMemIndices(numDims-1)
        width = abs(indices[0] - indices[1]) + 1
      else:
        width = self.getWidth()
        
      return width

    ##   Iterate all children of the node
    #    
    #    This iterates the children of this node
    #        
    #    \returns a DBIter.DBIter object
    def iterChildren(self):
        return DBIter.DBIter(self.mCObj, carbondbapi.carbonDBLoopChildren, self.mNode)
##
#    @}

