#******************************************************************************
# Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#******************************************************************************

# Useful libraries
import sys
import string
import time

# Code to parse/write XML files
import xml.sax.handler
import xml.dom.minidom

import Analyze

class ReadConfig (xml.sax.ContentHandler):
  def __init__(self):
    # Create a port<->function dictionary
    self.__mPortDict = {}
    self.__mOptions = {}
    self.__mPortFunctions = {}

  def startElement(self, name, attributes):
    if name == "Port":
      if not attributes.has_key("name"):
        print "Warning: Module element is missing \"name\" attribute"
        return

      if not attributes.has_key("functionClass"):
        print "Warning: Module element is missing \"functionClass\" attribute"
        return

      fclass = str(attributes["functionClass"])

      # The mapping dictionary will look like this:
      #         (class)  (actual name)
      # dict["InputVector"]["RADDR"]["Function"] = "ReadAddr"
      # dict["InputVector"]["RADDR"]["Port"] = "0"
      
      for f in ["function", "portNumber"]:

        if not attributes.has_key(f):
          print "Warning: Module element is missing \"%(f)s\" attribute" % f
          return

        # Make the dictionary key capitalized, preserving embedded captial letters
        # (This is only for aesthetics)
        fname = f[0].upper() + f[1:]
        # dict[type][name]["Function"] = function
        # dict[type][name]["PortNumber"] = 0
        self.__mPortDict.setdefault(fclass, {}).setdefault(str(attributes["name"]), {})[fname] \
                                            = str(attributes[f])
        self.__mPortFunctions[str(attributes[f])] = 1


    elif name == "Option":
      if attributes.has_key("name") and attributes.has_key("value"):
        self.__mOptions[attributes["name"]] = attributes["value"]


  def getDict(self):
    return self.__mPortDict

  def getFunctions(self):
    return self.__mPortFunctions

  def getOptions(self):
    return self.__mOptions

  def read(self, filename):
    # Make sure we have a valid file
    try:
      f = open(filename, "r")
    except IOError, (errno, strerror):
      print "Error: could not read port configuration file '%s', reason: '%s'" % \
            (filename, strerror)
      return 1
    except:
      print "Error: could not read port configuration file '%s', unexpected error: '%s'" % \
            (filename, sys.exc_info()[0])
      return 1

    # Parse the file
    try:
      xml.sax.parse(f, self)
    except xml.sax.SAXException, msg:
      print "%s" %msg
      return 2

    return 0

class Generate:
  def __init__(self, portDict):
    self.__mPortDict = portDict         # The mapping of port name to function

  # Create a dict of port functions and their names, given a list of names and sizes
  def assignFunctions(self, portTupleList):
    ports = {}
    params = {}
    sizes = {}
    # ("portName", "Direction", size)
    for port_tuple in portTupleList:
      name, direction, size = port_tuple
      
      # type is one of "InputBit", "InputVector", "OutputBit", "OutputVector"
      type = Analyze.analyze.getType(direction, size)

      port_num = self.__mPortDict[type][name]["PortNumber"]
      port_function = self.__mPortDict[type][name]["Function"]

      # ports["0"]["ReadAddr"] = "RADDR"
      # or 
      # ports["0"]["ExtraAsserts"] = [("RTN", "", "1'b1"), ...]

      # "ExtraAsserts", "ExtraInputs", and "ExtraOutputs" are special

      if port_function == "ExtraInputs" \
                                or port_function == "ExtraOutputs" \
                                or port_function == "ExtraAsserts0" \
                                or port_function == "ExtraAsserts1":
        ports.setdefault(port_num, {}).setdefault(port_function, []).append((name, size))

      else:
        ports.setdefault(port_num, {})[port_function] = (name, size)

    return ports
  
