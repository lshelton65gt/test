#******************************************************************************
# Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

# Useful libraries
import time

# Symbol table files
import Carbon.MV.SymbolTable
import Carbon.MV.ShadowHierarchy

# This helper class is a base class to generate various portions
# of the wrapper model. The derived class must overload functions
# to emit various parts of the files. The base class provides
# access to some of the design names and ports as well as some
# target simulator/API neutral emitting code
class Base:

  # The constructor
  def __init__(self, modelName, objectName, hierarchy, hdlExt, dbType,
               preserveNames):
    self.__mModelName = modelName
    self.__mObjectName = objectName
    self.__mHierarchy = hierarchy
    self.__mHdlExt = hdlExt
    self.__mDBType = dbType
    self.__mPreserveNames = preserveNames
    self.__mCPPNames = {} # used C++ names
    self.__mCommandLine = "";

    self.__mCountObserves = 0
    self.__mCountDeposits = 0
    self.__mCountPorts = 0

  def getNumberPorts(self):
    return self.__mCountPorts
  def getNumberObserves(self):
    return self.__mCountObserves
  def getNumberDeposits(self):
    return self.__mCountDeposits
  
  ## The following set of functions must be overriden by the
  ## implementation class 

  # Function to emit the start of the HDL file (returns the string)
  def emitHdlStart(self):
    raise 'Must implement emitHdlStart()'

  # Function to emit the start of the top HDL model (returns the string)
  def emitHdlModelStart(self, node, isTop, ports):
    raise 'Must implement emitHdlModelStart()'

  # Function to emit the start of the logic once all net declarations are made
  def emitHdlLogicStart(self, node, isTop):
    raise 'Must implement emitHdlLogicStart()'
  
  # Function to emit the hookup of the Carbon Model and net c-models (returns the string)
  def emitHdlCModelHookup(self):
    raise 'Must implement emitHdlCModelHookup()'

  # Function to emit the end of the top HDL model (returns the string)
  def emitHdlModelEnd(self, node, isTop):
    raise 'Must implement emitHdlModelEnd()'

  # Function to emit the declaration type for a node
  def emitDeclarationType(self, node):
    raise 'Must implement emitDeclarationType()'

  def countPortObserveDeposits(self, node):
    # Ignore primary ports
    if node.isLeaf():
      if node.isPrimaryPort():
        self.__mCountPorts += 1
      else:
       if node.isObservable():
         self.__mCountObserves += 1
       if node.isDepositable():
         self.__mCountDeposits += 1
    

  # Function to emit an internal net declaration
  def emitInternalNet(self, node):
    raise 'Must implement emitInternalNet()'

  # Function to emit the beginning of a generate block.
  def emitGenerateInstanceBegin(self, label, min_idx, max_idx):
    raise 'Must implement emitGenerateInstanceBegin()'

  # Function to emit the end of a generate block.
  def emitGenerateInstanceEnd(self, node, label, min_idx, max_idx):
    return ''

  # Function to emit a sub-instances declaration
  def emitComponentDeclaration(self, subNode, ignorePreserves):
    raise 'Must implement emitComponentDeclaration()'

  # Function to emit a sub-instance of an HDL component
  def emitComponentInstance(self, subNode):
    raise 'Must implement emitComponentInstance()'

  # Function to emit the start of the C File (returns the string)
  def emitCStart(self):
    raise 'Must implement emitCStart()'

  # Function to emit the structure declaration
  def emitCStructureDeclaration(self, direction):
    raise 'Must implement emitCStructureDeclaration()'

  # Function eo emit the C Structure model field (returns the string)
  def emitCStructureModel(self):
    raise 'Must implement emitCStructureModel()'

  # Function to emit the start of the init C-Model process/task
  def emitInitCModelStart(self, evalFn, quitFn):
    raise 'Must implement emitInitCModelStart()'

  # Function to emit a hookup for a port
  def emitCPortHookup(self, node, scopeStack):
    raise 'Must implement emitCPortHookup()'

  # Function to emit the start of the init C-Model process/task
  def emitInitCModelEnd(self, evalFn, quitFn):
    raise 'Must implement emitInitCModelStart()'

  # Function to emit the function prototype for a callback model
  def emitCallbackFn(self, fnName):
    raise 'Must implement emitCallbackFn()'

  # Function to emit the structure name
  def emitStructureName(self):
    raise 'Must implement emitStructureName()'

  # Function to emit the structure initialization (casting) code
  def emitStructureInit(self):
    raise 'Must implement emitStructureInit()'

  # Function to emit the initialization of the time variable
  def emitTimeInit(self, variable):
    raise 'Must implement emitTimeInit()'
  
  # Function to emit the return of the eval c-model
  def emitEvalCModelReturn(self):
    raise 'Must implement emitEvalCModelReturn()'

  # Function to emit the data port update
  def emitDataPortUpdate(self, node):
    raise 'Must implement emitDataPortUpdate()'

  # Function to emit the disconnect of a port in the terminate function
  def emitCPortDisconnect(self, node):
    raise 'Must implement emitCPortDisconnect()'

  # Function to emit any extra make file variables
  def emitExtraMakeVariables(self):
    raise 'Must implement emitExtraMakeVariables()'

  # Function to emit the makefile type
  def emitMakefileType(self):
    raise 'Must implement emitMakefileType()'

  # Function that returns a comment string for the language
  def commentStr(self):
    raise 'Must implement commentStr()'

  # Function to return the free function for the target simulator
  def freeFn(self):
    raise 'Must implement freeFn()'

  # Function to emit any simulator/language specific files
  def emitOtherFiles(self):
    raise 'Must implement emitOtherFiles()'

  # Function to emit any simulator/language specific files
  def emitPrintString(self, varName):
    raise 'Must implement emitPrintString()'

  # Function to emit any simulator/language specific files
  def emitSimulatorFatalExit(self):
    raise 'Must implement emitSimulatorFatalExit()'

  ## The following functions are utilities that can be used by
  ## the derived classes.

  # Returns the top model name
  def modelName(self):
    return self.__mModelName

  # Returns the object name - can be used to create
  # names like lib%(objectName)%.h
  def objectName(self):
    return self.__mObjectName

  # Returns a process name for the Carbon Model
  def vhmProcessName(self):
    return 'carbon_proc_' + self.modelName()

  # Returns a process name to sync a net
  def syncNetProcessName(self):
    return 'carbon_proc_' + self.modelName() + '_sync_net'

  # returns a c-function name for initing the Carbon Model
  def cVHMFuncName(self):
    return 'carbon_' + self.modelName() + '_vhm_init'

  # returns a c-function name for creating the Carbon Model
  def cVHMCreateName(self):
    return 'carbon_' + self.modelName() + '_vhm_create'

  # Returns the shared library name for the Carbon Model
  def libraryName(self):
    return 'carbon_' + self.modelName() + '.so'

  # Returns the c file name
  def cFileName(self):
    return 'carbon_' + self.modelName() + '.c'

  # Returns the HDL file name
  def hdlFileName(self):
    return 'carbon_' + self.modelName() + '.' + self.__mHdlExt

  # Returns the Makefile name
  def makefileName(self):
    return 'Makefile.carbon.' + self.modelName()

  # Return the port list
  def getPrimaryPorts(self):
    return self.__mHierarchy.getPrimaryPorts()

  # Emit a string for the correct DB Type
  def emitDBType(self):
    return 'eCarbon%s' % self.__mDBType

  # Default seperator for simulator hierarchy
  def getSepSim(self, node):
    raise 'Must implement getSepSim()'

  # Default seperator for Carbon hierarchy
  def getSepCarbon(self):
    return "."

  # Default seperator for strtuctures hierarchy
  def getSepStruct(self):
    return "."

  # Function to create a hierachical name from scopes, a node, and a
  # separator
  def hierName(self, node, scopeStack, sep):
    hierName = ''
    for scope in scopeStack:
      hierName += '%s%s' % (scope, sep)
    hierName += node.leafName()
    return hierName

  # Save the actual command line so we can write it to all generated files.
  def setCommandLine(self, commandLine):
    self.__mCommandLine = commandLine

  # Function to emit a language neutral copyright notice. The variables
  # are the language comment start/middle end notation.
  def copyrightNotice(self, commentStartStr, commentInterStr, commentEndStr):
    return '''\
%(commentStartStr)s******************************************************************************
%(commentInterStr)s Copyright (c) 2006-%(year)s by Carbon Design Systems, Inc., All Rights Reserved.
%(commentInterStr)s
%(commentInterStr)s THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
%(commentInterStr)s DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
%(commentInterStr)s THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
%(commentInterStr)s APPEARS IN ALL COPIES OF THIS SOFTWARE.
%(commentInterStr)s 
%(commentInterStr)s This file was automatically generated by Model Validation.  Any manual changes
%(commentInterStr)s may be overwritten and lost.
%(commentInterStr)s
%(commentInterStr)s Generated by MV on %(date)s
%(commentInterStr)s Command line: %(command)s
%(commentInterStr)s******************************************************************************%(commentEndStr)s
''' % { 'year'            : time.strftime("%Y", time.localtime()),
        'commentStartStr' : commentStartStr,
        'commentInterStr' : commentInterStr,
        'commentEndStr'   : commentEndStr,
        'date'            : time.strftime("%a, %d %b %Y %H:%M:%S %Z", time.localtime()),
        'command'         : self.__mCommandLine
        }

  # Function to create a valid C identifier from an HDL name
  def cIdentifier(self, node):
    # Get the HDL name to start, check the cache
    hdlName = 's_' + node.uniqueName()
    if self.__mCPPNames.has_key(hdlName):
      return self.__mCPPNames[hdlName]
    
    # Replace all questionable chars with _
    name = ''
    for c in hdlName:
      code = ord(c)
      if code >= ord('a') and code <= ord('z'):
        name += c
      elif code >= ord('A') and code <= ord('Z'):
        name += c
      elif code >= ord('0') and code <= ord('9'):
        name += c
      else:
        name += '_'
    
    # If name is not unique, keep appending a number to it until it is
    newName = name
    i = 0;
    while self.__mCPPNames.has_key(newName):
      newName = name + '_' + str(i)
      i = i + 1

    # Use the new uniquified and fixed c identifier
    self.__mCPPNames[newName] = hdlName
    self.__mCPPNames[hdlName] = newName
    return newName

  # Function to create a valid C string from an HDL name
  def cString(self, name):
    # Replace any \'s with a double \\ and any string characters with \"
    newName = ''
    for c in name:
      code = ord(c)
      if code == ord('\\'):
        newName += c + c
      elif code == ord('"'):
        newName += '\\"'
      else:
        newName += c
    return newName

  # Returns true if entity/module names should be preserved
  def preserveNames(self):
    return self.__mPreserveNames


  # Emits the types for composite nets
  def emitCompositeType(self, node, top, skipArray) :
    raise 'Must implement emitCompositeType()'

  # Emit the type of Array
  def emitArrayType(self, node):
    raise 'Must implement emitArrayType()'

  # Emit the type of VHDL record
  def emitStructType(self, node):
    raise 'Must implement emitStructType()'

  # This function is used only for memory nets, which are
  # a field of a record.
  def emitLeafType(self, node):
    raise 'Must implement emitLeafType()'

  # Function to emit libraries to be included
  def emitLibraryIncludes(self, node) :
    raise 'Must implement emitLibraryIncludes()'
  
  # Returns true if the node has custom type and it's not defined in a package
  def filterLibPack(self, node) :
    return False
