#******************************************************************************
# Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

import Carbon.MV.VHPI

# Implement the helper class to emit the HDL and C-Model for NCSim VHDL
# The base class Carbon.MV.VHDL.Helper implements the target simulator-
# independent code for VHDL.  The base class Carbon.MV.VHPI.Helper
# implements the target simulator-independent code for VHPI.
class Helper(Carbon.MV.VHPI.Helper):

  # Constructor
  def __init__(self, modelName, objectName, hierarchy, dbType, preserveNames):
    Carbon.MV.VHPI.Helper.__init__(self, modelName, objectName, hierarchy,
                                   dbType, preserveNames)

  # Function to emit the start of the C file
  def emitCStart(self):
    code = '#define NCSIM\n'
    code += '#include "carbon/MVVhpiUtils.c"\n'
    return code

  # Function to emit the makefile type
  def emitMakefileType(self):
    return 'ncsimvhpi'

  # Function to emit the end of the top HDL model
  def emitHdlModelEnd(self, node, isTop):
    if node.isStruct() or node.isArray():
      return ""
    
    code = ""

    if isTop:
      code += self.emitVHPIcompInstance()
      code += 'end carbon;\n\n'
    else:
      code += '''\
end %(archName)s;  -- %(compName)s

''' % self.namesHelper(node)

    return code

  # Function to return the port setup function
  def portSetupFn(self, node):
    code = 'vhpiSetup'

    # The input or bids can have a Clock/Data field, so outputs don't
    if not node.isOutput():
      if node.isPrimaryClock():
        code += 'Clock'
      else:
        code += 'Data'

    # Add the direction part of the function name
    code += node.portDirection()
    return code

  # Function to emit the type of a leaf net
  def emitLeafType(self, node):
    return ""

  # Function to emit the type of composite
  def emitCompositeType(self, node, top, skipArray) :
    return ""

  # Function to emit libraries to be included
  def emitLibraryIncludes(self, node) :
    library_package = {}
    code = ""

    for subNode in node.subNodes():
      netName = subNode.leafName()
      # check the libraries and packages
      libName = subNode.typeLibraryName()
      packName = subNode.typePackageName()
      if libName and packName :
        library_package.setdefault(libName, set()).add(packName)


    for lib in library_package.keys() :
      code += "library %s;\n" %lib
      for pack in library_package[lib] :
        code += "use %s.%s.all;\n" %(lib, pack)

      code += "\n"
      
    return code

  # Returns true if the node has custom type and it's not defined in a package
  def filterLibPack(self, node) :
    rtnFlag = False
    if node.isBranch() :
      if node.isStruct() or (node.isArray() and not node.canBeCarbonNet()):
        libName = node.typeLibraryName()
        packName = node.typePackageName()
        if libName and packName :
          rtnFlag = False
        else :
          rtnFlag = True

    elif node.isLeaf() and  not node.canBeCarbonNet( ) :
      libName = node.typeLibraryName()
      packName = node.typePackageName()
      if libName and packName :
        rtnFlag = False
      else :
        rtnFlag = True
    elif node.isVector() and not (node.declarationType() == "bit_vector"
                                  or node.declarationType() == "std_logic_vector"
                                  or node.declarationType() == "std_ulogic_vector"
                                  or node.declarationType() == "signed"
                                  or node.declarationType() == "unsigned"):
      # This is an array of bits ot std_logic.
      # The array type should be declared in a package
      libName = node.typeLibraryName()
      packName = node.typePackageName()
      if libName and packName :
        rtnFlag = False
      else :
        rtnFlag = True
      
    return rtnFlag    

  

