#******************************************************************************
# Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

import Carbon.MV.Helper

# Implement the helper class to emit the HDL and C-Model for */VPI
# This should be derived and overloaded for target simulator specific
# changes.
class Helper(Carbon.MV.Helper.Base):

  # Constructor
  def __init__(self, modelName, objectName, hierarchy, dbType, preserveNames):
    Carbon.MV.Helper.Base.__init__(self, modelName, objectName, hierarchy, 'v',
                                   dbType, preserveNames)

  # Function to return the c init function name; must be overloaded
  def simInit(self):
    raise 'Must implement simInit()'

  # Function to return the c routine to init all the carbon command
  # system tasks.  Default is vpiCarbonCommandInit. Override if the
  # simulator doesn't support vpi access to user defined system
  # function arguments and results
  def cCarbonCmdInit(self):
    return 'vpiCarbonCommandInit'
 
  # Function to emit the port list
  def emitPortList(self):
    return '\n\t, '.join([port.leafName() for port in self.getPrimaryPorts()])

  # Function to emit the vector range (or empty) for a node
  def emitNodeRange(self, node):
    code = ''
    if node.isVector():
      code += ' [%d:%d]' % (node.getMsb(), node.getLsb())
    return code

  # Function to emit the port direction
  def emitPortDirection(self, node):
    type = node.portDirection()
    direction = ''
    if type == "Input":
      direction = "input"
    elif type == "Output":
      direction = "output"
    elif type == "Bid":
      direction = "inout"
    else:
      raise "Unexepcted port direction '%s'" % type
    return direction

  # Function to emit the port declarations
  def emitPortDeclarationList(self):
    code = ''
    for port in self.getPrimaryPorts():
      code += '  %(portDirection)s%(portRange)s %(leafName)s;\n' \
              % { 'portDirection' : self.emitPortDirection(port),
                  'portRange'     : self.emitNodeRange(port),
                  'leafName'      : port.leafName() }
      if port.portDirection() == "Output":
        code += '  reg%(portRange)s %(leafName)s;\n' \
                % { 'portRange' : self.emitNodeRange(port),
                    'leafName'  : port.leafName() }
    return code

  # Function to emit a port index
  def emitPrimaryBidIndex(self, port, i):
    if port.isVector():
      return '[%d]' % i
    else:
      return ''
    
  # Function to emit hookups for primary bids
  def emitPrimaryBidHookups(self):
    # For primary bids, also emit enable and data declarations
    code = ''
    for port in self.getPrimaryPorts():
      if port.isPrimaryBidi():
        # Emit the declaration
        code += '''
  reg %(portRange)s %(leafName)s_carbon_data;
  reg %(portRange)s %(leafName)s_carbon_en;
''' % { 'portRange' : self.emitNodeRange(port),
        'leafName'  : port.leafName() }

        # Emit the code to assign to the bid from the data and enable
        if port.getLsb() < port.getMsb():
          bits = range(port.getLsb(), port.getMsb()+1)
        else:
          bits = range(port.getMsb(), port.getLsb()+1)
        for i in bits:
          code += "  assign %(leafName)s%(index)s = %(leafName)s_carbon_en%(index)s ? %(leafName)s_carbon_data%(index)s : 1'bz;\n" % \
            { 'leafName' : port.leafName(),
              'index'    : self.emitPrimaryBidIndex(port, i) }

        # Emit any pull up or pull down code
        mode = port.getPullMode()
        if mode == "pullup" or mode == "pulldown":
          code += "  %(mode)s %(leafName)s_carbon_pull [%(msb)d:%(lsb)d] (%(leafName)s);\n" % \
                  { 'mode'      : mode,
                    'leafName'  : port.leafName(),
                    'msb'       : port.getMsb(),
                    'lsb'       : port.getLsb() }
    return code

  # Function to emit the start of an HDL model
  def emitHdlModelStart(self, node, isTop, ports):
    # Start with the generic library
    code = ''

    # Add any user preamble. Note, we add a line feed in case the user didn't.
    preamble = node.getPreamble()
    if len(preamble) > 0:
      code += preamble + "\n"

    # Add the module declaration
    if isTop:
      code += '''\
module %(modelName)s(%(portList)s);
%(portDeclarationList)s
%(primaryBidHookups)s
''' % { 'modelName'           : self.modelName(),
        'portList'            : self.emitPortList(),
        'portDeclarationList' : self.emitPortDeclarationList(),
        'primaryBidHookups'   : self.emitPrimaryBidHookups() }
    else:
      code += 'module %(uniqueName)s;\n' % { 'uniqueName' : node.uniqueName() }
    return code

  # Function to emit the start of the logic once all net declarations are made
  def emitHdlLogicStart(self, node, isTop):
    return '\n'

  # Function to emit the hookup of the VPI-wrapper Carbon Model and sync nets
  def emitHdlCModelHookup(self):
    return '''
  initial begin
    #0 $%(vhmProcessName)s;
  end
''' % { "vhmProcessName" : self.vhmProcessName() }
    
  # Function to emit the declaration type for a node
  def emitDeclarationType(self, node):
    # If the user defined a type, use that instead
    declType = node.getTypeInfo()
    if not declType or len(declType) == 0:
      declType = node.intrinsicType()
    return declType

  # Function to emit an internal net declaration
  def emitInternalNet(self, node):
    # Create the signal declaration
    code = '  %(declaration)s%(nodeRange)s %(leafName)s;' \
           % { 'declaration' : self.emitDeclarationType(node),
               'nodeRange'   : self.emitNodeRange(node),
               'leafName'    : node.leafName() }

    # Add a comment if this is depositable and/or observable
    if node.isDepositable() or node.isObservable():
      code += ' // carbon'
      if node.isDepositable():
        code += ' depositSignal'
      if node.isObservable():
        code += ' observeSignal'
    code += '\n'
    return code

  # Function to emit the beginning of a generate block. Not impemented
  # for Verilog
  def emitGenerateInstanceBegin(self, label, min_idx, max_idx):
    return ''

  # Function to emit the end of a generate block. Not implemented
  # for Verilog
  def emitGenerateInstanceEnd(self, node, label, min_idx, max_idx):
    return ''

  # Function to emit a sub-instances declaration (none for Verilog)
  def emitComponentDeclaration(self, node, ignorePreserves):
    return ''

  # Function to emit a sub-instance of an HDL component
  def emitComponentInstance(self, node):
    return "  %s %s ();\n" % (node.uniqueName(), node.leafName())

  # Function to emit the end of the top HDL model
  def emitHdlModelEnd(self, node, isTop):
    return 'endmodule\n\n'

  # Function to emit the start of the C file
  def emitCStart(self):
    code = '#include "carbon/MVVpiUtils.c"\n'
    return code

  # Function to emit the structure declaration
  def emitCStructureDeclaration(self, direction):
    return "Vpi%(portDirection)s" % { "portDirection" : direction }

  # Function to emit the C structure model field
  def emitCStructureModel(self):
    return '  VpiCarbonModel\tcarbon;\n'

  # Function to return the port setup function
  def portSetupFn(self, node):
    code = 'vpiSetup'

    # The input or bids can have a Clock/Data field, so outputs don't
    if not node.isOutput():
      if node.isPrimaryClock():
        code += 'Clock'
      else:
        code += 'Data'

    # Add the direction part of the function name
    code += node.portDirection()
    return code

  # Seperator for Verilog hierarchy, overloaded from Helper
  def getSepSim(self, node):
    return "."

  # return the code to hookup a single port
  def emitCPortHookup(self, node, simHierPathName, carbonHierPathName):
    # Create a string for the hierarchical name.
    # (In Verilog, the Carbon name and the Verilog name use the same
    #  seperators, so we just use one name, unlike VHDL.)
    hierName = self.cString(carbonHierPathName + node.leafName())

    # Primary bids have a drive flag
    drive = ''
    if node.isPrimaryBidi():
      drive = 'TRUE, '
    elif node.isBidi():
      drive = 'FALSE, '

    # Emit the code for the hookup
    return '  success &= %(portSetupFn)s(&data->carbon, moduleHndl, "%(modelName)s", "%(hierName)s", %(netWidth)d, %(drive)s%(delay)d, &data->%(fieldName)s);\n' % \
      { "portSetupFn" : self.portSetupFn(node),
        "modelName"   : self.modelName(),
        "hierName"    : hierName,
        "netWidth"    : node.getWidth(),
        "drive"       : drive,
        'delay'       : node.getDelay(),
        'fieldName'   : self.cIdentifier(node) }
    
  # Return the code to update the data ports
  def emitDataPortUpdate(self, node):
    code = '    vpiUpdateData%(portDirection)s(&data->%(fieldName)s);\n' % \
           { "portDirection" : node.portDirection(),
             "fieldName"     : self.cIdentifier(node) }
    return code

  # Return the code to cleanup all ports
  def emitCPortDisconnect(self, node):
    code = '  vpiCleanup%(portDirection)s(&data->%(fieldName)s);\n' % \
           { "portDirection" : node.portDirection(),
             "fieldName"     : self.cIdentifier(node) }
    return code

  # Function to emit the function prototype for a callback cmodel
  def emitCallbackFn(self, fnName):
    return 'int %s(s_cb_data* cb_data)' % fnName

  # Returns the structure name for the ports
  def emitStructureName(self):
    return 'Vpi' + self.modelName() + 'Data'

  # Function to emit the structure initialization (casting) code
  def emitStructureInit(self):
    return 'cb_data->user_data'

  # Function to emit the initialization of the time variable
  def emitTimeInit(self, variable):
    return '''\
  /* Compute the time from the VPI time; do not use the callback
   * value because it is wrong in NCSim if the timescale is changed
   */
  {
    s_vpi_time vpiTime = { vpiSimTime, 0, 0, 0 };
    vpi_get_time(NULL, &vpiTime);
    time = (((CarbonTime)(vpiTime.high)) << 32);
    time |= ((CarbonTime)(vpiTime.low));
  }'''    
  
  # Function to emit the return of the eval c-model
  def emitEvalCModelReturn(self):
    return '''
  /* Test if $finish was called; if so, tell the target simulator */
  if (status == eCarbon_FINISH) {
    tf_dofinish();
  }
    
  /* Clear the cbRegistered flag so it can be enabled again */
  data->carbon.cbRegistered = FALSE;
  return 0;'''

  # Function to emit the start of the init C Model
  def emitInitCModelStart(self, evalFn, quitFn):
    code = '''\
/* Init user defined task; creates communication/execution callbacks */
static int %(cVHMFuncName)s(PLI_BYTE8 *user_data)
{
  char\tsuccess = TRUE;
  %(structureName)s*\tdata;
  vpiHandle\ttaskHandle;
  vpiHandle\t moduleHndl;
  s_vpi_time\ttime = {vpiSuppressTime, 0, 0, 0};
  s_cb_data\tquit_cb = {cbEndOfSimulation, %(quitFn)s, NULL, &time, NULL, 0, NULL};
  s_vpi_error_info\terror_info;

  /* Get the parent module */
  taskHandle = vpi_handle(vpiSysTfCall, NULL);
  moduleHndl = vpi_handle(vpiScope, taskHandle);

  /* Get the storage created during the compiletf call */
  data = (%(structureName)s*)vpi_get_userdata(taskHandle);

  /* Set up the carbon model callback data */
  data->carbon.evalCallback = %(evalFn)s;
  data->carbon.cbData = (PLI_BYTE8*)data;
'''  % { 'cVHMFuncName'    : self.cVHMFuncName(),
         'structureName'   : self.emitStructureName(),
         'modelName'       : self.modelName(),
         'evalFn'          : evalFn,
         'quitFn'          : quitFn }
    return code

  # Function to emit the end of the init C-Model
  def emitInitCModelEnd(self, evalFn, quitFn):
    return '''\
  /* Set up a callback to exit */
  quit_cb.user_data = (void*)data;
  vpi_register_cb(&quit_cb);
  if (vpi_chk_error(&error_info)) {
    vpi_printf(error_info.message);
  }
  return 0;
}

/* Create the Carbon Model and initialize the carbon command processing */
static int %(cVHMCreateName)s(PLI_BYTE8* user_data)
{
  char\tsuccess = TRUE;
  %(structureName)s*\tdata;
  vpiHandle taskHandle;
  vpiHandle moduleHndl;
  char* fullName;
  CarbonObjectID* model;
  s_cb_data	endOfCompile_cb = {cbEndOfCompile, vpiEndOfCompileCallback, NULL, NULL, NULL, 0, NULL};

  /* Get the parent module */
  taskHandle = vpi_handle(vpiSysTfCall, NULL);
  moduleHndl = vpi_handle(vpiScope, taskHandle);
  fullName = vpi_get_str(vpiFullName, moduleHndl);

  /* Allocate space to keep track of the sync data */
  data = (%(structureName)s*)malloc(sizeof(%(structureName)s));
  vpi_put_userdata(taskHandle, (void*)data);

  /* Create and init the Carbon model (handles wave dumping etc) */
  model = carbon_%(objectName)s_create(%(dbType)s, eCarbon_NoFlags);
  %(simInit)s(&data->carbon, fullName, "%(modelName)s", model);
  data->carbon.cbRegistered = FALSE;

  /* Register message callback */
  carbonAddMsgCB(model, messageCB, NULL);

  vpi_register_cb(&endOfCompile_cb);
}

void %(modelName)s_RegisterTfs( void )
{
  s_vpi_systf_data systf_data;
  vpiHandle        systf_handle;
  CarbonSys*       sys = NULL;
  int              firstCall;

  /* Get access to the Carbon system so we can tell if we are the first and
     register the carbon command */
  sys = carbonGetSystem(&firstCall);

  systf_data.type        = vpiSysTask;
  systf_data.sysfunctype = 0;
  systf_data.tfname      = "$%(vhmProcessName)s";
  systf_data.calltf      = %(cVHMFuncName)s;
  systf_data.compiletf   = %(cVHMCreateName)s;
  systf_data.sizetf      = 0;
  systf_data.user_data   = NULL;
  systf_handle = vpi_register_systf( &systf_data );

/* Causes failures on VCS */
/*  vpi_free_object( systf_handle ); */

  /* Register Verilog system functions to execute Carbon commands */
  if (firstCall) {
    /* We are the first Carbon Model, register the Carbon command UDTFs */
    %(cCarbonCmdInit)s();
  }
}
''' % { 'modelName'      : self.modelName(),
        'structureName'  : self.emitStructureName(),
        'objectName'     : self.objectName(),
        'dbType'         : self.emitDBType(),
        'vhmProcessName' : self.vhmProcessName(),
        'cVHMFuncName'   : self.cVHMFuncName(),
        'cVHMCreateName' : self.cVHMCreateName(),
        'cCarbonCmdInit' : self.cCarbonCmdInit(),
        'simInit'        : self.simInit() }

  # Function to emit the makefile type
  def emitMakefileType(self):
    return 'vpi'

  # Function that returns a comment string for the language
  def commentStr(self):
    return '//'

  # Function to return the free function for Modelsim
  def freeFn(self):
    return 'free'

  # Function to print a const char*
  def emitPrintString(self):
    # mti_PrintMessage() doesn't have the const qualifier for some reason
    return 'vpi_printf'

  # Function to cause the simulator to exit
  def emitSimulatorFatalExit(self):
    return  'tf_dofinish()'
