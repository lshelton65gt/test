#******************************************************************************
# Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

import Carbon.MV.VPI

# Implement the helper class to emit the HDL and C-Model for Aldec/VPI
# The base class Carbon.MV.VPI.Helper implements the target simulator
# independent code for VPI.
class Helper(Carbon.MV.VPI.Helper):

  # Constructor
  def __init__(self, modelName, objectName, hierarchy, dbType, preserveNames):
    Carbon.MV.VPI.Helper.__init__(self, modelName, objectName, hierarchy, dbType,
                                  preserveNames)

  # Function to emit the name of the c init function
  def simInit(self):
    return 'vpiInit'

  # Function to emit any extra make file variables - NC doesn't have
  # a HOME variable, use the default SIM_HOME
  def emitExtraMakeVariables(self):
    return ''

  # Function to emit the makefile type
  def emitMakefileType(self):
    return 'ncsimvpi'

  # There are no other Modelsim files
  def emitOtherFiles(self):
    return
