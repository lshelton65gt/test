#******************************************************************************
# Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

import Carbon.MV.VHDL

# Implement the helper class to emit the HDL and C-Model for VHDL/FLI
# The base class Carbon.MV.VHDL.Helper implements the target simulator-
# independent code for VHDL.
class Helper(Carbon.MV.VHDL.Helper):

  # Constructor
  def __init__(self, modelName, objectName, hierarchy, dbType, preserveNames):
    Carbon.MV.VHDL.Helper.__init__(self, modelName, objectName, hierarchy,
                                   dbType, preserveNames)

  # Function to emit the start of the top HDL model
  def emitHdlModelStartTop(self, node):
    return '''\
architecture carbon of %(modelName)s is
attribute foreign of carbon : architecture is "+%(cVHMFuncName)s %(libraryName)s";
''' %  { 'modelName'          : self.modelName(),
         'cVHMFuncName'       : self.cVHMFuncName(),
         'libraryName'        : self.libraryName() }

  # Function to emit the start of the C file
  def emitCStart(self):
    code  = '#include "mti.h"\n'
    code += '#include "carbon/MVFliUtils.c"\n'
    return code

  # Function to emit the structure declaration
  def emitCStructureDeclaration(self, direction):
    return "Fli%(portDirection)s" % { "portDirection" : direction }

  # Function to emit the C structure model field
  def emitCStructureModel(self):
    return '  FliCarbonModel\tcarbon;\n'

  # Function to return the port setup function
  def portSetupFn(self, node):
    code = 'fliSetup'

    # The input or bids can have a Clock/Data field, so outputs don't
    if not node.isOutput():
      if node.isPrimaryClock():
        code += 'Clock'
      else:
        code += 'Data'

    # Add the direction part of the function name
    code += node.portDirection()
    return code

  # Function to return the port cleanup function
  def portCleanupFn(self, node):
    code = 'fli'
    return code

  # Seperator for MTI hierarchy, overloaded from Helper
  def getSepSim(self, node):
    # Struct (i.e.VHDL record) seperators are usually periods...
    if node.isStruct() :
      return self.getSepStruct()
    else :
      return "/"

  # return the code to hookup a single port
  def emitCPortHookup(self, node, mtiHierPathName, carbonHierPathName):
    # Create a string for the MTI and Carbon hierarchical names. They
    # are different because they use different separators
    mtiHierName = mtiHierPathName + node.leafName()
    carbonHierName = carbonHierPathName + node.leafName()

    # Only primary output or bid ports are driven. This is not a valid
    # argument for inputs so set it to blank.
    if node.isPrimaryOutput() or node.isPrimaryBidi():
      drive = ", TRUE"
    elif node.isOutput() or node.isBidi():
      drive = ", FALSE"
    else:
      drive = ""

    code = ""

    # Wrap a "for" loop around memories
    if node.isMemory():
      code += '''\
  {
'''
      index = ""
      arr_index = ""
      index_values = ""
      numDims = node.getArrayDims()

      # carbon_i is the index of vector net, which
      # all the model nets are mapped to.
      code += '''\
    int carbon_i = 0;
'''
  
      # We are looping over all the dimensions,
      # of the memory net, except the inner one
      for k in range(0, numDims-1):
        index += "i"
        arr_index += "[%d]"
        index_values += ''', %(index)s''' % {"index" : index}
        rangeDim = node.getDBNode().getMemIndices(k)
        loop_min = min(rangeDim)
        loop_max = max(rangeDim)

        code += '''\
    int %(loop_index)s; 
''' % { "loop_index"          : (index)}
        
        # We emit the for loop for all the dimensions
        code += '''\
    for (%(loop_index)s = %(loop_min)d; %(loop_index)s <= %(loop_max)d;  %(loop_index)s++) {
''' % { "loop_index"          : (index),
        "loop_min"            : (loop_min),
        "loop_max"            : (loop_max)}

        if k == numDims-2:
          code +='''\
         char mtiName[1024];
         char carbonName[1024];

         sprintf (mtiName, "%(mtiHierName)s%(arr_idx)s"%(index_values)s);
         sprintf (carbonName, "%(carbonHierName)s%(arr_idx)s"%(index_values)s);
         success &= %(portSetupFn)s(&data->carbon, regionName, mtiName, "%(modelName)s", carbonName, %(netWidth)d%(drive)s, %(delay)d, &data->%(fieldName)s[carbon_i]);
''' % { "mtiHierName"    : mtiHierName,
        "carbonHierName" : carbonHierName,
        "portSetupFn"    : self.portSetupFn(node),
        "modelName"      : self.modelName(),
        "netWidth"       : node.getDBNode().getMemWidth(),
        'fieldName'      : self.cIdentifier(node),
        'drive'          : drive,
        'delay'          : node.getDelay(),
        "arr_idx"        : arr_index,
        "index_values"   : index_values}


          code +='''\
         carbon_i = carbon_i + 1;
'''

      # This loop emits end of for loops for extenal dimensions
      # of the memory net
      for m in range(0, numDims-1):
        code += '''\
    }
'''  
      code += '''\
  }
'''
    else:
      # Emit the code for the hookup
      code += '  success &= %(portSetupFn)s(&data->carbon, regionName, "%(mtiHierName)s", "%(modelName)s", "%(carbonHierName)s", %(netWidth)d%(drive)s, %(delay)d, &data->%(fieldName)s);\n' % \
            { "portSetupFn"    : self.portSetupFn(node),
              "mtiHierName"    : mtiHierName,
              "modelName"      : self.modelName(),
              "carbonHierName" : carbonHierName,
              "netWidth"       : node.getWidth(),
              'fieldName'      : self.cIdentifier(node),
              'drive'          : drive,
              'delay'          : node.getDelay() }

    return code
    
  # Return the code to update the data ports
  def emitDataPortUpdate(self, node):
    code = '    fliUpdateData%(portDirection)s(&data->%(fieldName)s);\n' % \
           { "portDirection" : node.portDirection(),
             "fieldName"     : self.cIdentifier(node) }
    return code

  # Return the code to cleanup all ports
  def emitCPortDisconnect(self, node):
    # depth is zero for non-memories
    numNets = 1
    if node.isMemory():
      numNets = node.getDBNode().getMemNumCarbonNets()
      
    code = ""
    # emit the port hookups "depth" times for memories, once for everything else
    if node.isMemory():
      index = "[i]"
      indent = "    "
      code +='''\
  {
    int i;
    for (i = 0; i < %d; i++) {
''' % (numNets - 1)

    else:
      index = ""
      indent = ""

    code += '  %(indent)s%(portCleanupFn)sCleanup%(portDirection)s(&data->%(fieldName)s%(index)s);\n' % \
            { "indent"        : indent,
              "portCleanupFn" : self.portCleanupFn(node),
              "portDirection" : node.portDirection(),
              "fieldName"     : self.cIdentifier(node),
              "index"         : index }

    if node.isMemory():
      code += '''\
    }
  }
'''
    return code

  # Function to emit the function prototype for a callback cmodel
  def emitCallbackFn(self, fnName):
    return 'void %s(void* param)' % fnName

  # Returns the structure name for the ports
  def emitStructureName(self):
    return 'Fli' + self.modelName() + 'Data'

  # Function to emit the structure initialization (casting) code
  def emitStructureInit(self):
    return 'param'

  # Function to emit the initialization of the time variable
  def emitTimeInit(self, variable):
    return '''\
  /* Compute the time from the MTI time */
  time = (((CarbonTime)((unsigned)mti_NowUpper())) << 32);
  time |= ((CarbonTime)((unsigned)mti_Now()));'''    
  
  # Function to emit the return of the eval c-model
  def emitEvalCModelReturn(self):
    return ''

  # Function to emit the start of the init C Model
  def emitInitCModelStart(self, evalFn, quitFn):
    code = '''\
/* Init process; creates communication/execution callbacks */
void %(cVHMFuncName)s(
    mtiRegionIdT       region,
    char*              param,
    mtiInterfaceListT* generics,
    mtiInterfaceListT* ports)
{
  char\tsuccess = TRUE;
  %(structureName)s*\tdata = NULL;
  char*\tregionName;
  CarbonObjectID*\tvhm;

  /* Allocate space to keep track of the sync data */
  data = (%(structureName)s*)mti_Malloc(sizeof(%(structureName)s));

  /* Current region name */
  regionName = mti_GetRegionFullName(region);

  /* Create and init the Carbon model */
  vhm = carbon_%(objectName)s_create(%(dbType)s, eCarbon_NoFlags);
  fliInit(&data->carbon, regionName, "%(modelName)s", vhm);

  /* Create a process to run the Carbon model */
  data->carbon.proc = mti_CreateProcessWithPriority("VHM_%(modelName)s", %(evalFn)s, data, MTI_PROC_SYNCH);
'''  % { 'evalFn'          : evalFn,
         'quitFn'          : quitFn,
         'cVHMFuncName'    : self.cVHMFuncName(),
         'structureName'   : self.emitStructureName(),
         'objectName'      : self.objectName(),
         'dbType'          : self.emitDBType(),
         'modelName'       : self.modelName() }
    return code

  # Function to emit the end of the init C-Model
  def emitInitCModelEnd(self, evalFn, quitFn):
    return '''\
  /* Create cleanup callbacks */
  mti_AddQuitCB(%(quitFn)s, data);
  mti_AddRestartCB(%(quitFn)s, data);

  /* Register message callback */
  carbonAddMsgCB(vhm, messageCB, NULL);
 }

''' % { 'quitFn' : quitFn }

  # Function to emit any extra make file variables
  def emitExtraMakeVariables(self):
    return ''

  # Function to emit the makefile type
  def emitMakefileType(self):
    return 'mtifli'

  # Function that returns a comment string for the language
  def commentStr(self):
    return '--'

  # Function to return the free function for Modelsim
  def freeFn(self):
    return 'mti_Free'

  # There are no other Modelsim files
  def emitOtherFiles(self):
    return

  # Function to print a const char*
  def emitPrintString(self):
    return 'mti_PrintFormatted'

  # Function to cause the simulator to exit
  def emitSimulatorFatalExit(self):
    return 'mti_FatalError()'


  # Function to emit the type of composite
  def emitCompositeType(self, node, top, skipArray) :
    code = ""
    if node.isLeaf():
      # if the node is Leaf, go to the leaf function
      code += self.emitLeafType(node)
      
    elif node.isStruct() :
      # if the node is structure,
      # recurse it
      for subNode in node.subNodes():
        code += self.emitCompositeType(subNode, False, False)

    elif node.isArray() :
      # if the node is array,
      # get the element and recurse it.
      numDims = node.getArrayNumDeclaredDims()
       
      # We need to walk over all elements of arrays to get
      # correct count of observes/deposits, despite of that
      # we need to do it only once to emit the type declaration.
      leftBound = node.getArrayLeftBound()
      rightBound = node.getArrayRightBound()
      if leftBound < rightBound:
        vec = range(leftBound, rightBound+1)
      else:
        vec = range(rightBound, leftBound+1)        
      for i in vec:
        leftDbNode = node.getArrayElement([i], 1)
        elem = node.findSubNode(leftDbNode);
        
        # if this is multirange array, the numDims is more than 1.
        # we should emit the type once. For example, if the array is
        # of type array(3 to 4, 5 to 6), the numDims is 2, but internally
        # it is represented as array of array. We want to emit the top
        # array, and skip all the internal once.
        if numDims > 1:
          shouldSkip = True
        else:
          shouldSkip = False

        code += self.emitCompositeType(elem, False, shouldSkip)
        
    else :
      None

    # We are done with sub declaration,
    # now we declare the parent level
    if node.isStruct() :
      code += self.emitStructType(node)
    elif node.isArray() and (node.canBeCarbonNet() == False) :
      # Skip emitting for internal arrays of multirange arrays
      if skipArray == False:
        code += self.emitArrayType(node)
    else :
      None
        
    return code


  # This function returns the range of an array.
  # The array can have single or multiple ranges
  # This function is used only by VHDL helper.
  def getArrayRange(self, node):
    numDims = node.getArrayNumDeclaredDims()
    # dimStr will contain the string for the range,
    # like (5 to 7) or (5 to 7, 7 downto 3)
    dimStr = "("
    indices = []
      
    for k in range(0, numDims):
      left = node.getArrayDimLeftBound(k)
      right = node.getArrayDimRightBound(k)
      indices.append(left)
      if left > right:
        arrayRange = "downto"
      else:
        arrayRange = "to"

      dimStr += '''%d %s %d''' %  (left, arrayRange, right)
      if k != numDims-1:
        dimStr += ", "

    elemNode = node.getArrayElement(indices, numDims)
    dimStr += ")"
    
    return elemNode,dimStr


  # Function to emit the type of Array
  def emitArrayType(self, node):
    code = ""
    typeName = node.declarationType()
    if not self.__mSeenTypes.has_key(typeName):
      self.__mSeenTypes[typeName] = True

      elemNode,dimStr = self.getArrayRange(node)
      if node.isLeaf():
        elemTypeName = self.emitDeclarationType(node)
      else:
        elemTypeName = elemNode.declarationType()

      code = "  type %s is array %s of %s;\n" % (typeName, dimStr, elemTypeName)
    return code

  # Function to emit the type of VHDL record
  def emitStructType(self, node):
    code = ""
    typeName = node.declarationType()
    # The type declaration is emitted once,
    # but the count of observes,deposits is
    # done for every signal
    if not self.__mSeenTypes.has_key(typeName):
      self.__mSeenTypes[typeName] = True

      code = "  type %s is record\n" % typeName
      for subNode in node.subNodes():
        code += "    %s : %s;\n" % ( subNode.leafName(), self.emitDeclarationType(subNode))
      code += "  end record;\n"

    return code

  # Function to emit the constraint of a net of Enum type
  def getEnumConstraint(self, node):
    code = ""
    d1 = node.getRangeConstraintLeftBound()
    d2 = node.getRangeConstraintRightBound()
    if d1 != -1 and d2 != -1:
      
      code += " range  %s to %s" %(node.getEnumElem(d2), node.getEnumElem(d1))

    return code


  # This function is used only for memory nets, which are
  # a field of a record.
  def emitLeafType(self, node):
    code = ""
    typeName = node.declarationType()

    # We emit here the simple nets and memory nets
    if node.canBeCarbonNet() == True:
      if not self.__mSeenTypes.has_key(typeName):
        self.__mSeenTypes[typeName] = True
        if node.isEnum() :
          code += "  type %s is (" % typeName
          numElems = node.getNumberElemInEnum()
          for k in range(numElems) :
            if k == numElems - 1:
              code += "%s);\n" %node.getEnumElem(k)
            else:
              code += "%s, " %node.getEnumElem(k)

        elif node.intrinsicType() != node.declarationType() :
          # This is subtype declaration
          if node.intrinsicType() == "integer" or node.intrinsicType() == "natural" or  node.intrinsicType() == "positive" :
            # This is one of subtypes of integer, including ones with constraint
            d1 = node.getRangeConstraintLeftBound()
            d2 = node.getRangeConstraintRightBound()
            if d1 > d2:
              direction = "to"
            else :
              direction = "downto"
            
            code += "  subtype %s is %s range %d %s %d;\n" %(typeName, node.intrinsicType(), d2, direction, d1)
          elif node.intrinsicType() == "std_logic" or node.intrinsicType() == "std_ulogic" or node.intrinsicType() == "bit" :
            code += "  subtype %s is %s;\n" %(typeName, node.intrinsicType())
          elif node.intrinsicType() == "std_logic_vector" or node.intrinsicType() == "std_ulogic_vector" or  node.intrinsicType() == "bit_vector" or node.intrinsicType() == "signed" or node.intrinsicType() == "unsigned" :
            code += "  type %s is array " %node.declarationType()

            netRange = ""
            if not node.isRangeRequiredInDeclaration():
              if node.getMsb() > node.getLsb():
                netRange = "(%(high)d downto %(low)d)" % { 'high' : node.getMsb(),
                                                        'low' : node.getLsb() }
              else:
                netRange = "(%(high)d to %(low)d)" % { 'high' : node.getMsb(),
                                                  'low' : node.getLsb() }
            else:
              netRange ="(natural range<>)";

            code += netRange  

            elemType = ""
            if node.intrinsicType() == "std_logic_vector" or node.intrinsicType() == "signed" or node.intrinsicType() == "unsigned":
              elemType = "std_logic"
            elif  node.intrinsicType() == "std_ulogic_vector":
              elemType = "std_ulogic"
            elif node.intrinsicType() == "bit_vector":
              elemType = "bit"
              
            code += " of %s;\n" %elemType
       
    else:
      typeName = node.declarationType()

      if not self.__mSeenTypes.has_key(typeName):
        self.__mSeenTypes[typeName] = True

        elemNode,dimStr = self.getArrayRange(node)
        
        if elemNode.canBeCarbonNet():
          # This is vector net
          typeVect = elemNode.intrinsicType()
          left = elemNode.getArrayLeftBound()
          right = elemNode.getArrayRightBound()
          if left > right:
            vectRange = "downto"
          else:
            vectRange = "to"
 
          code +=  '''\
  type %s is array %s of %s (%d %s %d);
''' % (typeName, dimStr, typeVect, left, vectRange, right)
          
        else:
          elemType = elemNode.declarationType()
          code += self.emitLeafType(elemNode)
          code +=  '''\
  type %s is array %s of %s;
''' % (typeName, dimStr, elemType)

    return code


  # Function to emit libraries to be included
  def emitLibraryIncludes(self, node) :
    return ""
