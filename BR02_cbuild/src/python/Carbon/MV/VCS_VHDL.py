#******************************************************************************
# Copyright (c) 2008-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

import Carbon.MV.VHPI

# Implement the helper class to emit the HDL and C-Model for */VPI
# This should be derived and overloaded for target simulator specific
# changes.
class Helper(Carbon.MV.VHPI.Helper):

  # Constructor
  def __init__(self, modelName, objectName, hierarchy, dbType, preserveNames):
    Carbon.MV.VHPI.Helper.__init__(self, modelName, objectName, hierarchy,
                                   dbType, preserveNames)

  # Function to emit the start of the top HDL model; VCS's implementation is non-standard
  def emitHdlModelStartTop(self, node):
    return '''\
package carbon is
  procedure command (constant i : in string);
  attribute foreign of command : procedure is "vhpi:%(libraryName)s:vcsCarbonCommand:carboncommand";
  -- Not yet implemented, until there is a way to pass data from VHDL to the VCS UCLI command line.
  procedure command_res (constant i : in string);
  attribute foreign of command_res : procedure is "vhpi:%(libraryName)s:vcsCarbonCommandRes:carboncommand";
end carbon;

package body carbon is
  procedure command (constant i : in string) is
  begin
    ASSERT false
      REPORT "foreign C subprogram carbon not called."
      SEVERITY ERROR;
  end command;
  procedure command_res (constant i : in string) is
  begin
    ASSERT false
      REPORT "foreign C subprogram carbon not called."
      SEVERITY ERROR;
  end command_res;
end carbon;

library IEEE;
use IEEE.std_logic_1164.all;

use work.carbon.all;
architecture carbon of %(modelName)s is
attribute foreign of carbon : architecture is "vhpi:%(libraryName)s:noelabf:%(cVHMFuncName)s:carbonmodel";
''' %  { 'modelName'          : self.modelName(),
         'cVHMFuncName'       : self.cVHMFuncName(),
         'libraryName'        : self.libraryName() }

  # Function to emit the start of the C file
  def emitCStart(self):
    code = '''\
#define VCS
#include "carbon/MVVhpiUtils.c"
'''
    return code

  # Function to emit the initialization of the time variable
  def emitTimeInit(self, variable):
    return '''\
  /* Compute the time from the VHPI time */
  {
    vhpiTimeT vhpiTime = 0;
    vhpi_get_time(&vhpiTime, NULL);
    time = (CarbonTime)vhpiTime;
  }'''    
  
  # Function to emit the start of the init C Model
  def emitInitCModelStart(self, evalFn, quitFn):
    code = '''\
static void startSimulation(vhpiCbDataT* cb_data);

void noelabf(vhpiHandleT moduleHndl)
{
}
void %(cVHMFuncName)s(vhpiHandleT moduleHndl)
{
  vhpiTimeT\ttime = 0;
  // Need to get into simulation before output drivers will work
  vhpiCbDataT\tstart_cb = {vhpiCbLastKnownDeltaCycle, (void *)startSimulation, NULL, &time, NULL};
  %(structureName)s*\tdata;
  const char*\tfullName;
  CarbonObjectID*\tmodel;

  fullName = vhpi_get_str(vhpiFullCaseNameP, moduleHndl);

  /* Allocate space to keep track of the sync data */
  data = (%(structureName)s*)malloc(sizeof(%(structureName)s));

  /* Create and init the Carbon model (handles wave dumping etc) */
  model = carbon_%(objectName)s_create(%(dbType)s, eCarbon_NoFlags);
  vhpiInit(&data->carbon, fullName, "%(modelName)s", model);
  data->moduleHndl = moduleHndl;
  data->carbon.cbRegistered = FALSE;

  /* pass this data to the callback */
  start_cb.user_data = (PLI_VOID *)data;

  /* Get into the simulation to register the signals */
  vhpi_register_cb(&start_cb, 0);
}

/* Init user defined task; creates communication/execution callbacks */
static void startSimulation(struct vhpiCbDataS  *cbDataP)
{
  char\tsuccess = TRUE;
  %(structureName)s*\tdata;
  vhpiHandleT\tmoduleHndl;
  vhpiTimeT\ttime = 0;
  vhpiCbDataT\tquit_cb = {vhpiCbEndOfSimulation, (void *)%(quitFn)s, NULL, &time, NULL};
  vhpiErrorInfoT\terror_info;

  /* Get the data from the initialization */
  data = (%(structureName)s*)cbDataP->user_data;
  /* Get the parent module */
  moduleHndl = data->moduleHndl;
  /* Set up the carbon model callback data */
  data->carbon.evalCallback = evalModel;
  data->carbon.cbData = (PLI_BYTE8*)data;
'''  % { 'cVHMFuncName'    : self.cVHMFuncName(),
         'structureName'   : self.emitStructureName(),
         'modelName'       : self.modelName(),
         'dbType'          : self.emitDBType(),
         'objectName'      : self.objectName(),
         'evalFn'          : evalFn,
         'quitFn'          : quitFn }
    return code

  # Function to emit the makefile type
  def emitMakefileType(self):
    return 'vcsvhpi'

  # Returns the shared library name for the Carbon Model
  # (no .so for VCS)
  def libraryName(self):
    return 'carbon_' + self.modelName()

  def emitExtraMakeVariables(self):
    return '''\
SIM_HOME=$(VCS_HOME)

lib:\tlib$(MODEL_NAME).so

lib$(MODEL_NAME).so:\t$(MODEL_NAME).so
\tcp $(MODEL_NAME).so lib$(MODEL_NAME).so
'''

  # Function to emit the type of a leaf net
  def emitLeafType(self, node):
    return ""

  # Function to emit the type of composite
  def emitCompositeType(self, node, top, skipArray) :
    return ""

  # Function to emit libraries to be included
  def emitLibraryIncludes(self, node) :
    return ""

