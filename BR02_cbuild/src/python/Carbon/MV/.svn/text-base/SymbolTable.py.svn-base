import Carbon.DB

# The following set of classes is a mirror hierarchy of Carbon DB nodes.
# The purpose is to provide better iteration and allow us to add additional
# data to each leaf/branch node

# Base class for nodes in the symbol table. The nodes can
# be a branch or leaf
class Node:
  def __init__(self, dbNode, parent):
    self.__mDBNode = dbNode
    self.__mData = None
    self.__mParent = parent

  # For access to the Carbon specific information
  def getDBNode(self):
    return self.__mDBNode

  # Get the parent for this node
  def getParent(self):
    return self.__mParent

  # Override in the derived class to return true as appropriate
  def isBranch(self):
    return False

  # Override in the derived class to return true as appropriate
  def isLeaf(self):
    return False

  # Compare function for stability of the table
  def __cmp__(self, other):
    if self.isLeaf() and other.isLeaf():
      return cmp(self.leafName(), other.leafName())
    elif self.isBranch() and other.isBranch():
      return cmp(self.leafName(), other.leafName())
    elif self.isLeaf():
      # Leafs before branches
      return -1
    else:
      # Branches after leafs
      return True

  # Redirect some of the more common functions. The __getattr method is
  # not used to protect against mistakes
  def isPrimaryInput(self):
    return self.getDBNode().isPrimaryInput()
  def isPrimaryOutput(self):
    return self.getDBNode().isPrimaryOutput()
  def isPrimaryBidi(self):
    return self.getDBNode().isPrimaryBidi()
  def leafName(self):
    return self.getDBNode().leafName()
  def fullName(self):
    return self.getDBNode().fullName()
  def declarationType(self):
    return self.getDBNode().declarationType()
  def intrinsicType(self):
    return self.getDBNode().intrinsicType()
  def typeLibraryName(self):
     return self.getDBNode().typeLibraryName()
  def typePackageName(self):
     return self.getDBNode().typePackageName()
  def getMsb(self):
    return self.getDBNode().getMsb()
  def getLsb(self):
    return self.getDBNode().getLsb()
  def isVector(self):
    return self.getDBNode().isVector()
  def isScalar(self):
    return self.getDBNode().isScalar()
  def getWidth(self):
    return self.getDBNode().getWidth()
  def sourceLanguage(self):
    return self.getDBNode().sourceLanguage()
  def getPullMode(self):
    return self.getDBNode().getPullMode()
  def canBeCarbonNet(self):
    return self.getDBNode().canBeCarbonNet()
  def isContainedByComposite(self):
    return self.getDBNode().isContainedByComposite()
  def isArray(self):
    return self.getDBNode().isArray()
  def isStruct(self):
    return self.getDBNode().isStruct()
  def isEnum(self):
    return self.getDBNode().isEnum()
  def getArrayDims(self):
    return self.getDBNode().getArrayDims()
  def getArrayNumDeclaredDims(self):
    return self.getDBNode().getArrayNumDeclaredDims()
  def getArrayElement(self, indices, dims):
    return self.getDBNode().getArrayElement(indices, dims)
  def getArrayLeftBound(self):
    return self.getDBNode().getArrayLeftBound()
  def getArrayRightBound(self):
    return self.getDBNode().getArrayRightBound()
  def getArrayDimLeftBound(self, dim):
    return self.getDBNode().getArrayDimLeftBound(dim)
  def getArrayDimRightBound(self, dim):
    return self.getDBNode().getArrayDimRightBound(dim)
  def getRangeConstraintRightBound(self):
    return self.getDBNode().getRangeConstraintRightBound()
  def getRangeConstraintLeftBound(self):
    return self.getDBNode().getRangeConstraintLeftBound()
  def isRangeRequiredInDeclaration(self):
    return self.getDBNode().isRangeRequiredInDeclaration()
  def getNumberElemInEnum(self):
    return self.getDBNode().getNumberElemInEnum()
  def getEnumElem(self, index):
    return self.getDBNode().getEnumElem(index)
    


# A leaf node in the symbol table
class Leaf(Node):
  def __init__(self, dbNode, parent):
    Node.__init__(self, dbNode, parent);

  def isLeaf(self):
    return True


  # Test if this is a primary net (if parent is a primary branch)
  def isPrimary(self):
    return self.getParent().isPrimary()

  # Test if this is an output (includes internal nets as outputs)
  def isOutput(self):
    if self.isPrimaryPort():
      return self.isPrimaryOutput()
    else:
      # If it isn't depositable, we can't write it, so treat it as an output
      return not self.isDepositable()

  # Test if this is a bid (includes internal nets as bids)
  def isBidi(self):
    if self.isPrimaryPort():
      return self.isPrimaryBidi()
    else:
      # It is depositable, sync both ways
      return self.isDepositable()

  # Test if this is an input (internals can't be inputs)
  def isInput(self):
    return self.isPrimaryInput()

# A branch node in the symbol table. This can have sub branches and
# lears
class Branch(Node):
  def __init__(self, dbNode, parent):
    Node.__init__(self, dbNode, parent);
    self.__mSubNodes = {}

  def isBranch(self):
    return True

  def isPrimary(self):
    return self.getDBNode().getParent() == None

  # Returns all the sub nodes for iteration
  def subNodes(self):
    return self.__mSubNodes.values()

  # Add a new branch or leaf node as a child of this node
  def addSubNode(self, node):
    name = node.leafName()
    if self.__mSubNodes.has_key(name):
      raise "Inserting a duplicate node with name `%s'" % name
    self.__mSubNodes[name] = node

  # Find a given child by node
  def findSubNode(self, dbNode):
    name = dbNode.leafName()
    return self.__mSubNodes.get(name, None)

  # Find a given child by name
  def findSubNodeByName(self, name):
    return self.__mSubNodes.get(name, None)

# The top node of the symbol table. This is essentially a branch node but
# it has an invalid dbNode
class Top(Branch):
  def getDBNode(self):
    raise "Top node doesn't have a DBNode"

# The symbol table class to support population and iteration
class SymbolTable:
  def __init__(self):
    self.__mRoot = Top(None, None)

  # Override this function to create a derived leaf
  def newLeaf(self, dbNode, parent):
    return Leaf(dbNode, parent)

  # Override this function to create a derived branch
  def newBranch(self, dbNode, parent):
    return Branch(dbNode, parent)

  # Iterate over the top nodes
  def topNodes(self):
    return self.__mRoot.subNodes()

  # Get the root
  def root(self):
    return self.__mRoot

  def addDBNode(self, dbNode, findOnly = False):
    # Walk the list of parents and record the nodes
    parentList = []
    parentNode = dbNode.getParent()
    while parentNode:
      parentList.insert(0, parentNode)
      parentNode = parentNode.getParent()

    # Walk the list of nodes in order and find/add the hierarchy
    curNode = self.__mRoot
    for parentNode in parentList:
      # Find the node if it exists
      nextNode = curNode.findSubNode(parentNode)
      if nextNode:
        # It exists, move on
        curNode = nextNode
      else:
        if findOnly:
          return None
        # It doesn't exist, add it
        node = self.newBranch(parentNode, curNode)
        curNode.addSubNode(node)
        curNode = node

    # Add the db node if it doesn't exist
    node = curNode.findSubNode(dbNode)
    if not node:
      if findOnly:
        return None
      if (dbNode.getParent() == None):
        # This must be the top level node, not a leaf
        node = self.newBranch(dbNode)
      else:
        node = self.newLeaf(dbNode, curNode)
        curNode.addSubNode(node)
    return node

# Walker class to visit all the nodes in the symbol table
class Walker:
  def __init__(self, symbolTable):
    self.__mSymbolTable = symbolTable

  # Access function to get the symbol table
  def getSymbolTable(self):
    return self.__mSymbolTable

  # Override this function to visit the node before visiting any children
  # It returns whether the walk should continue or not
  def visitBranchPre(self, node):
    return True

  # Override this function to visit the node after visiting any children
  # It returns whether the walk should continue or not
  def visitBranchPost(self, node):
    return True

  # Override this function to visit all the leaf nodes
  # It returns whether the walk should continue or not
  def visitLeaf(self, node):
    return True

  # Override this function to filter a node
  def filter(self, node):
    return False

  # Function to recursively walk the symbol table from the given start point
  def walkFrom(self, node):
    # Test if we are filtering. We return to continue walking even if
    # we filter because this is not a stop condition.
    if self.filter(node):
      return True
    
    # We are visiting, continue
    if node.isBranch():
      # Visit a branch node
      if not self.visitBranchPre(node):
        return False
      subNodes = node.subNodes()
      subNodes.sort()
      for subNode in subNodes:
        if not self.walkFrom(subNode):
          return False
      if not self.visitBranchPost(node):
        return False
    else:
      # Visit a leaf node
      if not self.visitLeaf(node):
        return False
    return True

  # Function to do a one level walk of the sub nodes for a node
  # This is only legal on a branch node
  def subNodes(self, node):
    for subNode in node.subNodes():
      if not self.filter(subNode):
        yield subNode

  # Function to start the walk from the top of the symbol table
  def walk(self):
    for node in self.__mSymbolTable.topNodes():
      self.walkFrom(node)
