#******************************************************************************
# Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#******************************************************************************

# Useful libraries
import sys
import time
import re
import string

# Symbol table files
import Carbon.MV.ShadowHierarchy

# Code to parse/write XML files
import xml.sax.handler
import xml.dom.minidom

# This class describes an attribute value in an XML file
class Attribute:
  def __init__(self, default = None, required = True):
    self.__mDefault = default
    self.__mRequired = required

  def getDefault(self): return self.__mDefault
  def getRequired(self): return self.__mRequired

# This class describes an element in an XML file
class Element:
  def __init__(self, subElements):
    self.__mSubElements = subElements
    self.__mAttributes = {}

  def addSubElement(self, name):
    self.__mSubElements.append(name)

  def addAttribute(self, name, attr):
    self.__mAttributes[name] = attr

  def getSubElements(self):
    return self.__mSubElements

  def getAttributes(self):
    return self.__mAttributes

# This class describes a schema for an XML file
class XMLSchema:
  def __init__(self, elements):
    # Create a top level element call ConfigStart
    self.__mElements = {}
    self.__mElements["ConfigStart"] = Element(elements)

  # Add an element
  def addElement(self, name, subElements):
    self.__mElements[name] = Element(subElements)

  # Add an element attribute
  def addAttribute(self, name, attrName, attribute):
    element = self.__mElements[name]
    element.addAttribute(attrName, attribute)

  # get the sub element names for a given element
  def getSubElements(self, elementName):
    return self.__mElements[elementName].getSubElements()

  # Get the attributes for an element
  def getAttributes(self, elementName):
    return self.__mElements[elementName].getAttributes()

# General class to create an error message with a locator
class SchemaError(xml.sax.SAXException):
  def __init__(self, locator, msg):
    fileName = locator.getSystemId()
    lineNumber = locator.getLineNumber()
    columnNumber = locator.getColumnNumber()
    locMsg = "%s:%d:%d: Error: %s" % (fileName, lineNumber, columnNumber, msg)
    xml.sax.SAXException.__init__(self, locMsg, None)

# Class to raise and invalid element exception
class InvalidElementError(SchemaError):
  def __init__(self, loc, name, expected):
    msg = "Unexpected Element '%s' expecting one of '%s'" % (name, expected)
    SchemaError.__init__(self, loc, msg)

# Class to raise an invalid attribute exception
class InvalidAttributeError(SchemaError):
  def __init__(self, loc, elemName, attrName):
    msg = "Unexpected Attribute '%s' for element '%s'" % (attrName, elemName)
    SchemaError.__init__(self, loc, msg)

# Class to raise an missing required attribute exception
class MissingAttributeError(SchemaError):
  def __init__(self, loc, elemName, attrName):
    msg = "Missing required Attribute '%s' for element '%s'" % (attrName, elemName)
    SchemaError.__init__(self, loc, msg)

# This class handles reading XML configuration files. It takes
# a schema and uses the xml.sax parser to first validate the
# schema and then call a derived class to process the content.
class ContentHandler(xml.sax.ContentHandler):
  def __init__(self, schema):
    # Init the config state
    self.__mElementStack = [ "ConfigStart" ]
    self.__mSchema = schema

  def setSchema(self, schema):
    self.__mSchema = schema

  # Function to get the parent element
  def getParentElement(self):
    length = len(self.__mElementStack)
    return self.__mElementStack[length-1]

  # Gets a new element when parsing the file
  def startElement(self, name, attrs):
    # Handle elements based on the current state. The current state is
    # the parent element for this element.
    parentElement = self.getParentElement()
    subElements = self.__mSchema.getSubElements(parentElement)

    # Make sure this element is in the parent elements list
    if name in subElements:
      # It is valid sub element, validate the attributes, first make sure
      # They are valid attributes
      attributes = self.__mSchema.getAttributes(name)
      for attrName in attrs.keys():
        if not attributes.has_key(attrName):
          raise InvalidAttributeError(self._locator, name, attrName)
      
      # Make sure all required attributes are present
      for (attrName, attribute) in attributes.items():
        if attribute.getRequired() and not attrs.has_key(attrName):
          raise MissingAttributeError(self._locator, name, attrName)

      # Everything is valid if we get here, call the handler
      self.handleElementStart(name, attrs)
    else:
      # Invalid sub element
      raise InvalidElementError(self._locator, name, subElements)

    # Set the current parent element
    self.__mElementStack.append(name)

  # Ends an element
  def endElement(self, name):
    # Remove the current parse state
    self.__mElementStack.pop()

    # Handle the end of a scope
    self.handleElementEnd(name)
    return

  # Characters in an element
  def characters(self, content):
    # Call the derived class to handle this
    self.handleElementContent(self.getParentElement(), content)

# Class to raise an invalid connection exception
class InvalidConnectionError(SchemaError):
  def __init__(self, loc, netName, declType, delay):
    msg = "Net '%s' not found, declaration '%s', delay '%s'" % \
          (netName, declType, delay)
    SchemaError.__init__(self, loc, msg)

# Class to raise an invalid scope exception
class InvalidScopeError(SchemaError):
  def __init__(self, loc, instName):
    msg = "Scope '%s' not found" % instName;
    SchemaError.__init__(self, loc, msg)

# Class to raise an invalid scope exception
class InvalidPortError(SchemaError):
  def __init__(self, loc, netName):
    msg = "Port '%s' not found" % netName;
    SchemaError.__init__(self, loc, msg)

# Class to raise a simulator conflict exception
class SimulatorConflict(SchemaError):
  def __init__(self, loc, origSim, newSim):
    msg = "Conflicting simulators found, original specification '%s', configuration file value '%s'." % (origSim, newSim)
    SchemaError.__init__(self, loc, msg)

# Class to apply config information to the symbol table and gather
# general information like the target simulator and language.
class Config(ContentHandler):
  def __init__(self, file, simulator, hierarchy):
    # Create the schema for the content handler
    schema = XMLSchema(["mvconfig"])
    schema.addElement("mvconfig", [ "target", "primaryports", "scope", "input_flow_mode" ])
    schema.addAttribute("mvconfig", "version", Attribute("2.0", False))
    schema.addAttribute("mvconfig", "startsWith", Attribute("All", False))
    schema.addElement("target", [])
    schema.addAttribute("target", "simulator", Attribute())
    schema.addAttribute("target", "language", Attribute(required = False))
    schema.addElement("primaryports", [ "port" ])
    schema.addElement("port", [])
    schema.addAttribute("port", "Name", Attribute())
    schema.addAttribute("port", "direction", Attribute())
    schema.addAttribute("port", "type", Attribute(required = False))
    schema.addAttribute("port", "delay", Attribute(required = False))
    schema.addElement("input_flow_mode", [])
    schema.addElement("scope", [ "scope", "preamble", "connection" ])
    schema.addAttribute("scope", "Name", Attribute())
    schema.addElement("preamble", [])
    schema.addElement("connection", [])
    schema.addAttribute("connection", "Name", Attribute())
    schema.addAttribute("connection", "type", Attribute(required = False))
    schema.addAttribute("connection", "delay", Attribute(required = False))
    schema.addAttribute("connection", "dirObservable", Attribute(required = False))
    schema.addAttribute("connection", "dirDepositable", Attribute(required = False))

    schema10 = XMLSchema(["mvconfig"])
    schema10.addElement("mvconfig", [ "target", "scope", "input_flow_mode" ])
    schema10.addAttribute("mvconfig", "version", Attribute("1.0", False))
    schema10.addElement("target", [])
    schema10.addAttribute("target", "simulator", Attribute())
    schema10.addAttribute("target", "language", Attribute(required = False))
    schema10.addElement("input_flow_mode", [])
    schema10.addElement("scope", [ "scope", "preamble", "connection" ])
    schema10.addAttribute("scope", "name", Attribute())
    schema10.addElement("preamble", [])
    schema10.addElement("connection", [])
    schema10.addAttribute("connection", "name", Attribute())
    schema10.addAttribute("connection", "type", Attribute(required = False))
    schema10.addAttribute("connection", "delay", Attribute(required = False))

    # Init the content handler
    ContentHandler.__init__(self, schema)

    # Init the general config state
    self.__mVersion = '2.0'
    self.__mLanguage = ''
    self.__mSimulator = simulator
    self.__mInputFlow = True
    self.__mConfigFile = file
    self.__mHierarchy = hierarchy
    self.__mInputFlowMode = False
    self.__mSchema10 = schema10
    self.__mStartsWith = "All"

    # get the top node in the shadow hierarchy to work on
    self.__mScopeStacks = [ hierarchy.root() ]

  # Read the configuration file
  def read(self):
    # Make sure we have a valid file
    try:
      file = open(self.__mConfigFile, "r")
    except IOError, (errno, strerror):
      print "Error: could not read config file '%s', reason: '%s'" % \
            (self.__mConfigFile, strerror)
      return 1
    except:
      print "Error: could not read config file '%s', unexpected error: '%s'" % \
            (self.__mConfigFile, sys.exc_info()[0])
      return 1

    # Parse the file
    try:
      xml.sax.parse(file, self)
    except xml.sax.SAXException, msg:
      print "%s" %msg
      return 2
    return 0

  # Function to get the current scope from the stack
  def curScope(self):
    stackLength = len(self.__mScopeStacks)
    return self.__mScopeStacks[stackLength - 1]

  # Function to handle a new element
  def handleElementStart(self, name, attrs):
    if name == "mvconfig":
      # Get the version if it is there
      self.__mVersion = attrs.get("version", "2.0")
      self.__mStartsWith = attrs.get("startsWith", "All")

      if self.__mVersion == "1.0":
        ContentHandler.setSchema(self, self.__mSchema10)
      
    elif name == "target":
      # get the required simulator, note we do conflict resolution
      simulator = attrs["simulator"]
      if not self.__mSimulator:
        # No conflict resolution needed, it was not pre-specified
        self.__mSimulator = simulator
      else:
        if simulator != self.__mSimulator:
          raise SimulatorConflict(self._locator, self.__mSimulator, simulator)

      # and optional language
      if attrs.has_key("language"):
        self.__mLanguage = attrs.getValue("language")
        
    elif name == "scope":
      # Check if we can find this sub scope, if not, we have a problem
      if self.__mVersion == "1.0":
        instName = attrs.getValue("name")
      else:
        instName = attrs.getValue("Name")

      subScope = self.curScope().findSubNodeByName(instName)
      if subScope:
        # Push this scope on the stack
        self.__mScopeStacks.append(subScope)
        subScope.putInConfig();
      else:
        raise InvalidScopeError(self._locator, instName)

    elif name == "connection":
      # get the required name and optional delay and type
      if self.__mVersion == "1.0":
        netName = attrs.getValue("name")
      else:
        netName = attrs.getValue("Name")

      declType = attrs.get("type", "")
      delay = attrs.get("delay", None)
      
      # Find this net node
      node = self.curScope().findSubNodeByName(netName)
      if node:
        # Add the information to the node
        node.putTypeInfo(declType)
        if delay:
          node.putDelay(delay)
        node.putInConfig();
      else:
        raise InvalidConnectionError(self._locator, netName, declType, delay)

    elif name == "port":
      if self.__mVersion == "1.0":
        netName = attrs.getValue("name")
      else:
        netName = attrs.getValue("Name")

      # Find this port node
      node = self.__mHierarchy.findPrimaryPortByName(netName)
      if node:
        delay = attrs.get("delay", None)
        if delay:
          node.putDelay(delay)
      else:
        raise InvalidPortError(self._locator, netName)

    elif name == "input_flow_mode":
      self.__mInputFlowMode = True

    elif name != "preamble" and name != "primaryports" and name != "port":
      print "type is %s" % name
      raise "Unhandled new element type '%s'" % name

  # Function to handle the end of an element (only scopes matter)
  def handleElementEnd(self, name):
    if name == "scope":
      self.__mScopeStacks.pop()

  # Handle the preamble content
  def handleElementContent(self, name, content):
    if name == "preamble" and len(content) > 0:
      # Add the preamble text to the shadow hierarchy for this scope
      self.curScope().appendToPreamble(content)

  # Function to get the simulator
  def getSimulator(self):
    return self.__mSimulator

  # Function to get the language
  def getLanguage(self):
    return self.__mLanguage

  # Returns true if the model should use input flow semantics
  def getInputFlowMode(self):
    return self.__mInputFlowMode

  # Function to get the language
  def getVersion(self):
    return self.__mVersion

  def isStartsWithNone(self):
    # We'll be nice and be caseless; also "all" is explicit - anything else assumes "none"
    return (string.lower(self.__mStartsWith) != "all")

# Class to write a config file from a given shadow hierarchy
class ConfigWriter(xml.dom.minidom.Document, Carbon.MV.ShadowHierarchy.Walker):
  def __init__(self, config, symbolTable, helper, cmdLine, filterMode):
    # Store the helper to get the declaration type
    self.__mHelper = helper

    # Start the document
    xml.dom.minidom.Document.__init__(self)
    comment1 = self.createComment(" Generated by MV on " + \
               time.strftime("%a, %d %b %Y %H:%M:%S %Z", time.localtime()) + " ")
    commentstring = " Commandline: " + re.sub("--", "__", cmdLine) + " "
    comment2 = self.createComment(commentstring)
    topElement = self.createElement("mvconfig")
    topElement.setAttribute("version", "2.0")
    # These start with no signals.  Otherwise, start will all signals
    # FILTER_ALL is on if -P is set.
    # FILTER_DIRCONFIG_IN is on if a config file was read with startsWith=None
    if filterMode == "FILTER_ALL" or filterMode == "FILTER_DIRCONFIG_IN":
      topElement.setAttribute("startsWith", "None")

    self.__mElementStack = []
    self.__mElementStack.append(topElement)
    self.appendChild(comment1)
    self.appendChild(comment2)
    self.appendChild(topElement)

    # Add the target simulator and language
    targetElement = self.createElement("target")
    targetElement.setAttribute("simulator", config.getSimulator())
    targetElement.setAttribute("language", config.getLanguage())
    topElement.appendChild(targetElement)

    # Add the primary ports
    primPort = self.createElement("primaryports")
    # These are in the symbol table in the original order
    for portnode in symbolTable.getPrimaryPorts():
      port = self.createElement("port");
      if portnode.isPrimaryInput():
        port.setAttribute("direction", "input")
      elif portnode.isPrimaryOutput():
        port.setAttribute("direction", "output")
      elif portnode.isPrimaryBidi():
        port.setAttribute("direction", "bidirectional")

      netName = portnode.leafName()
      declType = self.__mHelper.emitDeclarationType(portnode)
    
      port.setAttribute("Name", netName)
      port.setAttribute("type", declType)
      delay = portnode.getDelay();
      if delay != 0:
        port.setAttribute("delay", str(delay))
      primPort.appendChild(port)

    topElement.appendChild(primPort)

    # Start the walker to add the config data
    Carbon.MV.ShadowHierarchy.Walker.__init__(self, symbolTable, filterMode)
    self.walk()

  # Helper function to get the current element
  def currentElement(self):
    length = len(self.__mElementStack)
    return self.__mElementStack[length - 1]

  # Start a new scope
  def visitBranchPre(self, node):
    # Create the scope element
    instName = node.leafName()
    scope = self.createElement("scope")
    scope.setAttribute("Name", instName)

    # Add the child and set it as the current element
    self.currentElement().appendChild(scope)
    self.__mElementStack.append(scope)

    # Create the preamble element if we need one
    preamble = self.createElement("preamble");
    text = node.getPreamble()
    if len(text) > 0:
      data = self.createTextNode(text)
    else:
      data = self.createTextNode("")
    preamble.appendChild(data);

    # Add it to the current scope
    scope.appendChild(preamble)
    return True

  # End a scope
  def visitBranchPost(self, node):
    # Pop the scope stack
    self.__mElementStack.pop()
    return True

  # Add a preamble element
  # Add a connection
  def visitLeaf(self, node):
    # Get the leaf data

    # Primary ports are a spcial case, handled at the top level, above
    if node.isPrimaryPort():
      return True

    netName = node.leafName()
    declType = self.__mHelper.emitDeclarationType(node)
    
    # Create the connection element
    connection = self.createElement("connection")
    connection.setAttribute("Name", netName)
    connection.setAttribute("type", declType)

    delay = node.getDelay();
    if delay != 0:
      connection.setAttribute("delay", delay)

# Leave these out for now.  They could be confusing to the customer.
#    if node.isObservable():
#      connection.setAttribute("dirObservable", "true")
#    if node.isDepositable():
#      connection.setAttribute("dirDepositable", "true")

    # Add it to the current scope
    self.currentElement().appendChild(connection)
    return True

  # Write out the XML file
  def writeFile(self, fileName):
    file = open(fileName, "w")
    self.writexml(file, indent="", addindent=" ", newl="\n")
    file.close()


# Handle the XML that Carbon Model Studio generates
#<ModelValidation version="1.0">
#  <Nets>
#    <Net locator="a.x.b" observeSignal="true" depositSignal="false"/>
#    <Net locator="a.x.b" observeSignal="true" depositSignal="false"/>
#    <Net locator="a.x.b" observeSignal="true" depositSignal="false"/>
#    <Net locator="a.x.b" observeSignal="true" depositSignal="false"/>
#    <Net locator="a.x.b" observeSignal="true" depositSignal="false"/>
#  </Nets>
# 
#</ModelValidation>

class Directive (xml.sax.ContentHandler):
  def __init__(self, file, DB, hierarchy):
    self.__mConfigFile = file
    self.__mDB = DB                     # The Carbon database
    self.__mHierarchy = hierarchy       # Shadow hierarchy
    self.__mStartsWith = "None"


  def startElement(self, name, attributes):
    if name == "Net":
      if attributes.has_key("locator"):
        fullNodeName = attributes["locator"]
        if attributes.get("observeSignal") == "true":
          isObservable = True
        if attributes.get("depositSignal") == "true":
          isDepositable = True

        # The node name may contain wildcards, or could be
        # module-based instead of instance-based, so calling
        # findNode() won't necessarily work.  Instead, iterate over
        # all matching signals.
        nodesFound = False
        for DBnode in self.__mDB.iterMatching(str(fullNodeName)):
          node = self.__mHierarchy.addDBNode(DBnode, True) # find-only
          # This could happen if the signal is in the DB, but not marked as
          #  observable or depositable.
          if not node:
            print "Warning: net \"%s\" found in database, but not in MV's database" %fullNodeName

          else:
            node.putInConfig();
            nodesFound = True;

        if nodesFound == False:
          print "Warning: no nets matching \"%s\" found in database" %fullNodeName
            
      else:
        print "Warning: \"net\" element is missing \"locator\" attribute"

    elif name == "Nets":
      self.__mStartsWith = attributes.get("startsWith", "None")

  def isStartsWithNone(self):
    # We'll be nice and be caseless; also "all" is explicit - anything else assumes "none"
    return (string.lower(self.__mStartsWith) != "all")

  def read(self):
    # Make sure we have a valid file
    try:
      file = open(self.__mConfigFile, "r")
    except IOError, (errno, strerror):
      print "Error: could not read config file '%s', reason: '%s'" % \
            (self.__mConfigFile, strerror)
      return 1
    except:
      print "Error: could not read config file '%s', unexpected error: '%s'" % \
            (self.__mConfigFile, sys.exc_info()[0])
      return 1

    # Parse the file
    try:
      xml.sax.parse(file, self)
    except xml.sax.SAXException, msg:
      print "%s" %msg
      return 2
    return 0


class ConfigDirWriter(xml.dom.minidom.Document, Carbon.MV.ShadowHierarchy.Walker):
  def __init__(self, config, symbolTable, helper, cmdLine, filterMode):
    # Store the helper to get the declaration type
    self.__mHelper = helper

    # Start the document
    xml.dom.minidom.Document.__init__(self)
    comment1 = self.createComment(" Generated by MV on " + \
               time.strftime("%a, %d %b %Y %H:%M:%S %Z", time.localtime()) + " ")
    commentstring = " Commandline: " + re.sub("--", "__", cmdLine) + " "
    comment2 = self.createComment(commentstring)
    topElement = self.createElement("ModelValidation")
    topElement.setAttribute("version", "1.0")

    self.appendChild(comment1)
    self.appendChild(comment2)
    self.appendChild(topElement)

    # Add the target simulator and language
    self.__mNetsElement = self.createElement("Nets")
    self.__mNetsElement.setAttribute("startsWith", "None")
    topElement.appendChild(self.__mNetsElement)

    # Start the walker to add the config data
    Carbon.MV.ShadowHierarchy.Walker.__init__(self, symbolTable, filterMode)
    self.walk()

  # Add a connection
  def visitLeaf(self, node):
    # Get the leaf data

    # Primary ports are a spcial case, handled at the top level, above
    if node.isPrimaryPort():
      return True

    # Create the connection element
    net = self.createElement("Net")
    net.setAttribute("locator", node.fullName())

    # Add it to the current scope
    self.__mNetsElement.appendChild(net)
    return True

  # Write out the XML file
  def writeFile(self, fileName):
    file = open(fileName, "w")
    self.writexml(file, indent="", addindent=" ", newl="\n")
    file.close()

