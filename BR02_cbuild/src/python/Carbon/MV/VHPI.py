#******************************************************************************
# Copyright (c) 2008-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

import Carbon.MV.VHDL

# Implement the helper class to emit the HDL and C-Model for */VPI
# This should be derived and overloaded for target simulator specific
# changes.
class Helper(Carbon.MV.VHDL.Helper):

  # Constructor
  def __init__(self, modelName, objectName, hierarchy, dbType, preserveNames):
    Carbon.MV.VHDL.Helper.__init__(self, modelName, objectName, hierarchy,
                                   dbType, preserveNames)

  # Function to return the c init function name; must be overloaded
  def simInit(self):
      return 'vhpiInit'

  # Function to emit the start of the C file
  def emitCStart(self):
    code = '#include "carbon/MVVhpiUtils.c"\n'
    return code

  # Function to emit the structure declaration
  def emitCStructureDeclaration(self, direction):
    return "Vhpi%(portDirection)s" % { "portDirection" : direction }

  # Function to emit the C structure model field
  def emitCStructureModel(self):
    return '  VhpiCarbonModel\tcarbon;\n  vhpiHandleT\tmoduleHndl;\n'

  # Function to return the port setup function
  def portSetupFn(self, node):
    code = 'vhpiSetup'

    # The input or bids can have a Clock/Data field, so outputs don't
    if not node.isOutput():
      if node.isPrimaryClock():
        code += 'Clock'
      else:
        code += 'Data'

    # Add the direction part of the function name
    code += node.portDirection()
    return code

  # Function to return the port cleanup function
  def portCleanupFn(self, node):
    code = 'vhpi'
    return code


  # Seperator for VHDL hierarchy, overloaded from Helper
  def getSepSim(self, node):
    return "."

  # return the code to hookup a single port
  def emitCPortHookup(self, node, simHierPathName, carbonHierPathName):
    # Create a string for the MTI and Carbon hierarchical names. They
    # are different because they use different separators
    simHierName = simHierPathName + node.leafName()
    carbonHierName = carbonHierPathName + node.leafName()

    # Only primary output or bid ports are driven. This is not a valid
    # argument for inputs so set it to blank.
    if node.isPrimaryBidi():
      drive = ", TRUE"
    elif node.isOutput() or node.isBidi():
      drive = ", FALSE"
    else:
      drive = ""

    code = ""

    # Wrap a "for" loop around memories
    if node.isMemory():
      code += '''\
  {
'''
      index = ""
      arr_index = ""
      index_values = ""
      numDims = node.getArrayDims()

      # carbon_i is the index of vector net, which
      # all the model nets are mapped to.
      code += '''\
    int carbon_i = 0;
'''
  
      # We are looping over all the dimensions,
      # of the memory net, except the inner one
      for k in range(0, numDims-1):
        index += "i"
        arr_index += "[%d]"
        index_values += ''', %(index)s''' % {"index" : index}
        rangeDim = node.getDBNode().getMemIndices(k)
        loop_min = min(rangeDim)
        loop_max = max(rangeDim)

        code += '''\
    int %(loop_index)s; 
''' % { "loop_index"          : (index)}
        
        # We emit the for loop for all the dimensions
        code += '''\
    for (%(loop_index)s = %(loop_min)d; %(loop_index)s <= %(loop_max)d;  %(loop_index)s++) {
''' % { "loop_index"          : (index),
        "loop_min"            : (loop_min),
        "loop_max"            : (loop_max)}

        if k == numDims-2:
          code +='''\
         char simName[1024];
         char carbonName[1024];

         sprintf (simName, "%(simHierName)s%(arr_idx)s"%(index_values)s);
         sprintf (carbonName, "%(carbonHierName)s%(arr_idx)s"%(index_values)s);
         success &= %(portSetupFn)s(&data->carbon, moduleHndl, simName, "%(modelName)s", carbonName, %(netWidth)d%(drive)s, %(delay)d, &data->%(fieldName)s[carbon_i]);
''' % { "simHierName"    : simHierName,
        "carbonHierName" : carbonHierName,
        "portSetupFn"    : self.portSetupFn(node),
        "modelName"      : self.modelName(),
        "netWidth"       : node.getDBNode().getMemWidth(),
        'fieldName'      : self.cIdentifier(node),
        'drive'          : drive,
        'delay'          : node.getDelay(),
        "arr_idx"        : arr_index,
        "index_values"   : index_values}


          code +='''\
         carbon_i = carbon_i + 1;
'''

      # This loop emits end of for loops for extenal dimensions
      # of the memory net
      for m in range(0, numDims-1):
        code += '''\
    }
'''  
      code += '''\
  }
'''
    else:
      # Emit the code for the hookup
      code += '  success &= %(portSetupFn)s(&data->carbon, moduleHndl, "%(simHierName)s", "%(modelName)s", "%(carbonHierName)s", %(netWidth)d%(drive)s, %(delay)d, &data->%(fieldName)s);\n' % \
            { "portSetupFn"    : self.portSetupFn(node),
              "simHierName"    : simHierName,
              "modelName"      : self.modelName(),
              "carbonHierName" : carbonHierName,
              "netWidth"       : node.getWidth(),
              'fieldName'      : self.cIdentifier(node),
              'drive'          : drive,
              'delay'          : node.getDelay() }

    return code
    



    
  # Return the code to update the data ports
  def emitDataPortUpdate(self, node):
    code = '    vhpiUpdateData%(portDirection)s(&data->%(fieldName)s);\n' % \
           { "portDirection" : node.portDirection(),
             "fieldName"     : self.cIdentifier(node) }
    return code

  # Return the code to cleanup all ports
  def emitCPortDisconnect(self, node):
    # depth is zero for non-memories
    numNets = 1
    if node.isMemory():
      numNets = node.getDBNode().getMemNumCarbonNets()
      
    code = ""
    # emit the port hookups "depth" times for memories, once for everything else
    if node.isMemory():
      index = "[i]"
      indent = "    "
      code +='''\
  {
    int i;
    for (i = 0; i < %d; i++) {
''' % (numNets - 1)

    else:
      index = ""
      indent = ""

    code += '  %(indent)s%(portCleanupFn)sCleanup%(portDirection)s(&data->%(fieldName)s%(index)s);\n' % \
            { "indent"        : indent,
              "portCleanupFn" : self.portCleanupFn(node),
              "portDirection" : node.portDirection(),
              "fieldName"     : self.cIdentifier(node),
              "index"         : index }

    if node.isMemory():
      code += '''\
    }
  }
'''

    return code

  # Function to emit the function prototype for a callback cmodel
  def emitCallbackFn(self, fnName):
    return 'void %s(vhpiCbDataT* cb_data)' % fnName

  # Returns the structure name for the ports
  def emitStructureName(self):
    return 'Vhpi' + self.modelName() + 'Data'

  # Function to emit the structure initialization (casting) code
  def emitStructureInit(self):
    return 'cb_data->user_data'

  # Function to emit the initialization of the time variable
  def emitTimeInit(self, variable):
    return '''\
  /* Compute the time from the VHPI time */
  {
    vhpiTimeT vhpiTime = { 0, 0 };
    vhpi_get_time(&vhpiTime, NULL);
    time = (((CarbonTime)(vhpiTime.high)) << 32);
    time |= ((CarbonTime)(vhpiTime.low));
  }'''    
  
  # Function to emit the return of the eval c-model
  def emitEvalCModelReturn(self):
    return '''\
  /* Test if $finish was called; if so, tell the target simulator */
  if (status == eCarbon_FINISH) {
    vhpi_control(vhpiFinish);
  }
    
  /* Clear the cbRegistered flag so it can be enabled again */
  data->carbon.cbRegistered = FALSE;
  return;'''

  # Function to emit the start of the init C Model
  def emitInitCModelStart(self, evalFn, quitFn):
    code = '''\
static void startSimulation(vhpiCbDataT* cb_data);

void %(cVHMFuncName)s(const struct vhpiCbDataS  *cbDataP)
{
  vhpiTimeT\ttime = {0, 0};
  // Need to get into simulation before output drivers will work
  vhpiCbDataT\tstart_cb = {vhpiCbLastKnownDeltaCycle, (void *)startSimulation, NULL, &time, (PLI_VOID*)cbDataP->obj};
  vhpiHandleT\tmoduleHndl;
  vhpiHandleT\tparentHndl;
  %(structureName)s*\tdata;
  const char*\tfullName;
  CarbonObjectID*\tmodel;

  moduleHndl = cbDataP->obj;	/* The parent handle comes with the initial calllback */
  parentHndl = vhpi_handle(vhpiUpperRegion,  moduleHndl); /* The parrent entity holds the foreign attribute */

  fullName = vhpi_get_str(vhpiFullCaseNameP, parentHndl);

  /* Allocate space to keep track of the sync data */
  data = (%(structureName)s*)malloc(sizeof(%(structureName)s));

  /* Create and init the Carbon model (handles wave dumping etc) */
  model = carbon_%(objectName)s_create(%(dbType)s, eCarbon_NoFlags);
  vhpiInit(&data->carbon, fullName, "%(modelName)s", model);
  data->moduleHndl = parentHndl;
  data->carbon.cbRegistered = FALSE;

  /* pass this data to the callback */
  start_cb.user_data = (PLI_VOID *)data;

  /* Get into the simulation to register the signals */
  vhpi_register_cb(&start_cb, 0);
}

/* Init user defined task; creates communication/execution callbacks */
static void startSimulation(struct vhpiCbDataS  *cbDataP)
{
  char\tsuccess = TRUE;
  %(structureName)s*\tdata;
  vhpiHandleT\tmoduleHndl;
  vhpiTimeT\ttime = {0, 0};
  vhpiCbDataT\tquit_cb = {vhpiCbEndOfSimulation, (void *)%(quitFn)s, NULL, &time, NULL};
  vhpiErrorInfoT\terror_info;

  /* Get the data from the initialization */
  data = (%(structureName)s*)cbDataP->user_data;
  /* Get the parent module */
  moduleHndl = data->moduleHndl;
  /* Set up the carbon model callback data */
  data->carbon.evalCallback = evalModel;
  data->carbon.cbData = (PLI_BYTE8*)data;
'''  % { 'cVHMFuncName'    : self.cVHMFuncName(),
         'structureName'   : self.emitStructureName(),
         'modelName'       : self.modelName(),
         'dbType'          : self.emitDBType(),
         'objectName'      : self.objectName(),
         'evalFn'          : evalFn,
         'quitFn'          : quitFn }
    return code

  # Function to emit the end of the init C-Model
  def emitInitCModelEnd(self, evalFn, quitFn):
    return '''\
  /* Set up a callback to exit */
  quit_cb.user_data = (void*)data;
  vhpi_register_cb(&quit_cb, 0);
  if (vhpi_check_error(&error_info)) {
    vhpi_printf(error_info.message);
  }
}
'''

  # Function to emit any extra make file variables
  def emitExtraMakeVariables(self):
    return ''

  # Function to emit the makefile type
  def emitMakefileType(self):
    return 'vhpi'

  # Function to return the free function for Modelsim
  def freeFn(self):
    return 'free'

  # There are no other Modelsim files
  def emitOtherFiles(self):
    return

  # Function to print a const char*
  def emitPrintString(self):
    # mti_PrintMessage() doesn't have the const qualifier for some reason
    return 'vhpi_printf'

  # Function to cause the simulator to exit
  def emitSimulatorFatalExit(self):
    return 'vhpi_control(vhpiStop)'


