#******************************************************************************
# Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

import Carbon.MV.Helper

# Implement the helper class to emit the HDL and C-Model for VHDL/FLI
class Helper(Carbon.MV.Helper.Base):
  def __init__(self, modelName, objectName, hierarchy, dbType, preserveNames):
    Carbon.MV.Helper.Base.__init__(self, modelName, objectName, hierarchy, 'vhdl',
                                   dbType, preserveNames)
    self.__mEmittedEntities = {}
    self.__mSeenComponents = {}  # Only emit one -p component declaration per architecture
    self.__mSeenTypes = {}


  # Function to emit the vhpi instance
  def emitVHPIcomponent(self):
    code = '''\
  component carbon_vhpi
  port(
'''

    code += self.emitVHPIcompPorts()


    code += '''\
  end component;
  
'''
    return code;


  # Function to emit the vhpi entity
  def emitVHPIentity(self):
    code = '''\
use ieee.numeric_std.all;

'''

    code += '''\
entity carbon_vhpi is
  port(
'''

    code += self.emitVHPIcompPorts();
    
    code += '''\
end carbon_vhpi;
'''
    return code;

  # Function to emit the vhpi architecture
  def emitVHPIarch(self):
    code = '''
architecture carbon_vhpi_rtl of carbon_vhpi is
  attribute foreign of carbon_vhpi_rtl : architecture is "VHPIDIRECT %(libraryName)s:NULL %(cVHMFuncName)s";
begin

end carbon_vhpi_rtl;




library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

''' %  { 'cVHMFuncName'       : self.cVHMFuncName(),
         'libraryName'        : self.libraryName() }

    return code

  # Function to emit the ports for VHPI component
  # The vhpi component ports are the same as the primary ports.
  def emitVHPIcompPorts(self):
    code = ""

    nports = len(self.getPrimaryPorts())
    index = 0
    for port in self.getPrimaryPorts():
      if index < nports - 1:
        code += "    %s : %s %s;\n" % ( port.leafName(), self.emitPortDirection(port), self.emitDeclarationType(port))
      else:
        code += "    %s : %s %s);\n" % (port.leafName(), self.emitPortDirection(port), self.emitDeclarationType(port))
        
      index += 1 

    return code


  # Function to emit the port map for the VHPI instance
  def emitVHPIcompPortMap(self):
    code = ""

    nports = len(self.getPrimaryPorts())

    code += "    port map(\n"
    index = 0
    for port in self.getPrimaryPorts():
      if index < nports - 1:
        code += "      %s => %s,\n" % ( port.leafName(), port.leafName())
      else:
        code += "      %s => %s);\n" % (port.leafName(), port.leafName())
        
      index += 1 

    return code
      
      

  # Function to emit the vhpi instance
  def emitVHPIcompInstance(self):
    code = '''\
  u_carbon_vhpi : carbon_vhpi
'''
    code += self.emitVHPIcompPortMap();
    
    return code

  
  # Function to emit the start of the top HDL model
  def emitHdlModelStartTop(self, node):
    code = ""
    
    code += self.emitVHPIentity()
    code += self.emitVHPIarch()
    
    code +=  '''\
architecture carbon of %(modelName)s is
''' %  { 'modelName'          : self.modelName(),
         'cVHMFuncName'       : self.cVHMFuncName(),
         'libraryName'        : self.libraryName() }

    code += self.emitVHPIcomponent()
    
    return code

  # Function to return translations for the component, architecture,
  # and instance name
  def namesHelper(self, node):
    # Either use a unique name or preserve the entit name
    if self.preserveNames():
      compName = node.getDBNode().componentName()
      archName = 'rtl%d' % node.getIndex()
    else:
      compName = node.uniqueName()
      archName = 'rtl'
    return { 'instName': node.leafName(),
             'compName': compName,
             'archName': archName }

  # Function to emit the start of an intermediate HDL model
  def emitHdlModelStartIntermediate(self, node):
    code = ''
    # Optionally emit the entity name if we are preserving names
    translator = self.namesHelper(node)
    compName = node.getDBNode().componentName()

    if not self.preserveNames() or not self.__mEmittedEntities.has_key(compName):
      self.__mEmittedEntities[compName] = True
      code += '''\
entity %(compName)s is
end %(compName)s;

''' % translator

    # Emit the unique architecture
    code += '''\
architecture %(archName)s of %(compName)s is
''' % translator

    # New architecture, so start over with component and type definitions
    self.__mSeenComponents.clear()
    self.__mSeenTypes.clear()
    return code

  # Function to emit the start of an HDL model
  def emitHdlModelStart(self, node, isTop, ports):
    # We skip emitting the HDL for structures and arrays
    if node.isStruct() or node.isArray():
      return ""
    
    # Start with the generic library
    code = '''\
library IEEE;
use IEEE.std_logic_1164.all;

'''
    # Add any user preamble. Note, we add a line feed in case the user didn't.
    preamble = node.getPreamble()
    if len(preamble) > 0:
      code += preamble + "\n"

    # Add the entity/architecture
    if isTop:
      code += self.emitHdlModelStartTop(node)
    else:
      code += self.emitLibraryIncludes(node)
      code += self.emitHdlModelStartIntermediate(node)
    return code

  # Function to emit the start of the logic once all net declarations are made
  def emitHdlLogicStart(self, node, isTop):
    # We skip emitting the HDL for structures and arrays
    if node.isStruct() or node.isArray():
      return ""
    else:
      return '\nbegin\n'

  # Function to emit the hookup of the VPI-wrapper Carbon Model and sync nets
  def emitHdlCModelHookup(self):
    return ''

  # Function to emit the declaration type for a node
  def emitDeclarationType(self, node):
    # If the user defined a type, use that instead
    declType = node.getTypeInfo()
    if not declType or len(declType) == 0:
      declType = node.intrinsicType()
      # A vector net could be bit_vector(or other std types) or array of bits.
      # If it's a bit_vector, we declare it here, otherwise,
      # we declare it of type of array and the array type
      # is declared in the emitLeafType function.
      if node.isVector() and  (node.declarationType() == "bit_vector"
                              or node.declarationType() == "std_logic_vector"
                              or node.declarationType() == "std_ulogic_vector"
                              or node.declarationType() == "signed"
                              or node.declarationType() == "unsigned"
                              or node.isRangeRequiredInDeclaration()):

        if node.getMsb() > node.getLsb():
          range = "(%(high)d downto %(low)d)" % { 'high' : node.getMsb(),
                                                  'low' : node.getLsb() }
        else:
          range = "(%(high)d to %(low)d)" % { 'high' : node.getMsb(),
                                              'low' : node.getLsb() }

        declType += range;
      elif node.isEnum() :
        declType = node.declarationType()
        declType += self.getEnumConstraint(node)
      else:
        declType = node.declarationType()
    return declType


  # Function to emit the port direction
  def emitPortDirection(self, node):
    type = node.portDirection()
    direction = ''
    if type == "Input":
      direction = "in"
    elif type == "Output":
      direction = "out"
    elif type == "Bid":
      direction = "inout"
    else:
      raise "Unexpected port direction '%s'" % type
    return direction


  # Function to emit an internal net declarationg
  def emitInternalNet(self, node):
    code = ""
    if node.isLeaf():
      code = self.emitLeafType(node)
      
    # Create the signal declaration
    code += "  signal %s : %s;" % ( node.leafName(), self.emitDeclarationType(node))

    # Add a comment if this is depositable and/or observable
    if node.isLeaf() and (node.isDepositable() or node.isObservable()):
      code += " -- carbon"
      if node.isDepositable():
        code += " depositSignal"
      if node.isObservable():
        code += " observeSignal"
    code += "\n"
    return code

  # Function to emit a sub-instances declaration
  def emitComponentDeclaration(self, node, ignorePreserves=False):
    # if node is sub node of composite type, skip 
    # the declaration
    if node.isContainedByComposite() :
      return ""
    
    if node.isStruct() or node.isArray() :
      code = self.emitCompositeType(node, True, False);

      if node.isStruct() :
        code += self.emitInternalNet(node)
      elif node.isArray() and (node.canBeCarbonNet() == False) :
        code += self.emitInternalNet(node)
      else :
        None
      
      return code;
        
    # Either use a unique name or preserve the entity name
    if self.preserveNames():
      compName = node.getDBNode().componentName()
      archName = 'rtl%d' % node.getIndex()
    else:
      compName = node.uniqueName()
      archName = 'rtl'

    # Emit the code for the component
    translator = self.namesHelper(node)
    # There could be multiple instances of a component; only declare it once.
    if not self.__mSeenComponents.has_key(compName):
      self.__mSeenComponents[compName] = True
      code = '''\
  component %(compName)s
  end component;
''' % translator
    else:
      code = ""

    # If we are preserving names we need to indicate which entity to use
    if self.preserveNames() and not ignorePreserves:
      code += '''\
  for %(instName)s : %(compName)s use entity work.%(compName)s(%(archName)s);
''' % translator
    return code

  # Function to emit a sub-instance of an HDL component
  def emitComponentInstance(self, node):
    # Skip the HDL emit for structures and arrays
    if node.isStruct() or node.isArray():
      code = ""
    else :  
      code = '''\
  %(instName)s : %(compName)s;
''' % self.namesHelper(node)

    return code

  # Emit the beginning of a generate block
  def emitGenerateInstanceBegin(self, label, min_idx, max_idx):
    code = '''\
  %(gen_block)s: for k in %(gen_idx_min)s to %(gen_idx_max)s generate
  ''' % {'gen_block'   : label,'gen_idx_min' : min_idx,'gen_idx_max' : max_idx}

    return code

  # Emit end of a generate blcok
  def emitGenerateInstanceEnd(self, node, label, min_idx, max_idx):
    code = '''
    begin  
'''    
    for subNode in node.subNodes():
      if subNode.isLeaf() == False:
        if self.preserveNames():
          code +='''\
      %(instName)s : entity work.%(compName)s(%(archName)s);
''' % self.namesHelper(subNode)
        else:
          code +='''\
      %(instName)s : %(compName)s;
''' % self.namesHelper(subNode)
    code += "    end generate;\n"
    return code  

  # Function to emit the end of the top HDL model
  def emitHdlModelEnd(self, node, isTop):
    if node.isStruct() or node.isArray():
      return ""
    
    code = ""

    if isTop:
      code += 'end carbon;\n\n'
    else:
      code += '''\
end %(archName)s;  -- %(compName)s

''' % self.namesHelper(node)

    return code

  # Function that returns a comment string for the language
  def commentStr(self):
    return '--'

  def getEnumConstraint(self, node):
    return ""
