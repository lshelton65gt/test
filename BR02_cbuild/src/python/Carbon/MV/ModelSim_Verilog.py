#******************************************************************************
# Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

import Carbon.MV.VPI

# Implement the helper class to emit the HDL and C-Model for ModelSim/VPI
# The base class Carbon.MV.VPI.Helper implements the target simulator
# independent code for VPI.
class Helper(Carbon.MV.VPI.Helper):

  # Constructor
  def __init__(self, modelName, objectName, hierarchy, dbType, preserveNames):
    Carbon.MV.VPI.Helper.__init__(self, modelName, objectName, hierarchy, dbType,
                                  preserveNames)

  # Function to emit the name of the c init function
  def simInit(self):
    return 'vpiMtiInit'

  # Function to emit the start of the C file
  def emitCStart(self):
#    code  = '#include "vpi_user.h"\n'
    code = '#define VPI_MTI\n'
    code += '#include "carbon/MVVpiUtils.c"\n'
    return code

  # Function to emit any extra make file variables
  def emitExtraMakeVariables(self):
    return 'SIM_HOME=$(MTI_HOME)\n'

  # Function to emit the makefile type
  def emitMakefileType(self):
    return 'mtivpi'
    
  # Overload the emitInitCModelEnd function because unlike the other
  # simulators, the vlog_startup_routines must be in the shared
  # library
  def emitInitCModelEnd(self, evalFn, quitFn):
    code = Carbon.MV.VPI.Helper.emitInitCModelEnd(self, evalFn, quitFn)
    code += '''
void (*vlog_startup_routines[])() = {
  %(modelName)s_RegisterTfs,
  0
};

''' % { 'modelName' : self.modelName() }
    return code

  # There are no other Modelsim files
  def emitOtherFiles(self):
    return
