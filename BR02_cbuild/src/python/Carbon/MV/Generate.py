#******************************************************************************
# Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#******************************************************************************

# Useful libraries
import sys
import re
import os.path
import optparse
import string

# Carbon Scripting API
import Carbon
import Carbon.DB

# Symbol table files
import Carbon.MV.ShadowHierarchy

# Generation helpers
import Carbon.MV.EmitModel
import Carbon.MV.ModelSim_VHDL

# Class to manage config files
import Carbon.MV.Config

# Main class to generate the various MV files
class Generate:
  def __init__(self):
    self.__mCount = {}
    self.__mCount["Observes"] = 0
    self.__mCount["Deposits"] = 0
    self.__mCount["Ports"] = 0
    self.__mCountString = {}
    self.__mCountString["Observes"] = "observable signal"
    self.__mCountString["Deposits"] = "depositable signal"
    self.__mCountString["Ports"] = "primary port"
    self.__Version = "MVGenerate 3.0"
    self.__mDBUid = ""            # Database UID
    self.__mFoundMvDirectives = False
    self.__mMaxMemorySize = 16    # This is for memory nets in VHDL and Verilog
    self.__mLanguage = ""

  def writeHdlFile(self, emitModel, helper, num):
    # Open the file for processing
    file = open(helper.hdlFileName(), "w")

    # Write a copy write notice
    code = helper.copyrightNotice(helper.commentStr(), helper.commentStr(), '')
    file.write(code)

    # Ask the emitModel class to emit the HDL file and write it
    file.write(emitModel.emitHdlFile (num))

    # All done with this file
    file.close()

  def writeCFile(self, emitModel, helper):
    # Open the file for processing
    file = open(helper.cFileName(), "w")

    # Write a copyright notice
    code = helper.copyrightNotice('/', '', '/')
    file.write(code)

    # Ask the emitModel class to emit the contents of the file and write it
    file.write(emitModel.emitCFile())

    # All done with the file
    file.close()

  def writeMakefile(self, emitModel, helper):
    # Open the file for writing
    file = open(helper.makefileName(), "w")

    # Write a copyright notice
    code = helper.copyrightNotice('#', '#', '')
    file.write(code)

    # Write the file using the emitModel class
    file.write(emitModel.emitMakefile())

    # All done with the file
    file.close()

  # This function calls the helper so it can write any uncommon files
  def writeOtherFiles(self, emitModel, helper):
    emitModel.emitOtherFiles()

  # Function to validate and add a node to the shadow hierarchy
  def addNode(self, hierarchy, node):
    # This is temporary, it should be removed
    # when the verilog memory nets are supported
    if ((node.sourceLanguage() == "Verilog") and node.isArray() and (node.getArrayDims() > 1)):
      type = "shadow net"
      print "Warning: ignoring unsupported %s '%s'; check the type." % (type, node.fullName())
      print "         only scalars and vectors are supported."
      print "         memories can be accessed through the Carbon command."
      return None
    elif((node.sourceLanguage() == "VHDL") and ((node.isArray() and node.getArrayDims() > 1))): 
      if node.isPrimaryInput() and node.isPrimaryOutput():
        type = "primary port"
      else:
        type = "shadow net"
        
      numElems = node.getMemNumCarbonNets()
      if numElems > self.__mMaxMemorySize:
        message = '''Warning: ignoring %s %s
    The number of elements of this memory net is %d, which exceeds the max limit.
    Use -M switch to change the maximum allowed size for the memory nets.
''' % (type, node.fullName(), numElems)
        print message
        return None

    newNode = hierarchy.addDBNode(node)
    return newNode

  # This function creates a shadow hierarchy for design which contains
  # the top level ports and internal nets that have to be kept in
  # sync.
  def createShadowHierarchy(self):
    # Add the top primary ports
    hierarchy = Carbon.MV.ShadowHierarchy.SymbolTable()
    for node in self.__mDB.iterPrimaryPorts():
      self.__mCount["Ports"]+=1
      self.addNode(hierarchy, node)

    # If there are no MV* signals, use observe- and depositSignal
    if self.__mCount["Deposits"] == 0 and self.__mCount["Observes"] == 0:
      for dbNode in self.__mDB.iterDepositable():
        node = self.addNode(hierarchy, dbNode)
        if node:
          self.__mCount["Deposits"]+=1
          node.putDepositable(True)

      # Add the observables to the hierarchy
      for dbNode in self.__mDB.iterObservable():
        node = self.addNode(hierarchy, dbNode)
        if node:
          self.__mCount["Observes"]+=1
          node.putObservable(True)

    # Warning: we found old-style Mv directives
    else:
      self.__mFoundMvDirectives = True

    # Mark the primary clocks in the hierarchy
    for dbNode in self.__mDB.iterPrimaryClks():
      # There may be port split nets in this set. If so, ignore them
      # and print a warning.
      if dbNode.isTemp():
        # We found an invalid named primary clock, print a warning
        print "Warning: found a temporary primary clock '%s'; this may cause clock propagation problems. Consider using delays or restructuring the clock logic to resolve the problem.\n" % dbNode.fullName()
      else:
        node = self.addNode(hierarchy, dbNode)
        if node:
          node.putPrimaryClock(True)
    return hierarchy

  # Create a helper class to generate the simulator/language specific logic
  def createHelper(self, config, modelName, interfaceName, hierarchy, dbType,
                   preserveNames):
    # If they didn't provide a valid simulator, fail. If they didn't provide
    # a langugage, use the top component language
    simulator = config.getSimulator()
    language = config.getLanguage()
    if language == "":
      for node in hierarchy.topNodes():
        topNode = node
      language = topNode.sourceLanguage()

    # This assumes that MV doesn't handle mixed languages, which it doesn't yet.
    self.__mLanguage = language

    # import the module containing the Helper class
    moduleName = 'Carbon.MV.' + simulator + '_' + language
    try:
      exec('import %s; helperModule = %s' % (moduleName, moduleName))
    except Exception,msg:
      print "Error: unsupported simulator '%s' and/or language '%s'" % \
            (simulator, language)
      print "      ", msg
      return None

    # A better place for this would be to have checker routines in each
    # of the derived Helper classes
    if language == "Verilog" and preserveNames:
      print "Error: preserveNames option is not yet supported for Verilog models"
      return None

    # create an instance opf the helper
    helper = helperModule.Helper(modelName, interfaceName, hierarchy, dbType,
                                 preserveNames)

    return helper

  def createOptParser(self, progName):
    usage = "usage: %s [options] <Carbon-db-file>" % progName
    parser = optparse.OptionParser(usage=usage, version=self.__Version)
    parser.add_option("-s", "--simulator", help="Target simulator")
    parser.add_option("-c", "--config", help="Use an input configuration file (cannot be used with -d)")
    parser.add_option("-d", "--directive", help="Use an input directive file (cannot be used with -c)")
    parser.add_option("-w", "--writeConfig",
                      help="Generates an output config file containing the shadow hierarchy")
    parser.add_option("-W", "--writeDirective",
                      help="Generates an output directive file containing the MV signals")
    parser.add_option("-p", "--preserveNames",
                      action="store_true", dest="preserveNames", default=False,
                      help="preserve orignal names of VHDL entities")
    parser.add_option("-i", "--inputFlowMode",
                      action="store_true", default=False,
                      help="Set input flow mode to true")
    parser.add_option("-P", "--portsOnly",
                      action="store_true", default=False,
                      help="Only use primary ports; ignore all internal nets")
    parser.add_option("-y", "--overwrite",
                      action="store_true", dest="overwrite", default=False,
                      help="Overwrite existing config file without asking")
    parser.add_option("-q", "--quiet",
                      action="store_true", dest="quiet", default=False,
                      help="Don't display information or warnings")
    parser.add_option("--allowIODB",
                      action="store_true", default=False,
                      help="Do not warn if io.db is used instead of symtab.db")
    parser.add_option("--ignoreOldDirectives",
                      action="store_true", default=False,
                      help="Ignored; retained for compatibility")
    # The add_option for maxMemorySize mentions ModelSim only.
    # it's partially incorrect, But it makes sence. the reason is below.
    # 1) memories are not supported in verilog for any simulator
    # 2) internal signals are not supported in VCS and NCSim (bug 9032).
    #    But most of designs don't use composite types as a primary port.
    #    So, this switch is only useful for ModelSim.
    parser.add_option("-M", "--maxMemorySize",
                      action="store", type="int", default=self.__mMaxMemorySize,
                      help="Set the number of elements of a memory net. The memory nets, which have more elements, will be skipped. The default value is %default. (ModelSim Only)")

    return parser
  
  def run(self, argv):
    # get the arguments
    commandLine = string.join (argv[:], " ")
    parser = self.createOptParser(argv[0])
    (options, args) = parser.parse_args(args=argv[1:])

    # Get the symbol table file
    numArgs = len(args)
    if numArgs != 1:
      print "Error: invalid number of arguments %d." % numArgs
      parser.print_help()
      return 1
    dbFile = args[0]

    # Get the options
    writeFile  = options.writeConfig
    configFile = options.config
    writeDir  = options.writeDirective
    directiveFile = options.directive
    simulator  = options.simulator
    preserveNames = options.preserveNames
    self.__mMaxMemorySize = options.maxMemorySize
    
    # Open the file and if succesful, generate the wrapper model
    self.__mDB = Carbon.DB.DBContext(dbFile)
    if self.__mDB:
      # Gather the interface name, top model name, I/Os, and internal nets
      interfaceName = self.__mDB.getInterfaceName()
      modelName = self.__mDB.getTopLevelModuleName()
      dbType = self.__mDB.getDBType()
      if dbType != "FullDB" and not options.allowIODB:
        print "Warning: Using io.db.  Symtab.db will provide better visibility.  Use -allowIODB to suppress this message."
      self.__mDBUid = self.__mDB.idString()
      # Save this for later
      #print "Database version: %s" %self.__mDBUid
      #print "API version: %s" %self.__mDB.version()
      #print "software version: %s" %self.__mDB.softwareVersion()
      hierarchy = self.createShadowHierarchy()

      # Read the configuration data and annotate the shadow hierarchy
      config = Carbon.MV.Config.Config(configFile, simulator, hierarchy)
      if configFile and not directiveFile:
        if config.read() != 0:
          return 2

      elif directiveFile and not configFile:
        dirConfig = Carbon.MV.Config.Directive(directiveFile, self.__mDB, hierarchy)
        if dirConfig.read() != 0:
          return 5

      elif directiveFile and configFile:
        print "Error: you may only specify a -c config file OR a -d directive file"
        parser.print_help()
        return 6

      # Make sure we got a simulator
      if not config.getSimulator():
        print "Error: must specify a valid simulator using simulator option or in a configuration file."
        parser.print_help()
        return 3

      # Allocate the right helper class. It will open the files for
      # writing
      helper = self.createHelper(config, modelName, interfaceName, hierarchy,
                                 dbType, preserveNames)
      if not helper:
        return 4

      # This is the actual MVGen command line.  We'll write this into generated files as a favor to the reader.
      helper.setCommandLine(commandLine);

      # Create the class that emits the model
      inputFlowMode = options.inputFlowMode or config.getInputFlowMode()

      # Ports-only option overrides config directives
      if options.portsOnly:
        filter = "FILTER_ALL"

      # Use the config file if the option is set, and if the minimal is not set
      elif configFile and config.getVersion() == "2.0":
        if config.isStartsWithNone():
          filter = "FILTER_CONFIG_IN"
        else:
          # This is the default behavior for config files
          filter = "FILTER_CONFIG_OUT"
      elif directiveFile and dirConfig.isStartsWithNone():
        filter = "FILTER_DIRCONFIG_IN"
      elif directiveFile:               # startsWith="All"
        filter = "FILTER_DIRCONFIG_OUT"
      else:
        filter = "FILTER_NONE"

      # VCS and NCSim don't support hierarchical references for VHDL,
      # which makes observes/deposits and subnodes not accessible.
      language = str(self.__mLanguage)
      sim = str(options.simulator)
      if (language == "VHDL") and ((sim == "VCS")):
        print("Warning: only primary ports are configured for VHDL designs with %s simulator.") % (sim)
        filter = "FILTER_ALL"

      # Possibly write the config file
      configWriter = fileWrite("ConfigWriter", "Config", writeFile, config, hierarchy, helper, commandLine, filter, options.overwrite)
      # Possibly write the directive file
      directiveWriter = fileWrite("ConfigDirWriter", "Directive", writeDir, config, hierarchy, helper, commandLine, filter, options.overwrite)

      emitModel = Carbon.MV.EmitModel.EmitModel(hierarchy, helper, inputFlowMode, filter)

      # Write the HDL, C, and Makefile files
      num = {}
      self.writeHdlFile(emitModel, helper, num)
      self.writeCFile(emitModel, helper)
      self.writeMakefile(emitModel, helper)
      self.writeOtherFiles(emitModel, helper)

      if not options.quiet:
        print "%s" %self.__Version

        if self.__mFoundMvDirectives == True:
          print "Note: deprecated mvObserve/mvDeposit directives were found in the database and used"

        for type in ("Ports", "Observes", "Deposits"):
          if num[type] == 1:
            verb = "has"
          else:
            verb = "have"
          if self.__mCount[type] == 1:
            plural = ""
          else:
            plural = "s"
          if num[type] == 0:
            print "Of %i %s%s, none have been configured" % (
              self.__mCount[type], self.__mCountString[type], plural)
          elif self.__mCount[type] == num[type]:
            print "Of %i %s%s, all have been configured" % (
              self.__mCount[type], self.__mCountString[type], plural)
          else:
            print "Of %i %s%s, %i %s been configured" % (
              self.__mCount[type], self.__mCountString[type], plural, num[type], verb)


        configWriter.message()
        directiveWriter.message()

      return 0

    else:
      print "Error: Failed to read the design database `%s'" % dbFile
      return 1

def run(argv):
  generate = Generate()
  return generate.run(argv)


class fileWrite:
  def __init__(self, writeClass, name, filename, config, hierarchy, helper, commandLine, filter, overwrite):
    self.__mMightWrite = False
    self.__mWasWritten = False
    self.__mName = name                 # "Config" or "Directive"
    self.__mOverwrite = overwrite
    self.__mFileName = filename

    if not filename:
      return

    self.__mMightWrite = True

    # Call the Configure or Directive class
    exec ("writer = Carbon.MV.Config." + writeClass + "(config, hierarchy, helper, commandLine, filter)")

    yn = "y"
    if os.path.isfile(self.__mFileName) and not self.__mOverwrite:
      yn = raw_input ("%s file '%s' exists.  Do you want to overwite? (y/n) " %(self.__mName, self.__mFileName))

    if yn == "y" or self.__mOverwrite:
      writer.writeFile(self.__mFileName)
      self.__mWasWritten = True
    else:
      self.__mWasWritten = False

  def message (self):
    if self.__mMightWrite:
      print
      if self.__mWasWritten:
        print "Wrote %s XML file to '%s'" %(string.lower(self.__mName), self.__mFileName)
      else: 
        print "%s file '%s' not written" %(self.__mName, self.__mFileName)

