#******************************************************************************
# Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

import Carbon.MV.SymbolTable

import re

# Class to store details about an accessible port or internal net
class Leaf(Carbon.MV.SymbolTable.Leaf):
  def __init__(self, dbNode, parent):
    Carbon.MV.SymbolTable.Leaf.__init__(self, dbNode, parent)
    self.__mDepositable = False
    self.__mObservable = False
    self.__mPrimaryClock = False
    self.__mTypeInfo = None
    self.__mDelay = 0
    self.__mInConfig = False      # True if the node is in the config XML file
        
  def isDepositable(self):
    return self.__mDepositable

  def isObservable(self):
    return self.__mObservable

  def isPrimaryPort(self):
    return self.isPrimaryInput() or self.isPrimaryOutput() or self.isPrimaryBidi()

  def isInConfig(self):
    return self.__mInConfig

  def getTypeInfo(self):
    return self.__mTypeInfo

  # Return the delay as either None (uninitialized) or a string
  def getDelay(self):
    return self.__mDelay

  def isPrimaryClock(self):
    return self.__mPrimaryClock or self.getDBNode().isClk()

  def putDepositable(self, depositable):
    self.__mDepositable = depositable

  def putObservable(self, observable):
    self.__mObservable = observable

  def putTypeInfo(self, typeInfo):
    self.__mTypeInfo = typeInfo

  def putInConfig(self):
    self.__mInConfig = True

  def putDelay(self, delayStr):
    try:
      self.__mDelay = int(delayStr)
    except:
      print "Error: invalid delay integer '%s' for connection '%s'" % \
            (delayStr, self.leafName())

  def putPrimaryClock(self, primaryClock):
    self.__mPrimaryClock = primaryClock

  def isGenerate(self):
    return None

  # This function checks whether the net is a memory net
  def isMemory(self):
    if self.getDBNode().getArrayDims() > 1:
      return True
    else:
      return False

        
  # Return a text version of the port direction. Internal nets are
  # handled as bids
  def portDirection(self):
    if self.isOutput():
      code = 'Output'
    elif self.isInput():
      code = 'Input'
    elif self.isBidi():
      code = 'Bid'
    else:
      raise "Error: unknown node type (must be input, output, bid, or non-port)"
    return code

  # If a node is a member of a structure, return a name that includes
  # all nested member names; otherwise return the name directly.
  def structMemberName(self):
    name = self.leafName()
    node = self
    while node.isContainedByComposite() and node.getParent() and node.getParent().isStruct():
      node = node.getParent()
      name = node.leafName() + '_' + name
    return name
  
  # Return a unique name used for a flattened declaration. This is
  # created from the parent unique name if it isn't a port. If it is
  # a port, return a name that includes all members of a nested structure;
  # or the leaf name, if it's not a structure.
  def uniqueName(self):
    if self.isPrimaryPort():
      return self.structMemberName()
    else:
      return self.getParent().uniqueName() + '_' + self.leafName()

  # Override the declaration type to use the local info if it exists
  def declarationType(self):
    return Carbon.MV.SymbolTable.Leaf.declarationType(self)

# Class to store details about a branch node in the shadow hierarchy
class Branch(Carbon.MV.SymbolTable.Branch):

  # A VHDL generate makes an entity with two underscores and a number
  gen_re = re.compile("^(.+)?__(\d+)$")

  def __init__(self, dbNode, parent):
    Carbon.MV.SymbolTable.Branch.__init__(self, dbNode, parent)
    self.__mIndex = 0
    self.__mPreamble = ''
    self.__mInConfig = False           # True if the node is in the config XML file

  def putIndex(self, index):
    self.__mIndex = index;

  def putInConfig(self):
    self.__mInConfig = True

  def getIndex(self):
    return self.__mIndex

  def isInConfig(self):
    return self.__mInConfig

  # Apparently, port information is on the leaf, not the branch.
  # Recurse down to the first leaf, and return the port status of
  # the leaf.
  def isPrimaryPort(self):
    subNodes = self.subNodes()
    for subNode in subNodes:
      return subNode.isPrimaryPort()

  # If there are two underscores, then this could only mean it is a
  # VHDL Generate statement
  def isGenerate(self):
    if self.sourceLanguage() == "VHDL":
      gen_match = self.gen_re.search(self.leafName())
      if gen_match:
        return gen_match.groups()
      else:
        return None
    else:
      return None

  def uniqueName(self):
    return "carbon_%s_%d" % (self.getDBNode().componentName(), self.__mIndex)

  def appendToPreamble(self, text):
    self.__mPreamble += text;

  def getPreamble(self):
    return self.__mPreamble

  def getTypeInfo(self):
    return None

# Class to keep track of the symbol table
class SymbolTable(Carbon.MV.SymbolTable.SymbolTable):
  def __init__(self):
    Carbon.MV.SymbolTable.SymbolTable.__init__(self)
    self.__mComponentIndices = {}
    self.__mPrimaryPorts = []

  # Override the leaf creation function to add our data
  def newLeaf(self, dbNode, parent):
    # If this is a primary port, record it so that we can visit them
    # in the original order (needed for Verilog)
    node = Leaf(dbNode, parent)
    if node.isPrimaryPort():
      self.__mPrimaryPorts.append(node)
    return node

  # Override the branch creation function to add our data
  def newBranch(self, dbNode, parent):
    # There can be multiple branches with the same component
    # name. Give them unique names
    name = dbNode.componentName()
    index = 0
    if self.__mComponentIndices.has_key(name):
      index = self.__mComponentIndices[name] + 1
    self.__mComponentIndices[name] = index
    branch = Branch(dbNode, parent)
    branch.putIndex(index)
    return branch

  # Get the primary ports in the original declaration order
  #
  # Note, this counts on the primary ports getting added to the symbol
  # table in the original order.
  def getPrimaryPorts(self):
    return self.__mPrimaryPorts

  def findPrimaryPortByName(self, name):
    for portnode in self.__mPrimaryPorts:
      if portnode.leafName() == name:
        return portnode
    return None

# Shadow hierarchy walker. This does a filtered walk and prunes
# parts of the hierarchy where the language changes
class Walker(Carbon.MV.SymbolTable.Walker):
  def __init__(self, symbolTable, filterMode = "FILTER_NONE"):
    Carbon.MV.SymbolTable.Walker.__init__(self, symbolTable)
    self.__mLanguage = None
    self.__mFilterMode = filterMode

  # Override the filter function.
  def filter(self, node):
    # The language is stored at the branches
    if node.isBranch():
      # If no language has been set, set it now and don't filter
      # (Presumably, this is the Primary (top-level) branch)
      if self.__mLanguage == None:
        self.__mLanguage = node.sourceLanguage()
        return False
      elif self.__mLanguage != node.sourceLanguage():
        return True
      if self.__mFilterMode == "FILTER_ALL":
        return True
      else:
        return False

    # Three cases: take all signals, take only primary nets, and use config file
    # (Remember that "True" means "Yes, filter this node out")
    # These are used after the initial parse, i.e. for emit and config file writing

    # Use all Observes and Deposits (filter nothing)
    elif self.__mFilterMode == "FILTER_NONE":
      return False

    elif node.isLeaf() and not node.isPrimaryPort():
      # Use only primary nets (filter all internal signals)
      if self.__mFilterMode == "FILTER_ALL":
        return True

      # FILTER_DIRCONFIG_IN: Add only the nodes that have been explicity specified by the
      #   directive XML file
      elif self.__mFilterMode == "FILTER_DIRCONFIG_IN":
        if not node.isInConfig():
          return True

      # FILTER_DIRCONFIG_OUT: Add all signals EXCEPT those in the directive file
      elif self.__mFilterMode == "FILTER_DIRCONFIG_OUT":
        if node.isInConfig():
          return True

      # FILTER_CONFIG_OUT: Use internal nets specified in the config file, IF
      #  the scope is also specified in XML file (if scope is not specified,
      #  pass ALL signals in that scope).
      # This is the default beavior of config files
      elif self.__mFilterMode == "FILTER_CONFIG_OUT":
        if node.getParent().isInConfig() and not node.isInConfig():
          return True

      # FILTER_CONFIG_IN: Only the nets specified in the config file are in
      elif self.__mFilterMode == "FILTER_CONFIG_IN":
        if not node.isInConfig():
          return True

    return False


# This class walks the hierarchy without a filter and prints
# information About all levels of hierarchy that will be ignored
# because they are a different language.
class MixedLanguageWarning(Walker):
  def __init__(self, symbolTable):
    Walker.__init__(self, symbolTable)

  # Override the filter to catch the locations and print a message
  def filter(self, node):
    # Use the base classes filter to know if we are going to filter it
    if Walker.filter(self, node):
      print "Warning: hierarchy starting at '%s' ignored because it is a different language than the top component." % node.fullName()
      return True
    else:
      return False
