#******************************************************************************
# Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

import Carbon.MV.VPI

# Implement the helper class to emit the HDL and C-Model for Aldec/VPI
# The base class Carbon.MV.VPI.Helper implements the target simulator
# independent code for VPI.
class Helper(Carbon.MV.VPI.Helper):

  # Constructor
  def __init__(self, modelName, objectName, hierarchy, dbType, preserveNames):
    Carbon.MV.VPI.Helper.__init__(self, modelName, objectName, hierarchy, dbType,
                                  preserveNames)

  # Function to emit the name of the c init function
  def simInit(self):
    return 'vpiInit'

  # Function to emit the start of the C file
  def emitCStart(self):
    code = '#include "carbon/MVVcsUtils.c"\n'
    return code

  # Override the carbon command to use the PLI variant
  def cCarbonCmdInit(self):
    return 'pliCarbonCommandInit'

  # Function to emit any extra make file variables
  def emitExtraMakeVariables(self):
    return 'SIM_HOME=$(VCS_HOME)\n'

  # Function to emit the makefile type
  def emitMakefileType(self):
    return 'vcsvpi'

  # For VCS we have to write PLI table files
  def emitOtherFiles(self):
    # Open a file based of the model name
    fileName = 'carbon_' + self.modelName() + '.tab'
    file = open(fileName, "w")

    # write the copyright notice
    code = self.copyrightNotice('//', '//', '')

    # Emit the contents of the table
    code += '''acc+=rw,cbk,wn:%(modelName)s+\n''' \
            % { 'vhmProcessName' : self.vhmProcessName(),
                'cVHMCreateName' : self.cVHMCreateName(),
                'cVHMFuncName'   : self.cVHMFuncName(),
                'modelName'      : self.modelName() }
    file.write(code)

    # All done
    file.close()
