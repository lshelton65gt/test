#******************************************************************************
# Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

# Symbol table files
import Carbon.MV.ShadowHierarchy

# This class is Base class for CodeWalker and CountObservesDeposits classes.
# It wraps the ShadowHierarchy.Walker class and holds the helper class instance
# and function for filtering nodes with types defined not in library/package.
class CodeWalkerBase(Carbon.MV.ShadowHierarchy.Walker):
  def __init__(self, symbolTable, helper, filterMode):
    Carbon.MV.ShadowHierarchy.Walker.__init__(self, symbolTable, filterMode)
    self.mHelper = helper

# General code walker class to walk the hierarchy and return the code
class CodeWalker(CodeWalkerBase):
  def __init__(self, symbolTable, helper, filterMode):
    CodeWalkerBase.__init__(self, symbolTable, helper, filterMode)
    self.mCode = ''

  def filter(self, node):
    # run parent filter
    if Carbon.MV.ShadowHierarchy.Walker.filter(self, node) :
      return True
  
    rtnFlag = False
    if self.mHelper.filterLibPack(node):
      rtnFlag = True
      
    return rtnFlag    

  # Routine to add code for use by derived classes
  def addCode(self, code):
    self.mCode += code

  # Function to walk and return the code
  def code(self):
    self.walk()
    return self.mCode

# This class counts the observe/deposits
# and issues warning messages for the ones, that it shoud skip.
# The checkLibPackage helper function defines, whether
# it should check for location of the type declaration.
class CountObservesDeposits(CodeWalkerBase):
  def __init__(self, symbolTable, helper, filterMode):
    CodeWalkerBase.__init__(self, symbolTable, helper, filterMode)

  def filter(self, node):
    # run parent filter
    if Carbon.MV.ShadowHierarchy.Walker.filter(self, node) :
      return True

    rtnFlag = False
    if self.mHelper.filterLibPack(node):
      print "Warning: Net %s has not been configured, because the type declaration is not in a package." % (node.fullName())
      return True

    return False

  def visitLeaf(self, node):
    self.mHelper.countPortObserveDeposits(node)
    return True
  

# Class to emit the shadow hierarchy definitions
#
# This does a post walk so that we emit the hierarchy in reverse order.
# This is needed for VHDL.
class EmitShadowHierarchy(CodeWalker):
  def __init__(self, symbolTable, helper, filter):
    CodeWalker.__init__(self, symbolTable, helper, filter)

    self.__mGenNodes = {}
    self.__mPassNumber = 0

  def incrementPassNumber(self):
    self.__mPassNumber = self.__mPassNumber + 1;
    # remove the previous code
    self.mCode ="" 

  def visitBranchPre(self, node):
    # Emit Generate block entity
    # This pass is used only for vhdl generate.
    if self.__mPassNumber == 1:
      self.doGenerateDeclaration(node)

    return True



  # Emit the hierarchy for a given branch node
  def visitBranchPost(self, node):
    # Skip emit code in second pass
    if self.__mPassNumber == 1:
      return True
    
    # Skip this if this is a struct
    if node.isStruct():
      return True
    # Emit the start of the module/architecture
    ports = []
    isTop = node.isPrimary()

    # Skip this if this is a VHDL generate
    skipNode = False
    if isTop == False:
      # Gather Generate Data
      skipNode = self.gatherGenerateData(node)
    
    if isTop:
      ports = self.getSymbolTable().getPrimaryPorts()

    # Don't emit entity/architecture for vhdl generate 
    if skipNode == False: 
      self.addCode(self.mHelper.emitHdlModelStart(node, isTop, ports))

    # Walk the sub nets/instances and add declarations
    for subNode in self.subNodes(node):
      if skipNode == False:
        temp = []
        self.doDeclarations(subNode, False, temp)

    # Emit the code to start the logic
    if skipNode == False: 
      self.addCode(self.mHelper.emitHdlLogicStart(node, isTop))

    # Walk the sub instances and add them
    for subNode in self.subNodes(node):
      # A struct is a branch, so ignore those
      if subNode.isBranch():
        gen = subNode.isGenerate()
        if gen:
          gen_label = gen[0]
          gen_index = gen[1]
          if self.__mGenNodes.has_key(gen_label):
            # Emit the generate statement with min/max indexes
            if self.__mGenNodes[gen_label][3] == subNode.uniqueName():
              self.addCode(self.mHelper.emitGenerateInstanceBegin(gen_label,
                                                                  self.__mGenNodes[gen_label][0], 
                                                                  self.__mGenNodes[gen_label][1]))
              for subSubNode in subNode.subNodes():
                if subSubNode.isLeaf() == True:
                  temp = []
                  self.doDeclarations(subSubNode, False, temp)
               
              self.addCode(self.mHelper.emitGenerateInstanceEnd(subNode, gen_label, 
                                                                self.__mGenNodes[gen_label][0], 
                                                                self.__mGenNodes[gen_label][1]))
        elif skipNode == False:
          self.addCode(self.mHelper.emitComponentInstance(subNode))

    # Emit the c-model processes to hook up the nets and Carbon Model
    if isTop:
      self.addCode(self.mHelper.emitHdlCModelHookup())

    # Emit the end of the module/architecture
    if skipNode == False: 
      self.addCode(self.mHelper.emitHdlModelEnd(node, isTop))
    return True

  # This method gathers all the generate blocks and
  # saves the info in the __mGenNodes dictionary
  # It return skipNode == True, if the node or it's
  # parrent is generate block.
  def gatherGenerateData(self, node):
    skipNode = False
    genNodeData = node.isGenerate()
    if genNodeData:
      genLabel = genNodeData[0]
      genIndex = genNodeData[1]
      # Now save the generate data
      self.saveGenerateData(node, genLabel, genIndex)
    # Get the parent Node
    genParentNode = node.getParent()
    genParentNodeData = genParentNode.isGenerate()

    if genNodeData != None or genParentNodeData != None:
      skipNode = True

    return skipNode

  # Save the data of generate block
  def saveGenerateData(self, node, gen_label, gen_index):
    if self.__mGenNodes.has_key(gen_label):
      # calculate min/max of generate index
      gen_min_idx = self.__mGenNodes[gen_label][0]
      gen_max_idx = self.__mGenNodes[gen_label][1]
      if gen_index < gen_min_idx :
        self.__mGenNodes[gen_label][0] = gen_index
      elif gen_index > gen_max_idx :
        self.__mGenNodes[gen_label][1] = gen_index
      else :
        None

    else:
      # This is first time we see this generate block, save it
      # We save here the min, max indices and first instance of the generate.
      # The data saved as [gen_label] = [min, max, current, block name]
      # The block name is the name of the block to be used for emmiting
      # generate code
      self.__mGenNodes[gen_label] = [gen_index, gen_index, gen_index, ""]
      self.__mGenNodes[gen_label][3] = node.uniqueName()
 
  # Emits entity/architecture for vhdl generate block
  def doGenerateDeclaration(self, node):
    sub_sub_sub_nodes = []
    item_names = []
    # Check whether this is a parent of a generate block
    # if yes, gather all the nets to be declared
    for subNode in self.subNodes(node):
      gen = subNode.isGenerate()
      if gen:
        for subSubNode in self.subNodes(subNode):
          if subSubNode.isBranch() == True:
            for subSubSubNode in self.subNodes(subSubNode):
              sub_sub_sub_nodes.append(subSubSubNode)
            
    # if we have no nets, then we don't need to declare them
    if len(sub_sub_sub_nodes) == 0:
      return

    # now declare them
    for subNode in self.subNodes(node):
      gen = subNode.isGenerate()
      if gen:
        gen_label = gen[0]
        gen_index = gen[1]
        # We are emitting only single entity inside the generate block
        # All the observes/deposits inside the all the generate blocks
        # are put in that single entity
        if self.__mGenNodes[gen_label][3] == subNode.uniqueName():
          for subSubNode in self.subNodes(subNode):
            self.addCode(self.mHelper.emitHdlModelStart(subSubNode, False, None))
            for item in sub_sub_sub_nodes:
              name = item.leafName()
              self.doDeclarations(item, False,item_names)
              
            self.addCode(self.mHelper.emitHdlLogicStart(subSubNode, False))
            self.addCode(self.mHelper.emitHdlModelEnd(subSubNode, False))


  # Emit the declarations
  def doDeclarations(self, subNode, ignorePreserves, node_names):
    # The code below is for vhdl generate blocks only
    # We need to count the observe/deposit for each net from different
    # generate blocks, but emit it only ones
    name = subNode.leafName();
    isAdded = False
    if node_names.count(name) == 0:
      node_names.append(name)
    else:
      isAdded = True
    #####################################
    
    gen_label = ""
    gen_index = 0
    genData = subNode.isGenerate()
    if genData != None:
      gen_label = genData[0]
      gen_index = genData[1]

    # Don't do anything with a primary port. It's already declared  
    if subNode.isPrimaryPort():
      None

    elif subNode.isLeaf():
      # skip it, if it was added before
      if isAdded == False:
        self.addCode(self.mHelper.emitInternalNet(subNode))
        
    elif genData != None:
      # don't emit the component declaration for vhdl generate,
      # instead emit component for sub node of generate.
      if self.__mGenNodes.has_key(gen_label):
        if self.__mGenNodes[gen_label][3] == subNode.uniqueName():
          for subSubNode in self.subNodes(subNode):
            self.addCode(self.mHelper.emitComponentDeclaration(subSubNode, ignorePreserves))
    else:
      self.addCode(self.mHelper.emitComponentDeclaration(subNode, ignorePreserves))

  # local: return the counts
  def getStats(self, stats):
    stats['Observes'] = self.mHelper.getNumberObserves()
    stats['Deposits'] = self.mHelper.getNumberDeposits()
    stats['Ports'] = self.mHelper.getNumberPorts()


# Class to emit the shadow hierarchy C structure declarations
#
# This does a walk of the shadow hierarchy and emits an input, output,
# or bid declaration for each net. Ports use their defined direction
# and internal nets are handled as bids (see portDirection in
# ShadowHierarchy.py)
class EmitShadowStructureDeclarations(CodeWalker):
  # Emit the declaration for this port or internal net
  def visitLeaf(self, node):
    direction = node.portDirection()
    declaration = self.mHelper.emitCStructureDeclaration(direction)
    if node.isPrimaryPort():
      comment = '/* primary port */'
    else:
      comment = '/* internal net */'

     # If this is a memory, then add the bounds to the decl
    if node.isMemory():
      numCarbonNets = node.getDBNode().getMemNumCarbonNets()
      bounds = "[" + str(numCarbonNets) + "]"      
    else:
      bounds = ""

    self.addCode("  %(structureDeclaration)s\t%(fieldName)s%(bounds)s;\t\t%(comment)s\n" % \
                 { 'structureDeclaration' : declaration,
                   'fieldName'            : self.mHelper.cIdentifier(node),
                   'bounds'               : bounds,
                   'comment'              : comment })
    return True

# Class to emit the shadow hierarchy C port hookups
#
# This does a walk of the shadow hierarchy and calls the
# helper class to emit port hookups. This will set up
# the appropriate callbacks to keep the ports and
# internal nets in sync between the two models.
class EmitCPortHookups(CodeWalker):
  def __init__(self, symbolTable, helper, filter):
    CodeWalker.__init__(self, symbolTable, helper, filter)
    self.mSimHierNameStack = []
    self.mCarbonHierNameStack = []

  # In the pre function, add the scope name for constructing hierarchical names
  def visitBranchPre(self, node):
    # Don't store the top level name, it is different when looking into the
    # target simulator view or Carbon Model view. Also only the target simulator
    # knows the top instance name.
    if not node.isPrimary() and not node.isArray():
      sepSim = self.mHelper.getSepSim(node)
      sepCarbon = self.mHelper.getSepCarbon()
      self.mCarbonHierNameStack.append(node.leafName()+sepCarbon)
      self.mSimHierNameStack.append(node.leafName()+sepSim)
    return True

  # In the post function, pop the scope off the name stacks
  def visitBranchPost(self, node):
    # Don't do anything for the top level and arrays
    if not node.isPrimary() and not node.isArray():
      self.mCarbonHierNameStack.pop()
      self.mSimHierNameStack.pop()
    return True

  # Emit the hookup for this port or internal net
  def visitLeaf(self, node):
    self.mCode += self.mHelper.emitCPortHookup(node,
                                               "".join(self.mSimHierNameStack),
                                               "".join(self.mCarbonHierNameStack))
    return True

  # Return the resulting string
  def getResult(self):
    return self.mCode

# Class to emit the shadow hierarchy C port data updates
#
# This does a walk of the shadow hierarchy and calls the
# helper class to emit port data updates. This will update
# the Carbon value for any data ports
class EmitCPortDataUpdates(CodeWalker):
  # Emit the hookup for this port of internal net
  def visitLeaf(self, node):
    if not node.isPrimaryClock() and not node.isOutput():
      self.addCode(self.mHelper.emitDataPortUpdate(node))
    return True

# Class to emit the shadow hiearchy C port disconnects
#
# This does a walk of the shadow hierarchy and calls the
# helper class to emit port disconnects. This will free
# any processes or memory associated with the hookup.
class EmitCPortDisconnects(CodeWalker):
  # Emit the disconnects for this port of internal net
  def visitLeaf(self, node):
    self.addCode(self.mHelper.emitCPortDisconnect(node))
    return True

# This class provides the main entry points to emit the model files.
# It contains the logic that is common across all the target
# language and simulators.
#
# Returns strings for the C, HDL, and Makefile. It uses the
# provided helper class to emit the language/target simulator
# specific logic.
class EmitModel:
  # constructor
  def __init__(self, hierarchy, helper, inputFlow, filter):
    self.mHierarchy = hierarchy
    self.mHelper = helper
    self.mInputFlow = inputFlow
    self.__mFilter = filter

  ## The following are set of general emitting functions that apply no
  ## matter what the target simulator or language.

  # Function to emit the C structure (returns the string)
  def emitCStructure(self):
    # Walk the shadow hierarchy and emit the structure contents
    emit = EmitShadowStructureDeclarations \
           (self.mHierarchy, self.mHelper, self.__mFilter)
    structureContents = emit.code()

    code  = '''\
typedef struct {
  /* Carbon Model Interface Nets */
%(structureContents)s

  /* General Model Data */
%(modelStructureField)s

} %(structureName)s;
''' % { "structureContents"   : structureContents,
        "modelStructureField" : self.mHelper.emitCStructureModel(),
        "structureName"       : self.mHelper.emitStructureName() }
    return code

  # Function to emit the schedule execution code. This is dependent
  # on the flow mode
  def emitScheduleCall(self):
    portUpdates = EmitCPortDataUpdates \
                  (self.mHierarchy, self.mHelper, self.__mFilter)
    if self.mInputFlow:
      code = '''\
  /* Update the data ports because we want them to flow */
  if (dataChanged) {
%(portUpdates)s  }

  /* Execute the single Carbon Model schedule */
  if (clockChanged || dataChanged) {
    printTraceString("CM", "Start Schedule"); /* "CM" for "Carobn Model" */
    status = carbonSchedule(mvGetModel(&data->carbon.vhm), time);
    printTraceString("CM", "End Schedule");
  }
''' % { 'portUpdates' : portUpdates.code() }
    else:
      code = '''\
  /* Execute the clock schedule */
  if (clockChanged) {
    printTraceString("CM", "Start Clock Schedule");
    status = carbonClkSchedule(mvGetModel(&data->carbon.vhm), time);
    printTraceString("CM", "End Clock Schedule");
  }

  /* Update the input data values and run the data schedule */
  if (dataChanged && (status != eCarbon_FINISH)) {
%(portUpdates)s
    /* Run the data schedule */
    printTraceString("CM", "Start Data Schedule");
    status = carbonDataSchedule(mvGetModel(&data->carbon.vhm), time);
    printTraceString("CM", "End Data Schedule");
  }''' % { 'portUpdates' : portUpdates.code() }
    return code

  # Function to emit the Carbon Model execute C-Model
  def emitEvalCModel(self):
    return '''\
/* Callback function to execute the Carbon Model */
static %(evalCModelFn)s
{
  %(structureName)s*\tdata = (%(structureName)s*)%(structureInit)s;
  char\t\tdataChanged;
  char\t\tclockChanged;
  CarbonTime\ttime;
  CarbonStatus\tstatus = eCarbon_OK;

  /* Optionally print that we are executing the Carbon Model */
  printTraceString("CM", "Start Execution");

%(timeInit)s

  /* Get and clear the clock/data changed flags */
  mvTestAndClearChangedFlags(&data->carbon.vhm, &clockChanged, &dataChanged);

%(scheduleCall)s

  /* Optionally print that we are executing the Carbon Model */
  printTraceString("CM", "End Execution");
%(evalCModelEnd)s}
''' % { 'evalCModelFn'  : self.mHelper.emitCallbackFn("evalModel"),
        'structureName' : self.mHelper.emitStructureName(),
        'structureInit' : self.mHelper.emitStructureInit(),
        'timeInit'      : self.mHelper.emitTimeInit("time"),
        'scheduleCall'  : self.emitScheduleCall(),
        'evalCModelEnd' : self.mHelper.emitEvalCModelReturn()}

  # Function to emit the start of the quit C function
  def emitTerminateFunction(self):
    disconnect = EmitCPortDisconnects \
                 (self.mHierarchy, self.mHelper, self.__mFilter)
    return '''
static %(quitFnDecl)s
{
  %(structureName)s*\tdata = (%(structureName)s*)%(structureInit)s;
  CarbonObjectID*\tmodel = mvGetModel(&data->carbon.vhm);

  /* destroy the model */
  carbonDestroy(&model);

  /* Clean up the inputs, outputs, and bids */
%(disconnectCalls)s
  %(freeFn)s(data);
}
''' % { 'quitFnDecl'      : self.mHelper.emitCallbackFn('quitModel'),
        'structureName'   : self.mHelper.emitStructureName(),
        'structureInit'   : self.mHelper.emitStructureInit(),
        'disconnectCalls' : disconnect.code(),
        'freeFn'          : self.mHelper.freeFn() }

  # Function to emit the message callback C function
  def emitMessageFunction(self):
    return '''
static eCarbonMsgCBStatus messageCB(CarbonClientData data, CarbonMsgSeverity severity, int num, const char* text, unsigned int len)
{
  /* If fatal, end the simulation */
  if (severity == eCarbonMsgFatal) {
    %(printCall)s("Fatal %%d: %%s\\n", num, text);
    %(exitCall)s;
    return eCarbonMsgStop;
  }

  return eCarbonMsgContinue;
}
''' % { 'printCall' : self.mHelper.emitPrintString(),
        'exitCall' : self.mHelper.emitSimulatorFatalExit() }


   ## The following are the main entry points into this class. They
  ## emit a string to be written to each of the three output files.

  # Return a string for the HDL File contents
  def emitHdlFile(self, stats):
    # Walk the shadow hierarchy and print warnings about mixed language
    Carbon.MV.ShadowHierarchy.MixedLanguageWarning(self.mHierarchy).walk()

    # Emit a comment to indicate the purpose of this file
    code = '''\
%s Carbon-generated shadow hierarchy that executes the Carbon Model\n\n''' % \
    self.mHelper.commentStr()

    count = CountObservesDeposits(self.mHierarchy, self.mHelper, self.__mFilter)
    count.walk()

    # Emit the shadow hierarchy including the top level ports
    emit = EmitShadowHierarchy (self.mHierarchy, self.mHelper, self.__mFilter)
    code += emit.code()
    emit.incrementPassNumber()
    code += emit.code()
    emit.getStats(stats);

    return code

  # Return a string for the C File contents
  def emitCFile(self):
    # Emit the start of the C File
    code =  '#include <stdio.h>\n'
    code += '#include "lib' + self.mHelper.objectName() + '.h"\n'
    code += self.mHelper.emitCStart()

    # Emit the C Structure to store the Carbon Model and I/O data
    code += self.emitCStructure()

    # Emit the forward declarations of the eval and quit functions
    code += '''
/* Forward declarations of callback functions */
static %(evalFnDecl)s;
static %(quitFnDecl)s;
static eCarbonMsgCBStatus messageCB(CarbonClientData, CarbonMsgSeverity severity, int, const char* text, unsigned int);


''' % { 'evalFnDecl' : self.mHelper.emitCallbackFn('evalModel'),
        'quitFnDecl' : self.mHelper.emitCallbackFn('quitModel') }

    # Emit the start of the init C-Model process/task
    code += self.mHelper.emitInitCModelStart('evalModel', 'quitModel')

    # Emit the code to hook up all the ports
    code += '\n  /* Hookup all the ports an internal nets */\n'
    emit = EmitCPortHookups(self.mHierarchy, self.mHelper, self.__mFilter)
    code += emit.code()
    code += '\n'

    # Emit the end of the init C-Model process/task
    code += self.mHelper.emitInitCModelEnd('evalModel', 'quitModel')

    # Emit the eval C-Model
    code += self.emitEvalCModel()

    # Emit the start of the terminate function
    code += self.emitTerminateFunction()

    # Emit the message callback function
    code += self.emitMessageFunction()
    return code

  # Return a string for the Makefile contents
  def emitMakefile(self):
    return '''\
# Make sure CARBON_HOME is set correctly
ifeq ($(CARBON_HOME),)
  $(error "CARBON_HOME must be set to the Carbon release directory!")
endif

# Set the required variables
MODEL_NAME=carbon_%(modelName)s
DESIGN_NAME=%(objectName)s
%(extraMakeVariables)s
# Optional variable to turn on I/O tracing
TRACE=

# Target to create the sharable library
all : $(MODEL_NAME).so

# File containing rules to create the sharable library
include $(CARBON_HOME)/makefiles/Makefile.%(makefileType)s
''' % { 'modelName'          : self.mHelper.modelName(),
        'objectName'         : self.mHelper.objectName(),
        'extraMakeVariables' : self.mHelper.emitExtraMakeVariables(),
        'makefileType'       : self.mHelper.emitMakefileType() }

  # Tell the helper it can now write other simulator specific files
  def emitOtherFiles(self):
    self.mHelper.emitOtherFiles()
