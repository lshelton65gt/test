import os
import os.path
import Carbon.DB
import sys

## NOTE if you want to insert a breakpoint into this file just put the following two stmts at the point you want to break:
##     import pdb; pdb.set_trace()

from carboncfg import *

class ConfigError(Exception):
  pass

class CustomCode:
  def __init__(self, code):
    self.__code = code

  # Add additional code to this section
  def append(self, code):
    self.__code += '\n' + code

  # Return the combined code
  def code(self):
    return self.__code

class CustomCodeContainer:
  def __init__(self):
    self.__customCodes = {}
    self.__retrievedCodes = set()

  # Create key from a section/position
  def __key(self, section, position):
    return (section, position)

  # Add a piece of custom code to the container
  def addCustomCode(self, section, position, code):
    # Check if we have a code section for this already. If so, we
    # append to it. We use the section and position as a key.
    key = self.__key(section, position)
    if key in self.__customCodes:
      customCode = self.__customCodes[key]
      customCode.append(code)
    else:
      customCode = CustomCode(code)
      self.__customCodes[key] = customCode

  # Get the custom code for a given section/position; keep track of
  # pieces that have been retrieved
  def __getCustomCode(self, section, position):
    # Get the code for this key if it exists
    key = self.__key(section, position)
    if key in self.__customCodes:
      # Remember this key
      self.__retrievedCodes.add(key)

      # Return the code
      return self.__customCodes[key].code()
    return ''

  # get the PRE custom code for a given section; keep track of pieces
  # that have been retrieved. This allows us to not export the
  # position enum.
  def getCustomCodePre(self, section):
    return self.__getCustomCode(section, eCarbonCfgPre)
  
  # get the POST custom code for a given section; keep track of pieces
  # that have been retrieved. This allows us to not export the
  # position enum.
  def getCustomCodePost(self, section):
    return self.__getCustomCode(section, eCarbonCfgPost)
  
  # Print a message about unused custom codes
  def printUnusedCodes(self, output, type):
    # Sort because we have gold files that need to match
    keyList = self.__customCodes.keys()
    keyList.sort()
    for key in keyList:
      if key not in self.__retrievedCodes:
        msg = "%s Custom Code" % type
        msg += " for (%s, %s) not processed." % key
        output.warning(msg)

# A base class to put shared functionality and data between components
# and sub components. The component information is stored in the
# Config class and the sub component information is stored in the
# SubComponent class below.
class ComponentBase:
  def __init__(self, cfg):
    self._cfg = cfg
    self.__elf = None

    # Add the cadi sub piece
    self.__cadi = Cadi(carbonCfgGetCadi(self._cfg), self.compName())

    # compute list of debug regs
    self.__debugRegs = []
    for i in range(carbonCfgNumRegisters(self._cfg)):
      reg = carbonCfgGetRegister(self._cfg, i)
      self.__debugRegs.append(DebugReg(reg))

    # compute list of debug mems
    self.__debugMemories = []
    for i in range(carbonCfgNumMemories(self.cfg())):
      mem = carbonCfgGetMemory(self.cfg(), i)
      self.__debugMemories.append(Memory(mem))

    # Run through the memories and assign each an index for the array
    # it is in
    mem_count = 0
    for mem in self.__debugMemories:
      mem.putIndex(mem_count)
      mem_count = mem_count + 1

    # Compute the list of component custom codes
    self.__customCodes = CustomCodeContainer()
    for i in range(carbonCfgNumCustomCodes(self._cfg)):
      cc = carbonCfgGetCustomCode(self._cfg, i)
      section = carbonCfgCustomCodeGetSectionString(cc)
      position = carbonCfgCustomCodeGetPosition(cc)
      code = carbonCfgCustomCodeGetCode(cc)
      self.__customCodes.addCustomCode(section, position, code)

    # Legacy debug memory structure for SoC Designer
    if carbonCfgUseLegacyMemories(self._cfg):
      self.__legacyMemories = True
    else:
      self.__legacyMemories = False
    
    # ELF Loader
    elf = carbonCfgGetELFLoader(self._cfg)
    if(carbonCfgELFLoaderNumSections(elf) > 0):
      self.__elf = ELFLoader(elf)

    # Processor Info
    self.__procInfo = ProcessorInfo(self._cfg)

  def elfLoader(self):
    return self.__elf

  def procInfo(self):
    return self.__procInfo
  
  def cfg(self):
    return self._cfg;

  # Custom code
  def getCustomCodePre(self, section):
    code = self.__customCodes.getCustomCodePre(section)
    return code

  # Custom code
  def getCustomCodePost(self, section):
    code = self.__customCodes.getCustomCodePost(section)
    return code

  # Print any unused custom code sections
  def printUnusedCodes(self, output):
    # Start with the component unused custom codes
    self.__customCodes.printUnusedCodes(output, "Component")

    # Print any cadi unsigned custom codes
    self.__cadi.printUnusedCodes(output)
    
    # Print any debug register unused codes
    for reg in self.debugRegs():
      reg.printUnusedCodes(output)

  # Return the cadi wrapper
  def cadi(self):
    return self.__cadi

  # Display name for the component
  def compDisplayName(self):
    return carbonCfgGetCompDisplayName(self._cfg)

  # Component type name
  def typeName(self):
    return carbonCfgGetType(self._cfg)

  # Load File Extension
  def loadfileExtension(self):
    return carbonCfgGetLoadfileExtension(self._cfg)

  # Return a string containing the qualified PC trace accessor method.
  # The method needs to be defined in the .ccfg, and the
  # PCTraceProviderIF interface needs to be defined in SoCD.
  def qualifiedPCTraceAccessor(self):
    accessor = carbonCfgGetPCTraceAccessor(self._cfg)
    if accessor != '':
      maxsimHome = os.getenv('MAXSIM_HOME')
      pcTraceProviderIF = os.path.join(maxsimHome, "include", "PCTraceProviderIF.h")
      if not os.path.exists(pcTraceProviderIF):
        # File definining the interface doesn't exist
        accessor = ''
    return accessor

  # debug registers
  def numDebugRegs(self):
    return carbonCfgNumRegisters(self._cfg)
  def debugRegs(self): return self.__debugRegs

  # debug memories
  def numDebugMemories(self): return len(self.__debugMemories)
  def debugMemories(self): return self.__debugMemories

  def useLegacyMemories(self):
    return self.__legacyMemories

  # If this returns true the model should use add debuggable point functions to the component(s)
  def useDebuggablePoint(self):
    return self.__procInfo.debuggablePoint()

  def useStaticScheduling(self):
    return carbonCfgGetUseStaticScheduling(self._cfg)

# the Config class contains general information about components,sub-componets,... 
# it is a representation of the data model from the ccfg file.

class Config(ComponentBase):
  def __init__(self, ccfgFile, xtorDefFiles, mode):
    # create a CFG context
    cfg = carbonCfgCreate()

    # load the system xtor definitions
    if (mode == eCarbonXtorsMaxsim) or (mode == eCarbonXtorsSystemC):
      if carbonCfgReadXtorLib(cfg, mode) != eCarbonCfgSuccess:
        msg = carbonCfgGetErrmsg(cfg)
        raise ConfigError(msg)

      # read in all of the user transactor definition files
      for defFile in xtorDefFiles:
        if carbonCfgReadXtorDefinitions(cfg, defFile) != eCarbonCfgSuccess:
          msg = carbonCfgGetErrmsg(cfg)
          raise ConfigError(msg)

    elif mode == eCarbonXtorsCoware:
      envCowareIP = os.environ.get('COWAREIPDIR')
      envCowareHome = os.environ.get('COWAREHOME')

      if envCowareIP != None or envCowareHome != None:
        if carbonCfgReadXtorLib(cfg, eCarbonXtorsCoware) != eCarbonCfgSuccess:
          msg = carbonCfgGetErrmsg(cfg)
          raise ConfigError(msg)

      # read in all of the coware user transactor definition files
      for defFile in xtorDefFiles:
        if carbonCfgReadCowareXtorDefinitions(cfg, defFile) != eCarbonCfgSuccess:
          msg = carbonCfgGetErrmsg(cfg)
          raise ConfigError(msg)

    else:
      raise ConfigError("Unknown config mode '%d'" % mode)
      
    # Print any Info or Warning messages
    self.message(carbonCfgGetErrmsg(cfg))

    # read the user ccfg file
    success = carbonCfgRead(cfg, ccfgFile)
    msg = carbonCfgGetErrmsg(cfg)
    if success  != eCarbonCfgSuccess:
      raise ConfigError(msg)
    else :
      self.message(carbonCfgGetErrmsg(cfg))
      
    # Note that we init the component base after reading the ccfg
    # files above. This is because the component base accesses the
    # data populated by the above code.
    #
    # This means that self.cfg() only works after this point
    ComponentBase.__init__(self, cfg)

    self.__clocks = []
    self.__scClocks = []
    self.__disconnects = []
    self.__inputs = []
    self.__nullInputs = []
    self.__nullOutputs = []
    self.__outputs = []
    self.__inouts = []
    self.__ports = []
    self.__resets = []
    self.__ties = []
    self.__portModes = {}
    self.__parameters = []
    self.__namespaceUsed = ''
    
    # find all of the transactor instances
    self.__xtors = []
    for i in range(carbonCfgNumXtorInstances(self.cfg())):
      xtorInstance = carbonCfgGetXtorInstance(self.cfg(), i)
      xtorType = carbonCfgXtorInstanceGetXtor(xtorInstance)
      typeName = carbonCfgXtorGetName(xtorType)
      if typeName == "Null_Input":
        self.__nullInputs.append(Transactor(self.cfg(), xtorInstance))
      elif typeName == "Null_Output":
        self.__nullOutputs.append(Transactor(self.cfg(), xtorInstance))
      else:
        self.__xtors.append(Transactor(self.cfg(), xtorInstance))

    # group ports into lists by type
    for i in range(carbonCfgNumRTLPorts(self.cfg())):
      rtlPort = carbonCfgGetRTLPort(self.cfg(), i)
      rtlName = carbonCfgRTLPortGetName(rtlPort)
      width = carbonCfgRTLPortGetWidth(rtlPort)
      if carbonCfgRTLPortNumConnections(rtlPort) == 0:
        self.__disconnects.append(rtlName)

      for j in range(carbonCfgRTLPortNumConnections(rtlPort)):
        conn = carbonCfgRTLPortGetConnection(rtlPort, j)
        connType = carbonCfgRTLConnectionGetType(conn)

        if connType == eCarbonCfgESLPort:
          eslPort = carbonCfgCastESLPort(conn)
          eslType = carbonCfgESLPortGetType(eslPort)
          eslName = carbonCfgESLPortGetName(eslPort)
          eslTypeDef = carbonCfgESLPortGetTypeDef(eslPort)
          eslPortMode = carbonCfgESLPortGetMode(eslPort)
          eslXtorInstName = carbonCfgESLPortGetParamXtorInstName(eslPort)
          eslParamName = carbonCfgESLPortGetParamName(eslPort)
          #print "eslName %s %s" % (eslName,eslParamName)
          
          paramName = ''
          if (eslXtorInstName != ''): paramName = eslXtorInstName + ' ' + eslParamName
          else: paramName = eslParamName

          self.__portModes[eslName] = eslPortMode

          expr = carbonCfgESLPortGetExpr(eslPort)
          port = None
          if (eslType == eCarbonCfgESLInput):
            port = Input(eslName, rtlName, width, expr, eslTypeDef, eslPortMode, paramName)
            self.__inputs.append(port)
          elif (eslType == eCarbonCfgESLOutput):
            port = Output(eslName, rtlName, width, expr, eslTypeDef, eslPortMode)
            self.__outputs.append(port)
          elif (eslType == eCarbonCfgESLInout):
            port = Inout(eslName, rtlName, width, expr, eslTypeDef, eslPortMode, paramName)
            self.__inouts.append(port)
          if port != None:
            self.__ports.append(port) # Ordered set of ports

        elif connType == eCarbonCfgClockGen:
          clockGen = carbonCfgCastClockGen(conn)
          self.__clocks.append(Clock(rtlName, clockGen))

        elif connType == eCarbonCfgSystemCClock:
          systemCClock = carbonCfgCastSystemCClock(conn)
          self.__scClocks.append(ScClock(rtlName, systemCClock))

        elif connType == eCarbonCfgResetGen:
          resetGen = carbonCfgCastResetGen(conn)
          self.__resets.append(Reset(rtlName, resetGen))

        elif connType == eCarbonCfgTie:
          tie = carbonCfgCastTie(conn)
          self.__ties.append(Tie(rtlName, tie, width))

        elif connType == eCarbonCfgTieParam:
          tie = carbonCfgCastTieParam(conn)
          self.__ties.append(TieParam(rtlName, tie, width))

        elif connType == eCarbonCfgXtorConn:
          xtorConn = carbonCfgCastXtorConn(conn)
          xtorPort = carbonCfgXtorConnGetPort(xtorConn)
          xtorPortType = carbonCfgXtorPortGetType(xtorPort)
          eslName = carbonCfgXtorPortGetName(xtorPort)
          width = carbonCfgXtorPortGetSize(xtorPort)
          clockSense = carbonCfgXtorPortIsClockSensitive(xtorPort)
          if clockSense == 0:
            clockSensitivity = False
          else:
            clockSensitivity = True
          typeDef = carbonCfgXtorPortGetTypeDef(xtorPort)
          if typeDef == "":
            typeDef = carbonCfgXtorConnGetTypeDef(xtorConn)
          xtorInstance = carbonCfgXtorConnGetXtorInstance(xtorConn)
          expr = carbonCfgXtorConnGetExpr(xtorConn)
          xtorInstanceName = carbonCfgXtorInstanceGetName(xtorInstance)
          xtor = self.getXtor(xtorInstanceName)
          portMode = self.portMode(eslName)
          rtlWidth = carbonCfgRTLPortGetWidth(rtlPort)
          eventName = carbonCfgXtorPortGetEventName(xtorPort)
          xport = TransactorPort(eslName, rtlName, width, xtor, expr, portMode,
                                 rtlWidth, clockSensitivity, typeDef, eventName, xtorPort)
          if xtorPortType == eCarbonCfgRTLInput:
            xtor.addInput(xport)
          elif xtorPortType == eCarbonCfgRTLOutput:
            xtor.addOutput(xport)
          else:
            xtor.addUnconnected(xport)

    # list of sub component
    self.__subComponents = []
    for i in range(carbonCfgNumSubComponents(self.cfg())):
      comp = carbonCfgGetSubComponent(self.cfg(), i)
      self.__subComponents.append(SubComponent(comp))
    
    # Should this component be generated as a standalone component instead of a SoC Designer component?
    if carbonCfgIsStandAloneComp(self.cfg()):
      self.__standAlone = True
    else:
      self.__standAlone = False

    # Should sub components be generated using the top component as a prefix (to uniquify it)
    if carbonCfgIsSubComponentFullName(self.cfg()):
      self.__subComponentFullName = True
    else:
      self.__subComponentFullName = False

    # sort lists by name
    for portList in (self.__inputs, self.__clocks, self.__scClocks, self.__resets,
                     self.__outputs, self.__inouts, self.__xtors,
                     self.__nullInputs, self.__nullOutputs):
      portList.sort(lambda a, b: cmp(a.name(), b.name()))

    # sort lists by carbonName
    self.__ties.sort(lambda a, b: cmp(a.carbonName(), b.carbonName()))    

    # sort the disconnects
    self.__disconnects.sort()

    # list top level parameters
    for param in range(carbonCfgNumParams(self.cfg())):
      self.__parameters.append(Parameter(carbonCfgGetParam(self.cfg(), param)))

  # Used to output notes and warnings from the ccfg reader
  # by print to stdout. It can be overridden to output the
  # message to the appropriate channel.
  def message(self, msg):
    if msg != '':
      print msg
  
  def write(self, filename):
    carbonCfgWrite(self.cfg(), filename)
    
  def portMode(self,eslPortName):
    if eslPortName in self.__portModes:
      return self.__portModes[eslPortName]
    else:
      return eCarbonCfgESLPortControl
  
  # component name
  def compName(self):
    # There is a bug in carbonCfgGetCompName where it returns a full file path.
    # Just grab the last word.
    import os.path
    name = os.path.split(carbonCfgGetCompName(self.cfg()))[-1]

    # Another bug - strip 'lib' off of the name
    if name.startswith('lib'):
      name = name[3:]
    return name

  # Return the unaltered component name (for SystemC)
  def compNameUnaltered(self):
    return carbonCfgGetCompName(self.cfg())

  # Unique namespace for the component.  This is used when SystemC
  # ports are present.
  def namespaceName(self):
    return 'namespace_%s' % self.compName()
  
  def namespaceUsed(self):
    return self.__namespaceUsed

  def setNamespaceUsed(self, name):
    self.__namespaceUsed = name

  # name of top level module
  def topModuleName(self):
    return carbonCfgGetTopModuleName(self.cfg())

  # Componentversion
  def version(self):
    return carbonCfgGetVersion(self.cfg())

  # Component documentation file
  def docFile(self):
    return carbonCfgGetDocFile(self.cfg())

  def cxxFlags(self):
    return carbonCfgGetCxxFlags(self.cfg())

  def linkFlags(self):
    return carbonCfgGetLinkFlags(self.cfg())

  # Iterate over source files
  def sourceFiles(self):
    for file in carbonCfgGetSourceFiles(self.cfg()).split(';'):
      if file != "":
        yield file

  # Iterate over include files
  def includeFiles(self):
    for file in carbonCfgGetIncludeFiles(self.cfg()).split(';'):
      if file != "":
        yield file
    
  # name of iodb file
  def ioDbFile(self):
    return carbonCfgGetIODBFile(self.cfg())

  # list of ports in order of how they were created (matches RTL order)
  def ports(self): return self.__ports

  # sorted list of input ports
  def inputs(self): return self.__inputs

  # sorted list of output ports
  def outputs(self): return self.__outputs

  # sorted list of inout ports
  def inouts(self): return self.__inouts

  # sorted list of clock ports
  def clocks(self): return self.__clocks

  # sorted list of scClock ports
  def scClocks(self): return self.__scClocks

  # sorted list of reset ports
  def resets(self): return self.__resets
  
  # sorted list of disconnects
  def disconnects(self): return self.__disconnects

  # The number of transactors
  def numTransactors(self):
    return len(self.__xtors)

  # sorted list of xtors
  def transactors(self): return self.__xtors
  def getXtor(self, name):
    for xtor in self.__xtors:
      if xtor.name() == name:
        return xtor

  # Null input and output ports
  def nullInputs(self): return self.__nullInputs
  def nullOutputs(self): return self.__nullOutputs

  # ties
  def ties(self): return self.__ties

  # sub components
  def numSubComponents(self):
    return len(self.__subComponents)
  def subComponents(self): return self.__subComponents
  
  def waveType(self):
    return carbonCfgGetWaveType(self.cfg())

  def waveFilename(self):
    return carbonCfgGetWaveFilename(self.cfg())

  # profiling
  def numProfileStreams(self):
    return carbonCfgNumPStreams(self.cfg())
  def profileStreams(self):
    return [ProfileStream(i, carbonCfgGetPStream(self.cfg(), i))
            for i in range(carbonCfgNumPStreams(self.cfg()))]

  # parameters
  def parameters(self):
    return self.__parameters

  # Return whether this component needs the model kit library
  def requiresMkLibrary(self):
    # If we're using the ELF loader we always need libmodelkit
    if self.elfLoader() != None: return True
    for comp in self.__subComponents:
      if comp.elfLoader() != None: return True

    # Otherwise get the value from the .ccfg
    if carbonCfgGetRequiresMkLibrary(self.cfg()):
      return True
    return False

  def useVersionedMkLibrary(self):
    return carbonCfgGetUseVersionedMkLibrary(self.cfg())

  def standAloneComponent(self):
    return self.__standAlone

  def setStandAloneComponent(self, val):
    self.__standAlone = val
  
  def subComponentFullName(self):
    return self.__subComponentFullName

  def setSubComponentFullName(self, val):
    self.__subComponentFullName = val
  
  # Return text to add the definition of $CARBON_MK_HOME if this ccfg
  # requires the model kit library
  def libmkHomeMakefileText(self, os):
    code = ''
    if self.requiresMkLibrary() or self.standAloneComponent():
      if os == "unix":
        code += '''\
ifndef CARBON_MK_HOME
CARBON_MK_HOME = $(CARBON_PROJECT)/ModelKitLibrary
endif
'''
      elif os == "windows":
        code += '''\
!IFNDEF CARBON_MK_HOME
CARBON_MK_HOME = ../../ModelKitLibrary
!ENDIF
'''
    return code

class Cadi(CustomCodeContainer):
  def __init__(self, cadi, compName):
    CustomCodeContainer.__init__(self)
    self.__cadi = cadi
    self.__compName = compName

    # Compute the list of cadi custom code sections
    self.__customCodes = CustomCodeContainer()
    for i in range(carbonCfgCadiNumCustomCodes(self.__cadi)):
      cc = carbonCfgCadiGetCustomCode(self.__cadi, i)
      section = carbonCfgCustomCodeGetSectionString(cc)
      position = carbonCfgCustomCodeGetPosition(cc)
      code = carbonCfgCustomCodeGetCode(cc)
      self.addCustomCode(section, position, code)

  def disassemblerName(self):
    return carbonCfgCadiGetDisassemblerName(self.__cadi)
  
  # Print unused custom codes
  def printUnusedCodes(self, output):
    # Start with component unused custom codes
    CustomCodeContainer.printUnusedCodes(self, output,
                                         "%s::Cadi" % self.__compName)

class SubComponent(ComponentBase):
  def __init__(self, comp):
    ComponentBase.__init__(self, comp)
    
  def compName(self):
    return carbonCfgGetCompName(self.cfg())

class Port:
  def __init__(self, eslName, rtlName, width, expr, typeDef, portMode, paramName=''):
    self._eslName = eslName
    self._rtlName = rtlName
    self._width = width
    self._expr = expr
    self._typeDef = typeDef
    self._mode = portMode
    self._paramName = paramName
    
    # should be able to get rtl port from ESL port, but not sure about
    # cross-language upcasting

  def name(self): return self._eslName
  def carbonName(self): return self._rtlName
  def shortName(self):
    # Historically, we've been using the ESL name to build the net ID
    # name, which isn't really right.  We should be using the RTL name
    # (shortened).  So that existing user code sections don't break,
    # we'll continue to use the ESL name, if it exists.
    # Generated clocks and resets don't have an ESL name anymore,
    # although they used to.  If the ESL name is null, fall back
    # on the RTL name.
    name = self.name()
    if name == None:
      name = self.leafName()
    return name
  def leafName(self): return self.carbonName().split('.')[-1]
  def width(self): return self._width
  def carbonNetId(self): return 'mCarbonNet_' + self.shortName()
  def expr(self): return self._expr
  def typedef(self): return self._typeDef
  def mode(self): return self._mode
  def paramName(self) : return self._paramName

class Input(Port):
  def __init__(self, eslName, rtlName, width, expr, typeDef, portMode, paramName):
    Port.__init__(self, eslName, rtlName, width, expr, typeDef, portMode, paramName)

class Output(Port):
  def __init__(self, eslName, rtlName, width, expr, typeDef, portMode):
    Port.__init__(self, eslName, rtlName, width, expr, typeDef, portMode)

class Inout(Port):
  def __init__(self, eslName, rtlName, width, expr, typeDef, portMode, paramName):
    Port.__init__(self, eslName, rtlName, width, expr, typeDef, portMode, paramName)

class Clock(Port):
  def __init__(self, rtlName, clockGen):
    Port.__init__(self, None, rtlName, 1, "", "", eCarbonCfgESLPortClock)
    self._clockGen = clockGen
  def initialValue(self):
    return carbonCfgClockGetInitialVal(self._clockGen)
  def delay(self):
    return carbonCfgClockGetDelay(self._clockGen)
  def clockCycles(self):
    return carbonCfgClockGetClockCycles(self._clockGen)
  def compCycles(self):
    return carbonCfgClockGetCompCycles(self._clockGen)
  def dutyCycle(self):
    return carbonCfgClockGetDutyCycle(self._clockGen)

class ScClock(Port):
  def __init__(self, rtlName, scClock):
    Port.__init__(self, None, rtlName, 1, "", "", eCarbonCfgSystemCClock)
    self.__scClock = scClock
  def period(self):
    return carbonCfgSystemCClockPeriod(self.__scClock)
  def periodUnits(self):
    return carbonCfgSystemCClockPeriodUnits(self.__scClock)
  def dutyCycle(self):
    return carbonCfgSystemCClockDutyCycle(self.__scClock)
  def startTime(self):
    return carbonCfgSystemCClockStartTime(self.__scClock)
  def startTimeUnits(self):
    return carbonCfgSystemCClockStartTimeUnits(self.__scClock)
  def initialValue(self):
    return carbonCfgSystemCClockInitialValue(self.__scClock)

class Reset(Port):
  def __init__(self, rtlName, resetGen):
    Port.__init__(self, None, rtlName, 1, "", "", eCarbonCfgESLPortReset)
    self._resetGen = resetGen
  def activeValueLow(self):
    return carbonCfgResetGetActiveValueLow(self._resetGen)
  def activeValueHigh(self):
    return carbonCfgResetGetActiveValueHigh(self._resetGen)
  def inactiveValueLow(self):
    return carbonCfgResetGetInactiveValueLow(self._resetGen)
  def inactiveValueHigh(self):
    return carbonCfgResetGetInactiveValueHigh(self._resetGen)
  def clockCycles(self):
    return carbonCfgResetGetClockCycles(self._resetGen)
  def compCycles(self):
    return carbonCfgResetGetCompCycles(self._resetGen)
  def cyclesBefore(self):
    return carbonCfgResetGetCyclesBefore(self._resetGen)
  def cyclesAsserted(self):
    return carbonCfgResetGetCyclesAsserted(self._resetGen)
  def cyclesAfter(self):
    return carbonCfgResetGetCyclesAfter(self._resetGen)

class Tie(Port):
  def __init__(self, rtlName, tie, width):
    Port.__init__(self, rtlName.split('.')[-1], rtlName, width, "", "", eCarbonCfgESLPortControl)
    self._tie = tie
  def type(self):
    return 'Tie'
  def numWords(self):
    return carbonCfgTieGetNumWords(self._tie)
  def getWord(self, i):
    return carbonCfgTieGetWord(self._tie, i)
  def getParam(self):
    return ''

class TieParam(Tie):
  def __init__(self, rtlName, tie, width):
    Tie.__init__(self, rtlName, tie, width)
    self._tie = tie
  def type(self):
    return 'TieParam'
  def numWords(self):
    return (self.width()+31)/32
  def getWord(self, i):
    return 0
  def getParam(self):
    return carbonCfgTieParamGetFullParam(self._tie)

class DebugRegLocConstant:
  def __init__(self, loc):
    self.__loc = loc

  def value(self):
    return carbonCfgRegisterLocConstantGetValue(self.__loc)

class DebugRegLocRegister:
  def __init__(self, loc):
    self.__loc = loc

  def path(self):
    return carbonCfgRegisterLocRegGetPath(self.__loc)

  def hasRange(self):
    if carbonCfgRegisterLocRegGetHasRange(self.__loc):
      return True
    return False

  def left(self):
    return carbonCfgRegisterLocRegGetLeft(self.__loc)

  def right(self):
    return carbonCfgRegisterLocRegGetRight(self.__loc)

  def lsb(self):
    return carbonCfgRegisterLocRegGetLSB(self.__loc)

  def msb(self):
    return carbonCfgRegisterLocRegGetMSB(self.__loc)

class DebugRegLocArray:
  def __init__(self, loc):
    self.__loc = loc

  def path(self):
    return carbonCfgRegisterLocArrayGetPath(self.__loc)

  def hasRange(self):
    if carbonCfgRegisterLocArrayGetHasRange(self.__loc):
      return True
    return False

  def left(self):
    return carbonCfgRegisterLocArrayGetLeft(self.__loc)

  def right(self):
    return carbonCfgRegisterLocArrayGetRight(self.__loc)

  def lsb(self):
    return carbonCfgRegisterLocArrayGetLSB(self.__loc)

  def msb(self):
    return carbonCfgRegisterLocArrayGetMSB(self.__loc)

  def index(self):
    return carbonCfgRegisterLocArrayGetIndex(self.__loc)

class DebugRegLocUser:
  def __init__(self, loc):
    self.__loc = loc

class DebugRegLoc:
  def __init__(self, loc):
    self.__loc = loc

  def type(self):
    if (carbonCfgRegisterLocGetType(self.__loc) == eCfgRegLocConstant):
      return 'constant'
    if (carbonCfgRegisterLocGetType(self.__loc) == eCfgRegLocRegister):
      return 'register'
    if (carbonCfgRegisterLocGetType(self.__loc) == eCfgRegLocArray):
      return 'array'
    if (carbonCfgRegisterLocGetType(self.__loc) == eCfgRegLocUser):
      return 'user'
    return None

  def castConstant(self):
    return DebugRegLocConstant(carbonCfgRegisterLocCastConstant(self.__loc))
  
  def castRegister(self):
    return DebugRegLocRegister(carbonCfgRegisterLocCastReg(self.__loc))
  
  def castArray(self):
    return DebugRegLocArray(carbonCfgRegisterLocCastArray(self.__loc))

  def castUser(self):
    return DebugRegLocUser(carbonCfgRegisterLocCastUser(self.__loc))
  
class DebugRegField:
  def __init__(self, field):
    self.__field = field
    self.__loc = DebugRegLoc(carbonCfgRegisterFieldGetLoc(field))

  def name(self):
    return carbonCfgRegisterFieldGetName(self.__field)

  def high(self):
    return carbonCfgRegisterFieldGetHigh(self.__field)

  def low(self):
    return carbonCfgRegisterFieldGetLow(self.__field)

  def access(self):
    if (carbonCfgRegisterFieldGetAccess(self.__field) == eCfgRegAccessRW):
      return 'read-write'
    if (carbonCfgRegisterFieldGetAccess(self.__field) == eCfgRegAccessRO):
      return 'read-only'
    return 'write-only'

  def loc(self):
    return self.__loc

class DebugReg:
  def __init__(self, reg):
    self.__reg = reg
    self.__fields = []
    for i in range(carbonCfgRegisterNumFields(reg)):
      self.__fields.append(DebugRegField(carbonCfgRegisterGetField(reg, i)))

    self.__customCodes = CustomCodeContainer()
    for i in range(carbonCfgRegisterNumCustomCodes(reg)):
      cc = carbonCfgRegisterGetCustomCode(reg, i)
      section = carbonCfgCustomCodeGetSectionString(cc)
      position = carbonCfgCustomCodeGetPosition(cc)
      code = carbonCfgCustomCodeGetCode(cc)
      self.__customCodes.addCustomCode(section, position, code)

  def name(self):
    return carbonCfgRegisterGetName(self.__reg)

  def putGroup(self,cfg,groupname):
    carbonCfgRegisterPutGroup(cfg.cfg(), self.__reg, groupname)

  def group(self):
    return carbonCfgRegisterGetGroup(self.__reg)

  def width(self):
    return carbonCfgRegisterGetWidth(self.__reg)

  def comment(self):
    return carbonCfgRegisterGetComment(self.__reg)

  def radix(self):
    return carbonCfgRegisterGetRadix(self.__reg)

  def eslPort(self):
    return carbonCfgRegisterGetPort(self.__reg)

  def bigEndian(self):
    return carbonCfgRegisterIsBigEndian(self.__reg)

  def pcReg(self):
    return carbonCfgRegisterIsPcReg(self.__reg)

  def offset(self):
    return carbonCfgRegisterGetOffset(self.__reg)
  
  def fields(self):
    return self.__fields;

  def getCustomCodePre(self, section):
    return self.__customCodes.getCustomCodePre(section)

  def getCustomCodePost(self, section):
    return self.__customCodes.getCustomCodePost(section)

  def printUnusedCodes(self, output):
    self.__customCodes.printUnusedCodes(output, "Register")

class MemoryLocRTL:
  def __init__(self, loc):
    self._loc = loc

  def path(self):
    return carbonCfgMemoryLocRTLGetPath(self._loc)

  def addrOffset(self):
    return carbonCfgMemoryLocRTLGetStartWordOffset(self._loc)

  def endWord(self):
    return carbonCfgMemoryLocRTLGetEndWordOffset(self._loc)

  def msb(self):
    return carbonCfgMemoryLocRTLGetMsb(self._loc)
  
  def lsb(self):
    return carbonCfgMemoryLocRTLGetLsb(self._loc)
  
  def width(self):
    return carbonCfgMemoryLocRTLGetBitWidth(self._loc)
    
class MemoryLocPort:
  def __init__(self, loc):
    self._loc = loc

  def port(self):
    return carbonCfgMemoryLocPortGetPortName(self._loc)

  def hasFixedAddr(self):
    if carbonCfgMemoryLocPortHasFixedAddr(self._loc):
      return True
    return False

class MemoryLocUser:
  def __init__(self, loc):
    self._loc = loc

  def name(self):
    return carbonCfgMemoryLocUserGetName(self._loc)
  
class MemoryLoc:
  def __init__(self, loc):
    self._loc = loc

  def type(self):
    if (carbonCfgMemoryLocGetType(self._loc) == eCfgMemLocRTL):
      return 'rtl'
    if (carbonCfgMemoryLocGetType(self._loc) == eCfgMemLocPort):
      return 'port'
    if (carbonCfgMemoryLocGetType(self._loc) == eCfgMemLocUser):
      return 'user'

  def castRTL(self):
    return MemoryLocRTL(carbonCfgMemoryLocCastRTL(self._loc))
  
  def castPort(self):
    return MemoryLocPort(carbonCfgMemoryLocCastPort(self._loc))
  
  def castUser(self):
    return MemoryLocUser(carbonCfgMemoryLocCastUser(self._loc))

  def displayStartWord(self):
    return carbonCfgMemoryLocGetDisplayStartWordOffset(self._loc)
    
  def displayEndWord(self):
    return carbonCfgMemoryLocGetDisplayEndWordOffset(self._loc)
    
  def displayMsb(self):
    return carbonCfgMemoryLocGetDisplayMsb(self._loc)

  def displayLsb(self):
    return carbonCfgMemoryLocGetDisplayLsb(self._loc)

class MemoryBlock(CustomCodeContainer):
  def __init__(self, block):
    CustomCodeContainer.__init__(self)
    self._block = block
    self._locs = []

    for i in range(carbonCfgMemoryBlockNumLocs(self._block)):
      self._locs.append(MemoryLoc(carbonCfgMemoryBlockGetLoc(self._block, i)))

    for i in range(carbonCfgMemoryBlockNumCustomCodes(block)):
      cc = carbonCfgMemoryBlockGetCustomCode(block, i)
      section = carbonCfgCustomCodeGetSectionString(cc)
      position = carbonCfgCustomCodeGetPosition(cc)
      code = carbonCfgCustomCodeGetCode(cc)
      self.addCustomCode(section, position, code)

  def index(self):
    return 0 # FIXME
  
  def name(self):
    return carbonCfgMemoryBlockGetName(self._block)
    
  def baseAddrLo(self):
    return carbonCfgMemoryBlockGetBaseLo(self._block)

  def baseAddrHi(self):
    return carbonCfgMemoryBlockGetBaseHi(self._block)

  def baseAddr(self):
    return carbonCfgMemoryBlockGetBase(self._block)
  
  def sizeLo(self):
    return carbonCfgMemoryBlockGetSizeLo(self._block)

  def sizeHi(self):
    return carbonCfgMemoryBlockGetSizeHi(self._block)

  def size(self):
    return carbonCfgMemoryBlockGetSize(self._block)
  
  def width(self):
    return carbonCfgMemoryBlockGetWidth(self._block)

  def hasFixedAddr(self):
    if len(self.locs()) > 0:
      return ((self.locs()[0].type() == 'rtl') or
              (self.locs()[0].type() == 'user') or
              (self.locs()[0].type() == 'port' and self.locs()[0].castPort().hasFixedAddr()))
    return False
  
  def locs(self):
    return self._locs
  
  def numLocs(self):
    return len(self._locs)

class MemorySystemAddressPort:
  def __init__(self, mem, portIndex):
    self._mem = mem
    self._portIndex = portIndex
  
  def name(self):
    return carbonCfgMemoryGetSystemAddressESLPortName(self._mem, self._portIndex);
    
  def baseAddress(self):
    return carbonCfgMemoryGetSystemAddressESLPortBaseAddress(self._mem, self._portIndex);
  
class Memory(CustomCodeContainer):
  def __init__(self, mem):
    CustomCodeContainer.__init__(self)
    self._mem = mem
    self.__index = -1
    self.__blocks = []
    self.__systemAddressPorts = []

    for i in range(carbonCfgMemoryNumSystemAddressESLPorts(self._mem)):
      self.__systemAddressPorts.append(MemorySystemAddressPort(self._mem, i))
      
    for i in range(carbonCfgMemoryNumBlocks(self._mem)):
      self.__blocks.append(MemoryBlock(carbonCfgMemoryGetBlock(self._mem, i)))
                           
    for i in range(carbonCfgMemoryNumCustomCodes(mem)):
      cc = carbonCfgMemoryGetCustomCode(mem, i)
      section = carbonCfgCustomCodeGetSectionString(cc)
      position = carbonCfgCustomCodeGetPosition(cc)
      code = carbonCfgCustomCodeGetCode(cc)
      self.addCustomCode(section, position, code)

  def name(self):
    return carbonCfgMemoryGetName(self._mem)

  def width(self):
    return carbonCfgMemoryGetWidth(self._mem)

  def isProgramMemory(self):
    return carbonCfgMemoryGetProgramMemory(self._mem) != 0

  def comment(self):
    return carbonCfgMemoryGetComment(self._mem)

  def initFile(self):
    return carbonCfgMemoryGetInitFile(self._mem)

  def initFileType(self):
    if carbonCfgMemoryGetReadmemType(self._mem) == eCarbonCfgReadmemb:
      return 'readmemb'
    else:
      return 'readmemh'

  def initType(self):
    memInitType = carbonCfgMemoryGetInitType(self._mem)
    if memInitType == eCarbonCfgMemInitNone:
      return 'MemInitNone'
    elif memInitType == eCarbonCfgMemInitProgPreload:
      return 'MemInitProgPreload'
    elif memInitType == eCarbonCfgMemInitReadmem:
      return 'MemInitReadmem'
    else:
      raise ConfigError('Unsupported mem init type')
    
  def hasInitialization(self):
    if carbonCfgMemoryGetInitType(self._mem) != eCarbonCfgMemInitNone:
      return 1
    else:
      return 0

  def displayAtZero(self):
    if carbonCfgMemoryGetDisplayAtZero(self._mem):
      return True
    return False
  
  def bigEndian(self):
    if carbonCfgMemoryGetBigEndian(self._mem):
      return True
    return False
  
  def isProgramPreload(self):
    if carbonCfgMemoryGetInitType(self._mem) == eCarbonCfgMemInitProgPreload:
      return 1
    else:
      return 0

  def programPreloadEslPort(self):
    return carbonCfgMemoryGetProgPreloadEslPort(self._mem)

  def eslPort(self):
    return carbonCfgMemoryGetProgPreloadEslPort(self._mem)

  def isReadmemInit(self):
    if carbonCfgMemoryGetInitType(self._mem) == eCarbonCfgMemInitReadmem:
      return 1
    else:
      return 0
      
  def systemAddressPorts(self):
    return self.__systemAddressPorts
    
  def blocks(self):
    return self.__blocks
  
  def numBlocks(self):
    return len(self.__blocks)

  def signal(self):
    return carbonCfgMemoryGetSignal(self._mem)

  def maxAddrs(self):
    return carbonCfgMemoryGetMaxAddrs(self._mem)

  def numCycles(self):
    return 0

  def readWrite(self):
    return 2

  def baseAddrHi(self):
    return carbonCfgMemoryGetBaseAddrHi(self._mem)

  def baseAddrLo(self):
    return carbonCfgMemoryGetBaseAddrLo(self._mem)

  def putIndex(self, index):
    self.__index = index

  def index(self):
    return self.__index

class Parameter:
  def __init__(self, paramInstance):
    self.__paramInstance = paramInstance
    self.__enumChoices = []
    for i in range(carbonCfgParamNumEnumChoices(paramInstance)):
      enumChoice = carbonCfgParamGetEnumChoice(paramInstance, i)
      self.__enumChoices.append(enumChoice)

  def name(self):
    return carbonCfgParamGetName(self.__paramInstance)

  def size():
    return carbonCfgParamGetSize(self.__paramInstance)
    
  def value(self):
    return carbonCfgParamGetValue(self.__paramInstance)

  def defaultValue(self):
    return carbonCfgParamGetDefaultValue(self.__paramInstance)

  def type(self):
    return carbonCfgParamGetType(self.__paramInstance)
  
  def scope(self):
    return carbonCfgParamGetFlag(self.__paramInstance)

  def description(self):
    return carbonCfgParamGetDescription(self.__paramInstance)

  def enumChoices(self):
    return self.__enumChoices


class TransactorPort(Port):
  def __init__(self, eslName, rtlName, width, xtor, expr, portMode, rtlWidth, clockSensitive, typeDef,
               eventName, xtorPort):
    Port.__init__(self, eslName, rtlName, width, expr, typeDef, portMode)
    self.__xtor = xtor
    self.__rtlWidth = rtlWidth
    self.__isClockSensitive = clockSensitive
    self.__eventName = eventName
    self.__phaseNames = []
    for i in range(carbonCfgXtorPortNumPhaseNames(xtorPort)):
      self.__phaseNames.append(carbonCfgXtorPortGetPhaseName(xtorPort, i))
    self.__widthExpr = carbonCfgXtorPortGetWidthExpr(xtorPort)
    
  def carbonNetId(self):
    return 'mCarbonNet_' + self.__xtor.name() + '_' + self.name()

  def numWords(self):
    return (self.width() + 31) / 32;

  def rtlWidth(self):
    return self.__rtlWidth

  def numRtlWords(self):
    return (self.__rtlWidth + 31) / 32

  def clockSensitive(self):
    return self.__isClockSensitive

  def eventName(self):
    return self.__eventName

  def phaseNames(self):
    return self.__phaseNames

  def widthExpr(self):
    return self.__widthExpr

class TransactorSystemCPort:
  def __init__(self, port):
    self.__name        = carbonCfgXtorSystemCPortGetName(port)
    self.__type        = carbonCfgXtorSystemCPortGetType(port)
    self.__interface   = carbonCfgXtorSystemCPortGetInterface(port)
    self.__description = carbonCfgXtorSystemCPortGetDescription(port)

  def name(self):
    return self.__name
  def type(self):
    return self.__type
  def interface(self):
    return self.__interface
  def description(self):
    return self.description()

class TemplateArg:
  def __init__(self, cfgArg):
    self.__cfgArg = cfgArg

  def isClass(self):
    return carbonCfgXtorTemplateArgGetType(self.__cfgArg) == "class"

  def isPortWidth(self):
    return carbonCfgXtorTemplateArgGetType(self.__cfgArg) == "port-width"

  def isText(self):
    return carbonCfgXtorTemplateArgGetType(self.__cfgArg) == "text"

  def value(self):
    return carbonCfgXtorTemplateArgGetValue(self.__cfgArg)

  def default(self):
    return carbonCfgXtorTemplateArgGetDefault(self.__cfgArg)

  def busDataWidth(self):
    if self.isPortWidth():
      portWidthArg = carbonCfgXtorCastPortWidthTemplateArg(self.__cfgArg)
      return carbonCfgXtorPortWidthTemplateArgGetBusDataWidth(portWidthArg) != 0
    return False

class Transactor:
  def __init__(self, cfg, xtorInstance):
    self.__cfg = cfg
    self._xtorInstance = xtorInstance
    self.__className = ""
    self.__inputs = []
    self.__outputs = []
    self.__unconnected = []
    self.__params = []
    self.__systemCPorts = []
    self.__includeFiles = []
    self.__incPaths = []
    self.__libFiles = []
    self.__libPaths = []
    self.__flags = {}
    
    for i in range(carbonCfgXtorInstanceNumConnections(self._xtorInstance)):
      xtorConn = carbonCfgXtorInstanceGetConnection(self._xtorInstance, i)
      rtlPort = carbonCfgXtorConnGetRTLPort(xtorConn)

      xtorPort = carbonCfgXtorConnGetPort(xtorConn)
      xtorPortType = carbonCfgXtorPortGetType(xtorPort)
      eslName = carbonCfgXtorPortGetName(xtorPort)
      width = carbonCfgXtorPortGetSize(xtorPort)
      xtorInstance = carbonCfgXtorConnGetXtorInstance(xtorConn)
      expr = carbonCfgXtorConnGetExpr(xtorConn)
      xtorInstanceName = carbonCfgXtorInstanceGetName(xtorInstance)
      portMode = eCarbonCfgESLPortControl

      # build up the list of unconnected ports here
      if rtlPort == None:
        # Note we use the same width for the rtl and xtor port. We may
        # want to pass an rtl width of 0
        xport = TransactorPort(eslName, eslName, width, self, expr, portMode,
                               width, False, '', '', xtorPort)
        self.addUnconnected(xport)

    # List of SystemC Ports
    xtor = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    for i in range(carbonCfgXtorNumSystemCPorts(xtor)):
      sysCPort = carbonCfgXtorGetSystemCPort(xtor, i)
      self.__systemCPorts.append(TransactorSystemCPort(sysCPort))
                                
    # Build list of non-hidden Parameters
    for i in range(carbonCfgXtorInstanceNumParams(self._xtorInstance)):
      ## hidden parameters will be returned as None here and are ignored
      paramInst = carbonCfgXtorInstanceGetParam(self._xtorInstance, i)
      if ( paramInst != None ):
        self.__params.append(Parameter(paramInst))

    # Build list of include files
    for i in range(carbonCfgXtorNumIncludeFiles(xtor)):
      self.__includeFiles.append(carbonCfgXtorGetIncludeFile(xtor, i))

    # Build list of include paths
    for i in range(carbonCfgXtorNumIncPaths(xtor)):
      self.__incPaths.append(carbonCfgXtorGetIncPath(xtor, i))

    # Build list of library files
    for i in range(carbonCfgXtorNumLibFiles(xtor)):
      self.__libFiles.append(carbonCfgXtorGetLibFile(xtor, i))

    # Build list of library paths
    for i in range(carbonCfgXtorNumLibPaths(xtor)):
      self.__libPaths.append(carbonCfgXtorGetLibPath(xtor, i))

    # Build list of flags
    for i in range(carbonCfgXtorNumFlags(xtor)):
      self.__flags[carbonCfgXtorGetFlag(xtor, i)] = True

    # Use the default class name and interface information
    self.__className = carbonCfgXtorGetClassName(xtor)
      
    # Assume there is no layer ID; this can be overriden below
    self.__layerId = ""

    # Some of the xtor information can also come from an abstraction.
    # The xtor information is common across the abstractions while the
    # abstraction one is specific to the abstraction.
    #
    # The class name and layer id are overriden if there is a abstraction.
    xtorAbstraction = carbonCfgXtorInstanceGetAbstraction(self._xtorInstance)
    if xtorAbstraction != None:
      # Class name
      className = carbonCfgXtorAbstractionGetClassName(xtorAbstraction)
      if (className != None) and (className != ""):
        self.__className = className

      # Get the layer ID
      self.__layerId = carbonCfgXtorAbstractionGetLayerId(xtorAbstraction)

      # Add to the list of include files
      for i in range(carbonCfgXtorAbstractionNumIncludeFiles(xtorAbstraction)):
        self.__includeFiles.append(carbonCfgXtorAbstractionGetIncludeFile(xtorAbstraction, i))

      # Add to the list of include paths
      for i in range(carbonCfgXtorAbstractionNumIncPaths(xtorAbstraction)):
        self.__incPaths.append(carbonCfgXtorAbstractionGetIncPath(xtorAbstraction, i))

      # Add to the list of library files
      for i in range(carbonCfgXtorAbstractionNumLibFiles(xtorAbstraction)):
        self.__libFiles.append(carbonCfgXtorAbstractionGetLibFile(xtorAbstraction, i))

      # Add to the list of library paths
      for i in range(carbonCfgXtorAbstractionNumLibPaths(xtorAbstraction)):
        self.__libPaths.append(carbonCfgXtorAbstractionGetLibPath(xtorAbstraction, i))

      # Add to the list of flags
      for i in range(carbonCfgXtorAbstractionNumFlags(xtorAbstraction)):
        self.__flags[carbonCfgXtorAbstractionGetFlag(xtorAbstraction, i)] = True

  def name(self): return carbonCfgXtorInstanceGetName(self._xtorInstance)

  def writeDebug(self): 
    xtorWriteDebug = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    writeDebug = carbonCfgXtorHasWriteDebug(xtorWriteDebug)
    return writeDebug

  def debugTransaction(self): 
    xtorDebugTransaction = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    debugTransaction = carbonCfgXtorHasDebugTransaction(xtorDebugTransaction)
    return debugTransaction

  def useDebugAccess(self):
    return carbonCfgXtorInstanceUseDebugAccess(self._xtorInstance)
  
  def className(self):
    return self.__className

  def libraryName(self):
    xtorDef = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    name = carbonCfgXtorGetLibraryName(xtorDef)
    return name

  def variant(self):
    xtorDef = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    name = carbonCfgXtorGetVariant(xtorDef)
    return name

  def includeFiles(self):
    return self.__includeFiles

  def type(self):
    xtorType = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    name = carbonCfgXtorGetName(xtorType)
    return name

  # Returns the value for a given parameter (none if it is not found)
  def findParam(self, param):
    return carbonCfgXtorInstanceFindParameter(self._xtorInstance, param)

  def params(self):
    for parm in self.__params:
      yield parm
      
  def addInput(self, port):
    self.__inputs.append(port)

  def addOutput(self, port):
    self.__outputs.append(port)

  def addUnconnected(self, port):
    self.__unconnected.append(port)

  def numInputs(self): return len(self.__inputs)
  def numOutputs(self): return len(self.__outputs)

  def inputs(self): return iter(self.__inputs)
  def outputs(self): return iter(self.__outputs)
  def unconnected(self): return iter(self.__unconnected)

  def systemCPorts(self): return iter(self.__systemCPorts)
  
  def isMaster(self):
    xtor = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    if carbonCfgXtorIsMaster(xtor):
      return True
    else:
      return False
    
  def isSlave(self):
    xtor = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    if carbonCfgXtorIsSlave(xtor):
      return True
    else:
      return False
    
  def isAmba(self):
    xtor = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    if carbonCfgXtorIsAmba(xtor):
      return True
    else:
      return False
    
  def clockMaster(self):
    xtor = carbonCfgXtorInstanceGetClockMaster(self._xtorInstance)
    if xtor != None:
      xtor = Transactor(self.__cfg, xtor)
    return xtor
    
  def eslClockMaster(self):
    xtor = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    if carbonCfgXtorUseESLClockMaster(xtor):
      return carbonCfgXtorInstanceGetESLClockMaster(self._xtorInstance)
    return ""
    
  def eslResetMaster(self):
    xtor = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    if carbonCfgXtorUseESLResetMaster(xtor):
      return carbonCfgXtorInstanceGetESLResetMaster(self._xtorInstance)
    return ""
    
  def supportFlowThru(self): 
    xtor = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    ft = carbonCfgXtorIsFlowThru(xtor)
    if ft == 0:
      return False
    else:
      return True

  def supportSaveRestore(self): 
    xtor = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    sr = carbonCfgXtorIsSaveRestore(xtor)
    if sr == 0:
      return False
    else:
      return True

  def protocolName(self):
    xtorProtocolName = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    protocolName = carbonCfgXtorGetProtocolName(xtorProtocolName); 
    return protocolName

  def libFiles(self):
    return self.__libFiles

  def incPaths(self):
    return self.__incPaths

  def libPaths(self):
    return self.__libPaths

  def hasFlag(self, flag):
    return flag in self.__flags

  def constructorArgs(self):
    xtorArgs = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    args = carbonCfgXtorGetConstructorArgs(xtorArgs); 
    return args

  def sdXactor(self): 
    xtorSDXactor = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    sdXtor = carbonCfgXtorIsSDXactor(xtorSDXactor); 
    if sdXtor == 0:
      return False
    else:
      return True

  def useEventQueue(self): 
    xtorEventQueue = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    eventQueue = carbonCfgXtorUseEventQueue(xtorEventQueue); 
    if eventQueue == 0:
      return False
    else:
      return True

  def preservePortNameCase(self): 
    xtor = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    preserveCase = carbonCfgXtorPreservePortNameCase(xtor); 
    if preserveCase == 0:
      return False
    else:
      return True

  def useCommunicate(self): 
    xtorCommunicate = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    communicate = carbonCfgXtorUseCommunicate(xtorCommunicate); 
    if communicate == 0:
      return False
    else:
      return True

  # Return the template args as a list of (type, value) pairs
  def templateArgs(self):
    args = []
    xtor = carbonCfgXtorInstanceGetXtor(self._xtorInstance)
    for i in range(carbonCfgXtorNumTemplateArgs(xtor)):
      xtorTemplateArg = carbonCfgXtorGetTemplateArg(xtor, i)
      args.append(TemplateArg(xtorTemplateArg))
    return args

  # Get the layer ID (empty string if there isn't one)
  def layerId(self):
    return self.__layerId

class ProfileStream:
  def __init__(self, index, stream):
    self.__index = index
    self.__stream = stream
    carbonCfgPStreamComputeChannelDescriptors(self.__stream)
  def name(self): return carbonCfgPStreamGetName(self.__stream)
  def trigger(self): return carbonCfgPStreamGetTrigger(self.__stream)
  def numChannels(self): return carbonCfgPStreamNumChannels(self.__stream)
  def numNets(self): return carbonCfgPStreamNumNets(self.__stream)
  def index(self): return self.__index
  def nets(self):
    return [ProfileNet(carbonCfgPStreamGetNet(self.__stream, i))
            for i in range(carbonCfgPStreamNumNets(self.__stream))]
  def triggers(self):
    return [ProfileTrigger(i, carbonCfgPStreamGetTrigger(self.__stream, i),
                           self.__stream)
            for i in range(carbonCfgPStreamNumTriggers(self.__stream))]
  def channelDescriptors(self):
    return [ProfileChannelDescriptor(i, carbonCfgPStreamGetChannelDescriptor(self.__stream, i))
            for i in range(carbonCfgPStreamNumChannels(self.__stream))]
  
class ProfileTrigger:
  # note that we have to keep track of the stream in the trigger
  # so we can get access to the number of channels, which is
  # common to all triggers in the stream

  def __init__(self, index, trigger, stream):
    self.__index = index
    self.__trigger = trigger
    self.__stream = stream
  def expr(self): return carbonCfgPTriggerGetExpr(self.__trigger)
  def index(self): return self.__index

  # note that we get the number of channels from the stream, not the trigger
  def channels(self):
    return [ProfileChannel(i, carbonCfgPTriggerGetChannel(self.__trigger, i))
            for i in range(carbonCfgPStreamNumChannels(self.__stream))]
  
class ProfileNet:
  def __init__(self, net):
    self.__net = net
  def name(self): return carbonCfgPNetGetName(self.__net)
  def path(self): return carbonCfgPNetGetPath(self.__net)
  def width(self): return carbonCfgPNetGetWidth(self.__net)

class ProfileChannel:
  def __init__(self, index, channel):
    self.__index = index
    self.__channel = channel
  def name(self): return carbonCfgPChannelGetName(self.__channel)
  def index(self): return self.__index
  def expr(self): return carbonCfgPChannelGetExpr(self.__channel)
  def numBuckets(self): return carbonCfgPChannelNumBuckets(self.__channel)
  def numTotalBuckets(self): return carbonCfgPChannelNumTotalBuckets(self.__channel)
  def buckets(self):
    return [ProfileBucket(i, getBucket(self.__channel, i))
            for i in range(carbonCfgPChannelNumBuckets(self.__channel))]

class ProfileChannelDescriptor:
  def __init__(self, index, channel):
    self.__index = index
    self.__channel = channel
  def name(self): return carbonCfgPChannelDescriptorGetName(self.__channel)
  def index(self): return self.__index
  def numBuckets(self): return carbonCfgPChannelDescriptorNumBuckets(self.__channel)
  def buckets(self):
    return [ProfileBucket(i, carbonCfgPChannelDescriptorGetBucket(self.__channel, i))
            for i in range(carbonCfgPChannelDescriptorNumBuckets(self.__channel))]

class ProfileBucket:
  def __init__(self, index, bucket):
    self.__bucket = bucket
  def name(self): return carbonCfgPBucketGetName(self.__bucket)
  def expr(self): return carbonCfgPBucketGetExpr(self.__bucket)
  def index(self): return carbonCfgPBucketGetIndex(self.__bucket)
  def color(self): return carbonCfgPBucketGetColor(self.__bucket)

class ELFLoaderSection:
  def __init__(self, index, name, space, access):
    self.__index  = index
    self.__name   = name
    self.__space  = space
    self.__access = access
    
  def index(self):
    return self.__index
  
  def name(self):
    return self.__name

  def space(self):
    return self.__space

  def access(self):
    return self.__access
  
class ELFLoader:
  def __init__(self, elf):
    self.__elf      = elf
    self.__sections = []

    for i in range(carbonCfgELFLoaderNumSections(self.__elf)):
      self.__sections.append(ELFLoaderSection(i, carbonCfgELFLoaderGetName(self.__elf, i), carbonCfgELFLoaderGetSpace(self.__elf,i), carbonCfgELFLoaderGetAccess(self.__elf,i)))

  def sections(self):
    return self.__sections

class ProcessorInfo:
  'This class is only used for Model Kits of ARM processors. It holds information specific the processor'
  def __init__(self, cfg):
    self.__procInfo      = carbonCfgGetProcInfo(cfg)
    self.__isProcessor   = carbonCfgIsProcessor(cfg)

    # Gather the processor options
    self.__processorOptions = ''
    sep = ''
    for i in range(carbonCfgProcInfoGetNumProcessorOptions(self.__procInfo)):
      option = carbonCfgProcInfoGetProcessorOption(self.__procInfo, i)
      self.__processorOptions += sep + option
      sep =','

  def isProcessor(self):
    if self.__isProcessor:
      return True
    return False
  
  def debuggerName(self):
    return carbonCfgProcInfoGetDebuggerName(self.__procInfo)
  
  def numPipeStages(self):
    return carbonCfgProcInfoGetPipeStages(self.__procInfo)

  def numHWThreads(self):
    return carbonCfgProcInfoGetHwThreads(self.__procInfo)

  def processorOptions(self):
    return self.__processorOptions

  def pcRegGroupName(self):
    return carbonCfgProcInfoGetPCRegGroupName(self.__procInfo)

  def pcRegName(self):
    return carbonCfgProcInfoGetPCRegName(self.__procInfo)

  def extendedFeaturesRegGroupName(self):
    return carbonCfgProcInfoGetExtendedFeaturesRegGroupName(self.__procInfo)

  def extendedFeaturesRegName(self):
    return carbonCfgProcInfoGetExtendedFeaturesRegName(self.__procInfo)

  def debuggablePoint(self):
    return carbonCfgProcInfoGetDebuggablePoint(self.__procInfo)

  def targetName(self):
    return carbonCfgProcInfoGetTargetName(self.__procInfo)

