
import carbondbapi
import DBNode

##
# \file
#
# This file contains the implementation of the base class for node iteration

##
#    \defgroup CarbonDBIterDoc Database Node Iterator
#    @{
#    @}

##
#    \addtogroup CarbonDBIterDoc
#    \brief The following class or classes describe the general database node iterator
#    @{

##  Base class for carbon database node iteration.
#
#  Specializations of this class give access to particular types of
#  DBNode objects. 
class DBIter:
    def __init__(self, dbObj, iterCreateFunc, iterArg = None):
        self.mDBObj = dbObj
        # Pass the argument if it exists
        if iterArg != None:
            self.mIter = iterCreateFunc(dbObj, iterArg)
        else:
            self.mIter = iterCreateFunc(dbObj)
        
    def __del__(self):
        carbondbapi.carbonDBFreeNodeIter(self.mIter)

    ## Return the next DBNode.DBNode in the list
    def next(self):
        node = carbondbapi.carbonDBNodeIterNext(self.mIter)
        while (node):
            yield DBNode.DBNode(self.mDBObj, node)
            node = carbondbapi.carbonDBNodeIterNext(self.mIter)
        raise StopIteration

    def __iter__(self):
        return self.next()




##
#
#    @}
#
