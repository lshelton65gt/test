#******************************************************************************
# Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

# Classes for generation of SCML SystemC Memories
import Carbon.Cfg

class ScmlMemory:
  def __init__(self, module, mem):
    self.__module = module
    self.__mem = mem

  def memory(self):
    return self.__mem

  def numCarbonUInt32(self):
    return (self.size() + 31) / 32
  
  def size(self):
    width = self.memory().width()
    if width <= 32:
      return 32
    elif width <= 64:
      return 64
    else:
      raise "Unexpected memory width '%d' for '%s'" % (width, self.memName())
    return 0

  def memName(self):
    return self.memory().name().replace(".","_")
    
  def varName(self):
    return "m_" + self.memName()
  
  def carbonVarName(self):
    return "m_memory_" + self.memName()
  
  def rtlIncr(self, rtlLoc):
    if rtlLoc.endWord() > rtlLoc.addrOffset():
      return 1
    else:
      return -1

  def findNode(self, rtlLoc):
    node = self.__module.config().getDB().findNode(rtlLoc.path())
    return node

  def emitCarbonMemoryInit(self):
    code = ''
    id = 0
    for block in self.memory().blocks():
      for loc in block.locs():
        rtlLoc = loc.castRTL()
        if rtlLoc:
          # Find the db node associated with this rtl locator
          node = self.findNode(rtlLoc)
          if node == None:
            print "Error: Could not find RTL memory path '%s' for memory: '%s', block: '%s'" % (rtlLoc.path(),
                                                                                                self.memName(),
                                                                                                block.name())
          else:
            # Emit code for this RTL locator
            code += '''\
  // Block: %(blockName)s
  CarbonScMemoryBlockBase<%(cppType)s>* %(blockVar)s;
  %(blockVar)s = new CarbonScMemoryBlock<%(memName)s_response_type, %(memName)s_request_type, %(cppType)s,
                                     /* block start addr: */ %(blockStartAddr)d, /* word Size */ %(blockWordSize)d,
                                     /* rtl start addr: */ %(rtlStartAddr)d, /* rtl incr */ %(rtlIncr)d,
                                     /* block start bit: */ %(blockStartBit)d, /* rtl start bit: */ %(rtlStartBit)d, /* bit Size: */ %(blockBitSize)d>("%(blockName)s");
  %(blockVar)s->initialize(carbonModelHandle, "%(memPath)s");
  %(carbonVar)s.addBlock(%(blockVar)s);

''' % { 'blockName'     : block.name(),
        'memName'       : self.memory().name(),
        'blockVar'      : 'blockBase_%(memName)s_%(id)d' % { 'memName': self.memory().name(), 'id' : id },
        'cppType'       : self.cppType(),
        'blockStartAddr': loc.displayStartWord() + (block.baseAddrLo() / (self.size() / 8 )),
        'blockWordSize' : loc.displayEndWord() - loc.displayStartWord() + 1,
        'rtlStartAddr'  : rtlLoc.addrOffset() ,
        'rtlIncr'       : self.rtlIncr(rtlLoc),
        'blockStartBit' : loc.displayLsb(),
        'rtlStartBit'   : rtlLoc.lsb() - node.getLsb(),
        'blockBitSize'  : loc.displayMsb() - loc.displayLsb() + 1,
        'memPath'       : rtlLoc.path(),
        'carbonVar'     : self.carbonVarName()
        }
            id = id + 1
    return code
  
  def emitCallbackDeclaration(self):
    return '    MEMORY_REGISTER_NB_TRANSPORT(%(varName)s, transport_%(memName)s);' % { "varName" : self.varName(), "memName" : self.memName() }

  def emitAddressingMode(self):
    code = """\
    %(varName)s.set_addressing_mode(8); 
    %(varName)s.set_trigger_behaviour_on_debug_read(true);
    %(varName)s.set_trigger_behaviour_on_debug_write(true);
""" % { "varName" : self.varName() }
# Not yet supported
#    if self.memory().bigEndian():
#      code += "    %(varName)s.set_endianness(scml_big_endian);\n" % { "varName" : self.varName() }
    return code

  def getConstructorInitializers(self):
    varMap = { "varName" : self.varName(),
               "memName" : self.memName(),
               "numWords" : self.memory().maxAddrs()+1,
               "carbonVarName" : self.carbonVarName()
               }
    initializers = []
    initializers.append('%(varName)s("%(memName)s", scml_memsize(%(numWords)s))' % varMap)
    initializers.append('%(carbonVarName)s("%(memName)s")' % varMap)
    return initializers

  def emitDeclaration(self):
    code = """\
scml_memory <%(cppType)s> %(varName)s;
   typedef scml_memory_pv%(width)s_if::request_type %(memName)s_request_type;
   typedef scml_memory_pv%(width)s_if::response_type %(memName)s_response_type;
   typedef scml_memory_pv%(width)s_if::address_type %(memName)s_address_type;
   typedef scml_memory_pv%(width)s_if::data_type %(memName)s_data_type;
   %(memName)s_response_type transport_%(memName)s(const %(memName)s_request_type &);
   CarbonScMemory<%(memName)s_request_type, %(memName)s_response_type, %(cppType)s> %(carbonVar)s;
""" % {
        "width" : self.size(),
        "cppType" : self.cppType(),
        "varName" : self.varName(),
        "memName" : self.memName(),
        "carbonVar" : self.carbonVarName()
      }
    return code

  def emitCallback(self,moduleName):
    code = """\
inline %(moduleName)s::%(memName)s_response_type %(moduleName)s::transport_%(memName)s(const %(memName)s_request_type & req)
{
  return %(memVar)s.transport(req, %(varName)s);
}
""" % {
      "memVar" : self.carbonVarName(),
      "moduleName" : moduleName,
      "memName" : self.memName(),
      "varName" : self.varName()
      }
    return code

class ScmlMemory32(ScmlMemory):
  def __init__(self, config, mem):
    ScmlMemory.__init__(self, config, mem)

  def cppType(self):
    return "unsigned int"
  
  def emitUIntStorage(self, varName):
    return "CarbonUInt32 " + varName + ""

  def emitUIntAccess(self, varName):
    return varName;

  def emitUIntStorageRef(self, varName):
    return "&"+varName;

class ScmlMemory64(ScmlMemory):
  def __init__(self, config, mem):
    ScmlMemory.__init__(self, config, mem)

  def cppType(self):
    return "unsigned long long"
  
  def emitUIntAccess(self, varName):
    return varName;

  def emitUIntStorage(self, varName):
    return "CarbonUInt32 " + varName + "[" + str(self.numCarbonUInt32()) + "]"
  
  def emitUIntStorageRef(self, varName):
    return varName;
