#******************************************************************************
# Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

# Classes for generation of SCML SystemC Registers
import Carbon.Cfg

class ScmlError(Exception):
  pass

class ScmlRegister:
  def __init__(self, mod, reg):
    self.__reg = reg
    self.__mod = mod

  def module(self):
    return self.__mod

  def register(self):
    return self.__reg

  def numCarbonUInt32(self):
    return (self.size() + 31) / 32
  
  def size(self):
    return self.register().width()

  def regName(self):
    return self.register().name().replace(".","_")

  def offset(self):
    return self.__reg.offset()

  def varName(self):
    return "m_" + self.register().group() + "_" + self.regName()

  def carbonVarName(self):
    return "m_regnet_" + self.regName()
  
  def bitfieldName(self,field):
    return self.register().group() + "_" + self.regName() + "_" + field.name()

  def fieldType(self):
    # Note that the field type is currently hard coded to unsigned int
    # because that is what SCML v1 implements. This is true for 8 and
    # 16 bit register banks as well. if that changes we need to
    # address it somehow.
    return 'unsigned int'

  def emitBitfieldCallbackDeclarations(self,field):
    code = ""
    if (field.access() == 'read-write') or (field.access() == 'write-only'):
      code += '''\
  void bf_write_%(fieldName)s(%(fieldType)s);
  CarbonAbstractRegisterID m_ar_write_%(fieldName)s;''' % { 'fieldName': self.bitfieldName(field),
                                                            'fieldType': self.fieldType() }

    if  (field.access() == 'read-write') or (field.access() == 'read-only'):
      code += '''\
  %(fieldType)s bf_read_%(fieldName)s();\
  CarbonAbstractRegisterID m_ar_read_%(fieldName)s;'''  % { 'fieldName': self.bitfieldName(field),
                                                            'fieldType': self.fieldType() }
    return code


  def isConstant(self, loc):
    for node in self.module().config().getDB().iterInputs():
      if node.fullName() == loc.path() and node.isConstant():
        return True
    for node in self.module().config().getDB().iterOutputs():
      if node.fullName() == loc.path() and node.isConstant():
        return True
    for node in self.module().config().getDB().iterBidis():
      if node.fullName() == loc.path() and node.isConstant():
        return True
    for node in self.module().config().getDB().iterObservable():
      if node.fullName() == loc.path() and node.isConstant():
        return True
    for node in self.module().config().getDB().iterDepositable():
      if node.fullName() == loc.path() and node.isConstant():
        return True

    return False

    
  def emitBitfieldCBDecl(self, field, loc):
    code=""
    if field.access() != 'write-only' and not self.isConstant(loc):
      code = """
  CarbonNetValueCBData * %(cbVarName)s_%(regFieldName)s;
  static void valueChangeCallback%(fieldName)s_%(carbonLoc)s(CarbonObjectID *, CarbonNetID *, void * data, CarbonUInt32 *, CarbonUInt32 *);
""" % {
      "regFieldName" : field.name(),
      "fieldName" : self.bitfieldName(field),
      "cbVarName" : self.bitfieldCBVarName(loc),
      "regName" : self.regName(),
      "carbonLoc" : self.getCppPathName(loc)
      }

    return code
  
  def bitfieldCBVarName(self, loc):
    return """m_cb_%(regName)s_%(carbonLoc)s""" % {"regName" : self.regName(),
                                               "carbonLoc" : self.getCppPathName(loc) }

  def emitBitfieldCarbonDeclaration(self,field):
    fieldLoc = field.loc()
    if fieldLoc.type() == 'register':
      loc = fieldLoc.castRegister()
      code = """\
  CarbonNetID * m_regnet_%(regName)s_%(carbonLoc)s_%(regFieldName)s;
  %(vcCallbackDecl)s
""" % {
        "regFieldName" : field.name(),
        "vcCallbackDecl" : self.emitBitfieldCBDecl(field, loc),
        "regName" : self.regName(),
        "carbonLoc" : self.getCppPathName(loc)
      }
      return code
    elif fieldLoc.type() == 'array':
      loc = fieldLoc.castArray()
      code = """\
  CarbonMemoryID * m_regnet_%(regName)s_%(carbonLoc)s_%(regFieldName)s;
""" % {
        "regFieldName" : field.name(),
        "regName" : self.regName(),
        "carbonLoc" : self.getCppPathName(loc)
      }
      return code
    return ""
    
  def emitCallbackDecl(self, v2010):
    code = "\n    " + self.varName() + ".set_trigger_behaviour_on_debug_read(true);"
    code += "\n    " + self.varName() + ".set_trigger_behaviour_on_debug_write(true);"
    for field in self.register().fields():
      if field.access() == 'read-write':
        code += "\n    BITFIELD_REGISTER_NB_READ(m_" + self.bitfieldName(field) + ", bf_read_" + self.bitfieldName(field) + ");"
        code += "\n    BITFIELD_REGISTER_NB_WRITE(m_" + self.bitfieldName(field) + ", bf_write_" + self.bitfieldName(field) + ");"
      if field.access() == 'read-only':
        code += "\n    BITFIELD_REGISTER_NB_READ(m_" + self.bitfieldName(field) + ", bf_read_" + self.bitfieldName(field) + ");"
        if v2010:
          code += "\n    m_" + self.bitfieldName(field) + ".set_read_only();"
      if field.access() == 'write-only':
        code += "\n    BITFIELD_REGISTER_NB_WRITE(m_" + self.bitfieldName(field) + ", bf_write_" + self.bitfieldName(field) + ");"
        if v2010:
          code += "\n    m_" + self.bitfieldName(field) + ".set_write_only();"
      code += "\n"
    return code
      
  def emitCarbonRegisterValueCB(self, field, loc, varname):
    cbvarname = self.bitfieldCBVarName(loc)
    code = """
    %(cbVarName)s_%(regFieldName)s = carbonAddNetValueChangeCB(carbonModelHandle, valueChangeCallback%(fieldName)s_%(carbonLoc)s, this, %(varName)s);
""" % {
      "regFieldName" : field.name(),
      "varName" : varname,
      "cbVarName" : cbvarname,
      "fieldName" : self.bitfieldName(field),
      "carbonLoc" : self.getCppPathName(loc)
      }
    return code

  def emitCarbonRegisterInit(self):
    code=""
    for field in self.register().fields():
      loc = field.loc()
      if loc.type() == 'register':
        regLoc = loc.castRegister()
        varname = 'm_regnet_' + self.regName() + "_" + self.getCppPathName(regLoc) + "_" + field.name()
        code += '    ' + varname + ' = carbonFindNet(carbonModelHandle, \"' + regLoc.path() + '\");\n'
        code += '    assert(' + varname + ');\n' # //this_assert_OK


        if field.access() != 'write-only':

          if not self.isConstant(regLoc):
            code += self.emitCarbonRegisterValueCB(field, regLoc, varname)

        if field.access() == 'read-write' or field.access() == 'read-only':
          code += '    m_ar_read_' + self.bitfieldName(field) + ' = carbonAbstractRegisterCreate(' + str(regLoc.right()) + ', ' + str(1+regLoc.left()-regLoc.right()) + ', 0);\n'
        if field.access() == 'read-write' or field.access() == 'write-only':
          code += '    m_ar_write_' + self.bitfieldName(field) + ' = carbonAbstractRegisterCreate(0, ' + str(1+regLoc.left()-regLoc.right()) + ', ' + str(regLoc.right()) + ');\n'
      if loc.type() == 'array':
        regLoc = loc.castArray()
        varname = 'm_regnet_' + self.regName() + "_" + self.getCppPathName(regLoc) + "_" + field.name()
        code += '    ' + varname + ' = carbonFindMemory(carbonModelHandle, \"' + regLoc.path() + '\");\n'
        code += '    assert(' + varname + ');\n' # //this_assert_OK
        if field.access() == 'read-write' or field.access() == 'read-only':
          code += '    m_ar_read_' + self.bitfieldName(field) + ' = carbonAbstractRegisterCreate(' + str(regLoc.right()) + ', ' + str(1+regLoc.left()-regLoc.right()) + ', 0);\n'
        if field.access() == 'read-write' or field.access() == 'write-only':
          code += '    m_ar_write_' + self.bitfieldName(field) + ' = carbonAbstractRegisterCreate(0, ' + str(1+regLoc.left()-regLoc.right()) + ', ' + str(regLoc.right()) + ');\n'
    code += '\n'
    return code
      
  def getConstructorInitializers(self):
    initializers = []

    # Add the register initializer
    varMap = { "regGroup" : self.register().group(),
               "regOffset" : self.register().offset(),
               "regName" : self.regName(),
               "varName" : self.varName()
               }
    initializers.append('%(varName)s("%(regName)s", m_%(regGroup)s, %(regOffset)s, 1)' % varMap)

    # And the bitifield initializers
    for field in self.register().fields():
      varMap = { 'bitfieldName' : self.bitfieldName(field),
                 'fieldName'    : field.name(),
                 'varName'      : self.varName(),
                 'fieldLow'     : field.low(),
                 'fieldWidth'   : 1+field.high()-field.low() }
      code = 'm_%(bitfieldName)s("%(fieldName)s", %(varName)s, %(fieldLow)d, %(fieldWidth)d)' % varMap
      initializers.append(code)
    return initializers
    
  def emitBitfieldDeclaration(self,field):
    code = """\
  scml_bitfield %(varName)s_%(fieldName)s;
%(bfCarbonDecl)s
%(bfCallbacksDecl)s
""" % {
        "carbonVar" : self.carbonVarName(),
        "varName" : self.varName(),
        "fieldName" : field.name(),
        "bfCarbonDecl" : self.emitBitfieldCarbonDeclaration(field),
        "bfCallbacksDecl" : self.emitBitfieldCallbackDeclarations(field)
      }
    return code

  def emitBitfieldDeclarations(self):
    code = ""
    for field in self.register().fields():
      code += self.emitBitfieldDeclaration(field)
    return code
  
  def emitDeclaration(self):
    code = """\
scml_memory <%(cppType)s> %(varName)s;
%(bitfieldDecls)s
""" % {
        "bitfieldDecls" : self.emitBitfieldDeclarations(),
        "cppType" : self.cppType(),
        "varName" : self.varName()
      }
    return code
  
  def getCppPathName(self, loc):
    hdlName = loc.path()
    name = ''
    
    # replace all questionable chars with _
    for c in hdlName:
      code = ord(c)
      if code >= ord('a') and code <= ord('z'):
        name += c
      elif code >= ord('A') and code <= ord('Z'):
        name += c
      elif code >= ord('0') and code <= ord('9'):
        name += c
      else:
        name += '_'

    return name

# Common operations
class ScmlRegisterCommon(ScmlRegister):
  def __init__(self, mod, reg, size):
    ScmlRegister.__init__(self, mod, reg)
    self.__size = size
    
  def emitUIntStorage(self, varName):
    return "CarbonUInt32 " + varName + "[" + str(self.numCarbonUInt32()) + "]"
  
  def emitUIntStorageRef(self, varName):
    return varName;

  def emitCallbackImplementation(self):
    code=""
    for field in self.register().fields():
      code += self.emitBitfieldCallback(field)
    return code

  def emitBitfieldCallback(self, field):
    code=""
    if field.access() == 'read-write' or field.access() == 'read-only':
      code += self.emitBitfieldReadCallback(field)
    if field.access() == 'read-write' or field.access() == 'write-only':
      code += self.emitBitfieldWriteCallback(field)
    return code

  def emitBitfieldReadCallback(self, field):
    code=""
    floc = field.loc()
    loc = None
    if floc.type() == 'register':
      return self.emitBitfieldReadRegister(field, floc.castRegister())
    elif floc.type() == 'array':
      return self.emitBitfieldReadArray(field, floc.castArray())
    elif floc.type() == 'constant':
      return self.emitBitfieldReadConstant(field, floc.castConstant())
    else:
      raise ScmlError('Unknown field location: ' + floc.type())
    return code

  def emitBitfieldWriteCallback(self, field):
    code=""
    floc = field.loc()
    loc = None
    if floc.type() == 'register':
      return self.emitBitfieldWriteRegister(field, floc.castRegister())
    elif floc.type() == 'array':
      return self.emitBitfieldWriteArray(field, floc.castArray())
    elif floc.type() == 'constant':
      return ""
    else:
      raise ScmlError('Unknown field location: ' + floc.type())
    return code

  def emitBitfieldReadValueCB(self,field,loc):
    code=""
    userCodeName = 'VC ' + self.bitfieldName(field) + '_' + self.getCppPathName(loc)
    if field.access() != 'write-only' and not self.isConstant(loc):
      code="""
inline void %(moduleName)s::valueChangeCallback%(fieldName)s_%(carbonLoc)s(CarbonObjectID *model, CarbonNetID *net, void * data, CarbonUInt32 *currValue, CarbonUInt32 *)
{
  CarbonUInt32 retValue=0;
  %(moduleName)s* pThis = reinterpret_cast<%(moduleName)s*>(data);
%(userCodeBegin)s
  CarbonAbstractRegisterXfer(pThis->m_ar_read_%(fieldName)s, currValue, &retValue);
  pThis->m_%(fieldName)s = retValue;
%(userCodeEnd)s
}
""" % {
      "userCodeBegin" : self.module().userCodeSection("h", "Pre", userCodeName),
      "userCodeEnd" : self.module().userCodeSection("h", "Post", userCodeName),
      "cbVarName" : self.bitfieldCBVarName(loc),
      "scmlRegName" : self.regName(),
      "carbonLoc" : self.getCppPathName(loc),
      "moduleName": self.module().scModuleName(),
      "regName": self.varName(),
      "varName": self.carbonVarName() + "_" + self.getCppPathName(loc),
      "fieldType": self.fieldType(),
      "fieldName": self.bitfieldName(field)
      }
    return code

  def emitBitfieldReadRegister(self,field,loc):
    code = self.emitBitfieldReadValueCB(field,loc)

    userCodeName = 'BFRR' + str(self.__size) + ' ' + self.bitfieldName(field) + '_' + self.getCppPathName(loc)
    code += """\
inline %(fieldType)s %(moduleName)s::bf_read_%(fieldName)s()
{
  CarbonUInt32 currValue[%(numWords)s];
  CarbonUInt32 retValue=0;
  memset(currValue, 0, sizeof(currValue));
%(userCodeBegin)s

  carbonExamine(carbonModelHandle, %(varName)s, currValue, NULL);
  CarbonAbstractRegisterXfer(m_ar_read_%(fieldName)s, currValue, &retValue);

#if _CARBON_DEBUG_SCML
  cout << "CARBON-DEBUG: " << sc_time_stamp().value() << " Bitfield Register Read %(fieldName)s Value=0x" << hex << retValue << dec << endl;
#endif

%(userCodeEnd)s
  return retValue;
}
""" % {
      "userCodeBegin" : self.module().userCodeSection("h", "Pre", userCodeName),
      "userCodeEnd" : self.module().userCodeSection("h", "Post", userCodeName),
      "numWords" : str(self.numCarbonUInt32()),
      "regName": self.varName(),
      "varName": self.carbonVarName() + "_" + self.getCppPathName(loc) + "_" + field.name(),
      "fieldType": self.fieldType(),
      "fieldName": self.bitfieldName(field),
      "moduleName": self.module().scModuleName()
      }
    return code

  def emitBitfieldReadArray(self,field,loc):
    userCodeName = 'BFRA' + str(self.__size) + ' ' + self.bitfieldName(field) + '_' + self.getCppPathName(loc)
    code = """\
inline %(fieldType)s %(moduleName)s::bf_read_%(fieldName)s()
{
  CarbonUInt32 currValue[%(numWords)s];
  CarbonUInt32 retValue=0;
  memset(currValue, 0, sizeof(currValue));

%(userCodeBegin)s

  carbonExamineMemory(%(varName)s, %(memIndex)s, currValue);
  CarbonAbstractRegisterXfer(m_ar_read_%(fieldName)s, currValue, &retValue);

#if _CARBON_DEBUG_SCML
  cout << "CARBON-DEBUG: " << sc_time_stamp().value() << " Bitfield Array Read %(fieldName)s Value=0x" << hex << retValue << dec << endl;
#endif

%(userCodeEnd)s

  return retValue;
}
""" % {
      "userCodeBegin" : self.module().userCodeSection("h", "Pre", userCodeName),
      "userCodeEnd" : self.module().userCodeSection("h", "Post", userCodeName),
      "numWords" : str(self.numCarbonUInt32()),
      "memIndex": str(loc.index()),
      "regName": self.varName(),
      "varName": self.carbonVarName() + "_" + self.getCppPathName(loc) + "_" + field.name(),
      "fieldType": self.fieldType(),
      "fieldName": self.bitfieldName(field),
      "moduleName": self.module().scModuleName()
      }
    return code

  def emitBitfieldReadConstant(self,field,loc):
    userCodeName = 'BFRC' + str(self.__size) + ' ' + self.bitfieldName(field) + ' CONSTANT'
    code = """\
inline %(fieldType)s %(moduleName)s::bf_read_%(fieldName)s()
{
%(userCodeBegin)s
%(userCodeEnd)s
  return %(hexValue)s;
}
""" % {
      "userCodeBegin" : self.module().userCodeSection("h", "Pre", userCodeName),
      "userCodeEnd" : self.module().userCodeSection("h", "Post", userCodeName),
      "hexValue": hex(loc.value()),
      "fieldType": self.fieldType(),
      "fieldName": self.bitfieldName(field),
      "moduleName": self.module().scModuleName()
      }
    return code

  def emitBitfieldWriteRegister(self,field,loc):
    userCodeName = 'BFWW' + str(self.__size) + ' ' + self.bitfieldName(field) + '_' + self.getCppPathName(loc)
    code = '''\
inline void %(moduleName)s::bf_write_%(fieldName)s(%(fieldType)s newVal)
{
  CarbonUInt32 currValue[%(numWords)s];
  memset(currValue, 0, sizeof(currValue));
%(userCodeBegin)s
  %(regName)s = newVal;
  carbonExamine(carbonModelHandle, %(varName)s, currValue, NULL);
  CarbonAbstractRegisterXfer(m_ar_write_%(fieldName)s, &newVal, currValue);
  carbonDepositFast(carbonModelHandle, %(varName)s, currValue, NULL);

#if _CARBON_DEBUG_SCML
  cout << "CARBON-DEBUG: " << sc_time_stamp().value() << " Bitfield Register Write %(fieldName)s Value=0x" << hex << newVal << dec << endl;
#endif

%(userCodeEnd)s
}
''' % {
      "userCodeBegin" : self.module().userCodeSection("h", "Pre", userCodeName),
      "userCodeEnd" : self.module().userCodeSection("h", "Post", userCodeName),
      "numWords" : str(self.numCarbonUInt32()),
      "regName" : self.varName(),
      "varName" : self.carbonVarName() + "_" + self.getCppPathName(loc) + "_" + field.name(),
      "fieldType": self.fieldType(),
      "fieldName": self.bitfieldName(field),
      "moduleName": self.module().scModuleName()
      }
    return code

  def emitBitfieldWriteArray(self,field,loc):
    userCodeName = 'BFWA' + str(self.__size) + ' ' + self.bitfieldName(field) + '_' + self.getCppPathName(loc)
    code = """\
inline void %(moduleName)s::bf_write_%(fieldName)s(%(fieldType)s newVal)
{
  CarbonUInt32 currValue[%(numWords)s];
  memset(currValue, 0, sizeof(currValue));
%(userCodeBegin)s

  %(regName)s = newVal;
  carbonExamineMemory(%(varName)s, %(memIndex)s, currValue);
  CarbonAbstractRegisterXfer(m_ar_write_%(fieldName)s, &newVal, currValue);
  carbonDepositMemory(%(varName)s, %(memIndex)s, currValue);

#if _CARBON_DEBUG_SCML
  cout << "CARBON-DEBUG: " << sc_time_stamp().value() << " Bitfield Array Write %(fieldName)s Value=0x" << hex << newVal << dec << endl;
#endif

%(userCodeEnd)s
}
""" % {
      "userCodeBegin" : self.module().userCodeSection("h", "Pre", userCodeName),
      "userCodeEnd" : self.module().userCodeSection("h", "Post", userCodeName),
      "numWords" : str(self.numCarbonUInt32()),
      "memIndex": str(loc.index()),
      "regName" : self.varName(),
      "varName" : self.carbonVarName() + "_" + self.getCppPathName(loc) + "_" + field.name(),
      "fieldType": self.fieldType(),
      "fieldName": self.bitfieldName(field),
      "moduleName": self.module().scModuleName()
      }
    return code
  

class ScmlRegister32(ScmlRegisterCommon):
  def __init__(self, mod, reg):
    ScmlRegisterCommon.__init__(self, mod, reg, 32)

  def cppType(self):
    return "unsigned int"


#
# 64 Bit Implementations
#
class ScmlRegister64(ScmlRegisterCommon):
  def __init__(self, mod, reg):
    ScmlRegisterCommon.__init__(self, mod, reg, 64)
    
  def cppType(self):
    return "unsigned long long"

#
# 8 Bit Implementations
#
class ScmlRegister8(ScmlRegisterCommon):
  def __init__(self, mod, reg):
    ScmlRegisterCommon.__init__(self, mod, reg, 8)
    
  def cppType(self):
    return "unsigned char"

#
# 16 Bit Implementations
#
class ScmlRegister16(ScmlRegisterCommon):
  def __init__(self, mod, reg):
    ScmlRegisterCommon.__init__(self, mod, reg, 16)
    
  def cppType(self):
    return "unsigned short"
