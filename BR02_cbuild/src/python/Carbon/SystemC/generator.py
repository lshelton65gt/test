#******************************************************************************
# Copyright (c) 2010-2012 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

##
# \file
# Classes that represent generation logic for the SystemC wrapper

import os
import re
import subprocess

import Carbon
import Carbon.Cfg
from Carbon.carboncfg import *

# Base class for generator, variants should derive from this and overload functions
class ScGenerator:
  def __init__(self, config, args, ifaceName):
    # Class to manage the output files and user code
    self.__outputFiles = ScOutputFiles(config, args, ifaceName)

    # class that maintains objects that make up the SystemC wrapper
    self.__moduleObjects = ScModuleObjects(config, self.__outputFiles)

    # Keep the config for convenience
    self.__config = config
    
  # Initialize the module objects
  def init(self):
    self.__moduleObjects.init()
    self.__moduleObjects.fileIncludes.addObject(self.carbonModelInclude())

  # Get the ScConfig class
  def config(self):
    return self.__config

  # Get the object to emit files
  def outputFiles(self):
    return self.__outputFiles

  # Get the generator data
  def moduleObjects(self):
    return self.__moduleObjects
    
  # Return the pre/post section name for h/cpp user code
  def userCodeSection(self, file, position, section):
    return self.outputFiles().userCodeSection(file, position, section)

  def scModuleName(self):
    return self.outputFiles().scModuleName()

  def generate(self):
    # Open the header and cpp files
    self.outputFiles().openFiles()
    
    # Write the two files
    self._writeInitOutputs = self.__generateInits()
    self._writeOutputs = self.__generateOutputs()
    self.outputFiles().writeHeaderFile(self.__generateHeader())
    self.outputFiles().writeCppFile(self.__generateCode())
    self.outputFiles().close()

  def __generateTieOffInit(self, tie, userCode):
    varname = tie.carbonName().replace(".","_")
    userCodeBegin =  userCode.preSection('TIE ' + varname)
    userCodeEnd = userCode.postSection('TIE ' + varname)
    code = "// Tie off: " + tie.carbonName() + "\n"
    code += userCodeBegin + "\n"
    code += "  carbonNetID tie" + varname + ' = carbonFindNet(carbonModelHandle, \"' + tie.carbonName() + '\");\n'
    code += '  assert(' + varname + ');\n' # //this_assert_OK
    code += '  carbonDepositFast(carbonModelHandle, %(varName)s, %(byReference)scurrValue, NULL);\n'

    if tie.numWords() == 1:
      code = """\
  // Tie off %(varName)s
  CarbonUInt32 tie%(varName)s_value=%(tieValue)s;
  CarbonNetID * tie%(varName)s = carbonFindNet(carbonModelHandle, "%(carbonName)s");
  carbonDepositFast(carbonModelHandle, tie%(varName)s, &tie%(varName)s_value, NULL);
%(userCodeEnd)s  
""" % { "varName": varname,
        "userCodeEnd": userCodeEnd,
        "carbonName": tie.carbonName(),
        "tieValue": hex(tie.getWord(0))}

    else:
     code = """\
  // Tie off %(varName)s
  CarbonUInt32 tie%(varName)s_value[%(numWords)s];
  tie%(varName)s_value[0] = %(tieValue0)s;
  tie%(varName)s_value[1] = %(tieValue1)s;
  CarbonNetID * tie%(varName)s = carbonFindNet(carbonModelHandle, "%(carbonName)s");
  carbonDepositFast(carbonModelHandle, tie%(varName)s, tie%(varName)s_value, NULL);
%(userCodeEnd)s
""" % { "varName": varname,
        "userCodeEnd": userCodeEnd,
        "numWords": tie.numWords(),
        "carbonName": tie.carbonName(),
        "tieValue0": hex(tie.getWord(0)),
        "tieValue1": hex(tie.getWord(1)),
        }

    return code
  
  def __generateInits(self):
    code = ""

    # code for clocks & observables
    for port in self.config().scOutputs():
      code += port.generateInitializeOutput(self.outputFiles().cppUserCode())
        
    for port in self.config().scClockOutputs():
      code += port.generateInitializeOutput(self.outputFiles().cppUserCode())

    for tie in self.config().scTies():
      code += self.__generateTieOffInit(tie, self.outputFiles().cppUserCode())

    return code

  def __generateOutputs(self):
    def writeQueuedChanges(delayed = ''):
      return """\
  // perform any writes queued up by net change callbacks
  while (mCarbon%(delayed)sWriteFuncs != NULL) {
    void (*func)(void *) = (void (*)(void *)) mCarbon%(delayed)sWriteFuncs;
    func(mCarbon%(delayed)sWriteData);
  }
""" % { 'delayed': delayed }

    def writeInoutPorts():
      code = ''
      for port in self.config().scOutputs():
        writeCode = port.generateWriteOutput(self.outputFiles().cppUserCode())
        if writeCode != '':
          code += writeCode
      return code

    callDelayedWrites = ''
    if self.moduleObjects().hasDelayedOutputs():
      writeInouts = ''
      writeDelayedInouts = writeInoutPorts()
      callDelayedWrites = """\
    carbon_schedule_channel.carbon_write_delayed_outputs_event.notify(SC_ZERO_TIME);
  """
    else:
      writeInouts = writeInoutPorts()
      writeDelayedInouts = ''

    code = """\
void %(scName)s::carbon_write_outputs()
{
  %(writeQueuedChanges)s
%(writeInouts)s
%(callDelayedWrites)s
}
""" % { 'scName': self.scModuleName(),
        'writeInouts': writeInouts,
        'writeQueuedChanges': writeQueuedChanges(),
        'callDelayedWrites' : callDelayedWrites }

    if self.moduleObjects().hasDelayedOutputs():
      code += """
void %(scName)s::carbon_write_delayed_outputs()
{
  %(writeDelayedOutputs)s
  %(writeDelayedInouts)s
}
""" % { 'scName': self.scModuleName(),
        'writeDelayedOutputs': writeQueuedChanges('Delayed'),
        'writeDelayedInouts': writeDelayedInouts }
    return code
      
  def __generateHeader(self):

    code = """\
#define __CARBON_SYS_INCLUDE_H__

#ifdef CARBON_SC_TRACE
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
using namespace std;
#endif
#include <cctype>

%(defines)s
%(fileIncludes)s

%(classDefine)s%(baseClasses)s
{
  #include "carbon/carbon_scmodule.h"
  #include "carbon/carbon_valutil.h"
  #include "carbon/carbon_sctypes.h"

  %(userCodePreDeclarations)s
  
  // SytemC Module Ports
%(ports)s

  static double getSimTime(void* clientData);

  virtual void start_of_simulation();
  virtual void end_of_simulation();

  /*
   * To instantiate the model without running the initialization
   * code (i.e. Verilog initial blocks) you should pass 'false' as the
   * value for 'initialize'.  This is important if you are going to
   * restore a simulation from a checkpoint file and have initialization
   * code (e.g. open file for write) that will affect the restored
   * simulation.
   */
  SC_HAS_PROCESS(%(scName)s);
  %(scName)s(sc_module_name inst_name, bool initialize = true, bool dumpWaves = false, CarbonWaveFileType waveType = eWaveFileTypeVCD)
  %(constructorInitializers)s
  {
    bool scheduleAllClockEdges;

#if CARBON_DUMP_VCD
    dumpWaves = true;
    waveType = eWaveFileTypeVCD;
#endif

#if CARBON_DUMP_FSDB
    dumpWaves = true;
    waveType = eWaveFileTypeFSDB;
#endif

#if CARBON_SCHED_ALL_CLKEDGES
    scheduleAllClockEdges = true;
#else
    scheduleAllClockEdges = dumpWaves;
#endif
  
    carbonModelName = "%(topLevelModule)s";
    carbonWaveHandle = NULL; // No wave file yet defined.

    // set these to NULL, since there are not yet any writes queued up
    mCarbonWriteData = NULL;
    mCarbonWriteFuncs = NULL;
    mCarbonDelayedWriteData = NULL;
    mCarbonDelayedWriteFuncs = NULL;

#ifndef CARBON_SC_USE_2_0
    // register a handler for generic Carbon messages
    carbonAddMsgCB(NULL, sCarbonMessage, NULL);
#endif

    // Get the system, this disables any Replay/OnDemand messages we don't want to
    // see
    CarbonSys* system = carbonGetSystem(NULL);

    // Create an instance of the Carbon Model.
    %(carbonCreate)s
    if (carbonModelHandle == NULL) {
#ifdef CARBON_SC_USE_2_0
      cerr << "Unable to create instance of Carbon model " << carbonModelName;
      exit(1);
#else
      SC_REPORT_FATAL("Carbon", "Unable to create instance of Carbon model.");
#endif
    }
    else {
      %(userCodePreInit)s

#ifndef CARBON_SC_USE_2_0
      // register a handler for Carbon Model-specific Carbon messages
      carbonAddMsgCB(carbonModelHandle, sCarbonMessage, NULL);
#endif

      if (initialize) {
%(carbonInitialize)s

        if (dumpWaves) {
          if (waveType == eWaveFileTypeFSDB) {
            carbonSCWaveInitFSDB();
          }
          else {
            carbonSCWaveInitVCD();
          }
          carbonSCDumpVars();
        }
      }

      // a method to write the values of all outputs from the model to SystemC
      SC_METHOD( carbon_write_outputs );
      sensitive << carbon_schedule_channel.carbon_write_outputs_event;
      dont_initialize();

%(constructorStatements)s

      %(userCodePostInit)s
    }
  }

  // Release the net handles and Carbon Model when the
  // module instance is deleted.
  ~%(scName)s()
  {
%(terminate)s
  }

%(publicDeclarations)s
%(inlinePublicMethods)s

private:
%(privateDeclarations)s

  // request_update/update object used to call carbon_schedule() once per
  // delta cycle.
  class CarbonScheduleChannel: public sc_prim_channel
  {
    %(scName)s* mParent;
    CarbonObjectID *mCarbonModelHandle;
    public:
    sc_event carbon_write_outputs_event;
    sc_event carbon_write_delayed_outputs_event;
    void setCarbonModel( CarbonObjectID *cm_handle ) { mCarbonModelHandle = cm_handle; }
    void setParent(%(scName)s* parent) { mParent = parent; }
    void requestSchedule()
    {
#ifdef CARBON_SC_TRACE
    cout << "CARBON_SC_TRACE: carbonSchedule requested." << endl;
#endif
      %(userCodePreRequestSchedule)s
      request_update();
      %(userCodePostRequestSchedule)s
    }
    virtual void update() { 
#ifdef CARBON_SC_TRACE
    cout << "CARBON_SC_TRACE: calling carbonSchedule" << endl;
#endif
      %(userCodePreScheduleUpdate)s
      %(carbonSchedule)s
      carbon_write_outputs_event.notify( SC_ZERO_TIME );
      %(userCodePostScheduleUpdate)s
    }
  };
  void carbon_write_outputs();

  %(writeDelayedDeclaration)s
  CarbonScheduleChannel carbon_schedule_channel;

  // lists of data and functions for writing outputs and delayed outputs
  void *mCarbonWriteData;
  void *mCarbonWriteFuncs;
  void *mCarbonDelayedWriteData;
  void *mCarbonDelayedWriteFuncs;

  // interface data
%(dataMembers)s
  
  /* The next 2 defines control waveform dumping. See carbon_scmodule.h
   * for details about carbonSCDumpVars.
   * define CARBON_DUMP_FSDB to be 1 if you want to dump fsdb waves.
   * define CARBON_DUMP_VCD to be 1 if you want to dump vcd.
   * FSDB takes precedence over VCD.
   * The name of the file is the instance name + .vcd or .fsdb appropriately
   * Note that you can define these on the gcc compile line with -D.
   * E.g., -DCARBON_DUMP_FSDB=1

   * Beware that defining this on the gcc compile line will dump waves
   * for all carbon wrapped designs.
   * All instances of a design can be dumped by manually changing the wrapper
   * to define one of the dump options.
   */

#ifndef CARBON_DUMP_FSDB
#define CARBON_DUMP_FSDB 0
#endif
#ifndef CARBON_DUMP_VCD
#define CARBON_DUMP_VCD 0
#endif

  /* This define allows waveforms to be updated on every clock edge.
   * But, it can be used generally without waveforms. Note that use of
   * this define impacts performance, as normally an irrelevant
   * edge of a clock is ignored.
   *
   * define CARBON_SCHED_ALL_CLKEDGES to be 1 if you want the schedule
   * function to be called on every clock edge.
   * Note that you can define this on the gcc compile line with -D.
   * E.g., -DCARBON_SCHED_ALL_CLKEDGES=1
   *
   * Be aware that defining this on the gcc compile line will cause all
   * wrapped carbon designs to schedule on every clock edge.
   * Note also that if either CARBON_DUMP_FSDB or CARBON_DUMP_VCD are
   * defined this is automatically set to 1.
   */

#ifndef CARBON_SCHED_ALL_CLKEDGES
#define CARBON_SCHED_ALL_CLKEDGES 0
#endif

#if CARBON_DUMP_VCD || CARBON_DUMP_FSDB
#undef CARBON_SCHED_ALL_CLKEDGES
#define CARBON_SCHED_ALL_CLKEDGES 1
#endif

%(inlinePrivateMethods)s

#ifndef CARBON_SC_USE_2_0
  // this function will be registered to forward any Carbon messages to the
  // SystemC report handler.
  static eCarbonMsgCBStatus sCarbonMessage(void *data, CarbonMsgSeverity sev, int, const char *text, unsigned int)
  {
    switch (sev) {
      case eCarbonMsgSuppress:
        // do nothing - message has been supressed
        break;
      case eCarbonMsgStatus:
      case eCarbonMsgNote:
        SC_REPORT_INFO("Carbon", text);
        break;
      case eCarbonMsgWarning:
        SC_REPORT_WARNING("Carbon", text);
        break;
      case eCarbonMsgError:     // Error message
      case eCarbonMsgAlert:     // Demotable Error message
        SC_REPORT_ERROR("Carbon", text);
        break;
      case eCarbonMsgFatal:     // Fatal error message
        SC_REPORT_FATAL("Carbon", text);
        break;
      default:
        SC_REPORT_ERROR("Carbon", text);
        break;
    }
    // this message has been handled; do not call any other handlers.
    return eCarbonMsgStop;
  }
#endif

  %(userCodePostDeclarations)s
};

%(privateMethods)s
""" % { "defines": self.moduleObjects().defines.emitObjects('#define ', '\n#define ', ''),
        "fileIncludes": self.moduleObjects().fileIncludes.emitObjects('#include ', '\n#include ', ''),
        "userCodePreDeclarations": self.userCodeSection("h", "pre", ' DECLARATIONS'),
        "userCodePostDeclarations": self.userCodeSection("h", "post", ' DECLARATIONS'),
        "userCodePreScheduleUpdate": self.userCodeSection("h", "pre", ' SCHEDULE UPDATE'),
        "userCodePostScheduleUpdate": self.userCodeSection("h", "post", ' SCHEDULE UPDATE'),
        "userCodePreRequestSchedule": self.userCodeSection("h", "pre", ' REQUEST SCHEDULE'),
        "userCodePostRequestSchedule": self.userCodeSection("h", "post", ' REQUEST SCHEDULE'),
        "userCodePreInit": self.userCodeSection("h", "pre", ' INIT'),
        "userCodePostInit": self.userCodeSection("h", "post", ' INIT'),
        "ifaceName": self.outputFiles().interfaceName(),
        "safeIfaceName": self.outputFiles().cppInterfaceName(),
        "scName": self.scModuleName(),
        "topLevelModule": self.outputFiles().carbonTopName(),
        "carbonSchedule": self.carbonSchedule(),
        "ports": self.moduleObjects().ports.emitObjects('  ', '\n  ', ''),
        "publicDeclarations": self.moduleObjects().publicDeclarations.emitObjects('  ', '\n  ', ''),
        "privateDeclarations": self.moduleObjects().privateDeclarations.emitObjects('  ', '\n  ', ''),
        "constructorStatements": self.moduleObjects().constructorStatements.emitObjects('    ', '\n    ', ''),
        "privateMethods" : self.moduleObjects().privateMethods.emitObjects('\n\n', '\n\n', ''),
        "dataMembers": self.moduleObjects().dataMembers.emitObjects('  ', '\n  ', ''),
        "writeDelayedDeclaration": ("","void carbon_write_delayed_outputs();")[len(self.config().scOutputs()) > 0 and len(self.config().scClockOutputs()) > 0],
        "classDefine": self.classDefine(),
        "baseClasses": self.moduleObjects().baseClasses.emitObjects(': ', ', ', ''),
        "inlinePublicMethods": self.moduleObjects().inlinePublicMethods.emitObjects('  ', '\n  ', ''),
        "inlinePrivateMethods": self.moduleObjects().inlinePrivateMethods.emitObjects('  ', '\n  ', ''),
        "constructorInitializers": self.moduleObjects().getConstructorInitializers(),
        "carbonCreate": self.carbonCreate(),
        "terminate": self.terminate(),
        "carbonInitialize": self.carbonInitialize() }
    return code

  def carbonModelInclude(self):
    return '"lib%(ifaceName)s.h"' % {'ifaceName': self.outputFiles().interfaceName() }
  
  def classDefine(self):
    code = 'SC_MODULE(%(scName)s)' % {'scName': self.scModuleName()}
    return code

  def constructs(self):
    return ''

  def carbonCreate(self):
    code = '   carbonModelHandle = carbon_%(safeIfaceName)s_create(eCarbonAutoDB, CarbonInitFlags(eCarbon_Replay | eCarbon_OnDemand | eCarbon_NoInit));' % {"safeIfaceName": self.outputFiles().cppInterfaceName() }
    return code

  def carbonSchedule(self):
    return '      carbonSchedule( mCarbonModelHandle, sc_time_stamp().value() );'

  def terminate(self):
    code = """\
    if (carbonModelHandle != NULL) {
      carbonDestroy(&carbonModelHandle);
    }
"""
    return code

  def carbonInitialize(self):
    return '        carbonInitialize(carbonModelHandle, NULL, NULL, NULL);'
  
  def __generateCode(self):
    code = """\
#include "%(headerFile)s"

#ifdef MTI_SYSTEMC
  SC_MODULE_EXPORT(%(scName)s);
#endif

#ifdef NC_SYSTEMC
  NCSC_MODULE_EXPORT(%(scName)s);
#endif

void %(scName)s::start_of_simulation()
{
%(userCodeSOSStart)s
  CarbonSys* system = carbonGetSystem(NULL);
  if (system)
  {
    int only_if_changed = 1;
    CarbonSystemReadCmdlineStatus status = carbonSystemReadFromGUI(system, only_if_changed);
    if (status == eCarbonSystemCmdError)
    {
     std::cerr << "Carbon System Command Error: " << carbonSystemGetErrmsg(system);
    }
    
    // Previously we used sc_get_default_time_unit() here.
    carbonSystemPutSimTimeUnits(system, "1 ns");
    carbonSystemPutSimTimeCB(system, %(scName)s::getSimTime, NULL);
  }
%(userCodeSOSEnd)s
}

void %(scName)s::end_of_simulation()
{
%(userCodeEOSStart)s
  CarbonSys* system = carbonGetSystem(NULL);
  if (system && !carbonSystemIsShutdown(system))
    carbonSystemUpdateGUI(system);
%(userCodeEOSEnd)s
}

void %(scName)s::end_of_elaboration()
{
%(userCodeEOEStart)s
  carbon_schedule_channel.setCarbonModel( carbonModelHandle );
  carbon_schedule_channel.setParent( this );

  %(bidirectInputChanges)s

  // Setup initial values for testing for changes also for SystemC
  %(writeInitOutputs)s

  // schedule a write of the initial values to SystemC
  carbon_schedule_channel.carbon_write_outputs_event.notify(SC_ZERO_TIME);

  // Update the system after the system name has been set up
  CarbonSys* system = carbonGetSystem(NULL);
  if(system && mSystemComp) {
    if (carbonSystemComponentReplayable(mSystemComp) ||
        carbonOnDemandIsEnabled(carbonModelHandle))
    {
      carbonSystemUpdateGUI(system);
    }
  }

%(userCodeEOEEnd)s
}

%(writeOutputs)s

double %(scName)s::getSimTime(void* /* clientdata*/)
{
  return sc_time_stamp().to_seconds() * 1e9; // Convert to ns
}
""" % { "scName": self.scModuleName(),
        "userCodeSOSStart": self.userCodeSection("cpp", "pre", "Start Of Simulation"),
        "userCodeSOSEnd": self.userCodeSection("cpp", "post", "Start Of Simulation"),
        "userCodeEOEStart": self.userCodeSection("cpp", "pre", "End Of Elaboration"),
        "userCodeEOEEnd": self.userCodeSection("cpp", "post", "End Of Elaboration"),
        "userCodeEOSStart": self.userCodeSection("cpp", "pre", "End Of Simulation"),
        "userCodeEOSEnd": self.userCodeSection("cpp", "post", "End Of Simulation"),
        "headerFile": self.outputFiles().hFileName(),
        "bidirectInputChanges" : "",
        "writeDelayedOutputs" : "",
        "writeOutputs": self._writeOutputs,
        "writeInitOutputs": self._writeInitOutputs }

    return code

# Class to emit a SystemC module
class ScGeneratorSystemC(ScGenerator):
  def __init__(self, config, args, ifaceName):
    ScGenerator.__init__(self, config, args, ifaceName)
    self.__computeTimingMap()

  # Overload the init function so we can add the xtor objects
  def init(self):
    # init the base class, then add any TLM2 objects
    ScGenerator.init(self)

    # Add any transactor objects
    ambaAdded = False
    debugAccessNeeded = False
    for xtor in self.config().transactors():
      # Add the various parts to the SystemC module objects. Start
      # with the include files:
      # - amba.h - ambasocket definitions
      # - CarbonStandAloneComponentIF - needed for debug access
      # - transactor include file
      for includeFile in xtor.xtor().includeFiles():
        if not ambaAdded and xtor.xtor().isAmba():
          # Note that we need to set the SC_INCLUDE_DYNAMIC_PROCESSES macro so that
          # sc_spawn_options is defined in systemc.h
          self.moduleObjects().defines.addObject('SC_INCLUDE_DYNAMIC_PROCESSES')
          self.moduleObjects().defines.addObject('CARBON_DEBUG_ACCESS')
          self.moduleObjects().fileIncludes.addObject('"amba.h"')
          self.moduleObjects().fileIncludes.addObject('"amba_tlm_version.h"')
          self.moduleObjects().fileIncludes.addObject('"carbon/CarbonStandAloneComponentIF.h"')
          ambaAdded = True
        self.moduleObjects().fileIncludes.addObject('"%s"' % includeFile)

      # Add the class declarations:
      # - socket port
      # - transactor instance
      self.moduleObjects().ports.addObject(xtor.socketDecl())
      self.moduleObjects().privateDeclarations.addObject("transactor declarations", xtor.instanceDeclaration())

      # If the transactor supports debug access, add the component
      # debug access function and the transactor debug access class.
      # transactor
      if xtor.xtor().debugTransaction():
        # Indicate that we need a debug access function in the component
        debugAccessNeeded = True

        # Add the transactor debug access code
        userCodeHitBytesPre = self.userCodeSection('h', 'pre', ' ' + xtor.name() + ' Num Hit Bytes')
        userCodeHitBytesPost = self.userCodeSection('h', 'post', ' ' + xtor.name() + ' Num Hit Bytes')
        self.moduleObjects().privateDeclarations.addObject("transactor debug access classes",
                                                    xtor.debugAccessClassDecl(userCodeHitBytesPre,
                                                                              userCodeHitBytesPost))
        self.moduleObjects().dataMembers.addObject(xtor.debugAccessMemberDecl())
        self.moduleObjects().variableInitializers.addObject(xtor.debugAccessConstructorInit())
        group = "register slave transactor debug access"
        self.moduleObjects().constructorStatements.addObject(group, xtor.debugAccessRegisterCB())

      # Add the class member functions. This is the timing listener
      # for this transactor
      config = self.config()
      self.moduleObjects().inlinePrivateMethods.addObject(xtor.defineTimingListener(self.__scPortTimingMap))

      # Add the constructor initializers
      self.moduleObjects().variableInitializers.addObject(xtor.initSocket())
      self.moduleObjects().variableInitializers.addObject(xtor.initAdapter())

      # Add constructor statements
      # - connect signals from Carbon Model to the transactor
      # - connect the timing listener to the transactor
      # - bind the ammba socket ports to the transactor
      connectGroup = "connect transactor's RTL side to signals connected to the model pins"
      for stmt in xtor.connectSignals():
        self.moduleObjects().constructorStatements.addObject(connectGroup, stmt)
      timingGroup = "register timing listener methods"
      for fanoutXtor in self.__xtorTimingMap.get(xtor, []):
        code = xtor.registerTimingListeners(config.ccfg().compName(), fanoutXtor)
        self.moduleObjects().constructorStatements.addObject(timingGroup, code)
      group = "bind socket ports to transactors"
      self.moduleObjects().constructorStatements.addObject(group, xtor.connectAdapterSocket())

      # Deal with unconnected pins
      for (portName, scDataType) in xtor.unconnected():
        # Add a dummy signal to connect it
        declName = "%s_%s" % (xtor.name(), portName)
        decl = "%s %s;" % (scDataType, declName)
        self.moduleObjects().privateDeclarations.addObject("signal declarations", decl)

        # Connect it to the transactor
        connect = xtor.connectSignalPort(portName, declName)
        self.moduleObjects().constructorStatements.addObject(connectGroup, connect)

      # Add the monitor access function if needed
      self.moduleObjects().inlinePublicMethods.addObject(xtor.monitorFunction())

    # Add the component debug access function.  This function is the
    # spot to manage the component address map and pass debug
    # transactions from slave tranactors to either local access or
    # pass it on to a master
    if debugAccessNeeded:
      self.moduleObjects().inlinePublicMethods.addObject(self.__componentDebugAccess())

    # Add an amba version check to the constructor if it needs one
    if ambaAdded:
      self.moduleObjects().constructorStatements.addObject("verion check", self.__ambaVersionCheck())

  # Overload the terminate function to add the terminate statements
  def terminate(self):
    # Add the common statements
    code = ScGenerator.terminate(self)

    # Add any transactor destruction
    for xtor in self.config().transactors():
      code += '    ' + xtor.deleteAdapter() + ';\n'
    return code
  
  # Analyze the transactor ports and populate the timing map. This is
  # needed to compute the timing listener functions per transactor.
  # The timing map goes from an scPort to [xtor, scPort]
  # The direction is from any async inputs to async outputs
  def __computeTimingMap(self):
    # Create a map from dbNode's to scPort's for fast lookup below
    dbScPortMap = {}
    for xtor in self.config().transactors():
      for scPort in xtor.scPorts():
        dbNode = scPort.dbNode()
        dbScPortMap[dbNode.fullName()] = [scPort, xtor]

    # Compute the timing maps by going through the transactors again and
    # Creating a map from ScPort to fanout xtor and ScPort as well
    # as a map from xtor to xtor
    self.__scPortTimingMap = {}
    self.__xtorTimingMap = {}
    for xtor in self.config().transactors():
      for scPort in xtor.scPorts():
        dbNode = scPort.dbNode()
        if dbNode.isAsyncOutput():
          for faninDBNode in self.config().getDB().iterAsyncFanin(dbNode):
            if dbScPortMap.has_key(faninDBNode.fullName()):
              # There is a connection, record the scPort from fanin to fanout
              # scPort and xtor
              (faninScPort, faninXtor) = dbScPortMap[faninDBNode.fullName()]
              if not self.__scPortTimingMap.has_key(faninScPort):
                self.__scPortTimingMap[faninScPort] = []
              self.__scPortTimingMap[faninScPort].append([xtor, scPort])

              # Also record the xtor to xtor timing map as long as it is not a self reference
              if faninXtor != xtor:
                if not self.__xtorTimingMap.has_key(faninXtor):
                  self.__xtorTimingMap[faninXtor] = set()
                self.__xtorTimingMap[faninXtor].add(xtor)

  def __componentDebugAccess(self):
    code = """\
  //! Component Debug Access.
  /*! Debug Access function to be called by slave transactors. Add Code
   *  to resolve the debug access locally or pass it to the apporpriate master
   *  transactors.
  */
  CarbonDebugAccessStatus debugAccess(const std::string& callingSlave, CarbonDebugAccessDirection dir, uint64_t startAddress, uint32_t numBytes, uint8_t* data, uint32_t* numBytesProcessed, uint32_t* ctrl)
  {
    CarbonDebugAccessStatus status = eCarbonDebugAccessStatusOk;
%(userCodeComponentDebugAccessPre)s

    // Walk the master transactors and process the debug access
    uint64_t curAddr = startAddress;
    uint32_t curNumBytes = numBytes;
    uint32_t accessBytes = 0;
    uint32_t index = 0;
    while ((curNumBytes > 0) && (status == eCarbonDebugAccessStatusOk)) {
      // Perform the operation locally or on the the correct master transactor
%(userCodeComponentDebugLocalAccessPre)s
%(xtorAccess)s
%(userCodeComponentDebugLocalAccessPost)s

      // Update if any bytes were copied
      if (accessBytes > 0) {
        curAddr += accessBytes;
        curNumBytes -= accessBytes;
        index += accessBytes;
        accessBytes = 0;

      } else {
        // Failed, set status and exit loop
        status = eCarbonDebugAccessStatusOutRange;
      }
    }
    (*numBytesProcessed) = index;
     
%(userCodeComponentDebugAccessPost)s
    return status;
  }
""" % { 'userCodeComponentDebugAccessPre' : self.userCodeSection("h", "pre", " Component Debug Access"),
        'userCodeComponentDebugAccessPost' : self.userCodeSection("h", "post", " Component Debug Access"),
        'userCodeComponentDebugLocalAccessPre' : self.userCodeSection("h", "pre", " Component Debug Local Access"),
        'userCodeComponentDebugLocalAccessPost' : self.userCodeSection("h", "post", " Component Debug Local Access"),
        'xtorAccess': self.__xtorAccess() }
    return code

  def __xtorAccess(self):
    ifStmt = '      if'
    code = ''
    for xtor in self.config().transactors():
      if xtor.xtor().isMaster():
        code += '''\
%(ifStmt)s ((accessBytes == 0) && ((accessBytes = %(debugAccessMember)s.numHitBytes(curAddr, curNumBytes)) > 0)) {
        switch(dir)
        {
        case eCarbonDebugAccessRead:
          status = %(debugAccessMember)s.debugMemRead(curAddr, &data[index], accessBytes, ctrl);
          break;

        case eCarbonDebugAccessWrite:
          status = %(debugAccessMember)s.debugMemWrite(curAddr, &data[index], accessBytes, ctrl);
          break;
        }

     }''' % { 'ifStmt': ifStmt,
              'debugAccessMember':  xtor.debugAccessMember() }
      ifStmt = ' else if'
    return code

  def __ambaVersionCheck(self):
    versionMap = { 'major': 1,
                   'minor': 0,
                   'revision': 13,
                   'build': 592 }
    code = '''\
#define REQUIRED_TLM_VERSION_MAJOR %(major)d
#define REQUIRED_TLM_VERSION_MINOR %(minor)d
#define REQUIRED_TLM_VERSION_REVISION %(revision)d
#define REQUIRED_TLM_VERSION_BUILD %(build)d
#define REQUIRED_TLM_VERSION_STRING "%(major)d.%(minor)d.%(revision)d Build %(build)d"

// Must be at least version %(major)d.%(minor)d.%(revision)d
  if ((AMBA_TLM_VERSION_MAJOR < REQUIRED_TLM_VERSION_MAJOR) ||
      ((AMBA_TLM_VERSION_MAJOR == REQUIRED_TLM_VERSION_MAJOR) && (AMBA_TLM_VERSION_MINOR < REQUIRED_TLM_VERSION_MINOR)) ||
      ((AMBA_TLM_VERSION_MAJOR == REQUIRED_TLM_VERSION_MAJOR) && (AMBA_TLM_VERSION_MINOR == REQUIRED_TLM_VERSION_MINOR) && (AMBA_TLM_VERSION_REVISION < REQUIRED_TLM_VERSION_REVISION))) {
    std::ostringstream msg;
    msg << "verion check failed, expecting: " << REQUIRED_TLM_VERSION_STRING << ", using: " << AMBA_TLM_VERSION_STRING << ".";
    SC_REPORT_ERROR("Carbon", msg.str().c_str());
  }''' % versionMap
    return code
              
class ScGeneratorCoWare(ScGenerator):
  def __init__(self, config, args, ifaceName, defineMap):
    ScGenerator.__init__(self, config, args, ifaceName)

    # Extra defines to add to the Makefile compile
    self.__defineMap = defineMap
    
    # use 'svsd_version' to determine the CoWare version.
    self.__cowareMajorVersion = 0
    cowareHome = os.getenv("COWAREHOME")
    version_checker = "svsd_version"
    if cowareHome:
      pipe = os.popen(version_checker)
      if pipe  == None:
        raise "Could not run 'svsd_version' to determine version"
      versionString = pipe.readline()
      if len(versionString) == 0:
         print "Error: '%s' failed to return a version string. This is probably because it is not in the path.\n" % version_checker
         print "Error:  '%s' is usually located under COWAREHOME. Add this to the path and try again.\n" % version_checker
         raise "Unable to find 'svsd_version' in the path."
#     newer versions begin with F-, E- and E causing int cast to fail.
      versionString = re.sub('^[^0-9]*','',versionString)
      
      versionFields = versionString.split('.')

      self.__cowareMajorVersion = int(versionFields[0])

      if ((self.__cowareMajorVersion != 2012) and (self.__cowareMajorVersion != 2011)):
        raise "Unexpected CoWare version '%s'; expecting major version V2011 or V2012" % versionString
    
  # Overload
  def init(self):
    # First init the base class.  Then add CoWare specific objects
    ScGenerator.init(self)

    # Set the base classes for V2010 and beyond.  State change
    # observer is only available in versions 2010 and later
    config = self.config()
    if self.__cowareMajorVersion >= 2010:
      self.moduleObjects().baseClasses.addObject('public sc_module')
      self.moduleObjects().baseClasses.addObject('public nCoWare::nSimRunState::Observer')
      self.moduleObjects().baseClassInitializers.addObject('Observer()')

    # Add the module objects for the registers grouped by size
    for (size, type) in [(8, "char"), (16, "short"), (32, "int"), (64, "long long")]:
      if config.numScmlRegisters(size) > 0:
        # Figure out the replacement variables
        varMap = { 'size'               : size,
                   'type'               : type,
                   'alloc_reg_size'     : self.__maxScmlRegOffset(config, size) + 1 }

        # Member variables
        var = "scml_memory <unsigned %(type)s> m_Registers%(size)d; // %(size)d Bit Register Bank" % varMap
        self.moduleObjects().publicDeclarations.addObject("SCML", var)
      
        # constructor variable initializers
        init = 'm_Registers%(size)d("Registers%(size)d", scml_memsize(%(alloc_reg_size)d))' % varMap
        self.moduleObjects().variableInitializers.addObject(init)

        # constructor statements
        code = "m_Registers%(size)d.set_trigger_behaviour_on_debug_read(true);" % varMap
        self.moduleObjects().constructorStatements.addObject("SCML Register Init", code)
        code = "m_Registers%(size)d.set_trigger_behaviour_on_debug_write(true);" % varMap
        self.moduleObjects().constructorStatements.addObject("SCML Register Init", code)

    # Add the module objects for each individual registers
    for reg in config.scRegisters():
      # constructor variable initializers
      for initializer in reg.getConstructorInitializers():
        self.moduleObjects().variableInitializers.addObject(initializer)

      # member variables
      self.moduleObjects().publicDeclarations.addObject("SCML Registers", reg.emitDeclaration())

      # constructor statements
      self.moduleObjects().constructorStatements.addObject("SCML Register Init", reg.emitCarbonRegisterInit())
      code = reg.emitCallbackDecl(self.__cowareMajorVersion >= 2010)
      self.moduleObjects().constructorStatements.addObject("SCML Register Callbacks", code)

      # Methods
      self.moduleObjects().privateMethods.addObject(reg.emitCallbackImplementation())

    # Add the module objects for each individual memories
    for mem in config.scMemories():
      # constructor variable initializers
      for initializer in mem.getConstructorInitializers():
        self.moduleObjects().variableInitializers.addObject(initializer)

      # member variables
      self.moduleObjects().publicDeclarations.addObject("SCML Memories", mem.emitDeclaration())

      # constructor statements
      self.moduleObjects().constructorStatements.addObject("SCML Memories Init", mem.emitCallbackDeclaration())
      self.moduleObjects().constructorStatements.addObject("SCML Memories Init", mem.emitAddressingMode())
      self.moduleObjects().constructorStatements.addObject("SCML Memories Init", mem.emitCarbonMemoryInit())

      # callback methods
      self.moduleObjects().privateMethods.addObject(mem.emitCallback(self.scModuleName()))

    # Add the include files
    self.moduleObjects().fileIncludes.addObject('<scml.h>')
    self.moduleObjects().fileIncludes.addObject('"carbon/CarbonAbstractRegister.h"')
    self.moduleObjects().fileIncludes.addObject('"carbon/carbon_coware.h"')

    # When targeting CoWare, emit the state changed function that will
    # flush waveforms.  This is only available in versions 2010 and
    # later.
    code = ""
    if self.__cowareMajorVersion >= 2010:
      code = '''\
  virtual void handleStateChanged(nCoWare::nSimRunState::eState newState)
  {
    if ((newState != nCoWare::nSimRunState::eRunning) && (carbonWaveHandle != NULL)) {
      carbonDumpFlush(carbonWaveHandle);
#ifdef CARBON_VERBOSE_WAVE_FLUSH
      std::cout << "CARBON: Flushing waveform at time " << sc_time_stamp().value() << std::endl;
#endif
    }
  }
'''
    self.moduleObjects().inlinePrivateMethods.addObject(code)

    # Add some V2010 only constructs
    if self.__cowareMajorVersion >= 2010:
      # state change callbacks only exist in CoWare V2010 and later
      self.moduleObjects().fileIncludes.addObject('<CwrBase/SimBase/SimRunState.h>')
      self.moduleObjects().constructorStatements.addObject("SCML Observer",
                                                           '    nCoWare::nSimRunState::addObserver(this);')

  # Overload
  def classDefine(self):
    code = ''
    if self.__cowareMajorVersion >= 2010:
      # State change observer is only available in versions 2010 and later
      code = 'struct %(scName)s' % {'scName': self.scModuleName()}
    else :
      code = ScGenerator.classDefine(self)
    return code
    
  def generate(self):
    # Run the common generation first
    ScGenerator.generate(self)

    # Write the CoWare additional files
    makeFileName = "Makefile.coware." + self.outputFiles().interfaceName()
    cmdFileName = "lib" + self.outputFiles().interfaceName() + ".coware.cmd"
    scriptFileName = "import_" + self.outputFiles().interfaceName() + ".coware.tcl"
    self.__generateImportScript(scriptFileName)
    makeFile = open(makeFileName, "w")
    makeFile.write(self.__generateMakefile(cmdFileName, self.config().dbFile()))
    makeFile.close()
    targetLibName = os.path.join(os.getcwd(),"lib"+self.outputFiles().interfaceName())
    if (not os.path.exists(targetLibName+".a")):
      targetLibName = os.path.abspath(os.path.join(os.getcwd(),"../lib"+self.outputFiles().interfaceName()))

    cmdArray = []
    # Posix for linux
    if os.name == "posix":
       makePath = os.path.join(os.path.join(os.environ['CARBON_HOME'],'bin'), 'make')
       cmdArray.extend([makePath, '-s', '-f', makeFileName, 'coware', 'TARGET='+targetLibName])
       ret = subprocess.call(cmdArray, shell=False)
    # For now, we are not generating tcl, and cmd files on windows. This is consistent with
    # how the modelkits work.
    # else:   
    #   makePath = os.path.join(os.path.join(os.environ['CARBON_HOME'], 'Win', 'bin'), 'make')
    #   cmdArray.extend([makePath, '-s', '-f', makeFileName, 'coware', 'TARGET='+targetLibName])
    #   # Does requires shell=True, unless cmd.exe is part of the command line
    #   ret = subprocess.call(cmdArray, shell=True)

  def generateCoWareGuardBegin(self):
    code = "\n#ifndef __COWARE_SCML_REGISTERS"
    return code

  def generateCoWareGuardEnd(self):
    code = "\n#endif // __COWARE_SCML_REGISTERS"
    return code

  def __targetLibraryName(self):
    targetLibName = os.path.join(os.getcwd(),"lib"+self.outputFiles().interfaceName())+".a"
    if (not os.path.exists(targetLibName)):
      return "../lib" + self.outputFiles().interfaceName() + ".a"
    else:
      return "lib" + self.outputFiles().interfaceName() + ".a"

  def __carbonVars(self):
    carbonModel = os.environ.get('CARBON_MODEL')
    if carbonModel == None:
      carbonModel = os.path.basename(self.__targetLibraryName())

    carbonOutput = os.environ.get('CARBON_OUTPUT')
    if carbonOutput == None:
      targetLibName = os.path.join(os.getcwd(),"lib"+self.outputFiles().interfaceName())
      if (not os.path.exists(targetLibName+".a")):
         carbonOutput = os.getcwd() + '/../'
      else:
         carbonOutput = os.getcwd()
    vars = """\
if { [ info exists env(CARBON_OUTPUT) ] } { set CARBON_OUTPUT $env(CARBON_OUTPUT) } else { set CARBON_OUTPUT %(carbonOutput)s }
if { [ info exists env(CARBON_MODEL) ] } { set CARBON_MODEL $CARBON_OUTPUT/$env(CARBON_MODEL) } else { set CARBON_MODEL $CARBON_OUTPUT/%(carbonModel)s }
""" % {
      "carbonOutput" : carbonOutput,
      "carbonModel" : carbonModel
      }
    return vars

  def __generateImportScript(self, scriptFileName):
    numTrans = len(self.config().transactors())
    if True:
      print "Transactors: " + str(len(self.config().transactors()))
      print "\nGenerating CoWare Import Script: " + scriptFileName
      ofile = open(scriptFileName, "w")

      code = """
set block SYSTEM_LIBRARY:%(ifaceName)s
set encap $block/%(ifaceName)s
""" % { "ifaceName" : self.scModuleName() }
      ofile.write(code)

      if self.__cowareMajorVersion >= 2007:
        # Since 2007.1 we can just emit the library name, not the path. This makes it easier
        for xtor in self.config().transactors():
          xtorLibName = xtor.findParam("library")
          if xtorLibName == None:
            xtorLibName = xtor.libraryName()
          code='''
::pct::open_library %(libName)s
::pct::copy_protocol_to_system_library %(libName)s:%(xtorName)s
::pct::close_library %(libName)s
''' % { "libName" : xtorLibName,
        "xtorName" : xtor.type() }
          ofile.write(code)
      else:
        # Delete this code when we can be done with versions before 2007.1
        cowareIP = os.environ.get('COWAREIPDIR', '$env(COWAREHOME)/IP')
        for xtor in self.config().transactors():
          xtorLibName = xtor.findParam("library")
          if xtorLibName == None:
            xtorLibName = xtor.libraryName()
          code="""
::pct::open_library %(cowareIP)s/%(libName)s/ConvergenSC/%(libName)s.xml
::pct::copy_protocol_to_system_library %(libName)s:%(xtorName)s
::pct::close_library %(libName)s
""" % { "libName" : xtorLibName,
        "cowareIP" : cowareIP,
        "xtorName" : xtor.type() }
          ofile.write(code)

      interfaceName = "$CARBON_OUTPUT/CoWare/lib" + self.outputFiles().interfaceName()
      carbonProject = os.environ.get('CARBON_PROJECT')
      if carbonProject == None:
        if os.path.exists(os.getcwd() + "/lib" + self.outputFiles().interfaceName() + ".systemc.cpp"):
          interfaceName = os.getcwd() + "/lib" +  self.outputFiles().interfaceName()
        else:
          interfaceName = os.getcwd() + "/CoWare/lib" +  self.outputFiles().interfaceName()

      code = """
%(carbonVars)s
::pct::set_preference_value /Messages/FilterIDs "DIM0023"
::pct::clear_systemc_defines
::pct::clear_systemc_include_path
::pct::set_import_protocol_generation_flag false
::pct::add_to_systemc_include_path %(incPath)s/include
::pct::add_to_systemc_include_path $CARBON_OUTPUT
::pct::load_all_modules %(intfaceName)s.systemc.cpp

""" % {
        "carbonVars" : self.__carbonVars(),
        "intfaceName" : interfaceName,
        "incPath" : "$env(CARBON_HOME)"
      }
      ofile.write(code)

      for xtor in self.config().transactors():
        for port in xtor.inputs():
          ofile.write("::pct::remove_block_port $block/" + port.carbonName().split('.')[-1] + "\n")

        for port in xtor.outputs():
          ofile.write("::pct::remove_block_port $block/" + port.carbonName().split('.')[-1] + "\n")
        
        code = '''
::pct::add_block_port $block %(xtorInstName)s InOut
::pct::set_block_port_protocol $block/%(xtorInstName)s SYSTEM_LIBRARY:%(xtorName)s
::pct::set_param_value $block/%(xtorInstName)s "Port Properties" Category Memory
::pct::set_param_value $block/%(xtorInstName)s "Port Properties" AddressingMode "1 Byte"
''' % {
        "xtorInstName" : xtor.name(),
        "xtorName" : xtor.type(),
        "ifaceName" : self.scModuleName(),
        "incPath" : "$env(CARBON_HOME)"
      }
        if xtor.variant() != '':
          code += '''\
::pct::set_param_value $block/%(xtorInstName)s "Port Properties" MasterSlaveness %(variant)s
''' % { 'xtorInstName': xtor.name(),
        'variant':      xtor.variant()
        }
          code += '\n'
        ofile.write(code)

        code = ""
        for port in xtor.inputs():
          code += """::pct::map_encap_port $block/%(ifaceName)s/%(carbonPortName)s %(xtorInstName)s %(portName)s
""" % {
        "xtorInstName" : xtor.name(),
        "portName" : port.name(),
        "carbonPortName" : port.carbonName().split('.')[-1],
        "ifaceName" : self.scModuleName()
      }
        ofile.write(code)


        code = ""
        for port in xtor.outputs():
          code += """::pct::map_encap_port $block/%(ifaceName)s/%(carbonPortName)s %(xtorInstName)s %(portName)s
""" % {
        "xtorInstName" : xtor.name(),
        "portName" : port.name(),
        "carbonPortName" : port.carbonName().split('.')[-1],
        "ifaceName" : self.scModuleName()
      }
        ofile.write(code)

      ofile.write("::pct::remove_block SYSTEM_LIBRARY:CarbonScheduleChannel\n");
      
      for port in self.config().scPorts():
        if self.__portXtorConnected(port.fullName()) == False:
          if port.scPortDirection() == Carbon.SystemC.config.ScPort.INPUT:
            ofile.write(self.__generateCowareControlInput(port))
          elif port.scPortDirection() == Carbon.SystemC.config.ScPort.OUTPUT:
            ofile.write(self.__generateCowareControlOutput(port))
          elif port.scPortDirection() == Carbon.SystemC.config.ScPort.INOUT:
            print "BiDi Unconnected: " + port.scName() + " " + str(port.scPortDirection())
   
      ofile.write("::pct::set_image $block $env(CARBON_HOME)/lib/images/carbon.xpm center center false true\n")
      ofile.write("::pct::set_encap_build_script $encap $CARBON_OUTPUT/CoWare/lib" + self.outputFiles().interfaceName() + "_build.tcl\n")

      encapText = """
foreach encap_source_file [::pct::list_encap_source_files $encap] {
  if { [regexp "\\\\.h" $encap_source_file] } {
    ::pct::remove_encap_source_file $encap $encap_source_file
  }
}
"""
      ofile.write(encapText)
      ofile.close();
    


  def __generateCowareControlInput(self, port):
    portMode = self.config().ccfg().portMode(port.scName())

    if portMode == eCarbonCfgESLPortControl or portMode == eCarbonCfgESLPortUndefined:
      code = """
::pct::set_param_value $block/%(portName)s "Port Properties" Category Control
::pct::set_param_value $block/%(portName)s "Port Properties" MasterSlaveness Slave
::pct::set_param_value $block/%(portName)s:%(ifaceName)s "Protocol Common Parameters" data_width %(bitWidth)s
""" % {
       "portName" : port.scName(),
       "bitWidth" : str(port.scDataType().bitSize()),
       "ifaceName" : self.scModuleName()
       }
      return code
    elif portMode == eCarbonCfgESLPortClock:
      code = """
::pct::set_block_port_protocol $block/%(portName)s SYSTEM_LIBRARY:CLOCK
""" % {
       "portName" : port.scName()
       }
      return code
    elif portMode == eCarbonCfgESLPortReset:
      code = """
::pct::set_block_port_protocol $block/%(portName)s SYSTEM_LIBRARY:RESET
""" % {
       "portName" : port.scName()
       }
      return code
    raise ScmlError('Port:' + port.scName() + ' Unsupported Port Mode: '+str(portMode))

  def __generateCowareControlOutput(self, port):
    portMode = self.config().ccfg().portMode(port.scName())

    if portMode == eCarbonCfgESLPortControl or portMode == eCarbonCfgESLPortUndefined:
      code = """
::pct::set_block_port_protocol $block/%(portName)s SYSTEM_LIBRARY:Default
::pct::set_param_value $block/%(portName)s "Port Properties" Category Control
::pct::set_param_value $block/%(portName)s "Port Properties" MasterSlaveness Master
::pct::set_param_value $block/%(portName)s:%(ifaceName)s "Protocol Common Parameters" data_width %(bitWidth)s
""" % {
       "portName" : port.scName(),
       "bitWidth" : str(port.scDataType().bitSize()),
       "ifaceName" : self.scModuleName()
       }
      return code
    elif portMode == eCarbonCfgESLPortClock:
      code = """
::pct::set_block_port_protocol $block/%(portName)s SYSTEM_LIBRARAY:CLOCK
""" % {
       "portName" : port.scName()
       }
      return code
    elif portMode == eCarbonCfgESLPortReset:
      code = """
::pct::set_block_port_protocol $block/%(portName)s SYSTEM_LIBRARAY:RESET
""" % {
       "portName" : port.scName()
       }
      return code
    raise ScmlError('Unsupported Port Mode')

  
  def __portXtorConnected(self, portName):
    for xtor in self.config().transactors():
      for port in xtor.inputs():
        if port.carbonName() == portName:
          return True
      for port in xtor.outputs():
        if port.carbonName() == portName:
          return True
    return False

  def __echoCarbonVars(self, rest):
    carbonModel = os.environ.get('CARBON_MODEL')
    if carbonModel == None:
      carbonModel = os.path.basename(self.__targetLibraryName())

    carbonOutput = os.environ.get('CARBON_OUTPUT')
    if carbonOutput == None:
      targetLibName = os.path.join(os.getcwd(),"lib"+self.outputFiles().interfaceName())
      if (not os.path.exists(targetLibName+".a")):
         carbonOutput = os.getcwd() + '/../'
      else:
         carbonOutput = os.getcwd()
    value = """\
	@echo if \{ [ info exists env\(CARBON_OUTPUT\) ] \} \{ set CARBON_OUTPUT '$$'env\(CARBON_OUTPUT\) \} else \{ set CARBON_OUTPUT %(carbonOutput)s \} >> $@
	@echo if \{ [ info exists env\(CARBON_MODEL\) ] \} \{ set CARBON_MODEL '$$'env\(CARBON_MODEL\) \} else \{ set CARBON_MODEL %(carbonModel)s \} >> $@\
""" % {
      "carbonOutput" : carbonOutput,
      "carbonModel" : carbonModel
      }
    return value

  def __echoFastLinkOffBuildOption(self):
    # With versions 2009 and later of CoWare, the fast link option
    # needs to be disabled (bug 12052).  We can't always emit the
    # option, though, because older CoWare versions don't understand
    # it.
    code = ""
    if self.__cowareMajorVersion >= 2009:
      code = "	@echo ::scsh::build-options -fast-link off >> $@"
    return code

  def __generateMakefile(self, cmdFileName, dbFile):
    extraDefines = ""
    for key in self.__defineMap.keys():
      extraDefines += "-D" + key + "=" + self.__defineMap[key] + " " 

    code='''
#
#dbfile: %(dbFile)s
#

include $(CARBON_HOME)/makefiles/Makefile.carbon.vsp

TARGETNAME:=$(notdir $(TARGET))
TARGETBASE:=$(basename $(TARGETNAME))

# '/path/libdesign' without an extension...
TARGETROOT:=$(basename $(TARGET))

# Determine the proper extension for the archive
AREXT_Win = lib
AREXT_Linux = a
AREXT_Linux64 = a
AREXT = $(AREXT_$(CARBON_TARGET_ARCH))

# Generate a config script for CoWare
coware: $(TARGETBASE).coware.cmd $(TARGETBASE).coware.lib.tcl libcarbon.coware.link.tcl $(TARGETBASE)_build.tcl

dummy:

$(TARGETBASE).coware.lib.tcl: dummy
	@echo generating $@
	@echo "#linker command file" > $@
%(echoCarbonVars)s
	@echo ::scsh::cwr_append_ipsimbld_opts linker '$$'CARBON_OUTPUT/'$$'CARBON_MODEL >> $@
%(echoFastLinkOffBuildOption)s

$(TARGETBASE)_build.tcl: dummy
	@echo generating $@
	@echo "#build command file" > $@
	@echo proc $(TARGETNAME)_build_options { args } { >> $@
%(echoCarbonVars)s
	@echo ::scsh::cwr_append_ipsimbld_opts preprocessor -I${CARBON_HOME}/include -I'$$'CARBON_OUTPUT %(extraDefines)s >> $@
	@echo ::scsh::cwr_append_ipsimbld_opts linker '$$'CARBON_OUTPUT/'$$'CARBON_MODEL ${CARBON_LIB_LIST_NOMEM} -lz >> $@
%(echoFastLinkOffBuildOption)s
	@echo } >> $@
	@echo ::scsh::add_build_callback [namespace current]::$(TARGETNAME)_build_options >> $@

libcarbon.coware.link.tcl: dummy
	@echo generating $@
	@echo "#linker command file" > $@
%(echoCarbonVars)s
	@echo ::scsh::cwr_append_ipsimbld_opts linker ${CARBON_LIB_LIST_NOMEM} -lz >> $@
%(echoFastLinkOffBuildOption)s

$(TARGETBASE).coware.cmd : dummy
	@echo generating $@
	@echo "#command file" > $@
%(echoCarbonVars)s
	@echo Carbon Libs: ${CARBON_LIB_LIST_NOMEM}
	@echo cwr_append_ipsimbld_opts preprocessor -I${CARBON_HOME}/include -I'$$'CARBON_OUTPUT %(extraDefines)s >> $@
	@echo cwr_append_ipsimbld_opts linker '$$'CARBON_OUTPUT/'$$'CARBON_MODEL ${CARBON_LIB_LIST_NOMEM} -lz >> $@
%(echoFastLinkOffBuildOption)s
''' %  {
         "echoCarbonVars" : self.__echoCarbonVars(' >> $@'),
         "echoFastLinkOffBuildOption" : self.__echoFastLinkOffBuildOption(),
         "extraDefines" : extraDefines,
         "dbFile" : dbFile }
    return code
  
  # Overload
  def constructs(self):
    # State change observer is only available in versions 2010 and later
    if self.__cowareMajorVersion >= 2010:
      return ', Observer()'
    else:
      return ''

  # Overload
  def terminate(self):
    # State change observer is only available in versions 2010 and later
    code = ''
    if self.__cowareMajorVersion > 2010:
      code += '    nCoWare::nSimRunState::removeObserver(this);\n'

    # Add the ScGenerator code which is needed to shutdown the model
    code += ScGenerator.terminate(self)
    return code

  # Helper function to figure out the maximum register offset per size
  def __maxScmlRegOffset(self, config, size):
    max_offset = 0
    for reg in config.scRegisters():
      if reg.size() == size:
        if reg.offset() > max_offset:
          max_offset = reg.offset()
    return max_offset

# Generator for standalone. This overloads a number of functions that
# emit code for SA
class ScGeneratorSA(ScGenerator):
  def __init__(self, config, args, ifaceName, saName):
    ScGenerator.__init__(self, config, args, ifaceName)
    self.__saClassName = saName

    # Change the class define and base classes
    self.moduleObjects().baseClasses.addObject('public sc_module')
    self.moduleObjects().baseClasses.addObject('public %s' % self.__saClassName)

  # Overload
  def carbonModelInclude(self):
    return '"sa.%(className)s.h"' % {'className': self.__saClassName }

  # Overload
  def classDefine(self):
    return 'class %(scName)s' % { 'scName': self.scModuleName() }

  # Overload
  def constructs(self):
    return ', %(className)s()' % {'className': self.__saClassName }
    
  # Overload
  def carbonCreate(self):
    code = """
   init();
   carbonModelHandle = getCarbonObject();
   reset();"""
    return code
  
  # Overload
  def carbonSchedule(self):
    return '      mParent->update( sc_time_stamp().value() );'

  # Overload
  def terminate(self):
    return '    terminate();'

  # Overload
  # Standalone models always initialize themselves in the init() routine
  def carbonInitialize(self):
    return ''

# A container to hold statements for a part of the SystemC module
class ScObjectContainer:
  def __init__(self):
    self.__objects = []

  def addObject(self, object):
    # If the object is empty, don't add it. This is done for convenience
    if (object != None) and (object != ""):
      self.__objects.append(object)

  def emitObjects(self, prefix, separator, postfix):
    code = ''
    if len(self.__objects) > 0:
      code = prefix + separator.join(self.__objects) + postfix
    return code

# A statement container that groups the statements by
# type. That way they can be added to the same region
# of the output file with a comment (using the group name).
class ScObjectGroups:
  def __init__(self):
    self.__groups = {}

  def addObject(self, groupName, object):
    # If the object is empty, don't add it. This is done for convenience
    if (object != None) and (object != ""):
      if not self.__groups.has_key(groupName):
        self.__groups[groupName] = ScObjectContainer()
      self.__groups[groupName].addObject(object)

  def emitObjects(self, prefix, separator, postfix):
    code = ''
    for (groupName, objects) in self.__groups.items():
      code += '  // %s\n' % groupName
      code += objects.emitObjects(prefix, separator, postfix)
      code += '\n\n'
    return code

# Class to manage the objects that will be emitted in the
# various parts of the SystemC module
class ScModuleObjects:
  def __init__(self, config, outputFiles):
    # Save constructor params
    self.__config = config
    self.__outputFiles = outputFiles

    # The following store data to be emitted in the header/cpp files
    self.baseClasses = ScObjectContainer()
    self.baseClassInitializers = ScObjectContainer()
    self.variableInitializers = ScObjectContainer()
    self.constructorStatements = ScObjectGroups()
    self.ports = ScObjectContainer()
    self.publicDeclarations = ScObjectGroups()
    self.privateDeclarations = ScObjectGroups()
    self.defines = ScObjectContainer()
    self.fileIncludes = ScObjectContainer()
    self.dataMembers = ScObjectContainer()
    self.inlinePublicMethods = ScObjectContainer()
    self.inlinePrivateMethods = ScObjectContainer()
    self.privateMethods = ScObjectContainer()
    
  def init(self):
    # Set the default class define and base class initializers. The define can be overridden
    self.baseClassInitializers.addObject('sc_module(inst_name)')

    # Add the port objects
    for port in self.config().scPorts():
      # Create class members and their initialization
      if port.exported():
        # Port initializers must be first
        self.variableInitializers.addObject(port.generateConstructorParms())
      if port.exported():
        self.ports.addObject(port.generateDeclaration())
      else:
        self.privateDeclarations.addObject("signal declarations", port.generateDeclaration())
      self.dataMembers.addObject(port.generateDataMembers())

      # Constructor statements to initialize the Carbon net ids
      delayed = self.hasDelayedOutputs()
      if delayed:
        delayed = port.scName() not in [p.scName() for p in self.config().scClockOutputs()]
      code = port.net().initialize(delayed)
      self.constructorStatements.addObject("Initialize CarbonNetIDs", code)

      # Constructor statements to register change methods for inputs and outputs
      self.constructorStatements.addObject("Register Input Change Methods", port.registerInputChangeMethod())
      self.constructorStatements.addObject("Register Inout Changed Meothods", port.registerOutputResolvedMethod())

      # Class methods for inputs and outputs
      self.inlinePrivateMethods.addObject(port.generateInputChangeMethod(self.__outputFiles.hUserCode()))
      self.inlinePrivateMethods.addObject(port.generateOutputResolvedMethod())

    # Rewalk the port objects and initialize the non port
    # objects. They must be after the ports to preserve the init
    # order.
    for port in self.config().scPorts():
      # Create class members and their initialization
      if not port.exported():
        # Signal/Clock initializers must be after ports
        self.variableInitializers.addObject(port.generateConstructorParms())

    # Add the system component (for replay)
    self.variableInitializers.addObject("mSystemComp(0)")
    self.constructorStatements.addObject("Add the component to the system.", "mSystemComp = carbonSystemAddComponent(system, name(), &carbonModelHandle);")
    self.privateDeclarations.addObject("System declaration", "CarbonSC* mSystemComp;")

    # Add the port wrapper constructor variable initializers
    for port in self.config().scPorts():
      code = port.net().constructorParm()
      if len(code) > 0:
        self.variableInitializers.addObject(code)

    # Add the includes
    self.fileIncludes.addObject('"systemc.h"')
    self.fileIncludes.addObject('"carbon/carbon_sc_multiwrite_signal.h"')
    self.fileIncludes.addObject('"carbon/carbon_system.h"')

    # Delayed outputs add some constructor statements
    if self.hasDelayedOutputs():
      self.constructorStatements.addObject("Delayed Outputs", "SC_METHOD( carbon_write_delayed_outputs );")
      self.constructorStatements.addObject("Delayed Outputs", "sensitive << carbon_schedule_channel.carbon_write_delayed_outputs_event;")
      self.constructorStatements.addObject("Delayed Outputs", "dont_initialize();")

  def config(self):
    return self.__config

  # Combine the base class and variable initializers, the base class must come first
  def getConstructorInitializers(self):
    # There is at least one base class initializer (sc_module)
    code = self.baseClassInitializers.emitObjects(': ', ',\n    ', '')
    code += self.variableInitializers.emitObjects(',\n    ', ',\n    ', '')
    return code

  def hasDelayedOutputs(self):
    # if there are both clocks and data to be output, then we need to insert
    # a delta cycle between them
    insertDelay = len(self.config().scOutputs()) > 0 and len(self.config().scClockOutputs()) > 0
    return insertDelay

# Class to manage the output files for the generator
class ScOutputFiles:
  def __init__(self, config, args, interfaceName):
    # Store constructor arguments
    self.__config = config
    self.__args = args
    self.__interfaceName = interfaceName
    self.__cppIfaceName = interfaceName.replace("-","_")

    # Used for user code sections in the generated code
    self.__hUserCode = Carbon.userCode.UserCode()
    self.__cppUserCode = Carbon.userCode.UserCode()

    # The files we generate are based on the interface name
    self.__hFileName = "lib" + self.__interfaceName + ".systemc.h"
    self.__cppFileName = "lib" + self.__interfaceName + ".systemc.cpp"

    # Include guard is based on header file name
    # Replace any . or - characters with _
    self.__includeGuard = "_" + self.__hFileName.replace(".", "_").replace("-", "_") + "_"

    # Create streams for the two files
    hOutputStream = Carbon.outputStream.OutputStream()
    cppOutputStream = Carbon.outputStream.OutputStream()

    # Read the user code sections so we can maintain them (if they exist)
    self.__hUserCode.parseFile(self.__hFileName, hOutputStream)
    self.__cppUserCode.parseFile(self.__cppFileName, cppOutputStream)

    # Escape any '"' or '\' in the name
    self.__carbonTopName = config.getDB().getTopLevelModuleName().replace('\\', '\\\\').replace('\"','\\"')

  def cppFileName(self):
    return self.__cppFileName

  def hFileName(self):
    return self.__hFileName

  def interfaceName(self):
   return self.__interfaceName

  def cppInterfaceName(self):
   return self.__cppIfaceName

  def scModuleName(self):
    return self.__config.scModuleName()

  def hUserCode(self):
    return self.__hUserCode
  
  def cppUserCode(self):
    return self.__cppUserCode
  
  def carbonTopName(self):
    return self.__carbonTopName

  # generate a comment header for the .h and .cpp files
  def headerComment(self, fileName):
    import datetime
    comment = """\
// -*- C++ -*-
//
// This file contains generated source code for a SystemC module
// that interfaces with a Carbon Model.
//
// File: %(fileName)s
// Date: %(date)s
//
// SystemC Module Name: %(scModuleName)s
//
// Carbon Model Top Level Module: %(topLevelModule)s
// Carbon Model I/O Database: %(iodb)s
// Carbon Model Interface Name: %(target)s
//
// Carbon SystemC Module Generator version: %(version)s
//
// To generate this file:
//   (unix)    $(CARBON_HOME)/bin/carbon systemCWrapper %(args)s
//   (windows) %%CARBON_HOME%%\\bin\\carbon systemCWrapper %(args)s
//
""" % { 'fileName': fileName,
        'date': datetime.datetime.now().ctime(),
        'scModuleName': self.scModuleName(),
        'topLevelModule': self.carbonTopName(),
        'iodb': self.__config.dbFile(),
        'target': self.interfaceName(),
        'version': Carbon.getReleaseId(),
        'args': self.__args }
    return comment

  # Return the pre/post section name for h/cpp user code
  def userCodeSection(self, file, position, section):
    # getattr does not work on __ elements so use an if then else
    if (file == "h"):
      userCode = self.__hUserCode
    elif (file == "cpp"):
      userCode = self.__cppUserCode
    else:
      raise "Invalid user code file '%s'" % file
    function = getattr(userCode, position.lower() + "Section")
    return function(self.scModuleName() + section)

  # Open the output files
  def openFiles(self):
    # Open the files for writing, this truncates the file
    self.__hFile = open(self.__hFileName, "w")
    self.__cppFile = open(self.__cppFileName, "w")

    # Add the common header information
    self.__hFile.write(self.headerComment(self.__hFileName))
    self.__cppFile.write(self.headerComment(self.__cppFileName))

    # Add the header include guard
    self.__hFile.write('''
#ifndef %(includeGuard)s
#define %(includeGuard)s
''' % { 'includeGuard': self.__includeGuard })

  # Write to the header file
  def writeHeaderFile(self, code):
    self.__hFile.write(code)

  # Write to the cpp file
  def writeCppFile(self, code):
    self.__cppFile.write(code)

  # Finish writing the files
  def close(self):
    # Terminate the include guard
    self.__hFile.write("""
#endif  // %(includeGuard)s
""" % { 'includeGuard': self.__includeGuard })
    
    # No need to close any files in Python, but emit a message
    print "Wrote:", self.__cppFileName.strip()
    print "Wrote:", self.__hFileName.strip()
